<?php
# using: 

###########################################################
#
#	Date:	2019-05-08  Bill    [2019-0508-1200-59235]
#           prevent SQL Injection
#           Support "EventGroupType[]" first, then "EventGroupID[]"
#
#	Date:	2018-11-20  Bill    [2018-1023-1036-47066]
#			copy logic from result_export_process.php
#           > support export      ($sys_custom['eSports']['ResultPaperAddAthleteNum'])
#
###########################################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libsports.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");

intranet_auth();
intranet_opendb();

$lexport = new libexporttext();
$lclass = new libclass();
$lsports = new libsports();
$lsports->authSportsSystem();

$numberOfLane = $lsports->numberOfLanes;
$numOfAttempt = 3;

$EventExport = array();

# Get all Event Result 
$EventResultArr = $lsports->Get_All_Event_Round_Result();
$EventResultArr = BuildMultiKeyAssoc($EventResultArr, array("StudentID","EventGroupID","RoundType"), "EventResult", 1);

# Prepare $LastRoundMap
$EventInvolvedRound = $lsports->Get_EventGroup_Involved_Round();

//$EventGroupID = (array)$EventGroupID;
$EventGroupID = isset($EventGroupType)? $EventGroupType : $EventGroupID;
$EventGroupID = (array)$EventGroupID;
for($x=0; $x<sizeof($EventGroupID); $x++)
{
	$tf_flag = 0;
	$hr_flag = 0;
	$cr_flag = 0;
	$theID = $EventGroupID[$x];
	
	$temp = explode("_", $theID);
	$eventGroupID = $temp[0];
	$eventGroupID = IntegerSafe($eventGroupID);
	$roundType = $temp[1];
	$roundType = IntegerSafe($roundType);
	
	# Retreive EventID and GroupID
	$eventGroupInfo = $lsports->retrieveEventGroupDetail($eventGroupID);
	$eventID = $eventGroupInfo[1];
	$groupID = $eventGroupInfo[2];
	
	# Retrieve Event Info
	$eventInfo = $lsports->returnEventInfo($eventID);
	$eventType = $eventInfo[0];
	$eventName = $eventInfo[1];
	$isJump = $eventInfo[2];
	
	# Retrieve Group Name
	if($groupID > 0) {
		$groupName = $lsports->returnAgeGroupName($groupID);
	}
	else if($groupID=='-1') {
		$groupName = $i_Sports_Event_Boys_Open;
	}
	else if($groupID=='-2') {
		$groupName = $i_Sports_Event_Girls_Open;
	}
	else if($groupID=='-4') {
		$groupName = $i_Sports_Event_Mixed_Open;
	}
	
	# Retrieve Ext Info
	$ExtInfo = $lsports->retrieveEventGroupExtInfo($eventGroupID, $eventType);
	
	//$roundTypeArr = $temp[1];
	if(trim($roundType)=='')
	{
		if($eventType <= 2) {
			$roundTypeArr = $lsports->Get_Exist_Round_Of_EventGroup($eventGroupID);
		}
		else {
			$roundTypeArr = array(0);
		}
	}
	else
	{
		$roundTypeArr = $roundType;
	}
	
	foreach((array)$roundTypeArr as $roundType)
	{
		# Check if number of lanes of this round equal to lane number
		$isLaneNum = 0;
		if($eventType==1) {
			if(($ExtInfo["FirstRoundGroupCount"]==0 && $roundType==1) || ($ExtInfo["SecondRoundLanes"]==0 && $roundType==2) || ($ExtInfo["FinalRoundNum"]==0 && $roundType==0)) {
				$isLaneNum = 1;
			}
		}
		
		$roundName = "";
		if($roundType==1 && $ExtInfo["FinalRoundReq"]==1) {
			$roundName = "(".$i_Sports_First_Round.")";
		}
		else if($roundType==2) {
			$roundName = "(".$i_Sports_Second_Round.")";
		}
		else if($roundType==0) {
			$roundName = "(".$i_Sports_Final_Round.")";
		}
		
		if($eventType==2)
		{
			$record = $ExtInfo["RecordMetre"];
			$standard = $ExtInfo["StandardMetre"];
		}
		else
		{
			$record = $ExtInfo["RecordMin"];
			$record .= "'".$ExtInfo["RecordSec"];
			$record .= "''".$ExtInfo["RecordMs"];
	
			$standard = $ExtInfo["StandardMin"];
			$standard .= "'".$ExtInfo["StandardSec"];
			$standard .= "''".$ExtInfo["StandardMs"];
		}
		
		# Retrive total number of heat in this round
		$maxHeat = $lsports->Get_Max_Heat_Of_Round($eventGroupID,$roundType);
		
		# Track and Field Events Table
		if($eventType==1 || $eventType==2)
		{
			$Arrange = $lsports->returnTFLaneArrangeDetailByEventGroupID($eventGroupID, 1, $roundType);
			$thisLastRound = $EventInvolvedRound[$eventGroupID][array_search($roundType,$EventInvolvedRound[$eventGroupID])-1];
			
			if(sizeof($Arrange)!=0)
			{
				// Add blank rows to Field Event 
				if($eventType==2 && !$isResult)
				{
					for($j=0; $j<2; $j++)
					{
						$emptyfield[0] = $Arrange[0][0]; 						// Assign Heat for empty field
						$emptyfield[1] = $Arrange[sizeof($Arrange)-1][1]+1; 	// Assign Arrange Order
						for($i=2; $i<4; $i++) {
							$emptyfield[$i] = "&nbsp;";
						}
						$Arrange[] = $emptyfield;
					}
				}
				
				$orderField = ($eventType==1) ? $i_Sports_Line : $i_Sports_Order;
				$Field_Round_Num = ($isJump==1 || $isResult) ? JUMP_NUMBER : FIELD_NUMBER;
				$spaceReq = ($eventType==1 || $isResult) ? 5 : $Field_Round_Num+4;
				$contentTable = "";
				$currHeat = "";
				
				for($k=0; $k<sizeof($Arrange); $k++)
				{
					list($heat, $order, $sname, $sid,$rank, $score, $trackresult, $fieldresult, $trial1, $trial2, $trial3, $s_ename, $s_cname, $s_classname, $s_classnumber) = $Arrange[$k];
					$nextHeat = $Arrange[$k+1][0];
					
					$athleticNum = $lsports->returnStudentAthleticNum($sid);
					$className =  $lsports->returnStudentClassName($sid);
					$house = $lsports->retrieveStudentHouseInfo($sid);
					$houseName = $house[0][0];
					
					if($heat != $currHeat || $currHeat == "")
					{
					    if(!empty($EventExport)) {
					       $EventExport[] = "";
					    }
					    
					    $EventExport[] = array($i_Sports_Item, $eventName." ".$roundName);
					    $EventExport[] = array($i_Sports_field_Group, $groupName);
					    //$EventExport[] = array($i_Sports_field_Heat, $heat."/".$maxHeat[0]);
					    $EventExport[] = array($i_Sports_field_Heat, $heat);
					    $EventExport[] = array($i_Sports_Record, $record);
					    $EventExport[] = array($i_Sports_Standard_Record, $standard);
					    $EventExport[] = "";
					    
					    $TempAry = array($orderField, $i_Sports_Participant_Number, $Lang['General']['EnglishName'], $Lang['General']['ChineseName'], $i_ClassName, $i_ClassNumber, $i_Sports_House);
					    if($thisLastRound && !$isResult) {
					        $TempAry[] = $Lang['eSports']['RoundResult'][$thisLastRound];
					    }
					    $TempAry[] = $i_Sports_Present;
					    
					    if($eventType==1 || $isResult)
					    {
					        $TempAry[] = $i_Sports_field_Rank;
					        if($eventType == EVENT_TYPE_FIELD && !$isJump) {
					            for($t=1; $t<=$numOfAttempt; $t++) {
					                $TempAry[] = $lsports->Get_Lang_Trial($t);
					            }
					        }
					        $TempAry[] = $eventType==2? $i_Sports_field_Result : $i_Sports_Time;
					        $TempAry[] = $i_Sports_field_Score;
					        $TempAry[] = $i_Sports_field_Remark;
					    }
					    else
					    {
					        for($n=1; $n<=$Field_Round_Num; $n++) {
					            $TempAry[] = $n;
					            if($isJump == 1) {
					                $TempAry[] = '';
					                $TempAry[] = '';
					            }
					        }
					        $TempAry[] = $i_Sports_field_Result;
					        $TempAry[] = $i_Sports_field_Rank;
					        $TempAry[] = $i_Sports_field_Score;
					    }
					    $EventExport[] = $TempAry;
						$currHeat = $heat;
						
						if($isLaneNum==1)
						{
							$diff = $order-1;
							for($a=1; $a<=$diff; $a++)
							{
							    $TempAry = array($a);
// 							    for($m=1; $m<=$spaceReq+6; $m++) {
// 							        $TempAry[] = '';
// 							    }
							    $EventExport[] = $TempAry;
							}
						}
					}
					
					$TempAry = array($order, $athleticNum, $s_ename, $s_cname, $s_classname, $s_classnumber, $houseName);
					if($isResult)
					{
					    $TempAry[] = '';           // present
					    $TempAry[] = $rank;
					    $TempAry[] = '';
						if($eventType == EVENT_TYPE_FIELD && !$isJump) {
						    for($t=1; $t<=$numOfAttempt; $t++) {
						        $TempAry[] = ${"trial$t"};
							}
						}
						$TempAry[] = $eventType == 1? $trackresult : $fieldresult;
						$TempAry[] = $score;
// 						$TempAry[] = '';
					}
					else
					{
						if($isJump==1)
						{
							if($thisLastRound) {
							    $TempAry[] = $EventResultArr[$sid][$eventGroupID][$thisLastRound];
							}
// 							$TempAry[] = '';
// 							for($m=1; $m<=$Field_Round_Num; $m++)
// 							{
// 							    $TempAry[] = '';
// 							    $TempAry[] = '';
// 							    $TempAry[] = '';
// 							}
// 							$TempAry[] = '';
// 							$TempAry[] = '';
// 							$TempAry[] = '';
						}
						else 
						{
							if($thisLastRound) {
							    $TempAry[] = $EventResultArr[$sid][$eventGroupID][$thisLastRound];
							}
// 							for($m=1; $m<=$spaceReq; $m++) {
// 							    $TempAry[] = '';
// 							}
						}
					}
					$EventExport[] = $TempAry;
					
					if($heat != $nextHeat)
					{
						if($isLaneNum==1)
						{
							for($a=($order+1); $a<=$numberOfLane; $a++)
							{
								$TempAry = array($a);
// 								for($m=1; $m<=$spaceReq+6; $m++) {
// 								    $TempAry[] = '';
// 								}
								$EventExport[] = $TempAry;
							}
						}
					}
				}
			}
		}
		# House/Class Relay Event Tables
		else if($eventType==3 || $eventType==4)
		{
			if($eventType==3) {
				$Arrange = $lsports->returnHRLaneArrangeDetailByEventGroupID($eventGroupID);
			}
			else {
				$Arrange = $lsports->returnCRLaneArrangeDetailByEventGroupID($eventGroupID);
			}
			
			$countLane = 1;
			if(sizeof($Arrange)!=0)
			{
			    if(!empty($EventExport)) {
			        $EventExport[] = "";
			    }
			    
			    $EventExport[] = array($i_Sports_Item, $eventName." ".$roundName);
			    $EventExport[] = array($i_Sports_field_Group, $groupName);
			    $EventExport[] = array($i_Sports_Record, $record);
			    $EventExport[] = array($i_Sports_Standard_Record, $standard);
			    $EventExport[] = "";
			    
			    $EventExport[] = array($i_Sports_Line, ($eventType==3? $i_Sports_House : $i_general_class), $i_Sports_field_Rank, $i_Sports_Time, $i_Sports_field_Score, $i_Sports_field_Remark);
				
				for($j=0; $j<sizeof($Arrange); $j++)
				{
					if($eventType==3) {
						list($hname, $order, $hid, $colorCode,$Rank, $Score, $TrackResult) = $Arrange[$j];
					}
					else {
						list($hname_en,$hname_b5, $order, $hid, $Rank, $Score, $TrackResult) = $Arrange[$j];
						$hname = Get_Lang_Selection($hname_b5,$hname_en);
						$Score = '';      // class do not count score
					}
					$nextExist = (sizeof($Arrange[$j+1])==0) ? 0 : 1;
					
					while(($countLane!=$order && $countLane<=$numberOfLane))
					{
						$EventExport[] = array($countLane);
						$countLane++;
					}
					
					$EventExport[] = array($order, $hname, ($isResult? $Rank : ""), ($isResult? $TrackResult : ""), ($isResult? $Score : ""));
					$countLane++;
					
					if($nextExist==0 && $order!=$numberOfLane)
					{
						for($k=($order+1); $k<=$numberOfLane; $k++)
						{
							$EventExport[] = array($k);
						}
					}
				}
			}
		}
	}
}

$ExportArr = $EventExport;

$filename = "EventResultPaper2.csv";
$export_content = $lexport->GET_EXPORT_TXT($ExportArr, "", "\t", "\r\n", "\t", 0, "11");
$lexport->EXPORT_FILE($filename, $export_content);

intranet_closedb();
?>