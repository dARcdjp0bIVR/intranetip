<?php
// Using:
/* 
 * 2018-10-19 (Bill)  : trim() column data for csv export       [2018-1009-1038-48066]
 * 2016-06-13 (Cara)  : Change data table's CSS
 */

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libsports.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$linterface 	= new interface_html();
$CurrentPage	= "PageRaceTopRecord";

$lclass = new libclass();
$lsports = new libsports();
$lsports->authSportsSystem();

# retrieve a initial age group ID
$sql = "SELECT AgeGroupID FROM SPORTS_AGE_GROUP ORDER BY AgeGroupID LIMIT 0, 1";
$default_group = $lsports->returnVector($sql);

if($ageGroup=="")
	$ageGroup = $default_group[0];

$AgeGroups = $lsports->retrieveAgeGroupIDNames();	# retrieve all age group ids and names

# Add Boys Open and Girls Open 
$openBoy[0] = '-1';
$openBoy[1] = $i_Sports_Event_Boys_Open;
$openGirl[0] = '-2';
$openGirl[1] = $i_Sports_Event_Girls_Open;
$openMixed[0] = '-4';
$openMixed[1] = $i_Sports_Event_Mixed_Open;
$AgeGroups[] = $openBoy;
$AgeGroups[] = $openGirl;
$AgeGroups[] = $openMixed;

# Create Age Group Selection List
$select_age_group = getSelectByArray($AgeGroups,"name=ageGroup onChange='document.form2.submit()'","$ageGroup",0,1);

$TrackEventGroups = $lsports->retrieveTrackEventIDAndName();
$FieldEventGroups = $lsports->retrieveFieldEventIDAndName();
$HREventGroups = $lsports->retrieveDistinctHouseRelayName();
$CREventGroups = $lsports->retrieveDistinctClassRelayName();

if($ageGroup>0)
	$EventGroupsArr = array_merge($TrackEventGroups, $FieldEventGroups, $HREventGroups);
else
	$EventGroupsArr = array_merge($TrackEventGroups, $FieldEventGroups, $HREventGroups, $CREventGroups);

$trackNum = sizeof($TrackEventGroups);
$fieldNum = sizeof($FieldEventGroups);

### Table content 
$content = "";
$content .= "<table width='100%' border='0' cellspacing='0' cellpadding='4'>";
$content .= "<tr>";
$content .= "<td class='tablebluetop tabletopnolink'>$i_Sports_Report_Item_Name</td>";
$content .= "<td class='tablebluetop tabletopnolink'>$i_Sports_Record_Holder</td>";
$content .= "<td class='tablebluetop tabletopnolink'>$i_Sports_Record</td>";
$content .= "<td class='tablebluetop tabletopnolink'>$i_Sports_Record_Year</td>";
$content .= "<td class='tablebluetop tabletopnolink'>$i_Sports_House</td>";
$content .= "</tr>";

$groupInfo = $lsports->retrieveAgeGroupDetail($ageGroup);
$groupName = ($intranet_session_language=="en") ? $groupInfo[1] : $groupInfo[2];

$export_content .= $i_Sports_field_Group.":\t";
$export_content .= trim($groupName)."\n\n";
$export_content .= $i_Sports_Report_Item_Name."\t";
$export_content .= $i_Sports_Record_Holder."\t";
$export_content .= $i_Sports_Record."\t";
$export_content .= $i_Sports_Record_Year."\t";
$export_content .= $i_Sports_House."\n";

$tableDisplay = "";
for($j=0; $j<sizeof($EventGroupsArr); $j++)
{			
	list($eventID, $ename) = $EventGroupsArr[$j];

	if($j < $trackNum)
		$eventType = 1;
	else if($j >= $trackNum && $j < ($trackNum+$fieldNum))
		$eventType = 2;
	else
	{
		$temp_data = $lsports->retrieveTrackFieldDetail($eventID);
		list($tempEventType, $tempEnglishName, $tempChineseName, $tempDisplayOrder, $tempIsJump) = $temp_data;
		$eventType = $tempEventType;
	}

	$egid = $lsports->retrieveEventGroupID($eventID, $ageGroup, $eventType);
	$ExtInfo = $lsports->retrieveEventGroupExtInfo($egid[0], $eventType);
	
        $tableDisplay .= "<tr class='tablebluerow".($j%2?"2":"1")."'>";
	$tableDisplay .= "<td class='tabletext'>".$ename."</td>";

	if($ExtInfo["EventGroupID"]!="")
	{
		$recordHolderName = $ExtInfo["RecordHolderName"];
		$recordHouseID = $ExtInfo["RecordHouseID"];
		$recordYear = $ExtInfo["RecordYear"];

		$tableDisplay .= "<td class='tabletext'>".$recordHolderName."</td>";
		if($eventType==2)
		{
			$record = $ExtInfo["RecordMetre"];
		}
		else
		{
			$record = $ExtInfo["RecordMin"];
			$record .= "'".$ExtInfo["RecordSec"];
			$record .= "''".$ExtInfo["RecordMs"];
		}
		$tableDisplay .= "<td class='tabletext'>".$record."</td>";
		$tableDisplay .= "<td class='tabletext'>".$recordYear."</td>";

		$house = $lsports->getHouseDetail($recordHouseID);
		$houseName = ($intranet_session_language=="en") ? $house[1] : $house[2];
                $houseColor = $house[4];
                $tableDisplay .= "<td class='tabletext'>".$lsports->house_flag2($houseColor, $houseName)."</td>";
                

		$export_content .= trim($ename)."\t";
		$export_content .= trim($recordHolderName)."\t";
		$export_content .= trim($record)."\t";
		$export_content .= trim($recordYear)."\t";
		$export_content .= trim($houseName) ."\n";
	}
	else
	{
		$tableDisplay .= "<td colspan='4' class='tabletext'>&nbsp;</td>";

		$export_content .= trim($ename)."\t\t\t\t\n";

	}
	
}
$content .= $tableDisplay;
$content .= "</table>";
### Table content - End

### Title ###
//$TAGS_OBJ[] = array($i_Sports_menu_Report_RaceTopRecord , "race.php", 1);    
$TAGS_OBJ[] = array($i_Sports_menu_Report_RaceTopRecord , "race.php", 1);    
//$TAGS_OBJ[] = array($i_Sports_menu_Report_Best_Athlete_By_Group, "best_athlete_by_group.php", 0);    
//$TAGS_OBJ[] = array($i_Sports_menu_Report_Best_Athlete_By_Class, "best_athlete_by_class.php", 0);    

$MODULE_OBJ = $lsports->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();
?>

<br />
<table width="96%" border="0" cellpadding="5" cellspacing="0">
<tr>
	<td><br />
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
			<td>
                        	<form name="form2" action="race.php" method="post">
                                <table class="form_table_v30">
                                <tr>
                                <td class="field_title"><span class="tabletext"><?=$i_Sports_field_Group?> </span></td>
                                <td><?=$select_age_group?></td>
                                </tr>
                                </table>
                                </form>
			</td>
		</tr>
                <tr>
                	<td height="1" class="dotline"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
                </tr>
                </table>
	</td>
</tr>
<tr>
	<td><br /><?=$content?></td>
</tr>        

<form name="form1" action='general_export.php' method="post">
<tr>
	<td height='1' class='dotline'><img src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif' width='10' height='1'></td>
</tr>     
<tr>
	<td align='center'><?=$linterface->GET_ACTION_BTN($button_export, "submit", "","submit2")?></td>
</tr>

<input type="hidden" name="exportFileName" value="race">
<input type="hidden" name="exportContent" value="<?=$export_content?>">
</form>

</table>
<br />

<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>