<?php
# using: 

/*********************************************
 * Modification log
 *  2020-02-04  Philips
 *  - Added Print button for $sys_custom['eSports']['SportDay_FinalResultReport_MKSS']
 *  
 *  2019-11-20  Philips [2019-1024-1748-37066]
 *  - Replace $i_Sports_menu_Report_EventRanking by $Lang['eSports']['IndividualEventRanking']
 *  
 *  2019-10-25	Philips [2019-1024-1748-37066]
 *  - event select box filter house relay and class relay event type
 *
 *  2019-04-11  Bill    [2019-0301-1144-56289]
 *  - Change Tab - Redirect to cust Group Relay page    ($sys_custom['eSports']['KaoYipRelaySettings'])
 *  
 *  2018-10-29  Ivan    [2018-1026-1022-53235]
 *  - Show House Name
 *  
 * 	2017-02-27	Bill	[2016-0627-1013-19066]
 * 	- Hide Tab Group Champion		($sys_custom['eSports']['PuiChi_MultipleGroup'])
 *
 *	2013-08-30	Roy
 *	- add export all score details button
 *
 *	2011-06-14	YatWoon
 *	- Improved: Display event for user selection [Case#2011-0106-1041-15073]
 *
 *	2011-02-07	YatWoon
 *	- change the general export call page "event_rank_export.php"
 *
 *	2011-01-05	YatWoon
 *	- add print function
 *	- update export / print UI
 *
 * 	20101103 Marcus:
 * 		add column "EventCode" to the export function
 *
 *********************************************/

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libsports.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$CurrentPage = "PageRaceResult";

$lclass = new libclass();
$lsports = new libsports();
$lsports->authSportsSystem();

$AgeGroups = $lsports->retrieveAgeGroupIDNames();	# retrieve all age group ids and names

# Add Boys Open and Girls Open
$openBoy[0] = '-1';
$openBoy[1] = $i_Sports_Event_Boys_Open;
$openGirl[0] = '-2';
$openGirl[1] = $i_Sports_Event_Girls_Open;
$openMixed[0] = '-4';
$openMixed[1] = $i_Sports_Event_Mixed_Open;
$AgeGroups[] = $openBoy;
$AgeGroups[] = $openGirl;
$AgeGroups[] = $openMixed;

# Create Age Group Selection List
$select_age_group = getSelectByArray($AgeGroups,"name=ageGroup","$ageGroup",1,0,$AllGroups);

$TFEventGroups = $lsports->retrieveTrackFieldEventName();
$HREventGroups = $lsports->retrieveDistinctHouseRelayName();
$CREventGroups = $lsports->retrieveDistinctClassRelayName();

$EventGroupInfo = $lsports->Get_EventGroup_Info();
$EventCodeArr = BuildMultiKeyAssoc($EventGroupInfo,"EventGroupID","EventCode",1);

$tfNum = sizeof($TFEventGroups);
$maxRank = 8;

$db_field = ($intranet_session_language=="en"? "EnglishName" : "ChineseName");
$cond = " EventType IN ('" . EVENT_TYPE_TRACK. "', '" . EVENT_TYPE_FIELD. "') ";
$sql = "SELECT
            EventID,
            $db_field
        FROM SPORTS_EVENT
		WHERE $cond
        ORDER BY EventType, DisplayOrder ";
$result = $lsports->returnArray($sql);

$select_event = getSelectByArray($result, "id='targetEventID' name='targetEventID[]' multiple size='10'", $targetEventID, 0, 1);
$select_event .= "&nbsp;".$linterface->Get_Small_Btn($Lang['Btn']['SelectAll'],"button","js_Select_All_With_OptGroup('targetEventID',1)");
$select_event .= $linterface->spacer();
$select_event .= $linterface->MultiSelectionRemark();
if(!empty($targetEventID)) {
    $targetEventIDstr = implode(",",$targetEventID);
}

# Content
$content = "";
$export_content = $Lang['eSports']['IndividualEventRanking']."\n\n";

for($i=0; $i<sizeof($AgeGroups); $i++)
{
    list($gid, $gname) = $AgeGroups[$i];
    if($gid>0) {
        $EventGroupsArr = array_merge($TFEventGroups, $HREventGroups);
    }
    else {
        $EventGroupsArr = array_merge($TFEventGroups, $HREventGroups, $CREventGroups);
    }
    if($ageGroup==$gid || $ageGroup=="")
    {
        $export_content .= $gname."\n";
        $export_content .= $i_Sports_Report_Item_Name."\t";
        $export_content .= $Lang['eSports']['EventCode']."\t";
        
        $content .= "<table width='100%' border='0' cellspacing='0' cellpadding='0'>";
        $content .= "<tr>";
        $content .= "<td align='right'><table width='100%' border='0' cellspacing='0' cellpadding='0'>";
        $content .= "<tr>";
        $content .= "<td align='right'><table width='100%' border='0' cellspacing='0' cellpadding='0'>";
        $content .= "<tr>";
        $content .= "<td align='left'>". $linterface->GET_NAVIGATION2($gname) ."</td>";
        $content .= "</tr>";
        $content .= "</table></td>";
        $content .= "</tr>";
        $content .= "<tr>";
        $content .= "<td height='10' align='right' class='tabletextremark'><table width='100%' border='0' cellspacing='0' cellpadding='0'>";
        $content .= "<tr>";
        $content .= "</tr>";
        $content .= "</table></td>";
        $content .= "</tr>";
        $content .= "</table></td>";
        $content .= "</tr>";
        
        # Header Title
        $content .= "<tr>";
        $content .= "<td><table width='100%' border='0' cellspacing='0' cellpadding='0'>";
        $content .= "<tr><td><table width='100%' border='0' cellspacing='0' cellpadding='4'>";
        $content .= "<tr class='tablebluetop'>";
        $content .= "<td width='20%'><span class='tabletopnolink'>$i_Sports_Report_Item_Name</span></td>";
        
        for($j=1; $j<=$maxRank; $j++)
        {
            if($intranet_session_language=="en")
            {
                if($j==1) {
                    $temp = $j."st";
                }
                else if($j==2) {
                    $temp = $j."nd";
                }
                else if($j==3) {
                    $temp = $j."rd";
                }
                else {
                    $temp = $j."th";
                }
            }
            else
            {
                $temp = $i_Sports_The.$j.$i_Sports_Rank;
            }
            $content .= "<td class='tabletopnolink' align='center' width='10%'>".$temp."</td>";
            
            if(($j+1)<=$maxRank) {
                $export_content .= $temp."\t";
            }
            else {
                $export_content .= $temp."\n";
            }
        }
        $content .= "</tr>";
        
        # Table Content
        $tableDisplay = "";
        $css = 0;
        for($j=0; $j<sizeof($EventGroupsArr); $j++)
        {
            list($eventID, $ename) = $EventGroupsArr[$j];
            
            if ((!empty($targetEventID) && in_array($eventID, $targetEventID)) || empty($targetEventID))
            {
                $temp_data = $lsports->retrieveTrackFieldDetail($eventID);
                list($tempEventType, $tempEnglishName, $tempChineseName, $tempDisplayOrder, $tempIsJump) = $temp_data;
                
                $tempEventGroupID = $lsports->retrieveEventGroupID($eventID, $gid, $tempEventType);
                $EventGroupID = $tempEventGroupID['EventGroupID'];
                
                if($j<$tfNum)
                {
                    # only display the event is student enroled
                    $enroledStudentNo = $lsports->retrieveEventEnroledCount($EventGroupID);
                    if(!$enroledStudentNo)	continue;
                    
                    $Ranking = $lsports->retrieveTFRanking($eventID, $gid, $finalOnly=true);
                    $isRelay = 0;
                }
                else
                {
                    if($tempEventType==3)
                    {
                        # only display the event is student enroled
                        $enroledNo = $lsports->retrieveHREnroledCount($EventGroupID);
                        if($enroledNo==0 or $enroledNo=="")	continue;
                        
                        $Ranking = $lsports->retrieveHRRanking($eventID, $gid);
                    }
                    else
                    {
                        # only display the event is student enroled
                        $enroledNo = $lsports->retrieveCREnroledCount($EventGroupID);
                        if(!$enroledNo)	continue;
                        
                        $Ranking = $lsports->retrieveCRRanking($eventID, $gid);
                    }
                    $isRelay = 1;
                }
                
                $tableDisplay .= "<tr class='tablebluerow".($css%2?"2":"1")."'>";
                $tableDisplay .= "<td class='tabletext'>". $ename."</td>";
                $export_content .= $ename."\t";
                $export_content .= $EventCodeArr[$EventGroupID]."\t";
                
                $tctr = 0; //count all same-ranked individual in each event and print empty td at the end
                $col = 0;
                for($k=0; $k<$maxRank; $k++)
                {
                    if(sizeof($Ranking[$k])!=0)
                    {
                        if(!$isRelay)
                        {
                            # 20081013 add result in "Event Rankings" list
                            // modified by marcus 22/6
                            $ctr = 0; // count same-ranked individual
                            $all_stu_info="";
                            $export_tmp = "";
                            do{
                                list($sname, $sid, $rank, $status, $stu_name, $stu_class, $min, $sec, $ms, $metre) = $Ranking[$k+$ctr];
                                if($metre) {
                                    $event_result = $metre ."m";
                                }
                                else {
                                    $event_result = $min ."'". $sec ."''". $ms;
                                }
                                
                                $house = $lsports->retrieveStudentHouseInfo($sid);
                                $hcode = $house[0][3];
                                $hcolor = $house[0][2];
                                // [K151652]
                                $hname = $house[0][0];
                                
                                # Student Info
                                $stu_info = "<table width='100%' border='0' cellspacing='0' cellpadding='2'>";
                                $stu_info .= "<tr>";
                                $stu_info .= "<td width='100%'><table width='100%' border='0' cellspacing='0' cellpadding='0'>";
                                $stu_info .= "<tr>";
                                $stu_info .= "<td align='center' class='tabletext'>$stu_name</td>";
                                $stu_info .= "</tr>";
                                $stu_info .= "<tr>";
                                
                                // [K151652]
                                //$stu_info .= "<td align='center' class='tabletext'>".$lsports->house_flag2($hcolor, $stu_class)."</td>";
                                $stu_info .= "<td align='center' class='tabletext'>".$lsports->house_flag2($hcolor, $hname.' '.$stu_class)."</td>";
                                $stu_info .= "</tr>";
                                $stu_info .= "<tr>";
                                
                                $status_tag = "";
                                if($status==3 || $status==4)
                                {
                                    $status_img = ($status==3?"icon_qualified.gif":($status==4?"icon_recordbroken.gif":""));
                                    $status_tag = "<img src='{$image_path}/{$LAYOUT_SKIN}/esport/$status_img' width='20' height='20' align='absmiddle' />";
                                }
                                
                                $stu_info .= "<td align='center'><span class='tabletext'>$status_tag $event_result</span></td>";
                                $stu_info .= "</tr>";
                                $stu_info .= "</table></td>";
                                $stu_info .= "</tr>";
                                $stu_info .= "</table>";
                                $all_stu_info .=$stu_info; //print same-ranked individual in the same ranking td
                                
                                $ctr++;
                                
                                $sname = "(".$hcode.")".$sname;
                                if($status==3) {
                                    $sname = "#".$sname;
                                }
                                else if($status==4) {
                                    $sname = "*".$sname;
                                }
                                
                                if($export_tmp)
                                {
                                    $export_tmp = substr($export_tmp, 0,strlen($export_tmp)-1) . ", ". $sname . ' ('. $event_result.')"';
                                }
                                else
                                {
                                    $export_tmp .= '"'.$sname . ' ('. $event_result.')"';
                                }
                            }while($Ranking[$k+$ctr]["Rank"]==$rank);
                            $ctr-=1;
                            $tableDisplay .= "<td class='tabletext'>".$all_stu_info."</td>";
                            $col++; // count no of columns printed
                            $k+=$ctr;
                            
                            if($k<7)	  // if more than 1 student rank in 8 , don't need to print empty td at the end
                            {
                                if($lsports->RankPattern=="1223")
                                {
                                    $tctr+=$ctr;
                                }
                                else
                                {
                                    for($ctr; $ctr>0; $ctr--)
                                    {
                                        $export_tmp .= "\t";
                                        $tableDisplay .= "<td class='tabletext'>&nbsp;</td>";
                                        $col++; // count no of columns printed
                                    }
                                }
                            }
                            
                            // modified by marcus end
                            /*
                             $sname = "(".$hcode.")".$sname;
                             
                             if($status==3)
                             $sname = "#".$sname;
                             else if($status==4)
                             $sname = "*".$sname;
                             
                             # add event result to export
                             if(($k+1)<$maxRank)
                             $export_content .= '"'.$sname . ' ('. $event_result.')"'."\t";
                             else
                             $export_content .= '"'.$sname . ' ('. $event_result.')"'."\n";
                             */
                             if(($k+1)<$maxRank) {
                                 $export_content .= $export_tmp."\t";
                             }
                             else {
                                 $export_content .= $export_tmp."\n";
                             }
                        }
                        else	# 20081014 process relay ranking (yatwooon)
                        {
                            // modified by marcus 22/6
                            $ctr = 0;
                            $all_house_info="";
                            $export_tmp = "";
                            do{
                                list($housename, $hid, $rank, $status, $min, $sec, $ms) = $Ranking[$k+$ctr];
                                $event_result = $min ."'". $sec ."''". $ms;
                                
                                if($tempEventType==3)
                                {
                                    $house = $lsports->getHouseDetail($hid);
                                    $hcode = $house[5];
                                    $hcolor = $house[4];
                                    $display_name = $lsports->house_flag2($hcolor, $housename);
                                }
                                else
                                {
                                    $display_name = $housename;
                                }
                                
                                # House / Class Info
                                $house_info = "<table width='100%' border='0' cellspacing='0' cellpadding='2'>";
                                $house_info .= "<tr>";
                                $house_info .= "<td width='100%'><table width='100%' border='0' cellspacing='0' cellpadding='0'>";
                                $house_info .= "<tr>";
                                $house_info .= "<td align='center' class='tabletext'>".$display_name. "</td>";
                                $house_info .= "</tr>";
                                $house_info .= "<tr>";
                                
                                $status_tag = "";
                                if($status==3 || $status==4)
                                {
                                    $status_img = ($status==3?"icon_qualified.gif":($status==4?"icon_recordbroken.gif":""));
                                    $status_tag = "<img src='{$image_path}/{$LAYOUT_SKIN}/esport/$status_img' width='20' height='20' align='absmiddle' />";
                                }
                                
                                $house_info .= "<td align='center'><span class='tabletext'>$status_tag $event_result</span></td>";
                                $house_info .= "</tr>";
                                $house_info .= "</table></td>";
                                $house_info .= "</tr>";
                                $house_info .= "</table>";
                                $all_house_info .=$house_info;
                                $ctr++;
                                
                                $sname = "(".$hcode.")".$sname;
                                if($status==3) {
                                    $housename = "#".$housename;
                                }
                                else if($status==4) {
                                    $housename = "*".$housename;
                                }
                                
                                if($export_tmp)
                                {
                                    $export_tmp = substr($export_tmp, 0,strlen($export_tmp)-1) . ", ". $housename . ' ('. $event_result.')"';
                                }
                                else
                                {
                                    $export_tmp .= '"'.$housename . ' ('. $event_result.')"';
                                }
                            }while($Ranking[$k+$ctr]["Rank"]==$rank);
                            $ctr-=1;
                            $tableDisplay .= "<td class='tabletext'>".$all_house_info."</td>";
                            $col++; // count no of columns printed
                            $k+=$ctr;
                            
                            if($k<7)	 //if more than 1 student rank in 8 , don't need to print empty td at the end
                            {
                                if($lsports->RankPattern=="1223")
                                {
                                    $tctr+=$ctr;
                                }
                                else
                                {
                                    for($ctr; $ctr>0; $ctr--)
                                    {
                                        $export_tmp .= "\t";
                                        $tableDisplay .= "<td class='tabletext'>&nbsp;</td>";
                                        $col++; // count no of columns printed
                                    }
                                }
                            }
                            
                            //modified by marcus end
                            /*
                             if($status==3)
                             $housename = "#".$housename;
                             else if($status==4)
                             $housename = "*".$housename;
                             
                             # add event result to export
                             if(($k+1)<$maxRank)
                             $export_content .= '"'.$housename . ' ('. $event_result.')"'."\t";
                             else
                             $export_content .= '"'.$housename . ' ('. $event_result.')"\n';
                             */
                             
                             if(($k+1)<$maxRank) {
                                 $export_content .= $export_tmp."\t";
                             }
                             else {
                                 $export_content .= $export_tmp."\n";
                             }
                        }
                    }
                    else
                    {
                        $tableDisplay .= "<td width='50' class='tabletext'>&nbsp;</td>";
                        $col++; // count no of columns printed
                        
                        if(($k+1)<$maxRank) {
                            $export_content .= "\t";
                        }
                        else {
                            $export_content .= "\n";
                        }
                    }
                }
                
                // Print empty td at the end
                for($col=$maxRank-$col; $col>0; $col--) {
                    $tableDisplay .= "<td class='tabletext'>&nbsp;</td>";
                }
                $tableDisplay .= "</tr>";
                
                $export_content .= "\n";
                $css++;
            }
        }
        
        $export_content .= "(*) ".$i_Sports_RecordBroken."\n";
        $export_content .= "(#) ".$i_Sports_Qualified."\n";
        $export_content .= "\n\n";
        
        $content .= $tableDisplay;
        $content .= "</table></td></tr>";
        $content .= "<tr>";
        $content .= "<td height='1' class='dotline'><img src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif' width='10' height='1'></td>";
        $content .= "</tr>";
        $content .= "</table>";
        
        $content .= "<table cellspacing='0' cellpadding='3' border='0'>";
        $content .= "<tr>";
        $content .= "<td nowrap='nowrap' class='tabletextremark'><img src='{$image_path}/{$LAYOUT_SKIN}/esport/icon_recordbroken.gif' width='20' height='20' align='absmiddle' /> $i_Sports_RecordBroken</td>";
        $content .= "<td class='tabletextremark'><img src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif' width='10' height='10' /></td>";
        $content .= "<td nowrap='nowrap' class='tabletextremark'><img src='{$image_path}/{$LAYOUT_SKIN}/esport/icon_qualified.gif' width='20' height='20' align='absmiddle' /> $i_Sports_Qualified</td>";
        $content .= "</tr>";
        $content .= "</table><br />";
        
        $content .= "</table>";
        $content .= "<br>";
    }
}
$export_content = str_replace('"', '&quot;',$export_content);

# Title
$house_relay_name = $lsports->retrieveEventTypeNameByID(3);
$class_relay_name = $lsports->retrieveEventTypeNameByID(4);

$TAGS_OBJ[] = array($i_Sports_menu_Participation_TrackField, "../participation/tf_record.php", 0);
$TAGS_OBJ[] = array($house_relay_name, "../participation/relay_record.php", 0);
// [2019-0301-1144-56289]
if($sys_custom['eSports']['KaoYipRelaySettings']) {
    $TAGS_OBJ[] = array($class_relay_name, "../participation/class_relay_group_record.php", 0);
} else {
    $TAGS_OBJ[] = array($class_relay_name, "../participation/class_relay_record.php", 0);
}
$TAGS_OBJ[] = array($Lang['eSports']['IndividualEventRanking'], "../report/event_rank.php", 1);
$TAGS_OBJ[] = array($i_Sports_menu_Report_HouseGroupScore, "../report/house_group.php", 0);
// [2016-0627-1013-19066]
if(!$sys_custom['eSports']['PuiChi_MultipleGroup']) {
    $TAGS_OBJ[] = array($i_Sports_menu_Report_GroupChampion, "../report/group_champ.php", 0);
}
$MODULE_OBJ = $lsports->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();
?>

<div class="content_top_tool">
	<div class="Conntent_tool">
		<div class="btn_option">
			<a href="#" class="export" id="btn_export" onclick="MM_showHideLayers('export_option','','show');document.getElementById('btn_export').className ='export parent_btn';"> <?=$button_export?></a>
			<br style="clear:both" />
			<div class="btn_option_layer" id="export_option" onclick="MM_showHideLayers('export_option','','hide');document.getElementById('btn_export').className ='export';">
				<a href="#" onClick="document.form1.submit();" class="sub_btn"> <?=$button_export?></a>
				<a href="#" onClick="document.form3.submit();" class="sub_btn"> <?=$i_Sports_export_reuslt?></a>
				<a href="all_score2.php" class="sub_btn"> <?=$Lang['eSports']['ExportAthleteResultInDetail']?></a>
			</div>
		</div>
	</div>
	<?= $linterface->GET_LNK_PRINT_IP25("javascript:newWindow('event_rank_print.php?ageGroup=$ageGroup&targetEventIDstr=$targetEventIDstr',93);") ?>
	<?php if($sys_custom['eSports']['SportDay_FinalResultReport_MKSS']){
		echo $linterface->GET_LNK_PRINT_IP25("javascript:newWindow('event_rank_print_mkss.php',93);", $Lang['eSports']['Sports']['Print_FinalResultReport_MKSS']);
	}?>
<br style="clear:both" />
</div>

<form name="form2" action="event_rank.php" method="post">
<table class="form_table_v30">
<tr>
	<td class="field_title"><?=$i_Sports_field_Group?></td>
	<td><?=$select_age_group?></td>
</tr>
<tr>
	<td class="field_title"><?=$i_Sports_menu_Settings_TrackFieldName?></td>
	<td><?=$select_event?></td>
</tr>
</table>

<div class="edit_bottom_v30">
<p class="spacer"></p>
<?= $linterface->GET_ACTION_BTN($button_submit, "submit")?>&nbsp;
</div>
</form>

<?=$content?>

<form name="form1" action='event_rank_export.php' method="post">
<input type="hidden" name="ageGroup" value="<?=$ageGroup?>">
<input type="hidden" name="targetEventIDstr" value="<?=$targetEventIDstr?>">
</form>

<form name="form3" action='event_rank_export_result.php' method="post">
<input type="hidden" name="ageGroup" value="<?=$ageGroup?>">
<input type="hidden" name="targetEventIDstr" value="<?=$targetEventIDstr?>">
</form>

<?php
if(empty($targetEventID))
{
?>
	<script language="javascript">
	js_Select_All_With_OptGroup('targetEventID',1);
	</script>
<? 	
}

intranet_closedb();
$linterface->LAYOUT_STOP();
?>