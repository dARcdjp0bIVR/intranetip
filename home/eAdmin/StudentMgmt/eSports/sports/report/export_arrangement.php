<?php
# using:

######################################
#
#	Date:	2019-05-08  Bill     [2019-0508-1200-59235]
#           prevent SQL Injection
#           Change Selection name "AgeGroupID" to "AgeGroup"
#           Change Selection name "genderID" to "gender"
#
#	Date:	2018-11-02  Bill     [2018-1031-0950-40066]
#			add another export format (Athlete Number only)
#
######################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libsports.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable{$LAYOUT_SKIN}.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

global $openGroupList;

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$CurrentPage = "PageReport_ExportRecord";

$lclass = new libclass();
$lsports = new libsports();
$lsports->authSportsSystem();

# initial event selection
$all_1[0] = '-3';
$all_1[1] = $i_Sports_All;
$eventList[] = $all_1;

$db_field = $intranet_session_language=="en"? "EnglishName" : "ChineseName";
$sql = "SELECT EventID, $db_field FROM SPORTS_EVENT";
$events = $lsports->returnArray($sql,2);
foreach($events as $arr) {
	$eventList[] = $arr;
}

if(!isset($AgeGroup) || $AgeGroup == "") {
     $AgeGroup = -3;
}
if($AgeGroup == "") {
    header("Location: tf_name.php");
    exit();
}
if(!isset($eventID) || $eventID == "") {
	$eventID = -3;
}
if(!isset($gender) || $gender == "") {
	$gender = -3;
}
if(!isset($raceDay) || $raceDay == "") {
	$raceDay = -3;
}

# initial group selection
$all_2[0] = '-3';
$all_2[1] = $i_Sports_All_Grade;
$groups[0] = $all_2;

$sql = " SELECT DISTINCT GradeChar FROM SPORTS_AGE_GROUP ";
$Grade=$lsports->returnArray($sql,1);
foreach($Grade as $i => $value)
{
	$groups[$i+1][0]= $value[0];
	$groups[$i+1][1]= $value[0];	
}

$Open[0] = 'O';
$Open[1] = $i_Sports_Event_Open;
$groups[] = $Open;

$AllGender[0] = '-3';
$AllGender[1] = $i_Sports_Event_All;
$Boys[0] = 'M';
$Boys[1] = $i_Sports_Event_Boys;
$Girls[0] = 'F';
$Girls[1] = $i_Sports_Event_Girls;
$Mixed[0] = '-4'; // Mixed Group
$Mixed[1] = $i_Sports_Event_Mixed;
$genderArr[] = $AllGender;
$genderArr[] = $Boys;
$genderArr[] = $Girls;
$genderArr[] = $Mixed;

### Handle SQL Injection + XSS [START]
$allAgeGroupArr = Get_Array_By_Key($groups, 0);
if(!in_array($AgeGroup, (array)$allAgeGroupArr)) {
    $AgeGroup = '';
}

$allGenderArr = Get_Array_By_Key($genderArr, 0);
if(!in_array($gender, (array)$allGenderArr)) {
    $gender = '';
}

$eventID = IntegerSafe($eventID);
$raceDay = IntegerSafe($raceDay);
### Handle SQL Injection + XSS [END]

// initial day select
$day[0][0]=-3;
$day[0][1]=$i_Sports_Event_All;
$no_of_day=3;
for($i=1; $i<=$no_of_day; $i++) {
	$day[$i][0] = $i;
	$day[$i][1] = $i;
}

$select_event = $i_Sports_Item." : ".getSelectByArray($eventList,"name='eventID' onChange='this.form.action=\"\"; this.form.submit()'",$eventID,0,1);
$select_group = $i_Sports_field_Grade." : ".getSelectByArray($groups,"name='AgeGroup' onChange='this.form.action=\"\"; this.form.submit()'",$AgeGroup,0,1);
$select_gender = $i_Sports_field_Gender." : ".getSelectByArray($genderArr,"name='gender' onChange='this.form.action=\"\"; this.form.submit()'",$gender,0,1);
$select_day = $i_Sports_Race_Day." : ".getSelectByArray($day,"name='raceDay' onChange='this.form.action=\"\"; this.form.submit()'",$raceDay,0,1);

$AgeGroup_cond = "";
$eventList_cond = "";

# involved table
#a -- SPORTS_EVENTGROUP
#b -- SPORTS_EVENT
#c -- SPORTS_AGE_GROUP
#d -- SPORTS_EVENTGROUP_EXT_RELAY

if($eventID!="-3")
{
    $eventList_cond = "AND b.EventID='$eventID'";
}

if($raceDay!="-3")          // for track and field only, as house and class relay don't indicate raceday
{
    $raceDay_cond = " AND (e.FirstRoundDay='$raceDay' OR e.SecondRoundDay='$raceDay' OR e.FinalRoundDay='$raceDay' 
						OR f.FirstRoundDay='$raceDay' OR f.FinalRoundDay='$raceDay') "; 
}

if($AgeGroup=="O")          // select open event only
{
	switch($gender)
	{
		case 'M':     $tmp="-1"; break;       // boys open
		case 'F':     $tmp="-2"; break;       // girls open
		case '-4':    $tmp="-4"; break;       // mixed open
		default:      $tmp="-1,-2,-4";        // all open
	}
	$AgeGroup_cond = " AND a.GroupID IN ($tmp) " ;
}
else if($AgeGroup!="-3")    // select any 1 age group 
{
	switch($gender)
	{
		case 'M':                                                                                 // boys (same as girls)
		case 'F':     $tmp=" AND c.GradeChar = '$AgeGroup' AND c.gender = '$gender' "; break;     // girls
		case '-4':    $tmp=" AND c.GradeChar = '$AgeGroup' AND a.GroupID = -4 "; break;           // mixed
		default:      $tmp=" And c.GradeChar = '$AgeGroup' ";                                     // all open
	}
	$AgeGroup_cond = $tmp;
}
else                        // select all age group
{
	switch($gender)
	{
		case 'M':     $tmp=" AND (c.gender = '$gender' OR a.GroupID = -1)"; break;                // boys
		case 'F':     $tmp=" AND (c.gender = '$gender' OR a.GroupID = -2)"; break;                // girls
		case '-4':    $tmp=" AND a.GroupID = -4 "; break;                                         // mixed
		default:      $tmp=" ";                                                                   // all open
	}
	$AgeGroup_cond = $tmp;
}
$con = $AgeGroup_cond.$eventList_cond.$raceDay_cond;

$AgeGroups = $lsports->retrieveAgeGroupIDNames();                       # retrieve all age group ids and names
$TFEvents = $lsports->retrieveTrackFieldEventNameByCondition($con);     # retrieve track and field event_ids, group_ids and names
$HREvents = $lsports->retrieveHouseRelayNameByCondition($con);          # retrieve house relay event_ids, group_ids and names
$CREvents = $lsports->retrieveClassRelayNameByCondition($con);          # retrieve house relay event_ids, group_ids and names
// modified by marcus end

for($i=0; $i<sizeof($AgeGroups); $i++)
{
    list($gid, $gname) = $AgeGroups[$i];
    $groupArr[$gid] = $gname;
}
$groupArr['-1'] = $i_Sports_Event_Boys_Open;
$groupArr['-2'] = $i_Sports_Event_Girls_Open;
$groupArr['-4'] = $i_Sports_Event_Mixed_Open;

# TABLE INFO
if($field=="") $field = 0;
$li = new libdbtable2007($field, $order, $pageNo);

$li->no_col = 2;
$li->IsColOff = "eSportExportArrangement";

// TABLE COLUMN
$pos = 0;
$li->column_list .= "<td width='20%' class='tablebluetop tabletoplink' align='center'>".$li->check("EventGroupID[]")."</td>\n";
$li->column_list .= "<td width='80%' class='tablebluetop tabletoplink'>".$i_Sports_Report_Item_Name."</td>\n";

### Title ###
$TAGS_OBJ[] = array($i_Sports_menu_Report_ExportRaceArrangement,"export_arrangement.php", 1);
$TAGS_OBJ[] = array($i_Sports_menu_Report_ExportRaceResult,"export_result.php", 0);
$TAGS_OBJ[] = array($i_Sports_menu_Report_ExportAthleticNumber,"export_athletic_num.php", 0);
$MODULE_OBJ = $lsports->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();
?>

<form name="form1" method="post">
<br />
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td align="center"><br />
    	<table width="96%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td colspan="2" height="28"><?=$select_event?> <?=$select_group?> <?=$select_gender?> <?=$select_day?></td>
    		</tr>
    		<tr>
            	<td colspan="2"><?=$li->display();?></td>
			</tr>
		</table>
	</td>
</tr>
<tr>
	<td>
    	<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
            <tr>
            	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
            </tr>
            <tr>
    			<td align="center">
    				<?= $linterface->GET_ACTION_BTN($button_export, "submit", "document.form1.action='arrange_export_process.php';","submit2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
    				<?= $linterface->GET_ACTION_BTN($button_export.' ('.$Lang['eSports']['AthleteNumberOnly'].')', "submit", "document.form1.action='arrange_export_process.php?athleticNumOnly=1';","submit2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
    			</td>
			</tr>
        </table>
	</td>
</tr>
</table>        

<input type="hidden" name="openGroupList" value="<?=$openGroupList?>">
</form>

<?
intranet_closedb();

$linterface->LAYOUT_STOP();
?>