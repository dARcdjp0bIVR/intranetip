<?php 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libsports.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$lsports = new libsports();
$lsports->authSportsSystem();
$li = new libdb();


$AgeGroupOrder = $lsports->returnAgeGroupOrderArr();
$AgeIdArr = $AgeGroupOrder['idArr'];
$AgeGroupArr = $AgeGroupOrder['groupArr'];

$halfSpan = sizeof($AgeGroupArr) / 2;
$column_list .= "<tr>\n";
$column_list .= "<th rowspan='2' class=''>".$Lang['Sports']['Report']['Events']."</th>\n";
$column_list .= "<th colspan='$halfSpan'>".$Lang['Sports']['Report']['Boy']."</th>";
$column_list .= "<th colspan='$halfSpan'>".$Lang['Sports']['Report']['Girl']."</th>";
$column_list .= "<th rowspan='2' class=''>".$Lang['Sports']['Report']['ClassTotal']."</th>\n";
$column_list .= "</tr>\n<tr>\n";
foreach($AgeGroupArr as $aga){
    $column_list .= "<th class=''>$aga[name]</th>\n";
}
$column_list .= "</tr>\n";

$result = $lsports->returnAllEventArrangement(true);

?>
<style>
@media print{
 *{
 -webkit-print-color-adjust: exact; 
 }
}
@page {
    size: a4;
    margin 27mm 16mm 27mm 16mm;
}
html, body{
    size: a4;
}
.print_table_title{
 font-size: 150%;
 font-weight: bold;
}
.print_table_title_tr td{
 vertical-align: bottom;
}
.print_table, .print_table td, .print_table th{
 border-style: solid;
 border-width: 2px;
 border-color: black;
 border-spacing: 0px;
 white-space: nowrap;
}
.print_table th, .print_class_total{
 background: lightgrey;
}
.print_table td{
 text-align: center;
 padding: 0;
 padding-top: 2px;
}
.print_table tr:last-child td:nth-child(1n+2){
 background: lightgrey;
}
</style>

<div class="print_main">
 <div class="print_container">
  <table width="100%" class="print_body">
   <thead>
    <th width="100%"></th>
   </thead>
   <tbody>
    <tr>
     <td>
      <table width="100%" class="print_top">
       <thead>
       	<th width="100%"></th>
       </thead>
       <tbody>
        <tr></tr>
        <tr class="print_table_title_tr">
         <td align="center" class="print_table_title"><?=$Lang['Sports']['Report']['GameArrangement']?></td>
        </tr>
       </tbody>
      </table>
     </td>
    </tr>
    <tr>
     <td>
      <table width="100%" class="print_table">
       <thead>
         <?=$column_list?>
       </thead>
       <tbody>
        <?php foreach($result as $k => $rs){?>
         <tr>
          <td><?=$rs['Title']?></td>
          <?php for($i=0;$i<sizeof($AgeGroupArr); $i++){?>
           <td><?=(($rs['AgeGroup'.$i]!=0||$k==sizeof($result)-1)?$rs['AgeGroup'.$i]:'')?></td>
          <?php }?>
          <td class="print_class_total"><?=$rs['EventTotal']?></td>
         </tr>
        <?php }?>
       </tbody>
      </table>
     </td>
    </tr>
    <tr>
     <td></td>
    </tr>
   </tbody>
  </table>
 </div>
</div>

<?php 
intranet_closedb();

?>