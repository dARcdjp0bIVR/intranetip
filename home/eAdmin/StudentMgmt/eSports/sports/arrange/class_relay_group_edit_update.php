<?php
# using:

#################################################
#
#   Date:   2019-04-11  Bill    [2018-0628-1634-01096]
#           Create File
#
#################################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libsports.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if(!$sys_custom['eSports']['KaoYipRelaySettings']) {
    echo "You have no priviledge to access this page.";
    exit();
}

$lsports = new libsports();

// [2018-0628-1634-01096] Kao Yip Class Teacher
// if($lsports->isYearClassTeacher()) {
//     // do nothing
// } else {
$lsports->authSportsSystem();

$eventGroupID = $_POST['eventGroupID'];
$groupLineAssign = $_POST['groupHeatLine'];

# Retrieve Event ID and Name
$CREventName = $lsports->retrieveClassRelayNameByCondition(" AND a.EventGroupID = '$eventGroupID' ");
list($eventID, $groupID, $eventName) = $CREventName[0];

$numberOfLane = $lsports->numberOfLanes;
$finalLineCount = $numberOfLane;

# Retrieve Event Group Info
$CREventExtInfo = $lsports->retrieveEventGroupExtInfo($eventGroupID, EVENT_TYPE_CLASSRELAY);
if($CREventExtInfo["FinalRoundReq"] == 1 && $CREventExtInfo["FinalRoundNum"] > 0) {
    $finalLineCount = $CREventExtInfo["FinalRoundNum"];
}

# Retrieve Event Class Group
$ClassGroupArr = $lsports->retrieveClassRelayClassGroup('', $groupID, '', '', $eventGroupID);
$ClassGroupIDMapArr = BuildMultiKeyAssoc($ClassGroupArr, 'ClassRelayGroupID');
$totalGroupCount = count($ClassGroupArr);

# Retrieve Event Heat Number
$heatNumber = ceil($totalGroupCount / $finalLineCount);
$heatNumber = $heatNumber? $heatNumber : 1;
// $isFirstRound = $heatNumber > 1;
// $roundType = $isFirstRound? ROUND_TYPE_FIRSTROUND : ROUND_TYPE_FINALROUND;

# Clear records of this event to SPORTS_CLASS_RELAY_LANE_ARRANGEMENT table
$sql = "DELETE FROM SPORTS_CLASS_RELAY_LANE_ARRANGEMENT WHERE EventGroupID = '$eventGroupID'";
$lsports->db_db_query($sql);

// loop Heats & Lines
$insertArr = array();
for($h=1; $h<=$heatNumber; $h++)
{
    for($i=1; $i<=$numberOfLane; $i++)
    {
        $classGroupID = $groupLineAssign[$h][$i];
        if($classGroupID != '' && isset($ClassGroupIDMapArr[$classGroupID]) && !empty($ClassGroupIDMapArr[$classGroupID])) {
            $classGroupInfo = $ClassGroupIDMapArr[$classGroupID];
            $insertArr[] = "('$eventGroupID', '".ROUND_TYPE_FIRSTROUND."', '".$classGroupInfo['YearClassID']."', '$classGroupID', '$h', '$i', NOW())";
        }
	}
}
$values = '';
if(!empty($insertArr)) {
    $values = implode(', ', (array)$insertArr);
}

# Insert lanes arrangement to SPORTS_CLASS_RELAY_LANE_ARRANGEMENT table
if($values != '') {
    $fields = "(EventGroupID, RoundType, ClassID, ClassGroupID, Heat, ArrangeOrder, DateModified)";
    
    $sql = "INSERT INTO SPORTS_CLASS_RELAY_LANE_ARRANGEMENT $fields VALUES $values";
    $lsports->db_db_query($sql);
}

intranet_closedb();
header("Location: schedule_class_relay.php?xmsg2=update");
?>