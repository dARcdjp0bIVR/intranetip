<?php
# using: Bill

####################################################
#
#	2017-02-27	Bill	[2016-0627-1013-19066]
#	- support defined Age Group for student		($sys_custom['eSports']['PuiChi_MultipleGroup'])
#
#	2016-10-12	Bill	[2016-0927-1445-23073]
#	- pass $skipOnlineEnrolChecking = 1 to allow admin to do the amendment for events with Online Enrol : false
#
#	2013-09-03	Roy
#	- POST hidden input fc, tc to update page
#
#	2013-08-06	YatWoon
#	- add restrict checking
#
#	2012-06-08	YatWoon
#	- add event quota checking
#
####################################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libsports.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");

intranet_auth();
intranet_opendb();

$lsports = new libsports();
$lsports->authSportsSystem();

# Get Student Info (Group)
# Get Event available for Enrolment
# Show Enrolment details
$names = $lsports->retrieveEventTypeName();
$txt_track = $names[0];
$txt_field = $names[1];

# Retrieve Athletic Number
$sql = "SELECT AthleticNum FROM SPORTS_STUDENT_ENROL_INFO WHERE StudentID = '$StudentID'";
$athleticNum = $lsports->returnArray($sql, 1);

# Retrieve Student Info
$lu = new libuser($StudentID);
$s_name = $lu->UserNameLang();
$s_class = $lu->ClassName;
$s_num = $lu->ClassNumber;
$gender = $lu->Gender;
$birth = $lu->DateOfBirth;

# Retrieve House Info
$house_namefield = ($intranet_session_language=="en") ? "c.EnglishName" : "c.ChineseName";
$sql = "SELECT $house_namefield, c.ColorCode FROM INTRANET_USERGROUP as a LEFT OUTER JOIN INTRANET_GROUP as b ON a.GroupID = b.GroupID, INTRANET_HOUSE as c WHERE b.GroupID = c.GroupID AND a.UserID = '$StudentID' AND b.AcademicYearID = '".Get_Current_Academic_Year_ID()."' AND b.RecordType = '4'";
$temp = $lsports->returnArray($sql, 2);
if(sizeof($temp)>0)
{
	$house_name	= $temp[0][0];
	$house_color = $temp[0][1];
}

# Student without Birthday
if($birth=="0000-00-00 00:00:00")
{
        $have_DOB = 0;
        $targetGroupID = "";
        $targetGroupName = "N/A";
}
# [2016-0627-1013-19066] Defined Age Group from enrol_update.php
else if($sys_custom['eSports']['PuiChi_MultipleGroup'] && !empty($AgeGroup) && $AgeGroup > 0)
{
		$have_DOB = 1;
	    //$birthArr = explode(" ", $birth);
	    //$birth = trim($birthArr[0]);
		$gid = $AgeGroup;	
		
	    # Retrieve Age Group Info
		$namefield = ($intranet_session_language == "b5") ? "ChineseName" : "EnglishName";
	    $sql = "SELECT $namefield, DOBLowLimit, DOBUpLimit, EnrolMaxTrack, EnrolMaxField, EnrolMaxTotal FROM SPORTS_AGE_GROUP WHERE AgeGroupID = '$gid'";
	    $groups = $lsports->returnArray($sql, 3);
	    
	    list($gname, $lowDOB, $upDOB, $maxTrack, $maxField, $maxTotal) = $groups[0];
		$targetGroupID = $gid;
		$targetGroupName = $gname;
	
	    # Retrieve Enroled EventGroupIDs
	    $sql = "SELECT ssee.EventGroupID FROM SPORTS_STUDENT_ENROL_EVENT ssee INNER JOIN SPORTS_EVENTGROUP seg ON (ssee.EventGroupID = seg.EventGroupID) WHERE ssee.StudentID = '$StudentID' AND (seg.GroupID = '$gid' OR seg.GroupID < 0);";
	    $enroledID = $lsports->returnArray($sql, 1);
	    
	    # Age Group Enrol Count Enhancement Follow Up  
	    $maxTrack = $maxTrack+0;
	    $maxField = $maxField+0;
	    $maxTotal = $maxTotal+0;
}
# Get Student related Age Group
else
{
        $have_DOB = 1;
        $birthArr = explode(" ", $birth);
        $birth = trim($birthArr[0]);

        # Retrieve Group Info
		$namefield = ($intranet_session_language == "b5") ? "ChineseName" : "EnglishName";
        $sql = "SELECT AgeGroupID, $namefield, DOBLowLimit, DOBUpLimit FROM SPORTS_AGE_GROUP WHERE Gender = '$gender'";
        $groups = $lsports->returnArray($sql,4);

        # Check which Age Group Student belongs to
        for($i=0; $i<sizeof($groups); $i++)
        {
                list($gid, $gname, $lowDOB, $upDOB) = $groups[$i];

                if($upDOB == NULL)
                {
                        if($birth <= $lowDOB)
                        {
                                $targetGroupID = $gid;
                                $targetGroupName = $gname;
                                break;
                        }
                }
                else if($lowDOB == NULL)
                {
                        if($birth >= $upDOB)
                        {
                                $targetGroupID = $gid;
                                $targetGroupName = $gname;
                                break;
                        }
                }
                else
                {
                        if(($birth <= $lowDOB) && ($birth >= $upDOB))
                        {
                                $targetGroupID = $gid;
                                $targetGroupName = $gname;
                                break;
                        }
                }
        }

        # Retrieve Enroled EventGroupIDs
        $sql = "SELECT EventGroupID FROM SPORTS_STUDENT_ENROL_EVENT WHERE StudentID = '$StudentID'";
        $enroledID = $lsports->returnArray($sql, 1);
}

# Retrieve Enrollment Rules
# 1 - Track
# 2 - Field
# modified by marcus 20090917 - Age Group Enrol Count Enhancement Follow Up
// $rules = $lsports->retrieveEnrolmentRules();
// list($maxTotal, $maxTrack, $maxField, $details) = $rules[0];
if(!$sys_custom['eSports']['PuiChi_MultipleGroup'] || empty($AgeGroup) || $AgeGroup == 0){
	$AgeGroupInfo = $lsports->returnStudentAgeGroup($StudentID);
	$maxTrack = $AgeGroupInfo['EnrolMaxTrack']+0;
	$maxField = $AgeGroupInfo['EnrolMaxField']+0;
	$maxTotal = $AgeGroupInfo['EnrolMaxTotal']+0;
}
if($maxTrack == 0)
{
	$rules = $lsports->retrieveEnrolmentRules();
	list($maxTotal, $maxTrack, $maxField, $details) = $rules[0];
}

$MODULE_OBJ['title'] = $i_Sports_Participant.($intranet_session_language=="en"?" ":"").$i_Sports_Detail;
$linterface = new interface_html("popup.html");
$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$xmsg]);
?>

<SCRIPT LANGUAGE=Javascript>
var maxTotal = "<?=$maxTotal?>";
var maxTrack = "<?=$maxTrack?>";
var maxField = "<?=$maxField?>";

var track = '<?=$txt_track?>';
var field = '<?=$txt_field?>';
</SCRIPT>

<?php
if($have_DOB==1)
{
    # Retrieve Restrict Quota Track and Field Events that can be Enrolled
    $RQ_eventgroup = $lsports->retrieveRestrictQuotaEventGroupInfo($targetGroupID, $StudentID, $skipOnlineEnrolChecking=1);

    # Retrieve Unrestrict Quota Track and Field Events that can be Enrolled
    $UQ_eventgroup = $lsports->retrieveUnrestrictQuotaEventGroupInfo($targetGroupID, $StudentID, $skipOnlineEnrolChecking=1);
}
$navigation =  $i_frontpage_separator.$i_Sports_Enrolment;

# Check if Student is allowed for enrol or not
$sql = "select Reason from SPORTS_ENROL_RESTRICTION_LIST where UserID=$StudentID";
$restrict_reason = $lsports->returnVector($sql);
$can_apply = $restrict_reason[0] ? 0 : 1; 
?>

<SCRIPT LANGUAGE=Javascript>
function checkform(obj)
{
		        var tc = countChecked(obj, "RQ_EventGroup1[]");
		        var fc = countChecked(obj, "RQ_EventGroup2[]");

                var totalCount = parseInt(tc) + parseInt(fc);

                if(tc > maxTrack)
                {
                        alert('<?=$i_Sports_Enrolment_Warn_MaxEvent?> ' + maxTrack + ' <?=$Lang['eSports']['per']?>' + track + '<?=$Lang['eSports']['events']?>');
                        return false;
                }
                else if(fc > maxField)
                {
                        alert('<?=$i_Sports_Enrolment_Warn_MaxEvent?> ' + maxField + '  <?=$Lang['eSports']['per']?>' + field + '<?=$Lang['eSports']['events']?>');
                        return false;
                }
                else if(totalCount > maxTotal)
                {
                        alert('<?=$i_Sports_Enrolment_Warn_MaxEvent?> ' + maxTotal + '  <?=$Lang['eSports']['per'].$i_Sports_field_Event_Name?>');
                        return false;
                }
                else
						document.getElementById("tc").value = tc;
						document.getElementById("fc").value = fc;
                        
                        return true;
}
</SCRIPT>

<table width="90%" border="0" cellpadding="5" cellspacing="0">
<tr> 
	<td><table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
<tr>
<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_UserStudentName?></td>
<td width="80%" valign="top" class="tabletext"><?=$s_name?></td>
</tr>
<tr>
<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_ClassName?></td>
<td valign="top" class="tabletext"><?=$s_class?></td>
</tr>
<tr>
<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_UserClassNumber?></td>
<td valign="top" class="tabletext"><?=$s_num?></td>
</tr>
<tr>
<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_Sports_field_Group?></td>
<td valign="top" class="tabletext"><?=$targetGroupName?></td>
</tr>
<tr>
<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_Sports_Participant_Number?></td>
<td valign="top" class="tabletext"><?=$athleticNum[0][0]?></td>
</tr>
<tr>
<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?="$i_Sports_Enrolment_MaxEvent ($txt_track)"?></td>
<td valign="top" class="tabletext"><?=$maxTrack?></td>
</tr>
<tr>
<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?="$i_Sports_Enrolment_MaxEvent ($txt_field)"?></td>
<td valign="top" class="tabletext"><?=$maxField?></td>
</tr>
<tr>
<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?="$i_Sports_Enrolment_MaxEvent ($i_Sports_total)"?></td>
<td valign="top" class="tabletext"><?=$maxTotal?></td>
</tr>
<tr>
<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_House?></td>
<td valign="top" class="tabletext"><?=$lsports->house_flag2($house_color, $house_name);?></td>
</tr>
</table></td>
</tr>
<tr> 
	<td height="1" class="dotline"><img src="/images/2007a/10x10.gif" width="10" height="1"></td>
</tr>
</table>

<!----------------- List //----------------->      
<form name="form1" method="post" action="detail_update.php" enctype="multipart/form-data" ONSUBMIT="return checkform(this)">
<table width="90%" border="0" cellpadding="5" cellspacing="0">
<?php
         if($have_DOB==1 && $can_apply)
         {
                 $currType = "";
                 $displayHTML = "";
                 for($i=0; $i<sizeof($RQ_eventgroup); $i++)
                 {
                        list($eg_id, $event_id, $event_name, $type_id, $type_name, $group_id, $event_quota) = $RQ_eventgroup[$i];
                        $ext_exist = $lsports->checkExtInfoExist($type_id, $eg_id);
                        if($ext_exist == 1)
                        {
                                 if($currType != $type_id)
                                 {
                                         $displayHTML .= "<tr class='tabletop'>";
                                         $displayHTML .= "<td width='25'>&nbsp;</td>";
                                         $displayHTML .= "<td><span class='tabletopnolink'>".$type_name."</span> </td>";
                                         $displayHTML .= "<td>&nbsp;</td>";
                                         $displayHTML .= "</tr>";

                                         $currType = $type_id;
                                         $typeArr[] = $type_id;
                                 }

                                $checked = "";
                                $enroled_hint = "";
                                for($j=0; $j<sizeof($enroledID); $j++)
                                {
                                         if($eg_id == $enroledID[$j][0])
                                         {
                                                 $checked = "CHECKED";
                                                 $enroled_hint = "<font color=red> ($i_Sports_Enrolled)</font>";
                                         }
                                }

                                if($group_id == '-2')
                                {
                                        $event_name = $i_Sports_Event_Girls_Open."&nbsp;".$event_name;
                                }
                                else if($group_id == '-1')
                                {
                                        $event_name = $i_Sports_Event_Boys_Open."&nbsp;".$event_name;
                                }
                                else if($group_id == '-4')
                                {
                                        $event_name = $i_Sports_Event_Mixed_Open."&nbsp;".$event_name;
                                }

                                $css = ($i%2?"":"2");
                                
                                # Check if Event is full or not
                                $cur_enroled = $lsports->returnEventEnroledNo($eg_id);
                                $enroled_hint .= ($event_quota && $cur_enroled>=$event_quota) ? " <font color=red> (". $Lang['eSports']['QuotaFull'] .")</font>" : "";
                                $this_disabled = ($event_quota && $cur_enroled>=$event_quota && !$checked) ? " disabled" : "";
                                
                                $displayHTML .= "<tr class='tablerow". ( ($i)%2 + 1 )."'>";
                                $displayHTML .= "<td><input type='checkbox' ". $this_disabled ." name='RQ_EventGroup".$type_id."[]' value='".$eg_id."' ". $checked ." id='RQ_EventGroup_". $i."'></td>";
                                $displayHTML .= "<td height='34' class='tabletext'> <label for='RQ_EventGroup_". $i ."'>".$event_name."</label> </td>";
                                $displayHTML .= "<td class='tabletext'><span class='tabletextrequire'>". $enroled_hint ."</span></td>";
                                $displayHTML .= "</tr>";
                        }
                 }
                 if(is_array($typeArr))
                        $types = implode($typeArr, ",");
                 else
                        $types = $typeArr;
				
                 if($displayHTML == "")
                 	$displayHTML .= "<tr><td align='center' colspan='3' class='tabletext'>".$i_no_record_exists_msg."</td></tr>";

       	############################## Unrestrict Quota Events ##############################
       			
                $recordExist = 0;
                $displayHTML2 = "<tr><td colspan='3' class='tabletext'>".$i_Sports_Event_UnrestrictQuota_Event."</td></tr>";
				
                for($i=0; $i<sizeof($UQ_eventgroup); $i++)
                {
                        list($eg_id, $event_name, $type_id, $group_id, $event_quota) = $UQ_eventgroup[$i];
                        $ext_exist = $lsports->checkExtInfoExist($type_id, $eg_id);
                        if($ext_exist == 1)
                        {
                                $recordExist = 1;
                                //$css = ($i%2?"":"2");

                                $checked = "";
                                $enroled_hint = "";
                                for($j=0; $j<sizeof($enroledID); $j++)
                                {
                                        if($eg_id == $enroledID[$j][0])
                                        {
                                                $checked = "CHECKED";
                                                $enroled_hint = "<font color=red> ($i_Sports_Enrolled)</font>";
                                        }
                                }

                                if($group_id == '-2')
                                {
                                        $event_name = $i_Sports_Event_Girls_Open.$event_name;
                                }
                                else if($group_id == '-1')
                                {
                                        $event_name = $i_Sports_Event_Boys_Open.$event_name;
                                }
                                else if($group_id == '-4')
                                {
                                        $event_name = $i_Sports_Event_Mixed_Open.$event_name;
                                }

								# Check if Event is full or not
                                $cur_enroled = $lsports->returnEventEnroledNo($eg_id);
                                $enroled_hint .= ($event_quota && $cur_enroled>=$event_quota) ? " <font color=red> (". $Lang['eSports']['QuotaFull'] .")</font>" : "";
                                $this_disabled = ($event_quota && $cur_enroled>=$event_quota && !$checked) ? " disabled" : "";
                                
                                $displayHTML2 .= "<tr class='tablerow". ( ($i)%2 + 1 )."'>";
                                $displayHTML2 .= "<td><input type='checkbox' ". $this_disabled ." name='UQ_EventGroup[]' value='".$eg_id."' ". $checked ." id='UQ_EventGroup_". $i ."'></td>";
                                $displayHTML2 .= "<td height='34' class='tabletext'> <label for='UQ_EventGroup_". $i ."'>".$event_name."</label> </td>";
                                $displayHTML2 .= "<td class='tabletext'><span class='tabletextrequire'>". $enroled_hint ."</span></td>";
                                $displayHTML2 .= "</tr>";
                        }
                }
				
                if($recordExist!=0)
                {
                        $displayHTML = $displayHTML.$displayHTML2;
                }
				
                echo $displayHTML;
         }
         else
         {
	         if(!$can_apply)
	         {
		         echo "<tr><td align='center' colspan='3' class='tabletext'>".$Lang['eSports']['NotAllowForEnrolment'] . $restrict_reason[0] ."</td></tr>";
	         }
	         else
	         {
                 echo "<tr><td align='center' colspan='3' class='tabletext'>".$i_Sports_No_DOB."</td></tr>";
             }
         }
?>
    </table>
    
	<table width="95%" border="0" cellpadding="5" cellspacing="0">    
    	<tr>
        	<td colspan="3">        
            	<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
                    <tr>
                    	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                    </tr>
                    <tr>
        				<td align="center">
        				<? if($have_DOB==1 && $can_apply) { ?>
        					<?= $linterface->GET_ACTION_BTN($button_save, "submit", "","submit2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
                        	<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
                   	 	<? } ?>
                        	<?= $linterface->GET_ACTION_BTN($button_close, "button", "self.close();","close_btn") ?>
        				</td>
        			</tr>
        		</table>                                
        	</td>
		</tr>
	</table>

<input type="hidden" name="types" value="<?=$types?>">
<input type="hidden" name="StudentID" value="<?=$StudentID?>">
<input type="hidden" name="AgeGroupID" value="<?=$gid?>">
<input type="hidden" id="tc" name="tc" value="" />
<input type="hidden" id="fc" name="fc" value="" />

</form>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>