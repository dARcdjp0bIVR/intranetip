<?php
# using:

#################################################
#
#   Date:   2019-04-09 Bill  [2019-0301-1144-56289]
#           Updated logic - support Class Relay Event Settings
#
#   Date:   2018-09-04 Bill  [2018-0628-1634-01096]
#           Allow Kao Yip Class Teacher to update relay ($sys_custom['eSports']['KaoYipRelaySettings'])
#
#   Date:   2018-08-10 Philips  [2018-0628-1634-01096]
#           support adding class students ($sys_custom['eSports']['KaoYipRelaySettings'])
#
#################################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libsports.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if(!$sys_custom['eSports']['KaoYipRelaySettings']) {
    echo "You have no priviledge to access this page.";
    exit();
}

$lsports = new libsports();

$isClassTeacherAccess = !$lsports->isAdminOrHelper($UserID) && $lsports->isYearClassTeacher();
// $isClassTeacherAccess = true;

// [2018-0628-1634-01096] Kao Yip Class Teacher
if($isClassTeacherAccess) {
    // do nothing
} else {
    $lsports->authSportsSystem();
}

$ageGroupID = $_POST['ageGroupID'];         // for admin usage
$YearClassID = $_POST['YearClassID'];       // for class teacher usage
$numberOfEnrol = $_POST['numberOfEnrol'];

// $ageGroupName = $lsports->retrieveAgeGroupName($ageGroupID);

// $yearClassObjAry = array();
// $yearClassGroupTitleAry = array();
foreach((array)$ClassID as $eventGroupID => $eventClassGroupArr)
{
    $classGroupCountArr = array();
    foreach((array)$eventClassGroupArr as $classGroupID => $yearClassID) {
        if(!isset($classGroupCountArr[$yearClassID])) {
            $classGroupCountArr[$yearClassID] = 0;
        }
        $classGroupCountArr[$yearClassID]++;
    }
    
    $classGroupIndexArr = array();
    foreach((array)$eventClassGroupArr as $classGroupID => $yearClassID)
    {
        //$gid = $_POST['GroupID'.$i];
    	//$cid = $_POST['ClassID'.$i];
    	//$gtype = $_POST['GroupType'.$i];
        $isClassGroupExist = $classGroupID > 0 && !(strpos($classGroupID, 'row') !== false);
        
        $isValidClassGroupData = false;
        if($yearClassID != "")	      # Insert / Update Record 
        {
            # Class Group Student
            $selectedStudentIDArr = $_POST['Student_Selected'][$eventGroupID][$classGroupID];
            if(!empty($selectedStudentIDArr))
            {
                $isValidClassGroupData = true;
                
                /* 
                if(!isset($yearClassObjAry[$yearClassID])) {
                    $yearClassObjAry[$yearClassID] = new year_class($yearClassID);
                }
                $yearClass = $yearClassObjAry[$yearClassID];
                $yearClassName = $yearClass->ClassTitleB5;
                
                $title = $yearClassName.$gtype;
                $yearClassGroupTitleAry[$title] += 0;
                $yearClassGroupTitleAry[$title]++;
                
                $title .= ' (第'.$yearClassGroupTitleAry[$title].'隊)';
                 */
                
                # Class Group Title
                $title = '';
                if($classGroupCountArr[$yearClassID] > 1) {
                    if(!isset($classGroupIndexArr[$yearClassID])) {
                        $classGroupIndexArr[$yearClassID] = 0;
                    }
                    $classGroupIndexArr[$yearClassID]++;
                    
                    $title .= '第'.$classGroupIndexArr[$yearClassID].'隊';
                }
        	    
        		# Check whether record exist
                $sql = "SELECT ClassRelayGroupID FROM SPORTS_CLASS_RELAY_CLASS_GROUP WHERE ClassRelayGroupID = '$classGroupID'";
        		$temp = $lsports->returnVector($sql);
        		if($isClassGroupExist && $temp[0] != "")		# Update Record
        		{
        			$sql = "UPDATE SPORTS_CLASS_RELAY_CLASS_GROUP 
        						SET YearClassID = '$yearClassID', GroupTitle = '$title', DateModified = NOW(), ModifiedBy = '$UserID'
    						WHERE
                                ClassRelayGroupID = '$classGroupID'";
        			$lsports->db_db_query($sql);
        		}
        		else		# Add Record
        		{
        		    $thisAgeGroupID = '';
        		    if($isClassTeacherAccess) {
        		        $thisAgeGroupID = $_POST['AgeGroupID'][$eventGroupID][$classGroupID];
        		    }
        		    else {
        		        $thisAgeGroupID = $ageGroupID;
        		    }
        		    
        			$sql = "INSERT INTO SPORTS_CLASS_RELAY_CLASS_GROUP 
        						(YearClassID, AgeGroupID, EventGroupID, GroupTitle, DateInput, InputBy, DateModified, ModifiedBy)
    						VALUES
                                ('$yearClassID', '$thisAgeGroupID', '$eventGroupID', '$title', NOW(), '$UserID', NOW(), '$UserID')";
        			$lsports->db_db_query($sql);
        			$classGroupID = $lsports->db_insert_id();
        		}
        		
        		# Update Student in Class Group
        		$lsports->updateClassRelayGroupStudent($classGroupID, $eventGroupID, $selectedStudentIDArr);
            }
        }
        
        if($isClassGroupExist && !$isValidClassGroupData)	      # Delete Record 
    	{
    	    $sql = "SELECT ClassRelayGroupID FROM SPORTS_CLASS_RELAY_CLASS_GROUP WHERE ClassRelayGroupID = '$classGroupID'";
    	    $temp = $lsports->returnVector($sql);
    	    if($temp[0] != "")
    	    {
    	        $sql = "DELETE FROM SPORTS_CLASS_RELAY_CLASS_GROUP WHERE ClassRelayGroupID = '$classGroupID'";
        		$lsports->db_db_query($sql);
        		
        		$lsports->updateClassRelayGroupStudent($classGroupID, $eventGroupID, '');
    	    }
    	}
    }
}
intranet_closedb();

$parms = $isClassTeacherAccess? "YearClassID=$YearClassID" : "ageGroupID=$ageGroupID";
header("Location: class_relay_enrol.php?$parms&xmsg2=update");
?>