<?php
# editing by:
/***************************************************************************
*	modification log:
*	2013-09-03
*	- bug fix: add insert record to table SPORTS_STUDENT_ENROL_INFO
*
***************************************************************************/
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libsports.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$lsports = new libsports();
$lsports->authSportsSystem();

$linterface 	= new interface_html();

### get Import Data
for($i=0;$i<count($ImportRawData);$i++)
{
	$ImportData[$i]	= explode(":_Separator_:",$ImportData[$i]);
	$RawData[$i] 		= explode(":_Separator_:",$ImportRawData[$i]);
}
$TargetClass	= explode(":_Separator_:",$ImportTargetClasses);

## select all student of target Class / Form
$TargetClassStr = count($TargetClass)>1?implode("','",$TargetClass):array_shift($TargetClass);
$sql = "SELECT UserID FROM INTRANET_USER WHERE ClassName IN ('$TargetClassStr')";
$TargetClassStudentList = $lsports->returnVector($sql);

## delete all enrolment record of target class student
$TargetClassStudentListStr = count($TargetClassStudentList)>1?implode(",",$TargetClassStudentList):array_shift($TargetClassStudentList);
$sql = "DELETE FROM SPORTS_STUDENT_ENROL_EVENT WHERE StudentID IN ($TargetClassStudentListStr)";
$lsports->db_db_query($sql) or die ($sql.": ".mysql_error());

## count events
$trackCount = 0;
$fieldCount = 0;
## insert import data
foreach($ImportData as $key => $Record)
{
	list($StudentID,$EventGroupID)=$Record;
	list($ClassName,$ClassNumber,$EventCode)=$RawData[$key];
	$eventGroupInfoArr = $lsports->Get_EventGroup_Info('', $EventGroupID);
	$eventID = $eventGroupInfoArr[0]['EventID'];
	$eventInfoArr = $lsports->Get_Event_Info($eventID);
	$eventType = $eventInfoArr[0]['EventType'];
	if ($eventType == 1) $trackCount++;
	if ($eventType == 2) $fieldCount++;
	
	$sql = "	INSERT INTO 
					SPORTS_STUDENT_ENROL_EVENT 
					(
						`StudentID`,
						`EventGroupID`,
						`DateModified`
					)
				VALUES
					(
						'$StudentID',
						'$EventGroupID',
						NOW()
					)";
					
	$SuccessArr[] = $lsports->db_db_query($sql);
	
	#Check Record Exist Or Not
	$sql = "SELECT COUNT(*) FROM SPORTS_STUDENT_ENROL_INFO WHERE StudentID = '$StudentID'";
	$exist_info = $lsports->returnVector($sql);

	if($exist_info[0] == 0)
	{
		#Insert New Record To SPORTS_STUDENT_ENROL_INFO
		$fields1 = "(StudentID, TrackEnrolCount, FieldEnrolCount, DateModified)";
		$values1 = "(".$StudentID.", ".$trackCount.", ".$fieldCount.", now())";
		$sql = "INSERT INTO SPORTS_STUDENT_ENROL_INFO $fields1 VALUES $values1";
		$SuccessArr[] = $lsports->db_db_query($sql);
	}
	else
	{
		#Update Existing Record in SPORTS_STUDENT_ENROL_INFO
		$sql = "UPDATE SPORTS_STUDENT_ENROL_INFO SET TrackEnrolCount = '$trackCount', FieldEnrolCount = '$fieldCount', DateModified = now() WHERE StudentID = '$StudentID'";
		$SuccessArr[] = $lsports->db_db_query($sql);
	}

		
	#Build Table Content
	$rowcss = " class='".(($key)%2==0? "tablebluerow2":"tablebluerow1")."' ";
	$insertStatus = !in_array(0, $SuccessArr)?$i_con_msg_import_success:$i_con_msg_import_failed2;
	$failcss = !in_array(0, $SuccessArr)?"":" class='red' ";
	
	$Confirmtable .= "	<tr $rowcss>";
	$Confirmtable .= "		<td class='tabletext'>".($key+1)."</td>";
	$Confirmtable .= "		<td class='tabletext'>$ClassName</td>";
	$Confirmtable .= "		<td class='tabletext'>$ClassNumber</td>";
	$Confirmtable .= "		<td class='tabletext'>$EventCode</td>";
	$Confirmtable .= "		<td $failcss>$insertStatus</td>"; 
	$Confirmtable .= "	</tr>";	
}

# BackBtn
$BackBtn = $linterface->GET_ACTION_BTN($button_back, "button", "window.location='enrol_update.php'","back"," class='formbutton'  onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"");

### Title ###
$TitleTitle1 = "<span class='contenttitle'>". $i_Sports_menu_Arrangement_EnrolmentUpdate ."</span>";
$TitleTitle = "<table width='100%' height='25' cellpadding='0' cellspacing='0' ><tr><td style=\"vertical-align: bottom;\" >".$TitleTitle1."</td></tr></table>";
$CurrentPage	= "PageArrangement_EnrolmentUpdate";

$TAGS_OBJ[] = array($TitleTitle, "", 0);    
$STEPS_OBJ[] = array($i_general_select_csv_file, 0);
$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 0);
$STEPS_OBJ[] = array($i_general_imported_result, 1);
$MODULE_OBJ = $lsports->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();
?>

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
	<tr>
		<td colspan="2"><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
	</tr>
	<tr>
		<td clospan="2" align="center">
			<table width="95%" border="0" cellpadding="5" cellspacing="0">
			<tr>
				<td class='tablebluetop tabletopnolink'>#</td>
				<td class='tablebluetop tabletopnolink'><?=$Lang['eSports']['csv']['ClassTitle']?></td>
				<td class='tablebluetop tabletopnolink'><?=$Lang['eSports']['csv']['ClassNumber']?></td>
				<td class='tablebluetop tabletopnolink'><?=$Lang['eSports']['csv']['EventCode']?></td>
				<td class='tablebluetop tabletopnolink'><?=$Lang['eSports']['ImportStatus']?></td>
			</tr>
			<?=$Confirmtable?>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="2" align="center"><?=$BackBtn?></td>
	</tr>
</table>


<?
$linterface->LAYOUT_STOP();
?>
