<?php
# using:

###########################################
#
#	Date:	2019-04-17	Bill     [2018-1019-1043-03066]
#			Create File
#
###########################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libsports.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

function convertToChineseNumber($number)
{
    if($number <= 0) {
        return '';
    }
    
    $digits_arr = array('一', '二', '三', '四', '五', '六', '七', '八', '九');
    $tens = '十';
    
    $str = '';
    if($number >= 10) {
        $tens_digit = floor($number / 10);
        if($tens_digit > 1) {
            $str .= $digits_arr[($tens_digit - 1)];
        }
        $str .= $tens;
    }
    
    $units_digit = $number % 10;
    $str .= $digits_arr[($units_digit - 1)];
    
    return $str;
}

$lsports = new libsports();
$lsports->authSportsSystem();

$lexport = new libexporttext();

$current_year_id = Get_Current_Academic_Year_ID();

# Retrieve all age group ids and names
$age_groups = $lsports->retrieveAgeGroupIDNames();

# Add Boys / Girls / Mixed Open
$openBoy[0] = '-1';
$openBoy[1] = $i_Sports_Event_Boys_Open;
$openGirl[0] = '-2';
$openGirl[1] = $i_Sports_Event_Girls_Open;
$openMixed[0] = '-4';
$openMixed[1] = $i_Sports_Event_Mixed_Open;
$age_groups[] = $openBoy;
$age_groups[] = $openGirl;
$age_groups[] = $openMixed;

# Retrieve all round types
//$round_types = array(ROUND_TYPE_FIRSTROUND, ROUND_TYPE_SECONDROUND, ROUND_TYPE_FINALROUND);
$round_types = array(ROUND_TYPE_FIRSTROUND);

# Retrieve all event types
$type_ids = array(EVENT_TYPE_TRACK, EVENT_TYPE_FIELD);

$utf_content = array();
$student_info_ary = array();

# CSV Header
$export_header[] = array("組別", "賽事名稱", "學生號碼布編碼", "班級", "姓名", "道次", "組號 ");

# Retrieve all track and field event group ids and names
$events = $lsports->retrieveTrackFieldEventName('b5');
for($i=0; $i<sizeof($events); $i++)
{
    # Retrieve event info
	list($event_id, $event_name) = $events[$i];
	
	// loop age group
	for($j=0; $j<sizeof($age_groups); $j++)
	{
	    # Retrieve age group info
		list($group_id, $group_name) = $age_groups[$j];
	    $group_info = $lsports->retrieveAgeGroupDetail($group_id);
	    $group_code = $group_info['GroupCode'];
	    
	    // exclude Open Event first
	    if($group_id < 0) {
	        continue;
	    }
	    
		# Retrieve event group info
		$event_group_info = $lsports->retrieveEventGroupID($event_id, $group_id, $type_ids);
		if(!empty($event_group_info))
		{
			$event_group_id = $event_group_info[0];
// 			$EventGroupInfoDetails = $lsports->retrieveEventGroupDetail($event_group_id);
			$total_heat_num = $lsports->returnHeatNumberByEventGroupID($event_group_id);
			
			# Retrieve student enrolment
			$enroled_students = $lsports->retrieveEnroledStudentList($event_group_id);
			$enroled_students = BuildMultiKeyAssoc((array)$enroled_students, 'UserID');
			
			// loop round type
			foreach((array)$round_types as $round_type)
			{
				# Retrieve line arrangement
				$line_arrange = $lsports->retrieveTFLaneArrangeDetail($event_group_id, $round_type);
				if(!empty($line_arrange))
				{
					foreach((array)$line_arrange as $k => $result)
					{
					    $student_id = $result['StudentID'];
// 						$house = $lsports->retrieveStudentHouseInfo($result['StudentID']);
                        
					    $order = $result['ArrangeOrder'];
					    $heatNumber = convertToChineseNumber($result['Heat']);
					    
					    # Retrieve student info
					    if(!isset($student_info_ary[$student_id]))
					    {
    					    $lu = new libuser($student_id);
    					    $student_name_ch = $lu->ChineseName;
    					    
    					    $student_info = $lu->getStudentInfoByAcademicYear($current_year_id);
    					    $class_name_ch = $student_info[0]['ClassNameCh'];
    					    
    					    # Retireve student athlete number
    					    $athlete_num = $lsports->returnStudentAthleticNum($student_id);
					        
    					    # Store student into
    					    $student_info_ary[$student_id] = array($student_name_ch, $class_name_ch, $athlete_num);
					    }
					    list($student_name_ch, $class_name_ch, $athlete_num) = $student_info_ary[$student_id];
					    
    				    $this_row = array();
    				    $this_row[] = "$group_code";
    				    $this_row[] = "$event_name";
    				    $this_row[] = "$athlete_num";
    				    $this_row[] = "$class_name_ch";
    				    $this_row[] = "$student_name_ch";
    				    $this_row[] = "$order";
    				    //$this_row[] = $total_heat_num > 1? '第'.convertToChineseNumber($result['Heat']).'組' : '';
    				    $this_row[] = "第"."$heatNumber"."組";
						$utf_content[] = $this_row;
						
						unset($enroled_students[$student_id]);
					}
				}
			}
			
			if(!empty($enroled_students))
			{
			    foreach((array)$enroled_students as $student_id => $result)
			    {
			        # Retrieve student info
			        if(!isset($student_info_ary[$student_id]))
			        {
			            $lu = new libuser($student_id);
			            $student_name_ch = $lu->ChineseName;
			            
			            $student_info = $lu->getStudentInfoByAcademicYear($current_year_id);
			            $class_name_ch = $student_info[0]['ClassNameCh'];
			            
			            # Retireve student athlete number
			            $athlete_num = $lsports->returnStudentAthleticNum($student_id);
			            
			            # Store student into
			            $student_info_ary[$student_id] = array($student_name_ch, $class_name_ch, $athlete_num);
			        }
			        list($student_name_ch, $class_name_ch, $athlete_num) = $student_info_ary[$student_id];
			        
			        $this_row = array();
			        $this_row[] = "$group_code";
			        $this_row[] = "$event_name";
			        $this_row[] = "$athlete_num";
			        $this_row[] = "$class_name_ch";
			        $this_row[] = "$student_name_ch";
			        $this_row[] = "";
			        $this_row[] = "";
			        $utf_content[] = $this_row;
			    }
			}
		}
	}
}

$filename = "athlete_export.csv";
$export_text = $lexport->GET_EXPORT_TXT($utf_content,$export_header);
$lexport->EXPORT_FILE($filename,$export_text); 

intranet_closedb();
?>