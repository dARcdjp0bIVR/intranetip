<?php
# using:

#################################################
#
#	Date:	2020-03-20 Philips  [2020-0117-1205-31207]
#			Enlarge Student selection box
#
#   Date:   2019-04-09 Bill  [2019-0301-1144-56289]
#           Updated logic - support Class Relay Event Settings
#
#   Date:   2018-09-21 Bill  [2018-0628-1634-01096]
#           Create file
#
#################################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libsports.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if(!$sys_custom['eSports']['KaoYipRelaySettings']) {
    echo "You have no priviledge to access this page.";
    exit();
}

$linterface = new interface_html();
$CurrentPage = "PageArrangement_ClassRelayEnrol";

$lsports = new libsports();
$lclass = new libclass();

$isClassTeacherAccess = !$lsports->isAdminOrHelper($UserID) && $lsports->isYearClassTeacher();
// $isClassTeacherAccess = true;

// [2018-0628-1634-01096] Kao Yip Class Teacher
// if($lsports->isYearClassTeacher()) {
if($isClassTeacherAccess) {
    // do nothing
} else {
    $lsports->authSportsSystem();
}

# Get Teaching Class & Students
// if(!$lsports->isAdminOrHelper($UserID)) {
if($isClassTeacherAccess)
{
    $yearClassIDArr = $lsports->teachingYearClassList();
    if(empty($yearClassIDArr)) {
        $yearClassIDArr[] = -1;
    }
}

# Get Age Groups
$ageGroupIDAry = $lsports->retrieveAgeGroupIDNames();
$ageGroupIDAry[] = array('-1', $i_Sports_Event_Boys_Open);;
$ageGroupIDAry[] = array('-2', $i_Sports_Event_Girls_Open);
$ageGroupIDAry[] = array('-4', $i_Sports_Event_Mixed_Open);
$ageGroupMappingAry = build_assoc_array($ageGroupIDAry);

$cond = '';
$order_by = '';
if($isClassTeacherAccess) {
    $YearClassID = isset($_POST['YearClassID'])? $_POST['YearClassID'] : $_GET['YearClassID'];
    if($YearClassID == '' || !in_array($YearClassID, (array)$yearClassIDArr)) {
        $YearClassID = $yearClassIDArr[0];
    }
    
    $order_by = '';
}
else {
    $ageGroupID = isset($_POST['ageGroupID'])? $_POST['ageGroupID'] : $_GET['ageGroupID'];
    $ageGroupID = $ageGroupID? $ageGroupID : -1;
    
    $cond = "AND a.GroupID = '$ageGroupID' ";
}

/* 
# Get Relay Event Type
$relayEventTypeAry = $lsports->retrieveClassRelayType();
$relayEventTypeAry = Get_Array_By_Key($relayEventTypeAry, 'RelayType');
*/

# Get Class Relay Event
$name_field = $intranet_session_language=="en"? "EnglishName" : "ChineseName";
$sql = "SELECT
            a.EventGroupID,
            b.EventID,
            b.$name_field as event_name,
            a.GroupID
        FROM
            SPORTS_EVENTGROUP as a
            INNER JOIN SPORTS_EVENT as b ON a.EventID = b.EventID
        WHERE
            b.EventType = '".EVENT_TYPE_CLASSRELAY."' 
            $cond 
        $order_by ";
$relayEventAry = $lsports->returnArray($sql);

# General Class Selection
if($isClassTeacherAccess) {
    $select_class_tag = "name='YearClassID' id='YearClassID' onchange='document.form1.submit();'";
    $select_class = $lclass->getSelectClassID($select_class_tag, $YearClassID, 0, '', '', $yearClassIDArr);
}
# General Age Group Selection
else {
    $select_age_group = getSelectByArray($ageGroupIDAry, " id='ageGroupID' name='ageGroupID' onchange='document.form1.submit();' ", $ageGroupID, 0, 1);
}

// $numberOfEnrol = 0;

/* 
// loop Event Type
foreach($relayEventTypeAry as $thisRelayEvent)
{
    # Existing Class Group
    //$thisRelayClassGroup = $lsports->retrieveClassRelayClassGroup('', $ageGroupID, $yearClassIDArr, $thisRelayEvent);
    $thisRelayClassGroup = $lsports->retrieveClassRelayClassGroup('', $ageGroupID, $yearClassIDArr, '', $thisRelayEvent);
    $relayClassGroupCount = count((array)$thisRelayClassGroup);
    
    // loop group list
    $thisDisplaySize = $relayClassGroupCount > 3? $relayClassGroupCount : 3;
    for($j=0; $j<$thisDisplaySize; $j++) {
        $classGroupID = '';
        $classGroupTitle = $thisRelayEvent;
        $yearClassID = '';
        $classStudentArr[$i] = array();
        
        $thisClassGroup = $thisRelayClassGroup[$j];
        if(isset($thisClassGroup)) {
            $classGroupID = $thisClassGroup['ClassRelayGroupID'];
            $classGroupTitle = $thisClassGroup['GroupTitle'];
            $yearClassID = $thisClassGroup['YearClassID'];
            
            $arrangedStudentArr = $lsports->retrieveClassRelayGroupStudent($classGroupID);
            foreach($arrangedStudentArr as $stud) {
                $classStudentArr[$i][] = $stud['UserID'];
            }
        }
        
        # Class Group Title
        $classGroupTitleInput = "<input type='text' id='GroupTitle$i' name='GroupTitle$i' value='$classGroupTitle' style='width:60%' disabled>";
        $classGroupTitleInput .= "<input type='hidden' id='GroupType$i' name='GroupType$i' value='$thisRelayEvent'>";
        
        # Class Group ID Field
        $classGroupIDField = "<input type='hidden' id='GroupID$i' name='GroupID$i' value='$classGroupID'>";
        
    	# Class Selection
        $select_class = $lclass->getSelectClassID("name='ClassID$i' class='class_select' onchange='get_class_stud(\"$i\", \"\")'", $yearClassID, 1, '', '', $yearClassIDArr);
        
        # Student Selection Box
    	$student_select = "<select class='stud_select' name='Student_Select$i' id='Student_Select$i' multiple='yes'></select>";
        $push = "<button type='button' onclick='stud_push(\"$i\")'>>></button>";
        $pop = "<button type='button' onclick='stud_pop(\"$i\")'><<</button>";
    	$student_selected = "<select class='stud_select' name='Student_Selected".$i."[]' id='Student_Selected$i' multiple='yes'></select>";
    	
    	$student_select_div_1 = $student_select.
                              "<div class='push_pop' style='display: inline'>".$push."</div>";
    	$student_select_div_2 = "<div class='push_pop' style='display: inline'>".$pop."</div>".
    	                      $student_selected.
                              "<div style='display:none;' class='warnMsgDiv' id='warnDiv$i'>
                                  <span class='tabletextrequire'>".$Lang['eSports']['Sports']['ClassRelay']['EnrolLimitAlert']."!</span>
                              </div>
                              <div style='display:none;' class='warnMsgDiv' id='warnStuDiv$i'>
                                  <span class='tabletextrequire'>".$Lang['eSports']['Sports']['ClassRelay']['StudentAlreadyInAnotherLine']."!</span>
                              </div>";
    	
    	$css = $i % 2? "" : "2";
    	$form_content .= "<tr>";
        	$form_content .= "<td valign='top' nowrap='nowrap' class='formfieldtitle'>".$classGroupTitleInput."</td>";
        	$form_content .= "<td valign='top' class='tableContent$css'>".$select_class."</td>";
    	   $form_content .= "<td class='tableContent$css'>".$student_select_div_1."</td>";
    	   $form_content .= "<td class='tableContent$css'>".$student_select_div_2."</td>";
    	$form_content .= "</tr>";
    	
    	$i++;
    	$numberOfEnrol++;
    }
}
*/

# Get Class Student group by Age Group
$classStudentGroupIDArr = array();
if($isClassTeacherAccess) {
    if($YearClassID != '' && $YearClassID > 0) {
        foreach($ageGroupMappingAry as $thisAgeGroupID => $thisAgeGroupName) {
            $classStudentGroupIDArr[$thisAgeGroupID] = $lsports->retrieveClassStudent($YearClassID, $thisAgeGroupID);
        }
    }
}

$form_content = "";
$eventGroupIDHiddleInput = "";

$i = 0;
$numberOfEventGroup = 0;
$classStudentArr = array();

# 2020-03-20 (Philips) - Default student selection box size
$selectSize = 'size="16"';

// loop Class Relay Event
foreach((array)$relayEventAry as $thisRelayEvent)
{
    $thisEventGroupID = $thisRelayEvent['EventGroupID'];
    $thisEventGroupName = $thisRelayEvent['event_name'];
    $thisEventAgeGroupID = $thisRelayEvent['GroupID'];
    $thisEventAgeGroupName = $ageGroupMappingAry[$thisEventAgeGroupID];
    
    # Get Class Group
    //$thisRelayClassGroup = $lsports->retrieveClassRelayClassGroup('', $ageGroupID, $yearClassIDArr, $thisRelayEvent);
    $thisRelayClassGroup = $lsports->retrieveClassRelayClassGroup('', $ageGroupID, ($YearClassID? $YearClassID : $yearClassIDArr), '', $thisEventGroupID);
    $relayClassGroupCount = count((array)$thisRelayClassGroup);
    
    // loop Class Group
    $thisDisplaySize = $relayClassGroupCount? $relayClassGroupCount : 1;
    for($j=0; $j<$thisDisplaySize; $j++)
    {
        if($j > 0) {
            $thisEventGroupName = '&nbsp;';
        }
        
        $classGroupID = 'row'.$j;
        $classGroupTitle = '';
        $yearClassID = '';
        $classStudentArr[$thisEventGroupID][$classGroupID] = array();
        
        # Get Class Group Students
        $thisClassGroup = $thisRelayClassGroup[$j];
        if(isset($thisClassGroup)) {
            $classGroupID = $thisClassGroup['ClassRelayGroupID'];
            $classGroupTitle = $thisClassGroup['GroupTitle'];
            $yearClassID = $thisClassGroup['YearClassID'];
            
            $arrangedStudentArr = $lsports->retrieveClassRelayGroupStudent($classGroupID, $thisEventGroupID);
            $classStudentArr[$thisEventGroupID][$classGroupID] = Get_Array_By_Key((array)$arrangedStudentArr, 'UserID');
        }
        $row_id = $thisEventGroupID.'_'.$classGroupID;
        $row_name = '['.$thisEventGroupID.']['.$classGroupID.']';
        
        # Class Group Title
        $classGroupTitleInput = '';
        if($classGroupTitle != '') {
            $classGroupTitleInput = "<input type='text' value='$classGroupTitle' style='width:60%' disabled>";
        }
        // $classGroupTitleInput .= "<input type='hidden' id='GroupType$i' name='GroupType$i' value='$thisRelayEvent'>";
        
        # Class Group ID Field
        // $classGroupIDField = "<input type='hidden' id='GroupID$i' name='GroupID$i' value='$classGroupID'>";
        
        if($isClassTeacherAccess)
        {
            // skip if no class student available in this age group
            if($thisEventAgeGroupID && empty($classStudentGroupIDArr[$thisEventAgeGroupID])) {
                continue;
            }
            
            # Age Group Title
            $ageGroupTitle = $thisEventAgeGroupName;
            
            # Age Group ID Field
            $ageGroupIDField = "<input type='hidden' id='AgeGroupID_".$row_id."' name='AgeGroupID".$row_name."' value='$thisEventAgeGroupID'>";
            
            # Class ID Field
            $classIDField = "<input type='hidden' id='ClassID_".$row_id."' name='ClassID".$row_name."' value='$YearClassID'>";
            
            # Options in Student Selection
            $defaultOptions = '';
            $selectedOptions = '';
            foreach((array)$classStudentGroupIDArr[$thisEventAgeGroupID] as $thisClassStudent) {
                $thisClassStudentID = $thisClassStudent['UserID'];
                $thisStudentSelectOption = '<option sort="'.$thisClassStudent['ClassNumber'].'" value="'.$thisClassStudentID.'">'.$thisClassStudent['sname'].' ('.$thisClassStudent['ClassNumber'].')</option>'."\n";
                
                if(!in_array($thisClassStudentID, (array)$classStudentArr[$thisEventGroupID][$classGroupID])) {
                    $defaultOptions .= $thisStudentSelectOption;
                } else {
                    $selectedOptions .= $thisStudentSelectOption;
                }
            }
            
            # Student Selection
            $student_select = "<select class='stud_select' name='Student_Select".$row_name."' id='Student_Select_".$row_id."' multiple='yes' $selectSize>".$defaultOptions."</select>";
            $push = "<button type='button' onclick='stud_push(\"$row_id\")'>>></button>";
            $pop = "<button type='button' onclick='stud_pop(\"$row_id\")'><<</button>";
            $student_selected = "<select class='stud_select' name='Student_Selected".$row_name."[]' id='Student_Selected_".$row_id."' multiple='yes' $selectSize>".$selectedOptions."</select>";
            
            $student_select_div_1 = $student_select.
                                    "<div class='push_pop' style='display: inline'>".$push."</div>";
            $student_select_div_2 = "<div class='push_pop' style='display: inline'>".$pop."</div>".
                                    $student_selected.
                                    "<div style='display:none;' class='warnMsgDiv' id='warnDiv_".$row_id."'>
                                        <span class='tabletextrequire'>".$Lang['eSports']['Sports']['ClassRelay']['EnrolLimitAlert']."!</span>
                                    </div>
                                    <div style='display:none;' class='warnMsgDiv' id='warnClassDiv_".$row_id."'>
                                        <span class='tabletextrequire'>".$Lang['eSports']['Sports']['ClassRelay']['ClassIsAlreadySelected']."!</span>
                                    </div>
                                    <div style='display:none;' class='warnMsgDiv' id='warnStuDiv_".$row_id."'>
                                        <span class='tabletextrequire'>".$Lang['eSports']['Sports']['ClassRelay']['StudentAlreadyInAnotherLine']."!</span>
                                    </div>";
            
            $css = $i % 2? "" : "2";
            $form_content .= "<tr id='group_row_".$row_id."'>";
                $form_content .= "<td valign='top' nowrap='nowrap' class='formfieldtitle tableContent$css'>".$thisEventGroupName."</td>";
                $form_content .= "<td valign='top' class='formfieldtitle tableContent$css'>".$ageGroupTitle."<br/>".$ageGroupIDField."<br/>".$classIDField."</td>";
                $form_content .= "<td class='formfieldtitle tableContent$css'>".$student_select_div_1."</td>";
                $form_content .= "<td class='formfieldtitle tableContent$css'>".$student_select_div_2."</td>";
            $form_content .= "</tr>";
        }
        else
        {
            # Class Selection
            //$select_class = $lclass->getSelectClassID("name='ClassID$i' class='class_select' onchange='get_class_stud(\"$i\", \"\")'", $yearClassID, 1, '', '', $yearClassIDArr);
            $select_class_tag = "name='ClassID".$row_name."' id='ClassID_".$row_id."' class='class_select' onchange='get_class_stud(\"$row_id\", \"$row_name\", \"\")'";
            $select_class = $lclass->getSelectClassID($select_class_tag, $yearClassID, 1, '', '', $yearClassIDArr);
            
            # Student Selection
            $student_select = "<select class='stud_select' name='Student_Select".$row_name."' id='Student_Select_".$row_id."' multiple='yes' $selectSize></select>";
            $push = "<button type='button' onclick='stud_push(\"$row_id\")'>>></button>";
            $pop = "<button type='button' onclick='stud_pop(\"$row_id\")'><<</button>";
            $student_selected = "<select class='stud_select' name='Student_Selected".$row_name."[]' id='Student_Selected_".$row_id."' multiple='yes' $selectSize></select>";
            
            $student_select_div_1 = $student_select.
                                    "<div class='push_pop' style='display: inline'>".$push."</div>";
            $student_select_div_2 = "<div class='push_pop' style='display: inline'>".$pop."</div>".
                                    $student_selected.
                                    "<div style='display:none;' class='warnMsgDiv' id='warnDiv_".$row_id."'>
                                        <span class='tabletextrequire'>".$Lang['eSports']['Sports']['ClassRelay']['EnrolLimitAlert']."!</span>
                                    </div>
                                    <div style='display:none;' class='warnMsgDiv' id='warnClassDiv_".$row_id."'>
                                        <span class='tabletextrequire'>".$Lang['eSports']['Sports']['ClassRelay']['ClassIsAlreadySelected']."!</span>
                                    </div>
                                    <div style='display:none;' class='warnMsgDiv' id='warnStuDiv_".$row_id."'>
                                        <span class='tabletextrequire'>".$Lang['eSports']['Sports']['ClassRelay']['StudentAlreadyInAnotherLine']."!</span>
                                    </div>";
            
            $css = $i % 2? "" : "2";
            $form_content .= "<tr id='group_row_".$row_id."'>";
                $form_content .= "<td valign='top' nowrap='nowrap' class='tableContent$css'>".$thisEventGroupName."</td>";
                $form_content .= "<td valign='top' class='tableContent$css'>".$select_class."<br/>".$classGroupTitleInput."</td>";
                $form_content .= "<td class='tableContent$css'>".$student_select_div_1."</td>";
                $form_content .= "<td class='tableContent$css'>".$student_select_div_2."</td>";
                $form_content .= "<td class='tableContent$css' valign='top' style='text-align: right'>";
                if($j > 0) {
                    $form_content .= "<span class='table_row_tool row_content_tool'>";
                        $form_content .= "<a onclick='delete_class_group(this, \"$thisEventGroupID\", \"$classGroupID\")' title='delete' class='delete' href='javascript:void(0);'></a>";
                    $form_content .= "</span>";
                } else {
                    $form_content .= "&nbsp;";
                }
                $form_content.= "</td>";
            $form_content .= "</tr>";
        }
        
        $i++;
        // $numberOfEnrol++;
    }
    
    if($isClassTeacherAccess)
    {
        // do nothing
    }
    else
    {
        // Add class group row button
        $addButton = $linterface->GET_ACTION_LNK("javascript:void(0);", "", $ParOnClick="add_class_group('".$thisEventGroupID."')", $ParClass="add");
        
        // Add class group row
        $css = $i % 2? "" : "2";
        $form_content.= "<tr id='group_row_".$thisEventGroupID."_add'>";
            $form_content .= "<td valign='top' nowrap='nowrap' class='formfieldtitle tableContent$css'>&nbsp;</td>";
            $form_content .= "<td class='formfieldtitle tableContent$css' colspan='3'>&nbsp;</td>";
            $form_content .= "<td class='formfieldtitle tableContent$css'>".$addButton."</td>";
        $form_content .= "</tr>";
    }
    
    # Event Group ID Field
    $numberOfEventGroup++;
    $eventGroupIDHiddleInput .= "<input type='hidden' name='eventGroupID[$numberOfEventGroup]' value='$thisEventGroupID'>";
    
    $i++;
}

### Title ###
$TAGS_OBJ[] = array($Lang['eSports']['ClassRelayEnrol']);
$MODULE_OBJ = $lsports->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();
?>

<br />
<form name="form1" action="class_relay_enrol.php" method="post">
<table id="html_body_frame" width="97%" border="0" cellspacing="0" cellpadding="5" align="center">
<tr>
	<td>
		<?php if($isClassTeacherAccess) { ?>
			<span class="tabletext"><?=	$Lang["eSports"]["Class"]?>:</span> <?=$select_class?>
		<?php } else { ?>
			<span class="tabletext"><?=	$i_Sports_menu_Settings_AgeGroup?>:</span> <?=$select_age_group?>
		<?php } ?>
		<br/>
		<br/>
		
        <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
            <tr><td>
        			<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
            			<tr>
            				<td class='tabletop tabletoplink' width='28%'><?=$Lang['eSports']['Event']?></td>
            				
            				<?php if($isClassTeacherAccess) { ?>
            					<td class='tabletop tabletoplink' width='20%'><?=$i_Sports_menu_Settings_AgeGroup?></td>
            				<?php } else { ?>
            					<td class='tabletop tabletoplink' width='20%'><?=$Lang["eSports"]["Class"]?></td>
            				<?php } ?>
            				
            				<td class='tabletop tabletoplink' width='22%'><?=$Lang['eSports']['SelectStudent']?></td>
            				<td class='tabletop tabletoplink' width='22%'><?=$Lang['eSports']['SelectedStudent']?></td>
            				
            				<?php if($isClassTeacherAccess) {
            				    // do nothing
            				} else { ?>
            					<td class='tabletop tabletoplink' width='8%'>&nbsp;</td>
            				<?php } ?>
            			</tr>
                        <?=$form_content?>
        			</table>
    		</td></tr>
        </table>
	</td>
</tr>
<tr>
	<td>
        <table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
            <tr>
            	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
            </tr>
            <tr>
				<td align="center">
					<?= $linterface->GET_ACTION_BTN($button_submit, "button", "handle_submit()", "submit2") ?>
                    <?= $linterface->GET_ACTION_BTN($button_reset, "reset", "", "reset2") ?>
				</td>
			</tr>
        </table>
	</td>
</tr>

<?= $eventGroupIDHiddleInput ?>
</table>
<br />

</form>

<script type="text/javascript">
function handle_submit()
{
	$('.warnMsgDiv').hide();

	// loop event groups
	var valid = true;
	var stud_arr = [];
	for(var i=1; i<=<?=$numberOfEventGroup?>; i++)
	{
		var event_group_id = $('input[name="eventGroupID[' + i + ']"]').val();
		if(event_group_id)
		{
			// loop event group rows
			var class_arr = [];
			$.each($("select[name^='Student_Selected[" + event_group_id + "]']" ), function(index, select_obj)
			{
	    		var row_id = select_obj.id.replace("Student_Selected_", "");
				
	    		<?php if($isClassTeacherAccess) { ?>
    	    		// check if selected 4 - 6 students 
            		var stud_options = $('select#Student_Selected_' + row_id + ' option');
        			if(stud_options.length > 0 && (stud_options.length < 4 || stud_options.length > 6)) {
        				valid = false;
        				$('#warnDiv_' + row_id).show();
        			}
    				else {
    	    			// check if selected students already in another class group
        				for(var j=0; j<stud_options.length; j++) {
            				var stud_option_val = stud_options[j].value;
            				if(stud_option_val != '') {
            					if(stud_arr.indexOf(stud_option_val) != -1) {
            						valid = false;
            	    				$('#warnStuDiv_' + row_id).show();
            					}
            				}
            				stud_arr.push(stud_option_val);
        				}
        			}
        			
    				$('select#Student_Selected_' + row_id + ' option').attr('selected', 'selected');
	    		<?php } else { ?>
    	    		var class_id = $('select#ClassID_' + row_id).val();
    	    		if(class_id != '')
    	    		{
    	    			// check if current class already selected
    					if(class_arr.indexOf(class_id) != -1) {
    						valid = false;
    	    				$('#warnClassDiv_' + row_id).show();
    					}
    					class_arr.push(class_id);
    					
    		    		// check if selected 4 - 6 students 
    	        		var stud_options = $('select#Student_Selected_' + row_id + ' option');
    	    			if(stud_options.length < 4 || stud_options.length > 6) {
    	    				valid = false;
    	    				$('#warnDiv_' + row_id).show();
    	    			}
    					else {
    		    			// check if selected students already in another class group
    	    				for(var j=0; j<stud_options.length; j++) {
    	        				var stud_option_val = stud_options[j].value;
    	        				if(stud_option_val != '') {
    	        					if(stud_arr.indexOf(stud_option_val) != -1) {
    	        						valid = false;
    	        	    				$('#warnStuDiv_' + row_id).show();
    	        					}
    	        				}
    	        				stud_arr.push(stud_option_val);
    	    				}
    	    			}
    	    			
    					$('select#Student_Selected_' + row_id + ' option').attr('selected', 'selected');
    	    		}
	    		<?php } ?>
            });
		}
	}
	
	if(valid) {
		document.form1.action = 'class_relay_enrol_update.php';
		document.form1.submit();
	} else {
		return false;
    }
}

<?php if(!$isClassTeacherAccess) { ?>
function get_class_stud(row_id, row_name, ori_arr)
{
	var stud_select = $('select#Student_Select_' + row_id);
	var stud_selected = $('select#Student_Selected_' + row_id);
	
	var class_id = $('select#ClassID_' + row_id).val();
	if(class_id != '') {
    	$.ajax({
    		url: 'ajax_ageGroupClass.php',
    		method: 'post',
    		data: {
    			"ageGroupID": "<?=$ageGroupID?>",
    			"classID": class_id
    		},
    		success: function(res){
    			stud_select.empty().append(res);
    			stud_selected.empty();
    			
    			// load students in class group when reload page
    			if(ori_arr) {
					stud_opts = stud_init(stud_select, ori_arr);
					for(var x in stud_opts) {
						if(!isNaN(x)) {
							stud_opts[x].remove().appendTo(stud_selected);
						}
					}
    			}
    		}
    	});
	} else {
		stud_select.empty();
		stud_selected.empty();
	}
}

function stud_init(stud_select, data_arr)
{
	var opts_arr = [];
	for(var x in data_arr) {
		stud_select.children().each(function() {
			if($(this).val() == data_arr[x]) {
				opts_arr.push($(this));
			}
		});
	}
	
	return opts_arr;
}
<?php } ?>

function stud_push(row_id)
{
	$('select#Student_Select_' + row_id + ' option:selected').remove().appendTo($('select#Student_Selected_' + row_id));

	// sort students by class number
	var stud_arr = [];
	$('select#Student_Selected_' + row_id + ' option').each(function() {
		stud_arr[$(this).attr('sort') - 1] = $(this);
	});
	$('select#Student_Selected_' + row_id).empty();
	
	for(var x in stud_arr) {
		$('select#Student_Selected_' + row_id).append(stud_arr[x]);
	}
}

function stud_pop(row_id)
{
	$('select#Student_Selected_' + row_id + ' option:selected').remove().appendTo($('select#Student_Select_' + row_id));

	// sort students by class number
	var stud_arr = [];
	$('select#Student_Select_' + row_id + ' option').each(function() {
		stud_arr[$(this).attr('sort') - 1] = $(this);
	});
	$('select#Student_Select_' + row_id).empty();
	
	for(var x in stud_arr) {
		$('select#Student_Select_' + row_id).append(stud_arr[x]);
	}
}

<?php
// load students in class group
if($isClassTeacherAccess)
{
    // no need to load students
}
else
{
    $script_content = "";
    foreach((array)$classStudentArr as $thisEventGroupID => $classEventGroupStudentArr) {
        foreach((array)$classEventGroupStudentArr as $thisClassGroupID => $classGroupStudentArr) {
            if(!empty($classGroupStudentArr)) {
                $row_id = $thisEventGroupID.'_'.$thisClassGroupID;
                $row_name = '['.$thisEventGroupID.']['.$thisClassGroupID.']';
                
                $script_content .= "get_class_stud(\"$row_id\", \"$row_name\", [" .implode(',', (array)$classGroupStudentArr). "]);\n\r";
            } else {
                //$script_content .= "get_class_stud($i, \"\");";
            }
        }
    }
    
    echo $script_content;
}
?>

<?php if(!$isClassTeacherAccess) { ?>
function add_class_group(event_group_id)
{
	if(event_group_id <= 0 || event_group_id == '') {
		return false;
	}
	var group_row_prefix = "group_row_" + event_group_id + "_";
	
	// get student existing rows
	var group_row_count = 1;
	var target_group_rows = $("tr[id^='" + group_row_prefix + "']");
	if(target_group_rows) {
		group_row_count = target_group_rows.length - 1;
	}
	while ($("tr#" + group_row_prefix + "row" + group_row_count)[0]) {
		group_row_count++;
	}
	
	// build new row content
	$.ajax({
		type: "POST",
		url: "ajax_reload.php", 
		data:{ 
			"event_group_id": event_group_id,
			"group_row": "row" + group_row_count
		},
		success: function(group_row_content){
			// Append new row Content
			if(group_row_content != '')
			{
				$("tr#" + group_row_prefix + "add").before(group_row_content);
			}
		}
	});
}

function delete_class_group(obj, event_group_id, class_group_id)
{
	if(event_group_id <= 0 || event_group_id == '' || class_group_id <= 0 || class_group_id == '') {
		return false;
	}
	var target_group_row = 'tr#group_row_' + event_group_id + '_' + class_group_id;
	
	// check group row type
	var is_temp_row = class_group_id.indexOf('row');
	is_temp_row = is_temp_row != -1;
	
	// JS Handling for temp added row
	if(is_temp_row)
	{
		// display delete message
		obj.parentElement.innerHTML = '<span class="tabletextrequire"><?=$i_con_gen_msg_delete?></span>';
		
// 		// disable event group row
// 		$(target_group_row).find('select,button').each(function() {
// 			if($(this)) {
// 				$(this)[0].disabled = true;
// 			}
// 	    });
		
		// remove event group row
		$(target_group_row).remove();
		
		// Update all input fields > for copy and paste
		// rebuildInputScore();
	}
	// AJAX Handling for existing class group in DB
	else
	{
		$.ajax({
			type: "POST",
			url: "ajax_remove_class_group.php", 
			data:{ 
				"event_group_id": event_group_id,
				"class_group_id": class_group_id
			},
			success: function(result){
				if(result)
				{
					// display delete message
					obj.parentElement.innerHTML = '<span class="tabletextrequire"><?=$i_con_gen_msg_delete?></span>';
					
// 					// disable event group row
// 					$(target_group_row).find('select,button').each(function() {
// 						if($(this)) {
// 							$(this)[0].disabled = true;
// 						}
// 				    });
					
					// remove event group row
					$(target_group_row).remove();
					
					// Update all input fields > for copy and paste
					// rebuildInputScore();
				}
			}
		});
	}
}
<?php } ?>
</script>

<style>
    body{
        min-width: 1080px;
    }
    select.class_select{
        vertical-align: top;
    }
    select.stud_select{
        width: 180px;
        padding: 3px;
    }
    .push_pop{
        vertical-align: top;
        display: inline-block;
        width: 35px;
    }
    .push_pop button{
        display: inline;
    }
</style>

<?
intranet_closedb();

print $linterface->FOCUS_ON_LOAD("form1.ClassID1");
$linterface->LAYOUT_STOP();
?>