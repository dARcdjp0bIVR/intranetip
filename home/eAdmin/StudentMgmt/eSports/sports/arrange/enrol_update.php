<?php
// Using: Bill
/*
 * 2016-06-13 (Cara)  : updated css style for data table
 */
 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libsports.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable{$LAYOUT_SKIN}.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$CurrentPage = "PageArrangement_EnrolmentUpdate";

$lsports = new libsports();
$lclass = new libclass();
$lsports->authSportsSystem();

# Create House Selection List
$houses = $lsports->retrieveHouseSelectionInfo();
$select_house = getSelectByArray($houses, "name=house", "$house", 0, 0);

# Create Age Group Selection List
$age_groups = $lsports->retrieveAgeGroupIDNames();
$select_age_group = getSelectByArray($age_groups, "name=ageGroup", "$ageGroup", 0, 0);

# Create Class Selection List
// $sql = "SELECT ClassID, ClassName FROM INTRANET_CLASS WHERE RecordStatus = 1 ORDER BY ClassID";
// $classes = $lsports->returnArray($sql, 2);
$select_class = $lclass->getSelectClass("name=theClass", $theClass, 0, 0); 

# Import Button
$ImportBtn 	= "<a href=\"student_import.php\" class='contenttool'><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_import.gif\" width=\"18\" height=\"18\" border=\"0\" align=\"absmiddle\" /> " . $button_import . "</a>";

# Export Button 
if($sys_custom['eSports_student_info_export'] === true)
	$ExportBtn 	= $linterface->GET_ACTION_BTN($button_export, "button","click_export();"); 

# Title
$TitleTitle1 = "<span class='contenttitle'>". $i_Sports_menu_Arrangement_EnrolmentUpdate ."</span>";
$TitleTitle = "<table width='100%' height='25' cellpadding='0' cellspacing='0' ><tr><td style=\"vertical-align: bottom;\" >".$TitleTitle1."</td></tr></table>";

# Tag
$TAGS_OBJ[] = array($TitleTitle, "", 0);
$MODULE_OBJ = $lsports->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();
?>

<SCRIPT LANGUAGE=Javascript>
function checkform(obj)
{
	obj.action="enrol_update.php";
	if(obj.engName.value == "" && obj.chiName.value == "" && obj.house.value == "" && obj.ageGroup.value == "" && obj.theClass.value == "")
	{
		alert('<?=$i_Sports_Warn_Please_Select?>');
		return false;
	}
	else
		return true;
}

function click_export()
{
	var obj = document.form1;
	obj.action="enrol_export.php";
	obj.submit();
}
</SCRIPT>

<br />
<form name="form1" method="post" action="enrol_update.php" onSubmit="return checkform(this);">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td align='right' colspan="2"><?=$linterface->GET_SYS_MSG("",$xmsg);?></td>
</tr>
<tr>
	<td colspan="2">
            <table width="96%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr> 
                	<td><?= $ImportBtn ?></td>
                </tr>
                <tr>
                	<td><br />
				<table class="form_table_v30">
				<tr>
					<td class="field_title"><span class="tabletext"><?="$i_UserName ($i_general_english)"?></span></td>
					<td><input name="engName" type="text" class="textboxtext" maxlength="255" value="<?=$engName?>"/></td>
				</tr>
                <tr>
					<td class="field_title"><span class="tabletext"><?="$i_UserName ($i_general_chinese)"?></span></td>
					<td><input name="chiName" type="text" class="textboxtext" maxlength="255" value="<?=$chiName?>"/></td>
				</tr>
                <tr>
					<td class="field_title"><span class="tabletext"><?=$i_ClassName?> </span></td>
					<td><?=$select_class?></td>
				</tr>
                <tr>
					<td class="field_title"><span class="tabletext"><?=$i_Sports_field_Group?> </span></td>
					<td><?=$select_age_group?></td>
				</tr>
                <tr>
					<td class="field_title"><span class="tabletext"><?=$i_House?> </span></td>
					<td><?=$select_house?></td>
				</tr>
				</table>
					</td>
        		</tr>
        	</table>
	</td>
</tr>
<tr>
	<td colspan="2">        
                <table width="96%" border="0" cellspacing="0" cellpadding="5" align="center">
                <tr>
                	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
                <tr>
					<td align="center">
						<?= $linterface->GET_ACTION_BTN($button_search, "submit") ?>
						<?= $ExportBtn?>
					</td>
				</tr>
                </table>                                
	</td>
</tr>
</table>                        

<input type="hidden" name="submitFlag" value="1">
</form>

<? if($submitFlag == 1) 
{ 
		# Retrieve Searching Result by Passing the Searching Values
		$results = $lsports->searchEnrolmentRecords($engName, $chiName, $theClass, $ageGroup, $house);
        
        # TABLE INFO
        if($field=="") $field = 0;
        $li = new libdbtable2007($field, $order, $pageNo);
        $li->IsColOff = "eSportEnrolUpdateList";
        
        // TABLE COLUMN
        $pos = 0;
        $li->column_list .= "<td class='tablebluetop tabletoplink'>$i_UserName ($i_general_english)</td>\n";
        $li->column_list .= "<td class='tablebluetop tabletoplink'>$i_UserName ($i_general_chinese)</td>\n";
        $li->column_list .= "<td class='tablebluetop tabletoplink'>$i_ClassName</td>\n";
        $li->column_list .= "<td class='tablebluetop tabletoplink'>$i_Sports_field_Group</td>\n";
        $li->column_list .= "<td class='tablebluetop tabletoplink'>$i_House</td>\n";
        $li->column_list .= "<td class='tablebluetop tabletoplink'>$i_Sports_Participant_Number</td>\n";
        $li->column_list .= "<td class='tablebluetop tabletoplink'>&nbsp;</td>\n";
?>

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td colspan="2">        
                <table width="96%" border="0" cellspacing="0" cellpadding="5" align="center">
                <tr>
					<td><?=$li->display();?></td>
				</tr>
                </table>
	</td>
</tr>
</table>
<? } ?>

<?php
intranet_closedb();
print $linterface->FOCUS_ON_LOAD("form1.engName");
$linterface->LAYOUT_STOP();
?>