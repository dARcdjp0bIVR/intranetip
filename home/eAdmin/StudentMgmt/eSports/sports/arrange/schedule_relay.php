<?php
# using: Bill

############################################
#
#   Date:   2019-04-23  Bill    [2019-0312-0932-01073]      ($sys_custom['eSports']['HouseTeamNameDisplay'])
#           Add team name if one house has more than one team
#
############################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libsports.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

function convertToChineseNumber($number)
{
    global $Lang;
    
    if($number <= 0) {
        return '';
    }
    
    /*
     $tens = '十';
     
     $str = '';
     if($number > 10) {
     $tens_digit = floor($number / 10);
     if($tens_digit > 1) {
     $str .= $digits_arr[($tens_digit - 1)];
     }
     $str .= $tens;
     }
     */
    $units_digit = $number % 10;
    $str = $Lang['eSports']['RelayTeamNumber'][$units_digit];
    
    return $str;
}

$linterface = new interface_html();
$CurrentPage = "PageArrangement_Schedule";

$lsports = new libsports();
$lsports->authSportsSystem();

$HREvents = $lsports->retrieveHouseRelayName();			# Retrieve house relay eventgroup ids and names

$house_relay_name = $lsports->retrieveEventTypeNameByID(3);
$class_relay_name = $lsports->retrieveEventTypeNameByID(4);
$numberOfLane = $lsports->numberOfLanes;

### Number of Lane for House relay
$lane_number = "<tr>";
$lane_number .= "<td width='80'>&nbsp;</td>";
for($i=1; $i<=$numberOfLane; $i++) {
	$lane_number .= "<td class='tabletop tabletoplink' align='center'>".$i_Sports_The.$i.$i_Sports_Line."</td>";
}
$lane_number .= "<td class='tabletop tabletoplink'>&nbsp;</td>";
$lane_number .= "</tr>";

### House relay conrent
$house_content = "";
for($j=0; $j<sizeof($HREvents); $j++)
{
	list($event_id, $group_id, $event_name) = $HREvents[$j];
	$typeIDs = 3;
    
	if(true || $group_id > 0)
	{
		$group_name = $lsports->retrieveAgeGroupName($group_id);	
		$eventGroupInfo= $lsports->retrieveEventGroupID($event_id, $group_id, $typeIDs);
		$eventGroupID = $eventGroupInfo[0];
        
		$house_arrange = $lsports->retrieveHouseRelayLaneArrangement($eventGroupID);
        
		# Replace the space in the string
		$tempName = str_replace(" ", ",", $event_name);
 		$temp_group_name = str_replace("'", "\'", $group_name);
		
//		$edit_link = "house_relay_edit.php?eventGroupID=".$eventGroupID."&groupName=".$temp_group_name."&eventName=".$tempName;
		$edit_link = "house_relay_edit.php?eventGroupID=".urlencode($eventGroupID)."&groupName=".urlencode($temp_group_name)."&eventName=".urlencode($tempName);
		
		$house_content .= "<tr class='tablerow". ($j%2?"2":"1") ."'>";
		$house_content .= "<td class='tablelist'>(".$group_name.") ".$event_name."</td>";
		
		if(sizeof($house_arrange) != 0)
		{
		    // [2019-0312-0932-01073]
		    if($sys_custom['eSports']['HouseTeamNameDisplay'])
		    {
		        $houseArrangedArr = array();
		        $houseLineArrangeArr = array();
		        for($k=0; $k<sizeof($house_arrange); $k++)
		        {
		            list($house_id, $order) = $house_arrange[$k];
		            $HouseInfo = $lsports->getHouseDetail($house_id);
		            $hname = ($intranet_session_language=="en"? $HouseInfo[1] : $HouseInfo[2]);
		            
	                if(empty($houseArrangedArr[$hname])) {
	                    $houseArrangedArr[$hname] = 0;
	                }
	                $houseArrangedArr[$hname]++;
	                $houseLineArrangeArr[$k] = $houseArrangedArr[$hname];
		        }
		    }
		    
			$hr_display = "";
			for($m=1; $m<=$numberOfLane; $m++)
			{
				$flag = 0;
				for($k=0; $k<sizeof($house_arrange); $k++)
				{
					list($house_id, $order) = $house_arrange[$k];
					if($order == $m)
					{
						$HouseInfo = $lsports->getHouseDetail($house_id);
						$h_color = $HouseInfo[4];
						$h_name = ($intranet_session_language=="en"? $HouseInfo[1] : $HouseInfo[2]);
						
						// [2019-0312-0932-01073]
						if($sys_custom['eSports']['HouseTeamNameDisplay'] && $houseArrangedArr[$h_name] > 1) {
						    $h_name .= convertToChineseNumber($houseLineArrangeArr[$k]);
						}
						
                        $hr_display .= "<td align='center'>".$lsports->house_flag2($h_color, $h_name) ."</td>";
						$flag = 1;
						break;
					}
				}
                
				if($flag != 1) {
					$hr_display .= "<td>&nbsp;</td>";
				}
			}
			$house_content .= $hr_display;
		}
		else 
		{
			$house_content .= "<td colspan=".$numberOfLane.">&nbsp;</td>";
		}
        
		$house_content .= "<td align='center'>". $linterface->GET_BTN($button_edit, "button", "window.location='". addslashes($edit_link) ."'","submit2") ."</td>";
		$house_content .= "</tr>";
	}
}

### Title ###
$TAGS_OBJ[] = array($i_Sports_menu_Settings_CommonTrackFieldEvent,"schedule.php", 0);
$TAGS_OBJ[] = array($house_relay_name,"schedule_relay.php", 1);
$TAGS_OBJ[] = array($class_relay_name,"schedule_class_relay.php", 0);
$MODULE_OBJ = $lsports->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();
?>

<!------------- Start of the House Relay Table --------------------------------------//-->
<br />
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td>&nbsp;</td>
    <td align="right"><?=$linterface->GET_SYS_MSG($xmsg2);?></td>
</tr>
<tr>
	<td align="center" colspan="2">
    	<table width="96%" border="0" cellspacing="0" cellpadding="0">                             
		<tr>
        	<td>
                <table width="100%" border="0" cellspacing="0" cellpadding="4">
                    <?=$lane_number?>
                    <?=$house_content?>
            	</table>
            </td>
		</tr>
        <tr>
			<td height="1" class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
		</tr>
		</table>
	</td>
</tr>
</table>

<br />

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>