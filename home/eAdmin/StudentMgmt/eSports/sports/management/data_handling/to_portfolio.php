<?php
// Using :

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libsports.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$lsports = new libsports();

if(!$lsports->isAdminUser($_SESSION['UserID']) || !$sys_custom['eSports']['SyncResultToPortfolio']) {
    No_Access_Right_Pop_Up();
    die();
}

# Menu highlight setting
$CurrentPage = "PageManagement_SyncResultToPortfolio";

# Title
$TAGS_OBJ[] = array($Lang['Sports']['Management']['SyncResultToPortfolio']);
$MODULE_OBJ = $lsports->GET_MODULE_OBJ_ARR();

# Get Academic Year
$sql = "Select AcademicYearID FROM ACADEMIC_YEAR_TERM WHERE TermStart <= CURDATE()";
$AcademicYearTermIDArr = $lsports->returnVector($sql);
$sql = "SELECT AcademicYearID, ".Get_Lang_Selection('YearNameB5', 'YearNameEN')." as yearName FROM ACADEMIC_YEAR WHERE AcademicYearID IN ('".implode("', '", (array)$AcademicYearTermIDArr)."') ORDER BY Sequence";
$AcademicYearArr = $lsports->returnArray($sql);

# Year Selection
$YearSelection = getSelectByArray($AcademicYearArr, ' name="targetYearID" id="targetYearID"', Get_Current_Academic_Year_ID(), 0, 1);

# Get Last Transfer Info
$sql = "SELECT 
            sync_log.DateInput, ".getNameFieldByLang("iu.")." as SyncUserName
        FROM 
            {$intranet_db}.SPORTS_SYNC_DATA_LOG sync_log 
            INNER JOIN {$intranet_db}.INTRANET_USER iu ON (sync_log.InputBy = iu.UserID)
        WHERE 
            sync_log.IsOldRecord = 0
        ORDER BY
            sync_log.DateInput DESC ";
$LastTransferInfo = $lsports->returnArray($sql);

# Start layout
$linterface->LAYOUT_START();

# Button Row
$ButtonHTML = $linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "button", "checkForm()", "btnSubmit", '' , 0, '');

# System Message
$SysMsgHTML = "";
if (isset($Result) && $Result != "") {
    $SysMsgHTML = $linterface->GET_SYS_MSG("", $eDiscipline["WebSAMS_Warning_".$Result]);
}
?>

<script>
function checkForm()
{
    if(confirm(globalAlertMsgTransferToWebSAMS)) {
        $('form#form1').attr('action', 'update.php').submit();
    }
}
</script>

<form id="form1" name="form1" method="post">
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td><table width="98%" border="0" cellpadding="5" cellspacing="0" align="center">
                    <tr>
                        <td class="formfieldtitle" align="left" width="30%"><?=$Lang['SysMgr']['SubjectClassMapping']['CopyToAcademicYear']?></td>
                        <td class="tabletext"><?=$YearSelection?></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <br />
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td><table width="98%" border="0" cellpadding="5" cellspacing="0" align="center">
                    <? if(!empty($LastTransferInfo)) { ?>
                        <tr>
                            <td class="tabletextremark">
                                <?=$Lang['Sports']['LastTransferDateTime'] . " : ". $LastTransferInfo[0][0] ?></p></td>
                            </td>
                        </tr>
                    <? } ?>
                    <tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
                    <tr>
                        <td align="center"><?=$ButtonHTML?></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <br />
</form>

<?
$linterface->LAYOUT_STOP();

intranet_closedb();
?>