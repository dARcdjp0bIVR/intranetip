<?php

//using:marcus

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libsports.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libaccessright.php");


intranet_auth();
intranet_opendb();

$linterface 	= new interface_html();
$lsports	= new libsports();
$laccessright = new libaccessright();

if (!$plugin['Sports'] or $_SESSION['intranet_sports_right']!=1)
{
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
}

### Title ###
$MODULE_OBJ = $lsports->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();

?>
<br />
<table align="center" width="90%" border="0" cellpadding="5" cellspacing="0" >
	<tr valign="top">
		<td class="tabletext" align="center"><?=$i_Sports_Select_Menu?> </td>
	</tr>
</table>
<br />
<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>

