<?php
// Using:
/*
 * 2019-04-30 (Bill) : Clear new db tables for Class Relay  [2019-0301-1144-56289]
 *
 */

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libsports.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$CurrentPage = "PageAnnualSettings";

$lsports = new libsports();
$lsports->authSportsSystem();

### Title ###
$TAGS_OBJ[] = array($i_Sports_menu_Settings_AgeGroup,"agegroup.php", 0);
$TAGS_OBJ[] = array($i_Sports_menu_Settings_ParticipantGenerate,"participant_gen.php", 0);
$TAGS_OBJ[] = array($i_Sports_menu_Settings_Enrolment,"enrolment.php", 0);
$TAGS_OBJ[] = array($i_Sports_menu_Settings_YearEndClearing,"record_clear.php", 1);
$MODULE_OBJ = $lsports->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();
?>

<?
if($clear=="1")
{
	$success = false;
	
	# Clear all enrollment records
	$sqlDelLaneArrangement = " DELETE FROM SPORTS_LANE_ARRANGEMENT WHERE 1";
	$sqlDelStudentEnrolEvent = " DELETE FROM SPORTS_STUDENT_ENROL_EVENT WHERE 1";
	$sqlDelStudentEnrolInfo = " DELETE FROM SPORTS_STUDENT_ENROL_INFO WHERE 1";
	$sqlDelRelayLaneArrangement = " DELETE FROM SPORTS_HOUSE_RELAY_LANE_ARRANGEMENT WHERE 1";
	$sqlDelClassRelayLaneArrangement = " DELETE FROM SPORTS_CLASS_RELAY_LANE_ARRANGEMENT WHERE 1";
	$sqlDelClassRelayClassGroup = " DELETE FROM SPORTS_CLASS_RELAY_CLASS_GROUP WHERE 1";
	$sqlDelClassRelayLaneArrangementMapping = " DELETE FROM SPORTS_CLASS_RELAY_LANE_ARRANGEMENT_MAPPING WHERE 1";
	
	$lsports->db_db_query($sqlDelLaneArrangement);
	$lsports->db_db_query($sqlDelStudentEnrolEvent);
	$lsports->db_db_query($sqlDelStudentEnrolInfo);
	$lsports->db_db_query($sqlDelRelayLaneArrangement);
	$lsports->db_db_query($sqlDelClassRelayLaneArrangement);
	$lsports->db_db_query($sqlDelClassRelayClassGroup);
	$lsports->db_db_query($sqlDelClassRelayLaneArrangementMapping);
	
	# Update Age Group Year
	$sqlAgeGroup = "UPDATE SPORTS_AGE_GROUP SET DOBLowLimit=DOBLowLimit+00010000, DOBUpLimit=DOBUpLimit+00010000";
	$lsports->db_db_query($sqlAgeGroup);
	
	# Update record holders
	$year = date(Y);
	$sqlField = "UPDATE SPORTS_EVENTGROUP_EXT_FIELD SET RecordHolderName=NewRecordHolderName,RecordMetre=NewRecordMetre,";
	$sqlField .= " RecordYear='$year', RecordHouseID=NewRecordHouseID, NewRecordHolderUserID=NULL,NewRecordHolderName=NULL,";
	$sqlField .= " NewRecordHouseID=NULL,NewRecordMetre=NULL WHERE ";
	
	$sqlSelectField = "SELECT EventGroupID FROM SPORTS_EVENTGROUP_EXT_FIELD WHERE NewRecordHolderUserID IS NOT NULL ORDER BY EventGroupID";
	$resultField = $lsports->returnArray($sqlSelectField,1);
	for($i=0; $i<sizeof($resultField); $i++) {
		$sqlField .= " EventGroupID='".$resultField[$i][0]."'";
		if($i<sizeof($resultField)-1) {
			$sqlField.=" OR ";
		}
	}
    
	$sqlTrack = "UPDATE SPORTS_EVENTGROUP_EXT_TRACK SET RecordHolderName=NewRecordHolderName,RecordMin=NewRecordMin,";
	$sqlTrack .= " RecordSec=NewRecordSec,RecordMs=NewRecordMs,RecordYear='$year',";
	$sqlTrack .= " RecordHouseID=NewRecordHouseID, NewRecordHolderUserID=NULL , NewRecordHolderName=NULL ,";
	$sqlTrack .= " NewRecordMin=NULL , NewRecordSec=NULL ,NewRecordMs=NULL, NewRecordHouseID=NULL WHERE ";
	
	$sqlSelectTrack = "SELECT EventGroupID FROM SPORTS_EVENTGROUP_EXT_TRACK WHERE NewRecordHolderUserID IS NOT NULL ORDER BY EventGroupID";
	$resultTrack = $lsports->returnArray($sqlSelectTrack,1);
	for($i=0; $i<sizeof($resultTrack); $i++) {
		$sqlTrack .= " EventGroupID='".$resultTrack[$i][0]."'";
		if($i<sizeof($resultTrack)-1) {
			$sqlTrack .= " OR ";
		}
	}
	
	$sqlRelay = "UPDATE SPORTS_EVENTGROUP_EXT_RELAY SET RecordHolderName=NewRecordHolderName,RecordMin=NewRecordMin,";
	$sqlRelay .= " RecordSec=NewRecordSec,RecordMs=NewRecordMs,RecordYear='$year',";
	$sqlRelay .= " RecordHouseID=NewRecordHouseID, NewRecordHolderName=NULL , NewRecordMin=NULL ,NewRecordSec=NULL,";
	$sqlRelay .= " NewRecordMs=NULL, NewRecordHouseID=NULL WHERE ";
	
	$sqlSelectRelay = "SELECT EventGroupID FROM SPORTS_EVENTGROUP_EXT_RELAY WHERE NewRecordHouseID IS NOT NULL ORDER BY EventGroupID";
	$resultRelay = $lsports->returnArray($sqlSelectRelay,1);
	for($i=0; $i<sizeof($resultRelay); $i++) {
		$sqlRelay .= " EventGroupID='".$resultRelay[$i][0]."'";
		if($i<sizeof($resultRelay)-1) {
			$sqlRelay.=" OR ";
		}
	}
	
	$lsports->db_db_query($sqlField);
	$lsports->db_db_query($sqlTrack);
	$lsports->db_db_query($sqlRelay);

	# Update SPORTS_SYNC_DATA_LOG
    $sqlSyncLog = "UPDATE SPORTS_SYNC_DATA_LOG SET IsOldRecord = 1";
    $lsports->db_db_query($sqlSyncLog);

    $success = true;
    $xmsg = $success ? $i_Sports_YearEndClearing_Msg2:"";
}
?>

<script language="javascript">
function confirmation(formObj){
	if(confirm('<?=$i_Sports_YearEndClearing_Msg1?>. <?=$i_Sports_YearEndClearing_Continue?>?')==1){
		formObj.clear.value = 1;
		formObj.submit();
	}
	else{
		formObj.clear.value='';
	}
}
</script>

<br />   
<form name="form1" method="post" action="record_clear.php" >
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td>&nbsp;</td>
	<td align='right'><?=$linterface->GET_SYS_MSG("",$xmsg);?></td>
</tr>
<tr>
	<td colspan="2">
        <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
            <tr>
            	<td align="center"><?=$linterface->GET_SYS_MSG("",$i_Sports_YearEndClearing_Msg3, "50%");?></td>
            </tr>
        </table>
	</td>
</tr>
<tr>
	<td colspan="2">        
        <table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
            <tr>
            	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
            </tr>
            <tr>
    			<td align="center">
    				<?= $linterface->GET_ACTION_BTN($i_Sports_YearEndClearing_Clear, "submit", "confirmation(this.form);","submit2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
    			</td>
    		</tr>
        </table>
	</td>
</tr>
</table>
<br />

<input type="hidden" name="clear" value="">
</form>

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>