<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libsports.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$linterface 	= new interface_html();
$CurrentPage	= "PageGeneralSettings";

# Get info
$lsports = new libsports();
$lsports->authSportsSystem();
$ID = (is_array($ID)? $ID[0]:$ID);

$score_standard_details = $lsports->retrieveScoreStandardDetail($ID);

list ($scoreName, $pos1, $pos2, $pos3, $pos4, $pos5, $pos6, $pos7, $pos8, $recordBroken, $qualified) = $score_standard_details;
for ($i=1; $i<=8; $i++) {
     ${"select_score_$i"} = "<SELECT name=\"rank$i\">\n";
}
$select_broken = "<SELECT name=\"recordBroken\">\n";
$select_qualify = "<SELECT name=\"qualify\">\n";

$max_score = max($pos1,$pos2,$pos3,$pos4,$pos5,$pos6,$pos7,$pos8,$recordBroken,$qualified,25)+5;
for ($i=0; $i<$max_score; $i+=0.5)
{
     for ($j=1; $j<=8; $j++)
     {
          ${"select_score_$j"} .= "<OPTION value='".$i."' ".($i==${"pos$j"}?"SELECTED":"").">$i</OPTION>";
     }
     $select_broken .= "<OPTION value='".$i."' ".($i==$recordBroken?"SELECTED":"").">$i</OPTION>";
     $select_qualify .= "<OPTION value='".$i."' ".($i==$qualified?"SELECTED":"").">$i</OPTION>";
}
for ($i=1; $i<=8; $i++) {
     ${"select_score_$i"} .= "</SELECT>\n";
}
$select_broken .= "</SELECT>";
$select_qualify .= "</SELECT>";

### Title ###
$TAGS_OBJ[] = array($i_Sports_menu_Settings_House,"house.php", 0);
$TAGS_OBJ[] = array($i_Sports_menu_Settings_Score,"score.php", 1);
$TAGS_OBJ[] = array($i_Sports_menu_Settings_Lane,"lane.php", 0);
$TAGS_OBJ[] = array($i_Sports_menu_Settings_LaneSet,"laneset.php", 0);
$TAGS_OBJ[] = array($i_Sports_menu_Settings_ParticipantFormat,"participant.php", 0);
$TAGS_OBJ[] = array($Lang['eSports']['Arrangement_Schedule_Setting'],"arrangement_schedule_setting.php", 0);
$MODULE_OBJ = $lsports->GET_MODULE_OBJ_ARR(); 

$linterface->LAYOUT_START();

$PAGE_NAVIGATION[] = array($button_edit.($intranet_session_language=="en"?" ":""). $i_Sports_menu_Settings_Score, "");

?>
<SCRIPT LANGUAGE="Javascript">
<!--
function checkForm(obj)
{
	if(!check_text(obj.scoreName, "<?=$i_alert_pleasefillin.$i_Sports_field_ScoreStandard?>")) return false;
}
//-->
</SCRIPT>

<br />   
<form name="form1" method="post" action="score_standard_edit_update.php" onSubmit="return checkForm(this);">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	<td align='right'><?=$linterface->GET_SYS_MSG($xmsg);?></td>
</tr>
<tr>
	<td colspan="2">
                <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr> 
                	<td><br />
				<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
				<tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_Sports_field_ScoreStandard?> <span class='tabletextrequire'>*</span></span></td>
					<td><input name="scoreName" type="text" class="textboxtext" maxlength="255" value="<?=$scoreName?>"/></td>
				</tr>
                                <? for ($i=1; $i<=8; $i++) { ?>
                                <tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=${"i_Sports_field_Rank_$i"}?> </span></td>
					<td><?=${"select_score_$i"}?></td>
				</tr>
                                <? } ?>
                                <tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_Sports_RecordBroken?> </span></td>
					<td><?=$select_broken?></td>
				</tr>
                                <tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_Sports_Qualified?> </span></td>
					<td><?=$select_qualify?></td>
				</tr>
			</table>
			</td>
                </tr>
                </table>
	</td>
</tr>
<tr>
	<td colspan="2">        
                <table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
                <tr>
                	<td align="left" class="tabletextremark">&nbsp;&nbsp;&nbsp;<?=$i_general_required_field2?></td>
		</tr>
                <tr>
                	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
                <tr>
			<td align="center">
				<?= $linterface->GET_ACTION_BTN($button_save, "submit", "","submit2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
                                <?= $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
				<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='score.php'","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
			</td>
		</tr>
                </table>                                
	</td>
</tr>
</table>                        
<br />

<input type="hidden" name="StandardID" value="<?=$ID?>">
</form>

<?
intranet_closedb();
print $linterface->FOCUS_ON_LOAD("form1.scoreName");
$linterface->LAYOUT_STOP();
?>