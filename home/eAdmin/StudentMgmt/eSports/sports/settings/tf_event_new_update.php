<?php
# Using:

#################################################
#
#   Date:   2019-04-09 Bill     [2019-0301-1144-56289]
#           Kao Yip group relay settings update     ($sys_custom['eSports']['KaoYipRelaySettings'])
#
#################################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libsports.php");

intranet_auth();
intranet_opendb();

$lsports = new libsports();
$lsports->authSportsSystem();

$AgeGroupID = $_POST['AgeGroupID'];
$EventID = $_POST['EventID'];
$GroupID = $_POST['GroupID'];

if (!is_array($GroupID) && sizeof($GroupID)==0)
{
    header("Location: tf_event.php?AgeGroupID=".$AgeGroupID);
    exit();
}

$onlineEnrol = $_POST['onlineEnrol'];
$Quota = $_POST['Quota'];
$countHouse = $_POST['countHouse'];
$countClass = $_POST['countClass'];
$countIndividual = $_POST['countIndividual'];
$scoreStandard = $_POST['scoreStandard'];
$eventQuota = $_POST['eventQuota'] ? $_POST['eventQuota'] : 0;

$sql = "SELECT EventType FROM SPORTS_EVENT WHERE EventID='$EventID'";
$event_type = $lsports->returnVector($sql);
if($event_type[0] == 3)
{
	$onlineEnrol = 0;
	$countIndividual = 0;
	$Quota = 0;
}
if($event_type[0] == 4)
{
    // [2019-0301-1144-56289]
    if($sys_custom['eSports']['AllowRelayWithAgeGroup'] && !$sys_custom['eSports']['KaoYipRelaySettings']) {
	    // do nothing
    }
    else {
    	$onlineEnrol = 0;
    	$Quota = 0;
    	$countHouse = 0;
    	$countIndividual = 0;
	}
}

# SELECT Age Groups which has been assigned with the Event Already
$sql = "SELECT GroupID FROM SPORTS_EVENTGROUP WHERE EventID='$EventID'";
$groups = $lsports->returnVector($sql,1);

$return_msg = "";
$values = "";
$delim = "";
for($i=0; $i<sizeof($GroupID); $i++)
{
	# if the group not yet assigned with the event 
	if(is_array($groups) && !in_array($GroupID[$i], $groups)) {
	     $values .= "$delim ('$EventID', '".$GroupID[$i]."', '$onlineEnrol', '$Quota', '$countHouse',
	                         '$countClass', '$countIndividual', '$scoreStandard', '$eventQuota', now(), now())";
	     $delim = ",";
    }
}
if($values != "")
{
	$sql = "INSERT INTO SPORTS_EVENTGROUP (
                    EventID, GroupID, IsOnlineEnrol, CountPersonalQuota ,
                    CountHouseScore, CountClassScore, CountIndividualScore,
                    ScoreStandardID, EventQuota, DateInput, DateModified)
                    VALUES
                    $values";
	$lsports->db_db_query($sql) or die(mysql_error());
	$return_msg = 1;
}

switch(sizeof($GroupID)) {
	case 1:    $AgeGroupID = $GroupID[0]; break;
	default:   $AgeGroupID = -3;
}

intranet_closedb();
header("Location: tf_event.php?eventID=".$EventID."&AgeGroupID=".$AgeGroupID."&xmsg=add");
?>