<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libsports.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$lsports = new libsports();
$lsports->authSportsSystem();

$date_start = $_POST['date_start'];
$date_end = $_POST['date_end'];
$num_track = $_POST['num_track'];
$num_minTrack = $_POST['num_minTrack'];
$num_field = $_POST['num_field'];
$num_total = $_POST['num_total'];
$detail = intranet_htmlspecialchars($_POST['detail']);

$lsports->enrolDateStart = $date_start;
$lsports->enrolDateEnd = $date_end;
$lsports->enrolMaxTotal = $num_total;
$lsports->enrolMaxTrack = $num_track;
if($sys_custom['eSports']['SportDay_MinEvent_MKSS']){
	$lsports->enrolMinTrack = $num_minTrack;
}
$lsports->enrolMaxField = $num_field;
$lsports->enrolDetails = $detail;
$lsports->saveSettings();

intranet_closedb();
header("Location: enrolment.php?xmsg=update");
?>