<?php
// using : 

/*****************************************************************
 * 	Modification log
 *  20181012 Bill:  [2018-0628-1634-01096]
 *      - Add Settings for Event Type & Round (for Class Relay only)    ($sys_custom['eSports']['KaoYipRelaySettings'])     [removed] [2019-0301-1144-56289]
 * 	20170906 Bill:	[2017-0405-1457-30236]
 * 		- Add Settings for Fields Event: Best Attempt > Best records in both First Round & Final Round
 * ****************************************************************/
 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libsports.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$CurrentPage = "PageItemSettings";

$lsports = new libsports();
$lsports->authSportsSystem();

# Get Event Type
$db_field = ($intranet_session_language=="en"?"EnglishName":"ChineseName");
$sql = "SELECT EventTypeID, $db_field FROM SPORTS_EVENT_TYPE_NAME ORDER BY EventTypeID";
$event_types = $lsports->returnArray($sql,2);
$select_event_type = getSelectByArray($event_types, "name='EventType' onChange='changeEventType(this.value);displayJumpBox(this.value);'","",0,1);

# Get Event Display Order
$sql = "SELECT EventType, MAX(DisplayOrder) FROM SPORTS_EVENT GROUP BY EventType";
$event_order = $lsports->returnArray($sql,2);
$array_order = build_assoc_array($event_order);

/* 
# [2018-0628-1634-01096] Get Relay Event Type
if($sys_custom['eSports']['KaoYipRelaySettings'])
{
    $relayEventTypeAry = $lsports->retrieveClassRelayType();
    $select_relay_event = "<select name='relayEventType'>\n";
    for ($i=0; $i < count($relayEventTypeAry); $i++) {
        $thisRelayEventType = $relayEventTypeAry[$i]['RelayType'];
        $select_relay_event .= "<option value='".$thisRelayEventType."'>".$thisRelayEventType."</option>\n";
    }
    $select_relay_event .= "</select>\n";
}
*/

$pos = 1;
$array_order[$pos++] += 1;
$array_order[$pos++] += 1;
$array_order[$pos++] += 1;
$array_order[$pos++] += 1;

$default_type = 1;

$select_order = "<SELECT name='DisplayOrder'>\n";
for ($i=1; $i<=$array_order[$default_type]; $i++) {
    $select_order .= "<OPTION value='$i' ".($i==$array_order[$default_type]?"SELECTED":"").">$i</OPTION>\n";
}
$select_order .= "</SELECT>\n";

// [2017-0405-1457-30236]
//$jump_box_html = "<input type='checkbox' name='is_jump' value='1'>&nbsp;".$i_Sports_Event_Jump;
$field_setting_select = "<SELECT name='FieldSettings'>\n";
	$field_setting_select .= "<OPTION value='' SELECTED>".$i_general_NotSet."</OPTION>\n";
	$field_setting_select .= "<OPTION value='is_jump'>".$Lang['eSports']['HighJumpEvent']."</OPTION>\n";
	$field_setting_select .= "<OPTION value='best_include_round1'>".$Lang['eSports']['BestTrialIncludeRound1']."</OPTION>\n";
$field_setting_select .= "</SELECT>\n";

### Title ###
$TAGS_OBJ[] = array($i_Sports_menu_Settings_TrackFieldName,"tf_name.php", 1);
$TAGS_OBJ[] = array($i_Sports_menu_Settings_TrackFieldEvent,"tf_event.php", 0);
if($sys_custom['eSports']['SportDay_EventGroupSchedule_MKSS']){
	$TAGS_OBJ[] = array($Lang['eSports']['Settings']['EventGroupSchedule'],"tf_eventgroupschedule.php", 0);
}
$MODULE_OBJ = $lsports->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();

$PAGE_NAVIGATION[] = array($button_new.($intranet_session_language=="en"?" ":""). $i_Sports_menu_Settings_TrackFieldName, "");

$pos = 1;
?>

<SCRIPT LANGUAGE=Javascript>
var type_max = new Array();
var pos = 1;
type_max[pos++] = <?=$array_order[$pos++]?>;
type_max[pos++] = <?=$array_order[$pos++]?>;
type_max[pos++] = <?=$array_order[$pos++]?>;
type_max[pos++] = <?=$array_order[$pos++]?>;

function changeEventType(type_value)
{
     var order_select = document.form1.elements["DisplayOrder"];
     while (order_select.options.length > 0) {
     	order_select.options[0] = null;
     }
     
     var i;
     for (i=1; i<=type_max[type_value]; i++) {
     	order_select.options[i-1] = new Option(i,i);
     }
     order_select.selectedIndex = type_max[type_value] - 1;
	 
	<?php /*if($sys_custom['eSports']['KaoYipRelaySettings']) { ?>
 		$('#relay_event_type').hide();
	 	$('#relay_round_type').hide();
		if(type_value == <?php echo EVENT_TYPE_CLASSRELAY ?>) {
			$('#relay_event_type').show();
			$('#relay_round_type').show();
        }
    <?php } */ ?>
}

function displayJumpBox(type_value)
{
	if(type_value==2) {
		//document.getElementById('DivJumpBox').innerHTML = "<?=$jump_box_html?>";
		$('span#DivJumpBox').show();
	}
	else {
		//document.getElementById('DivJumpBox').innerHTML = "";
		$('span#DivJumpBox').hide();
	}
}

function checkForm(obj)
{
	if(!check_text(obj.engName, "<?=$i_Sports_Warn_Please_Fill_EventName?> <?="($i_general_english)"?>")) return false;
    if(!check_text(obj.chiName, "<?=$i_Sports_Warn_Please_Fill_EventName?> <?="($i_general_chinese)"?>")) return false;
        
    obj.action = "tf_name_new_update.php";
	return true;
}
</SCRIPT>

<br />
<form name="form1" method="post" onSubmit="return checkForm(this);">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	<td align='right'><?=$linterface->GET_SYS_MSG($xmsg);?></td>
</tr>
<tr>
	<td colspan="2">
        <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
        <tr>
        	<td><br />
				<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
				<tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?="$i_Sports_field_Event_Name ($i_general_english)"?> <span class='tabletextrequire'>*</span></span></td>
					<td><input name="engName" type="text" class="textboxtext" maxlength="255" /></td>
				</tr>
                <tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?="$i_Sports_field_Event_Name ($i_general_chinese)"?> <span class='tabletextrequire'>*</span></span></td>
					<td><input name="chiName" type="text" class="textboxtext" maxlength="255" /></td>
				</tr>
                <tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_Sports_field_Event_Type?> </span></td>
					<td><?=$select_event_type?> <span class="tabletext" id="DivJumpBox" style="display:none"><?=$field_setting_select?></span></td>
				</tr>
				<?php /*if($sys_custom['eSports']['KaoYipRelaySettings']) { ?>
                    <tr valign="top" id='relay_event_type' style='display:none'>
    					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$Lang['eSports']['RelayEventType']?> </span></td>
    					<td><?=$select_relay_event?></td>
    				</tr>
                    <tr valign="top" id='relay_round_type' style='display:none'>
    					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$Lang['eSports']['RelayRoundType']?> </span></td>
    					<td>
    						<input type="radio" name="relayRoundType" id="relayRoundType1" value="1"> <label for="relayRoundType1"><?=$i_Sports_First_Round?></label> &nbsp; 
    						<input type="radio" name="relayRoundType" id="relayRoundType2" value="0" checked> <label for="relayRoundType2"><?=$i_Sports_Final_Round?></label>
    					</td>
    				</tr>
				<?php }*/ ?>
                <tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_general_DisplayOrder?> </span></td>
					<td><?=$select_order?></td>
				</tr>
				</table>
			</td>
        </tr>
        </table>
	</td>
</tr>
<tr>
	<td colspan="2">
        <table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
        <tr>
        	<td align="left" class="tabletextremark">&nbsp;&nbsp;&nbsp;<?=$i_general_required_field2?></td>
		</tr>
        <tr>
        	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
        </tr>
        <tr>
			<td align="center">
			<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "","submit2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
            <?= $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
            <?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='tf_name.php'","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
			</td>
		</tr>
        </table>
	</td>
</tr>
</table>

<br />
</form>

<?
intranet_closedb();

print $linterface->FOCUS_ON_LOAD("form1.engName");
$linterface->LAYOUT_STOP();
?>