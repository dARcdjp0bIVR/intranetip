<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libsports.php");

intranet_auth();
intranet_opendb();

$lsports = new libsports();
$lsports->authSportsSystem();

# Get House info
$grade = $_POST['grade'];
$engName = $_POST['engName'];
$chiName = $_POST['chiName'];
$gender = $_POST['gender'];
$gradeCode = $_POST['gradeCode'];
$dobUp = ($_POST['dobUp']==""? 'null':"'".$_POST['dobUp']."'");
$dobLow = ($_POST['dobLow']==""? 'null':"'".$_POST['dobLow']."'");
$GroupID = $_POST['GroupID'];
$num_track = $_POST['num_track'];
$num_field = $_POST['num_field'];
$num_total = $_POST['num_total'];
$displayOrder=$_POST['displayOrder'];

$sql = "UPDATE  SPORTS_AGE_GROUP SET
                    GradeChar = '$grade', EnglishName = '$engName' , ChineseName = '$chiName',
                    Gender = '$gender', GroupCode = '$gradeCode', DOBUpLimit = $dobUp,
                    DOBLowLimit = $dobLow, DateModified = now(), DisplayOrder=$displayOrder,
					EnrolMaxTotal = $num_total, EnrolMaxTrack = $num_track, EnrolMaxField = $num_field
               WHERE AgeGroupID = $GroupID";
$lsports->db_db_query($sql) or die(mysql_error());

intranet_closedb();
header("Location: agegroup.php?xmsg=update");
?>