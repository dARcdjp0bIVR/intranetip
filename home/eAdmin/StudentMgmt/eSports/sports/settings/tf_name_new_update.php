<?php
// using : 

/*****************************************************************
 * 	Modification log
 *  20181012 Bill:  [2018-0628-1634-01096]
 *      - Update Settings for Event Type & Round (for Class Relay only)    ($sys_custom['eSports']['KaoYipRelaySettings'])     [removed] [2019-0301-1144-56289]
 * 	20170906 Bill:	[2017-0405-1457-30236]
 * 		- Update Settings for Fields Event: Best Attempt > Best records in both First Round & Final Round
 * ****************************************************************/
 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libsports.php");

intranet_auth();
intranet_opendb();

$lsports = new libsports();
$lsports->authSportsSystem();

$engName = $_POST['engName'];
$chiName = $_POST['chiName'];
$EventType = $_POST['EventType'];
$DisplayOrder = $_POST['DisplayOrder'];

// [2017-0405-1457-30236]
//$IsJump = ($_POST['is_jump']=="") ? 0 : $_POST['is_jump'];
$IsJump = ($EventType==EVENT_TYPE_FIELD && $_POST['FieldSettings']=="is_jump") ? 1 : 0;
$IsAllTrialIncluded = ($EventType==EVENT_TYPE_FIELD && $_POST['FieldSettings']=="best_include_round1") ? 1 : 0;

/* 
// [2018-0628-1634-01096]
$isKaoYipClassRelay = $sys_custom['eSports']['KaoYipRelaySettings'] && $EventType==EVENT_TYPE_CLASSRELAY;
$relayRoundType = ($isKaoYipClassRelay && $_POST['relayRoundType']=="1") ? 1 : 0;
$relayEventType = $isKaoYipClassRelay? trim($_POST['relayEventType']) : '';
 */

$sql = "INSERT INTO SPORTS_EVENT
            (EventType, EnglishName, ChineseName, DisplayOrder, IsJump, IsAllTrialIncluded, RelayRoundType, RelayEventType, DateInput, DateModified)
        VALUES
            ('$EventType', '$engName', '$chiName', '$DisplayOrder', '$IsJump', '$IsAllTrialIncluded', '0', '', now(), now())";
$lsports->db_db_query($sql);

intranet_closedb();
header("Location: tf_name.php?xmsg=add");
?>