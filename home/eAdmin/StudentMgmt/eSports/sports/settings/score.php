<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libsports.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable{$LAYOUT_SKIN}.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$linterface 	= new interface_html();
$CurrentPage	= "PageGeneralSettings";

# Get House info
$lsports = new libsports();
//$lsports->authSportsSystem();
$results = $lsports->retrieveScoreStandards();

$sql = "
SELECT 
		StandardID, 
		CONCAT('<a class=\"tablelink\" href=\"score_standard_edit.php?ID=', StandardID, '\">', Name, '</a>'),
		Position1, 
		Position2, 
		Position3, 
		Position4,
		Position5, 
		Position6, 
		Position7, 
		Position8,
		RecordBroken,
		Qualified, 
		CONCAT('<input type=\"checkbox\" name=\"ID[]\" value=\"', StandardID ,'\">')
FROM 
		SPORTS_SCORE_STANDARD 
";
		
# TABLE INFO
if($field=="") $field = 0;
$li = new libdbtable2007($field, 1, $pageNo);
$li->field_array = array("DateInput");
$li->sql = $sql;
$li->no_col = 13;
$li->IsColOff = "eSportSettings";

// TABLE COLUMN
$pos = 0;
$li->column_list .= "<td width=1 class='tabletopnolink'>#</td>\n";
$li->column_list .= "<td class='tabletoplink'>$i_Sports_field_ScoreStandard</td>\n";
$li->column_list .= "<td class='tabletoplink'>$i_Sports_field_Rank_1</td>\n";
$li->column_list .= "<td class='tabletoplink'>$i_Sports_field_Rank_2</td>\n";
$li->column_list .= "<td class='tabletoplink'>$i_Sports_field_Rank_3</td>\n";
$li->column_list .= "<td class='tabletoplink'>$i_Sports_field_Rank_4</td>\n";
$li->column_list .= "<td class='tabletoplink'>$i_Sports_field_Rank_5</td>\n";
$li->column_list .= "<td class='tabletoplink'>$i_Sports_field_Rank_6</td>\n";
$li->column_list .= "<td class='tabletoplink'>$i_Sports_field_Rank_7</td>\n";
$li->column_list .= "<td class='tabletoplink'>$i_Sports_field_Rank_8</td>\n";
$li->column_list .= "<td class='tabletoplink'>$i_Sports_RecordBroken</td>\n";
$li->column_list .= "<td class='tabletoplink'>$i_Sports_Qualified</td>\n";
$li->column_list .= "<td width=1 class='tabletoplink'>".$li->check("ID[]")."</td>\n";

$enrol_interval = $sys_custom['eSports_enrol_score_interval'] ? $sys_custom['eSports_enrol_score_interval'] : 0.5;
$scoreEnrol = $lsports->scoreEnrol;
settype($scoreEnrol, "string"); 
$select_enrol = "<SELECT name='scoreEnrol'>";
for ($i=0; $i<=5; $i+=$enrol_interval)
{
 	settype($i, "string"); 
     $select_enrol .= "<OPTION value='".$i."' ".($i==$scoreEnrol?"SELECTED":"").">".number_format($i,1)."</OPTION>\n";
}
$select_enrol .= "</SELECT>\n";

$present_interval = $sys_custom['eSports_present_score_interval'] ? $sys_custom['eSports_present_score_interval'] : 0.5;
$scorePresent = $lsports->scorePresent;
settype($scorePresent, "string"); 
$select_present = "<SELECT name='scorePresent'>";
for ($i=0; $i<=5; $i+=$present_interval)
{
	settype($i, "string"); 
	$select_present .= "<OPTION value='".$i."' ".($i==$scorePresent?"SELECTED":"").">".number_format($i,1)."</OPTION>\n";
}
$select_present .= "</SELECT>\n";

$absent_interval = $sys_custom['eSports_absent_score_interval'] ? $sys_custom['eSports_absent_score_interval'] : 0.5;
$scoreAbsent = $lsports->scoreAbsent;
settype($scoreAbsent, "string"); 
$select_absent = "<SELECT name='scoreAbsent'>";
for ($i=0; $i<=5; $i+=$absent_interval)
{
	settype($i, "string"); 
	$select_absent .= "<OPTION value='".$i."' ". ($i==$scoreAbsent?"SELECTED":"") .">".number_format($i,1)."</OPTION>\n";
}
$select_absent .= "</SELECT>\n";

$select_pattern = "<SELECT name='RankPattern'><option value='1223' ".($lsports->RankPattern=='1223'?"SELECTED":"").">1,2,2,3...</option><option value='1224' ".($lsports->RankPattern=='1224'?"SELECTED":"").">1,2,2,4...</option></SELECT>";
### Button
$AddBtn 	= "<a href=\"javascript:checkNew('score_standard_new.php')\" class='contenttool'><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_new.gif\" width=\"18\" height=\"18\" border=\"0\" align=\"absmiddle\" /> " . $button_new . "</a>";
$delBtn 	= "<a href=\"javascript:checkRemove(document.form1,'ID[]','score_standard_remove.php')\" class=\"tabletool\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_delete.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\" /> " . $button_remove . "</a>";
$editBtn 	= "<a href=\"javascript:checkEdit(document.form1,'ID[]','score_standard_edit.php')\" class=\"tabletool\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_edit.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\" /> " . $button_edit . "</a>";

### Title ###
$TAGS_OBJ[] = array($i_Sports_menu_Settings_House,"house.php", 0);
$TAGS_OBJ[] = array($i_Sports_menu_Settings_Score,"score.php", 1);
$TAGS_OBJ[] = array($i_Sports_menu_Settings_Lane,"lane.php", 0);
$TAGS_OBJ[] = array($i_Sports_menu_Settings_LaneSet,"laneset.php", 0);
$TAGS_OBJ[] = array($i_Sports_menu_Settings_ParticipantFormat,"participant.php", 0);
$TAGS_OBJ[] = array($Lang['eSports']['Arrangement_Schedule_Setting'],"arrangement_schedule_setting.php", 0);
$MODULE_OBJ = $lsports->GET_MODULE_OBJ_ARR();

$PAGE_NAVIGATION1 = $i_Sports_field_ScoreStandard.($intranet_session_language=="en"?" ":"").$i_QB_Settings;
$PAGE_NAVIGATION2 = $i_Sports_Participation_Score.($intranet_session_language=="en"?" ":"").$i_QB_Settings;

$linterface->LAYOUT_START();
?>

<br />
<form name="form1" method="get">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td>&nbsp;&nbsp;&nbsp;<?= $linterface->GET_NAVIGATION2($PAGE_NAVIGATION1) ?></td>
</tr>
<tr>
	<td align="center">
        	<table width="96%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td align="left" class="tabletext">
                              	<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td>
                                        	<table border="0" cellspacing="0" cellpadding="2">
                                            	<tr>
                                              		<td><p><?=$AddBtn?></p></td>
                                            	</tr>
                                          	</table>
					</td>
                                        <td align="right" valign="bottom"><?=$linterface->GET_SYS_MSG($xmsg, $xmsg2);?></td>
				</tr>

				<tr class="table-action-bar">
					<td align="right" valign="bottom" colspan="2" height="28">
						<table border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td width="21"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/table_tool_01.gif" width="21" height="23" /></td>
							<td background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/table_tool_02.gif">
								<table border="0" cellspacing="0" cellpadding="2">
								<tr>
									<td nowrap><?=$delBtn?></td>
                                                                        <td><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="5"></td>
                                                                        <td nowrap><?=$editBtn?></td>
								</tr>
        							</table>
							</td>
        						<td width="6"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/table_tool_03.gif" width="6" height="23" /></td>
						</tr>
						</table>
					</td>
				</tr>
                                <tr>
                                	<td colspan="2">
						<?php
							echo $li->display($li->no_col, 1);
						?>
					</td>
				</tr>
                                
                                
				</table>
			</td>
		</tr>
                </table>
        </td>
</tr>
</table>
<br />

</form>



<!--- participation --->
<br />   
<form name="form2" method="post" action="score_participate_update.php">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td>&nbsp;&nbsp;&nbsp;<?= $linterface->GET_NAVIGATION2($PAGE_NAVIGATION2) ?></td>
	<td align='right'><?=$linterface->GET_SYS_MSG($xmsg3);?></td>
</tr>
<tr>
	<td colspan="2">
                <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr> 
                	<td><br />
				<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
                                <tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_Sports_Enrolment.($intranet_session_language=="en"?" ":"").$i_Sports_field_Score?> </span></td>
					<td><?=$select_enrol?></td>
				</tr>
                                <tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_Sports_Present.($intranet_session_language=="en"?" ":"").$i_Sports_field_Score?> </span></td>
					<td><?=$select_present?></td>
				</tr>
                                <tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_Sports_Absent.($intranet_session_language=="en"?" ":"").$i_Sports_field_Score?> </span></td>
					<td class="tabletext"><?=$select_absent?> (<?=$i_Sports_Decrement?>)</td>
				</tr>
				
			</table>
			</td>
                </tr>
                </table>
	</td>
</tr>
<tr>
	<td><br><br>&nbsp;&nbsp;&nbsp;<?= $linterface->GET_NAVIGATION2($i_Sports_Rank_Pattern) ?></td>
</tr>
<tr>
	<td colspan="2">
                <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr> 
                	<td><br />
				<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
                 	 <tr valign="top">
						<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_Sports_Rank_Pattern?> </span></td>
						<td class="tabletext"><?=$select_pattern?> </td>
					</tr>

			</table>
			</td>
                </tr>
                </table>
	</td>
</tr>
<tr>
	<td colspan="2">        
                <table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
                <tr>
                	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
                <tr>
			<td align="center">
				<?= $linterface->GET_ACTION_BTN($button_save, "submit", "","submit2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
                                <?= $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
			</td>
		</tr>
                </table>                                
	</td>
</tr>
</table>                        
<br />
</form>

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>