<?
# modifying : 

####################### Change Log [start] ###########
#
#	Date:	2016-07-12	Bill
#			get reg from $_GET, fixed cannot get value in PHP 5.4
#
####################### Change Log [end] ###########

function prependChar($str, $c, $maxlength) {
	$len = strlen($str);
	for ($i = 0; $i < $maxlength - $len; $i++) {
		$str = $c.$str;
	}
	return $str;
} 

header("Content-type: image/png");

$rgbok = false;

$rgb = $_GET["rgb"];
if (isset($rgb)) {
	if (strlen($rgb) < 6) {
		$rgb = prependChar($rgb, "0", 6);
	}
	$r = substr($rgb, 0, 2);
	$g = substr($rgb, 2, 2);
	$b = substr($rgb, 4, 2);
}
if (!isset($r)) $r = "00";
if (!isset($g)) $g = "00";
if (!isset($b)) $b = "00";

$r = hexdec($r);
$g = hexdec($g);
$b = hexdec($b);

$im = ImageCreate(1, 1);
$color = ImageColorAllocate($im, (int)$r, (int)$g, (int)$b);
ImageFill($im, 0, 0, $color);
ImagePng($im);
ImageDestroy($im);
?>