<?php
# using: yat

#############################################
#
#	Date:	2013-08-06	YatWoon
#			add customization for $sys_custom['eSports']['SantaRosa']	[Case#2013-0424-1211-29096]
#
#	Date:	2013-08-06	YatWoon
#			fixed: failed to display system message
#
#	Date:	2011-01-04 YatWoon
#			add export function
#
#############################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libsports.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$linterface 	= new interface_html();
$CurrentPage	= "PageAnnualSettings";

# Get House info
$lsports = new libsports();
$lsports->authSportsSystem();

$results = $lsports->retrieveParticipantSummary();
# TABLE INFO
if($field=="") $field = 0;
$li = new libdbtable2007($field, $order, $pageNo);
$li->IsColOff = "eSportParticipant";

// TABLE COLUMN
$pos = 0;
$li->column_list .= "<td class='tablebluetop tabletoplink'>$i_ClassName</td>\n";
$li->column_list .= "<td class='tablebluetop tabletoplink'>$i_Sports_Participant_Number_Summary_NumberOfStudents</td>\n";
$li->column_list .= "<td class='tablebluetop tabletoplink'>$i_Sports_Participant_Number_Summary_NoClassNum</td>\n";
$li->column_list .= "<td class='tablebluetop tabletoplink'>$i_Sports_Participant_Number_Summary_NoHouse</td>\n";
$li->column_list .= "<td class='tablebluetop tabletoplink'>$i_Sports_Participant_Number_Summary_MultipleHouse</td>\n";
$li->column_list .= "<td class='tablebluetop tabletoplink'>$i_Sports_Participant_Number_Summary_NoGender</td>\n";
$li->column_list .= "<td class='tablebluetop tabletoplink'>$i_Sports_Participant_Number_Summary_NoDOB</td>\n";
$li->column_list .= "<td class='tablebluetop tabletoplink'>$i_Sports_Participant_Number_Summary_NoAthleticNumber</td>\n";

### Title ###
$TAGS_OBJ[] = array($i_Sports_menu_Settings_AgeGroup,"agegroup.php", 0);
$TAGS_OBJ[] = array($i_Sports_menu_Settings_ParticipantGenerate,"participant_gen.php", 1);
$TAGS_OBJ[] = array($i_Sports_menu_Settings_Enrolment,"enrolment.php", 0);
$TAGS_OBJ[] = array($i_Sports_menu_Settings_YearEndClearing,"record_clear.php", 0);
$MODULE_OBJ = $lsports->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$xmsg]);
?>
<SCRIPT LANGUAGE=Javascript>
function number_generate()
{
         if (confirm('<?=$i_Sports_Participant_Number_alert_generate?>'))
         {
	         <? if($sys_custom['eSports']['SantaRosa']) {?>
	         	location.href = 'participant_generate_santarosa.php';
	         <? } else {?>
             	location.href = 'participant_generate.php?flag=1';
             <? } ?>
         }
}

</SCRIPT>

<div class="content_top_tool">
<?= $linterface->GET_LNK_EXPORT_IP25("participant_gen_export.php", $Lang['eSports']['ExportStudentDetails']) ?>
<br style="clear:both" />
</div>

<form name="form1" method="get">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
	<td>
		<?php
		echo $li->display();
		?>
	</td>
</tr>
</table>
                                        

<div class="edit_bottom_v30">
                      	<p class="spacer"></p>
<?= $linterface->GET_ACTION_BTN($i_Sports_Participant_Number_Generate, "button", "number_generate();","submit2") ?>
<p class="spacer"></p>
</div>                
                
</form>

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>
