<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libsports.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$lsports = new libsports();
$lsports->authSportsSystem();

# Get House info
$scoreName = $_POST['scoreName'];
$rank1 = $_POST['rank1'];
$rank2 = $_POST['rank2'];
$rank3 = $_POST['rank3'];
$rank4 = $_POST['rank4'];
$rank5 = $_POST['rank5'];
$rank6 = $_POST['rank6'];
$rank7 = $_POST['rank7'];
$rank8 = $_POST['rank8'];
$recordBroken = $_POST['recordBroken'];
$qualify = $_POST['qualify'];

$sql = "INSERT INTO SPORTS_SCORE_STANDARD (Name, Position1, Position2, Position3, Position4,
               Position5, Position6, Position7, Position8, RecordBroken, Qualified, DateInput,
               DateModified) VALUES
               ('$scoreName','$rank1','$rank2','$rank3','$rank4','$rank5','$rank6','$rank7','$rank8',
               '$recordBroken','$qualify',now(),now())";
$lsports->db_db_query($sql);

intranet_closedb();
header("Location: score.php?xmsg=add");
?>