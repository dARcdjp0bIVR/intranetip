<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libsports.php");

intranet_auth();
intranet_opendb();

$lsports = new libsports();
$lsports->authSportsSystem();

# Get House info
$grade = $_POST['grade'];
$engName = $_POST['engName'];
$chiName = $_POST['chiName'];
$gender = $_POST['gender'];
$gradeCode = $_POST['gradeCode'];
$dobUp = ($_POST['dobUp']==""? 'null':"'".$_POST['dobUp']."'");
$dobLow = ($_POST['dobLow']==""? 'null':"'".$_POST['dobLow']."'");
$num_track = $_POST['num_track'];
$num_field = $_POST['num_field'];
$num_total = $_POST['num_total'];
$displayOrder = ($_POST['displayOrder']==""? 'null':"'".$_POST['displayOrder']."'");

$sql = "INSERT INTO SPORTS_AGE_GROUP (
                    GradeChar, EnglishName, ChineseName, Gender, GroupCode, DOBUpLimit,
                    DOBLowLimit ,DisplayOrder, DateInput, DateModified,
					EnrolMinTotal, EnrolMaxTotal, EnrolMinTrack, EnrolMaxTrack, EnrolMinField, EnrolMaxField) VALUES
                    ('$grade','$engName','$chiName','$gender','$gradeCode',$dobUp,$dobLow,$displayOrder,
                    now(), now(),NULL,$num_total,NULL,$num_track,NULL,$num_field)";
$lsports->db_db_query($sql);

intranet_closedb();
header("Location: agegroup.php?xmsg=add");
?>