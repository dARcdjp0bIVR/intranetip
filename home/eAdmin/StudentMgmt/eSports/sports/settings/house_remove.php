<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libsports.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$lsports = new libsports();
$lsports->authSportsSystem();

# Get House info
$list = implode(",",$HouseID);
if($list!=""){
	$sql = "DELETE FROM INTRANET_HOUSE WHERE HouseID IN ($list)";
	$lsports->db_db_query($sql);
}

intranet_closedb();
header("Location: house.php?xmsg=delete");
?>

