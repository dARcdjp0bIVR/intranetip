<?php
######################################################
# customization for $sys_custom['eSports']['SantaRosa']	[Case#2013-0424-1211-29096]
#
# 	號碼布--按學校提供的編號碼布順序方法。
#    - 級別大至小 > 班別小至大 > 班號小至大 (即中六至中一, 小六至至小三)
#    - 班別順序根據學校基本設定>班別列表次序
#    - 小一及小二不需要號碼布
######################################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libsports.php");
include_once($PATH_WRT_ROOT."includes/libsports_cust.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

if(!$sys_custom['eSports']['SantaRosa'])
{
	header("Location: participant_gen.php?xmsg=UpdateUnsuccess");
	exit;
}

intranet_auth();
intranet_opendb();

$lsports = new libsports();
$lsports->authSportsSystem();
$lsports_cust = new libsports_cust();
$lclass = new libclass();

##### enrolled event student list
$students = $lsports->Get_Enroled_Participant_List("Vector");

##### Reset all athlete number
$sql = "update SPORTS_STUDENT_ENROL_INFO set AthleticNum = NULL";
$lsports->db_db_query($sql);

$athlnum = 0;
# 級別大至小
$year_list = $lsports_cust->Get_Year_List();
foreach($year_list as $yk=>$ydata)
{
	list($this_YearID, $this_YearName) = $ydata;
	
	# 小一及小二不需要號碼布
	if($this_YearName=="Primary 1" || $this_YearName=="Primary 2")	continue;
	
	# 班別小至大
	$class_list = $lclass->returnClassListByLevel($this_YearID);
	if(empty($class_list))	continue;
	foreach($class_list as $ck=>$cdata)
	{
		$this_YearClassID = $cdata['YearClassID'];
		# 班號小至大
		$student_list = $lclass->getStudentByClassId($this_YearClassID);
		if(empty($student_list))	continue;
		foreach($student_list as $sk=>$sdata)
		{
			$this_StudentID = $sdata['UserID'];
			# only count for participant
			if(in_array($this_StudentID, $students))
			{
				# check with SPORTS_ENROL_RESTRICTION_LIST
				$sql = "select count(UserID) from SPORTS_ENROL_RESTRICTION_LIST where UserID=$this_StudentID";
				$restrict = $lsports->returnVector($sql);
				if(!$restrict[0])
				{
					$athlnum++;
					$athlnum = str_pad($athlnum, 4, "0", STR_PAD_LEFT);
					
					# Check whether student record exist in SPORTS_STUDENT_ENROL_INFO
					$sql = "SELECT COUNT(*) FROM SPORTS_STUDENT_ENROL_INFO WHERE StudentID = '$this_StudentID'";
					$exist_flag = $lsports->returnVector($sql);
	
					if($exist_flag[0] == 1)		# Update Athletic Number if record exist
					{
						$sql = "UPDATE SPORTS_STUDENT_ENROL_INFO SET AthleticNum = '$athlnum', DateModified = now() WHERE StudentID = '$this_StudentID'";
						$lsports->db_db_query($sql);
					}
					else		# Insert record is record not exist
					{
						$sql = "INSERT INTO SPORTS_STUDENT_ENROL_INFO (StudentID, AthleticNum, TrackEnrolCount, FieldEnrolCount, DateModified) VALUES ('$this_StudentID', '$athlnum', 0, 0, now())";
						$lsports->db_db_query($sql);
					}
				}
			}
		}
		
	}
}

intranet_closedb();
header("Location: participant_gen.php?xmsg=UpdateSuccess");
?>