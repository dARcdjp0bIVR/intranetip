<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libsports.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$lsports = new libsports();
$lsports->authSportsSystem();

# Get House info
$numbertype = $_POST['numbertype'];

$cond1_1 = $_POST['cond1_1'];
$cond1_2 = $_POST['cond1_2'];
$autonum_len = $_POST['autonum_len'];

$cond2_1 = $_POST['cond2_1'];
$cond2_2 = $_POST['cond2_2'];
$cond2_3 = $_POST['cond2_3'];

$lsports->numberGenerationType = $numbertype;
if ($numbertype==1)
{
    $lsports->numberGenRule1 = $cond1_1;
    $lsports->numberGenRule2 = $cond1_2;
    $lsports->numberGenAutoLength = $autonum_len;
}
else
{
    $lsports->numberGenRule1 = $cond2_1;
    $lsports->numberGenRule2 = $cond2_2;
    $lsports->numberGenRule3 = $cond2_3;
}

$lsports->saveSettings();

intranet_closedb();
header("Location: participant.php?xmsg=update");
?>