<?php
// using:

/********************************************************
 *  Modification Log
 *  Date: 2019-04-15 (Bill)     [2019-0301-1144-56289]
 *          Add Event Type Selection
 *	Date: 2016-06-21 (Cara)
 * 			Add Searchbox
 ********************************************************/

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libsports.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable{$LAYOUT_SKIN}.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

if ($page_size_change == 1) {
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$CurrentPage = "PageItemSettings";

$lsports = new libsports();
$lsports->authSportsSystem();

# TABLE SQL
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

# [2019-0301-1144-56289] Get Event Type
$db_field = $intranet_session_language=="en"? "EnglishName" : "ChineseName";
$sql = "SELECT EventTypeID, $db_field FROM SPORTS_EVENT_TYPE_NAME ORDER BY EventTypeID";
$eventTypes = $lsports->returnArray($sql, 2);

$field = $field==""? 1 : $field;
switch($field) {
	case 0: $field = 0; break;
	case 1: $field = 1; break;
	case 2: $field = 2; break;
	default: $field = 1;
}

$order = $order==""? 1 : $order;
$db_field = $intranet_session_language=="en"? "EnglishName" : "ChineseName";

$keyword = standardizeFormPostValue($_GET['keyword']);
if ($keyword != '') {
	$Keyword_cond = " AND (a.$db_field LIKE '%".$lsports->Get_Safe_Sql_Like_Query($keyword)."%' OR b.$db_field LIKE '%".$lsports->Get_Safe_Sql_Like_Query($keyword)."%')" ;
}

// [2019-0301-1144-56289]
$eventType_cond = "";
if($eventType != "") {
    $eventType_cond = " AND a.EventType = '$eventType' ";
}

$sql = "SELECT 
        	1, 
        	CONCAT('<a class=tablelink href=tf_name_edit.php?EventID=', a.EventID, '>', a.$db_field, '</a>'), 
            b.$db_field, 
            a.DisplayOrder,
        	CONCAT('<input type=checkbox name=EventID[] value=', a.EventID, '>')
    	FROM
            SPORTS_EVENT as a
    	    LEFT OUTER JOIN SPORTS_EVENT_TYPE_NAME as b ON a.EventType = b.EventTypeID
    	WHERE
    		1
    		$Keyword_cond
    		$eventType_cond ";

# TABLE INFO
$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("a.$db_field", "b.EventTypeID", "a.DisplayOrder");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;
$li->IsColOff = "SmartCardReminder";

// TABLE COLUMN
$pos = 0;
$li->column_list .= "<td width=1 class='tabletop tabletoplink'>#</td>\n";
$li->column_list .= "<td width=40% class='tabletop tabletoplink'>".$li->column($pos++, $i_Sports_field_Event_Name)."</td>\n";
$li->column_list .= "<td width=40% class='tabletop tabletoplink'>".$li->column($pos++, $i_Sports_field_Event_Type)."</td>\n";
$li->column_list .= "<td width=20% class='tabletop tabletoplink'>".$li->column($pos++, $i_general_DisplayOrder)."</td>\n";
$li->column_list .= "<td width=1 class='tabletop tabletoplink'>".$li->check("EventID[]")."</td>\n";

$select_event_type = $i_Sports_field_Event_Type." : ".getSelectByArray($eventTypes, "id='eventType' name='eventType' onChange='this.form.submit()'", $eventType, 0, 0, $i_status_all);

### Button
$AddBtn = "<a href=\"javascript:checkNew('tf_name_new.php')\" class='contenttool'><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_new.gif\" width=\"18\" height=\"18\" border=\"0\" align=\"absmiddle\" /> " . $button_new . "</a>";
$delBtn = "<a href=\"javascript:checkRemove(document.form1,'EventID[]','tf_name_remove.php')\" class=\"tabletool\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_delete.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\" /> " . $button_remove . "</a>";
$editBtn = "<a href=\"javascript:checkEdit(document.form1,'EventID[]','tf_name_edit.php')\" class=\"tabletool\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_edit.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\" /> " . $button_edit . "</a>";

$htmlAry['searchBox'] = $linterface->Get_Search_Box_Div('keyword', $keyword);

### Title ###
$TAGS_OBJ[] = array($i_Sports_menu_Settings_TrackFieldName,"tf_name.php", 1);
$TAGS_OBJ[] = array($i_Sports_menu_Settings_TrackFieldEvent,"tf_event.php", 0);
if($sys_custom['eSports']['SportDay_EventGroupSchedule_MKSS']){
	$TAGS_OBJ[] = array($Lang['eSports']['Settings']['EventGroupSchedule'],"tf_eventgroupschedule.php", 0);
}
$MODULE_OBJ = $lsports->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();
?>

<br />
<form name="form1" method="get">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td align="center">
    	<table width="96%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td align="left" class="tabletext">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
               		<tr><td align="right" valign="bottom" colspan="2"><?=$linterface->GET_SYS_MSG($xmsg, $xmsg2);?></td></tr>
					<tr>
						<td colspan="2">
                    		<table border="0" cellspacing="0" cellpadding="2" style="width:100%;">
                        		<tr>
                            		<td><p><?=$AddBtn?></p></td>
                            		<td align="right"><?=$htmlAry['searchBox']?></td>
                        		</tr>
                    		</table>
						</td>
					</tr>
					<tr class="table-action-bar">
                        <td height="28" class="tabletext"><?=$select_event_type?></td>
						<td align="right" valign="bottom" height="28">
							<table border="0" cellspacing="0" cellpadding="0">
								<tr><td width="21"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/table_tool_01.gif" width="21" height="23" /></td>
									<td background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/table_tool_02.gif">
										<table border="0" cellspacing="0" cellpadding="2">
											<tr><td nowrap><?=$delBtn?></td>
                                            	<td><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="5"></td>
                                                <td nowrap><?=$editBtn?></td>
											</tr>
	        							</table>
									</td>
	        						<td width="6"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/table_tool_03.gif" width="6" height="23" /></td>
								</tr>
							</table>
						</td>
					</tr>
                    <tr>
                         <td colspan="2">
							<?php echo $li->display($li->no_col, 0, 1); ?>
						 </td>
					</tr>
				</table>
			</td>
		</tr>
		</table>
	</td>
</tr>
</table>
<br />

<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>">
<input type="hidden" name="order" value="<?php echo $li->order; ?>">
<input type="hidden" name="field" value="<?php echo $li->field; ?>">
<input type="hidden" name="page_size_change" value="">
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>">
</form>

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>