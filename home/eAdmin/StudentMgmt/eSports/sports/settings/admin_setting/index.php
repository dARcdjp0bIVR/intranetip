<?php
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();
if(!$plugin['Sports'] || !$_SESSION["SSV_USER_ACCESS"]["eAdmin-eSportsAdmin"])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

### Cookies handling
# set cookies
$arrCookies = array();
$arrCookies[] = array("ck_settings_admin_setting_page_size", "numPerPage");
$arrCookies[] = array("ck_settings_admin_setting_page_number", "pageNo");
$arrCookies[] = array("ck_settings_admin_setting_page_order", "order");
$arrCookies[] = array("ck_settings_admin_setting_page_field", "field");

if(isset($clearCoo) && $clearCoo == 1)
{
	clearCookies($arrCookies);
	$ck_settings_admin_setting_page_size = '';
}
else 
	updateGetCookies($arrCookies);

include_once($PATH_WRT_ROOT."includes/libsports.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");


$linterface 	= new interface_html();
$CurrentPage	= "PageAdminSettings";
$lsports = new libsports();

$name_field = getNameFieldWithClassNumberByLang("iu.");
$name_field = getNameFieldInactiveUserIndicator($name_field,"iu.");
$sql = "
	SELECT 
		$name_field as NameField, 
		CASE UserType
		WHEN 'Admin' THEN '".$Lang['Sports']['Admin']."'
		WHEN 'Helper' THEN '".$Lang['Sports']['Helper']."'
		END AS UserType,
		CONCAT('<input type=\"checkbox\" name=\"AdminID[]\" value=\"', AdminUserID ,'\">')
	FROM 
		SPORTS_ADMIN_USER_ACL as acl
		INNER JOIN INTRANET_USER as iu ON acl.AdminUserID = iu.UserID
";

if($AdminType>'-1')
	$sql .= " WHERE acl.UserType = '$AdminType'";

# TABLE INFO
if($field=="") $field = 0;
if($order=="") $order =1;

$li = new libdbtable2007($field, $order, $pageNo);
$li->page_size = $numPerPage?$numPerPage:20;
$li->field_array = array("SUBSTRING(NameField,2)", "UserType");
$li->sql = $sql;
$li->no_col = 4;
$li->IsColOff = "IP25_table";

// TABLE COLUMN
$li->column_list .= "<th class='tabletop' width='1'>#</th>\n";
$li->column_list .= "<th class='tabletop' width='50%'>". $li->column_IP25(0,$Lang['Sports']['HelperName']) ."</th>\n";
$li->column_list .= "<th class='tabletop' >". $li->column_IP25(1,$Lang['Sports']['AdminType']) ."</th>\n";
$li->column_list .= "<th class='tabletop' width='25'>".$li->check("AdminID[]")."</th>\n";

### Button
$AddBtn = $linterface->GET_LNK_ADD("add.php","","","","",0);
$delBtn = $linterface->GET_LNK_REMOVE("javascript:checkRemove(document.form1,'AdminID[]','remove.php')");
$editBtn = $linterface->GET_LNK_EDIT("javascript:checkEdit(document.form1,'AdminID[]','edit.php')","","","","",0);
//$editBtn 	= "<a href=\"javascript:checkEdit(document.form1,'ID[]','agegroup_edit.php')\" class=\"tabletool\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_edit.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\" /> " . $button_edit . "</a>";

### Filter
$AdminList = array();
$AdminList[] = array("-1",$Lang['Btn']['All']);
$AdminList[] = array("Admin",$Lang['Sports']['Admin']);
$AdminList[] = array("Helper",$Lang['Sports']['Helper']);
$AdminLevelSelection = $Lang['Sports']['AdminType']." : ".getSelectByArray($AdminList,"name='AdminType' onChange='this.form.submit()'",$AdminType,0,1);

### Title ###
$TAGS_OBJ[] = array($Lang['Sports']['AdminSettings']);
$MODULE_OBJ = $lsports->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();
?>

<br />
<form name="form1" method="get">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td align="center">
        	<table width="96%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td align="left" class="tabletext">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td>
                        	<table border="0" cellspacing="0" cellpadding="2">
                            	<tr>
                              		<td><p><?=$AddBtn?></p></td>
                            	</tr>
                          	</table>
					</td>
					<td align="right" valign="bottom"><?=$linterface->GET_SYS_MSG($msg, $msg2);?></td>
				</tr>
				<tr class="table-action-bar">
					<td valign="bottom" height="28">
					<?=$AdminLevelSelection?>
					</td>
					<td align="right" valign="bottom" height="28">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
								<td>
									<div class="common_table_tool">
								        <?=$editBtn?>
								        <?=$delBtn?>
									</div>		
								</td>		
							</tr>
						</table>
					</td>
				</tr>
                                <tr>
                                	<td colspan="2">
						<?php
							echo $li->display($li->no_col);
						?>
					</td>
				</tr>
				</table>
				<?=$linterface->InactiveStudentField()?>
			</td>
		</tr>
                </table>
        </td>
</tr>
</table>
<br />

<input type="hidden" name="pageNo" value="<?=$li->pageNo?>" />
<input type="hidden" name="order" value="<?=$li->order?>" />
<input type="hidden" name="field" value="<?=$li->field?>" />
<input type="hidden" name="page_size_change" value="" />
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />
</form>
 
<?

intranet_closedb();
$linterface->LAYOUT_STOP();
?>