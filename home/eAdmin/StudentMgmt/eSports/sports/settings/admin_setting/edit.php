<?php
# using: yat

#####################################################
#	Date:	2013-08-06	YatWoon
#			fixed: $this_UserType replace $UserType
#####################################################
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

if(!$plugin['Sports'] || !$_SESSION["SSV_USER_ACCESS"]["eAdmin-eSportsAdmin"])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libsports.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");

intranet_auth();
intranet_opendb();

$CurrentPage = "PageAdminSettings";

$linterface = new interface_html();
$lsports = new libsports();

$PAGE_NAVIGATION[] = array($Lang['Btn']['Edit']);

# Left menu 
$TAGS_OBJ[] = array($Lang['Sports']['AdminSettings']);
$MODULE_OBJ = $lsports->GET_MODULE_OBJ_ARR();

# Start layout
$linterface->LAYOUT_START();

$name_field = getNameFieldByLang("b.");
$sql = "
	SELECT 
		$name_field as NameField, 
		UserType
	FROM 
		SPORTS_ADMIN_USER_ACL as a
		LEFT OUTER JOIN INTRANET_USER as b ON a.AdminUserID = b.UserID
	WHERE 
		a.AdminUserID = '".$AdminID[0]."'
";
$tmp = $lsports->returnArray($sql,2);

list($NameField,$this_UserType) = $tmp[0];

if ($this_UserType == "Admin")
    $strFull = "CHECKED";
else
    $strNormal = "CHECKED";
?>

<br />   
<form name="form1" method="get" action="edit_update.php" onSubmit="return checkform();">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	<td align='right'><?=$linterface->GET_SYS_MSG($xmsg);?></td>
</tr>

<tr>
	<td colspan="2">
		<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr valign='top'>
			<td width='30%' valign="top" class="formfieldtitle" nowrap><?=$Lang['Sports']['HelperName']?></td>
			<td class='tabletext'><?=$NameField?></td>
		</tr>
		</table>
	</td>
</tr>

<tr>
	<td colspan="2">
		<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr valign='top'>
			<td width='30%' valign="top" class="formfieldtitle" nowrap><?=$Lang['Sports']['AdminType']?></td>
			<td class='tabletext'>
			<input type="radio" name="AdminLevel" value="Helper" id="AdminLevel0" <?=$strNormal?>> <label for="AdminLevel0"><?=$Lang['Sports']['Helper']?></label> <br>
			<input type="radio" name="AdminLevel" value="Admin" id="AdminLevel1" <?=$strFull?>> <label for="AdminLevel1"><?=$Lang['Sports']['Admin']?></label>
			</td>
		</tr>
		</table>
	</td>
</tr>

<tr>
	<td colspan="2">        
        <table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
        <tr>
        	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
        </tr>
        <tr>
			<td align="center">
				<?= $linterface->GET_ACTION_BTN($button_submit, "submit") ?>
				<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "history.back()","cancelbtn") ?>
			</td>
		</tr>
        </table>                                
	</td>
</tr>

</table>                        
<br />

<input type="hidden" name="AdminID" value="<?=$AdminID[0]?>">
</form>

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>