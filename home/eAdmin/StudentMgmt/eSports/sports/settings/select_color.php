<?php
# modifying : 

####################### Change Log [start] ###########
#
#	Date:	2016-07-12	Bill
#			change eregi() to preg_match(), for PHP 5.4
#
####################### Change Log [end] ###########

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libsports.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$linterface 	= new interface_html();

$navbarHeight = 300;
$navbarWidth = 20;
$navbarColorCount = 150;	

$gradLTColor = "FFFFFF";
$gradRTColor = "FF0000";
$gradLBColor = "000000";
$gradRBColor = "000000";
$gradHeight = 300;
$gradWidth = 300;
$gradVColorCount = 30;		
$gradHColorCount = 30;		

$trans_image_path = $PATH_WRT_ROOT."images/space.gif";

$lg_color_preview = $i_Sports_Color_Preview;
$lg_ok = "OK";
$lg_cancel = "Cancel";

if (!isset($colorpreview)) $colorpreview = "";
if (!isset($formname)) $formname = "";
if (!isset($rgbinput)) $rgbinput = "";
if (!isset($rgb)) $rgb = "FF0000";

// this function repeatedly prepend the character $c into the string $str until length meet $maxlength
function prependChar($str, $c, $maxlength) {
	$len = strlen($str);
	for ($i = 0; $i < $maxlength - $len; $i++) {
		$str = $c.$str;
	}
	return $str;
} 

function fixrgb($rgb) {
	$rgb = trim($rgb);
	$rgblen = strlen($rgb);
	if ($rgblen > 6) {
		$rgb = substr($rgb, 0, 6);
	} elseif ($rgblen < 6) {
		$rgb = prependChar($rgb, "0", 6);
	}

	return $rgb;
} 

function splitrgb($rgb) {
	$r = substr($rgb, 0, 2); $g = substr($rgb, 2, 2); $b = substr($rgb, 4, 2);
	$r = hexdec($r); $g = hexdec($g); $b = hexdec($b);

	return array($r, $g, $b);
}

function dec2rgb($r, $g, $b) {
	return strtoupper(prependChar(dechex($r), "0", 2).prependChar(dechex($g), "0", 2).prependChar(dechex($b), "0", 2));
}

function colorGradient1D($startrgb, $endrgb, $height, $width, $colorcount, $jsname="", $orientation="v") {
	global $trans_image_path;

	list($r0, $g0, $b0) = splitrgb($startrgb);
	list($r1, $g1, $b1) = splitrgb($endrgb);

	$grad = "";
	if ($orientation == "v") {
		$eachheight = (int)($height / $colorcount);
		$rinc = ($r1-$r0)/($colorcount-1);
		$ginc = ($g1-$g0)/($colorcount-1);
		$binc = ($b1-$b0)/($colorcount-1);
		for ($i = 0; $i < $colorcount; $i++) {
			$r = (int)($r0 + $rinc*$i);
			$g = (int)($g0 + $ginc*$i);
			$b = (int)($b0 + $binc*$i);
			$rgb = dec2rgb($r, $g, $b);
			if ($jsname == "") {
				$grad .= "<tr><td bgcolor=\"#$rgb\"><img border=0 src=\"$trans_image_path\" alt=\"$rgb\" height=$eachheight width=$width></td></tr>\r\n";
			} else {
				$grad .= "<tr><td bgcolor=\"#$rgb\"><a href=\"javascript:$jsname('$rgb');\"><img border=0 src=\"$trans_image_path\" alt=\"$rgb\" height=$eachheight width=$width></a></td></tr>\r\n";
			}
		}
		if (($slack=($height-$colorcount*$eachheight)) > 0) {
			if ($jsname == "") {
				$grad .= "<tr><td bgcolor=\"#$endrgb\"><img border=0 src=\"$trans_image_path\" alt=\"$endrgb\" height=$slack width=$width></td></tr>\r\n";
			} else {
				$grad .= "<tr><td bgcolor=\"#$endrgb\"><a href=\"javascript:$jsname('$rgb');\"><img border=0 src=\"$trans_image_path\" alt=\"$endrgb\" height=$slack width=$width></a></td></tr>\r\n";
			}
		}
	} elseif ($orientation == "h") {
		$eachwidth = (int)($width / $colorcount);
		$rinc = ($r1-$r0)/($colorcount-1);
		$ginc = ($g1-$g0)/($colorcount-1);
		$binc = ($b1-$b0)/($colorcount-1);
		for ($i = 0; $i < $colorcount; $i++) {
			$r = (int)($r0 + $rinc*$i);
			$g = (int)($g0 + $ginc*$i);
			$b = (int)($b0 + $binc*$i);
			$rgb = dec2rgb($r, $g, $b);
			if ($jsname == "") {
				$grad .= "<td bgcolor=\"#$rgb\"><img border=0 src=\"$trans_image_path\" alt=\"$rgb\" height=$height width=$eachwidth></td>\r\n";
			} else {
				$grad .= "<td bgcolor=\"#$rgb\"><a href=\"javascript:$jsname('$rgb');\"><img border=0 src=\"$trans_image_path\" alt=\"$rgb\" height=$height width=$eachwidth></a></td>\r\n";
			}
		}
		if (($slack=($width-$colorcount*$eachwidth)) > 0) {
			if ($jsname == "") {
				$grad .= "<td bgcolor=\"#$endrgb\"><img border=0 src=\"$trans_image_path\" alt=\"$endrgb\" height=$height width=$slack></td>\r\n";
			} else {
				$grad .= "<td bgcolor=\"#$endrgb\"><a href=\"javascript:$jsname('$rgb');\"><img border=0 src=\"$trans_image_path\" alt=\"$endrgb\" height=$height width=$slack></a></td>\r\n";
			}
		}
	}

	return $grad;
} 

function colorGradient2D($ltcolor, $rtcolor, $lbcolor, $rbcolor, $height, $width, $vcolorcount, $hcolorcount, $jsname="") {
	list($lr0, $lg0, $lb0) = splitrgb($ltcolor);
	list($lr1, $lg1, $lb1) = splitrgb($lbcolor);
	list($rr0, $rg0, $rb0) = splitrgb($rtcolor);
	list($rr1, $rg1, $rb1) = splitrgb($rbcolor);

	$grad = "";
	$grad .= "<table border=0 cellpadding=0 cellspacing=0>\r\n";
	$eachheight = (int)($height / $vcolorcount);
	$lrinc = ($lr1-$lr0)/($vcolorcount-1);
	$lginc = ($lg1-$lg0)/($vcolorcount-1);
	$lbinc = ($lb1-$lb0)/($vcolorcount-1);
	$rrinc = ($rr1-$rr0)/($vcolorcount-1);
	$rginc = ($rg1-$rg0)/($vcolorcount-1);
	$rbinc = ($rb1-$rb0)/($vcolorcount-1);
	for ($i = 0; $i < $vcolorcount; $i++) {
		$lr = (int)($lr0 + $lrinc*$i);
		$lg = (int)($lg0 + $lginc*$i);
		$lb = (int)($lb0 + $lbinc*$i);
		$startrgb = dec2rgb($lr, $lg, $lb);
		$rr = (int)($rr0 + $rrinc*$i);
		$rg = (int)($rg0 + $rginc*$i);
		$rb = (int)($rb0 + $rbinc*$i);
		$endrgb = dec2rgb($rr, $rg, $rb);
		$grad .= "<tr>".colorGradient1D($startrgb, $endrgb, $eachheight, $width, $hcolorcount, $jsname, "h")."</tr>\r\n";
	}
	if (($slack=($height-$vcolorcount*$eachheight)) > 0) {
		$grad .= "<tr>".colorGradient1D($lbcolor, $rbcolor, $slack, $width, $hcolorcount, $jsname, "h")."</tr>\r\n";
	}
	$grad .= "</table>";

	return $grad;
} 

function color_navigation_bar($height, $width, $linecount, $jsname) {
	global $trans_image_path;

	$color_stages = array("FF0000", "FFFF00", "00FF00", "00FFFF", "0000FF", "FF00FF"); // red, yellow, green, cyan, blue, magnet
	$total_color_stage = count($color_stages);

	$height1 = (int)($height / 6);
	$linecount1 = (int)($linecount / 6);
	$inc1 = (int)(255 / ($linecount1-1));

	$bar = "";
	$bar .= "<table border=0 cellpadding=0 cellspacing=0>\r\n";
	for ($i = 0; $i < $total_color_stage; $i++) {
		$bar .= colorGradient1D($color_stages[$i%$total_color_stage], $color_stages[($i+1)%$total_color_stage], $height1, $width, $linecount1, $jsname);
	}

	if (($slack=($height-6*$height1)) > 0) {
		$hex = "FF0000";
		$bar .= "  <tr><td bgcolor=\"#$hex\"><a href=\"javascript:$jsname('$hex');\"><img border=0 src=\"$trans_image_path\" alt=\"$hex\" height=$slack width=$width></a></td></tr>\r\n";
	}
	$bar .= "</table>";

	return $bar;
} 

function color_palette($rgb) {
	global $trans_image_path;														
	global $lg_color_preview, $lg_ok, $lg_cancel;							
	global $formname, $rgbinput, $colorpreview;
	global $navbarHeight, $navbarWidth, $navbarColorCount;		
	global $gradLTColor, $gradRTColor, $gradLBColor, $gradRBColor, $gradHeight, $gradWidth, $gradVColorCount, $gradHColorCount;
	global $image_path, $intranet_session_language;
	global $PATH_WRT_ROOT, $LAYOUT_SKIN, $linterface, $button_submit, $button_close;

	$pal = "<br>";
	$pal = $pal."<table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\r\n";
	$pal = $pal."<form action=\"#\" name=\"frmColorPalette\">\r\n";
	$pal = $pal."	<tr>\r\n";
	// color gradient
	$pal = $pal."		<td>".colorGradient2D($gradLTColor, $gradRTColor, $gradLBColor, $gradRBColor, $gradHeight, $gradWidth, $gradVColorCount, $gradHColorCount, "selectRGB")."</td>\r\n";
	// spacing
	$pal = $pal."		<td><img border=0 src=\"".$trans_image_path."\" height=1 width=10></td>\r\n";

	$pal = $pal."		<td valign=\"top\">".color_navigation_bar($navbarHeight, $navbarWidth, $navbarColorCount, "previewColor")."</td>\r\n";
	$pal = $pal."	</tr>\r\n";
	// color preview
	$pal = $pal."	<tr><td colspan=3><img border=0 src=\"$trans_image_path\" height=10 width=1></td></tr>\r\n";
	$pal = $pal."	<tr>\r\n";
	$pal = $pal."		<td colspan=3>\r\n";
	$pal = $pal."			<table border=0 cellpadding=0 cellspacing=0>\r\n";
	$pal = $pal."				<tr>\r\n";
	$pal = $pal."					<td valign=\"middle\" class=\"tabletext\">$lg_color_preview</td>\r\n";
	$pal = $pal."					<td valign=\"middle\">&nbsp;<img border=0 name=\"color_preview\" src=\"./color_tile.php?rgb=$rgb\" height=30 width=30 alt=\"$rgb\"></td>\r\n";
	$pal = $pal."				</tr>\r\n";
	$pal = $pal."			</table>\r\n";
	$pal = $pal."		</td>\r\n";
	$pal = $pal."	</tr>\r\n";
	// function buttons
	$pal = $pal."	<tr><td colspan=3><img border=0 src=\"".$trans_image_path."\" height=10 width=1></td></tr>\r\n";
        $pal = $pal."
        <tr>
        	<td colspan=\"3\">        
                        <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"5\" align=\"center\">
                        <tr>
                        	<td class=\"dotline\"><img src=\"".$image_path."/".$LAYOUT_SKIN ."/10x10.gif\" width=\"10\" height=\"1\" /></td>
                        </tr>
                        <tr>
        			<td align=\"center\">".
                                $linterface->GET_ACTION_BTN($button_submit, "submit", "confirmColor();","submit2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") . "&nbsp;". 
                                $linterface->GET_ACTION_BTN($button_close, "button", "self.close();","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") 
                                ."</td>
        		</tr>
                        </table>                                
        	</td>
        </tr>
        ";

	// hidden value(s)
	$pal = $pal."	<input type=\"hidden\" name=\"formname\" value=\"$formname\">\r\n";
	$pal = $pal."	<input type=\"hidden\" name=\"rgbinput\" value=\"$rgbinput\">\r\n";
	$pal = $pal."	<input type=\"hidden\" name=\"colorpreview\" value=\"$colorpreview\">\r\n";
	$pal = $pal."	<input type=\"hidden\" name=\"rgb\" value=\"$rgb\">\r\n";
	$pal = $pal."</form>\r\n";
	$pal = $pal."</table>";
	
	return $pal;
} // end of function color_palette

$rgb = fixrgb($rgb);

//if (!eregi("^[0-F]{6}$", $rgb)) die();
if (!preg_match("/^[0-F]{6}$/i", $rgb)) die();

$gradRTColor = $rgb;
?>

<html>
<head>
	<title><? echo $i_Sports_Color_Palatte; ?></title>
        <link href="<?=$PATH_WRT_ROOT?>templates/<?=$LAYOUT_SKIN?>/css/content.css" rel="stylesheet" type="text/css" />
        <META http-equiv=Content-Type content='text/html; charset=UTF-8'>
        <style type="text/css">
	body {font-family:Verdana; font-size:8pt; background-color:#FFFFFF; background-image: none;}
	</style>
</head>

<body bgcolor="#FFFFFF" marginheight="0" marginwidth="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0">
<?
echo color_palette($rgb);
?>
</body>

</html>

<script language="javascript">
function previewColor(rgb_value) {
	with (document.frmColorPalette) {
		rgb.value = rgb_value;
		submit();
	}
} // end of function previewColor

function selectRGB(rgb_value) {
	with (document.frmColorPalette) {
		rgb.value = rgb_value;
		color_preview.src = "./color_tile.php?rgb="+rgb_value+"&nocache"+Math.random();
		color_preview.alt = rgb_value;
	}
} // end of function selectRGB

<?
$js = "";
$js .= "function confirmColor() {\r\n";
$js .= "	var objParentForm = window.opener.document.forms[\"$formname\"];\r\n";
if ($rgbinput != "") {
	$js .= "	objParentForm.elements['$rgbinput'].value = document.frmColorPalette.rgb.value;\r\n";
}

if ($colorpreview != "") {

	//$js .= "	window.opener.document.getElementById('$colorpreview').bgColor= document.frmColorPalette.rgb.value;\n";
	$js .= "	window.opener.document.getElementById('$colorpreview').style.backgroundColor=  '#'+document.frmColorPalette.rgb.value;\n";
}

$js .= "	window.close();\r\n";
$js .= "} // end of function confirmColor\r\n";

echo $js;
?>
</script>
