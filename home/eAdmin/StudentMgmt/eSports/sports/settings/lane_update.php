<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libsports.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$lsports = new libsports();
$lsports->authSportsSystem();

$laneNum = $_POST['laneNum'];

#added by marcus 6/7/2009
#gen default lane arrangement (e.g. 5,3,1,2,4,6 for 6 lane)
$mid=floor($laneNum/2);
$pos=array();
for($i=1;$i<=$laneNum;$i++)
{
	if($i%2==1)
		array_unshift($pos,$i);
	else
		array_push($pos,$i);
}
$defaultLandArangement= implode($pos,",");

$lsports->numberOfLanes = $laneNum;
$lsports->defaultLaneArrangement = $defaultLandArangement;
$lsports->saveSettings();

intranet_closedb();
header("Location: lane.php?xmsg=update");
?>