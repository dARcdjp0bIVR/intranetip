<?php
# using: 

########## Change Log [Start] ############
#
#	Date:   2019-05-14  Bill
#           prevent SQL Injection
#
#   Date:   2019-04-11  Bill    [2019-0301-1144-56289]
#           Create File
#           Copy logic from tf_record_update.php
#
########## Change Log [End] ############

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libsports.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

### Handle SQL Injection + XSS [START]
$eventGroupID = IntegerSafe($eventGroupID);
$eventType = IntegerSafe($eventType);
$roundType = IntegerSafe($roundType);
$currHeat = IntegerSafe($currHeat);
### Handle SQL Injection + XSS [END]

$lc = new libclass();

$lsports = new libsports();
$lsports->authSportsSystem();

# Get Event Group Extra Settings
$eventType = 4;
$ExtInfo = $lsports->retrieveEventGroupExtInfo($eventGroupID, $eventType);
$finalReq = $ExtInfo["FinalRoundReq"];
$numberOfLane = $lsports->numberOfLanes;

/*
if($eventType==EVENT_TYPE_FIELD)
{
    # clear old result of current heat
    $sql = "UPDATE SPORTS_LANE_ARRANGEMENT
                SET ResultMetre = NULL
                WHERE
                EventGroupID = $eventGroupID
                AND roundType = $roundType
                AND heat = $currHeat";
    $lsports->db_db_query($sql);
    
    #search for best result of this eventgroup
    $BestResult=$lsports->returnBestRecordResultOfFieldEventGroup($eventGroupID);
    
    if($BestResult)
    {
        $h_info=$lsports->retrieveStudentHouseInfo($BestResult[0]["StudentID"]);
        $student_name=$lsports->retrieveStudentName($BestResult[0]["StudentID"]);
        #Update New Record
        $sql = "	UPDATE
						SPORTS_EVENTGROUP_EXT_FIELD
					SET ";
        $sql .= "		NewRecordMetre = ".($BestResult[0]["ResultMetre"]?$BestResult[0]["ResultMetre"]:"NULL").",";
        $sql .= "		NewRecordHolderUserID = ".($BestResult[0]["StudentID"]?$BestResult[0]["StudentID"]:"NULL").",
						NewRecordHouseID = ".($h_info[0]["HouseID"]?$h_info[0]["HouseID"]:"NULL").",
						NewRecordHolderName = ".($student_name?"'$student_name'":"NULL")."
						WHERE
						EventGroupID = $eventGroupID ";
        $lsports->db_db_query($sql);
    }
    else
    {
        $lsports->removeNewRecordOfFieldEventGroup($eventGroupID);
    }
}
else
*/
{
    $table = " SPORTS_EVENTGROUP_EXT_RELAY ";
    $updateFields = " ResultMin = NULL, ResultSec = NULL, ResultMs = NULL ";
    
    # Clear old result of current heat
    $sql = "UPDATE
                SPORTS_CLASS_RELAY_LANE_ARRANGEMENT 
            SET
                $updateFields
            WHERE
                EventGroupID = '$eventGroupID' AND 
                roundType = '$roundType'
                heat = '$currHeat' ";
    $lsports->db_db_query($sql);
    
    # Search for best result of this event group
    $BestResult = $lsports->returnBestRecordResultOfClassRelayEvent($eventGroupID);
    if($BestResult)
    {
        /*
        $h_info=$lsports->retrieveStudentHouseInfo($BestResult[0]["StudentID"]);
        $student_name=$lsports->retrieveStudentName($BestResult[0]["StudentID"]);
        */
        
        $holderDisplayName = '';
        if($BestResult[0]["YearClassID"]) {
            $holderDisplayName = $lc->getClassName($BestResult[0]["YearClassID"]);
        }
        if($BestResult[0]["GroupTitle"] != '') {
            $holderDisplayName .= ' ('.$BestResult[0]["GroupTitle"].')';
        }
        
        # Upadte Student Record Status (to Record Broken)
        # Update New Record
        $sql = "UPDATE
                    $table
                SET 
                    NewRecordMin = ".($BestResult[0]["ResultMin"] > 0? $BestResult[0]["ResultMin"] : "0").",
    				NewRecordSec = ".($BestResult[0]["ResultSec"] > 0? $BestResult[0]["ResultSec"] : "0").",
    				NewRecordMs = ".($BestResult[0]["ResultMs"] > 0? $BestResult[0]["ResultMs"] : "0").",
                    NewRecordHolderName = ".($holderDisplayName != ''? $holderDisplayName : "NULL").",
    				NewRecordHouseID = NULL
    			WHERE
    				EventGroupID = '$eventGroupID' ";
        $lsports->db_db_query($sql);
    }
    else
    {
        $sql = "UPDATE
                    $table
                SET
                    NewRecordMin = NULL,
                    NewRecordSec = NULL,
                    NewRecordMs = NULL,
                    NewRecordHouseID = NULL,
                    NewRecordHolderName = NULL
                WHERE
                    EventGroupID = '$eventGroupID' ";
        $lsports->db_db_query($sql);
    }
}

$RankIsGenerated = 0;
// if($eventType == EVENT_TYPE_TRACK)
{
//  $record = $ExtInfo["RecordAsMS"];
    $record = ($ExtInfo["RecordMin"]*60*100) + ($ExtInfo["RecordSec"]*100) + $ExtInfo["RecordMs"];
    if($record <= 0) {
        $record = NULL;
    }
    
//  $standard = $ExtInfo["StandardAsMS"];
    $standard = ($ExtInfo["StandardMin"]*60*100) + ($ExtInfo["StandardSec"]*100) + $ExtInfo["StandardMs"];
}
/* 
else
{
    # check if it is high jump event
    $isHighJump = $lsports->Is_High_Jump($eventGroupID);
    
    $record = $ExtInfo["RecordMetre"];
    $standard = $ExtInfo["StandardMetre"];
}
 */

$arrange = $lsports->returnClassRelayLaneArrangeDetailByEventGroupID($eventGroupID, $roundType);
for($i=0; $i<sizeof($arrange); $i++)
{
    list($heat, $order, $cgid, $tmp_rank, $tmp_score, $tmp_trackresult, $result_min1, $result_sec1, $result_ms1, $result_status, $cid, $c_name, $c_cname, $c_ename, $cg_title, $absent_reason) = $arrange[$i];
    if ($heat != $currHeat) continue;
    
//  if($eventType == EVENT_TYPE_TRACK)
    {
        $result_min = (${"result_min_".$heat."_".$order}=="") ? 0 : IntegerSafe(${"result_min_".$heat."_".$order});
        $result_sec = (${"result_sec_".$heat."_".$order}=="") ? 0 : IntegerSafe(${"result_sec_".$heat."_".$order});
        $result_ms = (${"result_ms_".$heat."_".$order}=="") ? 0 : IntegerSafe(${"result_ms_".$heat."_".$order});
        
        $resultRecord = $lsports->Convert_TimeArr_To_Millisecond(array($result_min,$result_sec,$result_ms));
        $resultRecordArr[$cgid] = $resultRecord;
    }
    /* 
    else
    {
        $resultRecord = (${"record_".$heat."_".$order}=="") ? 0 : ${"record_".$heat."_".$order};
        
        if($isHighJump==1)
        {
            $tie_breaker_rank[$sid] = $resultRecord==0?0:${"tie_breaker_".$heat."_".$order};
        }
    }
     */
    
    $other = IntegerSafe(${"other_".$heat."_".$order});
    
    $tmpAttend = IntegerSafe(${"attend_".$heat."_".$order});
    if($resultRecord == 0 && $tmpAttend == 1 && $other != 4)
    {
        if($roundType == ROUND_TYPE_FIRSTROUND) {
            $attend = 1;
        }
        else {
            $attend = 3;
        }
    }
    else
    {
        $attend = IntegerSafe(${"attend_".$heat."_".$order});
    }
    $absent_reason = ${"attend_".$heat."_".$order."_reason"}? intranet_htmlspecialchars(${"attend_".$heat."_".$order."_reason"}) : "N.A.";
    
    $group_name = $lc->getClassName($cid);
    $group_name .= $cg_title == ''? '' : "($cg_title)";
    
    # Create a associate array
//  if($eventType == EVENT_TYPE_TRACK)
    {
        $resultArr[$cgid]["min"] = $result_min;
        $resultArr[$cgid]["sec"] = $result_sec;
        $resultArr[$cgid]["ms"] = $result_ms;
        $resultArr[$cgid] = $lsports->Pad_TimerArr($resultArr[$cgid]);
    }
    /* 
    else
    {
        $resultArr[$sid]["metre"] = $resultRecord;
        for($try=0; $try<3; $try++) {
            $resultArr[$sid]["trial".($try+1)] =  ${"record_trial_".$order}[$try];
        }
    }
     */
    $resultArr[$cgid]["group_name"] = $group_name;
    $resultArr[$cgid]["attend"] = $attend;
    $resultArr[$cgid]["other"] = $other;
    $resultArr[$cgid]["absent_reason"] = $absent_reason;
    
    if($resultRecord != 0 && $resultArr[$cgid]["other"] != 4 && $resultArr[$cgid]["other"] != 5 && $attend==1)
    {
        $Ranking[$cgid] = $resultRecord;
    }
}

if($Ranking != null)
{
    # Sort the Ranking Array
//  if($eventType == EVENT_TYPE_TRACK)
        asort($Ranking);
    /* 
    else
        arsort($Ranking);
     */
    
    $temp_rank = 0;
    $last_rank = 0;
    $ctr = 0;
    foreach((array)$Ranking as $cgid => $value)
    {
        /* 
        if($eventType == 2)
        {
            // High Jump
            if($tie_breaker_rank[$sid]!=null)
            {
                $resultArr[$sid]["rank"] = $tie_breaker_rank[$sid];
            }
            // Other Fields
            else{
                $r = 1;
                foreach($Ranking as $s =>$v)
                {
                    if($v>$Ranking[$sid])
                    {
                        $r++;
                    }
                }
                $tie_breaker_rank[$sid] = $r;
                $resultArr[$sid]["rank"]=$r;
            }
        }
        else //track assign rank
         */
        {
            //modified by marcus 20091113
            # $temp_rank++;
            # $resultArr[$cgid]["rank"] = $temp_rank;
            if($lsports->RankPattern=="1223")
            {
                if($last_cgid != "" && $resultRecordArr[$cgid] == $resultRecordArr[$last_cgid])     // check if record same as neighbour
                {
                    $resultArr[$cgid]["rank"] = $temp_rank;
                }
                else
                {
                    $resultArr[$cgid]["rank"] = (++$temp_rank);
                }
                $last_cgid = $cgid;
            }
            else
            {
                ++$temp_rank;
                if($last_cgid != "" && $resultRecordArr[$cgid] == $resultRecordArr[$last_cgid])      // check if record same as neighbour
                {
                    $resultArr[$cgid]["rank"] = $last_rank;
                }
                else
                {
                    $resultArr[$cgid]["rank"] = $temp_rank;
                    $last_rank = $temp_rank;
                }
                $last_cgid = $cgid;
            }
        }
        
        if($resultArr[$cgid]["other"] != 4 && $resultArr[$cgid]["other"] != 5 && $resultArr[$cgid]["other"] != 3)
        {
            # Compare the result record with the broken record and standard record
//          if($eventType == 1)
            {
                # Retrieve the updated new record for compare
                $sql = "SELECT NewRecordMin, NewRecordSec, NewRecordMs FROM SPORTS_EVENTGROUP_EXT_RELAY Where EventGroupID = '$eventGroupID'";
                $newRecord = $lsports->returnArray($sql, 3);
                
                $new = ($newRecord[0][0]*60*100) + ($newRecord[0][1]*100) + $newRecord[0][2];
                $check_record = $new==0? $record : $new;
                
                # modified by Marcus 20091209 (change "<=" to "<" , same as Record should not be counted as Record Broken)
                if($value < $check_record || !$check_record)
                {
                    $resultArr[$cgid]["status"] = 4;
                    
                    $cg_name = $resultArr[$cgid]["group_name"];
                    $newRecordMin = $resultArr[$cgid]["min"];
                    $newRecordSec = $resultArr[$cgid]["sec"];
                    $newRecordMs = $resultArr[$cgid]["ms"];
                    
                    # Update broken record time
                    # 20091207 YatWoon
                    # Fixed: if there are 2 student with broken records, only the 1st student can earn the broken score
                    # make sure only the first student is add in the record [Assumption]
                    $sql1 = "SELECT EventGroupID FROM SPORTS_EVENTGROUP_EXT_RELAY WHERE NewRecordMin = '$newRecordMin' AND NewRecordSec = '$newRecordSec' AND NewRecordMs = '$newRecordMs' AND EventGroupID = '$eventGroupID'";
                    $result1 = $lsports->returnVector($sql1);
                    if(empty($result1))
                    {
                        $sql = "UPDATE SPORTS_EVENTGROUP_EXT_RELAY SET NewRecordMin = '$newRecordMin', NewRecordSec = '$newRecordSec', NewRecordMs = '$newRecordMs', NewRecordHolderName = '$cg_name' WHERE EventGroupID = '$eventGroupID'";
                        $lsports->db_db_query($sql);
                    }
                }
                else if($value <= $standard && $standard)
                {
                    $resultArr[$cgid]["status"] = 3;
                }
                else
                {
                    $resultArr[$cgid]["status"] = 2;
                }
            }
            /*
            else
            {
                # Retrieve the updated new record for compare
                $sql = "SELECT NewRecordMetre FROM SPORTS_EVENTGROUP_EXT_FIELD Where EventGroupID = '$eventGroupID'";
                $newRecord = $lsports->returnArray($sql, 1);
                $check_record = ($newRecord[0][0]==""||$newRecord[0][0]==0) ? $record : $newRecord[0][0];
                
                if($value > $check_record)
                {
                    $resultArr[$sid]["status"] = 4;
                    
                    # get record year
                    $today = getdate();
                    $year = $today["year"];
                    
                    #get record House ID
                    $h_info = $lsports->retrieveStudentHouseInfo($sid);
                    $hid = $h_info[0][1];
                    
                    $sname = $resultArr[$sid]["student_name"];
                    $newRecordMetre = $resultArr[$sid]["metre"];
                    
                    # udpate broken record metre
                    $sql = "UPDATE SPORTS_EVENTGROUP_EXT_FIELD SET NewRecordMetre = '$newRecordMetre', NewRecordHolderName = '$sname', NewRecordHolderUserID = '$sid', RecordHouseID = '$hid' WHERE EventGroupID = '$eventGroupID'";
                    
                    $lsports->db_db_query($sql);
                    
                }
                else if($value >= $standard && $standard)
                {
                    $resultArr[$sid]["status"] = 3;
                }
                else
                {
                    $resultArr[$sid]["status"] = 2;
                }
            }
             */
        }
        else
        {
            $resultArr[$cgid]["status"] = 2;
        }
    }
}
else
{
    if($other == "4") {
        $resultArr[$cgid]["status"] = 5;
    }
    else if($other == "5") {
        $resultArr[$cgid]["status"] = 1;
    }
}

###############################
# value for selection $order
# 1 RecordBroken
# 2 Qualified
# 3 Unsuitable
# 4 Foul
# 5 Not Attend
# 0 - -
###############################

###############################
# value for STATUS in DB
# 4 RecordBroken
# 3 Qualified
# 2 Unsuitable
# 5 Foul
# 1 Not Attend
###############################

# Update Result Records
foreach((array)$resultArr as $cgid => $value)
{
    if($value["status"]=="")
    {
        if($value["attend"] == 2) {
            $value["status"] = RESULT_STATUS_ABSENT;
        }
        else if($value["attend"] == 3) {
            $value["status"] = RESULT_STATUS_WAIVEDABSENT;
        }
        else if($value["other"] == 4) {
            $value["status"] = RESULT_STATUS_FOUL;
        }
        else {
            $value["status"] = RESULT_STATUS_PRESENT;
        }
    }
    
//  if($eventType == EVENT_TYPE_TRACK) //track
    {
        $fields = "ResultMin = '".$value['min']."', ";
        $fields .= "ResultSec = '".$value['sec']."', ";
        $fields .= "ResultMs = '".$value['ms']."', ";
        $fields .= "RecordStatus = ".$value['status'].", ";
        $fields .= "AbsentReason = '".$value['absent_reason']."', ";
       
        if($value['attend'] == 1)
        {
            if($roundType==ROUND_TYPE_FINALROUND) //modified by marcus 16/6
            {
                $RankIsGenerated = 1;
                $fields .= ($value["status"] != 5 && $value["status"] != 1)? "Rank = '".$value['rank']."', " : "Rank = NULL, ";
            }
            else
            {
                $fields .= "Rank = NULL, ";
            }
        }
        else
        {
            $fields .= "Rank = NULL, ";
        }
        $fields .= "DateModified = now()";
    }
    /* 
    else //field
    {
        $fields = "ResultMetre = '".$value['metre']."', ";
        $fields .= "ResultTrial1 = '".$value['trial1']."', ";
        $fields .= "ResultTrial2 = '".$value['trial2']."', ";
        $fields .= "ResultTrial3 = '".$value['trial3']."', ";
        $fields .= "RecordStatus = ".$value['status'].", ";
        $fields .= "AbsentReason = '".$value['absent_reason']."', ";
        
        //if(($roundType==0 && $value['attend']==1) || ($roundType==1  && $total_heat_num==1 && $value['attend']==1))
        if($roundType==ROUND_TYPE_FINALROUND)
        {
            $fields .= ($value["status"]!=5 && $value["status"]!=1) ? "Rank = '".$value['rank']."', " : "Rank = NULL, ";
        }
        else
            $fields .= "Rank = NULL, ";
            $fields .= "DateModified = now()";
    }
     */   
    
    $sql = "UPDATE SPORTS_CLASS_RELAY_LANE_ARRANGEMENT SET $fields WHERE EventGroupID = '$eventGroupID' AND ClassGroupID = '$cgid' AND Heat = '$currHeat' AND RoundType = '$roundType'";
    $lsports->db_db_query($sql);
    
    ##########################
    # Direct go to final
    if(isset($directFinal) && $directFinal=="1")
    {
        # Get results of first round
        $sqlFirst = "SELECT 
                        EventGroupID, 0 as RoundType, ClassID, ClassGroupID, Heat, ArrangeOrder, ResultMin, ResultSec, ResultMs, RecordStatus, Rank, DateModified
                    FROM 
                        SPORTS_CLASS_RELAY_LANE_ARRANGEMENT 
                    WHERE 
                        EventGroupID = '$eventGroupID' AND ClassGroupID = '$cgid' AND Heat = '$currHeat' AND RoundType = '$roundType'";
        $resultFirst = $lsports->returnArray($sqlFirst,11);
        
        # Delete results of Final Round
        $sqlDelFinal = "DELETE FROM SPORTS_CLASS_RELAY_LANE_ARRANGEMENT WHERE EventGroupID = '$eventGroupID' AND ClassGroupID = '$cgid' AND Heat = '$currHeat' AND RoundType = '0'";
        $lsports->db_db_query($sqlDelFinal);
        
        # Insert results of First Round to Final Round
        $sqlFinal = "INSERT INTO SPORTS_CLASS_RELAY_LANE_ARRANGEMENT";
        $sqlFinal .= " (EventGroupID, RoundType, ClassID, ClassGroupID, Heat, ArrangeOrder, ResultMin, ResultSec, ResultMs, RecordStatus, Rank, DateModified) VALUES(";
        for($j=0; $j<count($resultFirst[0]); $j++) {
            if(trim($resultFirst[0][$j]) == '')   continue;
            
            $sqlFinal .= "'".$resultFirst[0][$j]."'";
            if($j == count($resultFirst[0]) - 1) {
                $sqlFinal .= ")";
            } else {
                $sqlFinal .= ",";
            }
        }
        
        $lsports->db_db_query($sqlFinal);
    }
}

#############################
# Calculate the Attendant Score of Student and enrolment score
// if($roundType==1)
// {
    # return attendant score standard
    $scoreEnrol = $lsports->scoreEnrol;
    $scorePresent = $lsports->scorePresent;
    $scoreAbsent = $lsports->scoreAbsent;
    
    foreach((array)$resultArr as $cgid => $value)
    {
        # Retrieve record status of student in the first round (only first round will count the attendance score)
        $sql = "SELECT RecordStatus FROM SPORTS_CLASS_RELAY_LANE_ARRANGEMENT WHERE EventGroupID = '$eventGroupID' AND ClassGroupID = '$cgid' AND RoundType = '$roundType'";
        $row = $lsports->returnVector($sql);
        $record_status = $row[0];
        
        # Calculate the attendance scores
        if($resultArr[$cgid]['attend'] != NULL && $resultArr[$cgid]['attend'] != "")
        {
            $score = 0;
            if($roundType==ROUND_TYPE_FIRSTROUND)
            {
                $score += $scoreEnrol;
                if($resultArr[$cgid]['attend'] == 1)
                {
                    $score += $scorePresent;
                }
                
                if($resultArr[$cgid]['attend'] == 2)
                {
                    $score = $score - $scoreAbsent;
                }
            }
            
            $sql = "UPDATE SPORTS_CLASS_RELAY_LANE_ARRANGEMENT SET Score = '$score' WHERE EventGroupID = '$eventGroupID' AND ClassGroupID = '$cgid' AND RoundType = '$roundType'";
            $lsports->db_db_query($sql);
            
            # Update Final Round also (when number of participants <=lane number)
            if(isset($directFinal) && $directFinal == "1") {
                $sql2 = "UPDATE SPORTS_CLASS_RELAY_LANE_ARRANGEMENT SET Score = '$score' WHERE EventGroupID = '$eventGroupID' AND ClassGroupID = '$cgid' AND RoundType = 0";
                $lsports->db_db_query($sql2);
            }
        }
    }
// }

########## Calculate Score of Record Broken or Standard Record ##################
if($Ranking!=null)
{
    $ranked_cgid = array_keys($Ranking);
    
    # Retrieve score standard
    $sql = "SELECT ScoreStandardID FROM SPORTS_EVENTGROUP WHERE EventGroupID = '$eventGroupID'";
    $temp = $lsports->returnVector($sql);
    $ScoreStandardDetail = $lsports->retrieveScoreStandardDetail($temp[0]);
    
    ######### Customization [Start] ##########
    # 2009-12-07 YatWoon
    # if there is 3 students = rank 1, then the score need to be avg of (1st + 2nd + 3rd's score)
    # assumption, they select the ranking display should be 1,2,2,4
    if($sys_custom['eSports_same_position_avg_score'] && $lsports->RankPattern!="1223")
    {
        $rank_couting = array();
        foreach($resultArr as $k=>$d)
        {
            if($d['rank'])
            {
                if($rank_couting[$d['rank']]) {
                    $rank_couting[$d['rank']]++;
                } else {
                    $rank_couting[$d['rank']] = 1;
                }
            }
        }
        
        foreach($rank_couting as $k=>$d)
        {
            $temp = 0;
            for($i=$k;$i<$k+$d;$i++) {
                $temp += $ScoreStandardDetail[$i];
            }
            $ScoreStandardDetail[$k] = my_round($temp / $d, 1);
        }
    }
    ######### Customization [End] ##########
    
    # 20081022 - fix Rank 9 and 10 will calculated with "Record Broken" and "Qualified"
    $RecordBroken = $ScoreStandardDetail[9];
    
    # 2012-03-21
    # check this event have standard recod setting or not, if not, then no need add Qualified score to student
    $Qualified = $standard ? $ScoreStandardDetail[10] : 0;
    
    # clear the "Record Broken" and "Qualified" score in array
    $ScoreStandardDetail[9] = 0;
    $ScoreStandardDetail[10] = 0;
    
    # clear old Score
    $ranked_cgid_sql = implode(",",(array)$ranked_cgid);
    
    $sql = "UPDATE SPORTS_CLASS_RELAY_LANE_ARRANGEMENT set Score = 0 WHERE EventGroupID = '$eventGroupID' and RoundType = 0 AND ClassGropuID IN ($ranked_cgid_sql)";
    $lsports->db_db_query($sql);
    foreach($ranked_cgid as $key => $cgid)
    {
        $score = 0;
        
        ///// Record broken
        if($resultArr[$cgid]['status'] == RESULT_STATUS_RECORDBROKEN)
        {
            $score = $score + $RecordBroken;
            
            # 2013-08-28
            # fixed: change back old record breaker's status and score
            $sql = "SELECT ClassGroupID, RoundType, ResultMin, ResultSec, ResultMS, Score FROM SPORTS_CLASS_RELAY_LANE_ARRANGEMENT WHERE EventGroupID = '$eventGroupID' AND RecordStatus = 4 AND ClassGroupID != '$cgid'
                        UNION SELECT ClassGroupID, RoundType, ResultMin, ResultSec, ResultMS, Score FROM SPORTS_CLASS_RELAY_LANE_ARRANGEMENT WHERE EventGroupID = '$eventGroupID' AND RecordStatus = 4 AND ClassGroupID = '$cgid' AND RoundType != '$roundType'";
            $oldBreakRecordresultArr = $lsports->returnArray($sql);
            foreach ((array)$oldBreakRecordresultArr as $data)
            {
                $thisClassGroupID = $data['ClassGroupID'];
                $thisRoundType = $data['RoundType'];
                $thisScore = $data['Score'];
                
                /* 
                if ($eventType==EVENT_TYPE_FIELD) {
                    $thisResultRecord = $data['ResultMetre'];
                    $thisRecordStatus = ($thisResultRecord >= $standard && $standard) ? 3 : 2;
                }
                else
                 */
                {
                    $thisResultRecord = $lsports->Convert_TimeArr_To_Millisecond(array($data['ResultMin'], $data['ResultSec'], $data['ResultMS']));
                    $thisRecordStatus = ($thisResultRecord <= $standard && $standard) ? 3 : 2;
                }
                $thisScore = $thisScore - $RecordBroken;
                
                $sql = "UPDATE SPORTS_CLASS_RELAY_LANE_ARRANGEMENT SET Score = '$thisScore', RecordStatus = '$thisRecordStatus' WHERE EventGroupID = '$eventGroupID' AND RecordStatus = 4 AND ClassGroupID = '$thisClassGroupID' AND RoundType = '$thisRoundType'";
                $lsports->db_db_query($sql);
            }
        }
        
        // avoid double count present / enrol score
        $ObtainedBonusScore = $lsports->Get_Obtained_Bonus_Score_Class_Relay($roundType, $eventGroupID, $cgid);
        
        # Calculate the score by checking the status
        $score = $score + $scoreEnrol + $scorePresent;
        
        /// Qualified
        if(($resultArr[$cgid]['status'] == RESULT_STATUS_RECORDBROKEN || $resultArr[$cgid]['status'] == RESULT_STATUS_QUALIFY))
        {
            $score = $score + $Qualified;
        }
        
        if($score <= $ObtainedBonusScore)
        {
            $score = 0;
        }
        else
        {
            $score = $score - $ObtainedBonusScore;
        }
        
        $fields = "";
        
        # Calculate Ranking and Scores if it is the final round
        if($roundType == 0 || (isset($directFinal) && $directFinal=="1"))
        {
            if($ScoreStandardDetail[$resultArr[$cgid]['rank']] != "")
            {
                $score = $score + $ScoreStandardDetail[$resultArr[$cgid]['rank']];
            }
        }
        
        $fields .= "Score = ".$score.", ";
        $fields .= "DateModified = now()";
        
        $sql = "UPDATE SPORTS_CLASS_RELAY_LANE_ARRANGEMENT SET $fields WHERE EventGroupID = '$eventGroupID' AND ClassGroupID = '$cgid' AND RoundType = '$roundType'";
        $lsports->db_db_query($sql);
        
        if(isset($directFinal) && $directFinal == "1") {
            $sql2 = "UPDATE SPORTS_CLASS_RELAY_LANE_ARRANGEMENT SET $fields WHERE EventGroupID = '$eventGroupID' AND ClassGroupID = '$cgid' AND RoundType ='0'";
            $lsports->db_db_query($sql2);
        }
    }
    
    if(($roundType==1 && $finalReq==0) || $directFinal) // gen ranking for event not require final
    {
        if($roundType==1 && $finalReq==0)
        {
            $sql = "DELETE FROM SPORTS_CLASS_RELAY_LANE_ARRANGEMENT WHERE EventGroupID = '$eventGroupID' AND RoundType = '0'";
            $lsports->db_db_query($sql);
        }
        $sql = "SELECT COUNT(*) AS SUM FROM SPORTS_CLASS_RELAY_LANE_ARRANGEMENT WHERE EventGroupID = '$eventGroupID' and RecordStatus IS NULL ";
        $result = $lsports->returnArray($sql,1);
        
        if($result[0]["SUM"] == 0)  // Check whether all heats are finished
        {
//          if($eventType == 1)
            {
                $eventresult = " ResultMin*60*100+ResultSec*100+ResultMS ";
                $table = " SPORTS_CLASS_RELAY_LANE_ARRANGEMENT ";
                $orderBy = " ASC ";
            }
            /* 
            else
            {
                $eventresult = " ResultMetre ";
                $table = " SPORTS_LANE_ARRANGEMENT ";
                $orderBy = " DESC ";
            }
             */
            
            $sql = "SELECT ClassGroupID, $eventresult AS EventResult, RecordStatus FROM SPORTS_CLASS_RELAY_LANE_ARRANGEMENT";
            $sql .= " WHERE EventGroupID = '$eventGroupID' AND RecordStatus IN (2,3,4) ";
            $sql .= " AND ((ResultMin*60*100+ResultSec*100+ResultMS)>0 OR ResultMetre>0)";
            $sql .= " ORDER BY ($eventresult) $orderBy";
            $result = $lsports->returnArray($sql,8);
            
            $ctr=0;
            $temp_rank = 0;
            
            if(!empty($result))
            {
                for($k=0; ($k < sizeof($result)+$ctr&&$k<8) || ($result[$k]['EventResult'] == $result[$k-1]['EventResult'] && !empty($result[$k]['EventResult'])); $k++)
                {
                    list($cgid, $time, $status) = $result[$k];
                    if(!$cgid) break;
                    
                    switch($status)
                    {
                        case 2: $score = $scoreEnrol + $scorePresent; break;
                        case 3: $score = $scoreEnrol + $Qualified + $scorePresent ; break;
                        case 4: $score = $scoreEnrol + $Qualified + $scorePresent + $RecordBroken ;break;
                    }
                    
                        if($lsports->RankPattern == "1223")
                        {
                            if($last_cgid !== "" && $result[$k]['EventResult'] == $result[$last_cgid]['EventResult'])   // check if record same as neighbour
                            {
                                $rank = $temp_rank;
                            }
                            else
                            {
                                $rank = (++$temp_rank);
                            }
                            $last_cgid = $k;
                        }
                        else
                        {
                            ++$temp_rank;
                            if($last_cgid !== "" && $result[$k]['EventResult'] == $result[$last_cgid]['EventResult'])   // check if record same as neighbour
                            {
                                $rank = $last_rank;
                            }
                            else
                            {
                                $rank = $temp_rank;
                                $last_rank = $temp_rank;
                            }
                            $last_cgid = $k;
                        }
                        
                        if($ScoreStandardDetail[$rank] != "") {
                            $score += $ScoreStandardDetail[$rank];
                        }
                        $sql = "UPDATE SPORTS_CLASS_RELAY_LANE_ARRANGEMENT SET Rank = '$rank', Score = '$score' WHERE EventGroupID = '$eventGroupID' and ClassGroupID = '$cgid' ";
                        $lsports->db_db_query($sql);
                    }
                    
                    $RankIsGenerated = 1;
                }
            }
        }
    }

intranet_closedb();

$msg = $RankIsGenerated ? "sports_ranking_generated" : "update";
header ("Location:class_relay_group_record_detail.php?eventGroupID=$eventGroupID&xmsg=$msg");
?>