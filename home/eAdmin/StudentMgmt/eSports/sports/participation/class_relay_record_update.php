<?php
# using: 

########## Change Log [Start] ############
#
#   Date:   2018-10-29 (Ivan)   [2018-1026-1548-26207]
#           fixed: cannot set status to Foul
#
#   Date:   2018-10-15 (Bill)   [2018-0628-1634-01096]
#           handle score received and record status - Record Broken
#
#	Date:	2017-11-03 (Bill)
#			fixed: class relay score not correct (always the same)
#
########## Change Log [End] ############

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libsports.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$lsports = new libsports();
$lsports->authSportsSystem();

# Get Record & Standard Record
$eventType = 4;
$ExtInfo = $lsports->retrieveEventGroupExtInfo($eventGroupID, $eventType);
$record = ($ExtInfo["RecordMin"]*60*1000) + ($ExtInfo["RecordSec"]*1000) + $ExtInfo["RecordMs"];
$check_record = $record;

$standard = ($ExtInfo["StandardMin"]*60*1000) + ($ExtInfo["StandardSec"]*1000) + $ExtInfo["StandardMs"];

# [2018-0628-1634-01096] Get Class Relay related Info 
if($sys_custom['eSports']['KaoYipRelaySettings'])
{
    // Get Recod Broken Score received
    $sql = "SELECT ScoreStandardID FROM SPORTS_EVENTGROUP WHERE EventGroupID = '$eventGroupID'";
    $temp = $lsports->returnVector($sql);
    $ScoreStandardDetail = $lsports->retrieveScoreStandardDetail($temp[0]);
    $RecordBroken = $ScoreStandardDetail[9];
    
    // Get Event Group Details
    $eventGroupDetails = $lsports->retrieveEventGroupDetail($eventGroupID);
    $eventID = $eventGroupDetails['EventID'];
    $ageGroupID = $eventGroupDetails['GroupID'];
    
    // Get Event Details
    $eventDetails = $lsports->retrieveTrackFieldDetail($eventID);
    $eventRelayType = $eventDetails['RelayEventType'];
    $isCountRelayScore = $eventDetails['RelayRoundType'] == ROUND_TYPE_FINALROUND;
    
    // Get Related Event Groups
    $relatedEventGroupIDs = $lsports->returnRelatedClassRelayEvent($eventRelayType, $ageGroupID);
}

# Clear New Record
$table = "SPORTS_EVENTGROUP_EXT_RELAY";
$updateFields = "NewRecordMin = NULL, NewRecordSec = NULL, NewRecordMs = NULL ";
//$cond = $sys_custom['eSports']['KaoYipRelaySettings']? " EventGroupID IN ('".implode("', '", $relatedEventGroupIDs)."')" : " EventGroupID = '$eventGroupID' ";
//$orderType = "ASC";
$sql = " UPDATE $table SET $updateFields WHERE EventGroupID = '$eventGroupID'";
$lsports->db_db_query($sql);

# [2018-0628-1634-01096] Get best result of this relay type
if($sys_custom['eSports']['KaoYipRelaySettings'])
{
    // Get Best Result [OLD]
    $check_record = $lsports->returnOldBestResultOfClassRelayEventType($relatedEventGroupIDs);
    
    // Get Best Result [CURRENT]
    $BestResult = $lsports->returnBestRecordResultOfClassRelayEventType($relatedEventGroupIDs, $eventGroupID);
    if($BestResult)
    {
        include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
        $yc = new year_class($BestResult[0]['ClassID']);
        $class_name = $yc->Get_Class_Name();
        
        $sql = "UPDATE
                    SPORTS_EVENTGROUP_EXT_RELAY 
                SET 
                    NewRecordMin = '".$BestResult[0]['ResultMin']."',
                    NewRecordSec = '".$BestResult[0]['ResultSec']."',
                    NewRecordMs = '".$BestResult[0]['ResultMs']."', 
                    NewRecordHolderName = '$class_name'
                WHERE
                    EventGroupID = '$eventGroupID'";
        $lsports->db_db_query($sql);
    }
}

$arrangeDetail = $lsports->returnCRLaneArrangeDetailByEventGroupID($eventGroupID);
for($i=0; $i<sizeof($arrangeDetail); $i++)
{
	list($cname_en,$cname_b5, $order, $cid) = $arrangeDetail[$i];
	$cname = Get_Lang_Selection($cname_en,$cname_b5);
	
	$result_min = (${"result_min_".$order}=="")?0:${"result_min_".$order};
	$result_sec = (${"result_sec_".$order}=="")?0:${"result_sec_".$order};
	$result_ms = (${"result_ms_".$order}=="")?0:${"result_ms_".$order};
	
	$result_min = (strlen($result_min)==1) ? "0".$result_min : $result_min;
	$result_sec = (strlen($result_sec)==1) ? "0".$result_sec : $result_sec;
	$result_ms = (strlen($result_ms)==1) ? "0".$result_ms : $result_ms;
	
	$result_record = ($result_min*60*1000) + ($result_sec*1000) + $result_ms;
	
	$resultArr[$order]["min"] = $result_min;
	$resultArr[$order]["sec"] = $result_sec;
	$resultArr[$order]["ms"] = $result_ms;
	
	$resultArr[$order]["other"] = ${"other_".$order};
	
	// [2018-0628-1634-01096] Store extra info
	$resultArr[$order]["className"] = $cname;
	$resultArr[$order]["classGroup"] = $arrangeDetail[$i]['ClassGroupID'];
	
	if($result_record != 0 && $resultArr[$order]["other"]!=4) {
		$Ranking[$order] = $result_record;
	}
}

if(sizeof($Ranking)==0)
{
	header ("Location:class_relay_record_detail.php?eventGroupID=$eventGroupID");
}

asort($Ranking);
$ranked_cid = array_keys($Ranking);

$rank = 0;
foreach($Ranking as $order => $result_record)
{
	$result_min = $resultArr[$order]["min"];
	$result_sec = $resultArr[$order]["sec"];
	$result_ms = $resultArr[$order]["ms"];
	
	$other = $resultArr[$order]["other"];
	if($other==0)
	{
		# Retrieve Updated New Record for Compare
		$sql = "SELECT NewRecordMin, NewRecordSec, NewRecordMs FROM SPORTS_EVENTGROUP_EXT_RELAY Where EventGroupID = '$eventGroupID'";
		$newRecord = $lsports->returnArray($sql, 3);
		
		# [2018-0628-1634-01096] Get best result of this relay type
// 		if($sys_custom['eSports']['KaoYipRelaySettings'])
// 		{
// 		    $sql = "SELECT NewRecordMin, NewRecordSec, NewRecordMs FROM SPORTS_EVENTGROUP_EXT_RELAY Where EventGroupID IN ('".implode("', '", (array)$relatedEventGroupIDs)."')
//                     ORDER BY (NewRecordMin*60*1000+NewRecordSec*1000+NewRecordMs) ASC LIMIT 0,1 ";
// 		    $newRecord = $lsports->returnArray($sql, 3);
// 		}
		
		$new = ($newRecord[0][0]*60*1000) + ($newRecord[0][1]*1000) + $newRecord[0][2];
		$check_record = ($new==0) ? $check_record : $new;
		if($result_record < $check_record)
		{
			$status = 4;
			$cname = $resultArr[$order]["className"];

			# Update Broken Record Time
			$sql = "UPDATE SPORTS_EVENTGROUP_EXT_RELAY SET NewRecordMin = '$result_min', NewRecordSec = '$result_sec', NewRecordMs = '$result_ms', NewRecordHolderName = '$cname' WHERE EventGroupID = '$eventGroupID'";
			$lsports->db_db_query($sql);
			
			# [2018-0628-1634-01096] Update best result of this relay type
			if($sys_custom['eSports']['KaoYipRelaySettings'])
			{
			    $cg_id = $resultArr[$order]["classGroup"];
			    
			    // Update best result
			    $sql = "UPDATE SPORTS_EVENTGROUP_EXT_RELAY SET NewRecordMin = '$result_min', NewRecordSec = '$result_sec', NewRecordMs = '$result_ms', NewRecordHolderName = '$cname' WHERE EventGroupID IN ('".implode("', '", (array)$relatedEventGroupIDs)."')";
			    $lsports->db_db_query($sql);
			    
			    // Change old record breaker's status and score
			    $sql = "SELECT
                            ClassID, ClassGroupID, EventGroupID, ResultMin, ResultSec, ResultMS, Score 
                        FROM 
                            SPORTS_CLASS_RELAY_LANE_ARRANGEMENT 
                        WHERE 
                            EventGroupID IN ('".implode("', '", (array)$relatedEventGroupIDs)."') AND RecordStatus = ".RESULT_STATUS_RECORDBROKEN." AND ClassGroupID != '$cg_id'
			            UNION 
                        SELECT 
                            ClassID, ClassGroupID, EventGroupID, ResultMin, ResultSec, ResultMS, Score 
                        FROM 
                            SPORTS_CLASS_RELAY_LANE_ARRANGEMENT 
                        WHERE 
                            EventGroupID IN ('".implode("', '", (array)$relatedEventGroupIDs)."') AND RecordStatus = ".RESULT_STATUS_RECORDBROKEN." AND ClassGroupID = '$cg_id' AND EventGroupID != '$eventGroupID'";
			    $oldBreakRecordresultArr = $lsports->returnArray($sql);
			    
			    foreach ((array)$oldBreakRecordresultArr as $data)
			    {
			        $thisClassGroupID = $data['ClassGroupID'];
			        $thisEventGroupID = $data['EventGroupID'];
			        $thisScore = $data['Score'];
			        
		            $thisResultRecord = $lsports->Convert_TimeArr_To_Millisecond(array($data['ResultMin'], $data['ResultSec'], $data['ResultMS']));
		            $thisRecordStatus = ($thisResultRecord <= $standard && $standard) ? RESULT_STATUS_QUALIFY : RESULT_STATUS_PRESENT;
		            
			        $thisScore = $thisScore - $RecordBroken;
			        $sql = "UPDATE SPORTS_CLASS_RELAY_LANE_ARRANGEMENT SET Score = '$thisScore', RecordStatus = '$thisRecordStatus' 
			                WHERE EventGroupID = '$thisEventGroupID' AND RecordStatus = ".RESULT_STATUS_RECORDBROKEN." AND ClassGroupID = '$thisClassGroupID'";
			        $lsports->db_db_query($sql);
			    }
			}
		}
		else if($result_record <= $standard)
		{
			$status = 3;
		}
		else
		{
			$status = 2;
		}
	}
	else if($other==1)
	{
		$status = 4;
	}
	else if($other==2)
	{
		$status = 3;
	}
	else if($other==3)
	{
		$status = 2;
	}
	
	$resultArr[$order]["status"] = $status;
	
	// add by marcus 17/6
	$rank++;
	if($last_cid!="" && $Ranking[$last_cid]==$result_record) 	// Check if record same as neighbour
	{
		$resultArr[$order]["rank"] = $last_rank;
	}
	else
	{
		$resultArr[$order]["rank"] = $rank;
		$last_rank = $rank;
	}
	$last_cid = $order;
}

# Retrieve Score Standard 
$sql = "SELECT ScoreStandardID FROM SPORTS_EVENTGROUP WHERE EventGroupID = '$eventGroupID'";
$temp = $lsports->returnVector($sql);
$ScoreStandardDetail = $lsports->retrieveScoreStandardDetail($temp[0]);

foreach($resultArr as $order => $result_record)
{
	/* modified by marcus 17/6
	for($i=0; $i<sizeof($ranked_cid); $i++)
	{
		if($cid == $ranked_cid[$i])
		{
			$rank = $i+1;
			$result_record["rank"] = $rank;
			break;
		}
	}

	$result_record["rank"] = ($result_record["rank"]=="") ? "NULL" : $result_record["rank"];
	*/
	
	// Assign Rank
	$result_record["rank"] = $resultArr[$order]["rank"];

	# Calculate Score by Ranking
	if($ScoreStandardDetail[$result_record["rank"]]!="") {
		$score = $ScoreStandardDetail[$result_record["rank"]];
	} else {
		$score = 0;
	}
	
	# [2018-0628-1634-01096] 1st round event not count score
	if($sys_custom['eSports']['KaoYipRelaySettings'] && !$isCountRelayScore) {
	    $score = 0;
	}
	
	# Calculate Score by Status
	if($result_record["status"]==3) {
		$score = $score + $ScoreStandardDetail[10];
	} else if($result_record["status"]==4) {
		$score = $score + $ScoreStandardDetail[9];
	}
	
	$result_record["score"] = $score;
	if($result_record["status"]=="")
	{
		$result_record["status"] = ($result_record["other"]==4) ? 5 : 1;
	}
	
	// [K151704]
	if ($result_record["rank"]=='') {
	    $result_record["rank"] = 'null';
	}
	
	$values = "ResultMin = ".$result_record["min"].", ";
	$values .= "ResultSec = ".$result_record["sec"].", ";
	$values .= "ResultMs = ".$result_record["ms"].", ";
	$values .= "RecordStatus = ".$result_record["status"].", ";
	$values .= "Rank = ".$result_record["rank"].", ";
	$values .= "Score = ".$result_record["score"];

	$sql = "UPDATE SPORTS_CLASS_RELAY_LANE_ARRANGEMENT SET $values WHERE EventGroupID = '$eventGroupID' AND ArrangeOrder = '$order'";
	$lsports->db_db_query($sql);
}

##########
# status
# 1 - absent
# 2 - Unsuitable
# 3 - Qualified
# 4 - Record Broken
# 5 - Foul
####################

intranet_closedb();
header ("Location:class_relay_record_detail.php?eventGroupID=$eventGroupID&xmsg=update");
?>