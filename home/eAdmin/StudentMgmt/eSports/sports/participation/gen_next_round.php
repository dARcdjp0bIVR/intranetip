<?php
# using: yat

###########################################
#
#	Date:	2012-09-26	YatWoon
#			skip result = 0 record
#
###########################################
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libsports.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

$lsports = new libsports();
$lsports->authSportsSystem();

################################################
# Retrieve the result of previous round in order to generate the next round participation list
$arrange = $lsports->returnParticipationRecordsByEventGroupID($eventGroupID, $eventType, $currRound);

$numberOfLane = $lsports->numberOfLanes;

$EventGroupDetail = $lsports->retrieveEventGroupDetail($eventGroupID);
$eventID = $EventGroupDetail['EventID'];

if(sizeof($arrange) != 0)
{
	if($eventType == 1)
	{
		for($i=0; $i<sizeof($arrange); $i++)
		{
			list($sid, $result_min, $result_sec, $result_ms) = $arrange[$i];
			$result_record = ($result_min*60*100) + ($result_sec*100) + $result_ms;
			if(!$result_record) continue;
			$resultArr[$sid] = $result_record;
		}
	}
	else
	{
		for($i=0; $i<sizeof($arrange); $i++)
		{
			list($sid, $result_metre) = $arrange[$i];
			if(!$result_metre) continue;
			$resultArr[$sid] = $result_metre;
		}
	}
	
	if($eventType == 1)
		asort($resultArr);
	else
		arsort($resultArr);
}
else
{
			header ("Location:input_tf_record.php?eventGroupID=$eventGroupID&msg=3");
}
#############################
# Return the next round participation number

	$ExtInfo = $lsports->retrieveEventGroupExtInfo($eventGroupID, $eventType);
	$numberOfLane = $lsports->numberOfLanes;
	
	if($roundType == 2)
	{
		$roundLanes = ($ExtInfo["SecondRoundLanes"]==0)?$numberOfLane:$ExtInfo["SecondRoundLanes"];
		$totalPartNum =$roundLanes*$ExtInfo["SecondRoundGroups"];
	}
	else
	{
		$totalPartNum = ($ExtInfo["FinalRoundNum"]==0)?$numberOfLane:$ExtInfo["FinalRoundNum"];
	}
	
	$StudentList = array_keys($resultArr);
	if(sizeof($StudentList)>$totalPartNum)
	{
		$ResultList = array_slice($StudentList, 0, $totalPartNum);
	}
	else
	{
		$ResultList = $StudentList;
	}
	
################# This part is for second round ###################
if($roundType == 2)
{
	# Retrieve the lane set of this event
   	$RoundLaneArrangement = $lsports->Get_Event_Special_Lane_Arrangement($eventID);
	list($final_rank, $second_rank) = $RoundLaneArrangement;
	if(empty($second_rank))
	{
		$lanesetArr = $final_rank;
		$lanesetArr = array_flip($lanesetArr); // flip arr form $idx=>$rank to $rank => $idx;  so $lanesetArr[$rank]+1 = $lane
		
		$pos=0;
		$delim = '';
		for($i=1;$i<=sizeof($lanesetArr);$i++) // loop lane
		{
			$lane = $lanesetArr[$i]+1;
			for($heat=1; $heat<=$ExtInfo["SecondRoundGroups"];$heat++) // loop heat
			{
				if($sid =  $ResultList[$pos])
				{
					$values .= $delim."(".$eventGroupID.", ".$roundType.", ".$sid.", ".$heat.", ".$lane.", now())";
					$delim = ", ";
				}
				$pos++;
			}
		}
//	    $lanesetArr = $final_rank;
//		$lanesetArr = array_flip($lanesetArr); // flip arr form $idx=>$rank to $rank => $idx;  so $lanesetArr[$rank]+1 = $lane 
//	
//		$minLaneNum = floor(sizeof($ResultList)/$ExtInfo["SecondRoundGroups"]);
//		$reminder = sizeof($ResultList)%$ExtInfo["SecondRoundGroups"];
//	
//		for($i=1; $i<=$ExtInfo["SecondRoundGroups"]; $i++)
//		{
//			$heatArrange[$i] = $minLaneNum;
//			if($reminder>0)
//			{
//				$heatArrange[$i] = $heatArrange[$i]+1;
//				$reminder--;
//			}
//		}
//	
//		$i=0;
//		$delim = "";
//		while(sizeof($ResultList)>$i)
//		{
//			for($j=1; $j<=sizeof($heatArrange); $j++)
//			{
//				if(${"pos".$j}=="")
//				{
//					if($ExtInfo["SecondRoundLanes"]==0)
//					{
//						${"pos".$j} = floor(($numberOfLane-$heatArrange[$j])/2) + 1;
//					}
//					else
//						${"pos".$j} = 1;
//				}
//			
//				if($ResultList[$i]!="")
//				{
//					$sid = $ResultList[$i];
//					$heat = $j;
//					$pos = $lanesetArr[${"pos".$j}]+1;
//					$values .= $delim."(".$eventGroupID.", ".$roundType.", ".$sid.", ".$heat.", ".$pos.", now())";
//					$delim = ", ";
//				}
//				${"pos".$j}++;
//				$i++;
//			}
//		}
		
	}
	else
	{
	    $lanesetArr = $second_rank;
		
		$pos=0;
		$delim = '';
		for($heat=1; $heat<=$ExtInfo["SecondRoundGroups"];$heat++) // loop heat
		{
			for($lane=1;$lane<=$numberOfLane;$lane++) // loop lane
			{
				$rank = $lanesetArr[$pos]; // get rank of corresponding position.
				if($sid = $ResultList[$rank-1]) // get student rank in $rank, rank 1 locate in $ResultList[0] ,  
				{
					$values .= $delim."(".$eventGroupID.", ".$roundType.", ".$sid.", ".$heat.", ".$lane.", now())";
					$delim = ", ";
				}
				$pos++;	
			}
		}
		
	}
	# Clear the records of this event in the SPORTS_LANE_ARRANGEMENT table
    $sql = "DELETE FROM SPORTS_LANE_ARRANGEMENT WHERE EventGroupID = '$eventGroupID' AND RoundType = '$roundType'";
    $lsports->db_db_query($sql);

	#Reset new Record
	#search for best result of this eventgroup
	$BestResult=$lsports->returnBestRecordResultOfTrackEventGroup($eventGroupID);
	
	if($BestResult)
	{
		$h_info=$lsports->retrieveStudentHouseInfo($BestResult[0]["StudentID"]);
		$student_name=$lsports->retrieveStudentName($BestResult[0]["StudentID"]);
		#Update New Record 
		$sql = "	UPDATE 
						SPORTS_EVENTGROUP_EXT_TRACK 
					SET ";
		$sql .= "		NewRecordMin = ".($BestResult[0]["ResultMin"]>0?$BestResult[0]["ResultMin"]:0).",
						NewRecordSec = ".($BestResult[0]["ResultSec"]>0?$BestResult[0]["ResultSec"]:0).",
						NewRecordMs = ".($BestResult[0]["ResultMs"]>0?$BestResult[0]["ResultMs"]:0).",";
		$sql .= "		NewRecordHolderUserID = ".($BestResult[0]["StudentID"]?$BestResult[0]["StudentID"]:"NULL").",
						NewRecordHouseID = ".($h_info[0]["HouseID"]?$h_info[0]["HouseID"]:"NULL").",
						NewRecordHolderName = ".($student_name?"'$student_name'":"NULL")."
					WHERE 
						 EventGroupID = $eventGroupID ";
		$lsports->db_db_query($sql);
	}
	else
	{
		$lsports->removeNewRecordOfTrackEventGroup($eventGroupID);
	}
	
	# Insert lanes arrangement to the SPORT_LANE_ARRANGEMENT table
	$fields = "(EventGroupID, RoundType, StudentID, Heat, ArrangeOrder, DateModified)";
    $sql = "INSERT INTO SPORTS_LANE_ARRANGEMENT $fields VALUES $values";
    
    $lsports->db_db_query($sql);
}
else	###################### This part is for final round #################
{
	$heat = 1;
	$delim = "";

	if($eventType == 1)
	{
		# Retrieve the lane set of this event
        $RoundLaneArrangement = $lsports->Get_Event_Special_Lane_Arrangement($eventID);
		list($final_rank, $second_rank) = $RoundLaneArrangement;
        $lanesetArr = $final_rank;
        
//        foreach($ResultList as $k1 => $sid)
//		{
//			foreach($lanesetArr as $k2 => $rank)
//			{
//				debug_pr("$rank == ".($k1+1)."");
//				if($rank == ($k1+1))
//				{
//					$pos = $k2 + 1;
//					break;
//				}
//			}
//			$values .= $delim."(".$eventGroupID.", ".$roundType.", ".$sid.", ".$heat.", ".$pos.", now())";
//			$delim = " ,";
//		}
		$lanesetArr = array_flip($lanesetArr); // flip arr form $idx=>$rank to $rank => $idx;  so $lanesetArr[$rank]+1 = $lane
		
		$pos=0;
		$delim = '';
		if(count($ResultList)>count($lanesetArr))
			$isGroupEvent = 1;
		
		foreach($ResultList as $k1 => $sid)
		{
			$lane = $lanesetArr[$k1+1]+1;
			$pos++;
			$order = $isGroupEvent?$pos:$lane;
			$values .= $delim."(".$eventGroupID.", ".$roundType.", ".$sid.", ".$heat.", ".$order.", now())";
			$delim = ", ";
		}
	}
	else
	{
		$tempIDList = "(";
		$tempIDList .= implode(",", $ResultList);
		$tempIDList .= ")";
	
		# Arrange the order of students in the final round according to their class name and number
		$sql = "SELECT UserID, ClassName, ClassNumber FROM INTRANET_USER WHERE UserID IN $tempIDList ORDER BY ClassName, ClassNumber";
		$newArr = $lsports->returnArray($sql, 3);

		for($i=0; $i<sizeof($newArr); $i++)
		{
			$sid = $newArr[$i][0];
			$pos = $i+1;
			$values .= $delim."(".$eventGroupID.", ".$roundType.", ".$sid.", ".$heat.", ".$pos.", now())";
			$delim = " ,";
		}
	}
	# Clear the records of this event in the SPORTS_LANE_ARRANGEMENT table
    $sql = "DELETE FROM SPORTS_LANE_ARRANGEMENT WHERE EventGroupID = '$eventGroupID' AND RoundType = '$roundType'";
    $lsports->db_db_query($sql);

	if($eventType == 1)
	{
		#Reset new Record
		#search for best result of this eventgroup
		$BestResult=$lsports->returnBestRecordResultOfTrackEventGroup($eventGroupID);
		
		if($BestResult)
		{
			$h_info=$lsports->retrieveStudentHouseInfo($BestResult[0]["StudentID"]);
			$student_name=$lsports->retrieveStudentName($BestResult[0]["StudentID"]);
			#Update New Record 
			$sql = "	UPDATE 
							SPORTS_EVENTGROUP_EXT_TRACK 
						SET ";
			$sql .= "		NewRecordMin = ".($BestResult[0]["ResultMin"]>0?$BestResult[0]["ResultMin"]:0).",
							NewRecordSec = ".($BestResult[0]["ResultSec"]>0?$BestResult[0]["ResultSec"]:0).",
							NewRecordMs = ".($BestResult[0]["ResultMs"]>0?$BestResult[0]["ResultMs"]:0).",";
			$sql .= "		NewRecordHolderUserID = ".($BestResult[0]["StudentID"]?$BestResult[0]["StudentID"]:"NULL").",
							NewRecordHouseID = ".($h_info[0]["HouseID"]?$h_info[0]["HouseID"]:"NULL").",
							NewRecordHolderName = ".($student_name?"'$student_name'":"NULL")."
						WHERE 
							 EventGroupID = $eventGroupID ";
			$lsports->db_db_query($sql);
		}
		else
		{
			$lsports->removeNewRecordOfTrackEventGroup($eventGroupID);
		}
	}
	else
	{

		#search for best result of this eventgroup
		$BestResult=$lsports->returnBestRecordResultOfFieldEventGroup($eventGroupID);
		
		if($BestResult)
		{
			$h_info=$lsports->retrieveStudentHouseInfo($BestResult[0]["StudentID"]);
			$student_name=$lsports->retrieveStudentName($BestResult[0]["StudentID"]);
			#Update New Record 
			$sql = "	UPDATE 
							SPORTS_EVENTGROUP_EXT_FIELD 
						SET ";
			$sql .= "		NewRecordMetre = ".($BestResult[0]["ResultMetre"]?$BestResult[0]["ResultMetre"]:"NULL").",";
			$sql .= "		NewRecordHolderUserID = ".($BestResult[0]["StudentID"]?$BestResult[0]["StudentID"]:"NULL").",
							NewRecordHouseID = ".($h_info[0]["HouseID"]?$h_info[0]["HouseID"]:"NULL").",
							NewRecordHolderName = ".($student_name?"'$student_name'":"NULL")."
						WHERE 
							 EventGroupID = $eventGroupID ";
			$lsports->db_db_query($sql);
		}
		else
		{
			$lsports->removeNewRecordOfFieldEventGroup($eventGroupID);
		}
	
	}
	# Insert lanes arrangement to the SPORT_LANE_ARRANGEMENT table
	$fields = "(EventGroupID, RoundType, StudentID, Heat, ArrangeOrder, DateModified)";
    $sql = "INSERT INTO SPORTS_LANE_ARRANGEMENT $fields VALUES $values";
    $lsports->db_db_query($sql);

}

intranet_closedb();
header ("Location:input_tf_record.php?eventGroupID=$eventGroupID&msg=2");

?>