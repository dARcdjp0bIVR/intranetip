<?php
# using: Bill

############################################
#	Date:	2019-11-20  Philips [2019-1024-1748-37066]
#			Replace $i_Sports_menu_Report_EventRanking by $Lang['eSports']['IndividualEventRanking']
#
#   Date:   2019-04-11  Bill    [2019-0301-1144-56289]
#           Create File
#           Copy logic from tf_record.php
#
############################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libsports.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$CurrentPage = "PageRaceResult";

$lsports = new libsports();
$lsports->authSportsSystem();

$AgeGroups = $lsports->retrieveAgeGroupIDNames();	# retrieve all age group ids and names

# Add Boys Open and Girls Open 
$openBoy[0] = '-1';
$openBoy[1] = $i_Sports_Event_Boys_Open;
$openGirl[0] = '-2';
$openGirl[1] = $i_Sports_Event_Girls_Open;
$openMixed[0] = '-4';
$openMixed[1] = $i_Sports_Event_Mixed_Open;
$AgeGroups[] = $openBoy;
$AgeGroups[] = $openGirl;
$AgeGroups[] = $openMixed;

# Age Group
$ageGroup = "";
$ageGroup .= "<tr>";
$ageGroup .= "<td width='100'>&nbsp;</td>";
for($i=0; $i<sizeof($AgeGroups); $i++) {
	$ageGroup .= "<td class='tabletop tabletoplink' align='center'>".$AgeGroups[$i][1]."</td>";
}
$ageGroup .= "</tr>";

$CREvents = $lsports->retrieveClassRelayName();		# retrieve class relay event group ids and names
$numberOfLane = $lsports->numberOfLanes;

$type_id = array(4);
$indicator = "<font style='color:red'>*</font>";    # non-complete event indicator

# Table List
$tr = 0;
$list = "";
$cur_event_id = "";
for($j=0; $j<sizeof($CREvents); $j++)
{
	list($event_id, $group_id, $event_name) = $CREvents[$j];
    if($event_id == $cur_event_id)	continue;
    $cur_event_id = $event_id;
    
    $list .= "<tr class='tablerow".($tr % 2? "2" : "1")."'>";
        $list .= "<td class='tablelist'>". $event_name."</td>";
    
    for($k=0; $k<sizeof($AgeGroups); $k++)
	{
    	$group_id = $AgeGroups[$k][0];
    	$eventGroupInfo = $lsports->retrieveEventGroupID($event_id, $group_id, $type_id);
        if(sizeof($eventGroupInfo) != 0)
        {
        	$egid = $eventGroupInfo[0];
        	
        	// Get Class Relay Lane Arrangement
        	$class_arrange = $lsports->retrieveClassRelayLaneArrangementByRound($egid, ROUND_TYPE_FIRSTROUND);
         	// $class_arrange_final = $lsports->retrieveClassRelayLaneArrangementByRound($egid, ROUND_TYPE_FINALROUND);
        	
        	// Get Class Relay Ranking
        	$class_ranking = $lsports->retrieveClassRelayGroupRankingByRound($event_id, $group_id);
        	
        	# Check whether Event has been set up
        	$is_setup = $lsports->checkExtInfoExist(4, $egid);
        	if($is_setup == 1)
        	{		
        	    $arranged = !empty($class_arrange)? count($class_arrange) : 0;
        	    if($arranged != 0)
        	    {
        	        $tr_display = ((sizeof($class_ranking)==0)? $indicator : "")."<a class='tablelink' href='class_relay_group_record_detail.php?eventGroupID=$egid'>".$arranged."</a>";
        	    }
        	    else
        	    {
        	        $tr_display = "0";
        	    }
        	    $list .= "<td class='tabletext' align='center'>".$tr_display."</td>";
    		}
    		else
    		{
    		    $list .= "<td class='tabletext' align='center'>- -</td>";
    		}
    	}
    	else {
    		$list .= "<td>&nbsp;</td>";
    	}
	}
    $list .= "</tr>";
    
	$tr++;
}

# Title
$house_relay_name = $lsports->retrieveEventTypeNameByID(3);
$class_relay_name = $lsports->retrieveEventTypeNameByID(4);

# Tag
$TAGS_OBJ[] = array($i_Sports_menu_Participation_TrackField, "../participation/tf_record.php", 0);
$TAGS_OBJ[] = array($house_relay_name, "../participation/relay_record.php", 0);
$TAGS_OBJ[] = array($class_relay_name, "../participation/class_relay_group_record.php", 1);
$TAGS_OBJ[] = array($Lang['eSports']['IndividualEventRanking'], "../report/event_rank.php", 0);
$TAGS_OBJ[] = array($i_Sports_menu_Report_HouseGroupScore, "../report/house_group.php", 0);
if(!$sys_custom['eSports']['PuiChi_MultipleGroup']) {
	$TAGS_OBJ[] = array($i_Sports_menu_Report_GroupChampion, "../report/group_champ.php", 0);
}
$MODULE_OBJ = $lsports->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();
?>

<br />
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td align="center"><br />
    	<table width="96%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td align="left" class="tabletext">
              	<table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                    	<td>
                    		<table width='100%' border='0' cellspacing='0' cellpadding='4'>
                                <?=$ageGroup?>
                                <?=$list?>
							</table>
						</td>
					</tr>
                    <tr>
                    	<td height='1' class='dotline'><img src='<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif' width='10' height='1'></td>
                    </tr>
    				<tr>
    					<td class="tabletextremark">(<?=$i_Sports_Explain?>: - - <?=$i_Sports_Item_Without_Setting_Meaning?> , <?=$indicator?> <?=$i_Sports_Not_Complete_Events_Meaning?>)</td>
    				</tr>
				</table>
			</td>
		</tr>
        </table>
    </td>
</tr>
</table>

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>