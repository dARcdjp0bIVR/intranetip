<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libsports.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

$lsports = new libsports();
$lsports->authSportsSystem();

$lexport = new libexporttext();

$content = str_replace('&quot;','"',$content);

$today = date('Y-m-d');
$filename = $today.".csv";
$AgeGroups = array();
$AgeGroups = $lsports->retrieveAgeGroupIDNames();	# retrieve all age group ids and names

# Add Boys Open and Girls Open
$openBoy[0] = '-1';
$openBoy[1] = $i_Sports_Event_Boys_Open;
$openGirl[0] = '-2';
$openGirl[1] = $i_Sports_Event_Girls_Open;
$openMixed[0] = '-4';
$openMixed[1] = $i_Sports_Event_Mixed_Open;
$AgeGroups[] = $openBoy;
$AgeGroups[] = $openGirl;
$AgeGroups[] = $openMixed;

$TREvents = $lsports->retrieveTrackFieldEventName();
$numberOfLane = $lsports->numberOfLanes;
$eContent = array();
for($i=0; $i<sizeof($TREvents); $i++)
{
	list($event_id, $event_name) = $TREvents[$i];
	for($j=0; $j<sizeof($AgeGroups); $j++)
	{
		list($group_id, $group_name) = $AgeGroups[$j];

		$typeIDs = array(1, 2);
		# Retrieve event group id
		$eventGroupInfo = $lsports->retrieveEventGroupID($event_id, $group_id, $typeIDs);

		$eventGroupID = $eventGroupInfo[0];
		$eventType = $eventGroupInfo[1];
		if($eventGroupID != "")
		{
			$participantCount = $lsports->retrieveTFLaneArrangedEventCount($eventGroupID);
            //debug_pr($lsports->Get_Group_Event_By_EventGroupID($eventGroupID));
			if($group_id > 0)
			{
				$EventGroupInfo = $lsports->retrieveEventAndGroupNameByEventGroupID($eventGroupID);
				$eventName = $EventGroupInfo[0];
				$ageGroupName  = $EventGroupInfo[1];
				$eventType = $EventGroupInfo[2];
			}
			else
			{
			    $GroupEventInfo = $lsports->Get_Group_Event_By_EventGroupID($eventGroupID);
                list($GroupID, $EventID) = $GroupEventInfo[0];
				$EventInfo = $lsports->Get_Event_Info($EventID);

				list($eventType, $eventName) = $EventInfo[0];
				$ageGroupName = $lsports->Get_Open_Event_Name($group_id);
			}

			$ExtInfo = $lsports->retrieveEventGroupExtInfo($eventGroupID, $eventType);

			if($eventType == EVENT_TYPE_TRACK)
			{
				if($ExtInfo["NewRecordMin"]!=NULL && $ExtInfo["NewRecordSec"]!=NULL && $ExtInfo["NewRecordMs"]!=NULL)
				{
					$NewRecordArr = array($ExtInfo["NewRecordMin"], $ExtInfo["NewRecordSec"], $ExtInfo["NewRecordMs"]);
					$new_record_result = $lsports->Format_TimerArr($NewRecordArr);
				}
				else
				{
					$new_record = "&nbsp;";
					$new_record_result = "";
				}
				
				$RecordArr = array($ExtInfo["RecordMin"], $ExtInfo["RecordSec"], $ExtInfo["RecordMs"]);
				$record = $lsports->Format_TimerArr($RecordArr);
				
				$StandardArr = array($ExtInfo["StandardMin"], $ExtInfo["StandardSec"], $ExtInfo["StandardMs"]);
				$standard = $lsports->Format_TimerArr($StandardArr);
				$ResultArr = $lsports->Get_Track_Result($eventGroupID);
			}
			else
			{
				# Check if it is high jump event
				$isHighJump = $lsports->Is_High_Jump($eventGroupID);
				
				# Check if consider all 6 trials
				$IsAllTrialIncluded = $lsports->Is_All_Trial_Included($eventGroupID);
				$needToUpdateFinalRound = false;
				
				if($ExtInfo["NewRecordMetre"]!=NULL)
				{
					$new_record = $ExtInfo["NewRecordMetre"];
					$new_record_result = $new_record;
				}
				else
				{
					$new_record = "&nbsp;";
					$new_record_result = "";
				}
				
				$record = $ExtInfo["RecordMetre"];
				$standard = $ExtInfo["StandardMetre"];
				
				$ResultArr = $lsports->Get_Field_Result($eventGroupID);
			}
			
			$rankingResult = Get_Array_By_Key($ResultArr, 'Rank');
			$rankingResult = array_filter($rankingResult);
			$ResultArr = BuildMultiKeyAssoc($ResultArr, array("StudentID", "RoundType"));
			
			# Build Ext. Record Export Content
			$export_header = array();
			$export_header[] = array($i_Sports_Item,$eventName);
			$export_header[] = array($i_Sports_field_Group,$group_name);
			$export_header[] = array($i_Sports_Record,$record);
			$export_header[] = array($i_Sports_New_Record,$new_record_result);
			$export_header[] = array($i_Sports_Standard_Record, $standard);
			//$export_header[] = array();
			
			$export_arr = $export_header;
			
			if($ExtInfo["FinalRoundReq"]==1) {
				$finalReq=1;
			}
			else {
				$finalReq=0;
			}
			
			$rows_per_table = 0;
			
			$RoundTypeArr = array(ROUND_TYPE_FIRSTROUND,ROUND_TYPE_SECONDROUND,ROUND_TYPE_FINALROUND);
			$export_content_arr = array();
			foreach($RoundTypeArr as $roundType)
			{
				$export_content = array();
				$directFinal = 0;
				$arrangeDetail1 = array();
				$arrange1 = $lsports->returnTFLaneArrangeDetailByEventGroupID($eventGroupID, 1, $roundType);
				if(empty($arrange1)) continue;

                $export_content_arr = array();
				
				# Store existing round
				$InvolvedRoundArr[] = $roundType;
				
				$total_heat_num = $lsports->returnHeatNumberByEventGroupID($eventGroupID);
				
				$gen_next_btn = '';
				if($roundType == ROUND_TYPE_SECONDROUND)
				{
					if($ExtInfo["SecondRoundReq"] == 0)
					{
						continue;
					}
				}
				else if($roundType == ROUND_TYPE_FINALROUND)
				{
					if($ExtInfo["FinalRoundReq"] == 0)
					{
						continue;
					}
				}
				for($k=0; $k<sizeof($arrange1); $k++)
				{
					list($heat, $order, $sname, $sid,$tmp_rank, $tmp_score, $tmp_trackresult, $tmp_fieldresult, $tmp_trial1, $tmp_trial2, $tmp_trial3, $s_ename, $s_cname) = $arrange1[$k];
					
					# Get House name of Student
					$house1 = $lsports->retrieveStudentHouseInfo($sid);
					# Retrieve result if exist
					if($eventType == EVENT_TYPE_TRACK)
					{
						list($result_min1, $result_sec1, $result_ms1, $s_status1, $reason, $score, $rank) = $ResultArr[$sid][$roundType];
					}
					else
					{
						list($s_record1, $s_status1, $s_rank1, $reason, $trial1, $trial2, $trial3, $score, $rank) = $ResultArr[$sid][$roundType];
						
						// [2017-0405-1457-30236] Get First Round Best Attempts
						if($roundType == ROUND_TYPE_FINALROUND && $IsAllTrialIncluded)
						{
							$arrangeDetail1[$heat][$order]["trial"]["first_round"] = "";
							$first_round_record = $ResultArr[$sid][ROUND_TYPE_FIRSTROUND]["ResultMetre"];
							if(!empty($first_round_record))
							{
								$arrangeDetail1[$heat][$order]["trial"]["first_round"] = $first_round_record;
								
								// Display Warning Message - *Please update Final Round Result
								list($final_round_status, $final_round_attend) = $lsports->Get_Attend_And_Status($s_status1);
								if($final_round_attend == 1 && ($first_round_record > $s_record1)) {
									$s_record1 = $first_round_record;
									$needToUpdateFinalRound = true;
								}
							}
						}
					}
					
					$arrangeDetail1[$heat][$order]["sid"] = $sid;
					$arrangeDetail1[$heat][$order]["student_name"] = $sname;
					$arrangeDetail1[$heat][$order]["house_name"] = $house1[0][0];
					$arrangeDetail1[$heat][$order]["house_code"] = $house1[0][3];
					$arrangeDetail1[$heat][$order]["color_code"] = $house1[0][2];
					if($eventType == EVENT_TYPE_TRACK)
					{
						$arrangeDetail1[$heat][$order]["min"] = $result_min1;
						$arrangeDetail1[$heat][$order]["sec"] = $result_sec1;
						$arrangeDetail1[$heat][$order]["ms"] = $result_ms1;
					}
					else
					{
						$arrangeDetail1[$heat][$order]["record"] = $s_record1;
						$arrangeDetail1[$heat][$order]["trial"][1] = $trial1;
						$arrangeDetail1[$heat][$order]["trial"][2] = $trial2;
						$arrangeDetail1[$heat][$order]["trial"][3] = $trial3;
						$arrangeDetail1[$heat][$order]["rank"]=$s_rank1;
					}
					$arrangeDetail1[$heat][$order]["rank"] = $rank;
					$arrangeDetail1[$heat][$order]["score"] = $score;
					$arrangeDetail1[$heat][$order]["status"] = $s_status1;
					$arrangeDetail1[$heat][$order]["reason"] = $reason;
				}
				$tableContent1 = "";
				if(sizeof($arrangeDetail1) != 0)
				{
                    foreach($arrangeDetail1 as $heat => $value)
					{
						# Round Title & Table row class
						switch($roundType)
						{
							case ROUND_TYPE_FIRSTROUND:
								$RoundTitle = $lsports->Get_Lang_Heat($heat);
								$RoundTitle .= ($ExtInfo["SecondRoundReq"] == 1 || $ExtInfo["FinalRoundReq"] == 1) ? " (".$i_Sports_First_Round.")" :"";
								break;
							case  ROUND_TYPE_SECONDROUND:
								$RoundTitle = $lsports->Get_Lang_Heat($heat) ." (".$i_Sports_Second_Round .")";
								break;
							case ROUND_TYPE_FINALROUND:
								$RoundTitle = $i_Sports_Final_Round;
								break;
						}
						
						# The following if statement seems have logic problem
						$GroupPersonNum = $ExtInfo['FirstRoundGroupCount'];
						
						// [2015-1201-1443-45073] added (min : sec : ms)
						// [2016-1124-1539-50066] hide "(min : sec : ms) for highjump event"
						//if ($isHighJump) {
						if ($eventType == EVENT_TYPE_FIELD) {
							$unitDisplay = '';
						}
						else {
							$unitDisplay = '(min : sec : ms)';
						}
						
						$export_row = array();
						$exportData = array();
						
						$export_roundTitle = array($RoundTitle);
						$export_row[] = ($eventType == EVENT_TYPE_TRACK? $i_Sports_Line : "&nbsp;");
						$export_row[] = $Lang['General']['EnglishName'];
						$export_row[] = $Lang['General']['ChineseName'];
						$export_row[] = $i_general_class;
						$export_row[] = $i_Sports_House;
						$export_row[] = $i_Sports_Present."/".$i_Sports_Absent;
						$export_row[] = $i_Sports_field_Rank;

                        $exportData[] = array();
                        $exportData[] = $export_roundTitle;
						if($eventType == EVENT_TYPE_FIELD && !$isHighJump)
						{
							for($try = 1; $try<=3; $try++) {
								$export_row[] = $lsports->Get_Lang_Trial($try);
							}
						}
						
						$export_row[] = $i_Sports_Record;
						$export_row[] = $i_Sports_field_Score;
						$export_row[] = $i_Sports_Other;
						
						$exportData[] = $export_row;
						# Retrieve the maxinum key
						if(sizeof($value) != 0)
						{
							$key = array_keys($value);
							rsort($key);
							$maxPos = $key[0];
						}
						else
						{
							$maxPos = 0;
						}
						$thisEventStudentList = Get_Array_By_Key(array_values($value), "sid");
						$lu = new libuser("","",$thisEventStudentList);
						for($l=1; $l<=$maxPos; $l++)
						{
							$detail = $value[$l];
							$export_rowData = array();
							$orderShow = ($eventType == EVENT_TYPE_TRACK)? $lsports->Get_Lang_Line($l):$l;
							$export_rowData[] = $orderShow;
							if(sizeof($detail) != 0)
							{
								$lu->LoadUserData($detail["sid"]);
								
								$color = ($detail["color_code"] == "") ? "FFFFFF" : $detail["color_code"];
								
								list($status,$attend) = $lsports->Get_Attend_And_Status($detail["status"]);
								
								# Student Name and House
								$export_rowData[] = $lu->EnglishName;
								$export_rowData[] = $lu->ChineseName;
								$export_rowData[] = $lu->ClassName;
								$export_rowData[] = $detail["house_name"];
								
								# Present / Absent Selection
								if($eventType==EVENT_TYPE_FIELD && $isHighJump) {
									$checkTie = " checkTieResult($roundType,$heat) ";
								}
								$AttendSelection = $lsports->Get_Present_Absent_Selection($heat, $l,$attend,$detail['reason'],"doValidation2($roundType,$heat); $checkTie");
								switch($attend)
								{
									case 1: $export_rowData[] = $i_Sports_Present; break;
									case 2: $export_rowData[] = $i_Sports_Absent; break;
									case 3: $export_rowData[] = $i_Sports_Absent." - ".$Lang['eSports']['Waive']."(".$detail['reason'].")"; break;
									default: $export_rowData[] = "";
								}
								
								# Rank
								$export_rowData[] = $detail['rank'];
								
								# Result Display
								$result_disabled = $attend==2 || $attend==3?" disabled ":"";
								if($eventType == EVENT_TYPE_TRACK)
								{
									$msLength = $sys_custom['eSports']['SportDay_ParticipationMs_MKSS'] ? 3 : 2;
									if($attend!=2 && ($detail['min'] ||$detail['sec'] ||$detail['ms'] )) {    # 2 = absent
										$export_rowData[] = $detail['min'] ."'".$detail['sec']."''".$detail['ms'];
									}
									else {
										$export_rowData[] = "";
									}
								}
								else
								{
									# Tie Breaker Selection
									if($isHighJump)
									{
										$tieResult = $lsports->Get_Tie_Break_Result($eventGroupID, $roundType, $detail["record"]);
										$optionArr = array();
										$optionArr[] = array(0, $i_Sports_Tie_Breaker);
										if(count($tieResult) > 1)
										{
											// [2019-0117-1351-39207] rank options - updated logic
											$rankBase = $tieResult[0]['Rank'];
											for($rankCount=0; $rankCount<count($tieResult); $rankCount++) {
												list($oriResult, $oriRank) = $tieResult[$rankCount];
												$oriRank = $rankBase + $rankCount;
												
												$optionArr[] = array($oriRank,$i_Sports_field_Rank." ".$oriRank);
											}
											$disabled = "";
										}
										else
										{
											$disabled = " disabled ";
										}
										$onChange = " onchange='doValidation2($roundType,$heat)' ";
										$TieBreakSelection = getSelectByArray($optionArr, " name='tie_breaker_".$heat."_".$l."' $disabled $onChange",$detail["rank"],0,1,$i_Sports_Tie_Breaker);
										
										if($attend!=2) {	# 2 = absent
											$export_rowData[] = $detail['record'];
										}
										else {
											$export_rowData[] = "";
										}
										
										$CheckTieOnStatusSelection = " checkTieResult($roundType,$heat); ";
									}
									else
									{										
										if($attend != 2) {     # 2 = absent
											for($try = 1; $try<=3; $try++) {
												$export_rowData[] = $detail["trial"][$try];
											}
											$export_rowData[] = $detail['record'];
										}
										else {
											$export_rowData[] = "";
										}
									}
								}
								
								$export_rowData[] = $detail["score"];
								
								switch($status)
								{
									case 0: $export_rowData[] = "--"; 					break;
									case 1: $export_rowData[] = $i_Sports_RecordBroken; 	break;
									case 2: $export_rowData[] = $i_Sports_Qualified; 	break;
									case 3: $export_rowData[] = $i_Sports_Unsuitable; 	break;
									case 4: $export_rowData[] = $i_Sports_Foul; 			break;
									default: $i_Sports_Unsuitable;
								}
							}
							else
							{
							}
							$exportData[] = $export_rowData;
							$rows_per_table++;
						}
						
						$rows_per_table=0;
						if(!empty($exportData)){
							$export_arr = array_merge($export_arr, $exportData);

                            //if($group_id < 0) {
                            //    debug_pr($exportData);
                            //}
							$export_content_arr = array_merge($export_content_arr, $export_arr);
						}
// 						$export_content_arr[$roundType] = $export_content;
					}
				}
			}
			if(!empty($export_content_arr)){
				$eContent = array_merge($eContent, $export_content_arr);
				$eContent[] = array();
			}


        }
	}
}
$export_content = $lexport->GET_EXPORT_TXT($eContent, array());

$lexport->EXPORT_FILE($filename, $export_content);

// }


intranet_closedb();
?>