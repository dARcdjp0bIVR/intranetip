<?php
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libswimminggala.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");


intranet_auth();
intranet_opendb();

$linterface 	= new interface_html();
$lswimminggala	= new libswimminggala();

if (!$plugin['swimming_gala'] or $_SESSION['intranet_swimminggala_right']!=1)
{
	header("Location: index.php");
        exit();
}

### Title ###
$MODULE_OBJ = $lswimminggala->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();

?>
<br />
<table align="center" width="90%" border="0" cellpadding="5" cellspacing="0" >
	<tr valign="top">
		<td class="tabletext" align="center"><?=$i_Sports_Select_Menu?> </td>
	</tr>
</table>
<br />
<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>

