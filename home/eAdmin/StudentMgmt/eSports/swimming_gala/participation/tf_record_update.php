<?php
# using: 

############ Changed Log [Start] ######################
#
#	2019-05-14 [Bill]
#   - prevent SQL Injection
#
#	2013-09-02 [Roy]
#		- Fixed: wrong score for old record breakers
#		- Fixed: absent score should only count in first round
#
#	2012-03-21 [YatWoon]
#		- Update: if the event hasn't set the standard record, no need to count with qualify score
#
#	2010-10-25 [Marcus]
#		- modified coding, 
#			if client don't input a result for first round, abs-deduct mark will be assigned to the student while abs-waived will be assigned for 2nd/final round
#			
#	2010-08-26 [Marcus]
#		- modified foul will not deduct mark.
#		- modified coding to cater absent - waive
#		- fixed Qualifying marks will not count if student not qualify in first round but qualify in sec/final round.   
# 
#	2009-12-07 [YatWoon]
#	1.	Fixed: if there are 2 student with broken records, only the 1st student can earn the broken score
#	2.	Add customization for www.mfbmclct.edu.hk: 
#		if there is 3 students = rank 1, then the score need to be avg of (1st + 2nd + 3rd's score)
# 		assumption, they select the ranking display should be 1,2,2,4
#		flag: $sys_custom['eSports_same_position_avg_score'] = true;
#	2009-12-09 [Marcus]
#		Fixed: Same as Record should not be count as Record Broken.
############ Changed Log [Start] ######################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libswimminggala.php");

intranet_auth();
intranet_opendb();

### Handle SQL Injection + XSS [START]
$eventGroupID = IntegerSafe($eventGroupID);
$eventType = IntegerSafe($eventType);
$roundType = IntegerSafe($roundType);
$currHeat = IntegerSafe($currHeat);
### Handle SQL Injection + XSS [END]

$lsports = new libswimminggala();
$lsports->authSportsSystem();

$arrange = $lsports->returnTFLaneArrangeDetailByEventGroupID($eventGroupID, 0, $roundType);
$ExtInfo = $lsports->retrieveEventGroupExtInfo($eventGroupID, $eventType);
$finalReq = $ExtInfo["FinalRoundReq"];
$numberOfLane = $lsports->numberOfLanes;

# Update newrecord
if($eventType==EVENT_TYPE_FIELD)
{
	# Clear old result of current heat
	$sql = "	UPDATE SWIMMINGGALA_LANE_ARRANGEMENT 
					SET ResultMetre = NULL
				WHERE
					EventGroupID = '$eventGroupID'
					AND roundType = '$roundType'
					AND heat = '$currHeat'";
	$lsports->db_db_query($sql);

	# Search for best result of this eventgroup
	$BestResult = $lsports->returnBestRecordResultOfFieldEventGroup($eventGroupID);
	if($BestResult)
	{
		$h_info = $lsports->retrieveStudentHouseInfo($BestResult[0]["StudentID"]);
		$student_name = $lsports->retrieveStudentName($BestResult[0]["StudentID"]);
		
		# Update New Record 
		$sql = "	UPDATE 
						SWIMMINGGALA_EVENTGROUP_EXT_FIELD 
					SET ";
		$sql .= "		NewRecordMetre = ".($BestResult[0]["ResultMetre"]? $BestResult[0]["ResultMetre"] : "NULL").",";
		$sql .= "		NewRecordHolderUserID = ".($BestResult[0]["StudentID"]? $BestResult[0]["StudentID"] : "NULL").",
						NewRecordHouseID = ".($h_info[0]["HouseID"]? $h_info[0]["HouseID"] : "NULL").",
						NewRecordHolderName = ".($student_name? "'$student_name'" : "NULL")."
					WHERE 
						 EventGroupID = '$eventGroupID' ";
		$lsports->db_db_query($sql);
	}
	else
	{
        $lsports->removeNewRecordOfFieldEventGroup($eventGroupID);
	}
}
else
{
	$table = "SWIMMINGGALA_EVENTGROUP_EXT_TRACK";
	$updateFields = "ResultMin = NULL, ResultSec = NULL, ResultMs = NULL ";
    
	# Clear old result of current heat
	$sql = "	UPDATE SWIMMINGGALA_LANE_ARRANGEMENT 
					SET $updateFields
				WHERE
					EventGroupID = '$eventGroupID'
					AND roundType = '$roundType'
					AND heat = '$currHeat'";
	$lsports->db_db_query($sql);
    
	# Search for best result of this eventgroup
	$BestResult = $lsports->returnBestRecordResultOfTrackEventGroup($eventGroupID);
	if($BestResult)
	{
		$h_info = $lsports->retrieveStudentHouseInfo($BestResult[0]["StudentID"]);
		$student_name = $lsports->retrieveStudentName($BestResult[0]["StudentID"]);
		
		# Upadte Student Record Status (to Record Broken)
		# Update New Record 
		$sql = "	UPDATE 
						$table 
					SET ";
		$sql .= "		NewRecordMin = ".($BestResult[0]["ResultMin"]>0? $BestResult[0]["ResultMin"] : "0").",
						NewRecordSec = ".($BestResult[0]["ResultSec"]>0? $BestResult[0]["ResultSec"] : "0").",
						NewRecordMs = ".($BestResult[0]["ResultMs"]>0? $BestResult[0]["ResultMs"] : "0").",";
		$sql .= "		NewRecordHolderUserID = ".($BestResult[0]["StudentID"]? $BestResult[0]["StudentID"] : "NULL").",
						NewRecordHouseID = ".($h_info[0]["HouseID"]? $h_info[0]["HouseID"] : "NULL").",
						NewRecordHolderName = ".($student_name? "'$student_name'" : "NULL")."
					WHERE 
						 EventGroupID = '$eventGroupID' ";
		$lsports->db_db_query($sql);
	}
	else
	{
		$lsports->removeNewRecordOfTrackEventGroup($eventGroupID);
	}
}

$RankIsGenerated = 0;

if($eventType == EVENT_TYPE_TRACK)
{
	$record = $ExtInfo["RecordAsMS"];
	if($record <= 0) {
		$record = NULL;
	}
	$standard = $ExtInfo["StandardAsMS"];
}
else
{
	# check if it is high jump event
	$isHighJump = $lsports->Is_High_Jump($eventGroupID);
	
	$record = $ExtInfo["RecordMetre"];
	$standard = $ExtInfo["StandardMetre"];
}

for($i=0; $i<sizeof($arrange); $i++)
{
	list($heat, $order, $sname, $sid) = $arrange[$i];
	if ($heat != $currHeat) continue;
	
	if($eventType == EVENT_TYPE_TRACK)
	{
	    $result_min = (${"result_min_".$heat."_".$order}=="")? 0 : IntegerSafe(${"result_min_".$heat."_".$order});
	    $result_sec = (${"result_sec_".$heat."_".$order}=="")? 0 : IntegerSafe(${"result_sec_".$heat."_".$order});
	    $result_ms = (${"result_ms_".$heat."_".$order}=="")? 0 : IntegerSafe(${"result_ms_".$heat."_".$order});
		
		$resultRecord = $lsports->Convert_TimeArr_To_Millisecond(array($result_min,$result_sec,$result_ms));
		$resultRecordArr[$sid] = $resultRecord;
	}
	else
	{
	    $resultRecord = (${"record_".$heat."_".$order}=="")? 0 : IntegerSafe(${"record_".$heat."_".$order});
		if($isHighJump==1)
		{
		    $tie_breaker_rank[$sid] = $resultRecord==0? 0 : IntegerSafe(${"tie_breaker_".$heat."_".$order});
		}
	}
	$other = IntegerSafe(${"other_".$heat."_".$order});
	$tmpAttend = IntegerSafe(${"attend_".$heat."_".$order});
	
	if($resultRecord == 0 && $tmpAttend == 1 && $other != 4)
	{
		if($roundType==ROUND_TYPE_FIRSTROUND)
			$attend = 2;
		else
			$attend = 3;
	}
	else
	{
	    $attend = IntegerSafe(${"attend_".$heat."_".$order});
	}
//	$attend = ($resultRecord == 0 && $tmpAttend==1 && $other!=4)? 2:${"attend_".$heat."_".$order};
	$absent_reason = ${"attend_".$heat."_".$order."_reason"}? intranet_htmlspecialchars(${"attend_".$heat."_".$order."_reason"}) : "N.A.";
    
	# Create a associate array
	if($eventType == EVENT_TYPE_TRACK)
	{
		$resultArr[$sid]["min"] = $result_min;
		$resultArr[$sid]["sec"] = $result_sec;
		$resultArr[$sid]["ms"] = $result_ms;
		$resultArr[$sid] = $lsports->Pad_TimerArr($resultArr[$sid]);
	}
	else
	{
		$resultArr[$sid]["metre"] = $resultRecord;
	}
	$resultArr[$sid]["student_name"] = $sname;
	$resultArr[$sid]["attend"] = $attend;
	$resultArr[$sid]["other"] = $other;
	$resultArr[$sid]["absent_reason"] = $absent_reason;
	
	if($resultRecord!=0 && $resultArr[$sid]["other"]!=4 && $resultArr[$sid]["other"]!=5 && $attend==1)
	{
		$Ranking[$sid] = $resultRecord;
	}
}

if($Ranking!=null)
{
	# Sort the Ranking Array
	if($eventType == EVENT_TYPE_TRACK)
		asort($Ranking);
	else
		arsort($Ranking);
		
	$temp_rank = 0;

	$last_rank=0;
	$ctr=0;
	foreach($Ranking as $sid => $value)
	{
		if($eventType == 2)
		{
			if($tie_breaker_rank[$sid]!=null)
			{
				$resultArr[$sid]["rank"] = $tie_breaker_rank[$sid];
			}
			else
			{
				$r = 1;
				foreach($Ranking as $s =>$v)
				{
					if($v>$Ranking[$sid])
					{
						$r++;
					}
				}
				$tie_breaker_rank[$sid] = $r;
				$resultArr[$sid]["rank"]=$r;
			}
		}
		else //track assign rank
		{
			//modified by marcus 20091113
			#$temp_rank++;
			#$resultArr[$sid]["rank"] = $temp_rank;
			if($lsports->RankPattern=="1223")
			{
				if($last_sid!=""&&$resultRecordArr[$sid]==$resultRecordArr[$last_sid]) //check if record same as neighbour
				{
					$resultArr[$sid]["rank"] = $temp_rank;
				}
				else
				{
					$resultArr[$sid]["rank"] = (++$temp_rank);
				}
				$last_sid=$sid; 
			}
			else
			{ 
				++$temp_rank;
				if($last_sid!=""&&$resultRecordArr[$sid]==$resultRecordArr[$last_sid]) //check if record same as neighbour
				{
					$resultArr[$sid]["rank"] = $last_rank;
				}
				else
				{
					$resultArr[$sid]["rank"] = $temp_rank;
					$last_rank = $temp_rank;
				}
				$last_sid=$sid;
			
			}
		}
	
		if($resultArr[$sid]["other"]!=4 && $resultArr[$sid]["other"]!=5 && $resultArr[$sid]["other"]!=3)
		{
			# Compare the result record with the broken record and standard record
			if($eventType == 1)
			{
				# Retrieve the updated new record for compare
				$sql = "SELECT NewRecordMin, NewRecordSec, NewRecordMs FROM SWIMMINGGALA_EVENTGROUP_EXT_TRACK Where EventGroupID = '$eventGroupID'";
				$newRecord = $lsports->returnArray($sql, 3);
				
				$new = ($newRecord[0][0]*60*100) + ($newRecord[0][1]*100) + $newRecord[0][2];			
				$check_record = ($new==0) ? $record : $new; 
//				debug_pr("$record : $new");
//				debug_pr("$value < $check_record || !$check_record");
                
				# modified by Marcus 20091209 (change "<=" to "<" , same as Record should not be counted as Record Broken) 
				if($value < $check_record || !$check_record)
				{
					$resultArr[$sid]["status"] = 4;
							
					# get record House ID
					$h_info = $lsports->retrieveStudentHouseInfo($sid);
					$hid = $h_info[0][1];
					
					$sname = $resultArr[$sid]["student_name"];
					$newRecordMin = $resultArr[$sid]["min"];
					$newRecordSec = $resultArr[$sid]["sec"];
					$newRecordMs = $resultArr[$sid]["ms"];

					# Update broken record time
					# 20091207 YatWoon
					# Fixed: if there are 2 student with broken records, only the 1st student can earn the broken score
					# make sure only the first student is add in the record [Assumption]
					$sql1 = "select EventGroupID from SWIMMINGGALA_EVENTGROUP_EXT_TRACK where NewRecordMin = '$newRecordMin' and NewRecordSec = '$newRecordSec' and NewRecordMs = '$newRecordMs' and EventGroupID = '$eventGroupID'";
					$result1 = $lsports->returnVector($sql1);
					
					if(empty($result1))
					{
						$sql = "UPDATE SWIMMINGGALA_EVENTGROUP_EXT_TRACK SET NewRecordMin = '$newRecordMin', NewRecordSec = '$newRecordSec', NewRecordMs = '$newRecordMs', NewRecordHolderName = '$sname', NewRecordHolderUserID = '$sid', NewRecordHouseID = '$hid' WHERE EventGroupID = '$eventGroupID'";
						$lsports->db_db_query($sql);
					}
				}
				else if($value <= $standard && $standard)
				{
					$resultArr[$sid]["status"] = 3;
				}
				else 
				{
					$resultArr[$sid]["status"] = 2;
				}
			}
			else
			{
				# Retrieve the updated new record for compare
				$sql = "SELECT NewRecordMetre FROM SWIMMINGGALA_EVENTGROUP_EXT_FIELD Where EventGroupID = '$eventGroupID'";
				$newRecord = $lsports->returnArray($sql, 1);
				$check_record = ($newRecord[0][0]==""||$newRecord[0][0]==0) ? $record : $newRecord[0][0]; 
                
				if($value > $check_record)
				{
					$resultArr[$sid]["status"] = 4;

					# get record year
					$today = getdate();
					$year = $today["year"];

					#get record House ID
					$h_info = $lsports->retrieveStudentHouseInfo($sid);
					$hid = $h_info[0][1];
							
					$sname = $resultArr[$sid]["student_name"];
					$newRecordMetre = $resultArr[$sid]["metre"];

					# udpate broken record metre
					$sql = "UPDATE SWIMMINGGALA_EVENTGROUP_EXT_FIELD SET NewRecordMetre = '$newRecordMetre', NewRecordHolderName = '$sname', NewRecordHolderUserID = '$sid', RecordHouseID = '$hid' WHERE EventGroupID = '$eventGroupID'";
					$lsports->db_db_query($sql);
				}
				else if($value >= $standard && $standard)
				{
					$resultArr[$sid]["status"] = 3;
				}
				else
				{
					$resultArr[$sid]["status"] = 2;
				}
			}
		}
		else 
		{
            $resultArr[$sid]["status"]=2;
		}
	}
}
else
{
	if($other=="4")
		$resultArr[$sid]["status"] = 5;
	else if($other=="5")
		$resultArr[$sid]["status"] = 1;
} 

	###############################
	# value for selection $order
	# 1 RecordBroken
	# 2 Qualified
	# 3 Unsuitable
	# 4 Foul
	# 5 Not Attend
	# 0 - -
	###############################
	
	###############################
	# value for STATUS in DB
	# 4 RecordBroken
	# 3 Qualified
	# 2 Unsuitable
	# 5 Foul
	# 1 Not Attend
	###############################
	
# Update Result Records
foreach($resultArr as $sid => $value)
{	
	if($value["status"]=="")
	{
		if($value["attend"]==2)
			$value["status"] = RESULT_STATUS_ABSENT;
		else if($value["attend"]==3)
			$value["status"] = RESULT_STATUS_WAIVEDABSENT;
		else if($value["other"]==4)
			$value["status"] = RESULT_STATUS_FOUL;
	}

	if($eventType == EVENT_TYPE_TRACK) //track
	{
		$fields = "ResultMin = '".$value['min']."', ";
		$fields .= "ResultSec = '".$value['sec']."', ";
		$fields .= "ResultMs = '".$value['ms']."', ";
		$fields .= "RecordStatus = ".$value['status'].", ";
		$fields .= "AbsentReason = '".$value['absent_reason']."', ";
		
		if($value['attend']==1)
		{
			//if(($roundType==0) || ($roundType==1  && $total_heat_num==1))
			//if(($roundType==0) || ($roundType==1 && $finalReq==0))	# even only have 1 total heat num, still need to according to the usering setting
			if($roundType==ROUND_TYPE_FINALROUND) //modified by marcus 16/6
			{
				$RankIsGenerated = 1;
				$fields .= ($value["status"]!=5 && $value["status"]!=1) ? "Rank = '".$value['rank']."', " : "Rank = NULL, ";		
			}
			else
			{
				$fields .= "Rank = NULL, ";
			}
		}
        else
        {
			$fields .= "Rank = NULL, ";
		}
		/*
		if(($roundType==0 && $value['attend']==1) || ($roundType==1  && $total_heat_num==1 && $value['attend']==1))
			$fields .= ($value["status"]!=5 && $value["status"]!=1) ? "Rank = '".$value['rank']."', " : "Rank = NULL, ";
        else
			$fields .= "Rank = NULL, ";
		*/
		$fields .= "DateModified = now()";
	}
	else //field
	{
		$fields = "ResultMetre = '".$value['metre']."', ";
		$fields .= "RecordStatus = ".$value['status'].", ";
		$fields .= "AbsentReason = '".$value['absent_reason']."', ";
		
		//if(($roundType==0 && $value['attend']==1) || ($roundType==1  && $total_heat_num==1 && $value['attend']==1))
		if($roundType==ROUND_TYPE_FINALROUND)
		{
		   $fields .= ($value["status"]!=5 && $value["status"]!=1) ? "Rank = '".$value['rank']."', " : "Rank = NULL, ";
		}
		else
           $fields .= "Rank = NULL, ";
		$fields .= "DateModified = now()";
	}
	$sql = "UPDATE SWIMMINGGALA_LANE_ARRANGEMENT SET $fields WHERE EventGroupID = '$eventGroupID' AND StudentID = '$sid' AND Heat = '$currHeat' AND RoundType = '$roundType'";
//	debug_pr($sql);
	$lsports->db_db_query($sql);
	
	##########################
	# Direct go to final
	if(isset($directFinal)&&$directFinal=="1")
	{
		# get results of first round
		$sqlFirst = "SELECT EventGroupID,0 as RoundType,StudentID,Heat,ArrangeOrder,ResultMin,ResultSec,";
		$sqlFirst.= "ResultMs,RecordStatus,Rank,DateModified FROM SWIMMINGGALA_LANE_ARRANGEMENT WHERE EventGroupID = '$eventGroupID' AND StudentID = '$sid' AND Heat = '$currHeat' AND RoundType = '$roundType'";
		$resultFirst = $lsports->returnArray($sqlFirst,11);
		
		# delete results of Final Round
		$sqlDelFinal = "DELETE FROM SWIMMINGGALA_LANE_ARRANGEMENT WHERE EventGroupID = '$eventGroupID' AND StudentID = '$sid' AND Heat = '$currHeat' AND RoundType = '0'";
		$lsports->db_db_query($sqlDelFinal);
		
		# insert results of First Round to Final Round
		$sqlFinal = "INSERT INTO SWIMMINGGALA_LANE_ARRANGEMENT(";
		$sqlFinal.=" EventGroupID,RoundType,StudentID,Heat,ArrangeOrder,ResultMin,";
		$sqlFinal.=" ResultSec,ResultMs,RecordStatus,Rank,DateModified) VALUES(";

		for($j=0;$j<count($resultFirst[0]);$j++){
			if(trim($resultFirst[0][$j])=='') continue;
			$sqlFinal.="'".$resultFirst[0][$j]."'";
			if($j==count($resultFirst[0])-1)
				$sqlFinal.=")";
			else $sqlFinal.=",";
		}
		
		$lsports->db_db_query($sqlFinal);
	}
}


#############################
# Calculate the Attendant Score of Student and enrolment score
// if($roundType==1)
// {
	# return attendant score standard
	$scoreEnrol = $lsports->scoreEnrol;
	$scorePresent = $lsports->scorePresent;
	$scoreAbsent = $lsports->scoreAbsent;

	foreach($resultArr as $sid => $value)
	{
		# retrieve record status of student in the first round (only first round will count the attendance score)
		$sql = "SELECT RecordStatus FROM SWIMMINGGALA_LANE_ARRANGEMENT WHERE EventGroupID = '$eventGroupID' AND StudentID = '$sid' AND RoundType = '$roundType'";
		$row = $lsports->returnVector($sql);
		$record_status = $row[0];

		# Calculate the attendance scores
		if($resultArr[$sid]['attend']!=NULL && $resultArr[$sid]['attend']!="")
		{
			$score = 0;
			if($roundType==ROUND_TYPE_FIRSTROUND)
			{
				$score += $scoreEnrol;
				if($resultArr[$sid]['attend']==1)
				{
					$score += $scorePresent;
				}
				
				if($resultArr[$sid]['attend']==2)
				{
					$score = $score - $scoreAbsent;
				}
			}
			
			$sql = "UPDATE SWIMMINGGALA_LANE_ARRANGEMENT SET Score = '$score' WHERE EventGroupID = '$eventGroupID' AND StudentID = '$sid' AND RoundType = '$roundType'";
			$lsports->db_db_query($sql);
			
			# Update Final Round also(when number of participants <=lane number)
			if(isset($directFinal)&&$directFinal=="1"){
				$sql2 = "UPDATE SWIMMINGGALA_LANE_ARRANGEMENT SET Score = '$score' WHERE EventGroupID = '$eventGroupID' AND StudentID = '$sid' AND RoundType = 0";
				$lsports->db_db_query($sql2);
			}
		}
	}
// }

########## Calculate Score of Record Broken or Standard Record ##################
if($Ranking!=null)
{
	$ranked_sid = array_keys($Ranking);

	# Retrieve score standard 
	$sql = "SELECT ScoreStandardID FROM SWIMMINGGALA_EVENTGROUP WHERE EventGroupID = '$eventGroupID'";
	$temp = $lsports->returnVector($sql);
	$ScoreStandardDetail = $lsports->retrieveScoreStandardDetail($temp[0]);

	######### Customization [Start] ##########
	# 2009-12-07 YatWoon
	# if there is 3 students = rank 1, then the score need to be avg of (1st + 2nd + 3rd's score)
	# assumption, they select the ranking display should be 1,2,2,4
	if($sys_custom['eSports_same_position_avg_score'] && $lsports->RankPattern!="1223")
	{
		$rank_couting = array();
		foreach($resultArr as $k=>$d)
		{
			if($d['rank'])
			{
				if($rank_couting[$d['rank']])
					$rank_couting[$d['rank']]++;
				else
					$rank_couting[$d['rank']] = 1;
			}
		}
        
		foreach($rank_couting as $k=>$d)
		{
			$temp = 0;
 			for($i=$k;$i<$k+$d;$i++)
 				$temp += $ScoreStandardDetail[$i];
 			$ScoreStandardDetail[$k] = my_round($temp / $d, 1);
		}
	}
	######### Customization [End] ##########
	
	# 20081022 - fix Rank 9 and 10 will calculated with "Record Broken" and "Qualified"
	$RecordBroken = $ScoreStandardDetail[9];
// 	$Qualified = $ScoreStandardDetail[10];
	$Qualified = $standard ? $ScoreStandardDetail[10] : 0;
    
	# clear the "Record Broken" and "Qualified" score in array
	$ScoreStandardDetail[9] = 0;
	$ScoreStandardDetail[10] = 0;
    
	# clear old Score
	$ranked_sid_sql = implode(",",(array)$ranked_sid);
	$sql = "update SWIMMINGGALA_LANE_ARRANGEMENT set Score=0 WHERE EventGroupID = '$eventGroupID' and RoundType=0 AND StudentID IN (ranked_sid_sql)";
	$lsports->db_db_query($sql);
	foreach($ranked_sid as $key => $sid)
	{
		$score = 0;

		if($resultArr[$sid]['status']==RESULT_STATUS_RECORDBROKEN )
		{
			$score = $score + $RecordBroken;
			
			# 2013-08-28
			# fixed: change back old record breaker's status and score
			$sql = "SELECT StudentID, RoundType, ResultMin, ResultSec, ResultMS, Score FROM SWIMMINGGALA_LANE_ARRANGEMENT WHERE EventGroupID = '$eventGroupID' AND RecordStatus = 4 AND StudentID != '$sid'
				UNION SELECT StudentID, RoundType, ResultMin, ResultSec, ResultMS, Score FROM SWIMMINGGALA_LANE_ARRANGEMENT WHERE EventGroupID = '$eventGroupID' AND RecordStatus = 4 AND StudentID = '$sid' AND RoundType != '$roundType'";
			$oldBreakRecordresultArr = $lsports->returnArray($sql);
			foreach ((array)$oldBreakRecordresultArr as $data) {
				$thisStudentID = $data['StudentID'];
				$thisRoundType = $data['RoundType'];
				$thisScore = $data['Score'];
				
				$thisResultRecord = $lsports->Convert_TimeArr_To_Millisecond(array($data['ResultMin'], $data['ResultSec'], $data['ResultMS']));
				$thisRecordStatus = ($thisResultRecord <= $standard && $standard) ? 3 : 2;				
				$thisScore = $thisScore - $RecordBroken;
				
				$sql = "UPDATE SWIMMINGGALA_LANE_ARRANGEMENT SET Score = '$thisScore', RecordStatus = '$thisRecordStatus' WHERE EventGroupID = '$eventGroupID' AND RecordStatus = 4 AND StudentID = '$thisStudentID' AND RoundType = '$thisRoundType'";
				$lsports->db_db_query($sql);
			}
		}
		
		// avoid double count present/enrol score
		$ObtainedBonusScore = $lsports->Get_Obtained_Bonus_Score($roundType,$eventGroupID,$sid);
		
		# Calculate the score by checking the status
		$score = $score + $scoreEnrol + $scorePresent;
		if(($resultArr[$sid]['status']==RESULT_STATUS_RECORDBROKEN || $resultArr[$sid]['status']==RESULT_STATUS_QUALIFY))
		{
			$score = $score + $Qualified;
		}
		if($score <= $ObtainedBonusScore)
		{
			$score = 0;
		}
		else
		{
			$score = $score-$ObtainedBonusScore;
		}

		$fields = "";
		
		# Calculate Ranking and Scores if it is the final round
		if($roundType == 0 ||(isset($directFinal)&&$directFinal=="1")) 
		{
			if($ScoreStandardDetail[$resultArr[$sid]['rank']] != "")
			{
				$score = $score + $ScoreStandardDetail[$resultArr[$sid]['rank']];
			}
				
		}
	
		$fields .= "Score = ".$score.", ";
		$fields .= "DateModified = now()";

		$sql = "UPDATE SWIMMINGGALA_LANE_ARRANGEMENT SET $fields WHERE EventGroupID = '$eventGroupID' AND StudentID = '$sid' AND RoundType = '$roundType'";
		$lsports->db_db_query($sql);
		
		if(isset($directFinal)&&$directFinal=="1"){
			$sql2 = "UPDATE SWIMMINGGALA_LANE_ARRANGEMENT SET $fields WHERE EventGroupID = '$eventGroupID' AND StudentID = '$sid' AND RoundType = '0'";
			$lsports->db_db_query($sql2);
		}
	}
	
	if(($roundType==1 && $finalReq==0 )||$directFinal) // gen ranking for event not require final
	{
		if($roundType==1 && $finalReq==0)
		{
			$sql = "DELETE FROM SWIMMINGGALA_LANE_ARRANGEMENT WHERE EventGroupID = '$eventGroupID' AND RoundType = '0'";
			$lsports->db_db_query($sql);
		}	
		$sql="SELECT COUNT(*) AS SUM FROM SWIMMINGGALA_LANE_ARRANGEMENT WHERE EventGroupID = '$eventGroupID' and RecordStatus IS NULL ";
		$result=$lsports->returnArray($sql,1);
		
		if($result[0]["SUM"]==0) //Check whether all heats are finished
		{
			if($eventType == 1)
			{
				$eventresult = " ResultMin*60*100+ResultSec*100+ResultMS ";
				$table = " SWIMMINGGALA_LANE_ARRANGEMENT ";
				$orderBy = " ASC ";
			}
			else
			{
				$eventresult = " ResultMetre ";
				$table = " SWIMMINGGALA_LANE_ARRANGEMENT ";
				$orderBy = " DESC ";
			}
			
			$sql = "SELECT StudentId, $eventresult AS EventResult, RecordStatus from SWIMMINGGALA_LANE_ARRANGEMENT";
			$sql .= " WHERE EventGroupID = '$eventGroupID' and RecordStatus in (2,3,4) ";
			$sql .= "order by ($eventresult) $orderBy";		
			$result = $lsports->returnArray($sql,8);
//			debug_pr($result);
			$ctr=0;
			$temp_rank = 0;
			if(!empty($result))
			{
				for($k=0;($k<sizeof($result)+$ctr&&$k<8) ||($result[$k]['EventResult']==$result[$k-1]['EventResult']&&!empty($result[$k]['EventResult']));$k++)
				{
					list($sid,$time,$status)=$result[$k];
					if(!$sid) break;
					
					switch($status)
					{
						case 2: $score = $scoreEnrol + $scorePresent; break;
						case 3: $score = $scoreEnrol + $Qualified + $scorePresent ; break;
						case 4: $score = $scoreEnrol + $Qualified + $scorePresent + $RecordBroken ;break;
					}
	
					if($tie_breaker_rank[$sid] && $isHighJump)
					{
						$rank = $tie_breaker_rank[$sid];
					}
					else
					{
						//debug_pr($time."==".$result[$k-1]['EventResult']."=>".($time==$result[$k-1]['EventResult']));
//						if($time==$result[$k-1]['EventResult']) //check if record same as neighbour
//							$ctr++;
//						$rank=$k+1-$ctr;	//
						if($lsports->RankPattern=="1223")
						{
							if($last_sid!==""&&$result[$k]['EventResult']==$result[$last_sid]['EventResult']) //check if record same as neighbour
							{
								$rank = $temp_rank;
							}
							else
							{
								$rank = (++$temp_rank);
							}
							$last_sid=$k; 
						}
						else
						{ 
							++$temp_rank;
							if($last_sid!==""&&$result[$k]['EventResult']==$result[$last_sid]['EventResult']) //check if record same as neighbour
							{
								$rank = $last_rank;
							}
							else
							{
								$rank = $temp_rank;
								$last_rank = $temp_rank;
							}
							$last_sid=$k;
						
						}

					}
					
					if($ScoreStandardDetail[$rank] != "")
						$score += $ScoreStandardDetail[$rank];
					$sql = "UPDATE SWIMMINGGALA_LANE_ARRANGEMENT SET Rank = $rank, Score = $score WHERE EventGroupID = '$eventGroupID' and StudentID = '$sid' ";
                    //debug_pr($sql);
					$lsports->db_db_query($sql);
				}
				$RankIsGenerated = 1;
			}
		}
	}
}

intranet_closedb();
$msg = $RankIsGenerated ? "sports_ranking_generated" : "update";
header ("Location: input_tf_record.php?eventGroupID=$eventGroupID&xmsg=$msg");
?>