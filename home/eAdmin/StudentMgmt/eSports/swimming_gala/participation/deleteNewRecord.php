<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libswimminggala.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$lswimminggala = new libswimminggala();
$lswimminggala->authSportsSystem();

if($eventType==1)
{
	$sql = "UPDATE SWIMMINGGALA_EVENTGROUP_EXT_TRACK SET NewRecordMin = NULL, NewRecordSec = NULL, NewRecordMs = NULL, NewRecordHolderName = NULL, NewRecordHolderUserID = NULL, NewRecordHouseID = NULL WHERE EventGroupID = '$eventGroupID'";
	$lswimminggala->db_db_query($sql);
}
else if($eventType==3 || $eventType==4)
{
	$sql = "UPDATE SWIMMINGGALA_EVENTGROUP_EXT_RELAY SET NewRecordMin = NULL, NewRecordSec = NULL, NewRecordMs = NULL, NewRecordHolderName = NULL, NewRecordHouseID = NULL WHERE EventGroupID = '$eventGroupID'";
	$lswimminggala->db_db_query($sql);
}

intranet_closedb();
if($eventType==3)
	header ("Location:relay_record_detail.php?eventGroupID=$eventGroupID&msg=1");
else if($eventType==4)
	header ("Location:class_relay_record_detail.php?eventGroupID=$eventGroupID&msg=1");
else
	header ("Location:input_tf_record.php?eventGroupID=$eventGroupID&xmsg=New_Record_Delete");
?>