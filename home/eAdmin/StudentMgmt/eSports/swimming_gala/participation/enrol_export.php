<?
# using: 
/*
 * 2017-10-26 (Carlos): Get house name with libsports.php retrieveStudentHouseInfo() instead of retrieveStudentHouse().
 */

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libswimminggala.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

$lsports = new libswimminggala();
$lexport = new libexporttext();


$result = array();

# header
$header_ary = array("student name (Eng)", "student name (Chi)","Class","Grade","House", "Event Participated", "School Year","Result");
$result[] = $header_ary;

$students = $lsports->retrieveStudentInfo($engName, $chiName, $theClass, $ageGroup, $house, " d.Sequence, a.ClassName, a.ClassNumber ");
$StudentInfoArr = BuildMultiKeyAssoc($students,"UserID");

# age group Info
$AgeGroupInfo = BuildMultiKeyAssoc((array)$lsports->retrieveAgeGroupInfo(), "AgeGroupID");

# Current Year
$CurrentYear = getCurrentAcademicYear();

$participateEventArr = $lsports->retrieveStudentEnroledWithBestResultArr( $excludeOpenEvent = 0, $presentOnly=1,$PassStandardOnly=1);
foreach($participateEventArr as $key=>$participateEvent)
{
	list($tmp_UserID, $engEvent, $chiEvent, $EventResult) = $participateEvent;
	
	if(empty($StudentInfoArr[$tmp_UserID]))
		continue;
	else	
		list($tmp_UserID, $tmp_EnName, $tmp_ChiName, $tmp_EnClass, $tmp_ChiClass, $tmp_ClassNumber, $Gender) = $StudentInfoArr[$tmp_UserID];

    $tmpResult = array();
	
	#Eng Student Name
	$tmpResult[] = $tmp_EnName;

	#Chi Student Name
	$tmpResult[] = $tmp_ChiName;

	# Class
	$tmpResult[] = $tmp_EnClass;

	# Grade
	$tmp_AgeGroup = $lsports->retrieveAgeGroupByStudentID($tmp_UserID);
	$tmp_grade = $AgeGroupInfo[$tmp_AgeGroup]['GroupCode'];
	$tmpResult[] = $tmp_grade;

	# House
	//$tmp_house = $lsports->retrieveStudentHouse($tmp_UserID);
	$house_info = $lsports->retrieveStudentHouseInfo($tmp_UserID);
	$tmp_house = $house_info[0][0];
	$tmpResult[] = $tmp_house;
	
	#Event 
	$tmpResult[] = $engEvent;
	
	# School Year
	$tmpResult[] = $CurrentYear;
	
	# Result
	$tmpResult[] = $lsports->Format_Millisecond($EventResult);
	
	$result[] = $tmpResult;

}

$utf_content = "";
foreach($result as $k=>$d)
{
	$utf_content .= implode("\t", $d);
	$utf_content .= "\r\n";
}

$filename = "swimminggala_student_paticipation_cetificate.csv";
$lexport->EXPORT_FILE($filename, $utf_content);

intranet_closedb();
?>
