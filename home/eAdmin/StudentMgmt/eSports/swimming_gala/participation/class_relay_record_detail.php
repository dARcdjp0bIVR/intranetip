<?php
# using: yat
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libswimminggala.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$linterface 	= new interface_html();
$CurrentPage	= "PageRaceResult";

$lswimminggala = new libswimminggala();
$lswimminggala->authSportsSystem();

# Retrieve Info of the Class Relay Event
$sql = "SELECT GroupID, EventID FROM SWIMMINGGALA_EVENTGROUP Where EventGroupID = '$eventGroupID'";
$tempGroupID = $lswimminggala->returnArray($sql, 2);

if($tempGroupID[0][0]>0)
{
  $EventGroupInfo = $lswimminggala->retrieveEventAndGroupNameByEventGroupID($eventGroupID);
  $eventName = $EventGroupInfo[0];
  $ageGroupName  = $EventGroupInfo[1];
  $eventType = $EventGroupInfo[2];
}
else
{
	$eventID = $tempGroupID[0][1];
	 $db_field = ($intranet_session_language=="en"?"EnglishName":"ChineseName");

	$sql = "SELECT EventType, $db_field FROM SWIMMINGGALA_EVENT WHERE EventID = '$eventID'";
	$temp = $lswimminggala->returnArray($sql, 2);

	$eventName = $temp[0][1];
	if($tempGroupID[0][0] == '-1')
		$ageGroupName = $i_Sports_Event_Boys_Open;
	else if($tempGroupID[0][0] == '-2')
		$ageGroupName = $i_Sports_Event_Girls_Open;
	else if($tempGroupID[0][0] == '-4')
		$ageGroupName = $i_Sports_Event_Mixed_Open;
	$eventType = $temp[0][0];
}


$ExtInfo = $lswimminggala->retrieveEventGroupExtInfo($eventGroupID, $eventType);

$deleteLink = "deleteNewRecord.php?eventGroupID=$eventGroupID&eventType=$eventType";

if($ExtInfo["NewRecordMin"]!=NULL && $ExtInfo["NewRecordSec"]!=NULL && $ExtInfo["NewRecordMs"]!=NULL)
{
	$ExtInfo["NewRecordMin"] = (strlen($ExtInfo["NewRecordMin"])==1) ? "0".$ExtInfo["NewRecordMin"] : $ExtInfo["NewRecordMin"];
	$ExtInfo["NewRecordSec"] = (strlen($ExtInfo["NewRecordSec"])==1) ? "0".$ExtInfo["NewRecordSec"] : $ExtInfo["NewRecordSec"];
	$ExtInfo["NewRecordMs"] = (strlen($ExtInfo["NewRecordMs"])==1) ? "0".$ExtInfo["NewRecordMs"] : $ExtInfo["NewRecordMs"];

	$new_record = $ExtInfo["NewRecordMin"];
	$new_record .= "'".$ExtInfo["NewRecordSec"];
	$new_record .= "''".$ExtInfo["NewRecordMs"];
	        $new_record .= "&nbsp;&nbsp;&nbsp;".$linterface->GET_BTN($button_remove, "button", "click_delete();","submit2");
}
else
{
	$new_record = "&nbsp;";
}

$ExtInfo["RecordMin"] = (strlen($ExtInfo["RecordMin"])==1) ? "0".$ExtInfo["RecordMin"] : $ExtInfo["RecordMin"];
$ExtInfo["RecordSec"] = (strlen($ExtInfo["RecordSec"])==1) ? "0".$ExtInfo["RecordSec"] : $ExtInfo["RecordSec"];
$ExtInfo["RecordMs"] = (strlen($ExtInfo["RecordMs"])==1) ? "0".$ExtInfo["RecordMs"] : $ExtInfo["RecordMs"];

$record = $ExtInfo["RecordMin"];
$record .= "'".$ExtInfo["RecordSec"];
$record .= "''".$ExtInfo["RecordMs"];

$ExtInfo["StandardMin"] = (strlen($ExtInfo["StandardMin"])==1) ? "0".$ExtInfo["StandardMin"] : $ExtInfo["StandardMin"];
$ExtInfo["StandardSec"] = (strlen($ExtInfo["StandardSec"])==1) ? "0".$ExtInfo["StandardSec"] : $ExtInfo["StandardSec"];
$ExtInfo["StandardMs"] = (strlen($ExtInfo["StandardMs"])==1) ? "0".$ExtInfo["StandardMs"] : $ExtInfo["StandardMs"];

$standard = $ExtInfo["StandardMin"];
$standard .= "'".$ExtInfo["StandardSec"];
$standard .= "''".$ExtInfo["StandardMs"];

$arrangeDetail = $lswimminggala->returnCRLaneArrangeDetailByEventGroupID($eventGroupID);

######################################################################
################# Lane Arrangement Table #############################
######################################################################
$rows_per_table=0;

$info = "";
$info .= "<form name='form1' action='class_relay_record_update.php' method='post'>";
$info .= "<table width='80%' border='0' cellspacing='0' cellpadding='4'>";
		
/*
### Title
$info .= "<tr>";
$info .= "<td colspan='6' align='left' class='tablename'>&nbsp;</td>";
$info .= "</tr>";	
*/

### space
$info .= "<tr><td colspan='4'><img src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif' width='10' height='10' /></td></tr>";

### header                
$info .= "<tr>";
$info .= "<td class='tablegreentop tabletoplink'>&nbsp;</td>";
$info .= "<td class='tablegreentop tabletoplink'>". $i_general_class ."</td>";
$info .= "<td class='tablegreentop tabletoplink'>". $i_Sports_Record ."</td>";
$info .= "<td class='tablegreentop tabletoplink'>". $i_Sports_Other ."</td>";
$info .= "</tr>";                

$displayContent = "";
$status = 0;
$arr_order="";

for($i=0; $i<sizeof($arrangeDetail); $i++)
{
	list($cname_en,$cname_b5, $order, $cid) = $arrangeDetail[$i];
	$cname = Get_Lang_Selection($cname_en,$cname_b5);
	
	 $sql = "SELECT ResultMin, ResultSec, ResultMs, RecordStatus FROM SWIMMINGGALA_CLASS_RELAY_LANE_ARRANGEMENT WHERE EventGroupID = $eventGroupID AND ArrangeOrder = '$order'";
	$result = $lswimminggala->returnArray($sql, 4);

	if(sizeof($result)!=0)
	{
		$result_min = (strlen($result[0][0])==1) ? "0".$result[0][0] : $result[0][0];
		$result_sec = (strlen($result[0][1])==1) ? "0".$result[0][1] : $result[0][1];
		$result_ms = (strlen($result[0][2])==1) ? "0".$result[0][2] : $result[0][2];
		$status = $result[0][3];		

		if($status==1 || $status==2)
			$other = 3;
		else if($status==3)
			$other = 2;
		else if($status==4)
			$other = 1;
		else if($status==5)
			$other = 4;
		else
			$other = 0;
	}
	
	$css = ($i%2?"":"2");
	$displayContent .= "<tr class='tablegreenrow". ( $i%2 + 1 )."'>";
	$displayContent .= "<td class='tabletext'>".$i_Sports_The.$order.$i_Sports_Line."</td>";
	$displayContent .= "<td class='tabletext'>". $cname ."</td>";
	$displayContent .= "<td class='tabletext' wrap><input type='text' name='result_min_".$order."' size='3' value='".$result_min."' class='tabletext' onChange='doValidation2()'> ' <input type='text' name='result_sec_".$order."' size='3'  value='".$result_sec."' class='tabletext' onChange='doValidation2()'> '' <input type='text' name='result_ms_".$order."' size='3' value='".$result_ms."' class='tabletext' onChange='doValidation2()'></td>";
	$displayContent .= "<td class='tabletext'><SELECT name='other_".$order."' onChange='doValidation()'>";
	$displayContent .= "<OPTION value='0' ".($other==0?'SELECTED':'')."> -- </OPTION>";
	$displayContent .= "<OPTION value='1' ".($other==1?'SELECTED':'').">".$i_Sports_RecordBroken."</OPTION>";
	$displayContent .= "<OPTION value='2' ".($other==2?'SELECTED':'').">".$i_Sports_Qualified."</OPTION>";
	$displayContent .= "<OPTION value='4' ".($other==4?'SELECTED':'').">".$i_Sports_Foul."</OPTION>";
	$displayContent .= "<OPTION value='3' ".($other==3?'SELECTED':'').">".$i_Sports_Unsuitable."</OPTION>";
	$displayContent .= "</SELECT></td>";
	$displayContent .= "</tr>";
	$arr_order[$i] .=$order;
	$rows_per_table++;

}
$info .= $displayContent;

$info .= "</td></tr>";
$info .= "<tr><td colspan='6' class='dotline'><img src='".$image_path."/".$LAYOUT_SKIN."/10x10.gif' width='10' height='1' /></td></tr>";
        
$info .= "<tr>";
$info .= "<td align='center' colspan='6' >";
$info .= $linterface->GET_ACTION_BTN($button_submit, "submit", "","submit2") . " ";
$info .= $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset");
$info .= "</td>";
$info .= "</tr>";
$info .= "</table><br>";
                       
$info .= "<input type='hidden' name='eventGroupID' value='".$eventGroupID."'>";
$info .= "<input type='hidden' name='order' value='".implode($arr_order,",")."'>";
$info .= "<input type='hidden' name='rows_per_table' value='".$rows_per_table."'>";
$info .= "</form>";

######################################################################
################# Lane Arrangement Table - End #######################
######################################################################

if($msg==1)	$xmsg2 = $i_Sports_New_Record_Delete;

### Title ###
$house_relay_name = $lswimminggala->retrieveEventTypeNameByID(3);
$class_relay_name = $lswimminggala->retrieveEventTypeNameByID(4);

$TAGS_OBJ[] = array($Lang['eSports']['Individual'],"../participation/tf_record.php", 0);
$TAGS_OBJ[] = array($house_relay_name,"../participation/relay_record.php", 0);
$TAGS_OBJ[] = array($class_relay_name,"../participation/class_relay_record.php", 1);
$TAGS_OBJ[] = array($i_Sports_menu_Report_EventRanking,"../report/event_rank.php", 0);
$TAGS_OBJ[] = array($i_Sports_menu_Report_HouseGroupScore,"../report/house_group.php", 0);
$TAGS_OBJ[] = array($i_Sports_menu_Report_GroupChampion,"../report/group_champ.php", 0);
$MODULE_OBJ = $lswimminggala->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();

$space = $intranet_session_language=="en"?" ":"";
$PAGE_NAVIGATION[] = array($button_edit.$space.$class_relay_name.$space.$i_Sports_Record , "");
?>
<script language="javascript">
<!--
function click_delete()
{
	if(confirm("<?=$i_Discipline_System_alert_remove_warning_level?>"))
        {	            
		window.location='<?=$deleteLink?>';				             
        }
}      
//-->

	function doValidation()
	{
		strForm		= 'form1';
		strMin 		= 'formObj.result_min_';
		strSec 		= 'formObj.result_sec_';
		strMs 		= 'formObj.result_ms_';
		strOther 	= 'formObj.other_';

		//formObj = eval(strForm);
		formObj = $("[name=form1]").get()[0];
		orderStr = formObj.order.value;
		orderObj=orderStr.split(",");

		r = parseInt(formObj.rows_per_table.value);

		for(i=0;i<r;i++){

			otherObj = eval(strOther+orderObj[i]);
			if(otherObj==null) 
				continue;
			selectedValue = otherObj.options[otherObj.selectedIndex].value;

			// Foul or Not Attend
			if(selectedValue==4 || selectedValue==5){
				eval(strMin+orderObj[i]+'.value=""');
				eval(strSec+orderObj[i]+'.value=""');
				eval(strMs+orderObj[i]+'.value=""');						
					
			}

		}
		
	}
	
	function doValidation2(){
		strForm		= 'form1';
		strMin 		= 'formObj.result_min_';
		strSec 		= 'formObj.result_sec_';
		strMs 		= 'formObj.result_ms_';
		strOther 	= 'formObj.other_';

		//formObj = eval(strForm);
		formObj = $("[name=form1]").get()[0];
		//alert(formObj)
		orderStr = formObj.order.value;
		orderObj=orderStr.split(",");

		r = parseInt(formObj.rows_per_table.value);

		for(i=0;i<r;i++){
			isNum = false;
				minObj = eval(strMin+orderObj[i]);
				secObj = eval(strSec+orderObj[i]);
				msObj = eval(strMs+orderObj[i]);
				if(minObj==null) continue;
				valMin = parseInt(minObj.value);
				valSec = parseInt(secObj.value);
				valMs  = parseInt(msObj.value);
				if(!isNaN(valMin)||!isNaN(valSec)||!isNaN(valMs)||valMin>0||valSec>0||valMs>0)
					isNum=true;
			if(isNum){
				otherObj = eval(strOther+orderObj[i]);
				valOther = otherObj.options[otherObj.selectedIndex].value;
				//if(valOther==4||valOther==3)
					otherObj.options[0].selected =true;

			}
		}
		
	}
</script>

<br />   

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	<td align="right" valign="bottom">
		<?=$linterface->GET_ACTION_BTN($button_print, "button", "window.open('../report/result_export_process.php?EventGroupID=$eventGroupID&isResult=1')");?>&nbsp;&nbsp;
		<?=$linterface->GET_ACTION_BTN($button_back, "button", "window.location='class_relay_record.php'");?>&nbsp;&nbsp;
	</td>
</tr>
<tr>
	<td colspan="2">
                <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr> 
                	<td><br />
				<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
                                <tr>
                                        <td align="right" valign="bottom" colspan="2"><?=$linterface->GET_SYS_MSG($xmsg,$xmsg2);?></td>
				</tr>
                                <tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_Sports_Item?> </span></td>
					<td class="tabletext"><?=$eventName?></td>
				</tr>
                                <tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_Sports_field_Group?> </span></td>
					<td class="tabletext"><?=$ageGroupName?></td>
				</tr>
                                <tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_Sports_Record?> </span></td>
					<td class="tabletext"><?=$record?></td>
				</tr>
                                <tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_Sports_New_Record?> </span></td>
					<td class="tabletext"><?=$new_record?></td>
				</tr>
                                <tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_Sports_Standard_Record?> </span></td>
					<td class="tabletext"><?=$standard?></td>
				</tr>
			</table>
			</td>
                </tr>
                </table>
	</td>
</tr>
<tr>
	<td colspan="2">        
                <table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
                <tr>
                	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
                </table>                                
	</td>
</tr>
</table>          

<?=$info?>

<br />

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>
