<?php
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libswimminggala.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

$lswimminggala = new libswimminggala();
$lswimminggala->authSportsSystem();

$EventGroupInfo = $lswimminggala->retrieveEventAndGroupNameByEventGroupID($eventGroupID);
$eventName = $EventGroupInfo[0];
$ageGroupName  = $EventGroupInfo[1];
$eventType = $EventGroupInfo[2];

$ExtInfo = $lswimminggala->retrieveEventGroupExtInfo($eventGroupID, $eventType);

if($eventType == 1)
{
	$record = $ExtInfo["RecordMin"];
	$record .= "''".$ExtInfo["RecordSec"];
	$record .= "''".$ExtInfo["RecordMs"];

	$standard = $ExtInfo["StandardMin"];
	$standard .= "''".$ExtInfo["StandardSec"];
	$standard .= "''".$ExtInfo["StandardMs"];
}
else
{
	$record = $ExtInfo["RecordMetre"];
	$standard = $ExtInfo["StandardMetre"];
}

include_once("../../../../templates/fileheader.php");
?>
<?=displayNavTitle($i_Sports_menu_Participation_TrackField,'tf_record.php')?>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="<?=$image_path?>/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
</table>

<table width=560 border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align="center">
<tr><td class=tableTitle width=40%><?=$i_Sports_Item?></td><td><?=$eventName?></td></tr>
<tr><td class=tableTitle width=40%><?=$i_Sports_field_Group?></td><td><?=$ageGroupName?></td></tr>
<tr><td class=tableTitle width=40%><?=$i_Sports_Record?></td><td><?=$record?></td></tr>
<tr><td class=tableTitle width=40%><?=$i_Sports_New_Record?></td><td>&nbsp;</td></tr>
<tr><td class=tableTitle width=40%><?=$i_Sports_Standard_Record?></td><td><?=$standard?></td></tr>
</table>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="<?=$image_path?>/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>
<br>

<?php

################# First Round Table ##################################
 $arrange = $lswimminggala->returnTFLaneArrangeDetailByEventGroupID($eventGroupID, 1, 1);

for($i=0; $i<sizeof($arrange); $i++)
{
	list($heat, $order, $sname, $sid) = $arrange[$i];
	# get the house name of the student
	$house = $lswimminggala->retrieveStudentHouseInfo($sid);

	# Retrieve result if exist
	if($eventType == 1)
	{
		 $sql = "SELECT ResultMin, ResultSec, ResultMs, RecordStatus FROM SPORTS_LANE_ARRANGEMENT WHERE EventGroupID = $eventGroupID and StudentID = '$sid'";
		 $result = $lswimminggala->returnArray($sql, 4);
		 $s_record = $result[0][0];
		 $s_record .= "''".$result[0][1];
		 $s_record .= "''".$result[0][2];
		 $s_status = $result[0][3];		
	}
	else
	{
		$sql = "SELECT ResultMetre, RecordStatus From SPORTS_LANE_ARRANGEMENT WHERE EventGroupID = $eventGroupID and StudentID = '$sid'";
		 $result = $lswimminggala->returnArray($sql, 2);
		 $s_record = $result[0][0];
		 $s_status = $result[0][1];
	}

	$arrangeDetail[$heat][$order]["student_name"] = $sname;
	$arrangeDetail[$heat][$order]["house_name"] = $house[0][0];
	$arrangeDetail[$heat][$order]["code"] = $house[0][2];
	$arrangeDetail[$heat][$order]["record"] = $s_record;
	$arrangeDetail[$heat][$order]["status"] = $s_status;
}

$tableContent1 = "";
if(sizeof($arrangeDetail) != 0)
{
	foreach($arrangeDetail as $heat => $value)
	{
		?>
		<form name=form_<?=$heat?> action='tf_record_update.php' method=post>
		<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
		<tr><td><img src="<?=$image_path?>/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
		</table>
		<table width=560 border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align="center">
		<tr>
		<td class=tableTitle width=10%><?=($eventType == 1? $i_Sports_Line : "&nbsp;")?></td>
		<td class=tableTitle width=25%><?=$i_Sports_Participant?></td>
		<td class=tableTitle width=20%><?=$i_Sports_Record?></td>
		<td class=tableTitle width=10%><?=$i_Sports_Present?></td>
		<td class=tableTitle width=10%><?=$i_Sports_Absent?></td>
		<td class=tableTitle width=20%><?=$i_Sports_Other?></td>
		</tr>
		<tr>
		<td colspan=6 align=center class=tableTitle><?=$i_Sports_The.$heat.$i_Sports_Group?> (<?=$i_Sports_First_Round?>)</td>
		</tr>		
		<?php
		foreach($value as $order => $detail)
		{
			$orderShow = ($eventType == 1)?$i_Sports_The.$order.$i_Sports_Line:$order;
			
			$color = ($detail["code"] == "") ? "FFFFFF" : $detail["code"];
			
			if($detail["status"]==1 || $detail["status"]==5)
			{
				$status = 3;
				$attend = 0;
			}
			else if($detail["status"]==2)
			{
				$status = 0;
				$attend = 1;
			}
			else if($detail["status"]==3)
			{
				$status = 2;
				$attend = 1;
			}
			else if($detail["status"]==4)
			{
				$status = 1;
				$attend = 1;
			}

			$tableContent1 .= "<tr bgcolor=#".$color.">";
			$tableContent1 .= "<td width=10%>".$orderShow."</td>";
			$tableContent1 .= "<td width=25%>".$detail["student_name"]."</td>";
			$tableContent1 .= "<td width=20%><input name=record_".$heat."_".$order." type=text size=10 value=".$detail["record"]."></td>";
			$tableContent1 .= "<td width=10%><input name=attend_".$heat."_".$order." type=radio value=1 ".($attend==1?'CHECKED':'')."></td>";
			$tableContent1 .= "<td width=10%><input name=attend_".$heat."_".$order." type=radio value=0 ".($attend==1?'CHECKED':'')."></td>";
			$tableContent1 .= "<td width=20%><SELECT name=other_".$heat."_".$order.">";
			$tableContent1 .= "<OPTION value=0 ".($status==0?'SELECTED':'')."> -- </OPTION>";
			$tableContent1 .= "<OPTION value=1 ".($status==1?'SELECTED':'').">".$i_Sports_RecordBroken."</OPTION>";
			$tableContent1 .= "<OPTION value=2 ".($status==2?'SELECTED':'').">".$i_Sports_Qualified."</OPTION>";
			$tableContent1 .= "<OPTION value=3 ".($status==3?'SELECTED':'').">".$i_Sports_Unsuitable."</OPTION>";
			$tableContent1 .= "</SELECT></td>";
			$tableContent1 .="</tr>";	
		}
		echo $tableContent1;
		unset($tableContent1);
	?>
	</td></tr>
	</table>
	<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
	<tr><td><img src="<?=$image_path?>/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
	</table>
	<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
	<tr><td><hr size=1></td></tr>
	<tr><td align="right" colspan=6>
	<?=btnReset('form'.$heat)."&nbsp;".btnSubmit()?>
	</td>
	</tr>
	</table>
	<br>
	</form>
	<?php
	}		
}

include_once("../../../../templates/filefooter.php");
intranet_closedb();
?>