<?php
//using : yat
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libswimminggala.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$linterface 	= new interface_html();
$CurrentPage	= "PageReport_House";

$lsports = new libswimminggala();
$lsports->authSportsSystem();

$AgeGroups = $lsports->retrieveAgeGroupIDNames();	# retrieve all age group ids and names
$houseStudentCount = $lsports->retrieveHouseStudentCount();
$houseEnrolCount = $lsports->retrieveHouseNumberOfEnrolmentAndScore();

## Order
for($i=1;$i<=3;$i++)
{
        $sortname[$i] 		= ${"sortname".$i}=="" ? "house_name" : ${"sortname".$i};
        $sortorder[$i]		= ${"sortorder".$i}=="" ? 0 : ${"sortorder".$i};
        $arrow_icon[$i]		= "icon_sort_". (${"sortorder".$i}==1?"d":"a")."";
        $mouseover_tag[$i] 	= "onMouseOver=\"MM_swapImage('sort_icon".$i."','','{$image_path}/{$LAYOUT_SKIN}/".$arrow_icon[$i]."_on.gif',1)\" onMouseOut='MM_swapImgRestore()'";
        $arrow_display[$i]	= "<img name='sort_icon1' id='sort_icon".$i."' src='{$image_path}/{$LAYOUT_SKIN}/".$arrow_icon[$i]."_off.gif' align='absmiddle'  border='0' />";
}        
#Export Content
$export_content .= $i_Sports_menu_Report_House."\n\n";

### Table 1
$t = 1;
$t1 = "";
$t1 .= "<table width='100%' border='0' cellpadding='0' cellspacing='0'><tr><td>";
$t1 .= "<table width='100%' border='0' cellpadding='4' cellspacing='0'>";
$t1 .= "<tr>";
$t1 .= "	<td class='tablebluetop tabletopnolink'><a ". ($sortname[$t]=="house_name"?$mouseover_tag[$t]:"")	." href=\"javascript:sort_order($t, 'house_name',". ($sortname[$t]=="house_name"? 	($sortorder[$t]==1?0:1):"0") .");\" class='tabletoplink'>$i_Sports_House ". 				($sortname[$t]=="house_name"? 	$arrow_display[$t]:"")."</a></td>";
$t1 .= "	<td class='tablebluetop tabletopnolink'><a ". ($sortname[$t]=="s_count"?$mouseover_tag[$t]:"")		." href=\"javascript:sort_order($t, 's_count', 	". ($sortname[$t]=="s_count"? 	($sortorder[$t]==1?0:1):"0") .");\" class='tabletoplink'>$i_Sports_Report_Total_Student_Count ". 		($sortname[$t]=="s_count"? 	$arrow_display[$t]:"")."</a></td>";
$t1 .= "	<td class='tablebluetop tabletopnolink'><a ". ($sortname[$t]=="e_count"?$mouseover_tag[$t]:"")		." href=\"javascript:sort_order($t, 'e_count', 	". ($sortname[$t]=="e_count"? 	($sortorder[$t]==1?0:1):"0") .");\" class='tabletoplink'>$i_Sports_Report_Enrolment_Count ". 			($sortname[$t]=="e_count"? 	$arrow_display[$t]:"")."</a></td>";
$t1 .= "	<td class='tablebluetop tabletopnolink'><a ". ($sortname[$t]=="score"?$mouseover_tag[$t]:"")		." href=\"javascript:sort_order($t, 'score', 	". ($sortname[$t]=="score"? 	($sortorder[$t]==1?0:1):"0") .");\" class='tabletoplink'>$i_Sports_field_Score ". 				($sortname[$t]=="score"? 	$arrow_display[$t]:"")."</a></td>";
$t1 .= "	<td class='tablebluetop tabletopnolink'><a ". ($sortname[$t]=="averageCount"?$mouseover_tag[$t]:"")	." href=\"javascript:sort_order($t, 'averageCount',". ($sortname[$t]=="averageCount"? 	($sortorder[$t]==1?0:1):"0") .");\" class='tabletoplink'>$i_Sports_Report_Average_Enrolment_Count ". 	($sortname[$t]=="averageCount"? $arrow_display[$t]:"")."</a></td>";
$t1 .= "</tr>";

$export_content .= $i_ClubsEnrollment_NumOfStudent."\n";
$export_content .= $i_Sports_House."\t";
$export_content .= $i_Sports_Report_Total_Student_Count."\t";
$export_content .= $i_Sports_Report_Enrolment_Count."\t";
$export_content .= $i_Sports_field_Score."\t";
$export_content .= $i_Sports_Report_Average_Enrolment_Count."\n";

## push the data into multi-array
if(sizeof($houseStudentCount)>0)
{
	$data1 = array();
	$EnrolCountArr = Array();
	foreach((array)$houseEnrolCount as $thishouseEnrolCount)
	{
		list($e_count, $score, $e_hid) = $thishouseEnrolCount;
		$EnrolCountArr[$e_hid] = array($e_count, $score,$e_hid);
	}
	
	for($i=0; $i<sizeof($houseStudentCount); $i++)
	{
		list($s_count, $s_hid, $house_name, $house_color) 	= $houseStudentCount[$i]; 
		list($e_count, $score,$e_hid) 				= $EnrolCountArr[$s_hid];
		
		if($s_hid == $e_hid && $s_count != 0)
		{
			# Calculate the average number of enrolment of this classlevel
			$averageCount = $e_count/$s_count;
			$averageCount = round($averageCount, 2);

					$data1[$i]['house_name']	= $house_name;
					$data1[$i]['house_color']	= $house_color;
					$data1[$i]['s_count'] 		= $s_count;
					$data1[$i]['e_count'] 		= $e_count;
					$data1[$i]['score'] 		= round($score,1);
					$data1[$i]['averageCount'] 	= $averageCount;
					
		}
	}

	foreach ($data1 as $key => $row) {
		$house_name_ary[$key]	= $row['house_name'];
		$house_color_ary[$key]	= $row['house_color'];
		$s_count_ary[$key] 		= $row['s_count'];
		$e_count_ary[$key] 		= $row['e_count'];
		$score_ary[$key] 		= $row['score'];
		$averageCount_ary[$key] 	= $row['averageCount'];
	}

	if(${$sortname[$t]."_ary"} != '')
		array_multisort( ${$sortname[$t]."_ary"}, $sortorder[$t]==1?SORT_DESC:SORT_ASC, $data1);
}

$display1 = "";
for($i=0; $i<sizeof($data1); $i++)
{
                $display1 .= "<tr class='tablebluerow".($i%2?"2":"1")."'>";
		$display1 .= "<td class='tabletext' width='20%'>".$lsports->house_flag2($data1[$i]['house_color'], $data1[$i]['house_name'])."</td>";
		$display1 .= "<td class='tabletext' width='20%'>".$data1[$i]['s_count']."</td>";
		$display1 .= "<td class='tabletext' width='20%'>".$data1[$i]['e_count']."</td>";
                $display1 .= "<td class='tabletext' width='20%'>".$data1[$i]['score']."</td>";
		$display1 .= "<td class='tabletext' width='20%'>".$data1[$i]['averageCount']."</td>";
		$display1 .= "</tr>";
		
		$export_content .=$data1[$i]['house_name']."\t";
		$export_content .= $data1[$i]['s_count']."\t";
		$export_content .= $data1[$i]['e_count']."\t";
		$export_content .= $data1[$i]['score']."\t";
		$export_content .= $data1[$i]['averageCount']."\n";
}

$t1 .= $display1;
$t1 .= "</table></td></tr>";
$t1 .= "<tr><td height='1' class='dotline'><img src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif' width='10' height='1'></td></tr>";
$t1 .= "</table><br />";
$export_content .= "\n";
### Table 1 - End

### Table 2
$export_content .= $Lang['eSports']['NoOfStudentsEnrolled']."\n";

$t = 2;
$t2 = "";
$t2 .= "<table width='100%' border='0' cellpadding='0' cellspacing='0'><tr><td>";
$t2 .= "<table width='100%' border='0' cellpadding='4' cellspacing='0'>";
$t2 .= "<tr>";
$t2 .= "<td class='tablebluetop tabletopnolink'><a ". ($sortname[$t]=="house_name"?$mouseover_tag[$t]:"")	." href=\"javascript:sort_order($t, 'house_name',". ($sortname[$t]=="house_name"? 	($sortorder[$t]==1?0:1):"0") .");\" class='tabletoplink'>$i_Sports_House(".$Lang['eSports']['NoOfStudentsEnrolled'].") ". 	($sortname[$t]=="house_name"? 	$arrow_display[$t]:"")."</a></td>";
$export_content .= "$i_Sports_House(".$Lang['eSports']['NoOfStudentsEnrolled'].")\t";

for($i=0; $i<sizeof($AgeGroups); $i++)
{
	$groupName = $AgeGroups[$i][1];
	$t2 .= "<td class='tablebluetop tabletopnolink'><a ". ($sortname[$t]=="AgeGroups".$i ? $mouseover_tag[$t] : "")	." href=\"javascript:sort_order($t, 'AgeGroups".$i."',". ($sortname[$t]=="AgeGroups".$i ? ($sortorder[$t]==1?0:1):"0") .");\" class='tabletoplink'>".$groupName. ($sortname[$t]=="AgeGroups".$i ? $arrow_display[$t]:"")."</a></td>";

	$export_content .= $groupName."\t";
}
$t2 .= "</tr>";
$export_content .= "\n";

## push the data into multi-array
if(sizeof($houseStudentCount)>0)
{
	$data2 = array();
	for($i=0; $i<sizeof($houseStudentCount); $i++)
	{
		list($s_count, $house_id, $house_name, $house_color)= $houseStudentCount[$i]; 
		$AGStudentCount = $lsports->retrieveAgeGroupParticipantCountByHouse($house_id);
			
			$data2[$i]['house_name']	= $house_name;
			$data2[$i]['house_color']	= $house_color;
			for($j=0; $j<sizeof($AgeGroups); $j++)
		{
			$agid = $AgeGroups[$j][0];
			
				if($AGStudentCount[$agid] != "")
						$data2[$i]['AgeGroups'.$j] 		= $AGStudentCount[$agid];
			else
				$data2[$i]['AgeGroups'.$j] 		= 0;
		}
	}
			
	foreach ($data2 as $key => $row) {
		$house_name_ary[$key]	= $row['house_name'];
		$house_color_ary[$key]	= $row['house_color'];
		
		for($j=0; $j<sizeof($AgeGroups); $j++)
			${"AgeGroups".$j."_ary"}[$key] 		= $row['AgeGroups'.$j];
	}
	if(${$sortname[$t]."_ary"})
		array_multisort( ${$sortname[$t]."_ary"}, $sortorder[$t]==1?SORT_DESC:SORT_ASC, $data2);
}

$display2 = "";
for($i=0; $i<sizeof($data2); $i++)
{
                $display2 .= "<tr class='tablebluerow".($i%2?"2":"1")."'>";
		$display2 .= "<td class='tabletext'>".$lsports->house_flag2($data2[$i]['house_color'], $data2[$i]['house_name'])."</td>";
		$export_content .= $data2[$i]['house_name']."\t";
		
		for($j=0; $j<sizeof($AgeGroups); $j++)
        {
			$display2 .= "<td class='tabletext'>".$data2[$i]['AgeGroups'.$j]."</td>";
			$export_content .= $data2[$i]['AgeGroups'.$j]."\t";
		}
		$display2 .= "</tr>";
		$export_content .= "\n";
}

$t2 .= $display2;
$t2 .= "</table></td></tr>";
$t2 .= "<tr><td height='1' class='dotline'><img src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif' width='10' height='1'></td></tr>";
$t2 .= "</table><br />";
$export_content .="\n";
### Table 2 - End

### Table 3
$export_content .= $i_Sports_Report_Enrolment_Count."\n";
$t = 3;
$t3 = "";
$t3 .= "<table width='100%' border='0' cellpadding='0' cellspacing='0'><tr><td>";
$t3 .= "<table width='100%' border='0' cellpadding='4' cellspacing='0'>";
$t3 .= "<tr>";
$t3 .= "<td class='tablebluetop tabletopnolink'><a ". ($sortname[$t]=="house_name"?$mouseover_tag[$t]:"")	." href=\"javascript:sort_order($t, 'house_name',". ($sortname[$t]=="house_name"? 	($sortorder[$t]==1?0:1):"0") .");\" class='tabletoplink'>$i_Sports_House($i_Sports_Report_Enrolment_Count) ". 	($sortname[$t]=="house_name"? 	$arrow_display[$t]:"")."</a></td>";
$export_content .= "$i_Sports_House($i_Sports_Report_Enrolment_Count)\t";
for($i=0; $i<sizeof($AgeGroups); $i++)
{
	$groupName = $AgeGroups[$i][1];
        $t3 .= "<td class='tablebluetop tabletopnolink'><a ". ($sortname[$t]=="AgeGroups".$i ? $mouseover_tag[$t] : "")	." href=\"javascript:sort_order($t, 'AgeGroups".$i."',". ($sortname[$t]=="AgeGroups".$i ? ($sortorder[$t]==1?0:1):"0") .");\" class='tabletoplink'>".$groupName. ($sortname[$t]=="AgeGroups".$i ? $arrow_display[$t]:"")."</a></td>";
		$export_content .= $groupName."\t";
}
$export_content .= "\n";
## push the data into multi-array
if(sizeof($houseStudentCount)>0)
{
	$data3 = array();
	for($i=0; $i<sizeof($houseStudentCount); $i++)
	{
		list($s_count, $house_id, $house_name, $house_color) = $houseStudentCount[$i]; 
		$AGEnrolCount = $lsports->retrieveAgeGroupNumberOfEnrolmentByHouse($house_id);
			
		$data3[$i]['house_name']	= $house_name;
			$data3[$i]['house_color']	= $house_color;
		for($j=0; $j<sizeof($AgeGroups); $j++)
		{
			$agid = $AgeGroups[$j][0];
			
			if($AGEnrolCount[$agid] != "")
							$data3[$i]['AgeGroups'.$j] 		= $AGEnrolCount[$agid];
			else
							$data3[$i]['AgeGroups'.$j] 		= 0;
		}
	}
			
	foreach ($data3 as $key => $row) {
		$house_name_ary[$key]	= $row['house_name'];
		$house_color_ary[$key]	= $row['house_color'];
		for($j=0; $j<sizeof($AgeGroups); $j++)
			${"AgeGroups".$j."_ary"}[$key] 		= $row['AgeGroups'.$j];
	}
	if(${$sortname[$t]."_ary"})
		array_multisort( ${$sortname[$t]."_ary"}, $sortorder[$t]==1?SORT_DESC:SORT_ASC, $data3);
}

$display3 = "";
for($i=0; $i<sizeof($data3); $i++)
{
                $display3 .= "<tr class='tablebluerow".($i%2?"2":"1")."'>";
		$display3 .= "<td class='tabletext'>".$lsports->house_flag2($data3[$i]['house_color'], $data3[$i]['house_name'])."</td>";
		$export_content .= $data3[$i]['house_name']."\t";
		for($j=0; $j<sizeof($AgeGroups); $j++)
		{
              	$display3 .= "<td class='tabletext'>".$data3[$i]['AgeGroups'.$j]."</td>";
				$export_content .= $data3[$i]['AgeGroups'.$j]."\t";
		}
		$display3 .= "</tr>";
		$export_content .= "\n";
}

$t3 .= $display3;
$t3 .= "</table></td></tr>";
$t3 .= "<tr><td height='1' class='dotline'><img src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif' width='10' height='1'></td></tr>";
$t3 .= "</table><br>";

### Table 3 - End

### Title ###
$TitleTitle1 = "<span class='contenttitle'>". $i_Sports_menu_Report_House ."</span>";
$TitleTitle = "<table width='100%' height='25' cellpadding='0' cellspacing='0' ><tr><td style=\"vertical-align: bottom;\" >".$TitleTitle1."</td></tr></table>";
$TAGS_OBJ[] = array($TitleTitle, "", 0);    

$MODULE_OBJ = $lsports->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();
?>

<script language="javascript">
<!--
function sort_order(x, xname, xorder)
{
	with(document.form1)
        {
        	eval("sortname"+x+".value	= xname;");
                eval("sortorder"+x+".value	= xorder;");
                //sortorder.value = xorder;
				document.form1.action="house.php";
				document.form1.method="POST";
                submit();
        }
}
//-->
</script>

<br />
<form name="form1" method="get" action="house.php">

<table width="100%" border="0" cellpadding="5" cellspacing="0">
<tr>
	<td>&nbsp;&nbsp;&nbsp;<?= $linterface->GET_NAVIGATION2($i_ClubsEnrollment_NumOfStudent) ?></td>
</tr>
<tr>
	<td align="center">
        	<table width="96%" border="0" cellpadding="5" cellspacing="0"><tr><td>
        	<!----------- start of the total student and enrolment count table --------------------------->
        	<?=$t1?>
                </td></tr></table>
	</td>
</tr>
<tr>
	<td>&nbsp;&nbsp;&nbsp;<?= $linterface->GET_NAVIGATION2($Lang['eSports']['NoOfStudentsEnrolled']) ?></td>
</tr>
<tr>
	<td align="center">
        	<table width="96%" border="0" cellpadding="5" cellspacing="0"><tr><td>
        	<!----------- start of house total participants table --------------------------->
        	<?=$t2?>
                </td></tr></table>
	</td>
</tr>
<tr>
	<td>&nbsp;&nbsp;&nbsp;<?= $linterface->GET_NAVIGATION2($i_Sports_Report_Enrolment_Count) ?></td>
</tr>
<tr>
	<td align="center">
        	<table width="96%" border="0" cellpadding="5" cellspacing="0"><tr><td>
        	<!----------- start of house enrolment table --------------------------->
        	<?=$t3?>
                </td></tr></table>
	</td>
</tr>
</table>
<br />

<input type="hidden" name="sortname1" value="<?=$sortname1?>">
<input type="hidden" name="sortorder1" value="<?=$sortorder1?>">
<input type="hidden" name="sortname2" value="<?=$sortname2?>">
<input type="hidden" name="sortorder2" value="<?=$sortorder2?>">
<input type="hidden" name="sortname3" value="<?=$sortname3?>">
<input type="hidden" name="sortorder3" value="<?=$sortorder3?>">
<table width="100%" border="0" cellpadding="5" cellspacing="0">
<tr>
	<td align='center'><?=$linterface->GET_ACTION_BTN($button_export, "submit", "document.form1.action='general_export.php'; document.form1.method='POST';","submit2")?></td>
</tr>

<input type="hidden" name="exportFileName" value="house">
<input type="hidden" name="exportContent" value="<?=$export_content?>">
</form>
</table>
<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>