<?php
# using: yat
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libswimminggala.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

$lexport = new libexporttext();
$lsports = new libswimminggala();
$lsports->authSportsSystem();

$EventExport = array();

$AgeGroups = $lsports->retrieveAgeGroupInfo();	# retrieve all age group ids and names
$AgeGroups[] = array('M');
$AgeGroups[] = array('F');

$ShowRankFromTop = 3;
$WholeSchoolGenderChamp = array();
for($i=0; $i<sizeof($AgeGroups); $i++)
{
	$result= array();
	$groupID = $AgeGroups[$i][0];
	if(in_array($groupID,array('M','F')))
	{
		if(!empty($WholeSchoolGenderChamp[$groupID]))
		{
			sortByColumn2($WholeSchoolGenderChamp[$groupID],"total",1,1);
			$result = (array)array_slice((array)array_values((array)$WholeSchoolGenderChamp[$groupID]),0,$ShowRankFromTop);
		}
		$groupName = $groupID=='M'?$Lang['eSports']['AllBoysGroup']:$Lang['eSports']['AllGirlsGroup'];
	}
	else
	{
		$groupName = $intranet_session_language=="en"?$AgeGroups[$i][2]:$AgeGroups[$i][3];
	    $result = $lsports->retrieveGroupTopTenScore($groupID);
	}
        
	$EventExport[] = array('',$groupName);
	
	if($WithResult)
		$EventExport[] = array($Lang['eSports']['Ranking'],$i_Sports_Participant, $i_Sports_Participant_Number, $i_Sports_House, $i_Sports_field_Score,$Lang['eSports']['Event'],$Lang['eSports']['Ranking'], $i_Sports_field_Score, $Lang['eSports']['Result']);
	else
		$EventExport[] = array($Lang['eSports']['Ranking'],$i_Sports_Participant, $i_Sports_Participant_Number, $i_Sports_House, $i_Sports_field_Score);
                
    
	if(sizeof($result)!=0)
    {
    	$EventGroupIDsArr = (array)$lsports->Get_EventGroup_Info();
                	$EventGroupIDsArr = BuildMultiKeyAssoc($EventGroupIDsArr,"EventGroupID",array("GroupID","EventID"));
		for($j=0; $j<sizeof($result); $j++)
		{
			list($sid, $sname, $hname, $colorCode, $athNum, $status, $score) = $result[$j];
			$WholeSchoolGenderChamp[$AgeGroups[$i]["Gender"]][$sid] = $result[$j];
				
			$EventResult = $lsports->retrieveStudentEventResult($sid);
//			debug_pr($EventResult);
			/*
			$details_table = "<table width='100%' border='0' cellspacing='0' cellpadding='2' id='table_$sid' style='display:none; border:1px solid #999999;'>";
			$details_table .= "<tr>";
			$details_table .= "<td class='tabletop tabletopnolink' >".$i_Sports_menu_Settings_TrackFieldEvent."</td>";
			$details_table .= "<td class='tabletop tabletopnolink' width='15%' align='center'>".$i_Sports_field_Rank."</td>";
			$details_table .= "<td class='tabletop tabletopnolink' width='15%' align='center'>".$i_Sports_field_Score."</td>";
			$details_table .= "</tr>";
			
			for($k=0;$k<sizeof($EventResult);$k++)
			{
				$css = ($k % 2)+1;
				//list($tempStudentID, $tempEventName, $tempRank, $tempScore) = $EventResult[$k];
				$tempRank = $EventResult[$k]['Rank'] ? $EventResult[$k]['Rank'] : "-";
				$tempScore = $EventResult[$k]['Score'] ? round($EventResult[$k]['Score'],1) : "-";
				$details_table .= "<tr class='tablerow". $css."'>";
				$details_table .= "<td>".$EventResult[$k]['event_name']."</td>";
				$details_table .= "<td align='center'>".$tempRank."</td>";
				$details_table .= "<td align='center'>".$tempScore."</td>";
				$details_table .= "</tr>";
			}
			$details_table .= "</table>";
			*/
			$score_display = round($score,1);
			$thisRank = $j+1;
			if($WithResult)
			{
				for($k=0;$k<sizeof($EventResult);$k++)
				{
					$tempRank = $EventResult[$k]['Rank'] ? $EventResult[$k]['Rank'] : "-";
					$tempScore = $EventResult[$k]['Score'] ? round($EventResult[$k]['Score'],1) : "-";
					$tempResult = $EventResult[$k]['Result'] ? $EventResult[$k]['Result'] : "-";
					$RecordBroken = $EventResult[$k]['RecordStatus']==4 ? "##" : "";
					$tempResult = $RecordBroken.$tempResult;
					$EventExport[] = array($thisRank, $sname, $athNum, $hname, $score_display, $EventResult[$k]['event_name'],$tempRank,$tempScore,$tempResult);
					$sname = $hname = $athNum = $score_display = $thisRank = "";	
				}
			}
			else
				$EventExport[] = array($thisRank, $sname, $athNum, $hname, round($score,1));
		}
		
		if($WithResult)
			$EventExport[] = array("##".$i_Sports_RecordBroken);
		$EventExport[] = "";
    }
    else
    {
	    $EventExport[] = array($i_no_record_exists_msg);
	    $EventExport[] = "";
    }
}

// debug_pr($EventExport);

$ExportArr = $EventExport;

$filename = "group_champion.csv";
$export_content = $lexport->GET_EXPORT_TXT($ExportArr, "", "\t", "\r\n", "\t", 0, "11");
$lexport->EXPORT_FILE($filename, $export_content);

intranet_closedb();
?>