<?php
# using: yat
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libswimminggala.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");

intranet_auth();
intranet_opendb();

$linterface 	= new interface_html();
$CurrentPage	= "PageRaceResult";

$lclass = new libclass();
$lswimminggala = new libswimminggala();
$lswimminggala->authSportsSystem();

$AgeGroups = $lswimminggala->retrieveAgeGroupInfo();	# retrieve all age group ids and names
$HouseInfo = $lswimminggala->retrieveHouseInfo();		# retrieve all houses info

for($i=0; $i<sizeof($AgeGroups); $i++)
{
	$groupID = $AgeGroups[$i][0];
	$groupName = $intranet_session_language=="en"?$AgeGroups[$i][2]:$AgeGroups[$i][3];
	$groupGender = $AgeGroups[$i][4];
	
	if($groupGender=="M")
	{
		$MGroup[] = $groupID;
		$table1GroupsHeader .= "<td class='eSporttdborder eSportprinttabletitle' align='center'>".$groupName."</td>";
	}
	else if($groupGender=="F")
	{
		$FGroup[] = $groupID;
		$table2GroupsHeader .= "<td class='eSporttdborder eSportprinttabletitle' align='center'>".$groupName."</td>";
	}
}


if($sys_custom['eSports_count_open_event_in_grade_score']!==true )
{
	# add open group
	$MGroup[] = '-1';
	$FGroup[] = '-2';
	
	$table1GroupsHeader .= "<td class='eSporttdborder eSportprinttabletitle' align='center'>".$i_Sports_Event_Boys_Open."</td>";
	$table2GroupsHeader .= "<td class='eSporttdborder eSportprinttabletitle' align='center'>".$i_Sports_Event_Girls_Open."</td>";
}
else
{
	$includeOpenEventScore = 1;
}

$HouseScoreSumArr = $lswimminggala->retrieveHouseScoreSumArr("","",$includeOpenEventScore);

### Table 1 (Boy)
$table1 = "";
$table1 .= "<tr>";
$table1 .= "<td class='eSporttdborder eSportprinttabletitle'>&nbsp;</td>";
$table1 .= $table1GroupsHeader;
$table1 .= "<td class='eSporttdborder eSportprinttabletitle' align='center'>".$i_Sports_field_Total_Score."</td>";
$table1 .= "</tr>";

//$MGroup[] = '-1';

$table_content = "";
$gender = "M";
for($i=0; $i<sizeof($HouseInfo); $i++)
{
	$houseID = $HouseInfo[$i][0];
	$houseName = ($intranet_session_language=="en") ? $HouseInfo[$i][2] : $HouseInfo[$i][3];
	$colorCode = $HouseInfo[$i][4];
	$maleHouseTotal = 0;

        $table_content .= "<tr>";
	$table_content .= "<td class='eSporttdborder eSportprinttabletitle' align='center'>".$houseName."</td>";
	
	for($k=0; $k<sizeof($MGroup); $k++)
	{
		$ageGroupID = $MGroup[$k];

		$score = round($HouseScoreSumArr[$houseID][$ageGroupID],1);

		$table_content .= "<td class='eSporttdborder eSportprinttext' align='center'>".$score."</td>";
		$maleHouseTotal = $maleHouseTotal + $score;

	}

	$table_content .= "<td class='eSporttdborder eSportprinttext' align='center'>".$maleHouseTotal."</td>";
	$table_content .= "</tr>";
	
	$allMaleTotal[$houseID] = $maleHouseTotal;
}
$table1 .= $table_content;
### Table 1 (Boy) - End

### Table 2 (Girl) 
$table2 = "";
$table2 .= "<tr>";
$table2 .= "<td class='eSporttdborder eSportprinttabletitle'>&nbsp;</td>";
$table2 .=$table2GroupsHeader;
$table2 .= "<td class='eSporttdborder eSportprinttabletitle' align='center'>".$i_Sports_field_Total_Score."</td>";
$table2 .= "</tr>";
//$FGroup[] = '-2';
$table_content = "";
$gender = "F";
for($i=0; $i<sizeof($HouseInfo); $i++)
{
	$houseID = $HouseInfo[$i][0];
	$houseName = ($intranet_session_language=="en") ? $HouseInfo[$i][2] : $HouseInfo[$i][3];
	$colorCode = $HouseInfo[$i][4];
	$femaleHouseTotal = 0;
	
        $table_content .= "<tr>";
        $table_content .= "<td class='eSporttdborder eSportprinttabletitle' align='center'>".$houseName."</td>";

	for($k=0; $k<sizeof($FGroup); $k++)
	{
		$ageGroupID = $FGroup[$k];

		$score = round($HouseScoreSumArr[$houseID][$ageGroupID],1);

		$table_content .= "<td class='eSporttdborder eSportprinttext' align='center'>".$score."</td>";
		$femaleHouseTotal = $femaleHouseTotal + $score;
	}
	$table_content .= "<td class='eSporttdborder eSportprinttext' align='center'>".$femaleHouseTotal."</td>";
	$table_content .= "</tr>";
	
	$allFemaleTotal[$houseID] = $femaleHouseTotal;
}
$table2 .= $table_content;
### Table 2 (Girl) - End

### Table 3 (Mixed) 
if($sys_custom['eSports_count_open_event_in_grade_score']!==true )
{
	$table3 = "";
	$table3 .= "<tr>";
	$table3 .= "<td class='eSporttdborder eSportprinttabletitle'>&nbsp;</td>";
	
	$table3 .= "<td class='eSporttdborder eSportprinttabletitle' align='center'>".$i_Sports_field_Total_Score."</td>";
	$table3 .= "</tr>";
	
	$MGroup[] = '-4';
	$table_content = "";
	for($i=0; $i<sizeof($HouseInfo); $i++)
	{
		$houseID = $HouseInfo[$i][0];
		$houseName = ($intranet_session_language=="en") ? $HouseInfo[$i][2] : $HouseInfo[$i][3];
		$colorCode = $HouseInfo[$i][4];
		
	  $table_content .= "<tr>";
	  $table_content .= "<td class='eSporttdborder eSportprinttabletitle' align='center'>".$houseName."</td>";
	
		$mixedTotal = $lswimminggala->retrieveHouseScoreSum($houseID, -4);
	
		$table_content .= "<td class='eSporttdborder eSportprinttabletitle' align='center'>".$mixedTotal."</td>";
		$table_content .= "</tr>";
		
		$allMixedTotal[$houseID] = $mixedTotal;
	}
	$table3 .= $table_content;
}
### Table 3 (Mixed) - End

### Table 4 (Total)
$table4 = ""; 
for($i=0; $i<sizeof($HouseInfo); $i++)
{
	$houseName = ($intranet_session_language=="en") ? $HouseInfo[$i][2] : $HouseInfo[$i][3];
	$table4 .= "<td class='eSporttdborder eSportprinttabletitle' align='center'>".$houseName."</td>";
}
$table4 .= "</tr>";
$table4 .= "<tr>";
for($i=0; $i<sizeof($HouseInfo); $i++)
{
	$houseID = $HouseInfo[$i][0];
	$colorCode = $HouseInfo[$i][4];
	$total = $allMaleTotal[$houseID] + $allFemaleTotal[$houseID] + $allMixedTotal[$houseID];

	$table4 .= "<td class='eSporttdborder eSportprinttext' align='center'>".$total."</td>";
}
$table4 .= "</tr>";
### Table 4 (Total) - End

//Print and Close button able
$displayTable = "
	<table width='100%' align='center' class='print_hide' border=0>
	<tr>
		<td align='right'>".$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")."</td>
	</tr>
</table>";

?>
<br />
<?=$displayTable?>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
	<td align="center">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
			<td class='eSportprinttitle'><strong><?=$i_Sports_Event_Boys?><strong></td>
		</tr>
		<tr>
			<td align="left" class="tabletext">
                <table width="100%" border="0" cellspacing="0" cellpadding="4" class="eSporttableborder">
				<?=$table1?>
				</table>
			</td>
		</tr>
                
                </table>
        </td>
</tr>

<tr>
	<td align="center">
	<br>
        <table width=100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
			<td class='eSportprinttitle'><strong><?=$i_Sports_Event_Girls?><strong></td>
		</tr>
		<tr>
			<td align="left" class="tabletext">
                <table width="100%" border="0" cellspacing="0" cellpadding="4" class="eSporttableborder">
				<?=$table2?>
				</table>
			</td>
		</tr>
                </table>
        </td>
</tr>
<?
if($sys_custom['eSports_count_open_event_in_grade_score']!==true )
{
?>
<tr>
	<td align="center">
	<br>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
			<td class='eSportprinttitle'><strong><?=$i_Sports_Event_Mixed_Open?><strong></td>
		</tr>
		<tr>
			<td align="left" class="tabletext">
                <table width="100%" border="0" cellspacing="0" cellpadding="4" class="eSporttableborder">
				<?=$table3?>
				</table>
			</td>
		</tr>
                </table>
        </td>
</tr>
<?}?>


<tr>
	<td align="center">
	<br>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
			<td class='eSportprinttitle'><strong><?=$i_Sports_field_Total_Score?><strong></td>
		</tr>
		<tr>
			<td align="left" class="tabletext">
                <table width="100%" border="0" cellspacing="0" cellpadding="4" class="eSporttableborder">
				<?=$table4?>
				</table>
			</td>
		</tr>
                </table>
        </td>
</tr>
</table>

<?
intranet_closedb(); 
?>
