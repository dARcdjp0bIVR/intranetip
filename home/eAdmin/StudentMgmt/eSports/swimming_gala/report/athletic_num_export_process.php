<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libswimminggala.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");

$linterface = new interface_html();

intranet_auth();
intranet_opendb();

$lswimminggala = new libswimminggala();
$lswimminggala->authSportsSystem();

$ageGroup = $_POST['ageGroup'];
$groupType = $_POST['groupType'];
$className = $_POST['className'];
$house = $_POST['house'];
$raceDay = $_POST['raceDay'];

$Students = $lswimminggala->getAthleticNumDeatil($groupType, $className, $house, $ageGroup, $raceDay);

?>


<STYLE TYPE="text/css">
<!--
p.text1 {font-size: 60px;font-weight: bolder;}
-->
</STYLE>

<?php
$content = "";
$count = 1;

$content .= "<br />";
$content .= "<br />";
$content .= "<br />";

if(sizeof($Students) != 0)
{
        $content .= "
	<table width='100%' align='center' class='print_hide' border='0'>
	<tr>
		<td align='right'>". $linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2") ."</td>
	</tr>
	</table>";

	$content .= "<br />";
	$content .= "<br />";
	$content .= "<br />";

	foreach($Students as $key => $value)
	{
		list($sname, $gname, $hname, $ahtleticNum, $detail) = $value;
		
		$breakStyle = ($count%2 == 0)?"style='page-break-after:always'":"";
		
		$content .= "<table width='70%' border='0' cellpadding='0' cellspacing='0' align='center' $breakStyle>";
		$content .= "<tr>";
		$content .= "<td align='right' width='40%'><font size='4'>".$gname."</font></td>";
		$content .= "<td align='center' width='25%'><font size='4'>".$hname."</font></td>";
		$content .= "<td><font size='3'>".$sname."</font></td>";
		$content .= "</tr>";
		$content .= "<tr><td colspan='3'>&nbsp;</td></tr>";
		$content .= "<tr><td colspan='3'>&nbsp;</td></tr>";
		$content .= "<tr><td colspan='3' align='center'><p class='tabletext text1'>".$ahtleticNum."</p></td></tr>";
		$content .= "<tr><td colspan='3'>&nbsp;</td></tr>";
		$content .= "<tr><td colspan='3'>&nbsp;</td></tr>";
		$content .= "<tr><td colspan='3'><font size='4'>&nbsp;".$detail."</font></td></tr>";
		$content .= "</table>";
		$content .= "<br />";
		$content .= "<br />";
		
		$content .= "<br />";
		$content .= "<br />";
		$content .= "<br />";
		$content .= "<br />";
		$content .= "<br />";
		$content .= "<br />";

		$count++;
	}
}
else
{
	$content .= "<table width='70%' border='0' cellpadding='0' cellspacing='0' align='center' $breakStyle>";
	$content .= "<tr><td align='center' class='tabletext'>".$i_no_record_exists_msg."</td></tr>";
	$content .= "<tr><td>&nbsp;</td></tr>";
        $content .= "<tr><td align='center'>". $linterface->GET_BTN($button_close, "button", "javascript:self.close();","clost_btn") ."</td></tr>";
	$content .= "</table>";
}
echo $content;

include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_footer.php");
intranet_closedb();
?>