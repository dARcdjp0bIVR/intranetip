<?php
//modified by marcus 23/6
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libswimminggala.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$linterface 	= new interface_html();
$CurrentPage	= "PageReport_Event";


$lswimminggala = new libswimminggala();
$lswimminggala->authSportsSystem();

$AgeGroups = $lswimminggala->retrieveAgeGroupIDNames();	# retrieve all age group ids and names
# Add Boys Open and Girls Open 
$openBoy[0] = '-1';
$openBoy[1] = $i_Sports_Event_Boys_Open;
$openGirl[0] = '-2';
$openGirl[1] = $i_Sports_Event_Girls_Open;
$openMixed[0] = '-4';
$openMixed[1] = $i_Sports_Event_Mixed_Open;
$AgeGroups[] = $openBoy;
$AgeGroups[] = $openGirl;
$AgeGroups[] = $openMixed;

$TREvents = $lswimminggala->retrieveTrackFieldEventName();		# retrieve all track and field eventgroup ids and names
$house_relay_name = $lswimminggala->retrieveEventTypeNameByID(3);
$class_relay_name = $lswimminggala->retrieveEventTypeNameByID(4);

$numberOfLane = $lswimminggala->numberOfLanes;

# Create a new array to store the track and field results
for($i=0; $i<sizeof($TREvents); $i++)
{
	list($event_id, $event_name) = $TREvents[$i];

	for($j=0; $j<sizeof($AgeGroups); $j++)
	{
		list($group_id, $group_name) = $AgeGroups[$j];
		
		$typeIDs = array(1);
		# Retrieve event group id
		$eventGroupInfo= $lswimminggala->retrieveEventGroupID($event_id, $group_id, $typeIDs);	

		$eventGroupID = $eventGroupInfo[0];
		$eventType = $eventGroupInfo[1];
		
		if($eventGroupID != "")
		{
			
			#  Retrieve event enroled count (exclude those not in intranet user)
			$totalCount = $lswimminggala->retrieveEventEnroledCountInIntranet($eventGroupID);
			# Retrieve Count of Participants
			$participationCount = $lswimminggala->returnNumberOfParticipationByEventGroupID($eventGroupID);
			
			$TrackFieldResult[$event_id][$group_id]["eg_id"] = $eventGroupID;
			$TrackFieldResult[$event_id][$group_id]["event_type"] = $eventType;
			$TrackFieldResult[$event_id][$group_id]["total"] = $totalCount;
			$TrackFieldResult[$event_id][$group_id]["participation"] = $participationCount;
		}
	}
}

#Export Content
$export_content .= $i_Sports_menu_Report_Event."\n\n";

### Age Group Title
$ageGroupTitle = "<tr><td>&nbsp;</td>";
$export_content .=",";
for($i=0; $i<sizeof($AgeGroups); $i++) 
{
	$ageGroupTitle .= '<td align="center" class="tabletop tabletopnolink">'.$AgeGroups[$i][1].'</td>';
	$export_content .= $AgeGroups[$i][1].",";
}
$ageGroupTitle .= "</tr>";
$export_content .= "\n";

### Event content
$event_content = "";
$de = "";
$eg_ids = "";
for($j=0; $j<sizeof($TREvents); $j++)
{
	list($event_id, $event_name) = $TREvents[$j];
	
	$event_content .= "<tr class='tablerow". ($j%2?"2":"1")."'>";
	$event_content .= "<td class='tablelist'>".$event_name."</td>";
	$export_content .= $event_name.",";
	
	for($k=0; $k<sizeof($AgeGroups); $k++)
	{
		$group_id = $AgeGroups[$k][0];

		if(sizeof($TrackFieldResult[$event_id][$group_id]) != 0)
		{
			$eventGroupID = $TrackFieldResult[$event_id][$group_id]["eg_id"];
			$event_type = $TrackFieldResult[$event_id][$group_id]["event_type"];
			$total = $TrackFieldResult[$event_id][$group_id]["total"];
			$participation = $TrackFieldResult[$event_id][$group_id]["participation"];
			
			#Check whether the Event has been set up
			$is_setup = $lswimminggala->checkExtInfoExist($event_type, $eventGroupID);
			
			if($is_setup == 1)
			{				
				if($total != 0)
				{
					$tr_display = $participation."/".$total;
					$export_content .= "\x27".$participation."/".$total.",";

					$eg_ids .=  $de.$eventGroupID;
					$eg_types .= $de.$event_type;
					$de = ",";
				}
				else
				{
					$tr_display = "0/0";
					$export_content .= "0/0,";
				}
				$event_content .= "<td class='tabletext' align='center'>".$tr_display."</td>";
			}
			else
			{
				$event_content .= "<td class='tabletext' align='center'>- -</td>";
				$export_content .= "- -,";
			}
		}
		else
		{
			$event_content .= "<td class='tabletext'>&nbsp;</b>";
			$export_content .= ",";
		}
	}
	$event_content .= "</tr>";
	$export_content .=  "\n";
}
$export_content .=  "$i_Sports_Explain : $i_Sports_Participation_Count / $i_Sports_Enroled_Student_Count ; - - $i_Sports_Item_Without_Setting_Meaning";
$no_record = 0;
if(trim($event_content)=="")
{
	$event_content .= "<tr>";
	$event_content .= "<td class='tablerow2 tabletext' align='center' colspan='". (sizeof($AgeGroups)+1) ."' ><br />".$i_no_record_searched_msg."<br /><br></td>";
	$event_content .= "</tr>";
	
	$no_record	= 1;
}

$ExportBtn 	= $linterface->GET_ACTION_BTN($button_export, "button","click_export('general_export.php');"); 


### Title ###
$TitleTitle1 = "<span class='contenttitle'>". $i_Sports_menu_Report_Event ."</span>";
$TitleTitle = "<table width='100%' height='25' cellpadding='0' cellspacing='0' ><tr><td style=\"vertical-align: bottom;\" >".$TitleTitle1."</td></tr></table>";
$TAGS_OBJ[] = array($TitleTitle, "", 0);    

$MODULE_OBJ = $lswimminggala->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();
?>

<br />
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td>&nbsp;</td>
        <td align="right"><?=$linterface->GET_SYS_MSG($xmsg);?></td>
</tr>   
<tr>
	<td align="center" colspan="2">
        	<table width="96%" border="0" cellspacing="0" cellpadding="0">
                             
		<tr>
                	<td>
                                <table width="100%" border="0" cellspacing="0" cellpadding="4">
                                <?=$ageGroupTitle?>
                                <?=$event_content?>
                        	</table>
                        </td>
		</tr>
                <tr>
			<td height="1" class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
		</tr>
		<? if(!$no_record) {?>
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="2">
				<tr>
                                	<td><span class="tabletextremark">(<?=$i_Sports_Explain?>: <?=$i_Sports_Participation_Count?> / <?=$i_Sports_Enroled_Student_Count?>; - - <?=$i_Sports_Item_Without_Setting_Meaning?>)</span></td>
                                </tr>
                                </table>
			</td>
		</tr>
		<? } ?>
		</table>
	</td>
</tr>
</table>      
<form name="form2" action='general_export.php' method="post">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td align='center'><?=$ExportBtn?></td>
</tr>

<input type="hidden" name="exportFileName" value="event">
<input type="hidden" name="exportContent" value="<?=$export_content?>">
</form>
</table>
       
<br />
<SCRIPT>
function click_export(action)
{
	var obj = document.form2;
	obj.action=action;
	obj.submit();
	
}
</SCRIPT>

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>
