<?php
# using: yat

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libswimminggala.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
intranet_auth();
intranet_opendb();

$lswimminggala = new libswimminggala();
$lexport = new libexporttext();

$AgeGroups = $lswimminggala->retrieveAgeGroupInfo();	# retrieve all age group ids and names
$HouseInfo = $lswimminggala->retrieveHouseInfo();		# retrieve all houses info

$table1GroupsHeader = array();
$table2GroupsHeader = array();
for($i=0; $i<sizeof($AgeGroups); $i++)
{
	$groupID = $AgeGroups[$i][0];
	$groupName = $intranet_session_language=="en"?$AgeGroups[$i][2]:$AgeGroups[$i][3];
	$groupGender = $AgeGroups[$i][4];
	
	if($groupGender=="M")
	{
		$MGroup[] = $groupID;
		$table1GroupsHeader[] = $groupName;
	}
	else if($groupGender=="F")
	{
		$FGroup[] = $groupID;
		$table2GroupsHeader[] = $groupName;
	}
}

if($sys_custom['eSports_count_open_event_in_grade_score']!==true )
{
	# add open group
	$MGroup[] = '-1';
	$FGroup[] = '-2';
	
	$table1GroupsHeader[] = $i_Sports_Event_Boys_Open;
	$table2GroupsHeader[] = $i_Sports_Event_Girls_Open;
}
else
{
	$includeOpenEventScore = 1;
}

$HouseScoreSumArr = $lswimminggala->retrieveHouseScoreSumArr("","",$includeOpenEventScore);

#################################
### Table 1 (Boy)
#################################
$table1 = array();
$table1[] = array_merge(array(''),$table1GroupsHeader, array($i_Sports_field_Total_Score));

for($i=0; $i<sizeof($HouseInfo); $i++)
{
	$row = array();
	$houseID = $HouseInfo[$i][0];
	$houseName = ($intranet_session_language=="en") ? $HouseInfo[$i][2] : $HouseInfo[$i][3];
	$colorCode = $HouseInfo[$i][4];
	$maleHouseTotal = 0;
	$row[] = $houseName;
	for($k=0; $k<sizeof($MGroup); $k++)
	{
		$ageGroupID = $MGroup[$k];
		$score = round($HouseScoreSumArr[$houseID][$ageGroupID],1);
		$row[] = $score;
		$maleHouseTotal = $maleHouseTotal + $score;
	}
	$row[] = $maleHouseTotal;
	$allMaleTotal[$houseID] = $maleHouseTotal;
	$table1[] = $row;
}
### Table 1 (Boy) - End

### Table 2 (Girl) 
$table2 = array();
$table2[] = array_merge(array(''),$table2GroupsHeader, array($i_Sports_field_Total_Score));
for($i=0; $i<sizeof($HouseInfo); $i++)
{
	$row = array();
	$houseID = $HouseInfo[$i][0];
	$houseName = ($intranet_session_language=="en") ? $HouseInfo[$i][2] : $HouseInfo[$i][3];
	$colorCode = $HouseInfo[$i][4];
	$femaleHouseTotal = 0;
	$row[] = $houseName;
	for($k=0; $k<sizeof($FGroup); $k++)
	{
		$ageGroupID = $FGroup[$k];
		$score = round($HouseScoreSumArr[$houseID][$ageGroupID],1);
		$row[] = $score;
		$femaleHouseTotal = $femaleHouseTotal + $score;
	}
	$row[] = $femaleHouseTotal;
	
	$allFemaleTotal[$houseID] = $femaleHouseTotal;
	$table2[] = $row;
}
### Table 2 (Girl) - End

### Table 3 (Mixed) 
if($sys_custom['eSports_count_open_event_in_grade_score']!==true )		
{
	$table3 = array();
	$table3[] = array('',$i_Sports_field_Total_Score);
	$MGroup[] = '-4';
	for($i=0; $i<sizeof($HouseInfo); $i++)
	{
		$row = array();
		$houseID = $HouseInfo[$i][0];
		$houseName = ($intranet_session_language=="en") ? $HouseInfo[$i][2] : $HouseInfo[$i][3];
		$colorCode = $HouseInfo[$i][4];
		$row[] = $houseName;
		$mixedTotal = round($lswimminggala->retrieveHouseScoreSum($houseID, -4),1);
		$row[] = $mixedTotal;
		$allMixedTotal[$houseID] = $mixedTotal;
		$table3[] = $row;
	}
	### Table 3 (Mixed) - End
}

### Table 4 (Total)
$table4 = array();
$row = array();
for($i=0; $i<sizeof($HouseInfo); $i++)
{
	$houseName = ($intranet_session_language=="en") ? $HouseInfo[$i][2] : $HouseInfo[$i][3];
	$row[] = $houseName;
}
$table4[] = $row;
$row = array();
for($i=0; $i<sizeof($HouseInfo); $i++)
{
	$houseID = $HouseInfo[$i][0];
	$colorCode = $HouseInfo[$i][4];
	$total = $allMaleTotal[$houseID] + $allFemaleTotal[$houseID] + $allMixedTotal[$houseID];
	$row[] = $total;
}
$table4[] = $row;
### Table 4 (Total) - End


$export_ary = array();
$export_ary[] = array($i_Sports_Event_Boys);
foreach($table1 as $k=>$export_ary[]);

$export_ary[] = array('');
$export_ary[] = array($i_Sports_Event_Girls);
foreach($table2 as $k=>$export_ary[]);

if($sys_custom['eSports_count_open_event_in_grade_score']!==true )
{
	$export_ary[] = array('');
	$export_ary[] = array($i_Sports_Event_Mixed_Open);
	foreach($table3 as $k=>$export_ary[]);
}

$export_ary[] = array('');
$export_ary[] = array($i_Sports_field_Total_Score);
foreach($table4 as $k=>$export_ary[]);



$utf_content = "";
foreach($export_ary as $k=>$d)
{
	$utf_content .= implode("\t", $d);
	$utf_content .= "\r\n";
}

intranet_closedb();

$filename = "house_group_score.csv";
$lexport->EXPORT_FILE($filename, $utf_content); 
?>