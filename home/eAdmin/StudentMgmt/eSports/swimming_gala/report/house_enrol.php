<?php
# using: 

############ Change Log [Start] #####
#
#	Date:	2016-06-15	(Cara)	[2016-0503-1555-00066]
#			added Export 2 button
#			updated css style for house info table
#
#	Date:	2012-10-18 YatWoon
#			fixed: missing to count absent/present for open group event [Case#2012-1016-1430-51156]
#
#	Date:	2010-05-31 YatWoon
#			retrieve current academic year's house info only
#	
#	Date:	2010-05-28 YatWoon
#			add Present / Absent data for House
#
############ Change Log [End] #####

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libswimminggala.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$linterface 	= new interface_html();
$CurrentPage	= "PageParticipantRecord";

$lswimminggala = new libswimminggala();
$lclass = new libclass();
$lswimminggala->authSportsSystem();

# retrieve a initial house ID
// $sql = "SELECT HouseID FROM INTRANET_HOUSE ORDER BY HouseID LIMIT 0, 1";
// $default_house = $lswimminggala->returnVector($sql);
$houses = $lswimminggala->retrieveHouseSelectionInfo();
$default_house = $houses[0]['HouseID'];

$totalPresent = 0;
$totalAbsent = 0;
$typeIDs = array(1,2);

if($_POST['houseID'] == "")
	$houseID = $default_house;
else
	$houseID = $_POST['houseID'];

# Create House Selection List
$select_house = getSelectByArray($houses,"name=houseID onChange='document.form2.submit()'","$houseID",0,1);

//$Students = $lswimminggala->returnStudentListByHouse($houseID);
$Students = $lswimminggala->returnStudentListByHouse2($houseID);

$EventGroups = $lswimminggala->retrieveTrackFieldEventName();
$export_content = "";

### Table content
$content = "";

# retrieve the house name
$houseInfo = $lswimminggala->getHouseDetail($houseID);
$engName = $houseInfo[1];
$chiName = $houseInfo[2];
$houseName = ($intranet_session_language=="en") ? $engName : $chiName;

# This part is to count the attendant rate
for($j=0; $j<sizeof($Students); $j++)
{				
	list($student_id, $student_name, $studentclass, $studentclassno) = $Students[$j];		
	$student_group_id = $lswimminggala->retrieveAgeGroupByStudentID($student_id);
	$enrolRecord = $lswimminggala->retrievePersonalEnrolmentRecord($student_id);
	
	for($k=0; $k<sizeof($EventGroups); $k++)
	{
		$eid = $EventGroups[$k][0];
		if($enrolRecord[$eid] == 1)
		{
			# count absent/present
			$egid = $lswimminggala->retrieveEventGroupID($eid, $student_group_id, $typeIDs);
			$status = $lswimminggala->returnFirstRoundStudentAttendantStatus($student_id, $egid[0]);
			for($s=0; $s<sizeof($status); $s++)
			{
				if($status[$s]==1)
					$totalAbsent++;
				else if($status[$s]==2 || $status[$s]==3 ||$status[$s]==4 ||$status[$s]==5)
					$totalPresent++;
			}
			
			# count absent/present for open group event
			$open_ary = array('-1', '-2', '-4');
			for($o=0;$o<sizeof($open_ary);$o++)
			{
				$egid = $lswimminggala->retrieveEventGroupID($eid, $open_ary[$o], $typeIDs);
				$status = $lswimminggala->returnFirstRoundStudentAttendantStatus($student_id, $egid[0]);
				for($s=0; $s<sizeof($status); $s++)
				{
					if($status[$s]==1)
						$totalAbsent++;
					else if($status[$s]==2 || $status[$s]==3 ||$status[$s]==4 ||$status[$s]==5)
						$totalPresent++;
				}
			}
			
		}
	}
}

$export_content .= $i_Sports_House.":\t";
$export_content .= $houseName."\n";
$export_content .= $i_Sports_Present."\t";
$export_content .= $totalPresent.$i_Sports_Count."\n";
$export_content .= $i_Sports_Absent."\t";
$export_content .= $totalAbsent.$i_Sports_Count."\n\n";

$content .= "<table width='100%' border='0' cellspacing='0' cellpadding='4'>";
$content .= "<tr>";
$content .= "<td class='tablebluetop tabletopnolink'>$i_ClassName</td>";
$content .= "<td class='tablebluetop tabletopnolink'>$i_ClassNumber</td>";
$content .= "<td class='tablebluetop tabletopnolink'>$i_Sports_field_Group</td>";
$content .= "<td class='tablebluetop tabletopnolink'>$i_Sports_field_Gender</td>";
$content .= "<td class='tablebluetop tabletopnolink'>$i_Sports_Participant</td>";
$content .= "<td class='tablebluetop tabletopnolink'>$i_Sports_menu_Settings_Participant</td>";


		$export_content .= $i_ClassName."\t";
		$export_content .= $i_ClassNumber."\t";
		$export_content .= $i_Sports_field_Group."\t";
		$export_content .= $i_Sports_field_Gender."\t";		
		$export_content .= $i_Sports_Participant."\t";
		$export_content .= $i_Sports_menu_Settings_Participant."\t";

        $eName = array();
		for($i=0; $i<sizeof($EventGroups); $i++)
		{
			$eventName = $EventGroups[$i][1];
			$content .= "<td class='tablebluetop tabletopnolink' align='center'>".$eventName."</td>";
			$eName[$i] = $eventName;
			if(($i+1)<sizeof($EventGroups))
				$export_content .= $eventName."\t";
			else
				$export_content .= $eventName."\n";
		}
		$content .= "</tr>";
		
		for($j=0; $j<sizeof($Students); $j++)
		{				
			list($student_id, $student_name, $studentclass, $studentclassno) = $Students[$j];		
			$student_group_id = $lswimminggala->retrieveAgeGroupByStudentID($student_id);
			$student_group = $lswimminggala->retrieveAgeGroupName($student_group_id);
			$student_gender = $lswimminggala->retrieveAgeGroupDetail($student_group_id);
			$enrolRecord = $lswimminggala->retrievePersonalEnrolmentRecord($student_id);
			$athleticNum = $lswimminggala->returnStudentAthleticNum($student_id);

			$content .= "<tr class='tablebluerow".($j%2?"2":"1")."'>";
			$content .= "<td class='tabletext'>".$studentclass."</td>";
			$content .= "<td class='tabletext'>".$studentclassno."</td>";
			$content .= "<td class='tabletext'>". $student_group ."</td>";
			$content .= "<td class='tabletext'>". $student_gender["Gender"] ."</td>";
			$content .= "<td class='tabletext'>".$student_name."</td>";
			$content .= "<td class='tabletext'>".$athleticNum."</td>";
			$export_content .= $studentclass."\t";
			$export_content .= $studentclassno."\t";
			$export_content .= $student_group."\t";
			$export_content .= $student_gender["Gender"]."\t";
			$export_content .= "\"".$student_name."\"\t";
			$export_content .= $athleticNum."\t";

			for($k=0; $k<sizeof($EventGroups); $k++)
			{
				$eid = $EventGroups[$k][0];
					
				if($enrolRecord[$eid] == 1)
				{
                    $content .= "<td class='tabletext' align='center'><span title='".$eName[$k]."'>X</span></td>";
					if(($k+1)<sizeof($EventGroups))
					{
						$export_content .= "X";
						$export_content .= "\t";
					}
					else
					{
						$export_content .= "X";
						$export_content .= "\n";
					}
				}
				else
				{
					$content .= "<td>&nbsp;</td>";
					if(($k+1)<sizeof($EventGroups))
						$export_content .= "\t";
					else
						$export_content .= "\n";
				}
			}
			$content .= "</tr>";
		}
		$content .= "</table>";
   
### Table content - End

### Title ###
$TAGS_OBJ[] = array($i_Sports_menu_Report_ClassEnrolment,"class_enrol.php", 0);
$TAGS_OBJ[] = array($i_Sports_menu_Report_HouseEnrolment,"house_enrol.php", 1);
$MODULE_OBJ = $lswimminggala->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();
?>

<br />
<table width="96%" border="0" cellpadding="5" cellspacing="0">
<tr>
	<td><br />
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
			<td>
                        	<form name="form2" action="house_enrol.php" method="post">
                                <table  class="form_table_v30">
                                <tr>
                                <td class="field_title"><span class="tabletext"><?=$i_Sports_House?> </span></td>
                                <td><?=$select_house?></td>
                                </tr>
                                
                                <tr>
                                <td class="field_title"><span class="tabletext"><?=$i_Sports_Present?> </span></td>
                                <td><?=$totalPresent." ".$i_Sports_Count?></td>
                                </tr>
                                <tr>
                                <td class="field_title"><span class="tabletext"><?=$i_Sports_Absent?> </span></td>
                                <td><?=$totalAbsent." ".$i_Sports_Count?></td>
                                </tr>
                                
                                </table>
                                </form>
			</td>
		</tr>
                <tr>
                	<td height="1" class="dotline"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
                </tr>
                </table>
	</td>
</tr>
<tr>
	<td><br /><?=$content?></td>
</tr>        

<form name="form1" action='general_export.php' method="post">
<tr>
	<td height='1' class='dotline'><img src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif' width='10' height='1'></td>
</tr>     
<tr>
	<td align='center'>
	<?=$linterface->GET_ACTION_BTN($button_export, "submit", "","submit2")?>
	<?if(file_exists("house_enrol_export2.php")) echo $linterface->GET_ACTION_BTN($Lang['Btn']['Export2'], "button","document.form2.action='house_enrol_export2.php';document.form2.submit();")?>
	</td>
</tr>

<? $export_content = intranet_htmlspecialchars($export_content); ?>
<input type="hidden" name="exportFileName" value="house-enrol">
<input type="hidden" name="exportContent" value='<?=$export_content?>'>
</form>

</table>
<br />

<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>