<?php
# using: yat
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libswimminggala.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$linterface 	= new interface_html();
$CurrentPage	= "PageReport_Class";

$lclass = new libclass();
$lswimminggala = new libswimminggala();
$lswimminggala->authSportsSystem();

$studentCount = $lswimminggala->retrieveClassStudentCount();
$enrolCount = $lswimminggala->retrieveClassNumberOfEnrolmentAndScore();
$ClassRelayResult = $lswimminggala->retrieveClassRelayScore();	# count on Class Relay
$studentenrolCount = $lswimminggala->retrieveClassNumberOfStudentEnrolment();

$sortname 	= $sortname=="" ? "class_name" : $sortname;
$sortorder	= $sortorder=="" ? 0 : $sortorder;
$arrow_icon = "icon_sort_". ($sortorder==1?"d":"a")."";
$mouseover_tag = "onMouseOver=\"MM_swapImage('sort_icon','','{$image_path}/{$LAYOUT_SKIN}/{$arrow_icon}_on.gif',1)\" onMouseOut='MM_swapImgRestore()'";
$arrow_display = "<img name='sort_icon' id='sort_icon' src='{$image_path}/{$LAYOUT_SKIN}/{$arrow_icon}_off.gif' align='absmiddle'  border='0' />";

### Content
$content ="";
$content .= "<table width='100%' border='0' cellpadding='0' cellspacing='0'><tr><td>";
$content .= "<table width='100%' border='0' cellpadding='4' cellspacing='0'>";
$content .= "<tr>";
$content .= "<td class='tablebluetop tabletopnolink'><a ". ($sortname=="class_name"?$mouseover_tag:"")	." href=\"javascript:sort_order('class_name', 	". ($sortname=="class_name"? 	($sortorder==1?0:1):"0") .");\" class='tabletoplink'>$i_Class ". 				($sortname=="class_name"? 	$arrow_display:"")."</a></td>";
$content .= "<td class='tablebluetop tabletopnolink'><a ". ($sortname=="s_count"?$mouseover_tag:"")	." href=\"javascript:sort_order('s_count', 	". ($sortname=="s_count"? 	($sortorder==1?0:1):"0") .");\" class='tabletoplink'>$i_Sports_Report_Total_Student_Count ". 	($sortname=="s_count"? 		$arrow_display:"")."</a></td>";
$content .= "<td class='tablebluetop tabletopnolink'><a ". ($sortname=="e_count"?$mouseover_tag:"")	." href=\"javascript:sort_order('e_count', 	". ($sortname=="e_count"? 	($sortorder==1?0:1):"0") .");\" class='tabletoplink'>$i_Sports_Report_Enrolment_Count ". 	($sortname=="e_count"? 		$arrow_display:"")."</a></td>";
$content .= "<td class='tablebluetop tabletopnolink'><a ". ($sortname=="se_count"?$mouseover_tag:"") 	." href=\"javascript:sort_order('se_count', 	". ($sortname=="se_count"? 	($sortorder==1?0:1):"0") .");\" class='tabletoplink'>". $Lang['eSports']['NoOfStudentsEnrolled'] .	($sortname=="se_count"? 		$arrow_display:"")."</a></td>";
$content .= "<td class='tablebluetop tabletopnolink'><a ". ($sortname=="score"?$mouseover_tag:"")	." href=\"javascript:sort_order('score', 	". ($sortname=="score"? 	($sortorder==1?0:1):"0") .");\" class='tabletoplink'>$i_Sports_field_Score ". 			($sortname=="score"? 		$arrow_display:"")."</a></td>";
$content .= "<td class='tablebluetop tabletopnolink'><a ". ($sortname=="averageCount"?$mouseover_tag:"")." href=\"javascript:sort_order('averageCount', ". ($sortname=="averageCount"? 	($sortorder==1?0:1):"0") .");\" class='tabletoplink'>$i_Sports_Report_Average_Enrolment_Count ".($sortname=="averageCount"? 	$arrow_display:"")."</a></td>";
$content .= "</tr>";

## push the data into multi-array
$data = array();

# build ClassID -> EnrolCount Mapping ($enrolCountAry[$YearClassID] = $EnrolCountInfo)
foreach($enrolCount as $thisEnrolCount)
	$enrolCountAry[$thisEnrolCount[2]] = $thisEnrolCount;

$studentenrolCountAry = array(); 
foreach($studentenrolCount as $formEnrolCount)
{
	$studentenrolCountAry[$formEnrolCount[1]] = $formEnrolCount;
}	
	
for($i=0; $i<sizeof($studentCount); $i++)
{
	list($s_count, $s_classid, $class_name_en,$class_name_b5) = $studentCount[$i];
	list($e_count, $score, $e_classid) 	= $enrolCountAry[$s_classid];
	list($se_count, $se_level) = $studentenrolCountAry[$s_classid];
	$class_name=Get_Lang_Selection($class_name_b5,$class_name_en);

	//if($s_classid == $e_classid )
	//{
		# Calculate the average number of enrolment of this classlevel
		$averageCount = $s_count!=0?$e_count/$s_count:0;
		$averageCount = round($averageCount, 2);

                $data[$i]['class_name']		= $class_name?$class_name:0;
                $data[$i]['s_count'] 		= $s_count?$s_count:0;
                $data[$i]['e_count'] 		= $e_count?$e_count:0;
                $data[$i]['se_count'] 		= $se_count?$se_count:0;
                $data[$i]['score'] 		= $score?round($score,1):0;
                if($ClassRelayResult[$e_classid] >0)
					$data[$i]['score'] += $ClassRelayResult[$e_classid];
                $data[$i]['averageCount'] 	= $averageCount?$averageCount:0;
	//}
}

foreach ($data as $key => $row) {
    $class_name_ary[$key]	= $row['class_name'];
    $s_count_ary[$key] 		= $row['s_count'];
    $e_count_ary[$key] 		= $row['e_count'];
    $se_count_ary[$key] 		= $row['se_count'];
    $score_ary[$key] 		= $row['score'];
    $averageCount_ary[$key] 	= $row['averageCount'];
}

array_multisort( ${$sortname."_ary"}, $sortorder==1?SORT_DESC:SORT_ASC, $data);

$display = "";
for($i=0; $i<sizeof($data); $i++)
{
                $display .= "<tr class='tablebluerow".($i%2?"2":"1")."'>";
                $display .= "<td width='15%' class='tabletext'>".$data[$i]['class_name']."</td>";
		$display .= "<td width='20%' class='tabletext'>".$data[$i]['s_count']."</td>";
		$display .= "<td width='15%' class='tabletext'>".$data[$i]['e_count']."</td>";
		$display .= "<td width='15%' class='tabletext'>".$data[$i]['se_count']."</td>";
		$display .= "<td width='15%' class='tabletext'>".$data[$i]['score']."</td>";
		$display .= "<td width='15%' class='tabletext'>".$data[$i]['averageCount']."</td>";
                $display .= "</tr>";
}

$content .= $display;
$content .= "</table></td></tr>";
$content .= "<tr><td height='1' class='dotline'><img src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif' width='10' height='1'></td></tr>";
$content .= "</table>";
### Content End

### Title ###
$TitleTitle1 = "<span class='contenttitle'>". $i_Sports_menu_Report_Class ."</span>";
$TitleTitle = "<table width='100%' height='25' cellpadding='0' cellspacing='0' ><tr><td style=\"vertical-align: bottom;\" >".$TitleTitle1."</td></tr></table>";
$TAGS_OBJ[] = array($TitleTitle, "", 0);    

$MODULE_OBJ = $lswimminggala->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();
?>

<script language="javascript">
<!--
function sort_order(xname, xorder)
{
	with(document.form1)
        {
        	sortname.value	= xname;
                sortorder.value = xorder;
                
                submit();
        }
}
//-->
</script>

<br />
<form name="form1" method="get" action="class.php">
<table width="96%" border="0" cellpadding="5" cellspacing="0">
<tr>
	<td><br />
        	<!-------------------------- start of class report table ----------------------------------------------->
        	<?=$content?>
	</td>
</tr>
</table>
<br />

<input type="hidden" name="sortname" value="">
<input type="hidden" name="sortorder" value="">
</form>
<form name="form2" action='class_export.php' method="post">
<table width="96%" border="0" cellpadding="5" cellspacing="0">
<tr>
	<td align='center'><?=$linterface->GET_ACTION_BTN($button_export, "submit", "","submit2")?></td>
</tr>

<input type="hidden" name="sortname" value="<?=$sortname?>">
<input type="hidden" name="sortorder" value="<?=$sortorder?>">
</form>
</table>
<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>