<?php
# using:

########### Change Log [Start] #######
#
#	Date:	2016-07-12 (Bill)
#			Check if $house_info is empty first, prevent php error in PHP 5.4
#
#	Date:   2016-06-16 (Cara)
#			add event group name at csv export file
#
#	Date:	2016-06-15	(Cara)	[2016-0503-1555-00066]
#			copy from /sports/report/class_enrol_export2.php
#
########### Change Log [End] #######

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libswimminggala.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");

intranet_auth();
intranet_opendb();

$lexport = new libexporttext();
$lsports = new libswimminggala();
$lsports->authSportsSystem();
$lclass = new libclass();
$fcm = new form_class_manage();

$classid = $_POST['classid'];

if($classid=="0")					# all classes
{	
	$year = Get_Current_Academic_Year_ID();
	//$sql2 = "SELECT YearClassID FROM YEAR_CLASS WHERE AcademicYearID=$year ORDER BY Sequence, ".Get_Lang_Selection('ClassTitleB5', 'ClassTitleEN');
	$sql2 = "
			SELECT 
				a.YearClassID 
			FROM 
				YEAR_CLASS as a
				left join YEAR as b on b.YearID=a.YearID
			WHERE 
				a.AcademicYearID=$year 
			ORDER BY 
				b.Sequence,a.Sequence, ".Get_Lang_Selection('ClassTitleB5', 'ClassTitleEN');
	$result = $lsports->returnArray($sql2);
	$export_classname = $i_general_all_classes;
}
elseif(substr($_POST['classid'],0,2)=="::")	# class
{
	$classid = str_replace("::","",$_POST['classid']);
	$result[0]['YearClassID'] = $classid;
	
	$className = $lclass->getClassName($classid);
	$export_classname = $className;
}
else
{
	$YearID = $_POST['classid'];
	$year = Get_Current_Academic_Year_ID();
	$sql2 = "SELECT YearClassID FROM YEAR_CLASS WHERE AcademicYearID=$year AND YearID=$YearID ORDER BY Sequence, ".Get_Lang_Selection('ClassTitleB5', 'ClassTitleEN');
	$result = $lsports->returnArray($sql2);
	
	$export_classname = $lclass->getLevelName($YearID);
}

$DataAry = array();
$i = 0;

foreach($result as $k=>$d)
{
	$classid = $d['YearClassID'];
	
	# Class Info
	$ClassName = $lclass->getClassName($classid);
	
	# Student List
	$Students = $lclass->getClassStudentNameList($classid);
	foreach($Students as $k=>$d)
	{
		list($thisUserID, $thisStudentName, $thisClassNumber) = $d;
		$this_student_info = $lsports->Get_Student_Basic_Info(array($thisUserID));
		
		$DataAry[$i][] = $ClassName;
		$DataAry[$i][] = $thisClassNumber;
		$DataAry[$i][] = $thisStudentName;
		$DataAry[$i][] = $this_student_info[0]['Gender'];
		
		$student_group_id = $lsports->retrieveAgeGroupByStudentID($thisUserID);
		$student_group = $lsports->retrieveAgeGroupName($student_group_id);
		$DataAry[$i][] = $student_group;
		
		# house info
 		$house_id = $this_student_info[0]['HouseID'];
 		$house_info = $house_id ? $lsports->retrieveHouseInfo($house_id) : "";
		// Check if $house_info is empty first
		$DataAry[$i][] = $house_info!=""? $house_info['HouseName'] : "";

		
		//$athleticNum = $lsports->returnStudentAthleticNum($thisUserID);
		$DataAry[$i][] = $this_student_info[0]['AthleticNum'];
		
		# enrolled event
		$sql = "SELECT EventGroupID FROM SWIMMINGGALA_STUDENT_ENROL_EVENT WHERE StudentID = '$thisUserID'";
		$enroledID = $lsports->returnArray($sql);
		
		$maxEvent = sizeof($enroledID) > $maxEvent ? sizeof($enroledID) : $maxEvent;
		foreach($enroledID as $k1=>$d1)
		{
			$thisEvent = $lsports->retrieveEventAndGroupNameByEventGroupID($d1['EventGroupID']);
			$groupID = $thisEvent['GroupID'];
			
			// Display Group Type for Open Event
			$eventGroupName = '';
			if($groupID == '-2')
			{
				$eventGroupName = $i_Sports_Event_Girls_Open;
			}
			else if($groupID == '-1')
			{
				$eventGroupName = $i_Sports_Event_Boys_Open;
			}
			else if($groupID == '-4')
			{
				$eventGroupName = $i_Sports_Event_Mixed_Open;
			}
			
			$DataAry[$i][] = $eventGroupName.$thisEvent[0];
		}
		$i++;
	}
}

$ExportArr = array();
$ExportArr[] = array($Lang["eSports"]["Class"] ." / ". $Lang["eSports"]["Form"] . ":". $export_classname);
$ExportArr[] = "";

$ExportRowHeader = array();
$ExportRowHeader  = array($i_ClassName, $i_ClassNumber, $i_Sports_Participant, $i_Sports_field_Gender, $i_Sports_field_Group, $i_House, $i_Sports_Participant_Number);
for($i=1;$i<=$maxEvent;$i++)
{
	$ExportRowHeader[] = $i_Sports_menu_Settings_TrackFieldName . " " . $i;
}
$ExportArr[] = $ExportRowHeader;
for($i=0;$i<sizeof($DataAry);$i++)
	$ExportArr[] = $DataAry[$i];

$filename = "ClassEnrol.csv";
$export_content = $lexport->GET_EXPORT_TXT($ExportArr, "", "\t", "\r\n", "\t", 0, "11");
$lexport->EXPORT_FILE($filename, $export_content);

intranet_closedb();
?>