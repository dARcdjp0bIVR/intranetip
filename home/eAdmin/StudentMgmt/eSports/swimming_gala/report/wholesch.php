<?php

# using: yat

#############################################
#	Date:	2010-11-15	YatWoon
#			Add "No. of Students Enrolled"
#			update export function  
#############################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libswimminggala.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$linterface 	= new interface_html();
$CurrentPage	= "PageReport_WholeSchool";

$lclass = new libclass();
$lswimminggala = new libswimminggala();
$lswimminggala->authSportsSystem();

# Retrieve Student Number and Enrolment Number of Different ClassLevel
$studentCount = $lswimminggala->retrieveClassLevelStudentCount();
$enrolCount = $lswimminggala->retrieveClassLevelNumberOfEnrolment();
$studentEnrolCount = $lswimminggala->retrieveClassLevelNumberOfStudentEnrolment();
// debug_pr($studentEnrolCount);

$studentTotal = 0;
$enrolTotal = 0;
for($i=0; $i<sizeof($studentCount); $i++)
{
	$studentTotal = $studentTotal + $studentCount[$i][0];
	$enrolTotal = $enrolTotal + $enrolCount[$i][0];
}

# Calculate the overall average number of enrolment
$averageCount = $enrolTotal/$studentTotal;
$overall_averageCount = round($averageCount, 2);

$sortname 	= $sortname=="" ? "level_name" : $sortname;
$sortorder	= $sortorder=="" ? 0 : $sortorder;
$arrow_icon = "icon_sort_". ($sortorder==1?"d":"a")."";
$mouseover_tag = "onMouseOver=\"MM_swapImage('sort_icon','','{$image_path}/{$LAYOUT_SKIN}/{$arrow_icon}_on.gif',1)\" onMouseOut='MM_swapImgRestore()'";
$arrow_display = "<img name='sort_icon' id='sort_icon' src='{$image_path}/{$LAYOUT_SKIN}/{$arrow_icon}_off.gif' align='absmiddle'  border='0' />";
                                                
### Content
$content = "";
$content .= "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'><tr><td>";
$content .= "<table width='100%' border='0' cellpadding='4' cellspacing='0' align='center'>";
$content .= "<tr>";
$content .= "<td class='tablebluetop tabletopnolink'><a ". ($sortname=="level_name"?$mouseover_tag:"")	." href=\"javascript:sort_order('level_name', 	". ($sortname=="level_name"? 	($sortorder==1?0:1):"0") .");\" class='tabletoplink'>$i_ClassLevel ". 				($sortname=="level_name"? 	$arrow_display:"")."</a></td>";
$content .= "<td class='tablebluetop tabletopnolink'><a ". ($sortname=="s_count"?$mouseover_tag:"") 	." href=\"javascript:sort_order('s_count', 	". ($sortname=="s_count"? 	($sortorder==1?0:1):"0") .");\" class='tabletoplink'>$i_Sports_Report_Total_Student_Count ". 	($sortname=="s_count"? 		$arrow_display:"")."</a></td>";
$content .= "<td class='tablebluetop tabletopnolink'><a ". ($sortname=="e_count"?$mouseover_tag:"") 	." href=\"javascript:sort_order('e_count', 	". ($sortname=="e_count"? 	($sortorder==1?0:1):"0") .");\" class='tabletoplink'>$i_Sports_Report_Enrolment_Count ". 	($sortname=="e_count"? 		$arrow_display:"")."</a></td>";
$content .= "<td class='tablebluetop tabletopnolink'><a ". ($sortname=="se_count"?$mouseover_tag:"") 	." href=\"javascript:sort_order('se_count', 	". ($sortname=="se_count"? 	($sortorder==1?0:1):"0") .");\" class='tabletoplink'>". $Lang['eSports']['NoOfStudentsEnrolled'] .	($sortname=="se_count"? 		$arrow_display:"")."</a></td>";
$content .= "<td class='tablebluetop tabletopnolink'><a ". ($sortname=="averageCount"?$mouseover_tag:"")." href=\"javascript:sort_order('averageCount', ". ($sortname=="averageCount"? 	($sortorder==1?0:1):"0") .");\" class='tabletoplink'>$i_Sports_Report_Average_Enrolment_Count ".($sortname=="averageCount"? 	$arrow_display:"")."</a></td>";
$content .= "</tr>";

## push the data into multi-array
$data = array();
$enrolCountAry = array(); 
foreach($enrolCount as $formEnrolCount)
{
	$enrolCountAry[$formEnrolCount[1]] = $formEnrolCount;
}
$studentenrolCountAry = array(); 
foreach($studentEnrolCount as $formEnrolCount)
{
	$studentenrolCountAry[$formEnrolCount[1]] = $formEnrolCount;
}
for($i=0; $i<sizeof($studentCount); $i++)
{
	list($s_count, $s_level, $level_name) = $studentCount[$i];
	list($e_count, $e_level) = $enrolCountAry[$s_level];
	list($se_count, $se_level) = $studentenrolCountAry[$s_level];
	
	//if($s_level == $e_level && $s_count != 0)
	//{
	//debug($i);
		# Calculate the average number of enrolment of this classlevel
		$averageCount = !empty($s_count)?$e_count/$s_count:0;
		$averageCount = round($averageCount, 2);

                $data[$i]['level_name']		= $level_name;
                $data[$i]['s_count'] 		= !empty($s_count)?$s_count:0;
                $data[$i]['e_count'] 		= !empty($e_count)?$e_count:0;
                $data[$i]['se_count'] 		= !empty($se_count)?$se_count:0;
                $data[$i]['averageCount'] 	= !empty($averageCount)?$averageCount:0;
	//}
}

foreach ($data as $key => $row) {
    $level_name_ary[$key]	= $row['level_name'];
    $s_count_ary[$key] 		= $row['s_count'];
    $e_count_ary[$key] 		= $row['e_count'];
    $se_count_ary[$key] 		= $row['se_count'];
    $averageCount_ary[$key] 	= $row['averageCount'];
}

array_multisort( ${$sortname."_ary"}, $sortorder==1?SORT_DESC:SORT_ASC, $data);

$display = "";
for($i=0; $i<sizeof($data); $i++)
{
                $display .= "<tr class='tablebluerow".($i%2?"2":"1")."'>";
		$display .= "<td class='tabletext' width='20%'>".$data[$i]['level_name']."</td>";
		$display .= "<td class='tabletext' width='20%'>".$data[$i]['s_count']."</td>";
		$display .= "<td class='tabletext' width='20%'>".$data[$i]['e_count']."</td>";
		$display .= "<td class='tabletext' width='20%'>".$data[$i]['se_count']."</td>";
		$display .= "<td class='tabletext' width='20%'>".$data[$i]['averageCount']."</td>";
		$display .= "</tr>";
}

$content .= $display;
$content .= "</table></td></tr>";
$content .= "<tr><td height='1' class='dotline'><img src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif' width='10' height='1'></td></tr>";
$content .= "</table>";

### Content End


### Title ###
$TitleTitle1 = "<span class='contenttitle'>". $i_Sports_menu_Report_WholeSchool ."</span>";
$TitleTitle = "<table width='100%' height='25' cellpadding='0' cellspacing='0' ><tr><td style=\"vertical-align: bottom;\" >".$TitleTitle1."</td></tr></table>";
$TAGS_OBJ[] = array($TitleTitle, "", 0);    

$MODULE_OBJ = $lswimminggala->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();
?>
<script language="javascript">
<!--
function sort_order(xname, xorder)
{
	with(document.form1)
        {
        	sortname.value	= xname;
                sortorder.value = xorder;
                
                submit();
        }
}
//-->
</script>

<br />
<form name="form1" method="get" action="wholesch.php">
<table width="96%" border="0" cellpadding="5" cellspacing="0">
<tr>
	<td><br />
        	<!----------- start of the total student and enrolment count table --------------------------->
		<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
		<tr>
			<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_Sports_Report_Total_Student_Count?> </span></td>
			<td valign="top" class="tabletext"><?=$studentTotal?></td>
		</tr>
                                
		<tr>
			<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_Sports_Report_Enrolment_Count?> </span></td>
			<td valign="top" class="tabletext"><?=$enrolTotal?></td>
		</tr>
                                
		<tr>
			<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_Sports_Report_Average_Enrolment_Count?> </span></td>
			<td valign="top" class="tabletext"><?=$overall_averageCount?></td>
		</tr>
                <tr>
			<td colspan="2" height="1" class="dotline"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
		</tr>
		</table>
	</td>
</tr>
<!-------------------------- start of class level report table ----------------------------------------------->
<tr>
	<td><br /><?=$content?></td>
</tr>        

</table>
<br />

<input type="hidden" name="sortname" value="<?=$sortname?>">
<input type="hidden" name="sortorder" value="<?=$sortorder?>">
</form>

<form name="form2" action='wholesch_export.php' method="post">
<table width="96%" border="0" cellpadding="5" cellspacing="0">
<tr>
	<td align='center'><?=$linterface->GET_ACTION_BTN($button_export, "submit", "","submit2")?></td>
</tr>

<input type="hidden" name="sortname" value="<?=$sortname?>">
<input type="hidden" name="sortorder" value="<?=$sortorder?>">
</form>
</table>
<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>