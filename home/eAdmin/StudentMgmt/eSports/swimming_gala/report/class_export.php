<?php
# using: yat
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libswimminggala.php");
// include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

// $lclass = new libclass();
$lswimminggala = new libswimminggala();
$lswimminggala->authSportsSystem();
$lexport = new libexporttext();

$studentCount = $lswimminggala->retrieveClassStudentCount();
$enrolCount = $lswimminggala->retrieveClassNumberOfEnrolmentAndScore();
$ClassRelayResult = $lswimminggala->retrieveClassRelayScore();	# count on Class Relay
$studentenrolCount = $lswimminggala->retrieveClassNumberOfStudentEnrolment();
 
$sortname 	= $sortname=="" ? "class_name" : $sortname;
$sortorder	= $sortorder=="" ? 0 : $sortorder;

## Export Content
$export_ary = array();
$export_ary[] = array($i_Sports_menu_Report_Class);
$export_ary[] = array();
$export_ary[] = array($i_ClassName, $i_Sports_Report_Total_Student_Count,$i_Sports_Report_Enrolment_Count,$Lang['eSports']['NoOfStudentsEnrolled'],$i_Sports_field_Score,$i_Sports_Report_Average_Enrolment_Count);

## push the data into multi-array
$data = array();

# build ClassID -> EnrolCount Mapping ($enrolCountAry[$YearClassID] = $EnrolCountInfo)
foreach($enrolCount as $thisEnrolCount)
	$enrolCountAry[$thisEnrolCount[2]] = $thisEnrolCount;

$studentenrolCountAry = array(); 
foreach($studentenrolCount as $formEnrolCount)
{
	$studentenrolCountAry[$formEnrolCount[1]] = $formEnrolCount;
}	
for($i=0; $i<sizeof($studentCount); $i++)
{
	list($s_count, $s_classid, $class_name_en,$class_name_b5) = $studentCount[$i];
	list($e_count, $score, $e_classid) 	= $enrolCountAry[$s_classid];
	list($se_count, $se_level) = $studentenrolCountAry[$s_classid];
	$class_name=Get_Lang_Selection($class_name_b5,$class_name_en);

	//if($s_classid == $e_classid )
	//{
		# Calculate the average number of enrolment of this classlevel
		$averageCount = $s_count!=0?$e_count/$s_count:0;
		$averageCount = round($averageCount, 2);

                $data[$i]['class_name']		= $class_name?$class_name:0;
                $data[$i]['s_count'] 		= $s_count?$s_count:0;
                $data[$i]['e_count'] 		= $e_count?$e_count:0;
                $data[$i]['se_count'] 		= $se_count?$se_count:0;
                $data[$i]['score'] 		= $score?round($score,1):0;
                if($ClassRelayResult[$e_classid] >0)
					$data[$i]['score'] += $ClassRelayResult[$e_classid];
                $data[$i]['averageCount'] 	= $averageCount?$averageCount:0;
	//}
}


foreach ($data as $key => $row) {
    $class_name_ary[$key]	= $row['class_name'];
    $s_count_ary[$key] 		= $row['s_count'];
    $e_count_ary[$key] 		= $row['e_count'];
    $se_count_ary[$key] 		= $row['se_count'];
    $score_ary[$key] 		= $row['score'];
    $averageCount_ary[$key] 	= $row['averageCount'];
}

array_multisort( ${$sortname."_ary"}, $sortorder==1?SORT_DESC:SORT_ASC, $data);

$display = "";
for($i=0; $i<sizeof($data); $i++)
{
	$export_ary[] = array($data[$i]['class_name'],$data[$i]['s_count'],$data[$i]['e_count'],$data[$i]['se_count'],$data[$i]['score'],$data[$i]['averageCount']);
}

$utf_content = "";
foreach($export_ary as $k=>$d)
{
	$utf_content .= implode("\t", $d);
	$utf_content .= "\r\n";
}

intranet_closedb();

$filename = "class.csv";
$lexport->EXPORT_FILE($filename, $utf_content); 
?>