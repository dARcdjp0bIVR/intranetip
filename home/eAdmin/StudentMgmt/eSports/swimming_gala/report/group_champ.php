<?php
# using: 

/*****************************************************************
 * 	Modification log
 * 	2016-05-30 Bill:	[2016-0527-1154-29206]
 * 	- correct wordings
 * ****************************************************************/ 
 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libswimminggala.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$linterface 	= new interface_html();
$CurrentPage	= "PageRaceResult";

$lclass = new libclass();
$lsports = new libswimminggala();
$lsports->authSportsSystem();

$AgeGroups = $lsports->retrieveAgeGroupInfo();	# retrieve all age group ids and names
$AgeGroups[] = array('M');
$AgeGroups[] = array('F');

$ShowRankFromTop = 3;
$WholeSchoolGenderChamp = array();
for($i=0; $i<sizeof($AgeGroups); $i++)
{
	$result= array();
	$groupID = $AgeGroups[$i][0];
	if(in_array($groupID,array('M','F')))
	{
		if(!empty($WholeSchoolGenderChamp[$groupID]))
		{
			sortByColumn2($WholeSchoolGenderChamp[$groupID],"total",1,1);
			$result = (array)array_slice((array)array_values((array)$WholeSchoolGenderChamp[$groupID]),0,$ShowRankFromTop);
		}
		$groupName = $groupID=='M'?$Lang['eSports']['AllBoysGroup']:$Lang['eSports']['AllGirlsGroup'];
	}
	else
	{
		$groupName = $intranet_session_language=="en"?$AgeGroups[$i][2]:$AgeGroups[$i][3];
	    $result = $lsports->retrieveGroupTopTenScore($groupID);
	}   
		$content .= "<table width='100%' border='0' cellspacing='0' cellpadding='4'>";
                $content .= "<tr>";
                $content .= "<td align='right' colspan='4'><table width='100%' border='0' cellspacing='0' cellpadding='0'>";
                $content .= "<tr>";
                $content .= "<td align='right'><table width='100%' border='0' cellspacing='0' cellpadding='0'>";
                $content .= "<tr>";
                $content .= "<td align='left'>". $linterface->GET_NAVIGATION2($groupName) ."</td>";
                $content .= "</tr>";
                $content .= "</table></td>";
                $content .= "</tr>";
                $content .= "<tr>";
                $content .= "<td height='10' align='right' class='tabletextremark'><table width='100%' border='0' cellspacing='0' cellpadding='0'>";
                $content .= "<tr>";
                $content .= "</tr>";
                $content .= "</table></td>";
                $content .= "</tr>";
                $content .= "</table></td>";
                $content .= "</tr>";
                
                $content .= "<tr><td>";
				$content .= "<table width='100%' border='0' cellspacing='0' cellpadding='0'><tr><td>";
                $content .= "<table width='100%' border='0' cellspacing='0' cellpadding='4'>";
                $content .= "<tr>";
                $content .= "<td class='tablebluetop tabletopnolink' width='50%'>$i_Sports_Participant</td>";
                $content .= "<td class='tablebluetop tabletopnolink' width='20%'>&nbsp;&nbsp;$i_Sports_House</td>";
                $content .= "<td class='tablebluetop tabletopnolink' width='20%'>$i_Sports_Participant_Number</td>";
                $content .= "<td class='tablebluetop tabletopnolink' width='10%'>$i_Sports_field_Score</td>";
                $content .= "</tr>";
                        
               
				if(sizeof($result)!=0)
                {
                	$EventGroupIDsArr = (array)$lsports->Get_EventGroup_Info();
                	$EventGroupIDsArr = BuildMultiKeyAssoc($EventGroupIDsArr,"EventGroupID",array("GroupID","EventID"));
                	
					for($j=0; $j<sizeof($result); $j++)
					{
						list($sid, $sname, $hname, $colorCode, $athNum, $status, $score) = $result[$j];
        				$WholeSchoolGenderChamp[$AgeGroups[$i]["Gender"]][$sid] = $result[$j];
        				
        			# 20080904 display the score details
        			$EventResult = $lsports->retrieveStudentEventResult($sid);
        			
        			$details_table = "<table width='100%' border='0' cellspacing='0' cellpadding='2' id='table_{$groupID}_{$sid}' style='display:none; border:1px solid #999999;'>";
        			$details_table .= "<tr>";
        			$details_table .= "<td class='tabletop tabletopnolink' >".$i_Sports_menu_Settings_TrackFieldEvent."</td>";
        			$details_table .= "<td class='tabletop tabletopnolink' width='15%' align='center'>".$i_Sports_field_Rank."</td>";
        			$details_table .= "<td class='tabletop tabletopnolink' width='15%' align='center'>".$i_Sports_field_Score."</td>";
        			$details_table .= "</tr>";
					
        			for($k=0;$k<sizeof($EventResult);$k++)
        			{
				 		$tempGroupID = $EventGroupIDsArr[$EventResult[$k]['EventGroupID']];
				 		
				 		if($tempGroupID['GroupID'] > 0)
						{
							$EventGroupInfo = $lsports->retrieveEventAndGroupNameByEventGroupID($EventResult[$k]['EventGroupID']);
							$ageGroupName  = $EventGroupInfo[1];
						}
						else
						{
							$eventID = $tempGroupID['EventID'];
							 $db_field = ($intranet_session_language=="en"?"EnglishName":"ChineseName");
						
//							$sql = "SELECT EventType, $db_field FROM SPORTS_EVENT WHERE EventID = '$eventID'";
//							$temp = $lsports->returnArray($sql, 2);
						
							if($tempGroupID['GroupID'] == '-1')
								$ageGroupName = $i_Sports_Event_Boys_Open;
							else if($tempGroupID['GroupID'] == '-2')
								$ageGroupName = $i_Sports_Event_Girls_Open;
							else if($tempGroupID['GroupID'] == '-4')
								$ageGroupName = $i_Sports_Event_Mixed_Open;
						}
						
						
	        			$css = ($k % 2)+1;
	        			//list($tempStudentID, $tempEventName, $tempRank, $tempScore) = $EventResult[$k];
	        			$tempRank = $EventResult[$k]['Rank'] ? $EventResult[$k]['Rank'] : "-";
	        			$tempScore = $EventResult[$k]['Score'] ? round($EventResult[$k]['Score'],1) : "-";
						$details_table .= "<tr class='tablerow". $css."'>";
	        			$details_table .= "<td>".$EventResult[$k]['event_name']." (". $ageGroupName .") </td>";
	        			$details_table .= "<td align='center'>".$tempRank."</td>";
	        			$details_table .= "<td align='center'>".$tempScore."</td>";
	        			$details_table .= "</tr>";
        			}
        			$details_table .= "</table>";
        			
					$content .= "<tr class='tablebluerow".($j%2?"2":"1")."'>";
        			$content .= "<td valign='top' class='tabletext'><a href='javascript:toggle_details(\"".$groupID."_".$sid."\")' class='tablebluelink'>".$sname."</a>". $details_table ."</td>";
        			$content .= "<td valign='top' class='tabletext'>".$lsports->house_flag2($colorCode, $hname)."</td>";
        			$content .= "<td valign='top' class='tabletext'>".$athNum."</td>";
        			$content .= "<td valign='top' class='tabletext'>".round($score,1)."</td>";
        			$content .= "</tr>";
					}
                }
                else
                {
                        $content .= "<tr class='tablebluerow1'>";
                        $content .= "<td class='tabletext' align='center' colspan='4'>$i_no_record_exists_msg</td>";
                        $content .= "</tr>";
                }
                $content .= "</table></td></tr>";
                $content .= "<tr>";
		$content .= "<td colspan='4' height='1' class='dotline'><img src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif' width='10' height='1'></td>";
                $content .= "</tr>";
                $content .= "</table>";
		$content .= "</td></tr>";
                
                $content .= "</table><br /><br />";
}

### Title ###
$house_relay_name = $lsports->retrieveEventTypeNameByID(3);
$class_relay_name = $lsports->retrieveEventTypeNameByID(4);

$TAGS_OBJ[] = array($Lang['eSports']['Individual'],"../participation/tf_record.php", 0);
$TAGS_OBJ[] = array($house_relay_name,"../participation/relay_record.php", 0);
$TAGS_OBJ[] = array($class_relay_name,"../participation/class_relay_record.php", 0);
$TAGS_OBJ[] = array($i_Sports_menu_Report_EventRanking,"../report/event_rank.php", 0);
$TAGS_OBJ[] = array($i_Sports_menu_Report_HouseGroupScore,"../report/house_group.php", 0);
$TAGS_OBJ[] = array($i_Sports_menu_Report_GroupChampion,"../report/group_champ.php", 1);
$MODULE_OBJ = $lsports->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();
?>
<script language="javascript">
<!--
function toggle_details(tid)
{
	/*obj = eval("table_"+tid+".style");
	if(obj.display=='none')
		obj.display='inline';
	else
		obj.display='none';*/
	$("#table_"+tid).toggle();

}

function js_Export(withResult)
{
	$("input#WithResult").val(withResult);
	document.form_export.submit()
}
//-->
</script>
<br />
<table width="96%" border="0" cellpadding="5" cellspacing="0">
<tr>
	<td><br /><?=$content?></td>
</tr>        
<tr>
	<td align="center">
		<form name="form_export" action="group_champ_export.php">
		<?= $linterface->GET_ACTION_BTN($Lang['eSports']['ExportTotalScore'], "button","js_Export(0)") ?>
		<?= $linterface->GET_ACTION_BTN($Lang['eSports']['ExportTotalScoreWithEventDetails'], "button","js_Export(1)") ?>
		<input type="hidden" id="WithResult" name="WithResult">
		</form>
	</td>
</tr>
</table>

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>