<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libswimminggala.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

$linterface = new interface_html();

intranet_opendb();

$le = new libexporttext();
$lswimminggala = new libswimminggala();
$lswimminggala->authSportsSystem();

$ageGroup = $_POST['ageGroup'];
$groupType = $_POST['groupType'];
$className = $_POST['className'];
$house = $_POST['house'];
$raceDay = $_POST['raceDay'];

$Students = $lswimminggala->getAthleticNumDeatil($groupType, $className, $house, $ageGroup, $raceDay);

$content = "";
$count = 1;

$content .= "<br />";
$content .= "<br />";
$content .= "<br />";

if(sizeof($Students) != 0)
{
	foreach($Students as $key => $value)
	{
		list($sname, $gname, $hname, $ahtleticNum, $detail) = $value;
		
		/*  export html format to excel 
		$breakStyle = ($count%2 == 0)?"style='page-break-after:always'":"";
		
		$content .= "<table width='70%' border='0' cellpadding='0' cellspacing='0' align='center' $breakStyle>";
		$content .= "<tr>";
		$content .= "<td align='right' width='40%'><font size='4'>".$gname."</font></td>";
		$content .= "<td align='center' width='25%'><font size='4'>".$hname."</font></td>";
		$content .= "<td><font size='3'>".$sname."</font></td>";
		$content .= "</tr>";
		$content .= "<tr><td colspan='3'>&nbsp;</td></tr>";
		$content .= "<tr><td colspan='3'>&nbsp;</td></tr>";
		$content .= "<tr><td colspan='3' align='center'><p class='tabletext text1'>".$ahtleticNum."</p></td></tr>";
		$content .= "<tr><td colspan='3'>&nbsp;</td></tr>";
		$content .= "<tr><td colspan='3'>&nbsp;</td></tr>";
		$content .= "<tr><td colspan='3'><font size='4'>&nbsp;".$detail."</font></td></tr>";
		$content .= "</table>";
		$content .= "<br />";
		$content .= "<br />";
		
		$content .= "<br />";
		$content .= "<br />";
		$content .= "<br />";
		$content .= "<br />";
		$content .= "<br />";
		$content .= "<br />";

		$count++;
		*/
		$export_content[] = array($gname,$hname,$sname,$ahtleticNum,$detail);
	}
	//debug_pr($export_content);
}
else
{
	/*
	$content .= "<table width='70%' border='0' cellpadding='0' cellspacing='0' align='center' $breakStyle>";
	$content .= "<tr><td align='center' class='tabletext'>".$i_no_record_exists_msg."</td></tr>";
	$content .= "<tr><td>&nbsp;</td></tr>";
        $content .= "<tr><td align='center'>". $linterface->GET_BTN($button_close, "button", "javascript:self.close();","clost_btn") ."</td></tr>";
	$content .= "</table>";
	*/
	$export_content[] = array($i_no_record_exists_msg);
}
	/*
	header("Pragma: public");  
    header("Expires: 0");  
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");   
    header("Content-Type: application/force-download");  
    header("Content-Type: application/octet-stream");  
    header("Content-Type: application/download");;  
    header("Content-Disposition: attachment;filename=athletic_num.xls");  
    header("Content-Transfer-Encoding: binary ");
    */
    $export_text = $le->GET_EXPORT_TXT($export_content,$export_header);
    $filename = "athletic_num.csv";
    $le->EXPORT_FILE($filename,$export_text); 
    
//echo $content;

intranet_closedb();
?>