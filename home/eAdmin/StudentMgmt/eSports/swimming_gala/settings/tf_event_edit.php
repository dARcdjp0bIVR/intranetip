<?php
# using: yat

############# Change Log [Start] ######
#
#	Date:	2010-09-06	YatWoon
#			update the max person/group from 40 to 100
#
#	Date:	2010-05-28	YatWoon
#			All "- select -" for house selection (requested by client, their house is different every year, so this field is no need)
#
############# Change Log [End] ######

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libswimminggala.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$lswimminggala = new libswimminggala();
$lswimminggala->authSportsSystem();

$linterface 	= new interface_html();
$CurrentPage	= "PageItemSettings";

$EventGroupID = (is_array($EventGroupID)?$EventGroupID[0]:$EventGroupID);

# Retrieve EventGroup Information By EventGroupID
$detail = $lswimminggala->retrieveEventGroupDetail($EventGroupID);
list($eventgroup_id, $event_id, $group_id, $online_enrol, $quota_count, $house_count, $class_count, $individual_count, $score_standand,$event_code, $event_quota) = $detail;
$event_code= empty($event_code)?$EventGroupID:$event_code;
$enroled_no = $lswimminggala->returnEventEnroledNo($eventgroup_id);

#Retrive Event Name and Type
$db_event_name = ($intranet_session_language=="en"?"a.EnglishName":"a.ChineseName");
$db_type_name = ($intranet_session_language=="en"?"b.EnglishName":"b.ChineseName");
$sql = "SELECT $db_type_name, $db_event_name, a.EventType FROM SWIMMINGGALA_EVENT as a LEFT JOIN SWIMMINGGALA_EVENT_TYPE_NAME as b ON a.EventType = b.EventTypeID WHERE a.EventID = '$event_id'";
$event = $lswimminggala->returnArray($sql,3);
list($eventTypeName, $eventName, $eventTypeID) = $event[0];

#Retrieve EventGroup Setting Information
$ext_info = $lswimminggala->retrieveEventGroupExtInfo($EventGroupID, $eventTypeID);

#Create Score Standard Selection List
$standards = $lswimminggala->retrieveScoreStandards();
$select_standard = getSelectByArray($standards,"name=scoreStandard",$score_standand,0,1);

#Create Race Day Selection List
for($i=1; $i<4; $i++)
{
	if($i == 1)
	{
		for($j=1; $j<4; $j++)
			${"selected".$j} = ($ext_info['FirstRoundDay'] == $j) ? "SELECTED" : "";
	}
	else if($i == 2)
	{
		for($j=1; $j<4; $j++)
			${"selected".$j} = ($ext_info['SecondRoundDay'] == $j) ? "SELECTED" : "";
	}
	else if($i == 3)
	{
		for($j=1; $j<4; $j++)
			${"selected".$j} = ($ext_info['FinalRoundDay'] == $j) ? "SELECTED" : "";
	}
	${"race_day".$i} = "<SELECT name='race_day".$i."'>";
	${"race_day".$i} .= "<OPTION value=1 $selected1>1</OPTION>";
	${"race_day".$i} .= "<OPTION value=2 $selected2>2</OPTION>";
	${"race_day".$i} .= "<OPTION value=3 $selected3>3</OPTION>";
	${"race_day".$i} .= "</SELECT>";
}	

# Retrieve Age Group Name
switch($group_id)
{
	case -1:	$group_name  = $i_Sports_Event_Boys_Open; break;
        case -2:	$group_name  = $i_Sports_Event_Girls_Open; break;
        case -4:	$group_name  = $i_Sports_Event_Mixed_Open; break;
        default:
                        $db_group_name = ($intranet_session_language=="en"?"EnglishName":"ChineseName");
                        $sql = "SELECT $db_group_name FROM SWIMMINGGALA_AGE_GROUP WHERE AgeGroupID = '$group_id'";
                        $group = $lswimminggala->returnArray($sql,1);
                        $group_name = $group[0][0];
                        break;
}

# Check whether the Ext Info of this Event Enrolment has been existed
$is_setup = $lswimminggala->checkExtInfoExist($eventTypeID, $eventGroupID);

# Create House Selection List
$houses = $lswimminggala->retrieveHouseSelectionInfo();
$select_house = getSelectByArray($houses,"name=house",$ext_info['RecordHouseID'],0,0);

	# Create First, Second and Final Round Person Per Group Selection List
	if($eventTypeID == 1)
	{
		$persons[0] = $i_Sports_field_NumberOfLane;
		$num = 3;
		for($i=1; $i<=98; $i++)
		{
			$persons[$i] = $num;
			$num++;
		}
	}
	else
	{
		$num = 3;
		for($i=0; $i<=97; $i++)
		{
			$persons[$i] = $num;
			$num++;
		}
	}

	if($ext_info["FirstRoundType"] == 0)
	{
		$lanesCount = $ext_info["FirstRoundGroupCount"];
		$groupsCount = "";
	}
	else if($ext_info["FirstRoundType"] == 1)
	{
		$lanesCount = "";
		$groupsCount = $ext_info["FirstRoundGroupCount"];
	}
	else
	{
		$lanesCount = "";
		$groupsCount = "";
	}

	$select_person1 = getSelectByValue($persons,"name=personNum1",$lanesCount,0,1);
	$select_person2 = getSelectByValue($persons,"name=personNum2",$ext_info["SecondRoundLanes"],0,1);
	$select_person3 = getSelectByValue($persons,"name=personNum3",$ext_info["FinalRoundNum"],0,1);

	# Create First and Second Round Number of Group Selection List
	$count1 = 1;
	for($i=0; $i<10; $i++)
	{
		$groups1[$i] = $count1;
		$count1++;
	}
	$select_group1 = getSelectByValue($groups1,"name=groupNum1",$groupsCount,0,1);

	$count2 = 2;
	for($i=0; $i<19; $i++)
	{
		$groups2[$i] = $count2;
		$count2++;
	}
	$select_group2 = getSelectByValue($groups2,"name=groupNum2",$ext_info["SecondRoundGroups"],0,1);

### Title ###
$TAGS_OBJ[] = array($i_Sports_menu_Settings_TrackFieldName,"tf_name.php", 0);
$TAGS_OBJ[] = array($i_Sports_menu_Settings_TrackFieldEvent,"tf_event.php", 1);
$MODULE_OBJ = $lswimminggala->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();

$PAGE_NAVIGATION[] = array($button_edit.($intranet_session_language=="en"?" ":""). $i_Sports_menu_Settings_TrackFieldEvent, "");
?>
<SCRIPT LANGUAGE=Javascript>
function checkform(obj)
{
    if(!obj.event_code.value)
    {
    	alert("<?=$Lang['eSports']['warnNoEventCode']?>");
		return false;
    }
    
	<? if($eventTypeID == '1') {?>
	if(obj.SecondRoundReq.checked)
	{
		if(obj.FinalRoundReq.checked)
			return true;
		else
		{
			alert("<?=$i_Sports_Setting_Alert_FinalRound_Req?>");
			return false;
		}
	}
	<? } ?>
    
    // check event quota
	if(obj.eventQuotaYN[0].checked)
	{
		var this_quota = obj.eventQuota.value;
		if(!isInteger(this_quota) || this_quota<1 || Trim(this_quota)=='')
		{
			alert("<?=$Lang['eSports']['Quota_jsAlert']?>");
			return false;
		}
		
		if(this_quota < <?=$enroled_no?>)
		{
			alert("<?=$Lang['eSports']['Quota_jsAlert_LessThanEnrolled']?>");
			return false;
		}
	}
	
    
	return true;
}
</SCRIPT>

<br />   
<form name="form1" method="post" action="tf_event_edit_update.php" onSubmit="return checkform(this);">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	<td align='right'><?=$linterface->GET_SYS_MSG($xmsg);?></td>
</tr>
<tr>
	<td colspan="2">
                <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr> 
                	<td><br />
				<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
				<tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_Sports_field_Event_Type?> </span></td>
					<td class="tabletext"><?=$eventTypeName?></td>
				</tr>
                                <tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_Sports_field_Event_Name?> </span></td>
					<td class="tabletext"><?=$eventName?></td>
				</tr>
                                 <tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_Sports_field_Grade?> </span></td>
					<td class="tabletext"><?=$group_name?></td>
				</tr>
			<tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$Lang['eSports']['EventCode']?>  <span class='tabletextrequire'>*</span></span></td>
					<td class="tabletext"><input name="event_code" type="text" class="textboxtext" maxlength="20" size="2" value="<?=$event_code?>" /></td>
				</tr>
				
				<?php if($eventTypeID == '1' ) {?>
                                <tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_Sports_Event_Set_OnlineEnrol?> </span></td>
					<td class="tabletext"><input type="radio" name="onlineEnrol" value="1" <?=($online_enrol==1)?"checked":""?> id="onlineEnrol1"> <label for="onlineEnrol1"><?=$i_general_yes?></label> &nbsp; <input type="radio" name="onlineEnrol" value="0" <?=($online_enrol==0)?"checked":"";?> id="onlineEnrol2"> <label for="onlineEnrol2"><?=$i_general_no?></label></td>
				</tr>
                                <tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_Sports_Event_Set_RestrictQuota?> </span></td>
					<td class="tabletext"><input type="radio" name="Quota" value="1" <?=($quota_count==1)?"checked":""?> id="Quota1"> <label for="Quota1"><?=$i_general_yes?></label> &nbsp; <input type="radio" name="Quota" value="0" <?=($quota_count==0)?"checked":""?> id="Quota2"> <label for="Quota2"><?=$i_general_no?></label></td>
				</tr>
                                <tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_Sports_Event_Set_CountClassScore?> </span></td>
					<td class="tabletext"><input type="radio" name="countClass" value="1" <?=($class_count==1)?"checked":""?> id="countClass1"> <label for="countClass1"><?=$i_general_yes?></label> &nbsp; <input type="radio" name="countClass" value="0" <?=($class_count==0)?"checked":""?> id="countClass2"> <label for="countClass2"><?=$i_general_no?></label></td>
				</tr>
                                <tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_Sports_Event_Set_CountIndividualScore?> </span></td>
					<td class="tabletext"><input type="radio" name="countIndividual" value="1" <?=($individual_count==1)?"checked":""?> id="countIndividual1"> <label for="countIndividual1"><?=$i_general_yes?></label> &nbsp; <input type="radio" name="countIndividual" value="0" <?=($individual_count==0)?"checked":""?> id="countIndividual2"> <label for="countIndividual2"><?=$i_general_no?></label></td>
				</tr>
                                <? } ?>
                                
				<?php if($eventTypeID == '4') {?>
					<tr valign="top">
						<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_Sports_Event_Set_CountClassScore?> </span></td>
						<td class="tabletext"><input type="radio" name="countClass" value="1" <?=($class_count==1)?"checked":""?> id="countClass1"> <label for="countClass1"><?=$i_general_yes?></label> &nbsp; <input type="radio" name="countClass" value="0" <?=($class_count==0)?"checked":""?> id="countClass2"> <label for="countClass2"><?=$i_general_no?></label></td>
					</tr>
				<? } ?>
                                
				<?php if($eventTypeID == '1' || $eventTypeID == '3') {?>
                                <tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_Sports_Event_Set_CountHouseScore?> </span></td>
					<td class="tabletext"><input type="radio" name="countHouse" value="1" <?=($house_count==1)?"checked":""?> id="countHouse1"> <label for="countHouse1"><?=$i_general_yes?></label> &nbsp; <input type="radio" name="countHouse" value="0" <?=($house_count==0)?"checked":""?> id="countHouse0"> <label for="countHouse0"><?=$i_general_no?></label></td>
				</tr>
				<? } ?>
				
				<tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$Lang['eSports']['EventQuota']?> </span></td>
					<td class="tabletext">
						<input type="radio" name="eventQuotaYN" value="1" <?=($event_quota>0)?"checked":""?> id="eventQuota1" onClick='document.form1.eventQuota.disabled=false;'> <label for="eventQuota1"><?=$i_general_yes?></label> ( <?=$Lang['eSports']['Quota']?>: <input type="text" class="textboxnum2" name="eventQuota" <?=$event_quota ? "":" disabled"?> value="<?=$event_quota?>">, <?=$Lang['eSports']['Enrolled']?>: <?=$enroled_no?> )&nbsp; 
						<input type="radio" name="eventQuotaYN" value="0" <?=($event_quota==0)?"checked":""?> id="eventQuota2" onClick='document.form1.eventQuota.disabled=true;'> <label for="eventQuota2"><?=$i_general_no?></label></td>
				</tr>
				
                                <tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_Sports_Event_Set_ScoreStandard?> </span></td>
					<td class="tabletext"><?=$select_standard?></td>
				</tr>
                                <tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_Sports_Record_Holder?> </span></td>
					<td><input name="record_holder" type="text" class="textboxtext" maxlength="255" value="<?=$ext_info['RecordHolderName']?>" /></td>
				</tr>
                                <tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_Sports_Record?> </span></td>
                    <td><input type="text" name="record_min" class="textboxnum" value="<?=$ext_info['RecordMin']?>">'<input type="text" name="record_sec" class="textboxnum" value="<?=$ext_info['RecordSec']?>">''<input type="text" name="record_ms" class="textboxnum" value="<?=$ext_info['RecordMs']?>"></td>
                                        
				</tr>
                                <tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_Sports_Record_Year?> </span></td>
					<td><input name="record_year" type="text" class="textboxtext" maxlength="255" value="<?=$ext_info['RecordYear']?>" /></td>
				</tr>
				
				<?php if($eventTypeID == '1'  || $eventTypeID == '3') {?>
				<tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_Sports_House?> </span></td>
					<td><?=$select_house?></td>
				</tr>
				<? } ?>
                                <tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_Sports_Standard_Record?> </span></td>
                    <td><input type="text" name="standard_min" class="textboxnum" value="<?=$ext_info['StandardMin']?>">'<input type="text" name="standard_sec" class="textboxnum" value="<?=$ext_info['StandardSec']?>">''<input type="text" name="standard_ms" class="textboxnum" value="<?=$ext_info['StandardMs']?>"></td>
				</tr>
                                
                                <? if($eventTypeID == '1' || $eventTypeID == '2') {?>
                                
                                <tr>
                                	<td class="dotline" colspan="2"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                                </tr>
                                <tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_Sports_First_Round?> </span></td>
					<td class="tabletext"><input type="radio" name="FirstRoundType" value="0" <?=($ext_info['FirstRoundType']==0?"CHECKED":"")?> id="FirstRoundType0"><label for="FirstRoundType0"><?=$i_Sports_Person_Per_Group?></label>:<?=$select_person1?></td>
				</tr>
                                <tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext">&nbsp;</span></td>
					<td class="tabletext"><input type="radio" name="FirstRoundType" value="1" <?=($ext_info['FirstRoundType']==1?"CHECKED":"")?> id="FirstRoundType1"><label for="FirstRoundType1"><?=$i_Sports_Num_Of_Group?></label>:<?=$select_group1?></td>
				</tr>
                                <tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext">&nbsp;</span></td>
					<td class="tabletext"><input type="checkbox" name="FirstRoundRandom" value="1" <?=($ext_info['FirstRoundRandom']==1?"CHECKED":"")?> id="FirstRoundRandom1"><label for="FirstRoundRandom1"><?=$i_Sports_Random_Order?></label></td>
				</tr>
                                <tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext">&nbsp;</span></td>
					<td class="tabletext"><?=$i_Sports_Race_Day?>: <?=$race_day1?></td>
				</tr>

                                <? if($eventTypeID == '1') {?>
                                <tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><input type="checkbox" name="SecondRoundReq" value="1" <?=($ext_info["SecondRoundReq"]==1?"CHECKED":"")?> id="SecondRoundReq1"><label for="SecondRoundReq1"><?=$i_Sports_Second_Round?></label></span></td>
					<td class="tabletext"><?=$i_Sports_Person_Per_Group?>:<?=$select_person2?></td>
				</tr>
                                <tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext">&nbsp;</span></td>
					<td class="tabletext"><?=$i_Sports_Num_Of_Group?>:<?=$select_group2?></td>
				</tr>
                                <tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext">&nbsp;</span></td>
					<td class="tabletext"><?=$i_Sports_Race_Day?>: <?=$race_day2?></td>
				</tr>
                                
                        	<? } ?>
                                
                                <tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><input type="checkbox" name="FinalRoundReq" value="1" <?=($ext_info["FinalRoundReq"]==1?"CHECKED":"")?> id="FinalRoundReq1"><label for="FinalRoundReq1"><?=$i_Sports_Final_Round?></label></span> <font color="green">#</font></td>
					<td class="tabletext"><?=$i_Sports_Person_Per_Group?>:<?=$select_person3?></td>
				</tr>
                                <tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext">&nbsp;</span></td>
					<td class="tabletext"><?=$i_Sports_Race_Day?>: <?=$race_day3?></td>
				</tr>
                                <? } ?>
 			</table>
			</td>
                </tr>
                </table>
	</td>
</tr>
<tr>
	<td colspan="2">        
                <table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
                <tr>
                	<td align="left" class="tabletextremark">&nbsp;&nbsp;&nbsp;<?=$i_general_required_field2?></td>
				</tr>
				
				<? if($eventTypeID == '1' || $eventTypeID == '2') {?>
				<tr>
                	<td align="left" class="tabletextremark">&nbsp;&nbsp;&nbsp;<font color="green">#</font> <?=$Lang['eSports']['FinalRoundSettingWarning']?></td>
				</tr>
				<? } ?>
				 
                <tr>
                	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
                <tr>
			<td align="center">
				<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "","submit2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
                                <?= $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
				<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='tf_event.php'","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
			</td>
		</tr>
                </table>                                
	</td>
</tr>
</table>                        
<br />

<input type="hidden" name="EventGroupID" value="<?=$EventGroupID?>">
<input type="hidden" name="EventTypeID" value="<?=$eventTypeID?>">
<input type="hidden" name="AgeGroupID" value="<?=$AgeGroupID?>">
<input type="hidden" name="eventID" value="<?=$eventID?>">
</form>

<?
intranet_closedb();
if($eventTypeID==1 || $eventTypeID==2)
	print $linterface->FOCUS_ON_LOAD("form1.onlineEnrol[0]");
$linterface->LAYOUT_STOP();
?>
