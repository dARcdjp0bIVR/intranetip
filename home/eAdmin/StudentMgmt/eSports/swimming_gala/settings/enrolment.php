<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libswimminggala.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");

intranet_auth();
intranet_opendb();

$linterface 	= new interface_html();
$CurrentPage	= "PageAnnualSettings";

# Get House info
$lswimminggala = new libswimminggala();
$lswimminggala->authSportsSystem();

$names = $lswimminggala->retrieveEventTypeName();
$txt_track = $names[0];
$txt_field = $names[1];

$select_track = "<SELECT name='num_track'>\n";
for ($i=0; $i<=15; $i++)
{
     $select_track .= "<OPTION value='$i' ".($lswimminggala->enrolMaxTrack==$i?"SELECTED":"").">$i</OPTION>\n";
}

$select_track .= "</SELECT>\n";

### Title ###
$TAGS_OBJ[] = array($i_Sports_menu_Settings_AgeGroup,"agegroup.php", 0);
$TAGS_OBJ[] = array($i_Sports_menu_Settings_ParticipantGenerate,"participant_gen.php", 0);
$TAGS_OBJ[] = array($i_Sports_menu_Settings_Enrolment,"enrolment.php", 1);
$TAGS_OBJ[] = array($i_Sports_menu_Settings_YearEndClearing,"record_clear.php", 0);
$MODULE_OBJ = $lswimminggala->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();

?>
<SCRIPT LANGUAGE="Javascript">
<!--
function checkForm(obj)
{
	if(!check_text(obj.date_start, "<?=$i_alert_pleasefillin.$i_Sports_field_Enrolment_Date_start?>")) return false;
        if(!check_date(obj.date_start, "<?=$i_invalid_date?>")) return false;
        if(!check_text(obj.date_end, "<?=$i_alert_pleasefillin.$i_Sports_field_Enrolment_Date_end?>")) return false;
        if(!check_date(obj.date_end, "<?=$i_invalid_date?>")) return false;
        
        if(compareDate(obj.date_start.value,obj.date_end.value)==1)
        { 
        	alert("<?=$i_invalid_date?>");
                obj.date_start.focus();
                return false;
	}

        //Check the number of application
        if( parseInt(obj.num_track.value) + parseInt(obj.num_field.value) < parseInt(obj.num_total.value))
        {
        	alert("<?=$i_Sports_Invalid_Data?>");
                obj.num_total.focus();
                return false;
        }
        
}
//-->
</SCRIPT>


<br />   
<form name="form1" method="post" action="enrolment_update.php" onSubmit="return checkForm(this);">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td>&nbsp;</td>
	<td align='right'><?=$linterface->GET_SYS_MSG($xmsg);?></td>
</tr>
<tr>
	<td colspan="2">
                <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr> 
                	<td>
				<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
				<tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?="$i_Sports_field_Enrolment_Date_start"?> <span class='tabletextrequire'>*</span></span></td>
					<td><input name="date_start" type="text" class="textboxnum" value="<?=$lswimminggala->enrolDateStart?>"/> <?=$linterface->GET_CALENDAR("form1","date_start")?></td>
				</tr>
                                <tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?="$i_Sports_field_Enrolment_Date_end"?> <span class='tabletextrequire'>*</span></span></td>
					<td><input name="date_end" type="text" class="textboxnum" value="<?=$lswimminggala->enrolDateEnd?>"/> <?=$linterface->GET_CALENDAR("form1","date_end")?></td>
				</tr>
                                <tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?="$i_Sports_Enrolment_MaxEvent ($txt_track)"?> </span></td>
					<td><?=$select_track?></td>
				</tr>
                                
                                <tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?="$i_Sports_Enrolment_Detail"?> </span></td>
					<td><?=$linterface->GET_TEXTAREA("detail", $lswimminggala->enrolDetails);?></td>
				</tr>
			</table>
			</td>
                </tr>
                </table>
	</td>
</tr>
<tr>
	<td colspan="2">        
                <table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
                <tr>
                	<td align="left" class="tabletextremark">&nbsp;&nbsp;&nbsp;<?=$i_general_required_field2?></td>
		</tr>
                <tr>
                	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
                <tr>
			<td align="center">
				<?= $linterface->GET_ACTION_BTN($button_save, "submit", "","submit2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
                                <?= $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
			</td>
		</tr>
                </table>                                
	</td>
</tr>
</table>                        
<br />
</form>

<?
intranet_closedb();
print $linterface->FOCUS_ON_LOAD("form1.date_start");
$linterface->LAYOUT_STOP();
?>