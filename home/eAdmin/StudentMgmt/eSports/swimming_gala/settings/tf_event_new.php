<?php
# using: Isaac

#################################################
#
#   Date:   2018-03-15 Isaac
#           Added remarks for enrol selection field which no online enrol for class relay and house relay types
#
#	Date:	2012-06-13	YatWoon
#			add "Event Quota"
#
#################################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libswimminggala.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$lswimminggala = new libswimminggala();
$lswimminggala->authSportsSystem();

$linterface 	= new interface_html();
$CurrentPage	= "PageItemSettings";

$db_field = ($intranet_session_language=="en"?"EnglishName":"ChineseName");
$sql = "SELECT EventID, $db_field, EventType FROM SWIMMINGGALA_EVENT ORDER BY EventType, DisplayOrder";
$events = $lswimminggala->returnArray($sql,3);
$select_events = getSelectByArray($events, "name='EventID' onChange='check_event_type();'",$eventID,0,1);

# Check if the first entry is class relay
$default_display_open_only = $events[0]['EventType']==4 ? true : false;

$class_relay_ary = array();
foreach($events as $k=>$data)
{
	if($data['EventType']==4)		
		$class_relay_ary[] = $data['EventID'];
}


$groups = $lswimminggala->retrieveAgeGroupInfo();
$curr_gender = $groups[0][4];
$select_group = "";
$select_open_group = "";
$select_open_group .= "<input type='checkbox' name='GroupID[]' value='-1' id='GroupIDBO'> <label for='GroupIDBO'>$i_Sports_Event_Boys_Open</label> &nbsp;";
$select_open_group .= "<input type='checkbox' name='GroupID[]' value='-2' id='GroupIDGO'> <label for='GroupIDGO'>$i_Sports_Event_Girls_Open</label> &nbsp;";
$select_open_group .= "<input type='checkbox' name='GroupID[]' value='-4' id='GroupIDMO'> <label for='GroupIDMO'>$i_Sports_Event_Mixed_Open</label> &nbsp;";

for ($i=0; $i<sizeof($groups); $i++)
{
     $t_groupID = $groups[$i][0];
     $t_engName = $groups[$i][2];
     $t_chiName = $groups[$i][3];
     $t_gender = $groups[$i][4];
     $str_name = ($intranet_session_language=="en"?$t_engName:$t_chiName);

     if ($curr_gender != $t_gender)
     {
         $select_group .= "<br>\n";
         $curr_gender = $t_gender;
     }
     $select_group .= "<input type='checkbox' name='GroupID[]' value='$t_groupID' id='GroupID".$i."'> <label for='GroupID".$i."'>$str_name</label> &nbsp;";
}
$select_group .= "<br>\n";
//$select_group .= $select_open_group;


$standards = $lswimminggala->retrieveScoreStandards();
$select_standard = getSelectByArray($standards,"name=scoreStandard","",0,1);

### Title ###
$TAGS_OBJ[] = array($i_Sports_menu_Settings_TrackFieldName,"tf_name.php", 0);
$TAGS_OBJ[] = array($i_Sports_menu_Settings_TrackFieldEvent,"tf_event.php", 1);
$MODULE_OBJ = $lswimminggala->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();

$PAGE_NAVIGATION[] = array($button_new.($intranet_session_language=="en"?" ":""). $i_Sports_menu_Settings_TrackFieldEvent, "");


?>
<SCRIPT LANGUAGE=Javascript>
var is_CLassRelay=0;

function checkForm(obj)
{
	if(is_CLassRelay==1)
		return true;
		
	len  = obj.elements.length;
	var i=0;
	var openExist = 0;
	var nonOpenExist = 0;

	for( i=0 ; i<len ; i++) 
	{
		if (obj.elements[i].name=="GroupID[]")
		{
			if(obj.elements[i].checked && obj.elements[i].value<0)
			{
				openExist = 1;
			}
			else if(obj.elements[i].checked && obj.elements[i].value>0)
			{
				nonOpenExist = 1;
			}
		}
	}
	/*
	if(openExist==1 && nonOpenExist==1)
	{
		alert("<?=$i_Sports_Warn_OpenGroup_Error?>");
		return false;
	}
	else 
	*/
	
	if(openExist==0 && nonOpenExist==0)
	{
		alert("<?=$i_Sports_Warn_Select_Group?>");
		return false;
	}
		
	// check event quota
	if(obj.eventQuotaYN[0].checked)
	{
		var this_quota = obj.eventQuota.value;
		if(!isInteger(this_quota) || this_quota<1 || Trim(this_quota)=='')
		{
			alert("<?=$Lang['eSports']['Quota_jsAlert']?>");
			return false;
		}
	}
	
	return true;
}

function check_event_type()
{

	var row_group1 = document.getElementById("row_group1");
	var row_onlineEnrol = document.getElementById("row_onlineEnrol");
	var row_Quota = document.getElementById("row_Quota");
	var row_countHouse = document.getElementById("row_countHouse");
	var row_countIndividual = document.getElementById("row_countIndividual");

	is_CLassRelay = 0;
	<?
	foreach($class_relay_ary as $k=>$d)
	{
	?>
	if(document.form1.EventID.value==<?=$d?>)
	{
		is_CLassRelay = 1;
	}
	<? } ?>
	
	if(is_CLassRelay==1)
	{
// 		alert(row_group1);
		row_group1.style.display = "none";
		row_onlineEnrol.style.display = "none";
		row_Quota.style.display = "none";
		row_countHouse.style.display = "none";
		row_countIndividual.style.display = "none";
	}
	else
	{
		row_group1.style.display = "block";
		row_onlineEnrol.style.display = "block";
		row_Quota.style.display = "block";
		row_countHouse.style.display = "block";
		row_countIndividual.style.display = "block";
	}
	
}


</SCRIPT>

<br />   
<form name="form1" method="post" action="tf_event_new_update.php" onSubmit="return checkForm(this);">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	<td align='right'><?=$linterface->GET_SYS_MSG($xmsg);?></td>
</tr>
<tr>
	<td colspan="2">
                <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr> 
                	<td><br />
				<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
				<tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_Sports_field_Event_Name?> </span></td>
					<td class="tabletext"><?=$select_events?></td>
				</tr>
                                <tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_Sports_field_Group?> </span><span class='tabletextrequire'>*</span></td>
					<td class="tabletext">
						<div id="row_group1" style="display:<?=$default_display_open_only?"none":""?>">
							<?=$select_group?>
						</div>
						<div id="row_group2" >
							<?=$select_open_group?>
						</div>
					</td>
				</tr>
        <tr valign="top" id="row_onlineEnrol" >
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_Sports_Event_Set_OnlineEnrol?> </span><span class='tabletextremark'>*</span></td>
					<td class="tabletext"><input type="radio" name="onlineEnrol" value="1" checked id="onlineEnrol1"> <label for="onlineEnrol1"><?=$i_general_yes?></label> &nbsp; <input type="radio" name="onlineEnrol" value="0" id="onlineEnrol0"> <label for="onlineEnrol0"><?=$i_general_no?></label></td>
				</tr>
				<tr valign="top" id="row_Quota" >
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_Sports_Event_Set_RestrictQuota?> </span></td>
					<td class="tabletext"><input type="radio" name="Quota" value="1" checked id="Quota1"> <label for="Quota1"><?=$i_general_yes?></label> &nbsp; <input type="radio" name="Quota" value="0" id="Quota0"> <label for="Quota0"><?=$i_general_no?></label></td>
				</tr>
                                <tr valign="top" id="row_countHouse" >
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_Sports_Event_Set_CountHouseScore?> </span></td>
					<td class="tabletext"><input type="radio" name="countHouse" value="1" checked id="countHouse1"> <label for="countHouse1"><?=$i_general_yes?></label> &nbsp; <input type="radio" name="countHouse" value="0" id="countHouse0"> <label for="countHouse0"><?=$i_general_no?></label></td>
				</tr>
                                <tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_Sports_Event_Set_CountClassScore?> </span></td>
					<td class="tabletext"><input type="radio" name="countClass" value="1" checked id="countClass1"> <label for="countClass1"><?=$i_general_yes?></label> &nbsp; <input type="radio" name="countClass" value="0" id="countClass0"> <label for="countClass0"><?=$i_general_no?></label></td>
				</tr>
                                <tr valign="top" id="row_countIndividual" >
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_Sports_Event_Set_CountIndividualScore?> </span></td>
					<td class="tabletext"><input type="radio" name="countIndividual" value="1" checked id="countIndividual1"> <label for="countIndividual1"><?=$i_general_yes?></label> &nbsp; <input type="radio" name="countIndividual" value="0" id="countIndividual0"> <label for="countIndividual0"><?=$i_general_no?></label></td>
				</tr>
				
				<tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$Lang['eSports']['EventQuota']?> </span></td>
					<td class="tabletext">
						<input type="radio" name="eventQuotaYN" value="1" id="eventQuota1" onClick='document.form1.eventQuota.disabled=false;'> <label for="eventQuota1"><?=$i_general_yes?></label> ( <?=$Lang['eSports']['Quota']?>: <input type="text" class="textboxnum2" name="eventQuota" <?=$event_quota ? "":" disabled"?> value=""> )&nbsp; 
						<input type="radio" name="eventQuotaYN" value="0" checked id="eventQuota2" onClick='document.form1.eventQuota.disabled=true;'> <label for="eventQuota2"><?=$i_general_no?></label></td>
				</tr>
				
                                <tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_Sports_Event_Set_ScoreStandard?> </span></td>
					<td class="tabletext"><?=$select_standard?></td>
				</tr>
			</table>
			</td>
                </tr>
                </table>
	</td>
</tr>
<tr>
	<td colspan="2">        
                <table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
               <tr>
                	<td align="left" class="tabletextremark">&nbsp;&nbsp;&nbsp;<?=$i_general_required_field2?></td>
		</tr>
				<tr>
                	<td align="left" class="tabletextremark">&nbsp;&nbsp;&nbsp;<?=$Lang['eSports']['NoOnlineEnrollForHouseClassRoleItemRemarks']?></td>
		</tr>
                <tr>
                	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
                <tr>
			<td align="center">
				<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "","submit2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
                                <?= $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
				<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='tf_event.php'","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
			</td>
		</tr>
                </table>                                
	</td>
</tr>
</table>                        
<br />
</form>

<?
intranet_closedb();
print $linterface->FOCUS_ON_LOAD("form1.EventID");
$linterface->LAYOUT_STOP();
?>
