<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libswimminggala.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable{$LAYOUT_SKIN}.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$linterface 	= new interface_html();
$CurrentPage	= "PageGeneralSettings";

# Get House info
$lswimminggala = new libswimminggala();
$lswimminggala->authSportsSystem();

$array_housecode = array(1,$i_House_HouseCode);
$array_houseorder = array(2,$i_Sports_Participant_Number_Generation_cond_HouseOrder);
$array_agegroup = array(3,$i_Sports_field_GradeCode);
$array_class = array(4, $i_UserClassName);
$array_classnum = array(5,$i_UserClassNumber);
$array_class_classnum = array(6,"$i_UserClassName+$i_UserClassNumber");

$array_select_type1 = array($array_housecode, $array_houseorder, $array_agegroup);
$array_select_type2 = array($array_housecode, $array_houseorder, $array_agegroup,$array_class,$array_classnum,$array_class_classnum);

$select_type1_row1 = getSelectByArray($array_select_type1,"name=cond1_1", ($lswimminggala->numberGenerationType==1?$lswimminggala->numberGenRule1:"") );
$select_type1_row2 = getSelectByArray($array_select_type1,"name=cond1_2", ($lswimminggala->numberGenerationType==1?$lswimminggala->numberGenRule2:"") );
$select_type1_auto = "<SELECT name=autonum_len>";
$select_type1_auto .= "<OPTION value=2 ".(($lswimminggala->numberGenAutoLength==2)?'SELECTED':'').">2</OPTION>\n";
$select_type1_auto .= "<OPTION value=3 ".(($lswimminggala->numberGenAutoLength==3)?'SELECTED':'').">3</OPTION>\n";
$select_type1_auto .= "</SELECT>\n";
$select_type2_row1 = getSelectByArray($array_select_type2,"name=cond2_1", ($lswimminggala->numberGenerationType==2?$lswimminggala->numberGenRule1:"") );
$select_type2_row2 = getSelectByArray($array_select_type2,"name=cond2_2", ($lswimminggala->numberGenerationType==2? $lswimminggala->numberGenRule2:"") );
$select_type2_row3 = getSelectByArray($array_select_type2,"name=cond2_3", ($lswimminggala->numberGenerationType==2?$lswimminggala->numberGenRule3:"") );

### Title ###
$TAGS_OBJ[] = array($i_Sports_menu_Settings_House,"house.php", 0);
$TAGS_OBJ[] = array($i_Sports_menu_Settings_Score,"score.php", 0);
$TAGS_OBJ[] = array($i_Sports_menu_Settings_Lane,"lane.php", 0);
$TAGS_OBJ[] = array($i_Sports_menu_Settings_LaneSet,"laneset.php", 0);
$TAGS_OBJ[] = array($i_Sports_menu_Settings_ParticipantFormat,"participant.php", 1);
$TAGS_OBJ[] = array($Lang['eSports']['Arrangement_Schedule_Setting'],"arrangement_schedule_setting.php", 0);
$MODULE_OBJ = $lswimminggala->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();
?>
<script language="JavaScript" type="text/javascript">
<!--
function toggle(x)
{

	var tb1 = document.getElementById("tb1") ;
	var tb2 = document.getElementById("tb2") ;

	if(x==1)
        {
        	tb1.style.display="block";
                tb2.style.display="none";
	}
        else
        {
        	tb1.style.display="none";
                tb2.style.display="block";
        }
                
        

}
//-->
</script>

<br />   
<form name="form1" method="post" action="participant_update.php">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td>&nbsp;</td>
	<td align='right'><?=$linterface->GET_SYS_MSG($xmsg);?></td>
</tr>
<tr>
	<td colspan="2">
                <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr> 
                	<td><br />
				<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
                                <tr valign="top">
					<td width="250px" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_Sports_Participant_Number_Type?></td>
					<td class="tabletext">
                                                <select name="numbertype" onChange="toggle(this.value);">
                                                <option value="1" <?=($lswimminggala->numberGenerationType==1?"selected":"")?>><?=$i_Sports_Participant_Number_Type?> 1</option>
                                                <option value="2" <?=($lswimminggala->numberGenerationType==2?"selected":"")?>><?=$i_Sports_Participant_Number_Type?> 2</option>
                                                </select>
                                        </td>
				</tr>
                                </table>
                                
                                <!-- Type 1 -->
                                <table align="center" width="100%" border="0" cellpadding="5" cellspacing="0" id="tb1" style="display:none;">
                                <tr valign="top">
					<td width="250px" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_Sports_Participant_Number_1stRow?></td>
					<td class="tabletext"><?=$select_type1_row1?></td>
				</tr>
                                
                                <tr valign="top">
					<td width="250px" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_Sports_Participant_Number_2ndRow?></td>
					<td class="tabletext"><?=$select_type1_row2?></td>
				</tr>
                                
                                <tr valign="top">
					<td width="250px" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_Sports_Participant_Number_AutoNumLength?></td>
					<td class="tabletext"><?=$select_type1_auto?></td>
				</tr>
                                </table>
                                
                                <!-- Type 2 -->
                                <table align="center" width="100%" border="0" cellpadding="5" cellspacing="0" id="tb2" style="display:none">
                                <tr valign="top">
					<td width="250px" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_Sports_Participant_Number_1stRow?></td>
					<td class="tabletext"><?=$select_type2_row1?></td>
				</tr>
                                
                                <tr valign="top">
					<td width="250px" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_Sports_Participant_Number_2ndRow?></td>
					<td class="tabletext"><?=$select_type2_row2?></td>
				</tr>
                                
                                <tr valign="top">
					<td width="250px" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_Sports_Participant_Number_3rdRow?></td>
					<td class="tabletext"><?=$select_type2_row3?></td>
				</tr>
                                </table>
			</td>
                </tr>
                </table>
	</td>
</tr>
<tr>
	<td colspan="2">        
                <table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
                <tr>
                	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
                <tr>
			<td align="center">
				<?= $linterface->GET_ACTION_BTN($button_save, "submit", "","submit2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
                                <?= $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
			</td>
		</tr>
                </table>                                
	</td>
</tr>
</table>                        
<br />
</form>

<script language="JavaScript" type="text/javascript">
<!--
toggle(<?=$lswimminggala->numberGenerationType?>);
//-->
</script>

<?
intranet_closedb();
print $linterface->FOCUS_ON_LOAD("form1.numbertype");
$linterface->LAYOUT_STOP();
?>