<?php
# using: yat

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libswimminggala.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

# Get House info
$lswimminggala = new libswimminggala();
$lswimminggala->authSportsSystem();
$lexport = new libexporttext();
$results = $lswimminggala->retrieveParticipantSummary_StudentDetails();

$export_ary = array();

### header
$export_ary[] = array($i_ClassName, $i_ClassNumber, $i_UserStudentName, $i_Sports_Participant_Number_Summary_NoClassNum, $i_Sports_Participant_Number_Summary_NoHouse,$i_Sports_Participant_Number_Summary_MultipleHouse,$i_Sports_Participant_Number_Summary_NoGender,$i_Sports_Participant_Number_Summary_NoDOB,$i_Sports_Participant_Number_Summary_NoAthleticNumber);

if(!empty($results))
{
	foreach($results as $thisClassName => $data)
	{
		ksort($data);
		foreach($data as $thisClassNumber => $d)
		{
			$this_data = array();
			
			foreach($d as $thisStudentName => $NoItem)
			{
				$this_data['ClassName'] = $thisClassName;
				$this_data['ClassNumber'] = $thisClassNumber;
				$this_data['StudentName'] = $thisStudentName;
				
				$this_data['NoClassNum'] = $NoItem['NoClassNum'] ? 1 : 0;
				$this_data['NoHouse'] = $NoItem['NoHouse'] ? 1 : 0;
				$this_data['MultipleHouse'] = $NoItem['MultipleHouse'] ? 1 : 0;
				$this_data['NoGender'] = $NoItem['NoGender'] ? 1 : 0;
				$this_data['NoDOB'] = $NoItem['NoDOB'] ? 1 : 0;
				$this_data['NoAthleticNum'] = $NoItem['NoAthleticNum'] ? 1 : 0;
				
				$export_ary[] = $this_data;
			}
		}
	}
}

$utf_content = "";
foreach($export_ary as $k=>$d)
{
	$utf_content .= implode("\t", $d);
	$utf_content .= "\r\n";
}

intranet_closedb();

$filename = "student_details.csv";
$lexport->EXPORT_FILE($filename, $utf_content); 

?>
