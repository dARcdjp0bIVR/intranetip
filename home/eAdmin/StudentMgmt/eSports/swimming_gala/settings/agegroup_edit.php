<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libswimminggala.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$linterface 	= new interface_html();
$CurrentPage	= "PageAnnualSettings";

$GroupID = (is_array($ID)? $ID[0]:$ID);

$lswimminggala = new libswimminggala();
$lswimminggala->authSportsSystem();

$linterface 	= new interface_html();
$CurrentPage	= "PageAnnualSettings";

$detail = $lswimminggala->retrieveAgeGroupDetail($GroupID);
list ($grade, $engName, $chiName, $gender, $gradeCode, $dobUp, $dobLow,$displayOrder) = $detail;

$sql = "SELECT COUNT(*) FROM SWIMMINGGALA_AGE_GROUP";
$temp = $lswimminggala->returnVector($sql,1);
$count=$temp[0];
$select_order = "<SELECT name='displayOrder'>\n";
for ($i=1; $i<=$count; $i++)
{
     $select_order .= "<OPTION value='$i' ".($displayOrder==$i?"SELECTED":"").">$i</OPTION>\n";
}
$select_order .= "</SELECT>\n";

$select_gender = "<SELECT name='gender'><OPTION value='M' ".($gender=='M'?"SELECTED":"").">$i_gender_male</OPTION><OPTION value='F' ".($gender=='F'?"SELECTED":"").">$i_gender_female</OPTION></SELECT>";

$names = $lswimminggala->retrieveEventTypeName();
$txt_track = $names[0];
$txt_field = $names[1];

# Get Enrol info
$sql = "	SELECT 
				EnrolMaxTrack
			FROM SWIMMINGGALA_AGE_GROUP
			WHERE AgeGroupID = $GroupID";
$EnrolInfo = $lswimminggala->returnArray($sql,3);
$MaxTrack=$EnrolInfo[0]["EnrolMaxTrack"]>0?$EnrolInfo[0]["EnrolMaxTrack"]:$lswimminggala->enrolMaxTrack;

$select_track = "<SELECT name='num_track'>\n";
for ($i=0; $i<=15; $i++)
{
     $select_track .= "<OPTION value='$i' ".($MaxTrack==$i?"SELECTED":"").">$i</OPTION>\n";
}

$select_track .= "</SELECT>\n";

### Title ###
$TAGS_OBJ[] = array($i_Sports_menu_Settings_AgeGroup,"agegroup.php", 1);
$TAGS_OBJ[] = array($i_Sports_menu_Settings_ParticipantGenerate,"participant_gen.php", 0);
$TAGS_OBJ[] = array($i_Sports_menu_Settings_Enrolment,"enrolment.php", 0);
$TAGS_OBJ[] = array($i_Sports_menu_Settings_YearEndClearing,"record_clear.php", 0);
$MODULE_OBJ = $lswimminggala->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();

$PAGE_NAVIGATION[] = array($button_edit.($intranet_session_language=="en"?" ":""). $i_Sports_menu_Settings_AgeGroup, "");

?>

<SCRIPT LANGUAGE="Javascript">
<!--
function checkForm(obj)
{
	if(!check_text(obj.grade, "<?=$i_alert_pleasefillin.$i_Sports_field_Grade?>")) return false;
        if(!check_text(obj.engName, "<?=$i_alert_pleasefillin.$i_Sports_field_GradeName ."(".$i_general_english.")"?>")) return false;
	if(!check_text(obj.chiName, "<?=$i_alert_pleasefillin.$i_Sports_field_GradeName ."(".$i_general_chinese.")"?>")) return false;
	if(!check_text(obj.gradeCode, "<?=$i_alert_pleasefillin.$i_Sports_field_GradeCode?>")) return false;

	//modified by marcus 25/6
	//fix bug that alert users if any of dob fields is empty, while any one of the field empty should be allowed 
		if(obj.dobUp.value==""&&obj.dobLow.value=="")
		{
			alert("<?=$i_alert_pleasefillin.$i_Sports_field_DOBLowLimit.$i_alert_or.$i_Sports_field_DOBUpLimit?>");
			return false;
		}
		//if(!check_text(obj.dobUp, "<?=$i_alert_pleasefillin.$i_Sports_field_DOBUpLimit?>")) return false;
		if(obj.dobUp.value!="")	
			if(!check_date(obj.dobUp, "<?=$i_invalid_date?>")) 
				return false;
        //if(!check_text(obj.dobLow, "<?=$i_alert_pleasefillin.$i_Sports_field_DOBLowLimit?>")) return false;
        if(obj.dobLow.value!="")
			if(!check_date(obj.dobLow, "<?=$i_invalid_date?>")) 
				return false;

		if(compareDate(obj.dobUp.value,obj.dobLow.value)==1)
        { 
        	alert("<?=$i_invalid_date?>");
                obj.dobUp.focus();
                return false;
	}
}
//-->
</SCRIPT>

<br />   
<form name="form1" method="post" action="agegroup_edit_update.php" onSubmit="return checkForm(this);">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	<td align='right'>&nbsp;</td>
</tr>
<tr>
	<td colspan="2">
                <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr> 
                	<td><br />
				<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
				<tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_Sports_field_Grade?> <span class='tabletextrequire'>*</span></span></td>
					<td><input name="grade" type="text" class="textboxtext" maxlength="255" value="<?=htmlspecialchars($grade)?>"/></td>
				</tr>
                                <tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?="$i_Sports_field_GradeName ($i_general_english)"?> <span class='tabletextrequire'>*</span></span></td>
					<td><input name="engName" type="text" class="textboxtext" maxlength="255" value="<?=htmlspecialchars($engName)?>"/></td>
				</tr>
                                <tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?="$i_Sports_field_GradeName ($i_general_chinese)"?> <span class='tabletextrequire'>*</span></span></td>
					<td><input name="chiName" type="text" class="textboxtext" maxlength="255" value="<?=htmlspecialchars($chiName)?>"/></td>
				</tr>
                                <tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_UserGender?> </span></td>
					<td><?=$select_gender?></td>
				</tr>
                                <tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_Sports_field_GradeCode?>  <span class='tabletextrequire'>*</span></span></td>
					<td><input name="gradeCode" type="text" class="textboxtext" maxlength="255" value="<?=$gradeCode?>" /></td>
				</tr>
                                <tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_Sports_field_DOBUpLimit?> <span class='tabletextrequire'>*</span></span></td>
					<td class="tabletext"><input name="dobUp" type="text" class="textboxnum" maxlength="255" value="<?=$dobUp?>" /> (YYYY-MM-DD) <?=$linterface->GET_CALENDAR("form1","dobUp")?></td>
				</tr>
                                <tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_Sports_field_DOBLowLimit?> <span class='tabletextrequire'>*</span></span></td>
					<td class="tabletext"><input name="dobLow" type="text" class="textboxnum" maxlength="255" value="<?=$dobLow?>" /> (YYYY-MM-DD) <?=$linterface->GET_CALENDAR("form1","dobLow")?></td>
				</tr>
                                <tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?="$i_Sports_Enrolment_MaxEvent ($txt_field)"?> <span class='tabletextrequire'>*</span></span></td>
					<td class="tabletext"><?=$select_track?></td>
				</tr>
                                <tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?="$i_general_DisplayOrder"?> <span class='tabletextrequire'>*</span></span></td>
					<td class="tabletext"><?=$select_order?></td>
				</tr>
			</table>
			</td>
                </tr>
                </table>
	</td>
</tr>
<tr>
	<td colspan="2">        
                <table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
                <tr>
                	<td align="left" class="tabletextremark">&nbsp;&nbsp;&nbsp;<?=$i_general_required_field2?></td>
		</tr>
                <tr>
                	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
                <tr>
			<td align="center">
				<?= $linterface->GET_ACTION_BTN($button_save, "submit", "","submit2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
                                <?= $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
				<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='agegroup.php'","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
			</td>
		</tr>
                </table>                                
	</td>
</tr>
</table>                        
<br />

<input type="hidden" name="GroupID" value="<?=$GroupID?>">
</form>

<?
intranet_closedb();
print $linterface->FOCUS_ON_LOAD("form1.grade");
$linterface->LAYOUT_STOP();
?>