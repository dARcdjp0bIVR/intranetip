<?php
//# using:
/********************************************************
 *  Modification Log
 *  Date: 2017-02-06 (Isaac)
 *          added Online Registration column
 *  
 *	Date: 2016-06-21 (Cara)
 * 			Add Searchbox
 ********************************************************/
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libswimminggala.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable{$LAYOUT_SKIN}.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

$keyword = standardizeFormPostValue($_GET['keyword']);

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

intranet_auth();
intranet_opendb();

$linterface 	= new interface_html();
$CurrentPage	= "PageItemSettings";

$lswimminggala = new libswimminggala();
$lswimminggala->authSportsSystem();

# TABLE SQL
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$db_field = ($intranet_session_language=="en"?"EnglishName":"ChineseName");

# get event list
$sql ="SELECT EventID,$db_field FROM SWIMMINGGALA_EVENT";
$eventList = $lswimminggala->returnArray($sql,2);


$groups = $lswimminggala->retrieveAgeGroupIDNames();
if (!isset($AgeGroupID) | $AgeGroupID == "")
{
     $AgeGroupID = -3;
}
if ($AgeGroupID == "")
{
    header("Location: tf_name.php");
    exit();
}
if(!isset($eventID)||$eventID==""){
	$eventID = -3;
}

# Add Open Groups to the selection list
$openBoy[0] = '-1';
$openBoy[1] = $i_Sports_Event_Boys_Open;
$openGirl[0] = '-2';
$openGirl[1] = $i_Sports_Event_Girls_Open;
$openMixed[0] = '-4';
$openMixed[1] = $i_Sports_Event_Mixed_Open;
$groups[] = $openBoy;
$groups[] = $openGirl;
$groups[] = $openMixed;
$all_1[0]='-3';
$all_1[1]=$i_Sports_All;
$all_2[0]='-3';
$all_2[1]=$AllGroups;
$eventList[]=$all_1;
$groups[]=$all_2;

switch($field){
	case 0 : $field=0; break;
	case 1 : $field=1; break;
	case 2 : $field=2; break;
	case 3 : $field=3; break;
	case 4 : $field=4; break;
	case 5 : $field=5; break;
	default : $field=1;break;
}
$order = $order==""?1:$order;

$groupName = "CASE a.GroupID
			   		WHEN -1 THEN '".$i_Sports_Event_Boys_Open."'
			   		WHEN -2 THEN '".$i_Sports_Event_Girls_Open."'
			   		WHEN -4 THEN '".$i_Sports_Event_Mixed_Open."'
			   		ELSE age_group.".$db_field."
			   END";

if ($keyword != '') {
	$Keyword_cond = " and ($groupName LIKE '%".$lswimminggala->Get_Safe_Sql_Like_Query($keyword)."%' or b.EventType LIKE '%".$lswimminggala->Get_Safe_Sql_Like_Query($keyword)."%' or b.$db_field LIKE '%".$lswimminggala->Get_Safe_Sql_Like_Query($keyword)."%' )" ;
}

$AgeGroup_cond="";
if($AgeGroupID!="-3")
	//$AgeGroup_cond =  " a.GroupID = $AgeGroupID";
	$AgeGroup_cond =  " and a.GroupID = '$AgeGroupID' ";

	$eventList_cond="";
	if($eventID!="-3")
		//$eventList_cond = "  b.EventID='$eventID'";
		$eventList_cond = " and b.EventID='$eventID' ";
	
$sql = "SELECT 
1,
CONCAT('<a class=tablelink href=tf_event_edit.php?AgeGroupID=".$AgeGroupID."&eventID=".$eventID."&EventGroupID=',a.EventGroupID,'>',b.$db_field,'</a>'), c.$db_field, 
			   $groupName AS group_name,
               CASE a.IsOnlineEnrol
                    WHEN 0 THEN CONCAT('<img src=".$PATH_WRT_ROOT."/images/".$LAYOUT_SKIN."/icon_delete_b.gif width=18 height=18 border=0 align=absmiddle','>')
                    WHEN 1 THEN CONCAT('<img src=".$PATH_WRT_ROOT."/images/".$LAYOUT_SKIN."/icon_tick_green.gif width=18 height=18 border=0 align=absmiddle','>')
                    END AS onlineEnrol,
			   b.DisplayOrder,
               CASE b.EventType
                    WHEN 1 THEN IF(d_track.EventGroupID IS NULL,'$i_general_no','$i_general_yes')
                    WHEN 3 THEN IF(d_relay.EventGroupID IS NULL,'$i_general_no','$i_general_yes')
                    WHEN 4 THEN IF(d_relay.EventGroupID IS NULL,'$i_general_no','$i_general_yes')
                    END AS setup,
               CONCAT('<input type=checkbox name=EventGroupID[] value=', a.EventGroupID ,'>') 
               FROM SWIMMINGGALA_EVENTGROUP as a
                    LEFT OUTER JOIN SWIMMINGGALA_EVENT as b ON a.EventID = b.EventID
                    LEFT OUTER JOIN SWIMMINGGALA_EVENT_TYPE_NAME as c ON b.EventType = c.EventTypeID
                    LEFT OUTER JOIN SWIMMINGGALA_EVENTGROUP_EXT_TRACK as d_track ON a.EventGroupID = d_track.EventGroupID
                    LEFT OUTER JOIN SWIMMINGGALA_EVENTGROUP_EXT_RELAY as d_relay ON a.EventGroupID = d_relay.EventGroupID 
                    LEFT OUTER JOIN SWIMMINGGALA_AGE_GROUP as age_group ON a.GroupID = age_group.AgeGroupID 
               Where
               		1
               		$Keyword_cond
               		$AgeGroup_cond
               		$eventList_cond
                    ";
     
//                     $AgeGroup_cond="";
//                     $eventList_cond="";
                    
// if($AgeGroupID!="-3")
//       $AgeGroup_cond =  " a.GroupID = $AgeGroupID";
// if($eventID!="-3")
// 	  $eventList_cond = "  b.EventID='$eventID'";
	  

// if($AgeGroup_cond!="" && $eventList_cond!=""){
// 	$sql.=" WHERE ".$AgeGroup_cond." AND ".$eventList_cond;
// }else if($AgeGroup_cond!="" || $eventList_cond!=""){
// 	$sql.=" WHERE ".$AgeGroup_cond.$eventList_cond;
// }

# TABLE INFO
$li = new libdbtable2007($field, $order, $pageNo);
$li->fieldorder2=", b.DisplayOrder ASC";
$li->field_array = array("b.$db_field", "c.EventTypeID","group_name", "onlineEnrol", "b.DisplayOrder","setup");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;
$li->IsColOff = "SmartCardReminder";

// TABLE COLUMN
$pos = 0;
$li->column_list .= "<td width=1 class='tabletop tabletoplink'>#</td>\n";
$li->column_list .= "<td width=20% class='tabletop tabletoplink'>".$li->column($pos++, $i_Sports_field_Event_Name)."</td>\n";
$li->column_list .= "<td width=15% class='tabletop tabletoplink'>".$li->column($pos++, $i_Sports_field_Event_Type)."</td>\n";
$li->column_list .= "<td width=20% class='tabletop tabletoplink'>".$li->column($pos++, $i_Sports_field_GradeName)."</td>\n";
$li->column_list .= "<td width=15% class='tabletop tabletoplink'>".$li->column($pos++, $Lang['StudentRegistry']['Settings']['OnlineReg'])."</td>\n";
$li->column_list .= "<td width=15% class='tabletop tabletoplink'>".$li->column($pos++, $i_general_DisplayOrder)."</td>\n";
$li->column_list .= "<td width=15% class='tabletop tabletoplink'>".$li->column($pos++, $i_Sports_already_set)."</td>\n";
$li->column_list .= "<td width=1 class='tabletop tabletoplink'>".$li->check("EventGroupID[]")."</td>\n";

$select_event = $i_Sports_Item." : ".getSelectByArray($eventList,"name=eventID onChange=this.form.submit()",$eventID,0,1);
$select_group = $i_Sports_field_Group." : ".getSelectByArray($groups,"name=AgeGroupID onChange=this.form.submit()",$AgeGroupID,0,1);

### Button
$AddBtn 	= "<a href=\"javascript:checkNew('tf_event_new.php?eventID=$eventID')\" class='contenttool'><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_new.gif\" width=\"18\" height=\"18\" border=\"0\" align=\"absmiddle\" /> " . $button_new . "</a>";
$delBtn 	= "<a href=\"javascript:checkRemove(document.form1,'EventGroupID[]','tf_event_remove.php')\" class=\"tabletool\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_delete.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\" /> " . $button_remove . "</a>";
$editBtn 	= "<a href=\"javascript:checkEdit(document.form1,'EventGroupID[]','tf_event_edit.php')\" class=\"tabletool\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_edit.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\" /> " . $button_edit . "</a>";

### Title ###
$TAGS_OBJ[] = array($i_Sports_menu_Settings_TrackFieldName,"tf_name.php", 0);
$TAGS_OBJ[] = array($i_Sports_menu_Settings_TrackFieldEvent,"tf_event.php", 1);
$MODULE_OBJ = $lswimminggala->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();
$htmlAry['searchBox'] = $linterface->Get_Search_Box_Div('keyword', $keyword);
?>

<br />
<form name="form1" method="get">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td align="center">
        	<table width="96%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td align="left" class="tabletext">
                              	<table width="100%" border="0" cellspacing="0" cellpadding="0">
                              		<tr> <td colspan="2" align="right" valign="bottom"><?=$linterface->GET_SYS_MSG($xmsg, $xmsg2);?></td></tr>
					 			
									<tr>
										<td colspan="2">
                                        	<table border="0" cellspacing="0" cellpadding="2" style="width:100%">
                                            	<tr>
                                              		<td><p><?=$AddBtn?></p></td>
                                              		<td align="right"><?=$htmlAry['searchBox']?></td>
                                            	</tr>
                                          	</table>
					</td>
				</tr>

				<tr class="table-action-bar">
                                	<td height="28" class="tabletext"><?=$select_event?> <?=$select_group?></td>
					<td align="right" valign="bottom" height="28">
						<table border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td width="21"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/table_tool_01.gif" width="21" height="23" /></td>
							<td background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/table_tool_02.gif">
								<table border="0" cellspacing="0" cellpadding="2">
								<tr>
									<td nowrap><?=$delBtn?></td>
                                                                        <td><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="5"></td>
                                                                        <td nowrap><?=$editBtn?></td>
								</tr>
        							</table>
							</td>
        						<td width="6"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/table_tool_03.gif" width="6" height="23" /></td>
						</tr>
						</table>
					</td>
				</tr>
                                <tr>
                                	<td colspan="2">
						<?php
							echo $li->display($li->no_col, 0, 1);
						?>
					</td>
				</tr>
				</table>
			</td>
		</tr>
                </table>
        </td>
</tr>
</table>
<br />

<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>">
<input type="hidden" name="order" value="<?php echo $li->order; ?>">
<input type="hidden" name="field" value="<?php echo $li->field; ?>">
<input type="hidden" name="page_size_change" value="">
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>">
</form>

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>
