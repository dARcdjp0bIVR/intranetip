<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libswimminggala.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$lswimminggala = new libswimminggala();
$lswimminggala->authSportsSystem();

# Get House info
$HouseID = $_POST['HouseID'];
$engName = $_POST['engName'];
$chiName = $_POST['chiName'];
$houseOrder = $_POST['houseOrder'];
$houseGroup = $_POST['houseGroup'];
$colorCode = $_POST['colorCode'];
$houseCode = $_POST['houseCode'];

$sql = "INSERT INTO INTRANET_HOUSE (EnglishName, ChineseName, DisplayOrder, GroupID,
                    ColorCode, HouseCode) VALUES
                    ('$engName', '$chiName','$houseOrder','$houseGroup','$colorCode',
                     '$houseCode')";
$lswimminggala->db_db_query($sql);

if($lswimminggala->db_affected_rows()<=0)
{
	$xmsg = "";
        $xmsg2 = $i_con_msg_add_failed.$i_con_msg_sports_house_group_existed;
	header("Location: house_new.php?xmsg=$xmsg&xmsg2=$xmsg2&engName=$engName&chiName=$chiName&houseOrder=$houseOrder&houseGroup=$houseGroup&colorCode=$colorCode&houseCode=$houseCode");
}
else
{
	intranet_closedb();
	header("Location: house.php?xmsg=add");
}
?>