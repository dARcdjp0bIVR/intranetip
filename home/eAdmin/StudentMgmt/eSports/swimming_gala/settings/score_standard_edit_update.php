<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libswimminggala.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$lswimminggala = new libswimminggala();
$lswimminggala->authSportsSystem();

# Get House info
$StandardID = $_POST['StandardID'];
$scoreName = $_POST['scoreName'];
$rank1 = $_POST['rank1'];
$rank2 = $_POST['rank2'];
$rank3 = $_POST['rank3'];
$rank4 = $_POST['rank4'];
$rank5 = $_POST['rank5'];
$rank6 = $_POST['rank6'];
$rank7 = $_POST['rank7'];
$rank8 = $_POST['rank8'];
$recordBroken = $_POST['recordBroken'];
$qualify = $_POST['qualify'];

$sql = "UPDATE SWIMMINGGALA_SCORE_STANDARD
               SET Name = '$scoreName',
                   Position1 = '$rank1',
                   Position2 = '$rank2',
                   Position3 = '$rank3',
                   Position4 = '$rank4',
                   Position5 = '$rank5',
                   Position6 = '$rank6',
                   Position7 = '$rank7',
                   Position8 = '$rank8',
                   RecordBroken = '$recordBroken',
                   Qualified = '$qualify'
                   WHERE StandardID = $StandardID
                   ";

$lswimminggala->db_db_query($sql);

intranet_closedb();
header("Location: score.php?xmsg=update");
?>