<?php
#####################################################
#	Date:	2013-08-06	YatWoon
#			fixed: $this_UserType replace $UserType
#####################################################
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

if(!$plugin['swimming_gala'] || !$_SESSION["SSV_USER_ACCESS"]["eAdmin-eSportsAdmin"])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

include_once($PATH_WRT_ROOT."includes/libswimminggala.php");

intranet_auth();
intranet_opendb();

$lswimminggala = new libswimminggala();

$this_UserType = $AdminLevel==0?"Helper":"Admin";

if(isset($target))
{
	for ($i=0; $i<sizeof($target); $i++)
	{
		$targetType = substr($target[$i],0,1);
		$user = substr($target[$i],1);
		if($targetType=="U")
		{
			$lswimminggala->addAdminUser(array($user), $this_UserType);
		}
		else
		{
			include_once($PATH_WRT_ROOT."includes/libgroup.php");
			$lg = new libgroup($user);
			$GroupMember = $lg->returnGroupUser();
			
			foreach($GroupMember as $k=>$d)
			{
				$lswimminggala->addAdminUser(array($d['UserID']), $this_UserType);
			}
			
		}
	}
}
intranet_closedb();
header("Location: index.php?msg=add");
?>