<?php
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");


if(!$plugin['swimming_gala'] || !$_SESSION["SSV_USER_ACCESS"]["eAdmin-eSportsAdmin"])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

include_once($PATH_WRT_ROOT."includes/libswimminggala.php");

intranet_auth();
intranet_opendb();

$lswimminggala = new libswimminggala(); 
$lswimminggala->updateAdminUser($AdminID, $AdminLevel);

intranet_closedb();
header("Location: index.php?msg=update");
?>