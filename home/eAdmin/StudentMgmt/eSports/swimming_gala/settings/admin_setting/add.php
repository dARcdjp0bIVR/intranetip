<?php
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

if(!$plugin['Sports'] || !$_SESSION["SSV_USER_ACCESS"]["eAdmin-eSportsAdmin"])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libswimminggala.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$CurrentPage = "PageAdminSettings";
$lsports = new libswimminggala();

$PAGE_NAVIGATION[] = array($Lang['Btn']['Add']);

# Left menu 
$TAGS_OBJ[] = array($Lang['Sports']['AdminSettings']);
$MODULE_OBJ = $lsports->GET_MODULE_OBJ_ARR();

# Start layout
$linterface->LAYOUT_START();

# 
//for($i = 0; $i < 40; $i++) $space .= "&nbsp;";
$AdminHelperIDArr = $lsports->Get_Admin_Helper_List();
foreach($AdminHelperIDArr as $AdminHelperID)
	$param .= "&ExcludedUserID[]=$AdminHelperID";

$StaffSelection .= "<td width=\"1\"><select id=target name=target[] size=4 multiple></select></td><td>";
$StaffSelection .= $linterface->GET_BTN($Lang['Btn']['Select'], "button","SelectUser();")."<br>";
$StaffSelection .= $linterface->GET_BTN($Lang['Btn']['RemoveSelected'], "button","removeSelected();");
$StaffSelection .= "</td>";
?>

<script language="javascript">
<!--
function checkform()
{
	obj = document.form1;
	
	if(obj.elements["target[]"].length==0)
	{
		alert("<?php echo $Lang['Circular']['PleaseSelectStaff']; ?>"); 
		return false; 
	}
	checkOptionAll(obj.elements["target[]"]);
}

function SelectUser()
{
	newWindow('../../../choose/index.php?fieldname=target[]<?=$param?>',9);
}

function removeSelected()
{
	checkOptionRemove(document.form1.elements['target[]'])
}
//-->
</script>

<br />   
<form name="form1" method="get" action="add_update.php" onSubmit="return checkform();">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	<td align='right'><?=$linterface->GET_SYS_MSG($xmsg);?></td>
</tr>

<tr>
	<td colspan="2">
		<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr valign='top'>
			<td width='30%' valign="top" class="formfieldtitle" nowrap><?=$Lang['Sports']['HelperName']?></td>
			<?=$StaffSelection?>
		</tr>
		</table>
	</td>
</tr>

<tr>
	<td colspan="2">
		<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr valign='top'>
			<td width='30%' valign="top" class="formfieldtitle" nowrap><?=$Lang['Sports']['AdminType']?></td>
			<td class='tabletext'>
			<input type="radio" name="AdminLevel" value="0" id="AdminLevel0" checked> <label for="AdminLevel0"><?=$Lang['Sports']['Helper']?></label> <br>
			<input type="radio" name="AdminLevel" value="1" id="AdminLevel1"> <label for="AdminLevel1"><?=$Lang['Sports']['Admin']?></label>
			</td>
		</tr>
		</table>
	</td>
</tr>


<tr>
	<td colspan="2">        
        <table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
        <tr>
        	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
        </tr>
        <tr>
			<td align="center">
				<?= $linterface->GET_ACTION_BTN($button_submit, "submit") ?>
				<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "history.back()","cancelbtn") ?>
			</td>
		</tr>
        </table>                                
	</td>
</tr>

</table>                        
<br />

</form>


<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>