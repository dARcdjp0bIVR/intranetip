<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libswimminggala.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable{$LAYOUT_SKIN}.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$linterface 	= new interface_html();
$CurrentPage	= "PageAnnualSettings";

$lswimminggala = new libswimminggala();
$lswimminggala->authSportsSystem();
//$results = $lswimminggala->retrieveAgeGroupInfo();

$sql = "
SELECT 
	AgeGroupID, 
	CONCAT('<a class=\"tablelink\" href=\"agegroup_edit.php?ID=', AgeGroupID, '\">', GradeChar, '</a>'),
	EnglishName, 
	ChineseName, 
	Gender, 
	GroupCode,
	DOBUpLimit, 
	DOBLowLimit, 
	DisplayOrder,
	CONCAT('<input type=\"checkbox\" name=\"ID[]\" value=\"', AgeGroupID ,'\">')
FROM 
	SWIMMINGGALA_AGE_GROUP
";

# TABLE INFO
if($field=="") $field = 0;
$li = new libdbtable2007($field, 1, $pageNo);
$li->field_array = array("DisplayOrder");
$li->sql = $sql;
$li->no_col = 10;
$li->IsColOff = "eSportSettings";

// TABLE COLUMN
$pos = 0;
$li->column_list .= "<td class='tabletoplink'>$i_Sports_field_Grade</td>\n";
$li->column_list .= "<td class='tabletoplink'>$i_Sports_field_GradeName ($i_general_english)</td>\n";
$li->column_list .= "<td class='tabletoplink'>$i_Sports_field_GradeName ($i_general_chinese)</td>\n";
$li->column_list .= "<td class='tabletoplink'>$i_UserGender</td>\n";
$li->column_list .= "<td class='tabletoplink'>$i_Sports_field_GradeCode</td>\n";
$li->column_list .= "<td class='tabletoplink'>$i_Sports_field_DOBUpLimit</td>\n";
$li->column_list .= "<td class='tabletoplink'>$i_Sports_field_DOBLowLimit</td>\n";
$li->column_list .= "<td class='tabletoplink'>$i_general_DisplayOrder</td>\n";
$li->column_list .= "<td width=1 class='tabletoplink'>".$li->check("ID[]")."</td>\n";

### Button
$AddBtn 	= "<a href=\"javascript:checkNew('agegroup_new.php')\" class='contenttool'><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_new.gif\" width=\"18\" height=\"18\" border=\"0\" align=\"absmiddle\" /> " . $button_new . "</a>";
$delBtn 	= "<a href=\"javascript:checkRemove(document.form1,'ID[]','agegroup_remove.php')\" class=\"tabletool\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_delete.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\" /> " . $button_remove . "</a>";
$editBtn 	= "<a href=\"javascript:checkEdit(document.form1,'ID[]','agegroup_edit.php')\" class=\"tabletool\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_edit.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\" /> " . $button_edit . "</a>";

### Title ###
$TAGS_OBJ[] = array($i_Sports_menu_Settings_AgeGroup,"agegroup.php", 1);
$TAGS_OBJ[] = array($i_Sports_menu_Settings_ParticipantGenerate,"participant_gen.php", 0);
$TAGS_OBJ[] = array($i_Sports_menu_Settings_Enrolment,"enrolment.php", 0);
$TAGS_OBJ[] = array($i_Sports_menu_Settings_YearEndClearing,"record_clear.php", 0);
$MODULE_OBJ = $lswimminggala->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();
?>

<br />
<form name="form1" method="get">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td align="center">
        	<table width="96%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td align="left" class="tabletext">
                              	<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td>
                                        	<table border="0" cellspacing="0" cellpadding="2">
                                            	<tr>
                                              		<td><p><?=$AddBtn?></p></td>
                                            	</tr>
                                          	</table>
					</td>
                                        <td align="right" valign="bottom"><?=$linterface->GET_SYS_MSG($xmsg, $xmsg2);?></td>
				</tr>

				<tr class="table-action-bar">
					<td align="right" valign="bottom" colspan="2" height="28">
						<table border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td width="21"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/table_tool_01.gif" width="21" height="23" /></td>
							<td background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/table_tool_02.gif">
								<table border="0" cellspacing="0" cellpadding="2">
								<tr>
									<td nowrap><?=$delBtn?></td>
                                                                        <td><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="5"></td>
                                                                        <td nowrap><?=$editBtn?></td>
								</tr>
        							</table>
							</td>
        						<td width="6"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/table_tool_03.gif" width="6" height="23" /></td>
						</tr>
						</table>
					</td>
				</tr>
                                <tr>
                                	<td colspan="2">
						<?php
							echo $li->display($li->no_col);
						?>
					</td>
				</tr>
				</table>
			</td>
		</tr>
                </table>
        </td>
</tr>
</table>
<br />

</form>
 
<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>