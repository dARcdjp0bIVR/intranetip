<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libswimminggala.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$lswimminggala = new libswimminggala();
$lswimminggala->authSportsSystem();

# Get House info
$scoreEnrol = $_POST['scoreEnrol'];
$scorePresent = $_POST['scorePresent'];
$scoreAbsent = $_POST['scoreAbsent'];

$lswimminggala->scoreEnrol = $scoreEnrol;
$lswimminggala->scorePresent = $scorePresent;
$lswimminggala->scoreAbsent = $scoreAbsent;
$lswimminggala->saveSettings();

intranet_closedb();
header("Location: score.php?xmsg3=update");
?>