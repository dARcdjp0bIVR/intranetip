<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libswimminggala.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$lsports = new libswimminggala();
$lsports->authSportsSystem();

$hidden_auto_arrange_buttom = $_POST['hidden_auto_arrange_buttom'];

$lsports->HiddenAutoArrangeButton = $hidden_auto_arrange_buttom;
$lsports->saveSettings();

intranet_closedb();
header("Location: arrangement_schedule_setting.php?xmsg=update");
?>