<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libswimminggala.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$linterface 	= new interface_html();
$CurrentPage	= "PageItemSettings";

$lswimminggala = new libswimminggala();
$lswimminggala->authSportsSystem();

$EventID = (is_array($EventID)?$EventID[0]:$EventID);
$detail = $lswimminggala->retrieveTrackFieldDetail($EventID);

list($r_type, $r_eng, $r_chi, $r_order, $r_is_jump) = $detail;

$db_field = ($intranet_session_language=="en"?"EnglishName":"ChineseName");
$sql = "SELECT EventTypeID, $db_field FROM SWIMMINGGALA_EVENT_TYPE_NAME ORDER BY EventTypeID";
$event_types = $lswimminggala->returnArray($sql,2);
$select_event_type = getSelectByArray($event_types, "name='EventType' onChange='changeEventType(this.value);'",$r_type,0,1);

$sql = "SELECT EventType, MAX(DisplayOrder) FROM SWIMMINGGALA_EVENT GROUP BY EventType";
$event_order = $lswimminggala->returnArray($sql,2);
$array_order = build_assoc_array($event_order);

$pos = 1;
$array_order[$pos++] += 1;
$array_order[$pos++] += 1;
$array_order[$pos++] += 1;
$array_order[$pos++] += 1;

$default_type = 1;

$select_order = "<SELECT name='DisplayOrder'>\n";
for ($i=1; $i<=$array_order[$default_type]; $i++)
{
     $select_order .= "<OPTION value='$i' ".($i==$r_order?"SELECTED":"").">$i</OPTION>\n";
}
$select_order .= "</SELECT>\n";

$checked = ($r_is_jump==1) ? "CHECKED" : "";


### Title ###
$TAGS_OBJ[] = array($i_Sports_menu_Settings_TrackFieldName,"tf_name.php", 1);
$TAGS_OBJ[] = array($i_Sports_menu_Settings_TrackFieldEvent,"tf_event.php", 0);
$MODULE_OBJ = $lswimminggala->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();

$PAGE_NAVIGATION[] = array($button_edit.($intranet_session_language=="en"?" ":""). $i_Sports_menu_Settings_TrackFieldName, "");


$pos = 1;
?>
<SCRIPT LANGUAGE=Javascript>

var type_max = new Array();
var pos = 1;
type_max[pos++] = <?=$array_order[$pos++]?>;
type_max[pos++] = <?=$array_order[$pos++]?>;
type_max[pos++] = <?=$array_order[$pos++]?>;
type_max[pos++] = <?=$array_order[$pos++]?>;

function changeEventType(type_value)
{
         var order_select = document.form1.elements["DisplayOrder"];
         while (order_select.options.length > 0)
         {
                order_select.options[0] = null;
         }
         var i;
         for (i=1; i<=type_max[type_value]; i++)
         {
              order_select.options[i-1] = new Option(i,i);
         }
         order_select.selectedIndex = type_max[type_value]-1;

}

function checkForm(obj)
{
	if(!check_text(obj.engName, "<?=$i_Sports_Warn_Please_Fill_EventName?> <?="($i_general_english)"?>")) return false;
        if(!check_text(obj.chiName, "<?=$i_Sports_Warn_Please_Fill_EventName?> <?="($i_general_chinese)"?>")) return false;
        
        obj.action = "tf_name_edit_update.php";
	return true;
}

</SCRIPT>

<br />   
<form name="form1" method="post" onSubmit="return checkForm(this);">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	<td align='right'><?=$linterface->GET_SYS_MSG($xmsg);?></td>
</tr>
<tr>
	<td colspan="2">
                <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr> 
                	<td><br />
				<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
				<tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?="$i_Sports_field_Event_Name ($i_general_english)"?> <span class='tabletextrequire'>*</span></span></td>
					<td><input name="engName" type="text" class="textboxtext" maxlength="255" value="<?=$r_eng?>" /></td>
				</tr>
                                <tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?="$i_Sports_field_Event_Name ($i_general_chinese)"?> <span class='tabletextrequire'>*</span></span></td>
					<td><input name="chiName" type="text" class="textboxtext" maxlength="255" value="<?=$r_chi?>" /></td>
				</tr>
                                <tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_Sports_field_Event_Type?> </span></td>
					<td><?=$select_event_type?> </td>
				</tr>
                                <tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_general_DisplayOrder?> </span></td>
					<td><?=$select_order?></td>
				</tr>
			</table>
			</td>
                </tr>
                </table>
	</td>
</tr>
<tr>
	<td colspan="2">        
                <table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
                <tr>
                	<td align="left" class="tabletextremark">&nbsp;&nbsp;&nbsp;<?=$i_general_required_field2?></td>
		</tr>
                <tr>
                	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
                <tr>
			<td align="center">
				<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "","submit2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
                                <?= $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
                                <?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='tf_name.php'","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
			</td>
		</tr>
                </table>                                
	</td>
</tr>
</table>                        
<br />

<input type="hidden" name="EventID" value="<?=$EventID?>">
</form>

<?
intranet_closedb();
print $linterface->FOCUS_ON_LOAD("form1.engName");
$linterface->LAYOUT_STOP();
?>