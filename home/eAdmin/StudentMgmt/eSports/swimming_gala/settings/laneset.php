<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libswimminggala.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable{$LAYOUT_SKIN}.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$linterface 	= new interface_html();
$CurrentPage	= "PageGeneralSettings";

$lsports = new libswimminggala();
$lsports->authSportsSystem();
$lanes = $lsports->numberOfLanes;

$event_select = $lsports->Get_Event_Selection("eventID",$eventID,"changeEvent()",0,0,$i_Discipline_System_default);

# Return Lane Rank
$RoundLaneArrangement = $lsports->Get_Event_Special_Lane_Arrangement($eventID);
list($final_rank, $second_rank) = $RoundLaneArrangement;

# final round setting table 
$SettingTable = $lsports->Get_Lane_Arrangement_Setting_Table($final_rank,"fr");

# second rond settingtable

if(!empty($second_rank))
{
	$NoOfHeat = count($second_rank)/$lanes;
	$SecSettingTable = $lsports->Get_Lane_Arrangement_Setting_Table($second_rank,"sr");
	$isChecked = true;
}
else
{
	$NoOfHeat = 2;
	$SecSettingTable = $lsports->Get_Lane_Arrangement_Setting_Table($second_rank,"sr",$NoOfHeat);
}

$SpecialArrangementCheckBox = $linterface->Get_Checkbox($ID="SpecialArrange", $Name="SpecialArrange", $Value=1, $isChecked, $Class='', $Lang['eSports']['SpecialArrangeForSecondRound'] , $Onclick='ShowHideTable()');

$count2 = 2;
for($i=0; $i<10; $i++)
{
	$groups2[$i] = $count2;
	$count2++;
}
$select_group = getSelectByValue($groups2," id='groupNum' name='groupNum' onchange='aj_reload_SecSettingTable()' ",$NoOfHeat,0,1);

//# TABLE INFO
//if($field=="") $field = 0;
//$li = new libdbtable2007($field, $order, $pageNo);
//$li->no_col = 2;
//$li->title = "";
//$li->sql = $sql;
//$li->IsColOff = "eSportLaneSetSettings";
//
//// TABLE COLUMN
//$pos = 0;
//$li->column_list .= "<td width='40' class='tablegreentop tabletopnolink' nowrap>$i_Sports_field_Lane</td>\n";
//$li->column_list .= "<td class='tablegreentop tabletoplink'>$i_Sports_field_Rank</td>\n";

//$special_rank = ($temp[0]==""||$temp[0]==NULL)?$lsports->defaultLaneArrangement:$temp[0];

### Title ###
$TAGS_OBJ[] = array($i_Sports_menu_Settings_House,"house.php", 0);
$TAGS_OBJ[] = array($i_Sports_menu_Settings_Score,"score.php", 0);
$TAGS_OBJ[] = array($i_Sports_menu_Settings_Lane,"lane.php", 0);
$TAGS_OBJ[] = array($i_Sports_menu_Settings_LaneSet,"laneset.php", 1);
$TAGS_OBJ[] = array($i_Sports_menu_Settings_ParticipantFormat,"participant.php", 0);
$TAGS_OBJ[] = array($Lang['eSports']['Arrangement_Schedule_Setting'],"arrangement_schedule_setting.php", 0);
$MODULE_OBJ = $lsports->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();
?>
<SCRIPT LANGUAGE=Javascript>
function changeEvent()
{
	document.form1.action = "laneset.php";
	document.form1.submit();
}

function checkform(obj)
{
	var exist_rank = new Array();
	var form_valid = true;
	$(".frSelection").each(function(){
		var val = $(this).val();
		if(exist_rank[val] == 1)
		{
			form_valid = false;
			return false;
		}
		else
		{
			exist_rank[val]=1;
		}
	})
	
	if(!form_valid)
	{
		alert("<?=$i_Sports_Record_Alert_Duplicate_RankNumber?>");
		return false;
	}
	
	var exist_rank = new Array();
	$(".srSelection").each(function(){
		var val = $(this).val();
		if(exist_rank[val] == 1)
		{
			form_valid = false;
			return false;
		}
		else
		{
			exist_rank[val]=1;
		}
	})
	
	if(!form_valid)
	{
		alert("<?=$i_Sports_Record_Alert_Duplicate_RankNumber?>");
		return false;
	}
}

function ShowHideTable()
{
	$("input#SpecialArrange").attr("checked")?$("span#SecondRoundSettingSpan").show():$("span#SecondRoundSettingSpan").hide();
}

function aj_reload_SecSettingTable()
{
	Block_Document();
	var groupNum = $("#groupNum").val();
	$.post(
		"ajax_reload_arrangement_table.php",
		{
			groupNum:groupNum
		},
		function(ReturnData)
		{
			$("span#SecondRoundTable").html(ReturnData);
			UnBlock_Document();
		}
	)
}

$().ready(function(){
	ShowHideTable();
});
</SCRIPT>

<br />
<form name="form1" action="laneset_update.php" method="post" onSubmit="return checkform(this);">

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION1) ?></td>
	</tr>
	<tr>
		<td align="center">
			<table width="96%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td align="left" class="tabletext">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td height="28"><?=$event_select?></td>
								<td align="right"><?=$linterface->GET_SYS_MSG($xmsg, $xmsg2);?></td>
							</tr>
							<tr>
								<td colspan="2"><?=$SettingTable?><br></td>
							</tr>
							<tr>
								<td colspan="2"><?=$SpecialArrangementCheckBox?></td>
							</tr>
							<tr>
								<td colspan="2">
									<span id="SecondRoundSettingSpan">
										<table class="form_table_v30">
											<tbody>
											<tr>
												<td class="field_title"><?=$i_Sports_Num_Of_Group?></td>
												<td><?=$select_group?></td>
											</tr>
											</tbody>
										</table>
										<span id="SecondRoundTable">
											<?=$SecSettingTable?>
										</span>
									</span>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>        
			<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
				<tr>
					<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
				</tr>
				<tr>
					<td align="center">
						<?= $linterface->GET_ACTION_BTN($button_save, "submit", "","submit2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
					</td>
				</tr>
			</table>                                
		</td>
	</tr>
</table>
<br />

</form>

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>