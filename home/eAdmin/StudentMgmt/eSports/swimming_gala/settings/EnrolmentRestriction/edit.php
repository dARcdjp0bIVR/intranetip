<?php
# using: 

/********************************************************
 *  Modification Log
 * 
 * 	Date: 	2017-09-14	Bill	[2017-0906-1708-05236]
 * 			- Copy form Sport Days > Support Enrolment Restriction
 * 
 ********************************************************/

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

if(!$plugin['swimming_gala'] || !$_SESSION["SSV_USER_ACCESS"]["eAdmin-eSportsAdmin"])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

include_once($PATH_WRT_ROOT."includes/libswimminggala.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

$lswimminggala = new libswimminggala();

$linterface = new interface_html();
$CurrentPage = "PageAdminSettings";
$PAGE_NAVIGATION[] = array($Lang['Btn']['Edit']);

# Left Menu
$TAGS_OBJ[] = array($Lang['eSports']['EnrolmentRestriction']);
$MODULE_OBJ = $lswimminggala->GET_MODULE_OBJ_ARR();

# Start layout
$linterface->LAYOUT_START();

# Get Student Reason
$name_field = getNameFieldByLang("b.");
$sid = $StudentID[0];
$sql = "SELECT 
			$name_field as NameField, 
			Reason
		FROM 
			SWIMMINGGALA_ENROL_RESTRICTION_LIST as a
			LEFT JOIN INTRANET_USER as b ON (a.UserID = b.UserID)
		WHERE 
			a.UserID = $sid ";
$tmp = $lswimminggala->returnArray($sql);
list($NameField, $this_reason) = $tmp[0];
?>

<br />
<form name="form1" method="get" action="edit_update.php" onSubmit="return checkform();">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
</tr>

<tr>
	<td colspan="2">
		<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr valign='top'>
			<td width='30%' valign="top" class="formfieldtitle" nowrap><?=$i_UserStudentName?></td>
			<td class='tabletext'><?=$NameField?></td>
		</tr>
		</table>
	</td>
</tr>

<tr>
	<td colspan="2">
		<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr valign='top'>
			<td width='30%' valign="top" class="formfieldtitle" nowrap><?=$i_Discipline_Reason?></td>
			<td class='tabletext'>
				<input type="text" name="reason" size="50" maxlength="100" value="<?=$this_reason?>">
			</td>
		</tr>
		</table>
	</td>
</tr>

<tr>
	<td colspan="2">        
        <table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
        <tr>
        	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
        </tr>
        <tr>
			<td align="center">
				<?= $linterface->GET_ACTION_BTN($button_submit, "submit") ?>
				<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "history.back()", "cancelbtn") ?>
			</td>
		</tr>
        </table>
	</td>
</tr>

</table>
<br />

<input type="hidden" name="sid" value="<?=$sid?>">
</form>

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>