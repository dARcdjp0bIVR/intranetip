<?php
# using: 

/********************************************************
 *  Modification Log
 * 
 * 	Date: 	2017-09-14	Bill	[2017-0906-1708-05236]
 * 			- Copy form Sport Days > Support Enrolment Restriction
 * 
 ********************************************************/
		
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

if(!$plugin['swimming_gala'] || !$_SESSION["SSV_USER_ACCESS"]["eAdmin-eSportsAdmin"])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

### Cookies handling
# set cookies
$arrCookies = array();
$arrCookies[] = array("ck_settings_admin_setting_page_size", "numPerPage");
$arrCookies[] = array("ck_settings_admin_setting_page_number", "pageNo");
$arrCookies[] = array("ck_settings_admin_setting_page_order", "order");
$arrCookies[] = array("ck_settings_admin_setting_page_field", "field");

if(isset($clearCoo) && $clearCoo == 1) {
	clearCookies($arrCookies);
	$ck_settings_admin_setting_page_size = "";
}
else {
	updateGetCookies($arrCookies);
}

include_once($PATH_WRT_ROOT."includes/libswimminggala.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

$lswimminggala = new libswimminggala();

$name_field = getNameFieldByLang("u.");
$keyword = standardizeFormGetValue($_GET["keyword"]);
if ($keyword != "") {
	$Keyword_cond = " AND ($name_field LIKE '%".$lswimminggala->Get_Safe_Sql_Like_Query($keyword)."%' OR r.Reason LIKE '%".$lswimminggala->Get_Safe_Sql_Like_Query(intranet_htmlspecialchars($keyword))."%' OR u.ClassNumber LIKE '%".$lswimminggala->Get_Safe_Sql_Like_Query($keyword)."%' OR u.ClassName LIKE '%".$lswimminggala->Get_Safe_Sql_Like_Query($keyword)."%' )" ;
}

$sql = "SELECT 
				u.ClassName,
				u.ClassNumber,
				$name_field as student_name,
				r.Reason,
				r.DateInput,
				CONCAT('<input type=\"checkbox\" name=\"StudentID[]\" value=\"', r.UserID,'\">')
		FROM 
				SWIMMINGGALA_ENROL_RESTRICTION_LIST as r
		LEFT JOIN 
				INTRANET_USER as u ON (u.UserID = r.UserID)
		WHERE
				1
				$Keyword_cond ";

$linterface = new interface_html();
$CurrentPage = "PageEnrolmentRestriction";

### Table
// Table Info
if($field=="") $field = 0;
if($order=="") $order = 1;

$li = new libdbtable2007($field, $order, $pageNo);
$li->page_size = $numPerPage? $numPerPage : 20;
$li->field_array = array("ClassName", "ClassNumber", "student_name", "Reason", "DateInput");
$li->fieldorder2 = ", ClassNumber asc";
$li->sql = $sql;
$li->no_col = 7;
$li->IsColOff = "IP25_table";

// Table Column
$li->column_list .= "<th class='tabletop' width='1'>#</th>\n";
$li->column_list .= "<th class='tabletop' width='5%'>". $li->column_IP25(0,$i_ClassName) ."</th>\n";
$li->column_list .= "<th class='tabletop' width='5%'>". $i_ClassNumber ."</th>\n";
$li->column_list .= "<th class='tabletop' width='20%'>". $li->column_IP25(2,$i_UserStudentName) ."</th>\n";
$li->column_list .= "<th class='tabletop' width='50%'>". $li->column_IP25(3,$i_Discipline_Reason) ."</th>\n";
$li->column_list .= "<th class='tabletop' width='20%'>". $li->column_IP25(4,$Lang['General']['RecordDate']) ."</th>\n";
$li->column_list .= "<th class='tabletop' width='1'>".$li->check("StudentID[]")."</th>\n";

### Button
$AddBtn = $linterface->GET_LNK_ADD("add.php", $button_new, "", "", "", 0);
$delBtn = $linterface->GET_LNK_REMOVE("javascript:checkRemove(document.form1,'StudentID[]','remove.php')");
$editBtn = $linterface->GET_LNK_EDIT("javascript:checkEdit(document.form1,'StudentID[]','edit.php')", "", "", "", "", 0);

### Title
$TAGS_OBJ[] = array($Lang['eSports']['EnrolmentRestriction']);
$MODULE_OBJ = $lswimminggala->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$xmsg]);
$htmlAry['searchBox'] = $linterface->Get_Search_Box_Div('keyword', $keyword);
?>

<form name="form1" method="get">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td align="left" class="tabletext">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td><?=$AddBtn?></td>
					<td align="right"><?=$htmlAry['searchBox']?></td>
				</tr>
				<tr>
					<td colspan="2" align="right" valign="bottom" height="28">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td>
									<div class="common_table_tool">
								        <?=$editBtn?>
								        <?=$delBtn?>
									</div>		
								</td>		
							</tr>
						</table>
					</td>
				</tr>
                <tr>
                	<td colspan="2">
						<?php
							echo $li->display($li->no_col);
						?>
					</td>
				</tr>
				</table>
			</td>
		</tr>
    </table>

<input type="hidden" name="pageNo" value="<?=$li->pageNo?>" />
<input type="hidden" name="order" value="<?=$li->order?>" />
<input type="hidden" name="field" value="<?=$li->field?>" />
<input type="hidden" name="page_size_change" value="" />
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />

</form>
 
<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>