<?php
# using: 

/********************************************************
 *  Modification Log
 * 
 * 	Date: 	2017-09-14	Bill	[2017-0906-1708-05236]
 * 			- Copy form Sport Days > Support Enrolment Restriction
 * 
 ********************************************************/

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

if(!$plugin['swimming_gala'] || !$_SESSION["SSV_USER_ACCESS"]["eAdmin-eSportsAdmin"])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

include_once($PATH_WRT_ROOT."includes/libswimminggala.php");
$lswimminggala = new libswimminggala();

# Update Reason
$reason = intranet_htmlspecialchars(trim($reason));
$sql = "UPDATE SWIMMINGGALA_ENROL_RESTRICTION_LIST SET Reason = '$reason', DateModified = now(), ModifyBy = $UserID WHERE UserID = $sid";
$lswimminggala->db_db_query($sql);

intranet_closedb();
header("Location: index.php?xmsg=UpdateSuccess");
?>