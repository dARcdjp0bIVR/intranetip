<?php

$PATH_WRT_ROOT = "../../../../../../"; 
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libswimminggala.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$lswimminggala = new libswimminggala();
$lswimminggala->authSportsSystem();
$house_relay_name = $lswimminggala->retrieveEventTypeNameByID(3);
$class_relay_name = $lswimminggala->retrieveEventTypeNameByID(4);

$linterface 	= new interface_html();
$CurrentPage	= "PageArrangement_Schedule";

$roundType = 1;
$LaneArrange = $lswimminggala->retrieveTFLaneArrangeDetail($eventGroupID, $roundType);
$GroupName = $lswimminggala->returnAgeGroupName($AgeGroup);

# Create a new array for lane arrangement
for($i=0; $i<sizeof($LaneArrange); $i++)
{
	list($s_id, $s_name, $s_class, $heat, $order) = $LaneArrange[$i];
	$arrangeArr[$heat][$order]["id"] = $s_id;
        
	$arrangeArr[$heat][$order]["info"] = "<table width='100%' border='0' cellspacing='0' cellpadding='0'>";
	$arrangeArr[$heat][$order]["info"] .= "<tr>";
        $arrangeArr[$heat][$order]["info"] .= "<td align='left' class='tabletext'> ". $s_name ." </td>";
        $arrangeArr[$heat][$order]["info"] .= "</tr>";
        if($s_class) {
        $arrangeArr[$heat][$order]["info"] .= "<tr>";
	$arrangeArr[$heat][$order]["info"] .= "<td align='left' class='tabletext'>(".$s_class.")</td>";
        $arrangeArr[$heat][$order]["info"] .= "</tr>";
        }
	$arrangeArr[$heat][$order]["info"] .= "</table>";
                
}
# Get the FirstRoundGroupCount (in order to check if �u�D�ƥ� is selected)
if($eventType == 1)		# If this is Track event
{
	$sql = "SELECT FirstRoundGroupCount,FirstRoundType FROM SWIMMINGGALA_EVENTGROUP_EXT_TRACK WHERE EventGroupID = '$eventGroupID'";
	$FirstRoundNum = $lswimminggala->returnVector($sql);
	$sql = "SELECT FirstRoundType FROM SWIMMINGGALA_EVENTGROUP_EXT_TRACK WHERE EventGroupID = '$eventGroupID'";
	$FirstRoundType=$lswimminggala->returnVector($sql);
}

# Lane Arrangement
$laneNumFlag = 0;
$numberOfLane = $lswimminggala->numberOfLanes;
$resultDisplay = "";
if($FirstRoundNum[0] == 0 || sizeof($FirstRoundNum) != 0)
{
	$laneNumFlag = 1;
	$resultDisplay .= "<tr>";
	$resultDisplay .= "<td>&nbsp;</td>";
	for($i=1; $i<=$numberOfLane; $i++)
	{
		$resultDisplay .= "<td class='tablebluetop tabletopnolink'>".$i_Sports_TheLine1.$i.$i_Sports_TheLine2."</td>";
	}
	$resultDisplay .= "</tr>";
}
else
{
	$resultDisplay .= "<tr><td colspan='".($numberOfLane+1)."' >&nbsp;</td></tr>";
}

# retrieve the maxinum key of the arrangeArr array
if(is_array($arrangeArr))
{
	rsort($keys1 = array_keys($arrangeArr));
	$maxGroup = $keys1[0];
}
else
	$maxGroup = 0;

	$playerCtr = 0;
	$slotCtr = 0;
for($i=1; $i<=$maxGroup; $i++)
{
	$resultDisplay .= "<tr height=70px>";
	$resultDisplay .= "<td width='50' class='tablebluelist'>".$i_Sports_TheGroup1.$i.$i_Sports_TheGroup2."</td>";
	$tempGroup = $arrangeArr[$i];

	# Retrieve the maxinum key of the tempGroup array
	if(sizeof($tempGroup) != 0)
	{
		rsort($keys2 = array_keys($tempGroup));
		//$maxPos = $keys2[0];
		$maxPos = $numberOfLane;
	}
	else
		$maxPos = 0;
	
	for($k=1; $k<=$maxPos; $k++)
	{
		$key = $k;
		
		$sid = $tempGroup[$key]["id"];
		$info = $tempGroup[$key]["info"];

		# Retrieve House Color Code of the Student
		$House = $lswimminggala->retrieveStudentHouseInfo($sid);
                
                # Student info 
				$stu_into_table = "<div id='s$i$k' class='slots'>";
                $stu_into_table .= "<div id='p$i$k'  class='players'>";
		$stu_into_table .= "<table width='100%' border='0' cellspacing='0' cellpadding='3'>";
                $stu_into_table .= "<tr>";
                $stu_into_table .= "<td align='center' valign='top'>";
                $stu_into_table .= $lswimminggala->house_flag($House[0][2]);
                $stu_into_table .= "</td>";
                $stu_into_table .= "<td width='100%'>". $info ."</td>";
                $stu_into_table .= "</tr>";
                $stu_into_table .= "</table>";
				$stu_into_table .=  "</div></div>";
                
				$js_player .=  "players[".($playerCtr)."] = new YAHOO.example.DDPlayer(\"p$i$k\", \"slots\");\n";
				$js_slot .=    "slots[".($slotCtr)."] = new YAHOO.util.DDTarget(\"s$i$k\", \"slots\");\n";
				$js_assign_slot .="players[".($playerCtr)."].slot = slots[".($slotCtr)."];\n";
				$js_assign_stu_id .="players[".($playerCtr)."].stu_id = '".($sid?$sid:"NULL")."';\n";
				$js_assign_player .="slots[".($slotCtr)."].player = players[".($playerCtr)."];\n";
				$js_assign_group .="slots[".($slotCtr)."].group = $i;\n";
				$js_assign_lane .="slots[".($slotCtr)."].lane = $k;\n";
				
				$slotCtr++;
				$playerCtr++;

		# Display the Student Info
		if($key > $numberOfLane && ($key%$numberOfLane) == 1)
		{
			$resultDisplay .= "</tr>";
			$resultDisplay .= "<tr height=70px>";
			$resultDisplay .= "<td width='50'>&nbsp;</td>";
			$resultDisplay .= "<td>".$stu_into_table."</td>";
		}
		else
		{
			$resultDisplay .= "<td>".$stu_into_table."</td>";
		}
	}
	if($maxPos < $numberOfLane)
	{
		$diff = $numberOfLane - $maxPos;
		for($j=0; $j<$diff; $j++)
			$resultDisplay .= "<td>&nbsp;</td>";
	}
	$resultDisplay .= "</tr>";
}

# Change Position
if($FirstRoundNum[0] == 0 || sizeof($FirstRoundNum) != 0)
{
	$changeGroupNum = floor($numberOfLane/2);
	$change_position_tag = "";
	for($i=0; $i<$changeGroupNum; $i++)
	{
		$lane_select1 = "<SELECT name='lane1_$i'>";
		$lane_select1 .= "<OPTION value=''>-</OPTION>";
		for($j=1; $j<=$numberOfLane; $j++)
		{
			$lane_select1 .= "<OPTION value='$j'>$j</OPTION>";
		}
		$lane_select1 .= "</SELECT>";
		
		$group_select1 = "<SELECT name='group1_$i'>";
		$group_select1 .= "<OPTION value=''>-</OPTION>";
		for($k=1; $k<=$maxGroup; $k++)
		{
			$group_select1 .= "<OPTION value='$k'>$k</OPTION>";
		}
		$group_select1 .= "</SELECT>";

		$lane_select2 = "<SELECT name='lane2_$i'>";
		$lane_select2 .= "<OPTION value=''>-</OPTION>";
		for($j=1; $j<=$numberOfLane; $j++)
		{
			$lane_select2 .= "<OPTION value='$j'>$j</OPTION>";
		}
		$lane_select2 .= "</SELECT>";
		
		$group_select2 = "<SELECT name='group2_$i'>";
		$group_select2 .= "<OPTION value=''>-</OPTION>";
		for($k=1; $k<=$maxGroup+1; $k++)
		{
			$group_select2 .= "<OPTION value='$k'>$k</OPTION>";
		}
		$group_select2 .= "</SELECT>";

		
		$change_position_tag .= "<tr class='tablerow". ( ($i)%2 + 1 )."'>";
		$change_position_tag .= "<td>".$group_select1." - ".$lane_select1."</td>";
		$change_position_tag .= "<td>".$group_select2." - ".$lane_select2."</td>";
		$change_position_tag .= "</tr>";
	}
} 
	
### Title ###
$TAGS_OBJ[] = array($i_Sports_menu_Settings_CommonTrackFieldEvent,"schedule.php", 1);
$TAGS_OBJ[] = array($house_relay_name,"schedule_relay.php", 0);
$TAGS_OBJ[] = array($class_relay_name,"schedule_class_relay.php", 0);
$MODULE_OBJ = $lswimminggala->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();

// added by marcus 17/6 
//$STU_NUM_EXCEED_LANE_NUM=($FirstRoundType[0]==1&&ceil($total/$FirstRoundNum[0])>$numberOfLane)?"true":"false";

?>
<style>
.players
{
	width:100%;
	height:100%;
}

.slots
{
	cursor:move;
	width:100%;
	height:100%;
}
</style>

<!-------------------Drag Drop Start----------------------->

<!-- Combo-handled YUI JS files: -->  
<script type="text/javascript" src="http://yui.yahooapis.com/combo?2.7.0/build/yahoo-dom-event/yahoo-dom-event.js&2.7.0/build/dragdrop/dragdrop-min.js"></script>  
  
<script type="text/javascript">

(function() {

YAHOO.example.DDPlayer = function(id, sGroup, config) {
    YAHOO.example.DDPlayer.superclass.constructor.apply(this, arguments);
    this.initPlayer(id, sGroup, config);
};

YAHOO.extend(YAHOO.example.DDPlayer, YAHOO.util.DDProxy, {

    TYPE: "DDPlayer",

    initPlayer: function(id, sGroup, config) {
        if (!id) { 
            return; 
        }

        var el = this.getDragEl()
        YAHOO.util.Dom.setStyle(el, "borderColor", "transparent");
        YAHOO.util.Dom.setStyle(el, "opacity", 0.76);

        // specify that this is not currently a drop target
        this.isTarget = false;

        this.originalStyles = [];

        this.type = YAHOO.example.DDPlayer.TYPE;
        this.slot = null;

        this.startPos = YAHOO.util.Dom.getXY( this.getEl() );
        YAHOO.log(id + " startpos: " + this.startPos, "info", "example");
    },

    startDrag: function(x, y) {
        YAHOO.log(this.id + " startDrag", "info", "example");
        var Dom = YAHOO.util.Dom;

        var dragEl = this.getDragEl();
        var clickEl = this.getEl();

        dragEl.innerHTML = clickEl.innerHTML;
        dragEl.className = clickEl.className;

        Dom.setStyle(dragEl, "color",  Dom.getStyle(clickEl, "color"));
        Dom.setStyle(dragEl, "backgroundColor", Dom.getStyle(clickEl, "backgroundColor"));

        Dom.setStyle(clickEl, "opacity", 0.1);

        var targets = YAHOO.util.DDM.getRelated(this, true);
        YAHOO.log(targets.length + " targets", "info", "example");
        for (var i=0; i<targets.length; i++) {
            
            var targetEl = this.getTargetDomRef(targets[i]);

            if (!this.originalStyles[targetEl.id]) {
                this.originalStyles[targetEl.id] = targetEl.className;
            }

            //targetEl.className = "target";
        }
    },

    getTargetDomRef: function(oDD) {
        if (oDD.player) {
            return oDD.player.getEl();
        } else {
            return oDD.getEl();
        }
    },

    endDrag: function(e) {
        // reset the linked element styles
        YAHOO.util.Dom.setStyle(this.getEl(), "opacity", 1);

        this.resetTargets();
    },

    resetTargets: function() {

        // reset the target styles
        var targets = YAHOO.util.DDM.getRelated(this, true);
        for (var i=0; i<targets.length; i++) {
            var targetEl = this.getTargetDomRef(targets[i]);
            var oldStyle = this.originalStyles[targetEl.id];
            if (oldStyle) {
                targetEl.className = oldStyle;
            }
        }
    },

    onDragDrop: function(e, id) {
        // get the drag and drop object that was targeted
        var oDD;
        
        if ("string" == typeof id) {
            oDD = YAHOO.util.DDM.getDDById(id);
        } else {
            oDD = YAHOO.util.DDM.getBestMatch(id);
        }

        var el = this.getEl();

        // check if the slot has a player in it already
        if (oDD.player) {
            // check if the dragged player was already in a slot
            if (this.slot) {
                // check to see if the player that is already in the
                // slot can go to the slot the dragged player is in
                // YAHOO.util.DDM.isLegalTarget is a new method
                if ( YAHOO.util.DDM.isLegalTarget(oDD.player, this.slot) ) {
                    YAHOO.log("swapping player positions", "info", "example");
                    YAHOO.util.DDM.moveToEl(oDD.player.getEl(), el);
                    this.slot.player = oDD.player;
                    oDD.player.slot = this.slot;
                } else {
                    YAHOO.log("moving player in slot back to start", "info", "example");
                    YAHOO.util.Dom.setXY(oDD.player.getEl(), oDD.player.startPos);
                    this.slot.player = null;
                    oDD.player.slot = null
                }
            } else {
                // the player in the slot will be moved to the dragged
                // players start position
                oDD.player.slot = null;
                YAHOO.util.DDM.moveToEl(oDD.player.getEl(), el);
            }
        } else {
            // Move the player into the emply slot
            // I may be moving off a slot so I need to clear the player ref
            if (this.slot) {
                this.slot.player = null;
            }
        }

        YAHOO.util.DDM.moveToEl(el, oDD.getEl());
        this.resetTargets();

        this.slot = oDD;
        this.slot.player = this;
		
		form1.laneStr.value = "";
		for(x in slots)
		{
			form1.laneStr.value += (form1.laneStr.value==""?"":",")+slots[x].group+":"+slots[x].lane+":"+slots[x].player.stu_id;
		}
    },

    swap: function(el1, el2) {
        var Dom = YAHOO.util.Dom;
        var pos1 = Dom.getXY(el1);
        var pos2 = Dom.getXY(el2);
        Dom.setXY(el1, pos2);
        Dom.setXY(el2, pos1);
		
    },

    onDragOver: function(e, id) {
    },

    onDrag: function(e, id) {
    }

});

var slots = [], players = [],
    Event = YAHOO.util.Event, DDM = YAHOO.util.DDM;

Event.onDOMReady(function() { 
    // slots
	<?=$js_slot?>

    // players
	<?=$js_player?>

	<?=$js_assign_slot?>
	<?=$js_assign_player?>
	<?=$js_assign_stu_id?>
	<?=$js_assign_group?>
	<?=$js_assign_lane?>
	
	
    DDM.mode = document.getElementById("ddmode").selectedIndex;

    Event.on("ddmode", "change", function(e) {
            YAHOO.util.DDM.mode = this.selectedIndex;
        });
});

})();
// -----------------Drag Drop End------------------------


</script>
<br />   

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?= $linterface->GET_NAVIGATION2($i_Sports_Event_Detail) ?></td>
	<td align='right'>&nbsp;</td>
</tr>
<tr>
	<td colspan="2">
                <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr> 
                	<td>
				<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
				<tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_Sports_field_Group?></span></td>
					<td class="tabletext"><?=$GroupName?></td>
				</tr>
				<tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_Sports_Item?></span></td>
					<td class="tabletext"><?=$eventName?></td>
				</tr>
                                <tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_Sports_Enroled_Student_Count?></span></td>
					<td class="tabletext"><?=$total?></td>
				</tr>
                                <tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_Sports_Arranged_Student_Count?></span></td>
					<td class="tabletext"><?=$arranged?></td>
				</tr>
                                
			</table>
			</td>
                </tr>
                </table>
	</td>
</tr>
<tr>
	<td colspan="2">        
                <table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
                <tr>
                	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
                </table>                                
	</td>
</tr>
</table>               



<br />
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?= $linterface->GET_NAVIGATION2($i_Sports_Arrange_Lane) ?></td>
	<td align="right" valign="bottom"><?=$linterface->GET_SYS_MSG($xmsg);?></td>
</tr>
<tr>
	<td colspan="2">
                <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr> 
                	<td>
				<table align="center" width="100%" border="0" cellpadding="3" cellspacing="0" style="table-layout:fixed">
                                <?=$resultDisplay?>
				</table>
			</td>
                </tr>
                </table>
	</td>
</tr>
<tr>
	<td colspan="2">        
                <table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
                <tr>
                	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
                <tr>
			<td align="center">
				<?= $linterface->GET_ACTION_BTN($i_Sports_Re_Auto_Arrange_Lane, "button", "document.form1.action='auto_lane_arrange2.php';document.form1.submit();","submit2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
			</td>
		</tr>
                </table>                                
	</td>
</tr>
</table>          

<br />
<form name="form1" action="manual_lane_arrange2.php" method="post" > 
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<!--<tr>
	<td><?= $linterface->GET_NAVIGATION2($i_Sports_Arrange_Change_Postion) ?></td>
	<td align='right'>&nbsp;</td>
</tr>
<tr>
	<td colspan="2">
                <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr> 
                	<td>
				<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
				<tr>
                                	<td class='tabletop tabletoplink'><?=$i_Sports_From?> (<?=$i_Sports_Group?> - <?=$i_Sports_Line?>)</td>
                                        <td class='tabletop tabletoplink'><?=$i_Sports_To?> (<?=$i_Sports_Group?> - <?=$i_Sports_Line?>)</td>
				</tr>
                                <?=$change_position_tag?>
				</table>
			</td>
                </tr>
                </table>
	</td>
</tr>-->
<tr>
	<td colspan="2">        
                <table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
                <tr>
                	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
                <tr>
			<td align="center">
				<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "","submit2") ?>
                                <?= $linterface->GET_ACTION_BTN($button_reset, "reset", "window.location.reload()","reset2") ?>
                                <?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='schedule.php'","cancel2") ?>
			</td>
		</tr>
                </table>                                
	</td>
</tr>
</table>
<br />

<input type="hidden" name="changeGroupNum" value="<?=$changeGroupNum?>">
<input type="hidden" name="eventGroupID" value="<?=$eventGroupID?>">
<input type="hidden" name="eventType" value="<?=$eventType?>">
<input type="hidden" name="roundType" value="<?=$roundType?>">
<input type="hidden" name="total" value="<?=$total?>">
<input type="hidden" name="arranged" value="<?=$arranged?>">
<input type="hidden" name="eventName" value="<?=$eventName?>">
<input type="hidden" name="laneStr" id="laneStr">;
</form>

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>