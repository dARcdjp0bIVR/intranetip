<?
# using: 
/*
 * 2017-10-26 (Carlos): Get house name with libsports.php retrieveStudentHouseInfo() instead of retrieveStudentHouse().
 */

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libswimminggala.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

$lsports = new libswimminggala();
$lexport = new libexporttext();

$result = array();

# header
$header_ary = array("student name (Eng)", "student name (Chi)","House", "Class", "Class Number","Sex","Grade","Event1","Event2","Event3");
$result[] = $header_ary;

$students = $lsports->retrieveStudentInfo($engName, $chiName, $theClass, $ageGroup, $house, " d.Sequence, a.ClassName, a.ClassNumber ");

# age group Info
$AgeGroupInfo = BuildMultiKeyAssoc((array)$lsports->retrieveAgeGroupInfo(), "AgeGroupID");

foreach($students as $key=>$thisStudent)
{
	list($tmp_UserID, $tmp_EnName, $tmp_ChiName, $tmp_EnClass, $tmp_ChiClass, $tmp_ClassNumber, $Gender) = $thisStudent;
    
    $tmpResult = array();
		    
	# Student Name
	$tmpResult[] = $tmp_EnName;
	$tmpResult[] = $tmp_ChiName;
	
	# house
	//$tmpResult[] = $lsports->retrieveStudentHouse($tmp_UserID);
	$house_info = $lsports->retrieveStudentHouseInfo($tmp_UserID);
	$tmpResult[] = $house_info[0][0];
	
	# Class
	$tmpResult[] = $tmp_EnClass;
	
	# Class Number
	$tmpResult[] = $tmp_ClassNumber;
 	
 	# Gender
 	if($Gender=="M")
 		$Gender = "男";
 	else if($Gender=="F")
		$Gender = "女";
 	$tmpResult[] = $Gender;
 	
 	# Grade
 	$AgeGroupID = $lsports->retrieveAgeGroupByStudentID($tmp_UserID);
	$tmpResult[] = $AgeGroupInfo[$AgeGroupID]["EnglishName"];
	
	# retrieve Enroled events
	$tmpEvents = $lsports->retrieveStudentEnroledEvent($tmp_UserID, 1);
    
    for($i=0;$i<3;$i++)
    {
		# Event

		$tmpResult[] = $tmpEvents[$i][0];
  	}   
 	
 	$result[] = $tmpResult;

		

}

$utf_content = "";
foreach($result as $k=>$d)
{
	$utf_content .= implode("\t", $d);
	$utf_content .= "\r\n";
}

$filename = "sport_student_enrolment_list.csv";
$lexport->EXPORT_FILE($filename, $utf_content);

intranet_closedb();
?>
