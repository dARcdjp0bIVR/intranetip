<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libswimminggala.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");

intranet_auth();
intranet_opendb();

$linterface 	= new interface_html();
$CurrentPage	= "PageArrangement_Schedule";

$lswimminggala = new libswimminggala();
$lclass = new libclass();
$lswimminggala->authSportsSystem();

$eventName = str_replace(",", " ", $eventName);
$numberOfLane = $lswimminggala->numberOfLanes;

// # get the selection list of houses
// $houses = $lswimminggala->retrieveHouseSelectionInfo();

# retrieve the class relay lane arrangement info
$class_arrange = $lswimminggala->retrieveClassRelayLaneArrangement($eventGroupID);

# form content
$form_content = "";
for($i=1; $i<=$numberOfLane; $i++)
{
	# Retrieve the current lane position
	$select_class = "";
	for($j=0; $j<sizeof($class_arrange); $j++)
	{
		list($class_id, $order) = $class_arrange[$j];
		if($order == $i)
		{
			$select_class = $class_id;
			break;
		}
	}
	//$houses_select = getSelectByArray($houses, "name='houseID$i'", $select_house, 0, 0);
	$class_select = $lclass->getSelectClassID("name='ClassID$i'", $select_class, 1);

	$css = ($i%2?"":"2");
	$form_content .= "<tr>";
        $form_content .= "<td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'><span class='tabletext'>".$i_Sports_The.$i.$i_Sports_Line."</span></td>";
	$form_content .= "<td class='tableContent$css'>".$class_select."</td>";
	$form_content .= "</tr>";
}

$house_relay_name = $lswimminggala->retrieveEventTypeNameByID(3);
$class_relay_name = $lswimminggala->retrieveEventTypeNameByID(4);

### Title ###
$TAGS_OBJ[] = array($Lang['eSports']['IndividualEvent'],"schedule.php", 0);
$TAGS_OBJ[] = array($house_relay_name,"schedule_relay.php", 0);
$TAGS_OBJ[] = array($class_relay_name,"schedule_class_relay.php", 1);
$MODULE_OBJ = $lswimminggala->GET_MODULE_OBJ_ARR();

$PAGE_NAVIGATION[] = array($button_edit.($intranet_session_language=="en"?" ":"").$class_relay_name);

$linterface->LAYOUT_START();
?>

<br />   
<form name="form1" action="class_relay_edit_update.php" method="post">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
</tr>
<tr>
	<td colspan="2">
                <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr> 
                	<td>
				<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
				<tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_Sports_Item?></span></td>
					<td class="tabletext"><?=stripcslashes($eventName)?></td>
				</tr>
                                <tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_Sports_field_Group?></span></td>
					<td class="tabletext"><?=stripcslashes($groupName)?></td>
				</tr>
                                <?=$form_content?>
			</table>
			</td>
                </tr>
                </table>
	</td>
</tr>
<tr>
	<td colspan="2">        
                <table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
                <tr>
                	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
                <tr>
			<td align="center">
				<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "","submit2") ?>
                                <?= $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2") ?>
                                <?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='schedule_class_relay.php'","cancel2") ?>
			</td>
		</tr>
                </table>                                
	</td>
</tr>
</table>               
<br />

<input type="hidden" name="eventGroupID" value="<?=$eventGroupID?>">
</form>

<?
intranet_closedb();
print $linterface->FOCUS_ON_LOAD("form1.ClassID1");
$linterface->LAYOUT_STOP();
?>