<?php
# editing by:
/***************************************************************************
*	modification log:
*	2013-09-04	Roy
*	- Fix: change export button type to "submit"
*
***************************************************************************/
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libswimminggala.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$linterface 	= new interface_html();
$CurrentPage	= "PageArrangement_EnrolmentUpdate";

$lswimminggala = new libswimminggala();
$lclass = new libclass();
$lswimminggala->authSportsSystem();


### form selection
$ClassLevelArr = $lclass->getLevelArray();
$FormSelection = getSelectByArray($ClassLevelArr, "name='FormSelection[]' id='FormSelection' multiple ", '', '', 1, '');

## class selection
$ClassArr = $lclass->getClassList();
$ClassSelection = getSelectByArray($ClassArr, "name='ClassSelection[]' id='ClassSelection' multiple style='display:none'", '', '', 1, '');

### download csv
$csvFile = "<a class='tablelink' href='". GET_CSV("enrolmentlist_sample.csv") ."'>". $Lang['General']['ClickHereToDownloadSample'] ."</a>";

### Title ###
$TitleTitle1 = "<span class='contenttitle'>". $i_Sports_menu_Arrangement_EnrolmentUpdate ."</span>";
$TitleTitle = "<table width='100%' height='25' cellpadding='0' cellspacing='0' ><tr><td style=\"vertical-align: bottom;\" >".$TitleTitle1."</td></tr></table>";
$TAGS_OBJ[] = array($TitleTitle, "", 0);    

$STEPS_OBJ[] = array($i_general_select_csv_file, 1);
$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 0);
$STEPS_OBJ[] = array($i_general_imported_result, 0);
$MODULE_OBJ = $lswimminggala->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();
?>
<script>
function showResult(type)
{
	if(type=="form")
	{
		$("#FormSelection").show();
		$("#ClassSelection").hide();
	}
	else
	{
		$("#ClassSelection").show();
		$("#FormSelection").hide();
		
	}
}

function SelectAll()
{
	$("[name*=Selection[]]:visible").contents().attr("selected",true);
}

function CheckForm ()
{
	if(!$("[name*=Selection[]]:visible").val()) 
	{
		alert("<?=$Lang['eSports']['warnSelectClass']?>");
		return false;
	}
	
	if(document.frm1.action == "import_update.php"&&!$("#csvfile").val()) 
	{
		alert("<?=$Lang['eSports']['warnSelectcsvFile']?>");
		return false;
	}
	
}

function SubmitForm(url)
{
	document.frm1.action = url
}

$(document).ready(function(){
	$("#importTarget").val("form")
});

</script>
<form method="POST" name="frm1" id="frm1" action="import_update.php" enctype="multipart/form-data" onsubmit="return CheckForm()">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td>&nbsp;</td>
	<td align='right'><?=$linterface->GET_SYS_MSG("",$xmsg);?></td>
</tr>
<tr>
	<td colspan="2"><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
</tr>
<tr>
	<td colspan="2">
                <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr> 
                	<td><br />
				<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">			
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle">
						<?=$i_Discipline_Target?> <span class="tabletextrequire">*</span>
					</td>
					<td>
						<table><tr>
						<td valign="top">
							<select name="importTarget" id="importTarget" onChange="showResult(this.value);">
								<option value="form" <? if($importTarget=="form") { echo "selected";} ?>><?=$Lang['eSports']['Form']?></option>
								<option value="class" <? if($importTarget=="class") { echo "selected";} ?>><?=$Lang['eSports']['Class']?></option>
							</select>
						</td>
						<td>
							<?=$FormSelection?><?=$ClassSelection?>
							<br><?= $linterface->GET_ACTION_BTN($button_select_all, "button", "SelectAll();", "selectAllBtn01")?>
							<?= $linterface->GET_ACTION_BTN($button_export, "submit", "SubmitForm('export.php')","submit3","") ?>
						</td>
						</tr></table>
					</td>
				</tr>
				<tr>
					<td class="formfieldtitle" align="left"><?=$Lang['General']['SourceFile']?> <span class="tabletextremark"><?=$Lang['General']['CSVFileFormat']?></span></td>
					<td class="tabletext"><input class="file" type="file" name="csvfile" id = "csvfile"></td>
				</tr>
				<tr>
					<td class="formfieldtitle" align="left"><?=$Lang['General']['CSVSample']?> </td>
					<td class="tabletext"><?=$csvFile?></td>
				</tr>				
			</table>
			</td>
                </tr>
                </table>
	</td>
</tr>
<tr>
	<td colspan="2">        
		<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
			<tr>
            	<td align="left" class="tabletextremark">&nbsp;&nbsp;&nbsp;<?=$i_general_required_field2?></td>
			</tr>		
			<tr>
                <td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
            </tr>
            <tr>
				<td align="center">
					<?= $linterface->GET_ACTION_BTN($button_import, "submit", "SubmitForm('import_update.php')","submit2"," class='formbutton'  onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>				
				</td>
			</tr>
        </table>                                
	</td>
</tr>
</table>  
</form>

<?
 $linterface->LAYOUT_STOP();
?>

