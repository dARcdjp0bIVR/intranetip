<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libswimminggala.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

# Get House info
$lsports = new libswimminggala();
$lsports->authSportsSystem();

$lsports->Start_Trans();
$success = array();
foreach($ManualAssignStudent as $thisStudentID)
{
	$thisGroup = $group[$thisStudentID];
	$thisLane = $lane[$thisStudentID];
	if(trim($thisGroup)=='' || trim($thisLane)=='')
		continue;

	$success[] = $lsports->Assign_Student_To_EventGroup($thisStudentID,$eventGroupID,$thisGroup,$thisLane,$roundType);
}

if(in_array(false,$success)|| count($success)==0)
{
	$lsports->RollBack_Trans();
	$msg = "update_failed";
}
else
{
	$lsports->Commit_Trans();
	$msg = "update";
}
intranet_closedb();
header ("Location:tf_lane_arrange_detail.php?eventGroupID=$eventGroupID&xmsg=$msg");

?>