<?php

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libswimminggala.php");

intranet_auth();
intranet_opendb();

# Get House info
$lswimminggala = new libswimminggala();
$lswimminggala->authSportsSystem();

if($EventGroupIDs != "")
{
	$EventGroupIDArr = explode(",", $EventGroupIDs);
	$EventGroupTypeArr = explode(",", $EventGroupTypes);
	
	$roundType = 1;

	$lswimminggala->autoLanesArrange($EventGroupIDArr, $EventGroupTypeArr, $roundType);
}

intranet_closedb();
header ("Location:schedule.php?xmsg=update");

?>