<?php

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libswimminggala.php");

intranet_auth();
intranet_opendb();

# Get House info
$lswimminggala = new libswimminggala();
$lswimminggala->authSportsSystem();

$EventGroupIDArr = array($eventGroupID);
$EventGroupTypeArr = array($eventType);

$lswimminggala->autoLanesArrange($EventGroupIDArr, $EventGroupTypeArr, $roundType);

intranet_closedb();
header ("Location:tf_lane_arrange_detail.php?eventGroupID=$eventGroupID&eventType=$eventType&total=$total&arranged=$arranged&eventName=$eventName&AgeGroup=$AgeGroup&xmsg=update");

?>
