<?php
# using: yat

###########################
#	2013-09-02	Roy
#	- delete wrong field (FieldEnrollCount) in SQL statement
#	- add trackCount
#	2012-06-13	YatWoon
#	- add event quota checking (double check)
#
###########################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libswimminggala.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();
$lswimminggala = new libswimminggala();

$typeArr = explode(",", $types);

$trackCount = $_REQUEST["tc"];

#Clear the old records 
$sql = "DELETE FROM SWIMMINGGALA_STUDENT_ENROL_EVENT WHERE StudentID = '$StudentID'";
$lswimminggala->db_db_query($sql);

# Insert Restrict Quota Event Enrolment Record(s)
$count = 0;
$face_full_quota = 0;
for($i=0; $i<sizeof($typeArr); $i++)
{
	$t = $typeArr[$i];
	$tempArr = ${RQ_EventGroup.$t};
	$de = "";
	$values = "";
	${totalCount.$t} = 0;
	for($j=0; $j<sizeof($tempArr); $j++)
	{
		$egid = $tempArr[$j];
		
		# check the event is full or not
        $cur_enroled = $lswimminggala->returnEventEnroledNo($egid, $StudentID);
        $eventQuota = $lswimminggala->returnEventQuota($egid);
        if($cur_enroled < $eventQuota || !$eventQuota)	
        {
	//		$values .= $de."(".$StudentID.", ".$egid.", now(), $UserID)";
	//		$de = ",";
			$values = "(".$StudentID.", ".$egid.", now(), $UserID)";
			
			# Get all enroled EventGroupID
			$allEvents[$count] = $egid;
			$count++;
			
			$fields = "(StudentID, EventGroupID, DateModified, InputBy)";
			$sql = "INSERT INTO SWIMMINGGALA_STUDENT_ENROL_EVENT $fields VALUES $values";
			$lswimminggala->db_db_query($sql);
		
			//${totalCount.$t} = sizeof($tempArr);
			${totalCount.$t}++;
		}
		else
		{
			$face_full_quota = 1;
		}
		
	}
}
// $totalCount1 = sizeof($EventGroup1);

###############
# Insert Unrestrict Quota Event Enrolment Record(s)
$de = "";
$values = "";
for($i=0; $i<sizeof($UQ_EventGroup); $i++)
{
	$egid = $UQ_EventGroup[$i];
	
	# check the event is full or not
    $cur_enroled = $lswimminggala->returnEventEnroledNo($egid, $StudentID);
    $eventQuota = $lswimminggala->returnEventQuota($egid);
        
    if($cur_enroled < $eventQuota || !$eventQuota)	
	{
//		$values .= $de."(".$StudentID.", ".$egid.", now(), $UserID)";
//		$de = ",";
		$values = "(".$StudentID.", ".$egid.", now(), $UserID)";
		
		$fields = "(StudentID, EventGroupID, DateModified, InputBy)";
		$sql = "INSERT INTO SWIMMINGGALA_STUDENT_ENROL_EVENT $fields VALUES $values";
		$lswimminggala->db_db_query($sql);
	}
	else
	{
		$face_full_quota = 1;
	}
}

#####################

#Check Record Exist Or Not
$sql = "SELECT COUNT(*) FROM SWIMMINGGALA_STUDENT_ENROL_INFO WHERE StudentID = '$StudentID'";
$exist_info = $lswimminggala->returnVector($sql);

if($exist_info[0] == 0) 
{
	#Insert New Record To SWIMMINGGALA_STUDENT_ENROL_INFO
	$fields1 = "(StudentID, TrackEnrolCount, DateModified)";
	$values1 = "(".$StudentID.", ".$trackCount.", now())";
	$sql = "INSERT INTO SWIMMINGGALA_STUDENT_ENROL_INFO $fields1 VALUES $values1";
	$lswimminggala->db_db_query($sql);
}
else
{
	// #Retrieve Amount of Events that the Student Have Enroled
	// $sql = "SELECT TrackEnrolCount FROM SWIMMINGGALA_STUDENT_ENROL_INFO WHERE StudentID = '$StudentID'";
	// $temp = $lswimminggala->returnArray($sql, 1);
	// if(sizeof($temp) != 0)
	// {
		// $trackCount = $temp[0][0];
	// }
	// else
	// {
		// $trackCount = 0;
	// }

	#Update Existing Record in SWIMMINGGALA_STUDENT_ENROL_INFO
	$sql = "UPDATE SWIMMINGGALA_STUDENT_ENROL_INFO SET TrackEnrolCount = '$trackCount', DateModified = now() WHERE StudentID = '$StudentID'";
	$lswimminggala->db_db_query($sql);
}

# Update the records in SWIMMINGGALA_LANE_ARRANGEMENT
if(sizeof($allEvents) != 0)
{
	$enroledID = (is_array($allEvents)) ? implode($allEvents, ",") : $allEvents;
	$sql = "DELETE FROM SWIMMINGGALA_LANE_ARRANGEMENT WHERE StudentID = '$StudentID' AND EventGroupID NOT IN ($enroledID)";
}
else
{
	$sql = "DELETE FROM SWIMMINGGALA_LANE_ARRANGEMENT WHERE StudentID = '$StudentID'";
}
$lswimminggala->db_db_query($sql);

intranet_closedb();
if($face_full_quota)
	$xmsg = "UpdatePartiallySuccess";
else
	$xmsg = "UpdateSuccess";
header ("Location: detail.php?StudentID=$StudentID&xmsg=$xmsg");
?>