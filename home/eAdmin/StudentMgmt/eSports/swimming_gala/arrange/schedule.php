<?php
# using: yat

###############################
#
#	Date:	2014-10-22	YatWoon	[Case#A70038] [ip.2.5.5.10.1]
#			add cust "Export for electronic board format".	
#			
###############################


$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libswimminggala.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$linterface 	= new interface_html();
$CurrentPage	= "PageArrangement_Schedule";

$lswimminggala = new libswimminggala();
$lswimminggala->authSportsSystem();

$AgeGroups = $lswimminggala->retrieveAgeGroupIDNames();	# retrieve all age group ids and names
# Add Boys Open and Girls Open 
$openBoy[0] = '-1';
$openBoy[1] = $i_Sports_Event_Boys_Open;
$openGirl[0] = '-2';
$openGirl[1] = $i_Sports_Event_Girls_Open;
$openMixed[0] = '-4';
$openMixed[1] = $i_Sports_Event_Mixed_Open;
$AgeGroups[] = $openBoy;
$AgeGroups[] = $openGirl;
$AgeGroups[] = $openMixed;

$TREvents = $lswimminggala->retrieveTrackFieldEventName();		# retrieve all track and field eventgroup ids and names
$house_relay_name = $lswimminggala->retrieveEventTypeNameByID(3);
$class_relay_name = $lswimminggala->retrieveEventTypeNameByID(4);

$numberOfLane = $lswimminggala->numberOfLanes;

# Create a new array to store the track and field results
for($i=0; $i<sizeof($TREvents); $i++)
{
	list($event_id, $event_name) = $TREvents[$i];

	for($j=0; $j<sizeof($AgeGroups); $j++)
	{
		list($group_id, $group_name) = $AgeGroups[$j];
		
		$typeIDs = array(1, 2);
		# Retrieve event group id
		$eventGroupInfo= $lswimminggala->retrieveEventGroupID($event_id, $group_id, $typeIDs);	

		$eventGroupID = $eventGroupInfo[0];
		$eventType = $eventGroupInfo[1];
		
		if($eventGroupID != "")
		{
			#modified by marcus 18/6
			#  Retrieve event enroled count (exclude those not in intranet user)
			$totalCount = $lswimminggala->retrieveEventEnroledCountInIntranet($eventGroupID);
			# Retrieve Count of Event that has been arranged
			$arrangedCount = $lswimminggala->retrieveTFLaneArrangedEventCount($eventGroupID);

			$TrackFieldResult[$event_id][$group_id]["eg_id"] = $eventGroupID;
			$TrackFieldResult[$event_id][$group_id]["event_type"] = $eventType;
			$TrackFieldResult[$event_id][$group_id]["total"] = $totalCount;
			$TrackFieldResult[$event_id][$group_id]["arranged"] = $arrangedCount;
		}
	}
}

### Age Group Title
$ageGroupTitle = "<tr><td>&nbsp;</td>";
for($i=0; $i<sizeof($AgeGroups); $i++) 
	$ageGroupTitle .= '<td align="center" class="tabletop tabletopnolink">'.$AgeGroups[$i][1].'</td>';
$ageGroupTitle .= "</tr>";

### Event content
$event_content = "";
$de = "";
$eg_ids = "";
for($j=0; $j<sizeof($TREvents); $j++)
{
	list($event_id, $event_name) = $TREvents[$j];
	
	$event_content .= "<tr class='tablerow". ($j%2?"2":"1")."'>";
	$event_content .= "<td class='tablelist'>".$event_name."</td>";
	
	for($k=0; $k<sizeof($AgeGroups); $k++)
	{
		$group_id = $AgeGroups[$k][0];

		if(sizeof($TrackFieldResult[$event_id][$group_id]) != 0)
		{
			$eventGroupID = $TrackFieldResult[$event_id][$group_id]["eg_id"];
			$event_type = $TrackFieldResult[$event_id][$group_id]["event_type"];
			$total = $TrackFieldResult[$event_id][$group_id]["total"];
			$arranged = $TrackFieldResult[$event_id][$group_id]["arranged"];
			
			#Check whether the Event has been set up
			$is_setup = $lswimminggala->checkExtInfoExist($event_type, $eventGroupID);
			
			if($is_setup == 1)
			{				
				if($total != 0)
				{
					$tr_display = "<a class='tablelink' href='tf_lane_arrange_detail.php?eventGroupID=$eventGroupID&eventType=$event_type&total=$total&arranged=$arranged&eventName=$event_name&AgeGroup=$group_id'>".$arranged."/".$total."</a>";

					$eg_ids .=  $de.$eventGroupID;
					$eg_types .= $de.$event_type;
					$de = ",";
				}
				else
				{
					$tr_display = "0/0";
				}
				$event_content .= "<td class='tabletext' align='center'>".$tr_display."</td>";
			}
			else
				$event_content .= "<td class='tabletext' align='center'>- -</td>";
		}
		else
			$event_content .= "<td class='tabletext'>&nbsp;</b>";
	}
	$event_content .= "</tr>";
}

$no_record = 0;
if(trim($event_content)=="")
{
	$event_content .= "<tr>";
	$event_content .= "<td class='tablerow2 tabletext' align='center' colspan='". (sizeof($AgeGroups)+1) ."' ><br />".$i_no_record_searched_msg."<br /><br></td>";
	$event_content .= "</tr>";
	
	$no_record	= 1;
}

### Title ###
$TAGS_OBJ[] = array($Lang['eSports']['IndividualEvent'],"schedule.php", 1);
$TAGS_OBJ[] = array($house_relay_name,"schedule_relay.php", 0);
$TAGS_OBJ[] = array($class_relay_name,"schedule_class_relay.php", 0);
$MODULE_OBJ = $lswimminggala->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();
?>

<? if($sys_custom['eSports']['ExportForElectronicBoard']) { ?>
<div class="Conntent_tool">
<?=$linterface->GET_LNK_EXPORT_IP25("export_electronic_board.php",$Lang['eSports']['ExportForElectronicBoard']);?>
</div>
<p class="spacer"></p>      
<? } ?>

<form name="form1" action="auto_lane_arrange.php" method="post">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td>&nbsp;</td>
        <td align="right"><?=$linterface->GET_SYS_MSG($xmsg);?></td>
</tr>   
<tr>
	<td align="center" colspan="2">
        	<table width="96%" border="0" cellspacing="0" cellpadding="0">
                             
		<tr>
                	<td>
                                <table width="100%" border="0" cellspacing="0" cellpadding="4">
                                <?=$ageGroupTitle?>
                                <?=$event_content?>
                        	</table>
                        </td>
		</tr>
                <tr>
			<td height="1" class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
		</tr>
		<? if(!$no_record && $lswimminggala->isAdminUser($UserID) && !$lswimminggala->HiddenAutoArrangeButton) {?>
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="2">
				<tr>
                                	<td><span class="tabletextremark">(<?=$i_Sports_Explain?>: <?=$i_Sports_Arranged_Student_Count?> / <?=$i_Sports_Enroled_Student_Count?>; - - <?=$i_Sports_Item_Without_Setting_Meaning?>)</span></td>
                                	<td align="right" valign="bottom">
                                        <?= $linterface->GET_BTN($i_Sports_menu_Arrangement_Schedule, "submit", "return confirm('". $Lang['eSports']['AutoArrangeWarning'] ."');","submit2") ?>
                                        </td>
                                </tr>
                                </table>
			</td>
		</tr>
		<? } ?>
		</table>
	</td>
</tr>
</table>      

       
<br />
<input type="hidden" name="EventGroupIDs" value="<?=$eg_ids?>">
<input type="hidden" name="EventGroupTypes" value="<?=$eg_types?>">
</form>

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>
