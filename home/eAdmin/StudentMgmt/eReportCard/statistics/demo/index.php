<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libteaching.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if ($plugin['ReportCard2008']) {
	include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
	if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
	{
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
		$lreportcard = new libreportcardcustom();
	} else {
		$lreportcard = new libreportcard();
	}
	$lteaching = new libteaching();
	$linterface = new interface_html();
	
	if ($lreportcard->hasAccessRight()) {
		$submitBtn = $linterface->GET_ACTION_BTN($button_submit, "submit", "", "submitBtn");
		
		if($ck_ReportCard_UserType=="TEACHER") {
			$FormArrCT = $lreportcard->returnSunjectTeacherForm($UserID);
						
			# check is class teacher or not
			$TeacherClassAry = $lteaching->returnTeacherClassWithLevel($UserID);
			$TeacherClass = $TeacherClassAry[0]['ClassName'];
			
			// Update FormArrCT if user is class teacher
			if(!empty($TeacherClass)) {
				for($i==0;$i<sizeof($TeacherClassAry);$i++) {
					$thisClassLevelID = $TeacherClassLevelAry[$i]['ClassLevelID'];
					$searchResult = multiarray_search($FormArrCT , "ClassLevelID", $thisClassLevelID);
					if($searchResult == "-1") {
						$thisAry = array(
							"0"=>$TeacherClassAry[$i]['ClassLevelID'],
							"ClassLevelID"=>$TeacherClassAry[$i]['ClassLevelID'],
							"1"=>$TeacherClassAry[$i]['LevelName'],
							"LevelName"=>$TeacherClassAry[$i]['LevelName']);
						$FormArrCT = array_merge($FormArrCT, array($thisAry));
					}
				}
				
				# sort $FormArrCT
				foreach ($FormArrCT as $key => $row) {
					$field1[$key] 	= $row['ClassLevelID'];
					$field2[$key]  	= $row['LevelName'];
				}
				array_multisort($field1, SORT_ASC, $field2, SORT_ASC, $FormArrCT);
			}
			
			$ClassLevelID = ($ClassLevelID == "") ? $FormArrCT[0][0] : $ClassLevelID;
			$ClassLevelName = $lreportcard->returnClassLevel($ClassLevelID);
			#############################################
			$ClassArrCT = $lreportcard->returnSubjectTeacherClass($UserID, $ClassLevelID);
			#############################################
			
			$FormSubjectArrCT = $lreportcard->returnSubjectwOrder($ClassLevelID, 1, $UserID);
			
			// Update $FormSubjectArrCT if user is class teacher
			if(!empty($TeacherClass)) {
				$searchResult = multiarray_search($TeacherClassAry , "ClassLevelID", $ClassLevelID);
				if($searchResult != "-1") {
					#############################################
					$searchResult2 = multiarray_search($ClassArrCT , "ClassName", $TeacherClassAry[$searchResult]['ClassName']);
					if($searchResult2 == "-1") {
						$thisAry = array(
							"0"=>$TeacherClassAry[$searchResult]['ClassID'],
							"ClassID"=>$TeacherClassAry[$searchResult]['ClassID'],
							"1"=>$TeacherClassAry[$searchResult]['ClassName'],
							"ClassName"=>$TeacherClassAry[$searchResult]['ClassName'],
							"2"=>$TeacherClassAry[$searchResult]['ClassLevelID'],
							"ClassLevelID"=>$TeacherClassAry[$searchResult]['ClassLevelID']);
						$ClassArrCT = array_merge($ClassArrCT, array($thisAry));
					}
					#############################################
					$FormSubjectArrCT = $lreportcard->returnSubjectwOrder($ClassLevelID, 1);
				}
			}
		}
		
		# Get ClassLevelID (Form) of the reportcard template
		$FormArr = $ck_ReportCard_UserType=="ADMIN" ? $lreportcard->GET_ALL_FORMS(1) : $FormArrCT;
		
		if(count($FormArr) > 0) {
			# Filters - By Form (ClassLevelID)
			$ClassLevelID = ($ClassLevelID == "") ? $FormArr[0][0] : $ClassLevelID;
			$ClassLevelName = $lreportcard->returnClassLevel($ClassLevelID);
			$FormSelection = $linterface->GET_SELECTION_BOX($FormArr, "name='ClassLevelID' class='' onchange='loadReportList(this.options[this.selectedIndex].value)'", "", $ClassLevelID);
			
			# Filters - By Class (ClassID)
			$ClassArr = $ck_ReportCard_UserType=="ADMIN" ? $lreportcard->GET_CLASSES_BY_FORM($ClassLevelID) : $ClassArrCT;
			$allClassesOption = array(0=> array("", $eReportCard['AllClasses']));
			$ClassArr = array_merge($allClassesOption, $ClassArr);
			$ClassID = ($ClassID == "") ? $ClassArr[0][0] : $ClassID;
			$ClassSelection = $linterface->GET_SELECTION_BOX($ClassArr, 'name="ClassID" class="" onchange=""', "", $ClassID);
			
			# Filters - By Report
			$ReportTypeSelection = '';
			$ReportTypeArr = array();
			$ReportTypeOption = array();
			$ReportTypeArr = $lreportcard->GET_REPORT_TYPES($ClassLevelID);
			$ReportTypeOptionCount = 0;
			
			if(count($ReportTypeArr) > 0){
				for($j=0 ; $j<count($ReportTypeArr) ; $j++){
					# for checking the existence of ReportID when changing ClassLevel
					// only display report which have been generated
					$reportTemplateInfo = $lreportcard->returnReportTemplateBasicInfo($ReportTypeArr[$j]['ReportID']);
					if ($reportTemplateInfo["LastGenerated"] != "0000-00-00 00:00:00" && $reportTemplateInfo["LastGenerated"] != "") {
						$ReportIDList[] = $ReportTypeArr[$j]['ReportID'];
						$ReportTypeOption[$ReportTypeOptionCount][0] = $ReportTypeArr[$j]['ReportID'];
						$ReportTypeOption[$ReportTypeOptionCount][1] = $ReportTypeArr[$j]['SemesterTitle'];
						$ReportTypeOptionCount++;
					}
				}
			}
			
			$ReportTypeSelectDisabled = "";
			if (sizeof($ReportTypeOption) == 0) {
				$ReportTypeOption[0][0] = "-1";
				$ReportTypeOption[0][1] = $eReportCard['NoReportAvailable'];
				$ReportTypeSelectDisabled = "disabled='disabled'";
			}
			
			$ReportID = ($ReportID == "" || !in_array($ReportID,$ReportIDList)) ? $ReportTypeOption[0][0] : $ReportID;
			$reportTemplateInfo = $lreportcard->returnReportTemplateBasicInfo($ReportID);
			$ReportTypeSelection = $linterface->GET_SELECTION_BOX($ReportTypeOption, 'name="ReportID" id="ReportID" class="" '.$ReportTypeSelectDisabled.' onchange="loadColumnList(this.options[this.selectedIndex].value)"', '', $ReportID, '');
			
			$ReportIDOrder = "";
			if (sizeof($ReportIDList) > 0) {
				sort($ReportIDList);
				$ReportIDOrder = array_search($ReportID, $ReportIDList);
			}
			
			# Filters - By Report Column
			if ($ReportTypeSelectDisabled == "") {
				$ColumnTitleAry = $lreportcard->returnReportColoumnTitle($ReportID);
				$ColumnOption = array();
				$ColumnOption[0] = array("", $eReportCard['Template']['OverallCombined']);
				$countColumn = 1;
				if (sizeof($ColumnTitleAry) > 0) {
					foreach ($ColumnTitleAry as $columnID => $columnTitle) {
						$ColumnOption[$countColumn][0] = $columnID;
						$ColumnOption[$countColumn][1] = $columnTitle;
						$countColumn++;
					}
				}
			} else {
				// disable Submit button
				$submitBtn = $linterface->GET_ACTION_BTN($button_submit, "submit", "", "submitBtn", "disabled='disabled'", 1);
				
				$ColumnOption[0][0] = "-1";
				$ColumnOption[0][1] = $eReportCard['NoTermAvailable'];
				$ColumnSelectDisabled = 'disabled="disabled"';
			}
			$ColumnSelection = $linterface->GET_SELECTION_BOX($ColumnOption, 'name="ReportColumnID" id="ReportColumnID" '.$ColumnSelectDisabled.' onchange=""', '');
			
			# Filters - By Form Subjects
			// A Flag, called $isFormSubject, is used to identify whether currect Form has that subject or not
			$FormSubjectArr = array();
			$SubjectOption = array();
			$SubjectOption[0] = array("", $eReportCard['AllSubjects']);
			$count = 1;
			$isFormSubject = 0;		// Default: Assume the selected subject does not belongs to that Form(ClassLevelID)
			$SubjectFormGradingArr = array();
		
			// Get Subjects By Form (ClassLevelID)
			$FormSubjectArr = $ck_ReportCard_UserType=="ADMIN" ? $lreportcard->returnSubjectwOrder($ClassLevelID, 1) : $FormSubjectArrCT;		
			if(!empty($FormSubjectArr)){
				foreach($FormSubjectArr as $FormSubjectID => $Data){
					if(is_array($Data)){
						foreach($Data as $FormCmpSubjectID => $SubjectName){							
							$FormSubjectID = ($FormCmpSubjectID==0) ? $FormSubjectID : $FormCmpSubjectID;
							if($FormSubjectID == $SubjectID)
								$isFormSubject = 1;
							$SubjectFormGradingArr = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $FormSubjectID,0,0,$ReportID);
							// Prepare Subject Selection Box 
							if($FormCmpSubjectID==0 && $SubjectFormGradingArr["ScaleInput"]=="M" && $SubjectFormGradingArr["ScaleDisplay"]=="M"){
								$SubjectOption[$count][0] = $FormSubjectID;
								$SubjectOption[$count][1] = $SubjectName;
								$count++;
							}
						}
					}
				}
			}
			
			// Use for Selection Box
			// $SubjectListArr[][0] : SubjectID
			// $SubjectListArr[][1] : SubjectName
			$SubjectID = ($isFormSubject) ? $SubjectID : '';
			if($SubjectID == ""){
				$SubjectID = $SubjectOption[0][0];
			}
			$SelectedSubjectName = $lreportcard->GET_SUBJECT_NAME($SubjectID);
			$SubjectSelection = $linterface->GET_SELECTION_BOX($SubjectOption, 'name="SubjectID" id="SubjectID" class="" onchange=""', '', $SubjectID);
			
			// Alternate sets of subjects & classes, they will be used when User selected a different Class Level
			// They will store as JS array
			$OtherSubjectSelection = array();
			for($i=0; $i<sizeof($FormArr); $i++) {
				$FormSubjectArrCT = $lreportcard->returnSubjectwOrder($FormArr[$i][0], 1, $UserID);
				$ClassArrCT = $lreportcard->returnSubjectTeacherClass($UserID, $FormArr[$i][0]);
				
				if(!empty($TeacherClass)) {
					$searchResult = multiarray_search($TeacherClassAry , "ClassLevelID", $FormArr[$i][0]);
					if($searchResult != "-1") {	
						$searchResult2 = multiarray_search($ClassArrCT , "ClassName", $TeacherClassAry[$searchResult]['ClassName']);
						if($searchResult2 == "-1") {
							$thisAry = array(
								"0"=>$TeacherClassAry[$searchResult]['ClassID'],
								"ClassID"=>$TeacherClassAry[$searchResult]['ClassID'],
								"1"=>$TeacherClassAry[$searchResult]['ClassName'],
								"ClassName"=>$TeacherClassAry[$searchResult]['ClassName'],
								"2"=>$TeacherClassAry[$searchResult]['ClassLevelID'],
								"ClassLevelID"=>$TeacherClassAry[$searchResult]['ClassLevelID']);
							$ClassArrCT = array_merge($ClassArrCT, array($thisAry));
						}
						$FormSubjectArrCT = $lreportcard->returnSubjectwOrder($FormArr[$i][0], 1);
					}
				}
				
				$FormSubjectArr = array();
				$SubjectOption = array();
				$SubjectOption[0] = array("", $eReportCard['AllSubjects']);
				$count = 1;
				$SubjectFormGradingArr = array();
				
				$FormSubjectArr = $ck_ReportCard_UserType=="ADMIN" ? $lreportcard->returnSubjectwOrder($FormArr[$i][0], 1) : $FormSubjectArrCT;
				if(!empty($FormSubjectArr)){
					foreach($FormSubjectArr as $FormSubjectID => $Data){
						if(is_array($Data)){
							foreach($Data as $FormCmpSubjectID => $SubjectName){							
								$FormSubjectID = ($FormCmpSubjectID==0) ? $FormSubjectID : $FormCmpSubjectID;
								
								$SubjectFormGradingArr = $lreportcard->GET_SUBJECT_FORM_GRADING($FormArr[$i][0], $FormSubjectID,0,0,$ReportID);
								// Prepare Subject Selection Box 
								if($FormCmpSubjectID==0 && $SubjectFormGradingArr["ScaleInput"]=="M" && $SubjectFormGradingArr["ScaleDisplay"]=="M"){
									$SubjectOption[$count][0] = $FormSubjectID;
									$SubjectOption[$count][1] = $SubjectName;
									$count++;
								}
							}
						}
					}
				}
				
				$SubjectID = $SubjectOption[0][0];
				$SelectedSubjectName = $lreportcard->GET_SUBJECT_NAME($SubjectID);
				$OtherSubjectSelection[$FormArr[$i][0]] = $linterface->GET_SELECTION_BOX($SubjectOption, 'name="SubjectID" id="SubjectID" class="" onchange=""', '', $SubjectID);	
				
				# Filters - By Class (ClassID)
				$ClassArr = $ck_ReportCard_UserType=="ADMIN" ? $lreportcard->GET_CLASSES_BY_FORM($FormArr[$i][0]) : $ClassArrCT;
				$ClassArr = array_merge($allClassesOption, $ClassArr);
				$ClassID = $ClassArr[0][0];
				$OtherClassSelection[$FormArr[$i][0]] = $linterface->GET_SELECTION_BOX($ClassArr, 'name="ClassID" class="" onchange=""', "", $ClassID);
			}
			
			// Alternate sets of Reports, they will be used when User selected a different Class Level
			// They will store as JS array
			$OtherReportTypeSelection = array();
			
			// default ReportColumn dropdown list for "No Report Available"
			$ColumnOption[0][0] = "-1";
			$ColumnOption[0][1] = $eReportCard['NoTermAvailable'];
			$ColumnSelectDisabled = 'disabled="disabled"';
			$OtherReportColumnSelection["-1"] = $linterface->GET_SELECTION_BOX($ColumnOption, 'name="ReportColumnID" id="ReportColumnID" '.$ColumnSelectDisabled.' onchange=""', '');
			
			for($i=0; $i<sizeof($FormArr); $i++) {
				$ClassLevelName = $lreportcard->returnClassLevel($FormArr[$i][0]);
				$ReportTypeArr = array();
				$ReportTypeOption = array();
				$ReportTypeArr = $lreportcard->GET_REPORT_TYPES($FormArr[$i][0]);
				$ReportTypeOptionCount = 0;
				$ReportIDList = array();
				$ReportIDOrder = 0;
				
				if(count($ReportTypeArr) > 0){
					for($j=0; $j<sizeof($ReportTypeArr); $j++){
						$reportTemplateInfo = $lreportcard->returnReportTemplateBasicInfo($ReportTypeArr[$j]['ReportID']);
						if ($reportTemplateInfo["LastGenerated"] != "0000-00-00 00:00:00" && $reportTemplateInfo["LastGenerated"] != "") {
							$ReportTypeOption[$ReportTypeOptionCount][0] = $ReportTypeArr[$j]['ReportID'];
							$ReportTypeOption[$ReportTypeOptionCount][1] = $ReportTypeArr[$j]['SemesterTitle'];
							$ReportTypeOptionCount++;
							
							$ReportIDList[] = $ReportTypeArr[$j]['ReportID'];
						}
					}
					
					sort($ReportIDList);
					
					for($j=0; $j<sizeof($ReportTypeArr); $j++){
						$reportTemplateInfo = $lreportcard->returnReportTemplateBasicInfo($ReportTypeArr[$j]['ReportID']);
						$ReportIDOrder = array_search($ReportTypeArr[$j]['ReportID'], $ReportIDList);
						if ($reportTemplateInfo["LastGenerated"] != "0000-00-00 00:00:00" && $reportTemplateInfo["LastGenerated"] != "") {		
							// Alternate sets of ReportColumn (Assessment, Term or Whole year)
							$ColumnTitleAry = $lreportcard->returnReportColoumnTitle($ReportTypeArr[$j]['ReportID']);
							$ColumnOption = array();
							$ColumnOption[0] = array("", $eReportCard['Template']['OverallCombined']);
							$countColumn = 1;
							if (sizeof($ColumnTitleAry) > 0) {
								foreach ($ColumnTitleAry as $columnID => $columnTitle) {
									$ColumnOption[$countColumn][0] = $columnID;
									$ColumnOption[$countColumn][1] = $columnTitle;
									$countColumn++;
								}
							}
							$OtherReportColumnSelection[$ReportTypeArr[$j]['ReportID']] = $linterface->GET_SELECTION_BOX($ColumnOption, 'name="ReportColumnID" id="ReportColumnID" class="" onchange=""', '');
						}
					}
				}
				
				$ReportTypeSelectDisabled = "";
				if (sizeof($ReportTypeOption) == 0) {
					$ReportTypeOption[0][0] = "-1";
					$ReportTypeOption[0][1] = $eReportCard['NoReportAvailable'];
					$ReportTypeSelectDisabled = 'disabled="disabled"';
				}
				
				$OtherReportTypeSelection[$FormArr[$i][0]] = $linterface->GET_SELECTION_BOX($ReportTypeOption, 'name="ReportID" id="ReportID" '.$ReportTypeSelectDisabled.' onchange="loadColumnList(this.options[this.selectedIndex].value)"', '');
			}
		}
		
		############## Interface Start ##############
		$linterface = new interface_html();
		$CurrentPage = "Statistics";
		$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
		# tag information
		$TAGS_OBJ[] = array($eReportCard['Statistics']);
		# page navigation (leave the array empty if no need)
		$PAGE_NAVIGATION[] = array($eReportCard['SelectCriteria']);
		$linterface->LAYOUT_START();
?>

<script type="text/JavaScript" language="JavaScript">
// Preloaded sets of Report list
var classLevelReportList = new Array();
var classLevelSubjectList = new Array();
var classLevelReportColunmList = new Array();
var classLevelClassList = new Array();

<?php
if(count($FormArr) > 0 && count($ReportTypeArr) > 0) {
	foreach($OtherReportTypeSelection as $key => $value) {
		$value = str_replace("\n", "", $value);
		$value = str_replace("'", "\'", $value);
		$x .= "classLevelReportList[".$key."] = '$value';\n";
	}
	echo $x;
	
	foreach($OtherSubjectSelection as $key => $value) {
		$value = str_replace("\n", "", $value);
		$value = str_replace("'", "\'", $value);
		$y .= "classLevelSubjectList[".$key."] = '$value';\n";
	}
	echo $y;
	
	foreach($OtherReportColumnSelection as $key => $value) {
		$value = str_replace("\n", "", $value);
		$value = str_replace("'", "\'", $value);
		$z .= "classLevelReportColunmList[".$key."] = '$value';\n";
	}
	echo $z;
	
	foreach($OtherClassSelection as $key => $value) {
		$value = str_replace("\n", "", $value);
		$value = str_replace("'", "\'", $value);
		$w .= "classLevelClassList[".$key."] = '$value';\n";
	}
	echo $w;
}
?>

function loadReportList(classLevelID) {
	document.getElementById("ReportList").innerHTML = classLevelReportList[classLevelID];
	document.getElementById("SubjectList").innerHTML = classLevelSubjectList[classLevelID];
	document.getElementById("ClassList").innerHTML = classLevelClassList[classLevelID];
	
	var reportIDSelect = document.getElementById("ReportID");
	loadColumnList(reportIDSelect.options[reportIDSelect.selectedIndex].value);
	
	if (reportIDSelect.disabled) {
		document.getElementById("submitBtn").disabled = true;
		document.getElementById("submitBtn").className = 'formbutton_disable';
	} else {
		document.getElementById("submitBtn").disabled = false;
		document.getElementById("submitBtn").className = 'formbutton';
	}
}

function loadColumnList(reportID) {
	document.getElementById("ColumnList").innerHTML = classLevelReportColunmList[reportID];
}

function checkNoAllSubject() {
	return true;
}

function resetForm() {
	document.form1.reset();
	return true;
}

function checkForm() {
	return true;
}


</script>


		<script type="text/javascript" src="swfobject.js"></script>
        <script type="text/javascript">
            <!-- For version detection, set to min. required Flash Player version, or 0 (or 0.0.0), for no version detection. --> 
            var swfVersionStr = "10.0.0";
            <!-- To use express install, set to playerProductInstall.swf, otherwise the empty string. -->
            var xiSwfUrlStr = "playerProductInstall.swf";
            var flashvars = {};
            var params = {};
            params.quality = "high";
            params.bgcolor = "#ffffff";
            params.allowscriptaccess = "sameDomain";
            params.allowfullscreen = "true";
            var attributes = {};
            attributes.id = "eStatistica";
            attributes.name = "eStatistica";
            attributes.align = "middle";
            swfobject.embedSWF(
                "eStatistica.swf", "flashContent", 
                "100%", "700px", 
                swfVersionStr, xiSwfUrlStr, 
                flashvars, params, attributes);
			<!-- JavaScript enabled so display the flashContent div in case it is not replaced with a swf object. -->
			swfobject.createCSS("#flashContent", "display:block;text-align:left;");
        </script>
<link type="text/css" rel="stylesheet" href="<?=$PATH_WRT_ROOT."templates/2007a/css/"?>ereportcard.css">
	    <style type="text/css" media="screen"> 
			html, body	{ height:800px; }
			body { margin:0; padding:0; overflow:auto; text-align:center; 
			       background-color: #ffffff; }   
			#flashContent { display:none; }
        </style>
<br />

        <div id="flashContent">
        	<p>
	        	To view this page ensure that Adobe Flash Player version 
				10.0.0 or greater is installed. 
			</p>
			<script type="text/javascript"> 
				var pageHost = ((document.location.protocol == "https:") ? "https://" :	"http://"); 
				document.write("<a href='http://www.adobe.com/go/getflashplayer'><img src='" 
								+ pageHost + "www.adobe.com/images/shared/download_buttons/get_flash_player.gif' alt='Get Adobe Flash player' /></a>" ); 
			</script> 
        </div>
	   	
       	<noscript>
            <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="100%" height="700px" id="main">
                <param name="movie" value="eStatistica.swf" />
                <param name="quality" value="high" />
                <param name="bgcolor" value="#ffffff" />
                <param name="allowScriptAccess" value="sameDomain" />
                <param name="allowFullScreen" value="true" />
                <!--[if !IE]>-->
                <object type="application/x-shockwave-flash" data="eStatistica.swf" width="100%" height="700px">
                    <param name="quality" value="high" />
                    <param name="bgcolor" value="#ffffff" />
                    <param name="allowScriptAccess" value="sameDomain" />
                    <param name="allowFullScreen" value="true" />
                <!--<![endif]-->
                <!--[if gte IE 6]>-->
                	<p> 
                		Either scripts and active content are not permitted to run or Adobe Flash Player version
                		10.0.0 or greater is not installed.
                	</p>
                <!--<![endif]-->
                    <a href="http://www.adobe.com/go/getflashplayer">
                        <img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash Player" />
                    </a>
                <!--[if !IE]>-->
                </object>
                <!--<![endif]-->
            </object>
	    </noscript>		

<?
	print $linterface->FOCUS_ON_LOAD("form1.ClassLevelID");
  	$linterface->LAYOUT_STOP();
  } else {
?>
You have no priviledge to access this page.
<?
	}
} else {
?>
You have no priviledge to access this page.
<?
}
?>
