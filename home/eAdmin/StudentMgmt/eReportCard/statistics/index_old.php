<?php
$PATH_WRT_ROOT = "../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libteaching.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if ($plugin['ReportCard']) {
	include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
	include_once($PATH_WRT_ROOT."includes/libreportcard2008w.php");
	
	$lreportcard = new libreportcard2008w();
	$lteaching = new libteaching();
	$linterface = new interface_html();
	
	if ($lreportcard->hasAccessRight()) {
		$submitBtn = $linterface->GET_ACTION_BTN($button_submit, "submit", "", "submitBtn");
		
		if($ck_ReportCard_UserType=="TEACHER") {
			$FormArrCT = $lreportcard->returnSunjectTeacherForm($UserID);
			
			# check is class teacher or not
			$TeacherClassAry = $lteaching->returnTeacherClassWithLevel($UserID);
			$TeacherClass = $TeacherClassAry[0]['ClassName'];
			
			// Update FormArrCT if user is class teacher
			if(!empty($TeacherClass)) {
				for($i==0;$i<sizeof($TeacherClassAry);$i++) {
					$thisClassLevelID = $TeacherClassLevelAry[$i]['ClassLevelID'];
					$searchResult = multiarray_search($FormArrCT , "ClassLevelID", $thisClassLevelID);
					if($searchResult == "-1") {
						$thisAry = array(
							"0"=>$TeacherClassAry[$i]['ClassLevelID'],
							"ClassLevelID"=>$TeacherClassAry[$i]['ClassLevelID'],
							"1"=>$TeacherClassAry[$i]['LevelName'],
							"LevelName"=>$TeacherClassAry[$i]['LevelName']);
						$FormArrCT = array_merge($FormArrCT, array($thisAry));
					}
				}
				
				# sort $FormArrCT
				foreach ($FormArrCT as $key => $row) {
					$field1[$key] 	= $row['ClassLevelID'];
					$field2[$key]  	= $row['LevelName'];
				}
				array_multisort($field1, SORT_ASC, $field2, SORT_ASC, $FormArrCT);
			}
			
			$ClassLevelID = ($ClassLevelID == "") ? $FormArrCT[0][0] : $ClassLevelID;
		}
		
		# Get ClassLevelID (Form) of the reportcard template
		$FormArr = $ck_ReportCard_UserType=="ADMIN" ? $lreportcard->GET_ALL_FORMS(1) : $FormArrCT;
		
		if(count($FormArr) > 0) {
			# Filters - By Form (ClassLevelID)
			$ClassLevelID = ($ClassLevelID == "") ? $FormArr[0][0] : $ClassLevelID;
			$ClassLevelName = $lreportcard->returnClassLevel($ClassLevelID);
			$FormSelection = $linterface->GET_SELECTION_BOX($FormArr, "name='ClassLevelID' class='' onchange='loadReportList(this.options[this.selectedIndex].value)'", "", $ClassLevelID);
			
			# Filters - By Report
			$ReportTypeSelection = '';
			$ReportTypeArr = array();
			$ReportTypeOption = array();
			$ReportTypeArr = $lreportcard->GET_REPORT_TYPES($ClassLevelID);
			$ReportTypeOptionCount = 0;
			
			if(count($ReportTypeArr) > 0){
				for($j=0 ; $j<count($ReportTypeArr) ; $j++){
					# for checking the existence of ReportID when changing ClassLevel
					// only display report which have been generated
					$reportTemplateInfo = $lreportcard->returnReportTemplateBasicInfo($ReportTypeArr[$j]['ReportID']);
					if ($reportTemplateInfo["LastGenerated"] != "0000-00-00 00:00:00" && $reportTemplateInfo["LastGenerated"] != "") {
						$ReportIDList[] = $ReportTypeArr[$j]['ReportID'];
						$ReportTypeOption[$ReportTypeOptionCount][0] = $ReportTypeArr[$j]['ReportID'];
						$ReportTypeOption[$ReportTypeOptionCount][1] = $ReportTypeArr[$j]['SemesterTitle'];
						$ReportTypeOptionCount++;
					}
				}
			}
			
			$ReportTypeSelectDisabled = "";
			if (sizeof($ReportTypeOption) == 0) {
				$ReportTypeOption[0][0] = "-1";
				$ReportTypeOption[0][1] = $eReportCard['NoReportAvailable'];
				$ReportTypeSelectDisabled = "disabled='disabled'";
			}
			
			$ReportID = ($ReportID == "" || !in_array($ReportID,$ReportIDList)) ? $ReportTypeOption[0][0] : $ReportID;
			$reportTemplateInfo = $lreportcard->returnReportTemplateBasicInfo($ReportID);
			$ReportTypeSelection = $linterface->GET_SELECTION_BOX($ReportTypeOption, 'name="ReportID" id="ReportID" class="" '.$ReportTypeSelectDisabled.' onchange="loadColumnList(this.options[this.selectedIndex].value)"', '', $ReportID, '');
			
			$ReportIDOrder = "";
			if (sizeof($ReportIDList) > 0) {
				sort($ReportIDList);
				$ReportIDOrder = array_search($ReportID, $ReportIDList);
			}
			
			# Filters - By Report Column
			if ($ReportTypeSelectDisabled == "") {
				$ColumnTitleAry = $lreportcard->returnReportColoumnTitle($ReportID);
				################## Customization for SIS ##################
				/*
				# if it is secondary school
				if (strtolower(substr($ClassLevelName, 0, 1)) == "s") {
					# Year-End report
					if ($reportTemplateInfo["Semester"] == "F" && sizeof($ColumnTitleAry) == 4) {
						$ColumnOption[0] = array("", $eReportCard['Template']['OverallCombined']);
						//$ColumnOption[1] = array("first", $eReportCard['Template']['FirstCombined']);
						//$ColumnOption[1] = array("second", $eReportCard['Template']['SecondCombined']);
					# Mid-term report
					} else {
						if ($ReportIDOrder == 0) $ColumnOption[0] = array("first", $eReportCard['Template']['FirstCombined']);
						if ($ReportIDOrder == 1) $ColumnOption[0] = array("second", $eReportCard['Template']['SecondCombined']);
					}
				# if it is primary school
				} else if (strtolower(substr($ClassLevelName, 0, 1)) == "p") {
					# Year-End report
					if ($reportTemplateInfo["Semester"] == "F") {
						if (strtolower(substr($ClassLevelName, 1, 1)) == "1" || strtolower(substr($ClassLevelName, 1, 1)) == "2") {
							$ColumnOption[0] = array("", $eReportCard['Template']['OverallCombined']);
						} else {
							$ColumnOption[0] = array("", $eReportCard['Template']['OverallCombined']);
							$countColumn = 1;
							foreach ($ColumnTitleAry as $columnID => $columnTitle) {
								if (sizeof($ColumnTitleAry) != 1 && $countColumn == 2) {
									$ColumnOption[1][0] = $columnID;
									$ColumnOption[1][1] = "SA2";
									break;
								}
								$ColumnOption[$countColumn][0] = $columnID;
								$ColumnOption[$countColumn][1] = $columnTitle;
								if ($countColumn == 1) $ColumnOption[$countColumn][1] = "SA1";
								if ($countColumn == 2) $ColumnOption[$countColumn][1] = "SA2";
								$countColumn++;
							}
						}
					# Mid-term report
					} else {
						$ColumnOption[0] = array("", "SA1");
					}
				} else {
					
				}
				*/
				$ColumnOption[0] = array("", $eReportCard['Template']['OverallCombined']);
				$countColumn = 1;
				foreach ($ColumnTitleAry as $columnID => $columnTitle) {
					$ColumnOption[$countColumn][0] = $columnID;
					$ColumnOption[$countColumn][1] = $columnTitle;
					$countColumn++;
				}
			} else {
				// disable Submit button
				$submitBtn = $linterface->GET_ACTION_BTN($button_submit, "submit", "", "submitBtn", "disabled='disabled'");
				
				$ColumnOption[0][0] = "-1";
				$ColumnOption[0][1] = $eReportCard['NoTermAvailable'];
				$ColumnSelectDisabled = 'disabled="disabled"';
			}
			$ReportColumnID = ($ReportColumnID == "") ? $ColumnOption[0][0] : $ReportColumnID;
			$ColumnSelection = $linterface->GET_SELECTION_BOX($ColumnOption, 'name="ReportColumnID" id="ReportColumnID" '.$ColumnSelectDisabled.' onchange=""', '');
			
			// Alternate sets of Reports, they will be used when User selected a different Class Level
			// They will store as JS array
			$OtherReportTypeSelection = array();
			
			// default ReportColumn dropdown list for "No Report Available"
			$ColumnOption[0][0] = "-1";
			$ColumnOption[0][1] = $eReportCard['NoTermAvailable'];
			$ColumnSelectDisabled = 'disabled="disabled"';
			$OtherReportColumnSelection["-1"] = $linterface->GET_SELECTION_BOX($ColumnOption, 'name="ReportColumnID" id="ReportColumnID" '.$ColumnSelectDisabled.' onchange=""', '');
			
			for($i=0; $i<sizeof($FormArr); $i++) {
				$ClassLevelName = $lreportcard->returnClassLevel($FormArr[$i][0]);
				$ReportTypeArr = array();
				$ReportTypeOption = array();
				$ReportTypeArr = $lreportcard->GET_REPORT_TYPES($FormArr[$i][0]);
				$ReportTypeOptionCount = 0;
				$ReportIDList = array();
				$ReportIDOrder = 0;
				if(count($ReportTypeArr) > 0){
					for($j=0; $j<sizeof($ReportTypeArr); $j++){
						$reportTemplateInfo = $lreportcard->returnReportTemplateBasicInfo($ReportTypeArr[$j]['ReportID']);
						if ($reportTemplateInfo["LastGenerated"] != "0000-00-00 00:00:00" && $reportTemplateInfo["LastGenerated"] != "") {
							$ReportTypeOption[$ReportTypeOptionCount][0] = $ReportTypeArr[$j]['ReportID'];
							$ReportTypeOption[$ReportTypeOptionCount][1] = $ReportTypeArr[$j]['SemesterTitle'];
							$ReportTypeOptionCount++;
							
							$ReportIDList[] = $ReportTypeArr[$j]['ReportID'];
						}
					}
					
					sort($ReportIDList);
					
					for($j=0; $j<sizeof($ReportTypeArr); $j++){
						$reportTemplateInfo = $lreportcard->returnReportTemplateBasicInfo($ReportTypeArr[$j]['ReportID']);
						$ReportIDOrder = array_search($ReportTypeArr[$j]['ReportID'], $ReportIDList);
						if ($reportTemplateInfo["LastGenerated"] != "0000-00-00 00:00:00" && $reportTemplateInfo["LastGenerated"] != "") {
							// Alternate sets of ReportColumn (Assessment, Term or Whole year)
							$ColumnTitleAry = $lreportcard->returnReportColoumnTitle($ReportTypeArr[$j]['ReportID']);
							$ColumnOption = array();
							################## Customization for SIS ##################
							/*
							# if it is secondary school
							if (strtolower(substr($ClassLevelName, 0, 1)) == "s") {
								# Year-End report
								if ($reportTemplateInfo["Semester"] == "F" && sizeof($ColumnTitleAry) == 4) {
									$ColumnOption[0] = array("", $eReportCard['Template']['OverallCombined']);
									//$ColumnOption[1] = array("first", $eReportCard['Template']['FirstCombined']);
									//$ColumnOption[1] = array("second", $eReportCard['Template']['SecondCombined']);
								# Mid-term report
								} else {
									if ($ReportIDOrder == 0) $ColumnOption[0] = array("first", $eReportCard['Template']['FirstCombined']);
									if ($ReportIDOrder == 1) $ColumnOption[0] = array("second", $eReportCard['Template']['SecondCombined']);
								}
							# if it is primary school
							} else if (strtolower(substr($ClassLevelName, 0, 1)) == "p") {
								# Year-End report
								if ($reportTemplateInfo["Semester"] == "F") {
									if (strtolower(substr($ClassLevelName, 1, 1)) == "1" || strtolower(substr($ClassLevelName, 1, 1)) == "2") {
										$ColumnOption[0] = array("", $eReportCard['Template']['OverallCombined']);
									} else {
										$ColumnOption[0] = array("", $eReportCard['Template']['OverallCombined']);
										
										$countColumn = 1;
										foreach ($ColumnTitleAry as $columnID => $columnTitle) {
											if (sizeof($ColumnTitleAry) != 1 && $countColumn == 2) {
												$ColumnOption[1][0] = $columnID;
												$ColumnOption[1][1] = "SA2";
												break;
											}
											$ColumnOption[$countColumn][0] = $columnID;
											$ColumnOption[$countColumn][1] = $columnTitle;
											if ($countColumn == 1) $ColumnOption[$countColumn][1] = "SA1";
											if ($countColumn == 2) $ColumnOption[$countColumn][1] = "SA2";
											$countColumn++;
										}
									}
								# Mid-term report
								} else {
									$ColumnOption[0] = array("", "SA1");
								}
							} else {
								
							}
							*/
							$ColumnOption[0] = array("", $eReportCard['Template']['OverallCombined']);
							$countColumn = 1;
							foreach ($ColumnTitleAry as $columnID => $columnTitle) {
								$ColumnOption[$countColumn][0] = $columnID;
								$ColumnOption[$countColumn][1] = $columnTitle;
								$countColumn++;
							}
							
							$OtherReportColumnSelection[$ReportTypeArr[$j]['ReportID']] = $linterface->GET_SELECTION_BOX($ColumnOption, 'name="ReportColumnID" id="ReportColumnID" onchange=""', '');
						}
					}
				}
				
				$ReportTypeSelectDisabled = "";
				if (sizeof($ReportTypeOption) == 0) {
					$ReportTypeOption[0][0] = "-1";
					$ReportTypeOption[0][1] = $eReportCard['NoReportAvailable'];
					$ReportTypeSelectDisabled = 'disabled="disabled"';
				}
				
				$OtherReportTypeSelection[$FormArr[$i][0]] = $linterface->GET_SELECTION_BOX($ReportTypeOption, 'name="ReportID" id="ReportID" '.$ReportTypeSelectDisabled.' onchange="loadColumnList(this.options[this.selectedIndex].value)"', '');
			}
		}
		############## Interface Start ##############
		$linterface = new interface_html();
		$CurrentPage = "Statistics";
		$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
		# tag information
		$TAGS_OBJ[] = array($eReportCard['Statistics']);
		# page navigation (leave the array empty if no need)
		$PAGE_NAVIGATION[] = array($eReportCard['SelectCriteria']);
		$linterface->LAYOUT_START();
?>
<script type="text/JavaScript" language="JavaScript">
// Preloaded sets of Report list
var classLevelReportList = new Array();
var classLevelReportColunmList = new Array();

<?php
	foreach($OtherReportTypeSelection as $key => $value) {
		$value = str_replace("\n", "", $value);
		$x .= "classLevelReportList[".$key."] = '$value';\n";
	}
	echo $x;
	
	foreach($OtherReportColumnSelection as $key => $value) {
		$value = str_replace("\n", "", $value);
		$z .= "classLevelReportColunmList[".$key."] = '$value';\n";
	}
	echo $z;
?>

function loadReportList(classLevelID) {
	document.getElementById("ReportList").innerHTML = classLevelReportList[classLevelID];
	
	var reportIDSelect = document.getElementById("ReportID");
	loadColumnList(reportIDSelect.options[reportIDSelect.selectedIndex].value);
	
	if (reportIDSelect.disabled) {
		document.getElementById("submitBtn").disabled = true;
	} else {
		document.getElementById("submitBtn").disabled = false;
	}
}

function loadColumnList(reportID) {
	document.getElementById("ColumnList").innerHTML = classLevelReportColunmList[reportID];
}

function IsNumeric(sText) {
	var ValidChars = "0123456789.";
	var IsNumber=true;
	var Char;
	
	for (i = 0; i < sText.length && IsNumber == true; i++) {
		Char = sText.charAt(i); 
		if (ValidChars.indexOf(Char) == -1) {
			IsNumber = false;
		}
	}
	return IsNumber;
}

function resetForm() {
	document.form1.reset();
	return true;
}

function checkForm() {
	//obj = document.form1;
	//obj.submit();
	return true;
}
</script>
<link type="text/css" rel="stylesheet" href="<?=$PATH_WRT_ROOT."templates/2007a/css/"?>ereportcard.css">
<br />
<form name="form1" action="generate.php" method="POST" onsubmit="return checkForm();" target="_blank">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	</tr>
<table>
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>
			<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr>
					<td></td>
					<td align="right"><?=$linterface->GET_SYS_MSG($Result);?></td>
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?= $eReportCard['Form'] ?>
					</td>
					<td width="75%" class='tabletext'>
						<?= $FormSelection ?>
					</td>
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?= $eReportCard['ReportCard'] ?>
					</td>
					<td width="75%" class='tabletext'>
						<span id="ReportList"><?= $ReportTypeSelection ?></span>
					</td>
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?= $eReportCard['Term'] ?>
					</td>
					<td width="75%" class='tabletext'>
						<span id="ColumnList"><?= $ColumnSelection ?></span>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr>
					<td class="dotline">
						<img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1">
					</td>
				</tr>
				<tr>
					<td align="center">
						<?= $submitBtn ?>
						<?= #$linterface->GET_ACTION_BTN($button_reset, "reset", "")?>
						<?= #$linterface->GET_ACTION_BTN($button_cancel, "button", "window.location = 'schedule.php'")?>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<input type="hidden" name="SecReportType" id="SecReportType" value="<?=$ReportColumnID?>" />
</form>
<?
	print $linterface->FOCUS_ON_LOAD("form1.ClassLevelID");
  	$linterface->LAYOUT_STOP();
  } else {
?>
You have no priviledge to access this page.
<?
	}
} else {
?>
You have no priviledge to access this page.
<?
}
?>
