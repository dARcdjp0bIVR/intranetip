<?php
######################################################
# Generate Statistic Report for Primary Class Level 5-6
# 	Trigger from: index.php
# 	Required variables: $ClassLevelID, $ReportID
# 	Optional variables: $ReportColumnID
#
# 	Last updated: Andy Chan on 2008/6/3
######################################################

function returnGrandMSBand($Marks='-1') {
	if($Marks == -1)	return array("A*","A","B","C","D","E","U");
	if($Marks >= 91)	return "A*";
	if($Marks >= 75)	return "A";
	if($Marks >= 60)	return "B";
	if($Marks >= 50)	return "C";
	if($Marks >= 35)	return "D";
	if($Marks >= 20)	return "E";
	return "U";
}

function calculatePercent($Numerator, $Denumerator) {
	return number_format(round(($Numerator/$Denumerator*100) ,1) ,1);
}

// Root path
$PATH_WRT_ROOT = "../../../../../";

// Page access right
$PageRight = "ADMIN";

// Include general libraries
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
#include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

// Authorize intranet user & connect Database
intranet_auth();
intranet_opendb();

// Check if ReportCard module is enable
if ($plugin['ReportCard']) {
	// Include ReportCard libraries
	include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
	include_once($PATH_WRT_ROOT."includes/libreportcard2008w.php");
	
	// Create objects
	$lreportcard = new libreportcard2008w();
	$linterface = new interface_html();
	$lclass = new libclass();
	
	// Check module access right
	if ($lreportcard->hasAccessRight()) {
		// Check required data submitted from index.php
		if (!isset($ClassLevelID, $ReportID)) {
			intranet_closedb();
			header("Location: index.php");
			exit();
		}
		
		// Get ReportCard info
		$reportTemplateInfo = $lreportcard->returnReportTemplateBasicInfo($ReportID);
		
		// Check to make sure it is a whole year & generated report
		if ($reportTemplateInfo["LastGenerated"] == "0000-00-00 00:00:00" || $reportTemplateInfo["LastGenerated"] == "") {
			intranet_closedb();
			header("Location: index.php");
			exit();
		}
		
		// Get subjects of the current ClassLevel
		$FormSubjectArr = $lreportcard->returnSubjectwOrder($ClassLevelID, 1);
		
		// Get subjects' grading scheme of the current ClassLevel
		$FormSubjectGradingSchemeArr = $lreportcard->GET_FROM_SUBJECT_GRADING_SCHEME($ClassLevelID,0,$ReportID);
		
		// Get ClassLevel name
		$ClassLevelName = $lreportcard->returnClassLevel($ClassLevelID);
		
		// Get Classes
		$ClassArr = $lreportcard->GET_CLASSES_BY_FORM($ClassLevelID);
		
		// Get current school year
		$SchoolYear = $lreportcard->schoolYear;
		
		// Get columns info of the current ReportCard
		$ColumnTitleAry = $lreportcard->returnReportColoumnTitle($ReportID);
		if (is_array($ColumnTitleAry))
		{
			$ColumnIDList = array_keys($ColumnTitleAry);
		}
		else
		{
			$ColumnIDList = array();
		}
		
		// Check if ReportColumnID have value
		// 	if yes, only term mark is used
		// 	if no, overall marks of all terms are used
		if (isset($ReportColumnID) && $ReportColumnID != "") {
			// Hardcoded Column Name since names in $ColumnTitleAry are not exactly "SA1" & "SA2"
			if ($ReportColumnID == $ColumnIDList[0])	$DisplayColumnName = "SA1";
			if ($ReportColumnID == $ColumnIDList[1])	$DisplayColumnName = "SA2";
			
			$StatReportTitle = "Summary of ".$DisplayColumnName." Results by Subject (Primary)";
		} else {
			$StatReportTitle = "Summary of Overall Results by Subject (Primary)";
		}
		
		$display = "";
		
		// Main table generaeting logic
		// Loop 1: Subjects
		foreach ($FormSubjectArr as $SubjectID => $SubjectName) {
			if ($FormSubjectGradingSchemeArr[$SubjectID]["scaleInput"] != "M")
				continue;
			
			$display .= "<tr><td class='SubjectName'>".str_replace(" ()", "", $SubjectName[0])."</td><td colspan='20'></td></tr>";
			
			// Init variables
			$AbsNumber = array();
			$ExNumber = array();
			$BandNumber = array();
			$BandPercent = array();
			$NumberOfClassStudent = array();
			$ClassStudentTotalmark = array();
			$ClassStudentAverageMark =  array();
			
			$ClassLevelAbsNumber = 0;
			$ClassLevelExNumber = 0;
			$NumberOfClassLevelStudent = 0;
			$ClassLevelBandNumber = array();
			$ClassLevelBandPercent = array();
			$ClassLevelStudentTotalMark = 0;
			$ClassLevelStudentAverageMark = 0;
			
			// Loop 2: Classes
			for($i=0; $i<sizeof($ClassArr); $i++) {
				// Construct StudentIDList in the class
				$ClassStudentArr = $lreportcard->GET_STUDENT_BY_CLASS($ClassArr[$i]["ClassID"]);
				$ClassStudentIDList = array();
				for($j=0; $j<sizeof($ClassStudentArr); $j++) {
					$ClassStudentIDList[] = $ClassStudentArr[$j]["UserID"];
				}
				$ClassStudentIDListQuery = implode("','", $ClassStudentIDList);
				$ClassStudentIDListQuery = "'".$ClassStudentIDListQuery."'";
				
				// Query the consolidated marks of the Class
				$table = $lreportcard->DBName.".RC_REPORT_RESULT_SCORE";
				$sql = "SELECT Mark, Grade FROM $table ";
				$sql .= "WHERE ReportID = '$ReportID' AND SubjectID = '$SubjectID' AND StudentID IN ($ClassStudentIDListQuery) AND ";
				if (isset($ReportColumnID) && $ReportColumnID != "") {
					$sql .= "ReportColumnID = '$ReportColumnID'";
				} else {
					$sql .= "(ReportColumnID = 0 OR ReportColumnID IS NULL)";
				}
				$ClassResult = $lreportcard->returnArray($sql, 2);
				
				$NumberOfClassStudent[$ClassArr[$i]["ClassID"]] = sizeof($ClassResult);
				
				// Init variables
				$AbsNumber[$ClassArr[$i]["ClassID"]] = 0;
				$ExNumber[$ClassArr[$i]["ClassID"]] = 0;
				
				$BandNumber["A*"][$ClassArr[$i]["ClassID"]] = 0;
				$BandNumber["A"][$ClassArr[$i]["ClassID"]] = 0;
				$BandNumber["B"][$ClassArr[$i]["ClassID"]] = 0;
				$BandNumber["C"][$ClassArr[$i]["ClassID"]] = 0;
				$BandNumber["D"][$ClassArr[$i]["ClassID"]] = 0;
				$BandNumber["E"][$ClassArr[$i]["ClassID"]] = 0;
				$BandNumber["U"][$ClassArr[$i]["ClassID"]] = 0;
				$BandNumber["EU"][$ClassArr[$i]["ClassID"]] = 0;
				$BandNumber["A*-A"][$ClassArr[$i]["ClassID"]] = 0;
				$BandNumber["A*-C"][$ClassArr[$i]["ClassID"]] = 0;
				
				$BandPercent["A*"][$ClassArr[$i]["ClassID"]] = 0;
				$BandPercent["A"][$ClassArr[$i]["ClassID"]] = 0;
				$BandPercent["B"][$ClassArr[$i]["ClassID"]] = 0;
				$BandPercent["C"][$ClassArr[$i]["ClassID"]] = 0;
				$BandPercent["D"][$ClassArr[$i]["ClassID"]] = 0;
				$BandPercent["E"][$ClassArr[$i]["ClassID"]] = 0;
				$BandPercent["U"][$ClassArr[$i]["ClassID"]] = 0;
				$BandPercent["EU"][$ClassArr[$i]["ClassID"]] = 0;
				$BandPercent["A*-A"][$ClassArr[$i]["ClassID"]] = 0;
				$BandPercent["A*-C"][$ClassArr[$i]["ClassID"]] = 0;
				
				$ClassStudentTotalmark[$ClassArr[$i]["ClassID"]] = 0;
				$ClassStudentAverageMark[$ClassArr[$i]["ClassID"]] = 0;
				
				// Loop the marks and group into different categories
				for($j=0; $j<sizeof($ClassResult); $j++) {
					$StudentBand = "";
					if ($ClassResult[$j]["Mark"] != "" && $ClassResult[$j]["Grade"] == "") {
						$ClassStudentTotalmark[$ClassArr[$i]["ClassID"]] += $ClassResult[$j]["Mark"];
						$StudentBand = returnGrandMSBand($ClassResult[$j]["Mark"]);
						if (!is_array($StudentBand))
							$BandNumber[$StudentBand][$ClassArr[$i]["ClassID"]]++;
					} else if ($ClassResult[$j]["Grade"] == "-") {
						$AbsNumber[$ClassArr[$i]["ClassID"]]++;
					} else if ($ClassResult[$j]["Grade"] == "/") {
						$ExNumber[$ClassArr[$i]["ClassID"]]++;
					}
				}
				
				$BandNumber["EU"][$ClassArr[$i]["ClassID"]] = $BandNumber["E"][$ClassArr[$i]["ClassID"]]+$BandNumber["U"][$ClassArr[$i]["ClassID"]];
				$BandNumber["A*-A"][$ClassArr[$i]["ClassID"]] = $BandNumber["A*"][$ClassArr[$i]["ClassID"]]+$BandNumber["A"][$ClassArr[$i]["ClassID"]];
				$BandNumber["A*-C"][$ClassArr[$i]["ClassID"]] = $BandNumber["A*"][$ClassArr[$i]["ClassID"]]+$BandNumber["A"][$ClassArr[$i]["ClassID"]]+$BandNumber["B"][$ClassArr[$i]["ClassID"]]+$BandNumber["C"][$ClassArr[$i]["ClassID"]];
				
				// Prevent divide by Zero
				if ($NumberOfClassStudent[$ClassArr[$i]["ClassID"]] != 0) {
					$BandPercent["A*"][$ClassArr[$i]["ClassID"]] = calculatePercent($BandNumber["A*"][$ClassArr[$i]["ClassID"]], $NumberOfClassStudent[$ClassArr[$i]["ClassID"]]);
					$BandPercent["A"][$ClassArr[$i]["ClassID"]] = calculatePercent($BandNumber["A"][$ClassArr[$i]["ClassID"]], $NumberOfClassStudent[$ClassArr[$i]["ClassID"]]);
					$BandPercent["B"][$ClassArr[$i]["ClassID"]] = calculatePercent($BandNumber["B"][$ClassArr[$i]["ClassID"]], $NumberOfClassStudent[$ClassArr[$i]["ClassID"]]);
					$BandPercent["C"][$ClassArr[$i]["ClassID"]] = calculatePercent($BandNumber["C"][$ClassArr[$i]["ClassID"]], $NumberOfClassStudent[$ClassArr[$i]["ClassID"]]);
					$BandPercent["D"][$ClassArr[$i]["ClassID"]] = calculatePercent($BandNumber["D"][$ClassArr[$i]["ClassID"]], $NumberOfClassStudent[$ClassArr[$i]["ClassID"]]);
					$BandPercent["E"][$ClassArr[$i]["ClassID"]] = calculatePercent($BandNumber["E"][$ClassArr[$i]["ClassID"]], $NumberOfClassStudent[$ClassArr[$i]["ClassID"]]);
					$BandPercent["U"][$ClassArr[$i]["ClassID"]] = calculatePercent($BandNumber["U"][$ClassArr[$i]["ClassID"]], $NumberOfClassStudent[$ClassArr[$i]["ClassID"]]);
					
					$BandPercent["EU"][$ClassArr[$i]["ClassID"]] = calculatePercent($BandNumber["EU"][$ClassArr[$i]["ClassID"]], $NumberOfClassStudent[$ClassArr[$i]["ClassID"]]);
					
					$BandPercent["A*-A"][$ClassArr[$i]["ClassID"]] = calculatePercent($BandNumber["A*-A"][$ClassArr[$i]["ClassID"]], $NumberOfClassStudent[$ClassArr[$i]["ClassID"]]);
					$BandPercent["A*-C"][$ClassArr[$i]["ClassID"]] = calculatePercent($BandNumber["A*-C"][$ClassArr[$i]["ClassID"]], $NumberOfClassStudent[$ClassArr[$i]["ClassID"]]);
					
					$ClassStudentAverageMark[$ClassArr[$i]["ClassID"]] = round(($ClassStudentTotalmark[$ClassArr[$i]["ClassID"]]/($NumberOfClassStudent[$ClassArr[$i]["ClassID"]]-$ExNumber[$ClassArr[$i]["ClassID"]])), 1);
				}
				
				$CTeacher = array();
				$ClassTeacherNameArr = $lclass->returnClassTeacher($ClassArr[$i]["ClassName"]);
				foreach($ClassTeacherNameArr as $key=>$val) {
					$CTeacher[] = $val['CTeacher'];
				}
				$ClassTeacherName = !empty($CTeacher) ? implode(", ", $CTeacher) : "-";
				
				$display .= "<tr>
												<td>".$ClassArr[$i]["ClassName"]."</td>
												<td>".$ClassTeacherName."</td>
												<td align='center'>".($AbsNumber[$ClassArr[$i]["ClassID"]]+$ExNumber[$ClassArr[$i]["ClassID"]])."</td>
												<td align='center'>".$NumberOfClassStudent[$ClassArr[$i]["ClassID"]]."</td>
												<td align='center'>".$BandNumber["A*"][$ClassArr[$i]["ClassID"]]."</td>
												<td align='center'>".$BandPercent["A*"][$ClassArr[$i]["ClassID"]]."</td>
												<td align='center'>".$BandNumber["A"][$ClassArr[$i]["ClassID"]]."</td>
												<td align='center'>".$BandPercent["A"][$ClassArr[$i]["ClassID"]]."</td>
												<td align='center'>".$BandNumber["B"][$ClassArr[$i]["ClassID"]]."</td>
												<td align='center'>".$BandPercent["B"][$ClassArr[$i]["ClassID"]]."</td>
												<td align='center'>".$BandNumber["C"][$ClassArr[$i]["ClassID"]]."</td>
												<td align='center'>".$BandPercent["C"][$ClassArr[$i]["ClassID"]]."</td>
												<td align='center'>".$BandNumber["D"][$ClassArr[$i]["ClassID"]]."</td>
												<td align='center'>".$BandPercent["D"][$ClassArr[$i]["ClassID"]]."</td>
												<td align='center'>".$BandNumber["EU"][$ClassArr[$i]["ClassID"]]."</td>
												<td align='center'>".$BandPercent["EU"][$ClassArr[$i]["ClassID"]]."</td>
												<td align='center'>".$BandNumber["A*-A"][$ClassArr[$i]["ClassID"]]."</td>
												<td align='center'>".$BandPercent["A*-A"][$ClassArr[$i]["ClassID"]]."</td>
												<td align='center'>".$BandNumber["A*-C"][$ClassArr[$i]["ClassID"]]."</td>
												<td align='center'>".$BandPercent["A*-C"][$ClassArr[$i]["ClassID"]]."</td>
												<td align='center'>".$ClassStudentAverageMark[$ClassArr[$i]["ClassID"]]."</td>
										</tr>";
			} // End Loop 2: Classes
			// Display a row sum up stat of the whole class level
			$ClassLevelAbsNumber = array_sum($AbsNumber);
			$ClassLevelExNumber = array_sum($ExNumber);
			$NumberOfClassLevelStudent = array_sum($NumberOfClassStudent);
			
			$ClassLevelBandNumber["A*"] = array_sum($BandNumber["A*"]);
			$ClassLevelBandNumber["A"] = array_sum($BandNumber["A"]);
			$ClassLevelBandNumber["B"] = array_sum($BandNumber["B"]);
			$ClassLevelBandNumber["C"] = array_sum($BandNumber["C"]);
			$ClassLevelBandNumber["D"] = array_sum($BandNumber["D"]);
			$ClassLevelBandNumber["EU"] = array_sum($BandNumber["EU"]);
			
			$ClassLevelBandNumber["A*-A"] = $ClassLevelBandNumber["A*"]+$ClassLevelBandNumber["A"];
			$ClassLevelBandNumber["A*-C"] = $ClassLevelBandNumber["A*"]+$ClassLevelBandNumber["A"]+$ClassLevelBandNumber["B"]+$ClassLevelBandNumber["C"];
			
			$ClassLevelStudentTotalMark = array_sum($ClassStudentTotalmark);
			
			// Prevent divide by Zero
			if ($NumberOfClassLevelStudent != 0) {
				$ClassLevelBandPercent["A*"] = calculatePercent($ClassLevelBandNumber["A*"], $NumberOfClassLevelStudent);
				$ClassLevelBandPercent["A"] = calculatePercent($ClassLevelBandNumber["A"], $NumberOfClassLevelStudent);
				$ClassLevelBandPercent["B"] = calculatePercent($ClassLevelBandNumber["B"], $NumberOfClassLevelStudent);
				$ClassLevelBandPercent["C"] = calculatePercent($ClassLevelBandNumber["C"], $NumberOfClassLevelStudent);
				$ClassLevelBandPercent["D"] = calculatePercent($ClassLevelBandNumber["D"], $NumberOfClassLevelStudent);
				$ClassLevelBandPercent["EU"] = calculatePercent($ClassLevelBandNumber["EU"], $NumberOfClassLevelStudent);
				
				$ClassLevelBandPercent["A*-A"] = calculatePercent($ClassLevelBandNumber["A*-A"], $NumberOfClassLevelStudent);
				$ClassLevelBandPercent["A*-C"] = calculatePercent($ClassLevelBandNumber["A*-C"], $NumberOfClassLevelStudent);
				
				$ClassLevelStudentAverageMark = round(($ClassLevelStudentTotalMark/$NumberOfClassLevelStudent) ,1);
			}
			
			$display .= "<tr>
											<td>&nbsp;</td>
											<td>Level Average</td>
											<td align='center'>&nbsp;</td>
											<td align='center'>".$NumberOfClassLevelStudent."</td>
											<td align='center'>".$ClassLevelBandNumber["A*"]."</td>
											<td align='center'>".$ClassLevelBandPercent["A*"]."</td>
											<td align='center'>".$ClassLevelBandNumber["A"]."</td>
											<td align='center'>".$ClassLevelBandPercent["A"]."</td>
											<td align='center'>".$ClassLevelBandNumber["B"]."</td>
											<td align='center'>".$ClassLevelBandPercent["B"]."</td>
											<td align='center'>".$ClassLevelBandNumber["C"]."</td>
											<td align='center'>".$ClassLevelBandPercent["C"]."</td>
											<td align='center'>".$ClassLevelBandNumber["D"]."</td>
											<td align='center'>".$ClassLevelBandPercent["D"]."</td>
											<td align='center'>".$ClassLevelBandNumber["EU"]."</td>
											<td align='center'>".$ClassLevelBandPercent["EU"]."</td>
											<td align='center'>".$ClassLevelBandNumber["A*-A"]."</td>
											<td align='center'>".$ClassLevelBandPercent["A*-A"]."</td>
											<td align='center'>".$ClassLevelBandNumber["A*-C"]."</td>
											<td align='center'>".$ClassLevelBandPercent["A*-C"]."</td>
											<td align='center'>".$ClassLevelStudentAverageMark."</td>
									</tr>
									<tr><td colspan='21'>&nbsp;</td></tr>";
			
			
		} // End Loop 1: Subjects
		
		#debug_r($FormSubjectGradingSchemeArr);
		############## Interface Start ##############
		
?>
<style type="text/css">
<!--
h1 {
	font-size: 24px;
	font-family: "Times CY", "Times New Roman", serif;
	font-style: italic;
	margin: 0px;
	padding-top: 3px;
	padding-bottom: 3px;
	border-bottom: 5px solid #DDD;
}

h2 {
	font-size: 16px;
	font-family: "Times CY", "Times New Roman", serif;
	margin: 0px;
	padding-top: 2px;
	padding-bottom: 2px;
}

#StatReportWrapper {
	min-height: 800px;
	height: auto;
	_height: 800px;
	border-top: 5px solid #DDD;
	border-bottom: 5px solid #DDD;
}

#StatTable thead {
	font-size: 15px;
	font-family: "Times CY", "Times New Roman", serif;
	font-style: italic;
}

#StatTable tbody {
	font-size: 12px;
	font-family: arial, "lucida console", sans-serif;
}

.SubjectName {
	font-size: 12px;
	font-weight: bold;
	font-family: arial, "lucida console", sans-serif;
	padding-bottom: 5px;
}

.smallTableHeader {
	font-size: 11px;
	font-weight: bold;
	font-family: "Times CY", "Times New Roman", serif;
}
-->
</style>
<script type="text/JavaScript" language="JavaScript">

</script>
<div id="StatReportWrapper" style="width:100%;">
	<h1><?=$StatReportTitle?></h1>
	<h2>
		School Year&nbsp;&nbsp;<?= $SchoolYear ?>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		Level:&nbsp;<?= $ClassLevelName ?>
	</h2>
	<table id="StatTable" width="100%" cellpadding="0" cellspacing="0" border="0">
		<thead>
			<tr>
				<th>&nbsp;</th>
				<th>Teacher</th>
				<th>Abs/Ex</th>
				<th>Total</th>
				<th colspan="2">Band A&#42;</th>
				<th colspan="2">Band A</th>
				<th colspan="2">Band B</th>
				<th colspan="2">Band C</th>
				<th colspan="2">Band D</th>
				<th colspan="2">Band E&#47;U</th>
				<th colspan="2" class="smallTableHeader">Quality<br />Passes(A&#42;-A)</th>
				<th colspan="2" class="smallTableHeader">Passes(A&#42;-C)</th>
				<th>Average</th>
			</tr>
			<tr>
				<th>&nbsp;</th>
				<th>&nbsp;</th>
				<th>Number</th>
				<th>Number</th>
				<th class="smallTableHeader">Number</th>
				<th class="smallTableHeader">&#37;</th>
				<th class="smallTableHeader">Number</th>
				<th class="smallTableHeader">&#37;</th>
				<th class="smallTableHeader">Number</th>
				<th class="smallTableHeader">&#37;</th>
				<th class="smallTableHeader">Number</th>
				<th class="smallTableHeader">&#37;</th>
				<th class="smallTableHeader">Number</th>
				<th class="smallTableHeader">&#37;</th>
				<th class="smallTableHeader">Number</th>
				<th class="smallTableHeader">&#37;</th>
				<th class="smallTableHeader">Number</th>
				<th class="smallTableHeader">&#37;</th>
				<th class="smallTableHeader">Number</th>
				<th class="smallTableHeader">&#37;</th>
				<th>Marks</th>
			</tr>
		</thead>
		<tbody>
			<?= $display ?>
		</tbody>
	</table>
</div>
<?
  } else {
?>
You have no priviledge to access this page.
<?
	}
} else {
?>
You have no priviledge to access this page.
<?
}
?>
