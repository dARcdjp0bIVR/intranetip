<?php
######################################################
# Generate Statistic Report for Secondary Class Level
# 	Trigger from: index.php
# 	Required variables: $ClassLevelID, $ReportID
# 	Optional variables: $ReportColumnID
#
# 	Last updated: Andy Chan on 2008/6/3
######################################################

function returnGrandMSBand($Marks='-1') {
	if($Marks == -1)	return array("A1","A2","B3","B4","C5","C6","D7","E8","F9");
	if($Marks >= 75)	return "A1";
	if($Marks >= 70)	return "A2";
	if($Marks >= 65)	return "B3";
	if($Marks >= 60)	return "B4";
	if($Marks >= 55)	return "C5";
	if($Marks >= 50)	return "C6";
	if($Marks >= 45)	return "D7";
	if($Marks >= 40)	return "E8";
	return "F9";
}

function calculatePercent($Numerator, $Denumerator) {
	return number_format(round(($Numerator/$Denumerator*100) ,1) ,1);
}

// Root path
$PATH_WRT_ROOT = "../../../../../";

// Page access right
$PageRight = "ADMIN";

// Include general libraries
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
#include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

// Authorize intranet user & connect Database
intranet_auth();
intranet_opendb();

// Check if ReportCard module is enable
if ($plugin['ReportCard']) {
	// Include ReportCard libraries
	include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
	include_once($PATH_WRT_ROOT."includes/libreportcard2008w.php");
	
	// Create objects
	$lreportcard = new libreportcard2008w();
	$linterface = new interface_html();
	$lclass = new libclass();
	
	// Check module access right
	if ($lreportcard->hasAccessRight()) {
		// Check required data submitted from index.php
		if (!isset($ClassLevelID, $ReportID)) {
			intranet_closedb();
			header("Location: index.php");
			exit();
		}
		
		// Get ReportCard info
		$reportTemplateInfo = $lreportcard->returnReportTemplateBasicInfo($ReportID);
		
		// Check to make sure it is a whole year & generated report
		if ($reportTemplateInfo["LastGenerated"] == "0000-00-00 00:00:00" || $reportTemplateInfo["LastGenerated"] == "") {
			intranet_closedb();
			header("Location: index.php");
			exit();
		}
		
		// Get all report cards of the current ClassLevel (use to identify report type)
		$ReportTypeArr = $lreportcard->GET_REPORT_TYPES($ClassLevelID);
		$ReportIDList = array();
		for($i=0; $i<sizeof($ReportTypeArr); $i++) {
			$ReportIDList[] = $ReportTypeArr[$i]['ReportID'];
		}
		sort($ReportIDList);
		$ReportIDOrder = array_search($ReportID, $ReportIDList);
		
		// Get subjects of the current ClassLevel
		$FormSubjectArr = $lreportcard->returnSubjectwOrder($ClassLevelID, 1);
		
		// Get subjects' grading scheme of the current ClassLevel
		$FormSubjectGradingSchemeArr = $lreportcard->GET_FROM_SUBJECT_GRADING_SCHEME($ClassLevelID,0,$ReportID);
		
		// Get ClassLevel name
		$ClassLevelName = $lreportcard->returnClassLevel($ClassLevelID);
		
		// Get Classes
		$ClassArr = $lreportcard->GET_CLASSES_BY_FORM($ClassLevelID);
		
		// Get current school year
		$SchoolYear = $lreportcard->schoolYear;
		
		// Get columns info of the current ReportCard
		$ColumnTitleAry = $lreportcard->returnReportColoumnTitle($ReportID);
		if (is_array($ColumnTitleAry))
		{
			$ColumnIDList = array_keys($ColumnTitleAry);
		}
		else
		{
			$ColumnIDList = array();
		}
		
		// For secondary year end report, there are 4 terms
		if (sizeof($ColumnTitleAry) == 4) {
			if (isset($ReportColumnID) && $ReportColumnID != "") {
				if ($ReportColumnID == "first") {
					$DisplayColumnName = "1st Combined";
					$ReportID = $ReportIDList[0];
					$ReportColumnID = "";
				}
				if ($ReportColumnID == "second") {
					$DisplayColumnName = "2nd Combined";
					$ReportID = $ReportIDList[1];
					$ReportColumnID = "";
				}
				$StatReportTitle = "Summary of ".$DisplayColumnName." Results by Subjects Grade (Secondary)";
			} else {
				$StatReportTitle = "Summary of Overall Results by Subjects Grade (Secondary)";
			}
		} else {	// not 4 terms, should be half year report
			if (isset($ReportColumnID) && $ReportColumnID != "") {
				if ($ReportColumnID == "first") {
					$DisplayColumnName = "1st Combined";
					$ReportColumnID == "";
				}
				if ($ReportIDOrder == 0) {
					if ($ReportColumnID == $ColumnIDList[0])	$DisplayColumnName = "CA1";
					if ($ReportColumnID == $ColumnIDList[1])	$DisplayColumnName = "SA1";
				}
				
				if ($ReportColumnID == "second") {
					$DisplayColumnName = "2nd Combined";
					$ReportColumnID == "";
				}
				if ($ReportIDOrder == 1) {
					if ($ReportColumnID == $ColumnIDList[0])	$DisplayColumnName = "CA2";
					if ($ReportColumnID == $ColumnIDList[1])	$DisplayColumnName = "SA2";
				}
				
				$StatReportTitle = "Summary of ".$DisplayColumnName." Results by Subjects Grade (Secondary)";
			}
		}
		
		$display = "";
		
		// Main table generaeting logic
		// Loop 1: Subjects
		foreach ($FormSubjectArr as $SubjectID => $SubjectName) {
			if ($FormSubjectGradingSchemeArr[$SubjectID]["scaleInput"] != "M")
				continue;
			
			$display .= "<tr><td class='SubjectName' colspan='2'>".str_replace(" ()", "", $SubjectName[0])."</td><td>&nbsp;</td>";
			for($i=0; $i<11; $i++) {
				$display .= "<td class='LeftBorder'>&nbsp;</td>";
			}
			$display .= "</tr>";
			
			// Init variables
			$AbsNumber = array();
			$ExNumber = array();
			$BandNumber = array();
			$BandPercent = array();
			$NumberOfClassStudent = array();
			$ClassStudentTotalmark = array();
			$ClassStudentAverageMark =  array();
			
			$ClassLevelAbsNumber = 0;
			$ClassLevelExNumber = 0;
			$NumberOfClassLevelStudent = 0;
			$ClassLevelBandNumber = array();
			$ClassLevelBandPercent = array();
			$ClassLevelStudentTotalMark = 0;
			$ClassLevelStudentAverageMark = 0;
			
			// Loop 2: Classes
			for($i=0; $i<sizeof($ClassArr); $i++) {
				// Construct StudentIDList in the class
				$ClassStudentArr = $lreportcard->GET_STUDENT_BY_CLASS($ClassArr[$i]["ClassID"]);
				$ClassStudentIDList = array();
				for($j=0; $j<sizeof($ClassStudentArr); $j++) {
					$ClassStudentIDList[] = $ClassStudentArr[$j]["UserID"];
				}
				$ClassStudentIDListQuery = implode("','", $ClassStudentIDList);
				$ClassStudentIDListQuery = "'".$ClassStudentIDListQuery."'";
				
				// Query the consolidated marks of the Class
				$table = $lreportcard->DBName.".RC_REPORT_RESULT_SCORE";
				$sql = "SELECT Mark, Grade FROM $table ";
				$sql .= "WHERE ReportID = '$ReportID' AND SubjectID = '$SubjectID' AND StudentID IN ($ClassStudentIDListQuery) AND ";
				
				if (isset($ReportColumnID) && $ReportColumnID != "") {
					$sql .= "ReportColumnID = '$ReportColumnID'";
				} else {
					$sql .= "(ReportColumnID = 0 OR ReportColumnID IS NULL)";
				}
				$ClassResult = $lreportcard->returnArray($sql, 2);
				
				$NumberOfClassStudent[$ClassArr[$i]["ClassID"]] = sizeof($ClassResult);
				
				// Init variables
				$AbsNumber[$ClassArr[$i]["ClassID"]] = 0;
				$ExNumber[$ClassArr[$i]["ClassID"]] = 0;
				
				$BandNumber["A1"][$ClassArr[$i]["ClassID"]] = 0;
				$BandNumber["A2"][$ClassArr[$i]["ClassID"]] = 0;
				$BandNumber["B3"][$ClassArr[$i]["ClassID"]] = 0;
				$BandNumber["B4"][$ClassArr[$i]["ClassID"]] = 0;
				$BandNumber["C5"][$ClassArr[$i]["ClassID"]] = 0;
				$BandNumber["C6"][$ClassArr[$i]["ClassID"]] = 0;
				$BandNumber["D7"][$ClassArr[$i]["ClassID"]] = 0;
				$BandNumber["E8"][$ClassArr[$i]["ClassID"]] = 0;
				$BandNumber["F9"][$ClassArr[$i]["ClassID"]] = 0;
				$BandNumber["A1-C6"][$ClassArr[$i]["ClassID"]] = 0;
				
				$BandPercent["A1"][$ClassArr[$i]["ClassID"]] = 0;
				$BandPercent["A2"][$ClassArr[$i]["ClassID"]] = 0;
				$BandPercent["B3"][$ClassArr[$i]["ClassID"]] = 0;
				$BandPercent["B4"][$ClassArr[$i]["ClassID"]] = 0;
				$BandPercent["C5"][$ClassArr[$i]["ClassID"]] = 0;
				$BandPercent["C6"][$ClassArr[$i]["ClassID"]] = 0;
				$BandPercent["D7"][$ClassArr[$i]["ClassID"]] = 0;
				$BandPercent["E8"][$ClassArr[$i]["ClassID"]] = 0;
				$BandPercent["F9"][$ClassArr[$i]["ClassID"]] = 0;
				$BandPercent["A1-C6"][$ClassArr[$i]["ClassID"]] = 0;
				
				$ClassStudentTotalmark[$ClassArr[$i]["ClassID"]] = 0;
				$ClassStudentAverageMark[$ClassArr[$i]["ClassID"]] = 0;
				
				// Loop the marks and group into different categories
				for($j=0; $j<sizeof($ClassResult); $j++) {
					$StudentBand = "";
					if ($ClassResult[$j]["Mark"] != "" && $ClassResult[$j]["Grade"] == "") {
						$ClassStudentTotalmark[$ClassArr[$i]["ClassID"]] += $ClassResult[$j]["Mark"];
						$StudentBand = returnGrandMSBand($ClassResult[$j]["Mark"]);
						if (!is_array($StudentBand))
							$BandNumber[$StudentBand][$ClassArr[$i]["ClassID"]]++;
					} else if ($ClassResult[$j]["Grade"] == "-") {
						$AbsNumber[$ClassArr[$i]["ClassID"]]++;
					} else if ($ClassResult[$j]["Grade"] == "/") {
						$ExNumber[$ClassArr[$i]["ClassID"]]++;
					}
				}
				
				$BandNumber["A1-C6"][$ClassArr[$i]["ClassID"]] += $BandNumber["A1"][$ClassArr[$i]["ClassID"]]+$BandNumber["A2"][$ClassArr[$i]["ClassID"]];
				$BandNumber["A1-C6"][$ClassArr[$i]["ClassID"]] += $BandNumber["B3"][$ClassArr[$i]["ClassID"]]+$BandNumber["B4"][$ClassArr[$i]["ClassID"]];
				$BandNumber["A1-C6"][$ClassArr[$i]["ClassID"]] += $BandNumber["C5"][$ClassArr[$i]["ClassID"]]+$BandNumber["C6"][$ClassArr[$i]["ClassID"]];
				
				
				// Prevent divide by Zero
				if ($NumberOfClassStudent[$ClassArr[$i]["ClassID"]] != 0) {
					$BandPercent["A1"][$ClassArr[$i]["ClassID"]] = calculatePercent($BandNumber["A1"][$ClassArr[$i]["ClassID"]], $NumberOfClassStudent[$ClassArr[$i]["ClassID"]]);
					$BandPercent["A2"][$ClassArr[$i]["ClassID"]] = calculatePercent($BandNumber["A2"][$ClassArr[$i]["ClassID"]], $NumberOfClassStudent[$ClassArr[$i]["ClassID"]]);
					$BandPercent["B3"][$ClassArr[$i]["ClassID"]] = calculatePercent($BandNumber["B3"][$ClassArr[$i]["ClassID"]], $NumberOfClassStudent[$ClassArr[$i]["ClassID"]]);
					$BandPercent["B4"][$ClassArr[$i]["ClassID"]] = calculatePercent($BandNumber["B4"][$ClassArr[$i]["ClassID"]], $NumberOfClassStudent[$ClassArr[$i]["ClassID"]]);
					$BandPercent["C5"][$ClassArr[$i]["ClassID"]] = calculatePercent($BandNumber["C5"][$ClassArr[$i]["ClassID"]], $NumberOfClassStudent[$ClassArr[$i]["ClassID"]]);
					$BandPercent["C6"][$ClassArr[$i]["ClassID"]] = calculatePercent($BandNumber["C6"][$ClassArr[$i]["ClassID"]], $NumberOfClassStudent[$ClassArr[$i]["ClassID"]]);
					$BandPercent["D7"][$ClassArr[$i]["ClassID"]] = calculatePercent($BandNumber["D7"][$ClassArr[$i]["ClassID"]], $NumberOfClassStudent[$ClassArr[$i]["ClassID"]]);
					$BandPercent["E8"][$ClassArr[$i]["ClassID"]] = calculatePercent($BandNumber["E8"][$ClassArr[$i]["ClassID"]], $NumberOfClassStudent[$ClassArr[$i]["ClassID"]]);
					$BandPercent["F9"][$ClassArr[$i]["ClassID"]] = calculatePercent($BandNumber["F9"][$ClassArr[$i]["ClassID"]], $NumberOfClassStudent[$ClassArr[$i]["ClassID"]]);
					$BandPercent["A1-C6"][$ClassArr[$i]["ClassID"]] = calculatePercent($BandNumber["A1-C6"][$ClassArr[$i]["ClassID"]], $NumberOfClassStudent[$ClassArr[$i]["ClassID"]]);
					
					$ClassStudentAverageMark[$ClassArr[$i]["ClassID"]] = number_format(round(($ClassStudentTotalmark[$ClassArr[$i]["ClassID"]]/($NumberOfClassStudent[$ClassArr[$i]["ClassID"]]-$ExNumber[$ClassArr[$i]["ClassID"]])), 1), 1);
				}
				
				$CTeacher = array();
				$ClassTeacherNameArr = $lclass->returnClassTeacher($ClassArr[$i]["ClassName"]);
				foreach($ClassTeacherNameArr as $key=>$val) {
					$CTeacher[] = $val['CTeacher'];
				}
				$ClassTeacherName = !empty($CTeacher) ? implode(", ", $CTeacher) : "-";
				
				$display .= "<tr>
												<td>".$ClassArr[$i]["ClassName"]."</td>
												<td>".$ClassTeacherName."</td>
												<td align='center'>".$NumberOfClassStudent[$ClassArr[$i]["ClassID"]]."</td>
												<td class='LeftBorder' align='center'>".$BandNumber["A1"][$ClassArr[$i]["ClassID"]]." (".str_pad($BandPercent["A1"][$ClassArr[$i]["ClassID"]], 5, " ", STR_PAD_LEFT)."%)</td>
												<td class='LeftBorder' align='center'>".$BandNumber["A2"][$ClassArr[$i]["ClassID"]]." (".str_pad($BandPercent["A2"][$ClassArr[$i]["ClassID"]], 5, " ", STR_PAD_LEFT)."%)</td>
												<td class='LeftBorder' align='center'>".$BandNumber["B3"][$ClassArr[$i]["ClassID"]]." (".str_pad($BandPercent["B3"][$ClassArr[$i]["ClassID"]], 5, " ", STR_PAD_LEFT)."%)</td>
												<td class='LeftBorder' align='center'>".$BandNumber["B4"][$ClassArr[$i]["ClassID"]]." (".str_pad($BandPercent["B4"][$ClassArr[$i]["ClassID"]], 5, " ", STR_PAD_LEFT)."%)</td>
												<td class='LeftBorder' align='center'>".$BandNumber["C5"][$ClassArr[$i]["ClassID"]]." (".str_pad($BandPercent["C5"][$ClassArr[$i]["ClassID"]], 5, " ", STR_PAD_LEFT)."%)</td>
												<td class='LeftBorder' align='center'>".$BandNumber["C6"][$ClassArr[$i]["ClassID"]]." (".str_pad($BandPercent["C6"][$ClassArr[$i]["ClassID"]], 5, " ", STR_PAD_LEFT)."%)</td>
												<td class='LeftBorder' align='center'>".$BandNumber["D7"][$ClassArr[$i]["ClassID"]]." (".str_pad($BandPercent["D7"][$ClassArr[$i]["ClassID"]], 5, " ", STR_PAD_LEFT)."%)</td>
												<td class='LeftBorder' align='center'>".$BandNumber["E8"][$ClassArr[$i]["ClassID"]]." (".str_pad($BandPercent["E8"][$ClassArr[$i]["ClassID"]], 5, " ", STR_PAD_LEFT)."%)</td>
												<td class='LeftBorder' align='center'>".$BandNumber["F9"][$ClassArr[$i]["ClassID"]]." (".str_pad($BandPercent["F9"][$ClassArr[$i]["ClassID"]], 5, " ", STR_PAD_LEFT)."%)</td>
												<td class='LeftBorder' align='center'>".$BandNumber["A1-C6"][$ClassArr[$i]["ClassID"]]." (".str_pad($BandPercent["A1-C6"][$ClassArr[$i]["ClassID"]], 5, " ", STR_PAD_LEFT)."%)</td>
												<td class='LeftBorder' align='center'>".$ClassStudentAverageMark[$ClassArr[$i]["ClassID"]]."</td>
										</tr>";
			} // End Loop 2: Classes
			// Display a row sum up stat of the whole class level
			$ClassLevelAbsNumber = array_sum($AbsNumber);
			$ClassLevelExNumber = array_sum($ExNumber);
			$NumberOfClassLevelStudent = array_sum($NumberOfClassStudent);
			
			$ClassLevelBandNumber["A1"] = array_sum($BandNumber["A1"]);
			$ClassLevelBandNumber["A2"] = array_sum($BandNumber["A2"]);
			$ClassLevelBandNumber["B3"] = array_sum($BandNumber["B3"]);
			$ClassLevelBandNumber["B4"] = array_sum($BandNumber["B4"]);
			$ClassLevelBandNumber["C5"] = array_sum($BandNumber["C5"]);
			$ClassLevelBandNumber["C6"] = array_sum($BandNumber["C6"]);
			$ClassLevelBandNumber["D7"] = array_sum($BandNumber["D7"]);
			$ClassLevelBandNumber["E8"] = array_sum($BandNumber["E8"]);
			$ClassLevelBandNumber["F9"] = array_sum($BandNumber["F9"]);
			
			$ClassLevelBandNumber["A1-C6"] = $ClassLevelBandNumber["A1"]+$ClassLevelBandNumber["A2"]+$ClassLevelBandNumber["B3"]+$ClassLevelBandNumber["B4"]+$ClassLevelBandNumber["C5"]+$ClassLevelBandNumber["C6"];
			
			$ClassLevelStudentTotalMark = array_sum($ClassStudentTotalmark);
			
			// Prevent divide by Zero
			if ($NumberOfClassLevelStudent != 0) {
				$ClassLevelBandPercent["A1"] = calculatePercent($ClassLevelBandNumber["A1"], $NumberOfClassLevelStudent);
				$ClassLevelBandPercent["A2"] = calculatePercent($ClassLevelBandNumber["A2"], $NumberOfClassLevelStudent);
				$ClassLevelBandPercent["B3"] = calculatePercent($ClassLevelBandNumber["B3"], $NumberOfClassLevelStudent);
				$ClassLevelBandPercent["B4"] = calculatePercent($ClassLevelBandNumber["B4"], $NumberOfClassLevelStudent);
				$ClassLevelBandPercent["C5"] = calculatePercent($ClassLevelBandNumber["C5"], $NumberOfClassLevelStudent);
				$ClassLevelBandPercent["C6"] = calculatePercent($ClassLevelBandNumber["C6"], $NumberOfClassLevelStudent);
				$ClassLevelBandPercent["D7"] = calculatePercent($ClassLevelBandNumber["D7"], $NumberOfClassLevelStudent);
				$ClassLevelBandPercent["E8"] = calculatePercent($ClassLevelBandNumber["E8"], $NumberOfClassLevelStudent);
				$ClassLevelBandPercent["F9"] = calculatePercent($ClassLevelBandNumber["F9"], $NumberOfClassLevelStudent);
				
				$ClassLevelBandPercent["A1-C6"] = calculatePercent($ClassLevelBandNumber["A1-C6"], $NumberOfClassLevelStudent);
				
				$ClassLevelStudentAverageMark = number_format(round(($ClassLevelStudentTotalMark/$NumberOfClassLevelStudent) ,1), 1);
			}
			
			$display .= "<tr>
											<td>&nbsp;</td>
											<td style='font-weight:bold;'>Level Average</td>
											<td align='center'>".$NumberOfClassLevelStudent."</td>
											<td class='LeftBorder' align='center'>".$ClassLevelBandNumber["A1"]." (".str_pad($ClassLevelBandPercent["A1"], 5, " ", STR_PAD_LEFT)."%)</td>
											<td class='LeftBorder' align='center'>".$ClassLevelBandNumber["A2"]." (".str_pad($ClassLevelBandPercent["A2"], 5, " ", STR_PAD_LEFT)."%)</td>
											<td class='LeftBorder' align='center'>".$ClassLevelBandNumber["B3"]." (".str_pad($ClassLevelBandPercent["B3"], 5, " ", STR_PAD_LEFT)."%)</td>
											<td class='LeftBorder' align='center'>".$ClassLevelBandNumber["B4"]." (".str_pad($ClassLevelBandPercent["B4"], 5, " ", STR_PAD_LEFT)."%)</td>
											<td class='LeftBorder' align='center'>".$ClassLevelBandNumber["C5"]." (".str_pad($ClassLevelBandPercent["C5"], 5, " ", STR_PAD_LEFT)."%)</td>
											<td class='LeftBorder' align='center'>".$ClassLevelBandNumber["C6"]." (".str_pad($ClassLevelBandPercent["C6"], 5, " ", STR_PAD_LEFT)."%)</td>
											<td class='LeftBorder' align='center'>".$ClassLevelBandNumber["D7"]." (".str_pad($ClassLevelBandPercent["D7"], 5, " ", STR_PAD_LEFT)."%)</td>
											<td class='LeftBorder' align='center'>".$ClassLevelBandNumber["E8"]." (".str_pad($ClassLevelBandPercent["E8"], 5, " ", STR_PAD_LEFT)."%)</td>
											<td class='LeftBorder' align='center'>".$ClassLevelBandNumber["F9"]." (".str_pad($ClassLevelBandPercent["F9"], 5, " ", STR_PAD_LEFT)."%)</td>
											<td class='LeftBorder' align='center'>".$ClassLevelBandNumber["A1-C6"]." (".str_pad($ClassLevelBandPercent["A1-C6"], 5, " ", STR_PAD_LEFT)."%)</td>
											<td class='LeftBorder' align='center'>".$ClassLevelStudentAverageMark."</td>
									</tr>
									<tr><td colspan='3'>&nbsp;</td>";
			for($i=0; $i<11; $i++) {
				$display .= "<td class='LeftBorder'>&nbsp;</td>";
			}
			$display .= "</tr>";
			
			
		} // End Loop 1: Subjects
		
?>
<style type="text/css">
<!--
h1 {
	font-size: 24px;
	font-family: "Times CY", "Times New Roman", serif;
	font-style: italic;
	margin: 0px 0px 5px 0px;
	padding-top: 3px;
	padding-bottom: 3px;
	border-bottom: 5px solid #DDD;
}

#StatReportWrapper {
	min-height: 645px;
	height: auto;
	_height: 645px;
	border-top: 3px solid #DDD;
	border-bottom: 3px solid #DDD;
}

#StatTable thead {
	font-size: 15px;
	font-family: "Times CY", "Times New Roman", serif;
	font-style: italic;
}

#StatTable tbody {
	font-size: 12px;
	font-family: arial, "lucida console", sans-serif;
}

#StatTable tbody td {
	padding-top: 2px;
	padding-bottom: 2px;
}

.SubjectName {
	font-size: 12px;
	font-weight: bold;
	font-family: arial, "lucida console", sans-serif;
	text-decoration: underline;
	padding-top: 5px;
	padding-bottom: 5px;
}

.SupInfo {
	font-size: 16px;
	font-family: "Times CY", "Times New Roman", serif;
	margin: 0px;
	padding-top: 2px;
	padding-bottom: 2px;
}

.NormalTableHeader {
	border-bottom: 2px #ccc solid;
}

.SmallTableHeader {
	font-size: 12px;
	border-bottom: 2px #ccc solid;
}

.LeftBorder {
	border-left: 2px #ccc solid;
}
-->
</style>
<script type="text/JavaScript" language="JavaScript">

</script>
<div id="StatReportWrapper" style="width:100%;">
	<h1>
		<?=$StatReportTitle?>
		<span class="SupInfo">
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			School Year: <?= $SchoolYear ?>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			Level: <?= $ClassLevelName ?>
		</span>
	</h1>
	<table id="StatTable" width="100%" cellpadding="0" cellspacing="0" border="0">
		<thead>
			<tr>
				<th>&nbsp;</th>
				<th>Teacher</th>
				<th>Total Number</th>
				<th class="LeftBorder">A1</th>
				<th class="LeftBorder">A2</th>
				<th class="LeftBorder">B3</th>
				<th class="LeftBorder">B4</th>
				<th class="LeftBorder">C5</th>
				<th class="LeftBorder">C6</th>
				<th class="LeftBorder">D7</th>
				<th class="LeftBorder">E8</th>
				<th class="LeftBorder">F9</th>
				<th class="LeftBorder">Pass Rate</th>
				<th class="LeftBorder">Avg.</th>
			</tr>
			<tr>
				<th class="NormalTableHeader">&nbsp;</th>
				<th class="NormalTableHeader">&nbsp;</th>
				<th class="NormalTableHeader">of students</th>
				<th class="SmallTableHeader LeftBorder">(75-100)</th>
				<th class="SmallTableHeader LeftBorder">(70-74)</th>
				<th class="SmallTableHeader LeftBorder">(65-69)</th>
				<th class="SmallTableHeader LeftBorder">(60-64)</th>
				<th class="SmallTableHeader LeftBorder">(55-59)</th>
				<th class="SmallTableHeader LeftBorder">(50-54)</th>
				<th class="SmallTableHeader LeftBorder">(45-49)</th>
				<th class="SmallTableHeader LeftBorder">(40-44)</th>
				<th class="SmallTableHeader LeftBorder">(below 39)</th>
				<th class="SmallTableHeader LeftBorder">(A1 - C6)</th>
				<th class="NormalTableHeader LeftBorder">Marks</th>
			</tr>
		</thead>
		<tbody>
			<?= $display ?>
		</tbody>
	</table>
</div>
<?
  } else {
?>
You have no priviledge to access this page.
<?
	}
} else {
?>
You have no priviledge to access this page.
<?
}
?>
