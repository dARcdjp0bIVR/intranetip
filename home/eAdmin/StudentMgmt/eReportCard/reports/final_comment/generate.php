<?php
# Editing by 
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();
 
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");

$lreportcard = new libreportcardcustom();
$lreportcard_ui = new libreportcard_ui();
$linterface = new interface_html();

$lreportcard->hasAccessRight();


$ClassLevelID = trim(stripslashes($_POST['ClassLevelID']));
$ReportID = trim(stripslashes($_POST['ReportID']));
$YearClassIDArr = $_POST['YearClassIDArr'];
$ViewFormat = trim(stripslashes($_POST['ViewFormat']));


### Get Form Name
$ClassLevelName = $lreportcard->returnClassLevel($ClassLevelID);


### Get Students
$isShowStyle = ($ViewFormat=='html')? true : false;
$StudentInfoAssoArr = $lreportcard->GET_STUDENT_BY_CLASSLEVEL($ReportID, $ClassLevelID, $YearClassIDArr, $ParStudentID="", $ReturnAsso=1, $isShowStyle);
$StudentIDArr = array_keys($StudentInfoAssoArr);
$numOfStudent = count($StudentIDArr);


### Get Final Comment
$FinalCommentAssoArr = $lreportcard->Get_Student_Final_Comment($ReportID, $StudentIDArr);


### Build Report Content
$lineBreakReplacement = ($isShowStyle=='html')? '<br />' : "\n";
$contentArr = array();
for ($i=0; $i<$numOfStudent; $i++) {
	$thisStudentID = $StudentIDArr[$i];
	$thisUserLogin = $StudentInfoAssoArr[$thisStudentID]['UserLogin'];
	$thisClassNameEn = $StudentInfoAssoArr[$thisStudentID]['ClassTitleEn'];
	$thisClassNameCh = $StudentInfoAssoArr[$thisStudentID]['ClassTitleCh'];
	$thisClassName = Get_Lang_Selection($thisClassNameCh, $thisClassNameEn);
	$thisClassNumber = $StudentInfoAssoArr[$thisStudentID]['ClassNumber'];
	$thisStudentName = $StudentInfoAssoArr[$thisStudentID]['StudentName'];
	
	$thisFinalComment = implode($lineBreakReplacement, $FinalCommentAssoArr[$thisStudentID]);
	
	$contentArr[$i]['UserLogin'] = $thisUserLogin;
	$contentArr[$i]['ClassName'] = $thisClassName;
	$contentArr[$i]['ClassNumber'] = $thisClassNumber;
	$contentArr[$i]['StudentName'] = $thisStudentName;
	$contentArr[$i]['FinalComment'] = $thisFinalComment;
}
$numOfContent = count($contentArr);


if ($ViewFormat=='html') {
	$css = $lreportcard->Get_GrandMS_CSS();
	
	$ReportTitle = $ClassLevelName.' '.$eReportCard['Reports_FinalComment'];
	
	$table = '';
	$table .= "<table id='ResultTable' class='GrandMSContentTable' width='100%' border='1' cellpadding='2' cellspacing='0'>\n";
		$table .= "<thead>\n";
			$table .= "<tr>\n";
				$table .= "<th style='width:3%;'>#</th>\n";
				$table .= "<th style='width:10%; text-align:left;'>".$Lang['SysMgr']['FormClassMapping']['Class']."</th>\n";
				$table .= "<th style='width:5%; text-align:left;'>".$Lang['SysMgr']['FormClassMapping']['ClassNo']."</th>\n";
				$table .= "<th style='width:25%; text-align:left;'>".$Lang['SysMgr']['FormClassMapping']['StudentName']."</th>\n";
				$table .= "<th style='width:57%; text-align:left;'>".$Lang['eReportCard']['ReportArr']['FinalCommentArr']['FinalComment']."</th>\n";
			$table .= "</tr>\n";
		$table .= "</thead>\n";
		
		$table .= "<tbody>\n";
			for ($i=0; $i<$numOfContent; $i++) {
				$table .= "<tr>\n";
					$table .= "<td align='center'>".($i + 1)."</td>\n";
					$table .= "<td style='text-align:left;'>".$contentArr[$i]['ClassName']."</td>\n";
					$table .= "<td style='text-align:left;'>".$contentArr[$i]['ClassNumber']."</td>\n";
					$table .= "<td style='text-align:left;'>".$contentArr[$i]['StudentName']."</td>\n";
					$table .= "<td style='text-align:left;'>".$contentArr[$i]['FinalComment']."</td>\n";
				$table .= "</tr>\n";
			}
		$table .= "</tbody>\n";
	$table .= "</table>\n";
	
	$allTable = '<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td align="center"><h1>'.$ReportTitle.'</h1></td>
					</tr>
					<tr>
						<td>'.$table.'</td>
					</tr>
				</table>';
	
	echo $lreportcard->Get_Report_Header($ReportTitle);
	echo $allTable.$css;
	echo $lreportcard->Get_Report_Footer();
	
	intranet_closedb();
}
else {
	include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
	include_once($PATH_WRT_ROOT."includes/libexporttext.php");
	
	$lexport = new libexporttext();
	$headerArr = array();
	$csvContentArr = array();
	
	$otherInfoConfig = $lreportcard->getOtherInfoConfig('remark');
	$numOfConfig = count($otherInfoConfig);
	for($i=0; $i<$numOfConfig; $i++) {
		$headerArr[] = $otherInfoConfig[$i]["EnglishTitle"];
		$csvContentArr[0][] = $otherInfoConfig[$i]["ChineseTitle"];
	}
	$csvContentArr[0][] = $Lang['General']['ImportArr']['SecondRowWarn'];
	
	for ($i=0; $i<$numOfContent; $i++) {
		$csvContentArr[$i+1][] = '';
		$csvContentArr[$i+1][] = '';
		$csvContentArr[$i+1][] = $contentArr[$i]['UserLogin'];
		$csvContentArr[$i+1][] = '';
		$csvContentArr[$i+1][] = $contentArr[$i]['StudentName'];
		$csvContentArr[$i+1][] = $contentArr[$i]['FinalComment'];
	}
	
	// Title of the grandmarksheet
	$filename = 'final_comment_'.$ClassLevelName;
	$filename = str_replace(" ", "_", $filename);
	$filename = $filename.".csv";
	
	$export_content = $lexport->GET_EXPORT_TXT($csvContentArr, $headerArr, $Delimiter="", $LineBreak="\r\n", $ColumnDefDelimiter="", $DataSize=0, $Quoted="00", $includeLineBreak=1);
	intranet_closedb();
	
	// Output the file to user browser
	$lexport->EXPORT_FILE($filename, $export_content);
}
?>