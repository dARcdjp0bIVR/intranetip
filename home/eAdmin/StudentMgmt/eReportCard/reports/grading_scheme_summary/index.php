<?php
//2016-10-03	Villa Create the file
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
{
	include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
	$lreportcard = new libreportcardcustom();
} else {
	$lreportcard = new libreportcard();
}

$linterface = new interface_html();
$lclass = new libclass();
	
$lreportcard->hasAccessRight();
		
$levels = $lclass->getLevelArray();
$select_list = "<SELECT name=\"TargetID[]\" MULTIPLE SIZE=\"10\">";
for ($i=0; $i<sizeof($levels); $i++) {
    list($id,$name) = $levels[$i];
	$select_list .= "<OPTION value='".$id."' SELECTED>".$name."</OPTION>";
}
$select_list .= "</SELECT>";


############## Interface Start ##############
$linterface = new interface_html();
$CurrentPage = "Report_GradingSchemeSummary";
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
# tag information
$TAGS_OBJ[] = array($eReportCard['Reports_GradingSchemeSummary']);
$linterface->LAYOUT_START();



### edit table
if ($lreportcard->hasAccessRight()) {
	$x = '';
	$x .= '<table class="form_table_v30">'."\r\n";
		// view format
		
		// form
		$x .= '<tr>'."\r\n";
			$x .= '<td class="field_title">'.$linterface->RequiredSymbol().$i_ClassLevel.'</td>'."\r\n";
			$x .= '<td>'."\r\n";
				$x .= $select_list."\r\n";
				$x .= $linterface->GET_SMALL_BTN($button_select_all, "button", "javascript:SelectAll(document.form1.elements['TargetID[]'])")."\r\n";
			$x .= '</td>'."\r\n";
		$x .= '</tr>'."\r\n";
	$x .= '</table>'."\r\n";
	$x .= '<br style="clear:both;">'."\r\n";
	$htmlAry['optionTable'] = $x;
	
	
	$htmlAry['submitBtn'] = $linterface->GET_ACTION_BTN($button_submit, "submit", "", "submitBtn");
}
?>


<script language="javascript">
$( document ).ready(function() {
	SelectAll(document.form1.elements['TargetID[]']);
});

function SelectAll(obj)
{
         for (i=0; i<obj.length; i++)
         {
              obj.options[i].selected = true;
         }
}

function checkForm() 
{
	var obj = document.form1;
	var select_obj = obj.elements['TargetID[]'];

	for (var i=0; i<select_obj.length; i++)
	{
		if (select_obj[i].selected)
		{
			return true;
		}
	}

	alert('<?=$i_alert_PleaseSelectClassForm?>');
	return false;
}
</script>
<form id="form1" name="form1" action="generate.php" method="POST" onsubmit="return checkForm();" target="_blank">
	<p class="spacer"></p>
	<div class="table_board">
		<?=$htmlAry['optionTable']?>
		<br style="clear:both;" />
		<?=$linterface->MandatoryField()?>
		<div class="edit_bottom_v30">
			<p class="spacer"></p>
			<?=$htmlAry['submitBtn']?>
			<p class="spacer"></p>
		</div>
	</div>
</form>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>