<?php
// 2016-11-23 Villa	Add SchemeTitle
// 2016-09-30 Villa File Create
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

intranet_auth();
intranet_opendb();


$formIdAry = $_POST['TargetID'];
$numOfForm = count($formIdAry);

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
// $lreportcard = new libreportcard();
$lreportcard = new libreportcardcustom();
$lreportcard->hasAccessRight();


$fcm = new form_class_manage();
$termAry = $fcm->Get_Academic_Year_Term_List($lreportcard->GET_ACTIVE_YEAR_ID());
$formList = $fcm->Get_Form_List();
$formAssoAry = BuildMultiKeyAssoc($formList, 'YearID', $IncludedDBField=array('YearName'));
$termAryAssoAry = BuildMultiKeyAssoc($termAry, 'YearTermID', $IncludedDBField=array('YearTermNameEN','YearTermNameB5'));
$numOfTerm = count($termAry);

// debug_pr($numOfTerm);

// debug_pr($termAryAssoAry);
$dataAry = array();
$titleAry = array();
// debug_pr($numOfForm);
for($i=0; $i<$numOfForm; $i++) { // form-base

	$formId = $formIdAry[$i];
	$report = $lreportcard->Get_Report_List($formId);
	$numOfReport = count($report);
	$countTerm = 0;
	
	for($j=0; $j<$numOfReport; $j++){	//term-base(report which is main_report)

		if($report[$j]['isMainReport'] ==1){
			if($report[$j]['Semester']!='F'){	//check if the report is main term report

				$updatedReport[$i][$j]['ReportID'] = $report[$j]['ReportID'];
				$updatedReport[$i][$j]['ReportTitle'] = $report[$j]['ReportTitle'];
				$updatedReport[$i][$j]['Semester'] = $report[$j]['Semester'];
				$SubjectDataArr = $lreportcard->GET_FROM_SUBJECT_GRADING_SCHEME($formId, 0, $updatedReport[$i][$j]['ReportID']);
				if(isset($SubjectDataArr)){
					foreach($SubjectDataArr as $value){
						$NewSubjectDataArr[] = $value;
					}
				}
				
				$numOfNewSubjectDataArr = count($NewSubjectDataArr);
				for($k=0; $k<$numOfNewSubjectDataArr; $k++){
					$reportArr[$i][$j][$k]['FormID'] = $formId;
				
					$reportArr[$i][$countTerm][$k]['FormName'] = $formAssoAry[$formId]['YearName'];
				
						$reportArr[$i][$countTerm][$k]['ReportID'] = $updatedReport[$i][$j]['ReportID'];
						$reportArr[$i][$countTerm][$k]['ReportTitle'] = $updatedReport[$i][$j]['ReportTitle'];
						$reportArr[$i][$countTerm][$k]['SemesterID'] = $updatedReport[$i][$j]['Semester'];
						
						$YearTermNameEN = $termAryAssoAry[$reportArr[$i][$countTerm][$k]['SemesterID']]['YearTermNameEN'];
						$YearTermNameB5 = $termAryAssoAry[$reportArr[$i][$countTerm][$k]['SemesterID']]['YearTermNameB5'];
						$reportArr[$i][$countTerm][$k]['SemesterName'] = Get_Lang_Selection($YearTermNameB5,$YearTermNameEN);
						if(isset($NewSubjectDataArr[$k]['schemeID'])){
							$reportArr[$i][$countTerm][$k]['schemeID'] = $NewSubjectDataArr[$k]['schemeID'];
							$reportArr[$i][$countTerm][$k]['subjectName'] = $NewSubjectDataArr[$k]['subjectName'];
							$result = $lreportcard->Get_Full_Scheme_Range($reportArr[$i][$countTerm][$k]['schemeID']);
							$numOfResult = count($result);
							for($kk = 0; $kk<$numOfResult; $kk++){
								$reportArr[$i][$countTerm][$k]['Grade'][$kk] = $result[$kk]['Grade'];
								$reportArr[$i][$countTerm][$k]['Marks'][$kk] = $result[$kk]['LowerLimit'];
								$reportArr[$i][$countTerm][$k]['TopPercentage'][$kk] = $result[$kk]['TopPercentage'];
								$reportArr[$i][$countTerm][$k]['SchemeTitle'] = $result[$kk]['SchemeTitle'];
							}
						
						}
					
					}
				$countTerm++; //count how much main term report we get 
					
			}
			$NewSubjectDataArr="";
			$SubjectDataArr="";
			
		}
			
	}
	$report = '';
	
}

$newReportArr = $reportArr;

for ($i=0; $i<$numOfForm; $i++) { //form-base
	foreach($formList as $value){
		if($formIdAry[$i]==$value[0]){
				$exportContentAry[][] = $value[1];
		}
	}
	$checkPrint = false;
	$NewOfnewReportArr = count($newReportArr[$i]);
	for($j=0; $j<$NewOfnewReportArr; $j++){ //term-base
// 		$content[$i][] = $newReportArr[$i][$j]['ReportTitle'];
		$numOfSubject = count($newReportArr[$i][$j]);
		for($k=0; $k<$numOfSubject; $k++){ //subject base
				$numOfMarks = count($newReportArr[$i][$j][$k]['Marks']);
				if($numOfMarks>0){
					$markCheck = array_filter($newReportArr[$i][$j][$k]['Marks']);
				}else{
					$markCheck ='';
				}
				$numOfCheck = count($markCheck);

				if($newReportArr[$i][$j][$k]['TopPercentage'][0]==0 && count($newReportArr[$i][$j][$k]['schemeID'])!='' && $numOfCheck>0){
					
					if($checkPrint==false){
						$title[$i][] = $eReportCard['Term'];
						$title[$i][] = $eReportCard['Subject'];
						$title[$i][] = $eReportCard['SchemeTitle'];
					}
					$content[$i][] = $newReportArr[$i][$j][$k]['SemesterName'];
					$subjectNameMix = explode("<br />",$newReportArr[$i][$j][$k]['subjectName']);
					$subjectNameEn = $subjectNameMix[0];
					$subjectNameCn = $subjectNameMix[1];
					$content[$i][] = Get_Lang_Selection($subjectNameCn,$subjectNameEn);
					$content[$i][] = $newReportArr[$i][$j][$k]['SchemeTitle'];
					$numOfGrade = count($newReportArr[$i][$j][$k]['Grade']);
					for ($ii=0; $ii<$numOfGrade; $ii++){
							
							if($checkPrint==false){ // print the grade with first subject in level mode
								$title[$i][] = $newReportArr[$i][$j][$k]['Grade'][$ii];
								$checked = true; //return value to the outside of the for loop
								
							}
							$content[$i][] = $newReportArr[$i][$j][$k]['Marks'][$ii];
							
					}
					
				}
				else{
					$checked = false;
				}
				if(isset( $title[$i])){
					$exportContentAry[] = $title[$i];
				}
				if(isset($content[$i])){
					$exportContentAry[] = $content[$i];
				}
				
				$content = "";
				$title = "";
				if($checked){
					$checkPrint = true;	//print the Grade Title or not
				}
			
		}
		
	}
	$checkPrint = false;
	$exportContentAry[] = ' ';
}
	
	

## Display the result

	include_once($PATH_WRT_ROOT."includes/libexporttext.php");
	
	$lexport = new libexporttext();
	

	
	// Title of the grandmarksheet
	$filename = "Grading_Scheme_Sammary.csv";
	
	$export_content = $lexport->GET_EXPORT_TXT($exportContentAry, array());
	intranet_closedb();
	
	// Output the file to user browser
	$lexport->EXPORT_FILE($filename, $export_content);

?>