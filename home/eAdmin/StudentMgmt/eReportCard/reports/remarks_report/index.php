<?php
// using :

/***********************************************
 * modification log
 * 	20180723 Bill:  [2017-0901-1525-31265]
 * 		Allow View Group to select all options
 * ***********************************************/

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if ($plugin['ReportCard2008']) {
	include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
	include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
	include_once($PATH_WRT_ROOT."includes/libinterface.php");
	include_once($PATH_WRT_ROOT."includes/form_class_manage_ui.php");
	include_once($PATH_WRT_ROOT."includes/subject_class_mapping_ui.php");
	
	if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") {
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
		$lreportcard = new libreportcardcustom();
	} else {
		$lreportcard = new libreportcard();
	}
	$lreportcard_ui = new libreportcard_ui();
	$linterface = new interface_html();
	
	if ($lreportcard->hasAccessRight())
	{
		############## Interface Start ##############
		$linterface = new interface_html();
		$CurrentPage = "Reports_RemarksReport";
		$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
		# tag information
		$TAGS_OBJ[] = array($eReportCard['Reports_RemarksReport']);
		$linterface->LAYOUT_START();
		#############################################
		
		### TeachingOnly 
		$TeachingOnly = ($lreportcard->IS_ADMIN_USER($UserID) || $lreportcard->IS_VIEW_GROUP_USER_VIEW($_SESSION['UserID']))? 0 : 1;
		
		### Get Form selection (show form which has report card only)
//		$FormArr = $lreportcard->GET_ALL_FORMS($hasTemplateOnly=1);
//		$YearID = $FormArr[0]['ClassLevelID'];
		$FormSelection = $lreportcard_ui->Get_Form_Selection('YearID', $YearID, 'js_Reload_Selection(this.value)', $noFirst=1,0,1,$TeachingOnly);
		
		### Get Report selection
//		$ReportSelection = $lreportcard->Get_Report_Selection($YearID, '', 'ReportID');
		
		### Font size textbox
		$FontSizeTb = '<input id="FontSize" name="FontSize" type="text" class="ratebox" maxlength="2" value="12" />';
?>

<link type="text/css" rel="stylesheet" href="<?=$PATH_WRT_ROOT."templates/2007a/css/"?>ereportcard.css">

<br />
<form name="form1" action="generate.php" method="POST" onsubmit="return checkForm();" target="_blank">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	</tr>
<table>
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>
			<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr>
					<td colspan="2" align="center"><?=$RemarksBox?></td>
				</tr>
				
				<tr id="ViewFormatRow">
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?= $eReportCard['ViewFormat'] ?>
					</td>
					<td width="75%" class='tabletext'>
						<input type="radio" name="ViewFormat" id="ViewHTML" value="html" CHECKED>
							<label for="ViewHTML"><?=$eReportCard['HTML']?></label>
						</input>
						<input type="radio" name="ViewFormat" id="ViewCSV" value="csv">
							<label for="ViewCSV"><?=$eReportCard['CSV']?></label>
						</input>
					</td>
				</tr>
				
				<!-- Form -->
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?= $eReportCard['FormName'] ?>
					</td>
					<td width="75%" class='tabletext'>
						<?= $FormSelection ?>
					</td>
				</tr>
				
				<!-- Class -->
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?= $eReportCard['Class'] ?>
					</td>
					<td width="75%" class='tabletext'>
						<span id="ClassSelectionSpan"></span>
						<span style="vertical-align:bottom;">
							<?= $linterface->GET_SMALL_BTN($button_select_all, "button", "javascript:js_Select_All('YearClassIDArr[]');"); ?>
						</div>
					</td>
				</tr>
				
				<!-- Report -->
				<tr class="tabletext">
					<td class="formfieldtitle" valign="top" width="30%"><?=$eReportCard['ReportCard']?> <span class="tabletextrequire">*</span></td>
					<td>
						<div id="ReportSelectionDiv">
							<?= $ReportSelection ?>
						</div>
					</td>
				</tr>
				
				<!-- Font Size -->
				<tr class="tabletext">
					<td class="formfieldtitle" valign="top" width="30%"><?=$eReportCard['RemarksReport']['RemarksFontSize']?> <span class="tabletextrequire">*</span></td>
					<td><?= $FontSizeTb ?> px</td>
				</tr>
				
				<tr>
					<td colspan="2" class="dotline">
						<img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1">
					</td>
				</tr>
				<tr>
					<td colspan="2" align="center">
						<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "", "submitBtn"); ?>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</form>


<script language="javascript">

$(document).ready( function() {	
	js_Reload_Selection($('select#YearID').val());
});

function js_Reload_Selection(jsYearID)
{
	$('#ReportSelectionDiv').html('').load(
		"../ajax_reload_selection.php", 
		{ 
			RecordType: 'Report',
			YearID: jsYearID,
			ReportID: '',
			SelectionID: 'ReportID'
		},
		function(ReturnData)
		{
			//$('#IndexDebugArea').html(ReturnData);
		}
	);
	
	js_Reload_Class_Selection(jsYearID);
}

function js_Reload_Class_Selection(jsYearID)
{
	$('span#ClassSelectionSpan').html('').load(
		"../ajax_reload_selection.php", 
		{ 
			RecordType: 'Class',
			YearID: jsYearID,
			SelectionID: 'YearClassIDArr[]',
			isMultiple: 1,
			TeachingOnly: <?=$TeachingOnly?>
		},
		function(ReturnData)
		{
			js_Select_All('YearClassIDArr[]');
			//$('#IndexDebugArea').html(ReturnData);
		}
	);
}

function checkForm() 
{
	var obj = document.form1;
	var select_obj = obj.elements['YearClassIDArr[]'];
	var hasSelectedClass = false;
	for (var i=0; i<select_obj.length; i++)
	{
		if (select_obj[i].selected)
		{
			hasSelectedClass = true;
			break;
		}
	}

	if (hasSelectedClass == false)
	{
		alert('<?=$eReportCard['jsSelectClassWarning']?>');
		obj.elements['YearClassIDArr[]'].focus();
		return false;
	}
	
	if (!check_positive_int(document.getElementById('FontSize'), "<?=sprintf($eReportCard['jsSthMustBePositive'], $eReportCard['RemarksReport']['RemarksFontSize'])?>"))
		return false;
	
	return true;
}
</script>


<?
	print $linterface->FOCUS_ON_LOAD("form1.ClassLevelID");
  	$linterface->LAYOUT_STOP();
    } else {
?>
You have no priviledge to access this page.
<?
	}
} else {
?>

<?
}
?>