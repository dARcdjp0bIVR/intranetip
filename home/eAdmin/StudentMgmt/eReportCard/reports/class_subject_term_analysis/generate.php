<?
//using: Bill

//@SET_TIME_LIMIT(1000);

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$lreportcard = new libreportcardcustom();
$lreportcard_ui = new libreportcard_ui();

$x = "";
$emptyDisplay = "---";
$exportArr = array();
$exportColumn = array();

$isFirst = true;
$table_count = 0;

$allColumns = array();
$MSStatArr = array();
$termStatArr = array();
$fullMarkArr = array();
$weightSettings = array();
$statTitleArr = array('最高分', '最低分', '平均分', '及格率');

# POST
$YearID = $_POST['YearID'];
$ReportID = $_POST['ReportID'];
$SubjectIDArr = $_POST['SubjectIDArr'];
$YearClassIDArr = $_POST['YearClassIDArr'];
$exportCSV = $_POST['submit_type'];

// [2015-1209-1703-15206]
# Subject Panel / Has Access Right
if ($_SESSION["SSV_PRIVILEGE"]["reportcard"]["is_subject_panel"] || $lreportcard->hasAccessRight()) 
{
    # Report Template Info
    $ReportTemplate = $lreportcard->returnReportTemplateBasicInfo($ReportID);
    $ReportSemester = $ReportTemplate['Semester'];
    $ReportClassLevel = $ReportTemplate['ClassLevelID'];
     	
	# Semester
    $Semester = $lreportcard->GET_ALL_SEMESTERS();
    if($ReportSemester != 'F'){
      	$Semester = array();
		$Semester[$ReportSemester] = $lreportcard->GET_ALL_SEMESTERS($ReportSemester);
	}
	
	$currentForm = $lreportcard->returnClassLevel($YearID);
	$Subjects = $lreportcard->returnSubjectwOrder($YearID, 0, '', 'b5', '', 0, $ReportID, 0);
	$SubjectFullMark = $lreportcard->returnSubjectFullMark($ReportClassLevel, 1, $SubjectIDArr, 1, $ReportID);

	# Year Class
	$YearClassNameArr = $lreportcard->GET_CLASSES_BY_FORM($YearID, "", 1, 1);
	foreach($YearClassIDArr as $YearClassID){
		$students = $lreportcard->GET_STUDENT_BY_CLASS($YearClassID, $ParStudentIDList="", $isShowBothLangs=0, $withClassNumber=0, $isShowStyle=0, $ReturnAsso=1);
		$classStudentArr[$YearClassID] = $students;
	}
		
	# Related Reports
	$relatedReports = $lreportcard->Get_Related_TermReport_Of_Consolidated_Report($ReportID, $MainReportOnly=1, $ReportColumnID="ALL");
	// For term report, only process 1 report 
	if($ReportTemplate['Semester'] != 'F'){
		$relatedReports = array();
		$relatedReports[0] = $ReportTemplate;
	}
	
	# Loop Reports
	// Build settings array
	for($i=0; $i<count($relatedReports); $i++)
	{
		# Related Report Info
		$relatedReportID = $relatedReports[$i]['ReportID'];
		$TermID = $relatedReports[$i]['Semester'];
		$grading_scheme = $lreportcard->GET_FROM_SUBJECT_GRADING_SCHEME($YearID, 0, $relatedReportID);
		
		# ReportColumns
		$ReportColumn = $lreportcard->returnReportTemplateColumnData($relatedReportID);
		$TermReportColumnArr[$TermID] = BuildMultiKeyAssoc($ReportColumn, array('ReportColumnID'));
		
		$weightSettings[$TermID] = BuildMultiKeyAssoc($lreportcard->returnReportTemplateSubjectWeightData($relatedReportID), array('SubjectID', 'ReportColumnID'));	
				
		$subjectList = array();
		# Loop ReportColumns
		foreach ($ReportColumn as $ReportColumnObj)
		{
			# ReportColumn Info
			$ReportColumnID = $ReportColumnObj['ReportColumnID'];
			$allColumns[$TermID][$ReportColumnID] = $ReportColumnObj;
				
			foreach((array)$YearClassIDArr as $YearClassID){
				$studentList = array_keys((array)$classStudentArr[$YearClassID]);
				$MarkSheetScore = $lreportcard->getMarks($relatedReportID, '', " AND a.StudentID IN ('".implode("','", $studentList)."')");
				$MSScoreArr[$YearClassID][$TermID] = $MarkSheetScore;
				
				foreach((array)$SubjectIDArr as $parentSubjectID){
					$componentSubject = $Subjects[$parentSubjectID];
//					$parentSubjectName = $componentSubject[0];
					$subjectList[] = $parentSubjectID;
					 
					foreach((array)$componentSubject as $subjectComponentID => $componentSubjectName){
						if(!isset($subjectTeacherArr[$YearClassID][$TermID][$parentSubjectID])){
							$subjectTeacher = $lreportcard->returnSubjectTeacher($YearClassID, $parentSubjectID, $relatedReportID, $studentList[0], 1);
							$subjectTeacherArr[$YearClassID][$TermID][$parentSubjectID] = $subjectTeacher[0]['ChineseName'];
						}
						
						$subjectList[] = $subjectComponentID;
						$subjectGrading = $lreportcard->GET_GRADING_SCHEME_MAIN_INFO($grading_scheme[($subjectComponentID? $subjectComponentID : $parentSubjectID)]['schemeID']);
						$fullMarkArr[$TermID][$ReportColumnID][($subjectComponentID? $subjectComponentID : $parentSubjectID)] = $subjectGrading;
				
//						$Marks = array();
//						$minMark = 0;
//						$maxMark = 0;
//						$passRatio = 0.00;
//						$passStudent = 0;
//						$averageMark = 0.00;
//						$totalStudent = 0;
//						foreach((array)$studentList as $currentStudentID){
//							$studentInput = $MarkSheetScore[$currentStudentID][($subjectComponentID? $subjectComponentID : $parentSubjectID)][$ReportColumnID];
//							$subjectGrading = $lreportcard->GET_GRADING_SCHEME_MAIN_INFO($grading_scheme[($subjectComponentID? $subjectComponentID : $parentSubjectID)]['schemeID']);
//							$fullMarkArr[$TermID][$ReportColumnID][($subjectComponentID? $subjectComponentID : $parentSubjectID)] = $subjectGrading;
//						
//							if($studentInput['Grade'] != 'N.A.'){
//								$score = $studentInput['Mark'];
//								$Marks[] = $score;
//								$totalStudent++;
//								
//								if($score > $subjectGrading['PassMark']){
//									$passStudent++;
//								}
//							}
//						}
//						if(count($Marks) > 0){
//							$minMark = min($Marks);		
//							$maxMark = max($Marks);
//							$averageMark = $lreportcard->getAverage($Marks);
//							$averageMark = my_round($averageMark, 2);
//							$passRatio = $passStudent / $totalStudent * 100;
//							$passRatio = my_round($passRatio, 2);
//						}
//						$MSStatArr[$YearClassID][$TermID][$parentSubjectID][$subjectComponentID][$ReportColumnID] = array($maxMark, $minMark, $averageMark, $passRatio);
					}
				}
			}
		}
		
		$subjectList = (array)array_unique((array)$subjectList);
		sort($subjectList);
		$termStatArr[$TermID] = $lreportcard->Get_Mark_Statistics_Info($relatedReportID, $subjectList, 2, 2, 2, 0, 1, "", 0);
	}
	
//	$x .= "<div class='main_container'>";
	
	foreach((array)$allColumns as $termID => $columnInfo){
		$SemesterSq = $lreportcard->Get_Semester_Seq_Number($termID);
		$termCode = "T".$SemesterSq;
		
		foreach((array)$YearClassIDArr as $YearClassID){
			$currentClassName = $YearClassNameArr[$YearClassID];
			$currentClassStudent = $classStudentArr[$YearClassID];
			
			foreach((array)$SubjectIDArr as $parentSubjectID){
				$componentSubject = $Subjects[$parentSubjectID];
				$parentSubjectName = $componentSubject[0];
				$currentSubjectTeacher = $subjectTeacherArr[$YearClassID][$termID][$parentSubjectID];
				$assessmentSq = 1;
				
				$subjectColspan = count($componentSubject);
				$subjectColspan = ($subjectColspan > 1)? ($subjectColspan - 1) : $subjectColspan;
				
				$colWidth = floor(74 / ($subjectColspan * count($columnInfo) + 1 ));
					
//				if(!$isFirst){
//					$x .= "</div>";
//					$x .= "<div class='page_break'>&nbsp;</div>";
//					$x .= "<div class='main_container'>";
//				}
				
				# 1st Header [Start]
				$x .= "<div class='main_container'>";
				$x .= "<table class='border_table' width='100%'>";
				$x .= "<tr>";
				$x .= "<td width='10%' class='leftaligntd'>".$Semester[$termID]."<br>($termCode)</td>";
				$x .= "<td width='16%' class='leftaligntd'>$currentClassName</td>";
				$x .= "<td colspan='".($subjectColspan * count($columnInfo))."'>任教老師： $currentSubjectTeacher</td>";
				$x .= "</tr>";
				$dataArr = array();
				$dataArr[] = $Semester[$termID]."(".$termCode.")";
				$dataArr[] = $currentClassName;
				$dataArr[] = "任教老師：".$currentSubjectTeacher;
				if($isFirst){
					$exportColumn = $dataArr;
					$isFirst = false;
				}
				else {
					$exportArr[] = $dataArr;
				}
				# 1st Header [End]
				
				# 2nd Header [Start]
				$x .= "<tr>";
				$x .= "<td rowspan='3' colspan='2' class='leftaligntd'>$parentSubjectName</td>";
				$dataArr = array();
				$dataArr[] = $parentSubjectName;
				$dataArr[] = "";
				foreach((array)$columnInfo as $currentReportColumnID => $currentReportColumn){
					$currentReportColumnName = $TermReportColumnArr[$termID][$currentReportColumnID]['ColumnTitle'];
					$assessmentCode = "T".$SemesterSq."A".$assessmentSq;
				
					$x .= "<td colspan='$subjectColspan'>".$Semester[$termID]."<br>".$currentReportColumnName."<br>($assessmentCode)</td>";
					$dataArr[] = $Semester[$termID].$currentReportColumnName."(".$assessmentCode.")";
					for($col=1; $col<$subjectColspan; $col++){
						$dataArr[] = "";
					}
					$assessmentSq++;
				}
				$x .= "</tr>";
				$exportArr[] = $dataArr;
				
				$x .= "<tr>";
				$dataArr = array();
				$dataArr[] = "";
				$dataArr[] = "";
				foreach((array)$columnInfo as $currentReportColumnID => $currentReportColumn){
					$currentAssessRatio = ($weightSettings[$termID][$parentSubjectID][$currentReportColumnID]['Weight'] * 100);
					$x .= "<td colspan='$subjectColspan'>$currentAssessRatio</td>";
					$dataArr[] = $currentAssessRatio;
					for($col=1; $col<$subjectColspan; $col++){
						$dataArr[] = "";
					}
				}	
				$x .= "</tr>";
				$exportArr[] = $dataArr;
				
				$x .= "<tr>";
				$dataArr = array();
				$dataArr[] = "";
				$dataArr[] = "";
				foreach((array)$columnInfo as $currentReportColumnID => $currentReportColumn){
					foreach((array)$componentSubject as $subjectComponentID => $subjectName){
						if(!$subjectComponentID && count($componentSubject) > 1)
							continue;
					
						$x .= "<td width='$colWidth%'>$subjectName</td>";
						$dataArr[] = $subjectName;
					}
				}
				$x .= "<td>".$Semester[$termID]."<br>(T$SemesterSq)<br>總分</td>";
				$x .= "</tr>";
				$dataArr[] = $Semester[$termID]."(T".$SemesterSq.")總分";
				$exportArr[] = $dataArr;
				# 2nd Header [End]
				
				$x .= "<tr>";
				$x .= "<td>&nbsp;</td><td class='leftaligntd'>比重</td>";
				$dataArr = array();
				$dataArr[] = "";
				$dataArr[] = "比重";
				foreach((array)$columnInfo as $currentReportColumnID => $currentReportColumn){
					foreach((array)$componentSubject as $subjectComponentID => $subjectName){
						if(!$subjectComponentID && count($componentSubject) > 1)
							continue;
							
						$currentCompRatio = $weightSettings[$termID][$subjectComponentID][null]['Weight'];
						$x .= "<td>$currentCompRatio</td>";
						$dataArr[] = $currentCompRatio;
					}
				}
				$x .= "<td>&nbsp;</td>";
				$x .= "</tr>";
				$dataArr[] = "";
				$exportArr[] = $dataArr;
						
				$x .= "<tr>";
				$x .= "<td>&nbsp;</td><td class='leftaligntd'>滿分</td>";
				$dataArr = array();
				$dataArr[] = "";
				$dataArr[] = "滿分";
				foreach((array)$columnInfo as $currentReportColumnID => $currentReportColumn){
					foreach((array)$componentSubject as $subjectComponentID => $subjectName){
						if(!$subjectComponentID && count($componentSubject) > 1)
							continue;
							
						$x .= "<td>".$fullMarkArr[$termID][$currentReportColumnID][($subjectComponentID? $subjectComponentID : $parentSubjectID)]['FullMark']."</td>";
						$dataArr[] = $fullMarkArr[$termID][$currentReportColumnID][($subjectComponentID? $subjectComponentID : $parentSubjectID)]['FullMark'];
					}
				}
				$x .= "<td>".$SubjectFullMark[$parentSubjectID]."</td>";
				$x .= "</tr>";
				$dataArr[] = $SubjectFullMark[$parentSubjectID];
				$exportArr[] = $dataArr;
				
				$studentCount = 0;
				foreach($currentClassStudent as $currentStudentID => $classInfo){
					$studentName = $classInfo['StudentNameCh'];
					$studentNum = $classInfo['ClassNumber'];
					
					$x .= "<tr>";
					$x .= "<td class='leftaligntd'>$studentNum</td><td class='leftaligntd'>$studentName</td>";
					$dataArr = array();
					$dataArr[] = $studentNum;
					$dataArr[] = $studentName;
					foreach((array)$columnInfo as $currentReportColumnID => $currentReportColumn){
						foreach((array)$componentSubject as $subjectComponentID => $subjectName){
							if(!$subjectComponentID && count($componentSubject) > 1)
								continue;
							
							$marks = $MSScoreArr[$YearClassID][$termID][$currentStudentID][($subjectComponentID? $subjectComponentID : $parentSubjectID)][$currentReportColumnID];
							$markDisplay = $marks['Mark'];
							$inputValid = $marks['Grade'] == "N.A." || $lreportcard->Check_If_Grade_Is_SpecialCase($marks['Grade']) || !isset($markDisplay) || $markDisplay == -1;
							if($inputValid){
								$markDisplay = $emptyDisplay;
							}
							$x .= "<td>$markDisplay</td>";
							$dataArr[] = $markDisplay;
						}
					}
					$marks = $MSScoreArr[$YearClassID][$termID][$currentStudentID][$parentSubjectID][0];
					$markDisplay = $marks['Mark'];
					$inputValid = $marks['Grade'] == "N.A." || $lreportcard->Check_If_Grade_Is_SpecialCase($marks['Grade']) || !isset($markDisplay) || $markDisplay == -1;
					if($inputValid){
						$markDisplay = $emptyDisplay;
					}
					$x .= "<td>$markDisplay</td>";
					$x .= "</tr>";
					$dataArr[] = $markDisplay;
					$exportArr[] = $dataArr;
					
					$studentCount++;
					if($studentCount > 26){
						$x .= "</table>";
						$x .= "</div>";
						$x .= "<div class='page_break'>&nbsp;</div>";
						
						$x .= "<div class='main_container'>";
						$x .= "<table class='border_table'>";
						$x .= "<tr><td width='10%' class='leftaligntd'>".$Semester[$termID]."<br>($termCode)</td><td width='16%' class='leftaligntd'>$currentClassName</td><td colspan='".($subjectColspan * count($columnInfo))."'>&nbsp;</td>";
						$x .= "</tr>";
						
						$studentCount = 0;
					}
				}
				
				$x .= "<tr>";
				$x .= "<td>&nbsp;</td><td colspan='".($subjectColspan * count($columnInfo) + 2)."'>&nbsp;</td>";
				$x .= "</tr>";
				$dataArr = array();
				$dataArr[] = "";
				$exportArr[] = $dataArr;
				
				$totalSummary = $termStatArr[$termID][0][$parentSubjectID][$YearClassID];
				$totalSummaryArr = array($totalSummary['MaxMark'], $totalSummary['MinMark'], $totalSummary['Average_Rounded'], $totalSummary['Nature']['TotalPassPercentage']);
				
				for($i=0; $i<4; $i++){
					$statTitle = $statTitleArr[$i];
					$x .= "<tr>";	
					$x .= "<td colspan='2'>$statTitle</td>";
					$dataArr = array();
					$dataArr[] = $statTitle;
					$dataArr[] = "";
							
					foreach((array)$columnInfo as $currentReportColumnID => $currentReportColumn){
						
						foreach((array)$componentSubject as $subjectComponentID => $subjectName){
							if(!$subjectComponentID && count($componentSubject) > 1)
								continue;
								
							$termSummary = $termStatArr[$termID][$currentReportColumnID][($subjectComponentID? $subjectComponentID : $parentSubjectID)][$YearClassID];
							$termSummaryArr = array($termSummary['MaxMark'], $termSummary['MinMark'], $termSummary['Average_Rounded'], $termSummary['Nature']['TotalPassPercentage']);

//							$statDisplay = $MSStatArr[$YearClassID][$termID][$parentSubjectID][$subjectComponentID][$currentReportColumnID][$i];
							$statDisplay = $termSummaryArr[$i];
							$statDisplay = isset($statDisplay)? $statDisplay : $emptyDisplay;
							$statDisplay = $statDisplay != -1? $statDisplay : $emptyDisplay;
							$x .= "<td>$statDisplay</td>";
							$dataArr[] = $statDisplay;
						}
					}
						
					$statDisplay = $totalSummaryArr[$i];
					$statDisplay = isset($statDisplay)? $statDisplay : $emptyDisplay;
					$statDisplay .= ($i==3? "%" : "");
					$x .= "<td>$statDisplay</td>";
					$x .= "</tr>";
					$dataArr[] = $statDisplay;
					$exportArr[] = $dataArr;
				}				
				$x .= "</table>";
				$x .= "</div>";
				$x .= "<div class='page_break'>&nbsp;</div>";
				$exportArr[] = array("");
			}
		}
	}
	
	if($exportCSV == "1"){
		$lexport = new libexporttext();
		$export_content = $lexport->GET_EXPORT_TXT($exportArr, $exportColumn,  "", "\r\n", "", 0, "11",1);		
		
		intranet_closedb();
		
		// Output CSV File
		$filename = "Class_Subject_Term_Analysis.csv";
		$filename = str_replace(' ', '_', $filename);
		$lexport->EXPORT_FILE($filename, $export_content);
	
	} else {
		
		$x .= "</div>";
		echo $x;
		
		echo'<style>';
		include_once($PATH_WRT_ROOT."file/reportcard2008/templates/general.css");
		echo '</style>';		
		
		$style_css  = "html, body { margin:0px; padding:0px; }\n";
		$style_css .= "body { font-family:'Times New Roman, Times, serif, 新細明體'; font-size:13px }\n";
//		$style_css .= ".main_container {display:block; width:750px; height:850px; margin:auo; padding:120px 20px; position:relative; padding-bottom:100px; clear:both; border:0px solid green;}\n";
		$style_css .= ".main_container { display:block; width:680px; height:850px; margin:auto; padding:60px 20px; position:relative; padding-bottom:20px; clear:both; border:0px solid green; }\n";
		$style_css .= ".page_break {page-break-after:always; margin:0; padding:0; line-height:0; font-size:0; height:0;}";
		
		$style_css .= ".border_table { width: 100%; margin-bottom:60px; }";
		$style_css .= ".border_table td { text-align:center; }";
		$style_css .= ".border_table > tbody > tr:first-child td { vertical-align:middle; }";
		$style_css .= ".border_table .leftaligntd { text-align:left; padding-left:4px; }";
		
		echo "<style>";
		echo $style_css;
		echo "</style>";	
		
		intranet_closedb();
	}
	
} else {
	
	echo "You have no priviledge to access this page.";
	intranet_closedb();
	die();
	
}

?>