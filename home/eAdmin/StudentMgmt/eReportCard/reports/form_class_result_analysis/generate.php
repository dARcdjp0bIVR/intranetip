<?
//using: Bill

//@SET_TIME_LIMIT(1000);

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$lreportcard = new libreportcardcustom();

$x = "";
$emptyDisplay = "---";
$exportArr = array();
$exportColumn = array();

$isFirst = true;
$columnCount = 0;

$MarkArr = array();
$classStudentArr = array();	

# POST
$YearID = $_POST['YearID'];
$ReportID = $_POST['ReportID'];
$SubjectIDArr = $_POST['SubjectIDArr'];
$YearClassIDArr = $_POST['YearClassIDArr'];
$exportCSV = $_POST['submit_type'];

// [2015-1209-1703-15206]
if ($_SESSION["SSV_PRIVILEGE"]["reportcard"]["is_subject_panel"] || $lreportcard->hasAccessRight()) {
		     	
	# Report Template Info
	$ReportTemplate = $lreportcard->returnReportTemplateBasicInfo($ReportID);
	$ReportSemester = $ReportTemplate['Semester'];
	    
	# Semester
	$Semester = $lreportcard->GET_ALL_SEMESTERS();
	if($ReportSemester != 'F'){
	  	$Semester = array();
		$Semester[$ReportSemester] = $lreportcard->GET_ALL_SEMESTERS($ReportSemester);
	}
	
	$currentForm = $lreportcard->returnClassLevel($YearID);
	$Subjects = $lreportcard->returnSubjectwOrder($YearID, 0, '', 'b5', '', 0, $ReportID);
		
	# Year Class
	$YearClassNameArr = $lreportcard->GET_CLASSES_BY_FORM($YearID, "", 1, 1);
	foreach((array)$YearClassIDArr as $YearClassID){
		$classStudents = $lreportcard->GET_STUDENT_BY_CLASS($YearClassID, "", 0, 0, 0, 1);
		$classStudentArr[$YearClassID] = $classStudents;
	}
	
	# Related Reports
	$relatedReports = $lreportcard->Get_Related_TermReport_Of_Consolidated_Report($ReportID, $MainReportOnly=1, $ReportColumnID="ALL");
	// For term report, only process 1 report 
	if($ReportTemplate['Semester'] != 'F'){
		$relatedReports = array();
		$relatedReports[0] = $ReportTemplate;
	} else {
		$relatedReports[] = $ReportTemplate;
	}

	# Loop Reports
	// Build settings array
	for($i=0; $i<count($relatedReports); $i++){
		
		# Related Report Info
		$relatedReportID = $relatedReports[$i]['ReportID'];
		$TermID = $relatedReports[$i]['Semester'];
			
		# ReportColumns
		$ReportColumn = $lreportcard->returnReportTemplateColumnData($relatedReportID);
		if($TermID != 'F')
			$TermReportColumnArr[$TermID] = BuildMultiKeyAssoc($ReportColumn, array('ReportColumnID'));
			
		foreach ((array)$ReportColumn as $ReportColumnObj){
			if($TermID == 'F')
				break;
				
			$columnCount++;
		}
		
		# Build Mark Array
		foreach((array)$YearClassIDArr as $YearClassID){
			$studentIDArr = array_keys((array)$classStudentArr[$YearClassID]);
					
			$currentMarks = $lreportcard->getMarks($relatedReportID, '', " AND a.StudentID IN ('".implode("','", $studentIDArr)."')");
			$MarkArr[$YearClassID][$TermID] = $currentMarks;
		}
		$columnCount++;
	}

	$column_width = floor(74 / $columnCount);
//	$x .= "<div class='main_container'>";
	
	# Loop Subjects
	foreach((array)$Subjects as $parentSubjectID => $parentSubject){
		if(!in_array($parentSubjectID, $SubjectIDArr))
			continue;
		
		foreach((array)$parentSubject as $componentSubjectID => $subjectName){
			$displaySubjectName = $componentSubjectID? $parentSubject[0]."_".$subjectName : $subjectName;
			$componentSubjectID = $componentSubjectID? $componentSubjectID : $parentSubjectID;

			foreach((array)$YearClassIDArr as $YearClassID){
				$colCount = 0;
				
//				if(!$isFirst){
//					$x .= "</div>";
//					$x .= "<div class='page_break'>&nbsp;</div>";
//					$x .= "<div class='main_container'>";
//				}
				
				# 1st Header [Start]
				$x .= "<div class='main_container'>";
				$x .= "<table class='border_table'>";
				$x .= "<tr>";
				$x .= "<td width='6%'>$displaySubjectName</td><td width='20%'>".$YearClassNameArr[$YearClassID]."</td>";
				$dataArr = array();
				$dataArr[] = $displaySubjectName;
				$dataArr[] = $YearClassNameArr[$YearClassID];
				
				foreach($TermReportColumnArr as $termID => $TermReport){
					$SemesterSq = $lreportcard->Get_Semester_Seq_Number($termID);
					$assessmentSq = 1;
								
					foreach($TermReport as $ReportColumnID => $reportColumn){
						$assessmentCode = "T".$SemesterSq."A".$assessmentSq;
					
						$x .= "<td colspan='3' width='$column_width%'>".$Semester[$termID]."<br>".$reportColumn['ColumnTitle']."<br>($assessmentCode)</td>";
						$dataArr[] = $Semester[$termID].$reportColumn['ColumnTitle']."(".$assessmentCode.")";
						$dataArr[] = "";
						$dataArr[] = "";
						$assessmentSq++;
						$colCount++;
					}
					
					$assessmentCode = "T".$SemesterSq;
						
					$x .= "<td colspan='3' width='$column_width%'>".$Semester[$termID]."<br>($assessmentCode)</td>";
					$dataArr[] = $Semester[$termID]."(".$assessmentCode.")";
					$dataArr[] = "";
					$dataArr[] = "";
					$colCount++;
				}
				
				if($ReportTemplate['Semester'] == 'F'){
					$x .= "<td colspan='3' width='$column_width%'>年終<br>(Annual)</td>";
					$dataArr[] = "年終(Annual)";
					$dataArr[] = "";
					$dataArr[] = "";
					$colCount++;
				}
				$x .= "</tr>";				
				if($isFirst){
					$exportColumn = $dataArr;
					$isFirst = false;
				}
				else {
					$exportArr[] = $dataArr;
				}
				# 1st Header [End]
				
				# 2nd Header [Start]
				$x .= "<tr>";
				$x .= "<td>&nbsp;</td><td>&nbsp;</td>";
				$dataArr = array();
				$dataArr[] = "";
				$dataArr[] = "";
				for($i=0; $i<$colCount; $i++){
					$x .= "<td>分<br>數</td><td>班<br>次</td><td>級<br>次</td>";
					$dataArr[] = "分數";
					$dataArr[] = "班次";
					$dataArr[] = "級次";
				}
				$x .= "</tr>";
				$exportArr[] = $dataArr;
				# 2nd Header [End]
				
				# Loop Students
				$studentCount = 0;
				foreach($classStudentArr[$YearClassID] as $studentid => $currentStudent){
					$x .= "<tr>";
					$x .= "<td>".$currentStudent['ClassNumber']."</td><td>".$currentStudent['StudentNameCh']."</td>";
					$dataArr = array();
					$dataArr[] = $currentStudent['ClassNumber'];
					$dataArr[] = $currentStudent['StudentNameCh'];

					foreach($TermReportColumnArr as $termID => $TermReport){
						$markInfo = $MarkArr[$YearClassID][$termID][$studentid][$componentSubjectID];
						
						foreach($TermReport as $ReportColumnID => $reportColumn){
							$colValid = $markInfo[$ReportColumnID]['Grade'] != 'N.A.';
							$colMark = $markInfo[$ReportColumnID]['Mark'];
							$colMark = $colValid && $colMark >= 0? $colMark : $emptyDisplay;
							$colClassOrder = $markInfo[$ReportColumnID]['OrderMeritClass'];
							$colClassOrder = $colValid && $colClassOrder > 0? $colClassOrder : $emptyDisplay;
							$colFormOrder = $markInfo[$ReportColumnID]['OrderMeritForm'];
							$colFormOrder = $colValid && $colFormOrder > 0? $colFormOrder : $emptyDisplay;
								
							$x .= "<td>$colMark</td>";
							$x .= "<td>$colClassOrder</td>";
							$x .= "<td>$colFormOrder</td>";
							$dataArr[] = $colMark;
							$dataArr[] = $colClassOrder;
							$dataArr[] = $colFormOrder;
						}
						
						$colValid = $markInfo[0]['Grade'] != 'N.A.';
						$colMark = $markInfo[0]['Mark'];
						$colMark = $colValid && $colMark >= 0? $colMark : $emptyDisplay;
						$colClassOrder = $markInfo[0]['OrderMeritClass'];
						$colClassOrder = $colValid && $colClassOrder > 0? $colClassOrder : $emptyDisplay;
						$colFormOrder = $markInfo[0]['OrderMeritForm'];
						$colFormOrder = $colValid && $colFormOrder > 0? $colFormOrder : $emptyDisplay;
							
						$x .= "<td>$colMark</td>";
						$x .= "<td>$colClassOrder</td>";
						$x .= "<td>$colFormOrder</td>";
						$dataArr[] = $colMark;
						$dataArr[] = $colClassOrder;
						$dataArr[] = $colFormOrder;
					}
					
					if($ReportTemplate['Semester'] == 'F'){
						$markInfo = $MarkArr[$YearClassID]['F'][$studentid][$componentSubjectID];
						$colValid = $markInfo[0]['Grade'] != 'N.A.';
						$colMark = $markInfo[0]['Mark'];
						$colMark = $colValid && $colMark >= 0? $colMark : $emptyDisplay;
						$colClassOrder = $markInfo[0]['OrderMeritClass'];
						$colClassOrder = $colValid && $colClassOrder > 0? $colClassOrder : $emptyDisplay;
						$colFormOrder = $markInfo[0]['OrderMeritForm'];
						$colFormOrder = $colValid && $colFormOrder > 0? $colFormOrder : $emptyDisplay;
							
						$x .= "<td>$colMark</td>";
						$x .= "<td>$colClassOrder</td>";
						$x .= "<td>$colFormOrder</td>";
						$dataArr[] = $colMark;
						$dataArr[] = $colClassOrder;
						$dataArr[] = $colFormOrder;
					}
					
					$x .= "</tr>";
					$exportArr[] = $dataArr;
					
					$studentCount++;
					if($studentCount > 31){
						$x .= "</table>";
						$x .= "</div>";
						$x .= "<div class='page_break'>&nbsp;</div>";
						
						$x .= "<div class='main_container'>";
						$x .= "<table class='border_table'>";
						$x .= "<tr>";
						$x .= "<td width='6%'>$displaySubjectName</td><td width='20%'>".$YearClassNameArr[$YearClassID]."</td>";
						for($i=0; $i<$colCount; $i++){
							$x .= "<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>";
						}
						$x .= "</tr>";
						$x .= "<tr>";
						$x .= "<td width='6%'>&nbsp;</td><td width='20%'>&nbsp;</td>";
						for($i=0; $i<$colCount; $i++){
							$x .= "<td>分<br>數</td><td>班<br>次</td><td>級<br>次</td>";
						}
						$x .= "</tr>";
						
						$studentCount = 0;
					}
				}
				$x .= "</table>";
				$x .= "</div>";
				$x .= "<div class='page_break'>&nbsp;</div>";
				$exportArr[] = array("");
			}
		}
	}
	
	if($exportCSV == "1"){
		$lexport = new libexporttext();
		$export_content = $lexport->GET_EXPORT_TXT($exportArr, $exportColumn,  "", "\r\n", "", 0, "11",1);		
		
		intranet_closedb();
		
		// Output CSV File
		$filename = "Form_Class_Result_Analysis.csv";
		$filename = str_replace(' ', '_', $filename);
		$lexport->EXPORT_FILE($filename, $export_content);
	
	} else {
		
//		$x .= "</div>";
		echo $x;
		
		echo'<style>';
		include_once($PATH_WRT_ROOT."file/reportcard2008/templates/general.css");
		echo '</style>';	
		
		$style_css  = "html, body { margin:0px; padding:0px; }\n";
		$style_css .= "body { font-family:'Times New Roman, Times, serif, 新細明體'; font-size:13px }\n";
//		$style_css .= ".main_container {display:block; width:750px; height:850px; margin:auo; padding:120px 20px; position:relative; padding-bottom:100px; clear:both; border:0px solid green;}\n";
		$style_css .= ".main_container { display:block; width:680px; height:850px; margin:auto; padding:60px 20px; position:relative; padding-bottom:20px; clear:both; border:0px solid green; }\n";
		$style_css .= ".page_break {page-break-after:always; margin:0; padding:0; line-height:0; font-size:0; height:0;}";
		
		$style_css .= ".border_table { width: 100%; margin-bottom:60px; }";
		$style_css .= ".border_table td { text-align:center; }";
		$style_css .= ".border_table > tbody > tr:first-child td { height:45px; vertical-align:middle; }";
		
		echo "<style>";
		echo $style_css;
		echo "</style>";		
		
		intranet_closedb();
	}
	
} else {
	
	echo "You have no priviledge to access this page.";
	intranet_closedb();
	die();
	
}

?>