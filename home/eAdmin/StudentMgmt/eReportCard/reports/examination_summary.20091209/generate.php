<?
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

intranet_auth();
intranet_opendb();

$ViewFormat = $_POST['ViewFormat'];
$TermID = $_POST['TermID'];
$Level = $_POST['Level'];
$TargetIDArr = $_POST['TargetID'];

if ($plugin['ReportCard2008']) {
	include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
	
	if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
	{
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
		$lreportcard = new libreportcardcustom();
	} else {
		$lreportcard = new libreportcard();
	}
	
	if ($lreportcard->hasAccessRight()) 
	{
		# $subjectPrizeInfoArr[$SubjectID][$StudentID][Mark, Order, ...] = value
		//$subjectPrizeInfoArr = array();
		
		# Report Title
		//$ActiveYear = $lreportcard->GET_ACTIVE_YEAR("-");
		$ObjAcademicYear = new academic_year($lreportcard->schoolYearID);
		$ActiveYear = $ObjAcademicYear->Get_Academic_Year_Name();
		$SemTitle = $lreportcard->returnSemesters($TermID);
		//$ReportType = $eReportCard['Reports_SubjectPrizeReport'];
		//$ReportTitle = $ActiveYear." ".$SemTitle." ".$ReportType;
		
		$numOfTarget = count($TargetIDArr);
		for ($j=0; $j<$numOfTarget; $j++)
		{
			if ($Level == "Class")
			{
				$thisClassID = $TargetIDArr[$j];
				$thisClassLevelID = $lreportcard->Get_ClassLevel_By_ClassID($thisClassID);
				//$thisOrderField = 'OrderMeritClass';
			}
			else if ($Level == "Form")
			{
				$thisClassLevelID = $TargetIDArr[$j];
				$thisClassID = '';
				//$thisOrderField = 'OrderMeritForm';
			}
			
			# Get Report Info
			$thisReportInfoArr = $lreportcard->GET_REPORT_TYPES($thisClassLevelID, $ReportID="", $TermID);
			$thisReportID = $thisReportInfoArr[0]['ReportID'];
			
			$thisStudentInfoArr = $lreportcard->GET_STUDENT_BY_CLASSLEVEL($thisReportID, $thisClassLevelID, $thisClassID);

			// ignore class / form with no student
			if (!is_array($thisStudentInfoArr) || count($thisStudentInfoArr)==0)
				continue;
				
			# Get MarkAry
			$MarkAry = $lreportcard->getMarks($thisReportID); 
			
			# Get Grading Scheme Info 
			$GradingInfo = $lreportcard->Get_Form_Grading_Scheme($thisClassLevelID); 
			
			# Get Subject Code ID Mapping
			$SubjectIDCodeMap = $lreportcard->GET_SUBJECTS_CODEID_MAP();
			

			# Loop each student		
			$numOfStudent = count($thisStudentInfoArr);			
			for ($i=0; $i<$numOfStudent; $i++)
			{
				$thisStudentID = $thisStudentInfoArr[$i]["UserID"];

				$SubjectColumnArr = $MarkAry[$thisStudentID];
				$thisClassInfo = $lreportcard->Get_Student_Class_ClassLevel_Info($thisStudentID);
				$thisClassID = $Level == "Class"?$thisClassID:$thisClassInfo["YearClassID"];
				$thisClassName = $thisClassInfo["ClassTitleEn"];
				
				# Get Other Info (e.g. Attendance, Conduct)
				$thisOtherInfo = $lreportcard->getReportOtherInfoData($thisReportID,$thisStudentID); 
				$last_sem = $lreportcard->Get_Last_Semester_Of_Report($thisReportID);
				$resultArr[$thisClassID][$thisStudentID]["OtherInfo"] = $thisOtherInfo[$thisStudentID][$last_sem];
								
				# Get Grand Average / OrderMeritForm
				$GrandMarks = $lreportcard->getReportResultScore($thisReportID, 0, $thisStudentID);
				$resultArr[$thisClassID][$thisStudentID]["GrandAverage"] = 	$GrandMarks["GrandAverage"];
				$resultArr[$thisClassID][$thisStudentID]["OrderMeritForm"] = 	$GrandMarks["OrderMeritForm"];
								
				foreach((array)$SubjectColumnArr as $thisSubjectID => $thisColumnInfo)
				{
					$thisSchemeID = $GradingInfo[$thisSubjectID]['SchemeID'];
					$thisMark = $thisColumnInfo[0]["RawMark"];
					$thisGrade = $lreportcard->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($thisSchemeID, $thisMark);
					$thisSubjectCode = $lreportcard->GET_SUBJECT_NAME_LANG($thisSubjectID,"EN",1);

					if(!$lreportcard->CHECK_SUBJECT_IS_COMPONENT_SUBJECT($thisSubjectID))
					{
						$resultArr[$thisClassID][$thisStudentID]["SubjectMarks"][$thisSubjectCode]['Mark'] = $thisMark;
						$resultArr[$thisClassID][$thisStudentID]["SubjectMarks"][$thisSubjectCode]['Grade'] = $thisGrade;
						$resultArr[$thisClassID][$thisStudentID]["SubjectMarks"][$thisSubjectCode]['Rank'] = $thisColumnInfo[0]["OrderMeritForm"];
						//$resultArr[$thisClassID][$thisStudentID]["ClassName"] = $thisClassName;
					}
				}
			}
		}
				//debug_pr($resultArr);
		## Display the result
		if ($ViewFormat == 'html')
		{
			$thead .= "<thead>\n";
				$thead .= "<tr class='summary_header'>\n";
					$thead .= "<th>&nbsp;</th>\n";
					for($i=0;$i<3;$i++)
					{
						$thead .= "<th width='45px'>&nbsp;</th>\n";
						$thead .= "<th width='30px' align='left' >".$eReportCard['ExaminationSummary']['Subj']."</th>\n";
						$thead .= "<th width='45px' align='right'>".$eReportCard['Mark']."</th>\n";
						$thead .= "<th width='45px' align='center'>".$eReportCard['Grade']."</th>\n";
						$thead .= "<th width='35px' align='right'>".$eReportCard['ExaminationSummary']['Rank']."</th>\n";
					}
				$thead .= "</tr>\n";
			$thead .= "</thead>\n";
			
			$studentCtr=0;
			$thisClassName='';
			foreach((array)$resultArr as $thisClassID => $StudentSummary)
			{
				foreach($StudentSummary as $thisStudentID => $thisSummaryDetail)
				{ 
					# Class Student Info 
					$thisStudentInfoArr = $lreportcard->Get_Student_Class_ClassLevel_Info($thisStudentID);
					$thisClassName = $thisStudentInfoArr[0]['ClassName'];
					$thisClassNumber = $thisStudentInfoArr[0]['ClassNumber'];
					
					$lu = new libuser($thisStudentID);	
					$ClassInfo = '';
					$ClassInfo .= "<table border='0' width='100%' cellpadding='0' cellspacing='0'>";
						$ClassInfo .= "<tr>";
							$ClassInfo .= "<td>";
								$ClassInfo .= $thisClassNumber."&nbsp;".Get_Lang_Selection($lu->ChineseName,$lu->EnglishName)."<br><br>";
							$ClassInfo .= "</td>";
						$ClassInfo .= "</tr>";
						$ClassInfo .= "<tr>";
							$ClassInfo .= "<td>";
								$ClassInfo .= $eReportCard['ExaminationSummary']['Year'].":".$lreportcard->GET_ACTIVE_YEAR_NAME();	
								$ClassInfo .= "<br>".$thisClassName;
							$ClassInfo .= "</td>";
						$ClassInfo .= "</tr>";
					$ClassInfo .= "</table>"; 
						
					# Subject Marks 	
					$SubjMarks = '';
					$SubjMarks .= "<table border='0' width='100%' cellpadding='0' cellspacing='0'>";
						$ctr=0;
						foreach((array)$thisSummaryDetail["SubjectMarks"] as $thisSubjCode => $thisSubjDetail)
						{
							if($ctr%3 == 0)	$SubjMarks .= "<tr>";
							$SubjMarks .= "<td width='45px'>&nbsp;</td>";
							$SubjMarks .= "<td width='25px' align='left'>".$thisSubjCode."&nbsp;</td>";
							$SubjMarks .= "<td width='45px' align='right'>".$thisSubjDetail["Mark"]."&nbsp;</td>";
							$SubjMarks .= "<td width='45px' align='center'>".$thisSubjDetail["Grade"]."&nbsp;</td>";
							$SubjMarks .= "<td width='35px' align='right'>".$thisSubjDetail["Rank"]."&nbsp;</td>";
							if($ctr%3 == 2)	$SubjMarks .= "</tr>";
							$ctr++;
						}
					$SubjMarks .= "</table>"; 
					
					
					
					# Main Conduct
					$OtherInfoAry = array();
					if(is_array($thisSummaryDetail["OtherInfo"])) $OtherInfoAry = $thisSummaryDetail["OtherInfo"];
					$ConductInfo = $eReportCard['ExaminationSummary']['Conduct']." ".$OtherInfoAry["Conduct"];
					
					# Other Info
					$GrandInfo = '';
					$GrandInfo .= "<table border='0' width='100%' cellpadding='0' cellspacing='0'>";
						$GrandInfo .= "<tr>";
							$GrandInfo .= "<td>".$eReportCard["ExaminationSummary"]["AverageMark"]."  ".$thisSummaryDetail["GrandAverage"]."</td>";
							$GrandInfo .= "<td>".$eReportCard["ExaminationSummary"]["Position"]."  ".$thisSummaryDetail["OrderMeritForm"]."</td>";
						$GrandInfo .= "</tr>";
						$GrandInfo .= "<tr>";
							$GrandInfo .= "<td>".$eReportCard["ExaminationSummary"]["Late"]." = ".($OtherInfoAry["Time Late"]>0?$OtherInfoAry["Time Late"]:'0')." ".$eReportCard["ExaminationSummary"]["times"]."</td>";
							$GrandInfo .= "<td>".$eReportCard["ExaminationSummary"]["Absent"]." = ".($OtherInfoAry["Days Absent"]>0?$OtherInfoAry["Days Absent"]:'0')." ".$eReportCard["ExaminationSummary"]["days"]."</td>";
						$GrandInfo .= "</tr>";
						$GrandInfo .= "<tr>";
							$GrandInfo .= "<td width='25%'>".$eReportCard["ExaminationSummary"]["Cd1"]." = ".(trim($OtherInfoAry["Diligence"])?$lreportcard->Get_Conduct_Wordings($OtherInfoAry["Diligence"]):' - ')."</td>";
							$GrandInfo .= "<td width='25%'>".$eReportCard["ExaminationSummary"]["Cd2"]." = ".(trim($OtherInfoAry["Discipline"])?$lreportcard->Get_Conduct_Wordings($OtherInfoAry["Discipline"]):' - ')."</td>";
							$GrandInfo .= "<td width='25%'>".$eReportCard["ExaminationSummary"]["Cd3"]." = ".(trim($OtherInfoAry["Manner"])?$lreportcard->Get_Conduct_Wordings($OtherInfoAry["Manner"]):' - ')."</td>";
							$GrandInfo .= "<td width='25%'>".$eReportCard["ExaminationSummary"]["Cd4"]." = ".(trim($OtherInfoAry["Sociability"])?$lreportcard->Get_Conduct_Wordings($OtherInfoAry["Sociability"]):' - ')."</td>";
						$GrandInfo .= "</tr>";
					$GrandInfo .= "</table>";
					
					
					$page_break_css = ($studentCtr++)%5==4?"style='page-break-after:always'":"";
					$table .= "<table id='ResultTable' class='GrandMSContentTable' width='80%' border='0' cellpadding='2' cellspacing='0' $page_break_css>\n";
						$table .= $thead;	
						$table .= "<tbody>\n";
							$table .="<tr>";				
								$table .="<td>";		
									$table .= $ClassInfo;		
								$table .="</td>";							
								$table .="<td colspan='15' valign='top'>";
									$table .=$SubjMarks;				
								$table .="</td>";											
							$table .="</tr>";	
							$table .="<tr>";				
								$table .="<td valign='bottom' colspan=2>";		
									$table .= $ConductInfo;		
								$table .="</td>";							
								$table .="<td colspan='15'>";
									$table .=$GrandInfo;				
								$table .="</td>";											
							$table .="</tr>";	
						$table .= "</tbody>\n";
					$table .= "</table><br><br>\n";
				}
			}
			
			$css = $lreportcard->Get_GrandMS_CSS();
			
			$allTable = '<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td>'.$table.'</td>
							</tr>
						</table>';
			
			echo $lreportcard->Get_Report_Header($ReportTitle);
			echo $table.$css;
			echo $lreportcard->Get_Report_Footer();
		}
		else if ($ViewFormat == 'csv')
		{
			include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
			include_once($PATH_WRT_ROOT."includes/libexporttext.php");
			
			$ExportHeaderArr = array();
			$ExportContentArr = array();
			$lexport = new libexporttext();
			
			foreach((array)$resultArr as $thisClassID => $StudentSummary)
			{
				$ExportClassArr= array();
				foreach($StudentSummary as $thisStudentID => $thisSummaryDetail)
				{ 
					$ExportRowArr = array();
					# Class Student Info 
					$lu = new libuser($thisStudentID);
					$thisEnglishName = $lu->EnglishName;
					$thisChineseName = $lu->ChineseName;
					
					$thisStudentInfoArr = $lreportcard->Get_Student_Class_ClassLevel_Info($thisStudentID);
					$thisClassName = $thisStudentInfoArr[0]['ClassName'];
					$thisClassNumber = $thisStudentInfoArr[0]['ClassNumber'];
					
					$thisClassLevelName = $thisStudentInfoArr[0]['ClassLevelName'];
						//debug_pr($StudentInfoArr);
					if(!isset($SubjList[$thisClassLevelName]))
					{
						$ExportHeaderArr = array();
						$ExportHeaderArr[] = $eReportCard['FormName'];
						$ExportHeaderArr[] = $eReportCard['Class'];
						$ExportHeaderArr[] = $eReportCard['Template']['StudentInfo']['ClassNo'];
						$ExportHeaderArr[] = $eReportCard['StudentNameEn'];
						$ExportHeaderArr[] = $eReportCard['StudentNameCh'];						
					}
					
					$ExportRowArr[] = $thisClassLevelName;
					$ExportRowArr[] = $thisClassName;
					$ExportRowArr[] = $thisClassNumber;
					$ExportRowArr[] = $thisEnglishName;
					$ExportRowArr[] = $thisChineseName;
					
					foreach((array)$thisSummaryDetail["SubjectMarks"] as $thisSubjCode => $thisSubjDetail)
					{
						if(!isset($SubjList[$thisClassLevelName]))
						{
							$ExportHeaderArr[] = $thisSubjCode." ".$eReportCard["ExaminationSummary"]['Mark'];
							$ExportHeaderArr[] = $thisSubjCode." ".$eReportCard["ExaminationSummary"]['Grade'];
							$ExportHeaderArr[] = $thisSubjCode." ".$eReportCard["ExaminationSummary"]['Rank'];
						}
						
						$ExportRowArr[] = $thisSubjDetail['Mark'];
						$ExportRowArr[] = $thisSubjDetail['Grade'];
						$ExportRowArr[] = $thisSubjDetail['Rank'];
					}
					
					if(!isset($SubjList[$thisClassLevelName]))
					{
						$ExportHeaderArr[] = $eReportCard['ExaminationSummary']['Conduct'];
						$ExportHeaderArr[] = $eReportCard['ExaminationSummary']['AverageMark'];
						$ExportHeaderArr[] = $eReportCard['ExaminationSummary']['Position'];
						$ExportHeaderArr[] = $eReportCard['ExaminationSummary']['Late'];
						$ExportHeaderArr[] = $eReportCard['ExaminationSummary']['Absent'];
						$ExportHeaderArr[] = $eReportCard['ExaminationSummary']['Cd1'];
						$ExportHeaderArr[] = $eReportCard['ExaminationSummary']['Cd2'];
						$ExportHeaderArr[] = $eReportCard['ExaminationSummary']['Cd3'];
						$ExportHeaderArr[] = $eReportCard['ExaminationSummary']['Cd4'];
						$SubjList[$thisClassLevelName] = true;
					}
					
					
					$OtherInfoAry = array();
					if(is_array($thisSummaryDetail["OtherInfo"])) $OtherInfoAry = $thisSummaryDetail["OtherInfo"];
					$ExportRowArr[] = $OtherInfoAry["Conduct"];
					$ExportRowArr[] = $thisSummaryDetail["GrandAverage"];
					$ExportRowArr[] = $thisSummaryDetail["OrderMeritForm"];
					$ExportRowArr[] = $OtherInfoAry["Time Late"];
					$ExportRowArr[] = $OtherInfoAry["Days Absent"];
					$ExportRowArr[] = (trim($OtherInfoAry["Diligence"])?$lreportcard->Get_Conduct_Wordings($OtherInfoAry["Diligence"]):' - ');
					$ExportRowArr[] = (trim($OtherInfoAry["Discipline"])?$lreportcard->Get_Conduct_Wordings($OtherInfoAry["Discipline"]):' - ');
					$ExportRowArr[] = (trim($OtherInfoAry["Manner"])?$lreportcard->Get_Conduct_Wordings($OtherInfoAry["Manner"]):' - ');
					$ExportRowArr[] = (trim($OtherInfoAry["Sociability"])?$lreportcard->Get_Conduct_Wordings($OtherInfoAry["Sociability"]):' - ');
					
					$ExportClassArr[] = $ExportRowArr;		
							
				}
				array_unshift($ExportClassArr,$ExportHeaderArr);
				$ExportClassArr[] = array("");
				$ExportContentArr = array_merge($ExportContentArr,$ExportClassArr);
			}
			
			
			// Title of the grandmarksheet
			//$ReportTitle = str_replace(" ", "_", $ReportTitle);
			$filename = intranet_undo_htmlspecialchars("examiniation_summary.csv");
			
			$export_content = $lexport->GET_EXPORT_TXT($ExportContentArr,'');
			
			intranet_closedb();
			
			// Output the file to user browser
			$lexport->EXPORT_FILE($filename, $export_content);
		}
	}
}


?>