<?php
#  Editing by  

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libteaching.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if (!$plugin['ReportCard2008'])
{
	header ("Location: /");
	intranet_closedb();
	exit();
}
 
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
{
	include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
	$lreportcard = new libreportcardcustom();
} else {
	$lreportcard = new libreportcard();
}

if (!$lreportcard->hasAccessRight()) 
{
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lreportcard_ui = new libreportcard_ui();
$linterface = new interface_html();

$CurrentPage = "Reports_PrintArchivedReport";
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();

# tag information
$TAGS_OBJ[] = array($eReportCard['Reports_PrintArchivedReport'], "", 0);
$linterface->LAYOUT_START();

echo $lreportcard_ui->Get_Print_Archived_Report_Index();

?>

<script>
$(function(){
	js_Toggle_Select_Method();
	js_Reload_Class_Selection();
})

function js_Toggle_Select_Method()
{
	if($("#Method1").attr("checked"))
	{
		$(".UserLoginTr").show();
		$(".ClassHistoryTr").hide();
		$("#UserLogin").attr("disabled","")
		$("#StudentID").attr("disabled","disabled")
	}
	else
	{
		$(".UserLoginTr").hide();
		$(".ClassHistoryTr").show();
		$("#UserLogin").attr("disabled","disabled")
		$("#StudentID").attr("disabled","")
	}
	
}

function js_Reload_Class_Selection()
{
	var AcademicYear = $("#AcademicYear").val();
	$.post(
		"../ajax_reload_selection.php",
		{ 
			RecordType: 'ClassHistoryClass',
			SelectionID: 'ClassName',
			AcademicYear: AcademicYear,
			NoFirst: 1,
			IsAll: 0,
			Onchange: 'js_Reload_Student_Selection()'
		},
		function(ReturnData)
		{
			$("#ClassSelectionDiv").html(ReturnData);
			js_Reload_Student_Selection();
		}
	)
}

function js_Reload_Student_Selection()
{
	var AcademicYear = $("#AcademicYear").val();
	var ClassName = $("#ClassName").val();
	
	$.post(
		"../ajax_reload_selection.php",
		{ 
			RecordType: 'ClassHistoryStudent',
			SelectionID: 'StudentID',
			AcademicYear: AcademicYear,
			ClassName: ClassName,
			NoFirst: 0,
			IsAll: 0
		},
		function(ReturnData)
		{
			$("#StudentSelectionDiv").html(ReturnData);
		}
	)
}

function js_Check_Form()
{
	var FormValid = true;
	$("div.WarnMsg").hide();
	
	if($("#Method1").attr("checked"))
	{
		if($("#UserLogin").val()=='')
		{
			FormValid = false;
			$("div#WarnEmptyUserLogin").show();
		}	
	}
	else
	{
		if($("#StudentID").val()=='')
		{
			FormValid = false;
			$("div#WarnEmptyStudentID").show();
		}
	}
	
	return FormValid;
}

</script>
<?

$linterface->LAYOUT_STOP();

?>