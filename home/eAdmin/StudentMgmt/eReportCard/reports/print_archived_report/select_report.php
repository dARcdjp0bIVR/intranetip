<?php
#  Editing by  
/**************************************************************************
 * 20160805 Bill:	[2016-0330-1524-42164]
 * 		- Submit to /archive_report_card/print_preview_by_pdf.php when $eRCTemplateSetting['Report']['ExportPDFReport'] = true
 * 20120405 Marcus:
 * 		- show academic year with archived report only
 * 		- 2012-0227-1124-07072 - 匯基書院 (東九龍) - 4 Problems of eRC 
 * ************************************************************************/
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php"); 
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libteaching.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if (!$plugin['ReportCard2008'])
{
	header ("Location: /");
	intranet_closedb();
	exit();
}
 
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
{
	include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
	$lreportcard = new libreportcardcustom();
} else {
	$lreportcard = new libreportcard();
}

if (!$lreportcard->hasAccessRight()) 
{
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lreportcard_ui = new libreportcard_ui();
$linterface = new interface_html();

$CurrentPage = "Reports_PrintArchivedReport";
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();

# tag information
$TAGS_OBJ[] = array($eReportCard['Reports_PrintArchivedReport'], "", 0);
$linterface->LAYOUT_START();

echo $lreportcard_ui->Get_Print_Archived_Report_Select_Report_UI($UserLogin, $StudentID);

?>

<script>
$(function(){
	js_Reload_Archive_Report_Selection()
})

function js_Reload_Archive_Report_Selection()
{
	var AcademicYear = $("#AcademicYear").val();
	var StudentID = $("#StudentID").val();
	
	$.post(
		"../ajax_reload_selection.php",
		{ 
			RecordType: 'ArchiveReport',
			SelectionID: 'ReportID',
			AcademicYear: AcademicYear,
			StudentID: StudentID,
			NoFirst: 0,
			IsAll: 0
		},
		function(ReturnData)
		{
			$("#ReportSelectionDiv").html(ReturnData);
		}
	)
}

function js_Print_Archive_Report()
{
	var FormValid = true;
	$("div.WarnMsg").hide();
	
	if($("#ReportID").val()=='')
	{
		$("div#WarnEmptyReportID").show();
		FormValid =  false;
	}	

	if($("#AcademicYear").val()=='')
	{
		$("div#WarnEmptyAcademicYear").show();
		FormValid =  false;
	}	
	
	if(!FormValid)
		return false;
	
	var AcademicYear = $("#AcademicYear").val();
	var StudentID = $("#StudentID").val();
	var ReportID = $("#ReportID").val();
	
	newWindow('../../management/archive_report_card/<?=($eRCTemplateSetting['Report']['ExportPDFReport']? "print_preview_by_pdf.php" : "print_preview.php")?>?AcademicYear='+AcademicYear+'&ReportID='+ReportID+'&StudentID='+StudentID, 10);
}

</script>
<?

$linterface->LAYOUT_STOP();

?>