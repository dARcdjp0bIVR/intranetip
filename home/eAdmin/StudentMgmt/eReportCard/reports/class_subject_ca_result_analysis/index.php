<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage_ui.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if ($plugin['ReportCard2008']) {
	
	$lreportcard = new libreportcardcustom();
	$lreportcard_ui = new libreportcard_ui();
	
	// [2015-1209-1703-15206]
	# Subject Panel / Has Access Right
	if ($_SESSION["SSV_PRIVILEGE"]["reportcard"]["is_subject_panel"] || $lreportcard->hasAccessRight()) {
		
		# Interface
		$linterface = new interface_html();
		$CurrentPage = "Reports_ClassSubjectCAResultAnalysis";
		$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
		
		# Tag
		$TAGS_OBJ[] = array($eReportCard['Reports_ClassSubjectCAResultAnalysis']);
		$linterface->LAYOUT_START();
		
		# Get Form Selection (show form which has report card only)
		$FormArr = $lreportcard->GET_ALL_FORMS($hasTemplateOnly=1);
		$YearID = $FormArr[0]['ClassLevelID'];
		$FormSelection = $lreportcard_ui->Get_Form_Selection('YearID', $YearID, 'js_Reload_Selection(this.value)', $noFirst=1);
		
		# Get Class selection
		$fcm_ui = new form_class_manage_ui();
		$ClassSelection = $fcm_ui->Get_Class_Selection($lreportcard_ui->schoolYearID, $YearID, 'YearClassIDArr[]', '', '', $noFirst=1, $isMultiple=1);
		
		# Get Report selection
		$ReportSelection = $lreportcard->Get_Report_Selection($YearID, '', 'ReportID', 'js_Reload_Subject_Selection()');
		
		// [2015-1209-1703-15206]
		# Get Subject selection
		//$SubjectSelection = $lreportcard->Get_Subject_Selection($YearID, '', 'SubjectIDArr[]', $ParOnchange, 1, 0, 1, 0, '', $ReportID);
		
		// check permission
		//$checkPermissionFormClass = ($lreportcard->IS_ADMIN_USER_VIEW($_SESSION['UserID']))? 0 : 3;
		$checkPermissionSubject = ($lreportcard->IS_ADMIN_USER_VIEW($_SESSION['UserID']))? 0 : 1;
		
?>

<link type="text/css" rel="stylesheet" href="<?=$PATH_WRT_ROOT."templates/2007a/css/"?>ereportcard.css">

<br />

<form name="form1" action="generate.php" method="POST" onsubmit="return checkForm();" target="_blank">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
		</tr>
	<table>
	<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr><td>
				<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
					
					<!-- Form -->
					<tr><td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
							<?= $eReportCard['FormName'] ?>
						</td>
						<td width="75%" class='tabletext'>
							<?= $FormSelection ?>
						</td>
					</tr>
					
					<!-- Class -->
					<tr class="tabletext">
						<td class="formfieldtitle" valign="top"><?=$eReportCard['Class']?> <span class="tabletextrequire">*</span></td>
						<td><span id="ClassSelectionSpan">
								<?=$ClassSelection?>
							</span>
							<span style="vertical-align:bottom">
								<?= $linterface->GET_SMALL_BTN($button_select_all, "button", "javascript:SelectAll(document.form1.elements['YearClassIDArr[]'])"); ?>
							</span>
						</td>
					</tr>
					
					<!-- Report -->
					<tr class="tabletext">
						<td class="formfieldtitle" valign="top" width="30%"><?=$eReportCard['ReportCard']?> <span class="tabletextrequire">*</span></td>
						<td>
							<div id="ReportSelectionDiv">
								<?= $ReportSelection ?>
							</div>
						</td>
					</tr>
					
					<!-- Subject -->
					<tr class="tabletext">
						<td class="formfieldtitle" valign="top"><?=$eReportCard['Subject']?> <span class="tabletextrequire">*</span></td>
						<td><span id="SubjectSelectionSpan">
								<?=$SubjectSelection?>
							</span>
							<span style="vertical-align:bottom">
								<?= $linterface->GET_SMALL_BTN($button_select_all, "button", "javascript:SelectAll(document.form1.elements['SubjectIDArr[]'])"); ?>
							</span>
						</td>
					</tr>
					
					<tr><td colspan="2" class="dotline">
							<img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1">
					</td></tr>
					
					<tr><td colspan="2" align="center">
							<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "document.getElementById('submit_type').value=0", "submitBtn"); ?>
							<?= $linterface->GET_ACTION_BTN($eReportCard['Export'], "submit", "document.getElementById('submit_type').value=1", "exportBtn"); ?>
					</td></tr>
					
				</table>
		</td></tr>
	</table>
	
	<input type="hidden" name="submit_type" id="submit_type" value="0">
	
</form>

<script language="javascript">
//var checkPermissionFormClass = '<?=$checkPermissionFormClass?>';
var checkPermissionSubject = '<?=$checkPermissionSubject?>';

function SelectAll(obj)
{
     for (i=0; i<obj.length; i++)
     {
          obj.options[i].selected = true;
     }
}

function js_Reload_Selection(jsYearID)
{
	$('#ReportSelectionDiv').hide();
	$('#ClassSelectionSpan').hide();
	$('#SubjectSelectionSpan').hide();
	
	$('#ReportSelectionDiv').load(
		"../ajax_reload_selection.php", 
		{ 
			RecordType: 'Report',
			YearID: jsYearID,
			ReportID: '',
			SelectionID: 'ReportID',
			onChange : 'js_Reload_Subject_Selection()'
		},
		function(ReturnData)
		{
			$('#ReportSelectionDiv').show();
			js_Reload_Subject_Selection(jsYearID)
			//$('#IndexDebugArea').html(ReturnData);
		}
	);
	
	$('#ClassSelectionSpan').load(
		"../ajax_reload_selection.php", 
		{ 
			RecordType: 'Class',
			YearID: jsYearID,
			SelectionID: 'YearClassIDArr[]',
			isMultiple: 1
		},
		function(ReturnData)
		{
			SelectAll(document.form1.elements['YearClassIDArr[]']);
			$('#ClassSelectionSpan').show();
			//$('#IndexDebugArea').html(ReturnData);
		}
	);
}

function js_Reload_Subject_Selection(jsYearID)
{
	var YearID = $("#YearID").val();
	var ReportID = $("#ReportID").val();
	$('#SubjectSelectionSpan').load(
		"../ajax_reload_selection.php", 
		{ 
			RecordType: 'Subject',
			YearID: YearID,
			SelectionID: 'SubjectIDArr[]',
			IncludeComponent: 0,
			InputMarkOnly: 1,
			ReportID: ReportID,
			TeachingOnly: checkPermissionSubject
		},
		function(ReturnData)
		{
			SelectAll(document.form1.elements['SubjectIDArr[]']);
			$('#SubjectSelectionSpan').show();
			//$('#IndexDebugArea').html(ReturnData);
		}
	);
}

function checkForm() 
{
	var obj = document.form1;
	var select_obj = obj.elements['YearClassIDArr[]'];
	var hasSelectedClass = false;
	for (var i=0; i<select_obj.length; i++)
	{
		if (select_obj[i].selected)
		{
			hasSelectedClass = true;
		}
	}

	if (hasSelectedClass == false)
	{
		alert('<?=$eReportCard["jsWarningSelectClass"]?>');
		obj.elements['YearClassIDArr[]'].focus();
		return false;
	}
	
	
	var select_obj = obj.elements['SubjectIDArr[]'];
	var hasSelectedSubject = false;
	for (var i=0; i<select_obj.length; i++)
	{
		if (select_obj[i].selected)
		{
			hasSelectedSubject = true;
		}
	}

	if (hasSelectedSubject == false)
	{
		alert('<?=$eReportCard["jsSelectSubjectWarning"]?>');
		obj.elements['SubjectIDArr[]'].focus();
		return false;
	}
	
	return true;
}

$().ready(function(){
	js_Reload_Subject_Selection();
});

//SelectAll(document.form1.elements['SubjectIDArr[]']);
SelectAll(document.form1.elements['YearClassIDArr[]']);

</script>


<?
	print $linterface->FOCUS_ON_LOAD("form1.ClassLevelID");
  	$linterface->LAYOUT_STOP();
  	
	} else {
?>
You have no priviledge to access this page.
<?
	}
} else {
?>

<?
}
?>
