<?php
#  Editing by  Connie
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();
 
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
$lreportcard = new libreportcardcustom();
$lreportcard->hasAccessRight();

$lreportcard_ui = new libreportcard_ui();
$lreportcard = new libreportcard();
$linterface = new interface_html();

$CurrentPage = "Reports_TrialPromotion";
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();


#get $_POST variables
$TrialPromotionSetting = $_POST;

$ClassLevelID = $TrialPromotionSetting['ClassLevelID'];
$ReportID = $TrialPromotionSetting['ReportID'];
$YearClassIDArr = array();
$YearClassIDArr = $TrialPromotionSetting['YearClassIDArr'];

$ViewFormat = $TrialPromotionSetting['ViewFormat']==1?"html":"csv";



###### Build Array for display 

//$ReportTemplateBasicInfo = $lreportcard->returnReportTemplateBasicInfo($ReportID);
////debug_r($ReportTemplateBasicInfo);
//
//$FormNumber = $lreportcard->GET_FORM_NUMBER($ClassLevelID);
////debug_r($FormNumber);
//
//// $MarksArr[$StudentID][$SubjectID][$ReportColumnID][Key] = Data
//$MarkArr = $lreportcard->getMarks($ReportID, $StudentID='', $cons='', $ParentSubjectOnly=1, $includeAdjustedMarks=1, $OrderBy='', $SubjectID='', $ReportColumnID=0);	
//
//// $OtherInfoDataArr[$StudentID][$YearTermID][Key] = Data
//$OtherInfoDataArr = $this->getReportOtherInfoData($ReportID);


$lreportcard_ui->Get_Trial_Promotion_Report($ReportID,$YearClassIDArr,$ClassLevelID,$ViewFormat);


//foreach((array)$YearClassIDArr as $key => $thisYearClassID)
//{
//	$thisClassStudentArr = $lreportcard->GET_STUDENT_BY_CLASS($thisYearClassID, $ParStudentIDList="", $isShowBothLangs=0, $withClassNumber=0, $isShowStyle=0, $ReturnAsso=0);
//	
//	foreach((array)$thisClassStudentArr as $key => $StdArray)
//	{
//		$thisStudentID = $StdArray['UserID'];
//	  
//		debug_pr($MarkArr);
//	}
//	//debug_pr($StdArray);
//}



# Get Student List

//$StudentList = $lreportcard->GET_STUDENT_BY_CLASSLEVEL($ReportID,$ClassLevelID);

intranet_closedb();
?>