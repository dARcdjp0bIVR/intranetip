<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if ($plugin['ReportCard2008']) {
	include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
	include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
	include_once($PATH_WRT_ROOT."includes/libinterface.php");
	include_once($PATH_WRT_ROOT."includes/form_class_manage_ui.php");
	include_once($PATH_WRT_ROOT."includes/subject_class_mapping_ui.php");
	
	if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
	{
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
		$lreportcard = new libreportcardcustom();
	} else {
		$lreportcard = new libreportcard();
	}
	$lreportcard_ui = new libreportcard_ui();
	$linterface = new interface_html();
	
	if ($lreportcard->hasAccessRight()) {
		
		############## Interface Start ##############
		$linterface = new interface_html();
		$CurrentPage = "Reports_SubjectPercentagePassReport";
		$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
		# tag information
		$TAGS_OBJ[] = array($eReportCard['Reports_SubjectPercentagePassReport']);
		$linterface->LAYOUT_START();
		#############################################
		
		
		### Get Form selection (show form which has report card only)
		$FormArr = $lreportcard->GET_ALL_FORMS($hasTemplateOnly=1);
		$YearID = $FormArr[0]['ClassLevelID'];
		
		$FormSelection = $lreportcard_ui->Get_Form_Selection('YearID', $YearID, 'js_Reload_Selection(this.value)', $noFirst=1);
		
		### Get Report selection
		$ReportSelection = $lreportcard->Get_Report_Selection($YearID, '', 'ReportID');
		
?>


<link type="text/css" rel="stylesheet" href="<?=$PATH_WRT_ROOT."templates/2007a/css/"?>ereportcard.css">

<br />
<form name="form1" action="generate.php" method="POST" onsubmit="return checkForm();" target="_blank">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	</tr>
<table>
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>
			<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr>
					<td colspan="2" align="center"><?=$RemarksBox?></td>
				</tr>
				
				<tr id="ViewFormatRow">
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?= $eReportCard['ViewFormat'] ?>
					</td>
					<td width="75%" class='tabletext'>
						<input type="radio" name="ViewFormat" id="ViewHTML" value="html" CHECKED>
							<label for="ViewHTML"><?=$eReportCard['HTML']?></label>
						</input>
						<input type="radio" name="ViewFormat" id="ViewCSV" value="csv">
							<label for="ViewCSV"><?=$eReportCard['CSV']?></label>
						</input>
					</td>
				</tr>
				
				<!-- Form -->
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?= $eReportCard['FormName'] ?>
					</td>
					<td width="75%" class='tabletext'>
						<?= $FormSelection ?>
					</td>
				</tr>
				
				<!-- Report -->
				<tr class="tabletext">
					<td class="formfieldtitle" valign="top" width="30%"><?=$eReportCard['ReportCard']?> <span class="tabletextrequire">*</span></td>
					<td>
						<div id="ReportSelectionDiv">
							<?= $ReportSelection ?>
						</div>
					</td>
				</tr>
				
				<!-- SubjectName Display-->
				<tr id="ShowGenderRow" style="<?= $ShowGenderRowDisplay ?>">
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?= $eReportCard['SubjectDisplay'] ?>
					</td>
					<td width="75%" class='tabletext'>
						<input type="radio" name="SubjDisplay" id="SubjDisplay_Desc" value="Desc" CHECKED>
							<label for="SubjDisplay_Desc">
								<?=$eReportCard['Desc']?>
							</label>
						</input>
						<input type="radio" name="SubjDisplay" id="SubjDisplay_Abbr" value="Abbr">
							<label for="SubjDisplay_Abbr">
								<?=$eReportCard['Abbr']?>
							</label>
						</input>
						<input type="radio" name="SubjDisplay" id="SubjDisplay_ShortName" value="ShortName">
							<label for="SubjDisplay_ShortName">
								<?=$eReportCard['ShortName']?>
							</label>
						</input>
					</td>
				</tr>
				
				<tr>
					<td colspan="2" class="dotline">
						<img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1">
					</td>
				</tr>
				<tr>
					<td colspan="2" align="center">
						<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "", "submitBtn"); ?>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</form>


<script language="javascript">

$(document).ready( function() {	
	
});

function js_Reload_Selection(jsYearID)
{
	$('#ReportSelectionDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"../ajax_reload_selection.php", 
		{ 
			RecordType: 'Report',
			YearID: jsYearID,
			ReportID: '',
			SelectionID: 'ReportID'
		},
		function(ReturnData)
		{
			//$('#IndexDebugArea').html(ReturnData);
		}
	);
}

function checkForm() 
{
	return true;
}
</script>


<?
	print $linterface->FOCUS_ON_LOAD("form1.ClassLevelID");
  	$linterface->LAYOUT_STOP();
  } else {
?>
You have no priviledge to access this page.
<?
	}
} else {
?>

<?
}
?>
