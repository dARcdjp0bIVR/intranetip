<?
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

intranet_auth();
intranet_opendb();

$ViewFormat = $_POST['ViewFormat'];
$YearID = $_POST['YearID'];
$ReportID = $_POST['ReportID'];
$SubjDisplay = $_POST['SubjDisplay'];

if ($plugin['ReportCard2008']) {
	include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
	
	if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
	{
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
		$lreportcard = new libreportcardcustom();
	} else {
		$lreportcard = new libreportcard();
	}
	
	if ($lreportcard->hasAccessRight()) 
	{
		# Report Title
		$objYear = new Year($YearID);
		$FormName = $objYear->YearName;
		
		$ObjAcademicYear = new academic_year($lreportcard->schoolYearID);
		$ActiveYear = $ObjAcademicYear->Get_Academic_Year_Name();
		
		$ReportCardInfoArr = $lreportcard->returnReportTemplateBasicInfo($ReportID);
		$ReportCardTitle = str_replace(':_:', ' ', $ReportCardInfoArr['ReportTitle']);
		
		$ReportTitle = strtoupper($eReportCard['Template']['SchoolNameEn'])."&nbsp;&nbsp;&nbsp;&nbsp;".$ReportCardTitle."&nbsp;&nbsp;&nbsp;&nbsp;".$ActiveYear."&nbsp;&nbsp;&nbsp;&nbsp;".$FormName;
		
		$SubjectIDArr = $lreportcard->returnSubjectIDwOrder($YearID, $ExcludeCmpSubject=1);
		$MarkStatisticsArr = $lreportcard->Get_Mark_Statistics_Info($ReportID, $SubjectIDArr, $Average_Rounding=2, $SD_Rounding=2, $Percentage_Rounding=2, $SubjectMarkRounding=0, $IncludeClassStat=0);
		
		$DisplayAbbrName = 0;
		$DisplayShortName = 0;
		if ($SubjDisplay == 'Abbr')
			$DisplayAbbrName = 1;
		else if ($SubjDisplay == 'ShortName')
			$DisplayShortName = 1;
		
		// $PassPercentageInfoArr[$SubjectID][InfoField] = Value
		$PassPercentageInfoArr = array();
		foreach ((array)$MarkStatisticsArr as $thisSubjectID => $thisSubjectStatisticsArr)
		{
			$PassPercentageInfoArr[$thisSubjectID]['SubjectName'] = $lreportcard->GET_SUBJECT_NAME_LANG($thisSubjectID, $Lang='', $DisplayShortName, $DisplayAbbrName);
			$PassPercentageInfoArr[$thisSubjectID]['NumOfStudent'] = $thisSubjectStatisticsArr[0]['NumOfStudent'];
			$PassPercentageInfoArr[$thisSubjectID]['AdjustedPassingScore'] = $thisSubjectStatisticsArr[0]['LowestPassingMark'];
			
			$thisAdjustedPassingPercentage = $thisSubjectStatisticsArr[0]['Nature']['TotalPassPercentage'];
			$thisAdjustedPassingStudent = $thisSubjectStatisticsArr[0]['Nature']['TotalPass'];
			$PassPercentageInfoArr[$thisSubjectID]['%PassAdjusted'] = my_round($thisAdjustedPassingPercentage, 0).'% ('.$thisAdjustedPassingStudent.')';
			
			$thisRawPassingPercentage = $thisSubjectStatisticsArr[0]['Nature_'.$lreportcard->HardCodePassMark]['TotalPassPercentage'];
			$thisRawPassingStudent = $thisSubjectStatisticsArr[0]['Nature_'.$lreportcard->HardCodePassMark]['TotalPass'];
			$PassPercentageInfoArr[$thisSubjectID]['%PassRaw'] = my_round($thisRawPassingPercentage, 0).'% ('.$thisRawPassingStudent.')';
			
			$PassPercentageInfoArr[$thisSubjectID]['Max'] = $thisSubjectStatisticsArr[0]['MaxMark'];
			$PassPercentageInfoArr[$thisSubjectID]['Min'] = $thisSubjectStatisticsArr[0]['MinMark'];
			$PassPercentageInfoArr[$thisSubjectID]['Mean'] = $thisSubjectStatisticsArr[0]['Average_Rounded'];
			$PassPercentageInfoArr[$thisSubjectID]['SD'] = $thisSubjectStatisticsArr[0]['SD_Rounded'];
		}
		
	
		
		## Display the result
		if ($ViewFormat == 'html')
		{
			$table .= '	<tr>
							<td align="center"><h1 style="text-align:left">'.$ReportTitle.'</h1></td>
						</tr>
						<tr>
							<td>
						';
					$table .= "<table id='ResultTable' class='GrandMSContentTable' width='100%' border='1' cellpadding='10' cellspacing='0'>\n";
						$table .= "<thead>\n";
							$table .= "<tr style='text-align:left'>\n";
								$table .= "<th>".$eReportCard['PassPercentageReport']['Subject']."</th>\n";
								$table .= "<th>".$eReportCard['PassPercentageReport']['NumOfStudent']."</th>\n";
								$table .= "<th>".$eReportCard['PassPercentageReport']['AdjustedPassingScore']."</th>\n";
								$table .= "<th>".$eReportCard['PassPercentageReport']['%PassAdjusted']."</th>\n";
								$table .= "<th>".$eReportCard['PassPercentageReport']['%PassRaw']."</th>\n";
								$table .= "<th>".$eReportCard['PassPercentageReport']['Max']."</th>\n";
								$table .= "<th>".$eReportCard['PassPercentageReport']['Min']."</th>\n";
								$table .= "<th>".$eReportCard['PassPercentageReport']['Mean']."</th>\n";
								$table .= "<th>".$eReportCard['PassPercentageReport']['StandardDeviation']."</th>\n";
							$table .= "</tr>\n";
						$table .= "</thead>\n";
						
						$table .= "<tbody>\n";
							foreach($PassPercentageInfoArr as $thisSubjectID => $thisSubjectInfoArr)
							{
								$table .= "<tr align='left' valign='top'>\n";
									$table .= "<td>".$thisSubjectInfoArr['SubjectName']."</td>\n";
									$table .= "<td>".$thisSubjectInfoArr['NumOfStudent']."</td>\n";
									$table .= "<td>".$thisSubjectInfoArr['AdjustedPassingScore']."</td>\n";
									$table .= "<td>".$thisSubjectInfoArr['%PassAdjusted']."</td>\n";
									$table .= "<td>".$thisSubjectInfoArr['%PassRaw']."</td>\n";
									$table .= "<td>".$thisSubjectInfoArr['Max']."</td>\n";
									$table .= "<td>".$thisSubjectInfoArr['Min']."</td>\n";
									$table .= "<td>".$thisSubjectInfoArr['Mean']."</td>\n";
									$table .= "<td>".$thisSubjectInfoArr['SD']."</td>\n";
								$table .= "</tr>\n";
							}
						$table .= "</tbody>\n";
					$table .= "</table>\n";
					$table .= "<br style='clear:both'>\n";
				$table .= "</td>\n";
			$table .= "</tr>\n";
			
			
			$css = $lreportcard->Get_GrandMS_CSS();
			
			$allTable = '<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
							'.$table.'
						</table>';
			
			echo $lreportcard->Get_Report_Header($eReportCard['Reports_SubjectPercentagePassReport']);
			echo $allTable.$css;
			echo $lreportcard->Get_Report_Footer();
		}
		else if ($ViewFormat == 'csv')
		{
			include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
			include_once($PATH_WRT_ROOT."includes/libexporttext.php");
			
			$ExportHeaderArr = array();
			$ExportContentArr = array();
			$lexport = new libexporttext();
			
			# Header
			$ExportHeaderArr[] = $eReportCard['PassPercentageReport']['Subject'];
			$ExportHeaderArr[] = $eReportCard['PassPercentageReport']['NumOfStudent'];
			$ExportHeaderArr[] = $eReportCard['PassPercentageReport']['AdjustedPassingScore'];
			$ExportHeaderArr[] = $eReportCard['PassPercentageReport']['%PassAdjusted'];
			$ExportHeaderArr[] = $eReportCard['PassPercentageReport']['%PassRaw'];
			$ExportHeaderArr[] = $eReportCard['PassPercentageReport']['Max'];
			$ExportHeaderArr[] = $eReportCard['PassPercentageReport']['Min'];
			$ExportHeaderArr[] = $eReportCard['PassPercentageReport']['Mean'];
			$ExportHeaderArr[] = $eReportCard['PassPercentageReport']['StandardDeviation'];
			
			# Content
			$i_counter = 0;
			$j_counter = 0;
			foreach((array)$PassPercentageInfoArr as $thisSubjectID => $thisSubjectInfoArr)
			{
				$ExportContentArr[$i_counter][$j_counter++] = $thisSubjectInfoArr['SubjectName'];
				$ExportContentArr[$i_counter][$j_counter++] = $thisSubjectInfoArr['NumOfStudent'];
				$ExportContentArr[$i_counter][$j_counter++] = $thisSubjectInfoArr['AdjustedPassingScore'];
				$ExportContentArr[$i_counter][$j_counter++] = $thisSubjectInfoArr['%PassAdjusted'];
				$ExportContentArr[$i_counter][$j_counter++] = $thisSubjectInfoArr['%PassRaw'];
				$ExportContentArr[$i_counter][$j_counter++] = $thisSubjectInfoArr['Max'];
				$ExportContentArr[$i_counter][$j_counter++] = $thisSubjectInfoArr['Min'];
				$ExportContentArr[$i_counter][$j_counter++] = $thisSubjectInfoArr['Mean'];
				$ExportContentArr[$i_counter][$j_counter++] = $thisSubjectInfoArr['SD'];
				
				$i_counter++;
				$j_counter = 0;
			}
			
			// Title of the grandmarksheet
			$ReportTitle = str_replace('&nbsp;', '', $ReportTitle);
			$filename = str_replace(' ', '_', $ReportTitle).".csv";
			
			$export_content = $lexport->GET_EXPORT_TXT($ExportContentArr, $ExportHeaderArr);
			intranet_closedb();
			
			// Output the file to user browser
			$lexport->EXPORT_FILE($filename, $export_content);
		}
	}
}


?>