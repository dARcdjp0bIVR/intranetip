<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "TEACHER";
ini_set('display_errors',1);
	error_reporting(E_ALL ^ E_NOTICE);	

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libteaching.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if ($plugin['ReportCard2008']) {
	include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
	include_once($PATH_WRT_ROOT."includes/libreportcard2008w.php");
	
	$lreportcard = new libreportcard2008w();
	$lteaching = new libteaching();
	$linterface = new interface_html();
	
	if ($lreportcard->hasAccessRight()) {
		$submitBtn = $linterface->GET_ACTION_BTN($button_submit, "submit", "", "submitBtn");
		
		if($ck_ReportCard_UserType=="TEACHER") {
			$FormArrCT = $lreportcard->returnSunjectTeacherForm($UserID);
						
			# check is class teacher or not
			$TeacherClassAry = $lteaching->returnTeacherClassWithLevel($UserID);
			$TeacherClass = $TeacherClassAry[0]['ClassName'];
			
			// Update FormArrCT if user is class teacher
			if(!empty($TeacherClass)) {
				for($i==0;$i<sizeof($TeacherClassAry);$i++) {
					$thisClassLevelID = $TeacherClassAry[$i]['ClassLevelID'];
					$searchResult = multiarray_search($FormArrCT , "ClassLevelID", $thisClassLevelID);
					if($searchResult == "-1") {
						$thisAry = array(
							"0"=>$TeacherClassAry[$i]['ClassLevelID'],
							"ClassLevelID"=>$TeacherClassAry[$i]['ClassLevelID'],
							"1"=>$TeacherClassAry[$i]['LevelName'],
							"LevelName"=>$TeacherClassAry[$i]['LevelName']);
						$FormArrCT = array_merge($FormArrCT, array($thisAry));
					}
				}
				
				# sort $FormArrCT
				foreach ($FormArrCT as $key => $row) {
					$field1[$key] 	= $row['ClassLevelID'];
					$field2[$key]  	= $row['LevelName'];
				}
				array_multisort($field1, SORT_ASC, $field2, SORT_ASC, $FormArrCT);
			}
			
			$ClassLevelID = ($ClassLevelID == "") ? $FormArrCT[0][0] : $ClassLevelID;
			$ClassLevelName = $lreportcard->returnClassLevel($ClassLevelID);
			#############################################
			$ClassArrCT = $lreportcard->returnSubjectTeacherClass($UserID, $ClassLevelID);
			#############################################
			
			$FormSubjectArrCT = $lreportcard->returnSubjectwOrder($ClassLevelID, 1, $UserID);
			
			// Update $FormSubjectArrCT if user is class teacher
			if(!empty($TeacherClass)) {
				$searchResult = multiarray_search($TeacherClassAry , "ClassLevelID", $ClassLevelID);
				if($searchResult != "-1") {
					#############################################
					$searchResult2 = multiarray_search($ClassArrCT , "ClassName", $TeacherClassAry[$searchResult]['ClassName']);
					if($searchResult2 == "-1") {
						$thisAry = array(
							"0"=>$TeacherClassAry[$searchResult]['ClassID'],
							"ClassID"=>$TeacherClassAry[$searchResult]['ClassID'],
							"1"=>$TeacherClassAry[$searchResult]['ClassName'],
							"ClassName"=>$TeacherClassAry[$searchResult]['ClassName'],
							"2"=>$TeacherClassAry[$searchResult]['ClassLevelID'],
							"ClassLevelID"=>$TeacherClassAry[$searchResult]['ClassLevelID']);
						$ClassArrCT = array_merge($ClassArrCT, array($thisAry));
					}
					#############################################
					$FormSubjectArrCT = $lreportcard->returnSubjectwOrder($ClassLevelID, 1);
				}
			}
		}
		
		# Get ClassLevelID (Form) of the reportcard template
		$FormArr = $ck_ReportCard_UserType=="ADMIN" ? $lreportcard->GET_ALL_FORMS(1) : $FormArrCT;
		
		if(count($FormArr) > 0) {
			# Filter: Grand Marksheet type (0: Class Summary Report, 1: Level Ranking, 2: Pupils' Progress)
			$grandMarksheetTypeOptions = array();
			for($i=0; $i<sizeof($eReportCard['GrandMarksheetTypeOption']); $i++) {
				if($ck_ReportCard_UserType=="TEACHER" && $i == 1)	continue;
				$grandMarksheetTypeOptions[] = array($i, $eReportCard['GrandMarksheetTypeOption'][$i]);
			}
			$GrandMarksheetTypeSelection = $linterface->GET_SELECTION_BOX($grandMarksheetTypeOptions, 'name="GrandMarksheetType" id="GrandMarksheetType" class="" onchange="changeGrandMarksheetType(this.options[this.selectedIndex].value)"', '', '');
			$GrandMarksheetType = ($GrandMarksheetType == "") ? $grandMarksheetTypeOptions[0][0] : $GrandMarksheetType;
			
			# Control default display table row when page loaded
			if ($GrandMarksheetType == 0) {
				$ClassRowDisplay = "";
				$SubjectRowDisplay = "display:none";
				$SubjectTypeRowDisplay = "";
			} else if ($GrandMarksheetType == 1) {
				$ClassRowDisplay = "display:none";
				$SubjectRowDisplay = "";
				$SubjectTypeRowDisplay = "display:none";
			} else {
				$ClassRowDisplay = "";
				$SubjectRowDisplay = "";
				$ColumnRowDisplay = "display:none";
				$ReportRow = "display:none";
				$SubjectTypeRowDisplay = "display:none";
			}
			
			# Filters - By Form (ClassLevelID)
			$ClassLevelID = ($ClassLevelID == "") ? $FormArr[0][0] : $ClassLevelID;
			$ClassLevelName = $lreportcard->returnClassLevel($ClassLevelID);
			$FormSelection = $linterface->GET_SELECTION_BOX($FormArr, 'name="ClassLevelID" class="" onchange="loadReportList(this.options[this.selectedIndex].value)"', "", $ClassLevelID);
			
			$FormArrForPupilProgress = array();
			for($i=0; $i<sizeof($FormArr); $i++) {
				if (strtoupper($FormArr[$i]["LevelName"] != "P1") && strtoupper($FormArr[$i]["LevelName"] != "P2")) {
					$FormArrForPupilProgress[] = $FormArr[$i];
				}
			}
			$FormSelectionForPupilProgress = $linterface->GET_SELECTION_BOX($FormArrForPupilProgress, 'name="ClassLevelID" class="" onchange="loadReportList(this.options[this.selectedIndex].value)"', "", $ClassLevelID);
			
			# Filters - By Class (ClassID)
			$ClassArr = $ck_ReportCard_UserType=="ADMIN" ? $lreportcard->GET_CLASSES_BY_FORM($ClassLevelID) : $ClassArrCT;
			$allClassesOption = array(0=> array("", $eReportCard['AllClasses']));
			$ClassArr = array_merge($allClassesOption, $ClassArr);
			$ClassID = ($ClassID == "") ? $ClassArr[0][0] : $ClassID;
			$ClassSelection = $linterface->GET_SELECTION_BOX($ClassArr, 'name="ClassID" class="" onchange=""', "", $ClassID);
			
			# Filters - By Report
			$ReportTypeSelection = '';
			$ReportTypeArr = array();
			$ReportTypeOption = array();
			$ReportTypeArr = $lreportcard->GET_REPORT_TYPES($ClassLevelID);
			$ReportTypeOptionCount = 0;
			
			if(count($ReportTypeArr) > 0){
				for($j=0 ; $j<count($ReportTypeArr) ; $j++){
					# for checking the existence of ReportID when changing ClassLevel
					// only display report which have been generated
					$reportTemplateInfo = $lreportcard->returnReportTemplateBasicInfo($ReportTypeArr[$j]['ReportID']);
					if ($reportTemplateInfo["LastGenerated"] != "0000-00-00 00:00:00" && $reportTemplateInfo["LastGenerated"] != "") {
						$ReportIDList[] = $ReportTypeArr[$j]['ReportID'];
						$ReportTypeOption[$ReportTypeOptionCount][0] = $ReportTypeArr[$j]['ReportID'];
						$ReportTypeOption[$ReportTypeOptionCount][1] = $ReportTypeArr[$j]['SemesterTitle'];
						$ReportTypeOptionCount++;
					}
				}
			}
			
			$ReportTypeSelectDisabled = "";
			if (sizeof($ReportTypeOption) == 0) {
				$ReportTypeOption[0][0] = "-1";
				$ReportTypeOption[0][1] = $eReportCard['NoReportAvailable'];
				$ReportTypeSelectDisabled = "disabled='disabled'";
			}
			
			$ReportID = ($ReportID == "" || !in_array($ReportID,$ReportIDList)) ? $ReportTypeOption[0][0] : $ReportID;
			$reportTemplateInfo = $lreportcard->returnReportTemplateBasicInfo($ReportID);
			$ReportTypeSelection = $linterface->GET_SELECTION_BOX($ReportTypeOption, 'name="ReportID" id="ReportID" class="" '.$ReportTypeSelectDisabled.' onchange="loadColumnList(this.options[this.selectedIndex].value)"', '', $ReportID, '');
			
			$ReportIDOrder = "";
			if (sizeof($ReportIDList) > 0) {
				sort($ReportIDList);
				$ReportIDOrder = array_search($ReportID, $ReportIDList);
			}
			
			# Filters - By Report Column
			if ($ReportTypeSelectDisabled == "") {
				$ColumnTitleAry = $lreportcard->returnReportColoumnTitle($ReportID);
				$ColumnOption = array();
				################## Customization for SIS ##################
				# if it is secondary school
				if (strtolower(substr($ClassLevelName, 0, 1)) == "s") {
					# Year-End report
					if ($reportTemplateInfo["Semester"] == "F" && sizeof($ColumnTitleAry) == 4) {
						$ColumnOption[0] = array("", $eReportCard['Template']['OverallCombined']);
						//$ColumnOption[1] = array("first", $eReportCard['Template']['FirstCombined']);
						//$ColumnOption[1] = array("second", $eReportCard['Template']['SecondCombined']);
					# Mid-term report
					} else {
						if ($ReportIDOrder == 0) $ColumnOption[0] = array("", $eReportCard['Template']['FirstCombined']);
						if ($ReportIDOrder == 1) $ColumnOption[0] = array("", $eReportCard['Template']['SecondCombined']);
					}
				# if it is primary school
				} else if (strtolower(substr($ClassLevelName, 0, 1)) == "p") {
					# Year-End report
					if ($reportTemplateInfo["Semester"] == "F") {
						if (strtolower(substr($ClassLevelName, 1, 1)) == "1" || strtolower(substr($ClassLevelName, 1, 1)) == "2") {
							$ColumnOption[0] = array("", $eReportCard['Template']['OverallCombined']);
						} else {
							$ColumnOption[0] = array("", $eReportCard['Template']['OverallCombined']);
							$countColumn = 1;
							foreach ($ColumnTitleAry as $columnID => $columnTitle) {
								if (sizeof($ColumnTitleAry) != 1 && $countColumn == 2) {
									$ColumnOption[1][0] = $columnID;
									$ColumnOption[1][1] = "SA2";
									break;
								}
								$ColumnOption[$countColumn][0] = $columnID;
								$ColumnOption[$countColumn][1] = $columnTitle;
								if ($countColumn == 1) $ColumnOption[$countColumn][1] = "SA1";
								if ($countColumn == 2) $ColumnOption[$countColumn][1] = "SA2";
								$countColumn++;
							}
						}
					# Mid-term report
					} else {
						$ColumnOption[0] = array("", "SA1");
					}
				} else {
					$ColumnOption[0] = array("", $eReportCard['Template']['OverallCombined']);
					$countColumn = 1;
					foreach ($ColumnTitleAry as $columnID => $columnTitle) {
						$ColumnOption[$countColumn][0] = $columnID;
						$ColumnOption[$countColumn][1] = $columnTitle;
						$countColumn++;
					}
				}
			} else {
				// disable Submit button
				$submitBtn = $linterface->GET_ACTION_BTN($button_submit, "submit", "", "submitBtn", "disabled='disabled'");
				
				$ColumnOption[0][0] = "-1";
				$ColumnOption[0][1] = $eReportCard['NoTermAvailable'];
				$ColumnSelectDisabled = 'disabled="disabled"';
			}
			$ColumnSelection = $linterface->GET_SELECTION_BOX($ColumnOption, 'name="ReportColumnID" id="ReportColumnID" '.$ColumnSelectDisabled.' onchange=""', '');
			
			# Filters - By Form Subjects
			// A Flag, called $isFormSubject, is used to identify whether currect Form has that subject or not
			$FormSubjectArr = array();
			$SubjectOption = array();
			$SubjectOption[0] = array("", $eReportCard['OverallTotal']);
			$count = 1;
			$isFormSubject = 0;		// Default: Assume the selected subject does not belongs to that Form(ClassLevelID)
			$SubjectFormGradingArr = array();
		
			// Get Subjects By Form (ClassLevelID)
			$FormSubjectArr = $ck_ReportCard_UserType=="ADMIN" ? $lreportcard->returnSubjectwOrder($ClassLevelID, 1) : $FormSubjectArrCT;		
			if(!empty($FormSubjectArr)){
				foreach($FormSubjectArr as $FormSubjectID => $Data){
					if(is_array($Data)){
						foreach($Data as $FormCmpSubjectID => $SubjectName){							
							$FormSubjectID = ($FormCmpSubjectID==0) ? $FormSubjectID : $FormCmpSubjectID;
							if($FormSubjectID == $SubjectID)
								$isFormSubject = 1;
							$SubjectFormGradingArr = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $FormSubjectID);
							// Prepare Subject Selection Box 
							if($FormCmpSubjectID==0 && $SubjectFormGradingArr["ScaleInput"]=="M" && $SubjectFormGradingArr["ScaleDisplay"]=="M"){
								$SubjectOption[$count][0] = $FormSubjectID;
								$SubjectOption[$count][1] = $SubjectName;
								$count++;
							}
						}
					}
				}
			}
			
			// Use for Selection Box
			// $SubjectListArr[][0] : SubjectID
			// $SubjectListArr[][1] : SubjectName
			$SubjectID = ($isFormSubject) ? $SubjectID : '';
			if($SubjectID == ""){
				$SubjectID = $SubjectOption[0][0];
			}
			$SelectedSubjectName = $lreportcard->GET_SUBJECT_NAME($SubjectID);
			$SubjectSelection = $linterface->GET_SELECTION_BOX($SubjectOption, 'name="SubjectID[]" id="SubjectID" class="" onchange="" multiple="true" size="8"', '', $SubjectID);
			$SelectAllBtn = $linterface->GET_BTN($button_select_all, "button", "jsSelectAllOption('SubjectID', true); return false;");
			
			// Alternate sets of subjects & classes, they will be used when User selected a different Class Level
			// They will store as JS array
			$OtherSubjectSelection = array();
			for($i=0; $i<sizeof($FormArr); $i++) {
				$FormSubjectArrCT = $lreportcard->returnSubjectwOrder($FormArr[$i][0], 1, $UserID);
				$ClassArrCT = $lreportcard->returnSubjectTeacherClass($UserID, $FormArr[$i][0]);
				
				if(!empty($TeacherClass)) {
					$searchResult = multiarray_search($TeacherClassAry , "ClassLevelID", $FormArr[$i][0]);
					if($searchResult != "-1") {	
						$searchResult2 = multiarray_search($ClassArrCT , "ClassName", $TeacherClassAry[$searchResult]['ClassName']);
						if($searchResult2 == "-1") {
							$thisAry = array(
								"0"=>$TeacherClassAry[$searchResult]['ClassID'],
								"ClassID"=>$TeacherClassAry[$searchResult]['ClassID'],
								"1"=>$TeacherClassAry[$searchResult]['ClassName'],
								"ClassName"=>$TeacherClassAry[$searchResult]['ClassName'],
								"2"=>$TeacherClassAry[$searchResult]['ClassLevelID'],
								"ClassLevelID"=>$TeacherClassAry[$searchResult]['ClassLevelID']);
							$ClassArrCT = array_merge($ClassArrCT, array($thisAry));
						}
						$FormSubjectArrCT = $lreportcard->returnSubjectwOrder($FormArr[$i][0], 1);
					}
				}
				
				$FormSubjectArr = array();
				$SubjectOption = array();
				$SubjectOption[0] = array("", $eReportCard['OverallTotal']);
				$count = 1;
				$SubjectFormGradingArr = array();
				
				$FormSubjectArr = $ck_ReportCard_UserType=="ADMIN" ? $lreportcard->returnSubjectwOrder($FormArr[$i][0], 1) : $FormSubjectArrCT;
				if(!empty($FormSubjectArr)){
					foreach($FormSubjectArr as $FormSubjectID => $Data){
						if(is_array($Data)){
							foreach($Data as $FormCmpSubjectID => $SubjectName){							
								$FormSubjectID = ($FormCmpSubjectID==0) ? $FormSubjectID : $FormCmpSubjectID;
								
								$SubjectFormGradingArr = $lreportcard->GET_SUBJECT_FORM_GRADING($FormArr[$i][0], $FormSubjectID);
								// Prepare Subject Selection Box 
								if($FormCmpSubjectID==0 && $SubjectFormGradingArr["ScaleInput"]=="M" && $SubjectFormGradingArr["ScaleDisplay"]=="M"){
									$SubjectOption[$count][0] = $FormSubjectID;
									$SubjectOption[$count][1] = $SubjectName;
									$count++;
								}
							}
						}
					}
				}
				
				$SubjectID = $SubjectOption[0][0];
				$SelectedSubjectName = $lreportcard->GET_SUBJECT_NAME($SubjectID);
				$OtherSubjectSelection[$FormArr[$i][0]] = $linterface->GET_SELECTION_BOX($SubjectOption, 'name="SubjectID[]" id="SubjectID" class="" onchange="" multiple="true" size="8"', '', $SubjectID);	
				
				# Filters - By Class (ClassID)
				$ClassArr = $ck_ReportCard_UserType=="ADMIN" ? $lreportcard->GET_CLASSES_BY_FORM($FormArr[$i][0]) : $ClassArrCT;
				$ClassArr = array_merge($allClassesOption, $ClassArr);
				$ClassID = $ClassArr[0][0];
				$OtherClassSelection[$FormArr[$i][0]] = $linterface->GET_SELECTION_BOX($ClassArr, 'name="ClassID" class="" onchange=""', "", $ClassID);
				
			}
			
			// Alternate sets of Reports, they will be used when User selected a different Class Level
			// They will store as JS array
			$OtherReportTypeSelection = array();
			
			// default ReportColumn dropdown list for "No Report Available"
			$ColumnOption[0][0] = "-1";
			$ColumnOption[0][1] = $eReportCard['NoTermAvailable'];
			$ColumnSelectDisabled = 'disabled="disabled"';
			$OtherReportColumnSelection["-1"] = $linterface->GET_SELECTION_BOX($ColumnOption, 'name="ReportColumnID" id="ReportColumnID" '.$ColumnSelectDisabled.' onchange=""', '');
			
			for($i=0; $i<sizeof($FormArr); $i++) {
				$ClassLevelName = $lreportcard->returnClassLevel($FormArr[$i][0]);
				$ReportTypeArr = array();
				$ReportTypeOption = array();
				$ReportTypeArr = $lreportcard->GET_REPORT_TYPES($FormArr[$i][0]);
				$ReportTypeOptionCount = 0;
				$ReportIDList = array();
				$ReportIDOrder = 0;
				
				if(count($ReportTypeArr) > 0){
					for($j=0; $j<sizeof($ReportTypeArr); $j++){
						$reportTemplateInfo = $lreportcard->returnReportTemplateBasicInfo($ReportTypeArr[$j]['ReportID']);
						if ($reportTemplateInfo["LastGenerated"] != "0000-00-00 00:00:00" && $reportTemplateInfo["LastGenerated"] != "") {
							$ReportTypeOption[$ReportTypeOptionCount][0] = $ReportTypeArr[$j]['ReportID'];
							$ReportTypeOption[$ReportTypeOptionCount][1] = $ReportTypeArr[$j]['SemesterTitle'];
							$ReportTypeOptionCount++;
							
							$ReportIDList[] = $ReportTypeArr[$j]['ReportID'];
						}
					}
					
					sort($ReportIDList);
					
					for($j=0; $j<sizeof($ReportTypeArr); $j++){
						$reportTemplateInfo = $lreportcard->returnReportTemplateBasicInfo($ReportTypeArr[$j]['ReportID']);
						$ReportIDOrder = array_search($ReportTypeArr[$j]['ReportID'], $ReportIDList);
						if ($reportTemplateInfo["LastGenerated"] != "0000-00-00 00:00:00" && $reportTemplateInfo["LastGenerated"] != "") {		
							// Alternate sets of ReportColumn (Assessment, Term or Whole year)
							$ColumnTitleAry = $lreportcard->returnReportColoumnTitle($ReportTypeArr[$j]['ReportID']);
							$ColumnOption = array();
							################## Customization for SIS ##################
							# if it is secondary school
							if (strtolower(substr($ClassLevelName, 0, 1)) == "s") {
								# Year-End report
								if ($reportTemplateInfo["Semester"] == "F" && sizeof($ColumnTitleAry) == 4) {
									$ColumnOption[0] = array("", $eReportCard['Template']['OverallCombined']);
									//$ColumnOption[1] = array("first", $eReportCard['Template']['FirstCombined']);
									//$ColumnOption[1] = array("second", $eReportCard['Template']['SecondCombined']);
								# Mid-term report
								} else {
									if ($ReportIDOrder == 0) $ColumnOption[0] = array("", $eReportCard['Template']['FirstCombined']);
									if ($ReportIDOrder == 1) $ColumnOption[0] = array("", $eReportCard['Template']['SecondCombined']);
								}
							# if it is primary school
							} else if (strtolower(substr($ClassLevelName, 0, 1)) == "p") {
								# Year-End report
								if ($reportTemplateInfo["Semester"] == "F") {
									if (strtolower(substr($ClassLevelName, 1, 1)) == "1" || strtolower(substr($ClassLevelName, 1, 1)) == "2") {
										$ColumnOption[0] = array("", $eReportCard['Template']['OverallCombined']);
									} else {
										$ColumnOption[0] = array("", $eReportCard['Template']['OverallCombined']);
										
										$countColumn = 1;
										foreach ($ColumnTitleAry as $columnID => $columnTitle) {
											if (sizeof($ColumnTitleAry) != 1 && $countColumn == 2) {
												$ColumnOption[1][0] = $columnID;
												$ColumnOption[1][1] = "SA2";
												break;
											}
											$ColumnOption[$countColumn][0] = $columnID;
											$ColumnOption[$countColumn][1] = $columnTitle;
											if ($countColumn == 1) $ColumnOption[$countColumn][1] = "SA1";
											if ($countColumn == 2) $ColumnOption[$countColumn][1] = "SA2";
											$countColumn++;
										}
										
									}
								# Mid-term report
								} else {
									$ColumnOption[0] = array("", "SA1");
								}
							} else {
								$ColumnOption[0] = array("", $eReportCard['Template']['OverallCombined']);
								$countColumn = 1;
								foreach ($ColumnTitleAry as $columnID => $columnTitle) {
									$ColumnOption[$countColumn][0] = $columnID;
									$ColumnOption[$countColumn][1] = $columnTitle;
									$countColumn++;
								}
							}
							
							$OtherReportColumnSelection[$ReportTypeArr[$j]['ReportID']] = $linterface->GET_SELECTION_BOX($ColumnOption, 'name="ReportColumnID" id="ReportColumnID" class="" onchange=""', '');
						}
					}
				}
				
				$ReportTypeSelectDisabled = "";
				if (sizeof($ReportTypeOption) == 0) {
					$ReportTypeOption[0][0] = "-1";
					$ReportTypeOption[0][1] = $eReportCard['NoReportAvailable'];
					$ReportTypeSelectDisabled = 'disabled="disabled"';
				}
				
				$OtherReportTypeSelection[$FormArr[$i][0]] = $linterface->GET_SELECTION_BOX($ReportTypeOption, 'name="ReportID" id="ReportID" '.$ReportTypeSelectDisabled.' onchange="loadColumnList(this.options[this.selectedIndex].value)"', '');
			}
		}
		
		# Subject Type Selection (exam, non-exam)
		$SubjectTypeArr = array();
		$SubjectTypeArr[] = array("all", $eReportCard['AllSubjectType']);
		$SubjectTypeArr[] = array("exam", $eReportCard['ExamSubject']);
		$SubjectTypeArr[] = array("nonExam", $eReportCard['NonExamSubject']);
		$SubjectTypeSelection = $linterface->GET_SELECTION_BOX($SubjectTypeArr, 'name="SubjectType" id="SubjectType"', $SubjectType);
		
		
		############## Interface Start ##############
		$linterface = new interface_html();
		$CurrentPage = "Reports_GrandMarksheet";
		$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
		# tag information
		$TAGS_OBJ[] = array($eReportCard['Reports_GrandMarksheet']);
		# page navigation (leave the array empty if no need)
		$PAGE_NAVIGATION[] = array($eReportCard['SelectCriteria']);
		$linterface->LAYOUT_START();
?>

<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery-1.3.2.min.js"></script>
<script type="text/JavaScript" language="JavaScript">
// Preloaded sets of Report list
var classLevelReportList = new Array();
var classLevelSubjectList = new Array();
var classLevelReportColunmList = new Array();
var classLevelClassList = new Array();

var classLevelList = ""; 
var classLevelListPupilProgress = "";
<?php
	foreach($OtherReportTypeSelection as $key => $value) {
		$value = str_replace("\n", "", $value);
		$x .= "classLevelReportList[".$key."] = '$value';\n";
	}
	echo $x;
	
	foreach($OtherSubjectSelection as $key => $value) {
		$value = str_replace("\n", "", $value);
		$y .= "classLevelSubjectList[".$key."] = '$value';\n";
	}
	echo $y;
	
	foreach($OtherReportColumnSelection as $key => $value) {
		$value = str_replace("\n", "", $value);
		$z .= "classLevelReportColunmList[".$key."] = '$value';\n";
	}
	echo $z;
	
	foreach($OtherClassSelection as $key => $value) {
		
		$value = str_replace("\n", "", $value);
		$w .= "classLevelClassList[".$key."] = '$value';\n";
	}
	echo $w;
	
	$value = str_replace("\n", "", $FormSelection);
	$v = "classLevelList = '$value';\n";
	echo $v;
	
	$value = str_replace("\n", "", $FormSelectionForPupilProgress);
	$t = "classLevelListPupilProgress = '$value';\n";
	echo $t;
?>

function loadReportList(classLevelID) {
	document.getElementById("ReportList").innerHTML = classLevelReportList[classLevelID];
	document.getElementById("SubjectList").innerHTML = classLevelSubjectList[classLevelID];
	document.getElementById("ClassList").innerHTML = classLevelClassList[classLevelID];
	
	var reportIDSelect = document.getElementById("ReportID");
	loadColumnList(reportIDSelect.options[reportIDSelect.selectedIndex].value);
	
	if (reportIDSelect.disabled) {
		document.getElementById("submitBtn").disabled = true;
	} else {
		document.getElementById("submitBtn").disabled = false;
	}
	
	jsSelectAllOption('SubjectID', true);
	disableReportIDSelect();
}

function loadColumnList(reportID) {
	document.getElementById("ColumnList").innerHTML = classLevelReportColunmList[reportID];
	
	//var grandMarkSheetTypeSelect = document.getElementById("GrandMarksheetType");
	//changeGrandMarksheetType(grandMarkSheetTypeSelect.options[grandMarkSheetTypeSelect.selectedIndex].value);
}

function changeGrandMarksheetType(grandMarkSheetType) {
	var FormCell = document.getElementById("FormCell");
	var ClassRow = document.getElementById("ClassRow");
	var SubjectRow = document.getElementById("SubjectRow");
	var ColumnRow = document.getElementById("ColumnRow");
	var SubjectTypeRow = document.getElementById("SubjectTypeRow");
	
	if (grandMarkSheetType == 0) {
		FormCell.innerHTML = classLevelList;
		ClassRow.style.display = "";
		SubjectRow.style.display = "none";
		ColumnRow.style.display = "";
		SubjectTypeRow.style.display = "";
	} else if (grandMarkSheetType == 1) {
		FormCell.innerHTML = classLevelList;
		ClassRow.style.display = "none";
		SubjectRow.style.display = "";
		ColumnRow.style.display = "";
		SubjectTypeRow.style.display = "none";
	} else if (grandMarkSheetType == 3) {
		FormCell.innerHTML = classLevelList;
		ClassRow.style.display = "none";
		SubjectRow.style.display = "none";
		ColumnRow.style.display = "";
		SubjectTypeRow.style.display = "none";
	} else {
		FormCell.innerHTML = classLevelListPupilProgress;
		ClassRow.style.display = "";
		SubjectRow.style.display = "";
		ColumnRow.style.display = "none";
		SubjectTypeRow.style.display = "none";
	}
	
	var classLevelIDSelect = document.getElementById("ClassLevelID");
	document.getElementById("ClassList").innerHTML = classLevelClassList[classLevelIDSelect.options[classLevelIDSelect.selectedIndex].value];
	jsSelectAllOption('SubjectID', true);
	
	disableReportIDSelect();
}

function disableReportIDSelect() {
	var reportIDSelect = document.getElementById("ReportID");
	var grandMarkSheetTypeSelect = document.getElementById("GrandMarksheetType");
	var grandMarkSheetType = grandMarkSheetTypeSelect.options[grandMarkSheetTypeSelect.selectedIndex].value;
	var ReportRow = document.getElementById("ReportRow");
	
	if (reportIDSelect.options[0].value != "-1") {
		if (grandMarkSheetType == 2) {
			//reportIDSelect.disabled = true;
			reportIDSelect.selectedIndex = reportIDSelect.length - 1;
			ReportRow.display = "none";
		} else {
			//reportIDSelect.disabled = false;
			reportIDSelect.selectedIndex = 0;
			ReportRow.display = "";
		}
	}
}

function resetForm() {
	document.form1.reset();
	return true;
}

function checkForm() {
	//obj = document.form1;
	//obj.submit();
	
	var GrandMS_Type_Sel = document.getElementById("GrandMarksheetType");
	var GrandMS_Type_Val = GrandMS_Type_Sel.options[GrandMS_Type_Sel.selectedIndex].value;
	
	var CSV_Checked = document.getElementById("ViewFormat_CSV").checked;
	
	if (GrandMS_Type_Val == 0)	// Class Summary
	{
		var SubjectType_Sel = document.getElementById("SubjectType");
		var SubjectType_Val = SubjectType_Sel.options[SubjectType_Sel.selectedIndex].value;
		
		if (CSV_Checked==true && SubjectType_Val=='all')
		{
			alert("<?=$eReportCard['GrandMarksheetAlertArr']['CSV_Export_Not_Support_All_Subject_Type']?>");
			return false;
		}
	}
	else if (GrandMS_Type_Val == 1 || GrandMS_Type_Val == 2)	// Level Ranking or Pupils' Progress
	{
		if ($('#SubjectID').val() == null)
		{
			alert("<?=$eReportCard['GrandMarksheetAlertArr']['PleaseSelectSubject']?>");
			return false;
		}
		else if (CSV_Checked==true)
		{
			var SubjectListText = $('#SubjectID').val().toString();
			var SubjectListArr = SubjectListText.split(',');
			var NumSelectedSubject = SubjectListArr.length;
			
			if (NumSelectedSubject > 1)
			{
				alert("<?=$eReportCard['GrandMarksheetAlertArr']['CSV_Export_Not_Support_Multiple_Subject']?>");
				return false;
			}
		}
	}
		
	return true;
}

</script>
<link type="text/css" rel="stylesheet" href="<?=$PATH_WRT_ROOT."templates/2007a/css/"?>ereportcard.css">
<br />
<form name="form1" action="generate.php" method="POST" onsubmit="return checkForm();" target="_blank">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	</tr>
<table>
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>
			<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr>
					<td></td>
					<td align="right"><?=$linterface->GET_SYS_MSG($Result);?></td>
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?= $eReportCard['GrandMarksheetType'] ?>
					</td>
					<td width="75%" class='tabletext'>
						<span id="GrandMarksheetTypeList"><?= $GrandMarksheetTypeSelection ?></span>
					</td>
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?= $eReportCard['Form'] ?>
					</td>
					<td id="FormCell" width="75%" class='tabletext'>
						<?= $FormSelection ?>
					</td>
				</tr>
				<tr id="ClassRow" style="<?= $ClassRowDisplay ?>">
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?= $eReportCard['Class'] ?>
					</td>
					<td width="75%" class='tabletext'>
						<span id="ClassList"><?= $ClassSelection ?></span>
					</td>
				</tr>
				<tr id="SubjectRow" style="<?= $SubjectRowDisplay ?>">
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?= $eReportCard['Subject'] ?>
					</td>
					<td width="75%" class='tabletext'>
						<span id="SubjectList"><?= $SubjectSelection ?></span><?=$SelectAllBtn?>
					</td>
				</tr>
				<tr id="ReportRow" style="<?= $ReportRowDisplay ?>">
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?= $eReportCard['ReportCard'] ?>
					</td>
					<td width="75%" class='tabletext'>
						<span id="ReportList"><?= $ReportTypeSelection ?></span>
					</td>
				</tr>
				<tr id="ColumnRow" style="<?= $ColumnRowDisplay ?>">
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?= $eReportCard['Term'] ?>
					</td>
					<td width="75%" class='tabletext'>
						<span id="ColumnList"><?= $ColumnSelection ?></span>
					</td>
				</tr>
				
				<tr id="SubjectTypeRow" style="<?= $SubjectTypeRowDisplay ?>">
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?= $eReportCard['SubjectType'] ?>
					</td>
					<td width="75%" class='tabletext'>
						<?=$SubjectTypeSelection?>
					</td>
				</tr>
				
				<tr id="ViewFormatRow" style="<?= $ViewFormatRowDisplay ?>">
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?= $eReportCard['ViewFormat'] ?>
					</td>
					<td width="75%" class='tabletext'>
						<input type="radio" name="ViewFormat" id="ViewFormat_HTML" value="html" CHECKED>
							<label for="ViewFormat_HTML">
								<?=$eReportCard['HTML']?>
							</label>
						</input>
						&nbsp;&nbsp;
						<input type="radio" name="ViewFormat" id="ViewFormat_CSV" value="csv">
							<label for="ViewFormat_CSV">
								<?=$eReportCard['CSV']?>
							</label>
						</input>
					</td>
				</tr>
				
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr>
					<td class="dotline">
						<img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1">
					</td>
				</tr>
				<tr>
					<td align="center">
						<?= $submitBtn ?>
						<? #$linterface->GET_ACTION_BTN($button_reset, "reset", "")?>
						<? #$linterface->GET_ACTION_BTN($button_cancel, "button", "window.location = 'schedule.php'")?>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</form>
<?
	print $linterface->FOCUS_ON_LOAD("form1.ClassLevelID");
  	$linterface->LAYOUT_STOP();
  } else {
?>
You have no priviledge to access this page.
<?
	}
} else {
?>
You have no priviledge to access this page.
<?
}
?>
