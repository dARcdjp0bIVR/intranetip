<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";
 ini_set('display_errors',1);
	error_reporting(E_ALL ^ E_NOTICE);
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
//include_once($PATH_WRT_ROOT."includes/libreportcard2008w.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008j.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_generate_sis.php");
//include_once($PATH_WRT_ROOT."includes/libreportcard2008_sis.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/sis.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

//$lreportcard = new libreportcardSIS();
$lreportcard = new libreportcardcustom();
$lreportcardgenerate = new libreportcard2008_generate();
$linterface = new interface_html();
$lclass = new libclass();

# Retrieve params
$ReportColumnID	= $ReportColumnID? $ReportColumnID : "0";
$SubjectIDArr		= $SubjectID 	? $SubjectID 	: "";
$ClassLevelID	= $ClassLevelID	? $ClassLevelID : "";
$ClassID		= "";
$ReportID		= $ReportID		? $ReportID : "";
$ClassLevel 	= $lreportcard->returnClassLevel($ClassLevelID);
$PSLevel		= substr($ClassLevel, 0, 1);
$Level			= substr($ClassLevel, 1, 1);
$ReportSetting 	= $lreportcard->returnReportTemplateBasicInfo($ReportID);
$SemID 			= $ReportSetting['Semester'];
$ReportType 	= $SemID == "F" ? "W" : "T";


$StudentAry = array();
$ListAry = array();
$ListAry2 = array();
$DataTemp = array();
$ColTotal = array();
$LastGenerated = $lreportcard->GET_CURRENT_DATETIME();

$SpecialCaseArr = $lreportcard->GET_POSSIBLE_MARKSHEET_SPECIAL_CASE_SET1();

# check condition
if(!$ClassLevelID || !$ReportID)
{
	intranet_closedb();
	header("Location: index.php");
}

include_once($PATH_WRT_ROOT."templates/2007a/layout/print_header.php");
# Retrieve Student Array
$classArr = $lreportcard->GET_CLASSES_BY_FORM($ClassLevelID, $ClassID);


$numOfSubject= count($SubjectIDArr);
$ReportTable = '';
for ($i=0; $i<$numOfSubject; $i++)
{
	$SubjectID = $SubjectIDArr[$i];
	$display = '';
	$x = '';
	$SubjectAry = array();
	$ReportIDList = array();
	$StudentAry = array();
	$ListAry = array();
	$ListAry2 = array();
	$DataTemp = array();
	$PColID = array();
	$ColTotal = array();
	$field1 = array();
	$cons = '';
	$where = array();
	
	foreach($classArr as $key => $data)
	{
		$thisClassID = $data['ClassID'];
		$StudentAryTemp = $lreportcard->GET_STUDENT_BY_CLASS($thisClassID);
		foreach($StudentAryTemp as $k=>$d)
		{
			list($thisUserID, $WebSAMS, $ClassNumber, $EngName) = $d;
			$StudentAry[] = $thisUserID;
			
			$lu = new libuser($thisUserID);
			$DataTemp[$thisUserID]['StudentName'] = $lu->EnglishName ." (". $lu->ChineseName .")";
			$DataTemp[$thisUserID]['ClassName'] = $lu->ClassName;
//			$DataTemp[$thisUserID]['AdmissionNo'] = $lu->AdmissionNo;
			$DataTemp[$thisUserID]['AdmissionNo'] = strtoupper($lu->UserLogin);
			$DataTemp[$thisUserID]['ClassNumber'] = $lu->ClassNumber; 
		}
	}
	
	if($SubjectID)	# 1 subject
	{
		$SubjectFormGradingSettings = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID);
		$SchemeID = $SubjectFormGradingSettings[SchemeID];
		$ScaleDisplay = $SubjectFormGradingSettings[ScaleDisplay];
		
		$SubjectAry[] = $SubjectID;
		$SubjectStr = $lreportcard->GET_SUBJECT_NAME_LANG($SubjectID);
	}
	else			# all subjects
	{
		$SubjectAryTmp = $lreportcard->returnSubjectwOrder($ClassLevelID);
	
		foreach($SubjectAryTmp as $s => $temp)
		{
			$thisSubjectFormGradingSettings = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $s);
			$thisScaleDisplay = $thisSubjectFormGradingSettings[ScaleDisplay];
			
			# check Subject wegith is 0 or not
			$thisWeightAry = $lreportcard->returnReportTemplateSubjectWeightData($ReportID, "SubjectID=$s and ReportColumnID is NULL");
			$thisWeight = $thisWeightAry[0]['Weight'];
			
			if($thisScaleDisplay=="M" && $thisWeight)			$SubjectAry[] = $s;
		}
		
		$SubjectStr = "Total";
	}
	
	# Grand MS  Title
	$SemNameAry = $lreportcard->returnReportColoumnTitle($ReportID);
	if($PSLevel=="P")
	{
		if($Level==1 || $Level==2)
			$SemStr = "Overall";
		else
		{
			if($ReportType=="T")
				$SemStr = "SA1";
			else
				$SemStr = $ReportColumnID ? "SA2 " : "Overall";
		}	
	}
	else
	{
		$ReportTypeArr = $lreportcard->GET_REPORT_TYPES($ClassLevelID);
		foreach($ReportTypeArr as $k=>$d)
			$ReportIDList[] = $d['ReportID'];
		sort($ReportIDList);
		$ReportIDOrder = array_search($ReportID, $ReportIDList);
		switch($ReportIDOrder)
		{
			case 0:
				$CASA = 1;
				$ComStr = "1st Com";
				$SemStr = "1st Combined";
				break;
			case 1:
				$CASA = 2;
				$ComStr = "2nd Com";
				$SemStr = "2nd Combined";
				break;
			case 2:
				$CASA = "";
				$ComStr = "Overall Com";
				$SemStr = "Whole Year";
				break;	
		}
		
		$ColumnData = $lreportcard->returnReportTemplateColumnData($ReportID);
		foreach($ColumnData as $k => $d)
		{
			$PColID[] = $d['ReportColumnID'];
		}	
		$PColID[] = 0;
	}
	$Title = "Level Ranking - ". $SemStr ." ". $SubjectStr ." (". ($PSLevel=="P" ? "Primary" : "Secondary") .")";
	
	# define where condition
	if($PSLevel=="P")	$where[] = "ReportColumnID='$ReportColumnID'";
	if($SubjectID)	$where[] = "SubjectID='$SubjectID'";
	if($ReportID)	$where[] = "ReportID='$ReportID'";
	$cons = implode(" and ", $where);
	$cons = $cons ? " and ".$cons : "";
	
	foreach($StudentAry as $key => $sid)
	{
		if($PSLevel=="P")		# Primary
		{
			$m = array();
			
			if($SubjectID)		# 1 Subject
			{
				$result = $lreportcard->getMarks($ReportID, $sid, $cons);
				foreach((array)$result as $subjid => $data)
				{
					$SubjectFormGradingSettings = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $subjid);
					$SchemeID = $SubjectFormGradingSettings[SchemeID];
				
					//$thisTargetMark = $lreportcard->Get_Raw_Or_Rounded_Mark($data[$ReportColumnID]['RawMark'], $data[$ReportColumnID]['Mark']);
					$thisTargetMark = $data[$ReportColumnID]['Mark'];
					$thisMark = my_round($thisTargetMark,1);
					list($thisMark, $nsp) = $lreportcard->checkSpCase($ReportID, $subjid, $thisMark, $data[$ReportColumnID]['Grade']);
			  				
					$DataTemp[$sid][$subjid]['Mark'] = $thisMark;
					$DataTemp[$sid][$subjid]['Avg'] = $thisMark;
					$DataTemp[$sid][$subjid]['Grade'] = $data[$ReportColumnID]['Grade'];
					$m[$ReportColumnID][$subjid] = $thisMark;
					
					if($nsp)
					{
						$ColTotal[$ReportColumnID]['Mark'] += $thisMark;
						$ColTotal[$ReportColumnID]['Avg'] += $thisMark;
						
						# exclude students who have not taken any exams
						$thisGrandTotalFullMark = $lreportcard->returnGrandTotalFullMark($ReportID, $sid);
						if ($thisGrandTotalFullMark != 0)
						{
							$ColTotal[$ReportColumnID]['no']++;
						}
					}
					else
					{
						if(!in_array($DataTemp[$sid][$subjid]['Grade'], $SpecialCaseArr))
							$ColTotal[$ReportColumnID]['no']++;
					}
					$thisBand = $lreportcard->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($SchemeID, $thisMark);
					$DataTemp[$sid][$subjid]['Band'] = $thisBand;
					
				}
			}
			else			# all Subjects
			{
				$result = $lreportcard->getMarks($ReportID, $sid, $cons);
				foreach((array)$result as $subjid => $data)
				{
					//$thisTargetMark = $lreportcard->Get_Raw_Or_Rounded_Mark($data[$ReportColumnID]['RawMark'], $data[$ReportColumnID]['Mark']);
					$thisTargetMark = $data[$ReportColumnID]['Mark'];
					$m[$ReportColumnID][$subjid] = my_round($thisTargetMark, 1);
				}
				$t = $lreportcardgenerate->CALCULATE_COLUMN_MARK($ReportID, $ClassLevelID, $sid, $m[$ReportColumnID]);
				$DataTemp[$sid][$ReportColumnID]['Mark'] = my_round($t['GrandTotal'],1);
				$DataTemp[$sid][$ReportColumnID]['Avg'] = my_round($t['GrandAverage'],1);
				
				$ColTotal[$ReportColumnID]['Mark'] += my_round($t['GrandTotal'],1);
				$ColTotal[$ReportColumnID]['Avg'] += my_round($t['GrandAverage'],1);
				
				# exclude students who have not taken any exams
				$thisGrandTotalFullMark = $lreportcard->returnGrandTotalFullMark($ReportID, $sid);
				if ($thisGrandTotalFullMark != 0)
				{
					$ColTotal[$ReportColumnID]['no']++;
				}
			}
		}
		else					# Secondary
		{
			if($CASA)		#CA/SA1, CA/SA2
			{
				if($SubjectID)	# 1 subject
				{
					$result = $lreportcard->getMarks($ReportID, $sid, $cons, $debug);
					
					foreach((array)$result as $subjid => $data)
					{
						if(in_array($subjid, $SubjectAry)===false)	continue;
						
		 				$SubjectFormGradingSettings = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $subjid);
		 	 			$SchemeID = $SubjectFormGradingSettings[SchemeID];
			 			
			 			foreach($data as $colid => $m_ary)
						{
							//$thisTargetMark = $lreportcard->Get_Raw_Or_Rounded_Mark($m_ary['RawMark'], $m_ary['Mark']);
							$thisTargetMark = $m_ary['Mark'];
							$thisMark = my_round($thisTargetMark,1);
			  				list($thisMark, $nsp) = $lreportcard->checkSpCase($ReportID, $subjid, $thisMark, $m_ary['Grade']);
			  				
							$DataTemp[$sid][$colid]['Mark'] = $thisMark;
							$thisGrade = $lreportcard->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($SchemeID, $thisMark);
							$DataTemp[$sid][$colid]['Grade'] = $thisGrade;
							
				 			if($nsp)
				 			{
				 		 			$ColTotal[$colid]['Mark'] += $thisMark;
				 		 			
				 		 			# exclude students who have not taken any exams
									$thisGrandTotalFullMark = $lreportcard->returnGrandTotalFullMark($ReportID, $sid);
									if ($thisGrandTotalFullMark != 0)
									{
										$ColTotal[$colid]['no']++;
									}
			 				}
			 				else
			 				{
					 				if(!in_array($m_ary['Grade'], $SpecialCaseArr))
										$ColTotal[$colid]['no']++;		
			 				}
						}
					}
				}
				else	# All subjects
				{
					$result = $lreportcard->getMarks($ReportID, $sid, $cons);
					
					foreach((array)$result as $subjid => $data)
					{
						foreach($data as $colid => $m_ary)
						{
							//$thisTargetMark = $lreportcard->Get_Raw_Or_Rounded_Mark($m_ary['RawMark'], $m_ary['Mark']);
							$thisTargetMark = $m_ary['Mark'];
							$thisMark = my_round($thisTargetMark,1);
			  				list($thisMark, $nsp) = $lreportcard->checkSpCase($ReportID, $subjid, $thisMark, $m_ary['Grade']);
			  				
	 						$m[$colid][$subjid] = $thisMark;
						}
					}
	
	 				foreach($PColID as $k1=>$thisColID)
	 				{
						$t = $lreportcardgenerate->CALCULATE_COLUMN_MARK($ReportID, $ClassLevelID, $sid, $m[$thisColID]);	 				
						$DataTemp[$sid][$thisColID]['Mark'] = my_round($t['GrandAverage'],1);
						
	 					$ColTotal[$thisColID]['Mark'] += my_round($t['GrandAverage'],1);
	 					
	 					# exclude students who have not taken any exams
						$thisGrandTotalFullMark = $lreportcard->returnGrandTotalFullMark($ReportID, $sid);
						if ($thisGrandTotalFullMark != 0)
						{
							$ColTotal[$thisColID]['no']++;
						}
	 				}
				}
			}
			else		# whole year
			{
				if($SubjectID)
				{
					foreach($ReportIDList as $k=>$thisReportID)
					{
						$result = $lreportcard->getMarks($thisReportID, $sid, "and ReportColumnID='$ReportColumnID'");	
						foreach((array)$result as $subjid => $data)
						{
							if(in_array($subjid, $SubjectAry)===false)	continue;
							
							foreach($data as $k1 => $m_ary)
							{
								//$thisTargetMark = $lreportcard->Get_Raw_Or_Rounded_Mark($m_ary['RawMark'], $m_ary['Mark']);
								$thisTargetMark = $m_ary['Mark'];
								$thisMark = my_round($thisTargetMark,1);
				  				list($thisMark, $nsp) = $lreportcard->checkSpCase($thisReportID, $subjid, $thisMark, $m_ary['Grade']);
				  				
								$DataTemp[$sid][$thisReportID][$subjid]['Mark'] = $thisMark;
					 			if($nsp)
					 			{
					 		 			$ColTotal[$thisReportID]['Mark'] += $thisMark;
					 		 			
					 		 			# exclude students who have not taken any exams
										$thisGrandTotalFullMark = $lreportcard->returnGrandTotalFullMark($ReportID, $sid);
										if ($thisGrandTotalFullMark != 0)
										{
											$ColTotal[$thisReportID]['no']++;
										}
				 				}
				 				else
				 				{
						 				if(!in_array($m_ary['Grade'], $SpecialCaseArr))
											$ColTotal[$thisReportID]['no']++;		
				 				}
							}
						}
					}
				}
				else
				{
					foreach($ReportIDList as $k=>$thisReportID)
					{
						$t = $lreportcard->getReportResultScore($thisReportID, 0, $sid);
						//$thisTargetMark = $lreportcard->Get_Raw_Or_Rounded_Mark($t['RawGrandAverage'], $t['GrandAverage']);
						$thisTargetMark = $t['GrandAverage'];
						
	 					$DataTemp[$sid][$thisReportID]['Mark'] = $thisTargetMark;
						$ColTotal[$thisReportID]['Mark'] += $thisTargetMark;
	 		 			
	 		 			# exclude students who have not taken any exams
						$thisGrandTotalFullMark = $lreportcard->returnGrandTotalFullMark($ReportID, $sid);
						if ($thisGrandTotalFullMark != 0)
						{
							$ColTotal[$thisReportID]['no']++;
						}
					}
				}
			}
		}
	}
	
	foreach($StudentAry as $k=>$stu)
	{
		$thisStudentID = $stu;
		
		if($SubjectID)
		{
			$thisMark = $DataTemp[$thisStudentID][$SubjectID]['Mark'];
			$thisBand = $lreportcard->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($SchemeID, $thisMark);
			
			list($thisMark, $nsp) = $lreportcard->checkSpCase($ReportID, $SubjectID, $thisMark, $DataTemp[$thisStudentID][$SubjectID]['Grade']);
			if($nsp) 
			{
				$ListAry[$thisStudentID]['StudentName'] = $DataTemp[$thisStudentID]['StudentName'];
				$ListAry[$thisStudentID]['AdmissionNo'] = $DataTemp[$thisStudentID]['AdmissionNo'];
				$ListAry[$thisStudentID]['ClassName'] = $DataTemp[$thisStudentID]['ClassName'];
				$ListAry[$thisStudentID]['ClassNumber'] = $DataTemp[$thisStudentID]['ClassNumber'];
				
				if($PSLevel=="P")
				{
					if($ScaleDisplay=="M")	
					{
						$ListAry[$thisStudentID]['Total'] = $thisMark;
						$ListAry[$thisStudentID]['Percent'] = $DataTemp[$thisStudentID][$SubjectID]['Avg'];
					}
					else
					{
						$ListAry[$thisStudentID]['Total'] = $DataTemp[$thisStudentID][$SubjectID]['Grade'];
						$ListAry[$thisStudentID]['Percent'] = "";
					}
					
					$ListAry[$thisStudentID]['Band'] = $thisBand;
				}
				else
				{
					if($CASA)	# CA1, SA1, CA2, SA2
					{
						foreach($PColID as $k1=>$thisColID)
						{
							if($thisColID)
								$ListAry[$thisStudentID][$SubjectID.$thisColID."Mark"] = $DataTemp[$thisStudentID][$thisColID]['Mark'];
							else
								$ListAry[$thisStudentID]['Total'] = $DataTemp[$thisStudentID][$thisColID]['Mark'];
						}
						
			 			# Grade
			 			$ListAry[$thisStudentID][$SubjectID.$thisColID."Grade"] = $DataTemp[$thisStudentID][0]['Grade'];
						
					}
					else		# whole year
					{
						foreach($ReportIDList as $k=>$thisReportID)
						{
							if($thisReportID==$ReportID)
								$ListAry[$thisStudentID]['Total'] = $DataTemp[$thisStudentID][$thisReportID][$SubjectID]['Mark'];
							else
								$ListAry[$thisStudentID][$SubjectID.$thisReportID."Mark"] = $DataTemp[$thisStudentID][$thisReportID][$SubjectID]['Mark'];
						}
						$ListAry[$thisStudentID]['Band'] = $lreportcard->returnGrandMSBand($ClassLevelID, $ListAry[$thisStudentID]['Total']);
					}
					
				}
			}
			else
			{
				$ListAry2[$thisStudentID]['StudentName'] = $DataTemp[$thisStudentID]['StudentName'];
				$ListAry2[$thisStudentID]['AdmissionNo'] = $DataTemp[$thisStudentID]['AdmissionNo'];
				$ListAry2[$thisStudentID]['ClassName'] = $DataTemp[$thisStudentID]['ClassName'];
				$ListAry2[$thisStudentID]['ClassNumber'] = $DataTemp[$thisStudentID]['ClassNumber'];
				$ListAry2[$thisStudentID]['Total'] = $thisMark;
				$ListAry2[$thisStudentID]['Percent'] = "";
				$ListAry2[$thisStudentID]['Band'] = "";
			}
		}
		else 
		{
			$ListAry[$thisStudentID]['StudentName'] = $DataTemp[$thisStudentID]['StudentName'];
			$ListAry[$thisStudentID]['AdmissionNo'] = $DataTemp[$thisStudentID]['AdmissionNo'];
			$ListAry[$thisStudentID]['ClassName'] = $DataTemp[$thisStudentID]['ClassName'];
			$ListAry[$thisStudentID]['ClassNumber'] = $DataTemp[$thisStudentID]['ClassNumber'];
			
			if($PSLevel=="P")
			{
				$thisMark = $DataTemp[$thisStudentID][$ReportColumnID]['Mark'];
				$ListAry[$thisStudentID]['Total'] = $thisMark;
				$ListAry[$thisStudentID]['Percent'] = $DataTemp[$thisStudentID][$ReportColumnID]['Avg'];
			}
			else
			{
				if($CASA)	# CA1, SA1, CA2, SA2
				{
					foreach($PColID as $k1=>$thisColID)
					{
						if($thisColID)
							$ListAry[$thisStudentID][$SubjectID.$thisColID."Mark"] = $DataTemp[$thisStudentID][$thisColID]['Mark'];
						else
							$ListAry[$thisStudentID]['Total'] = $DataTemp[$thisStudentID][$thisColID]['Mark'];
					}
						
					$ListAry[$thisStudentID]['Band'] = $lreportcard->returnGrandMSBand($ClassLevelID, $ListAry[$thisStudentID]['Total']);
				}
				else
				{
					foreach($ReportIDList as $k=>$thisReportID)
					{
						if($thisReportID==$ReportID)
							$ListAry[$thisStudentID]['Total'] = $DataTemp[$thisStudentID][$thisReportID]['Mark'];
						else
							$ListAry[$thisStudentID][$thisReportID."Mark"] = $DataTemp[$thisStudentID][$thisReportID]['Mark'];
					}
					$ListAry[$thisStudentID]['Band'] = $lreportcard->returnGrandMSBand($ClassLevelID, $ListAry[$thisStudentID]['Total']);
				}
		
			}
		}
	}
	
	# sort by Marks
	foreach ($ListAry as $key => $row) 
	{
		$field1[$key] = $row['Total'];
	}
	
	array_multisort($field1, SORT_DESC, $ListAry);
	$ListAry = array_merge($ListAry, $ListAry2);
	
	$display = "";
	$rankingCounter = 0;
	$sameRankingCounter = 0;
	$rowCounter = 0;
	$lastOverall = 0;
	
	if ($PSLevel=="P")
	{
		$rankingColumn = 5;
	}
	else
	{
		$rankingColumn = 6;
	}
	
	foreach($ListAry as $key=>$row)
	{
		$rowCounter++;
		
		$css = "tablerow" . (($rowCounter%2)+1);
		
		$display .= "<tr>";		
		
		$ki = 0;
		$thisRowOthersInfo = "";
		$thisRowRanking = "";
		foreach($row as $k => $d)	
		{
			$thisRowOthersInfo .= "<td class=\"$css tabletext tablerow_separator\" align=". ($ki>1 ? "center":"").">{$d}&nbsp;</td>";
			
			# updated on 11 Dec by Ivan - same ranking if the same % Total
			if ($ki == $rankingColumn) // % Total
			{
				if ($d != $lastOverall)
				{
					$rankingCounter++;
					$rankingCounter += $sameRankingCounter;		// Ranking: 1, 2, 2, 4
					
					$sameRankingCounter = 0;
				}
				else
				{
					$sameRankingCounter++;
				}
				$thisRowRanking = "<td class=\"$css tabletext tablerow_separator\">".$rankingCounter."&nbsp;</td>";
				$lastOverall = $d;
			}
			$ki++;
		}
		$display .= $thisRowRanking.$thisRowOthersInfo;
		$display .= "</tr>";
	}
	
	# Footer
	$display .= "<tr>";
	$display .= "<td>&nbsp;</td>";
	$display .= "<td><span class='GMS_text16bi'>Total Students:</span> <span class='tabletext'>". sizeof($StudentAry) ."</span></td>";
	$display .= "<td align='right'><span class='GMS_text16bi'>Average:</span></td>";
	$display .= "<td>&nbsp;</td>";
	$display .= "<td>&nbsp;</td>";	
	if($PSLevel=="P")
	{
		$display .= "<td class='tabletext' align='center'>". @my_round(($ColTotal[$ReportColumnID]['Mark'] / $ColTotal[$ReportColumnID]['no']), 1) ."</td>";
		$display .= "<td class='tabletext' align='center'>". @my_round(($ColTotal[$ReportColumnID]['Avg'] / $ColTotal[$ReportColumnID]['no']), 1) ."</td>";
	}
	else
	{
		if($CASA)
		{
			foreach($PColID as $k1=>$thisColID)
				$display .= "<td class='tabletext' align='center'>". @my_round(($ColTotal[$thisColID]['Mark'] / $ColTotal[$thisColID]['no']), 1) ."</td>";
		}
		else
		{
			foreach($ReportIDList as $k=>$thisReportID)
				$display .= "<td class='tabletext' align='center'>". @my_round(($ColTotal[$thisReportID]['Mark'] / $ColTotal[$thisReportID]['no']), 1) ."</td>";
		}	
	}
	$display .= "</tr>";
	
	# Table header
	$th = array();
	$th[]= "Ranking";
	$th[]= "Name";
	$th[]= "Student No.";
	$th[]= "Class";
	$th[]= "Class Index";
	if($PSLevel=="P")
	{
		$th[]= $SemStr. " Total";
		$th[]= "% Total";
	}
	else
	{
		if($CASA)
		{
			$th[]= "CA".$CASA;
			$th[]= "SA".$CASA;
		}
		else
		{
			$th[] = "1st Com"; 
			$th[] = "2nd Com";	
		}
		$th[] = $ComStr;
	}
	
	if($PSLevel=="S" || $SubjectID)
		$th[]= "Grade";
	
	$tableWidth = sizeof($th);
	
	$x .= "<tr><td colspan=\"". $tableWidth ."\" class=\"GMS_header\">". $Title ."</td></tr>";
	$x .= "<tr><td colspan=\"". $tableWidth ."\" class=\"GMS_header2\">School Year: ". getCurrentAcademicYear() ." &nbsp; &nbsp; Level: ". $lclass->getLevelName($ClassLevelID) ."</td></tr>";
	
	$x .= "<tr>";
	$ki = 0;
	foreach($th as $k=>$d)
	{
		$pars = $ki==1 ? "width=\"100%\" align=\"left\"" : "align=\"center\"";
		$x .= "<td class=\"GMS_text16bi tableline\" valign=\"bottom\" $pars><b>".$d."</b></td>";
		$ki++;
	}
	$x .= "</tr>";
	
	if ($i==$numOfSubject-1)
		$thisStyle = '';
	else
		$thisStyle = "style=\"page-break-after:always\""; 
	
	$ReportTable .= "<table valign=\"top\" width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"4\" $thisStyle>\n";
		$ReportTable .= $x;
	    $ReportTable .= $display;
	$ReportTable .= "</table>";
}
		

$eRtemplateCSS = $lreportcard->getTemplateCSS();
?>
<? if($eRtemplateCSS) {?><link href="<?=$eRtemplateCSS?>" rel="stylesheet" type="text/css" /><? } ?>

<table border="0" cellpadding="4" width="100%" cellspacing="0" class="print_hide">
<tr><td align="right"><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></span></td></tr>
</table>			
										
<!-- Start Main Table //-->
<table width="100%" border="0" cellspacing="0" cellpadding="5">
  <tr> 
    <td align="center" colspan="2"><table width="100%" border="0" cellspacing="0" cellpadding="2">
      <tr>
        <td align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td valign="top" >
            	<?=$ReportTable?>
            </td>
          </tr>
          <tr><td height="1" class="tabletext tableline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
          <tr><td class="GMS_text14bi">&nbsp;<?=$LastGenerated?></td></tr>
        </table>
          </td>
      </tr>
    </table>
    </td>
  </tr>
</table>


<!-- End Main Table //-->

<table border="0" cellpadding="4" width="100%" cellspacing="0" class="print_hide">
<tr><td align="right"><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></span></td></tr>
</table>
                        
<?
intranet_closedb();
?>

