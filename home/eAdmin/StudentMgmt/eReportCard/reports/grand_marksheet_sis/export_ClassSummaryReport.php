<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008w.php");
//include_once($PATH_WRT_ROOT."includes/libreportcard2008_sis.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/sis.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

//$lreportcard = new libreportcardSIS();
$lreportcard = new libreportcardcustom();
$lclass = new libclass();

# Retrieve params
$ReportColumnID	= $ReportColumnID ? $ReportColumnID 	: "0";
$SubjectID		= $SubjectID 	? $SubjectID 	: "";
$ClassLevelID	= $ClassLevelID	? $ClassLevelID : "";
$thisClassID	= $ClassID	? $ClassID : "";
$ReportID		= $ReportID		? $ReportID : "";
$ClassLevel 	= $lreportcard->returnClassLevel($ClassLevelID);
$PSLevel		= substr($ClassLevel, 0, 1);
$Level			= substr($ClassLevel, 1, 1);
$ReportSetting 	= $lreportcard->returnReportTemplateBasicInfo($ReportID);
$SemID 			= $ReportSetting['Semester'];
$ReportType 	= $SemID == "F" ? "W" : "T";

# Marginal Range
$MarginalRangeArr = $lreportcard->getMarginalRange($ClassLevelID);
$marginalUpperLimit = $MarginalRangeArr['upperLimit'];
$marginalLowerLimit = $MarginalRangeArr['lowerLimit'];

$StudentAry = array();
$ListAry = array();
$DataTemp = array();
$SubjectTotal = array();

$ExportArr = array();
$lexport = new libexporttext();
$LastGenerated = $lreportcard->GET_CURRENT_DATETIME();

$SpecialCaseArr = $lreportcard->GET_POSSIBLE_MARKSHEET_SPECIAL_CASE_SET1();

if(!$ClassLevelID || !$ReportID)
{
	intranet_closedb();
	header("Location: index.php");
}

# Subject Array
$SubjectAryTmp = $lreportcard->returnSubjectwOrder($ClassLevelID);
$SubjectAry = array();
$SubjectWeightAry = array();
foreach($SubjectAryTmp as $s => $temp)
{
	$thisSubjectFormGradingSettings = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $s);
	$thisScaleDisplay = $thisSubjectFormGradingSettings[ScaleDisplay];
	
	# check Subject wegith is 0 or not
	$thisWeightAry = $lreportcard->returnReportTemplateSubjectWeightData($ReportID, "SubjectID=$s and ReportColumnID is NULL");
	$thisWeight = $thisWeightAry[0]['Weight'];
	
	if ($SubjectType == "exam")
	{
		$SubjectWeightAry[$s] = $thisWeight;
		
		if($thisScaleDisplay=="M" && $thisWeight)
			$SubjectAry[] = $s;
	}
	else
	{
		if($thisScaleDisplay=="G" && $thisWeight)
			$SubjectAry[] = $s;
	}
	
}

# Grand MS  Title
if ($SubjectType == "exam")
{
	$SubjectStr ="Exam Subjects";	
}
else
{
	$SubjectStr ="Non-Exam Subjects";	
}

$SemNameAry = $lreportcard->returnReportColoumnTitle($ReportID);
if($PSLevel=="P")
{
	if($Level==1 || $Level==2)
		$SemStr = "Overall";
	else
	{
		if($ReportType=="T")
			$SemStr = "SA1";
		else
			$SemStr = $ReportColumnID ? "SA2 " : "Overall";
	}	
}
else
{
	$ReportTypeArr = $lreportcard->GET_REPORT_TYPES($ClassLevelID);
	$ReportIDList = array();
	foreach($ReportTypeArr as $k=>$d)
		$ReportIDList[] = $d['ReportID'];
	sort($ReportIDList);
	$ReportIDOrder = array_search($ReportID, $ReportIDList);
	switch($ReportIDOrder)
	{
		case 0:
			$CASA = 1;
			$SemStr = "1st Combined";
			break;
		case 1:
			$CASA = 2;
			$SemStr = "2nd Combined";
			break;
		case 2:
			$CASA = "";
			$SemStr = "Whole Year";
			break;	
	}
	
	$ColumnData = $lreportcard->returnReportTemplateColumnData($ReportID);
	$PColID = array();
	foreach($ColumnData as $k => $d)
	{
		$PColID[] = $d['ReportColumnID'];
	}	
}

# build file name
$Title = "Class Summary Report ". $SemStr ." ". $SubjectStr ." (". ($PSLevel=="P" ? "Primary" : "Secondary").")";
$thisClassName = $lclass->getClassName($thisClassID);
if($thisClassID)
{
	# class name
	$thisClassNameDisplay = $thisClassName;
}
else
{
	# level name
	$thisClassNameDisplay = $ClassLevel;
}
$thisAcadermicYear = getCurrentAcademicYear();
$filename = intranet_undo_htmlspecialchars($thisAcadermicYear."_".$thisClassNameDisplay."_".$Title.".csv");

# define column title
$exportColumn = array();
$exportColumn[]= $Title;
$numEmptyTitle = (sizeof($SubjectAry) * 5) + 5;
for ($i=0; $i<$numEmptyTitle; $i++)
{
	$exportColumn[]= "";
}

# define where condition
$where = array();
if($ReportColumnID && $PSLevel=="P")	$where[] = "ReportColumnID='$ReportColumnID'";
//if($SubjectID)							$where[] = "SubjectID='$SubjectID'";
$cons = !empty($where) ? implode(" and ", $where) : "";
$cons = $cons ? " and ".$cons : "";


# Retrieve Student Array
$classArr = $lreportcard->GET_CLASSES_BY_FORM($ClassLevelID, $thisClassID);
foreach($classArr as $key => $data)
{
	$thisClassID = $data['ClassID'];
	$thisClassName = $lclass->getClassName($thisClassID);
	$StudentAryTemp = $lreportcard->GET_STUDENT_BY_CLASS($thisClassID);
	$StudentAry = array();
	$DataTemp = array();
	$ListAry = array();
	$SubjectTotal = array();
	$SemTotal = array();
	
	foreach($StudentAryTemp as $k=>$d)
	{
		list($thisUserID, $WebSAMS, $ClassNumber, $EngName) = $d;
		$StudentAry[] = $thisUserID;
		
		$lu = new libuser($thisUserID);
		$DataTemp[$thisUserID]['StudentName'] = $lu->EnglishName ." (". $lu->ChineseName .")";
		$DataTemp[$thisUserID]['ClassName'] = $lu->ClassName;
//		$DataTemp[$thisUserID]['AdmissionNo'] = $lu->AdmissionNo;
		$DataTemp[$thisUserID]['AdmissionNo'] = strtoupper($lu->UserLogin);
		$DataTemp[$thisUserID]['ClassNumber'] = $lu->ClassNumber;
	}
	
	foreach($StudentAry as $key => $sid)
	{
		$m = array();
		
		if($PSLevel=="P")		# Primary
		{
			$result = $lreportcard->getMarks($ReportID, $sid, $cons);
			
			foreach($result as $subjid => $data)
			{
				$SubjectFormGradingSettings = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $subjid);
				$SchemeID = $SubjectFormGradingSettings[SchemeID];
				
				$thisTargetMark = $lreportcard->Get_Raw_Or_Rounded_Mark($data[$ReportColumnID]['RawMark'], $data[$ReportColumnID]['Mark']);
				
				$thisMarkRaw = my_round($thisTargetMark,1);
				$thisMark = my_round($data[$ReportColumnID]['Mark'],1);
				list($thisMarkRaw, $nsp) = $lreportcard->checkSpCase($ReportID, $subjid, $thisMarkRaw, $data[$ReportColumnID]['Grade']);
				
				$columnWeightConds = " ReportColumnID = '$ReportColumnID' AND SubjectID = '$subjid' " ;
				$columnSubjectWeightArr = $lreportcard->returnReportTemplateSubjectWeightData($ReportID, $columnWeightConds);
				$columnSubjectWeightTemp = $columnSubjectWeightArr[0]['Weight'];
				if ($columnSubjectWeightTemp == 0 && $ReportColumnID != 0)
				{
					$showEmpty = true;
				}
				else
				{
					$showEmpty = false;
				}
				
				if ($showEmpty)
				{
					$DataTemp[$sid][$subjid]['Mark'] = "";
					$DataTemp[$sid][$subjid]['MarkRaw'] = "";
					$DataTemp[$sid][$subjid]['Grade'] = "";
				}
				else
				{
					$DataTemp[$sid][$subjid]['Mark'] = $thisMark;
					$DataTemp[$sid][$subjid]['MarkRaw'] = $thisMarkRaw;
					$DataTemp[$sid][$subjid]['Grade'] = $data[$ReportColumnID]['Grade'];
				}
				
				if($nsp)
				{
					//$m[$ReportColumnID][$subjid] = $data[$ReportColumnID]['Mark'];
					$m[$ReportColumnID][$subjid] = $thisMarkRaw;
					//$SubjectTotal[$subjid]['Mark'] += $data[$ReportColumnID]['Mark'];
					$SubjectTotal[$subjid]['Mark'] += $thisMark;
					$SubjectTotal[$subjid]['MarkRaw'] += $thisMarkRaw;
					$SubjectTotal[$subjid]['no']++;
				}
				else
				{
					if($DataTemp[$sid][$subjid]['Grade']!="/" && $DataTemp[$sid][$subjid]['Grade']!="N.A.")
						$SubjectTotal[$subjid]['no']++;
					$m[$ReportColumnID][$subjid] = $DataTemp[$sid][$subjid]['Grade']=="/" ? "/" : 0;
				}
				$thisBand = $lreportcard->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($SchemeID, $thisMark);
				$DataTemp[$sid][$subjid]['Band'] = $thisBand;
				
			}
			
			if ($SubjectType == "exam")
			{
				# retrieve Grand Total / Avg
				if(!$ReportColumnID)
				{
					$t = $lreportcard->getReportResultScore($ReportID, $ReportColumnID, $sid);
					$SemTotal[$sid]['GrandTotal'] = my_round($t['GrandTotal'], 1);	
					
					$thisTargetMark = $lreportcard->Get_Raw_Or_Rounded_Mark($t['RawGrandAverage'], $t['GrandAverage']);
					$SemTotal[$sid]['GrandAverage'] = my_round($thisTargetMark, 1);	
				}
				else
				{
					$t = $lreportcard->CALCULATE_COLUMN_MARK($ReportID, $ClassLevelID, $sid, $m[$ReportColumnID]);
					$SemTotal[$sid]['GrandTotal'] = my_round($t['GrandTotal'], 1);	
					$SemTotal[$sid]['GrandAverage'] = my_round($t['GrandAverage'], 1);	
				}
			}
		}
		else					# Secondary
		{
			if($CASA)
			{
				$result = $lreportcard->getMarks($ReportID, $sid, $cons);
				# commented by Ivan on 11 Feb 2009 - wrong initialization - $SubjectTotal[$subjid]['no'] will always be 1
				//$SubjectTotal = array();
				foreach($result as $subjid => $data)
				{
					if(in_array($subjid, $SubjectAry)===false)	continue;
					
					$SubjectFormGradingSettings = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $subjid);
		 			$SchemeID = $SubjectFormGradingSettings[SchemeID];
		
					foreach($data as $colid => $m_ary)
					{
						$thisTargetMark = $lreportcard->Get_Raw_Or_Rounded_Mark($m_ary['RawMark'], $m_ary['Mark']);
						
						$thisMarkRaw = my_round($thisTargetMark,1);
						$thisMark = my_round($m_ary['Mark'],1);
		  				list($thisMarkRaw, $nsp) = $lreportcard->checkSpCase($ReportID, $subjid, $thisMarkRaw, $m_ary['Grade']);
		  				
		  				$columnWeightConds = " ReportColumnID = '$colid' AND SubjectID = '$subjid' " ;
						$columnSubjectWeightArr = $lreportcard->returnReportTemplateSubjectWeightData($ReportID, $columnWeightConds);
						$columnSubjectWeightTemp = $columnSubjectWeightArr[0]['Weight'];
						if ($columnSubjectWeightTemp == 0 && $colid != 0)
						{
							$showEmpty = true;
						}
						else
						{
							$showEmpty = false;
						}
						
						if ($showEmpty)
						{
							$DataTemp[$sid][$subjid][$colid]['Mark'] = "";
							$DataTemp[$sid][$subjid][$colid]['MarkRaw'] = "";
			 				$DataTemp[$sid][$subjid][$colid]['Grade'] = "";
						}
						else
						{
							$DataTemp[$sid][$subjid][$colid]['Mark'] = $thisMark;
							$DataTemp[$sid][$subjid][$colid]['MarkRaw'] = $thisMarkRaw;
			 				$DataTemp[$sid][$subjid][$colid]['Grade'] = $m_ary['Grade'];
						}
						
			 			if($nsp)
			 			{
		 		 			if($colid==0)	
		 		 			{	
			 		 			$SubjectTotal[$subjid]['Mark'] += $thisMark;
			 		 			$SubjectTotal[$subjid]['MarkRaw'] += $thisMarkRaw;
		 		 				$SubjectTotal[$subjid]['no']++;
	 		 				}
			 				$thisBand = $lreportcard->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($SchemeID, $thisMark);
			 				$DataTemp[$sid][$subjid][$colid]['Band'] = $thisBand;
		 				}
		 				else
		 				{
			 				if($colid==0)
			 				{
				 				if(!in_array($m_ary['Grade'], $SpecialCaseArr))
									$SubjectTotal[$subjid]['no']++;		
			 				}
			 				$DataTemp[$sid][$subjid][$colid]['Band'] = "";
		 				}
					}
				}
				
				if ($SubjectType == "exam")
				{
					# retrieve Grand Total / Avg
					$t = $lreportcard->getReportResultScore($ReportID, $ReportColumnID, $sid);
					$SemTotal[$sid]['GrandTotal'] = my_round($t['GrandTotal'], 1);	
					
					$thisTargetMark = $lreportcard->Get_Raw_Or_Rounded_Mark($t['RawGrandAverage'], $t['GrandAverage']);
					$SemTotal[$sid]['GrandAverage'] = my_round($thisTargetMark, 1);	
				}
			}
			else
			{
				$thisSubjectTotalWeight = 0;
				foreach($ReportIDList as $k=>$thisReportID)
				{
					$cons = "and ReportColumnID=0";
					$result = $lreportcard->getMarks($thisReportID, $sid, $cons);
					//$SubjectTotal = array();
					//$SemTotal = array();
					foreach($result as $subjid => $data)
					{
						if(in_array($subjid, $SubjectAry)===false)	continue;
						
						$SubjectFormGradingSettings = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $subjid);
			 			$SchemeID = $SubjectFormGradingSettings[SchemeID];
			
			 			foreach($data as $k1 => $m_ary)
						{
							/*
							if($k==2) 	# subject overall
								//$thisMarkRaw = number_format(ceil($m_ary['Mark']), 1);
								$thisMarkRaw = my_round($m_ary['RawMark'], 1);
							else
								$thisMarkRaw = my_round($m_ary['RawMark'],1);
							*/
							$thisTargetMark = $lreportcard->Get_Raw_Or_Rounded_Mark($m_ary['RawMark'], $m_ary['Mark']);
							$thisMarkRaw = my_round($thisTargetMark, 1);
							$thisMark = my_round($m_ary['Mark'], 1);
			  				list($thisMarkRaw, $nsp) = $lreportcard->checkSpCase($thisReportID, $subjid, $thisMarkRaw, $m_ary['Grade']);
			  				
							$columnWeightConds = " ReportColumnID = '0' AND SubjectID = '$subjid' " ;
							$columnSubjectWeightArr = $lreportcard->returnReportTemplateSubjectWeightData($thisReportID, $columnWeightConds);
							$columnSubjectWeightTemp = $columnSubjectWeightArr[0]['Weight'];
							if ($columnSubjectWeightTemp == 0 && $colid != 0)
							{
								$showEmpty = true;
							}
							else
							{
								$showEmpty = false;
							}
							
							if ($showEmpty)
							{
								$DataTemp[$sid][$subjid][$thisReportID]['Mark'] = "";
								$DataTemp[$sid][$subjid][$thisReportID]['MarkRaw'] = "";
				 				$DataTemp[$sid][$subjid][$thisReportID]['Grade'] = "";
							}
							else
							{
								$DataTemp[$sid][$subjid][$thisReportID]['Mark'] = $thisMark;
								$DataTemp[$sid][$subjid][$thisReportID]['MarkRaw'] = $thisMarkRaw;
				 				$DataTemp[$sid][$subjid][$thisReportID]['Grade'] = $m_ary['Grade'];
							}
							
				 			if($nsp)
				 			{
					 			if($k==2) 	# subject overall
					 			{	
						 			$SubjectTotal[$subjid]['Mark'] += $thisMark;
						 			$SubjectTotal[$subjid]['MarkRaw'] += $thisMarkRaw;
						 			$SubjectTotal[$subjid]['no']++;
						 			$thisSubjectTotalWeight += $SubjectWeightAry[$subjid];
						 			$SemTotal[$sid]['GrandTotal'] += $thisMarkRaw * $SubjectWeightAry[$subjid];
					 			}
				 				$thisBand = $lreportcard->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($SchemeID, $thisMark);
				 				$DataTemp[$sid][$subjid][$thisReportID]['Band'] = $thisBand;
			 				}
			 				else
			 				{
				 				if($k==2)
				 				{
					 				if(!in_array($m_ary['Grade'], $SpecialCaseArr))
					 				{
										$SubjectTotal[$subjid]['no']++;
										$thisSubjectTotalWeight += $SubjectWeightAry[$subjid];
									}
				 				}
				 				$DataTemp[$sid][$subjid][$thisReportID]['Band'] = "";
			 				}
						}
					}
				}
				
				if ($SubjectType == "exam")
				{
					//$SemTotal[$sid]['GrandTotal'] = my_round($SemTotal[$sid]['GrandTotal'],1);
					# Grand Avg
					//$SemTotal[$sid]['GrandAverage'] = my_round($SemTotal[$sid]['GrandTotal'] / sizeof($SubjectAry), 1);
					//$SemTotal[$sid]['GrandAverage'] = my_round($SemTotal[$sid]['GrandTotal'] / $thisSubjectTotalWeight, 1);
					
					# retrieve Grand Total / Avg
					$t = $lreportcard->getReportResultScore($ReportID, 0, $sid);
					$SemTotal[$sid]['GrandTotal'] = my_round($t['GrandTotal'], 0);
					
					$thisTargetMark = $lreportcard->Get_Raw_Or_Rounded_Mark($t['RawGrandAverage'], $t['GrandAverage']);
					$SemTotal[$sid]['GrandAverage'] = my_round($thisTargetMark, 1);	
				}
			}
		}
	}
	
	foreach($StudentAry as $k=>$stu)
	{
		$thisStudentID = $stu;
		$ListAry[$thisStudentID]['ClassNumber'] = $DataTemp[$thisStudentID]['ClassNumber'];
		$ListAry[$thisStudentID]['AdmissionNo'] = $DataTemp[$thisStudentID]['AdmissionNo'];
		$ListAry[$thisStudentID]['StudentName'] = $DataTemp[$thisStudentID]['StudentName'];
		
	// 	$ListAry[$thisStudentID]['average_space'] = "&nbsp;";
		
		if($PSLevel=="P")
		{
			foreach($SubjectAry as $k => $thisSubjectID)
			{
				$thisMark = $DataTemp[$thisStudentID][$thisSubjectID]['Mark'];
				$thisMarkRaw = $DataTemp[$thisStudentID][$thisSubjectID]['MarkRaw'];
				
				list($thisMarkRaw, $nsp) = $lreportcard->checkSpCase($ReportID, $thisSubjectID, $thisMarkRaw, $DataTemp[$thisStudentID][$thisSubjectID]['Grade']);
	
				$ListAry[$thisStudentID][$thisSubjectID."Mark"] = $thisMark;
				$ListAry[$thisStudentID][$thisSubjectID."MarkRaw"] = $thisMarkRaw;
				
				if ($SubjectType == "exam")
				{
					$ListAry[$thisStudentID][$thisSubjectID."Grade"] = $nsp ? $DataTemp[$thisStudentID][$thisSubjectID]['Band'] : "";
				}
				else
				{
	
					if ($thisMarkRaw == "0.0")
					{
						$ListAry[$thisStudentID][$thisSubjectID."Grade"] = $nsp ? $DataTemp[$thisStudentID][$thisSubjectID]['Grade'] : "";
					}
					else
					{
						$ListAry[$thisStudentID][$thisSubjectID."Grade"] = $nsp ? "" : $thisMarkRaw;
					}
					
				}
			}
		}
		else
		{
			foreach($SubjectAry as $k => $thisSubjectID)
			{
				if($CASA)
				{
					foreach($PColID as $k1=>$thisColID)
					{
						if ($SubjectType == "exam")
						{
							//$ListAry[$thisStudentID][$thisSubjectID.$thisColID."Mark"] = $DataTemp[$thisStudentID][$thisSubjectID][$thisColID]['Mark'];
							$ListAry[$thisStudentID][$thisSubjectID.$thisColID."MarkRaw"] = $DataTemp[$thisStudentID][$thisSubjectID][$thisColID]['MarkRaw'];
						}
						else
						{
							if ($DataTemp[$thisStudentID][$thisSubjectID][$thisColID]['Mark'] == "0.0")
							{
								$ListAry[$thisStudentID][$thisSubjectID.$thisColID."Mark"] = $DataTemp[$thisStudentID][$thisSubjectID][$thisColID]['Grade'];
							}
							else
							{
								$ListAry[$thisStudentID][$thisSubjectID.$thisColID."Mark"] = $DataTemp[$thisStudentID][$thisSubjectID][$thisColID]['Mark'];
							}
						}
					}
					
					# Combnied		
					if ($SubjectType == "exam")
					{
						$ListAry[$thisStudentID][$thisSubjectID.$thisColID."Com"] = $DataTemp[$thisStudentID][$thisSubjectID][0]['Mark'];
						$ListAry[$thisStudentID][$thisSubjectID.$thisColID."ComRaw"] = $DataTemp[$thisStudentID][$thisSubjectID][0]['MarkRaw'];
					}
					else
					{
						if ($DataTemp[$thisStudentID][$thisSubjectID][0]['Mark'] == "0.0")
						{
							$ListAry[$thisStudentID][$thisSubjectID.$thisColID."Com"] = $DataTemp[$thisStudentID][$thisSubjectID][0]['Grade'];
						}
						else
						{
							$ListAry[$thisStudentID][$thisSubjectID.$thisColID."Com"] = $DataTemp[$thisStudentID][$thisSubjectID][0]['Mark'];
						}
					}
		 			
		 			# Grade
		 			if ($SubjectType == "exam")
		 			{
		 				$ListAry[$thisStudentID][$thisSubjectID.$thisColID."Grade"] = $DataTemp[$thisStudentID][$thisSubjectID][0]['Band'];
	 				}
					
				}
				else
				{
					foreach($ReportIDList as $l=>$thisReportID)
					{
						if ($SubjectType == "exam")	
						{
							if ($l==2)
								$ListAry[$thisStudentID][$thisSubjectID.$thisReportID."Mark"] = $DataTemp[$thisStudentID][$thisSubjectID][$thisReportID]['Mark'];
							
							$ListAry[$thisStudentID][$thisSubjectID.$thisReportID."MarkRaw"] = $DataTemp[$thisStudentID][$thisSubjectID][$thisReportID]['MarkRaw'];
						}
						else
						{
							if ($DataTemp[$thisStudentID][$thisSubjectID][$thisReportID]['Mark'] == "0.0")
							{
								$ListAry[$thisStudentID][$thisSubjectID.$thisReportID."Mark"] = $DataTemp[$thisStudentID][$thisSubjectID][$thisReportID]['Grade'];
							}
							else
							{
								$ListAry[$thisStudentID][$thisSubjectID.$thisReportID."Mark"] = $DataTemp[$thisStudentID][$thisSubjectID][$thisReportID]['Mark'];
							}
						}
					}
					
					if ($SubjectType == "exam")
					{
						$ListAry[$thisStudentID][$thisSubjectID."Grade"] = $DataTemp[$thisStudentID][$thisSubjectID][$ReportIDList[2]]['Band'];
					}
				}
			}
		}
		if ($SubjectType == "exam")
		{
			$ListAry[$thisStudentID]['Total'] = $SemTotal[$thisStudentID]['GrandTotal'];
			$ListAry[$thisStudentID]['Percent'] = $SemTotal[$thisStudentID]['GrandAverage'];
			
			# Check marginal case
			if ( $marginalLowerLimit <= $ListAry[$thisStudentID]['Percent'] && $ListAry[$thisStudentID]['Percent'] <= $marginalUpperLimit )
				$ListAry[$thisStudentID]['Percent'] = "*".$ListAry[$thisStudentID]['Percent'];
		}
		
		//$result = $lreportcard->getReportResultScore($ReportID, 0, $thisStudentID);
		$result = $lreportcard->getReportResultScore($ReportID, $ReportColumnID, $thisStudentID);
		$thisClassPosition = $thisStudentID ? ($result['OrderMeritClass'] ? ($result['OrderMeritClass']==-1? "--": $result['OrderMeritClass']) : "--") : "#";
		$ListAry[$thisStudentID]['ClassPosition'] = $thisClassPosition;
	}
	
	
	# sort by Marks
	$field1 = array();
	foreach ($ListAry as $key => $row) 
	{
		$field1[$key] = $row['ClassNumber'];
	}
	array_multisort($field1, SORT_ASC, $ListAry);
	
	
	if (!isset($iCounter))
		$iCounter = 0;
	
	$jCounter = 0;
		
	# School Year, Level
	$ExportArr[$iCounter][$jCounter] = "School Year ".$thisAcadermicYear;
	$jCounter++;		
	$ExportArr[$iCounter][$jCounter] = "Level: ".$ClassLevel;
	$jCounter++;
	$iCounter++;
	
	# Class, Class Teacher
	# 20140730 IP25 returnClassTeacher should pass schoolYearID instead of IncludeSecond 
//	$ClassTeacherAry = $lclass->returnClassTeacher($lclass->getClassName($thisClassID));
	$ClassTeacherAry = $lclass->returnClassTeacher($lclass->getClassName($thisClassID),$lreportcard->schoolYearID);
		
	$CTeacher = array();
	foreach($ClassTeacherAry as $key=>$val)
	{
		$CTeacher[] = $val['CTeacher'];
	}
	$ClassTeacher = !empty($CTeacher) ? implode(", ", $CTeacher) : "--";
	$jCounter = 0;
	$ExportArr[$iCounter][$jCounter] = "Class: ".$thisClassName;
	$jCounter++;		
	$ExportArr[$iCounter][$jCounter] = "Class Teacher: ".$ClassTeacher;
	$jCounter++;
	$iCounter++;
	
	# Title Row
	$jCounter = 0;
	$ExportArr[$iCounter][$jCounter] = "Class no.";
	$jCounter++;
	$ExportArr[$iCounter][$jCounter] = "Student no.";
	$jCounter++;
	$ExportArr[$iCounter][$jCounter] = "Student Name";
	$jCounter++;
	foreach($SubjectAry as $k=>$subjid)
	{
		if($PSLevel=="P")
		{
			if ($SubjectType == "exam")
			{
				$ExportArr[$iCounter][$jCounter] = $lreportcard->GET_SUBJECT_NAME_LANG($subjid)." Marks";
				$jCounter++;
				$ExportArr[$iCounter][$jCounter] = $lreportcard->GET_SUBJECT_NAME_LANG($subjid)." Raw";
				$jCounter++;
			}
			$ExportArr[$iCounter][$jCounter] = $lreportcard->GET_SUBJECT_NAME_LANG($subjid)." Grade";
			$jCounter++;
		}
		else
		{
			if($CASA)
			{
				$ExportArr[$iCounter][$jCounter] = $lreportcard->GET_SUBJECT_NAME_LANG($subjid)." CA".$CASA;
				$jCounter++;
				$ExportArr[$iCounter][$jCounter] = $lreportcard->GET_SUBJECT_NAME_LANG($subjid)." SA".$CASA;
				$jCounter++;
				$ExportArr[$iCounter][$jCounter] = $lreportcard->GET_SUBJECT_NAME_LANG($subjid)." Com.";
				$jCounter++;
				$ExportArr[$iCounter][$jCounter] = $lreportcard->GET_SUBJECT_NAME_LANG($subjid)." Raw Com.";
				$jCounter++;
			}
			else
			{
				$ExportArr[$iCounter][$jCounter] = $lreportcard->GET_SUBJECT_NAME_LANG($subjid)." 1st Com.";
				$jCounter++;
				$ExportArr[$iCounter][$jCounter] = $lreportcard->GET_SUBJECT_NAME_LANG($subjid)." 2nd Com.";
				$jCounter++;
				$ExportArr[$iCounter][$jCounter] = $lreportcard->GET_SUBJECT_NAME_LANG($subjid)." Overall";
				$jCounter++;
				$ExportArr[$iCounter][$jCounter] = $lreportcard->GET_SUBJECT_NAME_LANG($subjid)." Raw Overall";
				$jCounter++;
			}
			if ($SubjectType=="exam")
			{
				$ExportArr[$iCounter][$jCounter] = $lreportcard->GET_SUBJECT_NAME_LANG($subjid)." Grade";
				$jCounter++;
			}
		}
	}
	if ($SubjectType == "exam")
	{
		$ExportArr[$iCounter][$jCounter] = ($PSLevel =="P" ? $SemStr." " : "") ."Total";
		$jCounter++;
		$ExportArr[$iCounter][$jCounter] = ($PSLevel =="P" ? "Total " : "") . "%";
		$jCounter++;
	}
	$ExportArr[$iCounter][$jCounter] = "Class Position";
	$jCounter++;
	
	$iCounter++;
	foreach($ListAry as $key=>$row)
	{
		$jCounter = 0;
		$jLocal = 0;
		foreach($row as $k => $d)	
		{ 
			if ($SubjectType != "exam")
			{
				if ($PSLevel == "P")
				{
					//if ( ($jLocal>2) && (($jLocal % 2)==1) ) // skip Marks column for non-exam subjects
					if ( ($jLocal>2) && ((($jLocal % 3)==0) || (($jLocal % 3)==1)) && ($jLocal!=count($row)-1))
					{
						$jLocal++;
						continue;
					}
				}
			}
			$ExportArr[$iCounter][$jCounter] = $d;
			$jCounter++;
			$jLocal++;
		}
		$iCounter++;
	}
	$jCounter = 0;
	
	# Footer
	$ExportArr[$iCounter][$jCounter] = "Total Students: ".sizeof($StudentAry);
	$jCounter++;
	
	$jCounter++;
	
	if ($SubjectType == "exam")
	{
		$ExportArr[$iCounter][$jCounter] = "Average";
		$jCounter++;
		
		$total = 0;
		foreach($SubjectAry as $k => $thisSubjectID)
		{
			# Empty Cell
			if($PSLevel != "P")
			{
				$ExportArr[$iCounter][$jCounter] = "";
				$jCounter++;
				$ExportArr[$iCounter][$jCounter] = "";
				$jCounter++;
			}
		
			# Marks
			if ($SubjectTotal[$thisSubjectID]['no'] == 0)
				$thisDisplayNum = 0;
			else
				$thisDisplayNum = my_round(($SubjectTotal[$thisSubjectID]['Mark'] / $SubjectTotal[$thisSubjectID]['no']), 1);
			$ExportArr[$iCounter][$jCounter] = $thisDisplayNum;
			$jCounter++;
			
			# Raw Marks
			if ($SubjectTotal[$thisSubjectID]['no'] == 0)
				$thisDisplayNumRaw = 0;
			else
				$thisDisplayNumRaw = my_round(($SubjectTotal[$thisSubjectID]['MarkRaw'] / $SubjectTotal[$thisSubjectID]['no']), 1);
			$ExportArr[$iCounter][$jCounter] = $thisDisplayNumRaw;
			$jCounter++;
			
			# Grade
			$ExportArr[$iCounter][$jCounter] = "";
			$jCounter++;
			
			$total += $thisDisplayNumRaw;
		}
		
		# Total
		$ExportArr[$iCounter][$jCounter] = my_round($total, 1);
		$jCounter++;
		
		# Total %
		$ExportArr[$iCounter][$jCounter] = my_round(($total / sizeof($SubjectAry)), 1);
		$jCounter++;
	}
	$iCounter++; 
	
	# Date and Time
	$jCounter = 0;
	$ExportArr[$iCounter][$jCounter] = $LastGenerated;
	$iCounter++;
	
	$ExportArr[$iCounter][$jCounter] = "";
	$iCounter++;
	
	
} // end of each classID

                        
$export_content = $lexport->GET_EXPORT_TXT($ExportArr, $exportColumn, "", "\r\n", "", 0, "11");
intranet_closedb();

// Output the file to user browser
$lexport->EXPORT_FILE($filename, $export_content);
?>

