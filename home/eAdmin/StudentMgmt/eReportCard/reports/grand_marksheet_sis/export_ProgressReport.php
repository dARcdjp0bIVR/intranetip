<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
//include_once($PATH_WRT_ROOT."includes/libreportcard2008w.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008j.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_generate_sis.php");
//include_once($PATH_WRT_ROOT."includes/libreportcard2008_sis.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/sis.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");


intranet_auth();
intranet_opendb();

//$lreportcard = new libreportcardSIS();
$lreportcard = new libreportcardcustom();
$lreportcardgenerate = new libreportcard2008_generate();
$lclass = new libclass();

$ExportArr = array();
$lexport = new libexporttext();
# Retrieve params
$ReportID		= $ReportID			? $ReportID : "";
$SubjectID		= $SubjectID 		? $SubjectID 	: "";
$ClassLevelID	= $ClassLevelID		? $ClassLevelID : "";
$ClassID		= $ClassID			? $ClassID : "";
$ClassLevel 	= $lreportcard->returnClassLevel($ClassLevelID);
$PSLevel		= substr($ClassLevel, 0, 1);
$Level			= substr($ClassLevel, 1, 1);
$ReportSetting 	= $lreportcard->returnReportTemplateBasicInfo($ReportID);
$SemID 			= $ReportSetting['Semester'];
$ReportType 	= $SemID == "F" ? "W" : "T";

$StudentAry = array();
$ListAry = array();
$DataTemp = array();
$LastGenerated = $lreportcard->GET_CURRENT_DATETIME();

if(!$ClassLevelID || !$ReportID || $ClassLevel=="P1" || $ClassLevel=="P2" || $ReportType=="T")
{
	intranet_closedb();
	header("Location: index.php");
}

# retrieve ReportColumnID
$ColID = array();
$ReportColoumnTitleTemp = $lreportcard->returnReportTemplateColumnData($ReportID);
foreach($ReportColoumnTitleTemp as $k=>$d)
{
	$ColID[] = $d['ReportColumnID'];
}
$SubjectID = current($SubjectID);
# Grand MS  Title
$SubjectStr = $SubjectID ? $lreportcard->GET_SUBJECT_NAME_LANG($SubjectID) : "Overall";
$LvStr = $ClassID ? $lclass->getClassName($ClassID) : "Same Level";
$Title = "Pupils' Progress of ". $SubjectStr ." Subject Marks In " . $LvStr;

$thisClassName = $lclass->getClassName($thisClassID);
if($thisClassID)
{
	# class name
	$thisClassNameDisplay = $thisClassName;
}
else
{
	# level name
	$thisClassNameDisplay = $lclass->getLevelName($ClassLevelID);
}
$thisAcadermicYear = getCurrentAcademicYear();
$filename = intranet_undo_htmlspecialchars($thisAcadermicYear."_".$thisClassNameDisplay."_".$Title.".csv");

# define column title
$exportColumn = array();
$exportColumn[]= $Title;
$numEmptyTitle = 4;
for ($i=0; $i<$numEmptyTitle; $i++)
{
	$exportColumn[]= "";
}
$SubjectAry = array();

# Bug fix for PMS cannot export csv correctly
if(count($SubjectID) == 1 &&$SubjectID[0] =='' ){
	//all subject
	$SubjectID = false;
}

if($SubjectID)	# 1 subject
{
	$SubjectFormGradingSettings = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID);
	$SchemeID = $SubjectFormGradingSettings[SchemeID];
	$ScaleDisplay = $SubjectFormGradingSettings[ScaleDisplay];

	$SubjectAry[] = $SubjectID;
}
else			# all subjects
{
	$SubjectAryTmp = $lreportcard->returnSubjectwOrder($ClassLevelID);

	foreach($SubjectAryTmp as $s => $temp)
	{
		$thisSubjectFormGradingSettings = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $s);
		$thisScaleDisplay = $thisSubjectFormGradingSettings[ScaleDisplay];
		
		# check Subject wegith is 0 or not
		$thisWeightAry = $lreportcard->returnReportTemplateSubjectWeightData($ReportID, "SubjectID=$s and ReportColumnID is NULL");
		$thisWeight = $thisWeightAry[0]['Weight'];
		
		if($thisScaleDisplay=="M" && $thisWeight)
			$SubjectAry[] = $s;
	}
}

$ReportIDList = array();
if($PSLevel=="S")
{
	$ReportTypeArr = $lreportcard->GET_REPORT_TYPES($ClassLevelID);
	foreach($ReportTypeArr as $k=>$d)
		$ReportIDList[] = $d['ReportID'];
	sort($ReportIDList);
}


# Retrieve Student Array
$classArr = $lreportcard->GET_CLASSES_BY_FORM($ClassLevelID, $ClassID);
foreach($classArr as $key => $data)
{
	$thisClassID = $data['ClassID'];
	$StudentAryTemp = $lreportcard->GET_STUDENT_BY_CLASS($thisClassID);
	
	$DataTemp = array();
	$StudentAry = array();
	foreach($StudentAryTemp as $k=>$d)
	{
		list($thisUserID, $WebSAMS, $ClassNumber, $EngName) = $d;
		$StudentAry[] = $thisUserID;
		$lu = new libuser($thisUserID);
		$DataTemp[$thisUserID]['StudentName'] = $lu->EnglishName ." (". $lu->ChineseName .")";
		$DataTemp[$thisUserID]['ClassName'] = $lu->ClassName;
		$DataTemp[$thisUserID]['ClassNumber'] = $lu->ClassNumber; 
	}
	
	$n = array();
	# construct data array for Calculate Total
	foreach($StudentAry as $k=>$sid)
	{
		if($PSLevel=="P")
		{
			$MarksAry = $lreportcard->getMarks($ReportID, $sid);
			$m = array();
			
			foreach($MarksAry as $sjid=>$data)
			{
				if(in_array($sjid, $SubjectAry)===false)	continue;
				
				foreach($data as $cid=>$m_ary)
				{
					$thisTargetMark = $lreportcard->Get_Raw_Or_Rounded_Mark($m_ary['RawMark'], $m_ary['Mark']);
					$thisMark = my_round($thisTargetMark,1);
					list($thisMark, $nsp) = $lreportcard->checkSpCase($ReportID, $sjid, $thisMark, $m_ary['Grade']);
					$m[$cid][$sjid] = $thisMark;
					$n[$sid][$cid] = $m_ary['Grade']=="/" ? $n[$sid][$cid] : 1;
				}
			}
				
			foreach($ColID as $k=>$cid)
			{
				$t = $lreportcardgenerate->CALCULATE_COLUMN_MARK($ReportID, $ClassLevelID, $StudentID, $m[$cid]);
				$SemTotal[$sid][$cid] = my_round($t['GrandTotal'], 1);	
			}
		}
		else		# Secondary
		{
			if($SubjectID)
			{
				foreach($ReportIDList as $k=>$thisReportID)
				{
					if($thisReportID==$ReportID)	continue;
					
					$MarksAry = $lreportcard->getMarks($thisReportID, $sid, "and ReportColumnID=0 and SubjectID=$SubjectID");
					$thisTargetMark = $lreportcard->Get_Raw_Or_Rounded_Mark($MarksAry[$SubjectID][0]['RawMark'], $MarksAry[$SubjectID][0]['Mark']);
	 				$thisMark = my_round($thisTargetMark,1);
	 				list($thisMark, $nsp) = $lreportcard->checkSpCase($thisReportID, $SubjectID, $thisMark, $MarksAry[$SubjectID][0]['Grade']);
	 				$SemTotal[$sid][$thisReportID] = $thisMark;
	 				$n[$sid][$thisReportID] = $MarksAry[$SubjectID][0]['Grade']=="/" ? $n[$sid][$thisReportID] : 1;
				}
			}
			else
			{
				foreach($ReportIDList as $k=>$thisReportID)
				{
					if($thisReportID==$ReportID)	continue;
					$t = $lreportcard->getReportResultScore($thisReportID, 0, $sid);
					$SemTotal[$sid][$thisReportID] = my_round($t['GrandTotal'], 1);	
					$n[$sid][$thisReportID] = 1;
				}
			}
		}
	}
	
	$ListAry = array();
	# construct data array
	foreach($StudentAry as $k=>$stu)
	{
		$thisStudentID = $stu;
		
		$ListAry[$thisStudentID]['StudentName'] = $DataTemp[$thisStudentID]['StudentName'];
		
		if($PSLevel=="P")
		{
			for($i=0; $i<sizeof($ColID); $i++)
			{
				if($n[$thisStudentID][$ColID[$i]])
				{
					$SemTotalTemp[$i] = $SemTotal[$thisStudentID][$ColID[$i]];
					$ListAry[$thisStudentID][$ColID[$i]] = $SemTotal[$thisStudentID][$ColID[$i]];
					$flag = 1;
				}
				else
				{
					$SemTotalTemp[$i] = "/";
					$ListAry[$thisStudentID][$ColID[$i]] = "/";
					$flag = 0;
				}
			}
		}
		else
		{
			for($i=0; $i<sizeof($ReportIDList)-1; $i++)
			{
				if($n[$thisStudentID][$ReportIDList[$i]])
				{
					$SemTotalTemp[$i] = $SemTotal[$thisStudentID][$ReportIDList[$i]];
					$ListAry[$thisStudentID][$ReportIDList[$i]] = $SemTotal[$thisStudentID][$ReportIDList[$i]];
					$flag=1;
				}
				else
				{
					$SemTotalTemp[$i] = "/";
					$ListAry[$thisStudentID][$ReportIDList[$i]] = "/";
					$flag=0;
				}
			}
		}
	
		# Difference
		$ListAry[$thisStudentID]['Difference'] = $flag ? @my_round( (($SemTotalTemp[1] - $SemTotalTemp[0])/$SemTotalTemp[0]) * 100, 1) : "-";
		$ListAry[$thisStudentID]['ClassName'] = $DataTemp[$thisStudentID]['ClassName'];
		$ListAry[$thisStudentID]['ClassNumber'] = $DataTemp[$thisStudentID]['ClassNumber'];
	}
	# sort by Marks
	$field1 = array();
	foreach ($ListAry as $key => $row) 
	{
		$field1[$key] = $row['ClassNumber'];
	}
	array_multisort($field1, SORT_ASC, $ListAry);
	
	if (!isset($iCounter))
		$iCounter = 0;
	
	$jCounter = 0;
		
	# School Year
	$ExportArr[$iCounter][$jCounter] = "School Year ".$thisAcadermicYear;
	$jCounter++;
	$iCounter++;
	
	# Title Row
	$jCounter = 0;
	$ExportArr[$iCounter][$jCounter] = "Student Name";
	$jCounter++;
	$ExportArr[$iCounter][$jCounter] = $PSLevel=="P" ? "SA1 Total" : "1st Com";
	$jCounter++;
	$ExportArr[$iCounter][$jCounter] = $PSLevel=="P" ? "SA2 Total" : "2nd Com";
	$jCounter++;
	$ExportArr[$iCounter][$jCounter] = "Difference (%)";
	$jCounter++;
	$ExportArr[$iCounter][$jCounter] = "Class Name";
	$jCounter++;
	$iCounter++;
	
	# Contents		
	foreach($ListAry as $key=>$row)
	{
		$jCounter = 0;
		
		foreach($row as $k => $d)	
		{
			if($jCounter == sizeof($row)-1)
				break;
			
			$ExportArr[$iCounter][$jCounter] = $d;
			
			$jCounter++;
		}
		
		$iCounter++;
	}
	
	# Date and Time
	$jCounter = 0;
	$ExportArr[$iCounter][$jCounter] = $LastGenerated;
	$iCounter++;
	
	$ExportArr[$iCounter][$jCounter] = "";
	$iCounter++;
	
} # End for each class


$export_content = $lexport->GET_EXPORT_TXT($ExportArr, $exportColumn, "", "\r\n", "", 0, "11");
intranet_closedb();

// Output the file to user browser
$lexport->EXPORT_FILE($filename, $export_content);
?>

