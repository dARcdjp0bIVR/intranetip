<?php
$PATH_WRT_ROOT = "../../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008w.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_sis.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."templates/2007a/layout/print_header.php");

intranet_auth();
intranet_opendb();

$lreportcard = new libreportcardSIS();
$linterface = new interface_html();

# Retrieve params
$SemID			= isset($SemID) 		? $SemID 		: "F";
$SubjectID		= isset($SubjectID) 	? $SubjectID 	: "";
$ClassLevelID	= isset($ClassLevelID)	? $ClassLevelID : "";
$ReportID		= isset($ReportID)		? $ReportID : "";

# Temp Data
$TestCase = $TestCase ? $TestCase : 1;
switch($TestCase)
{
	case 1:	
		# test �Ǵ�4 Maths By Marks in Same Level
		$ClassLevelID = 31; # P1
		$SubjectID = 278;	# Math
		$SemID = '9'; 		# �Ǵ�4
		$ReportID = 425;	# Assessment Results Second Semester 2007 / 08
		break;
	case 2:
		# test OverAll Subjects By Marks In Same Level
		$ClassLevelID = 31; # P1
		$SubjectID = "";	# all
		$SemID = "F"; 		# all
		$ReportID = 425;	# Assessment Results Second Semester 2007 / 08
		break;
	case 3:
		# test OverAll Mathematics By Marks In Same Level
		$ClassLevelID = 31; # P1
		$SubjectID = 278;	# Math
		$SemID = "F"; 		# all
		$ReportID = 425;	# Assessment Results Second Semester 2007 / 08
		break;
}

$StudentAry = array();
$ListAry = array();
$GradeAry = array();
$DataTemp = array();

if(!$ClassLevelID || !$ReportID)
{
	intranet_closedb();
	header("Location: index.php");
}

# Retrieve Student Array
$classArr = $lreportcard->GET_CLASSES_BY_FORM($ClassLevelID);
foreach($classArr as $key => $data)
{
	$thisClassID = $data['ClassID'];
	$StudentAryTemp = $lreportcard->GET_STUDENT_BY_CLASS($thisClassID);
	foreach($StudentAryTemp as $k=>$d)
	{
		list($thisUserID, $WebSAMS, $ClassNumber, $EngName) = $d;
		$StudentAry[] = $thisUserID;
	}
}

if($SubjectID)	# 1 subject
{
	$SubjectStr = $lreportcard->GET_SUBJECT_NAME_LANG($SubjectID);
	$SubjectFormGradingSettings = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID);
	$SchemeID = $SubjectFormGradingSettings[SchemeID];
	$ScaleDisplay = $SubjectFormGradingSettings[ScaleDisplay];
	
	# return and init. Grand/Band Array
	$SchemeRangeInfo = $lreportcard->GET_GRADING_SCHEME_RANGE_INFO($SchemeID);
	foreach($SchemeRangeInfo as $k => $d)
	{
		$GradeAry[$d['Grade']] = 0;
	}	
	$GradeAry['ExemptedAbsent']=0;
	
	$SubjectAry[] = $SubjectID;
	
	if($SemID=="F")		# Retrieve Sem Column
	{
		$ColoumnTitle = $lreportcard->returnReportColoumnTitle($ReportID);
		foreach($ColoumnTitle as $ColID[]=>$ColTitle[]);
		
		debug_r($ColID);
		
// 		# retrieve Total (ReportColumnID=0)
// 		foreach($StudentAry as $key => $sid)
// 		{
// 			//$ReportResultScore = $lreportcard->getMarks($ReportID, $sid);
// 			$table = $lreportcard->DBName.".RC_REPORT_RESULT_SCORE";
// 			$sql = "select Mark from $table where ReportID=$ReportID and StudentID=$sid and ReportColumnID=0 and SubjectID=$SubjectID";
// 			$result = $lreportcard->returnArray($sql);
// 			debug_r($result);
// 			$DataTemp[$sid]['Total'] = $ReportResultScore['GrandAverage'];
// 		
// 		// 		$DataTemp[$sid]['Total'] = $ReportResultScore['GrandAverage'];
// 		// 		$DataTemp[$sid]['Percent'] = number_format($ReportResultScore['GrandAverage'],0);
// 		// 		$DataTemp[$sid]['Band'] = $lreportcard->returnGrandMSBand($ClassLevelID, $DataTemp[$sid]['Total']);
// 		// 		$GradeAry[$DataTemp[$sid]['Band']]++;
// 		}
	}
}
else			# all subjects
{
	if($SemID!="F")	
	{
		echo "Invalid.";	exit;
	}
	
	
	$SubjectStr ="All Subjects";	
	
	$SubjectAryTmp = $lreportcard->returnSubjectwOrder($ClassLevelID);
	foreach($SubjectAryTmp as $s => $temp)
	{
		$thisSubjectFormGradingSettings = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $s);
		$thisScaleDisplay = $thisSubjectFormGradingSettings[ScaleDisplay];
		if($thisScaleDisplay=="M")			$SubjectAry[] = $s;
	}
	
	# return and init. Grand/Band Array
	$GradeAryTmp = $lreportcard->returnGrandMSBand($ClassLevelID, '-1');
	foreach($GradeAryTmp as $k => $d)
	{
		$GradeAry[$d] = 0;
	}	
	$GradeAry['ExemptedAbsent']=0;
	
	# retrieve GrandTotal 
	foreach($StudentAry as $key => $sid)
	{
		$ReportResultScore = $lreportcard->getReportResultScore($ReportID, 0, $sid);
		
		$DataTemp[$sid]['Total'] = $ReportResultScore['GrandAverage'];
		$DataTemp[$sid]['Percent'] = number_format($ReportResultScore['GrandAverage'],0);
		$DataTemp[$sid]['Band'] = $lreportcard->returnGrandMSBand($ClassLevelID, $DataTemp[$sid]['Total']);
		$GradeAry[$DataTemp[$sid]['Band']]++;
	}
	
	
}
//debug_r($DataTemp);

# Grand MS  Title
$SemStr = $SemID=="F" ? "OverAll" : $lreportcard->GET_ALL_SEMESTERS($SemID);
$Title = $SemStr. " ". $SubjectStr ." By Marks In Same Level";

# define where condition
if($SemID) 		$where[] = "a.Semester='$SemID'";
if($SubjectID)	$where[] = "a.SubjectID='$SubjectID'";
if($ReportID)	$where[] = "a.ReportID='$ReportID'";
if(!empty($StudentAry)) $where[] = "a.StudentID in (". implode(",", $StudentAry) .")";
$cons = implode(" and ", $where);

$table_RC_REPORT_RESULT_SCORE = $lreportcard->DBName.".RC_REPORT_RESULT_SCORE";
$NameField = getNameFieldByLang2("b.");
$sql = "
	SELECT 
		b.UserID,
		a.SubjectID, 
		a.Mark as Mark,
		a.Mark as Percent,
		a.Grade as Grade,
		$NameField as StudentName,
		b.ClassName
	FROM 
		$table_RC_REPORT_RESULT_SCORE as a
		LEFT JOIN eclassIP_dev.INTRANET_USER as b on (a.StudentID=b.UserID)
	WHERE 
		$cons
";
// debug_r($sql);
$result = $lreportcard->returnArray($sql);
// debug_r($result);

foreach($result as $k=>$data)
{
	$DataTemp[$data['UserID']]['StudentName'] = $data['StudentName'];
	$DataTemp[$data['UserID']]['ClassName'] = $data['ClassName'];
	$DataTemp[$data['UserID']][$data['SubjectID']]['Mark'] = $data['Mark'];
	$DataTemp[$data['UserID']][$data['SubjectID']]['Grade'] = $data['Grade'];
	$DataTemp[$data['UserID']][$data['SubjectID']]['Percent'] = $data['Percent'];
	
}
// debug_r($DataTemp);

foreach($StudentAry as $k=>$stu)
{
	$thisStudentID = $stu;
	
	$ListAry[$thisStudentID]['StudentName'] = $DataTemp[$thisStudentID]['StudentName'];
	
	if(!$SubjectID)	# All subjects
	{
		foreach($SubjectAry as $k => $thisSubjectID)
		{
			$thisMark = $DataTemp[$thisStudentID][$thisSubjectID]['Mark'];
			list($thisMark, $nsp) = $lreportcard->checkSpCase($ReportID, $thisSubjectID, $thisMark, $DataTemp[$thisStudentID][$thisSubjectID]['Grade']);
			
			$ListAry[$thisStudentID][$thisSubjectID] = $thisMark;
		}
		$ListAry[$thisStudentID]['Total'] = $DataTemp[$thisStudentID]['Total'];
		$ListAry[$thisStudentID]['Percent'] = $DataTemp[$thisStudentID]['Percent'];
		$ListAry[$thisStudentID]['Band'] = $DataTemp[$thisStudentID]['Band'];
	}
	else			# 1 subject
	{
		$thisMark = $DataTemp[$thisStudentID][$SubjectID]['Mark'];
		$thisBand = $lreportcard->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($SchemeID, $thisMark);
		
		list($thisMark, $nsp) = $lreportcard->checkSpCase($ReportID, $SubjectID, $thisMark, $DataTemp[$thisStudentID][$SubjectID]['Grade']);
		if($nsp) 
			$GradeAry[$thisBand]++;
		else
			$GradeAry['EA']++;
			
		if($ScaleDisplay=="M")	
		{
			$ListAry[$thisStudentID]['Total'] = $thisMark;
			if($SemID!="F")
				$ListAry[$thisStudentID]['Percent'] = $DataTemp[$thisStudentID][$SubjectID]['Percent'];
		}
		else
		{
			$ListAry[$thisStudentID]['Total'] = $DataTemp[$thisStudentID][$SubjectID]['Grade'];
			if($SemID!="F")
				$ListAry[$thisStudentID]['Percent'] = "";
		}
		$ListAry[$thisStudentID]['Band'] = $thisBand;
		if($SemID!="F")
		{
			$ListAry[$thisStudentID]['Exempted'] = "";
			$ListAry[$thisStudentID]['Absent'] = "";
		}
	}
	
	$ListAry[$thisStudentID]['ClassName'] = $DataTemp[$thisStudentID]['ClassName'];
	
}

//debug_r($ListAry);

/*
# re-struct data array
foreach($result as $k=>$data)
{
	
	$thisBand = $lreportcard->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($SchemeID, $data['Mark']);
		
	$thisMark = $data['Mark'];
	list($thisMark, $nsp) = $lreportcard->checkSpCase($ReportID, $SubjectID, $thisMark, $data['Grade']);
	if($nsp) 
		$GradeAry[$thisBand]++;
	else
		$GradeAry['EA']++;
	
		
	$ListAry[$data['UserID']]['StudentName'] = $data['StudentName'];
	
	if(!$SubjectID)
	{
		foreach($SubjectAry as $k => $thisSubjID)
		{
			$ListAry[$data['UserID']][$thisSubjID] = $thisMark;
		}
		//SubjectAry	
	}
	
	if($ScaleDisplay=="M")	
	{
		$ListAry[$data['UserID']]['Total'] = $thisMark;
		$ListAry[$data['UserID']]['Percent'] = $data['Percent'];
	}
	else
		$ListAry[$data['UserID']]['Total'] = $data['Grade'];
	$ListAry[$data['UserID']]['Band'] = $thisBand;
	$ListAry[$data['UserID']]['Exempted'] = "";
	$ListAry[$data['UserID']]['Absent'] = "";
	$ListAry[$data['UserID']]['ClassName'] = $data['ClassName'];
}
*/


# sort by Marks
foreach ($ListAry as $key => $row) 
{
	$field1[$key] = $row['Total'];
}
array_multisort($field1, SORT_DESC, $ListAry);

// debug_r($ListAry);

$display = "";
$xi = 0;
foreach($ListAry as $key=>$row)
{
	$xi++;
	$css = "tablerow" . (($xi%2)+1);
	
	$display .= "<tr>";		
	
	$display .= "<td class=\"$css tabletext\">$xi</td>";
	
	foreach($row as $k => $d)	
		$display .= "<td class=\"$css tabletext\">$d</td>";
	$display .= "</tr>";
}


# Table header
$th = array();
$th[]= "&nbsp;";
$th[]= "Student Name";
if(!$SubjectID)	# all subjects
{
	foreach($SubjectAry as $k=>$subjid)
		$th[]= $lreportcard->GET_SUBJECT_NAME_LANG($subjid);
}
else
{
	if($SemID=="F")
	{
		//$ColoumnTitle = $lreportcard->returnReportColoumnTitle($ReportID);
		foreach($ColTitle as $k=>$CTitle)
			$th[]= $CTitle;
	}
}
$th[]= "Total";
if(!($SubjectID && $SemID=="F"))
	$th[]= "Percent";
$th[]= "Grade/Band";
if($SubjectID && !$SemID=="F")
{
	$th[]= "Exempted";
	$th[]= "Absent";
}
$th[]= "Class Name";

$x = "<tr><td colspan=\"". sizeof($th) ."\" class=\"tabletext tableline\">School Year: ". getCurrentAcademicYear() ."</td></tr>";
$x .= "<tr>";
foreach($th as $k=>$d)
{
	$x .= "<td class=\"tabletext tableline\"><b>".$d."</b></td>";
}
$x .= "</tr>";


# summary table
//debug_r($GradeAry);
$summary = "<table border=\"01\" cellspacing=\"0\">";
$summary .= "<tr>
		<td class=\"tabletext\" align=\"right\" width=\"150\"><b>Band</b></td>
		<td class=\"tabletext\" align=\"right\" width=\"50\"><b>Pupils</b></td>
		<td class=\"tabletext\" align=\"right\" width=\"50\"><b>%</b></td>
		</tr>
";
foreach($GradeAry as $g=>$d)
{
	if($g=="ExemptedAbsent") $g = "Exempted or Absent";
	
	$summary .= "<tr>
		<td class=\"tabletext\" align=\"right\"><b>$g</b></td>
		<td class=\"tabletext\" align=\"right\">$d</td>
		<td class=\"tabletext\" align=\"right\">". number_format(($d / sizeof($StudentAry))*100, 1) ."</td>
		</tr>
	";
}
$summary .= "<tr><td class=\"tabletext\" align=\"right\">&nbsp;<br><br></td><td class=\"tabletext\" align=\"right\">&nbsp;</td><td class=\"tabletext\" align=\"right\">&nbsp;</td></tr>";	
// $summaryTotal += $GradeAry['EA'];
$summary .= "<tr>
		<td class=\"tabletext\" align=\"right\"><b>Total</b></td>
		<td class=\"tabletext\" align=\"right\">". sizeof($StudentAry) ."</td>
		<td class=\"tabletext\" align=\"right\">100%</td>
		</tr>
	";
$summary .= "</table>";

?>

<style type='text/css'>
.tableline {
	border-bottom-width: 2px;
	border-bottom-style: solid;
	border-bottom-color: #000000;
}
</style>

<table border="0" cellpadding="4" width="100%" cellspacing="0" class="print_hide">
<tr><td align="right"><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></span></td></tr>
</table>			
										
<!-- Start Main Table //-->
<table width="100%" border="0" cellspacing="0" cellpadding="5">
  <tr> 
    <td align="center" colspan="2"><table width="100%" border="0" cellspacing="0" cellpadding="2">
      <tr>
        <td align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td align="center" class="tabletext"><h2><?=$Title?></h2></td>
          </tr>
          <tr>
            <td>
            	
            	<table width="100%" border="0" cellspacing="0" cellpadding="4">
				<?=$x?>
                <?=$display?>
                </table>
            </td>
          </tr>
          <tr><td height="1" class="tabletext tableline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
			<tr>
				<td align="right">
				<?=$summary?>
				</td>
			</tr>          
          <tr><td height="1" class="tabletext tableline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
          <tr><td class="tabletext">&nbsp;<?=date("l, F d, Y");?></td></tr>
        </table>
          </td>
      </tr>
    </table>
    </td>
  </tr>
</table>

<!-- End Main Table //-->

<table border="0" cellpadding="4" width="100%" cellspacing="0" class="print_hide">
<tr><td align="right"><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></span></td></tr>
</table>
                        
<?
debug_r($ListAry);

intranet_closedb();
?>
