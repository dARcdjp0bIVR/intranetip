<?
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008w.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/sis.php");

intranet_auth();
intranet_opendb();

$lreportcard = new libreportcardcustom();

$ClassLevelID		= $_POST['ClassLevelID'];
$ClassID			= $_POST['ClassID'];
$ReportID			= $_POST['ReportID'];
$SubjectID			= $_POST['SubjectID'];
$ReportColumnID		= $_POST['ReportColumnID'];
$GrandMarksheetType	= $_POST['GrandMarksheetType'];
$parms = "ReportID=".$ReportID."&ClassLevelID=".$ClassLevelID."&ClassID=".$ClassID."&ReportColumnID=".$ReportColumnID."&SubjectType=".$SubjectType;

for($i=0; $i<sizeof($SubjectID); $i++)
{
	$parms .= "&SubjectID[]=".$SubjectID[$i];
}

switch($GrandMarksheetType)
{
	case 0:			# Class Summary Report
		if ($ViewFormat == "html")
		{
			$url = "ClassSummaryReport.php";
		}
		else
		{
			$url = "export_ClassSummaryReport.php";
		}
		break;
	
	case 1:			# Level Ranking
		if ($ViewFormat == "html")
		{
			$url = "LevelRankingReport.php";
		}
		else
		{
			$url = "export_LevelRankingReport.php";
		}
		break;

	case 2:			# Progress
		if ($ViewFormat == "html")
		{
			$url = "ProgressReport.php";
		}
		else
		{
			$url = "export_ProgressReport.php";
		}
		break;		
	case 3:			# Level Ranking (for Promotion)
		if ($ViewFormat == "html")
		{
			$url = "LevelRankingPromotionReport.php";
		}
		else
		{
			$url = "export_LevelRankingPromotionReport.php";
		}
		break;
}

$report_path = $url ."?".$parms;
header("Location: $report_path");

?>
