<?php
// Using: 

/*****************************************
 *  Modification log
 * 	20170125 Bill:
 * 		- Add log when generate reportcard
 * 	20140801 Ryan  :
 * 		- Merged SIS Cust
 * 	20110209 Marcus:
 * 		- set memory_limit to no limit, cannot generate report with heavy data, e.g. Shatin Methodist. 
 * ***************************************/

@SET_TIME_LIMIT(21600);
@ini_set(memory_limit, -1);

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
if($ReportCardCustomSchoolName == 'sis'){
	include_once($PATH_WRT_ROOT."includes/libreportcard2008j.php");
	include_once($PATH_WRT_ROOT."includes/libreportcard2008_generate_sis.php");
}
else
	include_once($PATH_WRT_ROOT."includes/libreportcard2008_generate.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include($PATH_WRT_ROOT."includes/eRCConfig.php");

intranet_auth();
intranet_opendb();

StartTimer('reportGeneration');
$startGenerateTime = date("Y-m-d H:i:s");

/*
if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
{
	include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
	$lreportcard = new libreportcardcustom();
} else {
	$lreportcard = new libreportcard();
}
*/

# Retrieve Report Type
$lreportcard = new libreportcard2008_generate();
$ReportSetting = $lreportcard->returnReportTemplateBasicInfo($ReportID);
$SemID = $ReportSetting['Semester'];
$ClassLevelID = $ReportSetting['ClassLevelID'];
$ReportType = $SemID == "F" ? "W" : "T";

$errArr = array();
#$errArr = $lreportcard->checkCanGenernateReport($ReportID);
if (empty($ReportID) || !empty($errArr) || sizeof($errArr) != 0) {
	intranet_closedb();
	header("Location: index.php");
}

## Delete Archived Report and Manual Adjustment
if ($eRCTemplateSetting['Report']['ReportGeneration']['DoNotRemoveManualAdjustment']) {
	// do not remove manual adjustment
}
else {
	$successArr['DeleteArchiveReport'] = $lreportcard->Delete_Manual_Adjustment($ReportID);
}

//$successArr['DeleteManualAdjustment'] = $lreportcard->Delete_Archived_Report($ReportID);

if($ReportType=="T")			# Generate Term Report
	$lreportcard->GENERATE_TERM_REPORTCARD($ReportID, $ClassLevelID);
else							# Generate Whole Year Report
	$lreportcard->GENERATE_WHOLE_YEAR_REPORTCARD($ReportID, $ClassLevelID);

$lreportcard->UPDATE_REPORT_LAST_DATE($ReportID, 'LastGenerated', $isReset=0, 'LastGeneratedBy');

$runTime = StopTimer($precision=5, $NoNumFormat=false, 'reportGeneration');
$EndGenerateTime = date("Y-m-d H:i:s");

$LogContent = "ReportID = $ReportID|||StartGeneration = $startGenerateTime|||EndGeneration = $EndGenerateTime|||Timespent = $reportGeneration|||GeneratedBy = ".$UserID;
$lreportcard->Insert_Year_Based_Log("Generate_ReportCard", $LogContent);

// For performance tunning info
//echo '<script language="JavaScript" src="/templates/jquery/jquery-1.3.2.min.js"></script>';
//debug_pr('$runTime = '.$runTime.'s');
//debug_pr('convert_size(memory_get_usage()) = '.convert_size(memory_get_usage()));
//debug_pr('convert_size(memory_get_usage(true)) = '.convert_size(memory_get_usage(true)));
//debug_pr('Query Count = '.$GLOBALS[debug][db_query_count]);
////$lreportcard->db_show_debug_log();
//$lreportcard->db_show_debug_log_by_query_number(1);
//die();

intranet_closedb();

$params = ((isset($Semester)) ? "Semester=$Semester&" : "")."msg=report_generate";
header("Location: index.php?ClassLevelID=$ClassLevelID&$params");

?>