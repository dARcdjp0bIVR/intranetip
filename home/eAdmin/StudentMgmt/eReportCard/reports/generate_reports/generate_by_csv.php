<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if ($plugin['ReportCard2008']) {
	include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
	include_once($PATH_WRT_ROOT."includes/reportcard_custom/sis.php");
	$lreportcard = new libreportcardcustom();
	
	if (!$lreportcard->hasAccessRight()) 
	{
		header ("Location: /");
		intranet_closedb();
		exit();
	}
	
	$CurrentPage = "Reports_GenerateReport";
	$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
	
	//$SampleCSV = "generate_reportcard_sample.csv";
	
	if ($lreportcard->hasAccessRight())
    {
         $linterface = new interface_html();

################################################################
		
		# generate table
		$UploadTableRow = "";
		$UploadTableRow .= "<tr>
								<td valgin='top' class='tabletext formfieldtitle' width='30%'>".$eReportCard['File']."</td>
								<td valgin='top'><input class=\"textboxtext\" type='file' name='userfile' size=\"60\">
							";
		
		if($g_encoding_unicode) {
			$UploadTableRow .= "<span class='tabletextremark'>$i_import_utf_type</span>";
		}
		
		$UploadTableRow .= "</tr>";
		$UploadTableRow .= "<tr>
								<td nowrap='nowrap' colspan='2'>
									<a target='_blank' href='".GET_CSV("generate_reportcard_sample.csv")."' class='tablelink'>
										<img src='".$PATH_WRT_ROOT."/images/2007a/icon_files/xls.gif' border='0' align='absmiddle' hspace='3'>".
										$i_general_clickheredownloadsample."
									</a>
								</td>
							</tr>";
		
		$ButtonHTML = $linterface->GET_ACTION_BTN($button_submit, "submit", "", "btnSubmit");
		$ButtonHTML .= "&nbsp;".$linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:history.back()");

################################################################
		${"link".$UploadType} = 1;

		$otherInfoTypes = $lreportcard->getOtherInfoType();

		# page navigation (leave the array empty if no need)
		$PAGE_NAVIGATION[] = array($eReportCard['GenerateReportFromCSV'], "");
		
		# tag information
		$TAGS_OBJ[] = array($eReportCard['Reports_GenerateReport'], "", 0);
		
		$linterface->LAYOUT_START();
		
		if (isset($WrongRow) && $WrongRow != "") {
			$WrongRowArr = explode(" ", $WrongRow);
			$WrongRowMsg = implode(", ", $WrongRowArr);
			$SysMsg = sprintf($i_con_msg_wrong_row, $WrongRowMsg);
			$SysMsg = $linterface->GET_SYS_MSG("", $SysMsg);
		} else {
			$SysMsg = $linterface->GET_SYS_MSG($Result);
		}
?>
<script language="javascript">
<!--
function trim(str) {
	return str.replace(/^\s+|\s+$/g,"");
}


function checkForm() {
	obj = document.form1;
	if (trim(obj.elements["userfile"].value) == "") {
		 alert('<?=$eReportCard['AlertSelectFile']?>');
		 obj.elements["userfile"].focus();
		 return false;
	}
}
//-->
</script>
<?=$js_reload?>
<form name="form1" enctype="multipart/form-data" action="print_preview_by_csv.php" method="post" onsubmit="return checkForm()" target='_blank'>
<br />
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
</tr>
<tr><td align="right" colspan="<?=$col_size?>"><?= $SysMsg ?></td></tr>
<tr>
	<td>
		<table width="90%" border="0" cellpadding="4" cellspacing="1" align="center">
			<?=$UploadTableRow?>
		</table>
	</td>
</tr>
<tr>
	<td><table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
		<tr>
			<td align="center"><?=$ButtonHTML?></td>
		</tr>
	</table>
	</td>
</tr>
</table>
</form>
<?
        $linterface->LAYOUT_STOP();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>
