<?php
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

$PageRight = "ADMIN";

if (!isset($_POST) && (!isset($promotionStatus) || !isset($newClassName)))
	header("Location:index.php");

if (!$plugin['ReportCard2008']) {
	intranet_closedb();
	header("Location:/index.php");
}

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
$lreportcard = new libreportcard();

if (!$lreportcard->hasAccessRight()) {
	intranet_closedb();
	header("Location:/index.php");
}

$success = true;
//if (sizeof($promotionStatus) > 0 && sizeof($newClassName) > 0) {
	foreach ((array)$promotionStatus as $key => $value) {
		$updateArr[$key]["Promotion"] = $value;
	}
	
	foreach ((array)$newClassName as $key => $value) {
		$updateArr[$key]["NewClassName"] = $value;
	}

	$table = $lreportcard->DBName.".RC_REPORT_RESULT";
	foreach ((array)$updateArr as $key => $value) {
		$sql  = "UPDATE $table SET ";
		$sql .= "Promotion = '".$value["Promotion"]."', NewClassName = '".$value["NewClassName"]."', AdjustedBy = '".$_SESSION["UserID"]."', DateModified = NOW() ";
		$sql .= "WHERE ResultID = '$key'";
		$success = $lreportcard->db_db_query($sql);
	}
//}

intranet_closedb();

$Result = ($success) ? "update" : "update_failed";
$parmeters = "ClassID=$ClassID&ReportID=$ReportID&ClassLevelID=$ClassLevelID";
header("Location:promotion_edit.php?$parmeters&Result=$Result");
?>

