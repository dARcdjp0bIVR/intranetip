<?
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");

intranet_opendb();

$lreportcard = new libreportcard();

#Subject Score
#$id : Score_$ReportColumnID_$SubjectID

#Grand Score 
#$id : $GrandMarkTitle_$ReportColumnID
$newValue = trim($value);

$postData = explode("_",$id);
$OtherInfoName = $postData[0];
$ReportColumnID = $postData[1];
$SubjectID = $postData[2];	
$oriValue = $postData[3];

#Get Manual Adjust List
$ManualAdjustedArr = $lreportcard->Get_Manual_Adjustment($ReportID, $StudentID, $ReportColumnID, $SubjectID,$OtherInfoName);

###### INSERT, DELETE, UPDATE RC_MANUAL_ADJUSTMENT ######
	### Init Table
	$table = $lreportcard->DBName.".RC_MANUAL_ADJUSTMENT";

if(isset($ManualAdjustedArr[$StudentID][$ReportColumnID][$SubjectID][$OtherInfoName]))
{
	if(trim($newValue)=='') //do nothing
	{
		echo $ManualAdjustedArr[$StudentID][$ReportColumnID][$SubjectID][$OtherInfoName]."<span style='color:#aaa'> ($oriValue)</span>"."|=msg=|0";
		exit;
	}
		
	
	if($newValue == $oriValue && $newValue!='') //delete record if adjusted mark == original mark
	{
		### DELETE ###
		$sql = "
			DELETE FROM
				$table
			WHERE
				ReportID = '$ReportID'
				AND StudentID = '$StudentID' 
				AND ReportColumnId = '$ReportColumnID'
				AND SubjectID = '$SubjectID' 
				AND OtherInfoName = '$OtherInfoName'
		";		
		
		$isOri = true;
	}
	else  //update record if adjusted mark != original mark
	{
		### UPDATE ###
		$sql = "	
			UPDATE
				$table
			SET 
				AdjustedValue = '".htmlspecialchars(trim($newValue))."',
				LastModified = NOW(),
				LastModifiedBy = '$UserID'					
			WHERE
				ReportID = '$ReportID'
				AND ReportColumnID = '$ReportColumnID'
				AND StudentID = '$StudentID'
				AND SubjectID = '$SubjectID' 
				AND OtherInfoName = '$OtherInfoName'
		";
	}
	if(!$lreportcard->db_db_query($sql))
	{
		echo $oriValue."|=msg=|0";
		exit;
	}
}
else if($newValue != $oriValue )//insert new record if the mark was have not been modified 
{
	if($newValue=='') //do nothing
	{
		echo $oriValue."|=msg=|0";
		exit;
	}
	
	### INSERT ###
	$sql = "	
		INSERT INTO 
			$table
			(
				`ReportID`,
				`ReportColumnID`,
				`StudentID`,
				`SubjectID`,
				`OtherInfoName`,
				`AdjustedValue`,
				`DateInput`,
				`LastModified`,
				`LastModifiedBy`
			)
			VALUES
			(
				'$ReportID',
				'$ReportColumnID',
				'$StudentID',
				'$SubjectID',
				'$OtherInfoName',
				'".htmlspecialchars(trim($newValue))."',
				NOW(),
				NOW(),
				'$UserID'
			)
	";

	if(!$lreportcard->db_db_query($sql))
	{
		echo $oriValue."|=msg=|0";
		exit;
	}		
	
}
else // no existing record , new value == original value, do nothing
{
	$isOri=true;
}
	
	echo $isOri?$oriValue:stripslashes(htmlspecialchars(trim($newValue)))."<span style='color:#aaa'> ($oriValue)</span>";

?>