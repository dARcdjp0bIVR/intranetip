<?php
// Using: 

####################################################
#
#   Date:   2019-07-11  Bill    [2019-0711-1055-59207]
#           - add auth checking
#           - add token to curl request
#
#	Date:	2017-02-17	Bill
#			- for batch export student reportcard	($eRCTemplateSetting['Report']['BatchExportReport'])
#			- copy logic from existing function : Archive (To Digital Archive) 
#
####################################################

@SET_TIME_LIMIT(21600);
@ini_set("memory_limit", -1);
@ini_set("zend.ze1_compatibility_mode", '0');

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

// Access Right checking
if (!$plugin['ReportCard2008']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

// Initial object
if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") {
	# load specific layout for teacher view of munsang college (primary) report
	if ($ReportCardCustomSchoolName == "munsang_pri" && $viewType == "teacher") {
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName."_teacher.php");
	}
	else {
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
	}
	$lreportcard = new libreportcardcustom();
}
else {
	$lreportcard = new libreportcard();
}

// Access Right checking
$isFromExport = $_POST["isExport"];
if (!$lreportcard->hasAccessRight() || !$eRCTemplateSetting['Report']['BatchExportReport'] || !$isFromExport) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

// Get Report Info
$targetReportID = $_POST["ReportID"];
$thisReportInfo = $lreportcard->returnReportTemplateBasicInfo($targetReportID);
$thisReportTitle = $thisReportInfo["ReportTitle"];
$thisReportTitle = str_replace(":_:", " ", $thisReportTitle);
$thisClassLevelID = $thisReportInfo["ClassLevelID"];

// Get Related Classes
$targetClassID = $_POST["ClassID"];
if(empty($targetClassID)) {
	// Get Class
	$targetClassID = array();
	$ClassArr = $lreportcard->GET_CLASSES_BY_FORM($thisClassLevelID);
	if(count($ClassArr) > 0 ){
		for($i=0 ; $i<count($ClassArr); $i++){
			$targetClassID[] = $ClassArr[$i][0];
		}
	}
}

// Get Target Students
$targetStudentIDAry = $_POST["TargetStudentID"];
$targetStudentIDAry = implode(",", (array)$targetStudentIDAry);
$studentInfoAry = $lreportcard->GET_STUDENT_BY_CLASS($targetClassID, $targetStudentIDAry);
$numOfStudent = count((array)$studentInfoAry);
if($targetReportID > 0 && $numOfStudent > 0)
{
	// Initial object
	include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
	$libfs = new libfilesystem();
	
	include_once($PATH_WRT_ROOT."includes/libdigitalarchive_moduleupload.php");
	$ldamu = new libdigitalarchive_moduleupload();
	
	
	############################################################################
	################# Prepare Zip and Html directory [Start] ###################
	############################################################################
	
	// Prepare Html and Zip folder
	$TempFolder = $intranet_root."/file/temp/erc_export_html";
	$TempUserFolder = $TempFolder."/".$_SESSION["UserID"];
	$TempHtmlFolder = $TempUserFolder."/student_report";
	
	// Prepare Zip path
	$ZipFileName = $thisReportTitle.".zip";
	$ZipFilePath = $TempUserFolder."/".$ZipFileName;
	
	// Create folder
	if (!file_exists($TempHtmlFolder)) {
		$SuccessArr['CreateTempCsvFolder'] = $libfs->folder_new($TempHtmlFolder);
	}
	// [Omas] Clear temp files from last session
	else {
		$fileList = array_diff(scandir($TempHtmlFolder), array("..", "."));
		if(count($fileList) > 0){
			foreach((array)$fileList as $filename){
				$libfs->file_remove($TempHtmlFolder."/".$filename);
			}
		}
	}
	
	############################################################################
	############### Prepare zip and html files directory [End] #################
	############################################################################
	
	
	// loop Students
	for ($i=0; $i<$numOfStudent; $i++) {
		// Get Student Info
		$thisStudentID = $studentInfoAry[$i]["UserID"];
		$thisClassName = $studentInfoAry[$i]["ClassName"];
		$thisClassNumber = $studentInfoAry[$i]["ClassNumber"];
		
		// Prepare Html Path
		$HtmlFileName = $thisClassName."-".$thisClassNumber."_report.html";
		$HtmlFilePath = $TempHtmlFolder."/".$HtmlFileName;

		// Prepare Report Url
		$thisReportUrl = "";
		$thisReportUrl .= curPageURL($withQueryString=0, $withPageSuffix=0);
        $thisReportUrl .= "/home/eAdmin/StudentMgmt/eReportCard/reports/generate_reports/print_preview.php?TargetStudentID[]=".$thisStudentID."&ReportID=".$targetReportID."&hp=1";

        // Prepare Curl Token
        $extraUrlToken = $thisReportUrl.$_SESSION['UserID'];
        $extraUrlToken = str_replace($_SERVER['HTTP_ORIGIN'], '', $extraUrlToken);

		// Get Report Content
		$thisFileContent = getHtmlByUrl($thisReportUrl, $extraUrlToken);
		$thisFileContent = str_replace($PATH_WRT_ROOT, "", $thisFileContent);
		$thisFileContent = $ldamu->Embed_Image_CSS_To_HTML($thisFileContent);

		// Store Html file
		$SuccessArr['createStudentTmpFile'][$thisStudentID] = $libfs->file_write($thisFileContent, $HtmlFilePath);
	}
	
	intranet_closedb();
	
	// Create Zip file
	$SuccessArr['DeleteLastGeneratedZipFile'] = $libfs->file_remove($ZipFilePath);
	$SuccessArr['ZipCsvFiles'] = $libfs->file_zip("student_report", $ZipFilePath, $TempUserFolder);
	$SuccessArr['DeleteTempCsvFiles'] = $libfs->folder_remove_recursive($TempHtmlFolder);
	
	// Output Zip file
	output2browser(get_file_content($ZipFilePath), $ZipFileName);
}
else {
	header ("Location: /");
	intranet_closedb();
	exit();
}
?>