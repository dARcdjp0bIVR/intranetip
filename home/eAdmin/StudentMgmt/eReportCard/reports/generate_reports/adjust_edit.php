<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");

intranet_auth();
intranet_opendb();

if (!$plugin['ReportCard2008']) 
{
	intranet_closedb();
	header("Location: index.php");
}

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008w.php");
//include_once($PATH_WRT_ROOT."includes/libreportcard2008_sis.php");
//$lreportcard = new libreportcardSIS();
$lreportcard = new libreportcard();
if (!$lreportcard->hasAccessRight() || !$ReportID || !$StudentID || !$ClassID) 
{
	intranet_closedb();
	header("Location: index.php");
}

$lu = new libuser($StudentID);
$lclass = new libclass();

$linterface = new interface_html();
$CurrentPage = "Reports_GenerateReport";
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
$TAGS_OBJ[] = array($eReportCard['Reports_GenerateReport'], "", 0);
$linterface->LAYOUT_START();

$PAGE_NAVIGATION[] = array($eReportCard['ManualAdjustment']);

$reportTemplateInfo = $lreportcard->returnReportTemplateBasicInfo($ReportID);
$ClassLevelID = $reportTemplateInfo['ClassLevelID'];
$ReportTitle = $reportTemplateInfo['ReportTitle'];
$ReportTitle = str_replace(":_:", "&nbsp;", $ReportTitle);


# Table Header
$TermHeaderAry = $lreportcard->genGrandMSTermHeader($ReportID);
// debug_r($TermHeaderAry);

# Subject
$SubjNameAry = $lreportcard->genGrandMSSubject($ReportID);
// debug_r($SubjNameAry);

# Marks
$MarksBoxAry = $lreportcard->MarksBox($ReportID, $StudentID);

$TableWidth = 0;
# construct table details
# Header
$h = "<tr>";
$h .= "<td class='tabletop tabletopnolink'>". $eReportCard['Subject'] ."</td>";
$TableWidth++;
foreach($TermHeaderAry as $k=>$d)
{
	$h .= "<td class='tabletop tabletopnolink' width='150'>". $d ."</td>";
	$TableWidth++;
}
$h .= "</tr>";

# Content
$hi = 0;
foreach($SubjNameAry as $SubjectID => $SubjectName)
{
	$css = ($hi % 2)+1;
	$h .= "<tr>";
	$h .= "<td class='tabletext tablerow".$css."'>". $SubjectName ."</td>";
	foreach($TermHeaderAry as $k=>$d)
		$h .= "<td class='tabletext tablerow".$css."'>". $MarksBoxAry[$SubjectID][$k] ."</td>";
	$h .= "</tr>";
	$hi++;
}

$h .= "<tr><td class='dotline' colspan='$TableWidth'><img src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif' width='10' height='1' /></td></tr>";


?>


<br />

<form name="form1" method="post" action="adjust_update.php">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td align="center">
        	<table width="90%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td align="left" class="tabletext"><?=$linterface->GET_NAVIGATION($PAGE_NAVIGATION);?></td>
			</tr>
			<tr>
				<td>
					<table width="100%" border="0" cellspacing="0" cellpadding="5">
					<tr>
						<td align="left" class="tabletext formfieldtitle" width="30%"><?=$eReportCard['ReportTitle']?></td>
						<td align="left" class="tabletext"><?=$ReportTitle?></td>
					</tr>
					<tr>
						<td align="left" class="tabletext formfieldtitle"><?=$eReportCard['Student']?></td>
						<td align="left" class="tabletext"><?=$lu->UserName()?></td>
					</tr>
					
					<tr>
						<td align="left" class="tabletext formfieldtitle"><?=$eReportCard['Class']?></td>
						<td align="left" class="tabletext"><?=$lclass->getClassName($ClassID)?></td>
					</tr>
					</table>
				</td> 
			</tr>
            </table>
        </td>
</tr>
<tr>
	<td align="center">
		<table width="90%" border="0" cellspacing="0" cellpadding="4">
		<?=$h?>
		</table>	
	</td>
</tr>	
	<tr>
		<td>        
            <table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">
            <tr>
				<td align="center">
					<?= $linterface->GET_ACTION_BTN($button_save, "submit", "", "submit2") ?>
					<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2") ?>
					<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='adjust_list.php?ReportID=$ReportID&ClassID=$ClassID'","cancelbtn") ?>
				</td>
			</tr>
			</table>                                
	</td>
</tr>
</table>
<br />

<input type="hidden" name="ReportID" value="<?=$ReportID?>">
<input type="hidden" name="StudentID" value="<?=$StudentID?>">
<input type="hidden" name="ClassID" value="<?=$ClassID?>">
</form>

<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>
