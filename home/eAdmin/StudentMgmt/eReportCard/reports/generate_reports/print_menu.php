<?php
# using: 
/**************************************************
 * 	Modification log
 *  20200514 Bill   [2020-0429-1507-45073]
 *      - allow users to choose display column num for F6 Year Report
 *  20170217 Bill
 * 		- Support export function	($eRCTemplateSetting['Report']['BatchExportReport'])
 * 	20160427 Bill	[2016-0330-1524-42164]
 * 		- allow users to choose language version of ES Semester Report
 * ***********************************************/
 
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if (!$plugin['ReportCard2008'])
{
	header ("Location: /");
	intranet_closedb();
	exit();
}

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008j.php");

$lreportcard = new libreportcard2008j();
$linterface = new interface_html();

if (!$lreportcard->hasAccessRight()) 
{
	header ("Location: /");
	intranet_closedb();
	exit();
}

# Main
$ReportSetting = $lreportcard->returnReportTemplateBasicInfo($ReportID);
$ClassLevelID = $ReportSetting['ClassLevelID'];
$SemID = $ReportSetting['Semester'];
$ReportType = $SemID == "F" ? "W" : "T";

if($ClassLevelID != ""){
	# Get Class
	$ClassArr = $lreportcard->GET_CLASSES_BY_FORM($ClassLevelID);
	# Filters - By Form (ClassLevelID)
	$ClassOptions[] = array("", "--All--");
	if(count($ClassArr) > 0 ){
		for($i=0 ; $i<count($ClassArr) ; $i++){
			$ClassOptions[] = array($ClassArr[$i][0], $ClassArr[$i][1]);
		}
	}
	$ClassSelection = $linterface->GET_SELECTION_BOX($ClassOptions, "name='ClassID' id='ClassID' class='tabletexttoptext' onchange='jFILTER_CHANGE(0)'", "", $ClassID);
	
	if($ClassID!="")
	{
		$StudentSelection = $lreportcard->GET_REPORT_STUDENT_SELECTION($ClassID, $ReportID, $PrintTemplateType);

		$StudentRow = "<tr>
						<td valign='top' nowrap='nowrap' ><span class='tabletext'>".$i_identity_student.": </span></td>
						<td width='80%' style='align: left'>
							<table border='0' cellpadding='0' cellspacing='0' align='left'>
								<tr> 
									<td class='tabletext'>".$StudentSelection."</td>
									<td style='vertical-align:bottom'>        
										<table width='100%' border='0' cellspacing='0' cellpadding='6'>
											<tr> 
												<td align='left'> 
													".$linterface->GET_BTN($button_select_all, 'button', "SelectAll(this.form.elements['TargetStudentID[]']); return false;")."
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>";
	}
}

# Customization for Munsang College (Primary) - Choose Report in Student View or Teacher View
$reportTypeRow = "";
if ($ReportCardCustomSchoolName == "munsang_pri")
{
	include_once($intranet_root."/lang/reportcard_custom/munsang_pri.$intranet_session_language.php");
	
	$reportTypeRow .= "<tr>";
	$reportTypeRow .= "<td nowrap=\"nowrap\" class=\"formfieldtitle tabletext\"><span class=\"tabletext\">".$eReportCard['ReportView'].":</span></td>";
	$reportTypeRow .= "<td>";
		$reportTypeRow .= "<input type=\"radio\" name=\"viewType\" value=\"student\" checked><label for=\"student\">".$eReportCard['Student']."</label>";
		$reportTypeRow .= toolBarSpacer();
		$reportTypeRow .= "<input type=\"radio\" name=\"viewType\" value=\"teacher\"><label for=\"teacher\">".$eReportCard['Teacher']."</label>";
	$reportTypeRow .= "</td>";													
	
	$reportTypeRow .= "</tr>";
	
}

if($ReportCardCustomSchoolName == "carmel_alison" && $ReportType=="T")
{
	$simpleReportTypeRow .= "<tr>";
	$simpleReportTypeRow .= "<td nowrap=\"nowrap\" class=\"formfieldtitle tabletext\"><span class=\"tabletext\">".$eReportCard['ReportView'].":</span></td>";
	$simpleReportTypeRow .= "<td>";
		$isChecked = ($SimpleReportType=='normal' || $SimpleReportType=='')?1:0;
		$simpleReportTypeRow .= $linterface->Get_Radio_Button("NormalReportRadio", "SimpleReportType", 'normal', $isChecked, "ArchiveReportTypeClass", $eReportCard['Normal']);
		$simpleReportTypeRow .= toolBarSpacer();
		$isChecked = $SimpleReportType=='simple'?1:0; 
		$simpleReportTypeRow .= $linterface->Get_Radio_Button("SimpleReportRadio", "SimpleReportType", 'simple', $isChecked, "ArchiveReportTypeClass", $eReportCard['Simple']);
	$simpleReportTypeRow .= "</td>";													
	
	$simpleReportTypeRow .= "</tr>";
}

if ($ReportCardCustomSchoolName == "sekolah_menengah_yu_yuan_ma")
{
	$PrintTemplateType = ($PrintTemplateType=='')?'normal':$PrintTemplateType;
	
	if($PrintTemplateType=='normal')
	{
		$normalSelect = 1;
		$foreignSelect = 0;
	}
	else if($PrintTemplateType=='foreign')
	{
		$normalSelect = 0;
		$foreignSelect = 1;
	}
	 
	$foreign_html .= "<tr>";
		$foreign_html .= "<td nowrap=\"nowrap\" class=\"formfieldtitle tabletext\"><span class=\"tabletext\">".$eReportCard['AcademicReport'].":</span></td>";
		$foreign_html .= "<td>";
			$foreign_html .= $linterface->Get_Radio_Button("NormalStdRadio", "PrintTemplateType", 'normal',$normalSelect, "ArchiveReportTypeClass", $eReportCard['NormalStd'],"js_changeStd('normal');");
			$foreign_html .= "<br/>";
			$foreign_html .= $linterface->Get_Radio_Button("ForeignStdRadio", "PrintTemplateType", 'foreign', $foreignSelect, "ArchiveReportTypeClass", $eReportCard['ForeignStd'],"js_changeStd('foreign');");
		$foreign_html .= "</td>";															
	$foreign_html .= "</tr>";
}

// [2016-0330-1524-42164] Customization for BIBA - Choose Language Version of ES Semester Report
$ReportTypeLangRow = "";
if($ReportCardCustomSchoolName == "biba_cn")
{
	$isMainReport = $ReportSetting['isMainReport'];
	$FormNumber = $lreportcard->GET_FORM_NUMBER($ClassLevelID);
	
	// display radio buttons for ES Semester Report Language
	if (is_numeric($FormNumber) && $FormNumber > 0 && $FormNumber < 6 && $isMainReport) {
		$ReportTypeLangRow .= "<tr>";
		$ReportTypeLangRow .= "<td nowrap=\"nowrap\" class=\"formfieldtitle tabletext\"><span class=\"tabletext\">".$eReportCard['ReportView'].":</span></td>";
		$ReportTypeLangRow .= "<td>";
			$isChecked = ($ReportLang=="bilingual" || $ReportLang=="")? 1 : 0;
			$ReportTypeLangRow .= $linterface->Get_Radio_Button("BilingualReportRadio", "ReportLang", "bilingual", $isChecked, "ArchiveReportTypeClass", $eReportCard['LangDisplayChoice']['both']);
			$ReportTypeLangRow .= "&nbsp;";
			$isChecked = $ReportLang=="chinese"? 1 : 0;
			$ReportTypeLangRow .= $linterface->Get_Radio_Button("ChineseReportRadio", "ReportLang", "chinese", $isChecked, "ArchiveReportTypeClass", $i_general_chinese);
			$ReportTypeLangRow .= "&nbsp;";
			$isChecked = $ReportLang=="english"? 1 : 0;
			$ReportTypeLangRow .= $linterface->Get_Radio_Button("EnglishReportRadio", "ReportLang", "english", $isChecked, "ArchiveReportTypeClass", $i_general_english);
		$ReportTypeLangRow .= "</td>";
		$ReportTypeLangRow .= "</tr>";
	}
}

// [2020-0429-1507-45073] Customization for Wu York Yu - Choose Display Column for F6 Year Report
$displayColRow = "";
if($ReportCardCustomSchoolName == "twgh_wu_york_yu_college")
{
    $isYearlyReport = $ReportSetting['isMainReport'] && $ReportType == 'W';
    $FormNumber = $lreportcard->GET_FORM_NUMBER($ClassLevelID);
    if (is_numeric($FormNumber) && $FormNumber == 6 && $isYearlyReport)
    {
        $displayColRow .= "<tr>";
            $displayColRow .= "<td nowrap=\"nowrap\" class=\"formfieldtitle tabletext\"><span class=\"tabletext\">".$eReportCard['DisplayColumnNum'].":</span></td>";
            $displayColRow .= "<td>";
                $displayColRow .= $linterface->Get_Radio_Button("DisplayColumnNum_2", "DisplayColumnNum", 2, 1, "ArchiveReportTypeClass", 2);
                $displayColRow .= "&nbsp;";
                $displayColRow .= $linterface->Get_Radio_Button("DisplayColumnNum_3", "DisplayColumnNum", 3, 0, "ArchiveReportTypeClass", 3);
            $displayColRow .= "</td>";
        $displayColRow .= "</tr>";
    }
}

$htmlAry['custReportViewTr'] = '';
if (is_array($eRCTemplateSetting['Report']['ReportGeneration']['ReportViewAry'])) {
	$reportViewAry = $eRCTemplateSetting['Report']['ReportGeneration']['ReportViewAry'];
	$numOfReportView = count($reportViewAry);
	
	$htmlAry['custReportViewTr'] .= "<tr>";
		$htmlAry['custReportViewTr'] .= "<td nowrap=\"nowrap\" class=\"formfieldtitle tabletext\"><span class=\"tabletext\">".$eReportCard['ReportView'].":</span></td>";
		$htmlAry['custReportViewTr'] .= "<td>";
			for ($i=0; $i<$numOfReportView; $i++) {
				$_reportView = $reportViewAry[$i];
				$_reportViewDisplay = $eReportCard['Template']['ReportView'][$_reportView];
				
				$_isChecked = false;
				if ($i==0 && $PrintTemplateType=='') {
					$_isChecked = true;
				}
				else if ($_reportView == $PrintTemplateType) {
					$_isChecked = true;
				}
				
				$htmlAry['custReportViewTr'] .= $linterface->Get_Radio_Button("reportViewRadio_".$_reportView, "PrintTemplateType", $_reportView, $_isChecked, $_class="", $_reportViewDisplay);
				$htmlAry['custReportViewTr'] .= toolBarSpacer();
			}
		$htmlAry['custReportViewTr'] .= "</td>";
	$htmlAry['custReportViewTr'] .= "</tr>";
}

# UI
$CurrentPage = "ReportGeneration";
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
$MODULE_OBJ['title'] = $eReportCard['ReportPrinting'];
$linterface = new interface_html("popup.html");
$linterface->LAYOUT_START();

$formMethod = ($eRCTemplateSetting['Reports']['ReportGeneration']['PrintReportMethod']=='')? 'POST' : $eRCTemplateSetting['Reports']['ReportGeneration']['PrintReportMethod'];
?>

<script language="javascript">
<!--
function jFILTER_CHANGE(jChangeType)
{
	obj = document.form1;

	if(jChangeType==1 && typeof(obj.ClassID)!="undefined")
		obj.ClassID.value = "";

	obj.target = "_parent";
	obj.action = "print_menu.php";
	obj.submit();
}

function jDO_PRINT()
{
	obj = document.form1;
	obj.action = "print_preview.php";
	<?php if($eRCTemplateSetting['Report']['ExportPDFReport']){ ?>
		obj.action = "print_preview_by_pdf.php";
	<?php } ?> 
	<? if($eRCTemplateSetting['Report']['BatchExportReport'] && $isExport){ ?>
		obj.action = "export_report_update.php";
	<? } ?> 
	obj.target = "_blank";
	obj.submit();
}

function SelectAll(obj)
{
	// if use i++, the menu will scroll to the bottom
	for (i=obj.length-1; i>=0; i--)
	{
		obj.options[i].selected = true;
	}
}

function js_changeStd(ParStd)
{
	obj = document.form1;

//	if(jChangeType==1 && typeof(obj.ClassID)!="undefined")
//		obj.ClassID.value = "";

	obj.target = "_parent";
	obj.action = "print_menu.php";
	obj.submit();
}
-->
</script>

<br/>
<form name="form1" method="<?=$formMethod?>">
<table border="0" cellpadding="0" cellspacing="0" align="center" width="90%">
  <tr>
	<td align="center">
	  <table width="95%" border="0" cellpadding="5" cellspacing="1">
	  	<?=$reportTypeRow?>
	  	<?=$simpleReportTypeRow?>	  
	  	<?=$foreign_html?>	
	  	<?=$ReportTypeLangRow?>
        <?=$displayColRow?>
	  	<?=$htmlAry['custReportViewTr']?>
	    <tr>
		  <td nowrap="nowrap" class="formfieldtitle tabletext"><?=$eReportCard['Class']?>:</td>
		  <td class="tabletext" width="75%"><?=$ClassSelection?></td>
		<tr>
		<?=$StudentRow?>
		
	  </table>
	</td>
  </tr>
  <tr>
    <td>
	  <table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
	  	<tr>
	      <td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
	  	</tr>
	  	<tr>
		  <td align="center">
			<?= $linterface->GET_ACTION_BTN($button_continue, "button", "javascript:jDO_PRINT()")?>
			<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:window.close()")?>
		  </td>
	  	</tr>
	  	
	  </table>
	  <br/>
	</td>
  </tr>
</table>

<input type="hidden" name="ReportID" id="ReportID" value="<?=$ReportID?>" />
<input type="hidden" name="isExport" id="isExport" value="<?=$isExport?>" />

</form>

<script>
// Select all students by default
var studentSelectionObj = document.getElementById('TargetStudentID[]');
if (studentSelectionObj)
	SelectAll(studentSelectionObj);
</script>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>