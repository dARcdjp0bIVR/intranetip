<?php
# using: 

@SET_TIME_LIMIT(21600);
@ini_set(memory_limit, -1);

/**************************************************
 * 	Modification log
 *  20200818 Bill  :    [2020-0818-1009-50206]
 *      - Handle IP30 upgrade - report layout changed issue
 *      - IP25 : '2009a'
 *      - IP30 : '2020a'
 *  20200722 Philips:	[2020-0717-1217-10066]
 *  	- seperate page 1 and page 2 div for carmel_alison
 *  20200717 Philips: 	[2020-0717-1217-10066]
 *  	- carmel_alison Page 3 layout use page-break-before instead of page-break-after
 *  20190711 Bill  :    [2019-0711-1055-59207]
 *      - allow access via print mode
 *  20190301 Bill  :	[2018-1130-1440-05164]
 *      always page break for page 1
 * 	20170623 Bill  :
 * 		Apply Promotion Status filtering
 * 	20170410 Bill  :	[2017-0407-1642-14164]
 * 		Publishing Period - control if user can print report or not
 * 		support student to print report (escola pui ching)
 *  20161117 Bill  :	[2015-1104-1130-08164]
 * 		added checking for parent to print student report (escola pui ching)
 *  20160315 Bill  :
 * 		set paper size for wu york yu college
 * 	20160222 Bill  :	[2015-0421-1144-05164]
 * 		set A3 paper size for pooi to semester report
 *  20140801 Ryan  : 
 * 		Merged SIS Cust 
 * 	20100205 Marcus:
 * 		for carmel_alison, separate page3 from the report and than print them after printing page1 and 2 of the report
 * ***********************************************/

##############################################################################################################################
### If you have modified this page, please check if you have to modify /management/archive_report_card/archive_update.php also
##############################################################################################################################

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

$isPrintMode = false;
$specialPrintHandling = false;
if ($plugin['ReportCard2008'])
{
    if (is_file($intranet_root."/includes/eRCConfig.php")) {
        include_once ($intranet_root."/includes/eRCConfig.php");
    }

    // [2019-0711-1055-59207] Check if valid access in Print Mode
    $isPrintMode = $_GET['hp'];
    if($eRCTemplateSetting['Report']['BatchExportReport'] && $isPrintMode && performSimpleTokenByDateTimeChecking($_POST['token'], $_SERVER['REQUEST_URI'].$_POST['UserID']))
    {
        @session_start();
        $_SESSION['UserID'] = $_POST['UserID'];

        $specialPrintHandling = true;
    }
}

intranet_auth();
intranet_opendb();

$SubjectID = $_REQUEST['SubjectID'];
$SubjectSectionOnly = $_REQUEST['SubjectSectionOnly'];
$CustomSchoolCode = $_REQUEST['CustomSchoolCode'];
$HidePrint = $_REQUEST['hp'];
$ShowPrintBtn = ($HidePrint==1)? false : true;
$PrintTemplateType = $_REQUEST['PrintTemplateType'];
$PrintTemplateType = ($PrintTemplateType=='')? 'normal' : $PrintTemplateType;

if (!$plugin['ReportCard2008'])
{
	header ("Location: /");
	intranet_closedb();
	exit();
}

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
# SIS Cust 
include_once($PATH_WRT_ROOT."includes/libreportcard2008w.php");
if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
{
	# load specific layout for teacher view of munsang college (primary) report 
	if ($ReportCardCustomSchoolName == "munsang_pri" && $viewType == "teacher")
	{
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName."_teacher.php");	
	}
	else
	{
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
	}
	
	$lreportcard = new libreportcardcustom();
}
else {
	$lreportcard = new libreportcard();
}

$linterface = new interface_html();

if($eRCTemplateSetting['Management']['MarksheetVerification']['PrintStudentReport'] && $ck_ReportCard_UserType == "PARENT" && $_POST["GenerateStudentReport"]==1)
{
	if($ReportID)
	{
		// Get Report Last Generate Date
		$ReportBasicInfo = $lreportcard->returnReportTemplateBasicInfo($ReportID);
		$ReportGenerateDate = $ReportBasicInfo['LastGenerated'];
		$isReportGenerated = !is_date_empty($ReportGenerateDate);

		$thisTargetStudentID = $TargetStudentID[0];
		if($thisTargetStudentID)
		{
			// Check if target student is child of this user
			include_once($PATH_WRT_ROOT."includes/libuser.php");
			$luser = new libuser($_SESSION['UserID']);
			$ChildrenList = $luser->getChildrenList();
			$ChildrenList = Get_Array_By_Key((array)$ChildrenList, 'StudentID');
			$isCurrentUserChild = $thisTargetStudentID!="" && in_array($thisTargetStudentID, $ChildrenList);

			// Check if target student is in related Class Level
			$StudentInfoArr = $lreportcard->Get_Student_Class_ClassLevel_Info($thisTargetStudentID);
			$ClassLevelID = $StudentInfoArr[0]['ClassLevelID'];
			$isStudentInReport = !empty($ClassLevelID);
		}
	}

	// Redirect if not valid
	if(!$isReportGenerated || !$isCurrentUserChild || !$isStudentInReport)
	{
		// always return false except has access right
		if(!$lreportcard->hasAccessRight())
		{
			header ("Location: /");
			intranet_closedb();
			exit();
		}
	}
}
else if($eRCTemplateSetting['Management']['MarksheetVerification']['PrintStudentReport'] && $ck_ReportCard_UserType == "STUDENT" && $_POST["GenerateStudentReport"]==1)
{
	if($ReportID)
	{
		// Get Report Last Generate Date
		$ReportBasicInfo = $lreportcard->returnReportTemplateBasicInfo($ReportID);
		$ReportGenerateDate = $ReportBasicInfo['LastGenerated'];
		$isReportGenerated = !is_date_empty($ReportGenerateDate);

		$thisTargetStudentID = $TargetStudentID[0];
		if($thisTargetStudentID)
		{
			// Check if target student is in related Class Level
			$StudentInfoArr = $lreportcard->Get_Student_Class_ClassLevel_Info($thisTargetStudentID);
			$ClassLevelID = $StudentInfoArr[0]['ClassLevelID'];
			$isStudentInReport = !empty($ClassLevelID);
		}
	}

	// Redirect if not valid
	if(!$isReportGenerated || !$isStudentInReport)
	{
		// always return false except has access right
		if(!$lreportcard->hasAccessRight())
		{
			header ("Location: /");
			intranet_closedb();
			exit();
		}
	}
}
else if ($isPrintMode && $specialPrintHandling)
{
    // do nothing
}
else if (!$lreportcard->hasAccessRight())
{
	header ("Location: /");
    intranet_closedb();
    exit();
}

# Print Button
$PrintBtn = '';
if ($ShowPrintBtn) {
	$PrintBtn .= '<table border="0" cellpadding="4" width="95%" cellspacing="0" class="print_hide">'."\n";
		$PrintBtn .= '<tr><td align="right">'.$linterface->GET_BTN($Lang['Btn']['Print'], "button", "javascript:window.print();","submit2").'</span></td></tr>'."\n";
	$PrintBtn .= '</table>'."\n";
} 

# Get the eRC Template Settings
include_once($PATH_WRT_ROOT."includes/eRCConfig.php");
if($ReportCardCustomSchoolName != 'sis')
{
	// [2020-0818-1009-50206] Handle IP30 upgrade - report layout changed issue
	if($eRCTemplateSetting['IsApplyIP30Style']) {
	    // do nothing
    } else {
        $LAYOUT_SKIN = '2009a';
    }
    $eRCtemplate = $lreportcard->getCusTemplate();
}
else
{
	# SIS : Follow PMS Style 
	$LAYOUT_SKIN = '2007a';
}	
$eRCtemplate = empty($eRCtemplate) ? $lreportcard : $eRCtemplate;
$eRtemplateCSS = $lreportcard->getTemplateCSS();

$ReportSetting = $lreportcard->returnReportTemplateBasicInfo($ReportID);
$isMainReport = $ReportSetting['isMainReport'];
$ClassLevelID = $ReportSetting['ClassLevelID'];
$SemID = $ReportSetting['Semester'];
$ReportType = $SemID == "F" ? "W" : "T";
if($ReportCardCustomSchoolName == "carmel_alison" && $ReportType=="T")
{
	$isSimpleReport = $SimpleReportType=='simple'?1:0;
}
if($ReportCardCustomSchoolName != 'sis')
include_once($PATH_WRT_ROOT."templates/{$LAYOUT_SKIN}/layout/print_header_erc.php");
else 
include_once($PATH_WRT_ROOT."templates/{$LAYOUT_SKIN}/layout/print_header.php");
?>
<? if($eRtemplateCSS) {?><link href="<?=$eRtemplateCSS?>" rel="stylesheet" type="text/css" /><? } ?>
<?=$PrintBtn?>
<!--br style="clear:both;" /-->
<?
$TargetStudentList = (is_array($TargetStudentID)) ? implode(",", $TargetStudentID) : "";
$StudentIDAry = $lreportcard->GET_STUDENT_BY_CLASSLEVEL($ReportID, $ClassLevelID, $ClassID, $TargetStudentList);

// Promotion Status filter
if($eRCTemplateSetting['Report']['ClassTeacherReportPrinting'] && $ReportType=="W" && $_POST['PromotionStatus'] > 0)
{
	$thisPromotionStatus = $_POST['PromotionStatus'];
	if($thisPromotionStatus==1) {
		// Promotion Status : Graduate / Promoted / Promoted with Subject Retention
		$relatedPromotionStatus = array(1, 2, 3);
	}
	else {
		// Promotion Status : Retained / Dropout
		$relatedPromotionStatus = array(4, 5);
		
		// Get Mark-up Exam Marks
		$studentid_ary = Get_Array_By_Key((array)$StudentIDAry, "UserID");
		$thisMarkUpResult = $lreportcard->GET_EXTRA_SUBJECT_INFO($studentid_ary, "", $ReportID);
	}
	
	// Get Student Promotion Status
	$StudentPromotionList = $lreportcard->GetPromotionStatusList($ClassLevelID, $ClassID, "", $ReportID);
	
	// loop Students
	$matchedStatusStudentAry = array();
	foreach($StudentIDAry as $thisStudentInfo)
	{
		$thisStudentID = $thisStudentInfo['UserID'];
		$thisStudentPromotion = $StudentPromotionList[$thisStudentID]['Promotion'];
		if($thisStudentPromotion && in_array($thisStudentPromotion, (array)$relatedPromotionStatus))
		{
			// Promotion Status : Graduate / Promoted / Promoted with Subject Retention
			if($thisPromotionStatus==1) {
				$matchedStatusStudentAry[] = $thisStudentInfo;
			}
			// Promotion Status : Retained / Dropout - Need Mark-up Exam checking
			else {
				// Check if student attend any Mark-up Exam
				$thisStudentMarkupResult = $thisMarkUpResult[$thisStudentID];
				if(!empty($thisStudentMarkupResult))
				{
					$thisStudentMarkupResult = array_values((array)$thisStudentMarkupResult);
					$thisStudentMarkupResult = Get_Array_By_Key($thisStudentMarkupResult, "Info");
					$thisStudentMarkupResult = array_filter((array)$thisStudentMarkupResult);
				}
				
				// Retained / Dropout - Before / After Make-up Exam
				if(($thisPromotionStatus==2 && empty($thisStudentMarkupResult)) || ($thisPromotionStatus==3 && !empty($thisStudentMarkupResult))) {
					$matchedStatusStudentAry[] = $thisStudentInfo;
				}
			}
		}
	}
	$StudentIDAry = $matchedStatusStudentAry;
}
$numOfStudent = count((array)$StudentIDAry);

$timer = array();
for($s=0;$s<$numOfStudent;$s++)
{
	$StudentID = $StudentIDAry[$s]['UserID'];
	
	if($ReportCardCustomSchoolName != "st_stephen_college" && $ReportCardCustomSchoolName != 'hkuga_college')
	{
		
		$TitleTable = $eRCtemplate->getReportHeader($ReportID, $StudentID, $PrintTemplateType);
		
		//$forPage3 -> carmel_alison
		//$PageNum -> wong_kam_fai_secondary
		//$PrintTemplateType -> hkcccu_logos_academy
		$StudentInfoTable = $eRCtemplate->getReportStudentInfo($ReportID, $StudentID, $forPage3='', $PageNum=1, $PrintTemplateType);
//		StartTimer();
		$MSTable = $eRCtemplate->getMSTable($ReportID, $StudentID, $PrintTemplateType, $PageNum=1);	// $PageNum for "wong_kam_fai_secondary"
//		$timer[] = StopTimer();
		$MiscTable = $eRCtemplate->getMiscTable($ReportID, $StudentID, $PrintTemplateType);
		if($ReportCardCustomSchoolName == 'sis'){
			$SignatureTable = $eRCtemplate->getSignatureTable($eRCTemplateSetting['Signature']);
		}
		else 
			$SignatureTable = $eRCtemplate->getSignatureTable($ReportID, $StudentID, $PrintTemplateType);
		$FooterRow = $eRCtemplate->getFooter($ReportID, $PrintTemplateType);
	}
	
	# do not print 'style="page-break-after:always"' for carmel_alison as it has been handled in reportcard_custom/carmel_alison.php
	$_pageBreak = '';
	// 2020-07-22 (Philips) - seperate page 1 and page 2 div for carmel_alison
	if($ReportCardCustomSchoolName == "carmel_alison"){
		$_pageBreak = ' style="page-break-before:always;" ';
	} else if(($s < $numOfStudent-1 && $ReportCardCustomSchoolName != "carmel_alison") || $ReportCardCustomSchoolName == "buddhist_fat_ho" || $ReportCardCustomSchoolName == "cdsj_5_macau") {    //2014-0912-1716-13164 
		$_pageBreak = ' style="page-break-after:always;" ';
	}
	
	// [2015-0421-1144-05164] Set A3 Paper Size
	if($ReportCardCustomSchoolName=="pooi_to_middle_school" && $isMainReport){
		$pageStyle = "_A3";
	}
	// Set paper size for main report
	if($ReportCardCustomSchoolName=="twgh_wu_york_yu_college" && $isMainReport){
		$pageStyle = "_main_report";
	}
	?>
		<div id="container<?=$pageStyle?>" <?=$_pageBreak?>>
		<? if (($ReportCardCustomSchoolName != "st_stephen_college" && $ReportCardCustomSchoolName != "carmel_alison") || ($ReportCardCustomSchoolName == "st_stephen_college" && $ReportSetting['isMainReport'] != 1) ) { ?>
			<table width="100%" border="0" cellpadding="02" cellspacing="0" valign="top">
		<? } ?>
		<?
		if($ReportCardCustomSchoolName == "st_stephen_college")
		{
			//2012-0516-1118-51071
//			if($ReportSetting['isMainReport'] == 1)
//			{
				$x = '';
				$x .= $eRCtemplate->FirstPage($ReportID, $StudentID);
				$x .= $eRCtemplate->getSubjectPages($ReportID, $StudentID);
						
				# Subejct Pages
				//style="page-break-before:always"
//				$ReportSetting = $eRCtemplate->returnReportTemplateBasicInfo($ReportID);
//				$ClassLevelID = $ReportSetting['ClassLevelID'];
//		 		$MainSubjectArray = $eRCtemplate->returnSubjectwOrder($ClassLevelID);
//		 		foreach($MainSubjectArray as $SubjectID => $SubejctData)
//		 		{
//					$x .= $eRCtemplate->SubjectPage($ReportID, $SubjectID, $StudentID);		
//		 		}
		 		
		 		echo $x;
//			}
//			else
//			{
//				$TitleTable = $eRCtemplate->getReportHeader($ReportID, $StudentID);
//				$StudentInfoTable = $eRCtemplate->getReportStudentInfo($ReportID, $StudentID);
//				$MSTable = $eRCtemplate->getMSTable($ReportID, $StudentID);
//				$x = $eRCtemplate->getLayout($TitleTable, $StudentInfoTable, $MSTable, $MiscTable, $SignatureTable, $FooterRow, $ReportID, $StudentID);
//				
//				echo $x;
//			}
			# OLE Page
			
			
		}
		else if($ReportCardCustomSchoolName == "carmel_alison")
		{
			if($isSimpleReport)
				echo $eRCtemplate->getSimpleLayout($TitleTable, $StudentInfoTable, $MSTable, $MiscTable, $SignatureTable, $FooterRow, $ReportID, $StudentID);
			else
			{
				$Layout = $eRCtemplate->getLayout($TitleTable, $StudentInfoTable, $MSTable, $MiscTable, $SignatureTable, $FooterRow, $ReportID, $StudentID);
				echo $Layout[0];
				$Page3Layout[]= $Layout[1];
			}		
		}else if($ReportCardCustomSchoolName == "sis"){
			$TopTable = "";
			$TopTable .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" valign='top' align='center'>\n";
			$TopTable .= "<tr valign='top'><td>".$TitleTable."<br/></td></tr>\n";
			$TopTable .= "<tr valign='top'><td>".$StudentInfoTable."</td></tr>\n";
			$TopTable .= "<tr valign='top'><td>".$MSTable."</td></tr>\n";
			$TopTable .= "<tr valign='top'><td>".$MiscTable."</td></tr>\n";
			$TopTable .= "</table>\n";
			
			$BottomTable = "";
			$BottomTable .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" valign='bottom' align='center'>\n";
			$BottomTable .= "<tr><td>".$SignatureTable."</td></tr>\n";
			$BottomTable .= $FooterRow."\n";
			$BottomTable .= "</table>\n";
			?>
			<div id="container" valign='top'>
			<?= ($s==0) ? "<br>":"" ?>
			<table valign='top' width="100%" border="0" cellpadding="0" cellspacing="0" <? if($s<sizeof($StudentIDAry)-1) {?>style="page-break-after:always"<? } ?>>
			<tr height='570px'><td valign='top' align='center'><?=$TopTable?></td><tr>
			<tr><td valign='bottom' align='center'><?=$BottomTable?></td><tr>
			</table>
			</div>
			<?
		}	
		else
		{
			if (method_exists($eRCtemplate, "getLayout")) {
				echo $eRCtemplate->getLayout($TitleTable, $StudentInfoTable, $MSTable, $MiscTable, $SignatureTable, $FooterRow, $ReportID, $StudentID, $SubjectID, $SubjectSectionOnly, $PrintTemplateType, $numOfStudent, $s);
			} else {
			?>
				<tr><td><?=$TitleTable?></td><tr>
				<tr><td><?=$StudentInfoTable?></td><tr>
				<tr><td><?=$MSTable?></td><tr>
				<tr><td><?=$MiscTable?></td><tr>
				<tr><td><?=$SignatureTable?></td><tr>
				<?=$FooterRow?>
			<?
			}
		}
		?>
		<? if ($ReportCardCustomSchoolName != "st_stephen_college" || ($ReportCardCustomSchoolName == "st_stephen_college" && $ReportSetting['isMainReport'] != 1) ) { ?>
			</table>
		<? } ?>
		<?php if($ReportCardCustomSchoolName != "carmel_alison"){?>
		</div>
		<?php }?>
	<?
}
//hdebug_pr($timer);
//hdebug_pr(array_sum($timer));

if($ReportCardCustomSchoolName == "carmel_alison" && is_array($Page3Layout) && !$isSimpleReport)
{
	for($i=0; $i<sizeof($Page3Layout);$i++)
	{
		// 2020-07-17 (Philips) - use page-break-before instead of page-break-after
	?>
	<div id="container" style="page-break-before:always;">
		<table width="100%" border="0" cellpadding="02" cellspacing="0" valign="top">
			<?=$Page3Layout[$i]?>
		</table>
		<!--<table width="100%" border="0" cellpadding="02" cellspacing="0" valign="top" style='page-break-before:avoid '>
			<tr><td>&nbsp;</td></tr>
		</table>
	</div>-->
	<?
	}
}

//hdebug_pr(convert_size(memory_get_usage()));
//hdebug_pr(convert_size(memory_get_usage(true)));
?>
<?=$PrintBtn?>
<?
if($ReportCardCustomSchoolName != 'sis')
include_once($PATH_WRT_ROOT."templates/{$LAYOUT_SKIN}/layout/print_footer_erc.php");
else 
include_once($PATH_WRT_ROOT."templates/{$LAYOUT_SKIN}/layout/print_footer.php");
?>