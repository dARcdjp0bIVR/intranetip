<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if (!$plugin['ReportCard2008'])
{
	header ("Location: /");
	intranet_closedb();
	exit();
}
 
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
{
	include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
	$lreportcard = new libreportcardcustom();
} else {
	$lreportcard = new libreportcard();
}

if (!$lreportcard->hasAccessRight()) 
{
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lreportcard_ui = new libreportcard_ui();
$linterface = new interface_html();

$CurrentPage = "Reports_GenerateReport";
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();

# tag information
$TAGS_OBJ[] = array($eReportCard['Reports_GenerateReport'], "", 0);
$linterface->LAYOUT_START();

$RankingSelection = '';
$RankingSelection .= '<select>';
	for ($i=1; $i<20; $i++) {
		$RankingSelection .= '<option>'.$i.'</option>';
	}
$RankingSelection .= '</select>';
?>

<script language="javascript">

</script>
<br />

<table id="html_body_frame" style="width:100%;">
	<tr><td class="navigation"> &nbsp; <img height="15" width="15" align="absmiddle" src="/images/2009a/nav_arrow.gif"><a class="navigation" href="javascript:jsGoBack()">成績表製作</a> &nbsp; <img height="15" width="15" align="absmiddle" src="/images/2009a/nav_arrow.gif"><span class="navigation">獎項製作</span></td></tr>
	<tr><td>
		<br />
		<table class="form_table_v30">
		    <tbody>
		        <tr>
					<td class="field_title">成績表標題</td>
					<td>Student Performance Report<br>2010-2011 First Term</td>
				</tr>
				<tr>
					<td class="field_title">成績表類型</td>
					<td>主要 學期成績表 [上學期]</td>
				</tr>
				<tr>
					<td class="field_title">級別</td>
					<td>F.1</td>
				</tr>
			</tbody>
		</table>
	
		<br />
		<?=$linterface->GET_NAVIGATION2_IP25('獎項設定')?>
		<table class="common_table_list_v30" id="ContentTable">
			<col align="left" style="width:40%;" />
			<col align="left" style="width:60%;" />
			
			<thead>
				<tr>
					<th>獎項名稱</th>
					<th>甄選準則</th>
				</tr>
			</thead>
			
			<tbody>
				<tr>
					<td>Academic Award</td>
					<td>全級第 <?=$RankingSelection?> 名至第 <?=$RankingSelection?> 名</td>
				</tr>
				<tr>
					<td>Subject Prize</td>
					<td>全級第 <?=$RankingSelection?> 名至第 <?=$RankingSelection?> 名</td>
				</tr>
				<tr>
					<td>Award for Exemplary Personal Qualities</td>
					<td>
						素質各方面至少獲得 <select><option>Very Good</option></select>
					</td>
				</tr>
				<tr>
					<td>Best-all-round Student Award</td>
					<td>
						全級第 <?=$RankingSelection?> 名至第 <?=$RankingSelection?> 名
						<br />
						<b>及</b>
						<br />
						素質各方面至少獲得 <select><option>Very Good</option></select>
					</td>
				</tr>
				<tr>
					<td>Subject Improvement Award</td>
					<td style="padding:0px;">
						<table>
							<tr>
								<td style="border:0px;">取決於</td>
								<td style="border:0px;"><select><option>標準分</option></select></td>
							</tr>
							<tr>
								<td style="border:0px;">最少進步分數</td>
								<td style="border:0px;"><input class="textboxnum" value="1.3" /></td>
							</tr>
							<tr>
								<td style="border:0px;">名額</td>
								<td style="border:0px;"><select><option>0</option></select> <span class="tabletextremark">(如沒有名額限制，請選擇「0」。)</span></td>
							</tr>
							<tr>
								<td style="border:0px;">由成績表</td>
								<td style="border:0px;"><select><option>Student Performance Report 2010-2011 First Term (主要 學期成績表 [上學期])</option></select><select><option>First Test</option></select></td>
							</tr>
							<tr>
								<td style="border:0px;">至成績表</td>
								<td style="border:0px;"><select><option>Student Performance Report 2010-2011 First Term (主要 學期成績表 [上學期])</option></select><select><option>First Exam</option></select></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>Improvement Scholarship (Will be shown for Whole Year Report only)</td>
					<td style="padding:0px;">
						<table>
							<tr>
								<td style="border:0px;">取決於</td>
								<td style="border:0px;"><select><option>標準分</option></select></td>
							</tr>
							<tr>
								<td style="border:0px;">最少進步分數</td>
								<td style="border:0px;"><input class="textboxnum" value="0" /></td>
							</tr>
							<tr>
								<td style="border:0px;">名額</td>
								<td style="border:0px;"><select><option>1</option></select> <span class="tabletextremark">(如沒有名額限制，請選擇「0」。)</span></td>
							</tr>
							<tr>
								<td style="border:0px;">由成績表</td>
								<td style="border:0px;"><select><option>Student Performance Report 2010-2011 First Term (主要 學期成績表 [上學期])</option></select><select><option>總成績</option></select></td>
							</tr>
							<tr>
								<td style="border:0px;">至成績表</td>
								<td style="border:0px;"><select><option>Student Performance Report 2010-2011 Second Term (主要 學期成績表 [下學期])</option></select><select><option>總成績</option></select></td>
							</tr>
						</table>
					</td>
				</tr>
			</tbody>
		</table>
		<br />
		<div class="edit_bottom_v30">
			<?=$linterface->GET_ACTION_BTN($Lang['Btn']['Generate'], "button", "js_Continue();")?>
			<?=$linterface->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "js_Continue();")?>
		</div>
	</td></tr>
</table>
<br />                  
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>