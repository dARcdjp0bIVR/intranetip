<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if ($plugin['ReportCard2008']) {
	include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
	if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
	{
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
		$lreportcard = new libreportcardcustom();
	} else {
		$lreportcard = new libreportcard();
	}
	
	$CurrentPage = "Reports_GenerateReport";
	$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
    
	if ($lreportcard->hasAccessRight()) {
        $linterface = new interface_html();
        
		############################################################################################################
		
		if (!isset($ClassLevelID, $ReportID)) {
			intranet_closedb();
			header("Location: index.php");
			exit();
		}
		
		$reportTemplateInfo = $lreportcard->returnReportTemplateBasicInfo($ReportID);
		// check if this report have been generated
		if ($reportTemplateInfo["LastGenerated"] == "0000-00-00 00:00:00" || $reportTemplateInfo["LastGenerated"] == "") {
			intranet_closedb();
			header("Location: index.php");
			exit();
		}
		
		// Class
		$ClassArr = $lreportcard->GET_CLASSES_BY_FORM($ClassLevelID);
		if (!isset($ClassID) || $ClassID == "")
			$ClassID = $ClassArr[0]["ClassID"];
		$ClassSelectBox = $linterface->GET_SELECTION_BOX($ClassArr, "name='ClassID' class='tabletexttoptext' onchange='document.form2.submit()'", "", $ClassID);
		
		for($i=0 ; $i<count($ClassArr) ;$i++) {
			if ($ClassID == $ClassArr[$i]["ClassID"])
				$ClassName = $ClassArr[$i]["ClassName"];
		}
		
		####### Customize checking for SIS Start #######
		if (getenv("SERVER_NAME") == "project6.broadlearning.com" || getenv("SERVER_NAME") == "sis-trial.broadlearning.com") {
			$SemID = $reportTemplateInfo['Semester'];
			$ReportType = $SemID == "F" ? "W" : "T";
			$ColoumnTitle = $lreportcard->returnReportColoumnTitle($ReportID);
			
			if(!((substr($ClassName, 0, 1)=="P" && $ReportType=="W") or (substr($ClassName, 0, 1)=="S" && sizeof($ColoumnTitle)==4))) {
				intranet_closedb();
				header("Location: index.php");
				exit();
			}
		}
		####### Customize checking for SIS Ended #######
		
		// Period (Term or Whole Year)
		#$ReportInfo = $lreportcard->GET_REPORT_TYPES($ClassLevelID, $ReportID);
		$PeriodName = $reportTemplateInfo['SemesterTitle'];
		$ReportTitle = $reportTemplateInfo['ReportTitle'];
		$ReportTitlePieces = explode(":_:", $ReportTitle);
		$ReportTitle = $ReportTitlePieces[0];
		
		// Student Info Array
		$StudentArray = $lreportcard->GET_STUDENT_BY_CLASS($ClassID, '', 1);
		$StudentSize = sizeof($StudentArray);
		
		// Student ID list of students in a class
		for($i=0; $i<$StudentSize; $i++)
			$StudentIDArray[] = $StudentArray[$i][0];
		if(!empty($StudentIDArray))
			$StudentIDList = implode(",", $StudentIDArray);
		
		$promotionInfo = $lreportcard->GET_PROMOTION_INFO($ReportID, $StudentIDList);
		
		# Student Content
		$promotionSelectBoxArr = array(
									array("3", $eReportCard['EmptyPromotion']),
									array("1", $eReportCard['Promoted']),
									array("0", $eReportCard['Retained']),
									array("2", $eReportCard['PromotedonTrial'])
								);
		
		// Tools for Select All to XXX
		$promotionOptions = $promotionSelectBoxArr;
		array_unshift($promotionOptions, array('', '--'.$eReportCard['ChangeAllTo'].'--'));
		
		$display = '<table class="yui-skin-sam" width="95%" cellpadding="5" cellspacing="0">';
			$display .= '<tr>';
		        $display .= '<td width="5%" align="center" class="tablegreentop tabletopnolink">'.$eReportCard['StudentNo_short'].'</td>';
		        $display .= '<td class="tablegreentop tabletopnolink">'.$eReportCard['Student'].'</td>';
				$display .= '<td width="10%" align="center" class="tablegreentop tabletopnolink">';
					$display .= $eReportCard['Promotion'];
					$display .= '<br />'.$linterface->GET_SELECTION_BOX($promotionOptions, 'name="promotionAll" id="promotionAll" class="tabletexttoptext" onchange="setAll(this.selectedIndex, \'promotionStatus\')"', '', '');
				$display .= '</td>';
				# SIS Promotion
				if($eRCTemplateSetting['ReportGeneration']['PromotionNewClass']){
					$display .= '<td width="10%" class="tablegreentop tabletopnolink">'.$eReportCard['NewClassLevel'].'</td>';
				}
		    $display .= '</tr>';
		
		if (isset($StudentSize) && $StudentSize > 0) {
		    for($j=0; $j<$StudentSize; $j++){
				list($StudentID, $WebSAMSRegNo, $StudentNo, $StudentName) = $StudentArray[$j];
				$tr_css = ($j % 2 == 1) ? "tablegreenrow2" : "tablegreenrow1";
				
				$selectedPromotionStatus = trim($promotionInfo[$StudentID]["Promotion"])!=''?$promotionInfo[$StudentID]["Promotion"]:1;
				$selectedPromotionResultID = $promotionInfo[$StudentID]["ResultID"];
				
				$display .= '<tr class="'.$tr_css.'">';
					$display .= '<td align="center" valign="top" class="tabletext">'.$StudentNo.'</td>';
					$display .= '<td class="tabletext" valign="top">'.$StudentName.'';
						$display .= '<input type="hidden" value="'.$selectedPromotionResultID.'" id="SelectedPromotionResultID_'.$selectedPromotionResultID.'" name="SelectedPromotionResultID[]"/>';
					$display .= '</td>';
					$display .= '<td align="center" class="tabletext" valign="top">';
						$display .= $linterface->GET_SELECTION_BOX($promotionSelectBoxArr, "name='promotionStatus[$selectedPromotionResultID]' id='promotionStatus_".$selectedPromotionResultID."' class='tabletexttoptext'", "", $selectedPromotionStatus);
					$display .=	'</td>';
					# SIS Promotion
					if($eRCTemplateSetting['ReportGeneration']['PromotionNewClass']){
					$display .=	'<td class="tabletext" valign="top">';
						$display .=	'<input type="text" name="newClassName['.$selectedPromotionResultID.']" id="newClassName_'.$selectedPromotionResultID.'" value="'.$promotionInfo[$StudentID]["NewClassName"].'" class="textboxnum" />';
					$display .=	'</td>';
					}
				$display .=	'</tr>';
			}
		    $display .= '</table>';
			
			$display_button .= $linterface->GET_ACTION_BTN($button_save, "button", "frmSubmit()")."&nbsp";
	        $display_button .= $linterface->GET_ACTION_BTN($button_reset, "reset", "")."&nbsp";
			$display_button .= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='index.php'");
		} else {
			$td_colspan = "4";
			$display .=	'<tr class="tablegreenrow1" height="40">
							<td class="tabletext" colspan="'.$td_colspan.'" align="center">'.$i_no_record_exists_msg.'</td>
						</tr>';
		}
		############################################################################################################
        
		$PAGE_NAVIGATION[] = array($eReportCard['PromotionEdit']);
		
		# tag information
		$TAGS_OBJ[] = array($eReportCard['Reports_GenerateReport'], "", 0);
		$linterface->LAYOUT_START();
?>
<script type="text/javascript">
function setAll(parStatus, type) {
	var indexToSelect = parStatus-1;
	if (indexToSelect < 0)
		return;
	var selectedPromotionResultID = document.getElementsByName("SelectedPromotionResultID[]");
	
	if(selectedPromotionResultID.length > 0){
		for (var i=0 ; i<selectedPromotionResultID.length ; i++) {
			var objName = type+"_"+selectedPromotionResultID[i].value;
			var obj = document.getElementById(objName);
			obj.selectedIndex = indexToSelect;
		}
	}
}

function frmSubmit() {
<? if($eRCTemplateSetting['ReportGeneration']['PromotionNewClass']){?>
	var selectedPromotionResultID = document.getElementsByName("SelectedPromotionResultID[]");
	
	if(selectedPromotionResultID.length > 0){
		for (var i=0 ; i<selectedPromotionResultID.length ; i++) {
			var objName = "newClassName_"+selectedPromotionResultID[i].value;
			var obj = document.getElementById(objName);
			if(obj.value.length != 2) {
				alert("<?=$eReportCard['NewClassLevelAlert']?>");
				obj.focus();
				return
			}
		}
	}
	<?}?>
	document.form1.submit();
}
</script>
<br/>
<form style="margin:0px;" name="form2" method="get" action="promotion_edit.php">
<div align="left" style="width:100%">
	<table border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td align="left" colspan="2">
				<?=$linterface->GET_NAVIGATION($PAGE_NAVIGATION)?>
			</td>
		</tr>
	</table>
</div>
<div align="left" style="width:95%">
<table border="0" cellpadding="5" cellspacing="0" class="tabletext">
	<tr>
		<td><?=$eReportCard['ReportTitle']?> : <strong><?=$ReportTitle?></strong></td>
		<td><?=$eReportCard['Class']?> : <strong><?=$ClassName?></strong></td>
	</tr>
</table>
</div>
<table width="95%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td><?=$ClassSelectBox?></td>
		<td align="right"><?= $linterface->GET_SYS_MSG($Result); ?></td>
	</tr>
</table>
<input type="hidden" name="ReportID" value="<?=$ReportID?>" />
<input type="hidden" name="ClassLevelID" value="<?=$ClassLevelID?>" />
</form>
<form name="form1" method="post" action="promotion_edit_update.php" onSubmit="return false">
<?=$display?>
<table width="95%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td height="1" class="dotline"><img src="<?=$image_path.'/'.$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
	</tr>
	<tr>
		<td>
			<table width="100%" border="0" cellspacing="0" cellpadding="2">
				<tr>
					<td align="center" valign="bottom"><?=$display_button?></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<input type="hidden" name="ClassLevelID" value="<?=$ClassLevelID?>"/>
<input type="hidden" name="ReportID" value="<?=$ReportID?>"/>
<input type="hidden" name="ClassID" value="<?=$ClassID?>"/>
</form>
<?
		intranet_closedb();
        $linterface->LAYOUT_STOP();
    } else {
    ?>
You have no priviledge to access this page.
    <?
    }
} else {
?>
You have no priviledge to access this page.
<?
}
?>