<?php
$PATH_WRT_ROOT = "../../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$plugin['ReportCard2008'])
{
	header ("Location: /");
	intranet_closedb();
	exit();
}
 
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
{
	include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
	$lreportcard = new libreportcardcustom();
} else {
	$lreportcard = new libreportcard();
}
include_once($PATH_WRT_ROOT."includes/libreportcard2008_award.php");

if (!$lreportcard->hasAccessRight()) 
{
	header ("Location: /");
	intranet_closedb();
	exit();
}

$ReportID = $_REQUEST['ReportID'];
$ReturnMsgKey = $_REQUEST['ReturnMsgKey'];

$linterface = new interface_html();
$lreportcard_ui = new libreportcard_ui();
$lreportcard_award = new libreportcard_award();

$CurrentPage = "Reports_GenerateReport";
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();

# tag information
$TAGS_OBJ[] = array($eReportCard['Reports_GenerateReport'], "", 0);

$ReturnMsg = $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['ReturnMsgArr'][$ReturnMsgKey];
$linterface->LAYOUT_START($ReturnMsg);

# sub-tag information
echo $lreportcard_ui->Get_Generate_Award_SubTag($ReportID, $CurrentTag=2);
echo '<br />'."\n";
echo $lreportcard_ui->Get_View_Award_Award_Mode_UI($ReportID);

?>

<script language="javascript" src="common.js"></script>
<script language="javascript">
$(document).ready( function() {
	
});

function js_Export() {
	window.location = 'export_award.php?ReportID=<?=$ReportID?>&ExportMode=Award';
}

function js_View_Class_Student_Award(jsClassID) {
	window.location = 'edit_award_student.php?ReportID=<?=$ReportID?>&YearClassID=' + jsClassID;
}

function js_View_Award_Student(jsAwardName) {
	window.location = 'edit_award_student.php?ReportID=<?=$ReportID?>&EditMode=Award&AwardName=' + jsAwardName;
}

function js_View_Award_Student_By_Generated_Result(jsAwardID, jsSubjectID) {
	window.location = 'edit_award_student.php?ReportID=<?=$ReportID?>&EditMode=Award&AwardID=' + jsAwardID + '&SubjectID=' + jsSubjectID;
}

//function goPrint() {
//	if ($('input.awardChk:checked').length == 0) {
//		alert(globalAlertMsg2);
//	}
//	else {
//		$('form#form1').attr('action', 'print_cert.php').attr('target', '_blank').submit().attr('action', '').attr('target', '_self');
//	}
//}
</script>
<br />
<br />
                        
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>