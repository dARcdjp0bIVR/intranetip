<?php
$PATH_WRT_ROOT = "../../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$plugin['ReportCard2008'])
{
	header ("Location: /");
	intranet_closedb();
	exit();
}
 
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
{
	include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
	$lreportcard = new libreportcardcustom();
} else {
	$lreportcard = new libreportcard();
}
include_once($PATH_WRT_ROOT."includes/libreportcard2008_award.php");

if (!$lreportcard->hasAccessRight()) 
{
	header ("Location: /");
	intranet_closedb();
	exit();
}

$ReportID = $_REQUEST['ReportID'];
$YearClassID = $_REQUEST['YearClassID'];
$ReturnMsgKey = $_REQUEST['ReturnMsgKey'];

$linterface = new interface_html();
$lreportcard_ui = new libreportcard_ui();
$lreportcard_award = new libreportcard_award();

$CurrentPage = "Reports_GenerateReport";
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();

# tag information
$TAGS_OBJ[] = array($eReportCard['Reports_GenerateReport'], "", 0);

$ReturnMsg = $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['ReturnMsgArr'][$ReturnMsgKey];
$linterface->LAYOUT_START($ReturnMsg);

# sub-tag information
echo $lreportcard_ui->Get_Generate_Award_SubTag($ReportID, $CurrentTag=2);
echo '<br />'."\n";
echo $lreportcard_ui->Get_View_Award_Student_UI($ReportID, $YearClassID);

?>

<script language="javascript">

$(document).ready( function() {
	js_Reload_Student_Table($('select#YearClassID').val());
});
	
function js_Go_Back() {
	window.location = 'view_award.php?ReportID=<?=$ReportID?>';
}

function js_Go_Back_To_Report_Generation() {
	window.location = '../index.php';
}

function js_Reload_Student_Table(jsYearClassID) {
	Block_Element('StudentTableDiv');
	$('div#StudentTableDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"ajax_reload.php", 
		{
			Action: 'View_Award_Student_Table',
			ReportID: '<?=$ReportID?>',
			YearClassID: jsYearClassID
		},
		function(ReturnData)
		{
			UnBlock_Element('StudentTableDiv');
		}
	);
}

function js_Save_Student_Award() {
	
}
</script>
<br />
<br />
                        
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>