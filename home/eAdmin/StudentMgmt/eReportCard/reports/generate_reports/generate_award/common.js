function js_Change_View_Mode(jsReportID, jsMode) {
	if (jsMode == 'Award') {
		window.location = 'view_award_award_mode.php?ReportID=' + jsReportID;
	}
	else if (jsMode == 'Class') {
		window.location = 'view_award_class_mode.php?ReportID=' + jsReportID;
	}
}

function js_Go_Back_To_Report_Generation() {
	window.location = '../index.php';
}