<?php
$PATH_WRT_ROOT = "../../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_award.php");

$lreportcard = new libreportcardcustom();
$lreportcard_award = new libreportcard_award();
$linterface = new interface_html();
$lreportcard_ui = new libreportcard_ui();

$lreportcard->hasAccessRight();

$ReportID = $_REQUEST['ReportID'];
$YearClassID = $_REQUEST['YearClassID'];
$ReturnMsgKey = $_REQUEST['ReturnMsgKey'];

# Current Page
$CurrentPage = "Reports_GenerateReport";
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();

# tag information
$TAGS_OBJ[] = array($eReportCard['Reports_GenerateReport'], "", 0);

$ReturnMsg = $Lang['General']['ReturnMessage'][$ReturnMsgKey];
$linterface->LAYOUT_START($ReturnMsg);

# sub-tag information
echo $lreportcard_ui->Get_Generate_Award_SubTag($ReportID, $CurrentTag=2);
echo '<br />'."\n";
echo $lreportcard_ui->Get_Import_Student_AwardText_Step1_UI($ReportID, $YearClassID);

?>

<script language="javascript" src="common.js"></script>
<script language="javascript">
$(document).ready( function() {
	
});

function js_Go_Export() {
	window.location = 'export_award.php?ReportID=<?=$ReportID?>&YearClassID=<?=$YearClassID?>&ExportMode=Class';
}

function js_Continue() {
	if ($('input#csvfile').val() == '') {
		alert('<?=$Lang['General']['warnSelectcsvFile']?>');
		$('input#csvfile').focus();
		return false;
	}
	
	$('form#form1').submit();
}

function js_Go_View_Index() {
	<? if ($YearClassID=='') { ?>
		window.location = 'view_award_class_mode.php?ReportID=<?=$ReportID?>&YearClassID=<?=$YearClassID?>';
	<? } else { ?>
		window.location = 'edit_award_student.php?ReportID=<?=$ReportID?>&YearClassID=<?=$YearClassID?>&EditMode=Class';
	<? } ?>
}

function getRemarkDivIdByRemarkType(remarkType) {
	return 'remarkDiv_' + remarkType;
}

function showRemarkLayer(remarkType) {
	var remarkBtnId = 'remarkBtn_' + remarkType;
	var remarkDivId = getRemarkDivIdByRemarkType(remarkType);
	var leftAdjustment = $('#' + remarkBtnId).width();
	var topAdjustment = 0;
	
	changeLayerPosition(remarkBtnId, remarkDivId, leftAdjustment, topAdjustment);
	hideAllRemarkLayer();
	MM_showHideLayers(remarkDivId, '', 'show');
}

function hideRemarkLayer(remarkType) {
	var remarkDivId = getRemarkDivIdByRemarkType(remarkType);
	MM_showHideLayers(remarkDivId, '', 'hide');
}

function hideAllRemarkLayer() {
	$('div.selectbox_layer').each( function() {
		MM_showHideLayers($(this).attr('id'), '', 'hide');
	});
}

</script>
<br />
<br />
                        
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>