<?php
// Using: Bill

#########################################
#
#	Date:	2016-08-09 (Bill)	[2016-0224-1423-31073]
#			display error message when conduct grade of import student received not high enough
#			($eRCTemplateSetting['Report']['ReportGeneration']['AwardGeneration_LimitByReceivedGrade'] = true)
#
#########################################

$PATH_WRT_ROOT = "../../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();
 
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_award.php");

$linterface = new interface_html();
$lreportcard_award = new libreportcard_award();
$lreportcard_award->hasAccessRight();

$Action = $_REQUEST['Action'];

if ($Action == 'Import_Student_AwardText') {
	include_once($PATH_WRT_ROOT."includes/libimporttext.php");
	include_once($PATH_WRT_ROOT."includes/libexporttext.php");
	include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
	
	$ReportID = $_REQUEST['ReportID'];
	$YearClassID = $_REQUEST['YearClassID'];
	$TargetFilePath = trim(urldecode(stripslashes($_REQUEST['TargetFilePath'])));
	
	$lexport = new libexporttext();
	$limport = new libimporttext();
	
	### Delete the temp data first
	$SuccessArr['DeleteOldTempData'] = $lreportcard_award->Delete_Import_Award_Temp_Data($ReportID);
	
	### Include js libraries
	$jsInclude = '';
	$jsInclude .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery-1.3.2.min.js"></script>';
	$jsInclude .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery.blockUI.js"></script>';
	$jsInclude .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/script.js"></script>';
	echo $jsInclude;
	
	
	### Get Csv data
	$DefaultCsvHeaderArr = $lreportcard_award->Get_Report_Award_Csv_Header_Title();
	$ColumnPropertyArr = $lreportcard_award->Get_Report_Award_Csv_Header_Property($forGetAllCsvContent=0);
	$CsvHeaderValidationArr = $lexport->GET_EXPORT_HEADER_COLUMN($DefaultCsvHeaderArr, $ColumnPropertyArr);
	$CsvHeaderDisplayArr = $lexport->GET_EXPORT_HEADER_COLUMN($DefaultCsvHeaderArr, $ColumnPropertyArr, $AddBracket=0);
	
	$ColumnPropertyAllInfoArr = $lreportcard_award->Get_Report_Award_Csv_Header_Property($forGetAllCsvContent=1);
	$CsvDataArr = $limport->GET_IMPORT_TXT_WITH_REFERENCE($TargetFilePath, "", "###linebreak###", $CsvHeaderValidationArr, $ColumnPropertyAllInfoArr);
	$CsvHeaderArr = array_shift($CsvDataArr);	// Remove Chinese Title
	$numOfCsvData = count($CsvDataArr);
	
	
	### Get Student Info Asso Array
	$ReportInfoArr = $lreportcard_award->returnReportTemplateBasicInfo($ReportID);
	$ClassLevelID = $ReportInfoArr['ClassLevelID'];
	$StudentInfoArr = $lreportcard_award->GET_STUDENT_BY_CLASSLEVEL('', $ClassLevelID, $YearClassID);
	
	# $ClassNameNumberAssoc[$ClassName][$ClassNumber] = $UserID
	$ClassNameNumberAssoArr = BuildMultiKeyAssoc($StudentInfoArr, array("ClassTitleEn", "ClassNumber"), "UserID", 1);
	# $UserLoginAssoc[$UserLogin] = $UserID
	$UserLoginAssoArr = BuildMultiKeyAssoc($StudentInfoArr, array("UserLogin"), "UserID", 1);
	# $WebSamsAssoc[$WebSAMSRegNo] = $UserID
	$WebSAMSRegNoAssoArr = BuildMultiKeyAssoc($StudentInfoArr, array("WebSAMSRegNo"), "UserID", 1);
		
	if ($eRCTemplateSetting['Report']['ReportGeneration']['AwardGeneration_ManageGeneratedResultOnly'])
	{
		# Get Award Info
		$ReportAwardAssoArr = $lreportcard_award->Get_Report_Award_Info($ReportID, $MapByCode=1);
		
		# Get Student Award Info
		$ReportAwardGeneratedInfoArr = $lreportcard_award->Get_Award_Generated_Student_Record($ReportID, $StudentIDArr='', $ReturnAsso=1, $AwardNameWithSubject=1);
		
		# Get Subject Info
		$libscm = new subject_class_mapping();
		$SubjectMapAssoArr = $libscm->Get_Subject_Code_ID_Mapping_Arr();
		
		# Get Form Subject Info
		$SubjectInfoArr = $lreportcard_award->returnSubjectwOrderNoL($ClassLevelID, $ParForSelection=0, $MainSubjectOnly=1, $ReportID);
		$FormSubjectIDArr = array_keys((array)$SubjectInfoArr);
		
		// [2016-0526-1018-39096]
		if($eRCTemplateSetting['Report']['ReportGeneration']['AwardGeneration_LimitByReceivedGrade'])
		{
			include_once($PATH_WRT_ROOT.'includes/libreportcard2008_extrainfo.php');
			$lreportcard_extrainfo = new libreportcard_extrainfo();
			
			// Get Report Semester
			$ReportSetting = $lreportcard_award->returnReportTemplateBasicInfo($ReportID);
			$SemID = $ReportSetting['Semester'];
			
			// Conduct List
			$ConductArr = $lreportcard_extrainfo->Get_Conduct();
			$ConductArr = BuildMultiKeyAssoc($ConductArr, "ConductID", "Conduct", 1);
			
			// Get Conduct
			if($SemID=="F")
			{
				include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
				$lreportcard_cust = new libreportcardcustom();
				
				// loop semester
				$SemesterList = getSemesters($lreportcard_cust->schoolYearID, 0);
				for($j=0; $j<3; $j++)
				{
					// Get Semester Report Info
					$thisSemester = $SemesterList[$j]["YearTermID"];
					$thisReport = $lreportcard_cust->returnReportTemplateBasicInfo("", "Semester='".$thisSemester."' and ClassLevelID = '".$ClassLevelID."' ");
					$thisReportID = $thisReport["ReportID"];
						
					// Get Conduct
					$StudentExtraInfo = $lreportcard_extrainfo->Get_Student_Extra_Info($thisReportID, $StudentIDArr);
					$StudentSemConduct = BuildMultiKeyAssoc($StudentExtraInfo, "StudentID", "ConductID", 1);
					$StudentConduct[$j] = $StudentSemConduct;
				}
			}
			else 
			{
				$StudentExtraInfo = $lreportcard_extrainfo->Get_Student_Extra_Info($ReportID, $StudentIDArr);
				$StudentConduct = BuildMultiKeyAssoc($StudentExtraInfo, "StudentID", "ConductID", 1);
			}
			
//			if ($ViewMode == 'Award')
//			{
//				$failConductStudentAry = array();
//				
//				$numOfTotalStudent = count($StudentIDArr);
//				for ($i=0; $i<$numOfTotalStudent; $i++) {
//					$thisStudentID = $StudentIDArr[$i];
//					
//					// Get Conduct
//					$thisStudentConductGrade = "";
//					if($SemID=="F")
//					{
//						// loop semester
//						$ConductDataAry = array();
//						for($j=0; $j<3; $j++)
//						{
//							$thisStudentConductID = $StudentConduct[$j][$thisStudentID];
//							$ConductDataAry[$j] = $ConductArr[$thisStudentConductID];
//						}
//						$ConductDataAry = array_filter((array)$ConductDataAry);
//						
//						// get overall conduct grade
//						if(!empty($ConductDataAry) && count($ConductDataAry)==3)
//							$thisStudentConductGrade = $lreportcard_cust->calculateOverallConduct($ConductDataAry, $ConductArr);
//					}
//					else
//					{
//						$thisStudentConductID = $StudentConduct[$thisStudentID];
//						$thisStudentConductGrade = $ConductArr[$thisStudentConductID];
//					}
//					$allowStudentAddAward = $thisStudentConductGrade!="" && (strpos($thisStudentConductGrade, "A")!==false || (strpos($thisStudentConductGrade, "B")!==false && $thisStudentConductGrade!="B-"));
//					
//					if(!$allowStudentAddAward)
//						$failConductStudentAry[] = $thisStudentID;
//				}
//			}
		}
	}
	
	
	### Variable Initialization
	$NumOfSuccessRow = 0;
	$NumOfErrorRow = 0;
	$ErrorRowInfoArr = array();		//$ErrorRowInfoArr[$RowNumber][ErrorField] = array(ErrorMsg)
	$InsertValueArr = array();
	$RowNumber = 3;
	for ($i=0; $i<$numOfCsvData; $i++)
	{
		$thisColumnCount = 0;
		$thisClassName = trim($CsvDataArr[$i][$thisColumnCount++]);
		$thisClassNumber = trim($CsvDataArr[$i][$thisColumnCount++]);
		$thisUserLogin = trim($CsvDataArr[$i][$thisColumnCount++]);
		$thisWebSAMSRegNo = trim($CsvDataArr[$i][$thisColumnCount++]);
		$thisStudentName = trim($CsvDataArr[$i][$thisColumnCount++]);
		
		if ($eRCTemplateSetting['Report']['ReportGeneration']['AwardGeneration_ManageGeneratedResultOnly']) {
			$thisAwardName = trim($CsvDataArr[$i][$thisColumnCount++]);
			$thisAwardCode = trim($CsvDataArr[$i][$thisColumnCount++]);
			$thisSubjectName = trim($CsvDataArr[$i][$thisColumnCount++]);
			$thisSubjectWebSAMSCode = trim($CsvDataArr[$i][$thisColumnCount++]);
		}
		else {
			$thisAwardText = str_replace("###linebreak###", "\r\n", trim($CsvDataArr[$i][$thisColumnCount++]));
		}
		
		
		$thisStudentID = $ClassNameNumberAssoArr[$thisClassName][$thisClassNumber];
		if ($thisStudentID == '') {
			$thisStudentID = $UserLoginAssoArr[$thisUserLogin];
			
			if ($thisStudentID == '') {
				$thisStudentID = $WebSAMSRegNoAssoArr[$thisWebSAMSRegNo];
				
				if ($thisStudentID == '') {
					$ErrorRowInfoArr[$RowNumber]['ClassName'] = 1;
					$ErrorRowInfoArr[$RowNumber]['ClassNumber'] = 1;
					$ErrorRowInfoArr[$RowNumber]['UserLogin'] = 1;
					$ErrorRowInfoArr[$RowNumber]['WebSAMSRegNo'] = 1;
					(array)$ErrorRowInfoArr[$RowNumber]['ErrorMsgArr'][] = '- '.$Lang['General']['ImportWarningArr']['WrongWebSamsRegNo'];
				}
			}
		}
		
		if ($eRCTemplateSetting['Report']['ReportGeneration']['AwardGeneration_ManageGeneratedResultOnly']) {
			$thisAwardID = $ReportAwardAssoArr[$thisAwardCode]['BasicInfo']['AwardID'];
			$thisAwardType = $ReportAwardAssoArr[$thisAwardCode]['BasicInfo']['AwardType'];
			$thisSubjectID = ($thisAwardType=='SUBJECT')? $SubjectMapAssoArr[$thisSubjectWebSAMSCode] : 0;
			
			// check award exist
			if ($thisAwardID == '') {
				$ErrorRowInfoArr[$RowNumber]['AwardName'] = 1;
				$ErrorRowInfoArr[$RowNumber]['AwardCode'] = 1;
				(array)$ErrorRowInfoArr[$RowNumber]['ErrorMsgArr'][] = '- '.$Lang['eReportCard']['ImportWarningArr']['award_not_found'];
			}
			
			// check subject exist
			if ($thisAwardType=='SUBJECT') {
				if ($thisSubjectID == '') {
					$ErrorRowInfoArr[$RowNumber]['SubjectName'] = 1;
					$ErrorRowInfoArr[$RowNumber]['SubjectWebSAMSCode'] = 1;
					(array)$ErrorRowInfoArr[$RowNumber]['ErrorMsgArr'][] = '- '.$Lang['eReportCard']['ImportWarningArr']['subject_not_found'];
				}
				else {
					// check if the subject is selected for this Form
					if (!in_array($thisSubjectID, $FormSubjectIDArr)) {
						$ErrorRowInfoArr[$RowNumber]['SubjectName'] = 1;
						$ErrorRowInfoArr[$RowNumber]['SubjectWebSAMSCode'] = 1;
						(array)$ErrorRowInfoArr[$RowNumber]['ErrorMsgArr'][] = '- '.$Lang['eReportCard']['ImportWarningArr']['form_subject_not_found'];
					}
				}
			}
			
			// check if the student has get this award already
			if (is_array($ReportAwardGeneratedInfoArr[$thisStudentID][$thisAwardID][$thisSubjectID])) {
				$ErrorRowInfoArr[$RowNumber]['SubjectName'] = 1;
				$ErrorRowInfoArr[$RowNumber]['SubjectWebSAMSCode'] = 1;
				$ErrorRowInfoArr[$RowNumber]['SubjectName'] = 1;
				$ErrorRowInfoArr[$RowNumber]['SubjectWebSAMSCode'] = 1;
				(array)$ErrorRowInfoArr[$RowNumber]['ErrorMsgArr'][] = '- '.$Lang['eReportCard']['ImportWarningArr']['student_has_award_already'];
			}
						
			// [2016-0526-1018-39096] check if can add award for student
			if($eRCTemplateSetting['Report']['ReportGeneration']['AwardGeneration_LimitByReceivedGrade'])
			{
				// Get Conduct
				$thisStudentConductGrade = "";
				$allowStudentAddAward = true;
				if($SemID=="F")
				{
					// loop semester
					$ConductDataAry = array();
					for($j=0; $j<3; $j++)
					{
						$thisStudentConductID = $StudentConduct[$j][$thisStudentID];
						$ConductDataAry[$j] = $ConductArr[$thisStudentConductID];
					}
					$ConductDataAry = array_filter((array)$ConductDataAry);
					
					// get overall conduct grade
					if(!empty($ConductDataAry) && count($ConductDataAry)==3)
						$thisStudentConductGrade = $lreportcard_cust->calculateOverallConduct($ConductDataAry, $ConductArr);
				}
				else
				{
					$thisStudentConductID = $StudentConduct[$thisStudentID];
					$thisStudentConductGrade = $ConductArr[$thisStudentConductID];
				}
				$allowStudentAddAward = $thisStudentConductGrade!="" && (strpos($thisStudentConductGrade, "A")!==false || (strpos($thisStudentConductGrade, "B")!==false && $thisStudentConductGrade!="B-"));
				
				if(!$allowStudentAddAward)
				{
					$ErrorRowInfoArr[$RowNumber]['ClassName'] = 1;
					$ErrorRowInfoArr[$RowNumber]['ClassNumber'] = 1;
					$ErrorRowInfoArr[$RowNumber]['UserLogin'] = 1;
					$ErrorRowInfoArr[$RowNumber]['WebSAMSRegNo'] = 1;
					(array)$ErrorRowInfoArr[$RowNumber]['ErrorMsgArr'][] = '- '.$Lang['eReportCard']['WarningArr']['ConductDisplay']['RejectAwardWhenFailConduct'];
				}
			}
		}
		
		### Insert DB Statement
		$thisClassName = $lreportcard_award->Get_Safe_Sql_Query($thisClassName);
		$thisUserLogin = $lreportcard_award->Get_Safe_Sql_Query($thisUserLogin);
		$thisStudentName = $lreportcard_award->Get_Safe_Sql_Query($thisStudentName);
		$thisAwardText = $lreportcard_award->Get_Safe_Sql_Query($thisAwardText);
		$thisAwardName = $lreportcard_award->Get_Safe_Sql_Query($thisAwardName);
		$thisAwardCode = $lreportcard_award->Get_Safe_Sql_Query($thisAwardCode);
		$thisSubjectName = $lreportcard_award->Get_Safe_Sql_Query($thisSubjectName);
		$thisSubjectWebSAMSCode = $lreportcard_award->Get_Safe_Sql_Query($thisSubjectWebSAMSCode);
		
		$InsertValueArr[] = "('".$_SESSION['UserID']."', '".$RowNumber."', '".$ReportID."', '".$thisClassName."', '".$thisClassNumber."', '".$thisUserLogin."', '".$thisWebSAMSRegNo."', '".$thisStudentID."', '".$thisStudentName."', 
								'".$thisAwardText."', '".$thisAwardName."', '".$thisAwardCode."', '".$thisAwardID."', '".$thisSubjectName."', '".$thisSubjectWebSAMSCode."', '".$thisSubjectID."', now())";
		
		
		### Count success / failed records	
		if (!isset($ErrorRowInfoArr[$RowNumber])) {
			$NumOfSuccessRow++;
		}
		else {
			$NumOfErrorRow++;
		}
		
			
		### Update Processing Display
		$thisJSUpdate = '';
		$thisJSUpdate .= '<script language="javascript">'."\n";
			$thisJSUpdate .= 'window.parent.document.getElementById("SuccessCountDiv").innerHTML = "'.$NumOfSuccessRow.'";';
			$thisJSUpdate .= 'window.parent.document.getElementById("FailCountDiv").innerHTML = "'.$NumOfErrorRow.'";';
			$thisJSUpdate .= 'window.parent.document.getElementById("BlockUI_Processed_Number_Span").innerHTML = "'.($RowNumber - 2).'";';
		$thisJSUpdate .= '</script>'."\n";
		echo $thisJSUpdate;
		Flush_Screen_Display(1);
		
		$RowNumber++;
	}
	
	
	### Insert the temp records into the temp table
	if (count($InsertValueArr) > 0) {
		$SuccessArr['InsertTempRecords'] = $lreportcard_award->Insert_Import_Award_Temp_Data($InsertValueArr);
	}
	
	
	### Display Error Result Table if there are any
	$numOfErrorRow = count($ErrorRowInfoArr);
	if($numOfErrorRow > 0)
	{
		### Update Processing Display
		$thisJSUpdate = '';
		$thisJSUpdate .= '<script language="javascript">'."\n";
			$thisJSUpdate .= 'window.parent.document.getElementById("BlockUI_Progress_Span").innerHTML = "'.$Lang['SysMgr']['SubjectClassMapping']['GeneratingErrorsInfo'].'...";';
		$thisJSUpdate .= '</script>'."\n";
		echo $thisJSUpdate;
		Flush_Screen_Display(1);
		
		
		### Get data of the error rows
		$ErrorRowArr = array_keys($ErrorRowInfoArr);
		$ErrorRowDataArr = $lreportcard_award->Get_Import_Award_Temp_Data($ReportID, $ErrorRowArr);
			
		
		### Build Error Info Table
		$CsvHeaderDisplayArr = Get_Lang_Selection($CsvHeaderDisplayArr[1], $CsvHeaderDisplayArr[0]);
		$numOfCsvHeader = count($CsvHeaderDisplayArr);
		$x .= '<table class="common_table_list_v30 view_table_list_v30">';
			$x .= '<thead>';
				$x .= '<tr>';
					$x .= '<th width="10">'.$Lang['General']['ImportArr']['Row'].'</th>';
					for ($i=0; $i<$numOfCsvHeader; $i++)
					{
						$thisDisplay = $CsvHeaderDisplayArr[$i];					
						$x .= '<th>'.$thisDisplay.'</th>';
					}
					$x .= '<th>'.$Lang['General']['Remark'].'</th>';
				$x .= '</tr>';
			$x .= '</thead>';
			
			$x .= '<tbody>';
				for ($i=0; $i<$numOfErrorRow; $i++)
				{
					$_RowNumber = $ErrorRowDataArr[$i]['RowNumber'];
					$_ClassName = $ErrorRowDataArr[$i]['ClassName'];
					$_ClassNumber = $ErrorRowDataArr[$i]['ClassNumber'];
					$_UserLogin = $ErrorRowDataArr[$i]['UserLogin'];
					$_WebSAMSRegNo = $ErrorRowDataArr[$i]['WebSAMSRegNo'];
					$_StudentName = $ErrorRowDataArr[$i]['StudentName'];
					$_AwardText = str_replace("\r\n", '<br />', $ErrorRowDataArr[$i]['AwardText']);
					$_AwardName = $ErrorRowDataArr[$i]['AwardName'];
					$_AwardCode = $ErrorRowDataArr[$i]['AwardCode'];
					$_SubjectName = $ErrorRowDataArr[$i]['SubjectName'];
					$_SubjectWebSAMSCode = $ErrorRowDataArr[$i]['SubjectWebSAMSCode'];
					
					# Check Empty
					$_AwardCode = $_AwardCode ? $_AwardCode : '<font color="red">***</font>';
					
					$_ColumnCount = 0;
					
					# Error Remarks
					$_ErrorInfoArr = $ErrorRowInfoArr[$_RowNumber];
					$_ErrorDisplayArr = $_ErrorInfoArr['ErrorMsgArr'];
					$_ErrorDisplayText = implode('<br />', (array)$_ErrorDisplayArr);
					
					$css_i = ($i % 2) ? "2" : "";
					$x .= '<tr style="vertical-align:top">';
						$x .= '<td>'.$_RowNumber.'</td>';
						
						// Class Name
						$_ClassName = ($_ErrorInfoArr['ClassName']==1)? '<font color="red">'.$_ClassName.'</font>' : $_ClassName;
						$x .= '<td>'.$_ClassName.'</td>';
						
						// Class Number
						$_ClassNumber = ($_ErrorInfoArr['ClassNumber']==1)? '<font color="red">'.$_ClassNumber.'</font>' : $_ClassNumber;
						$x .= '<td>'.$_ClassNumber.'</td>';
						
						// User Login
						$_UserLogin = ($_ErrorInfoArr['UserLogin']==1)? '<font color="red">'.$_UserLogin.'</font>' : $_UserLogin;
						$x .= '<td>'.$_UserLogin.'</td>';
						
						// WebSAMSRegNo
						$_WebSAMSRegNo = ($_ErrorInfoArr['WebSAMSRegNo']==1)? '<font color="red">'.$_WebSAMSRegNo.'</font>' : $_WebSAMSRegNo;
						$x .= '<td>'.$_WebSAMSRegNo.'</td>';
						
						// Student Name
						$x .= '<td>'.$_StudentName.'</td>';
						
						if ($eRCTemplateSetting['Report']['ReportGeneration']['AwardGeneration_ManageGeneratedResultOnly']) {
							// Award Name
							$x .= '<td>'.$_AwardName.'</td>';
							
							// Award Code
							$x .= '<td>'.$_AwardCode.'</td>';
							
							// Subject Name
							$x .= '<td>'.$_SubjectName.'</td>';
							
							// Subject WebSAMSCode
							$x .= '<td>'.$_SubjectWebSAMSCode.'</td>';
						}
						else {
							// Award Text
							$x .= '<td>'.$_AwardText.'</td>';
						}
						
						// Error Details
						$x .= '<td><font color="red">'.$_ErrorDisplayText.'</font></td>';
					$x .= '</tr>';
				}
			$x .= '</tbody>';
		$x .= '</table>';
	}

	
	$ErrorCountDisplay = ($NumOfErrorRow > 0) ? "<font color=\"red\">".$NumOfErrorRow."</font>" : $NumOfErrorRow;
	$thisJSUpdate = '';
	$thisJSUpdate .= '<script language="javascript">'."\n";
		$thisJSUpdate .= 'window.parent.document.getElementById("ErrorTableDiv").innerHTML = \''.$x.'\';';
		$thisJSUpdate .= 'window.parent.document.getElementById("SuccessCountDiv").innerHTML = \''.$NumOfSuccessRow.'\';';
		$thisJSUpdate .= 'window.parent.document.getElementById("FailCountDiv").innerHTML = \''.$ErrorCountDisplay.'\';';
		
		if ($NumOfErrorRow == 0) {
			$thisJSUpdate .= '$("input#ContinueBtn", window.parent.document).removeClass("formbutton_disable").addClass("formbutton").attr("disabled", "");';
		}
		
		$thisJSUpdate .= 'window.parent.UnBlock_Document();'."\n";
	$thisJSUpdate .= '</script>'."\n";
	echo $thisJSUpdate;
	
}
else if ($Action == 'Get_Award_AwardType') {
	$AwardID = $_POST['AwardID'];
	$AwardInfoArr = $lreportcard_award->Get_Award_Info($AwardID);
	
	echo $AwardInfoArr['AwardType'];
}


intranet_closedb();
?>