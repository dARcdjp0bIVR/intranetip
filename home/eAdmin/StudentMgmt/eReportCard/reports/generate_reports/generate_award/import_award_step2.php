<?php
$PATH_WRT_ROOT = "../../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_award.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

$lreportcard_award = new libreportcard_award();
$linterface = new interface_html();
$lreportcard_ui = new libreportcard_ui();
$libimport = new libimporttext();
$libfs = new libfilesystem();

$lreportcard_award->hasAccessRight();

$ReportID = $_REQUEST['ReportID'];
$YearClassID = $_REQUEST['YearClassID'];
$ReturnMsgKey = $_REQUEST['ReturnMsgKey'];

$name = $_FILES['csvfile']['name'];
$ReturnPath = 'import_award_step1.php?ReportID='.$ReportID.'&YearClassID='.$YearClassID;

if (!$libimport->CHECK_FILE_EXT($name))
{
	intranet_closedb();
	header('location: '.$ReturnPath.'&ReturnMsgKey=WrongFileFormat');
	exit();
}

### move to temp folder first for others validation
$TargetFilePath = $libfs->Copy_Import_File_To_Temp_Folder('reportcard/report_award/', $csvfile, $name);


### Validate file header format
$DefaultCsvHeaderArr = $lreportcard_award->Get_Report_Award_Csv_Header_Title();
$DefaultCsvHeaderArr = $DefaultCsvHeaderArr['En'];
$ColumnPropertyArr = $lreportcard_award->Get_Report_Award_Csv_Header_Property($forGetAllCsvContent=1);

$CsvData = $libimport->GET_IMPORT_TXT_WITH_REFERENCE($TargetFilePath, "", "", $DefaultCsvHeaderArr, $ColumnPropertyArr);
$CsvHeaderArr = array_shift($CsvData);
$numOfCsvData = count($CsvData);

$CsvHeaderWrong = false;
for($i=0; $i<count($DefaultCsvHeaderArr); $i++) {
	if ($CsvHeaderArr[$i] != $DefaultCsvHeaderArr[$i]) {
		$CsvHeaderWrong = true;
		break;
	}
}

if($CsvHeaderWrong || $numOfCsvData==0)
{
	$ReturnMsgKey = ($CsvHeaderWrong)? 'WrongCSVHeader' : 'CSVFileNoData';
	intranet_closedb();
	header('location: '.$ReturnPath.'&ReturnMsgKey='.$ReturnMsgKey);
	exit();
}




# Current Page
$CurrentPage = "Reports_GenerateReport";
$MODULE_OBJ = $lreportcard_award->GET_MODULE_OBJ_ARR();

# tag information
$TAGS_OBJ[] = array($eReportCard['Reports_GenerateReport'], "", 0);

$ReturnMsg = $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['ReturnMsgArr'][$ReturnMsgKey];
$linterface->LAYOUT_START($ReturnMsg);

# sub-tag information
echo $lreportcard_ui->Get_Generate_Award_SubTag($ReportID, $CurrentTag=2);
echo '<br />'."\n";
echo $lreportcard_ui->Get_Import_Student_AwardText_Step2_UI($ReportID, $YearClassID, $TargetFilePath, $numOfCsvData);



### Thickbox loading message
$h_ProgressMsg = $linterface->Get_Import_Progress_Msg_Span(0, $numOfCsvData, $Lang['General']['ImportArr']['RecordsValidated']);

?>

<script language="javascript" src="common.js"></script>
<script language="javascript">
$(document).ready( function() {
	Block_Document('<?=$h_ProgressMsg?>');
});

function js_Back_To_Import_Step1() {
	window.location = 'import_award_step1.php?ReportID=<?=$ReportID?>&YearClassID=<?=$YearClassID?>';
}

function js_Continue() {
	$('form#form1').submit();
}

function js_Go_View_Index() {
	<? if ($YearClassID=='') { ?>
		window.location = 'view_award_class_mode.php?ReportID=<?=$ReportID?>&YearClassID=<?=$YearClassID?>';
	<? } else { ?>
		window.location = 'edit_award_student.php?ReportID=<?=$ReportID?>&YearClassID=<?=$YearClassID?>&EditMode=Class';
	<? } ?>
}

</script>
<br />
<br />
                        
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>