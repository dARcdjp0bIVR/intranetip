<?php
$PATH_WRT_ROOT = "../../../../../../../";
$PageRight = "ADMIN";
 
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_award.php");

$lreportcard = new libreportcardcustom();
$linterface = new interface_html();
$lreportcard_ui = new libreportcard_ui();
$lreportcard_award = new libreportcard_award();

$lreportcard->hasAccessRight();

$ReportID = $_REQUEST['ReportID'];
$ReturnMsgKey = $_REQUEST['ReturnMsgKey'];


$CurrentPage = "Reports_GenerateReport";
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();

# tag information
$TAGS_OBJ[] = array($eReportCard['Reports_GenerateReport'], "", 0);

$ReturnMsg = $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['ReturnMsgArr'][$ReturnMsgKey];
$linterface->LAYOUT_START($ReturnMsg);

# sub-tag information
echo $lreportcard_ui->Get_Generate_Award_SubTag($ReportID, $CurrentTag=1);
echo '<br />'."\n";
echo $lreportcard_ui->Get_Generate_Award_UI($ReportID);

?>

<script language="javascript" src="common.js"></script>
<script language="javascript">
$(document).ready( function() {
	// Initialize the Improvement Unit	
	$('span.ImprovementUnitSpan').each( function() {
		var jsThisSpanID = $(this).attr('id');
		var jsThisIDPieces = jsThisSpanID.split('_');
		var jsThisAwardID = jsThisIDPieces[1];
		var jsThisCriteriaID = jsThisIDPieces[2];
		var jsThisTargerImprovementFieldSelID = 'ImprovementFieldSel_' + jsThisAwardID + '_' + jsThisCriteriaID;
		var jsThisImprovementField = $('select#' + jsThisTargerImprovementFieldSelID).val();
		
		js_Update_Improvement_Unit_Display(jsThisAwardID, jsThisCriteriaID, jsThisImprovementField);
	})
	
	js_Reload_Report_Last_Generate_Award_Display();
});

function js_Changed_FromRank_Selection(jsAwardID, jsCriteriaID) {
	var jsFromRankSelID = 'FromRankSel_' + jsAwardID + '_' + jsCriteriaID;
	var jsToRankSelID = 'ToRankSel_' + jsAwardID + '_' + jsCriteriaID;
	
	var jsFromRankValue = parseInt($('select#' + jsFromRankSelID).val());
	var jsToRankValue = parseInt($('select#' + jsToRankSelID).val());
	
	if (jsFromRankValue > jsToRankValue) {
		$('select#' + jsToRankSelID).val(jsFromRankValue);
	}
}

function js_Changed_ToRank_Selection(jsAwardID, jsCriteriaID) {
	var jsFromRankSelID = 'FromRankSel_' + jsAwardID + '_' + jsCriteriaID;
	var jsToRankSelID = 'ToRankSel_' + jsAwardID + '_' + jsCriteriaID;
	
	var jsFromRankValue = parseInt($('select#' + jsFromRankSelID).val());
	var jsToRankValue = parseInt($('select#' + jsToRankSelID).val());
	
	if (jsToRankValue < jsFromRankValue) {
		$('select#' + jsFromRankSelID).val(jsToRankValue);
	}
}

// jsType = "From" / "To"
function js_Changed_Report_Selection(jsReportID, jsAwardID, jsCriteriaID, jsType) {
	var jsTargetSpanID = jsType + 'ReportColumnSelSpan_' + jsAwardID + '_' + jsCriteriaID;
	var jsSelectionID = 'AwardPageInfoArr[' + jsAwardID + '][' + jsCriteriaID + '][' + jsType + 'ReportID]';
	
	$('span#' + jsTargetSpanID).html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"../../../ajax_reload.php", 
		{
			Action: 'ReportColumn_Selection',
			ReportID: jsReportID,
			Selected: '',
			SelectionID: jsSelectionID
		},
		function(ReturnData)
		{
			
		}
	);
}

function js_Changed_Improvement_Field_Selection(jsAwardID, jsCriteriaID, jsImprovementField) {
	js_Update_Improvement_Unit_Display(jsAwardID, jsCriteriaID, jsImprovementField);
}

function js_Update_Improvement_Unit_Display(jsAwardID, jsCriteriaID, jsImprovementField) {
	var jsUnitSpanID = 'ImprovementUnitSpan_' + jsAwardID + '_' + jsCriteriaID;
	var jsUnitType = js_Get_Improvement_Unit_Type(jsImprovementField);
	switch (jsUnitType)
	{
		case 'Mark':
			jsUnit = '<?=$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['Mark(s)']?>';
		break;
		case 'Rank':
			jsUnit = '<?=$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['Rank(s)']?>';
		break;
		default:
			jsUnit = '';
	}
	
	$('span#' + jsUnitSpanID).html(jsUnit);
}

function js_Get_Improvement_Unit_Type(jsImprovementField) {
	var jsUnitType;
	
	switch (jsImprovementField)
	{
		case 'Mark':
		case 'SDScore':
		case 'GrandAverage':
		case 'GrandTotal':
		case 'GrandGPA':
		case 'GrandSDScore':
			jsUnitType = 'Mark';
		break;
		case 'OrderMeritClass':
		case 'OrderMeritForm':
		case 'OrderMeritSubjectGroup':
			jsUnitType = 'Rank';
		break;
		default:
			jsUnitType = '';
	}
	
	return jsUnitType;
}

function js_Generate_Award() {
	
	$('div.WarningMsg').hide();
	
	// FromRank and ToRank Range checking
	var jsCanGenerateAward = true;
	$('select.FromRank').each( function() {
		// Find the corresponding ToRank Selection
		var jsThisFromRankSelID = $(this).attr('id');
		var jsThisFromRankSelIDPieces = jsThisFromRankSelID.split('_');
		var jsThisAwardID = jsThisFromRankSelIDPieces[1];
		var jsThisCriteriaID = jsThisFromRankSelIDPieces[2];
		var jsThisToRankSelID = 'ToRankSel_' + jsThisAwardID + '_' + jsThisCriteriaID;
		
		// Compare the value - FromRank must be smaller than / equal to the ToRank
		var jsThisFromRank = $(this).val();
		var jsThisToRank = $('select#' + jsThisToRankSelID).val();
		if (parseInt(jsThisFromRank) > parseInt(jsThisToRank)) {
			jsCanGenerateAward = false;
			
			var jsThisTargetWarningMsgDivID = 'RankWarning_' + jsThisAwardID + '_' + jsThisCriteriaID;
			$('div#' + jsThisTargetWarningMsgDivID).show();
			$(this).focus();
		}
	});
	
	// Positive Integer / Float checking for the Min Improvement
	$('input.MinImprovementTb').each( function() {
		// Find the corresponding Improvement Field
		var jsThisTbID = $(this).attr('id');
		var jsThisIDPieces = jsThisTbID.split('_');
		var jsThisAwardID = jsThisIDPieces[1];
		var jsThisCriteriaID = jsThisIDPieces[2];
		var jsThisImprovementFieldSelID = 'ImprovementFieldSel_' + jsThisAwardID + '_' + jsThisCriteriaID;
		var jsThisImprovementField = $('select#' + jsThisImprovementFieldSelID).val();
		var jsUnitType = js_Get_Improvement_Unit_Type(jsThisImprovementField);
		
		var jsThisMinImprovement = $(this).val();
		if (jsUnitType == 'Mark') {
			// check if the mark is a float
			if (!IsNumeric(jsThisMinImprovement) || parseInt(jsThisMinImprovement) < 0) {
				jsCanGenerateAward = false;
			
				var jsThisTargetWarningMsgDivID = 'MinImprovement_Mark_' + jsThisAwardID + '_' + jsThisCriteriaID;
				$('div#' + jsThisTargetWarningMsgDivID).show();
				$(this).focus();
			}
		}
		else if (jsUnitType == 'Rank') {
			// check if the rank is an integer
			if (!isInteger(jsThisMinImprovement) || parseInt(jsThisMinImprovement) < 0) {
				jsCanGenerateAward = false;
			
				var jsThisTargetWarningMsgDivID = 'MinImprovement_Rank_' + jsThisAwardID + '_' + jsThisCriteriaID;
				$('div#' + jsThisTargetWarningMsgDivID).show();
				$(this).focus();
			}
		}
	});
	
	if (jsCanGenerateAward) {
		if (confirm('<?=$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['WarningMsgArr']['GenerateAward']?>')) {
			Block_Document('<?=$linterface->Get_Ajax_Loading_Image($noLang=0, $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['GeneratingAward'])?>');
			var jsSubmitString = Get_Form_Values(document.getElementById('form1'));
			$.ajax({  
				type: "POST",  
				url: "generate_award_update.php",
				data: jsSubmitString,  
				success: function(data) {
					if (data=='1') {
						Get_Return_Message('<?=$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['ReturnMsgArr']['AwardGenerateSuccess']?>');
						js_Reload_Report_Last_Generate_Award_Display();
					}
					else {
						Get_Return_Message('<?=$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['ReturnMsgArr']['AwardGenerateFailed']?>');
					}
					UnBlock_Document();
					Scroll_To_Top();
				} 
			});
		}
	}
}

function js_Reload_Report_Last_Generate_Award_Display() {
	$('span#LastGeneratedSpan').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"ajax_reload.php", 
		{
			Action: 'Report_Award_Last_Generated_Display',
			ReportID: '<?=$ReportID?>'
		},
		function(ReturnData)
		{
		
		}
	);
}
</script>
<br />						
<br />                        
                        
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>