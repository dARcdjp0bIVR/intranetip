<?php
$PATH_WRT_ROOT = "../../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();
 
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_award.php");

$linterface = new interface_html();
$lreportcard_award = new libreportcard_award();
$lreportcard_award->hasAccessRight();

$Action = $_REQUEST['Action'];
if ($Action == 'Import_Student_AwardText') {
	$ReportID = $_REQUEST['ReportID'];
	
	$lreportcard_award->Start_Trans();
	
	$jsInclude = '';
	$jsInclude .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery-1.3.2.min.js"></script>';
	$jsInclude .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery.blockUI.js"></script>';
	$jsInclude .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/script.js"></script>';
	echo $jsInclude;
	
	
	### Get Import Details
	$ImportColumnInfoArr = $lreportcard_award->Get_Report_Award_Csv_Header_Info();
	$numOfDBColumn = count($ImportColumnInfoArr);
	$ImportDataArr = $lreportcard_award->Get_Import_Award_Temp_Data($ReportID);
	$numOfImportData = count($ImportDataArr);
	
	
	### Get Current AwardText Record for insert / update checking
	// $AwardTextInfoAssoArr[$ReportID][$StudentID]['RecordID', ...] = Value
	$AwardTextInfoAssoArr = $lreportcard_award->Get_Award_Student_Record($ReportID, $StudentIDArr='', $ReturnAsso=1);
	
	
	$DataArr = array();
	for ($i=0; $i<$numOfImportData; $i++)
	{
		$thisTempID = $ImportDataArr[$i]['TempID'];
		$thisRowNumber = $ImportDataArr[$i]['RowNumber'];
		$thisStudentID = $ImportDataArr[$i]['StudentID'];
		$thisAwardText = $ImportDataArr[$i]['AwardText'];
		$thisAwardID = $ImportDataArr[$i]['AwardID'];
		$thisSubjectID = $ImportDataArr[$i]['SubjectID'];
		$thisRecordID = $AwardTextInfoAssoArr[$ReportID][$thisStudentID]['RecordID'];
		
		$thisDataArr = array();
		if ($eRCTemplateSetting['Report']['ReportGeneration']['AwardGeneration_ManageGeneratedResultOnly']) {
			$thisDataArr[0]['StudentID'] = $thisStudentID;
			$thisDataArr[0]['SubjectID'] = $thisSubjectID;
			$thisDataArr[0]['AwardRank'] = $lreportcard_award->Get_Award_Generated_Student_Record_Max_AwardRank($ReportID, $thisAwardID, $thisSubjectID);
			
			$SuccessArr['UpdateAwardGenerateRecord'][] = $lreportcard_award->Update_Award_Generated_Student_Record($thisAwardID, $ReportID, $thisDataArr);
		}
		else {
			if ($thisRecordID == '') {
				$thisDataArr['StudentID'] = $thisStudentID;
			}
			else {
				$thisDataArr['RecordID'] = $thisRecordID;
			}
			$thisDataArr['AwardText'] = $thisAwardText;
		}
		
		$DataArr[] = $thisDataArr;
		
		$thisJSUpdate = '';
		$thisJSUpdate .= '<script language="javascript">'."\n";
			$thisJSUpdate .= 'window.parent.document.getElementById("BlockUI_Processed_Number_Span").innerHTML = "'.($i + 1).'";';
		$thisJSUpdate .= '</script>'."\n";
		echo $thisJSUpdate;
		
		Flush_Screen_Display(1);
	}
	
	if ($eRCTemplateSetting['Report']['ReportGeneration']['AwardGeneration_ManageGeneratedResultOnly']) {
		// do nth
	}
	else {
		$SuccessArr['UpdateAwardText'] = $lreportcard_award->Update_Award_Student_Record($ReportID, $DataArr, $UpdateLastModified=1);
	}
	
	
	### Delete the temp data first
	$SuccessArr['DeleteTempData'] = $lreportcard_award->Delete_Import_Award_Temp_Data($ReportID);
	
	
	### Roll Back data if import failed
	if (in_multi_array(false, $SuccessArr))
	{
		$lreportcard_award->RollBack_Trans();
		$ImportStatus = $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['ImportStudentAwardFailed'];
	}
	else
	{
		$lreportcard_award->Commit_Trans();
		$ImportStatus = $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['ImportStudentAwardSuccess'];
	}
	
	$thisJSUpdate = '';
	$thisJSUpdate .= '<script language="javascript">'."\n";
		$thisJSUpdate .= 'window.parent.document.getElementById("ImportStatusSpan").innerHTML = "'.$ImportStatus.'";';
		$thisJSUpdate .= 'window.parent.UnBlock_Document();'."\n";
	$thisJSUpdate .= '</script>'."\n";
	echo $thisJSUpdate;
}
else if ($Action == 'Delete_Generated_Award_Record') {
	$RecordID = $_POST['RecordID'];
	$Success = $lreportcard_award->Delete_Award_Generated_Student_Record($ReportIDArr='', $RecordID);
	
	echo $Success? '1' : '0';
}
else if ($Action == 'Add_Generated_Award_Record') {
	$StudentID = $_POST['StudentID'];
	$ReportID = $_POST['ReportID'];
	$AwardID = $_POST['AwardID'];
	$SubjectID = $_POST['SubjectID'];
	
	if ($StudentID != '') {
		$StudentIDArr = array($StudentID);
	}
	else {
		$StudentIDArr = explode(',', $_POST['StudentIDList']);
	}
	$numOfStudent = count($StudentIDArr);
	
	$MaxAwaradRank = $lreportcard_award->Get_Award_Generated_Student_Record_Max_AwardRank($ReportID, $AwardID, $SubjectID);
	
	$DataArr = array();
	for ($i=0; $i<$numOfStudent; $i++) {
		$thisStudentID = $StudentIDArr[$i];
		
		$DataArr[$i]['StudentID'] = $thisStudentID;
		$DataArr[$i]['SubjectID'] = $SubjectID;
		$DataArr[$i]['AwardRank'] = ++$MaxAwaradRank;
	}
	$Success = $lreportcard_award->Update_Award_Generated_Student_Record($AwardID, $ReportID, $DataArr);
	
	echo $Success? '1' : '0';
}
else if ($Action == 'Reorder_AwardRank') {
	$DisplayOrderString = $_POST['DisplayOrderString'];
	
	$RecordIDArr = explode(',', $DisplayOrderString);
	$numOfRecord = count($RecordIDArr);
	
	$lreportcard_award->Start_Trans();
	
	$SuccessArr = array();
	for ($i=0; $i<$numOfRecord; $i++) {
		$thisRecordID = $RecordIDArr[$i];
		$thisAwardRank = $i + 1;
		
		$SuccessArr[$thisRecordID] = $lreportcard_award->Update_Award_Generated_Student_AwardRank($thisRecordID, $thisAwardRank);
	}
	
	if (in_array(false, $SuccessArr)) {
		$lreportcard_award->RollBack_Trans();
		echo '0';
	}
	else {
		$lreportcard_award->Commit_Trans();
		echo '1';
	}
}

intranet_closedb();
?>