<?
// Edited by 

/*
 * 20170808 Bill [2017-0808-1102-23206]
 * 	- fixed cannot get semester name due to passing an array into function
 */

$PageRight = "ADMIN";

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

intranet_auth();
intranet_opendb();

$viewFormat = standardizeFormPostValue($_POST['ViewFormat']);
$yearTermId = standardizeFormPostValue($_POST['TermID']);
$pastYearTermId = standardizeFormPostValue($_POST['PastTermID']);
$numOfTargetYear = IntegerSafe($_POST['numOfTargetYear']);
$targetIdAry = $_POST['TargetID'];
$numOfTarget = count($targetIdAry);
//$level = standardizeFormPostValue($_POST['Level']);
$level = 'Form';

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");

$lreportcard = new libreportcardcustom();
$lreportcard->hasAccessRight();

function convertSubjectIdForAcademicYear($parAcademicYear, $parSubjectId) {
	$targetSubjectId = $parSubjectId;
	if ($parAcademicYear <= '2013') {
		if ($parSubjectId==368) {	// 080A
			$targetSubjectId = 255;		// 080
		}
	}
	
	return $targetSubjectId;
} 

$fcm = new form_class_manage();

// Get 3 latest Academic Years
$eRCActiveAcademicYearId = $lreportcard->GET_ACTIVE_YEAR_ID();
$academicYearAry = $fcm->Get_Academic_Year_List('', $OrderBySequence=false, $excludeYearIDArr=array(), $NoPastYear=0, $PastAndCurrentYearOnly=0, $ExcludeCurrentYear=0, $eRCActiveAcademicYearId, $SortOrder='');
$targetAcademicYearAry = array();
for ($i=0; $i<$numOfTargetYear; $i++) {
	$targetAcademicYearAry[] = $academicYearAry[$i];
}
$targetAcademicYearAry = array_reverse($targetAcademicYearAry);

// Get Target Term Sequence
$pastYearTermNum = $lreportcard->Get_Semester_Seq_Number($pastYearTermId);

$lreportcardAry = array();
$reportAssoAry = array();
for ($i=0; $i<$numOfTargetYear; $i++) {
	$_academicYearId = $targetAcademicYearAry[$i]['AcademicYearID'];
	
	// Get Target Year reportcard object
	if ($_academicYearId == $lreportcard->GET_ACTIVE_YEAR_ID()) {
		$lreportcardAry[$_academicYearId] = $lreportcard;
	}
	else {
		$lreportcardAry[$_academicYearId] = new libreportcard($_academicYearId);
	}
	
	// Get Target Term in this Academic Year
	//	- Current Year => from selected Term
	//	- Other Years  => from Target Term Sequence / Consolidated Report 
	$_targetYearTermId = '';
	if ($_academicYearId == $lreportcard->GET_ACTIVE_YEAR_ID()) {
		$_targetYearTermId = $yearTermId;
	}
	else {
		if ($pastYearTermId == 'F') {
			$_targetYearTermId = 'F';
		}
		else {
			$_termAry = $fcm->Get_Academic_Year_Term_List($_academicYearId);
			$_targetYearTermId = $_termAry[$pastYearTermNum-1];
			
			// [2017-0808-1102-23206] Get Target TermID
			if(!empty($_targetYearTermId)) {
				$_targetYearTermId = $_targetYearTermId['YearTermID'];
			}
		}
	}
	
	// Get Target Term Name
	$_targetYearTermName = '';
	if ($_targetYearTermId == 'F') {
		$_targetYearTermName = 'Yearly';
	}
	else {
		$_targetYearTermName = $lreportcardAry[$_academicYearId]->returnSemesters($_targetYearTermId, 'en');
	}
	$reportAssoAry[$_academicYearId]['yearTermId'] = $_targetYearTermId;
	$reportAssoAry[$_academicYearId]['yearTermName'] = $_targetYearTermName;
	
	// Get Report Info of this Academic Year of each selected Forms
	for ($j=0; $j<$numOfTarget; $j++) {
		$__targetId = $targetIdAry[$j];
		
		if ($level == 'Class') {
			$__classlevelId = $lreportcard->Get_ClassLevel_By_ClassID($__targetId);
		}
		else if ($level = 'Form') {
			$__classlevelId = $__targetId;
		}
		
		if (isset($reportAssoAry[$_academicYearId][$__classlevelId])) {
			continue;
		}
		
		$__reportInfoAry = $lreportcardAry[$_academicYearId]->returnReportTemplateBasicInfo('', '', $__classlevelId, $reportAssoAry[$_academicYearId]['yearTermId']);
		$reportAssoAry[$_academicYearId][$__classlevelId]['reportId'] = $__reportInfoAry['ReportID'];
		$reportAssoAry[$_academicYearId][$__classlevelId]['markAry'] = $lreportcardAry[$_academicYearId]->getMarks($__reportInfoAry['ReportID'], $StudentID='', $cons='', $ParentSubjectOnly=1, $includeAdjustedMarks=0, $OrderBy='', $SubjectID='', $ReportColumnID=0);
	}
}

// Generate data array
$dataAry = array();
for ($i=0; $i<$numOfTarget; $i++) {
	$_targetId = $targetIdAry[$i];
	
	// Get Class Level of selected Forms
	if ($level == 'Class') {
		$_classlevelId = $lreportcard->Get_ClassLevel_By_ClassID($_targetId);
	}
	else if ($level = 'Form') {
		$_classlevelId = $_targetId;
	}
	$_classlevelName = $lreportcard->returnClassLevel($_classlevelId);
	
	// Get Report of selected Academic Year
	$_reportInfoAry = $lreportcard->returnReportTemplateBasicInfo('', '', $_classlevelId, $yearTermId);
	$_reportId = $_reportInfoAry['ReportID'];
	
	// Get Subjects of selected Academic Year
	$_subjectAry = $lreportcard->returnSubjectwOrder($_classlevelId, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $_reportId);
	
	// loop Subjects
	foreach ((array)$_subjectAry as $__parentSubjectId => $__cmpSubjectAry) {
		$__numOfCmpSubject = count($__cmpSubjectAry) - 1;
		
		foreach ((array)$__cmpSubjectAry as $___cmpSubjectId => $___cmpSubjectName) {
			$___isCmpSubject = ($___cmpSubjectId == 0)? false : true;
			$___subjectId = ($___isCmpSubject)? $___cmpSubjectId : $__parentSubjectId;
			
			if ($___isCmpSubject) {
				continue;
			}
		
			// Get Grading Scheme from selected Academic Year
			$___statAry = $lreportcard->Get_Scheme_Grade_Statistics_Info($_reportId, $___subjectId, $forSubjectGroup=0, $ReportColumnID=0, $Rounding=2, $returnWithReportColumnID=0);
			$___gradingAry = array_keys($___statAry);
			$___numOfGrading = count($___gradingAry);
			
			// Subject and Grade Column
			$___rowCount = 0;
			$dataAry[$_targetId][$___subjectId][$___rowCount][] = $lreportcard->GET_SUBJECT_NAME_LANG($___subjectId);
			for ($j=0; $j<$___numOfGrading; $j++) {
				$dataAry[$_targetId][$___subjectId][$___rowCount][] = $___gradingAry[$j];
			}
			$dataAry[$_targetId][$___subjectId][$___rowCount][] = '全級總人數';
			$___rowCount++;
			
			// Statistics of each Academic Years
			for ($j=0; $j<$numOfTargetYear; $j++) {
				$____academicYearId = $targetAcademicYearAry[$j]['AcademicYearID'];
				$____lreportcard = $lreportcardAry[$____academicYearId];
				$____yearTermId = $reportAssoAry[$____academicYearId]['yearTermId'];
				$____termName = $reportAssoAry[$____academicYearId]['yearTermName'];
				$____reportId = $reportAssoAry[$____academicYearId][$_classlevelId]['reportId'];
				$____markAry = $reportAssoAry[$____academicYearId][$_classlevelId]['markAry'];
				
				$____academicYearInfoAry = $____lreportcard->Get_Academic_Year_By_AcademicYearId($____academicYearId);
				$____academicYearEn = $____academicYearInfoAry['YearNameEN'];
				$____academicYearSubjectId = convertSubjectIdForAcademicYear($____lreportcard->schoolYear, $___subjectId);
				
				if ($____academicYearId == $lreportcard->GET_ACTIVE_YEAR_ID()) {
					$____statAry = $___statAry;
				}
				else {
					$____statAry = $____lreportcard->Get_Scheme_Grade_Statistics_Info($____reportId, $____academicYearSubjectId, $forSubjectGroup=0, $ReportColumnID=0, $Rounding=2, $returnWithReportColumnID=0);	
				}
				
				// First Column: Academic Year, Term, and Form display 
				$dataAry[$_targetId][$___subjectId][$___rowCount][] = $____academicYearEn.' '.$_classlevelName.' '.$____termName;
				
				// Middle Columns: Number of Students for each Grade
				$____genderCountAry = array();
				for ($k=0; $k<$___numOfGrading; $k++) {
					$_____grade = $___gradingAry[$k];
					
					$_____numOfStudent = $____statAry[$_____grade][0]['M_NumOfStudent'];	// "0" means Whole Form
					$dataAry[$_targetId][$___subjectId][$___rowCount][] = $_____numOfStudent;
					$____genderCountAry['M'] += $_____numOfStudent;
					
					$_____numOfStudent = $____statAry[$_____grade][0]['F_NumOfStudent'];	// "0" means Whole Form
					$dataAry[$_targetId][$___subjectId][$___rowCount][] = $_____numOfStudent;
					$____genderCountAry['F'] += $_____numOfStudent;
					
					$_____numOfStudent = $____statAry[$_____grade][0]['NumOfStudent'];	// "0" means Whole Form
					$dataAry[$_targetId][$___subjectId][$___rowCount][] = $_____numOfStudent;
				}
				
				// Last Column: Total Number of Students of this Academic Year
				$____hasNumber = false;
				foreach ((array)$____markAry as $_____studentId => $_____studentMarkAry) {
					$_____formNumOfStudent = $_____studentMarkAry[$____academicYearSubjectId][0]['FormNoOfStudent'];	// 0 means Overall Column
					$_____orderMeritForm = $_____studentMarkAry[$____academicYearSubjectId][0]['OrderMeritForm'];
					
					if (!empty($_____formNumOfStudent) && $_____orderMeritForm != -1) {		// Old data, ranking -1 but included in number of students
						$dataAry[$_targetId][$___subjectId][$___rowCount][] = $____genderCountAry['M'];
						$dataAry[$_targetId][$___subjectId][$___rowCount][] = $____genderCountAry['F'];
						$dataAry[$_targetId][$___subjectId][$___rowCount][] = $_____formNumOfStudent;
						
						$____hasNumber = true;
						break;
					}
				}
				if (!$____hasNumber) {
					$dataAry[$_targetId][$___subjectId][$___rowCount][] = 0;
					$dataAry[$_targetId][$___subjectId][$___rowCount][] = 0;
					$dataAry[$_targetId][$___subjectId][$___rowCount][] = 0;
				}
				$___rowCount++;
			}
		}
	}
}
	
## Display the result
if ($viewFormat == 'html') {
	$x = '';
	foreach ((array)$dataAry as $_classlevelId => $_formAry) {
		foreach ((array)$_formAry as $__subjectId => $__statAry) {
			$__numOfRow = count($__statAry);
			
			$x .= '<table id="ResultTable" class="border_table" align="center">';
				for ($i=0; $i<$__numOfRow; $i++) {
					$___rowDataAry = $__statAry[$i];
					$___numOfCol = count($___rowDataAry);
					
					$___subjectWidth = 20;
					$___colWidth = floor((100 - $___subjectWidth) / (($___numOfCol - 1)*3));
					
					$x .= "<tr>\n";
						for ($j=0; $j<$___numOfCol; $j++) {
							$____data = $___rowDataAry[$j];
							
							$____width = '';
							if ($j==0) {
								$____width = $___subjectWidth;
							}
							
							$____center = ($j==0)? '' : 'text-align:center;';
							$____data = ($____data==='' || $____data===null)? '&nbsp;' : $____data;
							
							$____colspan = '';
							$____rowspan = '';
							if ($i==0) {
								if ($j>0) {
									$____colspan = ' colspan="3" ';
								}
								else {
									$____rowspan = ' rowspan="2" ';
								}
							}
							
							$x .= '<td style="width:'.$____width.'%; '.$____center.'" '.$____colspan.' '.$____rowspan.'>'.$____data.'</td>';
						}
					$x .= "</tr>\n";
					
					if ($i==0) {
						// Add a male, female, overall row
						$x .= "<tr>\n";
							for ($j=0; $j<$___numOfCol; $j++) {
								if ($j>0) {
									// Except first column and last column => i.e. all grading columns
									$x .= '<td style="text-align:center; width:'.$___colWidth.'%;">M</td>';
									$x .= '<td style="text-align:center; width:'.$___colWidth.'%;">F</td>';
									$x .= '<td style="text-align:center; width:'.$___colWidth.'%;">Total</td>';
								}
							}
						$x .= "</tr>\n";
					}
				}
			$x .= "</table>\n";
			$x .= "<br><br>\n";
		}
	}
	
	$css = '
<style>
body {
	font-size: small;
}

.border_table{
	border-collapse: collapse;
	border: 1px solid #000000;
	width: 900px;
}

.border_table tr td, .border_table tr th{
	border: 1px solid #000000;
	vertical-align: top;
	padding: 3px;
}
</style>';
	
	$allTable = '<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td align="center"><h1>'.$eReportCard['Reports_GradingReport'].'</h1></td>
					</tr>
					<tr>
						<td>'.$x.'</td>
					</tr>
				</table>';
	
	echo $lreportcard->Get_Report_Header($ReportTitle);
	echo $allTable.$css;
	echo $lreportcard->Get_Report_Footer();
}
else if ($viewFormat == 'csv') {
	include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
	include_once($PATH_WRT_ROOT."includes/libexporttext.php");
	
	$ExportHeaderArr = array();
	$ExportContentArr = array();
	$lexport = new libexporttext();
	
	$exportContentAry = array();
	$rowCount = 0;
	foreach ((array)$dataAry as $_classlevelId => $_formAry) {
		foreach ((array)$_formAry as $__subjectId => $__statAry) {
			$__numOfRow = count($__statAry);
			
			for ($i=0; $i<$__numOfRow; $i++) {
				if ($i==0) {
					$___numOfData = count($__statAry[$i]);
					for ($j=0; $j<$___numOfData; $j++) {
						if ($j==0) {
							$exportContentAry[$rowCount][] = $__statAry[$i][$j];
						}
						else {
							$exportContentAry[$rowCount][] = $__statAry[$i][$j].' (M)';
							$exportContentAry[$rowCount][] = $__statAry[$i][$j].' (F)';
							$exportContentAry[$rowCount][] = $__statAry[$i][$j].' (Total)';
						}
					}
				}
				else {
					$exportContentAry[$rowCount] = $__statAry[$i];
				}
				$rowCount++;
			}
			
			$exportContentAry[$rowCount][] = '';
			$rowCount++;
		}
	}
	
	// Title of the Grand Marksheet
	$filename = "grading_report.csv";
	
	$export_content = $lexport->GET_EXPORT_TXT($exportContentAry, array());
	intranet_closedb();
	
	// Output CSV File
	$lexport->EXPORT_FILE($filename, $export_content);
}
?>