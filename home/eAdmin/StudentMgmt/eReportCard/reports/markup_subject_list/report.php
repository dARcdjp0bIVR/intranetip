<?php
#  Editing by 

/****************************************************
 * Modification log
 *  20191101 Bill:  [2019-1030-0914-49066]
 *      - Set Up Main / Competition Subject from settings
 * 	20170621 Bill:	[2017-0621-1726-12164]
 * 		- Support Absent Students with failed Subjects
 * 	20170519 Bill:
 * 		- Allow View Group User to access
 * 	20160704 Bill:	[2015-1104-1130-08164]
 * 		- Create File 
 * **************************************************/

@SET_TIME_LIMIT(21600);
@ini_set(memory_limit, -1);

$PageRight = array("ADMIN", "VIEW_GROUP");

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

// Access right
if (!$plugin['ReportCard2008']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}
 
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_extrainfo.php");
if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") {
	include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
	$lreportcard = new libreportcardcustom();
}
else {
	$lreportcard = new libreportcard();
}
$lreportcard_ui = new libreportcard_ui();

// Access right
if (!$lreportcard->hasAccessRight() || !$eRCTemplateSetting['Report']['MarkupExamSubject']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

// Get Class Level
$ClassID = $TargetID[0];
$ClassLevelID = $lreportcard->Get_ClassLevel_By_ClassID($ClassID);

// Get Report Generate Date
$GenerationDateAry = $lreportcard->GetPromotionGenerationDate($ClassLevelID);
$ReportGenDate = $GenerationDateAry["LastGenerated"];
$PromotionGenDate = $GenerationDateAry["LastPromotionUpdate"];

// if report is not generated => access right
if(empty($ReportGenDate) || empty($PromotionGenDate)) {
	echo $eReportCard["ReportNotGenerated"];
	exit();
}

// Get Year Report
$ReportTemplateInfo = $lreportcard->returnReportTemplateBasicInfo("", "", $ClassLevelID, "F");
$ReportID = $ReportTemplateInfo["ReportID"];

// Get School Year Name
$SchoolYearName = $lreportcard->GET_ACTIVE_YEAR_NAME();

// [2019-1030-0914-49066] Set Up Main / Competition Subject
$lreportcard->Set_Main_Competition_Subject_List($ClassLevelID);

// Get Report Subjects
$MainSubjectArray = $lreportcard->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='b5', $ParDisplayType='Abbr', $ExcludeCmpSubject=1, $ReportID);
$MainSubjectCount = 0;
if (sizeof($MainSubjectArray) > 0)
{
	// Get Subject Code Mapping
	$SubjectCodeMapping = $lreportcard->GET_SUBJECTS_CODEID_MAP(0, 0);
	
	// Get Competition Subject List
	$CompletitionSubjectArr = $lreportcard->CompletitionSubjectList;
	
	// loop Subjects
	foreach((array)$MainSubjectArray as $thisMainSubjectID => $thisMainSubjectName){
		// Get Subject Code
		$thisMainSubjectCode = $SubjectCodeMapping[$thisMainSubjectID];
		
		// Exclude Completition Subject
		if(in_array($thisMainSubjectCode, (array)$CompletitionSubjectArr)){
			continue;
		}
		
		$MainSubjectIDArray[] = $thisMainSubjectID;
		$MainSubjectNameArray[] = $thisMainSubjectName;
	}
	$MainSubjectCount = count((array)$MainSubjectIDArray);
}

// Get Class Name and Teacher Name
$ClassObj = new year_class($ClassID, 0, 1);
$ClassName = $ClassObj->ClassTitleB5;
$ClassTeacherArr = Get_Array_By_Key($ClassObj->ClassTeacherList, "TeacherName");
$ClassTeacherArr = array_values(array_unique($ClassTeacherArr));
$ClassTeacherNameList = implode(", ", $ClassTeacherArr);

// Get Students waiting for Mark-up Exam
$StudentForMarkupExam = $lreportcard->GET_PROMOTION_RETENTION_SUMMARY_RECORDS($ClassID, $PromotionStatus=array("0", "6"));
$StudentForMarkupExam = BuildMultiKeyAssoc((array)$StudentForMarkupExam, "StudentID");
$StudentIDAry = array_keys($StudentForMarkupExam);

// Get Mark-up Exam Subjects
$sql = "SELECT
			StudentID, SubjectID
		FROM
			".$lreportcard->DBName.".RC_PROMOTION_RETENTION_FAILED_SUBJECT
		WHERE
			ReportID = '$ReportID' AND ClassLevelID = '$ClassLevelID' AND Year = '".$lreportcard->schoolYearID."' AND StudentID IN ('".implode("', '", (array)$StudentIDAry)."') AND SubjectType = '1'";
$MarkupExamSubject = $lreportcard->returnArray($sql);
$MarkupExamSubject = BuildMultiKeyAssoc((array)$MarkupExamSubject, array("StudentID", "SubjectID"), "SubjectID", 1);

include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_header.php");

// Report Header
$x = "<div class='main_container'>";
$x .= "	<table class='report_header' width='100%'>
		<tr>
			<td width='8%'><b>班　級:</b></td>
			<td width='32%'>$ClassName</td>
			<td class='report_title' rowspan='2'><b>$SchoolYearName 補考成績登記表</b></td>
		<tr>
			<td><b>班主任:</b></td>
			<td>$ClassTeacherNameList</td>
		</tr>
		</table>";

// Table Header
$x .= "	<table class='result_table' width='100%' border='1'>
		<tr>
			<td>學號</td>
			<td>學生姓名</td>
			<td>&nbsp;&nbsp;</td>";
for($i=0; $i<$MainSubjectCount; $i++) {
	$x .= "	<td>".$MainSubjectNameArray[$i][0]."</td>";
}
$x .= "	</tr>";

// Student Rows
$FailSubjectCountAry = array();
foreach((array)$StudentForMarkupExam as $thisStudentID => $thisStudentInfo)
{
	if($thisStudentInfo['PromotionStatusValue'] == 0 && empty($MarkupExamSubject[$thisStudentID]))
		continue;
	
	$x .= "	<tr>
				<td>".$thisStudentInfo["ClassNumber"]."</td>
				<td>".$thisStudentInfo["StudentNameB5"]."</td>
				<td>&nbsp;&nbsp;</td>";
	for($i=0; $i<$MainSubjectCount; $i++)
	{
		$thisSubjectID = $MainSubjectIDArray[$i];
		$isStudentFailedSubject = !empty($MarkupExamSubject[$thisStudentID][$thisSubjectID]);
		
		$x .= "<td>".($isStudentFailedSubject? "*" : "&nbsp;")."</td>";
		if($isStudentFailedSubject){
			$FailSubjectCountAry[$thisSubjectID]++;
		}
	}
	$x .= "</tr>";
}

// Summary Count Row
$x .= "	<tr>
			<td colspan='3'>&nbsp;</td>";
	for($i=0; $i<$MainSubjectCount; $i++)
	{
		$thisSubjectID = $MainSubjectIDArray[$i];
		$FailStudentCount = $FailSubjectCountAry[$thisSubjectID];
		$FailStudentCount = $FailStudentCount > 0? $FailStudentCount : "0";
		
		$x .= "<td>".$FailStudentCount."</td>";
	}
$x .= "	</tr>";
$x .= "	</table>";

$x .= "	</div>";

echo $x;

include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_footer.php");

intranet_closedb();
?>

<style type="text/css">
@charset "utf-8";
/* CSS Document */
.main_container { display: block; width: 773px; height: 920px; margin: auto; padding: 30px 20px; position: relative; }
.main_container .report_header td { font-size: 14px; font-family: "Arial", "PMingLiU", "Lucida Console"; }
.main_container .report_header td.report_title { font-size: 16px; font-family: "Arial", "PMingLiU", "Lucida Console"; text-align: left; }
.main_container .result_table  { border-collapse: collapse; margin-top: 8px; }
.main_container .result_table td { font-size: 14px; font-family: "Arial", "PMingLiU", "Lucida Console"; text-align: center; }
.page_break { page-break-after: always; margin: 0; padding: 0; line-height: 0; font-size: 0; height: 0; }
</style>