<?
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_award.php");

$ViewFormat = $_POST['ViewFormat'];
$YearID = $_POST['YearID'];
$YearClassIDArr = $_POST['YearClassIDArr'];
$ReportID = $_POST['ReportID'];
$AwardIDArr = $_POST['AwardIDArr'];

$lreportcard = new libreportcard_award();
$lreportcard->hasAccessRight();


### Get Report Info
$ReportInfoArr = $lreportcard->returnReportTemplateBasicInfo($ReportID);
$ClassLevelID = $ReportInfoArr['ClassLevelID'];
$YearTermID = $ReportInfoArr['Semester'];
$ReportType = ($YearTermID=='F')? 'W' : 'T';


### Get Report Title
$FormName = $lreportcard->returnClassLevel($ClassLevelID);
$ReportTitle = $FormName.' '.$eReportCard['Reports_AwardList'];


### Get Award Info
$AwardInfoArr = $lreportcard->Get_Award_Info();


### Get selected Student
$isShowStyle = ($ViewFormat=='html')? true : false;
$StudentInfoArr = $lreportcard->GET_STUDENT_BY_CLASSLEVEL($ReportID, $ClassLevelID, $YearClassIDArr, $ParStudentID="", $ReturnAsso=0, $isShowStyle);
$StudentIDArr = Get_Array_By_Key($StudentInfoArr, 'UserID');
$numOfStudent = count($StudentInfoArr);


### Get Student Award Info
//$StudentAwardInfoArr[$StudentID][$AwardID][$SubjectID][Field] = Data
$StudentAwardInfoArr = $lreportcard->Get_Award_Generated_Student_Record($ReportID, $StudentIDArr, $ReturnAsso=1, $AwardNameWithSubject=1, $AwardIDArr);


if ($ReportType == 'W') {
	### Get Term Reports
	$TermReportInfoArr = $lreportcard->Get_Related_TermReport_Of_Consolidated_Report($ReportID, $MainReportOnly=1, $ReportColumnID="ALL");
	//$TermReportIDArr = Get_Array_By_Key($TermReportInfoArr, 'ReportID');
	$numOfTermReport = count($TermReportInfoArr);
}
else if ($ReportType == 'T') {
	$TermReportInfoArr[0]['ReportID'] = $ReportID;
	$numOfTermReport = 1;
}
	
### Get Subject Info
$SubjectArr = $lreportcard->returnSubjectwOrderNoL($ClassLevelID, $ParForSelection=0, $MainSubjectOnly=0, $ReportID);

for ($i=0; $i<$numOfTermReport; $i++) {
	$thisTermReportID = $TermReportInfoArr[$i]['ReportID'];
	
	# Get Term Report Marksheet
	$ReportColumnInfoArr[$thisTermReportID] = $lreportcard->returnReportTemplateColumnData($thisTermReportID);
	$thisNumOfReportColumn = count($ReportColumnInfoArr[$thisTermReportID]);
	for ($j=0; $j<$thisNumOfReportColumn; $j++) {
		$thisReportColumnID = $ReportColumnInfoArr[$thisTermReportID][$j]['ReportColumnID'];
		
		foreach ((array)$SubjectArr as $thisSubjectID => $thisSubjectInfoArr) {
			$TermReportMarksheetArr[$thisTermReportID][$thisReportColumnID][$thisSubjectID] = $lreportcard->GET_MARKSHEET_SCORE($StudentIDArr, $thisSubjectID, $thisReportColumnID);
		}
	}
}


### Count student's number of estimated score
$StudentNumOfEstimatedScoreArr = array();
for ($i=0; $i<$numOfStudent; $i++) {
	$thisStudentID = $StudentInfoArr[$i]['UserID'];
	
	//$StudentNumOfEstimatedScoreArr[$thisStudentID] = 0;
	for ($j=0; $j<$numOfTermReport; $j++) {
		$thisTermReportID = $TermReportInfoArr[$j]['ReportID'];
		
		$thisReportColumnInfoArr = $ReportColumnInfoArr[$thisTermReportID];
		$thisNumOfReportColumn = count($thisReportColumnInfoArr);
		
		foreach ((array)$SubjectArr as $thisSubjectID => $thisSubjectInfoArr) {
			$thisGrade = $TermReportMarkArr[$thisTermReportID][$thisStudentID][$thisSubjectID][0]['Grade'];
			
			//2014-0520-1540-13140
			if ($lreportcard->CHECK_SUBJECT_IS_COMPONENT_SUBJECT($thisSubjectID)) {
				// component subject => count estimated score to parent subject
				$thisTargetSubjectId = $lreportcard->GET_PARENT_SUBJECT_ID($thisSubjectID); 
			}
			else {
				$thisTargetSubjectId = $thisSubjectID;
			}
			
			
			if ($lreportcard->Check_If_Grade_Is_SpecialCase($thisGrade)) {
				// If the student has not studied this Subject => ignore all checking
				continue;
			}
			
			# Check Estimated Score
			for ($k=0; $k<$thisNumOfReportColumn; $k++) {
				$thisReportColumnID = $thisReportColumnInfoArr[$k]['ReportColumnID'];
			
				$thisIsEstimatedScore = $TermReportMarksheetArr[$thisTermReportID][$thisReportColumnID][$thisSubjectID][$thisStudentID]['IsEstimated'];
				if ($thisIsEstimatedScore) {
					//2014-0520-1540-13140
					//$StudentNumOfEstimatedScoreArr[$thisStudentID][$thisSubjectID]++;
					$StudentNumOfEstimatedScoreArr[$thisStudentID][$thisTargetSubjectId]++;
				}
			}	// End loop Report Column
		}	// End loop Subject
	}	// End loop Term Report
}	// End loop Student



$HeaderArr = array();
if ($ViewFormat=='html') {
	$HeaderArr[] = '#';
}
$HeaderArr[] = $Lang['SysMgr']['FormClassMapping']['Class'];
$HeaderArr[] = $Lang['SysMgr']['FormClassMapping']['ClassNo'];
$HeaderArr[] = $Lang['SysMgr']['FormClassMapping']['StudentName'];
$HeaderArr[] = $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['AwardName'];
$HeaderArr[] = $Lang['SysMgr']['SubjectClassMapping']['Subject'];
$HeaderArr[] = $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['AwardRanking'];
$HeaderArr[] = $Lang['eReportCard']['ManagementArr']['MarksheetRevisionArr']['NoOfEstimatedScore'];
$numOfHeader = count($HeaderArr);


$InfoArr = array();
$InfoCounter = 0;
for ($i=0; $i<$numOfStudent; $i++) {
	$thisStudentID = $StudentInfoArr[$i]['UserID'];
	$thisClassName = Get_Lang_Selection($StudentInfoArr[$i]['ClassTitleCh'], $StudentInfoArr[$i]['ClassTitleEn']);
	$thisClassNumber = $StudentInfoArr[$i]['ClassNumber'];
	$thisStudentName = $StudentInfoArr[$i]['StudentName'];
	
	foreach ((array)$StudentAwardInfoArr[$thisStudentID] as $thisAwardID => $thisAwardInfoArr) {
		$thisAwardName = Get_Lang_Selection($AwardInfoArr[$thisAwardID]['AwardNameCh'], $AwardInfoArr[$thisAwardID]['AwardNameEn']);
		$thisAwardCode = $AwardInfoArr[$thisAwardID]['AwardCode'];
		
		foreach ((array)$thisAwardInfoArr as $thisSubjectID => $thisSubjectAwardInfoArr) {
			// Award Ranking
			$thisAwardRank = $thisSubjectAwardInfoArr['AwardRank'];
			$thisAwardRank = ($thisAwardRank=='')? $Lang['General']['EmptySymbol'] : $thisAwardRank; 
			
			// Subject Name
			$thisSubjectName = '';
			if ($thisSubjectID == 0) {
				$thisSubjectName = $Lang['General']['EmptySymbol'];
			}
			else {
				$thisSubjectName = $lreportcard->GET_SUBJECT_NAME_LANG($thisSubjectID);
			}
			
			// Get number of estimated score
			$thisNumOfEstimatedScore = '';
			if ($thisAwardCode == 'annual_academic_result') {
				$thisNumOfEstimatedScore = array_sum((array)$StudentNumOfEstimatedScoreArr[$thisStudentID]);
			}
			else if ($ReportType = 'T') {
				$thisNumOfEstimatedScore = $StudentNumOfEstimatedScoreArr[$thisStudentID][$thisSubjectID];
				$thisNumOfEstimatedScore = ($thisNumOfEstimatedScore=='')? 0 : $thisNumOfEstimatedScore;
			}
			else {
				$thisNumOfEstimatedScore = $Lang['General']['EmptySymbol'];
			}
			
			$InfoArr[$InfoCounter]['ClassName'] = $thisClassName;
			$InfoArr[$InfoCounter]['ClassNumber'] = $thisClassNumber;
			$InfoArr[$InfoCounter]['StudentName'] = $thisStudentName;
			$InfoArr[$InfoCounter]['AwardName'] = $thisAwardName;
			$InfoArr[$InfoCounter]['SubjectName'] = $thisSubjectName;
			$InfoArr[$InfoCounter]['AwardRank'] = $thisAwardRank;
			$InfoArr[$InfoCounter]['NumOfEstimatedScore'] = $thisNumOfEstimatedScore;
			$InfoCounter++;
		}
	}
}
$numOfInfo = count($InfoArr);


if ($ViewFormat=='html') {
	$x = '';
	$x .= '<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">'."\r\n";
		$x .= '<tr>'."\r\n";
			$x .= '<td align="center"><h1>'.$ReportTitle.'</h1></td>'."\r\n";
		$x .= '</tr>'."\r\n";
		$x .= '<tr>'."\r\n";
			$x .= '<td>'."\r\n";
				$x .= "<table id='ResultTable' class='GrandMSContentTable' width='100%' border='1' cellpadding='2' cellspacing='0'>\n";
					$x .= "<col width='2%' />\n";
					$x .= "<col width='8%' />\n";
					$x .= "<col width='8%' />\n";
					$x .= "<col width='20%' />\n";
					$x .= "<col width='15%' />\n";
					$x .= "<col width='25%' />\n";
					$x .= "<col width='8%' />\n";
					$x .= "<col width='14%' />\n";
					
					$x .= "<thead>\n";
						$x .= "<tr>\n";
							for ($i=0; $i<$numOfHeader; $i++) {
								$thisHeader = $HeaderArr[$i];
								$x .= "<th>".$thisHeader."</th>\n";
							}
						$x .= "</tr>\n";
					$x .= "</thead>\n";
					
					$x .= "<tbody>\n";
						if ($numOfInfo == 0) {
							$x .= "<tr><td colspan='100%' style='text-align:center;'>".$Lang['General']['NoRecordAtThisMoment']."</td></tr>\n";
						}
						else {
							for ($i=0; $i<$numOfInfo; $i++) {
								$thisClassName = $InfoArr[$i]['ClassName'];
								$thisClassNumber = $InfoArr[$i]['ClassNumber'];
								$thisStudentName = $InfoArr[$i]['StudentName'];
								$thisAwardName = $InfoArr[$i]['AwardName'];
								$thisSubjectName = $InfoArr[$i]['SubjectName'];
								$thisAwardRank = $InfoArr[$i]['AwardRank'];
								$thisNumOfEstimatedScore = $InfoArr[$i]['NumOfEstimatedScore'];
								
								$x .= "<tr>\n";
									$x .= "<td style='text-align:center;'>".($i + 1)."</td>\n";
									$x .= "<td style='text-align:center;'>".$thisClassName."</td>\n";
									$x .= "<td style='text-align:center;'>".$thisClassNumber."</td>\n";
									$x .= "<td>".$thisStudentName."</td>\n";
									$x .= "<td>".$thisAwardName."</td>\n";
									$x .= "<td>".$thisSubjectName."</td>\n";
									$x .= "<td style='text-align:center;'>".$thisAwardRank."</td>\n";
									$x .= "<td style='text-align:center;'>".$thisNumOfEstimatedScore."</td>\n";
								$x .= "</tr>\n";
							}
						}
					$x .= "</tbody>\n";
				$x .= "</table>\n";
			$x .= '</td>'."\r\n";
		$x .= '</tr>'."\r\n";
	$x .= '</table>';
	
	echo $lreportcard->Get_GrandMS_CSS();
	echo $lreportcard->Get_Report_Header($ReportTitle);
	echo $x;
	echo $lreportcard->Get_Report_Footer();
	
	intranet_closedb();
}
else if ($ViewFormat=='csv') {
	//include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
	include_once($PATH_WRT_ROOT."includes/libexporttext.php");
	
	$lexport = new libexporttext();
	
	
	# Content
	$ExportContentArr = array();
	for ($i=0; $i<$numOfInfo; $i++) {
		$thisClassName = $InfoArr[$i]['ClassName'];
		$thisClassNumber = $InfoArr[$i]['ClassNumber'];
		$thisStudentName = $InfoArr[$i]['StudentName'];
		$thisAwardName = $InfoArr[$i]['AwardName'];
		$thisSubjectName = $InfoArr[$i]['SubjectName'];
		$thisAwardRank = $InfoArr[$i]['AwardRank'];
		$thisNumOfEstimatedScore = $InfoArr[$i]['NumOfEstimatedScore'];
		
		$ExportContentArr[$i][] = $thisClassName;
		$ExportContentArr[$i][] = $thisClassNumber;
		$ExportContentArr[$i][] = $thisStudentName;
		$ExportContentArr[$i][] = $thisAwardName;
		$ExportContentArr[$i][] = $thisSubjectName;
		$ExportContentArr[$i][] = $thisAwardRank;
		$ExportContentArr[$i][] = $thisNumOfEstimatedScore;
	}
	
	
	// Title of the grandmarksheet
	$ReportTitle = str_replace(" ", "_", $ReportTitle);
	$filename = $ReportTitle.".csv";
	
	$export_content = $lexport->GET_EXPORT_TXT($ExportContentArr, $HeaderArr);
	intranet_closedb();
	
	// Output the file to user browser
	$lexport->EXPORT_FILE($filename, $export_content);
}
?>