<?php
# Editing by

/**************************************************
 * 	Modification log
 *  20190313 Bill:  [2017-0901-1525-31265]
 *      Add option - Include Current Year Result
 * 	20180723 Bill:  [2017-0901-1525-31265]
 * 		Allow View Group to select all options
 *  20180226 Bill	[2017-0901-1525-31265]
 * 		- Create File
 * ***********************************************/

$PageRight = array("ADMIN", "VIEW_GROUP");

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");

$lreportcard = new libreportcardcustom();
$lreportcard->hasAccessRight();

$linterface = new interface_html();
$lreportcard_ui = new libreportcard_ui();

# Tag
$CurrentPage = "Reports_ReportCardSummary";
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
$TAGS_OBJ[] = array($eReportCard['Reports_ReportCardSummary'], "", 0);

$checkPermissionFormClass = ($lreportcard->IS_ADMIN_USER_VIEW($_SESSION['UserID']) || $lreportcard->IS_VIEW_GROUP_USER_VIEW($_SESSION['UserID']))? 0 : 3;
$checkPermissionSubject = ($lreportcard->IS_ADMIN_USER_VIEW($_SESSION['UserID']) || $lreportcard->IS_VIEW_GROUP_USER_VIEW($_SESSION['UserID']))? 0 : 1;

$linterface->LAYOUT_START();

$x = '';
$x .= '<table class="form_table_v30">'."\n";
	
	// Form
	$x .= '<tr class="optionTr formClass">'."\n";
		$x .= '<td class="field_title">'.$Lang['General']['Form'].'</td>'."\n";
		$x .= '<td>'."\n";
			$x .= $lreportcard_ui->Get_Form_Selection('yearId', $SelectedYearID='', 'changedFormSelection();', $noFirst=1, $isAll=0, $hasTemplateOnly=1, $checkPermissionFormClass, $IsMultiple=0);
		$x .= '</td>'."\n";
	$x .= '</tr>'."\n";
	
	// Class
	$x .= '<tr class="optionTr formClass">'."\n";
		$x .= '<td class="field_title">'.$Lang['General']['Class'].'</td>'."\n";
		$x .= '<td>'."\n";
			$x .= '<div id="classSelDiv"></div>';
		$x .= '</td>'."\n";
	$x .= '</tr>'."\n";
	
	// Student
	$x .= '<tr class="optionTr formClass classHistory">'."\n";
		$x .= '<td class="field_title">'.$Lang['Identity']['Student'].'</td>'."\n";
		$x .= '<td>'."\n";
			$x .= '<div id="studentSelDiv"></div>';
			$x .= $linterface->Get_Form_Warning_Msg("studentWarning", $eReportCard['jsWarningArr']['PleaseSelectStudent'], "warnMsgDiv")."\n";
		$x .= '</td>'."\n";
	$x .= '</tr>'."\n";
	
	// Include Current Year Result
	$x .= '<tr class="optionTr formClass">'."\n";
	    $x .= '<td class="field_title">'.$Lang['eReportCard']['ReportArr']['ReportSummaryArr']['IncludeCurrentYearResult'].'</td>'."\n";
	    $x .= '<td>'."\n";
	        $x .= $linterface->Get_Radio_Button('includeCurrentYear_Yes', 'includeCurrentYear', '1', $isChecked=0, $Class="", $Lang['General']['Yes']);
	        $x .= '&nbsp;';
	        $x .= $linterface->Get_Radio_Button('includeCurrentYear_No', 'includeCurrentYear', '0', $isChecked=1, $Class="", $Lang['General']['No']);
	        $x .= '<br/>';
	        $x .= '<span class="tabletextremark">'.$Lang['eReportCard']['ReportArr']['ReportSummaryArr']['IncludeCurrentYearResultRemarks'].'</span>';
		$x .= '</td>'."\n";
	$x .= '</tr>'."\n";
	
$x .= '</table>'."\n";

$htmlAry = array();
$htmlAry['optionTable'] = $x;
$htmlAry['submitBtn'] = $linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], 'button', 'checkForm();');
?>

<script>
var loading = '<?=$linterface->Get_Ajax_Loading_Image($noLang=1)?>';
var checkPermissionFormClass = '<?=$checkPermissionFormClass?>';
var checkPermissionSubject = '<?=$checkPermissionSubject?>';

$(document).ready(function()
{
	// initialize selection for form class student source
	changedFormSelection();
});

function changedFormSelection()
{
	var selectedYearId = $('select#yearId').val();
	
	$('div#classSelDiv').html(loading).load(
		"../ajax_reload_selection.php",
		{
			RecordType: 'Class',
			YearID: selectedYearId,
			SelectionID: 'yearClassIdAry[]',
			onchange: 'changedClassSelection();',
			isMultiple: 1,
			TeachingOnly: checkPermissionFormClass,
			WithSelectAllButton: 1
		},
		function(ReturnData) {
			js_Select_All('yearClassIdAry[]');
			changedClassSelection();
		}
	);
}

function changedClassSelection()
{
	var classSelId = getJQuerySaveId('yearClassIdAry[]');
	var classIdList = $('select#' + classSelId).val();
	if (classIdList) {
		classIdList = classIdList.join(',');
	}
	else {
		classIdList = '-1'; 	// no class will be returned
	}
	
	$('div#studentSelDiv').html(loading).load(
		"../ajax_reload_selection.php", 
		{
			RecordType: 'StudentByClass',
			ClassID: classIdList,
			SelectionID: 'studentIdAry[]',
			noFirst: 1,
			isMultiple: 1,
			withSelectAll: 1
		},
		function(ReturnData) {
			js_Select_All('studentIdAry[]');
		}
	);
}

function checkForm()
{
	var canSubmit = true;
	$('div.warnMsgDiv').hide();
	
	var studentSelId = getJQuerySaveId('studentIdAry[]');
	if ($('select#' + studentSelId).val() == null || $('select#' + studentSelId).val() == '')
	{
		$('div#studentWarning').show();
		$('select#' + studentSelId).focus();
		
		canSubmit = false;
	}
	
	if (canSubmit) {
		$('form#form1').submit();
	}
}
</script>

<form id="form1" name="form1" method="post" target="_blank" action="report_by_pdf.php">
	<div class="table_board">
		<table id="html_body_frame" style="width:100%;">
			<tr><td><?=$htmlAry['optionTable']?></td></tr>
		</table>
	</div>
	<div class="edit_bottom_v30">
		<br />
		<?=$htmlAry['submitBtn']?>
	</div>
</form>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>