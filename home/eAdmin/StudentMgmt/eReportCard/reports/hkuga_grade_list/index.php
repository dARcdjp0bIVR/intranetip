<?php
// using :

/***********************************************
 * modification log
 * 	20180723 Bill:  [2017-0901-1525-31265]
 * 		Allow View Group to select all options
 * ***********************************************/

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if ($plugin['ReportCard2008']) {
	include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
	include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
	include_once($PATH_WRT_ROOT."includes/libinterface.php");
	
	if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
	{
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
		$lreportcard = new libreportcardcustom();
	} else {
		$lreportcard = new libreportcard();
	}
	$lreportcard_ui = new libreportcard_ui();
	$linterface = new interface_html();
	
	if ($lreportcard->hasAccessRight()) {
		
		############## Interface Start ##############
		$linterface = new interface_html();
		$CurrentPage = "Reports_HKUGAGradeList";
		$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
		# tag information
		$TAGS_OBJ[] = array($eReportCard['Reports_HKUGAGradeList']);
		$linterface->LAYOUT_START();
		#############################################
	
		echo $lreportcard_ui->Get_HKUGA_Grade_List_Index_UI(); 			

?>
<script>
var loading = '<?=$linterface->Get_Ajax_Loading_Image()?>';
var CheckTeaching = <?=($lreportcard->IS_ADMIN_USER($_SESSION['UserID']) || $lreportcard->IS_VIEW_GROUP_USER_VIEW($_SESSION['UserID']))? 0 : 3?>;

function js_Reload_Report_Selection()
{
	var YearID = $('#YearID').val();
	
	$("span.ReportSelectionSpan").html(loading).load(
		"../ajax_reload_selection.php",
		{
			RecordType: "Report",
			SelectionID: "ReportID",
			YearID:YearID,
			onChange: "js_Reload_Subject_Selection()"
		},
		function(ReturnData){
			js_Reload_Subject_Selection();
		}
	)

}

function js_Reload_Subject_Selection()
{
	var YearID = $('#YearID').val();
	var ReportID = $('#ReportID').val();
	
	$("span.SubjectSelectionSpan").html(loading).load(
		"../ajax_reload_selection.php",
		{
			RecordType: "Subject",
			SelectionID: "SubjectIDArr[]",
			YearID:YearID,
			ReportID:ReportID,
			onChange: "js_Reload_SubjectGroup_Selection()"/*,
			TeachingOnly: CheckTeaching*/
		},
		function(ReturnData){
			js_Select_All('SubjectIDArr[]',1);
			js_Reload_SubjectGroup_Selection();
		}	
	)	
}

function js_Reload_Class_Selection()
{
	var YearID = $('#YearID').val();
	
	$("span.ClassSelectionSpan").html(loading).load(
		"../ajax_reload_selection.php",
		{
			RecordType: "Class",
			SelectionID: "ClassIDArr[]",
			YearID:YearID,
			TeachingOnly: CheckTeaching,
			isMultiple: 1
		},
		function(ReturnData){
			js_Select_All('ClassIDArr[]',1);
			js_Toggle_View();
		}	
	)	
}

function js_Reload_SubjectGroup_Selection()
{
	var ReportID = $('#ReportID').val();
	var SubjectID = 'NULL';
	if($('#SubjectIDArr\\[\\]').val())
	{
		SubjectID = $('#SubjectIDArr\\[\\]').val().toString();
	}
	
	$("span.SubjectGroupSelectionSpan").html(loading).load(
		"../ajax_reload_selection.php",
		{
			RecordType: "SubjectGroup",
			SelectionID: "SubjectGroupIDArr[]",
			ReportID:ReportID,
			SubjectID:SubjectID,
			TeachingOnly: CheckTeaching,
			noFirst: 1,
			isMultiple: 1
		},
		function(ReturnData){
			js_Select_All('SubjectGroupIDArr[]',1);
			js_Toggle_View();
		}	
	)		
}


function js_Form_Selection_Changed()
{
	js_Reload_Report_Selection();
	js_Reload_Class_Selection();
}

function js_Toggle_View()
{
	if($("input.ViewBy:checked").val()==1)
	{
		$("#ClassSelectionRow").show()
		$("#SubjectGroupSelectionRow").hide()
	}
	else
	{
		$("#ClassSelectionRow").hide()
		$("#SubjectGroupSelectionRow").show()
	}
//	js_Reload_Student_Selection();
}

function checkForm()
{
	$("div.WarnMsg").hide();
	var FormValid = true;
	
	// subject
	if(!$("#SubjectIDArr\\[\\]").val())
	{
		$("#WarnEmptySubject").show();
		FormValid = false;
	}

	if($("input.ViewBy:checked").val()==1)
	{
		if(!$("#ClassIDArr\\[\\]").val())
		{
			$("#WarnEmptyClass").show();
			FormValid = false;
		}
		
	}
	else
	{
		// subject
		if(!$("#SubjectGroupIDArr\\[\\]").val())
		{
			$("#WarnEmptySubjectGroup").show();
			FormValid = false;
		}
	}
	
	return FormValid;
}

//function js_Reload_Student_Selection()
//{
//	if($("input.ViewBy:checked").val()==1)
//		js_Reload_Student_Selection_By_Class();
//	else
//		js_Reload_Student_Selection_By_SubjectGroup();
//	
//}
function js_Reload_Student_Selection() {
	
}

//function js_Reload_Student_Selection_By_Class()
//{
//	var ClassID = '';
//	if($('#ClassIDArr\\[\\]').val())
//	{
//		ClassID = $('#ClassIDArr\\[\\]').val().toString();
//	}
//	
//	$("span.StudentSelectionSpan").html(loading).load(
//		"../ajax_reload_selection.php",
//		{
//			RecordType: "StudentByClass",
//			SelectionID: "StudentIDArr[]",
//			ClassID:ClassID,
//			noFirst: 1,
//			isMultiple: 1
//		},
//		function(ReturnData){
//			js_Select_All('StudentIDArr[]',1);
//		}	
//	)	
//} 
//
//
//function js_Reload_Student_Selection_By_SubjectGroup()
//{
//	var YearID = $('#YearID').val();
//	var ReportID = $('#ReportID').val();
//	var SubjectGroupID = '';
//	if($('#SubjectGroupIDArr\\[\\]').val())
//	{
//		SubjectGroupID = $('#SubjectGroupIDArr\\[\\]').val().toString();
//	}
//	
//	$("span.StudentSelectionSpan").html(loading).load(
//		"../ajax_reload_selection.php",
//		{
//			RecordType: "StudentBySubjectGroup",
//			SelectionID: "StudentIDArr[]",
//			SubjectGroupID:SubjectGroupID,
//			noFirst: 1,
//			isMultiple: 1
//		},
//		function(ReturnData){
//			js_Select_All('StudentIDArr[]',1);
//		}	
//	)	
//} 

$().ready(function(){
	js_Form_Selection_Changed();
})
</script>
<?

  		$linterface->LAYOUT_STOP();
  } else {
?>
You have no priviledge to access this page.
<?
	}
} else {
?>

<?
}
?>
