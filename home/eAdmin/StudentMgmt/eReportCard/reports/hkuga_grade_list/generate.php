<?
/*****************************************************************
 * Modification log
 *  20170208 Villa: 
 *  	-#B112705  Fail to show achievement information and request update wordings in Report Summary
 * 	20120221 Marcus: [2012-0220-1119-33132 - 港大同學會書院 - eRC - Change report card subject status]
 * 		- Improved to display special case instead of N.A.
 * 	20111114 Marcus
 * 		- Get marks from marksheet instead of generated marks.
 * ***************************************************************/
@SET_TIME_LIMIT(21600);
@ini_set(memory_limit, -1);

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");


intranet_auth();
intranet_opendb();


if ($plugin['ReportCard2008']) {
	include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
	
	if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
	{
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
		$lreportcard = new libreportcardcustom();
	} else {
		$lreportcard = new libreportcard();
	}
	
	if ($lreportcard->hasAccessRight()) 
	{
		include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
		$lreportcard_ui = new libreportcard_ui();
		
		# Report Info
		$ReportSetting	= $lreportcard->returnReportTemplateBasicInfo($ReportID);
		$SemID = $ReportSetting['Semester'];
		$ClassLevelID 	= $ReportSetting['ClassLevelID'];
		
		$NameLang = Get_Lang_Selection("ClassTitleB5","ClassTitleEN");
		if($ViewBy == 1) //Class
		{
			# get class name
			$ClassArr = $lreportcard->GET_CLASSES_BY_FORM($ClassLevelID);
			$ClassNameArr = BuildMultiKeyAssoc($ClassArr, "ClassID", $NameLang,1);
			
			# get class teacher
			include_once($PATH_WRT_ROOT."includes/libclass.php");
			$lclass = new libclass();
			$ClassTeacherArr = $lclass->returnClassTeacherByClassID($ClassIDArr);
			$ClassTeacherArr = BuildMultiKeyAssoc($ClassTeacherArr, "YearClassID","CTeacher",1,1);
			
			$GroupingIDArr = $ClassIDArr;
			foreach($ClassIDArr as $thisClassID)
			{
				$GroupingTitleAssoc[$thisClassID] = $ClassNameArr[$thisClassID];
				if($ClassTeacherArr)
					$GroupingTitleAssoc[$thisClassID] .= " - ".implode(" / ",(array)$ClassTeacherArr[$thisClassID]);
			}
			
			# Student
			$StudentArr = $lreportcard->GET_STUDENT_BY_CLASSLEVEL($ParReportID, $ClassLevelID);
			$StudentArr = BuildMultiKeyAssoc($StudentArr, array("YearClassID","UserID"));
		}
		else
		{
			include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
			$scm = new subject_class_mapping();
			
			$GroupingIDArr = $SubjectGroupIDArr;
			$GroupingInfoArr = $scm->Get_Subject_Group_List($SemID, $SubjectIDArr);
			
			foreach($GroupingInfoArr['SubjectGroupList'] as $thisSubjectGroupID => $thisSubjectGroupInfo)
			{
				$TeacherList = $scm->Get_Subject_Group_Teacher_List($thisSubjectGroupID);
				$TeacherNameList = Get_Array_By_Key($TeacherList, "TeacherName");
				
				$GroupingTitleAssoc[$thisSubjectGroupID] = $thisSubjectGroupInfo[$NameLang];
				if($TeacherNameList)
					$GroupingTitleAssoc[$thisSubjectGroupID] .= " - ".implode(" / ",(array)$TeacherNameList);
			}

			# Student 
			$StudentArr = $lreportcard->GET_STUDENT_BY_SUBJECT_GROUP($SubjectGroupIDArr);
			$StudentArr = BuildMultiKeyAssoc($StudentArr, array("SubjectGroupID","UserID"));		
			
		}
		
		# Get Subject Name 
		$SubjectNameArr = $lreportcard->returnSubjectwOrder($ClassLevelID, $ParForSelection=1, $TeacherID='', $_SESSION['intranet_session_language'], $ParDisplayType='Desc', $ExcludeCmpSubject=1, $ReportID);
		
		# Get component subject 
		$ComponentSubject = $lreportcard->GET_COMPONENT_SUBJECT($SubjectIDArr, $ClassLevelID,'en','',$ReportID,1);
		
		# Get Subject Weight 
		$SubjectWeightAssoc = $lreportcard->returnReportTemplateSubjectWeightData($ReportID, " ReportColumnID Is NULL");
		$SubjectWeightAssoc = BuildMultiKeyAssoc($SubjectWeightAssoc, "SubjectID", "Weight", 1);
				
		# Get Grading Scheme 
		$GradingSchemeArr = $lreportcard->GET_FROM_SUBJECT_GRADING_SCHEME($ClassLevelID, $withGrandMark=0, $ReportID);
		
		# Get Term Seq Number
		$TermNumber = $lreportcard->Get_Semester_Seq_Number($SemID);
		
		# Get Marks
//		$MarksAry = $lreportcard->getMarksFromMarksheet($ReportID);
//		$MarksAry = $lreportcard->getMarks($ReportID);
		
		
		# Convert Marks to Grade
//		$MarksAry = $lreportcard->Convert_MarksArr_Mark_To_Grade($ReportID,$MarksAry);
		
		# Get Comment 
		$CommentArr = $lreportcard->returnSubjectTeacherCommentByBatch($ReportID);
		
		# Get Personal Characteristic 
		$PCDataArr = $lreportcard->returnPersonalCharSettingData($ClassLevelID);
		$PCDataArr = BuildMultiKeyAssoc($PCDataArr, "SubjectID", '',0,1);
		
		# Get Students' Personal Characteristic
		$PCArr = $lreportcard->getPersonalCharacteristicsProcessedDataByBatch('',$ReportID,$SubjectIDArr,0,1);
		
		
		$x = '<table cellpadding=0 cellspacing=0 border=0 width="100%"><tr><td align="center" valign="top">'."\n";
		
			foreach((array)$SubjectIDArr as $thisSubjectID) // loop subject
			{
				$MarksAry = $lreportcard->getMarksFromMarksheet($ReportID, $thisSubjectID);
				$MarksAry = $lreportcard->Convert_MarksArr_Mark_To_Grade($ReportID, $MarksAry, 0, 0);
				
				# build key table 
				$key = '<table cellpadding=0 cellspacing=0 border=0 width="100%">'."\n";
					$key .= '<tr valign=top>'."\n";
						$key .= '<td>'."\n";
							# Achievement
							$key .= '<table cellpadding=0 cellspacing=0 border=1 width="100%" class="key_table full_border">'."\n";
								$key .= '<tr>'."\n";
									$key .= '<td colspan=2>'.$eReportCard['HKUGAGradeList']['Achievement'].'</td>'."\n";
								$key .= '</tr>'."\n";
								
								$thisCmpSubjectArr = array();
								
								$tmpCmpSubjectArr = $ComponentSubject[$thisSubjectID];
								foreach((array)$tmpCmpSubjectArr as $thisCmpSubjectInfo)
								{
									if($SubjectWeightAssoc[$thisCmpSubjectInfo['SubjectID']]>0) // skip cmp subject if weight = 0 
										$thisCmpSubjectArr[] = $thisCmpSubjectInfo;
								}
								
								#Count EGCAYG Start
								$numOfEGCGYG = 0;
								$HasEG = false;
								$HasCA = false;
								$HasYG = false;
								foreach ((array)$thisCmpSubjectArr as $_CmpSubjectIDArr){
									if($_CmpSubjectIDArr['SubjectName']=='Examination' ){
										$HasEG = true;
										$numOfEGCGYG++;
									}
									if($_CmpSubjectIDArr['SubjectName']=='Continuous Assessment'){
										$HasCA = true;
										$numOfEGCGYG++;
									}
									if( $_CmpSubjectIDArr['SubjectName']=='Year Grade' ){
										$HasYG = true;
										$numOfEGCGYG++;
									}
								}
								#Count EGCAYG END
								
								$numOfCmp = count($thisCmpSubjectArr);
								
								for($i=0; $i<$numOfCmp-$numOfEGCGYG; $i++)
								{
									$thisCmpSubjectID = $thisCmpSubjectArr[$i]['SubjectID'];
									$thisScaleInput = $GradingSchemeArr[$thisCmpSubjectID]['scaleInput'];
									$key .= '<tr>'."\n";
										$key .= '<td align="center">'.($i+1).' '.$thisScaleInput.'</td>'."\n";
										$key .= '<td>'.$thisCmpSubjectArr[$i]['SubjectName'].'</td>'."\n";
									$key .= '</tr>'."\n";
								}

							$key .= '</table>'."\n";
						$key .= '</td>'."\n";
						
						$key .= '<td>'."\n";
							# Attitude
							$key .= '<table cellpadding=0 cellspacing=0 border=1 width="100%" class="key_table full_border">'."\n";
								$key .= '<tr>'."\n";
									$key .= '<td colspan=2>'.$eReportCard['HKUGAGradeList']['Attitude'].'</td>'."\n";
								$key .= '</tr>'."\n";
								foreach((array)$PCDataArr[$thisSubjectID] as $k => $thisChar)
								{
									$CharNameLang = Get_Lang_Selection("Title_CH","Title_EN");
									$key .= '<tr>'."\n";
										$key .= '<td align="center">&nbsp;'.($k+1).'&nbsp;</td>'."\n";
										$key .= '<td>'.$thisChar[$CharNameLang].'</td>'."\n";
									$key .= '</tr>'."\n";	
								}
							$key .= '</table>'."\n";
						$key .= '</td>'."\n";
						
						$key .= '<td>'."\n";	
							# Exam Grade, CA Grade ... 
							$key .= '<table cellpadding=0 cellspacing=0 border=1 width="100%" class="key_table full_border" >'."\n";
								$key .= '<tr>'."\n";
									$key .= '<td>&nbsp;'.$eReportCard['HKUGAGradeList']['EG'].'&nbsp;</td>'."\n";
									$key .= '<td>'.$eReportCard['HKUGAGradeList']['GradeArr']['EG'].'</td>'."\n";
								$key .= '</tr>'."\n";
								$key .= '<tr>'."\n";
									$key .= '<td>&nbsp;'.$eReportCard['HKUGAGradeList']['CG'].'&nbsp;</td>'."\n";
									$key .= '<td>'.$eReportCard['HKUGAGradeList']['GradeArr']['CG'].'</td>'."\n";
								$key .= '</tr>'."\n";
								$key .= '<tr>'."\n";
									$key .= '<td>&nbsp;'.$eReportCard['HKUGAGradeList']['YG'].'&nbsp;</td>'."\n";
									$key .= '<td>'.$eReportCard['HKUGAGradeList']['GradeArr']['YG'].'</td>'."\n";
								$key .= '</tr>'."\n";
							$key .= '</table>'."\n";
							
						$key .= '</td>'."\n";
					$key .= '</tr>'."\n";
				$key .= '</table>'."\n";
				
				foreach((array)$GroupingIDArr as $thisGroupingID) // loop Class / Subject Group
				{
					if($ViewBy == 2) //SubjectGroup
					{
						if($GroupingInfoArr['SubjectGroupList'][$thisGroupingID]['SubjectID'] != $thisSubjectID) 
						continue;
					}
					
					$x .= '<div class="report_wrapper">'; // A3 Size
					
						// table title
						$x .= '<div class="table_wrapper">'."\n";
							$x .= '<table cellpadding=0 cellspacing=0 border=0 width="100%">'."\n";
								$x .= '<tr>'."\n";
									$x .= '<td align="center" width="85%">'."\n";
										$x .= getCurrentAcademicYear().' '.$GroupingTitleAssoc[$thisGroupingID]."\n";
									$x .= '</td>'."\n";
									$x .= '<td align="right"  width="15%">'."\n";
										$x .= date("d/m/y H:i")."\n";
									$x .= '</td>'."\n";
								$x .= '</tr>'."\n"; 
							$x .= '</table>'."\n";
						$x .= '</div>'."\n";
						
						// key, term, subject 
						$term = '<table cellpadding=2 cellspacing=2 border=0 width="85%" class="term_table">'."\n";
							$term .= '<tr>'."\n";
								$term .= '<td align="right" width="20%">'.$eReportCard['HKUGAGradeList']['Term'].':</td>'."\n";
								$term .= '<td align="left">'.$TermNumber.'</td>'."\n";
							$term .= '</tr>'."\n";	
							$term .= '<tr>'."\n";
								$term .= '<td align="right">'.$eReportCard['HKUGAGradeList']['Subject'].':</td>'."\n";
								$term .= '<td align="left">'.$SubjectNameArr[$thisSubjectID][0].'</td>'."\n";
							$term .= '</tr>'."\n";
						$term .= '</table>'."\n";
						
						$x .= '<div class="table_wrapper">'."\n";
							$x .= '<table cellpadding=0 cellspacing=0 border=0 width="100%">'."\n";
								$x .= '<tr valign="top">'."\n";
									$x .= '<td width=65%>'.$key.'</td>'."\n";
									$x .= '<td width=35% align="right">'.$term.'</td>'."\n";
								$x .= '</tr>'."\n"; 
							$x .= '</table>'."\n";
						$x .= '</div>'."\n";
						
						// grade list table
						#Get Student List
						$x .= '<div class="table_wrapper">'."\n";
							$x .= $lreportcard_ui->Get_HKUGA_Grade_List_Table($MarksAry, $thisSubjectID, $StudentArr[$thisGroupingID], $thisCmpSubjectArr, $PCDataArr[$thisSubjectID], $PCArr[$ReportID][$thisSubjectID], $CommentArr[$thisSubjectID], $ReportID );
						$x .= '</div>'."\n";
					
						//Signature 
						$x .= '<div class="table_wrapper" style="text-align:left">'."\n";
							$x .= '<table cellpadding=0 cellspacing=0 border=0 class="signiture">'."\n";
								$x .= '<tr>'."\n"; 
									$x .= '<td align="right">'.$eReportCard['HKUGAGradeList']['SubjectTeacher'].':</td>'."\n";
									$x .= '<td class="sign_line">&nbsp;</td>'."\n";
									$x .= '<td align="right">'.$eReportCard['HKUGAGradeList']['HeadOfDepartment'].':</td>'."\n";
									$x .= '<td class="sign_line">&nbsp;</td>'."\n";
								$x .= '</tr>'."\n"; 
								$x .= '<tr>'."\n"; 
									$x .= '<td align="right">'.$eReportCard['HKUGAGradeList']['Date'].':</td>'."\n";
									$x .= '<td class="sign_line">&nbsp;</td>'."\n";
									$x .= '<td align="right">'.$eReportCard['HKUGAGradeList']['Date'].':</td>'."\n";
									$x .= '<td class="sign_line">&nbsp;</td>'."\n";
								$x .= '</tr>'."\n"; 
							$x .= '</table>'."\n";
						$x .= '</div>'."\n";
						
					$x .= '</div>';								
				} // end loop Class / Subject Group
					
			} // end loop Subject
		
		$x .= '</td></tr></table>'."\n"; 
		
		echo $lreportcard->Get_Report_Header("Report Summary");
		echo $x;
?>
<style>
body
{
	font-family: Arial;
}

.key_table
{
	font-size:9px;
}

.key_table td
{
	padding-left:2px;
}

.term_table
{
	border:4px solid #000;
	font-size:12px;
	font-weight:bold;
}

.report_wrapper
{
	width:1000px; 
	page-break-after:always;
}

.table_wrapper
{
	padding-bottom: 5px;
}

.full_border
{
	border-collapse:collapse;
	border:1px solid #000;
}

.full_border td, .full_border th 
{
	border:1px solid #000;
}

.grade_list_table th, .grade_list_table td 
{
	font-size:8px;
	padding:1px;
}

.signiture
{
	font-size:16px;
}

.sign_line
{
	border-bottom:1px solid #000;
	width:200px;
}

</style>
<?
		echo $lreportcard->Get_Report_Footer();
	}
}
?>

