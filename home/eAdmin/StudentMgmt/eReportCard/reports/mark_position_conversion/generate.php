<?
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

intranet_auth();
intranet_opendb();

$ViewFormat = $_POST['ViewFormat'];
$YearID = $_POST['YearID'];
$ReportID = $_POST['ReportID'];
$SubjectIDArr = $_POST['SubjectIDArr'];


if ($plugin['ReportCard2008']) {
	include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
	
	if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
	{
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
		$lreportcard = new libreportcardcustom();
	} else {
		$lreportcard = new libreportcard();
	}
	
	if ($lreportcard->hasAccessRight()) 
	{
		### Form Name
		$objYear = new Year($YearID);
		$FormName = $objYear->YearName;
		
		
		### Subject Mark-Position Info
		$SubjectMarkPositionArr = array();
		$numOfSubject = count($SubjectIDArr);
		for ($i=0; $i<$numOfSubject; $i++)
		{
			### Get subject name
			$thisSubjectID = $SubjectIDArr[$i];
			
			### Get all student's mark of the subject
			$MarkArr = $lreportcard->getMarks($ReportID, '', '', $ParentSubjectOnly=1, $includeAdjustedMarks=1, $OrderBy='Mark Desc', $thisSubjectID, $ReportColumnID=0);
			
			### Consolidate Student's mark
			$repeatCount = 0;
			foreach($MarkArr as $StudentID => $SubjectMarkArr)
			{
				$thisMark = $SubjectMarkArr[$thisSubjectID][0]['Mark'];
				$thisFormPosition = $SubjectMarkArr[$thisSubjectID][0]['OrderMeritForm'];
				
				### Count numeric marks only
				if (is_numeric($thisMark) == false)
					continue;
				
				if (isset($SubjectMarkPositionArr[$thisSubjectID][$thisMark]) == false)
				{
					### First appearance of the mark
					$SubjectMarkPositionArr[$thisSubjectID][$thisMark]['FirstPosition'] = $thisFormPosition;
					$SubjectMarkPositionArr[$thisSubjectID][$thisMark]['PositionDisplay'] = $thisFormPosition;
					$SubjectMarkPositionArr[$thisSubjectID][$thisMark]['Count'] = 1;
				}
				else
				{
					### Set the position display to a range
					$thisFirstPosition = $SubjectMarkPositionArr[$thisSubjectID][$thisMark]['FirstPosition'];
					$thisLastPosition = $thisFirstPosition + $SubjectMarkPositionArr[$thisSubjectID][$thisMark]['Count'];
					$SubjectMarkPositionArr[$thisSubjectID][$thisMark]['PositionDisplay'] = $thisFirstPosition.' - '.$thisLastPosition;
					
					$SubjectMarkPositionArr[$thisSubjectID][$thisMark]['Count']++;
				}
			}
		}
		
		## Display the result
		if ($ViewFormat == 'html')
		{
			$table = '';
			foreach ($SubjectMarkPositionArr as $thisSubjectID => $thisMarkPositionArr)
			{
				$thisSubjectName = $lreportcard->GET_SUBJECT_NAME_LANG($thisSubjectID);
				
				$table .= "<table id='ResultTable' class='GrandMSContentTable' width='80%' border='1' cellpadding='5' cellspacing='0' style='page-break-after:always'>\n";
					$table .= "<thead>";
						$table .= "<tr>";
							$table .= "<td colspan='5'>";
								$table .= "<div width='100%'>".$eReportCard["Subject"].": ".$thisSubjectName."</div>";
								$table .= "<div width='100%'>".$eReportCard["FormName"].": ".$FormName."</div>";
							$table .= "</td>";
						$table .= "</tr>";
						$table .= "<tr>";
							$table .= "<td width='50%'>".$eReportCard['Mark']."</td>";
							$table .= "<td width='50%'>".$eReportCard['Template']['Position']."</td>";
						$table .= "</tr>";
					$table .= "</thead>";
					
					$table .= "<tbody>\n";
						$counter = 0;
						foreach ($thisMarkPositionArr as $thisMark => $thisMarkInfoArr)
						{
							$thisPosition = $thisMarkInfoArr['PositionDisplay'];
							
							$table .= "<tr>\n";
								$table .= "<td>".$thisMark."</td>\n";
								$table .= "<td>".$thisPosition."</td>\n";
							$table .= "</tr>\n";
						}
					$table .= "</tbody>\n";
				$table .= "</table>\n";
				$table .= "<br style='clear:both' />\n";
			}
			
			
			$css = $lreportcard->Get_GrandMS_CSS();
			
			$allTable = '<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td valign="top">'.$table.'</td>
							</tr>
						</table>';
			
			echo $lreportcard->Get_Report_Header($ReportTitle);
			echo $allTable.$css;
			echo $lreportcard->Get_Report_Footer();
			
			intranet_closedb();
		}
		else if ($ViewFormat == 'csv')
		{
			include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
			include_once($PATH_WRT_ROOT."includes/libexporttext.php");
			
			$ExportHeaderArr = array();
			$ExportContentArr = array();
			$lexport = new libexporttext();
			
			# Header
			$ExportHeaderArr[] = $eReportCard['Subject'];
			$ExportHeaderArr[] = $eReportCard['FormName'];
			$ExportHeaderArr[] = $eReportCard['Mark'];
			$ExportHeaderArr[] = $eReportCard['Template']['Position'];
			
			# Content
			$i_counter = 0;
			foreach ($SubjectMarkPositionArr as $thisSubjectID => $thisMarkPositionArr)
			{
				$thisSubjectName = $lreportcard->GET_SUBJECT_NAME_LANG($thisSubjectID);
				
				foreach ($thisMarkPositionArr as $thisMark => $thisMarkInfoArr)
				{
					$j_counter = 0;
					$thisPosition = $thisMarkInfoArr['PositionDisplay'];
					
					$ExportContentArr[$i_counter][$j_counter++] = $FormName;
					$ExportContentArr[$i_counter][$j_counter++] = $thisSubjectName;
					$ExportContentArr[$i_counter][$j_counter++] = $thisMark;
					$ExportContentArr[$i_counter][$j_counter++] = $thisPosition;
					
					$i_counter++;
				}
			}
			
			
			// Title of the grandmarksheet
			$filename = 'Mark_Position_Conversion_of_'.$FormName;
			$filename = str_replace(" ", "_", $filename);
			$filename = $filename.".csv";
			
			$export_content = $lexport->GET_EXPORT_TXT($ExportContentArr, $ExportHeaderArr);
			intranet_closedb();
			
			// Output the file to user browser
			$lexport->EXPORT_FILE($filename, $export_content);
		}
	}
}


?>