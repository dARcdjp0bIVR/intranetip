<?
//using: Bill

//@SET_TIME_LIMIT(1000);

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

intranet_auth();
intranet_opendb();
	
$lreportcard = new libreportcardcustom();
$lreportcard_ui = new libreportcard_ui();

$x = "";
$emptyDisplay = "---";
$exportArr = array();
$exportColumn = array();

$isFirst = true;
$table_count = 0;

$allColumns = array();
$MSStatArr = array();
$SubMSStatArr = array();
$Grading_Arr = array();
$classStudentArr = array();
$subjectTeacherArr = array();

# POST
$YearID = $_POST['YearID'];
$ReportID = $_POST['ReportID'];
$SubjectIDArr = $_POST['SubjectIDArr'];
$YearClassIDArr = $_POST['YearClassIDArr'];
$exportCSV = $_POST['submit_type'];

// [2015-1209-1703-15206]
# Subject Panel / Has Access Right
if ($_SESSION["SSV_PRIVILEGE"]["reportcard"]["is_subject_panel"] || $lreportcard->hasAccessRight()) 
{
	# Report Info
    $ReportTemplate = $lreportcard->returnReportTemplateBasicInfo($ReportID);
    $ReportSemester = $ReportTemplate['Semester'];
				
	# Related Info
    $Semester = $lreportcard->GET_ALL_SEMESTERS();
    if($ReportSemester != 'F'){
       	$Semester = array();
		$Semester[$ReportSemester] = $lreportcard->GET_ALL_SEMESTERS($ReportSemester);
	}
		
    $Subjects = $lreportcard->returnSubjectwOrder($YearID, 0, '', 'b5', '', 0, $ReportID, 0);
        
	# Year Class
	$YearClassNameArr = $lreportcard->GET_CLASSES_BY_FORM($YearID, "", 1, 1);
	foreach((array)$YearClassIDArr as $YearClassID){
		$students = $lreportcard->GET_STUDENT_BY_CLASS($YearClassID, $ParStudentIDList="", $isShowBothLangs=0, $withClassNumber=0, $isShowStyle=0, $ReturnAsso=1);
		$classStudentArr[$YearClassID] = $students;
	}
		
	# Get Related Term Report
	$relatedReports = $lreportcard->Get_Related_TermReport_Of_Consolidated_Report($ReportID, $MainReportOnly=1, $ReportColumnID="ALL");
	// For term report, only process 1 report 
	if($ReportTemplate['Semester'] != 'F'){
		$relatedReports[0] = $ReportTemplate;
	}
		
	# Loop Reports
	// Build settings array
	for($i=0; $i<count($relatedReports); $i++)
	{
		# Related Report Info
		$relatedReportID = $relatedReports[$i]['ReportID'];
		$TermID = $relatedReports[$i]['Semester'];
		
		$ReportColumn = $lreportcard->returnReportTemplateColumnData($relatedReportID);
		$grading_scheme = $lreportcard->GET_FROM_SUBJECT_GRADING_SCHEME($YearID, 0, $relatedReportID);
		
		$Grading_Arr[$TermID] = $grading_scheme;
		$TermReportColumnArr[$TermID] = BuildMultiKeyAssoc($ReportColumn, array('ReportColumnID'));
		
		$subjectList = array();
		# Loop ReportColumns
		foreach ((array)$ReportColumn as $ReportColumnObj)
		{
			# Report Column Info
			$ReportColumnID = $ReportColumnObj['ReportColumnID'];
			$ReportColumnInfo = $lreportcard->GET_SUB_MARKSHEET_COLUMN_INFO('', $ReportColumnID);
			
			# Loop Submarksheet Column
			for($j=0; $j<count($ReportColumnInfo); $j++)
			{
				# Submarksheet Column Info
				$columnSettings = $ReportColumnInfo[$j];
				$columnID = $columnSettings['ColumnID'];
				$columnTitle = $columnSettings['ColumnTitle'];
				$SubjectID = $columnSettings['SubjectID'];
				$FullMark = $columnSettings['FullMark'];;
				$PassingMark = $columnSettings['PassMark'];
				
				$subjectList[] = $SubjectID;
				
				$subjectGrading = $lreportcard->GET_GRADING_SCHEME_MAIN_INFO($grading_scheme[$SubjectID]['schemeID']);
				
				$allColumns[$TermID][$ReportColumnID][$SubjectID][] = $columnSettings;
					
				# Set Parent and Component Subject ID
				$componentSubjectID = 0;
				$parentSubjectID = $lreportcard->GET_PARENT_SUBJECT_ID($SubjectID);
				// for component subject
				if($parentSubjectID){
					$componentSubjectID = $SubjectID;
					$SubjectID = $parentSubjectID;
				}
				
				foreach((array)$YearClassIDArr as $YearClassID){
					$studentList = array_keys((array)$classStudentArr[$YearClassID]);
					if(!isset($subjectTeacherArr[$YearClassID][$TermID][$SubjectID])){
						$subjectTeacher = $lreportcard->returnSubjectTeacher($YearClassID, $SubjectID, $relatedReportID, $studentList[0], 1);
						$subjectTeacherArr[$YearClassID][$TermID][$SubjectID] = $subjectTeacher[0]['ChineseName'];
					}
				}
				
				// if current subject not in selected subjects
				if(!in_array($SubjectID, (array)$SubjectIDArr)){
					continue;
				}
								
				# Submarksheet Score
				$SubMSScore = $lreportcard->GET_SUB_MARKSHEET_SCORE($ReportColumnID, ($componentSubjectID? $componentSubjectID : $SubjectID));
				
				foreach((array)$YearClassIDArr as $YearClassID){
					$studentList = array_keys((array)$classStudentArr[$YearClassID]);
					
					$Marks = array();
					$minMark = 0;
					$maxMark = 0;
					$averageMark = 0.00;
					$passStudent = 0;
					$totalStudent = 0;
					$passRatio = 0.00;
					foreach((array)$studentList as $currentStudentID){
						$studentInput = $SubMSScore[$currentStudentID];
						$score = $studentInput[$columnID];
						if(isset($score) && $score != '-3'){
						$Marks[] = $score;
						$totalStudent++;
								
						if($score >= $PassingMark){
							$passStudent++;
							}
						}
					}
						
					if(count($Marks) > 0){
						$minMark = min($Marks);
						$maxMark = max($Marks);
						$averageMark = $lreportcard->getAverage($Marks);
						$averageMark = my_round($averageMark, 2);
						$passRatio = $passStudent / $totalStudent * 100;
						$passRatio = my_round($passRatio, 2);
					}
	
					$SubMSStatArr[$YearClassID][$TermID][$SubjectID][$componentSubjectID][$columnID] = array($columnTitle, $FullMark, $passRatio, $maxMark, $minMark, $averageMark);
				}
				
				# Marksheet Score
//				foreach((array)$YearClassIDArr as $YearClassID){
//					$studentList = array_keys((array)$classStudentArr[$YearClassID]);
//					$MarkSheetScore = $lreportcard->getMarks($relatedReportID, '', " AND a.StudentID IN ('".implode("','", $studentList)."')");
//												
//					$Marks = array();
//					$minMark = 0;
//					$maxMark = 0;
//					$passRatio = 0.00;
//					$passStudent = 0;
//					$averageMark = 0.00;
//					$totalStudent = 0;
//					foreach((array)$studentList as $currentStudentID){
//						$studentInput = $MarkSheetScore[$currentStudentID][$componentSubjectID][$ReportColumnID];
//							
//						if($studentInput['Grade'] != 'N.A.'){
//							$score = $studentInput['Mark'];
//							$Marks[] = $score;
//							$totalStudent++;
//								
//							if($score > $subjectGrading['PassMark']){
//								$passStudent++;
//							}
//						}
//					}
//					if(count($Marks) > 0){
//						$minMark = min($Marks);		
//						$maxMark = max($Marks);
//						$averageMark = $lreportcard->getAverage($Marks);
//						$averageMark = my_round($averageMark, 2);
//						$passRatio = $passStudent / $totalStudent * 100;
//						$passRatio = my_round($passRatio, 2);
//					}
//					$MSStatArr[$YearClassID][$TermID][$SubjectID][$componentSubjectID][$ReportColumnID] = array($columnTitle, $subjectGrading['FullMark'], $averageMark, $maxMark, $minMark, $averageMark);
					
					# Parent Subject
//					if(!isset($MSStatArr[$YearClassID][$TermID][$SubjectID][0][$ReportColumnID])){
//						$parentSubjectGrading = $lreportcard->GET_GRADING_SCHEME_MAIN_INFO($grading_scheme[$SubjectID]['schemeID']);
//						$fullMarkArr[$TermID][$ReportColumnID][$SubjectID] = $parentSubjectGrading;
//						
//						$Marks = array();
//						$minMark = 0;
//						$maxMark = 0;
//						$passRatio = 0.00;
//						$passStudent = 0;
//						$averageMark = 0.00;
//						$totalStudent = 0;
//						foreach((array)$studentList as $currentStudentID){
//							$studentInput = $MarkSheetScore[$currentStudentID][$SubjectID][$ReportColumnID];
//						
//							if($studentInput['Grade'] != 'N.A.'){
//								$score = $studentInput['Mark'];
//								$Marks[] = $score;
//								$totalStudent++;
//									
//								if($score > $parentSubjectGrading['PassMark']){
//									$passStudent++;
//								}
//							}
//						}
//						if(count($Marks) > 0){
//							$minMark = min($Marks);		
//							$maxMark = max($Marks);
//							$averageMark = $lreportcard->getAverage($Marks);
//							$averageMark = my_round($averageMark, 2);
//							$passRatio = $passStudent / $totalStudent * 100;
//							$passRatio = my_round($passRatio, 2);
//						}
//						$MSStatArr[$YearClassID][$TermID][$SubjectID][0][$ReportColumnID] = array($columnTitle, $parentSubjectGrading['FullMark'], $averageMark, $maxMark, $minMark, $averageMark);
//					}
//				}
			}
		}
		
		$subjectList = (array)array_unique((array)$subjectList);
		sort($subjectList);
		$termStatArr[$TermID] = $lreportcard->Get_Mark_Statistics_Info($relatedReportID, $subjectList, 2, 2, 2, 0, 1, "", 0);
		
	}
	
	$x .= "<div class='main_container'>";
		
	# Loop Selected Subject
	foreach((array)$SubjectIDArr as $currentSubjectID)
	{
		# Selected Subject Info
		$currentMajorSubject = $Subjects[$currentSubjectID];
		$currentMajorSubjectName = $currentMajorSubject[0];
		$componentSubjectCount = count($currentMajorSubject);
		
		foreach($Semester as $SemesterID => $currentSemester)
		{
			$TermSquence = $lreportcard->Get_Semester_Seq_Number($SemesterID);
			$currentColumns = $allColumns[$SemesterID];
			
			
			# Loop Year Class
			foreach($YearClassIDArr as $YearClassID){
//				$isPrintHeader = false;
				$currentClassName = $YearClassNameArr[$YearClassID];
				$currentClassStudent = $classStudentArr[$YearClassID];
				$subjectTeacherName = $subjectTeacherArr[$YearClassID][$SemesterID][$currentSubjectID];
				
//				if(!$isPrintHeader){
					if($table_count != 0 && !($table_count % 2)){
						$x .= "</div>";
						$x .= "<div class='page_break'>&nbsp;</div>";
						$x .= "<div class='main_container'>";
					}
					$table_count++;
//				}
				
//				if(!$isPrintHeader){
					$x .= "<table class='border_table' width='100%'>";
					$x .= "<tr>";
					$x .= "<td width=15%'>$currentMajorSubjectName</td><td colspan='2' width='35%'> $currentClassName 任教老師： $subjectTeacherName </td> <td width='8%'> 滿分 </td> <td width='12%'> 及格率 </td> <td width='10%'> 最高分 </td> <td width='10%'> 最低分 </td> <td width='10%'> 平均分 </td> </tr>";
					$dataArr = array();
					$dataArr[] = $currentMajorSubjectName;
					$dataArr[] = $currentClassName."任教老師：".$subjectTeacherName;
					$dataArr[] = "滿分";
					$dataArr[] = "及格率";
					$dataArr[] = "最高分";
					$dataArr[] = "最低分";
					$dataArr[] = "平均分";
					if($isFirst){
						$exportColumn = $dataArr;
						$isFirst = false;
					}
					else {
						$exportArr[] = $dataArr;
					}
					
//					$isPrintHeader = true;
//				}
				
				$assessmentSq = 1;
				# Loop Report Column
				foreach((array)$currentColumns as $currentColumnID => $currentColumn)
				{
					# Report Column Info
					$currentColumnTitle = $TermReportColumnArr[$SemesterID][$currentColumnID]['ColumnTitle'];
					$assessmentCode = "T".$TermSquence."A".$assessmentSq;
					
					// Calculate Rowspan
					$termCount = 0;
					foreach($currentMajorSubject as $componentSubjectID => $currentComponentSubject){
						if(!$componentSubjectID && $componentSubjectCount != 1)
							continue;
						
						$componentCount = count($currentColumn[($componentSubjectID? $componentSubjectID : $currentSubjectID)]);
						$termCount += $componentCount? ($componentCount+1) : 1;
					}
					$termCount = $termCount? $termCount : 1;
					
					# Semester Cell Display
					$displaySemester = "<td rowspan='$termCount'> $currentSemester <br> $currentColumnTitle <br> (T".$TermSquence."A".$assessmentSq.")</td>";
					
					# Loop Component Subject
					foreach($currentMajorSubject as $componentSubjectID => $currentComponentSubject)
					{
						# Hide Major Subject
						if(!$componentSubjectID && $componentSubjectCount != 1){
							continue;
						}
						# Set subject name as 主卷  if major subject
						if(!$componentSubjectID){
							$currentComponentSubject = "總分";
						}
						
						$subjectColumn = $currentColumn[($componentSubjectID? $componentSubjectID : $currentSubjectID)];
						$subjectComponentCount = count($subjectColumn);
						$subjectComponentCount = $subjectComponentCount? $subjectComponentCount+1 : 1;
						$displayComponentSubject = "<td rowspan='$subjectComponentCount'> $currentComponentSubject </td>";
						
						foreach((array)$subjectColumn as $columnContent){
							$subMSColumnID = $columnContent['ColumnID'];
							$subMSStatData = $SubMSStatArr[$YearClassID][$SemesterID][$currentSubjectID][$componentSubjectID][$subMSColumnID];
							$x .= "<tr> $displaySemester$displayComponentSubject";
							
							$dataArr = array();
							$dataArr[] = ($displaySemester != "&nbsp;")? $currentSemester.$currentColumnTitle."(T".$TermSquence."A".$assessmentSq.")" : "";
							$dataArr[] = ($displayComponentSubject != "&nbsp;")? $currentComponentSubject : "";
							for($cols=0; $cols<6; $cols++){
								$displayContent = $subMSStatData[$cols];
								$displayContent = isset($displayContent)? $displayContent : $emptyDisplay;
								$displayContent .= $cols==2? "%" : "";
								
								$x .= "<td>$displayContent</td>";
								$dataArr[] = $displayContent;
							}
							
							$x .= "</tr>";
							$exportArr[] = $dataArr;
							
							$displaySemester = "&nbsp;";
							$displayComponentSubject = "&nbsp;";
						}
						
//						$MSStatData = $MSStatArr[$YearClassID][$SemesterID][$currentSubjectID][$componentSubjectID][$currentColumnID];
						$MSStat = $termStatArr[$SemesterID][$currentColumnID][($componentSubjectID? $componentSubjectID : $currentSubjectID)][$YearClassID];
						$MSStatData = array();
						$MSStatData[2] = $MSStat['Nature']['TotalPassPercentage'];
						$MSStatData[3] = $MSStat['MaxMark'];
						$MSStatData[4] = $MSStat['MinMark'];
						$MSStatData[5] = $MSStat['Average_Rounded'];
						
						$x .= "<tr> $displaySemester$displayComponentSubject";
						$dataArr = array();
						$dataArr[] = ($displaySemester != "&nbsp;")? $currentSemester.$currentColumnTitle."(T".$TermSquence."A".$assessmentSq.")" : "";
						$dataArr[] = ($displayComponentSubject != "&nbsp;")? $currentComponentSubject : "";
						
						for($cols=0; $cols<6; $cols++){
							$displayContent = $cols!=0? $MSStatData[$cols] : "總分";
							
							$colSchemeID = $Grading_Arr[$SemesterID][($componentSubjectID? $componentSubjectID : $currentSubjectID)]['schemeID'];
							if($cols==1 && !isset($displayContent)){
								$colScheme = $lreportcard->GET_GRADING_SCHEME_MAIN_INFO($colSchemeID);
							}
							$displayContent = $cols==1 && !isset($displayContent)? $colScheme['FullMark'] : $displayContent;
							$displayContent = isset($displayContent)? $displayContent : $emptyDisplay;
							$displayContent .= $cols==2? "%" : "";
							
							$x .= "<td>$displayContent</td>";
							$dataArr[] = $displayContent;
						}
						
						$x .= "</tr>";
						$exportArr[] = $dataArr;
						
						$displaySemester = "&nbsp;";
					}
					$displaySemester = "&nbsp;";
					
					$assessmentSq++;					
				}
				$x .= "</table>";	
				$exportArr[] = array("");
			}
		}
	}

	if($exportCSV == "1"){
		$lexport = new libexporttext();
		$export_content = $lexport->GET_EXPORT_TXT($exportArr, $exportColumn,  "", "\r\n", "", 0, "11",1);		
		
		intranet_closedb();
		
		// Output CSV File
		$filename = "Class_Subject_Component_Analysis.csv";
		$filename = str_replace(' ', '_', $filename);
		$lexport->EXPORT_FILE($filename, $export_content);
	
	} else {
		
		$x .= "</div>";
		echo $x;
		
		echo'<style>';
		include_once($PATH_WRT_ROOT."file/reportcard2008/templates/general.css");
		echo '</style>';	
		
		$style_css  = "html, body { margin:0px; padding:0px; }\n";
		$style_css .= "body { font-family:'Times New Roman, Times, serif, 新細明體'; font-size:13px }\n";
//		$style_css .= ".main_container {display:block; width:750px; height:850px; margin:auo; padding:120px 20px; position:relative; padding-bottom:100px; clear:both; border:0px solid green;}\n";
		$style_css .= ".main_container { display:block; width:680px; height:850px; margin:auto; padding:60px 20px; position:relative; padding-bottom:20px; clear:both; border:0px solid green; }\n";
		$style_css .= ".page_break {page-break-after:always; margin:0; padding:0; line-height:0; font-size:0; height:0;}";
		
		$style_css .= ".border_table { width: 100%; margin-bottom:60px; }";
		$style_css .= ".border_table td { padding-left:4px; }";
		$style_css .= ".border_table > tbody > tr:first-child td { vertical-align:middle; }";
		
		echo "<style>";
		echo $style_css;
		echo "</style>";	
		
		intranet_closedb();
	}
	
} else {
	
	echo "You have no priviledge to access this page.";
	intranet_closedb();
	die();
	
}	

//							$x .= "<tr> $displaySemester $displayComponentSubject <td> $CATitle </td> <td> $fullMark </td> <td> $passRatio% </td> <td> $maxMark </td> <td> $minMark </td> <td> $averageMark </td> </tr>";
//							$SubMSStatArr[$YearClassID][$termID][$currentSubjectID][$componentSubjectID][$columnID] = array($columnTitle, $FullMark, $passRatio, $maxMark, $minMark, $averageMark);
							
//							if(!$componentSubjectID)
//								$SubMSScore = $lreportcard->GET_SUB_MARKSHEET_SCORE($currentColumnID, $currentSubjectID);
//							else 
//								$SubMSScore = $lreportcard->GET_SUB_MARKSHEET_SCORE($currentColumnID, $componentSubjectID);
//							
//							# Get Subject Submarksheet Column Settings
//							$currentSubjectSettings = $subjectSettingsArr[$SemesterID][$currentColumnID][$currentSubjectID][$componentSubjectID];
//							$componentCount = count($currentSubjectSettings);	
//							if($componentCount == 0){
//								$displayComponentSubject = "<td colspan='".($componentCount+2)."'>$currentComponentSubject</td>";
//								$x .= "<tr>$displaySemester$displayComponentSubject<td colspan='6'>無記錄</td></tr>";
//							}
//							
//							// Loop Subject Submarksheet Column Settings
//							for($i=0; $i<$componentCount; $i++)
//							{
//								# Subject Submarksheet Column Settings
//								$currentComponentSubjectSettings = $currentSubjectSettings[$i];
//								$SubMSColumnID = $currentComponentSubjectSettings['ColumnID'];
//								$CATitle = $currentComponentSubjectSettings['ColumnTitle'];
//								$CATitle = $CATitle? $CATitle : "&nbsp;";
//								$fullMark = $currentComponentSubjectSettings['FullMark'];
//								$passMark = $currentComponentSubjectSettings['PassMark'];
////								$ratio = $currentComponentSubjectSettings['Ratio'];
//								
//								# Initiate
//								$totalStudent = 0;
//								$passStudent = 0;
//								$passRatio = 0;
//								$minMark = 0;
//								$maxMark = 0; 
//								$totalMark = 0;
//								
//								$SubMSSolumnArr = array();
//								foreach($SubMSScore as $currentStudentID => $subMSScoreInput){
////									if(in_array($currentStudentID, $excludeStudentAssoArr[$SemesterID]))
////										continue;
//									
//									$subMSSolumnInput = $subMSScoreInput[$SubMSColumnID];
//									
//									if($subMSSolumnInput == -3){
//										continue;
//									}
//									
//									$SubMSSolumnArr[] = $subMSSolumnInput;
//									$totalStudent++;
//
//									if($subMSSolumnInput >= $passMark)
//										$passStudent++;
//
//								}
//								
//								if($totalStudent > 0){				
//									$minMark = min($SubMSSolumnArr);		
//									$maxMark = max($SubMSSolumnArr);
//									$averageMark = $lreportcard->getAverage($SubMSSolumnArr);
//									$averageMark = my_round($averageMark, 2);
//									$passRatio = $passStudent / $totalStudent * 100;
//									$passRatio = my_round($passRatio, 2);
//								}
//								
//								# Component Subject and Ratio Display
//								if($i==0){
//									$displayComponentSubject = "<td rowspan='".($componentCount+1)."'>$currentComponentSubject</td>";
//								} else {
//									$displayComponentSubject = "&nbsp;";
//									$displayComponentRatio = "&nbsp;";
//								}
//								
//								$x .= "<tr> $displaySemester $displayComponentSubject <td> $CATitle </td> <td> $fullMark </td> <td> $passRatio% </td> <td> $maxMark </td> <td> $minMark </td> <td> $averageMark </td> </tr>";
//								
							
//							if($componentCount > 0){
//								
//								$totalStudent = 0;
//								$passStudent = 0;
//								$passRatio = 0;
//								$minMark = 0;
//								$maxMark = 0; 
//								$totalMark = 0;
//								
//								$MSSolumnArr = array();
//								$inputScore = $MarksheetScore[$componentSubjectID];
//								foreach ($inputScore as $MarkContent){
//									if($MarkContent['MarkType'] == 'M'){
//										$MSSolumnArr[] = $MarkContent['MarkRaw'];
//										$totalStudent++;
//
////										if($subMSSolumnInput >= $passMark)
////											$passStudent++;
////										}
//									}
//								}
//								
//								if($totalStudent > 0){				
//									$minMark = min($MSSolumnArr);		
//									$maxMark = max($MSSolumnArr);
//									$averageMark = $lreportcard->getAverage($MSSolumnArr);
//									$averageMark = my_round($averageMark, 2);
//									$passRatio = $passStudent / $totalStudent * 100;
//									$passRatio = my_round($passRatio, 2);
//								}
//								
//								$x .= "<tr> <td> 總分 </td> <td> $fullMark </td> <td> $passRatio% </td> <td> $maxMark </td> <td> $minMark </td> <td> $averageMark </td> </tr>";
//								//								debug_pr($lreportcard->GET_MARKSHEET_INPUT_SCORE($currentReportID, $currentColumnID, 1971, (array)$componentSubjectID));
////								debug_pr($stat_info = $lreportcard->Get_Mark_Statistics_Info($currentReportID, (array)$componentSubjectID, $Average_Rounding=2, $SD_Rounding=2, $Percentage_Rounding=2, $SubjectMarkRounding=0, $IncludeClassStat=1, $ReportColumnID=0, $forSubjectGroup=0));
//							}
?>