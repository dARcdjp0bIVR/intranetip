<?
@SET_TIME_LIMIT(1000);
//using: Bill

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$lreportcard = new libreportcardcustom();
$lreportcard_ui = new libreportcard_ui();
$scm = new subject();
 
$x = "";
$emptyDisplay = "---";
$exportArr = array();
$exportColumn = array();

$isFirst = true;
$table_count = 0;

$ClassNameArr = array();
$SubjectGroupIDArr = array();
$reportColumnArr = array();
$subjectContentArr = array();

# POST
$TeachingStaffIDArr = $_POST['TeachingStaffIDArr'];
$exportCSV = $_POST['submit_type'];

// [2015-1209-1703-15206] 
if ($_SESSION["SSV_PRIVILEGE"]["reportcard"]["is_subject_panel"] || $lreportcard->hasAccessRight()) 
{	
	# Reports
    $ReportIDArr = $lreportcard->Get_Report_List('', 'T');
    $SemesterList = $lreportcard->GET_ALL_SEMESTERS();
	
	# Class List
    $ClassNameArr = $lreportcard->GET_CLASSES_BY_FORM("", "", 0, 1);
    $ClassMatchArr = BuildMultiKeyAssoc($ClassNameArr, array('ClassTitleB5'));
    
    // [2015-1209-1703-15206]
	# Subject Panel Info
	$SubjectPanelInfo = $lreportcard->returnSubjectPanelInfo($UserID);
	$SubjectPanelInfo = BuildMultiKeyAssoc((array)$SubjectPanelInfo, array("YearID", "SubjectID"));
    
	$x .= "<div class='main_container'>"; 
	
    # loop: Teaching Staff
    foreach($TeachingStaffIDArr as $TeachingStaffID){
    	$SubjectTermContentArr = array();
    	
		$TeacherName = $lreportcard->returnUserName(array($TeachingStaffID), 'b5');
		$TeacherName = $TeacherName[0]['ChineseName'];
		
		$withContent = false;
		$subjectContentArr = array();
		
        # Teaching Class List
        $SubjectIDArr = array_unique((array)$lreportcard->returnSubjectTeacherClassInfo($TeachingStaffID));
	
        # loop: Report List - Subject Group List
        foreach($ReportIDArr as $ReportInfo){
        		
        	# Report Info
        	$ReportID = $ReportInfo['ReportID'];
        	$ClassLevelID = $ReportInfo['ClassLevelID'];
        	$Semester = $ReportInfo['Semester'];
        	$isMainReport = $ReportInfo['isMainReport'];
        
        	$SubjectGroupIDArr = array_merge($SubjectGroupIDArr, array_keys($scm->Get_Subject_Group_List($Semester, $ClassLevelID, '', $TeachingStaffID, 1, '', '', true)));	
//      	$reportColumnArr[$ReportID] = BuildMultiKeyAssoc($lreportcard->returnReportTemplateColumnData($ReportID), array('SemesterNum', 'ReportColumnID'));
        }
        $SubjectGroupIDArr = array_unique($SubjectGroupIDArr);
        	
        # loop: Report List - ClassName List
        foreach($SubjectGroupIDArr as $SubjectGroupID){
        	$studentList = $lreportcard->GET_STUDENT_BY_SUBJECT_GROUP($SubjectGroupID);
        		
        	foreach($studentList as $student){
        		$existClassArr[] = $studentList[0]['ClassTitleCh'];
        	}
        }
        $existClassArr = array_unique((array)$existClassArr);
        	        	
       	foreach($ReportIDArr as $ReportInfo){
        	# Report Info
        	$ReportID = $ReportInfo['ReportID'];
        	$ClassLevelID = $ReportInfo['ClassLevelID'];
        	$Semester = $ReportInfo['Semester'];
        	$isMainReport = $ReportInfo['isMainReport'];
        	
        	// [2015-1209-1703-15206] Skip if only Subject Panel and without current Class Level 
        	if(!$lreportcard->hasAccessRight(true) && !isset($SubjectPanelInfo[$ClassLevelID]))
        		continue;
        	
        	if($isMainReport && $Semester)
        	{
	       		# Mark Statistics
	       		$stat_info = $lreportcard->Get_Mark_Statistics_Info($ReportID, $SubjectIDArr, $Average_Rounding=2, $SD_Rounding=2, $Percentage_Rounding=2, $SubjectMarkRounding=0, $IncludeClassStat=1, $ReportColumnID=0, $forSubjectGroup=0);
				
				# Semester
				$SemesterName = $SemesterList[$Semester];
				$SemesterSquence = $lreportcard->Get_Semester_Seq_Number($Semester);
				$semesterDisplay = "$SemesterName (T$SemesterSquence)";
					
				if(isset($stat_info) && count($stat_info) > 0){
					
					foreach($stat_info as $currentSubjectID => $SubjectInfo){
						$SubjectName = $lreportcard->GET_SUBJECT_NAME_LANG($currentSubjectID, 'b5');
        	
        				// [2015-1209-1703-15206] Skip if only Subject Panel and without current Subject
			        	if(!$lreportcard->hasAccessRight(true) && !isset($SubjectPanelInfo[$ClassLevelID][$currentSubjectID]))
			        		continue;
						
						if(in_array($currentSubjectID, $SubjectIDArr)){
							$SubjectFullMark = $lreportcard->GET_SUBJECT_FULL_MARK($currentSubjectID, $ClassLevelID, $ReportID);
							
							foreach($SubjectInfo as $currentClassID => $ClassInfo){
								$ClassName = $ClassNameArr[$currentClassID]['ClassTitleB5'];
								if(!in_array($ClassName, $existClassArr))
									continue;
									
//								debug_pr($ReportID);
//								debug_pr('Semester	'.$Semester);
//								debug_pr('Semester	'.$semesterDisplay);
//								debug_pr('Subect Name	'.$SubjectName);
//								debug_pr('ClassName:	'.$ClassName);
									
								$PassRatio = $ClassInfo['Nature']['TotalPassPercentage'];
								$MaxMark = $ClassInfo['MaxMark'];
								$MinMark = $ClassInfo['MinMark'];
								$AverageMark = $ClassInfo['Average_Rounded'];
								
//								debug_pr('ClassName:	'.$PassRatio);
								
//								$SubjectTermContentArr[$currentSubjectID][$Semester][$currentClassID][] = $SubjectName;
								$SubjectTermContentArr[$currentSubjectID][$Semester][$currentClassID][] = $semesterDisplay;
								$SubjectTermContentArr[$currentSubjectID][$Semester][$currentClassID][] = $ClassName;
								$SubjectTermContentArr[$currentSubjectID][$Semester][$currentClassID][] = $SubjectFullMark;
								$SubjectTermContentArr[$currentSubjectID][$Semester][$currentClassID][] = $PassRatio."%";
								$SubjectTermContentArr[$currentSubjectID][$Semester][$currentClassID][] = $MaxMark;
								$SubjectTermContentArr[$currentSubjectID][$Semester][$currentClassID][] = $MinMark;
								$SubjectTermContentArr[$currentSubjectID][$Semester][$currentClassID][] = $AverageMark;
							}
						}
					}
        		}
        	}        	
        }

        if(count($SubjectTermContentArr) > 0){
        	foreach($SubjectTermContentArr as $currentSubjectID => $currentContentSubject){
        		$curSubjectName = $lreportcard->GET_SUBJECT_NAME_LANG($currentSubjectID, 'b5');
        		
        		if($table_count != 0 && !($table_count % 4)){
					$x .= "</div>";
					$x .= "<div class='page_break'>&nbsp;</div>";
					$x .= "<div class='main_container'>";
				}
				$table_count++;

				# 1st Header [Start]
				$x .= "<table class='border_table' width='100%'>";
				$x .= "<tr><td>&nbsp;</td><td colspan='3'> 任教老師： $TeacherName </td> <td colspan='3'> 科目： $curSubjectName </td> </tr>";
				$dataArr = array();
				$dataArr[] = "";
				$dataArr[] = "任教老師：". $TeacherName;
				$dataArr[] = "";
				$dataArr[] = "";
				$dataArr[] = $curSubjectName;
				if($isFirst){
					$exportColumn = $dataArr;
					$isFirst = false;
				}
				else {
					$exportArr[] = $dataArr;
				}
				# 1st Header [End]
				
				# 2nd Header [Start]
				$x .= "<tr> <td> 學期 </td> <td> 班別 </td> <td> 滿分 </td> <td> 及格率 </td> <td> 最高分 </td> <td> 最低分 </td> <td> 平均分 </td> </tr>";
				
				$dataArr = array();
				$dataArr[] = "學期";
				$dataArr[] = "班別";
				$dataArr[] = "滿分";
				$dataArr[] = "及格率";
				$dataArr[] = "最高分";
				$dataArr[] = "最低分";
				$dataArr[] = "平均分";
				$exportArr[] = $dataArr;
				# 2nd Header [End]
				
				$withContent = true;
				$subjectContentArr[$currentSubjectID] = "";
			
	    		foreach($currentContentSubject as $currentContentSemester){
	    			foreach($currentContentSemester as $currentClassID => $content){
	    				list($curSemesterDisplay, $curClassName, $curSubjectFullMark, $curPassRatio, $curMaxMark, $curMinMark, $curAverageMark) = $content;
	    				
						$x .= "<tr> <td> $curSemesterDisplay </td> <td> $curClassName </td> <td> $curSubjectFullMark </td> <td> $curPassRatio </td> <td> $curMaxMark </td> <td> $curMinMark </td> <td> $curAverageMark </td> </tr>";	
						$dataArr = array();
						$dataArr[] = $curSemesterDisplay;
						$dataArr[] = $curClassName;
						$dataArr[] = $curSubjectFullMark;
						$dataArr[] = $curPassRatio;
						$dataArr[] = $curMaxMark;
						$dataArr[] = $curMinMark;
						$dataArr[] = $curAverageMark;
						$exportArr[] = $dataArr;
	    			}
	        	}
        	}
        }
        
        if($withContent){
			$x .= "</table>";
			$exportArr[] = array("");
        }
	}
	
	if($exportCSV == "1"){
		$lexport = new libexporttext();
		$export_content = $lexport->GET_EXPORT_TXT($exportArr, $exportColumn,  "", "\r\n", "", 0, "11",1);		
		
		intranet_closedb();
		
		// Output CSV File
		$filename = "Teacher_Subject_Result_Analysis.csv";
		$filename = str_replace(' ', '_', $filename);
		$lexport->EXPORT_FILE($filename, $export_content);
	
	} else {
		
		$x .= "</div>";
		echo $x;
		
		echo'<style>';
		include_once($PATH_WRT_ROOT."file/reportcard2008/templates/general.css");
		echo '</style>';
		
		$style_css  = "html, body { margin:0px; padding:0px; }\n";
		$style_css .= "body { font-family:'Times New Roman, Times, serif, 新細明體'; font-size:13px }\n";
//		$style_css .= ".main_container {display:block; width:750px; height:850px; margin:auo; padding:120px 20px; position:relative; padding-bottom:100px; clear:both; border:0px solid green;}\n";
		$style_css .= ".main_container { display:block; width:680px; height:850px; margin:auto; padding:60px 20px; position:relative; padding-bottom:20px; clear:both; border:0px solid green; }\n";
		$style_css .= ".page_break {page-break-after:always; margin:0; padding:0; line-height:0; font-size:0; height:0;}";
		
		$style_css .= ".border_table { width: 100%; margin-bottom:60px; }";
		$style_css .= ".border_table td { padding-left:4px; }";
		$style_css .= ".border_table > tbody > tr:first-child td { vertical-align:middle; }";
		
		echo "<style>";
		echo $style_css;
		echo "</style>";		
		
		intranet_closedb();
	}
	
} else {
	
	echo "You have no priviledge to access this page.";
	intranet_closedb();
	die();
	
}

?>