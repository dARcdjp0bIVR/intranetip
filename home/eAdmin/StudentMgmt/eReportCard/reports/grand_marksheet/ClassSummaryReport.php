<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008w.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_sis.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$lreportcard = new libreportcardSIS();
$linterface = new interface_html();
$lclass = new libclass();

# Retrieve params
$ReportColumnID	= $ReportColumnID ? $ReportColumnID 	: "0";
$SubjectID		= $SubjectID 	? $SubjectID 	: "";
$ClassLevelID	= $ClassLevelID	? $ClassLevelID : "";
$ClassID		= $ClassID		? $ClassID : "";
$ReportID		= $ReportID		? $ReportID : "";
$ClassLevel 	= $lreportcard->returnClassLevel($ClassLevelID);
$PSLevel		= substr($ClassLevel, 0, 1);
$Level			= substr($ClassLevel, 1, 1);
$ReportSetting 	= $lreportcard->returnReportTemplateBasicInfo($ReportID);
$SemID 			= $ReportSetting['Semester'];
$ReportType 	= $SemID == "F" ? "W" : "T";


$StudentAry = array();
$ListAry = array();
$DataTemp = array();
$SubjectTotal = array();

if(!$ClassLevelID || !$ReportID || $SubjectID || !$ClassID )
{
	intranet_closedb();
	header("Location: index.php");
}

include_once($PATH_WRT_ROOT."templates/2007a/layout/print_header.php");

# Retrieve Student Array
$classArr = $lreportcard->GET_CLASSES_BY_FORM($ClassLevelID, $ClassID);
foreach($classArr as $key => $data)
{
	$thisClassID = $data['ClassID'];
	$StudentAryTemp = $lreportcard->GET_STUDENT_BY_CLASS($thisClassID);
	foreach($StudentAryTemp as $k=>$d)
	{
		list($thisUserID, $WebSAMS, $ClassNumber, $EngName) = $d;
		$StudentAry[] = $thisUserID;
		
		$lu = new libuser($thisUserID);
		$DataTemp[$thisUserID]['StudentName'] = $lu->EnglishName ." (". $lu->ChineseName .")";
		$DataTemp[$thisUserID]['ClassName'] = $lu->ClassName;
		$DataTemp[$thisUserID]['AdmissionNo'] = $lu->AdmissionNo;
		$DataTemp[$thisUserID]['ClassNumber'] = $lu->ClassNumber;
	}
}

# Subject Array
$SubjectAryTmp = $lreportcard->returnSubjectwOrder($ClassLevelID);

foreach($SubjectAryTmp as $s => $temp)
{
	$thisSubjectFormGradingSettings = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $s);
	$thisScaleDisplay = $thisSubjectFormGradingSettings[ScaleDisplay];
	
	# check Subject wegith is 0 or not
	$thisWeightAry = $lreportcard->returnReportTemplateSubjectWeightData($ReportID, "SubjectID=$s and ReportColumnID is NULL");
	$thisWeight = $thisWeightAry[0]['Weight'];
	
	if($thisScaleDisplay=="M" && $thisWeight)			$SubjectAry[] = $s;
}

# Grand MS  Title
$SubjectStr ="All Subjects";	
$SemNameAry = $lreportcard->returnReportColoumnTitle($ReportID);
if($PSLevel=="P")
{
	if($Level==1 || $Level==2)
		$SemStr = "Overall";
	else
	{
		if($ReportType=="T")
			$SemStr = "SA1";
		else
			$SemStr = $ReportColumnID ? "SA2 " : "Overall";
	}	
}
else
{
	$ReportTypeArr = $lreportcard->GET_REPORT_TYPES($ClassLevelID);
	foreach($ReportTypeArr as $k=>$d)
		$ReportIDList[] = $d['ReportID'];
	sort($ReportIDList);
	$ReportIDOrder = array_search($ReportID, $ReportIDList);
	switch($ReportIDOrder)
	{
		case 0:
			$CASA = 1;
			$SemStr = "1st Combined";
			break;
		case 1:
			$CASA = 2;
			$SemStr = "2nd Combined";
			break;
		case 2:
			$CASA = "";
			$SemStr = "Whole Year";
			break;	
	}
	
	$ColumnData = $lreportcard->returnReportTemplateColumnData($ReportID);
	foreach($ColumnData as $k => $d)
	{
		$PColID[] = $d['ReportColumnID'];
	}	
}

$Title = "Class Summary Report - ". $SemStr ." ". $SubjectStr ." (". ($PSLevel=="P" ? "Primary" : "Secondary").")";

# define where condition
if($ReportColumnID && $PSLevel=="P")	$where[] = "ReportColumnID='$ReportColumnID'";
if($SubjectID)							$where[] = "SubjectID='$SubjectID'";
$cons = !empty($where) ? implode(" and ", $where) : "";
$cons = $cons ? " and ".$cons : "";

foreach($StudentAry as $key => $sid)
{
	$m = array();
	
	if($PSLevel=="P")		# Primary
	{
		$result = $lreportcard->getMarks($ReportID, $sid, $cons);
		foreach($result as $subjid => $data)
		{
			$SubjectFormGradingSettings = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $subjid);
			$SchemeID = $SubjectFormGradingSettings[SchemeID];
		
			$thisMark = my_round($data[$ReportColumnID]['Mark'],1);
			list($thisMark, $nsp) = $lreportcard->checkSpCase($ReportID, $subjid, $thisMark, $data[$ReportColumnID]['Grade']);
	  				
			$DataTemp[$sid][$subjid]['Mark'] = $thisMark;
			$DataTemp[$sid][$subjid]['Grade'] = $data[$ReportColumnID]['Grade'];
			
			if($nsp)
			{
				$m[$ReportColumnID][$subjid] = $data[$ReportColumnID]['Mark'];
				$SubjectTotal[$subjid]['Mark'] += $data[$ReportColumnID]['Mark'];
				$SubjectTotal[$subjid]['no']++;
			}
			else
			{
				if($DataTemp[$sid][$subjid]['Grade']!="/")
					$SubjectTotal[$subjid]['no']++;
				$m[$ReportColumnID][$subjid] = $DataTemp[$sid][$subjid]['Grade']=="/" ? "/" : 0;
			}
			$thisBand = $lreportcard->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($SchemeID, $data[$ReportColumnID]['Mark']);
			$DataTemp[$sid][$subjid]['Band'] = $thisBand;
			
		}
		
		# retrieve Grand Total / Avg
		if(!$ReportColumnID)
		{
			$t = $lreportcard->getReportResultScore($ReportID, $ReportColumnID, $sid);
			$SemTotal[$sid]['GrandTotal'] = my_round($t['GrandTotal'], 1);	
			$SemTotal[$sid]['GrandAverage'] = my_round($t['GrandAverage'], 1);	
		}
		else
		{
			$t = $lreportcard->CALCULATE_COLUMN_MARK($ReportID, $ClassLevelID, $sid, $m[$ReportColumnID]);
			$SemTotal[$sid]['GrandTotal'] = my_round($t['GrandTotal'], 1);	
			$SemTotal[$sid]['GrandAverage'] = my_round($t['GrandAverage'], 1);	
		}
	}
	else					# Secondary
	{
		if($CASA)
		{
			$result = $lreportcard->getMarks($ReportID, $sid, $cons);
			foreach($result as $subjid => $data)
			{
				if(in_array($subjid, $SubjectAry)===false)	continue;
				
				$SubjectFormGradingSettings = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $subjid);
	 			$SchemeID = $SubjectFormGradingSettings[SchemeID];
	
				foreach($data as $colid => $m_ary)
				{
					$thisMark = my_round($m_ary['Mark'],1);
	  				list($thisMark, $nsp) = $lreportcard->checkSpCase($ReportID, $subjid, $thisMark, $m_ary['Grade']);
	  				
					$DataTemp[$sid][$subjid][$colid]['Mark'] = $thisMark;
		 			$DataTemp[$sid][$subjid][$colid]['Grade'] = $m_ary['Grade'];
		 			if($nsp)
		 			{
	 		 			if($colid==0)	
	 		 			{	
		 		 			$SubjectTotal[$subjid]['Mark'] += $thisMark;
	 		 				$SubjectTotal[$subjid]['no']++;
 		 				}
		 				$thisBand = $lreportcard->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($SchemeID, $m_ary['Mark']);
		 				$DataTemp[$sid][$subjid][$colid]['Band'] = $thisBand;
	 				}
	 				else
	 				{
		 				if($colid==0)
		 				{
			 				if($m_ary['Grade']!="/")
								$SubjectTotal[$subjid]['no']++;		
		 				}
		 				$DataTemp[$sid][$subjid][$colid]['Band'] = "";
	 				}
				}
			}
			
			# retrieve Grand Total / Avg
			$t = $lreportcard->getReportResultScore($ReportID, $ReportColumnID, $sid);
			$SemTotal[$sid]['GrandTotal'] = my_round($t['GrandTotal'], 1);	
			$SemTotal[$sid]['GrandAverage'] = my_round($t['GrandAverage'], 1);	
		}
		else
		{
			foreach($ReportIDList as $k=>$thisReportID)
			{
				$cons = "and ReportColumnID=0";
				$result = $lreportcard->getMarks($thisReportID, $sid, $cons);
				
				foreach($result as $subjid => $data)
				{
					if(in_array($subjid, $SubjectAry)===false)	continue;
					
					$SubjectFormGradingSettings = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $subjid);
		 			$SchemeID = $SubjectFormGradingSettings[SchemeID];
		
		 			foreach($data as $k1 => $m_ary)
					{
						$thisMark = my_round($m_ary['Mark'],1);
		  				list($thisMark, $nsp) = $lreportcard->checkSpCase($thisReportID, $subjid, $thisMark, $m_ary['Grade']);
		  				
						$DataTemp[$sid][$subjid][$thisReportID]['Mark'] = $thisMark;
			 			$DataTemp[$sid][$subjid][$thisReportID]['Grade'] = $m_ary['Grade'];
			 			if($nsp)
			 			{
				 			if($k==2) 	# subject overall
				 			{	
					 			$SubjectTotal[$subjid]['Mark'] += $thisMark;
					 			$SubjectTotal[$subjid]['no']++;
					 			$SemTotal[$sid]['GrandTotal'] += $thisMark;	
				 			}
			 				$thisBand = $lreportcard->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($SchemeID, $m_ary['Mark']);
			 				$DataTemp[$sid][$subjid][$thisReportID]['Band'] = $thisBand;
		 				}
		 				else
		 				{
			 				if($k==2)
			 				{
				 				if($m_ary['Grade']!="/")
									$SubjectTotal[$subjid]['no']++;			
			 				}
			 				$DataTemp[$sid][$subjid][$thisReportID]['Band'] = "";
		 				}
					}
				}
			}
			
			$SemTotal[$sid]['GrandTotal'] = my_round($SemTotal[$sid]['GrandTotal'],1);
			# Grand Avg
			$SemTotal[$sid]['GrandAverage'] = my_round($SemTotal[$sid]['GrandTotal'] / sizeof($SubjectAry), 1);
		}
	}
}

foreach($StudentAry as $k=>$stu)
{
	$thisStudentID = $stu;
	
	$ListAry[$thisStudentID]['ClassNumber'] = $DataTemp[$thisStudentID]['ClassNumber'];
	$ListAry[$thisStudentID]['AdmissionNo'] = $DataTemp[$thisStudentID]['AdmissionNo'];
	$ListAry[$thisStudentID]['StudentName'] = $DataTemp[$thisStudentID]['StudentName'];
	
// 	$ListAry[$thisStudentID]['average_space'] = "&nbsp;";
	
	if($PSLevel=="P")
	{
		foreach($SubjectAry as $k => $thisSubjectID)
		{
			$thisMark = $DataTemp[$thisStudentID][$thisSubjectID]['Mark'];
			list($thisMark, $nsp) = $lreportcard->checkSpCase($ReportID, $thisSubjectID, $thisMark, $DataTemp[$thisStudentID][$thisSubjectID]['Grade']);
			
			$ListAry[$thisStudentID][$thisSubjectID."Mark"] = $thisMark;
			$ListAry[$thisStudentID][$thisSubjectID."Grade"] = $nsp ? $DataTemp[$thisStudentID][$thisSubjectID]['Band'] : "";
			
		}
	}
	else
	{
		foreach($SubjectAry as $k => $thisSubjectID)
		{
			if($CASA)
			{
				foreach($PColID as $k1=>$thisColID)
					$ListAry[$thisStudentID][$thisSubjectID.$thisColID."Mark"] = $DataTemp[$thisStudentID][$thisSubjectID][$thisColID]['Mark'];
				
				# Combnied		
	 			$ListAry[$thisStudentID][$thisSubjectID.$thisColID."Com"] = $DataTemp[$thisStudentID][$thisSubjectID][0]['Mark'];
	 			# Grade
				$ListAry[$thisStudentID][$thisSubjectID.$thisColID."Grade"] = $DataTemp[$thisStudentID][$thisSubjectID][0]['Band'];
			}
			else
			{
				foreach($ReportIDList as $k=>$thisReportID)
					$ListAry[$thisStudentID][$thisSubjectID.$thisReportID."Mark"] = $DataTemp[$thisStudentID][$thisSubjectID][$thisReportID]['Mark'];
				
				$ListAry[$thisStudentID][$thisSubjectID."Grade"] = $DataTemp[$thisStudentID][$thisSubjectID][$ReportIDList[2]]['Band'];;
			}
		}
	}
	$ListAry[$thisStudentID]['Total'] = $SemTotal[$thisStudentID]['GrandTotal'];
	$ListAry[$thisStudentID]['Percent'] = $SemTotal[$thisStudentID]['GrandAverage'];
}
# sort by Marks
foreach ($ListAry as $key => $row) 
{
	$field1[$key] = $row['ClassNumber'];
}
array_multisort($field1, SORT_ASC, $ListAry);

$display = "";
foreach($ListAry as $key=>$row)
{
	
	$display .= "<tr>";		
	$ki = 0;
	foreach($row as $k => $d)	
	{ 
		$colspan = $ki==2 ? "colspan='2'":"";
		$display .= "<td $colspan class=\"$css tabletext\" align=". ($ki>3 ? "center":"").">$d</td>";
		$ki++;
	}
	$display .= "</tr>";
}

# Footer
$display .= "<tr>";
$display .= "<td colspan=2><span class='GMS_text16bi'>Total Students:</span></td>";
$display .= "<td align='left'><span class='tabletext'>". sizeof($StudentAry) ."</span></td>";
$display .= "<td align='right'><span class='GMS_text16bi'>Average:</span></td>";
$total = 0;
foreach($SubjectAry as $k => $thisSubjectID)
{
	if($PSLevel=="S")	
	{
		$display .= "<td>&nbsp;</td>";
		$display .= "<td>&nbsp;</td>";	
	}

	$display .= "<td class='tabletext' align='center'>". my_round(($SubjectTotal[$thisSubjectID]['Mark'] / $SubjectTotal[$thisSubjectID]['no']), 1) ."</td>";
	$display .= "<td>&nbsp;</td>";
	$total += my_round($SubjectTotal[$thisSubjectID]['Mark'] / $SubjectTotal[$thisSubjectID]['no'], 1);
}

$display .= "<td class='tabletext' align='center'>". my_round($total, 1) ."</td>";
$display .= "<td class='tabletext' align='center'>". my_round(($total / sizeof($SubjectAry)), 1) ."</td>";
$display .= "</tr>";

# Table header
$th = array();
$th[]= "Class no.";
$th[]= "Student no.";
$th[]= "Student Name";
$th[]= "&nbsp;";
foreach($SubjectAry as $k=>$subjid)
	$th[]= array($lreportcard->GET_SUBJECT_NAME_LANG($subjid), "$PSLevel");
$th[]= ($PSLevel =="P" ? $SemStr." " : "") ."Total";
$th[]= ($PSLevel =="P" ? "Total " : "") . "%";

$tableWidth = sizeof($th) + sizeof($SubjectAry)*($PSLevel=="P"?1:3);


# Class Teacher
$ClassTeacherAry = $lclass->returnClassTeacher($lclass->getClassName($ClassID));
foreach($ClassTeacherAry as $key=>$val)
{
	$CTeacher[] = $val['CTeacher'];
}
$ClassTeacher = !empty($CTeacher) ? implode(", ", $CTeacher) : "--";

$x .= "<tr><td colspan=\"". $tableWidth ."\" class=\"GMS_header\">". $Title ."</td></tr>";
$x .= "<tr><td colspan=\"". $tableWidth ."\" class=\"GMS_header2\">School Year: ". getCurrentAcademicYear() ." &nbsp; &nbsp; Level: ". $lclass->getLevelName($ClassLevelID) ."</td></tr>";
         
$x .= "<tr><td colspan=\"". $tableWidth ."\"><span class=\"GMS_text16bi\">Class:</span> <span class=\"tabletext\">". $lclass->getClassName($ClassID) ."</span> &nbsp; &nbsp; <span class=\"GMS_text16bi\">Class Teacher:</span> <span class=\"tabletext\">". $ClassTeacher."</span></td></tr>";
$x .= "<tr>";
$ki = 0;
foreach($th as $k=>$d)
{
	$pars = $ki==2 ? "width=\"100%\" align=\"left\"" : "align=\"center\"";
	
	if(!is_array($d))
		$x .= "<td class=\"GMS_text16bi tableline\" rowspan=\"2\" valign=\"bottom\" $pars>".$d."</td>";
	else
		$x .= "<td class=\"GMS_text16bi\" colspan=\"". ($d[1]=="P"?2:4) ."\" $pars>".$d[0]."</td>";
	$ki++;
}
$x .= "</tr>";
$x .= "<tr>";
foreach($th as $k=>$d)
{
	if(is_array($d))
	{ 
		if($PSLevel=="P")
		{
			$x .= "<td class=\"GMS_text12i tableline\" align=\"center\">Marks</td>";
			$x .= "<td class=\"GMS_text12i tableline\" align=\"center\">Grade</td>";
		}
		else
		{
			if($CASA)
			{
				$x .= "<td class=\"GMS_text12i tableline\" align=\"center\">CA".$CASA."</td>";
				$x .= "<td class=\"GMS_text12i tableline\" align=\"center\">SA".$CASA."</td>";
				$x .= "<td class=\"GMS_text12i tableline\" align=\"center\">Com.</td>";
			}
			else
			{
				$x .= "<td class=\"GMS_text12i tableline\" align=\"center\">1st Com.</td>";
				$x .= "<td class=\"GMS_text12i tableline\" align=\"center\">2nd Com.</td>";
				$x .= "<td class=\"GMS_text12i tableline\" align=\"center\">Overall</td>";
			}
			$x .= "<td class=\"GMS_text12i tableline\" align=\"center\">Grade</td>";
		}
	}

}
$x .= "</tr>";

$eRtemplateCSS = $lreportcard->getTemplateCSS();
?>
<? if($eRtemplateCSS) {?><link href="<?=$eRtemplateCSS?>" rel="stylesheet" type="text/css" /><? } ?>

<table border="0" cellpadding="4" width="100%" cellspacing="0" class="print_hide">
<tr><td align="right"><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></span></td></tr>
</table>			
										
<!-- Start Main Table //-->
<table width="100%" border="0" cellspacing="0" cellpadding="5">
  <tr> 
    <td align="center" colspan="2"><table width="100%" border="0" cellspacing="0" cellpadding="2">
      <tr>
        <td align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td>
            	<table width="100%" border="0" cellspacing="0" cellpadding="4">
				<?=$x?>
                <?=$display?>
                </table>
            </td>
          </tr>
          <tr><td height="1" class="tabletext tableline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
          <tr><td class="GMS_text14bi">&nbsp;<?=date("l, F d, Y");?></td></tr>
        </table>
          </td>
      </tr>
    </table>
    </td>
  </tr>
</table>

<!-- End Main Table //-->

<table border="0" cellpadding="4" width="100%" cellspacing="0" class="print_hide">
<tr><td align="right"><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></span></td></tr>
</table>
                        
<?
intranet_closedb();
?>

