<?php
// Root path
$PATH_WRT_ROOT = "../../../../../../";

// Page access right
$PageRight = "TEACHER";

// Include general libraries
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

// Authorize intranet user & connect Database
intranet_auth();
intranet_opendb();

// Check if ReportCard module is enable
if ($plugin['ReportCard2008']) {
	// Include ReportCard libraries
	include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
	if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
	{
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
		$lreportcard = new libreportcardcustom();
	} else {
		$lreportcard = new libreportcard();
	}
	
	#$linterface = new interface_html();
	$lclass = new libclass();
	
	// Check module access right
	if ($lreportcard->hasAccessRight()) {
		// Check required data submitted from index.php
		// optional: $SubjectID, $ReportColumnID
		if (!isset($ClassLevelID, $ClassID, $ReportID) || empty($ClassLevelID) || empty($ReportID) || empty($ClassID)) {
			intranet_closedb();
			header("Location: index.php");
			exit();
		}
		
		$exportColumn = array();
		$ExportArr = array();
		$lexport = new libexporttext();
		
		# get display options
		// 1. student name display (EnglishName, ChineseName)
		$showEnglishName = false;
		$showChineseName = false;
		if ($StudentNameDisplay == NULL)
		{
			$numOfNameColumn = 0;
		}
		else
		{
			$StudentNameDisplay = explode(",", $StudentNameDisplay);
			$numOfNameColumn = sizeof($StudentNameDisplay);
			foreach ($StudentNameDisplay as $key => $value)
			{
				if ($value == "EnglishName") $showEnglishName = true;
				if ($value == "ChineseName") $showChineseName = true;
			}
		}		
		// 2. ranking display (ClassRanking, OverallRanking)
		$showClassRanking = false;
		$showOverallRanking = false;
		$RankingDisplay = explode(",", $RankingDisplay);
		foreach ($RankingDisplay as $key => $value)
		{
			if ($value == "ClassRanking") $showClassRanking = true;
			if ($value == "OverallRanking") $showOverallRanking = true;
		}		
		// 3. show subject component (0,1)
		$showSubjectComponent = $ShowSubjectComponent;
		// 4. show Summary Info (0,1)
		$showSummaryInfo = $ShowSummaryInfo;		
		// 5. show Grand Total (0,1)
		$showGrandTotal = $ShowGrandTotal;		
		// 6. show Grand Average (0,1)
		$showGrandAverage = $ShowGrandAverage;
		// 7. ForSSPA => no decimal place in all marks
		$ForSSPA = $ForSSPA;
		// 8. show Statistics (0,1)
		$showStatistics = $ShowStatistics;
		
		$reportCalculationOrder = $lreportcard->returnReportTemplateCalculation($ReportID);
		
		// Get ReportCard info
		$reportTemplateInfo = $lreportcard->returnReportTemplateBasicInfo($ReportID);
		
		// Check to make sure it is a generated report
		if ($reportTemplateInfo["LastGenerated"] == "0000-00-00 00:00:00" || $reportTemplateInfo["LastGenerated"] == "") {
			intranet_closedb();
			header("Location: index.php");
			exit();
		}
		
		// Get subjects of the current ClassLevel
		$FormSubjectArr = $lreportcard->returnSubjectwOrder($ClassLevelID, 1, '', $intranet_session_language);
		
		$CmpSubjectIDList = array();
		$ParentSubjectIDList = array();
		$ParentCmpSubjectNum = array();
		
		// Reformat the subject array - Display component subjects also
		$FormSubjectArrFlat = array();
		if (sizeof($FormSubjectArr) > 0) {
			foreach($FormSubjectArr as $tmpSubjectID => $tmpSubjectName) {
				if (sizeof($tmpSubjectName) > 1) {
					$ParentSubjectIDList[] = $tmpSubjectID;
					$ParentCmpSubjectNum[$tmpSubjectID] = sizeof($tmpSubjectName) - 1;
					foreach ($tmpSubjectName as $tmpCmpSubjectID => $tmpCmpSubjectName) {
						if ($tmpCmpSubjectID != 0) {
							$CmpSubjectIDList[] = $tmpCmpSubjectID;
							$FormSubjectArrFlat[$tmpCmpSubjectID] = array(0 => $tmpCmpSubjectName);
						}
					}
				}
				$FormSubjectArrFlat[$tmpSubjectID] = array(0 => $tmpSubjectName[0]);
			}
		}
		$FormSubjectArr = $FormSubjectArrFlat;
		
		// Get component subjects if SubjectID is provided
		if (isset($SubjectID) && $SubjectID !== "" && in_array($SubjectID, $ParentSubjectIDList)) {
			$CmpSubjectArr = $lreportcard->GET_COMPONENT_SUBJECT($SubjectID, $ClassLevelID);
			$CmpSubjectIDNameArr = array();
			$CmpSubjectIDArr = array();
			for($i=0; $i<sizeof($CmpSubjectArr); $i++) {
				$CmpSubjectIDArr[] = $CmpSubjectArr[$i]["SubjectID"];
				$CmpSubjectIDNameArr[$CmpSubjectArr[$i]["SubjectID"]] = $CmpSubjectArr[$i]["SubjectName"];
			}
		}
		
		// Get subjects' grading scheme of the current ClassLevel
		$FormSubjectGradingSchemeArr = $lreportcard->GET_FROM_SUBJECT_GRADING_SCHEME($ClassLevelID);
		
		// Get ClassLevel name
		$ClassLevelName = $lreportcard->returnClassLevel($ClassLevelID);
		
		// Get Classes
		$ClassArr = $lreportcard->GET_CLASSES_BY_FORM($ClassLevelID);
		
		// Get current school year
		$SchoolYear = $lreportcard->schoolYear;
		
		// Get columns info of the current ReportCard
		$ColumnTitleAry = $lreportcard->returnReportColoumnTitle($ReportID);
		$ColumnIDList = array();
		$ColumnIDList = ($ColumnTitleAry == NULL)? NULL : array_keys($ColumnTitleAry);
		
		$studentIDList = array();
		$studentIDNameMap = array();
		$studentIDRegNoMap = array();
		
		// Get the list of students (in entire class level)
		$studentList = array();
		$studentIDList = array();
		$studentIDNameMap = array();
		$studentIDClassMap = array();
		$studentIDClassNumMap = array();
		$studentIDRegNoMap = array();
		$ClassNameArr = array();
		for($i=0; $i<sizeof($ClassArr); $i++) {
			$ClassNameArr[] = $ClassArr[$i]["ClassName"];
			$studentList = $lreportcard->GET_STUDENT_BY_CLASS($ClassArr[$i]["ClassID"], "", 1);
			for($j=0; $j<sizeof($studentList); $j++) {
				$studentIDNameMap[$studentList[$j]["UserID"]] = $studentList[$j][3];
				$studentIDClassNumMap[$studentList[$j]["UserID"]] = $studentList[$j][2];
				$studentIDClassMap[$studentList[$j]["UserID"]] = $ClassArr[$i]["ClassName"];
				$studentIDRegNoMap[$studentList[$j]["UserID"]] = $studentList[$j]["WebSAMSRegNo"];
				$studentIDList[] = $studentList[$j]["UserID"];
			}
		}
		
		// Students should be already sorted by Class Number in the SQL Query
		$studentIDListSql = implode(",", $studentIDList);
		$cond = "";
		
		// Get overall mark or term/assessment mark
		$ColumnTitle = "";
		if (!isset($ReportColumnID) || empty($ReportColumnID)) {
			$ColumnTitle = $eReportCard['Template']['OverallCombined'];
			$cond .= "AND (ReportColumnID = '' OR ReportColumnID = '0') ";
		} else {
			$ColumnTitle = $ColumnTitleAry[$ReportColumnID];
			$cond .= "AND ReportColumnID = '$ReportColumnID' ";
		}
		
		// Get individual subject mark or grand average (which table to retrieve)
		$SubjectName = "";
		if (!isset($SubjectID) || empty($SubjectID)) {
			// No SubjectID provided, get the averaage result
			$table = $lreportcard->DBName.".RC_REPORT_RESULT";
			$fieldToSelect = "StudentID, GrandTotal, GrandAverage, OrderMeritClass, OrderMeritForm";
			
			$sql = "SELECT $fieldToSelect FROM $table WHERE ";
			$sql .= "ReportID = '$ReportID' ";
			$sql .= "AND StudentID IN ($studentIDListSql) ";
			$sql .= $cond;
			$sql .= "ORDER BY OrderMeritForm";
			$tempArr = $lreportcard->returnArray($sql);
			
			$resultAverage = array();
			$studentIDListSorted = $studentIDList;
			if (sizeof($tempArr) > 0) {
				$studentIDListSorted = array();
				for($j=0; $j<sizeof($tempArr); $j++) {
					$studentIDListSorted[] = $tempArr[$j]["StudentID"];
					$resultAverage[$tempArr[$j]["StudentID"]]["GrandTotal"] = $tempArr[$j]["GrandTotal"];
					$resultAverage[$tempArr[$j]["StudentID"]]["GrandAverage"] = $tempArr[$j]["GrandAverage"];
					
					# Round up all the marks if for SSPA
					if ($ForSSPA)
					{
						$resultAverage[$tempArr[$j]["StudentID"]]["GrandTotal"] = round($resultAverage[$tempArr[$j]["StudentID"]]["GrandTotal"]);
						$resultAverage[$tempArr[$j]["StudentID"]]["GrandAverage"] = round($resultAverage[$tempArr[$j]["StudentID"]]["GrandAverage"]);
					}
					
					if ($tempArr[$j]["OrderMeritClass"] > 0) {
						$resultAverage[$tempArr[$j]["StudentID"]]["OrderMeritClass"] = $tempArr[$j]["OrderMeritClass"];
					} else {
						$resultAverage[$tempArr[$j]["StudentID"]]["OrderMeritClass"] = "-";
					}
					
					if ($tempArr[$j]["OrderMeritForm"] > 0) {
						$resultAverage[$tempArr[$j]["StudentID"]]["OrderMeritForm"] = $tempArr[$j]["OrderMeritForm"];
					} else {
						$resultAverage[$tempArr[$j]["StudentID"]]["OrderMeritForm"] = "-";
					}
				}
			}
			
			// Get all subject marks as additional information
			foreach($FormSubjectArr as $tmpSubjectID => $tmpSubjectName) {
				$subjectGradingScheme = $FormSubjectGradingSchemeArr[$tmpSubjectID];
				$subjectGradingSchemeInfo = $lreportcard->GET_GRADING_SCHEME_MAIN_INFO($subjectGradingScheme["schemeID"]);
				
				if ($subjectGradingScheme["scaleDisplay"] == "M") {
					$fieldToSelect = "StudentID, Mark As MarkUsed";
				} else {
					$fieldToSelect = "StudentID, Grade As MarkUsed";
				}
				$table = $lreportcard->DBName.".RC_REPORT_RESULT_SCORE";
				
				$sql = "SELECT $fieldToSelect FROM $table WHERE ";
				$sql .= "ReportID = '$ReportID' ";
				$sql .= "AND StudentID IN ($studentIDListSql) ";
				$sql .= "AND  SubjectID = '$tmpSubjectID' ";
				$sql .= $cond;
				$tempArr = $lreportcard->returnArray($sql);
				
				$resultSubject[$tmpSubjectID] = array();
				if (sizeof($tempArr) > 0) {
					for($j=0; $j<sizeof($tempArr); $j++) {
						$resultSubject[$tmpSubjectID][$tempArr[$j]["StudentID"]] = $tempArr[$j]["MarkUsed"];
						
						# Round up all the marks if for SSPA
						if ($ForSSPA && $subjectGradingScheme["scaleDisplay"] == "M")
						{
							$resultSubject[$tmpSubjectID][$tempArr[$j]["StudentID"]] = round($resultSubject[$tmpSubjectID][$tempArr[$j]["StudentID"]]);
						}
					}
				}
			}
		} else {
			$subjectGradingScheme = $FormSubjectGradingSchemeArr[$SubjectID];
			$subjectGradingSchemeInfo = $lreportcard->GET_GRADING_SCHEME_MAIN_INFO($subjectGradingScheme["schemeID"]);
			
			if ($subjectGradingScheme["scaleDisplay"] == "M") {
				$fieldToSelect = "StudentID, OrderMeritClass, OrderMeritForm, Mark As MarkUsed";
			} else {
				$fieldToSelect = "StudentID, OrderMeritClass, OrderMeritForm, Grade As MarkUsed";
			}
			$table = $lreportcard->DBName.".RC_REPORT_RESULT_SCORE";
			
			$sql = "SELECT $fieldToSelect FROM $table WHERE ";
			$sql .= "ReportID = '$ReportID' ";
			$sql .= "AND StudentID IN ($studentIDListSql) ";
			$sql .= "AND SubjectID = '$SubjectID' ";
			
			$tempArr = $lreportcard->returnArray($sql.$cond."ORDER BY OrderMeritForm");
			
			$resultSubject = array();
			$studentIDListSorted = array();
			$OrderFormSubject = array();
			if (sizeof($tempArr) > 0) {
				for($j=0; $j<sizeof($tempArr); $j++) {
					$studentIDListSorted[] = $tempArr[$j]["StudentID"];
					$resultSubject[$tempArr[$j]["StudentID"]] = $tempArr[$j]["MarkUsed"];
					
					# Round up all the marks if for SSPA
					if ($ForSSPA && $subjectGradingScheme["scaleDisplay"] == "M")
					{
						$resultSubject[$tempArr[$j]["StudentID"]] = round($resultSubject[$tempArr[$j]["StudentID"]]);
					}
					
					if ($tempArr[$j]["OrderMeritClass"] > 0) {
						$OrderClassSubject[$tempArr[$j]["StudentID"]] = $tempArr[$j]["OrderMeritClass"];
					} else {
						$OrderClassSubject[$tempArr[$j]["StudentID"]] = "-";
					}
					if ($tempArr[$j]["OrderMeritForm"] > 0) {
						$OrderFormSubject[$tempArr[$j]["StudentID"]] = $tempArr[$j]["OrderMeritForm"];
					} else {
						$OrderFormSubject[$tempArr[$j]["StudentID"]] = "-";
					}
				}
			}
			
			// If gettng subject overall mark for a single subject, get the column mark also
			if (!isset($ReportColumnID) || empty($ReportColumnID)) {
				for($i=0; $i<sizeof($ColumnIDList); $i++) {
					$tmpCond = "AND ReportColumnID = '".$ColumnIDList[$i]."'";
					$tempArr = $lreportcard->returnArray($sql.$tmpCond);
					
					$resultSubjectColumn[$ColumnIDList[$i]] = array();
					if (sizeof($tempArr) > 0) {
						for($j=0; $j<sizeof($tempArr); $j++) {
							$resultSubjectColumn[$ColumnIDList[$i]][$tempArr[$j]["StudentID"]] = $tempArr[$j]["MarkUsed"];
							
							# Round up all the marks if for SSPA
							if ($ForSSPA && $subjectGradingScheme["scaleDisplay"] == "M")
							{
								$resultSubjectColumn[$ColumnIDList[$i]][$tempArr[$j]["StudentID"]] = round($resultSubjectColumn[$ColumnIDList[$i]][$tempArr[$j]["StudentID"]]);
							}
						}
					}
				}
			}
			
			#################################
			// Special handling of subject which having component subjects
			if (in_array($SubjectID, $ParentSubjectIDList)) {
				// Push the SubjectID into the same array of its component subjects
				$SubjectPlusCompSubjectIDList = $CmpSubjectIDArr;
				$SubjectPlusCompSubjectIDList[] = $SubjectID;
				
				for($i=0; $i<sizeof($SubjectPlusCompSubjectIDList); $i++) {
					$tmpSubjectID = $SubjectPlusCompSubjectIDList[$i];
					$subjectGradingScheme = $FormSubjectGradingSchemeArr[$tmpSubjectID];
					$subjectGradingSchemeInfo = $lreportcard->GET_GRADING_SCHEME_MAIN_INFO($subjectGradingScheme["schemeID"]);
					
					if ($subjectGradingScheme["scaleDisplay"] == "M") {
						$fieldToSelect = "StudentID, Mark As MarkUsed";
					} else {
						$fieldToSelect = "StudentID, Grade As MarkUsed";
					}
					$table = $lreportcard->DBName.".RC_REPORT_RESULT_SCORE";
					
					$sql = "SELECT $fieldToSelect FROM $table WHERE ";
					$sql .= "ReportID = '$ReportID' ";
					$sql .= "AND StudentID IN ($studentIDListSql) ";
					$sql .= "AND  SubjectID = '$tmpSubjectID' ";
					$sql .= $cond;
					$tempArr = $lreportcard->returnArray($sql);
					
					$resultDetailSubject[$tmpSubjectID] = array();
					if (sizeof($tempArr) > 0) {
						for($j=0; $j<sizeof($tempArr); $j++) {
							$resultDetailSubject[$tmpSubjectID][$tempArr[$j]["StudentID"]] = $tempArr[$j]["MarkUsed"];
							
							# Round up all the marks if for SSPA
							if ($ForSSPA && $subjectGradingScheme["scaleDisplay"] == "M")
							{
								$resultDetailSubject[$tmpSubjectID][$tempArr[$j]["StudentID"]] = round($resultDetailSubject[$tmpSubjectID][$tempArr[$j]["StudentID"]]);
							}
						}
					}
				}
			}
			#################################
			# get Grand Total and Grand Average
			$table = $lreportcard->DBName.".RC_REPORT_RESULT";
			$fieldToSelect = "StudentID, GrandTotal, GrandAverage, OrderMeritClass, OrderMeritForm";
			
			$sql = "SELECT $fieldToSelect FROM $table WHERE ";
			$sql .= "ReportID = '$ReportID' ";
			$sql .= "AND StudentID IN ($studentIDListSql) ";
			$sql .= $cond;
			$tempArr = $lreportcard->returnArray($sql);
			
			$resultAverage = array();
			if (sizeof($tempArr) > 0) {
				for($j=0; $j<sizeof($tempArr); $j++) {
					$resultAverage[$tempArr[$j]["StudentID"]]["GrandTotal"] = $tempArr[$j]["GrandTotal"];
					$resultAverage[$tempArr[$j]["StudentID"]]["GrandAverage"] = sprintf("%02.2f", $tempArr[$j]["GrandAverage"]);
					
					# Round up all the marks if for SSPA
					if ($ForSSPA && $subjectGradingScheme["scaleDisplay"] == "M")
					{
						$resultAverage[$tempArr[$j]["StudentID"]]["GrandTotal"] = round($resultAverage[$tempArr[$j]["StudentID"]]["GrandTotal"]);
						$resultAverage[$tempArr[$j]["StudentID"]]["GrandAverage"] = round($resultAverage[$tempArr[$j]["StudentID"]]["GrandAverage"]);
					}
							
					if ($tempArr[$j]["OrderMeritClass"] > 0) {
						$resultAverage[$tempArr[$j]["StudentID"]]["OrderMeritClass"] = $tempArr[$j]["OrderMeritClass"];
					} else {
						$resultAverage[$tempArr[$j]["StudentID"]]["OrderMeritClass"] = "-";
					}
					if ($tempArr[$j]["OrderMeritForm"] > 0) {
						$resultAverage[$tempArr[$j]["StudentID"]]["OrderMeritForm"] = $tempArr[$j]["OrderMeritForm"];
					} else {
						$resultAverage[$tempArr[$j]["StudentID"]]["OrderMeritForm"] = "-";
					}
				}
			}
		}
		
		# get student class and class number of last year for munsang customization
		if ($studentIDList != NULL)
		{
			$studentIDListSql = implode(",", $studentIDList);
		}
		$activeYear = $lreportcard->GET_ACTIVE_YEAR("-");
		$lastYearArr = explode("-", $activeYear);
		$lastYearArr[0] -= 1; 
		$lastYearArr[1] -= 1; 
		$lastYear = implode("-", $lastYearArr);
		$sql = "SELECT UserID, ClassName, ClassNumber FROM PROFILE_CLASS_HISTORY WHERE UserID IN ($studentIDListSql) AND AcademicYear = '$lastYear' ";
		$result = $lreportcard->returnArray($sql, 3);
		$studentClassHistoryArr = array();
		for ($i=0; $i<sizeof($result); $i++)
		{
			list($tempUserID, $tempClassName, $tempClassNumber) = $result[$i];
			$studentClassHistoryArr[$tempUserID]['ClassName'] = $tempClassName;
			$studentClassHistoryArr[$tempUserID]['ClassNumber'] = $tempClassNumber;
		}
		
		// Get Summary Info from CSV
		# build data array
		$SummaryInfoFields = array("Conduct");	
		$ary = array();
		$csvType = $lreportcard->getOtherInfoType();
		if($reportTemplateInfo['Semester']=="F") {
			$InfoTermID = 0;
		} else {
			$InfoTermID = $reportTemplateInfo['Semester']+1;
		}
		if(!empty($csvType)) {
			foreach($csvType as $k=>$Type) {
				for($i=0; $i<sizeof($ClassNameArr); $i++) {
					$csvData = $lreportcard->getOtherInfoData($Type, $InfoTermID, $ClassNameArr[$i]);	
					if(!empty($csvData)) {
						foreach($csvData as $RegNo=>$data) {
							foreach($data as $key=>$val) {
								if (!is_numeric($key) && trim($key) !== "")
									$ary[$ClassNameArr[$i]][$RegNo][$key] = $val;
							}
						}
					}
				}
			}
		}
		
		
		
		# initialize variables to calculate Average, S.D, Variance, Max, Min, Passing Rate
		// $columnSubjectIDMapArr[#column] = SubjectID;  
		$columnSubjectIDMapArr = array();	// For getting the pasing mark from the grading scheme
		// $columnScaleOutputMapArr[#column] = "M" or "G"
		$columnScaleOutputMapArr = array();
		
		// Table header
		$exportColumn[] = "WebSAMSRegNumber";
		// show last year class and class number for SSPA
		if ($ForSSPA)
		{
			// if year = 2005-2007, display "06 Class" and "05 Class" as the column titles
			$activeYearDisplay = substr($activeYear,2,2);
			$lastYearDisplay = substr($lastYear,2,2);
			
			$exportColumn[] = $activeYearDisplay." ".$eReportCard['Template']['StudentInfo']['ClassName'];
			$exportColumn[] = $lastYearDisplay." ".$eReportCard['Template']['StudentInfo']['ClassName'];
		}
		else
		{
			$exportColumn[] = $eReportCard['Template']['StudentInfo']['ClassName'];
			$exportColumn[] = $eReportCard['Template']['StudentInfo']['ClassNo'];
		}
		
		if ($showEnglishName)
		{
			$exportColumn[] = $i_UserEnglishName;
		}
		if ($showChineseName)
		{
			$exportColumn[] = $i_UserChineseName;
		}
		
		if (!isset($SubjectID) || empty($SubjectID)) {
			
			# not specific subject	
			foreach($FormSubjectArr as $tmpSubjectID => $tmpSubjectName) {
				
				if ($showSubjectComponent)
				{
					if (in_array($tmpSubjectID, $ParentSubjectIDList)) 
					{
						$thisParentSubjectName = $tmpSubjectName[0];
						
						# add parent subject name to the component subjects' title
						# e.g. "Oral" -> "English Oral"
						foreach($tmpCmpSubjectTitleArr as $count => $id_title)
						{
							$thisSubjectDataArr = explode("::", $id_title);
							$thisID = $thisSubjectDataArr[0];
							$thisSubjectName = $thisSubjectDataArr[1];
							$exportColumn[] = $thisParentSubjectName." ".$thisSubjectName;
							
							$columnSubjectIDMapArr[] = $thisID;
							$thisSchemeInfo = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $thisID);
							$columnScaleOutputMapArr[] = $thisSchemeInfo['ScaleDisplay'];
						}
						
						# initialize $tmpCmpSubjectTitleArr for next component subjects group
						$tmpCmpSubjectTitleArr = array();
						
						# Total Title
						$exportColumn[] = $thisParentSubjectName." ".$eReportCard['Template']['Total'];
						
						$columnSubjectIDMapArr[] = $tmpSubjectID;
						$thisSchemeInfo = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $tmpSubjectID);
						$columnScaleOutputMapArr[] = $thisSchemeInfo['ScaleDisplay'];
					} 
					else if (in_array($tmpSubjectID, $CmpSubjectIDList)) 
					{
						$tmpCmpSubjectTitleArr[] = $tmpSubjectID."::".$tmpSubjectName[0];
					} 
					else 
					{
						$exportColumn[] = $tmpSubjectName[0];
						$columnSubjectIDMapArr[] = $tmpSubjectID;
						$thisSchemeInfo = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $tmpSubjectID);
						$columnScaleOutputMapArr[] = $thisSchemeInfo['ScaleDisplay'];
					}
				}
				else
				{
					# Hide component subjects
					if (in_array($tmpSubjectID, $ParentSubjectIDList)) {
						$exportColumn[] = $tmpSubjectName[0];
						
						$columnSubjectIDMapArr[] = $tmpSubjectID;
						$thisSchemeInfo = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $tmpSubjectID);
						$columnScaleOutputMapArr[] = $thisSchemeInfo['ScaleDisplay'];
					}
					else if (!in_array($tmpSubjectID, $CmpSubjectIDList)) 
					{
						$exportColumn[] = $tmpSubjectName[0];
						
						$columnSubjectIDMapArr[] = $tmpSubjectID;
						$thisSchemeInfo = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $tmpSubjectID);
						$columnScaleOutputMapArr[] = $thisSchemeInfo['ScaleDisplay'];
					}
				}
			}
						
		} else {
			# specific subject		
			$SubjectName = $FormSubjectArr[$SubjectID][0];
			
			if (!isset($ReportColumnID) || empty($ReportColumnID)) {
				# overall terms
				##############################
				// Special handling of subject which having component subjects
				if ($showSubjectComponent)
				{
					if (in_array($SubjectID, $ParentSubjectIDList)) 
					{
						$countCmpSubject = 1;
						foreach($CmpSubjectIDNameArr as $tmpSubjectID => $tmpSubjectName) {
							$exportColumn[] = "[".$countCmpSubject."] ".$tmpSubjectName;
							$countCmpSubject++;
						}
						#$markTable .= "<th>".$eReportCard['Template']['Total']."</th>";
					}
				}
				
				##############################
				$exportColumn[] = $eReportCard['Total'];
				
			} else {
				# specific terms
				##############################
				// Special handling of subject which having component subjects
				if ($showSubjectComponent && in_array($SubjectID, $ParentSubjectIDList)) {
					$countCmpSubject = 1;
					$ParentMarkFormula = array();
					foreach($CmpSubjectIDNameArr as $tmpSubjectID => $tmpSubjectName) {
						$OtherCondition = "SubjectID = ".$tmpSubjectID." AND ReportColumnID = $ReportColumnID";
						$CmpSubjectWeight = $lreportcard->returnReportTemplateSubjectWeightData($ReportID, $OtherCondition); 
						$CmpSubjectWeight = ($CmpSubjectWeight[0]['Weight']*100)."%";
						$ParentMarkFormula[] = "[".$countCmpSubject."](".$CmpSubjectWeight.")";
						
						$exportColumn[] = "[".$countCmpSubject."] ".$tmpSubjectName;
						$countCmpSubject++;
						
						$columnSubjectIDMapArr[] = $tmpSubjectID;
						$thisSchemeInfo = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $tmpSubjectID);
						$columnScaleOutputMapArr[] = $thisSchemeInfo['ScaleDisplay'];
					}
					$ParentMarkFormula = implode(" + ", $ParentMarkFormula);
					$exportColumn[] = $eReportCard['Total']." ".$ParentMarkFormula;
					
					$columnSubjectIDMapArr[] = $SubjectID;
					$thisSchemeInfo = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID);
					$columnScaleOutputMapArr[] = $thisSchemeInfo['ScaleDisplay'];
					
				} else {
					$exportColumn[] = $eReportCard['Total'];
					
					$columnSubjectIDMapArr[] = $SubjectID;
					$thisSchemeInfo = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID);
					$columnScaleOutputMapArr[] = $thisSchemeInfo['ScaleDisplay'];
				}
			}
		}
		
		# Summary Info
		if ($showSummaryInfo)
		{
			for($i=0; $i<sizeof($SummaryInfoFields); $i++) {
				$exportColumn[] = $eReportCard['Template'][$SummaryInfoFields[$i]];
				$columnScaleOutputMapArr[] = "G";
			}
		}
		
		if ($showGrandTotal)
		{
			$exportColumn[] = $eReportCard['Template']['GrandTotal'];
			$columnScaleOutputMapArr[] = "M";
		}
		if ($showGrandAverage)
		{
			$exportColumn[] = $eReportCard['Template']['GrandAverage'];
			$columnScaleOutputMapArr[] = "M";
		}
		if ($showClassRanking)
		{
			$exportColumn[] = $eReportCard['Template']['ClassPosition'];
		}
		if ($showOverallRanking)
		{
			$exportColumn[] = $eReportCard['Template']['FormPosition'];
		}
		
		
		# initialize variables to calculate Average, S.D, Variance, Max, Min, Passing Rate
		// $markDisplayedArr[#column][#row] = Mark;
		$markDisplayedArr = array();
		// $markNatureArr[#column][#row] = "Distinction" / "Pass" / "Fail";
		$markNatureArr = array();
		$ColumnCounter = 0;
			
		$iCounter = 0;
		$jCounter = 0;
		// Mark
		foreach($studentIDListSorted as $classNumber => $studentID) {
			$ColumnCounter = 0;
			$jCounter = 0;
			
			# WebSAMSNo
			$thisWebSAMSRegNo = $studentIDRegNoMap[$studentID];
			$ExportArr[$iCounter][$jCounter] = $thisWebSAMSRegNo;
			$jCounter++;
			
			# show last year class and class number if for SSPA
			if ($ForSSPA)
			{
				$ExportArr[$iCounter][$jCounter] = $studentIDClassMap[$studentID]." ".$studentIDClassNumMap[$studentID];
				$jCounter++;
				$ExportArr[$iCounter][$jCounter] = $studentClassHistoryArr[$studentID]['ClassName']." ".$studentClassHistoryArr[$studentID]['ClassNumber'];
				$jCounter++;
			}
			else
			{
				$ExportArr[$iCounter][$jCounter] = $studentIDClassMap[$studentID];
				$jCounter++;
				$ExportArr[$iCounter][$jCounter] = $studentIDClassNumMap[$studentID];
				$jCounter++;
			}
			
			// Separate the student name in "English (Chinese)" format
			$studentNameBi = $studentIDNameMap[$studentID];
			$studentNamePiece = explode("(",$studentNameBi);
			$studentNameEn = trim($studentNamePiece[0]);
			$studentNameCh = trim(str_replace(")", "", $studentNamePiece[1]));
			
			if ($showEnglishName)
			{
				$ExportArr[$iCounter][$jCounter] = $studentNameEn;
				$jCounter++;
			}
			if ($showChineseName)
			{
				$ExportArr[$iCounter][$jCounter] = $studentNameCh;
				$jCounter++;
			}
			
			if (!isset($SubjectID) || empty($SubjectID)) {
				# overall subjects
				foreach($FormSubjectArr as $tmpSubjectID => $tmpSubjectName) {
					if ($showSubjectComponent)
					{
						$thisMarks = $resultSubject[$tmpSubjectID][$studentID];
						$ExportArr[$iCounter][$jCounter] = $thisMarks;
						$jCounter++;
						
						$markDisplayedArr[$ColumnCounter][] = $thisMarks;
						$thisNature = $lreportcard->returnMarkNature($ClassLevelID, $tmpSubjectID, $thisMarks);
						$markNatureArr[$ColumnCounter][] = $thisNature;
						$ColumnCounter++;
					}
					else
					{
						# show parent subject only
						if (!in_array($tmpSubjectID, $CmpSubjectIDList)) {
							$thisMarks = $resultSubject[$tmpSubjectID][$studentID];
							$ExportArr[$iCounter][$jCounter] = $thisMarks;
							$jCounter++;
							
							$markDisplayedArr[$ColumnCounter][] = $thisMarks;
							$thisNature = $lreportcard->returnMarkNature($ClassLevelID, $tmpSubjectID, $thisMarks);
							$markNatureArr[$ColumnCounter][] = $thisNature;
							$ColumnCounter++;
						}
					}
				}
								
				# Summary Info
				if ($showSummaryInfo)
				{
					for($i=0; $i<sizeof($SummaryInfoFields); $i++) {
						$thisRegNo = $studentIDRegNoMap[$studentID];
						$thisClass = $studentIDClassMap[$studentID];
						$tmpInfo = isset($ary[$thisClass][$thisRegNo][$SummaryInfoFields[$i]]) ? $ary[$thisClass][$thisRegNo][$SummaryInfoFields[$i]]: "-";
						
						$ExportArr[$iCounter][$jCounter] = $tmpInfo;
						$jCounter++;
						
						$markDisplayedArr[$ColumnCounter][] = $tmpInfo;
						$ColumnCounter++;
					}
				}
				
				if ($showGrandTotal)
				{
					$ExportArr[$iCounter][$jCounter] = $resultAverage[$studentID]["GrandTotal"];
					$jCounter++;
					
					$markDisplayedArr[$ColumnCounter][] = $resultAverage[$studentID]["GrandTotal"];
					$ColumnCounter++;
				}
				if ($showGrandAverage)
				{
					$ExportArr[$iCounter][$jCounter] = $resultAverage[$studentID]["GrandAverage"];
					$jCounter++;
					
					$markDisplayedArr[$ColumnCounter][] = $resultAverage[$studentID]["GrandTotal"];
					$ColumnCounter++;
				}
				if ($showClassRanking)
				{
					$ExportArr[$iCounter][$jCounter] = $resultAverage[$studentID]["OrderMeritClass"];
					$jCounter++;
					
					$markDisplayedArr[$ColumnCounter][] = $resultAverage[$studentID]["GrandTotal"];
					$ColumnCounter++;
				}
				if ($showOverallRanking)
				{
					$ExportArr[$iCounter][$jCounter] = $resultAverage[$studentID]["OrderMeritForm"];
					$jCounter++;
					
					$markDisplayedArr[$ColumnCounter][] = $resultAverage[$studentID]["GrandTotal"];
					$ColumnCounter++;
				}
				
			} else {
				# specific subject
				if (!isset($ReportColumnID) || empty($ReportColumnID)) {
					# overall terms
					##############################
					// Special handling of subject which having component subjects
					if ($showSubjectComponent && in_array($SubjectID, $ParentSubjectIDList)) {
						foreach($CmpSubjectIDNameArr as $tmpSubjectID => $tmpSubjectName) {
							$thisMarks = $resultDetailSubject[$tmpSubjectID][$studentID];
							$ExportArr[$iCounter][$jCounter] = $thisMarks;
							$jCounter++;
							
							$markDisplayedArr[$ColumnCounter][] = $thisMarks;
							$thisNature = $lreportcard->returnMarkNature($ClassLevelID, $tmpSubjectID, $thisMarks);
							$markNatureArr[$ColumnCounter][] = $thisNature;
							$ColumnCounter++;
						}
						#$markTable .= "<td align='center'>".$resultDetailSubject[$SubjectID][$studentID]."</td>";
					}
					##############################
					$thisMarks = $resultSubject[$studentID];
					$ExportArr[$iCounter][$jCounter] = $thisMarks;
					$jCounter++;
					
					$markDisplayedArr[$ColumnCounter][] = $thisMarks;
					$thisNature = $lreportcard->returnMarkNature($ClassLevelID, $SubjectID, $thisMarks);
					$markNatureArr[$ColumnCounter][] = $thisNature;
					$ColumnCounter++;
					//if ($subjectGradingScheme["scaleDisplay"] == "M") {
					//	$markTable .= "<td align='center'>".$OrderFormSubject[$studentID]."</td>";
					//}
				} else {
					# specific term
					##############################
					// Special handling of subject which having component subjects
					if ($showSubjectComponent && in_array($SubjectID, $ParentSubjectIDList)) {
						foreach($CmpSubjectIDNameArr as $tmpSubjectID => $tmpSubjectName) {
							$thisMarks = $resultDetailSubject[$tmpSubjectID][$studentID];
							$ExportArr[$iCounter][$jCounter] = $thisMarks;
							$jCounter++;
							
							$markDisplayedArr[$ColumnCounter][] = $thisMarks;
							$thisNature = $lreportcard->returnMarkNature($ClassLevelID, $tmpSubjectID, $thisMarks);
							$markNatureArr[$ColumnCounter][] = $thisNature;
							$ColumnCounter++;
						}
						#$markTable .= "<td align='center'>".$resultDetailSubject[$SubjectID][$studentID]."</td>";
					}
					##############################
					$thisMarks = $resultSubject[$studentID];
					$ExportArr[$iCounter][$jCounter] = $thisMarks;
					$jCounter++;
					
					$markDisplayedArr[$ColumnCounter][] = $thisMarks;
					$thisNature = $lreportcard->returnMarkNature($ClassLevelID, $SubjectID, $thisMarks);
					$markNatureArr[$ColumnCounter][] = $thisNature;
					$ColumnCounter++;
					//$markTable .= "<td align='center'>".$OrderFormSubject[$studentID]."</td>";
				}
				
				# Summary Info
				if ($showSummaryInfo)
				{
					for($i=0; $i<sizeof($SummaryInfoFields); $i++) {
						$thisRegNo = $studentIDRegNoMap[$studentID];
						$thisClass = $studentIDClassMap[$studentID];
						$tmpInfo = isset($ary[$thisClass][$thisRegNo][$SummaryInfoFields[$i]]) ? $ary[$thisClass][$thisRegNo][$SummaryInfoFields[$i]]: "-";
						
						$ExportArr[$iCounter][$jCounter] = $tmpInfo;
						$jCounter++;
						
						$markDisplayedArr[$ColumnCounter][] = $tmpInfo;
						$ColumnCounter++;
					}
				}
				
				if ($showGrandTotal)
				{
					$ExportArr[$iCounter][$jCounter] = $resultAverage[$studentID]["GrandTotal"];
					$jCounter++;
					
					$markDisplayedArr[$ColumnCounter][] = $resultAverage[$studentID]["GrandTotal"];
					$ColumnCounter++;
				}
				if ($showGrandAverage)
				{
					$ExportArr[$iCounter][$jCounter] = $resultAverage[$studentID]["GrandAverage"];
					$jCounter++;
					
					$markDisplayedArr[$ColumnCounter][] = $resultAverage[$studentID]["GrandTotal"];
					$ColumnCounter++;
				}
				if ($showClassRanking)
				{
					$ExportArr[$iCounter][$jCounter] = $OrderClassSubject[$studentID];
					$jCounter++;
					
					$markDisplayedArr[$ColumnCounter][] = $resultAverage[$studentID]["GrandTotal"];
					$ColumnCounter++;
				}
				if ($showOverallRanking)
				{
					$ExportArr[$iCounter][$jCounter] = $OrderFormSubject[$studentID];
					$jCounter++;
					
					$markDisplayedArr[$ColumnCounter][] = $resultAverage[$studentID]["GrandTotal"];
					$ColumnCounter++;
				}
			}
			$iCounter++;
		}
		
		if ($showStatistics)
		{
			# Statistics Rows
			$decimalPlacesShown = 2;
			$numEmptyColumn = 2;
			if ($showEnglishName)
				$numEmptyColumn++;
			if ($showChineseName)
				$numEmptyColumn++;
			
			# Average
			$jCounter = 0;
			$ExportArr[$iCounter][$jCounter] = $eReportCard['Template']['Average'];
			$jCounter++;
			for ($i=0; $i<$numEmptyColumn; $i++)
			{
				$ExportArr[$iCounter][$jCounter] = "";
				$jCounter++;
			}
			for ($i=0; $i<count($markDisplayedArr); $i++)
			{
				$thisScaleDisplay = $columnScaleOutputMapArr[$i];
				
				if ($thisScaleDisplay != "M")
				{
					$thisDisplay = $lreportcard->EmptySymbol;
				}
				else
				{
					$thisColumnMarksArr = $markDisplayedArr[$i];
					$thisDisplay = $lreportcard->getAverage($thisColumnMarksArr, $decimalPlacesShown);
				}
				$ExportArr[$iCounter][$jCounter] = $thisDisplay;
				$jCounter++;
			}
			$iCounter++;
			
			# SD
			$jCounter = 0;
			$ExportArr[$iCounter][$jCounter] = $eReportCard['Template']['SD'];
			$jCounter++;
			for ($i=0; $i<$numEmptyColumn; $i++)
			{
				$ExportArr[$iCounter][$jCounter] = "";
				$jCounter++;
			}
			for ($i=0; $i<count($markDisplayedArr); $i++)
			{
				$thisScaleDisplay = $columnScaleOutputMapArr[$i];
				
				if ($thisScaleDisplay != "M")
				{
					$thisDisplay = $lreportcard->EmptySymbol;
				}
				else
				{
					$thisColumnMarksArr = $markDisplayedArr[$i];
					$thisDisplay = $lreportcard->getSD($thisColumnMarksArr, $decimalPlacesShown);
				}
				$ExportArr[$iCounter][$jCounter] = $thisDisplay;
				$jCounter++;
			}
			$iCounter++;
			
			# Highest Mark
			$jCounter = 0;
			$ExportArr[$iCounter][$jCounter] = $eReportCard['Template']['HighestMark'];
			$jCounter++;
			for ($i=0; $i<$numEmptyColumn; $i++)
			{
				$ExportArr[$iCounter][$jCounter] = "";
				$jCounter++;
			}
			for ($i=0; $i<count($markDisplayedArr); $i++)
			{
				$thisScaleDisplay = $columnScaleOutputMapArr[$i];
				
				if ($thisScaleDisplay != "M")
				{
					$thisDisplay = $lreportcard->EmptySymbol;
				}
				else
				{
					$thisColumnMarksArr = $markDisplayedArr[$i];
					$thisDisplay = max($thisColumnMarksArr);
				}
				$ExportArr[$iCounter][$jCounter] = $thisDisplay;
				$jCounter++;
			}
			$iCounter++;
			
			# Lowest Mark
			$jCounter = 0;
			$ExportArr[$iCounter][$jCounter] = $eReportCard['Template']['LowestMark'];
			$jCounter++;
			for ($i=0; $i<$numEmptyColumn; $i++)
			{
				$ExportArr[$iCounter][$jCounter] = "";
				$jCounter++;
			}
			for ($i=0; $i<count($markDisplayedArr); $i++)
			{
				$thisScaleDisplay = $columnScaleOutputMapArr[$i];
				
				if ($thisScaleDisplay != "M")
				{
					$thisDisplay = $lreportcard->EmptySymbol;
				}
				else
				{
					$thisColumnMarksArr = $markDisplayedArr[$i];
					$thisDisplay = min($thisColumnMarksArr);
				}
				$ExportArr[$iCounter][$jCounter] = $thisDisplay;
				$jCounter++;
			}
			$iCounter++;
			
			# Passing Rate
			$jCounter = 0;
			$ExportArr[$iCounter][$jCounter] = $eReportCard['Template']['PassingRate'];
			$jCounter++;
			for ($i=0; $i<$numEmptyColumn; $i++)
			{
				$ExportArr[$iCounter][$jCounter] = "";
				$jCounter++;
			}
			for ($i=0; $i<count($markNatureArr); $i++)
			{
				$thisScaleDisplay = $columnScaleOutputMapArr[$i];
				
				$thisColumnMarksArr = $markNatureArr[$i];
				$thisDisplay = $lreportcard->getPassingRate($thisColumnMarksArr, 2);
				$thisDisplay = ($thisDisplay == "")? $lreportcard->EmptySymbol : $thisDisplay;
					
				$ExportArr[$iCounter][$jCounter] = $thisDisplay;
				$jCounter++;
			}
			if ($showSummaryInfo)
			{
				$ExportArr[$iCounter][$jCounter] = $lreportcard->EmptySymbol;
				$jCounter++;
			}
			if ($showGrandTotal)
			{
				$ExportArr[$iCounter][$jCounter] = $lreportcard->EmptySymbol;
				$jCounter++;
			}
			if ($showGrandAverage)
			{
				$ExportArr[$iCounter][$jCounter] = $lreportcard->EmptySymbol;
				$jCounter++;
			}
			if ($showClassRanking)
			{
				$ExportArr[$iCounter][$jCounter] = $lreportcard->EmptySymbol;
				$jCounter++;
			}
			if ($showOverallRanking)
			{
				$ExportArr[$iCounter][$jCounter] = $lreportcard->EmptySymbol;
				$jCounter++;
			}
		}
		
		// Title of the grandmarksheet
		$schoolData = split("\n",get_file_content($PATH_WRT_ROOT."file/school_data.txt"));
		$schoolName = $schoolData[0];
		$academicYear = $lreportcard->GET_ACTIVE_YEAR("-");
		
		if ($ForSSPA)
		{
			// SSPA report contains last year information, so the acadermic year of the title is across two acadermic years
			$academicYearArr = explode("-", $academicYear);
			$academicYearArr[0] -= 1;
			$academicYear = implode("-", $academicYearArr);
			
			$GMSTitle = "$academicYear $ClassLevelName $ColumnTitle $SubjectName ".$eReportCard['Template']['GrandMarksheet'];
			$GMSTitle .= " (SSPA)";
		}
		else
		{
			$GMSTitle = "$academicYear $ClassLevelName $ColumnTitle $SubjectName ".$eReportCard['Template']['GrandMarksheet'];	
		}
		
		$filename = intranet_undo_htmlspecialchars($GMSTitle.".csv");
		
		$export_content = $lexport->GET_EXPORT_TXT($ExportArr, $exportColumn, "", "\r\n", "", 0, "11");
		intranet_closedb();
		
		// Output the file to user browser
		$lexport->EXPORT_FILE($filename, $export_content);
		
  } else {
?>
You have no priviledge to access this page.
<?
	}
} else {
?>
You have no priviledge to access this page.
<?
}
?>
