<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008w.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_sis.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$lreportcard = new libreportcardSIS();
$linterface = new interface_html();
$lclass = new libclass();

# Retrieve params
$ReportColumnID	= isset($ReportColumnID)? $ReportColumnID : "";
$SubjectID		= isset($SubjectID) 	? $SubjectID 	: "";
$ClassLevelID	= isset($ClassLevelID)	? $ClassLevelID : "";
$ClassID		= isset($ClassID)		? $ClassID : "";
$ReportID		= isset($ReportID)		? $ReportID : "";
$StudentAry = array();
$ListAry = array();
$ListAry2 = array();
// $GradeAry = array();
$DataTemp = array();

# check condition
if(!$ClassLevelID || !$ReportID)
{
	intranet_closedb();
	header("Location: index.php");
}

include_once($PATH_WRT_ROOT."templates/2007a/layout/print_header.php");

# Retrieve Student Array
$classArr = $lreportcard->GET_CLASSES_BY_FORM($ClassLevelID, $ClassID);
foreach($classArr as $key => $data)
{
	$thisClassID = $data['ClassID'];
	$StudentAryTemp = $lreportcard->GET_STUDENT_BY_CLASS($thisClassID);
	foreach($StudentAryTemp as $k=>$d)
	{
		list($thisUserID, $WebSAMS, $ClassNumber, $EngName) = $d;
		$StudentAry[] = $thisUserID;
		
		$lu = new libuser($thisUserID);
		$DataTemp[$thisUserID]['StudentName'] = $lu->EnglishName ." (". $lu->ChineseName .")";
		$DataTemp[$thisUserID]['ClassName'] = $lu->ClassName;
		$DataTemp[$thisUserID]['AdmissionNo'] = $lu->AdmissionNo;
		$DataTemp[$thisUserID]['ClassNumber'] = $lu->ClassNumber; 
	}
}

if($SubjectID)	# 1 subject
{
	$SubjectFormGradingSettings = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID);
	$SchemeID = $SubjectFormGradingSettings[SchemeID];
	$ScaleDisplay = $SubjectFormGradingSettings[ScaleDisplay];
	
// 	# return and init. Grand/Band Array
// 	$SchemeRangeInfo = $lreportcard->GET_GRADING_SCHEME_RANGE_INFO($SchemeID);
// 	foreach($SchemeRangeInfo as $k => $d)
// 	{
// 		$GradeAry[$d['Grade']] = 0;
// 	}	
// 	$GradeAry['ExemptedAbsent']=0;
	
	$SubjectAry[] = $SubjectID;
}

# Grand MS  Title
$SubjectStr = $lreportcard->GET_SUBJECT_NAME_LANG($SubjectID);
$SemNameAry = $lreportcard->returnReportColoumnTitle($ReportID);
$SemStr = $ReportColumnID=="" ? "OverAll" : $SemNameAry[$ReportColumnID];
$Title = "Level Ranking - ". $SemStr ." ". $SubjectStr ." (Primary)";

# define where condition
if($ReportColumnID) $where[] = "ReportColumnID='$ReportColumnID'";
if($SubjectID)	$where[] = "SubjectID='$SubjectID'";
if($ReportID)	$where[] = "ReportID='$ReportID'";
$cons = implode(" and ", $where);
$cons = $cons ? " and ".$cons : "";

foreach($StudentAry as $key => $sid)
{
	$result = $lreportcard->getMarks($ReportID, $sid, $cons);
	
	foreach($result as $subjid => $data)
	{
		foreach($data as $k=>$d)
		{
			$DataTemp[$sid][$subjid]['Mark'] = number_format($d['Mark'],0);
			$DataTemp[$sid][$subjid]['Grade'] = $d['Grade'];
			$DataTemp[$sid][$subjid]['Percent'] = number_format($d['Mark'],0);
		} 
	}
}

foreach($StudentAry as $k=>$stu)
{
	$thisStudentID = $stu;
	
	$thisMark = $DataTemp[$thisStudentID][$SubjectID]['Mark'];
	$thisBand = $lreportcard->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($SchemeID, $thisMark);
	
	list($thisMark, $nsp) = $lreportcard->checkSpCase($ReportID, $SubjectID, $thisMark, $DataTemp[$thisStudentID][$SubjectID]['Grade']);
	if($nsp) 
	{
// 		$GradeAry[$thisBand]++;
		
		$ListAry[$thisStudentID]['StudentName'] = $DataTemp[$thisStudentID]['StudentName'];
		$ListAry[$thisStudentID]['AdmissionNo'] = $DataTemp[$thisStudentID]['AdmissionNo'];
		$ListAry[$thisStudentID]['ClassName'] = $DataTemp[$thisStudentID]['ClassName'];
		$ListAry[$thisStudentID]['ClassNumber'] = $DataTemp[$thisStudentID]['ClassNumber'];
		
		if($ScaleDisplay=="M")	
		{
			$ListAry[$thisStudentID]['Total'] = $thisMark;
			$ListAry[$thisStudentID]['Percent'] = $DataTemp[$thisStudentID][$SubjectID]['Percent'];
		}
		else
		{
			$ListAry[$thisStudentID]['Total'] = $DataTemp[$thisStudentID][$SubjectID]['Grade'];
			$ListAry[$thisStudentID]['Percent'] = "";
		}
		$ListAry[$thisStudentID]['Band'] = $thisBand;
	}
	else
	{
// 		$GradeAry['ExemptedAbsent']++;
		
		$ListAry2[$thisStudentID]['StudentName'] = $DataTemp[$thisStudentID]['StudentName'];
		$ListAry2[$thisStudentID]['AdmissionNo'] = $DataTemp[$thisStudentID]['StudentName'];
		$ListAry2[$thisStudentID]['ClassName'] = $DataTemp[$thisStudentID]['ClassName'];
		$ListAry2[$thisStudentID]['ClassNumber'] = $DataTemp[$thisStudentID]['ClassNumber'];
		$ListAry2[$thisStudentID]['Total'] = $thisMark;
		$ListAry2[$thisStudentID]['Percent'] = "";
		$ListAry2[$thisStudentID]['Band'] = "";
	}
}

# sort by Marks
foreach ($ListAry as $key => $row) 
{
	$field1[$key] = $row['Total'];
}
array_multisort($field1, SORT_DESC, $ListAry);
$ListAry = array_merge($ListAry, $ListAry2);

$display = "";
$xi = 0;
foreach($ListAry as $key=>$row)
{
	$xi++;
	$css = "tablerow" . (($xi%2)+1);
	
	$display .= "<tr>";		
	
	$display .= "<td class=\"$css tabletext\">$xi</td>";
	
	foreach($row as $k => $d)	
		$display .= "<td class=\"$css tabletext\">$d</td>";
	$display .= "</tr>";
}


# Table header
$th = array();
$th[]= "Ranking";
$th[]= "Name";
$th[]= "Student No.";
$th[]= "Class";
$th[]= "Class Index";
$th[]= $SemStr. " Total";
$th[]= "% Total";
$th[]= "Grade";

$x = "<tr><td colspan=\"". sizeof($th) ."\" class=\"tabletext tableline\">School Year: ". getCurrentAcademicYear() ." &nbsp; &nbsp; Level: ". $lclass->getLevelName($ClassLevelID) ."</td></tr>";
$x .= "<tr>";
foreach($th as $k=>$d)
{
	$x .= "<td class=\"tabletext tableline\"><b>".$d."</b></td>";
}
$x .= "</tr>";

// # summary table
// $summary = "<table border=\"0\" cellpadding=\"4\" cellspacing=\"1\" bgcolor=\"#000000\">";
// $summary .= "<tr>
// 		<td class=\"tabletext\" align=\"right\" width=\"150\" bgcolor=\"#FFFFFF\"><b>Band</b></td>
// 		<td class=\"tabletext\" align=\"right\" width=\"50\" bgcolor=\"#FFFFFF\"><b>Pupils</b></td>
// 		<td class=\"tabletext\" align=\"right\" width=\"50\" bgcolor=\"#FFFFFF\"><b>%</b></td>
// 		</tr>
// ";
// foreach($GradeAry as $g=>$d)
// {
// 	if($g=="ExemptedAbsent") $g = "Exempted or Absent";
// 	
// 	$summary .= "<tr>
// 		<td class=\"tabletext\" align=\"right\" bgcolor=\"#FFFFFF\"><b>$g</b></td>
// 		<td class=\"tabletext\" align=\"right\" bgcolor=\"#FFFFFF\">$d</td>
// 		<td class=\"tabletext\" align=\"right\" bgcolor=\"#FFFFFF\">". number_format(($d / sizeof($StudentAry))*100, 1) ."</td>
// 		</tr>
// 	";
// }
// $summary .= "<tr><td class=\"tabletext\" align=\"right\" bgcolor=\"#FFFFFF\">&nbsp;<br><br></td><td class=\"tabletext\" align=\"right\" bgcolor=\"#FFFFFF\">&nbsp;</td><td class=\"tabletext\" align=\"right\" bgcolor=\"#FFFFFF\">&nbsp;</td></tr>";	
// $summary .= "<tr>
// 		<td class=\"tabletext\" align=\"right\" bgcolor=\"#FFFFFF\"><b>Total</b></td>
// 		<td class=\"tabletext\" align=\"right\" bgcolor=\"#FFFFFF\">". sizeof($StudentAry) ."</td>
// 		<td class=\"tabletext\" align=\"right\" bgcolor=\"#FFFFFF\">100%</td>
// 		</tr>
// 	";
// $summary .= "</table>";

?>

<style type='text/css'>
.tableline {
	border-bottom-width: 2px;
	border-bottom-style: solid;
	border-bottom-color: #000000;
}
</style>

<table border="0" cellpadding="4" width="100%" cellspacing="0" class="print_hide">
<tr><td align="right"><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></span></td></tr>
</table>			
										
<!-- Start Main Table //-->
<table width="100%" border="0" cellspacing="0" cellpadding="5">
  <tr> 
    <td align="center" colspan="2"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><h2><strong><i><?=$Title?></i></strong></h2></td>
          </tr>
          <tr>
            <td>
            	<table width="100%" border="0" cellspacing="0" cellpadding="4">
				<?=$x?>
                <?=$display?>
                </table>
            </td>
          </tr>
          <!--
          <tr><td height="1" class="tabletext tableline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
			<tr>
				<td align="right">
				<?=$summary?>
				</td>
			</tr>          
			//-->
          <tr><td height="1" class="tabletext tableline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
          <tr><td class="tabletext">&nbsp;<?=date("l, F d, Y");?></td></tr>
        </table>
          </td>
      </tr>
    </table>
    </td>
  </tr>
</table>

<!-- End Main Table //-->

<table border="0" cellpadding="4" width="100%" cellspacing="0" class="print_hide">
<tr><td align="right"><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></span></td></tr>
</table>
                        
<?
//debug_r($ListAry);

intranet_closedb();
?>
