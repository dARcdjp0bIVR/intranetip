<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008w.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_sis.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");


intranet_auth();
intranet_opendb();

$lreportcard = new libreportcardSIS();
$linterface = new interface_html();
$lclass = new libclass();

# Retrieve params
$ReportID		= $ReportID			? $ReportID : "";
$SubjectID		= $SubjectID 		? $SubjectID 	: "";
$ClassLevelID	= $ClassLevelID		? $ClassLevelID : "";
$ClassID		= $ClassID			? $ClassID : "";
$ClassLevel 	= $lreportcard->returnClassLevel($ClassLevelID);
$PSLevel		= substr($ClassLevel, 0, 1);
$Level			= substr($ClassLevel, 1, 1);
$ReportSetting 	= $lreportcard->returnReportTemplateBasicInfo($ReportID);
$SemID 			= $ReportSetting['Semester'];
$ReportType 	= $SemID == "F" ? "W" : "T";

$StudentAry = array();
$ListAry = array();
$DataTemp = array();

if(!$ClassLevelID || !$ReportID || $ClassLevel=="P1" || $ClassLevel=="P2" || $ReportType=="T")
{
	intranet_closedb();
	header("Location: index.php");
}

include_once($PATH_WRT_ROOT."templates/2007a/layout/print_header.php");

# retrieve ReportColumnID
$ColID = array();
$ReportColoumnTitleTemp = $lreportcard->returnReportTemplateColumnData($ReportID);
foreach($ReportColoumnTitleTemp as $k=>$d)
{
	$ColID[] = $d['ReportColumnID'];
}

# Grand MS  Title
$SubjectStr = $SubjectID ? $lreportcard->GET_SUBJECT_NAME_LANG($SubjectID) : "OverAll";
$LvStr = $ClassID ? $lclass->getClassName($ClassID) : "Same Level";
$Title = "Pupils' Progress of ". $SubjectStr ." Subject Marks In " . $LvStr;

# Retrieve Student Array
$classArr = $lreportcard->GET_CLASSES_BY_FORM($ClassLevelID, $ClassID);
foreach($classArr as $key => $data)
{
	$thisClassID = $data['ClassID'];
	$StudentAryTemp = $lreportcard->GET_STUDENT_BY_CLASS($thisClassID);
	foreach($StudentAryTemp as $k=>$d)
	{
		list($thisUserID, $WebSAMS, $ClassNumber, $EngName) = $d;
		$StudentAry[] = $thisUserID;
		$lu = new libuser($thisUserID);
		$DataTemp[$thisUserID]['StudentName'] = $lu->EnglishName ." (". $lu->ChineseName .")";
		$DataTemp[$thisUserID]['ClassName'] = $lu->ClassName;
		$DataTemp[$thisUserID]['ClassNumber'] = $lu->ClassNumber; 
	}
}

if($SubjectID)	# 1 subject
{
	$SubjectFormGradingSettings = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID);
	$SchemeID = $SubjectFormGradingSettings[SchemeID];
	$ScaleDisplay = $SubjectFormGradingSettings[ScaleDisplay];
	
	$SubjectAry[] = $SubjectID;
}
else			# all subjects
{
	$SubjectAryTmp = $lreportcard->returnSubjectwOrder($ClassLevelID);

	foreach($SubjectAryTmp as $s => $temp)
	{
		$thisSubjectFormGradingSettings = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $s);
		$thisScaleDisplay = $thisSubjectFormGradingSettings[ScaleDisplay];
		
		# check Subject wegith is 0 or not
		$thisWeightAry = $lreportcard->returnReportTemplateSubjectWeightData($ReportID, "SubjectID=$s and ReportColumnID is NULL");
		$thisWeight = $thisWeightAry[0]['Weight'];
		
		if($thisScaleDisplay=="M" && $thisWeight)			$SubjectAry[] = $s;
	}
}

# Table header
$th = array();
$th[]= "&nbsp;";
$th[]= "Student Name";
$th[] = $PSLevel=="P" ? "SA1 Total" : "1st Com";
$th[] = $PSLevel=="P" ? "SA2 Total" : "2nd Com";
$th[]= "Difference (%)";
$th[]= "Class Name";

$tableWidth = sizeof($th);

$x .= "<tr><td colspan=\"". $tableWidth ."\" class=\"GMS_header\">". $Title ."</td></tr>";
$x .= "<tr><td colspan=\"". $tableWidth ."\" class=\"tabletext tableline\">School Year: ". getCurrentAcademicYear() ."</td></tr>";
$x .= "<tr>";
$ki = 0;
foreach($th as $k=>$d)
{
	$style = $ki==1 ? "align='left' width='60%'" : ($ki>1?"align='center' width='10%'":"");
	$x .= "<td class=\"tabletext tableline\" $style><b>".$d."</b></td>";
	$ki++;
}
$x .= "</tr>";

if($PSLevel=="S")
{
	$ReportTypeArr = $lreportcard->GET_REPORT_TYPES($ClassLevelID);
	foreach($ReportTypeArr as $k=>$d)
		$ReportIDList[] = $d['ReportID'];
	sort($ReportIDList);
}

$n = array();
# construct data array for Calculate Total
foreach($StudentAry as $k=>$sid)
{
	if($PSLevel=="P")
	{
		$MarksAry = $lreportcard->getMarks($ReportID, $sid);
		$m = array();
		
		foreach($MarksAry as $sjid=>$data)
		{
			if(in_array($sjid, $SubjectAry)===false)	continue;
			
			foreach($data as $cid=>$m_ary)
			{
				$thisMark = my_round($m_ary['Mark'],1);
				list($thisMark, $nsp) = $lreportcard->checkSpCase($ReportID, $sjid, $thisMark, $m_ary['Grade']);
	 			
				$m[$cid][$sjid] = $thisMark;
				$n[$sid][$cid] = $m_ary['Grade']=="/" ? $n[$sid][$cid] : 1;
			}
		}
			
		foreach($ColID as $k=>$cid)
		{
			$t = $lreportcard->CALCULATE_COLUMN_MARK($ReportID, $ClassLevelID, $StudentID, $m[$cid]);
			$SemTotal[$sid][$cid] = $t['GrandTotal'];	
		}
	}
	else		# Secondary
	{
		if($SubjectID)
		{
			foreach($ReportIDList as $k=>$thisReportID)
			{
				if($thisReportID==$ReportID)	continue;
				
				$MarksAry = $lreportcard->getMarks($thisReportID, $sid, "and ReportColumnID=0 and SubjectID=$SubjectID");
								
 				$thisMark = my_round($MarksAry[$SubjectID][0]['Mark'],1);
 				list($thisMark, $nsp) = $lreportcard->checkSpCase($thisReportID, $SubjectID, $thisMark, $MarksAry[$SubjectID][0]['Grade']);
 				$SemTotal[$sid][$thisReportID] = $thisMark;
 				$n[$sid][$thisReportID] = $MarksAry[$SubjectID][0]['Grade']=="/" ? $n[$sid][$thisReportID] : 1;
			}
		}
		else
		{
			foreach($ReportIDList as $k=>$thisReportID)
			{
				if($thisReportID==$ReportID)	continue;
				$t = $lreportcard->getReportResultScore($thisReportID, 0, $sid);
				$SemTotal[$sid][$thisReportID] = $t['GrandTotal'];	
				$n[$sid][$thisReportID] = 1;
			}
			
		}
	}
	
}


# construct data array
foreach($StudentAry as $k=>$stu)
{
	$thisStudentID = $stu;
	
	$ListAry[$thisStudentID]['StudentName'] = $DataTemp[$thisStudentID]['StudentName'];
	
	if($PSLevel=="P")
	{
		for($i=0; $i<sizeof($ColID); $i++)
		{
			if($n[$thisStudentID][$ColID[$i]])
			{
				$SemTotalTemp[$i] = $SemTotal[$thisStudentID][$ColID[$i]];
				$ListAry[$thisStudentID][$ColID[$i]] = $SemTotal[$thisStudentID][$ColID[$i]];
				$flag = 1;
			}
			else
			{
				$SemTotalTemp[$i] = "/";
				$ListAry[$thisStudentID][$ColID[$i]] = "/";
				$flag = 0;
			}
		}
	}
	else
	{
		for($i=0; $i<sizeof($ReportIDList)-1; $i++)
		{
			if($n[$thisStudentID][$ReportIDList[$i]])
			{
				$SemTotalTemp[$i] = $SemTotal[$thisStudentID][$ReportIDList[$i]];
				$ListAry[$thisStudentID][$ReportIDList[$i]] = $SemTotal[$thisStudentID][$ReportIDList[$i]];
				$flag=1;
			}
			else
			{
				$SemTotalTemp[$i] = "/";
				$ListAry[$thisStudentID][$ReportIDList[$i]] = "/";
				$flag=0;
			}
		}
	}

	# Difference
	$ListAry[$thisStudentID]['Difference'] = $flag ? @my_round((($SemTotalTemp[1] - $SemTotalTemp[0])/$SemTotalTemp[0]) * 100) : "-";
	$ListAry[$thisStudentID]['ClassName'] = $DataTemp[$thisStudentID]['ClassName'];
	$ListAry[$thisStudentID]['ClassNumber'] = $DataTemp[$thisStudentID]['ClassNumber'];
}
# sort by Marks
foreach ($ListAry as $key => $row) 
{
	$field1[$key] = $row['ClassNumber'];
}
array_multisort($field1, SORT_ASC, $ListAry);

$display = "";
$xi = 0;
foreach($ListAry as $key=>$row)
{
	$xi++;
	$css = "tablerow" . (($xi%2)+1);
	
	$display .= "<tr>";		
	$display .= "<td class=\"$css tabletext\">$xi</td>";
	$ki=0;
	foreach($row as $k => $d)	
	{
		if($ki == sizeof($row)-1)	break;
		$display .= "<td class=\"$css tabletext\"". ($ki>0 ? "align='center'" : "").">$d</td>";
		$ki++;
	}
	$display .= "</tr>";
}

$eRtemplateCSS = $lreportcard->getTemplateCSS();
?>
<? if($eRtemplateCSS) {?><link href="<?=$eRtemplateCSS?>" rel="stylesheet" type="text/css" /><? } ?>

<table border="0" cellpadding="4" width="100%" cellspacing="0" class="print_hide">
<tr><td align="right"><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></span></td></tr>
</table>			
										
<!-- Start Main Table //-->
<table width="100%" border="0" cellspacing="0" cellpadding="5">
  <tr> 
    <td align="center" colspan="2"><table width="100%" border="0" cellspacing="0" cellpadding="2">
      <tr>
        <td align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td>
            	<table width="100%" border="0" cellspacing="0" cellpadding="4">
				<?=$x?>
                <?=$display?>
                </table>
            </td>
          </tr>
          <tr><td height="1" class="tabletext tableline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
          <tr><td class="GMS_text14bi">&nbsp;<?=date("l, F d, Y");?></td></tr>
        </table>
          </td>
      </tr>
    </table>
    </td>
  </tr>
</table>

<!-- End Main Table //-->

<table border="0" cellpadding="4" width="100%" cellspacing="0" class="print_hide">
<tr><td align="right"><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></span></td></tr>
</table>
                        
<?
intranet_closedb(); 
?>
