<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "TEACHER";
 
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
// include_once($PATH_WRT_ROOT."includes/libreportcard2008w.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_sis.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$lreportcard = new libreportcardSIS();

$linterface = new interface_html();
$lclass = new libclass();

# Retrieve params
$ReportColumnID	= $ReportColumnID? $ReportColumnID : "0";
$SubjectID		= "";
$ClassLevelID	= $ClassLevelID	? $ClassLevelID : "";
$ClassID		= "";
$ReportID		= $ReportID		? $ReportID : "";

$StudentAry = array();
$ListAry = array();
$ListAry2 = array();
$DataTemp = array();
$ColTotal = array();

# check condition
if(!$ClassLevelID || !$ReportID)
{
	intranet_closedb();
	header("Location: index.php");
}

include_once($PATH_WRT_ROOT."templates/2007a/layout/print_header.php");

# Retrieve Student Array
$classArr = $lreportcard->GET_CLASSES_BY_FORM($ClassLevelID, $ClassID);

foreach($classArr as $key => $data)
{
	$thisClassID = $data['ClassID'];
	$StudentAryTemp = $lreportcard->GET_STUDENT_BY_CLASS($thisClassID);
	foreach($StudentAryTemp as $k=>$d)
	{
		list($thisUserID, $WebSAMS, $ClassNumber, $EngName) = $d;
		$StudentAry[] = $thisUserID;
		
		$lu = new libuser($thisUserID);
		$DataTemp[$thisUserID]['StudentName'] = $lu->EnglishName ." (". $lu->ChineseName .")";
		$DataTemp[$thisUserID]['ClassName'] = $lu->ClassName;
		$DataTemp[$thisUserID]['AdmissionNo'] = $lu->AdmissionNo;
		$DataTemp[$thisUserID]['ClassNumber'] = $lu->ClassNumber; 
		$DataTemp[$thisUserID]['Gender'] = $lu->Gender; 
	}
}

# Grand MS  Title
$Title = "Level Ranking (for Promotion)";

foreach($StudentAry as $key => $sid)
{
	$t = $lreportcard->getReportResultScore($ReportID, 0, $sid);
	$DataTemp[$sid]['Mark'] = $t['GrandAverage'];
	$DataTemp[$sid]['NewClassName'] = $t['NewClassName'];
	
	$ColTotal[$ReportColumnID]['Avg'] += my_round($t['GrandAverage'],1);
	$ColTotal[$ReportColumnID]['no']++;
}

foreach($StudentAry as $k=>$stu)
{
	$thisStudentID = $stu;

	$ListAry[$thisStudentID]['StudentName'] = $DataTemp[$thisStudentID]['StudentName'];
	$ListAry[$thisStudentID]['AdmissionNo'] = $DataTemp[$thisStudentID]['AdmissionNo'];
	$ListAry[$thisStudentID]['Gender'] = $DataTemp[$thisStudentID]['Gender'];
	$ListAry[$thisStudentID]['ClassName'] = $DataTemp[$thisStudentID]['ClassName'];
	$ListAry[$thisStudentID]['ClassNumber'] = $DataTemp[$thisStudentID]['ClassNumber'];
	
	$ListAry[$thisStudentID]['Total'] = $DataTemp[$thisStudentID]['Mark'];
	$ListAry[$thisStudentID]['NewClassName'] = $DataTemp[$thisStudentID]['NewClassName'];
}

# sort by Marks
foreach ($ListAry as $key => $row) 
{
	$field1[$key] = $row['Total'];
}
array_multisort($field1, SORT_DESC, $ListAry);
$ListAry = array_merge($ListAry, $ListAry2);

$display = "";
$xi = 0;
foreach($ListAry as $key=>$row)
{
	$xi++;
	$css = "tablerow" . (($xi%2)+1);
	
	$display .= "<tr>";		
	
	$display .= "<td class=\"$css tabletext\">$xi</td>";
	$ki = 0;
	foreach($row as $k => $d)	
	{
		$display .= "<td class=\"$css tabletext\" align=". ($ki>1 ? "center":"").">$d</td>";
		$ki++;
	}
	$display .= "</tr>";
}

# Footer
$display .= "<tr>";
$display .= "<td>&nbsp;</td>";
$display .= "<td><span class='GMS_text16bi'>Total Students:</span> <span class='tabletext'>". sizeof($StudentAry) ."</span></td>";
$display .= "<td align='right'><span class='GMS_text16bi'>Average:</span></td>";
$display .= "<td>&nbsp;</td>";
$display .= "<td>&nbsp;</td>";	
$display .= "<td>&nbsp;</td>";	
$display .= "<td class='tabletext' align='center'>". @my_round(($ColTotal[$ReportColumnID]['Avg'] / $ColTotal[$ReportColumnID]['no']), 1) ."</td>";
$display .= "</tr>";

# Table header
$th = array();
$th[]= "Ranking";
$th[]= "Name";
$th[]= "Student No.";
$th[]= "Gender";
$th[]= "existing Class";
$th[]= "Class Index";
$th[]= "% Total";
$th[]= "Promotion Status";

$tableWidth = sizeof($th);

$x .= "<tr><td colspan=\"". $tableWidth ."\" class=\"GMS_header\">". $Title ."</td></tr>";
$x .= "<tr><td colspan=\"". $tableWidth ."\" class=\"GMS_header2\">School Year: ". getCurrentAcademicYear() ." &nbsp; &nbsp; Level: ". $lclass->getLevelName($ClassLevelID) ."</td></tr>";

$x .= "<tr>";
$ki = 0;
foreach($th as $k=>$d)
{
	$pars = $ki==1 ? "width=\"100%\" align=\"left\"" : "align=\"center\"";
	$x .= "<td class=\"GMS_text16bi tableline\" valign=\"bottom\" $pars><b>".$d."</b></td>";
	$ki++;
}
$x .= "</tr>";

$eRtemplateCSS = $lreportcard->getTemplateCSS();
?>
<? if($eRtemplateCSS) {?><link href="<?=$eRtemplateCSS?>" rel="stylesheet" type="text/css" /><? } ?>

<table border="0" cellpadding="4" width="100%" cellspacing="0" class="print_hide">
<tr><td align="right"><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></span></td></tr>
</table>			
										
<!-- Start Main Table //-->
<table width="100%" border="0" cellspacing="0" cellpadding="5">
  <tr> 
    <td align="center" colspan="2"><table width="100%" border="0" cellspacing="0" cellpadding="2">
      <tr>
        <td align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td>
            	<table width="100%" border="0" cellspacing="0" cellpadding="4">
				<?=$x?>
                <?=$display?>
                </table>
            </td>
          </tr>
          <tr><td height="1" class="tabletext tableline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
          <tr><td class="GMS_text14bi">&nbsp;<?=date("l, F d, Y");?></td></tr>
        </table>
          </td>
      </tr>
    </table>
    </td>
  </tr>
</table>


<!-- End Main Table //-->

<table border="0" cellpadding="4" width="100%" cellspacing="0" class="print_hide">
<tr><td align="right"><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></span></td></tr>
</table>
                        
<?
intranet_closedb();
?>
