<?php
// Root path
$PATH_WRT_ROOT = "../../../../../../";

// Page access right
$PageRight = "TEACHER";

// Include general libraries
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

// Authorize intranet user & connect Database
intranet_auth();
intranet_opendb();

// Check if ReportCard module is enable
if ($plugin['ReportCard2008']) {
	// Include ReportCard libraries
	include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
	if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
	{
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
		$lreportcard = new libreportcardcustom();
	} else {
		$lreportcard = new libreportcard();
	}
	
	#$linterface = new interface_html();
	$lclass = new libclass();
	
	// Check module access right
	if ($lreportcard->hasAccessRight()) {
		// Check required data submitted from index.php
		// optional: $SubjectID, $ReportColumnID
		if (!isset($ClassLevelID, $ClassID, $ReportID) || empty($ClassLevelID) || empty($ReportID) || empty($ClassID)) {
			intranet_closedb();
			header("Location: index.php");
			exit();
		}
		
		# get display options
		// 1. student name display (EnglishName, ChineseName)
		$showEnglishName = false;
		$showChineseName = false;
		if ($StudentNameDisplay == NULL)
		{
			$numOfNameColumn = 0;
		}
		else
		{
			$StudentNameDisplay = explode(",", $StudentNameDisplay);
			$numOfNameColumn = sizeof($StudentNameDisplay);
			foreach ($StudentNameDisplay as $key => $value)
			{
				if ($value == "EnglishName") $showEnglishName = true;
				if ($value == "ChineseName") $showChineseName = true;
			}
		}		
		// 2. ranking display (ClassRanking, OverallRanking)
		$showClassRanking = false;
		$showOverallRanking = false;
		$RankingDisplay = explode(",", $RankingDisplay);
		foreach ($RankingDisplay as $key => $value)
		{
			if ($value == "ClassRanking") $showClassRanking = true;
			if ($value == "OverallRanking") $showOverallRanking = true;
		}		
		// 3. show subject component (0,1)
		$showSubjectComponent = $ShowSubjectComponent;
		// 4. show Summary Info (0,1)
		$showSummaryInfo = $ShowSummaryInfo;		
		// 5. show Grand Total (0,1)
		$showGrandTotal = $ShowGrandTotal;		
		// 6. show Grand Average (0,1)
		$showGrandAverage = $ShowGrandAverage;
		// 7. ForSSPA => no decimal place in all marks
		$ForSSPA = $ForSSPA;
		
		$reportCalculationOrder = $lreportcard->returnReportTemplateCalculation($ReportID);
		
		// Get ReportCard info
		$reportTemplateInfo = $lreportcard->returnReportTemplateBasicInfo($ReportID);
		$lastGenerated = $reportTemplateInfo["LastGenerated"];
		
		// Check to make sure it is a generated report
		if ($reportTemplateInfo["LastGenerated"] == "0000-00-00 00:00:00" || $reportTemplateInfo["LastGenerated"] == "") {
			intranet_closedb();
			header("Location: index.php");
			exit();
		}
		
		// Get subjects of the current ClassLevel
		$FormSubjectArr = $lreportcard->returnSubjectwOrder($ClassLevelID, 1);
		
		$CmpSubjectIDList = array();
		$ParentSubjectIDList = array();
		$ParentCmpSubjectNum = array();
		
		// Reformat the subject array - Display component subjects also
		$FormSubjectArrFlat = array();
		if (sizeof($FormSubjectArr) > 0) {
			foreach($FormSubjectArr as $tmpSubjectID => $tmpSubjectName) {
				if (sizeof($tmpSubjectName) > 1) {
					$ParentSubjectIDList[] = $tmpSubjectID;
					$ParentCmpSubjectNum[$tmpSubjectID] = sizeof($tmpSubjectName) - 1;
					foreach ($tmpSubjectName as $tmpCmpSubjectID => $tmpCmpSubjectName) {
						if ($tmpCmpSubjectID != 0) {
							$CmpSubjectIDList[] = $tmpCmpSubjectID;
							$FormSubjectArrFlat[$tmpCmpSubjectID] = array(0 => $tmpCmpSubjectName);
						}
					}
				}
				$FormSubjectArrFlat[$tmpSubjectID] = array(0 => $tmpSubjectName[0]);
			}
		}
		$FormSubjectArr = $FormSubjectArrFlat;
		
		// Get component subjects if SubjectID is provided
		if (isset($SubjectID) && $SubjectID !== "" && in_array($SubjectID, $ParentSubjectIDList)) {
			$CmpSubjectArr = $lreportcard->GET_COMPONENT_SUBJECT($SubjectID, $ClassLevelID);
			$CmpSubjectIDNameArr = array();
			$CmpSubjectIDArr = array();
			for($i=0; $i<sizeof($CmpSubjectArr); $i++) {
				$CmpSubjectIDArr[] = $CmpSubjectArr[$i]["SubjectID"];
				$CmpSubjectIDNameArr[$CmpSubjectArr[$i]["SubjectID"]] = $CmpSubjectArr[$i]["SubjectName"];
			}
		}
		
		// Get subjects' grading scheme of the current ClassLevel
		$FormSubjectGradingSchemeArr = $lreportcard->GET_FROM_SUBJECT_GRADING_SCHEME($ClassLevelID);
		
		// Get ClassLevel name
		$ClassLevelName = $lreportcard->returnClassLevel($ClassLevelID);
		
		// Get Classes
		$ClassArr = $lreportcard->GET_CLASSES_BY_FORM($ClassLevelID);
		
		// Get current school year
		$SchoolYear = $lreportcard->schoolYear;
		
		// Get columns info of the current ReportCard
		$ColumnTitleAry = $lreportcard->returnReportColoumnTitle($ReportID);
		$ColumnIDList = array();
		$ColumnIDList = ($ColumnTitleAry == NULL)? NULL : array_keys($ColumnTitleAry);
		
		// Get the list of students (in entire class level)
		$studentList = array();
		$studentIDList = array();
		$studentIDNameMap = array();
		$studentIDClassMap = array();
		$studentIDClassNumMap = array();
		$ClassNameArr = array();
		for($i=0; $i<sizeof($ClassArr); $i++) {
			$ClassNameArr[] = $ClassArr[$i]["ClassName"];
			$studentList = $lreportcard->GET_STUDENT_BY_CLASS($ClassArr[$i]["ClassID"], "", 1);
			for($j=0; $j<sizeof($studentList); $j++) {
				$studentIDNameMap[$studentList[$j]["UserID"]] = $studentList[$j][3];
				$studentIDClassNumMap[$studentList[$j]["UserID"]] = $studentList[$j][2];
				$studentIDClassMap[$studentList[$j]["UserID"]] = $ClassArr[$i]["ClassName"];
				$studentIDList[] = $studentList[$j]["UserID"];
			}
		}
		
		// Students should be already sorted by Class Number in the SQL Query
		$studentIDListSql = implode(",", $studentIDList);
		$cond = "";
		
		// Get overall mark or term/assessment mark
		$ColumnTitle = "";
		if (!isset($ReportColumnID) || empty($ReportColumnID)) {
			$ColumnTitle = $eReportCard['Template']['OverallCombined'];
			$cond .= "AND (ReportColumnID = '' OR ReportColumnID = '0') ";
		} else {
			$ColumnTitle = $ColumnTitleAry[$ReportColumnID];
			$cond .= "AND ReportColumnID = '$ReportColumnID' ";
		}
		
		// Get individual subject mark or grand average (which table to retrieve)
		$SubjectName = "";
		if (!isset($SubjectID) || empty($SubjectID)) {
			// No SubjectID provided, get the averaage result
			$table = $lreportcard->DBName.".RC_REPORT_RESULT";
			$fieldToSelect = "StudentID, GrandTotal, GrandAverage, OrderMeritClass, OrderMeritForm";
			
			$sql = "SELECT $fieldToSelect FROM $table WHERE ";
			$sql .= "ReportID = '$ReportID' ";
			$sql .= "AND StudentID IN ($studentIDListSql) ";
			$sql .= $cond;
			$sql .= "ORDER BY OrderMeritForm";
			$tempArr = $lreportcard->returnArray($sql);
			
			$resultAverage = array();
			$studentIDListSorted = $studentIDList;
			if (sizeof($tempArr) > 0) {
				$studentIDListSorted = array();
				for($j=0; $j<sizeof($tempArr); $j++) {
					$studentIDListSorted[] = $tempArr[$j]["StudentID"];
					$resultAverage[$tempArr[$j]["StudentID"]]["GrandTotal"] = $tempArr[$j]["GrandTotal"];
					$resultAverage[$tempArr[$j]["StudentID"]]["GrandAverage"] = $tempArr[$j]["GrandAverage"];
					
					# Round up all the marks if for SSPA
					if ($ForSSPA)
					{
						$resultAverage[$tempArr[$j]["StudentID"]]["GrandTotal"] = round($resultAverage[$tempArr[$j]["StudentID"]]["GrandTotal"]);
						$resultAverage[$tempArr[$j]["StudentID"]]["GrandAverage"] = round($resultAverage[$tempArr[$j]["StudentID"]]["GrandAverage"]);
					}
					
					if ($tempArr[$j]["OrderMeritClass"] > 0) {
						$resultAverage[$tempArr[$j]["StudentID"]]["OrderMeritClass"] = $tempArr[$j]["OrderMeritClass"];
					} else {
						$resultAverage[$tempArr[$j]["StudentID"]]["OrderMeritClass"] = "-";
					}
					
					if ($tempArr[$j]["OrderMeritForm"] > 0) {
						$resultAverage[$tempArr[$j]["StudentID"]]["OrderMeritForm"] = $tempArr[$j]["OrderMeritForm"];
					} else {
						$resultAverage[$tempArr[$j]["StudentID"]]["OrderMeritForm"] = "-";
					}
				}
			}
			
			// Get all subject marks as additional information
			foreach($FormSubjectArr as $tmpSubjectID => $tmpSubjectName) {
				$subjectGradingScheme = $FormSubjectGradingSchemeArr[$tmpSubjectID];
				$subjectGradingSchemeInfo = $lreportcard->GET_GRADING_SCHEME_MAIN_INFO($subjectGradingScheme["schemeID"]);
				
				if ($subjectGradingScheme["scaleDisplay"] == "M") {
					$fieldToSelect = "StudentID, Mark As MarkUsed";
				} else {
					$fieldToSelect = "StudentID, Grade As MarkUsed";
				}
				$table = $lreportcard->DBName.".RC_REPORT_RESULT_SCORE";
				
				$sql = "SELECT $fieldToSelect FROM $table WHERE ";
				$sql .= "ReportID = '$ReportID' ";
				$sql .= "AND StudentID IN ($studentIDListSql) ";
				$sql .= "AND  SubjectID = '$tmpSubjectID' ";
				$sql .= $cond;
				$tempArr = $lreportcard->returnArray($sql);
				
				$resultSubject[$tmpSubjectID] = array();
				if (sizeof($tempArr) > 0) {
					for($j=0; $j<sizeof($tempArr); $j++) {
						$resultSubject[$tmpSubjectID][$tempArr[$j]["StudentID"]] = $tempArr[$j]["MarkUsed"];
						
						# Round up all the marks if for SSPA
						if ($ForSSPA && $subjectGradingScheme["scaleDisplay"] == "M")
						{
							$resultSubject[$tmpSubjectID][$tempArr[$j]["StudentID"]] = round($resultSubject[$tmpSubjectID][$tempArr[$j]["StudentID"]]);
						}
					}
				}
			}
		} else {
			$subjectGradingScheme = $FormSubjectGradingSchemeArr[$SubjectID];
			$subjectGradingSchemeInfo = $lreportcard->GET_GRADING_SCHEME_MAIN_INFO($subjectGradingScheme["schemeID"]);
			
			if ($subjectGradingScheme["scaleDisplay"] == "M") {
				$fieldToSelect = "StudentID, OrderMeritClass, OrderMeritForm, Mark As MarkUsed";
			} else {
				$fieldToSelect = "StudentID, OrderMeritClass, OrderMeritForm, Grade As MarkUsed";
			}
			$table = $lreportcard->DBName.".RC_REPORT_RESULT_SCORE";
			
			$sql = "SELECT $fieldToSelect FROM $table WHERE ";
			$sql .= "ReportID = '$ReportID' ";
			$sql .= "AND StudentID IN ($studentIDListSql) ";
			$sql .= "AND SubjectID = '$SubjectID' ";
			
			$tempArr = $lreportcard->returnArray($sql.$cond."ORDER BY OrderMeritForm");
			
			$resultSubject = array();
			$studentIDListSorted = array();
			$OrderFormSubject = array();
			if (sizeof($tempArr) > 0) {
				for($j=0; $j<sizeof($tempArr); $j++) {
					$studentIDListSorted[] = $tempArr[$j]["StudentID"];
					$resultSubject[$tempArr[$j]["StudentID"]] = $tempArr[$j]["MarkUsed"];
					
					# Round up all the marks if for SSPA
					if ($ForSSPA && $subjectGradingScheme["scaleDisplay"] == "M")
					{
						$resultSubject[$tempArr[$j]["StudentID"]] = round($resultSubject[$tempArr[$j]["StudentID"]]);
					}
					
					if ($tempArr[$j]["OrderMeritClass"] > 0) {
						$OrderClassSubject[$tempArr[$j]["StudentID"]] = $tempArr[$j]["OrderMeritClass"];
					} else {
						$OrderClassSubject[$tempArr[$j]["StudentID"]] = "-";
					}
					if ($tempArr[$j]["OrderMeritForm"] > 0) {
						$OrderFormSubject[$tempArr[$j]["StudentID"]] = $tempArr[$j]["OrderMeritForm"];
					} else {
						$OrderFormSubject[$tempArr[$j]["StudentID"]] = "-";
					}
				}
			}
			
			// If gettng subject overall mark for a single subject, get the column mark also
			if (!isset($ReportColumnID) || empty($ReportColumnID)) {
				for($i=0; $i<sizeof($ColumnIDList); $i++) {
					$tmpCond = "AND ReportColumnID = '".$ColumnIDList[$i]."'";
					$tempArr = $lreportcard->returnArray($sql.$tmpCond);
					
					$resultSubjectColumn[$ColumnIDList[$i]] = array();
					if (sizeof($tempArr) > 0) {
						for($j=0; $j<sizeof($tempArr); $j++) {
							$resultSubjectColumn[$ColumnIDList[$i]][$tempArr[$j]["StudentID"]] = $tempArr[$j]["MarkUsed"];
							
							# Round up all the marks if for SSPA
							if ($ForSSPA && $subjectGradingScheme["scaleDisplay"] == "M")
							{
								$resultSubjectColumn[$ColumnIDList[$i]][$tempArr[$j]["StudentID"]] = round($resultSubjectColumn[$ColumnIDList[$i]][$tempArr[$j]["StudentID"]]);
							}
						}
					}
				}
			}
			
			#################################
			// Special handling of subject which having component subjects
			if (in_array($SubjectID, $ParentSubjectIDList)) {
				// Push the SubjectID into the same array of its component subjects
				$SubjectPlusCompSubjectIDList = $CmpSubjectIDArr;
				$SubjectPlusCompSubjectIDList[] = $SubjectID;
				
				for($i=0; $i<sizeof($SubjectPlusCompSubjectIDList); $i++) {
					$tmpSubjectID = $SubjectPlusCompSubjectIDList[$i];
					$subjectGradingScheme = $FormSubjectGradingSchemeArr[$tmpSubjectID];
					$subjectGradingSchemeInfo = $lreportcard->GET_GRADING_SCHEME_MAIN_INFO($subjectGradingScheme["schemeID"]);
					
					if ($subjectGradingScheme["scaleDisplay"] == "M") {
						$fieldToSelect = "StudentID, Mark As MarkUsed";
					} else {
						$fieldToSelect = "StudentID, Grade As MarkUsed";
					}
					$table = $lreportcard->DBName.".RC_REPORT_RESULT_SCORE";
					
					$sql = "SELECT $fieldToSelect FROM $table WHERE ";
					$sql .= "ReportID = '$ReportID' ";
					$sql .= "AND StudentID IN ($studentIDListSql) ";
					$sql .= "AND  SubjectID = '$tmpSubjectID' ";
					$sql .= $cond;
					$tempArr = $lreportcard->returnArray($sql);
					
					$resultDetailSubject[$tmpSubjectID] = array();
					if (sizeof($tempArr) > 0) {
						for($j=0; $j<sizeof($tempArr); $j++) {
							$resultDetailSubject[$tmpSubjectID][$tempArr[$j]["StudentID"]] = $tempArr[$j]["MarkUsed"];
							
							# Round up all the marks if for SSPA
							if ($ForSSPA && $subjectGradingScheme["scaleDisplay"] == "M")
							{
								$resultDetailSubject[$tmpSubjectID][$tempArr[$j]["StudentID"]] = round($resultDetailSubject[$tmpSubjectID][$tempArr[$j]["StudentID"]]);
							}
						}
					}
				}
			}
			#################################
			# get Grand Total and Grand Average
			$table = $lreportcard->DBName.".RC_REPORT_RESULT";
			$fieldToSelect = "StudentID, GrandTotal, GrandAverage, OrderMeritClass, OrderMeritForm";
			
			$sql = "SELECT $fieldToSelect FROM $table WHERE ";
			$sql .= "ReportID = '$ReportID' ";
			$sql .= "AND StudentID IN ($studentIDListSql) ";
			$sql .= $cond;
			$tempArr = $lreportcard->returnArray($sql);
			
			$resultAverage = array();
			if (sizeof($tempArr) > 0) {
				for($j=0; $j<sizeof($tempArr); $j++) {
					$resultAverage[$tempArr[$j]["StudentID"]]["GrandTotal"] = $tempArr[$j]["GrandTotal"];
					$resultAverage[$tempArr[$j]["StudentID"]]["GrandAverage"] = sprintf("%02.2f", $tempArr[$j]["GrandAverage"]);
					
					# Round up all the marks if for SSPA
					if ($ForSSPA && $subjectGradingScheme["scaleDisplay"] == "M")
					{
						$resultAverage[$tempArr[$j]["StudentID"]]["GrandTotal"] = round($resultAverage[$tempArr[$j]["StudentID"]]["GrandTotal"]);
						$resultAverage[$tempArr[$j]["StudentID"]]["GrandAverage"] = round($resultAverage[$tempArr[$j]["StudentID"]]["GrandAverage"]);
					}
							
					if ($tempArr[$j]["OrderMeritClass"] > 0) {
						$resultAverage[$tempArr[$j]["StudentID"]]["OrderMeritClass"] = $tempArr[$j]["OrderMeritClass"];
					} else {
						$resultAverage[$tempArr[$j]["StudentID"]]["OrderMeritClass"] = "-";
					}
					if ($tempArr[$j]["OrderMeritForm"] > 0) {
						$resultAverage[$tempArr[$j]["StudentID"]]["OrderMeritForm"] = $tempArr[$j]["OrderMeritForm"];
					} else {
						$resultAverage[$tempArr[$j]["StudentID"]]["OrderMeritForm"] = "-";
					}
				}
			}
		}
		
		# get student class and class number of last year for munsang customization
		if ($studentIDList != NULL)
		{
			$studentIDListSql = implode(",", $studentIDList);
		}
		$activeYear = $lreportcard->GET_ACTIVE_YEAR("-");
		$lastYearArr = explode("-", $activeYear);
		$lastYearArr[0] -= 1; 
		$lastYearArr[1] -= 1; 
		$lastYear = implode("-", $lastYearArr);
		$sql = "SELECT UserID, ClassName, ClassNumber FROM PROFILE_CLASS_HISTORY WHERE UserID IN ($studentIDListSql) AND AcademicYear = '$lastYear' ";
		$result = $lreportcard->returnArray($sql, 3);
		$studentClassHistoryArr = array();
		for ($i=0; $i<sizeof($result); $i++)
		{
			list($tempUserID, $tempClassName, $tempClassNumber) = $result[$i];
			$studentClassHistoryArr[$tempUserID]['ClassName'] = $tempClassName;
			$studentClassHistoryArr[$tempUserID]['ClassNumber'] = $tempClassNumber;
		}
		
		// Get Summary Info from CSV
		# build data array
		$SummaryInfoFields = array("Conduct", "Diligence", "Discipline", "Politeness", "Sociability", "Tidness");
		$ary = array();
		$csvType = $lreportcard->getOtherInfoType();
		if($reportTemplateInfo['Semester']=="F") {
			$InfoTermID = 0;
		} else {
			$InfoTermID = $reportTemplateInfo['Semester']+1;
		}
		if(!empty($csvType)) {
			foreach($csvType as $k=>$Type) {
				for($i=0; $i<sizeof($ClassNameArr); $i++) {
					$csvData = $lreportcard->getOtherInfoData($Type, $InfoTermID, $ClassNameArr[$i]);	
					if(!empty($csvData)) {
						foreach($csvData as $RegNo=>$data) {
							foreach($data as $key=>$val) {
								if (!is_numeric($key) && trim($key) !== "")
									$ary[$ClassNameArr[$i]][$RegNo][$key] = $val;
							}
						}
					}
				}
			}
		}
				
		
		$markTable = "<table id='MarkTable' width='100%' border='1' cellpadding='2' cellspacing='0'>";
		
		// Table header
		$cmpNameRow = "";
		$markTable .= "<thead>";
		$markTable .= "<tr>";
		// show last year class and class number for SSPA
		if ($ForSSPA)
		{
			// if year = 2005-2007, display "06 Class" and "05 Class" as the column titles
			$activeYearDisplay = substr($activeYear,2,2);
			$lastYearDisplay = substr($lastYear,2,2);
			$markTable .= "<th rowspan='2' colspan='2'>".$activeYearDisplay." ".$eReportCard['Template']['StudentInfo']['ClassName']."</th>";
			$markTable .= "<th rowspan='2' colspan='2'>".$lastYearDisplay." ".$eReportCard['Template']['StudentInfo']['ClassName']."</th>";
		}
		else
		{
			# not specific subject			
			$markTable .= "<th rowspan='2'>".$eReportCard['Template']['StudentInfo']['ClassName']."</th>";
			$markTable .= "<th rowspan='2'>".$eReportCard['Template']['StudentInfo']['ClassNo']."</th>";
		}
		
		if ($numOfNameColumn > 0)
		{
			$markTable .= "<th rowspan='2' colspan='$numOfNameColumn'>".$eReportCard['Template']['StudentInfo']['Name']."</th>";
		}
		
		if (!isset($SubjectID) || empty($SubjectID)) {
			
			# not specific subject	
			foreach($FormSubjectArr as $tmpSubjectID => $tmpSubjectName) {
				if ($showSubjectComponent)
				{
					if (in_array($tmpSubjectID, $ParentSubjectIDList)) {
						$markTable .= "<th colspan='".($ParentCmpSubjectNum[$tmpSubjectID]+1)."'>".$tmpSubjectName[0]."</th>";
						$cmpNameRow .= "<th>".$eReportCard['Template']['Total']."</th>";
					} else if (in_array($tmpSubjectID, $CmpSubjectIDList)) {
						$cmpNameRow .= "<th>".$tmpSubjectName[0]."</th>";
					} else {
						$markTable .= "<th rowspan='2'>".$tmpSubjectName[0]."</th>";
					}
				}
				else
				{
					# Hide component subjects
					if (in_array($tmpSubjectID, $ParentSubjectIDList)) {
						$markTable .= "<th>".$tmpSubjectName[0]."</th>";
					}
					else if (!in_array($tmpSubjectID, $CmpSubjectIDList)) 
					{
						$markTable .= "<th rowspan='2'>".$tmpSubjectName[0]."</th>";
					}
				}
				
			}
			
			# Summary Info
			if ($showSummaryInfo)
			{
				for($i=0; $i<sizeof($SummaryInfoFields); $i++) {
					$markTable .= "<th rowspan='2'>".$eReportCard['Template'][$SummaryInfoFields[$i]]."</th>";
				}
			}
						
		} else {
			# specific subject		
			
			$SubjectName = $FormSubjectArr[$SubjectID][0];
			if (!isset($ReportColumnID) || empty($ReportColumnID)) {
				# overall terms
				##############################
				// Special handling of subject which having component subjects
				if ($showSubjectComponent)
				{
					if (in_array($SubjectID, $ParentSubjectIDList)) 
					{
						$countCmpSubject = 1;
						foreach($CmpSubjectIDNameArr as $tmpSubjectID => $tmpSubjectName) {
							$markTable .= "<th>[".$countCmpSubject."]<br />".$tmpSubjectName."</th>";
							$countCmpSubject++;
						}
						#$markTable .= "<th>".$eReportCard['Template']['Total']."</th>";
					}
				}
				
				##############################
				$markTable .= "<th>".$eReportCard['Total']."</th>";
				
				# Summary Info
				if ($showSummaryInfo)
				{
					for($i=0; $i<sizeof($SummaryInfoFields); $i++) {
						$markTable .= "<th rowspan='2'>".$eReportCard['Template'][$SummaryInfoFields[$i]]."</th>";
					}
				}
				
			} else {
				# specific terms
				##############################
				// Special handling of subject which having component subjects
				if ($showSubjectComponent && in_array($SubjectID, $ParentSubjectIDList)) {
					$countCmpSubject = 1;
					$ParentMarkFormula = array();
					foreach($CmpSubjectIDNameArr as $tmpSubjectID => $tmpSubjectName) {
						$OtherCondition = "SubjectID = ".$tmpSubjectID." AND ReportColumnID = $ReportColumnID";
						$CmpSubjectWeight = $lreportcard->returnReportTemplateSubjectWeightData($ReportID, $OtherCondition); 
						$CmpSubjectWeight = ($CmpSubjectWeight[0]['Weight']*100)."%";
						$ParentMarkFormula[] = "[".$countCmpSubject."](".$CmpSubjectWeight.")";
						$markTable .= "<th>[".$countCmpSubject."]<br />".$tmpSubjectName."</th>";
						$countCmpSubject++;
					}
					$ParentMarkFormula = implode(" + ", $ParentMarkFormula);
					$markTable .= "<th>".$eReportCard['Total']."<br />".$ParentMarkFormula."</th>";
					
					#$markTable .= "<th>".$eReportCard['Template']['Total']."</th>";
				} else {
					$markTable .= "<th>".$eReportCard['Total']."</th>";
				}
				
				# Summary Info
				if ($showSummaryInfo)
				{
					for($i=0; $i<sizeof($SummaryInfoFields); $i++) {
						$markTable .= "<th rowspan='2'>".$eReportCard['Template'][$SummaryInfoFields[$i]]."</th>";
					}
				}
				
			}
		}
		$markTable .= ($showGrandTotal)? "<th rowspan='2'>".$eReportCard['Template']['GrandTotal']."</th>" : "";
		$markTable .= ($showGrandAverage)? "<th rowspan='2'>".$eReportCard['Template']['AverageMark']."</th>" : "";		
		$markTable .= ($showClassRanking)? "<th rowspan='2'>".$eReportCard['Template']['ClassPosition']."</th>" : "";
		$markTable .= ($showOverallRanking)? "<th rowspan='2'>".$eReportCard['Template']['FormPosition']."</th>" : "";
		$markTable .= "</tr>";
		
		if ($cmpNameRow != "") {
			$markTable .= "<tr>$cmpNameRow</tr>";
		}
		$markTable .= "</thead>";
		
		// Mark
		$markTable .= "<tbody>";
		foreach($studentIDListSorted as $classNumber => $studentID) {
			$markTable .= "<tr>";
			$markTable .= "<td align='center'>".$studentIDClassMap[$studentID]."</td>";
			$markTable .= "<td align='center'>".$studentIDClassNumMap[$studentID]."</td>";
			
			# show last year class and class number if for SSPA
			if ($ForSSPA)
			{
				$markTable .= "<td align='center'>{$studentClassHistoryArr[$studentID]['ClassName']}</td>";
				$markTable .= "<td align='center'>{$studentClassHistoryArr[$studentID]['ClassNumber']}</td>";
			}
			
			// Separate the student name in "English (Chinese)" format
			$studentNameBi = $studentIDNameMap[$studentID];
			$studentNamePiece = explode("(",$studentNameBi);
			$studentNameEn = trim($studentNamePiece[0]);
			$studentNameCh = trim(str_replace(")", "", $studentNamePiece[1]));
						
			$markTable .= ($showEnglishName)? "<td>".$studentNameEn."</td>" : "";
			$markTable .= ($showChineseName)? "<td>".$studentNameCh."</td>" : "";
			
			if (!isset($SubjectID) || empty($SubjectID)) {
				# overall subjects
				foreach($FormSubjectArr as $tmpSubjectID => $tmpSubjectName) {
					if ($showSubjectComponent)
					{
						$markTable .= "<td align='center'>".$resultSubject[$tmpSubjectID][$studentID]."</td>";
					}
					else
					{
						# show parent subject only
						if (!in_array($tmpSubjectID, $CmpSubjectIDList)) {
							$markTable .= "<td align='center'>".$resultSubject[$tmpSubjectID][$studentID]."</td>";
						}
					}
				}
								
				# Summary Info
				if ($showSummaryInfo)
				{
					for($i=0; $i<sizeof($SummaryInfoFields); $i++) {
						$tmpInfo = isset($ary[$studentIDRegNoMap[$studentIDClassMap[$studentID]][$studentID]][$SummaryInfoFields[$i]]) ? $ary[$studentIDClassMap[$studentID]][$studentIDRegNoMap[$studentID]][$SummaryInfoFields[$i]]: "-";
						$markTable .= "<td align='center'>".$tmpInfo."</td>";
					}
				}
				
				$markTable .= ($showGrandTotal)? "<td align='center'>".$resultAverage[$studentID]["GrandTotal"]."</td>" : "";
				$markTable .= ($showGrandAverage)? "<td align='center'>".$resultAverage[$studentID]["GrandAverage"]."</td>" : "";				
				$markTable .= ($showClassRanking)? "<td align='center'>".$resultAverage[$studentID]["OrderMeritClass"]."</td>" : "";
				$markTable .= ($showOverallRanking)? "<td align='center'>".$resultAverage[$studentID]["OrderMeritForm"]."</td>" : "";
				
			} else {
				# specific subject
				if (!isset($ReportColumnID) || empty($ReportColumnID)) {
					# overall terms
					##############################
					// Special handling of subject which having component subjects
					if ($showSubjectComponent && in_array($SubjectID, $ParentSubjectIDList)) {
						foreach($CmpSubjectIDNameArr as $tmpSubjectID => $tmpSubjectName) {
							$markTable .= "<td align='center'>".$resultDetailSubject[$tmpSubjectID][$studentID]."</td>";
						}
						#$markTable .= "<td align='center'>".$resultDetailSubject[$SubjectID][$studentID]."</td>";
					}
					##############################
					$markTable .= "<td align='center'>".$resultSubject[$studentID]."</td>";
					//if ($subjectGradingScheme["scaleDisplay"] == "M") {
					//	$markTable .= "<td align='center'>".$OrderFormSubject[$studentID]."</td>";
					//}
				} else {
					# specific term
					##############################
					// Special handling of subject which having component subjects
					if ($showSubjectComponent && in_array($SubjectID, $ParentSubjectIDList)) {
						foreach($CmpSubjectIDNameArr as $tmpSubjectID => $tmpSubjectName) {
							$markTable .= "<td align='center'>".$resultDetailSubject[$tmpSubjectID][$studentID]."</td>";
						}
						#$markTable .= "<td align='center'>".$resultDetailSubject[$SubjectID][$studentID]."</td>";
					}
					##############################
					$markTable .= "<td align='center'>".$resultSubject[$studentID]."</td>";
					//$markTable .= "<td align='center'>".$OrderFormSubject[$studentID]."</td>";
				}
				
				# Summary Info
				if ($showSummaryInfo)
				{
					for($i=0; $i<sizeof($SummaryInfoFields); $i++) {
						$tmpInfo = isset($ary[$studentIDRegNoMap[$studentIDClassMap[$studentID]][$studentID]][$SummaryInfoFields[$i]]) ? $ary[$studentIDClassMap[$studentID]][$studentIDRegNoMap[$studentID]][$SummaryInfoFields[$i]]: "-";
						$markTable .= "<td align='center'>".$tmpInfo."</td>";
					}
				}
				
				$markTable .= ($showGrandTotal)? "<td align='center'>".$resultAverage[$studentID]["GrandTotal"]."</td>" : "";
				$markTable .= ($showGrandAverage)? "<td align='center'>".$resultAverage[$studentID]["GrandAverage"]."</td>" : "";
				$markTable .= ($showClassRanking)? "<td align='center'>".$OrderClassSubject[$studentID]."</td>" : "";
				$markTable .= ($showOverallRanking)? "<td align='center'>".$OrderFormSubject[$studentID]."</td>" : "";
			}
			$markTable .= "</tr>";
		}
		$markTable .= "</tbody>";
		$markTable .= "</table>";
		
		// Title of the grandmarksheet
		$schoolData = split("\n",get_file_content($PATH_WRT_ROOT."file/school_data.txt"));
		$schoolName = $schoolData[0];
		$academicYear = $lreportcard->GET_ACTIVE_YEAR("-");
		
		
		
		if ($ForSSPA)
		{
			// SSPA report contains last year information, so the acadermic year of the title is across two acadermic years
			$academicYearArr = explode("-", $academicYear);
			$academicYearArr[0] -= 1;
			$academicYear = implode("-", $academicYearArr);
			
			$GMSTitle = "$schoolName<br />$academicYear $ClassLevelName $ColumnTitle $SubjectName ".$eReportCard['Template']['GrandMarksheet'];
			$GMSTitle .= " (SSPA)";
			
			if (!isset($SubjectID) || empty($SubjectID))
			{
				$NumStudentOfFormTitle = $eReportCard['Template']['NumStudentsOfForm'].": ".count($studentIDList);
				$markTable .= "<table width='100%' border='0' cellpadding='2' cellspacing='0'>";
				$markTable .= "<tr><td class='footer' align='right'>";
				$markTable .= $lastGenerated;
				$markTable .= "</td></tr>";
				$markTable .= "</table>";
			}
			else
			{
				$markTable .= "<table width='100%' border='0' cellpadding='2' cellspacing='0'>";
				$markTable .= "<tr><td>&nbsp;</td></tr>";
				$markTable .= "<tr><td class='footer'>";
				$markTable .= $eReportCard['Template']['SSPA_Footer'];
				$markTable .= "</td></tr>";
				$markTable .= "<tr><td class='signature' align='right'>";
				$markTable .= $eReportCard['Template']['SSPA_Signature'];
				$markTable .= "</td></tr>";
				$markTable .= "</table>";
			}
		}
		else
		{
			$GMSTitle = "$schoolName<br />$academicYear $ClassLevelName $ColumnTitle $SubjectName ".$eReportCard['Template']['GrandMarksheet'];	
		}
		
		############## Interface Start ##############
		
?>
<style type="text/css">
<!--
h1 {
	font-size: 22px;
	font-family: arial, "lucida console", sans-serif;
	margin: 0px;
	padding-top: 3px;
	padding-bottom: 10px;
}

#MarkTable {
	border-collapse: collapse;
	border-color: #000000;
}

#MarkTable thead {
	font-size: 12px;
	font-family: arial, "lucida console", sans-serif;
}

#MarkTable tbody {
	font-size: 12px;
	font-family: arial, "lucida console", sans-serif;
}

.footer {
	font-family: "Arial", "Lucida Console";	
	font-size: 12px;
}
.signature {
	font-family: "Arial", "Lucida Console";	
	font-size: 14px;
}
-->
</style>
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td align="center"><h1><?= $GMSTitle ?></h1></td>
		<td class="footer" align="center" valign="bottom" width="5%" nowrap><?= $NumStudentOfFormTitle ?></td>
	</tr>
	<tr>
		<td colspan="2"><?= $markTable ?></td>
	</tr>
</table>
<br />
<?
  } else {
?>
You have no priviledge to access this page.
<?
	}
} else {
?>
You have no priviledge to access this page.
<?
}
?>
