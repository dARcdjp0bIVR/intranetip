<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008w.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_sis.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$lreportcard = new libreportcardSIS();
$linterface = new interface_html();
$lclass = new libclass();

# Retrieve params
$ReportColumnID	= isset($ReportColumnID) ? $ReportColumnID 	: "";
$SubjectID		= isset($SubjectID) 	? $SubjectID 	: "";
$ClassLevelID	= isset($ClassLevelID)	? $ClassLevelID : "";
$ClassID		= isset($ClassID)		? $ClassID : "";
$ReportID		= isset($ReportID)		? $ReportID : "";

$StudentAry = array();
$ListAry = array();
// $GradeAry = array();
$DataTemp = array();

// if(!$ClassLevelID || !$ReportID || $ReportColumnID!="" || $SubjectID)
// {
// 	intranet_closedb();
// 	header("Location: index.php");
// }

include_once($PATH_WRT_ROOT."templates/2007a/layout/print_header.php");

# Retrieve Student Array
$classArr = $lreportcard->GET_CLASSES_BY_FORM($ClassLevelID, $ClassID);
foreach($classArr as $key => $data)
{
	$thisClassID = $data['ClassID'];
	$StudentAryTemp = $lreportcard->GET_STUDENT_BY_CLASS($thisClassID);
	foreach($StudentAryTemp as $k=>$d)
	{
		list($thisUserID, $WebSAMS, $ClassNumber, $EngName) = $d;
		$StudentAry[] = $thisUserID;
		
		$lu = new libuser($thisUserID);
		$DataTemp[$thisUserID]['StudentName'] = $lu->EnglishName ." (". $lu->ChineseName .")";
		$DataTemp[$thisUserID]['ClassName'] = $lu->ClassName;
		$DataTemp[$thisUserID]['AdmissionNo'] = $lu->AdmissionNo;
		$DataTemp[$thisUserID]['ClassNumber'] = $lu->ClassNumber; 
	}
}

# Subject Array
$SubjectAryTmp = $lreportcard->returnSubjectwOrder($ClassLevelID);
foreach($SubjectAryTmp as $s => $temp)
{
	$thisSubjectFormGradingSettings = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $s);
	$thisScaleDisplay = $thisSubjectFormGradingSettings[ScaleDisplay];
	if($thisScaleDisplay=="M")			$SubjectAry[] = $s;
}

# return and init. Grand/Band Array
// $GradeAryTmp = $lreportcard->returnGrandMSBand($ClassLevelID, '-1');
// foreach($GradeAryTmp as $k => $d)
// {
// 	$GradeAry[$d] = 0;
// }	
// $GradeAry['ExemptedAbsent']=0;

# retrieve GrandTotal 
foreach($StudentAry as $key => $sid)
{
	$ReportResultScore = $lreportcard->getReportResultScore($ReportID, 0, $sid);
	
	$DataTemp[$sid]['Total'] = $ReportResultScore['GrandTotal'];
	$DataTemp[$sid]['Percent'] = number_format($ReportResultScore['GrandAverage'], 1);
	$DataTemp[$sid]['Band'] = $lreportcard->returnGrandMSBand($ClassLevelID, $DataTemp[$sid]['Total']);
// 	$GradeAry[$DataTemp[$sid]['Band']]++;
}

# Grand MS  Title
$SubjectStr ="AllSubjects";	
$SemNameAry = $lreportcard->returnReportColoumnTitle($ReportID);
$SemStr = $ReportColumnID=="" ? "OverAll" : $SemNameAry[$ReportColumnID];
//$Title = $SemStr. " ". $SubjectStr ." By Marks In Same Level";
$Title = "Level Ranking - ". $SemStr ." Total (Primary)";

# define where condition
if($ReportColumnID) 		$where[] = "ReportColumnID='$ReportColumnID'";
if($SubjectID)	$where[] = "SubjectID='$SubjectID'";
if($ReportID)	$where[] = "ReportID='$ReportID'";
$cons = implode(" and ", $where);
$cons = $cons ? " and ".$cons : "";

foreach($StudentAry as $key => $sid)
{
	$result = $lreportcard->getMarks($ReportID, $sid, $cons);
	
	foreach($result as $subjid => $data)
	{
		foreach($data as $k=>$d)
		{
			$DataTemp[$sid][$subjid]['Mark'] = number_format($d['Mark'],0);
			$DataTemp[$sid][$subjid]['Grade'] = $d['Grade'];
			$DataTemp[$sid][$subjid]['Percent'] = number_format($d['Mark'],0);
		} 
	}
}

foreach($StudentAry as $k=>$stu)
{
	$thisStudentID = $stu;
	
	$ListAry[$thisStudentID]['StudentName'] = $DataTemp[$thisStudentID]['StudentName'];
	$ListAry[$thisStudentID]['AdmissionNo'] = $DataTemp[$thisStudentID]['AdmissionNo'];
	$ListAry[$thisStudentID]['ClassName'] = $DataTemp[$thisStudentID]['ClassName'];
	$ListAry[$thisStudentID]['ClassNumber'] = $DataTemp[$thisStudentID]['ClassNumber'];
	
// 	foreach($SubjectAry as $k => $thisSubjectID)
// 	{
// 		$thisMark = $DataTemp[$thisStudentID][$thisSubjectID]['Mark'];
// 		list($thisMark, $nsp) = $lreportcard->checkSpCase($ReportID, $thisSubjectID, $thisMark, $DataTemp[$thisStudentID][$thisSubjectID]['Grade']);
// 		
// 		$ListAry[$thisStudentID][$thisSubjectID] = $thisMark;
// 	}
	$ListAry[$thisStudentID]['Total'] = $DataTemp[$thisStudentID]['Total'];
	$ListAry[$thisStudentID]['Percent'] = $DataTemp[$thisStudentID]['Percent'];
	
}
# sort by Marks
foreach ($ListAry as $key => $row) 
{
	$field1[$key] = $row['Total'];
}
array_multisort($field1, SORT_DESC, $ListAry);

$display = "";
$xi = 0;
foreach($ListAry as $key=>$row)
{
	$xi++;
	$css = "tablerow" . (($xi%2)+1);
	
	$display .= "<tr>";		
	
	$display .= "<td class=\"$css tabletext\">$xi</td>";
	
	foreach($row as $k => $d)	
		$display .= "<td class=\"$css tabletext\">$d</td>";
	$display .= "</tr>";
}


# Table header
$th = array();
$th[]= "Ranking";
$th[]= "Name";
$th[]= "Student No.";
$th[]= "Class";
$th[]= "Class Index";
$th[]= $SemStr. " Total";
$th[]= "% Total";

// $th[]= "&nbsp;";
// $th[]= "Student Name";
// foreach($SubjectAry as $k=>$subjid)
// 	$th[]= $lreportcard->GET_SUBJECT_NAME_LANG($subjid);
// $th[]= "Total";
// $th[]= "Percent";
// $th[]= "Grade";
// $th[]= "Class Name";

$x = "<tr><td colspan=\"". sizeof($th) ."\" class=\"tabletext tableline\">School Year: ". getCurrentAcademicYear() ." &nbsp; &nbsp; Level: ". $lclass->getLevelName($ClassLevelID) ."</td></tr>";
$x .= "<tr>";
foreach($th as $k=>$d)
{
	$x .= "<td class=\"tabletext tableline\"><b>".$d."</b></td>";
}
$x .= "</tr>";


# summary table
//$summary = "<table border=\"01\" cellspacing=\"0\">";
// $summary = "<table border=\"0\" cellpadding=\"4\" cellspacing=\"1\" bgcolor=\"#000000\">";
// $summary .= "<tr>
// 		<td class=\"tabletext\" align=\"right\" width=\"150\" bgcolor=\"#FFFFFF\"><b>Band</b></td>
// 		<td class=\"tabletext\" align=\"right\" width=\"50\" bgcolor=\"#FFFFFF\"><b>Pupils</b></td>
// 		<td class=\"tabletext\" align=\"right\" width=\"50\" bgcolor=\"#FFFFFF\"><b>%</b></td>
// 		</tr>
// ";
// foreach($GradeAry as $g=>$d)
// {
// 	if($g=="ExemptedAbsent") $g = "Exempted or Absent";
// 	
// 	$summary .= "<tr>
// 		<td class=\"tabletext\" align=\"right\" bgcolor=\"#FFFFFF\"><b>$g</b></td>
// 		<td class=\"tabletext\" align=\"right\" bgcolor=\"#FFFFFF\">$d</td>
// 		<td class=\"tabletext\" align=\"right\" bgcolor=\"#FFFFFF\">". number_format(($d / sizeof($StudentAry))*100, 1) ."</td>
// 		</tr>
// 	";
// }
// $summary .= "<tr><td class=\"tabletext\" align=\"right\" bgcolor=\"#FFFFFF\">&nbsp;<br><br></td><td class=\"tabletext\" align=\"right\" bgcolor=\"#FFFFFF\">&nbsp;</td><td class=\"tabletext\" align=\"right\" bgcolor=\"#FFFFFF\">&nbsp;</td></tr>";	
// $summary .= "<tr>
// 		<td class=\"tabletext\" align=\"right\" bgcolor=\"#FFFFFF\"><b>Total</b></td>
// 		<td class=\"tabletext\" align=\"right\" bgcolor=\"#FFFFFF\">". sizeof($StudentAry) ."</td>
// 		<td class=\"tabletext\" align=\"right\" bgcolor=\"#FFFFFF\">100%</td>
// 		</tr>
// 	";
// $summary .= "</table>";

?>

<style type='text/css'>
.tableline {
	border-bottom-width: 2px;
	border-bottom-style: solid;
	border-bottom-color: #000000;
}

.tableline1 {
	border-bottom-width: 1px;
	border-bottom-style: solid;
	border-bottom-color: #000000;
}
</style>

<table border="0" cellpadding="4" width="100%" cellspacing="0" class="print_hide">
<tr><td align="right"><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></span></td></tr>
</table>			
										
<!-- Start Main Table //-->
<table width="100%" border="0" cellspacing="0" cellpadding="5">
  <tr> 
    <td align="center" colspan="2"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><h2><strong><i><?=$Title?></i></strong></h2></td>
          </tr>
          <tr>
            <td>
            	
            	<table width="100%" border="0" cellspacing="0" cellpadding="4">
				<?=$x?>
                <?=$display?>
                </table>
            </td>
          </tr>
          <!--
          <tr><td height="1" class="tabletext tableline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
			<tr>
				<td align="right">
				<?=$summary?>
				</td>
			</tr>          
			//-->
          <tr><td height="1" class="tabletext tableline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
          <tr><td class="tabletext">&nbsp;<?=date("l, F d, Y");?></td></tr>
        </table>
          </td>
      </tr>
    </table>
    </td>  
  </tr>
</table>

<!-- End Main Table //-->

<table border="0" cellpadding="4" width="100%" cellspacing="0" class="print_hide">
<tr><td align="right"><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></span></td></tr>
</table>
                        
<?
//debug_r($ListAry);

intranet_closedb();
?>
