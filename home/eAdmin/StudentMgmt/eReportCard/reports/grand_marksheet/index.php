<?php
// using : 

/***********************************************
 * modification log
 *  20190604 Bill:  [2019-0123-1731-14066]
 *      added Total Unit(s) Failed option   ($eRCTemplateSetting['Report']['GrandMarksheet']['ShowTotalUnitFailed'])
 *  20180628 Bill:  [2017-1204-1601-38164]
 *      added active year selection ($eRCTemplateSetting['Report']['SupportAllActiveYear'])
 * 	20170519 Bill:
 * 		Allow View Group to select all options
 * 	20170111 Bill:	[DM#3136]
 * 		- modified js loadColumnList(), return "No Term Available" disabled drop down list if no valid reportid
 * 	20161116 Bill:	[2016-1115-1700-32235]
 * 		- fixed js error due to building $FormArrCT with a empty element for Class Teacher
 * 	20150402 Bill:
 * 		- add Show User Login option [2015-0306-1036-36206]
 *  20100414 Marcus:
 * 		- modify class selection into multiple selection
 * ***********************************************/

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libteaching.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if ($plugin['ReportCard2008'])
{
    include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
	if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") {
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
		$lreportcard = new libreportcardcustom();
	}
	else {
		$lreportcard = new libreportcard();
	}
	
	// [2017-1204-1601-38164] Set target active year
	if($eRCTemplateSetting['Report']['SupportAllActiveYear'])
	{
	    $targetActiveYear = stripslashes($_REQUEST['targetActiveYear']);
	    if(!empty($targetActiveYear)){
	        $targetYearID = $lreportcard->SET_TEMP_ACTIVE_YEAR($targetActiveYear);
	    }
	}
	
	$lteaching = new libteaching();
	$linterface = new interface_html();
	
	if ($lreportcard->hasAccessRight())
	{
		############## Interface Start ##############
		$linterface = new interface_html();
		
		$CurrentPage = "Reports_GrandMarksheet";
		$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
		
		# Tag information
		$TAGS_OBJ[] = array($eReportCard['Reports_GrandMarksheet']);
		
		# Page navigation (leave the array empty if no need)
		$PAGE_NAVIGATION[] = array($eReportCard['SelectCriteria']);
		$linterface->LAYOUT_START();
		
		$submitBtn = $linterface->GET_ACTION_BTN($button_submit, "submit", "", "submitBtn");
		
		if($ck_ReportCard_UserType=="TEACHER")
		{
			$FormArrCT = $lreportcard->returnSunjectTeacherForm($UserID);
									
			# Check is class teacher or not
			$TeacherClassAry = $lteaching->returnTeacherClassWithLevel($UserID, $lreportcard->GET_ACTIVE_YEAR_ID());
			$TeacherClass = $TeacherClassAry[0]['ClassName'];
			
			// Update FormArrCT if user is class teacher
			if(!empty($TeacherClass))
			{
				for($i=0; $i<sizeof($TeacherClassAry); $i++) {
					$thisClassLevelID = $TeacherClassAry[$i]['ClassLevelID'];
					$searchResult = multiarray_search($FormArrCT , "ClassLevelID", $thisClassLevelID);
					if($searchResult == "-1") {
						$thisAry = array(
							"0"=>$TeacherClassAry[$i]['ClassLevelID'],
							"ClassLevelID"=>$TeacherClassAry[$i]['ClassLevelID'],
							"1"=>$TeacherClassAry[$i]['LevelName'],
							"LevelName"=>$TeacherClassAry[$i]['LevelName']);
						$FormArrCT = array_merge($FormArrCT, array($thisAry));
					}
				}
				
				# sort $FormArrCT
				foreach ($FormArrCT as $key => $row) {
					$field1[$key] 	= $row['ClassLevelID'];
					$field2[$key]  	= $row['LevelName'];
				}
				array_multisort($field1, SORT_ASC, $field2, SORT_ASC, $FormArrCT);
			}
			
			$ClassLevelID = ($ClassLevelID == "") ? $FormArrCT[0][0] : $ClassLevelID;
			$ClassLevelName = $lreportcard->returnClassLevel($ClassLevelID);
			#############################################
			$ClassArrCT = $lreportcard->returnSubjectTeacherClass($UserID, $ClassLevelID);
			#############################################
			
			$FormSubjectArrCT = $lreportcard->returnSubjectwOrder($ClassLevelID, 1, $UserID);
			
			// Update $FormSubjectArrCT if user is class teacher
			if(!empty($TeacherClass))
			{
				$searchResult = multiarray_search($TeacherClassAry , "ClassLevelID", $ClassLevelID);
				if($searchResult != "-1") {
					#############################################
					$searchResult2 = multiarray_search($ClassArrCT , "ClassName", $TeacherClassAry[$searchResult]['ClassName']);
					if($searchResult2 == "-1") {
						$thisAry = array(
							"0"=>$TeacherClassAry[$searchResult]['ClassID'],
							"ClassID"=>$TeacherClassAry[$searchResult]['ClassID'],
							"1"=>$TeacherClassAry[$searchResult]['ClassName'],
							"ClassName"=>$TeacherClassAry[$searchResult]['ClassName'],
							"2"=>$TeacherClassAry[$searchResult]['ClassLevelID'],
							"ClassLevelID"=>$TeacherClassAry[$searchResult]['ClassLevelID']);
						$ClassArrCT = array_merge($ClassArrCT, array($thisAry));
					}
					#############################################
					$FormSubjectArrCT = $lreportcard->returnSubjectwOrder($ClassLevelID, 1);
				}
			}
		}
		
		# Get ClassLevelID (Form) of the reportcard template
		$FormArr = $ck_ReportCard_UserType=="ADMIN" || $ck_ReportCard_UserType=="VIEW_GROUP" ? $lreportcard->GET_ALL_FORMS(1) : $FormArrCT;
		if(count($FormArr) > 0)
		{
			# Filter: Grand Marksheet type (0: Class Summary Report, 1: Level Ranking, 2: Pupils' Progress)
			$grandMarksheetTypeOptions = array();
			for($i=0; $i<sizeof($eReportCard['GrandMarksheetTypeOption']); $i++) {
				$grandMarksheetTypeOptions[] = array($i, $eReportCard['GrandMarksheetTypeOption'][$i]);
			}
			
			# temp 20080627 Yat woon - START
			//$grandMarksheetTypeOptions[3] = array("3", "Level Ranking (for Promotion)");
			# temp 20080627 Yat woon - END
			
			$GrandMarksheetTypeSelection = $linterface->GET_SELECTION_BOX($grandMarksheetTypeOptions, 'name="GrandMarksheetType" id="GrandMarksheetType" class="" onchange="changeGrandMarksheetType(this.options[this.selectedIndex].value)"', '', '');
			$GrandMarksheetType = ($GrandMarksheetType == "") ? $grandMarksheetTypeOptions[0][0] : $GrandMarksheetType;
			
			# Control default display table row when page loaded
			if ($GrandMarksheetType == 0)
			{
				$ClassRowDisplay = "";
				$SubjectRowDisplay = "";
				$StudentNameRowDisplay = "";
				$ClassRankingRowDisplay = "";
				// [2015-0306-1036-36206]
				$ShowUserLoginRowDisplay = "";
				$ShowGenderRowDisplay = "";
				$ShowSubjectRowDisplay = "";
				$ShowSubjectComponentRowDisplay = "";
				$ShowSummaryInfoRowDisplay = "";
				$ShowGrandTotalRowDisplay = "";
				$ShowGrandAverageRowDisplay = "";
				$ShowActualAverageRowDisplay = "";
				$ShowGPARowDisplay = "";
				$ShowForSSPARowDisplay = "";
				$ViewFormatRowDisplay = "";
				$ShowStatisticsRowDisplay = "";
				
				//$SubjectRowDisplay = "display:none";
			}
			else if ($GrandMarksheetType == 1)
			{
				$ClassRowDisplay = "display:none";
				$SubjectRowDisplay = "";
				$StudentNameRowDisplay = "display:none";
				// [2015-0306-1036-36206]
				$ShowUserLoginRowDisplay = "display:none";
				$ShowGenderRowDisplay = "display:none";
				$ClassRankingRowDisplay = "display:none";
				$ShowSubjectRowDisplay = "display:none";
				$ShowSubjectComponentRowDisplay = "display:none";
				$ShowSummaryInfoRowDisplay = "display:none";
				$ShowGrandTotalRowDisplay = "display:none";
				$ShowGrandAverageRowDisplay = "display:none";
				$ShowActualAverageRowDisplay = "display:none";
				$ShowGPARowDisplay = "display:none";
				$ShowForSSPARowDisplay = "display:none";
				$ViewFormatRowDisplay = "";
				$ShowStatisticsRowDisplay = "display:none";
			}
			else
			{
				$ClassRowDisplay = "";
				$SubjectRowDisplay = "";
				$StudentNameRowDisplay = "";
				$ClassRankingRowDisplay = "";
				// [2015-0306-1036-36206]
				$ShowUserLoginRowDisplay = "";
				$ShowGenderRowDisplay = "";
				$ShowSubjectRowDisplay = "";
				$ShowSubjectComponentRowDisplay = "";
				$ShowSummaryInfoRowDisplay = "";
				$ShowGrandTotalRowDisplay = "";
				$ShowGrandAverageRowDisplay = "";
				$ShowActualAverageRowDisplay = "";
				$ShowGPARowDisplay = "";
				$ShowForSSPARowDisplay = "";
				$ViewFormatRowDisplay = "display:none";
				$ShowStatisticsRowDisplay = "";
			}
			
			# Filters - By School Year
			if($eRCTemplateSetting['Report']['SupportAllActiveYear'])
			{
			    include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
			    $lreportcard_ui = new libreportcard_ui();
			    if(!empty($targetActiveYear) && $targetYearID) {
			        $lreportcard_ui = new libreportcard_ui($targetYearID);
			    }
                $ActiveYearSelection = $lreportcard_ui->Get_eRC_Active_Year_Selection('targetActiveYear', $targetActiveYear, ' reloadTargetYear(); ');
			}
			
			# Filters - By Form (ClassLevelID)
			$ClassLevelID = ($ClassLevelID == "") ? $FormArr[0][0] : $ClassLevelID;
			$ClassLevelName = $lreportcard->returnClassLevel($ClassLevelID);
			$FormSelection = $linterface->GET_SELECTION_BOX($FormArr, "name='ClassLevelID' id='ClassLevelID' class='' onchange='loadReportList(this.options[this.selectedIndex].value)'", "", $ClassLevelID);
			
			# Filters - By Class (ClassID)
			$ClassArr = $ck_ReportCard_UserType=="ADMIN" || $ck_ReportCard_UserType=="VIEW_GROUP" ? $lreportcard->GET_CLASSES_BY_FORM($ClassLevelID) : $ClassArrCT;
			//$allClassesOption = array(0=> array("", $eReportCard['AllClasses']));
			//$ClassArr = array_merge($allClassesOption, $ClassArr);
			$ClassID = ($ClassID == "") ? $ClassArr[0][0] : $ClassID;
			//$ClassSelection = $linterface->GET_SELECTION_BOX($ClassArr, 'name="ClassID[]" id="ClassID" class="" onchange="" multiple size=6', "", $ClassID);
			$SelectAllClassBtn = $linterface->GET_SMALL_BTN($button_select_all, "button", "SelectAll('ClassID'); return false;");
			
			# Filters - By Report
			$ReportTypeSelection = '';
			$ReportTypeArr = array();
			$ReportTypeOption = array();
			$ReportTypeOptionCount = 0;
			$ReportTypeArr = $lreportcard->GET_REPORT_TYPES($ClassLevelID);
			if(count($ReportTypeArr) > 0)
			{
				for($j=0 ; $j<count($ReportTypeArr) ; $j++){
					# for checking the existence of ReportID when changing ClassLevel
					// only display report which have been generated
					$reportTemplateInfo = $lreportcard->returnReportTemplateBasicInfo($ReportTypeArr[$j]['ReportID']);
					if ($reportTemplateInfo["LastGenerated"] != "0000-00-00 00:00:00" && $reportTemplateInfo["LastGenerated"] != "") {
						$ReportIDList[] = $ReportTypeArr[$j]['ReportID'];
						$ReportTypeOption[$ReportTypeOptionCount][0] = $ReportTypeArr[$j]['ReportID'];
						$ReportTypeOption[$ReportTypeOptionCount][1] = $ReportTypeArr[$j]['SemesterTitle'];
						$ReportTypeOptionCount++;
					}
				}
			}
			
			$ReportTypeSelectDisabled = "";
			if (sizeof($ReportTypeOption) == 0) {
				$ReportTypeOption[0][0] = "-1";
				$ReportTypeOption[0][1] = $eReportCard['NoReportAvailable'];
				$ReportTypeSelectDisabled = "disabled='disabled'";
			}
			
			$ReportID = ($ReportID == "" || !in_array($ReportID,$ReportIDList)) ? $ReportTypeOption[0][0] : $ReportID;
			$reportTemplateInfo = $lreportcard->returnReportTemplateBasicInfo($ReportID);
			//$ReportTypeSelection = $linterface->GET_SELECTION_BOX($ReportTypeOption, 'name="ReportID" id="ReportID" class="" '.$ReportTypeSelectDisabled.' onchange="loadColumnList(this.options[this.selectedIndex].value)"', '', $ReportID, '');
			$ReportTypeSelection=$lreportcard->Get_Report_Selection($ClassLevelID,$ReportID,'ReportID',"loadColumnList(this.options[this.selectedIndex].value)",0,0,1,"class='' ".$ReportTypeSelectDisabled);
			
			$ReportIDOrder = "";
			if (sizeof($ReportIDList) > 0) {
				sort($ReportIDList);
				$ReportIDOrder = array_search($ReportID, $ReportIDList);
			}
			
			# Filters - By Report Column
			if ($ReportTypeSelectDisabled == "") {
				$ColumnTitleAry = $lreportcard->returnReportColoumnTitle($ReportID);
				$ColumnOption = array();
				$ColumnOption[0] = array("", $eReportCard['Template']['OverallCombined']);
				$countColumn = 1;
				if (sizeof($ColumnTitleAry) > 0) {
					foreach ($ColumnTitleAry as $columnID => $columnTitle) {
						$ColumnOption[$countColumn][0] = $columnID;
						$ColumnOption[$countColumn][1] = $columnTitle;
						$countColumn++;
					}
				}
			} else {
				// disable Submit button
				$submitBtn = $linterface->GET_ACTION_BTN($button_submit, "submit", "", "submitBtn", "disabled='disabled'", 1);
				
				$ColumnOption[0][0] = "-1";
				$ColumnOption[0][1] = $eReportCard['NoTermAvailable'];
				$ColumnSelectDisabled = 'disabled="disabled"';
			}
			$ColumnSelection = $linterface->GET_SELECTION_BOX($ColumnOption, 'name="ReportColumnID" id="ReportColumnID" '.$ColumnSelectDisabled.' onchange="checkNoAllSubject()"', '');
			
			# Filters - By Form Subjects
			// A Flag, called $isFormSubject, is used to identify whether currect Form has that subject or not
			$FormSubjectArr = array();
			$SubjectOption = array();
			//$SubjectOption[0] = array("", $eReportCard['AllSubjects']);
			$count = 0;
			$isFormSubject = 0;		// Default: Assume the selected subject does not belongs to that Form(ClassLevelID)
			$SubjectFormGradingArr = array();
		
			// Get Subjects By Form (ClassLevelID)
			$FormSubjectArr = $ck_ReportCard_UserType=="ADMIN" || $ck_ReportCard_UserType=="VIEW_GROUP" ? $lreportcard->returnSubjectwOrder($ClassLevelID, 1) : $FormSubjectArrCT;		
			if(!empty($FormSubjectArr))
			{
				foreach($FormSubjectArr as $FormSubjectID => $Data)
				{
					if(is_array($Data))
					{
						foreach($Data as $FormCmpSubjectID => $SubjectName)
						{
							$FormSubjectID = ($FormCmpSubjectID==0) ? $FormSubjectID : $FormCmpSubjectID;
							if($FormSubjectID == $SubjectID) {
								$isFormSubject = 1;
							}
							$SubjectFormGradingArr = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $FormSubjectID,0 ,0 ,$ReportID);
							
							// Prepare Subject Selection Box 
							//if($FormCmpSubjectID==0 && $SubjectFormGradingArr["ScaleInput"]=="M" && $SubjectFormGradingArr["ScaleDisplay"]=="M"){
							if($FormCmpSubjectID==0){
								include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
								$SubjObj = new subject($FormSubjectID);
								$LearnCatObj = new Learning_Category($SubjObj->LearningCategoryID);
								$LearnCatName = Get_Lang_Selection($LearnCatObj->NameChi,$LearnCatObj->NameEng);
								$SubjectOption[$count][0] = $FormSubjectID;
								$SubjectOption[$count][1] = $SubjectName;
								$SubjectOption[$count][2] = $LearnCatName;
								$count++;
							}
							//}
						}
					}
				}
			}
			// Use for Selection Box
			// $SubjectListArr[][0] : SubjectID
			// $SubjectListArr[][1] : SubjectName
			
			$SubjectID = ($isFormSubject) ? $SubjectID : '';
			if($SubjectID == ""){
				$SubjectID = $SubjectOption[0][0];
			}
			$SelectedSubjectName = $lreportcard->GET_SUBJECT_NAME($SubjectID);
			
			$SubjectSelection = $linterface->GET_SELECTION_BOX_WITH_OPTGROUP($SubjectOption, 'name="SubjectID[]" id="SubjectID" class="" onchange="checkNoAllSubject()" size="10" multiple', '', $SubjectID);
			//$SubjectSelection = $linterface->GET_SELECTION_BOX($SubjectOption, 'name="SubjectID" id="SubjectID" class="" onchange="checkNoAllSubject()"', '', $SubjectID);
			
			// Alternate sets of subjects & classes, they will be used when User selected a different Class Level
			// They will store as JS array
			$OtherSubjectSelection = array();
			
			for($i=0; $i<sizeof($FormArr); $i++)
			{
				$FormSubjectArrCT = $lreportcard->returnSubjectwOrder($FormArr[$i][0], 1, $UserID);
				$ClassArrCT = $lreportcard->returnSubjectTeacherClass($UserID, $FormArr[$i][0]);
				if(!empty($TeacherClass))
				{
					$searchResult = multiarray_search($TeacherClassAry , "ClassLevelID", $FormArr[$i][0]);
					if($searchResult != "-1")
					{
						$searchResult2 = multiarray_search($ClassArrCT , "ClassName", $TeacherClassAry[$searchResult]['ClassName']);
						if($searchResult2 == "-1")
						{
							$thisAry = array(
								"0"=>$TeacherClassAry[$searchResult]['ClassID'],
								"ClassID"=>$TeacherClassAry[$searchResult]['ClassID'],
								"1"=>$TeacherClassAry[$searchResult]['ClassName'],
								"ClassName"=>$TeacherClassAry[$searchResult]['ClassName'],
								"2"=>$TeacherClassAry[$searchResult]['ClassLevelID'],
								"ClassLevelID"=>$TeacherClassAry[$searchResult]['ClassLevelID']);
							$ClassArrCT = array_merge($ClassArrCT, array($thisAry));
						}
						$FormSubjectArrCT = $lreportcard->returnSubjectwOrder($FormArr[$i][0], 1);
					}
				}
				
				$FormSubjectArr = array();
				$SubjectOption = array();
				//$SubjectOption[0] = array("", $eReportCard['AllSubjects']);
				$count = 0;
				$SubjectFormGradingArr = array();
				
				$FormSubjectArr = $ck_ReportCard_UserType=="ADMIN" || $ck_ReportCard_UserType=="VIEW_GROUP" ? $lreportcard->returnSubjectwOrder($FormArr[$i][0], 1) : $FormSubjectArrCT;
				if(!empty($FormSubjectArr))
				{
					foreach($FormSubjectArr as $FormSubjectID => $Data)
					{
						if(is_array($Data))
						{
							foreach($Data as $FormCmpSubjectID => $SubjectName)
							{
								$FormSubjectID = ($FormCmpSubjectID==0) ? $FormSubjectID : $FormCmpSubjectID;								
								$SubjectFormGradingArr = $lreportcard->GET_SUBJECT_FORM_GRADING($FormArr[$i][0], $FormSubjectID,0 ,0 ,$ReportID);
								
								// Prepare Subject Selection Box 
								//if($FormCmpSubjectID==0 && $SubjectFormGradingArr["ScaleInput"]=="M" && $SubjectFormGradingArr["ScaleDisplay"]=="M"){
								if($FormCmpSubjectID==0) {
									include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
									$SubjObj = new subject($FormSubjectID);
									$LearnCatObj = new Learning_Category($SubjObj->LearningCategoryID);
									$LearnCatName = Get_Lang_Selection($LearnCatObj->NameChi,$LearnCatObj->NameEng);
									$SubjectOption[$count][0] = $FormSubjectID;
									$SubjectOption[$count][1] = $SubjectName;
									$SubjectOption[$count][2] = $LearnCatName;
									$count++;
								}
							}
						}
					}
				}
				
				$SubjectID = $SubjectOption[0][0];				
				$SelectedSubjectName = $lreportcard->GET_SUBJECT_NAME($SubjectID);
				//$OtherSubjectSelection[$FormArr[$i][0]] = $linterface->GET_SELECTION_BOX($SubjectOption, 'name="SubjectID" id="SubjectID" class="" onchange="checkNoAllSubject()"', '', $SubjectID);	
				$OtherSubjectSelection[$FormArr[$i][0]] = $linterface->GET_SELECTION_BOX_WITH_OPTGROUP($SubjectOption, 'name="SubjectID[]" id="SubjectID" class="" onchange="checkNoAllSubject()" size="10" multiple', '', $SubjectID);
								
				# Filters - By Class (ClassID)
				$ClassArr = $ck_ReportCard_UserType=="ADMIN" || $ck_ReportCard_UserType=="VIEW_GROUP" ? $lreportcard->GET_CLASSES_BY_FORM($FormArr[$i][0]) : $ClassArrCT;
				//$ClassArr = array_merge($allClassesOption, $ClassArr);
				$ClassID = $ClassArr[0][0];
				$OtherClassSelection[$FormArr[$i][0]] = $linterface->GET_SELECTION_BOX($ClassArr, 'id="ClassID" name="ClassID[]" class="" onchange="" multiple size=6', "", $ClassID);
			}
			
			$SelectAllBtn = $linterface->GET_SMALL_BTN($button_select_all, "button", "SelectAll('SubjectID'); return false;");
			
			// Alternate sets of Reports, they will be used when User selected a different Class Level
			// They will store as JS array
			$OtherReportTypeSelection = array();
			
			// default ReportColumn dropdown list for "No Report Available"
			$ColumnOption[0][0] = "-1";
			$ColumnOption[0][1] = $eReportCard['NoTermAvailable'];
			$ColumnSelectDisabled = 'disabled="disabled"';
			$OtherReportColumnSelection["-1"] = $linterface->GET_SELECTION_BOX($ColumnOption, 'name="ReportColumnID" id="ReportColumnID" '.$ColumnSelectDisabled.' onchange=""', '');
			
			for($i=0; $i<sizeof($FormArr); $i++)
			{
				$ClassLevelName = $lreportcard->returnClassLevel($FormArr[$i][0]);
				$ReportTypeArr = array();
				$ReportTypeOption = array();
				$ReportTypeArr = $lreportcard->GET_REPORT_TYPES($FormArr[$i][0]);
				$ReportTypeOptionCount = 0;
				$ReportIDList = array();
				$ReportIDOrder = 0;
				
				if(count($ReportTypeArr) > 0)
				{
					for($j=0; $j<sizeof($ReportTypeArr); $j++)
					{
						$reportTemplateInfo = $lreportcard->returnReportTemplateBasicInfo($ReportTypeArr[$j]['ReportID']);
						if ($reportTemplateInfo["LastGenerated"] != "0000-00-00 00:00:00" && $reportTemplateInfo["LastGenerated"] != "") {
							$ReportTypeOption[$ReportTypeOptionCount][0] = $ReportTypeArr[$j]['ReportID'];
							$ReportTypeOption[$ReportTypeOptionCount][1] = $ReportTypeArr[$j]['SemesterTitle'];
							$ReportTypeOptionCount++;
							
							$ReportIDList[] = $ReportTypeArr[$j]['ReportID'];
						}
					}
					sort($ReportIDList);
					
					for($j=0; $j<sizeof($ReportTypeArr); $j++)
					{
						$reportTemplateInfo = $lreportcard->returnReportTemplateBasicInfo($ReportTypeArr[$j]['ReportID']);
						$ReportIDOrder = array_search($ReportTypeArr[$j]['ReportID'], $ReportIDList);
						if ($reportTemplateInfo["LastGenerated"] != "0000-00-00 00:00:00" && $reportTemplateInfo["LastGenerated"] != "")
						{
							// Alternate sets of ReportColumn (Assessment, Term or Whole year)
							$ColumnTitleAry = $lreportcard->returnReportColoumnTitle($ReportTypeArr[$j]['ReportID']);
							$ColumnOption = array();
							
							$ColumnOption[0] = array("", $eReportCard['Template']['OverallCombined']);
							$countColumn = 1;
							if (sizeof($ColumnTitleAry) > 0) {
								foreach ($ColumnTitleAry as $columnID => $columnTitle) {
									$ColumnOption[$countColumn][0] = $columnID;
									$ColumnOption[$countColumn][1] = $columnTitle;
									$countColumn++;
								}
							}
							
							$OtherReportColumnSelection[$ReportTypeArr[$j]['ReportID']] = $linterface->GET_SELECTION_BOX($ColumnOption, 'name="ReportColumnID" id="ReportColumnID" class="" onchange="checkNoAllSubject()"', '');
							
							$reportTypeArr[$ReportTypeArr[$j]['ReportID']] = $reportTemplateInfo['Semester'];
							$reportCalculationOrder = $lreportcard->returnReportTemplateCalculation($ReportTypeArr[$j]['ReportID']);
							$calculationOrderArr[$ReportTypeArr[$j]['ReportID']] = $reportCalculationOrder;
						}
					}
				}
				
				$ReportTypeSelectDisabled = "";
				if (sizeof($ReportTypeOption) == 0) {
					$ReportTypeOption[0][0] = "-1";
					$ReportTypeOption[0][1] = $eReportCard['NoReportAvailable'];
					$ReportTypeSelectDisabled = 'disabled="disabled"';
				}
				
				//$OtherReportTypeSelection[$FormArr[$i][0]] = $linterface->GET_SELECTION_BOX($ReportTypeOption, 'name="ReportID" id="ReportID" '.$ReportTypeSelectDisabled.' onchange="loadColumnList(this.options[this.selectedIndex].value)"', '');
				$OtherReportTypeSelection[$FormArr[$i][0]]=$lreportcard->Get_Report_Selection($FormArr[$i][0],'','ReportID',"loadColumnList(this.options[this.selectedIndex].value)",0,0,1,"class='' ".$ReportTypeSelectDisabled);
			}
		}
?>

<script type="text/JavaScript" language="JavaScript">
// Preloaded sets of Report list
var classLevelReportList = new Array();
var classLevelSubjectList = new Array();
var classLevelReportColunmList = new Array();
var classLevelClassList = new Array();
var calculationOrderList = new Array();
var reportTypeList = new Array();

<?php
if(count($FormArr) > 0 && count($reportTypeArr) > 0) {
	foreach($OtherReportTypeSelection as $key => $value) {
		$value = addslashes(str_replace("\n", "", $value));
		$x .= "classLevelReportList[".$key."] = '$value';\n";
	}
	echo $x;
	
	foreach($OtherSubjectSelection as $key => $value) {
		$value = addslashes(str_replace("\n", "", $value));
		$y .= "classLevelSubjectList[".$key."] = '$value';\n";
	}
	echo $y;
	
	foreach($OtherReportColumnSelection as $key => $value) {
		$value = addslashes(str_replace("\n", "", $value));
		$z .= "classLevelReportColunmList[".$key."] = '$value';\n";
	}
	echo $z;
	
	foreach($OtherClassSelection as $key => $value) {
		$value = addslashes(str_replace("\n", "", $value));
		$w .= "classLevelClassList[".$key."] = '$value';\n";
	}
	echo $w;
	
	if (count($calculationOrderArr) > 0)
	{
		foreach($calculationOrderArr as $key => $value) {
			$a .= "calculationOrderList[".$key."] = '$value';\n";
		}
		echo $a;
	}
	
	if (count($reportTypeArr) > 0)
	{
		foreach($reportTypeArr as $key => $value) {
			$b .= "reportTypeList[".$key."] = '$value';\n";
		}
		echo $b;
	}
}
?>

function loadReportList(classLevelID) {
	document.getElementById("ReportList").innerHTML = classLevelReportList[classLevelID];
	document.getElementById("SubjectList").innerHTML = classLevelSubjectList[classLevelID];
	document.getElementById("ClassList").innerHTML = classLevelClassList[classLevelID];
	
	var reportIDSelect = document.getElementById("ReportID");
	//loadColumnList(reportIDSelect.options[reportIDSelect.selectedIndex].value);
	loadColumnList($('select#ReportID').val());
	
	if (reportIDSelect.disabled) {
		document.getElementById("submitBtn").disabled = true;
		document.getElementById("submitBtn").className = 'formbutton_disable';
		document.getElementById("submitBtn").onmouseover = " this.className = 'formbutton_disable' ";
		document.getElementById("submitBtn").onmouseout = " this.className = 'formbutton_disable' ";
		
	} else {
		document.getElementById("submitBtn").disabled = false;
		document.getElementById("submitBtn").className = 'formbutton';
		document.getElementById("submitBtn").onmouseover = " this.className = 'formbutton' ";
		document.getElementById("submitBtn").onmouseout = " this.className = 'formbutton' ";
	}
	
	SelectAll('SubjectID');
	SelectAll('ClassID');
	//checkShowGrandRows();
}

function loadColumnList(reportID) {
	if(reportID)
		document.getElementById("ColumnList").innerHTML = classLevelReportColunmList[reportID];
	else
		document.getElementById("ColumnList").innerHTML = classLevelReportColunmList[-1];
	
	var grandMarkSheetTypeSelect = document.getElementById("GrandMarksheetType");
	changeGrandMarksheetType(grandMarkSheetTypeSelect.options[grandMarkSheetTypeSelect.selectedIndex].value);
	
	//checkShowGrandRows();
}

function changeGrandMarksheetType(grandMarkSheetType)
{
	var ClassRow = document.getElementById("ClassRow");
	var SubjectRow = document.getElementById("SubjectRow");
	var StudentNameDisplayRow = document.getElementById("StudentNameDisplayRow");
	var ClassRankingRow = document.getElementById("ClassRankingRow");
	var ShowSubjectRow = document.getElementById("ShowSubjectRow");
    var ShowSubjectComponentRow = document.getElementById("ShowSubjectComponentRow");
	var ShowSummaryInfoRow = document.getElementById("ShowSummaryInfoRow");
	var ShowGrandTotalRow = document.getElementById("ShowGrandTotalRow");
	var ShowGPARow = document.getElementById("ShowGPARow");
	var ShowGrandAverageRow = document.getElementById("ShowGrandAverageRow");
	var ShowGrandAverageGradeRow = document.getElementById("ShowGrandAverageGradeRow");
	var ShowGrandAverageRow = document.getElementById("ShowGrandAverageRow");
	var ShowForSSPARow = document.getElementById("ShowForSSPARow");
	var ShowUserLoginRow = document.getElementById("ShowUserLoginRow");
	var ShowGenderRow = document.getElementById("ShowGenderRow");
	
	<? if ($eRCTemplateSetting['Calculation']['ActualAverage']) { ?>
		var ShowActualAverageRow = document.getElementById("ShowActualAverageRow");
		var ShowActualAverageGradeRow = document.getElementById("ShowActualAverageGradeRow");
	<? } ?>
	
	if (grandMarkSheetType == 0) {
		ClassRow.style.display = "";
		SubjectRow.style.display = "";
		StudentNameDisplayRow.style.display = "";
		ClassRankingRow.style.display = "";
		ShowUserLoginRow.style.display = "";
		ShowGenderRow.style.display = "";
		ShowSubjectRow.style.display = "";
	    ShowSubjectComponentRow.style.display = "";
		ShowSummaryInfoRow.style.display = "";
		ShowGrandTotalRow.style.display = "";
		ShowGrandAverageRow.style.display = "";
		ShowGrandAverageGradeRow.style.display = "";
		<? if ($eRCTemplateSetting['Calculation']['ActualAverage']) { ?>
			ShowActualAverageRow.style.display = "";
			ShowActualAverageGradeRow.style.display = "";
		<? } ?>
		ShowGPARow.style.display = "";
		ShowForSSPARow.style.display = "";
		
		//SubjectRow.style.display = "none";
	} else if (grandMarkSheetType == 1) {
		ClassRow.style.display = "none";
		SubjectRow.style.display = "";
		StudentNameDisplayRow.style.display = "";
		ClassRankingRow.style.display = "";
		ShowUserLoginRow.style.display = "";
		ShowGenderRow.style.display = "";
		ShowSubjectRow.style.display = "";
		ShowSubjectComponentRow.style.display = "";
		ShowSummaryInfoRow.style.display = "";
		ShowGrandTotalRow.style.display = "";
		ShowGrandAverageRow.style.display = "";
		ShowGrandAverageGradeRow.style.display = "";
		<? if ($eRCTemplateSetting['Calculation']['ActualAverage']) { ?>
			ShowActualAverageRow.style.display = "";
			ShowActualAverageGradeRow.style.display = "";
		<? } ?>
		ShowGPARow.style.display = "";
		ShowForSSPARow.style.display = "";
	} else {
		ClassRow.style.display = "";
		SubjectRow.style.display = "";
		StudentNameDisplayRow.style.display = "none";
		ClassRankingRow.style.display = "none";
		ShowUserLoginRow.style.display = "none";
		ShowGenderRow.style.display = "none";
		ShowSubjectRow.style.display = "none";
		ShowSubjectComponentRow.style.display = "none";
		ShowSummaryInfoRow.style.display = "none";
		ShowGrandTotalRow.style.display = "none";
		ShowGrandAverageRow.style.display = "none";
		ShowGrandAverageGradeRow.style.display = "none";
		<? if ($eRCTemplateSetting['Calculation']['ActualAverage']) { ?>
			ShowActualAverageRow.style.display = "none";
			ShowActualAverageGradeRow.style.display = "none";
		<? } ?>
		ShowGPARow.style.display = "none";
		ShowForSSPARow.style.display = "none";
	}
	
	//checkShowGrandRows();
}

function checkNoAllSubject() {
	/*
	var reportColumnIDSelect = document.getElementById("ReportColumnID");
	var subjectIDSelect = document.getElementById("SubjectID");
	
	var reportColumnIDSelectValue = reportColumnIDSelect.options[reportColumnIDSelect.selectedIndex].value;
	var subjectIDSelectValue = subjectIDSelect.options[subjectIDSelect.selectedIndex].value;
	
	if (subjectIDSelectValue == "" && reportColumnIDSelectValue != "" && reportColumnIDSelectValue != "-1" && reportColumnIDSelectValue != "first" && reportColumnIDSelectValue != "second") {
		alert("<?= $eReportCard['GrandMarksheetTypeAlert2'] ?>");
		reportColumnIDSelect.selectedIndex = 0;
	}
	*/
	
	//checkShowGrandRows();
	
	return true;
}

function resetForm() {
	document.form1.reset();
	return true;
}

function checkForm() {
	//obj = document.form1;
	//obj.submit();
	return true;
}

function checkShowGrandRows(){
	var reportIDSelect = document.getElementById("ReportID");
	var reportColumnIDSelect = document.getElementById("ReportColumnID");
	var subjectIDSelect = document.getElementById("SubjectID");
	
	var reportIDSelectValue = reportIDSelect.options[reportIDSelect.selectedIndex].value;
	var reportColumnIDSelectValue = reportColumnIDSelect.options[reportColumnIDSelect.selectedIndex].value;
	var subjectIDSelectValue = subjectIDSelect.options[subjectIDSelect.selectedIndex].value;	
	
	var ShowGrandTotalRow = document.getElementById("ShowGrandTotalRow");
	var ShowGrandAverageRow = document.getElementById("ShowGrandAverageRow");
	var ClassRankingRow = document.getElementById("ClassRankingRow");
		
	if ((calculationOrderList[reportIDSelectValue] != 2) && (reportColumnIDSelectValue != "") && (subjectIDSelectValue != ""))
	{
		//disable Grand Total and Grand Average if the calculation order is right-down AND a specific term is selected
		ShowGrandTotalRow.style.display = "none";
		ShowGrandAverageRow.style.display = "none";
		ClassRankingRow.style.display = "none";
	}
	else
	{
		ShowGrandTotalRow.style.display = "";
		ShowGrandAverageRow.style.display = "";	
		ClassRankingRow.style.display = "";
	}	
}

function reloadTargetYear()
{
	document.form1.action = 'index.php';
	document.form1.target = '';
	document.form1.submit();
}

function SelectAll(ObjID)
{
	$("#"+ObjID+" option").attr("selected","selected");
}

$().ready(function(){
	loadReportList($("#ClassLevelID").val());
	SelectAll('SubjectID');
	SelectAll('ClassID');
})

</script>
<link type="text/css" rel="stylesheet" href="<?=$PATH_WRT_ROOT."templates/2007a/css/"?>ereportcard.css">
<br />
<form name="form1" action="generate.php" method="POST" onsubmit="return checkForm();" target="_blank">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	</tr>
<table>
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>
			<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr>
					<td></td>
					<td align="right"><?=$linterface->GET_SYS_MSG($Result);?></td>
				</tr>
			<?php if(count($FormArr) == 0 || count($reportTypeArr) == 0) { ?>
				<tr>
					<td align="center" colspan="2" class='tabletext'>
						<br /><?= $i_no_record_exists_msg ?><br />
					</td>
				</tr>
			<?php } else { ?>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?= $eReportCard['GrandMarksheetType'] ?>
					</td>
					<td width="75%" class='tabletext'>
						<span id="GrandMarksheetTypeList"><?= $GrandMarksheetTypeSelection ?></span>
					</td>
				</tr>
				<?php if($eRCTemplateSetting['Report']['SupportAllActiveYear']) { ?>
    				<tr>
    					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
    						<?= $eReportCard['SchoolYear'] ?>
    					</td>
    					<td width="75%" class='tabletext'>
    						<?= $ActiveYearSelection ?>
    					</td>
    				</tr>
				<?php }?>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?= $eReportCard['Form'] ?>
					</td>
					<td width="75%" class='tabletext'>
						<?= $FormSelection ?>
					</td>
				</tr>
				<tr id="ClassRow" style="<?= $ClassRowDisplay ?>">
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?= $eReportCard['Class'] ?>
					</td>
					<td width="75%" class='tabletext'>
						<span id="ClassList"><?= $ClassSelection ?></span><?=$SelectAllClassBtn?>
					</td>
				</tr>
				<tr id="SubjectRow" style="<?= $SubjectRowDisplay ?>">
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?= $eReportCard['Subject'] ?>
					</td>
					<td width="75%" class='tabletext'>
						<span id="SubjectList"><?= $SubjectSelection ?></span><?=$SelectAllBtn?>
					</td>
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?= $eReportCard['ReportCard'] ?>
					</td>
					<td width="75%" class='tabletext'>
						<span id="ReportList"><?= $ReportTypeSelection ?></span>
					</td>
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?= $eReportCard['Term'] ?>
					</td>
					<td width="75%" class='tabletext'>
						<span id="ColumnList"><?= $ColumnSelection ?></span>
					</td>
				</tr>
				
				<?
				# Escola Tong Nam and Munsang Primary do not support exporting GrandMS to CSV
				if ($ReportCardCustomSchoolName!="escola_tong_nam" && $ReportCardCustomSchoolName!="munsang_pri")
				{
				?>
				
				<tr id="ViewFormatRow" style="<?= $ViewFormatRowDisplay ?>">
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?= $eReportCard['ViewFormat'] ?>
					</td>
					<td width="75%" class='tabletext'>
						<input type="radio" name="ViewFormat" value="html" CHECKED>
							<label for="ShowSubjectComponent">
								<?=$eReportCard['HTML']?>
							</label>
						</input>
						<input type="radio" name="ViewFormat" value="csv">
							<label for="ShowSubjectComponent">
								<?=$eReportCard['CSV']?>
							</label>
						</input>
					</td>
				</tr>				
				<? } ?>
				
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				
				<!-- Subject Name Display-->
				<tr id="ShowGenderRow" style="<?= $ShowGenderRowDisplay ?>">
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?= $eReportCard['SubjectDisplay'] ?>
					</td>
					<td width="75%" class='tabletext'>
						<input type="radio" name="SubjDisplay" value="Desc" CHECKED>
							<label for="SubjDisplay">
								<?=$eReportCard['Desc']?>
							</label>
						</input>
						<input type="radio" name="SubjDisplay" value="Abbr">
							<label for="SubjDisplay">
								<?=$eReportCard['Abbr']?>
							</label>
						</input>
						<input type="radio" name="SubjDisplay" value="ShortName">
							<label for="SubjDisplay">
								<?=$eReportCard['ShortName']?>
							</label>
						</input>
					</td>
				</tr>
				
				<!-- Student Name Display -->
				<tr id="StudentNameDisplayRow" style="<?= $StudentNameRowDisplay ?>">
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?= $eReportCard['StudentNameDisplay'] ?>
					</td>
					<td width="75%" class='tabletext'>
						<input type="checkbox" name="StudentNameDisplay[]" id="StudentNameDisplay" value="EnglishName" CHECKED><?=$eReportCard['EnglishName']?></input>
						<br />
						<input type="checkbox" name="StudentNameDisplay[]" id="StudentNameDisplay" value="ChineseName" CHECKED><?=$eReportCard['ChineseName']?></input>
					</td>
				</tr>	
				
				<!-- Ranking Display -->
				<tr id="ClassRankingRow" style="<?= $ClassRankingRowDisplay ?>">
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?= $eReportCard['RankingDisplay'] ?>
					</td>
					<td width="75%" class='tabletext'>
						<input type="checkbox" name="RankingDisplay[]" value="ClassRanking" CHECKED><?=$eReportCard['ClassRanking']?></input>
						<br />
						<input type="checkbox" name="RankingDisplay[]" value="OverallRanking" CHECKED><?=$eReportCard['OverallRanking']?></input>
					</td>
				</tr>	
				
				<!-- Show User Login -->
				<tr id="ShowUserLoginRow" style="<?= $ShowUserLoginRowDisplay ?>">
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?= $eReportCard['ShowUserLogin'] ?>
					</td>
					<td width="75%" class='tabletext'>
						<input type="radio" name="ShowUserLogin" value="1" CHECKED>
							<label for="ShowUserLogin">
								<?=$i_general_yes?>
							</label>
						</input>
						<input type="radio" name="ShowUserLogin" value="0">
							<label for="ShowUserLogin">
								<?=$i_general_no?>
							</label>
						</input>
					</td>
				</tr>
				
				<!-- Show Gender -->
				<tr id="ShowGenderRow" style="<?= $ShowGenderRowDisplay ?>">
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?= $eReportCard['ShowGender'] ?>
					</td>
					<td width="75%" class='tabletext'>
						<input type="radio" name="ShowGender" value="1" CHECKED>
							<label for="ShowGender">
								<?=$i_general_yes?>
							</label>
						</input>
						<input type="radio" name="ShowGender" value="0">
							<label for="ShowGender">
								<?=$i_general_no?>
							</label>
						</input>
					</td>
				</tr>
				
				<!-- Show Subject -->
				<tr id="ShowSubjectRow" style="<?= $ShowSubjectRowDisplay ?>">
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?= $eReportCard['ShowSubject'] ?>
					</td>
					<td width="75%" class='tabletext'>
						<input type="radio" name="ShowSubject" value="1" CHECKED>
							<label for="ShowSubject">
								<?=$i_general_yes?>
							</label>
						</input>
						<input type="radio" name="ShowSubject" value="0">
							<label for="ShowSubject">
								<?=$i_general_no?>
							</label>
						</input>
					</td>
				</tr>
				
				<!-- Show Subject Component -->
				<tr id="ShowSubjectComponentRow" style="<?= $ShowSubjectComponentRowDisplay ?>">
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?= $eReportCard['ShowSubjectComponent'] ?>
					</td>
					<td width="75%" class='tabletext'>
						<input type="radio" name="ShowSubjectComponent" value="1" CHECKED>
							<label for="ShowSubjectComponent">
								<?=$i_general_yes?>
							</label>
						</input>
						<input type="radio" name="ShowSubjectComponent" value="0">
							<label for="ShowSubjectComponent">
								<?=$i_general_no?>
							</label>
						</input>
					</td>
				</tr>
				
				<!-- Show Summary Info -->
				<tr id="ShowSummaryInfoRow" style="<?= $ShowSummaryInfoRowDisplay ?>">
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?= $eReportCard['ShowSummaryInfo'] ?>
					</td>
					<td width="75%" class='tabletext'>
						<input type="radio" name="ShowSummaryInfo" value="1" CHECKED>
							<label for="ShowSummaryInfo">
								<?=$i_general_yes?>
							</label>
						</input>
						<input type="radio" name="ShowSummaryInfo" value="0">
							<label for="ShowSummaryInfo">
								<?=$i_general_no?>
							</label>
						</input>
					</td>
				</tr>	
								
				<!-- Show Grand Total -->
				<tr id="ShowGrandTotalRow" style="<?= $ShowGrandTotalRowDisplay ?>">
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?= $eReportCard['ShowGrandTotal'] ?>
					</td>
					<td width="75%" class='tabletext'>
						<input type="radio" name="ShowGrandTotal" value="1" CHECKED>
							<label for="ShowGrandTotal">
								<?=$i_general_yes?>
							</label>
						</input>
						<input type="radio" name="ShowGrandTotal" value="0">
							<label for="ShowGrandTotal">
								<?=$i_general_no?>
							</label>
						</input>
					</td>
				</tr>
				
				<!-- Show Grand Total Grade-->
				<tr id="ShowGrandTotalGradeRow" style="<?= $ShowGrandTotalRowDisplay ?>">
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?= $eReportCard['ShowGrandTotal'].' '.$eReportCard['Grade'] ?>
					</td>
					<td width="75%" class='tabletext'>
						<input type="radio" name="ShowGrandTotalGrade" value="1" CHECKED>
							<label for="ShowGrandTotalGrade">
								<?=$i_general_yes?>
							</label>
						</input>
						<input type="radio" name="ShowGrandTotalGrade" value="0">
							<label for="ShowGrandTotalGrade">
								<?=$i_general_no?>
							</label>
						</input>
					</td>
				</tr>
				
				<!-- Show Grand Average Mark -->
				<tr id="ShowGrandAverageRow" style="<?= $ShowGrandAverageRowDisplay ?>">
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?= $eReportCard['ShowGrandAverage'] ?>
					</td>
					<td width="75%" class='tabletext'>
						<input type="radio" name="ShowGrandAverage" value="1" CHECKED>
							<label for="ShowGrandAverage">
								<?=$i_general_yes?>
							</label>
						</input>
						<input type="radio" name="ShowGrandAverage" value="0">
							<label for="ShowGrandAverage">
								<?=$i_general_no?>
							</label>
						</input>
					</td>
				</tr>	
				
				<!-- Show Grand Average Grade -->
				<tr id="ShowGrandAverageGradeRow" style="<?= $ShowGrandAverageRowDisplay ?>">
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?= $eReportCard['ShowGrandAverage'].' '.$eReportCard['Grade'] ?>
					</td>
					<td width="75%" class='tabletext'>
						<input type="radio" name="ShowGrandAverageGrade" value="1" CHECKED>
							<label for="ShowGrandAverageGrade">
								<?=$i_general_yes?>
							</label>
						</input>
						<input type="radio" name="ShowGrandAverageGrade" value="0">
							<label for="ShowGrandAverageGrade">
								<?=$i_general_no?>
							</label>
						</input>
					</td>
				</tr>
				
				<? if ($eRCTemplateSetting['Calculation']['ActualAverage']) { ?>
					<!-- Show Actual Average Mark -->
					<tr id="ShowActualAverageRow" style="<?= $ShowActualAverageRowDisplay ?>">
						<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
							<?= $eReportCard['ShowActualAverage'] ?> <span class="tabletextremark">(<?=$eReportCard['ForOverallColumnOnly']?>)</span>
						</td>
						<td width="75%" class='tabletext'>
							<input type="radio" name="ShowActualAverage" value="1" CHECKED>
								<label for="ShowActualAverage">
									<?=$i_general_yes?>
								</label>
							</input>
							<input type="radio" name="ShowActualAverage" value="0">
								<label for="ShowActualAverage">
									<?=$i_general_no?>
								</label>
							</input>
						</td>
					</tr>	
					
					<!-- Show Actual Average Grade -->
					<tr id="ShowActualAverageGradeRow" style="<?= $ShowActualAverageRowDisplay ?>">
						<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
							<?= $eReportCard['ShowActualAverage'].' '.$eReportCard['Grade'] ?> <span class="tabletextremark">(<?=$eReportCard['ForOverallColumnOnly']?>)</span>
						</td>
						<td width="75%" class='tabletext'>
							<input type="radio" name="ShowActualAverageGrade" value="1" CHECKED>
								<label for="ShowActualAverageGrade">
									<?=$i_general_yes?>
								</label>
							</input>
							<input type="radio" name="ShowActualAverageGrade" value="0">
								<label for="ShowActualAverageGrade">
									<?=$i_general_no?>
								</label>
							</input>
						</td>
					</tr>
				<? } ?>
				
				<!-- Show GPA -->
				<tr id="ShowGPARow" style="<?= $ShowGPARowDisplay ?>">
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?= $eReportCard['ShowGPA'] ?>
					</td>
					<td width="75%" class='tabletext'>
						<input type="radio" name="ShowGPA" value="1" CHECKED>
							<label for="ShowGPA">
								<?=$i_general_yes?>
							</label>
						</input>
						<input type="radio" name="ShowGPA" value="0">
							<label for="ShowGPA">
								<?=$i_general_no?>
							</label>
						</input>
					</td>
				</tr>
				
				<!-- Show GPA Grade-->
				<tr id="ShowGPAGradeRow" style="<?= $ShowGPARowDisplay ?>">
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?= $eReportCard['ShowGPA'].' '.$eReportCard['Grade'] ?>
					</td>
					<td width="75%" class='tabletext'>
						<input type="radio" name="ShowGPAGrade" value="1" CHECKED>
							<label for="ShowGPAGrade">
								<?=$i_general_yes?>
							</label>
						</input>
						<input type="radio" name="ShowGPAGrade" value="0">
							<label for="ShowGPAGrade">
								<?=$i_general_no?>
							</label>
						</input>
					</td>
				</tr>
				
				<?
				# Escola Tong Nam and Munsang Primary do not support show Statistics
				if ($ReportCardCustomSchoolName!="escola_tong_nam" && $ReportCardCustomSchoolName!="munsang_pri") {
				?>
				<!-- Show Statistics -->
				<tr id="ShowStatisticsRow" style="<?= $ShowStatisticsRowDisplay ?>">
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?= $eReportCard['ShowStatistics'] ?>
					</td>
					<td width="75%" class='tabletext'>
						<input type="radio" name="ShowStatistics" value="1" CHECKED>
							<label for="ShowStatistics">
								<?=$i_general_yes?>
							</label>
						</input>
						<input type="radio" name="ShowStatistics" value="0">
							<label for="ShowStatistics">
								<?=$i_general_no?>
							</label>
						</input>
					</td>
				</tr>
				<? } ?>
				
				<? if ($eRCTemplateSetting['Report']['GrandMarksheet']['ShowTotalUnitFailed']) { ?>
				<!-- Show Total Unit(s) Failed -->
				<tr id="ShowTotalUnitFailedRow">
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?= $eReportCard['ShowTotalUnitFailed'] ?> <span class="tabletextremark">(<?=$eReportCard['ForOverallColumnOnly']?>)</span>
					</td>
					<td width="75%" class='tabletext'>
						<input type="radio" name="ShowTotalUnitFailed" value="1" CHECKED>
    						<label for="ShowTotalUnitFailed">
    							<?=$i_general_yes?>
    						</label>
						</input>
						<input type="radio" name="ShowTotalUnitFailed" value="0">
							<label for="ShowTotalUnitFailed">
								<?=$i_general_no?>
							</label>
						</input>
					</td>
				</tr>
				<? } ?>
				
				<!-- For SSPA -->
				<tr id="ShowForSSPARow" style="<?= $ShowForSSPARowDisplay ?>">
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?= $eReportCard['For_SSPA'] ?>
					</td>
					<td width="75%" class='tabletext'>
						<input type="radio" name="ForSSPA" value="1">
							<label for="ForSSPA">
								<?=$i_general_yes?>
							</label>
						</input>
						<input type="radio" name="ForSSPA" value="0" CHECKED>
							<label for="ForSSPA">
								<?=$i_general_no?>
							</label>
						</input>
					</td>
				</tr>
				
				<tr>
					<td colspan="2" class="dotline">
						<img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1">
					</td>
				</tr>
				<tr>
					<td colspan="2" align="center">
						<?= $submitBtn ?>
					</td>
				</tr>
			<?php } ?>
			</table>
		</td>
	</tr>
</table>
</form>
<?
	print $linterface->FOCUS_ON_LOAD("form1.ClassLevelID");
  	$linterface->LAYOUT_STOP();
    }
    else
    {
?>
You have no priviledge to access this page.
<?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>