<?
/****************************************************************************************************************
 *  Remarks:
 * 		consolidate report involving both source and target term must be created before generate the report.
 * *************************************************************************************************************/
 
/*********************************************************
 *  modification log
 * 	20100708 marcus:
 * 		- add coding to skip ClassLevelID which has been processed in previous report to avoid duplicated record.
 * 
 * *******************************************************/
 
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

intranet_auth();
intranet_opendb();

$ClassLevelIDArr = $ClassLvID;
$ExcludedConductAry = $ConductMark?$ConductMark:array();
$transferToAward = IntegerSafe(trim($_POST['transferToAward']));

if ($plugin['ReportCard2008']) {
	include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
	
	if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
	{
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
		$lreportcard = new libreportcardcustom();
	} else {
		$lreportcard = new libreportcard();
	}
	
	if ($lreportcard->hasAccessRight()) 
	{
		
		$obj_AcademicYear = new academic_year($lreportcard->schoolYearID);
		
		#build title
		$ActiveYear = $obj_AcademicYear->Get_Academic_Year_Name();
		$ReportType = $eReportCard['Reports_ConductProgressReport'];
		$ReportTitle = $ActiveYear." ".$ReportType;
		
		#build YearTermArr to get TermName by TermID
		$YearTermArr = $obj_AcademicYear->Get_Term_List(0);
		
		#build ary : $YearTermName[$TermID]=$TermName
		foreach($YearTermArr as $data)
			$YearTermName[$data[0]]=$data[1];
			
		# Retrieve Conduct Ary
		$ConductAry = $lreportcard->ConductAry;
		
		$ReportList = $lreportcard->Get_Report_List();
		$ConductProgressList = array();
		$GradeImprovement = array();
		
		
		#loop each Report
		$studentFormAssoAry = array();
		foreach($ReportList as $Report)
		{
			
			//skip ClassLevelID which are not chosen by the user
			if(!in_array($Report["ClassLevelID"],(array)$ClassLevelIDArr)) continue;

			//skip ClassLevelID which has been processed in previous report
			if(in_array($Report["ClassLevelID"],(array)$ProcessedClassLevel)) continue;
			
			//debug_pr('ReportID = '.$Report["ReportID"]);

			# Check whether report has selected term
			//2013-0415-1143-53140
			//$InvolvedSem = (array)$lreportcard->returnReportInvolvedSem($Report["ReportID"]);
			//if(!in_array($FromTerm,array_keys($InvolvedSem)) || !in_array($ToTerm,array_keys($InvolvedSem))) continue;
			
			#Retrieve ClassList
			$ClassList = $lreportcard->GET_CLASSES_BY_FORM($Report["ClassLevelID"]);
				
			#loop each Class
			foreach($ClassList as $Class)
			{
				//2013-0415-1143-53140
				//$OtherInfoAry=$lreportcard->getReportOtherInfoData($Report["ReportID"], "", $Class['ClassID']);
				$FromOtherInfoAry = $lreportcard->getOtherInfoData('summary', $FromTerm, $Class['ClassID'], $Report["ClassLevelID"]);
				$ToOtherInfoAry = $lreportcard->getOtherInfoData('summary', $ToTerm, $Class['ClassID'], $Report["ClassLevelID"]);
				$OtherInfoAry = array();
				foreach ((array)$FromOtherInfoAry as $_studentId => $_studentOtherInfoAry) {
					$OtherInfoAry[$_studentId][$FromTerm] = $_studentOtherInfoAry;
				}
				foreach ((array)$ToOtherInfoAry as $_studentId => $_studentOtherInfoAry) {
					$OtherInfoAry[$_studentId][$ToTerm] = $_studentOtherInfoAry;
				}
				#$OtherInfoAry[$StudentID][$YearTermID][$UploadType] = Value
				

				#loop each student	
				foreach($OtherInfoAry as $StudentID => $TermAry)
				{	
					if(empty($TermAry)) continue;
					#loop each Term
					foreach($TermAry as $TermID => $UploadType)
					{
						$StudentConduct[$StudentID][$TermID]=$UploadType['Conduct'];
					}
					
					$FromTermConduct = $StudentConduct[$StudentID][$FromTerm];
					$ToTermConduct = $StudentConduct[$StudentID][$ToTerm];
					
					# Skip those excluded Conduct Mark  
					if(in_array($FromTermConduct,(array)$ExcludedConductAry)) continue;
					
					# B94975: skip student who has no conduct 
					if ($ToTermConduct == '' || $FromTermConduct == '') {
						continue;
					}
					
					# count grade improvement
					$reversedAry=array_reverse($ConductAry); //e.g. "7"=>"EX" , "6"=>"VG", "5"=>"GD" ... etc
					$GradeImprovement[$StudentID] = array_search($ToTermConduct,$reversedAry) - array_search($FromTermConduct,$reversedAry) ;
					
					
					
					if ($GradeImprovement[$StudentID] >= $requiredGradeImprovement) {
						$ConductProgressList[]=$StudentID;
						$studentFormAssoAry[$StudentID] = $Report["ClassLevelID"];
					}	
				}
								
			}
			$ProcessedClassLevel[] = $Report["ClassLevelID"];
		}
		
		
		if ($transferToAward) {
			include_once($PATH_WRT_ROOT."includes/libreportcard2008_award.php");
			$laward = new libreportcard_award();
			
			// get award info
			$awardInfoAry = $laward->Get_Award_Info_By_Code('conduct_progress_award');
			$awardId = $awardInfoAry['AwardID'];
			
			// transfer award list to last selected term report => get last selected term report data
			$reportIdAssoAry = array();
			$numOfForm = count((array)$ClassLevelIDArr);
			for ($i=0; $i<$numOfForm; $i++) {
				$_classLevelId = $ClassLevelIDArr[$i];
				
				$_lastTermReportInfoAry = $lreportcard->returnReportTemplateBasicInfo('', '', $_classLevelId, $ToTerm);
				$reportIdAssoAry[$_classLevelId] = $_lastTermReportInfoAry['ReportID'];
			}
			
			
			$numOfAwardStudent = count($ConductProgressList);
			$consolidatedAwardStudentAry = array();
			for ($i=0; $i<$numOfAwardStudent; $i++) {
				$_studentId = $ConductProgressList[$i];
				$_classLevelId = $studentFormAssoAry[$_studentId];
				$_reportId = $reportIdAssoAry[$_classLevelId];
				
				// 0 means overall award, not subject award
				$consolidatedAwardStudentAry[$_reportId][$awardId][0][$_studentId]['DetermineValue'] = $GradeImprovement[$_studentId];
				$consolidatedAwardStudentAry[$_reportId][$awardId][0][$_studentId]['RankField'] = 'GradeImprovement';
			}
					
			$involovedReportIdAry = array_values($reportIdAssoAry);
			$successAry['deleteOldAwardResult'] = $laward->Delete_Award_Generated_Student_Record($involovedReportIdAry, $RecordIDArr='', $awardId);
			foreach ((array)$consolidatedAwardStudentAry as $_reportId => $_reportAwardAry) {
				foreach ((array)$_reportAwardAry as $__awardId => $__awardStudentAry) {
					$__orderedAwardStudentAry = $laward->Sort_And_Add_AwardRank_In_InfoArr($__awardStudentAry, 'DetermineValue', 'desc');
					
					$__dbUpdateInfoAry = array();
					foreach ((array)$__orderedAwardStudentAry as $___subjectId => $___subjectStudentAry) {
						foreach ((array)$___subjectStudentAry as $____studentId => $____subjectStudentAwardAry) {
							$____tmpAry = array();
							$____tmpAry = $____subjectStudentAwardAry;
							$____tmpAry['SubjectID'] = $___subjectId;
							$____tmpAry['StudentID'] = $____studentId;
							$__dbUpdateInfoAry[] = $____tmpAry;
							
							unset($____tmpAry);
						}
					}
					
					if (count((array)$__dbUpdateInfoAry) > 0) {
						$successAry['updateAwardGeneratedStudentRecord'][$awardId] = $laward->Update_Award_Generated_Student_Record($awardId, $_reportId, $__dbUpdateInfoAry);
					}
					unset($__dbUpdateInfoAry);
				}
			}
		}
		

		## Display the result
		if ($ViewFormat == 'html')
		{
			$table = '';
			#Table Start
			$table .= "<table id='ResultTable' class='GrandMSContentTable' width='100%' border='1' cellpadding='2' cellspacing='0'>\n";
				//$table .= "<h1 align='center' style='text-align:center'>".$ReportTitle."</h1>";
	
				#THEAD
				$table .= "<thead>\n";
					$table .= "<tr>\n";
						$table .= "<th>#</th>\n";
						$table .= "<th>".$eReportCard['FormName']."</th>\n";
						$table .= "<th>".$eReportCard['Class']."</th>\n";
						$table .= "<th>".$eReportCard['StudentNo_short']."</th>\n";
						$table .= "<th>".$eReportCard['StudentNameEn']."</th>\n";
						$table .= "<th>".$eReportCard['StudentNameCh']."</th>\n";
						$table .= "<th>".$YearTermName[$FromTerm]."</th>\n";
						$table .= "<th>".$YearTermName[$ToTerm]."</th>\n";
						$table .= "<th>".$eReportCard['GradeImprovement']."</th>\n";
					$table .= "</tr>\n";
				$table .= "</thead>\n";
				#END THEAD
				
				#TBODY
				$table .= "<tbody>\n";
					#loop Conduct Award Student List 
					foreach($ConductProgressList as $key=>$StudentID)
					{
						$lu = new libuser($StudentID);
						$ClassLevelInfo = $lreportcard->Get_Student_Class_ClassLevel_Info($StudentID);
						$ClassNameField = ($intranet_session_language=="en")?"ClassName":"ClassNameCh";
	
						$table .= "<tr>\n";
							$table .= "<td align='center'>".($key+1)."</td>\n";
							$table .= "<td align='center'>".$ClassLevelInfo[0]['ClassLevelName']."</td>\n";
							$table .= "<td align='center'>".$ClassLevelInfo[0][$ClassNameField]."</td>\n";
							$table .= "<td align='center'>".$ClassLevelInfo[0]['ClassNumber']."</td>\n";
							$table .= "<td align='center'>".$lu->EnglishName."</td>\n";
							$table .= "<td align='center'>".$lu->ChineseName."</td>\n";
							$table .= "<td align='center'>".$StudentConduct[$StudentID][$FromTerm]."</td>\n";
							$table .= "<td align='center'>".$StudentConduct[$StudentID][$ToTerm]."</td>\n";
							$table .= "<td align='center'>".$GradeImprovement[$StudentID]."</td>\n";
						$table .= "</tr>\n"; 
					} # end loop Student List
				$table .= "</tbody>\n";
				#END TBODY
				
			$table .= "</table>\n"; 
			#END TABLE
				
			$css = $lreportcard->Get_GrandMS_CSS();
			
			$allTable = '<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="center"><h1>'.$ReportTitle.'</h1></td>
							</tr>
							<tr>
								<td>'.$table.'</td>
							</tr>
						</table>';
			
			echo $lreportcard->Get_Report_Header($ReportTitle);
			echo $allTable.$css;
			echo $lreportcard->Get_Report_Footer();
				
		}
		else if ($ViewFormat == 'csv')
		{
			include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
			include_once($PATH_WRT_ROOT."includes/libexporttext.php");
			
			$ExportHeaderArr = array();
			$ExportContentArr = array();
			$lexport = new libexporttext();
			
			
			# Header
			if ($eRCTemplateSetting['Report']['ConductProgress']['ExportUserLogin']) {
				$ExportHeaderArr[] = $Lang['General']['UserLogin'];
			}
			$ExportHeaderArr[] = $eReportCard['FormName'];
			$ExportHeaderArr[] = $eReportCard['Class'];
			$ExportHeaderArr[] = $eReportCard['StudentNo_short'];
			$ExportHeaderArr[] = $eReportCard['StudentNameEn'];
			$ExportHeaderArr[] = $eReportCard['StudentNameCh'];
			$ExportHeaderArr[] = $YearTermName[$FromTerm];
			$ExportHeaderArr[] = $YearTermName[$ToTerm];
			$ExportHeaderArr[] = $eReportCard['GradeImprovement'];
			
			
			#Body
			foreach($ConductProgressList as $key=>$StudentID)
			{
				$row=$key;
				$col=0;
				$lu = new libuser($StudentID);
				$ClassLevelInfo = $lreportcard->Get_Student_Class_ClassLevel_Info($StudentID);
				$ClassNameField = ($intranet_session_language=="en")? "ClassName" : "ClassNameCh";
				
				if ($eRCTemplateSetting['Report']['ConductProgress']['ExportUserLogin']) {
					$ExportContentArr[$row][$col++] = $lu->UserLogin;
				}
				$ExportContentArr[$row][$col++] = $ClassLevelInfo[0]['ClassLevelName'];
				$ExportContentArr[$row][$col++] = $ClassLevelInfo[0][$ClassNameField];
				$ExportContentArr[$row][$col++] = $ClassLevelInfo[0]['ClassNumber'];
				$ExportContentArr[$row][$col++] = $lu->EnglishName;
				$ExportContentArr[$row][$col++] = $lu->ChineseName;
				$ExportContentArr[$row][$col++] = $StudentConduct[$StudentID][$FromTerm];
				$ExportContentArr[$row][$col++] = $StudentConduct[$StudentID][$ToTerm];
				$ExportContentArr[$row][$col++] = $GradeImprovement[$StudentID];
			}
			
			$ReportTitle = str_replace(" ", "_", $ReportTitle);
			$filename = $ReportTitle.".csv";
			
			$export_content = $lexport->GET_EXPORT_TXT($ExportContentArr, $ExportHeaderArr);
			intranet_closedb();
			
			// Output the file to user browser
			$lexport->EXPORT_FILE($filename, $export_content);
		}
	}
}


?>
