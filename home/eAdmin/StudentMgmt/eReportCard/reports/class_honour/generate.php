<?php

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry.php");

intranet_auth();
intranet_opendb();


$HonourAry=$Honour;
$ClassLevelIDArr = $ClassLvID;
$transferToAward = IntegerSafe(trim($_POST['transferToAward']));

$Terms = ($ViewReport == 0)? $Terms_Dropdown : $Terms_Chkbox;
$TargetTerms = $Terms;

if ($plugin['ReportCard2008']) {
	include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
	
	if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
	{
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
		$lreportcard = new libreportcardcustom();
	} else {
		$lreportcard = new libreportcard();
	}
	
	if ($lreportcard->hasAccessRight()) 
	{
		$lsr = new libstudentregistry();
		
		$numOfClassLevelID = count($ClassLevelIDArr);
		$numOfHonour = count($eReportCard['Honour']);
		
		$ConductAry = $lreportcard->ConductAry;
		$numOfConduct = 0;
		if (is_array($ConductAry))
			$numOfConduct = count($ConductAry);
		
		$maxNumOfExamedSubject = 0;
		
		# Report Title
		$ObjAcademicYear = new academic_year($lreportcard->schoolYearID);
		$ActiveYear = $ObjAcademicYear->Get_Academic_Year_Name();
		if($ViewReport == 0)
		{
			$TermID=$Terms[0];
			$SemTitle = $lreportcard->returnSemesters($TermID);
			$ReportType = $eReportCard['Reports_ClassHonourReport'];
			$ReportTitle = $ActiveYear." ".$SemTitle." ".$ReportType;
		}	
		else
		{
			$ReportType = $eReportCard['Reports_ScholarshipReport'];
			$ReportTitle = $ActiveYear." ".$ReportType;
		}
		
		$TermsInfoArr= Array();
		$ScholarshipList = Array();
		$NotScholarshipList = Array();
		$ClassLevelMarkArr = Array();
		$involovedReportIdAry = array();
		
		# control display of OtherInfo 
		$SummaryInfoFields = $lreportcard->OtherInfoInHonour?$lreportcard->OtherInfoInHonour:array("Conduct");
		
		foreach((array)$Terms as $TermID)
		{
			## Get student list and result info for each form
			$allStudentIDArr = array();
			$allStudentInfoArr = array();
			for ($i=0; $i<$numOfClassLevelID; $i++)
			{
				$thisClassLevelID = $ClassLevelIDArr[$i];
				$thisClassLevelName = $lreportcard->returnClassLevel($thisClassLevelID);
				$thisReportInfoArr = $lreportcard->GET_REPORT_TYPES($thisClassLevelID, $ReportID="", $TermID);
				
				$thisReportID = $thisReportInfoArr[0]['ReportID'];
				$involovedReportIdAry[] = $thisReportID;
				
				$thisSemID = $thisReportInfoArr[0]['Semester'];
				$ReportType = ($thisSemID == "F") ? "W" : "T";
				
				
				$thisStudentIDArr = $lreportcard->GET_STUDENT_BY_CLASSLEVEL($thisReportID, $thisClassLevelID);
				
				# Get All GPA Info
				$thisGPA_Arr = $lreportcard->getReportResultScore($thisReportID, 0);
				
				# Get All Conduct Info
				$thisTargetTerm = ($ReportType == "W")? 0 : $thisSemID;
				
				$thisClassesArr = $lreportcard->GET_CLASSES_BY_FORM($thisClassLevelID);
				$thisNumOfClass = count($thisClassesArr);
				
				$OtherInfoAry = array();
				$csvType = $lreportcard->getOtherInfoType();
				for ($j=0; $j<$thisNumOfClass; $j++)
				{
					$thisClassName = $thisClassesArr[$j]['ClassName'];
					$thisClassID = $thisClassesArr[$j]['ClassID'];
					
					foreach((array)$csvType as $k=>$Type)
					{
						$csvData = $lreportcard->getOtherInfoData($Type, $thisTargetTerm, $thisClassID);
						if(!empty($csvData)) 
						{
							foreach((array)$csvData as $thisStudentID=>$data)
							{
								foreach((array)$data as $key=>$val)
								{
									$OtherInfoAry[$key][$thisStudentID] = $val;
				 				}
							}
						}
					}
				}
				
				# Get All Marks Info
				// $MarkArr[$StudentID][$SubjectID][$ReportColumnID][Mark, Grade...] = $val
				$MarkArr = $lreportcard->getMarks($thisReportID, '', $cons=' AND a.ReportColumnID=0', 0);
				$ClassLevelMarkArr[$thisClassLevelID] = $MarkArr;
				
				## Consolidate individual student info
				$numOfStudent = count($thisStudentIDArr);
				for ($j=0; $j<$numOfStudent; $j++)
				{
					$thisStudentID = $thisStudentIDArr[$j][0];
					$allStudentIDArr[] = $thisStudentID;
					
					# Form Class Info
					$lu = new libuser($thisStudentID);
					//$thisClassName = $lu->ClassName;
					$thisWebSamsRegNo = $lu->WebSamsRegNo;
					
					# Form Name
					$allStudentInfoArr[$thisStudentID]['ClassLevelName'] = $thisClassLevelName;
					$allStudentInfoArr[$thisStudentID]['ReportID'] = $thisReportID;
					
					# Class Name
					//$allStudentInfoArr[$thisStudentID]['ClassName'] = $thisClassName;
					
					# GPA
					$thisGPA = $thisGPA_Arr[$thisStudentID][0]['GPA'];
					$allStudentInfoArr[$thisStudentID]['GPA'] = $thisGPA;
					
					# OtherInfo
					foreach((array)$SummaryInfoFields as $key)
					{
						$thisOtherInfo =  $OtherInfoAry[$key][$thisStudentID];
						$allStudentInfoArr[$thisStudentID][$key] = $thisOtherInfo;
					}
										
					# All Pass 
					$thisMarkArr = $MarkArr[$thisStudentID];
					$thisMarkNatureArr = $lreportcard->Get_Subject_Mark_Nature_Arr($thisReportID, $thisClassLevelID, $thisMarkArr);
					$AllPass = true;
					foreach($thisMarkNatureArr as $thisSubjectID => $thisSubjectNature)
					{
						if($thisSubjectNature=="Fail" && !$lreportcard->CHECK_SUBJECT_IS_COMPONENT_SUBJECT($thisSubjectID)) 
						{
							$AllPass = false;
							break;
						}							
					}
					$allStudentInfoArr[$thisStudentID]['AllPass'] = $AllPass ? 1 : 0;
					
					# Subjects
					$thisExamedSubjectArr = $lreportcard->Get_Examed_Subject_Arr($thisReportID, $thisMarkArr);
					$allStudentInfoArr[$thisStudentID]['ExamedSubjectArr'] = $thisExamedSubjectArr;
				}
			}	// End loop class level

			## Select student according to the conditions selected
			// exclude student in the 1st honour when calculating the student of 2nd honour
			$honouredStudentIDArr = array();
			$honouredStudentInfoArr = array();
			$numOfAllStudent = count($allStudentIDArr);

			for ($i=1; $i<=$numOfHonour; $i++)
			{
				$thisHonour = $i;
				$thisGPA_LowerLimit = ${"GPA_".$i};
				$thisConductArr = ${"ConductMark_".$i};
				$thisNeedAllPass = ${"AllPass_".$i};
				
				for ($j=0; $j<$numOfAllStudent; $j++)
				{
					$thisStudentID = $allStudentIDArr[$j];
					$thisStudentInfoArr = $allStudentInfoArr[$thisStudentID];
					
					# Check if the student is in the higher honour already
					if (in_array($thisStudentID, $honouredStudentIDArr))
						continue;
					
					# Check GPA
					$thisGPA = $thisStudentInfoArr['GPA'];
					if ($thisGPA < $thisGPA_LowerLimit)
						continue;
					
					if ($numOfConduct > 0  && count($thisConductArr)>0)
					{
						# Check Conduct
						$thisConduct = $thisStudentInfoArr['Conduct'];
						if (!in_array($thisConduct, $thisConductArr))
							continue;
					}
					
					# Check all pass 
					$thisAllPass = $thisStudentInfoArr['AllPass'];
					if ($thisNeedAllPass && !$thisAllPass)
						continue;
						
					# Check Reading pass 
					if (is_array($thisStudentInfoArr['Reading'])) {
						$thisReadingGrade = $thisStudentInfoArr['Reading'][0];
						$thisStudentInfoArr['Reading'] = $thisReadingGrade;
					}
					else {
						$thisReadingGrade = $thisStudentInfoArr['Reading'];
					}
					if ($thisNeedAllPass && $thisReadingGrade=='UN')
						continue;
						
					## Student is in this honour
					$honouredStudentIDArr[] = $thisStudentID;
					
					$thisStudentInfoArr['Honour'] = $thisHonour;
					$honouredStudentInfoArr[$thisStudentID] = $thisStudentInfoArr;
					
					# Get the maximum number of examed subjects for display
					$thisExamedSubjectArr = $thisStudentInfoArr['ExamedSubjectArr'];
					$thisNumOfExamedSubject = count($thisExamedSubjectArr);
					$maxNumOfExamedSubject = ($thisNumOfExamedSubject > $maxNumOfExamedSubject)? $thisNumOfExamedSubject : $maxNumOfExamedSubject;
				
					$TermHonours[$thisStudentID][$TermID]=$thisHonour;
					
				}#loop each honour
			}#loop each student
		}// end loop each term
	}

	if($ViewReport==1) //for Scholarship Report, build disqualify list 
	{
		foreach((array)$TermHonours as $StudentID => $Term) //loop each student
		{
			foreach ((array)$TargetTerms as $TermID)  //loop each term
			{
				$thisTermHonours = $TermHonours[$StudentID][$TermID];
				
				if(!in_array($thisTermHonours,$TargetHonours)||$thisTermHonours=="") //no honour or honour not in target honour
				{
					if(!in_array($StudentID,$NotScholarshipList)) //student already exist in disqualify list
					{
						$NotScholarshipList[]=$StudentID; //add students who are not qualified into disqualify list
					}
				}
			}
		}		
	}
	
	// take away students in disqualify list from all student
	$ScholarshipList = array_values(array_diff($honouredStudentIDArr, $NotScholarshipList)); 
	
	$obj_AcademicYear = new academic_year($lreportcard->schoolYearID);
	$YearTermArr = $obj_AcademicYear->Get_Term_List(0);

	//build ary : $YearTermName[$TermID]=$TermName
	foreach((array)$YearTermArr as $data)
		$YearTermName[$data[0]]=$data[1];
		
		
	if ($ViewReport == 0 && $transferToAward) {
		include_once($PATH_WRT_ROOT."includes/libreportcard2008_award.php");
		$laward = new libreportcard_award();
		
		// get awards info
		$awardCodeAssoAry = array();
		$awardCodeAssoAry[1]['awardCode'] = 'first_class_honour';
		$awardCodeAssoAry[2]['awardCode'] = 'second_class_honour_1';
		$awardCodeAssoAry[3]['awardCode'] = 'second_class_honour_2';
		
		$awardIdAry = array();
		foreach ((array)$awardCodeAssoAry as $_honourNum => $_awardAry) {
			$_awardCode = $_awardAry['awardCode'];
			
			$_awardInfoAry = $laward->Get_Award_Info_By_Code($_awardCode);
			$_awardId = $_awardInfoAry['AwardID'];
			
			$awardIdAry[] = $_awardId;
			$awardCodeAssoAry[$_honourNum]['awardId'] = $_awardId;
		}
		
		// consolidate awards array by analyzing each student data
		$consolidatedAwardStudentAry = array();
		foreach ((array)$honouredStudentInfoArr as $_studentId => $_studentAwardInfoAry) {
			$__honourNum = $_studentAwardInfoAry['Honour'];
			$__gpa = $_studentAwardInfoAry['GPA'];
			
			$__awardId = $awardCodeAssoAry[$__honourNum]['awardId'];
			$__subjectId = 0;	// overall award, not subject-related => set as 0
			
			$__reportId = $allStudentInfoArr[$_studentId]['ReportID'];
			
			$consolidatedAwardStudentAry[$__reportId][$__awardId][0][$_studentId]['DetermineValue'] = $__gpa;
			$consolidatedAwardStudentAry[$__reportId][$__awardId][0][$_studentId]['RankField'] = 'GPA';
		}
		
		
		$successAry['deleteOldAwardResult'] = $laward->Delete_Award_Generated_Student_Record($involovedReportIdAry, $RecordIDArr='', $awardIdAry);
		foreach ((array)$consolidatedAwardStudentAry as $_reportId => $_reportAwardAry) {
			foreach ((array)$_reportAwardAry as $__awardId => $__awardStudentAry) {
				$__orderedAwardStudentAry = $laward->Sort_And_Add_AwardRank_In_InfoArr($__awardStudentAry, 'DetermineValue', 'desc');
				
				$__dbUpdateInfoAry = array();
				foreach ((array)$__orderedAwardStudentAry as $___subjectId => $___subjectStudentAry) {
					foreach ((array)$___subjectStudentAry as $____studentId => $____subjectStudentAwardAry) {
						$____tmpAry = array();
						$____tmpAry = $____subjectStudentAwardAry;
						$____tmpAry['SubjectID'] = $___subjectId;
						$____tmpAry['StudentID'] = $____studentId;
						$__dbUpdateInfoAry[] = $____tmpAry;
						
						unset($____tmpAry);
					}
				}
				
				if (count((array)$__dbUpdateInfoAry) > 0) {
					$successAry['updateAwardGeneratedStudentRecord'][$__awardId] = $laward->Update_Award_Generated_Student_Record($__awardId, $_reportId, $__dbUpdateInfoAry);
				}
				unset($__dbUpdateInfoAry);
			}
			
			$successAry['Update_LastGeneratedAward_Date'] = $lreportcard->UPDATE_REPORT_LAST_DATE($_reportId, 'LastGeneratedAward');
		}
	}	
	

	## Display the result
	if ($ViewFormat == 'html')
	{
		$table = '';
		$table .= "<table id='ResultTable' class='GrandMSContentTable' width='100%' border='1' cellpadding='2' cellspacing='0'>\n";
			$table .= "<thead>\n";
				$table .= "<tr>\n";
				
					#HonourReport table head
					if($ViewReport==0)
					{
						$table .= "<th>#</th>\n";
						$table .= "<th>".$eReportCard['AwardName']."</th>\n";
						$table .= "<th>".$eReportCard['FormName']."</th>\n";
						$table .= "<th>".$eReportCard['Class']."</th>\n";
						$table .= "<th>".$eReportCard['Template']['StudentInfo']['ClassNo']."</th>\n";
						$table .= "<th>".$eReportCard['StudentNameEn']."</th>\n";
						$table .= "<th>".$eReportCard['StudentNameCh']."</th>\n";
						$table .= "<th>".$eReportCard['GPA']."</th>\n";
						foreach((array)$SummaryInfoFields as $key)
						{
							$table .= "<th>".$eReportCard['Template'][$key]."</th>\n";
						}
						
						# Subjects
						for ($i=0; $i<$maxNumOfExamedSubject; $i++)
						{
							$table .= "<th>".$eReportCard['Subject']." ".($i+1)."</th>\n";
						}
						
						$table .= "<th>".$eReportCard['LastYearSchool']."</th>\n";
					}
					else #Scholarship Report table head
					{
						$table .= "<th>#</th>\n";
						$table .= "<th>".$eReportCard['FormName']."</th>\n";
						$table .= "<th>".$eReportCard['Class']."</th>\n";
						$table .= "<th>".$eReportCard['Template']['StudentInfo']['ClassNo']."</th>\n";
						$table .= "<th>".$eReportCard['StudentNameEn']."</th>\n";
						$table .= "<th>".$eReportCard['StudentNameCh']."</th>\n";
						
						for ($i=0; $i<count($Terms); $i++)
						{
							$table .= "<th>".$YearTermName[$Terms[$i]]."</th>\n";
						}
						
						$table .= "<th>".$eReportCard['LastYearSchool']."</th>\n";
					}
				$table .= "</tr>\n";
			$table .= "</thead>\n";
			
			$table .= "<tbody>\n";
			
				$StudentIDArr=($ViewReport==0)?$honouredStudentIDArr:$ScholarshipList;
				
				if (count($StudentIDArr) > 0) {
					$studentRegAssoAry = $lsr->RETRIEVE_STUDENT_INFO_BASIC_HK('', implode(',', (array)$StudentIDArr));
				}
				
				
				for ($i=0; $i<count($StudentIDArr); $i++)
				{
					$thisStudentID = $StudentIDArr[$i];
					$thisStudentInfoArr = $honouredStudentInfoArr[$thisStudentID];
					
					$thisHonour = $thisStudentInfoArr['Honour'];
					$thisHonourDisplay = $eReportCard['Honour'][$thisHonour];
					
					$thisClassLevelName = $thisStudentInfoArr['ClassLevelName'];
					
					$lu = new libuser($thisStudentID);
					$thisEnglishName = $lu->EnglishName;
					$thisChineseName = $lu->ChineseName;
					
					$thisClassInfoArr = $lreportcard->Get_Student_Class_ClassLevel_Info($thisStudentID);
					$thisClassName = $thisClassInfoArr[0]['ClassName'];
					$thisClassNumber = $thisClassInfoArr[0]['ClassNumber'];
					$thisClassLevelID = $thisClassInfoArr[0]['ClassLevelID'];
					
					$thisGPA = $thisStudentInfoArr['GPA'];
					
					$thisExamedSubjectArr = $thisStudentInfoArr['ExamedSubjectArr'];
					$thisNumOfExamedSubject = count($thisExamedSubjectArr);
					
					$thisPreviousSchoolEn = trim($studentRegAssoAry[$thisStudentID]['LAST_SCHOOL_EN']);
					$thisPreviousSchoolCh = trim($studentRegAssoAry[$thisStudentID]['LAST_SCHOOL_CH']);
					if ($thisPreviousSchoolEn=='' && $thisPreviousSchoolCh=='') {
						$thisPreviousSchool = '&nbsp;';
					}
					else {
						$thisPreviousSchool = $thisPreviousSchoolEn;
						if ($thisPreviousSchoolCh != '') {
							$thisPreviousSchool .= '<br>('.$thisPreviousSchoolCh.')';
						}
					}
					
					$table .= "<tr style='text-align:center'>\n";
					#HonourReport table body
					if($ViewReport==0)
					{
						$table .= "<td>".($i+1)."</td>\n";
						$table .= "<td>".$thisHonourDisplay."</td>\n";
						$table .= "<td>".$thisClassLevelName."</td>\n";
						$table .= "<td>".$thisClassName."</td>\n";
						$table .= "<td>".$thisClassNumber."</td>\n";
						$table .= "<td>".$thisEnglishName."</td>\n";
						$table .= "<td>".$thisChineseName."</td>\n";
						$table .= "<td>".$thisGPA."</td>\n";
						foreach((array)$SummaryInfoFields as $key)
						{
							$table .= "<td>".$thisStudentInfoArr[$key]."</td>\n";
						}
												
						for ($j=0; $j<$thisNumOfExamedSubject; $j++)
						{
							$thisSubjectID = $thisExamedSubjectArr[$j];
							//$thisSubjectName = $lreportcard->GET_SUBJECT_NAME($thisSubjectID, 1);
							$thisSubjectDisplay = $lreportcard->GET_SUBJECT_NAME_LANG($thisSubjectID);
							$thisSubjectGrade = $ClassLevelMarkArr[$thisClassLevelID][$thisStudentID][$thisSubjectID][0]['Grade'];
							
							if ($thisSubjectGrade != '')
								$thisSubjectDisplay .= ' ['.$thisSubjectGrade.']';
							
							$table .= "<td>".$thisSubjectDisplay."</td>\n";
						}
						
						$thisNumOfEmptyCell = $maxNumOfExamedSubject - $thisNumOfExamedSubject;
						for ($j=0; $j<$thisNumOfEmptyCell; $j++)
						{
							$table .= "<td>&nbsp;</td>\n";
						}
						
						$table .= "<td>".$thisPreviousSchool."</td>\n";
					}
					else #Scholarship Report table head
					{
						$table .= "<td>".($i+1)."</td>\n";
						$table .= "<td>".$thisClassLevelName."</td>\n";
						$table .= "<td>".$thisClassName."</td>\n";
						$table .= "<td>".$thisClassNumber."</td>\n";
						$table .= "<td>".$thisEnglishName."</td>\n";
						$table .= "<td>".$thisChineseName."</td>\n";
						for ($j=0; $j<count($Terms); $j++)
						{
							$thisTermHonours = $TermHonours[$thisStudentID][$Terms[$j]];
							$table .= "<td>".$eReportCard['Honour'][$thisTermHonours]."</td>\n";
						}
						
						$table .= "<td>".$thisPreviousSchool."</td>\n";
					}
						
					$table .= "</tr>\n";
				}
			$table .= "</tbody>\n";
		$table .= "</table>\n";
		
		$css = $lreportcard->Get_GrandMS_CSS();
		
		$allTable = '<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td align="center"><h1>'.$ReportTitle.'</h1></td>
						</tr>
						<tr>
							<td>'.$table.'</td>
						</tr>
					</table>';
		
		echo $lreportcard->Get_Report_Header($ReportTitle);
		echo $allTable.$css;
		echo $lreportcard->Get_Report_Footer();
	}
	else if ($ViewFormat == 'csv')
	{
		include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
		include_once($PATH_WRT_ROOT."includes/libexporttext.php");
		
		$ExportHeaderArr = array();
		$ExportContentArr = array();
		$lexport = new libexporttext();

		#HonourReport header
		if($ViewReport==0)
		{
			$ExportHeaderArr[] = $eReportCard['AwardName'];
			if ($eRCTemplateSetting['Report']['Honour']['ExportUserLogin']) {
				$ExportHeaderArr[] = $Lang['General']['UserLogin'];
			}
			$ExportHeaderArr[] = $eReportCard['FormName'];
			$ExportHeaderArr[] = $eReportCard['Class'];
			$ExportHeaderArr[] = $eReportCard['Template']['StudentInfo']['ClassNo'];
			$ExportHeaderArr[] = $eReportCard['StudentNameEn'];
			$ExportHeaderArr[] = $eReportCard['StudentNameCh'];
			$ExportHeaderArr[] = $eReportCard['GPA'];
			foreach((array)$SummaryInfoFields as $key)
			{
				$ExportHeaderArr[] = $eReportCard['Template'][$key];
			}
			
			// Subjects
			for ($i=0; $i<$maxNumOfExamedSubject; $i++)
			{
				$ExportHeaderArr[] = $eReportCard['Subject']." ".($i+1);
			}
			
			$ExportHeaderArr[] = $eReportCard['LastYearSchool'];
		}
		else # Scholarship Report
		{
			if ($eRCTemplateSetting['Report']['Honour']['ExportUserLogin']) {
				$ExportHeaderArr[] = $Lang['General']['UserLogin'];
			}
			$ExportHeaderArr[] = $eReportCard['FormName'];
			$ExportHeaderArr[] = $eReportCard['Class'];
			$ExportHeaderArr[] = $eReportCard['Template']['StudentInfo']['ClassNo'];
			$ExportHeaderArr[] = $eReportCard['StudentNameEn'];
			$ExportHeaderArr[] = $eReportCard['StudentNameCh'];

			for ($i=0; $i<count($Terms); $i++)
			{
				$ExportHeaderArr[] = $YearTermName[$Terms[$i]];
			}
			
			$ExportHeaderArr[] = $eReportCard['LastYearSchool'];
		}
		
		# Content
		$StudentIDArr=($ViewReport==0)?$honouredStudentIDArr:$ScholarshipList;
		if (count($StudentIDArr) > 0) {
			$studentRegAssoAry = $lsr->RETRIEVE_STUDENT_INFO_BASIC_HK('', implode(',', (array)$StudentIDArr));
		}
		
		for ($i=0; $i<count($StudentIDArr); $i++)
		{
			$thisStudentID = $StudentIDArr[$i];
			$thisStudentInfoArr = $honouredStudentInfoArr[$thisStudentID];
			
			$thisHonour = $thisStudentInfoArr['Honour'];
			$thisHonourDisplay = $eReportCard['Honour'][$thisHonour];
			
			$thisClassLevelName = $thisStudentInfoArr['ClassLevelName'];
			//$thisClassName = $thisStudentInfoArr['ClassName'];
			
			$lu = new libuser($thisStudentID);
			//$thisClassNumber = $lu->ClassNumber;
			$thisEnglishName = $lu->EnglishName;
			$thisChineseName = $lu->ChineseName;
			$thisUserLogin = $lu->UserLogin;
			
			$thisClassInfoArr = $lreportcard->Get_Student_Class_ClassLevel_Info($thisStudentID);
			$thisClassName = $thisClassInfoArr[0]['ClassName'];
			$thisClassNumber = $thisClassInfoArr[0]['ClassNumber'];
			$thisClassLevelID = $thisClassInfoArr[0]['ClassLevelID'];
			
			$thisGPA = $thisStudentInfoArr['GPA'];
			
			$thisExamedSubjectArr = $thisStudentInfoArr['ExamedSubjectArr'];
			$thisNumOfExamedSubject = count($thisExamedSubjectArr);
			
			$thisPreviousSchoolEn = trim($studentRegAssoAry[$thisStudentID]['LAST_SCHOOL_EN']);
			$thisPreviousSchoolCh = trim($studentRegAssoAry[$thisStudentID]['LAST_SCHOOL_CH']);
			if ($thisPreviousSchoolEn=='' && $thisPreviousSchoolCh=='') {
				$thisPreviousSchool = '&nbsp;';
			}
			else {
				$thisPreviousSchool = $thisPreviousSchoolEn;
				if ($thisPreviousSchoolCh != '') {
					$thisPreviousSchool .= ' ('.$thisPreviousSchoolCh.')';
				}
			}
			
			$j_counter = 0;
			if($ViewReport==0)
			{
				
				$ExportContentArr[$i][$j_counter++] = $thisHonourDisplay;
				if ($eRCTemplateSetting['Report']['Honour']['ExportUserLogin']) {
					$ExportContentArr[$i][$j_counter++] = $thisUserLogin;
				}
				$ExportContentArr[$i][$j_counter++] = $thisClassLevelName;
				$ExportContentArr[$i][$j_counter++] = $thisClassName;
				$ExportContentArr[$i][$j_counter++] = $thisClassNumber;
				$ExportContentArr[$i][$j_counter++] = $thisEnglishName;
				$ExportContentArr[$i][$j_counter++] = $thisChineseName;
				$ExportContentArr[$i][$j_counter++] = $thisGPA;
				foreach((array)$SummaryInfoFields as $key)
				{
					$ExportContentArr[$i][$j_counter++] = $thisStudentInfoArr[$key];
				}
				
				for ($j=0; $j<$thisNumOfExamedSubject; $j++)
				{
					$thisSubjectID = $thisExamedSubjectArr[$j];
					//$thisSubjectName = $lreportcard->GET_SUBJECT_NAME($thisSubjectID, 1);
					$thisSubjectDisplay = $lreportcard->GET_SUBJECT_NAME_LANG($thisSubjectID);
					$thisSubjectGrade = $ClassLevelMarkArr[$thisClassLevelID][$thisStudentID][$thisSubjectID][0]['Grade'];
					
					if ($thisSubjectGrade != '')
						$thisSubjectDisplay .= ' ['.$thisSubjectGrade.']';
					
					$ExportContentArr[$i][$j_counter++] = $thisSubjectDisplay;
				}
				
				$ExportContentArr[$i][$j_counter++] = $thisPreviousSchool;
			}
			else
			{
				if ($eRCTemplateSetting['Report']['Honour']['ExportUserLogin']) {
					$ExportContentArr[$i][$j_counter++] = $thisUserLogin;
				}
				$ExportContentArr[$i][$j_counter++] = $thisClassLevelName;
				$ExportContentArr[$i][$j_counter++] = $thisClassName;
				$ExportContentArr[$i][$j_counter++] = $thisClassNumber;
				$ExportContentArr[$i][$j_counter++] = $thisEnglishName;
				$ExportContentArr[$i][$j_counter++] = $thisChineseName;

				for ($j=0; $j<count($Terms); $j++)
				{
					$thisTermHonours = $TermHonours[$thisStudentID][$Terms[$j]];
					$ExportContentArr[$i][$j_counter++] =$eReportCard['Honour'][$thisTermHonours];
				}
				
				$ExportContentArr[$i][$j_counter++] = $thisPreviousSchool;
			}
		}
		
		//debug_pr($ExportContentArr);
		
		// Title of the grandmarksheet
		$ReportTitle = str_replace(" ", "_", $ReportTitle);
		$filename = $ReportTitle.".csv";
		
		$export_content = $lexport->GET_EXPORT_TXT($ExportContentArr, $ExportHeaderArr);
		
		// Output the file to user browser
		$lexport->EXPORT_FILE($filename, $export_content);
	}
	
	intranet_closedb();
}


?>