<?php
# Editing by Bill 

/***************************************************
 * 	Modification log:
 * 
 *  20190503 Bill
 *      - Term Selection > Change 'TermID' to 'TargetTerm' to prevent IntegerSafe()
 * 
 * 	20170519 Bill
 * 		- Allow View Group User to access
 * 	
 * 	20170427 Bill:
 * 		- Support Graduate Exam for S6
 * 
 * 	20160704 Bill:	[2015-1104-1130-08164]
 * 		- Create File 
 * 
 * *************************************************/

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = array("ADMIN", "VIEW_GROUP");

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping_ui.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage_ui.php");

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
	include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");

intranet_auth();
intranet_opendb();

$lreportcard = new libreportcard();
if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") {
	$lreportcard = new libreportcardcustom();
}
$lreportcard_ui = new libreportcard_ui();

if(!$plugin['ReportCard2008'] || !$eRCTemplateSetting['Report']['SubjectStat'] || !$lreportcard->hasAccessRight())
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	intranet_closedb();
	exit;
}

$linterface = new interface_html();
$CurrentPage = "Reports_SubjectStat";

$lclass = new libclass();

# Get Form selection (show form which has report card only)
$libForm = new Year();
$FormArr = $libForm->Get_All_Year_List();
$YearID = $YearID? $YearID : $FormArr[0]['YearID'];

//$FormSelection = $lreportcard_ui->Get_Form_Selection('YearID', $YearID, 'js_Reload();', $noFirst=1);
		
# Get Levels and Classes
$levels = $lclass->getLevelArray();
$classes = $lclass->getClassList($lreportcard->schoolYearID);
$select_list = "<SELECT name=\"TargetID[]\" onChange=\"handleGradExam(this.value);\">";
$is_sec_grad_ary = array();
if ($level==1)
{
    for ($i=0; $i<sizeof($levels); $i++)
    {
	    list($id, $name) = $levels[$i];
		$select_list .= "<OPTION value='".$id."' SELECTED>".$name."</OPTION>";
    }
}
else
{
    for ($i=0; $i<sizeof($classes); $i++)
    {
    	list($id, $name, $lvl) = $classes[$i];
		$sch_type = $lreportcard->GET_SCHOOL_TYPE_BY_CLASSLEVEL($lvl);
		$form_num = $lreportcard->GET_FORM_NUMBER($lvl, 1);
		$is_sec_grad = $sch_type == "S" && $form_num == 6;
		if($is_sec_grad)
			$is_sec_grad_ary[] = $id;
		
		$select_list .= "<OPTION value='".$id."' SELECTED>".$name."</OPTION>";
    }
}
$select_list .= "</SELECT>";

# Term Selection
$TermInfoArr = $lreportcard->returnReportTemplateTerms();
$TermSel_tag = 'name="TargetTerm" id="TargetTerm"';
$TermSelection = getSelectByArray($TermInfoArr, $TermSel_tag, "", $all=0, $noFirst=1);

# Tag information
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
$TAGS_OBJ[] = array($eReportCard['Reports_SubjectStat'], "", 0);

$linterface->LAYOUT_START();
?>

<script>
var SecondGradClassArr = new Array("", "<?= implode('","',(array)$is_sec_grad_ary); ?>");

function submitForm(format)
{
	$('input#format').val(format);
	
	ObjForm = document.getElementById('FormMain');
	if(format == 'csv'){
		ObjForm.action = 'print.php';
		ObjForm.target = '_self';
		ObjForm.submit();
	}
	else {
		ObjForm.action = 'print.php';
		ObjForm.target = '_blank';
		ObjForm.submit();
	}
}

function handleGradExam(val)
{
	var isGradExamClass = $.inArray(val, SecondGradClassArr) > 0;
	var isGradExamOptionExist = $("option#TargetGradExam").length;
	
	// Add Graduate Exam Option
	if(isGradExamClass && !isGradExamOptionExist){
		$('select#TargetTerm').append($("<option id='TargetGradExam'></option>").attr("value", 'Exam').text('<?=$eReportCard["SubjStatReport"]["GradExamOpt"]?>'));
	}
	// Remove Graduate Exam Option
	else if(!isGradExamClass && isGradExamOptionExist){
		$("option#TargetGradExam").remove();
	}
}

</script>

<br/>
<form id="FormMain" name="FormMain" method="post" action="report_print.php">
  	<table width="100%" border="0" cellspacing="0" cellpadding="0">
  	<tr>
      	<td class="tab_underline"><?=$display_tagstype?></td>
    </tr>
  	</table>
	<table class="form_table_v30">
		<!-- Year -->
		<tr valign="top">
			<td class="field_title"><?=$Lang['General']['SchoolYear']?></td>
			<td><?=$lreportcard->GET_ACTIVE_YEAR_NAME()?></td>
		</tr>
		<!-- Term -->
		<tr valign="top">
			<td class="field_title"><span class="tabletextrequire">*</span><?=$eReportCard['Term']?></td>
			<td><?=$TermSelection?></td>
		</tr>
		<!-- Form -->
		<tr valign="top">
			<td class="field_title"><span class="tabletextrequire">*</span><?=$eReportCard['Class'] ?></td>
			<td><?=$select_list?></td>
		</tr>
		
	</table>
	
	<?=$linterface->MandatoryField();?>
	
	<div class="edit_bottom_v30">
		<p class="spacer"></p>
		<?= $linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "button", "submitForm('');")?>
		&nbsp;
		<!--<?= $linterface->GET_ACTION_BTN($Lang['Btn']['Export'], "button", "submitForm('csv');")?>-->
	</div>
		
	<input type="hidden" name="format" id="format" value="pdf">
</form>
<br />
<br />

<?
	$linterface->LAYOUT_STOP();
?>