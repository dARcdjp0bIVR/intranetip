<?php
// Using: Bill

/*
 *	2018-03-07 (Bill)    [2017-1102-1135-27054]
 *	     - Create file
 */

$PageRight = "ADMIN";

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");

$lreportcard = new libreportcardcustom();
$lreportcard->hasAccessRight();

$lreportcard_ui = new libreportcard_ui();
$linterface = new interface_html();
		
############## Interface Start ##############
$CurrentPage = "Reports_AwardCertificate";
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();

# Tag information
$TAGS_OBJ[] = array($eReportCard['Reports_AwardCertificate']);
$linterface->LAYOUT_START();
#############################################

### Get Form selection (show form which has report card only)
$FormSelection = $lreportcard_ui->Get_Form_Selection('YearID', '', 'js_Reload_Selection(this.value)', $noFirst=1);
?>

<link type="text/css" rel="stylesheet" href="<?=$PATH_WRT_ROOT."templates/2007a/css/"?>ereportcard.css">

<script language="javascript">
$(document).ready( function() {
	var jsCurFormID = $('select#YearID').val();
	js_Reload_Selection(jsCurFormID);
});

function SelectAll(obj)
{
    for (i=0; i<obj.length; i++)
    {
		obj.options[i].selected = true;
    }
}

function js_Reload_Selection(jsYearID)
{
	$('#ReportSelectionDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"../ajax_reload_selection.php", 
		{
			RecordType: 'Report',
			YearID: jsYearID,
			ReportID: '',
			ReportType: 'T',
			SelectionID: 'ReportID'
		}
	);
	
	$('#ClassSelectionSpan').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"../ajax_reload_selection.php", 
		{
			RecordType: 'Class',
			YearID: jsYearID,
			SelectionID: 'YearClassIDArr[]',
			isMultiple: 1
		},
		function(ReturnData)
		{
			SelectAll(document.form1.elements['YearClassIDArr[]']);
		}
	);
}

function checkForm() 
{
	var obj = document.form1;
	var select_obj = obj.elements['YearClassIDArr[]'];
	
	var hasSelectedClass = false;
	for (var i=0; i<select_obj.length; i++)
	{
		if (select_obj[i].selected)
		{
			hasSelectedClass = true;
		}
	}
	
	if (hasSelectedClass == false)
	{
		alert('<?=$eReportCard["jsWarningSelectClass"]?>');
		obj.elements['YearClassIDArr[]'].focus();
		
		return false;
	}
	
	return true;
}
</script>

<br />
<form name="form1" action="generate.php" method="POST" onsubmit="return checkForm();" target="_blank">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	</tr>
<table>

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>
			<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr>
					<td></td>
					<td align="right"><?=$linterface->GET_SYS_MSG($Result);?></td>
				</tr>
				
				<!-- Form -->
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?= $eReportCard['FormName'] ?>
					</td>
					<td width="75%" class='tabletext'>
						<?= $FormSelection ?>
					</td>
				</tr>
				
				<!-- Class -->
				<tr class="tabletext">
					<td class="formfieldtitle" valign="top"><?=$eReportCard['Class']?> <span class="tabletextrequire">*</span></td>
					<td>
						<span id="ClassSelectionSpan">
							<?=$ClassSelection?>
						</span>
						<span style="vertical-align:bottom">
							<?= $linterface->GET_SMALL_BTN($button_select_all, "button", "javascript:SelectAll(document.form1.elements['YearClassIDArr[]'])"); ?>
						</span>
					</td>
				</tr>
				
				<!-- Report -->
				<tr class="tabletext">
					<td class="formfieldtitle" valign="top" width="30%"><?=$eReportCard['ReportCard']?> <span class="tabletextrequire">*</span></td>
					<td>
						<div id="ReportSelectionDiv">
							<?= $ReportSelection ?>
						</div>
					</td>
				</tr>
				
				<!-- Issue Date -->
				<tr class="tabletext">
					<td class="formfieldtitle" valign="top" width="30%"><?= $eReportCard['IssueDate'] ?></td>
					<td>
						<div id="ReportSelectionDiv">
							<?= $linterface->GET_DATE_PICKER('issueDate') ?>
						</div>
					</td>
				</tr>
				
				<tr>
					<td colspan="2" class="dotline">
						<img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1">
					</td>
				</tr>
				<tr>
					<td colspan="2" align="center">
						<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "", "submitBtn"); ?>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</form>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>