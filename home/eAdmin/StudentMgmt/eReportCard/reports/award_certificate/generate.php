<?
// Using: Bill

/*
 *	2018-03-07 (Bill)    [2017-1102-1135-27054]
 *	     - Create file
 */

$PageRight = "ADMIN";

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_award.php");

function getChineseYearName($yearNumbers, $displayYearNameOnly=false)
{
    global $eReportCard;
    
    $yearName = '';
    for($i=0; $i<strlen($yearNumbers); $i++)
    {
        $yearName .= $eReportCard['AwardCertificate']['NumberCH'][strval(substr($yearNumbers, $i, 1))];
    }
    
    return $yearName . ($displayYearNameOnly? '' : $eReportCard['AwardCertificate']['YearCH']);
}
function getChineseMonthName($monthNumber)
{
    global $eReportCard;
    
    $monthName = '';
    if(strval($monthNumber) > 9)
    {
        $monthName .= $eReportCard['AwardCertificate']['NumberCH'][10];
    }
    if(strval($monthNumber) % 10 != 0)
    {
        $monthName .= $eReportCard['AwardCertificate']['NumberCH'][strval(substr($monthNumber, -1))];
    }
    
    return $monthName . $eReportCard['AwardCertificate']['MonthCH'];
}
function getChineseDateName($dateNumber)
{
    global $eReportCard;
    
    $dateName = "";
    if(strval($dateNumber) > 9)
    {
        if(strval($dateNumber) > 19) {
            $dateName .= $eReportCard['AwardCertificate']['NumberCH'][strval(substr($dateNumber, -2, 1))];
        }
        $dateName .= $eReportCard['AwardCertificate']['NumberCH'][10];
    }
    if(strval($dateNumber) % 10 != 0)
    {
        $dateName .= $eReportCard['AwardCertificate']['NumberCH'][strval(substr($dateNumber, -1))];
    }
    
    return $dateName . $eReportCard['AwardCertificate']['DateCH'];
}

function handleChineseDate($dateStr)
{
    $dateParts = explode('-', $dateStr);
    return (getChineseYearName($dateParts[0]) . getChineseMonthName($dateParts[1]) . getChineseDateName($dateParts[2]));
}
function handleChineseYear($yearStr)
{
    global $eReportCard;
    
    $yearParts = explode('-', $yearStr);
    
    $yearTermStr = $eReportCard['AwardCertificate']['YearTermCH'];
    $yearTermStr = str_replace('<!--StartYear-->', getChineseYearName($yearParts[0], true), $yearTermStr);
    $yearTermStr = str_replace('<!--EndYear-->', getChineseYearName($yearParts[1], true), $yearTermStr);
    
    return $yearTermStr;
}

function getAwardCertificateDisplayStyle($awardSize)
{
    $displayStyleArr = array();
    
    if($awardSize > 0 && $awardSize <= 8)
    {
        $displayStyleArr['EngSection']['PaddingHeight'] = '25';
        $displayStyleArr['EngSection']['DataHeight'] = '72.5';
        $displayStyleArr['EngSection']['DataRow1LineHeight'] = '1.6';
        $displayStyleArr['EngSection']['DataRow2LineHeight'] = '1.2';
        $displayStyleArr['EngSection']['DataRowSpaceLineHeight'] = '0.2';
        
        $displayStyleArr['ChiSection']['DataRow1Width'] = '18';
        $displayStyleArr['ChiSection']['DataRow1PaddingRight'] = '20';
        $displayStyleArr['ChiSection']['DataRow2Width'] = '15';
        $displayStyleArr['ChiSection']['DataRow2LineHeight'] = '1.5';
        $displayStyleArr['ChiSection']['DataRow3LineHeight'] = '1.3';
    }
    else if($awardSize == 9)
    {
        $displayStyleArr['EngSection']['PaddingHeight'] = '24';
        $displayStyleArr['EngSection']['DataHeight'] = '73.5';
        $displayStyleArr['EngSection']['DataRow1LineHeight'] = '1.5';
        $displayStyleArr['EngSection']['DataRow2LineHeight'] = '1.2';
        $displayStyleArr['EngSection']['DataRowSpaceLineHeight'] = '0.2';
        
        $displayStyleArr['ChiSection']['DataRow1Width'] = '17';
        $displayStyleArr['ChiSection']['DataRow1PaddingRight'] = '20';
        $displayStyleArr['ChiSection']['DataRow2Width'] = '15';
        $displayStyleArr['ChiSection']['DataRow2LineHeight'] = '1.5';
        $displayStyleArr['ChiSection']['DataRow3LineHeight'] = '1.3';
    }
    else if($awardSize == 10)
    {
        $displayStyleArr['EngSection']['PaddingHeight'] = '22';
        $displayStyleArr['EngSection']['DataHeight'] = '75.5';
        $displayStyleArr['EngSection']['DataRow1LineHeight'] = '1.3';
        $displayStyleArr['EngSection']['DataRow2LineHeight'] = '1.15';
        $displayStyleArr['EngSection']['DataRowSpaceLineHeight'] = '0.1';
        
        $displayStyleArr['ChiSection']['DataRow1Width'] = '15';
        $displayStyleArr['ChiSection']['DataRow1PaddingRight'] = '20';
        $displayStyleArr['ChiSection']['DataRow2Width'] = '10';
        $displayStyleArr['ChiSection']['DataRow2LineHeight'] = '1.35';
        $displayStyleArr['ChiSection']['DataRow3LineHeight'] = '1.3';
    }
    else if($awardSize == 11)
    {
        $displayStyleArr['EngSection']['PaddingHeight'] = '21.5';
        $displayStyleArr['EngSection']['DataHeight'] = '76';
        $displayStyleArr['EngSection']['DataRow1LineHeight'] = '1.3';
        $displayStyleArr['EngSection']['DataRow2LineHeight'] = '1.15';
        $displayStyleArr['EngSection']['DataRowSpaceLineHeight'] = '0.1';
        
        $displayStyleArr['ChiSection']['DataRow1Width'] = '15';
        $displayStyleArr['ChiSection']['DataRow1PaddingRight'] = '20';
        $displayStyleArr['ChiSection']['DataRow2Width'] = '10';
        $displayStyleArr['ChiSection']['DataRow2LineHeight'] = '1.25';
        $displayStyleArr['ChiSection']['DataRow3LineHeight'] = '1.2';
    }
    else if($awardSize == 12)
    {
        $displayStyleArr['EngSection']['PaddingHeight'] = '21';
        $displayStyleArr['EngSection']['DataHeight'] = '76.5';
        $displayStyleArr['EngSection']['DataRow1LineHeight'] = '1.3';
        $displayStyleArr['EngSection']['DataRow2LineHeight'] = '1.1';
        $displayStyleArr['EngSection']['DataRowSpaceLineHeight'] = '0.1';
        
        $displayStyleArr['ChiSection']['DataRow1Width'] = '13';
        $displayStyleArr['ChiSection']['DataRow1PaddingRight'] = '12';
        $displayStyleArr['ChiSection']['DataRow2Width'] = '10';
        $displayStyleArr['ChiSection']['DataRow2LineHeight'] = '1.2';
        $displayStyleArr['ChiSection']['DataRow3LineHeight'] = '1.15';
    }
    else if($awardSize == 13)
    {
        $displayStyleArr['EngSection']['PaddingHeight'] = '19';
        $displayStyleArr['EngSection']['DataHeight'] = '78.5';
        $displayStyleArr['EngSection']['DataRow1LineHeight'] = '1.2';
        $displayStyleArr['EngSection']['DataRow2LineHeight'] = '1.1';
        $displayStyleArr['EngSection']['DataRowSpaceLineHeight'] = '0.1';
        
        $displayStyleArr['ChiSection']['DataRow1Width'] = '13';
        $displayStyleArr['ChiSection']['DataRow1PaddingRight'] = '12';
        $displayStyleArr['ChiSection']['DataRow2Width'] = '10';
        $displayStyleArr['ChiSection']['DataRow2LineHeight'] = '1.15';
        $displayStyleArr['ChiSection']['DataRow3LineHeight'] = '1.13';
        $displayStyleArr['ChiSection']['DataRow3FontSize'] = 'font-size: 15pt;';
    }
    else if($awardSize == 14)
    {
        $displayStyleArr['EngSection']['PaddingHeight'] = '19';
        $displayStyleArr['EngSection']['DataHeight'] = '78.5';
        $displayStyleArr['EngSection']['DataRow1LineHeight'] = '1.2';
        $displayStyleArr['EngSection']['DataRow2LineHeight'] = '1.1';
        $displayStyleArr['EngSection']['DataRowSpaceLineHeight'] = '0.1';
        $displayStyleArr['EngSection']['DataRowFontSize'] = 'font-size: 13.5pt;';
        
        $displayStyleArr['ChiSection']['DataRow1Width'] = '12';
        $displayStyleArr['ChiSection']['DataRow1PaddingRight'] = '7';
        $displayStyleArr['ChiSection']['DataRow2Width'] = '8';
        $displayStyleArr['ChiSection']['DataRow2LineHeight'] = '1.1';
        $displayStyleArr['ChiSection']['DataRow2FontSize'] = 'font-size: 15pt;';
        $displayStyleArr['ChiSection']['DataRow3LineHeight'] = '1.1';
        $displayStyleArr['ChiSection']['DataRow3FontSize'] = 'font-size: 15pt;';
        $displayStyleArr['ChiSection']['DataRow4FontSize'] = 'font-size: 15pt;';
    }
    else if($awardSize == 15)
    {
        $displayStyleArr['EngSection']['PaddingHeight'] = '18';
        $displayStyleArr['EngSection']['DataHeight'] = '79.5';
        $displayStyleArr['EngSection']['DataRow1LineHeight'] = '1.2';
        $displayStyleArr['EngSection']['DataRow2LineHeight'] = '1.1';
        $displayStyleArr['EngSection']['DataRowSpaceLineHeight'] = '0.1';
        $displayStyleArr['EngSection']['DataRowFontSize'] = 'font-size: 13pt;';
        
        $displayStyleArr['ChiSection']['DataRow1Width'] = '12';
        $displayStyleArr['ChiSection']['DataRow1PaddingRight'] = '5';
        $displayStyleArr['ChiSection']['DataRow2Width'] = '8';
        $displayStyleArr['ChiSection']['DataRow2LineHeight'] = '1.1';
        $displayStyleArr['ChiSection']['DataRow2FontSize'] = 'font-size: 15pt;';
        $displayStyleArr['ChiSection']['DataRow3LineHeight'] = '1.11';
        $displayStyleArr['ChiSection']['DataRow3FontSize'] = 'font-size: 14pt;';
        $displayStyleArr['ChiSection']['DataRow4FontSize'] = 'font-size: 15pt;';
    }
    
    return $displayStyleArr;
}

$lreportcard = new libreportcard_award();
$lreportcard->hasAccessRight();

### POST data
$ReportID = $_POST['ReportID'];
$YearClassIDArr = $_POST['YearClassIDArr'];
$IssueDate = $_POST['issueDate'];

### Get Report Info
$ReportInfoArr = $lreportcard->returnReportTemplateBasicInfo($ReportID);
$ClassLevelID = $ReportInfoArr['ClassLevelID'];
$SemID = $ReportInfoArr['Semester'];
$SemesterSeq = $lreportcard->Get_Semester_Seq_Number($SemID);

### Get Report Subject
$SubjectArr = $lreportcard->returnSubjectwOrderNoL($ClassLevelID, $ParForSelection=0, $MainSubjectOnly=0, $ReportID);

### Get selected Student
$isShowStyle = ($ViewFormat == '' || $ViewFormat == 'html') ? true : false;
$StudentInfoArr = $lreportcard->GET_STUDENT_BY_CLASSLEVEL($ReportID, $ClassLevelID, $YearClassIDArr, $ParStudentID="", $ReturnAsso=0, $isShowStyle);
$StudentIDArr = Get_Array_By_Key($StudentInfoArr, 'UserID');
$numOfStudent = count($StudentInfoArr);

### Get Award Info
$AwardInfoArr = $lreportcard->Get_Award_Info();

### Get Student Award Info      // $StudentAwardInfoArr[$StudentID][$AwardID][$SubjectID][Field] = Data
$AwardIDArr = array($eRCTemplateSetting['Report']['ReportGeneration']['AwardGeneration_Config']['AwardArr']['excellent']['AwardID'], $eRCTemplateSetting['Report']['ReportGeneration']['AwardGeneration_Config']['AwardArr']['good']['AwardID']);
$StudentAwardInfoArr = $lreportcard->Get_Award_Generated_Student_Record($ReportID, $StudentIDArr, $ReturnAsso=1, $AwardNameWithSubject=1, $AwardIDArr);

### Build Student Content
// loop students
$StudentDisplayArr = array();
for ($i=0; $i<$numOfStudent; $i++)
{
    $thisStudentID = $StudentInfoArr[$i]['UserID'];
    
    // Student Info
    $InfoArr = array();
	$InfoArr['StudentInfo']['StudentID'] = $thisStudentID;
	$InfoArr['StudentInfo']['StudentNameCh'] = $StudentInfoArr[$i]['StudentNameCh'];
	$InfoArr['StudentInfo']['StudentNameEn'] = $StudentInfoArr[$i]['StudentNameEn'];
	$InfoArr['StudentInfo']['ClassNameCh'] = $StudentInfoArr[$i]['ClassTitleCh'];
	$InfoArr['StudentInfo']['ClassNameCh'] = str_replace("(", "", $InfoArr['StudentInfo']['ClassNameCh']);
	$InfoArr['StudentInfo']['ClassNameCh'] = str_replace(")", "", $InfoArr['StudentInfo']['ClassNameCh']);
	$InfoArr['StudentInfo']['ClassNameEn'] = $StudentInfoArr[$i]['ClassTitleEn'];
	$InfoArr['StudentInfo']['ClassNumber'] = $StudentInfoArr[$i]['ClassNumber'];
	$InfoArr['StudentInfo']['Gender']      = $StudentInfoArr[$i]['Gender'];
	
	// loop subjects
	foreach ((array)$SubjectArr as $thisSubjectID => $thisSubjectName)
	{
	    // Component Subject > SKIP
	    if ($thisSubjectID == 0)
	    {
	        continue;
	    }
	    
	    // loop awards
    	foreach ((array)$StudentAwardInfoArr[$thisStudentID] as $thisAwardID => $thisStudentAwardArr)
    	{
    		$thisSubjectAwardInfoArr = $thisStudentAwardArr[$thisSubjectID];
		    if(!empty($thisSubjectAwardInfoArr))
		    {
		        // Award Info
			    $displayAwardArr = array();
			    $displayAwardArr['AwardCode']   = $AwardInfoArr[$thisAwardID]['AwardCode'];
			    $displayAwardArr['AwardNameCh'] = $AwardInfoArr[$thisAwardID]['AwardNameCh'];
			    $displayAwardArr['AwardNameEn'] = $AwardInfoArr[$thisAwardID]['AwardNameEn'];
			    $displayAwardArr['SubjectNameCh'] = $lreportcard->GET_SUBJECT_NAME_LANG($thisSubjectID, 'ch');
			    $displayAwardArr['SubjectNameEn'] = $lreportcard->GET_SUBJECT_NAME_LANG($thisSubjectID, 'en');
			    $InfoArr['AwardList'][$thisSubjectID][] = $displayAwardArr;
    		}
		}
	}
	
	// No award > SKIP
	if(empty($InfoArr['AwardList']))
	{
	    continue;
	}
	
	$StudentDisplayArr[$thisStudentID] = $InfoArr;
}

$x = '';
if ($ViewFormat == '' || $ViewFormat == 'html')
{
    // Handling for Chinese Section width in IE
    if (isset($_SERVER['HTTP_USER_AGENT']) && (preg_match('|MSIE ([0-9].[0-9]{1,2})|', $_SERVER['HTTP_USER_AGENT']) || strpos($_SERVER['HTTP_USER_AGENT'], 'Trident/7.0; rv:11.0') !== false)) {
        $chi_size_width = 'width: 141mm';
        $chi_size_width2 = 'width: 141mm; height:185mm;';
    }
    else {
        $chi_size_width = 'width: 50%';
        $chi_size_width2 = 'width: 100%; height:100%;';
    }
    
    // Year Name
    $yearNameEn = $lreportcard->GET_ACTIVE_YEAR_NAME('en');
    $yearNameCh = $lreportcard->GET_ACTIVE_YEAR_NAME('ch');
    $yearNameCh = handleChineseYear($yearNameCh);
    
    // Issue Date
    if(checkDateIsValid($IssueDate) && !is_date_empty($IssueDate))
    {
        $issueDateEn = date("Y/m/d", strtotime($IssueDate));
        $issueDateCh = handleChineseDate(date("Y-n-j", strtotime($IssueDate)));
    }
    
    ### CSS Style
    echo '  <head>
                <style>
                    body { margin: 0 }
                    
                    .report { margin: 0 auto; height: 185mm; width: 282mm; }
                    td { vertical-align: top; text-align: center; }
                    
                    table.eng-side td {
                        font-size: 14pt;
                        font-family: Times New Roman;
                        line-height: 1.1;
                    }
                    table.eng-side td.eng-date, table.eng-side td.eng-sign {
                        /* font-size: 12pt; */
                    }
                    table.chi-side {
                        writing-mode: tb-rl;
                        filter: flipv fliph;
                    }
                    table.chi-side td {
                        font-size: 16pt;
                        font-family: DFKai-sb;
                    }
                    table.chi-side td.chi_details {
                        /* font-size: 20pt; */
                    }
                    table.chi-side td.chi_title {
                        font-size: 36pt;
                    }
                    table.chi-side td.chi_award {
                        /* font-size: 18pt; */
                    }
                </style>
            </head>';
    
    ### Report Content
    echo '  <body>
                <div class="main_container"> ';
    
    foreach($StudentDisplayArr as $thisSubjectDisplayInfo)
    {
        // Merge Subject awards
        $displayAwardCh = '';
        $displayAwardEn = '';
        $seperator = '';
        foreach((array)$thisSubjectDisplayInfo['AwardList'] as $thisSubjectInfo)
        {
            foreach((array)$thisSubjectInfo as $thisAwardInfo)
            {
                $displayAwardCh .= $seperator.$thisAwardInfo['SubjectNameCh']." ".$thisAwardInfo['AwardNameCh'].$eReportCard['AwardCertificate']['AwardsDisplayCh'];
                $displayAwardEn .= $seperator.$thisAwardInfo['SubjectNameEn']. " (".$thisAwardInfo['AwardNameEn'].")";
                $seperator = '<br />';
            }
        }
        
        $thisContentEN_1 = $eReportCard['AwardCertificate']['ContentEN_1'];
        $thisContentEN_1 = str_replace('<!--StudentName-->', $thisSubjectDisplayInfo['StudentInfo']['StudentNameEn'], $thisContentEN_1);
        $thisContentEN_1 = str_replace('<!--ClassName-->', $thisSubjectDisplayInfo['StudentInfo']['ClassNameEn'], $thisContentEN_1);
        
        $thisContentEN_2 = $eReportCard['AwardCertificate']['ContentEN_2'];
        $thisContentEN_2 = str_replace('<!--Gender-->', $eReportCard['AwardCertificate']['Gender_'.$thisSubjectDisplayInfo['StudentInfo']['Gender']], $thisContentEN_2);
        
        $thisContentEN_3 = $eReportCard['AwardCertificate']['ContentEN_3'];
        $thisContentEN_3 = str_replace('<!--AwardName-->', $displayAwardEn, $thisContentEN_3);
        $thisContentEN_3 = str_replace('<!--YearName-->', $yearNameEn, $thisContentEN_3);
        
        $thisContentCH_1 = $eReportCard['AwardCertificate']['ContentCH_1'];
        $thisContentCH_1 = str_replace('<!--StudentName-->', $thisSubjectDisplayInfo['StudentInfo']['StudentNameCh'], $thisContentCH_1);
        $thisContentCH_1 = str_replace('<!--ClassName-->', $thisSubjectDisplayInfo['StudentInfo']['ClassNameCh'], $thisContentCH_1);
        $thisContentCH_1 = str_replace('<!--YearName-->', $yearNameCh, $thisContentCH_1);
        $thisContentCH_1 = str_replace('<!--TermNumber-->', $eReportCard['AwardCertificate']['NumberCH'][$SemesterSeq], $thisContentCH_1);
        
        $thisContentCH_2 = $eReportCard['AwardCertificate']['ContentCH_2'];
        $thisContentCH_2 = str_replace('<!--AwardName-->', $displayAwardCh, $thisContentCH_2);
        
        $studentAwardListSize = count((array)$thisSubjectDisplayInfo['AwardList']);
        $reportStyleArr = getAwardCertificateDisplayStyle($studentAwardListSize);
        
        $x .= ' <div class="report" style="page-break-after:always;">
                <table class="container" style="height: 100%; width: 100%;">
                    <tr class="container-control" style="height: 0%">
                        <th style="width: 50%;"></th>
                        <th style="width: 50%;"></th>
                    </tr>
                    <tr>
                        <td style="width: 50%;">
                        <table class="eng-side" style="height: 100%;">
                            <tr style="height: '.$reportStyleArr['EngSection']['PaddingHeight'].'%;">
                                <th style="width: 10%;"></th>
                                <th style="width: 20%;"></th>
                                <th style="width: 30%;"></th>
                                <th style="width: 30%;"></th>
                                <th style="width: 10%;"></th>
                            </tr>
                            <tr style="height: '.$reportStyleArr['EngSection']['DataHeight'].'%;">
                                <td></td>
                                <td colspan="3" style="">
                                    <span style="line-height: '.$reportStyleArr['EngSection']['DataRow1LineHeight'].'; '.$reportStyleArr['EngSection']['DataRowFontSize'].'">'.$thisContentEN_1.'</span>
                                    <span style="line-height: '.$reportStyleArr['EngSection']['DataRowSpaceLineHeight'].';"><br/>&nbsp;<br/></span>
                                    <span style="line-height: '.$reportStyleArr['EngSection']['DataRow2LineHeight'].'; '.$reportStyleArr['EngSection']['DataRowFontSize'].'">'.$thisContentEN_2.'</span>
                                    <span style="line-height: '.$reportStyleArr['EngSection']['DataRowSpaceLineHeight'].';"><br/>&nbsp;<br/></span>
                                    <span style="line-height: '.$reportStyleArr['EngSection']['DataRow2LineHeight'].'; '.$reportStyleArr['EngSection']['DataRowFontSize'].'">'.$thisContentEN_3.'</span>
                                </td>
                                <td></td>
                            </tr>
                            <tr style="height: 5%;">
                                <td></td>
                                <td class="eng-date">'.$issueDateEn.'</td>
                                <td></td>
                                <td class="eng-sign" style="border-top: 1px solid black; padding-top: 2px;" width="40%">
                                    '.$eReportCard['AwardCertificate']['PrincipalNameDisplay'].'<br/>'.$eReportCard['AwardCertificate']['PrincipalEN'].
                                '</td>
                            </tr>
                        </table>
                        </td>
                        <td style="'.$chi_size_width.'">
                        <table class="chi-side" style="'.$chi_size_width2.';">
                            <tr style="width: 0%">
                                <th style="height: 25%;"></th>
                                <th style="height: 5%;"></th>
                                <th style="height: 40%;"></th>
                                <th style="height: 25%;"></th>
                                <th style="height: 5%;"></th>
                            </tr>
                            <tr style="width: '.$reportStyleArr['ChiSection']['DataRow1Width'].'%;">
                                <td></td>
                                <td></td>
                                <td class="chi_title" style="vertical-align: bottom; padding-right: '.$reportStyleArr['ChiSection']['DataRow1PaddingRight'].'px; padding-bottom: 20px;">　獎<span style="font-size: 50%;">　　</span>狀</td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr style="width: '.$reportStyleArr['ChiSection']['DataRow2Width'].'%; text-align: left;">
                                <td></td>
                                <td colspan="3" style="line-height: '.$reportStyleArr['ChiSection']['DataRow2LineHeight'].'; text-align: left; '.$reportStyleArr['ChiSection']['DataRow2FontSize'].'" class="chi_details">'.$thisContentCH_1.'</td>
                                <td></td>
                            </tr>
                            <tr style="width: 50%; text-align: left;">
                                <td></td>
                                <td colspan="3" style="line-height: '.$reportStyleArr['ChiSection']['DataRow3LineHeight'].'; text-align: left; padding-top: 10%; '.$reportStyleArr['ChiSection']['DataRow3FontSize'].'" class="chi_award">'.$thisContentCH_2.'</td>
                                <td></td>
                            </tr>
                            <tr style="width: 5%; text-align: right;">
                                <td></td>
                                <td></td>
                                <td></td>
                                <td class="chi-sign" style="text-align: right; '.$reportStyleArr['ChiSection']['DataRow4FontSize'].'">'.$eReportCard['AwardCertificate']['PrincipalCH'].'&nbsp;'.$eReportCard['Template']['PrincipalCh'].'　</td>
                                <td></td>
                            </tr>
                            <tr style="width: 12%; text-align: left;">
                                <td></td>
                                <td class="chi-date" colspan="3" style="text-align: left; '.$reportStyleArr['ChiSection']['DataRow4FontSize'].'">'.$issueDateCh.'</td>
                                <td></td>
                            </tr>
                        </table>
                        </td>
                    </tr>
                </table>
                </div>';
    }
	echo $x;
	echo '  </div></body>';
	
	intranet_closedb();
}
// else if ($ViewFormat=='csv')
// {
// 	//include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
// 	include_once($PATH_WRT_ROOT."includes/libexporttext.php");
// 	$lexport = new libexporttext();
	
// 	# Content
// 	$ExportContentArr = array();
// 	for ($i=0; $i<$numOfInfo; $i++)
// 	{
// 		$thisClassName = $InfoArr[$i]['ClassName'];
// 		$thisClassNumber = $InfoArr[$i]['ClassNumber'];
// 		$thisStudentName = $InfoArr[$i]['StudentName'];
// 		$thisAwardName = $InfoArr[$i]['AwardName'];
// 		$thisSubjectName = $InfoArr[$i]['SubjectName'];
// 		$thisAwardRank = $InfoArr[$i]['AwardRank'];
// 		$thisNumOfEstimatedScore = $InfoArr[$i]['NumOfEstimatedScore'];
		
// 		$ExportContentArr[$i][] = $thisClassName;
// 		$ExportContentArr[$i][] = $thisClassNumber;
// 		$ExportContentArr[$i][] = $thisStudentName;
// 		$ExportContentArr[$i][] = $thisAwardName;
// 		$ExportContentArr[$i][] = $thisSubjectName;
// 		$ExportContentArr[$i][] = $thisAwardRank;
// 		$ExportContentArr[$i][] = $thisNumOfEstimatedScore;
// 	}
	
// 	// Title of the grandmarksheet
// 	$ReportTitle = str_replace(" ", "_", $ReportTitle);
// 	$filename = $ReportTitle.".csv";
	
// 	$export_content = $lexport->GET_EXPORT_TXT($ExportContentArr, $HeaderArr);
// 	intranet_closedb();
	
// 	// Output the file to user browser
// 	$lexport->EXPORT_FILE($filename, $export_content);
// }
?>