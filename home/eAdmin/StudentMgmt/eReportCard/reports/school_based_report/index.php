<?php

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if ($plugin['ReportCard2008']) {
	include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
	include_once($PATH_WRT_ROOT."includes/libclass.php");
	include_once($PATH_WRT_ROOT."includes/libinterface.php");
	
	if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "")
	{
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
		$lreportcard = new libreportcardcustom();
	} else {
		$lreportcard = new libreportcard();
	}
	$linterface = new interface_html();
	$lclass = new libclass();
	
	if ($lreportcard->hasAccessRight()) {
		# Form Selection
		$ClassLvlArr = $lclass->getLevelArray();
		$SelectedFormArr = explode(",", $SelectedForm);
		$SelectedFormTextArr = explode(",", $SelectedFormText);
		$selectFormHTML = "<select id=\"ClassLvID\" name=\"ClassLvID\" >\n";
		for ($i = 0; $i < sizeof($ClassLvlArr); $i++) {
			$selectFormHTML .= "<option value=\"".$ClassLvlArr[$i][0]."\"";
			if ($_POST["submit_flag"] != "YES" || in_array($ClassLvlArr[$i][0], $SelectedFormArr)) {
				$selectFormHTML .= " selected";
			}
			$selectFormHTML .= ">".$ClassLvlArr[$i][1]."</option>\n";
		}
		$selectFormHTML .= "</select>\n";
// 		$selectFormHTML .= $linterface->GET_SMALL_BTN($button_select_all, "button", "js_Select_All('ClassLvID[]'); return false;");
		
		
		############## Interface Start ##############
		$linterface = new interface_html();
		$CurrentPage = "Reports_StudentYearlyResultReport";
		$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
		# tag information
		$TAGS_OBJ[] = array($eReportCard['Reports_StudentYearlyResultReport']);
		$linterface->LAYOUT_START();
		
		
		?>

<script language="javascript">
<!--
//function SelectAll(obj){
//	for (i=0; i<obj.length; i++){
//		obj.options[i].selected = true;
//	}
//}

function hideSelection ()
{
	if($("#ScholarshipReport").attr("checked"))
	{
		$("#honourSelection").show();
		$("#TermSelectionChkbox").show();
		$("#TermSelectionDropdown").hide();
		$("#TransferToAwardTr").hide();
	}
	else
	{
		$("#honourSelection").hide();
		$("#TermSelectionChkbox").hide();
		$("#TermSelectionDropdown").show();
		$("#TransferToAwardTr").show();
	}
}

function checkForm() 
{
	var obj = document.form1;
	var select_obj = obj.elements['ClassLvID'];
}
$().ready(function () {
});
//-->
</script>
<link type="text/css" rel="stylesheet" href="<?=$PATH_WRT_ROOT."templates/2007a/css/"?>ereportcard.css">
<br />

<form name="form1" action="export.php" method="POST" target="_blank" onsubmit="return checkForm();">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	</tr>
<table>
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>
			<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr>
					<td width="30%"></td>
					<td width="70%" align="right"><?=$linterface->GET_SYS_MSG($Result);?></td>
				</tr>
				
				<!--<tr id="ViewFormatRow">
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?= $eReportCard['ViewFormat'] ?>
					</td>
					<td width="75%" class='tabletext'>
						<?=$eReportCard['CSV']?>
					</td>
				</tr>-->
			<?php if(count($ClassLvlArr) == 0) { ?>
				<tr>
					<td align="center" colspan="2" class='tabletext'>
						<br /><?= $eReportCard['NoFormSetting'] ?><br />
					</td>
				</tr>
			<?php } else { ?>
				<tr>
					<td width="45% valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?= $eReportCard['Form'] ?>
					</td>
					<td width="55%" class='tabletext'>
						<?= $selectFormHTML ?>
					</td>
				</tr>
				<tr>
					<td colspan="2" align="center">
						<br/>
						<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "", "submitBtn"); ?>
					</td>
				</tr>
			<?php } ?>
			</table>
		</td>
	</tr>
</table>
</form>
<?
	print $linterface->FOCUS_ON_LOAD("form1.ClassLevelID");
  	$linterface->LAYOUT_STOP();
  } else {
?>
You have no priviledge to access this page.
<?
	}
} else {
?>

<?
}
?>
