<?php

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";
// $intranet_session_language = 'en';

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libattendance.php");

intranet_auth();
intranet_opendb();

$ClassLvID = $_POST['ClassLvID'];

if ($plugin['ReportCard2008'])
{
	include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
	if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") {
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
		$lreportcard = new libreportcardcustom();
	} else {
		$lreportcard = new libreportcard();
	}
	
	if ($lreportcard->hasAccessRight())
	{
		$lexport = new libexporttext();
		$fcm = new form_class_manage();
		$lsubject = new subject();
		$subjectList = $lsubject->Get_Subject_By_Form($ClassLvID);

		$reportType = 'W';
		$reportList = $lreportcard->Get_Report_List($ClassLvID, $reportType);
		$mainReportList = $lreportcard->Get_Report_List($ClassLvID, 'T', '1');

		$la = new libattendance();
		
		# Get Year Report
		if(!empty($reportList))
		{
			$report = end($reportList);
			$ReportID = $report['ReportID'];

			$libyear = new year($ClassLvID);
			$filename = "StudentYearlyResultReport_".$libyear->YearName.".csv";

			$headerAry = array('User Login', 'Class', 'Class No.', 'English Name', 'Number of times late', 'Number of days absent', 'Conduct', 'Yearly Average mark', 'Yearly position in class', 'Yearly position in form', 'Promote');
			$rowAry = array();

			$lastTermID = $lreportcard->Get_Last_Semester_Of_Report($ReportID);
			$_tempReportInfoAry =  $lreportcard->returnReportTemplateBasicInfo($ReportID);
			$_termReportTermStartDate = $_tempReportInfoAry['TermStartDate'];
			$_termReportTermEndDate = $_tempReportInfoAry['TermEndDate'];

            $relatedTermReportAry = $lreportcard->Get_Related_TermReport_Of_Consolidated_Report($ReportID);
            $relatedTermReportAry = BuildMultiKeyAssoc((array)$relatedTermReportAry, "Semester");
            $lastTermReportID = $relatedTermReportAry[$lastTermID]["ReportID"];

			$ay = new academic_year($lreportcard->GET_ACTIVE_YEAR_ID());
			$classAry = $fcm->Get_Class_List($lreportcard->GET_ACTIVE_YEAR_ID(),$ClassLvID);

			# Retrieve Subject Array
			$MainSubjectArray = $lreportcard->returnSubjectwOrder($ClassLvID, $ParForSelection=0, $TeacherID="", $SubjectFieldLang="", $ParDisplayType="Desc", $ExcludeCmpSubject=0, $ReportID);
			if (sizeof($MainSubjectArray) > 0) {
				foreach((array)$MainSubjectArray as $MainSubjectIDArray[] => $MainSubjectNameArray[]);
			}
			$SubjectArray = $lreportcard->returnSubjectwOrderNoL($ClassLvID, $ParForSelection=0, $MainSubjectOnly=0, $ReportID);
			if (sizeof($SubjectArray) > 0) {
				foreach((array)$SubjectArray as $SubjectIDArray[] => $SubjectNameArray[]);
			}
			foreach($SubjectArray as $SubjectID => $SubjectName){
				$tempSubj = new subject($SubjectID);
				$sName = $tempSubj->EN_DES;
				$headerAry[] = $sName;

				unset($tempSubj);
			}

			$ECACount = 10;
			$AwardPunishmentCount = 12;
			for($i=0; $i<$ECACount; $i++){
				$headerAry[] = 'ES' . ($i+1);
			}
			for($i=0; $i<12; $i++){
				$headerAry[] = 'AP' . ($i+1);
			}
			$headerAry[] = 'Teacher remarks';
			$headerAry[] = 'Name of class teacher';
			
			foreach($classAry as $classInfo)
			{
				// YearClassID
				$yearClassID = $classInfo['YearClassID'];
				$className = $classInfo['ClassTitleEN'];
				$yc = new year_class($yearClassID);
				$yc->Get_Class_Teacher_List(0);
				$classTeacherAry = $yc->ClassTeacherList;
// 				$teacherIDAry = Get_Array_By_Key($classTeacherAry, 'UserID');

				$teacherNameAry = array();
				$teacherName = "";
				foreach($classTeacherAry as $classTeacher){
					$tid = $classTeacher['UserID'];
					if(is_int(strpos($classTeacher['TeacherName'], '*'))){
						$sql = "SELECT EnglishName, Gender FROM INTRANET_ARCHIVE_USER WHERE UserID = '$tid'";
						$result = $fcm->returnArray($sql);
						$result = $result[0];
						$calling = $result['Gender'] == 'M' ? 'Mr.' : 'Ms.';
						$teacherNameAry[] = $calling . ' ' . $result['EnglishName'];

						unset($result);
					} else {
						$lu = new libuser($tid);
						$gender = $lu->Gender;
						$calling = $gender == 'M' ? 'Mr.' : 'Ms.';
						$teacherNameAry[] = $calling . ' ' . $lu->EnglishName;

						unset($lu);
					}
				}
				$teacherName = implode(', ', $teacherNameAry);
// 				$teacherName = implode(', ', Get_Array_By_Key($classTeacherAry, 'TeacherName'));
				
				$studentAry = $fcm->Get_Student_By_Class($yearClassID);
				$studentAttendanceAry = $lreportcard->Get_Student_Profile_Attendance_Data(0, $yearClassID, $_termReportTermStartDate, $_termReportTermEndDate, '', Get_Array_By_Key($studentAry, 'UserID'));
				foreach($studentAry as $std)
				{
					$row = array();

					$sid = $std['UserID'];
					$classno = $std['ClassNumber'];
					$lu = new libuser($sid);
					$userlogin = $lu->UserLogin;
					$userlogin = str_replace('czm', '', $userlogin);
					$userName = $lu->EnglishName;
					
					$OtherInfoDataAry = $lreportcard->getReportOtherInfoData($ReportID, $sid);
					$OtherInfoDataAry = $OtherInfoDataAry[$sid][$lastTermID];
					$PromotedTo = ($OtherInfoDataAry["Promoted to"]) ? $OtherInfoDataAry["Promoted to"] : "";
					
					if($lu->RecordStatus == 3){ // For graduated student
						$attendanceInfo = $la->returnStudentRecordByYear($sid,$ay->YearNameEN);
						$attendanceInfo = $attendanceInfo[0];
						$attendanceInfo["Days Absent"] = $attendanceInfo[1];
						$attendanceInfo["Time Late"] = $attendanceInfo[2];
					} else {
						$attendanceInfo = $studentAttendanceAry[$sid];
					}

					$OtherInfoDataAry["Times Late"] = isset($OtherInfoDataAry["Times Late"]) ? $OtherInfoDataAry["Times Late"] : $attendanceInfo["Time Late"];
					$OtherInfoDataAry["Days Absent"] = isset($OtherInfoDataAry["Days Absent"]) ? $OtherInfoDataAry["Days Absent"] : $attendanceInfo["Days Absent"];
					$OtherInfoDataAry["Days Absent"] = my_round($OtherInfoDataAry["Days Absent"], 1);
					if(!empty($OtherInfoDataAry))
					{
						// $DaysAbsence = ($OtherInfoDataAry["Days Absent"])? $OtherInfoDataAry["Days Absent"] : "0.00";
						$absent = ($OtherInfoDataAry["Days Absent"])? $OtherInfoDataAry["Days Absent"] : "0.0";
						$late = ($OtherInfoDataAry["Times Late"])? $OtherInfoDataAry["Times Late"] : "0";
						$conduct = ($OtherInfoDataAry["Conduct"])? $OtherInfoDataAry["Conduct"] : $lreportcard->EmptySymbol;
					}
					
					# Retrieve Grand Result Data
					$result = $lreportcard->getReportResultScore($ReportID, 0, $sid, "", 0, 1);
					$GrandAverage = $sid ? $result["GrandAverage"]: "S";
					$ClassPosition = $sid ? $result["OrderMeritClass"]: "#";
					$ClassNumOfStudent = $sid ? $result["ClassNoOfStudent"] : "#";
					$FormPosition = $sid ? $result["OrderMeritForm"] : "#";
					$FormNumOfStudent = $sid ? $result["FormNoOfStudent"] : "#";
					
					# Yearly Subject Result
					# Retrieve Subject Marks
					$MarksAry = $lreportcard->getMarks($ReportID, $sid, "", 0, 1);
					$subjectMarkAry = array();
					foreach($SubjectArray as $SubjectID => $SubjectName)
					{
						# Subject Mark Display
						$thisMSMark = $MarksAry[$SubjectID][0]["Mark"];
						$thisMark = ($thisMSMark) ? $thisMSMark : $lreportcard->EmptySymbol;
						$subjectMarkAry[] = $thisMark;
					}

					# ECA / Service
					# Award / Punishment
					$OLEDataSize = 0;
					$MeritAwardDataSize = 0;
					$ProfilePortfolioData = $lreportcard->Get_Profile_Portfolio_Data($ReportID, $sid);
					$MeritAwardDataAry = $ProfilePortfolioData["MeritAward"];
					$MeritAwardDataSize = sizeof($MeritAwardDataAry);
					
					$allTermIdAry = $lreportcard->returnReportInvolvedSem($ReportID);
					$allTermIdAry = array_keys($allTermIdAry);
					$allTermIdAry[] = '0';

					$OLEDataAry = $lreportcard->Get_eEnrolment_Data($sid, $allTermIdAry);
					$OLEDataSize = sizeof($OLEDataAry);

					$ESAry = array();
					for($i=0; $i<$OLEDataSize; $i++)
					{
						$fieldLocation = ($i % 2);
						if($fieldLocation == 0) {
							$OLEDataTable .= "<tr>";
						}
						
						$thisOLEData = $OLEDataAry[$i];
						$thisRole = trim($thisOLEData["RoleTitle"]);
						$thisClubTitle = trim($thisOLEData["ClubTitle"]);
						$thisPerformance = trim($thisOLEData["Performance"]);
						
						$thisOLEDisplay = "";
						$thisOLEDisplay .= $thisRole != "" && $thisRole != "N/A" ? $thisRole." of " : "";
						$thisOLEDisplay .= $thisClubTitle != "" ? $thisClubTitle : "";
						$thisOLEDisplay .= $thisPerformance != "" ? " - ".$thisPerformance : "";
						$thisOLEDisplay = $thisOLEDisplay != "" ? $thisOLEDisplay : "";
						$thisOLEDisplay = stripslashes($thisOLEDisplay);
						
						$ESAry[] = $thisOLEDisplay;
					}
					
					$MeritAwardTable = "<table width='100%' border='0' cellpadding='0' cellspacing='0' class='footer_table footer_table2' style='padding-bottom: 6px'>";
					$MeritAwardTable .= "<tr>";
					$MeritAwardTable .= "<td class='tabletext' colspan='2'><u>".$eReportCard["Template"]["AwardPunishment"]."<u></td>";
					$MeritAwardTable .= "</tr>";

					$APAry = array();
					for($i=0; $i<$MeritAwardDataSize; $i++)
					{
						$fieldLocation = ($i % 2);
						if($fieldLocation == 0) {
							$MeritAwardTable .= "<tr>";
						}
						
						$thisMeritData = $MeritAwardDataAry[$i];
						$thisMeritData = !empty($thisMeritData) ? $thisMeritData : "&nbsp;";
						$APAry[] = $thisMeritData;
					}

					# Teacher Remarks
					//$CommentAry = $lreportcard->returnSubjectTeacherComment($mainReportList[1]['ReportID'], $sid);
                    $CommentAry = $lreportcard->returnSubjectTeacherComment($lastTermReportID, $sid);
					$ClassTeacherComment = $CommentAry[0];
					$ClassTeacherComment = $ClassTeacherComment? $ClassTeacherComment : "";
					
					## Add Record into row
					$row = array($userlogin, $className, $classno, $userName, $late, $absent, $conduct, $GrandAverage, $ClassPosition.'#'.$ClassNumOfStudent, $FormPosition.'#'.$FormNumOfStudent, $PromotedTo);
					$row = array_merge($row, $subjectMarkAry);
					for($i=0; $i<$ECACount; $i++){
						$row[] = ($ESAry[$i]) ? $ESAry[$i] : '';
					}
					for($i=0; $i<12; $i++){
						$row[] = ($APAry[$i]) ? $APAry[$i] : '';
					}
					$row[] = $ClassTeacherComment;
					$row[] = $teacherName;
					$rowAry[] = $row;
					
					unset($lu);
				}
			}

			$contentAry = $lexport->GET_EXPORT_TXT($rowAry, $headerAry);
			$lexport->EXPORT_FILE($filename, $contentAry);
		}
	}
	else
    {
?>
		You have no priviledge to access this page.
<?php 
	}
}
else
{
?>
		You have no priviledge to access this page.
<?php
}

intranet_closedb();
?>