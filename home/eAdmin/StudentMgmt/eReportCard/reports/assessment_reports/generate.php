<?php
// Using: 
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
//include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage_ui.php");


intranet_auth();
intranet_opendb();

if (!$plugin['ReportCard2008'])
{
	header ("Location: /");
	intranet_closedb();
	exit();
}
 
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
{
	include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
	$lreportcard = new libreportcardcustom();
} else {
	$lreportcard = new libreportcard();
}

if (!$lreportcard->hasAccessRight()) 
{
	header ("Location: /");
	intranet_closedb();
	exit();
}

$linterface = new interface_html();
$ReportTemplateData = $lreportcard->returnReportTemplateBasicInfo($ReportID);

### Gen Report Info
$ReportTitlePieces = explode(":_:",$ReportTemplateData["ReportTitle"]);
$ReportTitle = implode("<br>",$ReportTitlePieces);
$ClassLevelID = $ReportTemplateData["ClassLevelID"];

$fcm_ui = new form_class_manage_ui();
$ly = new Year($ClassLevelID);

$FormName = $ly->YearName;
$ClassSelection = $fcm_ui->Get_Class_Selection($lreportcard->schoolYearID,$ClassLevelID,"ClassID[]",'','ajax_load_student()',1,1); 

### Gen Setting Table
$SubjectList = $lreportcard->GET_FROM_SUBJECT_GRADING_SCHEME($ClassLevelID,0,$ReportID);
$ReportColumnDataArr = $lreportcard->returnReportTemplateColumnData($ReportID);
$numOfReportColumn = count($ReportColumnDataArr);


### Build apply to all textbox and button
$defaultFullMark = 100;
$defaultPassingMark = 60;
//$passMarkAllTb = '<input type="text" id="passMarkDefaultTb" class="textboxnum"  maxlength="'.$lreportcard->textAreaMaxChar.'" value="'.$defaultPassingMark.'">';
//$passMarkAllBtn = '<a href="javascript:jsApplyToAll(\'passMarkDefaultTb\', \'passMarkTb\');"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_assign.gif" width="12" height="12" border="0"></a>';

//$fullMarkAllTb = '<input type="text" id="fullMarkDefaultTb" class="textboxnum"  maxlength="'.$lreportcard->textAreaMaxChar.'" value="'.$defaultFullMark.'">';
//$fullMarkAllBtn = '<a href="javascript:jsApplyToAll(\'fullMarkDefaultTb\', \'fullMarkTb\');"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_assign.gif" width="12" height="12" border="0"></a>';

$AssessmentAllBtn = '<a href="javascript:CheckAll('.$numOfReportColumn.');"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_assign.gif" width="12" height="12" border="0"></a>';

$displayAllChk = $linterface->Get_Checkbox('displayDefaultChk', 'displayDefaultChk', $Value=1, $isChecked=1, '', '', 'jsCheckAll()');

### table header
$display = '';
$display .= '<table width="100%" border="0" cellspacing="0" cellpadding="4">'."\n";
	$display .= '<tr class="tabletop">'."\n";
		$display .= '<td class="tabletopnolink term_break_link" align="center" width="10%">'.$eReportCard['Subject'].'</td>'."\n";
		$display .= '<td class="tabletopnolink term_break_link" align="center">'.$eReportCard['Assessment'].'</td>'."\n";
		$display .= '<td class="tabletopnolink term_break_link" align="center" width="2%">'.$eReportCard['Display'].'</td>'."\n";
	$display .= '</tr>'."\n";
	
	$display .= '<tr class="tablebottom">'."\n";
		$display .= '<td class="tabletopnolink term_break_link" align="center" width="10%">&nbsp;</td>'."\n";
		$display .= '<td class="tabletopnolink term_break_link" align="center" width="8%">';
			for($i = 0; $i<$numOfReportColumn; $i++) //for each sub-marksheet item
			{
				$thisColumnID = $ReportColumnDataArr[$i]["ReportColumnID"];
				$thisColumnTitle = $ReportColumnDataArr[$i]["ColumnTitle"];
				$display .= '
					<span style="padding:2px; white-space:nowrap; ">
						<input type="radio" id="Assessment'.$thisColumnID.'" name="Assessment['.$SubjectID.']" class="assessmentDefaultRadio'.$i.'" value="'.$thisColumnID.'">
						<label for="Assessment'.$thisColumnID.'">'.$thisColumnTitle.'</label>
					</span>			
				';
			}
		$display .= $AssessmentAllBtn.'</td>'."\n";
		$display .= '<td class="tabletopnolink term_break_link" align="center" width="2%">'.$displayAllChk.'</td>'."\n";
	$display .= '</tr>'."\n";

### table body
foreach((array)$SubjectList as $SubjectID => $SubjectData) //for each row
{
	if($SubjectData["is_cmpSubject"]==1) continue;
	$SubjectName = $SubjectData['subjectName'];
	
	$css = ++$i%2==0?"tablerow1":"tablerow2";
	$display .= '
	<tr>
		<td class="tabletext '.$css.' term_break_link td_'.$SubjectID.' subjectTd">'.$SubjectName.'</td>
	';
	
	$display .= '<td class="tabletext '.$css.' term_break_link td_'.$SubjectID.' subjectTd" align="center">';
	for($i = 0; $i<$numOfReportColumn; $i++) //for each sub-marksheet item
	{
		$thisColumnID = $ReportColumnDataArr[$i]["ReportColumnID"];
		$thisColumnTitle = $ReportColumnDataArr[$i]["ColumnTitle"];
		$display .= '
			<span style="padding:2px; white-space:nowrap; ">
				<input type="radio" id="Assessment_'.$SubjectID.'" name="Assessment['.$SubjectID.']" class="assessmentRadio'.$i.'" value="'.$thisColumnID.'">
				<label for="Assessment'.$thisColumnID.'_'.$SubjectID.'">'.$thisColumnTitle.'</label>
			</span>			
		';
	}
	$display .= '</td>';
	

	/*	<td class="tabletext '.$css.' term_break_link td_'.$SubjectID.' subjectTd" align="center"><input type="text" name="FullMark['.$SubjectID.']" class="textboxnum fullMarkTb"  maxlength="'.$lreportcard->textAreaMaxChar.'" value="'.$defaultFullMark.'"></td>
		<td class="tabletext '.$css.' term_break_link td_'.$SubjectID.' subjectTd" align="center"><input type="text" name="PassMark['.$SubjectID.']" class="textboxnum passMarkTb"  maxlength="'.$lreportcard->textAreaMaxChar.'" value="'.$defaultPassingMark.'"></td>*/
	$display .= '
		<td class="tabletext '.$css.' term_break_link" align="center"><input type="checkbox" name="Display[]" id="Display_'.$SubjectID.'" value="'.$SubjectID.'" onclick="CheckDisplay(this,'.$SubjectID.')" class="displayChk" CHECKED></td>
	</tr>
	';
	
}

$display .= '</table>';


### Page Layout  
$CurrentPage = "Reports_AssessmentReport";
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();

$TAGS_OBJ[] = array($eReportCard['Reports_AssessmentReport'], "", 0);
$linterface->LAYOUT_START();
?>
<script>
function SelectAll(str)
{
	$("select#"+str).contents().attr("selected",true);
}

function CheckAll(numOfColumn)
{
	for(i=0; i<numOfColumn; i++)
	{
		if($(".assessmentDefaultRadio"+i).attr("checked"))
		{
			$(".assessmentRadio"+i).attr("checked","CHECKED");
		}
	}
}

function CheckDisplay(obj,SubjectID)
{
	$("[name='Assessment["+SubjectID+"]']").attr("disabled",!obj.checked);
	$("[name='FullMark["+SubjectID+"]']").attr("disabled",!obj.checked);
	$("[name='PassMark["+SubjectID+"]']").attr("disabled",!obj.checked);
	$(".td_" + SubjectID).attr("disabled",!obj.checked);
	
	$('#displayDefaultChk').attr('checked', false);
}

function jsCheckForm()
{
	if ($("[name='Display[]']:checked").length == 0)
	{
		alert("<?=$eReportCard['jsSelectSubjectWarning']?>");
		return false;
	}
	
	document.frm.submit();
}

function jsApplyToAll(targetValueElement, targetClass)
{
	var targetValue = $('#' + targetValueElement).val();
	
	$('.' + targetClass).each( function () {
		$(this).val(targetValue);
	});
}

function ajax_load_student()
{
	
	var ClassIDArr = new Array();
	$("#ClassID\\[\\]").each(function(){ 
		ClassIDArr[ClassIDArr.length] = $(this).val(); 
	});
	
	var ClassIDStr = ClassIDArr.join(",");
	
	
	$.post(
		"../ajax_reload_selection.php",
		{
			RecordType 	: 'loadStudentList',
			ReportID 	: $("#ReportID").val(),
			ClassLevelID 	: $("#ClassLevelID").val(),
			ClassIDStr	: ClassIDStr,
			IncludeAssessmentLib : 1
		},
		function(ReturnData)
		{
			$("td#StudentListTD").html(ReturnData);
			SelectAll("StudentID");
		}
	)
}

function jsCheckAll()
{
	var jsSelected = $('#displayDefaultChk').attr('checked');
	
	$('.displayChk').each( function () {
		$(this).attr('checked', jsSelected);
	});
	
	$('.fullMarkTb').each( function () {
		$(this).attr('disabled', !jsSelected);
	});
	
	$('.passMarkTb').each( function () {
		$(this).attr('disabled', !jsSelected);
	});
	
	$('.subjectTd').each( function () {
		$(this).attr('disabled', !jsSelected);
	});
	
	$('.assessmentRadio').each( function () {
		$(this).attr('disabled', !jsSelected);
	});
}

$().ready(function(){
	SelectAll("ClassID\\[\\]");
	ajax_load_student();
});
</script>

<br>
<!-- Start Main Table //-->
<form id="frm" name="frm" method="POST" action="print_preview.php" target="_blank">
<table width="96%" border="0" cellspacing="0" cellpadding="5">
  <tr> 
    <td align="center" colspan="2"><table width="100%" border="0" cellspacing="0" cellpadding="2">
      <tr>
        <td align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td align="right"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                
                <tr>
                  <td align="right" class="tabletextremark">
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                         
                        <td align="right" valign="bottom"><?=$linterface->GET_SYS_MSG($msg);?></td>
                      </tr>
                  </table>
                  </td>
                </tr>
            </table></td>
          </tr>
          <tr>
            <td>
	            <table width="100%" border="0" cellspacing="0" cellpadding="4">
		            <tr>
						<td width="30%" nowrap="nowrap" class="formfieldtitle tabletext" valign="top"><?=$eReportCard['ReportTitle']?></td>
						<td valign="top" class="tabletext" width="70%"><?=$ReportTitle?></td>
					</tr>
					<tr>
						<td width="30%" nowrap="nowrap" class="formfieldtitle tabletext" valign="top"><?=$eReportCard['Form']?></td>
						<td valign="top" class="tabletext" width="70%"><?=$FormName?></td>
					</tr>
					<tr>
						<td width="30%" nowrap="nowrap" class="formfieldtitle tabletext" valign="top"><?=$eReportCard['Class']?></td>
						<td valign="top" class="tabletext" width="70%">
							<table cellpadding=0 cellspacing=4 border=0>
								<tr>
									<td>
										<?=$ClassSelection?>
										<?= $linterface->GET_SMALL_BTN($button_select_all, "button", "SelectAll('ClassID\\\\[\\\\]'); ajax_load_student()", "selectAllBtn01")?>
									</td>
									<td id="StudentListTD"><!--load on ajax--></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td width="30%" nowrap="nowrap" class="formfieldtitle tabletext" valign="top"><?=$eReportCard['Assessment']." ".$eReportCard['ReportTitle']?></td>
						<td valign="top" class="tabletext" width="70%">
							<input name="title1" id="title1" type="text" value="<?=$ReportTitlePieces[0]?>" class="textboxtext" maxlength="<?=$lreportcard->textAreaMaxChar?>"><br>
							<input name="title2" id="title2" type="text" value="<?=$ReportTitlePieces[1]?>"  class="textboxtext"  maxlength="<?=$lreportcard->textAreaMaxChar?>"><br>
							<input name="title3" id="title3" type="text" value="<?=$ReportTitlePieces[2]?>"  class="textboxtext"  maxlength="<?=$lreportcard->textAreaMaxChar?>">
						</td>
					</tr>
					<tr>
						<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"> 
							<?= $eReportCard['IssueDate'] ?>
						</td>
						<td width="75%" class='tabletext'>
							<?= $linterface->GET_DATE_PICKER("IssueDate"); ?>
						</td>
					</tr>
					<?if($ReportCardCustomSchoolName=="kiansu_chekiang_college"){?>
					<tr>
						<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
							<?= $eReportCard['DataSource'] ?>
						</td>
						<td width="75%" class='tabletext'>
							<input type="radio" id="DataSource1" name="DataSource" value="Report" CHECKED><label for="DataSource1"><?=$eReportCard['DataSourceChoice']['Report']?></label>
							<input type="radio" id="DataSource2" name="DataSource" value="MarkSheet" ><label for="DataSource2"><?=$eReportCard['DataSourceChoice']['Marksheet']?></label>
						</td>
					</tr>
					<?
					}
					else
					{
					?>
						<input type="hidden" name="DataSource" value="Report" CHECKED>
					<?	
					}
					?>
	            </table>
	            <br>
	            <table width="100%" border="0" cellspacing="0" cellpadding="4">
		            <tr><td>
		           		<?=$linterface->GET_NAVIGATION2($eReportCard['SubjectSettings'])?>	
		            </td></tr>
		            <tr><td>
		            	<?=$display?>
		            </td></tr>
	            </table>
	            <br>
            </td>
          </tr>
          <tr>
            <td height="1" class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
          </tr>
          <tr>
          	<td align="center">
          		<br><br>
          		<?=$linterface->GET_ACTION_BTN($button_print, "button", "jsCheckForm();", "PrintBtn")?>
          		&nbsp;&nbsp;
          		<?=$linterface->GET_ACTION_BTN($button_back, "button", "self.location='index.php'", "PrintBtn")?>
          	</td>
          </tr>
        </table>
          </td>
      </tr>
    </table>
    </td>
  </tr>
</table>
<input type="hidden" id="ClassLevelID" name="ClassLevelID" value="<?=$ClassLevelID?>">
<input type="hidden" id="ReportID" name="ReportID" value="<?=$ReportID?>">
</form>
<br>
<br>
<!-- End Main Table //-->
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>