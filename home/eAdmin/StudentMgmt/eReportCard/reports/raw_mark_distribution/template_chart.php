<?php

# using by: adam


$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/flashchart_basic/php-ofc-library/open-flash-chart.php");

$postImgUrl = "http://192.168.0.146:31002/home/eAdmin/StudentMgmt/eReportCard/reports/raw_mark_distribution/uploadFlashImage.php";

function drawChart($data){
	//get the data
	$maxNo = 1; //decide the x axis range i.e. maximum number of student
	$yLabels = array(); //y labels i.e. Raw Marks
	$xValue = array(); //value along x-axis i.e. No. of Student
	$tip = array(); //string on every tooltip i.e. remarks
	foreach($data as $mark => $value){
		$yLabels[] = $mark;
		$xValue[] = $value["NumOfStn"];
		$tip[] = $value["Remarks"]; 
		if($value["NumOfStn"] > $maxNo){
			$maxNo = $value["NumOfStn"];
		}
	}//end foreach
	$maxNo += 4;

	# flash chart
	//$title = new title( "中文測試 2007-2008 No. of Merits and Demerits of Form 1 Statistics" );
	//$title->set_style( "{font-size: 16px; font-family: _sans; color: #333333; font-weight:bold; text-align:center;}" );

	$x_legend = new x_legend( 'No. of Students' );
	$x_legend->set_style( '{font-size: 12px; font-weight: normal; color: #000000}' );

	$x = new x_axis();
	$x->set_stroke( 2 );
	$x->set_tick_height( 2 );
	$x->set_colour( '#999999' );
	$x->set_grid_colour( '#CCCCCC' );
	$x->set_range( 0, $maxNo, 1 );
	$x->set_offset(false);

	$y_legend = new y_legend( 'Raw Marks' );
	$y_legend->set_style( '{font-size: 12px; font-weight: normal; color: #000000}' );

	$y = new y_axis();
	$y->set_stroke( 2 );
	$y->set_tick_length( 2 );
	$y->set_colour( '#999999' );
	$y->set_grid_colour( '#CCCCCC' );
	$y->set_labels( $yLabels );
	$y->set_offset(true);

	$bar0 = new hbar('#333333');
	$bar0->set_values( $xValue );
	$bar0->set_tooltip( $tip );
	//$bar0->set_key( '5.88%(64.71%)', '12' );
	$bar0->set_id( 0 );
	$bar0->set_visible( true );

	$tooltip = new tooltip();
	$tooltip->set_static();
	$tooltip->set_stroke( 2 );
	$tooltip->set_body_style( '{font-size: 9px; font-weight: lighter; color: #000000}' );

	# show/hide checkbox panel
	$key = new key_legend();
	$key->set_visible(false);
	$key->set_selectable(false);

	$chart = new open_flash_chart();
	$chart->set_bg_colour( '#FFFFFF' );
	//$chart->set_title( $title );
	$chart->set_x_legend( $x_legend );
	$chart->set_x_axis( $x );
	$chart->set_y_legend( $y_legend );
	$chart->set_y_axis( $y );
	$chart->add_element( $bar0 );
	$chart->set_tooltip( $tooltip );
	$chart->set_key_legend($key);

	return $chart;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
$chartWidth = "586";
$chartHeight = "737";

$chartArr = array();

$chinese_listening = array("43"=>array("NumOfStn"=>0, "Remarks"=>""), "44"=>array("NumOfStn"=>3, "Remarks"=>"1.6% (1.6%)"), "45"=>array("NumOfStn"=>0, "Remarks"=>""), "46"=>array("NumOfStn"=>2, "Remarks"=>"1.07% (2.67%)"), "47"=>array("NumOfStn"=>12, "Remarks"=>"6.42% (18.86%)"), "48"=>array("NumOfStn"=>2, "Remarks"=>"1.07% (3.74%)"), "49"=>array("NumOfStn"=>5, "Remarks"=>"2.67% (6.42%)"), "50"=>array("NumOfStn"=>4, "Remarks"=>"3.42% (10.86%)"), "51"=>array("NumOfStn"=>8, "Remarks"=>"4.28% (10.7%)"), "52"=>array("NumOfStn"=>0, "Remarks"=>""), "53"=>array("NumOfStn"=>2, "Remarks"=>"1.07% (11.76%)"), "54"=>array("NumOfStn"=>5, "Remarks"=>"2.67% (14.44%)"), "55"=>array("NumOfStn"=>0, "Remarks"=>""), "56"=>array("NumOfStn"=>12, "Remarks"=>"6.42% (20.86%)"), "57"=>array("NumOfStn"=>0, "Remarks"=>""), "58"=>array("NumOfStn"=>3, "Remarks"=>"1.6% (1.6%)"), "59"=>array("NumOfStn"=>6, "Remarks"=>"6.42% (12.86%)"), "60"=>array("NumOfStn"=>2, "Remarks"=>"1.07% (2.67%)"), "61"=>array("NumOfStn"=>19, "Remarks"=>"16.42% (32.86%)"), "62"=>array("NumOfStn"=>12, "Remarks"=>"1.07% (3.74%)"), "63"=>array("NumOfStn"=>5, "Remarks"=>"2.67% (6.42%)"), "64"=>array("NumOfStn"=>13, "Remarks"=>"12.42% (15.86%)"), "65"=>array("NumOfStn"=>8, "Remarks"=>"4.28% (10.7%)"), "66"=>array("NumOfStn"=>0, "Remarks"=>""), "67"=>array("NumOfStn"=>2, "Remarks"=>"1.07% (11.76%)"), "68"=>array("NumOfStn"=>5, "Remarks"=>"2.67% (14.44%)"), "69"=>array("NumOfStn"=>0, "Remarks"=>""), "70"=>array("NumOfStn"=>12, "Remarks"=>"6.42% (20.86%)"), "71"=>array("NumOfStn"=>0, "Remarks"=>""), "72"=>array("NumOfStn"=>8, "Remarks"=>"4.28% (10.7%)"), "73"=>array("NumOfStn"=>11, "Remarks"=>"8.40% (18.16%)"), "74"=>array("NumOfStn"=>18, "Remarks"=>"1.07% (11.76%)"), "75"=>array("NumOfStn"=>5, "Remarks"=>"2.67% (14.44%)"), "76"=>array("NumOfStn"=>0, "Remarks"=>""), "77"=>array("NumOfStn"=>12, "Remarks"=>"6.42% (20.86%)"), "78"=>array("NumOfStn"=>14, "Remarks"=>"13.42% (12.16%)"), "79"=>array("NumOfStn"=>7, "Remarks"=>"1.6% (1.6%)"), "80"=>array("NumOfStn"=>0, "Remarks"=>""), "81"=>array("NumOfStn"=>2, "Remarks"=>"1.07% (2.67%)"), "82"=>array("NumOfStn"=>4, "Remarks"=>"6.42% (2.86%)"), "83"=>array("NumOfStn"=>17, "Remarks"=>"1.07% (3.74%)"), "84"=>array("NumOfStn"=>5, "Remarks"=>"2.67% (6.42%)"), "85"=>array("NumOfStn"=>0, "Remarks"=>""), "86"=>array("NumOfStn"=>3, "Remarks"=>"4.28% (10.7%)"), "87"=>array("NumOfStn"=>6, "Remarks"=>"6.42% (20.86%)"), "88"=>array("NumOfStn"=>2, "Remarks"=>"1.07% (11.76%)"), "89"=>array("NumOfStn"=>11, "Remarks"=>"2.67% (14.44%)")); 

$chart0 = drawChart($chinese_listening);
$chartArr[] = $chart0;

$english_writting = array("43"=>array("NumOfStn"=>0, "Remarks"=>""), "44"=>array("NumOfStn"=>13, "Remarks"=>"11.6% (1.6%)"), "45"=>array("NumOfStn"=>0, "Remarks"=>""), "46"=>array("NumOfStn"=>2, "Remarks"=>"1.07% (2.67%)"), "47"=>array("NumOfStn"=>2, "Remarks"=>"16.42% (8.86%)"), "48"=>array("NumOfStn"=>2, "Remarks"=>"1.07% (3.74%)"), "49"=>array("NumOfStn"=>5, "Remarks"=>"2.67% (6.42%)"), "50"=>array("NumOfStn"=>4, "Remarks"=>"3.42% (10.86%)"), "51"=>array("NumOfStn"=>22, "Remarks"=>"4.28% (10.7%)"), "52"=>array("NumOfStn"=>0, "Remarks"=>""), "53"=>array("NumOfStn"=>10, "Remarks"=>"1.07% (11.76%)"), "54"=>array("NumOfStn"=>15, "Remarks"=>"2.67% (14.44%)"), "55"=>array("NumOfStn"=>0, "Remarks"=>""), "56"=>array("NumOfStn"=>12, "Remarks"=>"6.42% (20.86%)"), "57"=>array("NumOfStn"=>0, "Remarks"=>""), "58"=>array("NumOfStn"=>3, "Remarks"=>"1.6% (1.6%)"), "59"=>array("NumOfStn"=>6, "Remarks"=>"6.42% (12.86%)"), "60"=>array("NumOfStn"=>13, "Remarks"=>"1.07% (2.67%)"), "61"=>array("NumOfStn"=>9, "Remarks"=>"16.42% (32.86%)"), "62"=>array("NumOfStn"=>9, "Remarks"=>"1.07% (3.74%)"), "63"=>array("NumOfStn"=>5, "Remarks"=>"2.67% (6.42%)"), "64"=>array("NumOfStn"=>13, "Remarks"=>"12.42% (15.86%)"), "65"=>array("NumOfStn"=>8, "Remarks"=>"4.28% (10.7%)"), "66"=>array("NumOfStn"=>0, "Remarks"=>""), "67"=>array("NumOfStn"=>2, "Remarks"=>"1.07% (11.76%)"), "68"=>array("NumOfStn"=>5, "Remarks"=>"2.67% (14.44%)"), "69"=>array("NumOfStn"=>0, "Remarks"=>""), "70"=>array("NumOfStn"=>12, "Remarks"=>"6.42% (20.86%)"), "71"=>array("NumOfStn"=>0, "Remarks"=>""), "72"=>array("NumOfStn"=>8, "Remarks"=>"4.28% (10.7%)"), "73"=>array("NumOfStn"=>11, "Remarks"=>"8.40% (18.16%)"), "74"=>array("NumOfStn"=>18, "Remarks"=>"1.07% (11.76%)"), "75"=>array("NumOfStn"=>15, "Remarks"=>"2.67% (14.44%)"), "76"=>array("NumOfStn"=>0, "Remarks"=>""), "77"=>array("NumOfStn"=>12, "Remarks"=>"6.42% (20.86%)"), "78"=>array("NumOfStn"=>24, "Remarks"=>"13.42% (12.16%)"), "79"=>array("NumOfStn"=>17, "Remarks"=>"1.6% (1.6%)"), "80"=>array("NumOfStn"=>0, "Remarks"=>""), "81"=>array("NumOfStn"=>2, "Remarks"=>"1.07% (2.67%)"), "82"=>array("NumOfStn"=>4, "Remarks"=>"6.42% (2.86%)"), "83"=>array("NumOfStn"=>17, "Remarks"=>"1.07% (3.74%)"), "84"=>array("NumOfStn"=>5, "Remarks"=>"2.67% (6.42%)"), "85"=>array("NumOfStn"=>0, "Remarks"=>""), "86"=>array("NumOfStn"=>3, "Remarks"=>"4.28% (10.7%)"), "87"=>array("NumOfStn"=>6, "Remarks"=>"6.42% (20.86%)"), "88"=>array("NumOfStn"=>9, "Remarks"=>"1.07% (11.76%)"), "89"=>array("NumOfStn"=>11, "Remarks"=>"2.67% (14.44%)")); 

$chart1 = drawChart($english_writting);
$chartArr[] = $chart1;

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

?>

<html>
<head>
	<title>flash demo</title>
	<meta http-equiv="content-type" content="text/html; charset=utf-8">
	<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>includes/flashchart_basic/js/json/json2.js"></script>
	<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>includes/flashchart_basic/js/swfobject.js"></script>	
	<script type="text/javascript">

		function ofc_ready(){
			//alert('ofc_ready');
		}

		function open_flash_chart_data(id){
			return JSON.stringify(dataArr[id]);
		}

		function findSWF(movieName) {
		  if (navigator.appName.indexOf("Microsoft")!= -1) {
			return window[movieName];
		  } else {
			return document[movieName];
		  }
		}

		/*
		function jsonLoaded(id){
			//alert("json loaded and id: "+id);
		}	
	
		function onAutoReturnImgBinary(bmpBytStr){
			//alert("image binary string is automatically returned: "+bmpBytStr);
		}
		*/

		function onAutoPostImgBinary(id){
			//alert("image binary string is automatically posted and flash id: "+id);
			var swfDIV = document.getElementById("div"+id+"parent");
			//var swfDIV = document.getElementById("img"+id);
			//alert("swfDIV: "+swfDIV+" / width: "+swfDIV.width+" / height: "+swfDIV.height);
			while(swfDIV.hasChildNodes()){
				swfDIV.removeChild(swfDIV.firstChild);
			}
			var img = document.createElement("img");
			img.setAttribute("src", "tmp_chart/tmp_chart_png_"+id+".png");
			img.setAttribute("width", "<?=$chartWidth?>");
			img.setAttribute("height", "<?=$chartHeight?>");
			swfDIV.appendChild(img);
		}
		
		var chartNum = <?=count($chartArr)?>;
		//alert("chartNum: "+chartNum);
		
		var dataArr = new Array();
		<?php
			foreach( $chartArr as $chartID => $chartJSON ) {
				echo "dataArr[ ".$chartID." ] = ".$chartJSON->toPrettyString()."\n";
			}
		?>
		//alert("dataArr length: "+dataArr.length);

	</script>

	<script type="text/javascript">
		var chartNum = <?=count($chartArr)?>;
		for(var k=0; k<chartNum; k++){
			var flashvars = {id:k, postImgUrl:"<?=$postImgUrl?>"};
			swfobject.embedSWF("<?=$PATH_WRT_ROOT?>includes/flashchart_basic/open-flash-chart.swf", "div"+k, "<?=$chartWidth?>", "<?=$chartHeight?>", "9.0.0", "", flashvars);
		}//end for
	</script>
		
</head>
<body>	
<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center" style="height:100%;">
			<table width="100%" border="0" cellspacing="0" cellpadding="0" style="height:100%;">
				<tr>
					<td align="center" style="width:<?=$chartWidth?>px; height:<?=$chartHeight?>px;">
						<div id="div0parent" style="width:100%; height:100%;">
							<div id="div0" style="width:100%; height:100%;">no flash?</div>
						</div>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr><td>&nbsp;</td></tr>


	<tr>
		<td align="center" style="height:100%;">
			<table width="100%" border="0" cellspacing="0" cellpadding="0" style="height:100%;">
				<tr>
					<td align="center" style="width:<?=$chartWidth?>px; height:<?=$chartHeight?>px;">
						<div id="div1parent" style="width:100%; height:100%;">
							<div id="div1" style="width:100%; height:100%;">no flash?</div>
						</div>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr><td>&nbsp;</td></tr>

</table>
</body>
</html>

