<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if ($plugin['ReportCard2008']) {
	include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
	include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
	include_once($PATH_WRT_ROOT."includes/libinterface.php");
	include_once($PATH_WRT_ROOT."includes/form_class_manage_ui.php");
	include_once($PATH_WRT_ROOT."includes/subject_class_mapping_ui.php");
	
	if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
	{
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
		$lreportcard = new libreportcardcustom();
	} else {
		$lreportcard = new libreportcard();
	}
	$lreportcard_ui = new libreportcard_ui();
	$linterface = new interface_html();
	
	if ($lreportcard->hasAccessRight()) {
		
		############## Interface Start ##############
		$linterface = new interface_html();
		$CurrentPage = "Reports_SubjectSummarySheet";
		$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
		# tag information
		$TAGS_OBJ[] = array($eReportCard['Reports_SubjectSummarySheet']);
		$linterface->LAYOUT_START();
		#############################################
		
		### Get Form selection (show form which has report card only)
		$libForm = new Year();
		$FormArr = $libForm->Get_All_Year_List();
		$YearID = $FormArr[0]['YearID'];
		
		$FormSelection = $lreportcard_ui->Get_Form_Selection('YearID', $YearID, 'js_Reload_Selection(this.value)', $noFirst=1);
		
		### Get Class selection (for demo only - to be deleted)
		$libFCP_ui = new form_class_manage_ui();
		$ClassSelection = $libFCP_ui->Get_Class_Selection($lreportcard_ui->schoolYearID, $YearID, 'YearClassIDArr[]', '', '', $noFirst=1, $isMultiple=1);
		
		### Get Report selection
		$ReportSelection = $lreportcard->Get_Report_Selection($YearID, '', 'ReportID');
		
		### Get Subject selection
		$SubjectSelection = $lreportcard->Get_Subject_Selection($YearID, '', 'SubjectIDArr[]', $ParOnchange, $isMultiple=1);
?>


<link type="text/css" rel="stylesheet" href="<?=$PATH_WRT_ROOT."templates/2007a/css/"?>ereportcard.css">

<br />
<form name="form1" action="generate.php" method="POST" onsubmit="return checkForm();" target="_blank">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	</tr>
<table>
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>
			<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr>
					<td></td>
					<td align="right"><?=$linterface->GET_SYS_MSG($Result);?></td>
				</tr>
				
				<!-- View By -->
				<tr id="ViewByRow">
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?= $eReportCard['ViewBy'] ?>
					</td>
					<td width="75%" class='tabletext'>
						<input type="radio" name="ViewBy" id="ViewByClass" value="class" CHECKED onclick="toggle(this)">
							<label for="ViewHTML"><?=$eReportCard['Class']?></label>
						</input>
						<input type="radio" name="ViewBy" id="ViewBySubjGroup" value="subjgroup" onclick="toggle(this)">
							<label for="ViewCSV"><?=$eReportCard['SubjectGroup']?></label>
						</input>
					</td>
				</tr>
				
				<!-- View Format -->
				<tr id="ViewFormatRow">
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?= $eReportCard['ViewFormat'] ?>
					</td>
					<td width="75%" class='tabletext'>
						<input type="radio" name="ViewFormat" id="ViewHTML" value="html" CHECKED>
							<label for="ViewHTML"><?=$eReportCard['HTML']?></label>
						</input>
						<input type="radio" name="ViewFormat" id="ViewCSV" value="csv">
							<label for="ViewCSV"><?=$eReportCard['CSV']?></label>
						</input>
					</td>
				</tr>
				
				<!-- Form -->
				<tr id="FormSelection">
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?= $eReportCard['FormName'] ?>
					</td>
					<td width="75%" class='tabletext'>
						<?= $FormSelection ?>
					</td>
				</tr>
				
				<!-- Class Selection -->
				<tr class="tabletext" id="ClassSelection">
					<td class="formfieldtitle" valign="top"><?=$eReportCard['Class']?> <span class="tabletextrequire">*</span></td>
					<td>
						<span id="ClassSelectionSpan">
							<?=$ClassSelection?>
						</span>
						<span style="vertical-align:bottom">
							<?= $linterface->GET_SMALL_BTN($button_select_all, "button", "javascript:SelectAll(document.form1.elements['YearClassIDArr[]'])"); ?>
						</span>
					</td>
				</tr>
				
				<!-- Report -->
				<tr class="tabletext">
					<td class="formfieldtitle" valign="top" width="30%"><?=$eReportCard['ReportCard']?> <span class="tabletextrequire">*</span></td>
					<td>
						<div id="ReportSelectionDiv">
							<?= $ReportSelection ?>
						</div>
					</td>
				</tr>
				
				<!-- Subject Selection -->
				<tr class="tabletext">
					<td class="formfieldtitle" valign="top"><?=$eReportCard['Subject']?> <span class="tabletextrequire">*</span></td>
					<td>
						<span id="SubjectSelectionSpan">
							<?=$SubjectSelection?>
						</span>
						<span style="vertical-align:bottom">
							<?= $linterface->GET_SMALL_BTN($button_select_all, "button", "javascript:SelectAll(document.form1.elements['SubjectIDArr[]'])"); ?>
						</span>
					</td>
				</tr>
				
				<? if ($eRCTemplateSetting['SubjectSummarySheet']['PassingNumberDetermineByPercentage']) { ?>
				<tr class="tabletext">
					<td class="formfieldtitle" valign="top"><?=$eReportCard['SubjectSummarySheet']['SpecifiedPassingPercentage']?></td>
					<td>
						<input id="SpecifiedPassingPercentageTb" name="SpecifiedPassingPercentage" class="textboxnum" value="<?=$lreportcard->SpecifiedPassingPercentage?>" /><?=$eReportCard['SubjectSummarySheet']['%_OfFullMark']?>
					</td>
				</tr>
				<? } ?>
				
				
				<tr>
					<td colspan="2" class="dotline">
						<img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1">
					</td>
				</tr>
				<tr>
					<td colspan="2" align="center">
						<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "", "submitBtn"); ?>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</form>


<script language="javascript">
function toggle(obj)
{
	if(obj.value == "class")
	{
		$("#ClassSelection").show();
	}	
	else
	{
		$("#ClassSelection").hide();
	}
}

function SelectAll(obj)
{
     for (i=0; i<obj.length; i++)
     {
          obj.options[i].selected = true;
     }
}

function js_Reload_Selection(jsYearID)
{
	$('#ReportSelectionDiv').hide();
	$('#ClassSelectionSpan').hide();
	$('#SubjectSelectionSpan').hide();
	
	$('#ReportSelectionDiv').load(
		"../ajax_reload_selection.php", 
		{ 
			RecordType: 'Report',
			YearID: jsYearID,
			ReportID: '',
			SelectionID: 'ReportID'
		},
		function(ReturnData)
		{
			$('#ReportSelectionDiv').show();
			//$('#IndexDebugArea').html(ReturnData);
		}
	);
	
	$('#ClassSelectionSpan').load(
		"../ajax_reload_selection.php", 
		{ 
			RecordType: 'Class',
			YearID: jsYearID,
			SelectionID: 'YearClassIDArr[]',
			isMultiple: 1
		},
		function(ReturnData)
		{
			SelectAll(document.form1.elements['YearClassIDArr[]']);
			$('#ClassSelectionSpan').show();
			//$('#IndexDebugArea').html(ReturnData);
		}
	);
	
	$('#SubjectSelectionSpan').load(
		"../ajax_reload_selection.php", 
		{ 
			RecordType: 'Subject',
			YearID: jsYearID,
			SelectionID: 'SubjectIDArr[]'
		},
		function(ReturnData)
		{
			SelectAll(document.form1.elements['SubjectIDArr[]']);
			$('#SubjectSelectionSpan').show();
			//$('#IndexDebugArea').html(ReturnData);
		}
	);
}

function checkForm() 
{
	var obj = document.form1;
	var select_obj = obj.elements['YearClassIDArr[]'];
	var hasSelectedClass = false;
	for (var i=0; i<select_obj.length; i++)
	{
		if (select_obj[i].selected)
		{
			hasSelectedClass = true;
		}
	}

	if (hasSelectedClass == false)
	{
		alert('<?=$eReportCard["jsWarningSelectClass"]?>');
		obj.elements['YearClassIDArr[]'].focus();
		return false;
	}
	
	
	var select_obj = obj.elements['SubjectIDArr[]'];
	var hasSelectedSubject = false;
	for (var i=0; i<select_obj.length; i++)
	{
		if (select_obj[i].selected)
		{
			hasSelectedSubject = true;
		}
	}

	if (hasSelectedSubject == false)
	{
		alert('<?=$eReportCard["jsSelectSubjectWarning"]?>');
		obj.elements['SubjectIDArr[]'].focus();
		return false;
	}
	
	return true;
}


SelectAll(document.form1.elements['SubjectIDArr[]']);
SelectAll(document.form1.elements['YearClassIDArr[]']);

</script>


<?
	print $linterface->FOCUS_ON_LOAD("form1.ClassLevelID");
  	$linterface->LAYOUT_STOP();
  } else {
?>
You have no priviledge to access this page.
<?
	}
} else {
?>

<?
}
?>
