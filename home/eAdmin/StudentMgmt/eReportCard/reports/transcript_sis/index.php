<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($intranet_root."/lang/reportcard2008_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/reportcard_custom/sis.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable{$LAYOUT_SKIN}.php");
# PMS SIS 2007a
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
$NoLangWordings = true;
intranet_auth();
intranet_opendb();

### set cookies
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

if ($RecordStatus == "") $RecordStatus = 1;
if ($RecordStatus != 0 && $RecordStatus != 2 && $RecordStatus != 3) $RecordStatus = 1;

if (!isset($order)) $order = 0;
if (!isset($field)) $field = 0;
$order = ($order == 1) ? 1 : 0;


if (!$plugin['ReportCard2008'])
{
	header ("Location: /");
	intranet_closedb();
	exit();
}
 
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
$lreportcard = new libreportcard();

if (!$lreportcard->hasAccessRight()) 
{
	header ("Location: /");
	intranet_closedb();
	exit();
}
//echo "action ".$action."<br/>";
if($action == "search"){
//	searchStudentDetails($studentName , $admissionNo);

if($admissionNo != "")
{
	//$extraCriteria .= ($extraCriteria == "") ?  " AND iau.AdmissionNo like '%".$admissno."%'" : " psw iu.AdmissionNo like '%".$admissno."%'";
	$extraCriteria .= " AND m.AdmissionNo like '%".$admissionNo."%' ";
}

if($studentName != "")
{
	$extraCriteria .= " AND (iau.EnglishName like '%".$studentName."%' or iu.EnglishName like '%".$studentName."%')";
}
	$sql = "select 
				concat('<a href = \"print.php?StudentID=',m.UserID, '\" target=\"_blank\"> ',m.AdmissionNo,'</a>') as 'AdmissionNo',
				if(iu.AdmissionNo IS NOT NULL,if(iu.EnglishName IS NOT NULL ,iu.EnglishName,'--'),if(iau.EnglishName IS NOT NULL,iau.EnglishName,'--')) as 'engName',
				if(iu.AdmissionNo IS NOT NULL,if(iu.ChineseName IS NULL or trim(iu.ChineseName ) = '','--',iu.ChineseName),if(iau.ChineseName IS NULL or trim(iau.ChineseName) = '','--',iau.ChineseName)) as 'chiName'

			from 
				$pms_db.PMS_STUDENT_MASTER as m
			left join INTRANET_USER as iu on m.USERID = iu.USERID
			left join INTRANET_ARCHIVE_USER as iau on m.USERID = iau.USERID
			where 
			    1
				$extraCriteria			
	";

//	echo $sql."<br/>";



$li = new libdbtable2007($field, $order, $pageNo);
	$li->field_array = array("AdmissionNo" , "engName","chiName");
	$li->sql = $sql;
	$li->no_col = sizeof($li->field_array)+1;
	$li->title = "";
	$li->column_array = array(0,0,0,0);
	$li->wrap_array = array(0,0,0,0);
	$li->IsColOff = 2;
//hdebug($li->built_sql());
	$pos = 0;
	$li->column_list .= "<td width='1' class='tablegreytop tabletoplink'>#</td>\n";
	$li->column_list .= "<td width='33%' class='tablegreytop tabletoplink'>".$li->column($pos++, $eReportCard['TranscriptSIS']['AdmNo'])."</td>\n";
	$li->column_list .= "<td width='33%' class='tablegreytop tabletoplink'>".$li->column($pos++, $eReportCard['TranscriptSIS']['EngName'])."</td>\n";
	$li->column_list .= "<td width='33%' class='tablegreytop tabletoplink'>".$li->column($pos++, $eReportCard['TranscriptSIS']['ChiName'])."</td>\n";


	

}

$linterface = new interface_html();

$CurrentPage = "Reports_Transcript";
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();

# tag information
$TAGS_OBJ[] = array($eReportCard['Reports_Transcript'], "", 0);
$linterface->LAYOUT_START();

/*
### Form Selection
$ClassLevelArr = $lreportcard->GET_ALL_FORMS();
$ClassLevelSelection = "<select name=\"ClassLevelID\" class=\"tabletext\" id=\"ClassLevelID\" onChange=\"changeClassLevel(this.value)\">\n";
foreach($ClassLevelArr as $key => $value){
	$ClassLevelSelection .="<option value=\"".$value['ClassLevelID']."\">".$value['LevelName']."</option>\n";
}
$ClassLevelSelection .= "</select>\n";
*/
/*
### Class Selection
$defaultClassLevelID = $ClassLevelArr[0]['ClassLevelID'];
$ClassSelection = $lreportcard->Get_Class_Selection('ClassID', $defaultClassLevelID, '', 'changeClass(this.value)');

### Student Multiple Selection
$StudentSelection = $lreportcard->Get_Student_Selection('StudentIDArr[]', $defaultClassLevelID);
	
### Select All Btn
$SelectAllBtn = $linterface->GET_BTN($button_select_all, "button", "jsSelectAllOption('StudentIDArr[]', true); return false;");
*/
### Submit Btn
$SubmitBtn = $linterface->GET_ACTION_BTN($button_submit, "submit", "", "submitBtn");


?>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery-1.3.2.min.js"></script>
<script language="javascript">
function changeClassLevel(jsClassLevelID)
{
	// load class selection
	$('#ClassSelectionSpan').html('Loading...').load(
		"../ajax_reload_selection.php", 
		{ 
			RecordType: 'Class',
			ClassLevelID: jsClassLevelID,
			SelectionID: 'ClassID',
			OnChange: 'changeClass(this.value)'
		},
		function(ReturnData)
		{
			//$('#IndexDebugArea').html(ReturnData);
		}
	);
	
	// load student selection
	reloadStudentSelection(jsClassLevelID, '');
}

function changeClass(jsClassID)
{
	var jsClassLevelID = $('select#ClassLevelID').val();
	reloadStudentSelection(jsClassLevelID, jsClassID);
}


function reloadStudentSelection(jsClassLevelID, jsClassID)
{
	$('#StudentSelectionSpan').html('Loading...').load(
		"../ajax_reload_selection.php", 
		{ 
			RecordType: 'Student',
			ClassLevelID: jsClassLevelID,
			ClassID: jsClassID,
			SelectionID: 'StudentIDArr[]'
		},
		function(ReturnData)
		{
			//$('#IndexDebugArea').html(ReturnData);
		}
	);
}

function jsCheckForm()
{

//	var StudentSelectionObj = document.getElementById('StudentIDArr[]');

/*	
	if (countOption(StudentSelectionObj) == 0)
	{
		alert("<?=$eReportCard['jsSelectAStudent']?>");
		return false;
	}
*/	

	return true;

}
</script>

<br />	
<form id="form1" name="form1" method="POST"  action = "index.php" onSubmit="return jsCheckForm()">

	<table width="96%" border="0" cellspacing="0" cellpadding="5">
	  <tr>
	    <td align="center" >
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
			<tr><td class="formfieldtitle" ><span class="tabletext"><?=$eReportCard['TranscriptSIS']['StudentEnglishName']?> : </span></td><td><input type = "text" name = "studentName" value="<?=$studentName?>" /></td></tr>
			<tr><td class="formfieldtitle" ><span class="tabletext"><?=$eReportCard['TranscriptSIS']['AdmissionNumber']?> : </span></td><td><input type = "text" name = "admissionNo" value="<?=$admissionNo?>" /></td></tr>
			<tr><td colspan="2" height="1" class="dotline"><br/><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
			<tr><td colspan="2" align="center"><br/>
					<input type = "hidden" name="action" value="search">
					<?= $SubmitBtn ?>
				</td></tr>
			</table>
		</td>
	  </tr>
	 </table>
	 <table width="96%" border="0" cellspacing="0" cellpadding="5">
<tr><td align="center">
		<?php 
		if($action == "search"){
			echo $li->displayFormat2("100%"); 
		}
		?>&nbsp;
		<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>">
<input type="hidden" name="order" value="<?php echo $li->order; ?>">
<input type="hidden" name="field" value="<?php echo $li->field; ?>">
<input type="hidden" name="page_size_change" value="">
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>">
</td></tr>
</table>

</form>
<br />                        
                        
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
