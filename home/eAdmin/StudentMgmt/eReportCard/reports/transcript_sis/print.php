<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "TEACHER";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
//include_once($portal_filepath."/intranetIP/lang/iportfolio_lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

if (!$plugin['ReportCard2008'])
{
	header ("Location: /");
	intranet_closedb();
	exit();
}

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/sis.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008w.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_sis.php");
//$lreportcard = new libreportcardSIS();
$lreportcard = new libreportcardcustom();
$linterface = new interface_html();

# For NAPFA Preview
include_once($PATH_WRT_ROOT."includes/libPMS.php");
include_once($PATH_WRT_ROOT."includes/config.inc.php");
//include_once($PATH_WRT_ROOT."includes/libHolistic.php");
//include_once($PATH_WRT_ROOT."includes/libstudent.php"); move to sis.php

$objDB = new libdb();
$objHolistic = new libHolistic();
//$objHolistic->setCurrentYear(2009);
// from SIS PMS LibPMS.php 
function getFitnessStandardGrade()
{
	global $objDB,$pms_db;
	
	$sql = "select distinct Grade from $pms_db.PMS_FITNESS_STANDARDS order by Grade";	
	$result = $objDB->returnArray($sql);
	$sGradeArr = array();
	for($i=0; $i < sizeof($result); $i++)
	{
		$sGrade = $result[$i]['Grade'];
		$sGradeArr[] = $sGrade;		
	}
	
	return $sGradeArr;
}

$sGradeArr = getFitnessStandardGrade();

if (!$lreportcard->hasAccessRight()) 
{
	header ("Location: /");
	intranet_closedb();
	exit();
}

# Get the eRC Template Settings
include_once($PATH_WRT_ROOT."includes/eRCConfig.php");
//$eRCtemplate = $lreportcard->getCusTemplate();
$eRtemplateCSS = $lreportcard->getTemplateCSS();

$ReportSetting = $lreportcard->returnReportTemplateBasicInfo($ReportID);
$ClassLevelID = $ReportSetting['ClassLevelID'];

include_once($PATH_WRT_ROOT."templates/2007a/layout/print_header.php");
?>


<? if($eRtemplateCSS) {?><link href="<?=$eRtemplateCSS?>" rel="stylesheet" type="text/css" /><? } ?>

<style type='text/css' media='print'>
 .print_hide {display:none;}
</style>
<style>
 /*copy this css form /home/eclass/intranetIP/templates/2007a/css/iportfolio/sis.css*/
 .result_header{
	font-weight: bold;
	font-size: 11px;
	text-decoration: underline;
 }

 .result_col_label, .signature_label {
font-weight: bold;
font-size: 11px;
}
/*copy this css form /home/eclass/intranetIP/templates/2007a/css/iportfolio/sis.css*/
.result_mark {
font-size: 11px;
}

/*copy this css form /home/eclass/intranetIP/templates/2007a/css/iportfolio/sis.css*/
tr.bottom_dot_border td, .dot_bottom_border {
border-bottom:thin dotted black;
}
</style>
<table border="0" cellpadding="4" width="100%" cellspacing="0" class="print_hide">
<tr><td align="right"><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></span></td></tr>
</table>
<?
//echo $StudentID."<br/>";

//for testing now , support with two mode (query from index.php / index2.php)
if($StudentID != ""){
	//from index2.php
	$StudentIDArr = array($StudentID);
}else{
	//from index.php
	$StudentIDArr = $_POST['StudentIDArr'];
}


//$StudentIDAry = $lreportcard->GET_STUDENT_BY_CLASSLEVEL($ReportID, $ClassLevelID, $ClassID, $TargetStudentList);

for($s=0;$s<sizeof($StudentIDArr);$s++)
{
	$StudentID = $StudentIDArr[$s];
	$ReportHTML = $lreportcard->Get_Student_Transcript_HTML($StudentID);
	//$thisPageBreak = ($s==sizeof($StudentIDAry)-1)? '' : 'style="page-break-after:always"';
	
	$x = '';
	$h  = ''; // for holistic_reportcard	
	if ($s==0)
		$x .= '<br />'."\n";
		
	$x .= '<div id="container_transcript" valign="top">';
		//$x .= ($s==0)? '<br />' : '';
		//$x .= '<table valign="top" align="center" width="100%" border="0" cellpadding="0" cellspacing="0" '.$thisPageBreak.'>';
		//	$x .= '<tr><td>'.$ReportHTML.'</td></tr>';
		//$x .= '</table>';
		$x .= $ReportHTML;
	$x .= '</div>';
	
	echo $x;

	$TitleTable = $objHolistic->getReportHeader();
	$StudentInfoTable = $objHolistic->getReportStudentInfoForTranscript($StudentID);
	$MSTable = $objHolistic->getTranscriptDetailsTable($StudentID);
	
	$TopTable = "";
	$TopTable .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" valign='top' align='center' >\n";
	$TopTable .= "<tr valign='top'><td>".$TitleTable."</td></tr>\n";
	$TopTable .= "<tr valign='top'><td>".$StudentInfoTable."</td></tr>\n";
	$TopTable .= "<tr valign='top'><td>".$MSTable."</td></tr>\n";
	$TopTable .= "</table>\n";
	

	$h   = "<div id=\"container\" valign=\"top\">";
	$h  .= 	"<table valign=\"top\" width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"page-break-after:always\">";
	$h  .= 	"<tr height='570px'><td valign='top' align='center'>{$TopTable}</td><tr>";
	$h  .= 	"</table>";
	$h  .= 	"</div>";
	echo $h;
}
?>
<table border="0" cellpadding="4" width="100%" cellspacing="0" class="print_hide">
<tr><td align="right"><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></span></td></tr>
</table>

