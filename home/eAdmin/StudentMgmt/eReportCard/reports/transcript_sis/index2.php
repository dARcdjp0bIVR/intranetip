<?php
$PATH_WRT_ROOT = "../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if (!$plugin['ReportCard'])
{
	header ("Location: /");
	intranet_closedb();
	exit();
}
 
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
$lreportcard = new libreportcard();

if (!$lreportcard->hasAccessRight()) 
{
	header ("Location: /");
	intranet_closedb();
	exit();
}

$linterface = new interface_html();

$CurrentPage = "Reports_Transcript";
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();

# tag information
$TAGS_OBJ[] = array($eReportCard['Reports_Transcript'], "", 0);
$linterface->LAYOUT_START();


### Form Selection
$ClassLevelArr = $lreportcard->GET_ALL_FORMS();
$ClassLevelSelection = "<select name=\"ClassLevelID\" class=\"tabletext\" id=\"ClassLevelID\" onChange=\"changeClassLevel(this.value)\">\n";
foreach($ClassLevelArr as $key => $value){
	$ClassLevelSelection .="<option value=\"".$value['ClassLevelID']."\">".$value['LevelName']."</option>\n";
}
$ClassLevelSelection .= "</select>\n";

### Class Selection
$defaultClassLevelID = $ClassLevelArr[0]['ClassLevelID'];
$ClassSelection = $lreportcard->Get_Class_Selection('ClassID', $defaultClassLevelID, '', 'changeClass(this.value)');

### Student Multiple Selection
$StudentSelection = $lreportcard->Get_Student_Selection('StudentIDArr[]', $defaultClassLevelID);
	
### Select All Btn
$SelectAllBtn = $linterface->GET_BTN($button_select_all, "button", "jsSelectAllOption('StudentIDArr[]', true); return false;");

### Submit Btn
$SubmitBtn = $linterface->GET_ACTION_BTN($button_submit, "button", "jsCheckForm();", "submitBtn");


?>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery-1.3.2.min.js"></script>
<script language="javascript">
function changeClassLevel(jsClassLevelID)
{
	// load class selection
	$('#ClassSelectionSpan').html('Loading...').load(
		"../ajax_reload_selection.php", 
		{ 
			RecordType: 'Class',
			ClassLevelID: jsClassLevelID,
			SelectionID: 'ClassID',
			OnChange: 'changeClass(this.value)'
		},
		function(ReturnData)
		{
			//$('#IndexDebugArea').html(ReturnData);
		}
	);
	
	// load student selection
	reloadStudentSelection(jsClassLevelID, '');
}

function changeClass(jsClassID)
{
	var jsClassLevelID = $('select#ClassLevelID').val();
	reloadStudentSelection(jsClassLevelID, jsClassID);
}


function reloadStudentSelection(jsClassLevelID, jsClassID)
{
	$('#StudentSelectionSpan').html('Loading...').load(
		"../ajax_reload_selection.php", 
		{ 
			RecordType: 'Student',
			ClassLevelID: jsClassLevelID,
			ClassID: jsClassID,
			SelectionID: 'StudentIDArr[]'
		},
		function(ReturnData)
		{
			//$('#IndexDebugArea').html(ReturnData);
		}
	);
}

function jsCheckForm()
{
	var StudentSelectionObj = document.getElementById('StudentIDArr[]');
	
	if (countOption(StudentSelectionObj) == 0)
	{
		alert("<?=$eReportCard['jsSelectAStudent']?>");
		return false;
	}
	
	var ObjForm = document.getElementById('form1');
	ObjForm.action = "print.php";
	ObjForm.target = "_blank";
	ObjForm.submit();
}
</script>

<br />	
<form id="form1" name="form1" method="POST">					
	<table width="96%" border="0" cellspacing="0" cellpadding="5">
	  <tr> 
	    <td align="center" colspan="2">
	    	<table width="100%" border="0" cellspacing="0" cellpadding="2">
		      <tr>
		        <td class="formfieldtitle" width="30%" nowrap valign="top"><span class="tabletext">Class Level : </span></td> 
			    <td nowrap class="tabletext"><span class="tabletext"><?=$ClassLevelSelection?></span></td>
		      </tr>
		      
		      <tr>
		        <td class="formfieldtitle" width="30%" nowrap valign="top"><span class="tabletext">Class : </span></td> 
			    <td nowrap class="tabletext"><span class="tabletext" id="ClassSelectionSpan"><?=$ClassSelection?></span></td>
			  </tr>
		      
		      <tr>
		        <td class="formfieldtitle" width="30%" nowrap valign="top"><span class="tabletext">Student : </span></td> 
			    <td nowrap class="tabletext"><span class="tabletext" id="StudentSelectionSpan"><?=$StudentSelection?></span><?=$SelectAllBtn?></td>
			  </tr>
			  
			  <tr><td colspan="2" align="center">&nbsp;</td></tr>
		      
		      <tr>
		        <td colspan="2" height="1" class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
		      </tr>
		      <tr><td colspan="2" align="center"><?= $SubmitBtn ?></td></tr>
	    	</table>
	    </td>
	  </tr>
	</table>
</form>
<br />                        
                        
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
