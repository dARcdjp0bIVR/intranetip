<?php
// Using:

################## Change Log [Start] ##############
#
#   Date:   2019-06-28  Bill
#           Term Selection > Change 'TermID' to 'TargetTerm' to prevent IntegerSafe()
#
################## Change Log [End] ##############

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if ($plugin['ReportCard2008']) {
	include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
	include_once($PATH_WRT_ROOT."includes/libclass.php");
	include_once($PATH_WRT_ROOT."includes/libinterface.php");
	
	if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
	{
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
		$lreportcard = new libreportcardcustom();
	} else {
		$lreportcard = new libreportcard();
	}
	$linterface = new interface_html();
	$lclass = new libclass();
	
	if ($lreportcard->hasAccessRight()) {
		
		# Get Levels and Classes
		/*
		$levels = $lclass->getLevelArray();
		$classes = $lclass->getClassList($lreportcard->schoolYearID);
		
		$select_list = "<SELECT name=\"TargetID[]\" MULTIPLE SIZE=\"10\">";
		if ($level==1)
		{
		    for ($i=0; $i<sizeof($levels); $i++)
		    {
			    list($id,$name) = $levels[$i];
				$select_list .= "<OPTION value='".$id."' SELECTED>".$name."</OPTION>";
		    }
		}
		else
		{
		    for ($i=0; $i<sizeof($classes); $i++)
		    {
		         list($id, $name, $lvl) = $classes[$i];
				$select_list .= "<OPTION value='".$id."' SELECTED>".$name."</OPTION>";
		    }
		}
		$select_list .= "</SELECT>";
		*/
		
		# Term Selection
		$TermInfoArr = $lreportcard->returnReportTemplateTerms();
		$TermSel_tag = 'name="TargetTerm" id="TargetTerm" onchange="js_Changed_Term_Selection(this.value);"';
		$TermSelection = getSelectByArray($TermInfoArr, $TermSel_tag, "", $all=0, $noFirst=1);
		
		# Number of Prize Selection
		$PrizeNumArr = array();
		for ($i=1; $i<=10; $i++)
			$PrizeNumArr[] = array($i, $i);
		$PrizeNumSelection_tag = 'name="PrizeNum" id="PrizeNum"';
		$PrizeNumSelection = getSelectByArray($PrizeNumArr, $PrizeNumSelection_tag, 1, $all=0, $noFirst=1);
		
		
		# main or extra term report radio button
		$htmlAry['testRadio'] = $linterface->Get_Radio_Button('reportType_extra', 'reportType', $Value='extra', $isChecked=1, $Class="", $Lang['eReportCard']['Test']);
		$htmlAry['examRadio'] = $linterface->Get_Radio_Button('reportType_main', 'reportType', $Value='main', $isChecked=0, $Class="", $Lang['eReportCard']['Exam']);
		
		
		############## Interface Start ##############
		$linterface = new interface_html();
		$CurrentPage = "Reports_SubjectPrizeReport";
		$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
		# tag information
		$TAGS_OBJ[] = array($eReportCard['Reports_SubjectPrizeReport']);
		$linterface->LAYOUT_START();
?>


<script language="javascript">
var jsCurTermID;
var jsCurLevel;

$(document).ready(function () {
	jsCurTermID = $('select#TargetTerm').val();
	jsCurLevel = $('select#Level').val();
	
	js_Reload_Class_Selection();
});

function js_Changed_Term_Selection(jsTermID)
{
	if (jsTermID == 'F' && jsCurLevel=='SubjectGroup')
	{
		alert('<?=$eReportCard['ReportArr']['SubjectPrize']['jsWarningArr']['SubjectGroupRankingMustSelectATerm']?>');
		$('select#TargetTerm').val(jsCurTermID);
		return false;
	}
	
	jsCurTermID = jsTermID;
}

function changeType(form_obj,lvl_value)
{
	if (jsCurTermID == 'F' && lvl_value=='SubjectGroup')
	{
		alert('<?=$eReportCard['ReportArr']['SubjectPrize']['jsWarningArr']['SubjectGroupRankingMustSelectATerm']?>');
		$('select#Level').val(jsCurLevel);
		return false;
	}
	
	if (lvl_value=='SubjectGroup')
	{
		js_Reload_Form_Selection(1);
		js_Reload_Subject_Selection();
	}
	else
	{
		$('span#TargetFormSelectionSpan').html('');
		$('span#TargetSubjectGroupSelectionSpan').html('');
		
		if (lvl_value=='Form')
		{
			js_Reload_Form_Selection();
		}
		else if (lvl_value=='Class')
		{
			js_Reload_Class_Selection();
		}
	}
	
	jsCurLevel = lvl_value;
}

function js_Changed_Subject_Selection()
{
	js_Reload_Subject_Group_Selection();
} 

function js_Reload_Subject_Selection()
{
	$('span#TargetSourceSelectionSpan').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"../../ajax_reload.php", 
		{ 
			Action: 'Subject_Selection',
			SelectionID: 'SubjectIDArr[]',
			SubjectID: '',
			YearTermID: jsCurTermID,
			OnChange: 'js_Changed_Subject_Selection();',
			IsMultiple: 1,
			NoFirst: 1
		},
		function(ReturnData)
		{
			SelectAll(document.getElementById('SubjectIDArr[]'));
			js_Reload_Subject_Group_Selection();
		}
	);
}

function js_Reload_Subject_Group_Selection()
{
	var form_obj = document.form1;
	
	$('span#TargetSubjectGroupSelectionSpan').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"../../ajax_reload.php", 
		{ 
			Action: 'Subject_Group_Selection',
			SelectionID: 'TargetID[]',
			SubjectIDList: $('select#SubjectIDArr\\[\\]').val().toString(),
			YearTermID: jsCurTermID,
			IsMultiple: 1
		},
		function(ReturnData)
		{
			SelectAll(document.getElementById('TargetID[]'));
		}
	);
}

function js_Reload_Class_Selection()
{
	var form_obj = document.form1;
	
	$('span#TargetSourceSelectionSpan').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"../../ajax_reload.php", 
		{ 
			Action: 'Class_Selection',
			SelectionID: 'TargetID[]',
			NoFirst: 1,
			IsMultiple: 1
		},
		function(ReturnData)
		{
			SelectAll(document.getElementById('TargetID[]'));
		}
	);
}

function js_Reload_Form_Selection(jsForSubjectGroup)
{
	jsForSubjectGroup = jsForSubjectGroup || 0;
	
	var jsTargetSpan, jsSelectionID, jsOnchange;
	
	if (jsForSubjectGroup == 1)
	{
		jsTargetSpan = 'TargetFormSelectionSpan';
		jsSelectionID = 'YearIDArr[]';
		jsOnchange = 'js_Reload_Subject_Group_Selection();';
	}
	else
	{
		jsTargetSpan = 'TargetSourceSelectionSpan';
		jsSelectionID = 'TargetID[]';
		jsOnchange = '';
	}
	
	var form_obj = document.form1;
	$('span#' + jsTargetSpan).html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"../../ajax_reload.php", 
		{ 
			Action: 'Form_Selection',
			SelectionID: jsSelectionID,
			Onchange: jsOnchange,
			NoFirst: 1,
			IsMultiple: 1
		},
		function(ReturnData)
		{
			SelectAll(document.getElementById(jsSelectionID));
		}
	);
}

function SelectAll(obj)
{
     for (i=0; i<obj.length; i++)
     {
          obj.options[i].selected = true;
     }
}

function checkForm() 
{
	var obj = document.form1;
	var select_obj = obj.elements['TargetID[]'];

	for (var i=0; i<select_obj.length; i++)
	{
		if (select_obj[i].selected)
		{
			return true;
		}
	}

	alert('<?=$i_alert_PleaseSelectClassForm?>');
	return false;
}
</script>

<link type="text/css" rel="stylesheet" href="<?=$PATH_WRT_ROOT."templates/2007a/css/"?>ereportcard.css">

<br />
<form name="form1" action="generate.php" method="POST" onsubmit="return checkForm();" target="_blank">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	</tr>
<table>
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>
			<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr>
					<td></td>
					<td align="right"><?=$linterface->GET_SYS_MSG($Result);?></td>
				</tr>
				<tr id="ViewFormatRow">
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?= $eReportCard['ViewFormat'] ?>
					</td>
					<td width="75%" class='tabletext'>
						<input type="radio" name="ViewFormat" id="ViewHTML" value="html" CHECKED>
							<label for="ViewHTML"><?=$eReportCard['HTML']?></label>
						</input>
						<input type="radio" name="ViewFormat" id="ViewCSV" value="csv">
							<label for="ViewCSV"><?=$eReportCard['CSV']?></label>
						</input>
					</td>
				</tr>
				
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?= $eReportCard['ReportArr']['SubjectPrize']['NumOfPrize'] ?>
					</td>
					<td width="75%" class='tabletext'>
						<?= $PrizeNumSelection ?>
					</td>
				</tr>
				
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?= $eReportCard['Term'] ?>
					</td>
					<td width="75%" class='tabletext'>
						<?= $TermSelection ?>
						<?php if ($eRCTemplateSetting['Report']['SubjectPrize']['MainOrExtraReportOption']) { ?>
							<br>
							<?=$htmlAry['testRadio']?>
							<?=$htmlAry['examRadio']?>
						<?php } ?>
					</td>
				</tr>
				
				<tr class="tabletext">
					<td class="formfieldtitle" valign="top" width="30%"><?=$eReportCard['GeneralArr']['Ranking']?></td>
					<td>
						<SELECT id="Level" name="Level" onChange="changeType(this.form, this.value)">
							<OPTION value="Class" selected><?=$Lang['SysMgr']['FormClassMapping']['Class']?></OPTION>
							<OPTION value="Form"><?=$Lang['SysMgr']['FormClassMapping']['Form']?></OPTION>
							<OPTION value="SubjectGroup"><?=$Lang['SysMgr']['SubjectClassMapping']['SubjectGroupTitle']?></OPTION>
						</select>
					</td>
				</tr>
				<tr class="tabletext">
					<td class="formfieldtitle" valign="top"><?=$eReportCard['Target']?> <span class="tabletextrequire">*</span></td>
					<td>
						<span id="TargetFormSelectionSpan"></span>
						<span id="TargetSourceSelectionSpan"><?=$select_list?></span>
						<span id="TargetSubjectGroupSelectionSpan"></span>
						<span><?= $linterface->GET_SMALL_BTN($button_select_all, "button", "javascript:SelectAll(document.form1.elements['TargetID[]'])"); ?></span>
					</td>
				</tr>
				

				<tr>
					<td colspan="2" class="dotline">
						<img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1">
					</td>
				</tr>
				<tr>
					<td colspan="2" align="center">
						<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "", "submitBtn"); ?>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</form>
<?
	print $linterface->FOCUS_ON_LOAD("form1.ClassLevelID");
  	$linterface->LAYOUT_STOP();
  } else {
?>
You have no priviledge to access this page.
<?
	}
} else {
?>

<?
}
?>
