<?php
// using : 

/***********************************************
 * modification log
 *  20200622 Bill:  [2020-0515-1423-31164]
 *      Add 2 preset text input settings
 * 	20171211 Bill:
 * 		Create File
 * ***********************************************/

$PageRight = "ADMIN";

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libteaching.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if ($plugin["ReportCard2008"])
{
	include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
	include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
	if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") {
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
	}
	
	# Initial
	if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") {
		$lreportcard = new libreportcardcustom();
	}
	else {
		$lreportcard = new libreportcard();
	}
	
	if ($lreportcard->hasAccessRight() && $eRCTemplateSetting['Report']['FormTeacherCommentReport'])
	{
		# Current Page
		$CurrentPage = "Reports_FormTeacherCommentReport";
		
		$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
		$TAGS_OBJ[] = array($eReportCard["Reports_FormTeacherCommentReport"]);
		
		$linterface = new interface_html();
		$linterface->LAYOUT_START();
		
		# Get all Forms
		$FormArr = $lreportcard->GET_ALL_FORMS(1);
		if(count($FormArr) > 0)
		{
			# Filters - By Form
			$FormSelection = $linterface->GET_SELECTION_BOX($FormArr, "name='ClassLevelID' id='ClassLevelID' onchange='reloadFormSelection(this.options[this.selectedIndex].value)'", "", $FormArr[0][0]);
			
			# Filters - By Class
			for($i=0; $i<sizeof($FormArr); $i++)
			{
				$ClassArr = $lreportcard->GET_CLASSES_BY_FORM($FormArr[$i][0]);
				$OtherClassSelection[$FormArr[$i][0]] = $linterface->GET_SELECTION_BOX($ClassArr, "id='ClassID' name='ClassID[]' onchange='js_Reload_Student_Selection()' multiple size=6", "", $ClassArr[0][0]);
			}
			$SelectAllClassBtn = $linterface->GET_SMALL_BTN($button_select_all, "button", "js_Select_All('ClassID', 1); js_Reload_Student_Selection(); return false;");
			
			# Filters - By Term
			$TermArr = $lreportcard->returnReportTemplateTerms(1);
			$TermSelection = $linterface->GET_SELECTION_BOX($TermArr, "id='TermID' name='TermID[]' multiple size=6", "");
			$SelectAllTermBtn = $linterface->GET_SMALL_BTN($button_select_all, "button", "js_Select_All('TermID', 1); return false;");
		}

		# [2020-0515-1423-31164] Get Preset Settings
        $yearlyTheme = '';
        $themeVerse = '';
        $presetSetting = $lreportcard->Get_Report_Preset_Setting('FormTeacherCommentReport');
        if(!empty($presetSetting) && isset($presetSetting[0]['PresetValue']))
        {
            $yearlyTheme = $presetSetting[0]['PresetValue']['yearlyTheme'];
            $themeVerse = $presetSetting[0]['PresetValue']['themeVerse'];
        }
?>

<script type="text/JavaScript" language="JavaScript">
// Preload Class list
var classLevelClassList = new Array();

<?php
if(count($FormArr) > 0) {
	foreach($OtherClassSelection as $key => $value)
	{
		$value = addslashes(str_replace("\n", "", $value));
		$w .= "classLevelClassList[".$key."] = '".$value."';\n";
	}
	echo $w;
}
?>

function reloadFormSelection(classLevelID)
{
	document.getElementById("ClassList").innerHTML = classLevelClassList[classLevelID];
	
	document.getElementById("submitBtn").disabled = false;
	document.getElementById("submitBtn").className = 'formbutton';
	document.getElementById("submitBtn").onmouseover = " this.className = 'formbutton' ";
	document.getElementById("submitBtn").onmouseout = " this.className = 'formbutton' ";
	
	js_Select_All('ClassID', 1);
	js_Reload_Student_Selection();
}

function js_Reload_Student_Selection()
{
	var classIdList = $("select#ClassID").val()
	if (classIdList) {
		classIdList = classIdList.join(',');
	}
	else {
		classIdList = '-1'; 	// no class will be returned
	}
	
	$.post(
		"../ajax_reload_selection.php",
		{
			RecordType: 'StudentByClass',
			SelectionID: 'TargetStudentID[]',
			ClassID: classIdList,
			noFirst: 1,
			isMultiple: 1,
			isAll: 1,
			withSelectAll: 1
		},
		function(ReturnData)
		{
			$("#studentSelDiv").html(ReturnData);
			js_Select_All('TargetStudentID[]', 1);
		}
	)
}

function checkForm()
{
	var FormValid = true;
	$("div.warnMsgDiv").hide();
	
	var classIdList = $('select#ClassID').val();
	var stuSelId = getJQuerySaveId('TargetStudentID[]');
	var stuIdList = $('select#'+stuSelId).val();
	var termIdList = $('select#TermID').val();
	
	if(classIdList == null || classIdList == '')
	{
		FormValid = false;
		$("div#classWarningDiv").show();
	}
	else if(stuIdList == null || stuIdList == '')
	{
		FormValid = false;
		$("div#studentWarningDiv").show();
	}
	else if(termIdList == null || termIdList == '')
	{
		FormValid = false;
		$("div#termWarningDiv").show();
	}
	
	return FormValid;
}

$().ready(function() {
	reloadFormSelection($("#ClassLevelID").val());
	js_Select_All('TermID', 1);
})
</script>

<link type="text/css" rel="stylesheet" href="<?=$PATH_WRT_ROOT."templates/2007a/css/"?>ereportcard.css">
<br />
<form name="form1" action="report.php" method="POST" onsubmit="return checkForm();" target="_blank">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>
			<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
			<?php if(count($FormArr) == 0) { ?>
				<tr>
					<td align="center" colspan="2" class='tabletext'>
						<br /><?= $i_no_record_exists_msg ?><br />
					</td>
				</tr>
			<?php }
			else { ?>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?= $eReportCard["Form"] ?>
					</td>
					<td width="75%" class='tabletext'>
						<?= $FormSelection ?>
					</td>
				</tr>
				<tr id="ClassRow" style="">
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?= $eReportCard["Class"] ?>
					</td>
					<td width="75%" class='tabletext'>
						<span id="ClassList"></span> <?= $SelectAllClassBtn ?>
						<?= $linterface->Get_Form_Warning_Msg("classWarningDiv", $eReportCard["jsWarningSelectClass"], "warnMsgDiv") ?>
					</td>
				</tr>
				<tr id="StudentRow">
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?= $eReportCard["Student"] ?>
					</td>
					<td width="75%" class='tabletext'>
						<div id="studentSelDiv"></div>
						<?= $linterface->Get_Form_Warning_Msg("studentWarningDiv", $eReportCard["jsWarningArr"]["PleaseSelectStudent"], "warnMsgDiv") ?>
					</td>
				</tr>
				<tr id="TermRow">
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?= $eReportCard["Term"] ?>
					</td>
					<td width="75%" class='tabletext'>
						<?= $TermSelection ?> <?= $SelectAllTermBtn ?>
						<?= $linterface->Get_Form_Warning_Msg("termWarningDiv", $eReportCard["jsWarningArr"]["PleaseSelectSemester"], "warnMsgDiv") ?>
					</td>
				</tr>
                <tr>
                    <td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
                        <?= $eReportCard['FormTeacherCommentReport']['YearlyTheme'] ?>
                    </td>
                    <td width="75%" class='tabletext'>
						<textarea name="yearlyTheme" id="yearlyTheme" cols="80" rows="6"><?=$yearlyTheme?></textarea>
                    </td>
                </tr>
                <tr>
                    <td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
                        <?= $eReportCard['FormTeacherCommentReport']['ThemeVerse'] ?>
                    </td>
                    <td width="75%" class='tabletext'>
                        <textarea name="themeVerse" id="themeVerse" cols="80" rows="6"><?=$themeVerse?></textarea>
                    </td>
                </tr>
				<tr>
					<td colspan="2" class="dotline">
						<img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1">
					</td>
				</tr>
				<tr>
					<td colspan="2" align="center">
						<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "", "submitBtn") ?>
					</td>
				</tr>
			<?php } ?>
			</table>
		</td>
	</tr>
</table>
</form>
<?
		print $linterface->FOCUS_ON_LOAD("form1.ClassLevelID");
	  	$linterface->LAYOUT_STOP();
	}
	else {
?>
You have no priviledge to access this page.
<?
	}
}
else {
?>
You have no priviledge to access this page.
<?
}
?>