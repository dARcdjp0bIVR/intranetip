<?php
@SET_TIME_LIMIT(600);
# using: ivan

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if (!$plugin['ReportCard2008'])
{
	header ("Location: /");
	intranet_closedb();
	exit();
}
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! Temp Assign Only !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//$ReportCardCustomSchoolName= "general";
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
{
	if (is_file($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName."_sub_marksheet.php"))
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName."_sub_marksheet.php");
	else
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/general_sub_marksheet.php");
	
	$lreportcard = new libreportcardcustom();
} else {
	$lreportcard = new libreportcard();
}

$linterface = new interface_html();

if (!$lreportcard->hasAccessRight()) 
{
	header ("Location: /");
	intranet_closedb();
	exit();
}

# Get the eRC Template Settings
include_once($PATH_WRT_ROOT."includes/eRCConfig.php");
$eRCtemplate = $lreportcard ;
$eRtemplateCSS = $lreportcard->getSubMSTemplateCSS();

//$ReportSetting = $lreportcard->returnReportTemplateBasicInfo($ReportID);
//$ClassLevelID = $ReportSetting['ClassLevelID'];

include_once($PATH_WRT_ROOT."templates/{$LAYOUT_SKIN}/layout/print_header.php");
?>

<? if($eRtemplateCSS) {?><link href="<?=$eRtemplateCSS?>" rel="stylesheet" type="text/css" /><? } ?>

<style type='text/css' media='print'>
	.print_hide {display:none;}
</style>
<table border="0" cellpadding="4" width="95%" cellspacing="0" class="print_hide">
<tr><td align="right"><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></span></td></tr>
</table>
<br />
<?


//$TargetStudentList = (is_array($TargetStudentID)) ? implode(",", $TargetStudentID) : "";
$StudentIDAry = $lreportcard->GET_STUDENT_BY_CLASSLEVEL($ReportID, $ClassLevelID, $ClassID);

for($s=0;$s<sizeof($StudentIDAry);$s++)
{
	$StudentID = $StudentIDAry[$s]['UserID'];
	
//	if($ReportCardCustomSchoolName != "st_stephen_college")
//	{
//		if ($ReportCardCustomSchoolName == "escola_tong_nam")
//		{
			# Different school title banners (Kindergarten/Primary/Secondary) with different classes
			# So, have to pass studentID as well to determind the use of school title banner
//			$TitleTable = $eRCtemplate->getReportHeader($ReportID, $StudentID);
//		}
//		else
//		{
			$TitleTable = $eRCtemplate->getReportHeader($ReportID);
//		}
		
		$StudentInfoTable = $eRCtemplate->getReportStudentInfo($ReportID, $StudentID, $IssueDate);
		$MSTable = $eRCtemplate->getSubMSTable($ReportID, $SubMarkSheet, $FullMark, $PassMark, $Display, $StudentID);
		//$MiscTable = $eRCtemplate->getMiscTable($ReportID, $StudentID);
		$SignatureTable = $eRCtemplate->getSignatureTable($ReportID, $StudentID, $IssueDate);
		$FooterRow = $eRCtemplate->getFooter($ReportID);
//	}
	
	

	?>
	<div id="container">
		<table width="100%" border="0" cellpadding="02" cellspacing="0" valign="top" <? if($s<sizeof($StudentIDAry)-1) {?>style="page-break-after:always"<? } ?>>
		<?
		/*if($ReportCardCustomSchoolName == "st_stephen_college")
		{
			$x = $eRCtemplate->FirstPage($ReportID, $StudentID);
					
			# Subejct Pages
			//style="page-break-before:always"
			$ReportSetting = $eRCtemplate->returnReportTemplateBasicInfo($ReportID);
			$ClassLevelID = $ReportSetting['ClassLevelID'];
	 		$MainSubjectArray = $eRCtemplate->returnSubjectwOrder($ClassLevelID);
	 		foreach($MainSubjectArray as $SubjectID => $SubejctData)
	 		{
				$x .= $eRCtemplate->SubjectPage($ReportID, $SubjectID, $StudentID);		
	 		}
	
			# OLE Page
			
			echo $x;
		}
		else
		{*/
			if (method_exists($eRCtemplate, "getLayout")) {
				echo $eRCtemplate->getLayout($TitleTable, $StudentInfoTable, $MSTable, $MiscTable, $SignatureTable, $FooterRow, $ReportID, $StudentID);
			} else {
			?>
				<tr><td><?=$TitleTable?></td><tr>
				<tr><td><?=$StudentInfoTable?></td><tr>
				<tr><td><?=$MSTable?></td><tr>
				<tr><td><?=$MiscTable?></td><tr>
				<tr><td><?=$SignatureTable?></td><tr>
				<?=$FooterRow?>
			<?
			}
		//}
		?>
		</table>
	</div>
	<?
}
?>
<table border="0" cellpadding="4" width="100%" cellspacing="0" class="print_hide">
<tr><td align="right"><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></span></td></tr>
</table>