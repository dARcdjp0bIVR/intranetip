<?php

// Using: Ivan

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage_ui.php");


intranet_auth();
intranet_opendb();

if (!$plugin['ReportCard2008'])
{
	header ("Location: /");
	intranet_closedb();
	exit();
}
 
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
{
	include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
	$lreportcard = new libreportcardcustom();
} else {
	$lreportcard = new libreportcard();
}

if (!$lreportcard->hasAccessRight()) 
{
	header ("Location: /");
	intranet_closedb();
	exit();
}

$linterface = new interface_html();
$ReportTemplateData = $lreportcard->returnReportTemplateBasicInfo($ReportID);

### Gen Report Info
$ReportTitlePieces = explode(":_:",$ReportTemplateData["ReportTitle"]);
$ReportTitle = implode("<br>",$ReportTitlePieces);
$ClassLevelID = $ReportTemplateData["ClassLevelID"];

$fcm_ui = new form_class_manage_ui();
$ly = new Year($ClassLevelID);

$FormName = $ly->YearName;
$ClassSelection = $fcm_ui->Get_Class_Selection($lreportcard->schoolYearID,$ClassLevelID,"ClassID[]",'','',1,1); 

### Gen Setting Table
$SubjectList = $lreportcard->GET_FROM_SUBJECT_GRADING_SCHEME($ClassLevelID,0,$ReportID);
$ReportColumnDataArr = $lreportcard->returnReportTemplateColumnData($ReportID);

foreach($ReportColumnDataArr as $ReportColumnData)
{
	$ReportColumnIDArr[] = $ReportColumnData['ReportColumnID'];
	$ReportColumnTitleArr[] = $ReportColumnData['ColumnTitle'];
}


### Build apply to all textbox and button
$defaultFullMark = 100;
$defaultPassingMark = 60;
$passMarkAllTb = '<input type="text" id="passMarkDefaultTb" class="textboxnum"  maxlength="'.$lreportcard->textAreaMaxChar.'" value="'.$defaultPassingMark.'">';
$passMarkAllBtn = '<a href="javascript:jsApplyToAll(\'passMarkDefaultTb\', \'passMarkTb\');"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_assign.gif" width="12" height="12" border="0"></a>';

$fullMarkAllTb = '<input type="text" id="fullMarkDefaultTb" class="textboxnum"  maxlength="'.$lreportcard->textAreaMaxChar.'" value="'.$defaultFullMark.'">';
$fullMarkAllBtn = '<a href="javascript:jsApplyToAll(\'fullMarkDefaultTb\', \'fullMarkTb\');"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_assign.gif" width="12" height="12" border="0"></a>';

$displayAllChk = $linterface->Get_Checkbox('displayDefaultChk', 'displayDefaultChk', $Value=1, $isChecked=1, '', '', 'jsCheckAll()');

### table header
$display = '';
$display .= '<table width="100%" border="0" cellspacing="0" cellpadding="4">'."\n";
	$display .= '<tr class="tabletop">'."\n";
		$display .= '<td class="tabletopnolink term_break_link" rowspan="2" align="center" width="10%">'.$eReportCard['Subject'].'</td>'."\n";
		$display .= '<td class="tabletopnolink term_break_link" colspan="2" align="center">'.$eReportCard['SubMarksheetColumn'].'</td>'."\n";
		$display .= '<td class="tabletopnolink term_break_link" rowspan="2" align="center" width="8%">';
			$display .= $eReportCard['SchemesFullMark'];
		$display .= '</td>'."\n";
		$display .= '<td class="tabletopnolink term_break_link" rowspan="2" align="center" width="8%">';
			$display .= $eReportCard['SchemesPassingMark'];
		$display .= '</td>'."\n";
		$display .= '<td class="tabletopnolink term_break_link" rowspan="2" align="center" width="2%">'.$eReportCard['Display'].'</td>'."\n";
	$display .= '</tr>'."\n";
	
	$display .= '<tr class="tabletop">'."\n";
	foreach((array)$ReportColumnTitleArr as  $ReportColumnTitle)	
		$display .= '<td class="tabletopnolink term_break_link" align="center" width="25%">'.$ReportColumnTitle.'</td>';
	$display .= '	</tr>';
	
	### "Apply all" buttons
	$display .= '<tr class="tablebottom">'."\n";
		$display .= '<td class="tabletopnolink term_break_link" align="center" width="10%">&nbsp;</td>'."\n";
		$display .= '<td class="tabletopnolink term_break_link" align="center">&nbsp;</td>'."\n";
		$display .= '<td class="tabletopnolink term_break_link" align="center" width="8%">&nbsp;</td>'."\n";
		$display .= '<td class="tabletopnolink term_break_link" align="center" width="8%">';
			$display .= $fullMarkAllTb.'&nbsp;'.$fullMarkAllBtn;
		$display .= '</td>'."\n";
		$display .= '<td class="tabletopnolink term_break_link" align="center" width="8%">';
			$display .= $passMarkAllTb.'&nbsp;'.$passMarkAllBtn;
		$display .= '</td>'."\n";
		$display .= '<td class="tabletopnolink term_break_link" align="center" width="2%">'.$displayAllChk.'</td>'."\n";
	$display .= '</tr>'."\n";

### table body
foreach((array)$SubjectList as $SubjectID => $SubjectData) //for each row
{
	if($SubjectData["is_cmpSubject"]==1) continue;
	$SubjectName = $SubjectData['subjectName'];
	
	$css = ++$i%2==0?"tablerow1":"tablerow2";
	$display .= '
	<tr>
		<td class="tabletext '.$css.' term_break_link td_'.$SubjectID.' subjectTd">'.$SubjectName.'</td>
	';
	
	for($x = 0 ; $x<count($ReportColumnIDArr);$x++) // for each reportColumn
	{
		$SubMarksheetColumnAry = $lreportcard->GET_ALL_SUB_MARKSHEET_COLUMN($ReportColumnIDArr[$x],$SubjectID);

		$display .= '<td class="tabletext '.$css.'  term_break_link  td_'.$SubjectID.' subjectTd" align="center">';
		if(empty($SubMarksheetColumnAry))
		{
			$display .= $eReportCard['NoSubMarksheet'];
		}
		else
		{
			for($y = 0; $y<count($SubMarksheetColumnAry); $y++) //for each sub-marksheet item
			{
				$thisColumnID = $SubMarksheetColumnAry[$y]["ColumnID"];
				$thisColumnTitle = $SubMarksheetColumnAry[$y]["ColumnTitle"];
				$display .= '
					<span style="padding:2px; white-space:nowrap; ">
						<input type="radio" id="SubMarkSheet'.$thisColumnID.'" name="SubMarkSheet['.$SubjectID.']" class="subMSRadio" value="'.$thisColumnID.'">
						<label for="SubMarkSheet'.$thisColumnID.'">'.$thisColumnTitle.'</label>
					</span>			
				';
			}
		}
		$display .= '</td>';
	}
	
	$display .= '
		<td class="tabletext '.$css.' term_break_link td_'.$SubjectID.' subjectTd" align="center"><input type="text" name="FullMark['.$SubjectID.']" class="textboxnum fullMarkTb"  maxlength="'.$lreportcard->textAreaMaxChar.'" value="'.$defaultFullMark.'"></td>
		<td class="tabletext '.$css.' term_break_link td_'.$SubjectID.' subjectTd" align="center"><input type="text" name="PassMark['.$SubjectID.']" class="textboxnum passMarkTb"  maxlength="'.$lreportcard->textAreaMaxChar.'" value="'.$defaultPassingMark.'"></td>
		<td class="tabletext '.$css.' term_break_link" align="center"><input type="checkbox" name="Display[]" id="Display_'.$SubjectID.'" value="'.$SubjectID.'" onclick="CheckDisplay(this,'.$SubjectID.')" class="displayChk" CHECKED></td>
	</tr>
	';
	
}

$display .= '</table>';


### Page Layout  
$CurrentPage = "Reports_SubMarksheetReport";
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();

$TAGS_OBJ[] = array($eReportCard['Reports_SubMarksheetReport'], "", 0);
$linterface->LAYOUT_START();
?>
<script>
function SelectAll()
{
	$("[name='ClassID[]']").contents().attr("selected",true);
}

function CheckDisplay(obj,SubjectID)
{
	$("[name='SubMarkSheet["+SubjectID+"]']").attr("disabled",!obj.checked);
	$("[name='FullMark["+SubjectID+"]']").attr("disabled",!obj.checked);
	$("[name='PassMark["+SubjectID+"]']").attr("disabled",!obj.checked);
	$(".td_" + SubjectID).attr("disabled",!obj.checked);
	
	$('#displayDefaultChk').attr('checked', false);
}

function jsCheckForm()
{
	if ($("[name='Display[]']:checked").length == 0)
	{
		alert("<?=$eReportCard['jsSelectSubjectWarning']?>");
		return false;
	}
	
	document.frm.submit();
}

function jsApplyToAll(targetValueElement, targetClass)
{
	var targetValue = $('#' + targetValueElement).val();
	
	$('.' + targetClass).each( function () {
		$(this).val(targetValue);
	});
}

function jsCheckAll()
{
	var jsSelected = $('#displayDefaultChk').attr('checked');
	
	$('.displayChk').each( function () {
		$(this).attr('checked', jsSelected);
	});
	
	$('.fullMarkTb').each( function () {
		$(this).attr('disabled', !jsSelected);
	});
	
	$('.passMarkTb').each( function () {
		$(this).attr('disabled', !jsSelected);
	});
	
	$('.subjectTd').each( function () {
		$(this).attr('disabled', !jsSelected);
	});
	
	$('.subMSRadio').each( function () {
		$(this).attr('disabled', !jsSelected);
	});
}

$().ready(function(){
	SelectAll();
});
</script>

<br>
<!-- Start Main Table //-->
<form id="frm" name="frm" method="POST" action="print_preview.php" target="_blank">
<table width="96%" border="0" cellspacing="0" cellpadding="5">
  <tr> 
    <td align="center" colspan="2"><table width="100%" border="0" cellspacing="0" cellpadding="2">
      <tr>
        <td align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td align="right"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                
                <tr>
                  <td align="right" class="tabletextremark">
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                         
                        <td align="right" valign="bottom"><?=$linterface->GET_SYS_MSG($msg);?></td>
                      </tr>
                  </table>
                  </td>
                </tr>
            </table></td>
          </tr>
          <tr>
            <td>
	            <table width="100%" border="0" cellspacing="0" cellpadding="4">
		            <tr>
						<td width="30%" nowrap="nowrap" class="formfieldtitle tabletext" valign="top"><?=$eReportCard['ReportTitle']?></td>
						<td valign="top" class="tabletext" width="70%"><?=$ReportTitle?></td>
					</tr>
					<tr>
						<td width="30%" nowrap="nowrap" class="formfieldtitle tabletext" valign="top"><?=$eReportCard['Form']?></td>
						<td valign="top" class="tabletext" width="70%"><?=$FormName?></td>
					</tr>
					<tr>
						<td width="30%" nowrap="nowrap" class="formfieldtitle tabletext" valign="top"><?=$eReportCard['Class']?></td>
						<td valign="top" class="tabletext" width="70%">
							<?=$ClassSelection?>
							<?= $linterface->GET_SMALL_BTN($button_select_all, "button", "SelectAll();", "selectAllBtn01")?>
						</td>
					</tr>
					<tr>
						<td width="30%" nowrap="nowrap" class="formfieldtitle tabletext" valign="top"><?=$eReportCard['SubMarksheet']." ".$eReportCard['ReportTitle']?></td>
						<td valign="top" class="tabletext" width="70%">
							<input name="title1" id="title1" type="text" value="<?=$ReportTitlePieces[0]?>" class="textboxtext" maxlength="<?=$lreportcard->textAreaMaxChar?>"><br>
							<input name="title2" id="title2" type="text" value="<?=$ReportTitlePieces[1]?>"  class="textboxtext"  maxlength="<?=$lreportcard->textAreaMaxChar?>">
						</td>
					</tr>
					<tr>
						<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
							<?= $eReportCard['IssueDate'] ?>
						</td>
						<td width="75%" class='tabletext'>
							<?= $linterface->GET_DATE_PICKER("IssueDate"); ?>
						</td>
					</tr>
	            </table>
	            <br>
	            <table width="100%" border="0" cellspacing="0" cellpadding="4">
		            <tr><td>
		           		<?=$linterface->GET_NAVIGATION2($eReportCard['SubjectSettings'])?>	
		            </td></tr>
		            <tr><td>
		            	<?=$display?>
		            </td></tr>
	            </table>
	            <br>
            </td>
          </tr>
          <tr>
            <td height="1" class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
          </tr>
          <tr>
          	<td align="center">
          		<br><br>
          		<?=$linterface->GET_ACTION_BTN($button_print, "button", "jsCheckForm();", "PrintBtn")?>
          		&nbsp;&nbsp;
          		<?=$linterface->GET_ACTION_BTN($button_back, "button", "self.location='index.php'", "PrintBtn")?>
          	</td>
          </tr>
        </table>
          </td>
      </tr>
    </table>
    </td>
  </tr>
</table>
<input type="hidden" name="ClassLevelID" value="<?=$ClassLevelID?>">
<input type="hidden" name="ReportID" value="<?=$ReportID?>">
</form>
<br>
<br>
<!-- End Main Table //-->
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>