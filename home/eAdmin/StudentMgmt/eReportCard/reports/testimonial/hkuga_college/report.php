<?php
// Editing by 

/**************************************************
 * 	Modification log
 *  20190211 Bill
 *      - feedback from client
 *  20180907 Bill
 *      - feedback from client
 *  20180612 ANNA
 * 		- Create file
 * ***********************************************/

// ini_set('display_errors',1);
// error_reporting(E_ALL ^ E_NOTICE);

// $DebugMode = true;

function returnStudentYearAwards($ParUserID, $ParAcademicYearID)
{
    global $eclass_db, $luserObj;
    
    $sql = "SELECT
                AwardName
            FROM
                {$eclass_db}.AWARD_STUDENT
            WHERE
                UserID = '".$ParUserID."' AND
                AcademicYearID = '".$ParAcademicYearID."' ";
    return $luserObj->returnVector($sql);
}

function convertDateFormat($date)
{
    global $emptySymbol;
    
    $convertedDate = $emptySymbol;
    if (!is_date_empty($date)) {
        $convertedDate = date('d M Y', strtotime($date));
    }
    if($convertedDate == '01 Jan 1970') {
        $convertedDate = $emptySymbol;
    }
    
    return $convertedDate;
}

function returnTestimonialCSS()
{
    $css = '';
    $css = '<head>
			<style type="text/css">
                table {
                    font-family: times, kaiu, mingliu;
                }
                
                table td.report-title {
                    font-family: "balthazar";
                    font-size: 18pt;
                    text-align: center;
                    vertical-align: middle;
                    padding: 18px;
                    padding-bottom: 15px;
                }
                
                table.headerTable {
                    width: 100%;
                }
                table.studentInfo {
                    padding-bottom: 11px;
                }
                table.studentInfo tr {
                    font-size: 14pt;
                    text-align: left;
                }
                table.studentInfo td {
                    line-height: 19px;
                }
                
                table.contentTable {
                    width: 100%;
                    font-size: 10pt;
                }
                
                table td.footerText {
                    font-size: 10pt;
                    text-align: center;
                }
                table td.footerLine {
                    border-width: 1px;
                    border-bottom-style: solid;
                }
            </style>';
    
    return $css;
}

function returnTestimonialStudentInfoTable($dataAry)
{
    global $Lang, $emptySymbol;
    
    $table = '';
    $table .= '<tr>
                    <td class="report-title" colspan="2"><span style="font-size:21pt;">T</span>' . $Lang['eReportCard']['ReportArr']['TestimonialArr']['ReportTitleEn'] . '</td>
               </tr>
               <tr>
                    <td width="45%">
                        <table width="100%" class="studentInfo">
                            <tr>
                                <td width="42%"></td>
                                <td width="3%"></td>
                                <td width="55%"></td>
                            </tr>
                            <tr>
                                <td width="60%">' . $Lang['eReportCard']['ReportArr']['TestimonialArr']['HKIDEn'] . '</td>
                                <td >:</td>
                                <td>' . $dataAry['HKID']. '</td>
                            </tr>
                            <tr>
                                <td width="60%">' . $Lang['eReportCard']['ReportArr']['TestimonialArr']['EnglishNameEn'] . '</td>
                                <td >:</td>
                                <td>' . $dataAry['EnglishName']. '</td>
                            </tr>
                            <tr >
                                <td width="60%">' . $Lang['eReportCard']['ReportArr']['TestimonialArr']['ChineseNameEn'] . '</td>
                                <td >:</td>
                                <td>' . $dataAry['ChineseName']. '</td>
                            </tr>
                            <tr>
                                <td width="60%">' . $Lang['eReportCard']['ReportArr']['TestimonialArr']['GenderEn'] . '</td>
                                <td>:</td>
                                <td>' . $dataAry['Gender']. '</td>
                            </tr>
                        </table>
                    </td>
                    <td width="55%">
                        <table width="100%" class="studentInfo">
                            <tr>
                                <td width="42%"></td>
                                <td width="3%"></td>
                                <td width="55%"></td>
                            </tr>
                            <tr>
                                <td width="60%">' . $Lang['eReportCard']['ReportArr']['TestimonialArr']['DateOfBirthEn'] . '</td>
                                <td>:</td>
                                <td>' . $dataAry['DateOfBirth']. '</td>
                            </tr>
                            <tr>
                                <td width="60%">' . $Lang['eReportCard']['ReportArr']['TestimonialArr']['DateOfAdmissionEn'] . '</td>
                                <td >:</td>
                                <td>' . $dataAry['DateOfAdmission']. '</td>
                            </tr>';
            
            if($dataAry['DateOfGraduation'] != $emptySymbol || $dataAry['DateOfLeave'] == $emptySymbol)
            {
                $table .= ' <tr>
                                <td width="60%">' . $Lang['eReportCard']['ReportArr']['TestimonialArr']['DateOfGraduationEn'] . '</td>
                                <td>:</td>
                                <td>' . $dataAry['DateOfGraduation']. '</td>
                            </tr>';
            }
            else
            {
                $table .= ' <tr>
                                <td width="60%">' . $Lang['eReportCard']['ReportArr']['TestimonialArr']['DateOfLeaveEn'] . '</td>
                                <td>:</td>
                                <td>' . $dataAry['DateOfLeave']. '</td>
                            </tr>';
            }
            
    $table .= '             <tr>
                                <td width="60%">' . $Lang['eReportCard']['ReportArr']['TestimonialArr']['DateOfIssueEn'] . '</td>
                                <td>:</td>
                                <td>' . $dataAry['DateOfIssue']. '</td>
                            </tr>
                         </table>
                    </td>
                </tr>' . "\r\n";
    
    return $table;
}

function returnTeacherCommentTable($StudentID, $LastAcademicYearID)
{
    global $Lang;
 
    $_lreportcard = new libreportcard($LastAcademicYearID);
   
//     $sql = "SELECT Comment FROM $_lreportcard->DBName.RC_MARKSHEET_COMMENT 
//             WHERE StudentID = '".$StudentID."' AND SubjectID = '0'  ORDER BY CommentID DESC";
//     
    $CommentAry = $_lreportcard->getLastAcademicYearTeacherComment($StudentID);
    
    if(empty($CommentAry)) {
        $thisComment = 'No Records';
    } else {
        $thisComment = $CommentAry[0];
    }
   
    $table = "";
//     $table .="<table class='reportContent'>
//                 <thead>                    
//                     <tr>
//                         <th style='text-align: left; font-size: 14px;'>Teacher Comment</th>
               
//                     </tr>
//                 </thead>
//                 <tbody>
//                     <tr>
//                         <td style='text-align: left;'>";
//                         $table .=$thisComment;
//                         $table .="</td>
                        
//                     </tr>
//                 </tbody>
//                 </table>
    $table .= "
            <tr>
                <th colspan='3' style='text-align: left; font-size: 14px;'>Teacher's Comment:</th>
             </tr>
             <tr>
                <td colspan='3' width='90%' style='text-align: left;'>
                   ".$thisComment."
                </td>
             </tr>";
    // $table .= "<br><br>";
    
    return $table;
}

function returnAwardAndPositionTable($UploadType, $StudentID, $AcademicYearIDAry, $ItemCode, $AcademicYearNameAry, $YearIDAry)
{
    global $Lang, $emptySymbol;

    if($UploadType == 'eca') {
        $thisTitle = 'Positions of Responsibility:';
    }
    else {
        $thisTitle = 'Awards:';
    }

//     $table = "";
//     $table .="<table class='reportContent'>
//                 <tr>
//                     <th colspan='3' style='text-align: left; font-size: 14px;'>".$thisTitle."</th>
//                 </tr>";
    
//            for($i=0;$i<count($AcademicYearIDAry);$i++){
//                 $thisAcademicYearID = $AcademicYearIDAry[$i];
//                 $_lreportcard = new libreportcard($thisAcademicYearID);
//                 $thisYearID = $YearIDAry[$i];
//                 $otherInfoDataAry = $_lreportcard->Get_Student_OtherInfo_Data($UploadType, $TermID = '', $ClassID = '', $StudentID,$ItemCode, $thisYearID);
                    
//                 $table .="
//                 <tr>
//                     <td width='20%'  style='text-align: left;'>".$AcademicYearNameAry[$i]."</td>
//                     <td width='10%'> </td>
//                     <td>".$otherInfoDataAry[0]['Information']."</td>
//                 </tr>";
//              } 
    
//     $table .="</table>";
    $table = "  <tr><th colspan='3' style='text-align: left; font-size: 14px;'><div height='4mm' style='font-size: 10px;'>&nbsp;</div>".$thisTitle."</th></tr>";
    
    $hasValidData = false;
    $academicYearCount = count($AcademicYearIDAry);
    for($i = ($academicYearCount-1); $i >= 0; $i--)
    {
        $thisAcademicYearId = $AcademicYearIDAry[$i];
        $thisYearID = $YearIDAry[$i];
        
        $ResponsibilitiesOrAwardsAry = array();
        
        # Awards (from iPortfolio)
        if($UploadType != 'eca')
        {
            $portfolioDataAry = returnStudentYearAwards($StudentID, $thisAcademicYearId);
            if(!empty($portfolioDataAry))
            {
                foreach((array)$portfolioDataAry as $thisPortfolioData) {
                    if(trim($thisPortfolioData) == '' || trim($thisPortfolioData) == $emptySymbol) {
                        // do nothing
                    } else {
                        $ResponsibilitiesOrAwardsAry[] = trim($thisPortfolioData);
                    }
                }
            }
        }
        
        # Awards / Responsibilities (eReportCard > Other Info)
        $_lreportcard = new libreportcard($thisAcademicYearId);
        $otherInfoDataAry = $_lreportcard->Get_Student_OtherInfo_Data($UploadType, $TermID='', $ClassID='', $StudentID, $ItemCode, $thisYearID);
        if(!empty($otherInfoDataAry))
        {
            foreach((array)$otherInfoDataAry as $otherInfoAry) {
                $otherInfoAry = explode('##', $otherInfoAry['Information']);
                foreach((array)$otherInfoAry as $thisOtherInfo) {
                    if(trim($thisOtherInfo) == '' || trim($thisOtherInfo) == $emptySymbol) {
                        // do nothing
                    } else {
                        $ResponsibilitiesOrAwardsAry[] = trim($thisOtherInfo);
                    }
                }
            }
        }
        $ResponsibilitiesOrAwardsAry = array_values(array_unique(array_filter($ResponsibilitiesOrAwardsAry)));
        
        $displayData = implode(', ', $ResponsibilitiesOrAwardsAry);
        if($displayData != '' && $displayData != $emptySymbol)
        {
            $hasValidData = true;
            
            $table .= "
                <tr>
                    <td width='10%' style='text-align: left; vertical-align: top;'>".$AcademicYearNameAry[$i]."</td>
                    <td width='5%'> </td>
                    <td style='text-align: left;'>".$displayData."</td>
                </tr>";
        }
    }
    // $table .= "<br><br>";
    
    if(!$hasValidData) {
        $table = '';
    }
    return $table;
}

function returnActivitiesTable($StudentID,$AcademicYearIDAry,$YearName)
{
    global $libenroll, $emptySymbol;
    
    $table = "  <tr><th colspan='3' style='text-align: left; font-size: 14px;'><div height='4mm' style='font-size: 10px;'>&nbsp;</div>Activities and Community Service:</th></tr>";
    
    $hasValidData = false;
    $academicYearCount = count($AcademicYearIDAry);
    for($i = ($academicYearCount - 1); $i >= 0; $i--)
    {
        $TitleAry = array();
        $EventTitleAry = array();
        
        $thisAcademicYearID = $AcademicYearIDAry[$i];
        $StudentEnrolledClubAry = $libenroll->GET_STUDENT_ENROLLED_CLUB_LIST($StudentID,'',$thisAcademicYearID);
        if(!empty($StudentEnrolledClubAry))
        {
            foreach($StudentEnrolledClubAry as $StudentEnrolledClub) {
                $thisStudentEnrolGroupID = $StudentEnrolledClub[0];
                $DisplayTitle = $libenroll->get_Club_DisplayName($thisStudentEnrolGroupID);
                $DisplayTitle = (trim($DisplayTitle) == '' || trim($DisplayTitle) == $emptySymbol)? $StudentEnrolledClub[1] : $DisplayTitle;
                if(trim($DisplayTitle) == '' || trim($DisplayTitle) == $emptySymbol) {
                    // do nothing
                } else {
                    $TitleAry[] = trim($DisplayTitle);
                }
//              $TitleAry[] = $StudentEnrolledClub[1];
            }
        }
        
        $StudentEnrolledActivityAry = $libenroll->GET_STUDENT_ENROLLED_EVENT_LIST($StudentID,$thisAcademicYearID);
        if(!empty($StudentEnrolledActivityAry))
        {
            foreach($StudentEnrolledActivityAry as $StudentEnrolledActivity) {              
                $thisStudentEnrolEventID = $StudentEnrolledActivity[0];
                $DisplayTitle = $libenroll->get_Event_DisplayName($thisStudentEnrolEventID);
                $DisplayTitle = (trim($DisplayTitle) == '' || trim($DisplayTitle) == $emptySymbol)? $StudentEnrolledActivity['EventTitle'] : $DisplayTitle;
                if(trim($DisplayTitle) == '' || trim($DisplayTitle) == $emptySymbol) {
                    // do nothing
                } else {
                    $TitleAry[] = trim($DisplayTitle);
                }
//              $TitleAry[] = $StudentEnrolledActivity['EventTitle'];            
            }
        }
        $TitleAry = array_values(array_unique(array_filter($TitleAry)));
        
        if(empty($TitleAry)) {
            $displayData = '';
        }
        else {
            $displayData = implode(', ', $TitleAry);
            if($displayData != '' && $displayData != $emptySymbol)
            {
                $hasValidData = true;
                
                $table .= "
                    <tr>
                        <td width='10%' style='text-align: left; vertical-align: top;'>".$YearName[$i]."</td>
                        <td width='5%' style=''> </td>
                        <td style='text-align: left;'>".$displayData."</td>
                    </tr>";
            }
        }
    }
    // $table .= "<br><br>";
    
    if(!$hasValidData) {
        $table = '';
    }
    return $table;
}

// ## Set Common Variables
$emptySymbol = '--';

// ## POST data
$userLogin = standardizeFormPostValue($_POST['userLogin']);
$studentIdAry = $_POST['studentIdAry'];
$dateOfGraduation = standardizeFormPostValue($_POST['dateOfGraduation']);
$dateOfLeaving = standardizeFormPostValue($_POST['dateOfLeaving']);
$dateOfIssue = standardizeFormPostValue($_POST['dateOfIssue']);
$princialName = standardizeFormPostValue($_POST['princialName']);

// ## Load all Target Student
$luserObj = new libuser('', '', $studentIdAry);
$numOfStudent = count($studentIdAry);

// ## Get Student Class History Info
$numOfData = count($studentClassHistoryAry);

// // ## Analyze Student Class History Info
// $studentDataAssoAry = array();
// $thisStudentAcademicYearIdAry= array();
// $YearIdAry = array();
// $_academicYearName = array();
// for ($i = 0; $i < $numOfData; $i ++)
// {
//     $_studentId = $studentClassHistoryAry[$i]['UserID'];
// //     $LastYearId= $studentClassHistoryAry[$i]['YearID'];
//     $_formName = $studentClassHistoryAry[$i]['YearName'];
//     $_formNumber = $lreportcard->GET_FORM_NUMBER($_formId, 1);
//     $_formLevelKey = ($_formNumber < 4) ? 'junior' : 'senior';
//     $thisStudentAcademicYearIdAry[] = $studentClassHistoryAry[$i]['AcademicYearID'];
//     $LastAcademicYearId = $studentClassHistoryAry[$i]['AcademicYearID'];
//     $YearIdAry[] = $studentClassHistoryAry[$i]['YearID']; 
//     $_academicYearName[] = $studentClassHistoryAry[$i]['AcademicYearNameEn'];    
// }

$j = 0;
foreach ($studentIdAry as $StudentID) { 
   $studentClassHistoryAry[$StudentID] = $lreportcard->Get_Student_From_School_Settings($AcademicYearIDAry = '', $StudentID);
   for($i=0; $i<count($studentClassHistoryAry[$StudentID]); $i++) {
    //   $_thisacademicYearName[$j][] = $studentClassHistoryAry[$StudentID][$i]['AcademicYearNameEn'];
       $StudentAcademicYearIDAry[$StudentID][] = $studentClassHistoryAry[$StudentID][$i]['AcademicYearID'];
       $AcademicYearNameAry[$StudentID][] = $studentClassHistoryAry[$StudentID][$i]['AcademicYearNameEn'];
       
       $LastAcademicYearID[$StudentID] = $studentClassHistoryAry[$StudentID][$i]['AcademicYearID'];
       $YearIDAry[$StudentID][] = $studentClassHistoryAry[$StudentID][$i]['YearID'];
   }
//    $LastYearId = $studentClassHistoryAry[$StudentID];  
   $j++;
}

// ## Get Report related data
$markAry = array();
$subjectAry = array();
$subjectGradingSchemeAry = array();
$TestimonialRemarksAry = array();

include_once ($PATH_WRT_ROOT . "includes/libaccountmgmt.php");
$laccount = new libaccountmgmt();

// ## Build Report Content
$TestimonialContentAry = array();
for($i = 0; $i < $numOfStudent; $i++)
{
    $_reportResultHtml = '';
    
    $_isLastStudent = false;
    if ($i == $numOfStudent - 1) {
        $_isLastStudent = true;
    }
    
    // Get Student data
    $_studentId = $studentIdAry[$i];
    $luserObj->LoadUserData($_studentId);
    $_studentPersonalData = $laccount->getUserPersonalSettingByID($_studentId);
    
    $_thisStudetnAcademicYearIDAry = $StudentAcademicYearIDAry[$_studentId];
    $_thisStudentYearIDAry = $YearIDAry[$_studentId];
    
    $_studentInfoAry = array();
    $_studentInfoAry["EnglishName"] = str_replace('*', '', ($luserObj->EnglishName? $luserObj->EnglishName : $emptySymbol));
    $_studentInfoAry["ChineseName"] = str_replace('*', '', ($luserObj->ChineseName? $luserObj->ChineseName: $emptySymbol));
    $_studentInfoAry["UserLogin"]   = $luserObj->UserLogin? $luserObj->UserLogin : $emptySymbol;
    $_studentInfoAry["HKID"]        = $luserObj->HKID? $luserObj->HKID : $emptySymbol;
    $_studentInfoAry["Gender"]      = $luserObj->Gender? $luserObj->Gender : $emptySymbol;
    $_studentInfoAry["DateOfAdmission"]  = convertDateFormat($_studentPersonalData['AdmissionDate']);
    $_studentInfoAry["DateOfBirth"]      = convertDateFormat($luserObj->DateOfBirth);
    $_studentInfoAry["DateOfGraduation"] = convertDateFormat($dateOfGraduation);
    $_studentInfoAry["DateOfLeave"]      = convertDateFormat($dateOfLeaving);
    $_studentInfoAry["DateOfIssue"]      = convertDateFormat($dateOfIssue);
    
    // ## Get User Photo
    $_studentInfoAry["UserPhotoPath"] = $luserObj->GET_USER_PHOTO($_studentInfoAry["UserLogin"], $_studentId, $UseSamplePhoto=true);
    
    // Get Junior & Senior Years
    $_tableNumber = 0;
    $_reportPageAry = array();
  //  $TeacherComment = $this->returnTeacherComment();
    
    // Get Report Header
    $_reportHeaderHtml = returnTestimonialStudentInfoTable($_studentInfoAry);

    $TeacherCommentHtml = returnTeacherCommentTable($_studentId,$LastAcademicYearID[$_studentId]);

    $PositionOfResponbilityHtml = returnAwardAndPositionTable($UploadType = 'eca',$_studentId,$_thisStudetnAcademicYearIDAry,'Responsibilities/Services',$AcademicYearNameAry[$_studentId],$_thisStudentYearIDAry);
    
    $StudentActivityHtml = returnActivitiesTable($_studentId,$_thisStudetnAcademicYearIDAry,$AcademicYearNameAry[$_studentId]);
    $StudentAwardsHtml = returnAwardAndPositionTable($UploadType = 'award',$_studentId,$_thisStudetnAcademicYearIDAry,'Awards',$AcademicYearNameAry[$_studentId],$_thisStudentYearIDAry);
    
    $_reportResultHtml = $TeacherCommentHtml;
    $_reportResultHtml .= $PositionOfResponbilityHtml;  
    $_reportResultHtml .= $StudentActivityHtml;
    $_reportResultHtml .= $StudentAwardsHtml;
    
    // Report Footer
    $displayPrincialName = trim($princialName) != '' ? trim($princialName) . '<br/>' : $eReportCard['Template']['PrincipalName'].'<br/>';
    // $displayPrincialName = trim($princialName) != '' ? trim($princialName) . '<br/>' : '<br/>';
    
    //$needPageBreak = $_isLastStudent ? '' : ' page-break-after:always; ';
//     $_reportFooterHtml = ' <div class="sign-space" height="20mm" style=" ' . $needPageBreak . '" align="center">
//                                 <table width="100%">
//                                     <tr class="sign-line">
//                                     	<td width="15%" style="line-height: 1"></td>
//                                         <td width="27.5%" style="line-height: 1">_________________________</td>
//                                         <td width="15%" style="line-height: 1"></td>
//                                         <td width="27.5%" style="line-height: 1";>______________________________</td>
//                                         <td width="15%" style="line-height: 1"></td>
//                                     </tr>
//                                     <tr>
//                                     	<td></td>
//                                     	<td style="border-top: 1px black solid; text-align: center">' . $Lang['eReportCard']['ReportArr']['TestimonialArr']['SchoolChopEn'] . '</td>
//                                     	<td></td>
//                                     	<td style="border-top: 1px black solid; text-align: center">' . $displayPrincialName . $Lang['eReportCard']['ReportArr']['TestimonialArr']['PrincipalEn'] . '</td>
//                                     	<td></td>
//                                     </tr>
//                                     <tr class="end-line">
//                                     	<td colspan="5">' . $Lang['eReportCard']['ReportArr']['TestimonialArr']['EndOfTestimonialEn'] . '</td>
//                                     </tr>
//                                 </table>
//                             </div>';
    $_reportFooterHtml = '  <div style=" ' . $needPageBreak . '" align="center" height="35mm">
                                <table width="100%" height="35mm">
                                     <tr>
                                        <td width="5%"></td>
                                        <td height="35mm" width="22%">
                                            <table width="100%" style="padding: 0; text-align: center;">
                                                <tr>
                                                    <td width="100%">
                                                        <img src="'.$_studentInfoAry["UserPhotoPath"][2].'" height="35mm" style="border: 1px solid gray;"/>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td width="9%"></td>
                                    	<td height="35mm" width="25%">
                                            <table width="100%">
                                                <tr>
                                                	<td width="80%" class="footerLine" height="30mm">
                                                       <div style="font-size: 10px;">&nbsp;</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td height="5mm" class="footerText">'.$Lang['eReportCard']['ReportArr']['TestimonialArr']['SchoolChopEn'].'<br/>&nbsp;</td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td width="9%"></td>
                                    	<td height="35mm" width="25%">
                                            <table width="100%">
                                                <tr>
                                                	<td width="80%" class="footerLine" height="30mm">
                                                       <div height="35mm" style="font-size: 10px;">&nbsp;</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td height="5mm" class="footerText">'.$displayPrincialName.$Lang['eReportCard']['ReportArr']['TestimonialArr']['PrincipalEn'].'</td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td width="5%"></td>
                                     </tr>
                                </table>
                            </div>';
    $needPageBreak = $_isLastStudent ? '' : '<pagebreak />';
    
    // Report Header & Table
    $x = '';
    $x .= '<div style="width:100%;" align="center">' . "\r\n";
        $x .= '<div class="container" height="196mm;" align="center">' . "\r\n";
            $x .= '<table cellpadding="0" cellspacing="0" align="center" class="headerTable">' . "\r\n";
                $x .= $_reportHeaderHtml;
            $x .= '</table>' . "\r\n";
            $x .= '<table cellspacing="0" cellpadding="0" align="center" class="contentTable">' . "\r\n";
                $x .= $_reportResultHtml;
            $x .= '</table>' . "\r\n";
        $x .= '</div>' . "\r\n";
        $x .= $_reportFooterHtml;
        $x .= $needPageBreak;
    $x .= '</div>' . "\r\n";
    $TestimonialContentAry[] = $x;
}

# Define header
$mpdfObj->DefHTMLHeaderByName("titleHeader", "");
$mpdfObj->SetHTMLHeaderByName("titleHeader");

# Define footer
$mpdfObj->DefHTMLFooterByName("emptyFooter", "");
$mpdfObj->SetHTMLFooterByName("emptyFooter");

$TestimonialCSS = returnTestimonialCSS();
$mpdfObj->writeHTML($TestimonialCSS);

$mpdfObj->writeHTML("<body>");
foreach ($TestimonialContentAry as $thisTestimonialContent)
{
    $mpdfObj->writeHTML($thisTestimonialContent);
}
$mpdfObj->writeHTML("</body>");

// echo $TestimonialCSS;
// foreach($TestimonialContentAry as $thisTestimonialContent)
// {
// echo $thisTestimonialContent;
// }

// For performance tunning info
// echo '<script language="JavaScript" src="/templates/jquery/jquery-1.3.2.min.js"></script>';
// debug_pr('convert_size(memory_get_usage(true)) = '.convert_size(memory_get_usage(true)));
// debug_pr('Query Count = '.$GLOBALS[debug][db_query_count]);
// $lreportcard->db_show_debug_log();
// die();
?>