<?php
# Editing by

/**************************************************
 * 	Modification log
 *  20190625 Bill	[2017-0901-1525-31265]
 * 		- Support Date of Graduation input
 * 	20180723 Bill:  [2017-0901-1525-31265]
 * 		Allow View Group to select all options
 *  20180226 Bill	[2017-0901-1525-31265]
 * 		- Support PDF mode                  ($eRCTemplateSetting['Report']['TranscriptByPDF'])
 * ***********************************************/

$PageRight = array("ADMIN", "VIEW_GROUP");

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();
 
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");


$lreportcard = new libreportcardcustom();
$lreportcard->hasAccessRight();

$lreportcard_ui = new libreportcard_ui();
$linterface = new interface_html();

# Tag
$CurrentPage = "Reports_Testimonial";
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
$TAGS_OBJ[] = array($eReportCard['Reports_Testimonial'], "", 0);

$checkPermissionFormClass = ($lreportcard->IS_ADMIN_USER_VIEW($_SESSION['UserID']) || $lreportcard->IS_VIEW_GROUP_USER_VIEW($_SESSION['UserID']))? 0 : 3;
$checkPermissionSubject = ($lreportcard->IS_ADMIN_USER_VIEW($_SESSION['UserID']) || $lreportcard->IS_VIEW_GROUP_USER_VIEW($_SESSION['UserID']))? 0 : 1;

$linterface->LAYOUT_START();
echo $linterface->Include_AutoComplete_JS_CSS();

$x = '';
$x .= '<table class="form_table_v30">'."\n";	
	// Form
	$x .= '<tr >'."\n";
		$x .= '<td class="field_title">'.$Lang['General']['Form'].'</td>'."\n";
		$x .= '<td>'."\n";
			$x .= $lreportcard_ui->Get_Form_Selection('yearId', $SelectedYearID='', 'changedFormSelection();', $noFirst=1, $isAll=0, $hasTemplateOnly=1, $checkPermissionFormClass, $IsMultiple=0);
		$x .= '</td>'."\n";
	$x .= '</tr>'."\n";
	
	// Class
	$x .= '<tr>'."\n";
		$x .= '<td class="field_title">'.$Lang['General']['Class'].'</td>'."\n";
		$x .= '<td>'."\n";
			$x .= '<div id="classSelDiv"></div>';
		$x .= '</td>'."\n";
	$x .= '</tr>'."\n";
    
	// Student
	$x .= '<tr>'."\n";
		$x .= '<td class="field_title">'.$Lang['Identity']['Student'].'</td>'."\n";
		$x .= '<td>'."\n";
			$x .= '<div id="studentSelDiv"></div>';
			$x .= $linterface->Get_Form_Warning_Msg("studentWarning", $eReportCard['jsWarningArr']['PleaseSelectStudent'], "warnMsgDiv")."\n";
		$x .= '</td>'."\n";
	$x .= '</tr>'."\n";
	
	// Date of ISSUE
	$x .= '<tr>'."\n";
		$x .= '<td class="field_title">'.$Lang['eReportCard']['ReportArr']['TranscriptArr']['DateOfIssue'].'</td>'."\n";
		$x .= '<td>'."\n";
			$x .= $linterface->GET_DATE_PICKER('dateOfIssue');
		$x .= '</td>'."\n";
	$x .= '</tr>'."\n";
		
	// Date of GRADUATION
	$x .= '<tr>'."\n";
		$x .= '<td class="field_title">'.$Lang['eReportCard']['ReportArr']['TranscriptArr']['DateOfGraduation'].'</td>'."\n";
		$x .= '<td>'."\n";
		  $x .= $linterface->GET_DATE_PICKER('dateOfGraduation');
		$x .= '</td>'."\n";
	$x .= '</tr>'."\n";
	
	// Date of LEAVING
    $x .= '<tr>'."\n";
	    $x .= '<td class="field_title">'.$Lang['eReportCard']['ReportArr']['TranscriptArr']['DateOfLeaving'].'</td>'."\n";
	    $x .= '<td>'."\n";
	       $x .= $linterface->GET_DATE_PICKER('dateOfLeaving');
	    $x .= '</td>'."\n";
    $x .= '</tr>'."\n";
	
	// Prinical Name
    $x .= '<tr>'."\n";
        $x .= '<td class="field_title">'.$eReportCard['PrincipalName'].'</td>'."\n";
	    $x .= '<td>'."\n";
	       $x .= '<input name="princialName" type="text" class="textbox_name" value="" />'."\n";
	    $x .= '</td>'."\n";
    $x .= '</tr>'."\n";
	
$x .= '</table>'."\n";

$htmlAry = array();
$htmlAry['optionTable'] = $x;
$htmlAry['submitBtn'] = $linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], 'button', 'checkForm();');
$htmlAry['exportStudentInfoBtn'] = $linterface->GET_ACTION_BTN($eReportCard['ExportStudentInfo'], 'button', 'exportStudentInfo();');
$htmlAry['exportStudentRecordBtn'] = $linterface->GET_ACTION_BTN($eReportCard['ExportStudentRecords'], 'button', 'exportStudentRecords();');
?>

<script>
var loading = '<?=$linterface->Get_Ajax_Loading_Image($noLang=1)?>';
var checkPermissionFormClass = '<?=$checkPermissionFormClass?>';
var checkPermissionSubject = '<?=$checkPermissionSubject?>';
var autoCompleteObj_userLogin;

$(document).ready(function()
{
	$('select#yearId').val($('select#yearId option:last').val());	
	
	// initialize selection for form class student source
	changedFormSelection();
//	changedStudentSource('formClass', 1);
	
	// initialize selection for class history student source
// 	changedSchoolSettingsAcademicYearSelection(1);
});

function changedFormSelection()
{
	var selectedYearId = $('select#yearId').val();
	
	$('div#classSelDiv').html(loading).load(
		"../ajax_reload_selection.php", 
		{ 
			RecordType: 'Class',
			YearID: selectedYearId,
			SelectionID: 'yearClassIdAry[]',
			onchange: 'changedClassSelection();',
			isMultiple: 1,
			TeachingOnly: checkPermissionFormClass,
			WithSelectAllButton: 1
		},
		function(ReturnData) {
			js_Select_All('yearClassIdAry[]');
			changedClassSelection();
		}
	);
}

function changedClassSelection()
{
	var classSelId = getJQuerySaveId('yearClassIdAry[]');
	var classIdList = $('select#' + classSelId).val();
	if  (classIdList) {
		classIdList = classIdList.join(',');
	}
	else {
		classIdList = '-1'; 	// no class will be returned
	}
	
	$('div#studentSelDiv').html(loading).load(
		"../ajax_reload_selection.php", 
		{ 
			RecordType: 'StudentByClass',
			ClassID: classIdList,
			SelectionID: 'studentIdAry[]',
			noFirst: 1,
			isMultiple: 1,
			withSelectAll: 1
		},
		function(ReturnData) {
			js_Select_All('studentIdAry[]');
		}
	);
}
function checkForm()
{
		$('div.warnMsgDiv').hide();
		
		var canSubmit = true;
		var studentSelId = getJQuerySaveId('studentIdAry[]');
		if ($('select#' + studentSelId).val() == null || $('select#' + studentSelId).val() == '') {
			$('div#studentWarning').show();
			$('select#' + studentSelId).focus();
			
			canSubmit = false;
		}
		
		if (canSubmit) {
 			$('form#form1').submit();
		}
}

function exportStudentInfo(){
	var studentSelId = getJQuerySaveId('studentIdAry[]');
// 	document.form1.method = "post";
	var SelectedStudentID = $('select#'+studentSelId).val();

	var GraduationDate = document.getElementsByName("dateOfGraduation")[0].value;
	var IssueDate = document.getElementsByName("dateOfIssue")[0].value;
	window.open("export_studentInfo.php?StudentList="+SelectedStudentID+"&dateOfGraduation=" + GraduationDate+ "&dateOfIssue=" + IssueDate);
}

function exportStudentRecords(){
	var studentSelId = getJQuerySaveId('studentIdAry[]');
// 	document.form1.method = "post";
	var SelectedStudentID = $('select#'+studentSelId).val();
	window.open("export_studentRecords.php?StudentList="+SelectedStudentID);
}
</script>

<form id="form1" name="form1" method="post" target="_blank" action="report_by_pdf.php" >
	<div class="table_board">
		<table id="html_body_frame" style="width:100%;">
			<tr><td><?=$htmlAry['optionTable']?></td></tr>
		</table>
	</div>
	<div class="edit_bottom_v30">
		<br />
		<?=$htmlAry['submitBtn']?>
		<?=$htmlAry['exportStudentInfoBtn']?>
		<?=$htmlAry['exportStudentRecordBtn']?>
	</div>
</form>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>