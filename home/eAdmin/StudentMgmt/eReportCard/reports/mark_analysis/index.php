<?php
// Using:

################## Change Log [Start] ##############
#
#   Date:   2019-06-28  Bill
#           Term Selection > Change 'termId' to 'TargetTerm' to prevent IntegerSafe()
#
################## Change Log [End] ##############

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();
 
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");

$lreportcard = new libreportcardcustom();
$lreportcard_ui = new libreportcard_ui();
$linterface = new interface_html();

$lreportcard->hasAccessRight();

$CurrentPage = "Reports_MarkAnalysis";
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();

# tag information
$TAGS_OBJ[] = array($eReportCard['Reports_MarkAnalysis'], "", 0);
$linterface->LAYOUT_START();


# Term Selection
$termInfoAry = $lreportcard->returnReportTemplateTerms();
$termSelAttr = 'name="TargetTerm" id="termIdSel" onchange="changedTermSelection(this.value);"';
$htmlAry['termSelection'] = getSelectByArray($termInfoAry, $termSelAttr, "", $all=0, $noFirst=1);


# report column checkboxes
$x = '';
$x .= $linterface->Get_Checkbox('ReportColumn_All', 'ReportColumn_All', '', $isChecked=1, $Class='', $Lang['Btn']['SelectAll'], "Check_All_Options_By_Class('ReportColumnChk', this.checked);", $Disabled='');
$x .= '<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
$x .= $linterface->Get_Checkbox('ReportColumn_1', 'ReportColumnNumAry[]', 1, $isChecked=1, 'ReportColumnChk', $Lang['eReportCard']['ReportArr']['MarkAnalysisArr']['Test'], "Uncheck_SelectAll('ReportColumn_All', this.checked);", $Disabled='');
$x .= '<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
$x .= $linterface->Get_Checkbox('ReportColumn_2', 'ReportColumnNumAry[]', 2, $isChecked=1, 'ReportColumnChk', $Lang['eReportCard']['ReportArr']['MarkAnalysisArr']['DailyMark'], "Uncheck_SelectAll('ReportColumn_All', this.checked);", $Disabled='');
$x .= '<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
$x .= $linterface->Get_Checkbox('ReportColumn_3', 'ReportColumnNumAry[]', 3, $isChecked=1, 'ReportColumnChk', $Lang['eReportCard']['ReportArr']['MarkAnalysisArr']['Exam'], "Uncheck_SelectAll('ReportColumn_All', this.checked);", $Disabled='');
$x .= '<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
$x .= $linterface->Get_Checkbox('ReportColumn_'.$i, 'ReportColumnNumAry[]', 0, $isChecked=1, 'ReportColumnChk', $eReportCard["Overall"], "Uncheck_SelectAll('ReportColumn_All', this.checked);", $Disabled='');
$x .= '<br>'."\n";
$htmlAry['reportColumnCheckboxes'] = $x;


# % of student
$fullMarkPercentageTb = $linterface->GET_TEXTBOX_NUMBER('fullMarkPercentageTb', 'fullMarkPercentage', '', $OtherClass='requiredField');
$fullMarkPercentageTbField = str_replace('<!--textbox-->', $fullMarkPercentageTb, $Lang['eReportCard']['ReportArr']['MarkAnalysisArr']['MarkGreaterThanOrEqualToFullMarkPercentage']);


# main table
$x = '';
$x .= '<table class="form_table_v30">'."\n";
	// Term
	$x .= '<tr>'."\n";
		$x .= '<td class="field_title">'.$linterface->RequiredSymbol().$Lang['General']['Term'].'</td>'."\n";
		$x .= '<td>'."\n";
			$x .= $htmlAry['termSelection'];
		$x .= '</td>'."\n";
	$x .= '</tr>'."\n";
	
	// Type
	$x .= '<tr>'."\n";
		$x .= '<td class="field_title">'.$linterface->RequiredSymbol().$Lang['General']['Type'].'</td>'."\n";
		$x .= '<td>'."\n";
			$x .= $linterface->Get_Radio_Button('reportType_Class', 'reportType', 'class', $isChecked=1, $Class="reportTypeChk", $Lang['General']['Class'], 'changedReportTypeRadio(this.value);');
			$x .= $linterface->Get_Radio_Button('reportType_Form', 'reportType', 'form', $isChecked=0, $Class="reportTypeChk", $Lang['General']['Form'], 'changedReportTypeRadio(this.value);');
			$x .= $linterface->Get_Radio_Button('reportType_SubjectGroup', 'reportType', 'sg', $isChecked=0, $Class="reportTypeChk", $Lang['SysMgr']['SubjectClassMapping']['SubjectGroup'], 'changedReportTypeRadio(this.value);');
		$x .= '</td>'."\n";
	$x .= '</tr>'."\n";
	
	// Report Column
	$x .= '<tr>'."\n";
		$x .= '<td class="field_title">'.$linterface->RequiredSymbol().$eReportCard['ReportColumn'].'</td>'."\n";
		$x .= '<td>'."\n";
			$x .= $htmlAry['reportColumnCheckboxes'];
			$x .= $linterface->Get_Form_Warning_Msg('reportColumnEmptyWarnDiv', $eReportCard['MasterReport']['jsWarningArr']['SelectReportColumn'], $Class='warnMsgDiv')."\n";
		$x .= '</td>'."\n";
	$x .= '</tr>'."\n";
	
	// Class / Form / Subject Group selection
	$x .= '<tr>'."\n";
		$x .= '<td class="field_title">'.$linterface->RequiredSymbol().$eReportCard['Target'].'</td>'."\n";
		$x .= '<td>'."\n";
			$x .= '<span id="TargetFormSelectionSpan"></span>'."\n";
			$x .= '<span id="TargetSourceSelectionSpan"></span>'."\n";
			$x .= '<span id="TargetSubjectGroupSelectionSpan"></span>'."\n";
			$x .= '<span>'.$linterface->GET_SMALL_BTN($button_select_all, "button", "javascript:SelectAll(document.form1.elements['TargetID[]'])").'</span>'."\n";
			$x .= $linterface->Get_Form_Warning_Msg('targetClassEmptyWarnDiv', $Lang['eReportCard']['WarningArr']['Select']['Class'], $Class='warnMsgDiv')."\n";
			$x .= $linterface->Get_Form_Warning_Msg('targetFormEmptyWarnDiv', $Lang['eReportCard']['WarningArr']['Select']['Form'], $Class='warnMsgDiv')."\n";
			$x .= $linterface->Get_Form_Warning_Msg('targetSubjectGroupEmptyWarnDiv', $Lang['eReportCard']['WarningArr']['Select']['SubjectGroup'], $Class='warnMsgDiv')."\n";
		$x .= '</td>'."\n";
	$x .= '</tr>'."\n";
	
	// % of student
	$x .= '<tr>'."\n";
		$x .= '<td class="field_title">'.$linterface->RequiredSymbol().$Lang['eReportCard']['ReportArr']['MarkAnalysisArr']['PercentageOfStudent'].'</td>'."\n";
		$x .= '<td>'."\n";
			$x .= $fullMarkPercentageTbField;
			$x .= $linterface->Get_Form_Warning_Msg('percentageInvalidWarnDiv', $Lang['General']['JS_warning']['InputPositiveValue'], $Class='warnMsgDiv')."\n";
		$x .= '</td>'."\n";
	$x .= '</tr>'."\n";
	// View format
	$x .= '<tr>'."\n";
		$x .= '<td class="field_title">'.$linterface->RequiredSymbol().$eReportCard['ViewFormat'].'</td>'."\n";
		$x .= '<td>'."\n";
			$x .= $linterface->Get_Radio_Button('ViewFormatHTML', 'ViewFormat', 'html', $isChecked=1, $Class="", $eReportCard['HTML']);
			$x .= $linterface->Get_Radio_Button('ViewFormatCSV', 'ViewFormat', 'csv', $isChecked=0, $Class="", $eReportCard['CSV']);
			$x .= $linterface->Get_Radio_Button('ViewFormatPDF', 'ViewFormat', 'pdf', $isChecked=0, $Class="", $eReportCard['PDF']);
		$x .= '</td>'."\n";
	$x .= '</tr>'."\n";
	
$x .= '</table>'."\n";
$htmlAry['optionTable'] = $x;


$htmlAry['submitBtn'] = $linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], 'button', 'checkForm();');

?>
<script>
var loading = '<?=$linterface->Get_Ajax_Loading_Image($noLang=1)?>';
var academicYearId = '<?=$lreportcard->GET_ACTIVE_YEAR_ID()?>';
var curTermId = '';
var curReportType = '';

$(document).ready(function(){
	curTermId = $('select#termIdSel').val();
	curReportType = getReportType();
	
	changedReportTypeRadio(curReportType);
});

function getReportType() {
	var reportType = '';
	$('input.reportTypeChk').each( function() {
		if ($(this).attr('checked')) {
			reportType = $(this).val();
		}
	});
	
	return reportType;
}

function changedTermSelection(parTermId) {
	if (parTermId == 'F' && curReportType=='sg') {
		alert('<?=$eReportCard['ReportArr']['SubjectPrize']['jsWarningArr']['SubjectGroupRankingMustSelectATerm']?>');
		$('select#termIdSel').val(curTermId);
		return false;
	}
	else {
		curTermId = parTermId;
		changedReportTypeRadio(curReportType);
	}
}

function changedReportTypeRadio(parReportType) {
	if (curTermId == 'F' && parReportType=='sg') {
		alert('<?=$eReportCard['ReportArr']['SubjectPrize']['jsWarningArr']['SubjectGroupRankingMustSelectATerm']?>');
		$('input.reportTypeChk').each( function() {
			if ($(this).val() == curReportType) {
				$(this).attr('checked', 'checked');
			}
		});
		
		return false;
	}
	else {
		curReportType = parReportType;
		
		if (parReportType == 'class') {
			$('span#TargetFormSelectionSpan').html('');
			$('span#TargetSubjectGroupSelectionSpan').html('');
			
			reloadClassSelection();
		}
		else if (parReportType == 'form') {
			$('span#TargetFormSelectionSpan').html('');
			$('span#TargetSubjectGroupSelectionSpan').html('');
			
			reloadFormSelection();
		}
		else if (parReportType == 'sg') {
			reloadFormSelection(1);
			reloadSubjectSelection();
		}
	}
}

function reloadClassSelection()
{
	$('span#TargetSourceSelectionSpan').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"../../ajax_reload.php", 
		{ 
			Action: 'Class_Selection',
			SelectionID: 'TargetID[]',
			NoFirst: 1,
			IsMultiple: 1
		},
		function(ReturnData) {
			SelectAll(document.getElementById('TargetID[]'));
		}
	);
}

function reloadFormSelection(jsForSubjectGroup) {
	jsForSubjectGroup = jsForSubjectGroup || 0;
	
	var jsTargetSpan, jsSelectionID, jsOnchange;
	
	if (jsForSubjectGroup == 1) {
		jsTargetSpan = 'TargetFormSelectionSpan';
		jsSelectionID = 'YearIDArr[]';
		jsOnchange = 'reloadSubjectGroupSelection();';
	}
	else {
		jsTargetSpan = 'TargetSourceSelectionSpan';
		jsSelectionID = 'TargetID[]';
		jsOnchange = '';
	}
	
	var form_obj = document.form1;
	$('span#' + jsTargetSpan).html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"../../ajax_reload.php", 
		{ 
			Action: 'Form_Selection',
			SelectionID: jsSelectionID,
			Onchange: jsOnchange,
			NoFirst: 1,
			IsMultiple: 1
		},
		function(ReturnData) {
			SelectAll(document.getElementById(jsSelectionID));
		}
	);
}

function reloadSubjectSelection()
{
	$('span#TargetSourceSelectionSpan').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"../../ajax_reload.php", 
		{ 
			Action: 'Subject_Selection',
			SelectionID: 'SubjectIDArr[]',
			SubjectID: '',
			YearTermID: curTermId,
			OnChange: 'changedSubjectSelection();',
			IsMultiple: 1,
			NoFirst: 1
		},
		function(ReturnData) {
			SelectAll(document.getElementById('SubjectIDArr[]'));
			reloadSubjectGroupSelection();
		}
	);
}

function changedSubjectSelection() {
	reloadSubjectGroupSelection();
} 

function reloadSubjectGroupSelection()
{
	$('span#TargetSubjectGroupSelectionSpan').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"../../ajax_reload.php", 
		{ 
			Action: 'Subject_Group_Selection',
			SelectionID: 'TargetID[]',
			SubjectIDList: $('select#SubjectIDArr\\[\\]').val().toString(),
			YearTermID: curTermId,
			IsMultiple: 1
		},
		function(ReturnData) {
			SelectAll(document.getElementById('TargetID[]'));
		}
	);
}

function SelectAll(obj) {
     for (i=0; i<obj.length; i++) {
          obj.options[i].selected = true;
     }
}

function checkForm() {
	$('div.warnMsgDiv').hide();
	var canSubmit = true;
	
	if ($('input.ReportColumnChk:checked').length == 0) {
		$('div#reportColumnEmptyWarnDiv').show();
		$('input#ReportColumn_All').focus();
		canSubmit = false;
	}
	
	if (isObjectValueEmpty('TargetID[]')) {
		var reportType = getReportType();
		var warnDivId = '';
		
		if (reportType == 'class') {
			warnDivId = 'targetClassEmptyWarnDiv';
		}
		else if (reportType == 'form') {
			warnDivId = 'targetFormEmptyWarnDiv';
		}
		else if (reportType == 'sg') {
			warnDivId = 'targetSubjectGroupEmptyWarnDiv';
		}
		$('div#' + warnDivId).show();
		
		var targetSelId = getJQuerySaveId('TargetID[]');
		$('select#'+targetSelId).focus();
		
		canSubmit = false;
	}
		
	if (isObjectValueEmpty('fullMarkPercentageTb') || $('input#fullMarkPercentageTb').val() <= 0) {
		$('div#percentageInvalidWarnDiv').show();
		$('input#fullMarkPercentageTb').focus();
		canSubmit = false;
	}
	
	if (canSubmit) {
		$('form#form1').submit();
	}
}

</script>
<form id="form1" name="form1" method="post" target="_blank" action="report.php">
	<div class="table_board">
		<table id="html_body_frame" style="width:100%;">
			<tr><td><?=$htmlAry['optionTable']?></td></tr>
		</table>
	</div>
	<div class="edit_bottom_v30">
		<br />
		<?=$htmlAry['submitBtn']?>
	</div>
</form>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>