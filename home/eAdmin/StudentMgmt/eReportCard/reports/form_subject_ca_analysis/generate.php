<?
//using: Bill

//@SET_TIME_LIMIT(1000);

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$lreportcard = new libreportcardcustom();
$lreportcard_ui = new libreportcard_ui();
     
$x = "";
$emptyDisplay = "---";
$exportArr = array();
$exportColumn = array();

$isFirst = true;
$table_count = 0;

$SubMSScoreArr = array();
$SubMSStatArr = array();
$reportColumnsArr = array();
$TermReportColumnArr = array();
$statTitleArr = array('最高分', '最低分', '平均分', '及格率');

# POST
$YearID = $_POST['YearID'];
$ReportID = $_POST['ReportID'];
$SubjectIDArr = $_POST['SubjectIDArr'];
$exportCSV = $_POST['submit_type'];

// [2015-1209-1703-15206]
if ($_SESSION["SSV_PRIVILEGE"]["reportcard"]["is_subject_panel"] || $lreportcard->hasAccessRight()) {
     	
    # Report Template Info
    $ReportTemplate = $lreportcard->returnReportTemplateBasicInfo($ReportID);
    $ReportSemester = $ReportTemplate['Semester'];
     	     	
	# Semester
    $Semester = $lreportcard->GET_ALL_SEMESTERS();
    if($ReportSemester != 'F'){
       	$Semester = array();
		$Semester[$ReportSemester] = $lreportcard->GET_ALL_SEMESTERS($ReportSemester);
	}
	
	$currentForm = $lreportcard->returnClassLevel($YearID);
    $Subjects = $lreportcard->returnSubjectwOrder($YearID, 0, '', 'b5', '', 0, $ReportID, 0);
	
	# Year Class
	$YearClassNameArr = $lreportcard->GET_CLASSES_BY_FORM($YearID, "", 1, 1);
	$YearClassIDArr = array_keys($YearClassNameArr); 
	foreach($YearClassIDArr as $YearClassID){
		$students = $lreportcard->GET_STUDENT_BY_CLASS($YearClassID, $ParStudentIDList="", $isShowBothLangs=0, $withClassNumber=0, $isShowStyle=0, $ReturnAsso=1);
		$classStudentArr[$YearClassID] = $students;
	}
	
	# Related Reports
	$relatedReports = $lreportcard->Get_Related_TermReport_Of_Consolidated_Report($ReportID, $MainReportOnly=1, $ReportColumnID="ALL");
	// For term report, only process 1 report 
	if($ReportTemplate['Semester'] != 'F'){
		$relatedReports = array();
		$relatedReports[0] = $ReportTemplate;
	}
	
	# Loop Reports
	// Build settings array
	for($i=0; $i<count($relatedReports); $i++)
	{
		# Related Report Info
		$relatedReportID = $relatedReports[$i]['ReportID'];
		$TermID = $relatedReports[$i]['Semester'];
		
		# ReportColumns
		$ReportColumn = $lreportcard->returnReportTemplateColumnData($relatedReportID);
		$TermReportColumnArr[$TermID] = BuildMultiKeyAssoc($ReportColumn, array('ReportColumnID'));
		
		foreach ((array)$ReportColumn as $ReportColumnObj)
		{
			# ReportColumn Info
			$ReportColumnID = $ReportColumnObj['ReportColumnID'];
			$ReportColumnInfo = $lreportcard->GET_SUB_MARKSHEET_COLUMN_INFO('', $ReportColumnID);
					
			# Loop Submarksheet Columns
			for($j=0; $j<count($ReportColumnInfo); $j++)
			{
				# Submarksheet Column Info
				$columnSettings = $ReportColumnInfo[$j];
				$columnID = $columnSettings['ColumnID'];
				$SubjectID = $columnSettings['SubjectID'];
				
				$reportColumnsArr[$TermID][$ReportColumnID][$SubjectID][] = $columnSettings;
				
				# Set Parent and Component Subject ID
				$componentSubjectID = 0;
				$parentSubjectID = $lreportcard->GET_PARENT_SUBJECT_ID($SubjectID);
				// for component subject
				if($parentSubjectID){
					$componentSubjectID = $SubjectID;
					$SubjectID = $parentSubjectID;
				}
				
				// if current subject not in selected subjects
				if(!in_array($SubjectID, $SubjectIDArr))
					continue;
							
				# Submarksheet Score 
				$SubMSScore = $lreportcard->GET_SUB_MARKSHEET_SCORE($ReportColumnID, ($componentSubjectID? $componentSubjectID : $SubjectID));
				$SubMSScoreArr[$TermID][$ReportColumnID][$SubjectID][$componentSubjectID] = $SubMSScore;
						
				foreach((array)$YearClassIDArr as $YearClassID){
					$studentList = array_keys((array)$classStudentArr[$YearClassID]);
					
					$Marks = array();
					$minMark = 0;
					$maxMark = 0;
					$passRatio = 0.00;
					$passStudent = 0;
					$averageMark = 0.00;
					$totalStudent = 0;
					foreach((array)$studentList as $currentStudentID){
						$studentInput = $SubMSScore[$currentStudentID];

						$score = $studentInput[$columnID];
						if(isset($score) && $score != '-3'){
							$Marks[] = $score;
							$totalStudent++;
							
							if($score >= $columnSettings['PassMark']){
								$passStudent++;
							}
						}
					}
					
					if(count($Marks) > 0){
						$minMark = min($Marks);		
						$maxMark = max($Marks);
						$averageMark = $lreportcard->getAverage($Marks);
						$averageMark = my_round($averageMark, 2);
						$passRatio = $passStudent / $totalStudent * 100;
						$passRatio = my_round($passRatio, 2);
					}
					$SubMSStatArr[$YearClassID][$TermID][$SubjectID][$componentSubjectID][$columnID] = array($maxMark, $minMark, $averageMark, $passRatio);
				}
			}
		}
	}
	
//	$x .= "<div class='main_container'>";
	
	foreach((array)$reportColumnsArr as $termID => $columnInfo){
		$SemesterSq = $lreportcard->Get_Semester_Seq_Number($termID);
		$assessmentSq = 1;
		
		foreach((array)$columnInfo as $currentReportColumnID => $currentReportColumn){
				
			foreach((array)$SubjectIDArr as $parentSubjectID){
				$componentSubject = $Subjects[$parentSubjectID];
				$parentSubjectName = $componentSubject[0];
					
				foreach((array)$componentSubject as $subjectComponentID => $subjectName){
					$currentReportColumnName = $TermReportColumnArr[$termID][$currentReportColumnID]['ColumnTitle'];
					$currentColumn = $currentReportColumn[($subjectComponentID? $subjectComponentID : $parentSubjectID)];
					$assessmentCode = "T".$SemesterSq."A".$assessmentSq;
					
					$displayTable = "";
					
					if(count($componentSubject) > 1 && !$subjectComponentID)
						continue;
					
					if(count($currentColumn) < 1)
						continue;
					
//					if($table_count != 0 && !($table_count % 2)){
//						$x .= "</div>";
//						$x .= "<div class='page_break'>&nbsp;</div>";
//						$x .= "<div class='main_container'>";
//					}
//					$table_count++;
					
					# 1st Header [Start]
					$x .= "<div class='main_container'>";
					$x .= "<table class='border_table'>";
					$x .= "<tr>";
					$x .= "<td>".$Semester[$termID]."<br>$currentReportColumnName<br>($assessmentCode)</td><td>$subjectName</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>";
					$x .= "</tr>";
					$dataArr = array();
					$dataArr[] = $Semester[$termID].$currentReportColumnName."(".$assessmentCode.")";
					$dataArr[] = $subjectName;
					if($isFirst){
						$exportColumn = $dataArr;
						$isFirst = false;
					}
					else {
						$exportArr[] = $dataArr;
					}
					# 1st Header [End]
				
					# 2nd Header [Start]
					$x .= "<tr>";
					$x .= "<td>$parentSubjectName</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>";
					$x .= "</tr>";
					$dataArr = array();
					$dataArr[] = $parentSubjectName;
					$exportArr[] = $dataArr;
					# 2nd Header [End]
										
					foreach((array)$currentColumn as $columnContent){
						$reportColumnName = $columnContent['ColumnTitle'];
						$currentColumnID = $columnContent['ColumnID'];
						
						# CA Header [Start]
						$x .= "<tr>";
						$x .= "<td>$reportColumnName</td>";
						$dataArr = array();
						$dataArr[] = $reportColumnName;
						for($i=0; $i<4; $i++){
							$statTitle = $statTitleArr[$i];
							$x .= "<td>$statTitle</td>";
							$dataArr[] = $statTitle;
						}
						$exportArr[] = $dataArr;
						$x .= "</tr>";
						# CA Header [End]
						
						foreach((array)$YearClassIDArr as $YearClassID){
							$currentClassName = $YearClassNameArr[$YearClassID];
						
							$x .= "<tr>";
							$x .= "<td>$currentClassName</td>";
							$dataArr = array();
							$dataArr[] = $currentClassName;
							for($i=0; $i<4; $i++){
								$statDisplay = $SubMSStatArr[$YearClassID][$termID][$parentSubjectID][$subjectComponentID][$currentColumnID][$i];
								$statDisplay = isset($statDisplay)? $statDisplay : $emptyDisplay;
								$statDisplay .= ($i==3? "%" : "");
								$x .= "<td align='center'>$statDisplay</td>";
								$dataArr[] = $statDisplay;
							}
							$exportArr[] = $dataArr;
							$x .= "</tr>";
						}
					}
					$x .= "</table>";
					$x .= "</div>";
					$x .= "<div class='page_break'>&nbsp;</div>";
					$exportArr[] = array("");
				}
			}
			$assessmentSq++;
		}
	}
	
	if($exportCSV == "1"){
		$lexport = new libexporttext();
		$export_content = $lexport->GET_EXPORT_TXT($exportArr, $exportColumn,  "", "\r\n", "", 0, "11",1);		
		
		intranet_closedb();
		
		// Output CSV File
		$filename = "Form_Subject_CA_Result_Analysis.csv";
		$filename = str_replace(' ', '_', $filename);
		$lexport->EXPORT_FILE($filename, $export_content);
	
	} else {
		
//		if(!($table_count % 2)){
//			$x .= "</div>";
//		}
		echo $x;
				
		echo'<style>';
		include_once($PATH_WRT_ROOT."file/reportcard2008/templates/general.css");
		echo '</style>';	
		
		$style_css  = "html, body { margin:0px; padding:0px; }\n";
		$style_css .= "body { font-family:'Times New Roman, Times, serif, 新細明體'; font-size:13px }\n";
//		$style_css .= ".main_container {display:block; width:750px; height:850px; margin:auo; padding:120px 20px; position:relative; padding-bottom:100px; clear:both; border:0px solid green;}\n";
		$style_css .= ".main_container { display:block; width:680px; height:850px; margin:auto; padding:60px 20px; position:relative; padding-bottom:20px; clear:both; border:0px solid green; }\n";
		$style_css .= ".page_break {page-break-after:always; margin:0; padding:0; line-height:0; font-size:0; height:0;}";
		
		$style_css .= ".border_table { width: 100%; margin-bottom:60px; }";
		$style_css .= ".border_table td { width:20%; padding-left:4px; }";
		$style_css .= ".border_table > tbody > tr:first-child td { height:45px; vertical-align:middle; }";
		
		echo "<style>";
		echo $style_css;
		echo "</style>";		
		
		intranet_closedb();
	}
	
} else {
	
	echo "You have no priviledge to access this page.";
	intranet_closedb();
	die();
	
}

?>