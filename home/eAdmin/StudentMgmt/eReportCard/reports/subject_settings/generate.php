<?
//using: Bill

//@SET_TIME_LIMIT(1000);

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$lreportcard = new libreportcardcustom();
$lreportcard_ui = new libreportcard_ui();

$x = "";
$emptyDisplay = "---";
$exportArr = array();
$exportColumn = array();

$isFirst = true;
$table_count = 0;

$allReportColumns = array();
$subMSweightSettings = array();
$subjectSettingsArr = array();

# POST
$YearID = $_POST['YearID'];
$ReportID = $_POST['ReportID'];
$SubjectIDArr = $_POST['SubjectIDArr'];
$exportCSV = $_POST['submit_type'];


// [2015-1209-1703-15206]
# Subject Panel / Has Access Right
if ($_SESSION["SSV_PRIVILEGE"]["reportcard"]["is_subject_panel"] || $lreportcard->hasAccessRight()){
	        
     # Report Info
     $ReportTemplateInfo = $lreportcard->returnReportTemplateBasicInfo($ReportID);
     $ReportSemester = $ReportTemplateInfo['Semester'];
		
	# Related Info
    $SemesterArr = $lreportcard->GET_ALL_SEMESTERS();
    if($ReportSemester != 'F'){
       	$SemesterArr = array();
		$SemesterArr[$ReportSemester] = $lreportcard->GET_ALL_SEMESTERS($ReportSemester);
	}
		
	$currentForm = $lreportcard->returnClassLevel($YearID);
    $Subjects = $lreportcard->returnSubjectwOrder($YearID, 0, '', 'b5', '', 0, $ReportID, 0);
		
	# Get Related Term Report
	$relatedReportArr = $lreportcard->Get_Related_TermReport_Of_Consolidated_Report($ReportID, $MainReportOnly=1, $ReportColumnID="ALL");
	// For term report, only process current report 
	if($ReportTemplateInfo['Semester'] != 'F'){
		$relatedReportArr = array();
		$relatedReportArr[0] = $ReportTemplateInfo;
	}
		
	# Loop Related Reports
	// Build settings array
	for($i=0; $i<count($relatedReportArr); $i++)
	{
		# Related Report Info
		$relatedReportID = $relatedReportArr[$i]['ReportID'];
		$TermID = $relatedReportArr[$i]['Semester'];
			
		# ReportColumns
		$ReportColumnArr = $lreportcard->returnReportTemplateColumnData($relatedReportID);
		$allReportColumns[$TermID] = $ReportColumnArr;
			
		$subMSweightSettings[$TermID] = BuildMultiKeyAssoc($lreportcard->returnReportTemplateSubjectWeightData($relatedReportID), array('SubjectID', 'ReportColumnID'));
		
		# Loop ReportColumns
		foreach ($ReportColumnArr as $currentReportColumn)
		{
			# Report Column Info
			$ReportColumnID = $currentReportColumn['ReportColumnID'];
			$ReportColumnInfo = $lreportcard->GET_SUB_MARKSHEET_COLUMN_INFO('', $ReportColumnID);
				
			# Loop Submarksheet Columns
			for($j=0; $j<count($ReportColumnInfo); $j++)
			{
				# Submarksheet Column Info
				$columnSettings = $ReportColumnInfo[$j];
				$SubjectID = $columnSettings['SubjectID'];
					
				# Set Parent and Component Subject ID
				$componentSubjectID = 0;
				$parentSubjectID = $lreportcard->GET_PARENT_SUBJECT_ID($SubjectID);
				// for component subject
				if($parentSubjectID){
					$componentSubjectID = $SubjectID;
					$SubjectID = $parentSubjectID;
				}
				
				# Settings array
				// for Subject Submarksheet Column Settings data
				$subjectSettingsArr[$TermID][$ReportColumnID][$SubjectID][$componentSubjectID][] = $columnSettings;
			}
		}
	}
	
	$x .= "<div class='main_container'>";
		
	# Loop Subjects
	foreach($SubjectIDArr as $currentSubjectID)
	{
		# Selected Subject Info
		$currentMajorSubject = $Subjects[$currentSubjectID];
		$componentSubjectCount = count($currentMajorSubject);
		$currentMajorSubjectName = $currentMajorSubject[0];
		
		if($table_count != 0 && !($table_count % 2)){
			$x .= "</div>";
			$x .= "<div class='page_break'>&nbsp;</div>";
			$x .= "<div class='main_container'>";
		}
		$table_count++;
		
		$x .= "<table class='border_table' width='100%'>";
		$x .= "<tr>";
		$x .= "<td width=16%'>$currentMajorSubjectName</td>";
		$x .= "<td colspan='2' width='38%'>$currentForm</td>";
		$x .= "<td width='8%'>滿分</td>";
		$x .= "<td width='8%'>比重</td>";
		$x .= "<td width='15%'>卷別比重</td>";
		$x .= "<td width='15%'>學期比重</td>";
		$x .= "</tr>";	
		$dataArr = array();
		$dataArr[] = $currentMajorSubjectName;
		$dataArr[] = $currentForm;
		$dataArr[] = "";
		$dataArr[] = "滿分";
		$dataArr[] = "比重";
		$dataArr[] = "卷別比重";
		$dataArr[] = "學期比重";
		if($isFirst){
			$exportColumn = $dataArr;
			$isFirst = false;
		}
		else {
			$exportArr[] = $dataArr;
		}
		
		# Loop Semesters
		foreach($SemesterArr as $SemesterID => $currentSemester)
		{
			$currentColumns = $allReportColumns[$SemesterID];
			$TermSquence = $lreportcard->Get_Semester_Seq_Number($SemesterID);
			
			# Loop ReportColumns
			for($col=0; $col<count($currentColumns); $col++)
			{
				# Report Column Info
				$currentColumn = $currentColumns[$col];
				$currentColumnID = $currentColumn['ReportColumnID'];
				$currentColumnTitle = $currentColumn['ColumnTitle'];
				
				$assessmentCode = "(T".$TermSquence."A".($col+1).")";
				
				// Calculate Rowspan
				$termCount = 0;
				foreach($currentMajorSubject as $componentSubjectID => $currentComponentSubject){
					if(!$componentSubjectID && $componentSubjectCount != 1)
						continue;
					$currentSubjectSettings = $subjectSettingsArr[$SemesterID][$currentColumnID][$currentSubjectID][$componentSubjectID];
					$componentCount = count($currentSubjectSettings);
					$termCount += $componentCount? $componentCount : 1;
				}
				$termCount = $termCount? $termCount : 1;
					
				# Semester Cell Display
				$displaySemesterCell = "<td rowspan='$termCount'>$currentSemester<br>$currentColumnTitle<br>$assessmentCode</td>";
				
				$firstSubject = true;
				# Loop Component Subject
				foreach($currentMajorSubject as $componentSubjectID => $currentComponentSubject)
				{						
					# Set subject name as 主卷  if major subject
					if(!$componentSubjectID){
						$currentComponentSubject = "總分";
					}
					
					# Hide Major Subject if with component subject
					if(!$componentSubjectID && $componentSubjectCount != 1){
						continue;
					}
					
					# Get Ratio Settings
					$componentRatio = $subMSweightSettings[$SemesterID][$componentSubjectID][null]['Weight'];
					if(!$componentSubjectID){
						$componentRatio = 1;
					}
					$termRatio = $subMSweightSettings[$SemesterID][$currentSubjectID][$currentColumnID]['Weight'];
					
					# Term Ratio Cell Display
					if($displaySemesterCell != "&nbsp;"){
						$displayTermRatioCell = "<td rowspan='$termCount'> ".($termRatio * 100)." </td>";
					}
					 
					# Get Subject Submarksheet Column Settings
					$currentSubjectSettings = $subjectSettingsArr[$SemesterID][$currentColumnID][$currentSubjectID][$componentSubjectID];
					$componentSettingsCount = count($currentSubjectSettings);
					if($componentSettingsCount == 0){
						$displayComponentSubject = "<td colspan='2'>$currentComponentSubject</td>";
						$x .= "<tr>$displaySemesterCell$displayComponentSubject<td colspan='2' width='16%'>無記錄</td><td>$componentRatio</td>$displayTermRatioCell</tr>";
						$dataArr = array();
						$dataArr[] = $firstSubject? $currentSemester.$currentColumnTitle.$assessmentCode : "";
						$dataArr[] = $currentComponentSubject;
						$dataArr[] = "";
						$dataArr[] = "無記錄";
						$dataArr[] = "";
						$dataArr[] = $componentRatio;
						$dataArr[] = $displaySemesterCell != "&nbsp;"? ($termRatio * 100) : "";
						$exportArr[] = $dataArr;
						$firstSubject = false;
					}
						
					// Loop Subject SubMarksheet Column Settings
					for($i=0; $i<$componentSettingsCount; $i++)
					{
						# Subject Submarksheet Column Settings
						$currentComponentSubjectSettings = $currentSubjectSettings[$i];
						$CATitle = $currentComponentSubjectSettings['ColumnTitle'];
						$CATitle = $CATitle? $CATitle : "&nbsp;";
						$fullMark = $currentComponentSubjectSettings['FullMark'];
						$ratio = $currentComponentSubjectSettings['Ratio'];
						
						# Component Subject and Ratio Display
						if($i==0){
							$displayComponentSubject = "<td rowspan='$componentSettingsCount'>$currentComponentSubject</td>";
							$displayComponentRatio = "<td rowspan='$componentSettingsCount'>$componentRatio</td>";
						} else {
							$displayComponentSubject = "&nbsp;";
							$displayComponentRatio = "&nbsp;";
						}
						
						$x .= "<tr> $displaySemesterCell $displayComponentSubject <td> $CATitle </td> <td width='8%'> $fullMark </td> <td width='8%'> $ratio </td> $displayComponentRatio $displayTermRatioCell </tr>";
						$dataArr = array();
						$dataArr[] = $firstSubject? $currentSemester.$currentColumnTitle.$assessmentCode : "";
						$dataArr[] = $displayComponentSubject != "&nbsp;"? $currentComponentSubject : "";
						$dataArr[] = $CATitle;
						$dataArr[] = $fullMark;
						$dataArr[] = $ratio;
						$dataArr[] = $displayComponentRatio != "&nbsp;"? $componentRatio : "";
						$dataArr[] = $displayTermRatioCell != "&nbsp;"? ($termRatio * 100) : "";
						$exportArr[] = $dataArr;
						$firstSubject = false;
						
						$displaySemesterCell = "&nbsp;";
						$displayTermRatioCell = "&nbsp;";
					}
					$displaySemesterCell = "&nbsp;";
					$displayTermRatioCell = "&nbsp;";
				}
			}
		}
		$x .= "</table>";
		$exportArr[] = array(""); 
	}
	
	if($exportCSV == "1"){
		$lexport = new libexporttext();
		$export_content = $lexport->GET_EXPORT_TXT($exportArr, $exportColumn,  "", "\r\n", "", 0, "11",1);		
		
		intranet_closedb();
		
		// Output CSV File
		$filename = "Subject_Component_Settings.csv";
		$filename = str_replace(' ', '_', $filename);
		$lexport->EXPORT_FILE($filename, $export_content);
	
	} else {
		
		$x .= "</div>";
		echo $x;
		
		echo'<style>';
		include_once($PATH_WRT_ROOT."file/reportcard2008/templates/general.css");
		echo '</style>';	
		
		$style_css  = "html, body { margin:0px; padding:0px; }\n";
		$style_css .= "body { font-family:'Times New Roman, Times, serif, 新細明體'; font-size:13px }\n";
//		$style_css .= ".main_container {display:block; width:750px; height:850px; margin:auo; padding:120px 20px; position:relative; padding-bottom:100px; clear:both; border:0px solid green;}\n";
		$style_css .= ".main_container { display:block; width:680px; height:850px; margin:auto; padding:60px 20px; position:relative; padding-bottom:20px; clear:both; border:0px solid green; }\n";
		$style_css .= ".page_break {page-break-after:always; margin:0; padding:0; line-height:0; font-size:0; height:0;}";
		
		$style_css .= ".border_table { width: 100%; margin-bottom:60px; }";
		$style_css .= ".border_table td { padding-left:4px; }";
		$style_css .= ".border_table > tbody > tr:first-child td { vertical-align:middle; }";
		
		echo "<style>";
		echo $style_css;
		echo "</style>";		
		
		intranet_closedb();
	}
	
} else {
	
	echo "You have no priviledge to access this page.";
	intranet_closedb();
	die();
	
}

?>