<?php
@SET_TIME_LIMIT(21600);
@ini_set(memory_limit, -1);

# Editing by connie

/****************************************************
 *
 *	Date:	2020-02-26  (Philips) [2020-0122-1154-15073]
 *			Use Highchart instead of flash chart
 *  Date:   2020-02-21  (Bill)
 *          fixed PHP 5.4 error msg
 *
 * **************************************************/

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
//include_once($PATH_WRT_ROOT."includes/libteaching.php");
//include_once($PATH_WRT_ROOT."includes/libuser.php");
//include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();
 
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/flashchart_basic/php-ofc-library/open-flash-chart.php");
include_once($PATH_WRT_ROOT."templates/{$LAYOUT_SKIN}/layout/print_header.php");		

$lreportcard = new libreportcardcustom();
$lreportcard->hasAccessRight();

$curTimestamp = time();
$CSS_link = $PATH_WRT_ROOT."templates/2009a/css/reportcard/report_subject_academic_result.css";

### prepare tmp folder
$fs = new libfilesystem();
$folder_prefix = $intranet_root."/file/reportcard2008/tmp_chart/subject_academic_result/".$_SESSION['UserID']."/";

### Clear the tmp png folder
if (file_exists($folder_prefix)) {
	$fs->folder_remove_recursive($folder_prefix);
}

### Create the tmp png folder if not exist
//if (!file_exists($folder_prefix))
//{
	$folder_prefix .= $curTimestamp.'/';
	$fs->folder_new($folder_prefix);
	chmod($folder_prefix, 0777);
//}

$Post_Arr = array();
$Post_Arr['ClassLevelID'] = $_POST['ClassLevelID'];
$Post_Arr['ReportID'] = $_POST['ReportID'];
$Post_Arr['ReportColumnID'] = $_POST['ReportColumnID'];
$Post_Arr['SelectFrom'] = $_POST['SelectFrom'];  //subjectgroup or class
$Post_Arr['YearClassIDArr'] = $_POST['YearClassIDArr'];
$Post_Arr['TermID'] = $_POST['TermID'];
$Post_Arr['SubjectGroupIDArr'] = $_POST['SubjectGroupIDArr'];
//$Post_Arr['StudentIDArr'] = $_POST['StudentIDArr'];
$Post_Arr['SubjectIDArr'] = $_POST['SubjectIDArr'];

$lreportcard_ui = new libreportcard_ui();

### Get Personal CharSource ReportID
$YearTermID = $ReportInfoArr['Semester'];
$ReportType = ($YearTermID == 'F') ? 'W' : 'T';
if ($ReportType == 'T') {
	$PersonalCharSourceReportID = $ReportID;
}
else if ($ReportType == 'W') {
	$TermReportInfoArr = $this->Get_Related_TermReport_Of_Consolidated_Report($ReportID, $MainReportOnly=1, $ParReportColumnID="ALL");
	$TermReportIDArr = Get_Array_By_Key($TermReportInfoArr, 'ReportID');
	$numOfTermReport = count($TermReportInfoArr);
	
	$LastTermReportID = $TermReportIDArr[$numOfTermReport - 1];
	$ReportColumnInfoArr = $this->returnReportTemplateColumnData($LastTermReportID);
	
	$PersonalCharSourceReportID = $LastTermReportID;
}

$StudentPersonalCharInfoArr = $lreportcard->getPersonalCharacteristicsProcessedDataByBatch($StudentIDArr, $PersonalCharSourceReportID, $SubjectIDArr, $ReturnGradeCode=0, $ReturnCharID=1, $ReturnComment=1);
$chartArr = array();

$html_UI = '';
$SubjectAcademicResultReport = $lreportcard_ui->Get_SubjectAcademicResultReport($Post_Arr,$StudentPersonalCharInfoArr,$intranet_session_language,$chartArr);
$html_UI =  $SubjectAcademicResultReport;
//$chartWidth = 300;
//$chartHeight = 315;
$chartWidth = 250;
$chartHeight = 280;



# 20200226 (Philips) - Highchart replace
$libui = new interface_html();
echo $libui->Include_HighChart_JS();
//debug_r($chartArr);
?>

<link href="<?=$CSS_link?>" rel="stylesheet" type="text/css"/>
<?php /*?>
<!--
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>includes/flashchart_basic/js/json/json2.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>includes/flashchart_basic/js/swfobject.js"></script>	
<script type="text/javascript">
	function ofc_ready(){
		//alert('ofc_ready');
	}

	function open_flash_chart_data(id){
		return JSON.stringify(dataArr[id]);
	}

	function findSWF(movieName) {
	  if (navigator.appName.indexOf("Microsoft")!= -1) {
        return window[movieName];
	  }
	  else {
		return document[movieName];
	  }
	}

	function onAutoPostImgBinary(id){
		//alert("image binary string is automatically posted and flash id: "+id);
		var swfDIV = document.getElementById("div"+id+"parent");
		//var swfDIV = document.getElementById("img"+id);
		//alert("swfDIV: "+swfDIV+" / width: "+swfDIV.width+" / height: "+swfDIV.height);
		while(swfDIV.hasChildNodes()) {
            swfDIV.removeChild(swfDIV.firstChild);
        }

		var img = document.createElement("img");
		//alert('id = ' + id);
		img.setAttribute("src", "<?=$intranet_httppath?>/file/reportcard2008/tmp_chart/subject_academic_result/<?=$_SESSION['UserID']?>/<?=$curTimestamp?>/tmp_chart_png_"+id+".png");
		img.setAttribute("width", "<?=$chartWidth?>");
		img.setAttribute("height", "<?=$chartHeight?>");
		swfDIV.appendChild(img);
	}
	
	var chartNum = <?=count($chartArr)?>;
//	alert("chartNum: "+chartNum);
	
	var dataArr = new Array();
	<?php
		foreach( (array)$chartArr as $chartID => $chartJSON ) {
			echo "dataArr[ ".$chartID." ] = ".$chartJSON->toPrettyString()."\n";
		}
	?>
	
//	alert("dataArr length: "+dataArr.length);
</script>
	
<script type="text/javascript">
	var chartNum = <?=count($chartArr)?>;
	for(var k=0; k<chartNum; k++){
		var flashvars = {id:k, postImgUrl:"uploadFlashImage.php?curTimestamp=<?=$curTimestamp?>"};
        swfobject.embedSWF("<?=$PATH_WRT_ROOT?>includes/flashchart_basic/open-flash-chart.swf", "div"+k, "<?=$chartWidth?>", "<?=$chartHeight?>", "9.0.0", "", flashvars);
        //swfobject.embedSWF("<?//=$PATH_WRT_ROOT?>//includes/flashchart_basic_201301_in_use/open-flash-chart.swf", "div"+k, "<?//=$chartWidth?>//", "<?//=$chartHeight?>//", "9.0.0", "", flashvars);
		//swfobject.embedSWF("<?=$PATH_WRT_ROOT?>includes/flashchart_basic/open-flash-chart.swf", "div"+k, "<?=$chartWidth?>","<?=$chartHeight?>", "9.0.0", "expressInstall.swf", flashvars);
	}//end for
</script>
-->
<?php */?>
<script type="text/javascript">
$(document).ready(function(){
	<?php foreach( (array)$chartArr as $chartScript){
		echo $chartScript;
	}?>
});
</script>
<html>
<body>
<?=$html_UI?>
</body>
</html> 