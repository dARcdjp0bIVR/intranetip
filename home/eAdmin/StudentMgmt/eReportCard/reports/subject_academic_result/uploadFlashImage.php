<?php
// Using:
/*
 *  Date :  2020-02-21 (Bill)
 *          - added intranet_auth()
 */

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();

############################################################################
# UPLOAD
############################################################################

//if(!is_null($_GET['imgDir'])){
//	$imgdir = $_GET['imgDir'];
//}else{
	$curTimestamp = $_GET['curTimestamp'];
	$imgdir = $intranet_root."/file/reportcard2008/tmp_chart/subject_academic_result/".$_SESSION['UserID']."/".$curTimestamp."/";	
//}

if (!is_null($_FILES['Filedata'])){
	if (!is_dir($imgdir)){ 
		mkdir($imgdir, 0777);
	}

	move_uploaded_file($_FILES['Filedata']['tmp_name'], $imgdir.basename($_FILES['Filedata']['name']));  
	chmod($imgdir.$_FILES['Filedata']['name'], 0777); 

	if($imgdir.basename($_FILES['Filedata']['name'])){
		echo "success"; //let actionscript knows
	}

}else{
	//error - don't echo anything, if not, means success
 // echo 'fail';
}

?>