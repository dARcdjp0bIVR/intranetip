<?php
// using ivan
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");

intranet_auth();
intranet_opendb();


# Get data
$userLogin = stripslashes(urldecode($_REQUEST['q']));


### Get Select User and exclude them in the search result
$luser = new libuser();
$lreportcard = new libreportcard();

$studentIdAry = Get_Array_By_Key($luser->getUserInfoByLogin($userLogin, USERTYPE_STUDENT), 'UserID');
$numOfStudent = count($studentIdAry);

$returnString = '';
for ($i=0; $i<$numOfStudent; $i++) {
	 $_studentInfoAry = $lreportcard->Get_Student_Class_ClassLevel_Info($studentIdAry[$i]);
	 
	 if (isset($_studentInfoAry[0])) {
	 	 $_studentName = Get_Lang_Selection($_studentInfoAry[0]['ChineseName'], $_studentInfoAry[0]['EnglishName']);
		 $_className = Get_Lang_Selection($_studentInfoAry[0]['ClassNameCh'], $_studentInfoAry[0]['ClassName']);
		 $_classNumber = $_studentInfoAry[0]['ClassNumber'];
		 $_userLogin = $_studentInfoAry[0]['UserLogin'];
		 $_studentDisplay = '['.$_userLogin.'] '.$_studentName.' ('.$_className.' - '.$_classNumber.')';
		 
		 $returnString .= $_studentDisplay.'||'.$_userLogin."\n";
	 }
}

intranet_closedb();



echo $returnString;
?>