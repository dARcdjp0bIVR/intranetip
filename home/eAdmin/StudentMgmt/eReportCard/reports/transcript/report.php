<?php
# Editing by 
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();
 
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");

$lreportcard = new libreportcardcustom();
$lreportcard_ui = new libreportcard_ui();
$linterface = new interface_html();

$lreportcard->hasAccessRight();

$x = '';
$x .= '<table border="0" cellpadding="4" width="95%" cellspacing="0" class="print_hide">'."\n";
	$x .= '<tr><td align="right">'.$linterface->GET_BTN($Lang['Btn']['Print'], "button", "javascript:window.print();","submit2").'</span></td></tr>'."\n";
$x .= '</table>'."\n";
$html['printBtn'] = $x;



include_once($PATH_WRT_ROOT."templates/{$LAYOUT_SKIN}/layout/print_header_erc.php");
echo $lreportcard->Get_GrandMS_CSS();

echo $html['printBtn'];
if (file_exists($ReportCardCustomSchoolName.'/report.php')) {
	include_once($ReportCardCustomSchoolName.'/report.php');
}
echo $html['printBtn'];

include_once($PATH_WRT_ROOT."templates/{$LAYOUT_SKIN}/layout/print_footer_erc.php");
intranet_closedb();
?>