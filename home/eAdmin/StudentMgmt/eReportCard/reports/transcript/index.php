<?php
# Editing by

/**************************************************
 * 	Modification log
 *  20190625 Bill	[2017-0901-1525-31265]
 * 		- Support Date of Graduation input   ($eRCTemplateSetting['Report']['TranscriptwDateGraduation'])
 *  20190211 Bill	[2017-0901-1525-31265]
 * 		- Support Date of Leaving input      ($eRCTemplateSetting['Report']['TranscriptwDateLeaving'])
 * 	20180723 Bill:  [2017-0901-1525-31265]
 * 		Allow View Group to select all options
 *  20180226 Bill	[2017-0901-1525-31265]
 * 		- Support PDF mode                  ($eRCTemplateSetting['Report']['TranscriptByPDF'])
 *      - Support Principal Name input      ($eRCTemplateSetting['Report']['TranscriptwPrincipalName'])
 * ***********************************************/

$PageRight = array("ADMIN", "VIEW_GROUP");

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();
 
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");

$lreportcard = new libreportcardcustom();
$lreportcard->hasAccessRight();

$lreportcard_ui = new libreportcard_ui();
$linterface = new interface_html();

# Tag
$CurrentPage = "Reports_Transcript";
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
$TAGS_OBJ[] = array($eReportCard['Reports_Transcript'], "", 0);

$checkPermissionFormClass = ($lreportcard->IS_ADMIN_USER_VIEW($_SESSION['UserID']) || $lreportcard->IS_VIEW_GROUP_USER_VIEW($_SESSION['UserID']))? 0 : 3;
$checkPermissionSubject = ($lreportcard->IS_ADMIN_USER_VIEW($_SESSION['UserID']) || $lreportcard->IS_VIEW_GROUP_USER_VIEW($_SESSION['UserID']))? 0 : 1;

$linterface->LAYOUT_START();
echo $linterface->Include_AutoComplete_JS_CSS();

$x = '';
$x .= '<table class="form_table_v30">'."\n";
    // Student Source
	$x .= '<tr>'."\n";
		$x .= '<td class="field_title">'.$Lang['eReportCard']['ReportArr']['TranscriptArr']['StudentSource'].'</td>'."\n";
		$x .= '<td>'."\n";
			$x .= $linterface->Get_Radio_Button('studentSource_formClass', 'studentSource', 'formClass', $isChecked=1, $Class="", $Lang['eReportCard']['ReportArr']['TranscriptArr']['FormClass'], "changedStudentSource(this.value);");
			$x .= $linterface->Get_Radio_Button('studentSource_userLogin', 'studentSource', 'userLogin', $isChecked=0, $Class="", $Lang['eReportCard']['ReportArr']['TranscriptArr']['UserLogin'], "changedStudentSource(this.value);");
			$x .= $linterface->Get_Radio_Button('studentSource_classHistory', 'studentSource', 'classHistory', $isChecked=0, $Class="", $Lang['eReportCard']['ReportArr']['TranscriptArr']['ClassHistory'], "changedStudentSource(this.value);");
		$x .= '</td>'."\n";
	$x .= '</tr>'."\n";
	
	// Form
	$x .= '<tr class="optionTr formClass">'."\n";
		$x .= '<td class="field_title">'.$Lang['General']['Form'].'</td>'."\n";
		$x .= '<td>'."\n";
			$x .= $lreportcard_ui->Get_Form_Selection('yearId', $SelectedYearID='', 'changedFormSelection();', $noFirst=1, $isAll=0, $hasTemplateOnly=1, $checkPermissionFormClass, $IsMultiple=0);
		$x .= '</td>'."\n";
	$x .= '</tr>'."\n";
	
	// Class
	$x .= '<tr class="optionTr formClass">'."\n";
		$x .= '<td class="field_title">'.$Lang['General']['Class'].'</td>'."\n";
		$x .= '<td>'."\n";
			$x .= '<div id="classSelDiv"></div>';
		$x .= '</td>'."\n";
	$x .= '</tr>'."\n";
	
	// User Login
	$x .= '<tr class="optionTr userLogin">'."\n";
		$x .= '<td class="field_title">'.$Lang['General']['UserLogin'].'</td>'."\n";
		$x .= '<td>'."\n";
			$x .= $linterface->GET_TEXTBOX_NAME('userloginTb', 'userLogin', '');
			$x .= $linterface->Get_Form_Warning_Msg("userLoginWarning", $eReportCard['jsWarningArr']['InvalidUserLogin'], "warnMsgDiv")."\n";
		$x .= '</td>'."\n";
	$x .= '</tr>'."\n";
	
	// Class History (Academic Year)
	$x .= '<tr class="optionTr classHistory">'."\n";
		$x .= '<td class="field_title">'.$Lang['General']['AcademicYear'].'</td>'."\n";
		$x .= '<td>'."\n";
			$x .= $lreportcard_ui->Get_Academic_Year_Selection('schoolSettingsAcademicYearId', $Selected='', 'changedSchoolSettingsAcademicYearSelection();', $noFirst=1);
		$x .= '</td>'."\n";
	$x .= '</tr>'."\n";
	
	// Class History (Form)
	$x .= '<tr class="optionTr classHistory">'."\n";
		$x .= '<td class="field_title">'.$Lang['General']['Form'].'</td>'."\n";
		$x .= '<td>'."\n";
			$x .= $lreportcard_ui->Get_Form_Selection('schoolSettingsYearId', $SelectedYearID='', 'changedSchoolSettingsYearSelection();', $noFirst=1, $isAll=0, $hasTemplateOnly=1, $checkPermissionFormClass, $IsMultiple=0);
		$x .= '</td>'."\n";
	$x .= '</tr>'."\n";
	
	// Class History (Class)
	$x .= '<tr class="optionTr classHistory">'."\n";
		$x .= '<td class="field_title">'.$Lang['General']['Class'].'</td>'."\n";
		$x .= '<td>'."\n";
			$x .= '<div id="schoolSettingsClassSelDiv"></div>';
		$x .= '</td>'."\n";
	$x .= '</tr>'."\n";
	
	// Student
	$x .= '<tr class="optionTr formClass classHistory">'."\n";
		$x .= '<td class="field_title">'.$Lang['Identity']['Student'].'</td>'."\n";
		$x .= '<td>'."\n";
			$x .= '<div id="studentSelDiv"></div>';
			$x .= $linterface->Get_Form_Warning_Msg("studentWarning", $eReportCard['jsWarningArr']['PleaseSelectStudent'], "warnMsgDiv")."\n";
		$x .= '</td>'."\n";
	$x .= '</tr>'."\n";
	
	// Date of ISSUE
	$x .= '<tr>'."\n";
		$x .= '<td class="field_title">'.$Lang['eReportCard']['ReportArr']['TranscriptArr']['DateOfIssue'].'</td>'."\n";
		$x .= '<td>'."\n";
			$x .= $linterface->GET_DATE_PICKER('dateOfIssue');
		$x .= '</td>'."\n";
	$x .= '</tr>'."\n";
	
	// Date of GRADUATION
	if($eRCTemplateSetting['Report']['TranscriptwDateGraduation'])
	{
    	$x .= '<tr>'."\n";
    		$x .= '<td class="field_title">'.$Lang['eReportCard']['ReportArr']['TranscriptArr']['DateOfGraduation'].'</td>'."\n";
    		$x .= '<td>'."\n";
                $x .= $linterface->GET_DATE_PICKER('dateOfGraduation');
    		$x .= '</td>'."\n";
    	$x .= '</tr>'."\n";
	}
	
	// Date of LEAVING
	if($eRCTemplateSetting['Report']['TranscriptwDateLeaving'])
	{
	    $x .= '<tr>'."\n";
    	    $x .= '<td class="field_title">'.$Lang['eReportCard']['ReportArr']['TranscriptArr']['DateOfLeaving'].'</td>'."\n";
    	    $x .= '<td>'."\n";
    	       $x .= $linterface->GET_DATE_PICKER('dateOfLeaving');
    	    $x .= '</td>'."\n";
	    $x .= '</tr>'."\n";
	}
	
	// Prinical Name
	if($eRCTemplateSetting['Report']['TranscriptwPrincipalName'])
	{
	    $x .= '<tr>'."\n";
            $x .= '<td class="field_title">'.$eReportCard['PrincipalName'].'</td>'."\n";
    	    $x .= '<td>'."\n";
    	       $x .= '<input name="princialName" type="text" class="textbox_name" value="" />'."\n";
    	    $x .= '</td>'."\n";
	    $x .= '</tr>'."\n";
	}
	
$x .= '</table>'."\n";

$htmlAry = array();
$htmlAry['optionTable'] = $x;
$htmlAry['submitBtn'] = $linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], 'button', 'checkForm();');
?>

<script>
var loading = '<?=$linterface->Get_Ajax_Loading_Image($noLang=1)?>';
var checkPermissionFormClass = '<?=$checkPermissionFormClass?>';
var checkPermissionSubject = '<?=$checkPermissionSubject?>';
var autoCompleteObj_userLogin;

$(document).ready(function()
{
	$('select#yearId').val($('select#yearId option:last').val());	
	
	// initialize selection for form class student source
	changedFormSelection();
	changedStudentSource('formClass', 1);
	
	// initialize selection for class history student source
	changedSchoolSettingsAcademicYearSelection(1);
	
	// initialize auto complete object
	initializeAutoComplete('userloginTb');
});

function initializeAutoComplete(userLoginTbId)
{
	autoCompleteObj_userLogin = $("input#" + userLoginTbId).autocomplete(
		"ajax_search_user_by_login.php",
		{
			onItemSelect: function(li) {
				autoCompleteUserLogin(li.extra[1], userLoginTbId);
			},
			formatItem: function(row) {
				return row[0];
			},
			maxItemsToShow: 10,
			minChars: 1,
			delay: 0,
			width: 300
		}
	);
}

function autoCompleteUserLogin(userLogin, userLoginTbId)
{
	$('input#' + userLoginTbId).val(userLogin).focus();
}

function changedStudentSource(studentSource, isInitialize)
{
	isInitialize = isInitialize || 0;
	
	$('tr.optionTr').hide();
	if (studentSource == 'formClass') {
		$('tr.formClass').show();
		
		if (!isInitialize) {
			changedClassSelection();
		}
	}
	else if (studentSource == 'userLogin') {
		$('tr.userLogin').show();
	}
	else if (studentSource == 'classHistory') {
		$('tr.classHistory').show();
		
		changedSchoolSettingsYearClassSelection();
	}
}

function changedFormSelection()
{
	var selectedYearId = $('select#yearId').val();
	
	$('div#classSelDiv').html(loading).load(
		"../ajax_reload_selection.php", 
		{ 
			RecordType: 'Class',
			YearID: selectedYearId,
			SelectionID: 'yearClassIdAry[]',
			onchange: 'changedClassSelection();',
			isMultiple: 1,
			TeachingOnly: checkPermissionFormClass,
			WithSelectAllButton: 1
		},
		function(ReturnData) {
			js_Select_All('yearClassIdAry[]');
			changedClassSelection();
		}
	);
}

function changedClassSelection()
{
	var classSelId = getJQuerySaveId('yearClassIdAry[]');
	var classIdList = $('select#' + classSelId).val();
	if  (classIdList) {
		classIdList = classIdList.join(',');
	}
	else {
		classIdList = '-1'; 	// no class will be returned
	}
	
	$('div#studentSelDiv').html(loading).load(
		"../ajax_reload_selection.php", 
		{ 
			RecordType: 'StudentByClass',
			ClassID: classIdList,
			SelectionID: 'studentIdAry[]',
			noFirst: 1,
			isMultiple: 1,
			withSelectAll: 1
		},
		function(ReturnData) {
			js_Select_All('studentIdAry[]');
		}
	);
}

function changedSchoolSettingsAcademicYearSelection(isInitialize)
{
	reloadSchoolSettingsClassSelection(isInitialize);
}

function changedSchoolSettingsYearSelection()
{
	reloadSchoolSettingsClassSelection();
}

function reloadSchoolSettingsClassSelection(isInitialize)
{
	isInitialize = isInitialize || 0;
	
	var selectedAcademicYearId = $('select#schoolSettingsAcademicYearId').val();
	var selectedYearId = $('select#schoolSettingsYearId').val();
	
	$('div#schoolSettingsClassSelDiv').html(loading).load(
		"../ajax_reload_selection.php", 
		{ 
			RecordType: 'Class',
			YearID: selectedYearId,
			SelectionID: 'schoolSettingsYearClassIdAry[]',
			onchange: 'changedSchoolSettingsYearClassSelection();',
			isMultiple: 1,
			TeachingOnly: checkPermissionFormClass,
			WithSelectAllButton: 1,
			AcademicYearID: selectedAcademicYearId
		},
		function(ReturnData) {
			js_Select_All('schoolSettingsYearClassIdAry[]');
			
			if (!isInitialize) {
				changedSchoolSettingsYearClassSelection();
			}
		}
	);
}

function changedSchoolSettingsYearClassSelection()
{
	var selectedAcademicYearId = $('select#schoolSettingsAcademicYearId').val();
	
	var classSelId = getJQuerySaveId('schoolSettingsYearClassIdAry[]');
	var classIdList = $('select#' + classSelId).val();
	if  (classIdList) {
		classIdList = classIdList.join(',');
	}
	else {
		classIdList = '-1'; 	// no class will be returned
	}
	
	$('div#studentSelDiv').html(loading).load(
		"../ajax_reload_selection.php", 
		{ 
			RecordType: 'StudentByClass',
			ClassID: classIdList,
			SelectionID: 'studentIdAry[]',
			noFirst: 1,
			isMultiple: 1,
			withSelectAll: 1,
			AcademicYearID: selectedAcademicYearId
		},
		function(ReturnData) {
			js_Select_All('studentIdAry[]');
		}
	);
}

function checkForm()
{
	$('div.warnMsgDiv').hide();
	
	if ($('input#studentSource_userLogin').attr('checked'))
	{
		// validate user login
		var userLogin = $('input#userloginTb').val();
		
		$.post(
			"ajax_task.php", 
			{ 
				task: 'checkUserLogin',
				userLogin: userLogin
			},
			function(ReturnData) {
				if (ReturnData == '1') {
					$('form#form1').submit();
				}
				else {
					$('div#userLoginWarning').show();
					$('input#userloginTb').focus();
				}
			}
		);
	}
	else if ($('input#studentSource_formClass').attr('checked') || $('input#studentSource_classHistory').attr('checked'))
	{
		var canSubmit = true;
		
		var studentSelId = getJQuerySaveId('studentIdAry[]');
		if ($('select#' + studentSelId).val() == null || $('select#' + studentSelId).val() == '') {
			$('div#studentWarning').show();
			$('select#' + studentSelId).focus();
			
			canSubmit = false;
		}
		
		if (canSubmit) {
			$('form#form1').submit();
		}
	}
}
</script>

<form id="form1" name="form1" method="post" target="_blank" action="<?php echo ($eRCTemplateSetting['Report']['TranscriptByPDF'] ? 'report_by_pdf.php' : 'report.php') ?>">
	<div class="table_board">
		<table id="html_body_frame" style="width:100%;">
			<tr><td><?=$htmlAry['optionTable']?></td></tr>
		</table>
	</div>
	<div class="edit_bottom_v30">
		<br />
		<?=$htmlAry['submitBtn']?>
	</div>
</form>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>