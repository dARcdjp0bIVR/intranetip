<?php
# Editing by

/**************************************************
 * 	Modification log
 * 	20180723 Bill:  [2017-0901-1525-31265]
 * 		- Allow View Group to access
 * ***********************************************/

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = array("ADMIN", "VIEW_GROUP");

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();
 
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");

$lreportcard = new libreportcardcustom();
$lreportcard->hasAccessRight();

$task = trim(stripslashes($_POST['task']));

if ($task == 'checkUserLogin') {
	$userLogin = trim(stripslashes($_POST['userLogin']));
	
	include_once($PATH_WRT_ROOT."includes/libuser.php");
	$luser = new libuser('', $userLogin);
	
	if ($luser->UserID > 0 && $luser->RecordType == USERTYPE_STUDENT) {
		echo '1';
	}
	else {
		echo '0';
	}
}

intranet_closedb();
?>