<?php
# Editing by
//ini_set('display_errors',1);
//error_reporting(E_ALL ^ E_NOTICE); 
 
//$DebugMode = true;


function getWordSpan($parLang, $wordings, $fontSizePt='') {
	if ($fontSizePt == '') {
		$fontSizePt = 11;
	}
	$fontClass = 'font_'.$fontSizePt.'pt';
	
	return '<span class="'.$parLang.' '.$fontClass.'">'.$wordings.'</span>';
}

function convertDateFormat($date) {
	$convertedDate = '';
	if (!is_date_empty($date)) {
		$convertedDate = date('d-m-Y', strtotime($date));
	}
	
	return $convertedDate;
}

function convertMarkAry($lreportcard, $formId, &$fullMarkAry, &$markAry, $gradingSchemeAry) {
	global $eRCTemplateSetting;
	
	$schoolYear = $lreportcard->schoolYear;
	
	if ($schoolYear=='2006' || $schoolYear=='2007' || $schoolYear=='2008') {
	//dev
	//if ($schoolYear=='2009' || $schoolYear=='2006' || $schoolYear=='2007' || $schoolYear=='2008') {
		$gradeToGradePointAssoAry = $eRCTemplateSetting['TranscriptGradePointMappingAry'][$schoolYear];
		$scoreToLevelAssoAry = $eRCTemplateSetting['TranscriptWeightedScoreToConvertedLevelAry'][$schoolYear];
		
		if ($gradeToGradePointAssoAry==null || $scoreToLevelAssoAry==null) {
			return false;
		}
		
		$consolidatedReportAry = $lreportcard->returnReportTemplateBasicInfo($ReportID='', $others='', $formId, $Semester='F');
		$consolidatedReportId = $consolidatedReportAry['ReportID'];
		
		$reportColumnAry = $lreportcard->returnReportTemplateColumnData($consolidatedReportId);
		$numOfColumn = count($reportColumnAry);
		
		$reportWeightAry = $lreportcard->returnReportTemplateSubjectWeightData($consolidatedReportId, '', $ParSubjectID='', '', $convertEmptyToZero=true);
		$reportWeightAssoAry = BuildMultiKeyAssoc($reportWeightAry, array('SubjectID', 'ReportColumnID'), $IncludedDBField=array('Weight'), $SingleValue=1);
		
		$mathsSubjectId = $lreportcard->Get_SubjectID_From_Config('Maths');
		
		foreach ((array)$markAry as $_studentId => $_studentMarkAry) {
			foreach ((array)$_studentMarkAry as $__subjectId => $__subjectMarkAry) {
				
				$__scaleDisplay = $gradingSchemeAry[$__subjectId]['ScaleDisplay'];
				
				// 2012-1009-1115-03140
				//(Internal) Follow-up by karsonyam on 2013-07-09 10:04
//				if ($schoolYear == '2006' && $__subjectId==$mathsSubjectId) {
//					$__mark = $markAry[$_studentId][$__subjectId][0]['Mark'];
//					$__grade = $markAry[$_studentId][$__subjectId][0]['Grade'];
//					
//					if (!$lreportcard->Check_If_Grade_Is_SpecialCase($__grade)) {
//						$markAry[$_studentId][$__subjectId][0]['Mark'] = my_round($markAry[$_studentId][$__subjectId][0]['Mark'] * 1.5, 1);
//					}
//				}
				
				if ($__scaleDisplay != 'G') {
					continue;
				}
				
				$__subjectTotalMark = 0;
				$__subjectTotalWeight = 0;
				$__subjectExcludedWeight = 0;
				
				for ($i=0; $i<$numOfColumn; $i++) {
					$___reportColumnId = $reportColumnAry[$i]['ReportColumnID'];
					
					$___weight = $reportWeightAssoAry[$__subjectId][$___reportColumnId];
					
					$___mark = $markAry[$_studentId][$__subjectId][$___reportColumnId]['Mark'];
					$___grade = $markAry[$_studentId][$__subjectId][$___reportColumnId]['Grade'];
					
					$__subjectTotalWeight += $___weight;
					//2015-0521-1148-04073
					//if ($lreportcard->Check_If_Grade_Is_SpecialCase($___grade)) {
					if ($lreportcard->Check_If_Grade_Is_SpecialCase($___grade) || $___grade=='N/A') {
						$__subjectExcludedWeight += $___weight;
					}
					else {
						$___gradePoint = $gradeToGradePointAssoAry[$___grade];
						
						if ($___gradePoint != '') {
							$__subjectTotalMark += $___gradePoint * $___weight;
						}
					}
				}
				
				$__subjectRemainingWeight = $__subjectTotalWeight - $__subjectExcludedWeight;
				if ($__subjectExcludedWeight > 0 && $__subjectRemainingWeight > 0) {
					$__subjectTotalMark = $__subjectTotalMark * ($__subjectTotalWeight / $__subjectRemainingWeight);
				}
				
				
				$__subjectLevel = '';
				foreach ((array)$scoreToLevelAssoAry as $___lowerLimit => $___level) {
					if (floatcmp((float)$__subjectTotalMark, ">=", (float)$___lowerLimit)) {
						$__subjectLevel = $___level;
						break;
					} 
				}
				
				if ($__subjectLevel != '') {
					//$fullMarkAry[$__subjectId] = 'Level 5';
					$markAry[$_studentId][$__subjectId][$ReportColumnID=0]['Grade'] = 'Level '.$__subjectLevel;
				}
			}
		}
	}
		
	return true;
}


function getTargetSubjectId($parAcademicYearId, $parSubjectId, $parIsComponentSubject, $parFormGradeNumber) {
	global $lreportcard, $intranet_db;
	
	/*	2015-0211-1503-22073
	 * 	For CL,(中國語文)
		subject code 80 was used in G7 and G8 until 12-13 and did not use since 13-14
		subject code 080 was used in G9, G10, G11 and G12 until 12-13 and did not use since 13-14
		In 13-14 and 14-15, 080-2013 were used but not the old ones 80 and 080 for all grades
		 
		For VA,(視藝)
		subject code 432 was used form G7-9 until now.
		subject code 432 was used in G10-G12 up to 12-13 while 83S starts using since 13-14
		 
		For MU, (音)
		subject code 300 was used form G7-9 until now.
		subject code 300 was used from G10-G12 up to 12-13 while 21S starts using since 13-14
	 */
	
	$sql = "Select YearNameEN From $intranet_db.ACADEMIC_YEAR Where AcademicYearID = '".$parAcademicYearId."'";
	$academicYearInfoAry = $lreportcard->returnResultSet($sql);
	$academicYearStart = substr($academicYearInfoAry[0]['YearNameEN'], 0, 4); 
	
	
	$targetSubjectId = $parSubjectId;
	if (!$parIsComponentSubject) {
		if ($parSubjectId == $lreportcard->Get_SubjectID_From_Config('Chinese') || $parSubjectId == $lreportcard->Get_SubjectID_From_Config('Chinese(NSS)') || $parSubjectId == $lreportcard->Get_SubjectID_From_Config('Chinese(2013)')) {
			// Chinese special mapping
			if ($academicYearStart < 2013) {
				if ($parFormGradeNumber == 7 || $parFormGradeNumber == 8) {
					$targetSubjectId = $lreportcard->Get_SubjectID_From_Config('Chinese');
				}
				else {
					$targetSubjectId = $lreportcard->Get_SubjectID_From_Config('Chinese(NSS)');
				}
			}
			else {
				$targetSubjectId = $lreportcard->Get_SubjectID_From_Config('Chinese(2013)');
			}
		}
		else if ($parSubjectId == $lreportcard->Get_SubjectID_From_Config('VisualArts') || $parSubjectId == $lreportcard->Get_SubjectID_From_Config('VisualArts(HKDSE)')) {
			// Visual Art special mapping
			if ($parFormGradeNumber == 7 || $parFormGradeNumber == 8 || $parFormGradeNumber == 9) {
				$targetSubjectId = $lreportcard->Get_SubjectID_From_Config('VisualArts');
			}
			else {
				if ($academicYearStart < 2013) {
					$targetSubjectId = $lreportcard->Get_SubjectID_From_Config('VisualArts');
				}
				else {
					$targetSubjectId = $lreportcard->Get_SubjectID_From_Config('VisualArts(HKDSE)');
				}
			}
		}
		else if ($parSubjectId == $lreportcard->Get_SubjectID_From_Config('Music') || $parSubjectId == $lreportcard->Get_SubjectID_From_Config('Music(HKDSE)')) {
			if ($parFormGradeNumber == 7 || $parFormGradeNumber == 8 || $parFormGradeNumber == 9) {
				$targetSubjectId = $lreportcard->Get_SubjectID_From_Config('Music');
			}
			else {
				if ($academicYearStart < 2013) {
					$targetSubjectId = $lreportcard->Get_SubjectID_From_Config('Music');
				}
				else {
					$targetSubjectId = $lreportcard->Get_SubjectID_From_Config('Music(HKDSE)');
				}
			}
		}
	}
	
	return $targetSubjectId;
}


include_once($PATH_WRT_ROOT.'includes/form_class_manage.php');
$fcm = new form_class_manage();


$studentSource = standardizeFormPostValue($_POST['studentSource']);
$yearId = standardizeFormPostValue($_POST['yearId']);
$yearClassIdAry = $_POST['yearClassIdAry'];
$userLogin = standardizeFormPostValue($_POST['userLogin']);
$studentIdAry = $_POST['studentIdAry'];
$dateOfIssue = convertDateFormat(standardizeFormPostValue($_POST['dateOfIssue']));


### get student array
if ($studentSource == 'userLogin') {
	$luser = new libuser('', $userLogin);
	
	$studentIdAry = array();
	$studentIdAry[] = $luser->UserID;
}
$numOfStudent = count($studentIdAry);
$luserObj = new libuser('', '', $studentIdAry);


$levelKeyAry = array('junior', 'senior');
$numOfLevelKey = count($levelKeyAry);



### get all academic year from the system
$academicYearInfoAry = $fcm->Get_Academic_Year_List($AcademicYearIDArr='', $OrderBySequence=0);
$numOfAcademicYear = count($academicYearInfoAry);


### get all forms in the system
$formInfoAry = $lreportcard->GET_ALL_FORMS(); 
$numOfForm = count($formInfoAry);


### get student class history info
$studentClassHistoryAry = $lreportcard->Get_Student_From_School_Settings($AcademicYearIDAry='', $studentIdAry);
$academicYearFormAssoAry = BuildMultiKeyAssoc($studentClassHistoryAry, 'AcademicYearID', 'YearID', $SingleValue=1, $BuildNumericArray=1);
$numOfData = count($studentClassHistoryAry);



### Analyze student class history info
$studentDataAssoAry = array();
for ($i=0; $i<$numOfData; $i++) {
	$_studentId = $studentClassHistoryAry[$i]['UserID'];
	$_academicYearId = $studentClassHistoryAry[$i]['AcademicYearID'];
	$_formId = $studentClassHistoryAry[$i]['YearID'];
	$_formName = $studentClassHistoryAry[$i]['YearName'];
	$_academicYearName = $studentClassHistoryAry[$i]['AcademicYearNameEn'];
	
	$_formGradeNumber = $lreportcard->getFormGradeNumber($_formId);
	
	// record the first form grade to determine the form that the student has no class records later
	if (!isset($studentDataAssoAry[$_studentId])) {
		$studentDataAssoAry[$_studentId]['firstFormGradeNumber'] = $_formGradeNumber;
		$studentDataAssoAry[$_studentId]['firstAcademicYearId'] = $_academicYearId;
		
		// build the key first for easier debugging and make sure junior and always beofre senior
//		$studentDataAssoAry[$_studentId]['lastAcademicYearId'] = '';
		$studentDataAssoAry[$_studentId]['classHistoryAry']['junior'] = array();
		$studentDataAssoAry[$_studentId]['classHistoryAry']['senior'] = array();
		$studentDataAssoAry[$_studentId]['classHistoryAry']['junior']['firstFormGradeNumber'] = '';
		$studentDataAssoAry[$_studentId]['classHistoryAry']['junior']['firstFormId'] = '';
		$studentDataAssoAry[$_studentId]['classHistoryAry']['junior']['firstAcademicYearId'] = '';
		$studentDataAssoAry[$_studentId]['classHistoryAry']['senior']['firstFormGradeNumber'] = '';
		$studentDataAssoAry[$_studentId]['classHistoryAry']['senior']['firstFormId'] = '';
		$studentDataAssoAry[$_studentId]['classHistoryAry']['senior']['firstAcademicYearId'] = '';
	}
	
	// Store student basic info
	$_levelKey = ($_formGradeNumber < 10)? 'junior' : 'senior';
//	if ($_formGradeNumber < $studentDataAssoAry[$_studentId]['firstFormGradeNumber']) {
//		$studentDataAssoAry[$_studentId]['firstFormGradeNumber'] = $_formGradeNumber;
//		$studentDataAssoAry[$_studentId]['firstAcademicYearId'] = $_academicYearId;
//	}
	
//	if (!isset($studentDataAssoAry[$_studentId]['classHistoryAry'][$_levelKey]['firstFormGradeNumber']) || ($_formGradeNumber < $studentDataAssoAry[$_studentId]['classHistoryAry'][$_levelKey]['firstFormGradeNumber'])) {
//		// record the first form id of different level for subject list determination later
//		
//		$studentDataAssoAry[$_studentId]['classHistoryAry'][$_levelKey]['firstFormGradeNumber'] = $_formGradeNumber;
//		$studentDataAssoAry[$_studentId]['classHistoryAry'][$_levelKey]['firstFormId'] = $_formId;
//		$studentDataAssoAry[$_studentId]['classHistoryAry'][$_levelKey]['firstAcademicYearId'] = $_academicYearId;
//	}
	
	$studentDataAssoAry[$_studentId]['classHistoryAry'][$_levelKey]['levelAcademicYearAssoAry'][$_academicYearId]['formId'] = $_formId;
	$studentDataAssoAry[$_studentId]['classHistoryAry'][$_levelKey]['levelAcademicYearAssoAry'][$_academicYearId]['formName'] = $_formName;
	$studentDataAssoAry[$_studentId]['classHistoryAry'][$_levelKey]['levelAcademicYearAssoAry'][$_academicYearId]['formGradeNumber'] = $_formGradeNumber;
	$studentDataAssoAry[$_studentId]['classHistoryAry'][$_levelKey]['levelAcademicYearAssoAry'][$_academicYearId]['academicYearId'] = $_academicYearId;
	$studentDataAssoAry[$_studentId]['classHistoryAry'][$_levelKey]['levelAcademicYearAssoAry'][$_academicYearId]['academicYearName'] = $_academicYearName;
	$studentDataAssoAry[$_studentId]['classHistoryAry'][$_levelKey]['levelAcademicYearAssoAry'][$_academicYearId]['studentStudied'] = true;
}
// find the last academic year id of each student for later determination of table remaining columns
//foreach ((array)$studentDataAssoAry as $_studentId => $_studentDateAry) {
//	foreach ((array)$_studentDateAry['classHistoryAry'] as $__levelKey => $__levelDataAry) {
//		foreach ((array)$__levelDataAry['levelAcademicYearAssoAry'] as $___academicYearId => $___levelAcademicYearAry) {
//			$studentDataAssoAry[$_studentId]['lastAcademicYearId'] = $___academicYearId;
//		}	
//	}
//}



### Analyze student without class history year
$minFormGradeNumber = 7;
$maxFormGradeNumber = 12;

//// insert first n years if the student not stuided
//foreach ((array)$studentDataAssoAry as $_studentId => $_studentDataAry) {
//	$_firstFormGradeNumber = $_studentDataAry['firstFormGradeNumber'];
//	$_firstAcademicYearId = $_studentDataAry['firstAcademicYearId'];
//	
//	$_numOfEmptyColumn = $_firstFormGradeNumber - $minFormGradeNumber;
//	$_emptyColumnAcademicYearIdAry = array();
//	for ($i=0; $i<$numOfAcademicYear; $i++) {
//		$__academicYearId = $academicYearInfoAry[$i]['AcademicYearID'];
//		
//		if ($__academicYearId == $_firstAcademicYearId) {
//			for ($j=0; $j<$_numOfEmptyColumn; $j++) {
//				// $academicYearInfoAry is order by desc of TermStart, so the next index is the earlier academic year
//				$___academicYearId = $academicYearInfoAry[$i + $j + 1]['AcademicYearID'];
//				
//				$_emptyColumnAcademicYearIdAry[] = $___academicYearId;
//			}
//		}
//	}
//	
//	$_numOfEmptyCol = count($_emptyColumnAcademicYearIdAry);
//	for ($i=0; $i<$_numOfEmptyCol; $i++) {
//		$__academicYearId = $_emptyColumnAcademicYearIdAry[$i];
//		
//		$__academicYearInfoAry = $lreportcard->Get_Academic_Year_By_AcademicYearId($__academicYearId);
//		$__academicYearName = $__academicYearInfoAry['YearNameEN'];
//		
//		// insert array order should be G9 -> G8 -> G7
//		$__formGradeNumber = $minFormGradeNumber + ($_numOfEmptyCol - $i - 1);
//		$__formName = 'G'.$__formGradeNumber;
//		$__levelKey = ($__formGradeNumber < 10)? 'junior' : 'senior';
//		
//		$__targetFormId = '';
//		for ($j=0; $j<$numOfForm; $j++) {
//			$___formId = $formInfoAry[$j]['ClassLevelID'];
//			$___formNumber = $lreportcard->GET_FORM_NUMBER($___formId, true);
//			
//			if ($___formNumber == $__formGradeNumber - 6) {
//				$__targetFormId = $___formId;
//				break;
//			}
//		}
//		
//		$__tempAry = array();
//		$__tempAry[$__academicYearId]['formId'] = $__targetFormId;
//		$__tempAry[$__academicYearId]['formName'] = $__formName;
//		$__tempAry[$__academicYearId]['formGradeNumber'] = $__formGradeNumber;
//		$__tempAry[$__academicYearId]['academicYearId'] = $__academicYearId;
//		$__tempAry[$__academicYearId]['academicYearName'] = $__academicYearName;
//		$__tempAry[$__academicYearId]['studentStudied'] = false;
//		
//		// user "+" instead of "array_merge" to maintain array keys
//		$studentDataAssoAry[$_studentId]['classHistoryAry'][$__levelKey]['levelAcademicYearAssoAry'] = (array)$__tempAry + (array)$studentDataAssoAry[$_studentId]['classHistoryAry'][$__levelKey]['levelAcademicYearAssoAry'];
//	}
//}
//	
//// insert the middle n years if the student left and then join the school again
//foreach ((array)$studentDataAssoAry as $_studentId => $_studentDataAry) {
//	$_tempLevelAcademicYearAssoAry['junior'] = array();
//	$_tempLevelAcademicYearAssoAry['senior'] = array();
//	$_lastAcademicYearId = '';
//	for ($i=$minFormGradeNumber; $i<=$maxFormGradeNumber; $i++) {
//		$__targetFormGradeNumber = $i;
//		
//		if ($__targetFormGradeNumber == 7 || $__targetFormGradeNumber == 8 || $__targetFormGradeNumber == 9) { 
//			$__formLevelKey = 'junior';
//		}
//		else {
//			$__formLevelKey = 'senior';
//		}
//		
//		$__haveGradeNumber = false;
//		foreach ((array)$_studentDataAry['classHistoryAry'][$__formLevelKey]['levelAcademicYearAssoAry'] as $___academicYearId => $___academicYearInfoAry) {
//			$___formGradeNumber = $___academicYearInfoAry['formGradeNumber'];
//			
//			if ($___formGradeNumber == $__targetFormGradeNumber) {
//				$_tempLevelAcademicYearAssoAry[$__formLevelKey]['levelAcademicYearAssoAry'][$___academicYearId] = $___academicYearInfoAry;
//				
//				$_lastAcademicYearId = $___academicYearId;
//				$__haveGradeNumber = true;
//				
//				// cannot break due to may have repeat year
//				//break;
//			}
//		}
//		
//		if (!$__haveGradeNumber) {
//			for ($j=0; $j<$numOfAcademicYear; $j++) {
//				$___academicYearId = $academicYearInfoAry[$j]['AcademicYearID'];
//				
//				if ($___academicYearId == $_lastAcademicYearId) {
//					// $academicYearInfoAry is order by desc of TermStart, so the previous index is the next academic year
//					$___nextAcademicYearId = $academicYearInfoAry[$j - 1]['AcademicYearID'];
//					
//					$___academicYearInfoAry = $lreportcard->Get_Academic_Year_By_AcademicYearId($___nextAcademicYearId);
//					$___academicYearName = $___academicYearInfoAry['YearNameEN'];
//					$___formName = 'G'.$__targetFormGradeNumber;
//					
//					$___targetFormId = '';
//					for ($k=0; $k<$numOfForm; $k++) {
//						$____formId = $formInfoAry[$k]['ClassLevelID'];
//						$____formNumber = $lreportcard->GET_FORM_NUMBER($____formId, true);
//						
//						if ($____formNumber == $__targetFormGradeNumber - 6) {
//							$___targetFormId = $____formId;
//							break;
//						}
//					}
//					
//					$___tempAry = array();
//					$___tempAry['formId'] = $___targetFormId;
//					$___tempAry['formName'] = $___formName;
//					$___tempAry['formGradeNumber'] = $__targetFormGradeNumber;
//					$___tempAry['academicYearId'] = $___nextAcademicYearId;
//					$___tempAry['academicYearName'] = $___academicYearName;
//					$___tempAry['studentStudied'] = false;
//					
//					$_tempLevelAcademicYearAssoAry[$__formLevelKey]['levelAcademicYearAssoAry'][$___nextAcademicYearId] = $___tempAry;
//					break;
//				}
//			}
//		}
//	}
//	
//	$studentDataAssoAry[$_studentId]['classHistoryAry']['junior']['levelAcademicYearAssoAry'] = $_tempLevelAcademicYearAssoAry['junior']['levelAcademicYearAssoAry'];
//	$studentDataAssoAry[$_studentId]['classHistoryAry']['senior']['levelAcademicYearAssoAry'] = $_tempLevelAcademicYearAssoAry['senior']['levelAcademicYearAssoAry'];
//}

### remove the academic year that the student has left the school
foreach ((array)$studentDataAssoAry as $_studentId => $_studentDataAry) {
	// find the last academic year that the student studied in school
	$_studentLastStudiedAcademicYearId = '';
	for ($i=0; $i<$numOfLevelKey; $i++) {
		$__levelKey = $levelKeyAry[$i];
		
		foreach ((array)$_studentDataAry['classHistoryAry'][$__levelKey]['levelAcademicYearAssoAry'] as $___academicYearId => $___academicYearInfoAry) {
			$___studentStudied = $___academicYearInfoAry['studentStudied'];
			
			if ($___studentStudied) {
				$_studentLastStudiedAcademicYearId = $___academicYearId;
			}
		}
	}
	
	// remove the academic year info after the last studied year
	$_startToDelete = false;
	for ($i=0; $i<$numOfLevelKey; $i++) {
		$__levelKey = $levelKeyAry[$i];
		
		foreach ((array)$_studentDataAry['classHistoryAry'][$__levelKey]['levelAcademicYearAssoAry'] as $___academicYearId => $___academicYearInfoAry) {
			if ($_startToDelete) {
				unset($studentDataAssoAry[$_studentId]['classHistoryAry'][$__levelKey]['levelAcademicYearAssoAry'][$___academicYearId]);
			}
			
			if ($___academicYearId == $_studentLastStudiedAcademicYearId) {
				$_startToDelete = true;
			}
		}
	}
}


### remove the form level if the student did not study in any of the forms
foreach ((array)$studentDataAssoAry as $_studentId => $_studentDataAry) {
	for ($i=0; $i<$numOfLevelKey; $i++) {
		$__levelKey = $levelKeyAry[$i];
		$__studentLevelInfoAry = $_studentDataAry['classHistoryAry'][$__levelKey]['levelAcademicYearAssoAry'];
		
		$__studentNotStudyingAllForm = true;
		foreach ((array)$__studentLevelInfoAry as $___academicYearId => $___studentAcademicYearInfoAry) {
			$___isStudentStudiedThisYear = $___studentAcademicYearInfoAry['studentStudied'];
			
			if ($___isStudentStudiedThisYear) {
				$__studentNotStudyingAllForm = false;
				break;
			}
		}
		
		if ($__studentNotStudyingAllForm) {
			unset($studentDataAssoAry[$_studentId]['classHistoryAry'][$__levelKey]);
		}
	}
}



### get first academic year and form data for later use of determination of subject list
foreach ((array)$studentDataAssoAry as $_studentId => $_studentDataAry) {
	$_juniorAry = $_studentDataAry['classHistoryAry']['junior']['levelAcademicYearAssoAry'];
	$_seniorAry = $_studentDataAry['classHistoryAry']['senior']['levelAcademicYearAssoAry'];
	
	foreach ((array)$_juniorAry as $__academicYearId => $__academicYearInfoAry) {
		$__formId = $__academicYearInfoAry['formId'];
		$__formGradeNumber = $__academicYearInfoAry['formGradeNumber'];
		
		$studentDataAssoAry[$_studentId]['firstFormGradeNumber'] = $__formGradeNumber;
		$studentDataAssoAry[$_studentId]['firstAcademicYearId'] = $__academicYearId;
		
		$studentDataAssoAry[$_studentId]['classHistoryAry']['junior']['firstFormGradeNumber'] = $__formGradeNumber;
		$studentDataAssoAry[$_studentId]['classHistoryAry']['junior']['firstFormId'] = $__formId;
		$studentDataAssoAry[$_studentId]['classHistoryAry']['junior']['firstAcademicYearId'] = $__academicYearId;
		
		break;
	}
	
	foreach ((array)$_seniorAry as $__academicYearId => $__academicYearInfoAry) {
		$__formId = $__academicYearInfoAry['formId'];
		$__formGradeNumber = $__academicYearInfoAry['formGradeNumber'];
		
		$studentDataAssoAry[$_studentId]['classHistoryAry']['senior']['firstFormGradeNumber'] = $__formGradeNumber;
		$studentDataAssoAry[$_studentId]['classHistoryAry']['senior']['firstFormId'] = $__formId;
		$studentDataAssoAry[$_studentId]['classHistoryAry']['senior']['firstAcademicYearId'] = $__academicYearId;
		
		break;
	}
}


### get all related academic year and form for data retrieve later
$allAcademicYearFormInfoAry = array();
foreach ((array)$studentDataAssoAry as $_studentId => $_studentDataAry) {
	foreach ((array)$_studentDataAry['classHistoryAry'] as $__formLevelKey => $__formLevelAry) {
		foreach ((array)$__formLevelAry['levelAcademicYearAssoAry'] as $___academicYearId => $___academicYearInfoAry) {
			$____formId = $___academicYearInfoAry['formId'];
			
			if ($____formId != '' && !in_array($____formId, (array)$allAcademicYearFormInfoAry[$___academicYearId])) {
				$allAcademicYearFormInfoAry[$___academicYearId][] = $____formId;
			}
		}
	}
}


### get report related data (e.g. mark, subject list, grading scheme, etc...) for all related academic years and forms
$subjectAry = array();
$gradingSchemeAry = array();
$fullMarkAry = array();
// $markAry[$academicYearId][$formId][$studentId][$subjectId][$reportColumnId][key] = data
$markAry = array();
foreach ((array)$allAcademicYearFormInfoAry as $_academicYearId => $_formIdAry) {
	$_lreportcard = new libreportcard($_academicYearId);
	
	$_numOfForm = count($_formIdAry);
	for ($i=0; $i<$_numOfForm; $i++) {
		$__formId = $_formIdAry[$i];
		
		$__reportInfoAry = $_lreportcard->returnReportTemplateBasicInfo($ReportID='', $others='', $__formId, $Semester='F', $isMainReport=1);
		$__reportId = $__reportInfoAry['ReportID'];
		
		// get subjects for each form
		$subjectAry[$_academicYearId][$__formId] = $_lreportcard->returnSubjectwOrder($__formId, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $__reportId);
		
		// get grading scheme info
		$gradingSchemeAry[$_academicYearId][$__formId] = $_lreportcard->GET_SUBJECT_FORM_GRADING($__formId, $SubjectID='', $withGrandResult=0, $returnAsso=1, $__reportId);
		
		// get subject full mark
		$fullMarkAry[$_academicYearId][$__formId] = $_lreportcard->returnSubjectFullMark($__formId, $withSub=1, $ParSubjectIDArr=array(), $ParInputScale=0, $__reportId);
		
		// get marks for each form
		$markAry[$_academicYearId][$__formId] = $_lreportcard->getMarksCommonIpEj($__reportId, $StudentID='', $cons='', $ParentSubjectOnly=0, $includeAdjustedMarks=1, $OrderBy='', $SubjectID='', $ReportColumnID='');
		
		
		// 2013-1209-1126-03073
		if (count($markAry[$_academicYearId][$__formId]) == 0) {
			### remove the academic year that the student has no mark
			foreach ((array)$studentDataAssoAry as $___studentId => $___studentDataAry) {
				for ($j=0; $j<$numOfLevelKey; $j++) {
					$____levelKey = $levelKeyAry[$j];
					
					foreach ((array)$___studentDataAry['classHistoryAry'][$____levelKey]['levelAcademicYearAssoAry'] as $_____academicYearId => $_____academicYearInfoAry) {
						if ($_____academicYearId == $_academicYearId) {
							unset($studentDataAssoAry[$___studentId]['classHistoryAry'][$____levelKey]['levelAcademicYearAssoAry'][$_____academicYearId]);
						}
					}
				}
			}
		}
		
		convertMarkAry($_lreportcard, $__formId, $fullMarkAry[$_academicYearId][$__formId], $markAry[$_academicYearId][$__formId], $gradingSchemeAry[$_academicYearId][$__formId]);
	}
}


//debug_pr($markAry);

//if ($_SESSION['UserID']==1) {
//	debug_pr($subjectAry);
//}




### Analyze student mark info
foreach ((array)$studentDataAssoAry as $_studentId => $_dataAry) {
	for ($i=0; $i<$numOfLevelKey; $i++) {
		$__levelKey = $levelKeyAry[$i];
		
		// get subject list
		$__levelKeyFirstFormId = $studentDataAssoAry[$_studentId]['classHistoryAry'][$__levelKey]['firstFormId'];
		$__studentClassHistoryAry = $studentDataAssoAry[$_studentId]['classHistoryAry'][$__levelKey]['levelAcademicYearAssoAry'];
		$__levelKeyFirstAcademicYearId = $studentDataAssoAry[$_studentId]['classHistoryAry'][$__levelKey]['firstAcademicYearId'];
//		foreach ((array)$__studentClassHistoryAry as $___academicYearId => $___academicYearDataAry) {
//			$___formId = $___academicYearDataAry['formId'];
//			
//			if ($___formId == $__levelKeyFirstFormId) {
//				$__levelKeyFirstAcademicYearId = $___academicYearId;
//				break;
//			}
//		}
		
		//2016-0811-1143-33207
//		$__levelSubjectAry = $subjectAry[$__levelKeyFirstAcademicYearId][$__levelKeyFirstFormId];
		
		// consolidate all studied subjects of the same school level
		$__mergedSubjectAry = array();
		$__levelDataAry = $studentDataAssoAry[$_studentId]['classHistoryAry'][$__levelKey];
		foreach ((array)$__levelDataAry['levelAcademicYearAssoAry'] as $___academicYearId => $___academicYearInfoAry) {
			$___formId = $___academicYearInfoAry['formId'];
			$__mergedSubjectAry = $__mergedSubjectAry + (array)$subjectAry[$___academicYearId][$___formId];
		}
		
		// get marks display
		foreach ((array)$__mergedSubjectAry as $___parentSubjectId => $___cmpSubjectAry) {
			$___numOfComponentSubject = count($___cmpSubjectAry) - 1;
			$___isParentSubject = ($___numOfComponentSubject > 0)? true : false;
			
			foreach ((array)$___cmpSubjectAry as $____cmpSubjectID => $____cmpSubjectName) {
				$____isComponentSubject = ($____cmpSubjectID == 0)? false : true;
				$____subjectId = ($____isComponentSubject)? $____cmpSubjectID : $___parentSubjectId;
				
				// only display Maths component subject as "Mathematics - Compulsory" and "Mathematics - Module 1 / 2"
				//if ($___parentSubjectId != $lreportcard->Get_SubjectID_From_Config('Maths') && $____isComponentSubject) {
				if ($___parentSubjectId != $lreportcard->Get_SubjectID_From_Config('Maths') && $___parentSubjectId != $lreportcard->Get_SubjectID_From_Config('CombinedScience') && $____isComponentSubject) {
					continue;
				}
				
				// skip Maths main subject for higher form
				if ($____subjectId == $lreportcard->Get_SubjectID_From_Config('Maths') && $___isParentSubject) {
					continue;
				}
				
				// get subject marks for each academic year
				foreach ((array)$__studentClassHistoryAry as $_____academicYearId => $_____academicYearDataAry) {
					$_____formId = $_____academicYearDataAry['formId'];
					$_____formName = $_____academicYearDataAry['formName'];
					$_____formGradeNumber = $_____academicYearDataAry['formGradeNumber'];
					$_____studentStudiesThisForm = $_____academicYearDataAry['studentStudied'];
					
//					if (!$_____studentStudiesThisForm) {
//						$_____formId = $lreportcard->Get_ClassLevelId_By_ClassLevelName($_____formName);
//					}
					
					// 2015-0211-1503-22073
//					if ($_____academicYearId == 9 && $____subjectId == $lreportcard->Get_SubjectID_From_Config('Chinese(NSS)') && !$____isComponentSubject) {
//						// 2014-0312-1455-46073
//						// for 2013-2014 academic year, auto map to Chinese (2013)
//						$____subjectId = $lreportcard->Get_SubjectID_From_Config('Chinese(2013)');
//					}
//					else if ($_____academicYearId == 9 && $____subjectId == $lreportcard->Get_SubjectID_From_Config('VisualArts') && !$____isComponentSubject) {
//						// 2014-0312-1455-46073
//						// for 2013-2014 academic year, map to VisualArts(HKDSE) to VisualArt
//						$____subjectId = $lreportcard->Get_SubjectID_From_Config('VisualArts(HKDSE)');
//					}
//					else if ($_____academicYearId == 9 && $____subjectId == $lreportcard->Get_SubjectID_From_Config('Music') && !$____isComponentSubject) {
//						// 2014-0312-1455-46073
//						// for 2013-2014 academic year, map to Music(HKDSE) to Msuic
//						$____subjectId = $lreportcard->Get_SubjectID_From_Config('Music(HKDSE)');
//					}
//					else if (($_____formGradeNumber == 7 || $_____formGradeNumber == 8) && $____subjectId == $lreportcard->Get_SubjectID_From_Config('Chinese(NSS)') && !$____isComponentSubject) {
//						// for G7 G8 Chinese, auto map to Chinese
//						$____subjectId = $lreportcard->Get_SubjectID_From_Config('Chinese');
//					}
//					else if ($_____formGradeNumber >= 9 && $____subjectId == $lreportcard->Get_SubjectID_From_Config('Chinese') && !$____isComponentSubject) {
//						// for G9 or higher Chinese, auto map to Chinese (NSS) subject
//						$____subjectId = $lreportcard->Get_SubjectID_From_Config('Chinese(NSS)');
//					}
					$____subjectId = getTargetSubjectId($_____academicYearId, $____subjectId, $____isComponentSubject, $_____formGradeNumber);
					
					
					// get subject grading scheme info
					$_____subjectGradingSchemeInfoAry = $gradingSchemeAry[$_____academicYearId][$_____formId][$____subjectId];
					$_____subjectScaleDisplay = $_____subjectGradingSchemeInfoAry['ScaleDisplay'];
					$studentDataAssoAry[$_studentId]['classHistoryAry'][$__levelKey]['levelAcademicYearAssoAry'][$_____academicYearId]['markAry'][$____subjectId]['scaleDisplay'] = $_____subjectScaleDisplay;
					
					// get subject full mark
					$_____subjectFullMark = $fullMarkAry[$_____academicYearId][$_____formId][$____subjectId];
					if ($_____subjectFullMark == '') {
						// 2012-1009-1115-03140 -> (Internal) Follow-up by karsonyam on 2013-07-10 09:45
						// no subject settings in this academic year => show "N/A" for both full mark and score
						$_____subjectFullMark = 'N/A';
					}
					else if ($_____subjectScaleDisplay == 'G') {
						$_____subjectFullMark = 'Level 5';
					}
					$studentDataAssoAry[$_studentId]['classHistoryAry'][$__levelKey]['levelAcademicYearAssoAry'][$_____academicYearId]['markAry'][$____subjectId]['fullMark'] = $_____subjectFullMark;
					
					if ($_____subjectScaleDisplay=='' && $_____subjectFullMark=='N/A') {
						// 2012-1009-1115-03140 -> (Internal) Follow-up by karsonyam on 2013-07-10 09:45
						// no subject settings in this academic year => show "N/A" for both full mark and score
						$_____markDisplay = $eReportCard['RemarkNotAssessed'];
					}
					else if ($_____studentStudiesThisForm) {
						if (isset($markAry[$_____academicYearId][$_____formId][$_studentId][$____subjectId][$thisReportColumnID=0])) {
							$_____mark = my_round($markAry[$_____academicYearId][$_____formId][$_studentId][$____subjectId][$thisReportColumnID=0]['Mark'], 1);
							$_____grade = $markAry[$_____academicYearId][$_____formId][$_studentId][$____subjectId][$thisReportColumnID=0]['Grade'];
							
							$_____markDisplay = '';
							//2015-0521-1148-04073
							//if ($_____grade!='' && $lreportcard->Check_If_Grade_Is_SpecialCase($_____grade)) {
							if ($_____grade!='' && ($lreportcard->Check_If_Grade_Is_SpecialCase($_____grade) || $_____grade=='N/A')) {
								$_____markDisplay = $eReportCard['RemarkNotAssessed'];
							}
							else if ($_____subjectScaleDisplay == 'G') {
								$_____markDisplay = $_____grade;
							}
							else {
								$_____markDisplay = $_____mark;
							}
							
							//2015-0521-1148-04073
							//if ($_____markDisplay == '' || $_____markDisplay == 'N.A.') {
							if ($_____markDisplay == '' || $_____markDisplay == 'N.A.' || $_____markDisplay == 'N/A') {
								$_____markDisplay = $eReportCard['RemarkNotAssessed'];
							}
						}
						else {
							$_____markDisplay = $eReportCard['RemarkNotAssessed'];
						}
					}
					else {
						// not studied in this academic year => show "N/A"
						$_____markDisplay = $eReportCard['RemarkNotAssessed'];
					}
					
					$studentDataAssoAry[$_studentId]['classHistoryAry'][$__levelKey]['levelAcademicYearAssoAry'][$_____academicYearId]['markAry'][$____subjectId]['markDisplay'] = $_____markDisplay;
					$studentDataAssoAry[$_studentId]['classHistoryAry'][$__levelKey]['levelAcademicYearAssoAry'][$_____academicYearId]['markAry'][$____subjectId]['originalGrade'] = $_____grade;
					
				}
			}
		}
	}
}

//if ($_SESSION['UserID']==1) {
//	debug_pr($studentDataAssoAry);
//}


### set table common variables
$dateOfIssueDisplay = $Lang['eReportCard']['ReportArr']['TranscriptArr']['DateOfIssue'].' (dd-mm-yyyy): '.$dateOfIssue;
$paddingLeft = 10;
$greyBgColor = '#F2F2F2';
$gradingSchemeSeparator = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
$emptySymbol = '--';
$subjectNameStringLengthAdjustLimit = 30;

$numOfAcademicYearPerRow = 3;
$numOfRowPerPage = 2;
$numOfAcademicYearPerPage = $numOfAcademicYearPerRow * $numOfRowPerPage;
$titleColspan = ($numOfAcademicYearPerRow * 2) + 1;

$markColumnWidth = 11;
$subjectColumnWidth = floor(100 - (($numOfAcademicYearPerRow * 2) * $markColumnWidth));
?>
<style>
span.en { font-family: "Calibri"; }
span.ch { font-family: "細明體_HKSCS"; }
div.container { width: 715px; }
</style>
<?
// End of Report
$endOfReportHtml = '';
$endOfReportHtml .= '<table cellspacing="0" cellpadding="0" style="width:100%;">'."\r\n";
	$endOfReportHtml .= '<!--endOfReportTr-->'."\r\n";
	$endOfReportHtml .= '<tr><td>'.getWordSpan('en', '* 5-Excellent'.$gradingSchemeSeparator.'4-Good'.$gradingSchemeSeparator.'3-Fair'.$gradingSchemeSeparator.'2-Needs Improvement'.$gradingSchemeSeparator.'1-Unsatisfactory'.$gradingSchemeSeparator.'N/A-Not Applicable', 8).'</td></tr>'."\r\n";
	$endOfReportHtml .= '<tr><td>'.getWordSpan('en', '# Annual Total of the subject is the weighted score/level based on the term weighting each year.', 8).'</td></tr>'."\r\n";
$endOfReportHtml .= '</table>'."\r\n";

// "End of Report" row
$endOfReportTrHtml .= '<tr><td style="text-align:center;">'.getWordSpan('en', intranet_htmlspecialchars('<END OF REPORT>')).'</td></tr>'."\r\n";

// "Please Turn Over" row [2012-1009-1115-03140 -> (Internal) Follow-up by karsonyam on 2013-07-10 09:26]
$pleaseTurnOverTrHtml .= '<tr><td style="text-align:center;">'.getWordSpan('en', intranet_htmlspecialchars('<P.T.O.>')).'</td></tr>'."\r\n";

// school website and email
$reportFooterHtml = '';
$reportFooterHtml .= '<div class="container" style="page-break-after:always;">'."\r\n";
	$reportFooterHtml .= '<!--footerContentHtml-->'."\r\n";
$reportFooterHtml .= '</div>'."\r\n";

// website and email
$webSiteEmailHtml = '';
$webSiteEmailHtml .= '<table align="center" style="width:100%;">'."\r\n";
	$webSiteEmailHtml .= '<tr><td style="text-align:center;">'.getWordSpan('en', 'Website: http://www.hkbuas.edu.hk&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Email: a-school-ss@hkbuas.edu.hk', 9).'</td></tr>'."\r\n";
$webSiteEmailHtml .= '</table>'."\r\n";


$x = '';
$x .= '<div style="width:100%;" align="center">'."\r\n";
	for ($i=0; $i<$numOfStudent; $i++) {
		$_studentId = $studentIdAry[$i];
		$luserObj->LoadUserData($_studentId);
		
		$_isLastStudent = false;
		if ($i == $numOfStudent-1) {
			$_isLastStudent = true;
		}
		
		$_studentNameEn = str_replace('*', '', $luserObj->EnglishName);
		$_studentNameCh = str_replace('*', '', $luserObj->ChineseName);
		$_dateOfBirth = convertDateFormat($luserObj->DateOfBirth); 
		$_hkid = $luserObj->HKID;
		$_gender = $luserObj->Gender;
		$_studentNo = str_replace('s', '', $luserObj->UserLogin);
		
		
		// build student report header html
		$_reportHeaderHtml = '';
		$_reportHeaderHtml .= $lreportcard->Get_Empty_Image_Div(132);
		$_reportHeaderHtml .= '<table cellpadding="0px" cellspacing="0px" align="center" style="width:100%; vertical-align:top;">'."\r\n";
			$_reportHeaderHtml .= '<tr><td style="text-align:center;"><b>'.getWordSpan('en', $Lang['eReportCard']['ReportArr']['TranscriptArr']['ReportTitleEn']).'</b></td></tr>'."\r\n";
			$_reportHeaderHtml .= '<tr><td style="text-align:right;">'.getWordSpan('en', $dateOfIssueDisplay).'</td></tr>'."\r\n";
			$_reportHeaderHtml .= '<tr><td>'."\r\n";
				$_reportHeaderHtml .= '<table cellspacing="0" cellpadding="0" style="width:100%; border: 1px solid #000000;">'."\r\n";
					$_reportHeaderHtml .= '<tr style="background-color:'.$greyBgColor.';"><td colspan="6" class="border_bottom" style="padding-left: '.$paddingLeft.'px;"><b>'.getWordSpan('en', $Lang['eReportCard']['ReportArr']['TranscriptArr']['StudentParticularsEn']).'</b></td></tr>'."\r\n";
					$_reportHeaderHtml .= '<tr>'."\r\n";
						// English Student Name
						$_reportHeaderHtml .= '<td style="width:18%; padding-left: '.$paddingLeft.'px;">'.getWordSpan('en', $Lang['eReportCard']['ReportArr']['TranscriptArr']['StudentNameEn'].':').'</td>'."\r\n";
						$_reportHeaderHtml .= '<td style="width:4%;">&nbsp;</td>'."\r\n";
						$_reportHeaderHtml .= '<td style="width:28%;">'.getWordSpan('en', $_studentNameEn).'</td>'."\r\n";
						
						// Date of Birth
						$_reportHeaderHtml .= '<td style="width:16%;">'.getWordSpan('en', $Lang['eReportCard']['ReportArr']['TranscriptArr']['DateOfBirthEn'].':').'</td>'."\r\n";
						$_reportHeaderHtml .= '<td style="width:4%;">&nbsp;</td>'."\r\n";
						$_reportHeaderHtml .= '<td style="width:30%;">'.getWordSpan('en', $_dateOfBirth).'</td>'."\r\n";
					$_reportHeaderHtml .= '</tr>'."\r\n";
					$_reportHeaderHtml .= '<tr>'."\r\n";
						$_reportHeaderHtml .= '<td style="text-align:center;">'.getWordSpan('en', '('.$Lang['eReportCard']['ReportArr']['TranscriptArr']['EnglishEn'].')').'</td>'."\r\n";
						$_reportHeaderHtml .= '<td>&nbsp;</td>'."\r\n";
						$_reportHeaderHtml .= '<td>&nbsp;</td>'."\r\n";
						$_reportHeaderHtml .= '<td>'.getWordSpan('en', '(dd-mm-yyyy)').'</td>'."\r\n";
						$_reportHeaderHtml .= '<td>&nbsp;</td>'."\r\n";
						$_reportHeaderHtml .= '<td>&nbsp;</td>'."\r\n";
					$_reportHeaderHtml .= '</tr>'."\r\n";
					$_reportHeaderHtml .= '<tr>'."\r\n";
						// Chinese Student Name
						$_reportHeaderHtml .= '<td style="width:14%; padding-left: '.$paddingLeft.'px; padding-top: 3px;">'.getWordSpan('en', $Lang['eReportCard']['ReportArr']['TranscriptArr']['StudentNameEn'].':').'</td>'."\r\n";
						$_reportHeaderHtml .= '<td style="width:4%;">&nbsp;</td>'."\r\n";
						$_reportHeaderHtml .= '<td style="width:35%;">'.getWordSpan('ch', $_studentNameCh).'</td>'."\r\n";
						
						// HKID No.
						$_reportHeaderHtml .= '<td style="width:12%;">'.getWordSpan('en', $Lang['eReportCard']['ReportArr']['TranscriptArr']['HKIDEn'].':').'</td>'."\r\n";
						$_reportHeaderHtml .= '<td style="width:4%;">&nbsp;</td>'."\r\n";
						$_reportHeaderHtml .= '<td style="width:31%;">'.getWordSpan('en', $_hkid).'</td>'."\r\n";
					$_reportHeaderHtml .= '</tr>'."\r\n";
					$_reportHeaderHtml .= '<tr>'."\r\n";
						$_reportHeaderHtml .= '<td style="text-align:center;">'.getWordSpan('en', '('.$Lang['eReportCard']['ReportArr']['TranscriptArr']['ChineseEn'].')').'</td>'."\r\n";
						$_reportHeaderHtml .= '<td>&nbsp;</td>'."\r\n";
						$_reportHeaderHtml .= '<td>&nbsp;</td>'."\r\n";
						$_reportHeaderHtml .= '<td>&nbsp;</td>'."\r\n";
						$_reportHeaderHtml .= '<td>&nbsp;</td>'."\r\n";
						$_reportHeaderHtml .= '<td>&nbsp;</td>'."\r\n";
					$_reportHeaderHtml .= '</tr>'."\r\n";
					$_reportHeaderHtml .= '<tr>'."\r\n";
						// Sex
						$_reportHeaderHtml .= '<td style="padding-left: '.$paddingLeft.'px; padding-top: 3px;">'.getWordSpan('en', $Lang['eReportCard']['ReportArr']['TranscriptArr']['SexEn'].':').'</td>'."\r\n";
						$_reportHeaderHtml .= '<td>&nbsp;</td>'."\r\n";
						$_reportHeaderHtml .= '<td>'.getWordSpan('en', $_gender).'</td>'."\r\n";
						
						// Student No.
						$_reportHeaderHtml .= '<td>'.getWordSpan('en', $Lang['eReportCard']['ReportArr']['TranscriptArr']['StudentNoEn'].':').'</td>'."\r\n";
						$_reportHeaderHtml .= '<td>&nbsp;</td>'."\r\n";
						$_reportHeaderHtml .= '<td>'.getWordSpan('en', $_studentNo).'</td>'."\r\n";
					$_reportHeaderHtml .= '</tr>'."\r\n";
				$_reportHeaderHtml .= '</table>'."\r\n";
			$_reportHeaderHtml .= '</td></tr>'."\r\n";
		$_reportHeaderHtml .= '</table>'."\r\n";
		
		
		// analyze the page display of the academic result
		//$_reportPageAry[$pageNumber][$rowNumber]['levelKey'] = data
		//$_reportPageAry[$pageNumber][$rowNumber]['academicYearIdAry'][1,2,3] = $academicYearId;
		$_reportPageAry = array();
		$_curPageNumber = 0;
		$_curRowNumber = 0;
		$_pageAcademicYearCount = 0;
		$_rowAcademicYearCount = 0;
		foreach ((array)$studentDataAssoAry[$_studentId]['classHistoryAry'] as $__levelKey => $__levelDataAry) {
			if ($_curRowNumber == 0 || (($_curRowNumber-1) == $numOfRowPerPage)) {
				// open new page for the 1st academic year or the report have 2 rows already
				$_curPageNumber++;
				$_curRowNumber = 1;
				$_pageAcademicYearCount = 0;
				$_rowAcademicYearCount = 0;
			}
			else {
				// open a new row within the same page if there are only one 1 in the page
				//$_curRowNumber++;
			}
			
			$__numOfLevelAcademicYear = count((array)$__levelDataAry['levelAcademicYearAssoAry']);
			$__levelAcademicYearCount = 0; 
			
			foreach ((array)$__levelDataAry['levelAcademicYearAssoAry'] as $___academicYearId => $___academicYearInfoAry) {
				$_pageAcademicYearCount++;
				$_rowAcademicYearCount++;
				$__levelAcademicYearCount++;
				
				$_reportPageAry[$_curPageNumber][$_curRowNumber]['levelKey'] = $__levelKey;
				$_reportPageAry[$_curPageNumber][$_curRowNumber]['academicYearIdAry'][] = $___academicYearId;
				
				if ($_rowAcademicYearCount == $numOfAcademicYearPerRow || $__levelAcademicYearCount == $__numOfLevelAcademicYear) {
					// open new row if there are already 3 years in a column or need to start a new form level
					$_curRowNumber++;
					$_rowAcademicYearCount = 0;
				}
				
				// 2012-1009-1115-03140 -> (Internal) Follow-up by karsonyam on 2013-07-10 09:26
				//if ($_pageAcademicYearCount == $numOfAcademicYearPerPage || $__levelAcademicYearCount == $__numOfLevelAcademicYear) {
				if ($_curRowNumber > $numOfRowPerPage) {
					// open a new page if already have 2 rows or need to start a new form level
					$_curPageNumber++;
					$_curRowNumber = 1;
					$_pageAcademicYearCount = 0;
				}
			}
		}
		
		
		// determine student last page number for "end of report" display
		$_reportLastPageNumber = '';
		foreach ((array)$_reportPageAry as $__pageNumber => $__pageRowAry) {
			$_reportLastPageNumber = $__pageNumber;
		}
		
		
		// determine the subjects which need to be shown - show subjects which has studied in any years of the same school level
		// $_reportDisplaySubjectIdAry[$__levelKey][0,1,2,..] = $subjectId 
		$_reportDisplaySubjectIdAry = array();
		foreach ((array)$studentDataAssoAry[$_studentId]['classHistoryAry'] as $__levelKey => $__levelDataAry) {
			//2016-0811-1143-33207
//			$__levelFirstAcademicYearId = $studentDataAssoAry[$_studentId]['classHistoryAry'][$__levelKey]['firstAcademicYearId'];
//			$__levelFirstFormId = $studentDataAssoAry[$_studentId]['classHistoryAry'][$__levelKey]['firstFormId'];
//			$__levelSubjectAry = $subjectAry[$__levelFirstAcademicYearId][$__levelFirstFormId];
			
			// consolidate all studied subjects of the same school level
			$__mergedSubjectAry = array();
			foreach ((array)$__levelDataAry['levelAcademicYearAssoAry'] as $___academicYearId => $___academicYearInfoAry) {
				$___formId = $___academicYearInfoAry['formId'];
				$__mergedSubjectAry = $__mergedSubjectAry + (array)$subjectAry[$___academicYearId][$___formId];
			}
			
			//foreach ((array)$__levelSubjectAry as $___parentSubjectId => $___cmpSubjectAry) {
			foreach ((array)$__mergedSubjectAry as $___parentSubjectId => $___cmpSubjectAry) {
				$___numOfComponentSubject = count($___cmpSubjectAry) - 1;
				$___isParentSubject = ($___numOfComponentSubject > 0)? true : false;
				
				foreach ((array)$___cmpSubjectAry as $____cmpSubjectID => $____cmpSubjectName) {
					$____isComponentSubject = ($____cmpSubjectID == 0)? false : true;
					$____subjectId = ($____isComponentSubject)? $____cmpSubjectID : $___parentSubjectId;
					
					$____isAllNA = true;
					$____didNotStudiedThisLevel = true;
					foreach ((array)$__levelDataAry['levelAcademicYearAssoAry'] as $_____academicYearId => $_____academicYearInfoAry) {
						$_____studentStuided = $_____academicYearInfoAry['studentStudied'];
						$_____markDisplay = $_____academicYearInfoAry['markAry'][$____subjectId]['markDisplay'];
						$_____originalGrade = $_____academicYearInfoAry['markAry'][$____subjectId]['originalGrade'];
						
						if ($_____studentStuided) {
							$____didNotStudiedThisLevel = false;
						}
						
						if (($_____markDisplay != $eReportCard['RemarkNotAssessed'] && $_____markDisplay != 'N.A.' && $_____markDisplay != '') || $_____originalGrade == '*') {
							// show have mark subjects or dropped subjects
							$____isAllNA = false;
						}
					}
					
					if ($____didNotStudiedThisLevel || !$____isAllNA) {
						// did not studied this level => shows N/A
						$_reportDisplaySubjectIdAry[$__levelKey][] = $____subjectId;
					}
				}
			}
		}
		
		
		//$_reportPageAry[$pageNumber][$rowNumber]['levelKey'] = data
		//$_reportPageAry[$pageNumber][$rowNumber]['academicYearIdAry'][1,2,3] = $academicYearId;
		foreach ((array)$_reportPageAry as $__pageNumber => $__pageRowAry) {
			// start new page
			//$x .= '<div class="container" style="height:1010px;">'."\r\n";
			$x .= '<div class="container" style="height:1030px;">'."\r\n";
				$x .= $_reportHeaderHtml;
				$x .= '<br />'."\r\n";
				
				// academic result table
				foreach ((array)$__pageRowAry as $___rowNumber => $___rowDataAry) {
					$___levelKey = $___rowDataAry['levelKey'];
					$___academicYearIdAry = $___rowDataAry['academicYearIdAry'];
					
					$___styleBorderTop = '';
					if ($___rowNumber == 2) {
						$___styleBorderTop = 'border-top: 0px;';
					}
					
					// table title
					$x .= '<table cellspacing="0" cellpadding="0" class="border_table" style="width:100%; '.$___styleBorderTop.'">'."\r\n";
						// Academic Performance in School (Junior / Senior Grade)
						$x .= '<tr style="background-color:'.$greyBgColor.';"><td colspan="'.$titleColspan.'" style="padding-left: '.$paddingLeft.'px; '.$___styleBorderTop.'"><b>'.getWordSpan('en', $Lang['eReportCard']['ReportArr']['TranscriptArr']['AcademicPerformanceEn'].' ('.$Lang['eReportCard']['ReportArr']['TranscriptArr'][$___levelKey.'GradeEn'].')').'</b></td></tr>'."\r\n";
						
						// School Year
						$x .= '<tr>'."\r\n"; 
							$x .= '<td style="padding-left: '.$paddingLeft.'px;">'.getWordSpan('en', $Lang['eReportCard']['ReportArr']['TranscriptArr']['SchoolYearEn'], 10).'</td>'."\r\n";
							for ($j=0; $j<$numOfAcademicYearPerRow; $j++) {
								$____academicYearId = $___academicYearIdAry[$j];
								
								if ($____academicYearId == '') {
									$____display = $emptySymbol;
								}
								else {
									$____academicYearName = $studentDataAssoAry[$_studentId]['classHistoryAry'][$___levelKey]['levelAcademicYearAssoAry'][$____academicYearId]['academicYearName'];
									$____formName = $studentDataAssoAry[$_studentId]['classHistoryAry'][$___levelKey]['levelAcademicYearAssoAry'][$____academicYearId]['formName'];
									
									$____display = $____academicYearName.'('.$____formName.')';
								}
								$x .= '<td colspan="2" style="text-align:center;">'.getWordSpan('en', $____display, 10).'</td>'."\r\n";
							}
						$x .= '</tr>'."\r\n";
						
						// Subjects, Highest Score / Level*, Annual Total#
						$x .= '<tr>'."\r\n"; 
							$x .= '<td style="padding-left: '.$paddingLeft.'px; width:'.$subjectColumnWidth.'%;">'.getWordSpan('en', $Lang['eReportCard']['ReportArr']['TranscriptArr']['SubjectsEn'], 10).'</td>'."\r\n";
							for ($j=0; $j<$numOfAcademicYearPerRow; $j++) {
								$______academicYearId = $___academicYearIdAry[$j];
								$______formId = $studentDataAssoAry[$_studentId]['classHistoryAry'][$___levelKey]['levelAcademicYearAssoAry'][$______academicYearId]['formId'];
								$______formName = $studentDataAssoAry[$_studentId]['classHistoryAry'][$___levelKey]['levelAcademicYearAssoAry'][$______academicYearId]['formName'];
								
								if ($______formId == '' && $______formName == '') {
									$______highestScoreTitle = $emptySymbol;
									$______annualTotalTitle = $emptySymbol;
								}
								else {
									$______highestScoreTitle = $Lang['eReportCard']['ReportArr']['TranscriptArr']['HighestEn'].'<br />'.$Lang['eReportCard']['ReportArr']['TranscriptArr']['ScoreLevelEn'];
									$______annualTotalTitle = $Lang['eReportCard']['ReportArr']['TranscriptArr']['AnnualTotalEn'];
								}
								$x .= '<td style="text-align:center; width:'.$markColumnWidth.'%;">'.getWordSpan('en', $______highestScoreTitle, 8).'</td>'."\r\n";
								$x .= '<td style="text-align:center; width:'.$markColumnWidth.'%;">'.getWordSpan('en', $______annualTotalTitle, 8).'</td>'."\r\n";
							}
						$x .= '</tr>'."\r\n";
						
						// Subject scores
						$___levelFirstAcademicYearId = $studentDataAssoAry[$_studentId]['classHistoryAry'][$___levelKey]['firstAcademicYearId'];
						$___levelFirstFormId = $studentDataAssoAry[$_studentId]['classHistoryAry'][$___levelKey]['firstFormId'];
						
						//2016-0811-1143-33207
//						$___levelSubjectAry = $subjectAry[$___levelFirstAcademicYearId][$___levelFirstFormId];
						
						// consolidate all studied subjects of the same school level
						$___mergedSubjectAry = array();
						$___levelDataAry = $studentDataAssoAry[$_studentId]['classHistoryAry'][$___levelKey];
						foreach ((array)$___levelDataAry['levelAcademicYearAssoAry'] as $____academicYearId => $____academicYearInfoAry) {
							$____formId = $____academicYearInfoAry['formId'];
							
							$___mergedSubjectAry = $___mergedSubjectAry + (array)$subjectAry[$____academicYearId][$____formId];
						}
						
						$___displayedSubjectIdAry = array();
						foreach ((array)$___mergedSubjectAry as $____parentSubjectId => $____cmpSubjectAry) {
							$____numOfComponentSubject = count($____cmpSubjectAry) - 1;
							$____isParentSubject = ($____numOfComponentSubject > 0)? true : false;
							
							if (in_array($____parentSubjectId, (array)$___displayedSubjectIdAry)) {
								continue;
							}
							
							foreach ((array)$____cmpSubjectAry as $_____cmpSubjectID => $_____cmpSubjectName) {
								$_____isComponentSubject = ($_____cmpSubjectID == 0)? false : true;
								$_____subjectId = ($_____isComponentSubject)? $_____cmpSubjectID : $____parentSubjectId;
								
								// only display Maths component subject as "Mathematics - Compulsory" and "Mathematics - Module 1 / 2"
								if ($____parentSubjectId != $lreportcard->Get_SubjectID_From_Config('Maths') && $_____isComponentSubject) {
									continue;
								}
								
								// skip Maths main subject for higher form
								if ($_____subjectId == $lreportcard->Get_SubjectID_From_Config('Maths') && $____isParentSubject) {
									continue;
								}
								
								// skip the no need display subjects
								if (!in_array($_____subjectId, (array)$_reportDisplaySubjectIdAry[$___levelKey])) {
									continue;
								}
								
								// get subject name
								$_____subjectNameEn = $lreportcard->GET_SUBJECT_NAME_LANG($_____subjectId, 'en');
								$_____subjectNameCh = $lreportcard->GET_SUBJECT_NAME_LANG($_____subjectId, 'ch');
								
								// get subject grading scheme info
								$_____subjectGradingSchemeInfoAry = $gradingSchemeAry[$___levelFirstAcademicYearId][$___levelFirstFormId][$____parentSubjectId];	// component subject does not have this settings => component subject follows parent subject settings
								$_____subjectLangDisplay = $_____subjectGradingSchemeInfoAry['LangDisplay'];
								
								if ($_____subjectLangDisplay == '') {
									$_____subjectLangDisplay = 'en';
								}
								
								// determine subject name display
								$_____subjectNameDisplay = '';
								if ($____parentSubjectId == $lreportcard->Get_SubjectID_From_Config('Maths') && $_____isComponentSubject) {
									// e.g. Mathematics - Complusory
									$_____parentSubjectNameEn = $lreportcard->GET_SUBJECT_NAME_LANG($____parentSubjectId, 'en');
									$_____parentSubjectNameCh = $lreportcard->GET_SUBJECT_NAME_LANG($____parentSubjectId, 'ch');
									
									if ($_____subjectLangDisplay == 'en') {
										if (strlen($_____parentSubjectNameEn.' - '.$_____subjectNameEn) > $subjectNameStringLengthAdjustLimit) {
											$_____fontSize = 9;
										}
										else {
											$_____fontSize = 11;
										}
										$_____subjectNameDisplay = getWordSpan('en', $_____parentSubjectNameEn.' - '.$_____subjectNameEn, $_____fontSize);
									}
									else if ($_____subjectLangDisplay == 'ch') {
										if (strlen($_____parentSubjectNameCh.' - '.$_____subjectNameCh) > $subjectNameStringLengthAdjustLimit) {
											$_____fontSize = 9;
										}
										else {
											$_____fontSize = 11;
										}
										$_____subjectNameDisplay = getWordSpan('ch', $_____parentSubjectNameCh.' - '.$_____subjectNameCh, $_____fontSize);
									}
								}
								else {
									if ($____parentSubjectId == $lreportcard->Get_SubjectID_From_Config('CombinedScience') && $____isParentSubject) {
										// display the name of the student studied components
										$_____studentStudiedCmpSubjectNameEnAry = array();
										$_____studentStudiedCmpSubjectNameChAry = array();
										foreach ((array)$____cmpSubjectAry as $_____tempCmpSubjectID => $_____tempCmpSubjectName) {
											if ($_____tempCmpSubjectID == 0) {
												continue;
											}
											
											$_____originalGrade = $studentDataAssoAry[$_studentId]['classHistoryAry'][$___levelKey]['levelAcademicYearAssoAry'][$___levelFirstAcademicYearId]['markAry'][$_____tempCmpSubjectID]['originalGrade'];
											if ($_____originalGrade != 'N.A.') {
												$_____studentStudiedCmpSubjectNameEnAry[] = $lreportcard->GET_SUBJECT_NAME_LANG($_____tempCmpSubjectID, 'en');
												$_____studentStudiedCmpSubjectNameChAry[] = $lreportcard->GET_SUBJECT_NAME_LANG($_____tempCmpSubjectID, 'ch');
											}
										}
										
										$_____subjectNameEn .= ' ('.implode(' & ', (array)$_____studentStudiedCmpSubjectNameEnAry).')';
										$_____subjectNameCh .= ' ('.implode(' & ', (array)$_____studentStudiedCmpSubjectNameChAry).')';
									}
									
									if ($_____subjectLangDisplay == 'en') {
										if (strlen($_____subjectNameEn) > $subjectNameStringLengthAdjustLimit) {
											$_____fontSize = 9;
										}
										else {
											$_____fontSize = 11;
										}
										$_____subjectNameDisplay = getWordSpan('en', $_____subjectNameEn, $_____fontSize);
									}
//									else if ($_____subjectLangDisplay == 'ch') {
//										if (strlen($_____subjectNameCh) > $subjectNameStringLengthAdjustLimit) {
//											$_____fontSize = 9;
//										}
//										else {
//											$_____fontSize = 11;
//										}
//										$_____subjectNameDisplay = getWordSpan('ch', $_____subjectNameCh, $_____fontSize);
//									}
									else if ($_____subjectLangDisplay == 'ch' || $_____subjectLangDisplay == 'both') {
										if (strlen($_____subjectNameCh.' '.$_____subjectNameEn) > $subjectNameStringLengthAdjustLimit) {
											$_____fontSize = 9;
										}
										else {
											$_____fontSize = 11;
										}
										$_____subjectNameDisplay = getWordSpan('ch', $_____subjectNameCh, $_____fontSize).' '.getWordSpan('en', $_____subjectNameEn, $_____fontSize);
									}
								}
								
								$_____subjectTr = '';
								$_____subjectTr .= '<tr style="height: 22px;">'."\r\n"; 
									$_____subjectTr .= '<td style="padding-left: '.$paddingLeft.'px;">'.$_____subjectNameDisplay.'</td>'."\r\n";
									
//									$_____isAllNA = true;
//									$_____isAllYearNotStudied = true;
									for ($j=0; $j<$numOfAcademicYearPerRow; $j++) {
										$______academicYearId = $___academicYearIdAry[$j];
										$______formId = $studentDataAssoAry[$_studentId]['classHistoryAry'][$___levelKey]['levelAcademicYearAssoAry'][$______academicYearId]['formId'];
										$______formName = $studentDataAssoAry[$_studentId]['classHistoryAry'][$___levelKey]['levelAcademicYearAssoAry'][$______academicYearId]['formName'];
										$______formGradeNumber = $studentDataAssoAry[$_studentId]['classHistoryAry'][$___levelKey]['levelAcademicYearAssoAry'][$______academicYearId]['formGradeNumber'];
										$______studentStudied = $studentDataAssoAry[$_studentId]['classHistoryAry'][$___levelKey]['levelAcademicYearAssoAry'][$______academicYearId]['studentStudied'];
										
//										if ($______studentStudied) {
//											$_____isAllYearNotStudied = false;
//										}
										
										// 2015-0211-1503-22073
//										if ($______academicYearId == 9 && $_____subjectId == $lreportcard->Get_SubjectID_From_Config('Chinese(NSS)') && !$_____isComponentSubject) {
//											// 2014-0312-1455-46073
//											// for 2013-2014 academic year, auto map to Chinese (2013)
//											$_____targetSubjectId = $lreportcard->Get_SubjectID_From_Config('Chinese(2013)');
//										}
//										else if ($______academicYearId == 9 && $_____subjectId == $lreportcard->Get_SubjectID_From_Config('VisualArts') && !$_____isComponentSubject) {
//											// 2014-0312-1455-46073
//											// for 2013-2014 academic year, map to VisualArts(HKDSE) to VisualArts
//											$_____targetSubjectId = $lreportcard->Get_SubjectID_From_Config('VisualArts(HKDSE)');
//										}
//										else if ($______academicYearId == 9 && $_____subjectId == $lreportcard->Get_SubjectID_From_Config('Music') && !$_____isComponentSubject) {
//											// 2014-0312-1455-46073
//											// for 2013-2014 academic year, map to Music(HKDSE) to Msuic
//											$_____targetSubjectId = $lreportcard->Get_SubjectID_From_Config('Music(HKDSE)');
//										}
//										else if (($______formGradeNumber == 7 || $______formGradeNumber == 8) && $_____subjectId == $lreportcard->Get_SubjectID_From_Config('Chinese(NSS)') && !$_____isComponentSubject) {
//											// for G7 G8 Chinese, auto map to Chinese
//											$_____targetSubjectId = $lreportcard->Get_SubjectID_From_Config('Chinese');
//										}
//										else if ($______formGradeNumber >= 9 && $_____subjectId == $lreportcard->Get_SubjectID_From_Config('Chinese') && !$_____isComponentSubject) {
//											// for G9 or higher Chinese, auto map to Chinese (NSS) subject
//											$_____targetSubjectId = $lreportcard->Get_SubjectID_From_Config('Chinese(NSS)');
//										}
//										else {
//											$_____targetSubjectId = $_____subjectId;
//										}
										$_____targetSubjectId = getTargetSubjectId($______academicYearId, $_____subjectId, $_____isComponentSubject, $______formGradeNumber);
										$___displayedSubjectIdAry[] = $_____targetSubjectId;
										
										$______subjectScaleDisplay = $studentDataAssoAry[$_studentId]['classHistoryAry'][$___levelKey]['levelAcademicYearAssoAry'][$______academicYearId]['markAry'][$_____targetSubjectId]['scaleDisplay'];
										$______subjectYearFullMark = $studentDataAssoAry[$_studentId]['classHistoryAry'][$___levelKey]['levelAcademicYearAssoAry'][$______academicYearId]['markAry'][$_____targetSubjectId]['fullMark'];
										
										$______fontSize = ($______subjectScaleDisplay == 'G')? 10 : 11;
										
										if ($______formId == '' && $______formName != '') {
											// student did not studied in that year => show "N/A"
											$_____subjectTr .= '<td style="text-align:center;">'.getWordSpan('en', $______subjectYearFullMark, $______fontSize).'</td>'."\r\n";
											$_____subjectTr .= '<td style="text-align:center;"><b>'.getWordSpan('en', $eReportCard['RemarkNotAssessed'], $______fontSize).'</b></td>'."\r\n";
										}
										else if ($______formId == '' && $______formName == '') {
											// empty year column => show "--"
											$_____subjectTr .= '<td style="text-align:center;">'.getWordSpan('en', $emptySymbol).'</td>'."\r\n";
											$_____subjectTr .= '<td style="text-align:center;"><b>'.getWordSpan('en', $emptySymbol).'</b></td>'."\r\n";
										}
										else {
											// student studied this academic year => show score
											$______subjectMarkDisplay = $studentDataAssoAry[$_studentId]['classHistoryAry'][$___levelKey]['levelAcademicYearAssoAry'][$______academicYearId]['markAry'][$_____targetSubjectId]['markDisplay'];
											$______originalGrade = $studentDataAssoAry[$_studentId]['classHistoryAry'][$___levelKey]['levelAcademicYearAssoAry'][$______academicYearId]['markAry'][$_____targetSubjectId]['originalGrade'];
											
//											if (($______subjectMarkDisplay != $eReportCard['RemarkNotAssessed'] && $______subjectMarkDisplay != 'N.A.') || $______originalGrade == '*') {
//												$_____isAllNA = false;
//											}
											
											$_____subjectTr .= '<td style="text-align:center;">'.getWordSpan('en', $______subjectYearFullMark, $______fontSize).'</td>'."\r\n";
											$_____subjectTr .= '<td style="text-align:center;"><b>'.getWordSpan('en', $______subjectMarkDisplay, $______fontSize).'</b></td>'."\r\n";
										}
									}
								$_____subjectTr .= '</tr>'."\r\n";
								
//								if ($_____isAllYearNotStudied || !$_____isAllNA) {
									$x .= $_____subjectTr;
//								}
							}
						}
						
					$x .= '</table>'."\r\n";
				}
				
				if ($__pageNumber == $_reportLastPageNumber) {
					$__endOfReportHtml = str_replace('<!--endOfReportTr-->', $endOfReportTrHtml, $endOfReportHtml);
				}
				else {
					//$__endOfReportHtml = $endOfReportHtml;
					$__endOfReportHtml = str_replace('<!--endOfReportTr-->', $pleaseTurnOverTrHtml, $endOfReportHtml);
				}
				$x .= $__endOfReportHtml;
				
			$x .= '</div>'."\r\n";
			
//			$x .= $reportFooterHtml;
			// no page break for last student to avoid extra blank page
			if ($_isLastStudent) {
				$__reportFooterHtml = str_replace('style="page-break-after:always;"', '', $reportFooterHtml);
			}
			else {
				$__reportFooterHtml = $reportFooterHtml;
			}
			
			// add website and email info for the odd number pages
			if ($__pageNumber % 2 == 1) {
				$__reportFooterHtml = str_replace('<!--footerContentHtml-->', $webSiteEmailHtml, $__reportFooterHtml);
			}
			
			$x .= $__reportFooterHtml;
		}
	}
$x .= '</div>'."\r\n";

echo $x;


// For performance tunning info
//echo '<script language="JavaScript" src="/templates/jquery/jquery-1.3.2.min.js"></script>';
//debug_pr('convert_size(memory_get_usage(true)) = '.convert_size(memory_get_usage(true)));
//debug_pr('Query Count = '.$GLOBALS[debug][db_query_count]);
//$lreportcard->db_show_debug_log();
//die();
?>