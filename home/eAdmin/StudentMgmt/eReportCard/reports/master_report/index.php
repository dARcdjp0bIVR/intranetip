<?php
#  Editing by  

/****************************************************
 * Modification log
 *  Master Report 
 *  20180702 Ivan:
 *      modified js preset(), to exclude active year selection when apply preset settings
 *  20180628 Bill:  [2017-1204-1601-38164]
 *      added active year selection ($eRCTemplateSetting['Report']['SupportAllActiveYear'])
 * 	20170519 Bill:
 * 		View Group no need to check access right
 * 	20150130 Bill:
 * 		added $eRCTemplateSetting['Report']['MasterReport']['ShowNotGeneratedReport'] - allow display not generated report
 * 	20120308 Marcus:
 * 		-improved Master Report to display Class Teacher  // 2011-1111-1552-56042 - 聖若瑟教區中學(第二、三校) Master Report  
 * 	20111012 Marcus:
 * 		Cater show coresponding subject only for subject group  
 * **************************************************/

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libteaching.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if (!$plugin['ReportCard2008'])
{
	header ("Location: /");
	intranet_closedb();
	exit();
}
 
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
{
	include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
	$lreportcard = new libreportcardcustom();
} else {
	$lreportcard = new libreportcard();
}

if (!$lreportcard->hasAccessRight()) 
{
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lreportcard_ui = new libreportcard_ui();
$linterface = new interface_html();

$CurrentPage = "Reports_MasterReport";
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();

# tag information
$TAGS_OBJ[] = array($eReportCard['Reports_MasterReport'], "", 0);
$linterface->LAYOUT_START();

$checkPermissionFormClass = ($lreportcard->IS_ADMIN_USER_VIEW($_SESSION['UserID']) || $lreportcard->IS_VIEW_GROUP_USER_VIEW($_SESSION['UserID']))? 0 : 3;
$checkPermissionSubject = ($lreportcard->IS_ADMIN_USER_VIEW($_SESSION['UserID']) || $lreportcard->IS_VIEW_GROUP_USER_VIEW($_SESSION['UserID']))? 0 : 1;

// 2014-1014-1144-21164
// Added: 2015-01-30
$onlyShowGeneratedReport = $eRCTemplateSetting['Report']['MasterReport']['ShowNotGeneratedReport']? 0 : 1;
echo $lreportcard_ui->Get_Master_Report_Index_UI($PresetID);

?>

<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery-ui-1.7.3.custom.min.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.tablednd_0_5.js"></script>
<script>
var SchoolName = "<?=addslashes(htmlspecialchars_decode(GET_SCHOOL_NAME()))?>";
var loading = '<?=$linterface->Get_Ajax_Loading_Image()?>';
var checkPermissionFormClass = '<?=$checkPermissionFormClass?>';
var checkPermissionSubject = '<?=$checkPermissionSubject?>';

function toggleLayer()
{
	$("#PresetLayer").toggle();
}

function showOption(id)
{
	var tableID = "#table"+id;
	var showspanID = "#spanShowOption"+id;
	var hidespanID = "#spanHideOption"+id;

	$(tableID).show();
	$(hidespanID).show();
	$(showspanID).hide();
}

function hideOption(id)
{
	var tableID = "#table"+id;
	var showspanID = "#spanShowOption"+id;
	var hidespanID = "#spanHideOption"+id;

	$(tableID).hide();
	$(hidespanID).hide();
	$(showspanID).show();	
}

function js_Reload_Selection()
{
	var jsActiveYear = 1;
	if($("select#targetActiveYear")){
		jsActiveYear = $("select#targetActiveYear").val();
	}
	var jsYearID = $("select#ClassLevelID").val();
	
	$('span#ReportSelectionSpan').html(loading);
	$('span#ClassSelectionSpan').html(loading);
	$('span#SubjectSelectionSpan').html(loading);
		
	$('span#ReportSelectionSpan').load(
		"../ajax_reload_selection.php", 
		{ 
			RecordType: 'Report',
			ActiveYear: jsActiveYear,
			YearID: jsYearID,
			ReportID: '',
			SelectionID: 'ReportID',
			onChange: 'js_Reload_Report_Column(); js_Reload_Subject_Selection();',
			HideNonGenerated: <?=$onlyShowGeneratedReport?> 
		},
		function(ReturnData)
		{
			js_Reload_Report_Column();
			//$('#IndexDebugArea').html(ReturnData);
			js_Reload_Subject_Selection();
		}
	);
	
	$('span#ClassSelectionSpan').load(
		"../ajax_reload_selection.php", 
		{ 
			RecordType: 'Class',
			ActiveYear: jsActiveYear,
			YearID: jsYearID,
			SelectionID: 'YearClassIDArr[]',
			isMultiple: 1,
			TeachingOnly: checkPermissionFormClass
		},
		function(ReturnData)
		{
			SelectAll(document.form1.elements['YearClassIDArr[]']);
			//$('#IndexDebugArea').html(ReturnData);
		}
	);
	
//	$('span#SubjectSelectionSpan').load(
//		"../ajax_reload_selection.php", 
//		{ 
//			RecordType: 'Subject',
//			YearID: jsYearID,
//			SelectionID: 'SubjectIDArr[]',
//			IncludeComponent: 1,
//			onChange: 'refreshSortingTable(); '
//			
//		},
//		function(ReturnData)
//		{
//			SelectAll(document.form1.elements['SubjectIDArr[]']);
//			//$('#IndexDebugArea').html(ReturnData);
//		}
//	);
}

function js_Reload_Subject_Selection()
{
	var jsActiveYear = 1;
	if($("select#targetActiveYear")){
		jsActiveYear = $("select#targetActiveYear").val();
	}
	var YearID = $("#ClassLevelID").val();
	var ReportID = $("#ReportID").val();
	
	$('span#SubjectSelectionSpan').html(loading);
	$('span#StdSubjectSelectionSpan').html(loading);
	$('span#SortByClassSelectionSpan').html(loading);
	$('span#ReportColumnSelectionSpan').html(loading);
	
	$('span#SubjectSelectionSpan').load(
		"../ajax_reload_selection.php", 
		{ 
			RecordType: 'Subject',
			ActiveYear: jsActiveYear,
			YearID: YearID,
			SelectionID: 'SubjectIDArr[]',
			IncludeComponent: 1,
			onChange: 'refreshSortingTable()',
			TeachingOnly: checkPermissionSubject
		},
		function(ReturnData)
		{
			SelectAll(document.form1.elements['SubjectIDArr[]']);
			//$('#IndexDebugArea').html(ReturnData);
			js_Toggle_Subject_Selection();
			refreshSortingTable();
		}
	);

	$('span#StdSubjectSelectionSpan').load(
		"../ajax_reload_selection.php", 
		{ 
			RecordType: 'Subject',
			ActiveYear: jsActiveYear,
			YearID: YearID,
			SelectionID: 'StdSubjectIDArr[]',
			onChange: 'js_Reload_Subject_Group_Selection()'
			
		},
		function(ReturnData)
		{
			js_Select_All_Subject_Group_Subject();
		}
	);
	
	$.post
	(
		"../ajax_reload_selection.php", 
		{ 
			RecordType: 'Subject',
			ActiveYear: jsActiveYear,
			YearID: YearID,
			SelectionID: 'SortSubjectIDClass',
			IncludeComponent: 1,
			InputMarkOnly: 1,
			includeGrandMarks: 1,
			isMultiple:0,
			OtherTagInfo: "class='SortByClassSubjectSelection'",
			ReportID:ReportID
		},
		function(ReturnData)
		{
			$('span#SortByClassSelectionSpan').html(ReturnData);
			ChangeSortBy();
		}
	);
	
	$.post
	(
		"../ajax_reload_selection.php", 
		{ 
			RecordType: 'Subject',
			ActiveYear: jsActiveYear,
			YearID: YearID,
			SelectionID: 'SortSubjectIDForm',
			IncludeComponent: 1,
			InputMarkOnly: 1,
			includeGrandMarks: 1,
			isMultiple:0,
			OtherTagInfo: "class='SortByFormSubjectSelection'",
			ReportID:ReportID
		},
		function(ReturnData)
		{
			$('span#SortByFormSelectionSpan').html(ReturnData);
			ChangeSortBy();
		}
	);
}

function js_Reload_Report_Column()
{
	var jsActiveYear = 1;
	if($("select#targetActiveYear")){
		jsActiveYear = $("select#targetActiveYear").val();
	}
	var ReportID = $("select#ReportID").val();
	
	$.post(
		"../ajax_reload_selection.php", 
		{ 
			RecordType: 'ReportColumnCheckbox',
			ActiveYear: jsActiveYear,
			ReportID: ReportID
		},
		function(ReturnData)
		{
			var DataArr = ReturnData.split(":_delimiter_:");
			$('span#ReportColumnSelectionSpan').html(DataArr[2]);
			
			//change report title
			var ReportTitle = DataArr[0];
			$("#ReportTitle").val(SchoolName+" "+ReportTitle);
			
			// show/ hide item
			var isTermReport = DataArr[1];
			handleTermReport(isTermReport);
			refreshSortingTable();
			jsShowHideColumnLabelOption();
			preset();
						
		}
	);

}

function handleTermReport(isTermReport)
{
	if(isTermReport==1)
	{
		$("span#OrderMeritSubjectGroupSpan").show().find("input#OrderMeritSubjectGroup").attr("disabled","");
		$("tr#DataSourceRow").hide().find("input").attr("disabled","disabled");
		
	}
	else
	{
		$("span#OrderMeritSubjectGroupSpan").hide().find("input#OrderMeritSubjectGroup").attr("disabled","disabled");
		$("tr#DataSourceRow").show().find("input").attr("disabled","");
	}
}

// refresh Column Order List for user to adjust column order
function refreshSortingTable()
{
	if($("input.ColumnOrderType:checked").val()==0)
		var hiddenList = 'display:none;';
	else
		 var hiddenList = '';
	
	var spacer = '<img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="20" height="1">';
	var MoveLink = '<?=$linterface->GET_LNK_MOVE("javascript:void(0)", "test")?>';
	var OrderOptionsArr = new Array();
	var html = '<table class="DragAndDrop common_table_list" cellpadding=0 cellspacing=0 style="width:250px; margin-left:0; '+hiddenList+'" >';
	$("input.StudentDataDisplayChk:checkbox:checked").each(function(){
		var label = $(this).siblings("label").html();
		html += "<tr class='StudentData'><td class='Dragable' width='20px'>"+spacer+MoveLink+"</td><td nowrap>"+label+"<input type='hidden' value='"+$(this).val()+"' name='ColumnOrder[]'><input type='hidden' value='StudentInfo' name='ColumnType[]'></td></tr>";
	});
	
	$("select[name='SubjectIDArr\\[\\]']").children(":selected").each(function(){
		html += "<tr class='SubjectData'><td class='Dragable' width='20px'>"+spacer+MoveLink+"</td><td nowrap>"+$(this).html()+"<input type='hidden' value='"+$(this).val()+"' name='ColumnOrder[]'><input type='hidden' value='"+$(this).val()+"' name='ColumnType[]'></td></tr>";
	});
	$("input.OtherInfoDataChk:checkbox:checked").each(function(){
		var label = $(this).siblings("label").html();
		html += "<tr class='OtherInfoData'><td class='Dragable' width='20px'>"+spacer+MoveLink+"</td><td nowrap>"+label+"<input type='hidden' value='"+$(this).val()+"' name='ColumnOrder[]'><input type='hidden' value='OtherInfo' name='ColumnType[]'></td></tr>";
	});
	$("input.GrandMarkDataChk:checkbox:checked").each(function(){
		var label = $(this).siblings("label").html();
		html += "<tr class='GrandMarkData'><td class='Dragable' width='20px'>"+spacer+MoveLink+"</td><td nowrap>"+label+"<input type='hidden' value='"+$(this).val()+"' name='ColumnOrder[]'><input type='hidden' value='GrandMark' name='ColumnType[]'></td></tr>";
	});
	html += "</table>";
	
	$("#ColumnOrderSpan").html(html);
	
	Init_DND_Table();
}


function SelectAll(obj)
{
	$(obj).children().attr("selected","selected")
}

function jsSelectAllCheckbox(jsChecked, jsCheckboxClass)
{
	$('input.' + jsCheckboxClass).each( function() {
		$(this).attr('checked', jsChecked);
	})
}

function jsCheckUnSelectAll(jsChecked, jsSelectAllChkID)
{
	if (jsChecked == false)
		$('input#' + jsSelectAllChkID).attr('checked', false);
}

function ChangeSortBy()
{
	var sortby = $("input[name='SortBy']:checked").val();
	
	switch(sortby)
	{
		case '0' : //class class no
			$("select.SortByClassSubjectSelection").attr("disabled","disabled");
			$("select.SortByFormSubjectSelection").attr("disabled","disabled");
		break;
		case '1' : //class rank
			$("select.SortByClassSubjectSelection").attr("disabled",'');
			$("select.SortByFormSubjectSelection").attr("disabled","disabled");
		break;
		case '2' : //form rank			
			$("select.SortByFormSubjectSelection").attr("disabled",'');
			$("select.SortByClassSubjectSelection").attr("disabled","disabled");
		break;	
	}
}

function SelectStyle()
{
	var SelectedRadioID = $("input.ReportStyle:checked").attr("id");
	$("img.checkpic").removeClass("checkpic").addClass("uncheckpic");
	$("img#"+SelectedRadioID+"Pic").removeClass("uncheckpic").addClass("checkpic");
	$("input[class^='Separator']").attr("disabled","disabled");
	$("input.Separator"+SelectedRadioID).attr("disabled","");		
}

function jsShowHideColumnLabelOption()
{
	var ColumnCheckedCount =$("input.ReportColumnChk:checked").length; 

	if(ColumnCheckedCount==1)
	{
		$("tr#ColumnLabelOptionRow:hidden").show();
	}
	else
	{
		$("input#DisplayColumnLabel").attr("checked","checked");
		$("tr#ColumnLabelOptionRow:visible").hide();
	}
}

function js_Toggle_Student_Selection()
{
	if($(".SelectFrom:checked").val()=='class')
	{
		$(".SubjectGroupSelectOption").hide()
		$(".ClassSelectOption").show()
	}
	else
	{
		$(".SubjectGroupSelectOption").show()
		$(".ClassSelectOption").hide()
	}
	js_Toggle_Subject_Selection()
	
}

function js_Select_All_Term()
{
	js_Select_All('TermID');
	js_Reload_Subject_Group_Selection();
}

function js_Reload_Subject_Group_Selection()
{
	var jsActiveYear = 1;
	if($("select#targetActiveYear")){
		jsActiveYear = $("select#targetActiveYear").val();
	}
	var ClassLevelID = $("#ClassLevelID").val();
	var TermID = '';
	var SubjectID = '';
	
	if($("select#TermID").val())
	{
		TermID = $("select#TermID").val().toString();	
	}
	if($("select#StdSubjectIDArr\\[\\]").val())
	{
		SubjectID = $("select#StdSubjectIDArr\\[\\]").val().toString();	
	}
	
	if(!$("select#TermID").val() || !$("select#StdSubjectIDArr\\[\\]").val())
		return false;
		
	$("#SubjectGroupSelectionSpan").html(loading);	
		
	$.post(
		"../ajax_reload_selection.php", 
		{ 
			RecordType: 'SubjectGroup',
			ActiveYear: jsActiveYear,
			SelectionID: 'SubjectGroupIDArr[]',
			ClassLevelID: ClassLevelID,
			SemID:TermID,
			SubjectID:SubjectID,
			isMultiple:1,
			noFirst:1
		},
		function(ReturnData)
		{
			$("#SubjectGroupSelectionSpan").html(ReturnData);
			js_Select_All('SubjectGroupIDArr[]');
		}
	);
	
}

function js_Select_All_Subject_Group_Subject()
{
	js_Select_All('StdSubjectIDArr[]');
	js_Reload_Subject_Group_Selection();
}

function js_Reload_Active_Year_Setting()
{
	var jsActiveYear = 1;
	if($("select#targetActiveYear")){
		jsActiveYear = $("select#targetActiveYear").val();
	}
	
	$.post(
		"../ajax_reload_selection.php", 
		{
			RecordType: 'ClassLevelSelection',
			ActiveYear: jsActiveYear
		},
		function(ReturnData)
		{
			$("td#FormSelectionTD").html(ReturnData);
			js_Reload_Selection();
		}
	);

	$.post(
		"../ajax_reload_selection.php", 
		{
			RecordType: 'YearTermSelection',
			ActiveYear: jsActiveYear
		},
		function(ReturnData)
		{
			$("td#TermSelectionTD").html(ReturnData);
			js_Select_All('TermID');
		}
	);
}

$().ready(function(){
	js_Reload_Selection();

	ChangeSortBy();
	SelectStyle();
	$("ul.sortable").sortable({
		placeholder: 'placeholder'
	});
	$("ul.sortable").disableSelection();
	js_Toggle_Student_Selection();
	js_Select_All_Term();
	js_Toggle_Display_Class_Teacher();
	
});

function js_Check_Form()
{
	if($(".SelectFrom:checked").val()=='class')
	{
		// User must select at least one Class
		if (countOption(document.getElementById('YearClassIDArr[]')) == 0)
		{
			alert('<?=$eReportCard['jsSelectSubjectWarning']?>');
			document.getElementById('YearClassIDArr[]').focus();
			return false;
		}
	}
	else
	{
		if (countOption(document.getElementById('SubjectGroupIDArr[]')) == 0)
		{
			alert('<?=$Lang['eReportCard']['WarningArr']['Select']['SubjectGroup']?>');
			document.getElementById('SubjectGroupIDArr[]').focus();
			return false;
		}
	}
	
	var jsFocusElementID;
	var jsHasChecked = false;
	var jsElementCounter = 0;
	
	// User must select at least one student display data 
	$('input.StudentDataDisplayChk').each( function() {
		if (jsElementCounter == 0)
			jsFocusElementID = $(this).attr('id');	// focus the first element if the user selected nth
			
		jsElementCounter++;
		
		if ($(this).attr('checked') == true)
		{
			jsHasChecked = true;
			return false;
		}
	});
	
	if (jsHasChecked == false)
	{
		alert('<?=$eReportCard['MasterReport']['jsWarningArr']['SelectStudentdisplayInfo']?>');
		$('input#' + jsFocusElementID).focus();
		return false;
	}
	
	// User must select at least one Subject
	if (countOption(document.getElementById('YearClassIDArr[]')) == 0)
	{
		alert('<?=$eReportCard['jsSelectClassWarning']?>');
		document.getElementById('YearClassIDArr[]').focus();
		return false;
	}
	
	// user must select at least one report column for display
	jsHasChecked = false;
	jsElementCounter = 0;
	$('input.ReportColumnChk').each( function() {
		if (jsElementCounter == 0)
			jsFocusElementID = $(this).attr('id');	// focus the first element if the user selected nth
			
		jsElementCounter++;
		
		if ($(this).attr('checked') == true)
		{
			jsHasChecked = true;
			return false;
		}
	});
	
	if (jsHasChecked == false)
	{
		alert('<?=$eReportCard['MasterReport']['jsWarningArr']['SelectReportColumn']?>');
		$('input#' + jsFocusElementID).focus();
		return false;
	}
	
	// user must select at least one subject data for display
	jsHasChecked = false;
	jsElementCounter = 0;
	$('input.SubjectDataChk').each( function() {
		if (jsElementCounter == 0)
			jsFocusElementID = $(this).attr('id');	// focus the first element if the user selected nth
			
		jsElementCounter++;
		
		if ($(this).attr('checked') == true && $(this).attr('disabled') == false)	// for disabled SubjectGroup selection if Consolidated Report
		{
			jsHasChecked = true;
			return false;
		}
	});
	
	if (jsHasChecked == false)
	{
		alert('<?=$eReportCard['MasterReport']['jsWarningArr']['SelectSubjectData']?>');
		$('input#' + jsFocusElementID).focus();
		return false;
	}
	
	document.form1.action = "master_report.php";
	document.form1.target = "_blank";
	document.form1.submit();
	
}

// Drag and Drop table init
function Init_DND_Table() 
{
	var objDom = "table.DragAndDrop"
	
	var JQueryObj1 = $(objDom);
	
	// form drag and drop
	JQueryObj1.tableDnD({
		onAllowDrop:function(From,To){
			// avoid putting non-studentdata at the top 
			if($(JQueryObj1).find("tr").index(From)==0 && !$(JQueryObj1).find("tr").eq(1).hasClass("StudentData"))
				return false;				
			
			if(!$(From).hasClass("StudentData") && $(JQueryObj1).find("tr").index(To)==0)
				return false;
			
			return true;
		},
		dragHandle: "Dragable", 
		onDragClass: "move_selected"
	});
}

function refreshFormatOptions(format) 
{
	var showClass = format==1?"CSVSetting":"HTMLSetting";
	var hideClass = format==1?"HTMLSetting":"CSVSetting";
	
	$("tr."+showClass).show();
	$("tr."+hideClass).hide();
	
}

function preset()
{
	if($("[name='PresetID']:checked").val()=="new")
	{
		$("form select").attr("selected","selected");
		$("form input:checkbox").not(".ShowStyleChk").not(".ExcludeSubjectComponentChk").attr("checked","checked");
		return false;
	}
		
	$.post(
		"ajax_preset.php",
		{ 
			RecordType: 'GetMasterReportPreset',
			PresetID: $("[name='PresetID']:checked").val()
		},
		function(ReturnData)
		{
			$("select").not("#targetActiveYear").not("#YearClassIDArr\\[\\]").not("#ReportID").not("#ClassLevelID").children().attr("selected","");
			$("input:checkbox").not("[name='ReportColumnID[]']").attr("checked","");
			for(x in ReturnData)
			{
				
				var ElementType = $("[name='"+x+"']").attr("tagName");

				var InputType = $("[name='"+x+"']").attr("type");	
				for(var i=0 ; i<ReturnData[x].length; i++)
				{
					var value = ReturnData[x][i];
					if(ElementType=="INPUT")
					{
						if(InputType.toLowerCase()=="text")
						{
							$("input[name='"+x+"']").val(value);
						}
						else if(InputType.toLowerCase()=="radio")
						{
							$("input[name='"+x+"']:[value='"+value+"']").click();
						}
						else
						{
							$("input[name='"+x+"']:[value='"+value+"']").attr("checked","checked");
						}
							
					}
					else
					{
						$("select[name='"+x+"']").find(":options[value='"+value+"']").attr("selected","selected");
					}
				}	
			}
		},
		"json"	
	);
	
	$("input#ColumnOrderDefault").click();
	
}

function save_preset()
{
	if($("input:radio[name=PresetID]:checked").val()!='new')
	{
		if(!confirm("<?=$eReportCard['MasterReport']['jsConfirmArr']['ModifyPreset']?>"))
			return false;
	}
	else
	{
		if($("input#PresetName").val() == '<?=$eReportCard['MasterReport']['jsConfirmArr']['UseDefaultPresetName']?>')
		{
			if(!confirm("<?=$eReportCard['MasterReport']['jsConfirmArr']['UseDefaultPresetName']?>"))
				return false;
		}
	}
	
	document.form1.action = "save_preset.php";
	document.form1.target = "_self";
	document.form1.submit();
}

function delete_preset(PresetID)
{
	if(confirm("<?=$eReportCard['MasterReport']['jsConfirmArr']['DeletePreset']?>"))
		window.location = "delete_preset.php?PresetID="+PresetID;
}

function js_Disable_Style(option, fromObj)
{
	$("#"+option).attr("disabled",!$("#"+fromObj).attr("checked"));
}

function js_Enable_Disable_Passing_Percentage()
{
	var Disabled ='';
	var TextID = '';
	$("input.Cust_PassingNumberDetermineByPercentage_Option").each(
		function (){
			Disabled = $(this).attr("checked")?"":"disabled"; 
			TextID = $(this).attr("id")+"_val"; 
			
			$("input#"+TextID).attr("disabled",Disabled);
		}
	);
}

function js_Toggle_Subject_Selection()
{
	var isChecked = $("input#ShowCorrespondingSubjectOnly").attr("checked")
	
	if($(".SelectFrom:checked").val()=='subjectgroup' && isChecked)
	{
		js_Select_All('SubjectIDArr[]');
		refreshSortingTable();
		$("select#SubjectIDArr\\[\\]").attr("disabled","disabled");
	}
	else
	{
		$("select#SubjectIDArr\\[\\]").attr("disabled","");
	}

}

function js_Toggle_Display_Class_Teacher()
{
	if($(".SelectFrom:checked").val()=='class' && $(".DisplayFormat:checked").val()=='ClassSummary')
	{
		$("#ShowClassTeacherTR").show();	
	}
	else
	{
		$("#ShowClassTeacherTR").hide();
	}
	
}

function changedFullMarkPercentageTb(parValue) {
	$('input.fullMarkPercentageTb').each( function () {
		if ($(this).val() == '') {
			$(this).val(parValue);
		}
	});
}

</script>
<style>
ul { list-style-type: none; margin:0; padding-left:20px; }
.sortable li label, .sortable li{ cursor:move; }
.placeholder {border: 1px dotted #888; width:200px; height:14px; }
.common_table_list tr td {padding: 0px 0px;}
</style>
<?

$linterface->LAYOUT_STOP();

?>