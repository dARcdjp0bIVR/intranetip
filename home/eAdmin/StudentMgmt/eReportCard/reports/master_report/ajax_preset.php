<?php
 
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/json.php");

intranet_auth();
intranet_opendb();

if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
{
	include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
	$lreportcard = new libreportcardcustom();
} else {
	$lreportcard = new libreportcard();
}



$RecordType = stripslashes($_REQUEST['RecordType']);

$returnString = '';
if($RecordType == "GetMasterReportPreset")
{
	$PresetValueArr = $lreportcard->Get_Master_Report_Preset_Value($PresetID);
	$json = new JSON_obj();
	$returnString = $json->encode($PresetValueArr);
	
}




echo $returnString;
?>
