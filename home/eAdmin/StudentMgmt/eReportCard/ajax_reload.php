<?
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
intranet_auth();
intranet_opendb();

$lreportcard_ui = new libreportcard_ui();
$Action = stripslashes($_REQUEST['Action']);

if ($Action == 'Class_Selection')
{
	$SelectionID = $_REQUEST['SelectionID'];
	$YearID = $_REQUEST['YearID'];
	$YearClassID = $_REQUEST['YearClassID'];
	$NoFirst = $_REQUEST['NoFirst'];
	$IsAll = $_REQUEST['IsAll'];
	$CheckClassTeacher = $_REQUEST['CheckClassTeacher'];
	$CheckClassTeacher = ($ck_ReportCard_UserType=="ADMIN")? 0 : $CheckClassTeacher;
	$IsMultiple = $_REQUEST['IsMultiple'];
	
	echo $lreportcard_ui->Get_Class_Selection($SelectionID, $YearID, $YearClassID, $Onchange='', $NoFirst, $IsAll, $CheckClassTeacher, $IsMultiple);
}
else if ($Action == 'Form_Selection')
{
	$SelectionID = $_REQUEST['SelectionID'];
	$YearID = $_REQUEST['YearID'];
	$Onchange = stripslashes($_REQUEST['Onchange']);
	$NoFirst = $_REQUEST['NoFirst'];
	$IsAll = $_REQUEST['IsAll'];
	$CheckClassTeacher = $_REQUEST['CheckClassTeacher'];
	$CheckClassTeacher = ($ck_ReportCard_UserType=="ADMIN")? 0 : $CheckClassTeacher;
	$IsMultiple = $_REQUEST['IsMultiple'];
	
	echo $lreportcard_ui->Get_Form_Selection($SelectionID, $YearID, $Onchange, $NoFirst, $IsAll, $hasTemplateOnly=1, $CheckClassTeacher, $IsMultiple);
}
else if ($Action == 'Subject_Selection')
{
	include_once($PATH_WRT_ROOT."includes/subject_class_mapping_ui.php");
	$libSCM_ui = new subject_class_mapping_ui();
	
	$SelectionID = $_REQUEST['SelectionID'];
	$SubjectID = $_REQUEST['SubjectID'];
	$YearTermID = $_REQUEST['YearTermID'];
	$OnChange = $_REQUEST['OnChange'];
	$IsMultiple = $_REQUEST['IsMultiple'];
	$NoFirst = $_REQUEST['NoFirst'];
	
	echo $libSCM_ui->Get_Subject_Selection($SelectionID, $SubjectID, $OnChange, $NoFirst, $firstTitle='', $YearTermID, $OnFocus='', $FilterSubjectWithoutSG=1, $IsMultiple, $IncludeSubjectIDArr='');
}
else if ($Action == 'Subject_Group_Selection')
{
	include_once($PATH_WRT_ROOT."includes/subject_class_mapping_ui.php");
	$libSCM_ui = new subject_class_mapping_ui();
	
	$SubjectIDList = $_REQUEST['SubjectIDList'];
	$SubjectIDArr = explode(',', $SubjectIDList);
	
	$SelectionID = $_REQUEST['SelectionID'];
	$YearTermID = $_REQUEST['YearTermID'];
	$IsMultiple = $_REQUEST['IsMultiple'];
	
	echo $libSCM_ui->Get_Subject_Group_Selection($SubjectIDArr, $SelectionID, $Selected='', $OnChange='', $YearTermID, $IsMultiple, $AcademicYearID='', $noFirst=1, $DisplayTermInfo=0);
}
else if ($Action == 'Report_Selection')
{
	$lreportcard = new libreportcard();
	
	$ClassLevelID =  $_REQUEST['ClassLevelID'];
	$ReportID =  $_REQUEST['ReportID'];
	$Onchange = stripslashes($_REQUEST['Onchange']);
	$Selected = stripslashes($_REQUEST['Selected']);
	$SelectionID = stripslashes($_REQUEST['SelectionID']);
	$ForVerification = $_REQUEST['ForVerification'];
	$ForSubmission = $_REQUEST['ForSubmission'];
	$HideNonGenerated = $_REQUEST['HideNonGenerated'];
	$OtherTags = stripslashes($_REQUEST['OtherTags']);
	$ForGradingSchemeSettings = $_REQUEST['ForGradingSchemeSettings'];
	
	echo $lreportcard->Get_Report_Selection($ClassLevelID, $Selected, $SelectionID, $Onchange ,$ForVerification ,$ForSubmission,$HideNonGenerated,$OtherTags,$ForGradingSchemeSettings,$ReportID);
}
else if ($Action == 'ReportColumn_Selection')
{
	$ReportID =  $_REQUEST['ReportID'];
	$Selected = stripslashes($_REQUEST['Selected']);
	$SelectionID = stripslashes($_REQUEST['SelectionID']);
	
	echo $lreportcard_ui->Get_ReportColumn_Selection($ReportID, $SelectionID, $Selected);
}

intranet_closedb();
?>