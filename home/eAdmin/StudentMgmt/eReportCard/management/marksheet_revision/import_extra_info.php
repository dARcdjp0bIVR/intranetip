<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping_ui.php");

include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if ($plugin['ReportCard2008'])
{
	include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
	if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
	{
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
		$lreportcard = new libreportcardcustom();
	} else {
		$lreportcard = new libreportcard();
	}
	
	$CurrentPage = "Management_MarkSheetRevision";
	$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
	
	if ($lreportcard->hasAccessRight()) {
		
		if ($eRCTemplateSetting['Marksheet']['ManualInputGrade']) {
        	$column_data = $lreportcard->returnReportTemplateColumnData($ReportID);
			$dataTitle = $column_data[0]['ColumnTitle'].' '.$eReportCard['Grade'];
		}
		else {
			$dataTitle = $eReportCard['ExtraInfoLabel'];
		}
		
		// Class
		$ClassArr = $lreportcard->GET_CLASSES_BY_FORM($ClassLevelID);
		for($i=0 ; $i<count($ClassArr) ;$i++) {
			if($ClassArr[$i][0] == $ClassID)
				$ClassName = $ClassArr[$i][1];
		}
		
		// Subject
		$SubjectName = $lreportcard->GET_SUBJECT_NAME($SubjectID);
		
		// Period (Term or Whole Year)
		$ReportInfo = $lreportcard->GET_REPORT_TYPES($ClassLevelID, $ReportID);
		$PeriodName = $ReportInfo['SemesterTitle'];
		
		############## Interface Start ##############
		$linterface = new interface_html();
		
		# tag information
		$TAGS_OBJ[] = array($eReportCard['Management_MarksheetRevision'], "", 0);
		# page navigation (leave the array empty if no need)
		if ($type=="AdjustPosition")
		{
			$PAGE_NAVIGATION[] = array($button_import." ".$eReportCard['AdjustPositionLabal']);
		}
		else
		{
			$PAGE_NAVIGATION[] = array($button_import." ".$dataTitle);
		}

		$linterface->LAYOUT_START();
		
		if ($SubjectGroupID != '')
		{
			$obj_SubjectGroup = new subject_term_class($SubjectGroupID);
			$SubjectGroupName = $obj_SubjectGroup->Get_Class_Title();
			$TitleClassSubjectGroup = $Lang['SysMgr']['SubjectClassMapping']['SubjectGroup'];
			$TitleDisplay = $SubjectGroupName;
		}
		else if ($ClassID != '')
		{
			$TitleClassSubjectGroup = $eReportCard['Class'];
			$TitleDisplay = $ClassName;
		}
		
		
		if ($Result == 'WrongEffortValue') {
			$SysMsg = $linterface->GET_SYS_MSG('', $eReportCard['Template']['WrongEffortValue']);
		}
		else {
			$SysMsg = $linterface->GET_SYS_MSG($Result);
		}
		
		
		$parameters = "ClassID=$ClassID&SubjectID=$SubjectID&ReportID=$ReportID&ClassLevelID=$ClassLevelID&type=$type&SubjectGroupID=$SubjectGroupID";
?>
<script type="text/JavaScript" language="JavaScript">
function trim(str)
{
	return str.replace(/^\s+|\s+$/g,"");
}

function checkForm() {
	obj = document.form1;
	if (trim(obj.elements["userfile"].value) == "") {
		 alert('<?=$eReportCard['AlertSelectFile']?>');
		 obj.elements["userfile"].focus();
		 return false;
	}
	
	obj.submit();
}
</script>
<form name="form1" action="import_extra_info_update.php" method="POST" onsubmit="return checkForm();" enctype="multipart/form-data">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
		<td align="right"> <?=$SysMsg?></td>
	</tr>
	<tr><td>&nbsp;</td><td>&nbsp;</td></tr>
<table>
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>
			<table class="form_table_v30">
				<tr>
					<td class="field_title">
						<?= $eReportCard['Settings_ReportCardTemplates'] ?>
					</td>
					<td><?=$PeriodName?></td>
				</tr>
				<tr>
					<td class="field_title">
						<?= $TitleClassSubjectGroup ?>
					</td>
					<td><?=$TitleDisplay?></td>
				</tr>
				<tr>
					<td class="field_title">
						<?= $eReportCard['Subject'] ?>
					</td>
					<td><?=$SubjectName?></td>
				</tr>
				<tr>
					<td class="field_title">
						<?= $i_select_file ?>
					</td>
					<td>
						<input class="file" type='file' name='userfile'>
					</td>
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" colspan='2' class="tabletext">
						<a class="tablelink" href="export_extra_info.php?<?=$parameters?>&isTemplate=1" target="_blank">
							<img src='<?=$PATH_WRT_ROOT?>/images/<?=$LAYOUT_SKIN?>/icon_files/xls.gif' border='0' align='absmiddle' hspace='3'> 
							<?=$eReportCard['DownloadCSVFile']?>
						</a>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr>
					<td class="dotline">
						<img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1">
					</td>
				</tr>
				<tr>
					<td align="center">
						<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "")?>&nbsp;
						<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location = 'extra_info.php?".$parameters."'")?>&nbsp;
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<input type="hidden" name="ClassLevelID" id="ClassLevelID" value="<?=$ClassLevelID?>"/>
<input type="hidden" name="ReportID" id="ReportID" value="<?=$ReportID?>"/>
<input type="hidden" name="SubjectID" id="SubjectID" value="<?=$SubjectID?>"/>
<input type="hidden" name="SubjectGroupID" id="SubjectGroupID" value="<?=$SubjectGroupID?>"/>
<input type="hidden" name="ClassID" id="ClassID" value="<?=$ClassID?>"/>
<input type="hidden" name="type" id="type" value="<?=$type?>"/>
</form>
<?
	print $linterface->FOCUS_ON_LOAD("form1.userfile");
  	$linterface->LAYOUT_STOP();
  } else {
?>
You have no priviledge to access this page.
<?
	}
} else {
?>
You have no priviledge to access this page.
<?
}
?>
