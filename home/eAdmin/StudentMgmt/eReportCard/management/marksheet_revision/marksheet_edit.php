<?php
// Using: Bill
/*
 * 20190624 Bill [2019-0618-1504-34066]
 *      - allow access via print mode
 * 20190604 Bill [2019-0123-1731-14066]
 *      - always display marks according to decimal places settings  ($eRCTemplateSetting['Marksheet']['DisplayRoundMarks'])
 * 20190502 Bill
 *      - prevent SQL Injection + Cross-site Scripting 
 * 20181107 Bill [2018-1105-1216-00206]
 *      - General deploy function - Paste from Excel (Firefox & Chrome)
 * 20180723 Bill [2018-0718-0947-53066]
 *      - support copy grade from excel directly
 * 20180208 Bill [2018-0207-1120-00054]
 * 		- Support Paste from Excel (Firefox & Chrome)	($eRCTemplateSetting['Marksheet']['ApplyNewPasteMedthod'])
 * 20170629 Bill [2017-0626-1124-47206]
 * 		- display correct Term Subject Mark when Consolidate Report Calculation - Vertical > Horizontal
 * 20170519 Bill
 * 		- Allow View Group to access (view only)
 * 20170508 Bill [2017-0508-1128-10164]
 * 		- display correct Year Report Column Score when $eRCTemplateSetting['Settings']['TemplateSettings_ConsolidatedReportWithTermReportAssessment'] = true
 * 20170331 Bill [2017-0109-1818-40164]
 * 		- Support Term Result Preview
 * 20170330 Bill [2016-1115-1700-32235]
 * 		- Class Teacher (not subject group teacher) cannot edit marksheet ($eRCTemplateSetting['Marksheet']['OnlyAllowSubjectTeacherToEdit'])
 * 20170118 Bill [DM#3151]
 * 		- for correct display of special case "+" - for parent subjects calculated by components  
 * 20160426 Bill [2015-1008-1356-44164] 
 * 		- only display students in Subject Group - Whole Year Report and Overall column due to zero weight
 * 20160112 Bill [2015-0924-1821-07164] [ip.2.5.7.1.1]:
 * 		- show Sub-MS button for Wah Yan, Kowloon (F4-6) even with empty/zero weight column	($eRCTemplateSetting['Marksheet']['ManualInputGrade'])
 * 20151223 Ivan [X86217] [ip.2.5.7.1.1]:
 * 		- added $eRCTemplateSetting['Management']['Schedule']['SpecificMarksheetSubmissionPeriod_subjectBased'] logic
 * 20151130 Bill [2015-1130-0918-43073]
 * 		- hide Sub-MS button if column with empty/zero weight
 * 20141103 Siuwan [2014-0912-1716-13164]: 
 *		- pt2: diff style for assessments in marksheet 
 * 20140801 Ryan  :
 * 		- Merged SIS CUST
 * 20120403 Marcus:
 * 		- Allow teacher to view marks even after submission period
 * 		- 2012-0208-1800-43132 - 沙田循���衛���中學 - Edit mark after submit the marksheet 
 * Modification Log:
 * 20110829 (Ivan)
 * 	- added estimated mark logic
 */

$PageRight = "TEACHER";

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
//include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping_ui.php");
//include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

$isPrintMode = false;
if ($plugin['ReportCard2008'])
{
    if (is_file($intranet_root."/includes/eRCConfig.php")) {
        include_once ($intranet_root."/includes/eRCConfig.php");
    }
    
    // [2019-0618-1504-34066] Check if valid access in Print Mode
    $isPrintMode = isset($_POST['isPrintMode'])? $_POST['isPrintMode'] : $_GET['isPrintMode'];
    if($eRCTemplateSetting['Marksheet']['PrintPage'] && $isPrintMode && performSimpleTokenByDateTimeChecking($_POST['token'], $_SERVER['REQUEST_URI'].$_POST['UserID']))
    {
        @session_start();
        $_SESSION['UserID'] = $_POST['UserID'];
        
        $specialPrintHandling = true;
    }
}

intranet_auth();
intranet_opendb();

if ($plugin['ReportCard2008'])
{
	include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
	if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") {
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
	}
	include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
	
	/* Temp Library*/
	include_once($PATH_WRT_ROOT."includes/libreportcard2008j.php");
	include_once($PATH_WRT_ROOT."includes/libreportcard2008_schedule.php");
	
	$lreportcard = new libreportcard2008j();
	//$lreportcard = new libreportcard();
	$lreportcard_ui = new libreportcard_ui();
	$lreportcard_schedule = new libreportcard_schedule();
	
	$CurrentPage = "Management_MarkSheetRevision";
    // $isPrintMode = (isset($_POST['isPrintMode']))? $_POST['isPrintMode'] : $_GET['isPrintMode'];
	
	if (($isPrintMode && $specialPrintHandling) || $lreportcard->hasAccessRight())
    {
        ### Handle SQL Injection + XSS [START]
        $ClassLevelID = IntegerSafe($ClassLevelID);
        $ReportID = IntegerSafe($ReportID);
        $SubjectID = IntegerSafe($SubjectID);
        $ClassID = IntegerSafe($ClassID);
        $SubjectGroupID = IntegerSafe($SubjectGroupID);
        $ParentSubjectID = IntegerSafe($ParentSubjectID);
        $isProgress = IntegerSafe($isProgress);
        $isProgressSG = IntegerSafe($isProgressSG);
        ### Handle SQL Injection + XSS [END]
        
        $linterface = new interface_html();
        
		############################################################################################################
					
		# Customized Settings - disable percentage / weight label in Column Title
		$isSetLabel = (getenv("SERVER_NAME") == "project6.broadlearning.com" || getenv("SERVER_NAME") == "sis-trial.broadlearning.com") ? 0 : 1;
		
		/*
		# Check whether all column weights are zero or not corresponding to each subject of a report
		$isAllColumnWeightZero = $lreportcard->IS_ALL_REPORT_COLUMN_WEIGHTS_ZERO($ReportID, $SubjectID);
		if($isAllColumnWeightZero){
			header("Location: marksheet_revision.php?ClassLevelID=$ClassLevelID&ReportID=$ReportID&SubjectID=$SubjectID");
			exit();
		}
		*/
		
		# updated on 12 Jan 2009 by Ivan
		# Disable preview if calculation type is verticle->horizontal
		$ReportSetting = $lreportcard->returnReportTemplateBasicInfo($ReportID);
		$SemID = $ReportSetting['Semester'];
		$ReportSemSeq = $lreportcard->Get_Semester_Seq_Number($SemID);
		$ReportTypeTW = $SemID == "F" ? "W" : "T";
		if ($ReportTypeTW=="T") {
			$thisCalculationType = $TermCalculationType;
		}
		else {
			$thisCalculationType = $FullYearCalculationType;
		}
		
		# Get MarkSheet Info
		// Tags of MarkSheet(0: Raw Mark, 1: Weighted Mark, 2: Converted Grade
		$TagsType = (isset($TagsType) && $TagsType != "") ? $TagsType : 0;

		// Period (Term x, Whole Year, etc)
		$PeriodArr = $lreportcard->GET_REPORT_TYPES($ClassLevelID, $ReportID);
		$PeriodName = (count($PeriodArr) > 0) ? $PeriodArr['SemesterTitle'] : '';
		
		# Load Settings - Calculation
		// Get the Calculation Method of Report Type - 1: Right-Down,  2: Down-Right
		$SettingCategory = 'Calculation';
		$categorySettings = $lreportcard->LOAD_SETTING($SettingCategory);
		$TermCalculationType = $categorySettings['OrderTerm'];
		$FullYearCalculationType = $categorySettings['OrderFullYear'];
		
		# Load Settings - Storage&Display
		$SettingStorDisp = 'Storage&Display';
		$storDispSettings = $lreportcard->LOAD_SETTING($SettingStorDisp);
		
		// Get Absent&Exempt Settings
		$SettingAbsentExempt = 'Absent&Exempt';
		$absentExemptSettings = $lreportcard->LOAD_SETTING($SettingAbsentExempt);
		
		if(count($PeriodArr) > 0 && $PeriodArr['Semester'] == "F") {
			$usePercentage = ($FullYearCalculationType == 1) ? 1 : 0;
		}
		else { 
			$usePercentage = ($TermCalculationType == 1) ? 1 : 0;
		}
		
		// Class
		$ClassArr = $lreportcard->GET_CLASSES_BY_FORM($ClassLevelID);
		for($i=0; $i<count($ClassArr); $i++)
		{
			if($ClassArr[$i][0] == $ClassID) {
				$ClassName = $ClassArr[$i][1];
			}
		}
		
		// Subject
		$SubjectName = $lreportcard->GET_SUBJECT_NAME($SubjectID);
		
		# Get component subject(s) if any
		$isEdit = 1;					// default "1" as the marksheet is editable
		$CmpSubjectArr = array();
		$isCalculateByCmpSub = 0;		// set to "1" when one ScaleInput of component subjects is Mark(M)
		$isCmpSubject = $lreportcard->CHECK_SUBJECT_IS_COMPONENT_SUBJECT($SubjectID);
		if(!$isCmpSubject)
		{
			$CmpSubjectIDArr = array();
			$CmpSubjectArr = $lreportcard->GET_COMPONENT_SUBJECT($SubjectID, $ClassLevelID);
			if(!empty($CmpSubjectArr))
			{
				for($i=0; $i<count($CmpSubjectArr); $i++)
				{
					$CmpSubjectIDArr[] = $CmpSubjectArr[$i]['SubjectID'];
					
					$CmpSubjectFormGradingArr = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $CmpSubjectArr[$i]['SubjectID'], 0, 0, $ReportID);
					if(count($CmpSubjectFormGradingArr) > 0 && $CmpSubjectFormGradingArr['ScaleInput'] == "M") {
						$isCalculateByCmpSub = 1;
					}
				}
			}
			
			// Check whether the marksheet is editable or not 
			$isEdit = $lreportcard->CHECK_MARKSHEET_STATUS($SubjectID, $ClassLevelID, $ReportID, $CmpSubjectIDArr);
		}
		
		# Get Status of Completed Action
		# $PeriodAction = "Submission" or "Verification"
		$PeriodAction = "Submission";
		$PeriodType = $lreportcard->COMPARE_REPORT_PERIOD($ReportID, $PeriodAction);
		if($PeriodType != 1 && $ck_ReportCard_UserType != "ADMIN") {
			$ViewOnly = true;
		}
		
		// Check Special Submission Period (Parent and Component)
		if(!$isCmpSubject) {
			$SpecialPeriodType = $lreportcard_schedule->compareSpecialMarksheetSubmissionPeriodForTeacher($ReportID, $_SESSION['UserID'], $SubjectID, $SubjectGroupID);
		}
		else {
			$SpecialPeriodType = $lreportcard_schedule->compareSpecialMarksheetSubmissionPeriodForTeacher($ReportID, $_SESSION['UserID'], $ParentSubjectID, $SubjectGroupID);
		}
		if ($SpecialPeriodType == 1) {
			$ViewOnly = false;
		}
		
		// [2016-1115-1700-32235] Check Current Subject Group Teacher
		if($eRCTemplateSetting['Marksheet']['OnlyAllowSubjectTeacherToEdit'] && $ck_ReportCard_UserType != "ADMIN" && $ck_ReportCard_UserType != "VIEW_GROUP")
		{
			if(!$isCmpSubject) {
				$isSubjectGroupTeacher = $lreportcard->checkSubjectTeacher($_SESSION['UserID'], $SubjectID, "", $SubjectGroupID);
			}
			else {
				$isSubjectGroupTeacher = $lreportcard->checkSubjectTeacher($_SESSION['UserID'], $ParentSubjectID, "", $SubjectGroupID);
			}
			
			// Only Subject Group Teacher can edit
			$isSubjectGroupTeacher = !empty($isSubjectGroupTeacher);
			if(!$isSubjectGroupTeacher) {
				$ViewOnly = true;
			}
		}
		
		// View Group - View Only
		if($ck_ReportCard_UserType == "VIEW_GROUP") {
			$ViewOnly = true;
		}
		
		# Form Checking for Wah Wan, Kowoloon
		$CurrentFormNumber = $lreportcard->GET_FORM_NUMBER($ClassLevelID);
	    
	    # [2017-0109-1818-40164] Check if need to display result preview
	    $GetPreviousTermResult = false;
	    $ShowAllTermResultSummary = false;
	    if($eRCTemplateSetting['Marksheet']['CanPreviewOverallTermResult'] && $ReportType != "F" && $TagsType==0) {
		    $GetPreviousTermResult = $ReportSemSeq > 1 && $TermCalculationType==1;
		    $ShowAllTermResultSummary = $GetPreviousTermResult && $ReportSemSeq==3 && $FullYearCalculationType==1;
	    }
	    
		// Initialization of Grading Scheme
		$SchemeID = '';
		$ScaleInput = "M";		// M: Mark, G: Grade
		$ScaleDisplay = "M";	// M: Mark, G: Grade
		$SchemeType = "H";		// H: Honor Based, PF: PassFail
		$SchemeInfo = array();
		$SchemeMainInfo = array();
		$SchemeMarkInfo = array();
		$SubjectFormGradingArr = array();
		$possibleSpeicalCaseSet1 = array();
		$possibleSpeicalCaseSet2 = array();
		
		# Check if no any scheme is assigned to this Form(ClassLevelID) of this subject, redirect
		$SubjectFormGradingArr = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID,0,0,$ReportID);
		if(count($SubjectFormGradingArr) > 0) {
			if($SubjectFormGradingArr['SchemeID'] == "" || $SubjectFormGradingArr['SchemeID'] == null) {
				header("Location: marksheet_revision.php?ClassLevelID=$ClassLevelID&ReportID=$ReportID&SubjectID=$SubjectID&msg=1");
			}
		}
		
		# Get Grading Scheme Info of that subject
		if(count($SubjectFormGradingArr) > 0)
		{
			$SchemeID = $SubjectFormGradingArr['SchemeID'];
			$ScaleInput = $SubjectFormGradingArr['ScaleInput'];
			$ScaleDisplay = $SubjectFormGradingArr['ScaleDisplay'];
			
			$SchemeInfo = $lreportcard->GET_GRADING_SCHEME_INFO($SchemeID);
			$SchemeMainInfo = $SchemeInfo[0];
			$SchemeMarkInfo = $SchemeInfo[1];
			$SchemeType = $SchemeMainInfo['SchemeType'];
			
			$possibleSpeicalCaseSet1 = $lreportcard->GET_POSSIBLE_MARKSHEET_SPECIAL_CASE_SET1();
			
			// Prepare Selection Box using Grade as Input
			if($SchemeType == "H" && $ScaleInput == "G")
			{
				$InputTmpOption = array();
				$InputGradeOption = array();
				$SchemeGrade = $lreportcard->GET_GRADE_FROM_GRADING_SCHEME_RANGE($SchemeID);
				$InputGradeOption[0] = array('', '--');
				if(!empty($SchemeGrade)) {
					foreach($SchemeGrade as $GradeRangeID => $grade) {
						$InputGradeOption[] = array($GradeRangeID, $grade);
					}
				}
				
				for($k=0 ; $k<count($possibleSpeicalCaseSet1) ; $k++){
					//$InputGradeOption[] = array($possibleSpeicalCaseSet1[$k], $lreportcard->GET_MARKSHEET_SPECIAL_CASE_SET1_STRING($possibleSpeicalCaseSet1[$k]));
					$InputGradeOption[] = array($possibleSpeicalCaseSet1[$k], $possibleSpeicalCaseSet1[$k]);
				}
			}
			// Prepare Selection Box using Pass / Fail as Input
			else if($SchemeType == "PF")
			{
				$SchemePassFail = array();
				$SchemePassFail['P'] = $SchemeMainInfo['Pass'];
				$SchemePassFail['F'] = $SchemeMainInfo['Fail'];
			
				$InputPFOption = array();
				$InputPFOption[0] = array('', '--');
				$InputPFOption[1] = array('P', $SchemeMainInfo['Pass']);
				$InputPFOption[2] = array('F', $SchemeMainInfo['Fail']);
				
				$possibleSpeicalCaseSet2 = $lreportcard->GET_POSSIBLE_MARKSHEET_SPECIAL_CASE_SET2();
				for($k=0; $k<count($possibleSpeicalCaseSet2); $k++) {
					//$InputPFOption[] = array($possibleSpeicalCaseSet2[$k], $lreportcard->GET_MARKSHEET_SPECIAL_CASE_SET2_STRING($possibleSpeicalCaseSet2[$k]));
					$InputPFOption[] = array($possibleSpeicalCaseSet2[$k], $possibleSpeicalCaseSet2[$k]);
				}
			}
		}
		
		$InputGradeOptionAsso = array();
		$numOfOption = count($InputGradeOption);
		for ($i=0; $i<$numOfOption; $i++)
		{
			$thisIndex = $InputGradeOption[$i][0];
			$thisGrade = $InputGradeOption[$i][1];
			$InputGradeOptionAsso[$thisGrade] = $thisIndex;
		}
		
		// Mark Type Tags	
		$params = "ClassLevelID=$ClassLevelID&ReportID=$ReportID&SubjectID=$SubjectID&ClassID=$ClassID&SubjectGroupID=$SubjectGroupID&isProgress=$isProgress&isProgressSG=$isProgressSG";
		$params .= (isset($ParentSubjectID) && $ParentSubjectID != "") ? "&ParentSubjectID=".$ParentSubjectID : "";
		$display_tagstype = '';
		$display_tagstype .= '
		  <div class="shadetabs">
            <ul>';
        //$display_tagstype .= '<li '.(($TagsType == 0) ? 'class="selected"' : '').'><a href="marksheet_edit.php?'.$params.'&TagsType=0">'.$eReportCard['InputMarks'].'</a></li>';
        //$display_tagstype .= ($SchemeType != "PF" && $ScaleDisplay == "M" && $ScaleInput != "G") ? '<li '.(($TagsType == 1) ? 'class="selected"' : '').'><a href="marksheet_edit.php?'.$params.'&TagsType=1">'.$eReportCard['PreviewMarks'].'</a></li>' : '';
        //$display_tagstype .= ($SchemeType != "PF" && $ScaleDisplay == "G") ? '<li '.(($TagsType == 2) ? 'class="selected"' : '').'><a href="marksheet_edit.php?'.$params.'&TagsType=2">'.$eReportCard['PreviewGrades'].'</a></li>' : '';
        $display_tagstype .= '<li '.(($TagsType == 0) ? 'class="selected"' : '').'><a href="javascript:jCHECK_MARKSHEET_STATUS(0)">'.$eReportCard['InputMarks'].'</a></li>';
        //if ($ReportCardCustomSchoolName == "kadoorie_wk")
        //{
	    //   $display_tagstype .= ($SchemeType != "PF" && $ScaleDisplay == "M" && $ScaleInput != "G") ? '<li '.(($TagsType == 1) ? 'class="selected"' : '').'><a href="javascript:jCHECK_MARKSHEET_STATUS(1)">'.$eReportCard['PreviewMarks'].'</a></li>' : '';
	    //   $display_tagstype .= ($SchemeType != "PF" && $ScaleDisplay == "G") ? '<li '.(($TagsType == 2) ? 'class="selected"' : '').'><a href="javascript:jCHECK_MARKSHEET_STATUS(2)">'.$eReportCard['PreviewGrades'].'</a></li>' : '';
	    //}
	    if ($eRCTemplateSetting['Marksheet']['ShowPreviewMarkTag']) {
	    	$display_tagstype .= ($SchemeType != "PF" && $ScaleDisplay == "M" && $ScaleInput != "G") ? '<li '.(($TagsType == 1) ? 'class="selected"' : '').'><a href="javascript:jCHECK_MARKSHEET_STATUS(1)">'.$eReportCard['PreviewMarks'].'</a></li>' : '';
	    }
	    
	    if ($eRCTemplateSetting['Marksheet']['ShowPreviewGradeTag']) {
	    	$display_tagstype .= ($SchemeType != "PF" && $ScaleDisplay == "G") ? '<li '.(($TagsType == 2) ? 'class="selected"' : '').'><a href="javascript:jCHECK_MARKSHEET_STATUS(2)">'.$eReportCard['PreviewGrades'].'</a></li>' : '';
	    }
	    
        $display_tagstype .= '
            </ul>
          </div>';
		
		# Get Data Either Assessment Column(s) in Term Or Term Column(s) in Wholely Report	
		// Loading Report Template Data
		$basic_data = $lreportcard->returnReportTemplateBasicInfo($ReportID);			// Basic  Data
		$column_data = $lreportcard->returnReportTemplateColumnData($ReportID); 		// Column Data
		foreach((array)$column_data as $thiscolumndata) {
			$columnIDDataMap[$thiscolumndata['ReportColumnID']] = $thiscolumndata;
		}
		$ColumnSize = count($column_data);
		$ColumnTitleAry = $lreportcard->returnReportColoumnTitle($ReportID);
		
		$ReportTitle = $basic_data['ReportTitle'];
		$ReportType = $basic_data['Semester'];
		$isPercentage = $basic_data['PercentageOnColumnWeight'];
		
		# Get Existing Report Calculation Method
		$CalculationType = ($ReportType == "F") ? $FullYearCalculationType : $TermCalculationType;
		
		# [2017-0626-1124-47206] Check Consolidate Report Calculation - Vertical > Horizontal
		$ApplySubjectHorizontalCalculation = $ReportType == "F" && $TermCalculationType == 2;
		
		# Get Report Column Weight
		$WeightAry = array();
		if($CalculationType == 2){
			if(count($column_data) > 0){
		    	for($i=0; $i<count($column_data); $i++){
					//$OtherCondition = "SubjectID IS NULL AND ReportColumnID = ".$column_data[$i]['ReportColumnID'];
					$OtherCondition = "SubjectID = '$SubjectID' AND ReportColumnID = ".$column_data[$i]['ReportColumnID'];
					$weight_data = $lreportcard->returnReportTemplateSubjectWeightData($ReportID, $OtherCondition); 
					
					$WeightAry[$weight_data[0]['ReportColumnID']][$SubjectID] = $weight_data[0]['Weight'];
				}
			}
		}
		else {
			$weight_data = $lreportcard->returnReportTemplateSubjectWeightData($ReportID);
			for($i=0; $i<count($weight_data); $i++) {
				$WeightAry[$weight_data[$i]['ReportColumnID']][$weight_data[$i]['SubjectID']] = $weight_data[$i]['Weight'];
			}
		}
		
		### Check if all Column Weight Zero
		$isAllColumnWeightZero = true;
		foreach((array)$WeightAry as $thisReportColumnID => $thisArr)
		{
			if ($thisArr[$SubjectID] != 0 && $thisReportColumnID != '')
			{
				$isAllColumnWeightZero = false;
			}
		}
		
		if ($isAllColumnWeightZero == true && $SchemeType == "H" && ($ScaleInput == "M" || $ScaleInput == "G")) {
			$displayOverallColumnDueToAllZeroWeight = true;
		}
		else {
			$displayOverallColumnDueToAllZeroWeight = false;
		}
		$IsCmpAllZeroWeightArr = $lreportcard->IS_ALL_CMP_SUBJECT_ZERO_WEIGHT($ReportID, $SubjectID);
		
		// Student Info Array
		if ($SubjectGroupID != '')
		{
			$StudentArr = $lreportcard->GET_STUDENT_BY_SUBJECT_GROUP($SubjectGroupID, $ClassLevelID, '', 1, 1);
			$showClassName = true;
			
			$obj_SubjectGroup = new subject_term_class($SubjectGroupID, $GetTeacherList=true);
			$SubjectGroupName = $obj_SubjectGroup->Get_Class_Title();
			
			$SubjectOrGroupTitle = $Lang['SysMgr']['SubjectClassMapping']['SubjectGroup'];
			$SubjectOrGroupNameDisplay = $SubjectGroupName;
			
			$TeacherName = implode('<br />', Get_Array_By_Key((array)$obj_SubjectGroup->ClassTeacherList, 'TeacherName'));
			$TeacherName = ($TeacherName=='')? $Lang['General']['EmptySymbol'] : $TeacherName;
			$SubjectTeacherTr = '';
			$SubjectTeacherTr .= '<tr>'."\r\n";
				$SubjectTeacherTr .= '<td class="field_title">'.$Lang['General']['Teacher'].'</td>'."\r\n";
				$SubjectTeacherTr .= '<td>'.$TeacherName.'</td>'."\r\n";
			$SubjectTeacherTr .= '</tr>'."\r\n";
		}
		else
		{
//			if ($lreportcard->IS_ADMIN_USER($_SESSION['UserID']) == true || $lreportcard->Is_Class_Teacher($_SESSION['UserID'], $ClassID) == true)
//			{
//				# Admin / Class Teacher => Show all students
//				$TaughtStudentList = '';
//			}
//			else
//			{
//				# Subject Teacher => Show taught students only
//				$TargetSubjectID = ($ParentSubjectID != '')? $ParentSubjectID : $SubjectID;
//				$TaughtStudentArr = $lreportcard->Get_Student_List_Taught_In_This_Consolidated_Report($ReportID, $_SESSION['UserID'], $ClassID, $TargetSubjectID);
//				$TaughtStudentList = '';
//				if (count($TaughtStudentArr > 0))
//					$TaughtStudentList = implode(',', $TaughtStudentArr);
//			}
//			$StudentArr = $lreportcard->GET_STUDENT_BY_CLASS($ClassID, $TaughtStudentList, $isShowBothLangs=1, $withClassNumber=0, $isShowStyle=1, $ReturnAsso=0);

			//2014-0224-1422-33184
			$StudentArr = $lreportcard->Get_Marksheet_Accessible_Student_List_By_Subject_And_Class($ReportID, $_SESSION['UserID'], $ClassID, $ParentSubjectID, $SubjectID);
			
			// [2015-1008-1356-44164] only display students in Subject Group - Whole Year Report and Overall column due to zero weight
			if($eRCTemplateSetting['DoNotDisplayStudentNotInSubjectGroupForYearMS'] && $TagsType == 0 && $ReportType == "F" && $displayOverallColumnDueToAllZeroWeight)
			{
				// StudentID of all students in Subject and Class
				$allStudentIDArr = Get_Array_By_Key($StudentArr, "UserID");
				
				// Students in Subject Group
				$StudentArr = $lreportcard->Get_Marksheet_Accessible_Student_List_By_Subject_And_Class($ReportID, $_SESSION['UserID'], $ClassID, $ParentSubjectID, $SubjectID, true, true);
				$StudentIDListArr = Get_Array_By_Key($StudentArr, "UserID");
				
				// StudentID of students not in Subject Group
				$excludedStudentArr = array_diff($allStudentIDArr, $StudentIDListArr);
				$excludedStudentArr = array_values($excludedStudentArr);
				$excludedStudentSize = count($excludedStudentArr);
			}
			
			$showClassName = false;
			
			$SubjectOrGroupTitle = $eReportCard['Class'];
			$SubjectOrGroupNameDisplay = $ClassName;
			
			$SkipSubjectGroupCheckingForGetMarkSheet = 1;
		}
		$StudentSize = count($StudentArr);
		
		$StudentIDArr = array();
		if(count($StudentArr) > 0){
			for($i=0; $i<count($StudentArr); $i++) {
				$StudentIDArr[$i] = $StudentArr[$i]['UserID'];
			}
		}
		
		# Get MarkSheet OVERALL Score Info for the cases when it is (Term Report OR Whole Year Report) AND 
		# when the scheme type is Honor-Based (H) AND its scaleinput is Grade(G) OR
		# when the scheme type is PassFail-Based (PF)
		$MarksheetOverallScoreInfoArr = array();
		if(($TagsType == 0 || $TagsType == 2) && (($SchemeType == "H" && $ScaleInput == "G") || $SchemeType == "PF" || $displayOverallColumnDueToAllZeroWeight) ){
			$MarksheetOverallScoreInfoArr = $lreportcard->GET_MARKSHEET_OVERALL_SCORE($StudentIDArr, $SubjectID, $ReportID, $SkipSubjectGroupCheckingForGetMarkSheet);
		}
		
		# [2017-0109-1818-40164] Get Previous Term and Whole Year Report 
		if($GetPreviousTermResult)
		{
			// Get Semesters
			$allSemesterAry = getSemesters($lreportcard->schoolYearID, 1);
			
			// Related Term Report
			$relatedTermReports = $lreportcard->Get_Report_List($ClassLevelID, "T");
			$relatedTermReports = BuildMultiKeyAssoc($relatedTermReports, array("isMainReport", "Semester"));
			$relatedTermReports = $relatedTermReports[1];
			
			// Related Whole Year Report
			if($ShowAllTermResultSummary)
			{
				$YearReportInfo = $lreportcard->Get_Report_List($ClassLevelID, "W");
				$YearReportInfo = $YearReportInfo[0];
				$YearReportReportID = $YearReportInfo["ReportID"];
				
				// Report Info
				if($YearReportReportID > 0)
				{
					$YearReportColumnData = $lreportcard->returnReportTemplateColumnData($YearReportReportID);
					$YearReportColumnTermMapping = BuildMultiKeyAssoc((array)$YearReportColumnData, "ReportColumnID");
					$YearReportColumnWeightData = $lreportcard->returnReportTemplateSubjectWeightData($YearReportReportID);
					
					// loop Subject Report Column
					$YearReportSubjectColumnSize = count((array)$YearReportColumnWeightData);
					for($j=0; $j<$YearReportSubjectColumnSize; $j++)
					{
						$thisYearReportSubjectColumn = $YearReportColumnWeightData[$j];
						if($thisYearReportSubjectColumn["SubjectID"] == $SubjectID && $thisYearReportSubjectColumn["ReportColumnID"] != "")
						{
							// Coloumn ID
							$thisYearReportColumnID = $thisYearReportSubjectColumn["ReportColumnID"];
							$thisYearTermReportColumnID = $YearReportColumnTermMapping[$thisYearReportColumnID]["TermReportColumnID"];
							
							// Update Column and Total Weight
							$YearReportWeightInfo[$thisYearTermReportColumnID]["Weight"] = $thisYearReportSubjectColumn["Weight"];
							$YearReportWeightInfo[0]["TotalWeight"] += $thisYearReportSubjectColumn["Weight"];
						}
					}
				}
			}
		}
		
		####################################################################################################
		# General Approach dealing with Search Term Result From Whole Year Report not From Term Report
		# Can deal with Customization of SIS dealing with secondary template 
		$isAllWholeReport = 0;
		if($ReportType == "F")
		{
			$ReportTypeArr = array();
			$ReportTypeArr = $lreportcard->GET_REPORT_TYPES($ClassLevelID);
			if(count($ReportTypeArr) > 0)
			{
				$flag = 0;
				for($j=0 ; $j<count($ReportTypeArr) ; $j++){
					if($ReportTypeArr[$j]['ReportID'] != $ReportID){
						if($ReportTypeArr[$j]['Semester'] != "F")	$flag = 1;
					}
				}
				if($flag == 0) $isAllWholeReport = 1;
			}
		}
		
		# Get the Corresponding Whole Year ReportID from 
		# the ReportColumnID of the Current Whole Year Report 
		$WholeYearColumnHasTemplateArr = array();
		if($isAllWholeReport == 1)
		{
			if($ReportType == "F" && $ColumnSize > 0)
			{
				for($j=0; $j<$ColumnSize; $j++)
				{
					$ReportColumnID = $column_data[$j]['ReportColumnID'];
					# Return Whole Year Report Template ID - ReportID 
					$WholeYearColumnHasTemplateArr[$ReportColumnID] = $lreportcard->CHECK_WHOLE_YEAR_REPORT_TEMPLATE_FROM_COLUMN($ReportID, $column_data[$j]['SemesterNum'], $ClassLevelID);
					
					# Check Whether the Return WholeYear ReportID is not greater the Current ReportID
					# if User creates All Reports sequently
					if($WholeYearColumnHasTemplateArr[$ReportColumnID] != false) {
						if($WholeYearColumnHasTemplateArr[$ReportColumnID] > $ReportID) {
							$WholeYearColumnHasTemplateArr[$ReportColumnID] = false;
						}
					}
				}
			}
		}
		####################################################################################################
		
		# Get MarkSheet Score Info / ParentSubMarksheetScoreInfoArr	
		$MarksheetScoreInfoArr = array();
		$ColumnHasTemplateAry = array();
		$WeightedMark = array();
		$hasDirectInputMark = 0;
		
		if(count($column_data) > 0)
		{
		    for($i=0; $i<count($column_data); $i++)
		    {
			  if($isAllWholeReport == 1 && $WholeYearColumnHasTemplateArr[$column_data[$i]['ReportColumnID']] != false){
				#######################################################################################################################################
				# for the Whole Year Report which defined any Term Column in other Whole Year Report	added on 22 May by Jason
				// have defined term report template 
				$WholeYearReportID = $WholeYearColumnHasTemplateArr[$column_data[$i]['ReportColumnID']];
				$WholeYearReportColumnData = $lreportcard->returnReportTemplateColumnData($WholeYearReportID); 		// Column Data
				$WholeYearReportColumnWeightData = $lreportcard->returnReportTemplateSubjectWeightData($WholeYearReportID);  
				if(count($WholeYearReportColumnWeightData) > 0)
				{
					for($j=0; $j<count($WholeYearReportColumnWeightData); $j++){
						if($WholeYearReportColumnWeightData[$j]['SubjectID'] == $SubjectID && $WholeYearReportColumnWeightData[$j]['ReportColumnID'] != ""){
							$Weight[$WholeYearReportColumnWeightData[$j]['ReportColumnID']] = $WholeYearReportColumnWeightData[$j]['Weight'];
						}
					}
				}
				
				if(count($WholeYearReportColumnData) > 0)
				{
					for($j=0; $j<count($WholeYearReportColumnData); $j++){
						if($WholeYearReportColumnData[$j]['SemesterNum'] == $column_data[$i]['SemesterNum']){
							if($Weight[$WholeYearReportColumnData[$j]['ReportColumnID']] != 0){
								$MarksheetScoreInfoArr[$column_data[$i]['ReportColumnID']] = $lreportcard->GET_MARKSHEET_SCORE($StudentIDArr, $SubjectID, $WholeYearReportColumnData[$j]['ReportColumnID'], 0,'', $SkipSubjectGroupCheckingForGetMarkSheet);
							} else {
								$MarksheetScoreInfoArr[$column_data[$i]['ReportColumnID']] = '--'; // --? 20140722
							}
						}
					}
				}
				#######################################################################################################################################
			  } else {
			    # for the general Term Report &
			    # for the Whole Year Report which does not define any Term Column in other Whole Year Report
			    /* 
				* Check for the Mark Column is allowed to fill in mark when any term does not have related report template 
				* by SemesterNum (ReportType) && ClassLevelID
				* An array is initialized as a control variable [$ColumnHasTemplateAry]
				* A variable is initialized as a control variable [$hasDirectInputMark], must be used with [$ReportType == "F"]
				* to control the Import function to be used or not
				*/
				if($ReportType == "F"){
					$ColumnHasTemplateAry[$column_data[$i]['ReportColumnID']] = $lreportcard->CHECK_REPORT_TEMPLATE_FROM_COLUMN($column_data[$i]['SemesterNum'], $ClassLevelID);
					if(!$ColumnHasTemplateAry[$column_data[$i]['ReportColumnID']])
						$hasDirectInputMark = 1;
				} else {
					$ColumnHasTemplateAry[$column_data[$i]['ReportColumnID']] = 1;
				}
			    
			    # Get Data from Subject without Component Subjects OR Subject having Component Subjects with ScaleInput is Not Marked
			    if(count($CmpSubjectArr) == 0 || 
			    	(count($CmpSubjectArr) > 0 && !$isCalculateByCmpSub) || 
			    	(count($CmpSubjectArr) >0 && $isCalculateByCmpSub && (($SchemeType == "H" && $ScaleInput == "G") || $SchemeType == "PF")) ){
			    	$MarksheetScoreInfoArr[$column_data[$i]['ReportColumnID']] = $lreportcard->GET_MARKSHEET_SCORE($StudentIDArr, $SubjectID, $column_data[$i]['ReportColumnID'],0,'', $SkipSubjectGroupCheckingForGetMarkSheet);
				} else {
					# Conidtion: Subject contains Component Subjects & Mark is calculated by its components subjects
					// Get Parent Subject Marksheet Score by Marksheet Score of Component Subjects if any
					if($ReportType == "F" && !$ColumnHasTemplateAry[$column_data[$i]['ReportColumnID']]){
						// for whole year report & it does not have term report template defined 
						$ParentSubMarksheetScoreInfoArr[$column_data[$i]['ReportColumnID']] = $lreportcard->GET_STUDENT_TERM_SUBJECT_MARK_WITHOUT_TEMPLATE($StudentIDArr, $ReportID, $column_data[$i]['ReportColumnID'], $SubjectID, $CmpSubjectIDArr);
					} else {
						// for term report use only
						if ($IsCmpAllZeroWeightArr[$column_data[$i]['ReportColumnID']] == 1) { // all cmp zero weight => get the overall from the parent subject directly
							$ParentSubMarksheetScoreInfoArr[$column_data[$i]['ReportColumnID']] = $lreportcard->GET_PARENT_SUBJECT_MARKSHEET_SCORE($StudentIDArr, $SubjectID, $CmpSubjectIDArr, $ReportID, $column_data[$i]['ReportColumnID'], "Term", 1, 0, $checkSpecialFullMark=false);
						}
						else {
							$ParentSubMarksheetScoreInfoArr[$column_data[$i]['ReportColumnID']] = $lreportcard->GET_PARENT_SUBJECT_MARKSHEET_SCORE($StudentIDArr, $SubjectID, $CmpSubjectIDArr, $ReportID, $column_data[$i]['ReportColumnID'], "Term", 0, 0, $checkSpecialFullMark=false, $forViewOverallMSDisplay=true);
						}
					}
				}
				
				# For Whole Year Report use only when Case i
				if($ReportType == "F")
				{
					# Case 1: Get the marksheet OVERALL score when the inputscale is Grade(G) OR PassFail(PF)
					if(($SchemeType == "H" && $ScaleInput == "G") || $SchemeType == "PF" || $displayOverallColumnDueToAllZeroWeight){
						if($ColumnHasTemplateAry[$column_data[$i]['ReportColumnID']]){
							$TermReportID = $ColumnHasTemplateAry[$column_data[$i]['ReportColumnID']];
							$MarksheetOverallScoreInfoArr[$column_data[$i]['ReportColumnID']] = $lreportcard->GET_MARKSHEET_OVERALL_SCORE($StudentIDArr, $SubjectID, $TermReportID, $SkipSubjectGroupCheckingForGetMarkSheet);
						}
					}
					else if($SchemeType == "H" && $ScaleInput == "M"){
						# Case 2: Calculating The Weighted Mark of Parent Subjects if having Component Subjects
						if($ColumnHasTemplateAry[$column_data[$i]['ReportColumnID']]) {	
							// have defined term report template 
							$TermReportID = $ColumnHasTemplateAry[$column_data[$i]['ReportColumnID']];
							$TermReportColumnID = $column_data[$i]['TermReportColumnID'];
							
							// [2017-0508-1128-10164]
							$excludedTermReportColumnWeight = $eRCTemplateSetting['Settings']['TemplateSettings_ConsolidatedReportWithTermReportAssessment_ExcludeWeightDisplay'];
							
							// [2017-0626-1124-47206] Get Term Subject Mark - Vertical > Horizontal (Consolidate Report)
							if($ApplySubjectHorizontalCalculation) {
								if(!$isCalculateByCmpSub) {
									# Get Student Term Subject Mark of common subject without any component subjects
									$WeightedMark[$column_data[$i]['ReportColumnID']] = $lreportcard->GET_STUDENT_TERM_SUBJECT_MARK($StudentIDArr, $TermReportID, $SubjectID, $CmpSubjectIDArr, 2, $ReportID, $TermReportColumnID, $excludedTermReportColumnWeight, true);
								}
								else {
									# Get Student Term Subject Mark of a subject by manipulating data of its component subjects
									$WeightedMark[$column_data[$i]['ReportColumnID']] = $lreportcard->GET_STUDENT_TERM_SUBJECT_MARK($StudentIDArr, $TermReportID, $SubjectID, $CmpSubjectIDArr, 2, $ReportID, $TermReportColumnID, $excludedTermReportColumnWeight, true);
								}
							}
							// Normal
							else {
								if(!$isCalculateByCmpSub) {
									# Get Student Term Subject Mark of common subject without any component subjects
									$WeightedMark[$column_data[$i]['ReportColumnID']] = $lreportcard->GET_STUDENT_TERM_SUBJECT_MARK($StudentIDArr, $TermReportID, $SubjectID, $CmpSubjectIDArr, 1, $ReportID, $TermReportColumnID, $excludedTermReportColumnWeight);
								}
								else {
									# Get Student Term Subject Mark of a subject by manipulating data of its component subjects
									$WeightedMark[$column_data[$i]['ReportColumnID']] = $lreportcard->GET_STUDENT_TERM_SUBJECT_MARK($StudentIDArr, $TermReportID, $SubjectID, $CmpSubjectIDArr, $TermCalculationType, $ReportID, $TermReportColumnID, $excludedTermReportColumnWeight);
								}
							}
						}
						else {
							# does not have term report Template for whole year report &
							# subject contains component subjects
							if($isCalculateByCmpSub) {
								$ParentSubMarksheetScoreInfoArr[$column_data[$i]['ReportColumnID']] = $lreportcard->GET_STUDENT_TERM_SUBJECT_MARK_WITHOUT_TEMPLATE($StudentIDArr, $ReportID, $column_data[$i]['ReportColumnID'], $SubjectID, $CmpSubjectIDArr);
								$hasDirectInputMark = 0;
							}
						}
					}
				}
		  	  } # End of if($isAllWholeReport == 0)
			}
		}
		
		# Get Overall Term Subject Mark of a subject having Component Subjects when 
		# Condition 1: TagsType = 1 (Preview Mark)
		# Condition 2: $eRCTemplateSetting['Marksheet']['CanPreviewOverallTermResult'] and 2nd / 3rd Terms (Removed - Not Support Components) 
		$OverallTermSubjectWeightedMark = array();
		if($TagsType == 1 && $ReportType != "F" && !empty($CmpSubjectIDArr)){
			$OverallTermSubjectWeightedMark = $lreportcard->GET_STUDENT_TERM_SUBJECT_MARK($StudentIDArr, $ReportID, $SubjectID, $CmpSubjectIDArr, $TermCalculationType);
		}
		
		/*
		* General Case for generating MarkSheet Column
		* Providing 2 Arrays : one is Column Data [$column_data] , the other is Student Data [$StudentArr]
		* Input Text Box ID : mark_[ReportColumnID]_[StudentID]
		*/
		# Main Content of Marksheet Reivision
		$display = '';
		$display .= '<table width="100%" cellpadding="4" cellspacing="0">';
		$display .= '<tr>
	          <td width="30" align="center" valign="top" class="tablegreentop tabletopnolink">'.$eReportCard['StudentNo_short'].'</td>
	          <td valign="top" class="tablegreentop tabletopnolink">'.$eReportCard['Student'].'</td>';
	    
	    $colSpanNum = 2;
    	if(count($column_data) > 0)
		{
			# [2017-0109-1818-40164] Display Previous Term Header and Retrieve Term Student Score
			$extraColCount = 0;
			if($GetPreviousTermResult) 
			{
				$relatedTermReportStudentMarks = array();
				$relatedTermReportGradingScheme = array();
				$relatedTermReportGradingFullMark = array();
				
				// loop Semester
				$currentSemesterNum = 1;
				foreach($allSemesterAry as $currentSemesterID => $currentSemesterName) {
					$currentTermReportID = $relatedTermReports[$currentSemesterID]["ReportID"];
					if(!$currentTermReportID) {
						continue;
					}
					
		          	// Skip if not previous term
		          	if($currentSemesterNum >= $ReportSemSeq) {
		          		break;
		          	}
					
					// Get Term Report Student Marks
					$relatedTermReportStudentMarks[$currentSemesterNum] = $lreportcard->getMarks($currentTermReportID, "", "", $ParentSubjectOnly=0, $includeAdjustedMarks=1, $OrderBy='', $SubjectID);
					
					// Get Term Report Grading Scheme
					$relatedTermReportGradingScheme[$currentSemesterNum] = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID, 0, 0, $currentTermReportID);
					
					// Get Term Report Full Mark
					$TermReportSchemeID = $relatedTermReportGradingScheme[$currentSemesterNum]['SchemeID'];
					$TermReportSchemeInfo = $lreportcard->GET_GRADING_SCHEME_INFO($TermReportSchemeID);
					$relatedTermReportGradingFullMark[$currentSemesterNum] = $TermReportSchemeInfo[0]['FullMark'];
					
					// Check Grading Scheme 
					$thisTermGradingScheme = $relatedTermReportGradingScheme[$currentSemesterNum];
					$thisScaleInput = $thisTermGradingScheme["ScaleInput"];
					if($thisScaleInput=="G") {
						$ShowAllTermResultSummary = false;
					}
					
					// Display Semester
				    $display .= '<td align="center" class="tablegreentop tabletopnolink" valign="top">'.$currentSemesterName.'<br /></td>';
		          	
		          	$currentSemesterNum++;
		          	$extraColCount++;
		          	$colSpanNum++;
				}
			}
			
			$columnCount = 0;
		    for($i=0 ; $i<count($column_data) ; $i++)
			{
			    list($ReportColumnID, $ColumnTitle, $DefaultWeight, $SemesterNum) = $column_data[$i];
			    //$ColumnTitle = ($ColumnTitle != "" && $ColumnTitle != null) ? convert2unicode($ColumnTitle, 1, 2) : $ColumnTitleAry[$ReportColumnID];
			    $ColumnTitle = ($ColumnTitle != "" && $ColumnTitle != null) ? $ColumnTitle : $ColumnTitleAry[$ReportColumnID];
			    $WeightOrPercent = ($usePercentage && $isPercentage) ? ($WeightAry[$ReportColumnID][$SubjectID] * 100)."%" : $WeightAry[$ReportColumnID][$SubjectID];
			    
			    // [2015-1130-0918-43073] hide Sub-MS button if column (T1A1) with empty/zero weight, prevent transfer Sub-MS overall mark to MS incorrectly
			    // [2015-0924-1821-07164] skip checking for Wah Yan, Kowloon to store temp marks
			    $hideSubMSButtonCustChecking = !$eRCTemplateSetting['Marksheet']['ManualInputGrade'] || ($eRCTemplateSetting['Marksheet']['ManualInputGrade'] && $CurrentFormNumber<=3);
			    if($hideSubMSButtonCustChecking && ($eRCTemplateSetting['HideSubMarksheet'] || empty($WeightAry[$ReportColumnID][$SubjectID]))){
			   		// do nothing
			   	}
			   	else {
			   		// [2015-1130-0918-43073] for correct jsReportColumn value
					//$SubMarksheetBtn = ($TagsType==0 && $ScaleInput == "M") ? '<br /> <input type="button" class="formsmallbutton" onClick="javascript:newWindow(\'sub_marksheet.php?SubjectID='.$SubjectID.'&ClassID='.$ClassID.'&ReportColumnID='.$ReportColumnID.'&ReportID='.$ReportID.'&jsReportColumn='.$i.'&SubjectGroupID='.$SubjectGroupID.'&ParentSubjectID='.$ParentSubjectID.'\', 10)"  value="'.$eReportCard['SubMS'].'"   onMouseOver="this.className=\'formsmallbuttonon\'" onMouseOut="this.className=\'formsmallbutton\'"/>' : "";
					$SubMarksheetBtn = ($TagsType==0 && $ScaleInput == "M") ? '<br /> <input type="button" class="formsmallbutton" onClick="javascript:newWindow(\'sub_marksheet.php?SubjectID='.$SubjectID.'&ClassID='.$ClassID.'&ReportColumnID='.$ReportColumnID.'&ReportID='.$ReportID.'&jsReportColumn='.$columnCount.'&SubjectGroupID='.$SubjectGroupID.'&ParentSubjectID='.$ParentSubjectID.'\', 10)"  value="'.$eReportCard['SubMS'].'"   onMouseOver="this.className=\'formsmallbuttonon\'" onMouseOut="this.className=\'formsmallbutton\'"/>' : "";
			   		
			   		// [2015-0924-1821-07164] for Wah Yan, Kowloon, prevent transfer incorrect marks to marksheet
			   		if($eRCTemplateSetting['Marksheet']['ManualInputGrade'] && $CurrentFormNumber>3 && empty($WeightAry[$ReportColumnID][$SubjectID])){
			   			// do nothing
			   		}
			   		else{
			   			$columnCount++;
			   		}
			    }
			    
			    if($SchemeType == "PF" || ($SchemeType == "H" && $ScaleInput == "G"))
			    {
			    	$optionArr = array();
			    	$optionArr[] = array(-1,"-- ".$eReportCard['CopyTo']." --");
			    	for($j=0; $j<count($column_data); $j++)
			    	{
			    		if($column_data[$j]['ReportColumnID'] != $ReportColumnID)
			    		{
			    			$thisColumnTitle = $column_data[$j]['ColumnTitle'];
		    				$thisColumnTitle = ($thisColumnTitle != "" && $thisColumnTitle != null) ? $thisColumnTitle : $ColumnTitleAry[$column_data[$j]['ReportColumnID']];
			    			$optionArr[] = array($column_data[$j]['ReportColumnID'],$thisColumnTitle);
			    		}
			    	}
			    	
			    	$overallColumnNumber = $j;
			    	$optionArr[] = array(0,($ReportType != "F") ? $eReportCard['TermSubjectMark'] : $eReportCard['OverallSubjectMark']);
			    	$CopyToSelection = "<br>".getSelectByArray($optionArr, " id='CopyToSelection_".$ReportColumnID."' ",-1,0,1);
			    	$CopyToSelection .= '<a href="javascript:js_Copy_Grade_From_Column('.$ReportColumnID.');"><img width="12" height="12" border="0" title="'.$eReportCard['Copy'].'" alt="'.$eReportCard['Copy'].'" src="/images/2009a/icon_copy.gif"></a>';
			    }
			    
			    $display .= '<td align="center" class="tablegreentop tabletopnolink" valign="top">'.$ColumnTitle.'<br />'.(($isSetLabel) ? $WeightOrPercent : "");
			    	$display .= '<div class="hideInPrint">';
				    	$display .= $SubMarksheetBtn;
			           	$display .= $CopyToSelection;
		            $display .= '</div>';
		            ############## Hide Sub Marksheet button: Backend logic not written at all
					//$display .= '<input name="SubMS_'.$ReportColumnID.'" onMouseOver="this.className=\'formsmallbuttonon\'" onMouseOut="this.className=\'formsmallbutton\'" type="button" value="'.$eReportCard['SubMS'].'" class="formsmallbutton"/>';
		            ##############
					$display .= '<input type="hidden" id="ColumnWeight_'.$ReportColumnID.'" name="ColumnWeight_'.$ReportColumnID.'" value="'.$WeightAry[$ReportColumnID][$SubjectID].'"/>';
		            $display .= '<input type="hidden" id="ColumnID_'.$i.'" name="ColumnID_'.$i.'" value="'.$ReportColumnID.'"/>';	            
	          	$display .= '</td>';
	          	
	          	$colSpanNum++;
      		}
      		
			# [2017-0109-1818-40164] Display Current Term and Year Header
      		if($GetPreviousTermResult) {
      			$display .= '<td align="center" class="tablegreentop tabletopnolink" valign="top">'.$currentSemesterName.'<br /></td>';
      			$colSpanNum++;
      		}
  			if($GetPreviousTermResult && $ShowAllTermResultSummary) {
      			$display .= '<td align="center" class="tablegreentop tabletopnolink" valign="top">'.$eReportCard['WholeYear'].'<br /></td>';
      			$colSpanNum++;
  			}
      		
      		if ($eRCTemplateSetting['OverallGradeCalculatedFromAssesmentGPA'] && $ScaleInput == "G" && !$displayOverallColumnDueToAllZeroWeight) {
      			// hide overall column display
      		}
      		else {
      			if(($TagsType == 0 || $TagsType == 2) && (($SchemeType == "H" && $ScaleInput == "G") || $SchemeType == "PF" || $displayOverallColumnDueToAllZeroWeight))
      			{
		      		$thisDisplay = ($ReportType != "F") ? $eReportCard['TermSubjectMark'] : $eReportCard['OverallSubjectMark'];
		      		$thisDisplay = ($thisDisplay == '')? '&nbsp;' : $thisDisplay;
		      		
				    if($SchemeType == "PF" || ($SchemeType == "H" && $ScaleInput == "G"))
				    {
				    	$optionArr = array();
				    	
				    	$optionArr[] = array(-1,"-- ".$eReportCard['CopyTo']." --");
				    	for($j=0; $j<count($column_data); $j++)
				    	{
			    			$thisColumnTitle = $column_data[$j]['ColumnTitle'];
			    			$thisColumnTitle = ($thisColumnTitle != "" && $thisColumnTitle != null) ? $thisColumnTitle : $ColumnTitleAry[$column_data[$j]['ReportColumnID']];
			    			$optionArr[] = array($column_data[$j]['ReportColumnID'],$thisColumnTitle);
				    	}
				    	$CopyToSelection = "<br>".getSelectByArray($optionArr, " id='CopyToSelection_0' ",-1,0,1);
				    	$CopyToSelection .= '<a href="javascript:js_Copy_Grade_From_Column(0);"><img width="12" height="12" border="0" title="'.$eReportCard['Copy'].'" alt="'.$eReportCard['Copy'].'" src="/images/2009a/icon_copy.gif"></a>';
				    }
		      		
		      		$display .= '<td align="center" class="tablegreentop tabletopnolink">';
		      			$display .= $thisDisplay;
		      			$display .= '<div class="hideInPrint">';
		      				$display .= $CopyToSelection;
		      			$display .= '</div>';
		      		$display .= '</td>';
		      		$colSpanNum++;
	      		}
      		}
      		
      		if($TagsType == 1)
      		{
	      		$thisDisplay = ($ReportType != "F") ? $eReportCard['TermSubjectMark'] : $eReportCard['OverallSubjectMark'];
	      		$thisDisplay = ($thisDisplay == '')? '&nbsp;' : $thisDisplay;
      			$display .= '<td align="center" class="tablegreentop tabletopnolink">'.$thisDisplay.'</td>';
      			$colSpanNum++;
  			}
  			
  			# Add Empty Columns to make the mark input column more near to the student name
  			$minNumColumn = ($lreportcard->MS_MinimunColumn)? $lreportcard->MS_MinimunColumn : 0;
  			$numOfEmptyColumn = $minNumColumn - count($column_data);
  			$numOfEmptyColumn -= $extraColCount? $extraColCount : 0; 
  			if ($numOfEmptyColumn > 0)
  			{
	  			for($i=0 ; $i<$numOfEmptyColumn ; $i++)
	  			{
		  			$display .= '<td align="left" class="tablegreentop tabletopnolink" width="12%">&nbsp;</td>';
	  			}
  			}
  			else
  			{
	  			$numOfEmptyColumn = 0;
  			}
    	}
	    $display .= '</tr>';
	    
	    # updated on 12 Jan 2009 by Ivan
		# disable preview if calculation type is verticle->horizontal
	    if (($TagsType==1 || $TagsType==2) && $thisCalculationType==2)
	    {
		    if ($ReportTypeTW=="T")
		    {
				$thisWarning = $eReportCard['TermPreviewNotApplicable'];
		    }
		    else
		    {
			    $thisWarning = $eReportCard['YearPreviewNotApplicable'];
		    }
		    $display .= '<tr class="tablegreenrow"><td class="tabletext" colspan="10" align="center">'. $thisWarning .'</td></tr>';
	    }
	    else
	    {
		    # Student Content of Marksheet Revision
		    # $OverallSubjectMark is used as a reference for user 
		    # $isShowOverallMark is used as a control to display term/overall subject mark
		    $MarksheetOverallScoreInfoArr[0] = $lreportcard->GET_MARKSHEET_OVERALL_SCORE($StudentIDArr, $SubjectID, $ReportID, $SkipSubjectGroupCheckingForGetMarkSheet);
		    for($j=0; $j<$StudentSize; $j++)
		    {
				list($StudentID, $WebSAMSRegNo, $StudentNo, $StudentName, $ClassName) = $StudentArr[$j];
				$tr_css = ($j % 2 == 1) ? "tablegreenrow2" : "tablegreenrow1";
				$td_css = ($j % 2 == 1) ? "attendanceouting" : "attendancepresent";
				
				if ($showClassName) {
					$thisClassDisplay = $ClassName.'-'.$StudentNo;
				}
				else {
					$thisClassDisplay = $StudentNo;
				}
				
				$display .= '<tr class="'.$tr_css.' dataRow">
			          <td align="left" nowrap="nowrap" class="'.$td_css.' tabletext">'.$thisClassDisplay.'</td>
		          	  <td nowrap="nowrap" class="'.$td_css.' tabletext">'.$StudentName.'<input type="hidden" name="StudentID[]" id="StudentID_'.$j.'" value="'.$StudentID.'"/></td>';
		        
		        if(count($column_data) > 0)
		        {
			        $OverallSubjectMark = 0;
			        $OverallSubjectFullMark = 0;
			        $OverallYearSubjectMark = 0;
			        $OverallYearSubjectFullMark = 0;
			        $OverallYearSubjectSpecialCase = array();
			        $isShowOverallMark = 1;
			        
			        # Check for Combination of Special Case of Input Mark / Term Mark
			        $SpecialCaseArr = $lreportcard->GET_POSSIBLE_MARKSHEET_SPECIAL_CASE_SET1();
			        foreach($SpecialCaseArr as $key){
			        	$SpecialCaseCountArr[$key] = array("Count"=>0, "Value"=>'', "isCount"=>'');
		        	}
		        	
		        	# Prepare array for converting mark to weighted mark
					# AllColumnMarkArr is refer the all Column Marks of each student
					if($TagsType == 1 || $GetPreviousTermResult)
					{
						$AllColumnMarkArr = array();
						for($r=0; $r<$ColumnSize; $r++)
						{
							if( $WeightAry[$column_data[$r]['ReportColumnID']][$SubjectID] != 0 && 
								$WeightAry[$column_data[$r]['ReportColumnID']][$SubjectID] != null && 
								$WeightAry[$column_data[$r]['ReportColumnID']][$SubjectID] != "" )
							{
								if($isAllWholeReport == 1 && $WholeYearColumnHasTemplateArr[$column_data[$i]['ReportColumnID']] != false){
									// new approach 
								}
								else {
									if($ReportType == "F" && $ColumnHasTemplateAry[$column_data[$r]['ReportColumnID']] && $ScaleInput == "M"){	// whole year with term template
										if(!empty($CmpSubjectArr) && $isCalculateByCmpSub){
											// Subject with Component
											$AllColumnMarkArr[$column_data[$r]['ReportColumnID']] = $WeightedMark[$column_data[$r]['ReportColumnID']][$StudentID];
										}
										else {
											// Subject without Component
											$AllColumnMarkArr[$column_data[$r]['ReportColumnID']] = $WeightedMark[$column_data[$r]['ReportColumnID']][$StudentID];
										}
									}
									else {
										// Term Report OR Whole Year Report without Term Template
										if(!empty($CmpSubjectArr) && $isCalculateByCmpSub){
											// Subject with Component
											$AllColumnMarkArr[$column_data[$r]['ReportColumnID']] = $ParentSubMarksheetScoreInfoArr[$column_data[$r]['ReportColumnID']][$StudentID];
										}
										else {
											// Subject without Component
											if($MarksheetScoreInfoArr[$column_data[$r]['ReportColumnID']][$StudentID]['MarkType'] == "SC")
								    			$AllColumnMarkArr[$column_data[$r]['ReportColumnID']] = $MarksheetScoreInfoArr[$column_data[$r]['ReportColumnID']][$StudentID]['MarkNonNum'];
								    		else 
												$AllColumnMarkArr[$column_data[$r]['ReportColumnID']] = $MarksheetScoreInfoArr[$column_data[$r]['ReportColumnID']][$StudentID]['MarkRaw'];
										}
									}
								} # End if($isAllWholeReport)
					    	}
					    } # End Report Column Loop
					}
					
					# [2017-0109-1818-40164] Display Previous Term Student Score
					if($GetPreviousTermResult)
					{
						$displayOverallNotConsideredSymbol = false;
						$_tdCss = ($j % 2 == 1) ? "attendanceouting" : "attendancepresent";
						
						// loop Semester
						$currentSemesterNum = 1;
						foreach($allSemesterAry as $currentSemesterID => $currentSemesterName)
						{
							$currentTermReportID = $relatedTermReports[$currentSemesterID]["ReportID"];
							if(!$currentTermReportID) {
								continue;
							}
							
							// Student Overall Score
							$thisStudentTermScore = $relatedTermReportStudentMarks[$currentSemesterNum][$StudentID][$SubjectID];
							$thisStudentMarks = $thisStudentTermScore[0];
							$thisStudentMarkScore = $thisStudentMarks["Mark"];
							$thisStudentMarkGrade = $thisStudentMarks["Grade"];
							
							// Current Grading Scheme
							$thisGradingScheme = $relatedTermReportGradingScheme[$currentSemesterNum];
							$thisScaleInput = $thisGradingScheme["ScaleInput"];
							$thisScaleDisplay = $thisGradingScheme["ScaleDisplay"];
							
							// Handle Student Score
							if($thisScaleInput=="G") {
								$thisStudentMarkScore = $thisStudentMarkGrade;
							}
							else if($thisScaleInput=="M" && strlen($thisStudentMarkScore)) {
				    			$thisStudentMarkScore = $lreportcard->ROUND_MARK($thisStudentMarkScore, "SubjectScore");
				    			$thisStudentMarkScore = $lreportcard->CONVERT_MARK_TO_RELATIVE_DECIMAL_PLACES($thisStudentMarkScore, $storDispSettings["SubjectScore"]);
							}
							list($thisStudentDisplayMark, $needStyle) = $lreportcard->checkSpCase($currentTermReportID, $SubjectID, $thisStudentMarkScore, $thisStudentMarkGrade);
							
							// Student Report Column Grades - Not Considered Case Checking
							$displayNotConsideredSymbol = false;
							foreach($thisStudentTermScore as $thisTermScoreDetails) {
								$thisTermScoreGrade = $lreportcard->GET_MARKSHEET_SPECIAL_CASE_SET1_STRING($thisTermScoreDetails["Grade"]);
								if($thisTermScoreGrade == $eReportCard['RemarkAbsentNotConsidered']) {
									$displayNotConsideredSymbol = true;
									$displayOverallNotConsideredSymbol = true;
									break;
								}
							}
							if($displayNotConsideredSymbol) {
								$thisStudentMarkScore = $eReportCard['RemarkAbsentNotConsidered'];
								$thisStudentDisplayMark = $thisStudentMarkScore;
							}
							
							// Display Term Score
			      			$display .= '<td align="center" class="'.$_tdCss.' tabletext">'.$thisStudentDisplayMark.'</td>';
			      			
			    			# Overall Student Score Calculation
			    			if($ShowAllTermResultSummary && !$displayOverallNotConsideredSymbol && $thisScaleInput=="M" && strlen($thisStudentMarkScore))
			    			{
			    				$isSpecialCase = in_array($thisStudentDisplayMark, $lreportcard->specialCasesSet1);
			    				if(!$isSpecialCase || $thisStudentDisplayMark=="+")
			    				{
			    					// Initial
			    					$TermSubjectMark = 0;
			    					$TermSubjectFullMark = 0;
			    					
			    					// loop Term Report Column
			    					foreach((array)$thisStudentTermScore as $thisTermColumnID => $thisTermColumnScore)
			    					{
			    						// Exclude Term Report Overall Column
			    						if($thisTermColumnID==0) {
			    							continue;
			    						}
			    						
			    						// Get Term Report Column Weight
			    						$thisTermReportColumnWeight = $YearReportWeightInfo[$thisTermColumnID];
			    						$thisTermReportColumnWeight = $thisTermReportColumnWeight["Weight"];
			    						
			    						// Exclude Zero Weight Report Column
			    						if($thisTermReportColumnWeight==0) {
			    							continue;
			    						}
			    						
										// Student Report Column Score
										$thisStudentTermMarkScore = $thisTermColumnScore["Mark"];
										$thisStudentTermMarkGrade = $thisTermColumnScore["Grade"];
										
										// Handle Student Report Column Score
//										if($thisScaleInput=="M" && strlen($thisStudentTermMarkScore)) {
//							    			$thisStudentTermMarkScore = $lreportcard->ROUND_MARK($thisStudentTermMarkScore, "SubjectScore");
//							    			$thisStudentTermMarkScore = $lreportcard->CONVERT_MARK_TO_RELATIVE_DECIMAL_PLACES($thisStudentTermMarkScore, $storDispSettings["SubjectScore"]);
//										}
										list($thisStudentTermCalculateMark, $needStyle) = $lreportcard->checkSpCase($currentTermReportID, $SubjectID, $thisStudentTermMarkScore, $thisStudentTermMarkGrade);
			    						
			    						// Add Term Report Total
					    				$isColumnSpecialCase = in_array($thisStudentTermCalculateMark, $lreportcard->specialCasesSet1);
					    				if(strlen($thisStudentTermMarkScore) && (!$isColumnSpecialCase || $thisStudentTermCalculateMark=="+"))
					    				{
								    		$thisStudentTermMarkScore = $thisStudentTermMarkScore * $thisTermReportColumnWeight;
				    						//$thisStudentTermMarkScore = $lreportcard->ROUND_MARK($thisStudentTermMarkScore, "SubjectScore");
								    		//$thisStudentTermMarkScore = $lreportcard->CONVERT_MARK_TO_RELATIVE_DECIMAL_PLACES($thisStudentTermMarkScore, $storDispSettings["SubjectScore"]);
								    		$TermSubjectMark += $thisStudentTermMarkScore;
								    		
								    		$thisTermSubjectFullMark = $relatedTermReportGradingFullMark[$currentSemesterNum] * $thisTermReportColumnWeight;
				    						//$thisTermSubjectFullMark = $lreportcard->ROUND_MARK($thisTermSubjectFullMark, "SubjectScore");
								    		//$thisTermSubjectFullMark = $lreportcard->CONVERT_MARK_TO_RELATIVE_DECIMAL_PLACES($thisTermSubjectFullMark, $storDispSettings["SubjectScore"]);
								    		$TermSubjectFullMark += $thisTermSubjectFullMark;
					    				}
			    					}
			    					
			    					// Handle Term Report Score and Full Mark
			    					if($TermSubjectMark > 0 && $TermSubjectFullMark > 0)
			    					{
							    		$OverallYearSubjectMark += $TermSubjectMark;
							    		$OverallYearSubjectFullMark += $TermSubjectFullMark;
			    					}
			    				}
			    				else {
			    					$OverallYearSubjectSpecialCase[] = $thisStudentDisplayMark;
			    				}
			    			}
			      			
		          			// Skip if not previous term
				          	$currentSemesterNum++;
				          	if($currentSemesterNum >= $ReportSemSeq) {
				          		break;
				          	}
						}
					}
					
					// Initial
					$TermSubjectMark = 0;
					$TermSubjectFullMark = 0;
					
					# Loop Report Column
					$numOfInputBoxDisplayed = 0;
				    for($i=0; $i<count($column_data); $i++)
				    {
					    list($ReportColumnID, $ColumnTitle, $DefaultWeight, $SemesterNum) = $column_data[$i];
					    
					    if($isAllWholeReport == 1 && $WholeYearColumnHasTemplateArr[$column_data[$i]['ReportColumnID']] != false)
					    {
						    #############################################################################
						    # New Approach dealing with only having Whole Year Report
						    #############################################################################
					        
					        // View of Raw Mark
						    if($TagsType == 0)
						    {
						    	if($WeightAry[$ReportColumnID][$SubjectID] != 0 && $WeightAry[$ReportColumnID][$SubjectID] != null && $WeightAry[$ReportColumnID][$SubjectID] != "")
						    	{
								    $MarkType = (count($MarksheetScoreInfoArr) > 0)? $MarksheetScoreInfoArr[$ReportColumnID][$StudentID]['MarkType'] : '';
								    if(count($MarksheetScoreInfoArr) > 0)
								    {
									    if($MarksheetScoreInfoArr[$ReportColumnID] == '--') {
										    $InputMethod = '--';
									    } else if($MarkType == "SC") {
										    $InputMethod = $MarksheetScoreInfoArr[$ReportColumnID][$StudentID]['MarkNonNum'];
									    } else if($MarkType == "G") {
										    $tmpNonNum = $MarksheetScoreInfoArr[$ReportColumnID][$StudentID]['MarkNonNum'];
										    $InputMethod = $SchemeGrade[$tmpNonNum];
										} else if($MarkType == "PF") {
											$tmpNonNum = $MarksheetScoreInfoArr[$ReportColumnID][$StudentID]['MarkNonNum'];
											$InputMethod = $SchemePassFail[$tmpNonNum];
										} else {
										    $InputMethod = $MarksheetScoreInfoArr[$ReportColumnID][$StudentID]['MarkRaw'];
										    
										    // [2019-0123-1731-14066] add rounding logic
										    if($eRCTemplateSetting['Marksheet']['DisplayRoundMarks'] && is_numeric($InputMethod)) {
										        $InputMethod = $lreportcard->ROUND_MARK($InputMethod, "SubjectScore");
										        $InputMethod = $lreportcard->ROUND_MARK_TO_RELATIVE_DECIMAL_PLACES($InputMethod, $ReportType, "SubjectScore");
										    }
										}
									}
								}
								else
								{
									$InputMethod = '--';
								}
							}
							else if($TagsType == 1)
							{
								if($WeightAry[$ReportColumnID][$SubjectID] != 0 && $WeightAry[$ReportColumnID][$SubjectID] != null && $WeightAry[$ReportColumnID][$SubjectID] != "")
								{
									$MarkType = (count($MarksheetScoreInfoArr) > 0) ? $MarksheetScoreInfoArr[$ReportColumnID][$StudentID]['MarkType'] : '';
									if(count($MarksheetScoreInfoArr) > 0)
									{
									  	if($MarkType == "SC"){
									  		$tmpRaw = $MarksheetScoreInfoArr[$ReportColumnID][$StudentID]['MarkNonNum'];
									  	} else {
									  		$tmpRaw = $MarksheetScoreInfoArr[$ReportColumnID][$StudentID]['MarkRaw'];
									  	}
									  	
										if($tmpRaw == "-" || $tmpRaw == "*" || $tmpRaw == "/" || $tmpRaw == "N.A."){
								    		$InputMethod = $tmpRaw;
								    		$InputMethod .= '<input type="hidden" id="'.$mark_id.'" name="'.$mark_name.'" value="'.$tmpRaw.'"/>&nbsp;';
							    		} else if($tmpRaw != "NOTCOMPLETED"){
								    		# for displaying mark with weight calculation
										    $tmpWeightedVal = $lreportcard->CONVERT_MARK_TO_WEIGHTED_MARK($ReportID, $ReportColumnID, $SubjectID, $tmpRaw, $CalculationType);
							    			//$roundVal = ($storDispSettings["SubjectScore"] != "") ? round($tmpWeightedVal, $storDispSettings["SubjectScore"]) : round($tmpWeightedVal, 0);
							    			$roundVal = $lreportcard->ROUND_MARK($tmpWeightedVal, "SubjectScore");
							    			$InputMethod = $lreportcard->CONVERT_MARK_TO_RELATIVE_DECIMAL_PLACES($roundVal, $storDispSettings["SubjectScore"]);
							    			$InputMethod .= '<input type="hidden" id="'.$mark_id.'" name="'.$mark_name.'" value="'.$roundVal.'"/>&nbsp;';
							    			
							    			# for calculating real overall subject mark
							    			$tmpWeightedVal = $lreportcard->CONVERT_MARK_TO_WEIGHTED_MARK($ReportID, $ReportColumnID, $SubjectID, $tmpRaw, $CalculationType, $AllColumnMarkArr);
							    			$OverallSubjectMark += $tmpWeightedVal;
						    			} else {
							    			$InputMethod = '<input type="hidden" id="'.$mark_id.'" name="'.$mark_name.'" value=""/>&nbsp;';
							    			$isShowOverallMark = 0;
						    			}
						    		}
								} else {
									$InputMethod = '--';
								}
							}
							else if($TagsType == 2)
							{
								if($WeightAry[$ReportColumnID][$SubjectID] != 0 && $WeightAry[$ReportColumnID][$SubjectID] != null && $WeightAry[$ReportColumnID][$SubjectID] != ""){
									$MarkType = (count($MarksheetScoreInfoArr) > 0) ? $MarksheetScoreInfoArr[$ReportColumnID][$StudentID]['MarkType'] : '';
									if(count($MarksheetScoreInfoArr) > 0){
							    		$GradeVal = '';
							    		if($MarkType == "SC"){
							    			$displayVal = $MarksheetScoreInfoArr[$ReportColumnID][$StudentID]['MarkNonNum'];
							    		} else if($MarkType == "G"){
							    			 $displayVal = $SchemeGrade[$MarksheetScoreInfoArr[$ReportColumnID][$StudentID]['MarkNonNum']];	
							    		} else if($MarkType == "M"){
							    		    $tmpRaw = $MarksheetScoreInfoArr[$ReportColumnID][$StudentID]['MarkRaw'];
							    			$displayVal = ($tmpRaw < 0 || $tmpRaw == "") ? "" : $lreportcard->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($SchemeID, $tmpRaw);
							    		}						    	
									    $InputMethod = $displayVal;
								    }
								} else {
									$InputMethod = '--';
								}
							}
						}
						else
						{
							#############################################################################
							# Original Approach dealing with having Term Report & Whole Year Report
							#############################################################################
						    // Mark Type : M-Mark, G-Grade, PF-PassFail, SC-SpecialCase
							$MarkType = (count($MarksheetScoreInfoArr) > 0) ? $MarksheetScoreInfoArr[$ReportColumnID][$StudentID]['MarkType'] : '';
							
							// View of Raw Mark
						    if($TagsType == 0)
						    {
						        # mark_id is used for paseting values to clipbroad from Excel
							    # mark_name is used for passing values to update
							    # [$i] of mark[$j][$i] == [$i] of ColumnID_$i for matching
							    $mark_name = "mark_".$ReportColumnID."_".$StudentID;
							    $mark_id = "mark[$j][$numOfInputBoxDisplayed]";
							    
							    # For Subject without component subject And ReportColumn does not have its Report Template to use only
							    if(count($MarksheetScoreInfoArr) > 0)
							    {
								    if($MarkType == "G" || $MarkType == "PF" || $MarkType == "SC"){
								    	$InputVal = $MarksheetScoreInfoArr[$ReportColumnID][$StudentID]['MarkNonNum'];
							    	} else if($MarkType == "M"){
										# For ScaleInput is Mark & SchemeType is Honor-Based
										# if MarkRaw is -1, the MarkRaw has not been assigned yet.
										# if MarkRaw >= 0, the MarkRaw is valid
							    	    $InputVal = ($MarksheetScoreInfoArr[$ReportColumnID][$StudentID]['MarkRaw'] != -1) ? $MarksheetScoreInfoArr[$ReportColumnID][$StudentID]['MarkRaw'] : '';
							    	} else {
								    	$InputVal = '';
							    	}
							    }
							    else
							    {
								    $InputVal = '';
							    }
							    
							    # Set the InputMark/Grade to be an unchangable value if any column has defined template for whole year use & 
							    # Set Input Mark (Text) / Grade Method (Selection Box) / PassFail (Selection Box)
							    if($WeightAry[$ReportColumnID][$SubjectID] != 0 && $WeightAry[$ReportColumnID][$SubjectID] != null && $WeightAry[$ReportColumnID][$SubjectID] != "")
							    {
							        if($ColumnHasTemplateAry[$ReportColumnID] && $ReportType == "F")
							        {
									    if(($SchemeType == "H" && $ScaleInput == "G") || $SchemeType == "PF") {
										    if(count($MarksheetOverallScoreInfoArr) > 0)
										    {
											    $tmpType = $MarksheetOverallScoreInfoArr[$ReportColumnID][$StudentID]['MarkType'];
											    $tmpNonNum = $MarksheetOverallScoreInfoArr[$ReportColumnID][$StudentID]['MarkNonNum'];
											    if($tmpType != "SC") {
											    	$InputMethod = ($SchemeType == "PF") ? $SchemePassFail[$tmpNonNum] : $SchemeGrade[$tmpNonNum];
											    }
											    else {
											    	$InputMethod = $tmpNonNum;
											    }
											    
											    $tmp_mark_id = "tmp_for_copy_mark_".$ReportColumnID."_".$StudentID;
											    $tmpValIndex = $InputGradeOptionAsso[$InputMethod];
											    $InputMethod .= '<input type="hidden" class="Column_'.$ReportColumnID.'" id="'.$tmp_mark_id.'" value="'.$tmpValIndex.'"/>&nbsp;';
										    }
									    } else if($ScaleInput == "M") {
									        if(!empty($CmpSubjectArr) && $isCalculateByCmpSub)
									        {
										    	# Case for generate the mark of parent subject from component subjects
									            $tmpVal = $WeightedMark[$ReportColumnID][$StudentID];
									            if($tmpVal == "-" || $tmpVal == "*" || $tmpVal == "/" || $tmpVal == "N.A."){
									                // [2019-0123-1731-14066] add rounding logic
									                if($eRCTemplateSetting['Marksheet']['DisplayRoundMarks'] && is_numeric($tmpVal)) {
									                    $tmpVal = $lreportcard->ROUND_MARK($tmpVal, "SubjectScore");
									                    $tmpVal = $lreportcard->ROUND_MARK_TO_RELATIVE_DECIMAL_PLACES($tmpVal, $ReportType, "SubjectScore");
									                }
											        $InputMethod = $tmpVal;
												    $InputMethod .= '<input type="hidden" id="'.$mark_id.'" name="'.$mark_name.'" value="'.$tmpVal.'"/>&nbsp;';
											    } else if($tmpVal != "NOTCOMPLETED" && $tmpVal != ""){
									    			//$roundVal = ($storDispSettings["SubjectScore"] != "") ? round($tmpVal, $storDispSettings["SubjectScore"]) : round($tmpVal, 0);
									    			//$InputMethod = $lreportcard->CONVERT_MARK_TO_RELATIVE_DECIMAL_PLACES($roundVal, $storDispSettings["SubjectScore"]);
											        $roundVal = $lreportcard->ROUND_MARK($tmpVal, "SubjectScore");
											        $InputMethod = $lreportcard->ROUND_MARK_TO_RELATIVE_DECIMAL_PLACES($roundVal, $ReportType, "SubjectScore");
											        $InputMethod = ($MarksheetScoreInfoArr[$ReportColumnID][$StudentID]['IsEstimated'])? $lreportcard->Get_Estimated_Score_Display($InputMethod) : $InputMethod;
									    			$InputMethod .= '<input type="hidden" id="'.$mark_id.'" name="'.$mark_name.'" value="'.$tmpVal.'"/>&nbsp;';
									    			
								    			} else {
									    			$InputMethod = "";
									    			$InputMethod .= '<input type="hidden" id="'.$mark_id.'" name="'.$mark_name.'" value=""/>&nbsp;';
								    			}
										    }
										    else
										    {
											    # Case for generate the mark of subject without any component subjects
											    # Case i : ReportType is Whole Year and ReportColumn has its own Template => Cannot modify Mark/Grade	
											    # The mark displayed is calculated by the data inputed from the Term Report Template.				    
										        if($SchemeType == "H")
										        {
										    		$tmpVal = $WeightedMark[$ReportColumnID][$StudentID];
										    		if($tmpVal == "-" || $tmpVal == "*" || $tmpVal == "/" || $tmpVal == "N.A."){
										    		    // [2019-0123-1731-14066] add rounding logic
										    		    if($eRCTemplateSetting['Marksheet']['DisplayRoundMarks'] && is_numeric($tmpVal)) {
										    		        $tmpVal = $lreportcard->ROUND_MARK($tmpVal, "SubjectScore");
										    		        $tmpVal = $lreportcard->ROUND_MARK_TO_RELATIVE_DECIMAL_PLACES($tmpVal, $ReportType, "SubjectScore");
										    		    }
										    		    $InputMethod = $tmpVal;
													    $InputMethod .= '<input type="hidden" id="'.$mark_id.'" name="'.$mark_name.'" value="'.$tmpVal.'"/>&nbsp;';
										    		} else if($tmpVal != "NOTCOMPLETED" && $tmpVal != ""){
										    			//$roundVal = ($storDispSettings["SubjectScore"] != "") ? round($tmpVal, $storDispSettings["SubjectScore"]) : round($tmpVal, 0);
										    		    //$InputMethod = $lreportcard->CONVERT_MARK_TO_RELATIVE_DECIMAL_PLACES($roundVal, $storDispSettings["SubjectScore"]);
										    		    $roundVal = $lreportcard->ROUND_MARK($tmpVal, "SubjectScore");
										    		    $InputMethod = $lreportcard->ROUND_MARK_TO_RELATIVE_DECIMAL_PLACES($roundVal, $ReportType, "SubjectScore");
									    				$InputMethod .= '<input type="hidden" id="'.$mark_id.'" name="'.$mark_name.'" value="'.$tmpVal.'"/>&nbsp;';
										    		} else {
										    			$InputMethod = "";
										    			$InputMethod .= '<input type="hidden" id="'.$mark_id.'" name="'.$mark_name.'" value=""/>&nbsp;';
										    		}
								    			} 
									    	}
								    	}
								    }
								    else
								    {
									    # Case ii : ReportType is Whole Year and ReportColumn does not have its own Template => Can modify Mark/Grade
									    # Case iii: ReportType is Term => Can modify Mark/Grade
									    if(!empty($CmpSubjectArr) && $isCalculateByCmpSub && $ScaleInput == "M")
									    {
										    $tmpVal = $ParentSubMarksheetScoreInfoArr[$ReportColumnID][$StudentID];
										    
										    // Case ii
										    if($ReportType == "F")
										    {
										        if($tmpVal == "-" || $tmpVal == "*" || $tmpVal == "/" || $tmpVal == "N.A."){
										            // [2019-0123-1731-14066] add rounding logic
										            if($eRCTemplateSetting['Marksheet']['DisplayRoundMarks'] && is_numeric($tmpVal)) {
										                $tmpVal = $lreportcard->ROUND_MARK($tmpVal, "SubjectScore");
										                $tmpVal = $lreportcard->ROUND_MARK_TO_RELATIVE_DECIMAL_PLACES($tmpVal, $ReportType, "SubjectScore");
										            }
												    $InputMethod = $tmpVal;
													$InputMethod .= '<input type="hidden" id="'.$mark_id.'" name="'.$mark_name.'" value="'.$tmpVal.'"/>&nbsp;';
												} else if($tmpVal != "NOTCOMPLETED" && $tmpVal != ""){
									    			//$roundVal = ($storDispSettings["SubjectScore"] != "") ? round($tmpVal, $storDispSettings["SubjectScore"]) : round($tmpVal, 0);
									    			//$InputMethod = $lreportcard->CONVERT_MARK_TO_RELATIVE_DECIMAL_PLACES($roundVal, $storDispSettings["SubjectScore"]);
									    			$roundVal = $lreportcard->ROUND_MARK($tmpVal, "SubjectScore");
									    			$InputMethod = $lreportcard->ROUND_MARK_TO_RELATIVE_DECIMAL_PLACES($roundVal, $ReportType, "SubjectScore");
									    			$InputMethod .= '<input type="hidden" id="'.$mark_id.'" name="'.$mark_name.'" value="'.$tmpVal.'"/>&nbsp;';
								    			} else {
									    			$InputMethod = "";
									    			$InputMethod .= '<input type="hidden" id="'.$mark_id.'" name="'.$mark_name.'" value=""/>&nbsp;';
								    			}
							    			}
							    			// Case iii
							    			else
							    			{
							    				// [DM#3151] added special case "+" for correct display
							    			    if($tmpVal == "-" || $tmpVal == "*" || $tmpVal == "/" || $tmpVal == "N.A." || $tmpVal == "+"){
							    			        // [2019-0123-1731-14066] add rounding logic
							    			        if($eRCTemplateSetting['Marksheet']['DisplayRoundMarks'] && is_numeric($tmpVal)) {
							    			            $tmpVal = $lreportcard->ROUND_MARK($tmpVal, "SubjectScore");
							    			            $tmpVal = $lreportcard->ROUND_MARK_TO_RELATIVE_DECIMAL_PLACES($tmpVal, $ReportType, "SubjectScore");
							    			        }
												    $InputMethod = $tmpVal;
													$InputMethod .= '<input type="hidden" id="'.$mark_id.'" name="'.$mark_name.'" value="'.$tmpVal.'"/>&nbsp;';
												}
												else if($tmpVal != ""){	//!strcmp($tmpVal, 0) || 
													//$roundVal = ($storDispSettings["SubjectScore"] != "") ? round($tmpVal, $storDispSettings["SubjectScore"]) : round($tmpVal, 0);
										    		//$InputMethod = $lreportcard->CONVERT_MARK_TO_RELATIVE_DECIMAL_PLACES($roundVal, $storDispSettings["SubjectScore"]);
										    		$roundVal = $lreportcard->ROUND_MARK($tmpVal, "SubjectScore");
										    		$InputMethod = $lreportcard->ROUND_MARK_TO_RELATIVE_DECIMAL_PLACES($roundVal, $ReportType, "SubjectScore");
										    		$InputMethod .= '<input type="hidden" id="'.$mark_id.'" name="'.$mark_name.'" value="'.$tmpVal.'"/>&nbsp;';
										    		$OverallSubjectMark += $tmpVal;
									    		}
									    		else {
									    			$InputMethod = $tmpVal;
										    		$InputMethod .= '<input type="hidden" id="'.$mark_id.'" name="'.$mark_name.'" value="'.$tmpVal.'"/>&nbsp;';
										    		$isShowOverallMark = 0;
									    		}
								    		}
									    }
									    else
									    {
										    $otherPar = array(	'class'=>'textboxnum', 
										    					'onpaste'=>'isPasteContent(event,\''.$j.'\', \''.$numOfInputBoxDisplayed.'\')',
										    					
										    					'onkeyup'=>'isPasteContent(event, \''.$j.'\', \''.$numOfInputBoxDisplayed.'\')',
										    					'onkeydown'=>'isPasteContent(event, \''.$j.'\', \''.$numOfInputBoxDisplayed.'\')',
										    					'onchange'=>'jCHANGE_STATUS()'
										    				);
										    
										    $classname = '';

										    # updated on 06 Mar 2017 by Frankie
										    # Disable edit if submission is end
										    // if($lreportcard->Is_Marksheet_Blocked($columnIDDataMap[$ReportColumnID]["BlockMarksheet"]))
											if($lreportcard->Is_Marksheet_Blocked($ViewOnly) || $lreportcard->Is_Marksheet_Blocked($columnIDDataMap[$ReportColumnID]["BlockMarksheet"]))
									        {
									        	$disabled = " disabled ";
									        	$classname = " enableonsubmit ";
									        	$otherPar['disabled']='disabled';
									        	$otherPar['class']='textboxnum enableonsubmit';
									        }
									        else
									        {
									        	$disabled = '';
									        }
										    
										    $classname .= " Column_$ReportColumnID ";
										    $mark_tag = 'id="'.$mark_id.'" name="'.$mark_name.'" '.$disabled .' onchange="jCHANGE_STATUS()" onpaste="isPasteContent(event,'.$j.', '.$numOfInputBoxDisplayed.')" onkeyup="isPasteContent(event, '.$j.', '.$numOfInputBoxDisplayed.')" onkeydown="isPasteContent(event, '.$j.', '.$numOfInputBoxDisplayed.')" onfocusout="jCHANGE_STATUS()" class="tabletexttoptext '.$classname.'"';
										    $numOfInputBoxDisplayed++;
										    
				        					if($SchemeType == "H" && $ScaleInput == "G") {
									    		$InputMethod = $linterface->GET_SELECTION_BOX($InputGradeOption, $mark_tag, '', $InputVal);
									    		$MSwithSelectionBox = true;
								    		}
								    		else if($SchemeType == "PF"){
									    		$InputMethod = $linterface->GET_SELECTION_BOX($InputPFOption, $mark_tag, '', $InputVal);
									    		$MSwithSelectionBox = true;
								    		}
								    		else {
								    		    // [2019-0123-1731-14066] add rounding logic
								    		    if($eRCTemplateSetting['Marksheet']['DisplayRoundMarks'] && is_numeric($InputVal)) {
								    		        $InputVal = $lreportcard->ROUND_MARK($InputVal, "SubjectScore");
								    		        $InputVal = $lreportcard->ROUND_MARK_TO_RELATIVE_DECIMAL_PLACES($InputVal, $ReportType, "SubjectScore");
								    		    }
								    			$thisInputValue = ($MarksheetScoreInfoArr[$ReportColumnID][$StudentID]['IsEstimated'])? $lreportcard->Get_Estimated_Score_Display($InputVal) : $InputVal;
								    			$InputMethod = $lreportcard->GET_INPUT_TEXT($mark_name, $mark_id, $thisInputValue, '', $otherPar);
								    		}
								    		
							    			# [2017-0109-1818-40164] Calculate Overall Subject Mark
							    			if($GetPreviousTermResult && $SchemeType == "H" && $ScaleInput == "M")
							    			{
							    				if($MarkType == "SC" && $InputVal != "+") {
										    		$SpecialCaseCountArr = $lreportcard->DIFFERENTIATE_SPECIAL_CASE($SpecialCaseCountArr, $InputVal, $absentExemptSettings);
							    				}
										    	else {
										    		// Handle Special Case "+"
										    		$InputVal = $InputVal=="+"? "0" : $InputVal;
									    			if($InputVal != "" && $InputVal >= 0)
									    			{
									    				// Input Score
									    				$thisRawVal = $InputVal;
											    		if ($eRCTemplateSetting['UseSumOfAssessmentToBeTheTermOverallMark'] == true) {
											    			$InputVal = $InputVal;
											    		}
											    		else {
											    			$InputVal = $lreportcard->CONVERT_MARK_TO_WEIGHTED_MARK($ReportID, $ReportColumnID, $SubjectID, $InputVal, $CalculationType);
											    		}
//											    		$roundVal = $lreportcard->ROUND_MARK($InputVal, "SubjectScore");
//											    		$roundVal = $lreportcard->CONVERT_MARK_TO_RELATIVE_DECIMAL_PLACES($roundVal, $storDispSettings["SubjectScore"]);
											    		$OverallSubjectMark += $InputVal;
											    		
											    		// Full Mark
									    				$thisRawColFullMark = $SchemeMainInfo['FullMark'];
											    		if ($eRCTemplateSetting['UseSumOfAssessmentToBeTheTermOverallMark'] == true) {
											    			$colFullMark = $SchemeMainInfo['FullMark'];
											    		}
											    		else {
											    			$colFullMark = $lreportcard->CONVERT_MARK_TO_WEIGHTED_MARK($ReportID, $ReportColumnID, $SubjectID, $SchemeMainInfo['FullMark'], $CalculationType);
											    		}
//											    		$colFullMark = $lreportcard->ROUND_MARK($colFullMark, "SubjectScore");
//											    		$colFullMark = $lreportcard->CONVERT_MARK_TO_RELATIVE_DECIMAL_PLACES($colFullMark, $storDispSettings["SubjectScore"]);
											    		$OverallSubjectFullMark += $colFullMark;
											    		
											    		// Get Report Column Weight
							    						$thisTermReportColumnWeight = $YearReportWeightInfo[$ReportColumnID];
							    						$thisTermReportColumnWeight = $thisTermReportColumnWeight["Weight"];
			    										
			    										// Add Term Report Total
									    				if($thisTermReportColumnWeight > 0 && strlen($thisRawVal) && $thisRawColFullMark > 0)
									    				{
								    						$thisStudentTermMarkScore = $lreportcard->ROUND_MARK($thisRawVal, "SubjectScore");
												    		$thisStudentTermMarkScore = $lreportcard->CONVERT_MARK_TO_RELATIVE_DECIMAL_PLACES($thisStudentTermMarkScore, $storDispSettings["SubjectScore"]);
												    		$thisStudentTermMarkScore = $thisStudentTermMarkScore * $thisTermReportColumnWeight;
												    		$TermSubjectMark += $thisStudentTermMarkScore;
												    		
								    						$thisTermSubjectFullMark = $lreportcard->ROUND_MARK($thisRawColFullMark, "SubjectScore");
												    		$thisTermSubjectFullMark = $lreportcard->CONVERT_MARK_TO_RELATIVE_DECIMAL_PLACES($thisTermSubjectFullMark, $storDispSettings["SubjectScore"]);
												    		$thisTermSubjectFullMark = $thisTermSubjectFullMark * $thisTermReportColumnWeight;
												    		$TermSubjectFullMark += $thisTermSubjectFullMark;
									    				}
										    		}
										    		else {
								    					$isShowOverallMark = 0;
										    		}
										    	}
										    }
								    	}
								    }
							    }
							    else {
								    $InputMethod = "--";
							    }
					    	}
					    	else if($TagsType == 1){	# View of Weighted Mark
					    		if($WeightAry[$ReportColumnID][$SubjectID] != 0 && $WeightAry[$ReportColumnID][$SubjectID] != null && $WeightAry[$ReportColumnID][$SubjectID] != ""){
								    if($ColumnHasTemplateAry[$ReportColumnID] && $ReportType == "F" && $ScaleInput == "M"){
									    if(!empty($CmpSubjectArr) && $isCalculateByCmpSub){	// case for generate the mark of parent subject from component subjects
										    $tmpRaw = $WeightedMark[$ReportColumnID][$StudentID];
										    if($tmpRaw == "-" || $tmpRaw == "*" || $tmpRaw == "/" || $tmpRaw == "N.A."){
									    		$InputMethod = $tmpRaw;
									    		$InputMethod .= '<input type="hidden" id="'.$mark_id.'" name="'.$mark_name.'" value="'.$tmpRaw.'"/>&nbsp;';
								    		} else if($tmpRaw != "NOTCOMPLETED"){
									    		# for displaying mark with weight calculation
											    $tmpWeightedVal = $lreportcard->CONVERT_MARK_TO_WEIGHTED_MARK($ReportID, $ReportColumnID, $SubjectID, $tmpRaw, $CalculationType);
								    			//$roundVal = ($storDispSettings["SubjectScore"] != "") ? round($tmpWeightedVal, $storDispSettings["SubjectScore"]) : round($tmpWeightedVal, 0);
								    			$roundVal = $lreportcard->ROUND_MARK($tmpWeightedVal, "SubjectScore");
								    			$InputMethod = $lreportcard->CONVERT_MARK_TO_RELATIVE_DECIMAL_PLACES($roundVal, $storDispSettings["SubjectScore"]);
								    			$InputMethod .= '<input type="hidden" id="'.$mark_id.'" name="'.$mark_name.'" value="'.$roundVal.'"/>&nbsp;';
								    			
								    			# for calculating real overall subject mark
								    			//$tmpWeightedVal = $lreportcard->CONVERT_MARK_TO_WEIGHTED_MARK($ReportID, $ReportColumnID, $SubjectID, $tmpRaw, $CalculationType, $AllColumnMarkArr);
								    			$tmpWeightedVal = $lreportcard->CONVERT_MARK_TO_WEIGHTED_MARK($ReportID, $ReportColumnID, $SubjectID, $tmpRaw, 1, $AllColumnMarkArr);
								    			//$roundVal = ($storDispSettings["SubjectScore"] != "") ? round($tmpWeightedVal, $storDispSettings["SubjectScore"]) : round($tmpWeightedVal, 0);
								    			$OverallSubjectMark += $tmpWeightedVal;
							    			} else {
								    			$InputMethod = '<input type="hidden" id="'.$mark_id.'" name="'.$mark_name.'" value=""/>&nbsp;';
								    			$isShowOverallMark = 0;
							    			}
									    }
									    else {	# case for generate the mark of subject without any component subjects
										    if($SchemeType == "H"){
											   $tmpVal = $WeightedMark[$ReportColumnID][$StudentID];
									    		
									    		if($tmpVal == "-" || $tmpVal == "*" || $tmpVal == "/" || $tmpVal == "N.A."){
										    		$InputMethod = $tmpVal;
										    		$InputMethod .= '<input type="hidden" id="'.$mark_id.'" name="'.$mark_name.'" value="'.$tmpVal.'"/>&nbsp;';
									    		} else if($tmpVal != "NOTCOMPLETED" && $tmpVal != ""){
									    			# for displaying mark with weight calculation
									    			$tmpWeightedVal = $lreportcard->CONVERT_MARK_TO_WEIGHTED_MARK($ReportID, $ReportColumnID, $SubjectID, $tmpVal, $CalculationType);
										    		//$roundVal = ($storDispSettings["SubjectScore"] != "") ? round($tmpWeightedVal, $storDispSettings["SubjectScore"]) : round($tmpWeightedVal, 0);
										    		$roundVal = $lreportcard->ROUND_MARK($tmpWeightedVal, "SubjectScore");
										    		$InputMethod = $lreportcard->CONVERT_MARK_TO_RELATIVE_DECIMAL_PLACES($roundVal, $storDispSettings["SubjectScore"]);
										    		$InputMethod .= '<input type="hidden" id="'.$mark_id.'" name="'.$mark_name.'" value="'.$roundVal.'"/>&nbsp;';
										    		
										    		# for calculating real overall subject mark
										    		//$tmpWeightedVal = $lreportcard->CONVERT_MARK_TO_WEIGHTED_MARK($ReportID, $ReportColumnID, $SubjectID, $tmpVal, $CalculationType, $AllColumnMarkArr);
										    		$tmpWeightedVal = $lreportcard->CONVERT_MARK_TO_WEIGHTED_MARK($ReportID, $ReportColumnID, $SubjectID, $tmpVal, 1, $AllColumnMarkArr);
										    		//$roundVal = ($storDispSettings["SubjectScore"] != "") ? round($tmpWeightedVal, $storDispSettings["SubjectScore"]) : round($tmpWeightedVal, 0);
										    		$OverallSubjectMark += $tmpWeightedVal;
										    		
								    			} else {
									    			$InputMethod = "";
									    			$InputMethod .= '<input type="hidden" id="'.$mark_id.'" name="'.$mark_name.'" value=""/>&nbsp;';
									    			$isShowOverallMark = 0;
								    			}
								    		}
							    		}
								    }
								    else {
									    if(!empty($CmpSubjectArr) && $isCalculateByCmpSub){
										    $tmpRaw = $ParentSubMarksheetScoreInfoArr[$ReportColumnID][$StudentID];
										    
										    $SpecialCaseCountArr = $lreportcard->DIFFERENTIATE_SPECIAL_CASE($SpecialCaseCountArr, $tmpRaw, $absentExemptSettings);
										    
										    if($tmpRaw == "-" || $tmpRaw == "*" || $tmpRaw == "/" || $tmpRaw == "N.A."){
									    		$InputMethod = $tmpRaw;
									    		$InputMethod .= '<input type="hidden" id="'.$mark_id.'" name="'.$mark_name.'" value="'.$tmpRaw.'"/>&nbsp;';
								    		} else {
									    		# for displaying mark with weight calculation
									    		if ($eRCTemplateSetting['UseSumOfAssessmentToBeTheTermOverallMark'] == true)
									    			$tmpVal = $tmpRaw;
									    		else
									    			$tmpVal = $lreportcard->CONVERT_MARK_TO_WEIGHTED_MARK($ReportID, $ReportColumnID, $SubjectID, $tmpRaw, $CalculationType);
									    		//$roundVal = ($storDispSettings["SubjectScore"] != "") ? round($tmpVal, $storDispSettings["SubjectScore"]) : round($tmpVal, 0);
									    		$roundVal = $lreportcard->ROUND_MARK($tmpVal, "SubjectScore");
									    		$InputMethod = $lreportcard->CONVERT_MARK_TO_RELATIVE_DECIMAL_PLACES($roundVal, $storDispSettings["SubjectScore"]);
									    		$InputMethod .= '<input type="hidden" id="'.$mark_id.'" name="'.$mark_name.'" value="'.$roundVal.'"/>&nbsp;';
									    		
									    		# for calculating real overall subject mark
									    		if ($eRCTemplateSetting['UseSumOfAssessmentToBeTheTermOverallMark'] == true)
									    			$tmpVal = $tmpRaw;
									    		else
									    			$tmpVal = $lreportcard->CONVERT_MARK_TO_WEIGHTED_MARK($ReportID, $ReportColumnID, $SubjectID, $tmpRaw, $CalculationType, $AllColumnMarkArr);
									    		//$roundVal = ($storDispSettings["SubjectScore"] != "") ? round($tmpVal, $storDispSettings["SubjectScore"]) : round($tmpVal, 0);
									    		# SIS Cust
												if ($eRCTemplateSetting['RawMarksInsteadofRoundValue'])
									    			$OverallSubjectMark += $tmpVal;
								    			else 
									    			$OverallSubjectMark += $roundVal;
								    		}
									    }
									    else {
										   if(count($MarksheetScoreInfoArr) > 0){
									    		 if($MarkType == "SC") {
									    			$tmpVal = $MarksheetScoreInfoArr[$ReportColumnID][$StudentID]['MarkNonNum'];
									    			
									    			$SpecialCaseCountArr = $lreportcard->DIFFERENTIATE_SPECIAL_CASE($SpecialCaseCountArr, $tmpVal, $absentExemptSettings);
									    			
									    			$InputMethod = $tmpVal;
									    			$InputMethod .= '<input type="hidden" id="'.$mark_id.'" name="'.$mark_name.'" value="'.$tmpVal.'"/>&nbsp;';
								    			} 
								    			else {
									    			$tmpRaw = $MarksheetScoreInfoArr[$ReportColumnID][$StudentID]['MarkRaw'];
									    			
									    			if($tmpRaw != "" && $tmpRaw >= 0){
											    		# for displaying mark with weight calculation
											    		if ($eRCTemplateSetting['UseSumOfAssessmentToBeTheTermOverallMark'] == true)
											    			$tmpVal = $tmpRaw;
											    		else
											    			$tmpVal = $lreportcard->CONVERT_MARK_TO_WEIGHTED_MARK($ReportID, $ReportColumnID, $SubjectID, $tmpRaw, $CalculationType);
											    		
											    		//$roundVal = ($storDispSettings["SubjectScore"] != "") ? round($tmpVal, $storDispSettings["SubjectScore"]) : round($tmpVal, 0);
											    		$roundVal = $lreportcard->ROUND_MARK($tmpVal, "SubjectScore");
											    		$InputMethod = $lreportcard->CONVERT_MARK_TO_RELATIVE_DECIMAL_PLACES($roundVal, $storDispSettings["SubjectScore"]);
											    		$InputMethod .= '<input type="hidden" id="'.$mark_id.'" name="'.$mark_name.'" value="'.$roundVal.'"/>&nbsp;';
											    		
											    		# for calculating real overall subject mark
											    		//$tmpVal = $lreportcard->CONVERT_MARK_TO_WEIGHTED_MARK($ReportID, $ReportColumnID, $SubjectID, $tmpRaw, $CalculationType, $AllColumnMarkArr);
											    		if ($eRCTemplateSetting['UseSumOfAssessmentToBeTheTermOverallMark'] == true)
											    			$tmpVal = $tmpRaw;
											    		else
											    			$tmpVal = $lreportcard->CONVERT_MARK_TO_WEIGHTED_MARK($ReportID, $ReportColumnID, $SubjectID, $tmpRaw, 1, $AllColumnMarkArr);
											    		//$roundVal = ($storDispSettings["SubjectScore"] != "") ? round($tmpVal, $storDispSettings["SubjectScore"]) : round($tmpVal, 0);
											    		$OverallSubjectMark += $tmpVal;
										    		}
										    		else {
											    		$InputMethod = "";
											    		$InputMethod .= '<input type="hidden" id="'.$mark_id.'" name="'.$mark_name.'" value=""/>&nbsp;';
											    		$isShowOverallMark = 0;
										    		}
									    		}
								    		}
							    		}
						    		}
					    		}
					    		else {
								    $InputMethod = "--";
							    }
					    	}
					    	else if($TagsType == 2){	# View of Converted Grade
					    		if($WeightAry[$ReportColumnID][$SubjectID] != 0 && $WeightAry[$ReportColumnID][$SubjectID] != null && $WeightAry[$ReportColumnID][$SubjectID] != ""){
								    if($ColumnHasTemplateAry[$ReportColumnID] && $ReportType == "F"){	// according to student's term subject mark calculated by term report
								   		if(($SchemeType == "H" && $ScaleInput == "G") || $SchemeType == "PF"){
										    if(count($MarksheetOverallScoreInfoArr) > 0){
											    $tmpType = $MarksheetOverallScoreInfoArr[$ReportColumnID][$StudentID]['MarkType'];
											    $tmpNonNum = $MarksheetOverallScoreInfoArr[$ReportColumnID][$StudentID]['MarkNonNum'];
											    if($tmpType != "SC")
											    	$InputMethod = ($SchemeType == "PF") ? $SchemePassFail[$tmpNonNum] : $SchemeGrade[$tmpNonNum];
											    else 
											    	$InputMethod = $tmpNonNum;
										    }
									    }
									    else if($ScaleInput == "M"){
										    if($SchemeType == "H"){
									    		$tmpVal = $WeightedMark[$ReportColumnID][$StudentID];
									    		
									    		if($tmpVal == "+" || $tmpVal == "-" || $tmpVal == "abs"){
										    		$InputMethod = "abs";
									    		} else if($tmpVal == "/" || $tmpVal == "*" || $tmpVal == "N.A."){
										    		$InputMethod = $tmpVal;
									    		} else if($tmpVal != "NOTCOMPLETED" || $tmpVal != ""){
									    			$InputMethod = $lreportcard->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($SchemeID, $tmpVal);
								    			} else {
									    			$InputMethod = "";
								    			}
								    		}
							    		}
								    }
								    else {	# according to student's term subject mark provided by teacher without term report
							    		if(count($MarksheetScoreInfoArr) > 0){
								    		$GradeVal = '';
								    		
								    		if($ScaleInput == "G"){ 		// Input Scale is Grade
									    		$tmpVal = $MarksheetScoreInfoArr[$ReportColumnID][$StudentID]['MarkNonNum'];
										    	$GradeVal = ($MarkType == "SC") ? $tmpVal : $SchemeGrade[$tmpVal];
									    	}
									    	else if($ScaleInput == "M"){	// Input Scale is Mark
										    	$tmpRaw = $MarksheetScoreInfoArr[$ReportColumnID][$StudentID]['MarkRaw'];
										    	$tmpNonNum = $MarksheetScoreInfoArr[$ReportColumnID][$StudentID]['MarkNonNum'];
										    	if($MarkType == "SC")
										    	{
										    		$GradeVal = $tmpNonNum;
									    		}
										    	else 
										    	{
										    		$GradeVal = ($tmpRaw < 0 || $tmpRaw == "") ? "" : $lreportcard->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($SchemeID, $tmpRaw);
										    	}
									    	}
									    	
										    $InputMethod = $GradeVal;
									    }
									    else
									    {
											if ($ScaleInput=="M" && $ScaleDisplay=="G")
											{
												$thisScore = $ParentSubMarksheetScoreInfoArr[$ReportColumnID][$StudentID];
												$GradeVal = $lreportcard->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($SchemeID, $thisScore);
											}   
											$InputMethod = $GradeVal;
									    }
								    }
							    }
							    else {
								    $InputMethod = "--";
							    }
					    	}
			      		} 
			      		
						$_tdCss = ($j % 2 == 1) ? "attendanceouting" : "attendancepresent";//2014-0912-1716-13164 pt2
				        if($eRCTemplateSetting['Marksheet']['DiffCssStyleForAssessments']){ 
							$_tdCss = ($i % 2 == 0) ? $_tdCss."2" : $_tdCss;
						}
			      		$InputMethod = ($InputMethod == '')? '&nbsp;' : $InputMethod;
			      		$display .= '<td align="center" class="'.$_tdCss.' tabletext">'.$InputMethod.'</td>';
	      			} # End of Report Column Loop
		      		
		      		# [2017-0109-1818-40164] Display Current Term Result
		      		if($GetPreviousTermResult)
		      		{
			      		$display .= '<td align="center" class="'.$td_css.' tabletext">';
			      		
			      		if($isShowOverallMark)
			      		{
			      			// Handle Special Cases
				      		$setSpecialCase = 0;
							$displayNotConsideredSymbol = false;
				      		foreach($SpecialCaseCountArr as $case => $caseData){
					      		if($caseData['Count'] == count($column_data)){
					      			$setSpecialCase = 1;
					      			$setSpeicalCaseValue = $case;
				      			}
				      			
								// Student Report Column Grades - Not Considered Checking
								$thisSpecialCaseDisplay = $lreportcard->GET_MARKSHEET_SPECIAL_CASE_SET1_STRING($case);
								if($thisSpecialCaseDisplay == $eReportCard['RemarkAbsentNotConsidered'] && $caseData['Count'] > 0) {
									$displayNotConsideredSymbol = true;
									$displayOverallNotConsideredSymbol = true;
									break;
								}
				      		}
				      		
							if($displayNotConsideredSymbol) {
								$display .= $eReportCard['RemarkAbsentNotConsidered'];
							}
				      		// All Input Special Cases
				      		else if($setSpecialCase){
					      		$display .= $setSpeicalCaseValue;
			    				$OverallYearSubjectSpecialCase[] = $setSpeicalCaseValue;
				      		}
				      		else if($OverallSubjectFullMark > 0) {
				      			// Calculate Overall Subject Mark
								$OverallSubjectMark = $OverallSubjectMark / $OverallSubjectFullMark * 100;
								$OverallSubjectMark = $lreportcard->ROUND_MARK($OverallSubjectMark, "SubjectScore");
			      				$display .=  $lreportcard->CONVERT_MARK_TO_RELATIVE_DECIMAL_PLACES($OverallSubjectMark, $storDispSettings["SubjectScore"]);
				      			
			    				// Handle Term Report Score and Full Mark
		    					if($TermSubjectMark > 0 && $TermSubjectFullMark > 0)
		    					{
						    		$OverallYearSubjectMark += $TermSubjectMark;
						    		$OverallYearSubjectFullMark += $TermSubjectFullMark;
		    					}
		      				}
		      			}
		      			else {
			      			$display .= $eReportCard['NotCompleted'];
		      			}
			      		$display .= '</td>';
			      		
			      		# Display Whole Year Result
			      		if($ShowAllTermResultSummary)
			      		{
			      			if($displayOverallNotConsideredSymbol) {
								$OverallYearSubjectMark = $eReportCard['RemarkAbsentNotConsidered'];
							}
				      		else if($OverallYearSubjectMark > 0 && $OverallYearSubjectFullMark > 0) {
				      			$OverallYearSubjectMark = $OverallYearSubjectMark / $OverallYearSubjectFullMark * 100;
					      		$OverallYearSubjectMark = $lreportcard->ROUND_MARK($OverallYearSubjectMark, "SubjectScore");
								$OverallYearSubjectMark = $lreportcard->CONVERT_MARK_TO_RELATIVE_DECIMAL_PLACES($OverallYearSubjectMark, $storDispSettings["SubjectScore"]);
				      		}
				      		else {
				      			$OverallYearSubjectMark = "";
				      			
				      			// Special Case Handling
					      		if(count($OverallYearSubjectSpecialCase) == $ReportSemSeq){
					      			$OverallYearSubjectSpecialCase = array_unique($OverallYearSubjectSpecialCase);
					      			if(count($OverallYearSubjectSpecialCase) > 1){
					      				$OverallYearSubjectMark = "N.A.";
					      			}
					      			else{
					      				$OverallYearSubjectMark = $OverallYearSubjectSpecialCase[0];
					      			}
				      			}
				      		}
				      		$display .= '<td align="center" class="'.$td_css.' tabletext">';
				      		$display .= $OverallYearSubjectMark;
				      		$display .= '</td>';
			      		}
		      		}
		      		
	      			# Overall Grade - Generate Input Selection Box For ScaleInput is Grade(G) OR SchemeType is PassFail(PF)
      				if($TagsType == 0)
      				{
		      			$thisIndex = count($column_data);
			      		
			      		$classname = '';
			      		# updated on 06 Mar 2017 by Frankie
			      		# disable edit if submission is end
			      		// if($lreportcard->Is_Marksheet_Blocked($columnIDDataMap[$ReportColumnID]["BlockMarksheet"])) {
			      		if($lreportcard->Is_Marksheet_Blocked($ViewOnly) || $lreportcard->Is_Marksheet_Blocked($columnIDDataMap[$ReportColumnID]["BlockMarksheet"]))
			      		{
				        	$disabled = " disabled  ";
				        	$classname = " enableonsubmit ";
				        }
				        else
				        {
				        	$disabled = '';
				        }
			      		
			      		$classname .= " Column_0 ";
			      		$mark_id = "mark[$j][".$numOfInputBoxDisplayed."]";
			      		$mark_name = "overall_mark_".$StudentID;
						$mark_tag = 'id="'.$mark_id.'" name="'.$mark_name.'" '.$disabled .' onchange="jCHANGE_STATUS()" onpaste="isPasteContent(event,'.$j.', '.$numOfInputBoxDisplayed.')" onkeyup="isPasteContent(event, '.$j.', '.$numOfInputBoxDisplayed.')" onkeydown="isPasteContent(event, '.$j.', '.$numOfInputBoxDisplayed.')" onfocusout="jCHANGE_STATUS()" class="tabletexttoptext '.$classname.'"';
				        $otherPar = array(	'class'=>'textboxnum', 
					    					'onpaste'=>'isPasteContent(event,\''.$j.'\', \'0\')',
					    					'onkeyup'=>'isPasteContent(event, \''.$j.'\', \'0\')',
					    					'onkeydown'=>'isPasteContent(event, \''.$j.'\', \'0\')',
					    					'onchange'=>'jCHANGE_STATUS()'
					    				);
					    
				        $tmpValue = (count($MarksheetOverallScoreInfoArr) > 0) ? $MarksheetOverallScoreInfoArr[0][$StudentID]['MarkNonNum'] : "";
					    $tmpValue = ($MarksheetOverallScoreInfoArr[0][$StudentID]['IsEstimated'])? $lreportcard->Get_Estimated_Score_Display($tmpValue) : $tmpValue;
					    
					    if ($displayOverallColumnDueToAllZeroWeight && $ScaleInput == "M")
					    {
					        // [2019-0123-1731-14066] add rounding logic
					        if($eRCTemplateSetting['Marksheet']['DisplayRoundMarks'] && is_numeric($tmpValue)) {
					            $tmpValue = $lreportcard->ROUND_MARK($tmpValue, "SubjectScore");
					            $tmpValue = $lreportcard->ROUND_MARK_TO_RELATIVE_DECIMAL_PLACES($tmpValue, $ReportType, "SubjectScore");
					        }
				      		$mark_id = "mark[$j][0]";
				      		
				      		$display .= '<td align="center" class="'.$td_css.' tabletext">';
				      		$display .= $lreportcard->GET_INPUT_TEXT($mark_name, $mark_id, $tmpValue, '', $otherPar);
				      		$display .= '</td>';
				      		
				      		$ColumnSize = 1;
				      		$hasDirectInputMark = 1;
			      		}
			      		else if($SchemeType == "H" && $ScaleInput == "G")
			      		{
			      			if (!$eRCTemplateSetting['OverallGradeCalculatedFromAssesmentGPA'] || $displayOverallColumnDueToAllZeroWeight) {
				      			$display .= '<td align="center" class="'.$td_css.' tabletext">';
					      		$display .= $linterface->GET_SELECTION_BOX($InputGradeOption, $mark_tag, '', $tmpValue);
					      		$display .= '</td>';
					      		
					      		$MSwithSelectionBox = true;
			      			}
			      		}
			      		else if($SchemeType == "PF")
			      		{
				      		$display .= '<td align="center" class="'.$td_css.' tabletext">';
							$display .= $linterface->GET_SELECTION_BOX($InputPFOption, $mark_tag, '', $tmpValue);
							$display .= '</td>';
							
							$MSwithSelectionBox = true;
		      			}
		      		}
		      		else if($TagsType == 2)
		      		{
			      		$mark_name = "overall_mark_".$StudentID;
			      		$mark_id = "overall_mark_".$StudentID;
			      		$MarkType = (count($MarksheetOverallScoreInfoArr) > 0) ? $MarksheetOverallScoreInfoArr[$StudentID]['MarkType'] : "";
			      		$tmpValue = (count($MarksheetOverallScoreInfoArr) > 0) ? $MarksheetOverallScoreInfoArr[$StudentID]['MarkNonNum'] : "";
			      		
			      		if($SchemeType == "H" && $ScaleInput == "G"){
				      		$GradeVal = ($MarkType == "SC") ? $tmpValue : $SchemeGrade[$tmpValue];
				      		$display .= '<td align="center" class="'.$td_css.' tabletext">'.$GradeVal;
				      		$display .= '<input type="hidden" id="'.$mark_id.'" name="'.$mark_name.'" value="'.$GradeVal.'"/>';
				      		$display .= '</td>';
			      		}
		      		}
		      		else if($TagsType == 1)
		      		{
			      		# Generate Overall Term Subject Mark For ScaleInput is Grade OR SchemeType is PF (PassFail)
			      		$display .= '<td align="center" class="'.$td_css.' tabletext">';
			      		
			      		# Re-Assign the Overall Term Subject Mark for Term Report AND Subject having component subjects
			      		# in order to ensure the consistence of Term Subject mark with the Calculation Method in Settings
			      		if($ReportType != "F" && !empty($CmpSubjectArr)){
				      		if ($OverallTermSubjectWeightedMark[$StudentID] != 0)
				      			$OverallSubjectMark = $OverallTermSubjectWeightedMark[$StudentID];
				      		
				      		$isShowOverallMark = ((string)$OverallSubjectMark != "NOTCOMPLETED")? 1 : 0;
			      		}
			      		
			      		if($isShowOverallMark){
				      		$setSpecialCase = 0;
				      		foreach($SpecialCaseCountArr as $case => $caseData){
					      		if($caseData['Count'] == count($column_data)){
					      			$setSpecialCase = 1;
					      			$setSpeicalCaseValue = $case;
				      			}
				      		}
				      		
				      		if($setSpecialCase){
					      		$display .= $setSpeicalCaseValue;
				      		} else {
			      				//$tmpOverall = ($storDispSettings["SubjectScore"] != "") ? round($OverallSubjectMark, $storDispSettings["SubjectTotal"]) : round($OverallSubjectMark, 0);
			      				//$display .= $lreportcard->CONVERT_MARK_TO_RELATIVE_DECIMAL_PLACES($tmpOverall, $storDispSettings["SubjectTotal"]);
			      				
			      				// Get the subject overall from MS if all column's weights are zero
			      				if ($displayOverallColumnDueToAllZeroWeight)
			      				{
									$thisMSOverallScoreArr = $lreportcard->GET_MARKSHEET_OVERALL_SCORE($StudentIDArr, $SubjectID, $ReportID, $SkipSubjectGroupCheckingForGetMarkSheet);
									$OverallSubjectMark = $thisMSOverallScoreArr[$StudentID]['MarkNonNum'];
								}
								
								$tmpOverall = $lreportcard->ROUND_MARK($OverallSubjectMark, "SubjectTotal");
			      				$display .= $lreportcard->ROUND_MARK_TO_RELATIVE_DECIMAL_PLACES($tmpOverall, $ReportType, "SubjectTotal");
		      				}
		      			} else {
			      			$display .= $eReportCard['NotCompleted'];
		      			}
			      		$display .= '</td>';
		      		}
		      		
					# Add Empty Columns to make the mark input column more near to the student name
					for($i=0 ; $i<$numOfEmptyColumn ; $i++) {
						$display .= '<td align="center" class="'.$td_css.' tabletext">&nbsp;</td>';
					}
		    	}
		        $display .= '</tr>';
			}
			
			// [2015-1008-1356-44164] add hidden input fields to set excluded student score to N.A. - Whole Year Report and Overall column due to zero weight
			if($eRCTemplateSetting['DoNotDisplayStudentNotInSubjectGroupForYearMS'] && $TagsType == 0 &&
						$ReportType == "F" && $displayOverallColumnDueToAllZeroWeight && $SubjectGroupID == '' && $excludedStudentSize > 0){
				// loop excluded students
				for($es=0; $es<$excludedStudentSize; $es++){
					$currentStudentID = $excludedStudentArr[$es];
			      	$mark_name = "overall_mark_".$currentStudentID;
			    	$display .= "<input type='hidden' name='StudentID[]' value='$currentStudentID'/>";
			    	$display .= "<input type='hidden' name='$mark_name' value='N.A.'/>";
				}
			}
		}
	    $display .= '</table>';
	    
	    # Get Complete Status of Common Subject or Parent Subject
		$ProgressArr = array();
	    $ProgressArr = $lreportcard->GET_MARKSHEET_SUBMISSION_PROGRESS($SubjectID, $ReportID, $ClassID);
	    $isSubjectComplete = (count($ProgressArr) > 0) ? $ProgressArr['IsCompleted'] : 0;
	    
	    # Button
	    $display_button = '';
	    # Condition: TagsType Must be "0" - Input Raw Mark & case i
	  	# case 1: Whole Year Report & $hasDirectInputMark = 1 - allow direct input 
	  	# case 2: Term Report & subject without component subjects
	  	# case 3: Subject having Component Subjects & is Editable
	  	if($TagsType == 0 && !$ViewOnly && (
	  		$ScaleInput == "G" || 
			($ReportType == "F" && $hasDirectInputMark) || 
			($ReportType != "F" && empty($CmpSubjectArr)) || 
			(!empty($CmpSubjectArr) && $isEdit)) ){ 
				$display_button .= '<input name="Save" type="button" class="formbutton" onMouseOver="this.className=\'formbuttonon\'" onMouseOut="this.className=\'formbutton\'" value="'.$button_save.'" onclick="jSUBMIT_FORM()">&nbsp;';
				
				if ($eRCTemplateSetting['SubjectWebSAMSCodeArr']['eEnrolSubject'] != '' && $ReportType != "F") {
					$subjectMapping = $lreportcard->GET_SUBJECTS_CODEID_MAP();
					$sujbectWebSAMSCode = $subjectMapping[$SubjectID];
					
					if ($sujbectWebSAMSCode == $eRCTemplateSetting['SubjectWebSAMSCodeArr']['eEnrolSubject']) {
						$display_button .= '<input name="SynFromEnrol" type="button" class="formbutton" onMouseOver="this.className=\'formbuttonon\'" onMouseOut="this.className=\'formbutton\'" value="'.$Lang['eReportCard']['ManagementArr']['MarksheetRevisionArr']['SynchronizeFromEnrol'].'" onclick="clickedSynFromEnrolment();">&nbsp;';
					}
				}
				
				$display_button .= '<input name="ClearNA" type="button" class="formbutton" onMouseOver="this.className=\'formbuttonon\'" onMouseOut="this.className=\'formbutton\'" value="'.$eReportCard['ClearAllNA'].'"  onclick="jCLEAR_ALL_NA()">&nbsp;';
	          	$display_button .= '<input name="Reset" type="reset" class="formbutton" onMouseOver="this.className=\'formbuttonon\'" onMouseOut="this.className=\'formbutton\'" value="'.$button_reset.'">&nbsp;';
        }
        $display_button .= '<input name="Cancel" type="button" class="formbutton" onMouseOver="this.className=\'formbuttonon\'" onMouseOut="this.className=\'formbutton\'" value="'.$button_cancel.'" onClick="jBACK()">';
		
		if ($msg == "num_records_updated") {
			$SysMsg = $linterface->GET_SYS_MSG("", "<font color='green'>$SucessCount $i_con_msg_num_records_updated</font>");
		}
		else {
			$SysMsg = $linterface->GET_SYS_MSG($msg);
		}
		
		if ($eRCTemplateSetting['Marksheet']['PrintPage']) {
			$toolbarPrint = '<a href="javascript:void(0);" class="contenttool" onclick="js_Go_Print();"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_print.gif" width="18" height="18" border="0" align="absmiddle"> '.$Lang['Btn']['Print'].'</a>';
		}
		
		############################################################################################################
        
		# Tag
		$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
		$TAGS_OBJ[] = array($eReportCard['Management_MarksheetRevision'], "", 0);
		
		# Navigation
		$PAGE_NAVIGATION[] = array($eReportCard['Management_MarksheetRevision'], "javascript:jBACK(1)");
		if ($ParentSubjectID != '') {
			$PAGE_NAVIGATION[] = array($eReportCard['CmpSubjectMarksheet'], "javascript:jBACK()");
		} 
		$PAGE_NAVIGATION[] = array($eReportCard['Marksheet'], ''); 
		$PageNavigation = $linterface->GET_NAVIGATION($PAGE_NAVIGATION);
		
		if($isPrintMode && $specialPrintHandling) {
		    // do nothing
		}
		else {
            $linterface->LAYOUT_START();
		}
		
		// [2018-1105-1216-00206]
		// [2018-0207-1120-00054]
		//if($eRCTemplateSetting['Marksheet']['ApplyNewPasteMedthod']) {
		if(!$eRCTemplateSetting['Marksheet']['ApplyOldPasteMethod']) {
			echo $linterface->Include_CopyPaste_JS_CSS("2016");
		}
		else {
			echo $linterface->Include_CopyPaste_JS_CSS();
		}
		echo $linterface->Include_Excel_JS_CSS();
		echo $lreportcard_ui->Include_eReportCard_Common_JS();
?>

<script language="javascript">
var xno = "<?=$numOfInputBoxDisplayed?>"; yno = "<?=$StudentSize?>";		// set table size
var jsDefaultPasteMethod = "text";		// for copypaste.js

<?php if(!$eRCTemplateSetting['Marksheet']['ApplyOldPasteMethod']) { ?>
	var rowx = 0, coly = 0;
	var startContent = "";
	var setStart = 1;
	<?php if($MSwithSelectionBox) { ?>
// 		var jsDefaultPasteMethod = "selectedIndex";
		var jsDefaultPasteMethod = "selectedText";
	<?php } ?>
<?php } ?>

$(document).ready( function() {
	jQuery.excel('dataRow');
});

function jCLEAR_ALL_NA()
{
	$("input[value='N.A.'], input[value='N.A.#']").val('');
	$("option[value='N.A.']:selected").parent().val('');
}

/* JS Form */
<?php 
	$js_special_case = '';
	if(count($possibleSpeicalCaseSet1) > 0){
		$js_special_case .= 'var specialCaseArr = new Array(';
		for($i=0 ; $i<count($possibleSpeicalCaseSet1) ; $i++)
			$js_special_case .= ($i != 0 ) ? ', "'.$possibleSpeicalCaseSet1[$i].'"' : '"'.$possibleSpeicalCaseSet1[$i].'"' ;
		$js_special_case .= ');';
	}
	echo $js_special_case."\n";
?>

function jCHANGE_STATUS(){
	document.getElementById("isOutdated").value = 1;
}

function jCHECK_MARKSHEET_STATUS(newtagstype){
	var obj = document.getElementById('FormMain');
	var status = document.getElementById("isOutdated").value;
	
	document.getElementById("newTagsType").value = newtagstype;
	
	if(status == 1){
		if(confirm("<?=$eReportCard['SubmitMarksheetConfirm']?>")){
			obj.submit();
		}
	} else {
		var classlevelid = document.getElementById("ClassLevelID").value;
		var reportid = document.getElementById("ReportID").value;
		var subjectid = document.getElementById("SubjectID").value;
		var classid = document.getElementById("ClassID").value;
		var subjectgroupid = document.getElementById("SubjectGroupID").value;
		var params = "ClassLevelID="+classlevelid+"&ReportID="+reportid+"&SubjectID="+subjectid+"&ClassID="+classid+"&SubjectGroupID="+subjectgroupid;
		
		<?php if($isCmpSubject) { ?>
		var parent_subject_id = <?=((isset($ParentSubjectID) && $ParentSubjectID != "") ? $ParentSubjectID : "''")?>;
		params += "&ParentSubjectID="+parent_subject_id;
		<?php } ?>
		params += "&TagsType="+newtagstype;
		
		location.href = "marksheet_edit.php?"+params;
	}
}

function jCHECK_SPECIAL_CASE(scVal){
	if(specialCaseArr.length > 0){
		for(var i=0 ; i<specialCaseArr.length ; i++){
			if(scVal == specialCaseArr[i])
				return true;
		}
		return false;
	}
	else {
		return false;
	}
}

/*
* Check whether the input mark is valid or not
*/
function jCHECK_INPUT_MARK(xcor, ycor){
	var jFullMark = document.getElementById("FullMark").value;
	var obj = document.getElementById("mark["+xcor+"]["+ycor+"]");
	
	var jsScore = Trim(js_Remove_Estimate_Mark_Symbol(obj.value));
	
	if(obj && jsScore != ""){
		if(!jCHECK_SPECIAL_CASE(jsScore)){
			if(jsScore < 0){
				alert("<?=$eReportCard['jsInputMarkCannotBeNegative']?>");
				obj.focus();
				return false;
			}
			
			if(!IsNumeric(jsScore)){
				alert("<?=$eReportCard['jsInputMarkInvalid']?>");
				obj.focus();
				return false;
			}
			
			if(parseFloat(jsScore) > parseFloat(jFullMark)){
				alert("<?=$eReportCard['jsInputMarkCannotBeLargerThanFullMark']?>");
				obj.focus();
				return false;
			}
		}
	}
	return true;
}

function jCHECK_OVERALL_SUBJECT_WEIGHTED_MARK(xcor){
	var TotalWeightedMark = 0;
	var TotalColWeight = 0;
	var TotalMark = 0;
	var jFullMark = document.getElementById("FullMark").value;
	
	for(var j=0; j<xno ; j++){
		var ycor = j;
		if(document.getElementById("mark["+xcor+"]["+ycor+"]")){
			var obj = document.getElementById("mark["+xcor+"]["+ycor+"]");
			var columnID = document.getElementById("ColumnID_"+ycor).value;
			var colWeight = document.getElementById("ColumnWeight_"+columnID).value;
			TotalColWeight += parseFloat(colWeight);
			
			if(obj.value != ""){
				if(jCHECK_SPECIAL_CASE(obj.value)) 
					continue;
				if(parseFloat(colWeight) > 0){
					var tmp = parseFloat(obj.value) * parseFloat(colWeight);
					TotalMark += parseFloat(tmp);
				}
			}
		}
	}
	if(parseFloat(TotalColWeight) > 0){
		TotalWeightedMark = parseFloat(TotalMark).toFixed(5) / parseFloat(TotalColWeight).toFixed(5);
		TotalWeightedMark = TotalWeightedMark.toFixed(5);
		
		if(parseFloat(TotalWeightedMark) > parseInt(jFullMark)){
			alert("<?=$eReportCard['jsCheckOverallSubjectWeightedMark']?>");
			return false;
		}
	}
	return true;
}

<?php 
	if ($sys_custom['eRC']['Marksheet']['CopyLastColumnGradeToOverallColumn'] || getenv("SERVER_NAME") == "project6.broadlearning.com" || getenv("SERVER_NAME") == "sis-trial.broadlearning.com") { 
		if($SchemeType == "PF" || ($SchemeType == "H" && $ScaleInput == "G") ){
?>
// customized for SIS
function jCOPY_TO_OVERALL_MARK(){
	var lastReportColumnID = document.getElementById("LastReportColumnID").value;
	var studentID = document.getElementsByName("StudentID[]");
	var studentSize = studentID.length;

	if(studentSize > 0){
		for(var i=0 ; i<studentSize ; i++){
			//var lastColMarkSelectedIndex = document.getElementById("mark_"+lastReportColumnID+"_"+studentID[i].value).selectedIndex;
			//var overall = document.getElementById("overall_mark_"+studentID[i].value);
			//overall.selectedIndex = lastColMarkSelectedIndex;
			
			var jsThisStudentID = studentID[i].value;
			var targetValue = $('select[name=mark_' + lastReportColumnID + '_' + jsThisStudentID + ']').val();
			
			if (targetValue == null || targetValue == '')
				targetValue = $('#tmp_for_copy_mark_' + lastReportColumnID + '_' + jsThisStudentID).val();
				
			//if (i==studentSize-1)
			//	alert('targetValue = ' + targetValue);
			
			$('select[name=overall_mark_' + jsThisStudentID + ']').val(targetValue);
			
		}
	}
}


// End of customized for SIS
<?php 
		}
	} 
?>
function js_Copy_Grade_From_Column(FromColumnID)
{
	var targetColumnID = $("select#CopyToSelection_"+FromColumnID).val();

	for(var i=0; i<$(".Column_"+FromColumnID).length; i++)
	{
		$("select.Column_"+targetColumnID).eq(i).val($(".Column_"+FromColumnID).eq(i).val());
	}
}


// xno: ColumnSize ; yno: StudentSize
function jCHECK_FORM(){
	<?php if($SchemeType == "H" && $ScaleInput != "G"){?>
	// check for mark when mark is used for input value
	for(var i=0; i<yno ; i++){
		for(var j=0; j<xno ; j++){
			if(!jCHECK_INPUT_MARK(i, j))
				return false;
		}
	}
	
	// check for overall subject weighted mark which must be less than full mark
	for(var i=0; i<yno ; i++){
		if(!jCHECK_OVERALL_SUBJECT_WEIGHTED_MARK(i))
			return false;
	}
	<?php } ?>
	
	var isComplete = document.getElementById("isSubjectComplete").value;
	if(isComplete == 1){
		if(!confirm("<?=$eReportCard['jsConfirmMarksheetToIncomplete']?>"))
			return false;
	}
	
	return true;
}

function jSUBMIT_FORM(){
	var obj = document.getElementById('FormMain')
		
<?php 
	if ($sys_custom['eRC']['Marksheet']['CopyLastColumnGradeToOverallColumn'] || getenv("SERVER_NAME") == "project6.broadlearning.com" || getenv("SERVER_NAME") == "sis-trial.broadlearning.com") { 
		if($SchemeType == "PF" || ($SchemeType == "H" && $ScaleInput == "G") ){
?>
	// customized for SIS
	jCOPY_TO_OVERALL_MARK();
	// End of customized for SIS
<?php 
		}
	} 
?>	
	
	if(!jCHECK_FORM(obj))
		return;
	
	$(".enableonsubmit").removeAttr("disabled");
	
	obj.submit();
}

function jBACK(jsGoFirstPage){
	var form_id = <?=(($ClassLevelID != "") ? $ClassLevelID : "''")?>;
	var report_id = <?=(($ReportID != "") ? $ReportID : "''")?>;
	var subject_id = <?=(($SubjectID != "") ? $SubjectID : "''")?>;
	var isProgress = document.getElementById("isProgress").value;
	var isProgressSG = document.getElementById("isProgressSG").value;
	var status = document.getElementById("isOutdated").value;
	
	if(status == 1){
		if(<?=$ViewOnly?1:0?> || !confirm("<?=$Lang['eReportCard']['ManagementArr']['MarksheetRevisionArr']['SaveBeforeLeaveWarning']?>")){
			if(isProgress == 0 && isProgressSG == 0){
			<?php if($isCmpSubject) { ?>
				<? if ($SubjectGroupID != '') { ?>
					var subject_group_id = <?=(($SubjectGroupID != "") ? $SubjectGroupID : "''")?>;
					var parent_subject_id = <?=((isset($ParentSubjectID) && $ParentSubjectID != "") ? $ParentSubjectID : "''")?>;
					
					var jsPageURL;
					if (jsGoFirstPage==1)
						jsPageURL = 'marksheet_revision_subject_group.php';
					else
						jsPageURL = 'marksheet_revision2_subject_group.php';
						
					location.href = jsPageURL + "?ClassLevelID="+form_id+"&ReportID="+report_id+"&SubjectID="+parent_subject_id+"&SubjectGroupID="+subject_group_id;
				<? } else { ?>
					var class_id = <?=(($ClassID != "") ? $ClassID : "''")?>;
					var parent_subject_id = <?=((isset($ParentSubjectID) && $ParentSubjectID != "") ? $ParentSubjectID : "''")?>;
					location.href = "marksheet_revision2.php?ClassLevelID="+form_id+"&ReportID="+report_id+"&SubjectID="+parent_subject_id+"&ClassID="+class_id;
				<? } ?>
				
			<?php } else { ?>
				location.href = "marksheet_revision.php?ClassLevelID="+form_id+"&ReportID="+report_id+"&SubjectID="+subject_id;
			<?php } ?>
			}
			else {
				if (isProgressSG == 1) {
					location.href = "../progress/submission_sg.php";
				}
				else {
					location.href = "../progress/submission.php?ClassLevelID="+form_id+"&ReportID="+report_id;
				}
			}
		} else {
			document.getElementById("IsSaveBefCancel").value = 1;
			jSUBMIT_FORM();
		}
	} else {	
		if((isProgress == 0 || isProgress == '') && (isProgressSG == 0 || isProgressSG == '')){
		<?php if($isCmpSubject) { ?>
			<? if ($SubjectGroupID != '') { ?>
				var subject_group_id = <?=(($SubjectGroupID != "") ? $SubjectGroupID : "''")?>;
				var parent_subject_id = <?=((isset($ParentSubjectID) && $ParentSubjectID != "") ? $ParentSubjectID : "''")?>;
				
				var jsPageURL;
				if (jsGoFirstPage==1)
					jsPageURL = 'marksheet_revision_subject_group.php';
				else
					jsPageURL = 'marksheet_revision2_subject_group.php';
						
				location.href = jsPageURL + "?ClassLevelID="+form_id+"&ReportID="+report_id+"&SubjectID="+parent_subject_id+"&SubjectGroupID="+subject_group_id;	
			<? } else { ?>
				var class_id = <?=(($ClassID != "") ? $ClassID : "''")?>;
				var parent_subject_id = <?=((isset($ParentSubjectID) && $ParentSubjectID != "") ? $ParentSubjectID : "''")?>;
				location.href = "marksheet_revision2.php?ClassLevelID="+form_id+"&ReportID="+report_id+"&SubjectID="+parent_subject_id+"&ClassID="+class_id;	
			<? } ?>
			
		<?php } else { ?>
			location.href = "marksheet_revision.php?ClassLevelID="+form_id+"&ReportID="+report_id+"&SubjectID="+subject_id;
		<?php } ?>
		}
		else {
			if (isProgressSG == 1) {
				location.href = "../progress/submission_sg.php";
			}
			else {
				location.href = "../progress/submission.php?ClassLevelID="+form_id+"&ReportID="+report_id;
			}
		}
	}
}

var isUpdatedFromSubMS = 0;
function updateColumnScore(OverallScoreArr, ColumnSize, jsReportColumn)
{
	for (i=0; i<ColumnSize; i++)
	{
		var thisStudentID = OverallScoreArr[i][0];
		var thisMark = OverallScoreArr[i][1];
		thisMark = (thisMark=="---")? "" : thisMark;
		
		// format: mark_ReportColumnID_StudentID
		// var thisName = "mark_" + ReportColumnID + "_" + thisStudentID;
		var thisID = "mark[" + i + "][" + jsReportColumn + "]";
		var thisTB = document.getElementById(thisID);
		
		thisTB.value = thisMark;
	}
	
	isUpdatedFromSubMS = 1;
}

function alertMSChangedFromSubMS()
{
	if (isUpdatedFromSubMS == 1)
	{
		alert('<?=$eReportCard['jsMarkEditedFromSubMS']?>');
		isUpdatedFromSubMS = 0;
	}
}


function js_ReloadMS(targetSubjectGroupID)
{
	var jsClassLevelID = <?=$ClassLevelID?>;
	var jsReportID = <?=$ReportID?>;
	var jsSubjectID = <?=$SubjectID?>;
	
	self.location = "marksheet_edit.php?ClassLevelID=" + jsClassLevelID + "&ReportID=" + jsReportID + "&SubjectID=" + jsSubjectID + "&SubjectGroupID=" + targetSubjectGroupID;
}

function js_Go_Print() {
	var jsOriginalAction = $('form#FormMain').attr('action');
	$('form#FormMain').attr('action', '../../print_page.php').attr('target', '_blank').submit();
	
	// restore form attribute
	$('form#FormMain').attr('action', jsOriginalAction).attr('target', '_self');
}

function clickedSynFromEnrolment() {
	if (confirm('<?=$Lang['eReportCard']['ManagementArr']['MarksheetRevisionArr']['SynchronizeFromEnrolWarning']?>')) {
		Block_Document();
	
		var studentIdAry = new Array();
		$('input[name="StudentID\\[\\]"]').each( function() {
			studentIdAry[studentIdAry.length] = $(this).val();
		});
		var studentIdText = studentIdAry.join(',');
		
		$.post(
			"ajax_task.php", 
			{ 
				task: 'getEnrollmentPerformanceAverage',
				ReportID: '<?=$ReportID?>',
				StudentIDList: studentIdText
			},
			function(ReturnData) {
				var performanceAry = ReturnData.split('||');
				var numOfStudent = performanceAry.length;
				var i;
				for (i=0; i<numOfStudent; i++) {
					var studentDataAry = performanceAry[i].split('==');
					var studentId = studentDataAry[0];  
					var performance = studentDataAry[1];
					
					if (performance == '--') {
						performance = '';
					}
					
					$('input[name="overall_mark_'+ studentId +'"]').val(performance);
				}
				
				jCHANGE_STATUS();
				UnBlock_Document();
			}
		);
	}
}
</script>

<br/>
<form id="FormMain" name="FormMain" method="post" action="marksheet_edit_update.php">
<table border="0" cellspacing="0" cellpadding="0" align="right">
  <tr>
    <td><?=$SysMsg?></td>
  </tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
  	<td class="tab_underline"><?=$display_tagstype?></td>
  </tr>
</table>
<div class="content_top_tool">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	  <tr>
	  	<td>
	      <table border="0" cellspacing="0" cellpadding="2">
	      	<tr>
	      		<td>
		      		<?php 
		      		  # Condition: TagsType Must be "0" - Input Raw Mark & case i
		      		  # case 1: Whole Year Report & $hasDirectInputMark = 1 - allow direct input 
		      		  # case 2: Term Report & subject without component subjects
		      		  # case 3: Subject having Component Subjects & is Editable
		      		  if($TagsType == 0 && !$ViewOnly && (
		      		    $ScaleInput == "G" || 
		      			($ReportType == "F" && $hasDirectInputMark) || 
		      			($ReportType != "F" && empty($CmpSubjectArr)) || 
		      			(!empty($CmpSubjectArr) && $isEdit)) && $eRCTemplateSetting['DisableMarksheetImport']!==true){ 
		          			
		        	?>
	      	  			<a href="marksheet_import.php<?="?$params&TagsType=$TagsType"?>" class="contenttool"><img src="<?=$image_path.'/'.$LAYOUT_SKIN?>/icon_import.gif" width="18" height="18" border="0" align="absmiddle"> <?=$button_import?></a>
	      	  		<?php } ?>
	      	  		<a href="marksheet_export.php<?="?ClassLevelID=$ClassLevelID&SubjectID=$SubjectID&ReportID=$ReportID&ClassID=$ClassID&TagsType=$TagsType&SubjectGroupID=$SubjectGroupID"?>" class="contenttool"><img src="<?=$image_path.'/'.$LAYOUT_SKIN?>/icon_export.gif" width="18" height="18" border="0" align="absmiddle"> <?=$button_export?></a>
	      	  		<?=$toolbarPrint?>
	      		</td>
	      	</tr>
		  </table>
	 	 </td>
	  </tr>
	</table>
</div>
<br style="clear:both;" />
<table width="100%" border="0" cellpadding"0" cellspacing="0">
	<tr><td class="navigation"><?=$PageNavigation?></td></tr>
</table>

<!--PrintStart-->
<table width="100%" border="0" cellpadding="4" cellspacing="0">
  <tr>
    <td align="center">
      <table width="100%" border="0" cellspacing="0" cellpadding="2">
	  	<tr>
	      <td>
	      	<table class="form_table_v30">
	      		<tr>
					<td class="field_title"><?=$eReportCard['Settings_ReportCardTemplates']?></td>
					<td><?=$PeriodName?></td>
				</tr>
				<tr>
					<td class="field_title"><?=$eReportCard['Subject']?></td>
					<td><?=$SubjectName?></td>
				</tr>
				<tr>
					<td class="field_title"><?=$SubjectOrGroupTitle?></td>
					<td><?=$SubjectOrGroupNameDisplay?></td>
				</tr>
				<tr>
					<td class="field_title"><?=$eReportCard['SchemesFullMark']?></td>
					<td><?=((count($SchemeMainInfo) > 0 && $SchemeType != "PF") ? $SchemeMainInfo['FullMark']: $Lang['General']['EmptySymbol'])?></td>
				</tr>
				<?=$SubjectTeacherTr?>
	      	</table>
	      	<br style="clear:both;" />
	      	
	      	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	          <tr>
	          	<td align="right" class="tabletextremark"><?=(($SchemeType == "PF") ? $eReportCard['MarkRemindSet2'] : ($eRCTemplateSetting['Marksheet']['EstimatedMark'])? $eReportCard['MarkRemindSet1_WithEstimate'] : $eReportCard['MarkRemindSet1'])?></td>
	          </tr>
	      	</table>
	      </td>
	  	</tr>
	  	<tr>
	      <td>
	      <!-- Start of Content -->
	      <?=$display?>
	      <!-- End of Content -->
	      </td>
	  	</tr>
	  </table>
	</td>
  </tr>
</table>
<!--PrintEnd-->

<br>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td height="1" class="dotline"><img src="<?=$image_path.'/'.$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
  </tr>
  <tr>
    <td>
      <table width="100%" border="0" cellspacing="0" cellpadding="2">
        <tr>
          <td align="left" valign="bottom"><span class="tabletextremark"><?=$eReportCard['DeletedStudentLegend']?></span></td>
        </tr>
        <tr>
          <td align="center" valign="bottom"><?=$display_button?></td>
        </tr>
  	  </table>
    </td>
  </tr>
</table>

<input type="hidden" name="isProgress" id="isProgress" value="<?=$isProgress?>"/>
<input type="hidden" name="isProgressSG" id="isProgressSG" value="<?=$isProgressSG?>"/>
<input type="hidden" name="ClassLevelID" id="ClassLevelID" value="<?=$ClassLevelID?>"/>
<input type="hidden" name="ReportID" id="ReportID" value="<?=$ReportID?>"/>
<input type="hidden" name="SubjectID" id="SubjectID" value="<?=$SubjectID?>"/>
<input type="hidden" name="SchemeID" id="SchemeID" value="<?=$SchemeID?>"/>
<input type="hidden" name="ClassID" id="ClassID" value="<?=$ClassID?>"/>
<input type="hidden" name="SubjectGroupID" id="SubjectGroupID" value="<?=$SubjectGroupID?>"/>

<input type="hidden" name="isSubjectComplete" id="isSubjectComplete" value="<?=$isSubjectComplete?>"/>
<input type="hidden" name="isOutdated" id="isOutdated" value="0"/>
<input type="hidden" name="newTagsType" id="newTagsType" value=""/>
<input type="hidden" name="IsSaveBefCancel" id="IsSaveBefCancel" value="0"/>

<?php
if(isset($ParentSubjectID) && $ParentSubjectID != "")
	echo '<input type="hidden" name="ParentSubjectID" id="ParentSubjectID" value="'.$ParentSubjectID.'"/>'."\n";

if(count($column_data) > 0){
	for($i=0 ; $i<count($column_data) ; $i++)
		echo '<input type="hidden" name="ReportColumnID[]" id="ReportColumnID_'.$i.'" value="'.$column_data[$i]['ReportColumnID'].'"/>'."\n";
		
	echo '<input type="hidden" name="LastReportColumnID" id="LastReportColumnID" value="'.$column_data[count($column_data)-1]['ReportColumnID'].'"/>';
}
?>

<input type="hidden" name="FullMark" id="FullMark" value="<?=((count($SchemeMainInfo) > 0) ? $SchemeMainInfo['FullMark'] : '""')?>"/>
<input type="hidden" name="isAllColumnWeightZero" id="isAllColumnWeightZero" value="<?=$isAllColumnWeightZero?>" />

<textarea style="height: 1px; width: 1px; visibility:hidden;" id="text1" name="text1"></textarea>

</form>

<?

        if($isPrintMode && $specialPrintHandling) {
            // do nothing
        }
        else {
            $linterface->LAYOUT_STOP();
        }
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>