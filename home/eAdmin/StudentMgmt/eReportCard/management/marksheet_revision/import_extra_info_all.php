<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
$lreportcard = new libreportcardcustom();
$linterface = new interface_html();
	
$CurrentPage = "Management_MarkSheetRevision";
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
	
$lreportcard->hasAccessRight();

$reportId = integerSafe(standardizeFormGetValue($_GET['ReportID']));
$classlevelId = integerSafe(standardizeFormGetValue($_GET['ClassLevelID']));
$subjectId = integerSafe(standardizeFormGetValue($_GET['SubjectID']));
$type = standardizeFormGetValue($_GET['type']);
$returnMsgKey = standardizeFormGetValue($_GET['returnMsgKey']);

		
if ($eRCTemplateSetting['Marksheet']['ManualInputGrade']) {
	$column_data = $lreportcard->returnReportTemplateColumnData($reportId);
	$dataTitle = $column_data[0]['ColumnTitle'].' Grade';
}
else {
	$dataTitle = $eReportCard['ExtraInfoLabel'];
}


# tag information
$TAGS_OBJ[] = array($eReportCard['Management_MarksheetRevision'], "", 0);
# page navigation (leave the array empty if no need)
if ($type=="AdjustPosition") {
	$PAGE_NAVIGATION[] = array($button_import." ".$eReportCard['AdjustPositionLabal']);
}
else {
	$PAGE_NAVIGATION[] = array($button_import." ".$dataTitle);
}
$htmlAry['navigation'] = $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION);

$returnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];
$linterface->LAYOUT_START($returnMsg);


### step display
$htmlAry['generalImportStepTbl'] = $linterface->GET_IMPORT_STEPS($CurrStep=1);


# report title
$ReportInfo = $lreportcard->returnReportTemplateBasicInfo($reportId);
$ReportTitle = str_replace(":_:", "<br>", $ReportInfo['ReportTitle']);


# report info display
$x = '';
$x .= '<table class="form_table_v30">'."\r\n";
	// Date picker
	$x .= '<tr>'."\r\n";
		$x .= '<td class="field_title">'.$eReportCard['Settings_ReportCardTemplates'].'</td>'."\r\n";
		$x .= '<td>'."\r\n";
			$x .= $ReportTitle;
		$x .= '</td>'."\r\n";
	$x .= '</tr>'."\r\n";
	$x .= '<tr>'."\r\n";
		$x .= '<td class="field_title">'.$i_select_file.'</td>'."\r\n";
		$x .= '<td>'."\r\n";
			$x .= '<input class="file" type="file" name="userfile">'."\r\n";
			$x .= '<br><br>'."\r\n";
			$x .= '<a class="tablelink" href="export_extra_info_all.php?ReportID='.$reportId.'&type='.$type.'&isTemplate=1" target="_blank">'."\r\n";
				$x .= '<img src="'.$PATH_WRT_ROOT.'/images/'.$LAYOUT_SKIN.'/icon_files/xls.gif" border="0" align="absmiddle" hspace="3">'."\r\n";
				$x .= $eReportCard['DownloadCSVFile']."\r\n";
			$x .= '</a>'."\r\n";
		$x .= '</td>'."\r\n";
	$x .= '</tr>'."\r\n";
$x .= '</table>'."\r\n";
$htmlAry['reportInfoTbl'] = $x;


### action buttons
$htmlAry['submitBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Submit'], "button", "goSubmit()", 'submitBtn', $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");
$htmlAry['cancelBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Cancel'], "button", "goCancel()", 'cancelBtn', $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");

?>
<script type="text/JavaScript" language="JavaScript">
function trim(str) {
	return str.replace(/^\s+|\s+$/g,"");
}

function goCancel() {
	window.location = 'marksheet_revision_subject_group.php?ReportID=<?=$reportId?>&ClassLevelID=<?=$classlevelId?>&SubjectID=<?=$subjectId?>';
}

function goSubmit() {
	var canSubmit = true;
	
	obj = document.form1;
	if (trim(obj.elements["userfile"].value) == "") {
		canSubmit = false;
		alert('<?=$eReportCard['AlertSelectFile']?>');
		obj.elements["userfile"].focus();
	}
	
	if (canSubmit) {
		obj.submit();
	}
}
</script>
<form id="form1" name="form1" action="import_extra_info_all_validate.php" method="POST" enctype="multipart/form-data">
<?=$htmlAry['navigation']?>
	<p class="spacer"></p>
	<br />
	
	<?=$htmlAry['generalImportStepTbl']?>
	
	<div class="table_board">
		<?=$htmlAry['reportInfoTbl']?>
		<?=$htmlAry['errorTbl']?>
		<br style="clear:both;" />
		
		<div class="edit_bottom_v30">
			<p class="spacer"></p>
			<?=$htmlAry['submitBtn']?>
			<?=$htmlAry['cancelBtn']?>
			<p class="spacer"></p>
		</div>
	</div>
	<input type="hidden" name="ClassLevelID" id="ClassLevelID" value="<?=$classlevelId?>"/>
	<input type="hidden" name="ReportID" id="ReportID" value="<?=$reportId?>"/>
	<input type="hidden" name="SubjectID" id="SubjectID" value="<?=$subjectId?>"/>
	<input type="hidden" name="type" id="type" value="<?=$type?>"/>
</form>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>