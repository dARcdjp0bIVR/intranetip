<?php
// Using : 

/***********************************************
 *  20190523 Bill     [2019-0522-0908-45206]
 *      - Prevent SQL Injection + Cross-site Scripting
 *      - Change Input Fields 'extraInfoID' to 'extraInfoIDArr' to prevent IntegerSafe()
 *  20190201 Bill     [2019-0321-1136-13207]
 * 	    - Support Paste from Excel (Firefox & Chrome)
 * 	20170519 Bill
 * 	    - Allow View Group to access (view only)
 * *********************************************/
 
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping_ui.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if ($plugin['ReportCard2008'])
{
	include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
	if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") {
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
		$lreportcard = new libreportcardcustom();
	}
	else {
		$lreportcard = new libreportcard();
	}
	
	$CurrentPage = "Management_MarkSheetRevision";
	$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
	
    if ($lreportcard->hasAccessRight())
    {
        ### Handle SQL Injection + XSS [START]
        $ClassLevelID = IntegerSafe($ClassLevelID);
        $ReportID = IntegerSafe($ReportID);
        $SubjectID = IntegerSafe($SubjectID);
        $ClassID = IntegerSafe($ClassID);
        $SubjectGroupID = IntegerSafe($SubjectGroupID);
        
        $type = cleanCrossSiteScriptingCode($type);
        ### Handle SQL Injection + XSS [END]
        
        $linterface = new interface_html();
        
        if ($eRCTemplateSetting['Marksheet']['ManualInputGrade']) {
        	$column_data = $lreportcard->returnReportTemplateColumnData($ReportID);
			$dataTitle = $column_data[0]['ColumnTitle'].' '.$eReportCard['Grade'];
		}
		else {
			$dataTitle = $eReportCard['ExtraInfoLabel'];
		}
		
		// View Group - View Only
		if($ck_ReportCard_UserType == "VIEW_GROUP") {
			$ViewOnly = true;
		}
		############################################################################################################
		
		/*
		if (!isset($ClassLevelID, $SubjectID, $ReportID, $ClassID)) {
			header("Location: marksheet_revision.php");
			exit();
		}
		*/
		
		// Class
		$ClassArr = $lreportcard->GET_CLASSES_BY_FORM($ClassLevelID);
		if (sizeof($ClassArr) > 0)
		{
			for($i=0; $i<count($ClassArr); $i++) {
				if($ClassArr[$i][0] == $ClassID) {
					$ClassName = $ClassArr[$i][1];
				}
			}
			
			// Subject
			$SubjectName = $lreportcard->GET_SUBJECT_NAME($SubjectID);
			
			// Period (Term or Whole Year)
			$ReportInfo = $lreportcard->GET_REPORT_TYPES($ClassLevelID, $ReportID);
			$PeriodName = $ReportInfo['SemesterTitle'];
			
			// Student Info Array
			if ($SubjectGroupID != '')
			{
				$StudentArray = $lreportcard->GET_STUDENT_BY_SUBJECT_GROUP($SubjectGroupID, $ClassLevelID, '', 1);
				$showClassName = true;
				
				$obj_SubjectGroup = new subject_term_class($SubjectGroupID);
				$SubjectGroupName = $obj_SubjectGroup->Get_Class_Title();
				
				$TitleDisplay = "<td>".$Lang['SysMgr']['SubjectClassMapping']['SubjectGroup']." : <strong>".$SubjectGroupName."</strong></td>";
			}
			else
			{
				if ($lreportcard->IS_ADMIN_USER($_SESSION['UserID']) == true || $lreportcard->IS_VIEW_GROUP_USER_VIEW($_SESSION['UserID']) == true || $lreportcard->Is_Class_Teacher($_SESSION['UserID'], $ClassID) == true)
				{
					# Admin / Class Teacher => Show all students
					$TaughtStudentList = '';
				}
				else
				{
					# Subject Teacher => Show taught students only
					$TargetSubjectID = ($ParentSubjectID != '')? $ParentSubjectID : $SubjectID;
					$TaughtStudentArr = $lreportcard->Get_Student_List_Taught_In_This_Consolidated_Report($ReportID, $_SESSION['UserID'], $ClassID, $TargetSubjectID);
					$TaughtStudentList = '';
					if (count($TaughtStudentArr > 0)) {
						$TaughtStudentList = implode(',', $TaughtStudentArr);
					}
				}
				$StudentArray = $lreportcard->GET_STUDENT_BY_CLASS($ClassID, $TaughtStudentList, 1);
				
				$showClassName = false;
				$TitleDisplay = "<td>".$eReportCard['Class']." : <strong>".$ClassName."</strong></td>";
			}
			$StudentSize = sizeof($StudentArray);
		    
			//$StudentArray = $lreportcard->GET_STUDENT_BY_CLASS($ClassID, '', 1);
			//$StudentSize = sizeof($StudentArray);
			
			// Student ID list of students in a class
			for($i=0; $i<$StudentSize; $i++) {
				$StudentIDArray[] = $StudentArray[$i][0];
			}
			if(!empty($StudentIDArray)) {
				$StudentIDList = implode(",", $StudentIDArray);
			}
			
			if ($type=="AdjustPosition")
			{
				# pending
				$StudentExtraInfo = $lreportcard->GET_MANUAL_ADJUSTED_POSITION($StudentIDArray,$SubjectID,$ReportID);
			}
			else	// extra info
			{
				$StudentExtraInfo = $lreportcard->GET_EXTRA_SUBJECT_INFO($StudentIDArray,$SubjectID,$ReportID);
			}
		}
		
		$display = '<table class="yui-skin-sam" width="95%" cellpadding="5" cellspacing="0">';
		$display .= '<tr>
			          <td width="10%" align="center" class="tablegreentop tabletopnolink">'.$eReportCard['StudentNo_short'].'</td>
			          <td width="25%" class="tablegreentop tabletopnolink">'.$eReportCard['Student'].'</td>
					  <td class="tablegreentop tabletopnolink">&nbsp;</td>';
		
		if ($type == "AdjustPosition")
		{
			$display .= '<td width="15%" class="tablegreentop tabletopnolink">'.$eReportCard['AdjustPositionLabal'].'</td>';
		}
		else	    // extra info
		{
			$display .= '<td width="15%" class="tablegreentop tabletopnolink">'.$dataTitle.'</td>';
		}
		
		# Add Empty Columns to make the mark input column more near to the student name
		$minNumColumn = ($lreportcard->MS_MinimunColumn)? $lreportcard->MS_MinimunColumn : 0;
		$numOfEmptyColumn = $minNumColumn - 1;
		for($i=0; $i<$numOfEmptyColumn; $i++)
		{
			$display .= '<td align="left" class="tablegreentop tabletopnolink" width="12%">&nbsp;</td>';
		}
		
	    $display .= '</tr>';
	    
	    ### Check if the Extra Info is Drop down list / Textbox
	    $OptionArr = array();
	    $DefaultOptionArr = ($type=="AdjustPosition")? $lreportcard->ExtraInfoOptionArr['AdjustPosition'] : $lreportcard->ExtraInfoOptionArr['Effort'];
	    if ($DefaultOptionArr != '') {
	    	$isDropDownList = true;
	    	$OptionArr[] = array('', Get_Selection_First_Title($eReportCard['Template']['SelectEffort']));
	    	foreach ((array)$DefaultOptionArr as $thisEffortValue => $thisEffortDisplay) {
	    		$OptionArr[] = array($thisEffortValue, $thisEffortDisplay);
	    	}
	    }
	    else {
	    	$isDropDownList = false;
	    }
	    
	    # Student Content of Marksheet Revision
		if (isset($StudentSize) && $StudentSize > 0)
		{
		    for($j=0; $j<$StudentSize; $j++){
				list($StudentID, $WebSAMSRegNo, $StudentNo, $StudentName, $ClassName) = $StudentArray[$j];
				$tr_css = ($j % 2 == 1) ? "tablegreenrow2" : "tablegreenrow1";
				$td_css = ($j % 2 == 1) ? "attendanceouting" : "attendancepresent";
				
				if ($showClassName) {
					$thisClassDisplay = $ClassName.'-'.$StudentNo;
				} else {
					$thisClassDisplay = $StudentNo;
				}
				
				if (isset($StudentExtraInfo[$StudentID])) {
					$textfieldName = "extraInfoIDArr[".$StudentExtraInfo[$StudentID]["ExtraInfoID"]."]";
					$textfieldID = "mark[$j][0]";
					$textfieldValue = stripslashes($StudentExtraInfo[$StudentID]["Info"]);
				} else {
					$textfieldName = "extraInfo[$StudentID]";
					$textfieldID = "mark[$j][0]";
					$textfieldValue = "";
				}
				
				$otherPar = array(	'onpaste'=>'pasteContent(\''.$j.'\', \'0\')',
			    					'onkeyup'=>'isPasteContent(event, \''.$j.'\', \'0\')',
			    					'onkeypress'=>'isPasteContent(event, \''.$j.'\', \'0\');',
			    					'onkeydown'=>'isPasteContent(event, \''.$j.'\', \'0\')'
			    				);
			    
			    if($ViewOnly) {
		        	$disabled = " disabled  ";
		        	//$classname = " enableonsubmit ";
		        	$otherPar['disabled'] ='disabled';
		        	$otherPar['class'] ='textboxnum enableonsubmit';
			    }
			    
				if ($isDropDownList) {
					# Selection
					$mark_tag = 'id="'.$textfieldID.'" name="'.$textfieldName.'" '.$disabled .' onpaste="pasteTable('.$j.', 0)" onkeyup="isPasteContent(event, '.$j.', 0)" onkeydown="isPasteContent(event, '.$j.', 0)"';
					$thisInput = $linterface->GET_SELECTION_BOX($OptionArr, $mark_tag, '', $textfieldValue);
				}
				else {
					# Textbox
					$otherPar['class'] = 'textboxnum';
					$thisInput = $lreportcard->GET_INPUT_TEXT($textfieldName, $textfieldID, $textfieldValue, '', $otherPar);
				}
				
				$display .= '<tr class="'.$tr_css.' dataRow">
			          <td align="center" valign="top" class="'.$td_css.' tabletext">'.$thisClassDisplay.'</td>
		          	  <td class="'.$td_css.' tabletext" valign="top">'.$StudentName.'</td>
					  <td class="'.$td_css.' tabletext" valign="top">&nbsp;</td>
					  <td class="'.$td_css.' tabletext" valign="top">';
					//$display .= "<input type='text' name='$textfieldName' id='$textfieldID' class='textboxnum' value='$textfieldValue' onpaste='pasteContent($j, 0)' />";
					$display .= $thisInput;
					$display .= '</td>';
					
					for($i=0; $i<$numOfEmptyColumn; $i++)
					{
						$display .= '<td align="left" class="'.$td_css.' tabletext" width="12%">&nbsp;</td>';
					}
				$display .= '</tr>';
			} 	
		    $display .= '</table>';
			
			$redirectParm = "ReportID=$ReportID&ClassLevelID=$ClassLevelID&SubjectID=$SubjectID";
			$redirectLocation = "marksheet_revision.php?$redirectParm";
			
			if(!$ViewOnly) {
				$display_button .= $linterface->GET_ACTION_BTN($button_save, "button", "checkForm()")."&nbsp";
	       		$display_button .= $linterface->GET_ACTION_BTN($button_reset, "reset", "")."&nbsp";
			}
			$display_button .= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='$redirectLocation'");
		}
		else
		{
			$td_colspan = "3";
			$display .=	'<tr class="tablegreenrow1" height="40">
							<td class="tabletext" colspan="'.$td_colspan.'" align="center">'.$i_no_record_exists_msg.'</td>
						</tr>';
		}
		
		if ($Result == "num_records_updated") {
			$SysMsg = $linterface->GET_SYS_MSG("", "<font color='green'>$SucessCount $i_con_msg_num_records_updated</font>");
		} else {
			$SysMsg = $linterface->GET_SYS_MSG($Result);
		}
		############################################################################################################
        
		if ($type == "AdjustPosition")
		{
			$PAGE_NAVIGATION[] = array($eReportCard['AdjustPositionLabal']);
		}
		else	// extra info
		{
			$PAGE_NAVIGATION[] = array($dataTitle);
		}
		
		# Tag
		$TAGS_OBJ[] = array($eReportCard['Management_MarksheetRevision'], "", 0);
		$linterface->LAYOUT_START();
		
		if(!$eRCTemplateSetting['Marksheet']['ApplyOldPasteMethod']) {
		    echo $linterface->Include_CopyPaste_JS_CSS("2016");
		} else {
		    echo $linterface->Include_CopyPaste_JS_CSS();
		}
		echo $linterface->Include_Excel_JS_CSS();
		
		$parameters = "ClassID=$ClassID&SubjectID=$SubjectID&ReportID=$ReportID&ClassLevelID=$ClassLevelID&type=$type&SubjectGroupID=$SubjectGroupID";
?>

<script type="text/javascript">
var rowx = 0, coly = 0;						// init
var xno = 1; yno = "<?=$StudentSize?>";		// set table size

var startContent = "";
var setStart = 1;

//var jsDefaultPasteMethod = "value";
var jsDefaultPasteMethod = 'text';			// for copy and paste js
<?php if(!$eRCTemplateSetting['Marksheet']['ApplyOldPasteMethod'] && $isDropDownList) { ?>
	var jsDefaultPasteMethod = "selectedText";
<?php } ?>

$(document).ready( function() {
	jQuery.excel('dataRow');
});

function checkForm() {
	document.form1.submit();
}
</script>

<br/>
<form name="form1" method="post" action="extra_info_update.php" onSubmit="return false">
<div align="left" style="width:95%">
<table border="0" cellpadding="5" cellspacing="0" class="tabletext">
	<tr>
		<td><?=$eReportCard['Period']?> : <strong><?=$PeriodName?></strong></td>
		<?= $TitleDisplay ?>
		<td><?=$eReportCard['Subject']?> : <strong><?=$SubjectName?></strong></td>
	</tr>
</table>
</div>
<br />
<table width="98%" border="0" cellspacing="5" cellpadding="0">
	<tr>
		<td align="left" colspan="2">
			<?=$linterface->GET_NAVIGATION($PAGE_NAVIGATION)?>
		</td>
	</tr>
</table>

<? if ($type=="AdjustPosition") { ?>
	<table width="98%" border="0" cellspacing="5" cellpadding="0">
		<tr>
			<td align="center" colspan="2">
				<?=$eReportCard['ManualAdjustMarkInstruction']?>
			</td>
		</tr>
	</table>
<? } ?>

<table width="95%" border="0" cellspacing="5" cellpadding="0">
	<tr>
		<td>
			<table border="0" cellspacing="0" cellpadding="0">
				<tr>
				<? if(!$ViewOnly) { ?>
					<td>
						<a href="import_extra_info.php?<?=$parameters?>" class="contenttool">
							<img src="<?=$image_path.'/'.$LAYOUT_SKIN?>/icon_import.gif" width="18" height="18" border="0" align="absmiddle"> <?=$button_import?>
						</a>
					</td>
					<td><img src="<?=$image_path.'/'.$LAYOUT_SKIN?>/10x10.gif"></td>
				<? } ?>
					<td><a href="export_extra_info.php?<?=$parameters?>" class="contenttool"><img src="<?=$image_path.'/'.$LAYOUT_SKIN?>/icon_export.gif" width="18" height="18" border="0" align="absmiddle"> <?=$button_export?></a></td>
				</tr>
			</table>
		</td>
		<td align="right"><?= $SysMsg ?></td>
	</tr>
</table>
<?=$display?>
<table width="95%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td height="1" class="dotline"><img src="<?=$image_path.'/'.$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
	</tr>
	<tr>
		<td>
			<table width="100%" border="0" cellspacing="0" cellpadding="2">
				<tr>
					<td align="center" valign="bottom"><?=$display_button?></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<textarea style="height:1px; width:1px; visibility:hidden;" id="text1" name="text1" ></textarea>

<input type="hidden" name="ClassLevelID" id="ClassLevelID" value="<?=$ClassLevelID?>"/>
<input type="hidden" name="ReportID" id="ReportID" value="<?=$ReportID?>"/>
<input type="hidden" name="SubjectID" id="SubjectID" value="<?=$SubjectID?>"/>
<input type="hidden" name="ClassID" id="ClassID" value="<?=$ClassID?>"/>
<input type="hidden" name="SubjectGroupID" id="SubjectGroupID" value="<?=$SubjectGroupID?>"/>

<input type="hidden" name="Semester" id="Semester" value="<?=$ReportInfo['Semester']?>"/>
<input type="hidden" name="type" id="type" value="<?=$type?>"/>

</form>
<?
		intranet_closedb();
        $linterface->LAYOUT_STOP();
    } else {
    ?>
You have no priviledge to access this page.
    <?
    }
} else {
?>
You have no priviledge to access this page.
<?
}
?>