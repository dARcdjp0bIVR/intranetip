<?php
/***************************************************
 * 	Modification log:
 * 20201103 (Bill)      [2020-0915-1830-00164]
 *      - support term setting    ($eRCTemplateSetting['Settings']['SubjectTopics_TermSettings'])
 * 20170519 Bill
 * 		- Allow View Group to access (view only)
 * 20161113 Bill:
 * 		- BIBA Cust
 * 		- display "-" (Teacher Comment) for all BIBA Report
 * 20160112 Bill:	[2015-0924-1821-07164]
 * 		- Wah Yan, Kowloon Cust
 * 		- add Extra Info for subject component
 * 20151026 Bill:
 * 		- BIBA Cust
 * 		- display "-" (Teacher Comment) for KG Report
 * 20151013 Bill:
 * 		- BIBA Cust
 * 		- display "-" (Marksheet, Feedback) for ES Progress Report
 * 20150505 Bill:
 * 		- BIBA Cust
 * 		- display "-" and link to marksheet_subject_topic_edit.php if component subject with subject topics
 * 		- edit js function jEDIT_MARKSHEET() to redirect to marksheet_subject_topic_edit.php
 * 20120403 Marcus:
 * 		- Allow teacher to view marks even after submission period
 * 		- 2012-0208-1800-43132 - 沙田循道衛理中學 - Edit mark after submit the marksheet 
 * *************************************************/
 
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
//include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping_ui.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if ($plugin['ReportCard2008'])
{
	/* Temp Library*/
	include_once($PATH_WRT_ROOT."includes/libreportcard2008j.php");
	$lreportcard = new libreportcard2008j();
	//$lreportcard = new libreportcard();
	include_once($PATH_WRT_ROOT."includes/libreportcard2008_schedule.php");
	$lreportcard_schedule = new libreportcard_schedule();
	
	$CurrentPage = "Management_MarkSheetRevision";
	$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
    if ($lreportcard->hasAccessRight())
    {
        $linterface = new interface_html();
        $PAGE_TITLE = $eReportCard['Management_MarksheetRevision'];
        
        if($ck_ReportCard_UserType=="TEACHER")
		{
			$PAGE_TITLE = $eReportCard['Management_MarksheetSubmission'];
		}
		
		// Student Info Array
		if ($SubjectGroupID != '')
		{
			$StudentArr = $lreportcard->GET_STUDENT_BY_SUBJECT_GROUP($SubjectGroupID, $ClassLevelID, '', 1, 1);
			$SubjectGroupStudentList = Get_Array_By_Key($StudentArr,"UserID");
			unset($StudentArr);
			
			$obj_SubjectGroup = new subject_term_class($SubjectGroupID);
			$SubjectGroupName = $obj_SubjectGroup->Get_Class_Title();
			//$TitleDisplay = "<td>".$Lang['SysMgr']['SubjectClassMapping']['SubjectGroup']." : <strong>".$SubjectGroupName."</strong></td>";
			
			$SubjectOrGroupTitle = $Lang['SysMgr']['SubjectClassMapping']['SubjectGroup'];
			$SubjectOrGroupNameDisplay = $SubjectGroupName;
			
			$SubjectComponentID = $obj_SubjectGroup->SubjectComponentID;
		}
		else
		{
			//$TitleDisplay = "<td>".$eReportCard['Class']." : <strong>".$ClassName."</strong></td>";
			$SubjectOrGroupTitle = $eReportCard['Class'];
			$SubjectOrGroupNameDisplay = $ClassName;
		}
        
		############################################################################################################
		
		# Get component subject(s) if any
		$CmpSubjectArr = $lreportcard->GET_COMPONENT_SUBJECT($SubjectID, $ClassLevelID,'','',$ReportID);
		
		if(empty($CmpSubjectArr)){	// If no any component subjects, redirect
			$param = "SubjectID=$SubjectID&SubjectGroupID=$SubjectGroupID&ClassLevelID=$ClassLevelID&ReportID=$ReportID";
			header("Location: marksheet_edit.php?$param");
		}
		else {
		
			# Get Data
			// Period (Term x, Whole Year, etc)
			$PeriodArr = $lreportcard->GET_REPORT_TYPES($ClassLevelID, $ReportID);
			$PeriodName = (count($PeriodArr) > 0) ? $PeriodArr['SemesterTitle'] : '';
		
			// Class
			$ClassArr = $lreportcard->GET_CLASSES_BY_FORM($ClassLevelID);
			for($i=0 ; $i<count($ClassArr) ;$i++){
				if($ClassArr[$i][0] == $SubjectGroupID)
					$ClassName = $ClassArr[$i][1];
			}

			/*
			// BIBA Cust - get SubjectID with subject topics
			if($eRCTemplateSetting['Settings']['SubjectTopics']){
				$SubjectTopicArr = $lreportcard->CHECK_SUBJECT_WITH_SUBJECT_TOPICS($ClassLevelID);
			}
		    */

            // Loading Report Template Basic Data
            $ReportBasicInfo = $lreportcard->returnReportTemplateBasicInfo($ReportID);
            $SemID = $ReportBasicInfo['Semester'];
            $SemNum = $lreportcard->Get_Semester_Seq_Number($SemID);

			// BIBA Cust - check report type
			if($eRCTemplateSetting['Settings']['AutoSelectReportType']){
				//$ReportBasicInfo = $lreportcard->returnReportTemplateBasicInfo($ReportID);
				$isProgressReport = !$ReportBasicInfo['isMainReport'];
				
				$FormNumber = $lreportcard->GET_FORM_NUMBER($ClassLevelID);
				// ES/MS Report
				if (is_numeric($FormNumber)) {
					$FormNumber = intval($FormNumber);
					$isESReport = $FormNumber > 0 && $FormNumber < 6;
					$isMSReport = !$isESReport;
				}
				// KG Report
				else {
					$isKGReport = true;
				}
			}

            // [2020-0915-1830-00164]
            $targetTermSeq = 0;
            if ($eRCTemplateSetting['Settings']['SubjectTopics_TermSettings']) {
                $targetTermSeq = ($SemNum == 1 || $SemNum == 2) ? 1 : 2;
            }

            // BIBA Cust - get SubjectID with subject topics
            if($eRCTemplateSetting['Settings']['SubjectTopics'])
            {
                // [2020-0915-1830-00164]
                //$SubjectTopicArr = $lreportcard->CHECK_SUBJECT_WITH_SUBJECT_TOPICS($ClassLevelID);
                $SubjectTopicArr = $lreportcard->CHECK_SUBJECT_WITH_SUBJECT_TOPICS($ClassLevelID, '', $targetTermSeq);
            }
		
			// Parent Subject
			$SubjectName = $lreportcard->GET_SUBJECT_NAME($SubjectID);
					
			// Component Subject Data
			$count = 1;
			$CmpSubjectDataArr = array();
			for($i=0 ; $i<count($CmpSubjectArr) ; $i++){
				$CmpSubjectFormGradingArr = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $CmpSubjectArr[$i]['SubjectID'],0,0,$ReportID);
				
				$CmpSubjectDataArr[$i] = $CmpSubjectArr[$i];
				$CmpSubjectDataArr[$i]['SchemeID'] = $CmpSubjectFormGradingArr['SchemeID'];
			}
			
			// used to check whether there is any marksheet feedback which is not closed.
			$MarksheetFeedback = $lreportcard->Get_Marksheet_Feedback_Info($ReportID);
			$MarksheetFeedback = BuildMultiKeyAssoc($MarksheetFeedback, array("StudentID","SubjectID"),array("RecordStatus"),1);
			
			# Other Preparation
			# Filters - By Type (Term1, Term2, Whole Year, etc)
			// Get Semester Type from the reportcard template corresponding to ClassLevelID
			$ReportTypeSelection = '';
			$ReportTypeArr = array();
			$ReportTypeOption = array();
			$ReportTypeArr = $lreportcard->GET_REPORT_TYPES($ClassLevelID);
			
			$ReportType = '';			
			if(count($ReportTypeArr) > 0){
				for($j=0 ; $j<count($ReportTypeArr) ; $j++){
					if($ReportTypeArr[$j]['ReportID'] == $ReportID){
						$isFormReportType = 1;
						$ReportType = $ReportTypeArr[$j]['Semester'];
					}
					
					$ReportTypeOption[$j][0] = $ReportTypeArr[$j]['ReportID'];
					$ReportTypeOption[$j][1] = $ReportTypeArr[$j]['SemesterTitle'];
				}
				//$ReportTypeSelection .= $linterface->GET_SELECTION_BOX($ReportTypeOption, 'name="ReportType" id="ReportType" class="tabletexttoptext" onchange="jCHANGE_MARKSHEET_REVISION()"', '', $ReportID);
				
				$ReportType = ($ReportType != "") ? $ReportType : $ReportTypeArr[0]['Semester'];
			}
			$ReportID = ($isFormReportType) ? $ReportID : $ReportTypeOption[0][0];
			
			# Get Last Modified Info of This Report Template(Date-Time & Modified By who)
			$ReportColumnIDArr = array();
			$ReportColumnIDList = '';
			$column_data = $lreportcard->returnReportTemplateColumnData($ReportID); 	// Column Data
			if(count($column_data) > 0){
				for($i=0 ; $i<count($column_data) ; $i++)
					$ReportColumnIDArr[] = $column_data[$i]['ReportColumnID'];
				$ReportColumnIDList = implode(",", $ReportColumnIDArr);
			}
			for($i=0 ; $i<count($CmpSubjectArr) ; $i++){
				$LastModifiedArr[$CmpSubjectArr[$i]['SubjectID']] = $lreportcard->GET_SUBJECT_GROUP_MARKSHEET_LAST_MODIFIED($CmpSubjectArr[$i]['SubjectID'], $ReportColumnIDList, '', '', $SubjectID);
				$CommentLastModifiedArr[$CmpSubjectArr[$i]['SubjectID']] = $lreportcard->GET_SUBJECT_GROUP_MARKSHEET_SUBJECT_COMMENT_LAST_MODIFIED($CmpSubjectArr[$i]['SubjectID'], $ReportColumnIDList, '', '', $SubjectID);
			}
						
			
			# Get Status of Completed Action
			# $PeriodAction = "Submission" or "Verification"
			$PeriodAction = "Submission";
			$PeriodType = $lreportcard->COMPARE_REPORT_PERIOD($ReportID, $PeriodAction);
			$SpecialPeriodType = $lreportcard_schedule->compareSpecialMarksheetSubmissionPeriodForTeacher($ReportID, $_SESSION['UserID'], $SubjectID);
			if($eRCTemplateSetting['Management']['Schedule']['SpecificMarksheetSubmissionPeriod_subjectGroupBased'])
				$SpecialPeriodType = $lreportcard_schedule->compareSpecialMarksheetSubmissionPeriodForTeacher($ReportID, $_SESSION['UserID'], $SubjectID, $SubjectGroupID);
			if ($SpecialPeriodType == 1) {
				$PeriodType = 1;
			}
			
			# Array of Selection Box of Complete Function 
			$CompleteArr = array();
			$CompleteArr[] = array(1, $eReportCard['Confirmed']);
			$CompleteArr[] = array(0, $eReportCard['NotCompleted']);
			
			$CompleteAllArr = array();
			$CompleteAllArr[] = array("", "---".$eReportCard['ChangeAllTo']."---");
			$CompleteAllArr[] = array("completeAll", $eReportCard['Confirmed']);
			$CompleteAllArr[] = array("notCompleteAll", $eReportCard['NotCompleted']);
			
			
			# Component Subject List
			$AllSubjectColumnWeightStatus = array();
			$display = '';
			$cnt = 0;
			if(count($CmpSubjectDataArr) > 0){
				for($j=0 ; $j<count($CmpSubjectDataArr) ; $j++){
					list($CmpSubjectID, $CmpSubjectName, $CmpSchemeID) = $CmpSubjectDataArr[$j];
					
					# Show the specific component if the subject group is mapped to a component subject
					if ($SubjectComponentID!='' && $SubjectComponentID!=0 && $SubjectComponentID!=$CmpSubjectID)
						continue;
					
					$row_css = ($cnt % 3 == 1) ? "attendanceouting" : "attendancepresent";
					
					# Check whether all column weights are zero or not corresponding to each subject of a report
					$isAllColumnWeightZero = $lreportcard->IS_ALL_REPORT_COLUMN_WEIGHTS_ZERO($ReportID, $CmpSubjectID);
					$AllSubjectColumnWeightStatus[] = $isAllColumnWeightZero;
					
					# Check whether the marksheet is editable or not 
					$isEdit = $lreportcard->CHECK_MARKSHEET_STATUS($CmpSubjectID, $ClassLevelID, $ReportID);
					
					// BIBA Cust - check if component subject with subject topics
					$thisSubjectWithTopic = false;
					if($eRCTemplateSetting['Settings']['SubjectTopics'] && !$thisSubjectWithTopic && isset($SubjectTopicArr[$CmpSubjectID][1])){
						$thisSubjectWithTopic = true;
					}
					
					# Last Modified Info
					$LastModifiedDate = '-';
					$LastModifiedBy = '-';
					if(!empty($LastModifiedArr)){
						$LastModifiedDate = (isset($LastModifiedArr[$CmpSubjectID][$SubjectGroupID][0])) ? $LastModifiedArr[$CmpSubjectID][$SubjectGroupID][0] : $LastModifiedDate;
						$LastModifiedBy = (isset($LastModifiedArr[$CmpSubjectID][$SubjectGroupID][1])) ? $LastModifiedArr[$CmpSubjectID][$SubjectGroupID][1] : $LastModifiedBy;
						
						$SubjectCommentLastModifiedDate = (isset($CommentLastModifiedArr[$CmpSubjectID][$SubjectGroupID][0])) ? $CommentLastModifiedArr[$CmpSubjectID][$SubjectGroupID][0] : $LastModifiedDate;
						$SubjectCommentLastModifiedBy = (isset($CommentLastModifiedArr[$CmpSubjectID][$SubjectGroupID][1])) ? $CommentLastModifiedArr[$CmpSubjectID][$SubjectGroupID][1] : $LastModifiedBy;
						if ($SubjectCommentLastModifiedDate != '' && $SubjectCommentLastModifiedDate > $LastModifiedDate) {
							$LastModifiedDate = $SubjectCommentLastModifiedDate;
							$LastModifiedBy = $SubjectCommentLastModifiedBy;
						}
					}
					
					# check whether there is any marksheet feedback which is not closed.
					$InProgressRemarks = '';
					foreach((array)$SubjectGroupStudentList as $thisStudentID )
					{
						if($MarksheetFeedback[$thisStudentID][$CmpSubjectID]==2)
						{
							$InProgressRemarks = "<span class='tabletextrequire'>*</span>";
							break;
						}	
					}
					
					# State of IsCompleted 
					$ProgressArr = array();
					$ProgressArr = $lreportcard->GET_MARKSHEET_SUBMISSION_PROGRESS($CmpSubjectID, $ReportID, $SubjectGroupID);
					
					# Initialization - Display Icon
					$CmpSubjectIcon = "";
					$MarksheetIcon = "-";
					$TCommentIcon = "-";
					$FeedbackIcon = "-";
					# Added on 201601112 by Bill
					$ExtraInfoIcon = "-";
					
					if($isAllColumnWeightZero == 0){	// if Any Report Column Weight is not Zero
						if($ck_ReportCard_UserType == "TEACHER")
						{
							switch($PeriodType)
							{
								case -1:	 	# Period not set yet (NULL)
								case 0:			# before the Period
									break;
								case 1:			# within  the Period
									//$MarksheetIcon = ($CmpSubjectDataArr[$j]['SchemeID'] != null && $CmpSubjectDataArr[$j]['SchemeID'] != "") ? '<a href="javascript:jEDIT_MARKSHEET(\''.$SubjectGroupID.'\', \''.$CmpSubjectID.'\')"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_edit_b.gif" width="20" height="20" border="0" alt="'.$button_edit.' '.$eReportCard['Marksheet'].'"></a>' : 'X';
									if($CmpSubjectDataArr[$j]['SchemeID'] != null && $CmpSubjectDataArr[$j]['SchemeID'] != "")
									{
								    	$MarksheetIcon = '<a href="javascript:jEDIT_MARKSHEET(\''.$SubjectGroupID.'\', \''.$CmpSubjectID.'\', 1)">';
								    	$MarksheetIcon .= ($ReportType != "F" || ($isEdit && $ReportType == "F") ) ? '<img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_edit_b.gif" width="20" height="20" border="0" alt="'.$button_edit.' '.$eReportCard['Marksheet'].'">' : '<img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_view.gif" width="20" height="20" border="0" alt="'.$button_view.' '.$eReportCard['Marksheet'].'">';
								    	$MarksheetIcon .= '</a>';
										// BIBA Cust - redirect to correct page if component subject with subject topics
								    	if($thisSubjectWithTopic){
								    		$MarksheetIcon = '<a href="javascript:jEDIT_MARKSHEET(\''.$SubjectGroupID.'\', \''.$CmpSubjectID.'\', 3)">';
									    	$MarksheetIcon .= $ReportType!="F"? '<img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_edit_b.gif" width="20" height="20" border="0" alt="'.$button_edit.' '.$eReportCard['Marksheet'].'">' : '<img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_view.gif" width="20" height="20" border="0" alt="'.$button_view.' '.$eReportCard['Marksheet'].'">';
									    	$MarksheetIcon .= '</a>';
								    	}
										// BIBA Cust for ES Progress Report - display "-"
										if($isESReport && $isProgressReport){
											$MarksheetIcon = "-";
										}
							    	}
							    	else {
								    	$MarksheetIcon = 'X';
							    	}
	 								$TCommentIcon = '<a href="javascript:jEDIT_TEACHER_COMMENT(\''.$SubjectGroupID.'\', \''.$CmpSubjectID.'\')"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_edit_b.gif" width="20" height="20" border="0" alt="'.$button_edit.' '.$eReportCard['TeacherComment'].'"></a>';
									$FeedbackIcon = '<a href="#"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_view.gif" width="20" height="20" border="0" alt="'.$button_view.' '.$eReportCard['Feedback'].'"></a>'.$InProgressRemarks;
									// BIBA Cust - display "-" if component subject with subject topics / ES Progress Report 
							    	if($thisSubjectWithTopic || ($isESReport && $isProgressReport)){
							    		$FeedbackIcon = "-";
							    	}									
									$ExtraInfoIcon = '<a href="javascript:jEDIT_EXTRA_INFO(\''.$SubjectGroupID.'\', \''.$CmpSubjectID.'\')"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_edit_b.gif" width="20" height="20" border="0" title="'.$button_edit.' '.$eReportCard['ExtraInfoLabel'].'"></a>';
									break;
								case 2:			# after the Period
// 									$MarksheetIcon = ($CmpSubjectDataArr[$j]['SchemeID'] != null && $CmpSubjectDataArr[$j]['SchemeID'] != "") ? '<a href="javascript:jEDIT_MARKSHEET(\''.$SubjectGroupID.'\', \''.$CmpSubjectID.'\')"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_view.gif" width="20" height="20" border="0" alt="'.$button_preview.' '.$eReportCard['Marksheet'].'"></a>' : 'X';
// 									$TCommentIcon = '<a href="javascript:jEDIT_TEACHER_COMMENT(\''.$SubjectGroupID.'\', \''.$CmpSubjectID.'\')"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_view.gif" width="20" height="20" border="0" alt="'.$button_preview.' '.$eReportCard['TeacherComment'].'"></a>';
// 							    	$FeedbackIcon = '<a href="#"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_view.gif" width="20" height="20" border="0" alt="'.$button_preview.' '.$eReportCard['Feedback'].'"></a>';
							    	
// 							    	$MarksheetIcon = ($CmpSubjectDataArr[$j]['SchemeID'] != null && $CmpSubjectDataArr[$j]['SchemeID'] != "") ? '-' : 'X';
//									$TCommentIcon = '-';

									if($CmpSubjectDataArr[$j]['SchemeID'] != null && $CmpSubjectDataArr[$j]['SchemeID'] != "")
									{
								    	$MarksheetIcon = '<a href="javascript:jEDIT_MARKSHEET(\''.$SubjectGroupID.'\', \''.$CmpSubjectID.'\', 1)">';
								    	$MarksheetIcon .= ($ReportType != "F" || ($isEdit && $ReportType == "F") ) ? '<img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_edit_b.gif" width="20" height="20" border="0" alt="'.$button_edit.' '.$eReportCard['Marksheet'].'">' : '<img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_view.gif" width="20" height="20" border="0" alt="'.$button_view.' '.$eReportCard['Marksheet'].'">';
								    	$MarksheetIcon .= '</a>';
										// BIBA Cust - redirect to correct page if component subject with subject topics
								    	if($thisSubjectWithTopic){
								    		$MarksheetIcon = '<a href="javascript:jEDIT_MARKSHEET(\''.$SubjectGroupID.'\', \''.$CmpSubjectID.'\', 3)">';
									    	$MarksheetIcon .= $ReportType!="F"? '<img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_edit_b.gif" width="20" height="20" border="0" alt="'.$button_edit.' '.$eReportCard['Marksheet'].'">' : '<img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_view.gif" width="20" height="20" border="0" alt="'.$button_view.' '.$eReportCard['Marksheet'].'">';
									    	$MarksheetIcon .= '</a>';
								    	}
										// BIBA Cust for ES Progress Report - display "-"
										if($isESReport && $isProgressReport){
											$MarksheetIcon = "-";
										}
								    	
								    	$FeedbackIcon = '<a href="javascript:jVIEW_FEEDBACK(\''.$SubjectGroupID.'\', \''.$CmpSubjectID.'\')"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_view.gif" width="20" height="20" border="0" title="'.$button_preview.' '.$eReportCard['Feedback'].'"></a>'.$InProgressRemarks;
								    	// BIBA Cust - display "-" if component subject with subject topics / ES Progress Report
								    	if($thisSubjectWithTopic || ($isESReport && $isProgressReport)){
								    		$FeedbackIcon = "-";
								    	}
							    	}
							    	else {
								    	$MarksheetIcon = 'X';
								    	$FeedbackIcon = 'X';
							    	}
//	 								$TCommentIcon = '<a href="javascript:jEDIT_TEACHER_COMMENT(\''.$SubjectGroupID.'\', \''.$CmpSubjectID.'\')"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_edit_b.gif" width="20" height="20" border="0" alt="'.$button_edit.' '.$eReportCard['TeacherComment'].'"></a>';
									

									break;							}
						}
						else
						{
							//$MarksheetIcon = ($CmpSubjectDataArr[$j]['SchemeID'] != null && $CmpSubjectDataArr[$j]['SchemeID'] != "") ? '<a href="javascript:jEDIT_MARKSHEET(\''.$SubjectGroupID.'\', \''.$CmpSubjectID.'\')"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_edit_b.gif" width="20" height="20" border="0" alt="'.$button_edit.' '.$eReportCard['Marksheet'].'"></a>' : 'X';
							if($CmpSubjectDataArr[$j]['SchemeID'] != null && $CmpSubjectDataArr[$j]['SchemeID'] != "")
							{
						    	//$MarksheetIcon = '<a href="javascript:jEDIT_MARKSHEET(\''.$SubjectGroupID.'\', \''.$CmpSubjectID.'\', 1)">';
						    	//$MarksheetIcon .= ($ReportType != "F" || ($isEdit && $ReportType == "F") ) ? '<img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_edit_b.gif" width="20" height="20" border="0" alt="'.$button_edit.' '.$eReportCard['Marksheet'].'">' : '<img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_view.gif" width="20" height="20" border="0" alt="'.$button_view.' '.$eReportCard['Marksheet'].'">';
						    	//$MarksheetIcon .= '</a>';
						    	
						    	$showEditLink = false;
						    	$showViewLink = false;
						    	if ($ReportType != "F") {
						    		// term report
						    		if ($isEdit) {
						    			$showEditLink = true;
						    		}
						    	}
						    	else {
						    		// consolidated report
						    		if ($isEdit) {
						    			$showEditLink = true;
						    		}
						    		else {
						    			$showViewLink = true;
						    		}
						    	}
								
								if($ck_ReportCard_UserType == "VIEW_GROUP") {
									$showEditLink = false;
									$showViewLink = true;
								}
						    	
						    	if ($showEditLink) {
						    		$MarksheetIcon = '<a href="javascript:jEDIT_MARKSHEET(\''.$SubjectGroupID.'\', \''.$CmpSubjectID.'\', 1)">';
						    			$MarksheetIcon .= '<img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_edit_b.gif" width="20" height="20" border="0" alt="'.$button_edit.' '.$eReportCard['Marksheet'].'">';
						    		$MarksheetIcon .= '</a>';
									// BIBA Cust - redirect to correct page if component subject with subject topics
							    	if($thisSubjectWithTopic){
							    		$MarksheetIcon = '<a href="javascript:jEDIT_MARKSHEET(\''.$SubjectGroupID.'\', \''.$CmpSubjectID.'\', 3)">';
								    		$MarksheetIcon .= '<img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_edit_b.gif" width="20" height="20" border="0" alt="'.$button_edit.' '.$eReportCard['Marksheet'].'">';
							    		$MarksheetIcon .= '</a>';
							    	}
									// BIBA Cust for ES Progress Report - display "-"
									if($isESReport && $isProgressReport){
										$MarksheetIcon = "-";
									}
						    	}
						    	else if ($showViewLink) {
						    		$MarksheetIcon = '<a href="javascript:jEDIT_MARKSHEET(\''.$SubjectGroupID.'\', \''.$CmpSubjectID.'\', 1)">';
						    			$MarksheetIcon .= '<img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_view.gif" width="20" height="20" border="0" alt="'.$button_view.' '.$eReportCard['Marksheet'].'">';
						    		$MarksheetIcon .= '</a>';
						    		// BIBA Cust - redirect to correct page if component subject with subject topics
						    		if($thisSubjectWithTopic){
							    		$MarksheetIcon = '<a href="javascript:jEDIT_MARKSHEET(\''.$SubjectGroupID.'\', \''.$CmpSubjectID.'\', 3)">';
								    		$MarksheetIcon .= '<img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_view.gif" width="20" height="20" border="0" alt="'.$button_view.' '.$eReportCard['Marksheet'].'">';
							    		$MarksheetIcon .= '</a>';
							    	}
									// BIBA Cust for ES Progress Report - display "-"
									if($isESReport && $isProgressReport){
										$MarksheetIcon = "-";
									}
						    	}
						    	else {
						    		$MarksheetIcon = 'X';
						    	}
					    	}
					    	else {
						    	$MarksheetIcon = 'X';
					    	}
					    	
					    	if($ck_ReportCard_UserType == "VIEW_GROUP")
					    		$TCommentIcon = '<a href="javascript:jEDIT_TEACHER_COMMENT(\''.$SubjectGroupID.'\', \''.$CmpSubjectID.'\')"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_view.gif" width="20" height="20" border="0" alt="'.$button_view	.' '.$eReportCard['TeacherComment'].'"></a>';
					    	else
								$TCommentIcon = '<a href="javascript:jEDIT_TEACHER_COMMENT(\''.$SubjectGroupID.'\', \''.$CmpSubjectID.'\')"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_edit_b.gif" width="20" height="20" border="0" alt="'.$button_edit.' '.$eReportCard['TeacherComment'].'"></a>';
							
							$FeedbackIcon = '<a href="javascript:jVIEW_FEEDBACK(\''.$SubjectGroupID.'\', \''.$CmpSubjectID.'\')"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_view.gif" width="20" height="20" border="0" alt="'.$button_view.' '.$eReportCard['Feedback'].'"></a>'.$InProgressRemarks;
							// BIBA Cust - display "-" if component subject with subject topics / ES Progress Report
					    	if($thisSubjectWithTopic || ($isESReport && $isProgressReport)){
					    		$FeedbackIcon = "-";
					    	}
					    	$ExtraInfoIcon = '<a href="javascript:jEDIT_EXTRA_INFO(\''.$SubjectGroupID.'\', \''.$CmpSubjectID.'\')"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_edit_b.gif" width="20" height="20" border="0" title="'.$button_edit.' '.$eReportCard['ExtraInfoLabel'].'"></a>';
						}
						// BIBA Report - display "-" for Teacher Comment
						if($eRCTemplateSetting['Settings']['AutoSelectReportType']){
							$TCommentIcon = "-";
						}
					}
					
					// Extra Info Display - for Manual Input Grade Only
					if ($eRCTemplateSetting['Marksheet']['ManualInputGrade']) {
						$ExtraInfoIcon = '<td align="center" class="'.$row_css.' tabletext">'. $ExtraInfoIcon .'</td>';
					}
					else{
						$ExtraInfoIcon = "";
					}
					
					# Content
					$display .= '
						<tr class="tablegreenrow1">
				          <td class="'.$row_css.' tabletext">'.$CmpSubjectName.'</td>
				          <td align="center" class="'.$row_css.' tabletext">
				          '. $MarksheetIcon .'
				          </td>
				          <td align="center" class="'.$row_css.' tabletext">'. $TCommentIcon .'</td>
				          <td align="center" class="'.$row_css.' tabletext">'. $FeedbackIcon .'</td>
						  '.$ExtraInfoIcon.'
				          <td class="'.$row_css.' tabletext">'.$LastModifiedDate.'</td>
				          <td class="'.$row_css.' tabletext">'.$LastModifiedBy.'</td>';
				     
				     # Complete Status Function 
				     /*
				     $display .= '<td align="center" class="'.$row_css.' tabletext">';
				     if($isAllColumnWeightZero == 0){	// if Any Report Column Weight is not Zero  
					     if($ck_ReportCard_UserType == "TEACHER"){
						     if($PeriodType != 1){	// within submission period
						     	if($PeriodType <= 0){
							     	$display .= "-";
						     	} else {
							     	if(count($ProgressArr) > 0 && $ProgressArr['IsCompleted']){	// show imcompleted status
							     		$display .= $eReportCard['Confirmed'];
							     	} else {	// show completed status
							     		$display .= $eReportCard['NotCompleted'];
							     	}
						  		}   	
						     } else {
							     $display .= $linterface->GET_SELECTION_BOX($CompleteArr, "name='complete_".$CmpSubjectID."_".$SubjectGroupID."' id='complete_".$CmpSubjectID."_".$SubjectGroupID."' class='tabletexttoptext'", "", ((count($ProgressArr) > 0) ? $ProgressArr['IsCompleted'] : 0));
						     }
					     } else if($ck_ReportCard_UserType == "ADMIN"){
					     	$display .= $linterface->GET_SELECTION_BOX($CompleteArr, "name='complete_".$CmpSubjectID."_".$SubjectGroupID."' id='complete_".$CmpSubjectID."_".$SubjectGroupID."' class='tabletexttoptext'", "", ((count($ProgressArr) > 0) ? $ProgressArr['IsCompleted'] : 0));
				     	 }
			     	 } else {
				     	 $display .= '-';
			     	 }
				     $display .= '</td>';
				     */
				     $display .= '</tr>';
				      	
				    $cnt++;
				}
			}
			
			/*
			# Check Box Master of "Completed"
			if($ck_ReportCard_UserType == "TEACHER"){
				if($PeriodType != 1){	// within submission period
					$completeAll = '';
				}
				else {	// out of submission period
					$completeAll .= $linterface->GET_SELECTION_BOX($CompleteAllArr, 'name="completeAll" id="completeAll" class="tabletexttoptext" onchange="setCompleteAll(\''.$SubjectID.'\')"', "", "");
				}
			}
			else if($ck_ReportCard_UserType == "ADMIN"){
				$completeAll .= $linterface->GET_SELECTION_BOX($CompleteAllArr, 'name="completeAll" id="completeAll" class="tabletexttoptext" onchange="setCompleteAll(\''.$SubjectID.'\')"', "", "");
			}
			*/
			
			$total = $eReportCard['Total']." ".$cnt;
		}
		
		# Button
		$display_button = '';
		/*
		if($cnt > 0){
			if(in_array(0, $AllSubjectColumnWeightStatus)){
				$display_button .= '<input name="Save" type="button" class="formbutton" onMouseOver="this.className=\'formbuttonon\'" onMouseOut="this.className=\'formbutton\'" value="'.$button_save.'" onclick="jSUBMIT_FORM()">';
				$display_button .= '<input name="Reset" type="button" class="formbutton" onMouseOver="this.className=\'formbuttonon\'" onMouseOut="this.className=\'formbutton\'" value="'.$button_reset.'" onclick="jRESET_FORM()">';
			}
		}
		*/
		$display_button .= '<input name="Back" type="button" class="formbutton" onMouseOver="this.className=\'formbuttonon\'" onMouseOut="this.className=\'formbutton\'" value="'.$button_back.'" onClick="jBACK()">';
		
		$ExportBtn = $linterface->Get_Content_Tool_v30("export", "javascript:js_Export_Feedback();",$eReportCard['ManagementArr']['MarksheetRevisionArr']['MarksheetFeedbackArr']['ExportFeedback']);
		
		
		############################################################################################################
        
		# tag information
		$TAGS_OBJ[] = array($PAGE_TITLE, "", 0);
		
		# Navigation
		$PAGE_NAVIGATION[] = array($eReportCard['Management_MarksheetRevision'], "javascript:jBACK()"); 
		$PAGE_NAVIGATION[] = array($eReportCard['CmpSubjectMarksheet'], ''); 
		$PageNavigation = $linterface->GET_NAVIGATION($PAGE_NAVIGATION);
		
		$linterface->LAYOUT_START();
		
?>

<script language="javascript">
function jGET_SELECTBOX_SELECTED_VALUE(objName){
	var obj = document.getElementById(objName);
	var index = obj.selectedIndex;
	var val = obj[index].value;
	return val;
}

function setCompleteAll(parSubjectID){
	var classIDArr = document.getElementsByName("ClassIDList[]");
	var subjectIDArr = (document.getElementsByName("SubjectIDList[]")) ? document.getElementsByName("SubjectIDList[]") : "";
	var indexToSelect = "";
	
	var parStatus = jGET_SELECTBOX_SELECTED_VALUE("completeAll");
	if (parStatus == "completeAll"){
		indexToSelect = 0;
	}
	else if (parStatus == "notCompleteAll"){
		indexToSelect = 1;
	}
	
	if(parStatus != ""){
		if(classIDArr.length > 0 && subjectIDArr.length > 0){
			for(var i=0 ; i<classIDArr.length ; i++){
				for(var j=0 ; j<subjectIDArr.length ; j++){
					if(document.getElementById("complete_"+subjectIDArr[j].value+"_"+classIDArr[i].value)){
						var obj = document.getElementById("complete_"+subjectIDArr[j].value+"_"+classIDArr[i].value);
						obj.selectedIndex = indexToSelect;
						obj.options[indexToSelect].selected = true;
					}
				}
			}
		}
	}
}

function jCHANGE_MARKSHEET_REVISION(){
	var form_id = <?=(($ClassLevelID != "") ? $ClassLevelID : "''")?>;
	var report_id = jGET_SELECTBOX_SELECTED_VALUE("ReportType");
	var subject_id = <?=(($SubjectID != "") ? $SubjectID : "''")?>;
	var subject_group_id = <?=(($SubjectGroupID != "") ? $SubjectGroupID : "''")?>;
	
	if(subject_id != "" && subject_group_id != "" && form_id != "" && report_id != ""){
		location.href = "?ClassLevelID="+form_id+"&ReportID="+report_id+"&SubjectID="+subject_id+"&SubjectGroupID="+subject_group_id;
	}
}

function jEDIT_MARKSHEET(subject_group_id, target_subject_id, jType){
	var form_id = <?=(($ClassLevelID != "") ? $ClassLevelID : "''")?>;
	var report_id = <?=(($ReportID != "") ? $ReportID : "''")?>;
	var subject_group_id = <?=(($SubjectGroupID != "") ? $SubjectGroupID : "''")?>;
	var parent_subject_id = <?=(($SubjectID != "") ? $SubjectID : "''")?>;
	
	report_id = (report_id == "") ? jGET_SELECTBOX_SELECTED_VALUE("ReportType") : report_id;
	
	<?php if($eRCTemplateSetting['Settings']['SubjectTopics']) { ?>
		if(jType == 3 && target_subject_id != "" && subject_group_id != "" && form_id != "" && report_id != ""){
			location.href = "marksheet_subject_topic_edit.php?ClassLevelID="+form_id+"&ReportID="+report_id+"&SubjectID="+target_subject_id+"&SubjectGroupID="+subject_group_id+"&ParentSubjectID="+parent_subject_id;
		} 
		else if(target_subject_id != "" && subject_group_id != "" && form_id != "" && report_id != ""){
			location.href = "marksheet_edit.php?ClassLevelID="+form_id+"&ReportID="+report_id+"&SubjectID="+target_subject_id+"&SubjectGroupID="+subject_group_id+"&ParentSubjectID="+parent_subject_id;
		}
	<?php } else {?> 
		if(target_subject_id != "" && subject_group_id != "" && form_id != "" && report_id != ""){
			location.href = "marksheet_edit.php?ClassLevelID="+form_id+"&ReportID="+report_id+"&SubjectID="+target_subject_id+"&SubjectGroupID="+subject_group_id+"&ParentSubjectID="+parent_subject_id;
		}
	<?php } ?> 
}

function jEDIT_TEACHER_COMMENT(subject_group_id, target_subject_id){
	var form_id = <?=(($ClassLevelID != "") ? $ClassLevelID : "''")?>;
	var report_id = <?=(($ReportID != "") ? $ReportID : "''")?>;
	var subject_group_id = <?=(($SubjectGroupID != "") ? $SubjectGroupID : "''")?>;
	
	report_id = (report_id == "") ? jGET_SELECTBOX_SELECTED_VALUE("ReportType") : report_id;
	
	if(target_subject_id != "" && subject_group_id != "" && form_id != "" && report_id != ""){
		location.href = "teacher_comment.php?ClassLevelID="+form_id+"&ReportID="+report_id+"&SubjectID="+target_subject_id+"&SubjectGroupID="+subject_group_id;
	}
}

function jEDIT_EXTRA_INFO(subject_group_id, target_subject_id){
	var form_id = <?=(($ClassLevelID != "") ? $ClassLevelID : "''")?>;
	var report_id = <?=(($ReportID != "") ? $ReportID : "''")?>;
	//var subject_id = <?=(($SubjectID != "") ? $SubjectID : "''")?>;
	
	//report_id = (report_id == "") ? jGET_SELECTBOX_SELECTED_VALUE("ReportType") : report_id;
	subject_id = target_subject_id;
	
	location.href = "extra_info.php?ClassLevelID="+form_id+"&ReportID="+report_id+"&SubjectID="+subject_id+"&SubjectGroupID="+subject_group_id;
}

function jBACK(){
	var form_id = <?=(($ClassLevelID != "") ? $ClassLevelID : "''")?>;
	var report_id = <?=(($ReportID != "") ? $ReportID : "''")?>;
	var subject_id = <?=(($SubjectID != "") ? $SubjectID : "''")?>;
	
	location.href = "marksheet_revision.php?ClassLevelID="+form_id+"&ReportID="+report_id+"&SubjectID="+subject_id;
}

/* Form */
function jRESET_FORM(){
	var obj = document.FormMain;
	obj.reset();
}

function jSUBMIT_FORM(){
	var obj = document.FormMain;
	obj.submit();
}
/* End Form */

function jCOMPLETE_MARKSHEET(subject_group_id, target_subject_id, isAll){
	var jCheckBoxObj = (isAll == 1) ? document.getElementById("completeAll") : document.getElementById("complete_"+target_subject_id+"_"+subject_group_id);
	var jCheck = jCheckBoxObj.checked;
	var action = (jCheckBoxObj.value == 0) ? "COMPLETE" : "CANCEL";
	var msg = (jCheckBoxObj.value == 0) ? "<?=$eReportCard['jsAlertToCompleteMarksheet']?>" : "<?=$eReportCard['jsAlertToIncompleteMarksheet']?>";
		
	if(confirm(msg))
		completeMarksheet(subject_group_id, target_subject_id, isAll, action);
	else {
		jCheckBoxObj.checked = (jCheckBoxObj.value == 1) ? true : false;
	}
}

// Start of AJAX 
var callback = {
    success: function ( o )
    {
    	alert(o.responseText);
    	jUpdateCompleteCheckBox(o.responseXML);
    }
}
// Main
function completeMarksheet(subject_group_id, target_subject_id, isAll, action)
{
	var classlevel_id = <?=(($ClassLevelID != "") ? $ClassLevelID : "''")?>;
	var report_id = <?=(($ReportID != "") ? $ReportID : "''")?>;
	var subject_id = target_subject_id;

	report_id = (report_id == "") ? jGET_SELECTBOX_SELECTED_VALUE("ReportType") : report_id;
	
	var params = '';
	params += "?ActionCode="+action+"&ClassLevelID="+classlevel_id+"&ReportID="+report_id+"&SubjectID="+subject_id;
	if(subject_group_id != null)
		params += "&SubjectGroupID="+subject_group_id;
	params += (isAll == 1) ? "&isAll=1" : "&isAll=0";
	params += "&isCmpSubject=1";
	
    var path = "marksheet_revision_aj_update.php" + params;
    var connectionObject = YAHOO.util.Connect.asyncRequest("POST", path, callback);
}

function jUpdateCompleteCheckBox(xmlObj) 
{
	var actionInfo = xmlObj.getElementsByTagName("ActionCode");
	var classlevelinfo = xmlObj.getElementsByTagName("ClassLevelID");
	var subjectInfo = xmlObj.getElementsByTagName("SubjectID");
	var reportInfo = xmlObj.getElementsByTagName("ReportID");
	var isAll = xmlObj.getElementsByTagName("isAll");
	
	var classInfo = xmlObj.getElementsByTagName("ClassID");
	var isUpdate = xmlObj.getElementsByTagName("isUpdate");
	
	var action_code = actionInfo[0].childNodes[0].nodeValue;
	var isAllFlag = isAll[0].childNodes[0].nodeValue;
	
	for (var i=0 ; i<classInfo.length ; i++){
		var subject_id = subjectInfo[i].childNodes[0].nodeValue;
		var subject_group_id = classInfo[i].childNodes[0].nodeValue;
		var jCheckBoxObj = document.getElementById("complete_"+subject_id+"_"+subject_group_id);
		
		if(isUpdate[i].childNodes[0].nodeValue == 1){
			if(action_code == "COMPLETE"){
				jCheckBoxObj.checked = true;
				jCheckBoxObj.value = 1;
			}
			else if(action_code == "CANCEL"){
				jCheckBoxObj.checked = false;
				jCheckBoxObj.value = 0;
			}
		}
	}
	
	if(isAllFlag == 1){
		var jAllObj = document.getElementById("completeAll");
		if(action_code == "COMPLETE"){
			jAllObj.checked = true;
			jAllObj.value = 1;
		}
		else if(action_code == "CANCEL"){
			jAllObj.checked = false;
			jAllObj.value = 0;
		}
	}			
}
// End of AJAX

function jVIEW_FEEDBACK(subject_group_id, target_subject_id){
	var form_id = <?=(($ClassLevelID != "") ? $ClassLevelID : "''")?>;
	var report_id = <?=(($ReportID != "") ? $ReportID : "''")?>;
	
	report_id = (report_id == "") ? jGET_SELECTBOX_SELECTED_VALUE("ReportType") : report_id;
	subject_id = target_subject_id ;
	
	location.href = "./marksheet_feedback/index.php?ClassLevelID="+form_id+"&ReportID="+report_id+"&SubjectID="+subject_id+"&SubjectGroupID="+subject_group_id;
}

function js_Export_Feedback()
{
	var obj = document.FormMain;
	obj.action = './marksheet_feedback/export_form_feedback.php';
	obj.submit();
}

</script>

<br/>
<form name="FormMain" method="POST" action="marksheet_complete_confirm.php">
<table width="100%" border="0" cellpadding"0" cellspacing="0">
	<tr>
		<td class="navigation"><?=$PageNavigation?></td>
		<td align="right"><?=$ReportTypeSelection?></td>
	</tr>
</table>
<br style="clear:both;" />
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>
	  <table width="100%" border="0" cellspacing="0" cellpadding="2" class="tabletext">
        <tr>
			<td valign="top" width="30%" nowrap="nowrap" class="formfieldtitle tabletext"><?=$eReportCard['Settings_ReportCardTemplates']?></td>
			<td class="tabletext" nowrap="nowrap"><?=$PeriodName?></td>
		</tr>
		<tr>
			<td valign="top" width="30%" nowrap="nowrap" class="formfieldtitle tabletext"><?=$eReportCard['Subject']?></td>
			<td class="tabletext" nowrap="nowrap"><?=$SubjectName?></td>
		</tr>
		<tr>
			<td valign="top" width="30%" nowrap="nowrap" class="formfieldtitle tabletext"><?=$SubjectOrGroupTitle?></td>
			<td class="tabletext" nowrap="nowrap"><?=$SubjectOrGroupNameDisplay?></td>
		</tr>
  	  </table>
  	  <br style="clear:both;" />
  	</td>
  </tr>  
  <tr>
    <td>
    	<?php if(!$eRCTemplateSetting['Settings']['SubjectTopics']) { ?>
    	<div class="content_top_tool">
			<div class="Conntent_tool">
				<?=$ExportBtn?>
			</div>
		</div>
	 	<?php } ?>
      <table width="100%" border="0" cellspacing="0" cellpadding="4">
        <tr class="tablegreentop">
          <!--<td class="tabletoplink"><?=$eReportCard['Class']?></td>-->
          <td class="tabletoplink"><?=$eReportCard['Subject']?></td>
          <td width="80" align="center" class="tabletoplink"><?=$eReportCard['Marksheet']?></td>
          <td width="80" align="center" class="tabletoplink"><?=$eReportCard['TeacherComment']?></td>
          <td width="80" align="center" class="tabletoplink"><?=$eReportCard['Feedback']?></td>
		<?php if ($eRCTemplateSetting['Marksheet']['ManualInputGrade']) { ?>
		  <td width="80" align="center" class="tabletoplink"><?=($column_data[0]['ColumnTitle'].' '.$eReportCard['Grade'])?></td>
		<?php } ?>
          <td class="tabletoplink"><?=$eReportCard['LastModifiedDate']?></td>
          <td class="tabletoplink"><?=$eReportCard['LastModifiedBy']?></td>
          <!--<td width="80" align="center" class="tabletoplink"><?=$eReportCard['Confirmed'].' '.$completeAll?></td>-->
        </tr>
        <?=$display?>
      </table>
    </td>
  </tr>
  <tr class="tablegreenbottom">
    <td>
      <table width="100%" border="0" cellspacing="0" cellpadding="3">
        <tr>
    	  <td class="tabletext"><?=$total?></td>
    	</tr>
      </table>
    </td>
  </tr>
  <tr>
  	<td><span class="tabletextrequire">*</span><span class="tabletextremark"><?=$eReportCard['ManagementArr']['MarksheetRevisionArr']['MarksheetFeedbackArr']['FeedbackInProgress']?></span></td>
  </tr>
  
  <?php 
  	if($ck_ReportCard_UserType == "ADMIN" || $ck_ReportCard_UserType == "VIEW_GROUP" || ($ck_ReportCard_UserType == "TEACHER" && $PeriodType == 1) ){// within submission period 
  ?>
  <tr>
    <td>
      <br>
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
      	<tr>
          <td height="4" class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
      	</tr>
      	<tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="2">
              <tr>
                <td align="center" valign="bottom"><?=$display_button?></td>
              </tr>
          	</table>
          </td>
      	</tr>
      </table>
    </td>
  </tr>
  <?php } ?>
</table>

<input type="hidden" name="isProgress" id="isProgress" value="0"/>
<input type="hidden" name="ClassLevelID" id="ClassLevelID" value="<?=$ClassLevelID?>"/>
<input type="hidden" name="ReportID" id="ReportID" value="<?=$ReportID?>"/>
<input type="hidden" name="SubjectID" id="SubjectID" value="<?=$SubjectID?>"/>
<input type="hidden" name="ClassIDList[]" id="ClassIDList" value="<?=$SubjectGroupID?>"/>
<input type="hidden" name="SubjectGroupID" id="SubjectGroupID" value="<?=$SubjectGroupID?>"/>
<input type="hidden" name="MarksheetRevision" id="MarksheetRevision" value="2"/>

<?php
if($SubjectID != ""){
	if(count($CmpSubjectDataArr) > 0){
		for($j=0 ; $j<count($CmpSubjectDataArr) ; $j++){
			echo "<input type=\"hidden\" name=\"SubjectIDList[]\" id=\"SubjectIDList\" value=\"".$CmpSubjectDataArr[$j]['SubjectID']."\"/>\n";
		}
	}
}
?>

</form>
<?

        $linterface->LAYOUT_STOP();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>