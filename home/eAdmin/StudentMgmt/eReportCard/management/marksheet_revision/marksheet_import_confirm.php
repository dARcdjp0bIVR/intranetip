<?php
// Editing:
/*
 * Modification Log:
 * 20190502 (Bill)
 *  - prevent SQL Injection + Cross-site Scripting 
 * 20110829 (Ivan)
 * 	- added estimated mark logic
 */
 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");

intranet_auth();
intranet_opendb();

### Handle SQL Injection + XSS [START]
$ClassLevelID = IntegerSafe($ClassLevelID);
$ReportID = IntegerSafe($ReportID);
$SubjectID = IntegerSafe($SubjectID);
$ClassID = IntegerSafe($ClassID);
$SubjectGroupID = IntegerSafe($SubjectGroupID);
$ParentSubjectID = IntegerSafe($ParentSubjectID);
$isProgress = IntegerSafe($isProgress);
$isProgressSG = IntegerSafe($isProgressSG);
### Handle SQL Injection + XSS [END]

#################################################################################################

include_once($PATH_WRT_ROOT."includes/libreportcard2008j.php");
$lreportcard = new libreportcard2008j();
//$lreportcard = new libreportcard();

$limport = new libimporttext();
$linterface = new interface_html();

# Preparation
$ParentSubjectID = $lreportcard->GET_PARENT_SUBJECT_ID($SubjectID);
$params = "ClassLevelID=$ClassLevelID&SubjectID=$SubjectID&ReportID=$ReportID&ClassID=$ClassID&SubjectGroupID=$SubjectGroupID&isProgress=$isProgress&isProgressSG=$isProgressSG";
$params .= (isset($ParentSubjectID) && $ParentSubjectID != "") ? "&ParentSubjectID=$ParentSubjectID" : "";

# Initialization 
$result = array();
$FormatWrong = 0;
$MissMatchStudentArr = array();
$DateName = "LastMarksheetInput";

# Get Data
// uploaded file information
$filepath = $userfile;
$filename = $userfile_name;

// Last Modified User
$LastModifiedUserID = $_SESSION['UserID'];

// Class
$ClassArr = $lreportcard->GET_CLASSES_BY_FORM($ClassLevelID);
for($i=0; $i<count($ClassArr); $i++){
	if($ClassArr[$i][0] == $ClassID) {
		$ClassName = $ClassArr[$i][1];
	}
}

# Header including [WebSAMSRegNumber] [ClassName] [ClassNumber] [ReportColumn1] ... [ReportColumn(j-1)]
# Verification by Matching the WebSAMSRegNumber of students
# Main
if($filepath=="none" || $filepath == "" || !is_uploaded_file($filepath) || !$limport->CHECK_FILE_EXT($filename))
{
	// Check Valid File is uploaded
	header("Location: marksheet_import.php?$params&Result=AddUnsuccess");
}
else
{
	$lo = new libfilesystem();
	
	$data = $limport->GET_IMPORT_TXT($filepath);
	$HeaderRow = array_shift($data);		# drop the title bar
	
	# Loading Report Template Data
	$basic_data = $lreportcard->returnReportTemplateBasicInfo($ReportID);			// Basic  Data
	$ReportType = $basic_data['Semester'];
	
	// Subject Grading Scheme
	$SubjectFormGradingArr = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID,0,0,$ReportID);
	$isCmpSubject = $lreportcard->CHECK_SUBJECT_IS_COMPONENT_SUBJECT($SubjectID);
	
	// Student Info Array
	$StudentArr = array();
	$ClassNameField = "ClassName";
	if ($SubjectGroupID != '') // get all student in subject group
	{
		$StudentArr = $lreportcard->GET_STUDENT_BY_SUBJECT_GROUP($SubjectGroupID, $ClassLevelID, '', 1);
		$showClassName = true;
		
		$obj_SubjectGroup = new subject_term_class($SubjectGroupID);
		$SubjectGroupName = $obj_SubjectGroup->Get_Class_Title();
		
		$Class_SG_Title = $Lang['SysMgr']['SubjectClassMapping']['SubjectGroup'];
		$Class_SG_Name = $SubjectGroupName;
	}
	else if(trim($ClassID)!='') // consolidate get all student in class
	{
		$StudentArr = $lreportcard->GET_STUDENT_BY_CLASS($ClassID);
		$Class_SG_Title = "ClassName";
		$Class_SG_Name = $ClassName;
		
		$showClassName = true;
	}
	else if($ReportType == "F") // consolidate get all student in classlevel
	{
		$StudentArr = $lreportcard->GET_STUDENT_BY_CLASSLEVEL($ReportID, $ClassLevelID);
		
		$ClassNameField = "ClassTitleEn";
		$IsClassLevelStudentList=1;
	}
	else // get student in all subject group in classlevel
	{
		$MainSubjectID = $isCmpSubject?$lreportcard->GET_PARENT_SUBJECT_ID($SubjectID):$SubjectID;
		$SubjectGroupList = $lreportcard->Get_Subject_Group_List_Of_Report($ReportID,'',$MainSubjectID);

		foreach((array)$SubjectGroupList as $thisSubjectGroupID)
		{
			$StudentArr = array_merge($StudentArr,(array)$lreportcard->GET_STUDENT_BY_SUBJECT_GROUP($thisSubjectGroupID, $ClassLevelID, '', 1));
		}
	}
	
	$StudentSize = sizeof($StudentArr);
	
	$StudentWebSAMSRegNoArr = array();
	if(count($StudentArr) > 0){
		$StudentWebSAMSRegNoArr = BuildMultiKeyAssoc($StudentArr,"WebSAMSRegNo","UserID",1);
		$StudentClassInfoArr = BuildMultiKeyAssoc($StudentArr,array($ClassNameField,"ClassNumber"),"UserID",1);
		$StudentUserLoginArr = BuildMultiKeyAssoc($StudentArr,"UserLogin","UserID",1);
		$StudentClassInfo =  BuildMultiKeyAssoc($StudentArr,"UserID", array($ClassNameField,"ClassNumber"));
	}
	
	// Scheme Range Info
	$SchemeGrade = array();
	$GradeInfo = array();
	$GradeOption = array();
	$PassFailInfo = array();
	$PassFailOption = array();
	$SchemeFullMark = '';		// new
	
	if(count($SubjectFormGradingArr) > 0){
		$SchemeID = $SubjectFormGradingArr['SchemeID'];
		$ScaleInput = $SubjectFormGradingArr['ScaleInput'];
		$ScaleDisplay = $SubjectFormGradingArr['ScaleDisplay'];
		
		$SchemeInfo = $lreportcard->GET_GRADING_SCHEME_INFO($SchemeID);
		$SchemeMainInfo = $SchemeInfo[0];
		$SchemeMarkInfo = $SchemeInfo[1];
		$SchemeType = $SchemeMainInfo['SchemeType'];
		$SchemeFullMark = $SchemeMainInfo['FullMark'];	// new
		
		if($SchemeType == "H" && $ScaleInput == "G"){
			$SchemeGrade = $lreportcard->GET_GRADE_FROM_GRADING_SCHEME_RANGE($SchemeID);
			if(!empty($SchemeGrade)){
				foreach($SchemeGrade as $GradeRangeID => $grade){
					$GradeInfo[$GradeRangeID] = $grade;
					$GradeOption[] = $grade;
				}
			}
		}
		else if($SchemeType == "PF"){
			$PassFailInfo['P'] = $SchemeMainInfo['Pass'];
			$PassFailInfo['F'] = $SchemeMainInfo['Fail'];
			$PassFailOption = array(strtolower($SchemeMainInfo['Pass']), strtolower($SchemeMainInfo['Fail']));
		}
	}
	
	# Load Settings - Calculation
	// Get the Calculation Method of Report Type - 1: Right-Down,  2: Down-Right
	$SettingCategory = 'Calculation';
	$categorySettings = $lreportcard->LOAD_SETTING($SettingCategory);
	$TermCalculationType = $categorySettings['OrderTerm'];
	$FullYearCalculationType = $categorySettings['OrderFullYear'];
	
	
	# Get component subject(s) if any
	$CmpSubjectArr = array();
	$isCalculateByCmpSub = 0;
	
	if(!$isCmpSubject){
		$CmpSubjectArr = $lreportcard->GET_COMPONENT_SUBJECT($SubjectID, $ClassLevelID);
		$isParentSubject = (count($CmpSubjectArr) > 0)? true : false;
		
		$CmpSubjectIDArr = array();
		if(!empty($CmpSubjectArr)){	
			for($i=0 ; $i<count($CmpSubjectArr) ; $i++){
				$CmpSubjectIDArr[] = $CmpSubjectArr[$i]['SubjectID'];
				
				$CmpSubjectFormGradingArr = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $CmpSubjectArr[$i]['SubjectID'],0,0,$ReportID);
				if(count($CmpSubjectFormGradingArr) > 0 && $CmpSubjectFormGradingArr['ScaleInput'] == "M")
					$isCalculateByCmpSub = 1;
			}
		}
	}
	
	$column_data = $lreportcard->returnReportTemplateColumnData($ReportID); 		// Column Data
	$ColumnTitleAry = $lreportcard->returnReportColoumnTitle($ReportID);
	$ColumnSize = sizeof($column_data);
	
	# Get Existing Report Calculation Method
	$CalculationType = ($ReportType == "F") ? $FullYearCalculationType : $TermCalculationType;
	
	# Get Report Column Weight
	$WeightAry = array();
	if($CalculationType == 2){
		if(count($column_data) > 0){
	    	for($i=0 ; $i<sizeof($column_data) ; $i++){
				$OtherCondition = "SubjectID IS NULL AND ReportColumnID = ".$column_data[$i]['ReportColumnID'];
				$weight_data = $lreportcard->returnReportTemplateSubjectWeightData($ReportID, $OtherCondition); 
				
				$WeightAry[$weight_data[0]['ReportColumnID']][$SubjectID] = $weight_data[0]['Weight'];
			}
		}
	}
	else {
		$weight_data = $lreportcard->returnReportTemplateSubjectWeightData($ReportID);  
		
		for($i=0 ; $i<sizeof($weight_data) ; $i++)
			$WeightAry[$weight_data[$i]['ReportColumnID']][$weight_data[$i]['SubjectID']] = $weight_data[$i]['Weight'];
	}
	/*
	// Subject Weight Array
	$WeightAry = array();
	for($i=0 ; $i<sizeof($weight_data) ; $i++)
		$WeightAry[$weight_data[$i]['ReportColumnID']][$weight_data[$i]['SubjectID']] = $weight_data[$i]['Weight'];
	*/
	
	// special column checking for parent subject
	$isAllComponentZeroWeight = array();
	if ($isParentSubject) {
		for($i=0 ; $i<$ColumnSize ; $i++) {
			$_reportColumnId = $column_data[$i]['ReportColumnID'];
			
			$_isAllComponentZeroWeight = true;
			for($j=0 ; $j<count($CmpSubjectArr) ; $j++) {
				$__cmpSubjectId = $CmpSubjectArr[$i]['SubjectID'];
				
				$__weightAry = $lreportcard->returnReportTemplateSubjectWeightData($ReportID, $other_condition='', $__cmpSubjectId, $_reportColumnId);
				$__subjectWeight = $__weightAry[0]['Weight'];
				
				if ($__subjectWeight > 0) {
					$_isAllComponentZeroWeight = false;
					break;
				}
			}
			
			$isAllComponentZeroWeight[$_reportColumnId] = $_isAllComponentZeroWeight;
		}
	}
	
	
	# Generating Valid Header 
//	$ColumnTitle = array("WebSAMSRegNumber", "ClassName", "ClassNumber", "StudentName");			
	$ColumnTitle[] = $eReportCard['ImportHeader']['ClassName']['En'];
	$ColumnTitle[] = $eReportCard['ImportHeader']['ClassNumber']['En'];
	$ColumnTitle[] = $eReportCard['ImportHeader']['UserLogin']['En'];
	$ColumnTitle[] = $eReportCard['ImportHeader']['WebSamsRegNo']['En'];
	$ColumnTitle[] = $eReportCard['ImportHeader']['StudentName']['En'];
	
	$ReportColumn = array();
	$ColumnHasTemplateAry = array();
	if($ColumnSize > 0){
		for($i=0; $i<$ColumnSize ; $i++){
			list($ReportColumnID, $ReportColumnTitle, $DefaultWeight, $SemesterNum) = $column_data[$i];
			$BlockMarksheet = $column_data[$i]['BlockMarksheet'];
			/* 
			* For Whole Year Report use only
			* Check for the MarkSheet Column is allowed to fill in mark when any term does not have related report template in Whole Year Report
			* by SemesterNum (ReportType) && ClassLevelID
			* An array is created as a control parameter [$ColumnHasTemplateAry]
			*/
			if($ReportType == "F")
				$ColumnHasTemplateAry[$ReportColumnID] = $lreportcard->CHECK_REPORT_TEMPLATE_FROM_COLUMN($SemesterNum, $ClassLevelID);
			else 
				$ColumnHasTemplateAry[$ReportColumnID] = 1;
				
			# Get ReportColumnTitle
			$ReportColumnTitle = ($ReportColumnTitle != "" && $ReportColumnTitle != null) ? $ReportColumnTitle : $ColumnTitleAry[$ReportColumnID];
			
			if($WeightAry[$ReportColumnID][$SubjectID] != 0 && $WeightAry[$ReportColumnID][$SubjectID] != null && $WeightAry[$ReportColumnID][$SubjectID] != ""){
				# Checking for the Readable Columns with (N/A) marked when the case is 
				# It is Whole Year Report AND its terms column has defined Term Report Template OR
				# It has component subjects and the parent subject mark is calculated by its component subjects
				if(($ColumnHasTemplateAry[$ReportColumnID] && $ReportType == "F") || 
				   (!empty($CmpSubjectArr) && $isCalculateByCmpSub && $ScaleInput == "M") 
				    || $lreportcard->Is_Marksheet_Blocked($BlockMarksheet)){
				    	
				    	if ($isAllComponentZeroWeight[$ReportColumnID]) {
							$ColumnTitle[] = $ReportColumnTitle;
							$ReportColumn[] = $ReportColumnTitle;
				    	}
				    	else {
				    		$ColumnTitle[] = $ReportColumnTitle."(N/A)";
							$ReportColumn[] = $ReportColumnTitle."(N/A)";
				    	}
				}
				else {
					$ColumnTitle[] = $ReportColumnTitle;
					$ReportColumn[] = $ReportColumnTitle;
				}
			}
			else {
				$ColumnTitle[] = $ReportColumnTitle."(N/A)";
				$ReportColumn[] = $ReportColumnTitle."(N/A)";
			}
		}
	}
	
	# Add a Overall Term Subject Mark OR Overall Subject Mark Header when the case is 
	# The ScaleInput is Grade(G) OR PassFail(PF)
	$hasOverallSubjectMark = 0;
	if(($SchemeType == "H" && $ScaleInput == "G") || $SchemeType == "PF"){
		$hasOverallSubjectMark = 1;
		if($ReportType != "F"){
			$ColumnTitle[] = $eReportCard['TermSubjectMarkEn'];
			$ReportColumn[] = $eReportCard['TermSubjectMarkEn'];
		}
		else {
			$ColumnTitle[] = $eReportCard['OverallSubjectMarkEn'];
			$ReportColumn[] = $eReportCard['OverallSubjectMarkEn'];
		}
	}
	
	# Verification of Header of imported Csv file
	for($j=0; $j<sizeof($HeaderRow); $j++){
		if($HeaderRow[$j] != $ColumnTitle[$j]){
			$FormatWrong = 1;
			header("Location: marksheet_import.php?Result=WrongCSVHeader&ClassLevelID=$ClassLevelID&ReportID=$ReportID&SubjectID=$SubjectID&SubjectGroupID=$SubjectGroupID&isProgress=$isProgress&isProgressSG=$isProgressSG");
			exit;
		}
	}
	
	
	//$successUpdatedCount = 0;
	
	# Retrieve Existing Data & Manipulate if Header of Format is correct
	if($FormatWong!=1){
		$rec_no = 0;
		for($i=1; $i<sizeof($data) ; $i++){	
			$data_obj = $data[$i];
			$ClassName = trim(array_shift($data_obj));			// class name from csv file
			$ClassNumber = trim(array_shift($data_obj));			// class number from csv file
			$UserLogin = trim(array_shift($data_obj));
			$websamsregno = trim(array_shift($data_obj));	//	WebSAMSRegNumber from csv file
			$studentname = trim(array_shift($data_obj));	// student name from csv file
			
			if(trim($ClassName)!='' xor trim($ClassNumber)!='')
			{
				if(trim($ClassName)=='')
				{
					$WarnMsg[$i][] = $Lang['General']['ImportWarningArr']['EmptyClassName'];
					$WarnCss[$i][0] = true;
				}
				else if(trim($ClassNumber)=='')
				{
					$WarnMsg[$i][] = $Lang['General']['ImportWarningArr']['EmptyClassNumber'];
					$WarnCss[$i][1] = true;
				}
			}
			else if(trim($ClassName)!='' || trim($ClassNumber)!='')
			{
				if(!$StudentID = $StudentClassInfoArr[$ClassName][$ClassNumber])
				{
					$WarnMsg[$i][] = $Lang['General']['ImportWarningArr']['WrongClassNameNumber'];
					$WarnCss[$i][0] = true;
					$WarnCss[$i][1] = true;
				}
			}
			else if(trim($UserLogin)!='')
			{
				if(!$StudentID = $StudentUserLoginArr[$UserLogin])
				{
					$WarnMsg[$i][] = $Lang['General']['ImportWarningArr']['WrongUserLogin'];
					$WarnCss[$i][2] = true;											
				}
			}
			else if(trim($websamsregno)!='')
			{
				if(!$StudentID = $StudentWebSAMSRegNoArr[$websamsregno])
				{
					$WarnMsg[$i][] = $Lang['General']['ImportWarningArr']['WrongWebSamsRegNo'];
					$WarnCss[$i][3] = true;											
				}
				
			}
			else
			{
				$WarnMsg[$i][] = $Lang['General']['ImportWarningArr']['EmptyStudentData'];
				$WarnCss[$i][0] = true;
				$WarnCss[$i][1] = true;
				$WarnCss[$i][2] = true;
				$WarnCss[$i][3] = true;											
			}
//			debug_pr($StudentID);		
//	debug_pr($WarnMsg);		
	
			$sql = '';
//			$row = array();
			//$sql = "SELECT UserID, ClassName, ClassNumber FROM INTRANET_USER WHERE ClassName = '$cname' AND (ClassNumber = '$cnum' OR CONCAT('0',ClassNumber) = '$cnum' or ClassNumber = '0".$cnum."')";
			//$row = $lreportcard->returnArray($sql, 3);
//			$row = $lreportcard->Get_Student_Info_By_ClassName_ClassNumber($cname, $cnum);
//			list($StudentID, $ClassName, $ClassNumber) = $row[0];
			
			
			if(!empty($StudentID) && !empty($data_obj)){
				
				for($j=0; $j<$ColumnSize ; $j++){

					# Check for (N/A) exist in Report Column Internally
				  	if(!strpos($ReportColumn[$j], "(N/A)")){
						$col_no = $j+5;
						list($ReportColumnID, $ReportColumnTitle, $DefaultWeight, $SemesterNum) = $column_data[$j];
						
						$isValid = 1;
						$value = trim($data_obj[$j]);
						//if($value == "")
						if(($value) == "")
						{
							$WarnCss[$i][$col_no] = true;
							$WarnMsg[$i][] = $eReportCard['ImportWarningArr']['EmptyMark'];
							$isValid = 0;
							continue;
						}
						
						$IsEstimated = ($lreportcard->Is_Estimated_Score($value))? 1 : 0;
						$value = $lreportcard->Remove_Estimate_Score_Symbol($value);
						
						$value = (strtolower($value) == "abs") ? strtolower($value) : $value;
						
						# Mark Type : M-Mark, G-Grade, PF-PassFail, SC-SpecialCase
						# Manipulate RawData From CSV file and generate new data Before Insert 
						$MarkNonNum = '';
						$MarkRaw = '-1';				// '-1' : no value provided
						$isSpecialCase = ($SchemeType == "PF") ? $lreportcard->CHECK_MARKSHEET_SPECIAL_CASE_SET2($value) : $lreportcard->CHECK_MARKSHEET_SPECIAL_CASE_SET1($value);
						
						if($SchemeType == "H"){
							if($ScaleInput == "G"){
								$MarkType = ($isSpecialCase) ? "SC" : "G";
								
								// Check Whether the Input Raw Grade is valid (defined in grading scheme) or not
								if($value != "" && (!$isSpecialCase && !in_array($value, $GradeOption)) ){
									
									$WarnCss[$i][$col_no] = true;
									$WarnMsg[$i][] = $eReportCard['ImportWarningArr']['InvilidMark'];
									$isValid = 0;
									continue;
								}
									
								if($isSpecialCase){
									$MarkNonNum = $value;
								}
								else {
									$grade = '';
									foreach($GradeInfo as $RangeID => $grade){
										if($value == $grade)
											$MarkNonNum = $RangeID;
									}
								}
							}
							else {
								$MarkType = ($isSpecialCase) ? "SC" : "M";
								if($isSpecialCase){
									$MarkRaw = ($value == "+") ? 0 : -1;
									$MarkNonNum = $value;
								}
								else {	// new
									if($value != "" ){
										if($SchemeFullMark != "" && $value > $SchemeFullMark){
											$WarnCss[$i][$col_no] = true;
											$WarnMsg[$i][] = $eReportCard['ImportWarningArr']['MarkGreaterThanFullMark'];
											$isValid = 0;
											continue;
										} else {
											$MarkRaw = $lreportcard->ROUND_MARK($value, "SubjectScore");
										}
								 	} else {
									 	$MarkRaw = -1;
								 	} 
								 	//$MarkRaw = ($value != "" ) ? $lreportcard->ROUND_MARK($value, "SubjectScore") : -1;
								}
							}
						}
						else if($SchemeType == "PF"){
							// Check Whether the Input Raw PassFail is valid (defined in grading scheme) or not
							if($value != "" && (!$isSpecialCase && !in_array(strtolower($value), $PassFailOption)) ){
								$isValid = 0;
								$WarnCss[$i][$col_no] = true;
								$WarnMsg[$i][] = $eReportCard['ImportWarningArr']['InvilidMark'];
								continue;
							}
							else
							{	
								foreach($PassFailInfo as $PFKey => $PFString){
									if(strtolower($value) == strtolower($PFString))
										$value = $PFKey;
								}
								
								$MarkType = ($isSpecialCase) ? "SC" : "PF";
								$MarkNonNum = $value;
							}
						}
						else {
							$MarkType = ($isSpecialCase) ? "SC" : "M";
							$MarkRaw = $value;
						}
					
						/*
					    * Allow to import value when 
					    * case 1: Report Type is Term Report AND Weight is not zero / "" / null OR
					    * case 2: It is Whole Year Report AND Any Term Column has not defined term template 
					    * case 3: A subject having component subjects AND its Input Scale is Grade(G) 
					    */
					    if(($ReportType != "F" && $WeightAry[$ReportColumnID][$SubjectID] != 0 && $WeightAry[$ReportColumnID][$SubjectID] != ""
					    	&& $WeightAry[$ReportColumnID][$SubjectID] != null) || 
					    	(!$ColumnHasTemplateAry[$ReportColumnID] && $ReportType == "F") || 
					    	(!empty($CmpSubjectArr) && $ScaleInput == "G") ){
						
							$ImportData[$rec_no]["ColumnMark"][] = array($StudentID,$SubjectID,$ReportColumnID,$SchemeID,$MarkType,$MarkRaw,$MarkNonNum,$IsEstimated);
						}
			  	    }
				}
				
				# Update for Overall Term Subject Mark OR Overall SubjectMark
				# the case when the input scale is Grade(G) or the scheme type is PassFail(PF)
				if($hasOverallSubjectMark){
					$col_no++;
					$value = $data_obj[$j];
					$value = (strtolower($value) == "abs") ? strtolower($value) : $value;
					
					$IsEstimated = ($lreportcard->Is_Estimated_Score($value))? 1 : 0;
					$value = $lreportcard->Remove_Estimate_Score_Symbol($value);
					
					# Mark Type : M-Mark, G-Grade, PF-PassFail, SC-SpecialCase
					# Manipulate RawData From CSV file and generate new data Before Insert 
					$MarkNonNum = '';
					$isSpecialCase = ($SchemeType == "PF") ? $lreportcard->CHECK_MARKSHEET_SPECIAL_CASE_SET2($value) : $lreportcard->CHECK_MARKSHEET_SPECIAL_CASE_SET1($value);
					
					if($SchemeType == "H" && $ScaleInput == "G"){
						$MarkType = ($isSpecialCase) ? "SC" : "G";
						
						// Check Whether the Input Raw Grade is valid (defined in grading scheme) or not
						if($value != "" && (!$isSpecialCase && !in_array($value, $GradeOption)) ){
							$isValid = 0;
							$WarnCss[$i][$col_no] = true;
							$WarnMsg[$i][] = $eReportCard['ImportWarningArr']['InvilidMark'];
							continue;
						}
							
						if($isSpecialCase){
							$MarkNonNum = $value;
						}
						else {
							$grade = '';
							foreach($GradeInfo as $RangeID => $grade){
								if($value == $grade)
									$MarkNonNum = $RangeID;
							}
						}
					}
					else if($SchemeType == "PF"){
						// Check Whether the Input Raw PassFail is valid (defined in grading scheme) or not
						if($value != "" && (!$isSpecialCase && !in_array(strtolower($value), $PassFailOption)) ){
							$isValid = 0;
							$WarnCss[$i][$col_no] = true;
							$WarnMsg[$i][] = $eReportCard['ImportWarningArr']['InvilidMark'];
							continue;
						}
							
						foreach($PassFailInfo as $PFKey => $PFString){
							if(strtolower($value) == strtolower($PFString))
								$value = $PFKey;
						}
						
						$MarkType = ($isSpecialCase) ? "SC" : "PF";
						$MarkNonNum = $value;
					}
					
					$ImportData[$rec_no]["OverallMark"] = array($StudentID,$SubjectID,$ReportID,$SchemeID,$MarkType,$MarkNonNum,$IsEstimated);
		  	    
		  	    }
			}
			//$successUpdatedCount++;
			$rec_no++;
		}
	}
}

$NoOfFail = count($WarnMsg);
$NoOfSuccess = sizeof($data)-1-$NoOfFail;

if(count($WarnMsg)>0)
{
	# build Table 
	$display .= '<table width="95%" border="0" cellpadding="5" cellspacing="0">'."\n";
		$display .= '<tr>'."\n";
			$display .= '<td class="tablebluetop tabletopnolink" width="1%">Row#</td>'."\n";
			for($i=0;$i<sizeof($HeaderRow);$i++)
			{
				$display .= '<td class="tablebluetop tabletopnolink">'.$HeaderRow[$i].'</td>'."\n";
			}
			$display .= '<td class="tablebluetop tabletopnolink" width="30%">'.$Lang['General']['Remark'].'</td>'."\n";
		$display .= '</tr>'."\n";
	
		foreach($WarnMsg as $rowno => $Msg)
		{
			$rowcss = " class='".($errctr%2==0? "tablebluerow2":"tablebluerow1")."' ";
			
			$display .= '<tr '.$rowcss.'>'."\n";
				$display .= '<td class="tabletext">'.($rowno+2).'</td>'."\n";
				for($j=0; $j<sizeof($data[$rowno]); $j++)
				{
					$css = $WarnCss[$rowno][$j]?"red":"";
					$value = $WarnCss[$rowno][$j]&&empty($data[$rowno][$j])?"***":$data[$rowno][$j];
					$display .= '<td class="tabletext '.$css.'">'.$value.'</td>'."\n";
				}
				$display .= '<td class="tabletext">'.implode("<br>",$Msg).'</td>'."\n";
			$display .= '</tr>'."\n";
			
			$errctr++;
		}
	$display .= '</table>'."\n";

}

$BackBtn = $linterface->GET_ACTION_BTN($button_back, "button", "javascript:history.back()","back");
$NextBtn = $linterface->GET_ACTION_BTN($button_import, "submit", "","submit2");

$Btn .= empty($WarnMsg)?$NextBtn."&nbsp;":"";
$Btn .= $BackBtn;
					
# tag information
$TAGS_OBJ[] = array($eReportCard['Management_MarksheetRevision'], "", 0);

# page navigation (leave the array empty if no need)
$PAGE_NAVIGATION[] = array($eReportCard['ImportMarks'], "");


$CurrentPage = "Management_MarkSheetRevision";
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();
							
?>
<form id="form1" name="form1" enctype="multipart/form-data" action="marksheet_import_update.php" method="post">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?= $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION) ?><br/><br/></td>
</tr>
<tr>
	<td><?= $linterface->GET_IMPORT_STEPS(2) ?></td>
</tr>
<tr>
	<td>
		<table width="30%">
			<tr>
				<td class='formfieldtitle'><?=$Lang['General']['SuccessfulRecord']?></td>
				<td class='tabletext'><?=$NoOfSuccess?></td>
			</tr>
			<tr>
				<td class='formfieldtitle'><?=$Lang['General']['FailureRecord']?></td>
				<td class='tabletext <?=$NoOfFail>0?"red":""?>'><?=$NoOfFail?></td>
			</tr>
		</table>
	</td>
</tr>
<tr>
	<td>
		<?=$display?>
	</td>
</tr>
<tr>
	<td><table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
		<tr>
			<td align="center"><?=$Btn?></td>
		</tr>
	</table>
	</td>
</tr>
</table>
<input type="hidden" name="isProgress" id="isProgress" value="<?=$isProgress?>"/>
<input type="hidden" name="isProgressSG" id="isProgressSG" value="<?=$isProgressSG?>"/>
<input type="hidden" name="ClassLevelID" value="<?=$ClassLevelID?>" />
<input type="hidden" name="ReportID" value="<?=$ReportID?>" />
<input type="hidden" name="SubjectID" value="<?=$SubjectID?>" />
<input type="hidden" name="ClassID" value="<?=$ClassID?>" />
<input type="hidden" name="SubjectGroupID" value="<?=$SubjectGroupID?>" />
<input type="hidden" name="ParentSubjectID" value="<?=$ParentSubjectID?>" />
<input type="hidden" name="ImportData" value="<?=htmlspecialchars(serialize($ImportData))?>" />

</form>
<br><br>

<?
$linterface->LAYOUT_STOP();
?>