<?php
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
$lreportcard = new libreportcard();

if (!$lreportcard->hasAccessRight()) {
	intranet_closedb();
	header("Location:/index.php");
}

$ReportID = $_REQUEST['ReportID'];
$ClassLevelID = $_REQUEST['ClassLevelID'];
$SubjectID = $_REQUEST['SubjectID'];

$FormStudentArr = $lreportcard->GET_STUDENT_BY_CLASSLEVEL($ReportID, $ClassLevelID);
$numOfFormStudent = count($FormStudentArr);

if ($numOfFormStudent > 0)
{
	$FormStudentIDArr = array();
	for ($i=0; $i<$numOfFormStudent; $i++)
		$FormStudentIDArr[] = $FormStudentArr[$i]['UserID'];
		
	$FormStudentIDList = implode(',', $FormStudentIDArr);
	
	$table = $lreportcard->DBName.".RC_REPORT_RESULT_SCORE_ARCHIVE";
	$sql = "Delete From
				$table
			Where
				ReportID = '".$ReportID."'
				And
				SubjectID = '".$SubjectID."'
				And
				ReportColumnID = 0
				And
				StudentID in (".$FormStudentIDList.")
			";
	$success = $lreportcard->db_db_query($sql);
}

intranet_closedb();

$Result = ($success) ? "update" : "update_failed";
$parmeters = "ClassID=$ClassID&SubjectID=$SubjectID&ReportID=$ReportID&ClassLevelID=$ClassLevelID&type=$type";
header("Location:marksheet_revision.php?$parmeters&msg=$Result");
?>

