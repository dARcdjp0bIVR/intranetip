<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");


intranet_auth();
intranet_opendb();

if ($plugin['ReportCard2008']) {
	include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
	if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
	{
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
		$lreportcard = new libreportcardcustom();
	} else {
		$lreportcard = new libreportcard();
	}
	
	if ($lreportcard->hasAccessRight()) {
		$limport = new libimporttext();
		
		if ($type=="AdjustPosition")
		{
			$format_array = array("WebSAMSRegNumber", "Class Name", "Class Number","Student Name",$eReportCard['AdjustPositionLabal']);
		}
		else	// extra info
		{
			$EffortKeyArr = array();
			$CheckEffort = false;
			if (isset($lreportcard->ExtraInfoOptionArr['Effort'])) {
				$CheckEffort = true;
				$EffortKeyArr = array_keys($lreportcard->ExtraInfoOptionArr['Effort']);
			}
			
			$format_array = array("WebSAMSRegNumber", "Class Name", "Class Number","Student Name",$eReportCard['ExtraInfoLabel']);
		}
		
		$li = new libdb();
		$filepath = $userfile;
		$filename = $userfile_name;
		
		#  4 parameters need to be passed to other pages
		$parmeters = "ClassID=$ClassID&SubjectID=$SubjectID&ReportID=$ReportID&ClassLevelID=$ClassLevelID&type=$type&SubjectGroupID=$SubjectGroupID";
		
		// [2015-0924-1821-07164] Correct CSV format for Manual Input Grade
		if ($eRCTemplateSetting['Marksheet']['ManualInputGrade']) {
			$column_data = $lreportcard->returnReportTemplateColumnData($ReportID); 	// Column Data
			$format_array = array("WebSAMSRegNumber", "Class Name", "Class Number","Student Name",$column_data[0]['ColumnTitle'].' '.$eReportCard['Grade']);
		}
		
		if($filepath=="none" || $filepath == "" || !isset($SubjectID, $ReportID)){          # import failed
			header("Location: import_extra_info.php?$parmeters&Result=import_failed2");
		    exit();
		} else {
		    if($limport->CHECK_FILE_EXT($filename)) {
			    # read file into array
				# return 0 if fail, return csv array if success
				$data = $limport->GET_IMPORT_TXT($filepath);
				#$toprow = array_shift($data);                   # drop the title bar
				
				$limport->SET_CORE_HEADER($format_array);
				if (!$limport->VALIDATE_HEADER($data, true) || !$limport->VALIDATE_DATA($data)) {
					header("Location: import_extra_info.php?$parmeters&Result=import_failed");
					exit();
				}
				
				// Class
				$ClassArr = $lreportcard->GET_CLASSES_BY_FORM($ClassLevelID);
				for($i=0 ; $i<count($ClassArr) ;$i++) {
					if($ClassArr[$i][0] == $ClassID)
						$ClassName = $ClassArr[$i][1];
				}
				
				// Student Info Array
				if ($SubjectGroupID != '')
				{
					$StudentArray = $lreportcard->GET_STUDENT_BY_SUBJECT_GROUP($SubjectGroupID, $ClassLevelID, '', 1);
					$showClassName = true;
					
					$obj_SubjectGroup = new subject_term_class($SubjectGroupID);
					$SubjectGroupName = $obj_SubjectGroup->Get_Class_Title();
				}
				else
				{
					$StudentArray = $lreportcard->GET_STUDENT_BY_CLASS($ClassID);
					$showClassName = false;
				}
				$StudentSize = sizeof($StudentArray);
				
				//$StudentArray = $lreportcard->GET_STUDENT_BY_CLASS($ClassID);
				//$StudentSize = sizeof($StudentArray);
				
				if ($StudentSize == 0) {
					header("Location: import_extra_info.php?$parmeters&Result=import_failed");
					exit();
				}
				
				$StudentIDList = "";
				for($i=0; $i<$StudentSize; $i++)
					$StudentIDArray[] = $StudentArray[$i][0];
				$StudentIDList = implode(",", $StudentIDArray);
				
				if ($type=="AdjustPosition")
				{
					$table = $lreportcard->DBName.".RC_REPORT_RESULT_SCORE_ARCHIVE";
				}
				else	// extra info
				{
					$table = $lreportcard->DBName.".RC_EXTRA_SUBJECT_INFO";
				}
				
				# get a list of exist comment record
				$sql = "SELECT StudentID FROM $table WHERE 
						SubjectID = '$SubjectID' AND 
						ReportID = '$ReportID' AND 
						StudentID IN ($StudentIDList)";
				$existStudents = $lreportcard->returnVector($sql);
				
				# build an array mapping ClassNumber to UserID in the class
//				$sql = "SELECT UserID, ClassNumber FROM INTRANET_USER ";
//				$sql .= "WHERE UserID IN ($StudentIDList) ";
//				$sql .= "ORDER BY ClassNumber";
//				$tempResult = $lreportcard->returnArray($sql);
//				for($i=0; $i<sizeof($tempResult); $i++) {
//					$classNumMap[$tempResult[$i]["ClassNumber"]] = $tempResult[$i]["UserID"];
//				}
				$StudentInfoArr = $lreportcard->GET_STUDENT_BY_CLASSLEVEL($ReportID, $ClassLevelID, $ParClassID="", $ParStudentID="", $ReturnAsso=0, $isShowStyle=0);
				$numOfStudentInfo = count($StudentInfoArr);
				$StudentInfoAssoArr = array();
				for($i=0; $i<$numOfStudentInfo; $i++) {
					$thisUserID = $StudentInfoArr[$i]['UserID'];
					$thisClassName = $StudentInfoArr[$i]['ClassTitleEn'];
					$thisClassNumber = $StudentInfoArr[$i]['ClassNumber'];
					
					$StudentInfoAssoArr[$thisClassName][$thisClassNumber] = $thisUserID;
				}
				
				array_shift($data);
				$values = array();
			    $delim = "";
				$successUpdatedCount = 0;
				for ($i=0; $i<sizeof($data); $i++) {
					if (empty($data[$i]))
						continue;
					list($webSAMSRegNumber,$className,$classNum,$studentName,$info) = $data[$i];
					
					if ($CheckEffort) {
						if (!in_array($info, (array)$EffortKeyArr)) {
							// wrong effort
							header("Location: import_extra_info.php?$parmeters&Result=WrongEffortValue");
							exit();
						}
					}
					
					//$studentID = $classNumMap[$classNum];
					$studentID = $StudentInfoAssoArr[$className][$classNum];
					# if comment already exist, put it into array for UPDATE
					if (in_array($studentID, $existStudents))
						$updateExtraInfo[$studentID] = strip_tags(trim($info));
					# else,  INSERT
					else
						$insertExtraInfo[$studentID] = strip_tags(trim($info));
			    }
				
				$success = array();
				
				# INSERT new comment
				if (isset($insertExtraInfo)) {
					foreach($insertExtraInfo as $k => $v) {
						if (in_array($k, $StudentIDArray))
						{
							if ($v == 0 || $v == "")
							{
								$v = "NULL";
							}
							if ($type=="AdjustPosition")
							{
								$values[] = "('$k', '$SubjectID', '$ReportID', '0', $v, NOW(), NOW())";
							}
							else	// extra info
							{
								$values[] = "('$k', '$SubjectID', '$ReportID', '$v', '".$lreportcard->uid."', NOW(), NOW())";
							}
						}
					}
					
					$numOfValues = count((array)$values);
					$values = implode(",", $values);
					if ($type=="AdjustPosition")
					{
						$field = "StudentID, SubjectID, ReportID, ReportColumnID, OrderMeritClass, DateInput, DateModified";
					}
					else	// extra info
					{
						$field = "StudentID, SubjectID, ReportID, Info, TeacherID, DateInput, DateModified";
					}
					
				    $sql = "INSERT INTO $table ($field) VALUES $values";
					$success[] = $li->db_db_query($sql);
					$successUpdatedCount += $numOfValues;
				}	
				
				# UPDATE existing comment
				if (isset($updateExtraInfo)) {
					foreach($updateExtraInfo as $k => $v) {
						if ($type=="AdjustPosition")
						{
							if ($v == 0 || $v == "")
							{
								$v = "NULL";
							}
							$sql = "UPDATE $table SET OrderMeritClass = $v, DateModified = NOW() WHERE ";
							$sql .= "StudentID = '$k' AND SubjectID = '$SubjectID' AND ReportID = '$ReportID' AND ReportColumnID = '0'";
						}
						else // extra info
						{
							$sql = "UPDATE $table SET Info = '$v', TeacherID = '".$lreportcard->uid."', DateModified = NOW() WHERE ";
							$sql .= "StudentID = '$k' AND SubjectID = '$SubjectID' AND ReportID = '$ReportID'";
						}
						$success[] = $li->db_db_query($sql);
						$successUpdatedCount++;
					}
				}
				
				if (in_array(false, $success)) {
					header("Location: import_extra_info.php?$parmeters&Result=import_failed2");
					exit();
				}
		    } else {
				header("Location: import_extra_info.php?$parmeters&Result=import_failed");
				exit();
			}
		}
		intranet_closedb();
		header("Location: extra_info.php?$parmeters&SucessCount=$successUpdatedCount&Result=num_records_updated");
	} else {
		echo "You have no priviledge to access this page.";
	}
} else {
	echo "You have no priviledge to access this page.";
}
?>