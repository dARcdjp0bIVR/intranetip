<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
//include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

if ($plugin['ReportCard2008'])
{
	/* Temp Library*/
	include_once($PATH_WRT_ROOT."includes/libreportcard2008j.php");
	$lreportcard = new libreportcard2008j();
	//$lreportcard = new libreportcard()

	# Preparation for Report Checking
	# Initialization
	$isAllValid = 0;
	$result = array();
	$CompleteInfoArr = array();
	$CompleteData = array();
	
	# Get Data - $_POST 
	$ClassIDArr = (isset($ClassIDList) && !empty($ClassIDList)) ? $ClassIDList : array();
	$SubjectIDArr = (isset($SubjectIDList) && !empty($SubjectIDList)) ? $SubjectIDList : array();
	
	# For the use of Marksheet of subject having component subjects
	if(isset($MarksheetRevision) && $MarksheetRevision == 2)
		$TargetClassID = $ClassIDArr[0];
	
	$cnt = 0 ;
	if(count($ClassIDArr) > 0 && count($SubjectIDArr) > 0){
		for($i=0 ; $i<count($ClassIDArr) ;$i++){
			$ClassID = $ClassIDArr[$i];
			
			for($j=0 ; $j<count($SubjectIDArr) ; $j++){
				$ClassSubjectID = $SubjectIDArr[$j];
				
				//check whether the subject teacher teaches this subject in this class or not
				if($ck_ReportCard_UserType=="TEACHER")
				{
					$t = $lreportcard->checkSubjectTeacher($UserID, $ClassSubjectID, $ClassID);
					if(empty($t))	continue;
				}
				
				$CompleteInfoArr[$cnt]['Complete'] = ${"complete_".$ClassSubjectID."_".$ClassID};
				$CompleteInfoArr[$cnt]['SubjectID'] = $ClassSubjectID;
				$CompleteInfoArr[$cnt]['ClassID'] = $ClassID;
				
				$cnt++;
			}
		}
	}
	
	# Get Data Either Assessment Column(s) in Term Or Term Column(s) in Wholely Report	
	// Loading Report Template Data
	$basic_data = $lreportcard->returnReportTemplateBasicInfo($ReportID);			// Basic  Data
	$column_data = $lreportcard->returnReportTemplateColumnData($ReportID); 		// Column Data
	$ColumnSize = sizeof($column_data);
	$ReportType = $basic_data['Semester'];
	
	# For Whole Year Report Use only
	# if it is Whole Year Report, check whether any terms set in whole year repor has been defined or not
	$ColumnHasTemplateArr = array();
	if($ReportType == "F" && $ColumnSize > 0){
		for($j=0 ; $j<$ColumnSize ; $j++){
		    $ReportColumnID = $column_data[$j]['ReportColumnID'];
			$ColumnHasTemplateArr[$ReportColumnID] = $lreportcard->CHECK_REPORT_TEMPLATE_FROM_COLUMN($column_data[$j]['SemesterNum'], $ClassLevelID);
			if(!$ColumnHasTemplateArr[$ReportColumnID])
				$hasDirectInputMark = 1;
		}
	}
	
	// Get Class In ClassLevel
	$ClassArr = $lreportcard->GET_CLASSES_BY_FORM($ClassLevelID);
	
	####################################################################################################
	# General Approach dealing with Search Term Result From Whole Year Report not From Term Report
	# Can deal with Customization of SIS dealing with secondary template 
	$isAllWholeReport = 0;
	$finalWholeReportID = '';
	if($ReportType == "F"){
		$ReportTypeArr = array();
		$ReportTypeArr = $lreportcard->GET_REPORT_TYPES($ClassLevelID);
		if(count($ReportTypeArr) > 0){
			$flag = 0;
			for($j=0 ; $j<count($ReportTypeArr) ; $j++){
				$finalWholeReportID = $ReportTypeArr[$j]['ReportID'];
				if($ReportTypeArr[$j]['ReportID'] != $ReportID){
					if($ReportTypeArr[$j]['Semester'] != "F")	$flag = 1;
				}
				if($ReportTypeArr[$j]['ReportID'] > $finalWholeReportID){
					$finalWholeReportID = $ReportTypeArr[$j]['ReportID'];
				}
			}
			if($flag == 0) $isAllWholeReport = 1;
		}
	}
	
	# Get the Corresponding Whole Year ReportID from 
	# the ReportColumnID of the Current Whole Year Report 
	$WholeYearColumnHasTemplateArr = array();
	if($isAllWholeReport == 1){
		if($ReportType == "F" && $ColumnSize > 0){
			for($j=0 ; $j<$ColumnSize ; $j++){
				$ReportColumnID = $column_data[$j]['ReportColumnID'];
				# Return Whole Year Report Template ID - ReportID 
				$WholeYearColumnHasTemplateArr[$ReportColumnID] = $lreportcard->CHECK_WHOLE_YEAR_REPORT_TEMPLATE_FROM_COLUMN($ReportID, $column_data[$j]['SemesterNum'], $ClassLevelID);
				
				# Check Whether the Return WholeYear ReportID is not greater the Current ReportID
				# if User creates All Reports sequently
				if($WholeYearColumnHasTemplateArr[$ReportColumnID] != false){
					if($WholeYearColumnHasTemplateArr[$ReportColumnID] > $ReportID)
						$WholeYearColumnHasTemplateArr[$ReportColumnID] = false;
				}
			}
		}
	}
	####################################################################################################
	
	
	# Main - Checking
	if(count($CompleteInfoArr) > 0){
		for($i=0 ; $i<count($CompleteInfoArr) ; $i++){
			$isComplete = $CompleteInfoArr[$i]['Complete'];
			$ClassSubjectID = $CompleteInfoArr[$i]['SubjectID'];
			$ClassID = $CompleteInfoArr[$i]['ClassID'];
			
			// Get Subject Name
			$SubjectName = $lreportcard->GET_SUBJECT_NAME($ClassSubjectID);
			
			// Get Class Name
			for($j=0 ; $j<count($ClassArr) ;$j++){
				if($ClassArr[$j][0] == $ClassID)
					$ClassName = $ClassArr[$j][1];
			}
			
			$DataCollected = array(
								'SubjectID'=>$ClassSubjectID, 
								'SubjectName'=>$SubjectName,  
								'ClassID'=>$ClassID, 
								'ClassName'=>$ClassName, 
								'isComplete'=>$isComplete
								   );
			
			# Get Complete Status of Common Subject or Parent Subject
			$ProgressArr = array();
		    $ProgressArr = $lreportcard->GET_MARKSHEET_SUBMISSION_PROGRESS($ClassSubjectID, $ReportID, $ClassID);
		    $isSubjectComplete = (count($ProgressArr) > 0) ? $ProgressArr['IsCompleted'] : 0;
			    
			if($isComplete == 1){	// case 1: set to complete record
				if($isSubjectComplete == 0){	// when the marksheet of that class subject has not been completed yet
					
					# Get component subject(s) if any
					$CmpSubjectArr = array();
					$isCalculateByCmpSub = 0;	// set to "1" when one ScaleInput of component subjects is Mark(M)
					
					$isCmpSubject = $lreportcard->CHECK_SUBJECT_IS_COMPONENT_SUBJECT($ClassSubjectID);
					
					$isEdit = 1;
					if(!$isCmpSubject){
						$CmpSubjectArr = $lreportcard->GET_COMPONENT_SUBJECT($ClassSubjectID, $ClassLevelID);
						
						$CmpSubjectIDArr = array();
						if(!empty($CmpSubjectArr)){	
							for($j=0 ; $j<count($CmpSubjectArr) ; $j++){
								$CmpSubjectIDArr[] = $CmpSubjectArr[$j]['SubjectID'];
								
								$CmpSubjectFormGradingArr = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $CmpSubjectArr[$j]['SubjectID'],0,0,$ReportID);
								if(count($CmpSubjectFormGradingArr) > 0 && $CmpSubjectFormGradingArr['ScaleInput'] == "M")
									$isCalculateByCmpSub = 1;
							}
							
							# Check whether the marksheet is editable or not 
							$isEdit = $lreportcard->CHECK_MARKSHEET_STATUS($ClassSubjectID, $ClassLevelID, $ReportID, $CmpSubjectIDArr);
						}
					}
					
					
					# Get Student Info 
					$StudentArr = $lreportcard->GET_STUDENT_BY_CLASS($ClassID);
					$StudentSize = sizeof($StudentArr);
					$StudentIDArr = array();
					if($StudentSize > 0){
						for($j=0 ; $j<$StudentSize ; $j++)
							$StudentIDArr[$j] = $StudentArr[$j]['UserID'];
					}
					
					# Get Scheme Details & Subject Grading Details
					$SubjectFormGradingArr = array();
					$SubjectFormGradingArr = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $ClassSubjectID,0,0,$ReportID);
					if(count($SubjectFormGradingArr) > 0){
						$SchemeID = $SubjectFormGradingArr['SchemeID'];
						$ScaleInput = $SubjectFormGradingArr['ScaleInput'];
						
						$SchemeMainInfo = $lreportcard->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
						$SchemeType = $SchemeMainInfo['SchemeType'];
					}
					
					
					# Get MarkSheet Score Info / ParentSubMarksheetScoreInfoArr	
					$MarksheetScoreInfoArr = array();
					$RowError = array();
					
					# Main
					if($ColumnSize > 0){
						######################## Component Subjects #####################
						# Check for existing Report of each component Subjects
						$isAllCmpValid = 1;
						if(!empty($CmpSubjectArr)){
						    # Check Any Missing Values(Mark, Grade, Pass/Fail, Special Case) of Whole Marksheet of 
						    # All Component Subjects if selected Subject contains 
						    for($k=0 ; $k<count($CmpSubjectArr) ; $k++){
							    # Get Complete Status of Component Subjects
							    $CmpProgressArr = array();
							    $CmpProgressArr = $lreportcard->GET_MARKSHEET_SUBMISSION_PROGRESS($CmpSubjectArr[$k]['SubjectID'], $ReportID, $ClassID);
							    $isCompleteCmp = (count($CmpProgressArr) > 0) ? $CmpProgressArr['IsCompleted'] : 0;
								   
							    # Check whether the marksheet of component subject has been completed or not before
							    if($isCompleteCmp == 0){
								    
								    ##############################################################################################################
									# Use for the approach which does not have complete function in Component Subject Marksheet Revision List
								    for($j=0 ; $j<$ColumnSize ; $j++){
									    $ReportColumnID = $column_data[$j]['ReportColumnID'];
									    
									    if($ReportType == "F"){
										    if($isAllWholeReport == 0){
										    	# Check For Each Report Column Directly for Whole Year Report use only
											    $hasDirectInputMark = 0;
											    if(!$lreportcard->CHECK_REPORT_TEMPLATE_FROM_COLUMN($column_data[$j]['SemesterNum'], $ClassLevelID) )
													$hasDirectInputMark = 1;
											} else {
												####################################################################################################
												# Check For Each Report Column Directly for All Existing Report are Whole Year Report Type
												######
												$hasDirectInputMark = 0;
												$WholeYearColumnID = $lreportcard->CHECK_WHOLE_YEAR_REPORT_TEMPLATE_FROM_COLUMN($ReportID, $column_data[$j]['SemesterNum'], $ClassLevelID);
												# Check Whether the Return WholeYear ReportID is not greater the Current ReportID
												# IF User creates All Reports sequently (MUST Followed)
												if(!$WholeYearColumnID){
													$hasDirectInputMark = 1;
												} else {
													if($WholeYearColumnID > $ReportID)	$hasDirectInputMark = 1;
												}
											}
										}
									    
									    # Check for any report column weight is Zero or not
										$isColumnWeightZero = $lreportcard->IS_ALL_REPORT_COLUMN_WEIGHTS_ZERO($ReportID, $CmpSubjectArr[$k]['SubjectID'], $ReportColumnID);
										
										if($isColumnWeightZero == 0){
											# Case 1: is Term Report
											# Case 2: is Whole Year Report & no defined Term Report for Report Column & ClassLevel contains at least one Term Report
											# Case 3: is Whole Year Report & Input Mark Directly & ClassLevel does not contain any Term Report but all Whole Year Report
											if( $ReportType != "F" || 
							    			   ($ReportType == "F" && $hasDirectInputMark && !$ColumnHasTemplateArr[$ReportColumnID]) || 
							    			   ($ReportType == "F" && $hasDirectInputMark && $isAllWholeReport == 1) ){  			
												$empty = $lreportcard->IS_MARKSHEET_SCORE_EMPTY($StudentIDArr, $CmpSubjectArr[$k]['SubjectID'], $ReportColumnID);
												if($empty == "All" || $empty != $StudentSize){
													$RowError['Reason'][] = $eReportCard['MarksheetOfCmpSubjectNotCompleted'];
												    $RowError['Type'][] = "COMPONENT";
												    $isAllCmpValid = 0;
												}
											}
										}
										if(!$isAllCmpValid) break;
								    }
								    
								    # For Component Subject
								    # Check whether the Overall Subject mark or Overall Term Subject mark has empty scores or not
								    # when the scheme type is honor-based with Grade(G) as an Input Scale OR 
								    # when the scheme type is PassFail(PF)
								    if(($SchemeType == "H" && $ScaleInput == "G") || $SchemeType == "PF"){
									    $empty = '';
										$empty = $lreportcard->IS_MARKSHEET_SCORE_EMPTY($StudentIDArr, $CmpSubjectArr[$k]['SubjectID'], $ReportID, 1);
										
									    if($empty == "All" || $empty != $StudentSize){
									    	$RowError['Reason'][] = $eReportCard['MarksheetOfCmpSubjectNotCompleted'];
											$RowError['Type'][] = "COMPONENT";
											$isAllCmpValid = 0;
								    	}
								    }
								    ##############################################################################################################
								    
								    /*
								    # Approach with Complete Function in Component Subject Marksheet Revision
								    $RowError['Reason'][] = $eReportCard['MarksheetOfCmpSubjectNotCompleted'];
								    $RowError['Type'][] = "COMPONENT";
								    $isAllCmpValid = 0;
								    if(!$isAllCmpValid) break;
								    */
						    	}
						    }
						}
						################### End Of Component Subjects ###################
						
						# Check Any Missing Values(Mark, Grade, Pass/Fail, Special Case) of Marksheet of 
					    # Common Subject without any Componenet Subjects Or
					    # Parent Subject having Component Subjects when all its component subjects are valid to complete
					  	if(empty($CmpSubjectArr) || (!empty($CmpSubjectArr) && $isAllCmpValid) ){
							$isSubjectValid = 1;
							$isTermSubjectValid = 1;
							$isWholeYearSubjectValid = 1;
							
							# Check for Whole Year & Having Term Report Template
							if($ReportType == "F"){
								for($j=0 ; $j<$ColumnSize ; $j++){
								    $ReportColumnID = $column_data[$j]['ReportColumnID'];
								    
								    if($isAllWholeReport == 0){
									    # Case for Whole Year Report Template with defined Term Report Template
							    		if($ColumnHasTemplateArr[$ReportColumnID]){
								    		$TermReportID = $ColumnHasTemplateArr[$ReportColumnID];
								    		
								    		# Check for all Report Column Weight in Term Report are Zero or not
								    		$isTermColumnWeightAllZero = $lreportcard->IS_ALL_REPORT_COLUMN_WEIGHTS_ZERO($TermReportID, $ClassSubjectID);
									
								    		# Case 1: all report column weight in Term Report are Zero, ignore that report
								    		# Case 2: at least one report column weight in Term Report is greater than Zero, check for Complete Status
											if($isTermColumnWeightAllZero == 0){								    		
									    		# Get Complete Status of Term Report which belongs to Whole Year Report Template
												$ProgressArr = array();
											    //$ProgressArr = $lreportcard->GET_MARKSHEET_SUBMISSION_PROGRESS($ClassSubjectID, $TermReportID, $ClassID);
											    $ProgressArr = $lreportcard->GET_MARKSHEET_SUBMISSION_PROGRESS_OF_ALL_SUBJECT_GROUP($ClassSubjectID, $TermReportID, $ClassID);
											    $isTermComplete = (count($ProgressArr) > 0) ? $ProgressArr['IsCompleted'] : 0;
											    
											    if($isTermComplete == 0){
												    $RowError['Reason'][] = $eReportCard['MarksheetOfTermReportNotCompleted'];
									    			$RowError['Type'][] = "TERM";
									    			$RowError['TermReportID'][] = $TermReportID;
												    $isTermSubjectValid = 0;
											    }
											    
											    if($isTermSubjectValid == 0) continue;
										    }
									    }
								    } else {
									    ####################################################################################################
							    		# General Approach dealing with Search Term Result From Whole Year Report not From Term Report
							    		# Case for Whole Year Report Template withoud defined Term Report Template 
							    		# but having other defined Whole Year Template containing corresponding Term Report Column
							    		#######
						    			if($WholeYearColumnHasTemplateArr[$ReportColumnID]){
						    				$WholeYearReportID = $WholeYearColumnHasTemplateArr[$ReportColumnID];
						    				
						    				# Check for any report column weight is Zero or not	// added on 6 June
											$isAllColumnWeightZero = $lreportcard->IS_ALL_REPORT_COLUMN_WEIGHTS_ZERO($WholeYearReportID, $ClassSubjectID);
											
											if($isAllColumnWeightZero == 0){
							    				$ProgressArr = array();
											    $ProgressArr = $lreportcard->GET_MARKSHEET_SUBMISSION_PROGRESS($ClassSubjectID, $WholeYearReportID, $ClassID);
											    $isWholeYearComplete = (count($ProgressArr) > 0) ? $ProgressArr['IsCompleted'] : 0;
											    
											    if($isWholeYearComplete == 0){
												    //$RowError['Reason'][] = $eReportCard['MarksheetOfTermReportNotCompleted'];
												    $RowError['Reason'][] = "The MarkSheet of Whole Year Report is not Completed.";
									    			$RowError['Type'][] = "OWN";
									    			$RowError['TermReportID'][] = $WholeYearReportID;
												    $isWholeYearSubjectValid = 0;
											    }
										    }
										    
						    			}
								    }
						    		if($isTermSubjectValid == 0) continue;
						    		if($isWholeYearSubjectValid == 0) continue;
						    		####################################################################################################
								}
							}
							
							
							# Check for Common Subject or Parent Subject in case i
							# Case 1: When it is Term Report Template
					    	# Case 2: When it is Whole Year Report Template & has directly input Term Subject Mark & 
					    	#         All its defined Term Report Templates have been completed 
					    	# 
					    	if(($ReportType != "F" && $isEdit) || ($ReportType == "F" && $isTermSubjectValid && $isEdit) ){	
							    for($j=0 ; $j<$ColumnSize ; $j++){
								    $ReportColumnID = $column_data[$j]['ReportColumnID'];
						    		
								    if($ReportType == "F"){
									    if($isAllWholeReport == 0){
									    	######
									    	# Check For Each Report Column Directly for All Existing Report 
									    	# From Term Report
									    	######
										    $hasDirectInputMark = 0;
										    if(!$lreportcard->CHECK_REPORT_TEMPLATE_FROM_COLUMN($column_data[$j]['SemesterNum'], $ClassLevelID) )
												$hasDirectInputMark = 1;
										} else {
											######
											# Check For Each Report Column Directly for All Existing Report 
											# From Whole Year Report
											######
											$hasDirectInputMark = 0;
											$WholeYearColumnID = $lreportcard->CHECK_WHOLE_YEAR_REPORT_TEMPLATE_FROM_COLUMN($ReportID, $column_data[$j]['SemesterNum'], $ClassLevelID);
											# Check Whether the Return WholeYear ReportID is not greater the Current ReportID
											# if User creates All Reports sequently
											if(!$WholeYearColumnID){
												$hasDirectInputMark = 1;
											} else {
												if($WholeYearColumnID > $ReportID)	$hasDirectInputMark = 1;
											}
										}
									}
									
									# Check for any report column weight is Zero or not
									$isColumnWeightZero = $lreportcard->IS_ALL_REPORT_COLUMN_WEIGHTS_ZERO($ReportID, $ClassSubjectID, $ReportColumnID);
									
									# Case 1: that particular report column weight in Report is Zero, ignore that report
								    # Case 2: that particular report column weight in Report IS NOT Zero, check for Empty Field of Marksheet
									if($isColumnWeightZero == 0){
							    		if($ReportType != "F" || 
							    			($ReportType == "F" && $hasDirectInputMark && $isTermSubjectValid) ){
								    			 
								    		if($eRCTemplateSetting['Bypass_Parent_Subject_Marksheet_Checking'] == false)
								    		{
											    $empty = '';
												$empty = $lreportcard->IS_MARKSHEET_SCORE_EMPTY($StudentIDArr, $ClassSubjectID, $ReportColumnID);
												
											    if($empty == "All" || $empty != $StudentSize){
											    	$RowError['Reason'][] = $eReportCard['MarksheetNotCompleted'];
										    		$RowError['Type'][] = "OWN";
											    	$isSubjectValid = 0;
										    	}
								    		}
									    	if(!$isSubjectValid) break;
								    	}
							    	}
								}
							}
					   	}
					    else if(!empty($CmpSubjectArr) && $isAllCmpValid == 0){
						  	$RowError['Reason'][] = $eReportCard['MarksheetOfCmpSubjectNotCompleted'];
							$RowError['Type'][] = "COMPONENT";
					    }
				    }
				    
				    if ($eRCTemplateSetting['Bypass_Parent_Subject_Marksheet_Checking'] == false)
				    {
				    	// Check if the Parent Subject Marksheet Completed or not for general case
					    # For Non-component Subject
					    # Check whether the Overall Subject mark or Overall Term Subject mark has empty scores or not
					    # when the scheme type is honor-based with Grade(G) as an Input Scale OR 
					    # when the scheme type is PassFail(PF)
					    if(($SchemeType == "H" && $ScaleInput == "G") || $SchemeType == "PF"){
						    
						    ##########################
						    # Case 1: for Product use only when $isAllWholeReport == 0
						    # Case 2: for SIS use only when it is all Whole Year Report and this is not Final Whole Year Report
						    ######
						    if($isAllWholeReport == 0 || ($isAllWholeReport && $finalWholeReportID != $ReportID) ){	
							    $empty = '';
								$empty = $lreportcard->IS_MARKSHEET_SCORE_EMPTY($StudentIDArr, $ClassSubjectID, $ReportID, 1);
								
							    if($empty == "All" || $empty != $StudentSize){
							    	$RowError['Reason'][] = $eReportCard['MarksheetNotCompleted'];
									$RowError['Type'][] = "OWN";
						    	}
					    	}
					    }
				    }
									
				    # Prepare Log Data
					if(sizeof($RowError) == 0){
	                    # to be updated
	                    $CompleteData['valid'][] = array('data'=>$DataCollected);
	                }
	                else {
						# invalid record
						$CompleteData['invalid'][] = array('data'=>$DataCollected, 'error'=>$RowError);
	                }
					
	                reset($RowError);
					
				}
				else {
					$CompleteData['valid'][] = array('data'=>$DataCollected);
				}
			}
			else if($isComplete == 0){ 	// case 2: set to incompleted records
				$CompleteData['valid'][] = array('data'=>$DataCollected);
			}
		}
	}
	
	#################################################################################################
	# Update Valid Records
	if(isset($CompleteData['valid']) && count($CompleteData['valid']) > 0){
		for($i=0 ; $i<count($CompleteData['valid']) ;$i++){
			$subject_id = $CompleteData['valid'][$i]['data']['SubjectID'];
			$class_id = $CompleteData['valid'][$i]['data']['ClassID'];
			$is_complete = $CompleteData['valid'][$i]['data']['isComplete'];
			
			
			$parent_subject_id = $lreportcard->GET_PARENT_SUBJECT_ID($subject_id);
			if($parent_subject_id != ""){
				$result['update_submission_progress'] = $lreportcard->UPDATE_RELATED_SUBJECT_SUBMISSION_PROGRESS($ReportID, $ClassLevelID, $class_id, $subject_id, $is_complete, $parent_subject_id);
			} else {
				$result['update_submission_progress'] = $lreportcard->UPDATE_RELATED_SUBJECT_SUBMISSION_PROGRESS($ReportID, $ClassLevelID, $class_id, $subject_id, $is_complete);
			}
			
			/*
			##############################################################################################################
			# Use for the approach which does not have complete function in Component Subject Marksheet Revision List
			# Construct Component Subject Details if it contains
			$isCmpSubject = $lreportcard->CHECK_SUBJECT_IS_COMPONENT_SUBJECT($subject_id);
			if(!$isCmpSubject){
				$CmpSubjectArr = $lreportcard->GET_COMPONENT_SUBJECT($subject_id, $ClassLevelID);
				if(!empty($CmpSubjectArr)){	
					for($j=0 ; $j<count($CmpSubjectArr) ; $j++){
						$cmp_subject_id = $CmpSubjectArr[$j]['SubjectID'];
						
						# Get any existing record of Component Subject in Marksheet Submission Progress
						$CmpSubmissionProgressArr = $lreportcard->GET_MARKSHEET_SUBMISSION_PROGRESS($cmp_subject_id, $ReportID, $class_id);
						
						if(count($CmpSubmissionProgressArr) > 0){	
							# UPDATE - if having existing records
							$result['update_cmp_complete_'.$j] = $lreportcard->UPDATE_MARKSHEET_SUBMISSION_PROGRESS($cmp_subject_id, $ReportID, $class_id, $is_complete);
						} else {	
							# INSERT - if do not have existing records (first time to update marksheet submission progress of this term subject of a class
							$result['insert_cmp_complete_'.$j] = $lreportcard->INSERT_MARKSHEET_SUBMISSION_PROGRESS($cmp_subject_id, $ReportID, $class_id, $is_complete);
						}
					}
				}
			} */
			/*
			else {
				# When a Component Subject is set from "Completed" to "NotCompleted"
				# the Complete Status of its Parent Subject will set to "NotCompleted"
				if($is_complete == 0){
					//nth
					$parent_subject_id = $lreportcard->GET_PARENT_SUBJECT_ID($subject_id);
					$SubProgressArr = $lreportcard->GET_MARKSHEET_SUBMISSION_PROGRESS($parent_subject_id, $ReportID, $class_id);
					if(count($SubProgressArr) > 0 && $SubProgressArr['IsCompleted'] == 1){
						# UPDATE - if having existing records
						$result['update_parent_complete_'.$i] = $lreportcard->UPDATE_MARKSHEET_SUBMISSION_PROGRESS($parent_subject_id, $ReportID, $class_id, $is_complete);
					}
				}
			}
			*/
			##############################################################################################################
			/*
			# Get any existing record of Non-Component Subject in Marksheet Submission Progress
			$SubmissionProgressArr = $lreportcard->GET_MARKSHEET_SUBMISSION_PROGRESS($subject_id, $ReportID, $class_id);
			
			if(count($SubmissionProgressArr) > 0){	
				# UPDATE - if having existing records
				$result['update_complete_'.$i] = $lreportcard->UPDATE_MARKSHEET_SUBMISSION_PROGRESS($subject_id, $ReportID, $class_id, $is_complete);
			} else {	
				# INSERT - if do not have existing records (first time to update marksheet submission progress of this term subject of a class
				$result['insert_complete_'.$i] = $lreportcard->INSERT_MARKSHEET_SUBMISSION_PROGRESS($subject_id, $ReportID, $class_id, $is_complete);
			}*/
		}
		
		###########################################################################
		# For Case in All Whole Year Report in Class Level 
		# Trigger the Final Whole Year Report if Any other non-final whole year report changes to incomplete status
		##############
		if($isAllWholeReport == 1 && $finalWholeReportID != $ReportID){
			for($i=0 ; $i<count($CompleteData['valid']) ;$i++){
				$subject_id = $CompleteData['valid'][$i]['data']['SubjectID'];
				$class_id = $CompleteData['valid'][$i]['data']['ClassID'];
				$is_complete = $CompleteData['valid'][$i]['data']['isComplete'];
				
				
				$parent_subject_id = $lreportcard->GET_PARENT_SUBJECT_ID($subject_id);
				if($is_complete == 0){
					if($parent_subject_id != ""){
						$result['update_WYR_submission_progress'] = $lreportcard->UPDATE_RELATED_SUBJECT_SUBMISSION_PROGRESS($finalWholeReportID, $ClassLevelID, $class_id, $subject_id, $is_complete, $parent_subject_id);
					} else {
						$result['update_WYR_submission_progress'] = $lreportcard->UPDATE_RELATED_SUBJECT_SUBMISSION_PROGRESS($finalWholeReportID, $ClassLevelID, $class_id, $subject_id, $is_complete);
					}
				}
			}
		}
		###########################################################################
	}
	
	if(!isset($CompleteData['invalid']))
		$isAllValid = 1;

	#################################################################################################

	if($isAllValid){
		if($isProgress){
			$result = (!in_array(false, $result)) ? "update" : "update_failed";
			$params = "ClassLevelID=$ClassLevelID&ReportID=$ReportID&result=$result";
			header("Location: ../progress/submission.php?$params");
		}
		else {
			$msg = (!in_array(false, $result)) ? "update" : "update_failed";
			$params = "ClassLevelID=$ClassLevelID&ReportID=$ReportID&SubjectID=$SubjectID&msg=$msg";
			if($MarksheetRevision == 1){
				$url = "marksheet_revision.php?$params";
				header("Location: $url");
			}
			else {
				$url = "marksheet_revision2.php?$params&ClassID=$TargetClassID";
				header("Location: $url");
			}
		}
	}
	else {
		$CurrentPage = "Management_MarkSheetRevision";
		$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
		
	    if ($lreportcard->hasAccessRight())
	    {
	        $linterface = new interface_html();
	        $PAGE_TITLE = $eReportCard['Management_MarksheetRevision'];
	        
	        if($ck_ReportCard_UserType=="TEACHER")
			{
				$PAGE_TITLE = $eReportCard['Management_MarksheetSubmission'];
				$FormArrCT = $lreportcard->returnSunjectTeacherForm($UserID);
				$ClassLevelID = ($ClassLevelID == "") ? $FormArrCT[0][0] : $ClassLevelID;
				$ClassArrCT = $lreportcard->returnSubjectTeacherClass($UserID, $ClassLevelID);
			}
	        
			############################################################################################################
			
			// Period (Term x, Whole Year, etc)
			$PeriodArr = $lreportcard->GET_REPORT_TYPES($ClassLevelID, $ReportID);
			$PeriodName = (count($PeriodArr) > 0) ? $PeriodArr['SemesterTitle'] : '';
			
			# Valid record - updated
			$display_valid_record = '';
			$valid_cnt = 0;
			if(isset($CompleteData['valid']) && count($CompleteData['valid']) > 0){
				for($i=0 ; $i<count($CompleteData['valid']) ;$i++){
					$row_css = ($i % 2 == 1) ? "tablerow2" : "tablerow1";
					
					$params = "ClassLevelID=$ClassLevelID&ReportID=$ReportID";
					$params .= "&SubjectID=".$CompleteData['valid'][$i]['data']['SubjectID'];
					$params .= "&ClassID=".$CompleteData['valid'][$i]['data']['ClassID'];
					
					// Get Updated Complete Status
					$ProgressArr = array();
					$ProgressArr = $lreportcard->GET_MARKSHEET_SUBMISSION_PROGRESS($CompleteData['valid'][$i]['data']['SubjectID'], $ReportID, $CompleteData['valid'][$i]['data']['ClassID']);
					
					$display_valid_record .= '
			          <tr>
			            <td class="'.$row_css.' tabletext">'.$CompleteData['valid'][$i]['data']['ClassName'].'</td>
			            <td class="'.$row_css.' tabletext">'.$CompleteData['valid'][$i]['data']['SubjectName'].'</td>
			            <td align="center" class="'.$row_css.' tabletext"><a href="marksheet_edit.php?'.$params.'">
			              <img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_edit_b.gif" width="20" height="20" border="0" alt="'.$button_edit.' '.$eReportCard['Marksheet'].'">
			            </a></td>
			            <td align="center" class="'.$row_css.' tabletext">'.((count($ProgressArr) > 0) ? ($ProgressArr['IsCompleted']) ? $eReportCard['Confirmed'] : $eReportCard['NotCompleted'] : $eReportCard['NotCompleted']).'</td>
			          </tr>';
		        }
		        $valid_cnt = count($CompleteData['valid']);
	        }
	        else {
		        $display_valid_record .= '<tr><td colspan="4" align="center">'.$eReportCard['NoRecordIsUpdated'].'</td></tr>';
	        }
	        
	        
	        # Invalid record
	        $display_invalid_record = '';
	        $invalid_cnt = 0;
			if(isset($CompleteData['invalid']) && count($CompleteData['invalid']) > 0){
				for($i=0 ; $i<count($CompleteData['invalid']) ;$i++){
					$row_css = ($i % 2 == 1) ? "tablerow2" : "tablerow1";
					
					$params = "ClassLevelID=$ClassLevelID";
					$params .= "&SubjectID=".$CompleteData['invalid'][$i]['data']['SubjectID'];
					$params .= "&ClassID=".$CompleteData['invalid'][$i]['data']['ClassID'];
					
					if($CompleteData['invalid'][$i]['error']['Type'][0] == "TERM" && isset($CompleteData['invalid'][$i]['error']['TermReportID']) ){
						$params .= "&ReportID=".$CompleteData['invalid'][$i]['error']['TermReportID'][0];
					} else {
						$params .= "&ReportID=$ReportID";
					}
					
					// Get Updated Complete Status
					$ProgressArr = array();
					$ProgressArr = $lreportcard->GET_MARKSHEET_SUBMISSION_PROGRESS($CompleteData['invalid'][$i]['data']['SubjectID'], $ReportID, $CompleteData['invalid'][$i]['data']['ClassID']);
					
					$display_invalid_record .= '
			          <tr>
			            <td class="'.$row_css.' tabletext">'.$CompleteData['invalid'][$i]['data']['ClassName'].'</td>
			            <td class="'.$row_css.' tabletext">'.$CompleteData['invalid'][$i]['data']['SubjectName'].'</td>
			            <td align="center" class="'.$row_css.' tabletext">'.((count($ProgressArr) > 0) ? ($ProgressArr['IsCompleted']) ? $eReportCard['Confirmed'] : $eReportCard['NotCompleted'] : $eReportCard['NotCompleted']).'</td>
			            <td class="'.$row_css.' tabletext">'.$CompleteData['invalid'][$i]['error']['Reason'][0].'</td>
			        	<td align="center" class="'.$row_css.' tabletext">';
			        if($CompleteData['invalid'][$i]['error']['Type'][0] == "COMPONENT"){
				        $display_invalid_record .= '
			              <a href="marksheet_revision2.php?'.$params.'"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_edit_b.gif" width="20" height="20" border="0" alt="'.$button_edit.' '.$eReportCard['Marksheet'].'"></a>';
			        } else if($CompleteData['invalid'][$i]['error']['Type'][0] == "TERM"){
			        	$display_invalid_record .= '
			              <a href="marksheet_edit.php?'.$params.'"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_edit_b.gif" width="20" height="20" border="0" alt="'.$button_edit.' '.$eReportCard['Marksheet'].'"></a>';
		            } else {
			        	$display_invalid_record .= '
			              <a href="marksheet_edit.php?'.$params.'"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_edit_b.gif" width="20" height="20" border="0" alt="'.$button_edit.' '.$eReportCard['Marksheet'].'"></a>';
		            }
			        $display_invalid_record .= '
			            </td>
			          </tr>';
		        }
		        $invalid_cnt = count($CompleteData['invalid']);
	        }
	        else {
		        $display_invalid_record .= '<tr><td colspan="5" align="center">No record contains empty mark(s).</td></tr>';
	        }
			
			# Button
			$display_button = '';
			if(isset($CompleteData['invalid']) && count($CompleteData['invalid']) > 0){
				if ($eRCTemplateSetting['Disable_Auto_Fill_In_NA'] == true || getenv("SERVER_NAME") == "project6.broadlearning.com" || getenv("SERVER_NAME") == "sis-trial.broadlearning.com") {
					$display_button .= '';
				} else {
					$display_button .= '<input name="AutoFillNA" type="button" class="formbutton" onMouseOver="this.className=\'formbuttonon\'" onMouseOut="this.className=\'formbutton\'" value="'.$eReportCard['FillAllEmptyMarksWithNA'].'" onClick="jFILL_EMPTY_SCORE_WITH_NA()">';
				}
			}
			$display_button .= '<input name="Cancel" type="button" class="formbutton" onMouseOver="this.className=\'formbuttonon\'" onMouseOut="this.className=\'formbutton\'" value="'.$button_cancel.'" onClick="jBACK()">';
			
			############################################################################################################
        
			# tag information
			$TAGS_OBJ[] = array($PAGE_TITLE, "", 0);
			
			$linterface->LAYOUT_START();
?>

<script language="javascript">
function jFILL_EMPTY_SCORE_WITH_NA(){
	if(confirm('<?=$eReportCard['jsFillAllNA']?>'))
		jSUBMIT_FORM();
}

function jSUBMIT_FORM(){
	var obj = document.FormMain;
	obj.submit();
}

function jBACK(){
	var form_id = document.getElementById("ClassLevelID").value;
	var report_id = document.getElementById("ReportID").value;
	var subject_id = document.getElementById("SubjectID").value;
	
	<?php if($isCmpSubject) { ?>
		var class_id = document.getElementById("ClassID").value;
		var parent_subject_id = document.getElementById("SubjectID").value;
		
		location.href = "marksheet_revision2.php?ClassLevelID="+form_id+"&ReportID="+report_id+"&SubjectID="+parent_subject_id+"&ClassID="+class_id;	
	<?php } else { ?>
		location.href = "marksheet_revision.php?ClassLevelID="+form_id+"&ReportID="+report_id+"&SubjectID="+subject_id;
	<?php } ?>
}
</script>

<br/>
<form name="FormMain" method="POST" action="marksheet_complete_update.php">
<table width="98%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center">
      <table width="96%" border="0" cellspacing="0" cellpadding="2">
	  	<tr>
	      <td>
	        <table border="0" cellpadding="5" cellspacing="0" class="tabletext">
	          <tr>
	            <td><?=$eReportCard['ReportCard']?> : <strong><?=$PeriodName?></strong></td>
	          </tr>
	        </table>
	        <br>
	        <table border="0" cellpadding="5" cellspacing="0" class="tabletext">
	          <tr>
	            <th><?=$eReportCard['RecordUpdatedSuccessfully']?></th>
	          </tr>
	        </table>
	        <table width="100%" border="0" cellpadding="5" cellspacing="0" class="tabletext">
	          <tr class="tabletop" height="30">
	            <td width="60" class="tabletoplink"><?=$eReportCard['Class']?></td>
	            <td class="tabletoplink"><?=$eReportCard['Subject']?></td>
	            <td width="80" align="center" class="tabletoplink"><?=$eReportCard['Marksheet']?></td>
	            <td width="200" align="center" class="tabletoplink"><?=$eReportCard['CurrentStatus']?></td>
	          </tr>
	          <?=$display_valid_record?>
	        </table>
	        <table width="100%" border="0" cellpadding="3" cellspacing="0" class="tabletext">
	          <tr class="resubtabletop">
			    <td class="tabletext"><?=$eReportCard['Total']." ".$valid_cnt?></td>
			  </tr>
	        </table>
	        <br>
	        <table border="0" cellpadding="5" cellspacing="0" class="tabletext">
	          <tr">
	            <th><?=$eReportCard['RecordShowingMSContainEmptyMark']?></th>
	          </tr>
	        </table>
	        <table width="100%" border="0" cellpadding="5" cellspacing="0" class="tabletext">
	          <tr class="tabletop" height="30">
	            <td width="60" class="tabletoplink"><?=$eReportCard['Class']?></td>
	            <td class="tabletoplink"><?=$eReportCard['Subject']?></td>
	            <td width="100" align="center" class="tabletoplink"><?=$eReportCard['CurrentStatus']?></td>
	            <td class="tabletoplink"><?=$eReportCard['Reason']?></td>
	            <td width="80" align="center" class="tabletoplink"><?=$eReportCard['Marksheet']?></td>
	          </tr>
	          <?=$display_invalid_record?>
	        </table>
	        <table width="100%" border="0" cellpadding="3" cellspacing="0" class="tabletext">
	          <tr class="resubtabletop">
			    <td class="tabletext"><?=$eReportCard['Total']." ".$invalid_cnt?></td>
			  </tr>
	        </table>
	      </td>
	    </tr>
	  </table>
	  <br>
      <table width="96%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td height="1" class="dotline"><img src="<?=$image_path.'/'.$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
        </tr>
      	<tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="2">
              <tr>
                <td align="center" valign="bottom"><?=$display_button?></td>
              </tr>
        	</table>
          </td>
      	</tr>
      </table>
    </td>
  </tr>
</table>

<input type="hidden" name="isProgress" id="isProgress" value="<?=$isProgress?>"/>
<input type="hidden" name="ClassLevelID" id="ClassLevelID" value="<?=$ClassLevelID?>"/>
<input type="hidden" name="ReportID" id="ReportID" value="<?=$ReportID?>"/>
<input type="hidden" name="SubjectID" id="SubjectID" value="<?=$SubjectID?>"/>
<input type="hidden" name="MarksheetRevision" id="MarksheetRevision" value="<?=$MarksheetRevision?>"/>

<?php 
if($isCmpSubject) { 
	echo '<input type="hidden" name="ClassID" id="ClassID" value="'.$ClassID.'"/>';
}

if(isset($CompleteData['invalid']) && count($CompleteData['invalid']) > 0){
	for($i=0 ; $i<count($CompleteData['invalid']) ;$i++){
		echo '<input type="hidden" name="ClassIDList[]" id="ClassIDList" value="'.$CompleteData['invalid'][$i]['data']['ClassID'].'"/>'."\n";
		echo '<input type="hidden" name="SubjectIDList[]" id="SubjectIDList" value="'.$CompleteData['invalid'][$i]['data']['SubjectID'].'"/>'."\n";
	}
}
?>

</form>
	

<?

	        $linterface->LAYOUT_STOP();
	    }
	    else
	    {
?>
			You have no priviledge to access this page.
<?
	    }
    }
}
else
{
?>
	You have no priviledge to access this page.
<?
}
?>
\