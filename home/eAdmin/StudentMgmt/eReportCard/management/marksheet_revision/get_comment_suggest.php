<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

if ($plugin['ReportCard2008']) {
	include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
	$lreportcard = new libreportcard();
	
    if ($lreportcard->hasAccessRight()) {
		// retrieve the keyword passed as parameter
		$keyword = $_GET['keyword'];
		
		// clear the output
		if(ob_get_length()) ob_clean();
		
		// send the results to the client
		$suggest = $lreportcard->GET_COMMENT_BANK_SUGGEST();
		
		echo $suggest;
    } else {
		echo "";
    }
} else {
	echo "";
}
?>