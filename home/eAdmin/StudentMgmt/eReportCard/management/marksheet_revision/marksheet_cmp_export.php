<?php
// Editing:
/*
 * Modification Log:
 * 20160414 (Bill)	[DM#2970]
 *  - fixed: display "0" when Subject Overall Mark = "+"
 * 20110829 (Ivan)
 * 	- added estimated mark logic
 */
 
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
//include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");

intranet_auth();
intranet_opendb();

#################################################################################################

include_once($PATH_WRT_ROOT."includes/libreportcard2008j.php");
$lreportcard = new libreportcard2008j();
//$lreportcard = new libreportcard();

$lexport = new libexporttext();

# Get Data

// Period (Term x, Whole Year, etc)
$PeriodArr = $lreportcard->GET_REPORT_TYPES($ClassLevelID, $ReportID);
$PeriodName = (count($PeriodArr) > 0) ? $PeriodArr['SemesterTitle'] : '';

// Class
$ClassArr = $lreportcard->GET_CLASSES_BY_FORM($ClassLevelID);
for($i=0 ; $i<count($ClassArr) ;$i++){
	if($ClassArr[$i][0] == $ClassID)
		$ClassName = $ClassArr[$i][1];
}

// SubjectName
$SubjectName = $lreportcard->GET_SUBJECT_NAME($SubjectID);
$SubjectName = str_replace("&nbsp;", "_", $SubjectName);


# Load Settings - Calculation
// Get the Calculation Method of Report Type - 1: Right-Down,  2: Down-Right
$SettingCategory = 'Calculation';
$categorySettings = $lreportcard->LOAD_SETTING($SettingCategory);
$TermCalculationType = $categorySettings['OrderTerm'];
$FullYearCalculationType = $categorySettings['OrderFullYear'];

# Load Settings - Storage & Display
$SettingStorDisp = 'Storage&Display';
$storDispSettings = $lreportcard->LOAD_SETTING($SettingStorDisp);

# Get Component Subject(s) Info
$isEdit = 1;					// default "1" as the marksheet is editable
$CmpSubjectArr = array();
$CmpSubjectIDArr = array();
$isCalculateByCmpSub = 0;		// set to "1" when one ScaleInput of component subjects is Mark(M)
$isCmpSubject = $lreportcard->CHECK_SUBJECT_IS_COMPONENT_SUBJECT($SubjectID);
if(!$isCmpSubject){
	$CmpSubjectArr = $lreportcard->GET_COMPONENT_SUBJECT($SubjectID, $ClassLevelID, '', '', $ReportID);
	
	$CmpSubjectIDArr = array();
	if(!empty($CmpSubjectArr)){	
		for($i=0 ; $i<count($CmpSubjectArr) ; $i++){
			$CmpSubjectIDArr[] = $CmpSubjectArr[$i]['SubjectID'];
			
			$CmpSubjectFormGradingArr = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $CmpSubjectArr[$i]['SubjectID'],0,0,$ReportID);
			if(count($CmpSubjectFormGradingArr) > 0 && $CmpSubjectFormGradingArr['ScaleInput'] == "M")
				$isCalculateByCmpSub = 1;
		}
	}
	// Check whether the marksheet is editable or not 
	$isEdit = $lreportcard->CHECK_MARKSHEET_STATUS($SubjectID, $ClassLevelID, $ReportID, $CmpSubjectIDArr);
}

# Get Data Either Assessment Column(s) in Term Or Term Column(s) in Wholely Report	
// Loading Report Template Data
$basic_data = $lreportcard->returnReportTemplateBasicInfo($ReportID);			// Basic  Data
$column_data = $lreportcard->returnReportTemplateColumnData($ReportID); 		// Column Data
$ColumnSize = sizeof($column_data);
$ColumnTitleAry = $lreportcard->returnReportColoumnTitle($ReportID);
$ReportTitle = $basic_data['ReportTitle'];
$ReportType = $basic_data['Semester'];
$isPercentage = $basic_data['PercentageOnColumnWeight'];

# Get Existing Report Calculation Method
$CalculationType = ($ReportType == "F") ? $FullYearCalculationType : $TermCalculationType;

# Get Report Column Weight
$WeightAry = array();
if($CalculationType == 2){
	if(count($column_data) > 0){
    	for($i=0 ; $i<sizeof($column_data) ; $i++){
	    	if(!empty($CmpSubjectArr))
	    	{
		    	for ($j=0; $j<count($CmpSubjectArr); $j++)
		    	{
			    	$cmp_subject_id = $CmpSubjectArr[$j]['SubjectID'];
			    	$OtherCondition = "SubjectID IS NULL AND ReportColumnID = ".$column_data[$i]['ReportColumnID'];
					$weight_data = $lreportcard->returnReportTemplateSubjectWeightData($ReportID, $OtherCondition); 
					$WeightAry[$weight_data[0]['ReportColumnID']][$cmp_subject_id] = $weight_data[0]['Weight'];
		    	}
	    	}
	    	else
	    	{
		    	$cmp_subject_id = $CmpSubjectArr[$j]['SubjectID'];
		    	$OtherCondition = "SubjectID IS NULL AND ReportColumnID = ".$column_data[$i]['ReportColumnID'];
				$weight_data = $lreportcard->returnReportTemplateSubjectWeightData($ReportID, $OtherCondition); 
				$WeightAry[$weight_data[0]['ReportColumnID']][$SubjectID] = $weight_data[0]['Weight'];
	    	}
		}
	}
}
else {
	$weight_data = $lreportcard->returnReportTemplateSubjectWeightData($ReportID); 			
	for($i=0 ; $i<sizeof($weight_data) ; $i++) {
		$thisReportColumnID = $weight_data[$i]['ReportColumnID'];
		$thisReportColumnID = ($thisReportColumnID=='')? 0 : $thisReportColumnID;
		$WeightAry[$thisReportColumnID][$weight_data[$i]['SubjectID']] = $weight_data[$i]['Weight'];
	}
}

$isAllCmpSubjectWeightZeroArr = $lreportcard->IS_ALL_CMP_SUBJECT_ZERO_WEIGHT($ReportID, $SubjectID);


# Get Student Info
if ($SubjectGroupID != '')
{
	$StudentArr = $lreportcard->GET_STUDENT_BY_SUBJECT_GROUP($SubjectGroupID, $ClassLevelID, '', 1);
	$obj_SubjectGroup = new subject_term_class($SubjectGroupID);
	$SubjectGroupName = $obj_SubjectGroup->Get_Class_Title();

	$Class_SG_Name = $SubjectGroupName;
}
else
{
	$StudentArr = $lreportcard->GET_STUDENT_BY_CLASS($ClassID, '', 1);
	$Class_SG_Name = $ClassName;
}
$StudentSize = sizeof($StudentArr);

$StudentIDArr = array();
if(count($StudentArr) > 0){
	for($i=0 ; $i<sizeof($StudentArr) ; $i++)
		$StudentIDArr[$i] = $StudentArr[$i]['UserID'];
}

# Get MarkSheet Score Info / ParentSubMarksheetScoreInfoArr	
$ColumnHasTemplateAry = array();
$hasDirectInputMark = 0;

if($ColumnSize > 0){
    for($i=0 ; $i<$ColumnSize ; $i++){
	    /* 
		* Check for the Mark Column is allowed to fill in mark when any term does not have related report template 
		* by SemesterNum (ReportType) && ClassLevelID
		* An array is initialized as a control variable [$ColumnHasTemplateAry]
		* A variable is initialized as a control variable [$hasDirectInputMark], must be used with [$ReportType == "F"]
		* to control the Import function to be used or not
		*/
		if($ReportType == "F"){
			$ColumnHasTemplateAry[$column_data[$i]['ReportColumnID']] = $lreportcard->CHECK_REPORT_TEMPLATE_FROM_COLUMN($column_data[$i]['SemesterNum'], $ClassLevelID);
			if(!$ColumnHasTemplateAry[$column_data[$i]['ReportColumnID']])
				$hasDirectInputMark = 1;
		}
		else {
			$ColumnHasTemplateAry[$column_data[$i]['ReportColumnID']] = 1;
		}
	}
}

####################################################################################################
# General Approach dealing with Search Term Result From Whole Year Report not From Term Report
# Can deal with Customization of SIS dealing with secondary template 
$isAllWholeReport = 0;
if($ReportType == "F"){
	$ReportTypeArr = array();
	$ReportTypeArr = $lreportcard->GET_REPORT_TYPES($ClassLevelID);
	if(count($ReportTypeArr) > 0){
		$flag = 0;
		for($j=0 ; $j<count($ReportTypeArr) ; $j++){
			if($ReportTypeArr[$j]['ReportID'] != $ReportID){
				if($ReportTypeArr[$j]['Semester'] != "F")	$flag = 1;
			}
		}
		if($flag == 0) $isAllWholeReport = 1;
	}
}

# Get the Corresponding Whole Year ReportID from 
# the ReportColumnID of the Current Whole Year Report 
$WholeYearColumnHasTemplateArr = array();
if($isAllWholeReport == 1){
	if($ReportType == "F" && $ColumnSize > 0){
		for($j=0 ; $j<$ColumnSize ; $j++){
			$ReportColumnID = $column_data[$j]['ReportColumnID'];
			# Return Whole Year Report Template ID - ReportID 
			$WholeYearColumnHasTemplateArr[$ReportColumnID] = $lreportcard->CHECK_WHOLE_YEAR_REPORT_TEMPLATE_FROM_COLUMN($ReportID, $column_data[$j]['SemesterNum'], $ClassLevelID);
			
			# Check Whether the Return WholeYear ReportID is not greater the Current ReportID
			# if User creates All Reports sequently
			if($WholeYearColumnHasTemplateArr[$ReportColumnID] != false){
				if($WholeYearColumnHasTemplateArr[$ReportColumnID] > $ReportID)
					$WholeYearColumnHasTemplateArr[$ReportColumnID] = false;
			}
		}
	}
}
####################################################################################################

# Get Special Case 
$possibleSpeicalCaseSet1 = $lreportcard->GET_POSSIBLE_MARKSHEET_SPECIAL_CASE_SET1();
$possibleSpeicalCaseSet2 = $lreportcard->GET_POSSIBLE_MARKSHEET_SPECIAL_CASE_SET2();

# Main - Preparation
$SchemeID = array();
$ScaleInput = array();		// M: Mark, G: Grade
$SchemeType = array();		// H: Honor Based, PF: PassFail
$InputGradeOption = array();
$InputPFOption = array();
$NewReportColumn = array();
$MarksheetScoreInfoArr = array();
$SchemeFullMark = array();
$ScaleInput = array();
$SchemeType = array();
$MarksheetOverallScoreInfoArr = array();
$ProgressArr = array();
$isSetSubjectOverall = array();

if(count($CmpSubjectArr) > 0 && $ColumnSize > 0){
	for($i=0 ; $i<$ColumnSize ; $i++){
		$report_column_id = $column_data[$i]['ReportColumnID'];
		
	    for($j=0 ; $j<count($CmpSubjectArr) ; $j++){
		    $cmp_subject_id = $CmpSubjectArr[$j]['SubjectID'];
		    
		    if ($eRCTemplateSetting['Marksheet']['HideZeroWeightSubjectComponent']) {
		    	$thisSubjectColumnWeight = $WeightAry[$report_column_id][$cmp_subject_id];
		    	if ($thisSubjectColumnWeight == 0) {
		    		continue;
		    	}
		    }
		    
		    if($isAllWholeReport == 1){
			    if($WholeYearColumnHasTemplateArr[$report_column_id] != false){
				    
			    } else {
					#######################################################################################################################################
					# for the Whole Year Report which defined any Term Column in other Whole Year Report	added on 22 May by Jason
					# the case for Whole Year Report Column Which cannot be found in Report Column of other Whole Year report
					# code enter here
					#######################################################################################################################################
					# Get Input Mark From Marksheet 
			    	$MarksheetScoreInfoArr[$report_column_id][$cmp_subject_id] = $lreportcard->GET_MARKSHEET_SCORE($StudentIDArr, $cmp_subject_id, $report_column_id);
					
					# Set Valid Report Columns into a new Column Array
			    	$column_data[$i]['CmpSubjectID'] = $cmp_subject_id;
			    	$NewReportColumn[] = $column_data[$i];
		    	}
			} else {
		    	if($ReportType != "F" || ($ReportType == "F" && !$ColumnHasTemplateAry[$report_column_id]) ){
			    	# Get Input Mark From Marksheet 
			    	$MarksheetScoreInfoArr[$report_column_id][$cmp_subject_id] = $lreportcard->GET_MARKSHEET_SCORE($StudentIDArr, $cmp_subject_id, $report_column_id);
			    	
			    	# Set Valid Report Columns into a new Column Array
			    	$column_data[$i]['CmpSubjectID'] = $cmp_subject_id;
			    	$NewReportColumn[] = $column_data[$i];
		    	}
	    	}
		}
	}
	
	for($j=0 ; $j<count($CmpSubjectArr) ; $j++){
		$cmp_subject_id = $CmpSubjectArr[$j]['SubjectID'];
		
		$isSetSubjectOverall[$CmpSubjectArr[$i]['SubjectID']] = 0;
		
		//Initialization of Grading Scheme
		$SchemeID[$cmp_subject_id] = '';
		$ScaleInput[$cmp_subject_id] = "M";		// M: Mark, G: Grade
		$SchemeType[$cmp_subject_id] = "H";		// H: Honor Based, PF: PassFail
		$SchemeInfo = array();
		$SubjectFormGradingArr = array();
		
		$SubjectFormGradingArr = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $cmp_subject_id,0,0,$ReportID);
		
		# Get Grading Scheme Info of that subject
		if(count($SubjectFormGradingArr) > 0){
			$SchemeID[$cmp_subject_id] = $SubjectFormGradingArr['SchemeID'];
			$ScaleInput[$cmp_subject_id] = $SubjectFormGradingArr['ScaleInput'];
			
			$SchemeMainInfo = $lreportcard->GET_GRADING_SCHEME_MAIN_INFO($SchemeID[$cmp_subject_id]);
			$SchemeType[$cmp_subject_id] = $SchemeMainInfo['SchemeType'];
			$SchemeFullMark[$cmp_subject_id] = $SchemeMainInfo['FullMark'];
		}
		
		
		if($SchemeType[$cmp_subject_id] == "H" && $ScaleInput[$cmp_subject_id] == "G"){	// Prepare Selection Box using Grade as Input
			$GradeOption[$cmp_subject_id] = array();
			
			$SchemeGrade = $lreportcard->GET_GRADE_FROM_GRADING_SCHEME_RANGE($SchemeID[$cmp_subject_id]);
			if(!empty($SchemeGrade)){
				foreach($SchemeGrade as $GradeRangeID => $grade)
					$GradeOption[$cmp_subject_id][$GradeRangeID] = $grade;
			}
		}
		else if($SchemeType[$cmp_subject_id] == "PF"){			// Prepare Selection Box using Pass/Fail as Input		
			$PFOption[$cmp_subject_id] = array();
			$PFOption[$cmp_subject_id]['P'] = $SchemeMainInfo['Pass'];
			$PFOption[$cmp_subject_id]['F'] = $SchemeMainInfo['Fail'];
		}
		
		# Get Overall Subject Mark Or Overall Term Subject Mark 
		# for InputScale is Grade or Scheme Type is PassFail
		if($SchemeType[$cmp_subject_id] == "PF" || ($SchemeType[$cmp_subject_id] == "H" && $ScaleInput[$cmp_subject_id] == "G") ){
			$MarksheetOverallScoreInfoArr[$cmp_subject_id] = $lreportcard->GET_MARKSHEET_OVERALL_SCORE($StudentIDArr, $cmp_subject_id, $ReportID);
		}
		
		# Get Complete Status of Common Subject or Parent Subject
	    $ProgressArr[$cmp_subject_id] = $lreportcard->GET_MARKSHEET_SUBMISSION_PROGRESS($cmp_subject_id, $ReportID, $ClassID);
	}
}


# Header 
$Content = "WebSAMSRegNumber,ClassName,ClassNumber,StudentName";
//$exportColumn[] = "WebSAMSRegNumber";
//$exportColumn[] = "ClassName";
//$exportColumn[] = "ClassNumber";
//$exportColumn[] = "StudentName";
$exportColumn[0][] = $eReportCard['ImportHeader']['ClassName']['En'];
$exportColumn[0][] = $eReportCard['ImportHeader']['ClassNumber']['En'];
$exportColumn[0][] = $eReportCard['ImportHeader']['UserLogin']['En'];
$exportColumn[0][] = $eReportCard['ImportHeader']['WebSamsRegNo']['En'];
$exportColumn[0][] = $eReportCard['ImportHeader']['StudentName']['En'];
$exportColumn[1][] = $eReportCard['ImportHeader']['ClassName']['Ch'];
$exportColumn[1][] = $eReportCard['ImportHeader']['ClassNumber']['Ch'];
$exportColumn[1][] = $eReportCard['ImportHeader']['UserLogin']['Ch'];
$exportColumn[1][] = $eReportCard['ImportHeader']['WebSamsRegNo']['Ch'];
$exportColumn[1][] = $eReportCard['ImportHeader']['StudentName']['Ch'];

$positionToAddOverall = count($CmpSubjectArr);
if(count($NewReportColumn) > 0){
	# Report Column Header
    for($i=0 ; $i<sizeof($NewReportColumn) ; $i++){
	    list($ReportColumnID, $ColumnTitle, $DefaultWeight, $SemesterNum) = $NewReportColumn[$i];
	    $CmpSubjectID = $NewReportColumn[$i]['CmpSubjectID'];
	    //$ColumnTitle = ($ColumnTitle != "" && $ColumnTitle != null) ? convert2unicode($ColumnTitle, 1, 2) : $ColumnTitleAry[$ReportColumnID];
	    $ColumnTitle = ($ColumnTitle != "" && $ColumnTitle != null) ? $ColumnTitle : $ColumnTitleAry[$ReportColumnID];
	    
	    if (($isCalculateByCmpSub && $isAllCmpSubjectWeightZeroArr[$ReportColumnID]) || $eRCTemplateSetting['ManualInputParentSubjectMark'])
	    {
		    $inputParentMark = true;
	    }
	    else
	    {
		    $inputParentMark = false;
	    }
	    
	    # Get Component Subject Name
	    $CmpSubjectNameEN = $lreportcard->GET_SUBJECT_NAME_LANG($CmpSubjectID, "EN");
	    $CmpSubjectNameCH = $lreportcard->GET_SUBJECT_NAME_LANG($CmpSubjectID, "CH");
	    $CmpSubjectName = '('.$CmpSubjectNameEN.' - '.$CmpSubjectNameCH.')';
	    
	    if( $WeightAry[$ReportColumnID][$CmpSubjectID] != 0 && $WeightAry[$ReportColumnID][$CmpSubjectID] != null && 
	    	$WeightAry[$ReportColumnID][$CmpSubjectID] != "" ){
//	    	$exportColumn[] = $ColumnTitle." ".$CmpSubjectName;
			$exportColumn[0][] = $ColumnTitle." ".$CmpSubjectName;
			$exportColumn[1][] = "(".$ColumnTitle." ".$CmpSubjectName.")";
	    	
    	} else {
//	    	$exportColumn[] = $ColumnTitle." ".$CmpSubjectName.'(N/A)';
			$exportColumn[0][] = $ColumnTitle." ".$CmpSubjectName."(N/A)";
			$exportColumn[1][] = "(".$ColumnTitle." ".$CmpSubjectName."(N/A)".")";

    	}
    	
    	### Subject Overall Column if all components are zero weight
    	if ($inputParentMark && ((($i+1) % $positionToAddOverall)==0))
	    {
//		    $exportColumn[] = $eReportCard['SubjectOverallMark'];
			$exportColumn[0][] =  $eReportCard['SubjectOverallMark'];
			$exportColumn[1][] = "(".$eReportCard['SubjectOverallMark'].")";
	    }
    }
    
    # Overall Subject Mark Header
    $overall_cnt = 0;
    for($i=0 ; $i<count($CmpSubjectArr) ; $i++){
    	$cmp_subject_id = $CmpSubjectArr[$i]['SubjectID'];
    	
    	if ($eRCTemplateSetting['Marksheet']['HideZeroWeightSubjectComponent']) {
    		$report_column_id = 0;
	    	$thisSubjectColumnWeight = $WeightAry[$report_column_id][$cmp_subject_id];
	    	if ($thisSubjectColumnWeight == 0) {
	    		continue;
	    	}
	    }
	    
		if($SchemeType[$cmp_subject_id] == "PF" || ($SchemeType[$cmp_subject_id] == "H" && $ScaleInput[$cmp_subject_id] == "G") ){
			# Get Component Subject Name
			$CmpSubjectNameEN = $lreportcard->GET_SUBJECT_NAME_LANG($NewReportColumn[$i]['CmpSubjectID'], "EN");
		    $CmpSubjectNameCH = $lreportcard->GET_SUBJECT_NAME_LANG($NewReportColumn[$i]['CmpSubjectID'], "CH");
		    $CmpSubjectName = '('.$CmpSubjectNameEN.' - '.$CmpSubjectNameCH.')';
		    
		    $ColumnTitle = ($ReportType == "F") ? $eReportCard['OverallSubjectMark'] : $eReportCard['TermSubjectMark'];
//			$exportColumn[] = $ColumnTitle.' '.$CmpSubjectName;
			$exportColumn[0][] = $ColumnTitle." ".$CmpSubjectName;
			$exportColumn[1][] = "(".$ColumnTitle." ".$CmpSubjectName.")";
			$overall_cnt++;
	   	}
	}
}

// content
for($j=0; $j<$StudentSize; $j++){
	list($StudentID, $WebSAMSRegNo, $StudentNo, $StudentName, $ClassName, $UserLogin) = $StudentArr[$j];
	
	$Content .= trim($WebSAMSRegNo).",".trim($ClassName).",".trim($StudentNo).",".trim($StudentName);
//	$row[] = trim($WebSAMSRegNo);
//    $row[] = trim($ClassName);
//	$row[] = trim($StudentNo);
//	$row[] = trim($StudentName);
    $row[] = trim($ClassName);
	$row[] = trim($StudentNo);
	$row[] = trim($UserLogin);
	$row[] = trim($WebSAMSRegNo);
	$row[] = trim($StudentName);
	
	if(count($NewReportColumn) > 0){
	    $inputmark_num = 0;
	    
	    for($i=0 ; $i<sizeof($NewReportColumn) ; $i++){
	        list($ReportColumnID, $ColumnTitle, $DefaultWeight, $SemesterNum) = $NewReportColumn[$i];
	        $CmpSubjectID = $NewReportColumn[$i]['CmpSubjectID'];
	        
	        if ( ($isCalculateByCmpSub && $isAllCmpSubjectWeightZeroArr[$ReportColumnID]) || $eRCTemplateSetting['ManualInputParentSubjectMark'] )
	        {
		    	$inputParentMark = true;   
	        }
	        else
	        {
		        $inputParentMark = false;
	        }
	        	    
	        if( $WeightAry[$ReportColumnID][$CmpSubjectID] != 0 && $WeightAry[$ReportColumnID][$CmpSubjectID] != null && 
	        	$WeightAry[$ReportColumnID][$CmpSubjectID] != "" ){
		        
		        $isSpecialCase = 0;
		        # Get Mark / GradeID / PassFailID
		        if(count($MarksheetScoreInfoArr) > 0){
				    // Mark Type : M-Mark, G-Grade, PF-PassFail, SC-SpecialCase
					$MarkType = $MarksheetScoreInfoArr[$ReportColumnID][$CmpSubjectID][$StudentID]['MarkType'];
				    
					$isSpecialCase = ($MarkType == "SC") ? 1 : 0;
					
				    if($MarkType == "G" || $MarkType == "PF" || $MarkType == "SC"){
				    	$tmpVal = $MarksheetScoreInfoArr[$ReportColumnID][$CmpSubjectID][$StudentID]['MarkNonNum'];
			    	} else if($MarkType == "M"){
						# For ScaleInput is Mark & SchemeType is Honor-Based
						# if MarkRaw is -1, the MarkRaw has not been assigned yet.
						# if MarkRaw >= 0, the MarkRaw is valid
				    	$tmpVal = ($MarksheetScoreInfoArr[$ReportColumnID][$CmpSubjectID][$StudentID]['MarkRaw'] != -1) ? $MarksheetScoreInfoArr[$ReportColumnID][$CmpSubjectID][$StudentID]['MarkRaw'] : '';
			    	} else {
				    	$tmpVal = '';
			    	}
			    } else {
				    $tmpVal = '';
			    }	
		        	
			    # Get the Exact Mark / Grade
			    if($isSpecialCase){ 
				    $Value = $tmpVal;
			    } else if($SchemeType[$CmpSubjectID] == "H" && $ScaleInput[$CmpSubjectID] == "G") {
		    		$Value = $GradeOption[$CmpSubjectID][$tmpVal];
	    		} else if($SchemeType[$CmpSubjectID] == "PF") {
		    		$Value = $PFOption[$CmpSubjectID][$tmpVal];
	    		} else {
		    		$Value = $tmpVal;
	    		}
			    
	        } else {
		        $Value = "--";
	        }
	        
	        // Check if the mark is estimated or not
    		$Value = ($MarksheetScoreInfoArr[$ReportColumnID][$CmpSubjectID][$StudentID]['IsEstimated'])? $lreportcard->Get_Estimated_Score_Display($Value) : $Value;
			$row[] = $Value;
			
			
			
			### all component zero weight => show parent overall mark input by user
	        if ($inputParentMark && ((($i+1) % $positionToAddOverall)==0) && ((($i+1) % $positionToAddOverall)==0))
		    {
			    $thisColumnScore = $lreportcard->GET_MARKSHEET_COLUMN_SCORE($StudentID, $ReportColumnID);
			    $thisRawMark = $thisColumnScore[$StudentID][$SubjectID]['MarkRaw'];
			    $MarkNonNum = $thisColumnScore[$StudentID][$SubjectID]['MarkNonNum'];
			   
			   	// [DM#2970] display $MarkNonNum for special case
			   	// $thisRawMark == -1 						: 	"-" or "*" or "/" or "N.A."
			   	// $thisRawMark == 0 && $MarkNonNum == "+"  : 	"+"		(added)
			    // $thisScore = ($thisRawMark != -1) ? $thisRawMark : $MarkNonNum;
			    $thisScore = ($thisRawMark != -1 && !($thisRawMark == 0 && $MarkNonNum == "+")) ? $thisRawMark : $MarkNonNum;
			    
			    // Check if the mark is estimated or not
    			$thisScore = ($thisColumnScore[$StudentID][$SubjectID]['IsEstimated'])? $lreportcard->Get_Estimated_Score_Display($thisScore) : $thisScore;
    		
			    $row[] = $thisScore;
		    }
	    }
	    
	    # Overall Term Subjcet Mark / Overall Subject Mark when (SchemeType is 'PF') OR (SchemeType is 'H' AND ScaleInput is 'G')
	    if($overall_cnt > 0){
			for($i=0 ; $i<count($CmpSubjectArr) ; $i++){
				$cmp_subject_id = $CmpSubjectArr[$i]['SubjectID'];
				
				if ($eRCTemplateSetting['Marksheet']['HideZeroWeightSubjectComponent']) {
					$report_column_id = 0;
					$thisSubjectColumnWeight = $WeightAry[$report_column_id][$cmp_subject_id];
					if ($thisSubjectColumnWeight == 0) {
						continue;
					}
				}
				
				if($SchemeType[$cmp_subject_id] == "PF"){	# PassFail Scheme
					$overall_value = (count($MarksheetOverallScoreInfoArr) > 0 && isset($MarksheetOverallScoreInfoArr[$cmp_subject_id]) ) ? $MarksheetOverallScoreInfoArr[$cmp_subject_id][$StudentID]['MarkNonNum'] : "";
					
					if(in_array($overall_value, $possibleSpeicalCaseSet1) || in_array($overall_value, $possibleSpeicalCaseSet2)){
						$Value = $overall_value;	# Special Case
					} else {
						$Value = $PFOption[$cmp_subject_id][$overall_value];
					}
				}
				else if($SchemeType[$cmp_subject_id] == "H" && $ScaleInput[$cmp_subject_id] == "G"){	# Grade Scheme
					$overall_value = (count($MarksheetOverallScoreInfoArr) > 0 && isset($MarksheetOverallScoreInfoArr[$cmp_subject_id]) ) ? $MarksheetOverallScoreInfoArr[$cmp_subject_id][$StudentID]['MarkNonNum'] : "";
					if(in_array($overall_value, $possibleSpeicalCaseSet1) || in_array($overall_value, $possibleSpeicalCaseSet2)){
						$Value = $overall_value;	# Special Case
					} else {
						$Value = $GradeOption[$cmp_subject_id][$overall_value];
					}
				}
				
				// Check if the mark is estimated or not
    			$Value = ($MarksheetOverallScoreInfoArr[$cmp_subject_id][$StudentID]['IsEstimated'])? $lreportcard->Get_Estimated_Score_Display($Value) : $Value;
				$row[] = $Value;
			}
		}
    }	
	
	$rows[] = $row;
	unset($row);
}


#################################################################################################
// Output the file to user browser
if($TagsType == 0)
	$Prefix = "Raw_";
else if($TagsType == 1)
	$Prefix = "Weighted_";
else if($TagsType == 2)
	$Prefix = "Grade_";
	
$set1 = array("&nbsp;", " ");
$set2 = array("-", ",");
$SubjectName = $lreportcard->GET_SUBJECT_NAME_LANG($SubjectID, "EN");

//$filename = $Prefix."Marksheet_".$PeriodName."_".$SubjectName."_".$Class_SG_Name.".csv";
$filename = $SubjectName.'_'.$Class_SG_Name.'_'.$Prefix.'CmpMarksheet.csv';
$filename = str_replace($set2, "_", $filename);
$filename = str_replace($set1, "_", $filename);


//$export_content = $lexport->GET_EXPORT_TXT($rows, $exportColumn);
$export_content = $lexport->GET_EXPORT_TXT_WITH_REFERENCE($rows, $exportColumn, "", "\r\n", "", 0, "11");

intranet_closedb();
$lexport->EXPORT_FILE($filename, $export_content);

?>