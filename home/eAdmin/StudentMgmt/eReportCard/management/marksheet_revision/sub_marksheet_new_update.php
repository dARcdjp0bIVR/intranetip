<?php
/*
 * Change Log:
 * Date:	2017-01-05 Villa	change param for submark sheet layer
 */

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

intranet_auth();
intranet_opendb();

if ($plugin['ReportCard2008'])
{
	include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
	if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
	{
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
	}
	
	$li = new libreportcard();

// 	$param = "SubjectID=$SubjectID&ClassID=$ClassID&ReportColumnID=$ReportColumnID&ReportID=$ReportID&jsReportColumn=$jsReportColumn&SubjectGroupID=$SubjectGroupID&UsePercent=$UsePercent";
	$param = "SubjectID=$SubjectID&ClassID=$ClassID&SchoolCode=$SchoolCode&ReportColumnID=$ReportColumnID&ReportID=$ReportID&jsReportColumn=$jsReportColumn&SubjectGroupID=$SubjectGroupID&SubReportColumnID=$SubReportColumnID&currentLevel=$currentLevel&UsePercent=$UsePercent";
	
	$columnInfo["SubjectID"] = $SubjectID;
	$columnInfo["ReportColumnID"] = $ReportColumnID;
	$columnInfo["ColumnTitle"] = $ColumnTitle;
	$columnInfo["Ratio"] = $UsePercent==1?$ColumnRatio/100:$ColumnRatio;
	$columnInfo["ColumnID"] = $ColumnID;
	$columnInfo["FullMark"] = $FullMark;
    // Add $PassingMark [2014-1121-1759-15164]
	$columnInfo["PassingMark"] = $PassingMark;
	$columnInfo["Calculation"] = ($subMS_Calculation=="average")? "A" : "F";
	// Multi-SupLayer
	$columnInfo["SubLevel"] = $currentLevel;
	$columnInfo["fromColumnID"] = $SubReportColumnID;
	
	if(!empty($ColumnID))
	{
		$success = $li->UPDATE_SUB_MARKSHEET_COLUMN($columnInfo);
	}
	else if(!empty($ReportColumnID))
	{
		$success = $li->INSERT_SUB_MARKSHEET_COLUMN($columnInfo);
	}

	if(!empty($ColumnID))
	{
		$Result = ($success==1) ? "update" : "update_failed";
		$url = ($success==1) ? "Location:sub_marksheet.php?$param&Result=$Result" : "Location:sub_marksheet_column_edit.php?$param&Result=$Result&ColumnID=$ColumnID";
	}
	else
	{
		$Result = ($success==1) ? "add" : "add_failed";
		$url = "Location:sub_marksheet.php?$param&Result=$Result";
	}
	
	intranet_closedb();
	header($url);
}
?>