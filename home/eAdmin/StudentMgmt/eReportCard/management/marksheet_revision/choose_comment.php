<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if ($plugin['ReportCard2008']) {
	include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
	include($PATH_WRT_ROOT."includes/eRCConfig.php");
	
	if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
	{
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
		$lreportcard = new libreportcardcustom();
	} else {
		$lreportcard = new libreportcard();
	}
	
	if (!isset($SubjectID, $fieldname)) {
		header("Location: /index.php");
		exit();
	}
	
	if ($lreportcard->hasAccessRight()) {

		$MODULE_OBJ['title'] = $eReportCard['SelectComment'];
		$linterface = new interface_html("popup.html");
		$linterface->LAYOUT_START();
		
		$CommentArr = $lreportcard->GET_COMMENT_BANK_SUGGEST($SubjectID, TRUE);
		$CommentOptions = array();
		$CommentEngOptions = array();
		$tempCount1 = 0;
		$tempCount2 = 0;
		for($i=0; $i<sizeof($CommentArr); $i++) {
			if (trim($CommentArr[$i]["Comment"]) != "") {
				# count substr before changing to htmlspecialchars
				# otherwise, "yeah" 'yeah' yeah will become "yeah" 'yeah#0
				//$tempComment = htmlspecialchars(stripslashes($CommentArr[$i]["Comment"]), ENT_QUOTES);
				//$CommentOptions[$tempCount1][0] = $tempComment;
				//$CommentOptions[$tempCount1][1] = mb_substr($tempComment, 0, 30);
				
				if ($eRCCommentSetting['ShowCompleteSentence'])
				{
					$tempComment = trim(intranet_htmlspecialchars($CommentArr[$i]["Comment"] , ENT_QUOTES));
					
					// Trim the line break of the comment in the "value" field
					$CommentOptions[$tempCount1][0] = str_replace(chr(13), ' ', $tempComment);
					$CommentOptions[$tempCount1][1] = $tempComment;
					$tempCount1++;
				}
				else
				{
					$tempCommentLong = trim(intranet_htmlspecialchars($CommentArr[$i]["Comment"] , ENT_QUOTES));
					$tempCommentShort = trim(intranet_htmlspecialchars(mb_substr($CommentArr[$i]["Comment"], 0, 30), ENT_QUOTES));
					
					// Trim the line break of the comment in the "value" field
					$tempCommentLong = str_replace(chr(13), ' ', $tempCommentLong);
					
					$CommentOptions[$tempCount1][0] = $tempCommentLong;
					$CommentOptions[$tempCount1][1] = $tempCommentShort;
					if (mb_strlen($CommentArr[$i]["Comment"]) > 30)
						$CommentOptions[$tempCount1][1] .="...";
					$tempCount1++;
				}
			}
			
			if (trim($CommentArr[$i]["CommentEng"]) != "") {
				//$tempCommentEng = htmlspecialchars(stripslashes($CommentArr[$i]["CommentEng"]), ENT_QUOTES);
				//$CommentEngOptions[$tempCount2][0] = $tempCommentEng;
				//$CommentEngOptions[$tempCount2][1] = mb_substr($tempCommentEng , 0, 30);
				
				if ($eRCCommentSetting['ShowCompleteSentence'])
				{
					$tempComment = trim(intranet_htmlspecialchars($CommentArr[$i]["CommentEng"] , ENT_QUOTES));
					// Trim the line break of the comment in the "value" field
					$CommentEngOptions[$tempCount2][0] = str_replace(chr(13), ' ', $tempComment);
					$CommentEngOptions[$tempCount2][1] = $tempComment;
					$tempCount2++;
				}
				else
				{
					$tempCommentLong = trim(intranet_htmlspecialchars($CommentArr[$i]["CommentEng"] , ENT_QUOTES));
					$tempCommentShort = trim(intranet_htmlspecialchars(mb_substr($CommentArr[$i]["CommentEng"], 0, 30), ENT_QUOTES));
					
					// Trim the line break of the comment in the "value" field
					$tempCommentLong = str_replace(chr(13), ' ', $tempCommentLong);
					
					$CommentEngOptions[$tempCount2][0] = $tempCommentLong;
					$CommentEngOptions[$tempCount2][1] = $tempCommentShort;
					if (mb_strlen($CommentArr[$i]["CommentEng"]) > 30)
						$CommentEngOptions[$tempCount2][1] .="...";
					$tempCount2++;
				}
			}
		}
		
		$select_comment = $linterface->GET_SELECTION_BOX($CommentOptions, 'name="Comment[]" id="Comment" size="21" multiple="multiple" onchange=""', '');
		$select_comment_eng = $linterface->GET_SELECTION_BOX($CommentEngOptions, 'name="Comment[]" id="Comment" size="21" multiple="multiple" onchange=""', '');
		
		//$select_comment = trim(str_replace("\n", "", $select_comment));
		//$select_comment_eng = trim(str_replace("\n", "", $select_comment_eng));
?>

<script language="javascript">
function isExists(obj2,val){
	for(j=0;j<obj2.options.length;j++) {
		if(obj2.options[j].value==val) return true;
	}
	return false;
}
function AddOptions(obj){
     par = opener.window;
     parObj = opener.window.document.form1.elements["<?php echo $fieldname; ?>"];
	 
     i = obj.selectedIndex;
	 
     while(i !=- 1) {
         addtext = Trim(obj.options[i].value) + "\n"; 
         par.checkOptionAdd(parObj, addtext);
		 obj.options[i] = null;
         i = obj.selectedIndex;
     }
}

function SelectAll(obj) {
	for (i=0; i<obj.length; i++)
	{
		obj.options[i].selected = true;
	}
}

function jsChangeLang() {
	var lang_eng = document.getElementById("lang_eng");
	var lang_chi = document.getElementById("lang_chi");
	var CommentSelectCell = document.getElementById("CommentSelectCell");
	
	if (lang_eng.checked) CommentSelectCell.innerHTML = '<?=str_replace("\n", "", $select_comment_eng)?>';
	if (lang_chi.checked) CommentSelectCell.innerHTML = '<?=str_replace("\n", "", $select_comment)?>';
}

</script>
<br />
<form name="form1" action="index.php" method="post">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>
			<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr>
					<td valign="top" nowrap="nowrap" class='tabletext'>
						<?=$eReportCard['Language']?>:
					</td>
					<td width="80%" class='tabletext'>
						<input type="radio" name="Language" id="lang_eng" value="eng" onclick="jsChangeLang()" <?=(!isset($Language) || $Language=="eng") ? "checked" : ""?> />
						<label for="lang_eng"><?=$i_general_english?></label> 
						<input type="radio" name="Language" id="lang_chi" value="chi" onclick="jsChangeLang()" <?=($Language=="chi") ? "checked" : ""?> />
						<label for="lang_chi"><?=$i_general_chinese?></label>
					</td>
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" class='tabletext'>
						<?=$eReportCard['CommentList']?>:
					</td>
					<td width="80%">
						<table width="100%" cellpadding="0" cellspacing="0">
							<tr>
								<td id="CommentSelectCell">
									<div id="EngCommentSelection"><?= $select_comment_eng ?></div>
									<div id="ChiCommentSelection" style="display:none"><?= $select_comment ?></div>
								</td>
								<td valign="bottom">
									<table width="100%" border="0" cellspacing="0" cellpadding="6">
										<tr> 
											<td align="left"> 
												<?= $linterface->GET_BTN($button_add, "button", "checkOption(document.getElementById('Comment'));AddOptions(document.getElementById('Comment'))")?>
											</td>
										</tr>
										<tr> 
											<td align="left"> 
												<?= $linterface->GET_BTN($button_select_all, "button", "SelectAll(document.getElementById('Comment')); return false;")?>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
                	<td class="dotline" colspan="2"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
				<tr>
					<td align="center" colspan="2">
						<?= $linterface->GET_ACTION_BTN($button_close, "button", "self.close();") ?>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<input type="hidden" name="fieldname" value="<?php echo $fieldname; ?>" />
</form>
</br />
<?
		intranet_closedb();
        $linterface->LAYOUT_STOP();
    } else {
    ?>
You have no priviledge to access this page.
    <?
    }
} else {
?>
You have no priviledge to access this page.
<?
}
?>