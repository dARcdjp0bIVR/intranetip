<?php
// Modifying by 

/********************************************************
 * Modification log
 * 20201103 (Bill)      [2020-0915-1830-00164]
 *      - support term setting    ($eRCTemplateSetting['Settings']['SubjectTopics_TermSettings'])
 * 20190502 Bill:
 *      - prevent SQL Injection + Cross-site Scripting 
 * 20181120 Bill    [2018-1120-1055-50206]
 *      - fixed - display PHP error message due to undefined $TeacherClassIDArr
 * 20181029 Bill    [2018-1029-1017-31066]
 *      - Class Teacher view - allow access to related subject groups if teach more than 1 classes
 * 20170519 Bill
 * 		- Allow View Group to access (view only)
 * 20170502 Bill	[2017-0412-1431-44256]
 * 		- Last Modified Date included Score Adjustment
 * 20170323 Bill	[2017-0125-1736-15225]
 * 		- added $eRCCommentSetting['ManualInputScore_ExtraReportOnly'] logic
 * 20170210 Villa
 * 		- HKUGA
 * 		- Allow Admin edit subject teacher comment
 * 20170119 Villa
 * 		- HKUGA cust
 * 		- hide attributes Btn if Form 6 or sem2
 * 		- hide comment Btn if sem 1 and NOT form 6
 * 20160428 Bill:	[2016-0302-1428-20207]
 * 		- display * if Component Subjects with Marksheet feedback in progress
 * 20160204 Bill:	[2015-1209-1137-25164]
 * 		- added $eRCTemplateSetting['Management']['ExportSubjectDetails'] to export marksheet input details
 * 20160129 Kenneth: [2016-0105-1050-50207]
 * 		- added $ExportCommentBtn, js_export_comment() - Export Techear Comment Button	(related to export_form_comment.php)
 * 20160120 Bill:	[2015-0421-1144-05164]
 * 		- added $eRCTemplateSetting['Marksheet']['ReportDisplayWithinSubmissionDate'] logic
 * 			display report within submission period only
 * 20151223 Ivan [X86217] [ip.2.5.7.1.1]:
 * 		- added $eRCTemplateSetting['Management']['Schedule']['SpecificMarksheetSubmissionPeriod_subjectBased'] logic to hide import function if using subject-based special submission period and within the special submission period
 * 		- added $eRCTemplateSetting['Marksheet']['DefaultReportUsingIssueDate'] logic
 * 20151216 Ivan [X86217] [ip.2.5.7.1.1]:
 * 		- added $eRCTemplateSetting['Marksheet']['ManualInputGrade'] logic
 * 20151026 Bill:	[2016-0113-1542-04164 removed]
 * 		- BIBA Cust
 * 		- display "-" (Teacher Comment) for KG Report
 * 20151014 Bill:
 * 		- BIBA Cust
 * 		- display "-" (Component Subject Marksheet, Marksheet, Feedback) for ES Progress Report
 * 		- fixed: not display edit icon for report without period setting (class teacher access through eService > eReportCard)
 * 20151012 Bill:
 * 		- Cust: Import Marksheet Score for All subjects
 * 		- 2015-0520-1100-36066 - 中華基督教青年會中學 - Import marksheet for whole school
 * 20150505 Bill:
 * 		- BIBA Cust
 * 		- display "-" and link to marksheet_subject_topic_edit.php if subject with subject topics
 * 		- edit js function jEDIT_MARKSHEET() to redirect to marksheet_subject_topic_edit.php
 * 20130205	Rita:
 * 		- add verification period checking - $accessSettingData[EnableVerificationPeriod]==1
 * 20120403 Marcus:
 * 		- Allow teacher to view marks even after submission period
 * 		- 2012-0208-1800-43132 - 沙田循道衛理中學 - Edit mark after submit the marksheet
 * 20120326 Marcus:
 * 		- Cater allow to set time for submission, verification
 * 		- 2012-0112-1622-04073 - 迦密愛禮信中學 - Schedule of Report Card Management
 * ******************************************************/

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
//include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libteaching.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");

intranet_auth();
intranet_opendb();

if ($plugin['ReportCard2008'])
{
    /* Temp Library*/
    include_once($PATH_WRT_ROOT."includes/libreportcard2008j.php");
    $lreportcard = new libreportcard2008j();
    
    if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") {
        include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
        $libreportcardcustom = new libreportcardcustom();
    }
    include_once($PATH_WRT_ROOT."includes/libreportcard2008_schedule.php");
    $lreportcard_schedule = new libreportcard_schedule();
    
    $CurrentPage = "Management_MarkSheetRevision";
    
    if ($lreportcard->hasAccessRight())
    {
        ### Handle SQL Injection + XSS [START]
        $ClassLevelID = IntegerSafe($ClassLevelID);
        $ReportID = IntegerSafe($ReportID);
        $SubjectID = IntegerSafe($SubjectID);
        ### Handle SQL Injection + XSS [END]
        
        $linterface = new interface_html();
        $PAGE_TITLE = $eReportCard['Management_MarksheetRevision'];
        
        $accessSettingData = $lreportcard->LOAD_SETTING("AccessSettings");
        
        // [2018-1120-1055-50206]
        $TeacherClassIDArr = array();
        
        if($ck_ReportCard_UserType=="TEACHER")
        {
            $PAGE_TITLE = $eReportCard['Management_MarksheetSubmission'];
            $lteaching = new libteaching();
            
            $ReportInfoArr = $lreportcard->returnReportTemplateBasicInfo($ReportID);
            $FormArrCT = $lreportcard->returnSunjectTeacherForm($UserID);
            
            # updated on 08 Dec 2008 by Ivan
            # added $special_feature['eRC_class_teacher_as_subject_teacher'] to check if the class teacher can view all subjects of the class or just the teaching subjects of the class
            # (for �װ�Ѱ| (�F�E�s) CRM Ref No.: 2008-1205-1459)
            # check is class teacher or not
            if (!$special_feature['eRC_class_teacher_as_subject_teacher'])
            {
                $TeacherClassAry = $lteaching->returnTeacherClassWithLevel($UserID, $lreportcard->schoolYearID);
                $TeacherClass = $TeacherClassAry[0]['ClassName'];
                $TeacherClassLevelID = $TeacherClassAry[0]['ClassLevelID'];
                $TeacherClassID = $TeacherClassAry[0]['ClassID'];
                
                // [2018-1029-1017-31066]
                //$TeacherClassIDArr = array();
                if(!empty($TeacherClassAry)) {
                    $TeacherClassIDArr = Get_Array_By_Key($TeacherClassAry, 'ClassID');
                }
                $TeacherClassLevelIDArr = array();
                if(!empty($TeacherClassAry)) {
                    $TeacherClassLevelIDArr = Get_Array_By_Key($TeacherClassAry, 'ClassLevelID');
                }
            }
            
            if(!empty($TeacherClass))
            {
                for($i=0;$i<sizeof($TeacherClassAry);$i++)
                {
                    $thisClassLevelID = $TeacherClassAry[$i]['ClassLevelID'];
                    $searchResult = multiarray_search($FormArrCT , "ClassLevelID", $thisClassLevelID);
                    if($searchResult == "-1")
                    {
                        $thisAry = array(
                            "0"=>$TeacherClassAry[$i]['ClassLevelID'],
                            "ClassLevelID"=>$TeacherClassAry[$i]['ClassLevelID'],
                            "1"=>$TeacherClassAry[$i]['LevelName'],
                            "LevelName"=>$TeacherClassAry[$i]['LevelName']);
                        $FormArrCT = array_merge($FormArrCT, array($thisAry));
                    }
                }
                
                # sort $FormArrCT
                foreach ($FormArrCT as $key => $row)
                {
                    $field1[$key] 	= $row['ClassLevelID'];
                    $field2[$key]  	= $row['LevelName'];
                }
                array_multisort($field1, SORT_ASC, $field2, SORT_ASC, $FormArrCT);
            }
            
            $ClassLevelID = ($ClassLevelID == "") ? $FormArrCT[0][0] : $ClassLevelID;
            $ClassArrCT = $lreportcard->returnSubjectTeacherClass($UserID, $ClassLevelID);
            
            $FormSubjectArrCT = $lreportcard->returnSubjectwOrder($ClassLevelID, 1, $UserID);
            
            if(!empty($TeacherClass))
            {
                $searchResult = multiarray_search($TeacherClassAry , "ClassLevelID", $ClassLevelID);
                if($searchResult != "-1")
                {
                    $searchResult2 = multiarray_search($ClassArrCT , "ClassName", $TeacherClassAry[$searchResult]['ClassName']);
                    if($searchResult2 == "-1")
                    {
                        $thisAry = array(
                            "0"=>$TeacherClassAry[$searchResult]['ClassID'],
                            "ClassID"=>$TeacherClassAry[$searchResult]['ClassID'],
                            "1"=>$TeacherClassAry[$searchResult]['ClassName'],
                            "ClassName"=>$TeacherClassAry[$searchResult]['ClassName'],
                            "2"=>$TeacherClassAry[$searchResult]['ClassLevelID'],
                            "ClassLevelID"=>$TeacherClassAry[$searchResult]['ClassLevelID']);
                        $ClassArrCT = array_merge($ClassArrCT, array($thisAry));
                    }
                    
                    $FormSubjectArrCT = $lreportcard->returnSubjectwOrder($ClassLevelID, 1);
                }
            }
        }
        
        ############################################################################################################
        
        # 3 GET Parameters : ClassLevelID, SubjectID, SemesterType_[ClassLevelID]
        # Get ClassLevelID (Form) of the reportcard template
        
        //$FormArr = $lreportcard->GET_ALL_FORMS(1);
        $FormArr = ($ck_ReportCard_UserType=="ADMIN" || $ck_ReportCard_UserType=="VIEW_GROUP")? $lreportcard->GET_ALL_FORMS(1) : $FormArrCT;
        $ClassLevelID = ($ClassLevelID == "") ? $FormArr[0][0] : $ClassLevelID;
        
        # Filters - By Form (ClassLevelID)
        $FormSelection = $linterface->GET_SELECTION_BOX($FormArr, "name='ClassLevelID' id='ClassLevelID' class='tabletexttoptext' onchange='jCHANGE_MARKSHEET_REVISION(1);'", "", $ClassLevelID);
        
        # Filters - By Type (Term1, Term2, Whole Year, etc)
        // Get Semester Type from the reportcard template corresponding to ClassLevelID
        $ReportTypeSelection = '';
        $ReportTypeArr = array();
        $ReportTypeOption = array();
        //2013-1119-1033-11156
        $orderBySubmissionTime = ($eRCTemplateSetting['Marksheet']['ReportSelectionOrderingSortBySubmissionDate'])? true : false;
        
        // [2015-0421-1144-05164]
        // Teacher - hide reports that out of submission period
        $displayReportInSubmissionPeriod = ($eRCTemplateSetting['Marksheet']['ReportDisplayWithinSubmissionDate'] && $ck_ReportCard_UserType=="TEACHER")? true : false;
        $ReportTypeArr = $lreportcard->GET_REPORT_TYPES($ClassLevelID, '', '', 0, $orderBySubmissionTime, $displayReportInSubmissionPeriod);
        
        # if not selected specific report card, preset the filter to the current submission period report template
        if ($ReportID == '')
        {
//			$conds = " 		ClassLevelID = '$ClassLevelID'
//							AND
//								NOW() BETWEEN MarksheetSubmissionStart AND MarksheetSubmissionEnd
//						";
//			$ReportInfoArr = $lreportcard->returnReportTemplateBasicInfo('', $conds, '', '', 1, '', $issueDate);
//			$ReportID = ($ReportInfoArr['ReportID'])? $ReportInfoArr['ReportID'] : '';
            $ReportID = $lreportcard->getReportFilteringDefaultReportId($ClassLevelID);
        }
        
        $ReportType = '';
        if(count($ReportTypeArr) > 0){
            for($j=0 ; $j<count($ReportTypeArr) ; $j++){
                if($ReportTypeArr[$j]['ReportID'] == $ReportID){
                    $isFormReportType = 1;
                    $ReportType = $ReportTypeArr[$j]['Semester'];
                }
                
                $ReportTypeOption[$j][0] = $ReportTypeArr[$j]['ReportID'];
                $ReportTypeOption[$j][1] = $ReportTypeArr[$j]['SemesterTitle'];
            }
            $ReportTypeSelection .= $linterface->GET_SELECTION_BOX($ReportTypeOption, 'name="ReportType" id="ReportType" class="tabletexttoptext" onchange="jCHANGE_MARKSHEET_REVISION(0)"', '', $ReportID);
            
            $ReportType = ($ReportType != "") ? $ReportType : $ReportTypeArr[0]['Semester'];
        }
        $ReportID = ($isFormReportType) ? $ReportID : $ReportTypeOption[0][0];
        $ReportSetting = $lreportcard->returnReportTemplateBasicInfo($ReportID);
        $isThisMainReport = $ReportSetting['isMainReport'];
        $SemID = $ReportSetting['Semester'];
        $SemNum = $lreportcard->Get_Semester_Seq_Number($SemID);
        $ClassLevelID = $ReportSetting['ClassLevelID'];
        
        // HKUGA Cust Villa
        $ISSemester = $lreportcard->Get_Semester_Seq_Number($SemID);
        $FormNumber = $lreportcard->GET_FORM_NUMBER($ClassLevelID);
// 		$isForm6 = $lreportcard->isFrom6($ClassLevelID);
        
        if ($SemID == 'F')
        {
            header("Location: marksheet_revision.php?ClassLevelID=$ClassLevelID&ReportID=$ReportID&SubjectID=$SubjectID");
            exit();
        }
        
        // used to check whether there is any marksheet feedback which is not closed.
        $MarksheetFeedback = $lreportcard->Get_Marksheet_Feedback_Info($ReportID, $SubjectID);
        $MarksheetFeedback = BuildMultiKeyAssoc($MarksheetFeedback, array("StudentID","SubjectID"),array("RecordStatus"),1);
        
        # Filters - By Form Subjects
        // A Flag, called $isFormSubject, is used to identify whether currect Form has that subject or not
        $FormSubjectArr = array();
        $SubjectListArr = array();
        $SubjectOption = array();
        $SubjectOption[0] = array("", "ALL");
        $count = 1;
        $isFormSubject = 0;		// Default: Assume the selected subject does not belongs to that Form(ClassLevelID)
        // Get Subjects By Form (ClassLevelID)
        //$FormSubjectArr = $lreportcard->returnSubjectwOrder($ClassLevelID, 1);
        $FormSubjectArr = ($ck_ReportCard_UserType=="ADMIN" || $ck_ReportCard_UserType=="VIEW_GROUP")? $lreportcard->returnSubjectwOrder($ClassLevelID, 1, '', '', 'Desc', 0, $ReportID) : $FormSubjectArrCT;
        
        // Hide Report Info if empty ReportID and only display reports that within submission period
        $hideAllReportInfo = false;
        if($displayReportInSubmissionPeriod && $ReportID==""){
            $FormSubjectArr = array();
            $hideAllReportInfo = true;
        }
        
        if(!empty($FormSubjectArr)){
            foreach($FormSubjectArr as $FormSubjectID => $Data){
                if(is_array($Data)){
                    foreach($Data as $FormCmpSubjectID => $SubjectName){
                        $FormSubjectID = ($FormCmpSubjectID==0) ? $FormSubjectID : $FormCmpSubjectID;
                        if($FormSubjectID == $SubjectID)
                            $isFormSubject = 1;
                            
                            // Prepare Subject Selection Box
                            if($FormCmpSubjectID==0){
                                $SubjectFormGradingArr = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $FormSubjectID,0,0,$ReportID);
                                $SubjectListArr[($count-1)]['SubjectID'] = $FormSubjectID;
                                $SubjectListArr[($count-1)]['SubjectName'] = $SubjectName;
                                $SubjectListArr[($count-1)]['SchemeID'] = $SubjectFormGradingArr['SchemeID'];
                                
                                $SubjectOption[$count][0] = $FormSubjectID;
                                $SubjectOption[$count][1] = $SubjectName;
                                $count++;
                            }
                    }
                }
            }
        }
        
        /*
         * Use for Selection Box
         * $SubjectListArr[][0] : SubjectID
         * $SubjectListArr[][1] : SubjectName
         */
        $isSelectAllSubject = $SubjectID=="";
        $SubjectID = ($isFormSubject) ? $SubjectID : '';
        if($SubjectID == ""){
            $SubjectID = $SubjectOption[0][0];
        }
        else {
            $SubjectFormGradingArr = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID,0,0,$ReportID);
            $SubjectListArr = array();
            $SubjectListArr[0]['SubjectID'] = $SubjectID;
            $SubjectListArr[0]['SubjectName'] = $lreportcard->GET_SUBJECT_NAME($SubjectID);
            $SubjectListArr[0]['SchemeID'] = $SubjectFormGradingArr['SchemeID'];
        }
        $SelectedSubjectName = $lreportcard->GET_SUBJECT_NAME($SubjectID);
        $SubjectSelection = $linterface->GET_SELECTION_BOX($SubjectOption, 'name="SubjectID" id="SubjectID" class="tabletexttoptext" onchange="jCHANGE_MARKSHEET_REVISION(0)"', '', $SubjectID);
        
        # Get Last Modified Info of This Report Template(Date-Time & Modified By who)
        $ReportColumnIDArr = array();
        $ReportColumnIDList = '';
        $column_data = $lreportcard->returnReportTemplateColumnData($ReportID); 	// Column Data
        if(count($column_data) > 0){
            for($i=0 ; $i<count($column_data) ; $i++)
                $ReportColumnIDArr[] = $column_data[$i]['ReportColumnID'];
                $ReportColumnIDList = implode(",", $ReportColumnIDArr);
        }
        $LastModifiedArr = $lreportcard->GET_SUBJECT_GROUP_MARKSHEET_LAST_MODIFIED('', $ReportColumnIDList, '', $SemID);
        $CommentLastModifiedArr = $lreportcard->GET_SUBJECT_GROUP_MARKSHEET_SUBJECT_COMMENT_LAST_MODIFIED('', $ReportColumnIDList, '', $SemID);
        
        
        # Get Status of Completed Action
        # $PeriodAction = "Submission" or "Verification"
        // $PeriodType & $PeriodAction can be used in teacher view
        $PeriodAction = "Submission";
        $PeriodType = $lreportcard->COMPARE_REPORT_PERIOD($ReportID, $PeriodAction);
        $OverallPeriodType = $PeriodType;
        $SpecialPeriodType = $lreportcard_schedule->compareSpecialMarksheetSubmissionPeriodForTeacher($ReportID, $_SESSION['UserID']);
        if ($SpecialPeriodType == 1) {
            $PeriodType = 1;
        }
        if($ck_ReportCard_UserType=="VIEW_GROUP") {
            $PeriodType = 2;
        }
        
        # Array of Selection Box of Complete Function
        $CompleteArr = array();
        $CompleteArr[] = array(1, $eReportCard['Confirmed']);
        $CompleteArr[] = array(0, $eReportCard['NotCompleted']);
        
        $CompleteAllArr = array();
        $CompleteAllArr[] = array("", "---".$eReportCard['ChangeAllTo']."---");
        $CompleteAllArr[] = array("completeAll", $eReportCard['Confirmed']);
        $CompleteAllArr[] = array("notCompleteAll", $eReportCard['NotCompleted']);
        
        # Main Content of Marksheet Reivision
        //$ClassArr = $lreportcard->GET_CLASSES_BY_FORM($ClassLevelID);
        //$ClassArr = $ck_ReportCard_UserType=="ADMIN" ? $lreportcard->GET_CLASSES_BY_FORM($ClassLevelID) : $ClassArrCT;
        
        # Subject List
        $AllSubjectColumnWeightStatus = array();
        $display = '';
        $cnt = 0;
        $numOfSubject = count($SubjectListArr);
        $numOfDisplayedSubjectGroup = 0;
        $allSubjectGroupIDArr = array();
        
        $SubjectIDArr = Get_Array_By_Key($SubjectListArr, 'SubjectID');
        $CmpSubjectAssoArr = $lreportcard->GET_COMPONENT_SUBJECT($SubjectIDArr, $ClassLevelID, $ParLang='', $ParDisplayType='Desc', $ReportID, $ReturnAsso=1);

        // [2020-0915-1830-00164]
        $targetTermSeq = 0;
        if ($eRCTemplateSetting['Settings']['SubjectTopics_TermSettings']) {
            $targetTermSeq = ($SemNum == 1 || $SemNum == 2) ? 1 : 2;
        }

        // BIBA Cust - get SubjectID with subject topics
        if($eRCTemplateSetting['Settings']['SubjectTopics'])
        {
            //$SubjectTopicArr = $lreportcard->CHECK_SUBJECT_WITH_SUBJECT_TOPICS($ClassLevelID);
            $SubjectTopicArr = $lreportcard->CHECK_SUBJECT_WITH_SUBJECT_TOPICS($ClassLevelID, '', $targetTermSeq);
        }
        
        // BIBA Cust - check report type
        if($eRCTemplateSetting['Settings']['AutoSelectReportType'])
        {
            $ReportBasicInfo = $lreportcard->returnReportTemplateBasicInfo($ReportID);
            $isProgressReport = !$ReportBasicInfo['isMainReport'];
            
            $FormNumber = $lreportcard->GET_FORM_NUMBER($ClassLevelID);
            // ES/MS Report
            if (is_numeric($FormNumber)) {
                $FormNumber = intval($FormNumber);
                $isESReport = $FormNumber > 0 && $FormNumber < 6;
                $isMSReport = !$isESReport;
            }
            // KG Report
            else {
                $isKGReport = true;
            }
        }
        
//		$lreportcard->Get_Subject_Marksheet_Feedback_Info($ReportID, $ClassLevelID, Get_Array_By_Key($AllSubjectGroupArr, 'SubjectGroupID'));
        for($i=0 ; $i<$numOfSubject ; $i++)
        {
            $thisSubjectName = $SubjectListArr[$i]['SubjectName'];
            $thisSubjectID = $SubjectListArr[$i]['SubjectID'];
            $thisSchemeID = $SubjectListArr[$i]['SchemeID'];
            
            // BIBA Cust
            $thisSubjectWithTopic = false;
            $thisParentSubjectWithTopic = false;
            // check if main subject with subject topic
            if($eRCTemplateSetting['Settings']['SubjectTopics'] && !$thisSubjectWithTopic && isset($SubjectTopicArr[$thisSubjectID][1])){
                $thisSubjectWithTopic = true;
                $thisParentSubjectWithTopic = true;
            }
            
            # Get component subject(s) if any
            $ParentLastModifiedArr = array();
            $CmpSubjectIDArr = array();
            //$CmpSubjectArr = array();
            //$CmpSubjectArr = $lreportcard->GET_COMPONENT_SUBJECT($thisSubjectID, $ClassLevelID, $ParLang='', $ParDisplayType='Desc', $ReportID);
            $CmpSubjectArr = (array)$CmpSubjectAssoArr[$thisSubjectID];
            
            if(!empty($CmpSubjectArr)){
                $CmpLastModifiedArr = array();
                for($k=0 ; $k<count($CmpSubjectArr) ; $k++){
                    $CmpSubjectIDArr[] = $CmpSubjectArr[$k]['SubjectID'];
                    
                    // BIBA Cust - check if component subject with subject topic
                    if($eRCTemplateSetting['Settings']['SubjectTopics'] && !$thisSubjectWithTopic && isset($SubjectTopicArr[$CmpSubjectArr[$k]['SubjectID']][1])){
                        $thisSubjectWithTopic = true;
                    }
                }
                
                // [2016-0302-1428-20207] get marksheet feedback of component subjects
                if(!$isSelectAllSubject){
                    $CmpSubjectFbArr = $CmpSubjectIDArr;
                    $CmpSubjectFbArr[] = $thisSubjectID;
                    $MarksheetFeedback = $lreportcard->Get_Marksheet_Feedback_Info($ReportID, $CmpSubjectFbArr);
                    $MarksheetFeedback = BuildMultiKeyAssoc($MarksheetFeedback, array("StudentID","SubjectID"),array("RecordStatus"),1);
                }
            }
            
//			# Check whether all column weights are zero or not corresponding to each subject of a report
//			$isAllColumnWeightZero = $lreportcard->IS_ALL_REPORT_COLUMN_WEIGHTS_ZERO($ReportID, $thisSubjectID);
//			$AllSubjectColumnWeightStatus[] = $isAllColumnWeightZero;
            
            # Check whether the marksheet is editable or not
            $isEdit = $lreportcard->CHECK_MARKSHEET_STATUS($thisSubjectID, $ClassLevelID, $ReportID, $CmpSubjectIDArr);
            
            # Get all Subject Groups of the Subject
            $TeacherID = '';
            
            // [2018-1029-1017-31066] - not Class Teacher / not teaching classes in this form
            //if ($ck_ReportCard_UserType=="TEACHER" && (empty($TeacherClass) || $TeacherClassLevelID!=$ClassLevelID))
            if ($ck_ReportCard_UserType=="TEACHER" && (empty($TeacherClass) || empty($TeacherClassLevelIDArr) || !in_array($ClassLevelID, (array)$TeacherClassLevelIDArr)))
            {
                # Subject Teacher => View teaching subject(s) only
                $TeacherID = $_SESSION['UserID'];
            }
            
            $obj_Subject = new Subject($thisSubjectID);
            $AllSubjectGroupArr = $obj_Subject->Get_Subject_Group_List($SemID, $ClassLevelID, '', $TeacherID);
            $SubjectGroupIDArr = Get_Array_By_Key($AllSubjectGroupArr, 'SubjectGroupID');
            $numOfSubjectGroup = count($AllSubjectGroupArr);

            # Get Subject Groups Students
            $SubjectStudentInfoAssoArr = $lreportcard->GET_STUDENT_BY_SUBJECT_GROUP($SubjectGroupIDArr, $ClassLevelID, '', 1, 1, 1);
            
            for ($j=0; $j<$numOfSubjectGroup; $j++)
            {
                $thisSubjectGroupID = $AllSubjectGroupArr[$j]['SubjectGroupID'];
                $thisSubjectComponentID = $AllSubjectGroupArr[$j]['SubjectComponentID'];
                $thisSubjectGroupTitleEN = $AllSubjectGroupArr[$j]['ClassTitleEN'];
                $thisSubjectGroupTitleB5 = $AllSubjectGroupArr[$j]['ClassTitleB5'];
                $thisSubjectGroupName = Get_Lang_Selection($thisSubjectGroupTitleB5, $thisSubjectGroupTitleEN);
                
                // Added checking for Subject Group Submission Period
                $thisSubjectGroupPeriodType = $PeriodType;
                if($eRCTemplateSetting['Management']['Schedule']['SpecificMarksheetSubmissionPeriod_subjectGroupBased'] && $PeriodType == 1 && $SpecialPeriodType == 1)
                {
                    $thisSubjectGroupPeriodType = $lreportcard_schedule->compareSpecialMarksheetSubmissionPeriodForTeacher($ReportID, $_SESSION['UserID'], $thisSubjectID, $thisSubjectGroupID);
                    if ($thisSubjectGroupPeriodType != 1) {
                        $thisSubjectGroupPeriodType = $OverallPeriodType;
                    }
                }
                
                //$StudentArr = $lreportcard->GET_STUDENT_BY_SUBJECT_GROUP($thisSubjectGroupID, $ClassLevelID, '', 1, 1);
                $StudentArr = $SubjectStudentInfoAssoArr[$thisSubjectGroupID];
                $SubjectGroupStudentList = Get_Array_By_Key((array)$StudentArr, "UserID");
                unset($StudentArr);
                
                # Check if the teacher is teaching this class / have class student enrolled in this class
                if ($ck_ReportCard_UserType!="ADMIN" && $ck_ReportCard_UserType!="VIEW_GROUP")
                {
                    $isTeachingThisSubjectGroup = $lreportcard->Is_Teaching_Subject_Group($_SESSION['UserID'], $thisSubjectGroupID);
                    
                    // [2018-1029-1017-31066] Check all teaching classes
                    //$hasClassStudentInThisSubjectGroup = $lreportcard->Has_Class_Student_Enrolled_In_Subject_Group($TeacherClassID, $thisSubjectGroupID);
                    $hasClassStudentInThisSubjectGroup = false;
                    foreach((array)$TeacherClassIDArr as $thisTeacherClassID) {
                        $hasClassStudentInThisSubjectGroup = $hasClassStudentInThisSubjectGroup || ($lreportcard->Has_Class_Student_Enrolled_In_Subject_Group($thisTeacherClassID, $thisSubjectGroupID));
                        if($hasClassStudentInThisSubjectGroup) {
                            break;
                        }
                    }
                    
                    if (!$isTeachingThisSubjectGroup && !$hasClassStudentInThisSubjectGroup) {
                        continue;
                    }
                }
                
                if ($thisSubjectComponentID==0 || $thisSubjectComponentID=='')
                    $thisTargetSubjectID = $thisSubjectID;
                    else
                        $thisTargetSubjectID = $thisSubjectComponentID;
                        
                        # Get Marksheet isComplete Status for each Subject Group
                        $ProgressAssoArr = $lreportcard->GET_MARKSHEET_SUBMISSION_PROGRESS($thisTargetSubjectID, $ReportID, '', $SubjectGroupIDArr, 1);
                        $ProgressAssoArr = $lreportcard->GET_MARKSHEET_SUBMISSION_PROGRESS($thisTargetSubjectID, $ReportID, '', $SubjectGroupIDArr, 1);
                        
                        
                        $isAllColumnWeightZero = $lreportcard->IS_ALL_REPORT_COLUMN_WEIGHTS_ZERO($ReportID, $thisTargetSubjectID);
                        //2014-0704-1231-53073
                        if ($eRCTemplateSetting['Marksheet']['CanInputMarksheetForZeroWeightSubject']) {
                            $isAllColumnWeightZero = 0;
                        }
                        $AllSubjectColumnWeightStatus[] = $isAllColumnWeightZero;
                        
                        # Record the displayed subject group
                        $numOfDisplayedSubjectGroup++;
                        $cnt++;
                        $allSubjectGroupIDArr[] = $thisSubjectGroupID;
                        
                        # Get Last Modified Time Among Component Subjects
                        $ParentLastModifiedArr = $lreportcard->GET_SUBJECT_GROUP_PARENT_MARKSHEET_LAST_MODIFIED($thisSubjectID, $CmpSubjectIDArr, $ReportColumnIDList, $thisSubjectGroupID, $SemID);
                        $ParentCommentLastModifiedArr = $lreportcard->GET_SUBJECT_GROUP_PARENT_MARKSHEET_SUBJECT_COMMENT_LAST_MODIFIED($thisSubjectID, $CmpSubjectIDArr, $ReportColumnIDList, $thisSubjectGroupID, $SemID);
                        
                        $effortLastModifiedAry = $lreportcard->GET_SUBJECT_GROUP_EXTRA_SUBJECT_INFO_LAST_MODIFIED($ReportID, $thisSubjectGroupID, $thisSubjectID);
                        $effortLastModifiedDate = (isset($effortLastModifiedAry[0]['DateModified'])) ? $effortLastModifiedAry[0]['DateModified'] : '';
                        $effortLastModifiedBy = (isset($effortLastModifiedAry[0]['TeacherName'])) ? $effortLastModifiedAry[0]['TeacherName'] : '';
                        
                        // [2017-0412-1431-44256] Score Adjustment
                        $ScoreAdjustLastModifiedAry = $lreportcard->GET_SUBJECT_GROUP_SCORE_ARCHIVE_INFO_LAST_MODIFIED($ReportID, $thisSubjectGroupID, $thisSubjectID);
                        $ScoreAdjustLastModifiedDate = (isset($ScoreAdjustLastModifiedAry[0]['DateModified'])) ? $ScoreAdjustLastModifiedAry[0]['DateModified'] : "";
                        $ScoreAdjustLastModifiedBy = (isset($ScoreAdjustLastModifiedAry[0]['TeacherName'])) ? $ScoreAdjustLastModifiedAry[0]['TeacherName'] : "";
                        
                        # Feedback
                        
                        # Last Modified Info
                        $LastModifiedDate = '';
                        $LastModifiedBy = '';
                        $LastModified = '-';
                        
                        if(!empty($CmpSubjectArr) && count($ParentLastModifiedArr) > 0 && ($thisSubjectComponentID=='' || $thisSubjectComponentID==0)){
                            $LastModifiedDate = (isset($ParentLastModifiedArr[$thisSubjectGroupID][0])) ? $ParentLastModifiedArr[$thisSubjectGroupID][0] : '';
                            $LastModifiedBy = (isset($ParentLastModifiedArr[$thisSubjectGroupID][1])) ? $ParentLastModifiedArr[$thisSubjectGroupID][1] : '';
                            
                            $ParentCommentLastModifiedDate = (isset($ParentCommentLastModifiedArr[$thisSubjectGroupID][0])) ? $ParentCommentLastModifiedArr[$thisSubjectGroupID][0] : '';
                            $ParentCommentLastModifiedBy = (isset($ParentCommentLastModifiedArr[$thisSubjectGroupID][1])) ? $ParentCommentLastModifiedArr[$thisSubjectGroupID][1] : '';
                            if ($ParentCommentLastModifiedDate != '' && $ParentCommentLastModifiedDate > $LastModifiedDate) {
                                $LastModifiedDate = $ParentCommentLastModifiedDate;
                                $LastModifiedBy = $ParentCommentLastModifiedBy;
                            }
                            
                            if ($effortLastModifiedDate != '' && $effortLastModifiedDate > $LastModifiedDate) {
                                $LastModifiedDate = $effortLastModifiedDate;
                                $LastModifiedBy = $effortLastModifiedBy;
                            }
                            
                            // [2017-0412-1431-44256]
                            if ($ScoreAdjustLastModifiedDate != '' && $ScoreAdjustLastModifiedDate > $LastModifiedDate) {
                                $LastModifiedDate = $ScoreAdjustLastModifiedDate;
                                $LastModifiedBy = $ScoreAdjustLastModifiedBy;
                            }
                            
                            if($LastModifiedDate != "") $LastModified = $LastModifiedBy.'<br>'.$LastModifiedDate;
                        }
                        else if(!empty($LastModifiedArr)){
                            //if($SubjectID != ""){
                            //	$LastModifiedDate = (isset($LastModifiedArr[$thisSubjectGroupID][0])) ? $LastModifiedArr[$thisSubjectGroupID][0] : '';
                            //	$LastModifiedBy = (isset($LastModifiedArr[$thisSubjectGroupID][1])) ? $LastModifiedArr[$thisSubjectGroupID][1] : '';
                            //}
                            //else {
                            
                            $LastModifiedDate = (isset($LastModifiedArr[$thisTargetSubjectID][$thisSubjectGroupID][0])) ? $LastModifiedArr[$thisTargetSubjectID][$thisSubjectGroupID][0] : '';
                            $LastModifiedBy = (isset($LastModifiedArr[$thisTargetSubjectID][$thisSubjectGroupID][1])) ? $LastModifiedArr[$thisTargetSubjectID][$thisSubjectGroupID][1] : '';
                            //}
                            
                            $SubjectCommentLastModifiedDate = (isset($CommentLastModifiedArr[$thisTargetSubjectID][$thisSubjectGroupID][0])) ? $CommentLastModifiedArr[$thisTargetSubjectID][$thisSubjectGroupID][0] : '';
                            $SubjectCommentLastModifiedBy = (isset($CommentLastModifiedArr[$thisTargetSubjectID][$thisSubjectGroupID][1])) ? $CommentLastModifiedArr[$thisTargetSubjectID][$thisSubjectGroupID][1] : '';
                            
                            if ($SubjectCommentLastModifiedDate != '' && $SubjectCommentLastModifiedDate > $LastModifiedDate) {
                                $LastModifiedDate = $SubjectCommentLastModifiedDate;
                                $LastModifiedBy = $SubjectCommentLastModifiedBy;
                            }
                            
                            if ($effortLastModifiedDate != '' && $effortLastModifiedDate > $LastModifiedDate) {
                                $LastModifiedDate = $effortLastModifiedDate;
                                $LastModifiedBy = $effortLastModifiedBy;
                            }
                            
                            // [2017-0412-1431-44256]
                            if ($ScoreAdjustLastModifiedDate != '' && $ScoreAdjustLastModifiedDate > $LastModifiedDate) {
                                $LastModifiedDate = $ScoreAdjustLastModifiedDate;
                                $LastModifiedBy = $ScoreAdjustLastModifiedBy;
                            }
                            
                            if($LastModifiedDate != "") $LastModified = $LastModifiedBy.'<br>'.$LastModifiedDate;
            }
            
            if ($eRCTemplateSetting['ShowPersonalCharInMarksheetRevision']) {
                # get last modified date
                if (isset($ReportID) && $ReportID != "") {
                    $conds = " AND pcd.ReportID = $ReportID ";
                }
                $namefield = getNameFieldByLang2("iu.");
                $table = $lreportcard->DBName .".RC_PERSONAL_CHARACTERISTICS_DATA ";
                $sql = "SELECT
                Max(pcd.DateModified) as LastModifiedDate,
                $namefield as LastModifiedBy
                FROM
                $table as pcd
                Inner Join
                INTRANET_USER as iu On (pcd.TeacherID = iu.UserID)
                WHERE
                pcd.SubjectID = '$thisSubjectID'
                And
                pcd.SubjectGroupID = '$thisSubjectGroupID'
                $conds
                Group By
                pcd.ReportID, pcd.SubjectID, pcd.SubjectGroupID
                ";
                $result = $lreportcard->returnArray($sql);
                
                $PcLastModifiedDate = $result[0]['LastModifiedDate'];
                $PcLastModifiedBy = $result[0]['LastModifiedBy'];
                
                if ($PcLastModifiedDate != '' && $PcLastModifiedBy != '') {
                    $showPcLastModified = false;
                    
                    if ($LastModifiedDate == '') {
                        $showPcLastModified = true;
                    }
                    if ($PcLastModifiedDate > $LastModifiedDate) {
                        $showPcLastModified = true;
                    }
                    
                    if ($showPcLastModified) {
                        $LastModified = $PcLastModifiedBy.'<br>'.$PcLastModifiedDate;
                    }
                }
            }
            
            # State of IsCompleted
            //$ProgressArr = array();
            //$ProgressArr = $lreportcard->GET_MARKSHEET_SUBMISSION_PROGRESS($thisTargetSubjectID, $ReportID, '', $thisSubjectGroupID);
            $ProgressArr = (array)$ProgressAssoArr[$thisSubjectGroupID];
            
            # Initialization - Display Icon
            $CmpSubjectIcon = "-";
            $CmpSubjectMSIcon = "-";
            $MarksheetIcon = "-";
            $TCommentIcon = "-";
            $ReportPreviewIcon = "-";
            $FeedbackIcon = "-";
            # Added on 20080829 by Andy
            $ExtraInfoIcon = "-";
            # Added on 20081208 by Ivan
            $AdjustPositionIcon = "-";
            
            if ($eRCTemplateSetting['ShowPersonalCharInMarksheetRevision']) {
                $PersonalCharIcon = '<a href="javascript:jEDIT_PERSONAL_CHAR(\''.$thisSubjectGroupID.'\', \''.$thisSubjectID.'\', 2)"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_edit_b.gif" width="20" height="20" border="0" title="'.$button_edit.' '.$eReportCard['PersonalCharacteristics'].'"></a>';
            }
            //2017-01-19 Villa HKUGA Cust
//             if($sys_custom['eRC']['Report']['HKUGAHideComment'] && ($ISSemester == '2' || $FormNumber=='6')){
//                 $PersonalCharIcon = '-';
//             }
            $_hidePersonalCharIcon = false;
            if ($sys_custom['eRC']['Report']['HKUGAHideComment']){
                if ($ISSemester == '2' || $FormNumber=='6') {
                    $_hidePersonalCharIcon = true;
                }
                if ($FormNumber=='3' && $libreportcardcustom->isModularStudySubject($thisSubjectID)) {
                    $_hidePersonalCharIcon = true;
                }
            }
            if ($_hidePersonalCharIcon) {
                $PersonalCharIcon = '-';
            }
            
            if ($eRCTemplateSetting['PreviewReportSubjectSectionInMarksheet']) {
                $SubjectGroupStudentPara = '';
                if (count((array)$SubjectGroupStudentList) > 0) {
                    $SubjectGroupStudentPara = '&TargetStudentID[]='.implode('&TargetStudentID[]=', (array)$SubjectGroupStudentList);
                }
                $ReportPreviewIcon_HTML = "<a href=\"javascript:newWindow('../../reports/generate_reports/print_preview.php?ReportID=$ReportID&SubjectID=$thisSubjectID&SubjectSectionOnly=1".$SubjectGroupStudentPara."', '10');\" class=\"tablelink\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_view.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\" title=\"".$Lang['eReportCard']['ManagementArr']['MarksheetRevisionArr']['PreviewReport']."\"></a>";
            }
            
            
            
            if ($thisSubjectComponentID=='' || $thisSubjectComponentID==0)
                $thisNumOfCmpSubjectDisplay = count($CmpSubjectArr);
                else
                    $thisNumOfCmpSubjectDisplay = 1;
                    
                    if($isAllColumnWeightZero == 0){	// if Any Report Column Weight is not Zero
                        
                        // check whether there is any marksheet feedback which is not closed.
                        $InProgressRemarks = '';
                        $CmpInProgressRemarks = '';
                        foreach($SubjectGroupStudentList as $thisStudentID)
                        {
                            // [2016-0302-1428-20207] check if any marksheet feedback of subject component which is not closed.
                            if(!empty($CmpSubjectIDArr))
                            {
                                foreach((array)$CmpSubjectIDArr as $thisCmpSubjectID)
                                {
                                    if($MarksheetFeedback[$thisStudentID][$thisCmpSubjectID]==2)
                                    {
                                        $CmpInProgressRemarks = "<span class='tabletextrequire'>*</span>";
                                        break;
                                    }
                                }
                            }
                            
                            if($MarksheetFeedback[$thisStudentID][$thisSubjectID]==2)
                            {
                                $InProgressRemarks = "<span class='tabletextrequire'>*</span>";
                                break;
                            }
                        }
                        
                        if($ck_ReportCard_UserType == "TEACHER")
                        {
                            //switch($PeriodType)
                            switch($thisSubjectGroupPeriodType)
                            {
                                case -1:	 	# Period not set yet (NULL)
                                case 0:			# before the Period
                                    break;
                                case 1:			# within  the Period
                                    $CmpSubjectIcon = ((!empty($CmpSubjectArr)) ? '<a href="javascript:jEDIT_MARKSHEET(\''.$thisSubjectGroupID.'\', \''.$thisSubjectID.'\', 0)" class="tablegreenlink"><strong>'.$thisNumOfCmpSubjectDisplay.'</strong></a>' : '-');
                                    
                                    if ($thisSubjectComponentID=='' || $thisSubjectComponentID==0)
                                    {
                                        if($thisSchemeID != null && $thisSchemeID != "")
                                        {
                                            $MarksheetIcon = '<a href="javascript:jEDIT_MARKSHEET(\''.$thisSubjectGroupID.'\', \''.$thisSubjectID.'\', 1)">';
                                            $MarksheetIcon .= (($isEdit && !empty($CmpSubjectArr)) || (empty($CmpSubjectArr) && $ReportType != "F") || ($isEdit && $ReportType == "F" && empty($CmpSubjectArr)) ) ? '<img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_edit_b.gif" width="20" height="20" border="0" title="'.$button_edit.' '.$eReportCard['Marksheet'].'">' : '<img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_view.gif" width="20" height="20" border="0" title="'.$button_view.' '.$eReportCard['Marksheet'].'">';
                                            $MarksheetIcon .= '</a>';
                                            
                                            $FeedbackIcon = '<a href="#"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_view.gif" width="20" height="20" border="0" title="'.$button_view.' '.$eReportCard['Feedback'].'"></a>';
                                            
                                            $CmpSubjectMSIcon = ((!empty($CmpSubjectArr)) ? '<a href="javascript:jEDIT_MARKSHEET(\''.$thisSubjectGroupID.'\', \''.$thisSubjectID.'\', 2)"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_edit_b.gif" width="20" height="20" border="0" title="'.$button_edit.' '.$eReportCard['CmpSubjectMarksheet'].'"></a>' : '-');
                                            
                                            // BIBA Cust - for subject with subject topic
                                            if ($thisSubjectWithTopic){
                                                $MarksheetIcon = "-";
                                                $FeedbackIcon = "-";
                                                $CmpSubjectMSIcon = "-";
                                                // Subject - redirect to marksheet_subject_topic_edit.php
                                                //											if($thisParentSubjectWithTopic){
                                                $MarksheetIcon = '<a href="javascript:jEDIT_MARKSHEET(\''.$thisSubjectGroupID.'\', \''.$thisSubjectID.'\', 3)">';
                                                $MarksheetIcon .= ((!empty($CmpSubjectArr)) || (empty($CmpSubjectArr) && $ReportType != "F"))? '<img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_edit_b.gif" width="20" height="20" border="0" title="'.$button_edit.' '.$eReportCard['Marksheet'].'">' : '<img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_view.gif" width="20" height="20" border="0" title="'.$button_view.' '.$eReportCard['Marksheet'].'">';
                                                $MarksheetIcon .= '</a>';
                                                //											}
                                                }
                                            }
                                            else {
                                                $MarksheetIcon = 'X';
                                                $FeedbackIcon = 'X';
                                                $CmpSubjectMSIcon = 'X';
                                            }
                                            $TCommentIcon = '<a href="javascript:jEDIT_TEACHER_COMMENT(\''.$thisSubjectGroupID.'\', \''.$thisSubjectID.'\')"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_edit_b.gif" width="20" height="20" border="0" title="'.$button_edit.' '.$eReportCard['TeacherComment'].'"></a>';
                                            //									$FeedbackIcon = '<a href="#"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_view.gif" width="20" height="20" border="0" title="'.$button_view.' '.$eReportCard['Feedback'].'"></a>';
                                            
                                            $ExtraInfoIcon = '<a href="javascript:jEDIT_EXTRA_INFO(\''.$thisSubjectGroupID.'\', \''.$thisSubjectID.'\')"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_edit_b.gif" width="20" height="20" border="0" title="'.$button_edit.' '.$eReportCard['ExtraInfoLabel'].'"></a>';
                                            
                                            # uccke only allows admin to use the manual adjust position
                                            if ($ReportCardCustomSchoolName != "ucc_ke") {
                                                $AdjustPositionIcon = '<a href="javascript:jEDIT_ADJUST_POSITION(\''.$thisSubjectGroupID.'\', \''.$thisSubjectID.'\')"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_edit_b.gif" width="20" height="20" border="0" title="'.$button_edit.' '.$eReportCard['AdjustPositionLabal'].'"></a>';
                                            }
                                            
                                            //									if ($eRCTemplateSetting['PreviewReportSubjectSectionInMarksheet']) {
                                            //								  		$ReportPreviewIcon = $ReportPreviewIcon_HTML;
                                            //								  	}
                                        }
                                        else
                                        {
                                            $TCommentIcon = '<a href="javascript:jEDIT_TEACHER_COMMENT(\''.$thisSubjectGroupID.'\', \''.$thisSubjectID.'\')"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_edit_b.gif" width="20" height="20" border="0" title="'.$button_edit.' '.$eReportCard['TeacherComment'].'"></a>';
                                        }
                                        
                                        break;
                                        case 2:			# after the Period
                                            // [2016-0302-1428-20207]
                                            $CmpSubjectIcon = ((!empty($CmpSubjectArr)) ? '<a href="javascript:jEDIT_MARKSHEET(\''.$thisSubjectGroupID.'\', \''.$thisSubjectID.'\', 0)" class="tablegreenlink"><strong>'.$thisNumOfCmpSubjectDisplay.'</strong></a>'.$CmpInProgressRemarks : '-');
                                            
                                            if ($thisSubjectComponentID=='' || $thisSubjectComponentID==0)
                                            {
                                                //									$CmpSubjectMSIcon = '-';
                                                if($thisSchemeID != null && $thisSchemeID != "")
                                                {
                                                    //										$MarksheetIcon = '-';
                                                    $MarksheetIcon = '<a href="javascript:jEDIT_MARKSHEET(\''.$thisSubjectGroupID.'\', \''.$thisSubjectID.'\', 1)">';
                                                    $MarksheetIcon .= (($isEdit && !empty($CmpSubjectArr)) || (empty($CmpSubjectArr) && $ReportType != "F") || ($isEdit && $ReportType == "F" && empty($CmpSubjectArr)) ) ? '<img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_edit_b.gif" width="20" height="20" border="0" title="'.$button_edit.' '.$eReportCard['Marksheet'].'">' : '<img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_view.gif" width="20" height="20" border="0" title="'.$button_view.' '.$eReportCard['Marksheet'].'">';
                                                    $MarksheetIcon .= '</a>';
                                                    
                                                    $FeedbackIcon = '<a href="javascript:jVIEW_FEEDBACK(\''.$thisSubjectGroupID.'\', \''.$thisSubjectID.'\')"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_view.gif" width="20" height="20" border="0" title="'.$button_preview.' '.$eReportCard['Feedback'].'"></a>'.$InProgressRemarks;
                                                    
                                                    $CmpSubjectMSIcon = ((!empty($CmpSubjectArr)) ? '<a href="javascript:jEDIT_MARKSHEET(\''.$thisSubjectGroupID.'\', \''.$thisSubjectID.'\', 2)"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_edit_b.gif" width="20" height="20" border="0" title="'.$button_edit.' '.$eReportCard['CmpSubjectMarksheet'].'"></a>' : '-');
                                                    
                                                    // BIBA Cust - for subject with subject topic
                                                    if ($thisSubjectWithTopic){
                                                        $MarksheetIcon = "-";
                                                        $FeedbackIcon = "-";
                                                        $CmpSubjectMSIcon = "-";
                                                        // Subject - redirect to marksheet_subject_topic_edit.php
                                                        //											if($thisParentSubjectWithTopic){
                                                        $MarksheetIcon = '<a href="javascript:jEDIT_MARKSHEET(\''.$thisSubjectGroupID.'\', \''.$thisSubjectID.'\', 3)">';
                                                        $MarksheetIcon .= ((!empty($CmpSubjectArr)) || (empty($CmpSubjectArr) && $ReportType != "F"))? '<img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_edit_b.gif" width="20" height="20" border="0" title="'.$button_edit.' '.$eReportCard['Marksheet'].'">' : '<img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_view.gif" width="20" height="20" border="0" title="'.$button_view.' '.$eReportCard['Marksheet'].'">';
                                                        $MarksheetIcon .= '</a>';
                                                        //											}
                                                        }
                                                    }
                                                    else {
                                                        $MarksheetIcon = 'X';
                                                        $FeedbackIcon = 'X';
                                                        $CmpSubjectMSIcon = 'X';
                                                    }
                                                    //$TCommentIcon = '<a href="javascript:jEDIT_TEACHER_COMMENT(\''.$thisSubjectGroupID.'\', \''.$thisSubjectID.'\')"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_view.gif" width="20" height="20" border="0" title="'.$button_preview.' '.$eReportCard['TeacherComment'].'"></a>';
                                                    //							    	$FeedbackIcon = '<a href="javascript:jVIEW_FEEDBACK(\''.$thisSubjectGroupID.'\', \''.$thisSubjectID.'\')"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_view.gif" width="20" height="20" border="0" title="'.$button_preview.' '.$eReportCard['Feedback'].'"></a>';
                                                    
                                                    //									if ($eRCTemplateSetting['PreviewReportSubjectSectionInMarksheet']) {
                                                    //								  		$ReportPreviewIcon = "X";
                                                    //								  	}
                                            }
                                            
                                            break;
                            }
                            
                            if ($eRCTemplateSetting['PreviewReportSubjectSectionInMarksheet']) {
                                $ReportPreviewIcon = $ReportPreviewIcon_HTML;
                            }
                        }
                        else
                        {
                            // [2016-0302-1428-20207]
                            $CmpSubjectIcon = ((!empty($CmpSubjectArr)) ? '<a href="javascript:jEDIT_MARKSHEET(\''.$thisSubjectGroupID.'\', \''.$thisSubjectID.'\', 0)" class="tablegreenlink"><strong>'.$thisNumOfCmpSubjectDisplay.'</strong></a>'.$CmpInProgressRemarks : '-');
                            
                            if ($thisSubjectComponentID=='' || $thisSubjectComponentID==0)
                            {
                                if($thisSchemeID != null && $thisSchemeID != "")
                                {
                                    $MarksheetIcon = '<a href="javascript:jEDIT_MARKSHEET(\''.$thisSubjectGroupID.'\', \''.$thisSubjectID.'\', 1)">';
                                    if($ck_ReportCard_UserType == "VIEW_GROUP") {
                                        $MarksheetIcon .= '<img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_view.gif" width="20" height="20" border="0" title="'.$button_view.' '.$eReportCard['Marksheet'].'">';
                                    }
                                    else {
                                        $MarksheetIcon .= (($isEdit && !empty($CmpSubjectArr)) || (empty($CmpSubjectArr) && $ReportType != "F") || ($isEdit && $ReportType == "F" && empty($CmpSubjectArr)) ) ? '<img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_edit_b.gif" width="20" height="20" border="0" title="'.$button_edit.' '.$eReportCard['Marksheet'].'">' : '<img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_view.gif" width="20" height="20" border="0" title="'.$button_view.' '.$eReportCard['Marksheet'].'">';
                                    }
                                    $MarksheetIcon .= '</a>';
                                    
                                    $FeedbackIcon = '<a href="javascript:jVIEW_FEEDBACK(\''.$thisSubjectGroupID.'\', \''.$thisSubjectID.'\')"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_view.gif" width="20" height="20" border="0" title="'.$button_view.' '.$eReportCard['Feedback'].'"></a>'.$InProgressRemarks;
                                    
                                    $CmpSubjectMSIcon = ((!empty($CmpSubjectArr)) ? '<a href="javascript:jEDIT_MARKSHEET(\''.$thisSubjectGroupID.'\', \''.$thisSubjectID.'\', 2)"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_edit_b.gif" width="20" height="20" border="0" title="'.$button_edit.' '.$eReportCard['CmpSubjectMarksheet'].'"></a>' : '-');
                                    if($ck_ReportCard_UserType=="VIEW_GROUP" && !empty($CmpSubjectArr)) {
                                        $CmpSubjectMSIcon = '<a href="javascript:jEDIT_MARKSHEET(\''.$thisSubjectGroupID.'\', \''.$thisSubjectID.'\', 2)"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_view.gif" width="20" height="20" border="0" title="'.$button_view.' '.$eReportCard['CmpSubjectMarksheet'].'"></a>';
                                    }
                                    
                                    // BIBA Cust - for subject with subject topic
                                    if ($thisSubjectWithTopic){
                                        $MarksheetIcon = "-";
                                        $FeedbackIcon = "-";
                                        $CmpSubjectMSIcon = "-";
                                        // Subject - redirect to marksheet_subject_topic_edit.php
                                        //									if($thisParentSubjectWithTopic){
                                        $MarksheetIcon = '<a href="javascript:jEDIT_MARKSHEET(\''.$thisSubjectGroupID.'\', \''.$thisSubjectID.'\', 3)">';
                                        $MarksheetIcon .= ((!empty($CmpSubjectArr)) || (empty($CmpSubjectArr) && $ReportType != "F"))? '<img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_edit_b.gif" width="20" height="20" border="0" title="'.$button_edit.' '.$eReportCard['Marksheet'].'">' : '<img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_view.gif" width="20" height="20" border="0" title="'.$button_view.' '.$eReportCard['Marksheet'].'">';
                                        $MarksheetIcon .= '</a>';
                                        //									}
                                        }
                                    }
                                    else
                                    {
                                        $MarksheetIcon = 'X';
                                        $FeedbackIcon = 'X';
                                        $CmpSubjectMSIcon = 'X';
                                    }
                                    
                                    $TCommentIcon = '<a href="javascript:jEDIT_TEACHER_COMMENT(\''.$thisSubjectGroupID.'\', \''.$thisSubjectID.'\')"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_edit_b.gif" width="20" height="20" border="0" title="'.$button_edit.' '.$eReportCard['TeacherComment'].'"></a>';
                                    if($ck_ReportCard_UserType=="VIEW_GROUP") {
                                        $TCommentIcon = '<a href="javascript:jEDIT_TEACHER_COMMENT(\''.$thisSubjectGroupID.'\', \''.$thisSubjectID.'\')"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_view.gif" width="20" height="20" border="0" title="'.$button_view.' '.$eReportCard['TeacherComment'].'"></a>';
                                    }
                                    //							$FeedbackIcon = '<a href="javascript:jVIEW_FEEDBACK(\''.$thisSubjectGroupID.'\', \''.$thisSubjectID.'\')"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_view.gif" width="20" height="20" border="0" title="'.$button_view.' '.$eReportCard['Feedback'].'"></a>';
                                    
                                    $ExtraInfoIcon = '<a href="javascript:jEDIT_EXTRA_INFO(\''.$thisSubjectGroupID.'\', \''.$thisSubjectID.'\')"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_edit_b.gif" width="20" height="20" border="0" title="'.$button_edit.' '.$eReportCard['ExtraInfoLabel'].'"></a>';
                                    
                                    $AdjustPositionIcon = '<a href="javascript:jEDIT_ADJUST_POSITION(\''.$thisSubjectGroupID.'\', \''.$thisSubjectID.'\')"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_edit_b.gif" width="20" height="20" border="0" title="'.$button_edit.' '.$eReportCard['AdjustPositionLabal'].'"></a>';
                                    
                                    if ($eRCTemplateSetting['PreviewReportSubjectSectionInMarksheet']) {
                                        $ReportPreviewIcon = $ReportPreviewIcon_HTML;
                                    }
                                }
                                else
                                {
                                    $TCommentIcon = '<a href="javascript:jEDIT_TEACHER_COMMENT(\''.$thisSubjectGroupID.'\', \''.$thisSubjectID.'\')"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_edit_b.gif" width="20" height="20" border="0" title="'.$button_edit.' '.$eReportCard['TeacherComment'].'"></a>';
                                }
                            }
                            // BIBA Cust for ES Progress Report - display "-"
                            if($isESReport && $isProgressReport && ($thisSubjectComponentID=='' || $thisSubjectComponentID==0) && ($thisSchemeID != null && $thisSchemeID != "")){
                                $MarksheetIcon = "-";
                                $FeedbackIcon = "-";
                                $CmpSubjectMSIcon = "-";
                            }
                            
                            //2017-01-19 Villa HKUGA Cust
//                             if($sys_custom['eRC']['Report']['HKUGAHideComment'] && ($ISSemester == '1' && !($FormNumber=='6') )){
//                                 $IsAdmin = $libreportcardcustom->IS_ADMIN_USER($_SESSION['UserID']);
//                                 if(!$IsAdmin){
//                                     $TCommentIcon = '-';
//                                 }
//                             }
                            $_hideCommentBtn = false;
                            $IsAdmin = $libreportcardcustom->IS_ADMIN_USER($_SESSION['UserID']);
                            if ($sys_custom['eRC']['Report']['HKUGAHideComment'] && !$IsAdmin) {
                                if ($ISSemester == '1' && $FormNumber!='6' ) {
                                    $_hideCommentBtn = true;
                                }
                                if ($FormNumber==3 && $libreportcardcustom->isModularStudySubject($thisSubjectID)) {
                                    $_hideCommentBtn = false;
                                }
                            }
                            if ($_hideCommentBtn) {
                                $TCommentIcon = '-';
                            }
                            
                            
                            // BIBA Cust for KG Report - display "-"
                            //					if($isKGReport){
                            //						$TCommentIcon = "-";
                            //					}
                            }	// End if ($isAllColumnWeightZero == 0)
                            
                            
                            $row_css = ($cnt % 3 == 1) ? "attendanceouting" : "attendancepresent";
                            
                            # Content
                            $display .= '<tr class="tablegreenrow1" height="38">'."\n";
                            $display .= '<td class="'.$row_css.' tabletext">'. $thisSubjectName .'</td>'."\n";
                            $display .= '<td class="'.$row_css.' tabletext">'. $thisSubjectGroupName. '</td>'."\n";
                            
                            if (!$eRCTemplateSetting['Management']['Marksheet']['HideComponentSubject']) {
                                $display .= '<td align="center" class="'.$row_css.' tabletext">'. $CmpSubjectIcon .'</td>'."\n";
                            }
                            
                            $display .= '<td align="center" class="'.$row_css.' tabletext">'. $CmpSubjectMSIcon .'</td>'."\n";
                            
                            if (!$eRCTemplateSetting['Management']['Marksheet']['HideMarksheet']) {
                                $display .= '<td align="center" class="'.$row_css.' tabletext">'. $MarksheetIcon .'</td>'."\n";
                            }
                            
                            // Personal Characteristics
                            if ($eRCTemplateSetting['ShowPersonalCharInMarksheetRevision']) {
                                $display .= '<td align="center" class="'.$row_css.' tabletext">'. $PersonalCharIcon .'</td>';
                            }
                            
                            // Subject Teacher Comment
                            $display .= '<td align="center" class="'.$row_css.' tabletext">'. $TCommentIcon .'</td>';
                            
                            // Report Preview
                            if ($eRCTemplateSetting['PreviewReportSubjectSectionInMarksheet']) {
                                $display .= '<td align="center" class="'.$row_css.' tabletext">'. $ReportPreviewIcon .'</td>';
                            }
                            
                            // Marksheet Feedback
                            if($accessSettingData[EnableVerificationPeriod]==1){  # 20130204 Check Access Setting
                                if (!$eRCTemplateSetting['Management']['Marksheet']['HideFeedback']) {
                                    $display .= '<td align="center" class="'.$row_css.' tabletext">'. $FeedbackIcon .'</td>';
                                }
                            }
                            
                            // Effort
                            if ($libreportcardcustom->IsEnableMarksheetExtraInfo) {
                                $display .= '<td align="center" class="'.$row_css.' tabletext">'. $ExtraInfoIcon .'</td>';
                            }
                            else if ($eRCTemplateSetting['Marksheet']['ManualInputGrade']) {
                                $display .= '<td align="center" class="'.$row_css.' tabletext">'. $ExtraInfoIcon .'</td>';
                            }
                            
                            // Manual Adjust Position
                            if ($libreportcardcustom->IsEnableManualAdjustmentPosition) {
                                $display .= '<td align="center" class="'.$row_css.' tabletext">'. $AdjustPositionIcon .'</td>';
                            }
                            else if ($eRCCommentSetting['ManualInputScore_ExtraReportOnly'] && !$isThisMainReport) {
                                $display .= '<td align="center" class="'.$row_css.' tabletext">'. $AdjustPositionIcon .'</td>';
                            }
                            
                            // Last Modified
                            $display .= '<td class="'.$row_css.' tabletext">'. $LastModified .'</td>'."\n";
                            $display .= '<td align="center" class="'.$row_css.' tabletext">';
                            
                            // the followings can be use in teahcer view
                            if($isAllColumnWeightZero == 0){
                                if($ck_ReportCard_UserType == "TEACHER" || $ck_ReportCard_UserType == "VIEW_GROUP"){
                                    //if($PeriodType != 1){	// not within submission period
                                    //if($PeriodType <= 0)
                                    if($thisSubjectGroupPeriodType != 1){
                                        if($thisSubjectGroupPeriodType <= 0)
                                        {
                                            $display .= "-";
                                        }
                                        else
                                        {
                                            if(count($ProgressArr) > 0 && $ProgressArr['IsCompleted']){	// show imcompleted status
                                                $display .= $eReportCard['Confirmed'];
                                            }
                                            else {	// show completed status
                                                $display .= $eReportCard['NotCompleted'];
                                            }
                                        }
                                    }
                                    else {
                                        $display .= $linterface->GET_SELECTION_BOX($CompleteArr, "name='complete_".$thisTargetSubjectID."_".$thisSubjectGroupID."' id='complete_".$thisTargetSubjectID."_".$thisSubjectGroupID."' class='tabletexttoptext CompleteSelection'", "", ((count($ProgressArr) > 0) ? $ProgressArr['IsCompleted'] : 0));
                                    }
                                }
                                else if($ck_ReportCard_UserType == "ADMIN"){
                                    $display .= $linterface->GET_SELECTION_BOX($CompleteArr, "name='complete_".$thisTargetSubjectID."_".$thisSubjectGroupID."' id='complete_".$thisTargetSubjectID."_".$thisSubjectGroupID."' class='tabletexttoptext CompleteSelection'", "", ((count($ProgressArr) > 0) ? $ProgressArr['IsCompleted'] : 0));
                                }
                            }
                            else
                            {
                                $display .= '-';
                            }
                            
                            $display .= '</td>'."\n";
                            $display .= '</tr>'."\n";
                    }
            }
            
            if ($numOfSubject == 0 || $numOfDisplayedSubjectGroup == 0)	{
                $td_colspan = "9";
                $display .= '
					<tr class="tablegreenrow1" height="40">
			          <td class="attendancepresent tabletext" colspan="'.$td_colspan.'" align="center">'.$i_no_record_exists_msg.'</td>
			      	</tr>';
            }
            
            
            // the followings can be used in teacher view
            if($ck_ReportCard_UserType == "TEACHER" || $ck_ReportCard_UserType=="VIEW_GROUP"){
                if($PeriodType != 1){	// not within submission period
                    $completeAll = '';
                }
                else {	// out of submission period
                    $completeAll .= $linterface->GET_SELECTION_BOX($CompleteAllArr, 'name="completeAll" id="completeAll" class="tabletexttoptext" onchange="setCompleteAll(\''.$SubjectID.'\', this.value)"', "", "");
                }
            }
            else if($ck_ReportCard_UserType == "ADMIN"){
                $completeAll .= $linterface->GET_SELECTION_BOX($CompleteAllArr, 'name="completeAll" id="completeAll" class="tabletexttoptext" onchange="setCompleteAll(\''.$SubjectID.'\', this.value)"', "", "");
            }
            
            
            
            $table = '';
            $table .= '<table width="100%" border="0" cellspacing="0" cellpadding="4">'."\r\n";
            $table .= '<tr class="tablegreentop">'."\r\n";
            $table .= '<td class="tabletopnolink">'.$eReportCard['Subject'].'</td>'."\r\n";
            $table .= '<td class="tabletopnolink">'.$Lang['SysMgr']['SubjectClassMapping']['SubjectGroup'].'</td>'."\r\n";
            
            if (!$eRCTemplateSetting['Management']['Marksheet']['HideComponentSubject']) {
                $table .= '<td width="80" align="center" class="tabletopnolink">'.$eReportCard['CmpSubjects'].'</td>'."\r\n";
            }
            
            $table .= '<td width="80" align="center" class="tabletopnolink">'.$eReportCard['CmpSubjectMarksheet'].'</td>'."\r\n";
            
            if (!$eRCTemplateSetting['Management']['Marksheet']['HideMarksheet']) {
                $table .= '<td width="80" align="center" class="tabletopnolink">'.$eReportCard['Marksheet'].'</td>'."\r\n";
            }
            
            // Personal Characteristics
            if ($eRCTemplateSetting['ShowPersonalCharInMarksheetRevision']) {
                $table .= '<td width="80" align="center" class="tabletopnolink">'.$eReportCard['PersonalCharacteristics'].'</td>'."\r\n";
            }
            
            // Subject Teacher Comment
            $table .= '<td width="80" align="center" class="tabletopnolink">'.$eReportCard['TeacherComment'].'</td>'."\r\n";
            
            // Report Preview
            if ($eRCTemplateSetting['PreviewReportSubjectSectionInMarksheet']) {
                $table .= '<td width="80" align="center" class="tabletopnolink">'.$Lang['eReportCard']['ManagementArr']['MarksheetRevisionArr']['PreviewReport'].'</td>'."\r\n";
            }
            
            // Marksheet Feedback
            if($accessSettingData[EnableVerificationPeriod]==1){
                if (!$eRCTemplateSetting['Management']['Marksheet']['HideFeedback']) {
                    $table .= '<td width="80" align="center" class="tabletopnolink">'.$eReportCard['Feedback'].'</td>'."\r\n";
                }
            }
            
            // Effort
            if ($libreportcardcustom->IsEnableMarksheetExtraInfo) {
                $table .= '<td width="80" align="center" class="tabletopnolink">'.$eReportCard['ExtraInfoLabel'].'</td>'."\r\n";
            }
            else if ($eRCTemplateSetting['Marksheet']['ManualInputGrade']) {
                $table .= '<td width="80" align="center" class="tabletopnolink">'.$column_data[0]['ColumnTitle'].' '.$eReportCard['Grade'].'</td>'."\r\n";
            }
            
            // Maunal Adjust Position
            if($libreportcardcustom->IsEnableManualAdjustmentPosition) {
                $table .= '<td width="80" align="center" class="tabletopnolink">'.$eReportCard['AdjustPositionLabal'].'</td>'."\r\n";
            }
            else if ($eRCCommentSetting['ManualInputScore_ExtraReportOnly'] && !$isThisMainReport) {
                $table .= '<td width="80" align="center" class="tabletopnolink">'.$eReportCard['AdjustPositionLabal'].'</td>'."\r\n";
            }
            
            $table .= '<td class="tabletopnolink">'.$eReportCard['LastModifiedDate'].'</td>'."\r\n";
            $table .= '<td width="80" align="center" class="tabletopnolink">'.$completeAll.'</td>'."\r\n";
            $table .= '</tr>'."\r\n";
            
            // Table Content
            $table .= $display."\r\n";
            
            $table .= '</table>'."\r\n";
            
            
            
            $total = $eReportCard['TotalRecords']." ";
            $total .= ($isFormSubject || $SubjectID == "") ? $numOfDisplayedSubjectGroup : 0;
            
            # Button
            $display_button = '';
            if($cnt > 0){
                if(in_array(0, $AllSubjectColumnWeightStatus)){
                    if($ck_ReportCard_UserType == "ADMIN" || ($ck_ReportCard_UserType == "TEACHER" && $PeriodType == 1) ){// within submission period
                        $display_button .= '
				  <tr>
				    <td>
				      <br>
				  	  <table width="100%" border="0" cellspacing="0" cellpadding="0">
				        <tr>
				          <td height="1" class="dotline"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/10x10.gif" width="10" height="1"></td>
				        </tr>
				      	<tr>
				          <td>
				            <table width="100%" border="0" cellspacing="0" cellpadding="2">
				              <tr>
				                <td align="center" valign="bottom">';
                        $display_button .= '<input name="Save" type="button" class="formbutton" onMouseOver="this.className=\'formbuttonon\'" onMouseOut="this.className=\'formbutton\'" value="'.$button_save.'" onclick="jSUBMIT_FORM()">';
                        //$display_button .= '<input name="Reset" type="reset" class="formbutton" onMouseOver="this.className=\'formbuttonon\'" onMouseOut="this.className=\'formbutton\'" value="'.$button_reset.'">';
                        $display_button .= '
				                </td>
				              </tr>
				        	</table>
				          </td>
				      	</tr>
				      </table>
				    </td>
				  </tr>';
                    }
                }
            }
            
            
            # Added on 5 Jan 2009 by Ivan
            # Show Submission Start Date and End Date
            # construct the condition
            $scheduleEntries = $lreportcard->GET_REPORT_DATE_PERIOD("", $ReportID);
            $subStart = ($scheduleEntries[0]["SubStart"]=="") ? "-" : date("Y-m-d H:i:s",$scheduleEntries[0]["SubStart"]);
            $subEnd = ($scheduleEntries[0]["SubEnd"]=="") ? "-" : date("Y-m-d H:i:s",$scheduleEntries[0]["SubEnd"]);
            if($hideAllReportInfo){
                $subStart = $subEnd = "-";
            }
            
            ### Added clear manual position for uccke (20091215)
            if ($sys_custom['eRC']['Marksheet']['ShowClearManualPositionButton'] && $ck_ReportCard_UserType=='ADMIN' && $SubjectID)
            {
                $ClearPositionBtn = $linterface->GET_SMALL_BTN($eReportCard['ClearPosition'], 'button', 'js_Clear_Manual_Position()');
            }
            
            # $PeriodAction = "Submission" or "Verification"
            $PeriodAction = "Submission";
            $PeriodType = $lreportcard->COMPARE_REPORT_PERIOD($ReportID, $PeriodAction);
            if($PeriodType != 1 && $ck_ReportCard_UserType != "ADMIN") {
                $ViewOnly = true;
            }
            $SpecialPeriodType = $lreportcard_schedule->compareSpecialMarksheetSubmissionPeriodForTeacher($ReportID, $_SESSION['UserID']);
            if ($SpecialPeriodType == 1) {
                if ($eRCTemplateSetting['Management']['Schedule']['SpecificMarksheetSubmissionPeriod_subjectBased'] || $eRCTemplateSetting['Management']['Schedule']['SpecificMarksheetSubmissionPeriod_subjectGroupBased']) {
                    // only allow import by subject if used subject-based special submission period and within the special submission period
                }
                else {
                    $ViewOnly = false;
                }
            }
            if($ck_ReportCard_UserType=="VIEW_GROUP")
                $ViewOnly = true;
                
                ### Btn
                if($eRCTemplateSetting['DisableMarksheetImport']!==true && !$ViewOnly) {
                    $ImportBtn = $linterface->Get_Content_Tool_v30("import", "javascript:jsIMPORT();");
                    // [2015-0520-1100-36066] add button - Import Marksheet Score for All subjects
                    if($eRCTemplateSetting['Marksheet']['ImportAllSubjectScore']){
                        $ImportAllBtn = $linterface->Get_Content_Tool_v30("import", "javascript:jsIMPORT_All();", $Lang['eReportCard']['ManagementArr']['DataHandlingArr']['ImportAllSubject']);
                    }
                }
                if ($eRCTemplateSetting['Marksheet']['ManualInputGrade'] && !$eRCTemplateSetting['DisableMarksheetImport']) {
                    if (!$ViewOnly) {
                        $ImportGradeBtn = $linterface->Get_Content_Tool_v30("import", "javascript:js_import_grade();", $Lang['Btn']['Import'].' '.$columnTitle);
                    }
                    $ExportGradeBtn = $linterface->Get_Content_Tool_v30("export", "javascript:js_export_grade();", $Lang['Btn']['Export'].' '.$columnTitle);
                }
                if($accessSettingData[EnableVerificationPeriod]==1 && !$hideAllReportInfo){
                    $ExportBtn = $linterface->Get_Content_Tool_v30("export", "javascript:js_Export();",$eReportCard['ManagementArr']['MarksheetRevisionArr']['MarksheetFeedbackArr']['ExportFeedback']);
                    // [2016-0105-1050-50207]
                    $exportTeacherCommentTitle = $Lang['Btn']['Export'].' '.$eReportCard['TeacherComment'];
                    $ExportCommentBtn = $linterface->Get_Content_Tool_v30("export", "javascript:js_export_comment();", $exportTeacherCommentTitle);
                }
                
                // [2015-1209-1137-25164]
                if($eRCTemplateSetting['Settings']['SubjectTopics'] && $eRCTemplateSetting['Management']['ExportSubjectDetails'] && !$ViewOnly){
                    $ExportDetailsBtn = $linterface->Get_Content_Tool_v30("export", "javascript:js_ExportDetails();",$Lang['eReportCard']['ManagementArr']['MarksheetRevisionArr']['ExportDetails']);
                }
                
                $specificSubmissionDisplay = $lreportcard_schedule->getTeacherSpecialMarksheetSubmissionPeriodDisplay($ReportID, $_SESSION['UserID']);
                if ($specificSubmissionDisplay != '') {
                    $specificSubmissionRemarks = '<tr><td>'.$lreportcard_schedule->getTeacherSpecialMarksheetSubmissionPeriodRemarks().'</td></tr>';
                }
                
                ############################################################################################################
                
                # tag information
                $MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
                $TAGS_OBJ[] = array($PAGE_TITLE, "", 0);
                
                $linterface->LAYOUT_START();
                
                ?>

<script language="javascript">
/*
* General JS function(s)
*/
function jGET_SELECTBOX_SELECTED_VALUE(objName){
	var obj = document.getElementById(objName);
	if(obj==null)	return "";
	var index = obj.selectedIndex;
	var val = obj[index].value;
	return val;
}

/* Global */
<?php
$js_form_var = "var FormArr = new Array(";
if(count($FormArr) > 0){
	for($i=0 ; $i<count($FormArr) ; $i++)
		$js_form_var .= ($i != 0) ? ', "'.$FormArr[$i][0].'"' : '"'.$FormArr[$i][0].'"';
}
$js_form_var .= ");";
echo $js_form_var;


?>

/*
* JS function(s) in this page
*/
function setCompleteAll(parSubjectID, jsValue){
//	var subjectGroupIDArr = (document.getElementsByName("SubjectGroupIDList[]")) ? document.getElementsByName("SubjectGroupIDList[]") : "";
//	var subjectIDArr = (document.getElementsByName("SubjectIDList[]")) ? document.getElementsByName("SubjectIDList[]") : "";
//	var indexToSelect = "";
//	
//	var parStatus = jGET_SELECTBOX_SELECTED_VALUE("completeAll");
//	if (parStatus == "completeAll"){
//		indexToSelect = 0;
//	}
//	else if (parStatus == "notCompleteAll"){
//		indexToSelect = 1;
//	}
//	
//	if(parStatus != ""){
//		if(subjectGroupIDArr.length > 0 && subjectIDArr.length > 0){
//			for(var i=0 ; i<subjectGroupIDArr.length ; i++){
//				for(var j=0 ; j<subjectIDArr.length ; j++){
//					if(document.getElementById("complete_"+subjectIDArr[j].value+"_"+subjectGroupIDArr[i].value)){
//						var obj = document.getElementById("complete_"+subjectIDArr[j].value+"_"+subjectGroupIDArr[i].value);
//						obj.selectedIndex = indexToSelect;
//						obj.options[indexToSelect].selected = true;
//					}
//				}
//			}
//		}
//	}

	if (jsValue == "completeAll")
		jsValue = 1;
	else if (jsValue == "notCompleteAll")
		jsValue = 0;
		
	$('select.CompleteSelection').val(jsValue);
}


function jCHANGE_MARKSHEET_REVISION(ResetReportID){
	var form_id = jGET_SELECTBOX_SELECTED_VALUE("ClassLevelID");
	var report_id = jGET_SELECTBOX_SELECTED_VALUE("ReportType");
	var subject_id = jGET_SELECTBOX_SELECTED_VALUE("SubjectID");
	
	if (ResetReportID)
		report_id = '';
	
	location.href = "?ClassLevelID="+form_id+"&ReportID="+report_id+"&SubjectID="+subject_id;
}

// jType: 1 - Parent Subject , 0 = Component Subject
function jEDIT_MARKSHEET(subject_group_id, target_subject_id, jType){
	var form_id = <?=(($ClassLevelID != "") ? $ClassLevelID : "''")?>;
	var report_id = <?=(($ReportID != "") ? $ReportID : "''")?>;
	var subject_id = <?=(($SubjectID != "") ? $SubjectID : "''")?>;
	
	report_id = (report_id == "") ? jGET_SELECTBOX_SELECTED_VALUE("ReportType") : report_id;
	subject_id = (subject_id == "") ? target_subject_id : subject_id;
	
	params = "ClassLevelID="+form_id+"&ReportID="+report_id+"&SubjectID="+subject_id+"&SubjectGroupID="+subject_group_id;
	if(jType == 1)
		location.href = "marksheet_edit.php?"+params;
	else if(jType == 0)
		location.href = "marksheet_revision2_subject_group.php?"+params;
	else if(jType == 2)
		location.href = "marksheet_cmp_edit.php?"+params;
	<?php if($eRCTemplateSetting['Settings']['SubjectTopics']) { ?>
		else if(jType == 3)
			location.href = "marksheet_subject_topic_edit.php?"+params;
	<?php } ?> 
}


function jEDIT_TEACHER_COMMENT(subject_group_id, target_subject_id){
	var form_id = <?=(($ClassLevelID != "") ? $ClassLevelID : "''")?>;
	var report_id = <?=(($ReportID != "") ? $ReportID : "''")?>;
	var subject_id = <?=(($SubjectID != "") ? $SubjectID : "''")?>;
	
	report_id = (report_id == "") ? jGET_SELECTBOX_SELECTED_VALUE("ReportType") : report_id;
	subject_id = (subject_id == "") ? target_subject_id : subject_id;
	
	location.href = "teacher_comment.php?ClassLevelID="+form_id+"&ReportID="+report_id+"&SubjectID="+subject_id+"&SubjectGroupID="+subject_group_id;
}

function jEDIT_EXTRA_INFO(subject_group_id, target_subject_id){
	var form_id = <?=(($ClassLevelID != "") ? $ClassLevelID : "''")?>;
	var report_id = <?=(($ReportID != "") ? $ReportID : "''")?>;
	var subject_id = <?=(($SubjectID != "") ? $SubjectID : "''")?>;
	
	report_id = (report_id == "") ? jGET_SELECTBOX_SELECTED_VALUE("ReportType") : report_id;
	subject_id = (subject_id == "") ? target_subject_id : subject_id;
	
	location.href = "extra_info.php?ClassLevelID="+form_id+"&ReportID="+report_id+"&SubjectID="+subject_id+"&SubjectGroupID="+subject_group_id;
}

function jEDIT_ADJUST_POSITION(subject_group_id, target_subject_id){
	var form_id = <?=(($ClassLevelID != "") ? $ClassLevelID : "''")?>;
	var report_id = <?=(($ReportID != "") ? $ReportID : "''")?>;
	var subject_id = <?=(($SubjectID != "") ? $SubjectID : "''")?>;
	
	report_id = (report_id == "") ? jGET_SELECTBOX_SELECTED_VALUE("ReportType") : report_id;
	subject_id = (subject_id == "") ? target_subject_id : subject_id;
	
	location.href = "extra_info.php?ClassLevelID="+form_id+"&ReportID="+report_id+"&SubjectID="+subject_id+"&SubjectGroupID="+subject_group_id+"&type=AdjustPosition";
}

function jVIEW_FEEDBACK(subject_group_id, target_subject_id){
	var form_id = <?=(($ClassLevelID != "") ? $ClassLevelID : "''")?>;
	var report_id = <?=(($ReportID != "") ? $ReportID : "''")?>;
	var subject_id = <?=(($SubjectID != "") ? $SubjectID : "''")?>;
	
	report_id = (report_id == "") ? jGET_SELECTBOX_SELECTED_VALUE("ReportType") : report_id;
	subject_id = (subject_id == "") ? target_subject_id : subject_id;

	location.href = "marksheet_feedback/index.php?ClassLevelID="+form_id+"&ReportID="+report_id+"&SubjectID="+subject_id+"&SubjectGroupID="+subject_group_id;
}

function jEDIT_PERSONAL_CHAR(subject_group_id, target_subject_id) {
	var form_id = <?=(($ClassLevelID != "") ? $ClassLevelID : "''")?>;
	var report_id = <?=(($ReportID != "") ? $ReportID : "''")?>;
	var subject_id = <?=(($SubjectID != "") ? $SubjectID : "''")?>;
	
	report_id = (report_id == "") ? jGET_SELECTBOX_SELECTED_VALUE("ReportType") : report_id;
	subject_id = (subject_id == "") ? target_subject_id : subject_id;
	
	
	location.href = "../personal_characteristics/edit.php?FromMS=1&ClassLevelID="+form_id+"&ReportID="+report_id+"&SubjectID="+subject_id+"&SubjectGroupID="+subject_group_id;
}


// Form
function jSUBMIT_FORM(){
	var obj = document.FormMain;
	obj.action = 'marksheet_complete_confirm_subject_group.php';
	obj.submit();
}

function js_Clear_Manual_Position()
{
	if (confirm("<?=$eReportCard['jsWarningClearPosition']?>"))
	{
		var obj = document.FormMain;
		obj.action = 'clear_manual_position_update.php';
		obj.submit();
	}
}

function jsIMPORT()
{
	var SubjectID = $("#SubjectID").val();
	window.location.href = "marksheet_import.php?ReportID=<?=$ReportID?>&ClassLevelID=<?=$ClassLevelID?>&TagsType=<?=$TagsType?>&SubjectID="+SubjectID;
}


<?php if($eRCTemplateSetting['Marksheet']['ImportAllSubjectScore']){ ?>
function jsIMPORT_All()
{
	var SubjectID = $("#SubjectID").val();
	window.location.href = "marksheet_import_all_subject.php?ReportID=<?=$ReportID?>&ClassLevelID=<?=$ClassLevelID?>&TagsType=<?=$TagsType?>&SubjectID="+SubjectID;
}
<?php } ?>

<?php if ($eRCTemplateSetting['Marksheet']['ManualInputGrade']) { ?>
function js_import_grade() {
	var SubjectID = $("#SubjectID").val();
	window.location.href = "import_extra_info_all.php?ReportID=<?=$ReportID?>&ClassLevelID=<?=$ClassLevelID?>&TagsType=<?=$TagsType?>&SubjectID="+SubjectID;
}
function js_export_grade() {
	window.location.href = "export_extra_info_all.php?ReportID=<?=$ReportID?>";
}
<?php } ?>

function js_Export()
{
	var obj = document.FormMain;
	obj.action = './marksheet_feedback/export_form_feedback.php';
	obj.submit();
	obj.action = './marksheet_complete_confirm_subject_group.php';
}

function js_export_comment(){
	var obj = document.FormMain;
	obj.action = './marksheet_feedback/export_form_comment.php';
	obj.submit();
	obj.action = './marksheet_complete_confirm_subject_group.php';
}

<?php if($eRCTemplateSetting['Settings']['SubjectTopics'] && $eRCTemplateSetting['Management']['ExportSubjectDetails']){ ?>
function js_ExportDetails(){
	var obj = document.FormMain;
	obj.action = './export_all_st_details.php';
	obj.submit();
	obj.action = './marksheet_complete_confirm_subject_group.php';
}
<?php } ?>
</script>

<br/>
<form name="FormMain" method="POST" action="marksheet_complete_confirm_subject_group.php">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>
      <table border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><?=$FormSelection?></td>
          <td>&nbsp;</td>
          <td><?=$ReportTypeSelection?></td>
          <td>&nbsp;</td>
          <td><?=$SubjectSelection?></td>
          <td>&nbsp;</td>
          <td align="right" width="100%">
          	<?=$linterface->GET_SYS_MSG($msg);?>
          	<?=$ClearPositionBtn?>
          </td>
  		</tr>
  		<tr>
          <td colspan="7" class="tabletext">
          	<b><?=$eReportCard['SubmissionStartDate']?>:</b> <?=$subStart?> &nbsp;&nbsp; <b><?=$eReportCard['SubmissionEndDate']?>:</b> <?=$subEnd?>
          	<?=$specificSubmissionDisplay?>
          </td>
  		</tr>
  	  </table>
  	</td>
  </tr> 
  <tr>
  	<td>
  	<br>
		<!--<table border="0" cellspacing="0" cellpadding="2">
	      	<tr>
	      	  <?php 
	      		  # Condition: TagsType Must be "0" - Input Raw Mark & case i
	      		  # case 1: Whole Year Report & $hasDirectInputMark = 1 - allow direct input 
	      		  # case 2: Term Report & subject without component subjects
	      		  # case 3: Subject having Component Subjects & is Editable
//	      		  if($TagsType == 0 && (
//	      		    $ScaleInput == "G" || 
//	      			($ReportType == "F" && $hasDirectInputMark) || 
//	      			($ReportType != "F" && empty($CmpSubjectArr)) || 
//	      			(!empty($CmpSubjectArr) && $isEdit)) )
	      			{ 
	          			
	          ?>
	      	  <td><img src="<?=$image_path.'/'.$LAYOUT_SKIN?>/10x10.gif"></td>
	      	  <?php } ?>
	      	  
	      	</tr>
		  </table>-->
		  <?php if($eRCTemplateSetting['Settings']['SubjectTopics'] && $eRCTemplateSetting['Management']['ExportSubjectDetails']){ ?>
		  	<div class="content_top_tool">
				<div class="Conntent_tool">
					<?=$ExportDetailsBtn?>
				</div>
			</div>
		  <?php } ?>
		  <?php if(!$eRCTemplateSetting['Settings']['SubjectTopics']) { ?>
		  	<div class="content_top_tool">
				<div class="Conntent_tool">
					<?=$ImportBtn?>
				<?php if($eRCTemplateSetting['Marksheet']['ImportAllSubjectScore']){ ?>
					<?=$ImportAllBtn?>
				<?php } ?>
					<?=$ExportBtn?>
					<?=$ExportCommentBtn?>
					<?php if ($eRCTemplateSetting['Marksheet']['ManualInputGrade']) { ?>
						<br style="clear:both;">
						<?=$ImportGradeBtn?>
						<?=$ExportGradeBtn?>
					<?php } ?>	
				</div>
			</div>
			<?php } ?>
		</td>
	</tr> 
  <tr>
    <td>
      <?=$table?>
    </td>
  </tr>
  <tr class="tablegreenbottom">
    <td>
      <table width="100%" border="0" cellspacing="0" cellpadding="3">
        <tr>
    	  <td class="tabletext"><?=$total?></td>
    	</tr>
      </table>
    </td>
  </tr>
  <tr>
  	<td><br /><span class="tabletextremark"><span class="tabletextrequire">*</span><?=$eReportCard['ManagementArr']['MarksheetRevisionArr']['MarksheetFeedbackArr']['FeedbackInProgress']?></span></td>
  </tr>
  <?=$specificSubmissionRemarks?>
  <?=$display_button?>
</table>

<br />
<br />

<input type="hidden" name="isProgress" id="isProgress" value="0"/>
<input type="hidden" name="ClassLevelID" id="ClassLevelID" value="<?=$ClassLevelID?>"/>
<input type="hidden" name="ReportID" id="ReportID" value="<?=$ReportID?>"/>
<input type="hidden" name="SubjectID" id="SubjectID" value="<?=$SubjectID?>"/>
<input type="hidden" name="MarksheetRevision" id="MarksheetRevision" value="1"/>

<?php 
$numOfAllSubjectGroup = count($allSubjectGroupIDArr);
if($numOfAllSubjectGroup > 0){
	for($i=0 ; $i<$numOfAllSubjectGroup ;$i++){
		$thisSubjectGroupID = $allSubjectGroupIDArr[$i];
		echo "<input type=\"hidden\" name=\"SubjectGroupIDList[]\" id=\"SubjectGroupIDList\" value=\"$thisSubjectGroupID\"/>\n";
	}
}

if($SubjectID != ""){
	echo "<input type=\"hidden\" name=\"SubjectIDList[]\" id=\"SubjectIDList\" value=\"$SubjectID\"/>\n";
}
else {
	if(count($SubjectListArr) > 0){
		for($j=0 ; $j<count($SubjectListArr) ; $j++){
			echo "<input type=\"hidden\" name=\"SubjectIDList[]\" id=\"SubjectIDList\" value=\"".$SubjectListArr[$j]['SubjectID']."\"/>\n";
		}
	}
}
?>


</form>


<?
		$linterface->LAYOUT_STOP();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>