<?php 
// Modifying by: Bill

######## If need to upload this file before [ip.2.5.7.7.1], MUST upload with libreportcard2008.php ########
/***************************************************
 * 	Modification log:
 * 20201103 (Bill)      [2020-0915-1830-00164]
 *      - support term setting    ($eRCTemplateSetting['Settings']['SubjectTopics_TermSettings'])
 * 20180801 Bill:   [2018-0621-1453-45066]
 *      - use getReportFilteringDefaultReportId() to get default report
 * 20170519 Bill:
 * 		- allow View Group to access (view only)
 * 20160629 Bill:	[2015-1104-1130-08164]
 * 		- display Make-up Exam Score Column for $eRCTemplateSetting['Marksheet']['ManualInputGrade_WholeYearOnly'] = true
 * 20160428 Bill:	[2016-0302-1428-20207]
 * 		- display * if Component Subjects with Marksheet feedback in progress
 * 20160426 Bill:		[2015-1008-1356-44164]
 * 		- display edit button for whole year report that display overall column due to all columns are zero weight
 * 20160129 Kenneth:	[2016-0105-1050-50207]
 * 		- added $ExportCommentBtn, js_export_comment() - Export Techear Comment Button	(related to export_form_comment.php)
 * 20160120 Bill:		[2015-0421-1144-05164]
 * 		- added $eRCTemplateSetting['Marksheet']['ReportDisplayWithinSubmissionDate'] logic
 * 			display report within submission period only
 * 20150507 Bill:
 * 		- BIBA Cust
 * 		- display "-" if subject with subject topics
 * 20130205 Rita:
 * 		-add Verification Period Checking
 * 20120403 Marcus:
 * 		- Allow teacher to view marks even after submission period
 * 		- 2012-0208-1800-43132 - 沙田循道衛理中學 - Edit mark after submit the marksheet 
 * 20120326 Marcus:
 * 		- Cater allow to set time for submission, verification
 * 		- 2012-0112-1622-04073 - 迦密愛禮信中學 - Schedule of Report Card Management 
 * 		20110621 Marcus:
 * 			- add checking to check whether the use is the subject teacher of certain class.
 * *************************************************/

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
//include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libteaching.php");


intranet_auth();
intranet_opendb();

if ($plugin['ReportCard2008'])
{
	/* Temp Library*/
	include_once($PATH_WRT_ROOT."includes/libreportcard2008j.php");
	$lreportcard = new libreportcard2008j();
	
	if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") {
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
		$libreportcardcustom = new libreportcardcustom();
	}
	include_once($PATH_WRT_ROOT."includes/libreportcard2008_schedule.php");
	$lreportcard_schedule = new libreportcard_schedule();
	
	$CurrentPage = "Management_MarkSheetRevision";
	$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
	
    if ($lreportcard->hasAccessRight())
    {
        $linterface = new interface_html();
        $PAGE_TITLE = $eReportCard['Management_MarksheetRevision'];
        
        # Load the highlight setting data
		$accessSettingData = $lreportcard->LOAD_SETTING("AccessSettings");

        if($ck_ReportCard_UserType=="TEACHER")
		{
			$PAGE_TITLE = $eReportCard['Management_MarksheetSubmission'];
			$lteaching = new libteaching();
			
			$FormArrCT = $lreportcard->returnSunjectTeacherForm($UserID);
			
			# updated on 08 Dec 2008 by Ivan
			# added $special_feature['eRC_class_teacher_as_subject_teacher'] to check if the class teacher can view all subjects of the class or just the teaching subjects of the class
			# (for �װ�Ѱ| (�F�E�s) CRM Ref No.: 2008-1205-1459)
			# check is class teacher or not
			if (!$special_feature['eRC_class_teacher_as_subject_teacher'])
			{
				# if is class teacher => has TeacherClass
				$TeacherClassAry = $lteaching->returnTeacherClassWithLevel($UserID, $lreportcard->schoolYearID);
				$TeacherClass = $TeacherClassAry[0]['ClassName'];
			}
			
			if(!empty($TeacherClass))
			{
				for($i=0;$i<sizeof($TeacherClassAry);$i++)
				{
					$thisClassLevelID = $TeacherClassAry[$i]['ClassLevelID'];
					$searchResult = multiarray_search($FormArrCT , "ClassLevelID", $thisClassLevelID);
					if($searchResult == "-1")
					{
						$thisAry = array(
							"0"=>$TeacherClassAry[$i]['ClassLevelID'],
							"ClassLevelID"=>$TeacherClassAry[$i]['ClassLevelID'],
							"1"=>$TeacherClassAry[$i]['LevelName'],
							"LevelName"=>$TeacherClassAry[$i]['LevelName']);
						$FormArrCT = array_merge($FormArrCT, array($thisAry));
					}
				}
				
				# sort $FormArrCT
				foreach ($FormArrCT as $key => $row) 
				{
				    $field1[$key] 	= $row['ClassLevelID'];
					$field2[$key]  	= $row['LevelName'];
				}
				array_multisort($field1, SORT_ASC, $field2, SORT_ASC, $FormArrCT);
			}
			
			$ClassLevelID = ($ClassLevelID == "") ? $FormArrCT[0][0] : $ClassLevelID;
			$ClassArrCT = $lreportcard->returnSubjectTeacherClass($UserID, $ClassLevelID);
			
			$FormSubjectArrCT = $lreportcard->returnSubjectwOrder($ClassLevelID, 1, $UserID);
			
			if(!empty($TeacherClass))
			{
				$searchResult = multiarray_search($TeacherClassAry , "ClassLevelID", $ClassLevelID);
				if($searchResult != "-1")
				{	
					$searchResult2 = multiarray_search($ClassArrCT , "ClassName", $TeacherClassAry[$searchResult]['ClassName']);
					if($searchResult2 == "-1")
					{
						$thisAry = array(
							"0"=>$TeacherClassAry[$searchResult]['ClassID'],
							"ClassID"=>$TeacherClassAry[$searchResult]['ClassID'],
							"1"=>$TeacherClassAry[$searchResult]['ClassName'],
							"ClassName"=>$TeacherClassAry[$searchResult]['ClassName'],
							"2"=>$TeacherClassAry[$searchResult]['ClassLevelID'],
							"ClassLevelID"=>$TeacherClassAry[$searchResult]['ClassLevelID']);
						$ClassArrCT = array_merge($ClassArrCT, array($thisAry));
					}
				
					$FormSubjectArrCT = $lreportcard->returnSubjectwOrder($ClassLevelID, 1);
				}
			}
		}
        
		############################################################################################################
		
		# 3 GET Parameters : ClassLevelID, SubjectID, SemesterType_[ClassLevelID]
		# Get ClassLevelID (Form) of the reportcard template
		
		//$FormArr = $lreportcard->GET_ALL_FORMS(1);
		$FormArr = ($ck_ReportCard_UserType=="ADMIN" || $ck_ReportCard_UserType == "VIEW_GROUP")? $lreportcard->GET_ALL_FORMS(1) : $FormArrCT;
		$ClassLevelID = ($ClassLevelID == "") ? $FormArr[0][0] : $ClassLevelID;
		
		# Filters - By Form (ClassLevelID)
		$FormSelection = $linterface->GET_SELECTION_BOX($FormArr, "name='ClassLevelID' id='ClassLevelID' class='tabletexttoptext' onchange='jCHANGE_MARKSHEET_REVISION(1);'", "", $ClassLevelID);

		# Filters - By Type (Term1, Term2, Whole Year, etc)
		// Get Semester Type from the reportcard template corresponding to ClassLevelID
		$ReportTypeSelection = '';
		$ReportTypeArr = array();
		$ReportTypeOption = array();
		//2013-1119-1033-11156
		$orderBySubmissionTime = ($eRCTemplateSetting['Marksheet']['ReportSelectionOrderingSortBySubmissionDate'])? true : false; 

		// [2015-0421-1144-05164]
		// Teacher - hide reports that out of submission period
		$displayReportInSubmissionPeriod = ($eRCTemplateSetting['Marksheet']['ReportDisplayWithinSubmissionDate'] && $ck_ReportCard_UserType=="TEACHER")? true : false; 
		$ReportTypeArr = $lreportcard->GET_REPORT_TYPES($ClassLevelID, '', '', 0, $orderBySubmissionTime, $displayReportInSubmissionPeriod);
		
		# if not selected specific report card, preset the filter to the current submission period report template
		if ($ReportID == '')
		{
// 			$conds = " 		ClassLevelID = '$ClassLevelID' 
// 						AND 
// 							NOW() BETWEEN MarksheetSubmissionStart AND MarksheetSubmissionEnd
// 					";
// 			$ReportInfoArr = $lreportcard->returnReportTemplateBasicInfo('', $conds);
// 			$ReportID = ($ReportInfoArr['ReportID'])? $ReportInfoArr['ReportID'] : '';
		    $ReportID = $lreportcard->getReportFilteringDefaultReportId($ClassLevelID);
		}
		
		$ReportType = '';
		if(count($ReportTypeArr) > 0){
			for($j=0 ; $j<count($ReportTypeArr) ; $j++){
				if($ReportTypeArr[$j]['ReportID'] == $ReportID){
					$isFormReportType = 1;
					$ReportType = $ReportTypeArr[$j]['Semester'];
				}
				
				$ReportTypeOption[$j][0] = $ReportTypeArr[$j]['ReportID'];
				$ReportTypeOption[$j][1] = $ReportTypeArr[$j]['SemesterTitle'];
			}
			$ReportTypeSelection .= $linterface->GET_SELECTION_BOX($ReportTypeOption, 'name="ReportType" id="ReportType" class="tabletexttoptext" onchange="jCHANGE_MARKSHEET_REVISION(0)"', '', $ReportID);
			
			$ReportType = ($ReportType != "") ? $ReportType : $ReportTypeArr[0]['Semester'];
		}
		$ReportID = ($isFormReportType) ? $ReportID : $ReportTypeOption[0][0];
		$ReportSetting = $lreportcard->returnReportTemplateBasicInfo($ReportID);
		$SemID = $ReportSetting['Semester'];
		
		if ($SemID != 'F') {
			header("Location: marksheet_revision_subject_group.php?ClassLevelID=$ClassLevelID&ReportID=$ReportID&SubjectID=$SubjectID&msg=$msg");
		}
			
		
		# Load Settings - Calculation
		// Get the Calculation Method of Report Type - 1: Right-Down,  2: Down-Right
		$SettingCategory = 'Calculation';
		$categorySettings = $lreportcard->LOAD_SETTING($SettingCategory);
		$TermCalculationType = $categorySettings['OrderTerm'];
		$FullYearCalculationType = $categorySettings['OrderFullYear'];
		$CalculationType = ($ReportType == "F") ? $FullYearCalculationType : $TermCalculationType;	
		
		# Filters - By Form Subjects
		// A Flag, called $isFormSubject, is used to identify whether currect Form has that subject or not
		$FormSubjectArr = array();
		$SubjectListArr = array();
		$SubjectOption = array();
		$SubjectOption[0] = array("", "ALL");
		$count = 1;
		$isFormSubject = 0;		// Default: Assume the selected subject does not belongs to that Form(ClassLevelID)
		
		// Get Subjects By Form (ClassLevelID)
		//$FormSubjectArr = $lreportcard->returnSubjectwOrder($ClassLevelID, 1);
		$FormSubjectArr = ($ck_ReportCard_UserType=="ADMIN" || $ck_ReportCard_UserType == "VIEW_GROUP") ? $lreportcard->returnSubjectwOrder($ClassLevelID, 1) : $FormSubjectArrCT;
		
		// Hide Report Info if empty ReportID and only display reports that within submission period
		$hideAllReportInfo = false;
		if($displayReportInSubmissionPeriod && $ReportID==""){
			$FormSubjectArr = array();
			$hideAllReportInfo = true;
		}
			
		if(!empty($FormSubjectArr)){
			foreach($FormSubjectArr as $FormSubjectID => $Data){
				if(is_array($Data)){
					foreach($Data as $FormCmpSubjectID => $SubjectName){							
						$FormSubjectID = ($FormCmpSubjectID==0) ? $FormSubjectID : $FormCmpSubjectID;
						if($FormSubjectID == $SubjectID)
							$isFormSubject = 1;
							
						// Prepare Subject Selection Box 
						if($FormCmpSubjectID==0){
							$SubjectFormGradingArr = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $FormSubjectID,0,0,$ReportID);
							$SubjectListArr[($count-1)]['SubjectID'] = $FormSubjectID;
							$SubjectListArr[($count-1)]['SubjectName'] = $SubjectName;
							$SubjectListArr[($count-1)]['SchemeID'] = $SubjectFormGradingArr['SchemeID'];
							
							$SubjectOption[$count][0] = $FormSubjectID;
							$SubjectOption[$count][1] = $SubjectName;
							$count++;
						}
					}
				}
			}
		}
				
		// BIBA Cust - get SubjectID with subject topics
        $targetTermSeq = 0;
		if($eRCTemplateSetting['Settings']['SubjectTopics'])
		{
            // [2020-0915-1830-00164]
			//$SubjectTopicArr = $lreportcard->CHECK_SUBJECT_WITH_SUBJECT_TOPICS($ClassLevelID);
            $SubjectTopicArr = $lreportcard->CHECK_SUBJECT_WITH_SUBJECT_TOPICS($ClassLevelID, '', $targetTermSeq);
		}
		
		/*
		* Use for Selection Box
		* $SubjectListArr[][0] : SubjectID
		* $SubjectListArr[][1] : SubjectName		
		*/
		$isSelectAllSubject = $SubjectID=="";
		$SubjectID = ($isFormSubject) ? $SubjectID : '';
		if($SubjectID == ""){
			$SubjectID = $SubjectOption[0][0];
		}
		else {
			$SubjectFormGradingArr = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID,0,0,$ReportID);
			$SubjectListArr = array();
			$SubjectListArr[0]['SubjectID'] = $SubjectID;
			$SubjectListArr[0]['SubjectName'] = $lreportcard->GET_SUBJECT_NAME($SubjectID);
			$SubjectListArr[0]['SchemeID'] = $SubjectFormGradingArr['SchemeID'];
		}
		$SelectedSubjectName = $lreportcard->GET_SUBJECT_NAME($SubjectID);
		$SubjectSelection = $linterface->GET_SELECTION_BOX($SubjectOption, 'name="SubjectID" id="SubjectID" class="tabletexttoptext" onchange="jCHANGE_MARKSHEET_REVISION(0)"', '', $SubjectID);
		
		# Get Last Modified Info of This Report Template(Date-Time & Modified By who)
		$ReportColumnIDArr = array();
		$ReportColumnIDList = '';
		$column_data = $lreportcard->returnReportTemplateColumnData($ReportID); 	// Column Data
		if(count($column_data) > 0){
			for($i=0 ; $i<count($column_data) ; $i++)
				$ReportColumnIDArr[] = $column_data[$i]['ReportColumnID'];
			$ReportColumnIDList = implode(",", $ReportColumnIDArr);
		}
		$LastModifiedArr = $lreportcard->GET_CLASS_MARKSHEET_LAST_MODIFIED($SubjectID, $ReportColumnIDList);
		$SubjectCommentLastModifiedArr = $lreportcard->GET_CLASS_MARKSHEET_SUBJECT_COMMENT_LAST_MODIFIED($SubjectID);
		$LastModifiedOverallArr = $lreportcard->GET_CLASS_MARKSHEET_OVERALL_LAST_MODIFIED($SubjectID, $ReportID);
		
		
		# Get Status of Completed Action
		# $PeriodAction = "Submission" or "Verification"
		// $PeriodType & $PeriodAction can be used in teacher view
		$PeriodAction = "Submission";
		$PeriodType = $lreportcard->COMPARE_REPORT_PERIOD($ReportID, $PeriodAction);
		$SpecialPeriodType = $lreportcard_schedule->compareSpecialMarksheetSubmissionPeriodForTeacher($ReportID, $_SESSION['UserID']);
		if ($SpecialPeriodType == 1) {
			$PeriodType = 1;
		}
		if($ck_ReportCard_UserType=="VIEW_GROUP") {
			$PeriodType = 2;
		}
		
		# Array of Selection Box of Complete Function 
		$CompleteArr = array();
		$CompleteArr[] = array(1, $eReportCard['Confirmed']);
		$CompleteArr[] = array(0, $eReportCard['NotCompleted']);
		
		$CompleteAllArr = array();
		$CompleteAllArr[] = array("", "---".$eReportCard['ChangeAllTo']."---");
		$CompleteAllArr[] = array("completeAll", $eReportCard['Confirmed']);
		$CompleteAllArr[] = array("notCompleteAll", $eReportCard['NotCompleted']);
		
		# Main Content of Marksheet Reivision
		//$ClassArr = $lreportcard->GET_CLASSES_BY_FORM($ClassLevelID);
		$ClassArr = ($ck_ReportCard_UserType=="ADMIN" || $ck_ReportCard_UserType == "VIEW_GROUP") ? $lreportcard->GET_CLASSES_BY_FORM($ClassLevelID) : $ClassArrCT;
		
		// used to check whether there is any marksheet feedback which is not closed.
		$MarksheetFeedback = $lreportcard->Get_Marksheet_Feedback_Info($ReportID, $SubjectID);
		$MarksheetFeedback = BuildMultiKeyAssoc($MarksheetFeedback, array("StudentID","SubjectID"),array("RecordStatus"),1);
		 
		# Subject List
		$AllSubjectColumnWeightStatus = array();
		$display = '';
		$cnt = 0;
		if(count($ClassArr) > 0 && count($SubjectListArr) > 0){
			for($i=0 ; $i<count($ClassArr) ;$i++){
				$ClassID = $ClassArr[$i][0];
				$ClassName = $ClassArr[$i][1];
				
				for($j=0 ; $j<count($SubjectListArr) ; $j++){
					$ClassSubjectName = $SubjectListArr[$j]['SubjectName'];
					$ClassSubjectID = $SubjectListArr[$j]['SubjectID'];
					
					if($ck_ReportCard_UserType=="TEACHER")
					{
						$t = $lreportcard->checkSubjectTeacher($UserID, $ClassSubjectID, $ClassID);
						if(empty($t))	continue;
					}
					
					// BIBA Cust
					$thisSubjectWithTopic = false;
					$thisParentSubjectWithTopic = false;
					// check if main subject with subject topic
					if($eRCTemplateSetting['Settings']['SubjectTopics'] && !$thisSubjectWithTopic && isset($SubjectTopicArr[$ClassSubjectID][1])){
						$thisSubjectWithTopic = true;
						$thisParentSubjectWithTopic = true;
					}
							
					$StudentArr = $lreportcard->Get_Marksheet_Accessible_Student_List_By_Subject_And_Class($ReportID, $_SESSION['UserID'], $ClassID, $ParentSubjectID, $ClassSubjectID);
					$StudentList = Get_Array_By_Key($StudentArr,"UserID");
					unset($StudentArr);
					
					//check whether the subject teacher teaches this subject in this class or not
					if($ck_ReportCard_UserType=="TEACHER")
					{
						/*
						//debug_r($ClassID);
						# is Class Teacher?
						$temp = 0;
						if(!empty($TeacherClass))
						{
							$searchResult = multiarray_search($TeacherClassAry , "ClassID", $ClassID);
							if($searchResult != "-1")	$temp = 1;
						}
							
						$t = $lreportcard->checkSubjectTeacher($UserID, $ClassSubjectID, $ClassID);
						if(!empty($t))	$temp = 1;
						
						if(!$temp) 	continue;
						*/
						
						## If the user is the class teacher of this class => show
						## Else, check if the teacher had taught any students in this class in the relevant terms
						if ($lreportcard->Is_Class_Teacher($_SESSION['UserID'], $ClassID) == false)
						{
							$TaughtStudentList = $lreportcard->Get_Student_List_Taught_In_This_Consolidated_Report($ReportID, $_SESSION['UserID'], $ClassID);
							if (count($TaughtStudentList) == 0)
								continue;
						}
					}
					
					### Check if all Column Weight Zero
					$WeightAry = array();
					if($CalculationType == 2){
						if(count($column_data) > 0){
					    	for($k=0 ; $k<count($column_data) ; $k++){
								//$OtherCondition = "SubjectID IS NULL AND ReportColumnID = ".$column_data[$i]['ReportColumnID'];
								$OtherCondition = "SubjectID = '$ClassSubjectID' AND ReportColumnID = ".$column_data[$k]['ReportColumnID'];
								$weight_data = $lreportcard->returnReportTemplateSubjectWeightData($ReportID, $OtherCondition); 
								
								$WeightAry[$weight_data[$k]['ReportColumnID']][$ClassSubjectID] = $weight_data[0]['Weight'];
							}
						}
					}
					else {
						// [2015-1008-1356-44164]
						//$weight_data = $lreportcard->returnReportTemplateSubjectWeightData($ReportID);
						$OtherCondition = "SubjectID = '$ClassSubjectID' AND ReportColumnID IS NOT NULL";
						$weight_data = $lreportcard->returnReportTemplateSubjectWeightData($ReportID, $OtherCondition);
						
						for($k=0 ; $k<count($weight_data) ; $k++)
							$WeightAry[$weight_data[$k]['ReportColumnID']][$ClassSubjectID] = $weight_data[$k]['Weight'];
					}
					
					$SubjectFormGradingArr = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $ClassSubjectID,0,0,$ReportID);
					$SchemeID = $SubjectFormGradingArr['SchemeID'];
					$ScaleInput = $SubjectFormGradingArr['ScaleInput'];
					$ScaleDisplay = $SubjectFormGradingArr['ScaleDisplay'];
					$SchemeInfo = $lreportcard->GET_GRADING_SCHEME_INFO($SchemeID);
					$SchemeMainInfo = $SchemeInfo[0];
					$SchemeMarkInfo = $SchemeInfo[1];
					$SchemeType = $SchemeMainInfo['SchemeType'];
					
					$isAllColumnWeightZero = true;
					foreach((array)$WeightAry as $thisReportColumnID => $thisArr) {
						if ($thisArr[$ClassSubjectID] != 0 && $thisReportColumnID != '') {
							$isAllColumnWeightZero = false;
						}
					}
					
					if ($isAllColumnWeightZero == true && $SchemeType == "H" && ($ScaleInput == "M" || $ScaleInput == "G")) {
						$displayOverallColumnDueToAllZeroWeight = true;
					}
					else {
						$displayOverallColumnDueToAllZeroWeight = false;
					}
					
					$row_css = ($cnt % 3 == 1) ? "attendanceouting" : "attendancepresent";
					
					
					# Get component subject(s) if any
					$ParentLastModifiedArr = array();
					$CmpSubjectIDArr = array();
					$CmpSubjectArr = array();
					$CmpSubjectArr = $lreportcard->GET_COMPONENT_SUBJECT($ClassSubjectID, $ClassLevelID);
					if(!empty($CmpSubjectArr)){	
						$CmpLastModifiedArr = array();
						for($k=0 ; $k<count($CmpSubjectArr) ; $k++){
							$CmpSubjectIDArr[] = $CmpSubjectArr[$k]['SubjectID'];
							
							// BIBA Cust - check if component subject with subject topic
							if($eRCTemplateSetting['Settings']['SubjectTopics'] && !$thisSubjectWithTopic && isset($SubjectTopicArr[$CmpSubjectArr[$k]['SubjectID']][1])){
								$thisSubjectWithTopic = true;
							}
						}
						# Get Last Modified Time Among Component Subjects
						$ParentLastModifiedArr = $lreportcard->GET_CLASS_PARENT_MARKSHEET_LAST_MODIFIED($ClassSubjectID, $CmpSubjectIDArr, $ReportColumnIDList, $ClassID, $ReportID);
						$ParentCommentLastModifiedArr = $lreportcard->GET_CLASS_PARENT_MARKSHEET_SUBJECT_COMMENT_LAST_MODIFIED($ClassSubjectID, $CmpSubjectIDArr, $ClassID, $ReportID);
						
						// [2016-0302-1428-20207] get marksheet feedback of component subjects
						if(!$isSelectAllSubject){
							$CmpSubjectFbArr = $CmpSubjectIDArr;
							$CmpSubjectFbArr[] = $ClassSubjectID;
							$MarksheetFeedback = $lreportcard->Get_Marksheet_Feedback_Info($ReportID, $CmpSubjectFbArr);
							$MarksheetFeedback = BuildMultiKeyAssoc($MarksheetFeedback, array("StudentID","SubjectID"),array("RecordStatus"),1);
						}
					}
					
					# Check whether all column weights are zero or not corresponding to each subject of a report
					$isAllColumnWeightZero = $lreportcard->IS_ALL_REPORT_COLUMN_WEIGHTS_ZERO($ReportID, $ClassSubjectID);
					$AllSubjectColumnWeightStatus[] = $isAllColumnWeightZero;
					
					# Check whether the marksheet is editable or not 
					$isEdit = $lreportcard->CHECK_MARKSHEET_STATUS($ClassSubjectID, $ClassLevelID, $ReportID, $CmpSubjectIDArr);
					
					# Feedback
					
					# Last Modified Info
					$LastModifiedDate = '';
					$LastModifiedBy = '';
					$LastModified = '-';
					
					
					//if(!empty($CmpSubjectArr) && count($ParentLastModifiedArr) > 0 && !$displayOverallColumnDueToAllZeroWeight){
					if(!empty($CmpSubjectArr) && count($ParentLastModifiedArr) > 0){
						$LastModifiedDate = (isset($ParentLastModifiedArr[$ClassID][0])) ? $ParentLastModifiedArr[$ClassID][0] : '';
						$LastModifiedBy = (isset($ParentLastModifiedArr[$ClassID][1])) ? $ParentLastModifiedArr[$ClassID][1] : '';
						
						$ParentCommentLastModifiedDate = (isset($ParentCommentLastModifiedArr[$ClassID][0])) ? $ParentCommentLastModifiedArr[$ClassID][0] : '';
						$ParentCommentLastModifiedBy = (isset($ParentCommentLastModifiedArr[$ClassID][1])) ? $ParentCommentLastModifiedArr[$ClassID][1] : '';
						if ($ParentCommentLastModifiedDate != '' && $ParentCommentLastModifiedDate > $LastModifiedDate) {
							$LastModifiedDate = $ParentCommentLastModifiedDate;
							$LastModifiedBy = $ParentCommentLastModifiedBy;
						}
						
						if($LastModifiedDate != "") $LastModified = $LastModifiedBy.'<br>'.$LastModifiedDate;
					} else if(!empty($LastModifiedArr) || !empty($LastModifiedOverallArr) || !empty($SubjectCommentLastModifiedArr)){
						/*
						if($SubjectID != ""){
							$LastModifiedDate = (isset($LastModifiedArr[$ClassID][0])) ? $LastModifiedArr[$ClassID][0] : '';
							$LastModifiedBy = (isset($LastModifiedArr[$ClassID][1])) ? $LastModifiedArr[$ClassID][1] : '';
						}
						else {
							$LastModifiedDate = (isset($LastModifiedArr[$ClassSubjectID][$ClassID][0])) ? $LastModifiedArr[$ClassSubjectID][$ClassID][0] : '';
							$LastModifiedBy = (isset($LastModifiedArr[$ClassSubjectID][$ClassID][1])) ? $LastModifiedArr[$ClassSubjectID][$ClassID][1] : '';
						}
						*/
						
						$LastAssessmentModifiedDate = ($SubjectID=="")? $LastModifiedArr[$ClassSubjectID][$ClassID][0] : $LastModifiedArr[$ClassID][0];
						$LastAssessmentModifiedBy = ($SubjectID=="")? $LastModifiedArr[$ClassSubjectID][$ClassID][1] : $LastModifiedArr[$ClassID][1];
						$LastOverallModifiedDate = ($SubjectID=="")? $LastModifiedOverallArr[$ClassSubjectID][$ClassID][0] : $LastModifiedOverallArr[$ClassID][0];
						$LastOverallModifiedBy = ($SubjectID=="")? $LastModifiedOverallArr[$ClassSubjectID][$ClassID][1] : $LastModifiedOverallArr[$ClassID][1];
						$LastOverallModifiedDate = ($SubjectID=="")? $LastModifiedOverallArr[$ClassSubjectID][$ClassID][0] : $LastModifiedOverallArr[$ClassID][0];
						$LastOverallModifiedBy = ($SubjectID=="")? $LastModifiedOverallArr[$ClassSubjectID][$ClassID][1] : $LastModifiedOverallArr[$ClassID][1];
						
						if ($LastAssessmentModifiedDate == '' && $LastOverallModifiedDate == '')
						{
							$LastModifiedDate = '';
							$LastModifiedBy = '';
						}
						else if ($LastAssessmentModifiedDate > $LastOverallModifiedDate)
						{
							$LastModifiedDate = $LastAssessmentModifiedDate;
							$LastModifiedBy = $LastAssessmentModifiedBy;
						}
						else
						{
							$LastModifiedDate = $LastOverallModifiedDate;
							$LastModifiedBy = $LastOverallModifiedBy;
						}
						
						$SubjectCommentLastModifiedDate = ($SubjectID=="")? $SubjectCommentLastModifiedArr[$ClassSubjectID][$ClassID][0] : $SubjectCommentLastModifiedArr[$ClassID][0];
						$SubjectCommentLastModifiedBy = ($SubjectID=="")? $SubjectCommentLastModifiedArr[$ClassSubjectID][$ClassID][1] : $SubjectCommentLastModifiedArr[$ClassID][1];
						if ($SubjectCommentLastModifiedDate != '' && $SubjectCommentLastModifiedDate > $LastModifiedDate) {
							$LastModifiedDate = $SubjectCommentLastModifiedDate;
							$LastModifiedBy = $SubjectCommentLastModifiedBy;
						}
							
						if($LastModifiedDate != "") $LastModified = $LastModifiedBy.'<br>'.$LastModifiedDate;
					}
					
					# State of IsCompleted 
					$ProgressArr = array();
					$ProgressArr = $lreportcard->GET_MARKSHEET_SUBMISSION_PROGRESS($ClassSubjectID, $ReportID, $ClassID);
					
					# Initialization - Display Icon
					$CmpSubjectIcon = "-";
					$CmpSubjectMSIcon = "-";
					$MarksheetIcon = "-";
					$TCommentIcon = "-";
					$ReportPreviewIcon = "-";
					$FeedbackIcon = "-";
					# Added on 20080829 by Andy
					$ExtraInfoIcon = "-";
					# Added on 20081208 by Ivan
					$AdjustPositionIcon = "-";
					
					if ($eRCTemplateSetting['ShowPersonalCharInMarksheetRevision']) {
						$PersonalCharIcon = '<a href="javascript:jEDIT_PERSONAL_CHAR(\''.$ClassArr[$i][0].'\', \''.$ClassSubjectID.'\', 2)"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_edit_b.gif" width="20" height="20" border="0" title="'.$button_edit.' '.$eReportCard['PersonalCharacteristics'].'"></a>';
					}
					
					if ($eRCTemplateSetting['PreviewReportSubjectSectionInMarksheet']) {
						$ReportPreviewIcon_HTML = "<a href=\"javascript:newWindow('../../reports/generate_reports/print_preview.php?ReportID=$ReportID&SubjectID=$thisSubjectID&SubjectSectionOnly=1&ClassID=$ClassID', '10');\" class=\"tablelink\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_view.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\"></a>";
					}
					
					if($isAllColumnWeightZero == 0){	// if Any Report Column Weight is not Zero
					
						// check whether there is any marksheet feedback which is not closed.
						$InProgressRemarks = '';
						$CmpInProgressRemarks = '';
						foreach($StudentList as $thisStudentID)
						{
							// [2016-0302-1428-20207] check if any marksheet feedback of subject component which is not closed.
							if(!empty($CmpSubjectIDArr))
							{
								foreach((array)$CmpSubjectIDArr as $thisCmpSubjectID)
								{	
									if($MarksheetFeedback[$thisStudentID][$thisCmpSubjectID]==2)
									{
										$CmpInProgressRemarks = "<span class='tabletextrequire'>*</span>";
										break;
									}
								}
							}
						
							if($MarksheetFeedback[$thisStudentID][$ClassSubjectID]==2)
							{
								$InProgressRemarks = "<span class='tabletextrequire'>*</span>";
								break;
							}	
						}
						
						if($ck_ReportCard_UserType == "TEACHER")
						{
							switch($PeriodType)
							{
								case -1:	 	# Period not set yet (NULL)
								case 0:			# before the Period
									break;
								case 1:			# within  the Period
									// [2016-0302-1428-20207]
									$CmpSubjectIcon = ((!empty($CmpSubjectArr)) ? '<a href="javascript:jEDIT_MARKSHEET(\''.$ClassArr[$i][0].'\', \''.$ClassSubjectID.'\', 0)" class="tablegreenlink"><strong>'.count($CmpSubjectArr).'</strong></a>'.$CmpInProgressRemarks : '-');
									$CmpSubjectMSIcon = ((!empty($CmpSubjectArr)) ? '<a href="javascript:jEDIT_MARKSHEET(\''.$ClassArr[$i][0].'\', \''.$ClassSubjectID.'\', 2)"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_edit_b.gif" width="20" height="20" border="0" title="'.$button_edit.' '.$eReportCard['CmpSubjectMarksheet'].'"></a>' : '-');
									if($SubjectListArr[$j]['SchemeID'] != null && $SubjectListArr[$j]['SchemeID'] != "")
									{
								    	$MarksheetIcon = '<a href="javascript:jEDIT_MARKSHEET(\''.$ClassArr[$i][0].'\', \''.$ClassSubjectID.'\', 1)">';
								    	// [2015-1008-1356-44164]
								    	$MarksheetIcon .= (($isEdit && !empty($CmpSubjectArr)) || (empty($CmpSubjectArr) && $ReportType != "F") || ($isEdit && $ReportType == "F" && empty($CmpSubjectArr)) || ($ReportType == "F" && $displayOverallColumnDueToAllZeroWeight)) ? '<img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_edit_b.gif" width="20" height="20" border="0" title="'.$button_edit.' '.$eReportCard['Marksheet'].'">' : '<img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_view.gif" width="20" height="20" border="0" title="'.$button_view.' '.$eReportCard['Marksheet'].'">';
								    	$MarksheetIcon .= '</a>';
							    	}
							    	else {
								    	$MarksheetIcon = 'X';
							    	}
									$TCommentIcon = '<a href="javascript:jEDIT_TEACHER_COMMENT(\''.$ClassArr[$i][0].'\', \''.$ClassSubjectID.'\')"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_edit_b.gif" width="20" height="20" border="0" title="'.$button_edit.' '.$eReportCard['TeacherComment'].'"></a>';
									$FeedbackIcon = '<a href="javascript:jVIEW_FEEDBACK(\''.$ClassArr[$i][0].'\', \''.$ClassSubjectID.'\')"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_view.gif" width="20" height="20" border="0" title="'.$button_view.' '.$eReportCard['Feedback'].'"></a>'.$InProgressRemarks;
									
									$ExtraInfoIcon = '<a href="javascript:jEDIT_EXTRA_INFO(\''.$ClassArr[$i][0].'\', \''.$ClassSubjectID.'\')"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_edit_b.gif" width="20" height="20" border="0" title="'.$button_edit.' '.$eReportCard['ExtraInfoLabel'].'"></a>';
									
									# uccke only allows admin to use the manual adjust position 
									if ($ReportCardCustomSchoolName != "ucc_ke")
										$AdjustPositionIcon = '<a href="javascript:jEDIT_ADJUST_POSITION(\''.$ClassArr[$i][0].'\', \''.$ClassSubjectID.'\')"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_edit_b.gif" width="20" height="20" border="0" title="'.$button_edit.' '.$eReportCard['AdjustPositionLabal'].'"></a>';
									
//									if ($eRCTemplateSetting['PreviewReportSubjectSectionInMarksheet']) {
//								  		$ReportPreviewIcon = $ReportPreviewIcon_HTML;
//								  	}
								  	
									break;
								case 2:			# after the Period
									// [2016-0302-1428-20207]
 									$CmpSubjectIcon = ((!empty($CmpSubjectArr)) ? '<a href="javascript:jEDIT_MARKSHEET(\''.$ClassArr[$i][0].'\', \''.$ClassSubjectID.'\', 0)" class="tablegreenlink"><strong>'.count($CmpSubjectArr).'</strong></a>'.$CmpInProgressRemarks : '-');
//									$CmpSubjectMSIcon = '-';
									$CmpSubjectMSIcon = ((!empty($CmpSubjectArr)) ? '<a href="javascript:jEDIT_MARKSHEET(\''.$ClassArr[$i][0].'\', \''.$ClassSubjectID.'\', 2)"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_edit_b.gif" width="20" height="20" border="0" title="'.$button_edit.' '.$eReportCard['CmpSubjectMarksheet'].'"></a>' : '-');
									if($SubjectListArr[$j]['SchemeID'] != null && $SubjectListArr[$j]['SchemeID'] != "")
									{
//										$MarksheetIcon = '-';
								    	$MarksheetIcon = '<a href="javascript:jEDIT_MARKSHEET(\''.$ClassArr[$i][0].'\', \''.$ClassSubjectID.'\', 1)">';
								    	// [2015-1008-1356-44164]
								    	$MarksheetIcon .= (($isEdit && !empty($CmpSubjectArr)) || (empty($CmpSubjectArr) && $ReportType != "F") || ($isEdit && $ReportType == "F" && empty($CmpSubjectArr)) || ($ReportType == "F" && $displayOverallColumnDueToAllZeroWeight)) ? '<img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_edit_b.gif" width="20" height="20" border="0" title="'.$button_edit.' '.$eReportCard['Marksheet'].'">' : '<img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_view.gif" width="20" height="20" border="0" title="'.$button_view.' '.$eReportCard['Marksheet'].'">';
								    	$MarksheetIcon .= '</a>';

							    	} else {
								    	$MarksheetIcon = 'X';
							    	}
							    	//$TCommentIcon = '<a href="javascript:jEDIT_TEACHER_COMMENT(\''.$ClassArr[$i][0].'\', \''.$ClassSubjectID.'\')"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_view.gif" width="20" height="20" border="0" title="'.$button_preview.' '.$eReportCard['TeacherComment'].'"></a>';
							    	$FeedbackIcon = '<a href="javascript:jVIEW_FEEDBACK(\''.$ClassArr[$i][0].'\', \''.$ClassSubjectID.'\')"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_view.gif" width="20" height="20" border="0" title="'.$button_preview.' '.$eReportCard['Feedback'].'"></a>'.$InProgressRemarks;
							    	
//							    	if ($eRCTemplateSetting['PreviewReportSubjectSectionInMarksheet']) {
//								  		$ReportPreviewIcon = "X";
//								  	}
									break;
							}
							
							if ($eRCTemplateSetting['PreviewReportSubjectSectionInMarksheet']) {
						  		$ReportPreviewIcon = $ReportPreviewIcon_HTML;
						  	}
					  	}
					  	else
					  	{
					  		// [2016-0302-1428-20207]
						  	$CmpSubjectIcon = ((!empty($CmpSubjectArr)) ? '<a href="javascript:jEDIT_MARKSHEET(\''.$ClassArr[$i][0].'\', \''.$ClassSubjectID.'\', 0)" class="tablegreenlink"><strong>'.count($CmpSubjectArr).'</strong></a>'.$CmpInProgressRemarks : '-');
						  	
						  	if($ck_ReportCard_UserType=="VIEW_GROUP") {
								$CmpSubjectMSIcon = ((!empty($CmpSubjectArr)) ? '<a href="javascript:jEDIT_MARKSHEET(\''.$ClassArr[$i][0].'\', \''.$ClassSubjectID.'\', 2)"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_view.gif" width="20" height="20" border="0" title="'.$button_view.' '.$eReportCard['CmpSubjectMarksheet'].'"></a>' : '-');
						  	}
							else {
								$CmpSubjectMSIcon = ((!empty($CmpSubjectArr)) ? '<a href="javascript:jEDIT_MARKSHEET(\''.$ClassArr[$i][0].'\', \''.$ClassSubjectID.'\', 2)"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_edit_b.gif" width="20" height="20" border="0" title="'.$button_edit.' '.$eReportCard['CmpSubjectMarksheet'].'"></a>' : '-');
							}
							
							if($SubjectListArr[$j]['SchemeID'] != null && $SubjectListArr[$j]['SchemeID'] != "")
							{
						    	$MarksheetIcon = '<a href="javascript:jEDIT_MARKSHEET(\''.$ClassArr[$i][0].'\', \''.$ClassSubjectID.'\', 1)">';
						    	
						    	if($ck_ReportCard_UserType=="VIEW_GROUP") {
						    		$MarksheetIcon .= '<img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_view.gif" width="20" height="20" border="0" title="'.$button_view.' '.$eReportCard['Marksheet'].'">';
						    	}
						    	else { 
						    		$MarksheetIcon .= (($isEdit && !empty($CmpSubjectArr)) || (empty($CmpSubjectArr) && $ReportType != "F") || ($isEdit && $ReportType == "F" && empty($CmpSubjectArr)) || ($ReportType == "F" && $displayOverallColumnDueToAllZeroWeight)) ? '<img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_edit_b.gif" width="20" height="20" border="0" title="'.$button_edit.' '.$eReportCard['Marksheet'].'">' : '<img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_view.gif" width="20" height="20" border="0" title="'.$button_view.' '.$eReportCard['Marksheet'].'">';
						    	}
						    	
						    	$MarksheetIcon .= '</a>';
					    	}
					    	else {
						    	$MarksheetIcon = 'X';
					    	}
					    	
					    	if($ck_ReportCard_UserType=="VIEW_GROUP") {
					    		$TCommentIcon = '<a href="javascript:jEDIT_TEACHER_COMMENT(\''.$ClassArr[$i][0].'\', \''.$ClassSubjectID.'\')"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_view.gif" width="20" height="20" border="0" title="'.$button_view.' '.$eReportCard['TeacherComment'].'"></a>';
					    	}
					    	else {
								$TCommentIcon = '<a href="javascript:jEDIT_TEACHER_COMMENT(\''.$ClassArr[$i][0].'\', \''.$ClassSubjectID.'\')"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_edit_b.gif" width="20" height="20" border="0" title="'.$button_edit.' '.$eReportCard['TeacherComment'].'"></a>';
					    	}
							
							$FeedbackIcon = '<a href="javascript:jVIEW_FEEDBACK(\''.$ClassArr[$i][0].'\', \''.$ClassSubjectID.'\')"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_view.gif" width="20" height="20" border="0" title="'.$button_view.' '.$eReportCard['Feedback'].'"></a>'.$InProgressRemarks;
							
							if($ck_ReportCard_UserType=="VIEW_GROUP") {
								$ExtraInfoIcon = '<a href="javascript:jEDIT_EXTRA_INFO(\''.$ClassArr[$i][0].'\', \''.$ClassSubjectID.'\')"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_view.gif" width="20" height="20" border="0" title="'.$button_view.' '.$eReportCard['ExtraInfoLabel'].'"></a>';
							}
							else {
								$ExtraInfoIcon = '<a href="javascript:jEDIT_EXTRA_INFO(\''.$ClassArr[$i][0].'\', \''.$ClassSubjectID.'\')"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_edit_b.gif" width="20" height="20" border="0" title="'.$button_edit.' '.$eReportCard['ExtraInfoLabel'].'"></a>';
							}
							
							$AdjustPositionIcon = '<a href="javascript:jEDIT_ADJUST_POSITION(\''.$ClassArr[$i][0].'\', \''.$ClassSubjectID.'\')"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_edit_b.gif" width="20" height="20" border="0" title="'.$button_edit.' '.$eReportCard['AdjustPositionLabal'].'"></a>';
							
							if ($eRCTemplateSetting['PreviewReportSubjectSectionInMarksheet']) {
						  		$ReportPreviewIcon = $ReportPreviewIcon_HTML;
						  	}
					  	}
					
						// BIBA Cust - for subjects or component subjects with subject topic
						if ($thisSubjectWithTopic){
							$CmpSubjectMSIcon = "-";
							if($SubjectListArr[$j]['SchemeID'] != null && $SubjectListArr[$j]['SchemeID'] != ""){
								$MarksheetIcon = "-";
							}
							$FeedbackIcon = "-";
						}
				    }

					# Content
					$display .= '<tr class="tablegreenrow1" height="38">'."\n";
				    	$display .= '<td class="'.$row_css.' tabletext">'. $ClassName. '</td>'."\n";
				    	$display .= '<td class="'.$row_css.' tabletext">'. $ClassSubjectName .'</td>'."\n";
				    	
				    	if (!$eRCTemplateSetting['Management']['Marksheet']['HideComponentSubject']) {
				    		$display .= '<td align="center" class="'.$row_css.' tabletext">'. $CmpSubjectIcon .'</td>'."\n";
				    	}
				        
				        $display .= '<td align="center" class="'.$row_css.' tabletext">'. $CmpSubjectMSIcon .'</td>'."\n";
				        
				        if (!$eRCTemplateSetting['Management']['Marksheet']['HideMarksheet']) {
				        	$display .= '<td align="center" class="'.$row_css.' tabletext">'. $MarksheetIcon .'</td>'."\n";
				        }
					
						// Personal Characteristics
						if ($eRCTemplateSetting['ShowPersonalCharInMarksheetRevision']) {
							$display .= '<td align="center" class="'.$row_css.' tabletext">'. $PersonalCharIcon .'</td>';
						}
					
						// Subject Teacher Comment	  
						$display .= '<td align="center" class="'.$row_css.' tabletext">'. $TCommentIcon .'</td>';
						
						// Preview Report
						if ($eRCTemplateSetting['PreviewReportSubjectSectionInMarksheet']) {
							$display .= '<td align="center" class="'.$row_css.' tabletext">'. $ReportPreviewIcon .'</td>';
					  	}
						
						// Marksheet Feedback
						if (!$eRCTemplateSetting['Management']['Marksheet']['HideFeedback']) {
							if($accessSettingData[EnableVerificationPeriod]==1){
								$display .= '<td align="center" class="'.$row_css.' tabletext">'. $FeedbackIcon .'</td>';
							}
						}
						
						// Effort
						// [2015-1104-1130-08164]
						if ($libreportcardcustom->IsEnableMarksheetExtraInfo) {
							$display .= '<td align="center" class="'.$row_css.' tabletext">'. $ExtraInfoIcon .'</td>';
						}
						else if ($eRCTemplateSetting['Marksheet']['ManualInputGrade_WholeYearOnly']) {
							$display .= '<td align="center" class="'.$row_css.' tabletext">'. $ExtraInfoIcon .'</td>';
						}
						
						// Manual Adjust Position
						if ($libreportcardcustom->IsEnableManualAdjustmentPosition) {
							$display .= '<td align="center" class="'.$row_css.' tabletext">'. $AdjustPositionIcon .'</td>';
						}
						
					    $display .= '<td class="'.$row_css.' tabletext">'. $LastModified .'</td>'."\n";
					    $display .= '<td align="center" class="'.$row_css.' tabletext">'."\n";
				          
				     
					    // the followings can be use in teahcer view 
					    if($isAllColumnWeightZero == 0){
					       	if($ck_ReportCard_UserType == "TEACHER" || $ck_ReportCard_UserType == "VIEW_GROUP"){
						    	if($PeriodType != 1){	// not within submission period
							     	if($PeriodType <= 0) 
							     	{
								     	$display .= "-";
							     	}
							     	else
							     	{
								     	if(count($ProgressArr) > 0 && $ProgressArr['IsCompleted']){	// show imcompleted status
								     		$display .= $eReportCard['Confirmed'];
								     	}
								     	else {	// show completed status
								     		$display .= $eReportCard['NotCompleted'];
								     	}
							     	}
							    }
							    else {
								    $display .= $linterface->GET_SELECTION_BOX($CompleteArr, "name='complete_".$ClassSubjectID."_".$ClassID."' id='complete_".$ClassSubjectID."_".$ClassID."' class='tabletexttoptext'", "", ((count($ProgressArr) > 0) ? $ProgressArr['IsCompleted'] : 0));
							    }
				     	    }
				     	    else if($ck_ReportCard_UserType == "ADMIN"){
						 		$display .= $linterface->GET_SELECTION_BOX($CompleteArr, "name='complete_".$ClassSubjectID."_".$ClassID."' id='complete_".$ClassSubjectID."_".$ClassID."' class='tabletexttoptext'", "", ((count($ProgressArr) > 0) ? $ProgressArr['IsCompleted'] : 0));
				     	 	}
			     	    }
			     	    else 
			     	    {
				     	 	$display .= '-';
			     	    }
					     
					    $display .= '</td>'."\n";
					$display .= '</tr>'."\n";
				      	
				    $cnt++;
			    }
			}
		}
		else {
			$td_colspan = "9";
			$display .= '
					<tr class="tablegreenrow1" height="40">
			          <td class="attendancepresent tabletext" colspan="'.$td_colspan.'" align="center">'.$i_no_record_exists_msg.'</td>
			      	</tr>';
		}
		
	
		// the followings can be used in teacher view
		if($ck_ReportCard_UserType == "TEACHER" || $ck_ReportCard_UserType=="VIEW_GROUP"){
			if($PeriodType != 1){	// not within submission period
				$completeAll = '';
			}
			else {	// out of submission period
				$completeAll .= $linterface->GET_SELECTION_BOX($CompleteAllArr, 'name="completeAll" id="completeAll" class="tabletexttoptext" onchange="setCompleteAll(\''.$SubjectID.'\')"', "", "");
			}
		}
		else if($ck_ReportCard_UserType == "ADMIN"){
			$completeAll .= $linterface->GET_SELECTION_BOX($CompleteAllArr, 'name="completeAll" id="completeAll" class="tabletexttoptext" onchange="setCompleteAll(\''.$SubjectID.'\')"', "", "");
		}
		
		
		$table = '';
		$table .= '<table width="100%" border="0" cellspacing="0" cellpadding="4">'."\r\n";
        	$table .= '<tr class="tablegreentop">'."\r\n";
          		$table .= '<td class="tabletopnolink">'.$Lang['General']['Class'].'</td>'."\r\n";
          		$table .= '<td class="tabletopnolink">'.$eReportCard['Subject'].'</td>'."\r\n";
          		
          		if (!$eRCTemplateSetting['Management']['Marksheet']['HideComponentSubject']) {
          			$table .= '<td width="80" align="center" class="tabletopnolink">'.$eReportCard['CmpSubjects'].'</td>'."\r\n";
          		}
          		
          		$table .= '<td width="80" align="center" class="tabletopnolink">'.$eReportCard['CmpSubjectMarksheet'].'</td>'."\r\n";
          		
          		if (!$eRCTemplateSetting['Management']['Marksheet']['HideMarksheet']) {
          			$table .= '<td width="80" align="center" class="tabletopnolink">'.$eReportCard['Marksheet'].'</td>'."\r\n";
          		}
          		
          		// Personal Characteristics
          		if ($eRCTemplateSetting['ShowPersonalCharInMarksheetRevision']) {
          			$table .= '<td width="80" align="center" class="tabletopnolink">'.$eReportCard['PersonalCharacteristics'].'</td>'."\r\n";
          		}
          		
          		// Subject Teacher Comment
          		$table .= '<td width="80" align="center" class="tabletopnolink">'.$eReportCard['TeacherComment'].'</td>'."\r\n";
		  		
		  		// Report Preview
		  		if ($eRCTemplateSetting['PreviewReportSubjectSectionInMarksheet']) {
		  			$table .= '<td width="80" align="center" class="tabletopnolink">'.$Lang['eReportCard']['ManagementArr']['MarksheetRevisionArr']['PreviewReport'].'</td>'."\r\n";
		  		}
		  		
		  		// Marksheet Feedback
		  		if (!$eRCTemplateSetting['Management']['Marksheet']['HideFeedback']) {
		  			if($accessSettingData[EnableVerificationPeriod]==1){
		  				$table .= '<td width="80" align="center" class="tabletopnolink">'.$eReportCard['Feedback'].'</td>'."\r\n";
		  			}
		  		}
		  		
		  		// Effort
		  		// [2015-1104-1130-08164]
		  		if($libreportcardcustom->IsEnableMarksheetExtraInfo) {
		  			$table .= '<td width="80" align="center" class="tabletopnolink">'.$eReportCard['ExtraInfoLabel'].'</td>'."\r\n";
		  		}
		  		else if ($eRCTemplateSetting['Marksheet']['ManualInputGrade_WholeYearOnly']) {
		  			$table .= '<td width="80" align="center" class="tabletopnolink">'.$eReportCard['ExtraInfoLabel'].'</td>'."\r\n";
		  		}
		  		
		  		// Maunal Adjust Position
		  		if($libreportcardcustom->IsEnableManualAdjustmentPosition) {
		  			$table .= '<td width="80" align="center" class="tabletopnolink">'.$eReportCard['AdjustPositionLabal'].'</td>'."\r\n";
		  		}
		  		
				$table .= '<td class="tabletopnolink">'.$eReportCard['LastModifiedDate'].'</td>'."\r\n";
				$table .= '<td width="80" align="center" class="tabletopnolink">'.$completeAll.'</td>'."\r\n";
			$table .= '</tr>'."\r\n";
			
			// Table Content
			$table .= $display."\r\n";
			
		$table .= '</table>'."\r\n";
		
		
		
		$total = $eReportCard['TotalRecords']." ";
		$total .= ($isFormSubject || $SubjectID == "") ? $cnt : 0;
		
		# Button
		$display_button = '';
		if($cnt > 0){
			if(in_array(0, $AllSubjectColumnWeightStatus)){
				if($ck_ReportCard_UserType == "ADMIN" || ($ck_ReportCard_UserType == "TEACHER" && $PeriodType == 1) ){// within submission period
					$display_button .= '
				  <tr>
				    <td>
				      <br>
				  	  <table width="100%" border="0" cellspacing="0" cellpadding="0">
				        <tr>
				          <td height="1" class="dotline"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/10x10.gif" width="10" height="1"></td>
				        </tr>
				      	<tr>
				          <td>
				            <table width="100%" border="0" cellspacing="0" cellpadding="2">
				              <tr>
				                <td align="center" valign="bottom">';
					$display_button .= '<input name="Save" type="button" class="formbutton" onMouseOver="this.className=\'formbuttonon\'" onMouseOut="this.className=\'formbutton\'" value="'.$button_save.'" onclick="jSUBMIT_FORM()">';
			        $display_button .= '<input name="Reset" type="reset" class="formbutton" onMouseOver="this.className=\'formbuttonon\'" onMouseOut="this.className=\'formbutton\'" value="'.$button_reset.'">';		                
				    $display_button .= '
				                </td>
				              </tr>
				        	</table>
				          </td>
				      	</tr>
				      </table>
				    </td>
				  </tr>';
			  	}
    		}
		}
		
		# Added on 5 Jan 2009 by Ivan
		# Show Submission Start Date and End Date
		# construct the condition
		$scheduleEntries = $lreportcard->GET_REPORT_DATE_PERIOD("", $ReportID);
		$subStart = ($scheduleEntries[0]["SubStart"]=="") ? "-" : date("Y-m-d H:i:s",$scheduleEntries[0]["SubStart"]);
		$subEnd = ($scheduleEntries[0]["SubEnd"]=="") ? "-" : date("Y-m-d H:i:s",$scheduleEntries[0]["SubEnd"]);
		if($hideAllReportInfo){
			$subStart = $subEnd = "-";
		}
		
		### Btn
		if($eRCTemplateSetting['DisableMarksheetImport']!==true && $ck_ReportCard_UserType!="VIEW_GROUP") {
			$ImportBtn = $linterface->Get_Content_Tool_v30("import", "marksheet_import.php?ReportID=$ReportID&ClassLevelID=$ClassLevelID&TagsType=$TagsType");
		}
		if($accessSettingData[EnableVerificationPeriod]==1 && !$hideAllReportInfo){
			$ExportBtn = $linterface->Get_Content_Tool_v30("export", "javascript:js_Export();",$eReportCard['ManagementArr']['MarksheetRevisionArr']['MarksheetFeedbackArr']['ExportFeedback']);
			// [2016-0105-1050-50207]
			$exportTeacherCommentTitle = $Lang['Btn']['Export'].' '.$eReportCard['TeacherComment'];
			$ExportCommentBtn = $linterface->Get_Content_Tool_v30("export", "javascript:js_export_comment();", $exportTeacherCommentTitle);
		}
		
		$specificSubmissionDisplay = $lreportcard_schedule->getTeacherSpecialMarksheetSubmissionPeriodDisplay($ReportID, $_SESSION['UserID']);
		if ($specificSubmissionDisplay != '') {
			$specificSubmissionRemarks = '<tr><td>'.$lreportcard_schedule->getTeacherSpecialMarksheetSubmissionPeriodRemarks().'</td></tr>';
		}
		
		############################################################################################################
		
		# tag information
		$TAGS_OBJ[] = array($PAGE_TITLE, "", 0);


		$linterface->LAYOUT_START();
		
?>

<script language="javascript">
/*
* General JS function(s)
*/
function jGET_SELECTBOX_SELECTED_VALUE(objName){
	var obj = document.getElementById(objName);
	if(obj==null)	return "";
	var index = obj.selectedIndex;
	var val = obj[index].value;
	return val;
}

/* Global */
<?php
$js_form_var = "var FormArr = new Array(";
if(count($FormArr) > 0){
	for($i=0 ; $i<count($FormArr) ; $i++)
		$js_form_var .= ($i != 0) ? ', "'.$FormArr[$i][0].'"' : '"'.$FormArr[$i][0].'"';
}
$js_form_var .= ");";
echo $js_form_var;


?>

/*
* JS function(s) in this page
*/
function setCompleteAll(parSubjectID){
	var classIDArr = (document.getElementsByName("ClassIDList[]")) ? document.getElementsByName("ClassIDList[]") : "";
	var subjectIDArr = (document.getElementsByName("SubjectIDList[]")) ? document.getElementsByName("SubjectIDList[]") : "";
	var indexToSelect = "";
	
	var parStatus = jGET_SELECTBOX_SELECTED_VALUE("completeAll");
	if (parStatus == "completeAll"){
		indexToSelect = 0;
	}
	else if (parStatus == "notCompleteAll"){
		indexToSelect = 1;
	}
	
	if(parStatus != ""){
		if(classIDArr.length > 0 && subjectIDArr.length > 0){
			for(var i=0 ; i<classIDArr.length ; i++){
				for(var j=0 ; j<subjectIDArr.length ; j++){
					if(document.getElementById("complete_"+subjectIDArr[j].value+"_"+classIDArr[i].value)){
						var obj = document.getElementById("complete_"+subjectIDArr[j].value+"_"+classIDArr[i].value);
						obj.selectedIndex = indexToSelect;
						obj.options[indexToSelect].selected = true;
					}
				}
			}
		}
	}
}


function jCHANGE_MARKSHEET_REVISION(ResetReportID){
	var form_id = jGET_SELECTBOX_SELECTED_VALUE("ClassLevelID");
	var report_id = jGET_SELECTBOX_SELECTED_VALUE("ReportType");
	var subject_id = jGET_SELECTBOX_SELECTED_VALUE("SubjectID");
	
	if (ResetReportID)
		report_id = '';
	
	location.href = "?ClassLevelID="+form_id+"&ReportID="+report_id+"&SubjectID="+subject_id;
}

// jType: 1 - Parent Subject , 0 = Component Subject
function jEDIT_MARKSHEET(class_id, target_subject_id, jType){
	var form_id = <?=(($ClassLevelID != "") ? $ClassLevelID : "''")?>;
	var report_id = <?=(($ReportID != "") ? $ReportID : "''")?>;
	var subject_id = <?=(($SubjectID != "") ? $SubjectID : "''")?>;
	
	report_id = (report_id == "") ? jGET_SELECTBOX_SELECTED_VALUE("ReportType") : report_id;
	subject_id = (subject_id == "") ? target_subject_id : subject_id;
	
	params = "ClassLevelID="+form_id+"&ReportID="+report_id+"&SubjectID="+subject_id+"&ClassID="+class_id;
	if(jType == 1)
		location.href = "marksheet_edit.php?"+params;
	else if(jType == 0)
		location.href = "marksheet_revision2.php?"+params;
	else if(jType == 2)
		location.href = "marksheet_cmp_edit.php?"+params;
}


function jEDIT_TEACHER_COMMENT(class_id, target_subject_id){
	var form_id = <?=(($ClassLevelID != "") ? $ClassLevelID : "''")?>;
	var report_id = <?=(($ReportID != "") ? $ReportID : "''")?>;
	var subject_id = <?=(($SubjectID != "") ? $SubjectID : "''")?>;
	
	report_id = (report_id == "") ? jGET_SELECTBOX_SELECTED_VALUE("ReportType") : report_id;
	subject_id = (subject_id == "") ? target_subject_id : subject_id;
	
	location.href = "teacher_comment.php?ClassLevelID="+form_id+"&ReportID="+report_id+"&SubjectID="+subject_id+"&ClassID="+class_id;
}

function jEDIT_EXTRA_INFO(class_id, target_subject_id){
	var form_id = <?=(($ClassLevelID != "") ? $ClassLevelID : "''")?>;
	var report_id = <?=(($ReportID != "") ? $ReportID : "''")?>;
	var subject_id = <?=(($SubjectID != "") ? $SubjectID : "''")?>;
	
	report_id = (report_id == "") ? jGET_SELECTBOX_SELECTED_VALUE("ReportType") : report_id;
	subject_id = (subject_id == "") ? target_subject_id : subject_id;
	
	location.href = "extra_info.php?ClassLevelID="+form_id+"&ReportID="+report_id+"&SubjectID="+subject_id+"&ClassID="+class_id;
}

function jEDIT_ADJUST_POSITION(class_id, target_subject_id){
	var form_id = <?=(($ClassLevelID != "") ? $ClassLevelID : "''")?>;
	var report_id = <?=(($ReportID != "") ? $ReportID : "''")?>;
	var subject_id = <?=(($SubjectID != "") ? $SubjectID : "''")?>;
	
	report_id = (report_id == "") ? jGET_SELECTBOX_SELECTED_VALUE("ReportType") : report_id;
	subject_id = (subject_id == "") ? target_subject_id : subject_id;
	
	location.href = "extra_info.php?ClassLevelID="+form_id+"&ReportID="+report_id+"&SubjectID="+subject_id+"&ClassID="+class_id+"&type=AdjustPosition";
}

function jEDIT_PERSONAL_CHAR(class_id, target_subject_id){
	var form_id = <?=(($ClassLevelID != "") ? $ClassLevelID : "''")?>;
	var report_id = <?=(($ReportID != "") ? $ReportID : "''")?>;
	var subject_id = <?=(($SubjectID != "") ? $SubjectID : "''")?>;
	
	report_id = (report_id == "") ? jGET_SELECTBOX_SELECTED_VALUE("ReportType") : report_id;
	subject_id = (subject_id == "") ? target_subject_id : subject_id;
	
	location.href = "../personal_characteristics/edit.php?FromMS=1&ClassLevelID="+form_id+"&ReportID="+report_id+"&SubjectID="+subject_id+"&ClassID="+class_id;
}

function jCOMPLETE_MARKSHEET(class_id, target_subject_id, isAll){
	var jCheckBoxObj = (isAll == 1) ? document.getElementById("completeAll") : document.getElementById("complete_"+target_subject_id+"_"+class_id);
	var jCheck = jCheckBoxObj.checked;
	var action = (jCheckBoxObj.value == 0) ? "COMPLETE" : "CANCEL";
	var msg = (jCheckBoxObj.value == 0) ? "<?=$eReportCard['jsAlertToCompleteMarksheet']?>?" : "<?=$eReportCard['jsAlertToIncompleteMarksheet']?>";
		
	if(confirm(msg))
		completeMarksheet(class_id, target_subject_id, isAll, action);
	else {
		jCheckBoxObj.checked = (jCheckBoxObj.value == 1) ? true : false;
	}
}

// Form
function jSUBMIT_FORM(){
	var obj = document.FormMain;
	obj.submit();
}

// Start of AJAX 
var callback = {
    success: function ( o )
    {
    	//alert(o.responseText);
    	jUpdateCompleteCheckBox(o.responseXML);
    }
}
// Main
function completeMarksheet(class_id, target_subject_id, isAll, action)
{
	var classlevel_id = <?=(($ClassLevelID != "") ? $ClassLevelID : "''")?>;
	var report_id = <?=(($ReportID != "") ? $ReportID : "''")?>;
	var subject_id = <?=(($SubjectID != "") ? $SubjectID : "''")?>;

	report_id = (report_id == "") ? jGET_SELECTBOX_SELECTED_VALUE("ReportType") : report_id;
	subject_id = (subject_id == "") ? target_subject_id : subject_id;
	
	var params = '';
	params += "?ActionCode="+action+"&ClassLevelID="+classlevel_id+"&ReportID="+report_id+"&SubjectID="+subject_id;
	if(class_id != null)
		params += "&ClassID="+class_id;
	params += (isAll == 1) ? "&isAll=1" : "&isAll=0";

    var path = "marksheet_revision_aj_update.php" + params;
    var connectionObject = YAHOO.util.Connect.asyncRequest("POST", path, callback);
}

function jUpdateCompleteCheckBox(xmlObj) 
{
	var actionInfo = xmlObj.getElementsByTagName("ActionCode");
	var classlevelinfo = xmlObj.getElementsByTagName("ClassLevelID");
	var subjectInfo = xmlObj.getElementsByTagName("SubjectID");
	var reportInfo = xmlObj.getElementsByTagName("ReportID");
	var isAll = xmlObj.getElementsByTagName("isAll");
	
	var classInfo = xmlObj.getElementsByTagName("ClassID");
	var isUpdate = xmlObj.getElementsByTagName("isUpdate");
	
	var action_code = actionInfo[0].childNodes[0].nodeValue;
	var isAllFlag = isAll[0].childNodes[0].nodeValue;
	
	for (var i=0 ; i<classInfo.length ; i++){
		var subject_id = subjectInfo[i].childNodes[0].nodeValue;
		var class_id = classInfo[i].childNodes[0].nodeValue;
		var jCheckBoxObj = document.getElementById("complete_"+subject_id+"_"+class_id);
		
		if(isUpdate[i].childNodes[0].nodeValue == 1){
			if(action_code == "COMPLETE"){
				jCheckBoxObj.checked = true;
				jCheckBoxObj.value = 1;
			}
			else if(action_code == "CANCEL"){
				jCheckBoxObj.checked = false;
				jCheckBoxObj.value = 0;
			}
		}
	}
	
	if(isAllFlag == 1){
		var jAllObj = document.getElementById("completeAll");
		if(action_code == "COMPLETE"){
			jAllObj.checked = true;
			jAllObj.value = 1;
		}
		else if(action_code == "CANCEL"){
			jAllObj.checked = false;
			jAllObj.value = 0;
		}
	}			
}
// End of AJAX


function jVIEW_FEEDBACK(class_id, target_subject_id){
	var form_id = <?=(($ClassLevelID != "") ? $ClassLevelID : "''")?>;
	var report_id = <?=(($ReportID != "") ? $ReportID : "''")?>;
	var subject_id = <?=(($SubjectID != "") ? $SubjectID : "''")?>;
	
	report_id = (report_id == "") ? jGET_SELECTBOX_SELECTED_VALUE("ReportType") : report_id;
	subject_id = (subject_id == "") ? target_subject_id : subject_id;
	
	location.href = "marksheet_feedback/index.php?ClassLevelID="+form_id+"&ReportID="+report_id+"&SubjectID="+subject_id+"&ClassID="+class_id;
}

function js_Export()
{
	var obj = document.FormMain;
	obj.action = './marksheet_feedback/export_form_feedback.php';
	obj.submit();
	obj.action = './marksheet_complete_confirm.php';
}

function js_export_comment(){
	var obj = document.FormMain;
	obj.action = './marksheet_feedback/export_form_comment.php';
	obj.submit();
	obj.action = './marksheet_complete_confirm_subject_group.php';
}

</script>

<br/>
<form name="FormMain" method="POST" action="marksheet_complete_confirm.php">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr><td>&nbsp;</td></tr> 
  <tr>
    <td>
      <table border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><?=$FormSelection?></td>
          <td>&nbsp;</td>
          <td><?=$ReportTypeSelection?></td>
          <td>&nbsp;</td>
          <td><?=$SubjectSelection?></td>
          <td>&nbsp;</td>
          <td align="right" width="100%"><?=$linterface->GET_SYS_MSG($msg);?></td>
  		</tr>
  		<tr>
          <td colspan="7" class="tabletext">
          	<b><?=$eReportCard['SubmissionStartDate']?>:</b> <?=$subStart?> &nbsp;&nbsp; <b><?=$eReportCard['SubmissionEndDate']?>:</b> <?=$subEnd?>
          	<?=$specificSubmissionDisplay?>
          </td>
  		</tr>
  	  </table>
  	</td>
  </tr> 
  <tr>
    <td>
   		<!--<table border="0" cellspacing="0" cellpadding="2">
	      	<tr>
	      	  <?php 
	      		  # Condition: TagsType Must be "0" - Input Raw Mark & case i
	      		  # case 1: Whole Year Report & $hasDirectInputMark = 1 - allow direct input 
	      		  # case 2: Term Report & subject without component subjects
	      		  # case 3: Subject having Component Subjects & is Editable
//	      		  if($TagsType == 0 && (
//	      		    $ScaleInput == "G" || 
//	      			($ReportType == "F" && $hasDirectInputMark) || 
//	      			($ReportType != "F" && empty($CmpSubjectArr)) || 
//	      			(!empty($CmpSubjectArr) && $isEdit)) )
	      			{ 
	          			
	          ?>
	      	  <td><br><a href="marksheet_import.php<?="?ReportID=$ReportID&ClassLevelID=$ClassLevelID&TagsType=$TagsType"?>" class="contenttool"><img src="<?=$image_path.'/'.$LAYOUT_SKIN?>/icon_import.gif" width="18" height="18" border="0" align="absmiddle"> <?=$button_import?></a></td>
	      	  <td><img src="<?=$image_path.'/'.$LAYOUT_SKIN?>/10x10.gif"></td>
	      	  <?php } ?>
	      	  
	      	</tr>
		  </table>-->
		<br>
		<?php if(!$eRCTemplateSetting['Settings']['SubjectTopics']){ ?>
		<div class="content_top_tool">
			<div class="Conntent_tool">
				<?=$ImportBtn?>
				<?=$ExportBtn?>
				<?=$ExportCommentBtn?>
			</div>
		</div>
		<?php } ?>
    </td>
  </tr> 
  <tr>
    <td>
      <?=$table?>
    </td>
  </tr>
  <tr class="tablegreenbottom">
    <td>
      <table width="100%" border="0" cellspacing="0" cellpadding="3">
        <tr>
    	  <td class="tabletext"><?=$total?></td>
    	</tr>
      </table>
    </td>
  </tr>
  <tr>
  	<td><br /><span class="tabletextrequire">*</span><span class="tabletextremark"><?=$eReportCard['ManagementArr']['MarksheetRevisionArr']['MarksheetFeedbackArr']['FeedbackInProgress']?></span></td>
  </tr>
  <?=$specificSubmissionRemarks?>
  <?=$display_button?>
</table>

<input type="hidden" name="isProgress" id="isProgress" value="0"/>
<input type="hidden" name="ClassLevelID" id="ClassLevelID" value="<?=$ClassLevelID?>"/>
<input type="hidden" name="ReportID" id="ReportID" value="<?=$ReportID?>"/>
<input type="hidden" name="SubjectID" id="SubjectID" value="<?=$SubjectID?>"/>
<input type="hidden" name="MarksheetRevision" id="MarksheetRevision" value="1"/>

<?php 
if(count($ClassArr) > 0){
	for($i=0 ; $i<count($ClassArr) ;$i++){
		$ClassID = $ClassArr[$i][0];
		echo "<input type=\"hidden\" name=\"ClassIDList[]\" id=\"ClassIDList\" value=\"$ClassID\"/>\n";
	}
}

if($SubjectID != ""){
	echo "<input type=\"hidden\" name=\"SubjectIDList[]\" id=\"SubjectIDList\" value=\"$SubjectID\"/>\n";
}
else {
	if(count($SubjectListArr) > 0){
		for($j=0 ; $j<count($SubjectListArr) ; $j++){
			echo "<input type=\"hidden\" name=\"SubjectIDList[]\" id=\"SubjectIDList\" value=\"".$SubjectListArr[$j]['SubjectID']."\"/>\n";
		}
	}
}
?>


</form>


<?

        $linterface->LAYOUT_STOP();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>