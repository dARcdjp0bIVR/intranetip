<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");

$lreportcard = new libreportcardcustom();
$linterface = new interface_html();
$limport = new libimporttext();

	
$CurrentPage = "Management_MarkSheetRevision";
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
	
$lreportcard->hasAccessRight();

$reportId = integerSafe(standardizeFormPostValue($_POST['ReportID']));
$classlevelId = integerSafe(standardizeFormPostValue($_POST['ClassLevelID']));
$subjectId = integerSafe(standardizeFormPostValue($_POST['SubjectID']));
$type = standardizeFormPostValue($_POST['type']);
$param = 'ReportID='.$reportId.'&ClassLevelID='.$classlevelId.'&SubjectID='.$subjectId.'&type='.$type;


### get header
if ($eRCTemplateSetting['Marksheet']['ManualInputGrade']) {
	$column_data = $lreportcard->returnReportTemplateColumnData($ReportID);
	$dataTitle = $column_data[0]['ColumnTitle'].' Grade';
}
else {
	$dataTitle = $eReportCard['ExtraInfoLabel'];
}


# tag information
$TAGS_OBJ[] = array($eReportCard['Management_MarksheetRevision'], "", 0);
# page navigation (leave the array empty if no need)
if ($type=="AdjustPosition") {
	$PAGE_NAVIGATION[] = array($button_import." ".$eReportCard['AdjustPositionLabal']);
}
else {
	$PAGE_NAVIGATION[] = array($button_import." ".$dataTitle);
}
$htmlAry['navigation'] = $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION);
$returnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];
$linterface->LAYOUT_START($returnMsg);


### step display
$htmlAry['generalImportStepTbl'] = $linterface->GET_IMPORT_STEPS($CurrStep=3);


### get report info
$reportInfoAry = $lreportcard->returnReportTemplateBasicInfo($reportId);
$reportTitle = str_replace(":_:", "<br>", $reportInfoAry['ReportTitle']);
$yearTermId = $reportInfoAry['Semester'];


### retrieve import data
$tmpTable = $lreportcard->DBName.".RC_TEMP_IMPORT_EXTRA_INFO";
$sql = "Select StudentID, SubjectID, Info From $tmpTable Where UserID = '".$_SESSION['UserID']."' And ReportID = '".$reportId."'";
$importDataAry = $lreportcard->returnResultSet($sql);
$numOfImportData = count($importDataAry);
$studentIdAry = Get_Array_By_Key($importDataAry, 'StudentID');
$subjectIdAry = Get_Array_By_Key($importDataAry, 'SubjectID');


### retrieve existing data for determining insert or update records
if ($type=="AdjustPosition") {
	$table = $lreportcard->DBName.".RC_REPORT_RESULT_SCORE_ARCHIVE";
	$dataDbField = 'OrderMeritClass';
	$primaryKeyField = 'MarksheetConsolidatedID';
	
	$conds_reportColumnId = " And ReportColumnID = '0' ";
	$update_teacherId = "";
}
else {
	$table = $lreportcard->DBName.".RC_EXTRA_SUBJECT_INFO";
	$dataDbField = 'Info';
	$primaryKeyField = 'ExtraInfoID';
	
	$conds_reportColumnId = "";
	$update_teacherId = ", TeacherID = '".$_SESSION['UserID']."'";
}
$sql = "Select $primaryKeyField as priKey, StudentID, SubjectID, $dataDbField From $table Where ReportID = '".$reportId."' And StudentID IN ('".implode("','", (array)$studentIdAry)."') And SubjectID IN ('".implode("','", (array)$subjectIdAry)."') $conds_reportColumnId";
$existingDataAry = BuildMultiKeyAssoc($lreportcard->returnResultSet($sql), array('StudentID', 'SubjectID'));


### process the import data
$insertAry = array();
for ($i=0; $i<$numOfImportData; $i++) {
	$_studentId = $importDataAry[$i]['StudentID'];
	$_subjectId = $importDataAry[$i]['SubjectID'];
	$_info = $importDataAry[$i]['Info'];
	
	if ($_info == '') {
		$_infoDbValue = 'null';
	}
	else {
		$_infoDbValue = "'".$lreportcard->Get_Safe_Sql_Query($_info)."'";
	}
	
	if (isset($existingDataAry[$_studentId][$_subjectId])) {
		// update
		$_primaryKey = $existingDataAry[$_studentId][$_subjectId]['priKey'];
	
		$sql = "Update $table Set $dataDbField = $_infoDbValue, DateModified = now() $update_teacherId Where $primaryKeyField = '".$_primaryKey."'";
		$successAry['update'][$_primaryKey] = $lreportcard->db_db_query($sql);
	}
	else {
		// insert
		if ($type=="AdjustPosition") {
			$insertAry[] = " ('".$_studentId."', '".$_subjectId."', '".$reportId."', 0, $_infoDbValue, now(), now()) ";
		}
		else {
			$insertAry[] = " ('".$_studentId."', '".$_subjectId."', '".$reportId."', $_infoDbValue, '".$_SESSION['UserID']."', now(), now()) ";
		}
	}
}

if (count($insertAry) > 0) {
	if ($type=="AdjustPosition") {
		$field = "StudentID, SubjectID, ReportID, ReportColumnID, OrderMeritClass, DateInput, DateModified";
	}
	else {
		$field = "StudentID, SubjectID, ReportID, Info, TeacherID, DateInput, DateModified";
	}
	$insertChunkAry = array_chunk($insertAry, 1000);
	$numOfChunk = count($insertChunkAry);
	
	for ($i=0; $i<$numOfChunk; $i++) {
		$_insertAry = $insertChunkAry[$i];
		
		$sql = "INSERT INTO $table ($field) VALUES ".implode(', ', $_insertAry);
		$successAry['insert'][$i] = $lreportcard->db_db_query($sql);
	}
}



### delete old temp data
//$table = $lreportcard->DBName.".RC_TEMP_IMPORT_EXTRA_INFO";
//$sql = "Delete From $table Where UserID = '".$_SESSION['UserID']."' And ReportID = '".$reportId."'";
//$successAry['deleteOldTempData'] = $lreportcard->db_db_query($sql);


$x = '';
$x .= '<table class="form_table_v30">'."\r\n";
	$x .= '<tr>'."\r\n";
		$x .= '<td class="field_title">'.$eReportCard['Settings_ReportCardTemplates'].'</td>'."\r\n";
		$x .= '<td>'."\r\n";
			$x .= $reportTitle;
		$x .= '</td>'."\r\n";
	$x .= '</tr>'."\r\n";
$x .= '</table>'."\r\n";
$htmlAry['reportInfoTbl'] = $x;


# result display
$htmlAry['numOFSuccessDisplay'] = $numOfImportData.' '.$Lang['SysMgr']['FormClassMapping']['RecordsImportedSuccessfully'];



### action buttons
$htmlAry['GoToMarksheetBtn'] = $linterface->Get_Action_Btn($Lang['eReportCard']['ManagementArr']['MarksheetRevisionArr']['GoToMarksheet'], "button", "goSubmit()", 'submitBtn', $ParOtherAttribute="", ($numOfErrorRow>0)? true : false, $ParClass="", $ParExtraClass="actionBtn");

?>
<script type="text/JavaScript" language="JavaScript">
function goSubmit() {
	window.location = 'marksheet_revision_subject_group.php?<?=$param?>';
}

</script>
<form id="form1" name="form1" method="POST">
	<?=$htmlAry['navigation']?>
	<p class="spacer"></p>
	<br />
	
	<?=$htmlAry['generalImportStepTbl']?>
	
	<div class="table_board">
		<?=$htmlAry['reportInfoTbl']?>
		<br style="clear:both;" />
		<br style="clear:both;" />
		
		<div style="width:100%; text-align:center;">
			<?=$htmlAry['numOFSuccessDisplay']?>
		</div>
		<br style="clear:both;" />
		
		<div class="edit_bottom_v30">
			<p class="spacer"></p>
			<?=$htmlAry['GoToMarksheetBtn']?>
			<p class="spacer"></p>
		</div>
	</div>
	<input type="hidden" name="ClassLevelID" id="ClassLevelID" value="<?=$classlevelId?>"/>
	<input type="hidden" name="ReportID" id="ReportID" value="<?=$reportId?>"/>
	<input type="hidden" name="SubjectID" id="SubjectID" value="<?=$subjectId?>"/>
	<input type="hidden" name="type" id="type" value="<?=$type?>"/>
</form>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>