<?php
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
//include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008j.php");

intranet_auth();
intranet_opendb();

#################################################################################################

$lreportcard = new libreportcard2008j();
//$lreportcard = new libreportcard();

# Initialization
$result = array();

# Get Data - $_POST 
$ClassIDArr = (isset($ClassIDList) && !empty($ClassIDList)) ? $ClassIDList : array();
$SubjectIDArr = (isset($SubjectIDList) && !empty($SubjectIDList)) ? $SubjectIDList : array();


# Get Data Either Assessment Column(s) in Term Or Term Column(s) in Wholely Report	
// Loading Report Template Data
$basic_data = $lreportcard->returnReportTemplateBasicInfo($ReportID);			// Basic  Data
$column_data = $lreportcard->returnReportTemplateColumnData($ReportID); 		// Column Data
$ColumnSize = sizeof($column_data);

if(count($ClassIDArr) > 0 && count($SubjectIDArr) > 0){
	for($i=0 ; $i<count($ClassIDArr) ; $i++){
		# Get Student Info 
		$StudentArr = $lreportcard->GET_STUDENT_BY_CLASS($ClassIDArr[$i]);
		$StudentSize = sizeof($StudentArr);
		$StudentIDArr = array();
		
		if($StudentSize > 0){
			for($j=0 ; $j<$StudentSize ; $j++){
				$StudentIDArr[$j] = $StudentArr[$j]['UserID'];
			}
				
			# Main
			for($j=0 ; $j<count($SubjectIDArr) ; $j++){
				// check if it is a parent subject, if yes find info of its components subjects
				$CmpSubjectArr = array();
				$CmpSubjectArr = $lreportcard->GET_COMPONENT_SUBJECT($SubjectIDArr[$j], $ClassLevelID);
				$CmpSubjectIDArr = array();
				if(!empty($CmpSubjectArr)){
					for($p=0 ; $p<count($CmpSubjectArr) ; $p++){
						for($q=0; $q<$ColumnSize ; $q++) {
							$ReportColumnID = $column_data[$q]['ReportColumnID'];
							# Update - fill in NA in empty scores
							$result['fill_na_'.$i.'_'.$j.'_'.$p.'_'.$q] = $lreportcard->FILL_EMPTY_SCORE_WITH_NA($StudentIDArr, $CmpSubjectArr[$p]['SubjectID'], $ReportColumnID);
						}
						
						# Get Scheme Details & Subject Grading Details
						$SchemeType = '';
						$ScaleInput = '';
						$SubjectFormGradingArr = array();
						$SubjectFormGradingArr = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $CmpSubjectArr[$p]['SubjectID'],0,0,$ReportID);
						if(count($SubjectFormGradingArr) > 0){
							$SchemeID = $SubjectFormGradingArr['SchemeID'];
							$ScaleInput = $SubjectFormGradingArr['ScaleInput'];
							$SchemeMainInfo = $lreportcard->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
							$SchemeType = $SchemeMainInfo['SchemeType'];
						}
						
						#Update 
						if(($SchemeType == "H" && $ScaleInput == "G") || $SchemeType == "PF"){
							# Update - fill in NA in empty overall scores
							$result['fill_na_'.$i.'_'.$j.'_'.$p] = $lreportcard->FILL_EMPTY_SCORE_WITH_NA($StudentIDArr, $CmpSubjectArr[$p]['SubjectID'], $ReportID, 1);
						}
						
						$result['update_cmp_subj_submission_progress_'.$i.'_'.$j.'_'.$p] = $lreportcard->UPDATE_RELATED_SUBJECT_SUBMISSION_PROGRESS($ReportID, $ClassLevelID, $ClassIDArr[$i], $CmpSubjectArr[$p]['SubjectID'], 1, $SubjectIDArr[$j]);
					}
				}
				
				for($k=0 ; $k<$ColumnSize ; $k++){
					$ReportColumnID = $column_data[$k]['ReportColumnID'];
					
					# Update - fill in NA in empty scores
					$result['fill_na_'.$i.'_'.$j.'_'.$k] = $lreportcard->FILL_EMPTY_SCORE_WITH_NA($StudentIDArr, $SubjectIDArr[$j], $ReportColumnID);
				}
				
				# Get Scheme Details & Subject Grading Details
				$SchemeType = '';
				$ScaleInput = '';
				$SubjectFormGradingArr = array();
				$SubjectFormGradingArr = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectIDArr[$j],0,0,$ReportID);
				if(count($SubjectFormGradingArr) > 0){
					$SchemeID = $SubjectFormGradingArr['SchemeID'];
					$ScaleInput = $SubjectFormGradingArr['ScaleInput'];
					
					$SchemeMainInfo = $lreportcard->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
					$SchemeType = $SchemeMainInfo['SchemeType'];
				}
				
				#Update 
				if(($SchemeType == "H" && $ScaleInput == "G") || $SchemeType == "PF"){
					# Update - fill in NA in empty overall scores
					$result['fill_na_'.$i.'_'.$j] = $lreportcard->FILL_EMPTY_SCORE_WITH_NA($StudentIDArr, $SubjectIDArr[$j], $ReportID, 1);
				}
				
				$result['update_subj_submission_progress_'.$i.'_'.$j] = $lreportcard->UPDATE_RELATED_SUBJECT_SUBMISSION_PROGRESS($ReportID, $ClassLevelID, $ClassIDArr[$i], $SubjectIDArr[$j], 1);
			}
		}
	}
}

# Update the LastMarksheetInput Time if any changes in This Report Template marksheet
if(in_array(true, $result)){
	$DateName = "LastMarksheetInput";
	$lreportcard->UPDATE_REPORT_LAST_DATE($ReportID, $DateName);
}


if(!in_array(false, $result))
	$msg = 0;
else 
	$msg = 1;

#################################################################################################
intranet_closedb();


if($isProgress){
	$url = "../progress/submission.php?ClassLevelID=$ClassLevelID&ReportID=$ReportID";
}
else {
	$params = "ClassLevelID=$ClassLevelID&ReportID=$ReportID&SubjectID=$SubjectID";
	if($MarksheetRevision == 2){	// Back to Marksheet Revision of Subject having Component subjects
		$params .= "&ClassID=$ClassID";
		$url = "marksheet_revision2.php?$params";
	}
	else {	// Back to Marksheet Revision of Subject without Component subjects
		$url = "marksheet_revision.php?$params";
	}
}

header("Location: $url");

?>