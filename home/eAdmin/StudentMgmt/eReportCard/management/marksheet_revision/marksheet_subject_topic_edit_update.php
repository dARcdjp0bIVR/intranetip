<?php
// Editing: 
/*
 * Modification Log:
 * 20201103 (Bill)  [2020-0915-1830-00164]
 * - support term setting    ($eRCTemplateSetting['Settings']['SubjectTopics_TermSettings'])
 * 20160603 (Bill)
 * - fixed: cannot fill NA to empty marksheet score due to null value in marksheet score table
 * 20160427 (Bill)	[2016-0330-1524-42164]
 * - insert NA to Overall Score Table
 * - redirect to edit page with selected report column after update
 * - fixed: insert empty data to subject topics that belong to main subject / other subject components
 * 20151207 (Bill)
 * - redirect to edit page with selected subject groups after update
 * 20151201 (Bill)
 * - redirect to edit page after update scores
 * 20151124 (Bill)	[2015-1111-1214-16164]
 * - for main subject, also update subject component score
 * 20151030 (Bill)
 * - update required fields in RC_MARKSHEET_SCORE
 * 20150508 (Bill)
 * 	- Create file
 */

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008j.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_subjectTopic.php");

intranet_auth();
intranet_opendb();

$lreportcard = new libreportcard2008j();
$lreportcard_topic = new libreportcard_subjectTopic();

# Get Data
$StudentID = $targetStudentID;
$LastModifiedUserID = $_SESSION['UserID'];
$ReportColumnID = $ReportColumnID? $ReportColumnID : $SelectReportColumnID;

// [2015-1111-1214-16164] Handle Component Subject ID
$SubjectIDArr = array();
$SubjectIDArr[] = $SubjectID;
$fromSubject = $SubjectID;
if(is_array($CmpSubjectID) && count($CmpSubjectID)>0){
	$SubjectIDArr = array_merge($SubjectIDArr, $CmpSubjectID);
}

// loop subjects
for($subjCount=0; $subjCount<count($SubjectIDArr); $subjCount++){
	# Initialization
	$result = array();
	$MarksheetScoreArr = array();
	
	$SubjectID = $SubjectIDArr[$subjCount];
	$SubjectFormGradingArr = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID, 0, 0, $ReportID);
	$SchemeID = $SubjectFormGradingArr['SchemeID'];
	$ScaleInput = $SubjectFormGradingArr['ScaleInput'];
	
	$SchemeInfo = $lreportcard->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
	$SchemeType = $SchemeInfo['SchemeType'];
	
	$DateName = "LastMarksheetInput";
		
	# Get Complete Status of Common Subject or Parent Subject
	$ProgressArr = array();
	$ProgressArr = $lreportcard->GET_MARKSHEET_SUBMISSION_PROGRESS($SubjectID, $ReportID, $ClassID);
	$isSubjectComplete = (count($ProgressArr) > 0) ? $ProgressArr['IsCompleted'] : 0;
	
	# Get Report Template Details
	$basic_data = $lreportcard->returnReportTemplateBasicInfo($ReportID);			// Basic  Data
	$column_data = $lreportcard->returnReportTemplateColumnData($ReportID); 		// Column Data
	$ReportType = $basic_data['Semester'];
    $ReportTypeTW = $SemID == "F" ? "W" : "T";
    $ReportTermNum = $lreportcard->Get_Semester_Seq_Number($ReportType);

    // [2020-0915-1830-00164]
    $targetTermSeq = 0;
    if ($eRCTemplateSetting['Settings']['SubjectTopics_TermSettings'] && $ReportTypeTW == "T") {
        $targetTermSeq = ($ReportTermNum == 1 || $ReportTermNum == 2) ? 1 : 2;
    }
	
	# Get Subject Topic
    // [2020-0915-1830-00164]
	//$CurrentSubjectTopics = $lreportcard_topic->getSubjectTopic($ClassLevelID, $SubjectID);
    $CurrentSubjectTopics = $lreportcard_topic->getSubjectTopic($ClassLevelID, $SubjectID, '', $targetTermSeq);
	$CurrentSubjectTopics = Get_Array_By_Key((array)$CurrentSubjectTopics, "SubjectTopicID");
	
	# Check whether it is Component Subject or Not
	$isCmpSubject = $lreportcard->CHECK_SUBJECT_IS_COMPONENT_SUBJECT($SubjectID);
	
	# Mark Type : M-Mark, G-Grade, PF-PassFail, SC-SpecialCase
	# Get Marksheet Column Input Mark Data
	$count = 0;
	if(count($SubjectTopics) > 0){
		for($i=0 ; $i<count($SubjectTopics) ; $i++){
			if(!in_array($SubjectTopics[$i], $CurrentSubjectTopics)){
				continue;
			}
			
			$MarkNonNum = '';
			$MarkRaw = '-1';
			$RawData = ${"mark_".$SubjectTopics[$i]};
			
			//$IsEstimated = ($lreportcard->Is_Estimated_Score($RawData))? 1 : 0;
			$RawData = $lreportcard->Convert_Marksheet_UIScore_To_DBRawData($RawData);
			
			$isSpecialCase = ($SchemeType == "PF") ? $lreportcard->CHECK_MARKSHEET_SPECIAL_CASE_SET2($RawData) : $lreportcard->CHECK_MARKSHEET_SPECIAL_CASE_SET1($RawData);
			
			// Set the MarkType 
			if($SchemeType == "H"){
				if($ScaleInput == "G"){
					$MarkType = "G";
					$MarkNonNum = $RawData;
				}
				else {
					$MarkType = "M";
					if($isSpecialCase)
						$MarkRaw = ($RawData == "+") ? 0 : -1;
					else 
						$MarkRaw = ($RawData != "" ) ? $lreportcard->ROUND_MARK($RawData, "SubjectScore") : -1;
				}
			}
			else if($SchemeType == "PF"){
				$MarkType = "PF";
				$MarkNonNum = $RawData;
			}
			else {
				$MarkType = "M";
				$MarkRaw = $RawData;
			}
			
			$MarksheetScoreArr[$count]["MarksheetScoreID"] = (isset(${"MarksheetScoreID_".$SubjectTopics[$i]})) ? ${"MarksheetScoreID_".$SubjectTopics[$i]} : "";
			$MarksheetScoreArr[$count]['StudentID'] = $targetStudentID;
			$MarksheetScoreArr[$count]['SubjectID'] = $SubjectID;
			$MarksheetScoreArr[$count]['SubjectTopicID'] = $SubjectTopics[$i];
			$MarksheetScoreArr[$count]['ReportColumnID'] = $ReportColumnID;
			$MarksheetScoreArr[$count]['SchemeID'] = $SchemeID;
			$MarksheetScoreArr[$count]['MarkType'] = ($isSpecialCase) ? "SC" : $MarkType;
			$MarksheetScoreArr[$count]['MarkRaw'] = trim($MarkRaw);
			$MarksheetScoreArr[$count]['MarkNonNum'] = ($isSpecialCase) ? trim($RawData) : trim($MarkNonNum);
			$MarksheetScoreArr[$count]["LastModifiedUserID"] = $LastModifiedUserID;
			//$MarksheetScoreArr[$count]['IsEstimated'] = $IsEstimated;
			
			$count++;
		}
	}
	
	# Main
	# INSERT - Insert New Record to the MarkScore Table
	// [2016-0330-1524-42164] using $CurrentSubjectTopics instead of $SubjectTopics
	if(count($SubjectTopics) > 0 && count($CurrentSubjectTopics) > 0){
		$result['insert'] = $lreportcard_topic->INSERT_MARKSHEET_SUBJECT_TOPIC_SCORE($StudentID, $SubjectID, $CurrentSubjectTopics, $ReportColumnID, $LastModifiedUserID);
		
		// removed: prevent cannot fill empty score with NA below
		//$result['insert_ori'] = $lreportcard->INSERT_MARKSHEET_SCORE(array($StudentID), $SubjectID, $ReportColumnID, $LastModifiedUserID);
	}
	
	# UPDATE - Update Record of Marksheet Score
	if(count($SubjectTopics) > 0 && count($CurrentSubjectTopics) > 0 && count($MarksheetScoreArr) > 0){
		$result['update_marksheet'] = $lreportcard_topic->UPDATE_MARKSHEET_SUBJECT_TOPIC_SCORE($MarksheetScoreArr, 1);
		
		// Fill NA to related Report Column Score
		if($lreportcard->IS_MARKSHEET_SCORE_EMPTY(array($StudentID), $SubjectID, $ReportColumnID, $isOverallScore=0)=="all"){
			$lreportcard->FILL_EMPTY_SCORE_WITH_NA(array($StudentID), $SubjectID, $ReportColumnID, $isOverallScore=0, $ClassLevelID, $UpdateLastModified=1);
		}

		// [2016-0330-1524-42164] Fill NA to Overall Score
		if($lreportcard->IS_MARKSHEET_SCORE_EMPTY(array($StudentID), $SubjectID, $ReportID, $isOverallScore=1)=="all"){
			$lreportcard->FILL_EMPTY_SCORE_WITH_NA(array($StudentID), $SubjectID, $ReportID, $isOverallScore=1, $ClassLevelID, $UpdateLastModified=1);
		}
		
		// for subjects with parent subject
		if($isCmpSubject){
			
			// get target parent subjectid
			$targetParentSubjectID = "";
			if($isCmpSubject && isset($ParentSubjectID) && $ParentSubjectID != ""){
				$targetParentSubjectID = $ParentSubjectID;
			}
			else if($isCmpSubject && isset($fromParentSubjectID) && $fromParentSubjectID != ""){
				$targetParentSubjectID = $fromParentSubjectID;
			}
			
			// fill NA to Report Column & Overall Score of parent subject
			if($targetParentSubjectID!="")
			{
				if($lreportcard->IS_MARKSHEET_SCORE_EMPTY(array($StudentID), $targetParentSubjectID, $ReportColumnID, $isOverallScore=0)=="all"){
					$lreportcard->FILL_EMPTY_SCORE_WITH_NA(array($StudentID), $targetParentSubjectID, $ReportColumnID, $isOverallScore=0, $ClassLevelID, $UpdateLastModified=1);
				}
		
				if($lreportcard->IS_MARKSHEET_SCORE_EMPTY(array($StudentID), $targetParentSubjectID, $ReportID, $isOverallScore=1)=="all"){
					$lreportcard->FILL_EMPTY_SCORE_WITH_NA(array($StudentID), $targetParentSubjectID, $ReportID, $isOverallScore=1, $ClassLevelID, $UpdateLastModified=1);
				}
			}
		}
		
		# Update the Time of LastMarksheetInput
		if($result['update_marksheet']){
			$result['udpate_report_lastdate'] = $lreportcard->UPDATE_REPORT_LAST_DATE($ReportID, $DateName);
			
			if($isSubjectComplete){
				//$result['udpate_submission_progress'] = $lreportcard->UPDATE_MARKSHEET_SUBMISSION_PROGRESS($SubjectID, $ReportID, $ClassID, 0);
				if($isCmpSubject && isset($ParentSubjectID) && $ParentSubjectID != ""){
					$result['update_submission_progress'] = $lreportcard->UPDATE_RELATED_SUBJECT_SUBMISSION_PROGRESS($ReportID, $ClassLevelID, $ClassID, $SubjectID, 0, $ParentSubjectID, $SubjectGroupID);
				} 
				else if($isCmpSubject && isset($fromParentSubjectID) && $fromParentSubjectID != ""){
					$result['update_submission_progress'] = $lreportcard->UPDATE_RELATED_SUBJECT_SUBMISSION_PROGRESS($ReportID, $ClassLevelID, $ClassID, $SubjectID, 0, $fromParentSubjectID, $SubjectGroupID);
				}
				else {
					$result['update_submission_progress'] = $lreportcard->UPDATE_RELATED_SUBJECT_SUBMISSION_PROGRESS($ReportID, $ClassLevelID, $ClassID, $SubjectID, 0, '', $SubjectGroupID);
				}
			}
		}
	}
	if(in_array(false, $result))
		$InValidResult = true;
	else
		$InValidResult = $InValidResult==true? $InValidResult : false;
}

//if(!in_array(false, $result))
if($InValidResult)
	$msg = "update_failed";
else 
	$msg = "update";

intranet_closedb();

// redirect to edit page
$redirectTo = "marksheet_subject_topic_edit.php";
$params = "ClassLevelID=$ClassLevelID&ReportID=$ReportID&SelectReportColumnID=$SelectReportColumnID&SubjectID=$fromSubject&SubjectGroupID=$SubjectGroupID&targetStudentID=".($nextStudentID?$nextStudentID:$targetStudentID)."&targetSubjectGroupID=$targetSubjectGroupID";
header("Location: $redirectTo?$params&msg=$msg");

//$params = "ClassLevelID=$ClassLevelID&ReportID=$ReportID&isProgress=$isProgress&isProgressSG=$isProgressSG";
//if(isset($ParentSubjectID) && $ParentSubjectID != ""){
//	if ($isProgress)
//		$redirectTo = "../progress/submission.php";
//	else
//	{
//		$redirectTo = "marksheet_revision2_subject_group.php";
//	}
//	$params .= "&SubjectID=$ParentSubjectID";
//	$params .= "&ClassID=$ClassID";
//	$params .= "&SubjectGroupID=$SubjectGroupID";
//	header("Location: $redirectTo?$params&msg=$msg");
//} else {
//	if ($isProgress)
//		$redirectTo = "../progress/submission.php";
//	else
//	{
//		$redirectTo = "marksheet_revision_subject_group.php";
//	}
//	$params .= "&SubjectID=$fromSubject";
//	$params .= "&SubjectGroupID=$SubjectGroupID";
//	header("Location: $redirectTo?$params&msg=$msg");
//}

?>