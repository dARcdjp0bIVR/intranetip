<?php
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
//include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/libteaching.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");

intranet_auth();
intranet_opendb();

#################################################################################################

include_once($PATH_WRT_ROOT."includes/libreportcard2008j.php");
$lreportcard = new libreportcard2008j();
//$lreportcard = new libreportcard();

$lexport = new libexporttext();

#Initialization
$Content = '';


# Get Data
// Period (Term x, Whole Year, etc)
$PeriodArr = $lreportcard->GET_REPORT_TYPES($ClassLevelID, $ReportID);
$PeriodName = (count($PeriodArr) > 0) ? $PeriodArr['SemesterTitle'] : '';

# Class
$ClassArr = $lreportcard->GET_CLASSES_BY_FORM($ClassLevelID);
for($i=0 ; $i<count($ClassArr) ;$i++){
	if($ClassArr[$i][0] == $ClassID)
		$ClassName = $ClassArr[$i][1];
}

# SubjectName
$SubjectName = $lreportcard->GET_SUBJECT_NAME_LANG($SubjectID, "EN");

# Load Settings - Calculation
// Get the Calculation Method of Report Type - 1: Right-Down,  2: Down-Right
$SettingCategory = 'Calculation';
$categorySettings = $lreportcard->LOAD_SETTING($SettingCategory);
$TermCalculationType = $categorySettings['OrderTerm'];
$FullYearCalculationType = $categorySettings['OrderFullYear'];


# Get component subject(s) if any
$CmpSubjectArr = array();
$isCalculateByCmpSub = 0;
$isCmpSubject = $lreportcard->CHECK_SUBJECT_IS_COMPONENT_SUBJECT($SubjectID);
if(!$isCmpSubject){
	$CmpSubjectArr = $lreportcard->GET_COMPONENT_SUBJECT($SubjectID, $ClassLevelID, $ParLang='', $ParDisplayType='Desc', $ReportID, $ReturnAsso=0);
	$isParentSubject = (count($CmpSubjectArr) > 0)? true : false;
	
	$CmpSubjectIDArr = array();
	if(!empty($CmpSubjectArr)){	
		for($i=0 ; $i<count($CmpSubjectArr) ; $i++){
			$CmpSubjectIDArr[] = $CmpSubjectArr[$i]['SubjectID'];
			
			$CmpSubjectFormGradingArr = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $CmpSubjectArr[$i]['SubjectID'],0,0,$ReportID);
			if(count($CmpSubjectFormGradingArr) > 0 && $CmpSubjectFormGradingArr['ScaleInput'] == "M")
				$isCalculateByCmpSub = 1;
		}
	}
}


// Loading Report Template Data
$basic_data = $lreportcard->returnReportTemplateBasicInfo($ReportID);			// Basic  Data
$column_data = $lreportcard->returnReportTemplateColumnData($ReportID); 		// Column Data
$ColumnTitleAry = $lreportcard->returnReportColoumnTitle($ReportID);
$ColumnSize = sizeof($column_data);
$ReportType = $basic_data['Semester'];


# Get Existing Report Calculation Method
$CalculationType = ($ReportType == "F") ? $FullYearCalculationType : $TermCalculationType;

# Get Report Column Weight
$WeightAry = array();
if($CalculationType == 2){
	if(count($column_data) > 0){
    	for($i=0 ; $i<sizeof($column_data) ; $i++){
			$OtherCondition = "SubjectID IS NULL AND ReportColumnID = ".$column_data[$i]['ReportColumnID'];
			$weight_data = $lreportcard->returnReportTemplateSubjectWeightData($ReportID, $OtherCondition); 
			
			$WeightAry[$weight_data[0]['ReportColumnID']][$SubjectID] = $weight_data[0]['Weight'];
		}
	}
}
else {
	$weight_data = $lreportcard->returnReportTemplateSubjectWeightData($ReportID);  
	
	for($i=0 ; $i<sizeof($weight_data) ; $i++)
		$WeightAry[$weight_data[$i]['ReportColumnID']][$weight_data[$i]['SubjectID']] = $weight_data[$i]['Weight'];
}


// Subject Grading Scheme
$SubjectFormGradingArr = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID,0,0,$ReportID);


// Scheme Range Info
$SchemeGrade = array();
$GradeInfo = array();
if(count($SubjectFormGradingArr) > 0){
	$SchemeID = $SubjectFormGradingArr['SchemeID'];
	$ScaleInput = $SubjectFormGradingArr['ScaleInput'];
	$ScaleDisplay = $SubjectFormGradingArr['ScaleDisplay'];
	
	$SchemeInfo = $lreportcard->GET_GRADING_SCHEME_INFO($SchemeID);
	$SchemeMainInfo = $SchemeInfo[0];
	$SchemeMarkInfo = $SchemeInfo[1];
	$SchemeType = $SchemeMainInfo['SchemeType'];
}


// special column checking for parent subject
$isAllComponentZeroWeight = array();
if ($isParentSubject) {
	for($i=0 ; $i<$ColumnSize ; $i++) {
		$_reportColumnId = $column_data[$i]['ReportColumnID'];
		
		$_isAllComponentZeroWeight = true;
		for($j=0 ; $j<count($CmpSubjectArr) ; $j++) {
			$__cmpSubjectId = $CmpSubjectArr[$i]['SubjectID'];
			
			$__weightAry = $lreportcard->returnReportTemplateSubjectWeightData($ReportID, $other_condition='', $__cmpSubjectId, $_reportColumnId);
			$__subjectWeight = $__weightAry[0]['Weight'];
			
			if ($__subjectWeight > 0) {
				$_isAllComponentZeroWeight = false;
				break;
			}
		}
		
		$isAllComponentZeroWeight[$_reportColumnId] = $_isAllComponentZeroWeight;
	}
}



// Student Info Array
//$SubjectGroupID = '';
$StudentArr = array();
if ($SubjectGroupID != '') // get all student in subject group
{
	$StudentArr = $lreportcard->GET_STUDENT_BY_SUBJECT_GROUP($SubjectGroupID, $ClassLevelID, '', 1);
	$showClassName = true;
	
	$obj_SubjectGroup = new subject_term_class($SubjectGroupID);
	$SubjectGroupName = $obj_SubjectGroup->Get_Class_Title();
	
	$Class_SG_Title = $Lang['SysMgr']['SubjectClassMapping']['SubjectGroup'];
	$Class_SG_Name = $SubjectGroupName;
}
else if(trim($ClassID)!='') // consolidate get all student in class
{
	$StudentArr = $lreportcard->GET_STUDENT_BY_CLASS($ClassID);
	$Class_SG_Title = "ClassName";
	$Class_SG_Name = $ClassName;
	
	$showClassName = true;
}
else if($ReportType == "F") // consolidate get all student in classlevel
{
	$StudentArr = $lreportcard->GET_STUDENT_BY_CLASSLEVEL($ReportID, $ClassLevelID);
	
	$IsClassLevelStudentList=1;
}
else // get student in all subject group in classlevel
{
	if (!$special_feature['eRC_class_teacher_as_subject_teacher']) {
		$lteaching = new libteaching();
		$TeacherClassAry = $lteaching->returnTeacherClassWithLevel($_SESSION['UserID'], $lreportcard->schoolYearID);
		$TeacherClass = $TeacherClassAry[0]['ClassName'];
		$TeacherClassLevelID = $TeacherClassAry[0]['ClassLevelID'];
		$TeacherClassID = $TeacherClassAry[0]['ClassID'];
	}
	
	# Get all Subject Groups of the Subject
	$TeacherID = '';
	if ($ck_ReportCard_UserType=="TEACHER" && (empty($TeacherClass) || $TeacherClassLevelID!=$ClassLevelID)) {
		# Subject Teacher => View teaching subject(s) only
		$TeacherID = $_SESSION['UserID'];
	}
	
	$MainSubjectID = $isCmpSubject?$lreportcard->GET_PARENT_SUBJECT_ID($SubjectID):$SubjectID;
	
	$obj_Subject = new Subject($MainSubjectID);
	$AllSubjectGroupArr = $obj_Subject->Get_Subject_Group_List($ReportType, $ClassLevelID, '', $TeacherID, $returnAsso=0, $TeacherClassID, '', $InYearClassOnly=true);
	$SubjectGroupIDArr = Get_Array_By_Key($AllSubjectGroupArr, 'SubjectGroupID');
	
	foreach((array)$SubjectGroupIDArr as $thisSubjectGroupID) {
		$StudentArr = array_merge($StudentArr,(array)$lreportcard->GET_STUDENT_BY_SUBJECT_GROUP($thisSubjectGroupID, $ClassLevelID, '', 1));
	}
}

//debug_pr($StudentArr);
$StudentSize = sizeof($StudentArr);

$StudentIDArr = array();
if($StudentSize > 0){
	for($i=0 ; $i<$StudentSize ; $i++)
		$StudentIDArr[$i] = $StudentArr[$i]['UserID'];
}

$MarksheetScoreInfoArr = array();
$ColumnHasTemplateAry = array();
if($ColumnSize > 0){
    for($i=0 ; $i<$ColumnSize ; $i++){
		/* 
		* For Whole Year Report use only
		* Check for the Mark Column is allowed to fill in mark when any term does not have related report template 
		* by SemesterNum (ReportType) && ClassLevelID
		* An array is created as a control variable $ColumnHasTemplateAry
		*/
		if($ReportType == "F")
			$ColumnHasTemplateAry[$column_data[$i]['ReportColumnID']] = $lreportcard->CHECK_REPORT_TEMPLATE_FROM_COLUMN($column_data[$i]['SemesterNum'], $ClassLevelID);
		else 
			$ColumnHasTemplateAry[$column_data[$i]['ReportColumnID']] = 1;
			
		if($ColumnHasTemplateAry[$column_data[$i]['ReportColumnID']] && $ReportType == "F"){
			$TermReportID = $ColumnHasTemplateAry[$column_data[$i]['ReportColumnID']];
			$WeightedMark[$column_data[$i]['ReportColumnID']] = $lreportcard->GET_STUDENT_TERM_SUBJECT_MARK($StudentIDArr, $TermReportID, $SubjectID);
		}
	}
}


# Header 
$Content = "WebSAMSRegNumber,ClassName,ClassNumber,StudentName";
$exportColumn[0][] = $eReportCard['ImportHeader']['ClassName']['En'];
$exportColumn[0][] = $eReportCard['ImportHeader']['ClassNumber']['En'];
$exportColumn[0][] = $eReportCard['ImportHeader']['UserLogin']['En'];
$exportColumn[0][] = $eReportCard['ImportHeader']['WebSamsRegNo']['En'];
$exportColumn[0][] = $eReportCard['ImportHeader']['StudentName']['En'];
$exportColumn[1][] = $eReportCard['ImportHeader']['ClassName']['Ch'];
$exportColumn[1][] = $eReportCard['ImportHeader']['ClassNumber']['Ch'];
$exportColumn[1][] = $eReportCard['ImportHeader']['UserLogin']['Ch'];
$exportColumn[1][] = $eReportCard['ImportHeader']['WebSamsRegNo']['Ch'];
$exportColumn[1][] = $eReportCard['ImportHeader']['StudentName']['Ch'];

for($i=0; $i<$ColumnSize ; $i++){
	
	list($ReportColumnID, $ColumnTitle, $DefaultWeight, $SemesterNum) = $column_data[$i];
	$BlockMarksheet = $column_data[$i]['BlockMarksheet'];
	
	$ColumnTitle = ($ColumnTitle != "" && $ColumnTitle != null) ? $ColumnTitle : $ColumnTitleAry[$ReportColumnID];
	$Content .= ",".$ColumnTitle;
	if($WeightAry[$ReportColumnID][$SubjectID] != 0 && $WeightAry[$ReportColumnID][$SubjectID] != null && $WeightAry[$ReportColumnID][$SubjectID] != "" ){
		# Checking for the Readable Columns with (N/A) marked when the case is 
		# It is Whole Year Report AND its terms column has defined Term Report Template OR
		# It has component subjects and the parent subject mark is calculated by its component subjects
		if(($ColumnHasTemplateAry[$ReportColumnID] && $ReportType == "F") 
			|| (!empty($CmpSubjectArr) && $isCalculateByCmpSub && $ScaleInput == "M")
			|| $lreportcard->Is_Marksheet_Blocked($BlockMarksheet)){
				
				if ($isAllComponentZeroWeight[$ReportColumnID]) {
					$exportColumn[0][] = $ColumnTitle;
					$exportColumn[1][] = "(".$ColumnTitle.")";
				}
				else {
					$Content .= "(N/A)";
					$exportColumn[0][] = $ColumnTitle."(N/A)";
					$exportColumn[1][] = "(".$ColumnTitle."(N/A)".")";
				}
		}
		else 
		{
			$exportColumn[0][] = $ColumnTitle;
			$exportColumn[1][] = "(".$ColumnTitle.")";
		}
	}
	else {
		$Content .= "(N/A)";
		$exportColumn[0][] = $ColumnTitle."(N/A)";
		$exportColumn[1][] = "(".$ColumnTitle."(N/A)".")";
	}
}

# Add a Overall Term Subject Mark OR Overall Subject Mark Header when the case is 
# The ScaleInput is Grade(G) OR PassFail(PF)
if(($SchemeType == "H" && $ScaleInput == "G") || $SchemeType == "PF"){
	if($ReportType != "F"){
		$Content .= ",".$eReportCard['TermSubjectMark'];
		$exportColumn[0][] = $eReportCard['TermSubjectMarkEn'];
		$exportColumn[1][] = "(".$eReportCard['TermSubjectMarkCh'].")";
	}
	else {
		$Content .= ",".$eReportCard['OverallSubjectMark'];
		$exportColumn[0][] = $eReportCard['OverallSubjectMarkEn'];
		$exportColumn[1][] = "(".$eReportCard['OverallSubjectMarkCh'].")";
	}
}

$Content .= "\n";


# content
$Value = '';
//debug_pr($StudentArr);
for($j=0; $j<$StudentSize; $j++)
{
	if($IsClassLevelStudentList)
	{
		$thisStudentInfo = $StudentArr[$j];
		$ClassName = $thisStudentInfo["ClassTitleEn"];
		$StudentID = $thisStudentInfo["UserID"];
		$WebSAMSRegNo = $thisStudentInfo["WebSAMSRegNo"];
		$StudentNo = $thisStudentInfo["ClassNumber"];
		$StudentName = $thisStudentInfo["StudentNameEn"]." (".$thisStudentInfo["StudentNameCh"].")";
		$UserLogin = $thisStudentInfo["UserLogin"];
	}
	else
		list($StudentID, $WebSAMSRegNo, $StudentNo, $StudentName, $ClassName, $UserLogin) = $StudentArr[$j];
	
	$Content .= trim($WebSAMSRegNo).",".trim($ClassName).",".trim($StudentNo).",".trim($StudentName);
    $row[] = trim($ClassName);
	$row[] = trim($StudentNo);
	$row[] = trim($UserLogin);
	$row[] = trim($WebSAMSRegNo);
	$row[] = trim($StudentName);

	if(count($column_data) > 0){
	    for($i=0 ; $i<sizeof($column_data) ; $i++){
		    list($ReportColumnID, $ColumnTitle, $DefaultWeight, $SemesterNum) = $column_data[$i];
		    
    		$Content .= ",".$Value;
    		$row[] = $Value;
	    }
	    
	    if(($SchemeType == "H" && $ScaleInput == "G") || $SchemeType == "PF"){
		    $Content .= ",".$Value;
		    $row[] = $Value;
	    }
	    
	    $Content .= "\n";
		$rows[] = $row;
		unset($row);
    }
}

#################################################################################################

intranet_closedb();

// Output the file to user browser
$set1 = array("&nbsp;", " ");
$set2 = array("-", ",");
//$filename = str_replace($set1, "", $Prefix."Marksheet_".trim(str_replace($set2, "_", $PeriodName))."_".trim(str_replace($set2, "_", $SubjectName))."_".trim($ClassName).".csv");

//$filename = str_replace("&nbsp;", " ", "Marksheet_Template_".trim($SubjectName)."_".trim($ClassName).".csv");
$filename = $Prefix."Marksheet_".$PeriodName."_".$SubjectName;
if($Class_SG_Name)
	$filename.=  "_".$Class_SG_Name;
$filename = str_replace($set2, "_", $filename);
$filename = str_replace($set1, "_", $filename);
//$filename = $lreportcard->Get_File_Name_Encoding($filename);
$filename = $filename.".csv";

$export_content = $lexport->GET_EXPORT_TXT_WITH_REFERENCE($rows, $exportColumn, "", "\r\n", "", 0, "11");
$lexport->EXPORT_FILE($filename, $export_content);

//output2browser($Content, $filename);

?>
