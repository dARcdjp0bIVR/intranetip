<?php
/*
 * Change Log: 
 * 	Date:	2017-01-05 Villa add hidden input $SubReportColumnID/ $currentLevel for submark sheet layer
 */

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
//include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if ($plugin['ReportCard2008'])
{
	/* Temp Library*/
	include_once($PATH_WRT_ROOT."includes/libreportcard2008j.php");
	$lreportcard = new libreportcard2008j();
	//$lreportcard = new libreportcard();
	
	$CurrentPage = "Sub-Marksheet Submission";
	$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
	$MODULE_OBJ['title'] = $eReportCard['SubMarksheet'];
	
    if ($lreportcard->hasAccessRight())
    {
        $linterface = new interface_html("popup.html");
        
		############################################################################################################
		
		# Get MarkSheet Info
		// Tags of MarkSheet(0: Raw Mark, 1: Weighted Mark, 2: Converted Grade
		$TagsType = (isset($TagsType) && $TagsType == "") ? $TagsType : 0;
		
		$ReportSetting = $lreportcard->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID = $ReportSetting['ClassLevelID'];
		
		// Period (Term x, Whole Year, etc)
		$PeriodArr = $lreportcard->GET_REPORT_TYPES($ClassLevelID, $ReportID);
		$PeriodName = (count($PeriodArr) > 0) ? $PeriodArr['SemesterTitle'] : '';
				
		// Class
		$ClassArr = $lreportcard->GET_CLASSES_BY_FORM($ClassLevelID);
		for($i=0 ; $i<count($ClassArr) ;$i++){
			if($ClassArr[$i][0] == $ClassID)
				$ClassName = $ClassArr[$i][1];
		}
		// Subject
		$SubjectName = $lreportcard->GET_SUBJECT_NAME($SubjectID);
		
		// Student Info Array
		if ($SubjectGroupID != '')
		{
			include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
			$obj_SubjectGroup = new subject_term_class($SubjectGroupID);
			$SubjectGroupName = $obj_SubjectGroup->Get_Class_Title();
			$TitleDisplay = "<td>".$Lang['SysMgr']['SubjectClassMapping']['SubjectGroup']." : <strong>".$SubjectGroupName."</strong></td>";
		}
		else
		{
			$TitleDisplay = "<td>".$eReportCard['Class']." : <strong>".$ClassName."</strong></td>";
		}
		
		// Report column info
		$ReportColumnInfo = $lreportcard->returnReportTemplateColumnData($ReportID);
		for ($i=0; $i<sizeof($ReportColumnInfo); $i++)
		{
			if ($ReportColumnInfo[$i]['ReportColumnID']==$ReportColumnID)
			{
				$ReportColumnName = $ReportColumnInfo[$i]['ColumnTitle'];
				$ReportColumnWeight = $ReportColumnInfo[$i]['DefaultWeight']*100;
			}
		}
		
		// isSubmarkSheet
		if($SubReportColumnID != ''){
			$ReportColumnInfo = $lreportcard->GET_ALL_SUB_MARKSHEET_COLUMN($ReportColumnID, $SubjectID, 'ALL');
// 			$ReportColumnInfo = $lreportcard->returnReportTemplateColumnData($ReportID);
			for ($i=0; $i<sizeof($ReportColumnInfo); $i++){
				if ($ReportColumnInfo[$i]['ColumnID']==$SubReportColumnID){
					$ReportColumnName = $ReportColumnInfo[$i]['ColumnTitle'];
					$ReportColumnWeight = $ReportColumnInfo[$i]['Ratio']*100;
				}
			}
		}
		
		//Initialization of Grading Scheme
		$SchemeID = '';
		$SchemeInfo = array();
		$SchemeMainInfo = array();
		$SchemeMarkInfo = array();
		$SubjectFormGradingArr = array();
		
		$SubjectFormGradingArr = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID,0,0,$ReportID);
		
		if(count($SubjectFormGradingArr) > 0){
			$SchemeID = $SubjectFormGradingArr['SchemeID'];
			$ScaleInput = $SubjectFormGradingArr['ScaleInput'];
			$ScaleDisplay = $SubjectFormGradingArr['ScaleDisplay'];
			
			$SchemeInfo = $lreportcard->GET_GRADING_SCHEME_INFO($SchemeID);
			$SchemeMainInfo = $SchemeInfo[0];
			$SchemeMarkInfo = $SchemeInfo[1];
			$SchemeType = $SchemeMainInfo['SchemeType'];
		}
		
		// Loading Report Template Data
		$basic_data = $lreportcard->returnReportTemplateBasicInfo($ReportID);			// Basic  Data
		$column_data = $lreportcard->returnReportTemplateColumnData($ReportID); 		// Column Data
		$ReportType = $basic_data['Semester'];
		
		/* 
		* For Whole Year Report use only
		* Check for the Mark Column is allowed to fill in mark when any term does not have related report template 
		* by SemesterNum (ReportType) && ClassLevelID
		* An array is initialized as a control variable [$ColumnHasTemplateAry]
		* A variable is initialized as a control variable [$hasDirectInputMark], must be used with [$ReportType == "F"]
		* to control the Import function to be used or not
		*/
		$ColumnHasTemplateAry = array();
		$hasDirectInputMark = 0;
		$ImportReportColumnMarkReminder = 0;
		if(count($column_data) > 0){	
			for($i=0 ; $i<sizeof($column_data) ; $i++){
				if($ReportType == "F"){
					$ColumnHasTemplateAry[$column_data[$i]['ReportColumnID']] = $lreportcard->CHECK_REPORT_TEMPLATE_FROM_COLUMN($column_data[$i]['SemesterNum'], $ClassLevelID);
					if(!$ColumnHasTemplateAry[$column_data[$i]['ReportColumnID']])
						$hasDirectInputMark = 1;
				}
				else {
					$ColumnHasTemplateAry[$column_data[$i]['ReportColumnID']] = 1;
				}
					
				//if($ColumnHasTemplateAry[$column_data[$i]['ReportColumnID']] && $ReportType == "F" && $ScaleInput == "M"){
				if($ColumnHasTemplateAry[$column_data[$i]['ReportColumnID']] && $ReportType == "F"){
					$ImportReportColumnMarkReminder = 1;
				}
			}
		}	
		
		# Button
// 		$param = "ClassLevelID=$ClassLevelID&ReportID=$ReportID&SubjectID=$SubjectID&ClassID=$ClassID&SubjectID=$SubjectID&ReportColumnID=$ReportColumnID&jsReportColumn=$jsReportColumn&SubjectGroupID=$SubjectGroupID";
		$param = "SubjectID=$SubjectID&ClassID=$ClassID&SchoolCode=$SchoolCode&ReportColumnID=$ReportColumnID&ReportID=$ReportID&jsReportColumn=$jsReportColumn&SubjectGroupID=$SubjectGroupID&SubReportColumnID=$SubReportColumnID&currentLevel=$currentLevel";
		
		$display_button = '';
		$display_button .= '<input name="Submit" type="button" class="formbutton" onMouseOver="this.className=\'formbuttonon\'" onMouseOut="this.className=\'formbutton\'" value="'.$button_submit.'" onclick="jSUBMIT_FORM()">';
        $display_button .= '<input name="Reset" type="reset" class="formbutton" onMouseOver="this.className=\'formbuttonon\'" onMouseOut="this.className=\'formbutton\'" value="'.$button_reset.'">';
        $display_button .= '<input name="Cancel" type="button" class="formbutton" onMouseOver="this.className=\'formbuttonon\'" onMouseOut="this.className=\'formbutton\'" value="'.$button_cancel.'" onClick="self.location.href=\'sub_marksheet.php?'.$param.'\'">';
        
		############################################################################################################
        
		# tag information
		$TAGS_OBJ[] = array($eReportCard['Management_MarksheetRevision'], "", 0);
		
		# page navigation (leave the array empty if no need)
		$PAGE_NAVIGATION[] = array($eReportCard['ImportMarks'], "");
		
		$linterface->LAYOUT_START();
?>

<script language="javascript">
function jCHECK_FORM(){
	return true;
}

function jSUBMIT_FORM(){
	var obj = document.FormMain;
	
	if(!jCHECK_FORM(obj))
		return;
	
	obj.submit();
}

</script>

<br/>
<form name="FormMain" method="post" action="sub_marksheet_import_update.php" enctype="multipart/form-data">
<table width="98%" border="0" cellpadding="4" cellspacing="0">
  <tr>
    <td align="center">
	  <table width="100%" border="0" cellspacing="0" cellpadding="2">
	    <tr>
	      <td colspan="2"><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	  	</tr>
	  	<tr><td align="right" colspan="2"><?= $linterface->GET_SYS_MSG($Result)?></td></tr>
	  	<tr>
	      <td width="30px">&nbsp;</td>
	      <td>
	      	<table border="0" cellpadding="5" cellspacing="0" class="tabletext">
	          <tr>
	            <td><?=$eReportCard['Period']?> : <strong><?=$PeriodName?></strong></td>
	          	<?= $TitleDisplay ?>
	          	<td><?=$eReportCard['Subject']?> : <strong><?=$SubjectName?></strong></td>
	          	<td><?=$eReportCard['ColumnTitle']?> : <strong><?=$ReportColumnName?></strong></td>
	          	<td><?=$eReportCard['AssessmentRatio']?> : <strong><?=$ReportColumnWeight?>%</strong></td>
	          </tr>
	      	</table>
	      	<?php if($ImportReportColumnMarkReminder){ ?>
	      	<br/>
	      	<table align="center" width="80%" border="0" cellpadding="3" cellspacing="0" class="Warningtable">
              <tr>
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="33%" valign="top">
                        <table width="100%" border="0" cellpadding="3" cellspacing="0">
                          <tr>
                            <td align="center" class="Warningtitletext"><br><?=$eReportCard['CSVMarksheetTemplateReminder']?><br><br></td>
                          </tr>
                        </table>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
	      	<?php } ?>
	      	<br/>
	      	<table width="100%" border="0" cellspacing="0" cellpadding="5" class="tabletext">
	      	  <tr>
	      	    <td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><?=$eReportCard['File']?></td>
	      	    <td><input type="file" name="userfile" size=25><br /><?php//$linterface->GET_IMPORT_CODING_CHKBOX() 
	      	    ?></td>
	      	  </tr>
	      	  <tr>
				<td colspan="2">
				<a class="tablelink" href="javascript:self.location.href='sub_marksheet_export.php?<?=$param.'&forExport=1'?>'"><img src="<?=$PATH_WRT_ROOT?>/images/<?=$LAYOUT_SKIN?>/icon_files/xls.gif" border="0" align="absmiddle" hspace="3"> <?=$eReportCard['DownloadCSVFile']?></a>
				</td>
			  </tr>
	      	</table>
	      </td>
	    </tr>
	  </table>
	  <br/>
	  <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td height="1" class="dotline"><img src="<?=$image_path.'/'.$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
        </tr>
      	<tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="2">
              <tr>
                <td align="center" valign="bottom"><?=$display_button?></td>
              </tr>
        	</table>
          </td>
      	</tr>
      </table>
	</td>
  </tr>
</table>

<input type="hidden" name="ClassLevelID" id="ClassLevelID" value="<?=$ClassLevelID?>"/>
<input type="hidden" name="ReportID" id="ReportID" value="<?=$ReportID?>"/>
<input type="hidden" name="SubjectID" id="SubjectID" value="<?=$SubjectID?>"/>
<input type="hidden" name="ClassID" id="ClassID" value="<?=$ClassID?>"/>
<input type="hidden" name="SubjectGroupID" id="SubjectGroupID" value="<?=$SubjectGroupID?>"/>

<input type="hidden" name="ReportColumnID" id="ReportColumnID" value="<?=$ReportColumnID?>"/>
<input type="hidden" name="jsReportColumn" value="<?=$jsReportColumn?>" />
<input type="hidden" name="SubReportColumnID" value="<?=$SubReportColumnID?>" />
<input type="hidden" name="currentLevel" value="<?=$currentLevel?>" />

<?php
if(isset($ParentSubjectID) && $ParentSubjectID != "")
	echo '<input type="hidden" name="ParentSubjectID" id="ParentSubjectID" value="'.$ParentSubjectID.'"/>'."\n";
?>

</form>

<?
        $linterface->LAYOUT_STOP();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>