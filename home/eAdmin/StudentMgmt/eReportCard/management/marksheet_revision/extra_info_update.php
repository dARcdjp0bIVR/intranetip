<?php
// Using: 

/********************************************************
 * Modification log
 * 
 *  20190523 Bill     [2019-0522-0908-45206]
 *      - Prevent SQL Injection + Cross-site Scripting
 *      - Change Input Fields 'extraInfoID' to 'extraInfoIDArr' to prevent IntegerSafe()
 *      
 * 20170502 Bill	[2017-0412-1431-44256]
 * 		- store modified user
 * 
********************************************************/

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

$PageRight = "TEACHER";

if (!isset($_POST, $SubjectID) && (!isset($extraInfo) || !isset($extraInfoIDArr))) {
	header("Location:marksheet_revision.php");
}

if (!$plugin['ReportCard2008']) {
	intranet_closedb();
	header("Location:/index.php");
}

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
$lreportcard = new libreportcard();

if (!$lreportcard->hasAccessRight()) {
	intranet_closedb();
	header("Location:/index.php");
}

### Handle SQL Injection + XSS [START]
$ClassLevelID = IntegerSafe($ClassLevelID);
$ReportID = IntegerSafe($ReportID);
$SubjectID = IntegerSafe($SubjectID);
$ClassID = IntegerSafe($ClassID);
$SubjectGroupID = IntegerSafe($SubjectGroupID);
### Handle SQL Injection + XSS [END]

if ($type=="AdjustPosition")
{
	$table = $lreportcard->DBName.".RC_REPORT_RESULT_SCORE_ARCHIVE";
	$success = true;
	if (sizeof($extraInfoIDArr) > 0)
	{
		foreach ($extraInfoIDArr as $key => $value)
		{
			$value = strip_tags(trim($value));
			if ($value == 0 || $value == "")
			{
				$value = "NULL";
			}
			else
			{
			    $value = IntegerSafe($value);
			}
			$key = IntegerSafe($key);
			
			$sql  = "UPDATE $table SET ";
			$sql .= "OrderMeritClass = $value, ModifiedBy = '".$lreportcard->uid."', DateModified = NOW() ";
			$sql .= "WHERE MarksheetConsolidatedID = '$key'";
			$success = $lreportcard->db_db_query($sql);
		}
	}
	
	if (sizeof($extraInfo) > 0)
	{
		$field = "StudentID, SubjectID, ReportID, ReportColumnID, OrderMeritClass, DateInput, DateModified, InputBy, ModifiedBy";
		$sql = "INSERT INTO $table ($field) VALUES ";
		foreach ($extraInfo as $key => $value)
		{
			$value = strip_tags(trim($value));
			if ($value == 0 || $value == "")
			{
				$value = "NULL";
			}
			else
			{
			    $value = IntegerSafe($value);
			}
			$key = IntegerSafe($key);
			
			$entries[] = "('$key', '$SubjectID', '$ReportID', '0', $value, NOW(), NOW(), '".$lreportcard->uid."', '".$lreportcard->uid."')";
		}
		$entries = implode(",", $entries);
		$sql .= $entries;
		$success = $lreportcard->db_db_query($sql);
	}
}
else	// extra info
{
	$table = $lreportcard->DBName.".RC_EXTRA_SUBJECT_INFO";
	$success = true;
	if (sizeof($extraInfoIDArr) > 0)
	{
		foreach ($extraInfoIDArr as $key => $value)
		{
		    $value = strip_tags(trim($value));
		    $key = IntegerSafe($key);
		    
			$sql  = "UPDATE $table SET ";
			$sql .= "Info = '$value', TeacherID = '".$lreportcard->uid."', DateModified = NOW() ";
			$sql .= "WHERE ExtraInfoID = '$key'";
			$success = $lreportcard->db_db_query($sql);
		}
	}
	
	if (sizeof($extraInfo) > 0)
	{
		$field = "StudentID, SubjectID, ReportID, Info, TeacherID, DateInput, DateModified";
		$sql = "INSERT INTO $table ($field) VALUES ";
		foreach ($extraInfo as $key => $value)
		{
		    $value = strip_tags(trim($value));
		    $key = IntegerSafe($key);
		    
			$entries[] = "('$key', '$SubjectID', '$ReportID', '$value', '".$lreportcard->uid."', NOW(), NOW())";
		}
		$entries = implode(",", $entries);
		$sql .= $entries;
		$success = $lreportcard->db_db_query($sql);
	}
}

$target_location_path = "marksheet_revision.php";
$parmeters = "ClassID=$ClassID&SubjectID=$SubjectID&ReportID=$ReportID&ClassLevelID=$ClassLevelID&type=$type";

# [2015-0924-1821-07164] for Component Subject when Manual Input Grade
if ($eRCTemplateSetting['Marksheet']['ManualInputGrade'] && $lreportcard->CHECK_SUBJECT_IS_COMPONENT_SUBJECT($SubjectID))
{
	$SubjectID = $lreportcard->GET_PARENT_SUBJECT_ID($SubjectID);
	$target_location_path = "marksheet_revision2_subject_group.php";
	$parmeters = "ClassID=&ClassLevelID=$ClassLevelID&ReportID=$ReportID&SubjectID=$SubjectID&SubjectGroupID=$SubjectGroupID";
}

intranet_closedb();

$Result = ($success) ? "update" : "update_failed";
header("Location:$target_location_path?$parmeters&msg=$Result");
?>