<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");


intranet_auth();
intranet_opendb();

if ($plugin['ReportCard2008']) {
	include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
	if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
	{
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
		$lreportcard = new libreportcardcustom();
	} else {
		$lreportcard = new libreportcard();
	}
	
	if ($lreportcard->hasAccessRight()) {
		if (!isset($_GET) || !isset($SubjectID, $ClassLevelID, $ReportID, $ClassID)) {
			header("Location: index.php");
			exit();
		}
		
		if ($eRCTemplateSetting['Marksheet']['ManualInputGrade']) {
        	$column_data = $lreportcard->returnReportTemplateColumnData($ReportID);
			$dataTitle = $column_data[0]['ColumnTitle'].' '.$eReportCard['Grade'];
		}
		else {
			$dataTitle = $eReportCard['ExtraInfoLabel'];
		}
		
		// Class
		$ClassArr = $lreportcard->GET_CLASSES_BY_FORM($ClassLevelID);
		for($i=0 ; $i<count($ClassArr) ;$i++) {
			if($ClassArr[$i][0] == $ClassID)
				$ClassName = $ClassArr[$i][1];
		}
		
		$table = $lreportcard->DBName.".RC_EXTRA_SUBJECT_INFO";
		$NameField = getNameFieldByLang2("");
		$ClassNumField = getClassNumberField("");
		
		$SubjectCond = "(SubjectID = '$SubjectID')";
		
		# Student Info Array
		if ($SubjectGroupID != '')
		{
			$StudentInfo = $lreportcard->GET_STUDENT_BY_SUBJECT_GROUP($SubjectGroupID, $ClassLevelID, '', 1);
			
			$obj_SubjectGroup = new subject_term_class($SubjectGroupID);
			$SubjectGroupName = $obj_SubjectGroup->Get_Class_Title();
			
			// Need the same format for export
			$showClassName = false;
			$Class_SG_Title = "ClassName";
			$Class_SG_Name = $ClassName;
		}
		else
		{
			$TaughtStudentArr = $lreportcard->Get_Student_List_Taught_In_This_Consolidated_Report($ReportID, $_SESSION['UserID'], $ClassID);
			$TaughtStudentList = '';
			if (count($TaughtStudentArr > 0))
				$TaughtStudentList = implode(',', $TaughtStudentArr);
				
			$StudentInfo = $lreportcard->GET_STUDENT_BY_CLASS($ClassID, $TaughtStudentList, 1);
			//$StudentArr = $lreportcard->GET_STUDENT_BY_CLASS($ClassID, '', 1);
			$showClassName = false;
			
			$Class_SG_Title = "ClassName";
			$Class_SG_Name = $ClassName;
		}
		$StudentSize = sizeof($StudentInfo);
		
		for($i=0; $i<$StudentSize; $i++)
			$StudentIDArray[] = $StudentInfo[$i][0];
		$StudentIDList = implode(",", $StudentIDArray);
		
		if (sizeof($StudentInfo) == 0) {
			header("Location: index.php");
			exit();
		}
		
		if (isset($isTemplate) && $isTemplate = "1") {
			if ($type=="AdjustPosition")
			{
				$filename = "position_template_$ClassName.csv";
			}
			else
			{
				$filename = "extra_info_template_$ClassName.csv";
			}
		} else {
			# only display the Comment if it match SubjectID & ReportID
			if ($type=="AdjustPosition")
			{
				$filename = "position_export_$ClassName.csv";
			}
			else
			{
				$filename = "extra_info_export_$ClassName.csv";
			}
		}
		
		if ($type=="AdjustPosition")
		{
			$PositionArr = $lreportcard->GET_MANUAL_ADJUSTED_POSITION($StudentIDArray, $SubjectID, $ReportID);
			for($i=0; $i<sizeof($StudentIDArray); $i++) {
				$thisStudentID = $StudentIDArray[$i];
				$thisPosition = $PositionArr[$thisStudentID]["Info"];
				$ExtraInfoAsso[$thisStudentID] = $thisPosition;
			}
		}
		else
		{
			# Get Extra Info and create an associated array
			$sql = "SELECT StudentID, Info FROM $table WHERE $SubjectCond AND ReportID='$ReportID' AND StudentID IN ($StudentIDList)";
			$ExtraInfo = $lreportcard->returnArray($sql,2);
			for($i=0; $i<sizeof($ExtraInfo); $i++) {
				$ExtraInfoAsso[$ExtraInfo[$i]["StudentID"]] = $ExtraInfo[$i]["Info"];
			}
		}
		
		# Create the data array for export
		$ExportArr = array();
		for($i=0; $i<sizeof($StudentInfo); $i++) {
			$ExportArr[$i][0] = $StudentInfo[$i]["WebSAMSRegNo"];
			$ExportArr[$i][1] = $StudentInfo[$i]["ClassName"];
			$ExportArr[$i][2] = $StudentInfo[$i]["ClassNumber"];
			$ExportArr[$i][3] = $StudentInfo[$i]["StudentName"];
			if (isset($ExtraInfoAsso[$StudentInfo[$i]["UserID"]]) && $isTemplate != "1")
				$ExportArr[$i][4] = $ExtraInfoAsso[$StudentInfo[$i]["UserID"]];
			else
				$ExportArr[$i][4] = "";
		}
		
		$lexport = new libexporttext();
		
		if ($type=="AdjustPosition")
		{
			$exportColumn = array("WebSAMSRegNumber", "Class Name", "Class Number", "Student Name", $eReportCard['AdjustPositionLabal']);
		}
		else	// extra info
		{
			$exportColumn = array("WebSAMSRegNumber", "Class Name", "Class Number", "Student Name", $dataTitle);
		}
		
		$export_content = $lexport->GET_EXPORT_TXT($ExportArr, $exportColumn);
		
		intranet_closedb();
		
		// Output the file to user browser
		$filename = str_replace(' ', '_', $filename);
		$lexport->EXPORT_FILE($filename, $export_content);
	} else {
		echo "You have no priviledge to access this page.";
	}
} else {
	echo "You have no priviledge to access this page.";
}
?>