<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");

intranet_auth();
intranet_opendb();

if ($plugin['ReportCard2008']) {
	
	if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
	{
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
		$lreportcard = new libreportcardcustom();
	} else {
		$lreportcard = new libreportcard();
	}

	if ($lreportcard->hasAccessRight()) {
		$limport = new libimporttext();
		$format_array = array("WebSAMSRegNumber", "Class Name", "Class Number","Student Name","Comment Content");
		
		$li = new libdb();
		$filepath = $userfile;
		$filename = $userfile_name;
		
		#  4 parameters need to be passed to other pages
		$parmeters = "ClassID=$ClassID&SubjectID=$SubjectID&ReportID=$ReportID&ClassLevelID=$ClassLevelID&SubjectGroupID=$SubjectGroupID&RedirectLocation=$RedirectLocation";
		
		//$TextareaMaxLength = ($lreportcard->textAreaMaxChar_SubjectTeacherComment=='')? $lreportcard->textAreaMaxChar : $lreportcard->textAreaMaxChar_SubjectTeacherComment;
		$TextareaMaxLength = $lreportcard->Get_Subject_Teacher_Comment_MaxLength($ReportID, $SubjectID);
		
		if($filepath=="none" || $filepath == "" || !isset($SubjectID, $ReportID)){          # import failed
		    header("Location: import_comment.php?$parmeters&Result=import_failed2");
		    exit();
		} else {
		    if($limport->CHECK_FILE_EXT($filename)) {
				# read file into array
				# return 0 if fail, return csv array if success
				//$data = $limport->GET_IMPORT_TXT($filepath);
				$data = $limport->GET_IMPORT_TXT($filepath, $incluedEmptyRow=0, $lineBreakReplacement='<!--LineBreak-->');
				#$toprow = array_shift($data);                   # drop the title bar
				
				$limport->SET_CORE_HEADER($format_array);
				
				if (!$limport->VALIDATE_HEADER($data, true) || !$limport->VALIDATE_DATA($data)) {
					header("Location: import_comment.php?$parmeters&Result=import_failed");
					exit();
				}
				
				// Class
				$ClassArr = $lreportcard->GET_CLASSES_BY_FORM($ClassLevelID);
				for($i=0 ; $i<count($ClassArr) ;$i++) {
					if($ClassArr[$i][0] == $ClassID)
						$ClassName = $ClassArr[$i][1];
				}
				
				// Student Info Array
				if ($SubjectGroupID != '')
				{
					$StudentArray = $lreportcard->GET_STUDENT_BY_SUBJECT_GROUP($SubjectGroupID, $ClassLevelID, '', 1);
				}
				else
				{
//					$StudentArray = $lreportcard->GET_STUDENT_BY_CLASS($ClassID);
					//2014-0224-1422-33184
					$StudentArray = $lreportcard->Get_Marksheet_Accessible_Student_List_By_Subject_And_Class($ReportID, $_SESSION['UserID'], $ClassID, $ParentSubjectID, $SubjectID);
				}
				$StudentSize = sizeof($StudentArray);
				
				if ($StudentSize == 0) {
					header("Location: import_comment.php?$parmeters&Result=import_failed");
					exit();
				}
				
				$StudentIDList = "";
				for($i=0; $i<$StudentSize; $i++)
					$StudentIDArray[] = $StudentArray[$i][0];
				$StudentIDList = implode(",", $StudentIDArray);
				
				$table = $lreportcard->DBName.".RC_MARKSHEET_COMMENT";
				
				# get a list of exist comment record
				$sql = "SELECT StudentID FROM $table WHERE 
						SubjectID = '$SubjectID' AND 
						ReportID = '$ReportID' AND 
						StudentID IN ($StudentIDList)";
				$existStudents = $lreportcard->returnVector($sql);
				
				# build an array mapping ClassNumber to UserID in the class
				/*
				$sql = "SELECT UserID, ClassNumber FROM INTRANET_USER ";
				$sql .= "WHERE UserID IN ($StudentIDList) ";
				$sql .= "ORDER BY ClassNumber";
				*/
				$sql = "SELECT 	ycu.UserID, ycu.ClassNumber, yc.ClassTitleEn as ClassName
						FROM	YEAR_CLASS_USER as ycu
								INNER JOIN
								YEAR_CLASS as yc
								ON (ycu.YearClassID = yc.YearClassID 
									AND ycu.UserID IN ($StudentIDList)
									AND yc.AcademicYearID = '".$lreportcard->schoolYearID."')
						ORDER BY
								ycu.ClassNumber
						";
				$tempResult = $lreportcard->returnArray($sql);
				
				for($i=0; $i<sizeof($tempResult); $i++) {
					$thisUserID = $tempResult[$i]["UserID"];
					$thisClassNumber = $tempResult[$i]["ClassNumber"];
					$thisClassName = $tempResult[$i]["ClassName"];
					
					$classNumMap[$thisClassName][$thisClassNumber] = $thisUserID;
				}
				
				array_shift($data);
			    $values = "";
			    $delim = "";
				$successUpdatedCount = 0;
				
				for ($i=0; $i<sizeof($data); $i++) {
					if (empty($data[$i]))
						continue;
					list($webSAMSRegNumber,$className,$classNum,$studentName,$comment) = $data[$i];
					$studentID = $classNumMap[$className][$classNum];
					
					if ($studentID == '') {
						continue;
					}
					
					//$comment = addslashes($comment);
					# if comment already exist, put it into array for UPDATE
					$finalComment = $lreportcard->Get_Safe_Sql_Query(strip_tags(str_replace('<!--LineBreak-->', "\n", $comment)));
					
					if ($TextareaMaxLength != -1) {
						if(mb_strlen($finalComment, "utf-8") > $TextareaMaxLength) 
						{
							$finalComment = mb_substr($finalComment,0,$TextareaMaxLength, "utf-8");
							$CutRow[] = ($i+2);
						}
					}
					
					
					if (in_array($studentID, $existStudents))
						$updateTeacherComment[$studentID] = $finalComment;
					# else,  INSERT
					else
						$insertTeacherComment[$studentID] = $finalComment;
			    }
				
				$success = array();
				
				# INSERT new comment
				if (isset($insertTeacherComment)) {
					foreach($insertTeacherComment as $k => $v) {
						if (in_array($k, $StudentIDArray)) {
							$values[] = "('$k', '$SubjectID', '$ReportID', '$v', '".$lreportcard->uid."', NOW(), NOW())";
							$successUpdatedCount++;
						}
					}
					
					$values = implode(",", $values);
					$field = "StudentID, SubjectID, ReportID, Comment, TeacherID, DateInput, DateModified";
				    $sql = "INSERT INTO $table ($field) VALUES $values";
					$success[] = $li->db_db_query($sql);
				}	
				
				# UPDATE existing comment
				if (isset($updateTeacherComment)) {
					foreach($updateTeacherComment as $k => $v) {
						$sql = "UPDATE $table SET Comment = '$v', TeacherID = '".$lreportcard->uid."', DateModified = NOW() WHERE ";
						$sql .= "StudentID = '$k' AND SubjectID = '$SubjectID' AND ReportID = '$ReportID'";
						$success[] = $li->db_db_query($sql);
						$successUpdatedCount++;
					}
				}
				
				if (in_array(false, $success)) {
					header("Location: import_comment.php?$parmeters&Result=import_failed2");
					exit();
				}
		    } else {
				header("Location: import_comment.php?$parmeters&Result=import_failed");
				exit();
			}
		}
		
		$CutRow = implode(",",(array)$CutRow);
		
		intranet_closedb();
		header("Location: teacher_comment.php?$parmeters&SuccessCount=$successUpdatedCount&Result=num_records_updated&cut_row=$CutRow");
	} else {
		echo "You have no priviledge to access this page.";
	}
} else {
	echo "You have no priviledge to access this page.";
}
?>