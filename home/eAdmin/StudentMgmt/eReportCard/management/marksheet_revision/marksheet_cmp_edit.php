<?php
// Using : Bill

/***********************************************************
 * Modification
 * 20181107 Bill [2018-1105-1216-00206]
 *      - General deploy function - Paste from Excel (Firefox & Chrome)
 * 20180723 Bill [2018-0718-0947-53066]
 *      - support copy grade from excel directly
 * 20180207 Bill [2018-0207-1120-00054]
 * 		- Support Paste from Excel (Firefox & Chrome)	($eRCTemplateSetting['Marksheet']['ApplyNewPasteMedthod'])
 * 20170519 Bill
 * 		- Allow View Group to access (view only)
 * 20170330 Bill [2016-1115-1700-32235]
 * 		- Class Teacher (not subject group teacher) cannot edit marksheet ($eRCTemplateSetting['Marksheet']['OnlyAllowSubjectTeacherToEdit'])
 * 20160122 Bill [2016-0122-1047-59207] 
 * 		- fixed: cannot display "+" after save
 * 20141103 Siuwan [2014-0912-1716-13164]: 
 *		- pt2: diff style for assessments in marksheet
 * 20140801 Ryan  : 
 * 		- SIS Cust
 * 20120403 Marcus:
 * 		- Allow teacher to view marks even after submission period
 * 		- 2012-0208-1800-43132 - �F�д`�D�òz���� - Edit mark after submit the marksheet 
 *	20110829 Ivan:
 *	- add estimated mark logic
 * 	20100108 Marcus:
 * 	- set input textbox to disabled  if user set BlockMarksheet of specific term to true
 * *********************************************************/

$PageRight = "TEACHER";

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
//include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping_ui.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if ($plugin['ReportCard2008'])
{
	include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
	if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") {
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
	}
	include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
	include_once($PATH_WRT_ROOT."includes/libreportcard2008_schedule.php");
	//include($PATH_WRT_ROOT."includes/eRCConfig.php");
	
	/* Temp Library*/
	# SIS CUST
	if ($ReportCardCustomSchoolName == "sis") {
		include_once($PATH_WRT_ROOT."includes/libreportcard2008j.php");
		$lreportcard = new libreportcard2008j();
	}
	else {
		$lreportcard = new libreportcard();
	}
	$lreportcard_ui = new libreportcard_ui();
	$lreportcard_schedule = new libreportcard_schedule();
	
	$CurrentPage = "Management_MarkSheetRevision";
	$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
	
	$isPrintMode = (isset($_POST['isPrintMode']))? $_POST['isPrintMode'] : $_GET['isPrintMode'];
	
    if ($lreportcard->hasAccessRight())
    {
        $linterface = new interface_html();
        
        # Tag
		$TAGS_OBJ[] = array($eReportCard['Management_MarksheetRevision'], "", 0);
		
		$linterface->LAYOUT_START();
		
		// [2018-1105-1216-00206]
		// [2018-0207-1120-00054]
		//if($eRCTemplateSetting['Marksheet']['ApplyNewPasteMedthod']) {
		if(!$eRCTemplateSetting['Marksheet']['ApplyOldPasteMethod']) {
			echo $linterface->Include_CopyPaste_JS_CSS("2016");
		}
		else {
			echo $linterface->Include_CopyPaste_JS_CSS();
		}
		echo $linterface->Include_Excel_JS_CSS();
		echo $lreportcard_ui->Include_eReportCard_Common_JS();
		
		############################################################################################################
		
		// Period (Term x, Whole Year, etc)
		$PeriodArr = $lreportcard->GET_REPORT_TYPES($ClassLevelID, $ReportID);
		$PeriodName = (count($PeriodArr) > 0) ? $PeriodArr['SemesterTitle'] : '';
		
		# Load Settings - Calculation
		// Get the Calculation Method of Report Type - 1: Right-Down,  2: Down-Right
		$SettingCategory = 'Calculation';
		$categorySettings = $lreportcard->LOAD_SETTING($SettingCategory);
		$TermCalculationType = $categorySettings['OrderTerm'];
		$FullYearCalculationType = $categorySettings['OrderFullYear'];
		
		# Load Settings - Storage&Display
		$SettingStorDisp = 'Storage&Display';
		$storDispSettings = $lreportcard->LOAD_SETTING($SettingStorDisp);
		
		# Load Settings - Absent&Exempt
		$SettingAbsentExempt = 'Absent&Exempt';
		$absentExemptSettings = $lreportcard->LOAD_SETTING($SettingAbsentExempt);
		
		# Check for Weight or Percentage will be used
		if(count($PeriodArr) > 0 && $PeriodArr['Semester'] == "F") {
			$usePercentage = ($FullYearCalculationType == 1) ? 1 : 0;
		}
		else {
			$usePercentage = ($TermCalculationType == 1) ? 1 : 0;
		}
		
		# Get Class Info 
		$ClassArr = $lreportcard->GET_CLASSES_BY_FORM($ClassLevelID);
		for($i=0; $i<count($ClassArr); $i++)
		{
			if($ClassArr[$i][0] == $ClassID) {
				$ClassName = $ClassArr[$i][1];
			}
		}
		
		# Get Subject Info
		$SubjectName = $lreportcard->GET_SUBJECT_NAME($SubjectID);
		
		# Get Student Info
		/*
		$StudentArr = $lreportcard->GET_STUDENT_BY_CLASS($ClassID, '', 1);
		$StudentSize = count($StudentArr);
		*/
		// Student Info Array
		if ($SubjectGroupID != '')
		{
			$StudentArr = $lreportcard->GET_STUDENT_BY_SUBJECT_GROUP($SubjectGroupID, $ClassLevelID, '', 1, 1);
			$showClassName = true;
			
			$obj_SubjectGroup = new subject_term_class($SubjectGroupID, $GetTeacherList=true);
			$SubjectGroupName = $obj_SubjectGroup->Get_Class_Title();
			//$TitleDisplay = "<td>".$Lang['SysMgr']['SubjectClassMapping']['SubjectGroup']." : <strong>".$SubjectGroupName."</strong></td>";
			
			$SubjectOrGroupTitle = $Lang['SysMgr']['SubjectClassMapping']['SubjectGroup'];
			$SubjectOrGroupNameDisplay = $SubjectGroupName;
			
			$TeacherName = implode('<br />', Get_Array_By_Key((array)$obj_SubjectGroup->ClassTeacherList, 'TeacherName'));
			$TeacherName = ($TeacherName=='')? $Lang['General']['EmptySymbol'] : $TeacherName;
			$SubjectTeacherTr = '';
			$SubjectTeacherTr .= '<tr>'."\r\n";
				$SubjectTeacherTr .= '<td class="field_title">'.$Lang['General']['Teacher'].'</td>'."\r\n";
				$SubjectTeacherTr .= '<td>'.$TeacherName.'</td>'."\r\n";
			$SubjectTeacherTr .= '</tr>'."\r\n";
		}
		else
		{
//			if ($lreportcard->IS_ADMIN_USER($_SESSION['UserID']) == true || $lreportcard->Is_Class_Teacher($_SESSION['UserID'], $ClassID) == true)
//			{
//				# Admin / Class Teacher => Show all students
//				$TaughtStudentList = '';
//			}
//			else
//			{
//				# Subject Teacher => Show taught students only
//				$TargetSubjectID = ($ParentSubjectID != '')? $ParentSubjectID : $SubjectID;
//				$TaughtStudentArr = $lreportcard->Get_Student_List_Taught_In_This_Consolidated_Report($ReportID, $_SESSION['UserID'], $ClassID, $TargetSubjectID);
//				$TaughtStudentList = '';
//				if (count($TaughtStudentArr > 0))
//					$TaughtStudentList = implode(',', $TaughtStudentArr);
//			}
//			$StudentArr = $lreportcard->GET_STUDENT_BY_CLASS($ClassID, $TaughtStudentList, 1, 0, 1);
//			//$StudentArr = $lreportcard->GET_STUDENT_BY_CLASS($ClassID, '', 1);

			//2014-0224-1422-33184
			$StudentArr = $lreportcard->Get_Marksheet_Accessible_Student_List_By_Subject_And_Class($ReportID, $_SESSION['UserID'], $ClassID, $ParentSubjectID, $SubjectID);
			$showClassName = false;
			
			//$TitleDisplay = "<td>".$eReportCard['Class']." : <strong>".$ClassName."</strong></td>";
			$SubjectOrGroupTitle = $eReportCard['Class'];
			$SubjectOrGroupNameDisplay = $ClassName;
			
			$SkipSubjectGroupCheckingForGetMarkSheet = 1;
		}
		$StudentSize = count($StudentArr);
		
		$StudentIDArr = array();
		if(count($StudentArr) > 0){
			for($i=0; $i<count($StudentArr); $i++) {
				$StudentIDArr[$i] = $StudentArr[$i]['UserID'];
			}
		}
		
		# Get Component Subject(s) Info
		$isEdit = 1;					// default "1" as the marksheet is editable
		$CmpSubjectArr = array();
		$CmpSubjectIDArr = array();
		$isCalculateByCmpSub = 0;		// set to "1" when one ScaleInput of component subjects is Mark(M)
		$isCmpSubject = $lreportcard->CHECK_SUBJECT_IS_COMPONENT_SUBJECT($SubjectID);
		
		if(!$isCmpSubject)
		{
			$CmpSubjectIDArr = array();
			$CmpSubjectArr = $lreportcard->GET_COMPONENT_SUBJECT($SubjectID, $ClassLevelID,'','',$ReportID);
			if(!empty($CmpSubjectArr))
			{
				for($i=0; $i<count($CmpSubjectArr); $i++)
				{
					$CmpSubjectIDArr[] = $CmpSubjectArr[$i]['SubjectID'];
					$CmpSubjectOrder[$CmpSubjectArr[$i]['SubjectID']] = $i;
					
					$CmpSubjectFormGradingArr = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $CmpSubjectArr[$i]['SubjectID'],0,0,$ReportID);
					if(count($CmpSubjectFormGradingArr) > 0 && $CmpSubjectFormGradingArr['ScaleInput'] == "M") {
						$isCalculateByCmpSub = 1;
					}
				}
			}
			// Check whether the marksheet is editable or not 
			$isEdit = $lreportcard->CHECK_MARKSHEET_STATUS($SubjectID, $ClassLevelID, $ReportID, $CmpSubjectIDArr);
		}
		
		# Get Status of Completed Action
		# $PeriodAction = "Submission" or "Verification"
		$PeriodAction = "Submission";
		$PeriodType = $lreportcard->COMPARE_REPORT_PERIOD($ReportID, $PeriodAction);
		if($PeriodType != 1 && $ck_ReportCard_UserType != "ADMIN") {
			$ViewOnly = true;
		}
		$SpecialPeriodType = $lreportcard_schedule->compareSpecialMarksheetSubmissionPeriodForTeacher($ReportID, $_SESSION['UserID'], $SubjectID, $SubjectGroupID);
		if ($SpecialPeriodType == 1) {
			$ViewOnly = false;
		}
		
		// [2016-1115-1700-32235] Check Current Subject Group Teacher
		if($eRCTemplateSetting['Marksheet']['OnlyAllowSubjectTeacherToEdit'] && $ck_ReportCard_UserType != "ADMIN" && $ck_ReportCard_UserType != "VIEW_GROUP") {
			if(!$isCmpSubject) {
				$isSubjectGroupTeacher = $lreportcard->checkSubjectTeacher($_SESSION['UserID'], $SubjectID, "", $SubjectGroupID);
			}
			else {
				$isSubjectGroupTeacher = $lreportcard->checkSubjectTeacher($_SESSION['UserID'], $ParentSubjectID, "", $SubjectGroupID);
			}
			
			// Only Subject Group Teacher can edit
			$isSubjectGroupTeacher = !empty($isSubjectGroupTeacher);
			if(!$isSubjectGroupTeacher) {
				$ViewOnly = true;
			}
		}
		
		// View Group - View Only
		if($ck_ReportCard_UserType == "VIEW_GROUP") {
			$ViewOnly = true;
		}
		
		# Get Data Either Assessment Column(s) in Term Or Term Column(s) in Wholely Report	
		// Loading Report Template Data
		$basic_data = $lreportcard->returnReportTemplateBasicInfo($ReportID);			// Basic  Data
		$column_data = $lreportcard->returnReportTemplateColumnData($ReportID); 		// Column Data
		
		foreach((array)$column_data as $thiscolumndata) {
			$columnIDDataMap[$thiscolumndata['ReportColumnID']] = $thiscolumndata;
		}
		
		$ColumnSize = count($column_data);
		$ColumnTitleAry = $lreportcard->returnReportColoumnTitle($ReportID);
		$ReportTitle = $basic_data['ReportTitle'];
		$ReportType = $basic_data['Semester'];
		$isPercentage = $basic_data['PercentageOnColumnWeight'];
		
		# Get Existing Report Calculation Method
		$CalculationType = ($ReportType == "F") ? $FullYearCalculationType : $TermCalculationType;
		
		# Get Report Column Weight
		$WeightAry = array();
		if($CalculationType == 2)
		{
			if(count($column_data) > 0)
			{
		    	for($i=0; $i<count($column_data); $i++)
		    	{
					//$OtherCondition = "SubjectID IS NULL AND ReportColumnID = ".$column_data[$i]['ReportColumnID'];
					//$weight_data = $lreportcard->returnReportTemplateSubjectWeightData($ReportID, $OtherCondition); 
					//$WeightAry[$weight_data[0]['ReportColumnID']][$SubjectID] = $weight_data[0]['Weight'];
					
					// modified on 30 July 2008
                    for($j=0; $j<count($CmpSubjectArr); $j++)
                    {
                    	$OtherCondition = "SubjectID = '".$CmpSubjectArr[$j]['SubjectID']."' AND ReportColumnID = ".$column_data[$i]['ReportColumnID'];
						$weight_data = $lreportcard->returnReportTemplateSubjectWeightData($ReportID, $OtherCondition); 
                    	$WeightAry[$weight_data[0]['ReportColumnID']][$CmpSubjectArr[$j]['SubjectID']] = $weight_data[0]['Weight'];
                    }
				}
			}
		}
		else
		{
			$weight_data = $lreportcard->returnReportTemplateSubjectWeightData($ReportID); 			
			for($i=0; $i<count($weight_data); $i++)
			{
				$thisReportColumnID = $weight_data[$i]['ReportColumnID'];
				$thisReportColumnID = ($thisReportColumnID=='')? 0 : $thisReportColumnID;
				$WeightAry[$thisReportColumnID][$weight_data[$i]['SubjectID']] = $weight_data[$i]['Weight'];
			}
		}
		
		$isAllCmpSubjectWeightZeroArr = $lreportcard->IS_ALL_CMP_SUBJECT_ZERO_WEIGHT($ReportID, $SubjectID);
		
		# Get MarkSheet Score Info / ParentSubMarksheetScoreInfoArr	
		$ColumnHasTemplateAry = array();
		$hasDirectInputMark = 0;
		
		if($ColumnSize > 0)
		{
		    for($i=0; $i<$ColumnSize; $i++)
		    {
			    /* 
				* Check for the Mark Column is allowed to fill in mark when any term does not have related report template 
				* by SemesterNum (ReportType) && ClassLevelID
				* An array is initialized as a control variable [$ColumnHasTemplateAry]
				* A variable is initialized as a control variable [$hasDirectInputMark], must be used with [$ReportType == "F"]
				* to control the Import function to be used or not
				*/
				if($ReportType == "F"){
					$ColumnHasTemplateAry[$column_data[$i]['ReportColumnID']] = $lreportcard->CHECK_REPORT_TEMPLATE_FROM_COLUMN($column_data[$i]['SemesterNum'], $ClassLevelID);
					if(!$ColumnHasTemplateAry[$column_data[$i]['ReportColumnID']])
						$hasDirectInputMark = 1;
				}
				else {
					$ColumnHasTemplateAry[$column_data[$i]['ReportColumnID']] = 1;
				}
			}
		}
		
		####################################################################################################
		# General Approach dealing with Search Term Result From Whole Year Report not From Term Report
		# Can deal with Customization of SIS dealing with secondary template 
		$isAllWholeReport = 0;
		if($ReportType == "F"){
			$ReportTypeArr = array();
			$ReportTypeArr = $lreportcard->GET_REPORT_TYPES($ClassLevelID);
			if(count($ReportTypeArr) > 0){
				$flag = 0;
				for($j=0 ; $j<count($ReportTypeArr) ; $j++){
					if($ReportTypeArr[$j]['ReportID'] != $ReportID){
						if($ReportTypeArr[$j]['Semester'] != "F")	$flag = 1;
					}
				}
				if($flag == 0) $isAllWholeReport = 1;
			}
		}
		
		# Get the Corresponding Whole Year ReportID from 
		# the ReportColumnID of the Current Whole Year Report 
		$WholeYearColumnHasTemplateArr = array();
		if($isAllWholeReport == 1){
			if($ReportType == "F" && $ColumnSize > 0){
				for($j=0 ; $j<$ColumnSize ; $j++){
					$ReportColumnID = $column_data[$j]['ReportColumnID'];
					# Return Whole Year Report Template ID - ReportID 
					$WholeYearColumnHasTemplateArr[$ReportColumnID] = $lreportcard->CHECK_WHOLE_YEAR_REPORT_TEMPLATE_FROM_COLUMN($ReportID, $column_data[$j]['SemesterNum'], $ClassLevelID);
					
					# Check Whether the Return WholeYear ReportID is not greater the Current ReportID
					# if User creates All Reports sequently
					if($WholeYearColumnHasTemplateArr[$ReportColumnID] != false){
						if($WholeYearColumnHasTemplateArr[$ReportColumnID] > $ReportID)
							$WholeYearColumnHasTemplateArr[$ReportColumnID] = false;
					}
				}
			}
		}
		####################################################################################################
		
		# Get Special Case 
		$possibleSpeicalCaseSet1 = $lreportcard->GET_POSSIBLE_MARKSHEET_SPECIAL_CASE_SET1();
		$possibleSpeicalCaseSet2 = $lreportcard->GET_POSSIBLE_MARKSHEET_SPECIAL_CASE_SET2();
		
		# Main - Preparation
		$SchemeID = array();
		$ScaleInput = array();		// M: Mark, G: Grade
		$SchemeType = array();		// H: Honor Based, PF: PassFail
		$InputGradeOption = array();
		$InputPFOption = array();
		$NewReportColumn = array();
		$MarksheetScoreInfoArr = array();
		$SchemeFullMark = array();
		$ScaleInput = array();
		$SchemeType = array();
		$MarksheetOverallScoreInfoArr = array();
		$ProgressArr = array();
		$isSetSubjectOverall = array();
		$MarksheetAssessmentAry = array();  //2014-0912-1716-13164 pt2
		if(count($CmpSubjectArr) > 0 && $ColumnSize > 0){
			for($i=0 ; $i<$ColumnSize ; $i++){
				$report_column_id = $column_data[$i]['ReportColumnID'];
				
				for($j=0 ; $j<count($CmpSubjectArr) ; $j++){
				    $cmp_subject_id = $CmpSubjectArr[$j]['SubjectID'];
				    
				    if ($eRCTemplateSetting['Marksheet']['HideZeroWeightSubjectComponent']) {
				    	$thisSubjectColumnWeight = $WeightAry[$report_column_id][$cmp_subject_id];
				    	if ($thisSubjectColumnWeight == 0) {
				    		continue;
				    	}
				    }
				    
				    if($isAllWholeReport == 1){
					    if($WholeYearColumnHasTemplateArr[$report_column_id] != false){
						    
					    } else {
							#######################################################################################################################################
							# for the Whole Year Report which defined any Term Column in other Whole Year Report	added on 22 May by Jason
							# the case for Whole Year Report Column Which cannot be found in Report Column of other Whole Year report
							# code enter here
							#######################################################################################################################################
							# Get Input Mark From Marksheet 
					    	$MarksheetScoreInfoArr[$report_column_id][$cmp_subject_id] = $lreportcard->GET_MARKSHEET_SCORE($StudentIDArr, $cmp_subject_id, $report_column_id,0,'',$SkipSubjectGroupCheckingForGetMarkSheet);
							
							# Set Valid Report Columns into a new Column Array
					    	$column_data[$i]['CmpSubjectID'] = $cmp_subject_id;
					    	$NewReportColumn[] = $column_data[$i];
				    	}
					} else {
				    	if($ReportType != "F" || ($ReportType == "F" && !$ColumnHasTemplateAry[$report_column_id]) ){
					    	# Get Input Mark From Marksheet 
					    	$MarksheetScoreInfoArr[$report_column_id][$cmp_subject_id] = $lreportcard->GET_MARKSHEET_SCORE($StudentIDArr, $cmp_subject_id, $report_column_id,0,'',$SkipSubjectGroupCheckingForGetMarkSheet);
					    	
					    	# Set Valid Report Columns into a new Column Array
					    	$column_data[$i]['CmpSubjectID'] = $cmp_subject_id;
					    	$NewReportColumn[] = $column_data[$i];
					    	
				    	}
			    	}
	    		}
			}
			
			for($j=0 ; $j<count($CmpSubjectArr) ; $j++){
				$cmp_subject_id = $CmpSubjectArr[$j]['SubjectID'];
				
				$isSetSubjectOverall[$CmpSubjectArr[$i]['SubjectID']] = 0;
				
				//Initialization of Grading Scheme
				$SchemeID[$cmp_subject_id] = '';
				$ScaleInput[$cmp_subject_id] = "M";		// M: Mark, G: Grade
				$SchemeType[$cmp_subject_id] = "H";		// H: Honor Based, PF: PassFail
				$SchemeInfo = array();
				$SubjectFormGradingArr = array();
				
				$SubjectFormGradingArr = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $cmp_subject_id,0,0,$ReportID);
				
				# Get Grading Scheme Info of that subject
				if(count($SubjectFormGradingArr) > 0){
					$SchemeID[$cmp_subject_id] = $SubjectFormGradingArr['SchemeID'];
					$ScaleInput[$cmp_subject_id] = $SubjectFormGradingArr['ScaleInput'];
					
					$SchemeMainInfo = $lreportcard->GET_GRADING_SCHEME_MAIN_INFO($SchemeID[$cmp_subject_id]);
					$SchemeType[$cmp_subject_id] = $SchemeMainInfo['SchemeType'];
					$SchemeFullMark[$cmp_subject_id] = $SchemeMainInfo['FullMark'];
				}
				
				
				if($SchemeType[$cmp_subject_id] == "H" && $ScaleInput[$cmp_subject_id] == "G"){	// Prepare Selection Box using Grade as Input
					$InputTmpOption = array();
					$InputGradeOption[$cmp_subject_id] = array();
					
					$SchemeGrade = $lreportcard->GET_GRADE_FROM_GRADING_SCHEME_RANGE($SchemeID[$cmp_subject_id]);
					$InputGradeOption[$cmp_subject_id][0] = array('', '--');
					if(!empty($SchemeGrade)){
						foreach($SchemeGrade as $GradeRangeID => $grade)
							$InputGradeOption[$cmp_subject_id][] = array($GradeRangeID, $grade);
					}
					
					for($k=0 ; $k<count($possibleSpeicalCaseSet1) ; $k++){
						$InputGradeOption[$cmp_subject_id][] = array($possibleSpeicalCaseSet1[$k], $possibleSpeicalCaseSet1[$k]);
					}
				}
				else if($SchemeType[$cmp_subject_id] == "PF"){			// Prepare Selection Box using Pass/Fail as Input
					$SchemePassFail = array();
					$SchemePassFail['P'] = $SchemeMainInfo['Pass'];
					$SchemePassFail['F'] = $SchemeMainInfo['Fail'];
				
					$InputPFOption[$cmp_subject_id] = array();
					$InputPFOption[$cmp_subject_id][0] = array('', '--');
					$InputPFOption[$cmp_subject_id][1] = array('P', $SchemeMainInfo['Pass']);
					$InputPFOption[$cmp_subject_id][2] = array('F', $SchemeMainInfo['Fail']);
					
					
					for($k=0 ; $k<count($possibleSpeicalCaseSet2) ; $k++){
						$InputPFOption[$cmp_subject_id][] = array($possibleSpeicalCaseSet2[$k], $possibleSpeicalCaseSet2[$k]);
					}
				}
				
				# Get Overall Subject Mark Or Overall Term Subject Mark 
				# for InputScale is Grade or Scheme Type is PassFail
				if($SchemeType[$cmp_subject_id] == "PF" || ($SchemeType[$cmp_subject_id] == "H" && $ScaleInput[$cmp_subject_id] == "G") ){
					$MarksheetOverallScoreInfoArr[$cmp_subject_id] = $lreportcard->GET_MARKSHEET_OVERALL_SCORE($StudentIDArr, $cmp_subject_id, $ReportID,$SkipSubjectGroupCheckingForGetMarkSheet);
				}
				
				# Get Complete Status of Common Subject or Parent Subject
			    $ProgressArr[$cmp_subject_id] = $lreportcard->GET_MARKSHEET_SUBMISSION_PROGRESS($cmp_subject_id, $ReportID, $ClassID);
			}
		}
		
		
		# Main Content of Marksheet Reivision
		$input_mark_count = 0;
		$overall_cnt = 0;
		$display_overall = '';
		$display = '';
		$display .= '<table width="100%" cellpadding="4" cellspacing="0">';
		
		
		# Display - Header
		$display .= '<tr>';
		$display .= '<td colspan="2" class="tablegreentop tabletopnolink">&nbsp;</td>';
		if(count($NewReportColumn) > 0){
			# Report Column Header
		    for($i=0 ; $i<count($NewReportColumn) ; $i++){
			    list($ReportColumnID, $ColumnTitle, $DefaultWeight, $SemesterNum) = $NewReportColumn[$i];
			    $CmpSubjectID = $NewReportColumn[$i]['CmpSubjectID'];
			    //$ColumnTitle = ($ColumnTitle != "" && $ColumnTitle != null) ? convert2unicode($ColumnTitle, 1, 2) : $ColumnTitleAry[$ReportColumnID];
			    $ColumnTitle = ($ColumnTitle != "" && $ColumnTitle != null) ? $ColumnTitle : $ColumnTitleAry[$ReportColumnID];
			    
			    if ($eRCTemplateSetting['ManualInputParentSubjectMark'] || ($isCalculateByCmpSub && $isAllCmpSubjectWeightZeroArr[$ReportColumnID])) {
				    $inputParentMark = true;
			    }
			    else {
				    $inputParentMark = false;
			    }
			    
			    $col_colpsan = 1;
			    for($j=$i+1 ; $j<count($NewReportColumn) ; $j++){
				    if($NewReportColumn[$j]['ReportColumnID'] == $ReportColumnID)
				    	$col_colpsan++;
				    else 
				    	break;
			    }
			    
			    if ($inputParentMark)
			    {
					$col_colpsan++;
		    	}
		    	
			    $display .= '<td align="center" colspan="'.$col_colpsan.'" class="tablegreentop tabletopnolink">'.$ColumnTitle.'</td>';
			    $MarksheetAssessmentAry[] = $ReportColumnID;  //2014-0912-1716-13164 pt2
			    $i = $j - 1;
		    }
		    
		    # Overall Subject Mark Header
		    for($i=0 ; $i<count($CmpSubjectArr) ; $i++){
		    	$cmp_subject_id = $CmpSubjectArr[$i]['SubjectID'];
		    	
		    	if ($eRCTemplateSetting['Marksheet']['HideZeroWeightSubjectComponent']) {
		    		$report_column_id = 0;
			    	$thisSubjectColumnWeight = $WeightAry[$report_column_id][$cmp_subject_id];
			    	if ($thisSubjectColumnWeight == 0) {
			    		continue;
			    	}
			    }
			    
				if($SchemeType[$cmp_subject_id] == "PF" || ($SchemeType[$cmp_subject_id] == "H" && $ScaleInput[$cmp_subject_id] == "G") ) {
				    $overall_cnt++;
				}
			}
		    
		    if($overall_cnt > 0){
		    	$display .= '<td align="center" colspan="'.$overall_cnt.'" class="tablegreentop tabletopnolink">';
		    	$display .= ($ReportType == "F") ? $eReportCard['OverallSubjectMark'] : $eReportCard['TermSubjectMark'];
		    	$display .= '</td>';
	    	}
    	}
		$display .= '</tr>';
		$display .= '<tr>
	          <td width="30" align="center" valign="top" class="tablegreentop tabletopnolink">'.$eReportCard['StudentNo_short'].'</td>
	          <td valign="top" class="tablegreentop tabletopnolink">'.$eReportCard['Student'].'</td>';
	    if(count($NewReportColumn) > 0){
		    
		    $positionToAddOverall = count($CmpSubjectArr);
		    $counter = 0;
		    $displayCounter = 0;
		    # Get Basic Column Title Info
		    for($i=0 ; $i<count($NewReportColumn) ; $i++){
			    list($ReportColumnID, $ColumnTitle, $DefaultWeight, $SemesterNum) = $NewReportColumn[$i];
			    $thisID = $i+$counter;
			    
			    $CmpSubjectID = $NewReportColumn[$i]['CmpSubjectID'];
			    //$ColumnTitle = ($ColumnTitle != "" && $ColumnTitle != null) ? convert2unicode($ColumnTitle, 1, 2) : $ColumnTitleAry[$ReportColumnID];
			    $ColumnTitle = ($ColumnTitle != "" && $ColumnTitle != null) ? $ColumnTitle : $ColumnTitleAry[$ReportColumnID];
			    
			    if (($isCalculateByCmpSub && $isAllCmpSubjectWeightZeroArr[$ReportColumnID]) || $eRCTemplateSetting['ManualInputParentSubjectMark'])
			    {
				    $inputParentMark = true;
			    }
			    else
			    {
				    $inputParentMark = false;
			    }
			    
			    
			    # Get Component Subject Name
			    $CmpSubjectNameEN = $lreportcard->GET_SUBJECT_NAME_LANG($NewReportColumn[$i]['CmpSubjectID'], "EN");
			    $CmpSubjectNameCH = $lreportcard->GET_SUBJECT_NAME_LANG($NewReportColumn[$i]['CmpSubjectID'], "CH");
			    
			    if (count((array)$eRCTemplateSetting['Management']['Marksheet']['ShowSubjectNameInLangArr']) == 0) {
			    	// normal case
			    	$CmpSubjectName = $CmpSubjectNameEN.' - '.$CmpSubjectNameCH;
			    }
			    else if (in_array('en', (array)$eRCTemplateSetting['Management']['Marksheet']['ShowSubjectNameInLangArr'])) {
			    	$CmpSubjectName = $CmpSubjectNameEN;
			    }
			    else if (in_array('ch', (array)$eRCTemplateSetting['Management']['Marksheet']['ShowSubjectNameInLangArr'])) {
			    	$CmpSubjectName = $CmpSubjectNameCH;
			    }
			    
			        
	            # Copy To Other Column
	            $CopyToSelection = '';
	            if($SchemeType[$CmpSubjectID] == "PF" || ($SchemeType[$CmpSubjectID] == "H" && $ScaleInput[$CmpSubjectID] == "G"))
			    {
			    	$optionArr = array();
			    	$optionArr[] = array(-1,"-- ".$eReportCard['CopyTo']." --");
			    	for($j=0 ; $j<count($column_data) ; $j++)
			    	{
			    		if($column_data[$j]['ReportColumnID'] != $ReportColumnID)
			    		{
			    			$TargetColID = $column_data[$j]['ReportColumnID'];
			    			$optionArr[] = array($TargetColID,$column_data[$j]['ColumnTitle']);
			    		}
			    	}
			    	$overallColumnNumber = $j*count($CmpSubjectArr)+$CmpSubjectOrder[$CmpSubjectID];
			    	$optionArr[] = array(0,($ReportType != "F") ? $eReportCard['TermSubjectMark'] : $eReportCard['OverallSubjectMark']);
			    	$CopyToSelection = "<br>".getSelectByArray($optionArr, " id='CopyToSelection_".$ReportColumnID."_".$CmpSubjectID."' ",-1,0,1);
			    	$CopyToSelection .= '<a href="javascript:js_Copy_Grade_From_Column('.$ReportColumnID.','.$CmpSubjectID.');"><img width="12" height="12" border="0" title="'.$eReportCard['Copy'].'" alt="'.$eReportCard['Copy'].'" src="/images/2009a/icon_copy.gif"></a>';
			    }
	            
			    # UI
	            $display .= '<td width="12%" align="center" class="tablegreentop tabletopnolink">'.$CmpSubjectName.$CopyToSelection.'<br />';
	            
	            if( $WeightAry[$ReportColumnID][$CmpSubjectID] != 0 && $WeightAry[$ReportColumnID][$CmpSubjectID] != null && $WeightAry[$ReportColumnID][$CmpSubjectID] != "" )
	            {
		           	$display .= '<input type="hidden" id="ColumnID_'.$displayCounter.'" name="ColumnID_'.$displayCounter.'" value="'.$ReportColumnID.'"/>';	
	            	$display .= '<input type="hidden" id="SubjectID_Column_'.$displayCounter.'" name="SubjectID_Column_'.$displayCounter.'" value="'.$CmpSubjectID.'"/>';	
	            	
	            	$displayCounter++;
	            	
	            	# Count the number of Report Column which use Input Mark as Input Method
		          	if($SchemeType[$CmpSubjectID] == "H" && $ScaleInput[$CmpSubjectID] == "M"){
			          	$input_mark_count++;
		          	}
	          	}
	          	
	          	$display .= '</td>';
	          	
	          	# Get Overall Subject Mark Title Info if any
	          	if($isSetSubjectOverall[$CmpSubjectID] == 0 && 
          		($SchemeType[$CmpSubjectID] == "PF" || ($SchemeType[$CmpSubjectID] == "H" && $ScaleInput[$CmpSubjectID] == "G")) )
	          	{
			    	$optionArr = array();
			    	
			    	$optionArr[] = array(-1,"-- ".$eReportCard['CopyTo']." --");
			    	for($j=0 ; $j<count($column_data) ; $j++)
			    	{
//			    		$TargetColNo = $j*count($CmpSubjectArr)+$CmpSubjectOrder[$CmpSubjectID];
		    			$optionArr[] = array($column_data[$j]['ReportColumnID'],$column_data[$j]['ColumnTitle']);
			    	}
			    	$CopyToSelection = "<br>".getSelectByArray($optionArr, " id='CopyToSelection_0_".$CmpSubjectID."' ",-1,0,1);
			    	$CopyToSelection .= '<a href="javascript:js_Copy_Grade_From_Column(0,'.$CmpSubjectID.');"><img width="12" height="12" border="0" title="'.$eReportCard['Copy'].'" alt="'.$eReportCard['Copy'].'" src="/images/2009a/icon_copy.gif"></a>';
	          		
		          	$display_overall .= '<td width="12%" align="center" class="tablegreentop tabletopnolink">'.$CmpSubjectName.$CopyToSelection.'</td>';
		          	$isSetSubjectOverall[$CmpSubjectID] = 1;
	          	} 
	          	
	          	if ($inputParentMark && ((($i+1) % $positionToAddOverall)==0))
			    {
				    $thisID = $input_mark_count;
				    
				    $display .= '<td width="12%" align="center" class="tablegreentop tabletopnolink">'.$eReportCard['SubjectOverallMark'].'<br />';
		            $display .= '<input type="hidden" id="ColumnID_'.$thisID.'" name="ColumnID_'.$thisID.'" value="'.$ReportColumnID.'"/>';	
		            $display .= '<input type="hidden" id="SubjectID_Column_'.$thisID.'" name="SubjectID_Column_'.$thisID.'" value="'.$SubjectID.'"/>';	
		          	$display .= '</td>';
		          	$input_mark_count++;
		          	$counter++;
		          	$displayCounter++;
			    }
      		}
      		
      		$display .= $display_overall;
    	}
	    $display .= '</tr>';      
	        
	   	# Display - Student Marksheet
	    for($j=0; $j<$StudentSize ; $j++){
			list($StudentID, $WebSAMSRegNo, $StudentNo, $StudentName, $ClassName) = $StudentArr[$j];
			$tr_css = ($j % 2 == 1) ? "tablegreenrow2" : "tablegreenrow1";
			$td_css = ($j % 2 == 1) ? "attendanceouting" : "attendancepresent";
			
			if ($showClassName)
				$thisClassDisplay = $ClassName.'-'.$StudentNo;
			else
				$thisClassDisplay = $StudentNo;
			
			$display .= '<tr class="'.$tr_css.' dataRow">
		          <td align="left" nowrap="nowrap" class="'.$td_css.' tabletext">'.$thisClassDisplay.'</td>
	          	  <td nowrap="nowrap" class="'.$td_css.' tabletext">'.$StudentName.'<input type="hidden" name="StudentID[]" id="StudentID_'.$j.'" value="'.$StudentID.'"/></td>';
	        if(count($NewReportColumn) > 0){
		        $inputmark_num = 0;
		        $positionToAddOverall = count($CmpSubjectArr);
		        $numOverallAdded = 0;
		        $numOfDisplayedColumn = 0;
		        
		        for($i=0 ; $i<count($NewReportColumn) ; $i++){
			        list($ReportColumnID, $ColumnTitle, $DefaultWeight, $SemesterNum) = $NewReportColumn[$i];
			        $CmpSubjectID = $NewReportColumn[$i]['CmpSubjectID'];
			        $_tdCss = ($j % 2 == 1) ? "attendanceouting" : "attendancepresent";//2014-0912-1716-13164 pt2
			        if($eRCTemplateSetting['Marksheet']['DiffCssStyleForAssessments']){ 
						$__pos = array_search($ReportColumnID,$MarksheetAssessmentAry);
						$_tdCss = ($__pos % 2 == 0) ? $_tdCss."2" : $_tdCss;
					}
			        if ( ($isCalculateByCmpSub && $isAllCmpSubjectWeightZeroArr[$ReportColumnID]) || $eRCTemplateSetting['ManualInputParentSubjectMark'] )
			        {
				    	$inputParentMark = true;   
			        }
			        else
			        {
				        $inputParentMark = false;
			        }
			        
			        # Format of Mark Name: mark[index of row][index of column]
				    $mark_name = "mark_".$CmpSubjectID."_".$ReportColumnID."_".$StudentID;
				    
			        if( $WeightAry[$ReportColumnID][$CmpSubjectID] != 0 && $WeightAry[$ReportColumnID][$CmpSubjectID] != null && $WeightAry[$ReportColumnID][$CmpSubjectID] != "" ){
				        
					    if(count($MarksheetScoreInfoArr) > 0){
						    // Mark Type : M-Mark, G-Grade, PF-PassFail, SC-SpecialCase
							$MarkType = $MarksheetScoreInfoArr[$ReportColumnID][$CmpSubjectID][$StudentID]['MarkType'];
						    
						    if($MarkType == "G" || $MarkType == "PF" || $MarkType == "SC"){
						    	$InputVal = $MarksheetScoreInfoArr[$ReportColumnID][$CmpSubjectID][$StudentID]['MarkNonNum'];
					    	} else if($MarkType == "M"){
								# For ScaleInput is Mark & SchemeType is Honor-Based
								# if MarkRaw is -1, the MarkRaw has not been assigned yet.
								# if MarkRaw >= 0, the MarkRaw is valid
						    	$InputVal = ($MarksheetScoreInfoArr[$ReportColumnID][$CmpSubjectID][$StudentID]['MarkRaw'] != -1) ? $MarksheetScoreInfoArr[$ReportColumnID][$CmpSubjectID][$StudentID]['MarkRaw'] : '';
					    	} else {
						    	$InputVal = '';
					    	}
					    } else {
						    $InputVal = '';
					    }
					    $InputVal = ($MarksheetScoreInfoArr[$ReportColumnID][$CmpSubjectID][$StudentID]['IsEstimated'])? $lreportcard->Get_Estimated_Score_Display($InputVal) : $InputVal;
				        
				        $otherPar = array(	'class'=>'textboxnum', 
				        					'onpaste'=>'isPasteContent(event, \''.$j.'\', \''.($numOfDisplayedColumn + $numOverallAdded).'\')', 
				        					'onkeyup'=>'isPasteContent(event, \''.$j.'\', \''.($numOfDisplayedColumn + $numOverallAdded).'\')',
										    'onkeydown'=>'isPasteContent(event, \''.$j.'\', \''.($numOfDisplayedColumn + $numOverallAdded).'\')',
				        					'onchange'=>'jCHANGE_STATUS()'
				        				 );
				        
				        $classname = '';				 
				        if(($lreportcard->Is_Marksheet_Blocked($ViewOnly) || $columnIDDataMap[$ReportColumnID]["BlockMarksheet"]=='true') && !$lreportcard->IS_ADMIN_USER($_SESSION['UserID']))
				        {
				        	$disable= " disabled  ";
				        	$classname = " enableonsubmit ";
				        	$otherPar['disabled']='disabled';
				        	$otherPar['class']='textboxnum enableonsubmit';
				        }
				        else
				        {
				        	$disable= '';
				        }
				        
				        $classname.= " Column_$ReportColumnID Subject_$CmpSubjectID";
				        
				        $mark_id = "mark[$j][".($numOfDisplayedColumn + $numOverallAdded)."]";
				        $mark_tag = 'id="'.$mark_id.'" name="'.$mark_name.'" '.$disabled .' onchange="jCHANGE_STATUS()" onpaste="isPasteContent(event, '.$j.', '.$i.')" onkeyup="isPasteContent(event, '.$j.', '.($i + $numOverallAdded).')" onkeydown="isPasteContent(event, '.$j.', '.($i + $numOverallAdded).')" onfocusout="jCHANGE_STATUS()" class="tabletexttoptext '.$classname.'"';
				        if($SchemeType[$CmpSubjectID] == "H" && $ScaleInput[$CmpSubjectID] == "G") {
				    		$InputMethod = $linterface->GET_SELECTION_BOX($InputGradeOption[$CmpSubjectID], $mark_tag, '', $InputVal);
				    		$MSwithSelectionBox = true;
			    		}
			    		else if($SchemeType[$CmpSubjectID] == "PF") {
				    		$InputMethod = $linterface->GET_SELECTION_BOX($InputPFOption[$CmpSubjectID], $mark_tag, '', $InputVal);
				    		$MSwithSelectionBox = true;
			    		}
			    		else {
				    		# Format of Mark ID  : mark_[Component Subject ID]_[ReportColumnID]_[StudentID]
				    		# [$i] of mark[$j][$i] == [$i] of ColumnID_$i for matching
				    		$InputMethod = $lreportcard->GET_INPUT_TEXT($mark_name, $mark_id, $InputVal, '', $otherPar);
				    		//$inputmark_num++;
			    		}
			    		
			    		$numOfDisplayedColumn++;
			        }
			        else {
					    $InputMethod = "--";
					}
					$display .= '<td align="center" class="'.$_tdCss.' tabletext">'.$InputMethod.'</td>';
					
					if ($inputParentMark && ((($i+1) % $positionToAddOverall)==0) && ((($i+1) % $positionToAddOverall)==0))
				    {
					    $numOverallAdded++;
					    
					    # Load Main Subject Data
						$SubjectFormGradingArr = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID,0,0,$ReportID);
						# Get Grading Scheme Info of that subject
						if(count($SubjectFormGradingArr) > 0){
							$SchemeID[$SubjectID] = $SubjectFormGradingArr['SchemeID'];
							$ScaleInput[$SubjectID] = $SubjectFormGradingArr['ScaleInput'];
							
							$SchemeMainInfo = $lreportcard->GET_GRADING_SCHEME_MAIN_INFO($SchemeID[$SubjectID]);
							$SchemeType[$SubjectID] = $SchemeMainInfo['SchemeType'];
							$SchemeFullMark[$SubjectID] = $SchemeMainInfo['FullMark'];
						}
						
						echo '<input type="hidden" name="FullMark_'.$SubjectID.'" id="FullMark_'.$SubjectID.'" value="'.$SchemeFullMark[$SubjectID].'"/>'."\n";
						echo '<input type="hidden" name="ScaleInput_'.$SubjectID.'" id="ScaleInput_'.$SubjectID.'" value="'.$ScaleInput[$SubjectID].'"/>'."\n";
						echo '<input type="hidden" name="SchemeType_'.$SubjectID.'" id="SchemeType_'.$SubjectID.'" value="'.$SchemeType[$SubjectID].'"/>'."\n";
		
				
					    $thisColumnScore = $lreportcard->GET_MARKSHEET_COLUMN_SCORE($StudentID, $ReportColumnID);
					    $thisRawMark = $thisColumnScore[$StudentID][$SubjectID]['MarkRaw'];
					    $MarkNonNum = $thisColumnScore[$StudentID][$SubjectID]['MarkNonNum'];
					    
					    // [2016-0122-1047-59207] display input value depending on mark type
					    // Mark Type : M-Mark, G-Grade, PF-PassFail, SC-SpecialCase
						$MarkType = $thisColumnScore[$StudentID][$SubjectID]['MarkType'];
					    if($MarkType == "G" || $MarkType == "PF" || $MarkType == "SC"){
					    	$InputVal = $MarkNonNum;
				    	} else if($MarkType == "M"){
							# For ScaleInput is Mark & SchemeType is Honor-Based
							# if MarkRaw is -1, the MarkRaw has not been assigned yet.
							# if MarkRaw >= 0, the MarkRaw is valid
					    	$InputVal = ($thisRawMark != -1) ? $thisRawMark : '';
				    	} else {
					    	$InputVal = '';
				    	}
					    
					    $thisIndex = $numOfDisplayedColumn + $numOverallAdded - 1;
					    //$InputVal = ($thisRawMark != -1) ? $thisRawMark : $MarkNonNum;
					    $InputVal = ($thisColumnScore[$StudentID][$SubjectID]['IsEstimated'])? $lreportcard->Get_Estimated_Score_Display($InputVal) : $InputVal;
					    
					    $mark_name = "sub_overall_mark_".$SubjectID."_".$ReportColumnID."_".$StudentID;
					    $mark_id = "mark[$j][".$thisIndex."]";
					        
					    $otherPar = array(	'class'=>'textboxnum', 
					    					'onpaste'=>'isPasteContent(event, \''.$j.'\', \''.$thisIndex.'\')', 
					    					'onkeyup'=>'isPasteContent(event, \''.$j.'\', \''.$thisIndex.'\')',
										    'onkeydown'=>'isPasteContent(event, \''.$j.'\', \''.$thisIndex.'\')',
					    					'onchange'=>'jCHANGE_STATUS()'
					    				 );
					    if(($lreportcard->Is_Marksheet_Blocked($ViewOnly) || $columnIDDataMap[$ReportColumnID]["BlockMarksheet"]=='true') && !$lreportcard->IS_ADMIN_USER($_SESSION['UserID']))
					    {
				        	$otherPar['disabled']='disabled';
				        	$otherPar['class']='textboxnum enableonsubmit';
					    }				 
					    $InputMethod = $lreportcard->GET_INPUT_TEXT($mark_name, $mark_id, $InputVal, '', $otherPar);
				    	//$inputmark_num++;
				    	
				    	$display .= '<td align="center" class="'.$td_css.' tabletext">'.$InputMethod.'</td>';
				    	
				    }
		        }
		        
		        
				# Get Overall Subject Mark Or Overall Term Subject Mark 
				# for InputScale is Grade or Scheme Type is PassFail
				if($overall_cnt > 0){
//					$numOfPreviousSelection = $numOfDisplayedColumn + $numOverallAdded;

					for($i=0 ; $i<count($CmpSubjectArr) ; $i++){
						$numOfPreviousSelection = $numOfDisplayedColumn + $numOverallAdded;
						$cmp_subject_id = $CmpSubjectArr[$i]['SubjectID'];
						
						if ($eRCTemplateSetting['Marksheet']['HideZeroWeightSubjectComponent']) {
				    		$report_column_id = 0;
					    	$thisSubjectColumnWeight = $WeightAry[$report_column_id][$cmp_subject_id];
					    	if ($thisSubjectColumnWeight == 0) {
					    		continue;
					    	}
					    }
						
						$classname = "";
						if(($lreportcard->Is_Marksheet_Blocked($ViewOnly) || $columnIDDataMap[$ReportColumnID]["BlockMarksheet"]=='true') && !$lreportcard->IS_ADMIN_USER($_SESSION['UserID']))
				        {
				        	$disable= " disabled  ";
				        	$classname= " enableonsubmit ";
				        }
				        else
				        {
				        	$disable= '';
				        }
						
						$classname.= " Column_0 Subject_$cmp_subject_id";
						$mark_id = "mark[$j][".($numOfPreviousSelection)."]";
						$mark_name = "overall_mark_".$cmp_subject_id."_".$StudentID;
				        $mark_tag = 'id="'.$mark_id.'" name="'.$mark_name.'" ';
				        $mark_tag .= $disabled;
				        $mark_tag .= ' onchange="jCHANGE_STATUS()"';
				        $mark_tag .= ' onpaste="isPasteContent(event, '.$j.', '.($numOfPreviousSelection).')"';
  						$mark_tag .= ' onkeyup="isPasteContent(event, '.$j.', '.($numOfPreviousSelection).')"';
 						$mark_tag .= ' onkeydown="isPasteContent(event, '.$j.', '.($numOfPreviousSelection).')"';
  						$mark_tag .= ' onfocusout="jCHANGE_STATUS()"';
 						$mark_tag .= ' class="tabletexttoptext  '.$classname.'"';
 						
 						if($SchemeType[$cmp_subject_id] == "PF"){
							$overall_value = (count($MarksheetOverallScoreInfoArr) > 0 && isset($MarksheetOverallScoreInfoArr[$cmp_subject_id]) ) ? $MarksheetOverallScoreInfoArr[$cmp_subject_id][$StudentID]['MarkNonNum'] : "";
							$display .= '<td align="center" class="'.$td_css.' tabletext">';
							$display .= $linterface->GET_SELECTION_BOX($InputPFOption[$cmp_subject_id], $mark_tag, '', $overall_value);
							$display .= '</td>';
							$numOfDisplayedColumn++;
							$MSwithSelectionBox = true;
						}
						else if($SchemeType[$cmp_subject_id] == "H" && $ScaleInput[$cmp_subject_id] == "G"){
							$overall_value = (count($MarksheetOverallScoreInfoArr) > 0 && isset($MarksheetOverallScoreInfoArr[$cmp_subject_id]) ) ? $MarksheetOverallScoreInfoArr[$cmp_subject_id][$StudentID]['MarkNonNum'] : "";
							$display .= '<td align="center" class="'.$td_css.' tabletext">';
							$display .= $linterface->GET_SELECTION_BOX($InputGradeOption[$cmp_subject_id], $mark_tag, '', $overall_value);
							$display .= '</td>';
							$numOfDisplayedColumn++;
							$MSwithSelectionBox = true;
						}
					}
				}
	         }
	         $display .= '</tr>';
        }
	    
	    $display .= '</table>';
	    
		# Button
		$display_button = '';
	    if(!$ViewOnly && count($NewReportColumn) > 0){
			$display_button .= '<input name="Save" type="button" class="formbutton" onMouseOver="this.className=\'formbuttonon\'" onMouseOut="this.className=\'formbutton\'" value="'.$button_save.'" onclick="jSUBMIT_FORM()">&nbsp;';
			$display_button .= '<input name="ClearNA" type="button" class="formbutton" onMouseOver="this.className=\'formbuttonon\'" onMouseOut="this.className=\'formbutton\'" value="'.$eReportCard['ClearAllNA'].'"  onclick="jCLEAR_ALL_NA()">&nbsp;';
	      	$display_button .= '<input name="Reset" type="reset" class="formbutton" onMouseOver="this.className=\'formbuttonon\'" onMouseOut="this.className=\'formbutton\'" value="'.$button_reset.'">&nbsp;';
      	}
        $display_button .= '<input name="Cancel" type="button" class="formbutton" onMouseOver="this.className=\'formbuttonon\'" onMouseOut="this.className=\'formbutton\'" value="'.$button_cancel.'" onClick="jBACK()">';
	    
	    $params = "ClassLevelID=$ClassLevelID&SubjectID=$SubjectID&ReportID=$ReportID&ClassID=$ClassID&SubjectGroupID=$SubjectGroupID";
	    
	    if ($eRCTemplateSetting['Marksheet']['PrintPage']) {
			$toolbarPrint = '<a href="javascript:void(0);" class="contenttool" onclick="js_Go_Print();"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_print.gif" width="18" height="18" border="0" align="absmiddle"> '.$Lang['Btn']['Print'].'</a>';
		}
	    
		############################################################################################################
		
		# Navigation
		$PAGE_NAVIGATION[] = array($eReportCard['Management_MarksheetRevision'], "javascript:jBACK()");
		$PAGE_NAVIGATION[] = array($eReportCard['CmpSubjectMarksheet'], ''); 
		$PageNavigation = $linterface->GET_NAVIGATION($PAGE_NAVIGATION);
        
?>

<script language="javascript">
var xno = "<?=$input_mark_count?>"; yno = "<?=$StudentSize?>";		// set table size
var jsDefaultPasteMethod = "text";		// for copypaste.js

<?php if(!$eRCTemplateSetting['Marksheet']['ApplyOldPasteMethod']) { ?>
	var rowx = 0, coly = 0;
	var startContent = "";
	var setStart = 1;
	<?php if($MSwithSelectionBox) { ?>
// 		var jsDefaultPasteMethod = "selectedIndex";
		var jsDefaultPasteMethod = "selectedText";
<?php }
} ?>

$(document).ready( function() {
	jQuery.excel('dataRow');
});

/* JS Form */
<?php 
	$js_special_case = '';
	if(count($possibleSpeicalCaseSet1) > 0){
		$js_special_case .= 'var specialCaseArr = new Array(';
		for($i=0 ; $i<count($possibleSpeicalCaseSet1) ; $i++)
			$js_special_case .= ($i != 0 ) ? ', "'.$possibleSpeicalCaseSet1[$i].'"' : '"'.$possibleSpeicalCaseSet1[$i].'"' ;
		$js_special_case .= ');';
	}
	echo $js_special_case."\n";
?>

function jCHANGE_STATUS(){
	document.getElementById("isOutdated").value = 1;
}

function jCHECK_SPECIAL_CASE(scVal){
	if(specialCaseArr.length > 0){
		for(var i=0 ; i<specialCaseArr.length ; i++){
			if(scVal == specialCaseArr[i])
				return true;
		}
		return false;
	}
	else {
		return false;
	}
}

function jCHECK_INPUT_MARK(xcor, ycor){
	
	var jCmpSubjectID = document.getElementById("SubjectID_Column_"+ycor).value;
	var jFullMark = document.getElementById("FullMark_"+jCmpSubjectID).value;
	
	var jScaleInput = document.getElementById("ScaleInput_"+jCmpSubjectID).value;
	var jSchemeType = document.getElementById("SchemeType_"+jCmpSubjectID).value;
	
	if(jSchemeType == "H" && jScaleInput == "M"){
		var obj = document.getElementById("mark["+xcor+"]["+ycor+"]");
		//obj.value = Trim(obj.value);
		var jsValue = $("#mark\\[" + xcor + "\\]\\[" + ycor + "\\]").val();
		jsValue = Trim(js_Remove_Estimate_Mark_Symbol(jsValue));
		
		if(obj && jsValue != ""){
			if(!jCHECK_SPECIAL_CASE(jsValue)){
				if(jsValue < 0){
					alert("<?=$eReportCard['jsInputMarkCannotBeNegative']?>");
					obj.focus();
					return false;
				}
				
//				if(!check_numeric(obj, jsValue, "<?=$eReportCard['jsInputMarkInvalid']?>")){
//					obj.focus();
//					return false;
//				}
				if (jsValue!='' && !IsNumeric(jsValue)) {
					alert('<?=$eReportCard['jsInputMarkInvalid']?>');
					obj.focus();
					return false;
				}
				
				if(parseFloat(jsValue) > parseFloat(jFullMark)){
					alert("<?=$eReportCard['jsInputMarkCannotBeLargerThanFullMark']?>");
					obj.focus();
					return false;
				}
			}
		}
	}
	
	return true;
}

// xno: ColumnSize ; yno: StudentSize
function jCHECK_FORM(){
	// check for mark when mark is used for input value
	for(var i=0; i<yno ; i++){
		for(var j=0; j<xno ; j++){
			if(!jCHECK_INPUT_MARK(i, j))
				return false;
		}
	}
	
	var cmp_subject_id = document.getElementsByName("CmpSubjectID[]");
	if(cmp_subject_id.length > 0){
		for(var i=0 ; i<cmp_subject_id.length ; i++){			
			var isComplete = document.getElementById("isCmpSubjectComplete_"+cmp_subject_id[i].value).value;
			if(isComplete == 1){
				if(!confirm("<?=$eReportCard['jsConfirmCmpSubjectMarksheetToIncomplete']?>"))
					return false;
				else 
					break;
			}
		}
	}
	
	return true;
}

function jSUBMIT_FORM(){
	var obj = document.FormMain;
	
	if(!jCHECK_FORM(obj))
		return;
	
	$(".enableonsubmit").removeAttr("disabled");
	
	obj.submit();
}

function jBACK(){
	var form_id = <?=(($ClassLevelID != "") ? $ClassLevelID : "''")?>;
	var report_id = <?=(($ReportID != "") ? $ReportID : "''")?>;
	var subject_id = <?=(($SubjectID != "") ? $SubjectID : "''")?>;
	var isProgress = document.getElementById("isProgress").value;
	var isProgressSG = document.getElementById("isProgressSG").value;
	var status = document.getElementById("isOutdated").value;
	
	if(status == 1){
		if(<?=$ViewOnly?1:0?> || !confirm("<?=$Lang['eReportCard']['ManagementArr']['MarksheetRevisionArr']['SaveBeforeLeaveWarning']?>")){
			if (isProgress == 0 && isProgressSG == 0){
				location.href = "marksheet_revision.php?ClassLevelID="+form_id+"&ReportID="+report_id+"&SubjectID="+subject_id;
			} else {
				if (isProgressSG == 1) {
					location.href = "../progress/submission_sg.php?ClassLevelID="+form_id+"&ReportID="+report_id;
				}
				else {
					location.href = "../progress/submission.php?ClassLevelID="+form_id+"&ReportID="+report_id;
				}
			}
		} else {
			document.getElementById("IsSaveBefCancel").value = 1;
			jSUBMIT_FORM();
		}
	} else {
		if(isProgress == 0 && isProgressSG == 0){
			location.href = "marksheet_revision.php?ClassLevelID="+form_id+"&ReportID="+report_id+"&SubjectID="+subject_id;
		} else {
			if (isProgressSG == 1) {
				location.href = "../progress/submission_sg.php?ClassLevelID="+form_id+"&ReportID="+report_id;
			}
			else {
				location.href = "../progress/submission.php?ClassLevelID="+form_id+"&ReportID="+report_id;
			}
		}
	}
}

function jCLEAR_ALL_NA()
{
	$("input[value='N.A.'], input[value='N.A.#']").val('');
	$("option[value='N.A.']:selected").parent().val('');
}

function js_Copy_Grade_From_Column(FromColumnID, FromSubjectID)
{
	var targetColumnID = $("select#CopyToSelection_"+FromColumnID+"_"+FromSubjectID).val();

	for(var i=0; i<$(".Column_"+FromColumnID+"&.Subject_"+FromSubjectID).length; i++)
	{
		$(".Column_"+targetColumnID+"&.Subject_"+FromSubjectID).eq(i).val($(".Column_"+FromColumnID+"&.Subject_"+FromSubjectID).eq(i).val());
	}
}

function js_Go_Print() {
	var jsOriginalAction = $('form#FormMain').attr('action');
	$('form#FormMain').attr('action', '../../print_page.php').attr('target', '_blank').submit();
	
	// restore form attribute
	$('form#FormMain').attr('action', jsOriginalAction).attr('target', '_self');
}
</script>

<br/>
<form id="FormMain" name="FormMain" method="post" action="marksheet_cmp_edit_update.php">
<table width="100%" border="0" cellpadding"0" cellspacing="0">
	<tr><td class="navigation"><?=$PageNavigation?></td></tr>
	<td align="right" valign="bottom">&nbsp;<?=$linterface->GET_SYS_MSG($msg);?></td>
</table>
<table width="100%" border="0" cellpadding="2" cellspacing="0">
  <tr>
	  <td>
	  	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	      <tr>
		      <?php if(count($NewReportColumn) > 0){ ?>
		        <td colspan="2">
		        	<a href="marksheet_cmp_export.php?<?=$params?>" class="contenttool"><img src="<?=$image_path.'/'.$LAYOUT_SKIN?>/icon_export.gif" width="18" height="18" border="0" align="absmiddle"> <?=$button_export?></a>
		        	<?=$toolbarPrint?>
		        </td>
		      <?php } ?>
	      </tr>
	    </table>
	    <br />
	  </td>
  </tr>
  <tr>
    <td align="center">
      <!--PrintStart-->
	  <table width="100%" border="0" cellspacing="0" cellpadding="2">
	  	<tr>
	      <td>
	      	<table class="form_table_v30">
	      		<tr>
					<td class="field_title"><?=$eReportCard['Settings_ReportCardTemplates']?></td>
					<td><?=$PeriodName?></td>
				</tr>
				<tr>
					<td class="field_title"><?=$SubjectOrGroupTitle?></td>
					<td><?=$SubjectOrGroupNameDisplay?></td>
				</tr>
				<tr>
					<td class="field_title"><?=$eReportCard['Subject']?></td>
					<td><?=$SubjectName?></td>
				</tr>
				<?=$SubjectTeacherTr?>
	      	</table>
	      	<br style="clear:both;" />
          </td>
	  	</tr>
	  	<tr>
		  <td>
		  	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		      <tr>
		        <td colspan="2" align="right" class="tabletextremark"><?=$eRCTemplateSetting['Marksheet']['EstimatedMark']? $eReportCard['MarkRemindSet1_WithEstimate'] : $eReportCard['MarkRemindSet1']?></td>
		      </tr>
		    </table>
		  </td>
	    </tr>
	  	<tr>
	      <td>
	      <!-- Start of Content -->
	      <?=$display?>
	      <!-- End of Content -->
	      </td>
	  	</tr>
	  </table>
	  <!--PrintEnd-->
	  
	  <br>
	  <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td height="1" class="dotline"><img src="<?=$image_path.'/'.$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
        </tr>
      	<tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="2">
              <tr>
                <td align="left" valign="bottom"><span class="tabletextremark"><?=$eReportCard['DeletedStudentLegend']?></span></td>
              </tr>
              <tr>
                <td align="center" valign="bottom"><?=$display_button?></td>
              </tr>
        	</table>
          </td>
      	</tr>
      </table>
	</td>
  </tr>
</table>

<input type="hidden" name="isProgress" id="isProgress" value="<?=$isProgress?>"/>
<input type="hidden" name="isProgressSG" id="isProgressSG" value="<?=$isProgressSG?>"/>
<input type="hidden" name="ClassLevelID" id="ClassLevelID" value="<?=$ClassLevelID?>"/>
<input type="hidden" name="ReportID" id="ReportID" value="<?=$ReportID?>"/>
<input type="hidden" name="ClassID" id="ClassID" value="<?=$ClassID?>"/>
<input type="hidden" name="SubjectGroupID" id="SubjectGroupID" value="<?=$SubjectGroupID?>"/>

<input type="hidden" name="isOutdated" id="isOutdated" value="0"/>
<input type="hidden" name="IsSaveBefCancel" id="IsSaveBefCancel" value="1"/>

<?php
echo '<input type="hidden" name="SubjectID" id="SubjectID" value="'.$SubjectID.'"/>'."\n";

if(count($column_data) > 0){
	for($i=0 ; $i<count($column_data) ; $i++){
		if($ReportType != "F" || ($ReportType == "F" && !$ColumnHasTemplateAry[$column_data[$i]['ReportColumnID']]) ){
			echo '<input type="hidden" name="ReportColumnID[]" id="ReportColumnID_'.$i.'" value="'.$column_data[$i]['ReportColumnID'].'"/>'."\n";
			
			# Format of Columen Weight ID: ColumnWeight_[ColumnID]_[CmpSubjectID]
			for($j=0 ; $j<count($CmpSubjectIDArr) ; $j++){
				echo '<input type="hidden" id="ColumnWeight_'.$column_data[$i]['ReportColumnID'].'_'.$CmpSubjectIDArr[$j].'" name="ColumnWeight_'.$column_data[$i]['ReportColumnID'].'_'.$CmpSubjectIDArr[$j].'" value="'.$WeightAry[$ReportColumnID][$CmpSubjectIDArr[$j]].'"/>'."\n";  
			}
		}
	}
}
	            
if(count($CmpSubjectIDArr) > 0){
	for($i=0 ; $i<count($CmpSubjectIDArr) ; $i++){
		echo '<input type="hidden" name="CmpSubjectID[]" id="CmpSubjectID_'.$i.'" value="'.$CmpSubjectIDArr[$i].'"/>'."\n";
	}
	
	if(count($SchemeFullMark) > 0){
		for($i=0 ; $i<count($CmpSubjectIDArr) ; $i++){
			echo '<input type="hidden" name="FullMark_'.$CmpSubjectIDArr[$i].'" id="FullMark_'.$CmpSubjectIDArr[$i].'" value="'.$SchemeFullMark[$CmpSubjectIDArr[$i]].'"/>'."\n";
			echo '<input type="hidden" name="ScaleInput_'.$CmpSubjectIDArr[$i].'" id="ScaleInput_'.$CmpSubjectIDArr[$i].'" value="'.$ScaleInput[$CmpSubjectIDArr[$i]].'"/>'."\n";
			echo '<input type="hidden" name="SchemeType_'.$CmpSubjectIDArr[$i].'" id="SchemeType_'.$CmpSubjectIDArr[$i].'" value="'.$SchemeType[$CmpSubjectIDArr[$i]].'"/>'."\n";
		}
	}
	
	if(count($ProgressArr) > 0){
		for($i=0 ; $i<count($CmpSubjectIDArr) ; $i++){
			$isCmpSubjectComplete = (count($ProgressArr) > 0 && isset($ProgressArr[$CmpSubjectIDArr[$i]]) ) ? $ProgressArr[$CmpSubjectIDArr[$i]]['IsCompleted'] : 0;
			echo '<input type="hidden" name="isCmpSubjectComplete_'.$CmpSubjectIDArr[$i].'" id="isCmpSubjectComplete_'.$CmpSubjectIDArr[$i].'" value="'.$isCmpSubjectComplete.'"/>'."\n";
		}
	}
}
?>
<input type="hidden" name="SubjectID" value="<?=$SubjectID?>">

<textarea style="height: 1px; width: 1px; visibility:hidden;" id="text1" name="text1"></textarea>

</form>


<?

        $linterface->LAYOUT_STOP();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>