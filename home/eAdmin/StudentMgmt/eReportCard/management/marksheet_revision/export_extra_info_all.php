<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");

$lreportcard = new libreportcardcustom();
$lreportcard->hasAccessRight();


if (!isset($_GET) || !isset($_GET['ReportID'])) {
	header("Location: index.php");
	exit();
}

$reportId = integerSafe(standardizeFormGetValue($_GET['ReportID']));
$type = standardizeFormGetValue($_GET['type']);


$reportInfoAry = $lreportcard->returnReportTemplateBasicInfo($reportId);
$classlevelId = $reportInfoAry['ClassLevelID'];
$yearTermId = $reportInfoAry['Semester'];
		
		
if ($eRCTemplateSetting['Marksheet']['ManualInputGrade']) {
	$column_data = $lreportcard->returnReportTemplateColumnData($reportId);
	$dataTitle = $column_data[0]['ColumnTitle'].' Grade';
}
else {
	$dataTitle = $eReportCard['ExtraInfoLabel'];
}


### get related subjects
if ($lreportcard->IS_ADMIN_USER_VIEW($_SESSION['UserID'])) {
	$checkPermission = false;
}
else {
	$checkPermission = true;
	
}
$subjectAry = $lreportcard->returnSubjectwOrder($classlevelId, $ParForSelection=0, $_SESSION['UserID'], $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $reportId, $checkPermission);
$subjectIdAry = array();
foreach ((array)$subjectAry as $_parentSubjectId => $_cmpSubjectAry) {
	$_cmpSubjectIdAry = array_keys($_cmpSubjectAry);
	$_numOfCmp = count($_cmpSubjectIdAry);
	for ($i=0; $i<$_numOfCmp; $i++) {
		$__cmpSubjectId = $_cmpSubjectIdAry[$i];
		
		if ($__cmpSubjectId == 0) {
			$subjectIdAry[] = $_parentSubjectId;
		}
		else {
			$subjectIdAry[] = $__cmpSubjectId;
		}
	}
}
$numOfSubject = count($subjectIdAry);

if ($lreportcard->IS_ADMIN_USER_VIEW($_SESSION['UserID'])) {
	// do nth
}
else {
	$teachingSubjectGroupIdAry = Get_Array_By_Key($lreportcard->Get_Teacher_Related_Subject_Groups_By_Subjects($yearTermId, $classlevelId, $_SESSION['UserID'], $subjectIdAry), 'SubjectGroupID');
	$teachingClassIdAry = Get_Array_By_Key($lreportcard->GET_TEACHING_CLASS(), 'YearClassID');
}


### get related subject groups
$scm = new subject_class_mapping();
$subjectGroupAry = $scm->Get_Subject_Group_List($yearTermId, '', $returnAsso=false);
$subjectGroupIdAry = Get_Array_By_Key($subjectGroupAry, 'SubjectGroupID');
$subjectGroupAssoAry = BuildMultiKeyAssoc($subjectGroupAry, 'SubjectID', '', $SingleValue=0, $BuildNumericArray=1);
unset($subjectGroupAry);


### get students
$studentAry = $lreportcard->GET_STUDENT_BY_SUBJECT_GROUP($subjectGroupIdAry, $classlevelId);
$studentIdAry = array_values(array_unique(Get_Array_By_Key($studentAry, 'UserID')));
$studentAssoAry = BuildMultiKeyAssoc($studentAry, 'SubjectGroupID', '', $SingleValue=0, $BuildNumericArray=1);
unset($studentAry);


### get extra info data
if ($type=="AdjustPosition") {
	$table = $lreportcard->DBName.".RC_REPORT_RESULT_SCORE_ARCHIVE";
	$sql = "SELECT StudentID, SubjectID, Info FROM $table Where ReportColumnID = 0 And ReportID = '".$reportId."' And SubjectID IN ('".implode("','", (array)$subjectIdAry)."') And StudentID IN ('".implode("','", (array)$studentIdAry)."')";
}
else {
	# Get Extra Info and create an associated array
	$table = $lreportcard->DBName.".RC_EXTRA_SUBJECT_INFO";
	$sql = "SELECT StudentID, SubjectID, Info FROM $table WHERE ReportID = '".$reportId."' And SubjectID IN ('".implode("','", (array)$subjectIdAry)."') And StudentID IN ('".implode("','", (array)$studentIdAry)."')";
}
$extraInfoAssoAry = BuildMultiKeyAssoc($lreportcard->returnResultSet($sql), array('StudentID', 'SubjectID'), 'Info');


### consolidate data
$dataAry = array();
$counter = 0;
for ($i=0; $i<$numOfSubject; $i++) {
	$_subjectId = $subjectIdAry[$i];
	
	$_subjectGroupAry = (array)$subjectGroupAssoAry[$_subjectId];
	$_numOfSubjectGroup = count($_subjectGroupAry);
	
	for ($j=0; $j<$_numOfSubjectGroup; $j++) {
		$__subjectGroupId = $_subjectGroupAry[$j]['SubjectGroupID'];
		$__subjectGroupCode = $_subjectGroupAry[$j]['ClassCode'];
		$__subjectGroupName = $_subjectGroupAry[$j]['ClassTitleEN'];
		
		$__studentAry = (array)$studentAssoAry[$__subjectGroupId];
		$__numOfStudent = count($__studentAry);
		for ($k=0; $k<$__numOfStudent; $k++) {
			$___studentId = $__studentAry[$k]['UserID'];
			$___studentName = strip_tags($__studentAry[$k]['StudentName']);
			$___webSAMSRegNo = $__studentAry[$k]['WebSAMSRegNo'];
			$___classTitleEn = $__studentAry[$k]['ClassTitleEn'];
			$___classNumber = $__studentAry[$k]['ClassNumber'];
			$___yearClassId = $__studentAry[$k]['YearClassID'];
			
			if ($lreportcard->IS_ADMIN_USER_VIEW($_SESSION['UserID'])) {
				// do nth
			}
			else {
				if (!in_array($__subjectGroupId, $teachingSubjectGroupIdAry) && !in_array($___yearClassId, $teachingClassIdAry)) {
					// not subject group teacher and not class teacher of the student => skip student
					continue;
				}
			}
			
			if ($isTemplate) {
				$___extraInfo = '';
			}
			else {
				$___extraInfo = $extraInfoAssoAry[$___studentId][$_subjectId]['Info'];
			}
			
			$dataAry[$counter][] = $___webSAMSRegNo;
			$dataAry[$counter][] = $___classTitleEn;
			$dataAry[$counter][] = $___classNumber;
			$dataAry[$counter][] = $___studentName;
			$dataAry[$counter][] = $__subjectGroupCode;
			$dataAry[$counter][] = $__subjectGroupName;
			$dataAry[$counter][] = $___extraInfo;
			$counter++;
		}
	}
}

$lexport = new libexporttext();
if ($type=="AdjustPosition") {
	$filename = "position_all_".date('Ymd_His').".csv";
	$headerAry = array("WebSAMSRegNumber", "Class Name", "Class Number", "Student Name", "Subject Group Code", "Subject Group Name", $eReportCard['AdjustPositionLabal']);
}
else {
	$dataTitleFileName = str_replace(' ', '_', $dataTitle);
	$filename = $dataTitleFileName."_all_".date('Ymd_His').".csv";
	$headerAry = array("WebSAMSRegNumber", "Class Name", "Class Number", "Student Name", "Subject Group Code", "Subject Group Name", $dataTitle);
}
$exportContent = $lexport->GET_EXPORT_TXT($dataAry, $headerAry);

intranet_closedb();

$lexport->EXPORT_FILE($filename, $exportContent);
?>