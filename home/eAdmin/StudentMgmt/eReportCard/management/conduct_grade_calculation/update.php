<?php
// Using :

/**************************************************************
 *	Modification log:
 *  20200428 Bill:  [2019-0924-1029-57289]
 *      - Create File   ($eRCTemplateSetting['Management']['ConductGradeCalculation'])
 ***************************************************************/

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = array("ADMIN", "VIEW_GROUP");

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if (!isset($_POST) || $ReportID == '' || $ClassID == '' || empty($modifyGradeArr)) {
    header("Location:index.php");
    exit();
}

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");

if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") {
    include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
    $lreportcard = new libreportcardcustom();
}
else {
    $lreportcard = new libreportcard();
}

if (!$eRCTemplateSetting['Management']['ConductGradeCalculation'] || !$lreportcard->hasAccessRight()) {
    intranet_closedb();
    header("Location:/index.php");
    exit();
}

$ReportID = IntegerSafe($ReportID);
$ClassID = IntegerSafe($ClassID);
$reportTermID = ($TermID == '' || $TermID == 0) ? 'F' : IntegerSafe($TermID);

// Get class students
$studentArr = $lreportcard->GET_STUDENT_BY_CLASS($ClassID);
$studentIDArr = Get_Array_By_Key($studentArr, 'UserID');

// loop modified grades
foreach($modifyGradeArr as $thisStudentID => $thisModifyGrade) {
    if(in_array($thisStudentID, (array)$studentIDArr)) {
        $lreportcard->Adjust_Student_Conduct_Grade($ReportID, $thisStudentID, trim($thisModifyGrade));
    }
}

intranet_closedb();

header("Location: index.php?FormID=$FormID&TermID=$TermID&msg=AddSuccess");
?>