<?php
// Editing by 
/*
 * 	Date: 2017-05-19 (Bill)
 * 		- Allow View Group to access
 * 	Date: 2017-04-19 (Bill)		[2017-0109-1818-40164]
 * 		- Create File
 */

@SET_TIME_LIMIT(1000);
@ini_set(memory_limit, -1);
 
$PageRight = array("ADMIN", "VIEW_GROUP");

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if ($plugin['ReportCard2008'])
{
	# Initial
	include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
	if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
	{
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
		$lreportcard = new libreportcardcustom();
	}
	else {
		$lreportcard = new libreportcard();
	}
	
	if ($lreportcard->hasAccessRight() && $eRCTemplateSetting['ReportGeneration']['PromoteRetainQuit'] && isset($_POST))
	{
		// Initial Last Year Object
		$LastYearID = $lreportcard->Get_Previous_YearID_By_Active_Year();
		if($LastYearID > 0)
			$lreportcardObj = new libreportcard($LastYearID);
		
		// Inital
		$fcm = new form_class_manage();
		
		// Get Promotion Status
		$promotionStatusAry = $lreportcard->customizedPromotionStatus;
		
		# Get Export Data
		$PromoteRetentRecords = $lreportcard->GET_PROMOTION_RETENTION_SUMMARY_RECORDS($_POST["ClassID"], $_POST["PromotionStatus"], $_POST["Keyword"]);
		$relatedStudentIDAry = Get_Array_By_Key((array)$PromoteRetentRecords, "StudentID");

		# Get Retented Subjects
		$retentedSubjectAry = $lreportcard->GET_STUDENT_PAST_YEAR_PROMOTION_RETENTION_SUBJECT_RECORD($relatedStudentIDAry, "", "2");
		$allRetentedSubjectIds = BuildMultiKeyAssoc((array)$retentedSubjectAry, array("StudentID", "SubjectID"), "SubjectID", 1);

		# Get Last Year Student Promotion Status
		if($LastYearID > 0)
		{
			$lastYearPromotionStatusAry = array();
			$lastYearStudentReportMapping = array(); 
			$sql = "SELECT
						report_result.ReportID, report_template.ClassLevelID, report_result.StudentID
					FROM
						".$lreportcardObj->DBName.".RC_REPORT_RESULT report_result
						INNER JOIN ".$lreportcardObj->DBName.".RC_REPORT_TEMPLATE report_template ON (report_result.ReportID = report_template.ReportID)
					WHERE
						report_result.StudentID IN ('".implode("', '", (array)$relatedStudentIDAry)."') AND report_result.ReportColumnID = '0' AND report_result.Semester = 'F' AND report_template.Semester = 'F'";
			$LastYearStudentReportAry = $lreportcard->returnArray($sql);
			if(!empty($LastYearStudentReportAry))
			{
				foreach($LastYearStudentReportAry as $thisStudentReportInfo) {
					$thisStudentID = $thisStudentReportInfo["StudentID"];
					$thisStudentReportID = $thisStudentReportInfo["ReportID"];
					$thisStudentClassLevelID = $thisStudentReportInfo["ClassLevelID"];
					
					// Last Year Status
					if(empty($lastYearPromotionStatusAry[$thisStudentReportID])) {
						$lastYearPromotionStatusAry[$thisStudentReportID] = $lreportcardObj->GetPromotionStatusList($thisStudentClassLevelID, "", "", $thisStudentReportID);
					}
					$lastYearStudentReportMapping[$thisStudentID] = $thisStudentReportID;
				}
			}
		}
		
		# Create the data array for export
		$ExportArr = array();
		for($i=0; $i<sizeof((array)$PromoteRetentRecords); $i++)
		{
			// Get Student Records
			$thisRecord = $PromoteRetentRecords[$i];
			$thisStudentID = $thisRecord["StudentID"];
			$thisStudentName = $thisRecord["StudentNameB5"];
			$thisClassName = $thisRecord["ClassNameEN"];
			$thisClassNumber = $thisRecord["ClassNumber"];
			$thisClassNumber = $thisClassNumber>=10? $thisClassNumber : "0$thisClassNumber";
			$thisPromotionStatus = $thisRecord["PromotionStatus"];
			$thisFailSubjectMark = $thisRecord["SubjectAndMark"];
			
			// Get Retented Subject Display
			$thisRetentedSubjDisplay = "";
			$studentRetentedSubjectIds = $allRetentedSubjectIds[$thisStudentID];
			foreach((array)$studentRetentedSubjectIds as $thisSubjectID) {
				if($thisSubjectID > 0) {
					$thisRetentedSubjDisplay .= $lreportcard->GET_SUBJECT_NAME_LANG($thisSubjectID, 'b5', true, true);
				}
			}
			
			if($LastYearID > 0)
			{
				// Check if New Student
				$LastYearClassInfo = $fcm->Get_Student_Class_Info_In_AcademicYear($thisStudentID, $LastYearID);
				$LastYearLevelID = $LastYearClassInfo[0]["YearID"];
//				if($LastYearLevelID > 0) {
//					$LastYearSchoolType	= $lreportcard->GET_SCHOOL_TYPE_BY_CLASSLEVEL($LastYearLevelID);
//				}
//				$ActiveYearClassInfo = $fcm->Get_Student_Class_Info_In_AcademicYear($thisStudentID, $lreportcard->schoolYearID);
//				$ActiveYearLevelID = $ActiveYearClassInfo[0]["YearID"];
//				if($ActiveYearLevelID > 0){
//					$ActiveYearSchoolType = $lreportcard->GET_SCHOOL_TYPE_BY_CLASSLEVEL($ActiveYearLevelID);
//				}
				$isNewStudent = empty($LastYearLevelID);
				
				// Check if retented last year
				$LastYearPromotionStatus = "";
				if(!$isNewStudent)
				{
					$LastYearReportID = $lastYearStudentReportMapping[$thisStudentID];
					if($LastYearReportID > 0) {
						$LastYearPromotionStatus = $lastYearPromotionStatusAry[$LastYearReportID][$thisStudentID]["Promotion"];
					}
				}
				$isStudentRetented = !$isNewStudent && $LastYearReportID > 0 && $LastYearPromotionStatus == $promotionStatusAry['Retained'];
			}
			
			// Get Student Status
			$thisStudentStatus = "";
			$thisStudentStatus .= $isNewStudent? "☆" : "";
			$thisStudentStatus .= $isStudentRetented? "△" : "";
			
			$ExportArr[$i][0] = $thisClassName.$thisClassNumber;
			$ExportArr[$i][1] = $thisStudentName;
			$ExportArr[$i][2] = $thisStudentStatus;
			$ExportArr[$i][3] = $thisFailSubjectMark;
			$ExportArr[$i][4] = $thisRetentedSubjDisplay;
			$ExportArr[$i][5] = $thisPromotionStatus;
		}
		if(empty($ExportArr)) {
			$ExportArr[] = array("");
		}
		
		# Export Header
		$ExportHeader = array("tclassno", "tname", "promo", "reexam", "predata", "promotion");
		
		# Initial
		$lexport = new libexporttext();
		$export_content = $lexport->GET_EXPORT_TXT($ExportArr, $ExportHeader);
		
		intranet_closedb();
		
		// Output File
		$filename = "promote_retent_info.csv";
		$lexport->EXPORT_FILE($filename, $export_content);
	}
	else {
		echo "You have no priviledge to access this page.";
	}
}
else {
	echo "You have no priviledge to access this page.";
}
?>