<?php
//Editing by 
/*
 * 2017-05-19 Bill: Allow View Group to access
 * 2013-04-30 Carlos: Created
 */
 
$PATH_WRT_ROOT = "../../../../../";
$PageRight = array("ADMIN", "VIEW_GROUP");

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_cust.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if (!$plugin['ReportCard2008'] || !$eRCTemplateSetting['PromoteRetainQuit'])
{
	header ("Location: /");
	intranet_closedb();
	exit();
}

if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
{
	include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
	$lreportcard = new libreportcard2008_cust();
} else {
	$lreportcard = new libreportcard();
}

if (!$lreportcard->hasAccessRight()) 
{
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lreportcard_ui = new libreportcard_ui();
$linterface = new interface_html();

$CurrentPage = "Management_PromotionRetention";
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();


# ClassLevelID Selection box
$FormArr = $lreportcard->GET_ALL_FORMS(1);
for($i=0; $i<sizeof($FormArr); $i++) {
	$classLevelName[$FormArr[$i]["ClassLevelID"]] = $FormArr[$i]["LevelName"];
}
$AllFormOption = array(
					0 => array(
						0 => "", 
						1 => $eReportCard['AllForms']
					)
				);
$FormArr = array_merge($AllFormOption, $FormArr);

# Filters - By Form (ClassLevelID)
$FormSelection = $linterface->GET_SELECTION_BOX($FormArr, 'id="ClassLevelID" name="ClassLevelID" onChange="jsReloadTable();"', "", $ClassLevelID);

$display = $lreportcard->GET_PROMOTION_RETENTION_FORM_REPORT_TABLE($ClassLevelID);


# tag information
$TAGS_OBJ[] = array($eReportCard['MakeupExam']['Generate'], $PATH_WRT_ROOT."home/admin/reportcard2008/management/promotion_retention/generate.php?clearCoo=1", 1);
$TAGS_OBJ[] = array($eReportCard['MakeupExam']['Manage'], $PATH_WRT_ROOT."home/admin/reportcard2008/management/promotion_retention/manage.php?clearCoo=1", 0);
$linterface->LAYOUT_START();

?>
<script type="text/javascript" language="Javascript">
function jsReloadTable()
{
	Block_Element('DivTable');
	$('#DivMainTable').load(
		'ajax_task.php',
		{
			task:'GET_PROMOTION_RETENTION_FORM_REPORT_TABLE',
			ClassLevelID:$('#ClassLevelID').val()
		},
		function(data){
			UnBlock_Element('DivTable');
		}
	);
}

function jsGenerateRecord(ReportID,ClassLevelID)
{
	if(confirm('<?=$eReportCard['PromoteRetainQuit']['jsWarningArr']['ConfirmGeneration']?>')) {
		Block_Element('DivTable');
		$.post(
			'ajax_task.php',
			{
				task:'GENERATE_PROMOTION_RETENTION_STUDENT_RECORDS',
				ReportID: ReportID,
				ClassLevelID: ClassLevelID
			},
			function(ReturnMsg){
				Get_Return_Message(ReturnMsg);
				jsReloadTable();
				Scroll_To_Top();
			}
		);
	}
}

</script>
<br />
<!-- Start Main Table //-->
<table width="96%" border="0" cellspacing="0" cellpadding="5">
  <tr> 
    <td align="center" colspan="2"><table width="100%" border="0" cellspacing="0" cellpadding="2">
      <tr>
        <td align="left" valign="top">
        	<div id="DivTable">
	        <table width="100%" border="0" cellspacing="0" cellpadding="0">
	          <tr>
	            <td align="right"><table width="100%" border="0" cellspacing="0" cellpadding="0">
	                
	                <tr>
	                  <td align="right" class="tabletextremark">
	                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
	                      <tr>
	                          <td><table border="0" cellspacing="0" cellpadding="2">
	                              <tr>
	                                <td><?=$FormSelection?></td>
	                              </tr>
	                          </table></td>
	                        <td align="right" valign="bottom"><?=$linterface->GET_SYS_MSG($msg);?></td>
	                      </tr>
	                  </table></td>
	                </tr>
	            </table></td>
	          </tr>
	          <tr>
	            <td>
	            	<div id="DivMainTable"><?=$display?></div>
	            </td>
	          </tr>
	          <tr>
	            <td height="1" class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
	          </tr>
	        </table>
	        </div>
          </td>
      </tr>
    </table>
    </td>
  </tr>
</table>
<!-- End Main Table //-->
<br />
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>