<?php
//Editing by 
/*
 * 2013-04-30 Carlos: Created
 */
$PATH_WRT_ROOT = "../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_cust.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if (!$plugin['ReportCard2008'] || !$eRCTemplateSetting['PromoteRetainQuit'])
{
	header ("Location: /");
	intranet_closedb();
	exit();
}

if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
{
	include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
	$lreportcard = new libreportcard2008_cust();
} else {
	$lreportcard = new libreportcard();
}

if (!$lreportcard->hasAccessRight()) 
{
	header ("Location: /");
	intranet_closedb();
	exit();
}

$SubjectID = $_POST['SubjectID'];
$ReportID = $_POST['ReportID'];
$StudentID = $_POST['StudentID'];
$PromotionStatus = $_POST['PromotionStatus'];
$SubjectID = $_POST['SubjectID'];
$ClassLevelID = $_POST['ClassLevelID'];

if($ReportID == '' || count($StudentID)==0 || $PromotionStatus == '') {
	header("Location: manage.php?msg=update_failed");
	intranet_closedb();
	exit();
}

$existRecords = $lreportcard->GET_PROMOTION_RETENTION_STUDENT_RECORDS($ReportID,'',$StudentID,array('1'));
$existStudents = Get_Array_By_Key($existRecords,'StudentID');

if($PromotionStatus=='1' || $PromotionStatus=='2') {
	// Get mark data
	$marksAry = $lreportcard->getMarksCommonIpEj($ReportID);
	//$marksNatureArr = $lreportcard->Get_Student_Subject_Mark_Nature_Arr($ReportID, $ClassLevelID, $marksAry);
}

$dataAry = array();
for($i=0;$i<count($StudentID);$i++) {
	if(!in_array($StudentID[$i],$existStudents)) {
		$tmpAry = array('ReportID' => $ReportID, 'StudentID' => $StudentID[$i], 'PromotionStatus'=>$PromotionStatus, 'RecordType'=>2);
		if($PromotionStatus == '1' || $PromotionStatus == '2') {
			$mark_ary = array();
			$grade_ary = array();
			$subject_type_ary = array();
			for($j=0;$j<count($SubjectID);$j++){
				if(isset($marksAry[$StudentID[$i]]) && isset($marksAry[$StudentID[$i]][$SubjectID[$j]])) {
					$mark_ary[] = $marksAry[$StudentID[$i]][$SubjectID[$j]][0]['Mark'];
					$grade_ary[] = $marksAry[$StudentID[$i]][$SubjectID[$j]][0]['Grade'];
					$subject_type_ary[] = '1';
				}else{
					$mark_ary[] = '';
					$grade_ary[] = '';
					$subject_type_ary[] = '1';
				}
			}
			$tmpAry['SubjectID'] = implode(",",$SubjectID);
			$tmpAry['Mark'] = implode(",",$mark_ary);
			$tmpAry['Grade'] = implode(",",$grade_ary);
			$tmpAry['SubjectType'] = implode(",",$subject_type_ary);
		}
		$dataAry[] = $tmpAry;
	}
}

//debug_r($_REQUEST);
//debug_r($dataAry);
//intranet_closedb();
//exit;

$success = $lreportcard->INSERT_PROMOTION_RETENTION_STUDENT_RECORD($dataAry);

header("Location: manage.php?msg=".($success?"update":"update_failed"));
intranet_closedb();
?>