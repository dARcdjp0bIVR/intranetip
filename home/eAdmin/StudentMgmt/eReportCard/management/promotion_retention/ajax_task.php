<?php
// Editing by 
/*
 * 	Date: 2017-05-19 (Bill)
 * 		- Allow View Group to access
 * 	Date: 2017-03-10 (Bill)		[2017-0109-1818-40164]
 * 		- Copy from EJ
 */

$PageRight = array("ADMIN", "VIEW_GROUP");

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

# Access Checking
if (!$plugin['ReportCard2008'] || !$eRCTemplateSetting['ReportGeneration']['PromoteRetainQuit'])
{
	header ("Location: /");
	intranet_closedb();
	exit();
}

# Initial
if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
{
	include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
	$lreportcard = new libreportcardcustom();
}
else
{
	$lreportcard = new libreportcard();
}

# Access Checking
if (!$lreportcard->hasAccessRight()) 
{
	header ("Location: /");
	intranet_closedb();
	exit();
}

# Initial
$linterface = new interface_html();

# AJAX Task
$task = $_REQUEST['task'];
switch($task)
{
	case "GET_PROMOTION_RETENTION_FORM_REPORT_TABLE": 
		$ClassLevelID = $_REQUEST['ClassLevelID'];
		echo $lreportcard->GET_PROMOTION_RETENTION_FORM_REPORT_TABLE($ClassLevelID);
	break;
	
	case "GENERATE_PROMOTION_RETENTION_STUDENT_RECORDS":
		$ReportID = $_REQUEST['ReportID'];
		$ClassLevelID = $_REQUEST['ClassLevelID'];
		$success = $lreportcard->GENERATE_PROMOTION_RETENTION_STUDENT_RECORDS($ReportID,$ClassLevelID);
		echo $success? $Lang['General']['ReturnMessage']['UpdateSuccess'] : $Lang['General']['ReturnMessage']['UpdateUnsuccess'];
	break;
	
	case "DELETE_PROMOTION_RETENTION_STUDENT_RECORD":
		$RecordID = (array)$_REQUEST['RecordID'];
		$DeleteAry = array();
		for($i=0;$i<count($RecordID);$i++) {
			$DeleteAry[] = array('RecordID'=>$RecordID[$i]);
		}
		$success = $lreportcard->DELETE_PROMOTION_RETENTION_STUDENT_RECORD($DeleteAry);
		echo $success? $Lang['General']['ReturnMessage']['DeleteSuccess'] : $Lang['General']['ReturnMessage']['DeleteUnsuccess'];
	break;
	
	case "GET_PROMOTION_RETENTION_MANAGE_TABLE":
		//include_once($PATH_WRT_ROOT."includes/libdbtable.php");
		//include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
	
		$ClassID = $_REQUEST['ClassID'];
		$PromotionStatus = $_REQUEST['PromotionStatus'];
		
		$field = trim(urldecode(stripslashes($_REQUEST['field'])));
		$order = trim(urldecode(stripslashes($_REQUEST['order'])));
		$pageNo = trim(urldecode(stripslashes($_REQUEST['pageNo'])));
		$numPerPage = trim(urldecode(stripslashes($_REQUEST['numPerPage'])));
		//$Keyword = convert2unicode(trim(urldecode(stripslashes($_REQUEST['Keyword']))),1,0);
		
		$pageSizeChangeEnabled = true;
		if (isset($ck_reportcard_promotion_retention_page_field) && $ck_reportcard_promotion_retention_page_field != "" && $field == "")
		{
			$field = $ck_reportcard_promotion_retention_page_field;
		}
		if (isset($ck_reportcard_promotion_retention_page_order) && $ck_reportcard_promotion_retention_page_order != "" && $order == "")
		{
			$order = $ck_reportcard_promotion_retention_page_order;
		}
		if (isset($ck_reportcard_promotion_retention_page_number) && $ck_reportcard_promotion_retention_page_number != "" && $pageNo == "")
		{
			$pageNo = $ck_reportcard_promotion_retention_page_number;
		}
		if (isset($ck_reportcard_promotion_retention_page_size) && $ck_reportcard_promotion_retention_page_size != "" && $numPerPage == "")
		{
			$page_size = $ck_reportcard_promotion_retention_page_size;
			$numPerPage = $page_size;
		}
		$field = ($field=='')? 0 : $field;
		$order = ($order=='')? 1 : $order;
		$pageNo = ($pageNo=='')? 1 : $pageNo; 
		$numPerPage = ($numPerPage=='')? 50 : $numPerPage;
		
		$arrCookies = array();
		$arrCookies[] = array("ck_reportcard_promotion_retention_page_size", "numPerPage");
		$arrCookies[] = array("ck_reportcard_promotion_retention_page_number", "pageNo");
		$arrCookies[] = array("ck_reportcard_promotion_retention_page_order", "order");
		$arrCookies[] = array("ck_reportcard_promotion_retention_page_field", "field");	
		$arrCookies[] = array("ck_reportcard_promotion_retention_page_keyword", "Keyword");
		updateGetCookies($arrCookies);
		
		echo $lreportcard->GET_PROMOTION_RETENTION_MANAGE_TABLE($ClassID, $PromotionStatus, $Keyword, $field, $order, $pageNo, $numPerPage);
	break;
}

intranet_closedb();
?>