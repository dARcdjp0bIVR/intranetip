<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "TEACHER";
/**
*	20140801 Ryan
* 		- Hide CIP NAPFA
**/
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");

intranet_auth();
intranet_opendb();

if ($plugin['ReportCard2008']) {
	$lreportcard = new libreportcard();
	
	if ($lreportcard->hasAccessRight()) {
		$limport = new libimporttext();
		
		# 20140722 SIS HIDE CIP NAPFA

		$format_array[] = 'WebSAMSRegNumber';
		$format_array[] = 'ClassName';
		$format_array[] = 'ClassNumber';
		$format_array[] = 'StudentName';
		if(!$eRCTemplateSetting['OtherStudentInfo']['HideCIP']){
			$format_array[] ='CipHours';
		}
		$format_array[] = 'Attendance';
		$format_array[] = 'Conduct';
		if(!$eRCTemplateSetting['OtherStudentInfo']['HideNAPFA']){
			$format_array[] = 'NAPFA';
		}
		$format_array[] = 'CCARemarks';
//		$format_array = array("WebSAMSRegNumber","ClassName","ClassNumber","StudentName","CipHours","Attendance","Conduct","NAPFA","CCARemarks");
		
		$li = new libdb();
		$filepath = $userfile;
		$filename = $userfile_name;
		
		# 3 parameters need to be passed to other pages
		$params = "ClassID=$ClassID&ReportID=$ReportID&ClassLevelID=$ClassLevelID";
		
		if($filepath=="none" || $filepath == "" || !isset($ReportID, $ClassID)){          # import failed
		    header("Location: import.php?$params&Result=import_failed2");
		    exit();
		} else {
			if($limport->CHECK_FILE_EXT($filename)) {
				# read file into array
				# return 0 if fail, return csv array if success
				$data = $limport->GET_IMPORT_TXT($filepath);
				#$toprow = array_shift($data);                   # drop the title bar
				
				$limport->SET_CORE_HEADER($format_array);
				if (!$limport->VALIDATE_HEADER($data, true) || !$limport->VALIDATE_DATA($data)) {
					header("Location: import.php?$params&Result=import_failed");
					exit();
				}
				
				# Get Class
				$ClassArr = $lreportcard->GET_CLASSES_BY_FORM($ClassLevelID);
				for($i=0 ; $i<count($ClassArr) ;$i++) {
					if($ClassArr[$i][0] == $ClassID)
						$ClassName = $ClassArr[$i][1];
				}
				
				# Get Student Info
				$StudentArr = $lreportcard->GET_STUDENT_BY_CLASS($ClassID);
				$StudentSize = sizeof($StudentArr);
				
				
				if ($StudentSize == 0) {
					header("Location: import.php?$params&Result=import_failed");
					exit();
				}
				
				$StudentIDList = "";
				$StudentIDArray = array();
				$StudentWebSAMSRegNoArr = array();
				for($i=0; $i<$StudentSize; $i++){
					$StudentIDArray[] = $StudentArr[$i][0];
					$StudentWebSAMSRegNoArr[] = $StudentArr[$i]['WebSAMSRegNo'];
				}
				$StudentIDList = implode(",", $StudentIDArray);
				
				
				# Get a list of exist Other Student Info Records
				$table = $lreportcard->DBName.".RC_OTHER_STUDENT_INFO";
				$sql = "SELECT StudentID FROM $table WHERE 
						ReportID = '$ReportID' AND 
						StudentID IN ($StudentIDList)";
				$existStudents = $lreportcard->returnVector($sql);
				
				
				# build an array mapping ClassNumber to UserID in the class
				$sql = "SELECT UserID, ClassNumber FROM INTRANET_USER ";
				$sql .= "WHERE UserID IN ($StudentIDList) ";
				$sql .= "ORDER BY ClassNumber";
				$tempResult = $lreportcard->returnArray($sql);
				for($i=0; $i<sizeof($tempResult); $i++) {
					$classNumMap[(int)$tempResult[$i]["ClassNumber"]] = $tempResult[$i]["UserID"];
				}
				
			
				array_shift($data);		// shift to 2nd row 
			    $values = "";
			    $delim = "";
			    for ($i=0; $i<sizeof($data); $i++) {
					if (empty($data[$i]))
						continue;
					# Get Row Data From CSV file
					switch(1){
						case ($eRCTemplateSetting['OtherStudentInfo']['HideCIP']):
						list($webSAMSRegNumber,$className,$classNum,$studentName,$attendance,$conduct,$napfa, $cca) = $data[$i];
						break;
						case ($eRCTemplateSetting['OtherStudentInfo']['HideNAPFA']):
						list($webSAMSRegNumber,$className,$classNum,$studentName,$ciphours,$attendance,$conduct, $cca) = $data[$i];
						break;
						case ($eRCTemplateSetting['OtherStudentInfo']['HideCIP'] && $eRCTemplateSetting['OtherStudentInfo']['HideNAPFA']):
						list($webSAMSRegNumber,$className,$classNum,$studentName,$attendance,$conduct, $cca) = $data[$i];
						break;
						default: 
						list($webSAMSRegNumber,$className,$classNum,$studentName,$ciphours,$attendance,$conduct,$napfa, $cca) = $data[$i];
						break;
					}
					# Check by WebSAMSReg Number
					if(!in_array($webSAMSRegNumber, $StudentWebSAMSRegNoArr)){
						continue;
					}
					
					$studentID = $classNumMap[$classNum];
					
					# Check for input wordings
					$Attendance = '';
					$Conduct = '';
					if(strtolower($attendance) == "regular")
					 	$Attendance = 1;
					else if(strtolower($attendance) == "irregular")
					 	$Attendance = 2;
					 	
					if(strtolower($conduct) == "excellent")
					 	$Conduct = 4;
					else if(strtolower($conduct) == "very good")
					 	$Conduct = 3;
					else if(strtolower($conduct) == "good")
					 	$Conduct = 2;
					else if(strtolower($conduct) == "satisfactory")
					 	$Conduct = 1;
					 	
				 	if(strtolower($napfa) == "nil")
					 	$napfa = 1;
					else if(strtolower($napfa) == "gold")
					 	$napfa = 2;
					else if(strtolower($napfa) == "silver")
					 	$napfa = 3;
					else if(strtolower($napfa) == "bronze")
					 	$napfa = 4;
					else if(strtolower($napfa) == "exempted")
					 	$napfa = 5;	
					
					# if comment already exist, put it into array for UPDATE
					if (in_array($studentID, $existStudents)){						
						$updateOtherStudentInfo[$studentID]['ciphours'] = $ciphours;
						$updateOtherStudentInfo[$studentID]['attendance'] = $Attendance;
						$updateOtherStudentInfo[$studentID]['conduct'] = $Conduct;
						$updateOtherStudentInfo[$studentID]['cca'] = $cca;
						$updateOtherStudentInfo[$studentID]['napfa'] = $napfa;
					# else,  INSERT
					} else {
						$insertOtherStudentInfo[$studentID]['ciphours'] = $ciphours;
						$insertOtherStudentInfo[$studentID]['attendance'] = $Attendance;
						$insertOtherStudentInfo[$studentID]['conduct'] = $Conduct;
						$insertOtherStudentInfo[$studentID]['cca'] = $cca;
						$insertOtherStudentInfo[$studentID]['napfa'] = $napfa;
					}
			    }
			    
			    $success = array();
			    
				# INSERT new Other Student Info
				if (isset($insertOtherStudentInfo)) {
					foreach($insertOtherStudentInfo as $key => $info) {
						if (in_array($key, $StudentIDArray))
							$values[] = "('$key', '$ReportID', 
											".(($info['ciphours'] != "") ? "'".$info['ciphours']."'" : "NULL").", 
											".(($info['attendance'] != "") ? "'".$info['attendance']."'" : "NULL").", 
											".(($info['conduct'] != "") ? "'".$info['conduct']."'" : "NULL").", 
											".(($info['napfa'] != "") ? "'".$info['napfa']."'" : "NULL").", 
											'".$info['cca']."', '".$lreportcard->uid."', NOW(), NOW())";
					}
					$values = implode(",", $values);
					$field = "StudentID, ReportID, CIPHours, Attendance, Conduct, NAPFA, CCA, TeacherID, DateInput, DateModified";
				    $sql = "INSERT INTO $table ($field) VALUES $values";
					$success[] = $li->db_db_query($sql);
				}	
				
				# UPDATE existing Other Student Info
				if (isset($updateOtherStudentInfo)) {
					foreach($updateOtherStudentInfo as $key => $info) {
						$sql = "UPDATE 
									$table 
								SET 
									CIPHours = ".(($info['ciphours'] != "") ? "'".$info['ciphours']."'" : "NULL").", 
									Attendance = ".(($info['attendance'] != "") ? "'".$info['attendance']."'" : "NULL").", 
									Conduct = ".(($info['conduct'] != "") ? "'".$info['conduct']."'" : "NULL").", 
									NAPFA = ".(($info['napfa'] != "") ? "'".$info['napfa']."'" : "NULL").", 
									CCA = '".$info['cca']."', 
									TeacherID = '".$lreportcard->uid."', 
									DateModified = NOW() 
								WHERE 
									StudentID = '$key' AND ReportID = '$ReportID'";
						$success[] = $li->db_db_query($sql);
					}
				}
				
				if (in_array(false, $success)) {
					header("Location: import.php?$params&Result=import_failed2");
					exit();
				}
				
			} else {
				header("Location: import.php?$params&Result=import_failed");
				exit();
			}
		}
		
		intranet_closedb();
		
		header("Location: edit.php?$params&Result=import_success");
		
	} else {
		echo "You have no priviledge to access this page.";
	}
} else {
	echo "You have no priviledge to access this page.";
}
?>
	