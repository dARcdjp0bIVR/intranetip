<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();
 
if ($plugin['ReportCard2008']) {
	$lreportcard = new libreportcard();
	
	if($ck_ReportCard_UserType=="TEACHER")
	{
		$FormArrCT = $lreportcard->returnClassTeacherForm($UserID);
		$ClassLevelID = ($ClassLevelID == "") ? $FormArrCT[0][0] : $ClassLevelID;
		$ClassArrCT = $lreportcard->returnClassTeacherClass($UserID, $ClassLevelID);
	}
	
    if ($lreportcard->hasAccessRight()) {
        $linterface = new interface_html();
        $CurrentPage = "Management_OtherStudentInfo";
		$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
		
		############################################################################################################
		
		# Get ClassLevelID (Form) of the reportcard template
		$FormArr = $ck_ReportCard_UserType=="ADMIN" ? $lreportcard->GET_ALL_FORMS(1) : $FormArrCT;
		
		$display = '';
		if(count($FormArr) > 0){
			# Filters - By Form (ClassLevelID)
			$ClassLevelID = ($ClassLevelID == "") ? $FormArr[0][0] : $ClassLevelID;
			$FormSelection = $linterface->GET_SELECTION_BOX($FormArr, "name='ClassLevelID' class='tabletexttoptext' onchange='document.form2.submit()'", "", $ClassLevelID);
			
			# Filters - By Type (Term1, Term2, Whole Year, etc)
			// Get Semester Type from the reportcard template corresponding to ClassLevelID
			$ReportTypeSelection = '';
			$ReportTypeArr = array();
			$ReportTypeOption = array();
			$ReportTypeArr = $lreportcard->GET_REPORT_TYPES($ClassLevelID);
			
			if(count($ReportTypeArr) > 0){
				for($j=0 ; $j<count($ReportTypeArr) ; $j++){
					# for checking the existence of ReportID when changing ClassLevel
					$ReportIDList[] = $ReportTypeArr[$j]['ReportID'];
					$ReportTypeOption[$j][0] = $ReportTypeArr[$j]['ReportID'];
					$ReportTypeOption[$j][1] = $ReportTypeArr[$j]['SemesterTitle'];
				}
				$ReportTypeSelection = $linterface->GET_SELECTION_BOX($ReportTypeOption, 'name="ReportID" class="tabletexttoptext" onchange="document.form2.submit()"', '', $ReportID);
				
				$ReportID = ($ReportID == "" || !in_array($ReportID,$ReportIDList)) ? $ReportTypeOption[0][0] : $ReportID;
			}
		
			# Main Content
			$ClassArr = $ck_ReportCard_UserType=="ADMIN" ? $lreportcard->GET_CLASSES_BY_FORM($ClassLevelID) : $ClassArrCT;
			
			$display = "";
			if(sizeof($ClassArr) > 0 && sizeof($ReportTypeArr) > 0) {
				$table = $lreportcard->DBName.".RC_OTHER_STUDENT_INFO";
				
				for($i=0 ; $i<count($ClassArr) ;$i++){
					$ClassID = $ClassArr[$i][0];
					$ClassName = $ClassArr[$i][1];
					// Student Info Array
					$StudentArray = $lreportcard->GET_STUDENT_BY_CLASS($ClassID);
					$StudentSize = sizeof($StudentArray);
					$StudentIDArray = array();
					$StudentIDList = "";
					if ($StudentSize > 0) {
						// Student ID list of students in a class
						for($j=0; $j<$StudentSize; $j++)
							$StudentIDArray[] = $StudentArray[$j][0];
						if(!empty($StudentIDArray))
							$StudentIDList = implode(",", $StudentIDArray);
					}
					
					# get the last modified date and last modified user
					$dateModified = '-';
					$lastModifiedBy = '-';
							
					if($StudentSize > 0){			
						$NameField = getNameFieldByLang2("b.");
						$sql = "SELECT a.ReportID, $NameField AS Name, MAX(a.DateModified) AS DateModified ";
						$sql .= "FROM $table AS a, INTRANET_USER AS b ";
						$sql .= "WHERE a.TeacherID = b.UserID AND a.ReportID = '$ReportID'";
						$sql .= "AND a.StudentID IN ($StudentIDList) ";
						$sql .= "GROUP BY a.ReportID";
						
						$result = $lreportcard->returnArray($sql);
						if (sizeof($result) > 0) {
							$dateModified = $result[0]["DateModified"];
							$lastModifiedBy = $result[0]["Name"];
						} 
					}
					
					# Content
					$tr_css = ($i % 2 == 1) ? "tablegreenrow2" : "tablegreenrow1";
					
					$display .= "<tr class='$tr_css'>";
					$display .= "<td class='tabletext'>$ClassName</td>";
					$display .= "<td align='center' class='tabletext'><a href='edit.php?ClassLevelID=$ClassLevelID&ClassID=$ClassID&ReportID=$ReportID'><img src='".$image_path."/".$LAYOUT_SKIN."/icon_edit_b.gif' width='20' height='20' border='0'></a></td>";
					$display .= "<td class='tabletext'>$dateModified</td>";
					$display .= "<td class='tabletext'>$lastModifiedBy</td>";
					$display .= "</tr>";
				}
			}
			
		} else {
			$td_colspan = "4";
			$display .=	'<tr class="tablegreenrow1" height="40">
							<td class="tabletext" colspan="'.$td_colspan.'" align="center">'.$i_no_record_exists_msg.'</td>
						</tr>';
		}
		
		$table_tool = $linterface->GET_SYS_MSG($Result);
		############################################################################################################
        
		# tag information
		$TAGS_OBJ[] = array($eReportCard['Management_OtherStudentInfo'], "", 0);
		
		$linterface->LAYOUT_START();
?>


<br/>
<form name="form2" method="POST" action="" style="margin:0px;">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>
      <table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
        <tr>
          <td>
            <table border="0" cellspacing="0" cellpadding="0">
              <tr>
	            <td><?=$FormSelection?></td>
	            <td width="1">&nbsp;</td>
	            <td><?=$ReportTypeSelection?></td>
	            <td width="1">&nbsp;</td>
	            <td align="right"><?=$table_tool?></td>
	          </tr>
	        </table>
	      </td>
  		</tr>
  	  </table>
  	</td>
  </tr>
</table>
</form>
<br/>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>
      <table width="95%" border="0" cellspacing="0" cellpadding="4" align="center">
        <tr class="tablegreentop">
          <td class="tabletoplink"><?=$eReportCard['Class']?></td>
          <td align="center" class="tabletoplink"><?=$eReportCard['OtherStudentInfo']?></td>
          <td class="tabletoplink"><?=$eReportCard['LastModifiedDate']?></td>
          <td class="tabletoplink"><?=$eReportCard['LastModifiedBy']?></td>
        </tr>
		<?=$display?>
      </table>
    </td>
  </tr>
</table>
<br/>
<input type="hidden" name="ReportID" value="<?=$ReportID?>" />
<input type="hidden" name="ClassLevelID" value="<?=$ClassLevelID?>" />


<?
        $linterface->LAYOUT_STOP();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>