<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if ($plugin['ReportCard2008']) {
	include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
//	include_once($PATH_WRT_ROOT."includes/libreportcard2008w.php");
//	include_once($PATH_WRT_ROOT."includes/libreportcard2008_sis.php");
//	$lreportcard = new libreportcardSIS();
 if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName == "sis") 
{
	include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
	$lreportcard = new libreportcardcustom();
} else {
	$lreportcard = new libreportcard2008();
}
	$CurrentPage = "Management_OtherStudentInfo";
	$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
    if ($lreportcard->hasAccessRight()) {
        $linterface = new interface_html();
        
		############################################################################################################
		
		if (!isset($ClassLevelID, $ReportID, $ClassID)) {
			header("Location: index.php");
			exit();
		}
		
		
		// Class
		$ClassArr = $lreportcard->GET_CLASSES_BY_FORM($ClassLevelID);
		if (sizeof($ClassArr) > 0) {
			for($i=0 ; $i<count($ClassArr) ;$i++) {
				if($ClassArr[$i][0] == $ClassID)
					$ClassName = $ClassArr[$i][1];
			}
			
			// Period (Term or Whole Year)
			$ReportInfo = $lreportcard->GET_REPORT_TYPES($ClassLevelID, $ReportID);
			$PeriodName = $ReportInfo['SemesterTitle'];
			
			// Student Info Array
			$StudentArray = $lreportcard->GET_STUDENT_BY_CLASS($ClassID, '', 1);
			$StudentSize = sizeof($StudentArray);
			
			// Student ID list of students in a class
			for($i=0; $i<$StudentSize; $i++)
				$StudentIDArray[] = $StudentArray[$i][0];
			if(!empty($StudentIDArray))
				$StudentIDList = implode(",", $StudentIDArray);
			
			$otherStudentInfo = array();
			$otherStudentInfo = $lreportcard->GET_OTHER_STUDENT_INFO($StudentIDArray, $ReportID);
		}
		
		# Preparation
		// Attendance
		$AttendanceOptions = array();
		$AttendanceOptions[] = array('', '--'.$eReportCard['ChangeAllTo'].'--');
		foreach($eReportCard['OtherStudentInfo_Attendance'] as $key => $value){
			$AttendanceOptions[] = array($key, $value);
		}
		
		// Conduct
		$ConductOptions = array();
		$ConductOptions[] = array('', '--'.$eReportCard['ChangeAllTo'].'--');
		foreach($eReportCard['OtherStudentInfo_Conduct'] as $key => $value){
			$ConductOptions[] = array($key, $value);
		}
		
		// NAPFA
		$NAPFAOptions = array();
		$NAPFAOptions[] = array('', '--'.$eReportCard['ChangeAllTo'].'--');
		foreach($eReportCard['OtherStudentInfo_NAPFA'] as $key => $value){
			$NAPFAOptions[] = array($key, $value);
		}
		
		# UI
		$display = '<table class="yui-skin-sam" width="95%" cellpadding="5" cellspacing="0">';
		$display .= '<tr>
	          <td width="5%" align="center" class="tablegreentop tabletopnolink">'.$eReportCard['StudentNo_short'].'</td>
	          <td width="25%" class="tablegreentop tabletopnolink">'.$eReportCard['Student'].'</td>';
	    
	    # 20140722 SIS hide CIP 
	    if(!$eRCTemplateSetting['OtherStudentInfo']['HideCIP']){
	    	$display .= '<td width="12%" align="center" class="tablegreentop tabletopnolink">'.$eReportCard['OtherStudentInfoValue_CIPHours'].'</td>';
	    }
		$display .= '<td width="15%" align="center" class="tablegreentop tabletopnolink">'.$eReportCard['OtherStudentInfoValue_Attendance'].'<br>';
		$display .= $linterface->GET_SELECTION_BOX($AttendanceOptions, 'name="attendanceAll" id="attendanceAll" class="tabletexttoptext" onchange="setAll(this.selectedIndex, \'attendance\')"', '', '');
		$display .= '</td>';
		$display .= '<td width="15%" align="center" class="tablegreentop tabletopnolink">'.$eReportCard['OtherStudentInfoValue_Conduct'].'<br>';
		$display .= $linterface->GET_SELECTION_BOX($ConductOptions, 'name="conductAll" id="conductAll" class="tabletexttoptext" onchange="setAll(this.selectedIndex, \'conduct\')"', '', '');
		$display .= '</td>';
	    if(!$eRCTemplateSetting['OtherStudentInfo']['HideNAPFA']){
		$display .= '<td width="15%" align="center" class="tablegreentop tabletopnolink">'.$eReportCard['OtherStudentInfoValue_NAPFA'].'<br>';
		$display .= $linterface->GET_SELECTION_BOX($NAPFAOptions, 'name="NAPFAAll" id="NAPFAAll" class="tabletexttoptext" onchange="setAll(this.selectedIndex, \'napfa\')"', '', '');
		$display .= '</td>';
	    }
		$display .= '<td class="tablegreentop tabletopnolink">'.$eReportCard['OtherStudentInfoValue_CCARemarks'].'</td>';
	    $display .= '</tr>';
	    
	    $AttendanceOptions[0] = array('', '--');
	    $ConductOptions[0] = array('', '--');
	    $NAPFAOptions[0] = array('', '--');
	    
		# Student Content
		if (isset($StudentSize) && $StudentSize > 0) {
			for($j=0; $j<$StudentSize; $j++){
				list($StudentID, $WebSAMSRegNo, $StudentNo, $StudentName) = $StudentArray[$j];
				$tr_css = ($j % 2 == 1) ? "tablegreenrow2" : "tablegreenrow1";
				
				$display .= '<tr class="'.$tr_css.'">
			          <td align="center" valign="top" class="tabletext">'.$StudentNo.' <input type="hidden" name="StudentID[]" id="StudentID_'.$j.'" value="'.$StudentID.'"/></td>
		          	  <td class="tabletext" valign="top">'.$StudentName.' 
		          	  '.((isset($otherStudentInfo[$StudentID])) ? '<input type="hidden" name="otherStudentInfoID['.$StudentID.']" id="otherStudentInfoID_'.$StudentID.'" value="'.$otherStudentInfo[$StudentID]['OtherStudentInfoID'].'"/>' : '').'
		          	  </td>';
		          	  
				# 20140722 SIS Hide CIP
		        // CIP Hours
		        $otherPar = array('size'=>5, 'class'=>'ratebox', 'onchange'=>'jCHANGE_STATUS()');
		        $inputValue = (isset($otherStudentInfo[$StudentID])) ? $otherStudentInfo[$StudentID]['CIPHours'] : '';
		        $inputID = 'ciphours_'.$StudentID;
		        $inputName = 'ciphours['.$StudentID.']';

				if($eRCTemplateSetting['OtherStudentInfo']['HideCIP']){	
					$display .= '<input type="hidden" name="'.$inputName.'" id="'.$inputID.'" value="">';
				}else{	          	  
		        $otherPar = array('size'=>5, 'class'=>'ratebox', 'onchange'=>'jCHANGE_STATUS()');
		        $inputValue = (isset($otherStudentInfo[$StudentID])) ? $otherStudentInfo[$StudentID]['CIPHours'] : '';
				$display .= '<td class="tabletext" valign="top" align="center">';
				$display .= $lreportcard->GET_INPUT_TEXT($inputName, $inputID, $inputValue, '', $otherPar);
				$display .= '</td>';
				}
				
				// Attendance
				$selectValue = (isset($otherStudentInfo[$StudentID])) ? $otherStudentInfo[$StudentID]['Attendance'] : '';
				$selectID = 'attendance_'.$StudentID;
				$selectName = 'attendance['.$StudentID.']';
				$display .= '<td class="tabletext" valign="top" align="center">';
				$display .= $linterface->GET_SELECTION_BOX($AttendanceOptions, 'name="'.$selectName.'" id="'.$selectID.'" class="tabletexttoptext" onchange="jCHANGE_STATUS()" ', '', $selectValue);
				$display .= '</td>';
				
				// Conduct
				$selectValue = (isset($otherStudentInfo[$StudentID])) ? $otherStudentInfo[$StudentID]['Conduct'] : '';
				$selectID = 'conduct_'.$StudentID;
				$selectName = 'conduct['.$StudentID.']';
				$display .= '<td class="tabletext" valign="top" align="center">';
				$display .= $linterface->GET_SELECTION_BOX($ConductOptions, 'name="'.$selectName.'" id="'.$selectID.'" class="tabletexttoptext" onchange="jCHANGE_STATUS()" ', '', $selectValue);
				$display .= '</td>';
				
				# 20140722 SIS Hide NAPFA
				if(!$eRCTemplateSetting['OtherStudentInfo']['HideNAPFA']){
				// NAPFA
				$selectValue = (isset($otherStudentInfo[$StudentID])) ? $otherStudentInfo[$StudentID]['NAPFA'] : '';
				$selectID = 'napfa_'.$StudentID;
				$selectName = 'napfa['.$StudentID.']';
				$display .= '<td class="tabletext" valign="top" align="center">';
				$display .= $linterface->GET_SELECTION_BOX($NAPFAOptions, 'name="'.$selectName.'" id="'.$selectID.'" class="tabletexttoptext" onchange="jCHANGE_STATUS()" ', '', $selectValue);
				$display .= '</td>';
				}
				
				// CCA Remarks
				$textareaValue = (isset($otherStudentInfo[$StudentID])) ? $otherStudentInfo[$StudentID]['CCA'] : '';
				$textareaID = 'cca_'.$StudentID;
				$textareaName = 'cca['.$StudentID.']';
				$display .= '<td class="tabletext" valign="top">';
				$display .= "<textarea class=\"textboxtext\" id=\"$textareaID\" name=\"$textareaName\" cols=\"20\" rows=\"3\" wrap=\"virtual\" onFocus=\"this.rows=3;\" maxlength=\"".$lreportcard->textAreaMaxChar."\" onkeyup=\"return ismaxlength(this)\"  onchange=\"jCHANGE_STATUS()\" disabled>$textareaValue</textarea>";
				$display .= '</td>';
				
				$display .= '</tr>';
			}
			
			$display .= '</table>';
			
			$display_button .= $linterface->GET_ACTION_BTN($button_save, "button", "jSUBMIT_FORM()")."&nbsp";
	        $display_button .= $linterface->GET_ACTION_BTN($button_reset, "reset", "")."&nbsp";
			$display_button .= $linterface->GET_ACTION_BTN($button_cancel, "button", "jBACK()");
		} else {
			$td_colspan = "6";
			$display .=	'<tr class="tablegreenrow1" height="40">
							<td class="tabletext" colspan="'.$td_colspan.'" align="center">'.$i_no_record_exists_msg.'</td>
						</tr>';
			$display .= '</table>';
		}
		
		$params = "ClassLevelID=$ClassLevelID&ClassID=$ClassID&ReportID=$ReportID";
		
		############################################################################################################
        
		$PAGE_NAVIGATION[] = array($button_edit);
		
		# tag information
		$TAGS_OBJ[] = array($eReportCard['Management_OtherStudentInfo'], "", 0);
		$linterface->LAYOUT_START();
		
		$parameters = "ClassID=$ClassID&ReportID=$ReportID&ClassLevelID=$ClassLevelID";
?>

<script language="javascript">
function jCHANGE_STATUS(){
	document.getElementById("isOutdated").value = 1;
}

function ismaxlength(obj){
	var mlength = obj.getAttribute ? parseInt(obj.getAttribute("maxlength")) : "";
	
	if (obj.getAttribute && obj.value.length > mlength){
		obj.value = obj.value.substring(0,mlength);
	}
}


function setAll(parStatus, type) {
	var indexToSelect = parStatus;
	var student_id = document.getElementsByName("StudentID[]");
	
	if(student_id.length > 0){
		for (var i=0 ; i<student_id.length ; i++) {
			var objName = type+"_"+student_id[i].value;
			var obj = document.getElementById(objName);
			obj.selectedIndex = indexToSelect;
		}
	}
}

function jSUBMIT_FORM(){
	if(!jCHECK_FORM())
		return false;
	
	var obj = document.form1;
	obj.submit();
}

function jCHECK_FORM(){
	var student_id = document.getElementsByName("StudentID[]");
	if(student_id.length > 0){
		for (var i=0 ; i<student_id.length ; i++) {
			var objName = "ciphours_"+student_id[i].value;
			var obj = document.getElementById(objName);
			
			if(obj.value < 0){
				alert("<?=$eReportCard['jsCIPHourCannotBeNegative']?>");
				obj.focus();
				return false;
			}
			
			if(obj.value != "" && !check_numeric(obj, obj.value, "<?=$eReportCard['jsCIPHourInvalid']?>")){
				obj.focus();
				return false;
			}
		}
	}
	
	return true;
}

function jBACK(){
	var status = document.getElementById("isOutdated").value;
	
	if(status == 1){
		if(!confirm("Do you want to save before you leave this page?")){
			window.location = "index.php";
		} else {
			document.getElementById("IsSaveBefCancel").value = 1;
			jSUBMIT_FORM();
		}
	} else {
		window.location = "index.php";
	}
}
</script>


<form name="form1" method="post" action="edit_update.php" onSubmit="return false">
<div align="left" style="width:95%">
<table border="0" cellpadding="5" cellspacing="0" class="tabletext">
  <tr>
	<td><?=$eReportCard['Period']?> : <strong><?=$PeriodName?></strong></td>
	<td><?=$eReportCard['Class']?> : <strong><?=$ClassName?></strong></td>
  </tr>
</table>
</div>
<br />
<table width="98%" border="0" cellspacing="5" cellpadding="0">
  <tr>
	<td align="left" colspan="2"><?=$linterface->GET_NAVIGATION($PAGE_NAVIGATION)?></td>
  </tr>
</table>
<table width="95%" border="0" cellspacing="5" cellpadding="0">
  <tr>
	<td>
	  <table border="0" cellspacing="0" cellpadding="0">
		<tr>
		  <td>
			<a href="import.php?<?=$params?>" class="contenttool">
				<img src="<?=$image_path.'/'.$LAYOUT_SKIN?>/icon_import.gif" width="18" height="18" border="0" align="absmiddle"> <?=$button_import?>
			</a>
		  </td>
		  <td><img src="<?=$image_path.'/'.$LAYOUT_SKIN?>/10x10.gif"></td>
		  <td>
			<a href="export.php?<?=$params?>" class="contenttool">
				<img src="<?=$image_path.'/'.$LAYOUT_SKIN?>/icon_export.gif" width="18" height="18" border="0" align="absmiddle"> <?=$button_export?>
			</a>
		  </td>
		</tr>
	  </table>
	</td>
	<td align="right"><?= $linterface->GET_SYS_MSG($Result); ?></td>
  </tr>
</table>
<?=$display?>
<table width="95%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td height="1" class="dotline"><img src="<?=$image_path.'/'.$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
	</tr>
	<tr>
		<td>
			<table width="100%" border="0" cellspacing="0" cellpadding="2">
				<tr>
					<td align="center" valign="bottom"><?=$display_button?></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<input type="hidden" name="ClassLevelID" id="ClassLevelID" value="<?=$ClassLevelID?>"/>
<input type="hidden" name="ReportID" id="ReportID" value="<?=$ReportID?>"/>
<input type="hidden" name="ClassID" id="ClassID" value="<?=$ClassID?>"/>

<input type="hidden" name="isOutdated" id="isOutdated" value="0"/>
<input type="hidden" name="IsSaveBefCancel" id="IsSaveBefCancel" value="0"/>
</form>

<?
		intranet_closedb();
        $linterface->LAYOUT_STOP();
    } else {
    ?>
You have no priviledge to access this page.
    <?
    }
} else {
?>
You have no priviledge to access this page.
<?
}
?>