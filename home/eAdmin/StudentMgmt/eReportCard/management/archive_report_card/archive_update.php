<?php
@SET_TIME_LIMIT(21600);
@ini_set(memory_limit, -1);

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
//include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
//include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

include($PATH_WRT_ROOT."includes/eRCConfig.php");

intranet_auth();
intranet_opendb();

$PrintTemplateType = $_POST['PrintTemplateType'];
$PrintTemplateType = ($PrintTemplateType=='')? 'normal' : $PrintTemplateType;

//debug_r($_POST);

if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") {
	# load specific layout for teacher view of munsang college (primary) report 
	if ($ReportCardCustomSchoolName == "munsang_pri" && $viewType == "teacher") {
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName."_teacher.php");	
	}
	else {
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
	}
	$lreportcard = new libreportcardcustom();
} else {
	$lreportcard = new libreportcard();
}



# Retrieve Report Type
$ReportSetting = $lreportcard->returnReportTemplateBasicInfo($ReportID);
$SemID = $ReportSetting['Semester'];
$ClassLevelID = $ReportSetting['ClassLevelID'];
$ReportType = $SemID == "F" ? "W" : "T";

if($ReportCardCustomSchoolName == "carmel_alison" && $ReportType=="T") {
	$isSimpleReport = $SimpleReportType=='simple'?1:0;
}

### Backup CSS
$ArchiveFolderPath = $lreportcard->ArchiveCSSPath;

# Backup Tempplate CSS
$SourceTemplateCSS_Filename = $eRCTemplateSetting['CSS'];
$SourceTemplateCSSPath = $PATH_WRT_ROOT."file/reportcard2008/templates/".$SourceTemplateCSS_Filename;
$TargetTemplateCSSPath = $ArchiveFolderPath.$SourceTemplateCSS_Filename;
$successArr['TemplateCSS'] = $lreportcard->Archive_CSS_File($SourceTemplateCSSPath, $TargetTemplateCSSPath);


# Backup Print CSS
$SourcePrintCSSPath = $PATH_WRT_ROOT."templates/2009a/css/print.css";
$TargetPrintCSSPath = $ArchiveFolderPath."print.css";
$successArr['Print'] = $lreportcard->Archive_CSS_File($SourcePrintCSSPath, $TargetPrintCSSPath);


# Backup Content CSS
$SourceContentCSSPath = $PATH_WRT_ROOT."templates/2009a/css/content_25.css";
$TargetContentCSSPath = $ArchiveFolderPath."content_25.css";
$successArr['Content'] = $lreportcard->Archive_CSS_File($SourceContentCSSPath, $TargetContentCSSPath);


### Generate student report card
$StudentIDAry = $lreportcard->GET_STUDENT_BY_CLASSLEVEL($ReportID, $ClassLevelID, $ClassID);
$numOfStudent = count($StudentIDAry);

for($s=0; $s<$numOfStudent; $s++)
{
	$StudentID = $StudentIDAry[$s]['UserID'];
	$thisHTML = '';
	
	/*
	if ($s < $numOfStudent-1)
		$thisPageBreakCSS = ' style="page-break-after:always" ';
	else
		$thisPageBreakCSS = '';
	
	$thisHTML .= '<div id="container">'."\n";
	$thisHTML .= '<table width="100%" border="0" cellpadding="02" cellspacing="0" valign="top" '.$thisPageBreakCSS.' >'."\n";
	*/
	
	if($ReportCardCustomSchoolName == "st_stephen_college")
	{
		$thisHTML .= $lreportcard->FirstPage($ReportID, $StudentID);
		$thisHTML .= $lreportcard->getSubjectPages($ReportID, $StudentID);
			
		# Subejct Pages
//		$MainSubjectArray = $lreportcard->returnSubjectwOrder($ClassLevelID);
//		foreach($MainSubjectArray as $SubjectID => $SubejctData)
//		{
//			$thisHTML .= $lreportcard->SubjectPage($ReportID, $SubjectID, $StudentID);
//		}
	}
	else
	{
		if($ReportCardCustomSchoolName != 'hkuga_college')
		{	
			$TitleTable = $lreportcard->getReportHeader($ReportID, $StudentID, $PrintTemplateType);
			$StudentInfoTable = $lreportcard->getReportStudentInfo($ReportID, $StudentID, $forPage3='', $PageNum=1, $PrintTemplateType);
			$MSTable = $lreportcard->getMSTable($ReportID, $StudentID, $PrintTemplateType, $PageNum=1);	// $PageNum for "wong_kam_fai_secondary"
			$MiscTable = $lreportcard->getMiscTable($ReportID, $StudentID, $PrintTemplateType);
			$SignatureTable = $lreportcard->getSignatureTable($ReportID, $StudentID, $PrintTemplateType);
			$FooterRow = $lreportcard->getFooter($ReportID, $PrintTemplateType);
		}
		
//		if($ReportCardCustomSchoolName == 'sekolah_menengah_yu_yuan_ma')
//		{	
//			$TitleTable = $lreportcard->getReportHeader($ReportID, $StudentID);
//			$StudentInfoTable = $lreportcard->getReportStudentInfo($ReportID, $StudentID);
//			$MSTable = $lreportcard->getMSTable($ReportID, $StudentID,$PrintTemplateType);
//			$MiscTable = $lreportcard->getMiscTable($ReportID, $StudentID);
//			$SignatureTable = $lreportcard->getSignatureTable($ReportID, $StudentID);
//			$FooterRow = $lreportcard->getFooter($ReportID);
//		}
		
		if($ReportCardCustomSchoolName == "carmel_alison")
		{
			if($isSimpleReport)
				$thisHTML.= $lreportcard->getSimpleLayout($TitleTable, $StudentInfoTable, $MSTable, $MiscTable, $SignatureTable, $FooterRow, $ReportID, $StudentID);
			else
			{
				$thisHTMLAry = $lreportcard->getLayout($TitleTable, $StudentInfoTable, $MSTable, $MiscTable, $SignatureTable, $FooterRow, $ReportID, $StudentID);
				$thisHTML .= $thisHTMLAry[0].$thisHTMLAry[1];
			}	
			
		}
		else if (method_exists($lreportcard, "getLayout")) {
			$thisHTML .= $lreportcard->getLayout($TitleTable, $StudentInfoTable, $MSTable, $MiscTable, $SignatureTable, $FooterRow, $ReportID, $StudentID, $SubjectID, $SubjectSectionOnly, $PrintTemplateType, $numOfStudent, $s)."\n";
		} else {

			$thisHTML .= '<tr><td>'.$TitleTable.'</td><tr>'."\n";
			$thisHTML .= '<tr><td>'.$StudentInfoTable.'</td><tr>'."\n";
			$thisHTML .= '<tr><td>'.$MSTable.'</td><tr>'."\n";
			$thisHTML .= '<tr><td>'.$MiscTable.'</td><tr>'."\n";
			$thisHTML .= '<tr><td>'.$SignatureTable.'</td><tr>'."\n";
			$thisHTML .= $FooterRow."\n";
		}
	}
	
	//	$thisHTML .= '</table>'."\n";
	//$thisHTML .= '</div>'."\n";
	
	$successArr[$StudentID] = $lreportcard->Archive_Report_Card($ReportID, $StudentID, $thisHTML);
}


if (in_array(false, $successArr) || $numOfStudent == 0)
	$msg = 'report_archive_failed';
else
{
	$msg = 'report_archive';
	
	# Update last report archived date
	$lreportcard->UPDATE_REPORT_LAST_DATE($ReportID, 'LastArchived');
}
	
$params = ((isset($Semester)) ? "Semester=$Semester&" : "")."msg=$msg";

intranet_closedb();
header("Location: index.php?ClassLevelID=$ClassLevelID&$params");
?>