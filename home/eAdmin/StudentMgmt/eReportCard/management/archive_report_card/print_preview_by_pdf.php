<?php
# using: 

######################################################################################################################################
### If you have modified this page, please check if you have to modify /management/archive_report_card/archive_pdf_update.php
######################################################################################################################################

####################################################
#
#	Date:	2016-08-05	Bill	[2016-0330-1524-42164]
#			Create file
#			- Support PDF report in browser
#
####################################################

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

# Access right checking
if (!$plugin['ReportCard2008'])
{
	header ("Location: /");
	intranet_closedb();
	exit();
}

// Init libreportcard Object
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008w.php");
include($PATH_WRT_ROOT."includes/eRCConfig.php");
if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") {
	if ($ReportCardCustomSchoolName == "munsang_pri" && $viewType == "teacher") {
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName."_teacher.php");
		$lreportcard = new libreportcard();
	}
	else {
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
		$lreportcard = new libreportcardcustom("subject_topic");
	}
} else {
	$lreportcard = new libreportcard();
}

# Access right checking
if (!$lreportcard->hasAccessRight() || !$eRCTemplateSetting['Report']['ExportPDFReport']) 
{
	header ("Location: /");
	intranet_closedb();
	exit();
}

// Set to archive year db
if(trim($AcademicYear)!='')
{
	$lreportcard->schoolYear = $AcademicYear;
}

// Set current studentid
if(trim($StudentID)=='')
{
	$StudentID = $TargetStudentID[0];
}

intranet_closedb();

// Open archive PDF file
$target_filepath = $intranet_root."/file/reportcard2008/".$lreportcard->schoolYear."/archivePDFReport/".$ReportID."/".$StudentID.".pdf";
if (file_exists($target_filepath)) {
	header('Content-type: application/pdf');
	header('Content-Disposition:inline; filename="'.$target_filepath.'"');
	header('Content-Transfer-Encoding: binary');
	header('Content-Length: '.filesize($target_filepath));
	header('Accept-Ranges: bytes');
	
	@readfile($target_filepath);
}
else {
	echo $eReportCard['WarningArr']['NoArchivedReport'];
}
?>