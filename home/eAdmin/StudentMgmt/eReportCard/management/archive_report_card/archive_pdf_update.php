<?php
# using: 

####################################################
#
#	Date	:	2018-01-03	Bill
#				fixed: cannot archive report header
#
#	Date	:	2016-08-05	Bill	[2016-0330-1524-42164]
#				Create File
#				- Copy related logic from : 
#				1. archive_update.php 	2. /generate_reports/print_preview_by_pdf.php
#
####################################################

@SET_TIME_LIMIT(21600);
@ini_set(memory_limit, -1);

$PageRight = "ADMIN";

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

# Access right checking
if (!$plugin['ReportCard2008'])
{
	header ("Location: /");
	intranet_closedb();
	exit();
}

// Init libreportcard Object
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008w.php");
include($PATH_WRT_ROOT."includes/eRCConfig.php");
if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "")
{
	if ($ReportCardCustomSchoolName == "munsang_pri" && $viewType == "teacher") {
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName."_teacher.php");
		$lreportcard = new libreportcard();
	}
	else {
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
		$lreportcard = new libreportcardcustom("subject_topic");
	}
}
else
{
	$lreportcard = new libreportcard();
}

# Access right checking
if (!$lreportcard->hasAccessRight() || !$eRCTemplateSetting['Report']['ExportPDFReport']) 
{
	header ("Location: /");
	intranet_closedb();
	exit();
}

require_once($PATH_WRT_ROOT."includes/mpdf/mpdf.php");

// Get POST data
$PrintTemplateType = $_POST['PrintTemplateType'];
$PrintTemplateType = ($PrintTemplateType=='')? 'normal' : $PrintTemplateType;

# Get Report Info
$ReportSetting = $lreportcard->returnReportTemplateBasicInfo($ReportID);
$isMainReport = $ReportSetting['isMainReport'];
$ClassLevelID = $ReportSetting['ClassLevelID'];
$SemID = $ReportSetting['Semester'];
$ReportType = $SemID == "F" ? "W" : "T";
$SemesterNo = $lreportcard->Get_Semester_Seq_Number($SemID);

# Get Report Type
$bilingual = false;
if($eRCTemplateSetting['Settings']['AutoSelectReportType'])
{
	// ES / MS Report
	$FormNumber = $lreportcard->GET_FORM_NUMBER($ClassLevelID);
	if (is_numeric($FormNumber)) {
		$FormNumber = intval($FormNumber);
		$isESReport = $FormNumber > 0 && $FormNumber < 6;
		$isMSReport = !$isESReport;
		
		// check if need bilingual report
		$bilingual = $eRCTemplateSetting['Report']['ReportGeneration']['GenerateBilingualReport'] && $isMainReport && $isESReport;
	}
	// KG Report
	else {
		$isKGReport = true;
	}
}

//if($ReportCardCustomSchoolName == "carmel_alison" && $ReportType=="T") {
//	$isSimpleReport = $SimpleReportType=='simple'?1:0;
//}

# Get Template CSS
$ReportCSS = "";
$eRCtemplate = empty($eRCtemplate) ? $lreportcard : $eRCtemplate;
$eRtemplateCSS = $lreportcard->getCSSContent($ReportID);
if($eRtemplateCSS) {
	$ReportCSS = $eRtemplateCSS;
}

# Get Report Header
$ReportHeader = $eRCtemplate->getReportHeader($ReportID, $StudentID, $PrintTemplateType);

# Get Report Footer
$ReportFooter = $eRCtemplate->getFooter($ReportID, $PrintTemplateType);

# Get Student List
$StudentIDAry = $lreportcard->GET_STUDENT_BY_CLASSLEVEL($ReportID, $ClassLevelID, $ClassID);
$numOfStudent = count((array)$StudentIDAry);

# Get Student Record
if($eRCTemplateSetting['Settings']['AutoSelectReportType'] && $isMainReport)
{
	// get attendance records for semester report
	include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
	$libcardstudentarrend = new libcardstudentattend2();
	
	$eRCtemplate->EnableEntryLeavePeriod = $libcardstudentarrend->EnableEntryLeavePeriod;
	$eRCtemplate->ProfileAttendCount = $libcardstudentarrend->ProfileAttendCount;
	$eRCtemplate->retrieveAttandanceMonthData($StudentIDAry, $SemID, $SemesterNo);

	// get gpa for MS Transcript
	if($isMSReport){
		$eRCtemplate->retrieveGPAData($ReportID, $StudentIDAry);
	}
}

# Set up PDF Object
//$eRCtemplate->reportObjectSetUp($ReportID, $bilingual);

# Create folder to store pdf
$libfs = new libfilesystem();
$target_filepath = $intranet_root."/file/reportcard2008/".$eRCtemplate->schoolYear."/archivePDFReport/".$ReportID;
if (!file_exists($target_filepath)) {
	$result = $libfs->folder_new($target_filepath);
	$failToCreateFolder = $result===false;
}

$success = true; 
if($numOfStudent==0 || $failToCreateFolder)
{
	$success = false;
}
else
{
	// loop students
	for($s=0; $s<$numOfStudent; $s++)
	{
		// Create mPDF Object
		$eRCtemplate->reportObjectSetUp($ReportID, $bilingual);
		
		if(!isset($eRCtemplate->pdf))
			continue;
		$mpdfObj = $eRCtemplate->pdf;
		
		// Define Report Header
		$mpdfObj->DefHTMLHeaderByName("titleHeader", $ReportHeader);
		$mpdfObj->SetHTMLHeaderByName("titleHeader");
		
		// Define Report Footer
		$mpdfObj->DefHTMLFooterByName("emptyFooter", "");
		$mpdfObj->DefHTMLFooterByName("signFooter", $ReportFooter);
		$mpdfObj->SetHTMLFooterByName("emptyFooter");
		
		// Set CSS for Report
		$mpdfObj->writeHTML($ReportCSS);
		$mpdfObj->writeHTML("<body>");
		
		$StudentID = $StudentIDAry[$s]['UserID'];
		
		// Get Report Content
		$reportLoop = $bilingual? 2 : 1;
		for($i=0; $i<$reportLoop; $i++) 
		{
			$thisHTML = "";
			
			if($ReportCardCustomSchoolName == "st_stephen_college")
			{
				$thisHTML .= $lreportcard->FirstPage($ReportID, $StudentID);
				$thisHTML .= $lreportcard->getSubjectPages($ReportID, $StudentID);
			}
			else
			{
				if($ReportCardCustomSchoolName != 'hkuga_college')
				{
					# Get HTML content
					$StudentInfoTable = $eRCtemplate->getReportStudentInfo($ReportID, $StudentID, $forPage3='', $PageNum=1, $PrintTemplateType, ($i > 0 || $ReportLang == "english"));
					$MSTable = $eRCtemplate->getMSTable($ReportID, $StudentID, $PrintTemplateType, $PageNum=1, ($i > 0 || $ReportLang == "english"));
					$MiscTable = $eRCtemplate->getMiscTable($ReportID, $StudentID, $PrintTemplateType, ($i > 0 || $ReportLang == "english"));
					if($ReportCardCustomSchoolName == 'sis'){
						$SignatureTable = $eRCtemplate->getSignatureTable($eRCTemplateSetting['Signature']);
					} else {
						$SignatureTable = $eRCtemplate->getSignatureTable($ReportID, $StudentID, $PrintTemplateType, ($i > 0 || $ReportLang == "english"));
					}
				}
				
				// Add Pagebreak
				if($i > 0){
					$thisHTML .= "<pagebreak />";
					//$thisHTML .= "<!--another_language_version-->";
				}
			
				# Build HTML
				// BIBA MS Transcript
				if($eRCTemplateSetting['Settings']['AutoSelectReportType'] && $isMainReport && $isMSReport){
					$thisHTML .= "<div id=\"container\" style=\"width:297mm; height:210mm; background-color:white;\">";
				}
				else{
					$thisHTML .= "<div id=\"container\" style=\"width:210mm; height:297mm; background-color:white;\">";
				}
				
				if($ReportCardCustomSchoolName == "carmel_alison")
				{
					if($isSimpleReport)
						$thisHTML.= $lreportcard->getSimpleLayout($TitleTable, $StudentInfoTable, $MSTable, $MiscTable, $SignatureTable, $FooterRow, $ReportID, $StudentID);
					else
					{
						$thisHTMLAry = $lreportcard->getLayout($TitleTable, $StudentInfoTable, $MSTable, $MiscTable, $SignatureTable, $FooterRow, $ReportID, $StudentID);
						$thisHTML .= $thisHTMLAry[0].$thisHTMLAry[1];
					}
				}
				else {
					$thisHTML .= $StudentInfoTable;
					$thisHTML .= $MSTable;
					$thisHTML .= $MiscTable;
					$thisHTML .= $SignatureTable;
				}
				$thisHTML .= "</div>";
			}
			$mpdfObj->writeHTML($thisHTML);
		}
		
		// Set Report Footer
		$mpdfObj->SetHTMLFooterByName("signFooter");
		$mpdfObj->writeHTML("</body>");
	
		// Output file to target folder
		$filename = $target_filepath."/".$StudentID.".pdf";
		$mpdfObj->Output($filename, 'F');
		
		// Unset mPDF object
		unset($eRCtemplate->pdf);
		unset($mpdfObj);
	}
}

if ($success===false)
{
	$msg = 'report_archive_failed';
}
else
{
	$msg = 'report_archive';
	
	# Update last report archived date
	$lreportcard->UPDATE_REPORT_LAST_DATE($ReportID, 'LastArchived');
}

intranet_closedb();

$params = ((isset($Semester)) ? "Semester=$Semester&" : "")."msg=$msg";
header("Location: index.php?ClassLevelID=$ClassLevelID&$params");
?>