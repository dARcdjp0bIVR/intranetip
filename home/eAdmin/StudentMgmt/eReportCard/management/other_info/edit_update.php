<?php
// Using: 

#########################################
#
#   Date:   2019-05-01  Bill
#           prevent SQL Injection
#
#########################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");

intranet_auth();
intranet_opendb();

### Handle SQL Injection + XSS [START]
$UploadType = cleanCrossSiteScriptingCode($UploadType);

$TermID = IntegerSafe($TermID);
### Handle SQL Injection + XSS [END]

$lreportcard = new libreportcard();
if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
{
	include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
	$lreportcard = new libreportcardcustom();
}
else
{
	$lreportcard = new libreportcard();
}

$lreportcard->Start_Trans();

$OtherInfoAssocArr = array();
foreach((array)$OtherInfoArr as $thisClassID => $thisClassOtherInfoArr)
{
    $thisClassID = IntegerSafe($thisClassID);
	foreach((array)$thisClassOtherInfoArr as $thisStudentID => $thisStudentOtherInfoArr)
	{
	    $thisStudentID = IntegerSafe($thisStudentID);
		foreach((array)$thisStudentOtherInfoArr as $thisOtherInfoCode => $thisValue)
		{
			//$OtherInfoArr[$thisClassID][$thisStudentID][stripslashes($thisOtherInfoCode)] = stripslashes($thisValue);
		    $OtherInfoAssocArr[$thisClassID][$thisStudentID][stripslashes($thisOtherInfoCode)] = stripslashes($thisValue);
		}
	}
}
$Success = $lreportcard->Update_Other_Info_Data($TermID, $UploadType, $OtherInfoAssocArr);

if($Success)
{
	$lreportcard->Commit_Trans();
	$msg = "UpdateSuccess";
}
else
{
	$lreportcard->RollBack_Trans();
	$msg = "UpdateUnsuccess";
}

header("location: index.php?UploadType=$UploadType&TermID=$TermID&Msg=$msg");
?>