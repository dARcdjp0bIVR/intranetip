<?php

#	Editing by

/**************************************************************
 *	Modification log:
 *
 *  20140801 Ryan
 *  	- Modified function js_Export() for sis cust
 *	20130708 Roy
 *		- Add function js_Export()
 *
 *	20130705 Roy
 *		- Add $FormID for filtering by form
 * 
 ***************************************************************/
 

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
//include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
//include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");

intranet_auth();
intranet_opendb();

if ($plugin['ReportCard2008'])
{
	include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
	include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php"); 
	
	# Temp
	//$ReportCardCustomSchoolName = "st_stephen_college";

	if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") {
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
		$lreportcard = new libreportcardcustom();
	} else {
		$lreportcard = new libreportcard();
	}
	
	$CurrentPage = "Management_OtherInfo";
	$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
    if ($lreportcard->hasAccessRight())
    {
        $linterface = new interface_html();
        $lreportcard_ui = new libreportcard_ui();

		if(!$UploadType)
		{
			$otherInfoTypes = $lreportcard->getOtherInfoType();
			$UploadType = $otherInfoTypes[0];
		}
		
		$TermID = '';
		if (isset($_GET['TermID'])) {
			$TermID = $_GET['TermID'];
		}
		else if (isset($_COOKIE['eRC_Mgmt_OtherInfo_TermID'])) {
			$TermID = $_COOKIE['eRC_Mgmt_OtherInfo_TermID'];
		}
		
		$FormID = '';
		
		if (isset($_GET['FormID'])) {
			$FormID = $_GET['FormID'];
		}
		else if (isset($_COOKIE['eRC_Mgmt_OtherInfo_FormID'])) {
			$FormID = $_COOKIE['eRC_Mgmt_OtherInfo_FormID'];
		}

		$TAGS_OBJ =array();
		$TAGS_OBJ = $lreportcard->getOtherInfoTabObjArr($UploadType);
		$ReturnMsg = $Lang['General']['ReturnMessage'][$Msg];
		$linterface->LAYOUT_START($ReturnMsg);

		echo $linterface->Include_Cookies_JS_CSS();
		echo $lreportcard_ui->Get_Other_Info_Overview_UI($UploadType, $TermID, $FormID);

?>
<script>
$(document).ready( function() {
	<? if($clearCoo) { ?>
		$.cookies.del('eRC_Mgmt_OtherInfo_TermID');
		$.cookies.del('eRC_Mgmt_OtherInfo_FormID');
	<? } ?>
	js_Reload_Other_Info_Table();
});

function js_Reload_Other_Info_Table() {
	var TermID = $("#TermID").val();
	var FormID = $("#FormID").val();
	
	Block_Element("OtherInfoTableDiv");
	$.post(
		"ajax_task.php",
		{
			Task:"Reload_Other_Info_Table",
			UploadType: "<?=$UploadType?>",
			TermID: TermID,
			FormID: FormID
		},
		function(ReturnData){
			$.cookies.set('eRC_Mgmt_OtherInfo_TermID', TermID.toString());
			$.cookies.set('eRC_Mgmt_OtherInfo_FormID', FormID.toString());
			$("div#OtherInfoTableDiv").html(ReturnData);
			UnBlock_Element("OtherInfoTableDiv");
		}
	);	
}

function js_Import() {
	var TermID = $("#TermID").val();

	window.location = "file_upload.php?TermID="+TermID+"&UploadType=<?=$UploadType?>";	
}
<? # 20140731 SIS export ALL Data
if($eRCTemplateSetting['SpecialRemarks']['ExportAllData']){
?>
function js_Export() {
	window.location = "export.php?TermID=&UploadType=<?=$UploadType?>&YearID=";
}
<?}else{?>
function js_Export() {
	var TermID = $("#TermID").val();
	var FormID = $("#FormID").val();
	window.location = "export.php?TermID="+TermID+"&UploadType=<?=$UploadType?>&YearID="+FormID+"";
}
<?}?>
</script>
<?

        $linterface->LAYOUT_STOP();
		intranet_closedb();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>