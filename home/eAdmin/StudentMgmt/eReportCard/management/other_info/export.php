<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
 
intranet_auth();
intranet_opendb();

if (!isset($UploadType) || $UploadType=="")
	header("Location: index.php");

if ($plugin['ReportCard2008']) {
	include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
	include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
	
	
	if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") {
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
		$lreportcard = new libreportcardcustom();
	} else {
		$lreportcard = new libreportcard();
	}
	$lreportcard_ui = new libreportcard_ui();
	
	list($ExportHeader,$ExportData) = $lreportcard->Get_Student_OtherInfo_Export_Data($UploadType, $TermID, $YearClassID, $YearID);
	
	$filename = $lreportcard->Get_OtherInfo_Export_Filename($UploadType, $TermID, $YearClassID, $YearID);
	
	include_once($PATH_WRT_ROOT."includes/libexporttext.php");
	$lexport = new libexporttext();
	$exportContent = $lexport->GET_EXPORT_TXT($ExportData, $ExportHeader);
	
	$lexport->EXPORT_FILE($filename, $exportContent);
		
	intranet_closedb();		
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>