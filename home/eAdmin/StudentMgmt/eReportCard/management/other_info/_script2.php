<?php
# using: 
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");


intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_ui.php");
include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

$lreportcard = new libreportcardrubrics_custom();

$RC_OTHER_INFO_CATEGORY = $lreportcard->Get_Table_Name('RC_OTHER_INFO_CATEGORY');
$RC_OTHER_INFO_ITEM = $lreportcard->Get_Table_Name('RC_OTHER_INFO_ITEM');

$fs = new libfilesystem();
$limport = new libimporttext();

$config = array();
$config[] =array('summary','Summary Info','總結資料');
$config[] =array('award','Awards Record','獎項紀錄');
$config[] =array('merit','Merits & Demerits Record','獎懲紀錄');
$config[] =array('eca','ECA Record','課外活動紀錄');
$config[] =array('remark','Remark','備註');
$config[] =array('interschool','Inter-school Competitions Record','聯校比賽紀錄');
$config[] =array('schoolservice','School Service Record','學校服務紀錄');
$config[] =array('attendance','Attendance Record','考勤紀錄');
$config[] =array('dailyPerformance','Daily Performance Record','日常表現紀錄');
$config[] =array('demerit','Punishment Record','懲罰紀錄');
$config[] =array('OLE','OLE Record','其他學習經歷紀錄');
$config[] =array('assessment','Assessment','評估項目');
$config[] =array('post','Post Record','職責');
$config[] =array('subjectweight','Subject Weight','科目比重');
$config[] =array('others','Others Record','其他紀錄');
$config[] =array('additional_comments','Additional Comment','附加評語');
$config[] =array('position','Class Position','班名次');
$config[] =array('skills','Generic Skills','技能');
$config[] =array('themeForInvestigation','Theme For Investigation','主題調查');
$config[] =array('TFIPerformance','TFI Performance','主題調查表現');

	
$keyed_config = BuildMultiKeyAssoc($config,0);
	
$folderlist = $fs->return_folder(".");
//$lreportcard->Start_Trans();
for($i=0 ; $i<sizeof($folderlist);$i++)
{
	$fs->rs = array();
	$subfolderlist = $fs->return_folderlist($folderlist[$i]);
	
	$SchoolName = basename($folderlist[$i]);
	for($j=0;$j<sizeof($subfolderlist);$j++)
	{
		$filepath = $subfolderlist[$j];
		$config_name = str_replace("_config.csv","",basename($filepath));
		
		list($CateogryCode, $CategoryNameEn, $CategoryNameCh) = $keyed_config[$config_name];
		$sql = "
			INSERT INTO
				$RC_OTHER_INFO_CATEGORY
				(CategoryCode, CategoryNameEn, CategoryNameCh, SchoolName)
				VALUES
				('$CateogryCode','$CategoryNameEn','$CategoryNameCh','$SchoolName')
		";
		
		$success["Category"][$SchoolName] = $lreportcard->db_db_query($sql);
		
		if(!$success["Category"][$SchoolName])
			continue;
		
		$CategoryID = mysql_insert_id();
		
		$data = $limport->GET_IMPORT_TXT($filepath);
		$ItemSqlArr= array();
		for($k=6;$k<sizeof($data);$k++)
		{
			list($ItemNameEn,$ItemNameCh,$RecordType,$Length,$Sample1,$Sample2,$Sample3) = $data[$k];
			$ItemSqlArr[] ="('$ItemNameEn','$ItemNameEn','".substr($ItemNameCh, 1, -1)."','$RecordType','$Length','$Sample1','$Sample2','$Sample3','$CategoryID')";
		}
		$ItemSql = implode(",",$ItemSqlArr);
		
		$sql = "
			INSERT INTO
				$RC_OTHER_INFO_ITEM
				(ItemCode, ItemNameEn, ItemNameCh, RecordType, Length, Sample1, Sample2, Sample3, CategoryID)
				VALUES
				$ItemSql
		";
		
		$success["Item"][$SchoolName][$CateogryCode][$ItemNameEn] =  $lreportcard->db_db_query($sql);
	}
	
}

debug_pr($success);
//$lreportcard->RollBack_Trans();
//debug_pr($folderlist);
	
intranet_closedb();		
?>