<?php
// Using: 

/**************************************************************
 *	Modification log:
 *
 *  20190502 (Bill)
 *      - prevent SQL Injection
 *      - Class Selection > Change 'YearClassID' to 'YearClassSelect' to prevent IntegerSafe() 
 *  
 *	20130705 Roy
 *		- Add $FormID for filtering by form
 * 
 ***************************************************************/

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

### Handle SQL Injection + XSS [START]
$UploadType = cleanCrossSiteScriptingCode($UploadType);

$TermID = IntegerSafe($TermID);
$FormID = IntegerSafe($FormID);

$YearTermID = IntegerSafe($YearTermID);
### Handle SQL Injection + XSS [END]

$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");

# Temp
if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") {
	include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
	$lreportcard = new libreportcardcustom();
} else {
	$lreportcard = new libreportcard();
}
$lreportcard_ui = new libreportcard_ui();

switch ($Task)
{
	case "Reload_Other_Info_Table":
		echo $lreportcard_ui->Get_Other_Info_Overview_Table($UploadType, $TermID, $FormID);
	break;
//	case "Reload_Other_Info_Edit_Table":
//		echo $lreportcard_ui->Get_Other_Info_Edit_Table($UploadType, $TermID, $SelectClassID);
//	break;
	case "Check_Other_Info_Data_Exist":
	    if (is_numeric($YearClassSelect)) {
	        $YearID = $YearClassSelect;
		}
		else {
		    $ClassID = substr($YearClassSelect, 2);
		    $ClassID = IntegerSafe($ClassID);
		}
		echo count($lreportcard_ui->Get_Student_OtherInfo_Data($UploadType, $YearTermID, $ClassID, NULL, NULL, $YearID)) > 0? 1 : 0;
	break;
}

?>