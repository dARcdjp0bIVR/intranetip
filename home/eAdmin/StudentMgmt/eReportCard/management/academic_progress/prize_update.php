<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";
$NoLangWordings = true;

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");

intranet_auth();
intranet_opendb();

if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
{
	include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
	$lreportcard = new libreportcardcustom();
} else {
	$lreportcard = new libreportcard();
}

$ReportID = $_REQUEST['ReportID'];
$AcademicProgressIDArr = $_REQUEST['AcademicProgressIDArr'];
$TargetPrizeStatus = $_REQUEST['TargetPrizeStatus'];
$Success = $lreportcard->Update_Academic_Progress($AcademicProgressIDArr, $TargetPrizeStatus);

$ReturnMsgKey = ($Success)? 'ProgressPrizeUpdateSuccess' : 'ProgressPrizeUpdateFailed';	
$params = "ReportID=$ReportID&ReturnMsgKey=$ReturnMsgKey";

intranet_closedb();
header("Location: view.php?$params");

?>