<?php
# Editing by 
/**************************************************
 * 	Modification:
 *      20180420 Bill:  [2018-0418-1704-29206]
 *          - fixed php error if no records selected and click Set Prize / Cancel Prize
 * 		20110209 Marcus:
 * 			- comment the "Post Cookies handling" coding and "Get Data", which cause improper filtering.
 * ************************************************/

$PageRight = "ADMIN";

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage_ui.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") {
	include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
}

//debug_pr($_POST);
//debug_pr($_COOKIE);
	
### Cookies handling
# set cookies
$arrCookies = array();
$arrCookies[] = array("eRC_Mgmt_Academic_Progress_View_ReportID", "ReportID");
$arrCookies[] = array("eRC_Mgmt_Academic_Progress_View_ClassLevelID", "ClassLevelID");
$arrCookies[] = array("eRC_Mgmt_Academic_Progress_View_YearClassID", "YearClassID");
$arrCookies[] = array("eRC_Mgmt_Academic_Progress_View_MaxClassPosition", "MaxClassPosition");
$arrCookies[] = array("eRC_Mgmt_Academic_Progress_View_MaxFormPosition", "MaxFormPosition");
$arrCookies[] = array("eRC_Mgmt_Academic_Progress_View_PrizeStatus", "PrizeStatus");
$arrCookies[] = array("eRC_Mgmt_Academic_Progress_View_pageNo", "pageNo");
$arrCookies[] = array("eRC_Mgmt_Academic_Progress_View_numPerPage", "numPerPage");
$arrCookies[] = array("eRC_Mgmt_Academic_Progress_View_order", "order");
$arrCookies[] = array("eRC_Mgmt_Academic_Progress_View_field", "field");

//$ReportID = (isset($_POST['ReportID']) && $_POST['ReportID'] != '')? $_POST['ReportID'] : $ReportID;
//$ClassLevelID = (isset($_POST['ClassLevelID']) && $_POST['ClassLevelID'] != '')? $_POST['ClassLevelID'] : $ClassLevelID;
//$YearClassID = (isset($_POST['YearClassID']))? $_POST['YearClassID'] : $YearClassID;
//$MaxClassPosition = (isset($_POST['MaxClassPosition']))? $_POST['MaxClassPosition'] : $MaxClassPosition;
//$MaxFormPosition = (isset($_POST['MaxFormPosition']))? $_POST['MaxFormPosition'] : $MaxFormPosition;
//$PrizeStatus = (isset($_POST['PrizeStatus']))? $_POST['PrizeStatus'] : $PrizeStatus;
//$PrizeStatus = ($PrizeStatus==null)? '' : $PrizeStatus;
//$pageNo = (isset($_POST['pageNo']) && $_POST['pageNo'] != '')? $_POST['pageNo'] : $pageNo;
//$numPerPage = (isset($_POST['numPerPage']) && $_POST['numPerPage'] != '')? $_POST['numPerPage'] : $numPerPage;
//$order = (isset($_POST['order']) && $_POST['order'] != '')? $_POST['order'] : $order;
//$field = (isset($_POST['field']) && $_POST['field'] != '')? $_POST['field'] : $field;

if(isset($clearCoo) && $clearCoo == 1)
{
	clearCookies($arrCookies);
	
	$MaxClassPosition = '';
	$MaxFormPosition = '';
	$pageNo = '';
	$numPerPage = '';
	$order = '';
	$field = '';
	$PrizeStatus = '';
}
else
{
	updateGetCookies($arrCookies);
}

intranet_auth();
intranet_opendb();

$lreportcard = new libreportcard();
$lreportcard_ui = new libreportcard_ui();

if(!$plugin['ReportCard2008'] || !$lreportcard->hasAccessRight())
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	intranet_closedb();
	exit;
}

$CurrentPage = "Management_AcademicProgress";
$linterface = new interface_html();

# Get Data
//$ReportID = $_REQUEST['ReportID'];
//$ClassLevelID = $_REQUEST['ClassLevelID'];
//$Semester = $_REQUEST['Semester'];
//$YearClassID = $_REQUEST['YearClassID'];
//$ReturnMsgKey = $_REQUEST['ReturnMsgKey'];

# tag information
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
$TAGS_OBJ[] = array($eReportCard['Management_AcademicProgress'], "", 0);

$ReturnMsg = $eReportCard['ManagementArr']['AcademicProgressArr']['ReturnMsgArr'][$ReturnMsgKey];
$linterface->LAYOUT_START($ReturnMsg);

### Get Report Info
$ReportInfoArr = $lreportcard->returnReportTemplateBasicInfo($ReportID);
$ClassLevelID = $ReportInfoArr['ClassLevelID'];
$ReportTitlePieces = explode(":_:",$ReportInfoArr["ReportTitle"]);
$ReportTitle = implode("<br>", $ReportTitlePieces);
$ReportTypeDisplay = $lreportcard_ui->Get_Report_Card_Type_Display($ReportID, 1);

$fcm_ui = new form_class_manage_ui();
$ly = new Year($ClassLevelID);
$FormName = $ly->YearName;

### Class Selection
$libFCP_ui = new form_class_manage_ui();
$ClassSelection = $libFCP_ui->Get_Class_Selection($lreportcard_ui->schoolYearID, $ClassLevelID, 'YearClassID',$YearClassID, 'js_Reload();', $noFirst=0, $isMultiple=0, $isAll=1);

### Top 10 Selection
$ClassPositionMaxSelection = $lreportcard_ui->Get_Number_Selection('MaxClassPosition', 1, 10, $MaxClassPosition, 'js_Reload();', $noFirst=0, $isAll=0, $eReportCard['ManagementArr']['AcademicProgressArr']['TopClassPosition']);
$FormPositionMaxSelection = $lreportcard_ui->Get_Number_Selection('MaxFormPosition', 1, 10, $MaxFormPosition, 'js_Reload();', $noFirst=0, $isAll=0, $eReportCard['ManagementArr']['AcademicProgressArr']['TopFormPosition']);

### Prize only selection
$PrizeSelectionArr = array();
$PrizeSelectionArr[1] = $eReportCard['ManagementArr']['AcademicProgressArr']['WithPrize'];
$PrizeSelectionArr[2] = $eReportCard['ManagementArr']['AcademicProgressArr']['WithoutPrize'];
$selectionTags = 'id="PrizeStatus" name="PrizeStatus" onchange="js_Reload();"';
$ProgressPrizeSelection = getSelectByAssoArray($PrizeSelectionArr, $selectionTags, $PrizeStatus, $isAll=0, $noFirst=0, Get_Selection_First_Title($eReportCard['ManagementArr']['AcademicProgressArr']['ProgressPrizeStatus']));

### Remarks
//$showRemark = $linterface->Get_Warning_Message_Box($eReportCard['PromotionInstruction'], $eReportCard['PromotionStatusInstruction']);


### Toolbar (Export & Print)
$toolbar = '';
$toolbar .= $linterface->GET_LNK_EXPORT("javascript:js_Go_Export();", '', '', '', '', 0);
$toolbar .= toolBarSpacer();
$toolbar .= $linterface->GET_LNK_PRINT("javascript:js_Go_Print();", '', '', '', '', 0);


### Generation Date
$thisDisplay = ($ReportInfoArr["LastGeneratedAcademicProgress"]=='' || $ReportInfoArr["LastGeneratedAcademicProgress"]=='0000-00-00 00:00:00')? '--' : $ReportInfoArr["LastGeneratedAcademicProgress"];
$GenerationDate = $eReportCard['LastGenerate'].": ".$thisDisplay;


### Gen Promotion Status Table
if($YearClassID != '' && $YearClassID != 0)
	$StudentArr = $lreportcard->GET_STUDENT_BY_CLASS($YearClassID, '', 0, 0, 0, $ReturnAsso=1);
else
	$StudentArr = $lreportcard->GET_STUDENT_BY_CLASSLEVEL($ReportID, $ClassLevelID, '', '', $ReturnAsso=1);


$StudentIDArr = array_keys($StudentArr);
$SubjectID = $lreportcard->Get_Grand_Field_SubjectID($lreportcard->Get_Grand_Position_Determine_Field());	// Grand Mark
// $AcademicProgressArr[$StudentID][$SubjectID][InfoField...] = value
//$AcademicProgressArr = $lreportcard->Get_Academic_Progress($ReportID, $SubjectID, $StudentIDArr);


### Action Button
$TableActionArr = array();
$TableActionArr[] = array("javascript:js_Change_Prize_Status(1);", $PATH_WRT_ROOT.$image_path."/".$LAYOUT_SKIN."/management/icon_approve.gif", "imgPublic", $eReportCard['ManagementArr']['AcademicProgressArr']['SetPrize']);
$TableActionArr[] = array("javascript:js_Change_Prize_Status(0);", $PATH_WRT_ROOT.$image_path."/".$LAYOUT_SKIN."/management/icon_reject.gif", "imgPrivate", $eReportCard['ManagementArr']['AcademicProgressArr']['CancelPrize']);
$ActionButtonTable = $linterface->Get_Table_Action_Button($TableActionArr);


### Build Table
/*
$display = '';
$display .= "<table border='0' cellspacing='0' cellpadding='4' width='100%'>";
    # table head
    $display .= "<tr class='tabletop'>";
    	$display .= "<td class='tabletopnolink' width='10%'>".$eReportCard["Class"]."</td>";
    	$display .= "<td class='tabletopnolink' width='10%'>".$eReportCard["ClassNo"]."</td>";
    	$display .= "<td class='tabletopnolink' width='30%'>".$eReportCard["Student"]."</td>";
    	$display .= "<td class='tabletopnolink' width='10%'>".$eReportCard['ManagementArr']['AcademicProgressArr']['FromMark']."</td>";
    	$display .= "<td class='tabletopnolink' width='10%'>".$eReportCard['ManagementArr']['AcademicProgressArr']['ToMark']."</td>";
    	$display .= "<td class='tabletopnolink' width='10%'>".$eReportCard['ManagementArr']['AcademicProgressArr']['MarkDifference']."</td>";
    	$display .= "<td class='tabletopnolink' width='10%'>".$eReportCard['ManagementArr']['AcademicProgressArr']['ClassPosition']."</td>";
    	$display .= "<td class='tabletopnolink' width='10%'>".$eReportCard['ManagementArr']['AcademicProgressArr']['FormPosition']."</td>";
    $display .= "</tr>";
 
	# table body
	$DisplayDataArr = array();
	$counter_i = 0;
	$counter_j = 0;
	foreach((array)$AcademicProgressArr as $thisStudentID => $thisStudentProgressArr)
	{
		# Get Student Info
		$thisStudentName = $StudentArr[$thisStudentID]['StudentName'];
		$thisClassName = Get_Lang_Selection($StudentArr[$thisStudentID]['ClassTitleCh'], $StudentArr[$thisStudentID]['ClassTitleEn']);
		$thisClassNumber = $StudentArr[$thisStudentID]['ClassNumber'];
		
		# Get Student Academic Progress Info
		$thisFromScore = $thisStudentProgressArr[$SubjectID]['FromScore'];
		$thisToScore = $thisStudentProgressArr[$SubjectID]['ToScore'];
		$thisScoreDifference = $thisStudentProgressArr[$SubjectID]['ScoreDifference'];
		$thisOrderClassScoreDifference = $thisStudentProgressArr[$SubjectID]['OrderClassScoreDifference'];
		$thisOrderFormScoreDifference = $thisStudentProgressArr[$SubjectID]['OrderFormScoreDifference'];
		
		$rowcss = "tablerow".(($counter_i%2)+1);
		$display .= "<tr class='$rowcss tabletext'>";
	    	$display .= "<td>".$thisClassName."</td>";
	    	$display .= "<td>".$thisClassNumber."</td>";
	    	$display .= "<td>".$thisStudentName."</td>";
	    	$display .= "<td>".$thisFromScore."</td>";
	    	$display .= "<td>".$thisToScore."</td>";
	    	$display .= "<td>".$thisScoreDifference."</td>";
	    	$display .= "<td>".$thisOrderClassScoreDifference."</td>";
	    	$display .= "<td>".$thisOrderFormScoreDifference."</td>";
	    $display .= "</tr>";
	    
	    $counter_j = 0;
	    $DisplayDataArr[$counter_i][$counter_j++] = $thisClassName;
	    $DisplayDataArr[$counter_i][$counter_j++] = $thisClassNumber;
	    $DisplayDataArr[$counter_i][$counter_j++] = $thisStudentName;
	    $DisplayDataArr[$counter_i][$counter_j++] = $thisFromScore;
	    $DisplayDataArr[$counter_i][$counter_j++] = $thisToScore;
	    $DisplayDataArr[$counter_i][$counter_j++] = $thisScoreDifference;
	    $DisplayDataArr[$counter_i][$counter_j++] = $thisOrderClassScoreDifference;
	    $DisplayDataArr[$counter_i][$counter_j++] = $thisOrderFormScoreDifference;
	    $counter_i++;
	}
	
	if ($counter_i == 0)
		$display .= "<tr class='tablerow1'><td class='tabletext' colspan='5' align='center'>".$Lang['General']['NoRecordFound']."</td></tr>";
		
$display .= "</table>";
*/


# Default Table Settings
/*
$PageNumber = ($_POST['pageNo'] == '')? 1 : $_POST['pageNo'];
$PageSize = ($_POST['num_per_page'] == '')? 20 : $_POST['num_per_page'];
$Order = ($_POST['order'] == '')? 1 : $_POST['order'];
$SortField = ($_POST['field'] == '')? 4 : $_POST['field'];
*/
$PageNumber = ($pageNo == '')? 1 : $pageNo;
$PageSize = ($numPerPage == '')? 20 : $numPerPage;
$Order = ($order == '')? 1 : $order;
$SortField = ($field == '')? 4 : $field;


# TABLE INFO
$libdbtable = new libdbtable2007($SortField, $Order, $PageNumber);
$libdbtable->field_array = array("ClassName",
								 "Progress.FromScore",
								 "Progress.ToScore",
								 "Progress.ScoreDifference",
								 "Progress.OrderClassScoreDifference",
								 "Progress.OrderFormScoreDifference",
								 "HavePrize"
								 );
$PrizeStatus = ($PrizeStatus==2)? 0 : $PrizeStatus;

//2013-0415-1143-53140
//$libdbtable->sql = $lreportcard->Get_Academic_Progress_View_Sql($ReportID, $YearClassID, $SubjectID, $ShowStyle=1, $MaxClassPosition, $MaxFormPosition, $PrizeStatus);
$libdbtable->no_col = sizeof($libdbtable->field_array) + 4;
$libdbtable->title = "";
$libdbtable->column_array = array(0,0,0,0);
$libdbtable->wrap_array = array(0,0,0,0);
$libdbtable->IsColOff = 2;
$libdbtable->page_size = $PageSize;
$libdbtable->fieldorder2 = " , yc.Sequence Asc, ycu.ClassNumber Asc";


//2013-0415-1143-53140
// For export
$libdbtable->sql = $lreportcard->Get_Academic_Progress_View_Sql($ReportID, $YearClassID, $SubjectID, $ShowStyle=0, $MaxClassPosition, $MaxFormPosition, $PrizeStatus, $forExport=true);
$exportSql = $libdbtable->built_sql('');

$libdbtable->sql = $lreportcard->Get_Academic_Progress_View_Sql($ReportID, $YearClassID, $SubjectID, $ShowStyle=1, $MaxClassPosition, $MaxFormPosition, $PrizeStatus);


// TABLE COLUMN
$pos = 0;
$libdbtable->column_list .= "<td width='1' class='tableTitle'>#</td>\n";
$libdbtable->column_list .= "<td width='8%' class='tableTitle'>".$libdbtable->column($pos++, $eReportCard["Class"])."</td>\n";
$libdbtable->column_list .= "<td width='10%' class='tableTitle'>".$eReportCard["ClassNo"]."</td>\n";
$libdbtable->column_list .= "<td width='25%' class='tableTitle'>".$eReportCard["Student"]."</td>\n";
$libdbtable->column_list .= "<td width='10%' class='tableTitle'>".$libdbtable->column($pos++, $eReportCard['ManagementArr']['AcademicProgressArr']['FromMark'])."</td>\n";
$libdbtable->column_list .= "<td width='10%' class='tableTitle'>".$libdbtable->column($pos++, $eReportCard['ManagementArr']['AcademicProgressArr']['ToMark'])."</td>\n";
$libdbtable->column_list .= "<td width='10%' class='tableTitle'>".$libdbtable->column($pos++, $eReportCard['ManagementArr']['AcademicProgressArr']['MarkDifference'])."</td>\n";
$libdbtable->column_list .= "<td width='10%' class='tableTitle'>".$libdbtable->column($pos++, $eReportCard['ManagementArr']['AcademicProgressArr']['ClassPosition'])."</td>\n";
$libdbtable->column_list .= "<td width='10%' class='tableTitle'>".$libdbtable->column($pos++, $eReportCard['ManagementArr']['AcademicProgressArr']['FormPosition'])."</td>\n";
$libdbtable->column_list .= "<td width='10%' class='tableTitle'>".$libdbtable->column($pos++, $eReportCard['ManagementArr']['AcademicProgressArr']['ProgressPrize'])."</td>\n";
$libdbtable->column_list .= "<td width=\"1\">".$libdbtable->check("AcademicProgressIDArr[]")."</td>\n";
?>

<script>
function js_Reload()
{
	var ObjForm = document.getElementById('form1');
	ObjForm.action = 'view.php';
	ObjForm.target = '_self';
	ObjForm.submit();
}

function js_Go_Export()
{
	var ObjForm = document.getElementById('form1');
	ObjForm.action = 'export.php';
	ObjForm.target = '_self';
	ObjForm.submit();
	
	js_Restore_Form_Submit();
}

function js_Go_Print()
{
	var ObjForm = document.getElementById('form1');
	ObjForm.action = 'print.php';
	ObjForm.target = '_blank';
	ObjForm.submit();
	
	js_Restore_Form_Submit();
}

function js_Restore_Form_Submit()
{
	var ObjForm = document.getElementById('form1');
	ObjForm.action = 'view.php';
	ObjForm.target = '_self';
}

function js_Go_Back()
{
	var semester = $('input#Semester').val();
	var classLevelID = $('input#ClassLevelID').val();
	
	var params = 'Semester=' + semester + '&ClassLevelID=' + classLevelID;
	window.location='index.php?'+params;
}

function js_Change_Prize_Status(jsPrizeStatus)
{
	if(check_checkbox(document.form1, 'AcademicProgressIDArr[]'))
	{
    	document.getElementById('TargetPrizeStatus').value = jsPrizeStatus;
    	
    	var ObjForm = document.getElementById('form1');
    	ObjForm.action = 'prize_update.php';
    	ObjForm.target = '_self';
    	ObjForm.submit();
	}
	else
	{
		alert('<?=$Lang['General']['JS_warning']['SelectAtLeastOneRecord']?>');
	}
}
</script>

<br/>
<form id="form1" name="form1" method="post" action="view.php" >
	<table width="95%" border="0" cellpadding="4" cellspacing="0">
		<tr>
			<td align="center">
				<table width="100%" border="0" cellspacing="0" cellpadding="2">
					<tr>
						<td width="30%" nowrap="nowrap" class="formfieldtitle tabletext" valign="top"><?=$eReportCard['ReportTitle']?></td>
						<td valign="top" class="tabletext" width="70%"><?=$ReportTitle?></td>
					</tr>
					<tr>
						<td width="30%" nowrap="nowrap" class="formfieldtitle tabletext" valign="top"><?=$eReportCard['ReportType']?></td>
						<td valign="top" class="tabletext" width="70%"><?=$ReportTypeDisplay?></td>
					</tr>
					<tr>
						<td width="30%" nowrap="nowrap" class="formfieldtitle tabletext" valign="top"><?=$eReportCard['Form']?></td>
						<td valign="top" class="tabletext" width="70%"><?=$FormName?></td>
					</tr>
					
					
					<tr><td colspan="2">&nbsp;</td></tr>
					
              		<tr>
              			<td colspan="2"><?=$ClassSelection?> <?=$ClassPositionMaxSelection?> <?=$FormPositionMaxSelection?> <?=$ProgressPrizeSelection?></td>
              		</tr>
              		
              		<tr>
	          			<td colspan="2" align="center"><?=$showRemark?></td>
	          		</tr>
	          		
	          		<tr>
	          			<td colspan="2"><?=$toolbar?></td>
	          		</tr>
	          		<tr>
	              		<td colspan="2" class='tabletextremark'><?=$GenerationDate?></td>
	              	</tr>
	              	
	              	<!--
	              	<tr>
	              		<td colspan="2"><?=$display?></td>
	              	</tr>
	              	-->
	              	
	              	<tr class="table-action-bar">
	          			<td colspan="2" align="right" style="padding-bottom:0px;"><?=$ActionButtonTable?></td>
	          		</tr>
	              	<tr>
	              		<td colspan="2" style="padding-top:0px;"><?=$libdbtable->display()?></td>
	              	</tr>
            	</table>
            	
            	<table width="100%" border="0" cellspacing="0" cellpadding="1" align="center">
					<tr><td class="dotline" colspan="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
					<tr><td align="center" colspan="6">
						<div style="padding-top: 5px">
						<?// $linterface->GET_ACTION_BTN($button_reset, "reset")?>&nbsp;
						<?= $linterface->GET_ACTION_BTN($button_back, "button", "javascript:js_Go_Back();")?>
						</div>
					</td></tr>
				</table>
				
            </td>
		</tr>
	</table>
	
	<input type="hidden" id="ReportID" name="ReportID" value="<?=$ReportID?>">
	<input type="hidden" id="ClassLevelID" name="ClassLevelID" value="<?=$ClassLevelID?>">
	<input type="hidden" id="Semester" name="Semester" value="<?=$Semester?>">
	<!--<input type="hidden" id="DisplayDataArr" name="DisplayDataArr" value="<?=rawurlencode(serialize($DisplayDataArr));?>">-->
	
	<input type="hidden" id="pageNo" name="pageNo" value="<?=$libdbtable->pageNo?>">
	<input type="hidden" id="order" name="order" value="<?=$libdbtable->order?>">
	<input type="hidden" id="field" name="field" value="<?=$libdbtable->field?>">
	<input type="hidden" id="page_size_change" name="page_size_change" value="">
	<input type="hidden" id="numPerPage" name="numPerPage" value="<?=$libdbtable->page_size?>">
	
	<input type="hidden" id="sql_query" name="sql_query" value="<?=intranet_htmlspecialchars($exportSql)?>">	
	
	<input type="hidden" id="TargetPrizeStatus" name="TargetPrizeStatus" value="">
</form>

<br />
<br />

<?
	$linterface->LAYOUT_STOP();
?>