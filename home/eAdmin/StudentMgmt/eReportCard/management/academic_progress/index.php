<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if (!$plugin['ReportCard2008'])
{
	header ("Location: /");
	intranet_closedb();
	exit();
}
 
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
{
	include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
	$lreportcard = new libreportcardcustom();
} else {
	$lreportcard = new libreportcard();
}

if (!$lreportcard->hasAccessRight()) 
{
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lreportcard_ui = new libreportcard_ui();
$linterface = new interface_html();

$CurrentPage = "Management_AcademicProgress";
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();

# tag information
$TAGS_OBJ[] = array($eReportCard['Management_AcademicProgress'], "", 0);

$ReturnMsg = $eReportCard['ManagementArr']['AcademicProgressArr']['ReturnMsgArr'][$ReturnMsgKey];
$linterface->LAYOUT_START($ReturnMsg);

# Semester Selection box
$ReportTemplateTerms = $lreportcard->returnReportTemplateTerms();
$array_term_name = array();
$array_term_data = array();
for($i=0; $i<sizeof($ReportTemplateTerms); $i++) {
	$array_term_data[] = $ReportTemplateTerms[$i][0];
	$array_term_name[] = $ReportTemplateTerms[$i][1];
}
$allTypesOption = $eReportCard['AllTypes'];
$ReportTemplateTermsSelection = getSelectByValueDiffName($array_term_data,$array_term_name,'id="Semester" name="Semester" onChange="window.location=\'index.php?Semester=\'+this.value+\'&ClassLevelID='. $ClassLevelID .'\'"',$Semester,1,0, $allTypesOption);

$Semester = isset($Semester) ? $Semester : "";

# ClassLevelID Selection box
$FormArr = $lreportcard->GET_ALL_FORMS(1);
for($i=0; $i<sizeof($FormArr); $i++) {
	$classLevelName[$FormArr[$i]["ClassLevelID"]] = $FormArr[$i]["LevelName"];
}
$AllFormOption = array(
					0 => array(
						0 => "-1", 
						1 => $eReportCard['AllForms']
					)
				);
$FormArr = array_merge($AllFormOption, $FormArr);

if ($ClassLevelID == "") {
	$ClassLevelID = "-1";
}

# Filters - By Form (ClassLevelID)
$FormSelection = $linterface->GET_SELECTION_BOX($FormArr, 'id="ClassLevelID" name="ClassLevelID" onChange="window.location=\'index.php?ClassLevelID=\'+this.value+\'&Semester='. $Semester .'\'"', "", $ClassLevelID);

$display = $lreportcard_ui->GenReportList_Mangement_AcademicProgress($Semester, $ClassLevelID);

?>

<script language="javascript">
<!--

function js_Go_Generate_Academic_Progress(ReportID)
{
	var semester = document.getElementById('Semester').value;
	var classLevelID = $('select#ClassLevelID').val();
	
	var params = 'ReportID='+ReportID;
	params += (semester != '') ? '&Semester='+semester : '';
	params += (classLevelID != '') ? '&ClassLevelID='+classLevelID : '';
	
	if(confirm("<?=$eReportCard['ManagementArr']['AcademicProgressArr']['jsWarningArr']['ConfirmGenerate']?>"))
	{
		window.location='generate.php?'+params;
	}
}

function js_View_Academic_Progress(ReportID)
{
	var semester = document.getElementById('Semester').value;
	var classLevelID = $('select#ClassLevelID').val();
	
	var params = 'ReportID='+ReportID;
	params += (semester != '') ? '&Semester='+semester : '';
	params += (classLevelID != '') ? '&ClassLevelID='+classLevelID : '';
	
	window.location='view.php?' + params + '&clearCoo=1';
}

//-->
</script>
<br />						
										
<!-- Start Main Table //-->
<table width="96%" border="0" cellspacing="0" cellpadding="5">
  <tr> 
    <td align="center" colspan="2"><table width="100%" border="0" cellspacing="0" cellpadding="2">
      <tr>
        <td align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td align="right"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                
                <tr>
                  <td align="right" class="tabletextremark">
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                          <td><table border="0" cellspacing="0" cellpadding="2">
                              <tr>
                                <td><?=$FormSelection?> <?=$ReportTemplateTermsSelection?></td>
                              </tr>
                          </table></td>
                        <td align="right" valign="bottom"><?=$linterface->GET_SYS_MSG($msg);?></td>
                      </tr>
                  </table></td>
                </tr>
            </table></td>
          </tr>
          <tr>
            <td><table width="100%" border="0" cellspacing="0" cellpadding="4">
                <tr class="tabletop">
                  <td><span class="tabletopnolink"><?=$eReportCard['ReportTitle']?></span></td>
                  <td><span class="tabletopnolink"><?=$eReportCard['Type']?></span></td>
                  <td><span class="tabletopnolink"><?=$eReportCard['Form']?></span></td>
                  <td><span class="tabletopnolink"><?=$eReportCard['Generate']?></span></td>
                  <td><span class="tabletopnolink"><?=$Lang['Btn']['View']?></span></td>
                  <td><span class="tabletopnolink">&nbsp;</span></td>
                </tr>
                
                <?=$display?>
                
            </table></td>
          </tr>
          <tr>
            <td height="1" class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
          </tr>
        </table>
          </td>
      </tr>
    </table>
    </td>
  </tr>
</table>


<!-- End Main Table //-->

<br />                        
                        
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
