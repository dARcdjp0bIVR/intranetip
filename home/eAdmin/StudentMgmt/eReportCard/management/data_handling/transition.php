<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libteaching.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if ($plugin['ReportCard2008']) {
	include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
	include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
	if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
	{
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
		$lreportcard = new libreportcardcustom();
	} else {
		$lreportcard = new libreportcard();
	}
	$lreportcard_ui = new libreportcard_ui();
	$linterface = new interface_html();
	
	if ($lreportcard->hasAccessRight()) {
		
		$currentAcademicYear = $lreportcard->GET_ACADEMIC_YEAR("-");
		//$currentAcademicYear = getCurrentAcademicYear($ParLang='');
		
		$activeAcademicYear = $lreportcard->GET_ACTIVE_YEAR();
		$activeAcademicYearDisplay = $lreportcard->GET_ACTIVE_YEAR("-");
		//$AcademicYearObj = new academic_year($lreportcard->schoolYearID);
		//$activeAcademicYearDisplay = $AcademicYearObj->Get_Academic_Year_Name();
		
		$sql = "SHOW DATABASES LIKE '".$intranet_db."_DB_REPORT_CARD_%'";
		$reportcardDB = $lreportcard->returnVector($sql);
		
		$newAcademicYear = $lreportcard->GET_ACTIVE_YEAR();
		do {
			$newAcademicYear++;
		} while (in_array($intranet_db."_DB_REPORT_CARD_".$newAcademicYear, $reportcardDB));
		
		//$newAcademicYearDisplay = $newAcademicYear."-".($newAcademicYear+1);
		$newAcademicYearDisplay = $lreportcard->Get_Academic_Year_Long_Name($newAcademicYear);
		
		## Check if the data in the next term is complete or not
		$lib_AcademicYear = new academic_year();
		$AcademicYearInfoArr = $lib_AcademicYear->Get_Academic_Year_Info_By_YearName($newAcademicYear);
		$NewAcademicYearID = $AcademicYearInfoArr[0]['AcademicYearID'];
		
		$NewYear_Obj = new academic_year($NewAcademicYearID);
		$NewYearTermArr = $NewYear_Obj->Get_Term_List(0);
		
		if ($NewAcademicYearID=='' || count($NewYearTermArr) == 0)
		{
			$noTermNextYear = 1;
			$disableNewYear = 'disabled';
			$checkedNewYear = '';
			$checkedTransistion = 'checked';
			$disableOtherYearSelect = '';
			
			$warning = '<font color="red"><b> *'.$eReportCard['NextYearTermNotComplete'].'</b></font>';
		}
		else
		{
			$noTermNextYear = 0;
			$disableNewYear = '';
			$checkedNewYear = 'checked';
			$checkedTransistion = '';
			$disableOtherYearSelect = 'disabled';
			
			$warning = '';
		}
		
		
		$otherYearSelectControl = "";
		//$reportcardDB = array("xxx2006", "xxx2007", "xxx2008", "xxx2009", "xxx2010", "xxx2011", "xxx2012", "xxx2013", "xxx2014", "xxx2015");
		# only let user select previous year if there're other years other than the active one
		if (sizeof($reportcardDB) > 1) {
			for($i=0; $i<sizeof($reportcardDB); $i++) {
				$yearDisplay = substr($reportcardDB[$i], -4);
				//$yearPieces = explode('_', $reportcardDB[$i]);
				//$numOfPieces = count($yearPieces);
				
				$correctDbNameSubStr = '_DB_REPORT_CARD_'.$yearDisplay;
				
				if ($yearDisplay != $activeAcademicYear && is_numeric($yearDisplay) && substr($reportcardDB[$i], -20) == $correctDbNameSubStr) {
					//$yearDisplayLong = $yearDisplay."-".($yearDisplay + 1);
					$yearDisplayLong = $lreportcard->Get_Academic_Year_Long_Name($yearDisplay);
					
					$reportcardDBArr[] = array($yearDisplay, $yearDisplayLong);
				}
			}
			
			if (count($reportcardDBArr) > 0) {
				# have other years of eRC => can let the user to switch between years
				$yearRadioDisabled = '';
			}
			else {
				# no other years of eRC => disable the year selection
				$yearRadioDisabled = 'disabled';
				$disableOtherYearSelect = 'disabled';
				$checkedNewYear = 'checked';
				$checkedTransistion = '';
			}
			$otherYearSelect = $linterface->GET_SELECTION_BOX($reportcardDBArr, "name='OtherYear' id='OtherYear' class='' $disableOtherYearSelect onchange=''", "", "");
			
			$otherYearSelectControl = "<br />
										<input type='radio' name='transitionAction' id='transitionAction2' value='other' onclick='toggleAction(this.value)' $checkedTransistion $yearRadioDisabled /> 
										<label for='transitionAction2'>".$eReportCard['OtherAcademicYears']."</label> ".$otherYearSelect;
		}
		
		
		$newAcademicYearWarning = str_replace('<!--academicYearName-->', $activeAcademicYearDisplay, $eReportCard['ConfirmCreateNewYearDatabase']);
		
		############## Interface Start ##############
		$linterface = new interface_html();
		$CurrentPage = "Management_DataHandling";
		$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
		
		# tag information
		$TAGS_OBJ = $lreportcard_ui->Get_Mgmt_DataHandling_Tab_Array('dataTransition');
		
		# page navigation (leave the array empty if no need)
		//$PAGE_NAVIGATION[] = array($eReportCard['Management_DataHandling']);
		$linterface->LAYOUT_START();
?>
<script type="text/JavaScript" language="JavaScript">
<!--
function discardWarning() {
	document.getElementById("WarningMsgRow").style.display = "none";
	document.getElementById("WarningBtnRow").style.display = "none";
	
	document.getElementById("SysMsg").innerHTML = "";
	
	document.getElementById("FormContentRow1").style.display = "";
	document.getElementById("FormContentRow2").style.display = "";
	document.getElementById("FormContentRow3").style.display = "";
	document.getElementById("FormBtnRow").style.display = "";
}

function toggleAction(type) {
	var otherYearSelect = document.getElementById("OtherYear");
	if (type == "other") {
		otherYearSelect.disabled = false;
	} else {
		otherYearSelect.disabled = true;
	}
}

function confirmSubmit() {
	if ($('input#transitionAction1').attr('checked')) {
		return confirm("<?= $newAcademicYearWarning ?>");
	}
	else if ($('input#transitionAction2').attr('checked')) {
		return confirm("<?= $eReportCard['ConfirmTransition'] ?>");
	}
}
-->
</script>

<br />
<form name="form1" action="transition_update.php" method="POST" onsubmit="return confirmSubmit()">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	</tr>
<table>
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>
			<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr>
					<td></td>
					<td id="SysMsg" align="right"><?=$linterface->GET_SYS_MSG($Result);?></td>
				</tr>
				<tr id='WarningMsgRow'>
					<td align='' colspan='2' class='tabletextrequire'>
						<p><?= $eReportCard['DataTransitionWarning1'] ?></p>
						<p><?= $eReportCard['DataTransitionWarning2'] ?></p>
					</td>
				</tr>
				<tr id='FormContentRow1' style='display:none;'>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?= $eReportCard['ActiveAcademicYear'] ?>
					</td>
					<td width="75%" class='tabletext'>
						<?= $activeAcademicYearDisplay ?>
					</td>
				</tr>
				<tr id='FormContentRow2' style='display:none;'>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?= $eReportCard['CurrentAcademicYear'] ?>
					</td>
					<td width="75%" class='tabletext'>
						<?= $currentAcademicYear ?>
					</td>
				</tr>
				<tr id='FormContentRow3' style='display:none;'>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?= $eReportCard['Transition'] ?>
					</td>
					<td width="75%" class='tabletext'>
						<input type="radio" name="transitionAction" id="transitionAction1" value="<?=$newAcademicYear?>" <?=$checkedNewYear?> onclick="toggleAction(this.value)" <?=$disableNewYear?> /> 
						<label for="transitionAction1"><?= $eReportCard['NewAcademicYear']." ($newAcademicYearDisplay) $warning" ?></label>
						<?= $otherYearSelectControl ?>
					</td>
				</tr>
				<tr>
					<td colspan='2' class="dotline">
						<img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1">
					</td>
				</tr>
				<tr id='WarningBtnRow'>
					<td colspan='2' align="center">
						<?= $linterface->GET_ACTION_BTN($button_continue, "button", "discardWarning()") ?>
					</td>
				</tr>
				<tr id='FormBtnRow' style='display:none;'>
					<td colspan='2' align="center">
						<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "") ?>
						<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='transition.php'") ?>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</form>
<?
	//print $linterface->FOCUS_ON_LOAD("form1.ClassLevelID");
  	$linterface->LAYOUT_STOP();
  } else {
?>
You have no priviledge to access this page.
<?
	}
} else {
?>
You have no priviledge to access this page.
<?
}
?>
