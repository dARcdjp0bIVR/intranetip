<?
//using: 

//@set_time_limit(21600);
//@ini_set('max_input_time', 21600);
//@ini_set('memory_limit', -1);
ini_set('zend.ze1_compatibility_mode', '0');

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
/** PHPExcel_IOFactory */
include_once($PATH_WRT_ROOT."includes/phpxls/Classes/PHPExcel/IOFactory.php");
include_once($PATH_WRT_ROOT."includes/phpxls/Classes/PHPExcel.php");

intranet_auth();
intranet_opendb();

$libfs = new libfilesystem();
$libgs = new libgeneralsettings();
$lreportcard = new libreportcardcustom();

$x = "";
$emptyDisplay = "---";

$columnCount = 0;

$MarkArr = array();
$classStudentArr = array();	
$TermReportColumnArr = array();

# POST
$ReportID = $_GET['ReportID'];

# Has Access Right
if ($lreportcard->hasAccessRight()){
	
	# Report Template Info
	$ReportTemplate = $lreportcard->returnReportTemplateBasicInfo($ReportID);
	$YearID = $ReportTemplate['ClassLevelID'];
	$ReportSemester = $ReportTemplate['Semester'];
	
	# Current Academic Year
	$AcademicYearID = $lreportcard->schoolYearID;
	$thisObjYearTerm = new academic_year($AcademicYearID);
	$AcademicYear = $thisObjYearTerm->Get_Academic_Year_Name();
	
	$Subjects = $lreportcard->returnSubjectwOrder($YearID, 0, '', 'b5', '', 0, $ReportID);
	$subject_Filename = $lreportcard->returnSubjectwOrder($YearID, 0, '', 'en', '', 0, $ReportID);
	
	# Year Class		
	$YearClassNameArr = $lreportcard->GET_CLASSES_BY_FORM($YearID, "", 1, 1);
	$YearClassIDArr = array_keys((array)$YearClassNameArr);
	foreach((array)$YearClassIDArr as $YearClassID){
		$classStudents = $lreportcard->GET_STUDENT_BY_CLASS($YearClassID, "", 0, 0, 0, 1);
		$classStudentArr[$YearClassID] = $classStudents;
	}
	
	# Related Reports
	$relatedReports = $lreportcard->Get_Related_TermReport_Of_Consolidated_Report($ReportID, $MainReportOnly=1, $ReportColumnID="ALL");
	// For term report, only process 1 report 
	if($ReportTemplate['Semester'] != 'F'){
		$relatedReports = array();
		$relatedReports[0] = $ReportTemplate;
	} else {
		$relatedReports[] = $ReportTemplate;
	}
	
	# Loop Related Report
	for($i=0; $i<count($relatedReports); $i++){
		
		# Related Report Info
		$relatedReportID = $relatedReports[$i]['ReportID'];
		$TermID = $relatedReports[$i]['Semester'];
			
		# ReportColumn
		$ReportColumn = $lreportcard->returnReportTemplateColumnData($relatedReportID);
		if($TermID != 'F')
		{
			$TermReportColumnArr[$TermID] = BuildMultiKeyAssoc($ReportColumn, array('ReportColumnID'));
			$columnCount++;
		}
			
		foreach ((array)$ReportColumn as $ReportColumnObj){
			if($TermID == 'F'){
				break;
			}	
			$columnCount++;
		}
		
		# Build Mark Array
		foreach((array)$YearClassIDArr as $YearClassID){
			$studentIDArr = array_keys((array)$classStudentArr[$YearClassID]);
			
			$currentMarks = $lreportcard->getMarks($relatedReportID, '', " AND a.StudentID IN ('".implode("','", $studentIDArr)."')");
			$MarkArr[$YearClassID][$TermID] = $currentMarks;
		}
	}
	
	############################################################################
	############### Prepare zip and xls files directory [Start] ################
	############################################################################
		
	### Prepare csv and zip files Folder
	$TempFolder = $intranet_root."/file/temp/erc_to_websams";
	$TempUserFolder = $TempFolder.'/'.$_SESSION['UserID'];
	$TempCsvFolder = $TempUserFolder.'/erc_websams_format';
		
	$ZipFileName = "erc_websams_format.zip";
	$ZipFilePath = $TempUserFolder.'/'.$ZipFileName;
		
	if (!file_exists($TempCsvFolder)) {
		$SuccessArr['CreateTempCsvFolder'] = $libfs->folder_new($TempCsvFolder);
	}
	
	############################################################################
	################ Prepare zip and xls files directory [End] #################
	############################################################################
	# Loop Subjects
	foreach((array)$Subjects as $parentSubjectID => $parentSubject){
		$_xlsFileName = 'Subject_'.$subject_Filename[$parentSubjectID][0].'.xls';
		// [2016-1213-0939-37236] replace "/" in subject name to fix cannot export zip file due to directory problem
		$_xlsFileName = str_replace("/", "", $_xlsFileName);
		
		# Create XLS file
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getProperties()->setCreator("eClass")
									 ->setLastModifiedBy("eClass")
									 ->setTitle("eClass eReportCard records")
									 ->setSubject("eClass eReportCard records")
									 ->setDescription("to websams")
									 ->setKeywords("eClass eReportCard records")
									 ->setCategory("eClass");
		// Create a first sheet
		$objPHPExcel->setActiveSheetIndex(0);
		$ActiveSheet = $objPHPExcel->getActiveSheet();
			
		$row_count = 1;
		
		### Table 1st Header ###
		$x .= "<table border='1'>";
		$x .= "<tr><td>Year</td><td>$acadermicYear</td><td>Subject</td>";
		$ActiveSheet->setCellValue(strtoupper(numberToLetter(1)).$row_count, "Year");
		$ActiveSheet->setCellValue(strtoupper(numberToLetter(2)).$row_count, $AcademicYear);
		$ActiveSheet->setCellValue(strtoupper(numberToLetter(3))."$row_count", "Subject");
			
		$colCount = 4;
		$colPrefix = "";

		# Component Subject Header
		foreach((array)$parentSubject as $componentSubjectID => $subjectName){
			if(!$componentSubjectID)
				continue;
			
			$x .= "<td>".$parentSubject[0]."_".$subjectName."</td>";
			$ActiveSheet->setCellValue(strtoupper(numberToLetter($colCount))."$row_count", $parentSubject[0]."_".$subjectName);
			$colCount++;
				
			for($i=0; $i<($columnCount - 1); $i++){
				$x .= "<td>&nbsp;</td>";
				$ActiveSheet->setCellValue(strtoupper(numberToLetter($colCount))."$row_count", "");	
				$colCount++;
			}
			
			if($ReportSemester == 'F'){
				$x .= "<td>&nbsp;</td>";
				$ActiveSheet->setCellValue(strtoupper(numberToLetter($colCount))."$row_count", "");	
				$colCount++;
			}
		}
		
		# Parent Subject Header
		$x .= "<td>".$parentSubject[0]."_總分</td>";

		$ActiveSheet->setCellValue(strtoupper(numberToLetter($colCount))."$row_count", $parentSubject[0]."_總分");	
		$colCount++;
		
		for($i=0; $i<($columnCount - 1); $i++){
			$x .= "<td>&nbsp;</td>";
				
			$ActiveSheet->setCellValue(strtoupper(numberToLetter($colCount))."$row_count", "");	
			$colCount++;
		}
			
		if($ReportSemester == 'F'){
			$x .= "<td>&nbsp;</td>";
			$ActiveSheet->setCellValue(strtoupper(numberToLetter($colCount))."$row_count", "");	
			$colCount++;
		}
		
		$x .= "</tr>";
		$row_count++;
		
		
		### Table 2nd Header ###
		$x .= "<tr><td>Class</td><td>Class_No</td><td>Name</td>";
		$ActiveSheet->setCellValue(strtoupper(numberToLetter(1))."$row_count", "Class");
		$ActiveSheet->setCellValue(strtoupper(numberToLetter(2))."$row_count", "Class_No");
		$ActiveSheet->setCellValue(strtoupper(numberToLetter(3))."$row_count", "Name");
			
		$colCount = 4;
		$colPrefix = "";
		
		# Component Subject Header
		foreach((array)$parentSubject as $componentSubjectID => $subjectName){
			if(!$componentSubjectID)
				continue;
				
			$subjectFullMark = $lreportcard->GET_SUBJECT_FULL_MARK($componentSubjectID, $YearID, $ReportID);
				
			foreach((array)$TermReportColumnArr as $termID => $TermReportColumn){
				$SemesterSq = $lreportcard->Get_Semester_Seq_Number($termID);
				$assessmentSq = 1;
					
				foreach($TermReportColumn as $ReportColumnID => $reportColumn){
					$assessmentCode = "T".$SemesterSq."A".$assessmentSq;
					
					$x .= "<td>T".$SemesterSq."A".$assessmentSq."_Score ($subjectFullMark)</td>";
					$ActiveSheet->setCellValue(strtoupper(numberToLetter($colCount))."$row_count", $assessmentCode."_Score ($subjectFullMark)");
					$assessmentSq++;
					$colCount++;
				}
				
				$assessmentCode = "T".$SemesterSq;
				
				$x .= "<td>T".$SemesterSq."_Score ($subjectFullMark)</td>";
				$ActiveSheet->setCellValue(strtoupper(numberToLetter($colCount))."$row_count", $assessmentCode."_Score ($subjectFullMark)");
				$colCount++;
			}
			
			if($ReportSemester == 'F'){
				$x .= "<td>&nbsp;</td>";
				$ActiveSheet->setCellValue(strtoupper(numberToLetter($colCount))."$row_count", "Annual_Score ($subjectFullMark)");	
				$colCount++;
			}
			
		}
		
		# Parent Subject Header
		$subjectFullMark = $lreportcard->GET_SUBJECT_FULL_MARK($parentSubjectID, $YearID, $ReportID);
		
		foreach((array)$TermReportColumnArr as $termID => $TermReportColumn){
			$SemesterSq = $lreportcard->Get_Semester_Seq_Number($termID);
			$assessmentSq = 1;
					
			foreach($TermReportColumn as $ReportColumnID => $reportColumn){
				$assessmentCode = "T".$SemesterSq."A".$assessmentSq;
					
				$x .= "<td>T".$SemesterSq."A".$assessmentSq."_Score ($subjectFullMark)</td>";
				$ActiveSheet->setCellValue(strtoupper(numberToLetter($colCount))."$row_count", $assessmentCode."_Score ($subjectFullMark)");
				$assessmentSq++;
				$colCount++;
			}
				
			$assessmentCode = "T".$SemesterSq;
			
			$x .= "<td>T".$SemesterSq."_Score ($subjectFullMark)</td>";
			$ActiveSheet->setCellValue(strtoupper(numberToLetter($colCount))."$row_count", $assessmentCode."_Score ($subjectFullMark)");
			$colCount++;
		}
		
		if($ReportSemester == 'F'){
			$x .= "<td>&nbsp;</td>";
			$ActiveSheet->setCellValue(strtoupper(numberToLetter($colCount))."$row_count", "Annual_Score ($subjectFullMark)");	
			$colCount++;
		}
		
		$x .= "</tr>";
		$row_count++;
		
		### Student Content Row ###
		foreach((array)$YearClassIDArr as $YearClassID){
			foreach((array)$classStudentArr[$YearClassID] as $studentID => $currentStudent){
				
				$x .= "<tr><td>".$currentStudent['ClassName']."</td><td>".$currentStudent['ClassNumber']."</td><td>".$currentStudent['StudentNameCh']."</td>";
				$ActiveSheet->setCellValue(strtoupper(numberToLetter(1))."$row_count", $currentStudent['ClassName']);
				$ActiveSheet->setCellValue(strtoupper(numberToLetter(2))."$row_count", $currentStudent['ClassNumber']);
				$ActiveSheet->setCellValue(strtoupper(numberToLetter(3))."$row_count", $currentStudent['StudentNameCh']);
					
				$colCount = 4;
				$colPrefix = "";
				
				# Component Subject
				foreach($parentSubject as $componentSubjectID => $subjectName){
					if(!$componentSubjectID)
						continue;
					
					foreach($TermReportColumnArr as $termID => $TermReportColumn){
						$columnContent = $MarkArr[$YearClassID][$termID][$studentID][$componentSubjectID];
						
						foreach($TermReportColumn as $ReportColumnID => $reportColumn){
							$colValid = $columnContent[$ReportColumnID]['Grade'] != "N.A.";
							$columnMark = $columnContent[$ReportColumnID]['Mark'];
							$columnMark = $colValid? $columnMark : $emptyDisplay;

							$x .= "<td>".$columnMark."</td>";
							$ActiveSheet->setCellValue(strtoupper(numberToLetter($colCount))."$row_count", $columnMark);
							$colCount++;
						}
						$colValid = $columnContent[0]['Grade'] != "N.A.";
						$columnMark = $columnContent[0]['Mark'];
						$columnMark = $colValid? $columnMark : $emptyDisplay;
						
						$x .= "<td>".$columnMark."</td>";
						$ActiveSheet->setCellValue(strtoupper(numberToLetter($colCount))."$row_count", $columnMark);
						$colCount++;
					}
					
					if($ReportSemester == 'F'){
						$columnContent = $MarkArr[$YearClassID]['F'][$studentID][$componentSubjectID][0];
						$colValid = $columnContent['Grade'] != "N.A.";
						$columnMark = $columnContent['Mark'];
						$columnMark = $colValid? $columnMark : $emptyDisplay;
						
						$x .= "<td>&nbsp;</td>";
						$ActiveSheet->setCellValue(strtoupper(numberToLetter($colCount))."$row_count", $columnMark);
						$colCount++;
					}
					
				}
				
				# Parent Subject
				foreach($TermReportColumnArr as $termID => $TermReportColumn){
					$columnContent = $MarkArr[$YearClassID][$termID][$studentID][$parentSubjectID];
					foreach($TermReportColumn as $ReportColumnID => $reportColumn){
						$colValid = $columnContent[$ReportColumnID]['Grade'] != "N.A.";
						$columnMark = $columnContent[$ReportColumnID]['Mark'];
						$columnMark = $colValid? $columnMark : $emptyDisplay;
						
						$x .= "<td>".$columnMark."</td>";
						$ActiveSheet->setCellValue(strtoupper(numberToLetter($colCount))."$row_count", $columnMark);
						$colCount++;
					}
					
					$colValid = $columnContent[0]['Grade'] != "N.A.";
					$columnMark = $columnContent[0]['Mark'];
					$columnMark = $colValid? $columnMark : $emptyDisplay;
					
					$x .= "<td>".$columnMark."</td>";
					$ActiveSheet->setCellValue(strtoupper(numberToLetter($colCount))."$row_count", $columnMark);
					$colCount++;
				}
					
				if($ReportSemester == 'F'){
					$columnContent = $MarkArr[$YearClassID]['F'][$studentID][$parentSubjectID][0];
					$colValid = $columnContent['Grade'] != "N.A.";
					$columnMark = $columnContent['Mark'];
					$columnMark = $colValid? $columnMark : $emptyDisplay;
					
					$x .= "<td>&nbsp;</td>";
					$ActiveSheet->setCellValue(strtoupper(numberToLetter($colCount))."$row_count", $columnMark);
					$colCount++;
				}
				
				$x .= "</tr>";
				$row_count++;
			}
		}
			
		$path2write = ($row_count<=0) ? $TempCsvFolder.'/NoData_'.$_xlsFileName : $TempCsvFolder.'/'.$_xlsFileName;
		
		# Save XLS File
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$successArr[$parentSubjectID]['GenerateXls'] = $objWriter->save($path2write);
	}
		
	$SettingAry = array();
	$SettingAry['Generate_eRC_WebSAMS_Excel'] = "ReportID: ".$ReportID.", Generation Date: ".date('Y-m-d H:i:s');
	$libgs->Save_General_Setting($lreportcard->ModuleTitle, $SettingAry);
	
	# Update Last Export Data
	$lreportcard->UPDATE_REPORT_LAST_DATE($ReportID, "LastDateExportToWebSAMS");
	
	intranet_closedb();
	
	$SuccessArr['DeleteLastGeneratedZipFile'] = $libfs->file_remove($ZipFilePath);
	$SuccessArr['ZipCsvFiles'] = $libfs->file_zip('erc_websams_format', $ZipFilePath, $TempUserFolder);
	$SuccessArr['DeleteTempCsvFiles'] = $libfs->folder_remove_recursive($TempCsvFolder);
	
	# Output ZIP File
	output2browser(get_file_content($ZipFilePath), $ZipFileName);

} else {
	
	echo "You have no priviledge to access this page.";
	intranet_closedb();
	die();
	
}

?>