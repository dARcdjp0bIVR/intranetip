<?php
// Using: Bill

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$lreportcard = new libreportcardcustom();
$lreportcard_ui = new libreportcard_ui();
$linterface = new interface_html();

if ($lreportcard->hasAccessRight()) {
	
	# Interface
	$linterface = new interface_html();
	$CurrentPage = "Management_DataHandling";
	$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();		
	$TAGS_OBJ = $lreportcard_ui->Get_Mgmt_DataHandling_Tab_Array('exporttoWebSAMS');
	$linterface->LAYOUT_START();
		
	# Current Academic Year
	$AcademicYearID = $lreportcard->schoolYearID;
	$yearTermObj = new academic_year($AcademicYearID);
	$AcademicYear = $yearTermObj->Get_Academic_Year_Name();
		
	# Semester Selection Box
	$ReportTemplateTerms = $lreportcard->returnReportTemplateTerms();
	$array_term_name = array();
	$array_term_data = array();
	for($i=0; $i<sizeof($ReportTemplateTerms); $i++) {
		$array_term_data[] = $ReportTemplateTerms[$i][0];
		$array_term_name[] = $ReportTemplateTerms[$i][1];
	}
	$allTypesOption = $eReportCard['AllTypes'];
	$ReportTemplateTermsSelection = getSelectByValueDiffName($array_term_data,$array_term_name,'name="Semester" onChange="window.location=\'export_to_websams.php?Semester=\'+this.value+\'&ClassLevelID='. $ClassLevelID .'\'"',$Semester,1,0, $allTypesOption);
		
	$Semester = isset($Semester) ? $Semester : "";
		
	# ClassLevelID Selection Box
	$FormArr = $lreportcard->GET_ALL_FORMS(1);
	for($i=0; $i<sizeof($FormArr); $i++) {
		$classLevelName[$FormArr[$i]["ClassLevelID"]] = $FormArr[$i]["LevelName"];
	}
	$AllFormOption = array(0 => array(0 => "-1", 1 => $eReportCard['AllForms']));
	$FormArr = array_merge($AllFormOption, $FormArr);
		
	if ($ClassLevelID == "") {
		$ClassLevelID = "-1";
	}
		
	# Filters - By Form (ClassLevelID)
	$FormSelection = $linterface->GET_SELECTION_BOX($FormArr, 'name="ClassLevelID" onChange="window.location=\'export_to_websams.php?ClassLevelID=\'+this.value+\'&Semester='. $Semester .'\'"', "", $ClassLevelID);
	
	# Table
	$display = $lreportcard_ui->ExportExcelFile_To_WebSAMAS($Semester, $ClassLevelID);
		
?>

<script language="javascript">
<!--
function js_Go_Export_WebSAMS(ReportID)
{
	window.location = 'export_to_websams_generate.php?ReportID=' + ReportID;
}
//-->
</script>
<br />						
										
<!-- Start Main Table //-->
<table width="96%" border="0" cellspacing="0" cellpadding="5">
	<tr><td align="center" colspan="2">
			<table width="100%" border="0" cellspacing="0" cellpadding="2">
    			<tr><td align="left" valign="top">
    					<table width="100%" border="0" cellspacing="0" cellpadding="0">
	          				<tr><td align="right">
	          						<table width="100%" border="0" cellspacing="0" cellpadding="0">
	            						<tr><td>
			                					<table class="form_table_v30">
													<tr>
														<td class="field_title"><?=$eReportCard['CurrentAcadermicYear']?></td>
														<td><?=$AcademicYear?></td>
													</tr>
												</table>
										</td></tr>
										<tr><td>&nbsp;</td></tr>
                						<tr><td align="right" class="tabletextremark">
                  								<table width="100%" border="0" cellspacing="0" cellpadding="0">
                      								<tr>
                      									<td>
                      										<table border="0" cellspacing="0" cellpadding="2">
                              									<tr>
                                									<td><?=$FormSelection?> <?=$ReportTemplateTermsSelection?></td>
                              									</tr>
                          									</table>	
                          								</td>
                        								<td align="right" valign="bottom"><?=$linterface->GET_SYS_MSG($Result);?></td>
                      								</tr>
                  								</table>
                  						</td></tr>
            						</table>
            				</td></tr>
          					<tr><td>
          							<table width="100%" border="0" cellspacing="0" cellpadding="4">
                						<tr class="tabletop">
                  							<td><span class="tabletopnolink"><?=$eReportCard['ReportTitle']?></span></td>
							                <td><span class="tabletopnolink"><?=$eReportCard['Type']?></span></td>
							                <td><span class="tabletopnolink"><?=$eReportCard['Form']?></span></td>
							                <td><span class="tabletopnolink"><?=$eReportCard['LastGenerationDate']?></span></td>
							                <td><span class="tabletopnolink"><?=$eReportCard['LastExportDate']?></span></td>
							                <td><span class="tabletopnolink"><?=$eReportCard['Transfer']?></span></td>
							                <td><span class="tabletopnolink">&nbsp;</span></td>
                						</tr>
                
                						<?=$display?>

            						</table>
            				</td></tr>
          					<tr>
            					<td height="1" class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
          					</tr>
        				</table>
          		</td></tr>
    		</table>
    </td></tr>
</table>

</form>
<!-- End Main Table //-->

<br />

<?
} else {
?>
	You have no priviledge to access this page.
<?
}
$linterface->LAYOUT_STOP();
intranet_closedb();
?>