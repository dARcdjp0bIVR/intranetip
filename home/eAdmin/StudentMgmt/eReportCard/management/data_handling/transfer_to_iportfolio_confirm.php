<?php
// Using: Bill

#### This page is for IP25 only
/*
 * Date: 2018-08-06 (Bill)  [2017-0901-1527-45265]
 *  - Updated Term Assessment handling for HKUGA College
 * Date: 2018-03-07	(Bill)	[2017-1016-1446-12235]
 * 	- Fixed: Transfer duplicate Term Overall result when
 *          - Column 1  >   Term 1
 *          - Overall   >   Term 1
 *      (improved JS checking when submit form)
 * Date: 2018-02-20	(Bill)	[2017-0901-1522-34265]
 * 	- Default Column for Academic Result (Term Assessment) - T1A1 & T1A2 / T2A1 & T2A2  ($eRCTemplateSetting['Report']['HKUGACDataTransferHandling'])
 * Date: 2017-05-12	(Bill)	[2017-0228-0958-20066]
 * 	- Support Multiple Report Column for Academic Result
 */

$PageRight = "ADMIN";

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");

$lreportcard = new libreportcardcustom();
$lreportcard_ui = new libreportcard_ui();
$linterface = new interface_html();

# Access Right
$lreportcard->hasAccessRight();

# Tag
$CurrentPage = "Management_DataHandling";
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
$TAGS_OBJ = $lreportcard_ui->Get_Mgmt_DataHandling_Tab_Array('to_iPf');

$linterface->LAYOUT_START();

# Get Report Info
$reportId = $_GET['ReportID'];
$reportTemplateInfo = $lreportcard->returnReportTemplateBasicInfo($reportId);
$isMainReport = $reportTemplateInfo['isMainReport'];
$reportSemID = $reportTemplateInfo['Semester'];
$reportSemesterNum = $lreportcard->Get_Semester_Seq_Number($reportSemID);
$reportSemesterName = $lreportcard->returnSemesters($reportSemID);
$isYearReport = $reportSemID == "F";

# Get Report Column
$reportColumnAry = $lreportcard->returnReportTemplateColumnData($reportId);

# Build Report Assessment Array (Term Report Only)
$relatedColumnData = array();
if(!$isYearReport)
{
	// Main Term Report
	if($isMainReport)
	{
		// loop Columns
		$reportColumnNum = 1;
		foreach($reportColumnAry as $reportColumnInfo)
		{
			$relatedAssessment = "T".$reportSemesterNum."A".$reportColumnNum++;
			$relatedColumnData[] = array($relatedAssessment, $relatedAssessment);
		}
	}
	
	// Main & Extra Term Report
	if($eRCTemplateSetting['TransferToiPortfolio']['SpecialHandlingForAssessment'])
	{
		// loop Columns
		$reportColumnNum = $reportColumnNum? $reportColumnNum : 1;
		foreach($reportColumnAry as $reportColumnInfo)
		{
			$relatedAssessment = "T".$reportSemesterNum."A".$reportColumnNum++;
			$relatedColumnData[] = array($relatedAssessment, $relatedAssessment);
		}
	}
	
	// Overall Column
	$relatedColumnData[] = array("0", $reportSemesterName);
	
	// [2017-0901-1522-34265] No need to select Columns
	if($eRCTemplateSetting['Report']['HKUGACDataTransferHandling']) {
	   $relatedColumnData = array();
	}
}
$withColumnData = count($relatedColumnData) > 0;

/*
// Check if hide Academic Result Option (Extra Report)
$hideAcademicResult = false;
if(!$eRCTemplateSetting['TransferToiPortfolio']['SpecialHandlingForAssessment'])
	$hideAcademicResult = !$isMainReport;
*/

# Get current acadermic year (input from admin console one)
$acadermicYearID = $lreportcard->schoolYearID;
$thisObjYearTerm = new academic_year($acadermicYearID);
$htmlAry['academicYearName'] = $thisObjYearTerm->Get_Academic_Year_Name();
$htmlAry['reportInfoTable'] = $lreportcard_ui->Get_Settings_ReportCardTemplate_ReportCardInfo_Table($reportId);

$chkName = 'dataTypeAry[]';
$chkClass = 'dataTypeChk';
$chkOnClick = "Uncheck_SelectAll('dataTypeSelectAllChk', this.checked);";

$x = '';
$x .= '<table border="0" cellpadding="0" cellspacing="0" width="100%">'."\n";
	$x .= '<tr>'."\n";
		$x .= '<td width="100%">'."\n";
			// Select All
			$x .= $linterface->Get_Checkbox($ID="dataTypeSelectAllChk", $Name='', $Value='', $isChecked=false, $Class='', $Lang['Btn']['SelectAll'], $Onclick="Set_Checkbox_Value('dataTypeAry[]', this.checked); checkedAcademicResult(); checkedMockExam();")."\n";
			$x .= '<br />'."\n";
			
			// Academic Result
			if ($plugin['iPortfolio'] || $plugin['StudentDataAnalysisSystem'])
			{
				$x .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$linterface->Get_Checkbox('dataType_academicResult', $chkName, 'academicResult', $isChecked=true, $chkClass, $eReportCard['ManagementArr']['DataHandlingArr']['AcademicResult'], $chkOnClick.' checkedAcademicResult(); ');
				
				// Report Column Selection Table
				if($withColumnData)
				{
					// Table Header
					$x .= '<div id="academicResultTable" style="padding-top: 5px; padding-left: 40px;">';
						$x .= '<table width="70%" cellpadding="2" cellspacing="2" border="1" style="border-collapse:collapse;">';
						$x .= '<tr class="tablebluetop">';
							$x .= '<td class="tabletopnolink" width="4%">#</td>';
							$x .= '<td class="tabletopnolink" width="56%">'.$eReportCard['ReportColumn'].'</td>';
							$x .= '<td class="tabletopnolink" width="40%">&nbsp;</td>';
						$x .= '</tr>';
					
					// loop Column
					$reportColumnNum = 1;
					foreach($reportColumnAry as $reportColumnData)
					{
						$reportColumnID = $reportColumnData["ReportColumnID"];
						$relatedAssessment = "T".$reportSemesterNum."A".$reportColumnNum;
						
						$css = ($reportColumnNum % 2 == 0)? 2 : 1;
						$isColumnChecked = $eRCTemplateSetting['TransferToiPortfolio']['SpecialHandlingForAssessment'] && $isMainReport? false : true;
						$isColumnDisabled = $eRCTemplateSetting['TransferToiPortfolio']['SpecialHandlingForAssessment'] && $isMainReport? " disabled " : "";
						
						$x .= '<tr class="tablebluerow'.$css.'">';
							$x .= '<td class="tabletext">&nbsp;'.$reportColumnNum.'</td>';
							$x .= '<td class="tabletext">';
								$x .= $linterface->Get_Checkbox('academicResult_'.$reportColumnID, 'resultColumnID[]', $reportColumnID, $isColumnChecked, 'resultColumnChk', $reportColumnData["ColumnTitle"], ' checkedColumnAssessment(this); ');
							$x .= '</td>';
							$x .= '<td class="tabletext">&nbsp;';
								$x .= $linterface->GET_SELECTION_BOX($relatedColumnData, " id='Assessment_$reportColumnID' name='Assessment[".$reportColumnID."]' $isColumnDisabled ", "", $relatedAssessment);
							$x .= '</td>';
						$x .= '</tr>';
						
						$reportColumnNum++;
					}
					
					// Overall Row (Main Term Report only)
					if($isMainReport)
					{
						$css = ($reportColumnNum % 2 == 0)? 2 : 1;
						$x .= '<tr class="tablebluerow'.$css.'">';
							$x .= '<td class="tabletext">&nbsp;'.$reportColumnNum.'</td>';
							$x .= '<td class="tabletext">';
								$x .= $linterface->Get_Checkbox('academicResult_0', 'resultColumnID[]', 0, true, 'resultColumnChk', $eReportCard['Overall'], ' checkedColumnAssessment(this); ');
							$x .= '</td>';
							$x .= '<td class="tabletext">&nbsp;'.$reportSemesterName.'</td>';
						$x .= '</tr>';
					}
					
					$x .= '</table>';
					
					$x .= $linterface->Get_Form_Warning_Msg('WarningMsgDiv_selectAcademicAssessment', $eReportCard['ManagementArr']['DataHandlingArr']['WarningArr']['DuplicatedSelection'], 'warningMsg');
					$x .= '</div>';
				}
				else 
				{
				    // [2017-0901-1522-34265] Default - 2 Assessment Columns
				    if($eRCTemplateSetting['Report']['HKUGACDataTransferHandling'] && !$isYearReport)
				    {
// 				        $x .= '<input type="hidden" id="academicResult_A1" name="resultColumnID[]" value="T'.$reportSemesterNum.'A1" />';
// 				        $x .= '<input type="hidden" id="academicResult_A2" name="resultColumnID[]" value="T'.$reportSemesterNum.'A2" />';
//				        
// 				        $x .= '<input type="hidden" id="Assessment_A1" name="Assessment[T'.$reportSemesterNum.'A1]" value="T'.$reportSemesterNum.'A1" />';
// 				        $x .= '<input type="hidden" id="Assessment_A2" name="Assessment[T'.$reportSemesterNum.'A2]" value="T'.$reportSemesterNum.'A2" />';
				        
				        // loop Column
				        $reportColumnNum = 1;
				        foreach($reportColumnAry as $reportColumnData)
				        {
				            $reportColumnID = $reportColumnData["ReportColumnID"];
				            $relatedAssessment = "T".$reportSemesterNum."A".$reportColumnNum;
				            
				            $x .= '<input type="hidden" id="academicResult_'.$reportColumnID.'" name="resultColumnID[]" value="'.$reportColumnID.'" />';
				            $x .= '<input type="hidden" id="Assessment_'.$reportColumnID.'" name="Assessment['.$reportColumnID.']" value="'.$relatedAssessment.'" />';
				            
				            $reportColumnNum++;
				            if($reportColumnNum > 2) {
				                break;
				            }
				        }
				    }
				    
					// Hidden field for Overall Column
					$x .= '<input type="hidden" id="academicResult_0" name="resultColumnID[]" value="0" />';
				}
				
				$x .= "<br />\n";
			}
			
			// Mock Exam
			if ($plugin['StudentDataAnalysisSystem'])
			{
				$mockSel = $lreportcard_ui->Get_ReportColumn_Selection($reportId, 'mockExamReportColumnId');
				$mockSelDisplay = str_replace('<!--reportColumn-->', $mockSel, $eReportCard['ManagementArr']['DataHandlingArr']['MockExamScoreFrom']);
				
				$x .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$linterface->Get_Checkbox('dataType_mockExamResult', $chkName, 'mockExamResult', $isChecked=false, $chkClass, $eReportCard['ManagementArr']['DataHandlingArr']['MockExamResult'], $chkOnClick.' checkedMockExam();')."\n";
				$x .= '('.$mockSelDisplay.')';
				$x .= '<br>';
			}
			
			// Class Teacher Comment
			if ($plugin['iPortfolio'])
			{
				$disabled = ($isMainReport)? false : true;
				$x .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$linterface->Get_Checkbox('dataType_classTeacherComment', $chkName, 'classTeacherComment', $isChecked=false, $chkClass, $eReportCard['ManagementArr']['DataHandlingArr']['ClassTeacherComment'], $chkOnClick, $disabled);
				$x .= '<span class="tabletextremark"> ('.$eReportCard['ManagementArr']['DataHandlingArr']['ForMainTermReportAndConsolidatedReportOnly'].')</span>'."\n";
			}
			
			// Conduct
			if ($plugin['iPortfolio'] && $eRCTemplateSetting['TransferToiPortfolio']['TransferConduct'])
			{
				$disabled = ($isMainReport)? false : true;
				
				$x .= '<br />'."\n";
				$x .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$linterface->Get_Checkbox('dataType_conduct', $chkName, 'conduct', $isChecked=false, $chkClass, $eReportCard['ManagementArr']['DataHandlingArr']['Conduct'], $chkOnClick, $disabled);
				$x .= '<span class="tabletextremark"> ('.$eReportCard['ManagementArr']['DataHandlingArr']['ForMainTermReportAndConsolidatedReportOnly'].')</span>'."\n";
			}
			
			// Subject Full Mark
			if ($plugin['iPortfolio'] && $eRCTemplateSetting['TransferToiPortfolio']['TransferSubjectFullMark'])
			{
				$x .= '<br />'."\n";
				$x .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$linterface->Get_Checkbox('dataType_subjectFullMark', $chkName, 'subjectFullMark', $isChecked=false, $chkClass, $eReportCard['ManagementArr']['DataHandlingArr']['SubjectFullMark'], $chkOnClick, $disabled);
			}
			
			$x .= $linterface->Get_Form_Warning_Msg('WarningMsgDiv_selectTransferData', $eReportCard['ManagementArr']['DataHandlingArr']['WarningArr']['SelectData'], 'warningMsg');
		$x .= '</td>'."\n";
	$x .= '</tr>'."\n";
$x .= '</table>'."\n";
$htmlAry['transferDataTypeCheckboxTable'] = $x;

$htmlAry['submitBtn'] = $linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "button", "goSubmit();");
$htmlAry['cancelBtn'] = $linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "goCancel();");
?>

<script language="javascript">
function goSubmit()
{
	var canSubmit = true;
	$('div.warningMsg').hide();
	
	if ($('input.dataTypeChk:checked').length == 0)
	{
		$('div#WarningMsgDiv_selectTransferData').show();
		$('input#dataType_academicResult').focus();
		canSubmit = false;
	}
	
	<? if($withColumnData) { ?>
		var checkedAcademic = $('input#dataType_academicResult').attr('checked');
		if(checkedAcademic)
		{
			var isOverallChecked = false;
			if($('input#academicResult_0')) {
				isOverallChecked = $('input#academicResult_0').attr('checked');
			}
			
			var thisValAry = new Array();
			$('select[name^="Assessment"]:enabled').each(function()
			{
				var thisVal = $(this).val();
				if(jIN_ARRAY(thisValAry, thisVal) || (isOverallChecked && thisVal == '0'))
				{
					$('div#WarningMsgDiv_selectAcademicAssessment').show();
					$('input#dataType_academicResult').focus();
					canSubmit = false;
				}
				thisValAry.push(thisVal);
			});
		}
	<? } ?>
	
	if (canSubmit)
	{
		if(confirm("<?=$eReportCard['jsConfirmToTransferToiPortfolio']?>")) {
			$('form#form1').attr('action', 'transfer_to_iportfolio_update.php').submit();
		}
	}
}

function goCancel()
{
	window.location = 'transfer_to_iportfolio.php';
}

function checkedAcademicResult()
{
	<? if($withColumnData) { ?>
		var isColumnChecked = true;
		var isAssessmentDisabled = '';
		
		var checkedAcademic = $('input#dataType_academicResult').attr('checked');
		if (checkedAcademic)
		{
			$('div#academicResultTable').show();
		}
		else
		{
			$('div#academicResultTable').hide();
			isColumnChecked = false;
			isAssessmentDisabled = 'disabled';
		}
		
		$('input.resultColumnChk').each( function() {
			$(this).attr('checked', isColumnChecked);
		});
		
		$('select[name^="Assessment"]').each( function() {
			$(this).attr('disabled', isAssessmentDisabled);
		});
	<? } ?>
}

function checkedColumnAssessment(obj)
{
	<? if($withColumnData) { ?>
		var checkedColumn = obj.checked;
		var checkedColumnVal = obj.value;
		if(checkedColumnVal > 0)
		{
			if (checkedColumn) {
				$('select#Assessment_'+checkedColumnVal).attr('disabled', '');
			}
			else {
				$('select#Assessment_'+checkedColumnVal).attr('disabled', 'disabled');
			}
		}
		
		var allColumnUnchecked = true;
		$('input.resultColumnChk').each( function()
		{
			var isElementChecked = $(this).attr('checked');
			if (isElementChecked) {
				allColumnUnchecked = false;
			}
		});
		if(allColumnUnchecked)
		{
			$('div#academicResultTable').hide();
			$('input#dataType_academicResult').attr('checked', false);
		}
	<? } ?>
}

function checkedMockExam()
{
	var checkedMock = $('input#dataType_mockExamResult').attr('checked');
	if (checkedMock) {
		$('select#mockExamReportColumnId').attr('disabled', '');
	}
	else {
		$('select#mockExamReportColumnId').attr('disabled', 'disabled');
	}
}

$(document).ready( function () {
	checkedMockExam();
});
</script>

<br />
<!-- Start Main Table //-->
<form id="form1" name="form1" method="post">
	<table class="form_table_v30">
		<tr>
			<td class="field_title"><?=$eReportCard['CurrentAcadermicYear']?></td>
			<td><?=$htmlAry['academicYearName']?></td>
		</tr>
	</table>
	
	<?=$htmlAry['reportInfoTable']?>
	
	<table class="form_table_v30">
		<tr>
			<td class="field_title"><?=$eReportCard['ManagementArr']['DataHandlingArr']['TransferData']?></td>
			<td><?=$htmlAry['transferDataTypeCheckboxTable']?></td>
		</tr>
	</table>
	
	<div class="edit_bottom_v30">
		<?=$htmlAry['submitBtn']?>
		<?=$htmlAry['cancelBtn']?>
	</div>
	
	<input type="hidden" id="ReportID" name="ReportID" value="<?=$reportId?>" />
</div>
</form>
<!-- End Main Table //-->
<br />

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>