<?php
// Using :

##############################################
#
#	Date:   2019-05-02    Bill
#           - prevent Command Injection
#
##############################################

function TRUNCATE_TABLE($db, $tables) {
	global $lreportcard;
	$success = array();
	for($i=0; $i<sizeof($tables); $i++) {
		$sql = "TRUNCATE TABLE $db.".$tables[$i];
		$success[] = $lreportcard->db_db_query($sql);
	}
	return !in_array(false, $success);
}

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

intranet_auth();
intranet_opendb();

$PageRight = "ADMIN";

if (!$plugin['ReportCard2008']) {
	intranet_closedb();
	header("Location:/index.php");
}

if (isset($transitionAction) && $transitionAction != "") {
	if ($transitionAction == "other") {
		if (!isset($OtherYear) || !is_numeric($OtherYear)) {
			header("Location:transition.php");
		}
	}
	else if($transitionAction != "other" && !is_numeric($transitionAction)) {
	    header("Location:transition.php");
	}
}
else {
	header("Location:transition.php");
}

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
$lreportcard = new libreportcard();

if (!$lreportcard->hasAccessRight()) {
	intranet_closedb();
	header("Location:/index.php");
}
$fs = new libfilesystem();

$logFunctionality = 'Data Transition';
$originalYear = $lreportcard->GET_ACTIVE_YEAR();

# Case 1: change active database
if ($transitionAction == "other") {
	$lreportcard->UPDATE_ACTIVE_YEAR($OtherYear);
	$successArr['Update_ActiveYear'] = 1;
	
	$logContent = 'From Year '.$originalYear.' to Year '.$OtherYear;
	$lreportcard->Insert_General_Log($logFunctionality, $logContent);
}
# Case 2: create new database and tables
else {
	$activeDBName = $lreportcard->DBName;
	
	# Step 1: Create the new database
	$newDBName = $intranet_db."_DB_REPORT_CARD_".$transitionAction;
	$sql = "CREATE DATABASE $newDBName";
	$successArr['Create_Database'] = $lreportcard->db_db_query($sql);
	if ($successArr['Create_Database'])
	{
		$SqlHostPara = '';
		if ($sys_custom['MySQL_Server_Host']) {
			$SqlHostPara = "-h ".$sys_custom['MySQL_Server_Host'];
		}
		
		# Step 2: Execute external commands (mysqldump & mysql) as an easy way to duplicate the entire database
		$dumpCommand = "mysqldump $SqlHostPara -u $intranet_db_user --password=$intranet_db_pass $activeDBName | mysql $SqlHostPara -u $intranet_db_user --password=$intranet_db_pass $newDBName";
		exec($dumpCommand);
		
		# Step 3: Truncate all data from these tables, other are preserved so that they don't have to set again
		$tablesToTruncate = array(
								"RC_CLASS_COMMENT_PROGRESS", 
								"RC_MARKSHEET_COMMENT", 
								"RC_MARKSHEET_FEEDBACK", 
								"RC_MARKSHEET_OVERALL_SCORE", 
								"RC_MARKSHEET_SCORE", 
								"RC_MARKSHEET_SUBMISSION_PROGRESS", 
								"RC_MARKSHEET_VERIFICATION_PROGRESS", 
								"RC_OTHER_STUDENT_INFO", 
								"RC_PERSONAL_CHARACTERISTICS_DATA", 
								"RC_REPORT_RESULT", 
								"RC_REPORT_RESULT_ARCHIVE", 
								"RC_REPORT_RESULT_FULLMARK", 
								"RC_REPORT_RESULT_SCORE", 
								"RC_REPORT_RESULT_SCORE_ARCHIVE", 
								"RC_SUB_MARKSHEET_SCORE",
								"RC_MANUAL_ADJUSTMENT",
								"RC_REPORT_CARD_ARCHIVE",
								"RC_STUDENT_ACADEMIC_PROGRESS",
								"RC_AWARD_STUDENT_RECORD",
								"RC_AWARD_GENERATED_STUDENT_RECORD",
								"RC_OTHER_INFO_STUDENT_RECORD",
								"RC_MARKSHEET_SPECIFIC_USER_SUBMISSION_PERIOD",
								"RC_LOG",
								"RC_EXTRA_SUBJECT_INFO",
								"RC_MARKSHEET_TOPIC_SCORE",
								"RC_REPORT_STUDENT_EXTRA_INFO"
							);
		$successArr['Truncate_Table'] = TRUNCATE_TABLE($newDBName, $tablesToTruncate);
		
		# Step 4: Set these datetime field to NULL so the reportcard templates are back to non-generated status
		$sql = "UPDATE $newDBName.RC_REPORT_TEMPLATE SET ";
		$sql .= "MarksheetSubmissionStart = NULL, MarksheetSubmissionEnd = NULL, ";
		$sql .= "MarksheetVerificationStart = NULL, MarksheetVerificationEnd = NULL, ";
		$sql .= "Issued = NULL, LastPrinted = NULL, LastAdjusted = NULL, LastGenerated = NULL, LastDateToiPortfolio = NULL, LastArchived = NULL, ";
		$sql .= "LastMarksheetInput = NULL, LastPromotionUpdate = NULL, LastGeneratedAcademicProgress = NULL , LastGeneratedAward = NULL, ApplySpecificMarksheetSubmission = 0";
		$successArr['Reset_Dates'] = $lreportcard->db_db_query($sql);
		
		# Step 5: Set the active database to the newly created and modfied database
		$successArr['Update_ActiveYear'] = $lreportcard->UPDATE_ACTIVE_YEAR($transitionAction);
		
		# Step 6: Map the New Term ID
		$OldAcademicYearID = $lreportcard->schoolYearID;
		$OldSchoolYear = $lreportcard->schoolYear;
		
		// Get New AcademicYearID
		$new_lreportcard = new libreportcard();
		$NewAcademicYearID = $new_lreportcard->schoolYearID;
		$NewSchoolYear = $new_lreportcard->schoolYear;
		
		// Build the old term and new term mapping
		include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
		$OldYear_Obj = new academic_year($OldAcademicYearID);
		$OldYearTermArr = $OldYear_Obj->Get_Term_List(0);
		$numOfTerm = count($OldYearTermArr);
		
		$NewYear_Obj = new academic_year($NewAcademicYearID);
		$NewYearTermArr = $NewYear_Obj->Get_Term_List(0);
		
		for ($i=0; $i<$numOfTerm; $i++) {
			$thisOldYearTermID = $OldYearTermArr[$i]['YearTermID'];
			$thisNewYearTermID = $NewYearTermArr[$i]['YearTermID'];
			
			$sql = "UPDATE $newDBName.RC_REPORT_TEMPLATE SET Semester='$thisNewYearTermID' WHERE Semester='$thisOldYearTermID' ";
			$successArr['Reset_Report_Term_'.$i] = $lreportcard->db_db_query($sql);
			
			$sql = "UPDATE $newDBName.RC_REPORT_TEMPLATE_COLUMN SET SemesterNum='$thisNewYearTermID' WHERE SemesterNum='$thisOldYearTermID' ";
			$successArr['Reset_Report_Column_Term_'.$i] = $lreportcard->db_db_query($sql);
			
			$sql = "UPDATE $newDBName.RC_CURRICULUM_EXPECTATION SET TermID='$thisNewYearTermID' WHERE TermID='$thisOldYearTermID' ";
			$successArr['Reset_Curriculum_Expectation_'.$i] = $lreportcard->db_db_query($sql);
			
			
			if ($eRCTemplateSetting['CustomizaedTermDateRange']) {
				$thisNewYearTermStart = substr($NewYearTermArr[$i]['TermStart'], 0, 10);
				$thisNewYearTermEnd = substr($NewYearTermArr[$i]['TermEnd'], 0, 10);
				
				$sql = "UPDATE $newDBName.RC_REPORT_TEMPLATE SET TermStartDate='$thisNewYearTermStart', TermEndDate='$thisNewYearTermEnd' WHERE Semester='$thisNewYearTermID' ";
				$successArr['Update_Report_Term_Date_'.$i] = $lreportcard->db_db_query($sql);
			}
		}
		
		if ($eRCTemplateSetting['CustomizaedTermDateRange']) {
			$sql = "UPDATE $newDBName.RC_REPORT_TEMPLATE SET TermStartDate = DATE_ADD(TermStartDate, INTERVAL 1 YEAR), TermEndDate = DATE_ADD(TermEndDate, INTERVAL 1 YEAR)
					Where Semester='F'
							And TermEndDate is not null And TermEndDate != '0000-00-00'
							And TermEndDate is not null And TermEndDate != '0000-00-00'
					";
			$successArr['Update_Report_Term_Date_Consolidated'] = $lreportcard->db_db_query($sql);
		}
		
		### Template File handling
		// update file path in DB
		$sql = "UPDATE $newDBName.RC_REPORT_TEMPLATE_FILE SET FilePath = REPLACE(FilePath, '/file/reportcard2008/".$OldSchoolYear."/templateFile', '/file/reportcard2008/".$NewSchoolYear."/templateFile') ";
		$successArr['Update_Template_File_FilePath'] = $new_lreportcard->db_db_query($sql);
		
		// create Folder if not exist
		$dbFolderPathPrefix = '/file/reportcard2008/'.$NewSchoolYear.'/templateFile';
		$fullFolderPathPrefix = $intranet_root.$dbFolderPathPrefix;
		if (!file_exists($fullFolderPathPrefix)) {
			$successArr['Create_Template_File_Folder'] = $fs->folder_new($fullFolderPathPrefix);
		}
		
		// copy file in filesystem
		$sql = "Select FileID, FileType, FilePath From $newDBName.RC_REPORT_TEMPLATE_FILE";
		$fileAry = $new_lreportcard->returnResultSet($sql);
		$numOfFile = count($fileAry);
		for ($i=0; $i<$numOfFile; $i++) {
			$_fileId = $fileAry[$i]['FileID'];
			$_fileType = $fileAry[$i]['FileType'];
			$_filePath = $fileAry[$i]['FilePath'];
			
			$_fullFolderPathPrefix = $fullFolderPathPrefix.'/'.$_fileType;
			if (!file_exists($_fullFolderPathPrefix)) {
				$successArr['Create_Template_FileType_Folder'][$_fileType] = $fs->folder_new($_fullFolderPathPrefix);
			}
			
			$_oldFilePath = str_replace('/file/reportcard2008/'.$NewSchoolYear.'/templateFile', '/file/reportcard2008/'.$OldSchoolYear.'/templateFile', $_filePath);
			$successArr['Copy_Template_File'][$_fileId] = $fs->file_copy($intranet_root.$_oldFilePath, $intranet_root.$_filePath);
		}
	}
	
	$newYear = $new_lreportcard->GET_ACTIVE_YEAR();
	$logContent = 'From Year '.$originalYear.' to Year '.$newYear.' (NEW)';
	$lreportcard->Insert_General_Log($logFunctionality, $logContent);
}

intranet_closedb();

$Result = (!in_array(false, $successArr)) ? "update" : "update_failed";
header("Location:transition.php?Result=$Result");
?>