<?php

/********************************************************
 * 	modification log
 *  20180628 Bill:  [2017-1204-1601-38164]
 *      set target active year  ($eRCTemplateSetting['Report']['SupportAllActiveYear'])
 *  
 *  20111102 Connie:
 * 	modified 
 * 
 * 	20110613 Marcus:
 * 		modified Class Selection, use Get_Class_Selection in libreportcard_ui instead of form_class_manage_ui.php
 *
 * 	20100611 Marcus:
 * 		add $IncludeAssessmentLib to include assessment lib instead ,add loadStudentList
 *
 * 	20100301 Marcus:
 * 		add param $isAll, $onchange to $RecordType == "Class"
 * *******************************************************/
 
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();

if($IncludeAssessmentLib)
{
	if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
	{
		if (is_file($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName."_assessment.php"))
			include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName."_assessment.php");
		else
			include_once($PATH_WRT_ROOT."includes/reportcard_custom/general_assessment.php");
		
		$lreportcard = new libreportcardcustom();
	} else {
		$lreportcard = new libreportcard();
	}
}
else
{
	if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
	{
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
		$lreportcard = new libreportcardcustom();
	} else {
		$lreportcard = new libreportcard();
	}
}

if($eRCTemplateSetting['Report']['SupportAllActiveYear'])
{
    $targetActiveYear = stripslashes($_REQUEST['ActiveYear']);
    if(!empty($targetActiveYear)){
        $targetYearID = $lreportcard->SET_TEMP_ACTIVE_YEAR($targetActiveYear);
    }
}

$RecordType = stripslashes($_REQUEST['RecordType']);

$returnString = '';
if ($RecordType == "Report")
{
	$YearID = stripslashes($_REQUEST['YearID']);
	$ReportID = stripslashes($_REQUEST['ReportID']);
	$SelectionID = stripslashes($_REQUEST['SelectionID']);
	$onChange = stripslashes($_REQUEST['onChange']);
	$ForVerification = stripslashes($_REQUEST['ForVerification']);
	$ForSubmission = stripslashes($_REQUEST['ForSubmission']);
	$HideNonGenerated = stripslashes($_REQUEST['HideNonGenerated']);
	$OtherTags = stripslashes($_REQUEST['OtherTags']);
	$ForGradingSchemeSettings = stripslashes($_REQUEST['ForGradingSchemeSettings']);
	$excludeReportID = stripslashes($_REQUEST['excludeReportID']);
	$ReportType = trim(stripslashes($_REQUEST['ReportType']));
	$isMainReport = trim(stripslashes($_REQUEST['isMainReport']));
	
	$returnString = Get_Report_Selection($lreportcard, $YearID, $ReportID, $SelectionID,$onChange,$ForVerification, $ForSubmission, $HideNonGenerated, $OtherTags, $ForGradingSchemeSettings, $excludeReportID, $ReportType, $isMainReport);
}
else if ($RecordType == 'ReportColumnSelection') {
	$SelectionID = trim(stripslashes($_POST['SelectionID']));
	$ReportID = trim(stripslashes($_POST['ReportID']));
	$IncludeOverallOption = trim(stripslashes($_POST['IncludeOverallOption']));
	$OverallOptionValue = trim(stripslashes($_POST['OverallOptionValue']));
	$IsAll = trim(stripslashes($_POST['IsAll']));
	$NoFirst = trim(stripslashes($_POST['NoFirst']));
	$FirstTitle = trim(stripslashes($_POST['FirstTitle']));
	
	if ($IncludeOverallOption == '') {
		$IncludeOverallOption = true;
	}
	
	include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
	$lreportcard_ui = new libreportcard_ui();
	if($eRCTemplateSetting['Report']['SupportAllActiveYear'] && !empty($targetActiveYear) && $targetYearID){
	    $lreportcard_ui = new libreportcard_ui($targetYearID);
	}
	echo $lreportcard_ui->Get_ReportColumn_Selection($ReportID, $SelectionID, $SelectedValue='', $IncludeOverallOption, $IsAll, $NoFirst, $FirstTitle, $OverallOptionValue);
}

function Get_Report_Selection($lreportcard, $ClassLevelID, $ReportID, $ID_Name, $ParOnchange = '', $ForVerification = 0, $ForSubmission = 0, $HideNonGenerated = 0, $OtherTags = '', $ForGradingSchemeSettings = 0, $excludeReportID = '', $ReportType = '', $isMainReport = '0')
{
	global $eReportCard, $PATH_WRT_ROOT;
	include_once ($PATH_WRT_ROOT . "includes/form_class_manage.php");
	include_once ($PATH_WRT_ROOT . "includes/libreportcard2008_ui.php");
	$lreportcard_ui = new libreportcard_ui();
	
	// Preset the selected report as the current submission/verification report template if not specified
	if ($ReportID == '' && $ForGradingSchemeSettings != 1) {
		$Period_Conds = '';
		if ($ForVerification)
			$Period_Conds = ' AND NOW() BETWEEN MarksheetVerificationStart AND MarksheetVerificationEnd ';
			else if ($ForSubmission)
				$Period_Conds = ' AND NOW() BETWEEN MarksheetSubmissionStart AND MarksheetSubmissionEnd ';
				
				$conds = " 	ClassLevelID = '$ClassLevelID'
				$Period_Conds
				";
				$ReportInfoArr = $lreportcard->returnReportTemplateBasicInfo('', $conds);
				$ReportID = ($ReportInfoArr['ReportID']) ? $ReportInfoArr['ReportID'] : '';
	}
	
	if ($ForGradingSchemeSettings == 1) {
		$all = 1;
		$noFirst = 0;
		$firstTitle = $eReportCard['AllReportCards'];
	} else {
		$all = 0;
		$noFirst = 1;
	}
	// Get Report List of the ClassLevel
	$ReportInfoArr = $lreportcard->Get_Report_List($ClassLevelID, $ReportType, $isMainReport);
	$numOfReport = count($ReportInfoArr);
	
	$selectionArr = array();
	for ($i = 0; $i < $numOfReport; $i ++) {
		if ($HideNonGenerated == 1 && ($ReportInfoArr[$i]["LastGenerated"] == "0000-00-00 00:00:00" || $ReportInfoArr[$i]["LastGenerated"] == "") || in_array($ReportInfoArr[$i]['ReportID'], (array) $excludeReportID))
			continue;
			
			$thisReportID = $ReportInfoArr[$i]['ReportID'];
			$thisReportTitle = trim(str_replace(':_:', ' ', $ReportInfoArr[$i]['ReportTitle']));
			
			$ReportType = $lreportcard_ui->Get_Report_Card_Type_Display($thisReportID, $ForSelection = 1);
			
			$selectionArr[$thisReportID] = $thisReportTitle . ' (' . $ReportType . ')';
	}
	
	$onchange = '';
	if ($ParOnchange != "")
		$onchange = 'onchange="' . $ParOnchange . '"';
		
		$selectionTags = ' id="' . $ID_Name . '" name="' . $ID_Name . '" ' . $onchange . ' ' . $OtherTags;
		
		$reportSelection = getSelectByAssoArray($selectionArr, $selectionTags, $ReportID, $all, $noFirst, $firstTitle);
		
		return $reportSelection;
}

echo $returnString;
?>