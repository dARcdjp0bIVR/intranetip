<?php
// Modifing by Bill

/***************************************************
 * 	Modification log:
 * 	20170519 Bill:
 * 		- Allow View Group to access (view only)
 * 	20160120 Bill:	[2015-0421-1144-05164]
 * 		- added $eRCTemplateSetting['Management']['MarksheetVerification']['ShowStudentDetails'] logic
 * 			show eAttendance & eDiscipline records below Subject Teacher Comment
 * *************************************************/

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = array("TEACHER", "STUDENT", "PARENT");

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

intranet_auth();
intranet_opendb();

$lreportcard = new libreportcard();
if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
{
	include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
	$lreportcard = new libreportcardcustom();
}
else
{
	$lreportcard = new libreportcard();
}
	
//$CurrentPage = "Management_VerifyAssessmentResult";
$CurrentPage = ($ck_ReportCard_UserType == "STUDENT" || $ck_ReportCard_UserType == "PARENT")?"Management_VerifyAssessmentResult":"Management_MarkSheetRevision";

$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
	
if ($lreportcard->hasAccessRight())
{
	$linterface = new interface_html();
	
	$PAGE_TITLE = $eReportCard['Management_MarksheetVerification'];
	     
	# tag information
	$TAGS_OBJ[] = array($PAGE_TITLE, "", 0);
	$linterface->LAYOUT_START();
	
	if ($ck_ReportCard_UserType == "STUDENT") {
		if ($StudentID != $_SESSION['UserID']) {
			No_Access_Right_Pop_Up();
		}
	}
	else if ($ck_ReportCard_UserType == "PARENT") {
		$luser = new libuser($_SESSION['UserID']);
		$ChildrenList = $luser->getChildrenList();
		$ChlidrenIDAry = Get_Array_By_Key($ChildrenList, 'StudentID');
		
		if (!in_array($StudentID, (array)$ChlidrenIDAry)) {
			No_Access_Right_Pop_Up();
		}
	}
	
	// View Group - View Only
	if($ck_ReportCard_UserType == "VIEW_GROUP")
		$ViewOnly = true;
	
	$ReportID = IntegerSafe($ReportID);
	$SubjectID = IntegerSafe($SubjectID);
	$StudentID = IntegerSafe($StudentID);
	
	# Initilization
	$FeedbackArr = $lreportcard->Get_Marksheet_Feedback($ReportID, $SubjectID, $StudentID);

	###### Get Report Basic Info ######
	$ReportInfoArr = $lreportcard->returnReportTemplateBasicInfo($ReportID);
	$SemID = $ReportInfoArr['Semester'];
	$ReportType = ($SemID=='F')? 'W' : 'T';


	###### Gen Report Title ######
	$ReportTitle = str_replace(":_:","<br>",$lreportcard->Get_Report_Title($ReportID));

	###### Gen Mark Table ######
	$MarkTable = '<table width="70%" border="0" cellspacing="0" cellpadding="10">';
	$MarkTable .= '	<tr>';

	for($i=0; $i<count($ColumnTitle);$i++)
	{ 
		$MarkTable .= '		<td align="center" class="tablebluetop tabletopnolink">'.$ColumnTitle[$i].'</td>';	
		$postValue .= '		<input type="hidden" name="ColumnTitle[]" value="'.$ColumnTitle[$i].'">';
	}

	$MarkTable .= '	</tr>';	
	$MarkTable .= '	<tr class="tablerow2">';

	for($i=0; $i<count($ColumnTitle);$i++)
	{
		$MarkTable .= '		<td align="center" class="tabletext">'.$Score[$StudentID][$SubjectID][$i].'</td>';
		$postValue .= "<input type='hidden' name='Score[$StudentID][$SubjectID][]' value='".$Score[$StudentID][$SubjectID][$i]."'>";
	}
	$MarkTable .= '	</tr>';	
	$MarkTable .= '</table>';

	###### Gen Teacher Comment Area ######
	$SubjectTeacherCommentArr = $lreportcard->returnSubjectTeacherComment($ReportID, $StudentID);
	$SubjectTeacherComment = $SubjectTeacherCommentArr[$SubjectID];
	
	if ($SubjectTeacherComment == '') {
		$CommentStr = '<table id="noComment" style=" width:100%; text-align:center; height:200px;"><tr><td>'.$eReportCard['NoComment'].'</td></tr></table>';
	}
	else {
		$CommentStr = $SubjectTeacherComment;
	}


	$TeacherCommentArea ='';	
	if($eRCTemplateSetting['Management']['MarksheetVerification']['ShowSubjectTeacherComment'])
	{
		$TeacherComDiv = '';
		$TeacherComDiv = '<div id="TeacherCommentArea" style="border:1px solid #ccc; width:60%; padding:15px; text-align:left">';
		$TeacherComDiv .= $CommentStr;
		$TeacherComDiv .= '</div>';
		$TeacherCommentArea=<<<html
		<tr>
			<td>

				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr> 
						<td colspan="6">
							<br />
							{$linterface->GET_NAVIGATION2($eReportCard['Comment'])}		
						</td>
					</tr>
					<tr>
						<td align="center">
							{$TeacherComDiv}
						</td>
					</tr>
				</table>
			</td>
	</tr>
	<tr><td class="dotline" colspan="6"><img src="{$image_path}/{$LAYOUT_SKIN}/10x10.gif" width="10" height="10"></td></tr>	
html;
		
	}
	
	// [2015-0421-1144-05164]
	// Display eAttendance and eDiscipline records
	if($eRCTemplateSetting['Management']['MarksheetVerification']['ShowStudentDetails'] && $ck_ReportCard_UserType == "STUDENT"){
		// Get Student Info
		$studentData = $lreportcard->Get_Student_Profile_Data($ReportID, $StudentID, true, true, true, "", true);
		$studentAttendanceData = $studentData['AttendanceAry'][$StudentID];
		
		// Attendance Table
		$attendanceTable = '<table width="70%" border="0" cellspacing="0" cellpadding="10">';
		$attendanceTable .= '<tr>';
		$attendanceTable .= '<td align="center" class="tablebluetop tabletopnolink" width="35%">'.$Lang['eReportCard']['AdditionalInfo']['Absent']. '('.$Lang['eReportCard']['AdditionalInfo']['Days'].')</td>';
		$attendanceTable .= '<td align="center" class="tablebluetop tabletopnolink" width="35%">'.$Lang['eReportCard']['AdditionalInfo']['Absent']. '('.$Lang['eReportCard']['AdditionalInfo']['Period'].')</td>';
		$attendanceTable .= '<td align="center" class="tablebluetop tabletopnolink" width="30%">'.$Lang['eReportCard']['AdditionalInfo']['Late'].'</td>';	
		$attendanceTable .= '</tr>';
		
		$attendanceTable .= '<tr class="tablerow2">';
		$attendanceTable .= '<td align="center" class="tabletext">'.($studentAttendanceData['Days Absent']>0? $studentAttendanceData['Days Absent'] : 0).'</td>';
		$attendanceTable .= '<td align="center" class="tabletext">0</td>';
		$attendanceTable .= '<td align="center" class="tabletext">'.($studentAttendanceData['Time Late']>0? $studentAttendanceData['Time Late'] : 0).'</td>';
		$attendanceTable .= '</tr>';
		$attendanceTable .= '</table>';
		
		// Merit Table
		$meritTable = '<table width="70%" border="0" cellspacing="0" cellpadding="10">';
		$meritTable .= '<tr>';
		$meritTable .= '<td align="center" class="tablebluetop tabletopnolink" width="16%">'.$Lang['eReportCard']['AdditionalInfo']['Merits'].'</td>';
		$meritTable .= '<td align="center" class="tablebluetop tabletopnolink" width="16%">'.$Lang['eReportCard']['AdditionalInfo']['MinorMerit'].'</td>';
		$meritTable .= '<td align="center" class="tablebluetop tabletopnolink" width="16%">'.$Lang['eReportCard']['AdditionalInfo']['MajorMerit'].'</td>';
		$meritTable .= '<td align="center" class="tablebluetop tabletopnolink" width="16%">'.$Lang['eReportCard']['AdditionalInfo']['Demerits'].'</td>';
		$meritTable .= '<td align="center" class="tablebluetop tabletopnolink" width="16%">'.$Lang['eReportCard']['AdditionalInfo']['MinorDemerit'].'</td>';
		$meritTable .= '<td align="center" class="tablebluetop tabletopnolink" width="16%">'.$Lang['eReportCard']['AdditionalInfo']['MajorDemerit'].'</td>';	
		$meritTable .= '</tr>';
		
		$meritTable .= '<tr class="tablerow2">';
		$meritTable .= '<td align="center" class="tabletext">'.(intval($studentData[1])>0? intval($studentData[1]) : 0).'</td>';
		$meritTable .= '<td align="center" class="tabletext">'.(intval($studentData[2])>0? intval($studentData[2]) : 0).'</td>';
		$meritTable .= '<td align="center" class="tabletext">'.(intval($studentData[3])>0? intval($studentData[3]) : 0).'</td>';
		$meritTable .= '<td align="center" class="tabletext">'.(intval($studentData[-1])>0? intval($studentData[-1]) : 0).'</td>';
		$meritTable .= '<td align="center" class="tabletext">'.(intval($studentData[-2])>0? intval($studentData[-2]) : 0).'</td>';
		$meritTable .= '<td align="center" class="tabletext">'.(intval($studentData[-3])>0? intval($studentData[-3]) : 0).'</td>';
		$meritTable .= '</tr>';	
		$meritTable .= '</table>';
	}

	###### Gen Feedback Area ######
	if(count($FeedbackArr)==0 || !$FeedbackArr[$StudentID][$SubjectID]['Comment'])
	{
		$CommentList = '<table id="noFeedback" style=" width:100%; text-align:center; height:200px;"><tr><td>'.$eReportCard['NoFeedback'].'</td></tr></table>';
	}
	else
	{
		$CommentList = $FeedbackArr[$StudentID][$SubjectID]['Comment'];
	}
	$FeedbackID = $FeedbackArr[$StudentID][$SubjectID]['FeedbackID'];

	$FeedbackArea = '<div id="FeedbackArea" style="border:1px solid #ccc; width:60%; padding:15px; text-align:left">';
	$FeedbackArea .= $CommentList;
	$FeedbackArea .= '</div>';
	
	
	###### Gen Back Btn ######
	if ($ck_ReportCard_UserType == "TEACHER" || $ck_ReportCard_UserType == "ADMIN" || $ck_ReportCard_UserType == "VIEW_GROUP")
	{
		$url = "window.location.href='../marksheet_revision/marksheet_feedback/index.php?ClassLevelID=$ClassLevelID&ReportID=$ReportID&SubjectID=$SubjectID&SubjectGroupID=$SubjectGroupID&ClassID=$ClassID'";
		$ViewMarksheetBtn = $linterface->GET_ACTION_BTN($eReportCard['EditMarksheet'],"button"," window.location.href='../marksheet_revision/marksheet_edit.php?ClassLevelID=$ClassLevelID&ReportID=$ReportID&SubjectID=$SubjectID&SubjectGroupID=$SubjectGroupID&StudentID=$StudentID&ClassID=$ClassID'");
	}
	else
	{
		$url = "window.location.href='index.php?ReportID=$ReportID'";	
	}
	$BackBtn = $linterface->GET_ACTION_BTN($button_back,"button",$url);
}

?>

<script language="javascript">
function limitText(limitField, limitNum) {
	if (limitField.value.length > limitNum) {
		limitField.value = limitField.value.substring(0, limitNum);
	}
}
</script>


<br/>
<form name="form1" method="POST" action="feedback_update.php">
<table width="98%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<?=$toolbarRow?>
			</table>
		</td>
	</tr>
	<tr><td>&nbsp;</td></tr>
	<tr>
		<td>
			<table width="100%" border="0" cellpadding="5" cellspacing="1">
				<tr>
					<td width="10%" nowrap="nowrap" class="formfieldtitle tabletext" valign="top"><?=$eReportCard['ReportTitle']?></td>
					<td valign="top" class="tabletext">
						<?=$ReportTitle?>
					</td>
				</tr>	
				<tr>
					<td width="10%" nowrap="nowrap" class="formfieldtitle tabletext" valign="top"><?=$eReportCard['Subject']?></td>
					<td valign="top" class="tabletext">
						<?=$lreportcard->GET_SUBJECT_NAME_LANG($SubjectID)?>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr><td>&nbsp;</td></tr>
	<tr><td class="dotline" colspan="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
	<tr>
		<td>
			<!-- Mark -->
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr> 
					<td colspan="6">
						<br />
						<?=$linterface->GET_NAVIGATION2($eReportCard['Mark'])?>		
					</td>
				</tr>
				<tr>
					<td align="center">
						<?=$MarkTable?>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr><td>&nbsp;</td></tr>
	<tr><td class="dotline" colspan="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>

	<?php if($eRCTemplateSetting['Management']['MarksheetVerification']['ShowStudentDetails'] && $ck_ReportCard_UserType == "STUDENT"){ ?>
		
	<tr><td>
			<!-- Attendance Records -->
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr><td colspan="6">
						<br />
						<?=$linterface->GET_NAVIGATION2($Lang['eReportCard']['AdditionalInfo']['Attendance'])?>		
				</td></tr>
				<tr><td align="center">
						<?=$attendanceTable?>
				</td></tr>
			</table>
	</td></tr>
	<tr><td>&nbsp;</td></tr>
	<tr><td class="dotline" colspan="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
		
	<tr><td>
			<!-- Merits and Demerits -->
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr><td colspan="6">
						<br />
						<?=$linterface->GET_NAVIGATION2($Lang['eReportCard']['AdditionalInfo']['MeritsDemerits'])?>		
				</td></tr>
				<tr><td align="center">
						<?=$meritTable?>
				</td></tr>
			</table>
	</td></tr>
	<tr><td>&nbsp;</td></tr>
	<tr><td class="dotline" colspan="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
	
	<?php } ?>
	
	<!-- Teacher Comment -->
	<?=$TeacherCommentArea?>

	<tr>
		<td>
			<!-- Feedback -->
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr> 
					<td colspan="6">
						<br />
						<?=$linterface->GET_NAVIGATION2($eReportCard['Feedback'])?>		
					</td>
				</tr>
				<tr>
					<td align="center">
						<?=$FeedbackArea?>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	
	<? if($lreportcard->IS_ADMIN_USER($_SESSION['UserID']) || !$ViewOnly) { ?>
	<tr><td><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="10"></td></tr>
	<tr>
		<td>
			<!-- Feedback -->
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td valign="top" width="10%"><?=$eReportCard['Feedback']?>:</td>
					<td><?=$linterface->GET_TEXTAREA("Comment", "",70,5,"","","onkeyup='limitText(this,".$lreportcard->textAreaMaxChar.")'")?></td>
				</tr>
			</table>
		</td>
	</tr>
	<? } ?>
	
	<tr><td><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="10"></td></tr>
	<tr>
		<td align="center">
		<? if($lreportcard->IS_ADMIN_USER($_SESSION['UserID']) || !$ViewOnly) { ?>
			<?=$linterface->GET_ACTION_BTN($button_submit,"submit","")?>
			<img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="10">
		<? } ?>
			<?=$ViewMarksheetBtn?>
			<img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="10">
			<?=$BackBtn?>
		</td>
	</tr>
</table>
<?=$postValue?>
<input type="hidden" name="ClassID" value="<?=$ClassID?>">
<input type="hidden" name="SubjectID" value="<?=$SubjectID?>">
<input type="hidden" name="ReportID" value="<?=$ReportID?>">
<input type="hidden" name="StudentID" value="<?=$StudentID?>">
<input type='hidden' name='ClassLevelID' value='<?=$ClassLevelID?>'>
<input type='hidden' name='SubjectGroupID' value='<?=$SubjectGroupID?>'>
<input type='hidden' name='FeedbackID' value='<?=$FeedbackID?>'>

</form>
  		
<br />
<br />
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>