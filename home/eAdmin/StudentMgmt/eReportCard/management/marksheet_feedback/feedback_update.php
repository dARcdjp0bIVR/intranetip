<?php

$PATH_WRT_ROOT = "../../../../../../";
//$PageRight = array("TEACHER", "STUDENT", "PARENT");

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");

intranet_auth();
intranet_opendb();

if ($ck_ReportCard_UserType == "STUDENT") {
	if ($StudentID != $_SESSION['UserID']) {
		No_Access_Right_Pop_Up();
	}
}
else if ($ck_ReportCard_UserType == "PARENT") {
	$luser = new libuser($_SESSION['UserID']);
	$ChildrenList = $luser->getChildrenList();
	$ChlidrenIDAry = Get_Array_By_Key($ChildrenList, 'StudentID');
	
	if (!in_array($StudentID, (array)$ChlidrenIDAry)) {
		No_Access_Right_Pop_Up();
	}
}

$ReportID = IntegerSafe($ReportID);
$SubjectID = IntegerSafe($SubjectID);
$StudentID = IntegerSafe($StudentID);
$FeedbackID = IntegerSafe($FeedbackID);
$ClassID = IntegerSafe($ClassID);
$ClassLevelID = IntegerSafe($ClassLevelID);
$SubjectGroupID = IntegerSafe($SubjectGroupID);

$lreportcard = new libreportcard();
	
if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
{
	include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
	$lreportcard = new libreportcardcustom();
}
else
{
	$lreportcard = new libreportcard();
}

# INSERT OR UPDATE NEW Feedback
$lreportcard->INSERT_FEEDBACK($ReportID,$SubjectID,$StudentID,$Comment,$FeedbackID);

for($i=0; $i<count($ColumnTitle);$i++)
{
	$postValue .= '<input type="hidden" name="ColumnTitle[]" value="'.$ColumnTitle[$i].'">';
	$postValue .= "<input type='hidden' name='Score[$StudentID][$SubjectID][]' value='".$Score[$StudentID][$SubjectID][$i]."'>";
}
//header("location: view_feedback.php?SubjectID=".$SubjectID."&ReportID=".$ReportID."&StudentID=".$StudentID."&ClassLevelID=".$ClassLevelID."&SubjectGroupID=".$SubjectGroupID);
?>

<html>
<body>
<form name="form1" action="view_feedback.php" method="POST">
<?=$postValue?>
<input type="hidden" name="SubjectID" value="<?=$SubjectID?>"/>
<input type="hidden" name="ReportID" value="<?=$ReportID?>"/>
<input type="hidden" name="StudentID" value="<?=$StudentID?>"/>
<input type="hidden" name="ClassID" value="<?=$ClassID?>"/>
<input type="hidden" name="ClassLevelID" value="<?=$ClassLevelID?>"/>
<input type="hidden" name="SubjectGroupID" value="<?=$SubjectGroupID?>"/>
<input type="hidden" name="FeekbackID" value="<?=$FeedbackID?>"/></form>
</body>
</html>
<script>
document.form1.submit();
</script>