<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");

intranet_auth();
intranet_opendb();

if ($plugin['ReportCard2008']) {
	
	# Library definition
	if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
	{
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
		$lreportcard = new libreportcardcustom();
	}
	else
	{
		$lreportcard = new libreportcard();
	}

	if ($lreportcard->hasAccessRight()) {
		
		$ExportArr = unserialize(rawurldecode($_POST['ExportArr']));
		
		# csv header
		$exportColumn = array_shift($ExportArr);
		
		$lexport = new libexporttext();
		
		# file name
		$ClassArr = $lreportcard->GET_CLASSES_BY_FORM($ClassLevelID, $ClassID);
		$ClassName = $ClassArr[0]['ClassName'];
		
		$ReportSetting = $lreportcard->returnReportTemplateBasicInfo($ReportID);
		$ReportTitle = $ReportSetting['ReportTitle'];
		$ReportTitle = str_replace(":_:", "_", $ReportTitle);
		$ReportTitle = intranet_htmlspecialchars($ReportTitle);
		
		$filename = $ClassName."_".$ReportTitle."_"."conduct_mark.csv";
		
		
		$export_content = $lexport->GET_EXPORT_TXT($ExportArr, $exportColumn, "", "\r\n", "", 0, "11");
		
		intranet_closedb();
		
		// Output the file to user browser
		$lexport->EXPORT_FILE($filename, $export_content);
	} else {
		echo "You have no priviledge to access this page.";
	}
} else {
	echo "You have no priviledge to access this page.";
}
?>