<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if (!$plugin['ReportCard2008'])
{
	echo "You have no priviledge to access this page.";
}

if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
{
	include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
	$lreportcard = new libreportcardcustom();
}
else
{
	$lreportcard = new libreportcard();
}
$linterface = new interface_html();


if (!$lreportcard->hasAccessRight())
{
	echo "You have no priviledge to access this page.";
}

include_once($PATH_WRT_ROOT."templates/2007a/layout/print_header.php");


## Get Data from index page
$ClassLevelID = $_POST['ClassLevelID'];
$ClassID = $_POST['ClassID'];
$ReportID = $_POST['ReportID'];

$ExportArr = unserialize(rawurldecode($_POST['ExportArr']));
$HeaderArr = array_shift($ExportArr);
$numOfColumn = count($HeaderArr);
$numOfStudent = count($ExportArr);

$LastModifiedDate = $_POST['LastModifiedDate'];
$LastModifiedDisplay = $eReportCard['LastModifiedDate'].": ".$LastModifiedDate;


## Report Title
$ClassArr = $lreportcard->GET_CLASSES_BY_FORM($ClassLevelID, $ClassID);
$ClassName = $ClassArr[0]['ClassName'];

$ReportSetting = $lreportcard->returnReportTemplateBasicInfo($ReportID);
$ReportTitle = $ReportSetting['ReportTitle'];
$ReportTitle = intranet_htmlspecialchars($ReportTitle);
$ReportTitle = str_replace(":_:", "<br />", $ReportTitle);

$Title = $ReportTitle."<br />".$ClassName." ".$eReportCard['Management_ConductMark'];



## Build Mark Table
$markTable = "<table id='MarkTable' width='100%' border='1' cellpadding='2' cellspacing='0'>";
	# Title
	$markTable .= "<thead>";
		$markTable .= "<tr>";
			for ($i=0; $i<$numOfColumn; $i++)
			{
				$thisTitle = $HeaderArr[$i];
				$markTable .= "<th>".$thisTitle."</th>";
			}
		$markTable .= "</tr>";
	$markTable .= "</thead>";
	
	# Content
	$markTable .= "<tbody>";
		for ($i=0; $i<$numOfStudent; $i++)
		{
			$thisStudentInfoArr = $ExportArr[$i];
			
			$markTable .= "<tr>";
			for ($j=0; $j<$numOfColumn; $j++)
			{
				$thisDisplay = $thisStudentInfoArr[$j];
				$align = ($j==1)? "left" : "center";
				$markTable .= "<td align='".$align."'>".$thisDisplay."</td>";
			}
			$markTable .= "</tr>";
			
		}
	$markTable .= "</tbody>";
$markTable .= "</table>";
?>

<? if($eRtemplateCSS) {?><link href="<?=$eRtemplateCSS?>" rel="stylesheet" type="text/css" /><? } ?>

<style type='text/css' media='print'>
	.print_hide {display:none;}
</style>

<style type='text/css'>
<!--
h1 {
	font-size: 20px;
	font-family: arial, "lucida console", sans-serif;
	margin: 0px;
	padding-top: 3px;
	padding-bottom: 10px;
}

#MarkTable {
	border-collapse: collapse;
	border-color: #000000;
}

#MarkTable thead {
	font-size: 12px;
	font-family: arial, "lucida console", sans-serif;
}

#MarkTable tbody {
	font-size: 12px;
	font-family: arial, "lucida console", sans-serif;
}
.footer {
	font-family: "Arial", "Lucida Console";	
	font-size: 12px;
}
.signature {
	font-family: "Arial", "Lucida Console";	
	font-size: 14px;
}
.signature {
	font-family: "Arial", "Lucida Console";	
	font-size: 14px;
}
.stat_start_border_top {
	border-top-width: 2px;
	border-top-style: solid;
	border-top-color: #000000;
}
-->
</style>

<table border="0" cellpadding="4" width="95%" cellspacing="0" class="print_hide">
	<tr><td align="right"><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></span></td></tr>
</table>

<form name="form1" method="post">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr><td align="center"><h1><?= $Title ?></h1></td></tr>
		
		<tr><td><?= $markTable ?></td></tr>
		<tr><td>&nbsp;</td></tr>
		
		<tr><td class="tabletext"><span class="tabletextremark"><?= $LastModifiedDisplay ?></span></td></tr>
		
	</table>
	
</form>

<table border="0" cellpadding="4" width="95%" cellspacing="0" class="print_hide">
	<tr><td align="right"><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></span></td></tr>
</table>


<?
intranet_closedb();
?>