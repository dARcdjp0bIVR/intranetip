<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if (!$plugin['ReportCard2008'])
{
	echo "You have no priviledge to access this page.";
}
	
	
if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
{
	include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
	$lreportcard = new libreportcardcustom();
}
else
{
	$lreportcard = new libreportcard();
}

if (!$lreportcard->hasAccessRight())
{
	echo "You have no priviledge to access this page.";
}

# initialization
$ClassLevelID = $_POST['ClassLevelID'];
$ClassID = $_POST['ClassID'];
$ReportID = $_POST['ReportID'];
$StudentIDArr = unserialize(rawurldecode($_POST['StudentIDArr']));
$numOfStudent = count($StudentIDArr);

$thisFinalConductInfoArr = $lreportcard->Get_Student_Final_Conduct_Grade($StudentIDArr, $ReportID);
$InsertInfoArr = array();
$UpdateInfoArr = array();
for ($i=0; $i<$numOfStudent; $i++)
{
	$thisStudentID = $StudentIDArr[$i];
	$thisFinalConductMark = $thisFinalConductInfoArr[$thisStudentID]["Conduct"];
	
	$thisInfoArr = array();
	$thisInfoArr['StudentID'] = $thisStudentID;
	$thisInfoArr['ReportID'] = $ReportID;
	$thisInfoArr['Conduct'] = ${"finalGrade_".$thisStudentID};
		
	if ($thisFinalConductMark == "")
	{
		$InsertInfoArr[] = $thisInfoArr;
	}
	else
	{
		$UpdateInfoArr[] = $thisInfoArr;
	}
}


$successArr = array();
if (count($InsertInfoArr) > 0)
{
	$successArr['insert'] = $lreportcard->Insert_Student_Final_Conduct_Grade($InsertInfoArr);
}	
if (count($UpdateInfoArr) > 0)
{
	$successArr['update'] = $lreportcard->Update_Student_Final_Conduct_Grade($UpdateInfoArr);
}
	
intranet_closedb();

if (in_array(false, $successArr))
	$result = "update_failed";
else
	$result = "update";
	


$URL = "index.php";
$URL .= "?Result=".$result."&ClassLevelID=".$ClassLevelID."&ReportID=".$ReportID;

header("Location: $URL");

?>