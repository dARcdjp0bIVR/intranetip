<?php
# Edited by Bill

/***************************************************
 * 	Modification log:
 * 	20160120 Bill:	[2015-0421-1144-05164]
 * 	- skip eDiscipline conduct mark checking
 * 		$eRCTemplateSetting['ReportGeneration']['PromotionNotCheckConduct']
 * 	- added tag for Pooi To Promotion Meeting Report
 * 		$eRCTemplateSetting['Report']['PromotionSummaryReport']
 * *************************************************/
 
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
//include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
//include_once($PATH_WRT_ROOT."includes/subject_class_mapping_ui.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if ($plugin['ReportCard2008'])
{
	include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
	include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
	if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
	{
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
	}
	
	/* Temp Library*/
	//include_once($PATH_WRT_ROOT."includes/libreportcard2008j.php");
	$lreportcard = new libreportcard();
	$lreportcard_ui = new libreportcard_ui();
	//$lreportcard = new libreportcard();
	
	$CurrentPage = "Management_Promotion";
	
    if ($lreportcard->hasAccessRight())
    {
        $linterface = new interface_html();
        $ldis = new libdisciplinev12();
        
        ### Sys Msg
		$SysMsg = $linterface->GET_SYS_MSG($msg);
        
        ### Get Tag (List/Generation)
        $display_tagstype = '';
		$display_tagstype .= '
		  <div class="shadetabs">
            <ul>';
        $display_tagstype .= '<li><a href="index.php">'.$eReportCard['List'].'</a></li>';
        $display_tagstype .= '<li class="selected"><a href="generate.php">'.$eReportCard['Generate'].'</a></li>'; 
        // [2015-0421-1144-05164] Added tag for Promotion Meeting Report
		if($eRCTemplateSetting['Report']['PromotionSummaryReport']){
			$display_tagstype .= '<li><a href="report_index.php">'.$Lang['eReportCard']['ReportArr']['PromotionArr']['ReportName'].'</a></li>';
		}
        $display_tagstype .= '
            </ul>
          </div>';
        
		### Get Form selection (show form which has report card only)
		$libForm = new Year();
		$FormArr = $libForm->Get_All_Year_List();
		  		
  		### Generation Date
        //$GenerationDate = $eReportCard['LastGenerate'].": ".$lreportcard->GetPromotionGenerationDate($YearID);
        
        ### Gen Promotion Status Table
        $PromotionStatusAry = $lreportcard->GetPromotionStatusList($YearID, $YearClassID);
        
        # Table Header
        $display = "<table border=0 cellspacing='0' cellpadding='4' width='100%'>";
	        $display .= "<tr class='tabletop'>";
	        	$display .= "<td class='tabletopnolink'>".$eReportCard["Form"]."</td>";
	        	$display .= "<td class='tabletopnolink'>".$eReportCard["Generation"]."</td>";
	        	$display .= "<td class='tabletopnolink'>".$eReportCard["LastPromotionGeneration"]."</td>";
	        $display .= "</tr>";
     
		# Table Body
        foreach((array)$FormArr as $FormInfo)
        {
        	$GenerationDateAry = $lreportcard->GetPromotionGenerationDate($FormInfo["YearID"]);
        	$PromotionCriteriaArr = $lreportcard->getPromotionCriteria($FormInfo["YearID"]);
        	$PromotionCriteriaLastModifiedDate = $PromotionCriteriaArr[0]['DateModified'];
        	        	
        	$PromotionGenDate = $GenerationDateAry["LastPromotionUpdate"];
        	$ReportGenDate = $GenerationDateAry["LastGenerated"];
        	$eDisciplineGenDate = $ldis->getLastUpdateConductGrade($lreportcard->schoolYearID , "", $FormInfo["YearID"]);
        	
        	// [2015-0421-1144-05164] Skip eDiscipline Grade checking
        	if($eRCTemplateSetting['ReportGeneration']['PromotionNotCheckConduct']){
        		$eDisciplineGenDate = "";
        	}
        	
        	$LastGeneration = '';
        	if($PromotionGenDate!="0000-00-00 00:00:00" && $PromotionGenDate!="")  // show dates only if promotion are generated
        	{
        		$LastGeneration = $PromotionGenDate;
        		$modify_edis_alert = ($eDisciplineGenDate > $PromotionGenDate) ?  ($eReportCard["ConductGeneratedOn"] .": ". $eDisciplineGenDate."<br>") : "";
        		$modify_report_alert = ($ReportGenDate > $PromotionGenDate) ?  ($eReportCard["ReportGeneratedOn"] .": ". $ReportGenDate."<br>") : "";
        		$modify_promotion_criteria_alert = ($PromotionCriteriaLastModifiedDate > $PromotionGenDate) ?  ($eReportCard["PromotionCriteriaUpdatedOn"] .": ". $PromotionCriteriaLastModifiedDate."<br>") : "";
        		
        		if($modify_edis_alert||$modify_report_alert||$modify_promotion_criteria_alert) // show sysmsg box if any modification
        		{
        			$LastGeneration .= "<table border=\"0\" cellpadding=\"2\" cellspacing=\"0\" class=\"systemmsg\">";
	        			$LastGeneration .= "<tr>";
	        				$LastGeneration .= "<td>";
			        			$LastGeneration .= $modify_report_alert;
			        			$LastGeneration .= $modify_edis_alert;
			        			$LastGeneration .= $modify_promotion_criteria_alert;
        					$LastGeneration .= "</td>";
	        			$LastGeneration .= "</tr>";
        			$LastGeneration .= "</table>";
        		}
        		
        		$thisCss = 'tabletextremark';
        	}
        	else
        	{
        		$LastGeneration = '--';
        		$thisCss = 'tabletext';
        	}
        	
        	// [2015-0421-1144-05164] Hide eDiscipline Grade Message
        	if($eRCTemplateSetting['ReportGeneration']['PromotionNotCheckConduct']){
        		$eDisciplineGenDate = date("Y-m-d H:m:s");
        	}
        	
        	# if any one of Report or discipline have not been gen, hidden btn
        	$print_btn = '';
        	if ($ReportGenDate != "0000-00-00 00:00:00" && $ReportGenDate!="" && $eDisciplineGenDate!="0000-00-00 00:00:00" && $eDisciplineGenDate != "")
        		$print_btn .= $linterface->GET_ACTION_BTN($eReportCard['Generate'], "button", "Generate('".$FormInfo["YearID"]."')");
        	else
        	{
        		# Reason for cannot generating the promotion status
	        	$print_btn .= "<table border=\"0\" cellpadding=\"4\" cellspacing=\"0\" class=\"systemmsg\">";
        		
        		if ($ReportGenDate == "0000-00-00 00:00:00" || $ReportGenDate == "")
        		{
        			$print_btn .= "<tr>";
        				$print_btn .= "<td>- ".$eReportCard["ReportNotGenerated"]."</td>";
        			$print_btn .= "</tr>";
        		}        		
        		if ($eDisciplineGenDate == "0000-00-00 00:00:00" || $eDisciplineGenDate == "")
        		{
        			$print_btn .= "<tr>";
        				$print_btn .= "<td>- ".$eReportCard["ConductNotGenerated"]."</td>";
        			$print_btn .= "</tr>";
        		}
	        			
        		$print_btn .= "</table>";
        	}
        	
        	$rowcss = "tablerow".((++$ctr%2)+1);
        	$display .= "<tr class='$rowcss'>";
	        	$display .= "<td class='tabletext' width='20%' valign='top'>".$FormInfo["YearName"]."</td>";
	        	$display .= "<td class='tabletext' width='40%' valign='top'>".$print_btn."</td>";
	        	$display .= "<td class='$thisCss' valign='top'>".$LastGeneration."</td>";
	        $display .= "</tr>";
        }
        
        $display .= "</table>";
        
		# Tag information
		$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
		$TAGS_OBJ[] = array($eReportCard['Management_Promotion'], "", 0);
		
		$linterface->LAYOUT_START();
?>

<script>
function js_Reload_Selection(jsYearID)
{
	$('#ClassSelectionSpan>select').attr("disabled","disabled");
		
	$('#ClassSelectionSpan').load(
		"../../reports/ajax_reload_selection.php", 
		{ 
			RecordType: 'Class',
			YearID: jsYearID,
			SelectionID: 'YearClassID',
			isMultiple: 0,
			isAll: 1,
			onchange :'this.form.submit()'
		},
		function(ReturnData)
		{
			$('#ClassSelectionSpan>select').attr("disabled","");
			//SelectAll(document.form1.elements['YearClassIDArr[]']);
			//$('#ClassSelectionSpan').show();
			//$('#IndexDebugArea').html(ReturnData);
		}
	);

}

function Generate(YearID)
{
	window.location.href="generate_promotion.php?YearID="+YearID;
}
</script>

<br/>
<form name="FormMain" method="post" >
<table width="98%" border="0" cellpadding="4" cellspacing="0">
<tr>
    <td align="center">
		<table width="100%" border="0" cellspacing="0" cellpadding="2">
	  	<tr>
	    	<td>
		      	<table border="0" cellspacing="0" cellpadding="0" align="right">
	            <tr>
	                <td><?=$SysMsg?></td>
	            </tr>
	            </table>
		      	<br>
		      	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		      	<tr>
		          	<td class="tab_underline"><?=$display_tagstype?></td>
		        </tr>
		      	</table>
	      	</td>
	  	</tr>
	  	<tr>
	    	<td>
			    <br>
			    <!-- Start of Content -->
			    <?=$display?>
			    <!-- End of Content -->
			    <br>
		    </td>
	  	</tr>
	  	</table>
	  	<br>
	  	<table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
        	<td height="1" class="dotline"><img src="<?=$image_path.'/'.$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
        </tr>
      	</table>
	</td>
</tr>
</table>

</form>
<br><br>

<?
        $linterface->LAYOUT_STOP();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>