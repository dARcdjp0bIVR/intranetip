<?php
# Edited by Bill

/***************************************************
 * 	Modification log:
 * 	20170504 Bill:	[2017-0109-1818-40164]
 * 	- Set memory_limit to no limit, fixed cannot generate report with heavy data
 * 	20160120 Bill:	[2015-0421-1144-05164]
 * 	- Generate Promotion Status using customized logic
 * 		$eRCTemplateSetting['Report']['ReportGeneration']['GeneratePromotionStatus_CustomizedLogic']
 * *************************************************/

@SET_TIME_LIMIT(21600);
@ini_set(memory_limit, -1);
 
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
//include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
//include_once($PATH_WRT_ROOT."includes/subject_class_mapping_ui.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
//include_once($PATH_WRT_ROOT."lang/lang.marcus.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if ($plugin['ReportCard2008'])
{
	include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");

	if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
	{
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
	}
	/* Temp Library*/
	//include_once($PATH_WRT_ROOT."includes/libreportcard2008j.php");
	$lreportcard = new libreportcard();
	// [2015-0421-1144-05164] use cust reportcard object for generating promotion status
	if ($eRCTemplateSetting['Report']['ReportGeneration']['GeneratePromotionStatus_CustomizedLogic']) {
		$lreportcard = new libreportcardcustom();
	}
	
    if ($lreportcard->hasAccessRight())
    {
     	$ReportSetting = $lreportcard->returnReportTemplateBasicInfo(''," ClassLevelID='$YearID' AND Semester='F' ");
		$ReportID = $ReportSetting["ReportID"];
		
		# Get StudentList
		$StudentList= array();
		$StudentList = $lreportcard->GET_STUDENT_BY_CLASSLEVEL($ReportID, $YearID);
		
		$StudentIDAry = Get_Array_By_Key($StudentList,"UserID");
		$OtherInfo = $lreportcard->getReportOtherInfoData($ReportID);
		
		$LatestSemID = $lreportcard->Get_Last_Semester_Of_Report($ReportID);
		$result=array();
     	for($i=0; $i<sizeof($StudentIDAry); $i++)
     	{
     		$thisStudentID= $StudentIDAry[$i];
			
			// [2015-0421-1144-05164] added customized logic for generationg promotion status
			if ($eRCTemplateSetting['Report']['ReportGeneration']['GeneratePromotionStatus_CustomizedLogic']) {
				$PromotionStatus = $lreportcard->getPromotionStatusCustomized($ReportID, $thisStudentID, $OtherInfo[$thisStudentID], $LatestSemID);
			}
			else {
				$PromotionStatus = $lreportcard->getPromotionStatus($ReportID, $thisStudentID, $OtherInfo[$thisStudentID], $LatestSemID);
			}
     		$result["Promtion"][] = $lreportcard->UpdatePromotion($ReportID,$thisStudentID,$PromotionStatus);
     	}
     	
     	$result["LastPromotionUpdate"] = $lreportcard->UpdatePromotionDate($ReportID);
     	
     	if(!in_array(false,$result))
     		$msg="promotion_generate";
     	else
     		$msg="promotion_generate_failed";
     	
     	header("location: generate.php?msg=$msg");
     	
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>