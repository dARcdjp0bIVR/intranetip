<?php
/* Using: 
 * 
 * Modification Log:
 *  20200428 Bill:  [2019-0924-1029-57289]
 *      - add logic to support manual adjust comment for admin   ($eRCTemplateSetting['Management']['ClassTeacherComment']['SelectExistingCommentOnly'])
 *  20190708 Bill:
 *      - add js js_OnKeyPress_AutoFilter(), resetAJAXThickbox(), to support comment filter in thickbox     [2019-0520-1603-55206]
 *      - modified js js_Add_Student_Comment(), ensure new comment at the bottom                            [2019-0705-1034-09066]
 *  20190301 Bill:	[2018-1130-1440-05164]
 *      - Add Handling for Comment Selection  ($eRCTemplateSetting['Management']['ClassTeacherComment']['SelectExistingCommentOnly'])
 * 	20170519 Bill:
 * 		- Allow View Group to access (view only)
 *  20170308 Villa
 *  - add js_show_propic() - Show Selected Student ProPic
 *  - add js_hide_propic() - Hide Selected Student ProPic
 *  - add js_show_all_propic() - Show All Propic
 *  - add js_hide_all_propic() - Hide all Propic
 * 	20150619 Ivan [H68315]
 * 	- added logic for flag $eRCTemplateSetting['ClassTeacherComment']['AddCommentInSameLine']
 *  20130722 Roy
 * 	- modified js_Import() - change import link to import_step1.php
 * 	20120109 Ivan:
 * 	- added $eRCTemplateSetting['ClassTeacherComment']['PrintPage'] for print mode logic
 */
 
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "TEACHER";
 
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/json.php");

$lreportcard = new libreportcardcustom();
$linterface = new interface_html();
$lreportcard_ui = new libreportcard_ui();
$json = new JSON_obj();

$lreportcard->hasAccessRight();

$ReportID = $_REQUEST['ReportID'];
$ClassLevelID = $_REQUEST['ClassLevelID'];
$ClassID = $_REQUEST['ClassID'];
$ReturnMsgKey = $_REQUEST['ReturnMsgKey'];
$isPrintMode = $_REQUEST['isPrintMode'];

# Page
$CurrentPage = "Management_ClassTeacherComment";
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();

# Tag
if ($sys_custom['eRC']['Management']['ClassTeacherComment']['AddFromComment'] == true) {
	$TAGS_OBJ[] = array($eReportCard['ManagementArr']['ClassTeacherCommentArr']['Student'], "index.php", 1);
	if($ck_ReportCard_UserType != "VIEW_GROUP") {
		$TAGS_OBJ[] = array($eReportCard['ManagementArr']['ClassTeacherCommentArr']['Comment'], "index_comment.php", 0);
	}
}
else {
	$TAGS_OBJ[] = array($eReportCard['Management_ClassTeacherComment'], "", 0);
}
$linterface->LAYOUT_START($ReturnMsg);

$CommentMaxLength = $lreportcard->Get_Class_Teacher_Comment_MaxLength($ReportID);

$StudentProPicArr = $json->encode($lreportcard->GetStudentProPicLink($ClassID));

// [2018-1130-1440-05164] Use Thickbox JS CSS
if($eRCTemplateSetting['Management']['ClassTeacherComment']['SelectExistingCommentOnly']) {
    echo $linterface->Include_JS_CSS();
}
echo $lreportcard_ui->Include_JS_CSS();

echo $lreportcard_ui->Get_Management_Class_Teacher_Comment_Edit_UI($ReportID, $ClassLevelID, $ClassID, $Result, $SuccessCount, $cut_row, $isPrintMode, $WrongConductRow);
?>

<script language="javascript">
var jsCurrentCommentStudentID = '';
var AutoCompleteObj_Comment;
var AutoCompleteObj_Additional;
var isTimeout = false;

$(document).ready(function() {
	<? if ($eRCTemplateSetting['Management']['ClassTeacherComment']['SelectExistingCommentOnly']) { ?>
		// Init thickbox
        // initThickBox();
    	js_Init_DND_Table();

    	// Bind comment selection in thickbox
    	$('#newCommentID').live('change', SelectOptions);
    <? } ?>

    Init_JQuery_AutoComplete();
});

function js_Go_Back() {
	window.location = 'index.php?ClassLevelID=<?=$ClassLevelID?>&ClassID=<?=$ClassID?>&ReportID=<?=$ReportID?>';
}

function js_Export() {
	window.location = 'export.php?ClassLevelID=<?=$ClassLevelID?>&ClassID=<?=$ClassID?>&ReportID=<?=$ReportID?>';
}

function js_Import() {
	window.location = 'import_step1.php?ClassLevelID=<?=$ClassLevelID?>&ClassID=<?=$ClassID?>&ReportID=<?=$ReportID?>';
}

<? if ($eRCTemplateSetting['Management']['ClassTeacherComment']['SelectExistingCommentOnly']) { ?>
    function js_Delete_Comment(jsStudentID, jsRecordID)
    {
		//Block_Element('StudentDiv_' + jsStudentID);
        Block_Document();
		$('#StudentCommentDiv_' + jsStudentID + '_' + jsRecordID).remove();

		//UnBlock_Element('StudentDiv_' + jsStudentID);
		UnBlock_Document();
    }
    
    function js_Show_Add_Comment_Layer(jsStudentID)
    {
        Hide_Return_Message();

       	var commentIDList = $("input[name^='CommentIDList\\[" + jsStudentID + "\\]\\[\\]']").map(function(){ return $(this).val(); }).get().join(",");
		
    	// Get thickbox content
    	$.post(
    		"ajax_reload.php", 
    		{
    			Action: 'Reload_Add_Existing_Comment_Layer',
    			ReportID: '<?=$ReportID?>',
    			StudentID: jsStudentID,
    			CommentIDList: commentIDList
    		},
    		function(ReturnData)
    		{
    			$('div#TB_ajaxContent').html(ReturnData);
    		}
    	);
    }
    
    function js_Add_Student_Comment()
    {
    	$('div#CommentEmptyWarningDiv').hide();
    	var jsCanSubmit = true;
		
    	// Check selected comments
       	var jsCommentIDList = $("select#newCommentID").map(function(){ return $(this).val(); }).get();
       	jsCommentIDList = jsCommentIDList.filter(unique_array_values).join(",");
    	if (jsCommentIDList == '' || jsCommentIDList == null) {
    		$('div#CommentEmptyWarningDiv').show();
    		jsCanSubmit = false;
    	}
    	
    	if (jsCanSubmit)
    	{
        	var jsStudentID = $('input#StudentID').val();

        	// Get added comment display
    		$.post(
    			"ajax_reload.php", 
    			{ 
    				Action: 'Add_Student_Comment_Div',
    				StudentID: jsStudentID,
    				CommentIDList: jsCommentIDList
    			},
    			function(ReturnData)
    			{
        			// $(ReturnData).insertAfter($('#StudentDiv_' + jsStudentID));
                    $('#StudentDiv_' + jsStudentID).append(ReturnData);
        			
    				js_Hide_ThickBox();
    				// Get_Return_Message(jsReturnMsg);
    			}
    		);
    	}
    }
    
    function js_Init_DND_Table()
    {
    	$(".common_table_list_v30").tableDnD({
    		onDrop: function(table, DroppedRow) {
    			if(table.id == "AwardStudentTable") {
    				Block_Element(table.id);
    				
    				// Get the order string
    				var rowArr = table.tBodies[0].rows;
    				var numOfRow = rowArr.length;
    				var recordIDArr = new Array();
    				for (var i=0; i<numOfRow; i++) {
    					if (rowArr[i].id != "" && !isNaN(rowArr[i].id)) {
    						recordIDArr[recordIDArr.length] = rowArr[i].id;
    					}
    				}
    				var recordIDStr = recordIDArr.join(',');
    				
    				// Update DB
    				$.post(
    					"ajax_update.php", 
    					{ 
    						Action: "Reorder_AwardRank",
    						DisplayOrderString: recordIDStr
    					},
    					function(ReturnData)
    					{
    						// Reset the ranking display
    						var jsRankingCounter = 0;
    						$('span.RankingSpan').each( function () {
    							$(this).html(++jsRankingCounter);
    						})
    						
    						// Get system message
    						if (ReturnData == '1') {
    							jsReturnMsg = '<?=$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['ReturnMsgArr']['AwardReorderSuccess']?>';
    						} else {
    							jsReturnMsg = '<?=$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['ReturnMsgArr']['AwardReorderFailed']?>';
    						}
    						
    						UnBlock_Element(table.id);
    						Scroll_To_Top();
    						Get_Return_Message(jsReturnMsg);
    					}
    				);
    			}
    		},
    		onDragStart: function(table, DraggedRow) {
    			//$('#debugArea').html("Started dragging row "+row.id);
    		},
    		dragHandle: "Dragable", 
    		onDragClass: "move_selected"
    	});
    }
    
    function returnActiveDropDownListObj()
	{
    	return $("#newCommentID");
    }
    
    function SelectOptions(evt) {
    	// loop each selected options to found if there are any other options with the same value with it
    	// if so, also select those options (as they are the same multiple line comment )
    	var jObj = returnActiveDropDownListObj();
    	jQuery.each(jObj.children("option:selected"), function(idx,selectedOption) {
    		 thisVal = $(selectedOption).val(); 
    		 jObj.children("[value="+thisVal+"]").attr("selected", true);
         })
    }
	
    function unique_array_values(value, index, self) {
        // same as array_unique() 
        return self.indexOf(value) === index;
    }

    function js_OnKeyPress_AutoFilter()
    {
        if(!isTimeout)
        {
            isTimeout = true;
            setTimeout(function() {
                Block_Thickbox();

                $.ajax({
                    url: "ajax_reload.php",
                    type: "POST",
                    data: {
                        Action: 'Reload_Comment_Div_Content',
                        ReportID: $("input#curReportID").val(),
                        StudentID: $("input#StudentID").val(),
                        ExcludeCommentIDArr: $("input#excludeCommentIDArr").val(),
                        SearchText: $("input#commentSearchInput").val()
                    },
                    success: function(content){
                        $('td#commentDivTd').html(content);
                        resetAJAXThickbox();
                    },
                    error: function(){
                        resetAJAXThickbox();
                    }
                });
            }, 1200);
        }
    }

    function resetAJAXThickbox()
    {
        UnBlock_Thickbox();
        isTimeout = false;
    }
<? } ?>

function checkOptionAdd(parObj, addText, jsCommentType) {
    Add_Comment(addText, jsCommentType);
}

var numb = '0123456789';
var lwr = 'abcdefghijklmnopqrstuvwxyz';
var upr = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

function isValid(parm,val) {
    if (parm == "") return true;
    for (i=0; i<parm.length; i++) {
        if (val.indexOf(parm.charAt(i),0) == -1) return false;
    }
    return true;
}

function isNum(parm) {return isValid(parm,numb);}
function isAlpha(parm) {return isValid(parm,lwr+upr);}
function isAlphanum(parm) {return isValid(parm,lwr+upr+numb);}

function js_Clicked_Select_Comment(jsStudentID, jsCommentType) {
    jsCommentType = jsCommentType || '';
    var jsTextareaID = jsCommentType + 'Textarea_' + jsStudentID;

    js_Set_Current_Comment_Student(jsStudentID);

    var extraParm = '';
    <? if ($eRCTemplateSetting['Management']['ClassTeacherComment']['SelectExistingCommentOnly']) { ?>
        jsCommentType = 'Comment';
        extraParm = '&bothLang=1';
    <? } ?>
    newWindow('choose_comment.php?fieldname=' + jsTextareaID + '&CommentType=' + jsCommentType + extraParm, 9);
}

function Init_JQuery_AutoComplete() {
    <? if ($eRCTemplateSetting['Management']['ClassTeacherComment']['SelectExistingCommentOnly']) { ?>
        // do nothing
    <? } else { ?>
        AutoCompleteObj_Comment = $('input.CommentSearchTb').autocomplete(
            "ajax_search_comment.php",
            {
                onItemSelect: function(li) {
                    Add_Comment(li.extra[0], 'Comment');
                    //2014-0609-1130-09066
                    $('input.CommentSearchTb').val('');
                },
                formatItem: function(row) {
                    return row[0] + '  ' + row[1];
                },
                maxItemsToShow: 5,
                minChars: 1,
                delay: 0,
                width: 400
            }
        );
    <? } ?>

    AutoCompleteObj_Additional = $('input.AdditionalCommentSearchTb').autocomplete(
        "ajax_search_comment.php",
        {
            onItemSelect: function(li) {
                Add_Comment(li.extra[0], 'AdditionalComment');
                //2014-0609-1130-09066
                $('input.AdditionalCommentSearchTb').val('');
            },
            formatItem: function(row) {
                return row[0] + '  ' + row[1];
            },
            maxItemsToShow: 5,
            minChars: 1,
            delay: 0,
            width: 400
        }
    );

    <? if ($eRCTemplateSetting['Management']['ClassTeacherComment']['SelectExistingCommentOnly']) { ?>
        AutoCompleteObj_Comment = AutoCompleteObj_Additional;
    <? } ?>
}

function Add_Comment(jsComment, jsCommentType) {
    jsCommentType = jsCommentType || '';
    <? if ($eRCTemplateSetting['Management']['ClassTeacherComment']['SelectExistingCommentOnly']) { ?>
        jsCommentType = 'AdditionalComment';
    <? } ?>

    var jsTextareaID = jsCommentType + 'Textarea_' + jsCurrentCommentStudentID;
    var jsTextareaObj = document.getElementById(jsTextareaID);

    if (Trim(jsTextareaObj.value) != '') {
        var addCommentInSameLine = '<?=$eRCTemplateSetting['ClassTeacherComment']['AddCommentInSameLine']? 1 : 0?>';
        if (addCommentInSameLine == 1) {
            jsTextareaObj.value += "";
        }
        else {
            jsTextareaObj.value += "\n";
        }
    }
    jsTextareaObj.value += strip_tags(htmlspecialchars_decode(jsComment));
    jsTextareaObj.focus();

    <? if ($CommentMaxLength != -1) { ?>
        limitText(jsTextareaObj, <?=$CommentMaxLength?>);
    <? } ?>

    $('input#' + jsCommentType + 'CommentSearchTb_' + jsCurrentCommentStudentID).val('').focus();
}

function js_OnKeyPress_AutoComplete_Textbox(jsStudentID, jsIsAdditional) {
    jsIsAdditional = jsIsAdditional || 0;

    js_Set_Current_Comment_Student(jsStudentID);
    js_Update_Auto_Complete_Extra_Para(jsIsAdditional, jsStudentID);
}

function js_Set_Current_Comment_Student(jsStudentID) {
    jsCurrentCommentStudentID = jsStudentID;
}

function js_Update_Auto_Complete_Extra_Para(jsIsAdditional, jsStudentID) {
    jsIsAdditional = jsIsAdditional || 0;

    ExtraPara = new Array();
    ExtraPara['IsAdditional'] = jsIsAdditional;
    <? if ($eRCTemplateSetting['Management']['ClassTeacherComment']['SelectExistingCommentOnly']) { ?>
        ExtraPara['bothLang'] = 1;
    <? } ?>

    if (jsIsAdditional == 1) {
        AutoCompleteObj_Additional[0].autocompleter.setExtraParams(ExtraPara);
    }
    else {
        AutoCompleteObj_Comment[0].autocompleter.setExtraParams(ExtraPara);
    }
}

function js_Go_Print() {
	var jsOriginalAction = $('form#form1').attr('action');
	$('form#form1').attr('action', '../../print_page.php').attr('target', '_blank').submit();
	
	// restore form attribute
	$('form#form1').attr('action', jsOriginalAction).attr('target', '_self');
}

function alertCommentMaximum() {
	alert('<?=$Lang['eReportCard']['ManagementArr']['ClassTeacherCommentArr']['CommentTooLong']?>');
}

function js_show_propic(StudentID){
	var StudentProPicArr = '<?=$StudentProPicArr?>';
	var StudentProPicArrData = JSON.parse(StudentProPicArr);
	var StudentProPic = StudentProPicArrData[StudentID];
	if(StudentProPic == null){
		StudentProPic= '/images/myaccount_personalinfo/samplephoto.gif'
	}
	
	$('#profile_pic_'+StudentID).html("<img src='"+StudentProPic+"' width='100' height='130'>");
	$('#ProPicView_'+StudentID).hide();
	$('#ProPicHide_'+StudentID).show();
}

function js_hide_propic(StudentID){
	$('#profile_pic_'+StudentID).html("");
	$('#ProPicView_'+StudentID).show();
	$('#ProPicHide_'+StudentID).hide();
}

function js_show_all_propic(){
	$('.ProPicView').click();
	$('.ProPicView').hide();
	$('.ProPicHide').show();
	$('.ProPicShow_ALL').hide();
	$('.ProPicHide_ALL').show();
}

function js_hide_all_propic(){
	$('.ProPicHide').click();
	$('.ProPicView').show();
	$('.ProPicHide').hide();
	$('.ProPicShow_ALL').show();
	$('.ProPicHide_ALL').hide();
}

function js_show_comment_table(StudentID){
	$('#previousComment_'+StudentID).show();
	$('#CommentView_'+StudentID).hide();
	$('#CommentHide_'+StudentID).show();
}

function js_hide_comment_table(StudentID){
	$('#previousComment_'+StudentID).hide();
	$('#CommentView_'+StudentID).show();
	$('#CommentHide_'+StudentID).hide();
}
</script>
<br />
<br />

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>