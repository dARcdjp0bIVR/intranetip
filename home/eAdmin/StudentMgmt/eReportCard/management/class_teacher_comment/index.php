<?php
# editing by Bill

/********************************************************
 * Modification log
 * 20190301 Bill:	[2018-1130-1440-05164]
 *      - Hide Import function  ($eRCTemplateSetting['Management']['ClassTeacherComment']['SelectExistingCommentOnly'])
 * 20170519	Bill:
 * 		- Allow View Group to access (view only)
 * 20161118 Bill:	[2015-1104-1130-08164]
 * 		- added $eRCTemplateSetting['Management']['Schedule']['ClassTeacherCommentSubmissionPeriod'] logic
 * 		- allow edit if within Class Teacher's Comment Submission Period 
 * 20160120 Bill:	[2015-0421-1144-05164]
 * 		- added $eRCTemplateSetting['Marksheet']['ReportDisplayWithinSubmissionDate'] logic
 * 			display report within submission period only	[removed]
 * 20151223 Ivan [X86217] [ip.2.5.7.1.1]:
 * 		- added $eRCTemplateSetting['Marksheet']['DefaultReportUsingIssueDate'] logic
 * 20130722 Roy
 * 		- modified js_Import() - change import link to import_step1.php
 * 20130711 Roy
 * 		- add import class teacher comment by form
 * 		- show record updated notification after importing data
 * 
 * 20130703 Roy
 * 		- update layout to IP25 standard
 * 
 * 20120411 Ivan
 * 		- Cater export class teacher comment by form
 * 		- 2012-0217-1151-13071 - 葵涌循道中學 - How to export all Class teacher comment for verification
 * 
 * 20120326 Marcus:
 * 		- Cater allow to set time for submission, verification
 * 		- 2012-0112-1622-04073 - 迦密愛禮信中學 - Schedule of Report Card Management 
 * ******************************************************/
 
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();
 
if ($plugin['ReportCard2008'])
{
	include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
	if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") {
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
		$lreportcard = new libreportcardcustom();
	} else {
		$lreportcard = new libreportcard();
	}
	include_once($PATH_WRT_ROOT."includes/libreportcard2008_schedule.php");
	$lreportcard_schedule = new libreportcard_schedule();
	
	if ($lreportcard->hasAccessRight())
	{
		$linterface = new interface_html();
		$CurrentPage = "Management_ClassTeacherComment";
		$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
		
		# Tag
		if ($sys_custom['eRC']['Management']['ClassTeacherComment']['AddFromComment'])
		{
			$TAGS_OBJ[] = array($eReportCard['ManagementArr']['ClassTeacherCommentArr']['Student'], "index.php", 1);
			if($ck_ReportCard_UserType!="VIEW_GROUP") {
				$TAGS_OBJ[] = array($eReportCard['ManagementArr']['ClassTeacherCommentArr']['Comment'], "index_comment.php", 0);
			}
		}
		else
		{
			$TAGS_OBJ[] = array($eReportCard['Management_ClassTeacherComment'], "", 0);
		}
		
		$linterface->LAYOUT_START();
		############################################################################################################
		
		# Get ClassLevelID (Form) of the reportcard template
		if($ck_ReportCard_UserType=="TEACHER")
		{
			$FormArrCT = $lreportcard->returnClassTeacherForm($UserID);
			$ClassLevelID = ($ClassLevelID == "") ? $FormArrCT[0][0] : $ClassLevelID;
			$ClassArrCT = $lreportcard->returnClassTeacherClass($UserID, $ClassLevelID);
		}
		$FormArr = $ck_ReportCard_UserType=="ADMIN" || $ck_ReportCard_UserType=="VIEW_GROUP"? $lreportcard->GET_ALL_FORMS(1) : $FormArrCT;
		
		# Filters - By Form (ClassLevelID)
		if(count($FormArr) > 0)
		{
			$ClassLevelID = ($ClassLevelID == "") ? $FormArr[0][0] : $ClassLevelID;
			$FormSelection = $linterface->GET_SELECTION_BOX($FormArr, "name='ClassLevelID' class='tabletexttoptext' onchange='changedFormSelection(this.value)'", "", $ClassLevelID);
			
			# Filters - By Type (Term1, Term2, Whole Year, etc)
			// Get Semester Type from the reportcard template corresponding to ClassLevelID
			$ReportTypeSelection = '';
			$ReportTypeArr = array();
			$ReportTypeOption = array();
			$ReportTypeArr = $lreportcard->GET_REPORT_TYPES($ClassLevelID);

			// [2015-0421-1144-05164]
			// Teacher - hide reports that out of submission period	[removed]
			//$displayReportInSubmissionPeriod = ($eRCTemplateSetting['Marksheet']['ReportDisplayWithinSubmissionDate'] && $ck_ReportCard_UserType=="TEACHER")? true : false;
			//$ReportTypeArr = $lreportcard->GET_REPORT_TYPES($ClassLevelID, "", "", 0, false, $displayReportInSubmissionPeriod);
			
			# if not selected specific report card, preset the filter to the current submission period report template
			if ($ReportID == '')
			{
//				$conds = " 		ClassLevelID = '$ClassLevelID' 
//							AND 
//								NOW() BETWEEN MarksheetSubmissionStart AND MarksheetSubmissionEnd
//						";
//				$ReportInfoArr = $lreportcard->returnReportTemplateBasicInfo('', $conds);
//				$ReportID = ($ReportInfoArr['ReportID'])? $ReportInfoArr['ReportID'] : '';
				$ReportID = $lreportcard->getReportFilteringDefaultReportId($ClassLevelID);
			}
			
			if(count($ReportTypeArr) > 0) {
				for($j=0 ; $j<count($ReportTypeArr); $j++) {
					# for checking the existence of ReportID when changing ClassLevel
					$ReportIDList[] = $ReportTypeArr[$j]['ReportID'];
					$ReportTypeOption[$j][0] = $ReportTypeArr[$j]['ReportID'];
					$ReportTypeOption[$j][1] = $ReportTypeArr[$j]['SemesterTitle'];
				}
				$ReportTypeSelection = $linterface->GET_SELECTION_BOX($ReportTypeOption, 'name="ReportID" class="tabletexttoptext" onchange="document.form2.submit()"', '', $ReportID);
				
				$ReportID = ($ReportID == "" || !in_array($ReportID,$ReportIDList)) ? $ReportTypeOption[0][0] : $ReportID;
			}
			
			# UPDATE Completed/Not Completed Status if is form submit
			if (isset($_POST) && isset($isCompleted) && isset($ReportID)) {
				foreach ($isCompleted as $key => $value) {
					$success[] = $lreportcard->MARK_CLASS_TEACHER_COMMENT_COMPLETED($value, $ReportID, $key);
				}
				in_array(false, $success) ? $Result = "update_failed" : $Result = "update";
				$SysMsg = $linterface->GET_SYS_MSG($Result);
			}
			
			# UPDATE record_update status after import CSV
			if ($Result == "num_records_updated") {
			$msg = "$SuccessCount $i_con_msg_num_records_updated";
				if($cut_row) {
					$warnmsg .= " ".sprintf($eReportCard['CommentTooLong'], $cut_row);
				}	
				if($WrongConductRow) {
					$warnmsg .= " ".sprintf($eReportCard['ConductNotFound'], $WrongConductRow);
				}
				if($warnmsg)
					$color= 'red';
				else
					$color= 'green';
				$SysMsg = $linterface->GET_SYS_MSG("", "<font color='$color'>$msg $warnmsg</font>");
			} else {
				$SysMsg = $linterface->GET_SYS_MSG($Result);
			}
			
			# Main Content
			$ClassArr = $ck_ReportCard_UserType=="ADMIN" || $ck_ReportCard_UserType=="VIEW_GROUP"? $lreportcard->GET_CLASSES_BY_FORM($ClassLevelID) : $ClassArrCT;
			
			$display = "";
			if(sizeof($ClassArr) > 0 && sizeof($ReportTypeArr) > 0) {
				$ProgressArr = $lreportcard->GET_CLASS_TEACHER_COMMENT_PROGRESS($ReportID, "ClassLevelID", $ClassLevelID);
				$comparePeriod = $lreportcard->COMPARE_REPORT_PERIOD($ReportID, "Submission");
				
				// [2015-1104-1130-08164] use Class Teacher Comment Submission Period 
				if($eRCTemplateSetting['Management']['Schedule']['ClassTeacherCommentSubmissionPeriod'] && $ck_ReportCard_UserType=="TEACHER")
				{
					$comparePeriod = $lreportcard->COMPARE_REPORT_PERIOD($ReportID, "CTCSubmission");
				}
				
				$table = $lreportcard->DBName.".RC_MARKSHEET_COMMENT";
				for($i=0 ; $i<count($ClassArr) ;$i++){
					$ClassID = $ClassArr[$i][0];
					$ClassName = $ClassArr[$i][1];
					// Student Info Array
					$StudentArray = $lreportcard->GET_STUDENT_BY_CLASS($ClassID);
					$StudentSize = sizeof($StudentArray);
					$StudentIDArray = array();
					$StudentIDList = "";
					if ($StudentSize > 0) {
						// Student ID list of students in a class
						for($j=0; $j<$StudentSize; $j++)
							$StudentIDArray[] = $StudentArray[$j][0];
						if(!empty($StudentIDArray))
							$StudentIDList = implode(",", $StudentIDArray);
					}
					
					# get the last modified date and last modified user
					$NameField = getNameFieldByLang2("b.");
					$sql = "SELECT a.ReportID, $NameField AS Name, MAX(a.DateModified) AS DateModified ";
					$sql .= "FROM $table AS a, INTRANET_USER AS b ";
					$sql .= "WHERE a.TeacherID = b.UserID AND a.ReportID = '$ReportID' AND a.SubjectID = '' ";
					if ($StudentSize > 0)
						$sql .= "AND a.StudentID IN ($StudentIDList) ";
					$sql .= "GROUP BY a.ReportID";
					$result = $lreportcard->returnArray($sql);
//										
					if (sizeof($result) > 0 && $StudentSize > 0) {
						$dateModified = $result[0]["DateModified"];
						$lastModifiedBy = $result[0]["Name"];
					} else {
						$dateModified = "-";
						$lastModifiedBy = "-";
					}
					
					# State of IsCompleted 
					if (isset($ProgressArr[$ClassID])) {
						$isCompleted = $ProgressArr[$ClassID]["IsCompleted"];
					} else {
						$isCompleted = -1;
					}
					
					$isCompletedCheckbox = "";
					$NotCompletedSelect = "<select class='tabletexttoptext' name='isCompleted[$ClassID]'>";
					$NotCompletedSelect .= "<option value='1'>".$eReportCard['Confirmed']."</option>";
					$NotCompletedSelect .= "<option value='0' selected='selected'>".$eReportCard['NotCompleted']."</option>";
					$NotCompletedSelect .= "</select>"; 

					$CompletedSelect = "<select class='tabletexttoptext' name='isCompleted[$ClassID]'>";
					$CompletedSelect .= "<option value='1' selected='selected'>".$eReportCard['Confirmed']."</option>";
					$CompletedSelect .= "<option value='0'>".$eReportCard['NotCompleted']."</option>";
					$CompletedSelect .= "</select>"; 
					if ($ck_ReportCard_UserType == "ADMIN") {
						$editCommentIcon = "<a href='edit.php?ClassLevelID=$ClassLevelID&ClassID=$ClassID&ReportID=$ReportID'><img src='".$image_path."/".$LAYOUT_SKIN."/icon_edit_b.gif' width='20' height='20' border='0'></a>";
						if ($isCompleted == -1 || $isCompleted == 0) {
							$isCompletedSelect = $NotCompletedSelect;
						}
						if ($isCompleted == 1) {
							$isCompletedSelect = $CompletedSelect;
						}
					}
					else if ($ck_ReportCard_UserType=="VIEW_GROUP") {
						$editCommentIcon = "<a href='edit.php?ClassLevelID=$ClassLevelID&ClassID=$ClassID&ReportID=$ReportID'><img src='".$image_path."/".$LAYOUT_SKIN."/icon_view.gif' width='20' height='20' border='0'></a>";
						if ($isCompleted == -1 || $isCompleted == 0) {
							$isCompletedSelect = $eReportCard['NotCompleted'];
						}
						if ($isCompleted == 1) {
							$isCompletedSelect = $eReportCard['Confirmed'];
						}
					}
					else {
						$editCommentIcon = "-";
						switch($comparePeriod) {
							case -1: # Period not set yet (NULL)
							case 0: # before the Period
								$isCompletedSelect = ($isCompleted == -1 || $isCompleted = 0) ? $eReportCard['NotCompleted'] : $eReportCard['Confirmed'];
								break;
							case 1: # within  the Period
								if ($isCompleted == -1 || $isCompleted == 0) {
									$isCompletedSelect = $NotCompletedSelect;
								}
								if ($isCompleted == 1) {
									$isCompletedSelect = $CompletedSelect;
								}
								$editCommentIcon = "<a href='edit.php?ClassLevelID=$ClassLevelID&ClassID=$ClassID&ReportID=$ReportID'><img src='".$image_path."/".$LAYOUT_SKIN."/icon_edit_b.gif' width='20' height='20' border='0'></a>";
								break;
							case 2:  # after the Period
								$isCompletedSelect = ($isCompleted == -1 || $isCompleted = 0) ? $eReportCard['NotCompleted'] : $eReportCard['Confirmed'];
								break;
						}
					}
					
					$tr_css = ($i % 2 == 1) ? "tablegreenrow2" : "tablegreenrow1";
					$display .= "<tr class='$tr_css'>";
					$display .= "<td class='tabletext'>$ClassName</td>";
					$display .= "<td align='center' class='tabletext'>$editCommentIcon</td>";
					$display .= "<td class='tabletext'>$dateModified</td>";
					$display .= "<td class='tabletext'>$lastModifiedBy</td>";
					$display .= "<td align='center' class='tabletext'>$isCompletedSelect</td>";
					$display .= "</tr>";
				}
				
				$completeAll = "<select class='tabletexttoptext' name='completeAll' onchange='setCompleteAll(this.options[this.selectedIndex].value)'>";
				$completeAll .= "<option value=''>---".$eReportCard['ChangeAllTo']."---</option>";
				$completeAll .= "<option value='completeAll'>".$eReportCard['Confirmed']."</option>";
				$completeAll .= "<option value='notCompleteAll'>".$eReportCard['NotCompleted']."</option>";
				$completeAll .= "</select>"; 
				if ($ck_ReportCard_UserType == "VIEW_GROUP") {
					$completeAll = "&nbsp;";
				}
				
			} else {
				$td_colspan = "5";
				$display .=	'<tr class="tablegreenrow1" height="40">
								<td class="tabletext" colspan="'.$td_colspan.'" align="center">'.$i_no_record_exists_msg.'</td>
							</tr>';
			}
		} else {
			$td_colspan = "5";
			$display .=	'<tr class="tablegreenrow1" height="40">
							<td class="tabletext" colspan="'.$td_colspan.'" align="center">'.$i_no_record_exists_msg.'</td>
						</tr>';
		}
		
		$table_tool = "&nbsp;";
		
		# Added on 5 Jan 2009 by Ivan
		# Show Submission Start Date and End Date
		# construct the condition
		$scheduleEntries = $lreportcard->GET_REPORT_DATE_PERIOD("", $ReportID);
		$subStart = ($scheduleEntries[0]["SubStart"]=="") ? "-" : date("Y-m-d H:i:s",$scheduleEntries[0]["SubStart"]);
		$subEnd = ($scheduleEntries[0]["SubEnd"]=="") ? "-" : date("Y-m-d H:i:s",$scheduleEntries[0]["SubEnd"]);
		
		$specificSubmissionDisplay = $lreportcard_schedule->getTeacherSpecialMarksheetSubmissionPeriodDisplay($ReportID, $_SESSION['UserID']);
		if ($specificSubmissionDisplay != '') {
			$specificSubmissionRemarks = '<tr><td><br />'.$lreportcard_schedule->getTeacherSpecialMarksheetSubmissionPeriodRemarks().'</td></tr>';
		}
		
		// [2015-1104-1130-08164] display Class Teacher Comment Submission Period 
		if($eRCTemplateSetting['Management']['Schedule']['ClassTeacherCommentSubmissionPeriod']) {
			$subStart = ($scheduleEntries[0]["CTCStart"]=="") ? "-" : date("Y-m-d H:i:s",$scheduleEntries[0]["CTCStart"]);
			$subEnd = ($scheduleEntries[0]["CTCEnd"]=="") ? "-" : date("Y-m-d H:i:s",$scheduleEntries[0]["CTCEnd"]);
		}
		
		// [2018-1130-1440-05164]
		// if ($ck_ReportCard_UserType != "VIEW_GROUP") {
		if ($ck_ReportCard_UserType != "VIEW_GROUP" && !$eRCTemplateSetting['Management']['ClassTeacherComment']['SelectExistingCommentOnly']) {
			$btn_Import = $linterface->Get_Content_Tool_v30('import', "javascript:js_Import();");
		}
		$btn_Export = $linterface->Get_Content_Tool_v30('export', "javascript:js_Export();");
		
		############################################################################################################
        
?>

<script language="javascript">
/*
* General JS function(s)
*/
function setCompleteAll(parStatus) {
	var obj = document.getElementsByTagName("select");
	var indexToSelect = '';
	if (parStatus == "completeAll") {
		indexToSelect = 0;
	}
	if (parStatus == "notCompleteAll")	{
		indexToSelect = 1;
	}
	for (var i in obj) {
		if (obj[i].name != "completeAll" && obj[i].name != "ClassLevelID" && obj[i].name != "ReportID")
			obj[i].selectedIndex = indexToSelect;
	}
}

function changedFormSelection(jsSelectedFormID) {
	window.location = 'index.php?ClassLevelID=' + jsSelectedFormID;
}

function js_Export() {
	window.location = 'export.php?ClassLevelID=<?=$ClassLevelID?>&ReportID=<?=$ReportID?>';
}

function js_Import() {
	if ('<?=$comparePeriod?>' == '1' || '<?=$ck_ReportCard_UserType?>' == 'ADMIN') {
		window.location = 'import_step1.php?ClassLevelID=<?=$ClassLevelID?>&ReportID=<?=$ReportID?>';
	} else {
		alert("<?=$Lang['eReportCard']['ManagementArr']['ClassTeacherCommentArr']['NotInSubmissionPeriod']?>");
	}
}
</script>
<br/>

<div class="content_top_tool">
	<div class="Conntent_tool">
		<?=$btn_Import?>
		<?=$btn_Export?>
	</div>
	<br style="clear:both" />
</div>

<form name="form2" method="POST" action="" style="margin:0px;">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
 <tr>
    <td>
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td>&nbsp;</td>
				<td align="right"><?=$SysMsg?></td>
			</tr>
		</table>
  	</td>
  </tr>
  <tr>
    <td>
      <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
        <tr>
          <td width="1"><?=$FormSelection?></td>
          <td width="1"><?=$ReportTypeSelection?></td>
          <td align="right"><?=$table_tool?></td>
  		</tr>
  	  </table>
  	  <br />
  	  <div>
	  	  <table class="form_table_v30">
	  	  	<tr>
	  	  		<td class="field_title"><?=$eReportCard['SubmissionStartDate']?></td>
	  	  		<td><?=$subStart?></td>
  	  		</tr>
  	  		<tr>
	  	  		<td class="field_title"><?=$eReportCard['SubmissionEndDate']?></td>
	  	  		<td><?=$subEnd?></td>
  	  		</tr>
	  	  </table>
  	  </div>
  	  <br />
  	</td>
  </tr>
</table>
</form>

<form name="form1" method="POST" action="">
<table class="common_table_list_v30 edit_table_list_v30">
	
	<tr>
	  <th width="90"><?=$eReportCard['Class']?></th>
	  <th width="180"><?=$eReportCard['Management_ClassTeacherComment']?></th>
	  <th width="160"><?=$eReportCard['LastModifiedDate']?></th>
	  <th><?=$eReportCard['LastModifiedBy']?></th>
	  <th width="120" class="apply_all_top"><?=$completeAll?></th>
	</tr>
	<?=$display?>
	<?=$specificSubmissionRemarks?>
</table>

<div class="edit_bottom_v30">
	<?php
		if(count($FormArr) > 0 && $ck_ReportCard_UserType != "VIEW_GROUP"){
			if(sizeof($ClassArr) > 0 && sizeof($ReportTypeArr) > 0) {
	?>
						<?= $linterface->GET_ACTION_BTN($button_save, "submit", "")?>
						<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "")?>
	<?php
			}
		}
	?>
	<p class="spacer"></p>
</div>
<input type="hidden" name="ReportID" value="<?=$ReportID?>" />
<input type="hidden" name="ClassLevelID" value="<?=$ClassLevelID?>" />
</form>
<?
        $linterface->LAYOUT_STOP();
    } else {
    ?>
You have no priviledge to access this page.
    <?
    }
} else {
?>
You have no priviledge to access this page.
<?
}
?>