<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
{
	include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
	$lreportcard = new libreportcardcustom();
} else {
	$lreportcard = new libreportcard();
}

include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
$lreportcard_ui = new libreportcard_ui();
 
if ($lreportcard->hasAccessRight()) 
{
	$linterface = new interface_html();
	$CurrentPage = "Management_ClassTeacherComment";
	$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
	
	# tag information
	if ($sys_custom['eRC']['Management']['ClassTeacherComment']['AddFromComment'])
	{
		$TAGS_OBJ[] = array($eReportCard['ManagementArr']['ClassTeacherCommentArr']['Student'], "index.php", 0);
		$TAGS_OBJ[] = array($eReportCard['ManagementArr']['ClassTeacherCommentArr']['Comment'], "index_comment.php", 1);
	}
	else
	{
		$TAGS_OBJ[] = array($eReportCard['Management_ClassTeacherComment'], "", 0);
	}
	
	$ReturnMsgKey = stripslashes($_REQUEST['ReturnMsgKey']);
	$ReturnMsg = $eReportCard['ManagementArr']['ClassTeacherCommentArr']['ReturnMsgArr'][$ReturnMsgKey];
	$linterface->LAYOUT_START($ReturnMsg);
	
	$Keyword = stripslashes($_REQUEST['Keyword']);
	echo $lreportcard_ui->Get_Management_ClassTeacherComment_CommentView_UI($Keyword);
?>

<script language="javascript">
$(document).ready( function() {
	$('input#Keyword').focus().keyup(function(e){
		var keynum = Get_KeyNum(e);
		if (keynum==13) // keynum==13 => Enter
		{
			//	if there are any selected comment, ask the user if one really want to search comment as searching the comment now will lose all selected comments	
			var jsCheckedComment = false;
			$('input.CommentChk').each( function() {
				if ($(this).attr('checked') == true)
					jsCheckedComment = true;
					return false;
			});
			
			if (jsCheckedComment==false || confirm("<?=$eReportCard['ManagementArr']['ClassTeacherCommentArr']['jsWarningArr']['SelectedCommentWillBeLostIfSearch']?>"))
				js_Reload_Comment_Table();
		}
	});
});

function js_Reload_Comment_Table()
{
	jsKeyword = Trim($('input#Keyword').val());
	
	$('div#CommentTableDiv').html('');
	Block_Element('CommentTableDiv');
	
	$('div#CommentTableDiv').load(
		"ajax_reload.php", 
		{ 
			RecordType: 'Comment_Table',
			Keyword: jsKeyword
		},
		function(ReturnData)
		{
			UnBlock_Element('CommentTableDiv');
		}
	);
}

function js_CheckAll(jsChecked)
{
	$('input.CommentChk').each( function() {
		$(this).attr('checked', jsChecked);
	});
}

function js_Clicked_Comment(jsChecked)
{
	// uncheck the Select All checkbox if not checked
	if (jsChecked == false)
		$('input#CheckAll').attr('checked', false);
		
	
}

function js_Form_Check()
{
	var jsCheckedComment = false;
	$('input.CommentChk').each( function() {
		if ($(this).attr('checked') == true)
			jsCheckedComment = true;
	});
	
	if (jsCheckedComment == false)
	{
		alert("<?=$eReportCard['ManagementArr']['ClassTeacherCommentArr']['jsWarningArr']['SelectAtLeastOneComment']?>");
		return ;
	}
	
	// form submit
	var ObjForm = document.getElementById('CommentViewForm');
	ObjForm.action = 'choose_student.php';
	ObjForm.submit();
}
</script>


<?
    $linterface->LAYOUT_STOP();
    intranet_closedb();
}
?>