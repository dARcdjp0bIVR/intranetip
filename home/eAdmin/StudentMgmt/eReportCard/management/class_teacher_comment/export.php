<?php
#  Editing by

/****************************************************
 * Modification Log
 *
 * 20201030 Bill:   [2020-0708-0950-12308]
 *      - display cust wordings in csv header
 *
 * 20161118 Bill:
 * 		- fixed cannot export students' conduct 
 * 
 * 20130722 Roy:
 * 		- fixed template name bug ($filename)
 * 
 * 20130711 Roy:
 * 		- modified $filename for Form
 * 		- export user accessible class only
 * 
 *****************************************************/

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");

global $ck_ReportCard_UserType;

intranet_auth();
intranet_opendb();

if ($plugin['ReportCard2008']) {
	$lreportcard = new libreportcard();
	
	if ($lreportcard->hasAccessRight()) {
		if (!isset($_GET) || !isset($ClassLevelID, $ReportID)) {
			header("Location: index.php");
			exit();
		}
		
		// Form
		$FormName = $lreportcard->returnClassLevel($ClassLevelID);
		// Class
		$ClassArr = $lreportcard->Get_User_Accessible_Class($ClassLevelID); # check user access right
		if ($ClassID != '') {
			$ClassIDArr[0] = $ClassID;
			for($i=0 ; $i<count($ClassArr) ;$i++) {
			if($ClassArr[$i]['ClassID'] == $ClassID)
				$ClassName = $ClassArr[$i]['ClassName'];
			}
		} else {
			$ClassIDArr = Get_Array_By_Key($ClassArr, 'ClassID');
		}
		
		// Student Info Array
		//$StudentInfoArr = $lreportcard->GET_STUDENT_BY_CLASS($ClassID);
		//$StudentInfoArr = $lreportcard->GET_STUDENT_BY_CLASSLEVEL($ReportID, $ClassLevelID, $ClassID);
		
		$StudentInfoArr = array();
		foreach ($ClassIDArr as $thisClassID) {
			$StudentInfoArr = array_merge($StudentInfoArr, $lreportcard->GET_STUDENT_BY_CLASS($thisClassID));
		}
		
		$StudentSize = sizeof($StudentInfoArr);
		$StudentIDList = "";
		for($i=0; $i<$StudentSize; $i++) {
			$StudentIDArray[] = $StudentInfoArr[$i]['UserID'];
		}
		$StudentIDList = implode(",", $StudentIDArray);
				
		$table = $lreportcard->DBName.".RC_MARKSHEET_COMMENT";
		//$ClassNumField = getClassNumberField("u.");
		
		$SubjectCond = "(SubjectID = '' OR SubjectID IS NULL)";
		
		
		if (sizeof($StudentInfoArr) == 0) {
			header("Location: index.php");
			exit();
		}
		
		if (isset($isTemplate) && $isTemplate = "1") {
			if ($ClassID == '')
				$filename = "comment_template_$FormName.csv";
			else
				$filename = "comment_template_$ClassName.csv";
		} else {
			# only display the Comment if it match SubjectID & ReportID
			//$filename = $ClassName.'_ClassComment.csv';
			if ($ClassID == '') {
				$filename = "comment_export_$FormName.csv";
			}
			else {
				$filename = "comment_export_$ClassName.csv";
			}
		}
		
		# Get Comment and create an associated array
		$sql = "SELECT StudentID, Comment, AdditionalComment FROM $table WHERE $SubjectCond AND ReportID='$ReportID' AND StudentID IN ($StudentIDList)";
		$CommentInfo = $lreportcard->returnArray($sql);
		for($i=0; $i<sizeof($CommentInfo); $i++) {
			$thisStudentID = $CommentInfo[$i]["StudentID"];
			$CommentInfoAsso[$thisStudentID]["Comment"] = stripslashes($CommentInfo[$i]["Comment"]);
			$CommentInfoAsso[$thisStudentID]["AdditionalComment"] = stripslashes($CommentInfo[$i]["AdditionalComment"]);
		}

		if($eRCTemplateSetting['ClassTeacherComment_Conduct']) {
			include_once($PATH_WRT_ROOT.'includes/libreportcard2008_extrainfo.php');
			$lreportcard_extrainfo = new libreportcard_extrainfo();
			
			//$StudentExtraInfo = $lreportcard_extrainfo->Get_Student_Extra_Info($ReportID, $StudentIDList);
			$StudentExtraInfo = $lreportcard_extrainfo->Get_Student_Extra_Info($ReportID, (array)$StudentIDArray);
			$StudentConductID = BuildMultiKeyAssoc($StudentExtraInfo, "StudentID", "ConductID", 1);
			
			$ConductInfo = $lreportcard_extrainfo->Get_Conduct();
			$ConductInfo = BuildMultiKeyAssoc($ConductInfo, "ConductID", "Conduct", 1);
		}		
		
		# Create the data array for export
		$ExportArr = array();
		for($i=0; $i<sizeof($StudentInfoArr); $i++) {
			$thisStudentID = $StudentInfoArr[$i]["UserID"];
			//$thisStudentInfoArr = $lreportcard->Get_Student_Class_ClassLevel_Info($thisStudentID);
			//$thisClassName = $thisStudentInfoArr[0]['ClassName'];
			//$thisClassNumber = $thisStudentInfoArr[0]['ClassNumber'];
			
			$ExportArr[$i][0] = $StudentInfoArr[$i]["WebSAMSRegNo"];
			$ExportArr[$i][1] = $StudentInfoArr[$i]['ClassName'];
			$ExportArr[$i][2] = $StudentInfoArr[$i]['ClassNumber'];
			$ExportArr[$i][3] = $StudentInfoArr[$i]["StudentName"];
			
			if (isset($CommentInfoAsso[$thisStudentID]['Comment']) && $isTemplate != "1")
				$ExportArr[$i][4] = $CommentInfoAsso[$thisStudentID]['Comment'];
			else
				$ExportArr[$i][4] = "";
				
			if ($eRCTemplateSetting['ClassTeacherComment_AdditionalComment']) {
				if (isset($CommentInfoAsso[$thisStudentID]['AdditionalComment']) && $isTemplate != "1")
					$ExportArr[$i][5] = $CommentInfoAsso[$thisStudentID]['AdditionalComment'];
				else
					$ExportArr[$i][5] = "";
			}
			
			if($eRCTemplateSetting['ClassTeacherComment_Conduct'])
			{
				$ConductIndex = $eRCTemplateSetting['ClassTeacherComment_AdditionalComment']? 6 : 5;
				if (isset($ConductInfo[$StudentConductID[$thisStudentID]]) && $isTemplate != "1")
					$ExportArr[$i][$ConductIndex] = $ConductInfo[$StudentConductID[$thisStudentID]]; 
				else
					$ExportArr[$i][$ConductIndex] = "";
			}
				
		}
		
		$lexport = new libexporttext();
		
		$exportColumn = array("WebSAMSRegNumber", "Class Name", "Class Number", "Student Name", "Comment Content");
		if ($eRCTemplateSetting['ClassTeacherComment_AdditionalComment']) {
			$exportColumn[] = "Additional Comment";
		}
		// [2020-0708-0950-12308] cust wordings in csv header
        if ($eRCTemplateSetting['ClassTeacherComment_AdditionalComment_as_BilingualTeacher'])
        {
            $exportColumn[4] = $Lang['eReportCard']['ManagementArr']['ClassTeacherCommentArr']['CsvHeaderArr']['En']['CommentContent'];
            if ($eRCTemplateSetting['ClassTeacherComment_AdditionalComment']) {
                $exportColumn[5] = $Lang['eReportCard']['ManagementArr']['ClassTeacherCommentArr']['CsvHeaderArr']['En']['AdditionalComment'];
            }
        }
		if ($eRCTemplateSetting['ClassTeacherComment_Conduct']) {
			$exportColumn[] = "Conduct";
		}
		
		$export_content = $lexport->GET_EXPORT_TXT($ExportArr, $exportColumn,  "", "\r\n", "", 0, "11",1);
		
		intranet_closedb();
		
		// Output the file to user browser
		$filename = str_replace(' ', '_', $filename);
		$lexport->EXPORT_FILE($filename, $export_content);
	} else {
		echo "You have no priviledge to access this page.";
	}
} else {
	echo "You have no priviledge to access this page.";
}
?>