<?php
# Editing by

/****************************************************
 * Modification Log
 * 
 * 20171006 Bill [2017-0907-1704-15258]
 * 		- support new import mode - Append to original comment
 * 
 * 20130722 Roy
 * 		- New Import Page(Step 1), replace import.php
 * 
 *****************************************************/

$PageRight = "TEACHER";

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_comment.php");

$lreportcard = new libreportcardcustom();
$linterface = new interface_html();
$lreportcard_ui = new libreportcard_ui();
$lreportcard_comment = new libreportcard_comment();

$lreportcard->hasAccessRight();

$ReportID = $_REQUEST['ReportID'];
$ClassLevelID = $_REQUEST['ClassLevelID'];
$ClassID = $_REQUEST['ClassID'];
$ReturnMsgKey = $_REQUEST['ReturnMsgKey'];

# Current Page
$CurrentPage = "Management_ClassTeacherComment";
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();

# Tag Information
if ($sys_custom['eRC']['Management']['ClassTeacherComment']['AddFromComment'])
{
	$TAGS_OBJ[] = array($eReportCard['ManagementArr']['ClassTeacherCommentArr']['Student'], "index.php", 1);
	$TAGS_OBJ[] = array($eReportCard['ManagementArr']['ClassTeacherCommentArr']['Comment'], "index_comment.php", 0);
}
else
{
	$TAGS_OBJ[] = array($eReportCard['Management_ClassTeacherComment'], "", 0);
}

$ReturnMsg = $Lang['General']['ReturnMessage'][$ReturnMsgKey];
$linterface->LAYOUT_START($ReturnMsg);

//# sub-tag information
//echo $lreportcard_ui->Get_Class_Teacher_Comment_Subtag($ClassLevelID, $CurrentStep = 1);
//echo '<br />'."\n";

echo $lreportcard_ui->Get_Import_Class_Teacher_Comment_Step1_UI($ClassLevelID, $ClassID, $ReportID);
?>

<script type="text/JavaScript" language="JavaScript">
function js_Go_Export() {
	window.location = 'export.php?ClassID=<?=$ClassID?>&ReportID=<?=$ReportID?>&ClassLevelID=<?=$ClassLevelID?>&isTemplate=1';
}

function js_Continue() {
	if ($('input#csvfile').val() == '') {
		alert('<?=$Lang['General']['warnSelectcsvFile']?>');
		$('input#csvfile').focus();
		return false;
	}
	
	$('form#form1').submit();
}

function js_Cancel() {
	<? if ($ClassID=='') { ?>
		window.location = 'index.php?ReportID=<?=$ReportID?>&ClassLevelID=<?=$ClassLevelID?>';
	<? }
	else { ?>
		window.location = 'edit.php?ReportID=<?=$ReportID?>&ClassLevelID=<?=$ClassLevelID?>&ClassID=<?=$ClassID?>';
	<? } ?>
}

function js_Go_Class_Teacher_Comment() {
	window.location = 'index.php?ReportID=<?=$ReportID?>&ClassLevelID=<?=$ClassLevelID?>';
}

function js_Go_Edit() {
	window.location = 'edit.php?ReportID=<?=$ReportID?>&ClassLevelID=<?=$ClassLevelID?>&ClassID=<?=$ClassID?>';
}

function onClickImportMode(obj) {
//	var importMode = obj.value;
//	switch (importMode){
//		case "new":
//			//todo when new is click
//			$('.update').hide();
//			$('.new').show();
//		break;
//		case "update":
//			//todo when update is click
//			$('.update').show();
//			$('.new').hide();
//		break;
//	}
}
</script>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>