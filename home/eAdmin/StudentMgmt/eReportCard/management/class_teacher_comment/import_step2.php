<?
# Editing by

/****************************************************
 * Modification Log
 * 
 * 20171006 Bill [2017-0907-1704-15258]
 * 		- support new import mode - Append to original comment
 * 
 * 20130722 Roy
 * 		- New Import Page(Step 2), replace import.php
 * 
 *****************************************************/

$PageRight = "TEACHER";

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_comment.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

$lreportcard = new libreportcardcustom();
$linterface = new interface_html();
$lreportcard_ui = new libreportcard_ui();
$libimport = new libimporttext();
$lreportcard_comment = new libreportcard_comment();
$libfs = new libfilesystem();

$lreportcard->hasAccessRight();

$ReportID = $_REQUEST['ReportID'];
$ClassLevelID = $_REQUEST['ClassLevelID'];
$ClassID = $_REQUEST['ClassID'];
$ReturnMsgKey = $_REQUEST['ReturnMsgKey'];

$name = $_FILES['csvfile']['name'];
$ReturnPath = 'import_step1.php?ClassLevelID='.$ClassLevelID.'&ClassID='.$ClassID.'&ReportID='.$ReportID;

if (!$libimport->CHECK_FILE_EXT($name))
{
	intranet_closedb();
	header('location: '.$ReturnPath.'&ReturnMsgKey=WrongFileFormat');
	exit();
}

### move to temp folder first for others validation
$TargetFilePath = $libfs->Copy_Import_File_To_Temp_Folder('reportcard/class_teacher_comment/', $csvfile, $name);

### Validate file header format
$DefaultCsvHeaderArr = $lreportcard_comment->Get_Class_Teacher_Comment_Csv_Header_Title();
$DefaultCsvHeaderArr = $DefaultCsvHeaderArr['En'];
$ColumnPropertyArr = $lreportcard_comment->Get_Class_Teacher_Comment_Csv_Header_Property($forGetAllCsvContent=1);

$CsvData = $libimport->GET_IMPORT_TXT($TargetFilePath, "", "", $DefaultCsvHeaderArr, $ColumnPropertyArr);
$CsvHeaderArr = array_shift($CsvData);
$numOfCsvData = count($CsvData);

$CsvHeaderWrong = false;
for($i=0; $i<count($DefaultCsvHeaderArr); $i++) {
	if ($CsvHeaderArr[$i] != $DefaultCsvHeaderArr[$i]) {
		$CsvHeaderWrong = true;
		break;
	}
}

if($CsvHeaderWrong || $numOfCsvData==0)
{
	$ReturnMsgKey = ($CsvHeaderWrong)? 'WrongCSVHeader' : 'CSVFileNoData';
	intranet_closedb();
	header('location: '.$ReturnPath.'&ReturnMsgKey='.$ReturnMsgKey);
	exit();
}

# Current Page
$CurrentPage = "Management_ClassTeacherComment";
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();

# Tag Information
if ($sys_custom['eRC']['Management']['ClassTeacherComment']['AddFromComment'])
{
	$TAGS_OBJ[] = array($eReportCard['ManagementArr']['ClassTeacherCommentArr']['Student'], "index.php", 1);
	$TAGS_OBJ[] = array($eReportCard['ManagementArr']['ClassTeacherCommentArr']['Comment'], "index_comment.php", 0);
}
else
{
	$TAGS_OBJ[] = array($eReportCard['Management_ClassTeacherComment'], "", 0);
}

$ReturnMsg = $Lang['General']['ReturnMessage'][$ReturnMsgKey];
$linterface->LAYOUT_START($ReturnMsg);

//# sub-tag information
//echo $lreportcard_ui->Get_Class_Teacher_Comment_Subtag($ClassLevelID, $CurrentStep = 1);
//echo '<br />'."\n";

echo $lreportcard_ui->Get_Import_Class_Teacher_Comment_Step2_UI($ClassLevelID, $ClassID, $ReportID, $TargetFilePath, $numOfCsvData, $importMode);

### Thickbox loading message
$h_ProgressMsg = $linterface->Get_Import_Progress_Msg_Span(0, $numOfCsvData, $Lang['General']['ImportArr']['RecordsValidated']);
?>

<script type="text/JavaScript" language="JavaScript">
$(document).ready( function() {
	Block_Document('<?=$h_ProgressMsg?>');
});

function js_Continue() {
	$('form#form1').submit();
}

function js_Back_To_Import_Step1() {
	window.location = 'import_step1.php?ClassID=<?=$ClassID?>&ReportID=<?=$ReportID?>&ClassLevelID=<?=$ClassLevelID?>';
}

function js_Cancel() {
	<? if ($ClassID=='') { ?>
		window.location = 'index.php?ReportID=<?=$ReportID?>&ClassLevelID=<?=$ClassLevelID?>';
	<? }
	else { ?>
		window.location = 'edit.php?ReportID=<?=$ReportID?>&ClassLevelID=<?=$ClassLevelID?>&ClassID=<?=$ClassID?>';
	<? } ?>
}

function js_Go_Class_Teacher_Comment() {
	window.location = 'index.php?ReportID=<?=$ReportID?>&ClassLevelID=<?=$ClassLevelID?>';
}

function js_Go_Edit() {
	window.location = 'edit.php?ReportID=<?=$ReportID?>&ClassLevelID=<?=$ClassLevelID?>&ClassID=<?=$ClassID?>';
}
</script>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>