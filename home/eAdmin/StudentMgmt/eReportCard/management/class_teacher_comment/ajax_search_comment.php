<?php
// using

/***********************************************
 *  20200428 Bill:  [2019-0924-1029-57289]
 *      - add logic to support bothLang = 1
 * *********************************************/

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");

intranet_auth();
intranet_opendb();

# Get data
$SearchValue = trim(stripslashes(urldecode($_REQUEST['q'])));
$IsAdditional = ($_REQUEST['IsAdditional'] == '') ? 0 : $_REQUEST['IsAdditional'];

### Get Select User and exclude them in the search result
$lreportcard = new libreportcard();
$CommentArr = $lreportcard->Get_Comment_In_Comment_Bank($SubjectID='', $SearchValue, $CommentIDArr='', $IsAdditional);
$numOfComment = count($CommentArr);

$returnString = '';
for ($i=0; $i<$numOfComment; $i++)
{
	$thisCommentCode = $CommentArr[$i]['CommentCode'];
	$thisComment = str_replace("\n", "", nl2br(stripslashes(Get_Lang_Selection($CommentArr[$i]['Comment'], $CommentArr[$i]['CommentEng']))));

	// [2019-0924-1029-57289]
	if($_REQUEST['bothLang'])
	{
	    $thisComment = $CommentArr[$i]['Comment']."
".$CommentArr[$i]['CommentEng'];
        $thisComment = str_replace("\n", "", nl2br(stripslashes($thisComment)));
    }
    $returnString .= $thisCommentCode.'|'.$thisComment."\n";
}
intranet_closedb();

echo $returnString;
?>