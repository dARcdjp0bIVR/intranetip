<?php
// using ivan
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");

intranet_auth();
intranet_opendb();

# Get data
$SelectedUserIDList = $_REQUEST['SelectedUserIDList'];
$YearID = $_REQUEST['YearID'];
$SearchValue = stripslashes(urldecode($_REQUEST['q']));


### Get Select User and exclude them in the search result
$lreportcard = new libreportcard();
$SelectedUserArr = explode(',', $SelectedUserIDList);
$ExcludeUserIDArr = $lreportcard->Get_User_Array_From_Selection($SelectedUserArr, $YearID);


$SearchValueArr = explode(' ', $SearchValue);
//$ClassName = $SearchValueArr[0];
//$ClassNumber = $SearchValueArr[1];
//$StudentName = $SearchValueArr[2];
$ClassNameNumber = $SearchValueArr[0];
$StudentName = $SearchValueArr[1];

$OwnClassStudentOnly = ($lreportcard->IS_ADMIN_USER($_SESSION['UserID']))? 0 : 1;
$AvailableUserInfoArr = $lreportcard->Get_Available_User_To_Add_Comment($YearID, $ExcludeUserIDArr, $UserLogin, $ClassName, $ClassNumber, $StudentName, $OwnClassStudentOnly, $ClassNameNumber);
$numOfAvailableUser = count($AvailableUserInfoArr);
$returnString = '';
for ($i=0; $i<$numOfAvailableUser; $i++)
{
	$thisUserID = $AvailableUserInfoArr[$i]['UserID'];
	$thisUserNameWithClassAndClassNumber = $AvailableUserInfoArr[$i]['UserName'];
	
	$returnString .= $thisUserNameWithClassAndClassNumber.'|'.'|'.$thisUserID."\n";
}
intranet_closedb();



echo $returnString;
?>