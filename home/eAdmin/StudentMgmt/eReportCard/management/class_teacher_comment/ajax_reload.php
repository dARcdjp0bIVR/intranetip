<?php
/* Using:
 *
 * Modification Log:
 *  20190708 Bill:	[2019-0520-1603-55206]
 *  - Add new actions for Comment Selection     ($eRCTemplateSetting['Management']['ClassTeacherComment']['SelectExistingCommentOnly'])
 *  20190301 Bill:	[2018-1130-1440-05164]
 *  - Add 2 new actions for Comment Selection   ($eRCTemplateSetting['Management']['ClassTeacherComment']['SelectExistingCommentOnly'])
 */

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");

intranet_opendb();

$linterface = new interface_html();
$lreportcard_ui = new libreportcard_ui();

# Get data
$RecordType = stripslashes($_REQUEST['RecordType']);
if ($RecordType == "Comment_Table")
{
	$Keyword = stripslashes($_REQUEST['Keyword']);
	echo $lreportcard_ui->Get_Management_ClassTeacherComment_CommentView_Table($Keyword);
}
else if ($Action == 'Reload_Add_Existing_Comment_Layer')
{
    $ReportID = $_POST['ReportID'];
    $StudentID = $_POST['StudentID'];
    $CommentIDArr = $_POST['CommentIDList'];
    $CommentIDArr = explode(',', $CommentIDArr);
    $CommentIDArr = array_values(array_filter(array_unique($CommentIDArr)));
    
    $lreportcard_ui = new libreportcard_ui();
    echo $lreportcard_ui->Get_Add_Student_Existing_Comment_Layer_UI($ReportID, $StudentID, $CommentIDArr);
}
else if ($Action == 'Add_Student_Comment_Div')
{
    $StudentID = $_POST['StudentID'];
    $CommentIDArr = $_POST['CommentIDList'];
    $CommentIDArr = explode(',', $CommentIDArr);
    $CommentIDArr = array_values(array_filter(array_unique($CommentIDArr)));
    
    $lreportcard_ui = new libreportcard_ui();
    echo $lreportcard_ui->Get_Student_New_Comment_Div_UI($StudentID, $CommentIDArr);
}
else if ($Action == 'Reload_Comment_Div_Content')
{
    $ReportID = $_POST['ReportID'];
    $StudentID = $_POST['StudentID'];
    $ExcludeCommentIDArr = IntegerSafe($_POST['ExcludeCommentIDArr']);
    $ExcludeCommentIDArr = explode(',', $ExcludeCommentIDArr);
    $ExcludeCommentIDArr = array_values(array_filter(array_unique($ExcludeCommentIDArr)));
    $SearchText = intranet_htmlspecialchars($_POST['SearchText']);

    $lreportcard_ui = new libreportcard_ui();
    echo $lreportcard_ui->Get_Comment_Div_Selection($ReportID, $StudentID, $ExcludeCommentIDArr, $SearchText);
}

intranet_closedb();
?>