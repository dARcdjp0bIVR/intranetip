<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if ($plugin['ReportCard2008']) {
	include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
	//include_once($PATH_WRT_ROOT."includes/libreportcard2008j.php");
	//$lreportcard = new libreportcard2008j();
	//$lreportcard = new libreportcard();
	if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
	{
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
		$lreportcard = new libreportcardcustom();
	} else {
		$lreportcard = new libreportcard();
	}
	
	$CurrentPage = "Management_ClassTeacherComment";
	$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
    if ($lreportcard->hasAccessRight()) {
        $linterface = new interface_html();
        
		############################################################################################################
		
		if (!isset($ClassLevelID, $ReportID, $ClassID)) {
			header("Location: index.php");
			exit();
		}
		
		// Class
		$ClassArr = $lreportcard->GET_CLASSES_BY_FORM($ClassLevelID);
		if (sizeof($ClassArr) > 0) {
			for($i=0 ; $i<count($ClassArr) ;$i++) {
				if($ClassArr[$i][0] == $ClassID)
					$ClassName = $ClassArr[$i][1];
			}
			
			// Period (Term or Whole Year)
			$ReportInfo = $lreportcard->GET_REPORT_TYPES($ClassLevelID, $ReportID);
			$PeriodName = $ReportInfo['SemesterTitle'];
			
			// Student Info Array
			$StudentArray = $lreportcard->GET_STUDENT_BY_CLASS($ClassID, '', 1, 0, 1);
			$StudentSize = sizeof($StudentArray);
			
			// Student ID list of students in a class
			for($i=0; $i<$StudentSize; $i++)
				$StudentIDArray[] = $StudentArray[$i][0];
			if(!empty($StudentIDArray))
				$StudentIDList = implode(",", $StudentIDArray);
			
			$teacherComment = $lreportcard->GET_TEACHER_COMMENT($StudentIDArray, "", $ReportID);
		}
		
		$display = '<table class="yui-skin-sam" width="95%" cellpadding="5" cellspacing="0">';
		$display .= '<tr>
	          <td width="5%" align="center" class="tablegreentop tabletopnolink">'.$eReportCard['StudentNo_short'].'</td>
	          <td width="15%" class="tablegreentop tabletopnolink">'.$eReportCard['Student'].'</td>
			  <td class="tablegreentop tabletopnolink">'.$eReportCard['CommentContent'].'</td>
			  <td class="tablegreentop tabletopnolink">Additional Comment</td>
			  <td class="tablegreentop tabletopnolink">Conduct</td>';
				
	    $display .= '</tr>';
		
	    # Student Content
		if (isset($StudentSize) && $StudentSize > 0) {
		    for($j=0; $j<$StudentSize; $j++){
				list($StudentID, $WebSAMSRegNo, $StudentNo, $StudentName) = $StudentArray[$j];
				$tr_css = ($j % 2 == 1) ? "tablegreenrow2" : "tablegreenrow1";
				
				if (isset($teacherComment[$StudentID])) {
					$textareaName = "commentID[".$teacherComment[$StudentID]["CommentID"]."]";
					$textareaID = "commentID".$teacherComment[$StudentID]["CommentID"];
					$textareaValue = stripslashes($teacherComment[$StudentID]["Comment"]);
				} else {
					$textareaName = "comment[$StudentID]";
					$textareaID = "comment".$StudentID;
					$textareaValue = "";
				}
				
				$display .= '<tr class="'.$tr_css.'">
			          <td align="center" valign="top" class="tabletext">'.$StudentNo.'</td>
		          	  <td class="tabletext" valign="top">'.$StudentName.'</td>
					  <td class="tabletext" valign="top">';
					$script .= 'var myAutoComp'.$StudentID.' = new YAHOO.widget.AutoComplete("code-'.$StudentID.'","myContainer-'.$StudentID.'", myDataSource);';
				$script .= 'myAutoComp'.$StudentID.'.animVert = false;';
				//$script .= 'myAutoComp'.$StudentID.'.minQueryLength = 1;';
				
				$script .= 'myAutoComp'.$StudentID.'.formatResult = function(oResultItem, sQuery) {';
				$script .= 'var sMarkup = oResultItem[0];';
				$script .= 'return (sMarkup);';
				$script .= '};';
				
				$script .= 'myAutoComp'.$StudentID.'.itemSelectEvent.subscribe(function(e, args) {';
				$script .= 'if (document.getElementById("'.$textareaID.'").value != "") document.getElementById("'.$textareaID.'").value += "";';
				$script .= 'var contentToFill = args[2][1];';
				$script .= 'if (Trim(document.getElementById("'.$textareaID.'").value) != \'\')
								document.getElementById("'.$textareaID.'").value += "\n";';
				$script .= 'document.getElementById("'.$textareaID.'").value += contentToFill.replace(/<br>/gi, "\n");';
				#$script .= 'document.getElementById("'.$textareaID.'").value += args[2][1];';
				$script .= 'document.getElementById("code-'.$StudentID.'").value = "";';
				$script .= "limitText(document.getElementById(\"$textareaID\"),".$lreportcard->textAreaMaxChar.");";
				
				$script .= '});';
				
				$limitTextAreaEvent = "onKeyDown='limitText(document.getElementById(\"$textareaID\"),".$lreportcard->textAreaMaxChar.");return noEsc();'";
				$limitTextAreaEvent .= "onKeyUp='limitText(document.getElementById(\"$textareaID\"),".$lreportcard->textAreaMaxChar.");'";
				
				$display .= $textareaValue;
				$display .= '</td>';
				$display .= "<td>$textareaValue</td>";
				$display .= "<td><select><option>-- Select Conduct --</option></select></td>";
				$display .= '</tr>';
			} 	
		    $display .= '</table>';
			$display .= '<script type="text/javascript">';
			$display .= $script;
			$display .= '</script>';
			
			$display_button .= $linterface->GET_ACTION_BTN($button_save, "button", "document.form1.submit()")."&nbsp";
	        $display_button .= $linterface->GET_ACTION_BTN($button_reset, "reset", "")."&nbsp";
			$display_button .= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='index.php?ClassLevelID=$ClassLevelID&ClassID=$ClassID&ReportID=$ReportID'");
		} else {
			$td_colspan = "3";
			$display .=	'<tr class="tablegreenrow1" height="40">
							<td class="tabletext" colspan="'.$td_colspan.'" align="center">'.$i_no_record_exists_msg.'</td>
						</tr>';
		}
		
		if ($Result == "num_records_updated") {
			$msg = "$SuccessCount $i_con_msg_num_records_updated";
			if($cut_row)
			{
				$msg .= sprintf($eReportCard['CommentTooLong'], $cut_row);
				$color= 'red';
			}	
			else
				$color= 'green';
			$SysMsg = $linterface->GET_SYS_MSG("", "<font color='$color'>$msg</font>");
		} else {
			$SysMsg = $linterface->GET_SYS_MSG($Result);
		}
		############################################################################################################
        
		$PAGE_NAVIGATION[] = array($button_edit);
		
		# tag information
		if ($sys_custom['eRC']['Management']['ClassTeacherComment']['AddFromComment'] == true)
		{
			$TAGS_OBJ[] = array($eReportCard['ManagementArr']['ClassTeacherCommentArr']['Student'], "index.php", 1);
			$TAGS_OBJ[] = array($eReportCard['ManagementArr']['ClassTeacherCommentArr']['Comment'], "index_comment.php", 0);
		}
		else
		{
			$TAGS_OBJ[] = array($eReportCard['Management_ClassTeacherComment'], "", 0);
		}
		$linterface->LAYOUT_START();
		
		$parameters = "ClassID=$ClassID&ReportID=$ReportID&ClassLevelID=$ClassLevelID";
		
?>
<link type="text/css" rel="stylesheet" href="<?=$PATH_WRT_ROOT?>templates/yui/autocomplete_reportcard2008/autocomplete.css">
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/yui/autocomplete_reportcard2008/yahoo-dom-event.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/yui/autocomplete_reportcard2008/get-min.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/yui/autocomplete_reportcard2008/connection-min.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/yui/autocomplete_reportcard2008/animation-min.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/yui/autocomplete_reportcard2008/json-min.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/yui/autocomplete_reportcard2008/autocomplete-min.js"></script>
<script type="text/javascript">
var myArray = [<?=$lreportcard->GET_COMMENT_BANK_SUGGEST()?>];
var myDataSource = new YAHOO.widget.DS_JSArray(myArray);
myDataSource.queryMatchContains = true;

// Return false when ESC button is pressed, use to prevent RESET form

function checkOptionAdd(parObj, addText) {
	if (parObj.value != "")
	{
		//parObj.value += " ";
	}
	parObj.value += addText;
	limitText(parObj,<?=$lreportcard->textAreaMaxChar?>)
}


var numb = '0123456789';
var lwr = 'abcdefghijklmnopqrstuvwxyz';
var upr = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

function isValid(parm,val) {
	if (parm == "") return true;
	for (i=0; i<parm.length; i++) {
		if (val.indexOf(parm.charAt(i),0) == -1) return false;
	}
	return true;
}

function isNum(parm) {return isValid(parm,numb);}
function isAlpha(parm) {return isValid(parm,lwr+upr);}
function isAlphanum(parm) {return isValid(parm,lwr+upr+numb);}
</script>
<br/>
<form name="form1" method="post" action="edit_update.php" onSubmit="return false">

<!--PrintStart-->
<div align="left" style="width:95%">
<table border="0" cellpadding="5" cellspacing="0" class="tabletext">
	<tr>
		<td><?=$eReportCard['Period']?> : <strong><?=$PeriodName?></strong></td>
		<td><?=$eReportCard['Class']?> : <strong><?=$ClassName?></strong></td>
	</tr>
</table>
</div>
<br />
<table width="98%" border="0" cellspacing="5" cellpadding="0">
	<tr>
		<td align="left" colspan="2">
			<?=$linterface->GET_NAVIGATION($PAGE_NAVIGATION)?>
		</td>
	</tr>
</table>
<table width="95%" border="0" cellspacing="5" cellpadding="0">
	<tr>
		<td>
			<table border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td>
						<a href="import.php?<?=$parameters?>" class="contenttool">
							<img src="<?=$image_path.'/'.$LAYOUT_SKIN?>/icon_import.gif" width="18" height="18" border="0" align="absmiddle"> <?=$button_import?>
						</a>
					</td>
					<td><img src="<?=$image_path.'/'.$LAYOUT_SKIN?>/10x10.gif"></td>
					<td><a href="export.php?<?=$parameters?>" class="contenttool"><img src="<?=$image_path.'/'.$LAYOUT_SKIN?>/icon_export.gif" width="18" height="18" border="0" align="absmiddle"> <?=$button_export?></a></td>
				</tr>
			</table>
		</td>
		<td align="right"><?= $SysMsg ?></td>
	</tr>
</table>


<?=$display?>
<!--PrintEnd-->

<table width="95%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td height="1" class="dotline"><img src="<?=$image_path.'/'.$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
	</tr>
	<tr>
		<td>
			<table width="100%" border="0" cellspacing="0" cellpadding="2">
				<tr>
	            	<td align="left" valign="bottom"><span class="tabletextremark"><?=$eReportCard['DeletedStudentLegend']?></span></td>
	            </tr>
				<tr>
					<td align="center" valign="bottom"><?=$display_button?></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<input type="hidden" name="ClassLevelID" id="ClassLevelID" value="<?=$ClassLevelID?>"/>
<input type="hidden" name="ReportID" id="ReportID" value="<?=$ReportID?>"/>
<input type="hidden" name="ClassID" id="ClassID" value="<?=$ClassID?>"/>
</form>
<?
		intranet_closedb();
        $linterface->LAYOUT_STOP();
    } else {
    ?>
You have no priviledge to access this page.
    <?
    }
} else {
?>
You have no priviledge to access this page.
<?
}
?>