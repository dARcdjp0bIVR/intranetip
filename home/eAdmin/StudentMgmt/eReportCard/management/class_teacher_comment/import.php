<?php

#  Editing by Roy

/****************************************************
 * Modification Log
 * 
 * 20130711 Roy:
 * 		- update import.php layout to IP25 standard
 * 		- Add $ClassID == '' case to get form instead of class
 * 
 *****************************************************/
 
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if ($plugin['ReportCard2008'])
{
	include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
	if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
	{
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
		$lreportcard = new libreportcardcustom();
	} else {
		$lreportcard = new libreportcard();
	}
	
	$CurrentPage = "Management_ClassTeacherComment";
	$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
	
	if ($lreportcard->hasAccessRight()) {
		
		// Form
		if ($ClassID == ''){
			$ClassField = $eReportCard['Form'];
			$ClassName = $lreportcard->returnClassLevel($ClassLevelID);
		} else {
			$ClassField = $eReportCard['Class'];
			$ClassArr = $lreportcard->GET_CLASSES_BY_FORM($ClassLevelID);
			for($i=0 ; $i<count($ClassArr); $i++) {
				if($ClassArr[$i]["ClassID"] == $ClassID)
					$ClassName = $ClassArr[$i]["ClassName"];
			}
		}
		// Period (Term or Whole Year)
		$ReportInfo = $lreportcard->GET_REPORT_TYPES($ClassLevelID, $ReportID);
		$PeriodName = $ReportInfo['SemesterTitle'];
	
		############## Interface Start ##############
		$linterface = new interface_html();
		
		# tag information
		if ($sys_custom['eRC']['Management']['ClassTeacherComment']['AddFromComment'])
		{
			$TAGS_OBJ[] = array($eReportCard['ManagementArr']['ClassTeacherCommentArr']['Student'], "index.php", 1);
			$TAGS_OBJ[] = array($eReportCard['ManagementArr']['ClassTeacherCommentArr']['Comment'], "index_comment.php", 0);
		}
		else
		{
			$TAGS_OBJ[] = array($eReportCard['Management_ClassTeacherComment'], "", 0);
		}
		
		# page navigation (leave the array empty if no need)
		$PAGE_NAVIGATION[] = array($button_import);

		$linterface->LAYOUT_START();
		
		$parameters = "ClassID=$ClassID&ReportID=$ReportID&ClassLevelID=$ClassLevelID";
		
		if ($ClassID == '')
			$cancelURL = "window.location = 'index.php?ClassLevelID=$ClassLevelID'";
		else
			$cancelURL = "window.location = 'edit.php?$parameters'";
			
		
		
?>
<script type="text/JavaScript" language="JavaScript">
function resetForm() {
	document.form1.reset();
	changePicClass()
	return true;
}

function trim(str)
{
	return str.replace(/^\s+|\s+$/g,"");
}

function checkForm() {
	obj = document.form1;
	if (trim(obj.elements["userfile"].value) == "") {
		 alert('<?=$eReportCard['AlertSelectFile']?>');
		 obj.elements["userfile"].focus();
		 return false;
	}
	
	obj.submit();
}

function js_Go_Export() {
	window.location = 'export.php?ClassID=<?=$ClassID?>&ReportID=<?=$ReportID?>&ClassLevelID=<?=$ClassLevelID?>&isTemplate=1';
}


</script>
<link type="text/css" rel="stylesheet" href="<?=$PATH_WRT_ROOT."templates/<?=$LAYOUT_SKIN?>/css/"?>ereportcard.css">
<br />
<form name="form1" action="import_update.php" method="POST" onsubmit="return checkForm();" enctype="multipart/form-data">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
		<td align="right"> <?=$linterface->GET_SYS_MSG($Result);?></td>
	</tr>
	<tr><td>&nbsp;</td><td>&nbsp;</td></tr>
</table>
			<table class="form_table_v30">
				<tr>
					<td class="field_title">
						<?= $eReportCard['Period'] ?>
					</td>
					<td width="75%" class='tabletext'><?=$PeriodName?></td>
				</tr>
				<tr>
					<td class="field_title">
						<?=$ClassField?>
					</td>
					<td width="75%" class='tabletext'><?=$ClassName?></td>
				</tr>
				<tr>
					<td class="field_title">
						<?=$Lang['General']['SourceFile']?>&nbsp;<span class="tabletextremark"><?=$Lang['General']['CSVFileFormat']?>
					</td>
					<td width="75%" class='tabletext'>
						<input class="file" type='file' name='userfile'>
					</td>
				</tr>
				<tr>
					<td class="field_title"><?=$Lang['General']['CSVSample']?></td>
					<td valign="top" nowrap="nowrap" colspan='2' class="tabletext">
						<a class="tablelink" href="export.php?<?=$parameters?>&isTemplate=1" target="_blank">
							<?=$Lang['General']['ClickHereToDownloadSample']?>
						</a>
					</td>
				</tr>
			</table>
			<div class="edit_bottom_v30">
				<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "")?>&nbsp;
				<?= $linterface->GET_ACTION_BTN($button_cancel, "button", $cancelURL)?>&nbsp;
			</div>
			
		
<input type="hidden" name="ClassLevelID" id="ClassLevelID" value="<?=$ClassLevelID?>"/>
<input type="hidden" name="ReportID" id="ReportID" value="<?=$ReportID?>"/>
<input type="hidden" name="ClassID" id="ClassID" value="<?=$ClassID?>"/>
</form>
<?
	print $linterface->FOCUS_ON_LOAD("form1.userfile");
  	$linterface->LAYOUT_STOP();
  } else {
?>
You have no priviledge to access this page.
<?
	}
} else {
?>
You have no priviledge to access this page.
<?
}
?>
