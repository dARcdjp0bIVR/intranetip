<?php
# Editing by

/****************************************************
 * Modification Log
 *
 * 20201030 Bill:   [2020-0708-0950-12308]
 *      - keep existing comment if no comment inputted  ($eRCTemplateSetting['ClassTeacherComment_AdditionalComment_as_BilingualTeacher'])
 *      - additional comment > support append mode
 * 
 * 20171006 Bill [2017-0907-1704-15258]
 * 		- support new import mode - Append to original comment
 * 
 *****************************************************/

$PageRight = "TEACHER";

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_comment.php");

$linterface = new interface_html();
$lreportcard_comment = new libreportcard_comment();

$lreportcard_comment->hasAccessRight();

$Action = $_REQUEST['Action'];
if ($Action == 'Import_Class_Teacher_Comment')
{
	### Build Conduct Bank
	if($eRCTemplateSetting['ClassTeacherComment_Conduct']) {
		include_once($PATH_WRT_ROOT.'includes/libreportcard2008_extrainfo.php');
		$lreportcard_extrainfo = new libreportcard_extrainfo();
		
		$ConductInfo = $lreportcard_extrainfo->Get_Conduct();
		$ConductInfo = BuildMultiKeyAssoc($ConductInfo, "Conduct", "ConductID", 1);
	}	

	$ReportID = $_REQUEST['ReportID'];
	$ClassID = $_REQUEST['ClassID'];
	
	$lreportcard_comment->Start_Trans();
	
	$jsInclude = '';
	$jsInclude .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery-1.3.2.min.js"></script>';
	$jsInclude .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery.blockUI.js"></script>';
	$jsInclude .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/script.js"></script>';
	echo $jsInclude;
	
	### Get Import Details
	$ImportColumnInfoArr = $lreportcard_comment->Get_Class_Teacher_Comment_Csv_Header_Info();
	$numOfDBColumn = count($ImportColumnInfoArr);
	$ImportDataArr = $lreportcard_comment->Get_Temp_Class_Teacher_Comment_Data($ReportID);
	$numOfImportData = count($ImportDataArr);
	
	### Get Current Class Teacher Comment Record for insert / update checking
	// $ClassTeacherCommentInfoAssoArr[$ReportID][$thisStudentID]['CommentID'] = Value
	$ClassTeacherCommentInfoAssoArr = $lreportcard_comment->Get_Class_Teacher_Comment_Record($ReportID, $StudentIDArray, $SubjectID='', $ReturnAsso=1);
	
	$DataArr = array();
	for ($i=0; $i<$numOfImportData; $i++)
	{
		$thisTempID = $ImportDataArr[$i]['TempID'];
		$thisRowNumber = $ImportDataArr[$i]['RowNumber'];
		$thisStudentID = $ImportDataArr[$i]['StudentID'];
		$thisComment = str_replace('<!-br->', "\n", $ImportDataArr[$i]['CommentContent']);
		$thisCommentID = $ClassTeacherCommentInfoAssoArr[$ReportID][$thisStudentID]['CommentID'];
        if ($eRCTemplateSetting['ClassTeacherComment_AdditionalComment']) {
            $thisAdditionalComment = str_replace('<!-br->', "\n", $ImportDataArr[$i]['AdditionalComment']);
        }

		$thisDataArr = array();
		if ($thisCommentID == '') {
			$thisDataArr['StudentID'] = $thisStudentID;
		}
		else {
			$thisDataArr['CommentID'] = $thisCommentID;
		}
		
		// [2017-0907-1704-15258] Handle comment before update
		$thisDataArr['Comment'] = "";
		if($ImportMode == "append") {
			$thisOriginalComment = $ClassTeacherCommentInfoAssoArr[$ReportID][$thisStudentID]['Comment'];
			if(!empty($thisOriginalComment)) {
				$thisOriginalComment = trim($thisOriginalComment);
				$thisDataArr['Comment'] .= $thisOriginalComment;
				if(!empty($thisComment)) {
					$thisDataArr['Comment'] .= "\n";
				}
			}
		}
		$thisDataArr['Comment'] .= $thisComment;

		// [2020-0708-0950-12308] Handle additional comment before update
        if ($eRCTemplateSetting['ClassTeacherComment_AdditionalComment'])
        {
            $thisDataArr['AdditionalComment'] = "";
            if($ImportMode == "append") {
                $thisOriginalAdditionalComment = $ClassTeacherCommentInfoAssoArr[$ReportID][$thisStudentID]['AdditionalComment'];
                if(!empty($thisOriginalAdditionalComment)) {
                    $thisOriginalAdditionalComment = trim($thisOriginalAdditionalComment);
                    $thisDataArr['AdditionalComment'] .= $thisOriginalAdditionalComment;
                    if(!empty($thisAdditionalComment)) {
                        $thisDataArr['AdditionalComment'] .= "\n";
                    }
                }
            }
            $thisDataArr['AdditionalComment'] .= $thisAdditionalComment;
        }

        // [2020-0708-0950-12308] if no comment inputted in csv > keep existing comment
        if ($eRCTemplateSetting['ClassTeacherComment_AdditionalComment_as_BilingualTeacher'])
        {
            if(empty($thisComment)) {
                unset($thisDataArr['Comment']);
            }
            if ($eRCTemplateSetting['ClassTeacherComment_AdditionalComment'] && empty($thisAdditionalComment)) {
                unset($thisDataArr['AdditionalComment']);
            }
        }

		$DataArr[] = $thisDataArr;

		if($eRCTemplateSetting['ClassTeacherComment_Conduct'])
		{
			$thisConduct = $ImportDataArr[$i]['Conduct'];
			if(empty($thisConduct))
			{
				$StudentConductArr[$thisStudentID] = "";
			}
			else if($ConductID = $ConductInfo[$thisConduct]) // skip those not in conduct bank
			{
				$StudentConductArr[$thisStudentID] = $ConductID;
			}
		}
		
		$thisJSUpdate = '';
		$thisJSUpdate .= '<script language="javascript">'."\n";
			$thisJSUpdate .= 'window.parent.document.getElementById("BlockUI_Processed_Number_Span").innerHTML = "'.($i + 1).'";';
		$thisJSUpdate .= '</script>'."\n";
		echo $thisJSUpdate;
		
		Flush_Screen_Display(1);
	}
	
	$SuccessArr['UpdateClassTeacherComment'] = $lreportcard_comment->Update_Class_Teacher_Comment($ReportID, $DataArr, $UpdateLastModified=1);
	
	if($eRCTemplateSetting['ClassTeacherComment_Conduct'])
	{
		$success['UpdateConduct'] = $lreportcard_extrainfo->Insert_Update_Extra_Info("ConductID", $ReportID, $StudentConductArr);
	}
	
	### Delete the temp data first
	$SuccessArr['DeleteTempData'] = $lreportcard_comment->Delete_Import_Temp_Class_Teacher_Comment_Data($ReportID);
	
	### Roll Back data if import failed
	if (in_multi_array(false, $SuccessArr))
	{
		$lreportcard_comment->RollBack_Trans();
		$ImportStatus = $Lang['eReportCard']['ManagementArr']['ClassTeacherCommentArr']['ImportCommentFailed'];
	}
	else
	{
		$lreportcard_comment->Commit_Trans();
		$ImportStatus = $Lang['eReportCard']['ManagementArr']['ClassTeacherCommentArr']['ImportCommentSuccess'];
	}
	
	$thisJSUpdate = '';
	$thisJSUpdate .= '<script language="javascript">'."\n";
		$thisJSUpdate .= 'window.parent.document.getElementById("ImportStatusSpan").innerHTML = "'.$ImportStatus.'";';
		$thisJSUpdate .= 'window.parent.UnBlock_Document();'."\n";
	$thisJSUpdate .= '</script>'."\n";
	
	echo $thisJSUpdate;
}
intranet_closedb();
?>