<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_pc.php");
$lreportcard_pc = new libreportcard_pc();
$lreportcard = new libreportcardcustom();
$lreportcard->hasAccessRight(); 

if ($FromMS == 1) {
	$CurrentPage = "Management_MarkSheetRevision";
}
else {
	$CurrentPage = "Management_PersonalCharacteristics";
}
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();

# tag information
$TAGS_OBJ[] = array($eReportCard['PersonalCharacteristics'], "", 0);

# Step Info
$STEPS_OBJ[] = array($i_general_select_csv_file, 0);
$STEPS_OBJ[] = array($i_general_imported_result, 1);

$linterface = new interface_html();
$linterface->LAYOUT_START();


# navigation
$lclass = new libclass();
$className = $lclass->getClassName($ClassID);
$className = ($className=='')? $Lang['General']['EmptySymbol'] : $className;
$PAGE_NAVIGATION[] = array($eReportCard['PersonalCharacteristics']);
$PAGE_NAVIGATION[] = array($Lang['Btn']['Import']);

# Information above the table
//get report title
$ReportInfoArr = $lreportcard->returnReportTemplateBasicInfo($ReportID);
$reportTitle = str_replace(":_:", "<br>", $ReportInfoArr['ReportTitle']);

//get subject title
if ($SubjectID == 0)
	$subjectTitle = $eReportCard['Template']['OverallCombined'];
else
	$subjectTitle = $lreportcard->GET_SUBJECT_NAME_LANG($SubjectID, $intranet_session_language);
	
if ($SubjectGroupID != '') {
	$obj_SubjectGroup = new subject_term_class($SubjectGroupID);
	$SubjectGroupName = $obj_SubjectGroup->Get_Class_Title();	
}
else {
	$SubjectGroupName = $Lang['General']['EmptySymbol'];
}	

### Page Info
$pageInfo = '';
// Subject Group
if ($SubjectGroupID == '') {
	$pageInfo .= "<tr><td class=\"field_title\">".$Lang['General']['Class']."</td><td class=\"field_c\">".$className."</td></tr>";
}

// Subject
$pageInfo .= "<tr><td class=\"field_title\">".$eReportCard['Subject']."</td><td class=\"field_c\">".$subjectTitle."</td></tr>";

// Class
if ($SubjectGroupID != '') {
	$pageInfo .= "<tr><td class=\"field_title\">".$Lang['SysMgr']['SubjectClassMapping']['SubjectGroup']."</td><td class=\"field_c\">".$SubjectGroupName."</td></tr>";
}

// Report
$pageInfo .= "<tr><td class=\"field_title\">".$eReportCard['Reports']."</td><td class=\"field_c\">".$reportTitle."</td></tr>";

		


# initialize
$data = unserialize(rawurldecode($data));
$result_ary = unserialize(rawurldecode($result_ary));
$imported_student = unserialize(rawurldecode($imported_student));
$imported_performance = unserialize(rawurldecode($imported_performance));


# Get all characteristics of the subject of the class level
/*	
	[0] CharID
	[1] Title
*/
$DataArr['ClassLevelID'] = $ClassLevelID;
$DataArr['ReportID'] = $ReportID;
$DataArr['SubjectID'] = $SubjectID;
$DataArr['SubjectGroupID'] = $SubjectGroupID;
$CharArr = $lreportcard->GET_CLASSLEVEL_CHARACTERISTICS($DataArr);
$numOfChar = count($CharArr);


### List out the import result
# import result info
$total = sizeof($data);
$x = "<table width=\"100%\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\">";
$x .= "<tr><td class=\"tabletext\">$list_total : ". $total .", $i_Discipline_System_WaiveStatus_Success : ". sizeof($imported_student) .", ".$i_Discipline_System_WaiveStatus_Fail .": ". ($total - sizeof($imported_student)) ."</td></tr>";	
$x .= "</table>";

# Table Title
$ClassWidth = 5;
$ClassNoWidth = 5;
$StudentNameWidth = 10;
$CommentWidth = ($eRCTemplateSetting['PersonalCharacteristicComment'])? 15 : 0;
$ReasonWidth = 10;
$PCWidth = ($numOfChar > 0)? floor((100 - $ClassWidth - $ClassNoWidth - $StudentNameWidth - $CommentWidth - $ReasonWidth) / $numOfChar) : 0;
$x .= '<table class="common_table_list_v30 edit_table_list_v30">'."\r\n";
	$x .= '<thead>'."\r\n";
		$x .= '<tr>'."\r\n";
			$x .= '<th width="10">#</td>'."\r\n";
			$x .= '<th style="width:'.$ClassWidth.'%;">'.$Lang['SysMgr']['FormClassMapping']['Class'].'</th>'."\r\n";
			$x .= '<th style="width:'.$ClassNoWidth.'%;">'.$Lang['SysMgr']['FormClassMapping']['ClassNo'].'</th>'."\r\n";
			$x .= '<th style="width:'.$StudentNameWidth.'%;">'.$Lang['SysMgr']['FormClassMapping']['StudentName'].'</th>'."\r\n";
			for ($i=0; $i<$numOfChar; $i++) {
				$thisCharName = Get_Lang_Selection($CharArr[$i]['CharTitleCh'], $CharArr[$i]['CharTitleEn']);
				$x .= '<th style="width:'.$PCWidth.'%;">'.$thisCharName.'</th>'."\r\n";
			}
			if($eRCTemplateSetting['PersonalCharacteristicComment']) {
				$x .= '<th style="width:'.$CommentWidth.'%;">'.$eReportCard['Comment'].'</th>'."\r\n";	
			}
			$x .= '<th style="width:'.$ReasonWidth.'%;">'.$i_SAMS_import_error_reason.'</th>'."\r\n";
		$x .= '</tr>'."\r\n";
	$x .= '</thead>'."\r\n";

	$x .= '<body>'."\r\n";
	for($i=0;$i<sizeof($data);$i++) {
		/*
		[0] Class Name
		[1] Class Number
		[2] Performance code
		[3] Performance code
		...
		[n] Performance code
		*/	
		$ClassName = $data[$i][0];
		$ClassNumber = $data[$i][1];
		
		### checking - valid class & class number (student found)
		//$StudentID = $lu->returnUserID($ClassName, $ClassNumber, 2, '0,1,2');
		$thisStudentInfoArr = $lreportcard->Get_Student_Info_By_ClassName_ClassNumber($ClassName, $ClassNumber);
		$StudentID = $thisStudentInfoArr[0]['UserID'];
		$lu_tmp = new libuser($StudentID);
		
		if (in_array($StudentID, $imported_student)) {
			$status = '&nbsp;';
			$css = 'tabletext';
		}
		else {
			$thisReasonArr = $result_ary[$i];
			$thisNumOfReason = count($thisReasonArr);
			
			$thisStatusArr = array();
			for ($j=0; $j<$thisNumOfReason; $j++) {
				$thisReasonCode = $thisReasonArr[$j];
				$thisReasonMsg = $Lang['eReportCard']['ImportWarningArr'][$thisReasonCode];
				 
				$thisStatusArr[] = '- '.$thisReasonMsg;
			}
			
			$status = implode('<br />', $thisStatusArr);
			$css = 'red';
		}
		
		$x .= "<tr>";
			$x .= "<td class=\"$css\">".($i+1)."</td>";
			$x .= "<td class=\"$css\">". $ClassName ."</td>";
			$x .= "<td class=\"$css\">". $ClassNumber ."</td>";
			
			if ($ClassName!=NULL && $ClassNumber!=NULL)	{
				$x .= "<td class=\"$css\">". $lu_tmp->UserNameLang() ."</td>";
			}
			else {
				//have not input class and class number => don't show name
				$x .= "<td class=\"$css\">".$Lang['General']['EmptySymbol']."</td>";
			}
			
			$sizeofChar = $eRCTemplateSetting['PersonalCharacteristicComment']? sizeof($data[0])-1 : sizeof($data[0]);
			for($j=2; $j<$sizeofChar; $j++) {
				$thisScaleCode = $data[$i][$j];
				
				$css_perform = ($imported_performance[$i][$j]==1) ? "tabletext" : "red";
				$x .= "<td class=\"$css_perform\">".$thisScaleCode."</td>";
				
			}
			
			if($eRCTemplateSetting['PersonalCharacteristicComment']) {
				$x .= "<td class=\"$css\">".str_replace('<!--LineBreakHere-->', '<br />', $data[$i][$j])."</td>";	
			}
			
			$x .= "<td class=\"$css\">$status</td>";
		$x .= "</tr>";
	}  
	$x .= '</tbody>'."\r\n";
$x .= "</table>";

# Buttons at the bottom
$parameters = "ClassLevelID=$ClassLevelID&ClassID=$ClassID&ReportID=$ReportID&SubjectID=$SubjectID&SubjectGroupID=$SubjectGroupID&FromMS=$FromMS";
$buttons = "";
$buttons .= $linterface->GET_ACTION_BTN($iDiscipline['ImportOtherRecords'], "button", "window.location='import.php?$parameters'");
$buttons .= "&nbsp;";
$buttons .= $linterface->GET_ACTION_BTN($eReportCard['Button']['back_to_PersonalCharacteristics'], "button", "window.location = 'edit.php?$parameters'");

?>

<br />
<form name="form1" action="import_update.php" method="POST" onsubmit="return checkForm();" enctype="multipart/form-data">
	<?=$linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION)?>
	<br style="clear:both;" />
	
	<?=$linterface->GET_STEPS($STEPS_OBJ)?>
	<br style="clear:both;" />
	
	<div class="table_board">
		<table class="form_table_v30">
			<?=$pageInfo?>
		</table>
		<br />
		
    	<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr><td><?=$x?></td></tr>
		</table>
		<br />
	</div>
	<br style="clear:both;" />
	
	<div class="edit_bottom_v30">
		<?=$buttons?>
	</div>
	<br style="clear:both;" />
</form>
<br />

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>
