<?php
// modifying by : Bill
###########################################################################################################
############################################ !!! Important !!! ############################################
######## If need to upload this file before [ip.2.5.7.1.1], MUST upload with libreportcard2008.php ########
###########################################################################################################
###########################################################################################################  
/********************************************************
 * Modification log
 * 20160122 Bill:	[2016-0120-1521-35066]
 * 		- skip Subject Group Teacher Checking when display Overall Subject ($eRCTemplateSetting['PersonalChar']['SGTeacherOnly_HideOverall'])
 * 20160120 Bill:	[2015-0421-1144-05164]
 * 		- added $eRCTemplateSetting['Marksheet']['ReportDisplayWithinSubmissionDate'] logic
 * 			display report within submission period only	[removed]
 * 		- added $eRCTemplateSetting['PersonalChar']['DisplayOverallOnly'] logic
 * 			only allow users to edit Overall Subject
 * 20151223 Ivan [X86217] [ip.2.5.7.1.1]:
 * 		- added $eRCTemplateSetting['Marksheet']['DefaultReportUsingIssueDate'] logic
 * 20120326 Marcus:
 * 		- Cater allow to set time for submission, verification
 * 		- 2012-0112-1622-04073 - 迦密愛禮信中學 - Schedule of Report Card Management 
 * ******************************************************/

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libteaching.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");

intranet_auth();
intranet_opendb();

if (!$plugin['ReportCard2008'])
{
	header ("Location: /");
	intranet_closedb();
	exit();
}
	
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");

if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
{
	include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
	$lreportcard = new libreportcardcustom();
} else {
	$lreportcard = new libreportcard();
}

if (!$lreportcard->hasAccessRight()) 
{
	header ("Location: /");
	intranet_closedb();
	exit();
}

$CurrentPage = "Management_PersonalCharacteristics";
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();


# tag information
$TAGS_OBJ[] = array($eReportCard['PersonalCharacteristics'], "", 0);

$linterface = new interface_html();
$linterface->LAYOUT_START();

if($ck_ReportCard_UserType=="TEACHER")
{
	$PAGE_TITLE = $eReportCard['Management_MarksheetSubmission'];
	$lteaching = new libteaching();
	
	$FormArrCT = $lreportcard->returnSunjectTeacherForm($UserID);
				
	# check is class teacher or not
	$TeacherClassAry = $lteaching->returnTeacherClassWithLevel($UserID);
	$TeacherClass = $TeacherClassAry[0]['ClassName'];
		        	
	if(!empty($TeacherClass))
	{
		for($i=0;$i<sizeof($TeacherClassAry);$i++)
		{
			$thisClassLevelID = $TeacherClassAry[$i]['ClassLevelID'];
			$searchResult = multiarray_search($FormArrCT , "ClassLevelID", $thisClassLevelID);
			if($searchResult == "-1")
			{
				$thisAry = array(
					"0"=>$TeacherClassAry[$i]['ClassLevelID'],
					"ClassLevelID"=>$TeacherClassAry[$i]['ClassLevelID'],
					"1"=>$TeacherClassAry[$i]['LevelName'],
					"LevelName"=>$TeacherClassAry[$i]['LevelName']);
				$FormArrCT = array_merge($FormArrCT, array($thisAry));
			}
		}
		
		# sort $FormArrCT
		foreach ($FormArrCT as $key => $row) 
		{
		    $field1[$key] 	= $row['ClassLevelID'];
			$field2[$key]  	= $row['LevelName'];
		}
		array_multisort($field1, SORT_ASC, $field2, SORT_ASC, $FormArrCT);
	}
	
	$ClassLevelID = ($ClassLevelID == "") ? $FormArrCT[0][0] : $ClassLevelID;
	$ClassArrCT = $lreportcard->returnSubjectTeacherClass($UserID, $ClassLevelID);
				
	$FormSubjectArrCT = $lreportcard->returnSubjectwOrder($ClassLevelID, 1, $UserID, $intranet_session_language);
	
	if(!empty($TeacherClass))
	{
		$searchResult = multiarray_search($TeacherClassAry , "ClassLevelID", $ClassLevelID);
		if($searchResult != "-1")
		{	
			$searchResult2 = multiarray_search($ClassArrCT , "ClassName", $TeacherClassAry[$searchResult]['ClassName']);
			if($searchResult2 == "-1")
			{
				$thisAry = array(
					"0"=>$TeacherClassAry[$searchResult]['ClassID'],
					"ClassID"=>$TeacherClassAry[$searchResult]['ClassID'],
					"1"=>$TeacherClassAry[$searchResult]['ClassName'],
					"ClassName"=>$TeacherClassAry[$searchResult]['ClassName'],
					"2"=>$TeacherClassAry[$searchResult]['ClassLevelID'],
					"ClassLevelID"=>$TeacherClassAry[$searchResult]['ClassLevelID']);
				$ClassArrCT = array_merge($ClassArrCT, array($thisAry));
			}
		
			$FormSubjectArrCT = $lreportcard->returnSubjectwOrder($ClassLevelID, 1, "", $intranet_session_language);
		}
	}
	
}

$FormArr = $ck_ReportCard_UserType=="ADMIN" ? $lreportcard->GET_ALL_FORMS(1) : $FormArrCT;
$ClassLevelID = ($ClassLevelID == "") ? $FormArr[0][0] : $ClassLevelID;
$ClassArr = $ck_ReportCard_UserType=="ADMIN" ? $lreportcard->GET_CLASSES_BY_FORM($ClassLevelID) : $ClassArrCT;
		
# Filters - By Form (ClassLevelID)
$FormSelection = $linterface->GET_SELECTION_BOX($FormArr, "name='ClassLevelID' id='ClassLevelID' class='tabletexttoptext' onchange='jCHANGE_MARKSHEET_REVISION(1)'", "", $ClassLevelID);

# Filters - By Type (Term1, Term2, Whole Year, etc)
// Get Semester Type from the reportcard template corresponding to ClassLevelID
$ReportTypeSelection = '';
$ReportTypeArr = array();
$ReportTypeOption = array();
$ReportTypeArr = $lreportcard->GET_REPORT_TYPES($ClassLevelID);

// [2015-0421-1144-05164]
// Teacher - hide reports that out of submission period		[removed]
//$displayReportInSubmissionPeriod = ($eRCTemplateSetting['Marksheet']['ReportDisplayWithinSubmissionDate'] && $ck_ReportCard_UserType=="TEACHER")? true : false;
//$ReportTypeArr = $lreportcard->GET_REPORT_TYPES($ClassLevelID, "", "", 0, false, $displayReportInSubmissionPeriod);

# if not selected specific report card, preset the filter to the current submission period report template
if ($ReportID == '')
{
//	$conds = " 		ClassLevelID = '$ClassLevelID' 
//				AND 
//					NOW() BETWEEN MarksheetSubmissionStart AND MarksheetSubmissionEnd
//			";
//	$ReportInfoArr = $lreportcard->returnReportTemplateBasicInfo('', $conds);
//	$ReportID = ($ReportInfoArr['ReportID'])? $ReportInfoArr['ReportID'] : '';
	$ReportID = $lreportcard->getReportFilteringDefaultReportId($ClassLevelID);
}

$ReportType = '';
if(count($ReportTypeArr) > 0){
	for($j=0 ; $j<count($ReportTypeArr) ; $j++){
		if($ReportTypeArr[$j]['ReportID'] == $ReportID){
			$isFormReportType = 1;
			$ReportType = $ReportTypeArr[$j]['Semester'];
		}
		
		$ReportTypeOption[$j][0] = $ReportTypeArr[$j]['ReportID'];
		$ReportTypeOption[$j][1] = $ReportTypeArr[$j]['SemesterTitle'];
	}
	$ReportTypeSelection .= $linterface->GET_SELECTION_BOX($ReportTypeOption, 'name="ReportType" id="ReportType" class="tabletexttoptext" onchange="jCHANGE_MARKSHEET_REVISION()"', '', $ReportID);
	
	$ReportType = ($ReportType != "") ? $ReportType : $ReportTypeArr[0]['Semester'];
}
$ReportID = ($isFormReportType) ? $ReportID : $ReportTypeOption[0][0];

$ReportSetting = $lreportcard->returnReportTemplateBasicInfo($ReportID);
$SemID = $ReportSetting['Semester'];
		
# Filters - By Form Subjects
// A Flag, called $isFormSubject, is used to identify whether currect Form has that subject or not
$FormSubjectArr = array();
$SubjectListArr = array();
$SubjectOption = array();
$SubjectOption[0] = array("-1", "ALL");
$count = 1;
$isFormSubject = 0;		// Default: Assume the selected subject does not belongs to that Form(ClassLevelID)

if (!isset($SubjectID) && $SubjectID!='0')
{
	$SubjectID = '-1';
}

// Get Subjects By Form (ClassLevelID)
//$FormSubjectArr = $lreportcard->returnSubjectwOrder($ClassLevelID, 1);
$FormSubjectArr = $ck_ReportCard_UserType=="ADMIN" ? $lreportcard->returnSubjectwOrder($ClassLevelID, 1, "", $intranet_session_language) : $FormSubjectArrCT;

// [2015-0421-1144-05164]
// Show Subject List when
// 1. Current Form with Subjects
// 2. Settings - Not only display Overall
if(!empty($FormSubjectArr) && !$eRCTemplateSetting['PersonalChar']['DisplayOverallOnly']){
	foreach($FormSubjectArr as $FormSubjectID => $Data){
		if(is_array($Data)){
			foreach($Data as $FormCmpSubjectID => $SubjectName){							
				$FormSubjectID = ($FormCmpSubjectID==0) ? $FormSubjectID : $FormCmpSubjectID;
				if($FormSubjectID == $SubjectID || $SubjectID==0)
					$isFormSubject = 1;
					
				// Prepare Subject Selection Box 
				if($FormCmpSubjectID==0){
					$SubjectFormGradingArr = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $FormSubjectID,0,0,$ReportID);
					
					if ($SubjectID != 0)
					{
						$SubjectListArr[($count-1)]['SubjectID'] = $FormSubjectID;
						$SubjectListArr[($count-1)]['SubjectName'] = $SubjectName;
						$SubjectListArr[($count-1)]['SchemeID'] = $SubjectFormGradingArr['SchemeID'];
					}
					
					
					$SubjectOption[$count][0] = $FormSubjectID;
					$SubjectOption[$count][1] = $SubjectName;
					$count++;
				}
			}
		}
	}
}

/*
* Use for Selection Box
* $SubjectListArr[][0] : SubjectID
* $SubjectListArr[][1] : SubjectName		
*/
$SubjectID = ($isFormSubject) ? $SubjectID : '';
if($SubjectID == ""){
	$SubjectID = $SubjectOption[0][0];
}
else {
	
	if ($SubjectID != 0)
	{
		$SubjectFormGradingArr = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID,0,0,$ReportID);
		$SubjectListArr = array();
		$SubjectListArr[0]['SubjectID'] = $SubjectID;
		$SubjectListArr[0]['SubjectName'] = $lreportcard->GET_SUBJECT_NAME($SubjectID);
		$SubjectListArr[0]['SchemeID'] = $SubjectFormGradingArr['SchemeID'];
	}
}
$SelectedSubjectName = $lreportcard->GET_SUBJECT_NAME($SubjectID);

if ($SubjectID==0 || $SubjectID=='-1')
{
	$overallIndex = count($SubjectListArr);
	$SubjectListArr[$overallIndex]['SubjectID'] = 0;
	$SubjectListArr[$overallIndex]['SubjectName'] = $eReportCard['Template']['OverallCombined'];
}

$overallIndex = count($SubjectOption);
$SubjectOption[$overallIndex][0] = 0;
$SubjectOption[$overallIndex][1] = $eReportCard['Template']['OverallCombined'];
$SubjectSelection = $linterface->GET_SELECTION_BOX($SubjectOption, 'name="SubjectID" id="SubjectID" class="tabletexttoptext" onchange="jCHANGE_MARKSHEET_REVISION()"', '', $SubjectID);

// check personal characteristic
if($eRCTemplateSetting['PersonalCharacteristicSubmissionSchedule'] && $ck_ReportCard_UserType!="ADMIN" )
{
//	$reportDetail = $lreportcard->GET_REPORT_DATE_PERIOD($Condition="", $ReportID);
//	$PCStart = $reportDetail[0]['PCStart'];
//	$PCEnd = $reportDetail[0]['PCEnd']+(60*60*24); //inclusive

	$PeriodAction = "PCSubmission";
	$PeriodType = $lreportcard->COMPARE_REPORT_PERIOD($ReportID, $PeriodAction);

//	if($PCStart && $PCEnd && ($PCStart <= time() && $PCEnd >= time()))
	if($PeriodType==1)
		$ViewEditIcon = '<img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_edit_b.gif" width="20" height="20" border="0" title="'.$button_edit.' '.$eReportCard['PersonalCharacteristics'].'">';
	else
		$ViewEditIcon = '<img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_view.gif" width="20" height="20" border="0" title="'.$button_view.' '.$eReportCard['PersonalCharacteristics'].'">';
}
else
	$ViewEditIcon = '<img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_edit_b.gif" width="20" height="20" border="0" title="'.$button_edit.' '.$eReportCard['PersonalCharacteristics'].'">';

# Subject List
$AllSubjectColumnWeightStatus = array();
$display = '';
$cnt = 0;
$namefield = getNameFieldByLang('iu.');
if(count($ClassArr) > 0 && count($SubjectListArr) > 0)
{
	if ($SemID == 'F')
	{
		// consolidated report => Class-based
		for($i=0 ; $i<count($ClassArr) ;$i++)
		{
			$ClassID = $ClassArr[$i][0];
			$ClassName = $ClassArr[$i][1];
			
			for($j=0 ; $j<count($SubjectListArr) ; $j++)
			{
				$ClassSubjectName = $SubjectListArr[$j]['SubjectName'];
				$ClassSubjectID = $SubjectListArr[$j]['SubjectID'];
				
				# get last modified date
				$table = $lreportcard->DBName .".RC_PERSONAL_CHARACTERISTICS_DATA ";
				if (isset($ReportID) && $ReportID != "")
				{
					$conds = " AND pcd.ReportID = '$ReportID' ";	
				}
				$sql = "SELECT 
							Max(pcd.DateModified) as LastModifiedDate,
							$namefield as LastModifiedBy
						FROM 
							$table as pcd
							Inner Join
							YEAR_CLASS_USER as ycu On (pcd.StudentID = ycu.UserID)
							Inner Join
							INTRANET_USER as iu On (pcd.TeacherID = iu.UserID)
						WHERE 
							ycu.YearClassID = '".$ClassID."'
							AND
							pcd.SubjectID = '$ClassSubjectID' 
							And
							pcd.SubjectGroupID = 0
							$conds 
						Group By
							pcd.ReportID, pcd.SubjectID, pcd.SubjectGroupID
						";
				$result = $lreportcard->returnArray($sql);
				
				$LastModifiedDate = $result[0]['LastModifiedDate'];
				$LastModifiedBy = $result[0]['LastModifiedBy'];
				
				if ($LastModifiedDate != '' && $LastModifiedBy != '')
					$LastModified = $LastModifiedDate.' ('.$LastModifiedBy.')';
				else
					$LastModified = '&nbsp;';
				
				//check whether the subject teacher teaches this subject in this class or not
				if($ck_ReportCard_UserType=="TEACHER")
				{
					# is Class Teacher?
					$temp = 0;
					if(!empty($TeacherClass) || !empty($TaughtClass))
					{
						$searchResult = multiarray_search($TeacherClassAry , "ClassID", $ClassID);
						if($searchResult != "-1")	$temp = 1;
					}
					
					// [2016-0120-1521-35066] Skip Subject Group Teacher Checking
					if($eRCTemplateSetting['PersonalChar']['SGTeacherOnly_HideOverall'] && $ClassSubjectID==0){
						// do nothing
					}
					else{
						# is Subject Group Teacher?	
						$t = $lreportcard->checkSubjectTeacher($UserID, $ClassSubjectID, $ClassID);
						if(!empty($t))	$temp = 1;
					}
					
					if(!$temp) 	continue;
				}
				
				$row_css = ($cnt % 2 == 1) ? "tablerow1" : "tablerow2";
				
				$display .= '<tr>';
				$display .= '<td class="'.$row_css.' tabletext">'. $ClassName. '</td>';
				$display .= '<td class="'.$row_css.' tabletext">'. $ClassSubjectName .'</td>';
				$display .= '<td class="'.$row_css.' tabletext">'. $Lang['General']['EmptySymbol']. '</td>';
			    $display .= '<td class="'.$row_css.' tabletext">'. $LastModified .'</td>';
			    $display .= '<td class="'.$row_css.' tabletext" align="right">';
			    $display .= '<a href="edit.php?ClassLevelID='.$ClassLevelID.'&ClassID='.$ClassID.'&ReportID='.$ReportID.'&SubjectID='.$ClassSubjectID.'">'.$ViewEditIcon.'</a>';
				$display .= '</td>';
				$display .= '</tr>';			          
				$cnt++;
			}
			
		}
	}
	else
	{
		// term report => Subject Group based
		
		# Get all Subject Groups of the Subject
		$TeacherID = '';
		if ($ck_ReportCard_UserType=="TEACHER" && (empty($TeacherClass) || $TeacherClassLevelID!=$ClassLevelID))
		{
			# Subject Teacher => View teaching subject(s) only
			$TeacherID = $_SESSION['UserID'];
		}
		
		// Display Subject Group
		for($i=0 ; $i<count($SubjectListArr) ; $i++)
		{
			$thisSubjectID = $SubjectListArr[$i]['SubjectID'];
			$thisSubjectName = $SubjectListArr[$i]['SubjectName'];
			
			if ($thisSubjectID == 0)
				continue;
			
			$obj_Subject = new Subject($thisSubjectID);
			$AllSubjectGroupArr = $obj_Subject->Get_Subject_Group_List($SemID, $ClassLevelID, '', $TeacherID);
			$numOfSubjectGroup = count($AllSubjectGroupArr);
			
			for ($j=0; $j<$numOfSubjectGroup; $j++)
			{
				$thisSubjectGroupID = $AllSubjectGroupArr[$j]['SubjectGroupID'];
				$thisSubjectComponentID = $AllSubjectGroupArr[$j]['SubjectComponentID'];
				$thisSubjectGroupTitleEN = $AllSubjectGroupArr[$j]['ClassTitleEN'];
				$thisSubjectGroupTitleB5 = $AllSubjectGroupArr[$j]['ClassTitleB5'];
				$thisSubjectGroupName = Get_Lang_Selection($thisSubjectGroupTitleB5, $thisSubjectGroupTitleEN);
				
				# Check if the teacher is teaching this class / have class student enrolled in this class
				if ($ck_ReportCard_UserType!="ADMIN")
				{
					$isTeachingThisSubjectGroup = $lreportcard->Is_Teaching_Subject_Group($_SESSION['UserID'], $thisSubjectGroupID);
					$hasClassStudentInThisSubjectGroup = $lreportcard->Has_Class_Student_Enrolled_In_Subject_Group($TeacherClassID, $thisSubjectGroupID);
					
					if (!$isTeachingThisSubjectGroup && !$hasClassStudentInThisSubjectGroup)
						continue;
				}
				
				# get last modified date
				$table = $lreportcard->DBName .".RC_PERSONAL_CHARACTERISTICS_DATA ";
				if (isset($ReportID) && $ReportID != "")
				{
					$conds = " AND pcd.ReportID = '$ReportID' ";	
				}
				$sql = "SELECT 
							Max(pcd.DateModified) as LastModifiedDate,
							$namefield as LastModifiedBy
						FROM 
							$table as pcd
							Inner Join
							INTRANET_USER as iu On (pcd.TeacherID = iu.UserID)
						WHERE 
							pcd.SubjectID = '$thisSubjectID' 
							And
							pcd.SubjectGroupID = '$thisSubjectGroupID'
							$conds 
						Group By
							pcd.ReportID, pcd.SubjectID, pcd.SubjectGroupID
						";
				$result = $lreportcard->returnArray($sql);
					
				$LastModifiedDate = $result[0]['LastModifiedDate'];
				$LastModifiedBy = $result[0]['LastModifiedBy'];
				
				if ($LastModifiedDate != '' && $LastModifiedBy != '')
					$LastModified = $LastModifiedDate.' ('.$LastModifiedBy.')';
				else
					$LastModified = '&nbsp;';
				
				$row_css = ($cnt % 2 == 1) ? "tablerow1" : "tablerow2";
					
				$display .= '<tr>';
				$display .= '<td class="'.$row_css.' tabletext">'. $Lang['General']['EmptySymbol']. '</td>';
				$display .= '<td class="'.$row_css.' tabletext">'. $thisSubjectName .'</td>';
				$display .= '<td class="'.$row_css.' tabletext">'. $thisSubjectGroupName. '</td>';
			    $display .= '<td class="'.$row_css.' tabletext">'. $LastModified .'</td>';
			    $display .= '<td class="'.$row_css.' tabletext" align="right">';
			    $display .= '<a href="edit.php?ClassLevelID='.$ClassLevelID.'&ClassID='.$ClassID.'&ReportID='.$ReportID.'&SubjectID='.$thisSubjectID.'&SubjectGroupID='.$thisSubjectGroupID.'">'.$ViewEditIcon.'</a>';
				$display .= '</td>';
				$display .= '</tr>';			          
				$cnt++;
			}
		}
		
		// Overall Personal Characteristics Display
		for($i=0 ; $i<count($ClassArr) ;$i++)
		{
			$ClassID = $ClassArr[$i][0];
			$ClassName = $ClassArr[$i][1];
			
			for($j=0 ; $j<count($SubjectListArr) ; $j++)
			{
				$ClassSubjectName = $SubjectListArr[$j]['SubjectName'];
				$ClassSubjectID = $SubjectListArr[$j]['SubjectID'];
				
				if ($ClassSubjectID != 0)
					continue;
				
				# get last modified date
				$table = $lreportcard->DBName .".RC_PERSONAL_CHARACTERISTICS_DATA ";
				if (isset($ReportID) && $ReportID != "")
				{
					$conds = " AND pcd.ReportID = '$ReportID' ";	
				}
				$sql = "SELECT 
							Max(pcd.DateModified) as LastModifiedDate,
							$namefield as LastModifiedBy
						FROM 
							$table as pcd
							Inner Join
							YEAR_CLASS_USER as ycu On (pcd.StudentID = ycu.UserID)
							Inner Join
							INTRANET_USER as iu On (pcd.TeacherID = iu.UserID)
						WHERE 
							ycu.YearClassID = '".$ClassID."'
							AND
							pcd.SubjectID = '$ClassSubjectID' 
							And
							pcd.SubjectGroupID = 0
							$conds 
						Group By
							pcd.ReportID, pcd.SubjectID, pcd.SubjectGroupID
						";
				$result = $lreportcard->returnArray($sql);
				
				$LastModifiedDate = $result[0]['LastModifiedDate'];
				$LastModifiedBy = $result[0]['LastModifiedBy'];
				
				if ($LastModifiedDate != '' && $LastModifiedBy != '')
					$LastModified = $LastModifiedDate.' ('.$LastModifiedBy.')';
				else
					$LastModified = '&nbsp;';
				
				//check whether the subject teacher teaches this subject in this class or not
				if($ck_ReportCard_UserType=="TEACHER")
				{
					# is Class Teacher?
					$temp = 0;
					if(!empty($TeacherClass) || !empty($TaughtClass))
					{
						$searchResult = multiarray_search($TeacherClassAry , "ClassID", $ClassID);
						if($searchResult != "-1")	$temp = 1;
					}
					
					// [2016-0120-1521-35066] Skip Subject Group Teacher Checking
					if($eRCTemplateSetting['PersonalChar']['SGTeacherOnly_HideOverall']){
						// do nothing
					}
					else{
						# is Subject Group Teacher?	
						$t = $lreportcard->checkSubjectTeacher($UserID, $ClassSubjectID, $ClassID);
						if(!empty($t))	$temp = 1;
					}
					
					if(!$temp) 	continue;
				}
				
				$row_css = ($cnt % 2 == 1) ? "tablerow1" : "tablerow2";
				
				$display .= '<tr>';
				$display .= '<td class="'.$row_css.' tabletext">'. $ClassName. '</td>';
				$display .= '<td class="'.$row_css.' tabletext">'. $ClassSubjectName .'</td>';
				$display .= '<td class="'.$row_css.' tabletext">'. $Lang['General']['EmptySymbol']. '</td>';
			    $display .= '<td class="'.$row_css.' tabletext">'. $LastModified .'</td>';
			    $display .= '<td class="'.$row_css.' tabletext" align="right">';
			    $display .= '<a href="edit.php?ClassLevelID='.$ClassLevelID.'&ClassID='.$ClassID.'&ReportID='.$ReportID.'&SubjectID='.$ClassSubjectID.'">'.$ViewEditIcon.'</a>';
				$display .= '</td>';
				$display .= '</tr>';			          
				$cnt++;
			}
			
		}
	}
	
}
				
				
?>

<script language="javascript">
<!--
function jCHANGE_MARKSHEET_REVISION(ResetReportID)
{
	var form_id = jGET_SELECTBOX_SELECTED_VALUE("ClassLevelID");
	var report_id = jGET_SELECTBOX_SELECTED_VALUE("ReportType");
	var subject_id = jGET_SELECTBOX_SELECTED_VALUE("SubjectID");
	
	if (ResetReportID)
		report_id = '';
	
	location.href = "?ClassLevelID="+form_id+"&ReportID="+report_id+"&SubjectID="+subject_id;
}

function jGET_SELECTBOX_SELECTED_VALUE(objName)
{
	var obj = document.getElementById(objName);
	if(obj==null)	return "";
	var index = obj.selectedIndex;
	var val = obj[index].value;
	return val;
}

//-->
</script>

<br/>
<form name="FormMain" method="POST" action="marksheet_complete_confirm.php">
<table width="96%" border="0" cellspacing="0" cellpadding="0">
  <tr><td>&nbsp;</td></tr> 
  <tr>
    <td>
      <table border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><?=$FormSelection?></td>
          <td>&nbsp;</td>
          <td><?=$ReportTypeSelection?></td>
          <td>&nbsp;</td>
          <td><?=$SubjectSelection?></td>
          <td>&nbsp;</td>
          <td align="right" width="100%"><?=$linterface->GET_SYS_MSG($msg);?></td>
  		</tr>
  	  </table>
  	</td>
  </tr> 
  <tr><td>&nbsp;</td></tr> 
  <tr>
    <td>
      <table width="100%" border="0" cellspacing="0" cellpadding="4">
        <tr class="tabletop">
          <td width="6%" class="tabletoplink"><?=$eReportCard['Class']?></td>
          <td class="tabletoplink"><?=$eReportCard['Subject']?></td>
          <td class="tabletoplink"><?=$Lang['SysMgr']['SubjectClassMapping']['SubjectGroupTitle']?></td>
          <td width="20%" class="tabletoplink"><?=$eReportCard['LastModifiedDate']?></td>
          <td width="4%" align="right" class="tabletoplink">&nbsp;</td>
        </tr>
        <?=$display?>
      </table>
    </td>
  </tr>
  <tr class="tablebottom">
    <td>
      <table width="100%" border="0" cellspacing="0" cellpadding="3">
        <tr>
    	  <td class="tabletext"><?=$total?>&nbsp;</td>
    	</tr>
      </table>
    </td>
  </tr>
  <?=$display_button?>
</table>

<input type="hidden" name="ClassLevelID" id="ClassLevelID" value="<?=$ClassLevelID?>"/>
<input type="hidden" name="ReportID" id="ReportID" value="<?=$ReportID?>"/>
<input type="hidden" name="SubjectID" id="SubjectID" value="<?=$SubjectID?>"/>

</form>
<br />

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
