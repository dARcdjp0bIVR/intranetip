<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_pc.php");
$lreportcard_pc = new libreportcard_pc();
$lreportcard = new libreportcardcustom();
$lreportcard->hasAccessRight();


# initialize data
$StudentIDArr = $_POST['StudentID'];
$CharIDArr = $_POST['CharID'];
$ClassLevelID = $_POST['ClassLevelID'];
$ClassID = $_POST['ClassID'];
$ReportID = $_POST['ReportID'];
$SubjectID = $_POST['SubjectID'];
$SubjectGroupID = $_POST['SubjectGroupID'];

$DataArr['ClassLevelID'] = $ClassLevelID;
$DataArr['ReportID'] = $ReportID;
$DataArr['SubjectID'] = $SubjectID;
$DataArr['SubjectGroupID'] = $SubjectGroupID;

$limport = new libimporttext();
$filepath = $userfile;
$filename = $userfile_name;

$lclass = new libclass();
$className = $lclass->getClassName($ClassID);

# Get all characteristics of the subject of the class level
/*	
	[0] CharID
	[1] Title
*/
$CharArr = $lreportcard->GET_CLASSLEVEL_CHARACTERISTICS($DataArr);
$numOfChar = count($CharArr);


# Get ScaleInfo
$ScaleInfoArr = $lreportcard_pc->Get_Personal_Characteristics_Scale();
$ScaleInfoAssoArr = BuildMultiKeyAssoc($ScaleInfoArr, 'Code');

### Get Student Personal Char Info
//$StudentPersonalCharAssoArr[$ReportID][$SubjectID][$StudentID]['comment' or $scaleID] = data
$StudentPersonalCharAssoArr = $lreportcard_pc->getPersonalCharacteristicsProcessedDataByBatch($StudentIDArr, $ReportID, $SubjectID, $ReturnScaleID=1, $ReturnCharID=1, $ReturnComment=1);



$ApplicableScaleInfoArr = $lreportcard_pc->Get_User_Applicable_Personal_Characteristics_Scale($_SESSION['UserID']);
$ApplicableScaleCodeArr = Get_Array_By_Key($ApplicableScaleInfoArr, 'Code');
unset($ApplicableScaleInfoArr);


$parmeters = "ClassLevelID=$ClassLevelID&ClassID=$ClassID&ReportID=$ReportID&SubjectID=$SubjectID&FromMS=$FromMS";
	
if($filepath=="none" || $filepath == "" )
{   # import failed
    header("Location: import.php?$parmeters&Result=import_failed2");
    exit();
}
else
{
    if($limport->CHECK_FILE_EXT($filename)) {
		# read file into array
		# return 0 if fail, return csv array if success
		//$data = $limport->GET_IMPORT_TXT($filepath);
		
		$ColumnTitleArr = array($eReportCard['PersonalChar']['ExportTitleArr']['En'][0], $eReportCard['PersonalChar']['ExportTitleArr']['En'][1]);
		$exportColumnPropertyArr = array(1, 1, 2);
		for ($i = 0 ; $i < $numOfChar; $i++) {
			$ColumnTitleArr[] = $CharArr[$i]['CharTitleEn'];
			$exportColumnPropertyArr[] = 1;
		}
		if($eRCTemplateSetting['PersonalCharacteristicComment']) {
			$exportColumnPropertyArr[] = 1;
		}
		
		$data = $limport->GET_IMPORT_TXT_WITH_REFERENCE($filepath, 0, "<!--LineBreakHere-->", $ColumnTitleArr, $exportColumnPropertyArr);
		$TitleRowData = array_shift($data);
		
		$result_ary = array();
		$imported_student = array();
		$imported_performance = array();
		
		# construct column-ID mapping of the Characteristics
		$columnCharID = array();
		for ($i=2; $i<sizeof($TitleRowData); $i++)	//characteristics starts at index=1, sizeof($data[0]) = length of title row
		{
			for ($j=0; $j<sizeof($CharArr); $j++)
			{
				if (trim($TitleRowData[$i]) == trim($CharArr[$j]['CharTitleEn']))
				{
					$columnCharID[$i] = $CharArr[$j]['CharID'];
					break;
				}
			}
		}
										
		# loop through all students	
		$numOfData = count($data);
		for ($i=0; $i<$numOfData; $i++)	{
			/*
			 * $data[$i][0] = ClassName
			 * $data[$i][1] = ClassNumber
			 * others => performance
			 */
			if (empty($data[$i]))
			{
				$result_ary[$i][] = "empty_row";
				continue;
			}
			
			$className = $data[$i][0];
			$classNumber = $data[$i][1];
			
			//check if the student is existing
			//$studentID = $lu->returnUserID($className, $classNumber, 2, '0,1,2');
			$thisStudentInfoArr = $lreportcard->Get_Student_Info_By_ClassName_ClassNumber($className, $classNumber);
			$studentID = $thisStudentInfoArr[0]['UserID'];
			
			if(!$studentID)	{
				$result_ary[$i][] = "student_not_found";
				continue;
			}
								
			// check if comment is too long
			if($eRCTemplateSetting['PersonalCharacteristicComment']) {
				$Comment = str_replace('<!--LineBreakHere-->', "\r\n", trim($data[$i][sizeof($TitleRowData)-1]));
				if(mb_strlen($Comment) > $eRCTemplateSetting['PersonalCharacteristicCommentLength']) {
					$result_ary[$i][] = "comment_too_long";
					continue;
				}
			}
								
			$CharData = array();
			$CharCodeValid = true;
			$sizeofChar = $eRCTemplateSetting['PersonalCharacteristicComment']? sizeof($TitleRowData)-1 : sizeof($TitleRowData);
			// loop for each performance input
			for ($j=2; $j<$sizeofChar; $j++) {	//performance starts at index=2, sizeof($TitleRowData) = length of title row
				$thisCharID = $columnCharID[$j];
				$thisScaleCode = $data[$i][$j];
				$thisScaleID = $ScaleInfoAssoArr[$thisScaleCode]['ScaleID'];
				
				
				if ($thisScaleCode == '') {
					// if empty => copy from the existing result => no change in data
					$thisScaleID = $StudentPersonalCharAssoArr[$ReportID][$SubjectID][$studentID][$thisCharID];
				}
				else {
					// check if scale exist
					if ($thisScaleID == '') {
						# not exist	
						$result_ary[$i][] = "code_not_found";
						$CharCodeValid = false;
						$imported_performance[$i][$j] = false;
						continue;
					}
					
					// check if the user has scale mgmt right
					if (!in_array($thisScaleCode, (array)$ApplicableScaleCodeArr)) {
						$result_ary[$i][] = "option_no_right";
						$CharCodeValid = false;
						$imported_performance[$i][$j] = false;
						continue;
					}
				}
				
				# Construct array CharData
				$CharData[] = $thisCharID.":".$thisScaleID;
				$imported_performance[$i][$j] = true;
			}

			
			# Construct CharData to be insert into the array
			$DataArr['CharData'] = implode(",", $CharData);
			$DataArr['StudentID'] = $studentID;
			$DataArr['Comment'] = $Comment;
			
			if ($CharCodeValid) {
				if ($lreportcard->IS_STUDENT_CHARACTERISTICS_RECORD_EXIST($DataArr)) {
					# update	
					$result = $lreportcard->UPDATE_STUDENT_CHARACTERISTICS_RECORD($DataArr);
				}
				else {
					# insert
					$result = $lreportcard->INSERT_STUDENT_CHARACTERISTICS_RECORD($DataArr);
				}
				
				if ($result) {
					array_push($imported_student, $studentID);
				}
			}
		}	
    }
    else
    {
		header("Location: import.php?$parmeters&Result=import_failed");
		exit();
	}
}	
intranet_closedb();

?>
<body>
	<form name="form1" method="post" action="import_result.php">
		<input type="hidden" name="ClassLevelID" id="ClassLevelID" value="<?=$ClassLevelID?>"/>
		<input type="hidden" name="ReportID" id="ReportID" value="<?=$ReportID?>"/>
		<input type="hidden" name="SubjectID" id="SubjectID" value="<?=$SubjectID?>"/>
		<input type="hidden" name="SubjectGroupID" id="SubjectGroupID" value="<?=$SubjectGroupID?>"/>
		<input type="hidden" name="ClassID" id="ClassID" value="<?=$ClassID?>"/>
		<input type="hidden" name="data" value="<?=rawurlencode(serialize($data));?>">
		<input type="hidden" name="result_ary" value="<?=rawurlencode(serialize($result_ary));?>">
		<input type="hidden" name="imported_student" value="<?=rawurlencode(serialize($imported_student));?>">
		<input type="hidden" name="imported_performance" value="<?=rawurlencode(serialize($imported_performance));?>">
		<input type="hidden" name="FromMS" id="FromMS" value="<?=$FromMS?>"/>
	</form>
</body>

<script language="javascript">
	document.form1.submit();
</script>