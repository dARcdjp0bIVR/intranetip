<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_pc.php");
$lreportcard_pc = new libreportcard_pc();
$lreportcard = new libreportcardcustom();
$lreportcard->hasAccessRight();


if ($FromMS == 1) {
	$CurrentPage = "Management_MarkSheetRevision";
}
else {
	$CurrentPage = "Management_PersonalCharacteristics";
}
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();

# tag information
$TAGS_OBJ[] = array($eReportCard['PersonalCharacteristics'], "", 0);

$linterface = new interface_html();
$linterface->LAYOUT_START();

$PAGE_TITLE = $eReportCard['Management_MarksheetSubmission'];

# navigation
$lclass = new libclass();
$className = $lclass->getClassName($ClassID);
$className = ($className=='')? $Lang['General']['EmptySymbol'] : $className;
$PAGE_NAVIGATION[] = array($eReportCard['PersonalCharacteristics']);
$PAGE_NAVIGATION[] = array($Lang['Btn']['Import']);

# Step Info
$STEPS_OBJ[] = array($i_general_select_csv_file, 1);
$STEPS_OBJ[] = array($i_general_imported_result, 0);

# Information above the table
// Get Report Info
$ReportInfoArr = $lreportcard->returnReportTemplateBasicInfo($ReportID);
$reportTitle = str_replace(":_:", "<br>", $ReportInfoArr['ReportTitle']);

//get subject title
if ($SubjectID == 0)
	$subjectTitle = $eReportCard['Template']['OverallCombined'];
else
	$subjectTitle = $lreportcard->GET_SUBJECT_NAME_LANG($SubjectID, $intranet_session_language);
	
if ($SubjectGroupID != '') {
	$obj_SubjectGroup = new subject_term_class($SubjectGroupID);
	$SubjectGroupName = $obj_SubjectGroup->Get_Class_Title();	
}
else {
	$SubjectGroupName = $Lang['General']['EmptySymbol'];
}	


### Page Info
$pageInfo = '';
// Subject Group
if ($SubjectGroupID == '') {
	$pageInfo .= "<tr><td class=\"field_title\">".$Lang['General']['Class']."</td><td class=\"field_c\">".$className."</td></tr>";
}

// Subject
$pageInfo .= "<tr><td class=\"field_title\">".$eReportCard['Subject']."</td><td class=\"field_c\">".$subjectTitle."</td></tr>";

// Class
if ($SubjectGroupID != '') {
	$pageInfo .= "<tr><td class=\"field_title\">".$Lang['SysMgr']['SubjectClassMapping']['SubjectGroup']."</td><td class=\"field_c\">".$SubjectGroupName."</td></tr>";
}

// Report
$pageInfo .= "<tr><td class=\"field_title\">".$eReportCard['Reports']."</td><td class=\"field_c\">".$reportTitle."</td></tr>";

		
# Get Performance Wordings Description
$DataArr['ClassLevelID'] = $ClassLevelID;
$DataArr['ReportID'] = $ReportID;
$DataArr['SubjectID'] = $SubjectID;
$ScaleInfoAssoArr = $lreportcard->returnPersonalCharOptions($br=0, $order='', $Lang_Par='', $ReturnNormalTeacherOnly=0, $CodeAsKey=true);

$ApplicableScaleInfoArr = $lreportcard_pc->Get_User_Applicable_Personal_Characteristics_Scale($_SESSION['UserID']);
$ApplicableScaleCodeArr = Get_Array_By_Key($ApplicableScaleInfoArr, 'Code');

$format_str = '';
foreach ((array)$ScaleInfoAssoArr as $thisScaleCode => $thisScaleName) {
	if (!in_array($thisScaleCode, (array)$ApplicableScaleCodeArr)) {
		continue;
	}
	
	$format_str .= "\"".$thisScaleCode."\" ".$eReportCard['Represents']." \"".$thisScaleName."\"";
	$format_str .= "<br />";
}
$format_str .= "<a class=\"tablelink\" href=\"export.php?ClassLevelID=$ClassLevelID&ClassID=$ClassID&ReportID=$ReportID&SubjectID=$SubjectID&SubjectGroupID=$SubjectGroupID\" target=\"_blank\">[". $i_general_clickheredownloadsample ."]</a><br />";
?>

<script type="text/JavaScript" language="JavaScript">
	function trim(str){
		return str.replace(/^\s+|\s+$/g,"");
	}
	
	function checkForm() {
		obj = document.form1;
		if (trim(obj.elements["userfile"].value) == "") {
			 alert('<?=$eEnrolment['AlertSelectFile']?>');
			 obj.elements["userfile"].focus();
			 return false;
		}
		
		obj.submit();
	}
</script>
<br />
<form name="form1" action="import_update.php" method="POST" onsubmit="return checkForm();" enctype="multipart/form-data">
	<?=$linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION)?>
	<br style="clear:both;" />
	
	<?=$linterface->GET_STEPS($STEPS_OBJ)?>
	<br style="clear:both;" />
	
	<div class="table_board">
		<table class="form_table_v30">
			<?=$pageInfo?>
			<tr>
				<td class="field_title"><?=$i_select_file?></td>
				<td class="field_c">
					<input class="file" type="file" name="userfile">
					<br />
					<span class="tabletextrequire"><?= $Lang['General']['ImportArr']['SecondRowWarn']?></span>
				</td>
			</tr>
			<tr>
				<td class="field_title"><?=$eReportCard['Format']?></td>
				<td class="field_c"><?=$format_str?></td>
			</tr>
		</table>
	</div>
	<br style="clear:both;" />
	
	<div class="edit_bottom_v30">
		<?= $linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "submit", "")?>
		<?= $linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "self.location='edit.php?ClassLevelID=$ClassLevelID&ClassID=$ClassID&ReportID=$ReportID&SubjectID=$SubjectID&SubjectGroupID=$SubjectGroupID&FromMS=$FromMS'")?>
	</div>
	<br style="clear:both;" />
		
	<input type="hidden" name="ClassLevelID" id="ClassLevelID" value="<?=$ClassLevelID?>"/>
	<input type="hidden" name="ReportID" id="ReportID" value="<?=$ReportID?>"/>
	<input type="hidden" name="SubjectID" id="SubjectID" value="<?=$SubjectID?>"/>
	<input type="hidden" name="SubjectGroupID" id="SubjectGroupID" value="<?=$SubjectGroupID?>"/>
	<input type="hidden" name="ClassID" id="ClassID" value="<?=$ClassID?>"/>
	<input type="hidden" name="FromMS" id="FromMS" value="<?=$FromMS?>"/>
</form>
<?
	$linterface->LAYOUT_STOP();
?>