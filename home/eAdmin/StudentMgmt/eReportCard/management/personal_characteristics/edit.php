<?php
/* Using:
 * 
 * Modification Log:
 * 	20160120 Bill:	[2015-0421-1144-05164]
 * 	- added js function Toggle_Display_Student_Detail() to support student info preview
 * 		(related to $eRCTemplateSetting['PersonalChar']['ShowStudentDetails']) 
 * 	20120109 Ivan:
 * 	- added $eRCTemplateSetting['PersonalChar']['PrintPage'] for print mode logic
 */
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_pc.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
//include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.ivan.php");
$linterface = new interface_html();
$lreportcard_ui = new libreportcard_ui();
$lreportcard_pc = new libreportcard_pc();
$lreportcard_pc->hasAccessRight(); 

# initialize data
$FromMS = $_GET['FromMS'];
$SubjectID = $_GET['SubjectID'];
$SubjectGroupID = $_GET['SubjectGroupID'];
$ReportID = $_GET['ReportID'];
$ClassLevelID = $_GET['ClassLevelID'];
$ClassID = $_GET['ClassID'];
$ReturnMsgKey = $_GET['ReturnMsgKey'];

if ($FromMS == 1) {
	$CurrentPage = "Management_MarkSheetRevision";
}
else {
	$CurrentPage = "Management_PersonalCharacteristics";
}
$MODULE_OBJ = $lreportcard_pc->GET_MODULE_OBJ_ARR();

# tag information
$TAGS_OBJ[] = array($eReportCard['PersonalCharacteristics'], "", 0);


$ReturnMsg = $Lang['General']['ReturnMessage'][$ReturnMsgKey];
$linterface->LAYOUT_START($ReturnMsg);

echo $linterface->Include_CopyPaste_JS_CSS();
echo $lreportcard_ui->Get_Management_PersonalChar_Edit_UI($ReportID, $SubjectID, $SubjectGroupID, $ClassLevelID, $ClassID, $FromMS);

?>
<script language="javascript">
var jsDefaultPasteMethod = 'text';	// For copy and paste js

$(document).ready( function() {	
	
});

function goSubmit() {
	$('form#form1').submit();
}

function js_Go_Print() {
	var jsOriginalAction = $('form#form1').attr('action');
	$('form#form1').attr('action', '../../print_page.php').attr('target', '_blank').submit();
	
	// restore form attribute
	$('form#form1').attr('action', jsOriginalAction).attr('target', '_self');
}

function Toggle_Display_Student_Detail(targetID){
	var LinkID = 'personal_'+targetID;
	var DetailsLayerClass = 'personalDetail_'+targetID;
	
	if (document.getElementById(LinkID).className == "zoom_in") {
		document.getElementById(LinkID).className = "zoom_out";
		$('tr.'+DetailsLayerClass).css('display','');
	}
	else {
		document.getElementById(LinkID).className = "zoom_in";
		$('tr.'+DetailsLayerClass).css('display','none');
	}
}

</script>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>