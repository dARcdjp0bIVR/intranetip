<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

if (!$plugin['ReportCard2008'])
{
	header ("Location: /");
	intranet_closedb();
	exit();
}
	
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");

if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
{
	include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
	$lreportcard = new libreportcardcustom();
} else {
	$lreportcard = new libreportcard();
}

if (!$lreportcard->hasAccessRight()) 
{
	header ("Location: /");
	intranet_closedb();
	exit();
}

# initialize data
$Data['SubjectID'] = $SubjectID;
$Data['ReportID'] = $ReportID;
$Data['ClassLevelID'] = $ClassLevelID;
$Data['ClassID'] = $ClassID;
		
$ExportArr = array();
$lexport = new libexporttext();

$filename = "personal_characteristics_sample.csv";

# Get Performance selection wordings array
$PerformanceWordings = $lreportcard->returnPersonalCharOptions(0,0);

# Get all characteristics of the subject of the class level
if ($lreportcard->DisplayCharItemInReverseOrder == 1)
	$ReverseDisplay = 1;
else
	$ReverseDisplay = 0;
$CharArr = $lreportcard->GET_CLASSLEVEL_CHARACTERISTICS($Data, $ReverseDisplay);

# Construct 2 sample data
for($i=0; $i<2; $i++)
{
	$ExportArr[$i][0] = "0".strval($i+1);	//add a zero before the number
	
	for($j=0; $j<sizeof($CharArr); $j++)
	{
		if ((($j+$i) % 2) == 0)
		{
			$ExportArr[$i][$j+1] = 10;
		}
		else
		{
			$ExportArr[$i][$j+1] = 50;
		}
	}
}		

# Define column title
$exportColumn[] = $eEnrollment['sample_csv']['ClassNumber'];			
for ($i = 0 ; $i < sizeof($CharArr); $i++) {
	$exportColumn[] = $CharArr[$i][1];
}

$export_content = $lexport->GET_EXPORT_TXT($ExportArr, $exportColumn);

intranet_closedb();

# Output the file to user browser
$lexport->EXPORT_FILE($filename, $export_content);

?>
