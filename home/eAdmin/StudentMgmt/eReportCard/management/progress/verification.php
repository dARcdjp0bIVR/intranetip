<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if ($plugin['ReportCard2008']) {
	$lreportcard = new libreportcard();
	
    if ($lreportcard->hasAccessRight()) {
        $linterface = new interface_html();
        $CurrentPage = "Management_ClassTeacherComment";
		$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
		############################################################################################################
		
		# Get ClassLevelID (Form) of the reportcard template
		$FormArr = $lreportcard->GET_ALL_FORMS(1);
		$ClassLevelID = ($ClassLevelID == "") ? $FormArr[0][0] : $ClassLevelID;
		
		# Filters - By Form (ClassLevelID)
		$FormSelection = $linterface->GET_SELECTION_BOX($FormArr, "name='ClassLevelID' class='tabletexttoptext' onchange='document.form1.submit()'", "", $ClassLevelID);
		
		# Filters - By Type (Term1, Term2, Whole Year, etc)
		// Get Semester Type from the reportcard template corresponding to ClassLevelID
		$ReportTypeSelection = '';
		$ReportTypeArr = array();
		$ReportTypeOption = array();
		$ReportTypeArr = $lreportcard->GET_REPORT_TYPES($ClassLevelID);
		
		if(count($ReportTypeArr) > 0){
			for($j=0 ; $j<count($ReportTypeArr) ; $j++){		
				$ReportTypeOption[$j][0] = $ReportTypeArr[$j]['ReportID'];
				$ReportTypeOption[$j][1] = $ReportTypeArr[$j]['SemesterTitle'];
			}
			$ReportTypeSelection = $linterface->GET_SELECTION_BOX($ReportTypeOption, 'name="ReportID" class="tabletexttoptext" onchange="document.form1.submit()"', '', $ReportID);
		}
		$ReportID = ($ReportID == "") ? $ReportTypeOption[0][0] : $ReportID;
		
		# Main Content
		$ClassArr = $lreportcard->GET_CLASSES_BY_FORM($ClassLevelID);
		$display = "";
		if(sizeof($ClassArr) > 0) {
			$table = $lreportcard->DBName.".RC_MARKSHEET_COMMENT";
			for($i=0 ; $i<count($ClassArr) ;$i++){
				$ClassID = $ClassArr[$i][0];
				$ClassName = $ClassArr[$i][1];
				// Student Info Array
				$StudentArray = $lreportcard->GET_STUDENT_BY_CLASS($ClassID);
				$StudentSize = sizeof($StudentArray);
				$StudentIDArray = array();
				$StudentIDList = "";
				if ($StudentSize > 0) {
					// Student ID list of students in a class
					for($j=0; $j<$StudentSize; $j++)
						$StudentIDArray[] = $StudentArray[$j][0];
					if(!empty($StudentIDArray))
						$StudentIDList = implode(",", $StudentIDArray);
				}
				
				# get the last modified date and last modified user
				$NameField = getNameFieldByLang2("b.");
				$sql = "SELECT a.ReportID, $NameField AS Name, MAX(a.DateModified) AS DateModified ";
				$sql .= "FROM $table AS a, INTRANET_USER AS b ";
				$sql .= "WHERE a.TeacherID = b.UserID AND a.ReportID = '$ReportID' AND a.SubjectID = '' ";
				if ($StudentSize > 0)
					$sql .= "AND a.StudentID IN ($StudentIDList) ";
				$sql .= "GROUP BY a.ReportID";
				
				$result = $lreportcard->returnArray($sql);
				if (sizeof($result) > 0) {
					$dateModified = $result[0]["DateModified"];
					$lastModifiedBy = $result[0]["Name"];
				} else {
					$dateModified = "-";
					$lastModifiedBy = "-";
				}
				
				$tr_css = ($i % 2 == 1) ? "tablegreenrow2" : "tablegreenrow1";
				$display .= "<tr class='$tr_css'>";
				$display .= "<td class='tabletext'>$ClassName</td>";
				$display .= "<td align='center' class='tabletext'><a href='edit.php?ClassLevelID=$ClassLevelID&ClassID=$ClassID&ReportID=$ReportID'><img src='".$image_path."/".$LAYOUT_SKIN."/icon_edit_b.gif' width='20' height='20' border='0'></a></td>";
				$display .= "<td class='tabletext'>$dateModified</td>";
				$display .= "<td class='tabletext'>$lastModifiedBy</td>";
				$display .= "</tr>";
			}
		} else {
			$td_colspan = "4";
			$display .=	'<tr class="tablegreenrow1" height="40">
							<td class="tabletext" colspan="'.$td_colspan.'" align="center">'.$i_no_record_exists_msg.'</td>
						</tr>';
		}
		
		############################################################################################################
        
		# tag information
		$TAGS_OBJ[] = array($eReportCard['Management_ClassTeacherComment'], "", 0);
		
		$linterface->LAYOUT_START();
?>

<script language="javascript">
/*
* General JS function(s)
*/

/* Global */

</script>
<br/>
<form name="form1" method="POST">
<table width="98%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>
      <table border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><?=$FormSelection?></td>
          <td>&nbsp;</td>
          <td><?=$ReportTypeSelection?></td>
          <td>&nbsp;</td>
  		</tr>
  	  </table>
  	</td>
  </tr>  
  <tr>
    <td>
      <table width="100%" border="0" cellspacing="0" cellpadding="4">
        <tr class="tablegreentop">
          <td class="tabletoplink"><?=$eReportCard['Class']?></td>
          <td align="center" class="tabletoplink"><?=$eReportCard['TeacherComment']?></td>
          <td class="tabletoplink"><?=$eReportCard['LastModifiedDate']?></td>
          <td class="tabletoplink"><?=$eReportCard['LastModifiedBy']?></td>
        </tr>
		<?=$display?>
      </table>
    </td>
  </tr>
</table>
</form>
<?
        $linterface->LAYOUT_STOP();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>