<?php
// Using: Bill

/********************************************************
 * Modification log
 * 20170707 Bill [2017-0621-1726-12164]
 * 		- Create File
 * 		- Support Admin to view Verification Status (eRCTemplateSetting['MarksheetVerification']['OverallVerificationStatus'] = true) 
 * ******************************************************/

$PageRight = "ADMIN";

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

### Cookies handling
# Set Cookies
$arrCookies = array();
$arrCookies[] = array("ck_mgmt_progress_reportId", "ReportID");
$arrCookies[] = array("ck_mgmt_progress_classLevelId", "ClassLevelID");
if(isset($clearCoo) && $clearCoo == 1) {
	clearCookies($arrCookies);
}
else {
	updateGetCookies($arrCookies);
}

if ($plugin['ReportCard2008']) {
	# Initial
	include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
	include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
	if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") {
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
		$lreportcard = new libreportcardcustom();
	}
	else {
		$lreportcard = new libreportcard();
	}
	$lreportcard_ui = new libreportcard_ui();
	
    if ($lreportcard->hasAccessRight() && $eRCTemplateSetting['MarksheetVerification']['OverallVerificationStatus'])
    {
        $linterface = new interface_html();
        
        # Page Information
        $CurrentPage = "Management_Progress";
		$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
		
		############################################################################################################
		
		# Get Class Levels
		$FormArr = $lreportcard->GET_ALL_FORMS(1);
		if (sizeof($FormArr) > 0) {
			# Filter - Class Levels
			$ClassLevelID = ($ClassLevelID == "") ? $FormArr[0][0] : $ClassLevelID;
			$FormSelection = $linterface->GET_SELECTION_BOX($FormArr, "name='ClassLevelID' class='tabletexttoptext' onchange='document.form1.submit()'", "", $ClassLevelID);
			
			// Default Report
			if ($ReportID == '') {
				$ReportID = $lreportcard->getReportFilteringDefaultReportId($ClassLevelID);
			}
			
			# Get Report Templates
			$ReportTypeArr = array();
			$ReportTypeArr = $lreportcard->GET_REPORT_TYPES($ClassLevelID);
			
			# Filter - Report Templates
			$ReportTypeSelection = '';
			$ReportTypeOption = array();
			if(count($ReportTypeArr) > 0) {
				for($j=0 ; $j<count($ReportTypeArr) ; $j++) {
					$ReportIDList[] = $ReportTypeArr[$j]['ReportID'];
					$ReportTypeOption[$j][0] = $ReportTypeArr[$j]['ReportID'];
					$ReportTypeOption[$j][1] = $ReportTypeArr[$j]['SemesterTitle'];
				}
				$ReportTypeSelection = $linterface->GET_SELECTION_BOX($ReportTypeOption, "name='ReportID' class='tabletexttoptext' onchange='document.form1.submit()'", "", $ReportID);
			}
			
			# Get Report Info
			$ReportID = ($ReportID == "" || !in_array($ReportID, $ReportIDList)) ? $ReportTypeOption[0][0] : $ReportID;
			$ReportBasicInfoArr = $lreportcard->returnReportTemplateBasicInfo($ReportID);
			$SemID = $ReportBasicInfoArr['Semester'];
			
			# Get Report Verification Period
			$ReportDateArr = $lreportcard->GET_REPORT_DATE_PERIOD("ClassLevelID = '$ClassLevelID'", $ReportID);
			if ($ReportDateArr[0]["VerStart"] == "" || $ReportDateArr[0]["VerEnd"] == "") {
				$periodDisplay = $eReportCard['NotSet'];
			}
			else {
				$subStart = date("Y/m/d H:i:s", $ReportDateArr[0]["VerStart"]);
				$subEnd = date("Y/m/d H:i:s", $ReportDateArr[0]["VerEnd"]);
				$periodDisplay = "$subStart - $subEnd";
			}
				
			# Get Form Classes
			$ClassArr = $lreportcard->GET_CLASSES_BY_FORM($ClassLevelID);
			
			# Filter - Classes
			$ClassSelection = $linterface->GET_SELECTION_BOX($ClassArr, "name='ClassID' class='tabletexttoptext' onchange='document.form1.submit()'", $Lang['SysMgr']['FormClassMapping']['AllClass'], $ClassID);
			
			# Filter - Verification Status
			$StatusArr = array();
			$StatusArr[] = array(2, $Lang['eReportCard']['AllVerificationStatus']);
			$StatusArr[] = array(0, $Lang['eReportCard']['VerificationStatusIncomplete']);
			$StatusArr[] = array(1, $Lang['eReportCard']['VerificationStatusComplete']);
			$VerificationSelection = $linterface->GET_SELECTION_BOX($StatusArr, "name='StatusValue' class='tabletexttoptext' onchange='document.form1.submit()'", "", $StatusValue);
			
			# Get Form Class Students
			$StudentArr = $lreportcard->GET_STUDENT_BY_CLASSLEVEL($ReportID, $ClassLevelID, $ClassID);
			$StudentIDArr = Get_Array_By_Key((array)$StudentArr, "UserID");
			
			# Get Student Verification Status
			$VerificationStatus = $lreportcard->Get_Marksheet_Feedback_Info($ReportID, array(0), $StudentIDArr);
			$VerificationStatus = BuildMultiKeyAssoc((array)$VerificationStatus, array("StudentID"));
			
			$display = "";
			
			# Verification Period
			$display .= "<tr>";
				$display .= "<td class='tabletext'>".$eReportCard['VerificationPeriod'].": $periodDisplay</td>";
			$display .= "</tr>";
		
			# Verification Status Table
			$display .= "<tr>";
				$display .= "<td>";
					$display .= "<table class='common_table_list_v30' id='ContentTable'>";
						
						// Table Header
						$display .= "<thead>";
							$display .= "<tr>";	
								$display .= "<th style='width:2%;'>";
									$display .= "#";
								$display .= "</th>";
								$display .= "<th style='width:10%;'>";
									$display .= $Lang['General']['Class'];
								$display .= "</th>";
								$display .= "<th style='width:10%'>";
									$display .= $Lang['General']['ClassNumber'];
								$display .= "</th>";
								$display .= "<th style='width:28%;'>";
									$display .= $Lang['Identity']['Student'];
								$display .= "</th>";
								$display .= "<th style='width:15%;'>";
									$display .= $Lang['eReportCard']['VerificationStatus'];
								$display .= "</th>";
								$display .= "<th style='width:35%;'>";
									$display .= $Lang['General']['LastModifiedBy'];
								$display .= "</th>";
							$display .= "</tr>";
						$display .= "</thead>";
						
						// Table Content
						$i = 1;
						$display .= "<tbody>";
						foreach ($StudentArr as $thisStudentInfo)
						{
							// Student Info
							$thisStudentID = $thisStudentInfo['UserID'];
							$thisStudentName = $thisStudentInfo['StudentName'];
							$thisStudentClassName = $thisStudentInfo['ClassName'];
							$thisStudentClassNumber = $thisStudentInfo['ClassNumber'];
							
							// Student Verification Status
							$thisStudentVerStatusValue = 0;
							$thisStudentVerStatus = $Lang['eReportCard']['VerificationStatusIncomplete'];
							$thisStudentVerLastModified = "---";
							$thisStudentVerInfo = $VerificationStatus[$thisStudentID];
							if(!empty($thisStudentVerInfo)) {
								$thisStudentVerStatusValue = $thisStudentVerInfo['RecordStatus'];
								$thisStudentVerStatus = $thisStudentVerStatusValue==1? $Lang['eReportCard']['VerificationStatusComplete'] : $thisStudentVerStatus;
								$thisStudentVerLastModified = $thisStudentVerInfo['DateModified']. " (".$thisStudentVerInfo['LastModifiedBy'].")";
							}
								
							if($thisStudentVerStatusValue!=$StatusValue && $StatusValue!=2){
								continue;
							}
							
							// Table Content Row
							$display .= "<tr>";
								$display .= "<td>";
									$display .= $i++;
								$display .= "</td>";
								$display .= "<td>";
									$display .= $thisStudentClassName;
								$display .= "</td>";
								$display .= "<td>";
									$display .= $thisStudentClassNumber;
								$display .= "</td>";
								$display .= "<td>";
									$display .= $thisStudentName;
								$display .= "</td>";
								$display .= "<td>";
									$display .= $thisStudentVerStatus;
								$display .= "</td>";
								$display .= "<td>";
									$display .= $thisStudentVerLastModified;
								$display .= "</td>";
							$display .= "</tr>";
						}
						$display .= '</tbody>';
					
					$display .= "</table>";
			$display .= "</td></tr>";
		}
		else {
			$display .= "<tr>";
				$display .= "<td class='tabletext' height='40' align='center'>$i_no_record_exists_msg</td>";
			$display .= "</tr>";
		}
		
		############################################################################################################
        
		# Tag Information
		$TAGS_OBJ[] = array($eReportCard['Submission']." ".$eReportCard['Management_Progress'], $PATH_WRT_ROOT."/home/eAdmin/StudentMgmt/eReportCard/management/progress/submission.php", 0);
		if($eRCTemplateSetting['MarksheetVerification']['OverallVerificationStatus']) {
			$TAGS_OBJ[] = array($Lang['eReportCard']['VerificationStatus'], $PATH_WRT_ROOT."/home/eAdmin/StudentMgmt/eReportCard/management/progress/verification_status.php", 1);
		}
		
		$linterface->LAYOUT_START();
?>

<script language="javascript">

</script>

<br/>
<form id="form1" name="form1" method="POST" action="verification_status.php">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td class='tabletext' style='width:1%;'>&nbsp;</td>
		<td>
			<table width="98%" border="0" cellspacing="0" cellpadding="0" align="left">
				<tr>
					<td align="left"><?=$FormSelection?> <?=$ReportTypeSelection?> <?=$ClassSelection?> <?=$StatusSelection?> <?=$VerificationSelection?></td>
					<td align="right"><?=$linterface->GET_SYS_MSG($Result);?></td>
				</tr>
			</table>
			<br />
		</td>
	</tr>
	<tr>
		<td class='tabletext' style='width:1%;'>&nbsp;</td>
		<td>
			<table width="90%" border="0" cellspacing="0" cellpadding="4" align="left">
				<?= $display ?>
			</table>
		</td>
	</tr>
</table>
</form>
<?
		intranet_closedb();
        $linterface->LAYOUT_STOP();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>