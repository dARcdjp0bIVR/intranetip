<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
intranet_auth();
intranet_opendb();

$lreportcard_ui = new libreportcard_ui();
$Action = stripslashes($_REQUEST['Action']);

if ($Action == 'Progress_Subject_Group_Layer') {
	$ReportID = $_POST['ReportID'];
	$SubjectGroupID = $_POST['SubjectGroupID'];
	
	echo $lreportcard_ui->Get_Management_Progress_Subject_Group_Shortcut_Layer($ReportID, $SubjectGroupID);
}

intranet_closedb();
?>