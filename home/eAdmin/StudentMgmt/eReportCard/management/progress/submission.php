<?php
// Using: Bill

/********************************************************
 * Modification log
 * 20191031 Bill:   [2019-1031-1201-24235]
 *      - fixed cannot change reports, due to default report handling
 * 20190501 Bill:
 *      - prevent SQL Injection + Cross-site Scripting 
 * 20171227 Bill	[2017-0411-1005-40236]
 * 		- added Uniform Test Marksheet Subject Progress row for Term Extra Report ($eRCCommentSetting["ProgressDisplayUTScore"])
 * 20170707 Bill [2017-0621-1726-12164]
 * 		- Added Tab to view Verification Status (eRCTemplateSetting['MarksheetVerification']['OverallVerificationStatus'] = true)
 * 20170502 Bill	[2017-0412-1431-44256]
 * 		- Last Modified Date included Score Adjustment
 * 20170117 Bill	[DM#3147]	[ip.2.5.8.1.1]
 * 		- Update Parent Subject Last Modified Info Display
 * 		- Display component subject last modified date if larger than parent subject last modified date  
 * 20151223 Ivan [X86217] [ip.2.5.7.1.1]:
 * 		- added $eRCTemplateSetting['Marksheet']['DefaultReportUsingIssueDate'] logic
 * 20120326 Marcus:
 * 		- Cater allow to set time for submission, verification
 * 		- 2012-0112-1622-04073 - 迦密愛禮信中學 - Schedule of Report Card Management 
 * ******************************************************/

$PageRight = "ADMIN";

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

### Cookies Handling
$arrCookies = array();
$arrCookies[] = array("ck_mgmt_progress_reportId", "ReportID");
$arrCookies[] = array("ck_mgmt_progress_classLevelId", "ClassLevelID");
if(isset($clearCoo) && $clearCoo == 1) {
	clearCookies($arrCookies);
}
else {
	updateGetCookies($arrCookies);
}

if ($plugin["ReportCard2008"])
{
	include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
	include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
	if(isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") {
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
		$lreportcard = new libreportcardcustom();
	}
	else {
		$lreportcard = new libreportcard();
	}
	$lreportcard_ui = new libreportcard_ui();
	
    if ($lreportcard->hasAccessRight())
    {
        ### Handle SQL Injection + XSS [START]
        $ClassLevelID = IntegerSafe($ClassLevelID);
        $ReportID = IntegerSafe($ReportID);
        ### Handle SQL Injection + XSS [END]

        $linterface = new interface_html();
        
        # Current Page
        $CurrentPage = "Management_Progress";
		$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
		
		############################################################################################################
		
		# Get Forms of Report
		$FormArr = $lreportcard->GET_ALL_FORMS(1);
		if (sizeof($FormArr) > 0)
		{
			# Filters - By Form (ClassLevelID)
			$ClassLevelID = ($ClassLevelID == "") ? $FormArr[0][0] : $ClassLevelID;
			$FormSelection = $linterface->GET_SELECTION_BOX($FormArr, "name='ClassLevelID' class='tabletexttoptext' onchange='document.form1.submit()'", "", $ClassLevelID);
			
			# Get Form Classes & Class Student List
			$ClassIDList = array();
			$ClassStudentArr = array();
			$ClassArr = $lreportcard->GET_CLASSES_BY_FORM($ClassLevelID);
			if ($eRCCommentSetting["ProgressDisplayUTScore"] && sizeof($ClassArr) > 0)
			{
				for($i=0; $i<sizeof($ClassArr); $i++)
				{
					$ClassID = $ClassArr[$i]["ClassID"];
					$ClassStudentList = $lreportcard->GET_STUDENT_BY_CLASS($ClassID, "", 1);
					$ClassStudentArr[$ClassID] = Get_Array_By_Key((array)$ClassStudentList, "UserID");
				}
			}
			
			// Get Report corresponding to ClassLevelID
			if ($ReportID == "") {
				$ReportID = $lreportcard->getReportFilteringDefaultReportId($ClassLevelID);
			}
			
			# Filters - By Report (ReportID)
			$ReportIDList = array();
			$ReportTypeOption = array();
			$ReportTypeSelection = "";
			$ReportTypeArr = $lreportcard->GET_REPORT_TYPES($ClassLevelID);
			$ReportSemesterFirst = array(
                                            'Semester' => array(),
                                            'ReportID' => array()
			                        );
			if(count($ReportTypeArr) > 0)
			{
				for($j=0; $j<count($ReportTypeArr); $j++)
				{
					$ReportIDList[] = $ReportTypeArr[$j]["ReportID"];
					$ReportTypeOption[$j][0] = $ReportTypeArr[$j]["ReportID"];
					$ReportTypeOption[$j][1] = $ReportTypeArr[$j]["SemesterTitle"];
					
					// store the first option of each semester
					if(!in_array($ReportTypeArr[$j]['Semester'], $ReportSemesterFirst['Semester'])){
						$ReportSemesterFirst['Semester'][] = $ReportTypeArr[$j]['Semester'];
						$ReportSemesterFirst['ReportID'][] = $ReportTypeArr[$j]['ReportID'];
					}
				}

				// no report id > first report
				if($ReportID == "") {
					$ReportID = $ReportTypeOption[0][0];
				}
				// report not in current class level > first report of report semester
				else {
					$ReportInfo = $lreportcard->returnReportTemplateBasicInfo($ReportID);
                    if($ClassLevelID != $ReportInfo['ClassLevelID']) {
                        $index = array_search($ReportInfo['Semester'], $ReportSemesterFirst['Semester']);
                        $ReportID = ($index != -1) ? $ReportSemesterFirst['ReportID'][$index] : $ReportTypeOption[0][0];
                    }
				}
				$ReportTypeSelection = $linterface->GET_SELECTION_BOX($ReportTypeOption, "name='ReportID' class='tabletexttoptext' onchange='document.form1.submit()'", "", $ReportID);
			}
			$ReportID = ($ReportID == "" || !in_array($ReportID, $ReportIDList)) ? $ReportTypeOption[0][0] : $ReportID;

			# Report Template Info
			$BasicInfoArr = $lreportcard->returnReportTemplateBasicInfo($ReportID);
			$SemID = $BasicInfoArr["Semester"];
			$ReportType = $SemID == "F" ? "W" : "T";
			
			// Get Report Template Columns
			$ReportColumnIDArr = array();
			$ReportColumnIDList = "";
			$column_data = $lreportcard->returnReportTemplateColumnData($ReportID); 	// Column Data
			if(count($column_data) > 0)
			{
				for($i=0; $i<count($column_data); $i++) {
					$ReportColumnIDArr[] = $column_data[$i]["ReportColumnID"];
				}
				$ReportColumnIDList = implode(",", $ReportColumnIDArr);
			}
			
			// Get Report Template Submission Period
			$ReportDateArr = $lreportcard->GET_REPORT_DATE_PERIOD("ClassLevelID = '$ClassLevelID'", $ReportID);
			if ($ReportDateArr[0]["SubStart"] == "" || $ReportDateArr[0]["SubEnd"] == "") {
				$periodDisplay = $eReportCard["NotSet"];
			}
			else {
				$subStart = date("Y/m/d H:i:s", $ReportDateArr[0]["SubStart"]);
				$subEnd = date("Y/m/d H:i:s", $ReportDateArr[0]["SubEnd"]);
				$periodDisplay = "$subStart - $subEnd";
			}
			
			# Main Content Header
			$display = "";
			$classHeader = "";
			$classHeader .= "<tr>";
			$classHeader .= "<td width='25%' class='tabletext'>".$eReportCard["Period"].": $periodDisplay</td>";
			if (sizeof($ClassArr) > 0)
			{
				for($i=0; $i<sizeof($ClassArr); $i++)
				{
					$ClassID = $ClassArr[$i]["ClassID"];
					$ClassName = $ClassArr[$i]["ClassName"];
					$classHeader .= "<td class='retabletop tabletopnolink' align='center'>$ClassName</td>";
					$ClassIDList[] = $ClassID;
				}
			}
			else {
				$classHeader .= "<td class='tabletext' height='40' align='center'>$i_no_record_exists_msg</td>";
			}
			$classHeader .= "</tr>";
			
			$classHeader .= "<tr>";
			$classHeader .= "<td width='25%' class='tabletext'>&nbsp;</td>";
			for($i=0; $i<sizeof($ClassArr); $i++) {
				$classHeader .= "<td class='retabletop tabletopnolink' align='center'>&nbsp;</td>";
			}
			$classHeader .= "</tr>";
			
			// Get Report Template Subjects
			//$subjectList = $lreportcard->returnSubjectwOrder($ClassLevelID);
			$subjectList = $lreportcard->GET_FROM_SUBJECT_GRADING_SCHEME($ClassLevelID, 0, $ReportID);
			if(sizeof($subjectList) > 0)
			{
				$subjectRow = "";
				$SubjectIDList = array_keys($subjectList);
				
				// Check empty score in Marksheet
				$checkScoreEmptyArr = $lreportcard->ITERATE_MARKSHEET_SCORE($ClassIDList, $SubjectIDList, $ReportID, "checkEmpty");
				
				// Check Component Progress Status (if any of them is updated, then its parent subject is "In Progress")
				$CmpSubjectStatusArr = array();
				$CmpSubjectLastModifiedArr = array();
				foreach($subjectList as $subjectID => $subjectInfo)
				{
					if ($subjectInfo["is_cmpSubject"] != "1") continue;
					
					// Get Component Last Modified Info
					$LastModifiedArr = $lreportcard->GET_CLASS_MARKSHEET_LAST_MODIFIED($subjectID, $ReportColumnIDList);
					for($i=0; $i<sizeof($ClassArr); $i++)
					{
						if ($ReportType == "W") {
							$marksheetProgress = $lreportcard->GET_MARKSHEET_SUBMISSION_PROGRESS($subjectID, $ReportID, $ClassArr[$i]["ClassID"]);
						}
						else {
							$marksheetProgress = $lreportcard->GET_MARKSHEET_SUBMISSION_PROGRESS_OF_ALL_SUBJECT_GROUP($subjectID, $ReportID, $ClassArr[$i]["ClassID"]);
						}
						
						# Store Component Progress Status
						if (sizeof($marksheetProgress) > 0 && $marksheetProgress["IsCompleted"] == "1") {
							$CmpSubjectStatusArr[$ClassArr[$i]["ClassID"]][$subjectInfo["parentSubjectID"]][$subjectID] = "completed";
						}
						else if (isset($checkScoreEmptyArr[$ClassArr[$i]["ClassID"]][$subjectID])) { 
							if ($checkScoreEmptyArr[$ClassArr[$i]["ClassID"]][$subjectID] == "all") {
								$CmpSubjectStatusArr[$ClassArr[$i]["ClassID"]][$subjectInfo["parentSubjectID"]][$subjectID] = "notstart";
							}
							else if ($checkScoreEmptyArr[$ClassArr[$i]["ClassID"]][$subjectID] == 1) {
								$CmpSubjectStatusArr[$ClassArr[$i]["ClassID"]][$subjectInfo["parentSubjectID"]][$subjectID] = "inprogress";
							}
						}
						
						# [DM#3147] Store Component Last Modified Info
						if(!empty($LastModifiedArr))
						{
							$CmpSubjectLastModifiedDate = (isset($LastModifiedArr[$ClassArr[$i]["ClassID"]][0])) ? $LastModifiedArr[$ClassArr[$i]["ClassID"]][0] : "-";
							$CmpSubjectLastModifiedBy = (isset($LastModifiedArr[$ClassArr[$i]["ClassID"]][1])) ? $LastModifiedArr[$ClassArr[$i]["ClassID"]][1] : "-";
							if($CmpSubjectLastModifiedDate != "-")
							{
								// Get Temp Last Modifeid Info
								$tmpCmpSubjectLastModified = $CmpSubjectLastModifiedArr[$subjectInfo["parentSubjectID"]][$ClassArr[$i]["ClassID"]];
								$tmpCmpSubjectLastModifiedDate = $tmpCmpSubjectLastModified[0];
								
								// Update Temp Last Modifeid Info
								if(isset($tmpCmpSubjectLastModified)) {
									if($tmpCmpSubjectLastModifiedDate == "-" || $CmpSubjectLastModifiedDate > $tmpCmpSubjectLastModifiedDate) {
										$CmpSubjectLastModifiedArr[$subjectInfo["parentSubjectID"]][$ClassArr[$i]["ClassID"]][0] = $CmpSubjectLastModifiedDate;
										$CmpSubjectLastModifiedArr[$subjectInfo["parentSubjectID"]][$ClassArr[$i]["ClassID"]][1] = $CmpSubjectLastModifiedBy;
									}
								}
								else {
									$CmpSubjectLastModifiedArr[$subjectInfo["parentSubjectID"]][$ClassArr[$i]["ClassID"]][0] = $CmpSubjectLastModifiedDate;
									$CmpSubjectLastModifiedArr[$subjectInfo["parentSubjectID"]][$ClassArr[$i]["ClassID"]][1] = $CmpSubjectLastModifiedBy;
								}
							}
						}
					}
				}
				
				// Update Parent Subject Progress Status (if any)
				$ParentSubjectStatus = array();
				foreach ($CmpSubjectStatusArr as $tmpClassID => $ParentCmpSubjectList)
				{
					foreach ($ParentCmpSubjectList as $tmpParentSubjectID => $tmpCmpSubjectList)
					{
						$inProgressCount = 0;
						$notStartCount = 0;
						$completeCount = 0;
						foreach ($tmpCmpSubjectList as $tmpCmpSubjectID => $tmpStatus)
						{
							if ($tmpStatus == "inprogress") $inProgressCount++;
							if ($tmpStatus == "notstart") $notStartCount++;
							if ($tmpStatus == "completed") $completeCount++;
						}
						
						# Store Parent Subject Progress Status
						if ($notStartCount == sizeof($tmpCmpSubjectList)) {
							$ParentSubjectStatus[$tmpClassID][$tmpParentSubjectID] = "notstart";
						}
						else if ($completeCount == sizeof($tmpCmpSubjectList)) {
							$ParentSubjectStatus[$tmpClassID][$tmpParentSubjectID] = "completed";
						}
						else if ($inProgressCount > 0 || $completeCount > 0) {
							$ParentSubjectStatus[$tmpClassID][$tmpParentSubjectID] = "inprogress";
						}
					}
				}
				
				// Check Parent Subjects Progress Status
				foreach ($subjectList as $subjectID => $subjectInfo)
				{
					/*
					// skip this subject if it is a component subject
					if ($subjectInfo["is_cmpSubject"] == "1")
						continue;
					*/
					
					// Get Last Modified Info
					$LastModifiedArr = $lreportcard->GET_CLASS_MARKSHEET_LAST_MODIFIED($subjectID, $ReportColumnIDList);
					
					// [2017-0412-1431-44256] Get Score Adjustment Last Modified Info
					$ScoreAdjustLastModifiedArr = array();
					if($libreportcardcustom->IsEnableManualAdjustmentPosition || $eRCCommentSetting["ManualInputScore_ExtraReportOnly"]) {
						$ScoreAdjustLastModifiedArr = $lreportcard->GET_CLASS_SCORE_ARCHIVE_INFO_LAST_MODIFIED($subjectID, $ReportID);
					}
					
					# Main Content Subject rows
					$subjectRow .= "<tr>";
					$subjectNamePieces = explode("<br />",$subjectInfo["subjectName"]);
					$formatSubjectName = $subjectNamePieces[0]." (".$subjectNamePieces[1].")";
					if($subjectInfo["is_cmpSubject"] == "1") {
						$subjectRow .= "<td class='tablelist_sub_subject'><img src='$image_path/$LAYOUT_SKIN/10x10.gif' width='20' height='10'>".$formatSubjectName;
					}
					else {
						$subjectRow .= "<td class='tablelist_subject'>".$formatSubjectName;
					}
					$subjectRow .= "</td>";
					
					$classHasNoSubjectGroupAry = array();
					for($i=0; $i<sizeof($ClassArr); $i++)
					{
						$thisClassID = $ClassArr[$i]["ClassID"];
						$hasNoSubjectGroup = false;
						
						// Get Subjects Progress Status
						$marksheetProgress = array();
						if($ReportType == "W")
						{
							$marksheetProgress = $lreportcard->GET_MARKSHEET_SUBMISSION_PROGRESS($subjectID, $ReportID, $thisClassID);
						}
						else
						{
							$marksheetProgress = $lreportcard->GET_MARKSHEET_SUBMISSION_PROGRESS_OF_ALL_SUBJECT_GROUP($subjectID, $ReportID, $thisClassID);
							$StudiedSubjectArr = $lreportcard->Get_Student_Studying_SubjectID($ReportID, '', 1, $thisClassID);
							
							if(!in_array($subjectID, $StudiedSubjectArr))
							{
								$hasNoSubjectGroup = true;
								$classHasNoSubjectGroupAry[$thisClassID] = true;
								$displayStatus = $eReportCard["NoSubjectGroup"];
								$displayClass = "status_notstart";
							}
						}
						if($hasNoSubjectGroup == false)
						{
							$displayStatus = "&nbsp;";
							if(sizeof($marksheetProgress) > 0 && $marksheetProgress["IsCompleted"] == "1") {
								$displayStatus = "<strong>".$eReportCard["Confirmed"]."</strong>";
								$displayClass = "status_completed";
							}
							else if (isset($checkScoreEmptyArr[$ClassArr[$i]["ClassID"]][$subjectID])) { 
								if ($checkScoreEmptyArr[$ClassArr[$i]["ClassID"]][$subjectID] == "all") {
									$displayStatus = $eReportCard["NotStartedYet"];
									$displayClass = "status_notstart";
									if ($ParentSubjectStatus[$ClassArr[$i]["ClassID"]][$subjectID] == "inprogress") {
										$displayStatus = "<strong>".$eReportCard["InProgress"]."</strong>";
										$displayClass = "status_inprogress";
									}
								}
								else if ($checkScoreEmptyArr[$ClassArr[$i]["ClassID"]][$subjectID] == 1) {
									$displayStatus = "<strong>".$eReportCard["InProgress"]."</strong>";
									$displayClass = "status_inprogress";
								}
							}
						}
						
						// Get Subjects Last Modified Info
						$LastModifiedDate = "-";
						$LastModifiedBy = "-";
						if(!empty($LastModifiedArr))
						{
							$LastModifiedDate = (isset($LastModifiedArr[$ClassArr[$i]["ClassID"]][0])) ? $LastModifiedArr[$ClassArr[$i]["ClassID"]][0] : '-';
							$LastModifiedBy = (isset($LastModifiedArr[$ClassArr[$i]["ClassID"]][1])) ? $LastModifiedArr[$ClassArr[$i]["ClassID"]][1] : '-';
							
							# [DM#3147] Update Late Modified Info - Parent Subject
							if($subjectInfo["is_cmpSubject"] != "1")
							{
								$thisCmpSubjectLastModifiedDate = $CmpSubjectLastModifiedArr[$subjectID][$ClassArr[$i]["ClassID"]][0];
								if(isset($thisCmpSubjectLastModifiedDate) && $thisCmpSubjectLastModifiedDate != "-" && $thisCmpSubjectLastModifiedDate > $LastModifiedDate)
								{
									$LastModifiedDate = $thisCmpSubjectLastModifiedDate;
									$LastModifiedBy = (isset($CmpSubjectLastModifiedArr[$subjectID][$ClassArr[$i]["ClassID"]][1])) ? $CmpSubjectLastModifiedArr[$subjectID][$ClassArr[$i]["ClassID"]][1] : '-';
								}
							}
							
							// [2017-0412-1431-44256] 
							if(!empty($ScoreAdjustLastModifiedArr))
							{
								$ScoreAdjustLastModifiedDate = (isset($ScoreAdjustLastModifiedArr[$ClassArr[$i]["ClassID"]][0])) ? $ScoreAdjustLastModifiedArr[$ClassArr[$i]["ClassID"]][0] : "";
								$ScoreAdjustLastModifiedBy = (isset($ScoreAdjustLastModifiedArr[$ClassArr[$i]["ClassID"]][1])) ? $ScoreAdjustLastModifiedArr[$ClassArr[$i]["ClassID"]][1] : "";
								if ($ScoreAdjustLastModifiedDate != "" && $ScoreAdjustLastModifiedDate > $LastModifiedDate) {
									$LastModifiedDate = $ScoreAdjustLastModifiedDate;
									$LastModifiedBy = $ScoreAdjustLastModifiedBy;
								}
							}
						}
						
						$noScheme = ($subjectInfo["schemeID"] == null ||  $subjectInfo["schemeID"] == "") ? 1 : 0;
						$isCompleted = ($marksheetProgress["IsCompleted"] == "1") ? 1 : 0;
						
						$subjectRow .= "<td class='$displayClass tabletext' align='center'>";
						$subjectRow .= "<table width='100%' cellspacing='0' cellpadding='0' border='0'>
											<tr>
												<td class='tabletext' colspan='2' height='30' align='center'>";
												if ($hasNoSubjectGroup == false)
												{
													$subjectRow .= "<a class='contenttool' onclick='showShortCutPanel(this, $subjectID, ".$ClassArr[$i]["ClassID"].", $noScheme, $isCompleted, ".$subjectInfo["is_cmpSubject"].")' href='javascript:void(0)'>
																		$displayStatus&nbsp;
																		<img width='20' height='20' border='0' align='absmiddle' src='".$image_path."/".$LAYOUT_SKIN."/icon_edit_b.gif'/>
																	</a>";
												}
												else
												{
													$subjectRow .= $displayStatus;
												}
								$subjectRow .= "</td>
											</tr>";
						
						if ($LastModifiedDate != "-" && $LastModifiedBy != "-")
						{
							$subjectRow .= 	"<tr>
												<td class='retabletext_remark' align='left'>".$eReportCard["LastModifiedDate"]."</td>
												<td class='retabletext_remark' align='right'>$LastModifiedDate</td>
											</tr>
											<tr>
												<td class='retabletext_remark' align='left'></td>
												<td class='retabletext_remark' align='right'>$LastModifiedBy</td>
											</tr>";
						}
						$subjectRow .= "</table>";
						$subjectRow .= "</td>";
					}
					$subjectRow .= "</tr>";
					
					// [2017-0411-1005-40236] Uniform Test Marksheet Subject Progress row 
					if($eRCCommentSetting["ProgressDisplayUTScore"] && $ReportType == "T" && !$BasicInfoArr["isMainReport"] && $subjectInfo["is_cmpSubject"] != "1")
					{
						$subjectRow .= "<tr>";
							$subjectRow .= "<td class='tablelist_subject' align='right'>";
								$subjectRow .= $eReportCard["AdjustPositionLabal"];
								$subjectRow .= "<img src='$image_path/$LAYOUT_SKIN/10x10.gif' width='20' height='10'><img src='$image_path/$LAYOUT_SKIN/10x10.gif' width='20' height='10'>";
							$subjectRow .= "</td>";
						
						for($i=0; $i<sizeof($ClassArr); $i++)
						{
							$thisClassID = $ClassArr[$i]["ClassID"];
							$hasNoSubjectGroup = isset($classHasNoSubjectGroupAry[$thisClassID]);
							
							// Get Subjects Progress Status
							$UTLastModifiedDate = "-";
							$UTLastModifiedBy = "-";
							if($hasNoSubjectGroup)
							{
								$displayStatus = $eReportCard["NoSubjectGroup"];
								$displayClass = "status_notstart";
							}
							else
							{
								// Get Subject Class Student
								$thisClassStudent = $ClassStudentArr[$thisClassID];
								$thisSubjectGroupStudent = $lreportcard->Get_Student_In_Current_Subject_Group_Of_Subject($ReportID, $subjectID);
								$thisClassGroupStudent = array_intersect((array)$thisClassStudent, (array)$thisSubjectGroupStudent);
								$thisClassGroupStudentCount = count((array)$thisClassGroupStudent);
								
								// Get UT Student Result
								$UTStudentProgress = array();
								if($thisClassGroupStudentCount > 0)
								{
									$UTInputDataAry = $lreportcard->GET_MANUAL_ADJUSTED_POSITION($thisClassGroupStudent, $subjectID, $ReportID);
									foreach($UTInputDataAry as $thisStudentID => $thisUIInputData) {
										if(!empty($thisUIInputData["Info"])) {
											$UTStudentProgress[] = $thisStudentID;
										}
									}
								}
								$UTStudentProgressCount = count((array)$UTStudentProgress);
								
								// Get UT Last Modifeid Info
								$UTLastModifiedArr = $lreportcard->GET_STUDENT_SUBJECT_GROUP_SCORE_ARCHIVE_INFO_LAST_MODIFIED($ReportID, $thisClassGroupStudent, $subjectID);
								$UTLastModifiedDate = (isset($UTLastModifiedArr[0]["DateModified"])) ? $UTLastModifiedArr[0]["DateModified"] : "-";
								$UTLastModifiedBy = (isset($UTLastModifiedArr[0]["TeacherName"])) ? $UTLastModifiedArr[0]["TeacherName"] : "-";
								
								// Get UT Subject Progress Status
								$displayStatus = "&nbsp;";
								if($thisClassGroupStudentCount == $UTStudentProgressCount) {
									$displayStatus = "<strong>".$eReportCard["Confirmed"]."</strong>";
									$displayClass = "status_completed";
								}
								else {
									$displayStatus = $eReportCard["NotStartedYet"];
									$displayClass = "status_notstart";
									if ($UTStudentProgressCount > 0) {
										$displayStatus = "<strong>".$eReportCard["InProgress"]."</strong>";
										$displayClass = "status_inprogress";
									}
								}
							}
							
							$noScheme = ($subjectInfo["schemeID"] == null ||  $subjectInfo["schemeID"] == "") ? 1 : 0;
							$isCompleted = ($marksheetProgress["IsCompleted"] == "1") ? 1 : 0;
							
							$subjectRow .= "<td class='$displayClass tabletext' align='center'>";
							$subjectRow .= "<table width='100%' cellspacing='0' cellpadding='0' border='0'>
												<tr>
													<td class='tabletext' colspan='2' height='30' align='center'>";
//													if ($hasNoSubjectGroup == false)
//													{
//														$subjectRow .= "<a class='contenttool' onclick='showShortCutPanel(this, $subjectID, ".$ClassArr[$i]["ClassID"].", $noScheme, $isCompleted, ".$subjectInfo["is_cmpSubject"].")' href='javascript:void(0)'>
//																			$displayStatus&nbsp;
//																			<img width='20' height='20' border='0' align='absmiddle' src='".$image_path."/".$LAYOUT_SKIN."/icon_edit_b.gif'/>
//																		</a>";
//													}
//													else
//													{
														$subjectRow .= $displayStatus;
//													}
									$subjectRow .= "</td>
												</tr>";
							
							if ($UTLastModifiedDate != "-" && $UTLastModifiedBy != "-")
							{
								$subjectRow .= 	"<tr>
													<td class='retabletext_remark' align='left'>".$eReportCard["LastModifiedDate"]."</td>
													<td class='retabletext_remark' align='right'>".$UTLastModifiedDate."</td>
												</tr>
												<tr>
													<td class='retabletext_remark' align='left'></td>
													<td class='retabletext_remark' align='right'>".$UTLastModifiedBy."</td>
												</tr>";
							}
							$subjectRow .= "</table>";
							$subjectRow .= "</td>";
						}
						$subjectRow .= "</tr>";
					}
				}
			}
			else
			{
				$subjectRow = "<tr><td class='tabletext' height='40' align='center' colspan='".(sizeof($ClassArr)+1)."'>$i_no_record_exists_msg</td></tr>";
			}
			
			$display .= $classHeader.$subjectRow;
		}
		else
		{
			$display .= "<tr><td class='tabletext' height='40' align='center'>$i_no_record_exists_msg</td></tr>";
		}
		
		############################################################################################################
        
		# Tag Information
		$TAGS_OBJ[] = array($eReportCard["Submission"]." ".$eReportCard["Management_Progress"], $PATH_WRT_ROOT."/home/eAdmin/StudentMgmt/eReportCard/management/progress/submission.php", 1);
		if($eRCTemplateSetting["MarksheetVerification"]["OverallVerificationStatus"]) {
			$TAGS_OBJ[] = array($Lang["eReportCard"]["VerificationStatus"], $PATH_WRT_ROOT."/home/eAdmin/StudentMgmt/eReportCard/management/progress/verification_status.php", 0);
		}
		
		# View Mode info
		if ($eRCTemplateSetting["Management"]["Progress"]["SubjectGroup"]) {
			$ViewModeBtn = $lreportcard_ui->Get_Management_Progress_View_Mode_Button($ReportID, "Class");
		}
		
		$linterface->LAYOUT_START();
?>

<script language="javascript">
/*
* General JS function(s)
*/
var jsReportType = "<?=$ReportType?>";

function MM_showHideLayers() { 	// v9.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) 
  with (document) if (getElementById && ((obj=getElementById(args[i]))!=null)) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'block':(v=='hide')?'none':v; }
    obj.display=v; }
}

function showShortCutPanel(obj, subjectID, classID, noScheme, isCompleted, isComponent)
{
	var shortCutPanel = document.getElementById('shortCutPanel');
	var editMarksheetLink = document.getElementById('editMarksheetLink');
	var editCommentLink = document.getElementById('editCommentLink');
	var viewFeedbackLink = document.getElementById('viewFeedbackLink');
	var setCompleteLink = document.getElementById('setCompleteLink');
	
	var offsetL = -75;
	var reportID = <?= $ReportID=="" ? 0 : $ReportID ?>;
	var classLevelID = <?= $ClassLevelID=="" ? 0 : $ClassLevelID ?>;
	
	var parameters = 'ClassLevelID='+classLevelID+'&ReportID='+reportID+'&SubjectID='+subjectID+'&ClassID='+classID;
	
	editMarksheetLink.style.display = "inline";
	if (noScheme == 1) {
		editMarksheetLink.href = 'javascript:void(0)';
	}
	else {
		editMarksheetLink.href = '../marksheet_revision/marksheet_edit.php?'+parameters+'&isProgress=1';
	}
	editCommentLink.href = '../marksheet_revision/teacher_comment.php?'+parameters+'&redirectTo=submission';
	
	if (isComponent) {
		setCompleteLink.style.display = "none";
	}
	else {
		setCompleteLink.style.display = "inline";
		
		var ms_complete_confirm_filename = '';
		if (jsReportType == 'W') {
			ms_complete_confirm_filename = 'marksheet_complete_confirm';
		}
		else {
			ms_complete_confirm_filename = 'marksheet_complete_confirm_subject_group';
		}
		
		if (isCompleted == 1) {
			setCompleteLink.innerHTML = '<img width="20" height="20" border="0" align="absmiddle" src="<?=$image_path."/".$LAYOUT_SKIN?>/icon_undo_b.gif"/> <?=$eReportCard['SetToNotComplete']?>';
			setCompleteLink.href = '../marksheet_revision/' + ms_complete_confirm_filename + '.php?';
			setCompleteLink.href += 'ClassLevelID='+classLevelID+'&ReportID='+reportID+'&SubjectID='+subjectID+'&isProgress=1';
			setCompleteLink.href += '&complete_'+subjectID+'_'+classID+'=0'+'&ClassIDList[]='+classID+'&SubjectIDList[]='+subjectID;
		}
		else {
			setCompleteLink.innerHTML = '<img width="20" height="20" border="0" align="absmiddle" src="<?=$image_path."/".$LAYOUT_SKIN?>/icon_tick_green.gif"/> <?=$eReportCard['SetToComplete']?>';
			setCompleteLink.href = '../marksheet_revision/' + ms_complete_confirm_filename + '.php?';
			setCompleteLink.href += 'ClassLevelID='+classLevelID+'&ReportID='+reportID+'&SubjectID='+subjectID+'&isProgress=1';
			setCompleteLink.href += '&complete_'+subjectID+'_'+classID+'=1'+'&ClassIDList[]='+classID+'&SubjectIDList[]='+subjectID;
		}
	}
	
	shortCutPanel.style.left = getPostion(obj, 'offsetLeft') + offsetL + 'px';
	shortCutPanel.style.top = getPostion(obj, 'offsetTop') + 'px';
	
	MM_showHideLayers('shortCutPanel','','show');
}

function hideShortCutPanel() {	
	MM_showHideLayers('shortCutPanel','','hide');
}

function js_Change_View_Mode() {
	$('form#form1')	.attr('action', 'submission_sg.php')
					.submit();
}
/* Global */

</script>

<br/>
<form id="form1" name="form1" method="POST" action="submission.php">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>
			<table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
				<tr>
					<td align="right" colspan="2"><?=$ViewModeBtn?></td>
				</tr>
				<tr>
					<td align="left"><?=$FormSelection?><?=$ReportTypeSelection?></td>
					<td align="right"><?=$linterface->GET_SYS_MSG($Result);?></td>
				</tr>
			</table>
			<br />
		</td>
	</tr>
	<tr>
		<td>
			<table width="95%" border="0" cellspacing="0" cellpadding="4" align="center">
				<?=$display?>
			</table>
		</td>
	</tr>
</table>

<div style="position: absolute; width: 150px; z-index: 1000; display: none;" id="shortCutPanel">
	<table width="100%" cellspacing="0" cellpadding="0" border="0">
		<tr>
			<td height="19">
				<table width="100%" cellspacing="0" cellpadding="0" border="0">
					<tr>
						<td width="5" height="19"><img width="5" height="19" src="<?=$image_path."/".$LAYOUT_SKIN?>/can_board_01.gif"/></td>
						<td valign="middle" height="19" background="<?=$image_path."/".$LAYOUT_SKIN?>/can_board_02.gif">&nbsp;</td>
						<td width="19" height="19"><a onclick="hideShortCutPanel('shortCutPanel')" href="javascript:void(0)"><img width="19" height="19" border="0" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('pre_close22','','<?=$image_path."/".$LAYOUT_SKIN?>/can_board_close_on.gif',1)" id="pre_close22" name="pre_close22" src="<?=$image_path."/".$LAYOUT_SKIN?>/can_board_close_off.gif"/></a></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td>
				<table width="100%" cellspacing="0" cellpadding="0" border="0">
					<tr>
						<td width="5" background="<?=$image_path."/".$LAYOUT_SKIN?>/can_board_04.gif"><img width="5" height="19" src="<?=$image_path."/".$LAYOUT_SKIN?>/can_board_04.gif"/></td>
						<td bgcolor="#fffff7" align="center">
							<table cellspacing="0" cellpadding="3" border="0">
								<tr id="editMarksheetTr" class="tablegreenrow1">
									<td><a id="editMarksheetLink" class="contenttool" href=""><img width="20" height="20" border="0" align="absmiddle" src="<?=$image_path."/".$LAYOUT_SKIN?>/icon_edit_b.gif"/> <?=$eReportCard['Marksheet']?></a></td>
								</tr>
								<tr id="editCommentTr">
									<td><a id="editCommentLink" class="contenttool" href=""><img width="20" height="20" border="0" align="absmiddle" src="<?=$image_path."/".$LAYOUT_SKIN?>/icon_edit_b.gif"/> <?=$eReportCard['TeacherComment']?></a></td>
								</tr>
								<tr id="setCompleteTr" class="tablegreenrow1">
									<td><a id="setCompleteLink" class="contenttool" href="#"><img width="20" height="20" border="0" align="absmiddle" src="<?=$image_path."/".$LAYOUT_SKIN?>/icon_tick_green.gif"/> <?=$eReportCard['SetToComplete']?></a></td>
								</tr>
							</table>
							<br/>
						</td>
						<td width="6" background="<?=$image_path."/".$LAYOUT_SKIN?>/can_board_06.gif"><img width="6" height="6" src="<?=$image_path."/".$LAYOUT_SKIN?>/can_board_06.gif"/></td>
					</tr>
					<tr>
						<td width="5" height="6"><img width="5" height="6" src="<?=$image_path."/".$LAYOUT_SKIN?>/can_board_07.gif"/></td>
						<td height="6" background="<?=$image_path."/".$LAYOUT_SKIN?>/can_board_08.gif"><img width="5" height="6" src="<?=$image_path."/".$LAYOUT_SKIN?>/can_board_08.gif"/></td>
						<td width="6" height="6"><img width="6" height="6" src="<?=$image_path."/".$LAYOUT_SKIN?>/can_board_09.gif"/></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</div>
</form>
<?
		intranet_closedb();
        $linterface->LAYOUT_STOP();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>