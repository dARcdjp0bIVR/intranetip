<?php
// Using: 

/********************************************************
 * Modification log
 * 20190502 Bill:
 *      - prevent SQL Injection 
 * 20170322 Bill:	[2017-0109-1818-40164]
 * 		- added Report Publishing Period
 * 		- $eRCTemplateSetting['Management']['Schedule']['PublishReportPeriod']
 * 20161118 Bill:	[2015-1104-1130-08164]
 * 		- added Class Teacher's Comment Submission Period
 * 		- $eRCTemplateSetting['Management']['Schedule']['ClassTeacherCommentSubmissionPeriod']
 * 20130205 Rita:
 * 		- add verification period checking 
 * 		- $accessSettingData[EnableVerificationPeriod]==1
 * 20120326 Marcus:
 * 		- Cater allow to set time for submission, verification
 * 		- 2012-0112-1622-04073 - 迦密愛禮信中學 - Schedule of Report Card Management 
 * ******************************************************/

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

$PageRight = "ADMIN";

if (!isset($_POST) || (!isset($ReportID) && !isset($ReportIDs))) {
	header("Location:schedule.php");
}
if (!$plugin['ReportCard2008']) {
	intranet_closedb();
	header("Location:/index.php");
}

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_schedule.php");

$lreportcard = new libreportcard();
$lreportcard_schedule = new libreportcard_schedule();

if (!$lreportcard->hasAccessRight()) {
	intranet_closedb();
	header("Location:/index.php");
}

# Set the date to NULL for DB update if it is empty
function setDateFormat(&$date) {
    if (trim($date) != "" && trim($date) != "yyyy-mm-dd" && intranet_validateDate($date)) {
		$date = "'$date 00:00:00'";
	}
	else {
		$date = "NULL";
	}
}

function setDateTimeFormat(&$date, $hr, $min, $sec)
{
    if (trim($date) != "" && trim($date) != "yyyy-mm-dd" && intranet_validateDate($date))
	{
	    ### Handle SQL Injection + XSS [START]
	    $hr = IntegerSafe($hr);
	    $min = IntegerSafe($min);
	    $sec = IntegerSafe($sec);
	    ### Handle SQL Injection + XSS [END]
	    
		$hr = $hr<10?("0".$hr):$hr;
		$min = $min<10?("0".$min):$min;
		$sec = $sec<10?("0".$sec):$sec;
		$date = "'$date $hr:$min:$sec'";
	}
	else
	{
		$date = "NULL";
	}
}

# Load the highlight setting data
$accessSettingData = $lreportcard->LOAD_SETTING("AccessSettings");

### Handle SQL Injection + XSS [START]
if(isset($ReportIDs)) {
    $ReportIDs = IntegerSafe($ReportIDs);
}
else {
    $ReportID = IntegerSafe($ReportID);
}
$ApplySpecificMarksheetSubmission = IntegerSafe($_POST['ApplySpecificMarksheetSubmission']);
### Handle SQL Injection + XSS [END]

# Update Dates in RC_REPORT_TEMPLATE
setDateTimeFormat($subStart, $subStart_hour, $subStart_min, $subStart_sec);
setDateTimeFormat($subEnd, $subEnd_hour, $subEnd_min, $subEnd_sec);
if($accessSettingData[EnableVerificationPeriod]==1) {
	setDateTimeFormat($verStart, $verStart_hour, $verStart_min, $verStart_sec);
	setDateTimeFormat($verEnd, $verEnd_hour, $verEnd_min, $verEnd_sec);
}
setDateTimeFormat($PCStart, $PCStart_hour, $PCStart_min, $PCStart_sec);
setDateTimeFormat($PCEnd, $PCEnd_hour, $PCEnd_min, $PCEnd_sec);
setDateTimeFormat($specificSubStart, $specificSubStart_hour, $specificSubStart_min, $specificSubStart_sec);
setDateTimeFormat($specificSubEnd, $specificSubEnd_hour, $specificSubEnd_min, $specificSubEnd_sec);
setDateTimeFormat($PublishStart, $PublishStart_hour, $PublishStart_min, $PublishStart_sec);
setDateTimeFormat($PublishEnd, $PublishEnd_hour, $PublishEnd_min, $PublishEnd_sec);
setDateTimeFormat($CTCStart, $CTCStart_hour, $CTCStart_min, $CTCStart_sec);
setDateTimeFormat($CTCEnd, $CTCEnd_hour, $CTCEnd_min, $CTCEnd_sec);
if ($issue == "" || $issue == "yyyy-mm-dd" || !intranet_validateDate($issue)) {
	$issue = "NULL";
}
else {
	$issue = "'$issue'";
}

$successAry = array();
$table = $lreportcard->DBName.".RC_REPORT_TEMPLATE";
$sql  = "UPDATE $table SET ";
$sql .= "MarksheetSubmissionStart = $subStart, MarksheetSubmissionEnd = $subEnd, ";
if($accessSettingData[EnableVerificationPeriod]==1){
	$sql .= "MarksheetVerificationStart = $verStart, MarksheetVerificationEnd = $verEnd, ";
}
$sql .= "PCSubmissionStart = $PCStart, PCSubmissionEnd = $PCEnd, ";
$sql .= "PublishSubmissionStart = $PublishStart, PublishSubmissionEnd = $PublishEnd, ";
$sql .= "CTCSubmissionStart = $CTCStart, CTCSubmissionEnd = $CTCEnd, ";
$sql .= "Issued = $issue, ApplySpecificMarksheetSubmission = '$ApplySpecificMarksheetSubmission' ";
if(isset($ReportIDs)){
	$sql .= "WHERE ReportID IN ('".implode("','", (array)$ReportIDs)."')";
}
else{
	$sql .= "WHERE ReportID = '$ReportID'";
}
$successAry['updateTemplateData'] = $lreportcard->db_db_query($sql);


if ($eRCTemplateSetting['Management']['Schedule']['SpecificMarksheetSubmissionPeriod']) {
	if (isset($ReportIDs)) {
		$ReportID = $ReportIDs[0];
	}
	
	if ($eRCTemplateSetting['Management']['Schedule']['SpecificMarksheetSubmissionPeriod_subjectBased'] || $eRCTemplateSetting['Management']['Schedule']['SpecificMarksheetSubmissionPeriod_subjectGroupBased']) {
	    $specialSubmissionAssoAry = $_POST['specialSubmissionAssoAry'];
		$successAry['updateSpecificMarksheetSubmission'] = $lreportcard_schedule->saveSpecificMarksheetSubmissionSubjectBased($ReportID, $specificSubStart, $specificSubEnd, $specialSubmissionAssoAry);		
	}
	else {
	    $specificMarksheetSubmissionSelectedTeacherIdAry = $_POST['specificMarksheetSubmissionSelectedTeacherIdAry'];
	    $specificMarksheetSubmissionSelectedTeacherIdAry = IntegerSafe($specificMarksheetSubmissionSelectedTeacherIdAry);
	    
		$successAry['updateSpecificMarksheetSubmission'] = $lreportcard_schedule->saveSpecificMarksheetSubmission($ReportID, $specificSubStart, $specificSubEnd, $specificMarksheetSubmissionSelectedTeacherIdAry);
	}
}

### Log this action
$successAry['logAction'] = $lreportcard->Insert_Year_Based_Log('Edit_Schedule', $_POST);

intranet_closedb();

$Result = (!in_array(false, $successAry)) ? "update" : "update_failed";
header("Location:schedule.php?Result=$Result");
?>