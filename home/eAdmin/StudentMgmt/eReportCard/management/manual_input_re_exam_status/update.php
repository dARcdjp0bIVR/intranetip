<?php
// Using :

/**************************************************************
 *	Modification log:
 *
 *
 ***************************************************************/

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = array("ADMIN", "VIEW_GROUP");

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if (!isset($_POST) && ($ReportID == '' || $ClassID == '' || empty($modifyGradeArr))) {
    header("Location:index.php");
}

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");

if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") {
    include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
    $lreportcard = new libreportcardcustom();
}
else {
    $lreportcard = new libreportcard();
}

if (!$lreportcard->hasAccessRight()) {
    intranet_closedb();
    header("Location:/index.php");
}

$TargetYearReport = $lreportcard->returnReportTemplateBasicInfo('', '', $YearID, 'F', $isMainReport = 1);
$TargetYearReportID = $TargetYearReport['ReportID'];

foreach((array)$_POST['promotionArr'] as $thisStudentID => $thisPromotionStatus) {
    $lreportcard->UpdatePromotion($TargetYearReportID, $thisStudentID, $thisPromotionStatus);
}

$msg = $success ? "UpdateSuccess" : "UpdateUnsuccess";
header("location: index.php?YearID=$YearID&YearClassID=$YearClassID&PromotionStatus=$PromotionStatus&msg=$msg");
?>