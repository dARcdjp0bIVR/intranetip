<?php
######################################################
# Generate Statistic Report for Secondary Class Level
# 	Trigger from: index.php
# 	Required variables: $ClassLevelID, $ReportID
# 	Optional variables: $ReportColumnID
#
# 	Last updated: Andy Chan on 2008/6/3
######################################################

function returnGrandMSBand($Marks='-1') {
	if($Marks == -1)	return array("A1","A2","B3","B4","C5","C6","D7","E8","F9");
	if($Marks >= 75)	return "A1";
	if($Marks >= 70)	return "A2";
	if($Marks >= 65)	return "B3";
	if($Marks >= 60)	return "B4";
	if($Marks >= 55)	return "C5";
	if($Marks >= 50)	return "C6";
	if($Marks >= 45)	return "D7";
	if($Marks >= 40)	return "E8";
	return "F9";
}

function calculatePercent($Numerator, $Denumerator) {
	return number_format(round(($Numerator/$Denumerator*100) ,1) ,1);
}

// Root path
$PATH_WRT_ROOT = "../../../../../";

// Page access right
$PageRight = "ADMIN";

// Include general libraries
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

// Authorize intranet user & connect Database
intranet_auth();
intranet_opendb();

// Check if ReportCard module is enable
if ($plugin['ReportCard2008']) {
	// Include ReportCard libraries
	include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
	include_once($PATH_WRT_ROOT."includes/libreportcard2008w.php");
	
// Create objects
	if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName == "sis") 
	{
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
		$lreportcard = new libreportcardcustom();
	} else {
		$lreportcard = new libreportcard2008w();
	}
	$lclass = new libclass();
	
	$ExportArr = array();
	$lexport = new libexporttext();
	
	// Check module access right
	if ($lreportcard->hasAccessRight()) {
		// Check required data submitted from index.php
		if (!isset($ClassLevelID, $ReportID)) {
			intranet_closedb();
			header("Location: index.php");
			exit();
		}
		
		$LastGenerated = $lreportcard->GET_CURRENT_DATETIME();
		
		// Get ReportCard info
		$reportTemplateInfo = $lreportcard->returnReportTemplateBasicInfo($ReportID);
		
		// Check to make sure it is a whole year & generated report
		if ($reportTemplateInfo["LastGenerated"] == "0000-00-00 00:00:00" || $reportTemplateInfo["LastGenerated"] == "") {
			intranet_closedb();
			header("Location: index.php");
			exit();
		}
		
		// Get all report cards of the current ClassLevel (use to identify report type)
		$ReportTypeArr = $lreportcard->GET_REPORT_TYPES($ClassLevelID);
		$ReportIDList = array();
		for($i=0; $i<sizeof($ReportTypeArr); $i++) {
			$ReportIDList[] = $ReportTypeArr[$i]['ReportID'];
		}
		sort($ReportIDList);
		$ReportIDOrder = array_search($ReportID, $ReportIDList);
		
		// Get subjects of the current ClassLevel
		$FormSubjectArr = $lreportcard->returnSubjectwOrder($ClassLevelID, 1);
		
		// Get subjects' grading scheme of the current ClassLevel
		$FormSubjectGradingSchemeArr = $lreportcard->GET_FROM_SUBJECT_GRADING_SCHEME($ClassLevelID);
		
		// Get ClassLevel name
		$ClassLevelName = $lreportcard->returnClassLevel($ClassLevelID);
		
		// Get Classes
		$ClassArr = $lreportcard->GET_CLASSES_BY_FORM($ClassLevelID);
		
		// Get current school year
		$SchoolYear = $lreportcard->schoolYear;
		
		// Get columns info of the current ReportCard
		$ColumnTitleAry = $lreportcard->returnReportColoumnTitle($ReportID);
		$ColumnIDList = array_keys($ColumnTitleAry);
		
		// For secondary year end report, there are 4 terms
		if (sizeof($ColumnTitleAry) == 4) {
			if (isset($ReportColumnID) && $ReportColumnID != "") {
				if ($ReportColumnID == "first") {
					$DisplayColumnName = "1st Combined";
					$ReportID = $ReportIDList[0];
					$ReportColumnID = "";
				}
				if ($ReportColumnID == "second") {
					$DisplayColumnName = "2nd Combined";
					$ReportID = $ReportIDList[1];
					$ReportColumnID = "";
				}
				$StatReportTitle = "Summary of ".$DisplayColumnName." Results by Subjects Grade (Secondary)";
			} else {
				$StatReportTitle = "Summary of Overall Results by Subjects Grade (Secondary)";
			}
		} else {	// not 4 terms, should be half year report
			if (isset($ReportColumnID) && $ReportColumnID != "") {
				if ($ReportColumnID == "first") {
					$DisplayColumnName = "1st Combined";
					$ReportColumnID == "";
				}
				if ($ReportIDOrder == 0) {
					if ($ReportColumnID == $ColumnIDList[0])	$DisplayColumnName = "CA1";
					if ($ReportColumnID == $ColumnIDList[1])	$DisplayColumnName = "SA1";
				}
				
				if ($ReportColumnID == "second") {
					$DisplayColumnName = "2nd Combined";
					$ReportColumnID == "";
				}
				if ($ReportIDOrder == 1) {
					if ($ReportColumnID == $ColumnIDList[0])	$DisplayColumnName = "CA2";
					if ($ReportColumnID == $ColumnIDList[1])	$DisplayColumnName = "SA2";
				}
				
				$StatReportTitle = "Summary of ".$DisplayColumnName." Results by Subjects Grade (Secondary)";
			}
		}
		
		# build file name
		$thisAcadermicYear = getCurrentAcademicYear();
		$filename = intranet_undo_htmlspecialchars($thisAcadermicYear."_".$ClassLevelName."_".$StatReportTitle.".csv");
		
		# define column title
		$exportColumn = array();
		$exportColumn[]= $StatReportTitle;
		$numEmptyTitle = 15;
		for ($i=0; $i<$numEmptyTitle; $i++)
		{
			$exportColumn[]= "";
		}
		
		$iCounter = 0;
		$jCounter = 0;
		
		# School Year, Level
		$ExportArr[$iCounter][$jCounter] = "School Year ".$SchoolYear;
		$jCounter++;		
		$ExportArr[$iCounter][$jCounter] = "Level: ".$ClassLevelName;
		$jCounter++;
		$iCounter++;
		
		# Title Row
		$jCounter = 0;
		$ExportArr[$iCounter][$jCounter] = "";
		$jCounter++;
		$ExportArr[$iCounter][$jCounter] = "Subject Teacher";
		$jCounter++;
		$ExportArr[$iCounter][$jCounter] = "Total Number";
		$jCounter++;
		$ExportArr[$iCounter][$jCounter] = "Abs/Ex Number";
		$jCounter++;
		$ExportArr[$iCounter][$jCounter] = "Total Exam Number";
		$jCounter++;
		$ExportArr[$iCounter][$jCounter] = "A1 (75-100)";
		$jCounter++;
		$ExportArr[$iCounter][$jCounter] = "A2 (70-74)";
		$jCounter++;
		$ExportArr[$iCounter][$jCounter] = "B3 (65-69)";
		$jCounter++;
		$ExportArr[$iCounter][$jCounter] = "B4 (60-64)";
		$jCounter++;
		$ExportArr[$iCounter][$jCounter] = "C5 (55-59)";
		$jCounter++;
		$ExportArr[$iCounter][$jCounter] = "C6 (50-54)";
		$jCounter++;
		$ExportArr[$iCounter][$jCounter] = "D7 (45-49)";
		$jCounter++;
		$ExportArr[$iCounter][$jCounter] = "E8 (40-44)";
		$jCounter++;
		$ExportArr[$iCounter][$jCounter] = "F9 (below 39)";
		$jCounter++;
		$ExportArr[$iCounter][$jCounter] = "Pass Rate (A1-C6)";
		$jCounter++;
		$ExportArr[$iCounter][$jCounter] = "Avg. Marks";
		$jCounter++;
		
		$iCounter++;
		
		// Main table generaeting logic
		// Loop 1: Subjects
		foreach ($FormSubjectArr as $SubjectID => $SubjectName) {
			if ($FormSubjectGradingSchemeArr[$SubjectID]["scaleInput"] != "M")
				continue;
				
			$jCounter = 0;
			
			$ExportArr[$iCounter][$jCounter] = str_replace(" ()", "", $SubjectName[0]);
			$iCounter++;
			
			// Init variables
			$AbsNumber = array();
			$ExNumber = array();
			$BandNumber = array();
			$BandPercent = array();
			$NumberOfClassStudent = array();
			$ClassStudentTotalmark = array();
			$ClassStudentAverageMark =  array();
			
			$ClassLevelAbsNumber = 0;
			$ClassLevelExNumber = 0;
			$NumberOfClassLevelStudent = 0;
			$ClassLevelBandNumber = array();
			$ClassLevelBandPercent = array();
			$ClassLevelStudentTotalMark = 0;
			$ClassLevelStudentAverageMark = 0;
			
			// Loop 2: Classes
			for($i=0; $i<sizeof($ClassArr); $i++) {
				$thisClassID = $ClassArr[$i]["ClassID"];
				
				// Construct StudentIDList in the class
				$ClassStudentArr = $lreportcard->GET_STUDENT_BY_CLASS($thisClassID);
				$ClassStudentIDList = array();
				for($j=0; $j<sizeof($ClassStudentArr); $j++) {
					$ClassStudentIDList[] = $ClassStudentArr[$j]["UserID"];
				}
				$ClassStudentIDListQuery = implode("','", $ClassStudentIDList);
				$ClassStudentIDListQuery = "'".$ClassStudentIDListQuery."'";
				
				// Query the consolidated marks of the Class
				$table = $lreportcard->DBName.".RC_REPORT_RESULT_SCORE";
				$sql = "SELECT Mark, Grade FROM $table ";
				$sql .= "WHERE ReportID = '$ReportID' AND SubjectID = '$SubjectID' AND StudentID IN ($ClassStudentIDListQuery) AND ";
				
				if (isset($ReportColumnID) && $ReportColumnID != "") {
					$sql .= "ReportColumnID = '$ReportColumnID'";
				} else {
					$sql .= "(ReportColumnID = 0 OR ReportColumnID IS NULL)";
				}
				$ClassResult = $lreportcard->returnArray($sql, 2);
				
				$NumberOfClassStudent[$thisClassID] = sizeof($ClassResult);
				
				// Init variables
				$AbsNumber[$thisClassID] = 0;
				$ExNumber[$thisClassID] = 0;
				
				$BandNumber["A1"][$thisClassID] = 0;
				$BandNumber["A2"][$thisClassID] = 0;
				$BandNumber["B3"][$thisClassID] = 0;
				$BandNumber["B4"][$thisClassID] = 0;
				$BandNumber["C5"][$thisClassID] = 0;
				$BandNumber["C6"][$thisClassID] = 0;
				$BandNumber["D7"][$thisClassID] = 0;
				$BandNumber["E8"][$thisClassID] = 0;
				$BandNumber["F9"][$thisClassID] = 0;
				$BandNumber["A1-C6"][$thisClassID] = 0;
				
				$BandPercent["A1"][$thisClassID] = 0;
				$BandPercent["A2"][$thisClassID] = 0;
				$BandPercent["B3"][$thisClassID] = 0;
				$BandPercent["B4"][$thisClassID] = 0;
				$BandPercent["C5"][$thisClassID] = 0;
				$BandPercent["C6"][$thisClassID] = 0;
				$BandPercent["D7"][$thisClassID] = 0;
				$BandPercent["E8"][$thisClassID] = 0;
				$BandPercent["F9"][$thisClassID] = 0;
				$BandPercent["A1-C6"][$thisClassID] = 0;
				
				$ClassStudentTotalmark[$thisClassID] = 0;
				$ClassStudentAverageMark[$thisClassID] = 0;
				
				// Loop the marks and group into different categories
				for($j=0; $j<sizeof($ClassResult); $j++) {
					$StudentBand = "";
					if ($ClassResult[$j]["Mark"] != "" && $ClassResult[$j]["Grade"] == "") {
						$ClassStudentTotalmark[$thisClassID] += $ClassResult[$j]["Mark"];
						$StudentBand = returnGrandMSBand($ClassResult[$j]["Mark"]);
						if (!is_array($StudentBand))
							$BandNumber[$StudentBand][$thisClassID]++;
					} else if ($ClassResult[$j]["Grade"] == "-") {
						$AbsNumber[$thisClassID]++;
					} else if ($ClassResult[$j]["Grade"] == "/" || $ClassResult[$j]["Grade"] == "N.A.") {
						$ExNumber[$thisClassID]++;
					}
				}
				
				$TotalNumber = $NumberOfClassStudent[$thisClassID];
				$TotalAbsExNumber = $AbsNumber[$thisClassID] + $ExNumber[$thisClassID];
				$TotalExamNumber = $TotalNumber - $TotalAbsExNumber;
				
				$BandNumber["A1-C6"][$thisClassID] += $BandNumber["A1"][$thisClassID]+$BandNumber["A2"][$thisClassID];
				$BandNumber["A1-C6"][$thisClassID] += $BandNumber["B3"][$thisClassID]+$BandNumber["B4"][$thisClassID];
				$BandNumber["A1-C6"][$thisClassID] += $BandNumber["C5"][$thisClassID]+$BandNumber["C6"][$thisClassID];
				
				
				// Prevent divide by Zero
				if ($TotalExamNumber != 0) {
					$BandPercent["A1"][$thisClassID] = calculatePercent($BandNumber["A1"][$thisClassID], $TotalExamNumber);
					$BandPercent["A2"][$thisClassID] = calculatePercent($BandNumber["A2"][$thisClassID], $TotalExamNumber);
					$BandPercent["B3"][$thisClassID] = calculatePercent($BandNumber["B3"][$thisClassID], $TotalExamNumber);
					$BandPercent["B4"][$thisClassID] = calculatePercent($BandNumber["B4"][$thisClassID], $TotalExamNumber);
					$BandPercent["C5"][$thisClassID] = calculatePercent($BandNumber["C5"][$thisClassID], $TotalExamNumber);
					$BandPercent["C6"][$thisClassID] = calculatePercent($BandNumber["C6"][$thisClassID], $TotalExamNumber);
					$BandPercent["D7"][$thisClassID] = calculatePercent($BandNumber["D7"][$thisClassID], $TotalExamNumber);
					$BandPercent["E8"][$thisClassID] = calculatePercent($BandNumber["E8"][$thisClassID], $TotalExamNumber);
					$BandPercent["F9"][$thisClassID] = calculatePercent($BandNumber["F9"][$thisClassID], $TotalExamNumber);
					$BandPercent["A1-C6"][$thisClassID] = calculatePercent($BandNumber["A1-C6"][$thisClassID], $TotalExamNumber);
					
					$ClassStudentAverageMark[$thisClassID] = number_format(round(($ClassStudentTotalmark[$thisClassID]/$TotalExamNumber), 1), 1);
				}
				
				/*
				$CTeacher = array();
				$ClassTeacherNameArr = $lclass->returnClassTeacher($ClassArr[$i]["ClassName"]);
				foreach($ClassTeacherNameArr as $key=>$val) {
					$CTeacher[] = $val['CTeacher'];
				}
				$ClassTeacherName = !empty($CTeacher) ? implode(", ", $CTeacher) : "-";
				*/
				
				$SubjectTeacherArr = $lreportcard->returnSubjectTeacher($thisClassID, $SubjectID);
				$SubjectTeacherName = !empty($SubjectTeacherArr) ? implode(", ", $SubjectTeacherArr) : "-";
				
				$jCounter = 0;
				
				$ExportArr[$iCounter][$jCounter] = $ClassArr[$i]["ClassName"];
				$jCounter++;
				$ExportArr[$iCounter][$jCounter] = $SubjectTeacherName;
				$jCounter++;
				$ExportArr[$iCounter][$jCounter] = $TotalNumber;
				$jCounter++;
				$ExportArr[$iCounter][$jCounter] = $TotalAbsExNumber;
				$jCounter++;
				$ExportArr[$iCounter][$jCounter] = $TotalExamNumber;
				$jCounter++;
				$ExportArr[$iCounter][$jCounter] = $BandNumber["A1"][$thisClassID]." (".str_pad($BandPercent["A1"][$thisClassID], 5, " ", STR_PAD_LEFT)."%)";
				$jCounter++;
				$ExportArr[$iCounter][$jCounter] = $BandNumber["A2"][$thisClassID]." (".str_pad($BandPercent["A2"][$thisClassID], 5, " ", STR_PAD_LEFT)."%)";
				$jCounter++;
				$ExportArr[$iCounter][$jCounter] = $BandNumber["B3"][$thisClassID]." (".str_pad($BandPercent["B3"][$thisClassID], 5, " ", STR_PAD_LEFT)."%)";
				$jCounter++;
				$ExportArr[$iCounter][$jCounter] = $BandNumber["B4"][$thisClassID]." (".str_pad($BandPercent["B4"][$thisClassID], 5, " ", STR_PAD_LEFT)."%)";
				$jCounter++;
				$ExportArr[$iCounter][$jCounter] = $BandNumber["C5"][$thisClassID]." (".str_pad($BandPercent["C5"][$thisClassID], 5, " ", STR_PAD_LEFT)."%)";
				$jCounter++;
				$ExportArr[$iCounter][$jCounter] = $BandNumber["C6"][$thisClassID]." (".str_pad($BandPercent["C6"][$thisClassID], 5, " ", STR_PAD_LEFT)."%)";
				$jCounter++;
				$ExportArr[$iCounter][$jCounter] = $BandNumber["D7"][$thisClassID]." (".str_pad($BandPercent["D7"][$thisClassID], 5, " ", STR_PAD_LEFT)."%)";
				$jCounter++;
				$ExportArr[$iCounter][$jCounter] = $BandNumber["E8"][$thisClassID]." (".str_pad($BandPercent["E8"][$thisClassID], 5, " ", STR_PAD_LEFT)."%)";
				$jCounter++;
				$ExportArr[$iCounter][$jCounter] = $BandNumber["F9"][$thisClassID]." (".str_pad($BandPercent["F9"][$thisClassID], 5, " ", STR_PAD_LEFT)."%)";
				$jCounter++;
				$ExportArr[$iCounter][$jCounter] = $BandNumber["A1-C6"][$thisClassID]." (".str_pad($BandPercent["A1-C6"][$thisClassID], 5, " ", STR_PAD_LEFT)."%)";
				$jCounter++;
				$ExportArr[$iCounter][$jCounter] = $ClassStudentAverageMark[$thisClassID];
				$jCounter++;
				
				$iCounter++;
				
			} // End Loop 2: Classes
			// Display a row sum up stat of the whole class level
			$ClassLevelAbsNumber = array_sum($AbsNumber);
			$ClassLevelExNumber = array_sum($ExNumber);
			$NumberOfClassLevelStudent = array_sum($NumberOfClassStudent);
			
			$ClassLevelTotalExamNumber = $NumberOfClassLevelStudent - $ClassLevelAbsNumber - $ClassLevelExNumber;
			
			$ClassLevelBandNumber["A1"] = array_sum($BandNumber["A1"]);
			$ClassLevelBandNumber["A2"] = array_sum($BandNumber["A2"]);
			$ClassLevelBandNumber["B3"] = array_sum($BandNumber["B3"]);
			$ClassLevelBandNumber["B4"] = array_sum($BandNumber["B4"]);
			$ClassLevelBandNumber["C5"] = array_sum($BandNumber["C5"]);
			$ClassLevelBandNumber["C6"] = array_sum($BandNumber["C6"]);
			$ClassLevelBandNumber["D7"] = array_sum($BandNumber["D7"]);
			$ClassLevelBandNumber["E8"] = array_sum($BandNumber["E8"]);
			$ClassLevelBandNumber["F9"] = array_sum($BandNumber["F9"]);
			
			$ClassLevelBandNumber["A1-C6"] = $ClassLevelBandNumber["A1"]+$ClassLevelBandNumber["A2"]+$ClassLevelBandNumber["B3"]+$ClassLevelBandNumber["B4"]+$ClassLevelBandNumber["C5"]+$ClassLevelBandNumber["C6"];
			
			$ClassLevelStudentTotalMark = array_sum($ClassStudentTotalmark);
			
			// Prevent divide by Zero
			if ($ClassLevelTotalExamNumber != 0) {
				$ClassLevelBandPercent["A1"] = calculatePercent($ClassLevelBandNumber["A1"], $ClassLevelTotalExamNumber);
				$ClassLevelBandPercent["A2"] = calculatePercent($ClassLevelBandNumber["A2"], $ClassLevelTotalExamNumber);
				$ClassLevelBandPercent["B3"] = calculatePercent($ClassLevelBandNumber["B3"], $ClassLevelTotalExamNumber);
				$ClassLevelBandPercent["B4"] = calculatePercent($ClassLevelBandNumber["B4"], $ClassLevelTotalExamNumber);
				$ClassLevelBandPercent["C5"] = calculatePercent($ClassLevelBandNumber["C5"], $ClassLevelTotalExamNumber);
				$ClassLevelBandPercent["C6"] = calculatePercent($ClassLevelBandNumber["C6"], $ClassLevelTotalExamNumber);
				$ClassLevelBandPercent["D7"] = calculatePercent($ClassLevelBandNumber["D7"], $ClassLevelTotalExamNumber);
				$ClassLevelBandPercent["E8"] = calculatePercent($ClassLevelBandNumber["E8"], $ClassLevelTotalExamNumber);
				$ClassLevelBandPercent["F9"] = calculatePercent($ClassLevelBandNumber["F9"], $ClassLevelTotalExamNumber);
				
				$ClassLevelBandPercent["A1-C6"] = calculatePercent($ClassLevelBandNumber["A1-C6"], $ClassLevelTotalExamNumber);
				
				$ClassLevelStudentAverageMark = number_format(round(($ClassLevelStudentTotalMark/$ClassLevelTotalExamNumber) ,1), 1);
			}
			
			$jCounter = 0;
			
			$ExportArr[$iCounter][$jCounter] = "";
			$jCounter++;
			$ExportArr[$iCounter][$jCounter] = "Level Average";
			$jCounter++;
			$ExportArr[$iCounter][$jCounter] = $NumberOfClassLevelStudent;
			$jCounter++;
			$ExportArr[$iCounter][$jCounter] = "";
			$jCounter++;
			$ExportArr[$iCounter][$jCounter] = $ClassLevelTotalExamNumber;
			$jCounter++;
			$ExportArr[$iCounter][$jCounter] = $ClassLevelBandNumber["A1"]." (".str_pad($ClassLevelBandPercent["A1"], 5, " ", STR_PAD_LEFT)."%)";
			$jCounter++;
			$ExportArr[$iCounter][$jCounter] = $ClassLevelBandNumber["A2"]." (".str_pad($ClassLevelBandPercent["A2"], 5, " ", STR_PAD_LEFT)."%)";
			$jCounter++;
			$ExportArr[$iCounter][$jCounter] = $ClassLevelBandNumber["B3"]." (".str_pad($ClassLevelBandPercent["B3"], 5, " ", STR_PAD_LEFT)."%)";
			$jCounter++;
			$ExportArr[$iCounter][$jCounter] = $ClassLevelBandNumber["B4"]." (".str_pad($ClassLevelBandPercent["B4"], 5, " ", STR_PAD_LEFT)."%)";
			$jCounter++;
			$ExportArr[$iCounter][$jCounter] = $ClassLevelBandNumber["C5"]." (".str_pad($ClassLevelBandPercent["C5"], 5, " ", STR_PAD_LEFT)."%)";
			$jCounter++;
			$ExportArr[$iCounter][$jCounter] = $ClassLevelBandNumber["C6"]." (".str_pad($ClassLevelBandPercent["C6"], 5, " ", STR_PAD_LEFT)."%)";
			$jCounter++;
			$ExportArr[$iCounter][$jCounter] = $ClassLevelBandNumber["D7"]." (".str_pad($ClassLevelBandPercent["D7"], 5, " ", STR_PAD_LEFT)."%)";
			$jCounter++;
			$ExportArr[$iCounter][$jCounter] = $ClassLevelBandNumber["E8"]." (".str_pad($ClassLevelBandPercent["E8"], 5, " ", STR_PAD_LEFT)."%)";
			$jCounter++;
			$ExportArr[$iCounter][$jCounter] = $ClassLevelBandNumber["F9"]." (".str_pad($ClassLevelBandPercent["F9"], 5, " ", STR_PAD_LEFT)."%)";
			$jCounter++;
			$ExportArr[$iCounter][$jCounter] = $ClassLevelBandNumber["A1-C6"]." (".str_pad($ClassLevelBandPercent["A1-C6"], 5, " ", STR_PAD_LEFT)."%)";
			$jCounter++;
			$ExportArr[$iCounter][$jCounter] = $ClassLevelStudentAverageMark;
			$jCounter++;
			
			$iCounter++;
			
		} // End Loop 1: Subjects
		
		$jCounter = 0;
		$ExportArr[$iCounter][$jCounter] = $LastGenerated;
		
		$export_content = $lexport->GET_EXPORT_TXT($ExportArr, $exportColumn, "", "\r\n", "", 0, "11");
		intranet_closedb();
		
		// Output the file to user browser
		$lexport->EXPORT_FILE($filename, $export_content);
		
  } else {
?>
You have no priviledge to access this page.
<?
	}
} else {
?>
You have no priviledge to access this page.
<?
}
?>
