<?php

$PATH_WRT_ROOT = "../../../../../";
$PageRight = "ADMIN";
 ini_set('display_errors',1);
	error_reporting(E_ALL ^ E_NOTICE);	
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008w.php");

intranet_auth();
intranet_opendb();

$ClassLevelID	= $_POST['ClassLevelID'];
$ReportID	= $_POST['ReportID'];
$ReportColumnID	= $_POST['ReportColumnID'];

$SecReportType	= $_POST['SecReportType'];

if(!$ClassLevelID || !$ReportID) {
	intranet_closedb();
	header("Location: index.php");
}

# check Grand MS is Primary / Secondary
$lreportcard = new libreportcard2008w();
$ClassLevelName = $lreportcard->returnClassLevel($ClassLevelID);
$MS_Type = strtolower(substr($ClassLevelName, 0, 1));
$MS_Level = substr($ClassLevelName, 1, 1);

$summary = "";
if($MS_Type == "p") {
	if ($MS_Level < 5)
		$summary = "pri1";
	else
		$summary = "pri2";
} else if($MS_Type == "s") {
	$summary = "sec";
}

$parms = "ReportID=".$ReportID."&ClassLevelID=".$ClassLevelID."&ReportColumnID=".$ReportColumnID."&SecReportType=".$SecReportType;

if ($SubjectType == "exam")
{
	if ($ViewFormat == "html")
	{
		$summary_path = "summary_".$summary.".php?".$parms;
	}
	else
	{
		$summary_path = "export_summary_".$summary.".php?".$parms;
	}
}
else if ($SubjectType == "nonExam")
{
	if ($ViewFormat == "html")
	{
		$summary_path = "summary_nonexam.php?".$parms;
	}
	else
	{
		$summary_path = "export_summary_nonexam.php?".$parms;
	}
}
else if ($SubjectType == "subjectComponents")
{
	if ($ViewFormat == "html")
	{
		$summary_path = "summary_subject_components.php?".$parms;
	}
	else
	{
		$summary_path = "export_summary_subject_components.php?".$parms;
	}
}
else
{
	$summary_path = "index.php";
}

if($summary)
	header("Location: $summary_path");
else
	header("Location: index.php");

intranet_closedb();

?>