<?php
######################################################
# Generate Statistic Report for Non-Exam Subjects
######################################################

function calculatePercent($Numerator, $Denumerator) {
	return number_format(round(($Numerator/$Denumerator*100) ,1) ,1);
}
// Root path
$PATH_WRT_ROOT = "../../../../../";
// Page access right
$PageRight = "ADMIN";

// Include general libraries
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
#include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

// Authorize intranet user & connect Database
intranet_auth();
intranet_opendb();

// Check if ReportCard module is enable
if ($plugin['ReportCard2008']) {
	// Include ReportCard libraries
	include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
	include_once($PATH_WRT_ROOT."includes/libreportcard2008w.php");
	
	// Create objects
	if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName == "sis") 
	{
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
		$lreportcard = new libreportcardcustom();
	} else {
		$lreportcard = new libreportcard2008w();
	}
	$linterface = new interface_html();
	$lclass = new libclass();
	
	// Check module access right
	if ($lreportcard->hasAccessRight()) {
		// Check required data submitted from index.php
		if (!isset($ClassLevelID, $ReportID)) {
			intranet_closedb();
			header("Location: index.php");
			exit();
		}
		
		$ClassLevelName = $lreportcard->returnClassLevel($ClassLevelID);
		$MS_Type = strtolower(substr($ClassLevelName, 0, 1));
		$MS_Level = substr($ClassLevelName, 1, 1);
		
		$summary = "";
		if($MS_Type == "p") {
			$school = "Primary";
		} else if($MS_Type == "s") {
			$school = "Secondary";
		}
		
		// Get ReportCard info
		$reportTemplateInfo = $lreportcard->returnReportTemplateBasicInfo($ReportID);
		
		// Check to make sure it is a whole year & generated report
		if ($reportTemplateInfo["LastGenerated"] == "0000-00-00 00:00:00" || $reportTemplateInfo["LastGenerated"] == "") {
			intranet_closedb();
			header("Location: index.php");
			exit();
		}
		
		$LastGenerated = $lreportcard->GET_CURRENT_DATETIME();
		
		// Get subjects of the current ClassLevel
		$FormSubjectArr = $lreportcard->returnSubjectwOrder($ClassLevelID, 1);
		
		// Get subjects' grading scheme of the current ClassLevel
		$FormSubjectGradingSchemeArr = $lreportcard->GET_FROM_SUBJECT_GRADING_SCHEME($ClassLevelID);
		
		// Get subjects' full mark of the current ClassLevel
		$FormSubjectFullMarkArr = $lreportcard->returnSubjectFullMark_MarkOnly($ClassLevelID);
		
		// Get ClassLevel name
		$ClassLevelName = $lreportcard->returnClassLevel($ClassLevelID);
		
		// Get Classes
		$ClassArr = $lreportcard->GET_CLASSES_BY_FORM($ClassLevelID);
		
		// Get current school year
		$SchoolYear = $lreportcard->schoolYear;
		
		// Get columns info of the current ReportCard
		$ColumnTitleAry = $lreportcard->returnReportColoumnTitle($ReportID);
		$ColumnIDList = array_keys($ColumnTitleAry);
		
		// Check if ReportColumnID have value
		// 	if yes, only term mark is used
		// 	if no, overall marks of all terms are used
		if (isset($ReportColumnID) && $ReportColumnID != "") {
			// Hardcoded Column Name since names in $ColumnTitleAry are not exactly "SA1" & "SA2"
			if ($ReportColumnID == $ColumnIDList[0])
			{
				$DisplayColumnName = "SA1";
			}
			else if ($ReportColumnID == $ColumnIDList[1])
			{
				$DisplayColumnName = "SA2";
			}
			else if ($ReportColumnID == "first")
			{
				$DisplayColumnName = "First Combined";
			}
			
			$StatReportTitle = "Analysis Report of ".$DisplayColumnName." for Subject Components (".$school.")";
		} else {
			$StatReportTitle = "Analysis Report for Subject Components (".$school.")";
		}
		
		$SubjectArray 		= $lreportcard->returnSubjectwOrderNoL($ClassLevelID);
		$MainSubjectArray 	= $lreportcard->returnSubjectwOrder($ClassLevelID);
		if(sizeof($MainSubjectArray) > 0)
		foreach($MainSubjectArray as $MainSubjectIDArray[] => $MainSubjectNameArray[]);
		
		$display = "";
		
		// Find the maximun component number
		$maxCompNum = 0;
		$parentSubjectTitle = array();
		foreach ($FormSubjectArr as $SubjectID => $SubjectName) {
			if ($FormSubjectGradingSchemeArr[$SubjectID]["scaleInput"] != "M")
				continue;
				
			// check if it is a parent subject, if yes find info of its components subjects
			$isSub = 0;
			if(in_array($SubjectID, $MainSubjectIDArray)!=true)	$isSub=1;

			$CmpSubjectArr = array();
			$isParentSubject = 0;		// set to "1" when one ScaleInput of component subjects is Mark(M)
			if (!$isSub) {
				$CmpSubjectArr = $lreportcard->GET_COMPONENT_SUBJECT($SubjectID, $ClassLevelID);
				if(!empty($CmpSubjectArr)) 
					$isParentSubject = 1;
					
				if (!$isParentSubject)
				{
					continue;
				}
				
				$thisNumOfCompSubjects = sizeof($CmpSubjectArr);
				if ($thisNumOfCompSubjects > $maxCompNum)
				{
					$maxCompNum = $thisNumOfCompSubjects;
				}
			}
		}
		
		$parentSubjectTitle = array();
		foreach ($FormSubjectArr as $SubjectID => $SubjectName) {
			if ($FormSubjectGradingSchemeArr[$SubjectID]["scaleInput"] != "M")
				continue;
				
			// check if it is a parent subject, if yes find info of its components subjects
			$isSub = 0;
			if(in_array($SubjectID, $MainSubjectIDArray)!=true)	$isSub=1;

			$CmpSubjectArr = array();
			$isParentSubject = 0;		// set to "1" when one ScaleInput of component subjects is Mark(M)
			if (!$isSub) {
				$CmpSubjectArr = $lreportcard->GET_COMPONENT_SUBJECT($SubjectID, $ClassLevelID);
				if(!empty($CmpSubjectArr)) 
					$isParentSubject = 1;
					
				if (!$isParentSubject)
				{
					continue;
				}
				
				$display .= "<tr><td class='SubjectName'>". convert2unicode(str_replace(" ()", "", $SubjectName[0]), 1, 2) ."</td><td colspan='13'></td></tr>";
			
				// Init variables
				$AbsNumber = array();
				$ExNumber = array();
				$NumberOfClassStudent = array();
				$ClassStudentAverageMark =  array();
				
				$ClassLevelAbsNumber = 0;
				$ClassLevelExNumber = 0;
				$NumberOfClassLevelStudent = 0;
				$ClassLevelBandNumber = array();
				$ClassLevelBandPercent = array();
				$ClassLevelStudentAverageMark = array();
				
				// Loop 2: Classes
				# counting of Abs/Ex first
				# then calculate the average mark for each components
				for($i=0; $i<sizeof($ClassArr); $i++) {
					$thisClassID = $ClassArr[$i]["ClassID"];
					
					// Construct StudentIDList in the class
					$ClassStudentArr = $lreportcard->GET_STUDENT_BY_CLASS($thisClassID);
					
					$ClassStudentIDList = array();
					for($j=0; $j<sizeof($ClassStudentArr); $j++) {
						$ClassStudentIDList[] = $ClassStudentArr[$j]["UserID"];
					}
					$ClassStudentIDListQuery = implode("','", $ClassStudentIDList);
					$ClassStudentIDListQuery = "'".$ClassStudentIDListQuery."'";
					
					// Query the consolidated marks of the Class
					$table = $lreportcard->DBName.".RC_REPORT_RESULT_SCORE";
					$sql = "SELECT Mark, Grade FROM $table ";
					$sql .= "WHERE ReportID = '$ReportID' AND SubjectID = '$SubjectID' AND StudentID IN ($ClassStudentIDListQuery) AND ";
					if (isset($ReportColumnID) && $ReportColumnID != "") {
						$sql .= "ReportColumnID = '$ReportColumnID'";
					} else {
						$sql .= "(ReportColumnID = 0 OR ReportColumnID IS NULL)";
					}
					$ClassResult = $lreportcard->returnArray($sql, 2);
					
					$NumberOfClassStudent[$thisClassID] = sizeof($ClassResult);
					
					# counting of Abs/Ex
					// Init variables
					$AbsNumber[$thisClassID] = 0;
					$ExNumber[$thisClassID] = 0;
					$ClassAbsNumber = 0;
					$ClassExNumber = 0;
					
					// Loop the marks and group into different categories
					for($j=0; $j<sizeof($ClassResult); $j++) {
						$StudentBand = "";
						if ($ClassResult[$j]["Grade"] == "-") {
							$AbsNumber[$thisClassID]++;
						} else if ($ClassResult[$j]["Grade"] == "/" || $ClassResult[$j]["Grade"] == "N.A.") {
							$ExNumber[$thisClassID]++;
						}
					}
					
					$TotalNumber = $NumberOfClassStudent[$thisClassID];
					$AbsExNumber = $AbsNumber[$thisClassID]+$ExNumber[$thisClassID];
					$TotalExamNumber = $TotalNumber - $AbsExNumber;
					
					// for classlevel calculation
					$AbsExNumberArr[$thisClassID] = $AbsExNumber;
					
					$SubjectTeacherArr = $lreportcard->returnSubjectTeacher($thisClassID, $SubjectID,$ReportID,$ClassStudentIDList[0]);
					$SubjectTeacherName = !empty($SubjectTeacherArr) ? implode(", ", $SubjectTeacherArr) : "-";
					
					$display .= "<tr>
									<td class='tablerow_separator'>".$ClassArr[$i]["ClassName"]."</td>
									<td class='tablerow_separator'>".$SubjectTeacherName."</td>
									<td align='center' class='tablerow_separator'>".$TotalNumber."</td>
									<td align='center' class='tablerow_separator'>".$AbsExNumber."</td>
									<td align='center' class='tablerow_separator'>".$TotalExamNumber."</td>
								";
					
					# loop each subject components to get the average score
					$AbsNumber[$thisClassID] = array();
					$ExNumber[$thisClassID] = array();
					for ($j=0; $j<sizeof($CmpSubjectArr); $j++)
					{
						$thisCmpSubjectID = $CmpSubjectArr[$j]['SubjectID'];
						$thisCmpSubjectName = $CmpSubjectArr[$j]['SubjectName'];
						
						$AbsNumber[$thisClassID][$thisCmpSubjectID] = 0;
						$ExNumber[$thisClassID][$thisCmpSubjectID] = 0;
						
						# Component Subject Full Mark
						$thisCmpFullMark = $FormSubjectFullMarkArr[$thisCmpSubjectID];
						
						# get all marks of the student first
						# then calculate the average
						// Query the consolidated marks of the Class
						$table = $lreportcard->DBName.".RC_REPORT_RESULT_SCORE";
						$sql = "SELECT Mark, Grade FROM $table ";
						$sql .= "WHERE ReportID = '$ReportID' AND SubjectID = '$thisCmpSubjectID' AND StudentID IN ($ClassStudentIDListQuery) AND ";
						if (isset($ReportColumnID) && $ReportColumnID != "") {
							$sql .= "ReportColumnID = '$ReportColumnID'";
						} else {
							$sql .= "(ReportColumnID = 0 OR ReportColumnID IS NULL)";
						}
						$ClassResult = $lreportcard->returnArray($sql, 2);
						
						// Loop the marks and group into different categories
						for($k=0; $k<sizeof($ClassResult); $k++) {
							$StudentBand = "";
							if ($ClassResult[$k]["Mark"] != "" && $ClassResult[$k]["Grade"] == "") {
								$ClassStudentTotalmark[$thisClassID][$thisCmpSubjectID] += $ClassResult[$k]["Mark"];
								if (!is_array($StudentBand))
									$BandNumber[$StudentBand][$thisClassID]++;
							} else if ($ClassResult[$k]["Grade"] == "-") {
								$AbsNumber[$thisClassID][$thisCmpSubjectID]++;
							} else if ($ClassResult[$k]["Grade"] == "/" || $ClassResult[$k]["Grade"] == "N.A.") {
								$ExNumber[$thisClassID][$thisCmpSubjectID]++;
							}
						}
						
						$thisTotalNumber = $NumberOfClassStudent[$thisClassID];
						$thisAbsExNumber = $AbsNumber[$thisClassID][$thisCmpSubjectID] + $ExNumber[$thisClassID][$thisCmpSubjectID];
						$thisTotalExamNumber = $thisTotalNumber - $thisAbsExNumber;
						
						// Prevent divide by Zero
						if ($thisTotalExamNumber != 0) 
						{
							//$ClassStudentAverageMark[$thisClassID][$thisCmpSubjectID] = round(($ClassStudentTotalmark[$thisClassID][$thisCmpSubjectID]/($NumberOfClassStudent[$thisClassID]-$thisTotalAbsExNumber)), 1);
							$ClassStudentAverageMark[$thisClassID][$thisCmpSubjectID] = round(($ClassStudentTotalmark[$thisClassID][$thisCmpSubjectID]/$thisTotalExamNumber), 1);
						}
						// Convert to Percentage
						if ($thisCmpFullMark!=0 && is_numeric($thisCmpFullMark))
							$ClassStudentAverageMark[$thisClassID][$thisCmpSubjectID] = round(($ClassStudentAverageMark[$thisClassID][$thisCmpSubjectID]/$thisCmpFullMark)*100,1);
						else
							$ClassStudentAverageMark[$thisClassID][$thisCmpSubjectID] = 0;
							
						$display .= "<td align='center' class='tablerow_separator'>".($ClassStudentAverageMark[$thisClassID][$thisCmpSubjectID])."</td>";
						
						$ClassLevelStudentAverageMark[$thisCmpSubjectID] += $ClassStudentAverageMark[$thisClassID][$thisCmpSubjectID] / sizeof($ClassArr);
						
					}
					
					// add empty cell
					$thisNumOfCompSubjects = sizeof($CmpSubjectArr);
					$numEmptyCell = $maxCompNum - $thisNumOfCompSubjects;
					for ($a=0; $a<$numEmptyCell; $a++)
					{
						$display .= "<td align='center' class='tablerow_separator'>&nbsp;</td>";
					}
					$display .= "</tr>";
					
				}	// End of Loop Class
				
				# ClassLevel Statistics
				$ClassLevelAbsExNumber = array_sum($AbsExNumberArr);
				$NumberOfClassLevelStudent = array_sum($NumberOfClassStudent);
				$ClassLevelTotalExamNumber = $NumberOfClassLevelStudent - $ClassLevelAbsExNumber;
				
				$display .= "<tr>
								<td class='tablerow_separator'>&nbsp;</td>
								<td class='tablerow_separator'>Level Average Score</td>
								<td align='center' class='tablerow_separator'>".$NumberOfClassLevelStudent."</td>
								<td align='center' class='tablerow_separator'>&nbsp;</td>
								<td align='center' class='tablerow_separator'>".$ClassLevelTotalExamNumber."</td>";
				// classlevel average marks
				for ($j=0; $j<sizeof($CmpSubjectArr); $j++)
				{
					$thisCmpSubjectID = $CmpSubjectArr[$j]['SubjectID'];
					$thisAverage = round($ClassLevelStudentAverageMark[$thisCmpSubjectID], 1);
					$display .= "<td align='center' class='tablerow_separator'>".$thisAverage."</td>";
				}
				// add empty cell
				$thisNumOfCompSubjects = sizeof($CmpSubjectArr);
				$numEmptyCell = $maxCompNum - $thisNumOfCompSubjects;
				for ($a=0; $a<$numEmptyCell; $a++)
				{
					$display .= "<td align='center' class='tablerow_separator'>&nbsp;</td>";
				}
								
				$display .= "</tr>
							<tr><td colspan='14'>&nbsp;</td></tr>";
				
				
				
			}	# End if (!isSub)
		}	# End for each subject
		
		############## Interface Start ##############
		
?>
<style type="text/css">
<!--
h1 {
	font-size: 24px;
	font-family: "Times CY", "Times New Roman", serif;
	font-style: italic;
	margin: 0px;
	padding-top: 3px;
	padding-bottom: 3px;
	border-bottom: 5px solid #DDD;
}

h2 {
	font-size: 16px;
	font-family: "Times CY", "Times New Roman", serif;
	margin: 0px;
	padding-top: 2px;
	padding-bottom: 2px;
}

#StatReportWrapper {
	min-height: 800px;
	height: auto;
	_height: 800px;
	border-top: 5px solid #DDD;
	border-bottom: 5px solid #DDD;
}

#StatTable thead {
	font-size: 15px;
	font-family: "Times CY", "Times New Roman", serif;
	font-style: italic;
}

#StatTable tbody {
	font-size: 12px;
	font-family: arial, "lucida console", sans-serif;
}

.SubjectName {
	font-size: 12px;
	font-weight: bold;
	font-family: arial, "lucida console", sans-serif;
	padding-bottom: 5px;
}

.tablerow_separator {
	BORDER-BOTTOM: #DCDCDC 1px solid
}

.GMS_text14bi {
	font-family: "Times New Roman", "Arial", "Lucida Console";	
	font-weight: bold;
	font-size: 14px;
	font-style: italic;
}

.tableline {
	border-bottom-width: 2px;
	border-bottom-style: solid;
	border-bottom-color: #000000;
}
-->
</style>
<script type="text/JavaScript" language="JavaScript">

</script>
<div id="StatReportWrapper" style="width:100%;">
	<h1><?=$StatReportTitle?></h1>
	<h2>
		School Year&nbsp;&nbsp;<?= $SchoolYear ?>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		Level:&nbsp;<?= $ClassLevelName ?>
	</h2>
	<table id="StatTable" width="100%" cellpadding="0" cellspacing="0" border="0">
		<thead>
			<tr>
				<th>&nbsp;</th>
				<th align='left'>Subject Teacher</th>
				<th>Total</th>
				<th>Abs/Ex</th>
				<th>Total Exam</th>
				<? for ($i=1; $i<=$maxCompNum; $i++) { ?>
					<th><u>COMP<?=$i?></u></th>
				<? } ?>
			</tr>
			<tr>
				<th>&nbsp;</th>
				<th>&nbsp;</th>
				<th>Number</th>
				<th>Number</th>
				<th>Number</th>
				<? for ($i=1; $i<=$maxCompNum; $i++) { ?>
					<th>Average Score %</th>
				<? } ?>
			</tr>
		</thead>
		<tbody>
			<?= $display ?>
			<tr><td height="1" class="tabletext tableline" colspan='16'><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
	    	<tr><td class="GMS_text14bi" colspan='16'>&nbsp;<?=$LastGenerated?></td></tr>
		</tbody>
	</table>
</div>
<?
  } else {
?>
You have no priviledge to access this page.
<?
	}
} else {
?>
You have no priviledge to access this page.
<?
}
?>
