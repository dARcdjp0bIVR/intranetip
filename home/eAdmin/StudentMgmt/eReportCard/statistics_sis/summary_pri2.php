<?php
######################################################
# Generate Statistic Report for Primary Class Level 5-6
# 	Trigger from: index.php
# 	Required variables: $ClassLevelID, $ReportID
# 	Optional variables: $ReportColumnID
#
# 	Last updated: Andy Chan on 2008/6/3
######################################################

function returnGrandMSBand($Marks='-1') {
	if($Marks == -1)	return array("A*","A","B","C","D","E","U");
	if($Marks >= 91)	return "A*";
	if($Marks >= 75)	return "A";
	if($Marks >= 60)	return "B";
	if($Marks >= 50)	return "C";
	if($Marks >= 35)	return "D";
	if($Marks >= 20)	return "E";
	return "U";
}

function calculatePercent($Numerator, $Denumerator) {
	return number_format(round(($Numerator/$Denumerator*100) ,1) ,1);
}

// Root path
$PATH_WRT_ROOT = "../../../../../";

// Page access right
$PageRight = "ADMIN";

// Include general libraries
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
#include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

// Authorize intranet user & connect Database
intranet_auth();
intranet_opendb();

// Check if ReportCard module is enable
if ($plugin['ReportCard2008']) {
	// Include ReportCard libraries
	include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
	include_once($PATH_WRT_ROOT."includes/libreportcard2008w.php");
	
	// Create objects
	if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName == "sis") 
	{
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
		$lreportcard = new libreportcardcustom();
	} else {
		$lreportcard = new libreportcard2008w();
	}
	$linterface = new interface_html();
	$lclass = new libclass();
	
	// Check module access right
	if ($lreportcard->hasAccessRight()) {
		// Check required data submitted from index.php
		if (!isset($ClassLevelID, $ReportID)) {
			intranet_closedb();
			header("Location: index.php");
			exit();
		}
		
		// Get ReportCard info
		$reportTemplateInfo = $lreportcard->returnReportTemplateBasicInfo($ReportID);
		
		// Check to make sure it is a whole year & generated report
		if ($reportTemplateInfo["LastGenerated"] == "0000-00-00 00:00:00" || $reportTemplateInfo["LastGenerated"] == "") {
			intranet_closedb();
			header("Location: index.php");
			exit();
		}
		
		$LastGenerated = $lreportcard->GET_CURRENT_DATETIME();
		
		// Get subjects of the current ClassLevel
		$FormSubjectArr = $lreportcard->returnSubjectwOrder($ClassLevelID, 1);
		
		// Get subjects' grading scheme of the current ClassLevel
		$FormSubjectGradingSchemeArr = $lreportcard->GET_FROM_SUBJECT_GRADING_SCHEME($ClassLevelID);
		
		// Get ClassLevel name
		$ClassLevelName = $lreportcard->returnClassLevel($ClassLevelID);
		
		// Get Classes
		$ClassArr = $lreportcard->GET_CLASSES_BY_FORM($ClassLevelID);
		
		// Get current school year
		$SchoolYear = $lreportcard->schoolYear;
		
		// Get columns info of the current ReportCard
		$ColumnTitleAry = $lreportcard->returnReportColoumnTitle($ReportID);
		$ColumnIDList = array_keys($ColumnTitleAry);
		
		// Check if ReportColumnID have value
		// 	if yes, only term mark is used
		// 	if no, overall marks of all terms are used
		if (isset($ReportColumnID) && $ReportColumnID != "") {
			// Hardcoded Column Name since names in $ColumnTitleAry are not exactly "SA1" & "SA2"
			if ($ReportColumnID == $ColumnIDList[0])	$DisplayColumnName = "SA1";
			if ($ReportColumnID == $ColumnIDList[1])	$DisplayColumnName = "SA2";
			
			$StatReportTitle = "Summary of ".$DisplayColumnName." Results by Subject (Primary)";
		} else {
			$StatReportTitle = "Summary of Overall Results by Subject (Primary)";
		}
		
		$display = "";
		
		// Main table generaeting logic
		// Loop 1: Subjects
		foreach ($FormSubjectArr as $SubjectID => $SubjectName) {
			if ($FormSubjectGradingSchemeArr[$SubjectID]["scaleInput"] != "M")
				continue;
			
			$display .= "<tr><td class='SubjectName'>". convert2unicode(str_replace(" ()", "", $SubjectName[0]), 1, 2) ."</td><td colspan='20'></td></tr>";
			
			// Init variables
			$AbsNumber = array();
			$ExNumber = array();
			$BandNumber = array();
			$BandPercent = array();
			$NumberOfClassStudent = array();
			$ClassStudentTotalmark = array();
			$ClassStudentAverageMark =  array();
			
			$ClassLevelAbsNumber = 0;
			$ClassLevelExNumber = 0;
			$NumberOfClassLevelStudent = 0;
			$ClassLevelBandNumber = array();
			$ClassLevelBandPercent = array();
			$ClassLevelStudentTotalMark = 0;
			$ClassLevelStudentAverageMark = 0;
			
			// Loop 2: Classes
			for($i=0; $i<sizeof($ClassArr); $i++) {
				$thisClassID = $ClassArr[$i]["ClassID"];
				
				// Construct StudentIDList in the class
				$ClassStudentArr = $lreportcard->GET_STUDENT_BY_CLASS($thisClassID);
				$ClassStudentIDList = array();
				for($j=0; $j<sizeof($ClassStudentArr); $j++) {
					$ClassStudentIDList[] = $ClassStudentArr[$j]["UserID"];
				}
				$ClassStudentIDListQuery = implode("','", $ClassStudentIDList);
				$ClassStudentIDListQuery = "'".$ClassStudentIDListQuery."'";
				
				// Query the consolidated marks of the Class
				$table = $lreportcard->DBName.".RC_REPORT_RESULT_SCORE";
				$sql = "SELECT Mark, Grade FROM $table ";
				$sql .= "WHERE ReportID = '$ReportID' AND SubjectID = '$SubjectID' AND StudentID IN ($ClassStudentIDListQuery) AND ";
				if (isset($ReportColumnID) && $ReportColumnID != "") {
					$sql .= "ReportColumnID = '$ReportColumnID'";
				} else {
					$sql .= "(ReportColumnID = 0 OR ReportColumnID IS NULL)";
				}
				$ClassResult = $lreportcard->returnArray($sql, 2);
				
				$NumberOfClassStudent[$thisClassID] = sizeof($ClassResult);
				
				// Init variables
				$AbsNumber[$thisClassID] = 0;
				$ExNumber[$thisClassID] = 0;
				
				$BandNumber["A*"][$thisClassID] = 0;
				$BandNumber["A"][$thisClassID] = 0;
				$BandNumber["B"][$thisClassID] = 0;
				$BandNumber["C"][$thisClassID] = 0;
				$BandNumber["D"][$thisClassID] = 0;
				$BandNumber["E"][$thisClassID] = 0;
				$BandNumber["U"][$thisClassID] = 0;
				$BandNumber["EU"][$thisClassID] = 0;
				$BandNumber["A*-A"][$thisClassID] = 0;
				$BandNumber["A*-C"][$thisClassID] = 0;
				
				$BandPercent["A*"][$thisClassID] = 0;
				$BandPercent["A"][$thisClassID] = 0;
				$BandPercent["B"][$thisClassID] = 0;
				$BandPercent["C"][$thisClassID] = 0;
				$BandPercent["D"][$thisClassID] = 0;
				$BandPercent["E"][$thisClassID] = 0;
				$BandPercent["U"][$thisClassID] = 0;
				$BandPercent["EU"][$thisClassID] = 0;
				$BandPercent["A*-A"][$thisClassID] = 0;
				$BandPercent["A*-C"][$thisClassID] = 0;
				
				$ClassStudentTotalmark[$thisClassID] = 0;
				$ClassStudentAverageMark[$thisClassID] = 0;
				
				// Loop the marks and group into different categories
				for($j=0; $j<sizeof($ClassResult); $j++) {
					$StudentBand = "";
					if ($ClassResult[$j]["Mark"] != "" && $ClassResult[$j]["Grade"] == "") {
						$ClassStudentTotalmark[$thisClassID] += $ClassResult[$j]["Mark"];
						$StudentBand = returnGrandMSBand($ClassResult[$j]["Mark"]);
						if (!is_array($StudentBand))
							$BandNumber[$StudentBand][$thisClassID]++;
					} else if ($ClassResult[$j]["Grade"] == "-") {
						$AbsNumber[$thisClassID]++;
					} else if ($ClassResult[$j]["Grade"] == "/" || $ClassResult[$j]["Grade"] == "N.A.") {
						$ExNumber[$thisClassID]++;
					}
				}
				
				$TotalNumber = $NumberOfClassStudent[$thisClassID];
				$AbsExNumber = $AbsNumber[$thisClassID]+$ExNumber[$thisClassID];
				$TotalExamNumber = $TotalNumber - $AbsExNumber;
				
				$BandNumber["EU"][$thisClassID] = $BandNumber["E"][$thisClassID]+$BandNumber["U"][$thisClassID];
				$BandNumber["A*-A"][$thisClassID] = $BandNumber["A*"][$thisClassID]+$BandNumber["A"][$thisClassID];
				$BandNumber["A*-C"][$thisClassID] = $BandNumber["A*"][$thisClassID]+$BandNumber["A"][$thisClassID]+$BandNumber["B"][$thisClassID]+$BandNumber["C"][$thisClassID];
				
				// Prevent divide by Zero
				if ($TotalExamNumber != 0) {
					$BandPercent["A*"][$thisClassID] = calculatePercent($BandNumber["A*"][$thisClassID], $TotalExamNumber);
					$BandPercent["A"][$thisClassID] = calculatePercent($BandNumber["A"][$thisClassID], $TotalExamNumber);
					$BandPercent["B"][$thisClassID] = calculatePercent($BandNumber["B"][$thisClassID], $TotalExamNumber);
					$BandPercent["C"][$thisClassID] = calculatePercent($BandNumber["C"][$thisClassID], $TotalExamNumber);
					$BandPercent["D"][$thisClassID] = calculatePercent($BandNumber["D"][$thisClassID], $TotalExamNumber);
					$BandPercent["E"][$thisClassID] = calculatePercent($BandNumber["E"][$thisClassID], $TotalExamNumber);
					$BandPercent["U"][$thisClassID] = calculatePercent($BandNumber["U"][$thisClassID], $TotalExamNumber);
					
					$BandPercent["EU"][$thisClassID] = calculatePercent($BandNumber["EU"][$thisClassID], $TotalExamNumber);
					
					$BandPercent["A*-A"][$thisClassID] = calculatePercent($BandNumber["A*-A"][$thisClassID], $TotalExamNumber);
					$BandPercent["A*-C"][$thisClassID] = calculatePercent($BandNumber["A*-C"][$thisClassID], $TotalExamNumber);
					
					$ClassStudentAverageMark[$thisClassID] = round(($ClassStudentTotalmark[$thisClassID]/$TotalExamNumber), 1);
				}
				
				# update on 11 Dec by Ivan - show subject teacher instead of class teacher
				/*
				$CTeacher = array();
				$ClassTeacherNameArr = $lclass->returnClassTeacher($ClassArr[$i]["ClassName"]);
				foreach($ClassTeacherNameArr as $key=>$val) {
					$CTeacher[] = $val['CTeacher'];
				}
				$ClassTeacherName = !empty($CTeacher) ? implode(", ", $CTeacher) : "-";
				*/
				
				$SubjectTeacherArr = $lreportcard->returnSubjectTeacher($thisClassID, $SubjectID,$ReportID,$ClassStudentIDList[0]);
				$SubjectTeacherName = !empty($SubjectTeacherArr) ? implode(", ", $SubjectTeacherArr) : "-";
				
				$display .= "<tr>
												<td class='tablerow_separator'>".$ClassArr[$i]["ClassName"]."</td>
												<td class='tablerow_separator'>".$SubjectTeacherName."</td>
												<td align='center' class='tablerow_separator'>".$TotalNumber."</td>
												<td align='center' class='tablerow_separator'>".$AbsExNumber."</td>
												<td align='center' class='tablerow_separator'>".$TotalExamNumber."</td>
												<td align='center' class='tablerow_separator'>".$BandNumber["A*"][$thisClassID]."</td>
												<td align='center' class='tablerow_separator'>".$BandPercent["A*"][$thisClassID]."</td>
												<td align='center' class='tablerow_separator'>".$BandNumber["A"][$thisClassID]."</td>
												<td align='center' class='tablerow_separator'>".$BandPercent["A"][$thisClassID]."</td>
												<td align='center' class='tablerow_separator'>".$BandNumber["B"][$thisClassID]."</td>
												<td align='center' class='tablerow_separator'>".$BandPercent["B"][$thisClassID]."</td>
												<td align='center' class='tablerow_separator'>".$BandNumber["C"][$thisClassID]."</td>
												<td align='center' class='tablerow_separator'>".$BandPercent["C"][$thisClassID]."</td>
												<td align='center' class='tablerow_separator'>".$BandNumber["D"][$thisClassID]."</td>
												<td align='center' class='tablerow_separator'>".$BandPercent["D"][$thisClassID]."</td>
												<td align='center' class='tablerow_separator'>".$BandNumber["EU"][$thisClassID]."</td>
												<td align='center' class='tablerow_separator'>".$BandPercent["EU"][$thisClassID]."</td>
												<td align='center' class='tablerow_separator'>".$BandNumber["A*-A"][$thisClassID]."</td>
												<td align='center' class='tablerow_separator'>".$BandPercent["A*-A"][$thisClassID]."</td>
												<td align='center' class='tablerow_separator'>".$BandNumber["A*-C"][$thisClassID]."</td>
												<td align='center' class='tablerow_separator'>".$BandPercent["A*-C"][$thisClassID]."</td>
												<td align='center' class='tablerow_separator'>".$ClassStudentAverageMark[$thisClassID]."</td>
										</tr>";
			} // End Loop 2: Classes
			// Display a row sum up stat of the whole class level
			$ClassLevelAbsNumber = array_sum($AbsNumber);
			$ClassLevelExNumber = array_sum($ExNumber);
			$NumberOfClassLevelStudent = array_sum($NumberOfClassStudent);
			
			$ClassLevelTotalExamNumber = $NumberOfClassLevelStudent - $ClassLevelAbsNumber - $ClassLevelExNumber;
			
			$ClassLevelBandNumber["A*"] = array_sum($BandNumber["A*"]);
			$ClassLevelBandNumber["A"] = array_sum($BandNumber["A"]);
			$ClassLevelBandNumber["B"] = array_sum($BandNumber["B"]);
			$ClassLevelBandNumber["C"] = array_sum($BandNumber["C"]);
			$ClassLevelBandNumber["D"] = array_sum($BandNumber["D"]);
			$ClassLevelBandNumber["EU"] = array_sum($BandNumber["EU"]);
			
			$ClassLevelBandNumber["A*-A"] = $ClassLevelBandNumber["A*"]+$ClassLevelBandNumber["A"];
			$ClassLevelBandNumber["A*-C"] = $ClassLevelBandNumber["A*"]+$ClassLevelBandNumber["A"]+$ClassLevelBandNumber["B"]+$ClassLevelBandNumber["C"];
			
			$ClassLevelStudentTotalMark = array_sum($ClassStudentTotalmark);
			
			// Prevent divide by Zero
			if ($ClassLevelTotalExamNumber != 0) {
				$ClassLevelBandPercent["A*"] = calculatePercent($ClassLevelBandNumber["A*"], $ClassLevelTotalExamNumber);
				$ClassLevelBandPercent["A"] = calculatePercent($ClassLevelBandNumber["A"], $ClassLevelTotalExamNumber);
				$ClassLevelBandPercent["B"] = calculatePercent($ClassLevelBandNumber["B"], $ClassLevelTotalExamNumber);
				$ClassLevelBandPercent["C"] = calculatePercent($ClassLevelBandNumber["C"], $ClassLevelTotalExamNumber);
				$ClassLevelBandPercent["D"] = calculatePercent($ClassLevelBandNumber["D"], $ClassLevelTotalExamNumber);
				$ClassLevelBandPercent["EU"] = calculatePercent($ClassLevelBandNumber["EU"], $ClassLevelTotalExamNumber);
				
				$ClassLevelBandPercent["A*-A"] = calculatePercent($ClassLevelBandNumber["A*-A"], $ClassLevelTotalExamNumber);
				$ClassLevelBandPercent["A*-C"] = calculatePercent($ClassLevelBandNumber["A*-C"], $ClassLevelTotalExamNumber);
				
				$ClassLevelStudentAverageMark = round(($ClassLevelStudentTotalMark/$ClassLevelTotalExamNumber) ,1);
			}
			
			$display .= "<tr>
											<td class='tablerow_separator'>&nbsp;</td>
											<td class='tablerow_separator'>Level Average</td>
											<td align='center' class='tablerow_separator'>".$NumberOfClassLevelStudent."</td>
											<td align='center' class='tablerow_separator'>&nbsp;</td>
											<td align='center' class='tablerow_separator'>".$ClassLevelTotalExamNumber."</td>
											<td align='center' class='tablerow_separator'>".$ClassLevelBandNumber["A*"]."</td>
											<td align='center' class='tablerow_separator'>".$ClassLevelBandPercent["A*"]."</td>
											<td align='center' class='tablerow_separator'>".$ClassLevelBandNumber["A"]."</td>
											<td align='center' class='tablerow_separator'>".$ClassLevelBandPercent["A"]."</td>
											<td align='center' class='tablerow_separator'>".$ClassLevelBandNumber["B"]."</td>
											<td align='center' class='tablerow_separator'>".$ClassLevelBandPercent["B"]."</td>
											<td align='center' class='tablerow_separator'>".$ClassLevelBandNumber["C"]."</td>
											<td align='center' class='tablerow_separator'>".$ClassLevelBandPercent["C"]."</td>
											<td align='center' class='tablerow_separator'>".$ClassLevelBandNumber["D"]."</td>
											<td align='center' class='tablerow_separator'>".$ClassLevelBandPercent["D"]."</td>
											<td align='center' class='tablerow_separator'>".$ClassLevelBandNumber["EU"]."</td>
											<td align='center' class='tablerow_separator'>".$ClassLevelBandPercent["EU"]."</td>
											<td align='center' class='tablerow_separator'>".$ClassLevelBandNumber["A*-A"]."</td>
											<td align='center' class='tablerow_separator'>".$ClassLevelBandPercent["A*-A"]."</td>
											<td align='center' class='tablerow_separator'>".$ClassLevelBandNumber["A*-C"]."</td>
											<td align='center' class='tablerow_separator'>".$ClassLevelBandPercent["A*-C"]."</td>
											<td align='center' class='tablerow_separator'>".$ClassLevelStudentAverageMark."</td>
									</tr>
									<tr><td colspan='21'>&nbsp;</td></tr>";
			
			
		} // End Loop 1: Subjects
		
		#debug_r($FormSubjectGradingSchemeArr);
		############## Interface Start ##############
		
?>
<style type="text/css">
<!--
h1 {
	font-size: 24px;
	font-family: "Times CY", "Times New Roman", serif;
	font-style: italic;
	margin: 0px;
	padding-top: 3px;
	padding-bottom: 3px;
	border-bottom: 5px solid #DDD;
}

h2 {
	font-size: 16px;
	font-family: "Times CY", "Times New Roman", serif;
	margin: 0px;
	padding-top: 2px;
	padding-bottom: 2px;
}

#StatReportWrapper {
	min-height: 800px;
	height: auto;
	_height: 800px;
	border-top: 5px solid #DDD;
	border-bottom: 5px solid #DDD;
}

#StatTable thead {
	font-size: 15px;
	font-family: "Times CY", "Times New Roman", serif;
	font-style: italic;
}

#StatTable tbody {
	font-size: 12px;
	font-family: arial, "lucida console", sans-serif;
}

.SubjectName {
	font-size: 12px;
	font-weight: bold;
	font-family: arial, "lucida console", sans-serif;
	padding-bottom: 5px;
}

.smallTableHeader {
	font-size: 11px;
	font-weight: bold;
	font-family: "Times CY", "Times New Roman", serif;
}

.tablerow_separator {
	BORDER-BOTTOM: #DCDCDC 1px solid
}

.GMS_text14bi {
	font-family: "Times New Roman", "Arial", "Lucida Console";	
	font-weight: bold;
	font-size: 14px;
	font-style: italic;
}

.tableline {
	border-bottom-width: 2px;
	border-bottom-style: solid;
	border-bottom-color: #000000;
}
-->
</style>

<script type="text/JavaScript" language="JavaScript">

</script>
<div id="StatReportWrapper" style="width:100%;">
	<h1><?=$StatReportTitle?></h1>
	<h2>
		School Year&nbsp;&nbsp;<?= $SchoolYear ?>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		Level:&nbsp;<?= $ClassLevelName ?>
	</h2>
	<table id="StatTable" width="100%" cellpadding="0" cellspacing="0" border="0">
		<thead>
			<tr>
				<th>&nbsp;</th>
				<th align='left'>Subject Teacher</th>
				<th>Total</th>
				<th>Abs/Ex</th>
				<th>Total Exam</th>
				<th colspan="2">Band A&#42;</th>
				<th colspan="2">Band A</th>
				<th colspan="2">Band B</th>
				<th colspan="2">Band C</th>
				<th colspan="2">Band D</th>
				<th colspan="2">Band E&#47;U</th>
				<th colspan="2" class="smallTableHeader">Quality<br />Passes(A&#42;-A)</th>
				<th colspan="2" class="smallTableHeader">Passes(A&#42;-C)</th>
				<th>Average</th>
			</tr>
			<tr>
				<th>&nbsp;</th>
				<th>&nbsp;</th>
				<th>Number</th>
				<th>Number</th>
				<th>Number</th>
				<th class="smallTableHeader">Number</th>
				<th class="smallTableHeader">&#37;</th>
				<th class="smallTableHeader">Number</th>
				<th class="smallTableHeader">&#37;</th>
				<th class="smallTableHeader">Number</th>
				<th class="smallTableHeader">&#37;</th>
				<th class="smallTableHeader">Number</th>
				<th class="smallTableHeader">&#37;</th>
				<th class="smallTableHeader">Number</th>
				<th class="smallTableHeader">&#37;</th>
				<th class="smallTableHeader">Number</th>
				<th class="smallTableHeader">&#37;</th>
				<th class="smallTableHeader">Number</th>
				<th class="smallTableHeader">&#37;</th>
				<th class="smallTableHeader">Number</th>
				<th class="smallTableHeader">&#37;</th>
				<th>Marks</th>
			</tr>
		</thead>
		<tbody>
			<?= $display ?>
			<tr><td height="1" class="tabletext tableline" colspan='22'><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
	    	<tr><td class="GMS_text14bi" colspan='22'>&nbsp;<?=$LastGenerated?></td></tr>
		</tbody>
	</table>
</div>
<?
  } else {
?>
You have no priviledge to access this page.
<?
	}
} else {
?>
You have no priviledge to access this page.
<?
}
?>
