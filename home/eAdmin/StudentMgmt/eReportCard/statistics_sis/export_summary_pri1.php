<?php
######################################################
# Generate Statistic Report for Primary Class Level 1-4
# 	Trigger from: index.php
# 	Required variables: $ClassLevelID, $ReportID
# 	Optional variables: $ReportColumnID
#
# 	Last updated: Andy Chan on 2008/6/3
######################################################

function returnGrandMSBand($Marks='-1') {
	if($Marks == -1)	return array("1","2","3","4");
	if($Marks >= 85)	return "1";
	if($Marks >= 70)	return "2";
	if($Marks >= 50)	return "3";
	return "4";
}

function calculatePercent($Numerator, $Denumerator) {
	return number_format(round(($Numerator/$Denumerator*100) ,1) ,1);
}

// Root path
$PATH_WRT_ROOT = "../../../../../";

// Page access right
$PageRight = "ADMIN";

// Include general libraries
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

// Authorize intranet user & connect Database
intranet_auth();
intranet_opendb();

// Check if ReportCard module is enable
if ($plugin['ReportCard2008']) {
	// Include ReportCard libraries
	include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
	include_once($PATH_WRT_ROOT."includes/libreportcard2008w.php");
	
// Create objects
	if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName == "sis") 
	{
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
		$lreportcard = new libreportcardcustom();
	} else {
		$lreportcard = new libreportcard2008w();
	}
	$lclass = new libclass();
	
	$ExportArr = array();
	$lexport = new libexporttext();
	
	// Check module access right
	if ($lreportcard->hasAccessRight()) {
		// Check required data submitted from index.php
		if (!isset($ClassLevelID, $ReportID)) {
			intranet_closedb();
			header("Location: index.php");
			exit();
		}
		
		$LastGenerated = $lreportcard->GET_CURRENT_DATETIME();
		
		// Get ReportCard info
		$reportTemplateInfo = $lreportcard->returnReportTemplateBasicInfo($ReportID);
		
		// Check to make sure it is a whole year & generated report
		if ($reportTemplateInfo["LastGenerated"] == "0000-00-00 00:00:00" || $reportTemplateInfo["LastGenerated"] == "") {
			intranet_closedb();
			header("Location: index.php");
			exit();
		}
		
		// Get subjects of the current ClassLevel
		$FormSubjectArr = $lreportcard->returnSubjectwOrder($ClassLevelID, 1);
		
		// Get subjects' grading scheme of the current ClassLevel
		$FormSubjectGradingSchemeArr = $lreportcard->GET_FROM_SUBJECT_GRADING_SCHEME($ClassLevelID);
		
		// Get ClassLevel name
		$ClassLevelName = $lreportcard->returnClassLevel($ClassLevelID);
		
		// Get Classes
		$ClassArr = $lreportcard->GET_CLASSES_BY_FORM($ClassLevelID);
		
		// Get current school year
		$SchoolYear = $lreportcard->schoolYear;
		
		// Get columns info of the current ReportCard
		$ColumnTitleAry = $lreportcard->returnReportColoumnTitle($ReportID);
		$ColumnIDList = array_keys($ColumnTitleAry);
		
		// Check if ReportColumnID have value
		// 	if yes, only term mark is used
		// 	if no, overall marks of all terms are used
		if (isset($ReportColumnID) && $ReportColumnID != "") {
			// Hardcoded Column Name since names in $ColumnTitleAry are not exactly "SA1" & "SA2"
			if ($ReportColumnID == $ColumnIDList[0])	$DisplayColumnName = "SA1";
			if ($ReportColumnID == $ColumnIDList[1])	$DisplayColumnName = "SA2";
			
			$StatReportTitle = "Summary of ".$DisplayColumnName." Results by Subject (Primary)";
		} else {
			$StatReportTitle = "Summary of Overall Results by Subject (Primary)";
		}
		
		# build file name
		$thisAcadermicYear = getCurrentAcademicYear();
		$filename = intranet_undo_htmlspecialchars($thisAcadermicYear."_".$ClassLevelName."_".$StatReportTitle.".csv");
		
		# define column title
		$exportColumn = array();
		$exportColumn[]= $StatReportTitle;
		$numEmptyTitle = 15;
		for ($i=0; $i<$numEmptyTitle; $i++)
		{
			$exportColumn[]= "";
		}
		
		$iCounter = 0;
		$jCounter = 0;
		
		# School Year, Level
		$ExportArr[$iCounter][$jCounter] = "School Year ".$SchoolYear;
		$jCounter++;		
		$ExportArr[$iCounter][$jCounter] = "Level: ".$ClassLevelName;
		$jCounter++;
		$iCounter++;
		
		# Title Row
		$jCounter = 0;
		$ExportArr[$iCounter][$jCounter] = "";
		$jCounter++;
		$ExportArr[$iCounter][$jCounter] = "Subject Teacher";
		$jCounter++;
		$ExportArr[$iCounter][$jCounter] = "Total Number";
		$jCounter++;
		$ExportArr[$iCounter][$jCounter] = "Abs/Ex Number";
		$jCounter++;
		$ExportArr[$iCounter][$jCounter] = "Total Exam Number";
		$jCounter++;
		$ExportArr[$iCounter][$jCounter] = "Band 1 Number";
		$jCounter++;
		$ExportArr[$iCounter][$jCounter] = "Band 1 %";
		$jCounter++;
		$ExportArr[$iCounter][$jCounter] = "Band 2 Number";
		$jCounter++;
		$ExportArr[$iCounter][$jCounter] = "Band 2 %";
		$jCounter++;
		$ExportArr[$iCounter][$jCounter] = "Band 3 Number";
		$jCounter++;
		$ExportArr[$iCounter][$jCounter] = "Band 3 %";
		$jCounter++;
		$ExportArr[$iCounter][$jCounter] = "Band 4 Number";
		$jCounter++;
		$ExportArr[$iCounter][$jCounter] = "Band 4 %";
		$jCounter++;
		$ExportArr[$iCounter][$jCounter] = "Band 1-3 Number";
		$jCounter++;
		$ExportArr[$iCounter][$jCounter] = "Band 1-3 %";
		$jCounter++;
		$ExportArr[$iCounter][$jCounter] = "Average Marks";
		$jCounter++;
		
		$iCounter++;
		
		// Main table generaeting logic
		// Loop 1: Subjects
		foreach ($FormSubjectArr as $SubjectID => $SubjectName) {
			if ($FormSubjectGradingSchemeArr[$SubjectID]["scaleInput"] != "M")
				continue;
			
			$jCounter = 0;
			
			$ExportArr[$iCounter][$jCounter] = str_replace(" ()", "", $SubjectName[0]);
			$iCounter++;
			
			// Init variables
			$AbsNumber = array();
			$ExNumber = array();
			$BandNumber = array();
			$BandPercent = array();
			$NumberOfClassStudent = array();
			$ClassStudentTotalmark = array();
			$ClassStudentAverageMark =  array();
			
			$ClassLevelAbsNumber = 0;
			$ClassLevelExNumber = 0;
			$NumberOfClassLevelStudent = 0;
			$ClassLevelBandNumber = array();
			$ClassLevelBandPercent = array();
			$ClassLevelStudentTotalMark = 0;
			$ClassLevelStudentAverageMark = 0;
			
			// Loop 2: Classes
			for($i=0; $i<sizeof($ClassArr); $i++) {
				$thisClassID = $ClassArr[$i]["ClassID"];
				
				// Construct StudentIDList in the class
				$ClassStudentArr = $lreportcard->GET_STUDENT_BY_CLASS($thisClassID);
				$ClassStudentIDList = array();
				for($j=0; $j<sizeof($ClassStudentArr); $j++) {
					$ClassStudentIDList[] = $ClassStudentArr[$j]["UserID"];
				}
				$ClassStudentIDListQuery = implode("','", $ClassStudentIDList);
				$ClassStudentIDListQuery = "'".$ClassStudentIDListQuery."'";
				
				// Query the consolidated marks of the Class
				$table = $lreportcard->DBName.".RC_REPORT_RESULT_SCORE";
				$sql = "SELECT Mark, Grade FROM $table ";
				$sql .= "WHERE ReportID = '$ReportID' AND SubjectID = '$SubjectID' AND StudentID IN ($ClassStudentIDListQuery) AND ";
				if (isset($ReportColumnID) && $ReportColumnID != "") {
					$sql .= "ReportColumnID = '$ReportColumnID'";
				} else {
					$sql .= "(ReportColumnID = 0 OR ReportColumnID IS NULL)";
				}
				$ClassResult = $lreportcard->returnArray($sql, 2);
				
				$NumberOfClassStudent[$thisClassID] = sizeof($ClassResult);
				
				// Init variables
				$AbsNumber[$thisClassID] = 0;
				$ExNumber[$thisClassID] = 0;
				$BandNumber["1"][$thisClassID] = 0;
				$BandNumber["2"][$thisClassID] = 0;
				$BandNumber["3"][$thisClassID] = 0;
				$BandNumber["4"][$thisClassID] = 0;
				$BandNumber["1-3"][$thisClassID] = 0;
				$BandPercent["1"][$thisClassID] = 0;
				$BandPercent["2"][$thisClassID] = 0;
				$BandPercent["3"][$thisClassID] = 0;
				$BandPercent["4"][$thisClassID] = 0;
				$BandPercent["1-3"][$thisClassID] = 0;
				$ClassStudentTotalmark[$thisClassID] = 0;
				$ClassStudentAverageMark[$thisClassID] = 0;
				
				// Loop the marks and group into different categories
				for($j=0; $j<sizeof($ClassResult); $j++) {
					$StudentBand = "";
					if ($ClassResult[$j]["Mark"] != "" && $ClassResult[$j]["Grade"] == "") {
						$ClassStudentTotalmark[$thisClassID] += $ClassResult[$j]["Mark"];
						$StudentBand = returnGrandMSBand($ClassResult[$j]["Mark"]);
						if (!is_array($StudentBand))
							$BandNumber[$StudentBand][$thisClassID]++;
					} else if ($ClassResult[$j]["Grade"] == "-") {
						$AbsNumber[$thisClassID]++;
					} else if ($ClassResult[$j]["Grade"] == "/" || $ClassResult[$j]["Grade"] == "N.A.") {
						$ExNumber[$thisClassID]++;
					}
				}
				
				$TotalNumber = $NumberOfClassStudent[$thisClassID];
				$AbsExNumber = $AbsNumber[$thisClassID]+$ExNumber[$thisClassID];
				$TotalExamNumber = $TotalNumber - $AbsExNumber;
				
				$BandNumber["1-3"][$thisClassID] = $BandNumber["1"][$thisClassID]+$BandNumber["2"][$thisClassID]+$BandNumber["3"][$thisClassID];
				
				// Prevent divide by Zero
				if ($TotalExamNumber != 0) {
					$BandPercent["1"][$thisClassID] = calculatePercent($BandNumber["1"][$thisClassID], $TotalExamNumber);
					$BandPercent["2"][$thisClassID] = calculatePercent($BandNumber["2"][$thisClassID], $TotalExamNumber);
					$BandPercent["3"][$thisClassID] = calculatePercent($BandNumber["3"][$thisClassID], $TotalExamNumber);
					$BandPercent["4"][$thisClassID] = calculatePercent($BandNumber["4"][$thisClassID], $TotalExamNumber);
					
					$BandPercent["1-3"][$thisClassID] = calculatePercent($BandNumber["1-3"][$thisClassID], $TotalExamNumber);
					
					$ClassStudentAverageMark[$thisClassID] = round(($ClassStudentTotalmark[$thisClassID]/$TotalExamNumber), 1);
				}
				
				# update on 11 Dec by Ivan - show subject teacher instead of class teacher
				/*
				$CTeacher = array();
				$ClassTeacherNameArr = $lclass->returnClassTeacher($ClassArr[$i]["ClassName"]);
				foreach($ClassTeacherNameArr as $key=>$val) {
					$CTeacher[] = $val['CTeacher'];
				}
				$ClassTeacherName = !empty($CTeacher) ? implode(", ", $CTeacher) : "-";
				*/
				
				$SubjectTeacherArr = $lreportcard->returnSubjectTeacher($thisClassID, $SubjectID);
				$SubjectTeacherName = !empty($SubjectTeacherArr) ? implode(", ", $SubjectTeacherArr) : "-";
				
				$jCounter = 0;
				
				$ExportArr[$iCounter][$jCounter] = $ClassArr[$i]["ClassName"];
				$jCounter++;
				$ExportArr[$iCounter][$jCounter] = $SubjectTeacherName;
				$jCounter++;
				$ExportArr[$iCounter][$jCounter] = $TotalNumber;
				$jCounter++;
				$ExportArr[$iCounter][$jCounter] = $AbsExNumber;
				$jCounter++;
				$ExportArr[$iCounter][$jCounter] = $TotalExamNumber;
				$jCounter++;
				$ExportArr[$iCounter][$jCounter] = $BandNumber["1"][$thisClassID];
				$jCounter++;
				$ExportArr[$iCounter][$jCounter] = $BandPercent["1"][$thisClassID];
				$jCounter++;
				$ExportArr[$iCounter][$jCounter] = $BandNumber["2"][$thisClassID];
				$jCounter++;
				$ExportArr[$iCounter][$jCounter] = $BandPercent["2"][$thisClassID];
				$jCounter++;
				$ExportArr[$iCounter][$jCounter] = $BandNumber["3"][$thisClassID];
				$jCounter++;
				$ExportArr[$iCounter][$jCounter] = $BandPercent["3"][$thisClassID];
				$jCounter++;
				$ExportArr[$iCounter][$jCounter] = $BandNumber["4"][$thisClassID];
				$jCounter++;
				$ExportArr[$iCounter][$jCounter] = $BandPercent["4"][$thisClassID];
				$jCounter++;
				$ExportArr[$iCounter][$jCounter] = $BandNumber["1-3"][$thisClassID];
				$jCounter++;
				$ExportArr[$iCounter][$jCounter] = $BandPercent["1-3"][$thisClassID];
				$jCounter++;
				$ExportArr[$iCounter][$jCounter] = $ClassStudentAverageMark[$thisClassID];
				$jCounter++;
				
				$iCounter++;
				
				
			} // End Loop 2: Classes
			// Display a row sum up stat of the whole class level
			$ClassLevelAbsNumber = array_sum($AbsNumber);
			$ClassLevelExNumber = array_sum($ExNumber);
			$NumberOfClassLevelStudent = array_sum($NumberOfClassStudent);
			
			$ClassLevelTotalExamNumber = $NumberOfClassLevelStudent - $ClassLevelAbsNumber - $ClassLevelExNumber;
			
			$ClassLevelBandNumber["1"] = array_sum($BandNumber["1"]);
			$ClassLevelBandNumber["2"] = array_sum($BandNumber["2"]);
			$ClassLevelBandNumber["3"] = array_sum($BandNumber["3"]);
			$ClassLevelBandNumber["4"] = array_sum($BandNumber["4"]);
			$ClassLevelBandNumber["1-3"] = $ClassLevelBandNumber["1"]+$ClassLevelBandNumber["2"]+$ClassLevelBandNumber["3"];
			
			$ClassLevelStudentTotalMark = array_sum($ClassStudentTotalmark);
			
			// Prevent divide by Zero
			if ($ClassLevelTotalExamNumber != 0) {
				$ClassLevelBandPercent["1"] = calculatePercent($ClassLevelBandNumber["1"], $ClassLevelTotalExamNumber);
				$ClassLevelBandPercent["2"] = calculatePercent($ClassLevelBandNumber["2"], $ClassLevelTotalExamNumber);
				$ClassLevelBandPercent["3"] = calculatePercent($ClassLevelBandNumber["3"], $ClassLevelTotalExamNumber);
				$ClassLevelBandPercent["4"] = calculatePercent($ClassLevelBandNumber["4"], $ClassLevelTotalExamNumber);
				
				$ClassLevelBandPercent["1-3"] = calculatePercent($ClassLevelBandNumber["1-3"], $ClassLevelTotalExamNumber);
				
				$ClassLevelStudentAverageMark = round(($ClassLevelStudentTotalMark/$ClassLevelTotalExamNumber) ,1);
			}
			
			$jCounter = 0;
			
			$ExportArr[$iCounter][$jCounter] = "";
			$jCounter++;
			$ExportArr[$iCounter][$jCounter] = "Level Average";
			$jCounter++;
			$ExportArr[$iCounter][$jCounter] = $NumberOfClassLevelStudent;
			$jCounter++;
			$ExportArr[$iCounter][$jCounter] = "";
			$jCounter++;
			$ExportArr[$iCounter][$jCounter] = $ClassLevelTotalExamNumber;
			$jCounter++;
			$ExportArr[$iCounter][$jCounter] = ($ClassLevelBandNumber["1"] == "")? 0 : $ClassLevelBandNumber["1"];
			$jCounter++;
			$ExportArr[$iCounter][$jCounter] = ($ClassLevelBandPercent["1"] == "")? 0 : $ClassLevelBandPercent["1"];
			$jCounter++;
			$ExportArr[$iCounter][$jCounter] = ($ClassLevelBandNumber["2"] == "")? 0 : $ClassLevelBandNumber["2"];
			$jCounter++;
			$ExportArr[$iCounter][$jCounter] = ($ClassLevelBandPercent["2"] == "")? 0 : $ClassLevelBandPercent["2"];
			$jCounter++;
			$ExportArr[$iCounter][$jCounter] = ($ClassLevelBandNumber["3"] == "")? 0 : $ClassLevelBandNumber["3"];
			$jCounter++;
			$ExportArr[$iCounter][$jCounter] = ($ClassLevelBandPercent["3"] == "")? 0 : $ClassLevelBandPercent["3"];
			$jCounter++;
			$ExportArr[$iCounter][$jCounter] = ($ClassLevelBandNumber["4"] == "")? 0 : $ClassLevelBandNumber["4"];
			$jCounter++;
			$ExportArr[$iCounter][$jCounter] = ($ClassLevelBandPercent["4"] == "")? 0 : $ClassLevelBandPercent["4"];
			$jCounter++;
			$ExportArr[$iCounter][$jCounter] = ($ClassLevelBandNumber["1-3"] == "")? 0 : $ClassLevelBandNumber["1-3"];
			$jCounter++;
			$ExportArr[$iCounter][$jCounter] = ($ClassLevelBandPercent["1-3"] == "")? 0 : $ClassLevelBandPercent["1-3"];
			$jCounter++;
			$ExportArr[$iCounter][$jCounter] = ($ClassLevelStudentAverageMark == "")? 0 : $ClassLevelStudentAverageMark;
			$jCounter++;
			
			$iCounter++;
			
		} // End Loop 1: Subjects
		
		#debug_r($FormSubjectGradingSchemeArr);
		$jCounter = 0;
		$ExportArr[$iCounter][$jCounter] = $LastGenerated;
		
		$export_content = $lexport->GET_EXPORT_TXT($ExportArr, $exportColumn, "", "\r\n", "", 0, "11");
		intranet_closedb();
		
		// Output the file to user browser
		$lexport->EXPORT_FILE($filename, $export_content);
		
  } 
  else {
?>
		You have no priviledge to access this page.
<?
  }
} 
else {
?>
You have no priviledge to access this page.
<?
}
?>
