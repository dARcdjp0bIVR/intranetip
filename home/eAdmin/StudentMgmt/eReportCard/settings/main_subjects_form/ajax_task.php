<?php
#	Editing by

/**************************************************************
 *	Modification log:
 *		Date:	2016-01-20 (Bill)	[2015-0421-1144-05164]
 *				copy from EJ
 ***************************************************************/

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$lreportcard = new libreportcardcustom();
$lreportcard_ui = new libreportcard_ui();

switch ($Task)
{
	case "Reload_Subject_Class_Table":
		echo $lreportcard_ui->Get_Form_Main_Subject_Table($ClassLevelID);
		break;
}

?>