<?php
// Using :

##########################
#
#   Date:   2020-04-28  Bill    [2019-0924-1029-57289]
#           - Create File    ($eRCTemplateSetting['ViewPageAssignedOnly'])
#
##########################

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");

// Check access right
$lreportcard = new libreportcardcustom();
$lreportcard->hasAccessRight();
if(!$sys_custom['eRC']['Settings']['ViewGroupUserType'] || !$eRCTemplateSetting['ViewPageAssignedOnly']) {
    No_Access_Right_Pop_Up();
}

$linterface = new interface_html();
$lreportcard_ui = new libreportcard_ui();

// Get View Group Users
$ViewGroupUserID = IntegerSafe($_POST['user_id'][0]);
if(!$ViewGroupUserID) {
    No_Access_Right_Pop_Up();
}
$ViewGroupUserAssignedPage = $lreportcard->GET_VIEW_GROUP_USER_ASSIGNED_PAGE($ViewGroupUserID);

// Get Current Users
$name_field = getNameFieldByLang();
$sql = "SELECT 
			  UserID, 
			  $name_field as name, 
			  UserLogin 
		FROM 
			  INTRANET_USER 
		WHERE 
			  RecordType = 1 AND
			  UserID = '$ViewGroupUserID' ";
$userInfo = $lreportcard->returnArray($sql,5);

// Available pages for assign
$selectPageCheckbox = array();
if($eRCTemplateSetting['ViewPageAssignedArr'] && !empty($eRCTemplateSetting['ViewPageAssignedArr'])) {
    foreach((array)$eRCTemplateSetting['ViewPageAssignedArr'] as $thisPageAssigned) {
        $is_checked = in_array($thisPageAssigned, (array)$ViewGroupUserAssignedPage);
        $selectPageCheckbox[] = '<input type="checkbox" name="assignedPageArr[]" id="assignedPage_'.$thisPageAssigned.'" value="'.$thisPageAssigned.'" '.($is_checked ? 'checked' : '').'/> <label for="assignedPage_'.$thisPageAssigned.'">'.$eReportCard[$thisPageAssigned].'</label>';
    }
}

// Page
$CurrentPage = "Settings_ViewGroupUser";
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();

// Tag
$TAGS_OBJ = array();
$TAGS_OBJ[] = array($eReportCard["Settings_ViewGroupUser"]);

# Start layout
$linterface->LAYOUT_START();
?>

<script>
function checkForm() {
    $('form#form1').attr('action', 'role_update.php').submit();
}

function goBack() {
    self.location.href = "index.php";
}
</script>

<form name="form1" method="POST" onsubmit="return checkForm();" action="role_update.php">
<table width="98%" border="0" cellspacing="0" cellpadding="5">
    <tr>
        <td align="center">
            <table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
                <tr valign="top">
                    <td width="20%" class="tablerow2"><span class="tabletext"><?=$i_UserName?></span></td>
                    <td width="10" nowrap="nowrap"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="10"></td>
                    <td width="80%" nowrap="nowrap"><span class="tabletext"><?=$userInfo[0]['name']?></span></td>
                </tr>
            </table>

            <?php if(!empty($selectPageCheckbox)) { ?>
                <br />
                <table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
                    <tr valign="top">
                        <td width="20%" class="tablerow2"><span class="tabletext"><?=$eReportCard["UserAccessPages"]?></span></td>
                        <td width="10" nowrap="nowrap"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="10"></td>
                        <td width="80%" nowrap="nowrap">
                            <?= implode('<br /><br />', (array)$selectPageCheckbox) ?>
                        </td>
                    </tr>
                </table>
            <?php } ?>

            <div class="edit_bottom_v30">
                <?= $linterface->GET_ACTION_BTN($button_submit, "submit") ?>&nbsp;
                <?= $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:goBack()") ?>
            </div>
        </td>
    </tr>
</table>

<input type="hidden" name="flag" value="0" />
<input type="hidden" name="StudentID" value="<?=$ViewGroupUserID?>" />
</form>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>