<?
// Using :

##########################
#
#   Date:   2020-04-28  Bill    [2019-0924-1029-57289]
#           - Added logic to update assign page settings    ($eRCTemplateSetting['ViewPageAssignedOnly'])
#
#	Date: 	2017-09-22	Bill	[2017-0502-1024-21096]
#			- Create file
#
##########################

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");

// Check access right
$lreportcard = new libreportcardcustom();
$lreportcard->hasAccessRight();
if(!$sys_custom['eRC']['Settings']['ViewGroupUserType']) {
	No_Access_Right_Pop_Up();
}

// Added Group Users
$tempUserAry = array();
for($i=0, $i_max=sizeof($staff); $i<$i_max; $i++) {
	$tempUserAry[] = (is_numeric($staff[$i])) ? $staff[$i] : substr($staff[$i], 1);
}
$result = $lreportcard->Add_VIEW_GROUP_USER($tempUserAry);

// [2019-0924-1029-57289] Update assigned pages
if($eRCTemplateSetting['ViewPageAssignedOnly'] && !(sizeof($result) && in_array(false, $result))) {
    $lreportcard->Add_VIEW_GROUP_USER_ASSIGNED_PAGE($tempUserAry, $assignedPageArr);
}

intranet_closedb();

if(sizeof($result) && in_array(false, $result)) {
	$msg = "AddUnsuccess";
} else {
	$msg = "AddSuccess";
}

header("Location:index.php?msg=$msg");
?>