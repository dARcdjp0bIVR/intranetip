<?
// Using :

##########################
#
#   Date:   2020-04-28  Bill    [2019-0924-1029-57289]
#           - Added assign page settings    ($eRCTemplateSetting['ViewPageAssignedOnly'])
#
#	Date: 	2017-09-22	Bill	[2017-0502-1024-21096]
#			- Create file
#
##########################

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");

// Check access right
$lreportcard = new libreportcardcustom();
$lreportcard->hasAccessRight();
if(!$sys_custom['eRC']['Settings']['ViewGroupUserType']) {
	No_Access_Right_Pop_Up();
}

$linterface = new interface_html();
$lreportcard_ui = new libreportcard_ui();

// Get View Group Users
$ViewGroupUsers = $lreportcard->GET_ALL_VIEW_GROUP_USER();
if(!empty($ViewGroupUsers)) {
	$i = 1;
	foreach($ViewGroupUsers as $thisUser) {
	    // [2019-0924-1029-57289] Get assigned pages
        if($eRCTemplateSetting['ViewPageAssignedOnly']) {
            $assignedPageNameList = array();
            $assignedPageList = $lreportcard->GET_VIEW_GROUP_USER_ASSIGNED_PAGE($thisUser['UserID']);
            foreach ((array)$assignedPageList as $thisAssignedPage) {
                $assignedPageNameList[] = $eReportCard[$thisAssignedPage];
            }
        }

        $TableContent .= "<tr>";
			$TableContent .= "<td><span class='rowNumSpan'>".$i++."</span></td>";
            // [2019-0924-1029-57289]
            if($eRCTemplateSetting['ViewPageAssignedOnly']) {
                $TableContent .= "<td><span class='rowNumSpan'>".$thisUser["UserName"]."</span></td>";
                $TableContent .= "<td><span class='rowNumSpan'>".implode('<br />', (array)$assignedPageNameList)."</span></td>";
                $TableContent .= "<td><span class='rowNumSpan'>".$thisUser["InputDate"]."</span></td>";
            }
            else {
                $TableContent .= "<td><span class='rowNumSpan'>".$thisUser["UserName"]."</span></td>";
                $TableContent .= "<td><span class='rowNumSpan'>".$thisUser["InputDate"]."</span></td>";
            }
			$TableContent .= "<td><input type='checkbox' class='checkbox' name='user_id[]' id='checkbox_".$thisUser["UserID"]."' value='".$thisUser["UserID"]."'></td>";
		$TableContent .= "</tr>";
	}
}
else {
	$TableContent .= "<tr>";
        // [2019-0924-1029-57289]
        if($eRCTemplateSetting['ViewPageAssignedOnly']) {
            $TableContent .= "<td colspan='5' align='center'>";
                $TableContent .= $Lang['General']['NoRecordAtThisMoment'] ;
            $TableContent .= "</td>";
        }
        else {
            $TableContent .= "<td colspan='4' align='center'>";
                $TableContent .= $Lang['General']['NoRecordAtThisMoment'] ;
            $TableContent .= "</td>";
        }
	$TableContent .= "</tr>";
}

// Page
$CurrentPage = "Settings_ViewGroupUser";
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();

// Tag
$TAGS_OBJ = array();
$TAGS_OBJ[] = array($eReportCard["Settings_ViewGroupUser"]);

// Tool Bar
$btnAry = array();
$btnAry[] = array("new", "javascript: goNew();");
$htmlAry["ToolBar"] = $linterface->Get_Content_Tool_By_Array_v30($btnAry);

// Table Action Button
$actionBtnAry = array();
// [2019-0924-1029-57289]
if($eRCTemplateSetting['ViewPageAssignedOnly']) {
    $actionBtnAry[] = array("edit", "javascript:checkEdit2(document.form1, 'user_id[]', 'goEdit();')");
}
$actionBtnAry[] = array("delete", "javascript:checkRemove2(document.form1, 'user_id[]', 'goDelete();')");
$htmlAry["DataTableBtn"] = $linterface->Get_DBTable_Action_Button_IP25($actionBtnAry);

# Table Content
$htmlAry["DataTable"] = "";
$htmlAry["DataTable"] .= "<table class='common_table_list_v30' id='ContentTable'>";
	$htmlAry["DataTable"] .= "<thead>";
	$htmlAry["DataTable"] .= "<tr>";
		$htmlAry["DataTable"] .= "<th width='1'>#</th>\n";
        // [2019-0924-1029-57289]
		if($eRCTemplateSetting['ViewPageAssignedOnly']) {
            $htmlAry["DataTable"] .= "<th width='30%'>".$i_UserName."</th>\n";
            $htmlAry["DataTable"] .= "<th width='50%'>".$eReportCard["UserAccessPages"]."</th>\n";
            $htmlAry["DataTable"] .= "<th width='20%'>".$eReportCard["UserAddedDate"]."</th>\n";
        }
        else {
            $htmlAry["DataTable"] .= "<th width='60%'>".$i_UserName."</th>\n";
            $htmlAry["DataTable"] .= "<th width='40%'>".$eReportCard["UserAddedDate"]."</th>\n";
        }
		$htmlAry["DataTable"] .= "<th width='1'><input type='checkbox' id='checkmaster' name='checkmaster' onClick='CheckAll()'></th>\n";
	$htmlAry["DataTable"] .= "</tr>";
	$htmlAry["DataTable"] .= "</thead>";
	$htmlAry["DataTable"] .= "<tbody>";
		$htmlAry["DataTable"] .= $TableContent;
	$htmlAry["DataTable"] .= "</tbody>";
$htmlAry["DataTable"] .= "</table>";

// Return Message
$returnMsgKey = $_GET["msg"];
$ReturnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];

$linterface->LAYOUT_START($ReturnMsg);
?>

<script type="text/javascript">
$(document).ready(function() {
	$('.checkbox').click(function() {
		if($(this).attr('checked')) {
			// do nothing
		}
		else {
			$('#checkmaster').removeAttr('checked');
		}
	});
});

function CheckAll() {
	if($('#checkmaster').attr('checked')) {
		$('.checkbox').attr('checked','checked');
	}
	else {
		$('.checkbox').removeAttr('checked');
	}
}

function goNew() {
	window.location = 'edit.php';
}

function goEdit() {
    $('form#form1').attr('action', 'edit_role.php').submit();
}

function goDelete() {
	$('form#form1').attr('action', 'delete.php').submit();
}
</script>

<form name="form1" id="form1" method="POST">
	<div class="table_board">
		<div class="content_top_tool">
			<?=$htmlAry["ToolBar"]?>
		</div>
		<br style="clear:both;">
		<p class="spacer"></p>
		
		<?=$htmlAry["DataTableBtn"]?>
		<?=$htmlAry["DataTable"]?>
		<br style="clear:both;" />
		<br style="clear:both;" />
	</div>
</form>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>