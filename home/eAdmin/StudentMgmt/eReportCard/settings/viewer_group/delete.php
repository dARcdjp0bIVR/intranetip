<?
// Using :

##########################
#
#   Date:   2020-04-28  Bill    [2019-0924-1029-57289]
#           - Added logic to update assign page settings    ($eRCTemplateSetting['ViewPageAssignedOnly'])
#
#	Date: 	2017-09-22	Bill	[2017-0502-1024-21096]
#			- Create file
#
##########################

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");

// Check access right
$lreportcard = new libreportcardcustom();
$lreportcard->hasAccessRight();
if(!$sys_custom['eRC']['Settings']['ViewGroupUserType']) {
	No_Access_Right_Pop_Up();
}

// Delete Group Users
$result = $lreportcard->Delete_VIEW_GROUP_USER($user_id);

// [2019-0924-1029-57289] Delete assigned pages
if($eRCTemplateSetting['ViewPageAssignedOnly'] && $result) {
    $lreportcard->Delete_VIEW_GROUP_USER_ASSIGNED_PAGE($user_id);
}

intranet_closedb();

if($result) {
	$msg = "DeleteSuccess";
} else {
	$msg = "DeleteUnsuccess";
}

header("Location:index.php?msg=$msg");
?>