<?php
# using:
/*
 * 	Log
 * 	Date:	2018-06-12 Anna
 *          Created this page
 * 
 */

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");


intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();

$lreportcard = new libreportcardcustom();
$lreportcard->hasAccessRight();

if(!$plugin['eEnrollment'] || !$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']) )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

// include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
// $lgeneralsettings = new libgeneralsettings();


if($changeName == 'clubName'){
    $countOfLoop = count($EnrolGroupIDAry);
    for($i=0;$i<$countOfLoop;$i++){
        $_thisEnrolGroupID = $EnrolGroupIDAry[$i];
        $_dispalyName = $libenroll->get_Club_DisplayName($_thisEnrolGroupID);
        $_updateName = $ClubNameAry[$i];

        if(empty($_dispalyName)){
            //insert
           $SuccessAry[] =  $libenroll->insert_Club_DisplayName($_thisEnrolGroupID,$_updateName);
            
            
        }else{
            //update
            
            $SuccessAry[] = $libenroll->update_Club_DisplayName($_thisEnrolGroupID,$_updateName);
        }
    }


}else{
    

    $countOfActivityLoop = count($EnrolEventIDAry);
    
  
    for($i=0;$i<$countOfActivityLoop;$i++){
        $_thisEventID = $EnrolEventIDAry[$i];
        $_activitydispalyName = $libenroll->get_Event_DisplayName($_thisEventID);
      
        $_updateName = $EventNameAry[$i];
        if(empty($_activitydispalyName)){
            //insert
            $SuccessAry[] =  $libenroll->insert_Activity_DisplayName($_thisEventID,$_updateName);
            
            
        }else{
            //update
            
            $SuccessAry[] = $libenroll->update_Activity_DisplayName($_thisEventID,$_updateName);
        }
    }

   
    
}


intranet_closedb();
if($changeName == 'clubName'){
    header("Location: club_name.php?xmsg=".$Lang['eNotice']['SettingsUpdateSuccess']."&AcademicYearID=".$AcademicYearID);
}else{
    header("Location: activity_name.php?xmsg=".$Lang['eNotice']['SettingsUpdateSuccess']."&AcademicYear=".$AcademicYearID);
}


?>