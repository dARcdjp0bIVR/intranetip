<?php
// Using: Bill

/********************************************************
 * Modification log
 * 	2017-07-07	(Bill)	[2016-0822-1721-00164]
 * 	- to hide School Chop when $eRCTemplateSetting['Settings']['TeacherExtraInfo_HideSchoolChop'] is set
 * ******************************************************/
 
$PageRight = "ADMIN";

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

$arrCookies = array();
$arrCookies[] = array("ck_reportcard_settings_teacherExtraInfo_page_size", "numPerPage");
$arrCookies[] = array("ck_reportcard_settings_teacherExtraInfo_field", "field");	
$arrCookies[] = array("ck_reportcard_settings_teacherExtraInfo_order", "order");
$arrCookies[] = array("ck_reportcard_settings_teacherExtraInfo_pageNo", "pageNo");
$arrCookies[] = array("ck_reportcard_settings_teacherExtraInfo_Keyword", "Keyword");
$arrCookies[] = array("ck_reportcard_settings_teacherExtraInfo_FilterSignature", "FilterSignature");
$arrCookies[] = array("ck_reportcard_settings_teacherExtraInfo_FilterSchoolChop", "FilterSchoolChop");
if(isset($clearCoo) && $clearCoo == 1) {
	clearCookies($arrCookies);
}
else {
	updateGetCookies($arrCookies);
}

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_teacherExtraInfo.php");

$lreportcard = new libreportcardcustom();
$lreportcard_teacherExtraInfo = new libreportcard_teacherExtraInfo();
$linterface = new interface_html();

$lreportcard->hasAccessRight();

### Get Parameter
$returnMsgKey = $_GET['returnMsgKey'];

$CurrentPage = "Settings_TeacherExtraInfo";	
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();

$TAGS_OBJ = array();
$TAGS_OBJ[] = array($eReportCard['Settings_TeacherExtraInfo']);

$ReturnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];
$linterface->LAYOUT_START($ReturnMsg);

### Toolbar
if($eRCTemplateSetting['Settings']['TeacherExtraInfo_HideSchoolChop']) {
	$htmlAry['toolbar'] = $linterface->Get_Content_Tool_v30('upload', $href="javascript:goUploadSignature();", $text="", $optionAry="", $other="", $divID='', $extra_class='');
}
else {
	$optionAry = array();
	$optionAry[] = array('javascript:goUploadSignature();', $Lang['AccountMgmt']['Signature']);
	$optionAry[] = array('javascript:goUploadSchoolChop();', $eReportCard['Template']['SchoolChop']);
	$htmlAry['toolbar'] = $linterface->Get_Content_Tool_v30('upload', $href="#", $text="", $optionAry, $other="", $divID='', $extra_class='');
}

### Filtering
$optionAry = array();
$optionAry[] = array('uploaded', $Lang['eReportCard']['SettingsArr']['TeacherExtraInfoArr']['UploadedSignature']);
$optionAry[] = array('notUploaded', $Lang['eReportCard']['SettingsArr']['TeacherExtraInfoArr']['NotYetUploadedSignature']);
$htmlAry['filterSignature'] = getSelectByArray($optionAry," id='FilterSignature' name='FilterSignature' onchange='document.form1.submit();' ", $FilterSignature, 1);

if(!$eRCTemplateSetting['Settings']['TeacherExtraInfo_HideSchoolChop']) {
	$optionAry = array();
	$optionAry[] = array('uploaded', $Lang['eReportCard']['SettingsArr']['TeacherExtraInfoArr']['UploadedSchoolChop']);
	$optionAry[] = array('notUploaded', $Lang['eReportCard']['SettingsArr']['TeacherExtraInfoArr']['NotYetUploadedSchoolChop']);
	$htmlAry['filterSchoolChop'] = getSelectByArray($optionAry," id='FilterSchoolChop' name='FilterSchoolChop' onchange='document.form1.submit();' ", $FilterSchoolChop, 1);
}

### Search box
$htmlAry['searchBox'] = $linterface->Get_Search_Box_Div('Keyword', $Keyword);

### DB table action button
$TableBtnAry = array();
$TableBtnAry[] = array('edit', 'javascript:goEdit();');
$TableBtnAry[] = array('delete', 'javascript:goDeleteSignature();', $Lang['eReportCard']['SettingsArr']['TeacherExtraInfoArr']['DeleteSignature']);
if(!$eRCTemplateSetting['Settings']['TeacherExtraInfo_HideSchoolChop']) {
	$TableBtnAry[] = array('delete', 'javascript:goDeleteSchoolChop();', $Lang['eReportCard']['SettingsArr']['TeacherExtraInfoArr']['DeleteSchoolChop']);
}
$TableBtnAry[] = array('delete', 'javascript:goDelete();');
$htmlAry['dbTableBtn'] = $linterface->Get_DBTable_Action_Button_IP25($TableBtnAry);

$htmlAry['dbTable'] = $lreportcard_teacherExtraInfo->getExtraInfoDBTable($field, $order, $pageNo, $Keyword, $FilterSignature, $FilterSchoolChop);
?>

<script language="javascript">
function goEdit() {
	checkEdit(document.getElementById('form1'), 'userIdAry[]', 'edit.php');
}

function goDeleteSignature() {
	$('input#actionType').val('signature');
	checkRemove(document.getElementById('form1'), 'userIdAry[]', 'delete.php');
}

function goDeleteSchoolChop() {
	$('input#actionType').val('schoolChop');
	checkRemove(document.getElementById('form1'), 'userIdAry[]', 'delete.php');
}

function goDelete() {
	$('input#actionType').val('all');
	checkRemove(document.getElementById('form1'), 'userIdAry[]', 'delete.php');
}

function goUploadSignature() {
	window.location = 'upload.php?actionType=signature';
}

function goUploadSchoolChop() {
	window.location = 'upload.php?actionType=schoolChop';
}
</script>

<br />
<form id="form1" name="form1" method="post" action="index.php">
	<div class="content_top_tool">
		<div class="Conntent_tool">
	    	<?=$htmlAry['toolbar']?>
		</div>
        <?=$htmlAry['searchBox']?>
		<br style="clear:both" />
	</div>
	
	<div class="table_board">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td valign="bottom"><?=$htmlAry['filterSignature']?><?=$htmlAry['filterSchoolChop']?></td>
			<td valign="bottom"><div class="common_table_tool"><?=$deleteBtn?></div></td>
		</tr>
		</table>
		<?=$htmlAry['dbTableBtn']?>
		<?=$htmlAry['dbTable']?>
	</div>
	
	<input type="hidden" id="actionType" name="actionType" value="" />
</form>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>