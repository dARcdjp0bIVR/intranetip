<?php
// Using: Bill

/********************************************************
 * Modification log
 * 	2017-07-07	(Bill)	[2016-0822-1721-00164]
 * 	- to hide School Chop when $eRCTemplateSetting['Settings']['TeacherExtraInfo_HideSchoolChop'] is set
 * ******************************************************/
 
$PageRight = "ADMIN";
 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_teacherExtraInfo.php");

$lreportcard = new libreportcardcustom();
$lreportcard_teacherExtraInfo = new libreportcard_teacherExtraInfo();
$lreportcard->hasAccessRight();

### Get Parameters
$teacherId = $_POST['teacherId'];
$teacherTitle = standardizeFormPostValue($_POST['teacherTitle']);
$signatureFileLocation = $_FILES["signatureFile"]["tmp_name"];
$signatureFileName = standardizeFormPostValue($_FILES["signatureFile"]["name"]);
$signatureFileError = $_FILES["signatureFile"]["error"];
$schoolChopFileLocation = $_FILES["schoolChopFile"]["tmp_name"];
$schoolChopFileName = standardizeFormPostValue($_FILES["schoolChopFile"]["name"]);
$schoolChopFileError = $_FILES["schoolChopFile"]["error"];

### Get Teacher Signature Data
$extraInfoAry = $lreportcard_teacherExtraInfo->returnInfoByTeacherId($teacherId);
$recordId = $extraInfoAry[0]['RecordID'];

$successAry = array();
$lreportcard->Start_Trans();

### Save Title
$extraInfoObj = new libreportcard_teacherExtraInfo($recordId);
$extraInfoObj->setUserId($teacherId);
$extraInfoObj->setTeacherTitle($teacherTitle);
$recordId = $extraInfoObj->save();
$successAry['saveTitle'] = ($recordId > 0)? true : false;

### Handle Images
if ($signatureFileError == 0) {
	$successAry['deleteSignature'] = $extraInfoObj->deleteSignatureImage();
	$successAry['uploadSignature'] = $extraInfoObj->uploadSignatureImage($signatureFileLocation, $signatureFileName);
}
if ($schoolChopFileError == 0 && !$eRCTemplateSetting['Settings']['TeacherExtraInfo_HideSchoolChop']) {
	$successAry['deleteSchoolChop'] = $extraInfoObj->deleteSchoolChopImage();
	$successAry['uploadSchoolChop'] = $extraInfoObj->uploadSchoolChopImage($schoolChopFileLocation, $schoolChopFileName);
}

if (in_array(false, $successAry)) {
	$lreportcard->Rollback_Trans();
	$returnMsgKey = 'UpdateUnSuccess';
}
else {
	$lreportcard->Commit_Trans();
	$returnMsgKey = 'UpdateSuccess';
}

intranet_closedb();
header("Location: index.php?returnMsgKey=$returnMsgKey");
?>