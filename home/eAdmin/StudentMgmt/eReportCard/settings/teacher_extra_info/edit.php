<?php
// Using: Bill

/********************************************************
 * Modification log
 * 	2017-07-07	(Bill)	[2016-0822-1721-00164]
 * 	- to hide School Chop when $eRCTemplateSetting['Settings']['TeacherExtraInfo_HideSchoolChop'] is set
 * ******************************************************/
 
$PageRight = "ADMIN";

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_teacherExtraInfo.php");

$lreportcard = new libreportcardcustom();
$lreportcard_teacherExtraInfo = new libreportcard_teacherExtraInfo();
$linterface = new interface_html();

$lreportcard->hasAccessRight();

if (isset($_GET['teacherId'])) {
	$teacherId = $_GET['teacherId'];
}
else {
	$teacherId = $_POST['userIdAry'][0];
}
$userObj = new libuser($teacherId);

$signatureInfoAry = $lreportcard_teacherExtraInfo->returnInfoByTeacherId($teacherId);
$recordId = $signatureInfoAry[0]['RecordID'];
$extraInfoObj = new libreportcard_teacherExtraInfo($recordId);

$CurrentPage = "Settings_TeacherExtraInfo";	
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();

$TAGS_OBJ = array();
$TAGS_OBJ[] = array($eReportCard['Settings_TeacherExtraInfo']);

$linterface->LAYOUT_START();

$PAGE_NAVIGATION = array();
$PAGE_NAVIGATION[] = array($eReportCard['Settings_TeacherSignature'], "javascript:goBack();");
$PAGE_NAVIGATION[] = array($Lang['Btn']['Edit'], "");
$htmlAry['navigation'] = $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION);

$x = '';
$x .= '<table class="form_table_v30">'."\n";
	// Teacher Name
	$x .= '<tr>'."\n";
		$x .= '<td class="field_title">'.$Lang['AccountMgmt']['UserName'].'</td>'."\n";
		$x .= '<td>'.Get_Lang_Selection($userObj->ChineseName, $userObj->EnglishName).'</td>'."\n";
	$x .= '</tr>'."\n";
	
	// Job Title
	$x .= '<tr>'."\n";
		$x .= '<td class="field_title">'.$eReportCard['JobTitle'].'</td>'."\n";
		$x .= '<td>'.$linterface->GET_TEXTBOX('teacherTitleTb', 'teacherTitle', $extraInfoObj->getTeacherTitle(), $OtherClass='', array('maxlength' => 255)).'</td>'."\n";
	$x .= '</tr>'."\n";
	
	// Signature
	$x .= '<tr>'."\n";
		$x .= '<td class="field_title">'.$Lang['AccountMgmt']['Signature'].'</td>'."\n";
		$x .= '<td>'."\n";
			if ($extraInfoObj->getSignaturePath() != '') {
				$x .= $extraInfoObj->returnSignatureImage()."\n";
				$x .= '<br />'."\n";
			}
			$x .= '<input type="file" id="signatureFile" name="signatureFile" />'."\n";
			$x .= $linterface->Get_Form_Warning_Msg('signatureFileTypeWarningDiv', $Lang['eReportCard']['SettingsArr']['TeacherExtraInfoArr']['SignatureFileTypeWarning'], $Class='warningDiv');
		$x .= '</td>'."\n";
	$x .= '</tr>'."\n";
	
	// School Chop
	if(!$eRCTemplateSetting['Settings']['TeacherExtraInfo_HideSchoolChop']) {
		$x .= '<tr>'."\n";
			$x .= '<td class="field_title">'.$eReportCard['Template']['SchoolChop'].'</td>'."\n";
			$x .= '<td>'."\n";
				if ($extraInfoObj->getSchoolChopPath() != '') {
					$x .= $extraInfoObj->returnSchoolChopImage()."\n";
					$x .= '<br />'."\n";
				}
				$x .= '<input type="file" id="schoolChopFile" name="schoolChopFile" />'."\n";
				$x .= $linterface->Get_Form_Warning_Msg('schoolChopFileTypeWarningDiv', $Lang['eReportCard']['SettingsArr']['TeacherExtraInfoArr']['SchoolChopFileTypeWarning'], $Class='warningDiv');
			$x .= '</td>'."\n";
		$x .= '</tr>'."\n";
	}
$x .= '</table>'."\n";
$htmlAry['editTable'] = $x;

### Buttons
$htmlAry['submitBtn'] = $linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "button", "goSubmit();");
$htmlAry['cancelBtn'] = $linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "goBack();");

?>
<script language="javascript">
function goBack() {
	window.location = 'index.php';
}

function goSubmit() {
	var imageExtAry = new Array('.gif', '.jpg', '.jpeg', '.png', '.bmp');
	var signatureExt = getFileExtension(document.getElementById('signatureFile')).toLowerCase();
	if(document.getElementById('schoolChopFile'))
		var schoolChopExt = getFileExtension(document.getElementById('schoolChopFile')).toLowerCase();
	
	var canSubmit = true;
	$('div.warningDiv').hide();
	if (signatureExt != '' && !jIN_ARRAY(imageExtAry, signatureExt)) {
		$('div#signatureFileTypeWarningDiv').show();
		$('input#signatureFile').focus();
		
		canSubmit = false;
	}
	
	if (document.getElementById('schoolChopFile') && schoolChopExt != '' && !jIN_ARRAY(imageExtAry, schoolChopExt)) {
		$('div#schoolChopFileTypeWarningDiv').show();
		$('input#schoolChopFile').focus();
		
		canSubmit = false;
	}
	
	if (canSubmit) {
		$('form#form1').attr('action', 'edit_update.php').submit();
	}
}
</script>

<br />
<form id="form1" name="form1" method="post" enctype="multipart/form-data">
	<?=$htmlAry['navigation']?>
	<br />
	
	<?=$htmlAry['editTable']?>
	
	<div class="edit_bottom_v30">
		<?=$htmlAry['submitBtn']?>
		<?=$htmlAry['cancelBtn']?>
	</div>
	
	<input type="hidden" id="teacherId" name="teacherId" value="<?=$teacherId?>" />
</form>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>