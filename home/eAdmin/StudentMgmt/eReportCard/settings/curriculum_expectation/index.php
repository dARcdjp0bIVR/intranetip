<?php
/***********************************************
 * modification
 *      20190117 Bill:
 *          fixed PHP 5.4 error message
 * 		20100105 Marcus:
 * 			fixed a bug - the semID were lost after array_merge. 
 * *********************************************/

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") {
	include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
	$lreportcard = new libreportcardcustom();
}
else {
	$lreportcard = new libreportcard();
}

if (!$plugin['ReportCard2008'] || !$lreportcard->hasAccessRight()) 
{
	intranet_closedb();
    
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$linterface = new interface_html();
$CurrentPage = "Curriculum_Expectation";
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();

# Tag information
$TAGS_OBJ[] = array($eReportCard['Settings_CurriculumExpectation']);
$linterface->LAYOUT_START();

# Term filter
$semesters = $lreportcard->GET_ALL_SEMESTERS("-1", false);
$all_semester_status = $eReportCard['AllSemesters'];

//fixed by Marcus 20100105 - the keys will be rearranged after array_merge, so the semIDs will be lost.
//$semesters = array_merge(array("0"=>$eReportCard['WholeYear']), $semesters); 
$semesters[0] = $eReportCard['WholeYear'];
if ($semester == "") {
	$semester = "0";
}

$select_semester = "<select onchange='this.form.submit()' name='semester'>";
foreach ($semesters as $key => $value) {
	$select_semester .= "<option value='$key'".("$semester"==="$key"?" selected":"").">$value</option>";
}
$select_semester .= "</select>";

$sql = "
	SELECT 
		CONCAT(\"<a href='edit.php?ClassLevelID=\", a.YearID, \"&TermID=$semester' class='tablelink'>\", a.YearName, \"</a>\"),
		MAX(b.DateModified)
	FROM 
		YEAR as a 
		LEFT JOIN ". $lreportcard->DBName .".RC_CURRICULUM_EXPECTATION as b ON (a.YearID = b.ClassLevelID AND b.TermID = '".$semester."')
	GROUP BY 
		a.YearID
";

$li = new libdbtable2007(0, 1, 0);
$li->field_array = array("a.YearName");

$li->sql = $sql;
$li->no_col = 2;
$li->IsColOff = "CurriculumExpectationList";
$li->column_list .= "<td class='tabletop tabletopnolink' width='70%'>".$eReportCard['Form']."</td>\n";
$li->column_list .= "<td class='tabletop tabletopnolink' width='30%'>".$eReportCard['LastModifiedDate']."</td>\n";
?>

<br />
<form name="form1" method="get">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td align="center">
		<table width="96%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td align="left" class="tabletext">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td><?=$select_semester?></td>
					<td align="right" height='30'><?=$linterface->GET_SYS_MSG($xmsg, $xmsg2);?></td>
				</tr>
				<tr>
					<td colspan="2"><?=$li->display();?></td>
				</tr>
				</table>
			</td>
		</tr>
		</table>
	</td>
</tr>
</table>
<br />
</form>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>