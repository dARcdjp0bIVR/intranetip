<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");

intranet_auth();
intranet_opendb();

if ($plugin['ReportCard2008']) {
	if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
	{
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
		$lreportcard = new libreportcardcustom();
	} else {
		$lreportcard = new libreportcard();
	}
	
	if ($lreportcard->hasAccessRight()) {
		
		$ClassLevelID = $_GET['ClassLevelID'];
		$TermID = $_GET['TermID'];
		$IsImportSample = $_GET['IsImportSample'];
		
		
		### Get Course Description
		if ($IsImportSample == 1)
			$Content = array();
		else
			$Content = $lreportcard->returnCurriculumExpectation($ClassLevelID, '', $TermID);
		
		### Get Subject Code
		$SubjectCodeMapping = $lreportcard->GET_SUBJECTS_CODEID_MAP($withComponent=1);
		
		# Build the export array
		$exportContent = array();
		$MainSubjectArray = $lreportcard->returnSubjectwOrder($ClassLevelID);
		if(!empty($MainSubjectArray))
		{
			foreach($MainSubjectArray as $MainSubjectID => $MainSubjectNameArray)
			{
				foreach ($MainSubjectNameArray as $ComponentSubjectID => $SubjectNameBiLing)
				{
					if ($ComponentSubjectID == 0)
						$thisSubjectID = $MainSubjectID;
					else
						$thisSubjectID = $ComponentSubjectID;
					
					$thisSubjectName = $lreportcard->GET_SUBJECT_NAME_LANG($thisSubjectID);
					$thisCode = $SubjectCodeMapping[$thisSubjectID]['CODEID'];
					$thisCmpCode = $SubjectCodeMapping[$thisSubjectID]['CMP_CODEID'];
					$thisDescription = str_replace("\r\n", ' ', $Content[$thisSubjectID]);
					
					if ($IsImportSample == 1)
						$exportContent[] = array($thisCode, $thisCmpCode, $thisDescription);
					else
						$exportContent[] = array($thisCode, $thisCmpCode, $thisSubjectName, $thisDescription);
				}
			}
		}
		
		# Overall 
		$thisDescription = str_replace("\r\n", ' ', $Content[0]);
		if ($IsImportSample == 1)
			$exportContent[] = array("OVERALL", '', $thisDescription);
		else
			$exportContent[] = array("OVERALL", '', $eReportCard['CourseDescription']['Overall'], $thisDescription);
		
		$lexport = new libexporttext();
		$exportColumn = array();
		$exportColumn[] = 'SubjectCode';
		$exportColumn[] = 'SubjectComponentCode';
		if ($IsImportSample != 1)
			$exportColumn[] = 'SubjectName';
		$exportColumn[] = 'Description';
		
		$export_content = $lexport->GET_EXPORT_TXT($exportContent, $exportColumn);
		
		if ($IsImportSample == 1)
		{
			$filename = 'course_description_sample.csv';
		}
		else
		{
			include_once($PATH_WRT_ROOT."includes/libclass.php");
			$lclass = new libclass();
			$LevelName = str_replace(' ', '_', $lclass->getLevelName($ClassLevelID));
			$filename = 'course_description_'.$LevelName.'.csv';
		}
		
		intranet_closedb();
		$lexport->EXPORT_FILE($filename, $export_content);
	} else {
		echo "You have no priviledge to access this page.";
	}
} else {
	echo "You have no priviledge to access this page.";
}
?>