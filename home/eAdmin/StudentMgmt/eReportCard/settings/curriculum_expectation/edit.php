<?php
/*******************************************
 * Modification log
 * 20100105 Marcus:
 * 		added "Apply to other terms" option.
 * ******************************************/

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");

intranet_auth();
intranet_opendb();


if(!$plugin['ReportCard2008'])
{
	intranet_closedb();
	header("Location: index.php");
}
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
{
	include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
	$lreportcard = new libreportcardcustom();
} else {
	$lreportcard = new libreportcard();
}

if (!$lreportcard->hasAccessRight()) 
{
	header ("Location: /");
	intranet_closedb();
	exit();
}

$linterface = new interface_html();
$lclass = new libclass();

$CurrentPage = "Curriculum_Expectation";
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();

# tag information
$TAGS_OBJ[] = array($eReportCard['Settings_CurriculumExpectation']);
$linterface->LAYOUT_START();

$TermID = $_GET['TermID'];
$LevelName = $lclass->getLevelName($ClassLevelID);

$PAGE_NAVIGATION[] = array($eReportCard['Settings_CurriculumExpectation']);
$PAGE_NAVIGATION[] = array($LevelName);


# Retrieve Data
$Content = $lreportcard->returnCurriculumExpectation($ClassLevelID, '', $TermID);

# Build the form
$x = "";
$MainSubjectArray = $lreportcard->returnSubjectwOrder($ClassLevelID);
if(!empty($MainSubjectArray))
{
	foreach($MainSubjectArray as $MainSubjectID => $MainSubjectNameArray)
	{
		/*
		$sname = $lreportcard->GET_SUBJECT_NAME_LANG($MainSubjectID);
		
		$x .= "<tr valign='top'>";
		$x .= "<td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'><span class='tabletext'>$sname</span></td>";
		$x .= "<td class='tabletext'>". $linterface->GET_TEXTAREA("Content_".$MainSubjectID, $Content[$MainSubjectID])."</td>";
		$x .= "</tr>";
		*/
		
		### Display Subject Component also (Ivan 20091119)
		foreach ($MainSubjectNameArray as $ComponentSubjectID => $SubjectNameBiLing)
		{
			if ($ComponentSubjectID == 0)
			{
				$thisSubjectID = $MainSubjectID;
				$Prefix = '';
			}
			else
			{
				$thisSubjectID = $ComponentSubjectID;
				$Prefix = '&nbsp;&nbsp;&nbsp;&nbsp;';
			}
			$sname = $lreportcard->GET_SUBJECT_NAME_LANG($thisSubjectID);
				
			$x .= "<tr valign='top'>";
				$x .= "<td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'><span class='tabletext'>".$Prefix.$sname."</span></td>";
//				$x .= "<td class='tabletext'>". $linterface->GET_TEXTAREA("Content_".$thisSubjectID, $Content[$thisSubjectID])."</td>";
				$x .= "<td class='tabletext'>". $linterface->GET_TEXTAREA("Content[".$thisSubjectID."]", $Content[$thisSubjectID])."</td>";
			$x .= "</tr>";
		}
	}
}
//else
//{
//	$x .= "<tr valign='top'>";
//	$x .= "<td colspan='2' valign='top' nowrap='nowrap' class='tabletext' align='center'>". $eReportCard['NoSubjectSettings'] ."</td>";
//	$x .= "</tr>";
//}

	# Overall
	$x .= "<tr valign='top'>";
		$x .= "<td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'><span class='tabletext'>".$eReportCard['CourseDescription']['Overall']."</span></td>";
//		$x .= "<td class='tabletext'>". $linterface->GET_TEXTAREA("Content_0", $Content[0])."</td>";
		$x .= "<td class='tabletext'>". $linterface->GET_TEXTAREA("Content[0]", $Content[0])."</td>";
	$x .= "</tr>";
	
$x .= "<tr valign='top'>";
	$x .= "<td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'><span class='tabletext'>".$eReportCard['ApplyOtherTerm']."</span></td>";
	$x .= "<td class='tabletext'><input type='checkbox' name='applytootherterms' value='1'></td>";
$x .= "</tr>";


$parameters = '?ClassLevelID='.$ClassLevelID.'&TermID='.$TermID;
$toolbar = '';
$toolbar .= $linterface->GET_LNK_IMPORT("import.php".$parameters, '', '', '', '', 0);
$toolbar .= toolBarSpacer();
$toolbar .= $linterface->GET_LNK_EXPORT("export.php".$parameters, '', '', '', '', 0);

?>

<br />   
<form name="form1" method="post" action="edit_update.php">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	<td align='right'><?=$linterface->GET_SYS_MSG($xmsg);?></td>
</tr>

<tr>
	<td colspan="2">
		<table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr><td><?= $toolbar ?></td></tr>
		<tr> 
			<td>
				<br />
				<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
					<?=$x?>
				</table>
			</td>
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td colspan="2">        
		<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr>
			<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
		</tr>
		<tr>
			<td align="center">
				<? if(!empty($MainSubjectArray)) {?>
				<?= $linterface->GET_ACTION_BTN($button_submit, "submit") ?>
				<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2") ?>
				<? } ?>
				<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='index.php?semester=".$TermID."'") ?>
			</td>
		</tr>
		</table>                                
	</td>
</tr>

</table>                        
<br />

<input type="hidden" name="ClassLevelID" value="<?=$ClassLevelID?>" />
<input type="hidden" name="TermID" value="<?=$TermID?>" />

</form>


<?
print $linterface->FOCUS_ON_LOAD("form1.NoticeNumber");
$linterface->LAYOUT_STOP();
intranet_closedb();
?>