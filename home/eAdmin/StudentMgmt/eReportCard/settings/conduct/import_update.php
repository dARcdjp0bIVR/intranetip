<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_extrainfo.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");

intranet_auth();
intranet_opendb();

if ($plugin['ReportCard2008']) {
	$lreportcard = new libreportcard();
	
	if ($lreportcard->hasAccessRight()) {
		$limport = new libimporttext();
		$lreportcard_extrainfo = new libreportcard_extrainfo();

		$format_array = array("Conduct");
		
		$li = new libdb();
		$filepath = $userfile;
		$filename = $userfile_name;
		
		$ConductArr = $lreportcard_extrainfo->Get_Conduct();
		$ConductArr = Get_Array_By_Key($ConductArr, "Conduct");
		
		if($filepath=="none" || $filepath == ""){          # import failed
		    header("Location: import.php?Msg=ImportUnsuccess");
		    exit();
		} 
		else if(!$limport->CHECK_FILE_EXT($filename)) 
		{
			header("Location: import.php?Msg=ImportUnsuccess");
			exit();
		}
		
		
		# read file into array
		# return 0 if fail, return csv array if success
		$data = $limport->GET_IMPORT_TXT($filepath);
		#$toprow = array_shift($data);                   # drop the title bar
		
		$limport->SET_CORE_HEADER($format_array);
		if (!$limport->VALIDATE_HEADER($data, true) || !$limport->VALIDATE_DATA($data)) {
			header("Location: import.php?Msg=WrongCSVHeader");
			exit();
		}
		array_shift($data);
	    $values = "";
	    $delim = "";
		$successCount = 0;
		$failCount = 0;
		$csvCodeList = array();
	    for ($i=0; $i<sizeof($data); $i++) {
			if (empty($data[$i])) continue;
			
			if(!in_array($data[$i][0], $ConductArr))
			{
				$InsertConductArr[] = $data[$i][0];
			}
	    }
	    
	    if(count($InsertConductArr)>0)
	    {
	    	$lreportcard_extrainfo->Start_Trans();
	    	$Success = $lreportcard_extrainfo->Insert_Conduct($InsertConductArr);
	    	
	    	if($Success)
	    		$lreportcard_extrainfo->Commit_Trans();
	    	else
	    		$lreportcard_extrainfo->RollBack_Trans();
	    }
	    else
	    	$Success = true;

		if($Success)
		{
			$Msg = "ImportSuccess";
		}
		else
		{
			$Msg = "ImportUnsuccess";
		}
	    		
		intranet_closedb();
		
		header("Location: index.php?Msg=$Msg");
	} else {
		echo "You have no priviledge to access this page.";
	}
} else {
	echo "You have no priviledge to access this page.";
}
?>