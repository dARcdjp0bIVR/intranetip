<?php
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

$PageRight = "ADMIN";

if (!$plugin['ReportCard2008']) {
	intranet_closedb();
	header("Location:/index.php");
}

if (!isset($_POST))
	header("Location:index.php");

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_extrainfo.php");
$lreportcard = new libreportcard();
$lreportcard_extrainfo = new libreportcard_extrainfo();

if (!$lreportcard->hasAccessRight()) {
	intranet_closedb();
	header("Location:/index.php");
}

$success = $lreportcard_extrainfo->Delete_Conduct($ConductID);
$Msg = $success?"DeleteSuccess":"DeleteUnsuccess";

intranet_closedb();

header("Location:index.php?Msg=$Msg");
?>

