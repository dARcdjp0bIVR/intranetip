<?php
// Modifying by: Bill

/***************************************************
 * 	Modification log:
 * 20160630 Bill:	[2015-1104-1130-08164]
 * 		- Correct highlighted page in module menu
 * *************************************************/
 
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if ($plugin['ReportCard2008'])
{
	include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
	if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
	{
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
		$lreportcard = new libreportcardcustom();
	} else {
		$lreportcard = new libreportcard();
	}
	
	# Get http variable
	$IsAdditional = (!isset($_GET['IsAdditional']) || $_GET['IsAdditional']=='')? 0 : $_GET['IsAdditional'];
	
	$CurrentPage = "Settings_Conduct";
	$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
	
	if ($lreportcard->hasAccessRight()) {

		include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
		
		$linterface = new interface_html();
		$lreportcard_ui = new libreportcard_ui();
		
		# tag information
		$TAGS_OBJ = array();
		$TAGS_OBJ[] = array($eReportCard['Conduct']);
		
		$linterface->LAYOUT_START();
		
		echo $lreportcard_ui->Get_Setting_Conduct_Import_UI();
?>
<script type="text/JavaScript" language="JavaScript">
function checkForm() {
	$("div.WarnMsg").hide();
	obj = document.form1;
	if (obj.elements["userfile"].value == "") {
		 $("div#WarnEmptyUserFile").show();
		 obj.elements["userfile"].focus();
		 return false;
	}
	obj.submit();
}
</script>

<?
	print $linterface->FOCUS_ON_LOAD("form1.UserFile");
  	$linterface->LAYOUT_STOP();
  } else {
?>
You have no priviledge to access this page.
<?
	}
} else {
?>
You have no priviledge to access this page.
<?
}
?>
