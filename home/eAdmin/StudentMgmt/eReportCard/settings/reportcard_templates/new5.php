<?php

/**************************************************
 * 	Modification log
 * 	20150814 Bill:	[2015-0413-1359-48164]
 * 		- hide 2 ratio steps for BIBA ES Report
 * 	20150519 Bill
 * 		- BIBA Cust
 * 		- display PDF report preview in iframe
 * ***********************************************/
 
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if (!$plugin['ReportCard2008'])
{
	header ("Location: /");
	intranet_closedb();
	exit();
}

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
include_once($PATH_WRT_ROOT."includes/eRCConfig.php");
$lreportcard = new libreportcard();
$lreportcard_ui = new libreportcard_ui();


if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
{
	include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
	$lreportcard = new libreportcardcustom();
} else {
	$lreportcard = new libreportcard();
}

$linterface = new interface_html();
$lclass = new libclass();

if (!$lreportcard->hasAccessRight()) 
{
	header ("Location: /");
	intranet_closedb();
	exit();
}

$CurrentPage = "Settings_ReportCardTemplates";
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();

# Loading Report Template Data
$basic_data = $lreportcard->returnReportTemplateBasicInfo($ReportID);	// Basic Data
$ClassLevelID = $basic_data['ClassLevelID'];
$form_num = $lreportcard->GET_FORM_NUMBER($ClassLevelID);
if (is_numeric($form_num)) {
	$form_num = intval($form_num);
	$isESReport = $form_num > 0 && $form_num < 6;
}
// KG Report
else {
	$isKGReport = true;
}
$ReportType = $basic_data['Semester'];


# tag information
$TAGS_OBJ[] = array($eReportCard['Settings_ReportCardTemplates'], "", 0);

$linterface->LAYOUT_START();
$cal = $lreportcard->returnReportTemplateCalculation($ReportID);
$STEPS_OBJ[] = array($eReportCard['InputBasicInformation'], 0);

if($eRCTemplateSetting['Settings']['AutoSelectReportType'] && ($isESReport || $isKGReport))	# BIBA ES Report
{
	// do nothing - hide 2 step objects
}
else if($ReportType<>"F")	# Term Report type
{
	if($cal==1)
	{
		$STEPS_OBJ[] = array($eReportCard['InputAssessmentRatio'], 0);
		$STEPS_OBJ[] = array($eReportCard['InputOverallSubjectWeight'], 0);	
	}
	else
	{
		$STEPS_OBJ[] = array($eReportCard['InputSubjectWeight'], 0);
		$STEPS_OBJ[] = array($eReportCard['InputAssessmentRatio'], 0);	
	}
}
else					# Whole year report
{
	if($cal==1)
	{
		$STEPS_OBJ[] = array($eReportCard['InputTermsRatio'], 0);
		$STEPS_OBJ[] = array($eReportCard['InputOverallSubjectWeight'], 0);	
	}
	else
	{
		$STEPS_OBJ[] = array($eReportCard['InputSubjectWeight'], 0);
		$STEPS_OBJ[] = array($eReportCard['InputTermsRatio'], 0);	
	}
}
$STEPS_OBJ[] = array($eReportCard['InputReportDisplayInformation'], 0);
$STEPS_OBJ[] = array($button_preview, 1);
#$STEPS_OBJ[] = array($eReportCard['Finishandpreview'], 1);


?>

<script language="javascript">

function js_Go_Back() {
	window.location='index.php';
}

</script>

<br />
<form name="form1" method="post">


<table width="96%" border="0" cellspacing="0" cellpadding="4">
<tr>
	<td>
		<?= $lreportcard_ui->Get_Settings_ReportCardTemplate_Nav() ?>
		<br />
		<?= $linterface->GET_STEPS($STEPS_OBJ) ?>   
		
		<br />
		<?= $lreportcard_ui->Get_Settings_ReportCardTemplate_ReportCardInfo_Table($ReportID) ?>
		<br />
		
		<?php if($eRCTemplateSetting['Report']['ExportPDFReport']){ ?>
			<iframe src="../../reports/generate_reports/print_preview_by_pdf.php?ReportID=<?=$ReportID?>&PreviewReport=1" height="655px" width="100%"></iframe>
		<?php } else {
		# Get the eRC Template Settings
		if($ReportCardCustomSchoolName != 'sis'){
			$eRCtemplate = $lreportcard->getCusTemplate();
		}else{
			# SIS : Follow PMS Style 
			$LAYOUT_SKIN = '2007a';
		}	
		$eRCtemplate = empty($eRCtemplate) ? $lreportcard : $eRCtemplate;
		$eRtemplateCSS = $lreportcard->getTemplateCSS();
		if($eRCTemplateSetting['CusTemplate'])	$ReportCardCustomSchoolName = "sis";
		
		if($ReportCardCustomSchoolName != "st_stephen_college" && $ReportCardCustomSchoolName != 'hkuga_college')
		{
			if ($ReportCardCustomSchoolName == "escola_tong_nam")
			{
				# Different school title banners (Kindergarten/Primary/Secondary) with different classes
				# So, have to pass studentID as well to determind the use of school title banner
				$TitleTable = $eRCtemplate->getReportHeader($ReportID, $StudentID);
			}
			else
			{
				$TitleTable = $eRCtemplate->getReportHeader($ReportID);
			}
			
			$StudentInfoTable = $eRCtemplate->getReportStudentInfo($ReportID, $StudentID);
			$MSTable = $eRCtemplate->getMSTable($ReportID, $StudentID);
			$MiscTable = $eRCtemplate->getMiscTable($ReportID, $StudentID);
			# 20140723 Sis SignatureTable
			if($ReportCardCustomSchoolName == 'sis'){
				$SignatureTable = $eRCtemplate->getSignatureTable($eRCTemplateSetting['Signature']);
			}
			else 
				$SignatureTable = $eRCtemplate->getSignatureTable($ReportID, $StudentID);
			$FooterRow = $eRCtemplate->getFooter($ReportID);
		}
		?>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link href="<?=$PATH_WRT_ROOT?>templates/<?=$LAYOUT_SKIN?>/css/print.css" rel="stylesheet" type="text/css" />
		<? if($eRtemplateCSS) {?><link href="<?=$eRtemplateCSS?>" rel="stylesheet" type="text/css" /><? } ?>
		<br />
		
		<br />
		<table border="0" cellpadding="0" cellspacing="0" align="center" width="90%">
		<tr>
		<td align="center">
			<div id="container">
				<table width="100%" border="0" cellpadding="02" cellspacing="0" valign="top" <? if($s<sizeof($StudentIDAry)-1) {?>style="page-break-after:always"<? } ?>>
				<?
				if($ReportCardCustomSchoolName == "st_stephen_college")
				{
					$x = '';
					$x .= $eRCtemplate->FirstPage($ReportID, $StudentID);
					$x .= $eRCtemplate->getSubjectPages($ReportID, $StudentID);
							
					# Subejct Pages
					//style="page-break-before:always"
//					$ReportSetting = $eRCtemplate->returnReportTemplateBasicInfo($ReportID);
//					$ClassLevelID = $ReportSetting['ClassLevelID'];
//			 		$MainSubjectArray = $eRCtemplate->returnSubjectwOrder($ClassLevelID);
//			 		foreach($MainSubjectArray as $SubjectID => $SubejctData)
//			 		{
//						$x .= $eRCtemplate->SubjectPage($ReportID, $SubjectID, $StudentID);		
//			 		}
			
					# OLE Page
					
					echo $x;
				}
				else if($ReportCardCustomSchoolName == "carmel_alison")
				{
					$Layout = $eRCtemplate->getLayout($TitleTable, $StudentInfoTable, $MSTable, $MiscTable, $SignatureTable, $FooterRow, $ReportID, $StudentID);
					echo $Layout[0];
					echo $Layout[1];
				}
				else
				{
					if (method_exists($eRCtemplate, "getLayout")) {
						echo $eRCtemplate->getLayout($TitleTable, $StudentInfoTable, $MSTable, $MiscTable, $SignatureTable, $FooterRow, $ReportID, $StudentID);
					} else {
					?>
						<tr><td><?=$TitleTable?></td><tr>
						<tr><td><?=$StudentInfoTable?></td><tr>
						<tr><td><?=$MSTable?></td><tr>
						<tr><td><?=$MiscTable?></td><tr>
						<tr><td><?=$SignatureTable?></td><tr>
						<?=$FooterRow?>
					<?
					}
				}
				?>
				</table>
			</div>
		</td>
		</tr>
		
		</table>
		<?php } ?>
	</td>
</tr>
</table>
<br />

<table width="96%" border="0" cellspacing="0" cellpadding="5">
  <tr>
    <td height="1" class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
  </tr>
  <tr>
    <td align="center">
		<?= $linterface->GET_ACTION_BTN($button_finish, "button", "window.location='index.php'") ?>
		<?= $linterface->GET_ACTION_BTN($button_back, "button", "window.location='new4.php?ReportID=$ReportID'") ?>
	
	</td>
  </tr>
</table>

</form>

<br />
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
