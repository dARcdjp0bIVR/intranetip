<?php
# using: 

################# Change Log [Start] ############
#
#	Date:	2017-09-15	Bill	[2017-0914-0952-23066]
#			hide skip button when edit incomplete BIBA ES / KG Report
#
#	Date:	2015-12-01	Ivan	[J89803] [ip.2.5.7.1.1]
#			improved: ajax_yahoo.js and ajax_connection.js change to internal relative path
#
#	Date:	2015-11-15	Bill	[2015-1111-1214-16164]
#			support BIBA KG Semester Report
#
#	Date:	2015-10-19	Bill
#			support BIBA - 4 School Terms
#
#	Date:	2015-10-15	Bill	[2015-1013-1434-16164]
#			support BIBA KG Report
#
#	Date:	2015-08-17	Bill	[2015-0413-1359-48164]
#			hide 2 ratio steps for BIBA ES Report
#			use Qx for BIBA Progress Report in Term selection
#
#	Date:	2015-07-21	Bill	[2015-0413-1359-48164]
#			modified js function checkForm(), added js function js_updateReportList(), to support BIBA preset report type
#
################# Change Log [End] ##############

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if (!$plugin['ReportCard2008'])
{
	header ("Location: /");
	intranet_closedb();
	exit();
}

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");

include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
$lreportcard = new libreportcard();
$lreportcard_ui = new libreportcard_ui();
$linterface = new interface_html();


if (!$lreportcard->hasAccessRight()) 
{
	header ("Location: /");
	intranet_closedb();
	exit();
}

$CurrentPage = "Settings_ReportCardTemplates";
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();

$cal = $lreportcard->returnReportTemplateCalculation($ReportID);

// Retrieve Basic Information data (Edit Mode)
if ($ReportID=='') {
	$data = array();
}
else {
	$data = $lreportcard->returnReportTemplateBasicInfo($ReportID);
	$IsReportCompleted = $lreportcard->isReportTemplateComplete($ReportID);
}
list($report_title1, $report_title2) = explode(":_:", $data['ReportTitle']);
$isMainReport = $data['isMainReport'];
$TermStartDate = $data['TermStartDate']; 
$TermEndDate = $data['TermEndDate']; 

// Get Semester data
$report_type = $data['Semester']=="F" ? "whole" : "term";

if(!$ReportID)		// Select the report type and terms
{
	// Get Form data
	$FormArr = $lreportcard->GET_ALL_FORMS();
	$FormListSelection = $linterface->GET_SELECTION_BOX($FormArr, "name='ClassLevelID' id='ClassLevelID' onChange='jAJAX_changeForm(this.value);'", "--- Select Form ---", $data['ClassLevelID']);
		
	$SemSelection = $lreportcard->Get_Term_Selection('Semester', '', $SelectedYearTermID='', $OnChange='js_Select_ReportType(\'Term\'); js_Update_Term_Start_End_Date(this.value);', $NoFirst=1, $NoPastTerm=0, $withWholeYear=0);
	
	$activeYearID = $lreportcard->GET_ACTIVE_YEAR_ID();
	include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
	$obj_AcademicYear = new academic_year($activeYearID);
	$sem_ary = $obj_AcademicYear->Get_Term_List(1);
	$SemCheckedTable = "<table border='0' cellspacing='0' cellpadding='0'>";
	$SemCheckedTable .= "<tr><td class='tabletext'><input type='checkbox' name='Sem_all' id='Sem_all' onClick=\"(this.checked)?setChecked(1,this.form,'Sem[]'):setChecked(0,this.form,'Sem[]');js_Select_ReportType('WholeYear');\"> <label for='Sem_all'> ".$button_select_all."</label></td></tr>";
	$si = 0;
	
	foreach($sem_ary as $SemID => $SemName)
	{
		$SemCheckedTable .= !($si%4) ? "<tr>" : "";
		$SemCheckedTable .= "<td class='tabletext'><input type='checkbox' name='Sem[]' value='{$SemID}' id='Sem_".$si."' onClick=\"js_Select_ReportType('WholeYear');\"> <label for='Sem_".$si."'>".$SemName."</label></td>";
		$si++;
		$SemCheckedTable .= !($si%4) ? "</tr>" : "";
	}
	$SemCheckedTable .= "</table>";
	
	$ReportTypeTable = "<table border='0' cellspacing='0' cellpadding='1'>";
	$ReportTypeTable .= "<tr>
							<td><input type=\"radio\" name=\"term_report\" id=\"term_report\" value=\"TermReport\" onclick=\"js_Change_Report_Type('Term');\" ". ($report_type=="term"?"checked":"") ."></td>
							<td>
								<label for=\"term_report\">
									<span class=\"tabletext\">". $eReportCard['TermReport'] ." &nbsp; &nbsp;</span>
								</label>
								".$SemSelection."
							</td>
						</tr>";
	$ReportTypeTable .= "<tr>
							<td></td>
							<td>
								<input type=\"radio\" name=\"is_main_term_report\" id=\"is_main_term_report\" value=\"Main\" checked>
								<label for=\"is_main_term_report\">
									<span class=\"tabletext\">". $eReportCard['Main'] ." &nbsp; &nbsp;</span>
								</label>
								<input type=\"radio\" name=\"is_main_term_report\" id=\"is_extra_term_report\" value=\"Extra\">
								<label for=\"is_extra_term_report\">
									<span class=\"tabletext\">". $eReportCard['Extra'] ." &nbsp; &nbsp;</span>
								</label>
							</td>
						</tr>";
	$ReportTypeTable .= "<tr><td>&nbsp;</td></tr>";
	$ReportTypeTable .= "<tr>
							<td><input type=\"radio\" name=\"term_report\" id=\"wholeyear_report\" value=\"WholeYearReport\" onclick=\"js_Change_Report_Type('Consolidated');\" ". ($report_type=="whole"?"checked":"") ."></td>
							<td><label for=\"wholeyear_report\"><span class=\"tabletext\">". $eReportCard['WholeYearReport'] ." </span></label></td>
						</tr>";
	$ReportTypeTable .= "<tr>
							<td></td>
							<td>". $SemCheckedTable ."</td>
						</tr>
						";
	$ReportTypeTable .= "</table>";
	
	// [2015-0413-1359-48164]
	if($eRCTemplateSetting['Settings']['AutoSelectReportType']){
		// use js function js_updateReportList() when selected form item changed
		$FormListSelection = $linterface->GET_SELECTION_BOX($FormArr, "name='ClassLevelID' id='ClassLevelID' onChange='js_updateReportList(\"sem\");'", "--- Select Form ---", $data['ClassLevelID']);
		
		// drop down list for semester and preset report type
		$TargetTermSelect = "<select name=\"TargetTerm\" id=\"TargetTerm\" onchange=\"js_updateReportList('term');\"><option value=\"\">--- Select Term ---</option></select>";
		$ReportTypeTable = "<select name=\"TargetReport\" id=\"TargetReport\"><option value=\"\">--- Select Report ---</option></select>";
	}
}
else		// View only
{
	$FormListSelection = $lreportcard->returnClassLevel($data['ClassLevelID']) ."<input type='hidden' name='ClassLevelID' id='ClassLevelID' value='". $data['ClassLevelID'] ."'>";
	
	$TermReportType = '';
	if ($data['Semester']!="F")
	{	
		$TermReportType = ($isMainReport==1)? $eReportCard['Main'] : $eReportCard['Extra'];
		$HiddenValue = ($isMainReport==1)? 'Main' : 'Extra';
	}
	
	$SelectedSemAry = $lreportcard->returnReportInvolvedSem($ReportID);
	$SelectedSem = implode(", ", $SelectedSemAry);
	$ReportTypeTable = "<table border='0' cellspacing='0' cellpadding='1'>";
	$ReportTypeTable .= "<tr><td class=\"tabletext\">". ($report_type=="term"? $eReportCard['TermReport'].' ('.$TermReportType.')' : $eReportCard['WholeYearReport'] ) ."</td></tr>";
	$ReportTypeTable .= "<tr><td class=\"tabletext\">{$SelectedSem}</td></tr>";
	$ReportTypeTable .= "<input type=\"hidden\" name=\"Semester\" value=\"". $data['Semester'] ."\">";
	$ReportTypeTable .= "<input type=\"hidden\" name=\"is_main_term_report\" value=\"". $HiddenValue ."\">";
	$ReportTypeTable .= "</table>";
 	
 	// [2015-0413-1359-48164] display report info
 	if($eRCTemplateSetting['Settings']['AutoSelectReportType']){
 		
 		// [2015-1013-1434-16164] form number and report type
		$form_num = $lreportcard->GET_FORM_NUMBER($data['ClassLevelID']);
		// ES / MS Report
		if (is_numeric($form_num)) {
			$form_num = intval($form_num);
			$isESReport = $form_num > 0 && $form_num < 6;
			$isMSReport = !$isESReport;
		}
		// KG Report
		else {
			$isKGReport = true;
		}
		
		// semester sequence
	 	$sem_num = $lreportcard->Get_Semester_Seq_Number($data['Semester']);
	 	list($sem_name, $report_name) = $lreportcard->getBIBAReportName($form_num, $sem_num, $isMainReport, $data['Semester']=="F", $data['ReportSequence'], true);
	 	
	 	// html display and input
		$TargetTermSelect = "$sem_name";
		$ReportTypeTable = "$report_name";
		$ReportTypeTable .= "<input type=\"hidden\" name=\"Semester\" value=\"". $data['Semester'] ."\">";
		$ReportTypeTable .= "<input type=\"hidden\" name=\"is_main_term_report\" value=\"". $HiddenValue ."\">";
		
 	}
}

# tag information
$TAGS_OBJ[] = array($eReportCard['Settings_ReportCardTemplates'], "", 0);

$linterface->LAYOUT_START();

$STEPS_OBJ[] = array($eReportCard['InputBasicInformation'], 1);

// [2015-0413-1359-48164]
if($eRCTemplateSetting['Settings']['AutoSelectReportType'] && ($isESReport || $isKGReport))		# BIBA ES / KG Report
{
	// do nothing - hide 2 step objects
}
else if($ReportType<>"F")	# Term Report type
{
	if($cal==1)
	{
		$STEPS_OBJ[] = array($eReportCard['InputAssessmentRatio'], 0);
		$STEPS_OBJ[] = array($eReportCard['InputOverallSubjectWeight'], 0);	
	}
	else
	{
		$STEPS_OBJ[] = array($eReportCard['InputSubjectWeight'], 0);
		$STEPS_OBJ[] = array($eReportCard['InputAssessmentRatio'], 0);	
	}
	
	$TermDateStyle = '';
}
else					# Whole year report
{
	if($cal==1)
	{
		$STEPS_OBJ[] = array($eReportCard['InputTermsRatio'], 0);
		$STEPS_OBJ[] = array($eReportCard['InputOverallSubjectWeight'], 0);	
	}
	else
	{
		$STEPS_OBJ[] = array($eReportCard['InputSubjectWeight'], 0);
		$STEPS_OBJ[] = array($eReportCard['InputTermsRatio'], 0);	
	}
	
	$TermDateStyle = ' style="display:none" ';
}
$STEPS_OBJ[] = array($eReportCard['InputReportDisplayInformation'], 0);
$STEPS_OBJ[] = array($button_preview, 0);
#$STEPS_OBJ[] = array($eReportCard['Finishandpreview'], 0);

?>
<!--script type="text/javascript" src="http://<?=$eclass_httppath?>/src/includes/js/ajax_yahoo.js" ></script-->
<!--script type="text/javascript" src="http://<?=$eclass_httppath?>/src/includes/js/ajax_connection.js" ></script-->
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/ajax_yahoo.js" ></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/ajax_connection.js" ></script>


<script language="javascript">

<?php
	if($eRCTemplateSetting['Settings']['AutoSelectReportType'] && !$ReportID) {
		echo "var formNumberMap = {};\r\n";
		for($formCount=0; $formCount<count($FormArr); $formCount++){
			$curFormNum = $lreportcard->GET_FORM_NUMBER($FormArr[$formCount]['ClassLevelID']);
			$curFormName = $FormArr[$formCount]['LevelName'];
			
			if(is_numeric($curFormNum)){
				echo "formNumberMap[".$FormArr[$formCount]["ClassLevelID"]."] = $curFormNum;\r\n";
			}
			else{
				echo "formNumberMap[".$FormArr[$formCount]["ClassLevelID"]."] = \"$curFormName\";\r\n";
			}
		}
	}
?>

function js_Go_Back() {
	window.location='index.php';
}

<!--
$(document).ready( function() {	
	<? if ($ReportID == '') { ?>
		var jsCurYearTermID = $('select#Semester').val();
		js_Update_Term_Start_End_Date(jsCurYearTermID);
	<? } ?>
});

function checkForm(obj)
{
	//if(!check_text(obj.report_title, "<?php echo $i_alert_pleasefillin.$eReportCard['ReportTitle']; ?>.")) return false;	
	if(obj.report_title1.value=="" && obj.report_title2.value=="")
	{
		alert("<?php echo $i_alert_pleasefillin.$eReportCard['ReportTitle']; ?>.");
		obj.report_title1.focus();
		return false;
	}
	
	<? if(!$ReportID) {?>
	if(!check_select(obj.ClassLevelID, "<?php echo $i_alert_pleaseselect.$i_ClassLevel; ?>.", "")) 
		return false;	
	
		<? if($eRCTemplateSetting['Settings']['AutoSelectReportType']){ ?>		
			if(!check_select(obj.TargetTerm, "<?php echo $i_alert_pleaseselect.$eReportCard['Term']; ?>.", ""))
				return false;
			
			if(!check_select(obj.TargetReport, "<?php echo $i_alert_pleaseselect.$eReportCard['ReportType']; ?>.", "")) 
				return false;
		<? } ?>
		
	<? } ?>
	
	<? if ($eRCTemplateSetting['CustomizaedTermDateRange']) { ?>
	if(compareDate(obj.TermEndDate.value, obj.TermStartDate.value) < 0) {
		obj.TermStartDate.focus();
		alert("<?=$eReportCard['AlertComparePeriod']?>");
		return false;
	}
	<? } ?>
	
	<?php if(!$eRCTemplateSetting['Settings']['AutoSelectReportType']){ ?>
		if ($('input#term_report').attr('checked') == true && $('input#is_main_term_report').attr('checked') == true)
		{
			$.post(
				"aj_new1_check_main_report.php", 
				{
					ClassLevelID : $('select#ClassLevelID').val(),
					Semester : $('select#Semester').val()
				},
				function(hasMainReport)
				{
					if (hasMainReport == '1')
					{
						alert("<?=$eReportCard['jsHasMainTermReportAlready']?>");
						return false;
					}
					else
					{
						obj.submit();
					}
				}
			);
		}
	<?php } else { ?>
		if($("select#ClassLevelID")[0] && $("select#TargetTerm")[0] && $("select#TargetReport")[0])
		{
			$.post(
				"aj_new1_check_main_report.php",
				{
					ClassLevelID : $("select#ClassLevelID").val(),
					TargetTerm : $('select#TargetTerm').val(),
					ReportNumber : $('select#TargetReport').val(),
					CheckAllReport : true
				},
				function(hasMainReport)
				{
					if (hasMainReport == '1')
					{
						alert("<?=$eReportCard['jsHasSameReportAlready']?>");
						return false;
					}
					else
					{
						obj.submit();
					}
				}
			);
		}
	<?php } ?>
	else
	{
		obj.submit();
	}
}

function jAJAX_changeForm( ClassLevelID ) 
{
	jFormObject = document.selectformForm;
	jFormObject.ClassLevelID.value = ClassLevelID;
	jFormObject.Semester.value = document.form1.Semester.value;
	tr = document.getElementsByName('term_report');
	if(tr.length==2)
		jFormObject.term_report_selected.value = tr[0].checked ? "TermReport" : "WholeYearReport";
	else
		jFormObject.term_report_selected.value = tr[0].value;
		
	YAHOO.util.Connect.setForm(jFormObject);
	
	path = "./aj_new1_selectForm.php";
	var connectionObject = YAHOO.util.Connect.asyncRequest('POST', path, callback_changeForm);		
}

var callback_changeForm = 
{
	success: function (o)
	{
		jChangeContent( "selectTermsDiv", o.responseText );						
	}
}

function confirmSkip()
{
	if(confirm("<?=$eReportCard['SkipStep']?>"))
	{
		document.form1.reset();
		document.form1.skip.value=1;
		return checkForm(document.form1);
	}
	document.form1.skip.value=0;
	return false;
}

function js_Select_ReportType(type)
{
	if (type == "Term")
		document.getElementById('term_report').checked = true;
	else
		document.getElementById('wholeyear_report').checked = true;
}

function js_Change_Report_Type(type)
{
	if (type == 'Term')
	{
		// disable Consolidate Report Semester options
		$("input[name='Sem[]']").attr('disabled', true);
		$("input#Sem_all").attr('disabled', true);
		
		
		// enable Term Selection
		$('select#Semester').attr('disabled', false);
		
		// enable Main & Extra Radio Btn
		$('input#is_main_term_report').attr('disabled', false);
		$('input#is_extra_term_report').attr('disabled', false);	
		
		$('tr#TermStartDateTr').show();
		$('tr#TermEndDateTr').show();
	}
	else
	{
		// disable Term Selection
		$('select#Semester').attr('disabled', true);
		
		// disable Main & Extra Radio Btn
		$('input#is_main_term_report').attr('disabled', true);
		$('input#is_extra_term_report').attr('disabled', true);		
		
		// enable Consolidate Report Semester options
		$("input[name='Sem[]']").attr('disabled', false);
		$("input#Sem_all").attr('disabled', false);
		
		$('tr#TermStartDateTr').hide();
		$('tr#TermEndDateTr').hide();
	}
}

function js_Update_Term_Start_End_Date(jsYearTermID)
{
	$.post(
		"ajax_reload.php", 
		{ 
			Action: 'Reload_Term_Start_End_Date',
			YearTermID: jsYearTermID
		},
		function(ReturnData)
		{
			var tempArr = ReturnData.split('|||');
			var jsThisStartDate = tempArr[0];
			var jsThisEndDate = tempArr[1];
			
			$('input#TermStartDate').val(jsThisStartDate);
			$('input#TermEndDate').val(jsThisEndDate);
			//$('#IndexDebugArea').html(ReturnData);
		}
	);
}

<?php if($eRCTemplateSetting['Settings']['AutoSelectReportType']){ ?>
function js_updateReportList(type){
	var term_value = $('select#TargetTerm').val();
	var level_value = $('select#ClassLevelID').val();
	
	if(level_value==""){
		$("select#TargetTerm").html("<option value='' selected>--- Select Term ---</option>");
		$("select#TargetReport").html("<option value='' selected>--- Select Report ---</option>");
		return;
	}
	if(term_value==""){
		$("select#TargetReport").html("<option value='' selected>--- Select Report ---</option>");
	}
	
	var form_num = formNumberMap[level_value];
	
	if(type=="sem"){
		if(isNaN(form_num)){
			$("select#TargetTerm").html("<option value='' selected>--- Select Term ---</option><option value='s1'>Q1</option><option value='s2'>S1</option><option value='s3'>Q3</option><option value='s4'>S2</option>");
		} else {
			if(form_num > 0 && form_num < 6){
				$("select#TargetTerm").html("<option value='' selected>--- Select Term ---</option><option value='t1'>Q1</option><option value='s2'>S1</option><option value='t3'>Q3</option><option value='s4'>S2</option>");
			} else {
				$("select#TargetTerm").html("<option value='' selected>--- Select Term ---</option><option value='t1'>Q1</option><option value='t2'>Q2</option><option value='s2'>S1</option><option value='t3'>Q3</option><option value='t4'>Q4</option><option value='s4'>S2</option>");
			}
		}
		$("select#TargetReport").html("<option value='' selected>--- Select Report ---</option>");
	}
	if(type=="term" && term_value!=""){
		var term_num = term_value.match(/\d+/)[0];
		var term_type = "s"; 
		if (term_value.indexOf('s') == -1) {
			term_type = "t";
		}
		
		if(term_type=="t"){
			if(form_num > 0 && form_num < 6){
				$("select#TargetReport").html("<option value='1'><?=$eReportCard['Template']['ReportName']['ESProgressReport']?></option>");
			} else {
				$("select#TargetReport").html("<option value='1'><?=$eReportCard['Template']['ReportName']['MSProgressReport']?></option>");
			}
		} else {
			if(isNaN(form_num) && term_num % 2 == 1){
				var input_content = "<option value='1'><?=$eReportCard['Template']['ReportName']['MSProgressReport']?></option>";
			} else {
				var input_content = "<option value='1'><?=$eReportCard['Template']['ReportName']['SemesterReport']?></option>";
			}
			if(term_num==4){
				input_content = input_content + "<option value='2'><?=$eReportCard['Template']['ReportName']['FinalReport']?></option>";
			}
			$("select#TargetReport").html(input_content);
		}
	}
}
<?php } ?>

//-->
</script>
<br>

<form name="form1" method="post" action="new_update.php">
<table id="html_body_frame" width="96%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td align="right" valign="bottom"><?=$linterface->GET_SYS_MSG($msg);?></td><td></td>
</tr>
<tr> 
	<td align="center">
		
		<?= $lreportcard_ui->Get_Settings_ReportCardTemplate_Nav() ?>
		<br />
		<?= $linterface->GET_STEPS($STEPS_OBJ) ?>                            
		<br />
		
		<table class="form_table_v30" align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
		<tr>
			<td class="field_title" valign="top" nowrap="nowrap" class="formfieldtitle" width="30%"><span class="tabletextrequire">*</span><span class="tabletext"><?=$eReportCard['ReportTitle']?></span> </td>
			<td width="70%" valign="top">
				<input id="report_title1" name="report_title1" type="text" class="textboxtext" value="<?=$report_title1?>" />
				<input name="report_title2" type="text" class="textboxtext" value="<?=$report_title2?>" />
			</td>
		</tr>
		<tr>
			<td class="field_title" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletextrequire">*</span><span class="tabletext"><?=$i_ClassLevel?></span> </td>
			<td valign="top" class="tabletext"><?=$FormListSelection?></td>
		</tr>
		
		<?php if(!$eRCTemplateSetting['Settings']['AutoSelectReportType']){ ?>
			<tr valign="top">
				<td class="field_title" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$eReportCard['ReportType']?></span></td>
				<td>
					<div id="selectTermsDiv">
					<?=$ReportTypeTable?>	 
					</div>			
				</td>
			</tr>
		<?php } else { ?>
			<tr valign="top">
				<td class="field_title" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletextrequire">*</span><span class="tabletext"><?=$eReportCard['Term']?></span></td>
				<td>
					<?=$TargetTermSelect?>	 
				</td>
			</tr>
			<tr valign="top">
				<td class="field_title" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletextrequire">*</span><span class="tabletext"><?=$eReportCard['ReportType']?></span></td>
				<td>
					<?=$ReportTypeTable?>	 
				</td>
			</tr>
		<?php } ?>
		
		<tr>
			<td class="field_title" valign="top" nowrap="nowrap" class="formfieldtitle" style="border-bottom:1px #EEEEEE solid;"><span class="tabletext"><?=$i_general_description?></span></td>
			<td valign="top"><?=$linterface->GET_TEXTAREA("Description", $data['Description'])?></td>
		</tr>
		
		<? if ($eRCTemplateSetting['CustomizaedTermDateRange']) { ?>
		
		<tr id="TermStartDateTr" <?=$TermDateStyle?>>
			<td valign="top" nowrap="nowrap" class="field_title" style="border-bottom:1px #EEEEEE solid;"><span class="tabletext"><?=$eReportCard['TermStartDate']?></span></td>
			<td valign="top"><?=$linterface->GET_DATE_PICKER("TermStartDate",$TermStartDate);?></td>
		</tr>
		<tr id="TermEndDateTr" <?=$TermDateStyle?>>
			<td valign="top" nowrap="nowrap" class="field_title" style="border-bottom:1px #EEEEEE solid;"><span class="tabletext"><?=$eReportCard['TermEndDate']?></span></td>
			<td valign="top"><?=$linterface->GET_DATE_PICKER("TermEndDate",$TermEndDate);?></td>
		</tr>
		
		<? } ?>
		
		</table>
	</td>
</tr>
<tr>
	<td>
		<table width="100%" border="0" cellspacing="0" cellpadding="2">
		<tr>
			<td height="1" class="dotline" colspan="2"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1"></td>
		</tr>
		<tr>
        	<td align="left" class="tabletextremark">&nbsp;&nbsp;&nbsp;<?=$i_general_required_field2?></td>
		</tr>
		<tr>
			<td align="center" valign="bottom">
				<?= $linterface->GET_ACTION_BTN($button_submit_continue, "button","return checkForm(this.form);") ?>
				<?= $linterface->GET_ACTION_BTN($button_reset, "reset") ?>
				<? if($ReportID && !($eRCTemplateSetting['Settings']['AutoSelectReportType'] && ($isESReport || $isKGReport) && !$IsReportCompleted)) { 
					echo $linterface->GET_ACTION_BTN($button_skip, "submit", "return confirmSkip();"); 
				}?>
				<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='index.php'") ?>
			</td>
		</tr>
		</table>
	</td>
</tr>
</table>

<input type="hidden" name="ReportID" value="<?=$ReportID?>">
<input type="hidden" name="skip" value="0">
</form>

<br />

<form name="selectformForm">
<input type="hidden" name="ClassLevelID" value="">
<input type="hidden" name="Semester" value="">
<input type="hidden" name="term_report_selected" value="">
</form>

<script>
$('document').ready( function() { 
	<? if ($ReportType == 'F') { ?>
		$('select#Semester').attr('disabled', true);
	<? } else { ?>
		$("input[name='Sem[]']").attr('disabled', true);
		$("input#Sem_all").attr('disabled', true);
	<? } ?>
});

</script>

<?
print $linterface->FOCUS_ON_LOAD("form1.report_title1");
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
