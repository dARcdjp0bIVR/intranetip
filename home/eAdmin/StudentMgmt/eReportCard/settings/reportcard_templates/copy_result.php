<?
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");

intranet_opendb();

$linterface 	= new interface_html();
if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
{
	include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
	$lreportcard = new libreportcardcustom();
} else {
	$lreportcard = new libreportcard();
}

$lclass 		= new libclass();

$CurrentPage 	= "Settings_ReportCardTemplates";
$MODULE_OBJ 	= $lreportcard->GET_MODULE_OBJ_ARR();

# tag information
$TAGS_OBJ[] 		= array($eReportCard['Settings_ReportCardTemplates'], "", 0);
$PAGE_NAVIGATION[] 	= array($eReportCard['CopyResult']);
$linterface->LAYOUT_START();

$frForm = $frForm ? $frForm : $toForm;

$copyResult = unserialize($cr);
if($FormCopy)
	$copyAction = $eReportCard['CopyFrom'] ." ". $lclass->getLevelName($frForm) ." ". $i_To." ".  $lclass->getLevelName($toForm);	
else
	$copyAction = $eReportCard['CopyFrom'] ." ". $lclass->getLevelName($frForm) ." ". $i_To ." ".  $lclass->getLevelName($toForm);	
	
$ti = 0;
foreach($copyResult as $ReportID=>$status)
{
	$temp = $lreportcard->returnReportTemplateBasicInfo($ReportID);
	switch($status)
	{
		case 1:
			$status_msg = $eReportCard['Success'];
			break;
		case 0:
			$status_msg = $eReportCard['SuccessWithMissing'];
			break;	
		case -1:
			$status_msg = $eReportCard['TemplateExists'];
			break;	
	}
	$x .= "<tr>";
	$x .= "<td class=\"tablerow". ($ti%2 +1) ." tabletext\">". str_replace(":_:","<br />",$temp['ReportTitle']) ."</td>";
	$x .= "<td class=\"tablerow". ($ti%2 +1) ." tabletext\">". $status_msg ."</td>";
	$x .= "</tr>";
	$ti++;
}


?>

<br />


<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	<td align='right'>&nbsp;</td>
</tr>

<tr>
	<td colspan="2">
                <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr>  
                	<td>
	                	<br />
						<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
		                <tr class="tabletop">
		                  <td><span class="tabletopnolink"><?=$eReportCard['ReportTitle']?></span> </td>
		                  <td><span class="tabletopnolink"><?=$i_general_status?></span></td>
		                </tr>
	                
		                <tr class="resubtabletop">
							<td colspan="2" class="tabletext"><strong><?=$copyAction?></strong></td>
						</tr>
		                
		                <?=$x?>
						</table>
					</td>
                </tr>
                </table>
	</td>
</tr>

<tr>
	<td colspan="2">        
                <table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
                <tr>
                	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
                <tr>
			<td align="center">
				<?= $linterface->GET_ACTION_BTN($button_continue, "button", "window.location='index.php'") ?>
			</td>
		</tr>
                </table>                                
	</td>
</tr>

</table>                        
<br />

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
