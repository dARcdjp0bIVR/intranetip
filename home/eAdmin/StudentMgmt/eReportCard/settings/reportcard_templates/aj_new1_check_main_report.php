<?
# using: 

################# Change Log [Start] ############
#
#	Date:	2015-10-19	Bill
#			support BIBA - 4 School Terms
#
#	Date:	2015-07-21	Bill	[2015-0413-1359-48164]
#			extend checking of existing report to Extra Term Report for BIBA
#
################# Change Log [End] ##############

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");

intranet_auth();
intranet_opendb();

if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
{
	include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
	$lreportcard = new libreportcardcustom();
} else {
	$lreportcard = new libreportcard();
}

$ClassLevelID = $_REQUEST['ClassLevelID'];
$Semester = $_REQUEST['Semester'];

// [2015-0413-1359-48164] $_POST data
$CheckAllReport = $_POST['CheckAllReport'];
$TargetTerm = $_POST['TargetTerm'];
$ReportNumber = $_POST['ReportNumber'];

// [2015-0413-1359-48164] check any duplicate report of all report type
if($CheckAllReport){
	$other_field = "";
	
	$isMainReport = (strpos($TargetTerm,"s")!==false)? 1 : 0;
	$term_value = intval(preg_replace('/[^0-9]+/', '', $TargetTerm), 10);
	
	// Main Report
//	if($isMainReport){
//		$term_seq = $term_value > 1? 1 : 0;
//	} 
//	// Extra Report
//	else {
//		$term_seq = $term_value > 3? 1 : 0;
//		
//		// extra condition for MS Progress Report as each semester with 2 progress reports
//		$form_num = intval($lreportcard->GET_FORM_NUMBER($ClassLevelID));
//		if($form_num > 5){
//			$other_field = " (ReportSequence = '$term_value'".(($term_value==1 || $term_value==3)? " OR ReportSequence IS NULL)" : ")");
//		}
//	}
	
	// semester list
	$semList = getSemesters($lreportcard->GET_ACTIVE_YEAR_ID(),"",1);
	$Semester = $ReportNumber==2? "F" : $semList[($term_value-1)]["YearTermID"];
	
	// get report list
	//$ReportArr = $lreportcard->returnReportTemplateBasicInfo("", $other_field, $ClassLevelID, $Semester, $isMainReport);
	$ReportArr = $lreportcard->returnReportTemplateBasicInfo("", "", $ClassLevelID, $Semester, $isMainReport);

	$hasMainReport = 0;
	if(count($ReportArr)>0){
		$hasMainReport = 1;
	}	
} 
// check any duplicate main report
else {
	$MainReportArr = $lreportcard->returnReportTemplateBasicInfo($ReportID='', "isMainReport = '1' And Semester = '$Semester' And ClassLevelID = '$ClassLevelID'");
	if (count($MainReportArr) > 0)
		$hasMainReport = 1;
	else
		$hasMainReport = 0;
}

intranet_closedb();
echo $hasMainReport;

?>