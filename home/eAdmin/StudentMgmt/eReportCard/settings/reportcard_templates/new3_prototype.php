<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if (!$plugin['ReportCard2008'])
{
	header ("Location: /");
	intranet_closedb();
	exit();
}

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
{
	include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
	$lreportcard = new libreportcardcustom();
} else {
	$lreportcard = new libreportcard();
}

$linterface = new interface_html();
$lclass = new libclass();

if (!$lreportcard->hasAccessRight()) 
{
	header ("Location: /");
	intranet_closedb();
	exit();
}

$CurrentPage = "Settings_ReportCardTemplates";
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();

# Loading Report Template Data
$basic_data = $lreportcard->returnReportTemplateBasicInfo($ReportID);	// Basic Data

$ReportTitle = $basic_data['ReportTitle'];
$ReportTitle = str_replace(":_:", "<br>", $ReportTitle);
$ReportType = $basic_data['Semester'];
$ClassLevelID = $basic_data['ClassLevelID'];

$ColumnTitleAry = $lreportcard->returnReportColoumnTitle($ReportID);

$ColumnTitle_Display = "";
$ColumnIDAry = array();
$SelectedSemAry = $lreportcard->returnReportInvolvedSem($ReportID);
foreach($SelectedSemAry as $semid => $semname)
	$SelectedSem[] = $semid;
	
switch($ReportType)
{
	case "F":		// Whole Year Report Type
		$ReportTypeTitle = $eReportCard['WholeYearReport'];
	
		break;
	default:		// Term Type
		$ReportTypeTitle = $eReportCard['TermReport'] ." - ". $lreportcard->returnSemesters($SelectedSem[0]);
		break;
}

/* 1 - horizontal, 2 - vertical */
$cal = $lreportcard->returnReportTemplateCalculation($ReportID);
switch($cal)
{
	case "1":
		$defaultweight = $lreportcard->returnReportTemplateSubjectWeightData($ReportID, 'ReportColumnID is NULL and SubjectID is NULL');
		$dweight = $defaultweight[0]['Weight'];
		
		$assign_all_tr = "";
		$assign_all_tr .= "<tr>\r\n";
			$assign_all_tr .= "<td width=\"15\" align=\"left\">&nbsp;</td>\r\n";
			$assign_all_tr .= "<td width=\"25%\" align=\"right\" valign=\"bottom\" class=\"tabletextremark\"><a href=\"javascript:void(0);\" onClick=\"clickAssignAll();\" class=\"tabletool\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_copy.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">". $eReportCard['AssignToAll'] ."</a></td>\r\n";
			
			if ($eRCTemplateSetting['IndividualVerticalSubjectWeightForEachAssessments']) {
				$assign_all_tr .= "<td align=\"center\" class=\"retablerow1_total tabletext\"><input name=\"DefaultWeight\" type=\"text\" class=\"ratebox\" value=\"1\"></td>\r\n";
				$assign_all_tr .= "<td align=\"center\" class=\"retablerow1_total tabletext\"><input name=\"DefaultWeight\" type=\"text\" class=\"ratebox\" value=\"1\"></td>\r\n";
				$assign_all_tr .= "<td align=\"center\" class=\"retablerow1_total tabletext\"><input name=\"DefaultWeight\" type=\"text\" class=\"ratebox\" value=\"1\"></td>\r\n";
				$assign_all_tr .= "<td align=\"center\" class=\"retablerow1_total tabletext\"><input name=\"DefaultWeight\" type=\"text\" class=\"ratebox\" value=\"1\"></td>\r\n";
			}
			
			$assign_all_tr .= "<td align=\"center\" class=\"retablerow1_total tabletext\"><input name=\"DefaultWeight\" type=\"text\" class=\"ratebox\" value=\"". $dweight ."\"></td>\r\n";
		$assign_all_tr .= "</tr>\r\n";
		
		if ($eRCTemplateSetting['IndividualVerticalSubjectWeightForEachAssessments']) {
			$ColumnTitle_Display .= "<td align=\"center\" valign=\"top\"  class=\"retabletop tabletopnolink\">持續評估 Continuous Assessment</td>";
			$ColumnTitle_Display .= "<td align=\"center\" valign=\"top\"  class=\"retabletop tabletopnolink\">校本評核 SBA</td>";
			$ColumnTitle_Display .= "<td align=\"center\" valign=\"top\"  class=\"retabletop tabletopnolink\">統測成績 Joint Test</td>";
			$ColumnTitle_Display .= "<td align=\"center\" valign=\"top\"  class=\"retabletop tabletopnolink\">考試成績 Exam</td>";
		}
		$ColumnTitle_Display .= "<td align=\"center\" valign=\"top\"  class=\"retabletop tabletopnolink\">". $eReportCard['OverallSubjectWeight'] ."</td>";
		
		$ratio_tr = "";
		
		/* START Build Weight Array (for loading data) */
		$weight_data = $lreportcard->returnReportTemplateSubjectWeightData($ReportID, 'ReportColumnID is NULL'); // Column Data
		for($i=0;$i<sizeof($weight_data);$i++)
		{
			$WeightAry[$weight_data[$i]['SubjectID']] = $weight_data[$i]['Weight'];
		}
		/* END Build Weight Array (for loading data) */
				
		$SubjectArray = $lreportcard->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $ReportID);
		
		// Get the last sub-subject of each subjects if any & use for css
		$LastCmpSubject = array();
		if(!empty($SubjectArray))
		{
			foreach($SubjectArray as $SubjectID => $Data)
			{
				if(sizeof($Data)>1)
				{
					foreach($Data as $a => $b)
					{
						$LastCmpSubject[$SubjectID] = $a;
					}
				}
			}
		}

		
		$main_data = "";
		if(!empty($SubjectArray))
		{
			$mainSubRow = 0;
			foreach($SubjectArray as $SubjectID => $Data)
			{
				if(is_array($Data))
				{
					foreach($Data as $CmpSubjectID => $SubjectName)
					{	
						if($CmpSubjectID > 0)	// sub subject
						{
			 				$css1 = (count($LastCmpSubject) > 0 && in_array($CmpSubjectID, $LastCmpSubject)===true) ? 'tablelist_subject':'tablelist_subject_head';		
							$css2 = 'tablelist_sub_subject';		
			 				$Subject = "
								<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
								<tr>
									<td width=\"13\">
										<table border=\"0\" cellspacing=\"0\" cellpadding=\"2\">
										<tr>
											<td><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"13\" height=\"13\"></td>
										</tr>
										<tr>
											<td><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"13\" height=\"13\"></td>
										</tr>
										</table>
									</td>
									<td class=\"tabletext\">{$SubjectName}</td>
								</tr>
								</table>
							";
						}
						else					// main subject
						{
							$css1 = 'tablelist_subject';		
							$css2 = 'tablelist_subject';
							$Subject = $SubjectName;
						}
						$SubjectID = ($CmpSubjectID==0) ? $SubjectID : $CmpSubjectID;
						
						$main_data .= "<tr>";
						$main_data .= "
							<td width=\"15\" align=\"left\" class=\"{$css1}\">
								<table border=\"0\" cellspacing=\"0\" cellpadding=\"2\">
								<tr>
									<td><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"13\" height=\"13\"></td>
								</tr>
								<tr>
									<td><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"13\" height=\"13\"></td>
								</tr>
								</table>
							</td>
						";
						$main_data .= "<td width=\"25%\" class=\"{$css2}\" >{$Subject} </td>";
						
						//check css
						if($cal==1)
						{
							$rowCss = "retablerow".($mainSubRow%2+1);	
						}
						//Fields Column
						
						$thisPrefix = ($CmpSubjectID) ? "<img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"33\" height=\"13\">" : ""; 
						if ($eRCTemplateSetting['IndividualVerticalSubjectWeightForEachAssessments']) {
							$main_data .= "<td align=\"center\" class=\"$rowCss tabletext\">";
								$main_data .= $thisPrefix;
								$main_data .= ($CmpSubjectID) ? "<input name=\"Weight0[]\" type=\"text\" class=\"ratebox\" value=\"". $WeightAry[$SubjectID] ."\">" : '---';
							$main_data .= "</td>";
							
							$main_data .= "<td align=\"center\" class=\"$rowCss tabletext\">";
								$main_data .= $thisPrefix;
								$main_data .= ($CmpSubjectID) ? "<input name=\"Weight0[]\" type=\"text\" class=\"ratebox\" value=\"". $WeightAry[$SubjectID] ."\">" : '---';
							$main_data .= "</td>";
							
							$main_data .= "<td align=\"center\" class=\"$rowCss tabletext\">";
								$main_data .= $thisPrefix;
								$main_data .= ($CmpSubjectID) ? "<input name=\"Weight0[]\" type=\"text\" class=\"ratebox\" value=\"". $WeightAry[$SubjectID] ."\">" : '---';
							$main_data .= "</td>";
							
							$main_data .= "<td align=\"center\" class=\"$rowCss tabletext\">";
								$main_data .= $thisPrefix;
								$main_data .= ($CmpSubjectID) ? "<input name=\"Weight0[]\" type=\"text\" class=\"ratebox\" value=\"". $WeightAry[$SubjectID] ."\">" : '---';
							$main_data .= "</td>";
						}
						
						$main_data .= "<td align=\"center\" class=\"$rowCss tabletext\">";
							$main_data .= $thisPrefix;
							$main_data .= "<input name=\"Weight0[]\" type=\"text\" class=\"ratebox\" value=\"". $WeightAry[$SubjectID] ."\">";
						$main_data .= "</td>";

						
						$main_data .= "</tr>"."\n";
					}
				}
				$mainSubRow++;
			}
		}
		
		$ColumnNo = 1;
		if ($eRCTemplateSetting['IndividualVerticalSubjectWeightForEachAssessments']) {
			$ColumnNo += 4;
		}
		
		break;
	case "2":
		$assign_all_tr = "";
		
		$ColumnNo = sizeof($ColumnTitleAry);
		foreach($ColumnTitleAry as $ColID => $ColumnTitle)
		{
			$ColumnIDAry[] = $ColID;
			//$ColumnTitle =  (convert2unicode($ColumnTitle, 1, 2));
			//$ColumnTitle =  $ReportType=="F" ? $ColumnTitle : convert2unicode($ColumnTitle, 1, 2);
			$ColumnTitle_Display .= "<td align=\"center\" valign=\"top\"  class=\"retabletop tabletopnolink\">". $ColumnTitle ."<br>";
			$ColumnTitle_Display .= "<input type=\"hidden\" name=\"ReportColumnID[]\" value=\"$ColID\"></td>";
		}
		
		$usePercentage = $basic_data['PercentageOnColumnWeight'];
		
		$ratio_tr = "
			<tr>
	            <td colspan=\"{$ColumnNo}\" align=\"center\" class=\"tablerow2 term_break_link tabletext retabletop_total\"><span class=\"tabletexttopremark\">". $eReportCard['RatioBetweenAssesments'] ." 
	              <label for=\"usePercentage\"><input name=\"usePercentage\" type=\"checkbox\" id=\"usePercentage\" value=\"1\" onClick=\"clickusePercentage()\" ". ($usePercentage ? "checked":"")."> ". $eReportCard['Use'] ." %</label></span></td>
            </tr>
		";
		
		/* START Build Ratio Array (for loading data) */
		$ratio_data = $lreportcard->returnReportTemplateSubjectWeightData($ReportID, 'SubjectID is NULL and ReportColumnID is not NULL'); // Column Data
		for($i=0;$i<$ColumnNo;$i++)
		{
			$RatioAry[$ratio_data[$i]['ReportColumnID']] = $usePercentage ? $ratio_data[$i]['Weight']*100 : $ratio_data[$i]['Weight'];
		}
		/* END Build Subject Weight Array (for loading data) */
		
		$main_data = "<tr>";
		for($tc=0;$tc<$ColumnNo;$tc++)	
		{
			$main_data .= "<td height=\"150\" align=\"center\" class=\"retablerow1_total tabletext\"><input name=\"Ratio[]\" type=\"text\" class=\"ratebox\" value=\"". $RatioAry[$ColumnIDAry[$tc]] ."\"><span id=\"percentage[]\" name=\"percentage[]\">". ($usePercentage?"%":" ") ."</span></td>";
		}
		$main_data .= "</tr>";

		break;
}

# tag information
$TAGS_OBJ[] = array($eReportCard['Settings_ReportCardTemplates'], "", 0);

$linterface->LAYOUT_START();

$STEPS_OBJ[] = array($eReportCard['InputBasicInformation'], 0);
if($ReportType<>"F")	# Term Report type
{
	if($cal==1)
	{
		$STEPS_OBJ[] = array($eReportCard['InputAssessmentRatio'], 0);
		$STEPS_OBJ[] = array($eReportCard['InputOverallSubjectWeight'], 1);	
	}
	else
	{
		$STEPS_OBJ[] = array($eReportCard['InputAssessmentWeight'], 0);
		$STEPS_OBJ[] = array($eReportCard['InputAssessmentRatio'], 1);	
	}
}
else					# Whole year report
{
	if($cal==1)
	{
		$STEPS_OBJ[] = array($eReportCard['InputTermsRatio'], 0);
		$STEPS_OBJ[] = array($eReportCard['InputOverallSubjectWeight'], 1);	
	}
	else
	{
		$STEPS_OBJ[] = array($eReportCard['InputSubjectWeight'], 0);
		$STEPS_OBJ[] = array($eReportCard['InputTermsRatio'], 1);	
	}
}
$STEPS_OBJ[] = array($eReportCard['InputReportDisplayInformation'], 0);
$STEPS_OBJ[] = array($button_preview, 0);
#$STEPS_OBJ[] = array($eReportCard['Finishandpreview'], 0);

?>

<script language="javascript">
<!--
function clickusePercentage()
{
	obj = document.getElementById('usePercentage').checked;
	
	x = document.getElementsByName('percentage[]');
	for(i=0;i<x.length;i++)
	{
		if(obj)
			x[i].innerHTML = "%";			
		else
			x[i].innerHTML = "&nbsp;";			
	}
}

function checkform()
{
	<? if($cal==1) {?>
 	weight_data = document.getElementsByName('Weight0[]');
 	total = 0;
 	for(i=0;i<weight_data.length;i++)
 	{
	 	if(isNaN(weight_data[i].value))
	 	{
		 	alert("<?=$eReportCard['WeightNumeric']?>");
		 	return false;	
	 	}
	 	
	 	if(!(Trim(weight_data[i].value)))
	 	{
		 	alert("<?=$eReportCard['MissingData']?>.");
		 	return false;	
	 	}
 	}
 	<? } ?>
 	
	<? if($cal==2) {?>
 	ratio_data = document.getElementsByName('Ratio[]');
 	total = 0;
 	for(i=0;i<ratio_data.length;i++)
 	{
	 	if(isNaN(ratio_data[i].value))
	 	{
		 	alert("<?=$eReportCard['RatioNumeric']?>");
		 	return false;	
	 	}
	 	
	 	if(!(Trim(ratio_data[i].value)))
	 	{
		 	alert("<?=$eReportCard['MissingData']?>");
		 	return false;	
	 	}
	 	
	 	total = total + parseInt(ratio_data[i].value);
 	}
 	
 	
 	// Check and confirm the % is 100% or not 
 	if(document.form1.usePercentage.checked && total!=100)
 	{
	 	total_str = "<?=$eReportCard['RatioSumNot100']?>";
	 	
	 	if(!confirm(total_str))
	 	{
		 	return false;
	 	}
 	}
 	<? } ?>
 	
}

function clickAssignAll()
{
	we = document.getElementsByName('Weight0[]');
	var dw = document.form1.DefaultWeight.value;
	
 	for(i=0;i<we.length;i++)
 	{
		we[i].value = dw;
 	}
}

function confirmSkip()
{
	if(confirm("<?=$eReportCard['SkipStep']?>"))
	{
		document.form1.reset();
		document.form1.skip.value=1;
		return checkform();
	}
	document.form1.skip.value=0;
	return false;
}
//-->
</script>

<br />
<form name="form1" method="post" action="new3_update.php">
<table width="96%" border="0" cellspacing="0" cellpadding="4">
<tr>
	<td>
		<?= $linterface->GET_STEPS($STEPS_OBJ) ?>   
		
		<br \>
          <table width="100%" border="0" cellspacing="0" cellpadding="2">
          <tr>
          	<td align="right"><?=$linterface->GET_SYS_MSG($msg, $xmsg);?></td>
          	</tr>
            <tr>
              <td><?=$linterface->GET_NAVIGATION2($ReportTitle);?></td>
            </tr>
            <tr>
              <td align="left" valign="top">
              
              <div id="main_content">
              <table width="100%" border="0" cellspacing="0" cellpadding="4">
					<tr>
						<? if($cal==1) {?>
						<td width="15" align="left">&nbsp;</td>
						<td width="25%" align="right" valign="top">&nbsp;</td>
						<? } ?>
						<td align="center" valign="top" class="tablename" colspan="<?=$ColumnNo?>"><table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
                                        <td class="tabletext"><strong><?=$ReportTypeTitle?></strong></td>
                                        <td align="right">&nbsp;</td>
                                      </tr>
						</table></td>
					</tr>
					<tr>
						<? if($cal==1) {?>
						<td width="15" align="left">&nbsp;</td>
						<td width="25%" align="right" valign="bottom" class="tabletextremark">&nbsp;</td>
						<? } ?>
						<!-- Table Header TH//-->
						<?=$ColumnTitle_Display?>
					</tr>
					
					<?=$assign_all_tr?>
					<?=$ratio_tr?>
					
					<?=$main_data?>
					
				</table>
				</div>					
				</td>
            </tr>
          </table>

	</td>
</tr>
</table>

<br />
<table width="96%" border="0" cellspacing="0" cellpadding="1">
  <tr>
    <td height="1" class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
  </tr>
  <tr>
    <td align="center">
    	<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "document.form1.Continue.value=0;return checkform();") ?>
      	<?= $linterface->GET_ACTION_BTN($button_submit_continue, "submit", "document.form1.Continue.value=1;return checkform();") ?>
      	<?= $linterface->GET_ACTION_BTN($button_reset, "reset") ?>
      	<?= $linterface->GET_ACTION_BTN($button_skip, "submit", "return confirmSkip();") ?>
		<?= $linterface->GET_ACTION_BTN($button_back, "button", "window.location='new2.php?ReportID=$ReportID'") ?>
	</td>
  </tr>
</table>

<input type="hidden" name="ReportID" value="<?=$ReportID?>">
<input type="hidden" name="Continue" value="0">
<input type="hidden" name="skip" value="0">
</form>

<br />

    
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
