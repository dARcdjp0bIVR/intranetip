<?php
/*
 * Run reportcard schema before uploading this files to client site
 */
// Using: 

/***************************************************************
 * 	Modification
 * 	20180205 Bill:	[2017-0907-0952-53240]
 * 		- Show customized report display settings
 *  20170213 Bill:	[2017-0210-1003-52073]
 * 		- redirect to correct page when click back button
 * 	20150814 Bill:	[2015-0413-1359-48164]
 * 		- hide 2 ratio steps for BIBA ES / KG Report
 * 	20150116 Bill:
 * 		- Show Other Info input field - Principal Name
 * 	20120316 Marcus:
 * 		- Cater GreaterThanMark for $eRCTemplateSetting['DisplayPosition']['SubjectPositionDisplaySettings']
 * 		- 2011-1130-1655-19066 - 蘇浙公學 - Show position setting in report card template is not working
 * *************************************************************/

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
//include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$plugin['ReportCard2008'])
{
	header ("Location: /");
	intranet_closedb();
	exit();
}

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
include_once($PATH_WRT_ROOT."includes/eRCConfig.php");
if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
{
	include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
	include_once($PATH_WRT_ROOT."includes/libreportcard2008_comment.php");
	include_once($PATH_WRT_ROOT."includes/libreportcard2008_file.php");
		
	$lreportcard = new libreportcardcustom();
	$lreportcard_file = new libreportcard_file();
}
else {
	$lreportcard = new libreportcard();
}

if (!$lreportcard->hasAccessRight()) 
{
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lreportcard_ui = new libreportcard_ui();
$linterface = new interface_html();
//$lclass = new libclass();

$CurrentPage = "Settings_ReportCardTemplates";
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();

# Loading Report Template Data
$basic_data = $lreportcard->returnReportTemplateBasicInfo($ReportID);	// Basic Data
$ClassLevelID = $basic_data['ClassLevelID'];
$ReportType = $basic_data['Semester'];
$OverallPositionRangeClass = ($basic_data['OverallPositionRangeClass']=='')? 0 : $basic_data['OverallPositionRangeClass'];
$OverallPositionRangeForm = ($basic_data['OverallPositionRangeForm']=='')? 0 : $basic_data['OverallPositionRangeForm'];
$GreaterThanAverageClass = ($basic_data['GreaterThanAverageClass']=='')? 0 : $basic_data['GreaterThanAverageClass'];
$GreaterThanAverageForm = ($basic_data['GreaterThanAverageForm']=='')? 0 : $basic_data['GreaterThanAverageForm'];
$AttendanceDays = ($basic_data['AttendanceDays']=='')? 0 : $basic_data['AttendanceDays'];
$SpecialPercentage = ($basic_data['SpecialPercentage']=='')? 0 : $basic_data['SpecialPercentage'];
$DisplayOption = ($basic_data['DisplayOption']=='')? 1 : $basic_data['DisplayOption'];
$PrincipalName = $basic_data['PrincipalName'];

$DisplayCommentSection = $basic_data["DisplayCommentSection"] == "" ? 1 : $basic_data["DisplayCommentSection"];
$DisplayAwardSection = $basic_data["DisplayAwardSection"] == "" ? 1 : $basic_data["DisplayAwardSection"];
$MaxActivityCount = $basic_data["MaxActivityCount"] == "" ? 0 : $basic_data["MaxActivityCount"];
$DisplayActivitySection = $basic_data["DisplayActivitySection"] == "" ? 1 : $basic_data["DisplayActivitySection"]; 

// ES / MS Report
$form_num = $lreportcard->GET_FORM_NUMBER($ClassLevelID);
if (is_numeric($form_num)) {
	$form_num = intval($form_num);
	$isESReport = $form_num > 0 && $form_num < 6;
}
// KG Report
else {
	$isKGReport = true;
}

/* START - Build Header number of empty line selector */
for($i=0;$i<5;$i++)
{
	$HeaderNoSelection[$i][0] = $i+1;	
	$HeaderNoSelection[$i][1] = $i+1;	
}
$HeaderNoSelect = getSelectByArray($HeaderNoSelection, "name='HeaderHeight'",$basic_data['HeaderHeight'],0,1);
/* END - Build Header number of empty line selector */

/* START Build select student information */
$DisplaySettings = unserialize($basic_data['DisplaySettings']);
$DisplaySettings = $DisplaySettings ? $DisplaySettings : array();

foreach((array)$eRCTemplateSetting['StudentInfo']['Selection'] as $key => $val)
{
	$checked = (in_array($val, $DisplaySettings)===true) ? "checked" : "";
	$student_info .= "<input type='checkbox' name='DisplaySettings[]' value='$val' id='$val' $checked> <label for='$key'>". $eReportCard['Template']['StudentInfo'][$val] ."</label> <br>";
}
/* END Build select student information */

/* START Build */
$SelectedSem = $lreportcard->returnReportInvolvedSem($ReportID);
$IsDetailsTempAry = $lreportcard->returnReportTemplateColumnData($ReportID);
foreach($IsDetailsTempAry as $key => $datas)
{
	$IsDetailsAry[$datas['SemesterNum']] = !$datas['IsDetails'] ? 1 : $datas['IsDetails'];
}

$displayCol = "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">";
$i=0;
foreach($SelectedSem as $semid => $semname) 
{
	//check the term report / assesment existing or not
	$check = $lreportcard->checkTermReportExists($ClassLevelID, $semid);
	$disabled = $check ? "":"disabled";
	$displayCol .= $i%2 ? "" : "<tr>";
	$displayCol .= "
		<td align=\"center\" valign=\"top\">
			<table width=\"100%\" cellpadding=\"2\" cellspacing=\"0\">
			<TR>
				<TD nowrap class=\"tablerow2 tabletext\"><strong >{$semname}</strong></TD>
			</TR>
			<TR>
				<TD nowrap class=\"tabletext\">
					<input type=\"radio\" name=\"IsDetails{$semid}\" id=\"IsDetails{$semid}1\" value=\"1\" ". (($IsDetailsAry[$semid]==1 and $disabled=="")?"checked":"") ." $disabled> <label for=\"IsDetails{$semid}1\">". $eReportCard['ShowAllAssessments'] ."</label> <br>
					<input type=\"radio\" name=\"IsDetails{$semid}\" id=\"IsDetails{$semid}2\" value=\"2\" ". (($IsDetailsAry[$semid]==2 or $disabled!="")?"checked":"") ."> <label for=\"IsDetails{$semid}2\">". $eReportCard['ShowTermTotalOnly'] ."</label> 
				</TD>
			</TR>
			</table>
		</td>
	";
	$displayCol .= $i%2 ? "</tr>" : "";
	$i++;
}
$displayCol .= "</table>";

# tag information
$TAGS_OBJ[] = array($eReportCard['Settings_ReportCardTemplates'], "", 0);

$linterface->LAYOUT_START();
$cal = $lreportcard->returnReportTemplateCalculation($ReportID);
$STEPS_OBJ[] = array($eReportCard['InputBasicInformation'], 0);

if($eRCTemplateSetting['Settings']['AutoSelectReportType'] && ($isESReport || $isKGReport))		# BIBA ES / KG Report
{
	// do nothing - hide 2 step objects
}
else if($ReportType<>"F")	# Term Report type
{
	if($cal==1)
	{
		$STEPS_OBJ[] = array($eReportCard['InputAssessmentRatio'], 0);
		$STEPS_OBJ[] = array($eReportCard['InputOverallSubjectWeight'], 0);	
	}
	else
	{
		$STEPS_OBJ[] = array($eReportCard['InputSubjectWeight'], 0);
		$STEPS_OBJ[] = array($eReportCard['InputAssessmentRatio'], 0);	
	}
}
else						# Whole year report
{
	if($cal==1)
	{
		$STEPS_OBJ[] = array($eReportCard['InputTermsRatio'], 0);
		$STEPS_OBJ[] = array($eReportCard['InputOverallSubjectWeight'], 0);	
	}
	else
	{
		$STEPS_OBJ[] = array($eReportCard['InputSubjectWeight'], 0);
		$STEPS_OBJ[] = array($eReportCard['InputTermsRatio'], 0);	
	}
}
$STEPS_OBJ[] = array($eReportCard['InputReportDisplayInformation'], 1);
$STEPS_OBJ[] = array($button_preview, 0);
#$STEPS_OBJ[] = array($eReportCard['Finishandpreview'], 0);

# [2017-0210-1003-52073] URl for back button
if($eRCTemplateSetting['Settings']['AutoSelectReportType'] && ($isESReport || $isKGReport)) {		# BIBA ES  / KG Report
	$goBackUrl = "new.php?ReportID=$ReportID";
}
else {
	$goBackUrl = "new3.php?ReportID=$ReportID";
}

### Class Position Settings
$ClassPositionSettingsDiv = '';
$ClassPositionSettingsDiv .= '<div id="classPositionSettingDiv" style="display:block;">'."\n";

if ($eRCTemplateSetting['DisplayPosition']['SubjectPositionDisplaySettings'])
{
	$ClassPositionSettingsDiv .= $lreportcard_ui->Get_Ranking_Display_Table($ReportID, $ClassLevelID, 'Class');
	$ClassPositionSettingsDiv .= '<span class="tabletextremark">'.$eReportCard['PositionRangeDisplayRemarks'].'</span>';
}
else
{
	# Mark Selection 
	$ClassMarkSelection = "<select name='GreaterThanAverageClass' id='GreaterThanAverageClass'>";
	for($i=0; $i<=10;$i++)
		$ClassMarkSelection .= "<option value='$i' ".($i==$GreaterThanAverageClass?"SELECTED":"").">$i</option>";
	$ClassMarkSelection .= "</select>";
	
	$ClassPositionSettingsDiv .= '<table id="classPositionTable" class="form_table" style="display:block; border:1px solid #CCCCCC;">'."\n";
		$ClassPositionSettingsDiv .= '<tr>';
			$ClassPositionSettingsDiv .= '<td>';
				$ClassPositionSettingsDiv .= $eReportCard['MaximumPosition'].': ';
				$ClassPositionSettingsDiv .= '<input name="OverallPositionRangeClass" type="text" class="ratebox" maxlength="3" value="'.$OverallPositionRangeClass.'" />';
				$ClassPositionSettingsDiv .= '&nbsp;&nbsp;';
				$ClassPositionSettingsDiv .= '<span class="tabletextremark">'.$eReportCard['MaximumPositionRemarks'].'</span>';
			$ClassPositionSettingsDiv .= '</td>';
		$ClassPositionSettingsDiv .= '</tr>';
		
		if ($eRCTemplateSetting['DisplayPosition']['AverageChecking'])
		{
			$ClassPositionSettingsDiv .= '<tr>';
				$ClassPositionSettingsDiv .= '<td>';
					$ClassPositionSettingsDiv .= $eReportCard['GreaterThanGrandAverage'].': ';
					$ClassPositionSettingsDiv .= $ClassMarkSelection;
					$ClassPositionSettingsDiv .= '&nbsp;&nbsp;';
					$ClassPositionSettingsDiv .= $eReportCard['Mark(s)'];
				$ClassPositionSettingsDiv .= '</td>';
			$ClassPositionSettingsDiv .= '</tr>';
		}
	$ClassPositionSettingsDiv .= '</table>';
}
$ClassPositionSettingsDiv .= '</div>';

### Form Position Settings
### Class Position Settings
$FormPositionSettingsDiv = '';
$FormPositionSettingsDiv .= '<div id="formPositionSettingDiv" style="display:block;">'."\n";

if ($eRCTemplateSetting['DisplayPosition']['SubjectPositionDisplaySettings'])
{
	$FormPositionSettingsDiv .= $lreportcard_ui->Get_Ranking_Display_Table($ReportID, $ClassLevelID, 'Form');
	$FormPositionSettingsDiv .= '<span class="tabletextremark">'.$eReportCard['PositionRangeDisplayRemarks'].'</span>';
}
else
{
	# Mark Selection 
	$FormMarkSelection = "<select name='GreaterThanAverageForm' id='GreaterThanAverageForm'>";
	for($i=0; $i<=10;$i++)
		$FormMarkSelection .= "<option value='$i' ".($i==$GreaterThanAverageForm?"SELECTED":"").">$i</option>";
	$FormMarkSelection .= "</select>";
	
	$FormPositionSettingsDiv ='';
	$FormPositionSettingsDiv .= '<table id="formPositionTable" class="form_table" style="display:block; border:1px solid #CCCCCC;">'."\n";
		$FormPositionSettingsDiv .= '<tr>';
			$FormPositionSettingsDiv .= '<td>';
				$FormPositionSettingsDiv .= $eReportCard['MaximumPosition'].': ';
				$FormPositionSettingsDiv .= '<input name="OverallPositionRangeForm" type="text" class="ratebox" maxlength="3" value="'.$OverallPositionRangeForm.'" />';
				$FormPositionSettingsDiv .= '&nbsp;&nbsp;';
				$FormPositionSettingsDiv .= '<span class="tabletextremark">'.$eReportCard['MaximumPositionRemarks'].'</span>';
			$FormPositionSettingsDiv .= '</td>';
		$FormPositionSettingsDiv .= '</tr>';
		
		if ($eRCTemplateSetting['DisplayPosition']['AverageChecking'])
		{
			$FormPositionSettingsDiv .= '<tr>';
				$FormPositionSettingsDiv .= '<td>';
					$FormPositionSettingsDiv .= $eReportCard['GreaterThanGrandAverage'].': ';
					$FormPositionSettingsDiv .= $FormMarkSelection;
					$FormPositionSettingsDiv .= '&nbsp;&nbsp;';
					$FormPositionSettingsDiv .= $eReportCard['Mark(s)'];
				$FormPositionSettingsDiv .= '</td>';
			$FormPositionSettingsDiv .= '</tr>';
		}
	$FormPositionSettingsDiv .= '</table>';
}
$FormPositionSettingsDiv .= '</div>';

$OtherInfoRow = '';
if (count((array)$eRCTemplateSetting['TemplateSettings']['ShowOtherInfo']) > 0) {
	$OtherInfoRow .= '<tr valign="top">'."\n";
		$OtherInfoRow .= '<td colspan="2">'."\n";
			$OtherInfoRow .= '<em class="form_sep_title">'.Get_Selection_First_Title($eReportCard['Management_OtherInfo']).'</em>'."\n";
		$OtherInfoRow .= '</td>'."\n";
	$OtherInfoRow .= '</tr>'."\n";
}

if (in_array('AttendanceDays', (array)$eRCTemplateSetting['TemplateSettings']['ShowOtherInfo']))
{
	$OtherInfoRow .= '<tr valign="top">'."\n";
		$OtherInfoRow .= '<td class="field_title">'.$eReportCard['AttendanceDays'].'</td>'."\n";
		$OtherInfoRow .= '<td>'."\n";
			$OtherInfoRow .= '<input name="AttendanceDays" type="text" class="ratebox" maxlength="3" value="'.$AttendanceDays.'" />'."\n";
		$OtherInfoRow .= '</td>'."\n";
	$OtherInfoRow .= '</tr>'."\n";
}

if (in_array('SpecialPercentage', (array)$eRCTemplateSetting['TemplateSettings']['ShowOtherInfo']))
{
	$OtherInfoRow .= '<tr valign="top">'."\n";
		$OtherInfoRow .= '<td class="field_title">'.$eReportCard['SpecialPercentage'].'</td>'."\n";
		$OtherInfoRow .= '<td>'."\n";
			$OtherInfoRow .= '<input name="SpecialPercentage" type="text" class="ratebox" maxlength="3" value="'.$SpecialPercentage.'" /> %'."\n";
		$OtherInfoRow .= '</td>'."\n";
	$OtherInfoRow .= '</tr>'."\n";
}

if (in_array('PrintOption', (array)$eRCTemplateSetting['TemplateSettings']['ShowOtherInfo']))
{
	$OtherInfoRow .= '<tr valign="top">'."\n";
		$OtherInfoRow .= '<td class="field_title">'.$eReportCard['PrintOption'].'</td>'."\n";
		$OtherInfoRow .= '<td>'."\n";
			$thisChecked = 
			$OtherInfoRow .= $linterface->Get_Radio_Button('DisplayOption1Radio', 'DisplayOption', 1, ($DisplayOption==1), $Class="", $eReportCard['PrintOption_1'])."\n";
			$OtherInfoRow .= $linterface->Get_Radio_Button('DisplayOption2Radio', 'DisplayOption', 2, ($DisplayOption==2), $Class="", $eReportCard['PrintOption_2'])."\n";
		$OtherInfoRow .= '</td>'."\n";
	$OtherInfoRow .= '</tr>'."\n";
}

if (in_array('PrincipalName', (array)$eRCTemplateSetting['TemplateSettings']['ShowOtherInfo']))
{
	$OtherInfoRow .= '<tr valign="top">'."\n";
		$OtherInfoRow .= '<td class="field_title">'.$eReportCard['PrincipalName'].'</td>'."\n";
		$OtherInfoRow .= '<td width="25%">'."\n";
			$OtherInfoRow .= '<input name="PrincipalName" type="text" class="textbox_name" value="'.$PrincipalName.'" />'."\n";
		$OtherInfoRow .= '</td>'."\n";
	$OtherInfoRow .= '</tr>'."\n";
}

if (in_array("DisplayCommentSection", (array)$eRCTemplateSetting['TemplateSettings']['ShowOtherInfo']))
{
	$checked = $DisplayCommentSection ? "checked" : "";
	
	$OtherInfoRow .= '<tr valign="top">'."\n";
		$OtherInfoRow .= '<td class="field_title">'.$eReportCard['DisplayCommentSection'].'</td>'."\n";
		$OtherInfoRow .= '<td>'."\n";
			$OtherInfoRow .= '<input type="checkbox" name="DisplayCommentSection" id="DisplayCommentSection" value="1" '.$checked.'>';
		$OtherInfoRow .= '</td>'."\n";
	$OtherInfoRow .= '</tr>'."\n";
}

if (in_array("DisplayAwardSection", (array)$eRCTemplateSetting['TemplateSettings']['ShowOtherInfo']))
{
	$checked = $DisplayAwardSection ? "checked" : "";
	
	$DisplayAwardSettingsDiv = '';
	$DisplayAwardSettingsDiv .= '<div id="DisplayAwardSettingDiv" style="display:block;">'."\n";
		$DisplayAwardSettingsDiv .= '<table id="DisplayAwardSettingTable" class="form_table" style="display:block; border:1px solid #CCCCCC;">'."\n";
		$DisplayAwardSettingsDiv .= '<tr>';
			$DisplayAwardSettingsDiv .= '<td>';
				$DisplayAwardSettingsDiv .= $eReportCard['MaxActivityCount'].': ';
				$DisplayAwardSettingsDiv .= '<input name="MaxActivityCount" id="MaxActivityCount" type="text" class="ratebox" maxlength="3" value="'.$MaxActivityCount.'" />';
				$DisplayAwardSettingsDiv .= '<br/>';
				$DisplayAwardSettingsDiv .= '<span class="tabletextremark">'.$eReportCard['MaxActivityCountRemark'].'</span>';
				$DisplayAwardSettingsDiv .= '<br/>';
				$DisplayAwardSettingsDiv .= '<span class="tabletextremark">'.$eReportCard['MaxActivityCountRemark2'].'</span>';
			$DisplayAwardSettingsDiv .= '</td>';
		$DisplayAwardSettingsDiv .= '</tr>';
		$DisplayAwardSettingsDiv .= '</table>';
	$DisplayAwardSettingsDiv .= '</div>';
	
	$OtherInfoRow .= '<tr valign="top">'."\n";
		$OtherInfoRow .= '<td class="field_title">'.$eReportCard['DisplayAwardSection'].'</td>'."\n";
		$OtherInfoRow .= '<td>'."\n";
			$OtherInfoRow .= '<input type="checkbox" name="DisplayAwardSection" id="DisplayAwardSection" value="1" '.$checked.'><br/>';
			$OtherInfoRow .= $DisplayAwardSettingsDiv;
		$OtherInfoRow .= '</td>'."\n";
	$OtherInfoRow .= '</tr>'."\n";
}

if (in_array("DisplayActivitySection", (array)$eRCTemplateSetting['TemplateSettings']['ShowOtherInfo']))
{
	$checked = $DisplayActivitySection ? "checked" : "";
	
	$OtherInfoRow .= '<tr valign="top">'."\n";
		$OtherInfoRow .= '<td class="field_title">'.$eReportCard['DisplayActivitySection'].'</td>'."\n";
		$OtherInfoRow .= '<td>'."\n";
			$OtherInfoRow .= '<input type="checkbox" name="DisplayActivitySection" id="DisplayActivitySection" value="1" '.$checked.'>';
		$OtherInfoRow .= '</td>'."\n";
	$OtherInfoRow .= '</tr>'."\n";
}

$h_ctcMaxLengthTable = '';
$h_stcMaxLengthTable = '';
if ($eRCTemplateSetting['ClassTeacherComment']['MaxLengthSettings'] || $eRCTemplateSetting['SubjectTeacherComment']['MaxLengthSettings']) {
	
	$lreportcard_comment = new libreportcard_comment();
	$commentMaxLengthInfoArr = $lreportcard_comment->Get_Comment_MaxLength_Info($ReportID);
	
	if ($eRCTemplateSetting['ClassTeacherComment']['MaxLengthSettings']) {
		$ctcMaxLength = ($commentMaxLengthInfoArr[0]['MaxLength']=='')? $lreportcard->Get_Class_Teacher_Comment_MaxLength() : $commentMaxLengthInfoArr[0]['MaxLength']; 
		
		$h_ctcMaxLengthTable .= '<table class="form_table" style="display:block; border:1px solid #CCCCCC;">'."\r\n";
			$h_ctcMaxLengthTable .= '<tr>'."\r\n";
				$h_ctcMaxLengthTable .= '<td>'.$Lang['eReportCard']['CommentArr']['MaxNumberOfChar'].': <input class="textboxnum" id="ctcMaxLength" name="ctcMaxLength" value="'.$ctcMaxLength.'" /></td>'."\r\n";
			$h_ctcMaxLengthTable .= '</tr>'."\r\n";
		$h_ctcMaxLengthTable .= '</table>'."\r\n";
	}
	
	if ($eRCTemplateSetting['SubjectTeacherComment']['MaxLengthSettings']) {
		$h_stcMaxLengthTable = $lreportcard_ui->Get_Subject_Teacher_Comment_MaxLength_Settings_Table($ReportID);
	}
}

$imageTypeAry = $eRCTemplateSetting['Settings']['TemplateSettings_ImageAry'];
$numOfImage = count($imageTypeAry);
$x = '';
for ($i=0; $i<$numOfImage; $i++) {
	$_imageType = $imageTypeAry[$i];

	$infoAry = $lreportcard_file->returnFileInfoAry($ReportID, $_imageType);
	$fileId = $infoAry[0]['FileID'];
	$fileObj = new libreportcard_file($fileId);
	
	$x .= '<tr valign="top">'."\r\n";
		$x .= '<td class="field_title" valign="top" class="formfieldtitle"><span class="tabletext">'.$Lang['eReportCard']['SettingsArr']['TemplateSettingsArr']['ImageArr'][$_imageType].'</span></td>'."\r\n";
		$x .= '<td class="tabletext">'."\r\n";
			$x .= '<div><input type="file" id="image_'.$_imageType.'" name="image_'.$_imageType.'" class="textboxtext imageFile" /></div>'."\r\n";
			$x .= $linterface->Get_Form_Warning_Msg(''.$_imageType.'_fileTypeWarnDiv', $Lang['eReportCard']['SettingsArr']['TeacherExtraInfoArr']['SignatureFileTypeWarning'], 'warnDiv');
			if ($fileObj->getFilePath() != '') {
				$x .= '<div id="'.$_imageType.'_previewDiv">'."\r\n";
					$x .= $linterface->GET_LNK_DELETE('javascript:void(0);', $Lang['eReportCard']['SettingsArr']['TemplateSettingsArr']['DeleteImage'], 'deleteImage(\''.$_imageType.'\');', $ParClass="delete", $WithSpan=1);
					$x .= $linterface->Get_Form_Warning_Msg(''.$_imageType.'_deleteWarnDiv', $Lang['eReportCard']['SettingsArr']['TemplateSettingsArr']['DeleteImageFailed'], 'warnDiv');
					$x .= $fileObj->returnPreviewImage();
				$x .= '</div>'."\r\n";
			}
		$x .= '</td>'."\r\n";
	$x .= '</tr>'."\r\n";
}
$htmlAry['reportImageTr'] = $x;
?>

<br />
<form name="form1" method="post" action="new4_update.php" enctype="multipart/form-data">
<table width="96%" border="0" cellspacing="0" cellpadding="4">
<tr>
	<td>
		<?= $lreportcard_ui->Get_Settings_ReportCardTemplate_Nav() ?>
		<br />
		<?= $linterface->GET_STEPS($STEPS_OBJ) ?>   
		<br />	
		<?= $lreportcard_ui->Get_Settings_ReportCardTemplate_ReportCardInfo_Table($ReportID) ?>
		<br />
		
		<table class="form_table_v30" width="100%" border="0" cellspacing="0" cellpadding="5">
		<tr>
          	<td align="right" colspan="2"><?=$linterface->GET_SYS_MSG($msg, $xmsg);?></td>
      	</tr>
		<tr valign="top">
			<td class="field_title"><span class="tabletext"><?=$eReportCard['Header']?></span></td>
			<td class="tabletext">
				<input type="radio" name="Header" id="header0" value="-1" <?=($basic_data['HeaderHeight']==-1 or $basic_data['HeaderHeight']=="")?"checked":"";?>> <label for="header0"><?=$eReportCard['WithDefaul Header']?></label> <br>
				<input type="radio" name="Header" id="header1" value="1" <?=$basic_data['HeaderHeight']>-1?"checked":"";?>> <label for="header1"><?=$eReportCard['NoHeader']?> <?=$eReportCard['NumberOfEmptyLine']?>: <?=$HeaderNoSelect?></label>
			</td>
		</tr>
		
		<tr valign="top">
			<td class="field_title"><span class="tabletext"><?=$eReportCard['Footer']?></span></td>
			<td class="tabletext"><?=$linterface->GET_TEXTAREA("Footer", $basic_data['Footer'])?></td>
		</tr>
		
		<tr valign="top">
			<td class="field_title"><span class="tabletext"><span class='tabletextrequire'>*</span><?=$eReportCard['LineHeight']?> </span></td>
			<td class="tabletext"><input name="LineHeight" type="text" class="ratebox" maxlength="2" value="<?= ($basic_data['LineHeight']=='')? '12' : $basic_data['LineHeight']; ?>"> px</td>
		</tr>
               
        <? if ($student_info != '') { ?>
		<tr valign="top">
			<td class="field_title"><span class="tabletext"><?=$eReportCard['DisplayStudentInformation']?></span></td>
			<td class="tabletext"><?=$student_info?></td>
		</tr>
		<? } ?>
			
		<tr valign="top">
			<td class="field_title"><span class="tabletext"><?=$eReportCard['ShowSubjectComponent']?></span></td>
			<td class="tabletext">
				<input type="checkbox" name="ShowSubjectComponent" id="ShowSubjectComponent" value="1" <?=$basic_data['ShowSubjectComponent']==1?"checked":""?>> <label for="ShowSubjectComponent"></label> 
			</td>
		</tr>
		         
		<tr valign="top">
			<td class="field_title"><span class="tabletext"><?=$eReportCard['ShowSubjectFullMark']?></span></td>
			<td class="tabletext">
				<input type="checkbox" name="ShowSubjectFullMark" id="ShowSubjectFullMark" value="1" <?=$basic_data['ShowSubjectFullMark']==1?"checked":""?>> <label for="ShowSubjectFullMark"></label> 
			</td>
		</tr>
		
		<tr valign="top">
			<td class="field_title"><span class="tabletext"><?=$eReportCard['ShowSubjectOverall']?></span></td>
			<td class="tabletext">
				<input type="checkbox" name="ShowSubjectOverall" id="ShowSubjectOverall" value="1" <?=$basic_data['ShowSubjectOverall']==1?"checked":""?>> <label for="ShowSubjectOverall"></label> 
			</td>
		</tr>
		
		<tr valign="top">
			<td class="field_title"><span class="tabletext"><?=$eReportCard['DisplayOverallResult']?></span></td>
			<td class="tabletext">
				<input type="checkbox" name="ShowGrandTotal" id="ShowGrandTotal" value="1" <?=$basic_data['ShowGrandTotal']==1?"checked":""?>> <label for="ShowGrandTotal"></label> 
			</td>
		</tr>
		
		<tr valign="top">
			<td class="field_title"><span class="tabletext"><?=$eReportCard['DisplayGrandAvg']?></span></td>
			<td class="tabletext">
				<input type="checkbox" name="ShowGrandAvg" id="ShowGrandAvg" value="1" <?=$basic_data['ShowGrandAvg']==1?"checked":""?>> <label for="ShowGrandAvg"></label> 
			</td>
		</tr>
		
		<tr valign="top">
			<td class="field_title"><span class="tabletext"><?=$eReportCard['ShowClassPosition']?></span></td>
			<td class="tabletext">
				<input type="checkbox" name="ShowOverallPositionClass" id="ShowOverallPositionClass" value="1" onclick="js_EnableDisable_Position_Setting_Table('class', this.checked);" <?=$basic_data['ShowOverallPositionClass']==1?"checked":""?>> <label for="ShowOverallPositionClass"></label> 
				<br />
				<?=$ClassPositionSettingsDiv?>
			</td>
		</tr>
		 
		<tr valign="top">
			<td class="field_title"><span class="tabletext"><?=$eReportCard['ShowClassNumOfStudent']?></span></td>
			<td class="tabletext">
				<input type="checkbox" name="ShowNumOfStudentClass" id="ShowNumOfStudentClass" value="1" <?=$basic_data['ShowNumOfStudentClass']==1?"checked":""?>> <label for="ShowNumOfStudentClass"></label> 
			</td>
		</tr>
		
		<tr valign="top">
			<td class="field_title"><span class="tabletext"><?=$eReportCard['ShowFormPosition']?></span></td>
			<td class="tabletext">
				<input type="checkbox" name="ShowOverallPositionForm" id="ShowOverallPositionForm" value="1" onclick="js_EnableDisable_Position_Setting_Table('form', this.checked);" <?=$basic_data['ShowOverallPositionForm']==1?"checked":""?>> <label for="ShowOverallPositionForm"></label> 
				<br />
				<?=$FormPositionSettingsDiv?>
			</td>
		</tr>
		
		<tr valign="top">
			<td class="field_title" ><span class="tabletext"><?=$eReportCard['ShowFormNumOfStudent']?></span></td>
			<td class="tabletext">
				<input type="checkbox" name="ShowNumOfStudentForm" id="ShowNumOfStudentForm" value="1" <?=$basic_data['ShowNumOfStudentForm']==1?"checked":""?>> <label for="ShowNumOfStudentForm"></label> 
			</td>
		</tr>
		
		<? if($ReportType=="F") {?>
		<tr valign="top">
			<td class="field_title"><span class="tabletext"><?=$eReportCard['DisplayColumn']?></span></td>
			<td class="tabletext"><?=$displayCol?></td>
		</tr>
		<? } ?>
		      
		<tr valign="top">
			<td class="field_title"><span class="tabletext"><?=$eReportCard['CommentFrom']?></span></td>
			<td class="tabletext">
				<input type="checkbox" name="AllowClassTeacherComment" id="AllowClassTeacherComment" value="1" <?=$basic_data['AllowClassTeacherComment']==1?"checked":""?>> <label for="AllowClassTeacherComment"><?=$eReportCard['ClassTeache']?></label>
				<?=$h_ctcMaxLengthTable?>
				<br />  
				<input type="checkbox" name="AllowSubjectTeacherComment" id="AllowSubjectTeacherComment" value="1" <?=$basic_data['AllowSubjectTeacherComment']==1?"checked":""?>> <label for="AllowSubjectTeacherComment"><?=$eReportCard['SubjectTeacher']?></label>
				<?=$h_stcMaxLengthTable?> 
			</td>
		</tr>
		
		<?= $OtherInfoRow ?>
		<?= $htmlAry['reportImageTr'] ?>
		</table>
	</td>
</tr>
</table>

<br />
<table width="96%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="left" class="tabletextremark">&nbsp;&nbsp;&nbsp;<?=$i_general_required_field2?></td>
	</tr>
  	<tr>
    	<td height="1" class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
  	</tr>
  	<tr>
    	<td align="center">
    	<?= $linterface->GET_ACTION_BTN($button_finish, "button", "checkform(document.form1, 0);") ?>
      	<?= $linterface->GET_ACTION_BTN($button_finish_preview, "button", "checkform(document.form1, 1);") ?>
      	<?= $linterface->GET_ACTION_BTN($button_reset, "reset") ?>
		<?= $linterface->GET_ACTION_BTN($button_back, "button", "window.location='$goBackUrl'") ?>
		</td>
  	</tr>
</table>

<input type="hidden" name="ReportID" value="<?=$ReportID?>">
<input type="hidden" name="Continue" value="0">

</form>
<br />

<script language="javascript">

function js_Go_Back() {
	window.location='index.php';
}

<!--
function checkform(obj, jsContinue)
{
	if(!check_int(obj.LineHeight, "", "<?=$i_alert_pleasefillin?><?=$eReportCard['LineHeight']?>.")) return false;
	
	var canSubmit = true;
	<? if ($eRCTemplateSetting['DisplayPosition']['SubjectPositionDisplaySettings'] == true) { ?>
		var jsThisValue = '';
		
		// Check Upper Limit Textbox
		$('input.UpperLimitTb').each( function() {
			jsThisValue = $(this).val();
			if (isNaN(jsThisValue) || jsThisValue < 0)
			{
				alert("<?=sprintf($eReportCard['jsSthMustBePositive'], $eReportCard['UpperLimit'])?>");
				$(this).focus();
				canSubmit = false;
				return false;
			}
		});
		
		// Check Above Average Mark Textbox
		$('input.AboveAverageMarkTb').each( function() {
			jsThisValue = $(this).val();
			if (isNaN(jsThisValue) || jsThisValue < 0)
			{
				alert("<?=sprintf($eReportCard['jsSthMustBePositive'], $eReportCard['AboveAverageMark'])?>");
				$(this).focus();
				canSubmit = false;
				return false;
			}
		});
		
		if (canSubmit)
		{
			$('.PositionDisplayElement_Class, .PositionDisplayElement_Form').each( function() {
				$(this).attr('disabled', false);
			});
		}
	<? }
	else { ?>
		if(!check_positive_int(obj.OverallPositionRangeClass, "<?=sprintf($eReportCard['jsSthMustBePositive'], $eReportCard['MaximumPosition'])?>")) return false;
		if(!check_positive_int(obj.OverallPositionRangeForm, "<?=sprintf($eReportCard['jsSthMustBePositive'], $eReportCard['MaximumPosition'])?>")) return false;
	<? } ?> 
	
	<? if (in_array('AttendanceDays', (array)$eRCTemplateSetting['TemplateSettings']['ShowOtherInfo'])) { ?>
		if(!check_positive_int(obj.AttendanceDays, "<?=sprintf($eReportCard['jsSthMustBePositive'], $eReportCard['AttendanceDays'])?>")) return false;
	<? } ?>
	
	var imageExtAry = new Array('.gif', '.jpg', '.jpeg', '.png', '.bmp');
	$('input.imageFile').each( function() {
		var _imageExt = getFileExtension(document.getElementById($(this).attr('id'))).toLowerCase();
		if (_imageExt != '' && !jIN_ARRAY(imageExtAry, _imageExt)) {
			var _fileType = $(this).attr('id').replace('image_', '');
			
			$('div#' + _fileType + '_fileTypeWarnDiv').show();
			$(this).focus();
			
			canSubmit = false;
		}
	})
			
	if (canSubmit == true) {
		document.form1.Continue.value = jsContinue;
		obj.submit();
	}
}

function js_EnableDisable_Position_Setting_Table(jsRecordType, jsChecked)
{
	var jsTargetClass = (jsRecordType == 'class')? 'PositionDisplayElement_Class' : 'PositionDisplayElement_Form';
	
	var disabledStatus;
	if (jsChecked == true)
		disabledStatus = false;
	else
		disabledStatus = true;
		
	$('.' + jsTargetClass).each( function() {
		$(this).attr('disabled', disabledStatus);
	});
	DisableRow();
}

function js_ShowHide_Percentage_Symbol(jsValue, jsSubjectID, jsType)
{
	if (jsValue == 2 || jsValue == 3)
		$('span#' + 'PercentageSymbolSpan_' + jsType + '_' + jsSubjectID).show();
	else
		$('span#' + 'PercentageSymbolSpan_' + jsType + '_' + jsSubjectID).hide();
}

function jsApplyToAll(targetValueElement, targetElementType, targetPositionType)
{
	var isDisabled = $('#' + targetValueElement).attr('disabled')
	if (isDisabled == false)
	{
		var targetValue = $('#' + targetValueElement).val();
		var targetClass = targetElementType + '_' + targetPositionType;
		
		$('.' + targetClass).each( function () {
			$(this).val(targetValue);
		});
		
		// show hide the '%' symbol if changing the range type
		if (targetElementType == 'RangeType')
		{
			if (targetValue == 1)
			{
				$('.PercentageSymbolSpan_' + targetPositionType).each( function () {
					$(this).hide();
				});
			}
			else
			{
				$('.PercentageSymbolSpan_' + targetPositionType).each( function () {
					$(this).show();
				});
			}
		}
	}
}

function SetAll(checked,PositionType)
{
	$("input.DisplayPositionCheckBox"+PositionType).attr("checked",checked);
	DisableRow();
}

function DisableRow(obj)
{
	if(obj)
	{
		setDisable = !($(obj).attr("checked"));
		$(obj).parent().siblings().find("input, select").attr("disabled",setDisable);
	}
	else
	{
		$("input.DisplayPositionCheckBoxClass:not(':disabled'),input.DisplayPositionCheckBoxForm:not(':disabled')").each(function(){
			setDisable = !($(this).attr("checked"));
			$(this).parent().siblings().find("input, select").attr("disabled",setDisable);
		});
	}
}

function deleteImage(imageType) {
	$.post(
		"ajax_task.php", 
		{ 
			task: "deleteImage",
			reportId: '<?=$ReportID?>',
			imageType: imageType
		},
		function(ReturnData) {
			if (ReturnData == '1') {
				$('div#' + imageType + '_deleteWarnDiv').hide();
				$('div#' + imageType + '_previewDiv').html('');
			}
			else {
				$('div#' + imageType + 'WarnDiv').show();
			}
		}
	);
}

$('document').ready( function() {
	<? //if ($basic_data['ShowOverallPositionClass'] != 1) { ?>
	var checked = $("input#ShowOverallPositionClass").attr("checked")
		js_EnableDisable_Position_Setting_Table('class', checked);
	<? //} ?>
	<? //if ($basic_data['ShowOverallPositionForm'] != 1) { ?>
	var checked = $("input#ShowOverallPositionForm").attr("checked")
		js_EnableDisable_Position_Setting_Table('form', checked);
	<?// } ?>
	DisableRow();
});
//-->
</script>

<?
print $linterface->FOCUS_ON_LOAD("form1.Header[0]");
$linterface->LAYOUT_STOP();
intranet_closedb();
?>