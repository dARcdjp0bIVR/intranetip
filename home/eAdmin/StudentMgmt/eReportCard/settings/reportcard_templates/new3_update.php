<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008w.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if($skip)
{
	header("Location: new4.php?ReportID=$ReportID");
	exit;
}

$lreportcard = new libreportcard2008w();

$basic_data = $lreportcard->returnReportTemplateBasicInfo($ReportID);
$ClassLevelID = $basic_data['ClassLevelID'];

if(is_array($ReportColumnID))		// $cal=2, horizontal 
{
	//update Basic Info (PercentageOnColumnWeight)
	$data1[PercentageOnColumnWeight]= $usePercentage;
	$data1[DateModified] = date("Y-m-d H:i:s");
	$lreportcard->UpdateReportTemplateBasicInfo($ReportID, $data1);
	
	$del_result = $lreportcard->DELETE_REPORT_TEMPLATE_SUBJECT_WEIGHT('ReportID', $ReportID, 'SubjectID is NULL and ReportColumnID is not NULL');
		
	$i=0;
	foreach($ReportColumnID as $key => $ColID)
	{
		$data[$i]['ReportColumnID'] = $ColID;
		$data[$i]['SubjectID'] = '';
		$data[$i]['Weight'] = $usePercentage ? $Ratio[$key]/100 : $Ratio[$key];
		$i++;	
	}
	
	$lreportcard->InsertReportTemplateSubjectWeight($ReportID, $data);
}
else
{
	// save the DefaultWeight
	$del_result = $lreportcard->DELETE_REPORT_TEMPLATE_SUBJECT_WEIGHT('ReportID', $ReportID, 'ReportColumnID is NULL and SubjectID is NULL');
	$tdata[0]['ReportColumnID'] = '';
	$tdata[0]['SubjectID'] = '';
	$tdata[0]['Weight'] = $DefaultWeight;
	$lreportcard->InsertReportTemplateSubjectWeight($ReportID, $tdata);
	
	$del_result = $lreportcard->DELETE_REPORT_TEMPLATE_SUBJECT_WEIGHT('ReportID', $ReportID, 'ReportColumnID is NULL and SubjectID is not NULL');
	
	$i = 0;	
	$data = array();
	$SubjectIDAry = $lreportcard->returnSubjectIDwOrder($ClassLevelID, $ExcludeCmpSubject=0, $ReportID);
	foreach($SubjectIDAry as $SubjKey => $SubjID)
	{
		$data[$i]['ReportColumnID'] = '';
		$data[$i]['SubjectID'] = $SubjID;
		$data[$i]['Weight'] = $Weight0[$SubjKey];
		$i++;	
	}
	
	$lreportcard->InsertReportTemplateSubjectWeight($ReportID, $data);
	
	$lreportcard->UPDATE_REPORT_LAST_DATE($ReportID, 'DateModified');
}


if ($eRCTemplateSetting['IndividualVerticalSubjectWeightForEachAssessments']) {
	### Save the Assessment Vertical Weight
	//$VerticalWeightArr[$ReportColumnID][$SubjectID] = data	<= $SubjectID = 0 means default subject weight	
	$VerticalWeightArr = $_POST['VerticalWeightArr'];
	
	$SuccessArr['SaveAssessmentSubjectVerticalWeight'] = $lreportcard->Save_Assessment_Subject_Vertical_Weight($ReportID, $VerticalWeightArr);
}



//Check  Re-Generate
if($basic_data['LastGenerated'] != "")
	$xmsg = $eReportCard['SettingUpdateReminder'];
	
if($Continue)
	header("Location: new4.php?ReportID=$ReportID&xmsg=$xmsg");
else
	header("Location: new3.php?ReportID=$ReportID&xmsg=$xmsg");

?>