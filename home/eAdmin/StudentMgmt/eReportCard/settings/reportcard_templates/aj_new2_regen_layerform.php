<?
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008w.php");

intranet_auth();
intranet_opendb();

$lreportcard = new libreportcard2008w();

if($UpdateColumnTitle)
{
	$data = $lreportcard->returnReportTemplateColumnData($ReportID, "ReportColumnID=$ColID");
	$ColumnTitle = $data[0]['ColumnTitle'];
	//$ColumnTitle =  (convert2unicode($data[0]['ColumnTitle'], 1, 2));
	$ColumnTitle =  $data[0]['ColumnTitle'];
	$ShowPosition = $data[0]['ShowPosition'];
	$DisplayOrder = $data[0]['DisplayOrder'];
	// list($PositionRange1, $PositionRange2) = split(",",$data[0]['PositionRange']);
	list($PositionRange1, $PositionRange2) = explode(",",$data[0]['PositionRange']);
	$BlockMarksheet = $data[0]['BlockMarksheet'];
}
$DisplayOrder = strlen($DisplayOrder) ? $DisplayOrder : -2;
$basic_data = $lreportcard->returnReportTemplateBasicInfo($ReportID);	// Basic Data
$ReportType = $basic_data['Semester'];
// START Build Assesment Select for addBtn
		$ColumnTitle2Ary = $lreportcard->returnReportColoumnTitleWithOrder($ReportID);
		$t = 0;
		$selected = -1;
		$OrderSelection[$t] = array(-1, "(". $eReportCard['AtBegin'] .")");
		if(sizeof($ColumnTitle2Ary))
		{
			foreach($ColumnTitle2Ary as $Order => $ColName)
			{
				if($Order==$DisplayOrder)	
				{	
					$selected = $OrderSelection[$t][0];
					continue;
				}
				$t++;
				$OrderSelection[$t][0] = $Order;
				$OrderSelection[$t][1] = $ColName;
			}
		}
		$OrderSelect = getSelectByArray($OrderSelection, "name='DisplayOrder' id='DisplayOrder'",$selected,0,1);
// END Build Assesment selection for addBtn

$x ="
  <table align=\"center\" width=\"100%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\">
      <tr>
        <td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle\"><span class=\"tabletext\">". $eReportCard['ColumnTitle'] .":</span></td>
        <td width=\"80%\" valign=\"top\" class=\"tabletext\"><input name=\"column_title\" id=\"column_title\" type=\"text\" class=\"textboxtext\" value=\"". $ColumnTitle ."\"></td>
      </tr>
      <tr>
        <td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle\" style=\"border-bottom:1px #EEEEEE solid;\"><span class=\"tabletext\">". $eReportCard['InsertAfter'] ."</span></td>
        <td valign=\"top\"><div id=\"selectPosition_div\">". $OrderSelect ."</div></td>
      </tr>
      <tr valign=\"top\">
        <td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle\"><span class=\"tabletext\">". $eReportCard['ShowPosition'] ."</span></td>
        <td class=\"tabletext\">
			<LABEL for=\"show_position0\"><input type=\"radio\" name=\"show_positionT\" id=\"show_position0\" ". ($ShowPosition==0?"checked":"")."/>". $eReportCard['DonotShowPosition'] ."</LABEL><br>
			<LABEL for=\"show_position1\"><input type=\"radio\" name=\"show_positionT\" id=\"show_position1\" ". ($ShowPosition==1?"checked":"")."/>". $eReportCard['ShowPositionInClass'] ."</LABEL><br>
			<LABEL for=\"show_position2\"><input type=\"radio\" name=\"show_positionT\" id=\"show_position2\" ". ($ShowPosition==2?"checked":"")."/>". $eReportCard['ShowPositioninClassLevel'] ."</LABEL><br>
            <table border=\"0\" cellspacing=\"0\" cellpadding=\"1\">
              <tr>
                <td width=\"20\" valign=\"top\" class=\"tabletext\">". $i_From ."</td>
                <td class=\"tabletext\"><input name=\"PositionRange1\" id=\"PositionRange1\" type=\"text\" class=\"textboxnum\" value=\"". $PositionRange1 ."\"></td>
                <td class=\"tabletext\">". $i_To ."</td>
                <td class=\"tabletext\"><input name=\"PositionRange2\" id=\"PositionRange2\" type=\"text\" class=\"textboxnum\" value=\"". $PositionRange2 ."\"></td>
              </tr>
          	</table>
        </td>
      </tr>
	  <tr valign=\"top\">
		<td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle\"><span class=\"tabletext\">".$eReportCard['BlockMarksheet']."</span></td>
		<td class=\"tabletext\"><input name=\"BlockMarksheet\" id=\"BlockMarksheet\" type=\"checkbox\" ".($BlockMarksheet=="true"?"checked":"")." value='true'></td>
	  </tr>
    </table>
";

intranet_closedb();

echo $x;


?>