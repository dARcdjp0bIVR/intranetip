<?php

##########################
#
#	Date: 	2016-06-30	Bill	[2015-1104-1130-08164]
#			- Create file
#			- copy from /subject_topics/export.php
#
##########################

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_award.php");

$lexport = new libexporttext();
$lreportcard = new libreportcardcustom();
$lreportcard_award = new libreportcard_award();

$lreportcard->hasAccessRight();

// Get all forms
$formAry = $lreportcard->GET_ALL_FORMS($hasTemplateOnly=false, $excludeWithoutClass=false);
$formNameList = Get_Array_By_Key((array)$formAry, "LevelName");	

// Get all awards
$AwardInfoAssoArr = $lreportcard_award->Load_All_School_Award_Info();

// CSV Header
$csvHeaderAry = array();
$csvHeaderAry[] = $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['CsvHeaderArr']['En']['AwardCode'];
$csvHeaderAry[] = $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['CsvHeaderArr']['En']['AwardName'];
$csvHeaderAry[] = $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['CsvHeaderArr']['En']['AwardForm'];
	
// CSV Data
$dataAry = array();

// loop awards
$rowCount = 0;
foreach ((array)$AwardInfoAssoArr as $thisAwardID => $thisAwardInfoArr)
{
	// Get Award Info
	$thisAwardCode = $thisAwardInfoArr['BasicInfo']['AwardCode'];
	$thisAwardName = Get_Lang_Selection($thisAwardInfoArr['BasicInfo']['AwardNameCh'], $thisAwardInfoArr['BasicInfo']['AwardNameEn']);
	
	// Only export manual input awards
	if(!$thisAwardInfoArr['BasicInfo']['NotGenerateAward'])
		continue;
	
	// Get Award related Form
	$thisAwardFormList = array();
	$thisAwardForm = $thisAwardInfoArr['ApplicableFormInfo'];
	
	// loop related forms
	foreach((array)$thisAwardForm as $yearid => $thisForm)
		$thisAwardFormList[] = $thisForm["YearName"];
	
	$thisAwardFormList = array_filter((array)$thisAwardFormList);
	$thisAwardFormDisplay = implode("", (array)$thisAwardFormList);
	
	// Add to data array
	$dataAry[$rowCount][] = $thisAwardCode;
	$dataAry[$rowCount][] = $thisAwardName;
	$dataAry[$rowCount][] = $thisAwardFormDisplay;
	$rowCount++;
}

// Export
$exportContent = $lexport->GET_EXPORT_TXT($dataAry, $csvHeaderAry);

intranet_closedb();
	
// Output file to user browser
$fileName = 'School_Awards.csv';
$lexport->EXPORT_FILE($fileName, $exportContent);

?>