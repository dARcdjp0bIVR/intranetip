<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_pc.php");
$lreportcard_pc = new libreportcard_pc();
$lreportcard_pc->hasAccessRight();

$ScaleIDArr = $_GET['ScaleIDArr'];

$lreportcard_pc->Start_Trans();
$success = $lreportcard_pc->Delete_Scale($ScaleIDArr);


if ($success) {
	$lreportcard_pc->Commit_Trans();
	$ReturnMsgKey = 'DeleteScaleSuccess';
}
else{
	$lreportcard_pc->RollBack_Trans();
	$ReturnMsgKey = 'DeleteScaleFailed';
}


intranet_closedb();
header("Location: scale.php?ReturnMsgKey=".$ReturnMsgKey);
?>