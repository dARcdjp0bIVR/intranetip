<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
$lreportcard = new libreportcardcustom();
$lreportcard->hasAccessRight();

$lreportcard_ui = new libreportcard_ui();
$linterface = new interface_html();
$lclass = new libclass();


$CurrentPage = "PersonalCharacteristics";
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();

# tag information
//$TAGS_OBJ[] = array($eReportCard['PersonalCharacteristicsPool'], "index.php", 0);
//$TAGS_OBJ[] = array($eReportCard['PersonalCharacteristicsFormSettings'], "assign.php", 1);
$TAGS_OBJ = $lreportcard_ui->Get_Settings_PersonalCharacteristics_Tag_Array('formSettings');
$linterface->LAYOUT_START();

$PAGE_NAVIGATION[] = array($eReportCard['PersonalCharacteristicsFormSettings']);

$dataTmp = $lreportcard->returnPersonalCharSettingData($ClassLevelID, $SubjectID);
$data = array();
foreach($dataTmp as $k=>$d)
	$data[] = $d['CharID'];	

# Build Personal Characteristics Options
$CharData = $lreportcard->returnPersonalCharData();
foreach($CharData as $k=>$d)
{
	$checked = in_array($d['CharID'], $data) ? "checked" : "";
	$x .= "<input type='checkbox' name='CharIDAry[]' id='CharID_". $d['CharID'] ."' value='". $d['CharID'] ."' $checked><label for='CharID_". $d['CharID'] ."'>". $d['Title_EN'] . " (" . $d['Title_CH'] .")</label><br>";
}

if ($SubjectID == 0)
	$SubjectName = $eReportCard['Template']['OverallCombined'];
else
	$SubjectName = $lreportcard->GET_SUBJECT_NAME_LANG($SubjectID);

?>

<br />   
<form name="form1" method="post" action="assign_edit_update.php">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	<td align='right'><?=$linterface->GET_SYS_MSG($xmsg);?></td>
</tr>

<tr>
	<td colspan="2">
		<table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr> 
			<td>
				<br />
				<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
				<tr valign='top'>
					<td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'><span class='tabletext'><?=$eReportCard['Form']?></span> </td>
					<td class='tabletext'><?=$lclass->getLevelName($ClassLevelID)?></td>
				</tr>
				<tr valign='top'>
					<td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'><span class='tabletext'><?=$eReportCard['Subject']?></span> </td>
					<td class='tabletext'><?=$SubjectName?></td>
				</tr>
				<tr valign='top'>
					<td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'><span class='tabletext'><?=$eReportCard['PersonalCharacteristics']?></span> </td>
					<td class='tabletext'><?=$x?></td>
				</tr>
				<tr valign='top'>
					<td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'><span class='tabletext'><?=$eReportCard['ApplyOtherSubjects']?></span> </td>
					<td class='tabletext'><input type="checkbox" name="applyothers" value="1"></td>
				</tr>
				</table>
			</td>
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td colspan="2">        
		<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr>
			<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
		</tr>
		<tr>
			<td align="center">
				<?= $linterface->GET_ACTION_BTN($button_submit, "submit") ?>
				<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2") ?>
				<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='subject_list.php?ClassLevelID=". $ClassLevelID."'") ?>
			</td>
		</tr>
		</table>                                
	</td>
</tr>

</table>                        
<br />

<input type="hidden" name="ClassLevelID" value="<?=$ClassLevelID?>">
<input type="hidden" name="SubjectID" value="<?=$SubjectID?>">
</form>


<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>