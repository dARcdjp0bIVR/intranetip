<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
$lreportcard = new libreportcardcustom();
$lreportcard_ui = new libreportcard_ui();

$lreportcard->hasAccessRight();

$ReturnMsgKey = $_GET['ReturnMsgKey'];


$linterface = new interface_html();
$CurrentPage = "PersonalCharacteristics";
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
$TAGS_OBJ = $lreportcard_ui->Get_Settings_PersonalCharacteristics_Tag_Array('scale');

$ReturnMsg = $Lang['eReportCard']['PersonalCharArr']['ReturnMessageArr'][$ReturnMsgKey];
$linterface->LAYOUT_START($ReturnMsg);
echo $linterface->Include_JS_CSS();
echo $lreportcard_ui->Get_Settings_PersonalCharacteristics_Scale_UI();
?>
<script language="javascript">
$(document).ready( function() {	
	js_Init_DND_Table();
});

function js_Go_New_Scale() {
	window.location = 'scale_new_step1.php';
}

function js_Init_DND_Table() {
	$(".common_table_list_v30").tableDnD({
		onDrop: function(table, DroppedRow) {
			if(table.id == "ContentTable") {
				Block_Element(table.id);
				
				// Get the order string
				var rowArr = table.tBodies[0].rows;
				var numOfRow = rowArr.length;
				var recordIDArr = new Array();
				for (var i=0; i<numOfRow; i++) {
					var tempId = parseInt(rowArr[i].id.replace('tr_', ''));
					if (tempId != "" && !isNaN(tempId)) {
						recordIDArr[recordIDArr.length] = tempId;
					}
				}
				var recordIDStr = recordIDArr.join(',');
				
				// Update DB
				$.post(
					"ajax_update.php", 
					{ 
						Action: "Reorder_Scale",
						DisplayOrderString: recordIDStr
					},
					function(ReturnData)
					{
						// Reset the ranking display
						var jsRowCounter = 0;
						$('span.rowNumSpan').each( function () {
							$(this).html(++jsRowCounter);
						})
						
						// Get system message
						if (ReturnData == '1') {
							jsReturnMsg = '<?=$Lang['eReportCard']['PersonalCharArr']['ReturnMsgArr']['ScaleReorderSuccess']?>';
						}
						else {
							jsReturnMsg = '<?=$Lang['eReportCard']['PersonalCharArr']['ReturnMsgArr']['ScaleReorderFailed']?>';
						}
						
						UnBlock_Element(table.id);
						Scroll_To_Top();
						Get_Return_Message(jsReturnMsg);
					}
				);
			}
		},
		onDragStart: function(table, DraggedRow) {
			//$('#debugArea').html("Started dragging row "+row.id);
			
		},
		dragHandle: "Dragable", 
		onDragClass: "move_selected"
	});
}

function goDeleteScale() {
	var jsSelectedScaleIDArr = new Array();
	$('input.ScaleChk:checked').each( function() {
		jsSelectedScaleIDArr[jsSelectedScaleIDArr.length] = $(this).attr('scaleID');
	});
	var jsSelectedScaleIDList = jsSelectedScaleIDArr.join(',');
	
	$.post(
		"ajax_validate.php", 
		{ 
			Action: "Is_Scale_Linked_Record",
			ScaleIDList: jsSelectedScaleIDList
		},
		function(ReturnData) {
			var jsCanSubmit = false;
			if (ReturnData == '1') {
				if (confirm('<?=$Lang['eReportCard']['PersonalCharArr']['WarningArr']['ScaleLinkedToDataAlready']?>')) {
					jsCanSubmit = true;
				}
				else {
					jsCanSubmit = false;
				}
			}
			else {
				jsCanSubmit = true;
			}
			
			if (jsCanSubmit) {
				$('form#form1').attr('action', 'scale_delete.php').submit();
			}
		}
	);
}

</script>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>