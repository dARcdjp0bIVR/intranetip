<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") {
	include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
	$lreportcard = new libreportcardcustom();
} else {
	$lreportcard = new libreportcard();
}

// [2020-1109-1332-40206]
$Title_EN = trim(stripslashes($Title_EN));
$Title_CH = trim(stripslashes($Title_CH));
$Description = trim(stripslashes($Description));

// [2020-0708-0950-12308]
//$result = $lreportcard->insertPersonalCharItem($Title_EN, $Title_CH, $DisplayOrder);
$result = $lreportcard->insertPersonalCharItem($Title_EN, $Title_CH, $Description, $DisplayOrder);
$xmsg = $result ? "add" : "add_failed";

intranet_closedb();
header("Location: index.php?xmsg=$xmsg");

?>