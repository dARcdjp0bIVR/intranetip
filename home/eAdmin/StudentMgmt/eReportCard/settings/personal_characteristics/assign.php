<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
$lreportcard = new libreportcardcustom();
$lreportcard->hasAccessRight();

$lreportcard_ui = new libreportcard_ui();
$linterface = new interface_html();
$CurrentPage = "PersonalCharacteristics";
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();

# tag information
//$TAGS_OBJ[] = array($eReportCard['PersonalCharacteristicsPool'], "index.php", 0);
//$TAGS_OBJ[] = array($eReportCard['PersonalCharacteristicsFormSettings'], "assign.php", 1);
$TAGS_OBJ = $lreportcard_ui->Get_Settings_PersonalCharacteristics_Tag_Array('formSettings');
$linterface->LAYOUT_START();

$sql = "
	select 
		CONCAT(\"<a href='subject_list.php?ClassLevelID=\", a.YearID,\"' class='tablelink'>\", a.YearName, \"</a>\"),
		b.DateModified 
	from 
		YEAR as a 
		left join ". $lreportcard->DBName .".RC_LEVEL_SUBJECT_PERSONAL_CHARACTERISTICS as b on (a.YearID = b.ClassLevelID)
	group by 
		a.YearID
";

$li = new libdbtable2007(0, 1, 0);
$li->field_array = array("a.YearName");

$li->sql = $sql;
$li->no_col = 2;
$li->IsColOff = "CurriculumExpectationList";
$li->column_list .= "<td class='tabletop tabletopnolink' width='70%'>".$eReportCard['Form']."</td>\n";
$li->column_list .= "<td class='tabletop tabletopnolink' width='30%'>".$eReportCard['LastModifiedDate']."</td>\n";

?>

<br />
<form name="form1" method="get">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td align="center">
		<table width="96%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td align="left" class="tabletext">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td> </td>
					<td align="right" height='30'><?=$linterface->GET_SYS_MSG($xmsg, $xmsg2);?></td>
				</tr>
				<tr>
					<td colspan="2"><?=$li->display();?></td>
				</tr>
				</table>
			</td>
		</tr>
		</table>
	</td>
</tr>
</table>
<br />

</form>



<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>