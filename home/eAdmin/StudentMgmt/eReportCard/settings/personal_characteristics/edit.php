<?php
// Using:

/********************** Change Log ***********************/
#
#	Date:	2020-10-30 (Bill)	[2020-0708-0950-12308]
#			add description for personal characteristics
#
/******************* End Of Change Log *******************/

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");

$lreportcard = new libreportcardcustom();
$lreportcard->hasAccessRight();

$lreportcard_ui = new libreportcard_ui();
$linterface = new interface_html();

$CurrentPage = "PersonalCharacteristics";
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();

# tag information
//$TAGS_OBJ[] = array($eReportCard['PersonalCharacteristicsPool'], "index.php", 1);
//$TAGS_OBJ[] = array($eReportCard['PersonalCharacteristicsFormSettings'], "assign.php", 0);
$TAGS_OBJ = $lreportcard_ui->Get_Settings_PersonalCharacteristics_Tag_Array('pool');
$linterface->LAYOUT_START();

$PAGE_NAVIGATION[] = array($eReportCard['EditPersonalCharacteristics'] );

# retrieve data
if (is_array($CharID)) {
	$CharID = $CharID[0];
}
$temp = $lreportcard->returnPersonalCharData($CharID);
$data = $temp[0];
?>

<script language="javascript">
function checkform(obj){
     if(!check_text(obj.Title_EN, "<?php echo $i_alert_pleasefillin.$eReportCard['Title_EN']; ?>.")) return false;
     if(!check_text(obj.Title_CH, "<?php echo $i_alert_pleasefillin.$eReportCard['Title_CH']; ?>.")) return false;
     if(!check_positive_nonzero_int(obj.DisplayOrder, "<?php echo sprintf($eReportCard['jsSthMustBePositive'], $eReportCard['DisplayOrder']); ?>.")) return false;
}
</script>

<br />   
<form name="form1" method="post" action="edit_update.php" onSubmit="return checkform(this);">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	<td align='right'><?=$linterface->GET_SYS_MSG($xmsg);?></td>
</tr>

<tr>
	<td colspan="2">
		<table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr> 
			<td>
				<br />
				<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
				<tr valign='top'>
					<td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'><span class='tabletext'><?=$eReportCard['Title_EN']?></span> <span class='tabletextrequire'>*</span></td>
					<td class='tabletext'><input type='text' name='Title_EN' maxlength='255' value='<?=$data['Title_EN']?>' class='textboxtext'></td>
				</tr>
				<tr valign='top'>
					<td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'><span class='tabletext'><?=$eReportCard['Title_CH']?></span> <span class='tabletextrequire'>*</span></td>
					<td class='tabletext'><input type='text' name='Title_CH' maxlength='255' value='<?=$data['Title_CH']?>' class='textboxtext'></td>
				</tr>
                <tr valign='top'>
                    <td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'><span class='tabletext'><?=$i_general_description?></span> <span class='tabletextrequire'>*</span></td>
                    <td class='tabletext'>
                        <textarea class="textboxtext" name="Description" id="Description" cols="70" rows="5" wrap="virtual" onFocus="this.rows=7;"><?=$data['Description']?></textarea>
                    </td>
                </tr>
				<tr valign='top'>
					<td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'><span class='tabletext'><?=$eReportCard['DisplayOrder']?></span> <span class='tabletextrequire'>*</span></td>
					<td class='tabletext'><input type='text' name='DisplayOrder' maxlength='3' value='<?=$data['DisplayOrder']?>' class='textboxnum'></td>
				</tr>
				</table>
			</td>
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td colspan="2">        
		<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr>
			<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
		</tr>
		<tr>
			<td align="center">
				<?= $linterface->GET_ACTION_BTN($button_submit, "submit") ?>
				<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2") ?>
				<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='index.php'") ?>
			</td>
		</tr>
		</table>                                
	</td>
</tr>

</table>                        
<br />

<input type="hidden" name="CharID" value="<?=$CharID?>">
</form>

<?
print $linterface->FOCUS_ON_LOAD("form1.Title_EN");
$linterface->LAYOUT_STOP();
intranet_closedb();
?>