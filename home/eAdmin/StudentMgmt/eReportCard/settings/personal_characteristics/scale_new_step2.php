<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
$lreportcard = new libreportcardcustom();
$lreportcard_ui = new libreportcard_ui();

$lreportcard->hasAccessRight();


$ScaleID = $_GET['ScaleID'];
$FromNew = $_GET['FromNew'];
$ReturnMsgKey = $_GET['ReturnMsgKey'];


$linterface = new interface_html();
$CurrentPage = "PersonalCharacteristics";
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
$TAGS_OBJ = $lreportcard_ui->Get_Settings_PersonalCharacteristics_Tag_Array('scale');

$ReturnMsg = $Lang['eReportCard']['PersonalCharArr']['ReturnMessageArr'][$ReturnMsgKey];
$linterface->LAYOUT_START($ReturnMsg);
echo $linterface->Include_AutoComplete_JS_CSS();
echo $lreportcard_ui->Get_Settings_PersonalCharacteristics_Scale_Step2_UI($ScaleID, $FromNew);
?>
<script language="javascript">
var jsScaleID = '';
$(document).ready( function() {	
	// initialize jQuery Auto Complete plugin
	jsScaleID = $('input#ScaleID').val();
	Init_JQuery_AutoComplete('UserSearchTb');
	
	$('input#UserSearchTb').focus();
});


var AutoCompleteObj;
function Init_JQuery_AutoComplete(InputID) {
	AutoCompleteObj = $("input#" + InputID).autocomplete("ajax_search_user.php",
		{  			
			onItemSelect: function(li) {
				Add_Selected_User(li.extra[1],li.selectValue);
			},
			formatItem: function(row) {
				return row[0] + " (LoginID: " + row[1] + ")";
			},
			maxItemsToShow: 5,
			minChars: 1,
			delay: 0,
			width: 200
		}
	);
	
	Update_Auto_Complete_Extra_Para();
}

function Add_Selected_User(UserID, UserName) {
	var UserID = UserID || "";
	var UserName = UserName || "";
	var UserSelected = document.getElementById('SelectedUserIDArr[]');

	UserSelected.options[UserSelected.length] = new Option(UserName, UserID);
	Update_Auto_Complete_Extra_Para();
	
	$('input#UserSearchTb').val('').focus();
}

function Update_Auto_Complete_Extra_Para() {
	ExtraPara = new Array();
	ExtraPara['SelectedUserIDList'] = Get_Selection_Value('SelectedUserIDArr[]', 'Array', true);
	ExtraPara['ScaleID'] = jsScaleID;
	AutoCompleteObj[0].autocompleter.setExtraParams(ExtraPara);
}

function js_Add_Member()
{
	var objForm = document.getElementById('form1');
	
	// Check if the selections contains current members of this group
	var jsSelectedUserList = Get_Selection_Value('SelectedUserIDArr[]', 'String', true);
	
	$.post(
		"ajax_validate.php", 
		{
			Action: "Validate_Selected_Member", 
			ScaleID : jsScaleID,
			SelectedUserList : jsSelectedUserList
		},
		function(ReturnData)
		{
			if (ReturnData == '') {
				$('div#MemberSelectionWarningDiv').hide();
				checkOptionAll(objForm.elements["SelectedUserIDArr[]"]);
				objForm.action = 'scale_new_step2_update.php';
				objForm.submit();
			}
			else {
				var OptionValueArr = ReturnData.split(',');
				var numOfOption = OptionValueArr.length;
				
				for (i=0; i<numOfOption; i++) {
					$("select#SelectedUserIDArr\\[\\] option[value='" + OptionValueArr[i] + "']").attr('selected', 'selected');
				}
					 
				$('div#MemberSelectionWarningDiv').show();
			}
		}
	);
}

function js_Back_To_Scale_List() {
	window.location = 'scale.php';
}

function js_Back_To_Member_List() {
	window.location = 'scale_member_list.php?ScaleID=' + jsScaleID;
}
</script>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>