<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();
 
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_pc.php");

$lreportcard_pc = new libreportcard_pc();
$lreportcard_pc->hasAccessRight();

$Action = $_REQUEST['Action'];
if ($Action == 'Reorder_Scale') {
	$DisplayOrderString = $_POST['DisplayOrderString'];
	
	$RecordIDArr = explode(',', $DisplayOrderString);
	$numOfRecord = count($RecordIDArr);
	
	$lreportcard_pc->Start_Trans();
	
	$SuccessArr = array();
	for ($i=0; $i<$numOfRecord; $i++) {
		$thisScaleID = $RecordIDArr[$i];
		
		$thisDataArr = array();
		$thisDataArr['DisplayOrder'] = $i + 1; 
		
		$SuccessArr[$thisRecordID] = $lreportcard_pc->Update_Scale_Record($thisScaleID, $thisDataArr);
	}
	
	if (in_array(false, $SuccessArr)) {
		$lreportcard_pc->RollBack_Trans();
		echo '0';
	}
	else {
		$lreportcard_pc->Commit_Trans();
		echo '1';
	}
}

intranet_closedb();
?>