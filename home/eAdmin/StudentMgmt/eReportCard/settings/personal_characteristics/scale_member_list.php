<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_pc.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
$lreportcard_pc = new libreportcard_pc();
$lreportcard_ui = new libreportcard_ui();
$lreportcard_pc->hasAccessRight();


### Cookies handling
$arrCookies = array();
$arrCookies[] = array("ck_settings_personal_char_mgmt_user_page_size", "numPerPage");
$arrCookies[] = array("ck_settings_personal_char_mgmt_user_page_number", "pageNo");
$arrCookies[] = array("ck_settings_personal_char_mgmt_user_page_order", "order");
$arrCookies[] = array("ck_settings_personal_char_mgmt_user_page_field", "field");
$arrCookies[] = array("ck_settings_personal_char_mgmt_user_page_keyword", "Keyword");
//$Keyword = (isset($_POST['Keyword']))? $_POST['Keyword'] : $_COOKIE['Keyword'];
if(isset($clearCoo) && $clearCoo == 1) {
	clearCookies($arrCookies);
	$ck_settings_personal_char_mgmt_user_page_size = '';
	$Keyword = '';
}
else {
	updateGetCookies($arrCookies);
}


$ScaleID = $_GET['ScaleID'];
$ReturnMsgKey = $_GET['ReturnMsgKey'];

$linterface = new interface_html();
$CurrentPage = "PersonalCharacteristics";
$MODULE_OBJ = $lreportcard_pc->GET_MODULE_OBJ_ARR();
$TAGS_OBJ = $lreportcard_ui->Get_Settings_PersonalCharacteristics_Tag_Array('scale');

$ReturnMsg = $Lang['eReportCard']['PersonalCharArr']['ReturnMessageArr'][$ReturnMsgKey];
$linterface->LAYOUT_START($ReturnMsg);
echo $lreportcard_ui->Get_Settings_PersonalCharacteristics_Scale_Mgmt_User_UI($ScaleID, $pageNo, $order, $field);
?>
<script language="javascript">
$(document).ready( function() {
	
});

function js_Back_To_Scale_List() {
	window.location = 'scale.php';
}
</script>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>