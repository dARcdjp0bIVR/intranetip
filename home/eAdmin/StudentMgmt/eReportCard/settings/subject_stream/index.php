<?php
#################################
#	Change Log:
#
#
#
#	Date:	2019-12-17 Philips
#			Create this file
#
##################################
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
//debug_pr($ReportCardCustomSchoolName);
$linterface = new interface_html();
$lreportcard = new libreportcardcustom();
$lreportcard_ui = new libreportcard_ui();

$lreportcard->hasAccessRight();
if(!$eRCTemplateSetting['Settings']['StudentSettings_SubjectStream']){
	No_Access_Right_Pop_Up();
}
$CurrentPage = "Settings_SubjectStream";
$TAGS_OBJ = array();
$TAGS_OBJ[] = array($eReportCard['Settings_SubjectStream']);
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
$fcm = new form_class_manage();

### Filtering
$ClassLevelID = '';
if (isset($_POST['ClassLevelID'])) {
	$ClassLevelID = $_POST['ClassLevelID'];
} else if (isset($_GET['ClassLevelID'])) {
	$ClassLevelID = $_GET['ClassLevelID'];
}
if($ClassLevelID == -1) $ClassLevelID = '';
$YearSelectionBox = $lreportcard_ui->Get_Form_Selection('ClassLevelID', $ClassLevelID, 'document.form1.submit();', $noFirst=1, $isAll=1);

$classAry = $fcm->Get_Class_List_By_Academic_Year($lreportcard->Get_Active_Year_ID(), $ClassLevelID);

$classIDAry = Get_Array_By_Key($classAry, 'YearClassID');

$table = "YEAR_CLASS_USER ycu";
$join1 = "YEAR_CLASS yc";
$join1cond = "yc.YearClassID = ycu.YearClassID";
$join2 = $lreportcard->DBName . '.RC_STUDENT_SUBJECT_STREAM rsss';
$join2cond = "ycu.UserID = rsss.StudentID";
$col = "ycu.YearClassID AS YearClassID,
 COUNT(IF(rsss.SubjectStream IS NULL, 1, 1)) AS TotalCount,
 COUNT(IF(rsss.SubjectStream = 'A', 1, NULL)) AS ArtCount,
 COUNT(IF(rsss.SubjectStream = 'S', 1, NULL)) AS ScienceCount,
 MAX(rsss.DateModified) AS LastModified";
$cond = "AND yc.AcademicYearID = '" . $lreportcard->GET_ACTIVE_YEAR_ID() . "' ";
if($ClassLevelID != ''){
	$cond .= "AND ycu.YearClassID IN ('" . implode("','", $classIDAry) . "') ";
}
$groupBy = "ycu.YearClassID";
$sql = "
SELECT $col
FROM $table
INNER JOIN $join1 ON $join1cond
LEFT OUTER JOIN $join2 ON $join2cond
WHERE 1 $cond
GROUP BY $groupBy
";

$classTotal = $lreportcard->returnArray($sql);
$classTotalAry = BuildMultiKeyAssoc($classTotal, 'YearClassID');

$table = "<table id='Content_Table' width='100%' border='0' cellpadding='4' cellspacing='1' bgcolor='#CCCCCC'>";
$table .= "<thead>
			<tr class='tabletop'>
				<th width='50%'>". $Lang['General']['Class'] ."</th>
				<th width='10%'>". $Lang['General']['Total']."</th>
				<th width='10%'>". $eReportCard['SubjectStream']['Art']."</th>
				<th width='10%'>". $eReprotCard['SubjectStream']['Science']."</th>
				<th width='20%' align='center'>". $Lang['General']['LastModified']."</th>
			</tr>
		</thead>";
$table .= "<tbody>";
foreach($classAry as $classInfo){
	list($YearID, $YearName, $YearClassID, $ClassTitleEN, $ClassTitleB5, $Form_WebSAMSCode) = $classInfo;
	$classTitle = Get_Lang_Selection($ClassTitleB5, $ClassTitleEN);
	$classSSInfo = $classTotalAry[$YearClassID];
	if($classSSInfo != null){
		$table .= "<tr>
						<td class='tabletext' valign='top'>
							<a class='tablelink' href='javascript:editClass(\"{$YearClassID}\")'>{$classTitle}</a>
						</td>
						<td class='tabletext' valign='top'>
						".$classSSInfo['TotalCount']."
						</td>
						<td class='tabletext' valign='top'>
							".$classSSInfo['ArtCount']."
						</td>
						<td class='tabletext' valign='top'>
							".$classSSInfo['ScienceCount']."
						</td>
						<td class='tabletext' valign='top' align='center'>
							".($classSSInfo['LastModified'] ? $classSSInfo['LastModified'] : $Lang['General']['NoRecordAtThisMoment'])."
						</td>
					</tr>";
	}
}
$table .= "</tbody>";
$table .= "</table>";
$style = "<style>
table#Content_Table tbody tr:nth-child(odd){
	background-color: #FFFFFF;
}
table#Content_Table tbody tr:nth-child(even){
	background-color: #F3F3F3;
}
</style>";
$linterface->LAYOUT_START($eReportCard['SubjectStream']['ReturnMsg'][$xmsg]);
echo $style;
?>
<form name="form1" id="form1" method="POST" action="">
	<div class="table_board">
		<div class="table_filter">
			<?php echo $YearSelectionBox;?>
		</div>
	</div>
<br/>
<br/>
<?php
echo $table;
?>
</form>
<script type="text/javascript">
function editClass(classid){
	window.location.href = "edit.php?classID=" + classid;
}
</script>
<?php 
$linterface->LAYOUT_STOP();
intranet_closedb();
?>