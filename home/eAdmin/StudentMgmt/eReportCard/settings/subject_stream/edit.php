<?php
#################################
#	Change Log:
#
#
#
#	Date:	2019-12-17 Philips
#			Create this file
#
##################################
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
//debug_pr($ReportCardCustomSchoolName);
$linterface = new interface_html();
$lreportcard = new libreportcardcustom();
$lreportcard_ui = new libreportcard_ui();
$lclass = new libclass();

$lreportcard->hasAccessRight();
$CurrentPage = "Settings_SubjectStream";
$TAGS_OBJ = array();
$TAGS_OBJ[] = array($eReportCard['Settings_SubjectStream']);
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();

if(!isset($_GET['classID']) || $classID == '' || !$eRCTemplateSetting['Settings']['StudentSettings_SubjectStream']){
	No_Access_Right_Pop_Up();
} else {
	$classID = $_GET['classID'];
}
$className = $lclass->getClassNameByLang($classID);
$studentList = $lclass->getStudentByClassId($classID);
$studentInfoList = BuildMultiKeyAssoc($studentList, 'UserID');
sortByColumn($studentInfoList, 'ClassNumber');
$idAry = array_keys($studentInfoList);
$subjectStreamAry = $lreportcard->GET_STUDENT_SUBJECT_STREAM($idAry);
$subjectStreamAry = BuildMultiKeyAssoc($subjectStreamAry, 'StudentID');
$table = "<table id='Content_Table' class='common_table_list_v30 edit_table_list_v30' width='100%' >";
$table .= "<thead>
			<tr class='tabletop'>
				<th width='5%' align='left' style='text-align:left'>". $Lang['General']['ClassNumber'] ."</th>
				<th width='25%' align='left' style='text-align:left'>". $Lang['General']['StudentName'] ."</th>
				<th width='70%' align='center'>". $eReportCard['SubjectStream']['Selection'] ."</th>
			</tr>
		</thead>";
$table .= "<tbody>";
$selectArray = array(
		array("", $Lang['General']['NA']),
		array("A", $eReportCard['SubjectStream']['Art']),
		array("S", $eReprotCard['SubjectStream']['Science'])
);
foreach($idAry as $studentId){
	$luser = new libuser($studentId);
	$subjectStream = $subjectStreamAry[$studentId];
	$classNumber = $studentInfoList[$studentId]['ClassNumber'];
	$studentName = $luser->UserNameLang();
	$selectionBox = $linterface->GET_SELECTION_BOX($selectArray, "name='subjectStream[{$studentId}]'", "", $subjectStream);
	$table .= "<tr>
					<td class='tabletext' valign='top' align='left'>
						{$classNumber}
					</td>
					<td class='tabletext' valign='top' align='left'>
						{$studentName}
					</td>
					<td class='tabletext' valign='top' align='center'>
						{$selectionBox}
					</td>
				</tr>";
}
$table .= "</tbody>";
$table .= "</table>";
$toolbar = "";
$style = "<style>
.common_table_list_v30 tr th{
text-align: center;
}
</style>";
$linterface->LAYOUT_START();
?>
<?php
	echo $style;
	echo $linterface->GET_NAVIGATION2_IP25($className);
	?>
<form id="form1" name="form1" method="POST" action="update.php">
<?php
	echo $table;
?>
<div class="edit_bottom_v30">
	<?= $linterface->GET_ACTION_BTN($button_save, "submit", "")?>
	&nbsp;
	<?= $linterface->GET_ACTION_BTN($button_back, "button", "window.location.href='index.php'")?>
	<p class="spacer"></p>
</div>
<input type="hidden" name="classID" value="<?php echo $classID?>" />
</form>
<?php 
$linterface->LAYOUT_STOP();
intranet_closedb();
?>