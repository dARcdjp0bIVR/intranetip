<?php
#################################
#	Change Log:
#
#
#
#	Date:	2019-12-17 Philips
#			Create this file
#
##################################
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
//debug_pr($ReportCardCustomSchoolName);
$linterface = new interface_html();
$lreportcard = new libreportcardcustom();
$lreportcard_ui = new libreportcard_ui();

$lreportcard->hasAccessRight();
if(!isset($subjectStream) || !$eRCTemplateSetting['Settings']['StudentSettings_SubjectStream']){
	No_Access_Right_Pop_Up();
}

if(sizeof($subjectStream) > 0){
	$dataAry = array();
	foreach($subjectStream as $sid => $subjectStream){
		$dataAry[] = array("StudentID" => $sid, "SubjectStream" => $subjectStream);
	}
	$result = $lreportcard->EDIT_STUDENT_SUBJECT_STREAM($dataAry);
	$returnMsg = (!empty($result)) ? 'UpdateSuccess' : 'UpdateFailed';
} else {
	$returnMsg = "UpdateFailed";
}
header("location:index.php?xmsg=" . $returnMsg);
intranet_closedb();
?>