<?php
$PATH_WRT_ROOT = "../../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
//include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage_ui.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if (!$plugin['ReportCard2008']) {
	echo "You have no priviledge to access this page.";
	exit();
}

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
$lreportcard = new libreportcard();

if (!$lreportcard->hasAccessRight()) {
	echo "You have no priviledge to access this page.";
	exit();
}

$MODULE_OBJ['title'] = $iDiscipline['select_students'];
$linterface = new interface_html("popup.html");
$linterface->LAYOUT_START();
/*
$lclass = new libclass();
$select_class = $lclass->getSelectClass("name=\"targetClass\" onChange=\"this.form.action='';this.form.submit()\"", $targetClass, '', '', $lreportcard->schoolYearID);

if ($targetClass != "")
    $select_students = $lclass->getStudentSelectByClass($targetClass,"size=\"21\" multiple=\"multiple\" name=\"targetID[]\"");
*/
$libFCM_ui = new form_class_manage_ui();
$select_class = $libFCM_ui->Get_YearClass_Selection("targetClass", $targetClass, "this.form.action='';this.form.submit();", $lreportcard->schoolYearID);
    
if ($targetClass != "")
	$select_students = $libFCM_ui->Get_Student_Selection("targetID[]", $targetClass, '', '', $noFirst=1, $isMultiple=1);
?>

<script language="javascript">
function isExists(obj2,val){
	for(j=0;j<obj2.options.length;j++)
		if(obj2.options[j].value==val)
			return true;
	return false;
}
function AddOptions(obj){
     par = opener.window;
     parObj = opener.window.document.form1.elements["<?php echo $fieldname; ?>"];

     checkOption(obj);
     par.checkOption(parObj);
     i = obj.selectedIndex;
     
     while(i!=-1)
     {
         addtext = obj.options[i].text;
		 
          if(!isExists(parObj,obj.options[i].value)){
          	par.checkOptionAdd(parObj, addtext, obj.options[i].value);
          }
          obj.options[i] = null;
          i = obj.selectedIndex;
     }
}

function SelectAll(obj)
{
	 for (i=0; i<obj.length; i++)
	 {
		  obj.options[i].selected = true;
	 }
}

</script>
<br />
<form name="form1" action="index.php" method="post">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>
			<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr>
					<td valign="top" nowrap="nowrap" class='tabletext'>
						<?=$eReportCard['Class']?>:
					</td>
					<td width="80%"><?=$select_class?></td>
				</tr>
				<?php if($targetClass != "") { ?>
				<tr>
					<td class="dotline" colspan="2"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" class='tabletext'>
						<span class="tabletext"><?= $eReportCard['Student']?>:</span>
					</td>
					<td width="80%">
						<table width="100%" cellpadding="0" cellspacing="0">
							<tr>
								<td><?= $select_students ?></td>
								<td valign="bottom">
									<table width="100%" border="0" cellspacing="0" cellpadding="6">
										<tr> 
											<td align="left"> 
												<?= $linterface->GET_SMALL_BTN($button_add, "button", "checkOption(this.form.elements['targetID[]']);AddOptions(this.form.elements['targetID[]'])")?>
											</td>
										</tr>
										<tr> 
											<td align="left"> 
												<?= $linterface->GET_SMALL_BTN($button_select_all, "button", "SelectAll(this.form.elements['targetID[]']); return false;")?>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<?php } ?>
				<tr>
                	<td class="dotline" colspan="2"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
				<tr>
					<td align="center" colspan="2">
						<?= $linterface->GET_ACTION_BTN($button_close, "button", "self.close();") ?>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<input type="hidden" name="fieldname" value="<?php echo $fieldname; ?>" />
</form>
</br />

<script language="javascript">
if (document.getElementById('targetID[]'))
	SelectAll(document.getElementById('targetID[]'));
</script>

<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>