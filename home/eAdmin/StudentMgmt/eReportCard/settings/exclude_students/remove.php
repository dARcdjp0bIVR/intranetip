<?php
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");

intranet_auth();
intranet_opendb();

if (!isset($delRecordIDAry) || sizeof($delRecordIDAry) == 0)
	header("Location: index.php?Result=delete_failed");

$ClassLevelID = $_POST['ClassLevelID'];
$YearTermID = $_POST['YearTermID'];

$lreportcard = new libreportcard();
$success = $lreportcard->DELETE_EXCLUDE_ORDER_STUDENTS($delRecordIDAry);


intranet_closedb();

$Result = ($success==1) ? "delete" : "delete_failed";
header("Location: index.php?Result=$Result&LevelName=$LevelName&YearTermID=$YearTermID&ClassLevelID=$ClassLevelID");
?>