<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
//include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if ($plugin['ReportCard2008']) {
	include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
	if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
	{
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
		$lreportcard = new libreportcardcustom();
	} else {
		$lreportcard = new libreportcard();
	}
	include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
	$lreportcard = new libreportcard();
	
	$CurrentPage = "Settings_ExcludeStudents";
	$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
	
	if ($lreportcard->hasAccessRight()) {
		
		$linterface = new interface_html();
		$lclass = new libclass();
		
		$Result = "";
		//$excludeList = $lreportcard->GET_EXCLUDE_ORDER_STUDENTS();
		
		if(!isset($studentID))
			$studentID = array();
			
		# submitType 1 : add by class name ( class number)
		# submitType 2 : add by user login 
		if($submitType==1 && $user_id!=""){
			if(!in_array($user_id,$studentID))
				array_push($studentID,$user_id);
		}
		else if($submitType==2 && $user_login !=""){
			$sql ="SELECT UserID FROM INTRANET_USER WHERE UserLogin='$user_login' AND RecordType=2 AND RecordStatus IN (0,1,2)";
			$temp = $lclass->returnVector($sql);
			if($temp[0]!="" && !in_array($temp,$studentID))
				array_push($studentID,$temp[0]);
		}
		
		if(sizeof($studentID)>0) {
			$studentID = array_remove_empty($studentID);
			$selected_student_list = implode(",",$studentID);
			$namefield = getNameFieldWithClassNumberByLang("");
			$sql ="SELECT UserID,$namefield FROM INTRANET_USER WHERE RecordType=2 AND RecordStatus IN (0,1,2) AND UserID IN($selected_student_list) ORDER BY ClassName,ClassNumber";
			$temp = $lclass->returnArray($sql,2);
		} else {
			$temp = array();
			$selected_student_list="''";
		}
		
		if (count($temp) == 0)
			$temp[] = array('', "&nbsp;&nbsp;&nbsp;&nbsp;");
		
		$select_student = getSelectByArray($temp," name=\"studentID[]\" size=\"5\" multiple","",0,1);
		
		$select_class = $lclass->getSelectClassID("name=\"class\" onChange=\"changeClass()\"", $class, $DisplaySelect=0, $lreportcard->schoolYearID);
		//$select_class = $lclass->getSelectClass("name=\"class\" onChange=\"changeClass()\"",$class,0);
		
		if($class!="") {
			//$namefield = getNameFieldWithClassNumberByLang("");
			//$sql ="SELECT UserID,$namefield FROM INTRANET_USER WHERE RecordType=2 AND RecordStatus IN (0,1,2) AND ClassName='$class' AND UserID NOT IN ($selected_student_list) ORDER BY ClassName,ClassNumber";
			//$temp = $lclass->returnArray($sql,2);
			
			$StudentArr = $lreportcard->GET_STUDENT_BY_CLASS($class, $ParStudentIDList="", $isShowBothLangs=0, $withClassNumber=1);
			$numOfStudent = count($StudentArr);
			$temp = array();
			for ($i=0; $i<$numOfStudent; $i++)
			{
				$thisStudentID = $StudentArr[$i]['UserID'];
				$thisStudentName = $StudentArr[$i]['StudentName'];
				
				$temp[] = array($thisStudentID, $thisStudentName);
			}
			
			$select_classnum=getSelectByArray($temp,"name='user_id'",$user_id,0,0);
			$btn = $linterface->GET_SMALL_BTN($button_add, "button", "addStudent(1)");
			$select_classnum.="&nbsp$btn";
		}
		
		$YearTermID = ($_POST['YearTermID'])? $_POST['YearTermID'] : '';
		$htmlAry['termSelection'] = $lreportcard->Get_Term_Selection('YearTermID', $lreportcard->schoolYearID, $YearTermID, '', $NoFirst=1, $NoPastTerm=0, $withWholeYear=1,$isMultiple=0, $isAll=1, $eReportCard['WholeYearReport']);
		
		
		# tag information
		$TAGS_OBJ[] = array($eReportCard['Settings_ExcludeStudents']);
		# page navigation (leave the array empty if no need)
		$PAGE_NAVIGATION[] = array($button_new);

		$linterface->LAYOUT_START();
?>
<script type="text/JavaScript" language="JavaScript">
function changeClass(){
	submitForm('');
}

function submitForm(newurl){
	obj = document.form1;
	studentObjs = document.getElementsByName('studentID[]');
	studentObj = studentObjs[0];
	if(obj==null || studentObj==null) return;
	for(i=0;i<studentObj.options.length;i++){
		studentObj.options[i].selected = true;
	}
	obj.action=newurl;
	obj.submit();
}

function addStudent(stype){
	obj = document.form1.submitType;
	if(obj==null) return;
	obj.value = stype;
	submitForm('');	
}

function checkForm(){
	objStudent = document.getElementsByName('studentID[]')[0];
	if(objStudent==null) return;
	if(objStudent.options.length<=0){
		alert('<?=$i_Discipline_System_alert_PleaseSelectStudent?>');
		return false;
	}
	submitForm('new_update.php');
}
</script>
<link type="text/css" rel="stylesheet" href="<?=$PATH_WRT_ROOT."templates/2007a/css/"?>ereportcard.css">
<br />
<form name="form1" action="new_update.php" method="POST" onsubmit="return checkForm();">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
		<td align="right"> <?=$linterface->GET_SYS_MSG($Result, $SpMessage);?></td>
	</tr>
	<tr><td colspan="2">&nbsp;</td></tr>
<table>
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>
			<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr>
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?=$eReportCard['ReportType']?> <span class="tabletextrequire">*</span>
					</td>
					<td width="70%" class="tabletext">
						<?=$htmlAry['termSelection']?>
					</td>
				</tr>
				<tr>
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?=$i_general_students_selected?> <span class="tabletextrequire">*</span>
					</td>
					<td width="70%" class="tabletext">
						<table border="0" cellpadding="0" cellspacing="0">
							<tr>
								<td class="tabletext"><?=$select_student?></td>
								<td class="tabletext">&nbsp;</td>
								<td class="tabletext" valign="bottom">
									<?= $linterface->GET_SMALL_BTN($button_select, "button", "newWindow('choose/index.php?fieldname=studentID[]', 9)") ?><br />
									<?= $linterface->GET_SMALL_BTN($button_remove, "button", "checkOptionRemove(document.form1.elements['studentID[]']);") ?>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td width="30%" valign="top" nowrap="nowrap" class="tabletext">&nbsp;</td>
					<td class="tablerow2" width="70%" class="tabletext">
						<span class="tabletextremark">(<?=$i_general_alternative?>)</span>
						<table cellpadding="1" cellspacing="0" border="0">
							<tr><td class="tabletext" nowrap valign="bottom"><?=$i_UserLogin; ?></td></tr>
							<tr><td class="tabletext" nowrap valign="bottom">
								<input type="text" class="textboxnum" name="user_login" maxlength="100">&nbsp;
								<?= $linterface->GET_SMALL_BTN($button_add, "button", "addStudent(2)") ?><br />
							</td></tr>
							<!--<tr><td class="tabletext" nowrap valign="bottom"><?=$i_ClassNameNumber; ?></td></tr>
							<tr><td class="tabletext" nowrap valign="bottom">
								<?=$select_class?><?=$select_classnum?>
							</td></tr>-->
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr>
					<td class="dotline">
						<img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1">
					</td>
				</tr>
				<tr>
					<td align="center">
						<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "")?>&nbsp;
						<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location = 'index.php'")?>&nbsp;
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<input type="hidden" name="submitType" value="">
</form>
<?
	print $linterface->FOCUS_ON_LOAD("form1.code");
  	$linterface->LAYOUT_STOP();
  	intranet_closedb();
  } else {
?>
You have no priviledge to access this page.
<?
	}
} else {
?>
You have no priviledge to access this page.
<?
}
?>
