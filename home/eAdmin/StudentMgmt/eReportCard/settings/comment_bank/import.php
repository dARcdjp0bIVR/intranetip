<?php
// Using:

/********************** Change Log ***********************/
#
#	Date:	2019-03-01 (Bill)	[2018-1130-1440-05164]
#			Add CSV sample with Related grade settings  ($eRCTemplateSetting['Management']['ClassTeacherComment']['RelatedGradeSettings'])
#           (for Class Teacher's Comment only)
#
/******************* End Of Change Log *******************/

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if ($plugin['ReportCard2008'])
{
	include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
	if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") {
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
		$lreportcard = new libreportcardcustom();
	} else {
		$lreportcard = new libreportcard();
	}
	
	# Get HTTP variable
	$IsAdditional = (!isset($_GET['IsAdditional']) || $_GET['IsAdditional']=='')? 0 : $_GET['IsAdditional'];
	
	# Module
	$CurrentPage = "Settings_CommentBank";
	$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
	
	if ($lreportcard->hasAccessRight())
	{
		$linterface = new interface_html();
		
		$IsSubjectComment = (isset($CommentType) && $CommentType == "subject");
		
		// Subject Selection
		$SubjectArray = $lreportcard->GET_ALL_SUBJECTS(1);
		$SubjectOption = array();
		$SubjectSelect = "";
		if (sizeof($SubjectArray) > 0) {
			$tempCount = 0;
			foreach ($SubjectArray as $tempSubjectID => $tempSubjectDetail) {
				$SubjectOption[$tempCount][0] = $tempSubjectID;
				$SubjectOption[$tempCount][1] = $tempSubjectDetail[0];
				$tempCount++;
			}
			$SelectedSubject = (isset($SubjectID) && $SubjectID!="") ? $SubjectID : $SubjectOption[0][0];
			$SubjectSelect = $linterface->GET_SELECTION_BOX($SubjectOption, 'name="SubjectID" id="SubjectID" '.($IsSubjectComment?"":"disabled").' onchange=""', '', $SelectedSubject);
		}
		
		if ($IsSubjectComment) {
			$SampleCSV = "sample_subject_teacher_comment.csv";
		} else {
		    $SampleCSV = "sample_class_teacher_comment.csv";
		    
		    // [2018-1130-1440-05164]
		    if($eRCTemplateSetting['Management']['ClassTeacherComment']['RelatedGradeSettings']) {
		        $SampleCSV = "sample_class_teacher_comment_w_grade.csv";
		    }
		}
		
		$SubjectRowStyle = "display:none;";
		if ($IsSubjectComment) {
			$SubjectRowStyle = "";
		}
		############## Interface Start ##############

		# Tag
		#$TAGS_OBJ[] = array($eReportCard['Settings_CommentBank']);
		$TAGS_OBJ = array();
        // [2020-0708-0950-12308]
        if ($eRCTemplateSetting['ClassTeacherComment_AdditionalComment_as_BilingualTeacher']) {
            $TAGS_OBJ[] = array($eReportCard['ManagementArr']['ClassTeacherCommentArr']['CommentCustDisplay'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/settings/comment_bank/index.php?CommentType=class", (!$IsSubjectComment && $IsAdditional==0)?1:0);
        } else {
		    $TAGS_OBJ[] = array($eReportCard['Management_ClassTeacherComment'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/settings/comment_bank/index.php?CommentType=class", (!$IsSubjectComment && $IsAdditional==0)?1:0);
        }
		if ($eRCTemplateSetting['ClassTeacherComment_AdditionalComment']) {
			$TAGS_OBJ[] = array($eReportCard['ManagementArr']['ClassTeacherCommentArr']['AdditionalComment'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/settings/comment_bank/index.php?CommentType=class&IsAdditional=1", (!$IsSubjectTeacherComment && $IsAdditional==1)?1:0);
		}
		$TAGS_OBJ[] = array($eReportCard['Template']['SubjectTeacherComment'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/settings/comment_bank/index.php?CommentType=subject", ($IsSubjectComment)?1:0);
		
		# Page Navigation (leave the array empty if no need)
		$PAGE_NAVIGATION[] = array($button_import);

		$linterface->LAYOUT_START();
?>

<script type="text/JavaScript" language="JavaScript">
function jsChangeCommetType(commentType) {
	if (commentType == "0") {
		document.getElementById("SampleCsvLink").href = "<?=GET_CSV("sample_class_teacher_comment.csv")?>";
		document.getElementById("SubjectID").disabled = true;
		document.getElementById("SubjectRow").style.display = "none";
	} else {
		document.getElementById("SampleCsvLink").href = "<?=GET_CSV("sample_subject_teacher_comment.csv")?>";
		document.getElementById("SubjectID").disabled = false;
		document.getElementById("SubjectRow").style.display = "";
	}
}

function resetForm() {
	document.form1.reset();
	changePicClass()
	return true;
}

function trim(str) {
	return str.replace(/^\s+|\s+$/g,"");
}

function checkForm() {
	obj = document.form1;
	if (trim(obj.elements["userfile"].value) == "") {
		 alert('<?=$eReportCard['AlertSelectFile']?>');
		 obj.elements["userfile"].focus();
		 return false;
	}
	obj.submit();
}
</script>

<link type="text/css" rel="stylesheet" href="<?=$PATH_WRT_ROOT."templates/<?=$LAYOUT_SKIN?>/css/"?>ereportcard.css">
<br />
<form name="form1" action="import_update.php" method="POST" onsubmit="return checkForm();" enctype="multipart/form-data">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
		<td align="right"> <?=$linterface->GET_SYS_MSG($Result);?></td>
	</tr>
	<tr><td>&nbsp;</td><td>&nbsp;</td></tr>
<table>

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>
			<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
				<?php
				/*
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?= $eReportCard['Type'] ?>
					</td>
					<td width="75%" class='tabletext'>
						<input type="radio" name="type" id="type1" value="0" onclick="jsChangeCommetType(this.value)" <?=$IsSubjectComment?"":"checked"?> />
						<label for="type1"><?=$eReportCard['ClassTeacher']?></label><br />
						<input type="radio" name="type" id="type2" value="1" onclick="jsChangeCommetType(this.value)" <?=$IsSubjectComment?"checked":"="?> />
						<label for="type2"><?=$eReportCard['SubjectTeacher']?></label><br />
					</td>
				</tr>
				*/
				?>
				<tr id="SubjectRow" style="<?=$SubjectRowStyle?>">
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?= $eReportCard['Subject'] ?>
					</td>
					<td width="75%" class='tabletext'>
						<?=$SubjectSelect?>
					</td>
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?= $i_select_file ?>
					</td>
					<td width="75%" class='tabletext'>
						<input class="file" type='file' name='userfile'>
					</td>
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" colspan='2' class="tabletext">
						<a id="SampleCsvLink" class="tablelink" href="<?=GET_CSV($SampleCSV)?>" target="_blank">
							<img src='<?=$PATH_WRT_ROOT?>/images/<?=$LAYOUT_SKIN?>/icon_files/xls.gif' border='0' align='absmiddle' hspace='3'> 
							<?=$i_general_clickheredownloadsample?>
						</a>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr>
					<td class="dotline">
						<img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1">
					</td>
				</tr>
				<tr>
					<td align="center">
						<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "")?>&nbsp;
						<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location = 'index.php?CommentType=$CommentType'")?>&nbsp;
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>

<input type="hidden" name="type" id="type" value="<?=$IsSubjectComment?"1":"0"?>" />
<input type="hidden" name="IsAdditional" id="IsAdditional" value="<?=$IsAdditional?>" />
</form>

<?
	print $linterface->FOCUS_ON_LOAD("form1.userfile");
  	$linterface->LAYOUT_STOP();
  } else {
?>
You have no priviledge to access this page.
<?
	}
} else {
?>
You have no priviledge to access this page.
<?
}
?>