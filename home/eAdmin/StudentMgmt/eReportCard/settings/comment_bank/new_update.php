<?php
// Using:

/********************** Change Log ***********************/
#
#	Date:	2019-03-01 (Bill)	[2018-1130-1440-05164]
#			Support Related grade update  ($eRCTemplateSetting['Management']['ClassTeacherComment']['RelatedGradeSettings'])
#           (for Class Teacher's Comment only)
#
/******************* End Of Change Log *******************/

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

$PageRight = "ADMIN";

if (!$plugin['ReportCard2008']) {
	intranet_closedb();
	header("Location:/index.php");
}

if (!isset($_POST)) {
	header("Location:new.php");
}

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
$lreportcard = new libreportcard();

# Get HTTP variable
$IsAdditional = (!isset($_POST['IsAdditional']) || $_POST['IsAdditional']=='')? 0 : $_POST['IsAdditional'];

if (!$lreportcard->hasAccessRight()) {
	intranet_closedb();
	header("Location:index.php");
}

if ($code == "" || $type == "" || ($type == "1" && $SubjectID == "") || ($comment == "" && $commentEng == "")) {
	header("Location:new.php?Result=update_failed");
}

// Get all Comment Code to prevent duplicate
$table = $lreportcard->DBName.".RC_COMMENT_BANK";
$sql = "SELECT UPPER(CommentCode) FROM $table";
if ($type == 1) {
	$sql .= " WHERE SubjectID IS NOT NULL AND SubjectID != 0";
} else if ($type == 0) {
	$sql .= " WHERE (SubjectID IS NULL OR SubjectID = 0) And IsAdditional = '".$IsAdditional."' ";
}
$commentCodeList = $lreportcard->returnVector($sql);

if (in_array(strtoupper($code), $commentCodeList)) {
	header("Location:edit.php?Result=update_failed");
}

$category = "";
if (isset($CatSelect)) {
	$category = $CatSelect;
} else if (isset($CatText)) {
	$category = $CatText;
}

// [2018-1130-1440-05164] Related grade
$grade = $GradeSelect == ''? "NULL" : "'$GradeSelect'";

$table = $lreportcard->DBName.".RC_COMMENT_BANK";
$field = "CommentCode, CommentCategory, SubjectID, CommentEng, Comment, IsAdditional, RelatedGrade, DateInput, DateModified";
if ($type == "0") {
	$value = "'$code', NULL, NULL, '$commentEng', '$comment', '$IsAdditional', $grade, NOW(), NOW()";
} else if ($type == "1") {
	$value = "'$code', '$category', '$SubjectID', '$commentEng', '$comment', '$IsAdditional', NULL, NOW(), NOW()";
}

$sql = "INSERT INTO $table ($field) VALUES ($value)";
$success = $lreportcard->db_db_query($sql);

intranet_closedb();

$Result = ($success) ? "add" : "update_failed";
$CommentType = ($type == "0") ? "class":"subject";
header("Location:index.php?CommentType=$CommentType&Result=$Result&IsAdditional=$IsAdditional");

?>