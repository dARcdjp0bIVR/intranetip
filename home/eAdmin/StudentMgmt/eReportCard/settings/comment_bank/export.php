<?php
// Using:

/********************** Change Log ***********************/
#
#	Date:	2019-03-01 (Bill)	[2018-1130-1440-05164]
#			Export Related grade settings   ($eRCTemplateSetting['Management']['ClassTeacherComment']['RelatedGradeSettings'])
#           (for Class Teacher's Comment only)
#
/******************* End Of Change Log *******************/

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");

intranet_auth();
intranet_opendb();

if ($plugin['ReportCard2008'])
{
	if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") {
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
		$lreportcard = new libreportcardcustom();
	} else {
		$lreportcard = new libreportcard();
	}
	
	# Get HTTP variable
	$IsAdditional = (!isset($_GET['IsAdditional']) || $_GET['IsAdditional']=='')? 0 : $_GET['IsAdditional'];
	
	if ($lreportcard->hasAccessRight())
	{
		$conds = "";
		$table = $lreportcard->DBName.".RC_COMMENT_BANK";
		
		if (!isset($CommentType) || $CommentType == "class")
		{
			$filename = "class_teacher_comment.csv";
			
			// [2018-1130-1440-05164]
			$extra_fields = '';
			if($eRCTemplateSetting['Management']['ClassTeacherComment']['RelatedGradeSettings']) {
                $extra_fields = " , RelatedGrade ";
			}
			
			$conds .= " ";
			$sql = "SELECT
                        CommentCode, Comment, CommentEng
                        $extra_fields
					FROM
				        $table
					WHERE
						(Comment IS NOT NULL OR CommentEng IS NOT NULL) AND
						(SubjectID IS NULL OR SubjectID = '') AND
						IsAdditional = '".$IsAdditional."'
					ORDER BY
						CommentCode ";
	        $exportColumn = array("Code", "Comment Content", "Comment Content English");
	        
	        // [2018-1130-1440-05164]
	        if($eRCTemplateSetting['Management']['ClassTeacherComment']['RelatedGradeSettings']) {
	            $exportColumn[] = "Related grade";
	        }
		}
		else
		{
			$filename = "subject_teacher_comment";
			$conds .= " AND (SubjectID IS NOT NULL OR SubjectID != '')";
			if (isset($SubjectID) && trim($SubjectID) != '') {
				$conds .= " AND SubjectID = '$SubjectID'";
				$SubjectName = $lreportcard->GET_SUBJECT_NAME_LANG($SubjectID, "EN");
				$filename .= "_".$SubjectName;
			}
			if (isset($Category) && trim($Category) != '') {
				$conds .= " AND CommentCategory = '$Category'";
			}
			$filename .= ".csv";
			$sql = "SELECT CommentCode, CommentCategory, Comment, CommentEng
					FROM $table
					WHERE
						(Comment IS NOT NULL OR CommentEng IS NOT NULL)
						$conds
					ORDER BY
						CommentCode
					";
			$exportColumn = array("Code", "Category", "Comment Content", "Comment Content English");
		}
		$result = $lreportcard->returnArray($sql,4);
        
		$lexport = new libexporttext();
		$export_content = $lexport->GET_EXPORT_TXT($result, $exportColumn, "", "\r\n", "", 0, "11");
		
		intranet_closedb();
		$lexport->EXPORT_FILE($filename, $export_content);
	}
	else
	{
		echo "You have no priviledge to access this page.";
	}
}
else
{
	echo "You have no priviledge to access this page.";
}
?>