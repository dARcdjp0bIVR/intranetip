<?
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");

intranet_auth();
intranet_opendb();

$PageRight = "ADMIN";

if (!isset($_POST))
	header("Location:index.php");

if (!$plugin['ReportCard2008']) {
	intranet_closedb();
	header("Location:/index.php");
}

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
{
	include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
	$lreportcard = new libreportcardcustom();
} else {
	$lreportcard = new libreportcard();
}

if (!$lreportcard->hasAccessRight()) {
	intranet_closedb();
	header("Location:/index.php");
}

# Get http variable
$IsAdditional = (!isset($_POST['IsAdditional']) || $_POST['IsAdditional']=='')? 0 : $_POST['IsAdditional'];

if (sizeof($CommentID) > 0) {
	$deleteComments = implode(",", $CommentID);
	$table = $lreportcard->DBName.".RC_COMMENT_BANK";
	$sql = "DELETE FROM $table WHERE CommentID IN ($deleteComments)";
	$success = $lreportcard->db_db_query($sql);
} else {
	$success = false;
}
intranet_closedb();


$Result = ($success) ? "delete" : "delete_failed";
header("Location:index.php?Result=$Result&CommentType=$CommentType&IsAdditional=$IsAdditional");

?>
