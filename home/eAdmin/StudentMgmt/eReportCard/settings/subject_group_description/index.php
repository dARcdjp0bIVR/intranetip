<?php
// Using:

/********************** Change Log ***********************/
#
#	Date:	2020-10-30 (Bill)	[2020-0708-0950-12308]
#			create file
#           - copy logic from /marksheet_revision_subject_group.php
#
/******************* End Of Change Log *******************/

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
//include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libteaching.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");

intranet_auth();
intranet_opendb();

if ($plugin['ReportCard2008'])
{
    /* Temp Library */
    include_once($PATH_WRT_ROOT."includes/libreportcard2008j.php");
    $lreportcard = new libreportcard2008j();
    
    $CurrentPage = "Settings_SubjectGroupDescription";
    
    if ($lreportcard->hasAccessRight())
    {
        ### Handle SQL Injection + XSS [START]
        $ClassLevelID = IntegerSafe($ClassLevelID);
        $ReportID = IntegerSafe($ReportID);
        ### Handle SQL Injection + XSS [END]
        
        $linterface = new interface_html();
        $PAGE_TITLE = $eReportCard['Settings_SubjectGroupDescription'];
        
        // [2018-1120-1055-50206]
        $TeacherClassIDArr = array();
        if($ck_ReportCard_UserType=="TEACHER")
        {
            $ReportInfoArr = $lreportcard->returnReportTemplateBasicInfo($ReportID);
            $FormArrCT = $lreportcard->returnSunjectTeacherForm($UserID);

            # added $special_feature['eRC_class_teacher_as_subject_teacher'] to check if the class teacher can view all subjects of the class or just the teaching subjects of the class
            # check is class teacher or not
            if (!$special_feature['eRC_class_teacher_as_subject_teacher'])
            {
                $lteaching = new libteaching();
                $TeacherClassAry = $lteaching->returnTeacherClassWithLevel($UserID, $lreportcard->schoolYearID);
                $TeacherClass = $TeacherClassAry[0]['ClassName'];
                $TeacherClassLevelID = $TeacherClassAry[0]['ClassLevelID'];
                $TeacherClassID = $TeacherClassAry[0]['ClassID'];
                
                // [2018-1029-1017-31066]
                //$TeacherClassIDArr = array();
                if(!empty($TeacherClassAry)) {
                    $TeacherClassIDArr = Get_Array_By_Key($TeacherClassAry, 'ClassID');
                }
                $TeacherClassLevelIDArr = array();
                if(!empty($TeacherClassAry)) {
                    $TeacherClassLevelIDArr = Get_Array_By_Key($TeacherClassAry, 'ClassLevelID');
                }
            }
            
            if(!empty($TeacherClass))
            {
                for($i=0;$i<sizeof($TeacherClassAry);$i++)
                {
                    $thisClassLevelID = $TeacherClassAry[$i]['ClassLevelID'];
                    $searchResult = multiarray_search($FormArrCT , "ClassLevelID", $thisClassLevelID);
                    if($searchResult == "-1")
                    {
                        $thisAry = array(
                            "0"=>$TeacherClassAry[$i]['ClassLevelID'],
                            "ClassLevelID"=>$TeacherClassAry[$i]['ClassLevelID'],
                            "1"=>$TeacherClassAry[$i]['LevelName'],
                            "LevelName"=>$TeacherClassAry[$i]['LevelName']);
                        $FormArrCT = array_merge($FormArrCT, array($thisAry));
                    }
                }
                
                # sort $FormArrCT
                foreach ($FormArrCT as $key => $row)
                {
                    $field1[$key] = $row['ClassLevelID'];
                    $field2[$key] = $row['LevelName'];
                }
                array_multisort($field1, SORT_ASC, $field2, SORT_ASC, $FormArrCT);
            }
            
            $ClassLevelID = ($ClassLevelID == "") ? $FormArrCT[0][0] : $ClassLevelID;
            $ClassArrCT = $lreportcard->returnSubjectTeacherClass($UserID, $ClassLevelID);
            $FormSubjectArrCT = $lreportcard->returnSubjectwOrder($ClassLevelID, 1, $UserID);
            
            if(!empty($TeacherClass))
            {
                $searchResult = multiarray_search($TeacherClassAry , "ClassLevelID", $ClassLevelID);
                if($searchResult != "-1")
                {
                    $searchResult2 = multiarray_search($ClassArrCT , "ClassName", $TeacherClassAry[$searchResult]['ClassName']);
                    if($searchResult2 == "-1")
                    {
                        $thisAry = array(
                            "0"=>$TeacherClassAry[$searchResult]['ClassID'],
                            "ClassID"=>$TeacherClassAry[$searchResult]['ClassID'],
                            "1"=>$TeacherClassAry[$searchResult]['ClassName'],
                            "ClassName"=>$TeacherClassAry[$searchResult]['ClassName'],
                            "2"=>$TeacherClassAry[$searchResult]['ClassLevelID'],
                            "ClassLevelID"=>$TeacherClassAry[$searchResult]['ClassLevelID']);
                        $ClassArrCT = array_merge($ClassArrCT, array($thisAry));
                    }
                    
                    $FormSubjectArrCT = $lreportcard->returnSubjectwOrder($ClassLevelID, 1);
                }
            }
        }
        
        ############################################################################################################
        
        # 3 GET Parameters : ClassLevelID, SemesterType_[ClassLevelID]
        # Get ClassLevelID (Form) of reportcard template
        //$FormArr = $lreportcard->GET_ALL_FORMS(1);
        $FormArr = ($ck_ReportCard_UserType=="ADMIN" || $ck_ReportCard_UserType=="VIEW_GROUP")? $lreportcard->GET_ALL_FORMS(1) : $FormArrCT;
        $ClassLevelID = ($ClassLevelID == "") ? $FormArr[0][0] : $ClassLevelID;

        # Filters - By Form (ClassLevelID)
        $FormSelection = $linterface->GET_SELECTION_BOX($FormArr, "name='ClassLevelID' id='ClassLevelID' class='tabletexttoptext' onchange='jCHANGE_CURRENT_SUBJECT_GROUP(1);'", "", $ClassLevelID);
        
        # Filters - By Type (Term1, Term2, Whole Year, etc)
        // Get Semester Type from the reportcard template corresponding to ClassLevelID
        $ReportTypeSelection = '';
        $ReportTypeArr = array();
        $ReportTypeOption = array();
        //2013-1119-1033-11156
        $orderBySubmissionTime = ($eRCTemplateSetting['Marksheet']['ReportSelectionOrderingSortBySubmissionDate'])? true : false;
        $ReportTypeArr = $lreportcard->GET_REPORT_TYPES($ClassLevelID, '', '', 0, $orderBySubmissionTime);
        
        # if not selected specific report card, preset the filter to the current submission period report template
        if ($ReportID == '')
        {
            $ReportID = $lreportcard->getReportFilteringDefaultReportId($ClassLevelID);
        }
        
        $ReportType = '';
        if(count($ReportTypeArr) > 0){
            for($j=0 ; $j<count($ReportTypeArr) ; $j++){
                if($ReportTypeArr[$j]['ReportID'] == $ReportID){
                    $isFormReportType = 1;
                    $ReportType = $ReportTypeArr[$j]['Semester'];
                }
                
                $ReportTypeOption[$j][0] = $ReportTypeArr[$j]['ReportID'];
                $ReportTypeOption[$j][1] = $ReportTypeArr[$j]['SemesterTitle'];
            }
            $ReportTypeSelection .= $linterface->GET_SELECTION_BOX($ReportTypeOption, 'name="ReportType" id="ReportType" class="tabletexttoptext" onchange="jCHANGE_CURRENT_SUBJECT_GROUP(0)"', '', $ReportID);
            
            $ReportType = ($ReportType != "") ? $ReportType : $ReportTypeArr[0]['Semester'];
        }
        $ReportID = ($isFormReportType) ? $ReportID : $ReportTypeOption[0][0];
        $ReportSetting = $lreportcard->returnReportTemplateBasicInfo($ReportID);
        $SemID = $ReportSetting['Semester'];
        $ClassLevelID = $ReportSetting['ClassLevelID'];
        
        # Filters - By Form Subjects
        // A Flag, called $isFormSubject, is used to identify whether currect Form has that subject or not
        $FormSubjectArr = array();
        $SubjectListArr = array();
        $count = 1;
        $isFormSubject = 0;		// Default: Assume the selected subject does not belongs to that Form(ClassLevelID)
        // Get Subjects By Form (ClassLevelID)
        $FormSubjectArr = ($ck_ReportCard_UserType=="ADMIN" || $ck_ReportCard_UserType=="VIEW_GROUP")? $lreportcard->returnSubjectwOrder($ClassLevelID, 1, '', '', 'Desc', 0, $ReportID) : $FormSubjectArrCT;
        
        // Hide Report Info if empty ReportID and only display reports that within submission period
        $hideAllReportInfo = false;
        if($ReportID==""){
            $FormSubjectArr = array();
            $hideAllReportInfo = true;
        }
        
        if(!empty($FormSubjectArr)){
            foreach($FormSubjectArr as $FormSubjectID => $Data){
                if(is_array($Data)){
                    foreach($Data as $FormCmpSubjectID => $SubjectName){
                        $FormSubjectID = ($FormCmpSubjectID==0) ? $FormSubjectID : $FormCmpSubjectID;
                        if($FormSubjectID == $SubjectID)
                            $isFormSubject = 1;
                            
                            // Prepare Subject Selection Box
                            if($FormCmpSubjectID==0){
                                $SubjectFormGradingArr = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $FormSubjectID,0,0,$ReportID);
                                $SubjectListArr[($count-1)]['SubjectID'] = $FormSubjectID;
                                $SubjectListArr[($count-1)]['SubjectName'] = $SubjectName;
                                $SubjectListArr[($count-1)]['SchemeID'] = $SubjectFormGradingArr['SchemeID'];
                                $count++;
                            }
                    }
                }
            }
        }
        
        /*
         * Use for Selection Box
         * $SubjectListArr[][0] : SubjectID
         * $SubjectListArr[][1] : SubjectName
         */
        $isSelectAllSubject = $SubjectID=="";
        $SubjectID = ($isFormSubject) ? $SubjectID : '';
        $SubjectID = $SubjectOption[0][0];

        # Subject List
        $display = '';
        $cnt = 0;
        $numOfSubject = count($SubjectListArr);
        $numOfDisplayedSubjectGroup = 0;
        
        $SubjectIDArr = Get_Array_By_Key($SubjectListArr, 'SubjectID');
        for($i=0 ; $i<$numOfSubject ; $i++)
        {
            $thisSubjectName = $SubjectListArr[$i]['SubjectName'];
            $thisSubjectID = $SubjectListArr[$i]['SubjectID'];
            
            # Get all Subject Groups of the Subject
            $TeacherID = '';
            
            // [2018-1029-1017-31066] - not Class Teacher / not teaching classes in this form
            if ($ck_ReportCard_UserType=="TEACHER" && (empty($TeacherClass) || empty($TeacherClassLevelIDArr) || !in_array($ClassLevelID, (array)$TeacherClassLevelIDArr)))
            {
                # Subject Teacher => View teaching subject(s) only
                $TeacherID = $_SESSION['UserID'];
            }
            
            $obj_Subject = new Subject($thisSubjectID);
            $AllSubjectGroupArr = $obj_Subject->Get_Subject_Group_List($SemID, $ClassLevelID, '', $TeacherID);
            $SubjectGroupIDArr = Get_Array_By_Key($AllSubjectGroupArr, 'SubjectGroupID');
            $numOfSubjectGroup = count($AllSubjectGroupArr);

            $SubjectGroupDescriptionArr = $lreportcard->getSubjectGroupDescription($ReportID, $SubjectGroupIDArr);
            if(!empty($SubjectGroupDescriptionArr)) {
                $SubjectGroupDescriptionArr = BuildMultiKeyAssoc($SubjectGroupDescriptionArr, 'SubjectGroupID');
            }

            for ($j=0; $j<$numOfSubjectGroup; $j++)
            {
                $thisSubjectGroupID = $AllSubjectGroupArr[$j]['SubjectGroupID'];
                $thisSubjectComponentID = $AllSubjectGroupArr[$j]['SubjectComponentID'];
                $thisSubjectGroupTitleEN = $AllSubjectGroupArr[$j]['ClassTitleEN'];
                $thisSubjectGroupTitleB5 = $AllSubjectGroupArr[$j]['ClassTitleB5'];
                $thisSubjectGroupName = Get_Lang_Selection($thisSubjectGroupTitleB5, $thisSubjectGroupTitleEN);
                $thisSubjectGroupDescription = "";
                if (isset($SubjectGroupDescriptionArr[$thisSubjectGroupID])) {
                    $thisSubjectGroupDescription = ($SubjectGroupDescriptionArr[$thisSubjectGroupID]['Description']);
                }

                # Check if the teacher is teaching this class / have class student enrolled in this class
                if ($ck_ReportCard_UserType!="ADMIN" && $ck_ReportCard_UserType!="VIEW_GROUP")
                {
                    $isTeachingThisSubjectGroup = $lreportcard->Is_Teaching_Subject_Group($_SESSION['UserID'], $thisSubjectGroupID);
                    
                    // [2018-1029-1017-31066] Check all teaching classes
                    //$hasClassStudentInThisSubjectGroup = $lreportcard->Has_Class_Student_Enrolled_In_Subject_Group($TeacherClassID, $thisSubjectGroupID);
                    $hasClassStudentInThisSubjectGroup = false;
                    foreach((array)$TeacherClassIDArr as $thisTeacherClassID) {
                        $hasClassStudentInThisSubjectGroup = $hasClassStudentInThisSubjectGroup || ($lreportcard->Has_Class_Student_Enrolled_In_Subject_Group($thisTeacherClassID, $thisSubjectGroupID));
                        if($hasClassStudentInThisSubjectGroup) {
                            break;
                        }
                    }
                    
                    if (!$isTeachingThisSubjectGroup && !$hasClassStudentInThisSubjectGroup) {
                        continue;
                    }
                }

                $numOfDisplayedSubjectGroup++;
                $cnt++;

                $row_css = ($cnt % 3 == 1) ? "attendanceouting" : "attendancepresent";

                # Content
                $display .= '<tr class="tablegreenrow1" height="38">'."\n";
                $display .= '<td class="'.$row_css.' tabletext">'.$thisSubjectName.'</td>'."\n";
                    $display .= '<td class="'.$row_css.' tabletext">'.$thisSubjectGroupName.'</td>'."\n";
                    $display .= '<td class="'.$row_css.' tabletext">
                                    <textarea class="textboxtext" name="Description['.$thisSubjectGroupID.']" id="Description_'.$thisSubjectGroupID.'" cols="70" rows="5" wrap="virtual" onFocus="this.rows=7;">'.$thisSubjectGroupDescription.'</textarea>
                                 </td>'."\n";
                $display .= '</tr>'."\n";
            }
        }

        if ($numOfSubject == 0 || $numOfDisplayedSubjectGroup == 0)
        {
            $td_colspan = "3";
            $display .= '<tr class="tablegreenrow1" height="40">
                            <td class="attendancepresent tabletext" colspan="'.$td_colspan.'" align="center">'.$i_no_record_exists_msg.'</td>
                        </tr>';
        }

        $table = '';
        $table .= '<table width="100%" border="0" cellspacing="0" cellpadding="4">'."\r\n";
        $table .= '<tr class="tablegreentop">'."\r\n";
            $table .= '<td class="tabletopnolink">'.$eReportCard['Subject'].'</td>'."\r\n";
            $table .= '<td class="tabletopnolink">'.$Lang['SysMgr']['SubjectClassMapping']['SubjectGroup'].'</td>'."\r\n";
            $table .= '<td class="tabletopnolink">'.$eReportCard['Settings_SubjectGroupDescription'].'</td>'."\r\n";
        $table .= '</tr>'."\r\n";
        $table .= $display."\r\n";
        $table .= '</table>'."\r\n";

        $total = $eReportCard['TotalRecords']." ";
        $total .= ($isFormSubject || $SubjectID == "") ? $numOfDisplayedSubjectGroup : 0;
            
        # Button
        $display_button = '';
        if($cnt > 0)
        {
            $display_button .= '<tr>
                                    <td>
                                        <br>
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td height="1" class="dotline"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/10x10.gif" width="10" height="1"></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table width="100%" border="0" cellspacing="0" cellpadding="2">
                                                <tr>
                                                    <td align="center" valign="bottom">
                                                        <input name="Save" type="button" class="formbutton" onMouseOver="this.className=\'formbuttonon\'" onMouseOut="this.className=\'formbutton\'" value="'.$button_save.'" onclick="jSUBMIT_FORM()">
                                                    </td>
                                                </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        </table>
                                    </td>
                                </tr>';
        }

        ############################################################################################################

        # tag information
        $MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
        $TAGS_OBJ[] = array($PAGE_TITLE, "", 0);

        $linterface->LAYOUT_START();
?>

<script language="javascript">
/* General JS function(s) */
function jGET_SELECTBOX_SELECTED_VALUE(objName){
    var obj = document.getElementById(objName);
    if(obj==null) {
        return "";
    }

    var index = obj.selectedIndex;
    var val = obj[index].value;
    return val;
}

function jCHANGE_CURRENT_SUBJECT_GROUP(ResetReportID){
	var form_id = jGET_SELECTBOX_SELECTED_VALUE("ClassLevelID");
	var report_id = jGET_SELECTBOX_SELECTED_VALUE("ReportType");
	if (ResetReportID) {
		report_id = '';
    }
	
	location.href = "?ClassLevelID="+form_id+"&ReportID="+report_id;
}

// Form
function jSUBMIT_FORM()
{
	var obj = document.FormMain;
	obj.action = 'update.php';
	obj.submit();
}

/*
function jsIMPORT()
{
	var SubjectID = $("#SubjectID").val();
	window.location.href = "marksheet_import.php?ReportID=<?=$ReportID?>&ClassLevelID=<?=$ClassLevelID?>&TagsType=<?=$TagsType?>&SubjectID="+SubjectID;
}

function js_Export()
{
	var obj = document.FormMain;
	obj.action = './marksheet_feedback/export_form_feedback.php';
	obj.submit();
	obj.action = './marksheet_complete_confirm_subject_group.php';
}
*/
</script>

<br/>
<form name="FormMain" method="POST" action="marksheet_complete_confirm_subject_group.php">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
    <td>
        <table border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td><?=$FormSelection?></td>
            <td>&nbsp;</td>
            <td><?=$ReportTypeSelection?></td>
            <td>&nbsp;</td>
            <td align="right" width="100%">
                <?=$linterface->GET_SYS_MSG($msg);?>
            </td>
        </tr>
        </table>
    </td>
</tr>
<tr>
    <td>
        <br>
    </td>
</tr>
<tr>
    <td>
        <?=$table?>
    </td>
</tr>
<tr class="tablegreenbottom">
    <td>
        <table width="100%" border="0" cellspacing="0" cellpadding="3">
        <tr>
            <td class="tabletext"><?=$total?></td>
        </tr>
        </table>
    </td>
</tr>
<?=$display_button?>
</table>

<br />
<br />

<input type="hidden" name="ClassLevelID" id="ClassLevelID" value="<?=$ClassLevelID?>"/>
<input type="hidden" name="ReportID" id="ReportID" value="<?=$ReportID?>"/>
</form>

<?
		$linterface->LAYOUT_STOP();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>