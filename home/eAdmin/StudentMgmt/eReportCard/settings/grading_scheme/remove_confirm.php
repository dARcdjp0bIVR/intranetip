<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
intranet_auth();
intranet_opendb();
$lreportcard = new libreportcard();
$linterface = new interface_html();
$lreportcard->hasAccessRight();

$CurrentPage = "Settings_GradingScheme";
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
# tag information
$TAGS_OBJ = array();
$TAGS_OBJ[] = array($eReportCard['Settings_GradingScheme']);
$PAGE_NAVIGATION[] = array($eReportCard['Settings_GradingScheme'],"index.php");
$PAGE_NAVIGATION[] = array($Lang['Btn']['Remove']);
############################################################################################################
$x = '';
$hiddenInput = '';
### Warning table
$WarningBox = '';
$warningMsg = implode('<br />', $Lang['eReportCard']['GradingSchemeArr']['removeSchemeWarning']);
$WarningBox .= $linterface->GET_WARNING_TABLE($err_msg='', $DeleteItemName='', $DeleteButtonName='', $warningMsg);
$WarningBox .= '<br />';	

$sql = "
	SELECT 
		DISTINCT sfg.ClassLevelID,sfg.SchemeID,sfg.ReportID,gs.SchemeTitle,rt.ReportTitle
	FROM 
		".$lreportcard->DBName.".RC_SUBJECT_FORM_GRADING sfg
	INNER JOIN
		".$lreportcard->DBName.".RC_GRADING_SCHEME gs ON sfg.SchemeID = gs.SchemeID
	INNER JOIN
		".$lreportcard->DBName.".RC_REPORT_TEMPLATE	rt ON sfg.ReportID = rt.ReportID
	INNER JOIN
		ASSESSMENT_SUBJECT s ON sfg.SubjectID = s.RecordID
	WHERE 
		sfg.SchemeID IN (".(implode(',',(array)$SchemeIDs)).")
	AND
		sfg.ReportID > 0
	AND
		s.RecordStatus = 1
	ORDER BY sfg.SchemeID,sfg.ClassLevelID,sfg.ReportID	
";

$SchemeInfo = $lreportcard->returnArray($sql);
$SchemeIndexArr = $lreportcard->Get_Grand_Mark_Grading_Scheme_Index_Arr();
$SchemeNum = count($SchemeInfo);
if($SchemeNum>0){
	$SchemeSubjectInfo = array();
	for($i=0;$i<$SchemeNum;$i++){
		list($_classLevelId,$_schemeId,$_reportId,$_schemeTitle,$_reportTitle) = $SchemeInfo[$i];
		$_classLevelName = $lreportcard->returnClassLevel($_classLevelId);
		
		$_subjectInfo = $lreportcard->GET_FROM_SUBJECT_GRADING_SCHEME($_classLevelId,1,$_reportId);
		##Scheme Title
		$SchemeSubjectInfo[$_schemeId]['SchemeTitle'] = $_schemeTitle;
		##ClassLevel Name
		$SchemeSubjectInfo[$_schemeId][$_classLevelId]['ClassLevelName'] = $_classLevelName;	
		##Report Title
		$SchemeSubjectInfo[$_schemeId][$_classLevelId][$_reportId]['ReportTitle'] = str_replace(":_:", "<br>",$_reportTitle);
		##Subject
		foreach($_subjectInfo as $__subjectId=>$__subjectInfo){
			if($__subjectInfo['schemeID']==$_schemeId){
				$__subjectName = ($__subjectId<0)?$SchemeIndexArr[$__subjectId]:$__subjectInfo['subjectName'];
				$SchemeSubjectInfo[$_schemeId][$_classLevelId][$_reportId]['Subject'][] = array('subjectId'=>$__subjectId,'subjectName'=>$__subjectName,'is_cmpSubject'=>$__subjectInfo['is_cmpSubject']);
			}	
		}
	}
}	
##HTML Content
$selSchemeIDsNum = count((array)$SchemeIDs);
for($i=0;$i<$selSchemeIDsNum;$i++){
	$_schemeId = $SchemeIDs[$i];
	$hiddenInput .= $linterface->GET_HIDDEN_INPUT('', 'SchemeIDs[]', $_schemeId)."\r\n";
	$_schemeClassLevelInfo = $SchemeSubjectInfo[$_schemeId];
	if(is_array($_schemeClassLevelInfo)){
		$x .= '<table width="100%">'."\r\n";
			###SchemeTitle
			$x .= '<tr>'."\r\n";
				$x .= '<td>'."\r\n";
					$x .= $linterface->GET_NAVIGATION2($_schemeClassLevelInfo['SchemeTitle'])."\r\n";
				$x .= '</td>'."\r\n";
			$x .= '</tr>'."\r\n";		
			###SchemeDetails
			$x .= '<tr>'."\r\n";
				$x .= '<td>'."\r\n";
					foreach($_schemeClassLevelInfo as $__classLevelId => $__classLevelReportInfo){
						if($__classLevelId!='SchemeTitle'){
							$x .= '<table class="common_table_list_v30 view_table_list_v30">'."\r\n";
								###ClassLevelName
								$x .= '<tr>'."\r\n";
									$x .= '<th colspan="2" class="sub_row_top">'."\r\n";
										$x .= $__classLevelReportInfo['ClassLevelName']."\r\n";
									$x .= '</th>'."\r\n";
								$x .= '</tr>'."\r\n";
								###SubTitle
								$x .= '<tr>'."\r\n";
									$x .= '<th>'."\r\n";
										$x .= $eReportCard['ReportTitle']."\r\n";
									$x .= '</th>'."\r\n";
									$x .= '<th>'."\r\n";
										$x .= $eReportCard['Subject']."\r\n";
									$x .= '</th>'."\r\n";									
								$x .= '</tr>'."\r\n";									
								foreach($__classLevelReportInfo as $___reportId => $___reportSubjectInfo){
									if($___reportId!='ClassLevelName'){
										$___subjectInfo = $___reportSubjectInfo['Subject'];
										$subjectNum = count($___subjectInfo);
										$x .= '<tr>'."\r\n";
										###ReportTitle
											$x .= '<td rowspan="'.$subjectNum.'" width="30%">'."\r\n";
												$x .= $___reportSubjectInfo['ReportTitle']."\r\n";
											$x .= '</td>'."\r\n";
											###SubjectName (First Row)
											$x .= '<td>'."\r\n";
												$x .= $___subjectInfo[0]['subjectName']."\r\n";
											$x .= '</td>'."\r\n";
										$x .= '</tr>'."\r\n";
										###SubjectName
										for($k=1;$k<$subjectNum;$k++){
											$x .= '<tr>'."\r\n";
												$x .= '<td>'."\r\n";
													$x .= $___subjectInfo[$k]['subjectName']."\r\n";
												$x .= '</td>'."\r\n";
											$x .= '</tr>'."\r\n";
										}
									}
								}
							$x .= '</table>'."\r\n";
							$x .= '<br/>'."\r\n";
						}
					}
				$x .= '</td>'."\r\n";
			$x .= '</tr>'."\r\n";
			
		$x .= '</table>'."\r\n";
	}
}	

	
	
############################################################################################################



$linterface->LAYOUT_START();
?>
<form name="form1" method="post" action="remove.php">
<?=$hiddenInput?>
<?php if($SchemeNum>0){?>
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
	<tr>
		<td><?=$linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION);?></td>
	</tr>
	<tr>
		<td>
			<?=$WarningBox?>
			<?=$x?>
		</td>
	</tr>
	<tr>
		<td>
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
			<tr>
				<td height="1" class="dotline" colspan="2"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1"></td>
			</tr>
			<tr>
				<td align="center" valign="bottom">
					<?= $linterface->GET_ACTION_BTN($button_submit, "submit") ?>
					<?= $linterface->GET_ACTION_BTN($button_back, "button", "window.location='index.php'") ?>
				</td>
			</tr>	
			</table>
		</td>
	</tr>	
</table>
<?php }elseif($SchemeNum==0&&$selSchemeIDsNum>0){ ?>
<script>
	document.form1.submit();
</script>
<?php } ?>

<br />
</form>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>

