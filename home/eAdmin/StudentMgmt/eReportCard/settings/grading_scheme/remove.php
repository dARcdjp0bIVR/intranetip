<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
intranet_auth();
intranet_opendb();
$lreportcard = new libreportcard();
$lreportcard->hasAccessRight();
for($i=0;$i<count((array)$SchemeIDs);$i++){
	$result['delete_scheme_'.$i] = $lreportcard->DELETE_GRADING_SCHEME_AND_RANGE($SchemeIDs[$i]);
}

$Msg = (!in_array(false,$result))?"delete":"delete_failed";
intranet_closedb();

header("Location:index.php?Msg=$Msg");
?>

