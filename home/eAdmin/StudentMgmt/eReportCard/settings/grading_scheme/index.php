<?php
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
intranet_auth();
intranet_opendb();
$lreportcard = new libreportcard();
$linterface = new interface_html();
$lreportcard->hasAccessRight();

$CurrentPage = "Settings_GradingScheme";
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
# tag information
$TAGS_OBJ = array();
$TAGS_OBJ[] = array($eReportCard['Settings_GradingScheme']);
############################################################################################################
$sql = "
	select 
		SchemeTitle,
		CONCAT(\"<input type='checkbox' name='SchemeIDs[]' value='\", SchemeID,\"'>\")
	from 
		".$lreportcard->DBName.".RC_GRADING_SCHEME 
";

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

# initialize sorting order of the table - default order is ASC
if ($order=="")
	$order = 1;
if ($field=="")
	$field = 1;
		
$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("SchemeTitle");

$li->sql = $sql;
$li->no_col = count($li->field_array) + 2;
$li->IsColOff = "IP25_table";

$pos = 0; 
$li->column_list .= "<th width='1' class='num_check'>#</th>\n";
$li->column_list .= "<th>".$li->column_IP25($pos++, $eReportCard['GradingScheme'])."</th>\n";
$li->column_list .= "<th width=1>".$li->check("SchemeIDs[]")."</th>\n";
$DBTableBtnArr[] = array("delete", "javascript:checkRemove(document.form1, 'SchemeIDs[]', 'remove_confirm.php')");
$DBTableBtn = $linterface->Get_DBTable_Action_Button_IP25($DBTableBtnArr);
############################################################################################################



$linterface->LAYOUT_START();
?>
<form name="form1" method="get">
<table width="100%" border="0" cellpadding="3" cellspacing="0" align="center">
	<tr>
		<td align="left"></td>
		<td align="right"><?= $linterface->GET_SYS_MSG($Msg); ?></td>
	</tr>
</table>
<div class="table_board">
	<table width="100%" cellspacing="0" cellpadding="0" border="0">
		<tr>
			<td valign="bottom"><?=$DBTableBtn?></td>
		</tr>
	</table>
	<?=$li->display();?>
</div>	
<br />

<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>">
<input type="hidden" name="order" value="1">
<input type="hidden" name="field" value="<?php echo $li->field; ?>">
<input type="hidden" name="page_size_change" value="">
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>">

</form>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>

