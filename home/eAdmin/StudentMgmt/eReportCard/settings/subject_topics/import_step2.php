<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_subjectTopic.php");

$lreportcard = new libreportcardcustom();
$lreportcard_subjectTopic = new libreportcard_subjectTopic();
$limport = new libimporttext();
$lfs = new libfilesystem();
$linterface = new interface_html();

$lreportcard->hasAccessRight();
$successAry = array();


### Check csv file format
$name = $_FILES['csvfile']['name'];
$ext = strtoupper($lfs->file_ext($name));
if(!($ext == ".CSV" || $ext == ".TXT")) {
	intranet_closedb();
	header("location: import_step1.php?returnMsgKey=WrongFileFormat");
	exit();
}


### move to temp folder first for others validation
$folderPrefix = $intranet_root."/file/import_temp/reportcard/subject_topics";
if (!file_exists($folderPrefix)) {
	$lfs->folder_new($folderPrefix);
}
$targetFileName = date('Ymd_His').'_'.$_SESSION['UserID'].$ext;
$targetFilePath = stripslashes($folderPrefix."/".$targetFileName);
$successAry['MoveCsvFileToTempFolder'] = $lfs->lfs_move($csvfile, $targetFilePath);

	
$CurrentPage = "Settings_SubjectTopics";	
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
$TAGS_OBJ = array();
$TAGS_OBJ[] = array($eReportCard['Settings_SubjectTopics']);
$ReturnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];
$linterface->LAYOUT_START($ReturnMsg);


### navigation
// $navigationAry[] = array($displayLang, $onclickJs);
$navigationAry[] = array($eReportCard['Settings_SubjectTopics'], 'javascript: goCancel();');
$navigationAry[] = array($Lang['Btn']['Import']);
$htmlAry['navigation'] = $linterface->GET_NAVIGATION_IP25($navigationAry);


### steps
$htmlAry['steps'] = $linterface->GET_IMPORT_STEPS($CurrStep=2);


### Get Data from the csv file
$csvHeaderAry = $lreportcard_subjectTopic->getCsvHeader($targetPropertyAry=array(1));	// Required Column Only
$columnTitleAry = Get_Lang_Selection($csvHeaderAry['Ch'], $csvHeaderAry['En']);
$columnPropertyAry = $lreportcard_subjectTopic->getCsvHeaderProperty();
$csvData = $limport->GET_IMPORT_TXT_WITH_REFERENCE($targetFilePath, "", "", $columnTitleAry, $columnPropertyAry);
$csvColNameAry = array_shift($csvData);
$numOfData = count($csvData);


### Top Info Table
$x = '';
$x .= '<table class="form_table_v30">'."\n";
	$x .= '<tr>'."\n";
		$x .= '<td class="field_title">'.$Lang['General']['SuccessfulRecord'].'</td>'."\n";
		$x .= '<td><div id="SuccessCountDiv"></div></td>'."\n";
	$x .= '</tr>'."\n";
	$x .= '<tr>'."\n";
		$x .= '<td class="field_title">'.$Lang['General']['FailureRecord'].'</td>'."\n";
		$x .= '<td><div id="FailCountDiv"></div></td>'."\n";
	$x .= '</tr>'."\n";
$x .= '</table>'."\n";
$htmlAry['formTable'] = $x;


### iFrame for validation
$thisSrc = "ajax_task.php?task=validateSubjectTopicImport&targetFilePath=".$targetFilePath;
$htmlAry['iframe'] = '<iframe id="ImportIFrame" name="ImportIFrame" src="'.$thisSrc.'" style="width:100%;height:300px;display:none;"></iframe>'."\n";


### Block UI Msg
$processingMsg = str_replace('<!--NumOfRecords-->', '<span id="BlockUISpan">0</span> / '.$numOfData, $Lang['General']['ImportArr']['RecordsValidated']);


### Buttons
$htmlAry['importBtn'] = $linterface->GET_ACTION_BTN($Lang['Btn']['Import'], "button", "goImport();", 'ImportBtn', '', $Disabled=1);
$htmlAry['backBtn'] = $linterface->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "goBack();");
$htmlAry['cancelBtn'] = $linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "goCancel();");

?>
<script type="text/javascript">
$(document).ready( function() {
	Block_Document('<?=$processingMsg?>');
});

function goCancel() {
	window.location = 'index.php';
}

function goBack() {
	window.location = 'import_step2.php';
}

function goImport() {
	$('form#form1').attr('action', 'import_step3.php').submit();
}

</script>
<form name="form1" id="form1" method="POST">
	<?=$htmlAry['navigation']?>
	<p class="spacer"></p>
	
	<?=$htmlAry['steps']?>
	
	<div class="table_board">
		<?=$htmlAry['formTable']?>
		<?=$htmlAry['iframe']?>
		<div id="ErrorTableDiv"></div>
		<p class="spacer"></p>
	</div>
	<br style="clear:both;" />
		
	<div class="edit_bottom_v30">
		<p class="spacer"></p>
		<?=$htmlAry['importBtn']?>
		<?=$htmlAry['backBtn']?>
		<?=$htmlAry['cancelBtn']?>
		<p class="spacer"></p>
	</div>
</form>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>