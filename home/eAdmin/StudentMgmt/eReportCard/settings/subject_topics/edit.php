<?php
// Modifying by Bill

/********************************************************
 * Modification log
 * 20201103 Bill    [2020-0915-1830-00164]
 *      - added term setting    ($eRCTemplateSetting['Settings']['SubjectTopics_TermSettings'])
 * 20180105 Bill	[2018-0105-1141-20066]
 * 		- Increase topic size to 500
 * ******************************************************/

$PageRight = "ADMIN";

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_subjectTopic.php");

$lreportcard = new libreportcardcustom();
$lreportcard_subjectTopic = new libreportcard_subjectTopic();
$linterface = new interface_html();

$lreportcard->hasAccessRight();

### Get parameter
$returnMsgKey = $_GET['returnMsgKey'];
$formId = $_POST['formId'];
$subjectId = $_POST['subjectId'];
$subjectTopicId = $_POST['subjectTopicId'];
$subjectTopicIdAry = $_POST['subjectTopicIdAry'];
if (is_array($subjectTopicIdAry) && count($subjectTopicIdAry) > 0) {
	$subjectTopicId = $subjectTopicIdAry[0];
}

$CurrentPage = "Settings_SubjectTopics";	
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();

$TAGS_OBJ = array();
$TAGS_OBJ[] = array($eReportCard['Settings_SubjectTopics']);

$ReturnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];
$linterface->LAYOUT_START($ReturnMsg);

if ($subjectTopicId == '') {
	// new
	$actionDisplay = $Lang['Btn']['New'];
}
else {
	// edit
	$actionDisplay = $Lang['Btn']['Edit'];
	
	$subjectTopicAry = $lreportcard_subjectTopic->getSubjectTopic($formId, $subjectId, $subjectTopicId);
	$code = $subjectTopicAry[0]['Code'];
	$nameEn = $subjectTopicAry[0]['NameEn'];
	$nameCh = $subjectTopicAry[0]['NameCh'];
	// [2020-0915-1830-00164]
	$targetTermSeq = $subjectTopicAry[0]['TargetTermSeq'];
}

### navigation
// $navigationAry[] = array($displayLang, $onclickJs);
$navigationAry[] = array($eReportCard['Settings_SubjectTopics'], 'javascript: goBack();');
$navigationAry[] = array($actionDisplay);
$htmlAry['navigation'] = $linterface->GET_NAVIGATION_IP25($navigationAry);

### edit table
$formName = $lreportcard->returnClassLevel($formId);
$subjectName = $lreportcard->GET_SUBJECT_NAME_LANG($subjectId);

$x = '';
$x .= '<table class="form_table_v30">'."\r\n";
	$x .= '<tbody>'."\r\n"; 
		$x .= '<tr>'."\r\n";
			$x .= '<td class="field_title">'.$Lang['General']['Form'].'</td>'."\r\n";
			$x .= '<td>'."\r\n";
				$x .= $formName."\r\n";
			$x .= '</td>'."\r\n";
		$x .= '</tr>'."\r\n";
		$x .= '<tr>'."\r\n";
			$x .= '<td class="field_title">'.$Lang['SysMgr']['SubjectClassMapping']['Subject'].'</td>'."\r\n";
			$x .= '<td>'."\r\n";
				$x .= $subjectName."\r\n";
			$x .= '</td>'."\r\n";
		$x .= '</tr>'."\r\n";
		$x .= '<tr>'."\r\n";
			$x .= '<td class="field_title">'.$linterface->RequiredSymbol().$Lang['General']['Code'].'</td>'."\r\n";
			$x .= '<td>'."\r\n";
				$x .= $linterface->GET_TEXTBOX_NAME('codeTb', 'code', $code, $OtherClass='requiredField', $OtherPar=array());
				$x .= $linterface->Get_Form_Warning_Msg('codeTbEmptyWarnDiv', $Lang['General']['JS_warning']['InputCode'], $Class='warnMsgDiv')."\n";
				$x .= $linterface->Get_Form_Warning_Msg('codeTbCodeInUseWarnDiv', $Lang['General']['JS_warning']['CodeIsInUse'], $Class='warnMsgDiv')."\n";
			$x .= '</td>'."\r\n";
		$x .= '</tr>'."\r\n";
		$x .= '<tr>'."\r\n";
			$x .= '<td class="field_title" rowspan="2">'.$linterface->RequiredSymbol().$Lang['General']['Name'].'</td>'."\r\n";
			$x .= '<td>'."\r\n";
				$x .= '<span class="sub_row_content_v30">'.$Lang['General']['FormFieldLabel']['En'].'</span>'."\r\n";
				$x .= '<span class="row_content_v30" style="width:70%;">'."\r\n";
					$x .= $linterface->GET_TEXTBOX('nameEnTb', 'nameEn', $nameEn, $OtherClass='requiredField', $OtherPar=array('maxlength'=>500));
					$x .= $linterface->Get_Form_Warning_Msg('nameEnTbEmptyWarnDiv', $Lang['General']['JS_warning']['InputEnglishName'], $Class='warnMsgDiv')."\n";
				$x .= '</span>'."\r\n";
			$x .= '</td>'."\r\n";
		$x .= '</tr>'."\r\n";
		$x .= '<tr>'."\r\n";
			$x .= '<td>'."\r\n";
				$x .= '<span class="sub_row_content_v30">'.$Lang['General']['FormFieldLabel']['Ch'].'</span>'."\r\n";
				$x .= '<span class="row_content_v30" style="width:70%;">'."\r\n";
					$x .= $linterface->GET_TEXTBOX('nameChTb', 'nameCh', $nameCh, $OtherClass='requiredField', $OtherPar=array('maxlength'=>500));
					$x .= $linterface->Get_Form_Warning_Msg('nameChTbEmptyWarnDiv', $Lang['General']['JS_warning']['InputChineseName'], $Class='warnMsgDiv')."\n";
				$x .= '</span>'."\r\n";
			$x .= '</td>'."\r\n";
		$x .= '</tr>'."\r\n";

		// [2020-0915-1830-00164]
		if($eRCTemplateSetting['Settings']['SubjectTopics_TermSettings']) {
            $x .= '<tr>'."\r\n";
                $x .= '<td class="field_title" rowspan="2">'.$linterface->RequiredSymbol().$Lang['General']['Term'].'</td>'."\r\n";
                $x .= '<td>'."\r\n";
                    foreach((array)$lreportcard_subjectTopic->subjectTopicRelatedTermArr as $thisTermSeq => $thisTermName) {
                        if ($thisTermSeq == 0) {
                            $thisTermName = $eReportCard['WholeYear'];
                        }
                        $x .= $linterface->Get_Radio_Button('targetTermSeq_'.$thisTermSeq, 'targetTermSeq', $thisTermSeq, ($targetTermSeq==$thisTermSeq), '', $thisTermName)."&nbsp;";
                    }
                $x .= '</td>'."\r\n";
            $x .= '</tr>'."\r\n";
        }
	$x .= '</tbody>'."\r\n";
$x .= '</table>'."\r\n";
$htmlAry['formTable'] = $x;

### buttons
$htmlAry['submitBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Submit'], "button", "goSubmit()", 'submitBtn');
$htmlAry['backBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Back'], "button", "goBack()", 'backBtn');

### hidden fields
$htmlAry['formIdHiddenField'] = $linterface->GET_HIDDEN_INPUT('formId', 'formId', $formId);
$htmlAry['subjectIdHiddenField'] = $linterface->GET_HIDDEN_INPUT('subjectId', 'subjectId', $subjectId);
$htmlAry['subjectTopicIdHiddenField'] = $linterface->GET_HIDDEN_INPUT('subjectTopicId', 'subjectTopicId', $subjectTopicId);
?>

<script type="text/javascript">
$(document).ready( function() {
	$('input#codeTb').focus();
});

function goBack() {
	window.location = 'index.php';
}

function goSubmit() {
	var canSubmit = true;
	var isFocused = false;
	var emptyCount = 0;
	
	$('div.warnMsgDiv').hide();
	
	// disable action button to prevent double submission
	$('input.actionBtn').attr('disabled', 'disabled');
	
	// check required fields
	$('.requiredField').each( function()
	{
		var _elementId = $(this).attr('id');
		if (isObjectValueEmpty(_elementId))
		{
			if(_elementId == 'codeTb') {
				canSubmit = false;
				$('div#' + _elementId + 'EmptyWarnDiv').show();
				if (!isFocused) {
					// focus the first element
					$('#' + _elementId).focus();
					isFocused = true;
				}
			}
			else {
				emptyCount++;
			}
		}
	});
	
	// display warning if both topic lang empty
	if(emptyCount==2)
	{
		canSubmit = false;
		$('.requiredField').each( function()
		{
			var _elementId = $(this).attr('id');
			if (isObjectValueEmpty(_elementId))
			{
				if(_elementId != 'codeTb') {
					$('div#' + _elementId + 'EmptyWarnDiv').show();
					if (!isFocused) {
						// focus the first element
						$('#' + _elementId).focus();
						isFocused = true;
					}
				}
			}
		});
	}
	
	if (canSubmit) {
		$.post(
			"ajax_task.php", 
			{ 
				task: 'validateCode',
				formId: $('input#formId').val(),
				subjectId: $('input#subjectId').val(),
				code: $('input#codeTb').val(),
				subjectTopicId: $('input#subjectTopicId').val()
			},
			function(ReturnData) {
				if (ReturnData == '1') {
					$('form#form1').attr('action', 'edit_update.php').submit();
				}
				else {
					$('div#codeTbCodeInUseWarnDiv').show();
					$('input.actionBtn').attr('disabled', '');
				}
			}
		);
	}
	else {
		// enable the action buttons if not allowed to submit
		$('input.actionBtn').attr('disabled', '');
	}
}
</script>

<form name="form1" id="form1" method="POST">
	<?=$htmlAry['navigation']?>
	<p class="spacer"></p>
	
	<div class="table_board">
		<?=$htmlAry['formTable']?>
		<p class="spacer"></p>
	</div>
	<br style="clear:both;" />
		
	<div class="edit_bottom_v30">
		<p class="spacer"></p>
		<?=$htmlAry['submitBtn']?>
		<?=$htmlAry['backBtn']?>
		<p class="spacer"></p>
	</div>
	
	<?=$htmlAry['formIdHiddenField']?>
	<?=$htmlAry['subjectIdHiddenField']?>
	<?=$htmlAry['subjectTopicIdHiddenField']?>
</form>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>