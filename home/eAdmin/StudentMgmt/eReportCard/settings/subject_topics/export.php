<?php
// Modifying by Bill

/********************************************************
 * Modification log
 * 20201103 Bill    [2020-0915-1830-00164]
 *      - support term setting export   ($eRCTemplateSetting['Settings']['SubjectTopics_TermSettings'])
 * ******************************************************/

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_subjectTopic.php");

$lexport = new libexporttext();
$lreportcard = new libreportcardcustom();
$lreportcard_subjectTopic = new libreportcard_subjectTopic();

$lreportcard->hasAccessRight();

$formId = standardizeFormPostValue($_POST['formId']);
$subjectId = standardizeFormPostValue($_POST['subjectId']);
$exportMode = standardizeFormPostValue($_POST['exportMode']);

$formIdAry = array();
$subjectIdAry = array();

if ($exportMode == 1 || $exportMode == 4) {
	// all forms all subjects or import sample
	$formId = '';
	$subjectId = '';
	
	$formIdAry = Get_Array_By_Key($lreportcard->GET_ALL_FORMS($hasTemplateOnly=false, $excludeWithoutClass=false), 'ClassLevelID');
}
else if ($exportMode == 2) {
	// this form all subjects
	$formIdAry[] = $formId; 
	$subjectId = '';
}
else if ($exportMode == 3) {
	// this form this subject
	$formIdAry[] = $formId; 
}
$numOfForm = count($formIdAry);

if ($exportMode != 4) {
	$subjectTopicAssoAry = BuildMultiKeyAssoc($lreportcard_subjectTopic->getSubjectTopic($formId, $subjectId), array('YearID', 'SubjectID'), $IncludedDBField=array(), $SingleValue=0, $BuildNumericArray=1);
}
$subjectAssoAry = $lreportcard->GET_SUBJECTS_CODEID_MAP($withComponent=1, $mapByCode=0);
$subjectCodeAssoAry = $lreportcard->GET_SUBJECTS_CODEID_MAP($withComponent=1, $mapByCode=1);

$dataAry = array();
$rowCount = 0;
for ($i=0; $i<$numOfForm; $i++) {
	$_formId = $formIdAry[$i];
	$_formName = $lreportcard->returnClassLevel($_formId);
	
	$_subjectIdAry = array_keys($lreportcard->returnSubjectwOrderNoL($_formId));
	$_numOfSubject = count($_subjectIdAry);
	
	for ($j=0; $j<$_numOfSubject; $j++) {
		$__subjectId = $_subjectIdAry[$j];
		
		$__parentSubjectCode = $subjectAssoAry[$__subjectId]['CODEID'];
		$__parentSubjectId = $subjectCodeAssoAry[$__parentSubjectCode.'_'];
		$__parentSubjectName = $lreportcard->GET_SUBJECT_NAME_LANG($__parentSubjectId);
		
		$__componentSubjectCode = '';
		$__componentSubjectId = '';
		$__componentSubjectName = '';
		if ($subjectAssoAry[$__subjectId]['CMP_CODEID'] != '') {
			$__componentSubjectCode = $subjectAssoAry[$__subjectId]['CMP_CODEID'];
			$__componentSubjectId = $subjectCodeAssoAry[$__parentSubjectCode.'_'.$__componentSubjectCode];
			$__componentSubjectName = $lreportcard->GET_SUBJECT_NAME_LANG($__componentSubjectId);
		}
		
		$__subjectTopicAry = $subjectTopicAssoAry[$_formId][$__subjectId];
		$__numOfSubjectTopic = count((array)$__subjectTopicAry);
		
		// export empty subject row for import sample
		if ($exportMode == 4 && $__numOfSubjectTopic == 0) {
			$__numOfSubjectTopic = 1;
		}
		
		for ($k=0; $k<$__numOfSubjectTopic; $k++) {
			$___code = $__subjectTopicAry[$k]['Code'];
			$___nameEn = $__subjectTopicAry[$k]['NameEn'];
			$___nameCh = $__subjectTopicAry[$k]['NameCh'];

            // [2020-0915-1830-00164]
            if ($eRCTemplateSetting['Settings']['SubjectTopics_TermSettings']) {
                $___targetTermSeq = $__subjectTopicAry[$k]['TargetTermSeq'];
                $___targetTermSeq = $lreportcard_subjectTopic->subjectTopicRelatedTermArr[$___targetTermSeq];
            }
			
			$dataAry[$rowCount][] = $_formName;
			$dataAry[$rowCount][] = $__parentSubjectCode;
			$dataAry[$rowCount][] = $__parentSubjectName;
			$dataAry[$rowCount][] = $__componentSubjectCode;
			$dataAry[$rowCount][] = $__componentSubjectName;
			$dataAry[$rowCount][] = $___code;
			$dataAry[$rowCount][] = $___nameEn;
			$dataAry[$rowCount][] = $___nameCh;
            // [2020-0915-1830-00164]
            if ($eRCTemplateSetting['Settings']['SubjectTopics_TermSettings']) {
                $dataAry[$rowCount][] = $___targetTermSeq;
            }

			$rowCount++;
		}
	}
}

intranet_closedb();

$headerAry = $lexport->GET_EXPORT_HEADER_COLUMN($lreportcard_subjectTopic->getCsvHeader(), $lreportcard_subjectTopic->getCsvHeaderProperty());
$exportContent = $lexport->GET_EXPORT_TXT($dataAry, $headerAry);

// Output the file to user browser
$fileName = 'subjectTopic_'.date('Ymd_His').'.csv';
$lexport->EXPORT_FILE($fileName, $exportContent);
?>