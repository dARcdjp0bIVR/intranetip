<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping_ui.php");

$arrCookies = array();
$arrCookies[] = array("ck_reportcard_settings_subjectTopics_formId", "formId");
$arrCookies[] = array("ck_reportcard_settings_subjectTopics_subjectId", "subjectId");
$arrCookies[] = array("ck_reportcard_settings_subjectTopics_keyword", "keyword");
if(isset($clearCoo) && $clearCoo == 1) {
	clearCookies($arrCookies);
}
else {
	updateGetCookies($arrCookies);
}

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");

$lreportcard = new libreportcardcustom();
$lreportcard_ui = new libreportcard_ui();
$linterface = new interface_html();
$scm_ui = new subject_class_mapping_ui;


$lreportcard->hasAccessRight();


### Get parameter
$returnMsgKey = $_GET['returnMsgKey'];

	
$CurrentPage = "Settings_SubjectTopics";	
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
$TAGS_OBJ = array();
$TAGS_OBJ[] = array($eReportCard['Settings_SubjectTopics']);
$ReturnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];
$linterface->LAYOUT_START($ReturnMsg);


### Toolbar
$btnAry = array();
$btnAry[] = array('new', 'javascript: goNew();');
$btnAry[] = array('import', 'javascript: goImport();');

$subBtnAry = array();
$subBtnAry[] = array('javascript: goExport(1);', $Lang['eReportCard']['SettingsArr']['SubjectTopicsArr']['AllFormAllSubject']);
$subBtnAry[] = array('javascript: goExport(2);', $Lang['eReportCard']['SettingsArr']['SubjectTopicsArr']['ThisFormAllSubject']);
$subBtnAry[] = array('javascript: goExport(3);', $Lang['eReportCard']['SettingsArr']['SubjectTopicsArr']['ThisFormThisSubject']);
$btnAry[] = array('export', 'javascript: void(0);', '', $subBtnAry);

$htmlAry['toolbar'] = $linterface->Get_Content_Tool_By_Array_v30($btnAry);


### Filtering
if ($formId == '') {
	$formAry = $lreportcard->GET_ALL_FORMS($hasTemplateOnly=false, $excludeWithoutClass=false);
	$formId = $formAry[0]['ClassLevelID'];
}
$htmlAry['formSelection'] = $lreportcard_ui->Get_Form_Selection('formId', $formId, 'changedFormSelection(this.value);', $noFirst=1, $isAll=0, $hasTemplateOnly=false, $checkPermission=0, $IsMultiple=0, $excludeWithoutClass=false);
$htmlAry['subjectSelection'] = $lreportcard->Get_Subject_Selection($formId, $subjectId, 'subjectId', 'changedSubjectSelection(this.value);', $isMultiple=0, $includeComponent=1, $InputMarkOnly=0, $includeGrandMarks=0, $otherTagInfo='', $ReportID='', $ParentSubjectAsOptGroup=0, $ExcludeWithoutSubjectGroup=0, $TeachingOnly=0, $ExcludeSubjectIDArr='', $ParDefault='', $IncludeConduct=false);


### Action button
$actionBtnAry = array();
$actionBtnAry[] = array('edit', 'javascript:checkEdit(document.form1, \'subjectTopicIdAry[]\', \'edit.php\')');
$actionBtnAry[] = array('delete', 'javascript:checkRemove2(document.form1,\'subjectTopicIdAry[]\',\'goDelete();\')');
$htmlAry['actionBtn'] = $linterface->Get_DBTable_Action_Button_IP25($actionBtnAry);


### hidden fields
$htmlAry['exportModeHiddenField'] = $linterface->GET_HIDDEN_INPUT('exportMode', 'exportMode', '');
		
		
echo $linterface->Include_JS_CSS();
?>
<script type="text/javascript">
$(document).ready( function() {
	$('input#keyword').keydown( function(evt) {
		if (Check_Pressed_Enter(evt)) {
			// pressed enter
			goSearch();
		}
	});
	
	loadDataTable();
});

function initDndTable() {
	$(".common_table_list_v30").tableDnD({
		onDrop: function(table, DroppedRow) {
			if(table.id == "DataTable") {
				Block_Element(table.id);
				
				// Get the order string
				var rowArr = table.tBodies[0].rows;
				var numOfRow = rowArr.length;
				var recordIDArr = new Array();
				for (var i=0; i<numOfRow; i++) {
					var tempId = parseInt(rowArr[i].id.replace('tr_', ''));
					if (tempId != "" && !isNaN(tempId)) {
						recordIDArr[recordIDArr.length] = tempId;
					}
				}
				var recordIDStr = recordIDArr.join(',');
				
				// Update DB
				$.post(
					"ajax_task.php", 
					{ 
						task: "reorderSubjectTopic",
						formId: $('select#formId').val(),
						subjectId: $('select#subjectId').val(),
						displayOrderString: recordIDStr
					},
					function(ReturnData)
					{
						// Reset the ranking display
						var jsRowCounter = 0;
						$('span.rowNumSpan').each( function () {
							$(this).html(++jsRowCounter);
						})
						
						// Get system message
						if (ReturnData == '1') {
							jsReturnMsg = '<?=$Lang['General']['ReturnMessage']['UpdateSuccess']?>';
						}
						else {
							jsReturnMsg = '<?=$Lang['General']['ReturnMessage']['UpdateUnsuccess']?>';
						}
						
						UnBlock_Element(table.id);
						Scroll_To_Top();
						Get_Return_Message(jsReturnMsg);
					}
				);
			}
		},
		onDragStart: function(table, DraggedRow) {
			//$('#debugArea').html("Started dragging row "+row.id);
			
		},
		dragHandle: "Dragable", 
		onDragClass: "move_selected"
	});
}

function loadDataTable() {
	$('div#dataTableDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"ajax_task.php", 
		{ 
			task: 'getSubjectTopicTableHtml',
			formId: $('select#formId').val(),
			subjectId: $('select#subjectId').val()
		},
		function(ReturnData) {
			initDndTable();
		}
	);
}

function changedFormSelection() {
	$('form#form1').attr('action', 'index.php').submit();
}

function changedSubjectSelection() {
	$('form#form1').attr('action', 'index.php').submit();
}

function goNew() {
	$('form#form1').attr('action', 'edit.php').submit();
}

function goDelete() {
	$('form#form1').attr('action', 'delete_update.php').submit();
}

function goExport(parExportMode) {
	$('input#exportMode').val(parExportMode);
	$('form#form1').attr('action', 'export.php').submit();
}

function goImport() {
	$('form#form1').attr('action', 'import_step1.php').submit();
}
</script>
<form id="form1" name="form1" method="post" action="index.php">
	<div class="content_top_tool">
		<div class="Conntent_tool">
	       <?=$htmlAry['toolbar']?>
		</div>
	</div>
	
	<div class="table_board">
		<div class="table_filter">
			<?=$htmlAry['formSelection']?>
			<?=$htmlAry['subjectSelection']?>
		</div>
		<p class="spacer"></p>
		
		<?=$htmlAry['actionBtn']?>
		<div id="dataTableDiv"><!--load by ajax--></div>
	</div>
	
	<?=$htmlAry['exportModeHiddenField']?>
</form>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>