<?php 
#########################
#
#	Date: 2017-07-10 Anna
#	- create file for input Allow Access IP 
#
###########################
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if(!$plugin['ReportCard2008'])
{
	intranet_closedb();
	header("Location: index.php");
}
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "")
{
	include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
	$lreportcard = new libreportcardcustom();
} else {
	$lreportcard = new libreportcard();
}
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
$lreportcard_ui = new libreportcard_ui();
if (!$lreportcard->hasAccessRight())
{
	header ("Location: /");
	intranet_closedb();
	exit();
}

$linterface = new interface_html();
$CurrentPage = "Settings_AllowAccessIP";
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();

$TAGS_OBJ[] = array($eReportCard['Settings_AllowAccessIP']);

$linterface->LAYOUT_START();

$AllowAccessIP = $lreportcard->geteReportCardGeneralSetting("AllowAccessIP");

?>
<script type="text/javascript" language="Javascript">
function checkForm(obj)
{
	var input_ip = document.getElementById('AllowAccessIP').value.Trim();
	var is_valid = true;
	
	if(input_ip != '') {
		var ip_ary = input_ip.split('\n');
		for(var i=0;i<ip_ary.length;i++) {
			if(!ip_ary[i].match(/([0-9]+)\.([0-9]+)\.([0-9]+)\.([0-9]+|\[\d+-\d+\])(\/[0-9]+)*/)){
				is_valid = false;
				break;
			}
		}
	}
	if(!is_valid) {
		alert('<?=$Lang['Security']['WarnMsgArr']['InvalidIPAddress']?>');
		return false;
	}
	obj.submit();
}
</script>
<form name="form1" action="update.php" method="post" onsubmit="checkForm(this); return false;">
	<table align="center" width="90%" border="0" cellpadding="5" cellspacing="0" >
		<tr>
			<td>&nbsp;</td>
			<td align="right"><?=$linterface->GET_SYS_MSG($msg);?></td>
		</tr>
		<tr>
			<td colspan="2" align="right">&nbsp;</td>
		</tr>
		<tr>
			<td valign="top" nowrap="nowrap" class="tabletext">
				<?=$Lang['Security']['TerminalIPList']?>
			</td>
			<td class="tabletext" width="70%">
				<span class="tabletextremark"><?=$Lang['Security']['TerminalIPInput']?><br />
				<?=$Lang['Security']['TerminalYourAddress'].':'.getRemoteIpAddress()?></span><br />
				<?=$linterface->GET_TEXTAREA("AllowAccessIP", $AllowAccessIP)?>
			</td>
		</tr>
		<tr>
			<td colspan="2" class="formfieldtitle tabletext">
				<br /><?=$Lang['Security']['TerminalIPSettingsDescription']?>
			</td>
		</tr>
	</table>
	<table align="center" width="95%" border="0" cellpadding="5" cellspacing="0" >
		<tr>
			<td height="1" class="dotline" colspan="2">
				<img src="<?=$PATH_WRT_ROOT.'images/'.$LAYOUT_SKIN?>/10x10.gif" width="10" height="1">
			</td>
		</tr>
		<tr>
			<td align="center" valign="bottom" colspan="2">
				<?=$linterface->GET_ACTION_BTN($Lang['Btn']['Save'], "submit")?>
				<?=$linterface->GET_ACTION_BTN($Lang['Btn']['Reset'], "reset")?>
			</td>
		</tr>
	</table>
</form>
<br>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>