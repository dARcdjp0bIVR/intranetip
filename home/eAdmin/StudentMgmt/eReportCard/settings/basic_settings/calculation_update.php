<?php
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

$PageRight = "ADMIN";

if (!isset($_POST))
	header("Location:calculation.php");

if (!$plugin['ReportCard2008']) {
	intranet_closedb();
	header("Location:/index.php");
}

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
$lreportcard = new libreportcard();

if (!$lreportcard->hasAccessRight()) {
	intranet_closedb();
	header("Location:/index.php");
}


if (!isset($adjustToFullMarkFirst))
	$adjustToFullMarkFirst = "0";
if (!isset($useWeightedMark))
	$useWeightedMark = "0";

$SettingCategory = "Calculation";

$calSettings["UseWeightedMark"] = $useWeightedMark;
$calSettings["AdjustToFullMarkFirst"] = $adjustToFullMarkFirst;
$calSettings["OrderTerm"] = $calculationOrderTerm;
$calSettings["OrderFullYear"] = $calculationOrderWhole;

$temp = $lreportcard->LOAD_SETTING($SettingCategory, "", $returnDBValue=1);
if(sizeof($temp))
 	$success = $lreportcard->UPDATE_SETTING($SettingCategory, $calSettings);
else
	$success = $lreportcard->INSERT_SETTING($SettingCategory, $calSettings);

intranet_closedb();

$Result = ($success) ? "update" : "update_failed";
header("Location:calculation.php?Result=$Result");
?>

