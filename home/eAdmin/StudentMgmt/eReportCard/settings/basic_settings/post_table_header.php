<?php
// Using:

/****************************************
 *
 *  Date:   2020-02-10  (Bill)  [2019-0924-1219-32289]
 *          Create file
 *
 ****************************************/

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if (!$plugin['ReportCard2008']) {
    header ("Location: /");
    intranet_closedb();
    exit();
}

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") {
    include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
    $lreportcard = new libreportcardcustom();
}
else {
    $lreportcard = new libreportcard();
}

if (!$lreportcard->hasAccessRight() || !$eRCTemplateSetting['OtherInfo']['CustUploadType_Post'])
{
    header ("Location: /");
    intranet_closedb();
    exit();
}

$linterface = new interface_html();

$CurrentPage = "Settings_BasicSettings";
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();

# Tag
$TAGS_OBJ[] = array($eReportCard['StyleSettings'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/settings/basic_settings/highlight.php", 0);
$TAGS_OBJ[] = array($eReportCard['MarkStorageAndDisplay'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/settings/basic_settings/mark_storage_display.php", 0);
$TAGS_OBJ[] = array($eReportCard['AbsentAndExemptSettings'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/settings/basic_settings/absent_exempt.php", 0);
$TAGS_OBJ[] = array($eReportCard['CalculationSettings'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/settings/basic_settings/calculation.php", 0);
$TAGS_OBJ[] = array($eReportCard['AccessSettings'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/settings/basic_settings/access_setting.php", 0);
$TAGS_OBJ[] = array($eReportCard['PostTableHeader'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/settings/basic_settings/post_table_header.php", 1);

$linterface->LAYOUT_START();

# Declare variable value
$SettingCategory = "PostTableHeader";    // Setting Category in database

# Load the highlight setting data
$data = $lreportcard->LOAD_SETTING($SettingCategory);
$data["Column1Ch"] = $data["Column1Ch"] ? $data["Column1Ch"] : $eReportCard['Template']['SchoolManagementPostCh'];
$data["Column1En"] = $data["Column1En"] ? $data["Column1En"] : $eReportCard['Template']['SchoolManagementPostEn'];
$data["Column1Width"] = $data["Column1Width"] ? $data["Column1Width"] : 50;
$data["Column2Ch"] = $data["Column2Ch"] ? $data["Column2Ch"] : $eReportCard['Template']['OthersPostCh'];
$data["Column2En"] = $data["Column2En"] ? $data["Column2En"] : $eReportCard['Template']['OthersPostEn'];
$data["Column2Width"] = $data["Column2Width"] ? $data["Column2Width"] : 50;
?>

<form name="form1" action="post_table_header_update.php" method="post">
    <table width="98%" border="0" cellspacing="0" cellpadding="2">
        <tr>
            <td>&nbsp;</td>
            <td align="right"><?=$linterface->GET_SYS_MSG($msg);?></td>
        </tr>
        <tr>
            <td colspan="2">
                <table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
                    <tr>
                        <td valign="top" nowrap="nowrap" class="field_title" width="25%">
                            <span class="tabletextrequire">*</span><?= $eReportCard['Template']['Column1NameCh'] ?>
                        </td>
                        <td width="50%">
                            <input type="text" name="Column1Ch" class="textboxtext" value="<?=$data["Column1Ch"]?>" />
                            <div class="error_msg_div" id="Column1ChMsgDiv" style="display:none"><span class="tabletextrequire">*<?=$Lang['General']['JS_warning']['InputChineseName']?></span></div>
                        </td>
                        <td width="25%">&nbsp;&nbsp;</td>
                    </tr>
                    <tr>
                        <td valign="top" nowrap="nowrap" class="field_title" width="25%">
                            <span class="tabletextrequire">*</span><?= $eReportCard['Template']['Column1NameEn'] ?>
                        </td>
                        <td width="50%">
                            <input type="text" name="Column1En" class="textboxtext" value="<?=$data["Column1En"]?>" />
                            <div class="error_msg_div" id="Column1EnMsgDiv" style="display:none"><span class="tabletextrequire">*<?=$Lang['General']['JS_warning']['InputEnglishName']?></span></div>
                        </td>
                        <td width="25%">&nbsp;&nbsp;</td>
                    </tr>
                    <tr>
                        <td valign="top" nowrap="nowrap" class="field_title" width="25%">
                            <span class="tabletextrequire">*</span><?= $eReportCard['Template']['Column1Width'] ?>
                        </td>
                        <td width="50%">
                            <input type="number" name="Column1Width" id="Column1Width" value="<?=$data["Column1Width"]?>" /> %
                            <div class="error_msg_div" id="Column1WidthMsgDiv" style="display:none"><span class="tabletextrequire">*<?=$Lang['General']['JS_warning']['InputPositiveValue']?></span></div>
                        </td>
                        <td width="25%">&nbsp;&nbsp;</td>
                    </tr>
                    <tr>
                        <td valign="top" nowrap="nowrap" class="field_title" width="25%">
                            <span class="tabletextrequire">*</span><?= $eReportCard['Template']['Column2NameCh'] ?>
                        </td>
                        <td width="50%">
                            <input type="text" name="Column2Ch" class="textboxtext" value="<?=$data["Column2Ch"]?>" />
                            <div class="error_msg_div" id="Column2ChMsgDiv" style="display:none"><span class="tabletextrequire">*<?=$Lang['General']['JS_warning']['InputChineseName']?></span></div>
                        </td>
                        <td width="25%">&nbsp;&nbsp;</td>
                    </tr>
                    <tr>
                        <td valign="top" nowrap="nowrap" class="field_title" width="25%">
                            <span class="tabletextrequire">*</span><?= $eReportCard['Template']['Column2NameEn'] ?>
                        </td>
                        <td width="50%">
                            <input type="text" name="Column2En" class="textboxtext" value="<?=$data["Column2En"]?>" />
                            <div class="error_msg_div" id="Column2EnMsgDiv" style="display:none"><span class="tabletextrequire">*<?=$Lang['General']['JS_warning']['InputEnglishName']?></span></div>
                        </td>
                        <td width="25%">&nbsp;&nbsp;</td>
                    </tr>
                    <tr>
                        <td valign="top" nowrap="nowrap" class="field_title" width="25%">
                            <?= $eReportCard['Template']['Column2Width'] ?>
                        </td>
                        <td width="50%">
                            <input class="textboxnum" type="number" name="Column2Width" maxlength="16" value="<?=$data["Column2Width"]?>" disabled/> %
                        </td>
                        <td width="25%">&nbsp;</td>
                    </tr>
                </table>
                <br />

                <table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
                    <tr>
                        <td colspan="2" height="1" class="dotline"><img src="images/2007a/10x10.gif" width="10" height="1"></td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center" valign="bottom">
                            <?= $linterface->GET_ACTION_BTN($button_save, "button", "submitForm()", "submit_Btn") ?>
                            <?= $linterface->GET_ACTION_BTN($button_reset, "reset") ?>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</form>

<br />

<script>
$("input#Column1Width").bind('keyup mouseup', function () {
    calculateCol2Width(this.value);
});

function calculateCol2Width(val)
{
    if (val < 0 || val > 100)
    {
        if (val > 100) {
            val = 100
        }
        else {
            val = 0;
        }
        document.form1.Column1Width.value = val;
    }

    document.form1.Column2Width.value = 100 - val;
}

function submitForm()
{
    $("input.error_msg_div").hide();
    $("#submit_Btn")[0].disbled = true;

    $isValid = true;
    if(document.form1.Column1Ch.value.trim() == '') {
        $("div#Column1ChMsgDiv").show();
        $isValid = false;
    }
    if(document.form1.Column1En.value.trim() == '') {
        $("div#Column1EnMsgDiv").show();
        $isValid = false;
    }
    if(document.form1.Column1Width.value == '') {
        $("div#Column1WidthMsgDiv").show();
        $isValid = false;
    }
    if(document.form1.Column2Ch.value.trim() == '') {
        $("div#Column2ChMsgDiv").show();
        $isValid = false;
    }
    if(document.form1.Column2En.value.trim() == '') {
        $("div#Column2EnMsgDiv").show();
        $isValid = false;
    }

    if($isValid) {
        document.form1.submit()
    }
    else {
        $("#submit_Btn")[0].disbled = false;
    }
}
</script>

<?
//print $linterface->FOCUS_ON_LOAD("form1.AllowClassTeacherUploadCSV[0]");
$linterface->LAYOUT_STOP();
?>
