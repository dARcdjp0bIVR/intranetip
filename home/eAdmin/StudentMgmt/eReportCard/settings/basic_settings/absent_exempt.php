<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if (!$plugin['ReportCard2008'])
{
	header ("Location: /");
	intranet_closedb();
	exit();
}

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") {
	include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
	$lreportcard = new libreportcardcustom();
} else {
	$lreportcard = new libreportcard();
}

if (!$lreportcard->hasAccessRight()) 
{
	header ("Location: /");
	intranet_closedb();
	exit();
}

$linterface = new interface_html();

$CurrentPage = "Settings_BasicSettings";
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();

# tag information
$TAGS_OBJ[] = array($eReportCard['StyleSettings'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/settings/basic_settings/highlight.php", 0);
$TAGS_OBJ[] = array($eReportCard['MarkStorageAndDisplay'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/settings/basic_settings/mark_storage_display.php", 0);
$TAGS_OBJ[] = array($eReportCard['AbsentAndExemptSettings'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/settings/basic_settings/absent_exempt.php", 1);
$TAGS_OBJ[] = array($eReportCard['CalculationSettings'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/settings/basic_settings/calculation.php", 0);
$TAGS_OBJ[] = array($eReportCard['AccessSettings'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/settings/basic_settings/access_setting.php", 0);
if($eRCTemplateSetting['OtherInfo']['CustUploadType_Post']) {
    $TAGS_OBJ[] = array($eReportCard['PostTableHeader'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/settings/basic_settings/post_table_header.php", 0);
}

$linterface->LAYOUT_START();

# Declare variable value
$SettingCategory	= "Absent&Exempt";			// Setting Category in database

# Load the highlight setting data
$data = $lreportcard->LOAD_SETTING($SettingCategory);

?>

<br />

<form name="form1" action="absent_exempt_update.php" method="post">
<table width="96%" border="0" cellspacing="0" cellpadding="2">
<tr>
	<td>&nbsp;</td>
	<td align="right"><?=$linterface->GET_SYS_MSG($msg);?></td>
</tr>
<tr>
	<td colspan="2">
		<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
		<tr>
			<td valign="top" nowrap="nowrap" class="formfieldtitle" style="border-bottom:1px #EEEEEE solid;"><span class="tabletext"><?=$eReportCard['Absent']?></span></td>
			<td width="80%" valign="top" class="tabletext">
				<label for="ExWeight"><input type="radio" name="Absent" id="ExWeight" value="ExWeight" <?=($data[Absent]=="ExWeight" or !$data)?"checked":""?>> <?=$eReportCard['AbsentExcludeWeight']?></label> <br>
				<label for="ExFullMark"><input type="radio" name="Absent" id="ExFullMark" value="ExFullMark" <?=$data[Absent]=="ExFullMark"?"checked":""?>> <?=$eReportCard['AbsentExcludeFullMark']?></label> <br>
				<label for="Mark0"><input type="radio" name="Absent" id="Mark0" value="Mark0" <?=$data[Absent]=="Mark0"?"checked":""?>><?=$eReportCard['AbsentTreatAsZeroMark']?></label> <br>
			</td>
		</tr>
		<tr>
			<td valign="top" nowrap="nowrap" class="formfieldtitle" style="border-bottom:1px #EEEEEE solid;"><span class="tabletext"><?=$eReportCard['Exempt']?></span></td>
			<td valign="top" class="tabletext">
				<label for="ExWeight2"><input type="radio" name="Exempt" id="ExWeight2" value="ExWeight" <?=($data[Exempt]=="ExWeight" or !$data)?"checked":""?>> <?=$eReportCard['ExemptExcludeWeight']?></label> <br>
				<label for="ExFullMark2"><input type="radio" name="Exempt" id="ExFullMark2" value="ExFullMark" <?=$data[Exempt]=="ExFullMark"?"checked":""?>> <?=$eReportCard['ExemptExcludeFullMark']?></label><br>
				<?php /*<label for="Mark02"><input type="radio" name="Exempt" id="Mark02" value="Mark0" <?=$data[Exempt]=="Mark0"?"checked":""?>> <?=$eReportCard['ExemptTreatAsZeroMark']?></label> <br> */?>
			</td>
		</tr>
		
		<tr>
			<td colspan="2" height="1" class="dotline"><img src="images/2007a/10x10.gif" width="10" height="1"></td>
		</tr>
		
		<tr>
			<td colspan="2" align="center" valign="bottom">
				<?= $linterface->GET_ACTION_BTN($button_save, "submit") ?>
                <?= $linterface->GET_ACTION_BTN($button_reset, "reset") ?>
			</td>
		</tr>
		</table>
	</td>
</tr>
</table>
</form>

<br />
                                  
<? 
print $linterface->FOCUS_ON_LOAD("form1.Absent[0]");
$linterface->LAYOUT_STOP(); ?>
