<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
//include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") {
	include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
	$lreportcard = new libreportcardcustom();
} else {
	$lreportcard = new libreportcard();
}

if (!$plugin['ReportCard2008'] || !$lreportcard->hasAccessRight()) 
{
	header ("Location: /");
	intranet_closedb();
	exit();
}

$linterface = new interface_html();

$CurrentPage = "Settings_BasicSettings";
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();

# tag information
$TAGS_OBJ[] = array($eReportCard['StyleSettings'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/settings/basic_settings/highlight.php", 1);
$TAGS_OBJ[] = array($eReportCard['MarkStorageAndDisplay'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/settings/basic_settings/mark_storage_display.php", 0);
$TAGS_OBJ[] = array($eReportCard['AbsentAndExemptSettings'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/settings/basic_settings/absent_exempt.php", 0);
$TAGS_OBJ[] = array($eReportCard['CalculationSettings'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/settings/basic_settings/calculation.php", 0);
$TAGS_OBJ[] = array($eReportCard['AccessSettings'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/settings/basic_settings/access_setting.php", 0);
if($eRCTemplateSetting['OtherInfo']['CustUploadType_Post']) {
    $TAGS_OBJ[] = array($eReportCard['PostTableHeader'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/settings/basic_settings/post_table_header.php", 0);
}

$linterface->LAYOUT_START();

# Get the Color / Symbol data
$color_ary = $lreportcard->returnHighlightColorSelection();
$sym1_ary = $lreportcard->returnHighlightSymbolSelection1();
$sym2_ary = $lreportcard->returnHighlightSymbolSelection2();

# Declare variable value
$SettingCategory	= "HighLight";			// Setting Category in database
$levels = array(							// Level for setting
	array("D", $eReportCard['Distinction']),
	array("P", $eReportCard['Pass']),
	array("F", $eReportCard['Fail'])
);

# Load the highlight setting data
$data = $lreportcard->LOAD_SETTING($SettingCategory);

if($data)	
{
	foreach ($data as $key => $value) 
	{
		$index = substr($key,0,1);
		
		//Format of Setting:  Color|bold|underline|italic|LeftSymbol|RightSymbol
		list($c, $b, $u, $i, $l, $r) = explode("|",$value);
		$v[$index]['color'] = $c;
		$v[$index]['bui'] = ($b?"1":"0").($u?"1":"0").($i?"1":"0");
		$v[$index]['sym'] = $l.$r;
	}
}
else		// Init.
{
	$v['D']['color'] = "000000";	$v['D']['bui'] = "000";		$v['D']['sym'] = "  ";
	$v['P']['color'] = "000000";	$v['P']['bui'] = "000";		$v['P']['sym'] = "  ";
	$v['F']['color'] = "000000";	$v['F']['bui'] = "000";		$v['F']['sym'] = "  ";
}
?>
	
<script language="javascript">
<!--
function toggle_btn(btn_name, btn_index)
{
	var icon_path = "<?=$image_path?>/<?=$LAYOUT_SKIN?>/ereportcard/";
	
 	this_btn = "document.getElementById('" + btn_name + "Icon_" + btn_index + "')";
 	this_btn_src = eval(this_btn + ".src");
 	
 	if(this_btn_src.substr(this_btn_src.length-7)=="_on.gif")
 	{
	 	eval(this_btn + ".src = icon_path + 'btn_"+ btn_name +"_off.gif';");
	 	BUI = 0;
 	}
 	else
 	{
	 	eval(this_btn + ".src = icon_path + 'btn_"+ btn_name +"_on.gif';");
	 	BUI = 1;
 	}
 	
	changeBUI(btn_name, btn_index, BUI);
	changeStyleDisplay(btn_index);
}

// BUI means Bold, Underline & Italic
function changeBUI(btn_name, btn_index, BUI)
{
	form_obj = document.form1;
	
	BUIval	= eval("form_obj.BUI_"+ btn_index +"_select.value;");
	Bval	= BUIval.substring(0, 1);
	Uval	= BUIval.substring(1, 2);
	Ival	= BUIval.substring(2, 3);
	
	this_BUI = btn_name.toUpperCase().substring(0,1);
	eval(this_BUI + "val = BUI;");
	
	// update value
	eval("form_obj.BUI_"+ btn_index +"_select.value = Bval + Uval + Ival;");
}


function ChooseColor(color_btn, btn_index)
{
	document.getElementById('choosecolor').style.left = getPostion(color_btn, 'offsetLeft') + 10 + 'px';
 	document.getElementById('choosecolor').style.top = getPostion(color_btn, 'offsetTop') + 10 + 'px';
 	
	MM_showHideLayers('choosecolor','','show');
	
	document.form1.tempIndex.value = btn_index;
}

function clickColor(c)
{
	MM_showHideLayers('choosecolor','','hide');
	
	form_obj = document.form1;
	btn_index = form_obj.tempIndex.value;
	c = '#' + c;
	eval("document.getElementById('td"+ btn_index +"').bgColor  = c;");
	
	// update value
	eval("form_obj.color_"+ btn_index +"_select.value = c;");
	
	changeStyleDisplay(btn_index);
}

function ChooseSym(sym_btn, btn_index, lname)
{
	eval("document.getElementById('"+ lname +"').style.left = getPostion(sym_btn, 'offsetLeft') + 10 + 'px';");
 	eval("document.getElementById('"+ lname +"').style.top = getPostion(sym_btn, 'offsetTop') + 10 + 'px';");
	eval("MM_showHideLayers('"+ lname +"','','show')");
	
	document.form1.tempIndex.value = btn_index;
}

function clickSym(i, s)
{
	MM_showHideLayers('choosesymbola','','hide');
	MM_showHideLayers('choosesymbolb','','hide');
	
	form_obj = document.form1;
	btn_index = form_obj.tempIndex.value;
	eval("document.getElementById('sym"+ i + btn_index +"').innerHTML = s;");
	
	f = eval("document.getElementById('sym1"+ btn_index +"').innerHTML;");
	e = eval("document.getElementById('sym2"+ btn_index +"').innerHTML;");
	if(f=="") f = " ";
	if(e=="") e = " ";
	
	eval("document.getElementById('"+ btn_index +"_sample1').innerHTML = f + form_obj."+ btn_index +"1.value + e;");
	eval("document.getElementById('"+ btn_index +"_sample2').innerHTML = f + form_obj."+ btn_index +"2.value + e;");

	// update value
	eval("form_obj.sym_"+ btn_index +"_select.value = f+e;");
}

function changeStyleDisplay(btn_index)
{
	form_obj = document.form1;
	
	Colorval= eval("form_obj.color_"+ btn_index +"_select.value;");
	BUIval	= eval("form_obj.BUI_"+ btn_index +"_select.value;");
	Bval	= BUIval.substring(0, 1);
	Uval	= BUIval.substring(1, 2);
	Ival	= BUIval.substring(2, 3);
	
	for(i=1;i<=2;i++)
	{
		obj = "document.getElementById('"+ btn_index +"_sample"+ i +"')";
		
		eval(obj + ".style.fontWeight 		= (Bval==1 ? 'bold' : '');");
		eval(obj + ".style.textDecoration 	= (Uval==1 ? 'underline' : '');");
		eval(obj + ".style.fontStyle 		= (Ival==1 ? 'italic' : '');");
		
		eval(obj + ".style.color 			= Colorval;");
	}
}

function init()
{
	init_Style();

}

function init_Style()
{
	var form_obj = document.form1;
	var icon_path = "<?=$image_path?>/<?=$LAYOUT_SKIN?>/ereportcard/";
	
	var index = new Array("D","P","F");

	for(var i=0;i<3;i++)
	{
		// Style
		eval("document.getElementById('td"+ index[i] +"').bgColor  = form_obj.color_"+ index[i] +"_select.value;");
		eval("document.getElementById('boldIcon_"+ index[i] +"').src = icon_path + 'btn_bold_'+ (form_obj.BUI_"+ index[i] +"_select.value.substring(0, 1)==1?'on':'off') +'.gif';");
		eval("document.getElementById('underlineIcon_"+ index[i] +"').src = icon_path + 'btn_underline_'+ (form_obj.BUI_"+ index[i] +"_select.value.substring(1, 2)==1?'on':'off') +'.gif';");
		eval("document.getElementById('italicIcon_"+ index[i] +"').src = icon_path + 'btn_italic_'+ (form_obj.BUI_"+ index[i] +"_select.value.substring(2, 3)==1?'on':'off') +'.gif';");		
	
		// symbol
		eval("document.getElementById('sym1"+ index[i] +"').innerHTML = form_obj.sym_"+ index[i] +"_select.value.substring(0, 1);");
		eval("document.getElementById('sym2"+ index[i] +"').innerHTML = form_obj.sym_"+ index[i] +"_select.value.substring(1, 2);");
		
		//sample
		eval("document.getElementById('"+ index[i] +"_sample1').innerHTML = document.getElementById('sym1"+ index[i] +"').innerHTML + form_obj."+ index[i] +"1.value + document.getElementById('sym2"+ index[i] +"').innerHTML;");
 		eval("document.getElementById('"+ index[i] +"_sample2').innerHTML = document.getElementById('sym1"+ index[i] +"').innerHTML + form_obj."+ index[i] +"2.value + document.getElementById('sym2"+ index[i] +"').innerHTML;");
 		changeStyleDisplay(index[i]);
	}
}
//-->
</script>

<!-- Start Color Div //-->
<div id="choosecolor" style="position:absolute; width:100px; z-index:1; visibility: hidden; border-top:solid 1px; border-top-color:#999999; border-left:solid 1px; border-left-color:#999999; border-right:solid 2px; border-right-color:#646464; border-bottom:solid 2px; border-bottom-color:#646464" onmouseover="MM_showHideLayers('choosecolor','','show')" onmouseout="MM_showHideLayers('choosecolor','','hide')">
<table border="0" cellpadding="0" cellspacing="5" bgcolor="#feffed">
<? for($i=0;$i<sizeof($color_ary);$i++) 
{
	echo ($i%4==0) ? "<tr>" : "";	
?>
	<td bgcolor="#<?=$color_ary[$i]?>"><a href="javascript:clickColor('<?=$color_ary[$i]?>')"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="20" height="20" border="0"></a></td>
<? 
	echo ($i+1)%4==0 ? "</tr>" : "";	
} ?>
</table>
</div>
<!-- End Color Div //-->

<!-- Start Special Symbol 1 Div //-->
<div id="choosesymbola" style="position:absolute; width:30px; z-index:1; visibility: hidden; border-top:solid 1px; border-top-color:#999999; border-left:solid 1px; border-left-color:#999999; border-right:solid 2px; border-right-color:#646464; border-bottom:solid 2px; border-bottom-color:#646464" onMouseOver="MM_showHideLayers('choosesymbola','','show')" onMouseOut="MM_showHideLayers('choosesymbola','','hide')">
<table border="0" cellpadding="0" cellspacing="5" bgcolor="#feffed">
<? for($i=0;$i<sizeof($sym1_ary);$i++) 
{
	echo ($i%3==0) ? "<tr>" : "";	
?>
	<td><div id="btn_select_symbol"><a href="javascript:clickSym(1, '<?=$sym1_ary[$i]?>')"><span><?=$sym1_ary[$i]?></span></a></div></td>
	<? 
	echo ($i+1)%3==0 ? "</tr>" : "";	
} ?>
</table>
</div>
<!-- End Special Symbol 1 Div //-->

<!-- Start Special Symbol 2 Div //-->
<div id="choosesymbolb" style="position:absolute; width:30px; z-index:1; visibility: hidden; border-top:solid 1px; border-top-color:#999999; border-left:solid 1px; border-left-color:#999999; border-right:solid 2px; border-right-color:#646464; border-bottom:solid 2px; border-bottom-color:#646464" onMouseOver="MM_showHideLayers('choosesymbolb','','show')" onMouseOut="MM_showHideLayers('choosesymbolb','','hide')">
<table border="0" cellpadding="0" cellspacing="5" bgcolor="#feffed">
<? for($i=0;$i<sizeof($sym2_ary);$i++) 
{
	echo ($i%3==0) ? "<tr>" : "";	
?>
	<td><div id="btn_select_symbol"><a href="javascript:clickSym(2,'<?=$sym2_ary[$i]?>')"><span><?=$sym2_ary[$i]?></span></a></div></td>
	<? 
	echo ($i+1)%3==0 ? "</tr>" : "";	
} ?>
</table>
</div>
<!-- End Special Symbol 2 Div //-->

<br />
                            
<form name="form1" method="post" action="highlight_update.php">

<table width="96%" border="0" cellspacing="0" cellpadding="2" align="center">
<tr>
	<td height="320" align="left" valign="top">
		<table width="100%" border="0" cellspacing="0" cellpadding="2">
		<tr>
			<td>&nbsp;</td>
			<td align="right"><?=$linterface->GET_SYS_MSG($msg);?></td>
		</tr>
		<tr>
			<td align="center" colspan="2">
				<table width="80%" border="0" cellpadding="3" cellspacing="0">
				<tr>
					<td width="12%" class="tabletext">&nbsp;</td>
					<td width="22%" align="center" class="tabletop tabletopnolink"><?=$eReportCard['Format']?></td>
					<td width="22%" align="center" class="tabletop tabletopnolink"><?=$eReportCard['SpecialSymbol']?></td>
					<td width="22%" align="center" class="tabletop tabletopnolink"><?=$eReportCard['Sample']?></td>
				</tr>
				
				<? 
				for($i=0;$i<sizeof($levels);$i++) 
				{
					$index_type = $levels[$i][0];
					$level_name = $levels[$i][1];
				?>
				<!-- Start Row //-->
				<tr>
					<td height="40" class="tablelist_subject"><?=$level_name?></td>
					<td align="center" class="tabletext">
						<table border="0" cellspacing="0" cellpadding="2">
						<tr>
							<td align="left">
								<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td bgcolor="#000000" id="td<?=$index_type?>"><a href="#"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/ereportcard/icon_color.gif" width="20" height="20" border="0" onMouseOver="ChooseColor(this, '<?=$index_type?>')" onMouseOut="MM_showHideLayers('choosecolor','','hide')"></a></td>
								</tr>
								</table>
							</td>
							<td align="center"><a href="javascript:toggle_btn('bold', '<?=$index_type?>');"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/ereportcard/btn_bold_off.gif" alt="Bold" width="20" height="20" border="0" id="boldIcon_<?=$index_type?>"></a></td>
							<td align="center"><a href="javascript:toggle_btn('underline', '<?=$index_type?>');"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/ereportcard/btn_underline_off.gif" alt="Underline" width="20" height="20" border="0" id="underlineIcon_<?=$index_type?>"></a></td>
							<td align="center"><a href="javascript:toggle_btn('italic', '<?=$index_type?>');"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/ereportcard/btn_italic_off.gif" alt="Italic" width="20" height="20" border="0" id="italicIcon_<?=$index_type?>"></a></td>
						</tr>
						</table>
					</td>
					<td align="center" class="tabletext">
						<table border="0" cellspacing="0" cellpadding="2">
						<tr>
							<td align="left">
								<table width="100%" border="0" cellspacing="0" cellpadding="0" onMouseOver="ChooseSym(this, '<?=$index_type?>','choosesymbola')" onMouseOut="MM_showHideLayers('choosesymbola','','hide')">
								<tr>
									<td><div id="btn_style_symbol"><a href="#"><span id="sym1<?=$index_type?>"> </span></a></div></td>
								</tr>
								</table>
							</td>
							<td align="center" class="tabletextremark"><?=$eReportCard['Mark']?></td>
							<td align="left">
								<table width="100%" border="0" cellspacing="0" cellpadding="0" onMouseOver="ChooseSym(this, '<?=$index_type?>','choosesymbolb')" onMouseOut="MM_showHideLayers('choosesymbolb','','hide')">
								<tr>
									<td><div id="btn_style_symbol"><a href="#"><span id="sym2<?=$index_type?>"> </span></a></div></td>
								</tr>
								</table>
							</td>
						</tr>
						</table>                                          
					</td>
					<td align="center" class="tabletext"><span id="<?=$index_type?>_sample1"> </span><span class="tabletext"> | </span><span id="<?=$index_type?>_sample2"> </span></td>
				</tr>
				<!-- End Row //-->
				<? } ?>
				
            </table>
          <br></td>
        </tr>
		</table>
    
    
	    <table width="80%" border="0" align="center" cellpadding="0" cellspacing="0">
			<tr>
			  <td height="1" class="dotline"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
			</tr>
			<tr>
			  <td><table width="100%" border="0" cellspacing="0" cellpadding="2">
				  <tr>
					  <td>&nbsp;</td>
					<td align="center" valign="bottom">
						<?= $linterface->GET_ACTION_BTN($button_save, "submit") ?>
						<?= $linterface->GET_ACTION_BTN($button_reset, "button","this.form.reset();init();") ?>
				  </tr>
			  </table></td>
			</tr>
		</table>
	</td>
</tr>
</table>

<input type="hidden" name="tempIndex" value="">

<input type="hidden" name="D1" value="A">
<input type="hidden" name="D2" value="100">
<input type="hidden" name="P1" value="D">
<input type="hidden" name="P2" value="60">
<input type="hidden" name="F1" value="E">
<input type="hidden" name="F2" value="59">

<input type="hidden" name="color_D_select" value="<?=$v['D']['color']?>">
<input type="hidden" name="BUI_D_select" value="<?=$v['D']['bui']?>">
<input type="hidden" name="sym_D_select" value="<?=$v['D']['sym']?>">
<input type="hidden" name="color_P_select" value="<?=$v['P']['color']?>">
<input type="hidden" name="BUI_P_select" value="<?=$v['P']['bui']?>">
<input type="hidden" name="sym_P_select" value="<?=$v['P']['sym']?>">
<input type="hidden" name="color_F_select" value="<?=$v['F']['color']?>">
<input type="hidden" name="BUI_F_select" value="<?=$v['F']['bui']?>">
<input type="hidden" name="sym_F_select" value="<?=$v['F']['sym']?>">
</form>

<script language="javascript">
<!--
init();
//-->
</script>
<?
$linterface->LAYOUT_STOP();
?>
