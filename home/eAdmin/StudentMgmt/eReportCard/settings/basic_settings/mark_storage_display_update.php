<?php
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

$PageRight = "ADMIN";

if (!isset($_POST))
	header("Location:mark_storage_display.php");

if (!$plugin['ReportCard2008']) {
	intranet_closedb();
	header("Location:/index.php");
}

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
$lreportcard = new libreportcard();

if (!$lreportcard->hasAccessRight()) {
	intranet_closedb();
	header("Location:/index.php");
}

$SettingCategory = "Storage&Display";

$storDispSettings["SubjectScore"] = $subjectScore;
$storDispSettings["SubjectTotal"] = $subjectTotal;
$storDispSettings["GrandAverage"] = $grandAverage;
$storDispSettings["GrandTotal"] = $grandTotal;

$temp = $lreportcard->LOAD_SETTING($SettingCategory, "", $returnDBValue=1);

$success = $lreportcard->UPDATE_SETTING($SettingCategory, $storDispSettings);

if(sizeof($temp) != sizeof($storDispSettings)) {
	$success = $lreportcard->INSERT_SETTING($SettingCategory, $storDispSettings);
}

intranet_closedb();

$Result = ($success) ? "update" : "update_failed";
header("Location:mark_storage_display.php?Result=$Result");
?>