<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if ($plugin['ReportCard2008'])
{
	include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
	if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") {
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
		$lreportcard = new libreportcardcustom();
	} else {
		$lreportcard = new libreportcard();
	}
	$CurrentPage = "Settings_BasicSettings";
	$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
	
	if ($lreportcard->hasAccessRight()) {
		
		# Load calculation settings
		$calSettings = $lreportcard->LOAD_SETTING("Calculation");
		
		############## Interface Start ##############
		$linterface = new interface_html();

		# tag information
		$TAGS_OBJ[] = array($eReportCard['StyleSettings'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/settings/basic_settings/highlight.php", 0);
		$TAGS_OBJ[] = array($eReportCard['MarkStorageAndDisplay'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/settings/basic_settings/mark_storage_display.php", 0);
		$TAGS_OBJ[] = array($eReportCard['AbsentAndExemptSettings'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/settings/basic_settings/absent_exempt.php", 0);
		$TAGS_OBJ[] = array($eReportCard['CalculationSettings'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/settings/basic_settings/calculation.php", 1);
		$TAGS_OBJ[] = array($eReportCard['AccessSettings'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/settings/basic_settings/access_setting.php", 0);
        if($eRCTemplateSetting['OtherInfo']['CustUploadType_Post']) {
            $TAGS_OBJ[] = array($eReportCard['PostTableHeader'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/settings/basic_settings/post_table_header.php", 0);
        }
		
		# page navigation (leave the array empty if no need)
		$PAGE_NAVIGATION[] = array();

		$linterface->LAYOUT_START();
		
		# Check if there is compulsory calculation settings
		include($PATH_WRT_ROOT."includes/eRCConfig.php");
		$disableMethod1 = "";
		$disableMethod2 = "";
		if ($eRCTemplateSetting['CalculationMethod'] == 1)
		{
			$disableMethod2 = "disabled";
		}
		else if ($eRCTemplateSetting['CalculationMethod'] == 2)
		{
			$disableMethod1 = "disabled";
		}
?>
<script type="text/JavaScript" language="JavaScript">
	function changePicClass(period) {
		var order1 = document.getElementById(period + '1').checked;
		var order2 = document.getElementById(period + '2').checked;
		if (order1) {
			document.getElementById(period + 'Vert').className = 'uncheckpic';
			document.getElementById(period + 'Hort').className = 'checkpic';
		}
		if (order2) {
			document.getElementById(period + 'Vert').className = 'checkpic';
			document.getElementById(period + 'Hort').className = 'uncheckpic';
		}
	}
	
	function resetForm() {
		document.form1.reset();
		changePicClass('orderTerm');
		changePicClass('orderWhole');
		return true;
	}
</script>
<br />
<form name="form1" action="calculation_update.php" method="post">
	<table width="95%" align="center" border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td>&nbsp;</td>
			<td align="right"> <?=$linterface->GET_SYS_MSG($Result);?></td>
		</tr>
	</table>
	<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td>
				<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
					<tr>
						<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
							<?= $eReportCard['CalculationMethod'] ?>
						</td>
						<td width="75%" class='tabletext'>
							<input type="checkbox" name="useWeightedMark" id="useWeightedMark" value="1"<?=($calSettings["UseWeightedMark"]=="1")?" checked='true'":""?> />
							<label for="useWeightedMark"><?=$eReportCard['ConsolidateMark']?></label><br />
							<input type="checkbox" name="adjustToFullMarkFirst" id="adjustToFullMarkFirst" value="1"<?=($calSettings["AdjustToFullMarkFirst"]=="1")?" checked='true'":""?> />
							<label for="adjustToFullMarkFirst"><?=$eReportCard['AdjustFullMark']?></label>
						</td>
					</tr>
					<tr>
						<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
							<?= $eReportCard['CalculationOrder'] ?>
						</td>
						<td width="75%" class='tabletext'>
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td class="tablerow2 tabletext" height="25" colspan="5">
										<strong><?=$eReportCard['Term']?></strong>
									</td>
								</tr>
								<tr>
									<td valign="top" class='tabletext'>
										<input onclick="changePicClass('orderTerm');" type="radio" name="calculationOrderTerm" value="1" id="orderTerm1"<?=($calSettings["OrderTerm"]=="1")?" checked='true'":""?> <?=$disableMethod1?> />
									</td>
									<td class='tabletext'>
										<label for="orderTerm1">
											<?=$eReportCard['TermRightDown']?><br />
										</label>
										<img class="<?=($calSettings["OrderTerm"]=="1")?"checkpic":"uncheckpic"?>" id="orderTermHort" src="<?=$PATH_WRT_ROOT."images/{$LAYOUT_SKIN}/ereportcard/cal_hort_term.gif"?>" alt="Horizontal" />
									</td>
									<td width="20">&nbsp;</td>
									<td valign="top" class='tabletext'>
										<input onclick="changePicClass('orderTerm');" type="radio" name="calculationOrderTerm" value="2" id="orderTerm2"<?=($calSettings["OrderTerm"]=="2")?" checked='true'":""?> <?=$disableMethod2?> />
									</td>
									<td class='tabletext'>
										<label for="orderTerm2">
											<?=$eReportCard['TermDownRight']?><br />
										</label>
										<img class="<?=($calSettings["OrderTerm"]=="2")?"checkpic":"uncheckpic"?>" id="orderTermVert" src="<?=$PATH_WRT_ROOT."images/{$LAYOUT_SKIN}/ereportcard/cal_vert_term.gif"?>" alt="Vertical" />
									</td>
								</tr>
								<tr>
									<td colspan="5">&nbsp;</td>
								</tr>
								<tr>
									<td class="tablerow2 tabletext" height="25" colspan="5">
										<strong><?=$eReportCard['WholeYear']?></strong>
									</td>
								</tr>
								<tr>
									<td valign="top" class='tabletext'>
										<input onclick="changePicClass('orderWhole');" type="radio" name="calculationOrderWhole" value="1" id="orderWhole1"<?=($calSettings["OrderFullYear"]=="1")?" checked='true'":""?> <?=$disableMethod1?> />
									</td>
									<td class='tabletext'>
										<label for="orderWhole1">
											<?=$eReportCard['YearRightDown']?><br />
										</label>
										<img class="<?=($calSettings["OrderFullYear"]=="1")?"checkpic":"uncheckpic"?>" id="orderWholeHort" src="<?=$PATH_WRT_ROOT."images/{$LAYOUT_SKIN}/ereportcard/cal_hort_whole.gif"?>" alt="Horizontal" />
									</td>
									<td width="20">&nbsp;</td>
									<td valign="top" class='tabletext'>
										<input onclick="changePicClass('orderWhole');" type="radio" name="calculationOrderWhole" value="2" id="orderWhole2"<?=($calSettings["OrderFullYear"]=="2")?" checked='true'":""?> <?=$disableMethod2?> />
									</td>
									<td class='tabletext'>
										<label for="orderWhole2">
											<?=$eReportCard['YearDownRight']?><br />
										</label>
										<img class="<?=($calSettings["OrderFullYear"]=="2")?"checkpic":"uncheckpic"?>" id="orderWholeVert" src="<?=$PATH_WRT_ROOT."images/{$LAYOUT_SKIN}/ereportcard/cal_vert_whole.gif"?>" alt="Vertical" />
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td>
				<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
					<tr>
						<td class="dotline">
							<img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1">
						</td>
					</tr>
					<tr>
						<td align="center">
							<?= $linterface->GET_ACTION_BTN($button_save, "submit", "")?>&nbsp;
							<?= $linterface->GET_ACTION_BTN($button_reset, "button", "resetForm()")?>&nbsp;
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</form>
<?
  	$linterface->LAYOUT_STOP();
  } else {
?>
You have no priviledge to access this page.
<?
	}
} else {
?>
You have no priviledge to access this page.
<?
}
?>
