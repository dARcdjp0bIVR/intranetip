<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");

intranet_auth();
intranet_opendb();

$lreportcard = new libreportcard();

# Declare variable value
$SettingCategory	= "Absent&Exempt";

# Generate Setting value for database
$setting = array();
$setting[Absent] = $Absent;
$setting[Exempt] = $Exempt;

$temp = $lreportcard->LOAD_SETTING($SettingCategory, "", $returnDBValue=1);
if(sizeof($temp))
 	$result = $lreportcard->UPDATE_SETTING($SettingCategory, $setting);
else
	$result = $lreportcard->INSERT_SETTING($SettingCategory, $setting);

if($result)
	$msg = "update";
else
	$msg = "update_failed";
	
intranet_closedb();	

header("Location: absent_exempt.php?msg=$msg");

?>