<?php
/********************************************************
 * 	20151218 Bill:	[2015-1111-1214-16164]
 * 		- Display Subject Code when $sys_custom['Subject']['DisplaySubjectCode'] = true
 * 	20100113 Marcus:
 * 		- Re-enable scrollable table 
 * 		- use jQuery to hide/show layer, instead of MM_showhidelayer
 * ******************************************************/

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
//include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

if ($plugin['ReportCard2008'])
{
	if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
	{
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
		$lreportcard = new libreportcardcustom();
	} else {
		$lreportcard = new libreportcard();
	}
	$CurrentPage = "Settings_SubjectsAndForms";
	$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
    if ($lreportcard->hasAccessRight())
    {
        $linterface = new interface_html();
      
        //$lclass = new libclass();
		############################################################################################################
		
		# Get class level of school
		$FormArr = $lreportcard->GET_ALL_FORMS();
		
       
		# Preparation
		$colspan = 6;
		$UpdatePage = "subject_settings_update.php";
		
		// Class level
		$fx = "";
		$CheckSubjectArr = array();
		for($i=0 ; $i<count($FormArr) ; $i++){
			// Get whether each subject is checked for each ClassLevel(Form) or not
			$CheckSubjectArr[$FormArr[$i]['ClassLevelID']] = $lreportcard->CHECK_FROM_SUBJECT_GRADING($FormArr[$i]['ClassLevelID']);
			
			/*$fx .= '<th id="td_sort_'.$FormArr[$i]['ClassLevelID'].'" align="center" class="tabletop tabletopnolink" width="'.round(70/count($FormArr), 2).'%">
					  <table border="0" cellspacing="0" cellpadding="2">
						<tr>
						  <td colspan="2" class="tabletopnolink" align="center">'.$FormArr[$i]['LevelName'].'</td>
						</tr>
						<tr>
						  <td align="right"><table width="100%" border="0" cellspacing="0" cellpadding="1">
							<tr>
							  <td><a href="javascript:jSIFT_FORM(\''.$FormArr[$i]['ClassLevelID'].'\')"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/ereportcard/icon_sift.gif" alt="'.$eReportCard['Sift'].' '.$FormArr[$i]['LevelName'].'" width="12" height="12" border="0" align="absmiddle"></a>
							    <input type="hidden" name="siftFlag_'.$FormArr[$i]['ClassLevelID'].'" id="siftFlag_'.$FormArr[$i]['ClassLevelID'].'" value="0"/></td>
							  </td>
							  <td><table  border="0" cellspacing="0" cellpadding="0"onMouseOver="MM_showHideLayers(\'copyfrom_'.$FormArr[$i]['ClassLevelID'].'\',\'\',\'show\',this)" onMouseOut="MM_showHideLayers(\'copyfrom_'.$FormArr[$i]['ClassLevelID'].'\',\'\',\'hide\')">
								<tr>
								  <td><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_copy.gif" width="12" height="12" border="0" align="absmiddle"></td>
								  <td valign="top">';
			# Copy Class Level Layer
			$fx .= '<div id="copyfrom_'.$FormArr[$i]['ClassLevelID'].'" style="position:absolute; width:100px; z-index:2; visibility: hidden;">
					  <table width="100%" border="0" cellpadding="0" cellspacing="0">
		    			<tr>
		    			  <td height="19"><table width="100%" border="0" cellspacing="0" cellpadding="0">
		    				<tr>
		    				  <td width="5" height="19"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_01.gif" width="5" height="19"></td>
		    				  <td height="19" valign="middle" background="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_02.gif">&nbsp;</td>
		    				  <td width="19" height="19"><a href="javascript:void(0)" onClick="MM_showHideLayers(\'copyfrom_'.$FormArr[$i]['ClassLevelID'].'\',\'\',\'hide\')"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_close_off.gif" name="pre_close" width="19" height="19" border="0" id="pre_close" onMouseOver="MM_swapImage(\'pre_close\',\'\',\''.$image_path.'/'.$LAYOUT_SKIN.'/can_board_close_on.gif\',1)" onMouseOut="MM_swapImgRestore()"></a></td>
		    				</tr>
		    			  </table></td>
		    			</tr>
		    			<tr>
		    			  <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
		    				<tr>
		    				  <td width="5" background="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_04.gif"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_04.gif" width="5" height="19"></td>
							  <td bgcolor="#FFFFF7"><table width="100%" border="0" cellspacing="0" cellpadding="4">
								<tr class="tableorangetop">
								  <td class="tabletopnolink">'.$eReportCard['CopyFrom'].'</td>
								</tr>';
			for($j=0 ; $j<count($FormArr) ; $j++){
				if($FormArr[$j]['ClassLevelID'] != $FormArr[$i]['ClassLevelID'])
					$fx .= '<tr class="tableorangerow1"><td><a href="javascript:jCOPY_FORM_SETTING(\''.$FormArr[$j]['ClassLevelID'].'\', \''.$FormArr[$i]['ClassLevelID'].'\')" class="tableorangelink">'.$FormArr[$j]['LevelName'].'</a></td></tr>';
			}
			$fx .= '	  	  </table></td>
							  <td width="6" background="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_06.gif"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_06.gif" width="6" height="6"></td>
							</tr>
							<tr>
							  <td width="5" height="6"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_07.gif" width="5" height="6"></td>
	            			  <td height="6" background="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_08.gif"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_08.gif" width="5" height="6"></td>
	            			  <td width="6" height="6"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_09.gif" width="6" height="6"></td>
	            			</tr>
						  </table></td>
						</tr>
		    		  </table>
					</div>';
			# End of Copy Class Level Layer		
			$fx .= '			  </td>
								</tr>
							  </table></td>
							</tr>
						  </table></td>
						</tr>
						<tr>
						  <td align="center"><input type="checkbox" name="checkAll_'.$FormArr[$i]['ClassLevelID'].'" id="checkAll_'.$FormArr[$i]['ClassLevelID'].'" value="0" onclick="jCHECK_ALL('.$FormArr[$i]['ClassLevelID'].')"/></td>
						</tr>
					  </table>
					</th>';*/
					# Copy Class Level Layer
					$fx1 .= '<div id="copyfrom_'.$FormArr[$i]['ClassLevelID'].'" style="position:absolute; width:100px; z-index:2; visibility: hidden;" >
							  <table width="100%" border="0" cellpadding="0" cellspacing="0">
				    			<tr>
				    			  <td height="19"><table width="100%" border="0" cellspacing="0" cellpadding="0">
				    				<tr>
				    				  <td width="5" height="19"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_01.gif" width="5" height="19"></td>
				    				  <td height="19" valign="middle" background="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_02.gif">&nbsp;</td>
				    				  <td width="19" height="19"><a href="javascript:void(0)" onClick="jHideAllLayers()"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_close_off.gif" name="pre_close" width="19" height="19" border="0" id="pre_close" onMouseOver="MM_swapImage(\'pre_close\',\'\',\''.$image_path.'/'.$LAYOUT_SKIN.'/can_board_close_on.gif\',1)" onMouseOut="MM_swapImgRestore()"></a></td>
				    				</tr>
				    			  </table></td>
				    			</tr>
				    			<tr>
				    			  <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
				    				<tr>
				    				  <td width="5" background="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_04.gif"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_04.gif" width="5" height="19"></td>
									  <td bgcolor="#FFFFF7"><table width="100%" border="0" cellspacing="0" cellpadding="4">
										<tr class="tableorangetop">
										  <td class="tabletopnolink">'.$eReportCard['CopyFrom'].'</td>
										</tr>';
					for($j=0 ; $j<count($FormArr) ; $j++){
						if($FormArr[$j]['ClassLevelID'] != $FormArr[$i]['ClassLevelID'])
							$fx1 .= '<tr class="tableorangerow1"><td><a href="javascript:jCOPY_FORM_SETTING(\''.$FormArr[$j]['ClassLevelID'].'\', \''.$FormArr[$i]['ClassLevelID'].'\'); jHideAllLayers();" class="tableorangelink">'.$FormArr[$j]['LevelName'].'</a></td></tr>';
					}
					$fx1 .= '	  	  </table></td>
									  <td width="6" background="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_06.gif"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_06.gif" width="6" height="6"></td>
									</tr>
									<tr>
									  <td width="5" height="6"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_07.gif" width="5" height="6"></td>
			            			  <td height="6" background="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_08.gif"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_08.gif" width="5" height="6"></td>
			            			  <td width="6" height="6"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_09.gif" width="6" height="6"></td>
			            			</tr>
								  </table></td>
								</tr>
				    		  </table>
							</div>';
					
					$fx.='<th id="td_sort_'.$FormArr[$i]['ClassLevelID'].'" align="center" class="tabletop tabletopnolink" width="'.round(70/count($FormArr), 2).'%">';
						$fx.='<div class="tabletopnolink">';
							$fx.=$FormArr[$i]['LevelName'];
						$fx.='</div>';
						$fx.='<div>';
							//$fx.='<a href="javascript:void(0)"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_copy.gif" width="12" height="12" border="0" align="absmiddle" onclick="MM_showHideLayers(\'copyfrom_'.$FormArr[$i]['ClassLevelID'].'\',\'\',\'show\',this)"></a>';
							$fx.='<a href="javascript:void(0)"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_copy.gif" width="12" height="12" border="0" align="absmiddle" class="copy_img" layer="copyfrom_'.$FormArr[$i]['ClassLevelID'].'"></a>';
							$fx.='<a href="javascript:jSIFT_FORM(\''.$FormArr[$i]['ClassLevelID'].'\')"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/ereportcard/icon_sift.gif" alt="'.$eReportCard['Sift'].' '.$FormArr[$i]['LevelName'].'" width="12" height="12" border="0" align="absmiddle"></a>';
						    $fx.='<input type="hidden" name="siftFlag_'.$FormArr[$i]['ClassLevelID'].'" id="siftFlag_'.$FormArr[$i]['ClassLevelID'].'" value="0"/>';
						$fx.='</div>';
					$fx.='</th>';
					
		}
		
		
		// Subjects
		$SubjectArray = $lreportcard->GET_ALL_SUBJECTS(1);
		
		$rx = "";
		$count = 0;
		if(!empty($SubjectArray))
		{
			// [2015-1111-1214-16164] Get SubjectID and Subject Code Mapping
			if($eRCTemplateSetting['Settings']['DisplaySubjectCode']){
				$CODEID_Mapping = $lreportcard->GET_SUBJECTS_CODEID_MAP();
			}
			foreach($SubjectArray as $SubjectID => $Data)
			{
				if(is_array($Data))
				{
					$SubjectIDArr = array();
					$SubjectIDArr[] = $SubjectID;
					$ParentSubID = $SubjectID;
					$DisplayOrder = $lreportcard->GET_SUBJECTS_DISPLAY_ORDER($SubjectIDArr);
					
					foreach($Data as $CmpSubjectID => $SubjectName)
					{							
						$css = ($CmpSubjectID > 0) ? 'tablelist_sub_subject' : 'tablelist_subject';
						$SubjectID = ($CmpSubjectID==0) ? $SubjectID : $CmpSubjectID;
						$is_CmpSubject = ($CmpSubjectID==0) ? 0 : 1;
						// [2015-1111-1214-16164] Display Subject Code - $eRCTemplateSetting['Settings']['DisplaySubjectCode'] = true & Main Subject
						$SubjectCode = ($eRCTemplateSetting['Settings']['DisplaySubjectCode'] && !$is_CmpSubject)? "<br/>(".$CODEID_Mapping[$SubjectID].")" : "";
						
						$rx .= '<tr id="tr_subject_'.$SubjectID.'" style="display:table-row">';
						$rx .= '<td width="30%" class="'.$css.'">'.(($CmpSubjectID > 0) ? '<img src="'.$image_path.'/'.$LAYOUT_SKIN.'/10x10.gif" width="20" height="10">' : '').$SubjectName.$SubjectCode;
						$rx .= '<input type="hidden" name="order_'.$SubjectID.'" value="'.$DisplayOrder[$SubjectID].'"></td>';
						
						for($i=0 ; $i<count($FormArr) ; $i++){
							$subj_id = $SubjectID;
							$form_id = $FormArr[$i]['ClassLevelID'];
							
							$id = $subj_id.'_'.$form_id;
							$is_check = $CheckSubjectArr[$form_id][$subj_id];
							$is_disable = ($CheckSubjectArr[$form_id][$ParentSubID]) ? 0 : 1;
							$row_css = ($count % 2 == 0) ? 'retablerow1' : 'retablerow2';	
							$row_css = ($is_check) ? 'retablechose' : $row_css;
												
							$rx .= '<td id="td_check_'.$id.'" align="center" class="'.$row_css.'" width="'.floor(70/count($FormArr)).'%">';
							if($is_CmpSubject)
								$rx .= '<input type="hidden" id="cmpSubject_'.$ParentSubID.'_'.$form_id.'" name="cmpSubject_'.$ParentSubID.'_'.$form_id.'[]" value="'.$SubjectID.'"/>';
//							$rx .= '<input type="checkbox" name="checkBox_'.$id.'" id="checkBox_'.$id.'" value="'.$is_check.'" onclick="jCHECK_CMPSUBJECT_CHECKBOX(\''.$ParentSubID.'\', \''.$form_id.'\');jCHECK_BOX(\''.$subj_id.'\', \''.$form_id.'\')" '.(($is_check) ? "checked" : "").' '.(($is_CmpSubject && $is_disable) ? 'disabled' : '').'/></td>';
							$rx .= '<input type="checkbox" name="checkBox['.$subj_id.']['.$form_id.']" id="checkBox_'.$id.'" value="'.$is_check.'" onclick="jCHECK_CMPSUBJECT_CHECKBOX(\''.$ParentSubID.'\', \''.$form_id.'\');jCHECK_BOX(\''.$subj_id.'\', \''.$form_id.'\')" '.(($is_check) ? "checked" : "").' '.(($is_CmpSubject && $is_disable) ? 'disabled' : '').'/></td>';
								
						}
						
						$rx .= "</tr>";
						$flag = 1;
						$count++;
					}
				}
			}
		}
		else
		{
			$rx .= "<tr><td colspan=\"".(count($FormArr)+1)."\" align=\"center\" class=\"tabletext\">".$i_no_record_exists_msg."</td></tr>";
		}
		
        ############################################################################################################
        
        # use for scrollable table only
        //$header_onload_js .= "makeScrollableTable('subject_form_table',true,'320');";
        
		# tag information
		$link0 = 1;
		$link1 = 0;
		$link2 = 0;
		$link3 = 0;
		$TAGS_OBJ[] = array($eReportCard['SubjectSettings'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/settings/subjects_and_form/subject_settings.php", $link0);
		$TAGS_OBJ[] = array($eReportCard['FormSettings'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/settings/subjects_and_form/form_settings.php", $link1);
		if($eRCTemplateSetting['Settings']['FormSubjectTypeSettings']){
		    $TAGS_OBJ[] = array($eReportCard['SubjectType']['Type'],$PATH_WRT_ROOT . "home/eAdmin/StudentMgmt/eReportCard/settings/subjects_and_form/credit_settings.php",$link2);
		}
        if($eRCTemplateSetting['Settings']['GradeDescriptor']){
            $TAGS_OBJ[] = array($eReportCard['GradeSettings'], $PATH_WRT_ROOT . "home/eAdmin/StudentMgmt/eReportCard/settings/subjects_and_form/grade_settings.php", $link3);
        }
		
		$PAGE_NAVIGATION[] = array($button_edit);
		
		//echo "<div id='temp' style='display:none'></div>";
		$linterface->LAYOUT_START();
		echo '<link href="'.$PATH_WRT_ROOT.'templates/2007a/css/ereportcard.css" rel="stylesheet" type="text/css" />';
?>

<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>/templates/webtoolkit.scrollabletable.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>/templates/webtoolkit.jscrollable.js"></script>


<script language="javascript">
<?php
  $str = "var subjectArr = new Array(";
  if(count($SubjectArray) > 0){
    $i = 0;
	foreach($SubjectArray as $SubjectID => $Data){
		if(is_array($Data)){
			foreach($Data as $CmpSubjectID => $SubjectName){
				$SubjectID = ($CmpSubjectID==0) ? $SubjectID : $CmpSubjectID;
				if($i > 0)
					$str .= ", ";
				$str .= "'".$SubjectID."'";
				$i++;
			}
		}
	}
  }
  $str .= ");";
  echo $str."\n";
	
  $formStr = "var formArr = new Array(";
  if(count($FormArr) > 0){
	  for($i=0 ; $i<count($FormArr) ; $i++){
		  if($i > 0)
		      $formStr .= ", ";
		  $formStr .= "'".$FormArr[$i]['ClassLevelID']."'";
	  }
  }
  $formStr .= ");";
  echo $formStr."\n";
?>
function jCHECK_CMPSUBJECT_CHECKBOX(p_id, f_id){
	// p: parent, f: form, ck:check
	var p_index = p_id + "_" + f_id;
	var obj = document.getElementsByName("cmpSubject_" + p_index + "[]");
	var p_ck_obj = document.getElementById("checkBox_" + p_index);
	
	if(obj.length > 0){
		for(var i=0 ; i<obj.length ; i++){
			var index = obj[i].value + "_" + f_id;
			var ck_obj = document.getElementById("checkBox_" + index);
			
			if(p_ck_obj.checked == true){
				ck_obj.disabled = false;
			} else {
				ck_obj.checked = false;
				jCHECK_BOX(obj[i].value, f_id);
				ck_obj.disabled = true;
			}
		}
	}
}

function jCHECK_SUBJECT_CHECKED(f_id, msg){
	var num = subjectArr.length;
	var flag = 0;
	
	for(var i=0 ; i<num ; i++){
		var index = subjectArr[i] + "_" + f_id;
		if(document.getElementById("checkBox_" + index).checked){
			flag = 1;
			continue;
		}
	}
	
	if(flag == 1){
		return true;
	} else {
		alert(msg);
		return false;
	}
}

function jCHECK_ALL(f_id){
	var num = subjectArr.length;
	var val = "";
	
	if(document.getElementById("checkAll_" + f_id).checked == true)
		val = "checked";
	
	for(var i=0 ; i<num ; i++){
		var index = subjectArr[i] + "_" + f_id;
		document.getElementById("checkBox_" + index).checked = val;
		jCHECK_BOX(subjectArr[i], f_id);
		jCHECK_CMPSUBJECT_CHECKBOX(subjectArr[i], f_id);
	}
}

function jCOPY_FORM_SETTING(fr_id, to_id){
	// fr_id : from ClassLevelID, 	to_id : to ClassLevelID
	var num = subjectArr.length;
	
	for(var i=0 ; i<num ; i++){
		var fr_index = subjectArr[i] + "_" + fr_id;
		var to_index = subjectArr[i] + "_" + to_id;
		var fr_obj = document.getElementById("checkBox_" + fr_index);
		var to_obj = document.getElementById("checkBox_" + to_index);
		
		to_obj.checked = fr_obj.checked;
		jCHECK_BOX(subjectArr[i], to_id);
		jCHECK_CMPSUBJECT_CHECKBOX(subjectArr[i], to_id);
	}
}

function jCHECK_BOX(s_id, f_id){
	var index = s_id + "_" + f_id;
	var obj = document.getElementById("checkBox_" + index);
	var td_obj = document.getElementById("td_check_" + index);
	var currentCssClass = jGET_ORIGIN_CSS_CLASS(s_id);
	
	if(obj.checked == true){
		td_obj.className = "retablechose";
		obj.value = 1;
	} else {
		td_obj.className = "retablerow" + currentCssClass;
		obj.value = 0;
	}
		
}


function jGET_ORIGIN_CSS_CLASS(s_id){
	var num = subjectArr.length;
	var val = 0;
	
	for(var i=0 ; i<num ; i++){
		if(subjectArr[i] == s_id){
			val = i;
			i = num;
		}
	}
	
	if(val % 2 == 1)
		return 2;
	else 
		return 1;
}

function jENABLE_FORM_ELEMENTS(){
	var s_num = subjectArr.length;
	var f_num = formArr.length;
	
	// Enable all checkboxes including Subjects And CmpSubject
	for(var i=0 ; i<f_num ; i++){
		for(var j=0 ; j<s_num ; j++){
			var index = subjectArr[j] + "_" + formArr[i];
			var ck_obj = document.getElementById("checkBox_" + index);
			
			if(ck_obj.disabled == true)
				ck_obj.disabled = false;
		}
	}
}

function jSIFT_FORM(f_id){
	var s_num = subjectArr.length;
	var f_num = formArr.length;
	var is_sifted = document.getElementById("siftFlag_" + f_id);
	var msg = "<?=$eReportCard['jsAlertNoSubjectSelect']?>";
	
	if (is_sifted.value == 0) {
		if(!jCHECK_SUBJECT_CHECKED(f_id, msg))
			return ;
	}
	
	for(var i=0 ; i<f_num ; i++)
		document.getElementById("td_sort_" + formArr[i]).className = "tabletop tabletopnolink";
	
	for(var i=0 ; i<s_num ; i++){
		var index =  subjectArr[i] + "_" + f_id;
		var a = document.getElementById("tr_subject_" + subjectArr[i]);
		
		checkObj = document.getElementById("checkBox_" + index);
		
		if(is_sifted.value == 0 && checkObj.checked == false){
			displayTable("tr_subject_" + subjectArr[i], "none");
		} else {
			try {	// Firefox
				displayTable("tr_subject_" + subjectArr[i], "table-row");
			} catch (e){	// IE
				displayTable("tr_subject_" + subjectArr[i], "block");
			}
		}
	}
	
	if(is_sifted.value == 0){
		is_sifted.value = 1;
		document.getElementById("td_sort_" + f_id).className = "retabletop_sort tabletopnolink";
	}
	else {
		is_sifted.value = 0;
	}
}

function jCHECK_FORM(){
	return true;
}

function jSUBMIT_FORM(){
	if(!jCHECK_FORM())
		return;
	
	jENABLE_FORM_ELEMENTS();
		
	var obj = document.FormMain;
	obj.submit();
}

function jRESET_FORM(){
	var obj = document.FormMain;
	obj.reset();
	
	var s_num = subjectArr.length;
	var f_num = formArr.length;
	
	// Reset the Class of TD after reset 
	for(var i=0 ; i<f_num ; i++){
		for(var j=0 ; j<s_num ; j++){
			var currentCssClass = jGET_ORIGIN_CSS_CLASS(subjectArr[j]);
			var index = subjectArr[j] + "_" + formArr[i];
			var checkObj = document.getElementById("checkBox_" + index);
			var td_obj = document.getElementById("td_check_" + index);
			if(checkObj.checked == false && td_obj.className == "retablechose"){
				td_obj.className = "retablerow" + currentCssClass;
				jCHECK_CMPSUBJECT_CHECKBOX(subjectArr[j], formArr[i]);
			}
		}
	}
}

/*
function MM_showHideLayers() { //v9.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  jHideAllLayers();
  for (i=0; i<(args.length-2); i+=3) 
  with (document) if (getElementById && ((obj=getElementById(args[i]))!=null)) { v=args[i+2];
	if (args[3]) { p=$(args[3]).position();  alert(p.top+","+p.left); $(obj).css("left",p.left+5); $(obj).css("top",p.top+5);}
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v=='hide')?'hidden':v; }
	obj.visibility=v; }
    
}
*/

function jHideAllLayers()
{
	jQuery.each($("div[id^='copyfrom_']"),function(idx,obj){
		$(obj).css("visibility","hidden");
	});
}
jQuery(document).ready(function() {
	jQuery('#subject_form_table').Scrollable(500);
	$(".copy_img").click(function (e){
		jHideAllLayers()
		var layerid = $(this).attr("layer");
		$("#"+layerid).css("left",e.pageX+5);
		$("#"+layerid).css("top",e.pageY+5);
		$("#"+layerid).css("visibility","visible");
	})
});

</script>

<br/>
<form name="FormMain" method="POST" action="<?=$UpdatePage;?>">
<table width="98%" border="0" cellpadding="4" cellspacing="0">
  <tr>
    <td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
  </tr>
  <tr>
    <td align="right" colspan="<?=$colspan?>"><?= $linterface->GET_SYS_MSG($Result); ?></td>
  </tr>
  <tr> 
	<td align="center">
	  <table width="100%" border="0" cellspacing="0" cellpadding="2">
	    <tr>
		  <td>
	    	<table id="subject_form_table" width="98.5%" border="0" cellspacing="0" cellpadding="4" align="center">
	    	  <thead>
			    <tr>
				  <td width="30%" align="left" style="background-color:#fff">&nbsp;</td>
				  <?=$fx?>
			    </tr>
			  </thead>
			  <tbody>
			    <?=$rx?>
			  </tbody>
			  <tfoot>
			    <tr><td colspan="<?=count($FormArr)+1?>"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif"></td></tr>
			  </tfoot>
			</table>
		  </td>
        </tr>        
	  </table>
	  <?=$fx1?>
	  <br />
      <table width="90%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td height="1" class="dotline"><img src="<?=$image_path.'/'.$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
      	</tr>
      	<tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="2">
              <tr>
                <td>&nbsp;</td>
              	<td align="center" valign="bottom">
              	  <input name="Save" type="button" class="formbutton" onMouseOver="this.className='formbuttonon'" onMouseOut="this.className='formbutton'" onclick="jSUBMIT_FORM()" value="<?=$button_save?>">
                  <input name="Reset" type="button" class="formbutton" onMouseOver="this.className='formbuttonon'" onMouseOut="this.className='formbutton'" onclick="jRESET_FORM()" value="<?=$button_reset?>">
                </td>
              </tr>
        	</table>
          </td>
        </tr>
      </table>
	</td>
  </tr>
</table>
<br/>
<input type="hidden" name="flag" id="flag" value="1">

</form>

<?

        $linterface->LAYOUT_STOP();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>