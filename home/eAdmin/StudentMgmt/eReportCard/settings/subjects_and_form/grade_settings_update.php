<?php

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
//include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");

intranet_auth();
intranet_opendb();

/* Temp Library*/
include_once($PATH_WRT_ROOT."includes/libreportcard2008j.php");
$lreportcard = new libreportcard2008j();
//$lreportcard = new libreportcard();

//$textChinese = $_POST['TextChinese'];
$textEnglish = $_POST['TextEnglish'];
//$chKeys = array_keys($textChinese);
$enKeys = array_keys($textEnglish);
//$keys = array_merge($chKeys, $enKeys);
//$keys = array_unique($keys);
$keys = array_unique($enKeys);

$dataAry = array();
foreach($keys as $key){
    $dataAry[] = array(
        "Grade" => $key,
        "Descriptor_ch" => intranet_htmlspecialchars($textChinese[$key]),
        "Descriptor_en" => intranet_htmlspecialchars($textEnglish[$key])
    );
}
$result = $lreportcard->editGradeDescriptor($dataAry);

if(!in_array(false, $result))
	$msg = "update";
else 
	$msg = "update_failed";

intranet_closedb();

header("Location: grade_settings.php?Result=$msg");
?>