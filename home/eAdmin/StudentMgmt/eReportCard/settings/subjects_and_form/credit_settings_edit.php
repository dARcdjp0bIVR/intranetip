<?php
// ############ MODIFICATION ### LOG ####################
//
// File using : Philips
// Last Modified : 20160625
// Created : 20180625
//
//
// ############ MODIFICATION ### LOG ####################
//
// Date: 20180625 (Philips)
// Allow to check radio by clicking label
// Added Apply All function
//
// Date: 20180615 (Philips)
// Created File
//
// ############ MODIFICATION ### LOG ####################

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
// include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/libreportcard2008.php");
include_once ($PATH_WRT_ROOT . "includes/libclass.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable2007a.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if ($plugin['ReportCard2008']) {
    /* Temp Library */
    include_once ($PATH_WRT_ROOT . "includes/libreportcard2008j.php");
    $lreportcard = new libreportcard2008j();
    if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") {
        include_once ($PATH_WRT_ROOT . "includes/reportcard_custom/" . $ReportCardCustomSchoolName . ".php");
    }
    // $lreportcard = new libreportcard();
    
    $CurrentPage = "Settings_SubjectsAndForms";
    $MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
    
    // tag information
    $link0 = 0;
    $link1 = 0;
    $link2 = 1;
    $link3 = 0;
    $TAGS_OBJ[] = array(
        $eReportCard['SubjectSettings'],
        $PATH_WRT_ROOT . "home/eAdmin/StudentMgmt/eReportCard/settings/subjects_and_form/subject_settings.php",
        $link0
    );
    $TAGS_OBJ[] = array(
        $eReportCard['FormSettings'],
        $PATH_WRT_ROOT . "home/eAdmin/StudentMgmt/eReportCard/settings/subjects_and_form/form_settings.php",
        $link1
    );
    $TAGS_OBJ[] = array(
        $eReportCard['SubjectType']['Type'],
        $PATH_WRT_ROOT . "home/eAdmin/StudentMgmt/eReportCard/settings/subjects_and_form/credit_settings.php",
        $link2
    );
    if($eRCTemplateSetting['Settings']['GradeDescriptor']) {
        $TAGS_OBJ[] = array(
            $eReportCard['GradeSettings'],
            $PATH_WRT_ROOT . "home/eAdmin/StudentMgmt/eReportCard/settings/subjects_and_form/grade_settings.php",
            $link3
        );
    }
    $PAGE_NAVIGATION[] = array(
        $button_edit
    );
    
    if ($lreportcard->hasAccessRight()) {
        $linterface = new interface_html();
        
        // ###########################################################################################################
        
        // Get class level of school
        $FormArr = $lreportcard->GET_ALL_FORMS();
        
        $LevelNameArr = array();
        $LevelIDArr = array();
        if (sizeof($FormArr) > 0) {
            for ($i = 0; $i < sizeof($FormArr); $i ++) {
                $LevelIDArr[] = $FormArr[$i]["ClassLevelID"];
                $LevelNameArr[] = $FormArr[$i]["LevelName"];
            }
        }
        $ClassLevelSelect = getSelectByValueDiffName($LevelIDArr, $LevelNameArr, 'name="ClassLevelID" id="ClassLevelID" onchange="selfSubmit();"', $ClassLevelID, 0, 1);
        
        // Subjects
        $SubjectArray = $lreportcard->GET_ALL_SUBJECTS(0);
        
        // Initialization
        $FormName = '';
        $targetForm = '';
        $prevForm = '';
        $nextForm = '';
        if (isset($ClassLevelID) && $ClassLevelID != "") {
            $targetForm = $ClassLevelID;
            if (count($FormArr) > 0) {
                for ($i = 0; $i < count($FormArr); $i ++) {
                    if ($targetForm == $FormArr[$i]['ClassLevelID']) {
                        if ($i != 0) {
                            $prevForm = $FormArr[$i - 1]['ClassLevelID'];
                        }
                        else {
                            $prevForm = $FormArr[count($FormArr) - 1]['ClassLevelID'];
                        }
                        
                        if ($i != count($FormArr) - 1) {
                            $nextForm = $FormArr[$i + 1]['ClassLevelID'];
                        }
                        else {
                            $nextForm = $FormArr[0]['ClassLevelID'];
                        }
                        
                        $FormName = $FormArr[$i]['LevelName'];
                    }
                }
            }
        } else {
            if (count($FormArr) > 0) {
                $targetForm = $FormArr[0]['ClassLevelID'];
                $prevForm = $FormArr[(count($FormArr) - 1)]['ClassLevelID'];
                $nextForm = $FormArr[1]['ClassLevelID'];
                $FormName = $FormArr[0]['LevelName'];
            }
        }
        
        // Get Report Selection 20100924
        //$ReportSelection = $lreportcard->Get_Report_Selection($targetForm, $ReportID, "ReportID", $ParOnchange = 'selfSubmit();', 0, 0, 0, '', 1);
        
        $CheckSubjectArr = $lreportcard->CHECK_FROM_SUBJECT_GRADING($targetForm);
        
        $count = 0;
        $rx = '';
        
        $SubjectDataArr = array();
        $SubjectDataArr = $lreportcard->GET_FROM_SUBJECT_GRADING_SCHEME($targetForm, 0);
        
        // Get the last sub-subject of each subjects if any & use for css
        $LastCmpSubject = array();
        $subArr = array();
        if (! empty($SubjectDataArr)) {
            foreach ($SubjectDataArr as $SubjectID => $Data) {
                if ($Data['is_cmpSubject'] == 1){
                    $LastCmpSubject[$Data['parentSubjectID']] = $SubjectID;
                } else {
                    $subArr[] = $SubjectID;
                }
            }
        }
        
        $SubjectTypeArr = array(
            array("name" => $eReportCard['SubjectType']['SubjectNormal'], "type" => $eRCTemplateSetting['Settings']['SubjectType']['Core']),
            array("name" => $eReportCard['SubjectType']['SubjectElective'], "type" => $eRCTemplateSetting['Settings']['SubjectType']['Elective']),
            array("name" => $eReportCard['SubjectType']['SubjectCompetitive'],"type" => $eRCTemplateSetting['Settings']['SubjectType']['Competitive'])
        );
        
        $globalTypeSelection = '';
        foreach($SubjectTypeArr as $sta){
            $globalTypeSelection .= "<input type='radio' $checked name='globalSubjectType' id='globalSubjectType' value='$sta[type]' onchange='' /><label for='globalSubjectType[$sta[name]]'>$sta[name]</label>";
        }
        $globalTypeSelection .= '&nbsp;<a href="javascript:applyToAll(\'globalSubjectType\',\'subjectType\');"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_assign.gif" width="12" height="12" border="0" /></a>';
        
        $globalCreditInput = "<input type='number' min='0' name='globalCredit' id='globalCredit' value='0' style='margin-left:16px;'/>";
        $globalCreditInput .= '&nbsp;<a href="javascript:applyAllValue(\'globalCredit\',\'creditInput\');"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_assign.gif" width="12" height="12" border="0" /></a>';
        
        $globalLimitInput = "<input type='number' min='0' name='globalLimit' id='globalLimit' value='0' style='margin-left:16px;'/>";
        $globalLimitInput .= '&nbsp;<a href="javascript:applyAllValue(\'globalLimit\',\'limitInput\');"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_assign.gif" width="12" height="12" border="0" /></a>';
        
        if (! empty($SubjectDataArr)) {
            // [2015-1111-1214-16164] Get SubjectID and Subject Code Mapping
            if ($eRCTemplateSetting['Settings']['DisplaySubjectCode']) {
                $CODEID_Mapping = $lreportcard->GET_SUBJECTS_CODEID_MAP();
            }
            
            $credits = array();
            $creditArr = $lreportcard->Get_Report_Credit($targetForm, $ReportID, $subArr);
            foreach($creditArr as $ca){
                $credits[$ca['SubjectID']] = $ca;
            }
            
            foreach ($SubjectDataArr as $SubjectID => $Data) {
                $hasTypeSettings = $credits[$SubjectID]['Type'] != null;
                
                $is_CmpSubject = ($Data['is_cmpSubject'] == 0) ? 0 : 1;
                // [2015-1111-1214-16164] Display Subject Code - $eRCTemplateSetting['Settings']['DisplaySubjectCode'] = true & Main Subject
                $SubjectCode = ($eRCTemplateSetting['Settings']['DisplaySubjectCode'] && ! $is_CmpSubject) ? "<br/>(" . $CODEID_Mapping[$SubjectID] . ")" : "";
                
                // $row_css = ($count % 2 == 0 || $is_CmpSubject) ? 'retablerow1' : 'retablerow2';
                $head_1_css = ((count($LastCmpSubject) > 0 && $LastCmpSubject[$Data['parentSubjectID']] == $SubjectID) || $Data['is_cmpSubject'] == 0) ? 'tablelist_subject' : 'tablelist_subject_head';
                $head_2_css = ($Data['is_cmpSubject'] == 0) ? 'tablelist_subject' : 'tablelist_sub_subject';
                // $row_css .= ($Data['is_cmpSubject']==0) ? '' : ' retablerow_subsubject_line';
                $row_css = $is_CmpSubject ? 'retablerow1' : 'retablerow2';
                $row_css .= ' retablerow_subsubject_line';
                
                $SubjectTypeSelection = "";
                $ct = ($credits[$SubjectID]['Type']) ? $credits[$SubjectID]['Type'] : $eRCTemplateSetting['Settings']['SubjectType']['Core'];
                foreach($SubjectTypeArr as $sType){
                    $checked = ($ct == $sType['type']) ? 'checked' : '';
                    $SubjectTypeSelection .= $SubjectTypeSelection ? '&nbsp;&nbsp;' : '';
                    $SubjectTypeSelection .= "<input type='radio' $checked name='subjectType[$SubjectID]' class='subjectType_$sType[type]' id='subjectType_$SubjectID' value='$sType[type]' onchange='checkSubjectType(this)' /><label for='subjectType[$SubjectID]'>$sType[name]</label>";
                }
                $ci = ($credits[$SubjectID]) ? $credits[$SubjectID]['Credit'] : 0;
                $li = ($credits[$SubjectID]) ? $credits[$SubjectID]['LowerLimit'] : 0;
                $CreditInput = "<input type='number' min=0 name='subjectCredit[$SubjectID]' id='subjectCredit_$SubjectID' class='creditInput' value='$ci' />";
                $LimitInput = "<input type='number' min=0 name='subjectLimit[$SubjectID]' id='subjectLimit_$SubjectID' class='limitInput' value='$li' />";
                
                // for Main Subject
                if (! $is_CmpSubject) {
                    $rx .= '<tr class="' . $row_css . '">';
                    $rx .= '<td align="left" class="' . $head_1_css . '">&nbsp;</td>';
                    $rx .= '<td class="' . $head_2_css . '">' . $Data['subjectName'] . $SubjectCode . '</td>';
                    $rx .= '<td align="center">'.$SubjectTypeSelection.'</td>';
                    $rx .= '<td align="center">'.$CreditInput.'</td>';
                    $rx .= '<td align="center">'.$LimitInput.'</td>';
                    $rx .= '</tr>';
                }
                
                $flag = 1;
                $count ++;
            }
        } else {
            $rx .= '<tr><td width="15" align="left" class="tablelist_sub_subject">&nbsp;</td>';
            $rx .= '<td colspan="4" align="center" width="100%" class="tabletext tablelist_sub_subject" height="50">'.$Lang['General']['NoRecordAtThisMoment'].'</td>';
            $rx .= '</tr>';
        }
        
        $UpdatePage = 'credit_setting_update.php';
        
        // echo "<div id='top'></div>";
        $linterface->LAYOUT_START();
        
        echo '<link href="' . $PATH_WRT_ROOT . 'templates/2007a/css/ereportcard.css" rel="stylesheet" type="text/css" />';
        ?>
<script type="text/javascript">
$(document).ready(function(){
 var radiogrp = [<?=implode(',', $subArr)?>];
 $('#globalSubjectType').click();
 radiogrp.forEach(function(element){
  var target = $('#subjectType_' + element + ':checked');
  if(target.length < 1){
   checkSubjectType(null, element);
  } else {
   checkSubjectType(target);
  }
 });
 $('label').click(function(){
  $(this).prev().click();
 });
});
function backToPage(){
	$('#form1').attr('action', 'credit_settings.php');
	$('#form1').submit();
}
function selfSubmit(){
	$('#form1').attr('action', 'credit_settings_edit.php');
	$('#form1').submit();
}

function applyToAll(ele ,name){
	var value = $('#' + ele+':checked').val();
	$('.'+name+'_'+value).click();
}
function applyAllValue(ele, name){
	var value = $('#' + ele).val();
	$('.'+name).val(value);
}

function checkSubjectType(ele, id){
 if(ele==null){
	$('#subjectLimit_'+id).attr('disabled', true);
	return;
 }
 var id = $(ele).attr('id').replace('subjectType_', '');
 if($(ele).val() == '<?php echo $eRCTemplateSetting['Settings']['SubjectType']['Competitive']?>'){
  $('#subjectLimit_'+id).removeAttr('disabled');
  $('#subjectCredit_'+id).attr('disabled', true);
 } else if ($(ele).val() == '<?php echo $eRCTemplateSetting['Settings']['SubjectType']['Elective']?>'){
	  $('#subjectLimit_'+id).attr('disabled', true);
	  $('#subjectCredit_'+id).attr('disabled', true);
 } else {
  $('#subjectLimit_'+id).attr('disabled', true);
  $('#subjectCredit_'+id).removeAttr('disabled');
 }
}
</script>
<style>
tr.applyAll_Header td{
 margin-left: 10px;
}
input[type=number] {
 width: 50px;
}
td input[type="radio"]{
    margin: 0px 5px 0px 5px;
    padding: 0;
    width: auto;
}
</style>
<form id="form1" name="form1" method="POST" action="<?=$UpdatePage;?>">
	<table width="98%" border="0" cellpadding="4" cellspacing="0">
		<tr>
			<td align="right"><?=$linterface->GET_SYS_MSG($Result);?></td>
		</tr>
		<tr>
			<td align="center">
				<table width="100%" border="0" cellspacing="0" cellpadding="2">
					<tr>
						<td height="320" align="left" valign="top">
							<table width="100%" border="0" cellspacing="0" cellpadding="4">
								<tr>
									<td width="15" align="left">&nbsp;</td>
									<td width="25%" align="right">
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td valign="top">
													<table border="0" cellspacing="0" cellpadding="2">
														<tr>
															<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
															<td><img
																src="<?="{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif"></td>
														</tr>
													</table>
												</td>
												<td align="right"></td>
											</tr>
										</table>
									</td>
									<td align="left" valign="top" class="retabletop tabletopnolink">
										<table border="0" cellspacing="0" cellpadding="2">
											<tr>
												<td colspan="2" class="tabletopnolink"><?=$ClassLevelSelect?><a
													href="#"></a><?=$ReportSelection?></td>
											</tr>
										</table>
									</td>
									<td width="10" align="center"></td>
								</tr>
								<tr>
									<td align="left"><img
										src="<?="{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif"
										width="13" height="2"></td>
									<td align="right" valign="top"><img
										src="<?="{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif"
										width="13" height="2"></td>
									<td align="center" valign="top"><img
										src="<?="{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif"
										width="13" height="2"></td>
									<td align="center" valign="top"><img
										src="<?="{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif"
										width="13" height="2"></td>
								</tr>
							</table>
							<table width="100%" border="0" cellspacing="0" cellpadding="4">
								<tr class='applyAll_Header'>
									<th width="15" align="left">&nbsp;</th>
									<th width="25%" align="right" valign="top">&nbsp;</th>
									<th align="center" valign="top" class="tabletop tabletopnolink"><span
										class="tabletopnolink"><?=$eReportCard['SubjectType']['Type']?></span>
										<br/>
										<br/>
										<?=$globalTypeSelection?>
										</th>
									<th width="25%" align="center" valign="top"
										class="tabletop tabletopnolink"><span class="tabletopnolink"><?=$eReportCard['SubjectType']['SubjectUnit']?>
										<br/>
										<br/>
										<?=$globalCreditInput?>
										</span></th>
									<th width="20%" align="center" valign="top"
										class="tabletop tabletopnolink"><span class="tabletopnolink"><?=$eReportCard['SubjectType']['SubjectDisplayLimit']?>
										<br/>
										<br/>
										<?=$globalLimitInput?>
										</span></th>
								</tr>
              <?=$rx?>
            </table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<div class="edit_bottom_v30">
		<p class="spacer"></p>
		<?=(count($SubjectDataArr)<1 ? "" : $linterface->GET_ACTION_BTN($button_submit, "submit"))?>&nbsp;
		<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "backToPage();")?>
		<p class="spacer"></p>
	</div>
</form>

<?php
        $linterface->LAYOUT_STOP();
    } else {
        ?>
You have no priviledge to access this page.
    <?
    }
} else {
    ?>
You have no priviledge to access this page.
<?
}
?>