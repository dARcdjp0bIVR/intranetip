<?php
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
//include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");

intranet_auth();
intranet_opendb();

#################################################################################################

/* Temp Library*/
include_once($PATH_WRT_ROOT."includes/libreportcard2008j.php");
$lreportcard = new libreportcard2008j();
//$lreportcard = new libreportcard();

$result = array();
foreach($subjectType as $SubjectID => $Type){
        $result[] = $lreportcard->Update_Report_Credit($ClassLevelID, $ReportID, 
            $SubjectID, $Type, $subjectCredit[$SubjectID], $subjectLimit[$SubjectID]);
}


#################################################################################################
if(!in_array(false, $result))
	$msg = "update";
else 
	$msg = "update_failed";

#################################################################################################
intranet_closedb();

#header("Location: form_settings_edit.php?ClassLevelID=$ClassLevelID&Result=$msg");
header("Location: credit_settings.php?ClassLevelID=$ClassLevelID&ReportID=$ReportID&Result=$msg");

?>