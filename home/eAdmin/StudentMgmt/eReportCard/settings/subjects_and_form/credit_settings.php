<?php
// ############ MODIFICATION ### LOG ####################
//
// File using : Philips
// Last Modified : 20160615
// Created : 20180615
//
//
// ############ MODIFICATION ### LOG ####################
//
//
//
//
// Date: 20180615 (Philips)
// Created File
//
// ############ MODIFICATION ### LOG ####################

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
// include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/libreportcard2008.php");
include_once ($PATH_WRT_ROOT . "includes/libclass.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable2007a.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if ($plugin['ReportCard2008']) {
    /* Temp Library */
    include_once ($PATH_WRT_ROOT . "includes/libreportcard2008j.php");
    $lreportcard = new libreportcard2008j();
    if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") {
        include_once ($PATH_WRT_ROOT . "includes/reportcard_custom/" . $ReportCardCustomSchoolName . ".php");
    }
    // $lreportcard = new libreportcard();
    
    $CurrentPage = "Settings_SubjectsAndForms";
    $MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
    
    // tag information
    $link0 = 0;
    $link1 = 0;
    $link2 = 1;
    $link3 = 0;
    $TAGS_OBJ[] = array(
        $eReportCard['SubjectSettings'],
        $PATH_WRT_ROOT . "home/eAdmin/StudentMgmt/eReportCard/settings/subjects_and_form/subject_settings.php",
        $link0
    );
    $TAGS_OBJ[] = array(
        $eReportCard['FormSettings'],
        $PATH_WRT_ROOT . "home/eAdmin/StudentMgmt/eReportCard/settings/subjects_and_form/form_settings.php",
        $link1
    );
    $TAGS_OBJ[] = array(
        $eReportCard['SubjectType']['Type'],
        $PATH_WRT_ROOT . "home/eAdmin/StudentMgmt/eReportCard/settings/subjects_and_form/credit_settings.php",
        $link2
    );
    if($eRCTemplateSetting['Settings']['GradeDescriptor']) {
        $TAGS_OBJ[] = array(
            $eReportCard['GradeSettings'],
            $PATH_WRT_ROOT . "home/eAdmin/StudentMgmt/eReportCard/settings/subjects_and_form/grade_settings.php",
            $link3
        );
    }

    if ($lreportcard->hasAccessRight()) {
        $linterface = new interface_html();
        
        // ###########################################################################################################
        
        // Get class level of school
        $FormArr = $lreportcard->GET_ALL_FORMS();
        
        $LevelNameArr = array();
        $LevelIDArr = array();
        if (sizeof($FormArr) > 0) {
            for ($i = 0; $i < sizeof($FormArr); $i ++) {
                $LevelIDArr[] = $FormArr[$i]["ClassLevelID"];
                $LevelNameArr[] = $FormArr[$i]["LevelName"];
            }
        }
        $ClassLevelSelect = getSelectByValueDiffName($LevelIDArr, $LevelNameArr, 'name="ClassLevelID" id="ClassLevelID" onchange="document.form1.submit();"', $ClassLevelID, 0, 1);
        
        // Subjects
        $SubjectArray = $lreportcard->GET_ALL_SUBJECTS(0);
        
        // Initialization
        $FormName = '';
        $targetForm = '';
        $prevForm = '';
        $nextForm = '';
        if (isset($ClassLevelID) && $ClassLevelID != "")
        {
            $targetForm = $ClassLevelID;
            if (count($FormArr) > 0) {
                for ($i = 0; $i < count($FormArr); $i ++) {
                    if ($targetForm == $FormArr[$i]['ClassLevelID']) {
                        if ($i != 0) {
                            $prevForm = $FormArr[$i - 1]['ClassLevelID'];
                        }
                        else {
                            $prevForm = $FormArr[count($FormArr) - 1]['ClassLevelID'];
                        }
                        
                        if ($i != count($FormArr) - 1) {
                            $nextForm = $FormArr[$i + 1]['ClassLevelID'];
                        }
                        else {
                            $nextForm = $FormArr[0]['ClassLevelID'];
                        }
                        $FormName = $FormArr[$i]['LevelName'];
                    }
                }
            }
        }
        else
        {
            if (count($FormArr) > 0) {
                $targetForm = $FormArr[0]['ClassLevelID'];
                $prevForm = $FormArr[(count($FormArr) - 1)]['ClassLevelID'];
                $nextForm = $FormArr[1]['ClassLevelID'];
                $FormName = $FormArr[0]['LevelName'];
            }
        }
        
        // Get Report Selection 20100924
        //$ReportSelection = $lreportcard->Get_Report_Selection($targetForm, $ReportID, "ReportID", $ParOnchange = 'document.form1.submit();', 0, 0, 0, '', 1);
        
        $CheckSubjectArr = $lreportcard->CHECK_FROM_SUBJECT_GRADING($targetForm);
        
        $count = 0;
        $rx = '';
        
        $SubjectDataArr = array();
        $SubjectDataArr = $lreportcard->GET_FROM_SUBJECT_GRADING_SCHEME($targetForm, 0);
        
        $mainSubjects = array();
        // Get the last sub-subject of each subjects if any & use for css
        $LastCmpSubject = array();
        if (! empty($SubjectDataArr)) {
            foreach ($SubjectDataArr as $SubjectID => $Data) {
                if ($Data['is_cmpSubject'] == 1){
                    $LastCmpSubject[$Data['parentSubjectID']] = $SubjectID;
                } else {
                    $mainSubjects[] = $SubjectID;
                }
            }
        }
        
        if (! empty($SubjectDataArr)) {
            // [2015-1111-1214-16164] Get SubjectID and Subject Code Mapping
            if ($eRCTemplateSetting['Settings']['DisplaySubjectCode']) {
                $CODEID_Mapping = $lreportcard->GET_SUBJECTS_CODEID_MAP();
            }
            
            $SubjectTypeArr = array(
                $eRCTemplateSetting['Settings']['SubjectType']['Core'] => $eReportCard['SubjectType']['SubjectNormal'],
                $eRCTemplateSetting['Settings']['SubjectType']['Elective'] => $eReportCard['SubjectType']['SubjectElective'],
                $eRCTemplateSetting['Settings']['SubjectType']['Competitive'] => $eReportCard['SubjectType']['SubjectCompetitive']
            );
            
            $credits = array();
            $creditArr = $lreportcard->Get_Report_Credit($targetForm, $ReportID, $mainSubjects);
            foreach($creditArr as $ca){
                $credits[$ca['SubjectID']] = $ca;
            }
            
            foreach ($SubjectDataArr as $SubjectID => $Data) {
                $is_CmpSubject = ($Data['is_cmpSubject'] == 0) ? 0 : 1;
                // [2015-1111-1214-16164] Display Subject Code - $eRCTemplateSetting['Settings']['DisplaySubjectCode'] = true & Main Subject
                $SubjectCode = ($eRCTemplateSetting['Settings']['DisplaySubjectCode'] && ! $is_CmpSubject) ? "<br/>(" . $CODEID_Mapping[$SubjectID] . ")" : "";
                
                // $row_css = ($count % 2 == 0 || $is_CmpSubject) ? 'retablerow1' : 'retablerow2';
                $head_1_css = ((count($LastCmpSubject) > 0 && $LastCmpSubject[$Data['parentSubjectID']] == $SubjectID) || $Data['is_cmpSubject'] == 0) ? 'tablelist_subject' : 'tablelist_subject_head';
                $head_2_css = ($Data['is_cmpSubject'] == 0) ? 'tablelist_subject' : 'tablelist_sub_subject';
                // $row_css .= ($Data['is_cmpSubject']==0) ? '' : ' retablerow_subsubject_line';
                $row_css = $is_CmpSubject ? 'retablerow1' : 'retablerow2';
                $row_css .= ' retablerow_subsubject_line';
                
                // for Main Subject
                if (! $is_CmpSubject) {
                    $hasTypeSettings = $credits[$SubjectID]['Type'] != null;
                    
                    $rx .= '<tr class="' . $row_css . '">';
                    $rx .= '<td align="left" class="' . $head_1_css . '">&nbsp;</td>';
                    $rx .= '<td class="' . $head_2_css . '">' . $Data['subjectName'] . $SubjectCode . '</td>';
                    $rx .= '<td align="center">'.($hasTypeSettings ? $SubjectTypeArr[$credits[$SubjectID]['Type']] : $eReportCard['SubjectType']['NoSetting']).'</td>';
                    $rx .= '<td align="center">'.($hasTypeSettings ? ($credits[$SubjectID]['Type'] == $eRCTemplateSetting['Settings']['SubjectType']['Core']? $credits[$SubjectID]['Credit'] : $eReportCard['SubjectType']['NotApplicable']) : $eReportCard['SubjectType']['NoSetting']).'</td>';
                    $rx .= '<td align="center">'.($hasTypeSettings ? ($credits[$SubjectID]['Type'] == $eRCTemplateSetting['Settings']['SubjectType']['Competitive']? $credits[$SubjectID]['LowerLimit'] : $eReportCard['SubjectType']['NotApplicable']) : $eReportCard['SubjectType']['NoSetting']).'</td>';
                    $rx .= '</tr>';
                }
                
                $flag = 1;
                $count ++;
            }
        } else {
            $rx .= '<tr><td width="15" align="left" class="tablelist_sub_subject">&nbsp;</td>';
            $rx .= '<td colspan="4" align="center" width="100%" class="tabletext tablelist_sub_subject" height="50">'.$Lang['General']['NoRecordAtThisMoment'].'.</td>';
            $rx .= '</tr>';
        }
        
        // echo "<div id='top'></div>";
        $linterface->LAYOUT_START();
        
        echo '<link href="' . $PATH_WRT_ROOT . 'templates/2007a/css/ereportcard.css" rel="stylesheet" type="text/css" />';
        ?>
<script type="javascript">
</script>
<style>
</style>
<form name="form1" method="POST" action="<?=$UpdatePage;?>">
	<table width="98%" border="0" cellpadding="4" cellspacing="0">
		<tr>
			<td align="right"><?=$linterface->GET_SYS_MSG($Result);?></td>
		</tr>
		<tr>
			<td align="center">
				<table width="100%" border="0" cellspacing="0" cellpadding="2">
					<tr>
						<td height="320" align="left" valign="top">
							<table width="100%" border="0" cellspacing="0" cellpadding="4">
								<tr>
									<td width="15" align="left">&nbsp;</td>
									<td width="25%" align="right">
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td valign="top">
													<table border="0" cellspacing="0" cellpadding="2">
														<tr>
															<td><a
																href="credit_settings_edit.php?ClassLevelID=<?=$targetForm?>&ReportID=<?=$ReportID?>"
																class="contenttool"><img
																	src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_edit_b.gif"
																	width="20" height="20" border="0" align="absmiddle"
																	alt="<?=$button_edit?>"> <?=$button_edit?></a></td>
															<td><img
																src="<?="{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif"></td>
														</tr>
													</table>
												</td>
												<td align="right"></td>
											</tr>
										</table>
									</td>
									<td align="left" valign="top" class="retabletop tabletopnolink">
										<table border="0" cellspacing="0" cellpadding="2">
											<tr>
												<td colspan="2" class="tabletopnolink"><?=$ClassLevelSelect?><a
													href="#"></a><?=$ReportSelection?></td>
											</tr>
										</table>
									</td>
									<td width="10" align="center"></td>
								</tr>
								<tr>
									<td align="left"><img
										src="<?="{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif"
										width="13" height="2"></td>
									<td align="right" valign="top"><img
										src="<?="{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif"
										width="13" height="2"></td>
									<td align="center" valign="top"><img
										src="<?="{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif"
										width="13" height="2"></td>
									<td align="center" valign="top"><img
										src="<?="{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif"
										width="13" height="2"></td>
								</tr>
							</table>
							<table width="100%" border="0" cellspacing="0" cellpadding="4">
								<tr>
									<th width="15" align="left">&nbsp;</th>
									<th width="25%" align="right" valign="top">&nbsp;</th>
									<th align="center" valign="top" class="tabletop tabletopnolink"><span
										class="tabletopnolink"><?=$eReportCard['SubjectType']['Type']?></span></th>
									<th width="25%" align="center" valign="top"
										class="tabletop tabletopnolink"><span class="tabletopnolink"><?=$eReportCard['SubjectType']['SubjectUnit']?></span></th>
									<th width="20%" align="center" valign="top"
										class="tabletop tabletopnolink"><span class="tabletopnolink"><?=$eReportCard['SubjectType']['SubjectDisplayLimit']?></span></th>
								</tr>
              <?=$rx?>
            </table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</form>

<?php
        $linterface->LAYOUT_STOP();
    } else {
        ?>
You have no priviledge to access this page.
    <?
    }
} else {
    ?>
You have no priviledge to access this page.
<?
}
?>