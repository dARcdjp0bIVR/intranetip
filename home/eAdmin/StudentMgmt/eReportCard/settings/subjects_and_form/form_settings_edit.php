<?php
// Using: 

/***********************************************
 * 		Modification log
 * 	20151218 Bill:	[2015-1111-1214-16164]
 * 		- Display Subject Code when $sys_custom['Subject']['DisplaySubjectCode'] = true
 * 	20151215	Bill	[2015-1214-1438-52073]
 * 		- update CurrentEditingSelection value only when editing grading scheme, prevent Save As error when mouse over another subjects
 *  20151201	Ivan [J89803] [ip.2.5.7.1.1]
 * 		- improved: ajax_yahoo.js and ajax_connection.js change to internal relative path
 * 	20150505 Bill:
 * 		- BIBA Cust
 * 		- ensure only one language can be selected if $eRCTemplateSetting['SelectSubjectDisplayLang_OneLangOnly'] = true
 * 	20110610 Marcus:
 * 		1.	cater grading scheme grade description
 * 	20100331 Marcus:
 * 		1.	also refresh set all SelectionBox of grading scheme
 * 
 *  20091221 Marcus:
 * 		1.	cater more than 1 distinction
 * 	20091218 Marcus:
 * 		1.	migrate lang display option from ip20 to ip25, for DGS Reportcard
 * 
 * 	20091210 Marcus:
 * 		1.	replace $Data['displayOrder'] (may crash) with $row_index (row num always unique), 
 * 			so that the display order in RC_SUBJECT_FORM_GRADING will not crash.
 * 
 * *********************************************/ 
//temp

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
//include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include($PATH_WRT_ROOT."includes/eRCConfig.php");
	
intranet_auth();
intranet_opendb();

if ($plugin['ReportCard2008'])
{
	/* Temp Library*/
	//include_once($PATH_WRT_ROOT."includes/libreportcard2008j.php");
	//$lreportcard = new libreportcard2008j();
	if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
	{
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
		$lreportcard = new libreportcardcustom();
	} else {
		$lreportcard = new libreportcard();
	}
	
	include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
	$lreportcard_ui = new libreportcard_ui();
	
	$CurrentPage = "Settings_SubjectsAndForms";
	$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
    if ($lreportcard->hasAccessRight())
    {
        $linterface = new interface_html();
      	$hasAdditionalCriteria = $eRCTemplateSetting['AdditionalCriteria'];
 		############################################################################################################
		
		# Get Class Level of school
		$FormArr = $lreportcard->GET_ALL_FORMS();
		
		$LevelNameArr = array();
		$LevelIDArr = array();
		if (sizeof($FormArr) > 0) {
			for($i=0; $i<sizeof($FormArr); $i++) {
				$LevelIDArr[] = $FormArr[$i]["ClassLevelID"];
				$LevelNameArr[] = $FormArr[$i]["LevelName"];
			}
		}
		$ClassLevelSelect = getSelectByValueDiffName($LevelIDArr, $LevelNameArr, "name='ClassLevelID' id='ClassLevelID' onchange='changeClassLevel(1)'", $ClassLevelID, 0, 1);
		
		# Get Report Selection 20100924
		$ReportSelection = $lreportcard->Get_Report_Selection($ClassLevelID, $ReportID, "ReportID", $ParOnchange='changeClassLevel()',0,0,0,'',1);

		// Get All Subjects
		$SubjectArray = $lreportcard->GET_ALL_SUBJECTS(0);
		
		# Initialization
		$FormName = '';
		$targetForm = '';
		$prevForm = '';
		$nextForm = '';
		if(isset($ClassLevelID) && $ClassLevelID != ""){
			$targetForm = $ClassLevelID;
			if(count($FormArr) > 0){
				for($i=0 ;$i<count($FormArr) ; $i++){
					if($targetForm == $FormArr[$i]['ClassLevelID']){
						if($i != 0)
							$prevForm = $FormArr[$i-1]['ClassLevelID'];
						else 
							$prevForm = $FormArr[count($FormArr)-1]['ClassLevelID'];
							
						if($i != count($FormArr)-1)
							$nextForm = $FormArr[$i+1]['ClassLevelID'];
						else 
							$nextForm = $FormArr[0]['ClassLevelID'];
							
						$FormName = $FormArr[$i]['LevelName'];
					}
				}
			}			
		} else {
			if(count($FormArr) > 0){
				$targetForm = $FormArr[0]['ClassLevelID'];
				$prevForm = $FormArr[(count($FormArr)-1)]['ClassLevelID'];
				$nextForm = $FormArr[1]['ClassLevelID'];
				$FormName = $FormArr[0]['LevelName'];
			}
		}	
		
		// Get whether each subject is checked for target ClassLevel(Form) or not
//		$CheckSubjectArr = $lreportcard->CHECK_FROM_SUBJECT_GRADING($targetForm, $ReportID);
		
		/*
		* Necessary Data
		* - Display Order
		* - SubjectName, SubjectID, CmpSubjectID, Scheme, Subject Scale (Grade, Mark)
		* - SchemeTitle, Description, SchemeType, FullMark, Pass, Fail, Weighting
		* - coloring by basic setting & css style sheet
		* - Select / Radio Btn index : XXX_SubjectID or XXX_SubjectID_FormID
		*/
		
		$SubjectDataArr = array();
		$count = 0;
		$row = 1;
		$rx = '';
		
		$SubjectDataArr = $lreportcard->GET_FROM_SUBJECT_GRADING_SCHEME($targetForm,'',$ReportID);
		
		// Get the last sub-subject of each subjects if any & use for css 
		$LastParentSubjectID = '';
		$LastCmpSubject = array();
		$CmpSubject = array();
		
		if(!empty($SubjectDataArr)){
			foreach($SubjectDataArr as $SubjectID => $Data){
				if($Data['parentSubjectID'] == $SubjectID)
					$LastParentSubjectID = $SubjectID;
					
				if($Data['is_cmpSubject'] == 1){
					$CmpSubject[$Data['parentSubjectID']][] = $SubjectID;
					$LastCmpSubject[$Data['parentSubjectID']] = $SubjectID;
				}
			}
		}	
		
		// Main		
		$cmpNoUpFlag = 0;	// check for determining the start of sub-subject for each parent subject
		$cmpNoDownFlag = 0;	
		
		// Get LangDisplay options
		// set English as default language
		if($eRCTemplateSetting['SelectSubjectDisplayLang_DefaultEng']){
			$LangSeletionArr[] = array("en", $eReportCard['LangDisplayChoice']['en']);
		}
		foreach((array)$eReportCard['LangDisplayChoice'] as $value => $OptionDisplay){
			// BIBA Cust - $eRCTemplateSetting['SelectSubjectDisplayLang_OneLangOnly'] 
			// ensure only one language can be selected
			if($eRCTemplateSetting['SelectSubjectDisplayLang_OneLangOnly'] && $value=="both"){
				continue;
			}
			// skip as English is default language
			if($eRCTemplateSetting['SelectSubjectDisplayLang_DefaultEng'] && $value=="en"){
				continue;
			}
			$LangSeletionArr[] = array($value,$OptionDisplay);
		}
			
		
		### Global Settings
		# Grading Scheme
		$globalSchemeSelection = $lreportcard_ui->Get_Grading_Scheme_Selection('scheme_global', '', '', 'tabletexttoptext');
		$Scheme_ApplyAllBtn = '<a href="javascript:jsApplyToAll(\'scheme_global\', \'scheme_selection\');"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_assign.gif" width="12" height="12" border="0"></a>';
				
		# Input Scale
		$globalInputScale = '<input type="radio" id="input_scale_mark_global" name="input_scale_global" checked><label for="input_scale_mark_global">'.$eReportCard['Mark'].'</label> &nbsp;&nbsp;';
		$globalInputScale .= '<input type="radio" id="input_scale_grade_global" name="input_scale_global"><label for="input_scale_grade_global">'.$eReportCard['Grade'].'</label>';
		$InputScale_ApplyAllBtn = '<a href="javascript:jsApplyToAllRadio(\'input_scale_mark_global\', \'input_scale_mark\', \'input_scale_grade_global\', \'input_scale_grade\');"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_assign.gif" width="12" height="12" border="0"></a>';
		
		# Display Result
		$globalDisplayScale = '<input type="radio" id="display_scale_mark_global" name="display_scale_global" checked><label for="display_scale_mark_global">'.$eReportCard['Mark'].'</label> &nbsp;&nbsp;';
		$globalDisplayScale .= '<input type="radio" id="display_scale_grade_global" name="display_scale_global"><label for="display_scale_grade_global">'.$eReportCard['Grade'].'</label>';
		$DisplayScale_ApplyAllBtn = '<a href="javascript:jsApplyToAllRadio(\'display_scale_mark_global\', \'display_scale_mark\', \'display_scale_grade_global\', \'display_scale_grade\');"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_assign.gif" width="12" height="12" border="0"></a>';
		
		# Subject Display Language
		if($eRCTemplateSetting['SelectSubjectDisplayLang']) {
			$globalLangDisplaySelection = getSelectByArray($LangSeletionArr, "class='tabletexttoptext' id='lang_display_selection_global'", '', 0, 1);
			$LangDisplay_ApplyAllBtn = '<a href="javascript:jsApplyToAll(\'lang_display_selection_global\', \'lang_display_selection\');"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_assign.gif" width="12" height="12" border="0"></a>';
		}
		
		# Display Subject Group
		if($eRCTemplateSetting['SelectDisplaySubjectGroup']) {
			$globalDisplaySubjectGroup = '<input type="radio" name="display_subject_group_global" id="display_subject_group_yes_global"><label for="display_subject_group_yes_global">'.$Lang['General']['Yes'].'</label> &nbsp;&nbsp;';
			$globalDisplaySubjectGroup .= '<input type="radio" name="display_subject_group_global" id="display_subject_group_no_global" checked><label for="display_subject_group_no_global">'.$Lang['General']['No'].'</label>';
			$DisplaySubjectGroup_ApplyAllBtn = '<a href="javascript:jsApplyToAllRadio(\'display_subject_group_yes_global\', \'display_subject_group_yes\', \'display_subject_group_no_global\', \'display_subject_group_no\');"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_assign.gif" width="12" height="12" border="0"></a>';
		}
						
						
		if(!empty($SubjectDataArr)){
			$row_index=0;
			
			// [2015-1111-1214-16164] Get SubjectID and Subject Code Mapping
			if($eRCTemplateSetting['Settings']['DisplaySubjectCode']){
				$CODEID_Mapping = $lreportcard->GET_SUBJECTS_CODEID_MAP();
			}
			foreach($SubjectDataArr as $SubjectID => $Data){
				$is_CmpSubject = ($Data['is_cmpSubject']==0) ? 0 : 1;
				// [2015-1111-1214-16164] Display Subject Code - $eRCTemplateSetting['Settings']['DisplaySubjectCode'] = true & Main Subject
				$SubjectCode = ($eRCTemplateSetting['Settings']['DisplaySubjectCode'] && !$is_CmpSubject)? "<br/>(".$CODEID_Mapping[$SubjectID].")" : "";
				
				$row_css = ($count % 2 == 0 || $is_CmpSubject) ? 'retablerow1' : 'retablerow2';
				$head_1_css = ((count($LastCmpSubject) > 0 && $LastCmpSubject[$Data['parentSubjectID']] == $SubjectID) || $Data['is_cmpSubject']==0) ? 'tablelist_subject' : 'tablelist_subject_head';
				$head_2_css = ($is_CmpSubject==0) ? 'tablelist_subject' : 'tablelist_sub_subject';
				$row_css .= ($is_CmpSubject==0) ? '' : ' retablerow_subsubject_line';
				
				if(count($CmpSubject) > 0){
					$cmpNoUpFlag = ($CmpSubject[$Data['parentSubjectID']][0] == $SubjectID) ? 1 : 0;	
					$cmpNoDownFlag = ($CmpSubject[$Data['parentSubjectID']][count($CmpSubject[$Data['parentSubjectID']])-1] == $SubjectID) ? 1 : 0;
				}
							
				//Gen Lang Display Seletion			
				$thisDisplayLang = $lreportcard->GetFormSubjectDisplayLang($SubjectID,$ClassLevelID, $ReportID);
				$SeletionName = "LangDisplay_".$SubjectID;
				$thisDisplayLangSeletion = getSelectByArray($LangSeletionArr,"class='tabletexttoptext lang_display_selection' id='$SeletionName' name='$SeletionName'",$thisDisplayLang,0,1);
							
				// GET Grading Scheme Data
				$noSchemeFlag = 0;	// check for any scheme is assigned to each subject : 1-no scheme, 	0- has scheme
				$SchemeData = array();
				$SchemeMainArr = array();
				
				if($Data['schemeID'] != null || $Data['schemeID'] != ""){
					$SchemeData = $lreportcard->GET_GRADING_SCHEME_MAIN_INFO($Data['schemeID']);
					$SchemeMainArr = $SchemeData;
					
					if(count($SchemeMainArr) == 0)
						$noSchemeFlag = 1;
				}
				else {
					$noSchemeFlag = 1;	
				}
				
				// Generate SelectBox of Grading Scheme
				$AllSchemeIDNameMap = array();
				$AllSchemeData = $lreportcard->GET_GRADING_SCHEME_MAIN_INFO();
				$select = '<option value=""> --'.$eReportCard['SelectScheme'].'-- </option>';
				for($k=0 ; $k<count($AllSchemeData) ; $k++) {
					$AllSchemeIDNameMap[$AllSchemeData[$k]['SchemeID']] = $AllSchemeData[$k]['SchemeTitle'];
					$select .= '<option value="'.$AllSchemeData[$k]['SchemeID'].'" '.(($AllSchemeData[$k]['SchemeID'] == $Data['schemeID']) ? "selected" : "").'>'.$AllSchemeData[$k]['SchemeTitle'].'</option>';
				}
				
				
				// for Main Subject
				$rx .= '<tr><td width="15" align="left" id="td_head_'.$SubjectID.'" class="'.$head_1_css.'">';
				$rx .= '<table border="0" cellspacing="0" cellpadding="2">';
				if($Data['is_cmpSubject']){		// for cmpSubject
			        $rx .= '<tr><td><div id="div_up_'.$SubjectID.'" style="display:'.(($cmpNoUpFlag == 1) ? 'none' : 'block').'"><a name="move_up[]" id="move_up'.$SubjectID.'" href="javascript:void(0)" class="contenttool" onclick="jMOVE_TABLE_ROW(this, \'1\', \''.$SubjectID.'\')"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_sort_a_off.gif" width="13" height="13" border="0" alt="'.$eReportCard['MoveUp'].'"></a></div></td></tr>';
			        $rx .= '<tr><td><div id="div_down_'.$SubjectID.'" style="display:'.(($cmpNoDownFlag == 1) ? 'none' : 'block').'"><a name="move_down[]" id="move_down'.$SubjectID.'" href="javascript:void(0)" class="contenttool" onclick="jMOVE_TABLE_ROW(this, \'-1\', \''.$SubjectID.'\')"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_sort_d_off.gif" width="13" height="13" border="0" alt="'.$eReportCard['MoveDown'].'"></a></div></td></tr>';
		        }
		        else {		// for Main Subject
			        $rx .= '<tr><td><div id="div_up_'.$SubjectID.'" style="display:'.(($count != 0) ? 'block' : 'none').'"><a name="move_up[]" id="move_up'.$SubjectID.'" href="javascript:void(0)" class="contenttool" onclick="jMOVE_TABLE_ROW(this, \'1\', \''.$SubjectID.'\')"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_sort_a_off.gif" width="13" height="13" border="0" alt="'.$eReportCard['MoveUp'].'"></a></div></td></tr>';
			        $rx .= '<tr><td><div id="div_down_'.$SubjectID.'" style="display:'.(($LastParentSubjectID == $Data['parentSubjectID'] || $count == count($SubjectDataArr)-1) ? 'none' : 'block').'"><a name="move_down[]" id="move_down'.$SubjectID.'" href="javascript:void(0)" class="contenttool" onclick="jMOVE_TABLE_ROW(this, \'-1\', \''.$SubjectID.'\')"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_sort_d_off.gif" width="13" height="13" border="0" alt="'.$eReportCard['MoveDown'].'"></a></div></td></tr>';
		        }
		        $rx .= '</tr></table>';
		        $rx .= '</td>';
		        $rx .= '<td width="20%" class="'.$head_2_css.'">'.$Data['subjectName'].$SubjectCode;
		        $rx .= '<input type="hidden" name="subject_name_'.$SubjectID.'" id="subject_name_'.$SubjectID.'" value="'.$Data['subjectName'].'"/>';
		        //$rx .= '<input type="hidden" name="display_order_'.$SubjectID.'" id="display_order_'.$SubjectID.'" value="'.$Data['displayOrder'].'"/>';
		        $rx .= '<input type="hidden" name="display_order_'.$SubjectID.'" id="display_order_'.$SubjectID.'" value="'.++$row_index.'"/>';
		        $rx .= '<input type="hidden" name="cmpSubjectFlag_'.$SubjectID.'" id="cmpSubjectFlag_'.$SubjectID.'" value="'.$is_CmpSubject.'"/>';
		        $rx .= '<input type="hidden" name="parent_subject_id_'.$SubjectID.'" id="parent_subject_id_'.$SubjectID.'" value="'.$Data['parentSubjectID'].'"/>';
		        $rx .= '<input type="hidden" name="subjectID[]" id="subjectID_'.($row).'" value="'.$SubjectID.'"/></td>';
		        $rx .= '<td id="td_select_'.$SubjectID.'" align="center" class="'.$row_css.'">';
		        $rx .= '<table border="0" cellspacing="0" cellpadding="0"><tr>';
		        
		        $rx .= '<td align="center"><select name="select_scheme_'.$SubjectID.'" id="select_scheme_'.$SubjectID.'" class="tabletexttoptext scheme_selection" onMouseOver="jSHOW_GRADING_SCHEME1(document.getElementById(\'img_edit_'.$SubjectID.'\'), \''.$SubjectID.'\', \'0\')"  onMouseOut="jHIDE_GRADING_SCHEME1(\''.$SubjectID.'\', \'\', \'0\')" onchange="jCHANGE_GRADING_METHOD(\''.$SubjectID.'\')">'.$select.'</select></td>';
		        $rx .= '<td><a href="javascript:jSHOW_GRADING_SCHEME1(document.getElementById(\'img_edit_'.$SubjectID.'\'), \''.$SubjectID.'\', \'1\')"><img id="img_edit_'.$SubjectID.'" src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_edit_b.gif" width="20" height="20" border="0" alt="'.$button_edit.' '.$eReportCard['GradingScheme'].'"></a></td>';
		        
		        $rx .= '</tr></table>';
		        $rx .= '</td>';
		        /*
		        * dsiplay result
		        * Input Scale / Display Result: 0-Grade ; 1-Mark
		        * Radio : scale_[SubjectID]     , display_[SubjectID]
		        */
		        $scale_pf_str = '--';
		        $display_pf_str = '--';
		        
				$scale_h_str = '<input type="radio" class="input_scale_mark" name="scale_'.$SubjectID.'" id="scale_'.$SubjectID.'_2" value="M" '.(($Data['scaleInput'] == "M") ? "checked" : "").'><label for="scale_'.$SubjectID.'_2">'.$eReportCard['Mark'].'</label> &nbsp;&nbsp;';
		    	$scale_h_str .= '<input type="radio" class="input_scale_grade" name="scale_'.$SubjectID.'" id="scale_'.$SubjectID.'_1" value="G" '.(($Data['scaleInput'] == "G") ? "checked" : "").'><label for="scale_'.$SubjectID.'_1">'.$eReportCard['Grade'].'</label>';
				
				$display_h_str = '<input type="radio" class="display_scale_mark" name="display_'.$SubjectID.'" id="display_'.$SubjectID.'_2" value="M" '.(($Data['scaleDisplay'] == "M") ? "checked" : "").'><label for="display_'.$SubjectID.'_2">'.$eReportCard['Mark'].'</label> &nbsp;&nbsp;';
				$display_h_str .= '<input type="radio" class="display_scale_grade" name="display_'.$SubjectID.'" id="display_'.$SubjectID.'_1" value="G" '.(($Data['scaleDisplay'] == "G") ? "checked" : "").'><label for="display_'.$SubjectID.'_1">'.$eReportCard['Grade'].'</label>';
				
		        $rx .= '<td id="td_scale_'.$SubjectID.'" align="center" class="'.$row_css.' '.$dot_css.'"><span class="tabletext">';
		        $rx .= '<div id="div_scale_honor_'.$SubjectID.'" style="display:'.(($SchemeMainArr['SchemeType'] == "PF") ? 'none' : 'block').'">'.$scale_h_str.'</div>';
		        $rx .= '<div id="div_scale_passfail_'.$SubjectID.'" style="display:'.(($SchemeMainArr['SchemeType'] == "PF") ? 'block' : 'none').'">'.$scale_pf_str.'</div>';
		        $rx .= '</span></td>';        
		        $rx .= '<td id="td_display_'.$SubjectID.'" align="center" class="'.$row_css.' '.$dot_css.'"><span class="tabletext">';
		        $rx .= '<div id="div_display_honor_'.$SubjectID.'" style="display:'.(($SchemeMainArr['SchemeType'] == "PF") ? 'none' : 'block').'">'.$display_h_str.'</div>';
		        $rx .= '<div id="div_display_passfail_'.$SubjectID.'" style="display:'.(($SchemeMainArr['SchemeType'] == "PF") ? 'block' : 'none').'">'.$display_pf_str.'</div>';
		        $rx .= '</span></td>';
		        
		        if($eRCTemplateSetting['SelectSubjectDisplayLang'])   
			    {
			        $rx .= '<td id="td_langdisplay_'.$SubjectID.'" align="center" class="'.$row_css.' '.$dot_css.'">';
					if($Data['is_cmpSubject'])
					{
						$rx.='&nbsp;';
					}
					else
					{
						$rx .=$thisDisplayLangSeletion;
					}
					$rx .= '</td>';
			    }
			    
			    if($eRCTemplateSetting['SelectDisplaySubjectGroup']) 
			    { 
			    	//Get Display Subject Group options
					$thisDisplaySubjectGroup = $lreportcard->GetDisplaySubjectGroup($SubjectID,$ClassLevelID, $ReportID);
					$radioName = "Display_SubjGroup_".$SubjectID;
				    
				    if($Data['is_cmpSubject'])
					{
						$display_SubjGroup_str='&nbsp;';
					}
					else
					{
					  	$display_SubjGroup_str = '<input type="radio" class="display_subject_group_yes" name="'.$radioName.'" id="'.$radioName.'_2" value="Y" '.(($thisDisplaySubjectGroup == "Y") ? "checked" : "").'><label for="'.$radioName.'_2">'.$Lang['General']['Yes'].'</label> &nbsp;&nbsp;';
						$display_SubjGroup_str .= '<input type="radio" class="display_subject_group_no" name="'.$radioName.'" id="'.$radioName.'_1" value="N" '.(($thisDisplaySubjectGroup == "N"||empty($thisDisplaySubjectGroup)) ? "checked" : "").'><label for="'.$radioName.'_1">'.$Lang['General']['No'].'</label>';
					}
					
					$rx .= '<td id="td_Display_SubjGroup_'.$SubjectID.'" align="center" class="'.$row_css.' '.$dot_css.'"><span class="tabletext">';
			        $rx .= '<div id="div_Display_SubjGroup_'.$SubjectID.'" >'.$display_SubjGroup_str.'</div>';
			        $rx .= '</span></td>';
			    }
			    
		        $rx .= '</tr>';
        
				$flag = 1;
				$count++;
				$row++;
			}
			
			
			############ Grand Mark Granding Scheme ############
			$GrandSchemeIndexArr = $lreportcard->Get_Grand_Mark_Grading_Scheme_Index_Arr($ClassLevelID);
			$GrandSchemeInfoArr = $lreportcard->Get_Grand_Mark_Grading_Scheme($ClassLevelID, $ReportID);
			
			foreach ($GrandSchemeIndexArr as $GrandMarkIndex => $GrandMarkType)
			{
				$GrandMarkSchemeData = $GrandSchemeInfoArr[$GrandMarkIndex];
				$row_css = ($count % 2 == 0) ? 'retablerow1' : 'retablerow2';
				$head_1_css = 'tablelist_subject';
				$head_2_css = 'tablelist_subject';
				
				$cmpNoUpFlag = 0;
				$cmpNoDownFlag = 0;
							
				// GET Grading Scheme Data
				$noSchemeFlag = 0;	// check for any scheme is assigned to each subject : 1-no scheme, 	0- has scheme
				$SchemeData = array();
				$SchemeMainArr = array();
				
				if($GrandMarkSchemeData['SchemeID'] != null || $GrandMarkSchemeData['SchemeID'] != ""){
					$SchemeData = $lreportcard->GET_GRADING_SCHEME_MAIN_INFO($GrandMarkSchemeData['SchemeID']);
					$SchemeMainArr = $SchemeData;
					
					if(count($SchemeMainArr) == 0)
						$noSchemeFlag = 1;
				}
				else {
					$noSchemeFlag = 1;	
				}
				
				// Generate SelectBox of Grading Scheme
				$AllSchemeIDNameMap = array();
				$AllSchemeData = $lreportcard->GET_GRADING_SCHEME_MAIN_INFO();
				$select = '<option value=""> --'.$eReportCard['SelectScheme'].'-- </option>';
				for($k=0 ; $k<count($AllSchemeData) ; $k++) {
					$AllSchemeIDNameMap[$AllSchemeData[$k]['SchemeID']] = $AllSchemeData[$k]['SchemeTitle'];
					$select .= '<option value="'.$AllSchemeData[$k]['SchemeID'].'" '.(($AllSchemeData[$k]['SchemeID'] == $GrandMarkSchemeData['SchemeID']) ? "selected" : "").'>'.$AllSchemeData[$k]['SchemeTitle'].'</option>';
				}
				
				// for Main Subject
				//$rx .= '<tr><td>&nbsp;</td></tr>';
				$rx .= '<tr><td width="15" align="left" id="td_head_'.$GrandMarkType.'" class="'.$head_1_css.'">';
				$rx .= '<table border="0" cellspacing="0" cellpadding="2">';
				$rx .= '<tr><td>&nbsp;</td></tr>';
			    $rx .= '<tr><td>&nbsp;</td></tr>';
		        $rx .= '</tr></table>';
		        $rx .= '</td>';
		        $rx .= '<td width="20%" class="'.$head_2_css.'"><b>'.$eReportCard['Template'][$GrandMarkType.'En'].'<br />'.$eReportCard['Template'][$GrandMarkType.'Ch'].'</b>';
		         $rx .= '<td id="td_select_GrandAverage" align="center" class="'.$row_css.'">';
		        $rx .= '<table border="0" cellspacing="0" cellpadding="0"><tr>';
		        
		        $rx .= '<td align="center"><select name="select_scheme_'.$GrandMarkType.'" id="select_scheme_'.$GrandMarkType.'" class="tabletexttoptext scheme_selection" onMouseOver="jSHOW_GRADING_SCHEME1(document.getElementById(\'img_edit_'.$GrandMarkType.'\'), \''.$GrandMarkType.'\', \'0\')"  onMouseOut="jHIDE_GRADING_SCHEME1(\''.$GrandMarkType.'\', \'\', \'0\')" onchange="jCHANGE_GRADING_METHOD(\''.$GrandMarkType.'\')">'.$select.'</select></td>';
			    $rx .= '<td><a href="javascript:jSHOW_GRADING_SCHEME1(document.getElementById(\'img_edit_'.$GrandMarkType.'\'), \''.$GrandMarkType.'\', \'1\')"><img id="img_edit_'.$GrandMarkType.'" src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_edit_b.gif" width="20" height="20" border="0" alt="'.$button_edit.' '.$eReportCard['GradingScheme'].'"></a></td>';
			            
		        $rx .= '</tr></table>';
		        $rx .= '</td>';
		        /*
		        * dsiplay result
		        * Input Scale / Display Result: 0-Grade ; 1-Mark
		        * Radio : scale_[SubjectID]     , display_[SubjectID]
		        */
		        $scale_pf_str = '--';
		        $display_pf_str = '--';
		        
				//$scale_h_str = '<input type="radio" name="scale_GrandAverage" id="scale_GrandAverage_2" value="M" '.(($GrandMarkSchemeData['scaleInput'] == "M") ? "checked" : "").'>'.$eReportCard['Mark'].' &nbsp;&nbsp;';
		    	//$scale_h_str .= '<input type="radio" name="scale_GrandAverage" id="scale_GrandAverage_1" value="G" '.(($GrandMarkSchemeData['scaleInput'] == "G") ? "checked" : "").'>'.$eReportCard['Grade'];
				$scale_h_str = '--';
		    	
				$display_h_str = '<input type="radio" class="display_scale_mark" name="display_'.$GrandMarkType.'" id="display_'.$GrandMarkType.'_2" value="M" '.(($GrandMarkSchemeData['ScaleDisplay'] == "M") ? "checked" : "").'><label for="display_'.$GrandMarkType.'_2">'.$eReportCard['Mark'].'</label> &nbsp;&nbsp;';
				$display_h_str .= '<input type="radio" class="display_scale_grade" name="display_'.$GrandMarkType.'" id="display_'.$GrandMarkType.'_1" value="G" '.(($GrandMarkSchemeData['ScaleDisplay'] == "G") ? "checked" : "").'><label for="display_'.$GrandMarkType.'_1">'.$eReportCard['Grade'].'</label>';
				
		        $rx .= '<td id="td_scale_'.$GrandMarkType.'" align="center" class="'.$row_css.' '.$dot_css.'"><span class="tabletext">';
		        $rx .= '<div id="div_scale_honor_'.$GrandMarkType.'" style="display:'.(($SchemeMainArr['SchemeType'] == "PF") ? 'none' : 'block').'">'.$scale_h_str.'</div>';
		        $rx .= '<div id="div_scale_passfail_'.$GrandMarkType.'" style="display:'.(($SchemeMainArr['SchemeType'] == "PF") ? 'block' : 'none').'">'.$scale_pf_str.'</div>';
		        $rx .= '</span></td>';
		        $rx .= '<td id="td_display_'.$GrandMarkType.'" align="center" class="'.$row_css.' '.$dot_css.'"><span class="tabletext">';
		        $rx .= '<div id="div_display_honor_'.$GrandMarkType.'" style="display:'.(($SchemeMainArr['SchemeType'] == "PF") ? 'none' : 'block').'">'.$display_h_str.'</div>';
		        $rx .= '<div id="div_display_passfail_'.$GrandMarkType.'" style="display:'.(($SchemeMainArr['SchemeType'] == "PF") ? 'block' : 'none').'">'.$display_pf_str.'</div>';
		        $rx .= '</span></td>';
		        
    	        if($eRCTemplateSetting['SelectSubjectDisplayLang']) //print empty td for GrandAverage   
				{
		        	$rx .= '<td align="center" class="'.$row_css.' '.$dot_css.'">&nbsp;</td>';
				}
		        if($eRCTemplateSetting['SelectDisplaySubjectGroup']) //print empty td for GrandAverage   
				{
		        	$rx .= '<td align="center" class="'.$row_css.' '.$dot_css.'">&nbsp;</td>';
				}
		        
		        $rx .= '</tr>';
	    
				$flag = 1;
				$count++;
				$row++;
			}
			############ End of Grand Mark Granding Scheme ############
		}
		else {
			$rx .= '<tr><td width="15" align="left" class="tablelist_sub_subject">&nbsp;</td>';
		    $rx .= '<td colspan="4" align="center" width="100%" class="tabletext tablelist_sub_subject" height="50">There is no record at this moment.</td>';
		    $rx .= '</tr>';
		}
		
		
		
		
		# Get ClassLevel for Copying ClassLevel Settings
		$CopySelectStr = '';
		$CopyFormOption = array();
		for($j=0 ; $j<count($FormArr) ; $j++){
//			if($FormArr[$j]['ClassLevelID'] != $ClassLevelID){
				$CopyFormOption[] = array($FormArr[$j]['ClassLevelID'], $FormArr[$j]['LevelName']);
//			}
		}
		
		$CopyTemplateReportSelect = $lreportcard->Get_Report_Selection($FormArr[0]['ClassLevelID'], '', "toReportID", $ParOnchange='',0,0,0,'',1, $ReportID);
		
		$CopyFormSelectStr = '
			<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
			  <tr>
			    <td valign="middle" nowrap="nowrap" ><span class="tabletext">'.$eReportCard['CopyTo'].'</span></td>
			  </tr>
			  <tr>
			    <td valign="top">
			      '.$linterface->GET_SELECTION_BOX($CopyFormOption, 'name="copyClassLevel" id="copyClassLevel" onchange="aj_ReloadToReportSelect()" ', '', '').'
			    </td>
			  </tr>
			<tr>
			    <td valign="top">
		          	<div id="CopySchemeReportSelect">
          		      '.$CopyTemplateReportSelect.'
					</div>
			    </td>
			  </tr>
			</table>';
		
		
		
		############################################################################################################
        
		# tag information
		$link0 = 0;
		$link1 = 1;
		$link2 = 0;
		$link3 = 0;
		$TAGS_OBJ[] = array($eReportCard['SubjectSettings'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/settings/subjects_and_form/subject_settings.php", $link0);
		$TAGS_OBJ[] = array($eReportCard['FormSettings'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/settings/subjects_and_form/form_settings.php", $link1);
		if($eRCTemplateSetting['Settings']['FormSubjectTypeSettings']){
		    $TAGS_OBJ[] = array($eReportCard['SubjectType']['Type'],$PATH_WRT_ROOT . "home/eAdmin/StudentMgmt/eReportCard/settings/subjects_and_form/credit_settings.php",$link2);
		}
        if($eRCTemplateSetting['Settings']['GradeDescriptor']){
            $TAGS_OBJ[] = array($eReportCard['GradeSettings'], $PATH_WRT_ROOT . "home/eAdmin/StudentMgmt/eReportCard/settings/subjects_and_form/grade_settings.php", $link3);
        }
		
		$PAGE_NAVIGATION[] = array($button_edit);
		
		//echo "<div id='top'></div>";
		$linterface->LAYOUT_START();
		
		echo '<link href="'.$PATH_WRT_ROOT.'templates/2007a/css/ereportcard.css" rel="stylesheet" type="text/css" />';
?>
<style type="text/css">
<!--
#Layer1 {
	position:absolute;
	width:150px;
	height:14px;
	z-index:2;
}
.style_fail {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size:12px;
	font-weight: bold;
	color: #CC0000;
	font: Verdana, Arial, Helvetica, sans-serif;
}
.style_distinction {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size:12px;
	font-weight: bold;
	color: #5db300;
	font: Verdana, Arial, Helvetica, sans-serif;
}
.style_pass {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size:12px;
	font-weight: bold;
	color: #0063ea;
	font: Verdana, Arial, Helvetica, sans-serif;
}
-->
</style>

<!--script type="text/javascript" src="http://<?=$eclass_httppath?>/src/includes/js/ajax_yahoo.js" ></script-->
<!--script type="text/javascript" src="http://<?=$eclass_httppath?>/src/includes/js/ajax_connection.js" ></script-->
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/ajax_yahoo.js" ></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/ajax_connection.js" ></script>

<script language="javascript">

function aj_ReloadToReportSelect()
{
	var ClassLevelID = $("#copyClassLevel").val();
	var ReportID = $("#ReportID").val();
	$.post(
		"../../ajax_reload.php",
		{
			Action: "Report_Selection",
			ReportID: ReportID,
			ClassLevelID: ClassLevelID,
			SelectionID: "toReportID",
			ForGradingSchemeSettings: 1
		},
		function(ReturnData)
		{
			$("div#CopySchemeReportSelect").html(ReturnData);
			rePositionSettingDiv()
		}
	)
}

var AllSchemeIDNameMap = new Array();

<?php
	if (sizeof($AllSchemeIDNameMap) > 0) {
		foreach ($AllSchemeIDNameMap as $id => $name) {
			echo "AllSchemeIDNameMap[$id] = '$name';\n";
		}
	}
?>

Array.prototype.in_array = function(p_val) {
	for(var i = 0, l = this.length; i < l; i++) {
		if(!this[i]) continue;
		if(trim(this[i]) == trim(p_val)) {
			return true;
		}
	}
	return false;
}

function trim(stringToTrim){
	return stringToTrim.replace(/^\s+|\s+$/g,"");
}

function changeClassLevel(toFormGrading) {
	var ClassLevelID = $("select#ClassLevelID").val();
	var ReportID = $("select#ReportID").val();
	var param = '';
	
	if(!toFormGrading)
		param += '&ReportID='+ReportID;	
	window.location = 'form_settings_edit.php?ClassLevelID='+ClassLevelID+param;
}

function jCHECK_DUPLICATE_SCHEME_NAME(sc_id, ActionMode) {
	var schemeName = document.getElementById("grading_title_" + sc_id).value;
	schemeName = trim(schemeName);
	if (ActionMode == "SAVE" && schemeName ==  trim(AllSchemeIDNameMap[sc_id])) {
		return false;
	}
	
	//isDupName = AllSchemeName.in_array(schemeName);
	isDupName = AllSchemeIDNameMap.in_array(schemeName);
	
	if (!isDupName) {
		AllSchemeIDNameMap[sc_id] = schemeName;
	}
	
	return isDupName;
}

/* JS for common use */ 
 
function MM_showHideLayers() { //v9.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) 
  with (document) if (getElementById && ((obj=getElementById(args[i]))!=null)) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v=='hide')?'hidden':v; }
    obj.visibility=v; }
}

function jSET_DIV_STYLE_DISPLAY(jObjID, myStyle){
//	var curStyle = document.getElementById(jObjID).style.display;
	var curStyle = $("#"+jObjID).css("display");
	var newStyle = "none";
	
	if(typeof(myStyle) != "undefined"){
		newStyle = myStyle;
	}
	else {
		newStyle = (curStyle == "none") ? "block" : "none";
	}
	//document.getElementById(jObjID).style.display = newStyle;
	$("#"+jObjID).css("display",newStyle)
}

function jGET_RADIO_CHECKED(objName){
	if (document.getElementsByName(objName).length != 0) {
		var obj = document.getElementsByName(objName);
		var num = obj.length;
		var index = '';
		
		for(var i=0 ; i<num ; i++){
			if(obj[i].checked == true){
				index = i;
			}
		}
		return index;
	} else {
		return "";
	}
}

function jGET_RADIO_CHECKED_VALUE(objName){
	var index = jGET_RADIO_CHECKED(objName);
	var val = "";
	if(index !== "")
		val = document.getElementsByName(objName)[index].value;
		
	return val;
}

function jGET_SELECTBOX_SELECTED_VALUE(objName){
	var obj = document.getElementById(objName);
	if(obj)
	{
		var index = obj.selectedIndex;
		var val = obj[index].value;
		return val;
	}
	else
		return '';
}

function jGET_SELECT_ALL_OPTIONS(jName){
	var obj = document.getElementById(jName);
	var num = obj.length;
	var ret = '';
	for(var i=0 ; i<num ; i++){
		ret += '<option value="' + obj.options[i].value + '">' + obj.options[i].text + '</option>';
	}
	return ret;
}

function jSET_SELECTBOX_SELECTED_OPTION(objName, selVal){
	var obj = document.getElementById(objName);
	var num = obj.length;
	for(var i=0 ; i<num ; i++){
		if(obj.options[i].value == selVal)
			obj.options[i].selected = true;
	}
}

function jGET_ELEMENTS_ARRAY_INDEX(objName, jValue){
	var obj = document.getElementsByName(objName);
	var num = obj.length;
	var val = '';
	
	if(num > 0){
		for(var i=0 ; i<num ; i++){
			if(obj[i].value == jValue){
				val = i;
				continue;
			}	
		}
	}
	return val;
}

function jCHECK_FIELD(obj, msg){
	if(obj){
		if(obj.value == ""){
			alert(msg);
			return false;
		}
	}
	return true;
}

/* 
* JS for this page only 
*/

function jSET_GRADING_TYPE(sc_id){
	var index = jGET_RADIO_CHECKED("grading_type_" + sc_id);
	
	if(index == 0){
		jSET_DIV_STYLE_DISPLAY("div_honor_based_content_" + sc_id, "block");
		jSET_DIV_STYLE_DISPLAY("div_passfail_based_content_" + sc_id, "none");
	} else if(index == 1){
		jSET_DIV_STYLE_DISPLAY("div_honor_based_content_" + sc_id, "none");
		jSET_DIV_STYLE_DISPLAY("div_passfail_based_content_" + sc_id, "block");
	}
}

/*
* p_cnt: number of Pass Fields
* f_cnt: number of Fail Fields
*/
function jSET_GRADING_RANGE(sc_id){
	var index = jGET_RADIO_CHECKED("grading_range_" + sc_id);
	var p_cnt = document.getElementById("P_Cnt_" + sc_id).value;
	var f_cnt = document.getElementById("F_Cnt_" + sc_id).value;
	var d_cnt = document.getElementById("D_Cnt_" + sc_id).value;

	if(index == 0){
		$("#passing_mark_row").hide();
		
		var blockArr = new Array("div_naturetitle1_MR_",  "div_naturetitle2_MR_", "div_naturetitle3_MR_", "div_title1_MR_tips_", "div_title2_MR_tips_");
		var noneArr = new Array("div_naturetitle1_PR_", "div_naturetitle2_PR_", "div_naturetitle3_PR_", "div_title1_PR_tips_", "div_title2_PR_tips_");
		
		for(var i=0 ; i<blockArr.length ; i++){
			var str = blockArr[i] + sc_id;
			if(str != "div_title1_MR_tips_new" && str != "div_title2_MR_tips_new" && str != "div_D_MR_tips_new")
				jSET_DIV_STYLE_DISPLAY(blockArr[i] + sc_id, "block");
		}
			
		for(var i=0 ; i<noneArr.length ; i++){
			var str = noneArr[i] + sc_id;
			if(str != "div_title1_PR_tips_new" && str != "div_title2_PR_tips_new" && str != "div_D_PR_tips_new")
				jSET_DIV_STYLE_DISPLAY(noneArr[i] + sc_id, "none");
		}
		
		for(var i=0 ; i<p_cnt ; i++){
			if(document.getElementById("div_P_MR_" + sc_id + "_" + i)){
				jSET_DIV_STYLE_DISPLAY("div_P_MR_" + sc_id + "_" + i, "block");
				jSET_DIV_STYLE_DISPLAY("div_P_PR_" + sc_id + "_" + i, "none");
			}
			if(document.getElementById("div_P_MR_tips_" + sc_id + "_" + i)){
				jSET_DIV_STYLE_DISPLAY("div_P_MR_tips_" + sc_id + "_" + i, "block");
				jSET_DIV_STYLE_DISPLAY("div_P_PR_tips_" + sc_id + "_" + i, "none");
			}
		}
		for(var i=0 ; i<f_cnt ; i++){
			if(document.getElementById("div_F_MR_" + sc_id + "_" + i)){
				jSET_DIV_STYLE_DISPLAY("div_F_MR_" + sc_id + "_" + i, "block");
				jSET_DIV_STYLE_DISPLAY("div_F_PR_" + sc_id + "_" + i, "none");
			}
			if(document.getElementById("div_F_MR_tips_" + sc_id + "_" + i)){
				jSET_DIV_STYLE_DISPLAY("div_F_MR_tips_" + sc_id + "_" + i, "block");
				jSET_DIV_STYLE_DISPLAY("div_F_PR_tips_" + sc_id + "_" + i, "none");
			}
		}
		for(var i=0 ; i<d_cnt ; i++){
		
			if(document.getElementById("div_D_MR_" + sc_id + "_" + i)){
				jSET_DIV_STYLE_DISPLAY("div_D_MR_" + sc_id + "_" + i, "block");
				jSET_DIV_STYLE_DISPLAY("div_D_PR_" + sc_id + "_" + i, "none");
			}
			if(document.getElementById("div_D_MR_tips_" + sc_id + "_" + i)){
				jSET_DIV_STYLE_DISPLAY("div_D_MR_tips_" + sc_id + "_" + i, "block");
				jSET_DIV_STYLE_DISPLAY("div_D_PR_tips_" + sc_id + "_" + i, "none");
			}
		}
		
	} else if(index == 1 || index == 2){
		$("#passing_mark_row").show();
	
		var noneArr = new Array("div_naturetitle1_MR_", "div_D_MR_", "div_naturetitle2_MR_", "div_naturetitle3_MR_", "div_title1_MR_tips_", "div_title2_MR_tips_", "div_D_MR_tips_");
		var blockArr = new Array("div_naturetitle1_PR_", "div_D_PR_", "div_naturetitle2_PR_", "div_naturetitle3_PR_", "div_title1_PR_tips_", "div_title2_PR_tips_", "div_D_PR_tips_");
		
		for(var i=0 ; i<blockArr.length ; i++){
			var str = blockArr[i] + sc_id;
			if(str != "div_title1_PR_tips_new" && str != "div_title2_PR_tips_new" && str != "div_D_PR_tips_new")
				jSET_DIV_STYLE_DISPLAY(blockArr[i] + sc_id, "block");
		}
		for(var i=0 ; i<noneArr.length ; i++){
			var str = noneArr[i] + sc_id;
			if(str != "div_title1_MR_tips_new" && str != "div_title2_MR_tips_new" && str != "div_D_MR_tips_new")
				jSET_DIV_STYLE_DISPLAY(noneArr[i] + sc_id, "none");
		}
		
		for(var i=0 ; i<p_cnt ; i++){
			if(document.getElementById("div_P_MR_" + sc_id + "_" + i)){
				jSET_DIV_STYLE_DISPLAY("div_P_MR_" + sc_id + "_" + i, "none");
				jSET_DIV_STYLE_DISPLAY("div_P_PR_" + sc_id + "_" + i, "block");
			}
			if(document.getElementById("div_P_MR_tips_" + sc_id + "_" + i)){
				jSET_DIV_STYLE_DISPLAY("div_P_MR_tips_" + sc_id + "_" + i, "none");
				jSET_DIV_STYLE_DISPLAY("div_P_PR_tips_" + sc_id + "_" + i, "block");
			}
		}
		for(var i=0 ; i<f_cnt ; i++){
			if(document.getElementById("div_F_MR_" + sc_id + "_" + i)){
				jSET_DIV_STYLE_DISPLAY("div_F_MR_" + sc_id + "_" + i, "none");
				jSET_DIV_STYLE_DISPLAY("div_F_PR_" + sc_id + "_" + i, "block");
			}
			if(document.getElementById("div_F_MR_tips_" + sc_id + "_" + i)){
				jSET_DIV_STYLE_DISPLAY("div_F_MR_tips_" + sc_id + "_" + i, "none");
				jSET_DIV_STYLE_DISPLAY("div_F_PR_tips_" + sc_id + "_" + i, "block");
			}
		}
		for(var i=0 ; i<d_cnt ; i++){
			if(document.getElementById("div_D_MR_" + sc_id + "_" + i)){
				jSET_DIV_STYLE_DISPLAY("div_D_MR_" + sc_id + "_" + i, "none");
				jSET_DIV_STYLE_DISPLAY("div_D_PR_" + sc_id + "_" + i, "block");
			}
			if(document.getElementById("div_D_MR_tips_" + sc_id + "_" + i)){
				jSET_DIV_STYLE_DISPLAY("div_D_MR_tips_" + sc_id + "_" + i, "none");
				jSET_DIV_STYLE_DISPLAY("div_D_PR_tips_" + sc_id + "_" + i, "block");
			}
		}		
	}
}

/*
* jType: P-Pass , F-Fail
* sc_id: schemeID
*/
function jADD_GRADING_SCHEME_ROW(jType, sc_id){
	// jType : P: Pass, F: Fail
	var jSchemeTypeIndex = jGET_RADIO_CHECKED("grading_type_" + sc_id);
	var jSchemeRangeIndex = jGET_RADIO_CHECKED("grading_range_" + sc_id);
	var MR_displayA = "block";
	var PR_displayA = "none";
	
	var jTableBody = document.getElementById("tb_" + jType + "_" + sc_id).tBodies[0];
	
	/*
	if (jTableBody == null)
	{
		document.getElementById("tb_" + jType + "_" + sc_id).innerHTML = '<tbody></tbody>';
		var jTableBody = document.getElementById("tb_" + jType + "_" + sc_id).tBodies[0];
	}
	*/
		
	var jNewRow = jTableBody.insertRow(-1);
	var jRowNum = document.getElementById(jType + "_Cnt_" + sc_id).value;
    jIdx = sc_id + "_" + jRowNum;
    
	if(jSchemeTypeIndex == 0){
		if(jSchemeRangeIndex != 0){
			var MR_displayA = "none";
			var PR_displayA = "block";
		}
	}
	
	//alert(jSchemeTypeIndex + "=" + jSchemeRangeIndex);
	var MR_textNameA = jType + "_MR_" + jIdx + "_a";
	var PR_textNameA = jType + "_PR_" + jIdx + "_a";
    var textNameB = jType + "_" + jIdx + "_b";
    var textNameC = jType + "_" + jIdx + "_c";
    
    //alert(MR_textNameA + "| |" + PR_textNameA + "| |" + textNameB + "| |" + textNameC);
    
    jNewRow.id = "tr_" + jType + "_" + jIdx;
	
    var tmpCell0 = '';
    var jNewCell0 = jNewRow.insertCell(0);
    jNewCell0.style.cssText = "width:32%";
    tmpCell0 += "<strong><strong>";
    tmpCell0 += "<div id=\"div_" + jType + "_MR_" + sc_id + "_" + jRowNum + "\" style=\"display:" + MR_displayA + "\"><input name=\"" + MR_textNameA +"\" id=\"" + MR_textNameA +"\" type=\"text\" class=\"textboxtext\" value=\"\"></div>";
    tmpCell0 += "<div id=\"div_" + jType + "_PR_" + sc_id + "_" + jRowNum + "\" style=\"display:" + PR_displayA + "\">";
	tmpCell0 += "<span class=\"retablerow1_total tabletext\"><input name=\"" + PR_textNameA +"\" id=\"" + PR_textNameA +"\" type=\"text\" class=\"ratebox\" value=\"\" >";
	tmpCell0 += "</span><span class=\"tabletext\">%</span></div>";
    tmpCell0 += "</strong></strong>";
    jNewCell0.innerHTML = tmpCell0;
    
    var jNewCell1 = jNewRow.insertCell(1);
    jNewCell1.style.cssText = "width:31%";
	jNewCell1.innerHTML = "<strong><strong><input name=\"" + textNameB +"\" id=\"" + textNameB +"\" type=\"text\" class=\"textboxtext\" value=\"\"></strong></strong>";
    
    var jNewCell2 = jNewRow.insertCell(2);
    jNewCell2.style.cssText = "width:32%";
    jNewCell2.innerHTML = "<strong><strong><input name=\"" + textNameC +"\" id=\"" + textNameC +"\" type=\"text\" class=\"textboxtext\" value=\"\"></strong></strong>";
    
    var jNewCell3 = jNewRow.insertCell(3);
	jNewCell3.style.cssText = "width:8px";
    jNewCell3.innerHTML = "<a href=\"javascript:jREMOVE_GRADING_SCHEME_ROW('" + jType + "', '" + sc_id + "', '" + jRowNum + "')\" class=\"contenttool\">X</a>";
    
    document.getElementById(jType + "_Cnt_" + sc_id).value = parseInt(jRowNum) + 1;
    //alert(document.getElementById(jType + "_Cnt_" + sc_id).value);
}

function jADD_GRADING_SCHEME_ROW1(jType, sc_id){
	// jType : P: Pass, F: Fail, D: Distinct
	var jSchemeTypeIndex = jGET_RADIO_CHECKED("grading_type_" + sc_id);
	var jSchemeRangeIndex = jGET_RADIO_CHECKED("grading_range_" + sc_id);
	var MR_displayA = "block";
	var PR_displayA = "none";
	
	var jTableBody = document.getElementById("tb_" + jType + "_" + sc_id).tBodies[0];
	//alert($("#tb_" + jType + "_" + sc_id).length);
	//var jTableBody = $("#tb_" + jType + "_" + sc_id).get().tBodies[0];
	
	/*
	if (jTableBody == null)
	{
		document.getElementById("tb_" + jType + "_" + sc_id).innerHTML = '<tbody></tbody>';
		var jTableBody = document.getElementById("tb_" + jType + "_" + sc_id).tBodies[0];
	}
	*/
		
	var jNewRow = jTableBody.insertRow(-1);

	//var jRowNum = document.getElementById(jType + "_Cnt_" + sc_id).value;
	var jRowNum = $("#" + jType + "_Cnt_" + sc_id).val();
    jIdx = sc_id + "_" + jRowNum;
    
	if(jSchemeTypeIndex == 0){
		if(jSchemeRangeIndex != 0){
			var MR_displayA = "none";
			var PR_displayA = "block";
		}
	}
	
	//alert(jSchemeTypeIndex + "=" + jSchemeRangeIndex);
	var MR_textNameA = jType + "_MR_" + jIdx + "_a";
	var PR_textNameA = jType + "_PR_" + jIdx + "_a";
    var textNameB = jType + "_" + jIdx + "_b";
    var textNameC = jType + "_" + jIdx + "_c";
    var textNameD = jType + "_" + jIdx + "_d";
    var textNameE = jType + "_" + jIdx + "_e";
    
    
    //alert(MR_textNameA + "| |" + PR_textNameA + "| |" + textNameB + "| |" + textNameC);
    
    jNewRow.id = "tr_" + jType + "_" + jIdx;
	
    var tmpCell0 = '';
    var jNewCell0 = jNewRow.insertCell(0);
    jNewCell0.style.cssText = "width:50px";
    tmpCell0 += "<strong><strong>";
    tmpCell0 += "<div id=\"div_" + jType + "_MR_" + sc_id + "_" + jRowNum + "\" style=\"display:" + MR_displayA + "\"><input style=\"width:50px;\" name=\"" + MR_textNameA +"\" id=\"" + MR_textNameA +"\" type=\"text\" class=\"textboxtext\" value=\"\"></div>";
    tmpCell0 += "<div id=\"div_" + jType + "_PR_" + sc_id + "_" + jRowNum + "\" style=\"display:" + PR_displayA + "\">";
	tmpCell0 += "<span class=\"retablerow1_total tabletext\"><input style=\"width:50px;\" name=\"" + PR_textNameA +"\" id=\"" + PR_textNameA +"\" type=\"text\" class=\"ratebox\" value=\"\" >";
	tmpCell0 += "</span><span class=\"tabletext\">%</span></div>";
    tmpCell0 += "</strong></strong>";
    jNewCell0.innerHTML = tmpCell0;
    
    var jNewCell1 = jNewRow.insertCell(1);
    jNewCell1.style.cssText = "width:50px";
	jNewCell1.innerHTML = "<strong><strong><input style=\"width:50px;\" name=\"" + textNameB +"\" id=\"" + textNameB +"\" type=\"text\" class=\"textboxtext\" value=\"\"></strong></strong>";
    
    var jNewCell2 = jNewRow.insertCell(2);
    jNewCell2.style.cssText = "width:50px";
    jNewCell2.innerHTML = "<strong><strong><input style=\"width:50px;\" name=\"" + textNameC +"\" id=\"" + textNameC +"\" type=\"text\" class=\"textboxtext\" value=\"\"></strong></strong>";
    
    var CellHTMLArr = new Array();
	var CssTextArr = new Array();
	
	if( <?=$hasAdditionalCriteria== true?"true":"false"?>)
	{
		CellHTMLArr.push("<span class=\"retablerow1_total tabletext\"><strong><strong><input style=\"width:60px;\" name=\"" + textNameD +"\" id=\"" + textNameD +"\" type=\"text\" class=\"ratebox\" value=\"0\"></span><span class=\"tabletext\">%</span></strong></strong>");
		CssTextArr.push("width:60px");
	}
	if( <?=$eRCTemplateSetting['GradingSchemeGradeDescription'] == true?"true":"false"?>)
	{
		CellHTMLArr.push("<span class=\"retablerow1_total tabletext\"><strong><strong><textarea style=\"width:120px;\" name=\"" + textNameE +"\" id=\"" + textNameE +"\" class=\"textboxtext\" ></textarea></span></strong></strong>");
		CssTextArr.push("width:120px");
	}
	CellHTMLArr.push("<a href=\"javascript:jREMOVE_GRADING_SCHEME_ROW1('" + jType + "', '" + sc_id + "', '" + jRowNum + "')\" class=\"contenttool\">X</a>");
	CssTextArr.push("width:8px");
	
	for(var i = 0; i < CellHTMLArr.length; i++)
	{
		 var thisCell = jNewRow.insertCell(3+i);
		 thisCell.style.cssText = CssTextArr[i];
		 thisCell.innerHTML = CellHTMLArr[i];
	}
//    if( <?=$hasAdditionalCriteria== true?"true":"false"?>)
//    {
//	    var jNewCell3 = jNewRow.insertCell(3);
//	    jNewCell3.style.cssText = "width:25%";
//	    jNewCell3.innerHTML = "<span class=\"retablerow1_total tabletext\"><strong><strong><input name=\"" + textNameD +"\" id=\"" + textNameD +"\" type=\"text\" class=\"ratebox\" value=\"0\"></span><span class=\"tabletext\">%</span></strong></strong>";
//	
//	    var jNewCell4 = jNewRow.insertCell(4);
//		jNewCell4.style.cssText = "width:8px";
//	    jNewCell4.innerHTML = "<a href=\"javascript:jREMOVE_GRADING_SCHEME_ROW1('" + jType + "', '" + sc_id + "', '" + jRowNum + "')\" class=\"contenttool\">X</a>";
//	}
//	else
//	{
//	    var jNewCell3 = jNewRow.insertCell(3);
//		jNewCell3.style.cssText = "width:8px";
//	    jNewCell3.innerHTML = "<a href=\"javascript:jREMOVE_GRADING_SCHEME_ROW1('" + jType + "', '" + sc_id + "', '" + jRowNum + "')\" class=\"contenttool\">X</a>";
//	}
    
    //document.getElementById(jType + "_Cnt_" + sc_id).value = parseInt(jRowNum) + 1;
	$("#" + jType + "_Cnt_" + sc_id).val(parseInt(jRowNum) + 1);
    //alert(document.getElementById(jType + "_Cnt_" + sc_id).value);
}

function jREMOVE_GRADING_SCHEME_ROW(jType, sc_id, jRowNum){
	jIdx = sc_id + "_" + jRowNum; 
	document.getElementById("tb_" + jType + "_" + sc_id).tBodies[0].removeChild(document.getElementById("tr_" + jType + "_" + jIdx));
}

function jREMOVE_GRADING_SCHEME_ROW1(jType, sc_id, jRowNum){
	jIdx = sc_id + "_" + jRowNum; 
	//document.getElementById("tb_" + jType + "_" + sc_id).tBodies[0].removeChild(document.getElementById("tr_" + jType + "_" + jIdx));
	$("#tr_" + jType + "_" + jIdx).remove();
}
/*
* jType : 0-scheme , 1-scheme details
* sc_id : schemeID
*/
function jSHOW_GRADING_SCHEME(obj, s_id, jType)
{
	var selName = "select_scheme_" + s_id;
	var cur_id = jGET_SELECTBOX_SELECTED_VALUE(selName);
	
	if(jType == 0){
		var objName = 'scheme_' + cur_id;
		var offsetL = -10;
	} else {
		var objName = 'scheme_dtl_';
		
		if(cur_id == "")
			objName += 'new';
		else 
			objName += cur_id;
		var offsetL = 10;
	}
	
	if(jType == 1 || (jType == 0 && cur_id !="") ){
		document.getElementById(objName).style.left = getPostion(obj, 'offsetLeft') + offsetL + 'px';
	 	document.getElementById(objName).style.top = getPostion(obj, 'offsetTop') + 10 + 'px';
			
		MM_showHideLayers(objName,'','show');
	}
}

var CurrentEditingSelection = '';
function jSHOW_GRADING_SCHEME1(obj, s_id, jType)
{
	var selName = "select_scheme_" + s_id;
	//CurrentEditingSelection = selName;
	var cur_id = jGET_SELECTBOX_SELECTED_VALUE(selName);
	
	if(jType == 0){
		//var objName = 'scheme_' + cur_id;
		var	objName = 'scheme_box';
		var offsetL = -10;
		var TaskStr = "View"
	} else {
		// update only when editing grading scheme
		CurrentEditingSelection = selName;
		
		//var objName = 'scheme_dtl_';
		var	objName = 'scheme_dtl_box';
		var TaskStr = "Edit"
		
		/*if(cur_id == "")
			objName += 'new';
		else 
			objName += cur_id;*/
		var offsetL = 10;
	}
	
	if(jType == 1 || (jType == 0 && cur_id !="") ){
		document.getElementById(objName).style.left = getPostion(obj, 'offsetLeft') + offsetL + 'px';
		document.getElementById(objName).style.top = getPostion(obj, 'offsetTop') + 10 + 'px';
		document.getElementById(objName).innerHTML = '<table border="0" cellpadding="0" cellspacing="0" bgcolor="#feffed"><tr><td>loading...</td></tr></table>';
		MM_showHideLayers(objName,'','show');
		
		$.post("ajax_get_grading_scheme.php", { schemeID: cur_id, Task: TaskStr },
			function(data){
				document.getElementById(objName).innerHTML = data;
				if(cur_id=='')
				{
					jADD_GRADING_SCHEME_ROW1('D','new');
					jADD_GRADING_SCHEME_ROW1('P','new');
					jADD_GRADING_SCHEME_ROW1('F','new');
				}
				//jCHECK_GRADING_SCHEME_ROW();
				
			}
		);
		
		
	}
}

function jHIDE_GRADING_SCHEME(s_id, sc_id, jType){
	if(jType == 0){
		var selName = "select_scheme_" + s_id;
		var cur_id = jGET_SELECTBOX_SELECTED_VALUE(selName);
		var objName = 'scheme_' + cur_id;
	} else {
		var objName = 'scheme_dtl_' + sc_id;
	}
	
	if(cur_id != ""){
		MM_showHideLayers(objName,'','hide');
	}
}

function jHIDE_GRADING_SCHEME1(s_id, sc_id, jType){
	
	if(jType == 0){
		var objName = 'scheme_box';
	} else {
		var objName = 'scheme_dtl_box';
	}
	
	MM_showHideLayers(objName,'','hide');
}

function jCHANGE_GRADING_METHOD(s_id){
	var selName = "select_scheme_" + s_id;
	var sc_id = jGET_SELECTBOX_SELECTED_VALUE(selName);
	//var sc_type = (sc_id != "") ? jGET_RADIO_CHECKED_VALUE("grading_type_" + sc_id) : "";
	var sc_type = "";
	if (sc_id != "")
		sc_type = jsGradingTypeArr[sc_id];
	
	if(sc_type == "PF"){
		jSET_DIV_STYLE_DISPLAY('div_scale_honor_' + s_id, 'none');
		jSET_DIV_STYLE_DISPLAY('div_scale_passfail_' + s_id, 'block');
		jSET_DIV_STYLE_DISPLAY('div_display_honor_' + s_id, 'none');
		jSET_DIV_STYLE_DISPLAY('div_display_passfail_' + s_id, 'block');
	}
	else {
		jSET_DIV_STYLE_DISPLAY('div_scale_honor_' + s_id, 'block');
		jSET_DIV_STYLE_DISPLAY('div_scale_passfail_' + s_id, 'none');
		jSET_DIV_STYLE_DISPLAY('div_display_honor_' + s_id, 'block');
		jSET_DIV_STYLE_DISPLAY('div_display_passfail_' + s_id, 'none');
	}
}

/*
* Use for Table
*/

function jGET_TABLE_ACTION_INDEX(obj, jType){
	var tb = document.getElementById("tb_Form_Settings");
	var num = tb.rows.length;
	var val = "";
	var jName = (jType == 1) ? "move_up[]" : "move_down[]";
	
	for(var i=0 ; i<num ; i++){
		if(document.getElementsByName(jName)[i] == obj)	{
			val = i;
			continue;
		}
	}
	return val;
}

function jGET_TALBE_ROW_SUBJECT_SCHEME_INFO(s_id){
	var sc_id = jGET_SELECTBOX_SELECTED_VALUE("select_scheme_" + s_id);
	
	// Subject Info
	var sub_name = document.getElementById("subject_name_" + s_id).value;
	var sub_order = document.getElementById("display_order_" + s_id).value;
	var is_cmpSub = document.getElementById("cmpSubjectFlag_" + s_id).value;
	var parent_sub_id = document.getElementById("parent_subject_id_" + s_id).value;	
	var parent_index = (is_cmpSub == 0) ? jGET_ELEMENTS_ARRAY_INDEX("parentSubject[]", s_id) : "";
	var cmp_index = (is_cmpSub == 1) ? jGET_ELEMENTS_ARRAY_INDEX("cmpSubject"+parent_sub_id+"[]", s_id) : "";
	
	// Scheme Info
	//var sc_type = (sc_id != "") ? jGET_RADIO_CHECKED_VALUE("grading_type_" + sc_id) : "";
	var sc_type = "";
	if (sc_id != "")
		sc_type = jsGradingTypeArr[sc_id];
	
	var sc_scale_h = jGET_RADIO_CHECKED_VALUE("scale_" + s_id);
	var sc_scale_pf = '--';
	var sc_display_h = jGET_RADIO_CHECKED_VALUE("display_" + s_id);
	var sc_display_pf = '--';
	<?if($eRCTemplateSetting['SelectSubjectDisplayLang']==true){?>
		var sc_langdisplay = jGET_SELECTBOX_SELECTED_VALUE("LangDisplay_" + s_id);
	<?}else{?>
		var sc_langdisplay = '';
	<?}?>
		<?if($eRCTemplateSetting['SelectDisplaySubjectGroup']==true){?>
		var sc_displaysubjgroup = jGET_RADIO_CHECKED_VALUE("Display_SubjGroup_" + s_id);
	<?}else{?>
		var sc_displaysubjgroup = '';
	<?}?>
	
	//alert(sc_id + " | " + sub_name + " | " + sub_order + " | " + is_cmpSub + " | " + parent_sub_id);
	
	var info = new Array(12);
	info['sc_id'] = sc_id;
	info['sub_name'] = sub_name;
	info['sub_order'] = sub_order;
	info['is_cmpSub'] = is_cmpSub;
	info['parent_sub_id'] = parent_sub_id;
	info['parent_index'] = parent_index;
	info['cmp_index'] = cmp_index;
	info['sc_type'] = sc_type;
	info['sc_scale_h'] = sc_scale_h;
	info['sc_scale_pf'] = sc_scale_pf;
	info['sc_display_h'] = sc_display_h;
	info['sc_display_pf'] = sc_display_pf;	
	info['sc_langdisplay'] = sc_langdisplay;
	info['sc_displaysubjgroup'] = sc_displaysubjgroup;	
	
	
	return info;
}

function jSET_STYLE_OF_TABLE_ROWS(s_id, rowIndex, newIndex){
	// Reset the style of affected row
	var num = document.getElementsByName('parentSubject[]').length;
	document.getElementById("td_select_" + s_id).className = (rowIndex % 2 == 1) ? "retablerow1" : "retablerow2";
    document.getElementById("td_scale_" + s_id).className = (rowIndex % 2 == 1) ? "retablerow1" : "retablerow2";
    document.getElementById("td_display_" + s_id).className = (rowIndex % 2 == 1) ? "retablerow1" : "retablerow2";
    <?if($eRCTemplateSetting['SelectSubjectDisplayLang']==true){?>
    	document.getElementById("td_langdisplay_" + s_id).className = (rowIndex % 2 == 1) ? "retablerow1" : "retablerow2";
    <?}?>
    <?if($eRCTemplateSetting['SelectDisplaySubjectGroup']==true){?>
    	document.getElementById("td_Display_SubjGroup_" + s_id).className = (rowIndex % 2 == 1) ? "retablerow1" : "retablerow2";
    	
    <?}?>
    document.getElementById("div_up_" + s_id).style.display = (rowIndex != 1) ? "block" : "none";
    document.getElementById("div_down_" + s_id).style.display = (rowIndex == subjectArr.length || newIndex == (num-1)) ? "none" : "block";
}

function jSET_STYLE_OF_TABLE_SUB_ROWS(p_id, s_id, index){
	// Reset the style of affected row
	var num = document.getElementsByName("cmpSubject"+p_id+"[]").length;
	
	document.getElementById("div_up_" + s_id).style.display = (index == 0) ? "none" : "block";
    document.getElementById("div_down_" + s_id).style.display = (index == (num-1)) ? "none" : "block";
    document.getElementById("td_head_" + s_id).className = (index == (num-1)) ? "tablelist_subject" : "tablelist_subject_head";
}

/*
* Move UP or DOWN from the Table Rows
* Work Flow: [ Moving Up ]
* 	Parent Subject will move up by (-1) in table row 
* 	if having any sub-subjects, after moving the parent subject, 
* 	all sub-subjects will move up one by one starting from the UPPEREST one by a value (integer 1)
*
* Work Flow: [ Moving Down ]
* 	Parent Subject will move down by (+1) in table row if it does not contain any sub-subjects
* 	if having any sub-subjects, parent subject must not move first, 
* 	all sub-subjects will move down one by one starting from the LOWEREST one by a value (number of sub-subjects plus 1)
*/
function jMOVE_TABLE_ROW(obj, jType, s_id){
	//jLOOP();
	var info = new Array(12);
	
	info = jGET_TALBE_ROW_SUBJECT_SCHEME_INFO(s_id);
	var seloption = jGET_SELECT_ALL_OPTIONS("scheme_template");
	var seloption2 = (info['sc_langdisplay']!='')?jGET_SELECT_ALL_OPTIONS("LangDisplay_"+s_id):'';
	var index = jGET_TABLE_ACTION_INDEX(obj, jType);
	var oldRowIndex = index;
	
	// Parent Subject 
	var parentNum = document.getElementsByName("parentSubject[]").length;
	
	// Check Any Sub Subject 
	var cmpNum = 0;
	if(info['is_cmpSub'] == 0)
		var cmpObj = (document.getElementsByName("cmpSubject"+s_id+"[]")) ? document.getElementsByName("cmpSubject"+s_id+"[]") : new Array(0);
	else 
		var cmpObj = document.getElementsByName("cmpSubject"+info['parent_sub_id']+"[]");
	cmpNum = cmpObj.length;
	
	var jNum = (info['is_cmpSub'] == 0) ? parentNum : cmpNum;
	var jFlag = (info['is_cmpSub'] == 0 && cmpNum > 0) ? 1 : 0;		// flag for moving down with having sub-subjects
	
	if(jType == 1){
		var newRowIndex = parseInt(oldRowIndex) - 1;
		var newIndex = parseInt((info['is_cmpSub'] == 0) ? info['parent_index'] : info['cmp_index']) - 1;
	}
	else if(jType == '-1'){
		if(jFlag == 0)
			var newRowIndex = parseInt(oldRowIndex) + 1;
		else 
			var newRowIndex = parseInt(oldRowIndex) + parseInt(cmpNum) + 1;
		
		var newIndex = parseInt((info['is_cmpSub'] == 0) ? info['parent_index'] : info['cmp_index']) + 1;
	}
	
	var moveRowIndex = oldRowIndex;
	var moveIndex = (info['is_cmpSub'] == 0) ? info['parent_index'] : info['cmp_index'];
	var moveRowSubjectID = document.getElementById("subjectID_" + newRowIndex).value;		// newly added
	
	/* check case start before exhange order */
	if(jType == 1){
		if(info['is_cmpSub'] == 0){
			// case ii
			// case iv-a
			var tmpInfo = new Array(12);
			tmpInfo = jGET_TALBE_ROW_SUBJECT_SCHEME_INFO(moveRowSubjectID);
		
			if(tmpInfo['is_cmpSub'] == 1){		// it is sub-subjects
				var tmpObj = document.getElementsByName("cmpSubject"+tmpInfo['parent_sub_id']+"[]");
				var tmpNum = tmpObj.length;
				for(var i=0 ; i<tmpNum ; i++){	
					var moveObj = document.getElementById("move_up" + s_id);	
					jMOVE_TABLE_NON_SUB_ROW(moveObj, jType, s_id);
				}
				
				// Re-allocate all the Index Old and New Index before exhange order
				var newRowIndex = parseInt(newRowIndex) - parseInt(tmpNum);
				var moveRowSubjectID = document.getElementById("subjectID_" + newRowIndex).value;
				var oldRowIndex = parseInt(newRowIndex) + 1;
				var moveRowIndex = oldRowIndex;
			}
		}
	}
	else if(jType == '-1'){
		if(info['is_cmpSub'] == 0){
			if(cmpNum > 0){
				// vii  + viii-a (both parent with sub-subjects)
				// This case need jFlag to helps to find the next parent subjects rowIndex
				var affInfo = new Array(12);
				affInfo = jGET_TALBE_ROW_SUBJECT_SCHEME_INFO(moveRowSubjectID);
				for(var i=(cmpNum-1) ; i>=0 ; i--){
					var moveObj = document.getElementById("move_down"+cmpObj[i].value);
					
					jMOVE_TABLE_SUB_ROW(moveObj, jType, cmpObj[i].value);			// A A1 B A2
					
					// case for affected subjects with sub-subjects
					if(document.getElementsByName("cmpSubject"+moveRowSubjectID+"[]") && document.getElementsByName("cmpSubject"+moveRowSubjectID+"[]").length >0){
						var affObj = document.getElementsByName("cmpSubject"+moveRowSubjectID+"[]");
						var affNum = affObj.length;
						for(var j=0; j<affNum ; j++){
							var moveObj = document.getElementById("move_down"+cmpObj[i].value);
							jMOVE_TABLE_SUB_ROW(moveObj, jType, cmpObj[i].value);	// A A1 B B1 B2 A2 --> A B B1 B2 A1 A2
						}
					}
				}
				
				
				// Re-allocate all the Index Old and New Index before exhange order
				var newRowIndex = parseInt(newRowIndex) - parseInt(cmpNum);
				var oldRowIndex = parseInt(newRowIndex) - 1;
				
				var moveRowIndex = oldRowIndex;
			}
		}
	}
	
	// remove the selected row
	jREMOVE_ROW(oldRowIndex);
	
	// Get the info of the subject which is affected
    //var moveRowSubjectID = document.getElementById("subjectID_" + newRowIndex).value;
    var newRowDisOrder = document.getElementById("display_order_" + moveRowSubjectID).value; 
    var moveID = (info['is_cmpSub'] == 0) ? document.getElementById("parentSubject_" + newIndex).value : document.getElementById("cmpSubject"+info['parent_sub_id']+"_" + newIndex).value;
    
    // Update the info of the subject which is affected
    document.getElementById("display_order_" + moveRowSubjectID).value = info['sub_order'];
    document.getElementById("subjectID_" + newRowIndex).id = "subjectID_" + moveRowIndex;
    
    //alert(s_id + ": " + newRowDisOrder);
    //alert(moveRowSubjectID + ": " + info['sub_order']);
    
    if(info['is_cmpSub'] == 0){
    	document.getElementById("parentSubject_" + moveIndex).value = moveID;
    	document.getElementById("parentSubject_" + newIndex).value = s_id;
    	
    	var tmpInfo = new Array(12);
		tmpInfo = jGET_TALBE_ROW_SUBJECT_SCHEME_INFO(moveRowSubjectID);
		if(tmpInfo['is_cmpSub'] == 0)
	    	jSET_STYLE_OF_TABLE_ROWS(moveRowSubjectID, moveRowIndex, tmpInfo['parent_index']);
	} else {
    	document.getElementById("cmpSubject"+info['parent_sub_id']+"_" + moveIndex).value = moveID;
    	document.getElementById("cmpSubject"+info['parent_sub_id']+"_" + newIndex).value = s_id;
    	
    	jSET_STYLE_OF_TABLE_SUB_ROWS(info['parent_sub_id'], moveRowSubjectID, moveIndex);
	}
	
    
	// prepare subject scheme info
	var tmp0 = '<table border="0" cellpadding="2" cellspacing="0">';
  	tmp0 += '<tr><td><div id="div_up_' + s_id + '" style="display:' + ((newIndex == 0 || newRowIndex == 1) ? 'none' : 'block') +'"><a name="move_up[]" id="move_up' + s_id + '" href="javascript:void(0)" class="contenttool" onclick="jMOVE_TABLE_ROW(this, \'1\', \'' + s_id + '\')"><img src="<?=$image_path.'/'.$LAYOUT_SKIN?>/icon_sort_a_off.gif" width="13" height="13" border="0" alt="<?=$eReportCard['MoveUp']?>"></a></div></td></tr>';
  	tmp0 += '<tr><td><div id="div_down_' + s_id + '" style="display:' + ((newIndex == (parseInt(jNum)-1) || (newRowIndex == subjectArr.length && info['is_cmpSub'] == 0)) ? 'none' : 'block') + '"><a name="move_down[]" id="move_down' + s_id + '" href="javascript:void(0)" class="contenttool" onclick="jMOVE_TABLE_ROW(this, \'-1\', \'' + s_id + '\')"><img src="<?=$image_path.'/'.$LAYOUT_SKIN?>/icon_sort_d_off.gif" width="13" height="13" border="0" alt="<?=$eReportCard['MoveDown']?>"></a></div></td></tr>';
	tmp0 += '</table>';
	
	var tmp1 = '';
	tmp1 += info['sub_name'];
	tmp1 += '<input type="hidden" name="subject_name_' + s_id + '" id="subject_name_' + s_id + '" value="' + info['sub_name'] + '"/>';
	tmp1 += '<input type="hidden" name="display_order_' + s_id + '" id="display_order_' + s_id + '" value="' + newRowDisOrder + '"/>';
    tmp1 += '<input type="hidden" name="cmpSubjectFlag_' + s_id + '" id="cmpSubjectFlag_' + s_id + '" value="' + info['is_cmpSub'] + '"/>';
    tmp1 += '<input type="hidden" name="parent_subject_id_' + s_id + '" id="parent_subject_id_' + s_id + '" value="' + info['parent_sub_id'] + '"/>';
    tmp1 += '<input type="hidden" name="subjectID[]" id="subjectID_' + newRowIndex + '" value="' + s_id + '"/>';
	
	var tmp2 = '<table border="0" cellpadding="0" cellspacing="0"><tr>';
	tmp2 += '<td><select id="select_scheme_' + s_id + '" name="select_scheme_' + s_id + '" class="tabletexttoptext scheme_selection" onMouseOver="jSHOW_GRADING_SCHEME1(document.getElementById(\'img_edit_' + s_id + '\'), \'' + s_id + '\', \'0\')"  onMouseOut="jHIDE_GRADING_SCHEME1(\'' + s_id + '\', \'\', \'0\')" onchange="jCHANGE_GRADING_METHOD(\'' + s_id + '\')">' + seloption + '</select></td>';
	tmp2 += '<td><a href="javascript:jSHOW_GRADING_SCHEME1(document.getElementById(\'img_edit_' + s_id + '\'), \'' + s_id + '\', \'1\')"><img id="img_edit_' + s_id + '" src="<?=$image_path.'/'.$LAYOUT_SKIN?>/icon_edit_b.gif" width="20" height="20" border="0" alt="<?=$button_edit.' '.$eReportCard['GradingScheme']?>"></a></td>';
	tmp2 += '</tr></table>';
	
	var tmp3 = '<span class="tabletext">';
	tmp3 += '<div id="div_scale_honor_' + s_id + '" style="display:' + ((info['sc_type'] == 'PF') ? 'none' : 'block') + '">';
  	tmp3 += '<input type="radio" name="scale_' + s_id + '" id="scale_' + s_id + '_2" class="input_scale_mark" value="M" ' + (info['sc_scale_h'] == 'M' ? 'checked' : '') +'><label for="scale_' + s_id + '_2"><?=$eReportCard['Mark']?></label> &nbsp;&nbsp;';
    tmp3 += '<input type="radio" name="scale_' + s_id + '" id="scale_' + s_id + '_1" class="input_scale_grade" value="G" ' + (info['sc_scale_h'] != 'M' ? 'checked' : '') +'><label for="scale_' + s_id + '_1"><?=$eReportCard['Grade']?></label>';
    tmp3 += '</div>';
    tmp3 += '<div id="div_scale_passfail_' + s_id + '" style="display:' + ((info['sc_type'] == 'PF') ? 'block' : 'none') + '">' + info['sc_scale_pf'] + '</div>';
    tmp3 += '</span>';
    
    var tmp4 = '<span class="tabletext">';
    tmp4 += '<div id="div_display_honor_' + s_id + '" style="display:' + ((info['sc_type'] == 'PF') ? 'none' : 'block') + '">';
    tmp4 += '<input type="radio" name="display_' + s_id + '" id="display_' + s_id + '_2" class="display_scale_mark" value="M" ' + (info['sc_display_h'] == 'M' ? 'checked' : '') +'><label for="display_' + s_id + '_2"><?=$eReportCard['Mark']?></label> &nbsp;&nbsp;';
    tmp4 += '<input type="radio" name="display_' + s_id + '" id="display_' + s_id + '_1" class="display_scale_grade" value="G" ' + (info['sc_display_h'] != 'M' ? 'checked' : '') +'><label for="display_' + s_id + '_1"><?=$eReportCard['Grade']?></label>';
    tmp4 += '</div>';
    tmp4 += '<div id="div_display_passfail_' + s_id + '" style="display:' + ((info['sc_type'] == 'PF') ? 'block' : 'none') + '">' + info['sc_display_pf'] + '</div>';
    tmp4 += '</span>';    	
	
	if(seloption2!='')
    {
	    var tmp5 = '<span class="tabletext">';
	    tmp5 += '<select name="LangDisplay_'+ s_id +'" id="LangDisplay_'+ s_id +'" class="tabletexttoptext lang_display_selection">'+ seloption2 +'</select>';
	    tmp5 += '</span>';    	
    }
	
	if(info['sc_displaysubjgroup'])
	{
		var tmp6 = '<span class="tabletext">';
	    tmp6 += '<div id="div_Display_SubjGroup_' + s_id + '" >';
	    tmp6 += '<input type="radio" name="Display_SubjGroup_' + s_id + '" id="Display_SubjGroup_' + s_id + '_2" class="display_subject_group_yes" value="Y" ' + (info['sc_displaysubjgroup'] == 'Y' ? 'checked' : '') +'><label for="Display_SubjGroup_' + s_id + '_2"><?=$Lang['General']['Yes']?></label> &nbsp;&nbsp;';
	    tmp6 += '<input type="radio" name="Display_SubjGroup_' + s_id + '" id="Display_SubjGroup_' + s_id + '_1" class="display_subject_group_no" value="N" ' + (info['sc_displaysubjgroup'] == 'N' ? 'checked' : '') +'><label for="Display_SubjGroup_' + s_id + '_1"><?=$Lang['General']['No']?></label>';
	    tmp6 += '</div>';
	    tmp6 += '</span>';    
	    
	}

    // add sub-subject which is selected
	jADD_TABLE_ROW(s_id, newRowIndex, newIndex, info, tmp0, tmp1, tmp2, tmp3, tmp4, tmp5, tmp6, jNum);
 
	// restore value of SchemeID
	jSET_SELECTBOX_SELECTED_OPTION("select_scheme_" + s_id, info['sc_id']);
	console.log("select_scheme_" + s_id)
	console.log(info['sc_id'])
	if(seloption2!='')
    {
		jSET_SELECTBOX_SELECTED_OPTION("LangDisplay_" + s_id, info['sc_langdisplay']);
    }
	
	/*********************/
	/* Check Cases start */
	// get info of affected subjectID
	var tmpInfo = new Array(12);
	tmpInfo = jGET_TALBE_ROW_SUBJECT_SCHEME_INFO(moveRowSubjectID);
	
	if(jType == 1){					// move up
		if(info['is_cmpSub'] == 0){	// Parent Subject
			if(cmpNum > 0){			// Parent with Sub-Subjects
				if(tmpInfo['is_cmpSub'] == 1){
					// nth - done before 
				}
				else if(tmpInfo['is_cmpSub'] == 0){		// affected Subject without sub-subjects
					// iii  (affected subject without sub-subjects)
					// iv-b (affected subjects with sub-subjects)
					var tmpObj = document.getElementsByName("cmpSubject"+s_id+"[]");
					var tmpNum = tmpObj.length;
					
					for(var i=0 ; i<tmpNum ; i++){	
						var affObj = document.getElementsByName("cmpSubject"+moveRowSubjectID+"[]");
						var affNum = affObj.length;
						
						for(var j=0; j<affNum ; j++){
							var moveObj = document.getElementById("move_up" + tmpObj[i].value);
							jMOVE_TABLE_SUB_ROW(moveObj, jType, tmpObj[i].value);
						}
						var moveObj = document.getElementById("move_up" + tmpObj[i].value);
						jMOVE_TABLE_SUB_ROW(moveObj, jType, tmpObj[i].value);	// B < A < A1 < A2 < B1 < B2
					}
				}
			}
		}
	}
	else if(jType == '-1'){			// move down
		if(info['is_cmpSub'] == 0){ // Parent Subject
			if(cmpNum > 0){			// Parent with Sub-Subjects
				// case viii-b
				if(document.getElementsByName("cmpSubject"+moveRowSubjectID+"[]")){		
					var tmpObj = document.getElementsByName("cmpSubject"+moveRowSubjectID+"[]");
					var tmpNum = tmpObj.length;
					for(var i=0 ; i<tmpNum ; i++){
						var moveObj = document.getElementById("move_down" + s_id);
						jMOVE_TABLE_NON_SUB_ROW(moveObj, jType, s_id);		// B A B1 B2 A1 A2 --> B B1 B2 A A1 A2
					}
				}
			}
			else if(cmpNum == 0){	// Parent without Sub-Subjects
				// case vi
				if(document.getElementsByName("cmpSubject"+moveRowSubjectID+"[]")){
					var tmpObj = document.getElementsByName("cmpSubject"+moveRowSubjectID+"[]");
					var tmpNum = tmpObj.length;
					for(var i=0 ; i<tmpNum ; i++){	
						var moveObj = document.getElementById("move_down" + s_id);
						jMOVE_TABLE_NON_SUB_ROW(moveObj, jType, s_id);		// B A B1
					}
				}
			}
		}
	}
	
}

function jLOOP(){
	var num = document.getElementsByName("subjectID[]").length;
	var str = '';
	for(var i=0 ; i<num ; i++){
		str += ": " + document.getElementsByName("subjectID[]")[i].value;
	}
	//alert(str);
}

function jMOVE_TABLE_SUB_ROW(obj, jType, s_id){		// move up or down the su-subject row cross non-sub-subject row without exchange display order
	//jLOOP();
	var info = new Array(12);
	
	info = jGET_TALBE_ROW_SUBJECT_SCHEME_INFO(s_id);
	var seloption = jGET_SELECT_ALL_OPTIONS("scheme_template");
	
	var seloption2 = (info["sc_langdisplay"]!='')?jGET_SELECT_ALL_OPTIONS("LangDisplay"+s_id):'';
	var index = jGET_TABLE_ACTION_INDEX(obj, jType);
	
	var oldRowIndex = index;
	
	// Parent Subject 
	//var parentNum = document.getElementsByName("parentSubject[]").length;
	
	var cmpNum = 0;
	var cmpObj = document.getElementsByName("cmpSubject"+info['parent_sub_id']+"[]");
	cmpNum = cmpObj.length;
	
	//var jNum = (info['is_cmpSub'] == 0) ? parentNum : cmpNum;
	
	if(jType == 1){
		var newRowIndex = parseInt(oldRowIndex) - 1;
		//var newIndex = parseInt((info['is_cmpSub'] == 0) ? info['parent_index'] : info['cmp_index']) - 1;
	}
	else if(jType == '-1'){
		var newRowIndex = parseInt(oldRowIndex) + 1;
		//var newIndex = parseInt((info['is_cmpSub'] == 0) ? info['parent_index'] : info['cmp_index']) + 1;
	}
	var moveRowIndex = oldRowIndex;

	// remove the sub subject which is selected
	jREMOVE_ROW(oldRowIndex);
	
	// Get the info [Except Display Order] of the subject which is affected
    var moveRowSubjectID = document.getElementById("subjectID_" + newRowIndex).value;
    
    // Update the info [Except Display Order] of the subject which is affected
    document.getElementById("subjectID_" + newRowIndex).id = "subjectID_" + moveRowIndex;
    
    var tmpInfo = new Array(12);
	tmpInfo = jGET_TALBE_ROW_SUBJECT_SCHEME_INFO(moveRowSubjectID);
	//alert(tmpInfo['is_cmpSub']);
	if(tmpInfo['is_cmpSub'] == 0)
    	jSET_STYLE_OF_TABLE_ROWS(moveRowSubjectID, moveRowIndex, tmpInfo['parent_index']);
    
	// prepare info of sub-subject which is selected
	var tmp0 = '<table border="0" cellpadding="2" cellspacing="0">';
  	tmp0 += '<tr><td><div id="div_up_' + s_id + '" style="display:' + ((info['cmp_index'] == 0) ? 'none' : 'block') +'"><a name="move_up[]" id="move_up' + s_id + '" href="javascript:void(0)" class="contenttool" onclick="jMOVE_TABLE_ROW(this, \'1\', \'' + s_id + '\')"><img src="<?=$image_path.'/'.$LAYOUT_SKIN?>/icon_sort_a_off.gif" width="13" height="13" border="0" alt="<?=$eReportCard['MoveUp']?>"></a></div></td></tr>';
  	tmp0 += '<tr><td><div id="div_down_' + s_id + '" style="display:' + ((info['cmp_index'] == (cmpNum-1)) ? 'none' : 'block') + '"><a name="move_down[]" id="move_down' + s_id + '" href="javascript:void(0)" class="contenttool" onclick="jMOVE_TABLE_ROW(this, \'-1\', \'' + s_id + '\')"><img src="<?=$image_path.'/'.$LAYOUT_SKIN?>/icon_sort_d_off.gif" width="13" height="13" border="0" alt="<?=$eReportCard['MoveDown']?>"></a></div></td></tr>';
	tmp0 += '</table>';
	
	var tmp1 = '';
	tmp1 += info['sub_name'];
	tmp1 += '<input type="hidden" name="subject_name_' + s_id + '" id="subject_name_' + s_id + '" value="' + info['sub_name'] + '"/>';
	tmp1 += '<input type="hidden" name="display_order_' + s_id + '" id="display_order_' + s_id + '" value="' + info['sub_order'] + '"/>';
    tmp1 += '<input type="hidden" name="cmpSubjectFlag_' + s_id + '" id="cmpSubjectFlag_' + s_id + '" value="' + info['is_cmpSub'] + '"/>';
    tmp1 += '<input type="hidden" name="parent_subject_id_' + s_id + '" id="parent_subject_id_' + s_id + '" value="' + info['parent_sub_id'] + '"/>';
    tmp1 += '<input type="hidden" name="subjectID[]" id="subjectID_' + newRowIndex + '" value="' + s_id + '"/>';
	
	var tmp2 = '<table border="0" cellpadding="0" cellspacing="0"><tr>';
	tmp2 += '<td><select id="select_scheme_' + s_id + '" name="select_scheme_' + s_id + '" class="tabletexttoptext scheme_selection" onMouseOver="jSHOW_GRADING_SCHEME1(document.getElementById(\'img_edit_' + s_id + '\'), \'' + s_id + '\', \'0\')"  onMouseOut="jHIDE_GRADING_SCHEME1(\'' + s_id + '\', \'\', \'0\')" onchange="jCHANGE_GRADING_METHOD(\'' + s_id + '\')">' + seloption + '</select></td>';
	tmp2 += '<td><a href="javascript:jSHOW_GRADING_SCHEME1(document.getElementById(\'img_edit_' + s_id + '\'), \'' + s_id + '\', \'1\')"><img id="img_edit_' + s_id + '" src="<?=$image_path.'/'.$LAYOUT_SKIN?>/icon_edit_b.gif" width="20" height="20" border="0"></a></td>';
	tmp2 += '</tr></table>';
	
	var tmp3 = '<span class="tabletext">';
	tmp3 += '<div id="div_scale_honor_' + s_id + '" style="display:' + ((info['sc_type'] == 'PF') ? 'none' : 'block') + '">';
	tmp3 += '<input type="radio" name="scale_' + s_id + '" id="scale_' + s_id + '_2" class="input_scale_mark" value="M" ' + (info['sc_scale_h'] == 'M' ? 'checked' : '') +'><label for="scale_' + s_id + '_2"><?=$eReportCard['Mark']?></label> &nbsp;&nbsp;';
  	tmp3 += '<input type="radio" name="scale_' + s_id + '" id="scale_' + s_id + '_1" class="input_scale_grade" value="G" ' + (info['sc_scale_h'] != 'M' ? 'checked' : '') +'><label for="scale_' + s_id + '_1"><?=$eReportCard['Grade']?></label>';
    tmp3 += '</div>';
    tmp3 += '<div id="div_scale_passfail_' + s_id + '" style="display:' + ((info['sc_type'] == 'PF') ? 'block' : 'none') + '">' + info['sc_scale_pf'] + '</div>';
    tmp3 += '</span>';
    
    var tmp4 = '<span class="tabletext">';
    tmp4 += '<div id="div_display_honor_' + s_id + '" style="display:' + ((info['sc_type'] == 'PF') ? 'none' : 'block') + '">';
    tmp4 += '<input type="radio" name="display_' + s_id + '" id="display_' + s_id + '_2" class="display_scale_mark" value="M" ' + (info['sc_display_h'] == 'M' ? 'checked' : '') +'><label for="display_' + s_id + '_2"><?=$eReportCard['Mark']?></lable> &nbsp;&nbsp;';
    tmp4 += '<input type="radio" name="display_' + s_id + '" id="display_' + s_id + '_1" class="display_scale_grade" value="G" ' + (info['sc_display_h'] != 'M' ? 'checked' : '') +'><label for="display_' + s_id + '_1"><?=$eReportCard['Grade']?></lable>';
    tmp4 += '</div>';
    tmp4 += '<div id="div_display_passfail_' + s_id + '" style="display:' + ((info['sc_type'] == 'PF') ? 'block' : 'none') + '">' + info['sc_display_pf'] + '</div>';
    tmp4 += '</span>';
	
	var tmp5 = '';
    if(seloption2!='')
    {
	    tmp5 = '<span class="tabletext">';
	    tmp5 += '<select name="LangDisplay_'+ s_id +'" id="LangDisplay_'+ s_id +'" class="tabletexttoptext lang_display_selection">'+ seloption2 +'</select>';
	    tmp5 += '</span>';    	
    }
    
    if(info['sc_displaysubjgroup'])
	{
		var tmp6 = '<span class="tabletext">';
	    tmp6 += '<div id="div_Display_SubjGroup_' + s_id + '" >';
	    tmp6 += '<input type="radio" name="Display_SubjGroup_' + s_id + '" id="Display_SubjGroup_' + s_id + '_2" class="display_subject_group_yes" value="Y" ' + (info['sc_displaysubjgroup'] == 'Y' ? 'checked' : '') +'><label for="Display_SubjGroup_' + s_id + '_2"><?=$Lang['General']['Yes']?></label> &nbsp;&nbsp;';
	    tmp6 += '<input type="radio" name="Display_SubjGroup_' + s_id + '" id="Display_SubjGroup_' + s_id + '_1" class="display_subject_group_no" value="N" ' + (info['sc_displaysubjgroup'] == 'N' ? 'checked' : '') +'><label for="Display_SubjGroup_' + s_id + '_1"><?=$Lang['General']['No']?></label>';
	    tmp6 += '</div>';
	    tmp6 += '</span>';    	
	}
    
    
	// add sub-subject which is selected
	jADD_TABLE_ROW(s_id, newRowIndex, info['cmp_index'], info, tmp0, tmp1, tmp2, tmp3, tmp4, tmp5,tmp6,cmpNum);
	
	jSET_SELECTBOX_SELECTED_OPTION("select_scheme_" + s_id, info['sc_id']);
	if(seloption2!='')
    {
		jSET_SELECTBOX_SELECTED_OPTION("LangDisplay_" + s_id, info['sc_langdisplay']);
    }
	//jLOOP();
}

function jMOVE_TABLE_NON_SUB_ROW(obj, jType, s_id){	// move up or down the parent row cross sub-subject row without exchange display order
	//jLOOP();
	var info = new Array(12);
	
	info = jGET_TALBE_ROW_SUBJECT_SCHEME_INFO(s_id);
	var seloption = jGET_SELECT_ALL_OPTIONS("scheme_template");
	var seloption2 = (info["sc_langdisplay"]!='')?jGET_SELECT_ALL_OPTIONS("LangDisplay_"+s_id):'';
	var index = jGET_TABLE_ACTION_INDEX(obj, jType);
	var oldRowIndex = index;
	
	// Parent Subject 
	var parentNum = document.getElementsByName("parentSubject[]").length;
	
	if(jType == 1){
		var newRowIndex = parseInt(oldRowIndex) - 1;
	}
	else if(jType == '-1'){
		var newRowIndex = parseInt(oldRowIndex) + 1;
	}
	var moveRowIndex = oldRowIndex;

	// remove the sub subject which is selected
	jREMOVE_ROW(oldRowIndex);
	
	// Get the info [Except Display Order] of the subject which is affected
    var moveRowSubjectID = document.getElementById("subjectID_" + newRowIndex).value;

    // Update the info [Except Display Order] of the subject which is affected
    document.getElementById("subjectID_" + newRowIndex).id = "subjectID_" + moveRowIndex;
    
    var tmpInfo = new Array(12);
	tmpInfo = jGET_TALBE_ROW_SUBJECT_SCHEME_INFO(moveRowSubjectID);
	//alert(tmpInfo['is_cmpSub']);
	if(tmpInfo['is_cmpSub'] == 0)
    	jSET_STYLE_OF_TABLE_ROWS(moveRowSubjectID, moveRowIndex, tmpInfo['parent_index']);
    
	// prepare info of sub-subject which is selected
	var tmp0 = '<table border="0" cellpadding="2" cellspacing="0">';
  	tmp0 += '<tr><td><div id="div_up_' + s_id + '" style="display:' + ((info['parent_index'] == 0) ? 'none' : 'block') +'"><a name="move_up[]" id="move_up' + s_id + '" href="javascript:void(0)" class="contenttool" onclick="jMOVE_TABLE_ROW(this, \'1\', \'' + s_id + '\')"><img src="<?=$image_path.'/'.$LAYOUT_SKIN?>/icon_sort_a_off.gif" width="13" height="13" border="0" alt="<?=$eReportCard['MoveUp']?>"></a></div></td></tr>';
  	tmp0 += '<tr><td><div id="div_down_' + s_id + '" style="display:' + ((info['parent_index'] == (parseInt(parentNum)-1) || (newRowIndex == subjectArr.length && info['is_cmpSub'] == 0)) ? 'none' : 'block') + '"><a name="move_down[]" id="move_down' + s_id + '" href="javascript:void(0)" class="contenttool" onclick="jMOVE_TABLE_ROW(this, \'-1\', \'' + s_id + '\')"><img src="<?=$image_path.'/'.$LAYOUT_SKIN?>/icon_sort_d_off.gif" width="13" height="13" border="0" alt="<?=$eReportCard['MoveDown']?>"></a></div></td></tr>';
	tmp0 += '</table>';
	
	var tmp1 = '';
	tmp1 += info['sub_name'];
	tmp1 += '<input type="hidden" name="subject_name_' + s_id + '" id="subject_name_' + s_id + '" value="' + info['sub_name'] + '"/>';
	tmp1 += '<input type="hidden" name="display_order_' + s_id + '" id="display_order_' + s_id + '" value="' + info['sub_order'] + '"/>';
    tmp1 += '<input type="hidden" name="cmpSubjectFlag_' + s_id + '" id="cmpSubjectFlag_' + s_id + '" value="' + info['is_cmpSub'] + '"/>';
    tmp1 += '<input type="hidden" name="parent_subject_id_' + s_id + '" id="parent_subject_id_' + s_id + '" value="' + info['parent_sub_id'] + '"/>';
    tmp1 += '<input type="hidden" name="subjectID[]" id="subjectID_' + newRowIndex + '" value="' + s_id + '"/>';
	
	var tmp2 = '<table border="0" cellpadding="0" cellspacing="0"><tr>';
	tmp2 += '<td><select id="select_scheme_' + s_id + '" name="select_scheme_' + s_id + '" class="tabletexttoptext scheme_selection" onMouseOver="jSHOW_GRADING_SCHEME1(document.getElementById(\'img_edit_' + s_id + '\'), \'' + s_id + '\', \'0\')"  onMouseOut="jHIDE_GRADING_SCHEME1(\'' + s_id + '\', \'\', \'0\')" onchange="jCHANGE_GRADING_METHOD(\'' + s_id + '\')">' + seloption + '</select></td>';
	tmp2 += '<td><a href="javascript:jSHOW_GRADING_SCHEME1(document.getElementById(\'img_edit_' + s_id + '\'), \'' + s_id + '\', \'1\')"><img id="img_edit_' + s_id + '" src="<?=$image_path.'/'.$LAYOUT_SKIN?>/icon_edit_b.gif" width="20" height="20" border="0"></a></td>';
	tmp2 += '</tr></table>';
	
	var tmp3 = '<span class="tabletext">';
	tmp3 += '<div id="div_scale_honor_' + s_id + '" style="display:' + ((info['sc_type'] == 'PF') ? 'none' : 'block') + '">';
  	tmp3 += '<input type="radio" name="scale_' + s_id + '" id="scale_' + s_id + '_1" value="G" ' + (info['sc_scale_h'] != 'M' ? 'checked' : '') +'><?=$eReportCard['Grade']?> &nbsp;&nbsp;';
    tmp3 += '<input type="radio" name="scale_' + s_id + '" id="scale_' + s_id + '_2" value="M" ' + (info['sc_scale_h'] == 'M' ? 'checked' : '') +'><?=$eReportCard['Mark']?>';
    tmp3 += '</div>';
    tmp3 += '<div id="div_scale_passfail_' + s_id + '" style="display:' + ((info['sc_type'] == 'PF') ? 'block' : 'none') + '">' + info['sc_scale_pf'] + '</div>';
    tmp3 += '</span>';
    
    var tmp4 = '<span class="tabletext">';
    tmp4 += '<div id="div_display_honor_' + s_id + '" style="display:' + ((info['sc_type'] == 'PF') ? 'none' : 'block') + '">';
    tmp4 += '<input type="radio" name="display_' + s_id + '" id="display_' + s_id + '_2" value="G" ' + (info['sc_display_h'] != 'M' ? 'checked' : '') +'><?=$eReportCard['Grade']?> &nbsp;&nbsp;';
    tmp4 += '<input type="radio" name="display_' + s_id + '" id="display_' + s_id + '_2" value="M" ' + (info['sc_display_h'] == 'M' ? 'checked' : '') +'><?=$eReportCard['Mark']?>';
    tmp4 += '</div>';
    tmp4 += '<div id="div_display_passfail_' + s_id + '" style="display:' + ((info['sc_type'] == 'PF') ? 'block' : 'none') + '">' + info['sc_display_pf'] + '</div>';
    tmp4 += '</span>';
	
	var tmp5 = '';
    if(seloption2!='')
    {
	    tmp5 = '<span class="tabletext">';
	    tmp5 += '<select name="LangDisplay_'+ s_id +'" id="LangDisplay_'+ s_id +'" class="tabletexttoptext scheme_selection">'+ seloption2 +'</select>';
	    tmp5 += '</span>';    	
    }
	
    if(info['sc_displaysubjgroup'])
	{
		var tmp6 = '<span class="tabletext">';
	    tmp6 += '<div id="div_Display_SubjGroup_' + s_id + '" >';
	    tmp6 += '<input type="radio" name="Display_SubjGroup_' + s_id + '" id="Display_SubjGroup_' + s_id + '_2" value="Y" ' + (info['sc_displaysubjgroup'] == 'Y' ? 'checked' : '') +'><?=$Lang['General']['Yes']?> &nbsp;&nbsp;';
	    tmp6 += '<input type="radio" name="Display_SubjGroup_' + s_id + '" id="Display_SubjGroup_' + s_id + '_1" value="N" ' + (info['sc_displaysubjgroup'] == 'N' ? 'checked' : '') +'><?=$Lang['General']['No']?>';
	    tmp6 += '</div>';
	    tmp6 += '</span>';    	
	}
    
	// add sub-subject which is selected
	jADD_TABLE_ROW(s_id, newRowIndex, info['parent_index'], info, tmp0, tmp1, tmp2, tmp3, tmp4, tmp5,tmp6,parentNum);
	
	jSET_SELECTBOX_SELECTED_OPTION("select_scheme_" + s_id, info['sc_id']);
	if(seloption2!='')
    {
		jSET_SELECTBOX_SELECTED_OPTION("LangDisplay_" + s_id, info['sc_langdisplay']);
    }
	
	//jLOOP();
}

function jADD_TABLE_ROW(s_id, rowIndex, subIndex, info, tmp0, tmp1, tmp2, tmp3, tmp4, tmp5,tmp6,jNum){
	var newRowIndex = rowIndex;
	var newIndex = subIndex;
	
	// Set the Class Style
    if(info['is_cmpSub'] == 0){
		cell0Class = "tablelist_subject";
		cell1Class = "tablelist_subject tablelist_subject";
		cell2Class = (newRowIndex % 2 == 1 ) ? "retablerow1" : "retablerow2";
	}
	else {
		if(newIndex == 0)
			cell0Class = (newIndex == (jNum-1)) ? "tablelist_subject" : "tablelist_subject_head";
		else if(newIndex == (jNum-1))
			cell0Class = "tablelist_subject";
		else 
			cell0Class = "tablelist_subject_head";
			
		cell1Class = "tablelist_subject_head tablelist_sub_subject";
		cell2Class = "retablerow1 retablerow_subsubject_line";
	}
	
	// insert the selected row to a new position
	var tb = document.getElementById("tb_Form_Settings");
	var newRow = tb.insertRow(newRowIndex);
	
	var cell0 = newRow.insertCell(0);
	cell0.id = "td_head_" + s_id;
	cell0.className = cell0Class;
	cell0.width= "15";
	cell0.innerHTML = tmp0;
  	
  	var cell1 = newRow.insertCell(1);
  	cell1.width = "20%";
  	cell1.className = cell1Class;
  	cell1.innerHTML = tmp1;
    
	var cell2 = newRow.insertCell(2);
	cell2.id = "td_select_" + s_id;
	//cell2.width = "30%";
	cell2.align = "center";
	cell2.className = cell2Class;
	cell2.innerHTML = tmp2;
	
	var cell3 = newRow.insertCell(3);
	cell3.id = "td_scale_" + s_id;
	cell3.align = "center";
	cell3.className = cell2Class;
	cell3.innerHTML = tmp3;
	
	var cell4 = newRow.insertCell(4);
	cell4.id = "td_display_" + s_id;
	cell4.align = "center";
	cell4.className = cell2Class;
	cell4.innerHTML = tmp4;
	
	<?if($eRCTemplateSetting['SelectSubjectDisplayLang']==true){?>
		var cell5 = newRow.insertCell(5);
		cell5.id = "td_langdisplay_" + s_id;
		cell5.align = "center";
		cell5.className = cell2Class;
		cell5.innerHTML = tmp5?tmp5:'&nbsp;';
	<?}?>
	
	<?if($eRCTemplateSetting['SelectDisplaySubjectGroup']==true){?>
		var cell6 = newRow.insertCell(6);
		cell6.id = "td_Display_SubjGroup_" + s_id;
		cell6.align = "center";
		cell6.className = cell2Class;
		cell6.innerHTML = tmp6?tmp6:'&nbsp;';
	<?}?>
	
	
}

function jREMOVE_ROW(index){
	var tb = document.getElementById('tb_Form_Settings');
	var idx = index;
	
	tb.deleteRow(idx);
}

/*
* JS Form
*/
// If ScaleInput is Grade(G), then ScaleDisplay cannot be Mark(M)
function jCHECK_SCALE_INPUT_DISPLAY(s_id){
	var scaleInput = jGET_RADIO_CHECKED_VALUE("scale_" + s_id);
	var scaleDisplay = jGET_RADIO_CHECKED_VALUE("display_" + s_id);
	
	if(scaleInput == "G"){	
		if(scaleDisplay == "M"){
			var subjectNameStr = document.getElementById("subject_name_" + s_id).value;
			var subjectName = subjectNameStr.replace("<br />", " ");
			alert("<?=sprintf($eReportCard['jsCheckDisplayResult'], "\"+subjectName+\"")?>");
			return false;
		}
		else {
			return true;
		}
	}
	else {
		return true;
	}
}

function jCHECK_NUMERICAL_ELEMENT(obj, msg){
	if(obj){
		//if(obj.value != null && obj.value != ""){
			//if(!check_numeric(obj, obj.value, msg))
				//return false;
		//}
		if($(obj).val() != null && $(obj).val() != ""){
			if(!IsNumeric($(obj).val()))
			{	
				alert(msg)
				return false;
			}
			
		}
	}
	return true;
}

function jCHECK_POSITIVE_NUMERICAL_ELEMENT(obj, msg){
	if(obj){
		var val = obj.value;
		if(val != null && val != ""){
			if(parseInt(val) < 0){
				alert(msg);
				obj.focus();
				return false;
			}
		}
	}
	return true;
}

// LowerLimit cannot be larger than the Full Mark of Scheme if SchemeType is Honor
function jCHECK_GRADING_SCHEME_DETAILS(sc_id){
	var schemeNameObj = document.getElementById("grading_title_" + sc_id);
	var fullmarkObj = document.getElementById("grading_fullmark_" + sc_id);
	var fullmark = fullmarkObj.value;
	var passmarkObj = document.getElementById("grading_passingmark_" + sc_id);
	var passmark = passmarkObj.value;
	var range = jGET_RADIO_CHECKED_VALUE("grading_range_" + sc_id);
	
	if(fullmark != "" && fullmark != null && passmark != null && passmark != ""){
		if(!jCHECK_VALID_LOWERLIMIT(passmarkObj, fullmark, "<?=sprintf($eReportCard['jsSthCannotBeLargerThanFullMark'], $eReportCard['SchemesPassingMark'])?>")) 
			return false;
	}
	
	if(!jCHECK_FIELD(schemeNameObj, "<?=$eReportCard['jsFillGradingSchemeTitle']?>")) return false;
	if(!jCHECK_POSITIVE_NUMERICAL_ELEMENT(fullmarkObj, "<?=sprintf($eReportCard['jsSthMustBePositive'], $eReportCard['SchemesFullMark'])?>") ) return false;
	if(!jCHECK_NUMERICAL_ELEMENT(fullmarkObj, "<?=sprintf($eReportCard['jsSthMustBeNumericalNumber'],  $eReportCard['SchemesFullMark'])?>") ) return false;
				
	if(!jCHECK_POSITIVE_NUMERICAL_ELEMENT(passmarkObj, "<?=sprintf($eReportCard['jsSthMustBePositive'], $eReportCard['SchemesFullMark'])?>")) return false;
	if(!jCHECK_NUMERICAL_ELEMENT(passmarkObj, "<?=sprintf($eReportCard['jsSthMustBeNumericalNumber'],  $eReportCard['SchemesFullMark'])?>")) return false;
	
	// Check Range Info Details - Distinction
	var cnt = $("#D_Cnt_"+sc_id).val()
	var jType = "D";
	if(!jCHECK_RANGE_INFO(sc_id, fullmark, cnt, jType, range)) return false;
	
	// Check Range Info Details - Pass
	//var cnt = document.getElementById("P_Cnt_"+sc_id).value;
	var cnt = $("#P_Cnt_"+sc_id).val()
	var jType = "P";
	if(!jCHECK_RANGE_INFO(sc_id, fullmark, cnt, jType, range)) return false;
	
	// Check Range Info Details - Fail
	//var cnt = document.getElementById("F_Cnt_"+sc_id).value;
	var cnt = $("#F_Cnt_"+sc_id).val()
	var jType = "F";
	if(!jCHECK_RANGE_INFO(sc_id, fullmark, cnt, jType, range)) return false;
	
	if(range != 0){
		if(!jCHECK_UPPERLIMIT_INFO(sc_id)) return false;
	}
	else {
		if(!jCHECK_LOWERLIMIT_DESCENDING_ORDER(sc_id)) return false;
	}
	
	if(!jCHECK_ADDITIONAL_CRITERIA_DESCENDING_ORDER(sc_id)) return false;
	
	return true;
}

/* 
* sc_id: SchemeID ; cnt: number of range info of jType
* jType: D-Distinction, P-Pass, F-Fail
* rangeType: 0-Mark Range, 1 or 2-Percentage Range
*/
function jCHECK_RANGE_INFO(sc_id, fullmark, cnt, jType, rangeType){
	var lastVal = "";
	if(jType != "D"){
		if(cnt > 0){
			for(var i=0 ; i<cnt ; i++){
				if(rangeType == 0){
					var obj1Name = jType + "_MR_" + sc_id + "_" + i + "_a";
					var msg1 = "<?=sprintf($eReportCard['jsSthMustBePositive'], $eReportCard['LowerLimit'])?>";
					var msg2 = "<?=sprintf($eReportCard['jsSthMustBeNumericalNumber'],  $eReportCard['LowerLimit'])?>";
				} 
				else {
					var obj1Name = jType + "_PR_" + sc_id + "_" + i + "_a";
					var msg1 = "<?=sprintf($eReportCard['jsSthMustBePositive'], $eReportCard['UpperLimit'])?>";
					var msg2 = "<?=sprintf($eReportCard['jsSthMustBeNumericalNumber'],  $eReportCard['UpperLimit'])?>";
				}
				var obj1 = document.getElementById(obj1Name);
				if(!jCHECK_POSITIVE_NUMERICAL_ELEMENT(obj1, msg1)) return false;
				if(!jCHECK_NUMERICAL_ELEMENT(obj1, msg2)) return false;
				
				if(obj1){
					var val = obj1.value;
					
					// added on 16 Dec 2008 by Ivan
					// check strictly-decreasing order of passing mark
					if ( (lastVal!="") && (parseInt(val) >= parseInt(lastVal)) && (rangeType==0) )
					{
						alert("<?=$eReportCard['jsLowerLimitCannotBeTheSame']?>");
						return false;
					}
					
					if(rangeType == 0 && fullmark != "" && fullmark != null && val != null && val != ""){
						if(!jCHECK_VALID_LOWERLIMIT(obj1, fullmark, "<?=sprintf($eReportCard['jsSthCannotBeLargerThanFullMark'], $eReportCard['LowerLimit'])?>")) 
							return false;
					}
					
					lastVal = val;
				}
				
				var obj2Name = jType + "_" + sc_id + "_" + i + "_c";
				var obj2 = document.getElementById(obj2Name);
				if(!jCHECK_NUMERICAL_ELEMENT(obj2, "<?=sprintf($eReportCard['jsSthMustBeNumericalNumber'],  $eReportCard['GradePoint'])?>")) return false;
			}
		}
	}
	else {
		if(rangeType == 0){
			var obj1Name = jType + "_MR_" + sc_id + "_a";
			var msg1 = "<?=sprintf($eReportCard['jsSthMustBePositive'], $eReportCard['LowerLimit'])?>";
			var msg2 = "<?=sprintf($eReportCard['jsSthMustBeNumericalNumber'],  $eReportCard['LowerLimit'])?>";
		} 
		else {
			var obj1Name = jType + "_PR_" + sc_id + "_a";
			var msg1 = "<?=sprintf($eReportCard['jsSthMustBePositive'], $eReportCard['UpperLimit'])?>";
			var msg2 = "<?=sprintf($eReportCard['jsSthMustBeNumericalNumber'],  $eReportCard['UpperLimit'])?>";
		}
		
		var obj1 = document.getElementById(obj1Name);
		if(!jCHECK_POSITIVE_NUMERICAL_ELEMENT(obj1, msg1)) return false;
		if(!jCHECK_NUMERICAL_ELEMENT(obj1, msg2)) return false;
		
		if(obj1){
			var val = obj1.value;
			
			var passmarkObj = document.getElementById("grading_passingmark_" + sc_id);
			var passmark = passmarkObj.value;
			if(rangeType == 0 && passmark != "" && passmark != null && val != null && val != ""){
				if (parseInt(val) < parseInt(passmark))
				{
					//alert(val);
					//alert(passmark);
					alert("<?=sprintf($eReportCard['jsSthCannotBeLessThanPassingMark'], $eReportCard['Distinction'])?>");
					return false;
				}
			}
			
			if(rangeType == 0 && fullmark != "" && fullmark != null && val != null && val != ""){
				if(!jCHECK_VALID_LOWERLIMIT(obj1, fullmark, "<?=sprintf($eReportCard['jsSthCannotBeLargerThanFullMark'], $eReportCard['LowerLimit'])?>")) 
					return false;
			}
		}
		
		var obj2Name = jType + "_" + sc_id + "_c";
		var obj2 = document.getElementById(obj2Name);
		if(!jCHECK_NUMERICAL_ELEMENT(obj2, "<?=sprintf($eReportCard['jsSthMustBeNumericalNumber'],  $eReportCard['GradePoint'])?>")) return false;
	}
	return true;
}

function jCHECK_VALID_LOWERLIMIT(obj, val, msg){
	if(obj){
		if(parseInt(obj.value) > parseInt(val)){
			alert(msg);
			obj.focus();
			return false;
		}
	}
	return true;
}

function jCHECK_LOWERLIMIT_DESCENDING_ORDER(sc_id){
	var checkVal = "";
	/*var objName = "D_MR_" + sc_id + "_a";
	if(document.getElementById(objName).value != ""){
		var tmpVal = parseInt(document.getElementById(objName).value);
		checkVal = tmpVal;
	}*/
	var cnt = $("#D_Cnt_"+sc_id).val();
	if(cnt > 0){
		for(var i=0 ; i<cnt ; i++){
			var objName = "D_MR_" + sc_id + "_" + i + "_a";
			//if(document.getElementById(objName)){
				//if(document.getElementById(objName).value != ""){
					//var tmpObj = document.getElementById(objName);
			if($("#"+objName).length>0){
				if($("#"+objName).val() != ""){
					var tmpObj = $("#"+objName).get();
					if(!jCHECK_VALID_LOWERLIMIT(tmpObj, checkVal, "<?=sprintf($eReportCard['jsInvalidSth'], $eReportCard['LowerLimit'])?>")) 
						return false;
					else 
						checkVal = tmpObj.value;
				}
			}
		}
	}
	
		
	//var cnt = document.getElementById("P_Cnt_"+sc_id).value;
	var cnt = $("#P_Cnt_"+sc_id).val();
	if(cnt > 0){
		for(var i=0 ; i<cnt ; i++){
			var objName = "P_MR_" + sc_id + "_" + i + "_a";
			//if(document.getElementById(objName)){
				//if(document.getElementById(objName).value != ""){
					//var tmpObj = document.getElementById(objName);
			if($("#"+objName).length>0){
				if($("#"+objName).val() != ""){
					var tmpObj = $("#"+objName).get();
					if(!jCHECK_VALID_LOWERLIMIT(tmpObj, checkVal, "<?=sprintf($eReportCard['jsInvalidSth'], $eReportCard['LowerLimit'])?>")) 
						return false;
					else 
						checkVal = tmpObj.value;
				}
			}
		}
	}
	
	//var cnt = document.getElementById("F_Cnt_"+sc_id).value;
	var cnt = $("#F_Cnt_"+sc_id).val();
	if(cnt > 0){
		for(var i=0 ; i<cnt ; i++){
			var objName = "F_MR_" + sc_id + "_" + i + "_a";
			//if(document.getElementById(objName)){
				//if(document.getElementById(objName).value != ""){
					//var tmpObj = document.getElementById(objName);
			if($("#"+objName).length>0){
				if($("#"+objName).val() != ""){
					var tmpObj = $("#"+objName).get();					
					if(!jCHECK_VALID_LOWERLIMIT(tmpObj, checkVal, "<?=sprintf($eReportCard['jsInvalidSth'], $eReportCard['LowerLimit'])?>")) 
						return false;
					else 
						checkVal = tmpObj.value;
				}
			}
		}
	}
	return true;
}

function jCHECK_ADDITIONAL_CRITERIA_DESCENDING_ORDER(sc_id){
	var checkVal = "";
	/*var objName = "D_" + sc_id + "_d";

	if($("#"+objName).val() != ""){
		if(!jCHECK_NUMERICAL_ELEMENT($("#"+objName).get(), "<?=sprintf($eReportCard['jsSthMustBeNumericalNumber'],  $eReportCard['AdditionalCriteria'])?>")) return false;
		var tmpVal = $("#"+objName).val();
		checkVal = tmpVal;
	}*/
	var cnt = $("#D_Cnt_"+sc_id).val();
	if(cnt > 0){
		for(var i=0 ; i<cnt ; i++){
			var objName = "D_" + sc_id + "_" + i + "_d";
			if($("#"+objName).length>0){
				if($("#"+objName).val() != ""){
					var tmpObj = $("#"+objName).get();
					if(!jCHECK_NUMERICAL_ELEMENT(tmpObj, "<?=sprintf($eReportCard['jsSthMustBeNumericalNumber'],  $eReportCard['AdditionalCriteria'])?>")) 
						return false;
					if(checkVal!='' && $("#"+objName).val()>checkVal)
					{
						alert("<?=sprintf($eReportCard['jsInvalidSth'], $eReportCard['AdditionalCriteria'])?>")
						return false;
					}
 
						checkVal = $("#"+objName).val();
				}
			}
		}
	}
		
	var cnt = $("#P_Cnt_"+sc_id).val();
	if(cnt > 0){
		for(var i=0 ; i<cnt ; i++){
			var objName = "P_" + sc_id + "_" + i + "_d";
			if($("#"+objName).length>0){
				if($("#"+objName).val() != ""){
					var tmpObj = $("#"+objName).get();
					if(!jCHECK_NUMERICAL_ELEMENT(tmpObj, "<?=sprintf($eReportCard['jsSthMustBeNumericalNumber'],  $eReportCard['AdditionalCriteria'])?>")) 
						return false;
					if(checkVal!='' && $("#"+objName).val()>checkVal)
					{
						alert("<?=sprintf($eReportCard['jsInvalidSth'], $eReportCard['AdditionalCriteria'])?>")
						return false;
					}
 
						checkVal = $("#"+objName).val();
				}
			}
		}
	}

	var cnt = $("#F_Cnt_"+sc_id).val();
	if(cnt > 0){
		for(var i=0 ; i<cnt ; i++){
			var objName = "F_" + sc_id + "_" + i + "_d";
			if($("#"+objName).length>0){
				if($("#"+objName).val() != ""){
					var tmpObj = $("#"+objName).get();
					if(!jCHECK_NUMERICAL_ELEMENT(tmpObj, "<?=sprintf($eReportCard['jsSthMustBeNumericalNumber'],  $eReportCard['AdditionalCriteria'])?>")) 
						return false;
					if(checkVal!='' && $("#"+objName).val()>checkVal)
					{
						alert("<?=sprintf($eReportCard['jsInvalidSth'], $eReportCard['AdditionalCriteria'])?>")
						return false;
					} 
						checkVal = $("#"+objName).val();
				}
			}
		}
	}
	return true;
}

function jCHECK_UPPERLIMIT_INFO(sc_id){
	var range_type = jGET_RADIO_CHECKED_VALUE("grading_range_" + sc_id);
	
	if (range_type == 1) // % Range for all students
	{
		// return true if the total % of Distinction, Pass and Fail range equals 100
		
		var totalPercentage = 0;
		/*
		var objName = "D_PR_" + sc_id + "_a";
		//if(document.getElementById(objName).value != "")
		if($("[name="+objName+"]").val() != "")
			totalPercentage += parseInt($("[name="+objName+"]").val());
		*/
		var cnt = $("#D_Cnt_"+sc_id).val();
		if(cnt > 0){
			for(var i=0 ; i<cnt ; i++){
				var objName = "D_PR_" + sc_id + "_" + i + "_a";
				//if(document.getElementById(objName)){
				//	if(document.getElementById(objName).value != "")
				if($("[name="+objName+"]").length>0){
					if($("[name="+objName+"]").val() != "")
						totalPercentage += parseInt($("[name="+objName+"]").val());
						//totalPercentage += parseInt(document.getElementById(objName).value);
				}
			}
		}
		
		//var cnt = document.getElementById("P_Cnt_"+sc_id).value;
		var cnt = $("#P_Cnt_"+sc_id).val();
		if(cnt > 0){
			for(var i=0 ; i<cnt ; i++){
				var objName = "P_PR_" + sc_id + "_" + i + "_a";
				//if(document.getElementById(objName)){
				//	if(document.getElementById(objName).value != "")
				if($("[name="+objName+"]").length>0){
					if($("[name="+objName+"]").val() != "")
						totalPercentage += parseInt($("[name="+objName+"]").val());
						//totalPercentage += parseInt(document.getElementById(objName).value);
				}
			}
		}
		
		//var cnt = document.getElementById("F_Cnt_"+sc_id).value;
		var cnt = $("#F_Cnt_"+sc_id).val();
		if(cnt > 0){
			for(var i=0 ; i<cnt ; i++){
				var objName = "F_PR_" + sc_id + "_" + i + "_a";
				//if(document.getElementById(objName)){
					//if(document.getElementById(objName).value != "")
						//totalPercentage += parseInt(document.getElementById(objName).value);
				if($("[name="+objName+"]").length>0){
					if($("[name="+objName+"]").val() != "")
						totalPercentage += parseInt($("[name="+objName+"]").val());
				
				}
			}
		}

		if(parseInt(totalPercentage) != 100){
			alert("<?=$eReportCard['jsCheckTotalUpperLimit']?>");
			return false;
		}
		return true;
	}
	else if (range_type == 2) // % Range for passed students
	{
		// return true if the total % of Distinction, Pass range equals 100 , and the total % of Fail range equals 100
		
		var totalPercentage = 0;
		/*var objName = "D_PR_" + sc_id + "_a";
		//if(document.getElementById(objName).value != "")
			//totalPercentage += parseInt(document.getElementById(objName).value);
		if($("[name="+objName+"]").val() != "")
			totalPercentage += parseInt($("#"+objName).val());*/
		var cnt = $("#D_Cnt_"+sc_id).val() 
		if(cnt > 0){
			for(var i=0 ; i<cnt ; i++){
				var objName = "D_PR_" + sc_id + "_" + i + "_a";
				//if(document.getElementById(objName)){
					//if(document.getElementById(objName).value != "")
						//totalPercentage += parseInt(document.getElementById(objName).value);
				if($("[name="+objName+"]").length>0){
					if($("[name="+objName+"]").val() != "")
						totalPercentage += parseInt($("[name="+objName+"]").val());
						
				}
			}
		}
			
		
		//var cnt = document.getElementById("P_Cnt_"+sc_id).value;
		var cnt = $("#P_Cnt_"+sc_id).val() 
		if(cnt > 0){
			for(var i=0 ; i<cnt ; i++){
				var objName = "P_PR_" + sc_id + "_" + i + "_a";
				//if(document.getElementById(objName)){
					//if(document.getElementById(objName).value != "")
						//totalPercentage += parseInt(document.getElementById(objName).value);
				if($("[name="+objName+"]").length>0){
					if($("[name="+objName+"]").val() != "")
						totalPercentage += parseInt($("[name="+objName+"]").val());
						
				}
			}
		}
		
		if(parseInt(totalPercentage) != 100){
			alert("<?=$eReportCard['jsCheckTotalUpperLimitDistinction&Pass']?>");
			return false;
		}
		
		totalPercentage = 0;
		//var cnt = document.getElementById("F_Cnt_"+sc_id).value;
		var cnt = $("#F_Cnt_"+sc_id).val()
		if(cnt > 0){
			for(var i=0 ; i<cnt ; i++){
				var objName = "F_PR_" + sc_id + "_" + i + "_a";
				//if(document.getElementById(objName)){
					//if(document.getElementById(objName).value != "")
						//totalPercentage += parseInt(document.getElementById(objName).value);
				if($("[name="+objName+"]").length>0){
					if($("[name="+objName+"]").val() != "")
						totalPercentage += parseInt($("[name="+objName+"]").val());						
				}
			}
		}
		
		if(parseInt(totalPercentage) != 100){
			alert("<?=$eReportCard['jsCheckTotalUpperLimitFail']?>");
			return false;
		}
		return true;	
	}
	
}

function jCHECK_FORM(){
	// Check for Scale Input & Display Result
	var subjectArr = document.getElementsByName("subjectID[]");
	var s_num = subjectArr.length;
	for(var i=0 ; i<s_num ; i++){
		var s_id = subjectArr[i].value;
		var selName = "select_scheme_" + s_id;
		var sc_id = jGET_SELECTBOX_SELECTED_VALUE(selName);
		
		if(sc_id != "" && sc_id != null){
			// cannot get the radio value as the layer is not pre-generated now.
			// changed to a js array to store the scheme type value
			//var sc_type = jGET_RADIO_CHECKED_VALUE("grading_type_" + sc_id);
			var sc_type = jsGradingTypeArr[sc_id];
			
			// Check if selected all input scale
			var InputMarkChecked = $('input#scale_' + s_id + '_2').attr('checked');
			var InputGradeChecked = $('input#scale_' + s_id + '_1').attr('checked');
			if (InputMarkChecked == false && InputGradeChecked == false)
			{
				var subjectNameStr = document.getElementById("subject_name_" + s_id).value;
				var subjectName = subjectNameStr.replace("<br />", " ");
				alert("<?=sprintf($eReportCard['jsSelectInputScale'], "\"+subjectName+\"")?>");
				
				$('input#scale_' + s_id + '_2').focus();
				return false;
			}
			
			// Check if selected all display scale
			var InputMarkChecked = $('input#display_' + s_id + '_2').attr('checked');
			var InputGradeChecked = $('input#display_' + s_id + '_1').attr('checked');
			if (InputMarkChecked == false && InputGradeChecked == false)
			{
				var subjectNameStr = document.getElementById("subject_name_" + s_id).value;
				var subjectName = subjectNameStr.replace("<br />", " ");
				alert("<?=sprintf($eReportCard['jsSelectDisplayScale'], "\"+subjectName+\"")?>");
				
				$('input#display_' + s_id + '_2').focus();
				return false;
			}
			
			// Check input scale is Grade and display scale is Mark
			if(sc_type == "H"){
				if(!jCHECK_SCALE_INPUT_DISPLAY(s_id))
					return false;
			}
		}
		else {
			var subjectNameStr = document.getElementById("subject_name_" + s_id).value;
			var subjectName = subjectNameStr.replace("<br />", " ");
			alert("<?=sprintf($eReportCard['jsSelectGradingScheme'], "\"+subjectName+\"")?>");
			
			$('select#' + selName).focus();
			return false;
		}
	}
	
	if($("#ReportID").val() == 0)
		if(!confirm("<?=$eReportCard['jsConfirmArr']['FormGradingSchemeWillBeCovered']?>"))
			return false;
			
	return true;
}

function jSUBMIT_FORM(){
	if(!jCHECK_FORM())
		return;
	
	var obj = document.FormMain;
	obj.action = "form_settings_update.php";
	obj.submit();
}

function jRESET_FORM(obj){
	obj.reset();
}

function jEDIT_GRADING_SCHEME(jCode, sc_id){
	jSET_ACTION_CODE(jCode);
	
	if(jCode == "DELETE"){
		if(!confirm("<?=$eReportCard['jsConfirmDeleteGradingScheme']?>"))
			return false;
		document.getElementById("loadingScheme_"+sc_id).innerHTML = "<span class='tabletext status_alert'><?=$eReportCard['Removing']?>...</span>";
	}
	else {
		// Check LowerLimit of Grading Scheme		
		var sc_type = jGET_RADIO_CHECKED_VALUE("grading_type_" + sc_id);
		jsGradingTypeArr[sc_id] = sc_type;
		
		if(sc_type == "H"){
			if(!jCHECK_GRADING_SCHEME_DETAILS(sc_id))
				return;
		}
		var isDupName = jCHECK_DUPLICATE_SCHEME_NAME(sc_id, jCode);
		if (isDupName){
			alert("<?=$eReportCard['jsAlertDuplicateGradingTitle']?>");
			return false;
		}
		document.getElementById("loadingScheme_"+sc_id).innerHTML = "<span class='tabletext status_alert'><?=$eReportCard['Saving']?>...</span>";
	}
	
	updateGradingScheme(sc_id);
}

function jEDIT_GRADING_SCHEME1(jCode, sc_id){
	jSET_ACTION_CODE(jCode);
	
	if(jCode == "DELETE"){
		if(!confirm("<?=$eReportCard['jsConfirmDeleteGradingScheme']?>"))
			return false;
		//document.getElementById("loadingScheme_"+sc_id).innerHTML = "<span class='tabletext status_alert'><?=$eReportCard['Removing']?>...</span>";
		$("#loadingScheme_"+sc_id).html("<span class='tabletext status_alert'><?=$eReportCard['Removing']?>...</span>");
	}
	else {
		// Check LowerLimit of Grading Scheme		
		var sc_type = jGET_RADIO_CHECKED_VALUE("grading_type_" + sc_id);
		
		// store the new scheme type (if SAVEAS, the type is saved in the ajax file [form_settings_aj_update.php])
		if (jCode == "SAVE")
			jsGradingTypeArr[sc_id] = sc_type;
		
		if(sc_type == "H"){
			if(!jCHECK_GRADING_SCHEME_DETAILS(sc_id))
				return;
		}
		var isDupName = jCHECK_DUPLICATE_SCHEME_NAME(sc_id, jCode);
		if (isDupName){
			alert("<?=$eReportCard['jsAlertDuplicateGradingTitle']?>");
			return false;
		}
		//document.getElementById("loadingScheme_"+sc_id).innerHTML = "<span class='tabletext status_alert'><?=$eReportCard['Saving']?>...</span>";
		$("#loadingScheme_"+sc_id).html("<span class='tabletext status_alert'><?=$eReportCard['Saving']?>...</span>");
	}
	
	updateGradingScheme(sc_id);
}

function jSET_ACTION_CODE(jCode){
	document.getElementById('ActionCode').value = jCode;
}

// Start of AJAX 
var callback = {
    success: function ( o )
    {    	
    	eval(o.responseText);
		//var responseTextSplit = o.responseText.split("%Separator%");
		//$("#debugbox").html(responseTextSplit[0]);
		//eval(responseTextSplit[1]);
		//jChangeContent( "div_form_settings", responseTextSplit[0] );		
		//eval(responseTextSplit[1]);
    }
}

// Main
function updateGradingScheme(sc_id)
{
	var saveBtn = document.getElementById("SaveButton_"+sc_id);
	var saveAsBtn = document.getElementById("SaveAsButton_"+sc_id);
	var cancelBtn = document.getElementById("CancelButton_"+sc_id);
	var deleteBtn = document.getElementById("DeleteButton_"+sc_id);
	
	if (saveBtn != null) saveBtn.disabled = true;
	if (saveAsBtn != null) saveAsBtn.disabled = true;
	if (cancelBtn != null) cancelBtn.disabled = true;
	if (deleteBtn != null) deleteBtn.disabled = true;
	
    obj = document.FormMain;
    YAHOO.util.Connect.setForm(obj);
	
	var params = '';
	if(sc_id != null)
		params = "?ClassLevelID=<?=$targetForm?>&sc_id=" + sc_id;
	
    var path = "form_settings_aj_update.php" + params;
    var connectionObject = YAHOO.util.Connect.asyncRequest("POST", path, callback);
}

function jChangeContent(objID, new_content) 
{
	var select_scheme_box=$("[name^='select_scheme']");
	for(var i =0; i < select_scheme_box.length; i++)
	{
		temp ="";  
	}
	//alert(objID)
	//var obj = document.getElementById(objID);
	//if(obj != null)
	//{
	//	obj.innerHTML = new_content;
		MM_showHideLayers('scheme_dtl_box','','hide');
		jCHECK_GRADING_SCHEME_ROW();
	//}
}

// this function was called in form_setting_aj_update.php
function RefreshSelectScheme(action,sc_id,sc_value,P_Cnt,F_Cnt,D_Cnt)
{

	switch (action)
	{
		case "SAVEAS": 
			$("select[name^='select_scheme_']").append("<option value='"+sc_id+"'>"+sc_value+"</option>");
			$("select#scheme_template").append("<option value='"+sc_id+"'>"+sc_value+"</option>");
			$("#scheme_global").append("<option value='"+sc_id+"'>"+sc_value+"</option>");
			$("#div_form_settings").append("<input type='hidden' name='schemeID[]' value='"+sc_id+"'/>");
			$("#div_form_settings").append("<input type='hidden' name='P_Cnt_"+sc_id+"' id='P_Cnt_"+sc_id+"' value='"+P_Cnt+"'/>");
			$("#div_form_settings").append("<input type='hidden' name='F_Cnt_"+sc_id+"' id='F_Cnt_"+sc_id+"' value='"+F_Cnt+"'/>");
			$("#div_form_settings").append("<input type='hidden' name='D_Cnt_"+sc_id+"' id='D_Cnt_"+sc_id+"' value='"+D_Cnt+"'/>");
			AllSchemeIDNameMap[sc_id] = sc_value;
			
			$("select[name='"+CurrentEditingSelection+"']").val(sc_id);
			
		break;
		
		case "DELETE":
		 	$("select[name^='select_scheme_']").find("option[value='"+sc_id+"']").remove();
		 	$("select#scheme_template").find("option[value='"+sc_id+"']").remove();
		 	$("#scheme_global").find("option[value='"+sc_id+"']").remove();
		 	$("input[name$='_Cnt_"+sc_id+"']").remove();
		 	$("input[name='schemeID[]'][value='"+sc_id+"']").remove();
		 	AllSchemeIDNameMap[sc_id] = '';
		break;
		
		case "SAVE":
		 	$("[name^='select_scheme_']").find("option[value='"+sc_id+"']").html(sc_value);
		 	$("select#scheme_template").find("option[value='"+sc_id+"']").html(sc_value);
		 	AllSchemeIDNameMap[sc_id] = sc_value;
		break;
	}
	MM_showHideLayers('scheme_dtl_box','','hide');
	//jCHECK_GRADING_SCHEME_ROW();			
	

}
// End of AJAX

// Copy ClassLevel Settings 
function jSHOW_COPY_CLASSLEVEL_SETTINGS(){
	var objName = "copyClassLevelSettingsDiv";
	var SettingDiv = document.getElementById(objName);
	var obj = document.getElementById("copyTD");
	var subjectIDObj = document.getElementsByName("subjectID[]");
	
	if (subjectIDObj.length == 0) {
		alert("<?=$eReportCard['jsAlertNoSubjectAssigned']?>");
		return;
	}
	
//	var left = getPostion(obj, 'offsetLeft');
//	var top = getPostion(obj, 'offsetTop') + 15;
//	
//	if(left+$("#toReportID").width() > $(window).width())
//		SettingDiv.style.left = (left-$("#toReportID").width())+'px';
//	else
//		SettingDiv.style.left = left + 'px';
//	SettingDiv.style.top = top + 'px';
	rePositionSettingDiv();
	
	MM_showHideLayers(objName,'','show');
	
}

function rePositionSettingDiv()
{
	var objName = "copyClassLevelSettingsDiv";
	var SettingDiv = document.getElementById(objName);
	var obj = document.getElementById("copyTD");
	
	var left = getPostion(obj, 'offsetLeft');
	var top = getPostion(obj, 'offsetTop') + 15;
	
	if(left+$("#toReportID").width() > $(window).width())
		SettingDiv.style.left = ($(window).width()-50-$("#toReportID").width())+'px';
	else
		SettingDiv.style.left = left + 'px';
	SettingDiv.style.top = top + 'px';
}

function jCOPY_CLASSLEVEL_SETTINGS(){
	var obj = document.FormCopy;
	var toReportID = $("select#toReportID").val();
	
	if(!toReportID)
	{
		if(confirm("<?=$eReportCard['jsConfirmArr']['GradingSchemeWillBeCovered']?>"))
			obj.submit();		
	}
	else
	{
		if(confirm("<?=$eReportCard['jsConfirmArr']['ReportGradingSchemeWillBeCovered']?>"))
			obj.submit();
	}
}

function jsApplyToAll(targetValueElement, targetClass)
{
	var targetValue = $('#' + targetValueElement).val();
	
	$('.' + targetClass).each( function () {
		$(this).val(targetValue);
	});
}

function jsApplyToAllRadio(targetMarkElement, targetMarkClass, targetGradeElement, targetGradeClass)
{
	var targetMarkValue = $('input#' + targetMarkElement).attr('checked');
	$('.' + targetMarkClass).each( function () {
		$(this).attr('checked', targetMarkValue);
	});
	
	var targetGradeValue = $('input#' + targetGradeElement).attr('checked');
	$('.' + targetGradeClass).each( function () {
		$(this).attr('checked', targetGradeValue);
	});
}

</script>

<br/>

<?php	
  // Prepare Javascript Global variables  
  $subStr = "var subjectArr = new Array(";
  if(count($SubjectDataArr) > 0){
    $i = 0;
	foreach($SubjectDataArr as $SubjectID => $Data){
		if($i > 0)
			$subStr .= ", ";
		$subStr .= "'".$SubjectID."'";
		$i++;
	}
  }
  $subStr .= ");";
  
  $schStr = "var schemeArr = new Array(";
  if(count($AllSchemeData) > 0){
	  for($i=0 ; $i<count($AllSchemeData) ; $i++){
		  if($i > 0)
			$schStr .= ", ";
		  $schStr .= "'".$AllSchemeData[$i]['SchemeID']."'";
	  }
  }
  $schStr .= ");";
  
  $js_global_var = '<script language="javascript">';
  $js_global_var .= $subStr."\n";
  $js_global_var .= $schStr."\n";
  $js_global_var .= '</script>';
?>

<!-- Start of Copy ClassLevel Settings Div -->
<div id="copyClassLevelSettingsDiv" style="position:absolute; width:200px; z-index:2; visibility: hidden;" onMouseOver="MM_showHideLayers('copyClassLevelSettingsDiv','','show')">
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td height="19">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="5" height="19"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/can_board_01.gif" width="5" height="19"></td>
          <td height="19" valign="middle" background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/can_board_02.gif">&nbsp;</td>
          <td width="19" height="19"><a href="javascript:void(0);" onClick="MM_showHideLayers('copyClassLevelSettingsDiv','','hide')"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/can_board_close_off.gif" name="pre_close21" width="19" height="19" border="0" id="pre_close21" onMouseOver="MM_swapImage('pre_close21','','<?="{$image_path}/{$LAYOUT_SKIN}"?>/can_board_close_on.gif',1)" onMouseOut="MM_swapImgRestore()"></a></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td>
    <form id="FormCopy" name="FormCopy" action="form_settings_copy_update.php" method="post" >
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="5" background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/can_board_04.gif"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/can_board_04.gif" width="5" height="19"></td>
          <td bgcolor="#FFFFF7">
				<?=$CopyFormSelectStr?>
              <table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr> 
                  <td height="5" class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="5"></td>
                </tr>
                <tr>
                  <td><table width="100%" border="0" cellspacing="0" cellpadding="2">
                      <tr>
                        <td>&nbsp;</td>
                        <td align="center" valign="bottom">
                        <?= $linterface->GET_SMALL_BTN($button_submit, "button", "jCOPY_CLASSLEVEL_SETTINGS()") ?>
                        <?= $linterface->GET_SMALL_BTN($button_reset, "reset") ?>
                      </tr>
                  </table></td>
                </tr>
              </table>
            </td>
          <td width="6" background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/can_board_06.gif"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/can_board_06.gif" width="6" height="6"></td>
        </tr>
        <tr>
          <td width="5" height="6"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/can_board_07.gif" width="5" height="6"></td>
          <td height="6" background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/can_board_08.gif"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/can_board_08.gif" width="5" height="6"></td>
          <td width="6" height="6"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/can_board_09.gif" width="6" height="6"></td>
        </tr>
      </table>
      <input type="hidden" name="frClassLevel" value="<?=$ClassLevelID?>">
      <input type="hidden" name="frReportID" value="<?=$ReportID?>">
    </form>
    </td>
  </tr>
</table>
</div>
<!-- End of Copy ClassLevel Settings Div-->


<form name="FormMain" id="FormMain" method="POST" action="<?=$UpdatePage;?>">

<!-- Start of Selection Box of Grading Scheme use as a Template -->
<?php
$sc_template_str = '<div id="div_scheme_template" style="display:none">';
$sc_template_str .= '<select id="scheme_template" name="scheme_template">';
$sc_template_str .= $select;
$sc_template_str .= '</select>';
$sc_template_str .= '</div>';
?>
<!-- End of Selection Box of Grading Scheme use as a Template -->

<!-- add Ajax Start -->
<?
$div_str3 .='<div id="scheme_box" style="position:absolute; width:auto; z-index:1; visibility: hidden; border-top:solid 1px; border-top-color:#999999; border-left:solid 1px; border-left-color:#999999; border-right:solid 2px; border-right-color:#646464; border-bottom:solid 2px; border-bottom-color:#646464">';
$div_str3 .='</div>';
?>
<!-- add Ajax End -->

<!-- Start of grading scheme div for view -->
<?php
/*
$div_str1 = '';
for($i=0 ; $i<count($AllSchemeData) ; $i++){
	$div_str1 .= '<div id="scheme_'.$AllSchemeData[$i]['SchemeID'].'" style="position:absolute; width:280px; z-index:1; visibility: hidden; border-top:solid 1px; border-top-color:#999999; border-left:solid 1px; border-left-color:#999999; border-right:solid 2px; border-right-color:#646464; border-bottom:solid 2px; border-bottom-color:#646464">';
    $div_str1 .= '<table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#feffed">';
    $div_str1 .= '<tr><td>';
    $div_str1 .= '<table width="100%" border="0" cellspacing="0" cellpadding="5">';
    $div_str1 .= '<tr><td>';
    
    if($AllSchemeData[$i]['SchemeType'] == "H"){
	    $DistinctMarkData = array();
	    $PassMarkData = array();
	    $FailMarkData = array();
	    
	    $DistinctMarkData = $lreportcard->GET_GRADING_SCHEME_RANGE_INFO($AllSchemeData[$i]['SchemeID'], 'D');
	    $PassMarkData = $lreportcard->GET_GRADING_SCHEME_RANGE_INFO($AllSchemeData[$i]['SchemeID'], 'P');
	    $FailMarkData = $lreportcard->GET_GRADING_SCHEME_RANGE_INFO($AllSchemeData[$i]['SchemeID'], 'F');
	    
	    // Full Mark
	    $full_mark = ($AllSchemeData[$i]['FullMark'] != "") ? $lreportcard->ReturnTextwithStyle($AllSchemeData[$i]['FullMark'], 'HighLight', 'Distinction') : '&nbsp;';
	    
	    // Distinction
	    if(count($DistinctMarkData) > 0){
		    $distinct_mark = '<div id="div_D_MR_tips_'.$AllSchemeData[$i]['SchemeID'].'" style="display:'.(($AllSchemeData[$i]['TopPercentage'] == 0) ? "block" : "none").'">';
		    $distinct_mark .= (($DistinctMarkData[0]['LowerLimit'] != "") ? $lreportcard->ReturnTextwithStyle($DistinctMarkData[0]['LowerLimit'], 'HighLight', 'Distinction') : "&nbsp;").'</div>';
		    $distinct_mark .= '<div id="div_D_PR_tips_'.$AllSchemeData[$i]['SchemeID'].'" style="display:'.(($AllSchemeData[$i]['TopPercentage'] != 0) ? "block" : "none").'">';
		    $distinct_mark .= (($DistinctMarkData[0]['UpperLimit'] != "") ? $lreportcard->ReturnTextwithStyle($DistinctMarkData[0]['UpperLimit'], 'HighLight', 'Distinction') : "&nbsp;").' %</div>';
		    $distinct_grade = ($DistinctMarkData[0]['Grade'] != "") ? $lreportcard->ReturnTextwithStyle($DistinctMarkData[0]['Grade'], 'HighLight', 'Distinction') : "&nbsp;";
		}
	    
	    // Pass
	    $pass_str = '';
	    if(count($PassMarkData) > 0){
		    for($k=0 ; $k<count($PassMarkData) ; $k++){
			    $pass_mark = '<div id="div_P_MR_tips_'.$AllSchemeData[$i]['SchemeID'].'_'.$k.'" style="display:'.(($AllSchemeData[$i]['TopPercentage'] == 0) ? "block" : "none").'">';
			    $pass_mark .= (($PassMarkData[$k]['LowerLimit'] != "") ? $lreportcard->ReturnTextwithStyle($PassMarkData[$k]['LowerLimit'], 'HighLight', 'Pass') : "&nbsp;").'</div>';
			    $pass_range = '<div id="div_P_PR_tips_'.$AllSchemeData[$i]['SchemeID'].'_'.$k.'" style="display:'.(($AllSchemeData[$i]['TopPercentage'] != 0) ? "block" : "none").'">';
			    $pass_range .= (($PassMarkData[$k]['UpperLimit'] != "") ? $lreportcard->ReturnTextwithStyle($PassMarkData[$k]['UpperLimit'], 'HighLight', 'Pass') : "&nbsp;").' %</div>';
			    $pass_grade = ($PassMarkData[$k]['Grade'] != "") ? $lreportcard->ReturnTextwithStyle($PassMarkData[$k]['Grade'], 'HighLight', 'Pass') : "&nbsp;";
			    
			    $pass_str .= '<tr>';
			    $pass_str .= '<td class="tabletext">'.(($k==0) ? $eReportCard['Pass'] : '').'</td>';
		        $pass_str .= '<td align="center" class="tabletext formfieldtitle" nowrap="nowrap">'.$pass_mark.$pass_range.'</td>';
		        $pass_str .= '<td align="center" class="tabletext formfieldtitle">'.$pass_grade.'</td>';
		        $pass_str .= '</tr>';
	    	}
		}
		else {
			$pass_str .= '<tr>';
		    $pass_str .= '<td class="tabletext">'.$eReportCard['Pass'].'</td>';
	        $pass_str .= '<td align="center" class="tabletext formfieldtitle">-</td>';
	        $pass_str .= '<td align="center" class="tabletext formfieldtitle">-</td>';
	        $pass_str .= '</tr>';
		}
		
	    // Fail
	    $fail_str = '';
	    if(count($FailMarkData) > 0){
		    for($k=0 ; $k<count($FailMarkData) ; $k++){
		    	$fail_mark = '<div id="div_F_MR_tips_'.$AllSchemeData[$i]['SchemeID'].'_'.$k.'" style="display:'.(($AllSchemeData[$i]['TopPercentage'] == 0) ? "block" : "none").'">';
			    $fail_mark .= (($FailMarkData[$k]['LowerLimit'] != "") ? $lreportcard->ReturnTextwithStyle($FailMarkData[$k]['LowerLimit'], 'HighLight', 'Fail') : "&nbsp;").'</div>';
			    $fail_range = '<div id="div_F_PR_tips_'.$AllSchemeData[$i]['SchemeID'].'_'.$k.'" style="display:'.(($AllSchemeData[$i]['TopPercentage'] != 0) ? "block" : "none").'">';
			    $fail_range .= (($FailMarkData[$k]['UpperLimit'] != "") ? $lreportcard->ReturnTextwithStyle($FailMarkData[$k]['UpperLimit'], 'HighLight', 'Fail') : "&nbsp;").' %</div>';
			    $fail_grade = ($FailMarkData[$k]['Grade'] != "") ? $lreportcard->ReturnTextwithStyle($FailMarkData[$k]['Grade'], 'HighLight', 'Fail') : "&nbsp;";
			    
			    $fail_str .= '<tr>';
			    $fail_str .= '<td class="tabletext">'.(($k==0) ? $eReportCard['Fail'] : '').'</td>';
		        $fail_str .= '<td align="center" class="tabletext formfieldtitle" nowrap="nowrap">'.$fail_mark.$fail_range.'</td>';
		        $fail_str .= '<td align="center" class="tabletext formfieldtitle">'.$fail_grade.'</td>';
		        $fail_str .= '</tr>';
	    	}
    	}
    	else {
			$fail_str .= '<tr>';
		    $fail_str .= '<td class="tabletext">'.$eReportCard['Fail'].'</td>';
	        $fail_str .= '<td align="center" class="tabletext formfieldtitle">-</td>';
	        $fail_str .= '<td align="center" class="tabletext formfieldtitle">-</td>';
	        $fail_str .= '</tr>';
		}
	    
    	$div_str1 .= '<table width="100%" border="0" cellspacing="0" cellpadding="3">';
    	$div_str1 .= '<tr><td class="tabletext formfieldtitle">'.$eReportCard['SchemesFullMark'].'</td>';
        $div_str1 .= '<td class="formfieldtitle tabletext" colspan="2">'.$full_mark.'</td></tr>';
        
        $div_str1 .= '<tr><td width="25%" class="tabletext">&nbsp;</td>';
        $div_str1 .= '<td width="45%" align="center" class="tabletop tabletopnolink">';
        $div_str1 .= '<div id="div_title1_MR_tips_'.$AllSchemeData[$i]['SchemeID'].'" style="display:'.(($AllSchemeData[$i]['TopPercentage'] == 0) ? "block" : "none").'">'.$eReportCard['LowerLimit'].'</div>';
        $div_str1 .= '<div id="div_title1_PR_tips_'.$AllSchemeData[$i]['SchemeID'].'" style="display:'.(($AllSchemeData[$i]['TopPercentage'] != 0) ? "block" : "none").'">'.$eReportCard['StudentsFromTop_nowrap'].'</div>';
        $div_str1 .= '</td>';
        $div_str1 .= '<td width="30%" align="center" class="tabletop tabletopnolink">'.$eReportCard['Grade'].'</td></tr>';
        
        $div_str1 .= '<tr><td class="tabletext formfieldtitle" width="25%">'.$eReportCard['Distinction'].'</td>';
        $div_str1 .= '<td align="center" class="tabletext formfieldtitle" width="45%">'.$distinct_mark.'</td>';
        $div_str1 .= '<td align="center" class="tabletext formfieldtitle"with="30%">'.$distinct_grade.'</td>';
        $div_str1 .= '</tr>';
        
        $div_str1 .= '<tr><td width="25%" class="tabletext">&nbsp;</td>';
        $div_str1 .= '<td width="45%" align="center" class="tabletext">';
        $div_str1 .= '<div id="div_title2_MR_tips_'.$AllSchemeData[$i]['SchemeID'].'" style="display:'.(($AllSchemeData[$i]['TopPercentage'] == 0) ? "block" : "none").'">'.$eReportCard['LowerLimit'].'</div>';
        $div_str1 .= '<div id="div_title2_PR_tips_'.$AllSchemeData[$i]['SchemeID'].'" style="display:'.(($AllSchemeData[$i]['TopPercentage'] != 0) ? "block" : "none").'">'.$eReportCard['Next'].'</div>';
        $div_str1 .= '</td>';
        $div_str1 .= '<td width="30%" align="center" class="tabletext">'.$eReportCard['Grade'].'</td></tr>';
        
        $div_str1 .= $pass_str;
        $div_str1 .= $fail_str;
        $div_str1 .= '</table>';
	    
	}
	else if($AllSchemeData[$i]['SchemeType'] == "PF"){
    	$div_str1 .= '<table width="100%" border="0" cellspacing="0" cellpadding="3">';
        $div_str1 .= '<tr><td class="tabletext" width="25%">'.$eReportCard['Pass'].'</td>';
        $div_str1 .= '<td class="style_distinction formfieldtitle" width="75%">'.(($AllSchemeData[$i]['Pass'] == "") ? '&nbsp;' : $AllSchemeData[$i]['Pass']).'</td></tr>';
        $div_str1 .= '<tr><td class="tabletext">'.$eReportCard['Fail'].'</td>';
        $div_str1 .= '<td class="style_distinction formfieldtitle">'.(($AllSchemeData[$i]['Fail'] == "") ? '&nbsp;' : $AllSchemeData[$i]['Fail']).'</td></tr>';
        $div_str1 .= '</table>';
	}
	$div_str1 .= '</td></tr>';
    $div_str1 .= '</table>';
    $div_str1 .= '</td></tr>';
    $div_str1 .= '</table>';
	$div_str1 .= '</div>';
	//echo $div_str1;
}*/
?>
<!-- End of div of grading scheme for view -->

<!-- add Ajax Start -->
<?
$div_str4 .='<div id="scheme_dtl_box" style="position:absolute; width:500px; z-index:1; visibility: hidden;">';
$div_str4 .='</div>';
?>
<!-- add Ajax End -->

<!-- Start of div of grading scheme details for edit-->
<?php 
$noSchemeFlag = 0 ;
$AllSchemeArr = array();
$AllSchemeArr[0]['SchemeID'] = "new";
$AllSchemeArr[0]['is_new'] = 1;
for($i=0 ; $i<count($AllSchemeData) ; $i++){
	$j = $i + 1;
	$AllSchemeArr[$j] = $AllSchemeData[$i];
	$AllSchemeArr[$j]['is_new'] = 0;
}

$div_str2 = '';
for($i=0 ; $i<count($AllSchemeArr) ; $i++){
	$isNewFlag = ($AllSchemeArr[$i]['is_new']) ? 1 : 0;

	$DistinctMarkData = array();
    $PassMarkData = array();
    $FailMarkData = array();
    $DistinctMarkData = $lreportcard->GET_GRADING_SCHEME_RANGE_INFO($AllSchemeArr[$i]['SchemeID'], 'D');
    $PassMarkData = $lreportcard->GET_GRADING_SCHEME_RANGE_INFO($AllSchemeArr[$i]['SchemeID'], 'P');
    $FailMarkData = $lreportcard->GET_GRADING_SCHEME_RANGE_INFO($AllSchemeArr[$i]['SchemeID'], 'F');
	
	$div_str2 .= '
		  <div id="scheme_dtl_'.$AllSchemeArr[$i]['SchemeID'].'" style="position:absolute; width:380px; z-index:1; visibility: hidden;">
		    <table width="100%" border="0" cellpadding="0" cellspacing="0">
			  <tr>
				<td height="19"><table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
					<td width="5" height="19"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_01.gif" width="5" height="19"></td>
					<td height="19" valign="middle" background="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_02.gif">&nbsp;</td>
					<td width="19" height="19"><a href="javascript:jHIDE_GRADING_SCHEME(\'\',\''.$AllSchemeArr[$i]['SchemeID'].'\', \'1\')"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_close_off.gif" name="pre_close1" width="19" height="19" border="0" id="pre_close1"></a></td>
				  </tr>
				</table></td>
			  </tr>
			  <tr>
				<td><table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
					<td width="5" background="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_04.gif"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_04.gif" width="5" height="19"></td>
					<td align="left" bgcolor="#FFFFF7"><table width="100%" border="0" cellspacing="0" cellpadding="0">
					  <tr>
						<td><table border="0" cellspacing="0" cellpadding="2" width="100%">
						  <tr>
							<td class="navigation"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/nav_arrow.gif" width="15" height="15" align="absmiddle">'.(($isNewFlag) ? $button_new : $button_edit).' '.$eReportCard['GradingScheme'].'</td>
							<td id="loadingScheme_'.$AllSchemeArr[$i]['SchemeID'].'"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/10x10.gif" height="18"></td>
						  </tr>
						</table></td>
					  </tr>
					  <tr>
						<td height="220" align="left" valign="top">';
						/*
						* Div : div_scheme_[SubjectID] , div_honor_based_content_[SubjectID] , div_passfail_based_content_[SubjectID]
						*		div_nature_title1_[SubjectID] , div_nature_title2_[SubjectID], div_nature_title3_[SubjectID]
						*/
						// Content of Scheme
	$div_str2 .= '		  <div id="div_scheme_dtl_'.$AllSchemeArr[$i]['SchemeID'].'" style="height:220px; width:370px; position:absolute; overflow: auto;">
							<div><table align="center" width="95%" border="0" cellpadding="5" cellspacing="0">
							  <tr>
								<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext">'.$eReportCard['GradingTitle'].':</span></td>
								<td width="70%" valign="top"><span class="tabletext"><strong><strong><input name="grading_title_'.$AllSchemeArr[$i]['SchemeID'].'" id="grading_title_'.$AllSchemeArr[$i]['SchemeID'].'" type="text" class="textboxtext" value="'.(($isNewFlag || $AllSchemeArr[$i]['SchemeTitle'] == "") ? '' : $AllSchemeArr[$i]['SchemeTitle']).'"></strong></strong></span></td>
							  </tr>
							  <tr>
							    <td valign="top" nowrap="nowrap" class="formfieldtitle" style="border-bottom:1px #EEEEEE solid;"><span class="tabletext">'.$eReportCard['GradingType'].'</span></td>
							    <td valign="top"><span class="tabletext">
								  <input type="radio" name="grading_type_'.$AllSchemeArr[$i]['SchemeID'].'" id="grading_type_'.$AllSchemeArr[$i]['SchemeID'].'_0" value="H" onclick="jSET_GRADING_TYPE(\''.$AllSchemeArr[$i]['SchemeID'].'\')" '.(($isNewFlag || $AllSchemeArr[$i]['SchemeType'] == "H") ? "checked" : "").'><label for="grading_type_'.$AllSchemeArr[$i]['SchemeID'].'_0">'.$eReportCard['HonorBased'].'</label>&nbsp;&nbsp;<br>
								  <input type="radio" name="grading_type_'.$AllSchemeArr[$i]['SchemeID'].'" id="grading_type_'.$AllSchemeArr[$i]['SchemeID'].'_1" value="PF" onclick="jSET_GRADING_TYPE(\''.$AllSchemeArr[$i]['SchemeID'].'\')" '.(($AllSchemeArr[$i]['SchemeType'] == "PF") ? "checked" : "").'><label for="grading_type_'.$AllSchemeArr[$i]['SchemeID'].'_1">'.$eReportCard['PassFailBased'].'</label> 
								</span></td>
							  </tr>
							</table></div>
							<div id="div_honor_based_content_'.$AllSchemeArr[$i]['SchemeID'].'" style="display:'.(($isNewFlag || $AllSchemeArr[$i]['SchemeType'] == "H") ? 'block' : 'none').'"><table align="center" width="95%" border="0" cellpadding="5" cellspacing="0">
							  <tr>
                                <td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext">'.$eReportCard['GradingRange'].'</span></td>
                                <td class="tabletext">
                                  <input type="radio" name="grading_range_'.$AllSchemeArr[$i]['SchemeID'].'" id="grading_range_'.$AllSchemeArr[$i]['SchemeID'].'_0" value="0" onclick="jSET_GRADING_RANGE(\''.$AllSchemeArr[$i]['SchemeID'].'\')" '.(($AllSchemeArr[$i]['TopPercentage'] == 0) ? "checked" : "").'><label for="grading_range_'.$AllSchemeArr[$i]['SchemeID'].'_0">'.$eReportCard['MarkRange'].'</label><br>
								  <input type="radio" name="grading_range_'.$AllSchemeArr[$i]['SchemeID'].'" id="grading_range_'.$AllSchemeArr[$i]['SchemeID'].'_1" value="1" onclick="jSET_GRADING_RANGE(\''.$AllSchemeArr[$i]['SchemeID'].'\')" '.(($AllSchemeArr[$i]['TopPercentage'] == 1) ? "checked" : "").'><label for="grading_range_'.$AllSchemeArr[$i]['SchemeID'].'_1">'.$eReportCard['PercentageRangeForAllStudents'].'</label><br>
								  <input type="radio" name="grading_range_'.$AllSchemeArr[$i]['SchemeID'].'" id="grading_range_'.$AllSchemeArr[$i]['SchemeID'].'_2" value="2" onclick="jSET_GRADING_RANGE(\''.$AllSchemeArr[$i]['SchemeID'].'\')" '.(($AllSchemeArr[$i]['TopPercentage'] == 2) ? "checked" : "").'><label for="grading_range_'.$AllSchemeArr[$i]['SchemeID'].'_2">'.$eReportCard['PercentageRangeForPassedStudents'].'</label>
								</td>
                              </tr>
							  <tr>
							    <td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext">'.$eReportCard['SchemesFullMark'].'</span></td>
							    <td width="70%" class="tabletext"><strong><strong><input name="grading_fullmark_'.$AllSchemeArr[$i]['SchemeID'].'" id="grading_fullmark_'.$AllSchemeArr[$i]['SchemeID'].'" type="text" class="textboxnum" value="'.(($noSchemeFlag || $AllSchemeArr[$i]['FullMark'] == "") ? '' : $AllSchemeArr[$i]['FullMark']).'"></strong></strong></td>
							  </tr>
							  <tr>
							    <td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext">'.$eReportCard['SchemesPassingMark'].'</span></td>
							    <td width="70%" class="tabletext"><strong><strong><input name="grading_passingmark_'.$AllSchemeArr[$i]['SchemeID'].'" id="grading_passingmark_'.$AllSchemeArr[$i]['SchemeID'].'" type="text" class="textboxnum" value="'.(($noSchemeFlag || $AllSchemeArr[$i]['PassMark'] == "") ? '' : $AllSchemeArr[$i]['PassMark']).'"></strong></strong></td>
							  </tr>
							  <tr>
								<td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext">'.$eReportCard['Distinction'].'</span></td>
								<td class="tabletext"><table width="100%" border="0" cellspacing="0" cellpadding="2">
								  <tr>
								    <td>
								      <table border="0" cellpadding="2" cellspacing="0" width="100%">';
					/*$div_str2 .= '       <tr>
										  <td width="32%" class="tabletext">
										    <div id="div_naturetitle1_MR_'.$AllSchemeArr[$i]['SchemeID'].'" style="display:'.(($AllSchemeArr[$i]['TopPercentage'] == 0) ? "block" : "none").'">'.$eReportCard['LowerLimit'].'</div>
										    <div id="div_naturetitle1_PR_'.$AllSchemeArr[$i]['SchemeID'].'" style="display:'.(($AllSchemeArr[$i]['TopPercentage'] != 0) ? "block" : "none").'">'.$eReportCard['StudentsFromTop'].'</div>
										  </td>
										  <td width="31%" valign="top" align="center" class="tabletext"> '.$eReportCard['Grade'].'</td>
										  <td width="32%" valign="top" class="tabletext">'.$eReportCard['GradePoint'].'</td>
										  <td style="width:8px">&nbsp;</td>
									  	</tr>';*/
				if(count($DistinctMarkData) > 0){									  
					for($j=0 ; $j<count($DistinctMarkData) ; $j++){	
						$div_str2 .= '<tr id="tr_D_'.$AllSchemeArr[$i]['SchemeID'].'_'.$j.'">
										<td width="32%"><strong><strong>
										  <input type="hidden" name="RangeID_D_'.$AllSchemeArr[$i]['SchemeID'].'_'.$j.'" id="RangeID_D_'.$AllSchemeArr[$i]['SchemeID'].'_'.$j.'" value="'.$DistinctMarkData[$j]['GradingSchemeRangeID'].'"/>
										  <div id="div_D_MR_'.$AllSchemeArr[$i]['SchemeID'].'_'.$j.'" style="display:'.(($AllSchemeArr[$i]['TopPercentage'] == 0) ? "block" : "none").'">
										    <input name="D_MR_'.$AllSchemeArr[$i]['SchemeID'].'_'.$j.'_a" type="text" class="textboxtext" value="'.$DistinctMarkData[$j]['LowerLimit'].'">
										  </div>
										  <div id="div_D_PR_'.$AllSchemeArr[$i]['SchemeID'].'_'.$j.'" style="display:'.(($AllSchemeArr[$i]['TopPercentage'] != 0) ? "block" : "none").'"><span class="retablerow1_total tabletext">
										    <input name="D_PR_'.$AllSchemeArr[$i]['SchemeID'].'_'.$j.'_a" type="text" class="ratebox" value="'.$DistinctMarkData[$j]['UpperLimit'].'"></span><span class="tabletext">%</span>
										  </div>
										</strong></strong></td>
										<td width="31%"><strong><strong><input name="D_'.$AllSchemeArr[$i]['SchemeID'].'_'.$j.'_b" id="D_'.$AllSchemeArr[$i]['SchemeID'].'_'.$j.'_b" type="text" class="textboxtext" value="'.$DistinctMarkData[$j]['Grade'].'"></strong></strong></td>
										<td width="32%"><strong><strong><input name="D_'.$AllSchemeArr[$i]['SchemeID'].'_'.$j.'_c" id="D_'.$AllSchemeArr[$i]['SchemeID'].'_'.$j.'_c" type="text" class="textboxtext" value="'.$DistinctMarkData[$j]['GradePoint'].'"></strong></strong></td>
										<td style="width:8px"><a href="javascript:jREMOVE_GRADING_SCHEME_ROW(\'P\', \''.$AllSchemeArr[$i]['SchemeID'].'\', \''.$j.'\')" class="contenttool">X</a></td>
									  </tr>';
					}
				}									  	
					$div_str2 .= '   </table>
									</td>
								  </tr>
								  <tr>
								    <td>
								      <table id="tb_D_'.$AllSchemeArr[$i]['SchemeID'].'" border="0" cellpadding="2" cellspacing="0" width="100%">
								        <tr>
										  <td width="32%"><strong><strong>
										    <input type="hidden" name="RangeID_D_'.$AllSchemeArr[$i]['SchemeID'].'" id="RangeID_D_'.$AllSchemeArr[$i]['SchemeID'].'" value="'.((count($DistinctMarkData) > 0) ? $DistinctMarkData[0]['GradingSchemeRangeID'] : '').'"/>
										    <div id="div_D_MR_'.$AllSchemeArr[$i]['SchemeID'].'" style="display:'.(($AllSchemeArr[$i]['TopPercentage'] == 0) ? "block" : "none").'">
										      <input name="D_MR_'.$AllSchemeArr[$i]['SchemeID'].'_a" id="D_MR_'.$AllSchemeArr[$i]['SchemeID'].'_a" type="text" class="textboxtext" value="'.((count($DistinctMarkData) > 0) ? $DistinctMarkData[0]['LowerLimit'] : '').'">
										    </div>
										    <div id="div_D_PR_'.$AllSchemeArr[$i]['SchemeID'].'" style="display:'.(($AllSchemeArr[$i]['TopPercentage'] != 0) ? "block" : "none").'"><span class="retablerow1_total tabletext">
											  <input name="D_PR_'.$AllSchemeArr[$i]['SchemeID'].'_a" id="D_PR_'.$AllSchemeArr[$i]['SchemeID'].'_a" type="text" class="ratebox" value="'.((count($DistinctMarkData) > 0) ? $DistinctMarkData[0]['UpperLimit'] : '').'" ></span> <span class="tabletext">%</span>
											</div>
										  </strong></strong></td>
										  <td width="31%"><strong><strong><input name="D_'.$AllSchemeArr[$i]['SchemeID'].'_b" id="D_'.$AllSchemeArr[$i]['SchemeID'].'_b" type="text" class="textboxtext" value="'.((count($DistinctMarkData) > 0) ? $DistinctMarkData[0]['Grade'] : '').'"></strong></strong></td>
										  <td width="32%"><strong><strong><input name="D_'.$AllSchemeArr[$i]['SchemeID'].'_c" id="D_'.$AllSchemeArr[$i]['SchemeID'].'_c" type="text" class="textboxtext" value="'.((count($DistinctMarkData) > 0) ? $DistinctMarkData[0]['GradePoint'] : '').'"></strong></strong></td>
										  <td style="width:8px">&nbsp;</td>
									    </tr>
									  </table>
									</td>
								  </tr>
								</table></td>
							  </tr>
							  <tr>
								<td valign="top" nowrap="nowrap" class="formfieldtitle" style="border-bottom:1px #EEEEEE solid;"><span class="tabletext">'.$eReportCard['Pass'].'</span></td>
								<td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="2">
								  <tr>
								    <td>
								      <table border="0" cellpadding="2" cellspacing="0" width="100%">
								        <tr>
									      <td width="32%" class="tabletext">
										    <div id="div_naturetitle2_MR_'.$AllSchemeArr[$i]['SchemeID'].'" style="display:'.(($AllSchemeArr[$i]['TopPercentage'] == 0) ? "block" : "none").'">'.$eReportCard['LowerLimit'].'</div>
											<div id="div_naturetitle2_PR_'.$AllSchemeArr[$i]['SchemeID'].'" style="display:'.(($AllSchemeArr[$i]['TopPercentage'] != 0) ? "block" : "none").'">'.$eReportCard['Next'].'</div>
										  </td>
										  <td width="31%" align="center" class="tabletext"> '.$eReportCard['Grade'].'</td>
										  <td width="32%" class="tabletext">'.$eReportCard['GradePoint'].'</td>
										  <td style="width:8px">&nbsp;</td>
									    </tr>
									  </table>
									<a href="javascript:jADD_GRADING_SCHEME_ROW(\'D\',\''.$AllSchemeArr[$i]['SchemeID'].'\');" class="contenttool"><strong>+</strong> '.$eReportCard['AddMore'].'</a><LABEL for="grading_honor"></LABEL></td>
								  </tr>
								  <tr>
								    <td>
								      <table id="tb_P_'.$AllSchemeArr[$i]['SchemeID'].'" border="0" cellpadding="2" cellspacing="0" width="100%"><tbody>';
			/*
			* Order of Row / Box starting from "0"
			* Pass_[SubjectID]_[Order of row]_[Order of box]
			* [a: lower limit] ; [b: grade] ; [c:grade point]
			* Remark: the [Order of row] of box is strictly increasing
			*/
			if(count($PassMarkData) > 0){									  
				for($j=0 ; $j<count($PassMarkData) ; $j++){	
					$div_str2 .= '<tr id="tr_P_'.$AllSchemeArr[$i]['SchemeID'].'_'.$j.'">
									<td width="32%"><strong><strong>
									  <input type="hidden" name="RangeID_P_'.$AllSchemeArr[$i]['SchemeID'].'_'.$j.'" id="RangeID_P_'.$AllSchemeArr[$i]['SchemeID'].'_'.$j.'" value="'.$PassMarkData[$j]['GradingSchemeRangeID'].'"/>
									  <div id="div_P_MR_'.$AllSchemeArr[$i]['SchemeID'].'_'.$j.'" style="display:'.(($AllSchemeArr[$i]['TopPercentage'] == 0) ? "block" : "none").'">
									    <input name="P_MR_'.$AllSchemeArr[$i]['SchemeID'].'_'.$j.'_a" type="text" class="textboxtext" value="'.$PassMarkData[$j]['LowerLimit'].'">
									  </div>
									  <div id="div_P_PR_'.$AllSchemeArr[$i]['SchemeID'].'_'.$j.'" style="display:'.(($AllSchemeArr[$i]['TopPercentage'] != 0) ? "block" : "none").'"><span class="retablerow1_total tabletext">
									    <input name="P_PR_'.$AllSchemeArr[$i]['SchemeID'].'_'.$j.'_a" type="text" class="ratebox" value="'.$PassMarkData[$j]['UpperLimit'].'"></span><span class="tabletext">%</span>
									  </div>
									</strong></strong></td>
									<td width="31%"><strong><strong><input name="P_'.$AllSchemeArr[$i]['SchemeID'].'_'.$j.'_b" id="P_'.$AllSchemeArr[$i]['SchemeID'].'_'.$j.'_b" type="text" class="textboxtext" value="'.$PassMarkData[$j]['Grade'].'"></strong></strong></td>
									<td width="32%"><strong><strong><input name="P_'.$AllSchemeArr[$i]['SchemeID'].'_'.$j.'_c" id="P_'.$AllSchemeArr[$i]['SchemeID'].'_'.$j.'_c" type="text" class="textboxtext" value="'.$PassMarkData[$j]['GradePoint'].'"></strong></strong></td>
									<td style="width:8px"><a href="javascript:jREMOVE_GRADING_SCHEME_ROW(\'P\', \''.$AllSchemeArr[$i]['SchemeID'].'\', \''.$j.'\')" class="contenttool">X</a></td>
								  </tr>';
				}
			}
			$div_str2 .= '			  </tbody></table> 
									</td>
								  </tr>
							    </table><a href="javascript:jADD_GRADING_SCHEME_ROW(\'P\',\''.$AllSchemeArr[$i]['SchemeID'].'\');" class="contenttool"><strong>+</strong> '.$eReportCard['AddMore'].'</a><LABEL for="grading_honor"></LABEL></td>
							  </tr>
							  <tr>
								<td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext">'.$eReportCard['Fail'].'</span></td>
								<td class="tabletext"><table width="100%" border="0" cellspacing="0" cellpadding="2">
								  <tr>
								    <td>
								      <table border="0" cellpadding="2" cellspacing="0" width="100%">
								      	<tr>
										  <td width="32%" class="tabletext">
										    <div id="div_naturetitle3_MR_'.$AllSchemeArr[$i]['SchemeID'].'" style="display:'.(($AllSchemeArr[$i]['TopPercentage'] == 0) ? "block" : "none").'">'.$eReportCard['LowerLimit'].'</div>
											<div id="div_naturetitle3_PR_'.$AllSchemeArr[$i]['SchemeID'].'" style="display:'.(($AllSchemeArr[$i]['TopPercentage'] != 0) ? "block" : "none").'">'.$eReportCard['Next'].'</div>
										  </td>
										  <td width="31%" align="center" class="tabletext"> '.$eReportCard['Grade'].'</td>
										  <td width="32%" class="tabletext">'.$eReportCard['GradePoint'].'</td>
										  <td style="width:8px">&nbsp;</td>
									    </tr>
									  </table>
									</td>
								  </tr>
								  <tr>
								    <td>
								      <table id="tb_F_'.$AllSchemeArr[$i]['SchemeID'].'" border="0" cellpadding="2" cellspacing="0" width="100%"><tbody>';
			/*
			* Order of Row / Box starting from "0"
			* Fail_[SubjectID]_[Order of row]_[Order of box]
			* [a: lower limit] ; [b: grade] ; [c:grade point]
			*/
			if(count($FailMarkData) > 0){									  
				for($j=0 ; $j<count($FailMarkData) ; $j++){
					$div_str2 .= '<tr id="tr_F_'.$AllSchemeArr[$i]['SchemeID'].'_'.$j.'">
									<td width="32%"><strong><strong>
									  <input type="hidden" name="RangeID_F_'.$AllSchemeArr[$i]['SchemeID'].'_'.$j.'" id="RangeID_F_'.$AllSchemeArr[$i]['SchemeID'].'_'.$j.'" value="'.$FailMarkData[$j]['GradingSchemeRangeID'].'"/>
									  <div id="div_F_MR_'.$AllSchemeArr[$i]['SchemeID'].'_'.$j.'" style="display:'.(($AllSchemeArr[$i]['TopPercentage'] == 0) ? "block" : "none").'">
									    <input name="F_MR_'.$AllSchemeArr[$i]['SchemeID'].'_'.$j.'_a" type="text" class="textboxtext" value="'.$FailMarkData[$j]['LowerLimit'].'">
									  </div>
									  <div id="div_F_PR_'.$AllSchemeArr[$i]['SchemeID'].'_'.$j.'" style="display:'.(($AllSchemeArr[$i]['TopPercentage'] != 0) ? "block" : "none").'"><span class="retablerow1_total tabletext">
									    <input name="F_PR_'.$AllSchemeArr[$i]['SchemeID'].'_'.$j.'_a" type="text" class="ratebox" value="'.$FailMarkData[$j]['UpperLimit'].'"></span><span class="tabletext">%</span>
									  </div>
									</strong></strong></td>
									<td width="31%"><strong><strong><input name="F_'.$AllSchemeArr[$i]['SchemeID'].'_'.$j.'_b" id="F_'.$AllSchemeArr[$i]['SchemeID'].'_'.$j.'_b" type="text" class="textboxtext" value="'.$FailMarkData[$j]['Grade'].'"></strong></strong></td>
									<td width="32%"><strong><strong><input name="F_'.$AllSchemeArr[$i]['SchemeID'].'_'.$j.'_c" id="F_'.$AllSchemeArr[$i]['SchemeID'].'_'.$j.'_c" type="text" class="textboxtext" value="'.$FailMarkData[$j]['GradePoint'].'"></strong></strong></td>
									<td style="width:8px"><a href="javascript:jREMOVE_GRADING_SCHEME_ROW(\'F\', \''.$AllSchemeArr[$i]['SchemeID'].'\', \''.$j.'\')" class="contenttool">X</a></td>
								  </tr>';
				}
			}
			$div_str2 .= '			  </tbody></table> 
									</td>
								  </tr>
								</table><a href="javascript:jADD_GRADING_SCHEME_ROW(\'F\',\''.$AllSchemeArr[$i]['SchemeID'].'\');" class="contenttool"><strong>+</strong> '.$eReportCard['AddMore'].'</a></td>
							  </tr>
							</table></div>
							<div id="div_passfail_based_content_'.$AllSchemeArr[$i]['SchemeID'].'" style="display:'.(($AllSchemeArr[$i]['SchemeType'] == 'PF') ? 'block' : 'none').'"><table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
							  <tr>
								<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle" style="border-bottom:1px #EEEEEE solid;"><span class="tabletext">'.$eReportCard['Pass'].'</span></td>
								<td width="70%" valign="top"><span class="tabletext"><strong><strong><input name="grading_pass_'.$AllSchemeArr[$i]['SchemeID'].'" id="grading_pass_'.$AllSchemeArr[$i]['SchemeID'].'" class="textboxnum" type="text" value="'.(($isNewFlag || $AllSchemeArr[$i]['Pass'] == "") ? '' : $AllSchemeArr[$i]['Pass']).'"></strong></strong></span></td>
							  </tr>
							  <tr>
								<td valign="top" nowrap="nowrap" class="formfieldtitle" style="border-bottom:1px #EEEEEE solid;"><span class="tabletext">'.$eReportCard['Fail'].'</span></td>
								<td valign="top"><span class="tabletext"><strong><strong><input name="grading_fail_'.$AllSchemeArr[$i]['SchemeID'].'" id="grading_fail_'.$AllSchemeArr[$i]['SchemeID'].'" class="textboxnum" type="text" value="'.(($isNewFlag || $AllSchemeArr[$i]['Fail'] == "") ? '' : $AllSchemeArr[$i]['Fail']).'"></strong></strong></span></td>
							  </tr>
							</table></div>
						  </div>
						</td>
					  </tr>
					  <tr>
						<td><br><table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
						  <tr>
							<td height="1" class="dotline"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/10x10.gif" width="10" height="1"></td>
						  </tr>
						  <tr>
							<td><table width="99%" border="0" align="center" cellpadding="2" cellspacing="0">
							  <tr>
								<td>&nbsp;</td>
								<td align="center" valign="bottom">
								  '.(($isNewFlag) ? '' : '<input name="Save" id="SaveButton_'.$AllSchemeArr[$i]['SchemeID'].'" type="button" class="formsmallbutton" value="'.$button_save.'" onclick="jEDIT_GRADING_SCHEME(\'SAVE\', \''.$AllSchemeArr[$i]['SchemeID'].'\');">').'
								  <input name="SaveAs" id="SaveAsButton_'.$AllSchemeArr[$i]['SchemeID'].'" type="button" class="formsmallbutton" value="'.$button_save_as.'" onclick="jEDIT_GRADING_SCHEME(\'SAVEAS\', \''.$AllSchemeArr[$i]['SchemeID'].'\');">
								  <input name="Cancel" id="CancelButton_'.$AllSchemeArr[$i]['SchemeID'].'" type="button" class="formsmallbutton" value="'.$button_cancel.'" onclick="jHIDE_GRADING_SCHEME(\'\',\''.$AllSchemeArr[$i]['SchemeID'].'\', \'1\')">
								  '.(($isNewFlag) ? '' : '<input name="Delete" id="DeleteButton_'.$AllSchemeArr[$i]['SchemeID'].'" type="button" class="formsmallbutton" value="'.$button_remove.'" onclick="jEDIT_GRADING_SCHEME(\'DELETE\', \''.$AllSchemeArr[$i]['SchemeID'].'\')">').'
								</td>
							  </tr>
							</table></td>
						  </tr>
						</table></td>
					  </tr>
					</table></td>
					<td width="6" background="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_06.gif"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_06.gif" width="6" height="6"></td>
				  </tr>
				  <tr>
					<td width="5" height="6"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_07.gif" width="5" height="6"></td>
					<td height="6" background="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_08.gif"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_08.gif" width="5" height="6"></td>
					<td width="6" height="6"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_09.gif" width="6" height="6"></td>
				  </tr>
				</table></td>
			  </tr>
			</table>
		  </div>';
	
	# hide $div_str2 - function not yet finished
    //echo $div_str2;
}


### Create the grading scheme type js array
$jsGradingTypeArr = '';
$jsGradingTypeArr .= 'var jsGradingTypeArr = new Array();'."\n";
for($i=0 ; $i<count($AllSchemeData) ; $i++){
	$thisSchemeID = $AllSchemeArr[$i]['SchemeID'];
	$thisSchemeType = $AllSchemeArr[$i]['SchemeType'];
	
	$jsGradingTypeArr .= 'jsGradingTypeArr["'.$thisSchemeID.'"] = "'.$thisSchemeType.'";
	';
}

?>
<!-- End of div of grading scheme details for edit -->


<!--<form name="FormMain" method="POST" action="<?=$UpdatePage;?>">-->
<table width="98%" border="0" cellpadding="2" cellspacing="0">
  <tr>
    <td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
  </tr>
  <tr>
    <td align="right"> <?=$linterface->GET_SYS_MSG($Result);?></td>
  </tr>
  <tr> 
    <td align="center">
      <table width="100%" border="0" cellspacing="0" cellpadding="2">
        <tr>
          <td height="320" align="left" valign="top">
		    <table width="100%" border="0" cellspacing="0" cellpadding="4">
              <tr>
                <td width="15" align="left">&nbsp;</td>
                <td width="25%" align="right">
			      <table width="17" border="0" cellspacing="0" cellpadding="2">
	                <tr>
	                  <td><div id="btn_prevnext"><a href="<?="form_settings_edit.php?ClassLevelID=$prevForm"?>"><span><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/ecomm/icon_prev.gif" border="0" alt="<?=$eReportCard['PreviousForm']?>"></span></a></div></td>
	                </tr>
	              </table>
           		</td>
           		<td align="left" valign="top" class="retabletop tabletopnolink">
				  <table border="0" cellspacing="0" cellpadding="2">
					<tr>
					  <td colspan="2" class="tabletopnolink"><?=$ClassLevelSelect?><a href="#"></a><?=$ReportSelection?></td>
					  <td align="right">
						<table width="100%" border="0" cellspacing="0" cellpadding="1">
						  <tr>
							<td>&nbsp;</td>
							<td>
							  <table  border="0" cellspacing="0" cellpadding="0">
								<tr>
								  <!--<td><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_copy.gif" width="12" height="12" align="absmiddle"></td>-->
								  <td id="copyTD"><a href="javascript:jSHOW_COPY_CLASSLEVEL_SETTINGS();"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_copy.gif" alt="<?=$button_copy?>" width="12" height="12" align="absmiddle" border="0"></a></td>
								</tr>
							  </table>
							</td>
						  </tr>
						</table>
					  </td>
					</tr>
				  </table>
                </td>
                <td width="10" align="center">
                  <table width="17" border="0" cellspacing="0" cellpadding="2">
                    <tr>
                      <td><div id="btn_prevnext"><a href="<?="form_settings_edit.php?ClassLevelID=$nextForm"?>"><span><img src="<?="{$image_path}/{$LAYOUT_SKIN}" ?>/ecomm/icon_next.gif" border="0" alt="<?=$eReportCard['NextForm']?>"></span></a></div></td>
                    </tr>
                  </table>
                </td>
           	  </tr>
           	  <tr>
                <td align="left"><img src="<?="{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="13" height="2"></td>
                <td align="right" valign="top"><img src="<?="{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="13" height="2"></td>
                <td align="center" valign="top"><img src="<?="{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="13" height="2"></td>
                <td align="center" valign="top"><img src="<?="{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="13" height="2"></td>
              </tr>
           	</table>
           	
           	<div id="div_form_settings">
           	<?php
            	echo $js_global_var."\n";
            	echo $sc_template_str."\n";
            	//echo $div_str1."\n";
            	//echo $div_str2."\n";
            	echo $div_str3."\n";
            	echo $div_str4."\n";
           	?>
           	
           	<table id="tb_Form_Settings" width="100%" border="0" cellspacing="0" cellpadding="4">
              <tr>
                <td width="15" align="left">
                  <table border="0" cellpadding="0" cellspacing="0">
                    <tr><td><img src="<?="{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" name="move_up[]" id="move_up" width="13" height="13"></td></tr>
                    <tr><td><img src="<?="{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" name="move_down[]" id="move_down" width="13" height="13"></td></tr>
                  </table>
                </td>
                <td width="20%" align="right" valign="top">&nbsp;</td>
                <td width="20%" align="center" valign="middle" class="retabletop tabletopnolink" nowrap>
                	<?=$eReportCard['GradingScheme']?>
                	<br />
                	<br />
                	<?=$globalSchemeSelection?> <?=$Scheme_ApplyAllBtn?>
                </td>
                <td align="center" valign="middle" class="retabletop tabletopnolink" nowrap>
                	<?=$eReportCard['InputScaleType']?>
                	<br />
                	<br />
                	<?=$globalInputScale?> <?=$InputScale_ApplyAllBtn?>
                </td>
                <td align="center" valign="middle" class="retabletop tabletopnolink" nowrap>
                	<?=$eReportCard['ResultDisplayType']?>
                	<br />
                	<br />
                	<?=$globalDisplayScale?> <?=$DisplayScale_ApplyAllBtn?>
                </td>
                <?if($eRCTemplateSetting['SelectSubjectDisplayLang']) {?>
	                <td align="center" valign="middle" class="retabletop tabletopnolink" nowrap>
	                	<?=$eReportCard['LangDisplay']?>
	                	<br />
                		<br />
	                	<?=$globalLangDisplaySelection?> <?=$LangDisplay_ApplyAllBtn?>
	                </td>
                <?}?>
                <?if($eRCTemplateSetting['SelectDisplaySubjectGroup']) {?>
	                <td align="center" valign="middle" class="retabletop tabletopnolink" nowrap>
	                	<?=$eReportCard['DisplaySubjectGroup']?>
	                	<br />
                		<br />
	                	<?=$globalDisplaySubjectGroup?> <?=$DisplaySubjectGroup_ApplyAllBtn?>
	                </td>
                <?}?>
                
              </tr>
              <?=$rx?>
            </table>
            
            <input type="hidden" name="ActionCode" id="ActionCode" value=""/>
			<?php
			if(count($AllSchemeData) > 0){
				for($i=0 ; $i<count($AllSchemeData) ; $i++){
					$DistinctMarkData = array();
				    $PassMarkData = array();
				    $FailMarkData = array();
				    $DistinctMarkData = $lreportcard->GET_GRADING_SCHEME_RANGE_INFO($AllSchemeData[$i]['SchemeID'], 'D');
				    $PassMarkData = $lreportcard->GET_GRADING_SCHEME_RANGE_INFO($AllSchemeData[$i]['SchemeID'], 'P');
				    $FailMarkData = $lreportcard->GET_GRADING_SCHEME_RANGE_INFO($AllSchemeData[$i]['SchemeID'], 'F');
    
    				echo '<input type="hidden" name="D_Cnt_'.$AllSchemeData[$i]['SchemeID'].'" id="D_Cnt_'.$AllSchemeData[$i]['SchemeID'].'" value="'.count($DistinctMarkData).'"/>'."\n";
					echo '<input type="hidden" name="P_Cnt_'.$AllSchemeData[$i]['SchemeID'].'" id="P_Cnt_'.$AllSchemeData[$i]['SchemeID'].'" value="'.count($PassMarkData).'"/>'."\n";
					echo '<input type="hidden" name="F_Cnt_'.$AllSchemeData[$i]['SchemeID'].'" id="F_Cnt_'.$AllSchemeData[$i]['SchemeID'].'" value="'.count($FailMarkData).'"/>'."\n";
				}
			}
			echo '<input type="hidden" name="D_Cnt_new" id="D_Cnt_new" value="0"/>'."\n";
			echo '<input type="hidden" name="P_Cnt_new" id="P_Cnt_new" value="0"/>'."\n";
			echo '<input type="hidden" name="F_Cnt_new" id="F_Cnt_new" value="0"/>'."\n";
			
			for($i=0 ; $i<count($AllSchemeArr) ; $i++)
				echo '<input type="hidden" name="schemeID[]" id="schemeID_'.$i.'" value="'.$AllSchemeArr[$i]['SchemeID'].'"/>'."\n";
			?>
			
            </div>
            
          </td>
        </tr>
      </table>
      <br>
      <table width="90%" border="0" cellspacing="0" cellpadding="0">
      	<tr>
          <td height="1" class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
      	</tr>
      	<tr>
          <td><table width="100%" border="0" cellspacing="0" cellpadding="2">
            <tr>
              <td>&nbsp;</td>
              <td align="center" valign="bottom">
              <?php if(!empty($SubjectDataArr)){ ?>
                <input name="SaveMain" id="SaveMain" type="button" class="formbutton" onMouseOver="this.className='formbuttonon'" onMouseOut="this.className='formbutton'" value="<?=$button_save?>" onclick="jSUBMIT_FORM()">
              <?php } ?>
                <input name="Cancel" type="button" class="formbutton" onMouseOver="this.className='formbuttonon'" onMouseOut="this.className='formbutton'" value="<?=$button_cancel?>" onclick="location.href='form_settings.php?ClassLevelID=<?=$targetForm?>'">
              </td>
            </tr>
          </table></td>
      	</tr>
      </table>
    </td>
  </tr>
</table>

<input type="hidden" name="ClassLevelID" id="ClassLevelID" value="<?=$targetForm?>"/>

<?php
// newly added
$cnt_1 = 0;
$cnt_2 = 0;
if(!empty($SubjectDataArr)){
	foreach($SubjectDataArr as $SubjectID => $Data){
		if($Data['is_cmpSubject']){
			echo '<input type="hidden" name="cmpSubject'.$Data['parentSubjectID'].'[]" id="cmpSubject'.$Data['parentSubjectID'].'_'.$cnt_1.'" value="'.$SubjectID.'"/>'."\n";
			$cnt_1++;
		}
		else {
			echo '<input type="hidden" name="parentSubject[]" id="parentSubject_'.$cnt_2.'" value="'.$SubjectID.'"/>'."\n";
			$cnt_2++;
			$cnt_1 = 0;
		}
	}
}
?>

</form>
<script language="javascript" defer="1">

<?= $jsGradingTypeArr ?>
<?= $jsNextSchemeID ?>

function jCHECK_GRADING_SCHEME_ROW(){
	/*var sc_num = schemeArr.length;
	
	for(var i=0 ; i<sc_num ; i++){
		//alert(2 + ": " + schemeArr[i]);
		var pObj = document.getElementById("P_Cnt_" + schemeArr[i]);
		var fObj = document.getElementById("F_Cnt_" + schemeArr[i]);
		
		if(pObj && pObj.value == 0){
			jADD_GRADING_SCHEME_ROW1("P", schemeArr[i]);
			jADD_GRADING_SCHEME_ROW1("P", schemeArr[i]);
			jADD_GRADING_SCHEME_ROW1("P", schemeArr[i]);
		}
		
		if(fObj && fObj.value == 0){
			jADD_GRADING_SCHEME_ROW1("F", schemeArr[i]);
		}
	}*/
	
	// New Grading Scheme
	if(document.getElementById("tb_P_new")&&document.getElementById("P_Cnt_new").value == 0){
		jADD_GRADING_SCHEME_ROW1("P", "new");
		jADD_GRADING_SCHEME_ROW1("P", "new");
		jADD_GRADING_SCHEME_ROW1("P", "new");
	}
	
	if(document.getElementById("tb_F_new")&&document.getElementById("F_Cnt_new")&&document.getElementById("F_Cnt_new").value == 0){
		jADD_GRADING_SCHEME_ROW1("F", "new");
	}
}
//jCHECK_GRADING_SCHEME_ROW();
</script>


<?

        $linterface->LAYOUT_STOP();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>