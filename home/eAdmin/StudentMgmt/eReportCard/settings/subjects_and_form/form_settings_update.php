<?php
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
//include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");

intranet_auth();
intranet_opendb();

#################################################################################################

/* Temp Library*/
include_once($PATH_WRT_ROOT."includes/libreportcard2008j.php");
$lreportcard = new libreportcard2008j();
//$lreportcard = new libreportcard();

# Initialization
$SubjectIDArr = array();
$SchemeIDArr = array();

# Get Data
$SubjectIDArr = $subjectID;	// pass by array
$SchemeIDArr = $schemeID;	// pass by array

################################################################################################# 
# Form Subject Setting Data
# including Display Order, SubjectName, SubjectID, corresponding SchemeID, Input Scale, Display Result, IsCmpSubjectFlag
#################################################################################################
$SubjectInfo = array();

for($i=0 ; $i<count($SubjectIDArr) ; $i++){
	$SubjectInfo[$SubjectIDArr[$i]]['displayOrder'] = ${"display_order_".$SubjectIDArr[$i]};
	$SubjectInfo[$SubjectIDArr[$i]]['schemeID'] = ${"select_scheme_".$SubjectIDArr[$i]};
	$SubjectInfo[$SubjectIDArr[$i]]['scaleInput'] = ${"scale_".$SubjectIDArr[$i]};
	$SubjectInfo[$SubjectIDArr[$i]]['scaleDisplay'] = ${"display_".$SubjectIDArr[$i]};
	$SubjectInfo[$SubjectIDArr[$i]]['LangDisplay'] = ${"LangDisplay_".$SubjectIDArr[$i]};
	$SubjectInfo[$SubjectIDArr[$i]]['DisplaySubjectGroup'] = ${"Display_SubjGroup_".$SubjectIDArr[$i]};
	
}

## For Grand Mark
$GrandSchemeIndexArr = $lreportcard->Get_Grand_Mark_Grading_Scheme_Index_Arr($ClassLevelID);
foreach ($GrandSchemeIndexArr as $GrandMarkIndex => $GrandMarkType)
{
	$SubjectInfo[$GrandMarkIndex]['displayOrder'] = "";
	$SubjectInfo[$GrandMarkIndex]['schemeID'] = ${"select_scheme_".$GrandMarkType};
	$SubjectInfo[$GrandMarkIndex]['scaleInput'] = "";
	$SubjectInfo[$GrandMarkIndex]['scaleDisplay'] = ${"display_".$GrandMarkType};
}

# Main 
$result = array();
$SchemeData = array();
$SubjectData = array();

# Modified by Marcus 20101011 (for template based grading scheme) 
if(trim($ReportID)=='' || trim($ReportID)==0 )
{
	$ReportList = $lreportcard->Get_Report_List($ClassLevelID);
	$ReportIDArr = Get_Array_By_Key($ReportList, "ReportID");
	$ReportIDArr[] = 0;
}
else
{
	$ReportIDArr = array($ReportID);
}

foreach($SubjectInfo as $subjectID => $Data){
	if($Data['schemeID'] != "")
		$SchemeData = $lreportcard->GET_GRADING_SCHEME_MAIN_INFO($Data['schemeID']);
	
	$SubjectData['classLevelID'] = $ClassLevelID;
	$SubjectData['subjectID'] = $subjectID;
	$SubjectData['displayOrder'] = $Data['displayOrder'];
	$SubjectData['schemeID'] = $Data['schemeID'];
	$SubjectData['scaleInput'] = (count($SchemeData) > 0 && $SchemeData['SchemeType'] == "H") ? $Data['scaleInput'] : "";
	$SubjectData['scaleDisplay'] = (count($SchemeData) > 0 && $SchemeData['SchemeType'] == "H") ? $Data['scaleDisplay'] : "";
	$SubjectData['LangDisplay'] = $Data['LangDisplay'];
	$SubjectData['DisplaySubjectGroup'] = $Data['DisplaySubjectGroup'];
	
	foreach($ReportIDArr as $thisReportID)
	{
		$SubjectData['ReportID'] = $thisReportID;
		if ($lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $subjectID, 1, 0, $ReportID) == NULL)
		{
			$result['insert_sub_form_grading_'.$subjectID.'_'.$thisReportID] = $lreportcard->INSERT_SUBJECT_FORM_GRADING($SubjectData);
		}
		
		$result['update_sub_form_grading_'.$subjectID.'_'.$thisReportID] = $lreportcard->UPDATE_SUBJECT_FORM_GRADING($SubjectData);
	}
}


#################################################################################################
if(!in_array(false, $result))
	$msg = "update";
else 
	$msg = "update_failed";

#################################################################################################
intranet_closedb();

#header("Location: form_settings_edit.php?ClassLevelID=$ClassLevelID&Result=$msg");
header("Location: form_settings.php?ClassLevelID=$ClassLevelID&ReportID=$ReportID&Result=$msg");

?>