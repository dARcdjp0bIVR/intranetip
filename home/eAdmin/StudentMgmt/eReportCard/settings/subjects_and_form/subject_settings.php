<?php
/********************************************************
 * 	20151218 Bill:	[2015-1111-1214-16164]
 * 		- Display Subject Code when $sys_custom['Subject']['DisplaySubjectCode'] = true
 * 	20100113 Marcus:
 * 		- Re-enable scrollable table
 * ******************************************************/
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
//include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if ($plugin['ReportCard2008'])
{
	if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
	{
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
		$lreportcard = new libreportcardcustom();
	} else {
		$lreportcard = new libreportcard();
	}
	$CurrentPage = "Settings_SubjectsAndForms";
	$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
    if ($lreportcard->hasAccessRight())
    {
        $linterface = new interface_html();
      
		############################################################################################################
		
		# Get class level of school
		$FormArr = $lreportcard->GET_ALL_FORMS();
		
		# Preparation
		$colspan = 6;
		$UpdatePage = "";
		
		// Class level
		$fx = "";
		$CheckSubjectArr = array();
		$widthPercentage = floor(70 / count($FormArr));
		
		for($i=0 ; $i<count($FormArr) ; $i++){
			// Get whether each subject is checked for each ClassLevel(Form) or not
			$CheckSubjectArr[$FormArr[$i]['ClassLevelID']] = $lreportcard->CHECK_FROM_SUBJECT_GRADING($FormArr[$i]['ClassLevelID']);
			
		//for($i=0 ; $i<7 ; $i++){
			//$fx .= '<td id="td_sort_'.$FormArr[$i]['ClassLevelID'].'" align="center" class="tabletop tabletopnolink">';
			
			# Class Title at the top of the Table
			$fx .= '<th id="td_sort_'.$FormArr[$i]['ClassLevelID'].'" align="center" class="tabletop tabletopnolink" width="'.$widthPercentage.'%">';
				/*$fx .= '<table border="0" cellspacing="0" cellpadding="2" width="100%">';
					$fx .= '<tr><td class="tabletopnolink"><a href="form_settings_edit.php?ClassLevelID='.$FormArr[$i]['ClassLevelID'].'" class="tabletoplink">'.$FormArr[$i]['LevelName'].'</a></td></tr>';
					$fx .= '<tr><td align="right">';
						$fx .= '<table width="100%" border="0" cellspacing="0" cellpadding="1">';
							$fx .= '<tr><td><a href="javascript:jSIFT_FORM(\''.$FormArr[$i]['ClassLevelID'].'\')"><img id="SiftImg_'.$FormArr[$i]['ClassLevelID'].'" src="'.$image_path.'/'.$LAYOUT_SKIN.'/ereportcard/icon_sift.gif" alt="'.$eReportCard['ClickToSift'].' '.$FormArr[$i]['LevelName'].'" title="'.$eReportCard['ClickToSift'].' '.$FormArr[$i]['LevelName'].'" width="12" height="12" border="0" align="absmiddle"></a>';
								$fx .= '<input type="hidden" name="siftFlag_'.$FormArr[$i]['ClassLevelID'].'" id="siftFlag_'.$FormArr[$i]['ClassLevelID'].'" value="0"/></td>';
							$fx .= '<td>&nbsp;</td></tr>';
						$fx .= '</table>';
					$fx .= '</td></tr>';
				$fx .= '</table>';*/
				$fx .= '<div><a href="form_settings_edit.php?ClassLevelID='.$FormArr[$i]['ClassLevelID'].'" class="tabletoplink">'.$FormArr[$i]['LevelName'].'</a></div>';
				$fx .= '<div><a href="javascript:jSIFT_FORM(\''.$FormArr[$i]['ClassLevelID'].'\')"><img id="SiftImg_'.$FormArr[$i]['ClassLevelID'].'" src="'.$image_path.'/'.$LAYOUT_SKIN.'/ereportcard/icon_sift.gif" alt="'.$eReportCard['ClickToSift'].' '.$FormArr[$i]['LevelName'].'" title="'.$eReportCard['ClickToSift'].' '.$FormArr[$i]['LevelName'].'" width="12" height="12" border="0" align="absmiddle"></a></div>';
				$fx .= '<input type="hidden" name="siftFlag_'.$FormArr[$i]['ClassLevelID'].'" id="siftFlag_'.$FormArr[$i]['ClassLevelID'].'" value="0"/>';
				//$fx .= "test"; 
			$fx .= '</th>';
		}
		
		// Subjects
		$SubjectArray = $lreportcard->GET_ALL_SUBJECTS(1);
		$rx = "";
		$count = 0;
		if(!empty($SubjectArray))
		{
			// [2015-1111-1214-16164] Get SubjectID and Subject Code Mapping
			if($eRCTemplateSetting['Settings']['DisplaySubjectCode']){
				$CODEID_Mapping = $lreportcard->GET_SUBJECTS_CODEID_MAP();
			}
			foreach($SubjectArray as $SubjectID => $Data)
			{
				if(is_array($Data))
				{
					foreach($Data as $CmpSubjectID => $SubjectName)
					{
						$css = ($CmpSubjectID > 0) ? 'tablelist_sub_subject' : 'tablelist_subject';
						$SubjectID = ($CmpSubjectID==0) ? $SubjectID : $CmpSubjectID;
						// [2015-1111-1214-16164] Display Subject Code - $eRCTemplateSetting['Settings']['DisplaySubjectCode'] = true & Main Subject
						$SubjectCode = ($eRCTemplateSetting['Settings']['DisplaySubjectCode'] && $CmpSubjectID==0)? "<br/>(".$CODEID_Mapping[$SubjectID].")" : "";
						
						$rx .= '<tr id="tr_subject_'.$SubjectID.'" style="">'."\n";
						$rx .= '<td width="30%" class="'.$css.'">'.(($CmpSubjectID > 0) ? '<img src="'.$image_path.'/'.$LAYOUT_SKIN.'/10x10.gif" width="20" height="10">' : '').$SubjectName.$SubjectCode.'</td>'."\n";
						
						for($i=0 ; $i<count($FormArr) ; $i++){
							$id = $SubjectID.'_'.$FormArr[$i]['ClassLevelID'];
							$is_check = $CheckSubjectArr[$FormArr[$i]['ClassLevelID']][$SubjectID];
							$row_css = ($count % 2 == 0) ? 'retablerow1' : 'retablerow2';							
							$row_css = ($is_check) ? 'retablechose' : $row_css;
							$rx .= '<td align="center" class="'.$row_css.'" width="'.floor(70/count($FormArr)).'%">'.(($is_check) ? '<img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_tick_white.gif" width="20" height="20">' : '&nbsp;');
							$rx .= '<input type="hidden" id="check_'.$id.'" name="check_'.$id.'" value="'.$is_check.'"/></td>'."\n";
						}
						
						$rx .= "</tr>"."\n";
						$flag = 1;
						$count++;
					}
				}
			}
		}
		else
		{
			$rx .= "<tr><td colspan=\"".(count($FormArr)+1)."\" align=\"center\" class=\"tabletext\">".$i_no_record_exists_msg."</td></tr>";
		}
		
        ############################################################################################################
        
        # use for scrollable table only
      //  $header_onload_js .= "makeScrollableTable('subject_form_table',true,'320'); ";
        
		# tag information
		$link0 = 1;
		$link1 = 0;
		$link2 = 0;
		$link3 = 0;
		$TAGS_OBJ[] = array($eReportCard['SubjectSettings'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/settings/subjects_and_form/subject_settings.php", $link0);
		$TAGS_OBJ[] = array($eReportCard['FormSettings'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/settings/subjects_and_form/form_settings.php", $link1);
		if($eRCTemplateSetting['Settings']['FormSubjectTypeSettings']){
		    $TAGS_OBJ[] = array($eReportCard['SubjectType']['Type'],$PATH_WRT_ROOT . "home/eAdmin/StudentMgmt/eReportCard/settings/subjects_and_form/credit_settings.php",$link2);
		}
        if($eRCTemplateSetting['Settings']['GradeDescriptor']){
            $TAGS_OBJ[] = array($eReportCard['GradeSettings'], $PATH_WRT_ROOT . "home/eAdmin/StudentMgmt/eReportCard/settings/subjects_and_form/grade_settings.php", $link3);
        }
		
		//echo "<div id='top'></div>";
		$linterface->LAYOUT_START();
?>

<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>/templates/webtoolkit.scrollabletable.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>/templates/webtoolkit.jscrollable.js"></script>

<script language="javascript">
<?php
  $subStr = "var subjectArr = new Array(";
  if(count($SubjectArray) > 0){
    $i = 0;
	foreach($SubjectArray as $SubjectID => $Data){
		if(is_array($Data)){
			foreach($Data as $CmpSubjectID => $SubjectName){
				$SubjectID = ($CmpSubjectID==0) ? $SubjectID : $CmpSubjectID;
				if($i > 0)
					$subStr .= ", ";
				$subStr .= $SubjectID;
				$i++;
			}
		}
	}
  }
  $subStr .= ");";
  echo $subStr."\n";

  $formStr = "var formArr = new Array(";
  if(count($FormArr) > 0){
	  for($i=0 ; $i<count($FormArr) ; $i++){
		  if($i > 0)
		      $formStr .= ", ";
		  $formStr .= $FormArr[$i]['ClassLevelID'];
	  }
  }
  $formStr .= ");";
  echo $formStr."\n";
?>

function jCHECK_SUBJECT_CHECKED(f_id, msg){
	var num = subjectArr.length;
	var flag = 0;
	
	for(var i=0 ; i<num ; i++){
		var index = subjectArr[i] + "_" + f_id;
		if(document.getElementById("check_" + index).value == 1){
			flag = 1;
			continue;
		}
	}
	
	if(flag == 1){
		return true;
	} else {
		alert(msg);
		return false;
	}
}

function jSIFT_FORM(f_id){
	var s_num = subjectArr.length;
	var f_num = formArr.length;
	var is_sifted = document.getElementById("siftFlag_" + f_id);
	var msg = "No subject is selected in this Form";
	
	if(!jCHECK_SUBJECT_CHECKED(f_id, msg))
		return ;
	
	for(var i=0 ; i<f_num ; i++)
		document.getElementById("td_sort_" + formArr[i]).className = "tabletop tabletopnolink";
	
	for(var i=0 ; i<s_num ; i++){
		var index =  subjectArr[i] + "_" + f_id;
		var a = document.getElementById("tr_subject_" + subjectArr[i]);
		
		checkObj = document.getElementById("check_" + index);
		
		if(is_sifted.value == 0 && checkObj.value == 0){
			displayTable("tr_subject_" + subjectArr[i], "none");
		} else {
			try {
				displayTable("tr_subject_" + subjectArr[i], "table-row");
			} catch (e){
				displayTable("tr_subject_" + subjectArr[i], "");
			}
		}
	}
	
	var SiftImgAlt = document.getElementById("SiftImg_"+f_id).alt;
	var SiftImgTitle = document.getElementById("SiftImg_"+f_id).title;
	
	if(is_sifted.value == 0){
		is_sifted.value = 1;
		document.getElementById("td_sort_" + f_id).className = "retabletop_sort tabletopnolink";
		document.getElementById("SiftImg_"+f_id).alt = SiftImgAlt.replace("<?=$eReportCard['ClickToSift']?>", "<?=$eReportCard['ClickToUnsift']?>");
		document.getElementById("SiftImg_"+f_id).title = SiftImgAlt.replace("<?=$eReportCard['ClickToSift']?>", "<?=$eReportCard['ClickToUnsift']?>");
	}
	else {
		is_sifted.value = 0;
		document.getElementById("SiftImg_"+f_id).alt = SiftImgAlt.replace("<?=$eReportCard['ClickToUnsift']?>", "<?=$eReportCard['ClickToSift']?>");
		document.getElementById("SiftImg_"+f_id).title = SiftImgAlt.replace("<?=$eReportCard['ClickToUnsift']?>", "<?=$eReportCard['ClickToSift']?>");
	}
}

jQuery(document).ready(function() {
	jQuery('#subject_form_table').Scrollable(500);
});
</script>

<br />
<form name="form1" method="POST" action="<?=$UpdatePage;?>">
<table width="98%" border="0" cellpadding="4" cellspacing="0">
  <tr>
    <td align="right" colspan="<?=$colspan?>"><?= $linterface->GET_SYS_MSG($Result); ?></td>
  </tr>
  <tr> 
	<td>
	  <table width="100%" border="0" cellspacing="0" cellpadding="2">
	    <tr>
          <td>
            <table id="subject_form_table" width="98%" border="0" cellspacing="0" cellpadding="4">	  	  
		      <thead>
		        <tr>
		    	  <th width="30%" align="left" style='background-color:#fff'>
				    <a href="subject_settings_edit.php" class="contenttool"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_edit_b.gif" width="20" height="20" border="0" align="absmiddle" alt="<?=$button_edit?>"> <?=$button_edit?></a>
				  </th>
				  <?=$fx?>
	            </tr>
	          </thead>
	          <tbody>
	            <?=$rx?>
			  </tbody>
			  <tfoot>
			    <tr><td colspan="<?=count($FormArr)+1?>"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif"></td></tr>
			  </tfoot>
			</table>
		  </td>
		</tr>
	  </table>
	</td>
  </tr>
</table>
<br/>
<input type="hidden" name="flag" value="1">

</form>

<?

        $linterface->LAYOUT_STOP();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>