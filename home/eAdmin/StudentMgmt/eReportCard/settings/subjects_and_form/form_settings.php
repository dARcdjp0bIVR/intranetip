<?php
/***********************************************
 * 		Modification log
 * 	20151218 Bill:	[2015-1111-1214-16164]
 * 		1. Display Subject Code when $sys_custom['Subject']['DisplaySubjectCode'] = true
 * 	20091221 Marcus:
 * 		1.	cater more than 1 distinction
 * **********************************************/

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
//include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

if ($plugin['ReportCard2008'])
{
	/* Temp Library*/
	include_once($PATH_WRT_ROOT."includes/libreportcard2008j.php");
	$lreportcard = new libreportcard2008j();
	if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
	{
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
	}
	//$lreportcard = new libreportcard();
	
	
	
	$CurrentPage = "Settings_SubjectsAndForms";
	$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
    if ($lreportcard->hasAccessRight())
    {
        $linterface = new interface_html();
      
		############################################################################################################
		
		# Get class level of school
		$FormArr = $lreportcard->GET_ALL_FORMS();
		
		$LevelNameArr = array();
		$LevelIDArr = array();
		if (sizeof($FormArr) > 0) {
			for($i=0; $i<sizeof($FormArr); $i++) {
				$LevelIDArr[] = $FormArr[$i]["ClassLevelID"];
				$LevelNameArr[] = $FormArr[$i]["LevelName"];
			}
		}
		$ClassLevelSelect = getSelectByValueDiffName($LevelIDArr, $LevelNameArr, 'name="ClassLevelID" id="ClassLevelID" onchange="document.form1.submit();"', $ClassLevelID, 0, 1);
		
		// Subjects
		$SubjectArray = $lreportcard->GET_ALL_SUBJECTS(0);
		
		# Initialization
		$FormName = '';
		$targetForm = '';
		$prevForm = '';
		$nextForm = '';
		if(isset($ClassLevelID) && $ClassLevelID != ""){
			$targetForm = $ClassLevelID;
			if(count($FormArr) > 0){
				for($i=0 ;$i<count($FormArr) ; $i++){
					if($targetForm == $FormArr[$i]['ClassLevelID']){
						if($i != 0)
							$prevForm = $FormArr[$i-1]['ClassLevelID'];
						else 
							$prevForm = $FormArr[count($FormArr)-1]['ClassLevelID'];
							
						if($i != count($FormArr)-1)
							$nextForm = $FormArr[$i+1]['ClassLevelID'];
						else 
							$nextForm = $FormArr[0]['ClassLevelID'];
							
						$FormName = $FormArr[$i]['LevelName'];
					}
				}
			}			
		} else {
			if(count($FormArr) > 0){
				$targetForm = $FormArr[0]['ClassLevelID'];
				$prevForm = $FormArr[(count($FormArr)-1)]['ClassLevelID'];
				$nextForm = $FormArr[1]['ClassLevelID'];
				$FormName = $FormArr[0]['LevelName'];
			}
		}
		
		# Get Report Selection 20100924
		$ReportSelection = $lreportcard->Get_Report_Selection($targetForm, $ReportID, "ReportID", $ParOnchange='document.form1.submit();',0,0,0,'',1);
		
		// Get whether each subject is checked for target ClassLevel(Form) or not
		$CheckSubjectArr = $lreportcard->CHECK_FROM_SUBJECT_GRADING($targetForm);
		
		
		/*
		* Necessary Data
		* - SubjectName, SubjectID, CmpSubjectID, Scheme, Subject Scale (Grade, Score)
		* - SchemeTitle, Description, SchemeType, FullMark, Pass, Fail, Weighting
		* - coloring by basic setting & css style sheet
		*/
		
		$SubjectDataArr = array();
		$count = 0;
		$rx = '';
		
		$SubjectDataArr = $lreportcard->GET_FROM_SUBJECT_GRADING_SCHEME($targetForm, 0, $ReportID);
		
		
		// Get the last sub-subject of each subjects if any & use for css 
		$LastCmpSubject = array();
		if(!empty($SubjectDataArr)){
			foreach($SubjectDataArr as $SubjectID => $Data){
				if($Data['is_cmpSubject'] == 1)
					$LastCmpSubject[$Data['parentSubjectID']] = $SubjectID;
			}
		}
		
		// Main
		if(!empty($SubjectDataArr)){
			// [2015-1111-1214-16164] Get SubjectID and Subject Code Mapping
			if($eRCTemplateSetting['Settings']['DisplaySubjectCode']){
				$CODEID_Mapping = $lreportcard->GET_SUBJECTS_CODEID_MAP();
			}
			foreach($SubjectDataArr as $SubjectID => $Data){
				$is_CmpSubject = ($Data['is_cmpSubject']==0) ? 0 : 1;
				// [2015-1111-1214-16164] Display Subject Code - $eRCTemplateSetting['Settings']['DisplaySubjectCode'] = true & Main Subject
				$SubjectCode = ($eRCTemplateSetting['Settings']['DisplaySubjectCode'] && !$is_CmpSubject)? "<br/>(".$CODEID_Mapping[$SubjectID].")" : "";
						
//				$row_css = ($count % 2 == 0 || $is_CmpSubject) ? 'retablerow1' : 'retablerow2';
				$head_1_css = ((count($LastCmpSubject) > 0 && $LastCmpSubject[$Data['parentSubjectID']] == $SubjectID) || $Data['is_cmpSubject']==0) ? 'tablelist_subject' : 'tablelist_subject_head';
				$head_2_css = ($Data['is_cmpSubject']==0) ? 'tablelist_subject' : 'tablelist_sub_subject';
//				$row_css .= ($Data['is_cmpSubject']==0) ? '' : ' retablerow_subsubject_line';
				$row_css = $is_CmpSubject?'retablerow1':'retablerow2';
				$row_css .= ' retablerow_subsubject_line';
				
				$SchemeData = $lreportcard->GET_GRADING_SCHEME_MAIN_INFO($Data['schemeID']);
				
				$SchemeMainArr = $SchemeData;
			
				// for Main Subject
				$rx .= '<tr>';
				$rx .= '<td width="15" align="left" class="'.$head_1_css.'">&nbsp;</td>';
		        $rx .= '<td width="25%" class="'.$head_2_css.'">'.$Data['subjectName'].$SubjectCode.'</td>';
		        
		        if(count($SchemeMainArr) == 0 || $SchemeMainArr['SchemeType'] == "H"){
			        $DistinctMarkData = array();
				    $PassMarkData = array();
				    $FailMarkData = array();
				    $full_mark = '-';
				    $full_grade = '-';
				    $distinct_mark = '-';
				    $distinct_grade = '-';
				    $pass_mark = '-';
					$pass_grade = '-';
				    $fail_mark = '-';
				    $fail_grade = '-';
				    
				    $TopPercentage = $SchemeMainArr['TopPercentage'];
				    $DistinctMarkData = $lreportcard->GET_GRADING_SCHEME_RANGE_INFO($SchemeMainArr['SchemeID'], 'D');
				    $PassMarkData = $lreportcard->GET_GRADING_SCHEME_RANGE_INFO($SchemeMainArr['SchemeID'], 'P');
				    $FailMarkData = $lreportcard->GET_GRADING_SCHEME_RANGE_INFO($SchemeMainArr['SchemeID'], 'F');
				    
			    	// Distinction
			    	$isCompletedPassSetting = $lreportcard->IS_GRADING_SCHEME_RANGE_INFO_VALID($SchemeMainArr['SchemeID'], "D", $Data['scaleDisplay']);
				    if(count($DistinctMarkData) > 0 && $isCompletedPassSetting != -1){
					    if($isCompletedPassSetting){
						    if($TopPercentage == 0){	// Mark Range 
							    if($DistinctMarkData[0]['LowerLimit'] != "" && $SchemeMainArr['FullMark'] != ""){
								    $distinct_mark =  $lreportcard->ReturnTextwithStyle($DistinctMarkData[sizeof($DistinctMarkData)-1]['LowerLimit'], 'HighLight', 'Distinction');
								    $distinct_mark .= ' -';
								    $distinct_mark .= $lreportcard->ReturnTextwithStyle($SchemeMainArr['FullMark'], 'HighLight', 'Distinction');
							    }
						    } else {					// Percentage Range
						    	if($DistinctMarkData != ""){
						    		$DistinctPercentage=0;
						    		for($k=0 ; $k<count($DistinctMarkData) ; $k++){
							    		if($DistinctMarkData[$k]['UpperLimit'] != ""){
									    	$DistinctPercentage += $DistinctMarkData[$k]['UpperLimit'];
								    	}
							    	}
						    		
								    $distinct_mark =  $lreportcard->ReturnTextwithStyle("Top ".$DistinctPercentage."%", 'HighLight', 'Distinction');
							    }
						    }
						    
						    if($DistinctMarkData[0]['Grade'] != ""){
							    $distinct_grade = $lreportcard->ReturnTextwithStyle($DistinctMarkData[0]['Grade'], 'HighLight', 'Distinction');
						    }
					    }
					    else if(!$isCompletedPassSetting){
				    		$distinct_mark = '<span class="tabletext status_alert">'.$eReportCard['SettingsNotCompleted'].'</span>';
				    		$distinct_grade = '<span class="tabletext status_alert">'.$eReportCard['SettingsNotCompleted'].'</span>';
					    }
				    }
				    
				    // Pass
				    $isCompletedPassSetting = $lreportcard->IS_GRADING_SCHEME_RANGE_INFO_VALID($SchemeMainArr['SchemeID'], "P", $Data['scaleDisplay']);
				    if(count($PassMarkData) > 0 && $isCompletedPassSetting != -1){
					    if($isCompletedPassSetting){
						    if($TopPercentage == 0){	// Mark Range 
						    	$pass_mark = $lreportcard->ReturnTextwithStyle($PassMarkData[count($PassMarkData)-1]['LowerLimit'], 'HighLight', 'Pass');
						    	$pass_mark .= ' -';
						    	if(count($DistinctMarkData) > 0 && $DistinctMarkData[sizeof($DistinctMarkData)-1]['LowerLimit'] != "")
						    		$pass_mark .= $lreportcard->ReturnTextwithStyle(($DistinctMarkData[sizeof($DistinctMarkData)-1]['LowerLimit']-1), 'HighLight', 'Pass');
						    	else if($SchemeMainArr['FullMark'] != "") 
						    		$pass_mark .= $lreportcard->ReturnTextwithStyle($SchemeMainArr['FullMark'], 'HighLight', 'Pass');
						    	else 
						    		$pass_mark = '<span class="tabletext status_alert">'.$eReportCard['SettingsNotCompleted'].'</span>';
						    } else {					// Percentage Range
						    	$PassPercentage = '';
						    	for($k=0 ; $k<count($PassMarkData) ; $k++){
						    		if($PassMarkData[$k]['UpperLimit'] != ""){
								    	$PassPercentage += $PassMarkData[$k]['UpperLimit'];
							    	}
						    	}
						    	$pass_mark = $lreportcard->ReturnTextwithStyle("Next ".$PassPercentage."%", 'HighLight', 'Pass');
						    }
					    		
					    	$pass_grade = '';
					    	for($k=0 ; $k<count($PassMarkData) ; $k++){
						    	if($PassMarkData[$k]['Grade'] != ""){
						    		$pass_grade .= ($k != 0) ? ', ' : '';
						    		$pass_grade .= $lreportcard->ReturnTextwithStyle($PassMarkData[$k]['Grade'], 'HighLight', 'Pass');
					    		}
				    		}
			    		}
			    		else if(!$isCompletedPassSetting){
				    		$pass_mark = '<span class="tabletext status_alert">'.$eReportCard['SettingsNotCompleted'].'</span>';
				    		$pass_grade = '<span class="tabletext status_alert">'.$eReportCard['SettingsNotCompleted'].'</span>';
			    		}
					}
					
				    // Fail
				    $isCompletedFailSetting = $lreportcard->IS_GRADING_SCHEME_RANGE_INFO_VALID($SchemeMainArr['SchemeID'], "F", $Data['scaleDisplay']);
				    if(count($FailMarkData) > 0 && $isCompletedFailSetting != -1){
					    if($isCompletedFailSetting){
						    if($TopPercentage == 0){	// Mark Range 
					    		$fail_mark = $lreportcard->ReturnTextwithStyle($FailMarkData[count($FailMarkData)-1]['LowerLimit'], 'HighLight', 'Fail');
					    		$fail_mark .= ' - ';
					    		
					    		if($SchemeMainArr['PassMark'] != "")
					    		{
						    		$fail_upper_limit = $SchemeMainArr['PassMark'] - 1;
						    		$fail_mark .= $lreportcard->ReturnTextwithStyle($fail_upper_limit, 'HighLight', 'Fail');
					    		}
					    		else 
					    			$fail_mark = '<span class="tabletext status_alert">'.$eReportCard['SettingsNotCompleted'].'</span>';
					    	} else {					// Percentage Range
					    		$FailPercentage = '';
						    	for($k=0 ; $k<count($FailMarkData) ; $k++){
						    		if($FailMarkData[$k]['UpperLimit'] != ""){
								    	$FailPercentage += $FailMarkData[$k]['UpperLimit'];
							    	}
						    	}
						    	$fail_mark = $lreportcard->ReturnTextwithStyle("Last ".$FailPercentage."%", 'HighLight', 'Fail');
						    }
			    		
				    		$fail_grade = '';
					    	for($k=0 ; $k<count($FailMarkData) ; $k++){
						    	if($FailMarkData[$k]['Grade'] != ""){
						    		$fail_grade .= ($k != 0) ? ', ' : '';
						    		$fail_grade .= $lreportcard->ReturnTextwithStyle($FailMarkData[$k]['Grade'], 'HighLight', 'Fail');
					    		}
				    		}
			    		}
			    		else if(!$isCompletedFailSetting){
				    		$fail_mark = '<span class="tabletext status_alert">'.$eReportCard['SettingsNotCompleted'].'</span>';
				    		$fail_grade = '<span class="tabletext status_alert">'.$eReportCard['SettingsNotCompleted'].'</span>';
			    		}
			    	}
			    	
			        $rx .= '<td align="center" class="'.$row_css.'"><span class="tabletext">'.(($Data['scaleDisplay'] == "G") ? $distinct_grade : $distinct_mark).'</span></td>';
			        $rx .= '<td align="center" class="'.$row_css.'"><span class="tabletext">'.(($Data['scaleDisplay'] == "G") ? $pass_grade : $pass_mark).'</span></td>';
			        $rx .= '<td align="center" class="'.$row_css.'"><span class="tabletext">'.(($Data['scaleDisplay'] == "G") ? $fail_grade : $fail_mark).'</span></td>';
		        }
		        else if($SchemeMainArr['SchemeType'] == "PF"){
			        $rx .= '<td align="center" class="'.$row_css.'"><span class="tabletext">-</span></td>';
			        $rx .= '<td align="center" class="'.$row_css.'"><span class="tabletext">';
			        $rx .= ($SchemeMainArr['Pass'] != "") ? $lreportcard->ReturnTextwithStyle($SchemeMainArr['Pass'], 'HighLight', 'Pass') : '<span class="tabletext status_alert">'.$eReportCard['SettingsNotCompleted'].'</span>';
			        $rx .= '</span></td>';
			        $rx .= '<td align="center" class="'.$row_css.'"><span class="tabletext">';
			        $rx .= ($SchemeMainArr['Fail'] != "") ? $lreportcard->ReturnTextwithStyle($SchemeMainArr['Fail'], 'HighLight', 'Fail') : '<span class="tabletext status_alert">'.$eReportCard['SettingsNotCompleted'].'</span>';
			        $rx .= '</span></td>';
		        }
		        $rx .= '</tr>';
		        
		        $flag = 1;
				$count++;
			}
			
			########################### Grand Mark Info ###########################
			$GrandSchemeIndexArr = $lreportcard->Get_Grand_Mark_Grading_Scheme_Index_Arr($ClassLevelID);
			$GrandSchemeInfoArr = $lreportcard->Get_Grand_Mark_Grading_Scheme($ClassLevelID,$ReportID);
			
			foreach ($GrandSchemeIndexArr as $GrandMarkIndex => $GrandMarkType)
			{
				$GrandMarkSchemeData = $GrandSchemeInfoArr[$GrandMarkIndex];
				
//				$row_css = ($count % 2 == 0) ? 'retablerow1' : 'retablerow2';
				$row_css = 'retablerow2 retablerow_subsubject_line';
				$head_1_css = 'tablelist_subject';
				$head_2_css = 'tablelist_subject';
				$row_css .= '';
				
				$SchemeData = $lreportcard->GET_GRADING_SCHEME_MAIN_INFO($GrandMarkSchemeData['SchemeID']);
				$SchemeMainArr = $SchemeData;
				
				// for Main Subject
				$rx .= '<tr>';
				$rx .= '<td width="15" align="left" class="'.$head_1_css.'">&nbsp;</td>';
		        $rx .= '<td width="25%" class="'.$head_2_css.'"><b>'.$eReportCard['Template'][$GrandMarkType.'En'].'<br />'.$eReportCard['Template'][$GrandMarkType.'Ch'].'</b></td>';
		        
		        if(count($SchemeMainArr) == 0 || $SchemeMainArr['SchemeType'] == "H"){
			        $DistinctMarkData = array();
				    $PassMarkData = array();
				    $FailMarkData = array();
				    $full_mark = '-';
				    $full_grade = '-';
				    $distinct_mark = '-';
				    $distinct_grade = '-';
				    $pass_mark = '-';
					$pass_grade = '-';
				    $fail_mark = '-';
				    $fail_grade = '-';
				    
				    $TopPercentage = $SchemeMainArr['TopPercentage'];
				    $DistinctMarkData = $lreportcard->GET_GRADING_SCHEME_RANGE_INFO($SchemeMainArr['SchemeID'], 'D');
				    $PassMarkData = $lreportcard->GET_GRADING_SCHEME_RANGE_INFO($SchemeMainArr['SchemeID'], 'P');
				    $FailMarkData = $lreportcard->GET_GRADING_SCHEME_RANGE_INFO($SchemeMainArr['SchemeID'], 'F');
				    
			    	// Distinction
			    	$isCompletedPassSetting = $lreportcard->IS_GRADING_SCHEME_RANGE_INFO_VALID($SchemeMainArr['SchemeID'], "D", $GrandMarkSchemeData['ScaleDisplay']);
				    if(count($DistinctMarkData) > 0 && $isCompletedPassSetting != -1){
					    if($isCompletedPassSetting){
						    if($TopPercentage == 0){	// Mark Range 
							    if($DistinctMarkData[0]['LowerLimit'] != "" && $SchemeMainArr['FullMark'] != ""){
							    	# 2020-01-10 (Philips) [2020-0109-0946-26164] - Fix Range Display
							    	$distinct_mark =  $lreportcard->ReturnTextwithStyle($DistinctMarkData[sizeof($DistinctMarkData)-1]['LowerLimit'], 'HighLight', 'Distinction');
								    $distinct_mark .= ' -';
								    $distinct_mark .= $lreportcard->ReturnTextwithStyle($SchemeMainArr['FullMark'], 'HighLight', 'Distinction');
							    }
						    } else {					// Percentage Range
						    	if($DistinctMarkData[0]['UpperLimit'] != ""){
						    		# 2020-01-10 (Philips) [2020-0109-0946-26164] - Fix Range Display
						    		$distinct_mark =  $lreportcard->ReturnTextwithStyle("Top ".$DistinctMarkData[sizeof($DistinctMarkData)-1]['UpperLimit']."%", 'HighLight', 'Distinction');
							    }
						    }
						    
						    if($DistinctMarkData[0]['Grade'] != ""){
							    $distinct_grade = $lreportcard->ReturnTextwithStyle($DistinctMarkData[0]['Grade'], 'HighLight', 'Distinction');
						    }
					    }
					    else if(!$isCompletedPassSetting){
				    		$distinct_mark = '<span class="tabletext status_alert">'.$eReportCard['SettingsNotCompleted'].'</span>';
				    		$distinct_grade = '<span class="tabletext status_alert">'.$eReportCard['SettingsNotCompleted'].'</span>';
					    }
				    }
				    
				    // Pass
				    $isCompletedPassSetting = $lreportcard->IS_GRADING_SCHEME_RANGE_INFO_VALID($SchemeMainArr['SchemeID'], "P", $GrandMarkSchemeData['ScaleDisplay']);
				    if(count($PassMarkData) > 0 && $isCompletedPassSetting != -1){
					    if($isCompletedPassSetting){
					    	if($TopPercentage == 0){	// Mark Range
					    		# 2020-01-10 (Philips) [2020-0109-0946-26164] - Fix Range Display
						    	$pass_mark = $lreportcard->ReturnTextwithStyle($PassMarkData[count($PassMarkData)-1]['LowerLimit'], 'HighLight', 'Pass');
						    	$pass_mark .= ' -';
						    	if(count($DistinctMarkData) > 0 && $DistinctMarkData[sizeof($DistinctMarkData)-1]['LowerLimit'] != "")
						    		# 2020-01-10 (Philips) [2020-0109-0946-26164] - Fix Range Display
						    		$pass_mark .= $lreportcard->ReturnTextwithStyle(($DistinctMarkData[sizeof($DistinctMarkData)-1]['LowerLimit']-1), 'HighLight', 'Pass');
						    	else if($SchemeMainArr['FullMark'] != "") 
						    		$pass_mark .= $lreportcard->ReturnTextwithStyle($SchemeMainArr['FullMark'], 'HighLight', 'Pass');
						    	else 
						    		$pass_mark = '<span class="tabletext status_alert">'.$eReportCard['SettingsNotCompleted'].'</span>';
						    } else {					// Percentage Range
						    	$PassPercentage = '';
						    	for($k=0 ; $k<count($PassMarkData) ; $k++){
						    		if($PassMarkData[$k]['UpperLimit'] != ""){
								    	$PassPercentage += $PassMarkData[$k]['UpperLimit'];
							    	}
						    	}
						    	$pass_mark = $lreportcard->ReturnTextwithStyle("Next ".$PassPercentage."%", 'HighLight', 'Pass');
						    }
					    		
					    	$pass_grade = '';
					    	for($k=0 ; $k<count($PassMarkData) ; $k++){
						    	if($PassMarkData[$k]['Grade'] != ""){
						    		$pass_grade .= ($k != 0) ? ', ' : '';
						    		$pass_grade .= $lreportcard->ReturnTextwithStyle($PassMarkData[$k]['Grade'], 'HighLight', 'Pass');
					    		}
				    		}
			    		}
			    		else if(!$isCompletedPassSetting){
				    		$pass_mark = '<span class="tabletext status_alert">'.$eReportCard['SettingsNotCompleted'].'</span>';
				    		$pass_grade = '<span class="tabletext status_alert">'.$eReportCard['SettingsNotCompleted'].'</span>';
			    		}
					}
					
				    // Fail
				    $isCompletedFailSetting = $lreportcard->IS_GRADING_SCHEME_RANGE_INFO_VALID($SchemeMainArr['SchemeID'], "F", $GrandMarkSchemeData['ScaleDisplay']);
				    if(count($FailMarkData) > 0 && $isCompletedFailSetting != -1){
					    if($isCompletedFailSetting){
						    if($TopPercentage == 0){	// Mark Range 
					    		$fail_mark = $lreportcard->ReturnTextwithStyle($FailMarkData[count($FailMarkData)-1]['LowerLimit'], 'HighLight', 'Fail');
					    		$fail_mark .= ' - ';
					    		if($SchemeMainArr['PassMark'] != "")
						    		$fail_mark .= $lreportcard->ReturnTextwithStyle($SchemeMainArr['PassMark']-1, 'HighLight', 'Fail');
					    		else 
					    			$fail_mark = '<span class="tabletext status_alert">'.$eReportCard['SettingsNotCompleted'].'</span>';
					    	} else {					// Percentage Range
					    		$FailPercentage = '';
						    	for($k=0 ; $k<count($FailMarkData) ; $k++){
						    		if($FailMarkData[$k]['UpperLimit'] != ""){
								    	$FailPercentage += $FailMarkData[$k]['UpperLimit'];
							    	}
						    	}
						    	$fail_mark = $lreportcard->ReturnTextwithStyle("Last ".$FailPercentage."%", 'HighLight', 'Fail');
						    }
			    		
				    		$fail_grade = '';
					    	for($k=0 ; $k<count($FailMarkData) ; $k++){
						    	if($FailMarkData[$k]['Grade'] != ""){
						    		$fail_grade .= ($k != 0) ? ', ' : '';
						    		$fail_grade .= $lreportcard->ReturnTextwithStyle($FailMarkData[$k]['Grade'], 'HighLight', 'Fail');
					    		}
				    		}
			    		}
			    		else if(!$isCompletedFailSetting){
				    		$fail_mark = '<span class="tabletext status_alert">'.$eReportCard['SettingsNotCompleted'].'</span>';
				    		$fail_grade = '<span class="tabletext status_alert">'.$eReportCard['SettingsNotCompleted'].'</span>';
			    		}
			    	}
			    	
			        $rx .= '<td align="center" class="'.$row_css.'"><span class="tabletext">'.(($GrandMarkSchemeData['ScaleDisplay'] == "G") ? $distinct_grade : $distinct_mark).'</span></td>';
			        $rx .= '<td align="center" class="'.$row_css.'"><span class="tabletext">'.(($GrandMarkSchemeData['ScaleDisplay'] == "G") ? $pass_grade : $pass_mark).'</span></td>';
			        $rx .= '<td align="center" class="'.$row_css.'"><span class="tabletext">'.(($GrandMarkSchemeData['ScaleDisplay'] == "G") ? $fail_grade : $fail_mark).'</span></td>';
		        }
		        else if($SchemeMainArr['SchemeType'] == "PF"){
			        $rx .= '<td align="center" class="'.$row_css.'"><span class="tabletext">-</span></td>';
			        $rx .= '<td align="center" class="'.$row_css.'"><span class="tabletext">';
			        $rx .= ($SchemeMainArr['Pass'] != "") ? $lreportcard->ReturnTextwithStyle($SchemeMainArr['Pass'], 'HighLight', 'Pass') : '<span class="tabletext status_alert">'.$eReportCard['SettingsNotCompleted'].'</span>';
			        $rx .= '</span></td>';
			        $rx .= '<td align="center" class="'.$row_css.'"><span class="tabletext">';
			        $rx .= ($SchemeMainArr['Fail'] != "") ? $lreportcard->ReturnTextwithStyle($SchemeMainArr['Fail'], 'HighLight', 'Fail') : '<span class="tabletext status_alert">'.$eReportCard['SettingsNotCompleted'].'</span>';
			        $rx .= '</span></td>';
		        }
		        $rx .= '</tr>';
		        
		        $flag = 1;
				$count++;
			}
			########################### End of GrandAverage Info ###########################
		}
		else {
			$rx .= '<tr><td width="15" align="left" class="tablelist_sub_subject">&nbsp;</td>';
		    $rx .= '<td colspan="4" align="center" width="100%" class="tabletext tablelist_sub_subject" height="50">There is no record at this moment.</td>';
		    $rx .= '</tr>';
		}
		
		/*
		if(!empty($SubjectArray))
		{
			foreach($SubjectArray as $SubjectID => $Data)
			{
				if(is_array($Data))
				{
					$ParentSubID = $SubjectID;
					foreach($Data as $CmpSubjectID => $SubjectName)
					{	
						$is_CmpSubject = ($CmpSubjectID==0) ? 0 : 1;
						$row_css = ($count % 2 == 0 || $is_CmpSubject) ? 'retablerow1' : 'retablerow2';
						$head_1_css = ($CmpSubjectID==0) ? 'tablelist_subject' : 'tablelist_subject_head';
						$head_2_css = ($CmpSubjectID==0) ? 'tablelist_subject' : 'tablelist_sub_subject';
						$row_css .= ($CmpSubjectID==0) ? '' : ' retablerow_subsubject_line';
						$SubjectID = ($CmpSubjectID==0) ? $SubjectID : $CmpSubjectID;
				
						if($CheckSubjectArr[$SubjectID] == 1){
							// for Main Subject
							$rx .= '<tr><td width="15" align="left" class="'.$head_1_css.'">&nbsp;</td>';
					        $rx .= '<td width="25%" class="'.$head_2_css.'">'.$SubjectName.'</td>';
					        $rx .= '<td align="center" class="'.$row_css.'"><span class="style_distinction">-</span></td>';
					        $rx .= '<td align="center" class="'.$row_css.'"><span class="style_pass">-</span></td>';
					        $rx .= '<td align="center" class="'.$row_css.'"><span class="style_fail"><u>&nbsp;</u></span></td>';
					        $rx .= '</tr>';
					        
					        $flag = 1;
							$count++;
			        	}
			        }
		        }
	        }
        }
        */
		############################################################################################################
        
		# tag information
		$link0 = 0;
		$link1 = 1;
		$link2 = 0;
		$link3 = 0;
		$TAGS_OBJ[] = array($eReportCard['SubjectSettings'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/settings/subjects_and_form/subject_settings.php", $link0);
		$TAGS_OBJ[] = array($eReportCard['FormSettings'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/settings/subjects_and_form/form_settings.php", $link1);
		if($eRCTemplateSetting['Settings']['FormSubjectTypeSettings']){
		    $TAGS_OBJ[] = array($eReportCard['SubjectType']['Type'],$PATH_WRT_ROOT . "home/eAdmin/StudentMgmt/eReportCard/settings/subjects_and_form/credit_settings.php",$link2);
		}
        if($eRCTemplateSetting['Settings']['GradeDescriptor']){
            $TAGS_OBJ[] = array($eReportCard['GradeSettings'], $PATH_WRT_ROOT . "home/eAdmin/StudentMgmt/eReportCard/settings/subjects_and_form/grade_settings.php", $link3);
        }
		
		//echo "<div id='top'></div>";
		$linterface->LAYOUT_START();
				
		echo '<link href="'.$PATH_WRT_ROOT.'templates/2007a/css/ereportcard.css" rel="stylesheet" type="text/css" />';
?>

<script language="javascript">

</script>


<br/>
<form name="form1" method="POST" action="<?=$UpdatePage;?>">
<table width="98%" border="0" cellpadding="4" cellspacing="0">
  <tr>
    <td align="right"><?=$linterface->GET_SYS_MSG($Result);?></td>
  </tr>
  <tr> 
    <td align="center">
      <table width="100%" border="0" cellspacing="0" cellpadding="2">
        <tr>
          <td height="320" align="left" valign="top">
		    <table width="100%" border="0" cellspacing="0" cellpadding="4">
              <tr>
                <td width="15" align="left">&nbsp;</td>
                <td width="25%" align="right">
			      <table width="100%" border="0" cellspacing="0" cellpadding="0">
				  	<tr>
                      <td valign="top">
					    <table border="0" cellspacing="0" cellpadding="2">
                          <tr>
                            <td><a href="form_settings_edit.php?ClassLevelID=<?=$targetForm?>&ReportID=<?=$ReportID?>" class="contenttool"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_edit_b.gif" width="20" height="20" border="0" align="absmiddle" alt="<?=$button_edit?>"> <?=$button_edit?></a></td>
                            <td><img src="<?="{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif"></td>
						  </tr>
                        </table>
					  </td>
                      <td align="right">
					  	<table width="17" border="0" cellspacing="0" cellpadding="2">
                          <tr>
                          	<td><div id="btn_prevnext"><a href="<?="form_settings.php?ClassLevelID=$prevForm"?>"><span><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/ecomm/icon_prev.gif" border="0" alt="<?=$eReportCard['PreviousForm']?>"></span></a></div></td>
                          </tr>
                      	</table>
                      </td>
                  	</tr>
              	  </table>
           		</td>
           		<td align="left" valign="top" class="retabletop tabletopnolink">
           		  <table border="0" cellspacing="0" cellpadding="2">
                    <tr>
                      <td colspan="2" class="tabletopnolink"><?=$ClassLevelSelect?><a href="#"></a><?=$ReportSelection?></td>
                    </tr>
                  </table>
                </td>
                <td width="10" align="center">
                  <table width="17" border="0" cellspacing="0" cellpadding="2">
                    <tr>
                      <td><div id="btn_prevnext"><a href="<?="form_settings.php?ClassLevelID=$nextForm"?>"><span><img src="<?="{$image_path}/{$LAYOUT_SKIN}" ?>/ecomm/icon_next.gif" border="0" alt="<?=$eReportCard['NextForm']?>"></span></a></div></td>
                    </tr>
                  </table>
                </td>
           	  </tr>
           	  <tr>
                <td align="left"><img src="<?="{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="13" height="2"></td>
                <td align="right" valign="top"><img src="<?="{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="13" height="2"></td>
                <td align="center" valign="top"><img src="<?="{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="13" height="2"></td>
                <td align="center" valign="top"><img src="<?="{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="13" height="2"></td>
              </tr>
           	</table>
           	<table width="100%" border="0" cellspacing="0" cellpadding="4">
              <tr>
                <td width="15" align="left">&nbsp;</td>
                <td width="25%" align="right" valign="top">&nbsp;</td>
                <td align="center" valign="top" class="tabletop tabletopnolink"><span class="tabletopnolink"><?=$eReportCard['Distinction']?></span></td>
                <td width="25%" align="center" valign="top" class="tabletop tabletopnolink"><span class="tabletopnolink"><?=$eReportCard['Pass']?></span></td>
                <td width="20%" align="center" valign="top" class="tabletop tabletopnolink"><span class="tabletopnolink"><?=$eReportCard['Fail']?></span></td>
              </tr>
              <?=$rx?>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</form>


<?

        $linterface->LAYOUT_STOP();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>