<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
//include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

if ($plugin['ReportCard2008'])
{
	if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
	{
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
		$lreportcard = new libreportcardcustom();
	} else {
		$lreportcard = new libreportcard();
	}
	
	$CurrentPage = "Settings_SubjectsAndForms";
	$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
    if ($lreportcard->hasAccessRight())
    {
        $linterface = new interface_html();
      
		############################################################################################################
		
		# GET DATA
		$frClassLevelID = $frClassLevel;
		$toClassLevelID = $toClassLevel;
		
		# Get Class Level of school
		$FormArr = $lreportcard->GET_ALL_FORMS();
		
		$frClassName = '';
		$toClassName = '';
		if(count($FormArr) > 0){
			for($i=0 ;$i<count($FormArr) ; $i++){
				if($frClassLevelID == $FormArr[$i]['ClassLevelID']){
					$frClassName = $FormArr[$i]['LevelName'];
				}
				if($toClassLevelID == $FormArr[$i]['ClassLevelID']){
					$toClassName = $FormArr[$i]['LevelName'];
				}
			}
		}

		# Get ReportList
		$ReportArr = $lreportcard->Get_Report_List($toClassLevelID);
		$ReportTitleArr = buildMultiKeyAssoc($ReportArr, "ReportID", "ReportTitle",1);
		
		# GET Data of Copy Result
		$copyResult = unserialize(stripslashes($cpResult));
		
		$copyAction = $eReportCard['CopyFrom']." ".$frClassName." ".str_replace(":_:"," ",$ReportTitleArr[$frReportID])." ";
		$copyAction .= $i_To." ".$toClassName." ".str_replace(":_:"," ",$ReportTitleArr[$toReportID]);		
		
		$num = 0;
		$displayResult = '';
		foreach($copyResult as $SubjectReportID => $success){
			
			list($SubjectID, $ReportID) = explode("_",$SubjectReportID);
			
			if($ReportID == 0)
				continue;
			if ($SubjectID == -1)
				$SubjectName = $eReportCard['Template']['GrandAverage'];
			else if ($SubjectID == -2)
				$SubjectName = $eReportCard['Template']['GrandTotal'];
			else if ($SubjectID == -3)
				$SubjectName = $eReportCard['Template']['GPA'];
			else
				$SubjectName = $lreportcard->GET_SUBJECT_NAME($SubjectID);
			
			switch($success){
				case 1:
					$status_msg = $eReportCard['Success'];
					break;
				case 0:
					$status_msg = $eReportCard['GradingSchemeNotSet'];
					break;	
			}
			
//			if($ReportID == 0)
//				$ReportTitle = $eReportCard['AllReportCards'];
//			else
				$ReportTitle = str_replace(":_:","<br>",$ReportTitleArr[$ReportID]);
			
			$displayResult .= "<tr>";
			$displayResult .= "<td class=\"tablerow". ($num % 2 +1) ." tabletext\">".$ReportTitle."</td>";
			$displayResult .= "<td class=\"tablerow". ($num % 2 +1) ." tabletext\">".$SubjectName."</td>";
			$displayResult .= "<td class=\"tablerow". ($num % 2 +1) ." tabletext\">".$status_msg."</td>";
			$displayResult .= "</tr>";
			
			$num++;
		}
		
		############################################################################################################
        
		# tag information
		$link0 = 0;
		$link1 = 1;
		$TAGS_OBJ[] = array($eReportCard['SubjectSettings'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/settings/subjects_and_form/subject_settings.php", $link0);
		$TAGS_OBJ[] = array($eReportCard['FormSettings'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/settings/subjects_and_form/form_settings.php", $link1);
		$PAGE_NAVIGATION[] 	= array($eReportCard['CopyClassLevelSettingsResult'].' ('.$copyAction.')');

		$linterface->LAYOUT_START();
?>

<br/>
<table width="98%" border="0" cellspacing="0" cellpadding="0">
  <tr>
	<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	<td align='right'>&nbsp;</td>
  </tr>
  <tr>
	<td colspan="2">
	  <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
	    <tr>  
	      <td>
        	<br />
			<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
			<tr>
			  <td class="tabletext"><?=$i_ClassLevel.' : '?><strong><?=$toClassName?></strong></td>
			</tr>
            <tr class="tabletop">
            <td><span class="tabletopnolink"><?=$eReportCard['ReportCard']?></span> </td>
              <td><span class="tabletopnolink"><?=$eReportCard['Subject']?></span> </td>
              <td><span class="tabletopnolink"><?=$i_general_status?></span></td>
            </tr>
            <?=$displayResult?>
			</table>
		  </td>
	    </tr>
	  </table>
	</td>
  </tr>
  <tr>
	<td colspan="2">        
	  <table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
	    <tr>
	      <td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
	    </tr>
	    <tr>
		  <td align="center"><?= $linterface->GET_ACTION_BTN($button_continue, "button", "window.location='form_settings.php?ClassLevelID=$toClassLevelID'") ?></td>
		</tr>
	  </table>                                
	</td>
  </tr>
</table>                        
<br />


<?

        $linterface->LAYOUT_STOP();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>