<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
?>



<link href="<?=$PATH_WRT_ROOT?>templates/<?=$LAYOUT_SKIN?>/css/content.css" rel="stylesheet" type="text/css" />
<link href="<?=$PATH_WRT_ROOT?>templates/<?=$LAYOUT_SKIN?>/css/iportfolio/content_css.php" rel="stylesheet" type="text/css" />
<link href="<?=$PATH_WRT_ROOT?>templates/<?=$LAYOUT_SKIN?>/css/discipline.css" rel="stylesheet" type="text/css" />
<style>
	table {
		text-align: left;
		font-size: 12px;
		font-family: verdana;
		background: #c0c0c0;
	}
	
	table thead tr,
	table tfoot tr {
		background: #c0c0c0;
	}
	
	table tbody tr {
		background: #f0f0f0;
	}
	
	td, th {
		border: 1px solid white;
	}
</style>

<script language="JavaScript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery-1.3.2.min.js"></script>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.tablednd_0_5.js"></script>

<script type="text/javascript" src="webtoolkit.scrollabletable.js"></script>
<script type="text/javascript" src="webtoolkit.jscrollable.js"></script>

<script type="text/javascript">
	jQuery(document).ready(function() {
		//jQuery('table:eq(0)').Scrollable(200, 800);
		jQuery('#withFooter').Scrollable(200, 800);
		//jQuery('table:eq(1)').Scrollable(150, 900);
		jQuery('#withoutFooter').Scrollable(150, 900);
	});
</script>


<body>     
	<table id="withFooter" cellspacing="1">
		<thead>
			<tr>
				<th>Name</th>
				<th>Major</th>
				<th>Sex</th>
				<th>English</th>
				<th>Japanese</th>
				<th>Calculus</th>
				<th>Geometry</th>
			</tr>
		</thead>
		<tfoot>
			<tr>
				<th>Name</th>
				<th>Major</th>
				<th>Sex</th>
				<th>English</th>
				<th>Japanese</th>
				<th>Calculus</th>
				<th>Geometry</th>
			</tr>
		</tfoot>
		<tbody>
			<tr>
				<td>Student01</td>
				<td>Languages</td>
				<td>male</td>
				<td>80</td>
				<td>70</td>
				<td>75</td>
				<td>80</td>
			</tr>
			<tr>
				<td>Student02</td>
				<td>Mathematics</td>
				<td>male</td>
				<td>90</td>
				<td>88</td>
				<td>100</td>
				<td>90</td>
			</tr>
			<tr>
				<td>Student03</td>
				<td>Languages</td>
				<td>female</td>
				<td>85</td>
				<td>95</td>
				<td>80</td>
				<td>85</td>
			</tr>
			<tr>
				<td>Student04</td>
				<td>Languages</td>
				<td>male</td>
				<td>60</td>
				<td>55</td>
				<td>100</td>
				<td>100</td>
			</tr>
			<tr>
				<td>Student05</td>
				<td>Languages</td>
				<td>female</td>
				<td>68</td>
				<td>80</td>
				<td>95</td>
				<td>80</td>
			</tr>
			<tr>
				<td>Student06</td>
				<td>Mathematics</td>
				<td>male</td>
				<td>100</td>
				<td>99</td>
				<td>100</td>
				<td>90</td>
			</tr>
			<tr>
				<td>Student07</td>
				<td>Mathematics</td>
				<td>male</td>
				<td>85</td>
				<td>68</td>
				<td>90</td>
				<td>90</td>
			</tr>
			<tr>
				<td>Student08</td>
				<td>Languages</td>
				<td>male</td>
				<td>100</td>
				<td>90</td>
				<td>90</td>
				<td>85</td>
			</tr>
			<tr>
				<td>Student09</td>
				<td>Mathematics</td>
				<td>male</td>
				<td>80</td>
				<td>50</td>
				<td>65</td>
				<td>75</td>
			</tr>
			<tr>
				<td>Student10</td>
				<td>Languages</td>
				<td>male</td>
				<td>85</td>
				<td>100</td>
				<td>100</td>
				<td>90</td>
			</tr>
			<tr>
				<td>Student11</td>
				<td>Languages</td>
				<td>male</td>
				<td>86</td>
				<td>85</td>
				<td>100</td>
				<td>100</td>
			</tr>
			<tr>
				<td>Student12</td>
				<td>Mathematics</td>
				<td>female</td>
				<td>100</td>
				<td>75</td>
				<td>70</td>
				<td>85</td>
			</tr>
		</tbody>
	</table>
	     
	<br />
	<br />   
	
	<table id="withoutFooter" cellspacing="1">
		<thead>
			<tr>
				<th>Name</th>
				<th>Major</th>
				<th>Sex</th>
				<th>English</th>
				<th>Japanese</th>
				<th>Calculus</th>
				<th>Geometry</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>Student01</td>
				<td>Languages</td>
				<td>male</td>
				<td>80</td>
				<td>70</td>
				<td>75</td>
				<td>80</td>
			</tr>
			<tr>
				<td>Student02</td>
				<td>Mathematics</td>
				<td>male</td>
				<td>90</td>
				<td>88</td>
				<td>100</td>
				<td>90</td>
			</tr>
			<tr>
				<td>Student03</td>
				<td>Languages</td>
				<td>female</td>
				<td>85</td>
				<td>95</td>
				<td>80</td>
				<td>85</td>
			</tr>
			<tr>
				<td>Student04</td>
				<td>Languages</td>
				<td>male</td>
				<td>60</td>
				<td>55</td>
				<td>100</td>
				<td>100</td>
			</tr>
			<tr>
				<td>Student05</td>
				<td>Languages</td>
				<td>female</td>
				<td>68</td>
				<td>80</td>
				<td>95</td>
				<td>80</td>
			</tr>
			<tr>
				<td>Student06</td>
				<td>Mathematics</td>
				<td>male</td>
				<td>100</td>
				<td>99</td>
				<td>100</td>
				<td>90</td>
			</tr>
			<tr>
				<td>Student07</td>
				<td>Mathematics</td>
				<td>male</td>
				<td>85</td>
				<td>68</td>
				<td>90</td>
				<td>90</td>
			</tr>
			<tr>
				<td>Student08</td>
				<td>Languages</td>
				<td>male</td>
				<td>100</td>
				<td>90</td>
				<td>90</td>
				<td>85</td>
			</tr>
			<tr>
				<td>Student09</td>
				<td>Mathematics</td>
				<td>male</td>
				<td>80</td>
				<td>50</td>
				<td>65</td>
				<td>75</td>
			</tr>
			<tr>
				<td>Student10</td>
				<td>Languages</td>
				<td>male</td>
				<td>85</td>
				<td>100</td>
				<td>100</td>
				<td>90</td>
			</tr>
			<tr>
				<td>Student11</td>
				<td>Languages</td>
				<td>male</td>
				<td>86</td>
				<td>85</td>
				<td>100</td>
				<td>100</td>
			</tr>
			<tr>
				<td>Student12</td>
				<td>Mathematics</td>
				<td>female</td>
				<td>100</td>
				<td>75</td>
				<td>70</td>
				<td>85</td>
			</tr>
		</tbody>
	</table>
</body>   

