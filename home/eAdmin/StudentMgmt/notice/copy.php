<?php
# USING:

############ Change Log [Start] ############
#
# 	Date:   2020-09-07 (Bill)    [2020-0604-1821-16170]
#			access right checking > replace hasIssueRight by hasSchoolNoticeIssueRight
#
#   Date:   2020-04-16  Bill    [2020-0310-1051-05235]
#           Special notice > access checking for teacher    ($sys_custom['eNotice']['SpecialNoticeForPICAndAdmin'])
#
#	Date:	2015-12-11	Bill
#			- replace session_register() by session_register_intranet() for PHP 5.4
#
#	Date:	2015-10-28	Bill	[2015-0416-1040-06164]
#			support merge notice content
#
############ Change Log [End] ############

	$PATH_WRT_ROOT = "../../../../";
	include_once($PATH_WRT_ROOT."includes/global.php");
	include_once($PATH_WRT_ROOT."includes/libdb.php");
	include_once($PATH_WRT_ROOT."includes/libnotice.php");
	include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
	include_once($PATH_WRT_ROOT."includes/libuser.php");

	// [2020-0604-1821-16170]
	//if (!$plugin['notice'] || $_SESSION["SSV_PRIVILEGE"]["notice"]["disabled"] || !($_SESSION["SSV_PRIVILEGE"]["notice"]["hasIssueRight"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-eNotice"]))
	if (!$plugin['notice'] || $_SESSION["SSV_PRIVILEGE"]["notice"]["disabled"] || !($_SESSION["SSV_PRIVILEGE"]["notice"]["hasSchoolNoticeIssueRight"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-eNotice"]))
	{
		include_once($PATH_WRT_ROOT."includes/libaccessright.php");
		$laccessright = new libaccessright();
		$laccessright->NO_ACCESS_RIGHT_REDIRECT();
		exit;
	}

	intranet_auth();
	intranet_opendb();
	
	$li = new libdb();
	$luser = new libuser($UserID);

	$copy_from = $NoticeIDArr[0];
	$lnotice = new libnotice($copy_from);

	// [2020-0310-1051-05235] Special notice > access checking for teacher
	if($sys_custom['eNotice']['SpecialNoticeForPICAndAdmin'] && $lnotice->SpecialNotice)
	{
        $allowTeacherAccess = true;
        if($luser->isTeacherStaff()) {
            $allowTeacherAccess = $_SESSION["SSV_USER_ACCESS"]["eAdmin-eNotice"] || $lnotice->isNoticePIC($copy_from);
        }

        if (!$allowTeacherAccess)
        {
            include_once($PATH_WRT_ROOT."includes/libaccessright.php");
            $laccessright = new libaccessright();
            $laccessright->NO_ACCESS_RIGHT_REDIRECT();
            exit;
        }
	}

	$lf = new libfilesystem();
	
	# Copy INTRANET_NOTICE
	$sql = "INSERT INTO INTRANET_NOTICE 
				(NoticeNumber, Title, Description, DateStart, DateEnd, Question, RecordType, IsModule, ReplySlipContent, AllFieldsReq, DisplayQuestionNumber, ContentType, MergeFormat, MergeType, MergeFile)
	  		SELECT NoticeNumber, Title, Description, DateStart, DateEnd, Question, RecordType, IsModule, ReplySlipContent, AllFieldsReq, DisplayQuestionNumber, ContentType, MergeFormat, MergeType, MergeFile FROM INTRANET_NOTICE WHERE NoticeID = '$copy_from'";
	$li->db_db_query($sql) or die(mysql_error());
	$NoticeID = $li->db_insert_id();

	## Update description if user upload image with Flash upload (fck) [Start]
	$lnotice->updateNoticeFlashUploadPath($NoticeID,$lnotice->Description,'copy');

	## Update description if user upload image with Flash upload (fck) [End]	
	# update Date Input/Modified, suspended
	//session_register("noticeAttFolder");
	//$noticeAttFolder = session_id().".".time();
	$noticeAttFolder = $lnotice->genAttachmentFolderName();
	// for PHP 5.4, replace session_register()
	session_register_intranet("noticeAttFolder", $noticeAttFolder);
	
	$IssueUserName = $luser->getNameForRecord();
	$sql = "UPDATE INTRANET_NOTICE SET IssueUserID = $UserID, IssueUserName = '$IssueUserName', Attachment = '$noticeAttFolder', RecordStatus = 2, DateInput = now(), DateModified = now() WHERE NoticeID = {$NoticeID}";
	$li->db_db_query($sql);
	
	// [2015-0416-1040-06164] Content Type: Merge Content
	// upload csv file for merge notice
	if($lnotice->ContentType == 2 && $lnotice->MergeFile != "")
	{
		// get copy eNotice CSV path
		$copyfromCSV = "$file_path/file/mergenotice/".ceil($copy_from/10000)."/".$copy_from;
		if (is_dir($copyfromCSV) && !strpos($lnotice->MergeFile,"."==0)) {
			$copyfromCSV .= "/".$lnotice->MergeFile;
			
			$mergepath = "$file_path/file/mergenotice";
			$lf->folder_new($mergepath);
			
			$mergepath .= "/".ceil($NoticeID/10000);
			$lf->folder_new($mergepath);
			
			$mergepath .= "/".$NoticeID;
			$lf->folder_new($mergepath);
			
			// copy file to destination
			$mergedes = "$mergepath/".$lnotice->MergeFile;
           	$lf->lfs_copy($copyfromCSV, $mergedes);
		}
	}
	
	################### Attachment
	$path = "$file_path/file/notice/$noticeAttFolder";
	if (!is_dir($path))
	{
	     $lf->folder_new($path);
	}

	# copy attachment and powerVoice files
	if(!empty($lnotice->Attachment))
	{
		$old_path = "$file_path/file/notice/".$lnotice->Attachment;
		$copy_files = $lf->folder_content_copy($old_path, $path);
	}

	##############################################
	##### check for lasalle problem
	##### Append NoticeID to the attachment folder
	$Attachment2 = $noticeAttFolder . ".".$NoticeID;
	$path2 = "$file_path/file/notice/".$Attachment2;
	rename($path, $path2);

	$sql = "UPDATE INTRANET_NOTICE SET Attachment = '". $Attachment2 ."', OriginalAttachment = 'copy from ". $copy_from ."' WHERE NoticeID = '$NoticeID'";
	$lnotice->db_db_query($sql);
	##############################################
	
	intranet_closedb();
	header("Location: edit.php?NoticeID=$NoticeID&copied=1");
?>
