<?php
/*
 * This file should be run daily for sending the reminder push message to reader when the due date is TODAY
 * Created by Anna
 */

// $WRT_ROOT= "{$_SERVER['DOCUMENT_ROOT']}/";
$PATH_WRT_ROOT ="{$_SERVER['DOCUMENT_ROOT']}/";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
 
$libdb = new libdb();
$luser = new libuser();
$libeClassApp = new libeClassApp();
$lgeneralsettings = new libgeneralsettings();
$lnotice = new libnotice();

intranet_opendb();

echo "<br/>This file should be run daily for sending the reminder push message(s) to reader when the due date of eNotice is TODAY<br/><br/>";
echo "Program started!<br/>";

$SettingAry = $lgeneralsettings->Get_General_Setting($lnotice->ModuleName);
$notifcation_settings = array(
    'push_message_overdue' => $SettingAry['push_message_overdue']
);

$sendPushMessage = false;
if (!$notifcation_settings['push_message_overdue']) {
	echo "The setting is not allow to send overdue push message!<br/>";
}
else {
	$sql = "SELECT id FROM NOTICE_DAILY_SCRIPT WHERE DATE(DateCreated) >= DATE(now()) AND name = 'push_message_overdue' limit 1";
	$result = $lnotice->returnArray($sql);
	if ($result[0]['id']) {
		echo "Overdue push message(s) is/are already sent!<br/>";
	}
	else {
 		$sendPushMessage = true;
	}
}

// $sendPushMessage = true;
if ($sendPushMessage)
{
	$SQL = "SELECT * FROM INTRANET_NOTICE WHERE RecordStatus='1' AND IsModule='0' AND DATE_FORMAT(DateEnd,'%Y-%m-%d') = DATE_FORMAT(NOW(),'%Y-%m-%d') AND DateStart <= NOW() ";
	$NoticeInfoAry = $lnotice->returnArray($SQL);
	$NoticeNum = sizeof($NoticeInfoAry);

	$timeOfPushMessage = $SettingAry['push_message_time'];
	
	$isPublic = "";
	$sendTimeMode = 'scheduled';
	$sendTimeString = date('Y-m-d').' '.$timeOfPushMessage.':00';

	$messageTitle = $Lang['eNotice']['daily']['push_message']['overdue']['title'];

// 	$individualMessageInfoAry = array();
	$sendNum = 0;
	for($i=0; $i<$NoticeNum; $i++)
	{
		$NoticeID = $NoticeInfoAry[$i]['NoticeID'];
		$TargetType = $NoticeInfoAry[$i]['TargetType'];
		$NoticeTitle = $NoticeInfoAry[$i]['Title'];
		$EndDate = $NoticeInfoAry[$i]['DateEnd'];

		$UnsignedInfoAry = $lnotice->getUnsignList($NoticeID);

		if((time() - strtotime($sendTimeString)) > 0)
		{
		    echo "Time set to push message is later than the time right now, Program End.";
		}
		else
        {
		    if($TargetType == 'S')
		    {
		        $individualMessageInfoAry = array();
                $StudentAssoAry = array();  // [2020-0721-1316-45073] clear student list for each notice > fixed accumulated target student list
		      
		        foreach ($UnsignedInfoAry as $key => $studentId) {
		            $_targetStudentId = $libeClassApp->getDemoSiteUserId($studentId);
		            // link the message to be related to oneself
		            $StudentAssoAry[$studentId] = array($_targetStudentId);
		        }
		        $individualMessageInfoAry[0]['relatedUserIdAssoAry'] = $StudentAssoAry;
		        
		        $messageContent = geteNoticePushMessageContent2($NoticeTitle,$EndDate,$TargetType);
		        $sendNum = $sendNum + count($StudentAssoAry);
		        if(!empty($individualMessageInfoAry)){
		           $notifyMessageId = $libeClassApp->sendPushMessage($individualMessageInfoAry, $messageTitle, $messageContent, $isPublic, $recordStatus=1, $TargetType, $sendTimeMode, $sendTimeString, '', '', 'eNotice', $NoticeID);

		           #####UPDATE INPUT DATE
		           $sql = "UPDATE INTRANET_APP_NOTIFY_MESSAGE  Set DateInput  = '$sendTimeString'  Where NotifyMessageID = '$notifyMessageId'";
		           $libdb->db_db_query($sql);
		        }	
		    }
		    
		    if($TargetType == 'P')
		    {
		        $individualMessageInfoAry = array();
		        if (!empty($UnsignedInfoAry)) {
		            $parentStudentAssoAry = BuildMultiKeyAssoc($luser->getParentStudentMappingInfo($UnsignedInfoAry), 'ParentID', $IncludedDBField=array('StudentID'), $SingleValue=1, $BuildNumericArray=1);
		        } else {
		            $parentStudentAssoAry = array();
		        }
	
		        $individualMessageInfoAry[0]['relatedUserIdAssoAry'] = $parentStudentAssoAry;
		        $sendNum = $sendNum+ count($parentStudentAssoAry);
		        $messageContent = geteNoticePushMessageContent2($NoticeTitle,$EndDate,$TargetType);			
		        if(!empty($individualMessageInfoAry)){
		             $notifyMessageId = $libeClassApp->sendPushMessage($individualMessageInfoAry, $messageTitle, $messageContent, $isPublic, $recordStatus=1, $TargetType, $sendTimeMode, $sendTimeString, '', '', 'eNotice', $NoticeID);

		             $sql = "UPDATE INTRANET_APP_NOTIFY_MESSAGE  Set DateInput  = '$sendTimeString'  Where NotifyMessageID = '$notifyMessageId'";
		             $libdb->db_db_query($sql);
		        }
		    }
		}
	}

 	$sql = "INSERT INTO NOTICE_DAILY_SCRIPT (name) Values ('push_message_overdue') ";
	$result = $lnotice->db_db_query($sql);

	echo "The push message is sent to ".$sendNum." reader(s)!<br/>";
	echo "Program ended!<br/>";
}

function geteNoticePushMessageContent2($NoticeTitle,$EndDate,$appType){
	global $Lang;
	if($appType == 'P'){
	    $messageContent = $Lang['eNotice']['daily']['push_message']['overdue']['content'].'
			';	
	}else{
	    $messageContent = $Lang['eNotice']['daily']['push_message']['overdue']['Studentcontent'].'
			';	
	}
	
	$messageContent .= '
				 '.$NoticeTitle.' ('.$EndDate.')';
	return $messageContent;
}
?>