<?php 
# editing by: Roy

/***************************************************************************
*	modification log:
*	
*   2019-06-06 [Bill]
*   - add CSRF token checking
*  
*	2015-07-20 [Ivan] (ip.2.5.6.7.1)
*	- Added sms confirmation page logic
*
*	2013/10/08	Roy
*	- newly added
*
***************************************************************************/

ini_set('display_errors', '1');

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libsmsv2.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

if(!verifyCsrfToken($_POST['csrf_token'], $_POST['csrf_token_key'], $do_not_remove_token_data=true)){
    echo "<font color='red'>".$Lang['AppNotifyMessage']['eNotice']['send_failed']."</font>";
    exit;
}

intranet_auth();
intranet_opendb();

$Message = standardizeFormPostValue($_POST['Message']);

$lnotice = new libnotice($NoticeID);
$libsms = new libsmsv2();

$targetClass = '';
if (isset($_POST['SelectedClass'])) {
	$targetClass = $_POST['SelectedClass'];
}
else {
	$targetClass = $class;
}
//$UnsignedStudents = $lnotice->getUnsignList($NoticeID, $class);
$UnsignedStudents = $lnotice->getUnsignList($NoticeID, $targetClass);

$lu = new libuser($UserID);
$notifyuser = $lu->UserNameLang();

$SchoolID = isset($asl_parent_app['SchoolID'])? $asl_parent_app['SchoolID'] : $_SERVER['SERVER_NAME'];
$MsgNotifyDateTime = date("Y-m-d H:i:s");

if ($Message == '') {
	$NoticeNumber = "".$lnotice->NoticeNumber;
	$NoticeTitle = "".$lnotice->Title;
	$NoticeDeadline = "".$lnotice->DateEnd;
	
	$MessageTitle_sms = str_replace("[NoticeNumber]", $NoticeNumber, $Lang['AppNotifyMessage']['eNotice']['Title']['SMS']);
	$MessageContent_sms = str_replace("[NoticeNumber]", $NoticeNumber, $Lang['AppNotifyMessage']['eNotice']['Alert']['SMS']);
	$MessageContent_sms = str_replace("[NoticeTitle]", $NoticeTitle, $MessageContent_sms);
	$MessageContent_sms = str_replace("[NoticeDeadline]", $NoticeDeadline, $MessageContent_sms);
	
	$SMSContent = $MessageTitle_sms."\n".$MessageContent_sms;
}
else {
	$SMSContent = $Message;
}
$SMSContent = sms_substr($SMSContent);

$ParentFound = 0;

$namefield = getNameFieldByLang();

for($i=0; $i<sizeof($UnsignedStudents); $i++)
{
	$StudentID = $UnsignedStudents[$i];
    
	# Get Parent's Info
	$sql = "SELECT 	iu.UserID, 
					$namefield as UserName, 
					TRIM(iu.MobileTelNo) as Mobile 
			FROM 	INTRANET_PARENTRELATION AS ip,
					INTRANET_USER AS iu 
			WHERE 	ip.StudentID='{$StudentID}' AND 
					ip.ParentID=iu.UserID  AND 
					iu.RecordStatus=1 ";
	$recipient_rows = $lnotice->returnResultSet($sql);
	$recipient_parent_userlogin = array();
    
	for ($ri=0; $ri<sizeof($recipient_rows); $ri++)
	{
		$ParentFound ++;
		$ParentID = $recipient_rows[$ri]["UserID"];
		$mobile = $recipient_rows[$ri]["Mobile"];
		$mobile = $libsms->parsePhoneNumber($mobile);
		$ParentName = $recipient_rows[$ri]["UserName"];
		
		if($libsms->isValidPhoneNumber($mobile)) {
			$recipientData[] = array($mobile,$StudentID,addslashes($ParentName),"");
			$valid_list[] = array($ParentID,$ParentName,$mobile,$StudentID);
		}
		else{
			$reason =$i_SMS_Error_NovalidPhone;
			$error_list[] = array($ParentID,$ParentName,$mobile,$StudentID,$reason);
		}
	}
}

# Send sms
if(sizeof($recipientData) > 0)
{
	$targetType = 3;
	$picType = 2;
	$adminPIC = $PHP_AUTH_USER;
	$userPIC = $UserID;
	$frModule = "";
	$deliveryTime = $time_send;
	$isIndividualMessage = false;
	$sms_message = $SMSContent; #### Use original text - not htmlspecialchars processed
	$returnCode = $libsms->sendSMS($sms_message,$recipientData, $targetType, $picType, $adminPIC, $userPIC,"",$deliveryTime,$isIndividualMessage);
	$isSent = 1;
	$smsTotal = sizeof($recipientData);
}

intranet_closedb();

if($isSent && ($returnCode > 0 && $returnCode==true))
{
	echo "<font color='#DD5555'>".str_replace("[smsTotal]", $smsTotal, $Lang['AppNotifyMessage']['SMS_send_result']). "</font>";
}
else
{
	echo "<font color='red'>".$Lang['AppNotifyMessage']['send_failed']."</font>";
}
?>