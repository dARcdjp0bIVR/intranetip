<?php
$PATH_WRT_ROOT = "../../../../../../";

include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libnotice.php");
include_once ($PATH_WRT_ROOT . "includes/libgeneralsettings.php");
// include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");

intranet_auth();
intranet_opendb();

if (! $plugin['notice'] || ! $_SESSION["SSV_USER_ACCESS"]["eAdmin-eNotice"] || !$plugin['eClassApp']) {
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

$lnotice = new libnotice();
$lgeneralsettings = new libgeneralsettings();
$linterface = new interface_html();

// Top menu highlight setting
$CurrentPageArr['eAdminNotice'] = 1;
$CurrentPage = "PageNoticeSettings_NoticeSettings";

// left menu
$TAGS_OBJ[] = array(
    $Lang['eNotice']['Settings']
);
$MODULE_OBJ = $lnotice->GET_MODULE_OBJ_ARR();
// Start layout
$xmsg = $xmsg == 1 ? $Lang['eNotice']['SettingsUpdateSuccess'] : "";
$linterface->LAYOUT_START($xmsg);

$SettingAry = $lgeneralsettings->Get_General_Setting($lnotice->ModuleName);
// #######################################
// ## Start: construct table content #####
// #######################################
// #####################################
// # Start: PUSH MESSAGE ###############
// #######################################

if ($plugin['eClassApp']) {
    $x .= '<table class="form_table_v30">' . "\r\n";
        $x .= '<tbody>' . "\r\n";
        $x .= $linterface->GET_NAVIGATION2($Lang['General']['PushMessage']);
        
        $key = 'push_message_overdue';
        $content = ($SettingAry[$key] == 1) ? $Lang['eNotice']["settings"]['notification']['yes'] : $Lang['eNotice']["settings"]['notification']['no'];
        $yesIsChecked = '';
        $noIsChecked = '';
        if ($SettingAry[$key]) {
            $yesIsChecked = 'checked';
        } else {
            $noIsChecked = 'checked';
        }
        $x .= '<tr>' . "\r\n";
        $x .= '<td class="field_title">' . $Lang['eNotice']["settings"]['notification'][$key] . '</td>' . "\r\n";
        $x .= '<td class="showTable">' . $content . '</td>' . "\r\n";
        $x .= '<td class="editTable"> <input type="radio" name="notify[' . $key . ']"  value="1" ' . $yesIsChecked . ' onclick="onClickRadio(this);"><label>' . $Lang['eNotice']["settings"]['notification']['yes'] . '</label>
        						<input type="radio" name="notify[' . $key . ']" value="0" ' . $noIsChecked . ' onclick="onClickRadio(this);" ><label>' . $Lang['eNotice']["settings"]['notification']['no'] . '</label> </td>' . "\r\n";
        $x .= '</tr>' . "\r\n";
        
        $key = 'push_message_due_date';
        $content = ($SettingAry[$key] == 1) ? $Lang['eNotice']["settings"]['notification']['yes'] : $Lang['eNotice']["settings"]['notification']['no'];
        $yesIsChecked = '';
        $noIsChecked = '';
        if ($SettingAry[$key]) {
            $yesIsChecked = 'checked';
        } else {
            $noIsChecked = 'checked';
        }
        $secondLayerTitle = 'push_message_due_date_reminder';
        
        $titleSecondLayer = $Lang['eNotice']["settings"]['notification'][$secondLayerTitle];
        $contentSecondLayer = $SettingAry[$secondLayerTitle];
        
        $y = '';
        $y .= '<div>' . "\r\n";
        $y .= '<table class="common_table_list_v30"}>' . "\r\n";
        $y .= '<tr>' . "\r\n";
        $y .= '<td class="rights_not_select_sub"  style="width:30%">' . $titleSecondLayer . '</td>' . "\r\n";
        $y .= '<td> ' . $contentSecondLayer . '</td>' . "\r\n";
        $y .= '</tr>' . "\r\n";
        $y .= '</table>' . "\r\n";
        $y .= '</div>' . "\r\n";
        $content .= $y;
        
        // Editable second layer
        $y = '<div>' . "\r\n";
        $y .= '<table class="common_table_list_v30"}>' . "\r\n";
        $y .= '<tr>' . "\r\n";
        $y .= '<td class="rights_not_select_sub"  style="width:30%">' . $titleSecondLayer . '</td>' . "\r\n";
        $y .= '<td>' . '<input type="number" name="notify2[' . $secondLayerTitle . ']" id="' . $secondLayerTitle . '" value="' . $contentSecondLayer . '">' . '</td>' . "\r\n";
        $y .= '</tr>' . "\r\n";
        $y .= '</table>' . "\r\n";
        $y .= '</div>' . "\r\n";
        $editableContent = $y;
        
        $x .= '<tr>' . "\r\n";
        $x .= '<td class="field_title">' . $Lang['eNotice']["settings"]['notification'][$key] . '</td>' . "\r\n";
        $x .= '<td class="showTable">' . $content . '</td>' . "\r\n";
        $x .= '<td class="editTable"> <input type="radio" name="notify[' . $key . ']" id="checkbox_'.$key.'" value="1" ' . $yesIsChecked . ' onclick="onClickRadio(this);"><label>' . $Lang['eNotice']["settings"]['notification']['yes'] . '</label>
        						  <input type="radio" name="notify[' . $key . ']" id="checkbox_'.$key.'" value="0" ' . $noIsChecked . ' onclick="onClickRadio(this);" ><label>' . $Lang['eNotice']["settings"]['notification']['no'] . '</label> ' . $editableContent . ' </td>' . "\r\n";
        $x .= '</tr>' . "\r\n";
        
        // ### Time selection boxes
        $key = 'push_message_time';
        $content = $SettingAry[$key];
        $title = $Lang['eNotice']["settings"]['notification']['push_message_time'];
        $x .= '<tr>' . "\r\n";
        $x .= '<td class="field_title">' . $title . '</td>' . "\r\n";
        $x .= '<td class="showTable">' . $content . '</td>' . "\r\n";
        $x .= '<td class="editTable"> ';
        
        $time = explode(":", $content);
        $y = $linterface->Get_Time_Selection_Box('hour', 'hour', $time[0]);
        $y .= " : ";
        $y .= $linterface->Get_Time_Selection_Box('minute', 'min', $time[1], $others_tab = '', $interval = 15);
        $x .= $y . ' <input type="hidden" name="notify2[' . $key . ']" id="' . $key . '" value="' . $content . '">';
        
        $x .= ' </td>' . "\r\n";
        $x .= '</tr>' . "\r\n";
        
        $x .= ' </td>' . "\r\n";
        $x .= '</tr>' . "\r\n";
        
        $x .= '</tbody>' . "\r\n";
    $x .= '</table>' . "\r\n";
}
// #######################################
// # END :PUSH MESSAGE ###############
// #######################################
// #######################################
// ## END: construct table content #####
// #######################################
$htmlAry['formTable'] = $x;

// ## buttons
$htmlAry['editBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Edit'], "button", "goEdit()", 'editBtn');
$htmlAry['submitBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Submit'], "button", "goSubmit()", 'submitBtn');
$htmlAry['resetBtn'] = $linterface->GET_ACTION_BTN($button_cancel, "button", "ResetInfo();", 'resetBtn');

?>
<style>
.editTable {
	display: none;
}
</style>
<script language="javascript">
$( document ).ready(function() {
  $('#submitBtn').hide();
  $('#resetBtn').hide();
});

function goEdit(){
	$('.editTable').show();
	$('.showTable').hide();
	$('#editBtn').hide();
	$('#submitBtn').show();
	$('#resetBtn').show();	
	if($('#checkbox_push_message_due_date')[0].checked){
		$('#push_message_due_date_reminder').removeAttr('disabled');
	}else{
		$('#push_message_due_date_reminder').attr('disabled','disabled');
	}	
}
function goSubmit(){
	var hour = $('#hour :selected').val();
	var min = $('#minute :selected').text();
	var timeString = hour+':'+min;
	
	$('#push_message_time').val(timeString);	
	//formCheck
	var due_date_reminder = $('#due_date_reminder').val();	
	var push_message_due_date_reminder = $('#push_message_due_date_reminder').val();
	
	if (($("input:radio[name='notify\[push_message_due_date\]']:checked").val() == 1) && ((push_message_due_date_reminder =='') || (push_message_due_date_reminder < 0) || (push_message_due_date_reminder != parseInt(push_message_due_date_reminder)))) {
		alert("<?=$Lang['eNotice']["settings"]['system']['notification_input_non_negative_integer']?>");
		$('#push_message_due_date_reminder').focus();
		return false;
	}
	
	$('#form1').submit();
}
function onClickRadio(obj){
	var name = obj.getAttribute("name");
	if(name=='notify[push_message_due_date]'){
		if(obj.value==1){
			$('#push_message_due_date_reminder').removeAttr('disabled');
		}else{
			$('#push_message_due_date_reminder').attr('disabled','disabled');
		}
	}
}
function ResetInfo()
{
	document.form1.reset();
	$('.editTable').hide();
	$('.showTable').show();
	$('#submitBtn').hide();
    $('#resetBtn').hide();
    $('#editBtn').show();
}
</script>
<form name="form1" id="form1" method="post" action="update.php">
	<div id="show_div">

		<div class="table_board">
			<?=$htmlAry['formTable']?>
			<p class="spacer"></p>
		</div>
		<br style="clear: both;" />

		<div class="edit_bottom_v30">
			<p class="spacer"></p>
			<?=$htmlAry['editBtn']?>
			<?=$htmlAry['submitBtn']?>
			<?=$htmlAry['resetBtn']?>
			<p class="spacer"></p>
		</div>
	</div>

</form>
<?php
echo $linterface->FOCUS_ON_LOAD("form1.LocationCode");

$linterface->LAYOUT_STOP();
intranet_closedb();

?>