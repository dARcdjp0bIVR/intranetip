<?php
# using:

################# Change Log [Start] ############
#
#   Date:   2020-09-07 [Bill]   [2020-0604-1821-16170]
#           Create file
#
################# Change Log [End] ############

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");

if(!$plugin['notice'] || !$_SESSION["SSV_USER_ACCESS"]["eAdmin-eNotice"])
{
    include_once($PATH_WRT_ROOT."includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit;
}

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/eclassapp_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();

$CurrentPageArr['eAdminNotice'] = 1;
$CurrentPage = "PageNoticeSettings_BasicSettings";

$lnotice = new libnotice();
$lgroup = new libgroup();
$luser = new libuser();
$lpayment = new libpayment();

# Left menu
$TAGS_OBJ[] = array($i_Notice_ElectronicNotice2, 'index.php', 0);
$TAGS_OBJ[] = array($Lang['eNotice']['PaymentNotice'], "payment_notice.php", 1);
$MODULE_OBJ = $lnotice->GET_MODULE_OBJ_ARR();

# Start layout
$xmsg = $xmsg==1? $Lang['eNotice']['SettingsUpdateSuccess'] : "";
$linterface->LAYOUT_START($xmsg);

$adminGroups = $lgroup->returnGroupsByCategory(1, Get_Current_Academic_Year_ID());
$normalGroupSelect = getSelectByArray($adminGroups,"name=payment_normalAccessGroupID",$lnotice->payment_normalAccessGroupID,2,0);
$fullGroupSelect = getSelectByArray($adminGroups,"name=payment_fullAccessGroupID",$lnotice->payment_fullAccessGroupID,2,0);
//$DisciplineGroupSelect = getSelectByArray($adminGroups,"name=payment_DisciplineGroupID",$lnotice->payment_DisciplineGroupID,2,0);

$NormalControlGroupInfo = $lgroup->returnGroup($lnotice->payment_normalAccessGroupID);
$NormalControlGroupInfo_year = getAcademicYearByAcademicYearID($NormalControlGroupInfo[0]['AcademicYearID'], $intranet_session_language);
$NormalControlGroupTitle = ($intranet_session_language=="en" ? $NormalControlGroupInfo[0]['Title'] : $NormalControlGroupInfo[0]['TitleChinese']) . " <span class='tabletextremark'>(". $NormalControlGroupInfo_year .")</span>";

$FullControlGroupInfo = $lgroup->returnGroup($lnotice->payment_fullAccessGroupID);
$FullControlGroupInfo_year = getAcademicYearByAcademicYearID($FullControlGroupInfo[0]['AcademicYearID'], $intranet_session_language);
$FullControlGroupTitle = ($intranet_session_language=="en" ? $FullControlGroupInfo[0]['Title'] : $FullControlGroupInfo[0]['TitleChinese']) . " <span class='tabletextremark'>(". $FullControlGroupInfo_year .")</span>";

/*
if($plugin['Disciplinev12'])
{
    $DisciplineGroupInfo = $lgroup->returnGroup($lnotice->payment_DisciplineGroupID);
    $DisciplineGroupInfo_year = getAcademicYearByAcademicYearID($DisciplineGroupInfo[0]['AcademicYearID'], $intranet_session_language);
    $DisciplineGroupTitle = ($intranet_session_language=="en" ? $DisciplineGroupInfo[0]['Title'] : $DisciplineGroupInfo[0]['TitleChinese']) . " <span class='tabletextremark'>(". $DisciplineGroupInfo_year .")</span>";
}
*/

for($i=1; $i<=30; $i++) {
    $data[] = array($i, $i);
}
$defaultNumDays_selection = getSelectByArray($data, "name='payment_defaultNumDays'", $lnotice->payment_defaultNumDays , 0, 1);

for($i=3; $i<=100; $i++) {
    $data1[] = array($i, $i);
}
$MaxReplySlipOption_selection = getSelectByArray($data1, "name='payment_MaxReplySlipOption'", $lnotice->payment_MaxReplySlipOption , 0, 1);

// Get Approval User Info
$approvalUserInfoAry = $lnotice->getApprovalUserInfo(NOTICE_SETTING_TYPE_PAYMENT);
$approvalUserStringForShow = '';
foreach($approvalUserInfoAry as $i => $approvalUserInfo){
    if($i != 0) {
        $approvalUserStringForShow .= '<br>';
    }
    $approvalUserStringForShow .= Get_Lang_Selection($approvalUserInfo['ChineseName'],$approvalUserInfo['EnglishName']);
}

// Get Approval User Selection Box
$approvalUserSelectionBox = '';
$approvalUserIDArray = Get_Array_By_Key($approvalUserInfoAry,'UserID');
$approvalUserSelectionBox = "<select name='approvalUserID[]' id='approvalUserID' size='6' multiple='multiple'>";
for($i = 0; $i < sizeof($approvalUserIDArray); $i++) {
    $approvalUserSelectionBox .= "<option value=\"".$approvalUserIDArray[$i]."\">".$luser->getNameWithClassNumber($approvalUserIDArray[$i], $IncludeArchivedUser=1)."</option>";
}
$approvalUserSelectionBox .= "</select>";
?>

<? include_once($PATH_WRT_ROOT."home/eClassApp/common/pushMessageTemplate/js_AppMessageReminder.php") ?>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/html_editor/fckeditor_cust.js" ></script>
<script language="javascript">
        <!--
        $(document).ready(function() {
            <?php if(!$lnotice->payment_needApproval){ ?>
                disbleApprovalGroupSeletor(0);
            <?php } ?>
        });

        function EditInfo()
        {
            $('#EditBtn').attr('style', 'visibility: hidden');
            $('.Edit_Hide').attr('style', 'display: none');
            $('.Edit_Show').attr('style', '');

            $('#UpdateBtn').attr('style', '');
            $('#cancelbtn').attr('style', '');
        }

        function ResetInfo()
        {
            document.form1.reset();

            $('#EditBtn').attr('style', '');
            $('.Edit_Hide').attr('style', '');
            $('.Edit_Show').attr('style', 'display: none');

            $('#UpdateBtn').attr('style', 'display: none');
            $('#cancelbtn').attr('style', 'display: none');
        }

        function goSubmit(obj){
            disbleApprovalGroupSeletor(1);
            checkOptionAll(obj.elements["approvalUserID[]"]);
            return true;
        }

        function chooseRadioNeedApproval(obj){
            var needApproval = obj.value;
            disbleApprovalGroupSeletor(needApproval);
        }

        function disbleApprovalGroupSeletor(enable){
            if(enable==1){
                $('#approvalUserID').removeAttr('disabled');
                $('#approvalUserDiv').css('display', 'inline-block');
            }else if(enable==0){
                $('#approvalUserID').attr('disabled','disabled');
                $('#approvalUserDiv').hide();
            }
        }
        //-->
        
        function canSeeAll(){
            if($('#teacherCanSeeAllNotice1').attr('checked')){
                $('#accessGroupCanSeeAllNotice1').attr('disabled', true);
                $('#accessGroupCanSeeAllNotice0').attr('disabled', true);
            } else {
                $('#accessGroupCanSeeAllNotice1').removeAttr('disabled');
                $('#accessGroupCanSeeAllNotice0').removeAttr('disabled');
            }
        }
</script>

    <form name="form1" method="post" action="payment_notice_update.php">
        <div class="table_board">
            <div class="table_row_tool row_content_tool">
                <?= $linterface->GET_SMALL_BTN($button_edit, "button","javascript:EditInfo();","EditBtn")?>
            </div>
            <p class="spacer"></p>
            <div class="form_sub_title_v30"><em>- <span class="field_title"> <?=$Lang['eNotice']['SysProperties']['General']?> </span>-</em>
                <p class="spacer"></p>
            </div>

            <table class="form_table_v30">
                <!--
                <tr valign='top'>
                    <td class="field_title" nowrap><?=$Lang['eNotice']['DisableNotice']?></td>
                    <td>
                        <span class="Edit_Hide"><?=$lnotice->payment_disabled ? $i_general_yes:$i_general_no?></span>
                        <span class="Edit_Show" style="display:none">
        				<input type="radio" name="disabled" value="1" <?=$lnotice->payment_disabled ? "checked":"" ?> id="disabled1"> <label for="disabled1"><?=$i_general_yes?></label>
        				<input type="radio" name="disabled" value="0" <?=$lnotice->payment_disabled ? "":"checked" ?> id="disabled0"> <label for="disabled0"><?=$i_general_no?></label>
    				</span>
                    </td>
                </tr>
                -->

                <tr valign='top'>
                    <td class="field_title" nowrap><?=$Lang['eNotice']['NormalControlGroup']?> </td>
                    <td>
                        <span class="Edit_Hide"><?=$NormalControlGroupTitle ? $NormalControlGroupTitle : $i_general_NotSet ?></span>
                        <span class="Edit_Show" style="display:none"><?=$normalGroupSelect?> (<?=$Lang['eNotice']['AdminGroupOnly']?>)</span>
                    </td>
                </tr>

                <tr valign='top'>
                    <td class="field_title" nowrap><?=$Lang['eNotice']['FullControlGroup']?></td>
                    <td>
                        <span class="Edit_Hide"><?=$FullControlGroupTitle ? $FullControlGroupTitle : $i_general_NotSet ?></span>
                        <span class="Edit_Show" style="display:none"><?=$fullGroupSelect?> (<?=$Lang['eNotice']['AdminGroupOnly']?>)</span>
                    </td>
                </tr>

                <!--
                <?php if ($sys_custom['eNotice']['HideParenteNotice']) { ?>
                    <input type="hidden" name="payment_isClassTeacherEditDisabled" value="0" id="payment_isClassTeacherEditDisabled">
                <?php } else { ?>
                    <tr valign='top'>
                        <td class="field_title" nowrap><?=$Lang['eNotice']['DisableClassTeacher']?></td>
                        <td>
                            <span class="Edit_Hide"><?=$lnotice->payment_isClassTeacherEditDisabled ? $i_general_yes : $i_general_no?></span>
                            <span class="Edit_Show" style="display:none">
        				<input type="radio" name="payment_isClassTeacherEditDisabled" value="1" <?=$lnotice->payment_isClassTeacherEditDisabled ? "checked":"" ?> id="isClassTeacherEditDisabled1"> <label for="isClassTeacherEditDisabled1"><?=$i_general_yes?></label>
        				<input type="radio" name="payment_isClassTeacherEditDisabled" value="0" <?=$lnotice->payment_isClassTeacherEditDisabled ? "":"checked" ?> id="isClassTeacherEditDisabled0"> <label for="isClassTeacherEditDisabled0"><?=$i_general_no?></label>
    				</span>
                        </td>
                    </tr>
                <?php } ?>
                -->

                <?php if ($sys_custom['eNotice']['HideParenteNotice']) { ?>
                    <input type="hidden" name="isAllowAdminToSignPaymentNotice" value="0" id="isAllowAdminToSignPaymentNotice0">
                <?php } else { ?>
                    <tr valign='top'>
                        <td class="field_title" nowrap><?=$Lang['eNotice']['AllowAdminToSignPaymentNotice'].($lpayment->isEWalletDirectPayEnabled()?'<br />('.$Lang['eNotice']['PaymentNoticeAdminSignForParentSettingRemark'].')':'')?></td>
                        <td>
                            <span class="Edit_Hide"><?=$lnotice->isAllowAdminToSignPaymentNotice ? $i_general_yes:$i_general_no?></span>
                            <span class="Edit_Show" style="display:none">
        				<input type="radio" name="isAllowAdminToSignPaymentNotice" value="1" <?=$lnotice->isAllowAdminToSignPaymentNotice ? "checked":"" ?> id="isAllowAdminToSignPaymentNotice1"> <label for="isAllowAdminToSignPaymentNotice1"><?=$i_general_yes?></label>
        				<input type="radio" name="isAllowAdminToSignPaymentNotice" value="0" <?=$lnotice->isAllowAdminToSignPaymentNotice ? "":"checked" ?> id="isAllowAdminToSignPaymentNotice0"> <label for="isAllowAdminToSignPaymentNotice0"><?=$i_general_no?></label>
					</span>
                        </td>
                    </tr>
                <?php } ?>

                <!--
                <?php if ($sys_custom['eNotice']['HideParenteNotice']) { ?>
                    <input type="hidden" name="payment_isPICAllowReply" value="0" id="payment_isPICAllowReply">
                <?php } else { ?>
                    <tr valign='top'>
                        <td class="field_title" nowrap><?=$Lang['eNotice']['PICAllowReply']?></td>
                        <td>
                            <span class="Edit_Hide"><?=$lnotice->payment_isPICAllowReply ? $i_general_yes:$i_general_no?></span>
                            <span class="Edit_Show" style="display:none">
        				<input type="radio" name="payment_isPICAllowReply" value="1" <?=$lnotice->payment_isPICAllowReply ? "checked":"" ?> id="isPICAllowReply1"> <label for="isPICAllowReply1"><?=$i_general_yes?></label>
        				<input type="radio" name="payment_isPICAllowReply" value="0" <?=$lnotice->payment_isPICAllowReply ? "":"checked" ?> id="isPICAllowReply0"> <label for="isPICAllowReply0"><?=$i_general_no?></label>
					</span>
                        </td>
                    </tr>
                <?php } ?>
                -->

                <tr valign='top'>
                    <td class="field_title" nowrap><?=$Lang['eNotice']['AllHaveRight']?></td>
                    <td>
                        <span class="Edit_Hide"><?=$lnotice->payment_isAllAllowed ? $i_general_yes:$i_general_no?></span>
                        <span class="Edit_Show" style="display:none">
        				<input type="radio" name="payment_isAllAllowed" value="1" <?=$lnotice->payment_isAllAllowed ? "checked":"" ?> id="isAllAllowed1"> <label for="isAllAllowed1"><?=$i_general_yes?></label>
        				<input type="radio" name="payment_isAllAllowed" value="0" <?=$lnotice->payment_isAllAllowed ? "":"checked" ?> id="isAllAllowed0"> <label for="isAllAllowed0"><?=$i_general_no?></label>
    				</span>
                    </td>
                </tr>

                <tr valign='top'>
                    <td class="field_title" nowrap><?=$Lang['eNotice']['AllStaffAllow']?></td>
                    <td>
                        <span class="Edit_Hide"><?=$lnotice->payment_staffview ? $i_general_yes:$i_general_no?></span>
                        <span class="Edit_Show" style="display:none">
        				<input type="radio" name="payment_staffview" value="1" <?=$lnotice->payment_staffview ? "checked":"" ?> id="staffview1"> <label for="staffview1"><?=$i_general_yes?></label>
        				<input type="radio" name="payment_staffview" value="0" <?=$lnotice->payment_staffview ? "":"checked" ?> id="staffview0"> <label for="staffview0"><?=$i_general_no?></label>
    				</span>
                    </td>
                </tr>

                <?php if($plugin['eClassApp']){ ?>
                    <tr valign='top'>
                        <td class="field_title" nowrap><?=$Lang['eNotice']['AllowClassTeacherSendeNoticeMessage']?></td>
                        <td>
                            <span class="Edit_Hide"><?=$lnotice->payment_isAllowClassTeacherSendeNoticeMessage ? $i_general_yes:$i_general_no?></span>
                            <span class="Edit_Show" style="display:none">
        				<input type="radio" name="payment_isAllowClassTeacherSendeNoticeMessage" value="1" <?=$lnotice->payment_isAllowClassTeacherSendeNoticeMessage ? "checked":"" ?> id="isAllowClassTeacherSendeNoticeMessage1"> <label for="isAllowClassTeacherSendeNoticeMessage1"><?=$i_general_yes?></label>
        				<input type="radio" name="payment_isAllowClassTeacherSendeNoticeMessage" value="0" <?=$lnotice->payment_isAllowClassTeacherSendeNoticeMessage ? "":"checked" ?> id="isAllowClassTeacherSendeNoticeMessage0"> <label for="isAllowClassTeacherSendeNoticeMessage0"><?=$i_general_no?></label>
    				</span>
                        </td>
                    </tr>
                <?php } ?>

                <tr valign='top'>
                    <td class="field_title" nowrap><?=$Lang['eNotice']['DefaultNumDays']?></td>
                    <td>
                        <span class="Edit_Hide"><?=$lnotice->payment_defaultNumDays?></span>
                        <span class="Edit_Show" style="display:none"><?=$defaultNumDays_selection?></span>
                    </td>
                </tr>

                <!--
                <? /* ?>
                <tr valign='top'>
                    <td class="field_title" nowrap><?=$Lang['eNotice']['ParentStudentCanViewAll']?></td>
                    <td>
                    <span class="Edit_Hide"><?=$lnotice->payment_showAllEnabled ? $i_general_yes:$i_general_no?></span>
                    <span class="Edit_Show" style="display:none">
                    <input type="radio" name="payment_showAllEnabled" value="1" <?=$lnotice->payment_showAllEnabled ? "checked":"" ?> id="showAllEnabled1"> <label for="showAllEnabled1"><?=$i_general_yes?></label>
                    <input type="radio" name="payment_showAllEnabled" value="0" <?=$lnotice->payment_showAllEnabled ? "":"checked" ?> id="showAllEnabled0"> <label for="showAllEnabled0"><?=$i_general_no?></label>
                    </span></td>
                </tr>
                <? */ ?>

                <tr valign='top'>
                    <td class="field_title" nowrap><?=$Lang['eNotice']['AllowLateSign']?></td>
                    <td>
                        <span class="Edit_Hide"><?=$lnotice->payment_isLateSignAllow ? $i_general_yes:$i_general_no?></span>
                        <span class="Edit_Show" style="display:none">
        				<input type="radio" name="payment_isLateSignAllow" value="1" <?=$lnotice->payment_isLateSignAllow ? "checked":"" ?> id="isLateSignAllow1"> <label for="isLateSignAllow1"><?=$i_general_yes?></label>
        				<input type="radio" name="payment_isLateSignAllow" value="0" <?=$lnotice->payment_isLateSignAllow ? "":"checked" ?> id="isLateSignAllow0"> <label for="isLateSignAllow0"><?=$i_general_no?></label>
    				</span>
                    </td>
                </tr>

                <tr valign='top'>
                    <td class="field_title" nowrap><?=$Lang['eNotice']['NotAllowReSign']?></td>
                    <td>
                        <span class="Edit_Hide"><?=$lnotice->payment_NotAllowReSign ? $i_general_yes:$i_general_no?></span>
                        <span class="Edit_Show" style="display:none">
        				<input type="radio" name="payment_NotAllowReSign" value="1" <?=$lnotice->payment_NotAllowReSign ? "checked":"" ?> id="NotAllowReSign1"> <label for="NotAllowReSign1"><?=$i_general_yes?></label>
        				<input type="radio" name="payment_NotAllowReSign" value="0" <?=$lnotice->payment_NotAllowReSign ? "":"checked" ?> id="NotAllowReSign0"> <label for="NotAllowReSign0"><?=$i_general_no?></label>
    				</span>
                    </td>
                </tr>
                -->

                <tr valign='top'>
                    <td class="field_title" nowrap><?=$Lang['eNotice']['MaxNumberReplySlipOption']?></td>
                    <td>
                        <span class="Edit_Hide"><?=$lnotice->payment_MaxReplySlipOption?></span>
                        <span class="Edit_Show" style="display:none"><?=$MaxReplySlipOption_selection?></span>
                    </td>
                </tr>

                <tr valign='top'>
                    <td class="field_title" nowrap><?= $eComm['NeedApproval'] ?></td>
                    <td><span class="Edit_Hide">
					    <?= $lnotice->payment_needApproval ? $i_general_yes:$i_general_no?>
                        <div>
                            <table class="common_table_list_v30">
                                <tr>
                                    <td class="field_title" style="width:30%">
                                        <?=  $Lang['eNotice']['ApprovalUser'] ?> <br> <?= $Lang['eNotice']['AdminGroupHaveApprovalRight'] ?>
                                    </td>
                                    <td>
                                        <?= $approvalUserStringForShow ?>
                                    </td>
                                </tr>
                            </table>
                        </div>
				        </span>
                        <span class="Edit_Show" style="display:none">
                            <input type="radio" name="payment_needApproval" value="1" <?= $lnotice->payment_needApproval ? "checked":"" ?> id="needApprovalY" onclick="chooseRadioNeedApproval(this);"> <label for="needApprovalY"><?=$i_general_yes?></label>
                            <input type="radio" name="payment_needApproval"  value="0" <?= $lnotice->payment_needApproval? "":"checked" ?> id="needApprovalN" onclick="chooseRadioNeedApproval(this);"> <label for="needApprovalN"><?=$i_general_no?></label>
                            <div>
                                <table class="common_table_list_v30">
                                    <tr>
                                        <td class="field_title" style="width:30%">
                                            <?=  $Lang['eNotice']['ApprovalUser'] ?>
                                        </td>
                                        <td>
                                            <?= $approvalUserSelectionBox ?>
                                            <div style="display:inline-block" id="approvalUserDiv">
                                                <?= $linterface->GET_SMALL_BTN($button_select, "button", "javascript:newWindow('/home/common_choose/index.php?page_title=SelectMembers&permitted_type=1&fieldname=approvalUserID[]&excluded_type=4', 9)")?><br>
                                                <?= $linterface->GET_SMALL_BTN($button_remove, "button", "javascript:checkOptionRemove(document.form1.elements['approvalUserID[]']);");?>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
				    </span></td>
                </tr>

                <!-- villa 2016-09-23 -->
                <?php if($plugin['eClassApp']){?>
                    <tr valign='top'>
                        <td class="field_title" nowrap><?=$Lang['eClassApp']['DefaultUserNotifyUsingApp']?></td>
                        <td>
                            <span class="Edit_Hide"><?=$lnotice->payment_sendPushMsg ? $Lang['General']['Yes']:$Lang['General']['No']?></span>
                            <span class="Edit_Show" style="display:none">
                                <input type="radio" id= "sendPushMsg_yes" name="payment_sendPushMsg" value="1" <?=$lnotice->payment_sendPushMsg ? "checked":"" ?>> <label for="sendPushMsg_yes"><?=$Lang['General']['Yes']?></label>
                                <input type="radio" id= "sendPushMsg_no" name="payment_sendPushMsg" value="0" <?=$lnotice->payment_sendPushMsg ? "":"checked" ?>> <label for="sendPushMsg_no"><?=$Lang['General']['No']?></label>
                            </span>
                        </td>
                    </tr>
                <?php }?>

                <tr valign='top'>
                    <td class="field_title" nowrap><?=$Lang['eNotice']['DefaultUserNotifyUsingEmail']?></td>
                    <td>
                        <span class="Edit_Hide"><?=$lnotice->payment_sendEmail ? $Lang['General']['Yes']:$Lang['General']['No']?></span>
                        <span class="Edit_Show" style="display:none">
        				<input type="radio" id= "sendEmail_yes" name="payment_sendEmail" value="1" <?=$lnotice->payment_sendEmail? "checked":"" ?>> <label for="sendEmail_yes"><?=$Lang['General']['Yes']?></label>
        				<input type="radio" id= "sendEmail_no" name="payment_sendEmail" value="0" <?=$lnotice->payment_sendEmail? "":"checked" ?>> <label for="sendEmail_no"><?=$Lang['General']['No']?></label>
					</span>
                    </td>
                </tr>
            </table>

            <!--
            <? if($plugin['Disciplinev12'] && $_SESSION["platform"] != "KIS") {?>
                <p class="spacer"></p>
                <div class="form_sub_title_v30"><em>- <span class="field_title"> <?=$Lang['Header']['Menu']['eDiscipline']?> </span>-</em>
                    <p class="spacer"></p>
                </div>
                <table class="form_table_v30">

                    <tr valign='top'>
                        <td class="field_title" nowrap><?=$Lang['eNotice']['DisciplineGroup']?></td>
                        <td>
                            <span class="Edit_Hide"><?=$DisciplineGroupTitle ? $DisciplineGroupTitle : $i_general_NotSet ?></span>
                            <span class="Edit_Show" style="display:none"><?=$DisciplineGroupSelect?> (<?=$Lang['eNotice']['AdminGroupOnly']?>)</span>
                        </td>
                    </tr>

                    <tr valign='top'>
                        <td class="field_title" nowrap><?=$Lang['eNotice']['DefaultNumDays']?></td>
                        <td>
                            <span class="Edit_Hide"><?=($lnotice->payment_defaultDisNumDays ? $lnotice->payment_defaultDisNumDays : $lnotice->payment_defaultNumDays)?></span>
                            <span class="Edit_Show" style="display:none"><?=$defaultDisNumDays_selection?></span>
                        </td>
                    </tr>

                    <tr valign='top'>
                        <td class="field_title" nowrap><?=$Lang['eNotice']['AllowClassTeacerAccessDisciplineNotice']?></td>
                        <td>
                            <span class="Edit_Hide"><?=$lnotice->payment_ClassTeacherCanAccessDisciplineNotice ? $i_general_yes:$i_general_no?></span>
                            <span class="Edit_Show" style="display:none">
                                <input type="radio" name="payment_ClassTeacherCanAccessDisciplineNotice" value="1" <?=$lnotice->payment_ClassTeacherCanAccessDisciplineNotice ? "checked":"" ?> id="ClassTeacherCanAccessDisciplineNotice1"> <label for="ClassTeacherCanAccessDisciplineNotice1"><?=$i_general_yes?></label>
                                <input type="radio" name="payment_ClassTeacherCanAccessDisciplineNotice" value="0" <?=$lnotice->payment_ClassTeacherCanAccessDisciplineNotice ? "":"checked" ?> id="ClassTeacherCanAccessDisciplineNotice0"> <label for="ClassTeacherCanAccessDisciplineNotice0"><?=$i_general_no?></label>
                            </span>
                        </td>
                    </tr>
                </table>
            <? } ?>
            -->

            <?php if($plugin['eClassTeacherApp']){?>
                <div class="form_sub_title_v30"><em>- <span class="field_title"> <?=$Lang['eClassApp']['TeacherApp']?> </span>-</em>
                    <p class="spacer"></p>
                </div>

                <table class="form_table_v30">
                    <tr valign='top'>
                        <td class="field_title" nowrap><?=$Lang['eNotice']['TeacherCanSeeAllNotice']?></td>
                        <td>
                            <span class="Edit_Hide"><?=$lnotice->payment_teacherCanSeeAllNotice ? $i_general_yes:$i_general_no?></span>
                            <span class="Edit_Show" style="display:none">
                                <input type="radio" name="payment_teacherCanSeeAllNotice" value="1" <?=$lnotice->payment_teacherCanSeeAllNotice ? "checked":"" ?> id="teacherCanSeeAllNotice1" onclick='canSeeAll()'> <label for="teacherCanSeeAllNotice1"><?=$i_general_yes?></label>
                                <input type="radio" name="payment_teacherCanSeeAllNotice" value="0" <?=$lnotice->payment_teacherCanSeeAllNotice ? "":"checked" ?> id="teacherCanSeeAllNotice0" onclick='canSeeAll()'> <label for="teacherCanSeeAllNotice0"><?=$i_general_no?></label>
                            </span>
                        </td>
                    </tr>
                    <tr valign='top'>
                        <td class="field_title" nowrap><?=$Lang['eNotice']['AccessGroupCanSeeAllNotice']?></td>
                        <td>
                            <span class="Edit_Hide"><?=$lnotice->payment_accessGroupCanSeeAllNotice ? $i_general_yes:$i_general_no?></span>
                            <span class="Edit_Show" style="display:none">
                                <input type="radio" name="payment_accessGroupCanSeeAllNotice" value="1" <?=$lnotice->payment_accessGroupCanSeeAllNotice ? "checked":"" ?> id="accessGroupCanSeeAllNotice1"> <label for="accessGroupCanSeeAllNotice1"><?=$i_general_yes?></label>
                                <input type="radio" name="payment_accessGroupCanSeeAllNotice" value="0" <?=$lnotice->payment_accessGroupCanSeeAllNotice ? "":"checked" ?> id="accessGroupCanSeeAllNotice0"> <label for="accessGroupCanSeeAllNotice0"><?=$i_general_no?></label>
                            </span>
                        </td>
                    </tr>
                </table>
            <? } ?>

            <div class="edit_bottom_v30">
                <p class="spacer"></p>
                <?= $linterface->GET_ACTION_BTN($button_update, "submit","return goSubmit(document.form1);","UpdateBtn", "style='display: none'") ?>
                <?= $linterface->GET_ACTION_BTN($button_cancel, "button", "ResetInfo();","cancelbtn", "style='display: none'") ?>
                <p class="spacer"></p>
            </div>
        </div>

        <?php echo csrfTokenHtml(generateCsrfToken()); ?>
    </form>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>