<?php
// using : 

################## Change Log [Start] ##############
#   Date:   2020-06-29  Ray
#           Added multi payment gateway
#
#	Date:	2019-10-17	Philips
#			Added OverrideExistScheduledMessage of eNoticeS
#
#	Date:	2019-04-09  Carlos
#			use new library eNoticePaymentReplySlip.php to get related payment item ids, it supports more question types and have more features.
#
#	Date:	2018-12-06  Bill    [2018-1205-1037-34207]
#			use overrideExistingScheduledPushMessageFromModule() to remove not yet sent scheduled push message
#
#	Date:	2014-06-23 YatWoon	> ip.2.5.5.8.1
#			Improved: Revised system return message method [Case#Q63447]
#
####################################################

$PATH_WRT_ROOT = "../../../../../"; 
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp.php");
include_once($PATH_WRT_ROOT."includes/eNoticePaymentReplySlip.php");

intranet_auth();
intranet_opendb();

$lu = new libuser($UserID);
$eNoticePaymentReplySlip = new eNoticePaymentReplySlip();

$lu->Start_Trans();

$libeClassApp = new libeClassApp();

$NoticeID = $NoticeIDArr;
for($i=0;$i<sizeof($NoticeID);$i++)
{
	   $thisNoticeID = $NoticeID[$i];
        $lnotice = new libnotice($thisNoticeID);
        
        $lf = new libfilesystem();
        if ($lnotice->disabled || !$lnotice->hasRemoveRight())
        {
			include_once($PATH_WRT_ROOT."includes/libaccessright.php");
			$laccessright = new libaccessright();
			$laccessright->NO_ACCESS_RIGHT_REDIRECT();
			exit;
        }
        
        # Add deletion log
        include_once($PATH_WRT_ROOT."includes/liblog.php");
		$lg = new liblog();
		$sql = "select * FROM INTRANET_NOTICE WHERE NoticeID = '$thisNoticeID'";
		$result = $lnotice->returnArray($sql);
		foreach($result as $k=>$d)
		{
			$this_id = $thisNoticeID;
			
			$RecordDetailAry = array();
			$RecordDetailAry['DateStart'] = substr($d['DateStart'],0,10);
			$RecordDetailAry['DateEnd'] = substr($d['DateEnd'],0,10);
			$RecordDetailAry['IssueUserID'] = $d['IssueUserID'];
			$RecordDetailAry['IssueUserName'] = $d['IssueUserName'];
			$RecordDetailAry['NoticeNumber'] = $d['NoticeNumber'];
			$RecordDetailAry['Title'] = $d['Title'];
			$RecordDetailAry['NoticeType'] = "Payment Notice";
			
			$lg->INSERT_LOG('eNotice', 'eNotice', $RecordDetailAry, 'INTRANET_NOTICE', $thisNoticeID);
		}
		
		// remove all scheduled push message
		$libeClassApp->overrideExistingScheduledPushMessageFromModule("eNotice", $thisNoticeID, $newNotifyMessageId='');
		$libeClassApp->overrideExistingScheduledPushMessageFromModule("eNoticeS", $thisNoticeID, $newNotifyMessageId='');
		
        ### Check any student reply the payment notice already ###
        $sql = "SELECT COUNT(*) FROM INTRANET_NOTICE_REPLY WHERE NoticeID = '$thisNoticeID' AND RecordStatus=2";
        $NoticeReplyExist = $lnotice->returnVector($sql);
        if($NoticeReplyExist[0] > 0)
        {
        	### Someone replied the payment notice already ###
        	### Action : 	DISABLE the record in INTRANET_NOTICE
        	
        	$sql = "UPDATE INTRANET_NOTICE SET isDeleted = 1, DateModified=NOW() WHERE NoticeID = '$thisNoticeID'";
        	$result[] = $lnotice->db_db_query($sql);
    	}
    	else
    	{
    		### Nobody replied the payment notice yet ###
    		### Action : 	DELETE the record in INTRANET_NOTICE
    		### 			DELETE the record in INTRANET_NOTICE_REPLY
    		###				DELETE the record in PAYMENT_PAYMENT_ITEM
    		
    		### retrive all the related Payment Item IDs
	        $sql = "SELECT Question FROM INTRANET_NOTICE WHERE NoticeID = '$thisNoticeID'";
	        $strQuestion = $lnotice->returnVector($sql);
	                
	        //$arrPaymentItemID = $lnotice->returnPaymentItemID($strQuestion[0]);
	        $arrPaymentItemID = $eNoticePaymentReplySlip->getAllPaymentItemID($strQuestion[0]);
	        if(sizeof($arrPaymentItemID)>0)
	        {
	        	$targetPaymentItemID = implode(",",$arrPaymentItemID);
	        	
	        	### delete the Payment Items
	        	$sql = "DELETE FROM PAYMENT_PAYMENT_ITEM WHERE ItemID IN ($targetPaymentItemID)";
	        	$result[] = $lnotice->db_db_query($sql);

	        	if($sys_custom['ePayment']['MultiPaymentGateway']) {
					$sql = "DELETE FROM PAYMENT_PAYMENT_ITEM_PAYMENT_GATEWAY WHERE ItemID IN ($targetPaymentItemID)";
					$result[] = $lnotice->db_db_query($sql);
				}
	        }
        	
    		$sql = "DELETE FROM INTRANET_NOTICE_REPLY WHERE NoticeID = '$thisNoticeID'";
	        $result[] = $lnotice->db_db_query($sql);
	        
	        # Grab attachment
	        $path = "$file_path/file/notice/".$lnotice->Attachment;
	        if (trim($lnotice->Attachment) != "" && is_dir($path))
	        {
	        		  if($bug_tracing['notice_attachment_log']){
	                     $temp_log_filepath = "$file_path/file/log_notice_attachment.txt";
	                     $temp_time = date("Y-m-d H:i:s");
	                     $temp_user = $UserID;
	                     $temp_page = 'notice_remove_update.php';
	                     $temp_action="remove path:".$path;
	                     $temp_content = get_file_content($temp_log_filepath);
	                     $temp_logentry = "\"$temp_time\",\"$temp_user\",\"$temp_page\",\"$temp_action\"\n";
	                     $temp_content .= $temp_logentry;
	                     write_file_content($temp_content, $temp_log_filepath);
	                     $temp_cmd = "mv ".OsCommandSafe($path)." ".OsCommandSafe($path)."_bak";
	                	 exec($temp_cmd);
	        
	        		 }else{
	            		$lf->lfs_remove($path);
	            	}
	        }
	        
	        $sql = "DELETE FROM INTRANET_NOTICE WHERE NoticeID = '$thisNoticeID'";
	        $result[] = $lnotice->db_db_query($sql);

			if($sys_custom['ePayment']['MultiPaymentGateway']) {
				$sql = "DELETE FROM INTRANET_NOTICE_PAYMENT_GATEWAY WHERE NoticeID='$thisNoticeID'";
				$result[] = $lnotice->db_db_query($sql);
			}
    	}
}

if(!in_array(false,$result)) {
	$lu->Commit_Trans();
} else {
	$lu->RollBack_Trans();
}
        
intranet_closedb();

$location = "paymentNotice.php";

//header("Location: $location?noticeType=$noticeType&status=$status&year=$year&month=$month&xmsg=delete");
header("Location: $location?noticeType=".urlencode($noticeType)."&status=".urlencode($status)."&year=".urlencode($year)."&month=".urlencode($month)."&xmsg=DeleteSuccess");
?>