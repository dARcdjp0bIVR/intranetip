<?php
// Using : 

##### Change Log [Start] #####
#
#   Date:   2020-10-30  Ray
#           Add quota
#
#   Date:   2020-10-23  Ray
#           Add remove unsigned student
#
# 	Date:   2020-09-07  Bill    [2020-0604-1821-16170]
#			access right checking > replace isApprovalUser() by isPaymentNoticeApprovalUser()
#
#   Date:   2020-06-29  Ray
#           Added multi payment gateway
#
#	Date:	2019-10-17	Philips
#			Added OverrideExistScheduledMessage of eNoticeS
#
#	Date:	2019-10-14  Philips
#			Added $pushmessagenotify_PIC, $pushmessagenotify_ClassTeachers, $emailnotify_Students, $emailnotify_PIC, $emailnotify_ClassTeachers
#
#   Date:   2019-09-19  Bill    [2019-0917-1522-01206]
#           skip student status checking when send push message to parents
#
#	Date:	2019-04-08  Carlos
#			Improved reply slip with new php library eNoticePaymentReplySlip.php
#
#	Date:	2019-03-28 Carlos
#			Improved to allow add more target students.
#
#   Date:   2019-01-25 Bill     [2019-0118-1125-41206]
#           Improved: Description - support text input from FCKEditor
#
#   Date:   2018-12-06 Bill    [2018-1205-1037-34207]
#           use overrideExistingScheduledPushMessageFromModule() to remove not yet sent scheduled push message
#
#	Date:	2018-10-04 Bill
#			get noticeAttFolder from $_POST 
#
#	Date:	2018-09-28 Bill		[2018-0809-0935-01276]
#			Support New Payment Question Type - Must Pay
#
#	Date:	2018-08-21 Carlos 
#			$sys_custom['ePayment']['Alipay'] - add selection option to set Alipay merchant account for payment item.
#
#	Date:	2017-09-20 (Bill)	[2017-0914-1550-19207]
#			not allow to edit payment item when item_id = 1  (act as template item) 
#
#	Date:	2016-11-24 (Bill)	[2016-1124-1120-39066]
#			fixed cannot update notice if reply slip contain single quote
#
#	Date:	2016-11-18 (Bill)	[2016-1118-1145-57207]
#			update notice attachment field if that field is empty
#
# 	Date:	2016-11-14 (Bill)	[2016-0927-1003-30054]
#			- fixed error if payment item name is changed in ePayment
# 				(Error: delete existing payment item and create a new once)
# 			- removed categoetid checking when query related payment item name
# 			- updated logic for editing payment items from comparing item name to having related payment items 
#
#	Date:	2016-10-17	Bill	[2016-1013-1541-17214]
#			- allow approval user to edit payment notice
#
#	Date:	2016-09-26	Villa
#			-add send psuMsg in schedule
#
#	Date:	2016-07-11	Kenneth
#			- add approval logic
#
#	Date:	2016-06-06	Kenneth
#			- add '23:59:59' into DateEnd
#
#	Date:	2015-12-11	Bill
#			- replace session_unregister() by session_unregister_intranet() for PHP 5.4
#
#	Date:	2015-05-11 Omas	[Case #J73013]
#			- enabled send module mail new footer
#
#	Date:	2015-04-14 Bill
#			import UserID of PICs to INTRANET_NOTICE_PIC [2015-0323-1602-46073]
#
#	Date:	2014-10-30 Ivan > ip.2.5.5.10.1
#			Fixed: special character handling in reply slip
#
#	Date:	2014-10-14 Roy	> ip.2.5.5.10.1
#			Improved: allow send push message to parent
#
#	Date:	2014-06-23 YatWoon	> ip.2.5.5.8.1
#			Improved: Revised system return message method [Case#Q63447]
#
#	Date:	2014-04-01 Carlos
#			Modified template would not pre-create payment item records	
#
#	Date:	2014-03-19 Carlos
#			Fixed reply slip question #PaymentItemID# replacement problem for template questions and new questions
#
#	Date:	2011-11-01 (Henry Chow)
#			cater non-payment item (textarea, payment type = 10) 
#
#	Date:	2011-03-25	YatWoon
#			change email notification subject & content data 
#
#	Date:	2011-02-16	YatWoon
#			Add option "Display question number"
#
#	Date	:	2010-10-29 (Henry Chow)
#				revise the code on restruction of question string
#
###### Change Log [End] ######

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libfiletable.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libemail.php");
include_once($PATH_WRT_ROOT."includes/libsendmail.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
// include_once($PATH_WRT_ROOT."lang/email.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp.php");
include_once($PATH_WRT_ROOT."includes/eNoticePaymentReplySlip.php");

intranet_auth();
intranet_opendb();

$lnotice = new libnotice($NoticeID);
$lf = new libfilesystem();
$libeClassApp = new libeClassApp();
$luser = new libuser();
$lpayment = new libpayment();
$eNoticePaymentReplySlip = new eNoticePaymentReplySlip();

// [2020-0604-1821-16170]
// [2016-1013-1541-17214]
//if (!$plugin['notice'] && $_SESSION["SSV_PRIVILEGE"]["notice"]["disabled"] || !$lnotice->isNoticeEditable())
//if (!$plugin['notice'] && $_SESSION["SSV_PRIVILEGE"]["notice"]["disabled"] || (!$lnotice->isNoticeEditable() && !$lnotice->isApprovalUser()))
if (!$plugin['notice'] && $_SESSION["SSV_PRIVILEGE"]["notice"]["disabled"] || (!$lnotice->isNoticeEditable() && !$lnotice->isPaymentNoticeApprovalUser()))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$NoticeNumber = intranet_htmlspecialchars(trim($NoticeNumber));
$Title = intranet_htmlspecialchars(trim($Title));

// [2019-0118-1125-41206] handle Description input from FCKEditor
if($lnotice->RecordStatus != 1)
{
    $Description_updated = ($lf->copy_fck_flash_image_upload($NoticeID, stripslashes($Description), $file_path, $cfg['fck_image']['eNotice']));
    $Description = addslashes($Description_updated);
    if ($userBrowser->platform=="iPad" || $userBrowser->platform=="Andriod")
    {
        if ($Description==strip_tags($Description))
        {
            $Description = nl2br($Description);
        }
    }
}
$Description = intranet_htmlspecialchars(trim($Description));

//$qStr = intranet_htmlspecialchars(trim($qStr));

// [2016-1124-1120-39066] for 1st update SQL for INTRANET_NOTICE - keep slashes in Question
$original_qStr = trim($qStr);

// [2016-1124-1120-39066] for create / edit payment items and 2nd update SQL for INTRANET_NOTICE - remove slashes in Question for futhur handling 
$qStr = standardizeFormPostValue($qStr);

$pushMsgNoticeNumber = standardizeFormPostValue($_POST['NoticeNumber']);
$pushMsgTitle = standardizeFormPostValue($_POST['Title']);

$AllFieldsReq = $AllFieldsReq==1?1:0;
$DisplayQuestionNumber = $DisplayQuestionNumber==1 ? 1 : 0;

$AttachmentStr = (sizeof($Attachment)==0) ? "" : implode(",", $Attachment);
$AttachmentStr = stripslashes($AttachmentStr);

$noticeAttFolderName = $_POST['noticeAttFolder'];
$path = "$file_path/file/notice/".$noticeAttFolderName;

$lu = new libfilesystem();

if(!empty($noticeAttFolderName) && is_dir($path))
	$lu->folder_remove_recursive($path);

if (is_dir($path."tmp") && !is_dir($path))
{
    $command ="mv ".OsCommandSafe($path)."tmp ".OsCommandSafe($path);
    exec($command);
	if($bug_tracing['notice_attachment_log']){
         $temp_log_filepath = "$file_path/file/log_notice_attachment.txt";
         $temp_time = date("Y-m-d H:i:s");
         $temp_user = $UserID;
         $temp_page = 'notice_edit_update.php';
         $temp_action=$command;
         $temp_content = get_file_content($temp_log_filepath);
         $temp_logentry = "\"$temp_time\",\"$temp_user\",\"$temp_page\",\"$temp_action\"\n";
         $temp_content .= $temp_logentry;
         write_file_content($temp_content, $temp_log_filepath);
	}
	
	// [2016-1118-1145-57207] Need to update attachment field if the field is empty
	$updatedAttachmentField = trim($lnotice->Attachment)=="";
}

$lo = new libfiletable("", $path, 0, 0, "");
$files = $lo->files;
while (list($key, $value) = each($files)) {
     if(!strstr($AttachmentStr,$files[$key][0])){
	     if($bug_tracing['notice_attachment_log']){
             $temp_log_filepath = "$file_path/file/log_notice_attachment.txt";
             $temp_time = date("Y-m-d H:i:s");
             $temp_user = $UserID;
             $temp_page = 'notice_edit_update.php';
             $temp_action="remove file:".$path."/".$files[$key][0];
             $temp_content = get_file_content($temp_log_filepath);
             $temp_logentry = "\"$temp_time\",\"$temp_user\",\"$temp_page\",\"$temp_action\"\n";
             $temp_content .= $temp_logentry;
             write_file_content($temp_content, $temp_log_filepath);
		 }
          $lu->file_remove($path."/".$files[$key][0]);
     }
}
$IsAttachment = (isset($Attachment)) ? 1 : 0;
$Attachment = $noticeAttFolderName;

if ($IsAttachment)
{
}
else
{
    if ($noticeAttFolderName != ""){
	     if($bug_tracing['notice_attachment_log']){
             $temp_log_filepath = "$file_path/file/log_notice_attachment.txt";
             $temp_time = date("Y-m-d H:i:s");
             $temp_user = $UserID;
             $temp_page = 'notice_edit_update.php';
             $temp_action="remove path:".$path;
             $temp_content = get_file_content($temp_log_filepath);
             $temp_logentry = "\"$temp_time\",\"$temp_user\",\"$temp_page\",\"$temp_action\"\n";
             $temp_content .= $temp_logentry;
             write_file_content($temp_content, $temp_log_filepath);
             
             # rename folder
             $temp_cmd = "mv ".OsCommandSafe($path)." ".OsCommandSafe($path)."_bak";
        	exec($temp_cmd);

		 }else{
        	 $lu->lfs_remove($path);
        }
    }
}

# Store to DB
$fields = "";
$DateEnd = $DateEnd.' '.$DateEndTime;
$DateStart = $DateStart.' '.$DateStartTime;
$fields .= "NoticeNumber = '$NoticeNumber'";
$fields .= ",Title = '$Title'";
if($lnotice->RecordStatus!=1)
	$fields .= ",Description = '$Description'";
if($lnotice->RecordStatus!=1)		
	$fields .= ",DateStart = '$DateStart'";
$fields .= ",DateEnd = '$DateEnd'";
//$fields .= ",IssueUserID = '$UserID'";
//$fields .= ",Question = '$qStr'";
$fields .= ",Question = '$original_qStr'";
$fields .= ",RecordStatus = '$sus_status'";
$fields .= ",DebitMethod = '$debit_method'";
$fields .= ",DateModified = now()";
$fields .= ",AllFieldsReq = '$AllFieldsReq'";
$fields .= ",DisplayQuestionNumber = '$DisplayQuestionNumber'";
if($lnotice->RecordStatus != 1) {
    $fields .= ",isPaymentNoticeApplyEditor = '1'";
}

// [2016-1118-1145-57207] update Attachment field
if($updatedAttachmentField && !empty($Attachment))
	$fields .= ",Attachment = '$Attachment'";

#################################
#  ApprovedBy and ApprovedTime  #
#################################
//if organal status != 1
// => if new status == 1 => + ApprovedBy and ApprovedTime
if($lnotice->RecordStatus!=1){
	if($sus_status==1||$sus_status==5){
		$fields .= ",ApprovedBy = '$UserID'";
		$fields .= ",ApprovedTime = now()";
	}
}
#################################
#		Approval Comment		#
#################################
if($approvalComment!==''){
	$fields .= ",ApprovalComment = '".standardizeFormPostValue($approvalComment)."'";
}

$extraKeyValues = array();
$use_merchant_account = $lpayment->useEWalletMerchantAccount();

$multi_marchant_account_ids = array();

if ($use_merchant_account) {
	if($sys_custom['ePayment']['MultiPaymentGateway']) {
		$sql = "DELETE FROM INTRANET_NOTICE_PAYMENT_GATEWAY WHERE NoticeID='$NoticeID'";
		$Result['DeletePaymentGateway'] = $lnotice->db_db_query($sql);

		$last_MerchantAccountID_value = '';
		$service_provider_list = $lpayment->getPaymentServiceProviderMapping(false, true);
		foreach ($service_provider_list as $k => $temp) {
			$MerchantAccountID_value = ${'MerchantAccountID_' . $k};
			if ($MerchantAccountID_value == '') {
				continue;
			}
			$last_MerchantAccountID_value = $MerchantAccountID_value;
			$multi_marchant_account_ids[] = $MerchantAccountID_value;
			$sql = "INSERT INTO INTRANET_NOTICE_PAYMENT_GATEWAY
						(NoticeID, MerchantAccountID, DateInput, InputBy)
						VALUES
						($NoticeID, '$MerchantAccountID_value', now(), '" . $_SESSION['UserID'] . "')
						";
			$Result['CreatePaymentGateway' . $k] = $lnotice->db_db_query($sql);
		}

		$extraKeyValues = array('MerchantAccountID' => $last_MerchantAccountID_value);
		$fields .= ",MerchantAccountID=".(($last_MerchantAccountID_value == '') ? 'NULL' : "'$last_MerchantAccountID_value'")." ";
	} else {
		$extraKeyValues = array('MerchantAccountID' => $_POST['MerchantAccountID']);
		$fields .= ",MerchantAccountID='" . $_POST['MerchantAccountID'] . "' ";
	}
}

$sql = "UPDATE INTRANET_NOTICE SET $fields WHERE NoticeID = '$NoticeID'";
$lnotice->db_db_query($sql);

# Add PICs to DB
// remove current PICs
$sql = "DELETE FROM INTRANET_NOTICE_PIC WHERE NoticeID = '$NoticeID'";
$lnotice->db_db_query($sql);
// add PICs
if(isset($target_PIC) && sizeof($target_PIC) != 0)
{
	$fieldname = "NoticeID,PICUserID,InputBy,DateInput,ModifiedBy,DateModified";
	$values = "";
	
	$delim = "";
	$target_PIC = array_unique($target_PIC);
	for($i=0; $i<sizeof($target_PIC); $i++) {
		$this_PIC = $target_PIC[$i];
		$this_PIC = str_replace("&#160;", "", $this_PIC);
		$this_PIC = str_replace("U", "", $this_PIC);
		if(trim($this_PIC) != "") {
			$values .= "$delim ('$NoticeID','$this_PIC','$UserID',now(),'$UserID',now())";
			$delim = ",";
		}
	}
	
	$sql = "INSERT INTO INTRANET_NOTICE_PIC ($fieldname) VALUES $values";
	$lnotice->db_db_query($sql);
}

// Add more target students
if(isset($target) && sizeof($target)>0){
	// workaround the bug of missing prefix U for target users
	for($i=0;$i<count($target);$i++){
		if(substr($target[$i],0,1) != 'U'){
			$target[$i] = 'U'.$target[$i];
		}
	}
   	$targetID = implode(",",$target);
   
	$actual_target_users = $lnotice->returnTargetUserIDArray($targetID);
 	if(sizeof($actual_target_users)>0)
 	{
 		// exclude the already added students
 		$sql = "SELECT StudentID FROM INTRANET_NOTICE_REPLY WHERE NoticeID='$NoticeID'";
 		$existing_target_user_ids = $lnotice->returnVector($sql);
 		
		$delimiter = "";
		$values = "";
		for ($i=0; $i<sizeof($actual_target_users); $i++)
		{
		     list($uid,$name,$usertype) = $actual_target_users[$i];
		     if(!in_array($uid,$existing_target_user_ids)){
		     	$values .= "$delimiter ('$NoticeID','$uid','".$lnotice->Get_Safe_Sql_Query($name)."',0,now(),now())";
		 	 	$delimiter = ",";
		     }
		}
		if($values != ''){
			$sql = "INSERT IGNORE INTO INTRANET_NOTICE_REPLY (NoticeID,StudentID,StudentName,RecordStatus,DateInput,DateModified) VALUES $values";
			$lnotice->db_db_query($sql);
		}
 	}
}

if($lnotice->RecordType == 4) {
	if(isset($to_remove_target) && count($to_remove_target) > 0) {
		foreach($to_remove_target as $temp) {
			$sql = "DELETE FROM INTRANET_NOTICE_REPLY WHERE NoticeID='$NoticeID' AND StudentID='$temp' AND (RecordStatus IS NULL OR RecordStatus='0')";
			$lnotice->db_db_query($sql);
		}
	}
}



## Under development
$is_template = $sus_status == TEMPLATE_RECORD;

/*
$arrPaymentItemDetails = $lnotice->returnPaymentItemDetails($qStr);
$notice_array = $lnotice->splitQuestion($qStr);

if(sizeof($arrPaymentItemDetails)>0)
{
	foreach($arrPaymentItemDetails as $key=>$details)
	{
		$payment_type = $notice_array[$key-1][0];
		switch($payment_type)
		{
		    case 1:
		    case 12:
					$PaymentItemName = $details['PaymentItem'];
					$PaymentItemID = $details['PaymentItemID'];
					$PaymentCategroyID = $details['PaymentCategory'];
					$NonPayItem = $details['NonPayItem'];
					
					if($is_template) {
						$str_PaymentID = "#PAYMENTITEMID#1";
						$arrNewPaymentItemID[] = -1;
					} else if(!$NonPayItem) {
						//$sql = "SELECT Name FROM PAYMENT_PAYMENT_ITEM WHERE ItemID = '$PaymentItemID' AND CatID = '$PaymentCategroyID'";
						$sql = "SELECT Name FROM PAYMENT_PAYMENT_ITEM WHERE ItemID = '$PaymentItemID'";
						$ExistItemName = $lpayment->returnVector($sql);
						
						//if($ExistItemName[0] == $PaymentItemName)
						if(!empty($ExistItemName[0]) && $PaymentItemID != 1)
						{
							//debug("Case 1 - Edit<BR>");
							### Edit Payment Item ###
							$lpayment->Edit_Notice_Payment_Item($PaymentItemID, $PaymentItemName, $DateStart, $DateEnd, $PaymentCategroyID, $extraKeyValues);
							$payment_item_id = $PaymentItemID;
						}
						else
						{
							//debug("Case 1 - New<BR>");
							### Create Payment Item ###
							$payment_item_id = $lpayment->Create_Notice_Payment_Item($PaymentItemName, $DateStart, $DateEnd, $NoticeID, $PaymentCategroyID, $extraKeyValues);
						}
						$arrNewPaymentItemID[] = $payment_item_id;			// store the new payment item id
						
						$delim_itemid = "#PAYMENTITEMID#";
						$str_PaymentID = $delim_itemid.$payment_item_id;
					} else {
						$str_PaymentID = "#PAYMENTITEMID#1";	
					}
					
					### In here , replace the #PAYMENTITEMID# in the original $qStr, then update the record in INTRANET_NOTICE 
					$qSeparator = "#QUE#";
					$strDetails = explode($qSeparator,$qStr);
					//$temp_str = str_replace("#PAYMENTITEMID#1||",$str_PaymentID."||",$strDetails[$key]);
					if(strpos($strDetails[$key],"#PAYMENTITEMID#1||")!==false) {       // New question
						$temp_str = str_replace("#PAYMENTITEMID#1",$str_PaymentID,$strDetails[$key]);
					} else {       // Template question with PaymentItemID
						$temp_str = str_replace("#PAYMENTITEMID#$PaymentItemID||",$str_PaymentID."||",$strDetails[$key]);
					}
					$temp_str = $qSeparator.$temp_str;
					$arrFinalStr[] = $temp_str;
					break;
			case 2:
					$PaymentItemName = $details['PaymentItem'];
					$PaymentItemID = $details['PaymentItemID'];
					$PaymentCategroyID = $details['PaymentCategory'];
					$NonPayItem = $details['NonPayItem'];	
					
					if($is_template){
						$str_PaymentID = "#PAYMENTITEMID#1";
						$arrNewPaymentItemID[] = -1;
					}
					else if(!$NonPayItem) {
						//$sql = "SELECT Name FROM PAYMENT_PAYMENT_ITEM WHERE ItemID = '$PaymentItemID' AND CatID = '$PaymentCategroyID'";
						$sql = "SELECT Name FROM PAYMENT_PAYMENT_ITEM WHERE ItemID = '$PaymentItemID'";
						$ExistItemName = $lpayment->returnVector($sql);
						
						//if($ExistItemName[0] == $PaymentItemName)
						if(!empty($ExistItemName[0]) && $PaymentItemID != 1)
						{
							//debug("Case 2 - Edit<BR>");
							### Edit Payment Item ###
							$lpayment->Edit_Notice_Payment_Item($PaymentItemID,$PaymentItemName, $DateStart, $DateEnd, $PaymentCategroyID, $extraKeyValues);
							$payment_item_id = $PaymentItemID;
						}
						else
						{
							//debug("Case 2 - Edit<BR>");
							### Create Payment Item ###
							$payment_item_id = $lpayment->Create_Notice_Payment_Item($PaymentItemName, $DateStart, $DateEnd, $NoticeID, $PaymentCategroyID, $extraKeyValues);
						}
						$arrNewPaymentItemID[] = $payment_item_id;			// store the new payment item id
						
						$delim_itemid = "#PAYMENTITEMID#";
						$str_PaymentID = $delim_itemid.$payment_item_id;
					}
					else {
						$str_PaymentID = "#PAYMENTITEMID#1";	
					}
					
					### In here , replace the #PAYMENTITEMID# in the original $qStr, then update the record in INTRANET_NOTICE 
					$qSeparator = "#QUE#";
					$strDetails = explode($qSeparator,$qStr);
					if(strpos($strDetails[$key],"#PAYMENTITEMID#1||")!==false){ // New question
						$temp_str = str_replace("#PAYMENTITEMID#1",$str_PaymentID,$strDetails[$key]);
					}
					else{ // Template question PaymentItemID
						//$temp_str = str_replace("#PAYMENTITEMID#1||",$str_PaymentID."||",$strDetails[$key]);
						$temp_str = str_replace("#PAYMENTITEMID#$PaymentItemID||",$str_PaymentID."||",$strDetails[$key]);
					}
					$temp_str = $qSeparator.$temp_str;
					$arrFinalStr[] = $temp_str;
					break;
			case 3:
					$PaymentItemNameArray = $details['PaymentItem'];
					$PaymentItemIDArray = $details['PaymentItemID'];
					$PaymentCategroyIDArray = $details['PaymentCategory'];
					
					if($is_template){
						$strPaymentItemID = "#PAYMENTITEMID#1";
						$arrNewPaymentItemID[] = -1;
					}
					else{
						for($i=0; $i<sizeof($PaymentItemNameArray); $i++){
							$PaymentItemName = $PaymentItemNameArray[$i];
							$PaymentItemID = $PaymentItemIDArray[$i];
							$PaymentCategroyID = $PaymentCategroyIDArray[$i];
							
							//$sql = "SELECT Name FROM PAYMENT_PAYMENT_ITEM WHERE ItemID = '$PaymentItemID' AND CatID = '$PaymentCategroyID'";
							$sql = "SELECT Name FROM PAYMENT_PAYMENT_ITEM WHERE ItemID = '$PaymentItemID'";
							$ExistItemName = $lpayment->returnVector($sql);
							
							$CurrentItemIDArray = explode(",",$CurrentItemID);
							//if($ExistItemName[0] == $PaymentItemName || ($PaymentItemID!='' && in_array($PaymentItemID,$CurrentItemIDArray)))
							if((!empty($ExistItemName[0]) || ($PaymentItemID != '' && in_array($PaymentItemID, $CurrentItemIDArray))) && $PaymentItemID != 1)
							{
								//debug("Case 3 - Edit<BR>");
								### Edit Payment Item ###
								$lpayment->Edit_Notice_Payment_Item($PaymentItemID,$PaymentItemName, $DateStart, $DateEnd, $PaymentCategroyID, $extraKeyValues);
								$arr_payment_item_id[$key][] = $PaymentItemID;
							}
							else
							{
								//debug("Case 3 - New<BR>");
								### Create Payment Item ###
								$arr_payment_item_id[$key][] = $lpayment->Create_Notice_Payment_Item($PaymentItemName, $DateStart, $DateEnd, $NoticeID, $PaymentCategroyID, $extraKeyValues);
							}
						}
						foreach($arr_payment_item_id[$key] as $temp_key=>$val){
							$arrNewPaymentItemID[] = $val;
						}
						
						//$strPaymentItemID = implode("#PAYMENTITEMID#",$arr_payment_item_id[$key]);
						$strPaymentItemID = implode(",",$arr_payment_item_id[$key]);
						$strPaymentItemID = "#PAYMENTITEMID#".$strPaymentItemID;
					}
					
					### In here , replace the #PAYMENTITEMID# in the original $qStr, then update the record in INTRANET_NOTICE 
					$qSeparator = "#QUE#";
					$strDetails = explode($qSeparator,$qStr);
					if(strpos($strDetails[$key],"#PAYMENTITEMID#1||")!==false){ // New question
						$temp_str = str_replace("#PAYMENTITEMID#1",$strPaymentItemID,$strDetails[$key]);
					}
					else if(strpos($strDetails[$key],"#PAYMENTITEMID#".implode("#PAYMENTITEMID#",$PaymentItemIDArray)."||")!==false){
						$temp_str = str_replace("#PAYMENTITEMID#".implode("#PAYMENTITEMID#",$PaymentItemIDArray)."||",$strPaymentItemID."||",$strDetails[$key]);
					}
					else{ // Template question with PaymentItemID
						$temp_str = str_replace("#PAYMENTITEMID#".implode(",",$PaymentItemIDArray)."||",$strPaymentItemID."||",$strDetails[$key]);
					}
					$temp_str = $qSeparator.$temp_str;
					$arrFinalStr[] = $temp_str;
					
					break;

			case 9:
					$PaymentItemNameArray = $details['PaymentItem'];
					$PaymentItemIDArray = $details['PaymentItemID'];
					$PaymentCategroyIDArray = $details['PaymentCategory'];
					
					if($is_template){
						$strPaymentItemID = "#PAYMENTITEMID#1";
						$arrNewPaymentItemID[] = -1;
					}
					else{
						for($i=0; $i<sizeof($PaymentItemNameArray); $i++){
							$PaymentItemName = $PaymentItemNameArray[$i];
							$PaymentItemID = $PaymentItemIDArray[$i];
							$PaymentCategroyID = $PaymentCategroyIDArray[$i];
							
							//$sql = "SELECT Name FROM PAYMENT_PAYMENT_ITEM WHERE ItemID = '$PaymentItemID' AND CatID = '$PaymentCategroyID'";
							$sql = "SELECT Name FROM PAYMENT_PAYMENT_ITEM WHERE ItemID = '$PaymentItemID'";
							$ExistItemName = $lpayment->returnVector($sql);
							
							$CurrentItemIDArray = explode(",",$CurrentItemID);
							//if($ExistItemName[0] == $PaymentItemName || ($PaymentItemID!='' && in_array($PaymentItemID,$CurrentItemIDArray)))
							if((!empty($ExistItemName[0]) || ($PaymentItemID != '' && in_array($PaymentItemID, $CurrentItemIDArray))) && $PaymentItemID != 1)
							{
								//debug("Case 9 - Edit<BR>");
								### Edit Payment Item ###
								$lpayment->Edit_Notice_Payment_Item($PaymentItemID,$PaymentItemName, $DateStart, $DateEnd, $PaymentCategroyID, $extraKeyValues);
								$arr_payment_item_id[$key][] = $PaymentItemID;
							}
							else
							{
								//debug("Case 9 - New<BR>");
								### Create Payment Item ###
								$arr_payment_item_id[$key][] = $lpayment->Create_Notice_Payment_Item($PaymentItemName, $DateStart, $DateEnd, $NoticeID, $PaymentCategroyID, $extraKeyValues);
							}
						}
						foreach($arr_payment_item_id[$key] as $temp_key=>$val){
							$arrNewPaymentItemID[] = $val;
						}
						
						$strPaymentItemID = implode("#PAYMENTITEMID#",$arr_payment_item_id[$key]);
						$strPaymentItemID = "#PAYMENTITEMID#".$strPaymentItemID;
					}
					### In here , replace the #PAYMENTITEMID# in the original $qStr, then update the record in INTRANET_NOTICE 
					$qSeparator = "#QUE#";
					$strDetails = explode($qSeparator,$qStr);
					
					if(strpos($strDetails[$key],"#PAYMENTITEMID#1||")!==false){
						$temp_str = str_replace("#PAYMENTITEMID#1||",$strPaymentItemID."||",$strDetails[$key]);
					}
					else if(strpos($strDetails[$key],"#PAYMENTITEMID#".implode(",",$PaymentItemIDArray)."||")!==false){
						$temp_str = str_replace("#PAYMENTITEMID#".implode(",",$PaymentItemIDArray)."||",$strPaymentItemID."||",$strDetails[$key]);
					}
					else{
						$temp_str = str_replace("#PAYMENTITEMID#".implode("#PAYMENTITEMID#",$PaymentItemIDArray)."||",$strPaymentItemID."||",$strDetails[$key]);
					}
					
					$temp_str = $qSeparator.$temp_str;
					$arrFinalStr[] = $temp_str;
					break;
			case 10:
					$qSeparator = "#QUE#";
					$arrFinalStr[] = $qSeparator.$details['ItemName'];
					break;
		}
	}
	$strNewPaymentItemID = implode(",",$arrNewPaymentItemID);
	$sql = "DELETE FROM PAYMENT_PAYMENT_ITEM WHERE ItemID Not In ($strNewPaymentItemID) AND NoticeID = '$NoticeID'";
	$lnotice->db_db_query($sql);
	
	if(sizeof($arrFinalStr)>0){
		$final_str = implode("",$arrFinalStr);
		$sql = "UPDATE INTRANET_NOTICE SET Question = '".$lnotice->Get_Safe_Sql_Query($final_str)."' WHERE NoticeID = '$NoticeID'";
		$lnotice->db_db_query($sql);
	}
}
*/

$arrSignedCount = $lnotice->returnNoticeSignedCount($NoticeID);
$SignedCount = $arrSignedCount[$NoticeID][0];
$can_edit_replyslip = $lnotice->RecordStatus!=1 && ($SignedCount == 0 || $SignedCount == "");
// can modify replyslip if no one signed and notice status is not active/distributed
$payment_ids_array = array();
if($can_edit_replyslip)
{
	$old_question_string = $lnotice->Question;
	$old_payment_item_ids = $eNoticePaymentReplySlip->getAllPaymentItemID($old_question_string); // get existing payment item ids from old question
	
	$questions = $eNoticePaymentReplySlip->parseQuestionString($qStr);
	if(!$is_template){
		// create payment items if not created yet
		$questions = $eNoticePaymentReplySlip->preparePaymentItemsForQuestions($questions, $NoticeID, $DateStart, $DateEnd, $extraKeyValues);
		if ($use_merchant_account) {
			if ($sys_custom['ePayment']['MultiPaymentGateway']) {
				$sql_values = array();
				$payment_ids_array = array();
				foreach ($questions as $temp) {
					if(isset($temp['Options']) && is_array($temp['Options'])) {
						foreach($temp['Options'] as $option_temp) {
							if(isset($option_temp['PaymentItemID']) && $option_temp['PaymentItemID'] != '') {
								$payment_ids_array[] = $option_temp['PaymentItemID'];
							}
						}
					} else if (isset($temp['PaymentItemID']) && $temp['PaymentItemID'] != '') {
						$payment_ids_array[] = $temp['PaymentItemID'];
					}
				}

				$payment_ids_array = array_unique($payment_ids_array);
			}
		}
	}else{
		// clear the payment items for template
		$questions = $eNoticePaymentReplySlip->removePaymentItemsForQuestion($questions);
	}
	$final_qstr = $eNoticePaymentReplySlip->constructQuestionString($questions);
	$new_payment_item_ids = $eNoticePaymentReplySlip->getAllPaymentItemID($final_qstr); // get all payment item ids from the new question
	$to_remove_payment_item_ids = array_diff($old_payment_item_ids,$new_payment_item_ids); // remove the old payment item ids that not keep in new payment item ids
	if(count($to_remove_payment_item_ids)>0){
		for($i=0;$i<count($to_remove_payment_item_ids);$i++){
			if($to_remove_payment_item_ids[$i] > 0){
				$lpayment->Remove_Notice_Payment_Item($to_remove_payment_item_ids[$i]);
			}
		}
	}
	
	$sql = "UPDATE INTRANET_NOTICE SET Question = '".$lnotice->Get_Safe_Sql_Query($final_qstr)."' WHERE NoticeID = '$NoticeID'";
	$lnotice->db_db_query($sql);
} else {
	if (!$is_template && ($SignedCount == 0 || $SignedCount == "")) {
		$more_values = array();
		if (count($extraKeyValues) > 0) {
			foreach ($extraKeyValues as $key => $val) {
				$more_values[] = $key . '=\'' . $lpayment->Get_Safe_Sql_Query($val) . '\'';
			}
			$sql = "SELECT ItemID FROM PAYMENT_PAYMENT_ITEM WHERE NoticeID = '$NoticeID'";
			$temp = $lpayment->returnVector($sql);
			foreach ($temp as $temp_itemid) {
				$payment_ids_array[] = $temp_itemid;
				$sql = "UPDATE PAYMENT_PAYMENT_ITEM SET " . implode(",", $more_values) . " WHERE ItemID = $temp_itemid";
				$lpayment->db_db_query($sql);
			}
			$payment_ids_array = array_unique($payment_ids_array);
		}
	}
}

if ($use_merchant_account) {
	if ($sys_custom['ePayment']['MultiPaymentGateway']) {
		if (count($payment_ids_array) > 0) {
			foreach ($payment_ids_array as $temp) {
				$sql = "DELETE FROM PAYMENT_PAYMENT_ITEM_PAYMENT_GATEWAY WHERE ItemID='" . $temp . "'";
				$Result['DeleteItemPaymentGateway_' . $temp] = $lnotice->db_db_query($sql);
				foreach ($multi_marchant_account_ids as $MerchantAccountID_value) {
					$sql_values[] = "('" . $temp . "', '$MerchantAccountID_value', now(), '" . $_SESSION['UserID'] . "')";
				}
			}
			if (count($sql_values) > 0) {
				$sql = "INSERT INTO PAYMENT_PAYMENT_ITEM_PAYMENT_GATEWAY
								(ItemID, MerchantAccountID, DateInput, InputBy)
								VALUES " . implode(" , ", $sql_values);
				$Result['CreateItemPaymentGateway'] = $lnotice->db_db_query($sql);
			}
		}
	}
}

# Send Notification
//if ($sus_status==1 && (($emailnotify==1) || ($emailnotify_Students==1) || ($pushmessagenotify == 1)))
if ($sus_status == 1)
{
	if ($emailnotify==1 || $emailnotify_Students==1 || $emailnotify_PIC == 1 || $emailnotify_ClassTeachers == 1 || $pushmessagenotify == 1 || $pushmessagenotify_Students == 1 || $pushmessagenotify_PIC == 1 || $pushmessagenotify_ClassTeachers == 1)
    {
        $lwebmail = new libwebmail();
    	
        $sql = "SELECT DISTINCT StudentID FROM INTRANET_NOTICE_REPLY WHERE NoticeID = '$NoticeID'";
        $students = $lnotice->returnVector($sql);
        $student_list = implode(",", (array)$students);
        
        $sql = "SELECT DISTINCT b.UserID
                      FROM INTRANET_PARENTRELATION as a
                           LEFT OUTER JOIN INTRANET_USER as b ON a.ParentID = b.UserID
                      WHERE a.StudentID IN ($student_list) ";
        $parent = $lnotice->returnVector($sql);
    }
    
    if(($emailnotify==1) && ($emailnotify_Students==1)) {
    	$ToArray = array_merge($students,$parent);
    } else if($emailnotify==1) {
    	$ToArray = $parent;  
    } else {
    	$ToArray = $students;
    }
    
    if($emailnotify_PIC == 1 && sizeof($target_PIC) != 0) {
    	$PICs = array();
    	$target_PIC = array_unique($target_PIC);
    	foreach((array)$target_PIC as $tPICID) {
    		$tPICid = $tPICID;
    		$tPICid = str_replace("&#160;", "", $tPICid);
    		$tPICid = str_replace("U", "", $tPICid);
    		
    		$PICs[] = $tPICid;
    	}
    	$ToArray = array_merge($ToArray, $PICs);
    }
    
    if($emailnotify_ClassTeachers == 1) {
    	require_once ($PATH_WRT_ROOT."includes/libclass.php");
    	$libclass = new libclass();
    	
    	$teacherArr = array();
    	foreach ((array)$students as $student) {
    		$class = $libclass->returnCurrentClass($student);
    		$teachers = $libclass->returnClassTeacherID($class[0]['ClassName']);
    		foreach ((array)$teachers as $teacher) {
    			array_push($teacherArr, $teacher['UserID']);
    		}
    	}
    	$teacherArr = array_unique($teacherArr);
    	
    	//debug_pr($teacherArr);
    	$ToArray = array_merge($ToArray, $teacherArr);
    }
    
    # Send email notification
    if ($emailnotify==1 || ($emailnotify_Students==1) || ($emailnotify_PIC == 1) || ($emailnotify_ClassTeachers == 1))
    {
		list($email_subject, $email_body) = $lnotice->returnEmailNotificationData($DateStart,$DateEnd,$pushMsgTitle);    
	    $lwebmail->sendModuleMail($ToArray,$email_subject,$email_body,1,'','User',true);
    }
    
    # Send Push Message
    if ($pushmessagenotify == 1 || $pushmessagenotify_Students == 1 || $pushmessagenotify_PIC == 1 || $pushmessagenotify_ClassTeachers == 1)
	{
		$isPublic = "N";
		$appType = $eclassAppConfig['appType']['Parent'];
		$appType_s = $eclassAppConfig['appType']['Student'];
		$appType_t = $eclassAppConfig['appType']['Teacher'];
// 		$sendTimeMode = "now";
// 		$sendTimeString = "";
		if($sendTimeMode=='now'){
			$sendTimeString = "";
		}
		else{
		    $sendTimeMode = standardizeFormPostValue($_POST['sendTimeMode']);
		    $sendTimeString = standardizeFormPostValue($_POST['sendTimeString']);
		}
		//$libeClassApp->overrideExistingScheduledPushMessageFromModule("eNotice", $NoticeID, $newNotifyMessageId='');

		//Logic if sendTimeMode == 'now',
		//	check is issue day is in future
		//		-> yes:  sendTimeMode -> 'scheduled' and sendTimeString -> issueDate
		
// 		$now = date('Y-m-d h:i:s');
// 		if($sendTimeMode=='now'){
// 			if(strtotime($now)<strtotime($DateStart)){
// 				$sendTimeMode = 'scheduled';
// 				$sendTimeString = $DateStart;
// 			}
// 		}
		
		$i = 0;
		list($pushmessage_subject, $pushmessage_body) = $lnotice->returnPushMessageData($DateStart,$DateEnd,$pushMsgTitle,$pushMsgNoticeNumber);
		
		$individualMessageInfoAry = array();
//     	if (!empty($students)) {
//             // [2019-0917-1522-01206] skip student status checking
// 			//$parentStudentAssoAry = BuildMultiKeyAssoc($luser->getParentStudentMappingInfo($students), 'ParentID', $IncludedDBField=array('StudentID'), $SingleValue=1, $BuildNumericArray=1);
//             $parentStudentAssoAry = BuildMultiKeyAssoc($luser->getParentStudentMappingInfo($students, '', $skipStuStatusCheck=true), 'ParentID', $IncludedDBField=array('StudentID'), $SingleValue=1, $BuildNumericArray=1);
// 		}
// 		else {
// 			$parentStudentAssoAry = array();
// 		}
//     	$individualMessageInfoAry[0]['relatedUserIdAssoAry'] = $parentStudentAssoAry;
    	
//     	// always remove the old scheduled message first
//     	$libeClassApp->overrideExistingScheduledPushMessageFromModule("eNotice", $NoticeID, $newNotifyMessageId='', $appType);
		
// 		//$notifyMessageId = $libeClassApp->sendPushMessage($individualMessageInfoAry, $pushmessage_subject, $pushmessage_body, $isPublic, $recordStatus=1, $appType, $sendTimeMode, $sendTimeString, '', '', 'eNotice', $NoticeID);
// 		if ($libeClassApp->isEnabledSendBulkPushMessageInBatches()) {
// 			$notifyMessageId = $libeClassApp->sendPushMessageByBatch($individualMessageInfoAry, $pushmessage_subject, $pushmessage_body, $isPublic, $recordStatus=1, $appType, $sendTimeMode, $sendTimeString, '', '', 'eNotice', $NoticeID);
// 	    }
// 	    else {
// 	    	$notifyMessageId = $libeClassApp->sendPushMessage($individualMessageInfoAry, $pushmessage_subject, $pushmessage_body, $isPublic, $recordStatus=1, $appType, $sendTimeMode, $sendTimeString, '', '', 'eNotice', $NoticeID);
// 	    }
		if($pushmessagenotify==1)
		{
			if (!empty($students)) {
				// [2019-0917-1522-01206] skip student status checking
				//$parentStudentAssoAry = BuildMultiKeyAssoc($luser->getParentStudentMappingInfo($students), 'ParentID', $IncludedDBField=array('StudentID'), $SingleValue=1, $BuildNumericArray=1);
				$parentStudentAssoAry = BuildMultiKeyAssoc($luser->getParentStudentMappingInfo($students, '', $skipStuStatusCheck=true), 'ParentID', $IncludedDBField=array('StudentID'), $SingleValue=1, $BuildNumericArray=1);
			} else {
				$parentStudentAssoAry = array();
			}
			$individualMessageInfoAry = array();
			$individualMessageInfoAry[0]['relatedUserIdAssoAry'] = $parentStudentAssoAry;
			
			// always delete the old record to override the new one
			$libeClassApp->overrideExistingScheduledPushMessageFromModule("eNotice", $NoticeID, $newNotifyMessageId='',$appType);
			
			if ($libeClassApp->isEnabledSendBulkPushMessageInBatches()) {
				$notifyMessageId = $libeClassApp->sendPushMessageByBatch($individualMessageInfoAry, $pushmessage_subject, $pushmessage_body, $isPublic, $recordStatus=1, $appType, $sendTimeMode, $sendTimeString, '', '', 'eNotice', $NoticeID);
			} else {
				$notifyMessageId = $libeClassApp->sendPushMessage($individualMessageInfoAry, $pushmessage_subject, $pushmessage_body, $isPublic, $recordStatus=1, $appType, $sendTimeMode, $sendTimeString, '', '', 'eNotice', $NoticeID);
			}
		}
		else
		{
			// remove the schedule send msg to parent is not selected
			$libeClassApp->overrideExistingScheduledPushMessageFromModule("eNotice", $NoticeID, $newNotifyMessageId='',$appType);
		}
		
		if($pushmessagenotify_Students==1)
		{
			$StudentAssoAry = array();
			foreach ((array)$students as $studentId) {
				$_targetStudentId = $libeClassApp->getDemoSiteUserId($studentId);
				// link the message to be related to oneself
				$StudentAssoAry[$studentId] = array($_targetStudentId);
			}
			$individualMessageInfoAry = array();
			$individualMessageInfoAry[0]['relatedUserIdAssoAry'] = $StudentAssoAry;
			
			// always delete the old record to override the new one
			$libeClassApp->overrideExistingScheduledPushMessageFromModule("eNotice", $NoticeID, $newNotifyMessageId='',$appType_s);
			
			if ($libeClassApp->isEnabledSendBulkPushMessageInBatches()) {
				$notifyMessageId = $libeClassApp->sendPushMessageByBatch($individualMessageInfoAry, $pushmessage_subject, $pushmessage_body, $isPublic, $recordStatus=1, $appType_s, $sendTimeMode, $sendTimeString, '', '', 'eNotice', $NoticeID);
			} else {
				$notifyMessageId = $libeClassApp->sendPushMessage($individualMessageInfoAry, $pushmessage_subject, $pushmessage_body, $isPublic, $recordStatus=1, $appType_s, $sendTimeMode, $sendTimeString, '', '', 'eNotice', $NoticeID);
			}
		}
		else
		{
			$libeClassApp->overrideExistingScheduledPushMessageFromModule("eNotice", $NoticeID, $newNotifyMessageId='',$appType_s);
		}
		
		// [2018-1008-1030-09073] Handle both PIC and Class Teachers - push msg notification
		if($pushmessagenotify_PIC == 1 || $pushmessagenotify_ClassTeachers == 1)
		{
			$teacherAssoAry = array();
			if($pushmessagenotify_PIC == 1)
			{
				if(isset($target_PIC) && sizeof($target_PIC) != 0) {
					$target_PIC = array_unique($target_PIC);
					foreach((array)$target_PIC as $tPICID) {
						$tPICid = $tPICID;
						$tPICid = str_replace("&#160;", "", $tPICid);
						$tPICid = str_replace("U", "", $tPICid);
						
						//$PICAssoAry[$tPICid] = array($tPICid);
						$teacherAssoAry[$tPICid] = array($tPICid);
					}
				}
			}
			if($pushmessagenotify_ClassTeachers == 1)
			{
				require_once ($PATH_WRT_ROOT."includes/libclass.php");
				$libclass = new libclass();
				
				$teacherArr = array();
				foreach ((array)$students as $student) {
					$class = $libclass->returnCurrentClass($student);
					$teachers = $libclass->returnClassTeacherID($class[0]['ClassName']);
					foreach ((array)$teachers as $teacher) {
						array_push($teacherArr, $teacher['UserID']);
					}
				}
				$teacherArr = array_unique($teacherArr);
				foreach ((array)$teacherArr as $tid) {
					$teacherAssoAry[$tid] = array($tid);
				}
			}
			$individualMessageInfoAry = array();
			$individualMessageInfoAry[0]['relatedUserIdAssoAry'] = $teacherAssoAry;
			
			// always delete the old record to override the new one
			$libeClassApp->overrideExistingScheduledPushMessageFromModule("eNoticeS", $NoticeID, $newNotifyMessageId='',$appType_t);
			
			if ($libeClassApp->isEnabledSendBulkPushMessageInBatches()) {
				//$notifyMessageId = $libeClassApp->sendPushMessageByBatch($individualMessageInfoAry, $pushmessage_subject, $pushmessage_body, $isPublic, $recordStatus=1, $appType_t, $sendTimeMode, $sendTimeString, '', '', 'eNotice', $NoticeID);
				$notifyMessageId = $libeClassApp->sendPushMessageByBatch($individualMessageInfoAry, $pushmessage_subject, $pushmessage_body, $isPublic, $recordStatus=1, $appType_t, $sendTimeMode, $sendTimeString, '', '', 'eNoticeS', $NoticeID);
			} else {
				//$notifyMessageId = $libeClassApp->sendPushMessage($individualMessageInfoAry, $pushmessage_subject, $pushmessage_body, $isPublic, $recordStatus=1, $appType_t, $sendTimeMode, $sendTimeString, '', '', 'eNotice', $NoticeID);
				$notifyMessageId = $libeClassApp->sendPushMessage($individualMessageInfoAry, $pushmessage_subject, $pushmessage_body, $isPublic, $recordStatus=1, $appType_t, $sendTimeMode, $sendTimeString, '', '', 'eNoticeS', $NoticeID);
			}
		}
		else
		{
			$libeClassApp->overrideExistingScheduledPushMessageFromModule("eNoticeS", $NoticeID, $newNotifyMessageId='',$appType_t);
		}
	}
	else
	{
	    // remove the scheduled message if not selected
// 	    $appType = $eclassAppConfig['appType']['Parent'];
// 	    $libeClassApp->overrideExistingScheduledPushMessageFromModule("eNotice", $NoticeID, $newNotifyMessageId='', $appType);
		$libeClassApp->overrideExistingScheduledPushMessageFromModule("eNotice", $NoticeID, $newNotifyMessageId='');
		$libeClassApp->overrideExistingScheduledPushMessageFromModule("eNoticeS", $NoticeID, $newNotifyMessageId='');
	}
}
else if($sus_status == 4)
{
    // save the choices -> need approval only
	$sql = "UPDATE INTRANET_NOTICE
			SET pushmessagenotify = '$pushmessagenotify',
				pushmessagenotify_Students = '$pushmessagenotify_Students',
				pushmessagenotify_PIC = '$pushmessagenotify_PIC',
				pushmessagenotify_ClassTeachers = '$pushmessagenotify_ClassTeachers',
				pushmessagenotifyMode = '$sendTimeMode',
				pushmessagenotifyTime = '$sendTimeString',
				emailnotify = '$emailnotify',
				emailnotify_Students = '$emailnotify_Students',
				emailnotify_PIC = '$emailnotify_PIC',
				emailnotify_ClassTeachers = '$emailnotify_ClassTeachers'
			WHERE NoticeID = $NoticeID ";
	$lnotice->db_db_query($sql);		
}
else if($sus_status == 2 || $sus_status == 3)
{
    // suspend or set as template => remove all scheduled push message
    $libeClassApp->overrideExistingScheduledPushMessageFromModule("eNotice", $NoticeID, $newNotifyMessageId='');
}

// Send approval notice
if($lnotice->RecordStatus>=4 && $sus_status==1) {
	$lnotice->sendApprovalNotice();
}
else if ($sus_status==5) {
	$lnotice->sendApprovalNotice(false,$approvalComment);
}

// for PHP 5.4, replace session_unregister()
//session_unregister("noticeAttFolder");
session_unregister_intranet("noticeAttFolder");

intranet_closedb();

if($backUrl=='paymentNotice') {
	$location = "paymentNotice.php";
} else {
	$location = "index.php";
}

if($sus_status >= 4) {
	$additional_location = "&pendingApproval=1";
}	

//header("Location: $location?xmsg=update&pageNo=$pageNo&field=$field&page_size_change=$page_size_change&numPerPage=$numPerPage&noticeType=$noticeType&status=$status&year=$year&month=$month");
header("Location: $location?xmsg=UpdateSuccess&pageNo=$pageNo&field=$field&page_size_change=$page_size_change&numPerPage=$numPerPage&noticeType=$noticeType&status=$sus_status&year=$year&month=$month".$additional_location);
?>