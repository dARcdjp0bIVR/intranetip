<?php
//USING : 

### Change Log [Start] ###
/*
*
*  	Date    :   2020-09-07 (Bill)    [2020-0604-1821-16170]
* 	Detail	:   access right checking > replace staffview by payment_staffview
*
*   Date    :   2019-12-10 (Ray)
*	Detail	:   update no.of student need to pay number don't count not sign reply
*
* 	Date	:	2016-04-07 (Bill)
* 	Detail	:	allow user to access if All staff can view reply contents. = true
* 
*	Date	:	2015-04-14 (Bill)
*	Detail	:	allow eNotice PIC to access [2015-0323-1602-46073]
*  
*	Date	:	2010-07-09 (Henry Chow)
*	Detail	:	handle the case of null resultset, display "-" in the table
*/
### Change Log [End] ###

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");

intranet_auth();
intranet_opendb();

$lnotice = new libnotice($NoticeID);
$lpayment = new libpayment();

$AcademicYearID = Get_Current_Academic_Year_ID();

$lu = new libuser($UserID);
if (!$lu->isTeacherStaff())
{
     header("Location: /close.php");
     exit();
}

// [2015-0323-1602-46073] allow eNotice PIC to access
// [2016-04-07 updated] - allow user to access if All staff can view reply contents. = true
//if (!$lnotice->hasViewRight() && !$lnotice->isNoticePIC($NoticeID))
//if (!$lnotice->hasViewRight() && !$lnotice->staffview && !$lnotice->isNoticePIC($NoticeID))
if (!$lnotice->hasViewRight() && !$lnotice->payment_staffview && !$lnotice->isNoticePIC($NoticeID))
{
     header("Location: /close.php");
     exit();
}

if ($lnotice->RecordType == 4)
{
    header("Location: /home/eService/notice/tableview.php?NoticeID=$NoticeID");
    exit();
}

$MODULE_OBJ['title'] = $i_Notice_ResultForEachClass;
$linterface = new interface_html("popup.html");
$linterface->LAYOUT_START();

$array_total = $lnotice->returnTotalCountByClass();
$array_signed = $lnotice->returnSignedCountByClass();
$all_total = 0;
$all_signed = 0;
$classes = $lnotice->returnClassList();

$x .= "<table width='90%' border='0' cellspacing='0' cellpadding='4'>\n";
$x .= "<tr>";
$x .= "<td class='tablebluetop tabletopnolink'>$i_ClassName</td>";
$x .= "<td class='tablebluetop tabletopnolink' width='80'>$i_Notice_Signed</td>";
$x .= "<td class='tablebluetop tabletopnolink' width='80'>$i_Notice_Total</td>";
if(strtoupper($lnotice->Module) == "PAYMENT")
{
	$x .= "<td class='tablebluetop tabletopnolink' width='80'>".$Lang['eNotice']['FieldTitle']['NoOfStudentNeedToPaid']."</td>";
	$x .= "<td class='tablebluetop tabletopnolink' width='80'>".$Lang['eNotice']['FieldTitle']['NoOfStudentPaidSuccessfully']."</td>";
}
$x .= "</tr>\n";

for ($i=0; $i<sizeof($classes); $i++)
{
	$cnt = 0;
     $name = $classes[$i];
     ## check $class <>""
     if(!$name) continue;
     $signed = $array_signed[$name]+0;
     $total = $array_total[$name]+0;
     $displayName = ($name==""? "--":"$name");
     $link = "<a class='tablebluelink' href='/home/eService/notice/tableview.php?NoticeID=$NoticeID&class=".urlencode($name)."'>$displayName</a>";
     $x .= "<tr class='tablebluerow". ( $i%2 + 1 )."'>\n";
     $x .= "<td class='tabletext'>$link</td>";
     $x .= "<td class='tabletext' width='80'>$signed</td>";
     $x .= "<td class='tabletext' width='80'>$total</td>";
     
     if(strtoupper($lnotice->Module) == "PAYMENT")
     {
	     $sql = "SELECT b.UserID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_USER AS b ON (a.YearClassID = b.YearClassID) WHERE a.ClassTitleEN = '$name' AND a.AcademicYearID = '$AcademicYearID'";
	     $arrUserIDs = $lnotice->returnVector($sql);

	     $sql = "SELECT ItemID FROM PAYMENT_PAYMENT_ITEM WHERE NoticeID = '$NoticeID'";
	     $arrItemIDs = $lnotice->returnVector($sql);
	     
	     if(sizeof($arrUserIDs)>0){
	     	$targetUserID = implode(",",$arrUserIDs);
	     }
	     if(sizeof($arrItemIDs)>0){
	     	$targetItemID = implode(",",$arrItemIDs);
	     }
	     //$sql = "SELECT StudentID, COUNT(*) as TotalItemNeedToPaid FROM PAYMENT_PAYMENT_ITEMSTUDENT WHERE StudentID IN ($targetUserID) AND ItemID IN ($targetItemID) GROUP BY StudentID";
	     $sql = "SELECT a.StudentID, COUNT(*) as TotalItemNeedToPaid FROM PAYMENT_PAYMENT_ITEMSTUDENT a
				LEFT JOIN INTRANET_NOTICE_REPLY b ON a.StudentID=b.StudentID AND b.NoticeID='$NoticeID'
				WHERE a.StudentID IN ($targetUserID)
				AND ItemID IN ($targetItemID) 
				AND b.RecordStatus!=0
				GROUP BY a.StudentID";

	     $arrTotalNoOfStudentNeedToPaid = $lnotice->returnArray($sql,2);
	     
	     if(sizeof($arrTotalNoOfStudentNeedToPaid)>0)
	     {
	     	for($j=0; $j<sizeof($arrTotalNoOfStudentNeedToPaid); $j++)
	     	{
	     		list($student_id, $num_of_item_need_to_paid) = $arrTotalNoOfStudentNeedToPaid[$j];
	     		$arrNumOfItemNeedToPaied[$student_id]['NumOfItemNeedToPaid'] = $num_of_item_need_to_paid;
	     	}
	     }
	     
	     if($signed == 0)
	     {
	     	$total_num_of_class_student_need_to_paid = " - ";
	     }
	     else
	     {
		     if(sizeof($arrTotalNoOfStudentNeedToPaid)>0)
		     	$total_num_of_class_student_need_to_paid = sizeof($arrTotalNoOfStudentNeedToPaid);
		     else 
		     	$total_num_of_class_student_need_to_paid = " - ";
		     
		     $num_of_whole_school_student_need_to_paid = $num_of_whole_school_student_need_to_paid + $total_num_of_class_student_need_to_paid;
		 }
		 
	     
	     $sql = "SELECT StudentID, COUNT(*) as TotalItemPaid FROM PAYMENT_PAYMENT_ITEMSTUDENT WHERE StudentID IN ($targetUserID) AND ItemID IN ($targetItemID) AND RecordStatus = 1 GROUP BY StudentID";
	     $arrStudentPaid = $lnotice->returnArray($sql,2);

	     for($j=0; $j<sizeof($arrStudentPaid); $j++){
	     	list($sid, $NumOfItemPaid) = $arrStudentPaid[$j];
	     	if($NumOfItemPaid == $arrNumOfItemNeedToPaied[$sid]['NumOfItemNeedToPaid'])
	     	{
	     		$cnt++;
	     	}
	     }
	     if(sizeof($arrStudentPaid)==0)
	     	$cnt = " - ";
	     
	     
	     if($signed == 0)
	     {
	     	$num_of_class_student_paid_successfully = " - ";
	     }
	     else
	     {
	     	$num_of_class_student_paid_successfully = $cnt;
			$num_of_whole_school_student_paid_successfully = $num_of_whole_school_student_paid_successfully + $num_of_class_student_paid_successfully;
	     }
	     	     
     	 $x .= "<td class='tabletext' width='80'>$total_num_of_class_student_need_to_paid</td>";
     	 $x .= "<td class='tabletext' width='80'>$num_of_class_student_paid_successfully</td>";
 	 }
     
     $x .= "</tr>\n";
     $all_total += $total;
     $all_signed += $signed;
}
$x .= "</table>\n";

### All student
$link = "<a class='tablelink' href='/home/eService/notice/tableview.php?NoticeID=$NoticeID&all=1'>$i_Notice_AllStudents</a>";
$x .= "<br /><table width='90%' border='0' cellspacing='0' cellpadding='9'>\n";
$x .= "<tr class='tableorangebottom'><td class='tabletext'>$link</td>";
$x .= "<td class='tabletext' width='80'>$all_signed</td>";
$x .= "<td class='tabletext' width='80'>$all_total</td>";
if(strtoupper($lnotice->Module) == "PAYMENT")
{
	if($all_signed == 0){
		$num_of_whole_school_student_paid_successfully = " - ";
		$num_of_whole_school_student_need_to_paid = " - ";
	}
	$x .= "<td class='tabletext' width='80'>$num_of_whole_school_student_need_to_paid</td>";
	$x .= "<td class='tabletext' width='80'>$num_of_whole_school_student_paid_successfully</td>";
}
$x .= "</tr>\n";
$x .= "</table>\n";

?>

<form name="print_form" action="paymentNotice_result_print_preview.php" method="post" target = "_blank">

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td>
                <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr> 
                	<td><br />
				<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
                                <tr valign='top'>
					<td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'><span class='tabletext'><?=$i_Notice_NoticeNumber?></span></td>
					<td class='tabletext'><?=$lnotice->NoticeNumber?></td>
				</tr>
                                
                                <tr valign='top'>
					<td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'><span class='tabletext'><?=$i_Notice_Title?></span></td>
					<td class='tabletext'><?=$lnotice->Title?></td>
				</tr>

                            	<tr>
                                	<td colspan="2" class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                                </tr>
                                
				</table>
			</td>
                </tr>
                </table>
	</td>
</tr>

<!--  List -->
<tr>
	<td align="center"><?=$x?></td>
</tr>  

<!-- Button //-->
<tr>
	<td>
                <table width="95%" border="0" cellspacing="0" cellpadding="2" align="center">
                <tr>
                	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
		</tr>
                <tr>
                	<td align="center">
				<?= $linterface->GET_ACTION_BTN($button_print . ($intranet_session_language=="en"?" ":"") . $button_preview, "button", "document.print_form.submit();") ?>
                                <?= $linterface->GET_ACTION_BTN($button_close, "button", "self.close()","submit3") ?>
                	</td>
		</tr>
                </table>
	</td>
</tr>

</table>
<br />

<input type="hidden" name="NoticeID" value="<?=$NoticeID?>">
</form>

<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>