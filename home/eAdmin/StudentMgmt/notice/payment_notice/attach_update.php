<?php
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libfiletable.php");
include_once($PATH_WRT_ROOT."includes/libcampusquota.php");

intranet_auth();
intranet_opendb();

echo "<META http-equiv=Content-Type content='text/html; charset=utf-8'>";

$no_file = 5;
$path = "$file_path/file/notice/$folder";
//if (!is_dir($path))
//{
    $path = $path."tmp";
//}

$li = new libfilesystem();
$li->folder_new($path);
	     if($bug_tracing['notice_attachment_log']){
             $temp_log_filepath = "$file_path/file/log_notice_attachment.txt";
             $temp_time = date("Y-m-d H:i:s");
             $temp_user = $UserID;
             $temp_page = 'notice_attach_update.php';
             $temp_action="create folder:".$path;
             $temp_content = get_file_content($temp_log_filepath);
             $temp_logentry = "\"$temp_time\",\"$temp_user\",\"$temp_page\",\"$temp_action\"\n";
             $temp_content .= $temp_logentry;
             write_file_content($temp_content, $temp_log_filepath);
		 }

# remove user deleted files

$lremove = new libfiletable("", $path, 0, 0, "");
$files = $lremove->files;
$attachStr=stripslashes($attachStr);
while (list($key, $value) = each($files)) {
     if(!strstr($attachStr,$files[$key][0])){
	      if($bug_tracing['notice_attachment_log']){
             $temp_log_filepath = "$file_path/file/log_notice_attachment.txt";
             $temp_time = date("Y-m-d H:i:s");
             $temp_user = $UserID;
             $temp_page = 'notice_attach_update.php';
             $temp_action="remove file:".$path."/".$files[$key][0];
             $temp_content = get_file_content($temp_log_filepath);
             $temp_logentry = "\"$temp_time\",\"$temp_user\",\"$temp_page\",\"$temp_action\"\n";
             $temp_content .= $temp_logentry;
             write_file_content($temp_content, $temp_log_filepath);
		 }
          $li->file_remove($path."/".$files[$key][0]);
     }
}

for($i=0;$i<$no_file;$i++){
     $loc = ${"userfile".$i};
     $file = stripslashes(${"hidden_userfile_name$i"});     
     $des = "$path/$file";
     if($loc=="none"){
     } else {
          if(strpos($file, ".")==0){
          }else{
               $li->lfs_copy($loc, $des);
          }
     }
}

$lo = new libfiletable("", $path, 0, 0, "");
$files = $lo->files;
?>


<script language="JavaScript1.2">
par = opener.window;
obj = opener.window.document.form1.elements["Attachment[]"];

par.checkOptionClear(obj);
<?php while (list($key, $value) = each($files)) echo "par.checkOptionAdd(obj, \"".$files[$key][0]." (".ceil($files[$key][1]/1000)."Kb".")\", \"".$files[$key][0]."\");\n"; ?>
par.checkOptionAdd(obj, "<?php for($i = 0; $i < 40; $i++) echo " "; ?>", "");
self.close();
</script>
