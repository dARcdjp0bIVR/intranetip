<?php
# using: 

####### Change log [Start] #######
#
# 	Date:   2020-09-07  Bill    [2020-0604-1821-16170]
#			load settings from payment notice
#
#	Date:	2019-11-12  Carlos
#			Added notice DateEnd to posted data of get_target_students(callback).
#
#	Date:	2019-04-08  Carlos
#			Improved reply slip with new js script /templates/forms/eNoticePayment_replyslip.js and php library eNoticePaymentReplySlip.php
#
#	Date:	2016-09-13	Bill
#			modified js copyback()
#			- prevent reload the page after displaying alert message
#			- fixed js error when focus on empty input fields
#
#	Date:	2012-06-20 YatWoon
#			Fixed: If edit the reply slip again, the question with "enter" will become "<br>"
#
#	Date:	2011-02-24	YatWoon
#			add option "Max. Reply Slip Option"
#
#	Date:	2010-01-26 [Henry Chow]
#			add non payment item
#
#	Date:	2010-01-14 [YatWoon]
#			build the reply slip tempates array
#
####### Change log [End] #######

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libform.php");
include_once($PATH_WRT_ROOT."includes/eNoticePaymentReplySlip.php");
include_once($PATH_WRT_ROOT."includes/json.php");

intranet_auth();
intranet_opendb();

$lnotice = new libnotice();
$lpayment = new libpayment();
$libjson = new JSON_obj();
$eNoticePaymentReplySlip = new eNoticePaymentReplySlip();

// [2020-0604-1821-16170]
//if (!$lnotice->disabled && $lnotice->hasIssueRight())
if (!$lnotice->disabled && $lnotice->hasPaymentNoticeIssueRight())
{
    $lf = new libform();
    
    $MODULE_OBJ['title'] = $i_Notice_ReplyContent;
    
	$linterface = new interface_html("popup.html");
	$linterface->LAYOUT_START();
	
?>

<script type="text/javascript" language="javascript" src="/templates/forms/eNoticePayment_replyslip.js?t=<?=time()?>"></script>
<script type="text/javascript" language="javascript" src="/templates/jquery/jquery.tablednd_0_5.js"></script>
<style type="text/css">
	table#ContentTable tr.move_selected > td { background-color:#fbf786; border-top: 2px dashed #d3981a; border-bottom: 2px dashed #d3981a; }
</style>
<br />

<form id="ansForm" name="ansForm" action="" onsubmit="return false;">
<input type="hidden" name="qStr" value="">
<input type="hidden" name="aStr" value="">
<div id="ReplySlipContainer"><?php echo $linterface->Get_Ajax_Loading_Image();?></div>
<script language="Javascript">
function get_target_students(callback)
{
	var post_data = {};
	if(window.opener.document.form1['NoticeID']){
		post_data['NoticeID'] = window.opener.document.form1['NoticeID'].value;
	}
	if(window.opener.document.form1['DateStart']){
		post_data['DateStart'] = window.opener.document.form1['DateStart'].value;
	}
	if(window.opener.document.form1['DateEnd']){
		post_data['DateEnd'] = window.opener.document.form1['DateEnd'].value;
	}
	if(window.opener.document.form1.type){
		post_data['type'] = window.opener.document.form1.type.value;
	}
	post_data['target[]'] = [];
	var target_selection = window.opener.document.form1['target[]'];
	if(target_selection){
		if(!target_selection.options){
			for(var i=0;i<target_selection.length;i++){
				if(target_selection[i].checked){
					post_data['target[]'].push(target_selection[i].value);
				}
			}
		}else{
			for(var i=0;i<target_selection.options.length;i++){
				if(target_selection.options[i].value != ''){
					post_data['target[]'].push(target_selection.options[i].value);
				}
			}
		}
	}
	
	$.post('ajax_get_target_students.php',post_data,function(returnData){
		var student_ary = [];
		if(JSON && JSON.parse){
			student_ary = JSON.parse(returnData);
		}else{
			eval('student_ary='+returnData+';');
		}
		callback(student_ary);
	});
}

$(document).ready(function(){
var form_templates = [];
<?php
$paymentCategory = $lnotice->getPaymentCategory();
$subsidyIdentities = $lpayment->getSubsidyIdentity(array('GetStudentIDs'=>true,'OrderByIdentityName'=>true));
echo $lnotice->buildReplySlipTemplates("Payment");
?>
var options = {
"QuestionString":window.opener.document.form1.qStr.value,
"AnswerString":window.opener.document.form1.aStr.value,
"AllFieldsReq":window.opener.document.form1.AllFieldsReq && window.opener.document.form1.AllFieldsReq.checked?1:0,
"DisplayQuestionNumber":window.opener.document.form1.DisplayQuestionNumber && window.opener.document.form1.DisplayQuestionNumber.checked?1:0,
"IsTemplate":window.opener.document.form1['templateID'] && window.opener.document.form1['templateID'].value > 0? true : false,
"PaymentCategoryAry":<?=$libjson->encode($paymentCategory)?>,
"TemplatesAry":form_templates,
"MaxOptionNo":"<?=max(20,intval($lnotice->MaxReplySlipOption))?>",
"SubsidyIdentityAry":<?=$libjson->encode($subsidyIdentities)?>,
"TargetStudentAry":[],
"ClassNameAry":[],
"ClassNameSelectionData":[],
<?php
$words = $eNoticePaymentReplySlip->getRequiredWords();
foreach($words as $key => $value){
	echo '"'.$key.'":"'.$value.'",'."\n";
}
?>
"Debug":false
};

get_target_students(
	function(student_ary){
		var classname_ary = [];
		var classname_selection_data = [];
		for(var i=0;i<student_ary.length;i++){
			if(student_ary[i]['ClassName'] != '' && classname_ary.indexOf(student_ary[i]['ClassName']) == -1){
				classname_ary.push(student_ary[i]['ClassName']);
			}
		}
		classname_selection_data.push(['<?php echo $Lang['General']['All'];?>','']);
		for(var i=0;i<classname_ary.length;i++){
			classname_selection_data.push([classname_ary[i],classname_ary[i]]);
		}
		options["TargetStudentAry"] = student_ary;
		options["ClassNameAry"] = classname_ary;
		options["ClassNameSelectionData"] = classname_selection_data;
		var sheet = new Answersheet(options);	
		sheet.renderEditPanel('ReplySlipContainer');
		
		$('#submitBtn').bind('click',function(){
			var validate_ok = sheet.validateEditForm();
			if(validate_ok){
				var qstr = sheet.constructQuestionString();
				window.opener.document.form1.qStr.value = qstr;
				window.close();
			}else{
				alert("<?=$Lang['eNotice']['PleaseCompleteAllQuestions']?>");
			}
		});
	}
);

});
</script>

<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
<tr>
	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
</tr>
<tr>
	<td align="center">
		<?= $linterface->GET_ACTION_BTN($button_save, "button", "","submitBtn") ?>
		<?= $linterface->GET_ACTION_BTN($button_close, "button", "window.close();","cancelBtn") ?>
	</td>
</tr>
</table>

</form>
<?php
}
else
{
	intranet_closedb();
    header ("Location: /");
    exit;
}

intranet_closedb();
$linterface->LAYOUT_STOP();
?>