<?php
# using: Bill

########################################
#
#   Date:   2020-11-10  Bill    [EJ DM#1444]
#           set $noticeType = 'PY' for payment notice
#
#	Date:	2015-10-28	Bill	[2015-0416-1040-06164]
#			support merge notice content
#
#	Date:	2015-06-08 Roy	> ej.5.0.5.6.1
#			use overrideExistingScheduledPushMessageFromModule() to remove not yet sent scheduled push message
#
#	Date:	2014-06-23 YatWoon	> ip.2.5.5.8.1
#			Improved: Revised system return message method [Case#Q63447]
#
########################################

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp.php");
include_once($PATH_WRT_ROOT."includes/libemail.php");
include_once($PATH_WRT_ROOT."includes/libsendmail.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");

intranet_auth();
intranet_opendb();

$lu = new libuser($UserID);
$libeClassApp = new libeClassApp();

$NoticeID = $NoticeIDArr;

### Send Rejected Msg
foreach((array)$NoticeID as $_noticeID){
	$noticeObj = new libnotice($_noticeID);
	$noticeObj->sendApprovalNotice(false);
}

$libdb = new libdb();
$sql = "UPDATE INTRANET_NOTICE SET 
			RecordStatus = '5',
			ApprovedBy = '$UserID',
			ApprovedTime = now()
		WHERE NoticeID IN ('".implode("','",$NoticeID)."')";
$libdb->db_db_query($sql);
        
intranet_closedb();

if ($_GET['student_notice']) {
	$noticeType = 'S';
} else if ($_GET['is_payment_notice']) {
    $noticeType = 'PY';
} else {
	$noticeType = 'P';
}

//header("Location: $location?noticeType=$noticeType&status=$status&year=$year&month=$month&xmsg=". $Lang['General']['ReturnMessage']['DeleteSuccess']);
header("Location: after_save_location.php?noticeType=$noticeType&xmsg=UpdateSuccess&pageNo=$pageNo&field=$field&page_size_change=$page_size_change&numPerPage=$numPerPage&status=$sus_status");
?>