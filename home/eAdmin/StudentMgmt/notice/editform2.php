<?php
# using: 

####### Change log [Start] #####
#
# 	Date:   2020-09-07  Bill    [2020-0604-1821-16170]
#			access right checking > replace hasIssueRight() by hasSchoolNoticeIssueRight()
#
#	Date:	2016-08-04	Bill	[EJ DM#718]
#			Update layout
#
#	Date:	2016-07-20	Bill	[2016-0719-1134-29206]
#			- handle must submit and display question number settings
#
#	Date:	2016-07-14	Bill	[2016-0113-1514-09066]
#			- Updated JS to support MC questions to set interdependence
#			- Updated layout to edit questions
#			- Fix error in IE
#
#	Date:	2016-06-10	Bill	[2016-0113-1514-09066]
#			Create file
#			- Copy from editform.php
#			- Get Reply Slip Content through ajax
#
####### Change log [End] #######

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$lnotice = new libnotice();

// [2020-0604-1821-16170]
//if (!$lnotice->disabled && $lnotice->hasIssueRight())
if (!$lnotice->disabled && $lnotice->hasSchoolNoticeIssueRight())
{
    // Module & Navigation
	$PAGE_NAVIGATION = array();
	//$PAGE_NAVIGATION[] = array($Lang['eSurvey']['Question']);
	$PAGE_NAVIGATION[] = $i_general_BasicSettings;
	$PAGE_NAVIGATION[] = $Lang['eNotice']['ReplySlipQuestionSetting'];
    $MODULE_OBJ['title'] = $Lang['eNotice']['SetReplySlipContent'];
    
    // Build Layout
	$linterface = new interface_html("popup.html");
	$linterface->LAYOUT_START();
	
	// Help Button
	$onlineHelpBtn = gen_online_help_btn_and_layer('enotice_admin','edit_slip');
	
	// Reply Slip Templates
	$reply_templates = $lnotice->buildReplySlipTemplatesInArray();
	$template_selection = getSelectByArray($reply_templates, "id='fTemplate' name='fTemplate' onChange=\"selectTemplate(this)\"", "", "", "", "- $i_Form_answersheet_template -");
	
	// Question Type
	$question_type = array();
	$question_type[] = array(1, $i_Form_answersheet_tf);
	$question_type[] = array(2, $i_Form_answersheet_mc);
	$question_type[] = array(3, $i_Form_answersheet_mo);
	$question_type[] = array(4, $i_Form_answersheet_sq1);
	$question_type[] = array(5, $i_Form_answersheet_sq2);
	$question_type[] = array(6, $i_Form_answersheet_not_applicable);
	$type_selection = getSelectByArray($question_type, "id='qType' name='qType' onChange=\"changeQType(this.value)\"", "", "", "", "- $i_general_Format -");
	
	// No. of Option
	$MaxOption = $lnotice->MaxReplySlipOption;
	$MaxOption = $MaxOption? $MaxOption : 50;
	$optionNum = array();
    for($OptionCount=2; $OptionCount<=$MaxOption; $OptionCount++)
       $optionNum[] = array($OptionCount, $OptionCount);
	$option_selection = getSelectByArray($optionNum, "id='oNum' name='oNum'", "", "", "", "- $i_Form_answersheet_option -");
?>

<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/<?=$LAYOUT_SKIN?>/js/script.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/thickbox.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.blockUI.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.autocomplete.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.jeditable.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.tablednd_0_5.js"></script>
<link rel="stylesheet" href="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.autocomplete.css" type="text/css">
<link rel="stylesheet" href="<?=$PATH_WRT_ROOT?>templates/jquery/thickbox.css" type="text/css" media="screen">

<style type="text/css">
	table#ContentTable tr.move_selected > td { background-color:#fbf786; border-top: 2px dashed #d3981a; border-bottom: 2px dashed #d3981a; }
</style>

<script language="Javascript">

// Grab values for open window
s = new String(window.opener.document.form1.qStr.value);
s = s.replace(/"/g, '&quot;');

//if(window.opener.document.form1.AllFieldsReq.checked)
//	AllFieldsReq = 1;
//if(window.opener.document.form1.DisplayQuestionNumber.checked)
//	DisplayQuestionNumber = 1;

// Get Settings from parent window
AllFieldsReq = 0;
AllFieldsReq = window.opener.document.form1.AllFieldsReq.value || 0;
DisplayQuestionNumber = 0;
DisplayQuestionNumber = window.opener.document.form1.DisplayQuestionNumber.value || 0;

function recurReplace(exp, reby, txt)
{
    while (txt.search(exp)!=-1) {
        txt = txt.replace(exp, reby);
    }
	return txt;
}

// define trim function for IE
if(typeof String.prototype.trim !== 'function') {
	String.prototype.trim = function() {
    	return this.replace(/^\s+|\s+$/g, ''); 
	};
}

function selectTemplate(obj)
{
	// Selected Template Info
	TemplateIndex = obj.selectedIndex;
	TemplateContent = obj.value;
	
	if (confirm("<?=$i_Form_chg_template?>"))
	{
		// Existing Template
		if (TemplateIndex!=0) {
			document.ansForm.CurrentQStr.value = TemplateContent;
	    }
	    // Remove Existing Template
	    else {
			document.ansForm.CurrentQStr.value = "";
	    }
	    
	    // Update Reply Slip & Input Fields
	    updateReplySlip('copy');
	    document.ansForm.CurrentQTemplate.value = TemplateIndex;
		document.ansForm.secDesc.focus();
	} 
	else 
	{
		obj.selectedIndex = document.ansForm.CurrentQTemplate.value;
	}
}

function changeQType(type)
{
	// True/False & Short/Long Text Question
	if(type==1 || type==4 || type==5)
	{
		$('tr#optionTr').css( "display", "none" );
		$('tr#submitTr').css( "display", "" );
	}
	// MC Question
	else if(type==2 || type==3)
	{
		$('tr#optionTr').css( "display", "" );
		$('tr#submitTr').css( "display", "" );
	}
	// Not applicable
	else if(type==6)
	{
		$('tr#optionTr').css( "display", "none" );
		$('tr#submitTr').css( "display", "none" );
	}
}

function addNewQuestion()
{
	// Topic / Title
	if(document.ansForm.secDesc.value.trim()==""){
		alert("<?=$i_Form_pls_fill_in_title?>");
        document.ansForm.secDesc.focus();
		return false;
	}
	
	// Format
    qtype=String(document.ansForm.qType.selectedIndex);
	if (qtype==0){
        alert("<?=$i_Form_pls_specify_type?>");
        document.ansForm.qType.focus();
        return false;
    }
    
    // Options
	if((qtype==2 || qtype==3) && document.ansForm.oNum.value==""){
		alert("<?=$Lang['eNotice']['PleaseFillOptionNumber']?>");
        document.ansForm.oNum.focus();
		return false;
	}
	
	// Add New Question
	updateReplySlip('add');
	
	// Reset the form
	document.ansForm.reset();
	document.ansForm.secDesc.focus();
	
	// Load must submit settings
	var fieldSettingVal = 0;
	if(AllFieldsReq==1){
		fieldSettingVal = 1;
    	$('input#AllFieldsReqG').attr('checked', true);
    	$('input#mustSubmitOption').attr('disabled','disabled');
    	$('input#mustSubmitOption').attr('checked', true);
    }
    else{
    	$('input#AllFieldsReqG').attr('checked', false);
    	$('input#mustSubmitOption').attr('disabled','');
    }
    document.ansForm.AllFieldsReq.value = fieldSettingVal;
    
    // Load question number settings
	var QNumVal = 0;
    if(DisplayQuestionNumber==1){
    	QNumVal = 1;
    	$('input#DisplayQNumG').attr('checked', true);
    }
    else{
    	$('input#DisplayQNumG').attr('checked', false);
    }
    document.ansForm.DisplayQNum.value = QNumVal;
}

function copybackWindow()
{
    document.ansForm.CurrentQStr.value = document.ansForm.CurrentQStr.value;
    s = recurReplace(">", "&gt;", document.ansForm.CurrentQStr.value);
	s = recurReplace("<", "&lt;", s);
	window.opener.document.form1.qStr.value = s;
    window.opener.document.form1.aStr.value = document.ansForm.aStr.value;
	window.opener.document.form1.AllFieldsReq.value = document.ansForm.AllFieldsReq.value;
	window.opener.document.form1.DisplayQuestionNumber.value = document.ansForm.DisplayQNum.value;
    self.close();
}

function retainAnswerValues()
{
    var ansNum=1;
    var TempStr = "";
    var TempQStr = "";
    var valueTemp = new Array();
    
    // loop Questions
    var txtStr = document.ansForm.CurrentQStr.value;
    var submitAll = document.ansForm.AllFieldsReq.value;
    var QueArr = txtStr.split("#QUE#");
    for (var x=1; x<QueArr.length; x++)
    {
    	var myLen = 0;
        valueTemp[x-1] = new Array();
        
        // Question Details
        var QueDetails = QueArr[x].split("||");
        var QueType = QueDetails[0].split(",");
	    switch (QueType[0]){
            case "1":       myLen=2; break;
            case "4":
            case "5":
            case "6":       myLen=0; break;
            default:        myLen=QueType[1]; break;
	    }
        
        // Handle Question update
        QueDetails[1] = eval("document.answersheet.QTitle"+x+".value");
        if(submitAll=="0" && QueType[0]!=6 && QueDetails[2]!="")
        {
        	var QueDetailAry = QueDetails[2].split("#MSUB#");
        	var MSValue = "0";
        	if(eval("document.answersheet.MS"+x+".checked"))
        		MSValue = "1";
        		
        	QueDetails[2] = QueDetailAry[0] + "#MSUB#" + MSValue;
        } 
	    
	    if(x > 1){
	    	TempStr+='#QUE#';
	    }
	    
	    // loop Question Options
	    for (var m=0; m<myLen; m++){
	    	tmpVal = eval("document.answersheet.FD"+ansNum+"_"+m+".value");
	    	tmpMove = '';
	    	
	    	// T/F Question - Skip Target (Option based)
	    	if($('[name=FI'+ansNum+'_'+m+']').length){
	    		tmpMove = eval("document.answersheet.FI"+ansNum+"_"+m+".value");
	    	}
	    	// T/F & MC Question - Skip Target (Question based)
	    	else if(m==0 && $('[name=FI'+ansNum+']').length){
	    		tmpMove = eval("document.answersheet.FI"+ansNum+".value");
	    	}
	    	
    		// add tarMove to tempVal
    		if(tmpMove!=''){
    			tmpVal += '#TAR#' + tmpMove + '#';
    		}
    		
	        valueTemp[x-1][m] = recurReplace('"', '&quot;', tmpVal);
	    }
	    
	    // Text Question - Skip Target (Question based)
	    if(QueType[0]==4 || QueType[0]==5){
	    	if($('[name=FI'+ansNum+']').length){
	    		tmpMove = eval("document.answersheet.FI"+ansNum+".value");
	    		
	    		// add tarMove to tempVal
	    		if(tmpMove!=''){
	    			TempStr += '#TAR#' + tmpMove + '#';
	    		}
	    	}
	    }
	    else {
	    	TempStr+=valueTemp[x-1].join('#OPT#');
	    }
	    
	    QueArr[x] = QueDetails.join("||");
	    ansNum++;
    }
    
    var QueStr = QueArr.join("#QUE#");
    document.ansForm.CurrentQStr.value = QueStr;
    document.ansForm.tempOption.value = TempStr;
}

function updateReplySlip(action)
{
	// Block Document
	Block_Document('loading');
	
	// Update available answers
	if(action!='copy'){
		retainAnswerValues();
	}
	document.ansForm.action.value = action;
	
	// Update Reply Slip Content
	$('#blockInput').load
	(
		'ajax_loadFormContent.php',
		$('#ansForm').serializeArray(),
		function (data){
			document.ansForm.CurrentQStr.value = document.answersheet.updateQStr.value;
			
			// Support Drag & Drop of Questions
			$("#ContentTable").tableDnD({
				onDrop: function(table, row) {
					var existingOrder = '';
					var newOrderStr = '';
					
					// loop Questions
					var rows = table.tBodies[0].rows;
					if(rows.length > 0){
						for (var i=0; i<rows.length; i++)
						{
							existingOrder += "row" + (i+1);
							newOrderStr += rows[i].id;
							if(i!=(rows.length-1)) {
								existingOrder += ",";
								newOrderStr += ",";
							}
						}
						
						// check if any change in Question order
						if(newOrderStr!=existingOrder){
							document.ansForm.DisplayQOrder.value = newOrderStr;
							
							// Update Reply Slip when drag questions
							updateReplySlip('drag');
						}
					}
				},
				onDragStart: function(table, row) {
					// do nothing
				},
				dragHandle: "Dragable", 
				onDragClass: "move_selected"
			});
			
			// Return Questions and Close Window if save Reply Slip
			if(action=='copyback')
			{
				copybackWindow();
			}
		}
	);
	
	// UnBlock Document
	UnBlock_Document();
}

function updateGlobalSetting(type)
{
	if(type=="")	return false;
	if(type=="field")
	{
		var fieldSettingVal = 0;
		AllFieldsReq = 0;
    	$('input#mustSubmitOption').attr('disabled','');
    	
		if(document.ansForm.AllFieldsReqG.checked){
			fieldSettingVal = 1;
			AllFieldsReq = 1;
    		$('input#mustSubmitOption').attr('disabled','disabled');
    		$('input#mustSubmitOption').attr('checked', true);
		}
   		document.ansForm.AllFieldsReq.value = fieldSettingVal;
   		
   		updateReplySlip();
	}
	else if(type=="QNum")
	{
		var QNumVal = 0;
		DisplayQuestionNumber = 0;
		
		if(document.ansForm.DisplayQNumG.checked){
			QNumVal = 1;
			DisplayQuestionNumber = 1;
		}
   		document.ansForm.DisplayQNum.value = QNumVal;
	}
}

$(document).ready(function(){
	// Update parms
	document.ansForm.CurrentQStr.value = s;
	document.ansForm.aStr.value = window.opener.document.form1.aStr.value;
    document.ansForm.AllFieldsReq.value = AllFieldsReq;
    document.ansForm.DisplayQNum.value = DisplayQuestionNumber;
    
    // disable Required to be answered Option
    if(AllFieldsReq==1){
    	$('input#AllFieldsReqG').attr('checked', true);
    	$('input#mustSubmitOption').attr('disabled','disabled');
    	$('input#mustSubmitOption').attr('checked', true);
    }
    $('input#AllFieldsReq').val(AllFieldsReq);
    
    if(DisplayQuestionNumber==1){
    	$('input#DisplayQNumG').attr('checked', true);
    }
	$('input#DisplayQNum').val(DisplayQuestionNumber);
    
    // Load Reply Slip
    updateReplySlip('copy');
});

</script>

<?=$onlineHelpBtn?>

<form id="ansForm" name="ansForm" action="">
<input type="hidden" name="qStr" value="">
<input type="hidden" name="aStr" value="">

<input type="hidden" name="action" value="">
<input type="hidden" name="CurrentQStr" value="">
<input type="hidden" name="CurrentQOrder" value="">
<input type="hidden" name="CurrentQTemplate" value="0">
<input type="hidden" name="DisplayQNum" value="0">
<input type="hidden" name="DisplayQOrder" value="">
<input type="hidden" name="tempOption" value="">
<input type="hidden" name="AllFieldsReq" value="0">

<div>
	<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
	<tr>
		<td> &nbsp; <?=$linterface->GET_NAVIGATION2_IP25($PAGE_NAVIGATION[0])?></td>
	</tr>
	<tr><td>
			<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">
			<tr>
				<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_Survey_AllRequire2Fill?></span></td>
				<td class="tabletext"><input type="checkbox" id="AllFieldsReqG" name="AllFieldsReqG" value="1" onclick="updateGlobalSetting('field');"></td>
			</tr>
			<tr>
				<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$Lang['eNotice']['DisplayQuestionNumber']?></span></td>
				<td class="tabletext"><input type="checkbox" id="DisplayQNumG" name="DisplayQNumG" value="1" onclick="updateGlobalSetting('QNum');"></td>
			</tr>
			</table>
	</td></tr>
	</table>
</div>

<div id="blockDiv">
	<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
	<tr>
		<td> &nbsp; <?=$linterface->GET_NAVIGATION2_IP25($PAGE_NAVIGATION[1])?></td>
	</tr>
	<tr><td>
			<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">
			<tr>
				<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_Form_answersheet_template?></span></td>
				<td class="tabletext"><?=$template_selection?></td>
			</tr>
			<tr>
				<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_Form_answersheet_header?></span></td>
				<td class="tabletext"><textarea name="secDesc" class="tabletext" rows="3" cols="30"></textarea></td>
			</tr>
			<tr>
				<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_general_Format?></span></td>
				<td class="tabletext"><?=$type_selection?></td>
			</tr>
			<tr id="optionTr" name="optionTr">
				<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_Form_answersheet_option?></span></td>
				<td class="tabletext"><?=$option_selection?></td>
			</tr>
			<tr id="submitTr" name="submitTr">
				<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$Lang['eNotice']['QuestionNeedToReply']?></span></td>
				<td class="tabletext"><input type="checkbox" id="mustSubmitOption" name="mustSubmitOption"></td>
			</tr>
			</table>
	</td></tr>
	<tr><td>
			<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
			<tr>
				<td class="dotline" colspan="2"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
			</tr>
			</table>
	</td></tr>
	<tr><td colspan="2" align="center">
			<?= $linterface->GET_BTN($button_add, "button", "addNewQuestion();", "submit2") ?>
	</td></tr>
    </table>
</div>

<div id="blockInput">
	<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
	<tr><td>
			<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
			<tr>
				<td class="dotline" colspan="2"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
			</tr>
			</table>
	</td></tr>
	</table>
</div>

<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
<tr>
	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
</tr>
<tr>
	<td align="center">
		<?= $linterface->GET_ACTION_BTN($button_save, "button", "updateReplySlip('copyback');", "submit2") ?>
		<?= $linterface->GET_ACTION_BTN($button_close, "button", "self.close();", "cancelbtn") ?>
	</td>
</tr>
</table>
</form>

<?php
}
else
{
    header ("Location: /");
}

intranet_closedb();
$linterface->LAYOUT_STOP();
?>