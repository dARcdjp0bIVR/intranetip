<?php
# using: 
/*
 *  2020-07-16 [Bill]   [2020-0630-1400-45066]
 *  - apply bulk mode to send push msg
 *  - show no. of msg - sent out successfully + waiting to send out
 *
 *  2020-05-14 [Bill]   [2020-0505-1012-22206]
 *  - Added sendPushMessageByBatch() to send push msg
 *
 *  2019-06-06 [Bill]
 *  - add CSRF token checking
 *  
 *	2015-08-03 [Roy] (ip.2.5.6.7.1)
 *	- Added $fromModule and $moduleRecordID to link push message and notice
 *
 *	2015-07-20 [Ivan] (ip.2.5.6.7.1)
 *	- Added sms confirmation page logic
 */

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libsmsv2.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp.php");

if(!verifyCsrfToken($_POST['csrf_token'], $_POST['csrf_token_key'], $do_not_remove_token_data=true)){
    echo "<font color='red'>".$Lang['AppNotifyMessage']['eNotice']['send_failed']."</font>";
    exit;
}

intranet_auth();
intranet_opendb();

$lu = new libuser($UserID);
$lnotice = new libnotice($NoticeID);
$leClassApp = new libeClassApp();
$libsms = new libsmsv2();

$Message = standardizeFormPostValue($_POST['Message']);

$targetClass = '';
if (isset($_POST['SelectedClass'])) {
	$targetClass = $_POST['SelectedClass'];
}
else {
	$targetClass = $class;
}

//$UnsignedStudents = $lnotice->getUnsignList($NoticeID, $class);
$UnsignedStudents = $lnotice->getUnsignList($NoticeID, $targetClass);
$notifyuser = $lu->UserNameLang();

$NoticeNumber = "".intranet_undo_htmlspecialchars($lnotice->NoticeNumber);
$NoticeTitle = "".intranet_undo_htmlspecialchars($lnotice->Title);
$NoticeDeadline = "".$lnotice->DateEnd;

$MessageTitle = str_replace("[NoticeNumber]", $NoticeNumber, $Lang['AppNotifyMessage']['eNotice']['Title']['App']);
$MessageContent = str_replace("[NoticeNumber]", $NoticeNumber, $Lang['AppNotifyMessage']['eNotice']['Alert']['App']);
$MessageContent = str_replace("[NoticeTitle]", $NoticeTitle, $MessageContent);
$MessageContent = str_replace("[NoticeDeadline]", $NoticeDeadline, $MessageContent);

if ($Message == '') {
	$MessageTitle_sms = str_replace("[NoticeNumber]", $NoticeNumber, $Lang['AppNotifyMessage']['eNotice']['Title']['SMS']);
	$MessageContent_sms = str_replace("[NoticeNumber]", $NoticeNumber, $Lang['AppNotifyMessage']['eNotice']['Alert']['SMS']);
	$MessageContent_sms = str_replace("[NoticeTitle]", $NoticeTitle, $MessageContent_sms);
	$MessageContent_sms = str_replace("[NoticeDeadline]", $NoticeDeadline, $MessageContent_sms);
	
	$SMSContent = $MessageTitle_sms."\n".$MessageContent_sms;
}
else {
	$SMSContent = $Message;
}
$SMSContent = sms_substr($SMSContent);

$SentTotal = 0;
$namefield = getNameFieldByLang();
$parentAppFailArr = array();
$nonParentAppParentArr = array();

# Get parent app users list
if ($plugin['eClassApp']) {
	$logTable = 'APP_LOGIN_LOG';
}
else {
	$logTable = 'INTRANET_API_REQUEST_LOG';
}
$sql = "SELECT distinct UserID FROM $logTable";
$parentAppUserArr = $lu->returnArray($sql);
$parentAppUserIDArr = array();
foreach ((array)$parentAppUserArr as $parentAppUserInfo) {
	$parentAppUserIDArr[] = $parentAppUserInfo['UserID'];
}

### Get parent student mapping
$sql = "SELECT 
				ip.ParentID, ip.StudentID
		FROM 
				INTRANET_PARENTRELATION AS ip 
				INNER JOIN INTRANET_USER AS iu ON (ip.ParentID=iu.UserID) 
		WHERE 
				ip.StudentID IN ('".implode("','", (array)$UnsignedStudents)."') 
				AND iu.RecordStatus = 1";
$parentStudentAssoAry = BuildMultiKeyAssoc($lnotice->returnResultSet($sql), 'ParentID', array('StudentID'), $SingleValue=1, $BuildNumericArray=1);

$nonParentAppParentIdArr = array();
$messageParentStudentAssoAry = array();
foreach ((array)$parentStudentAssoAry as $_parentId => $_studentIdAry) {
	$_numOfStudent = count((array)$_studentIdAry);
	
	if (in_array($_parentId, (array)$parentAppUserIDArr)) {
		// build push message parent student asso array
		$messageParentStudentAssoAry[$_parentId] = $_studentIdAry;
	}
	else {
		if (!in_array($_parentId, (array)$nonParentAppParentIdArr)) {
			// send on sms to parent even if there are more than 1 student
			$nonParentAppParentArr[] = array('parentID'=>$_parentId, 'studentID'=>$_studentIdAry[0]);
			
			$nonParentAppParentIdArr[] = $_parentId;
		}
	}
}

// Send push message
$messageInfoAry = array();
$messageInfoAry[0]['relatedUserIdAssoAry'] = $messageParentStudentAssoAry;
//$notifyMessageId = $leClassApp->sendPushMessage($messageInfoAry, $MessageTitle, $MessageContent);

// [2020-0630-1400-45066] apply bulk mode to send push msg
if ($leClassApp->isEnabledSendBulkPushMessageInBatches()) {
    $notifyMessageId = $leClassApp->sendPushMessageByBatch($messageInfoAry, $MessageTitle, $MessageContent, $isPublic='', $recordStatus=1, $appType='', $sendTimeMode='', $sendTimeString='', $parNotifyMessageId='', $parNotifyMessageTargetIdAry='', $fromModule='eNotice', $NoticeID);
} else {
    $notifyMessageId = $leClassApp->sendPushMessage($messageInfoAry, $MessageTitle, $MessageContent, $isPublic='', $recordStatus=1, $appType='', $sendTimeMode='', $sendTimeString='', $parNotifyMessageId='', $parNotifyMessageTargetIdAry='', $fromModule='eNotice', $NoticeID);
}

$statisticsAry = $leClassApp->getPushMessageStatistics($notifyMessageId);
$statisticsAry = $statisticsAry[$notifyMessageId];

########### Send SMS to non Parent App users OR send SMS to all users when Parent App fail ###########
// Add push message failed parent to SMS
$failedParentAssoAry = $statisticsAry['sendFailAssoAry'];
foreach ((array)$failedParentAssoAry as $_parentId => $_studentIdAry) {
    // send on sms to parent even if there are more than 1 student
    $parentAppFailArr[] = array('parentID'=>$_parentId, 'studentID'=>$_studentIdAry[0]);
}
$smsParentArr = array_merge((array)$nonParentAppParentArr, (array)$parentAppFailArr);

if (sizeof($smsParentArr) > 0)
{
	foreach ((array)$smsParentArr as $smsParentInfo) {
		$ParentID = $smsParentInfo['parentID'];
		$StudentID = $smsParentInfo['studentID'];
		$sql = "SELECT 
					UserID, 
					TRIM(MobileTelNo) as Mobile,
					$namefield as UserName
				FROM 
					INTRANET_USER 
				WHERE 
					UserID = $ParentID
				ORDER BY 
					ClassName,
					ClassNumber,
					EnglishName
			";
		$users = $libsms->returnArray($sql, 3);
		
		$ParentID = $users[0]['UserID'];
		$mobile = $users[0]['Mobile'];
		$ParentName = $users[0]['UserName'];
		
		$mobile = $libsms->parsePhoneNumber($mobile);
		
		if($libsms->isValidPhoneNumber($mobile)){
			$recipientData[] = array($mobile,$StudentID,addslashes($ParentName),"");
			$valid_list[] = array($ParentID,$ParentName,$mobile,$StudentID);
		}
		else{
			$reason =$i_SMS_Error_NovalidPhone;
			$error_list[] = array($ParentID,$ParentName,$mobile,$StudentID,$reason);
		}
	}
}

# send sms
if(sizeof($recipientData)>0){
	$targetType = 3;
	$picType = 2;
	$adminPIC =$PHP_AUTH_USER;
	$userPIC = $UserID;
	$frModule= "";
	$deliveryTime = $time_send;
	$isIndividualMessage=false;
	$sms_message = $SMSContent; #### Use original text - not htmlspecialchars processed
	$returnCode = $libsms->sendSMS($sms_message,$recipientData, $targetType, $picType, $adminPIC, $userPIC,"",$deliveryTime,$isIndividualMessage);
	$isSent = 1;
	$smsTotal = sizeof($recipientData);
}
  
intranet_closedb();

if (($statisticsAry['numOfSendSuccess']>0 && $statisticsAry['numOfSendSuccess']!="") || ($statisticsAry['numOfWaitingToSend'] > 0 && $statisticsAry['numOfWaitingToSend'] != "") || ($isSent && $returnCode==true))
{
	if ($statisticsAry['numOfSendSuccess'] == "") {
		$statisticsAry['numOfSendSuccess'] = 0;
	}
	if ($smsTotal == "") {
		$smsTotal = 0;
	}
	//$returnMsg = str_replace("[smsTotal]", $smsTotal, str_replace("[SentTotal]", $statisticsAry['numOfSendSuccess'], $Lang['AppNotifyMessage']['PushMessageThenSMS_send_result']));

    // [2020-0630-1400-45066] return msg > include no. of push msg - waiting to send
    if($statisticsAry['numOfWaitingToSend'] == '') {
        $statisticsAry['numOfWaitingToSend'] = 0;
    }
    $notifyMessageCount = $statisticsAry['numOfSendSuccess'] + $statisticsAry['numOfWaitingToSend'];
    $returnMsg = str_replace("[smsTotal]", $smsTotal, str_replace("[SentTotal]", $notifyMessageCount, $Lang['AppNotifyMessage']['PushMessageThenSMS_msg_count']));

	echo "<font color='#DD5555'>".$returnMsg. "</font>";
}
else
{
	echo "<font color='red'>".$Lang['AppNotifyMessage']['send_failed']."</font>";
}
?>