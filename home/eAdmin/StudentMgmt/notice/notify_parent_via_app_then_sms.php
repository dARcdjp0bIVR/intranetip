<?php
# using: Roy

//error_reporting(E_ALL);
// ini_set('display_errors', '1');

########################################
#
#   Date:   2019-06-06  Bill
#           add CSRF token checking
#
#	Date:	2013-10-08	Roy
#			Newly added
#
########################################

ini_set('display_errors', '1');

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libsmsv2.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."plugins/asl_conf.php");
include_once($PATH_WRT_ROOT."includes/libaslparentapp.php");

if(!verifyCsrfToken($_POST['csrf_token'], $_POST['csrf_token_key'], $do_not_remove_token_data=true)){
    echo "<font color='red'>".$Lang['AppNotifyMessage']['eNotice']['send_failed']."</font>";
    exit;
}

intranet_auth();
intranet_opendb();

$lnotice = new libnotice($NoticeID);
$libsms = new libsmsv2();

$UnsignedStudents = $lnotice->getUnsignList($NoticeID, $class);

$lu = new libuser($UserID);
$notifyuser = $lu->UserNameLang();

$asl = new ASLParentApp($asl_parent_app["host"], $asl_parent_app["path"]);

$SchoolID = isset($asl_parent_app['SchoolID'])? $asl_parent_app['SchoolID'] : $_SERVER['SERVER_NAME'];
$MsgNotifyDateTime = date("Y-m-d H:i:s");

$NoticeTitle = "".$lnotice->NoticeNumber.":".$lnotice->Title." (".$i_Notice_DateEnd.":".$lnotice->DateEnd.")";
$MessageTitle = $Lang['AppNotifyMessage']['eNotice']['Title'] . " [#".$lnotice->NoticeNumber."]";
$MessageContent = str_replace("[NoticeTitle]", $NoticeTitle, $Lang['AppNotifyMessage']['eNotice']['Alert']);

$SMSContent = $MessageTitle."\n".$MessageContent;

$SentTotal = 0;
$NotifyMessageID = -1;

$namefield = getNameFieldByLang();

$ReturnMessage = "";

# Get parent app users list
$sql = "select distinct UserID from INTRANET_API_REQUEST_LOG";
$parentAppUserArr = $lu->returnArray($sql);
$parentAppUserIDArr = array();
foreach ((array)$parentAppUserArr as $parentAppUserInfo) {
	$parentAppUserIDArr[] = $parentAppUserInfo['UserID'];
}
// debug_pr($parentAppUserIDArr);

$parentAppFailArr = array();
$nonParentAppParentArr = array();

for($i=0; $i<sizeof($UnsignedStudents); $i++)
{
	$StudentID = $UnsignedStudents[$i];

	# Get Parent's userlogin
	$sql = "SELECT 	iu.UserLogin,
					iu.UserID,
					$namefield as UserName, 
					TRIM(iu.MobileTelNo) as Mobile
			from 	INTRANET_PARENTRELATION AS ip,
					INTRANET_USER AS iu 
			WHERE 	ip.StudentID='{$StudentID}' AND 
					ip.ParentID=iu.UserID  AND 
					iu.RecordStatus=1 ";
	$recipient_rows = $lnotice->returnResultSet($sql);
	
	for ($ri=0; $ri<sizeof($recipient_rows); $ri++)
	{
		$parentID = $recipient_rows[$ri]["UserID"];
		
		# Check is Parent App users
		if (in_array($parentID, (array)$parentAppUserIDArr)) {
			$recipient_parent_userlogin = $recipient_rows[$ri]["UserLogin"];
			
			$ASLParam = array(
				"SchID" => $SchoolID,
				"MsgNotifyDateTime" => $MsgNotifyDateTime,
				"MessageTitle" => $MessageTitle,
				"MessageContent" => $MessageContent,
				"NotifyFor" => $notifyfor,
				"IsPublic" => "N",
				"NotifyUser" => $notifyuser,
				"TargetUsers" => array(
					"TargetUser" => $recipient_parent_userlogin
				)
			);
			$Result = $asl->NotifyMessage($ASLParam, $ReturnMessage);
			
			if ($Result)
			{
				if ($NotifyMessageID==-1)
				{
					$sql = "INSERT INTO INTRANET_APP_NOTIFY_MESSAGE(NotifyDateTime,MessageTitle,MessageContent,NotifyFor,IsPublic,NotifyUser,NotifyUserID,RecordStatus,DateInput,DateModified)
							VALUES ('$MsgNotifyDateTime','".addslashes($MessageTitle)."','MULTIPLE MESSAGES','".$notifyfor."','N','".addslashes($notifyuser)."','$UserID','1',NOW(),NOW())";
				
					$success = $lnotice->db_db_query($sql);
					$NotifyMessageID = $lnotice->db_insert_id();
				}

				if ($NotifyMessageID>0)
				{
						$sql = "INSERT INTO INTRANET_APP_NOTIFY_MESSAGE_TARGET(NotifyMessageID,TargetType,TargetID,MessageTitle,MessageContent,DateInput)
								VALUES ('$NotifyMessageID','U','".$parentID."','".addslashes($MessageTitle)."','".addslashes($MessageContent)."',NOW())";
						$target_success = $lnotice->db_db_query($sql);
						
						$SentTotal ++;
				}
			}
			else
			{
				# Record fail Parent App users
				$parentAppFailArr[] = array('parentID'=>$parentID, 'studentID'=>$StudentID);
			}
		}
		else
		{
			# Record non Parent App users
			if (!in_array($parentID, (array)$nonParentAppParentArr)) {
				$nonParentAppParentArr[] = array('parentID'=>$parentID, 'studentID'=>$StudentID);
			}
		}
	}
}

$smsParentArr = array_merge((array)$nonParentAppParentArr, (array)$parentAppFailArr);

########### Send SMS to non Parent App users OR send SMS to all users when Parent App fail ###########
if (sizeof($smsParentArr) > 0)
{
	foreach ((array)$smsParentArr as $smsParentInfo) {
		$ParentID = $smsParentInfo['parentID'];
		$StudentID = $smsParentInfo['studentID'];
		$sql = "
				SELECT 
					UserID, 
					TRIM(MobileTelNo) as Mobile,
					$namefield as UserName
				FROM 
					INTRANET_USER 
				WHERE 
					UserID = $ParentID
				ORDER BY 
					ClassName,
					ClassNumber,
					EnglishName
			";
		$users = $libsms->returnArray($sql, 3);
		
		$ParentID = $users[0]['UserID'];
		$mobile = $users[0]['Mobile'];
		$ParentName = $users[0]['UserName'];
		
		$mobile = $libsms->parsePhoneNumber($mobile);
		
		if($libsms->isValidPhoneNumber($mobile)){
			$recipientData[] = array($mobile,$StudentID,addslashes($ParentName),"");
			$valid_list[] = array($ParentID,$ParentName,$mobile,$StudentID);
		}
		else{
			$reason =$i_SMS_Error_NovalidPhone;
			$error_list[] = array($ParentID,$ParentName,$mobile,$StudentID,$reason);
		}
	}
}

# Send sms
if(sizeof($recipientData) > 0)
{
	$targetType = 3;
	$picType = 2;
	$adminPIC = $PHP_AUTH_USER;
	$userPIC = $UserID;
	$frModule = "";
	$deliveryTime = $time_send;
	$isIndividualMessage = false;
	$sms_message = $SMSContent; #### Use original text - not htmlspecialchars processed
	$returnCode = $libsms->sendSMS($sms_message,$recipientData, $targetType, $picType, $adminPIC, $userPIC,"",$deliveryTime,$isIndividualMessage);
	$isSent = 1;
	$smsTotal = sizeof($recipientData);
}

intranet_closedb();

if (($SentTotal>0 && $SentTotal!="") || ($isSent && $returnCode==true))
{
	if ($SentTotal == "") {
		$SentTotal = 0;
	}
	if ($smsTotal == "") {
		$smsTotal = 0;
	}
	
	$returnMsg = str_replace("[smsTotal]", $smsTotal, str_replace("[SentTotal]", $SentTotal, $Lang['AppNotifyMessage']['PushMessageThenSMS_send_result']));
	echo "<font color='#DD5555'>".$returnMsg. "</font>";

}
else
{
	echo "<font color='red'>".$Lang['AppNotifyMessage']['send_failed']."</font>";
}
?>