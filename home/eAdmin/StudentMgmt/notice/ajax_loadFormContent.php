<?
# Using: 

###################################
#
#	Date:	2016-08-04	Bill	[EJ DM#718]
#			Update layout
#
#	Date:	2016-07-14	Bill	[2016-0113-1514-09066]
#			- Fix error when update question order
#
#	Date:	2016-06-10	Bill	[2016-0113-1514-09066]
#			Create file
#			- Perform actions on Reply Slip Content
#
###################################

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libform.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$lnotice = new libnotice();

// Page Navigation
//$PAGE_NAVIGATION = array();
//$PAGE_NAVIGATION[] = array($Lang['eNotice']['ReplySlipQuestions']);
$PAGE_NAVIGATION = $Lang['eNotice']['ReplySlipQuestions'];

// Row Mapping
$rowLocatMapping = array();
$rowLocatMapping['end'] = 'end';

// Handle Questions and Options
// Questions
$qStr = $_POST["CurrentQStr"];
//$qStr = stripslashes($qStr);
$qStr = str_replace(">", "&gt;", $qStr);
$qStr = str_replace("<", "&lt;", $qStr);
//$qStr = str_replace("&lt;br&gt;", "<br>", $qStr);
$qAry = $lnotice->splitQuestion($qStr, true);

// Options
$tempOption = $_POST["tempOption"];
//$tempOption = stripslashes($tempOption);
$qAry = $lnotice->handleQuestionAnswer($qAry, $tempOption);

// Question Settings
$AllFieldsReq = $_POST["AllFieldsReq"];
$DisplayQNum = $_POST["DisplayQNum"];

// Preform required action
$action = $_POST["action"];
switch(trim($action))
{
	// Add Question
	case "add":
		// New Question Settings
		$QType = $_POST["qType"];
		$Title = $_POST["secDesc"];
//		$Title = stripslashes($Title);
		$Title = str_replace(">", "&gt;", $Title);
		$Title = str_replace("<", "&lt;", $Title);
//		$Title = preg_replace("(\r\n|\n)", "<br>", $Title);
		$optionNum = $_POST["oNum"];
		$optionNum = $QType==1? 2 : $optionNum;
		$mustSubmitOption = $_POST["mustSubmitOption"];
		$mustSubmitOption = $mustSubmitOption=="on"? 1 : 0; 
		
		// Append new Question to existing array
		$addAry = array();
		$addAry[0] = $QType;
		$addAry[1] = $Title;
		$addAry[2] = array();
		if($optionNum >= 2){
			for($i=0; $i<$optionNum; $i++){
				// Add empty options
				$addAry[2][] = "";
			}
		}
		$addAry[3] = $mustSubmitOption;
		
		// Append
		$qAry[] = $addAry;
		break;
		
	// Drag Question
	case "drag":
		// New Order
		$DisplayQOrder = $_POST["DisplayQOrder"];
		$newQOrder = explode(",", $DisplayQOrder);
		
		// loop Order
		$newQAry = array();
		for($i=0; $i<count($newQOrder); $i++){
			$targetRowOrder = str_replace("row", "", $newQOrder[$i]);
			$targetRowOrder = intval($targetRowOrder)-1;
			
			// Update Question Order and Row Mapping
			$newQAry[] = $qAry[$targetRowOrder];
			$rowLocatMapping[$targetRowOrder] = $i;
		}
		
		// loop New Question Order
		$questionCount = count($newQAry);
		for($i=0; $i<$questionCount; $i++)
		{
			// Update Skip Question Order for Question
			$relatedQuestion = $newQAry[$i][4];
			if(!empty($relatedQuestion))
			{
				$lastQuestion = $i==$questionCount-1;
				$optionCount = count((array)$relatedQuestion);
				
				// loop Related Settings
				for($opt=0; $opt<$optionCount; $opt++){
					$OptionTarget = $relatedQuestion[$opt];
					$newOptionTarget = $rowLocatMapping[$OptionTarget];
					
					// Reset if Target Order not appropriate
					if($newOptionTarget!=="" && is_numeric($newOptionTarget) && (($newOptionTarget - $i) < 2) || $lastQuestion){
						$newOptionTarget = "";
					}
					
					$newQAry[$i][4][$opt] = $newOptionTarget;
				}
			}
		}
		
		// Replace Old Order
		$qAry = $newQAry;
		break;
		
	// Remove Question
	case "remove":
		// Remove 
		array_splice($qAry, $DisplayQOrder, 1);
		
		// loop Questions
		for($i=0; $i<count($qAry); $i++)
		{
			// Update Skip Question Order for T/F Question
			$relatedQuestion = $qAry[$i][4];
			if(!empty($relatedQuestion))
			{
				$optionCount = count((array)$relatedQuestion);
				
				// Update if Target Question is after deleted Question
				for($opt=0; $opt<$optionCount; $opt++){
					$OptionTarget = $relatedQuestion[$opt];
					if($OptionTarget!="" && is_numeric($OptionTarget) && $OptionTarget!="end" && $OptionTarget>$DisplayQOrder){
						$qAry[$i][4][$opt] = $OptionTarget-1;
					}
				}
			}
		}
		break;
		
	default:
		// Do Nothing
		break;
}

// Build Reply Slip Content
$x = "";
$x .= "<form name='answersheet'>
			<table width='100%' border='0' cellspacing='0' cellpadding='5' align='center'>
			<tr>
				<td align='center'>".$Lang['eNotice']['ReplySlip']."</td>
			</tr>
			<tr>
				<td>".$linterface->GET_NAVIGATION2_IP25($PAGE_NAVIGATION)."</td>
			</tr>
			<tr>
				<td>".$lnotice->returnReplySlipContent("edit", $qAry, "", $AllFieldsReq, $DisplayQNum)."</td>
			</tr>
			</table>
			<input type='hidden' name='updateQStr' value=\"".$lnotice->returnQuestionString($qAry)."\">
		</form>";
		
echo $x;
?>