<?php
// Using: 

/********************** Change Log ***********************/
#
#   Date    :   2020-09-22  Bill    [2020-0922-1006-24206]
#               fixed: cannot display whole notice title if attachment file name too lang - break by chars
#
#   Date    :   2020-09-15  Bill    [2020-0915-1022-58066]
#               fixed: incorrect Question Relation handling if reply slip has MC questions that not required to be answered
#
#   Date    :   2020-09-03  Bill    [2020-0826-1451-05073]
#               Performance Issue: use JS to handle Question Relation
#
#   Date    :   2020-08-27  Bill    [2020-0826-1725-41260]
#               fixed cannot display question number
#
#   Date    :   2020-08-25  Bill    [2020-0821-1028-21206]
#               fixed cannot access urls with 'https://' due to jquery mobile event listeners
#
#	Date	:	2019-10-25	Philips
#				Display $i_Notice_ReplySlip
#
#	Date	:	2019-09-11  Carlos
#				Improved payment notice reply slip with new js script /templates/forms/eNoticePayment_replyslip.js and php library eNoticePaymentReplySlip.php
#
#   Date    :   2019-09-05  Bill    [2019-0902-1654-50207]
#               fixed cannot access urls with '#' due to jquery mobile event listeners
#
#	Date    :	2019-05-10 (Bill)
#               - prevent SQL Injection
#               - apply intranet_auth()
#
#	Date	: 	2016-07-29	Bill	[2016-0113-1514-09066]
#				Support MC questions to set interdependence
#				Added response for questions that no required to submit
#
#	Date	:	2016-07-29	Bill	[2016-0728-1454-09073]
#				apply merge notice content
#
/********************** end of Change Log ***********************/

$PATH_WRT_ROOT = "../../../../../";

//set language
@session_start();

include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp_init.php");
$leClassApp_init = new libeClassApp_init();

$parLang = isset($_GET['parLang'])? $_GET['parLang'] : '';
$_SESSION['intranet_hardcode_lang'] = $leClassApp_init->getPageLang($parLang);

//$parLang = isset($_GET['parLang'])? $_GET['parLang'] : '';
//
//switch (strtolower($parLang)) {
//	case 'en':
//		$intranet_hardcode_lang = 'en';
//		break;
//	default:
//		$intranet_hardcode_lang = 'b5';
//}

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");
include_once($PATH_WRT_ROOT."includes/libform.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libfiletable.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/eNoticePaymentReplySlip.php");
include_once($PATH_WRT_ROOT."lang/eclassapp_lang.".$_SESSION['intranet_session_language'].".php");

### Handle SQL Injection + XSS [START]
$NoticeID = IntegerSafe($NoticeID);
$CurUserID = IntegerSafe($CurUserID);
$CurID = IntegerSafe($CurID);
$StudentID = IntegerSafe($StudentID);
### Handle SQL Injection + XSS [END]

$UserID = $CurUserID;
$_SESSION['UserID'] = $CurUserID;

$delimiter = "###";
$keyStr = $special_feature['ParentApp_PrivateKey'].$delimiter.$NoticeID.$delimiter.$CurUserID;
if($token != md5($keyStr)) {
	include_once($PATH_WRT_ROOT."lang/lang.en.php");
	echo $i_general_no_access_right	;
	exit;
}

intranet_auth();
intranet_opendb();

$lform = new libform();
$lpayment = new libpayment();
$lnotice = new libnotice($NoticeID);
$lu = new libuser($CurID);
$eNoticePaymentReplySlip = new eNoticePaymentReplySlip();


$use_payment_system = $lpayment->isEWalletDirectPayEnabled();
$do_not_check_balance = $sys_custom['PaymentNoticeDoNotCheckBalance'] || $use_payment_system;


$lnotice->retrieveReply($StudentID);
$isTeacher = $lu->isTeacherStaff();
$temp_ans =  str_replace("&amp;", "&", $lnotice->answer);
$ansString = $lform->getConvertedString($temp_ans);
$ansString .= trim($ansString)." ";
$reqFillAllFields = $lnotice->AllFieldsReq;
$DisplayQNum = $lnotice->DisplayQuestionNumber;

//$sql3 = "select * from INTRANET_NOTICE_PIC as p where p.NoticeID='".$NoticeID."' and p.PICUserID='".$CurUserID."'";
//$result3 = $lnotice->returnArray($sql3);

if($type==0)
{
	$sql2 = "SELECT
		    iu.UserID, iu.EnglishName, iu.ChineseName,a.RecordStatus, iu.ClassNumber, yc.ClassTitleEN , yc.ClassTitleB5
        FROM
		    INTRANET_NOTICE_REPLY as a
		    Inner JOIN INTRANET_USER as iu ON a.StudentID = iu.UserID
		    Inner JOIN YEAR_CLASS_USER as ycu ON iu.UserID = ycu.UserID
			Inner JOIN YEAR_CLASS yc on ycu.YearClassID = yc.YearClassID
		    Inner JOIN YEAR y on y.YearID = yc.YearID
	    WHERE
		    a.NoticeID ='".$NoticeID."' and yc.AcademicYearID = '".Get_Current_Academic_Year_ID()."'
		ORDER BY 
		    y.Sequence,yc.Sequence, iu.ClassNumber ASC";
}
else if($type==1)
{
	$sql2 = "SELECT
		    iu.UserID, iu.EnglishName, iu.ChineseName,a.RecordStatus, iu.ClassNumber, yc.ClassTitleEN , yc.ClassTitleB5
        FROM
		    INTRANET_NOTICE_REPLY as a
		    Inner JOIN INTRANET_USER as iu ON a.StudentID = iu.UserID
		    Inner JOIN YEAR_CLASS_USER as ycu ON iu.UserID = ycu.UserID
			Inner JOIN YEAR_CLASS yc on ycu.YearClassID = yc.YearClassID
			Inner JOIN YEAR y on y.YearID = yc.YearID
	    WHERE
		    a.NoticeID ='".$NoticeID."' and yc.AcademicYearID = '".Get_Current_Academic_Year_ID()."'
		ORDER BY 
		    y.Sequence,yc.Sequence,iu.ClassNumber ASC";	
}
else if($type==2)
{
	$sql2 = "SELECT
		    iu.UserID, iu.EnglishName, iu.ChineseName,a.RecordStatus, iu.ClassNumber, yc.ClassTitleEN , yc.ClassTitleB5
        FROM
		    INTRANET_NOTICE_REPLY as a
		    Inner JOIN INTRANET_USER as iu ON a.StudentID = iu.UserID
		    Inner JOIN YEAR_CLASS_USER as ycu ON iu.UserID = ycu.UserID
			Inner JOIN YEAR_CLASS yc on ycu.YearClassID = yc.YearClassID
		    Inner JOIN YEAR_CLASS_TEACHER yct ON yc.YearClassID=yct.YearClassID and yct.UserID='".$CurUserID."'
		    Inner JOIN YEAR y on y.YearID = yc.YearID
	    WHERE
		    a.NoticeID ='".$NoticeID."' and yc.AcademicYearID = '".Get_Current_Academic_Year_ID()."'
		ORDER BY 
		    y.Sequence,yc.Sequence, iu.ClassNumber ASC";
}

$result1 = $lnotice->returnArray($sql2);

$studentlist = "<select onChange='show_notice_content(this.value)'>"; 
for($i=0; $i<sizeof($result1); $i++){
	$studentName = Get_Lang_Selection($result1[$i][2], $result1[$i][1]);	
	$className = Get_Lang_Selection($result1[$i][6], c);	
	if($result1[$i][0]==$StudentID){
	   $studentlist .= "<option value=".$result1[$i][0]." selected='selected'>".$studentName." (".$className." - ".$result1[$i][4].")</option>";	
	}
	else{
		$studentlist .= "<option value=".$result1[$i][0]." >".$studentName." (".$className." - ".$result1[$i][4].")</option>";	
	}
}
$studentlist .= "</select>"; 
$targetType = array("",$i_Notice_RecipientTypeAllStudents, $i_Notice_RecipientTypeLevel, $i_Notice_RecipientTypeClass, $i_Notice_RecipientTypeIndividual);

#modified by Kelvin Ho 2008-11-12 for ucc
if($lnotice->Module=='')
{
	$attachment = $lnotice->displayAttachment($targetBlank=true);
}
else
{
	if(strtoupper($lnotice->Module) == "PAYMENT")
		$attachment = $lnotice->displayAttachment($targetBlank=true);
	else
		$attachment = $lnotice->displayModuleAttachment();
}

// [2016-0728-1454-09073] use getNoticeContent() to get display content
// $this_Description = $lnotice->Description;
$this_Description = $lnotice->getNoticeContent($StudentID);

$MODULE_OBJ['title'] = $i_Notice_ElectronicNotice2;
$linterface = new interface_html("popup6.html");
$linterface->LAYOUT_START();
?>

<SCRIPT LANGUAGE=Javascript>
    jQuery(document).ready(function($) {
	   $('div#descriptionDiv').css('width', Get_Screen_Width() - 10);
	   textboxes = $("input:text");

		if ($.browser && $.browser.mozilla) {
			$(textboxes).keypress(checkForEnter);
		} else {
			$(textboxes).keydown(checkForEnter);
		}
		
		function checkForEnter(event) {
			if (event.keyCode == 13) {
				currentTextboxNumber = textboxes.index(this);
				
				if (textboxes[currentTextboxNumber + 1] != null) {
					nextTextbox = textboxes[currentTextboxNumber + 1];
					nextTextbox.select();
				}
				
				event.preventDefault();
				return false;
			}
		}
	});
	
    function show_notice_content(StudentID){
    	var div = document.getElementById("div1");
        var input = document.createElement("input");
        input.type = "hidden";
        input.id = "StudentID";
        input.name = "StudentID";
        input.value = StudentID;
        div.appendChild(input);
        
        document.form1.action = "notice_content.php";
        document.form1.submit();
    }
    
    function back(){
    	document.form1.action = "students_list.php";
        document.form1.submit(); 	
    }
</script>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<script src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery-1.11.1.min.js"></script>
<script src="<?=$PATH_WRT_ROOT?>templates/jquery.mobile/jquery.mobile.min.js"></script>
<link rel="stylesheet" href="<?=$PATH_WRT_ROOT?>templates/jquery.mobile/jquery.mobile.min.css">
<style type="text/css">
.tabletext {
	font-size: 16px;
}
.tabletext2 {
	font-size: 16px;
}
.tabletextremark {
	font-size: 16px;
}
#rcorners_green {
    border-radius: 5px;
    background: #74DF00;
    padding: 1px; 
    width: 200px;
    height: 150px; 
} 
tr.disabled_q, tr.disabled_q td, tr.disabled_q td.tabletext, tr.disabled_q td span.tabletextrequire {
	color: gray;
}
</style>

<script>
    $(function(){
        $('div#descriptionDiv a.ui-link').filter(function() {
            return ($(this).attr('href').indexOf('https://') != -1 || $(this).attr('href').indexOf('#') != -1);
        })
        .attr('rel', 'external');
    });
</script>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" />	
</head>
	<body>
	  <div data-role="page" id="pageone" style = "background-color:#FFFFFF">
	   <form id="form1" name="form1" method="get" > 
		<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr valign='top'>
							<td>
							 <table width="100%" style ="background-color:#429DEA;font-size:1em;font-weight:normal;">					
					             <tr>					
					                <td style="padding-left:5px;width:2em">
					                    <a href="javascript:back();"> <img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/iMail/app_view/white/icon_outbox_white.png" style="width:2em; height:2em;" /></a>
					                </td>
					                <td style="text-align:left;color:white;text-shadow:0px 0px 0px">
					                    <a href="javascript:back();"  > <span style="color: white;"><?=$Lang['eClassApp']['eNoticeS']['BackToList']?></span></a>	
					                </td>					
					                <td  align= "right" style="padding-right:0.5em;">
										<span><?=$studentlist?></span>										
					                </td>	              	
					              </tr>					
					            </table>								
							</td>
						</tr>
						<tr valign='top'>
							<td bgcolor='#E7E7E7' style="padding:10px;"><span class='tabletext'><h1><?=$lnotice->Title?></h1></span>
							</td>
						</tr>
						<tr valign='top'>
							<td bgcolor='#FFFFFF' style="padding-left:10px; padding-right:20px; padding-top:10px; font-size:18px;">
								<table border="0" cellpadding="0" cellspacing="0" style="width:100%; font-size:18px;">
									<tr>
										<td style="width:50%; vertical-align:top; font-size:18px;">
											<?=$lnotice->NoticeNumber?>
										</td>
										<td style="width:50%; text-align:right; vertical-align:bottom; font-size:18px;">
											<img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/icon_date.png" align="absmiddle" style="width:25px; height:25px;" />
											<span style="vertical-align:bottom;"><?=$lnotice->DateStart?></span>
										</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
										<td style="text-align:right; vertical-align:bottom; font-size:18px;">
											<img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/icon_deadline.png" align="absmiddle" style="width:25px; height:25px;" />
											<span style="vertical-align:bottom;"><font color="red"><?=$lnotice->DateEnd?></font></span>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr valign='top'>
							<td bgcolor='#FFFFFF' style="font-size:18px; padding:10px;">
								<div id="descriptionDiv" style="width: 100%; overflow-x: auto; overflow-y: hidden;">
									<span class='tabletext2'><?=str_replace("&nbsp;&nbsp;", "&nbsp; ", htmlspecialchars_decode($this_Description))?></span>
								</div>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		<div data-role="collapsible" data-collapsed="true" style="font-size:18px">
		   <h1><?=$Lang['General']['Misc']?></h1>
		   <div class="ui-grid-a">
				<? if ($attachment != "") { ?>
				     <div class="ui-block-a" style="width:100%; word-break:break-all;">
				       <span><strong><?=$i_Notice_Attachment?></strong></span>
				       <br>
				       <span><?=$attachment?></span>
				     </div>
				<? } ?>
				<? if(strtoupper($lnotice->Module) == "PAYMENT") { ?>
					<div class="ui-block-a" style="width:100%;">
						<br>
				       	<span><strong><?=$Lang['eNotice']['FieldTitle']['DebitMethod']['Title']?></strong></span>
						<br>
					<? if($lnotice->DebitMethod == 1){ ?>
						<span><?=$Lang['eNotice']['FieldTitle']['DebitMethod']['DebitDirectly']?></span>
					<? } else { ?>
						<span><?=$Lang['eNotice']['FieldTitle']['DebitMethod']['DebitLLater']?></span>
					<? } ?>
					</div>
				<? } ?>
				 
				<? if ($isTeacher) {
					$issuer = $lnotice->returnIssuerName();
				?>
				<div class="ui-block-a" style="width:100%;">
					<br>
					<span><strong><?=$i_Notice_Issuer?></strong></span>
					<br>
					<span><?=$issuer?></span>
				</div>
				<? } ?>
			
				<? 
			        if ($lnotice->replyStatus != 2) {
			            $statusString = "$i_Notice_Unsigned";
			        }
			        else {
			            $signer = $lu->getNameWithClassNumber($lnotice->signerID);
			            $signby = ($lnotice->replyType==1? $i_Notice_Signer:$i_Notice_Editor);
			            $statusString = "$i_Notice_Signed <I>($signby $signer $i_Notice_At ".$lnotice->signedTime.")</I>";
			        }
				?>
				
				<div class="ui-block-a" style="width:100%;">
					<br>
					<span><strong><?=$i_Notice_SignStatus?></strong></span>
					<br>
					<span><?=$statusString?></span>
				</div>
				
				<div class="ui-block-a" style="width:100%;">
					<br>
					<span><strong><?=$i_UserStudentName?></strong></span>
					<br>
					<span><?=$lu->getNameWithClassNumber($StudentID)?></span>
				</div>
				 
				<div class="ui-block-a" style="width:100%;">
					<br>
					<span><strong><?=$i_Notice_RecipientType?></strong></span>
					<br>
					<span><?=$targetType[$lnotice->RecordType]?></span>
				</div>
			</div>
			<br>
		 </div>
		 <? 
		  ### check include reply slip or not
		  $hasContent = (trim($lnotice->Question) || trim($lnotice->ReplySlipContent)) ? true : false;
		 ?>
		 <? if($hasContent) {?>
		 <!-- Break -->
		 <table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/eOffice/scissors_line.gif"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/eOffice/icons_scissors.gif"></td>
			</tr>
		 </table>
		 <? } ?>
		 
		 
		 <table id="html_body_frame2" width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td>
					<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
		            	<tr> 
		            		<td>                              
								<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
		        					<? if($hasContent) { ?>
		                                <!-- Relpy Slip -->
		                                <tr valign='top'>
											<td  class='eNoticereplytitle' align='center'><span class='tabletext2'><?=$i_Notice_ReplySlip?></td>
										</tr>
		
									<? if($lnotice->ReplySlipContent) {?>
											<!-- Content Base Start //-->
											<tr valign='top'>
												<td  >
												<?=$lnotice->ReplySlipContent?>
												</td>
											</tr>
									<? } else {
											if($lnotice->Module=='') {
												// Question and Answer
												$queString = str_replace("&amp;", "&", $lnotice->Question);
												$queString = preg_replace('(\r\n|\n)', "<br>", $queString);
												$queString = trim($queString)." ";
												$qAry = $lnotice->splitQuestion($queString, true);
												$aAry = $lnotice->parseAnswerStr($ansString);
												
												// Convert Question String for display
												$queString = $lform->getConvertedString($queString);
												
												// init array for js checking
												$optionArray = array();
												$optionArray[0] = array();
												$optionArray[1] = array();
												$mustSubmitArray = array();
                                                $isSkipQuestionArray = array();

												// loop Questions
												for($i=0; $i<count($qAry); $i++){
													$currentQuestion = $qAry[$i];
													$mustSubmitArray[] = $reqFillAllFields || $currentQuestion[3];
													$optionArray[0][] = $currentQuestion[4][0];
													$optionArray[1][] = $currentQuestion[4][1];
                                                    $isSkipQuestionArray[] = $mustSubmitArray[$i] && $optionArray[1][$i] != '' && ($optionArray[0][$i] != $optionArray[1][$i]);
													
													// for MC Questions
                                                    // [2020-0915-1022-58066]
                                                    //if($currentQuestion[0]==2 && $mustSubmitArray[$i])
													if($currentQuestion[0]==2)
													{
														$optionCount = count((array)$currentQuestion[4]);
														for($j=2; $j<$optionCount; $j++)
														{
															$optionArray[$j][$i] = $currentQuestion[4][$j];
														}

                                                        // [2020-0915-1022-58066] MC questions that required to be answered
                                                        $isSkipQuestionArray[$i] = false;
                                                        if($mustSubmitArray[$i])
                                                        {
                                                            if(count(array_unique($currentQuestion[4])) > 1) {
                                                                $mustSubmitArray[$i] = $optionCount;
                                                                $isSkipQuestionArray[$i] = true;
                                                            }
                                                        }
													}
												}

                                                // [2020-0826-1451-05073] use JS to handle Question Relation
												// $qAryRelation = $lnotice->handleQuestionRelation ($mustSubmitArray, $optionArray);
									?>
												<!-- Question Base Start //-->
												<tr valign='top'>
													<td colspan="2">
							                        <table width="100%" border="0" cellspacing="0" cellpadding="5">
							                        <tr><td>
							                                <form name="ansForm" method="post" action="update.php" onsubmit="return false;">
							                                	<table width='100%' border='0' cellspacing='0' cellpadding='5' align='center'>
																<tr><td>
																	<?= $lnotice->returnReplySlipContent("sign", $qAry, $aAry, $reqFillAllFields, $DisplayQNum, 1);?>
																</td></tr>
																</table>
						                                        <input type=hidden name="qStr" value="<?=$queString?>">
						                                        <input type=hidden name="aStr" value="">
						                                	</form>
						                                
							                                <SCRIPT LANGUAGE=javascript>
								                                var mustSubmitArr = new Array("", "<?= implode('","',(array)$mustSubmitArray); ?>");
								                                var option0SkipArr = new Array("", "<?= implode('","',(array)$optionArray[0]); ?>");
																var option1SkipArr = new Array("", "<?= implode('","',(array)$optionArray[1]); ?>");
                                                                var isSkipQuestionArr = new Array("", "<?= implode('","',(array)$isSkipQuestionArray); ?>");
																var relateArr = {};
																var totalQNum = <?=count($qAry)?>;
																<?
																    /*
																	foreach($qAryRelation as $qNum => $qOptions)
																	{
																		echo "relateArr[".$qNum."] = {};\n";
																		foreach($qOptions as $oNum => $qr)
																		{
																			echo "relateArr[".$qNum."][".$oNum."] = new Array(\"".implode("\",\"",(array)$qr)."\");\n";
																		}
																	}
																	*/

                                                                    foreach((array)$optionArray as $oNum => $qOptionArray)
                                                                    {
                                                                        echo "relateArr[".$oNum."] = {};\n";
                                                                        foreach((array)$qOptionArray as $qNum => $targetQ)
                                                                        {
                                                                            if($targetQ == '') {
                                                                                $targetQ = $optionArray[0][$qNum];
                                                                            }
                                                                            if($targetQ == '') {
                                                                                $targetQ = $qNum + 1;
                                                                            }
                                                                            echo "relateArr[".$oNum."][".$qNum."] = '$targetQ';\n";
                                                                        }
                                                                    }
																?>
																var skipCheckingToQ = 0;
						                                		
						                                		function checkAvailableOption(i, opt)
						                                		{
						                                			var i = parseInt(i);
                                                                    var targetQ = relateArr[opt][i-1];
                                                                    if(targetQ != 'end') {
                                                                        targetQ = parseInt(targetQ);
                                                                    }
                                                                    var originTargetQ = targetQ;
                                                                    var isJumpQ = false;
                                                                    var preTextOnlyQCount = 0;

                                                                    //var opt = parseInt(opt);
		                                							/*
						                                			var availableAry = relateArr[i-1];
						                                			if(availableAry==undefined)
						                                				return;
						                                				
						                                			availableAry = availableAry[opt];
						                                			if(availableAry==undefined){
						                                				availableAry = new Array("-1");
						                                			}
						                                			*/

						                                			for(var v=i+1; v<=totalQNum; v++){
						                                				var eleObj = eval("document.form1.F" + v);
						                                				if (eleObj){
							                                				if (eleObj.length){
																	       		switch(eleObj[0].type)
																	        	{
																	        		// Radio Button
																	        	    case "radio":	
															                        case "checkbox":
                                                                                        if(v >= (targetQ + 1) && targetQ != 'end')
                                                                                        {
										                                					$("input[name='F" + v + "']").checkboxradio('enable');
										                                					//$("#msg_"+v).attr('style', 'display:none');
										                                					$("span#msg_" + v).hide();
										                                					$("tr#row" + v).removeClass("disabled_q");

                                                                                            var isPreJumpQ = isJumpQ;
                                                                                            if((targetQ == originTargetQ || !isPreJumpQ) && v == (targetQ + 1 + preTextOnlyQCount)) {
                                                                                                isJumpQ = isSkipQuestionArr[v];
                                                                                                if(!isJumpQ) {
                                                                                                    targetQ = relateArr[0][v-1];
                                                                                                    if(targetQ != 'end') {
                                                                                                        targetQ = parseInt(targetQ);
                                                                                                    }
                                                                                                }
                                                                                            }
										                                				}
										                                				else{
										                                					$("input[name='F" + v + "']").checkboxradio('disable');
										                                					$("input[name='F" + v + "']").attr("checked", false).checkboxradio("refresh");
										                                					//$("#msg_"+v).attr('style', '');
										                                					$("span#msg_" + v).show();
										                                					$("tr#row" + v).addClass("disabled_q");
										                                				}
														                                break;
																	        	}
							                                				}
							                                				else
							                                				{
                                                                                if(v >= (targetQ + 1) && targetQ != 'end')
                                                                                {
								                                					$("input[name='F" + v + "'], textarea[name='F" + v + "']").each(function(){
								                                						$(this).textinput('enable');
								                                						//$("#msg_"+v).attr('style', 'display:none');
									                                					$("span#msg_" + v).hide();
									                                					$("tr#row" + v).removeClass("disabled_q");
																					});

                                                                                    var isPreJumpQ = isJumpQ;
                                                                                    if((targetQ == originTargetQ || !isPreJumpQ) && v == (targetQ + 1 + preTextOnlyQCount)) {
                                                                                        isJumpQ = isSkipQuestionArr[v];
                                                                                        if(!isJumpQ) {
                                                                                            targetQ = relateArr[0][v-1];
                                                                                            if(targetQ != 'end') {
                                                                                                targetQ = parseInt(targetQ);
                                                                                            }
                                                                                        }
                                                                                    }
								                                				}
								                                				else
                                                                                {
								                                					$("input[name='F" + v + "'], textarea[name='F" + v + "']").each(function(){
																					  	$(this).textinput('disable');
								                                						$(this).val('');
								                                						//$("#msg_"+v).attr('style', '');
									                                					$("span#msg_" + v).show();
									                                					$("tr#row" + v).addClass("disabled_q");
																					});
								                                				}
							                                				}

                                                                            preTextOnlyQCount = 0;
                                                                        } else {
                                                                            if(v >= (targetQ + 1) && targetQ != 'end') {
                                                                                preTextOnlyQCount = preTextOnlyQCount + 1;
                                                                            }
                                                                        }
						                                			}
						                                		}
						                                		
								                                function getAns(i)
								                                {
								                                	// get target answer
								       								var eleObj = eval("document.form1.F"+i);
						        									
						        									// init
															        var strAns = null;
															        var tempAns = null;
																	var canSkipChecking = skipCheckingToQ=='end' || skipCheckingToQ > 0 && i <= skipCheckingToQ;
																	var mustSubmitQ = mustSubmitArr[i];
																	
																	// reutn if required answer not exist
															        if (eleObj==undefined)
															        {
															            return '';
															        }
																	
															        if (eleObj.length)
															        {
															        	if(eleObj[0].disabled==false)
															        	{
																        	switch(eleObj[0].type)
																        	{
																        		// Radio Button
																        	    case "radio":
																        			// check is T/F Question
																        			var isTFQ = false;
																		        	if(document.getElementById("typeOf"+i))
																		        	{
																		        		isTFQ = document.getElementById("typeOf"+i).value==1;
																		        	}
																		        	
													                                for (p=0; p<eleObj.length; p++){
												                                        if (eleObj[p].checked)
												                                        {
											                                                tempAns += p;
											                                                
											                                                // Update skipCheckingToQ for T/F Question 
											                                                if(isTFQ && p=="0" && !canSkipChecking){
											                                                	if (typeof option0SkipArr[i] != "undefined")
											                                                	{
											                                                		skipCheckingToQ = option0SkipArr[i];
											                                                	} 
											                                                }
											                                                else if(isTFQ && p=="1" && !canSkipChecking){
											                                                	if (typeof option1SkipArr[i] != "undefined")
											                                                	{
											                                                		skipCheckingToQ = option1SkipArr[i];
											                                                	}
											                                                }
												                                        }
													                                }
													                                
													                                // check empty input
													                                if ((need2checkform || mustSubmitQ==1) && tempAns == null && !canSkipChecking)
													                                {
													                                    formAllFilled = false;
													                                    return false;
													                                }
																					
													                                if (tempAns != null)
													                                {
													                                    strAns = tempAns;
													                                }
													                                break;
																				
																				// CheckBox
														                        case "checkbox":
													                                for (p=0; p<eleObj.length; p++) {
												                                        if (eleObj[p].checked)
												                                        {
												                                            if (tempAns != null)
												                                            {
												                                                tempAns += ","+p;
												                                            }
												                                            else
												                                            {
												                                                tempAns = p;
												                                            }
												                                        }
													                                }
													                                
													                                // check empty input
													                                if ((need2checkform || mustSubmitQ==1) && tempAns == null && !canSkipChecking)
													                                {
													                                    formAllFilled = false;
													                                    return false;
													                                }
													                                strAns = tempAns;
													                                break;
																				
														                        case "text":
													                                for (p=0; p<eleObj.length; p++){
													                                	tempAns += recurReplace('"', '&quot;', eleObj.value);
													                                }
													                                
																	        		// check empty input
													                                if ((need2checkform || mustSubmitQ==1) && (tempAns == null || tempAns=='') && !canSkipChecking)
													                                {
													                                    formAllFilled = false;
													                                    return false;
													                                }
													                                strAns = tempAns;
													                                break;                  	
															                }
															        	}
															        	else
															        	{
															        		if(eleObj[0].type=='text')
															        		{
												                                strAns = '';
															        		}
															        	}
														        	}
														        	// Text Field
														        	else
														        	{
														        		if(eleObj.disabled==false)
														        		{
															                tempAns = recurReplace('"', '&quot;', eleObj.value);
															                
															        		// check empty input
															                if ((need2checkform || mustSubmitQ==1) && (tempAns == null || tempAns=='') && !canSkipChecking)
															                {
															                    formAllFilled = false;
															                    return false;
															                }
															                strAns = tempAns;
															       		}
															       		else{
															       			strAns = '';
															       		}
														        	}
														        	return strAns;
														  		}
															  	
								                                function copyback()
								                                {
									                                // reset parms
								                                	txtStr = '';
								                                	formAllFilled = true;
								                                	skipCheckingToQ = 0;
								                                	
								                                   	// loop table rows
								                                    var tableArr = $("#ContentTable")[0].tBodies[0].rows;
														            for (var x=0; x<tableArr.length; x++)
														            {
														            	// get answers
														            	temp = getAns(x+1);
														            	
														            	// return false if required question not filled
													                    if (formAllFilled==false)
													                    {
													                        return false;
													                    }
													                    
													                    // append answer to txtStr
																		txtStr+="#ANS#"+temp;
														           	}
														           	
														           	// update aStr value
															        document.form1.aStr.value=txtStr;
									                                document.form1.qStr.value = document.form1.qStr.value;
									                                document.form1.aStr.value = document.form1.aStr.value;
								                                }
				                                
								                                function updateAllAvailableOption(){
								                               		// get T/F Question that involve skip action
								                                	var possibleTF = $('input[onclick]:checked');
								                                	if(possibleTF.length){
								                                		for(var TFCount=0; TFCount<possibleTF.length; TFCount++)
								                                		{
								                                			var currentTFQ = possibleTF[TFCount]
								                                			var TFQNum = currentTFQ.name.replace("F", "");
								                                			var TFQOption = currentTFQ.value;
								                                			
									                                		// perform to disable fields not related to answered question
																			if(!currentTFQ.disabled){
								                                				checkAvailableOption(TFQNum, TFQOption);
								                                			}
								                                		}
								                                	}
								                                }
								                                
								                               	$(document).ready(function(){
									                               	// update all options
								                               		updateAllAvailableOption();
								                                });
									                  		</SCRIPT>
						                            </td></tr>
						                            </table>
						                    		</td>
												</tr>
												<!-- Question Base End //-->
									<? 		} 
											else 
											{
												$temp_que =  str_replace("&amp;", "&", $lnotice->Question);
												$queString = $lform->getConvertedString($temp_que);
												$queString =trim($queString)." ";
												
												
												$result = $lpayment->checkBalance($StudentID,1);
												$finalBalance = str_replace(",","",$result[0]);
												
												$student_subsidy_identity_id = '';
												if($StudentID != '' && $StudentID > 0)
												{
													$student_subsidy_identity_records = $lpayment->getSubsidyIdentityStudents(array('StudentID'=>$StudentID,'DateWithinEffectiveDates'=>$lnotice->DateStart));
													if(count($student_subsidy_identity_records)>0){
														$student_subsidy_identity_id = $student_subsidy_identity_records[0]['IdentityID'];
													}
												}
									?>
												
												<!-- Question Base Start //-->
												<tr valign='top'>
													<td>
			                    						<table width="100%" border="0" cellspacing="0" cellpadding="0">
			                    							<tr>
			                    								<td>
			                    									
										                        	<? if(strtoupper($lnotice->Module) == "PAYMENT") { ?>
																		<script type="text/javascript" language="javascript" src="/templates/forms/eNoticePayment_replyslip.js?t=<?=time()?>"></script>
																		<script type="text/javascript" language="javascript" src="/templates/jquery/jquery.tablednd_0_5.js"></script>
																	<? } else { ?>
																		<script language="javascript" src="/templates/forms/layer.js"></script>
																		<script language="javascript" src="/templates/forms/form_edit.js"></script>
																	<? } ?>	
			                    
									                                <form name="ansForm" method="post" action="update.php" onsubmit="return false;">
									                                        <input type=hidden name="qStr" value="">
									                                        <input type=hidden name="aStr" value="">
									                                </form>
			                            							<div id="ReplySlipContainer"></div>
			                            							<script language="Javascript">
										                            //$(document).ready(function(){
			                                                       		var options = {
																			"QuestionString":"<?=$queString?>",
																			"AnswerString":"<?=$ansString?>",
																			"AccountBalance":<?=$finalBalance?>,
																			"DoNotShowBalance":<?=$do_not_check_balance?'1':'0'?>,
																			"PaymentCategoryAry":[],
																			"TemplatesAry":[],
																			"DisplayQuestionNumber":<?=$lnotice->DisplayQuestionNumber?1:0?>,
																			"SubsidyStudentID":'<?=IntegerSafe($StudentID)?>',
																			"SubsidyIdentityID":'<?=$student_subsidy_identity_id?>',
																			<?php
																			$words = $eNoticePaymentReplySlip->getRequiredWords();
																			foreach($words as $key => $value){
																				echo '"'.$key.'":"'.$value.'",'."\n";
																			}
																			?>
																			"Debug":false
																		};
																		var sheet = new Answersheet(options);
																		sheet.renderFillInPanel('ReplySlipContainer');
										                            //});
									                                </script>
									                                
									                                <SCRIPT LANGUAGE=javascript>
									                                function copyback()
									                                {
									                                         finish();
									                                         document.form1.qStr.value = document.ansForm.qStr.value;
									                                         document.form1.aStr.value = document.ansForm.aStr.value;
									                                }
									                                </SCRIPT>
			                    								</td>
															</tr>
			                							</table>
			                						</td>
												</tr>
												<!-- Question Base End //-->
										<? } ?>
									<? } ?>
								<? } ?>
		
		            			</table>
		            		</td>
		            	</tr>
		            </table>  
				</td>
			</tr>
		</table>
		<input type="hidden" id="NoticeID" name="NoticeID" value="<?=$NoticeID?>">
		<input type="hidden" id="CurUserID" name="CurUserID" value="<?=$CurUserID?>">
		<input type="hidden" id="token" name="token" value="<?=$token?>">
		<input type="hidden" id="parLang" name="parLang" value="<?=$parLang?>">
		<input type="hidden" id="type" name="type" value="<?=$type?>">
	    <div id="div1">
	    </div>	
	   </form>
	 </div> 
  </body>
</html>

<?php
intranet_closedb();
?>