<?php
# using: 

################## Change Log [Start] ##############
#
#	Date:	2020-10-08 YatWoon
#			Improved: Allow display user email as sender ($special_feature['DisplayUserEmailSender']['eNotice']) [Case#2020-0609-1346-18073]
#
# 	Date:   2020-09-07 (Bill)    [2020-0604-1821-16170]
#			access right checking > replace hasIssueRight() by hasSchoolNoticeIssueRight()
#
#	Date:	2020-04-15  Philips [2020-0310-1051-05235]
#			added SpecialNotice for $sys_custom['eNotice']['SpecialNoticeForPICAndAdmin']
#
#	Date:	2019-10-28  Philips [2019-1009-1010-07206]
#			added handleImageOrientation() to handle image orientation
#
#   Date:   2019-09-19  Bill    [2019-0917-1522-01206]
#           skip student status checking when send push message to parents
#
#   Date:   2019-04-30  Bill
#           prevent SQL Injection + Cross-site Scripting
#
#   Date:   2018-10-10  Bill    [2018-1008-1030-09073]
#           - for push message notification to PIC & Class Teacher
#           fixed: cannot send notification to both PIC & Class Teacher
#           fixed: cannot delete old scheduled notification when edit notice later (using 'eNoticeS' instead of '')
#
#   Date:   2018-09-10  Philips
#           - add email notify function for PIC and Class Teachers, sus_status == 4 save emailnotify_PIC and emailnotify_ClassTeachers
#
#	Date:	2018-09-10	Tiffany
#			Disable the enotice jump link for push message to teacher app if pushmessagenotify_PIC and pushmessagenotify_ClassTeachers
#
#   Date:   2018-05-21 Philips
#           Added pushmessagenotify_PIC & pushmessagenotify_ClassTeachers
#
#	Date:	2017-06-09	Bill	[2017-0607-1442-37206]
#			fixed: prevent preceding space added to recipient id when using merge content mode
#
#	Date:	2016-10-17	Villa
#			update pushmessagenotify, pushmessagenotify_student every new record
#
#	Date:	2016-07-18	Tiffany
#			- add push message to student app
#
#	Date:	2016-02-17	Bill	[2016-0217-1403-50207]
#			fixed: release push message even if notice cannot created
#			- redirect to index.php with error message if $NoticeID is empty
#
#	Date:	2015-12-11	Bill
#			- replace session_unregister() by session_unregister_intranet() for PHP 5.4
#
#	Date:	2015-11-12	Ivan [ip.2.5.7.1.1.0]
#			added send push message by batches logic
#
#	Date:	2015-10-28	Bill	[2015-0416-1040-06164]
#			support merge notice content
#
#	Date:	2015-06-10	Roy
#			set $sendTimeMode and $sendTimeString for scheduled push message
#
#	Date:	2015-04-14 	Bill	[2015-0323-1602-46073]
#			- import UserID of PICs to INTRANET_NOTICE_PIC
#
#	Date:	2015-04-02 Omas	[Case #J73013]
#			- enabled send module mail new footer
#
#	Date:	2014-12-01	Bill [Case #P70566]
#			- get noticeAttFolder from $_POST 
#
#	Date:	2014-11-19 Bill
#			Fixed: Type = Whole School exclude left student
#
#	Date:	2014-10-13 Roy	> ip.2.5.5.10.1
#			Improved: allow send push message to parent
#
#	Date:	2014-06-23 YatWoon	> ip.2.5.5.8.1
#			Improved: Revised system return message method [Case#Q63447]
#
#	Date:	2013-09-13	YatWoon
#			add OriginalAttachment field to keep track lasalle problem, store the attachment path which generate from new process only
#
#	Date:	2013-09-06	YatWoon
#			Improved: update the upload attachment method
#
#	Date:	2012-06-20 YatWoon
#			Fixed: If edit the reply slip again, the question with "enter" will become "<br>"
#
#	Date:	2011-12-14	YatWoon
#			improved: allow select suspended student [Case#2011-1202-1604-57066]
#
#	Date:	2011-08-25	Yuen
#			handled description for iPad/Andriod
#
#	Date:	2011-03-25	YatWoon
#			change email notification subject & content data 
#
#	Date:	2011-02-16	YatWoon
#			Add option "Display question number"
#
#   Date:	2010-10-25 Ronald
#			modified send mail notification, now can select send to parent and student spreatly
#
#   Date:	2010-03-24	YatWoon
#		    Add "All questions are required to be answered" option
#
################## Change Log [End] ##############

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libfiletable.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libemail.php");
include_once($PATH_WRT_ROOT."includes/libsendmail.php");
// include_once($PATH_WRT_ROOT."lang/email.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp.php");

intranet_auth();
intranet_opendb();

### Handle SQL Injection + XSS [START]
$ContentType = IntegerSafe($ContentType);
$MergeFormat = IntegerSafe($MergeFormat);
$MergeType = IntegerSafe($MergeType);

$type = IntegerSafe($type);
$sus_status = IntegerSafe($sus_status);
$DisplayQuestionNumber = IntegerSafe($DisplayQuestionNumber);

$attachment_size = IntegerSafe($attachment_size);

$noticeType = IntegerSafe($noticeType);
$status = IntegerSafe($status); 
$year = cleanCrossSiteScriptingCode($year);
$month = cleanCrossSiteScriptingCode($month);
### Handle SQL Injection + XSS [END]

$lnotice = new libnotice();
$lf = new libfilesystem();
$libeClassApp = new libeClassApp();

// [2020-0604-1821-16170]
//if ($lnotice->disabled || !$lnotice->hasIssueRight())
if ($lnotice->disabled || !$lnotice->hasSchoolNoticeIssueRight())
{
     header ("Location: /");
     intranet_closedb();
     exit();
}

$NoticeNumber = intranet_htmlspecialchars(trim($NoticeNumber));
$Title = intranet_htmlspecialchars(trim($Title));
$pushMsgNoticeNumber = standardizeFormPostValue($_POST['NoticeNumber']);
$pushMsgTitle = standardizeFormPostValue($_POST['Title']);
if ($userBrowser->platform=="iPad" || $userBrowser->platform=="Andriod")
{
	if ($Description==strip_tags($Description))
	{
		$Description = nl2br($Description);
	}
}
$Description = intranet_htmlspecialchars(trim($Description));
// $qStr = intranet_htmlspecialchars(trim($qStr));
$qStr = $lnotice->Get_Safe_Sql_Query(standardizeFormPostValue($qStr));
$AllFieldsReq = $AllFieldsReq==1 ? 1 : 0;
$DisplayQuestionNumber = $DisplayQuestionNumber==1 ? 1 : 0;

$luser = new libuser($UserID);

$noticeAttFolder_name = $_POST['noticeAttFolder'];
//$path = "$file_path/file/notice/".$noticeAttFolder;
$path = "$file_path/file/notice/".$noticeAttFolder_name;

# Delete Files
$file2delete = array_filter(explode(":",$deleted_files));
if (sizeof($file2delete) != 0){
	for ($i=0; $i<sizeof($file2delete); $i++)
	{
		$f = urldecode($file2delete[$i]);
		if(trim($f)=='') continue;
		
		$del_file = $path."/".$f;
		$lf->lfs_remove($del_file);
	}
}

# Upload Files
$attachment_size = $attachment_size==""? 0 : $attachment_size;
$firstRotate = 0;
for ($i=0; $i<$attachment_size; $i++)
{
	$key = "filea$i";
	$loc = ${"filea$i"};
	$file = stripslashes(${"hidden_userfile_name$i"});
	$des = "$path/$file";
	if ($loc == "none" || $loc=="")
	{} 
	else
	{
		if (strpos($file,"."==0)){
		} 
		else{
			$lf->lfs_copy($loc, $des);
			$filepath = $des;
			$filetype = strtolower(pathinfo($des, PATHINFO_EXTENSION));
            if($filetype=="jpg"||$filetype=="jpeg"||$filetype=="png")
           {
           	$firstRotate = handleImageOrientation($filepath, $filetype, $firstRotate);
           }
		}
	}
}
//$Attachment = $noticeAttFolder;
$Attachment = $noticeAttFolder_name;
/*
$AttachmentStr = (sizeof($Attachment)==0) ? "" : implode(",", $Attachment);
$AttachmentStr = stripslashes($AttachmentStr);
$path = "$file_path/file/notice/".$noticeAttFolder;
$lu = new libfilesystem();
$command ="mv $path"."tmp $path";

exec($command);

		if($bug_tracing['notice_attachment_log']){
             $temp_log_filepath = "$file_path/file/log_notice_attachment.txt";
             $temp_time = date("Y-m-d H:i:s");
             $temp_user = $UserID;
             $temp_page = 'notice_new_update.php';
             $temp_action=$command;
             $temp_content = get_file_content($temp_log_filepath);
             $temp_logentry = "\"$temp_time\",\"$temp_user\",\"$temp_page\",\"$temp_action\"\n";
             $temp_content .= $temp_logentry;
             write_file_content($temp_content, $temp_log_filepath);
		 }

$lo = new libfiletable("", $path, 0, 0, "");
$files = $lo->files;
while (list($key, $value) = each($files)) {
     if(!strstr($AttachmentStr,$files[$key][0])){
	     	if($bug_tracing['notice_attachment_log']){
             $temp_log_filepath = "$file_path/file/log_notice_attachment.txt";
             $temp_time = date("Y-m-d H:i:s");
             $temp_user = $UserID;
             $temp_page = 'notice_new_update.php';
             $temp_action="remove file:".$path."/".$files[$key][0];
             $temp_content = get_file_content($temp_log_filepath);
             $temp_logentry = "\"$temp_time\",\"$temp_user\",\"$temp_page\",\"$temp_action\"\n";
             $temp_content .= $temp_logentry;
             write_file_content($temp_content, $temp_log_filepath);
		 }
          $lu->file_remove($path."/".$files[$key][0]);
     }
}
$IsAttachment = (isset($Attachment)) ? 1 : 0;
$Attachment = $noticeAttFolder;

if ($IsAttachment)
{
}
else
{
    if ($noticeAttFolder != ""){
	    if($bug_tracing['notice_attachment_log']){
             $temp_log_filepath = "$file_path/file/log_notice_attachment.txt";
             $temp_time = date("Y-m-d H:i:s");
             $temp_user = $UserID;
             $temp_page = 'notice_new_update.php';
             $temp_action="remove path:".$path;
             $temp_content = get_file_content($temp_log_filepath);
             $temp_logentry = "\"$temp_time\",\"$temp_user\",\"$temp_page\",\"$temp_action\"\n";
             $temp_content .= $temp_logentry;
             write_file_content($temp_content, $temp_log_filepath);
             $temp_cmd = "mv $path ".$path."_bak";
        	 exec($temp_cmd);

		 }else{
        	 $lu->lfs_remove($path);
         }
    }
}
*/

if (isset($target) && sizeof($target)!=0)
    $targetID = implode(",",$target);

if($ContentType==2) {
	$type = 4;
}

# Store to DB
$IssueUserName = $luser->getNameForRecord();
$DateStart = $DateStart.' '.$DateStartTime;
$DateEnd = $DateEnd.' '.$DateEndTime;
$fieldname = "NoticeNumber,Title,Description,DateStart,DateEnd,IssueUserID,IssueUserName";
$fieldname .= ",RecipientID,Question,Attachment,RecordType,RecordStatus,DateInput,DateModified,AllFieldsReq,DisplayQuestionNumber";
$fieldname .= ", OriginalAttachment";
$fieldname .= ",ContentType,MergeFormat,MergeType,MergeFile";
$values = "'$NoticeNumber','$Title','$Description','$DateStart','$DateEnd','$UserID','$IssueUserName'";
$values .= ",'$targetID','".$qStr."','$Attachment','$type','$sus_status',now(),now(),'$AllFieldsReq','$DisplayQuestionNumber'";
$values .= ",'$Attachment'";

// insert merge related fields
if($ContentType==2) {
	$values .= ",'$ContentType','$MergeFormat','$MergeType','".stripslashes($FileCSV_Name)."'";
}
else {
	$values .= ",'$ContentType',NULL,NULL,NULL";
}
if($sus_status==1){
	$fieldname .= ',ApprovedBy,ApprovedTime ';
	$values .= ",'$UserID',now() ";
}
if($sys_custom['eNotice']['SpecialNoticeForPICAndAdmin']){
	$SpecialNotice = $SpecialNotice ? $SpecialNotice : '0';
	$fieldname .= ",SpecialNotice";
	$values .= ",'$SpecialNotice'";
}

$sql = "INSERT INTO INTRANET_NOTICE ($fieldname) VALUES ($values)";
$lnotice->db_db_query($sql) or dir(mysql_error());
$NoticeID = $lnotice->db_insert_id();
//debug_pr($NoticeID);
// [2016-0217-1403-50207] redirect to index.php if $NoticeID is empty
if(empty($NoticeID)){
	header("Location: index.php?xmsg=AddUnsuccess&pageNo=$pageNo&field=$field&page_size_change=$page_size_change&numPerPage=$numPerPage&noticeType=$noticeType&status=$status&year=$year&month=$month&NoticeID=$NoticeID");
    intranet_closedb();
	exit();
}

// [2015-0416-1040-06164] Content Type: Merge Content
// upload csv file for merge notice
if($ContentType==2 && $FileCSV_Name!="")
{
	// get file
	$mergefile = stripslashes($FileCSV_Name);
	$mergeloc = $FileCSV;
	if ($mergeloc == "none" || $mergeloc=="")
	{
		// do nothing
	} 
	else
	{
		if (!strpos($mergefile,"."==0)){
			$mergepath = "$file_path/file/mergenotice";
			$lf->folder_new($mergepath);
			
			$mergepath .= "/".ceil($NoticeID/10000);
			$lf->folder_new($mergepath);
			
			$mergepath .= "/".$NoticeID;
			$lf->folder_new($mergepath);
			
			// copy file to destination
			$mergedes = "$mergepath/$mergefile";
           	$lf->lfs_copy($mergeloc, $mergedes);
		}
	}
}
// [2015-0416-1040-06164] remove preview temp file
if($previewCSV=="1")
{
	$del_previewfile = "$file_path/file/mergenotice/temp/".$UserID;
	$lf->lfs_remove($del_previewfile);
}

# [2015-0323-1602-46073] - Add PICs to DB
if(isset($target_PIC) && sizeof($target_PIC) != 0)
{
	$fieldname = "NoticeID,PICUserID,InputBy,DateInput,ModifiedBy,DateModified";
	$values = "";
	
	$delim = "";
	$target_PIC = array_unique($target_PIC);
	for($i=0; $i<sizeof($target_PIC); $i++) {
		$this_PIC = $target_PIC[$i];
		$this_PIC = str_replace("&#160;", "", $this_PIC);
		$this_PIC = str_replace("U", "", $this_PIC);
		if(trim($this_PIC) != "") {
			$values .= "$delim ('$NoticeID','$this_PIC','$UserID',now(),'$UserID',now())";
			$delim = ",";
		}
	}
	
	$sql = "INSERT INTO INTRANET_NOTICE_PIC ($fieldname) VALUES $values";
	$lnotice->db_db_query($sql);
}

## Update description if user upload image with Flash upload (fck) [Start]
$lnotice->updateNoticeFlashUploadPath($NoticeID,$Description);
## Update description if user upload image with Flash upload (fck) [End]	
$username_field = getNameFieldWithClassNumberEng("");

if ($ContentType == "2") {
	$lnotice = new libnotice($NoticeID);
	$original_audience = array();
	$new_audience = $lnotice->getNoticeCSVUserInfo();
	$lnotice->rebuildNoticeReply($NoticeID, $original_audience, $new_audience);
	
	if (count($new_audience) > 0) {
		$strRecipientID = "U" . implode(",U", $new_audience) . "";
		$strSQL = "UPDATE INTRANET_NOTICE SET RecipientID='" . $strRecipientID. "' WHERE NoticeID='" . $NoticeID . "'";
		$lnotice->db_db_query($strSQL);
		$lnotice->RecipientID = $strRecipientID;
	} else {
		$strSQL = "UPDATE INTRANET_NOTICE SET RecipientID='' WHERE NoticeID='" . $NoticeID . "'";
		$lnotice->db_db_query($strSQL);
		$lnotice->RecipientID = "";
	}
	
} else {
	if ($type==1)         # Whole School
	{
	    /*
	    $sql = "INSERT IGNORE INTO INTRANET_NOTICE_REPLY (NoticeID,StudentID,StudentName,RecordStatus,DateInput,DateModified)
	            SELECT $NoticeID,UserID,$username_field,0,now(),now() FROM INTRANET_USER WHERE RecordType = 2 AND RecordStatus = 1";
	            $lnotice->db_db_query($sql);
	            */
	            $username_field = getNameFieldWithClassNumberEng("iu.");
	            $sql = "select 
							ycu.UserID, $username_field
						from
							YEAR as y 
							INNER JOIN YEAR_CLASS as yc on (yc.YearID = y.YearID and yc.AcademicYearID = ". Get_Current_Academic_Year_ID() .")
							INNER JOIN YEAR_CLASS_USER as ycu on (ycu.YearClassID = yc.YearClassID)
							INNER JOIN INTRANET_USER as iu on (iu.UserID = ycu.UserID)
						WHERE iu.RecordType = 2 AND iu.RecordStatus in (0,1)
	            		";
	            $actual_target_users = $lnotice->returnArray($sql);
	            
	     if (sizeof($actual_target_users)!=0)
	     {
	         $delimiter = "";
	         $values = "";
	         for ($i=0; $i<sizeof($actual_target_users); $i++)
	         {
	              list($uid,$name,$usertype) = $actual_target_users[$i];
	              $values .= "$delimiter ($NoticeID,$uid,'". addslashes($name) ."',0,now(),now())";
	              $delimiter = ",";
	         }
	         $sql = "INSERT IGNORE INTO INTRANET_NOTICE_REPLY (NoticeID,StudentID,StudentName,RecordStatus,DateInput,DateModified)
	                 VALUES $values";
			$lnotice->db_db_query($sql);
	     }
	    
	}
	else if ($type==2)    # Some levels only
	{
	     # Grab the class name for the levels
	     $list = implode(",",$target);
	     //$sql = "SELECT ClassName FROM INTRANET_CLASS WHERE ClassLevelID IN ($list)";
	     $sql = "
		     		SELECT 
		     			yc.ClassTitleEN 
		     		FROM 
		     			YEAR as y 
		     			INNER JOIN YEAR_CLASS as yc on (yc.YearID = y.YearID and yc.AcademicYearID = ". Get_Current_Academic_Year_ID() .")
		     		WHERE 
		     			y.YearID IN ($list)
	     		";
	     $classes = $lnotice->returnVector($sql);
	     $classList = "'".implode("','",$classes)."'";
	     $sql = "INSERT IGNORE INTO INTRANET_NOTICE_REPLY (NoticeID,StudentID,StudentName,RecordStatus,DateInput,DateModified)
	             SELECT $NoticeID,UserID,$username_field,0,now(),now() FROM INTRANET_USER WHERE RecordType = 2 AND RecordStatus in (0,1) AND ClassName IN ($classList)";
	     $lnotice->db_db_query($sql);
	}
	else if ($type==3)    # Some classes only
	{
	     # Grab the class name for the classes
	     $list = implode(",",$target);
	     //$sql = "SELECT ClassName FROM INTRANET_CLASS WHERE ClassID IN ($list)";
	     $sql = "SELECT ClassTitleEN FROM YEAR_CLASS WHERE YearClassID IN ($list)";
	     $classes = $lnotice->returnVector($sql);
	     $classList = "'".implode("','",$classes)."'";
	     if($classList)
	     {
	     	$sql = "INSERT IGNORE INTO INTRANET_NOTICE_REPLY (NoticeID,StudentID,StudentName,RecordStatus,DateInput,DateModified)
	             SELECT $NoticeID,UserID,$username_field,0,now(),now() FROM INTRANET_USER WHERE RecordType = 2 AND RecordStatus in (0,1) AND ClassName IN ($classList)";
	     	$lnotice->db_db_query($sql);
	 	}
	}
	else if ($type == 4)  # Some only
	{
		
	     $actual_target_users = $lnotice->returnTargetUserIDArray($targetID, "0,1");
	     if (sizeof($actual_target_users)!=0)
	     {
	         $delimiter = "";
	         $values = "";
	         for ($i=0; $i<sizeof($actual_target_users); $i++)
	         {
	              list($uid,$name,$usertype) = $actual_target_users[$i];
	              $values .= "$delimiter ('$NoticeID','$uid','". addslashes($name) ."',0,now(),now())";
	              $delimiter = ",";
	         }
	         $sql = "INSERT IGNORE INTO INTRANET_NOTICE_REPLY (NoticeID,StudentID,StudentName,RecordStatus,DateInput,DateModified)
	                 VALUES $values";
	         $lnotice->db_db_query($sql);
	     }
	}
}
# Send email notification
if ($sus_status==1 && (($emailnotify==1) || ($emailnotify_Students==1) || ($emailnotify_PIC == 1 && sizeof($target_PIC) != 0) || ($emailnotify_ClassTeachers == 1) || ($pushmessagenotify == 1) || ($pushmessagenotify_Students == 1) || ($pushmessagenotify_PIC == 1) || ($pushmessagenotify_ClassTeachers == 1)))
{
	$lwebmail = new libwebmail();
	
    $sql = "SELECT DISTINCT StudentID FROM INTRANET_NOTICE_REPLY WHERE NoticeID = '$NoticeID'";
    $students = $lnotice->returnVector($sql);
    
    $student_list = implode(",",$students);
    $sql = "SELECT DISTINCT b.UserID
                  FROM INTRANET_PARENTRELATION as a
                       LEFT OUTER JOIN INTRANET_USER as b ON a.ParentID = b.UserID
                  WHERE a.StudentID IN ($student_list) ";
    $parent = $lnotice->returnVector($sql);
    
    if(($emailnotify==1) && ($emailnotify_Students==1)) {
    	$ToArray = array_merge($students,$parent);
    } else if($emailnotify==1) {
    	$ToArray = $parent;
    } else if($emailnotify_Students==1) {
    	$ToArray = $students;
    } else {
        $ToArray = array();
    }
    
    if($emailnotify_PIC == 1 && sizeof($target_PIC) != 0) {
        $PICs = array();
        $target_PIC = array_unique($target_PIC);
        foreach($target_PIC as $tPICID) {
            $tPICid = $tPICID;
            $tPICid = str_replace("&#160;", "", $tPICid);
            $tPICid = str_replace("U", "", $tPICid);
            
            $PICs[] = $tPICid;
        }
        $ToArray = array_merge($ToArray, $PICs);
    }
    
    if($emailnotify_ClassTeachers == 1) {
        require_once ($PATH_WRT_ROOT."includes/libclass.php");
        $libclass = new libclass();
        
        $teacherArr = array();
        foreach ($students as $student) {
            $class = $libclass->returnCurrentClass($student);
            $teachers = $libclass->returnClassTeacherID($class[0]['ClassName']);
            foreach ($teachers as $teacher) {
                array_push($teacherArr, $teacher['UserID']);
            }
        }
        $teacherArr = array_unique($teacherArr);
        
        //debug_pr($teacherArr);
        $ToArray = array_merge($ToArray, $teacherArr);
    }
    //debug_pr($ToArray);die();
    
    if ($emailnotify==1 || ($emailnotify_Students==1) || ($emailnotify_PIC == 1 && sizeof($target_PIC) != 0) || ($emailnotify_ClassTeachers == 1)) {
	    
		$DisplayUserEmailSender = 1;
	    if($special_feature['DisplayUserEmailSender']['eNotice'])
	    {
			$sender_mail = $lwebmail->GetUserEmailAddress($UserID, 1);
	    	$DisplayUserEmailSender = 0;
	    }
		
		list($email_subject, $email_body) = $lnotice->returnEmailNotificationData($DateStart,$DateEnd,$pushMsgTitle,$sender_mail);
	    $lwebmail->sendModuleMail($ToArray,$email_subject,$email_body,$DisplayUserEmailSender,'','User',true);
    }
    
    # Send push message
    if ($pushmessagenotify == 1 || $pushmessagenotify_Students == 1 || $pushmessagenotify_PIC == 1 || $pushmessagenotify_ClassTeachers == 1)
    {
		$isPublic = "N";
		$appType = $eclassAppConfig['appType']['Parent'];
		$appType_s = $eclassAppConfig['appType']['Student'];
		$appType_t = $eclassAppConfig['appType']['Teacher']; // For PIC and Class Teacher
//		$sendTimeMode = "";
		$sendTimeMode = standardizeFormPostValue($_POST['sendTimeMode']);
//		$sendTimeString = "";
		$sendTimeString = standardizeFormPostValue($_POST['sendTimeString']);
		
		//Logic if sendTimeMode == 'now', 
		//	check is issue day is in future 
		//		-> yes:  sendTimeMode -> 'scheduled' and sendTimeString -> issueDate
// 		$now = date('Y-m-d h:i:s');
// 		if($sendTimeMode=='now'){
// 			if(strtotime($now)<strtotime($DateStart)){
// 				$sendTimeMode = 'scheduled';
// 				$sendTimeString = $DateStart;
// 			}
// 		}
		
		$i = 0;
		list($pushmessage_subject, $pushmessage_body) = $lnotice->returnPushMessageData($DateStart,$DateEnd,$pushMsgTitle,$pushMsgNoticeNumber);
   
	    if($pushmessagenotify == 1)
	    {
		    if (!empty($students)) {
                // [2019-0917-1522-01206] skip student status checking
				//$parentStudentAssoAry = BuildMultiKeyAssoc($luser->getParentStudentMappingInfo($students), 'ParentID', $IncludedDBField=array('StudentID'), $SingleValue=1, $BuildNumericArray=1);
                $parentStudentAssoAry = BuildMultiKeyAssoc($luser->getParentStudentMappingInfo($students, '', $skipStuStatusCheck=true), 'ParentID', $IncludedDBField=array('StudentID'), $SingleValue=1, $BuildNumericArray=1);
			} else {
				$parentStudentAssoAry = array();
			}
			$individualMessageInfoAry = array();
			$individualMessageInfoAry[0]['relatedUserIdAssoAry'] = $parentStudentAssoAry;
    	    
		    if ($libeClassApp->isEnabledSendBulkPushMessageInBatches()) {
				$notifyMessageId = $libeClassApp->sendPushMessageByBatch($individualMessageInfoAry, $pushmessage_subject, $pushmessage_body, $isPublic, $recordStatus=1, $appType, $sendTimeMode, $sendTimeString, '', '', 'eNotice', $NoticeID);
		    } else {
		    	$notifyMessageId = $libeClassApp->sendPushMessage($individualMessageInfoAry, $pushmessage_subject, $pushmessage_body, $isPublic, $recordStatus=1, $appType, $sendTimeMode, $sendTimeString, '', '', 'eNotice', $NoticeID);
		    }
	    }
	    if($pushmessagenotify_Students == 1)
	    {
	        $StudentAssoAry = array();
		    foreach ($students as $studentId) {
    			$_targetStudentId = $libeClassApp->getDemoSiteUserId($studentId);
    			// link the message to be related to oneself
    			$StudentAssoAry[$studentId] = array($_targetStudentId);
		    }
		    $individualMessageInfoAry = array();
			$individualMessageInfoAry[0]['relatedUserIdAssoAry'] = $StudentAssoAry;
		    
			if ($libeClassApp->isEnabledSendBulkPushMessageInBatches()) {
				$notifyMessageId = $libeClassApp->sendPushMessageByBatch($individualMessageInfoAry, $pushmessage_subject, $pushmessage_body, $isPublic, $recordStatus=1, $appType_s, $sendTimeMode, $sendTimeString, '', '', 'eNotice', $NoticeID);
		    } else {
		    	$notifyMessageId = $libeClassApp->sendPushMessage($individualMessageInfoAry, $pushmessage_subject, $pushmessage_body, $isPublic, $recordStatus=1, $appType_s, $sendTimeMode, $sendTimeString, '', '', 'eNotice', $NoticeID);
		    }
	    }
	    
	    // 2018-05-18 Added by Philips
	    // [2018-1008-1030-09073] Handle both PIC and Class Teachers - push msg notification
	    if($pushmessagenotify_PIC == 1 || $pushmessagenotify_ClassTeachers == 1)
	    {
	        $teacherAssoAry = array();
	        if($pushmessagenotify_PIC == 1)
	        {
    	        if(isset($target_PIC) && sizeof($target_PIC) != 0) {
    	            $target_PIC = array_unique($target_PIC);
    	            foreach($target_PIC as $tPICID) {
    	                $tPICid = $tPICID;
    	                $tPICid = str_replace("&#160;", "", $tPICid);
    	                $tPICid = str_replace("U", "", $tPICid);
    	                
    	                //$PICAssoAry[$tPICid] = array($tPICid);
    	                $teacherAssoAry[$tPICid] = array($tPICid);
    	            }
    	        }
	        }
	        if($pushmessagenotify_ClassTeachers == 1)
	        {
	            require_once($PATH_WRT_ROOT."includes/libclass.php");
	            $libclass = new libclass();
	            
	            $teacherArr = array();
	            foreach ($students as $student) {
	                $class = $libclass->returnCurrentClass($student);
	                $teachers = $libclass->returnClassTeacherID($class[0]['ClassName']);
	                foreach ($teachers as $teacher) {
	                    array_push($teacherArr, $teacher['UserID']);
	                }
	            }
	            $teacherArr = array_unique($teacherArr);
	            foreach ($teacherArr as $tid) {
	                $teacherAssoAry[$tid] = array($tid);
	            }
	        }
	        $individualMessageInfoAry = array();
	        $individualMessageInfoAry[0]['relatedUserIdAssoAry'] = $teacherAssoAry;
            
	        if ($libeClassApp->isEnabledSendBulkPushMessageInBatches()) {
	            //$notifyMessageId = $libeClassApp->sendPushMessageByBatch($individualMessageInfoAry, $pushmessage_subject, $pushmessage_body, $isPublic, $recordStatus = 1, $appType_t, $sendTimeMode, $sendTimeString, '', '', 'eNotice', $NoticeID);
                $notifyMessageId = $libeClassApp->sendPushMessageByBatch($individualMessageInfoAry, $pushmessage_subject, $pushmessage_body, $isPublic, $recordStatus=1, $appType_t, $sendTimeMode, $sendTimeString, '', '', 'eNoticeS', $NoticeID);
            } else {
                //$notifyMessageId = $libeClassApp->sendPushMessage($individualMessageInfoAry, $pushmessage_subject, $pushmessage_body, $isPublic, $recordStatus = 1, $appType_t, $sendTimeMode, $sendTimeString, '', '', 'eNotice', $NoticeID);
                $notifyMessageId = $libeClassApp->sendPushMessage($individualMessageInfoAry, $pushmessage_subject, $pushmessage_body, $isPublic, $recordStatus=1, $appType_t, $sendTimeMode, $sendTimeString, '', '', 'eNoticeS', $NoticeID);
            }
        }
        
// 	    if ($pushmessagenotify_ClassTeachers == 1) {
// 	        $individualMessageInfoAry = array();
// 	        $teacherArr = array();
// 	        require_once ($PATH_WRT_ROOT . "includes/libclass.php");
// 	        $libclass = new libclass();
// 	        foreach ($students as $student) {
// 	            $class = $libclass->returnCurrentClass($student);
// 	            $teachers = $libclass->returnClassTeacherID($class[0]['ClassName']);
// 	            foreach ($teachers as $teacher) {
// 	                array_push($teacherArr, $teacher['UserID']);
// 	            }
// 	        }
// 	        $teacherArr = array_unique($teacherArr);
// 	        $teacherAssoAry = array();
// 	        foreach ($teacherArr as $ts) {
// 	            $teacherAssoAry[$ts] = array(
// 	                $ts
// 	            );
// 	        }
// 	        $individualMessageInfoAry[0]['relatedUserIdAssoAry'] = $teacherAssoAry;
// 	        if ($libeClassApp->isEnabledSendBulkPushMessageInBatches()) {
// 	            $notifyMessageId = $libeClassApp->sendPushMessageByBatch($individualMessageInfoAry, $pushmessage_subject, $pushmessage_body, $isPublic, $recordStatus = 1, $appType_t, $sendTimeMode, $sendTimeString, '', '', '', '');
//                 //$notifyMessageId = $libeClassApp->sendPushMessageByBatch($individualMessageInfoAry, $pushmessage_subject, $pushmessage_body, $isPublic, $recordStatus = 1, $appType_t, $sendTimeMode, $sendTimeString, '', '', 'eNotice', $NoticeID);
//             } else {
//                 $notifyMessageId = $libeClassApp->sendPushMessage($individualMessageInfoAry, $pushmessage_subject, $pushmessage_body, $isPublic, $recordStatus = 1, $appType_t, $sendTimeMode, $sendTimeString, '', '', '', '');
//                 //$notifyMessageId = $libeClassApp->sendPushMessage($individualMessageInfoAry, $pushmessage_subject, $pushmessage_body, $isPublic, $recordStatus = 1, $appType_t, $sendTimeMode, $sendTimeString, '', '', 'eNotice', $NoticeID);
// 	        }
// 	    }
	}
	else
	{
		// cancel scheduled push message
	    $libeClassApp->overrideExistingScheduledPushMessageFromModule("eNotice", $NoticeID, $newNotifyMessageId='');
	    $libeClassApp->overrideExistingScheduledPushMessageFromModule("eNoticeS", $NoticeID, $newNotifyMessageId='');
	}
}
else if($sus_status==4)
{
	// Save the choices -> need approval Only
	$sql = "UPDATE INTRANET_NOTICE 
			SET pushmessagenotify = '$pushmessagenotify',
				pushmessagenotify_Students = '$pushmessagenotify_Students',
                pushmessagenotify_PIC = '$pushmessagenotify_PIC',
                pushmessagenotify_ClassTeachers = '$pushmessagenotify_ClassTeachers',
				pushmessagenotifyMode = '$sendTimeMode',
				pushmessagenotifyTime = '$sendTimeString',
				emailnotify = '$emailnotify',
				emailnotify_Students = '$emailnotify_Students',
                emailnotify_PIC = '$emailnotify_PIC',
                emailnotify_ClassTeachers = '$emailnotify_ClassTeachers'
			WHERE NoticeID = $NoticeID ";
	$lnotice->db_db_query($sql);
}

// for PHP 5.4, replace session_unregister()
//session_unregister("noticeAttFolder");
session_unregister_intranet("noticeAttFolder");

##############################################
##### check for lasalle problem
##### Append NoticeID to the attachment folder
$Attachment2 = $Attachment . ".".$NoticeID;
$path2 = "$file_path/file/notice/".$Attachment2;
rename($path, $path2);
$sql = "UPDATE INTRANET_NOTICE SET Attachment = '". $Attachment2 ."', OriginalAttachment = '". $Attachment2 ."' WHERE NoticeID = '$NoticeID'";
$lnotice->db_db_query($sql);
##############################################

intranet_closedb();

//header("Location: index.php?xmsg=". $Lang['General']['ReturnMessage']['AddSuccess'] ."&pageNo=$pageNo&field=$field&page_size_change=$page_size_change&numPerPage=$numPerPage&noticeType=$noticeType&status=$status&year=$year&month=$month&NoticeID=$NoticeID");
//header("Location: index.php?xmsg=AddSuccess&pageNo=$pageNo&field=$field&page_size_change=$page_size_change&numPerPage=$numPerPage&noticeType=$noticeType&status=$status&year=$year&month=$month&NoticeID=$NoticeID");
header("Location: after_save_location.php?noticeType=P&xmsg=AddSuccess&pageNo=$pageNo&field=$field&page_size_change=$page_size_change&numPerPage=$numPerPage&status=$sus_status&year=$year&month=$month");

function handleImageOrientation($image_path, $image_type, $first)
{
	if($first == 0){
		ini_set('memory_limit', '256M');
		$first++;
	}
	# Create image object
	if($image_type == 'jpg' || $image_type == 'jpeg') {
		$image_obj = imagecreatefromjpeg($image_path);
	} else {
		$image_obj = imagecreatefrompng($image_path);
	}
	
	# Rotate image if orientation is incorrect
	if (function_exists('exif_read_data'))
	{
		$exif = @exif_read_data($image_path);
		if($exif && isset($exif['Orientation']))
		{
			$orientation = $exif['Orientation'];
			if($orientation != 1)
			{
				$deg = 0;
				switch ($orientation) {
					case 3:
						$deg = 180;
						break;
					case 6:
						$deg = 270;
						break;
					case 8:
						$deg = 90;
						break;
				}
				if ($deg) {
					$image_obj = imagerotate($image_obj, $deg, 0);
				}
			}
		}
	}
	# Create jpeg file
	if($image_type == 'jpg' || $image_type == 'jpeg') {
		imagejpeg($image_obj, $image_path, 100);
	} else {
		imagepng($image_obj, $image_path, 0);
	}
	
	return $first;
}
?>