<?php
# using: 

####### Change log [Start] #####
#
#   Date:   2020-09-15  Bill    [2020-0915-1022-58066]
#           fixed: incorrect Question Relation handling if reply slip has MC questions that not required to be answered
#
# 	Date:   2020-09-07  Bill    [2020-0604-1821-16170]
#			access right checking > replace hasIssueRight() by hasSchoolNoticeIssueRight()
#
#   Date:   2020-09-03  Bill    [2020-0826-1451-05073]
#           Performance Issue: use JS to handle Question Relation
#
#	Date:	2016-07-15	Bill	[2016-0113-1514-09066]
#			- Create file
#
####### Change log [End] #######

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libform.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$lnotice = new libnotice($NoticeID);

// [2020-0604-1821-16170]
//if($lnotice->disabled || !$lnotice->hasIssueRight() || empty($NoticeID))
if($lnotice->disabled || !$lnotice->hasSchoolNoticeIssueRight() || empty($NoticeID))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

// Module & Navigation
$PAGE_NAVIGATION = array();
$PAGE_NAVIGATION[] = array($Lang['eSurvey']['Question']);
$MODULE_OBJ['title'] = $Lang['eNotice']['SetReplySlipContent'];

$lform = new libform();

$reqFillAllFields = $lnotice->AllFieldsReq;
$DisplayQNum = $lnotice->DisplayQuestionNumber;

// Build Layout
$linterface = new interface_html("popup.html");
$linterface->LAYOUT_START();
?>
<style>
tr.disabled_q, tr.disabled_q td, tr.disabled_q td.tabletext, tr.disabled_q td span.tabletextrequire {
	color: gray;
}
</style>
<br>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
	<td colspan="2">
        <table align="center" width="100%" border="0" cellpadding="0" cellspacing="0">
		
        <!-- Relpy Slip -->
            <tr valign='top'>
				<td colspan="2" class='eNoticereplytitle' align='center'><?=$Lang['eNotice']['ReplySlipForm']?></td>
			</tr>
			<? 			
				// Question and Answer
				$queString = str_replace("&amp;", "&", $lnotice->Question);
				$queString = preg_replace('(\r\n|\n)', "<br>", $queString);
				$queString = trim($queString)." ";
				$qAry = $lnotice->splitQuestion($queString, true);
				$aAry = $lnotice->parseAnswerStr($ansString);
				
				// Convert Question String for display
				$queString = $lform->getConvertedString($queString);
				
				// init array for js checking
				$optionArray = array();
				$optionArray[0] = array();
				$optionArray[1] = array();
				$mustSubmitArray = array();
                $isSkipQuestionArray = array();

				// loop Questions
				for($i=0; $i<count($qAry); $i++){
					$currentQuestion = $qAry[$i];
					$mustSubmitArray[] = $reqFillAllFields || $currentQuestion[3];
					$optionArray[0][] = $currentQuestion[4][0];
					$optionArray[1][] = $currentQuestion[4][1];
                    $isSkipQuestionArray[] = $mustSubmitArray[$i] && $optionArray[1][$i] != '' && ($optionArray[0][$i] != $optionArray[1][$i]);
					
					// for MC Questions
                    // [2020-0915-1022-58066]
                    //if($currentQuestion[0]==2 && $mustSubmitArray[$i])
					if($currentQuestion[0]==2)
					{
						$optionCount = count((array)$currentQuestion[4]);
						for($j=2; $j<$optionCount; $j++)
						{
							$optionArray[$j][$i] = $currentQuestion[4][$j];
						}

                        // [2020-0915-1022-58066] MC questions that required to be answered
                        $isSkipQuestionArray[$i] = false;
                        if($mustSubmitArray[$i])
                        {
                            if(count(array_unique($currentQuestion[4])) > 1) {
                                $mustSubmitArray[$i] = $optionCount;
                                $isSkipQuestionArray[$i] = true;
                            }
                        }
					}
				}

                // [2020-0826-1451-05073] use JS to handle Question Relation
				// $qAryRelation = $lnotice->handleQuestionRelation($mustSubmitArray, $optionArray);
			?>
			<!-- Question Base Start //-->
			<tr valign='top'>
				<td colspan="2">
                <table width="100%" border="0" cellspacing="0" cellpadding="5">
                <tr><td>
                        <form name="ansForm" method="post" action="update.php">
                        	<table width='100%' border='0' cellspacing='0' cellpadding='5' align='center'>
							<tr><td>
								<?= $lnotice->returnReplySlipContent("preview", $qAry, $aAry, $reqFillAllFields, $DisplayQNum);?>
							</td></tr>
							</table>
                            <input type=hidden name="qStr" value="<?=$queString?>">
                            <input type=hidden name="aStr" value="">
                    	</form>
                    
                        <SCRIPT LANGUAGE=javascript>
                            var mustSubmitArr = new Array("", "<?= implode('","',(array)$mustSubmitArray); ?>");
                            var option0SkipArr = new Array("", "<?= implode('","',(array)$optionArray[0]); ?>");
							var option1SkipArr = new Array("", "<?= implode('","',(array)$optionArray[1]); ?>");
                            var isSkipQuestionArr = new Array("", "<?= implode('","',(array)$isSkipQuestionArray); ?>");
							var relateArr = {};
							var totalQNum = <?=count($qAry)?>;
							<?
							    /*
								foreach($qAryRelation as $qNum => $qOptions)
								{
									echo "relateArr[".$qNum."] = {};\n";
									foreach($qOptions as $oNum => $qr)
									{
										echo "relateArr[".$qNum."][".$oNum."] = new Array(\"".implode("\",\"",(array)$qr)."\");\n";
									}
								}
								*/

                                foreach((array)$optionArray as $oNum => $qOptionArray)
                                {
                                    echo "relateArr[".$oNum."] = {};\n";
                                    foreach((array)$qOptionArray as $qNum => $targetQ)
                                    {
                                        if($targetQ == '') {
                                            $targetQ = $optionArray[0][$qNum];
                                        }
                                        if($targetQ == '') {
                                            $targetQ = $qNum + 1;
                                        }
                                        echo "relateArr[".$oNum."][".$qNum."] = '$targetQ';\n";
                                    }
                                }
							?>
							var skipCheckingToQ = 0;
                    		
                    		function checkAvailableOption(i, opt)
                    		{
                    			var i = parseInt(i);
                                var targetQ = relateArr[opt][i-1];
                                if(targetQ != 'end') {
                                    targetQ = parseInt(targetQ);
                                }
                                var originTargetQ = targetQ;
                                var isJumpQ = false;
                                var preTextOnlyQCount = 0;

                                //var opt = parseInt(opt);
                    			/*
                    			// Skip if no option settings 
                    			//var availableAry = relateArr[i-1][opt];
	                            var availableAry = relateArr[i-1];
	                            if(availableAry==undefined)
	                            	return;
	                            
	                            availableAry = availableAry[opt];
                    			if(availableAry==undefined){
                    				availableAry = new Array("-1");
                    			}
                    			*/

                    			for(var v=i+1; v<=totalQNum; v++){
                    				var eleObj = eval("document.ansForm.F" + v);
                    				if (eleObj){
                        				if (eleObj.length){
								       		switch(eleObj[0].type)
								        	{
								        		// Radio Button
								        	    case "radio":	
						                        case "checkbox":
                                                    if(v >= (targetQ + 1) && targetQ != 'end')
                                                    {
					                                    for (var p=0; p<eleObj.length; p++){
		                                					eleObj[p].disabled = false;
		                                					//eval("document.ansForm.F"+v+".disabled=false");
		                                					//$("#msg_"+v).attr('style', 'display:none');
		                                					$("td#msg_" + v).hide();
		                                					$("tr#row" + v).removeClass("disabled_q");
		                                				}

                                                        var isPreJumpQ = isJumpQ;
                                                        if((targetQ == originTargetQ || !isPreJumpQ) && v == (targetQ + 1 + preTextOnlyQCount)) {
                                                            isJumpQ = isSkipQuestionArr[v];
                                                            if(!isJumpQ) {
                                                                targetQ = relateArr[0][v-1];
                                                                if(targetQ != 'end') {
                                                                    targetQ = parseInt(targetQ);
                                                                }
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        for (var p=0; p<eleObj.length; p++){
		                                					eleObj[p].disabled = true;
		                                					eleObj[p].checked = false;
		                                					//eval("document.ansForm.F"+v+".disabled=true");
		                                					//$("#msg_"+v).attr('style', '');
		                                					$("td#msg_" + v).show();
		                                					$("tr#row" + v).addClass("disabled_q");
		                                				}
					                                }
					                                break;
								        	}
                        				}
                        				else
                        				{
                                            if(v >= (targetQ + 1) && targetQ != 'end')
                                            {
                            				    //eleObj[p].disabled=false;
                            					eval("document.ansForm.F" + v + ".disabled=false");
		                                		//$("#msg_"+v).attr('style', 'display:none');
		                                		$("td#msg_" + v).hide();
                            					$("tr#row" + v).removeClass("disabled_q");

                                                var isPreJumpQ = isJumpQ;
                                                if((targetQ == originTargetQ || !isPreJumpQ) && v == (targetQ + 1 + preTextOnlyQCount)) {
                                                    isJumpQ = isSkipQuestionArr[v];
                                                    if(!isJumpQ) {
                                                        targetQ = relateArr[0][v-1];
                                                        if(targetQ != 'end') {
                                                            targetQ = parseInt(targetQ);
                                                        }
                                                    }
                                                }
                            				}
                            				else
                                            {
                            					//eleObj[p].disabled=true;
                            					eval("document.ansForm.F" + v + ".disabled=true");
                            					eval("document.ansForm.F" + v + ".value=''");
		                                		//$("#msg_"+v).attr('style', '');
		                                		$("td#msg_" + v).show();
                            					$("tr#row" + v).addClass("disabled_q");
                            				}
                        				}

                                        preTextOnlyQCount = 0;
                                    } else {
                                        if(v >= (targetQ + 1) && targetQ != 'end') {
                                            preTextOnlyQCount = preTextOnlyQCount + 1;
                                        }
                                    }
                    			}
                    		}
                            
                            function updateAllAvailableOption(){
                           		// get T/F Question that involve skip action
                            	var possibleTF = $('input[onclick]:checked');
                            	if(possibleTF.length){
                            		for(var TFCount=0; TFCount<possibleTF.length; TFCount++)
                            		{
                            			var currentTFQ = possibleTF[TFCount]
                            			var TFQNum = currentTFQ.name.replace("F", "");
                            			var TFQOption = currentTFQ.value;
                            			
                                		// perform to disable fields not related to answered question
										if(!currentTFQ.disabled){
                            				checkAvailableOption(TFQNum, TFQOption);
                            			}
                            		}
                            	}
                            }
                            
                           	$(document).ready(function(){
                               	// update all options
                           		updateAllAvailableOption();
                            });
                  		</SCRIPT>
                </td></tr>
                </table>
        		</td>
			</tr>
			<!-- Question Base End //-->
		</table>
	</td>
</tr>

<tr>
	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
</tr>

<tr>
	<td align="center">
		<br/><?= $linterface->GET_ACTION_BTN($button_close, "button", "window.close(); return false;","cancelbtn") ?>
	</td>
</tr>

</table>                        
<br />

<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>