<?php
# using: yuen

//error_reporting(E_ALL);
ini_set('display_errors', '1');

########################################
#
#   Date:   2019-06-06  Bill
#           add CSRF token checking
#
#	Date:	2013-12-13	Roy
#			improved get Parent's userlogin SQL statement to filter parent app user
#
#	Date:	2013-10-08	Roy
#			Newly added
#
########################################

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."plugins/asl_conf.php");
include_once($PATH_WRT_ROOT."includes/libaslparentapp.php");

if(!verifyCsrfToken($_GET['csrf_token'], $_GET['csrf_token_key'], $do_not_remove_token_data=true)){
    echo "<font color='red'>".$Lang['AppNotifyMessage']['eNotice']['send_failed']."</font>";
    exit;
}

intranet_auth();
intranet_opendb();

$lnotice = new libnotice($NoticeID);
$UnsignedStudents = $lnotice->getUnsignList($NoticeID, $class);

$lu = new libuser($UserID);
$notifyuser = $lu->UserNameLang();

$asl = new ASLParentApp($asl_parent_app["host"], $asl_parent_app["path"]);

$SchoolID = isset($asl_parent_app['SchoolID'])? $asl_parent_app['SchoolID'] : $_SERVER['SERVER_NAME'];
$MsgNotifyDateTime = date("Y-m-d H:i:s");

$NotifyMessageID = -1;

$NoticeTitle = "".$lnotice->NoticeNumber.":".$lnotice->Title." (".$i_Notice_DateEnd.":".$lnotice->DateEnd.")";
//$lnotice->DateEnd

$ParentFound = 0;

# Get parent app users list
$sql = "select distinct UserID from INTRANET_API_REQUEST_LOG";
$parentAppUserArr = $lu->returnArray($sql);
$parentAppUserIDArr = array();
foreach ((array)$parentAppUserArr as $parentAppUserInfo) {
	$parentAppUserIDArr[] = $parentAppUserInfo['UserID'];
}

for($i=0; $i<sizeof($UnsignedStudents); $i++)
{
	$StudentID = $UnsignedStudents[$i];

	$MessageContent = str_replace("[NoticeTitle]", $NoticeTitle, $Lang['AppNotifyMessage']['eNotice']['Alert']);
	
	# Get Parent's userlogin
	$sql = "SELECT iu.UserLogin, iu.UserID from INTRANET_PARENTRELATION AS ip, INTRANET_USER AS iu WHERE ip.StudentID='{$StudentID}' AND ip.ParentID=iu.UserID  AND iu.RecordStatus=1 AND ip.ParentID IN ('".implode("','", (array)$parentAppUserIDArr)."')";
	$recipient_rows = $lnotice->returnResultSet($sql);
	$recipient_parent_userlogin = array();
	for ($ri=0; $ri<sizeof($recipient_rows); $ri++)
	{
		$parentID = $recipient_rows[$ri]["UserID"];
		# check is current parent app user
		// if (!in_array($parentID, (array)$parentAppUserIDArr)) {
			// continue;
		// }
		
		$recipient_parent_userlogin[] = $recipient_rows[$ri]["UserLogin"];
		$ParentFound ++;
	}
	
	$MessageTitle = $Lang['AppNotifyMessage']['eNotice']['Title'] . " [#".$lnotice->NoticeNumber."]";
	$ASLParam = array(
				"SchID" => $SchoolID,
				"MsgNotifyDateTime" => $MsgNotifyDateTime,
				"MessageTitle" => $MessageTitle,
				"MessageContent" => $MessageContent,
				"NotifyFor" => $notifyfor,
				"IsPublic" => "N",
				"NotifyUser" => $notifyuser,
				"TargetUsers" => array(
					"TargetUser" => $recipient_parent_userlogin
				)
		);
	
	$ReturnMessage = "";
	if (sizeof($recipient_parent_userlogin)>0)
	{
		$Result = $asl->NotifyMessage($ASLParam, $ReturnMessage);
	}
	else
	{
		$Result = false;
	}
	
	if ($Result)
	{
		//debug_r($ASLParam);
		if ($NotifyMessageID==-1)
		{
			$sql = "INSERT INTO INTRANET_APP_NOTIFY_MESSAGE(NotifyDateTime,MessageTitle,MessageContent,NotifyFor,IsPublic,NotifyUser,NotifyUserID,RecordStatus,DateInput,DateModified)
					VALUES ('$MsgNotifyDateTime','".addslashes($MessageTitle)."','MULTIPLE MESSAGES','".$notifyfor."','N','".addslashes($notifyuser)."','$UserID','1',NOW(),NOW())";
		
			$success = $lnotice->db_db_query($sql);
			$NotifyMessageID = $lnotice->db_insert_id();
		}
        
		if ($NotifyMessageID>0)
		{
			for ($ri=0; $ri<sizeof($recipient_rows); $ri++)
			{
				$sql = "INSERT INTO INTRANET_APP_NOTIFY_MESSAGE_TARGET(NotifyMessageID,TargetType,TargetID,MessageTitle,MessageContent,DateInput)
						VALUES ('$NotifyMessageID','U','".$recipient_rows[$ri]["UserID"]."','".addslashes($MessageTitle)."','".addslashes($MessageContent)."',NOW())";
				$target_success = $lnotice->db_db_query($sql);
				
				$SentTotal ++;
			}
		}
	}
	else
	{
		// do nothing
	}
}
 
intranet_closedb();

if ($SentTotal>0 && $SentTotal!="")
{
	echo "<font color='#DD5555'>".str_replace("[SentTotal]", $SentTotal, $Lang['AppNotifyMessage']['eNotice']['send_result']). "</font>";
}
else
{
	$ErrorMsg = $Lang['AppNotifyMessage']['eNotice']['send_failed'];
	if ($ParentFound<1)
	{
		$ErrorMsg .= $Lang['AppNotifyMessage']['eNotice']['send_result_no_parent'];
	}
	echo "<font color='red'>".$ErrorMsg."</font> <font color='grey'>(".sizeof($UnsignedStudents)."-".$ParentFound.")</font>";
}
?>