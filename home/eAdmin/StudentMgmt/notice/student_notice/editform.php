<?php
# using: Bill

####### Change log [Start] #######
#
# 	Date:   2020-09-07  Bill    [2020-0604-1821-16170]
#			access right checking > replace hasIssueRight() by hasSchoolNoticeIssueRight()
#
#	Date:	2015-11-16	Bill	[2015-0615-1438-48014]
#			added option - Question must be submitted (sheet.mustSubmit = 1)
#
#	Date:	2012-06-20 YatWoon
#			Fixed: If edit the reply slip again, the question with "enter" will become "<br>"
#
#	Date:	2011-02-24	YatWoon
#			add option "Max. Reply Slip Option"
#
#	Date:	2011-02-16	YatWoon
#			set DisplayQuestionNumber = 0 for editing form
#
#	Date:	2010-06-21	YatWoon
#			add online help button
#
#	Date:	2010-01-14 [YatWoon]
#			build the reply slip tempates array
#
####### Change log [End] #######

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");
include_once($PATH_WRT_ROOT."includes/libform.php");

intranet_auth();
intranet_opendb();

$lnotice = new libnotice();

// [2020-0604-1821-16170]
//if (!$lnotice->disabled && $lnotice->hasIssueRight())
if (!$lnotice->disabled && $lnotice->hasSchoolNoticeIssueRight())
{
    $lf = new libform();
    
    $MODULE_OBJ['title'] = $i_Notice_ReplyContent;
$linterface = new interface_html("popup.html");
$linterface->LAYOUT_START();

# build reply slip templates
// $sql = "select Title, Question from INTRANET_NOTICE where RecordStatus=3";
// $result = $lnotice->returnArray($sql);
// for($i=0;$i<sizeof($result);$i++)
// {
// 	$x .= "form_templates[". $i ."] = new Array(\"". $result[$i]['Title'] ."\", \"". $result[$i]['Question']."\");\n";
// }

$onlineHelpBtn = gen_online_help_btn_and_layer('enotice_admin','edit_slip');

?>

<?=$onlineHelpBtn?>

<script language="javascript" src="/templates/forms/form_edit.js"></script>
<script language="javascript" src="/templates/forms/layer.js"></script>

<br />

<form name="ansForm" action="">
<input type="hidden" name="qStr" value="">
<input type="hidden" name="aStr" value="">

<script language="Javascript">
// Grab values from
s = new String(window.opener.document.form1.qStr.value);
s = s.replace(/"/g, '&quot;');
document.ansForm.qStr.value = s;
document.ansForm.aStr.value = window.opener.document.form1.aStr.value;
function copyback()
{
         finish();
         s = recurReplace(">", "&gt;", document.ansForm.qStr.value);
		 s = recurReplace("<", "&lt;", s);
//          window.opener.document.form1.qStr.value = document.ansForm.qStr.value;
			window.opener.document.form1.qStr.value = s;
         window.opener.document.form1.aStr.value = document.ansForm.aStr.value;
         self.close();
}
<?=$lf->getWordsInJS()?>

var answer_sheet	= '<?=$i_Notice_ReplyContent?>';
var answer_sheet_mustsubmit = '<?=$Lang['eNotice']['QuestionNeedToReply']?>';
var space10 	= '<img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" />';
var add_btn 	= '<?=str_replace("'", "\'",$linterface->GET_BTN($button_add, 'button', 'retainValues(); if (appendTxt(this.form.name)) {writetolayer(\'blockInput\',sheet.writeSheet()); this.form.reset(); this.form.secDesc.focus();}','submit2'))?>';
var order_name	= '<?=$i_Sports_Order?>';
<? $PAGE_NAVIGATION1[] = array($Lang['eSurvey']['Question']); ?>
var Part1 = '<?=str_replace("'","\'",$linterface->GET_NAVIGATION($PAGE_NAVIGATION1))?>'
<? $PAGE_NAVIGATION2[] = array($i_general_DisplayOrder); ?>
var Part2 = '<?=str_replace("'","\'",$linterface->GET_NAVIGATION($PAGE_NAVIGATION2))?>'
<? $PAGE_NAVIGATION3[] = array($Lang['eSurvey']['ReplySlipQuestions']); ?>
var Part3 = '<?=str_replace("'","\'",$linterface->GET_NAVIGATION($PAGE_NAVIGATION3))?>'
var replyslip = '<?=$i_Notice_ReplySlip?>';
var MoveUpBtn = '<?=$Lang['Button']['MoveUp']?>';
var MoveDownBtn = '<?=$Lang['Button']['MoveDown']?>';
var DeleteBtn = '<?=$Lang['Button']['Delete']?>';
var EditBtn = '<?=$Lang['Button']['Edit']?>';
var DisplayQuestionNumber = 0;
if(window.opener.document.form1.DisplayQuestionNumber.checked)
	DisplayQuestionNumber = 1;
var MaxOptionNo = "<?=$lnotice->MaxReplySlipOption?>";
var form_templates = new Array();     
<?=$lnotice->buildReplySlipTemplates()?>	
background_image = "";

var sheet= new Answersheet();
// attention: MUST replace '"' to '&quot;'
sheet.qString = document.ansForm.qStr.value;
sheet.mode=0;        // 0:edit 1:fill in application
sheet.mustSubmit=1;	 // display option - Question must be submitted
sheet.answer=sheet.sheetArr();
sheet.templates=form_templates;
document.write(editPanel());
</script>



<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
<tr>
	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
</tr>
<tr>
	<td align="center">
				<?= $linterface->GET_ACTION_BTN($button_save, "submit", "copyback(); return false;","submit2") ?>
				<?= $linterface->GET_ACTION_BTN($button_close, "button", "self.close();","cancelbtn") ?>
	</td>
</tr>
</table>                   

</form>
<?php
}
else
{
    header ("Location: /");
}

intranet_closedb();
$linterface->LAYOUT_STOP();
?>