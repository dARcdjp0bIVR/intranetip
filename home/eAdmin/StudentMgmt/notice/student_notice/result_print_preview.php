<?php
# using: 

############ Change Log [Start] 
#
#	Date:	2015-04-14 Bill
#			allow eNotice PIC to access [2015-0323-1602-46073]
#			allow access when "All staff can issue notices" & "All staff can view reply contents" & not in access group & not eNotice admin
#
########################

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html("");

$lnotice = new libnotice($NoticeID);

$lu = new libuser($UserID);
if (!$lu->isTeacherStaff())
{
     header("Location: /close.php");
     exit();
}

// [2015-0323-1602-46073] allow eNotice PIC to access
if (!$lnotice->hasViewRight() && !$lnotice->staffview && !$lnotice->isNoticePIC($NoticeID))
{
     header("Location: /close.php");
     exit();
}

if ($lnotice->RecordType == 4)
{
    header("Location: tableview.php?NoticeID=$NoticeID");
    exit();
}

$array_total = $lnotice->returnTotalCountByClass();
$array_signed = $lnotice->returnSignedCountByClass();
$all_total = 0;
$all_signed = 0;
$classes = $lnotice->returnClassList();

$x .= "<table width='90%' border='0' cellspacing='0' cellpadding='4' class='eSporttableborder'>\n";
$x .= "<tr>";
$x .= "<td class='eSporttdborder eSportprinttabletitle'>$i_ClassName</td>";
$x .= "<td class='eSporttdborder eSportprinttabletitle' width='80'>$i_Notice_Signed</td>";
$x .= "<td class='eSporttdborder eSportprinttabletitle' width='80'>$i_Notice_Total</td>";
$x .= "</tr>\n";

for ($i=0; $i<sizeof($classes); $i++)
{
     $name = $classes[$i];
     ## check $class <>""
     if(!$name) continue;
     $signed = $array_signed[$name]+0;
     $total = $array_total[$name]+0;
     $displayName = ($name==""? "--":"$name");
     $x .= "<tr>\n";
     $x .= "<td class='eSporttdborder eSportprinttext'>$displayName</td>";
     $x .= "<td class='eSporttdborder eSportprinttext' width='80'>$signed</td>";
     $x .= "<td class='eSporttdborder eSportprinttext' width='80'>$total</td>";
     $x .= "</tr>\n";
     $all_total += $total;
     $all_signed += $signed;
}
$x .= "</table>\n";

### All student
$x .= "<br /><table width='90%' border='0' cellspacing='0' cellpadding='4' class='eSporttableborder'>\n";
$x .= "<tr><td class='eSporttdborder eSportprinttext'>$i_Notice_AllStudents</td>";
$x .= "<td class='eSporttdborder eSportprinttext' width='80'>$all_signed</td>";
$x .= "<td class='eSporttdborder eSportprinttext' width='80'>$all_total</td>";
$x .= "</tr>\n";
$x .= "</table>\n";

?>

<table width='100%' align='center' class='print_hide' border=0>
	<tr>
		<td align='right'><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></td>
	</tr>
</table>

<!-- Notice Title -->
<table width='100%' align='center' border=0>
	<tr>
        	<td align='center' class='eSportprinttitle'><b><?=$i_Notice_ResultForEachClass?></b></td>
	</tr>
</table>      


<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
<tr><td class="eSportprinttitle"><?=$i_Notice_NoticeNumber?> : <?=$lnotice->NoticeNumber?></td></tr>
<tr><td class="eSportprinttitle"><?=$i_Notice_Title?> : <?=$lnotice->Title?></td></tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="5">

<!--  List -->
<tr>
	<td align="center"><?=$x?></td>
</tr>  

</table>
<br />

<?php
include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_footer.php");
intranet_closedb();
?>
