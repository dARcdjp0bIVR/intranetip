<?php
# Using:

################## Change Log [Start] ##############
#
#   Date:   2020-09-07  Bill    [2020-0604-1821-16170]
#           - access right checking > replace hasIssueRight by hasSchoolNoticeIssueRight
#
#   Date:   2020-05-04  Bill    [2020-0407-1445-44292]
#           - support class teacher audience limit checking + UI update ($sys_custom['eNotice']['ClassTeacherSelectOwnClassStudentOnly'])
#
#   Date:   2020-04-15 Bill    [2020-0310-1051-05235]
#           - added js_handle_special_notice_options_display()   ($sys_custom['eNotice']['SpecialNoticeForPICAndAdmin'])
#
#   Date:   2020-02-17 Bill
#           - replace php inline coding (if('<?=$plugin['eClassApp']>'=='1') { ... }) in js
#
#   Date:   2019-10-18 Bill     [2019-1018-0914-28066]
#           - disabled submit button style
#
#	Date:	2019-09-30 Philips 	[2019-0930-0954-24073]
#			- call checkedSendPushMessage after select Template, Select Template seperated to the top
#
#   Date:   2019-07-05 Bill     [2019-0412-1159-01235]
#           Improved: use POST instead of GET for ajax_search_user_by_classname_classnumber.php
#
#   Date:   2018-11-08  Bill    [DM#3470]
#           Fixed: support js action related to [Notify PIC using email] & [Notify Class Teachers using email]
#
#   Date:   2018-09-06  Bill    [2018-0905-1434-14066]
#           - modified js selectTemplate(), rebind click action > prevent cannot search PIC
#
#   Date:   2018-09-05  Bill    [2018-0903-0945-20066]
#           - support js action related to [Notify Class Teacher] & [Notify PIC]
#
#   Date:   2018-05-21 Philips
#           - modified function checkedSendPushMessage()
#
#   Date:   2018-03-26 Bill    [2018-0221-1225-19206]
#           - sort notice templates by notice number and notice title
#
#   Date:   2018-02-21 Bill    [2017-1107-1707-25207]
#           Fixed: modified js selectTemplate(), init thickbox link again > prevent cannot open thickbox after Load from Template
#
#	Date:	2017-10-13	Bill
#			Fixed: JS error due to not using Student App
#
# 	Date:	2016-09-23	(Villa)
#			- add to module setting - "default using eclass App to notify"
#
#	Date:	2016-09-21 Villa
#			- added Push Message Setting box
#
#	Date:	2016-09-12	Bill
#			- modified js checkform(), send time must be equal to or later than issue time
#
#	Date:	2016-08-19	Kenneth
#			- fix default end date is not 23:59:59 issue
#
#	Date:	2016-07-18	Tiffany	
# 			- modified function checkedSendPushMessage()
#
#	Date:	2015-12-18	Kenneth		[2015-0713-0926-40206]
#			- if seleted as template and audience not been choicen, 'Whole School' would be selected (JS: checkForm())
#
#	Date:	2015-12-11	Bill
#			- replace session_register() by session_register_intranet() for PHP 5.4
#
#	Date:	2015-11-09	Kenneth		[2015-1020-1555-27066]
#			- allow auto count eNotice deadline for future eNotice (JS)
#
#	Date:	2015-10-28	Bill	[2015-0416-1040-06164]
#			- support merge notice content
#
#	Date:	2015-10-05	Bill	[2015-0930-1022-55071]
#			- added input date format checking
#
#	Date:	2015-09-15	Bill
#			- removed php error msg due to each()
#
#	Date:	2015-06-26 	Omas
#			- added autocompleter for type 4
#
#	Date:	2015-04-14	Bill	[2015-0323-1602-46073]
#			- add field to select PICs of eNotice
#
#	Date:	2015-01-02 (Bill)
#			Copy from "notice/new.php"
#			- Disable submit button after submit the form [Case #D72427]
#			- Add hidden input field "noticeAttFolder" for POST [Case #P70566]
#
#	Date:	2014-10-14 (Roy)
#			- add push message option in JavaScript
#
#	Date:	2013-09-06	YatWoon
#			Improved: update the upload attachment method
#
#	Date:	2012-09-12 YatWoon
#			Improved: default "All questions are required to be answered" is checked. [Case#2012-0907-1757-42071]
#
#	Date:	2012-05-04 YatWoon
#			Improved: skip the start date checking [Case#2012-0504-1030-05066]
#
################## Change Log [End] ##############

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

// [2020-0604-1821-16170]
//if (!$plugin['notice'] || $_SESSION["SSV_PRIVILEGE"]["notice"]["disabled"] || !$_SESSION["SSV_PRIVILEGE"]["notice"]["hasIssueRight"])
if (!$plugin['notice'] || $_SESSION["SSV_PRIVILEGE"]["notice"]["disabled"] || !$_SESSION["SSV_PRIVILEGE"]["notice"]["hasSchoolNoticeIssueRight"])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libform.php");
include_once($PATH_WRT_ROOT."includes/libfiletable.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();

$CurrentPage = "PageStudentNotice";
$CurrentPageArr['eAdminNotice'] = 1;

$lnotice = new libnotice();
$lform = new libform();
$li = new libfilesystem();

// [2020-0407-1445-44292]
$isAudienceLimited = $lnotice->isClassTeacherAudienceTargetLimited();

# Attachment Folder
# create a new folder to avoid more than 1 notice using the same folder
//session_register("noticeAttFolder");
$noticeAttFolder = $lnotice->genAttachmentFolderName();
// for PHP 5.4, replace session_register()
session_register_intranet("noticeAttFolder", $noticeAttFolder);

$path = "$file_path/file/notice/";
if (!is_dir($path))
{
	if($bug_tracing['notice_attachment_log']){
         $temp_log_filepath = "$file_path/file/log_notice_attachment.txt";
         $temp_time = date("Y-m-d H:i:s");
         $temp_user = $UserID;
         $temp_page = 'notice_new.php';
         $temp_action="create folder:".$path;
         $temp_content = get_file_content($temp_log_filepath);
         $temp_logentry = "\"$temp_time\",\"$temp_user\",\"$temp_page\",\"$temp_action\"\n";
         $temp_content .= $temp_logentry;
         write_file_content($temp_content, $temp_log_filepath);
	 }
     $li->folder_new($path);
}

//$path = "$file_path/file/notice/$noticeAttFolder"."tmp";
$path = "$file_path/file/notice/$noticeAttFolder";
if (!is_dir($path))
{
	if($bug_tracing['notice_attachment_log']){
         $temp_log_filepath = "$file_path/file/log_notice_attachment.txt";
         $temp_time = date("Y-m-d H:i:s");
         $temp_user = $UserID;
         $temp_page = 'notice_new.php';
         $temp_action="create folder:".$path;
         $temp_content = get_file_content($temp_log_filepath);
         $temp_logentry = "\"$temp_time\",\"$temp_user\",\"$temp_page\",\"$temp_action\"\n";
         $temp_content .= $temp_logentry;
         write_file_content($temp_content, $temp_log_filepath);
	 }
     $li->folder_new($path);
}
/*
$lo = new libfiletable("", $path, 0, 0, "");
$files = $lo->files;
*/

## Template
//$notice_templates = $lnotice->returnTemplates();
$notice_templates = $lnotice->returnTemplates('', $orderByTitle=true);
$NAoption = array(-1, $i_Notice_NotUseTemplate);
if (sizeof($notice_templates) != 0)
{
    array_unshift($notice_templates, $NAoption);
}
else
{
    $notice_templates = array($NAoption);
}
$template_selection = getSelectByArray($notice_templates,"name='templateID' onChange=\"selectTemplate()\"",'','',1);

// [2020-0407-1445-44292] limited to 2 audience types only
if($isAudienceLimited)
{
    $type_selection = "
    <SELECT name='type' onChange='selectAudience()'>
        <OPTION value='' > -- $button_select -- </OPTION>
        <OPTION value='3'>$i_Notice_RecipientTypeClass</OPTION>
        <OPTION value='4'>$i_Notice_RecipientTypeIndividual</OPTION>
    </SELECT>";
}
else
{
    $type_selection = "
    <SELECT name='type' onChange='selectAudience()'>
        <OPTION value='' > -- $button_select -- </OPTION>
        <OPTION value='1'>$i_Notice_RecipientTypeAllStudents</OPTION>
        <OPTION value='2'>$i_Notice_RecipientTypeLevel</OPTION>
        <OPTION value='3'>$i_Notice_RecipientTypeClass</OPTION>
        <OPTION value='4'>$i_Notice_RecipientTypeIndividual</OPTION>
    </SELECT>";
}

$now = time();
$defaultEnd = $now + ($lnotice->defaultNumDays*24*3600);
$start = date('Y-m-d H:i:s',$now);
$end = date('Y-m-d 23:59:59',$defaultEnd);
    	
### Title ###
$TAGS_OBJ[] = array($Lang['eNotice']['SchoolStudentNotice']);
if($sys_custom['PowerClass']){
	$TAGS_OBJ = array();
	$TAGS_OBJ[] = array($Lang['eNotice']['SchoolNotice'],'../new.php', 0);
	$TAGS_OBJ[] = array($Lang['eNotice']['SchoolStudentNotice'],'', 1);
	$TAGS_OBJ[] = array($Lang['Header']['Menu']['eCircular'],"../../../StaffMgmt/circular/new.php", 0);
}

$MODULE_OBJ = $lnotice->GET_MODULE_OBJ_ARR();

$onlineHelpBtn = gen_online_help_btn_and_layer('enotice_admin','new_settings');
//$TAGS_OBJ_RIGHT[] = array($onlineHelpBtn);

$linterface->LAYOUT_START();

$PAGE_NAVIGATION[] = array($i_Notice_New);

echo $linterface->Include_AutoComplete_JS_CSS();
echo $linterface->Include_JS_CSS();

// [2015-0416-1040-06164] Merge CSV sample files
$sample_csv_file1 = GET_CSV("sample_data.csv", '', false);
$sample_csv_file1_userlogin = GET_CSV("sample_data_userlogin.csv", '', false);
$sample_csv_file2 = GET_CSV("sample_data_format2.csv", '', false);
$sample_csv_file2_userlogin = GET_CSV("sample_data_format2_userlogin.csv", '', false);
?>

<script language="javascript">
var AutoCompleteObj_ClassNameClassNumber;
$(document).ready(function() {
	// 23-09-2016 villa
    <?php if($plugin['eClassApp']) { ?>
        <?php if($lnotice->sendPushMsg) { ?>
			$('div#PushMessageSetting').show();
			$('div#sendTimeSettingDiv').show();
		<?php } else { ?>
			$('div#PushMessageSetting').hide();
			$('div#sendTimeSettingDiv').hide();
        <?php } ?>
	<?php } ?>
	
	var initStartDate = getMillisecond(getCurrentDate());
	var endDate = getMillisecond($("input#DateEnd").val());
	var diff = endDate - initStartDate;
	var newStartDate;
	
	$(document).click(function() {
		newStartDate = getMillisecond(getCurrentDate());
		if(isNaN(newStartDate)){
			return;
		}

		if(initStartDate != newStartDate){
			var newDateEnd = new Date(newStartDate+diff);
			jQuery.datepick._selectDay('#DateEnd',newDateEnd,this);
			initStartDate = newStartDate;
		}
	});

	eFormAppJS.func.init();
});

function getMillisecond(dateString){
	var millisecond = Date.parse(dateString);
	if(isNaN(millisecond)){
		dateString = dateString.replace(/-/g, '/');
		millisecond = Date.parse(dateString);
	}

	return Date.parse(dateString);
}

function getCurrentDate(){
	var date = $("#DateStart").val();
	return date;
}

function Init_JQuery_AutoComplete(InputID_ClassNameClassNumber, InputID_UserLogin) {
	AutoCompleteObj_ClassNameClassNumber = $("input#" + InputID_ClassNameClassNumber).autocomplete(
		"../ajax_search_user_by_classname_classnumber.php",
		{  			
			onItemSelect: function(li) {
				Add_Selected_User(li.extra[1], li.selectValue, InputID_ClassNameClassNumber);
			},
			formatItem: function(row) {
				return row[0];
			},
			maxItemsToShow: 100,
			minChars: 1,
			delay: 0,
			width: 200
		}
	);
	
	Update_Auto_Complete_Extra_Para();
}

function Add_Selected_User(UserID, UserName, InputID) {
	var UserID = UserID || "";
	var UserName = UserName || "";
	var UserSelected = document.getElementById('target[]');
	UserSelected.options[UserSelected.length] = new Option(UserName, UserID);
	
	Update_Auto_Complete_Extra_Para();
	
	// reset and refocus the textbox
	$('input#' + InputID).val('').focus();
}

function Update_Auto_Complete_Extra_Para(){
	checkOptionAll(document.getElementById('form1').elements["target[]"]);

	ExtraPara = new Array();
	ExtraPara['SelectedUserIDList'] = Get_Selection_Value('target[]', 'Array', true);
	AutoCompleteObj_ClassNameClassNumber[0].autocompleter.setExtraParams(ExtraPara);
}

function js_Remove_Selected_Student(){
	checkOptionRemove(document.getElementById('target[]'));
	Update_Auto_Complete_Extra_Para();
}

var no_of_upload_file = <? echo $no_file == "" ? 1 : $no_file; ?>;

function selectAudience()
{
	var tid = document.form1.type.value;
	document.getElementById('div_audience_err_msg').innerHTML = "";
	
	$('#div_audience').load(
		'ajax_loadAudience.php', 
		{type: tid}, 
		function (data){
			if(tid == 4){
				// initialize jQuery Auto Complete plugin
				eFormAppJS.func.init();
				Init_JQuery_AutoComplete('UserClassNameClassNumberSearchTb', 'UserLoginSearchTb');
				$('input#UserClassNameClassNumberSearchTb').focus();
			}
		});
}

function selectTemplate()
{
	var t_index = document.form1.templateID.selectedIndex;
	
	if(confirm("<?=$i_Form_chg_template?>"))
	{
		document.form1.CurrentTemplateIndex.value = t_index;
		var tid = document.form1.templateID.value;
		
		$('#div_content').load(
			'ajax_loadContent.php', 
			{templateID: tid, isIssuedNotice: 0, PATH_WRT_ROOT2:'<?=$PATH_WRT_ROOT?>', noticeAttFolder:'<?=$noticeAttFolder?>'}, 
			function (data)
			{
                <?php if($plugin['eClassApp']) { ?>
					$('div#PushMessageSetting').hide();
					$('div#sendTimeSettingDiv').hide();
                <?php } ?>
				
				// call tb_init() again > prevent cannot open thickbox
				tb_init('a.thickbox');

				// rebind click action > prevent cannot search PIC
				eFormAppJS.func.init();
                <?php if($plugin['eClassApp']) { ?>
					checkedSendPushMessage();
				<?php } ?>
			}
		); 
	}
	else
	{
		document.form1.templateID.selectedIndex = document.form1.CurrentTemplateIndex.value;
	}
}

function reset_innerHtml()
{
 	document.getElementById('div_audience_err_msg').innerHTML = "";
 	document.getElementById('div_NoticeNumber_err_msg').innerHTML = "";
 	document.getElementById('div_Title_err_msg').innerHTML = "";
 	document.getElementById('div_DateStart_err_msg').innerHTML = "";
 	document.getElementById('div_DateEnd_err_msg').innerHTML = "";
}

function checkform(obj)
{
	var error_no = 0;
	var focus_field = "";
	var csv_error = false;
	
	//// Reset div innerHtml
	reset_innerHtml();

    // disable submit button
	$('#submit2').attr('disabled', true);
    $('#submit2').removeClass("formbutton_v30").addClass('formbutton_disable_v30');
	
	// If seleted as template and audience not been choicen, 'Whole School' would be selected
	if($('input[name="sus_status"]:checked').val() == 3 && obj.type.value == ''){
		obj.type.value = 1;
	}

	// if (obj.ContentType.value != "2") {
	if ($("input[name='ContentType']:checked").val() != "2") {
		if(obj.type.value == "")
		{
			document.getElementById('div_audience_err_msg').innerHTML = '<font color="red"><?=$i_alert_pleaseselect?><?=$Lang['eNotice']['Audience']?></font>';
			error_no++;
			focus_field = "type";
		}
			
		if(obj.type.value == 2)
		{
			if(!countChecked(obj, "target[]"))
			{ 
				document.getElementById('div_audience_err_msg').innerHTML = '<font color="red"><?=$i_alert_pleaseselect?><?=$i_Notice_RecipientLevel?></font>';
				error_no++;
				focus_field = "type";
			}
		}
	
		if(obj.type.value == 3)
		{
			if(!countChecked(obj, "target[]"))
			{ 
				document.getElementById('div_audience_err_msg').innerHTML = '<font color="red"><?=$i_alert_pleaseselect?><?=$i_Notice_RecipientClass?></font>';
				error_no++;
				focus_field = "type";
			}
		}
	
	    if(obj.type.value == 4)
	    {
		    checkOptionAll(obj.elements["target[]"]);
		    
			if(obj.elements["target[]"].length == 0)
			{ 
				document.getElementById('div_audience_err_msg').innerHTML = '<font color="red"><?=$i_frontpage_campusmail_choose?></font>';
				error_no++;
				focus_field = "type";
			}
		}
	}
	
	if(obj.elements["target_PIC[]"].length != 0)
	{
		checkOptionAll(obj.elements["target_PIC[]"]);
	}
	
	if(!check_text_30(obj.NoticeNumber, "<?php echo $i_alert_pleasefillin.$i_Notice_NoticeNumber; ?>.", "div_NoticeNumber_err_msg"))
	{
		error_no++;
		if(focus_field == "")	focus_field = "NoticeNumber";
	}
	 
	if(!check_text_30(obj.Title, "<?php echo $i_alert_pleasefillin.$i_Notice_Title; ?>.", "div_Title_err_msg"))
	{
		error_no++;
		if(focus_field == "")	focus_field = "Title";
	}
	
	/*
	if (!check_date_30(obj.DateStart,"<?php echo $i_invalid_date; ?>", "div_DateStart_err_msg")) 
	{
		 error_no++;
		if(focus_field=="")	focus_field = "DateStart";
	}
	
	if (!check_date_30(obj.DateEnd,"<?php echo $i_invalid_date; ?>", "div_DateEnd_err_msg"))
	{
		error_no++;
		if(focus_field=="")	focus_field = "DateEnd";
	}
	*/
	
	if(!check_date_without_return_msg(obj.DateStart))
	{
		error_no++;
		if(focus_field == "")	focus_field = "DateStart";
	}
	
	if(!check_date_without_return_msg(obj.DateEnd))
	{
		error_no++;
		if(focus_field == "")	focus_field = "DateEnd";
	}
	
	if (compareDate(obj.DateStart.value, obj.DateEnd.value) > 0) 
	{
		document.getElementById('div_DateEnd_err_msg').innerHTML = '<font color="red"><?=$i_Homework_new_duedate_wrong?></font>';
		error_no++;
		if(focus_field == "")	focus_field = "DateEnd";
	}
	
	<? /* ?>
	if (compareDate('<?=date('Y-m-d')?>', obj.DateStart.value) > 0)
	{
		document.getElementById('div_DateStart_err_msg').innerHTML = '<font color="red"><?=$i_Homework_new_startdate_wrong?></font>';
		error_no++;
		if(focus_field=="")	focus_field = "DateStart";
	}
	<? */ ?>

	var scheduleString = '';
    if ($('input#sendTimeRadio_scheduled').attr('checked')) {
    	scheduleString = $('input#sendTime_date').val() + ' ' + str_pad($('select#sendTime_hour').val(),2) + ':' + str_pad($('select#sendTime_minute').val(),2) + ':00';
    }
    $('input#sendTimeString').val(scheduleString);
    
    var minuteValid = false;
	if (str_pad($('select#sendTime_minute').val(),2) == '00' || str_pad($('select#sendTime_minute').val(),2) == '15' || str_pad($('select#sendTime_minute').val(),2) == '30' || str_pad($('select#sendTime_minute').val(),2) == '45') {
		minuteValid = true;
	}
	
	// if scheduled message => check time is past or not
	var dateNow = new Date();
	var dateNowString = dateNow.getFullYear() + '-' + str_pad((dateNow.getMonth()+1),2) + '-' + str_pad(dateNow.getDate(),2) + ' ' + str_pad(dateNow.getHours(),2) + ':' + str_pad(dateNow.getMinutes(),2) + ':' + str_pad(dateNow.getSeconds(),2);
	if($('#pushmessagenotify').attr('checked') || $('#pushmessagenotify_Students').attr('checked') || $('#pushmessagenotify_PIC').attr('checked') || $('#pushmessagenotify_ClassTeachers').attr('checked')){    	
		if ($('input#sendTimeRadio_scheduled').attr('checked') && minuteValid && scheduleString <= dateNowString) {
			// alert('<?=$Lang['AppNotifyMessage']['WarningMsg']['SendTimeInvalid']?>');
			// return false;
			document.getElementById('div_push_message_date_err_msg').innerHTML = '<font color="red"><?=$Lang['AppNotifyMessage']['WarningMsg']['SendTimeInvalid']?></font>';
			error_no++;
			if(focus_field == "")	focus_field = "sendTime_date";
		}
	}
	
	// if scheduled message => check time is within 30 days
	var maxDate = new Date();
	maxDate.setDate(dateNow.getDate() + 31);
	var maxDateString = maxDate.getFullYear() + '-' + str_pad((maxDate.getMonth()+1),2) + '-' + str_pad(maxDate.getDate(),2) + ' ' + str_pad(maxDate.getHours(),2) + ':' + str_pad(maxDate.getMinutes(),2) + ':' + str_pad(maxDate.getSeconds(),2);
	if($('#pushmessagenotify').attr('checked') || $('#pushmessagenotify_Students').attr('checked') || $('#pushmessagenotify_PIC').attr('checked') || $('#pushmessagenotify_ClassTeachers').attr('checked')){ 
    	if ($('input#sendTimeRadio_scheduled').attr('checked') && minuteValid && scheduleString > maxDateString) {
    		// alert('<?=$Lang['AppNotifyMessage']['WarningMsg']['PleaseSelectDateWithinRange']?>');
    		$('input#sendTime_date').focus();
    		// return false;
    		document.getElementById('div_push_message_date_err_msg').innerHTML = '<font color="red"><?=$Lang['AppNotifyMessage']['WarningMsg']['PleaseSelectDateWithinRange']?></font>';
    		error_no++;
    		if(focus_field == "")	focus_field = "sendTime_date";
    	}
	}
	
	// if scheduled message => check time is earlier than issue time
	if($('#pushmessagenotify').attr('checked') || $('#pushmessagenotify_Students').attr('checked') || $('#pushmessagenotify_PIC').attr('checked') || $('#pushmessagenotify_ClassTeachers').attr('checked')){
		var DateStartString = $('input#DateStart').val() + ' ' + str_pad($('#DateStart_hour').val(),2)+':'+str_pad($('#DateStart_min').val(),2)+':00';
		if ($('input#sendTimeRadio_scheduled').attr('checked') && minuteValid && scheduleString < DateStartString) {
			$('input#sendTime_date').focus();
			// return false;
			document.getElementById('div_push_message_date_err_msg').innerHTML = '<font color="red"><?=$Lang['AppNotifyMessage']['WarningMsg']['SendTimeMustAfterStartDate']?></font>';
			error_no++;
			if(focus_field == "")	focus_field = "sendTime_date";
		}
	}

	// assign time into hidden field
	var dateStartTime = str_pad($('#DateStart_hour').val(),2)+':'+str_pad($('#DateStart_min').val(),2)+':00';
	$('#DateStartTime').val(dateStartTime);
	var dateEndTime = str_pad($('#DateEnd_hour').val(),2)+':'+str_pad($('#DateEnd_min').val(),2)+':59';
	$('#DateEndTime').val(dateEndTime);
	
	// check merge csv file for Content Type: Merge Content
	if ($('input#ContentType2').attr('checked'))
	{
		objCSVFile = eval('document.form1.FileCSV');
		objCSVFileName = eval('document.form1.FileCSV_Name');
		// assign filename to FileCSV_Name
		if(objCSVFile != null && objCSVFile.value != '' && objCSVFileName != null)
	 	{
		 	var fileAry = objCSVFile.value.split('\\');
		 	objCSVFileName.value = fileAry[fileAry.length - 1];
		 	// Empty filename or not CSV file
			if($.trim(objCSVFileName.value) == "" || objCSVFileName.value.indexOf(".csv") == -1)
			{
				alert('<?=$Lang['eNotice']['ProblemNoCSV']?>');
				csv_error = true;
			}
		}
		else
		{
			alert('<?=$Lang['eNotice']['ProblemNoCSV']?>');
			csv_error = true;
		}
	}
	
	// check is type radio is checked
	var sus_status = $('input[name=sus_status]:checked').val();
	if(sus_status > 0){
		//do nothing
	}
	else{
		error_no++;
		if(focus_field == "")	focus_field = "status1";
	}
	
	var canSubmit = false;
	if(error_no > 0)
	{
		eval("obj." + focus_field +".focus();");
//		return false;
		$('#submit2').attr('disabled', false);
		canSubmit = false;
	}
	else if(csv_error){
		$('#submit2').attr('disabled', false);
		canSubmit = false;
	}
	else
	{
		//checkOption(obj.elements['Attachment[]']);
		//checkOptionAll(obj.elements["Attachment[]"]);
		//return true
		
		// create a list of files to be deleted
        objDelFiles = document.getElementsByName('file2delete[]');
        files = obj.deleted_files;
        if(objDelFiles == null) {
            return true;
        }

        x = "";
        for(i=0; i<objDelFiles.length; i++){
	        if(objDelFiles[i].checked == true){
		 		x += objDelFiles[i].value + ":";
		    }
	    }
	    files.value = x.substr(0,x.length-1);
	    
//		return Big5FileUploadHandler();
		canSubmit = Big5FileUploadHandler();
	}
	
	if (canSubmit) {
		$('form#form1').submit();
	}
	else {
		$('#submit2').attr('disabled', false);
        $('#submit2').removeClass("formbutton_disable_v30").addClass('formbutton_v30');
	}
}

function Big5FileUploadHandler() {
	for(i=0; i<=no_of_upload_file; i++)
	{
	 	objFile = eval('document.form1.filea'+i);
	 	objUserFile = eval('document.form1.hidden_userfile_name'+i);
	 	if(objFile != null && objFile.value != '' && objUserFile != null)
	 	{
		 	var Ary = objFile.value.split('\\');
		 	objUserFile.value = Ary[Ary.length - 1];
		}
	}
	return true;
}

function add_field()
{
	var table = document.getElementById("upload_file_list");
	var row = table.insertRow(no_of_upload_file);
	var cell = row.insertCell(0);
	x = '<input class="file" type="file" name="filea' + no_of_upload_file + '" size="40">';
	x += '<input type="hidden" name="hidden_userfile_name' + no_of_upload_file + '">';

	cell.innerHTML = x;
	no_of_upload_file++;
	
	document.form1.attachment_size.value = no_of_upload_file;
}

function setRemoveFile(index,v){
	obj = document.getElementById('a_' + index);
	obj.style.textDecoration = v?"line-through":"";
}

function ShowCSVFormat(FormatNo)
{
	if (FormatNo == 1)
	{
		$('#CSV_Sample2').hide();
		$('#CSV_Sample1').show();
	} 
	else
	{
		$('#CSV_Sample1').hide();
		$('#CSV_Sample2').show();
	}
}

function ShowMergeSetting(FormatNo)
{
	if (FormatNo == 1)
	{
		$('#MergeCSVTr').hide();
		$('#MergeTypeTr').hide();
		$('#mergeRemark').hide();
		$('tr.selectStudentArea').show();
	} 
	else
	{
		$('#MergeCSVTr').show();
		$('#MergeTypeTr').show();
		$('#mergeRemark').show();
		$('tr.selectStudentArea').hide();
		$('#div_audience .select_studentlist').empty();
	}
}

function previewCSVFile() {
	window.setTimeout(function(){
		var error_no = 0;
		var focus_field = "";
		
		// reset warning msg
		reset_innerHtml();
		
		$('#PreviewBtn').attr('disabled', true);
		
		// get form settings
		var formObj = document.form1;
		var oldTarget = document.form1.target;
		var oldAction = document.form1.action;

		if ($("input[name='ContentType']:checked").val() != "2")
		{
			// Audience
			if(formObj.type.value == "")
			{
				document.getElementById('div_audience_err_msg').innerHTML = '<font color="red"><?=$i_alert_pleaseselect?><?=$Lang['eNotice']['Audience']?></font>';
				focus_field = "type";
			}
			if(formObj.type.value == 2)
			{
				if(!countChecked(formObj, "target[]"))
				{ 
					document.getElementById('div_audience_err_msg').innerHTML = '<font color="red"><?=$i_alert_pleaseselect?><?=$i_Notice_RecipientLevel?></font>';
					focus_field = "type";
				}
			}
			if(formObj.type.value == 3)
			{
				if(!countChecked(formObj, "target[]"))
				{ 
					document.getElementById('div_audience_err_msg').innerHTML = '<font color="red"><?=$i_alert_pleaseselect?><?=$i_Notice_RecipientClass?></font>';
					focus_field = "type";
				}
			}
		    if(formObj.type.value == 4)
		    {
			    checkOptionAll(formObj.elements["target[]"]);
			    
				if(formObj.elements["target[]"].length == 0)
				{ 
					document.getElementById('div_audience_err_msg').innerHTML = '<font color="red"><?=$i_frontpage_campusmail_choose?></font>';
					focus_field = "type";
				}
			}
		}
		
		// check merge csv file
		var withCSVFile = false;
		if ($('input#ContentType2').attr('checked'))
		{
			objCSVFile = eval('document.form1.FileCSV');
			objCSVFileName = eval('document.form1.FileCSV_Name');

			// assign filename to FileCSV_Name
			if(objCSVFile != null && objCSVFile.value != '' && objCSVFileName != null)
		 	{
			 	var fileAry = objCSVFile.value.split('\\');
			 	objCSVFileName.value = fileAry[fileAry.length - 1];

			 	// Not empty filename and CSV file
				if($.trim(objCSVFileName.value) != "" && objCSVFileName.value.indexOf(".csv") != -1)
				{
					withCSVFile = true;
				}
			}
		}
		
		// valid csv file
		if(withCSVFile && focus_field == ""){
			// open thickbox
			$('#previewLink').click()
				
			// settings for preview
			formObj.target = 'csvPreviewFrame';
			formObj.action = '../preview_merge_csv.php';
			
			// form submit for preview
			$('form#form1').submit();
				
			// reset settings for normal submit 
			formObj.target = oldTarget;
			formObj.action = oldAction;
			
			// update parms
			$('#previewCSV').val(1);
		}
		// Empty Audience
		else if(focus_field != ""){
			eval("formObj." + focus_field + ".focus();");
		}
		// invalid or empty file
		else if(!withCSVFile){
			alert('<?=$Lang['eNotice']['ProblemNoCSV']?>');
		}
		
		$('#PreviewBtn').attr('disabled', false);
		
	}, 500);
}

<?php if($plugin['eClassApp']) { ?>
	function checkedSendPushMessage(checkBox) {
	//	if (checkBox.checked == true) {
	//		$('div#sendTimeSettingDiv').show();
	//	}
	//	else {
	//		$('div#sendTimeSettingDiv').hide();
	//	}
		
	    if($('#pushmessagenotify').attr('checked') || $('#pushmessagenotify_Students').attr('checked') || $('#pushmessagenotify_PIC').attr('checked') || $('#pushmessagenotify_ClassTeachers').attr('checked')){    	
	    	$('div#sendTimeSettingDiv').show();
	    	$('div#PushMessageSetting').show();
	    }
	    else{    	
	    	$('div#sendTimeSettingDiv').hide();
	    	$('div#PushMessageSetting').hide();
	    }
	}
	
	function clickedSendTimeRadio(targetType) {
		if (targetType == 'now') {
			$('div#specificSendTimeDiv').hide();
		}
		else {
			$('div#specificSendTimeDiv').show();
		}
	}
<?php } ?>

//function js_Show_Merge_Preview() {
//	var formObj = document.form1;
//	
//	// get form settings
//	var oldTarget = document.form1.target;
//	var oldAction = document.form1.action;
//	
//	// check merge csv file
//	var withCSVFile = false;
//	if ($('input#ContentType2').attr('checked')){
//		objCSVFile = eval('document.form1.FileCSV');
//		objCSVFileName = eval('document.form1.FileCSV_Name');
//		// assign filename to FileCSV_Name
//		if(objCSVFile!=null && objCSVFile.value!='' && objCSVFileName!=null)
//	 	{
//		 	var fileAry = objCSVFile.value.split('\\');
//		 	objCSVFileName.value = fileAry[fileAry.length-1];
//			if($.trim(objCSVFileName.value)!="")
//			{
//				withCSVFile = true;
//			}
//		}
//	}
//	
//	// valid csv file
//	if(withCSVFile){
//		// settings for preview
//		formObj.target = '_blank';
//		formObj.action = '../preview_merge_csv.php';
//		
//		// form submit for preview
//		checkform(formObj);
//		
//		// reset settings for normal submit 
//		formObj.target = oldTarget;
//		formObj.action = oldAction;
//		$('#submit2').attr('disabled', false);
//		
//		// update parms
//		$('#previewCSV').val(1);
//	}
//}
</script>

<?= $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION) ?>
<br>
<form name="form1" id="form1" method="post" action="new_update.php" enctype="multipart/form-data" >

<?php
### UserLogin
$UserPICTextarea = '';
$UserPICTextarea .= '<div style="float:left;" class="pic_textarea">';
$UserPICTextarea .= '<textarea style="height:100px;" id="PICAreaSearchTb" name="PICAreaSearchTb"></textarea><span id="textarea_msg"></span><br><br>';
$UserPICTextarea .= $linterface->GET_SMALL_BTN($Lang['AppNotifyMessage']['searchAndInsertPIC'], "button" , "");
$UserPICTextarea .= '</div>';

// <table class="form_table_v30">
$STUDENT_INPUT = "<tr valign='top' class='selectStudentArea'>
					<td nowrap class='field_title'><span class='tabletextrequire'>*</span>" . $i_Notice_RecipientType . "</td>
					<td>" . $type_selection . "<br><span id='div_audience_err_msg'></span>
					<div id='div_audience'></div>
					</td>
				</tr>
				<!-- Select PICs of eNotice -->
				<tr valign='top'>
					<td nowrap class='field_title'>" . $i_Profile_PersonInCharge . "</td>
					<td><table border='0' cellpadding='0' cellspacing='0'>
					<tr>
						<td class='tablerow2'>" . $Lang['AppNotifyMessage']['msgSearchAndInsertPICInfo'] . "<br>" . $UserPICTextarea. "</td>
						<td class='tabletext' ><img src='". $image_path.'/'.$LAYOUT_SKIN."/10x10.gif' width='10' height='1'></td>
						<td>" . $linterface->GET_SELECTION_BOX($PICUser, "id='target_PIC' name='target_PIC[]' size='6' multiple", "") . "</td>
						<td valign='bottom'>" . $linterface->GET_BTN($Lang['Btn']['Select'], "button", "newWindow('/home/common_choose/index.php?fieldname=target_PIC[]&permitted_type=1&DisplayGroupCategory=1&DisplayInternalRecipientGroup=1&Disable_AddGroup_Button=1', 16)")
						. "<br>" . $linterface->GET_BTN($Lang['Btn']['Remove'], "button", "checkOptionRemove(document.form1.elements['target_PIC[]'])") . "</td>
					</tr>
					</table></td>
				</tr>";
$TEMPLATE_SELECCTION = "<tr>
	<td class='field_title'>" . $i_Notice_FromTemplate . "</td>
	<td>" . $template_selection . "</td>
</tr>"; //<!--TEMPLATE_SELECTION-->
// </table>
/*
<table class="form_table_v30">
<tr valign='top'>
	<td nowrap class='field_title'><span class='tabletextrequire'>*</span><?=$i_Notice_RecipientType?></td>
	<td><?=$type_selection?><br><span id="div_audience_err_msg"></span>
	<div id="div_audience"></div>
	</td>
</tr>

<!-- Select PICs of eNotice -->
<tr valign='top'>
	<td nowrap class='field_title'><?=$i_Profile_PersonInCharge?></td>
	<td><table border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td><?=$linterface->GET_SELECTION_BOX($PICUser, "id='target_PIC' name='target_PIC[]' size='6' multiple", "");?></td>
				<td valign="bottom">
					<?=$linterface->GET_BTN($Lang['Btn']['Select'], "button", "newWindow('/home/common_choose/index.php?fieldname=target_PIC[]&permitted_type=1&DisplayGroupCategory=1&DisplayInternalRecipientGroup=1&Disable_AddGroup_Button=1', 16)");?>
					<br>
					<?=$linterface->GET_BTN($Lang['Btn']['Remove'], "button", "checkOptionRemove(document.form1.elements['target_PIC[]'])");?>
				</td>
			</tr>
	</table></td>
</tr>


<tr>
	<td class='field_title'><?=$i_Notice_FromTemplate?></td>
	<td><?=$template_selection?></td>
</tr>
</table>
*/ ?>

<div id="div_content">
<?php
$formBody = $lnotice->return_FormContent($start, $end,0,'','','','',1,1,$noticeAttFolder,'','',($plugin['eClassApp'] ? 1 : 0));
$formBody = str_replace("<!--TEMPLATE_SELECTION-->", $TEMPLATE_SELECCTION, $formBody);
echo str_replace("<!--___STUDENT_INPUT___-->", $STUDENT_INPUT, $formBody);
?>
</div>

<?=$linterface->MandatoryField();?>
<div class="edit_bottom_v30">
	<?//= $linterface->GET_ACTION_BTN($button_submit, "submit", "checkOption(this.form.elements['Attachment[]']); ". ($type==4 ? "checkOption(this.form.elements['target[]']);" :"") ."this.form.action='new_update.php'; this.form.method='post'; return checkform(this.form);","submit2") ?>
	<?= $linterface->GET_ACTION_BTN($button_submit, "button", ($type==4 ? "checkOption(this.form.elements['target[]']);" :"") ." checkform(this.form);","submit2") ?>
	<?= $linterface->GET_ACTION_BTN($button_reset, "reset") ?>
	<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='index.php'","cancelbtn") ?>
</div>

<input type="hidden" name="pageNo" value="<?=$pageNo?>" />
<input type="hidden" name="field" value="<?=$field?>" />
<input type="hidden" name="page_size_change" value="<?=$page_size_change?>" />
<input type="hidden" name="numPerPage" value="<?=$numPerPage?>" />

<input type="hidden" name="noticeType" value="<?=$noticeType?>" />
<input type="hidden" name="noticeAttFolder" value="<?=$noticeAttFolder?>" />
<input type="hidden" name="status" value="<?=$status?>" />
<input type="hidden" name="year" value="<?=$year?>" />
<input type="hidden" name="month" value="<?=$month?>" />

<input type="hidden" name="CurrentTemplateIndex" value="0">

<input type="hidden" name="deleted_files" value='' />
<input type="hidden" name='attachment_size' value='<? echo $no_file==""?1:$no_file;?>'>
</form>

<script language="JavaScript1.2">
/*
obj = document.form1.elements["Attachment[]"];
checkOptionClear(obj);
<?
//	while (list($key, $value) = each($files)) 
//        	echo "checkOptionAdd(obj, \"".$files[$key][0]." (".ceil($files[$key][1]/1000)."Kb".")\", \"".$files[$key][0]."\");\n";  
?>
checkOptionAdd(obj, "<?php /*for($i = 0; $i < 40; $i++) echo " ";*/ ?>", "");
*/

function js_show_email_notification(val)
{
	if(val == 1 || val == 4)
	{
        <?php if($plugin['eClassApp']) { ?>
			$('#pushmessagenotify').removeAttr('disabled');
			$('#pushmessagenotify_Students').removeAttr('disabled');
			$('#pushmessagenotify_PIC').removeAttr('disabled');
			$('#pushmessagenotify_ClassTeachers').removeAttr('disabled');
			
			// Villa 2016-10-18 
			var parentCheck = document.getElementById("pushmessagenotify");
			var studnetCheck = document.getElementById("pushmessagenotify_Students");
			var picCheck = document.getElementById("pushmessagenotify_PIC");
			var classTeacherCheck = document.getElementById("pushmessagenotify_ClassTeachers");
			if((parentCheck && parentCheck.check == true) || (studnetCheck && studnetCheck.check == true) || (picCheck && picCheck.check == true) || (classTeacherCheck && classTeacherCheck.check == true)){
				$('div#sendTimeSettingDiv').show();
				$('div#PushMessageSetting').show();
			}
		<?php } ?>
		$('#emailnotify').removeAttr('disabled');
		$('#emailnotify_Students').removeAttr('disabled');
		$('#emailnotify_PIC').removeAttr('disabled');
		$('#emailnotify_ClassTeachers').removeAttr('disabled');
	}
	else
	{
        <?php if($plugin['eClassApp']) { ?>
			$('#pushmessagenotify').removeAttr('checked');
			$('#pushmessagenotify_Students').removeAttr('checked');
			$('#pushmessagenotify_PIC').removeAttr('checked');
			$('#pushmessagenotify_ClassTeachers').removeAttr('checked');
			
			$('#pushmessagenotify').attr('disabled','true');
			$('#pushmessagenotify_Students').attr('disabled','true');
			$('#pushmessagenotify_PIC').attr('disabled','true');
			$('#pushmessagenotify_ClassTeachers').attr('disabled','true');

			$('div#PushMessageSetting').hide();
	        $('div#sendTimeSettingDiv').hide();
		<?php } ?>
		
		$('#emailnotify').removeAttr('checked');
		$('#emailnotify_Students').removeAttr('checked');
		$('#emailnotify_PIC').removeAttr('checked');
		$('#emailnotify_ClassTeachers').removeAttr('checked');
		
		$('#emailnotify').attr('disabled','true');
		$('#emailnotify_Students').attr('disabled','true');
		$('#emailnotify_PIC').attr('disabled','true');
		$('#emailnotify_ClassTeachers').attr('disabled','true');
	}
}

<?php if($sys_custom['eNotice']['SpecialNoticeForPICAndAdmin']) { ?>
    function js_handle_special_notice_options_display()
    {
        var val = $('input[name=SpecialNotice]:checked').val();
        if(val == 1) {
            $('#emailnotify_ClassTeachers').removeAttr('checked');
            $('#emailnotify_ClassTeachers').attr('disabled','true');
            $('#span_Emailnotify_ClassTeachers').hide();

            <?php if($plugin['eClassApp']) { ?>
                $('#pushmessagenotify_ClassTeachers').removeAttr('checked');
                $('#pushmessagenotify_ClassTeachers').attr('disabled','true');
                $('#span_Pushmessagenotify_ClassTeachers').hide();
                checkedSendPushMessage($('#pushmessagenotify_ClassTeachers'));
            <?php } ?>
        }
        else {
            var sus_status = $('input[name=sus_status]:checked').val();
            if(sus_status == 1 || sus_status == 4) {
                $('#emailnotify_ClassTeachers').removeAttr('disabled');
            }
            $('#span_Emailnotify_ClassTeachers').show();

            <?php if($plugin['eClassApp']) { ?>
                if(sus_status == 1 || sus_status == 4) {
                    $('#pushmessagenotify_ClassTeachers').removeAttr('disabled');
                }
                $('#span_Pushmessagenotify_ClassTeachers').show();
            <?php } ?>
        }
    }
<?php } ?>

var eFormAppJS = {
	vars: {
		ajaxURL: "../ajax_search_user_by_classname_classnumber.php",
		xhr: "",
		selectedUserContainer: "UserClassNameClassNumberSearchTb"
	},
	listener: {
		insertPICFromTextarea: function(e) {
			var textarea = $('div.pic_textarea textarea').eq(0);

			if (textarea.val() == "") {
				textarea.focus();
			} else {
				eFormAppJS.func.ajaxFromTextArea(textarea, "pic");
			}
		},
		insertFromTextarea: function(e) {
			var textarea = $('div.student_textarea textarea').eq(0);
			if (textarea.val() == "") {
				textarea.focus();
			} else {
				eFormAppJS.func.ajaxFromTextArea(textarea, "student");
			}
			e.preventDefault();
		}
	},
	func: {
		ajaxFromTextArea: function(textarea, contentType) {
			var targetSelectID = "target[]";
			var textareaMsgObj = '#textarea_studentmsg';
			switch (contentType) {
				case "pic":
					textareaMsgObj = '#textarea_msg';
					targetSelectID = "target_PIC";
					break;
			}
			if (textarea.val() == "") {
				textarea.focus();
				$(textareaMsgObj).css({"color":"#f00"}).html('<br>' + '<?php echo $Lang['eNotice']['WrongUserLogin']; ?>');
			} else {
				var xhr = eFormAppJS.vars.xhr;
				if (xhr != "") {
					xhr.abort();
				}

				var textareaVal = textarea.val();
				var getTargetListVal = "";
				if (contentType == "pic") {
					$('select#' + targetSelectID + ' option').each(function(ndex, obj) {
						if (getTargetListVal != "") {
							getTargetListVal += ",";
						}
						getTargetListVal += $(this).val();
					});
				}
				else {
					$('select[name="' + targetSelectID + '"] option').each(function(ndex, obj) {
						if (getTargetListVal != "") {
							getTargetListVal += ",";
						}
						getTargetListVal += $(this).val();
					});
				} 
				
				eFormAppJS.vars.xhr = $.ajax({
					url: eFormAppJS.vars.ajaxURL,
					type: 'post',
					data: { "q": textareaVal.split("\n").join("||"), "searchfrom":"textarea", "nt_userType" : contentType, "SelectedUserIDList" : getTargetListVal },
					error: function() {
						textarea.focus();
					},
					success: function(data) {
						var lineRec = data.split("\n");
						if (lineRec.length > 0) {
							var UserSelected = "";
							if (contentType == "pic") {
								UserSelected = $('#' + targetSelectID);
							} else {
								UserSelected = $('select[name="' + targetSelectID + '"]').eq(0);
							}
							// console.log(UserSelected);
							var afterTextarea = "";
							$.each(lineRec, function(index, data) {
								if (data != "") {
									var tmpData = data.split("||");
									if (typeof tmpData[0] != "undefined" && typeof tmpData[1] != "undefined" && tmpData[1] != "NOT_FOUND") {  
										// Add_Selected_User(tmpData[1], tmpData[0], eFormAppJS.vars.selectedUserContainer);
										// UserSelected.options[UserSelected.length] = new Option(tmpData[0], tmpData[1]);
										UserSelected.append($("<option></option>")
							                    .attr("value", tmpData[1])
							                    .text(tmpData[0])); 
										
									} else {
										if (afterTextarea != "") {
											afterTextarea += "\n";
										}
										afterTextarea += tmpData[2];									
									}
								}
							});
							if (afterTextarea != "") {
								textarea.val(afterTextarea);
								$(textareaMsgObj).css({"color":"#f00"}).html('<br>' + '<?php echo $Lang['eNotice']['WrongUserLogin']; ?>');
							} else {
								textarea.val("");
								$(textareaMsgObj).html('');
							}
							checkOptionAll(document.getElementById('form1').elements[targetSelectID]);
						}
						else {
							textarea.focus();
							$(textareaMsgObj).css({"color":"#f00"}).html('<br>' + '<?php echo $Lang['eNotice']['WrongUserLogin']; ?>');
						}
					}
				});
			}
		},
		init: function() {
			$('.form_table_v30 tr td tr td').css({"border-bottom":"0px"});

			$('div.student_textarea input[type="button"]').unbind('click', eFormAppJS.listener.insertFromTextarea);
			$('div.pic_textarea input[type="button"]').unbind('click', eFormAppJS.listener.insertPICFromTextarea);
			
			$('div.student_textarea input[type="button"]').bind('click', eFormAppJS.listener.insertFromTextarea);
			$('div.pic_textarea input[type="button"]').bind('click', eFormAppJS.listener.insertPICFromTextarea);
		}
	}
};
</script>

<div id="csvPreviewDiv" style="display: none;">
<iframe width="750px" height="450px" name="csvPreviewFrame" id="csvPreviewFrame" frameBorder="0" seamless='seamless'></iframe>
</div>

<?
intranet_closedb();

print $linterface->FOCUS_ON_LOAD("form1.type");
$linterface->LAYOUT_STOP();
?>