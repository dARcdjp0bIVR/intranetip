<?php
//Modifying by: 

############ Change Log Start ###############
#
#   Date:   2020-11-10  Bill    [EJ DM#1444]
#           Approve / Reject - hide action button, to fix multiple submit
#
#   Date:   2020-09-14  Bill    [DM#3780]
#           show payment notice tab only if has issue right
#
# 	Date:   2020-09-07  Bill    [2020-0604-1821-16170]
#			updated access right checking
#
#	Date:	2020-06-10	YatWoon [Case#T187245]
#			improved: fixed the "PIC" column width 
#
#   Date:   2020-03-13 Tommy
#           need flag - $special_feature['ePaymentNotice'] to show "payment notice" in powerclass
#
#   Date:   2019-10-14  Bill    [DM#3677]
#           improved: added drop down option - My Class Notice
#           fixed: Issued Notice / In-charge Notice displayed payment notice
#
#   Date:   2018-10-08  Isaac
#           Added navigation for payment notice to $TAGS_OBJ[] for PowerClass
#
#	Date:	2016-08-19	Kenneth
#			Add logic: Show Approval Tab Only if (approvalSettings is on OR there is more than one notice pending for approval)
#
#	Date:	2016-05-19	Kenneth
#			Add Tab for pending approval notice
#
#	Date:	2016-04-07	Bill
#			Fixed: display header Signed/Total for current user with Issue Right
#
#	Date:	2016-03-23	Kenneth	
# 			add filter - Pending Approval (recordStatus = 4)
#
#	Date:	2015-05-18	Bill	[2015-0428-1214-11207]
# 			Improved: Display PICs of eNotice
#
#	Date:	2015-04-21	Bill	[2015-0420-1504-59207]
#			Improved: sort notice by Issue Date, Notice ID in descending order
#
#	Date:	2015-04-14	Bill	[2015-0323-1602-46073]
#			Improved: add In-charge Notice to notice type drop down list
#			Fixed: add js function viewPaymentNotice() to prevent cannot preview payment notice
#
#	Date:	2015-02-03 Omas	> ip.2.5.6.3.1
#			Improved: add sorting to DateStart, DateEnd, NoticeNumber
#
#	Date:	2014-06-23 YatWoon	> ip.2.5.5.8.1
#			Improved: Revised system return message method [Case#Q63447]
#
#	Date:	2011-09-30 YatWoon
#			Enhanced: add "Copy" function [Case#2011-0802-1242-05071]
#
#	Date:	2010-11-29	YatWoon
#			after create a new notice, back to index page with popup for preview (with flag $sys_custom['PopupNoticeAfterCreated'])
#
############ Change Log End ###############

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

// [2020-0604-1821-16170]
//if (!$plugin['notice'] || $_SESSION["SSV_PRIVILEGE"]["notice"]["disabled"] || !$_SESSION["SSV_PRIVILEGE"]["notice"]["hasIssueRight"])
if (!$plugin['notice'] || $_SESSION["SSV_PRIVILEGE"]["notice"]["disabled"] || !$_SESSION["SSV_PRIVILEGE"]["notice"]["hasSchoolNoticeIssueRight"])
{
	if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eNotice"])
	{
		header("location: ./settings/basic_settings/");	
		exit;
	}
	
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."lang/email.php");

intranet_auth();
intranet_opendb();

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

$CurrentPageArr['eAdminNotice'] = 1;
$CurrentPage = "PageStudentNotice";

$lnotice = new libnotice();
$lclass = new libclass();

$linterface = new interface_html();

if($_GET['pendingApproval']){
	if($status != 5)$status = 4;
}else{
	if ($status != 2 && $status != 3) $status = 1;
}

// [2020-0604-1821-16170]
//if ($lnotice->hasIssueRight())
if ($lnotice->hasSchoolNoticeIssueRight())
{
    //$AddBtn = "<a href=\"javascript:newNotice()\" class='contenttool'><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_new.gif\" width=\"18\" height=\"18\" border=\"0\" align=\"absmiddle\" /> " . $button_new . "</a>";
    $AddBtn = $linterface->GET_LNK_ADD("javascript:newNotice()", $button_new ,"","","",0);
    
//     $status_select = "<SELECT name='status' onChange=this.form.submit()>\n";
//     $status_select.= "<OPTION value=4 ".($status==4? "SELECTED":"").">".$Lang['eNotice']['PendingApproval']."</OPTION>\n";
//     $status_select.= "<OPTION value=1 ".($status==1? "SELECTED":"").">$i_Notice_StatusPublished</OPTION>\n";
//     $status_select.= "<OPTION value=2 ".($status==2? "SELECTED":"").">$i_Notice_StatusSuspended</OPTION>\n";
//     $status_select.= "<OPTION value=3 ".($status==3? "SELECTED":"").">$i_Notice_StatusTemplate</OPTION>\n";
//     $status_select.= "</SELECT>&nbsp;\n";

    $optionAry = array();
    if($_GET['pendingApproval']){
    	$optionAry[4] = $Lang['Btn']['WaitingForApproval'];
		$optionAry[5] = $Lang['General']['Rejected'];
    	$status_select = getSelectByAssoArray($optionAry, 'name="status"  onchange="this.form.submit();"', $status, $all=0, $noFirst=1);
    }else{
		$optionAry[1] = $i_Notice_StatusPublished;
		$optionAry[2] = $i_Notice_StatusSuspended;
		$optionAry[3] = $i_Notice_StatusTemplate;
    	$status_select = getSelectByAssoArray($optionAry, 'name="status"  onchange="this.form.submit();"', $status, $all=0, $noFirst=1);
    }
}

# TABLE INFO
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;
if($order == "") $order = 0;
if($field == "") $field = 0;
$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("a.DateStart","a.DateEnd","a.NoticeNumber");
// [2015-0420-1504-59207] sort notice by Issue Date, Notice ID in descending order
$li->fieldorder2 = ", a.NoticeID desc"; 
//if($field != 2 ){
//	$li->fieldorder2 = ", a.NoticeNumber";	
//}
//else{
//	$li->fieldorder2 = "";
//}

## select Year
$filterbar = "";
$currentyear = date('Y');
$TargetType = "S";

$array_year = $lnotice->retrieveNoticeYears($TargetType);
if(empty($array_year))	$array_year[] = $currentyear;

$firstname = $i_status_all . ($intranet_session_language=="en"?" ":"") . $i_general_Years;
$select_year = getSelectByValueDiffName($array_year,$array_year,"name='year' onChange='document.form1.submit();'",$year,1,0, $firstname) . "&nbsp;";
## select Month
for ($i=1; $i<=12; $i++)
{
     $array_month[] = $i;
}
$firstname = $i_status_all . ($intranet_session_language=="en"?" ":"") . $i_general_Months;
$select_month = getSelectByValueDiffName($array_month,$array_month,"name='month' onChange='document.form1.submit();'",$month,1,0,$firstname) . "&nbsp;";

// [2020-0604-1821-16170]
//$viewRight = $lnotice->hasNormalRight();
//$fullRight = $lnotice->hasFullRight();
//$hasIssueRight = $lnotice->hasIssueRight();
$viewRight = $lnotice->hasSchoolNoticeNormalRight();
$fullRight = $lnotice->hasSchoolNoticeFullRight();
$hasIssueRight = $lnotice->hasSchoolNoticeIssueRight();
// [2015-0323-1602-46073] - check if user is eNotice PIC
$isPIC = $lnotice->isNoticePIC();

$keyword = convertKeyword($keyword);

if($_SESSION['UserType']==USERTYPE_STAFF)
{
        $class = $lclass->returnHeadingClass($UserID, 1);
        // $isClassTeacher = ($class!="");
        $isClassTeacher = empty($class)? 0 : 1;

        ## select of All Notices / Issued Notice / In-charge Notice
        $noticeTypeSelect = "";
        $noticeTypeSelect = "<SELECT name='noticeType' onChange='this.form.submit()'>\n";
        $noticeTypeSelect.= "<OPTION value=0 ".($noticeType==0? "SELECTED":"").">$i_Notice_AllNotice</OPTION>\n";
        // [2020-0604-1821-16170]
        //if ($lnotice->hasIssueRight()) {
        if ($lnotice->hasSchoolNoticeIssueRight()) {
            $noticeTypeSelect.= "<OPTION value=1 ".($noticeType==1? "SELECTED":"").">$i_Notice_MyNotice</OPTION>\n";
        }
        // [2015-0323-1602-46073] - add option to show In-charge Notice
        if ($isPIC) {
            $noticeTypeSelect.= "<OPTION value=3 ".($noticeType==3? "SELECTED":"").">".$Lang['eNotice']['InChargeNotice']."</OPTION>\n";
        }
        // [2019-0715-0929-00235] - add option to show notice of teaching classes
        if ($isClassTeacher && $status == 1) {
            $noticeTypeSelect.= "<OPTION value=4 ".($noticeType==4? "SELECTED":"").">".$Lang['eNotice']['MyClassNotice']."</OPTION>\n";
        }
        $noticeTypeSelect.= "</SELECT>&nbsp;\n";
}

$x = "";
        if ($_SESSION['UserType']==USERTYPE_STAFF)
        {
                if($noticeType && $noticeType != 4)     # Issued Notice / In-charge Notice
                {
                		// [2015-0323-1602-46073] - return sql for showing In-charge Notice only if $noticeType == 3
                        $li->sql = $lnotice->returnMyNotice($year, $month, $status, 1, $keyword, $TargetType, 0, $noticeType==3, 'SCHOOL');
                        $li->no_col = 9;
                        $li->IsColOff = "eNoticedisplayMyNotice";

                       	$pos = 0;
						$li->column_list .= "<th>".$li->column($pos++, $i_Notice_DateStart)."</th>\n";
						$li->column_list .= "<th>".$li->column($pos++, $i_Notice_DateEnd)."</th>\n";
						$li->column_list .= "<th>".$li->column($pos++, $i_Notice_NoticeNumber)."</th>\n";
	                    $li->column_list .= "<th>". $Lang['eNotice']['ViewNotice'] ."</tH>\n";
                        $li->column_list .= "<th>".$i_Notice_Title."</tH>\n";
                        $li->column_list .= "<th>".$i_Notice_Type."</tH>\n";
                        $li->column_list .= "<th>".$i_Notice_Signed."/".$i_Notice_Total."</tH>\n";
                        $li->column_list .= "<th>".$i_Notice_RecipientType."</tH>\n";

                        if($fullRight || $hasIssueRight)
                        $li->column_list .= "<th width=1>".$li->check("NoticeIDArr[]")."</th>\n";
                }
                else		# All Notices
                {
                    $class = $lclass->returnHeadingClass($UserID, 1);
					$isClassTeacher = empty($class)? 0 : 1;

                    $li->sql = $lnotice->returnNoticeListTeacherView($status, $year, $month, 1, $keyword, "", $TargetType, 0, $noticeType==4);
                    // [2015-0428-1214-11207] increase no of column
                    //$li->no_col = 8;
                    $li->no_col = 8;
                    $li->IsColOff = "displayTeacherView";
					
						$tabletop_css = "tabletop";
						
						$pos = 0;
						$li->column_list .= "<th>".$li->column($pos++, $i_Notice_DateStart)."</th>\n";
						$li->column_list .= "<th>".$li->column($pos++, $i_Notice_DateEnd)."</th>\n";
						$li->column_list .= "<th>".$li->column($pos++, $i_Notice_NoticeNumber)."</th>\n";
						if($_GET['pendingApproval']){
							$li->forApproval = 1;
						}else{
							$li->column_list .= "<th>". $Lang['eNotice']['ViewNotice'] ."</th>\n";
							$li->no_col++;
						}
						$li->column_list .= "<th>".$i_Notice_Title."</th>\n";
						$li->column_list .= "<th>".$i_Notice_Issuer."</th>\n";
						// [2015-0428-1214-11207] display table header - PIC
						$li->column_list .= "<th width='200'>".$i_Profile_PersonInCharge."</th>\n";
						if($lnotice->needApproval){
							if($_GET['status']==5){
								$li->column_list .= "<th>". $Lang['eNotice']['RejectedBy'] ."</th>\n";
								$li->no_col++;
								$li->column_list .= "<th>". $Lang['eNotice']['RejectedTime'] ."</th>\n";
								$li->no_col++;
								$li->column_list .= "<th>". $Lang['eNotice']['RejectedComment'] ."</th>\n";
								$li->no_col++;
							}else if($status==1){
								$li->column_list .= "<th>". $Lang['eNotice']['ApprovedBy'] ."</th>\n";
								$li->no_col++;
							}
						}
						if ($isClassTeacher && $status == 1)
						{
                            $li->column_list .= "<th>".$i_Notice_ViewOwnClass."</th>\n";
                            $li->no_col++;
						}
					
					// [2015-0323-1602-46073] - display table header "Signed/Total" if user has view right or is eNotice PIC
                    // [2016-04-07 updated] - display table header "Signed/Total" if user 
                    //									1. has view right
                    //									2. has issue right
                    //									3. is an eNotice PIC
                    //if (($viewRight || $lnotice->staffview || $isPIC) && $status == 1)
					if (($viewRight || $hasIssueRight || $isPIC || $lnotice->staffview) && $status == 1)
                    {
                            $li->column_list .= "<th>".$i_Notice_Signed."/".$i_Notice_Total."</th>\n";
                            $li->no_col++;
                    }
					$li->column_list .= "<th>".$i_Notice_RecipientType."</th>\n";
                    
                    $li->column_list .= "<th width='1'>".$li->check("NoticeIDArr[]")."</th>\n";
                }
       }
        else if ($_SESSION['UserType']==USERTYPE_PARENT)
        {
                $noticeType = $noticeType == "" ? "0" : $noticeType;
             switch($noticeType)
             {
                case 0:                        ## Current
                        # Grab children ids
                         $sql = "SELECT StudentID FROM INTRANET_PARENTRELATION WHERE ParentID = '$UserID'";
                         $children = $lnotice->returnVector($sql);
                         if (sizeof($children)==0)
                         {
                                 header ("Location: /");
                                                     intranet_closedb();
                                                     exit();
                             }

                         $child_list = implode(",",$children);
                         $name_field = getNameFieldWithClassNumberByLang("b.");
                         $name_field2 = getNameFieldWithLoginByLang("d.");
                         $conds = "";
                         if ($year != "")
                         {
                             $conds .= " AND DATE_FORMAT(a.DateStart,'%Y')='$year'";
                         }
                         if ($month != "")
                         {
                             $conds .= " AND DATE_FORMAT(a.DateStart,'%c')='$month'";
                         }
                         if($keyword != "")
		        		 {
		        		 	$keyword = intranet_htmlspecialchars(trim($keyword));
		        		 }
                         $sql = "SELECT a.NoticeID,DATE_FORMAT(a.DateStart,'%Y-%m-%d %H:%i'),
                                        DATE_FORMAT(a.DateEnd,'%Y-%m-%d %H:%i'),if(a.IsModule=1,'--',a.NoticeNumber),a.Title,c.StudentID,
                                        IF(b.UserID IS NULL,CONCAT('<I>',c.StudentName,'</I>'),$name_field),
                                        a.RecordType, c.RecordType, c.RecordStatus,
                                        IF(d.UserID IS NULL,CONCAT('<I>',c.SignerName,'</I>'),$name_field2),
                                        c.DateModified
                                 FROM INTRANET_NOTICE_REPLY as c
                                      LEFT OUTER JOIN INTRANET_NOTICE as a ON a.NoticeID = c.NoticeID
                                      LEFT OUTER JOIN INTRANET_USER as b ON b.UserID = c.StudentID
                                      LEFT OUTER JOIN INTRANET_USER as d ON d.UserID = c.SignerID
                                 WHERE ('%$keyword%' IS NOT NULL) AND c.StudentID IN ($child_list) AND a.RecordStatus = 1
								 AND ((a.NoticeNumber like '%$keyword%') OR (a.Title like '%$keyword%'))
                                       AND a.DateStart <= NOW() AND a.DateEnd >= NOW()
                                       AND a.Module IS NULL 
                                       $conds ";

                        $li->sql = $sql;
                        $li->no_col = 8;
                        $li->IsColOff = "displayParentView";

                        // TABLE COLUMN
                        $li->column_list .= "<td class='tablebluetop tabletopnolink'>".$i_Notice_DateStart."</td>\n";
                        $li->column_list .= "<td class='tablebluetop tabletopnolink'>".$i_Notice_DateEnd."</td>\n";
                        $li->column_list .= "<td class='tablebluetop tabletopnolink'>".$i_Notice_NoticeNumber."</td>\n";
                        $li->column_list .= "<td class='tablebluetop tabletopnolink'>".$i_Notice_Title."</td>\n";
                        $li->column_list .= "<td class='tablebluetop tabletopnolink'>".$i_Notice_StudentName."</td>\n";
                        $li->column_list .= "<td class='tablebluetop tabletopnolink'>".$i_Notice_RecipientType."</td>\n";
                        $li->column_list .= "<td class='tablebluetop tabletopnolink'>".$i_Notice_SignerNoColon."</td>\n";
                        $li->column_list .= "<td class='tablebluetop tabletopnolink'>".$i_Notice_SignedAt."</td>\n";

                        break;
                case 1:                        ## All
                        $li->sql = $lnotice->returnAllNotice($year,$month, 1, $keyword);

                        $li->no_col = 5;
                        $li->IsColOff = "displayAllNotice";

                        // TABLE COLUMN
                        $li->column_list .= "<td class='tablebluetop tabletopnolink'>".$i_Notice_DateStart."</td>\n";
                        $li->column_list .= "<td class='tablebluetop tabletopnolink'>".$i_Notice_DateEnd."</td>\n";
                        $li->column_list .= "<td class='tablebluetop tabletopnolink'>".$i_Notice_NoticeNumber."</td>\n";
                        $li->column_list .= "<td class='tablebluetop tabletopnolink'>".$i_Notice_Title."</td>\n";
                        $li->column_list .= "<td class='tablebluetop tabletopnolink'>".$i_Notice_RecipientType."</td>\n";

                        break;
                case 2:                        ## History
                        # Grab children ids
                         $sql = "SELECT StudentID FROM INTRANET_PARENTRELATION WHERE ParentID = '$UserID'";
                         $children = $lnotice->returnVector($sql);
                         if (sizeof($children)==0) return "";

                         $child_list = implode(",",$children);
                         $name_field = getNameFieldWithClassNumberByLang("b.");
                         $name_field2 = getNameFieldWithLoginByLang("d.");
                         $conds = "";
                         if ($year != "")
                         {
                             $conds .= " AND DATE_FORMAT(a.DateStart,'%Y')='$year'";
                         }
                         if ($month != "")
                         {
                             $conds .= " AND DATE_FORMAT(a.DateStart,'%c')='$month'";
                         }
                         if($keyword != "")
		        		 {
		        		 	$keyword = intranet_htmlspecialchars(trim($keyword));
		        		 }
                         $sql = "SELECT a.NoticeID,DATE_FORMAT(a.DateStart,'%Y-%m-%d %H:%i'),
                                        DATE_FORMAT(a.DateEnd,'%Y-%m-%d %H:%i'),if(a.IsModule=1,'--',a.NoticeNumber),a.Title,c.StudentID,
                                        IF(b.UserID IS NULL,CONCAT('<I>',c.StudentName,'</I>'),$name_field),
                                        a.RecordType, c.RecordType, c.RecordStatus,
                                        IF(d.UserID IS NULL,CONCAT('<I>',c.SignerName,'</I>'),$name_field2),
                                        c.DateModified
                                 FROM INTRANET_NOTICE_REPLY as c
                                      LEFT OUTER JOIN INTRANET_NOTICE as a ON c.NoticeID = a.NoticeID
                                      LEFT OUTER JOIN INTRANET_USER as b ON b.UserID = c.StudentID
                                      LEFT OUTER JOIN INTRANET_USER as d ON d.UserID = c.SignerID
                                 WHERE ('%$keyword%' IS NOT NULL) AND c.StudentID IN ($child_list)
                                        AND a.RecordStatus = 1
										AND ((a.NoticeNumber like '%$keyword%') OR (a.Title like '%$keyword%'))
                                       AND a.DateEnd < NOW()
                                       $conds ";

                        $li->sql = $sql;
                        $li->no_col = 8;
                        $li->IsColOff = "displayParentHistory";

                        // TABLE COLUMN
                        $li->column_list .= "<td class='tablebluetop tabletopnolink'>".$i_Notice_DateStart."</td>\n";
                        $li->column_list .= "<td class='tablebluetop tabletopnolink'>".$i_Notice_DateEnd."</td>\n";
                        $li->column_list .= "<td class='tablebluetop tabletopnolink'>".$i_Notice_NoticeNumber."</td>\n";
                        $li->column_list .= "<td class='tablebluetop tabletopnolink'>".$i_Notice_Title."</td>\n";
                        $li->column_list .= "<td class='tablebluetop tabletopnolink'>".$i_Notice_StudentName."</td>\n";
                        $li->column_list .= "<td class='tablebluetop tabletopnolink'>".$i_Notice_RecipientType."</td>\n";
                        $li->column_list .= "<td class='tablebluetop tabletopnolink'>".$i_Notice_SignerNoColon."</td>\n";
                        $li->column_list .= "<td class='tablebluetop tabletopnolink'>".$i_Notice_SignedAt."</td>\n";

                        break;
             }


             ## select of All Notices / Issued Notice
                $noticeTypeSelect = "";
                $noticeTypeSelect = "<SELECT name='noticeType' onChange='this.form.submit()'>\n";
                $noticeTypeSelect.= "<OPTION value=0 ".($noticeType==0? "SELECTED":"").">$i_Notice_ElectronicNotice_Current</OPTION>\n";
                if ($lnotice->showAllEnabled)
                        $noticeTypeSelect.= "<OPTION value=1 ".($noticeType==1? "SELECTED":"").">$i_Notice_ElectronicNotice_All</OPTION>\n";
                $noticeTypeSelect.= "<OPTION value=2 ".($noticeType==2? "SELECTED":"").">$i_Notice_ElectronicNotice_History</OPTION>\n";
                $noticeTypeSelect.= "</SELECT>&nbsp;\n";
        }
        else if ($_SESSION['UserType']==USERTYPE_STUDENT)
        {
                switch($noticeType)
                {
                        case 0:                ## Current
                                $conds = "";
                                 if ($year != "")
                                 {
                                     $conds .= " AND DATE_FORMAT(a.DateStart,'%Y')='$year'";
                                 }
                                 if ($month != "")
                                 {
                                     $conds .= " AND DATE_FORMAT(a.DateStart,'%c')='$month'";
                                 }
                                 if($keyword != "")
				        		 {
				        		 	$keyword = intranet_htmlspecialchars(trim($keyword));
				        		 }

                                 $name_field = getNameFieldWithClassNumberByLang("b.");
                                 $sql = "SELECT a.NoticeID,DATE_FORMAT(a.DateStart,'%Y-%m-%d %H:%i'),
                                                DATE_FORMAT(a.DateEnd,'%Y-%m-%d %H:%i'),if(a.IsModule=1,'--',a.NoticeNumber),a.Title,
                                                a.RecordType, c.RecordType, c.RecordStatus
                                         FROM INTRANET_NOTICE_REPLY as c
                                              LEFT OUTER JOIN INTRANET_NOTICE as a ON c.NoticeID = a.NoticeID
                                              LEFT OUTER JOIN INTRANET_USER as b ON b.UserID = c.StudentID
                                         WHERE ('%$keyword%' IS NOT NULL) AND c.StudentID = '$UserID' AND a.RecordStatus = 1
									           AND ((a.NoticeNumber like '%$keyword%') OR (a.Title like '%$keyword%'))
                                               AND a.DateStart <= NOW() AND a.DateEnd >= NOW() AND a.Module IS NULL $conds ";

                                $li->sql = $sql;
                                $li->no_col = 5;
                                $li->IsColOff = "displayStudentView";

                                // TABLE COLUMN
                                $li->column_list .= "<td class='tablebluetop tabletopnolink'>".$i_Notice_DateStart."</td>\n";
                                $li->column_list .= "<td class='tablebluetop tabletopnolink'>".$i_Notice_DateEnd."</td>\n";
                                $li->column_list .= "<td class='tablebluetop tabletopnolink'>".$i_Notice_NoticeNumber."</td>\n";
                                $li->column_list .= "<td class='tablebluetop tabletopnolink'>".$i_Notice_Title."</td>\n";
                                $li->column_list .= "<td class='tablebluetop tabletopnolink'>".$i_Notice_RecipientType."</td>\n";

                                break;
                        case 1:                ## All
                                //$x .= $lnotice->displayAllNotice($year,$month);
                                $li->sql = $lnotice->returnAllNotice($year,$month, 1, $keyword);
                                $li->no_col = 4;
                                $li->IsColOff = "displayAllNotice";

                                // TABLE COLUMN
                                $li->column_list .= "<td class='tablebluetop tabletopnolink'>".$i_Notice_DateStart."</td>\n";
                                $li->column_list .= "<td class='tablebluetop tabletopnolink'>".$i_Notice_NoticeNumber."</td>\n";
                                $li->column_list .= "<td class='tablebluetop tabletopnolink'>".$i_Notice_Title."</td>\n";
                                $li->column_list .= "<td class='tablebluetop tabletopnolink'>".$i_Notice_RecipientType."</td>\n";

                                break;
                        case 2:                ## History
                                $conds = "";
                                 if ($year != "")
                                 {
                                     $conds .= " AND DATE_FORMAT(a.DateStart,'%Y')='$year'";
                                 }
                                 if ($month != "")
                                 {
                                     $conds .= " AND DATE_FORMAT(a.DateStart,'%c')='$month'";
                                 }
                                 if($keyword != "")
				        		 {
				        		 	$keyword = intranet_htmlspecialchars(trim($keyword));
				        		 }

                                 $name_field = getNameFieldWithClassNumberByLang("b.");
                                 $sql = "SELECT a.NoticeID,DATE_FORMAT(a.DateStart,'%Y-%m-%d %H:%i'),
                                        DATE_FORMAT(a.DateEnd,'%Y-%m-%d %H:%i'),if(a.IsModule=1,'--',a.NoticeNumber),a.Title,
                                        a.RecordType, c.RecordType, c.RecordStatus,
                                        IF(b.UserID IS NULL,CONCAT('<I>',c.SignerName,'</I>'),$name_field),
                                        c.DateModified
                                 FROM INTRANET_NOTICE_REPLY as c
                                      LEFT OUTER JOIN INTRANET_NOTICE as a ON c.NoticeID = a.NoticeID
                                      LEFT OUTER JOIN INTRANET_USER as b ON c.SignerID = b.UserID
                                 WHERE ('%$keyword%' IS NOT NULL) AND c.StudentID = '$UserID' AND a.DateEnd < NOW() $conds 
								 AND ((a.NoticeNumber like '%$keyword%') OR (a.Title like '%$keyword%'))
								 AND a.RecordStatus=1 ";

                                 $li->sql = $sql;
                                $li->no_col = 7;
                                $li->IsColOff = "displayStudentHistory";

                                // TABLE COLUMN
                                $li->column_list .= "<td class='tablebluetop tabletopnolink'>".$i_Notice_DateStart."</td>\n";
                                $li->column_list .= "<td class='tablebluetop tabletopnolink'>".$i_Notice_DateEnd."</td>\n";
                                $li->column_list .= "<td class='tablebluetop tabletopnolink'>".$i_Notice_NoticeNumber."</td>\n";
                                $li->column_list .= "<td class='tablebluetop tabletopnolink'>".$i_Notice_Title."</td>\n";
                                $li->column_list .= "<td class='tablebluetop tabletopnolink'>".$i_Notice_RecipientType."</td>\n";
                                $li->column_list .= "<td class='tablebluetop tabletopnolink'>".$i_Notice_SignerNoColon."</td>\n";
                                $li->column_list .= "<td class='tablebluetop tabletopnolink'>".$i_Notice_SignedAt."</td>\n";

                                break;
                }


             ## select of All Notices / Issued Notice
                $noticeTypeSelect = "";
                $noticeTypeSelect = "<SELECT name='noticeType' onChange='this.form.submit()'>\n";
                $noticeTypeSelect.= "<OPTION value=0 ".($noticeType==0? "SELECTED":"").">$i_Notice_ElectronicNotice_Current</OPTION>\n";
                if ($lnotice->showAllEnabled)
                        $noticeTypeSelect.= "<OPTION value=1 ".($noticeType==1? "SELECTED":"").">$i_Notice_ElectronicNotice_All</OPTION>\n";
                $noticeTypeSelect.= "<OPTION value=2 ".($noticeType==2? "SELECTED":"").">$i_Notice_ElectronicNotice_History</OPTION>\n";
                $noticeTypeSelect.= "</SELECT>&nbsp;\n";

        }
        else $x .= "";

        
### Button 
//$delBtn         = "<a href=\"javascript:checkRemoveThis(document.form1,'NoticeIDArr[]','remove_update.php')\" class=\"tabletool\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_delete.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\" /> " . $button_delete . "</a>";
$delBtn = "<a href=\"javascript:checkRemoveThis(document.form1,'NoticeIDArr[]','remove_update.php')\" class=\"tool_delete\">$button_delete</a>";
//$editBtn         = "<a href=\"javascript:checkEdit(document.form1,'NoticeIDArr[]','edit.php')\" class=\"tabletool\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_edit.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\" /> " . $button_edit . "</a>";
//$edit_link        = "edit.php";
$copyBtn = "<a href=\"javascript:checkEdit(document.form1,'NoticeIDArr[]','copy.php')\" class=\"tool_copy\">$button_copy</a>";

$approvalBtn = '<a class="tool_approve" href="javascript: checkApprove(document.form1,\'NoticeIDArr[]\',\'../approve.php?student_notice=1\');">'.$Lang['Btn']['Approve'].'</a>';
$rejectBtn = '<a class="tool_reject" href="javascript: checkReject(document.form1,\'NoticeIDArr[]\',\'../reject.php?student_notice=1\');">'.$Lang['Btn']['Reject'].'</a>';


// $searchTag 	= "<table border=\"0\" cellspacing=\"0\" cellpadding=\"3\"><td>";
// $searchTag 	.= "&nbsp;<input type=\"text\" name=\"keyword\" class=\"formtextbox\" maxlength=\"50\" value=\"".stripslashes($keyword)."\" onFocus=\"this.form.pageNo.value=1;\"></td>";
// $searchTag 	.= "<td>".$linterface->GET_BTN($button_find, "submit", "","submit3"," class='formsubbutton' onMouseOver=\"this.className='formsubbuttonon'\" onMouseOut=\"this.className='formsubbutton'\" ")."</td>";
// $searchTag 	.= "</td></table>";

$searchTag = "<input name=\"keyword\" id=\"keyword\" type=\"text\" value=\"". stripslashes($keyword) ."\" onkeyup=\"Check_Go_Search(event);\"/>";

### Title ###
if($_GET['pendingApproval']){
	$curTab = 'approval';
}else{
	$curTab = 'normal';
}

// [2020-0604-1821-16170]
//if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eNotice"]||$lnotice->isApprovalUser()){
if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eNotice"] || $lnotice->isSchoolNoticeApprovalUser()){
	$countOfPendingNotice = $lnotice->getPendingApprovalNotice('S',NOTICE_SETTING_TYPE_SCHOOL);
	if($countOfPendingNotice>0){
		$countNotice = ' ('.$countOfPendingNotice.')';
	}
}
if($sys_custom['PowerClass']){
	$TAGS_OBJ[] = array($Lang['eNotice']['SchoolNotice'],'../index.php', 0);
}	
$TAGS_OBJ[] = array($Lang['eNotice']['SchoolStudentNotice'],'index.php',$curTab=='normal');
if($countOfPendingNotice>0 || $lnotice->needApproval){
	$TAGS_OBJ[] = array($Lang['eNotice']['PendingApproval'].$countNotice, 'index.php?pendingApproval=1', $curTab=='approval');
}
if($sys_custom['PowerClass'])
{
    // [DM#3780] show payment notice tab only if has issue right
    // if($special_feature['ePaymentNotice'])
    if($special_feature['ePaymentNotice'] && $_SESSION["SSV_PRIVILEGE"]["notice"]["hasPaymentNoticeIssueRight"]){
        $TAGS_OBJ[] = array($Lang['eNotice']['PaymentNotice'],'../payment_notice/paymentNotice.php', 0);
    }
	$TAGS_OBJ[] = array($Lang['Header']['Menu']['eCircular'],"../../../StaffMgmt/circular/index.php", 0);
}
//$onlineHelpBtn = gen_online_help_btn_and_layer('enotice_admin','home');
//$TAGS_OBJ_RIGHT[] = array($onlineHelpBtn);

$MODULE_OBJ = $lnotice->GET_MODULE_OBJ_ARR();

if(!$_SESSION['UserType']==USERTYPE_STAFF)
{
    $CurrentPageArr['eOffice']          = 0;
    $CurrentPageArr['eServiceeOffice']  = 1;
}

$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$xmsg]);

if($_GET['pendingApproval'])
{
    // [2020-0604-1821-16170]
	//if($lnotice->hasApprovalRight()){
    if ($lnotice->hasSchoolNoticeApprovalRight()){
        if($_GET['status']==5){
			$htmlAry['dbBtn']=$approvalBtn  . $delBtn;
		}else{
			$htmlAry['dbBtn']=$approvalBtn . $rejectBtn . $delBtn;
		}
	}else{
		$htmlAry['dbBtn']= $delBtn;
	}
}
else
{
	$htmlAry['dbBtn']=$copyBtn . $delBtn;
}
?>

<SCRIPT LANGUAGE=Javascript>
function viewNotice(id)
{
       newWindow('/home/eService/notice/sign.php?NoticeID='+id,10);
}
function viewResult(id)
{
         newWindow('./result.php?NoticeID='+id,10);
}
function viewNoticeClass(id)
{
         newWindow('/home/eService/notice/tableview.php?type=1&NoticeID='+id,10);
}

/*
function sign(id,studentID)
{
         newWindow('./sign.php?NoticeID='+id+'&StudentID='+studentID,10);
}
function viewReply(id,studentID)
{
         newWindow('./view.php?NoticeID='+id+'&StudentID='+studentID,10);
}

*/

function checkRemoveThis(obj,element,page){
        if(countChecked(obj,element)==0)
                alert(globalAlertMsg2);
        else{
                if(confirm("<?=$i_Notice_RemovalWarning?>")){
                obj.action=page;
                obj.method="POST";
                obj.submit();
                }
        }
}
function editNotice(nid)
{
         with(document.form1)
         {
                NoticeID.value = nid;
                action = "edit.php";
                submit();
         }
}
function editPaymentNotice(nid)
{
         with(document.form1)
         {
                NoticeID.value = nid;
                action = "payment_notice/paymentNotice_edit.php";
                submit();
         }
}
function newNotice()
{
         with(document.form1)
         {
                action = "new.php";
                submit();
         }
}

function Check_Go_Search(evt)
{
	var key = evt.which || evt.charCode || evt.keyCode;
	
	if (key == 13) // enter
		document.form1.submit();
	else
		return false;
}

function checkApprove(obj,element,page)
{
    if(countChecked(obj,element)==0) {
        alert(globalAlertMsg2);
    } else {
        if(confirm("<?= $Lang['eNotice']['SureToApproveAll'] ?>")) {
            $('.tool_approve').hide();
            $('.tool_reject').hide();
            $('.tool_delete').hide();

            obj.action = page;
            obj.method = "POST";
            obj.submit();
        }
    }
}

function checkReject(obj,element,page)
{
    if(countChecked(obj,element)==0) {
        alert(globalAlertMsg2);
    } else {
        if(confirm("<?=$Lang['eNotice']['ConfirmReject'] ?>")){
            $('.tool_approve').hide();
            $('.tool_reject').hide();
            $('.tool_delete').hide();

            obj.action = page;
            obj.method = "POST";
            obj.submit();
        }
    }
}
</SCRIPT>

<form name="form1" method="get" action="index.php">

<div class="content_top_tool">
		<?=$AddBtn?>
		<div class="Conntent_search"><?=$searchTag?></div>
	<br style="clear:both" />
</div>
		
<div class="table_board">

<!-- function & search bar //-->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr class="table-action-bar">
	<td valign="bottom"><div class="table_filter"><?=$noticeTypeSelect?><?=$status_select?><?=$select_year?><?=$select_month?></div></td>
	<td valign="bottom">
		<?
		if($_SESSION['UserType']==USERTYPE_STAFF and (($fullRight || $hasIssueRight))) { ?>
		<div class="common_table_tool"><?=$htmlAry['dbBtn']?></div>
		<? } ?>
	</td>
</tr>
</table>

<?=$li->display($TargetType);?>

<input type="hidden" name="NoticeID" value="" />
<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
<input type="hidden" name="order" id="order" value="<?=$li->order; ?>" />
<input type="hidden" name="page_size_change" value="<?=$page_size_change?>" />
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />
<input type="hidden" name="pendingApproval" value="<?=$_GET['pendingApproval'] ?>">
</form>
</div>

<?php
intranet_closedb();
$linterface->LAYOUT_STOP();

if($sys_custom['PopupNoticeAfterCreated'] && !empty($NoticeID))
{
?>
<SCRIPT LANGUAGE=Javascript>
newWindow('/home/eService/notice/sign.php?NoticeID=<?=$NoticeID?>',10);
</script>
<?	
}
?>