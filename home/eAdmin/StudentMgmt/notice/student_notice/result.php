<?php
# using: 

############ Change Log [Start]
#
#   Date:   2020-04-16  Bill    [2020-0310-1051-05235]
#           Special notice > access checking for teacher    ($sys_custom['eNotice']['SpecialNoticeForPICAndAdmin'])
#
#	Date:	2015-04-14 Bill
#			allow eNotice PIC to access [2015-0323-1602-46073] 
#			allow access when "All staff can issue notices" & "All staff can view reply contents" & not in access group & not eNotice admin
#
#	Date:	2010-08-04	YatWoon
#			update layout to IP25 standard
#
############ Change Log [End]

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");

intranet_auth();
intranet_opendb();

$lnotice = new libnotice($NoticeID);
$lu = new libuser($UserID);

// [2020-0310-1051-05235] Special notice > access checking for teacher
$allowTeacherAccess = true;
if($sys_custom['eNotice']['SpecialNoticeForPICAndAdmin'] && $lnotice->SpecialNotice && $lu->isTeacherStaff())
{
    $allowTeacherAccess = $_SESSION["SSV_USER_ACCESS"]["eAdmin-eNotice"] || $lnotice->isNoticePIC($NoticeID);
}

//if (!$lu->isTeacherStaff())
if (!$lu->isTeacherStaff() || !$allowTeacherAccess)
{
     header("Location: /close.php");
     exit();
}

// [2015-0323-1602-46073] allow eNotice PIC to access
if (!$lnotice->hasViewRight() && !$lnotice->staffview && !$lnotice->isNoticePIC($NoticeID))
{
     header("Location: /close.php");
     exit();
}

if ($lnotice->RecordType == 4)
{
    header("Location: /home/eService/notice/tableview.php?NoticeID=$NoticeID");
    exit();
}

$MODULE_OBJ['title'] = $i_Notice_ResultForEachClass;
$linterface = new interface_html("popup.html");
$linterface->LAYOUT_START();

$array_total = $lnotice->returnTotalCountByClass();
$array_signed = $lnotice->returnSignedCountByClass();
$all_total = 0;
$all_signed = 0;
$classes = $lnotice->returnClassList();

$x .= "<table class='common_table_list view_table_list_v30'>\n";
$x .= "<tr>";
$x .= "<th>$i_ClassName</th>";
$x .= "<th width='80'>$i_Notice_Signed</th>";
$x .= "<th width='80'>$i_Notice_Total</th>";
$x .= "</tr>\n";

for ($i=0; $i<sizeof($classes); $i++)
{
     $name = $classes[$i];
     ## check $class <>""
     if(!$name) continue;
     $signed = $array_signed[$name]+0;
     $total = $array_total[$name]+0;
     $displayName = ($name==""? "--":"$name");
     $link = "<a href='/home/eService/notice/tableview.php?NoticeID=$NoticeID&class=".urlencode($name)."'>$displayName</a>";
     $x .= "<tr>\n";
     $x .= "<td>$link</td>";
     $x .= "<td width='80'>$signed</td>";
     $x .= "<td width='80'>$total</td>";
     $x .= "</tr>\n";
     $all_total += $total;
     $all_signed += $signed;
}
// $x .= "</table>\n";

### All student
$link = "<a href='/home/eService/notice/tableview.php?NoticeID=$NoticeID&all=1'>$i_Notice_AllStudents</a>";
$x .="<tr class='total_row'>";
$x .= "<th>$link</th>";
$x .= "<td width='80'>$all_signed</td>";
$x .= "<td width='80'>$all_total</td>";
$x .= "</tr>\n";
$x .= "</table>\n";
?>

<br> 
<form name="print_form" action="result_print_preview.php" method="post" target = "_blank">

<table class="form_table_v30">
<tr valign='top'>
	<td class='field_title'><?=$i_Notice_NoticeNumber?></td>
	<td><?=$lnotice->NoticeNumber?></td>
</tr>

<tr valign='top'>
	<td class='field_title'><?=$i_Notice_Title?></td>
	<td><?=$lnotice->Title?></td>
</tr>
</table>

<br>
<?=$x?>

<!-- Button //-->
<div class="edit_bottom_v30">
<p class="spacer"></p>
	<?= $linterface->GET_ACTION_BTN($button_print . ($intranet_session_language=="en"?" ":"") . $button_preview, "button", "document.print_form.submit();") ?>
	<?= $linterface->GET_ACTION_BTN($button_close, "button", "self.close()","submit3") ?>
<p class="spacer"></p>
</div>

<input type="hidden" name="NoticeID" value="<?=$NoticeID?>">
</form>

<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>