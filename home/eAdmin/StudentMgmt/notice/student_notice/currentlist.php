<?php
# using: yat

############################################################
#
#	Date:	2012-02-10	YatWoon
#			Fixed: fail to display student notice current list (display as parent notice) [Case#2012-0210-1125-58132]
#
############################################################
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$lnotice = new libnotice();
$notices = $lnotice->returnAllNoticeWithStatus($moduleName, "S");

$MODULE_OBJ['title'] = $i_Notice_CurrentList. " (". $i_Notice_ListIncludingSuspend.")";

$linterface = new interface_html("popup.html");
$linterface->LAYOUT_START();

$display .= "<table width='96%' border='0' cellpadding='4' cellspacing='0' align='center'>\n";
$display .= "<tr>";
$display .= "<td class='tabletop tabletopnolink' align='left'>".$i_Notice_NoticeNumber."</td>";
$display .= "<td class='tabletop tabletopnolink' align='left'>".$i_Notice_DateStart."</td>";
$display .= "<td class='tabletop tabletopnolink' align='left'>".$i_Notice_Title."</td>";
$display .= "<td class='tabletop tabletopnolink' align='left'>".$i_Notice_Type."</td>";
$display .= "</tr>";

for ($i=0; $i<sizeof($notices); $i++)
{
     list ($nid, $datestart, $number, $title, $recordType, $recordStatus) = $notices[$i];
     $display .= "<tr class='tablerow". ( $i%2 + 1 )."'>";
     $display .= "<td class='tabletext'>$number</td>";
     $display .= "<td class='tabletext'>$datestart</td>";
     $display .= "<td class='tabletext'>$title</td>";
     $display .= "<td class='tabletext'>".($recordStatus==1?$i_Notice_StatusPublished:$i_Notice_StatusSuspended)."</td>";
     $display .= "</tr>\n";
}

$display .= "<tr>"; 
$display .= "<td height='1' colspan='4' class='dotline'><img src='$image_path/$LAYOUT_SKIN/10x10.gif' width='10' height='1'></td>";
$display .= "</tr>";
$display .= "<tr>"; 
$display .= "<td colspan='4' align='center'>". $linterface->GET_ACTION_BTN($button_close, "button","self.close();") ."</td>";
$display .= "</tr>";
$display .= "</table>\n";

?>

<br />
<?=$display?>
<br />
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>