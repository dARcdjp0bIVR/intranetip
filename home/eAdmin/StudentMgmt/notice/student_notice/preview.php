<?php
# using: 

####### Change log [Start] #######
#
#	Date:	2016-08-29	Bill	[2016-0829-1514-31066]
#			get Display Question Number setting from unsigned notice
#
#	Date: 	2016-07-14	Bill	[2016-0113-1514-09066]
#			remove target question syntax from question string
#
#	Date:	2015-11-13	Bill	[2015-0615-1438-48014]
#			display * for Questions must be submitted (sheet.mustSubmit = 1)
#
#	Date:	2011-02-16	YatWoon
#			set DisplayQuestionNumber = 0 for preview editing form
#
####### Change log [End] #######

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libform.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

intranet_auth();
$lform = new libform();

$MODULE_OBJ['title'] = $i_Notice_ReplySlip;
$linterface = new interface_html("popup.html");
$linterface->LAYOUT_START();
?>
<script language="javascript" src="/templates/forms/layer.js"></script>
<script language="javascript" src="/templates/forms/form_edit.js"></script>

<script language="Javascript">
var replyslip = '<?=$i_Notice_ReplySlip?>';
</script>

<br />
<p>
<table width="80%" align="center" border="0">
<form name="ansForm" method="post" action="update.php">
        <input type="hidden" name="qStr" value="">
        <input type="hidden" name="aStr" value="">
</form>

<tr>
	<td align="left">
                <script language="Javascript">
                <?=$lform->getWordsInJS()?>
                
                var sheet= new Answersheet();
                // attention: MUST replace '"' to '&quot;'
                sheet.qString=window.opener.document.form1.qStr.value;
                sheet.qString=sheet.qString.replace(/#TAR#(([A-Z]|[a-z]|[0-9])+)#/g, '');
                sheet.aString="";
                //edit submitted application
                sheet.mode=1;
                // display * for Questions must be submitted
				sheet.mustSubmit=1;
                sheet.answer=sheet.sheetArr();
                var DisplayQuestionNumber = 0;
                if(window.opener.document.form1.DisplayQuestionNumber.checked || (window.opener.document.form1.DisplayQuestionNumber.type=="hidden" && window.opener.document.form1.DisplayQuestionNumber.value=="1"))
                	DisplayQuestionNumber = 1;
                
                Part3 = '';
                document.write(editPanel());
                </script>
	</td>
</tr>

<tr>
	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
</tr>

<tr>
	<td align="center">
		<?= $linterface->GET_ACTION_BTN($button_close, "button", "window.close(); return false;","cancelbtn") ?>
	</td>
</tr>
</table>
<br />

<?php
$linterface->LAYOUT_STOP();
?>