<?php
// Using: Bill

#########################################
#	Date:	2019-09-30 Philips	[2019-0930-0954-24073]
#			Select Template seperated to the top
#
#   Date:   2018-02-21 Bill     [2017-1107-1707-25207]
#           fixed: cannot down load CSV sample files
#
#	Date:	2017-03-16	Bill	[2017-0316-1004-26206] 
#			fixed: cannot use jump answer function due to not passing "All fields required to be filled in" setting
#
#	Date:	2016-09-13	Bill 	[2016-0901-1208-57207]
#			fixed: cannot load template content correctly if question contain some special characters 
#
#	Date:	2014-12-01	Bill [Case #P70566]
#			- get noticeAttFolder from $_POST 
#
#	Date:	2014-10-15	Roy
#			add send push message option
#
#	Date:	2013-09-06	YatWoon
#			add copy attachment logic
#
#	Date:	2013-07-24	YatWoon	 [2013-0708-0924-06167]
#			stripslashes which create from templates
#
#########################################

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libform.php");
include_once($PATH_WRT_ROOT."includes/libfiletable.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$lform = new libform();
$lnotice = new libnotice();

if ($templateID != -1)
{
    $lnotice->returnRecord($templateID);
    
    $Title = $lnotice->Title;
	$Description = $lnotice->Description;
	$Question = stripslashes($lform->getConvertedString($lnotice->Question));
	
	// [2016-0901-1208-57207] handle some html special characters in question
	$Question = str_replace("\"", "&quot;", $Question);
	$Question = str_replace("'", "&apos;", $Question);
	$Question = str_replace("<br>", "\n", $Question);
	
	// [2017-0316-1004-26206] 
	$AllFieldsReq = $lnotice->AllFieldsReq;
	$DisplayQuestionNumber = $lnotice->DisplayQuestionNumber;
	$template_noticeAttFolder = $lnotice->Attachment;
	
	# clear attachment
	$lf = new libfilesystem();
//	$path = "$file_path/file/notice/$noticeAttFolder";
	$path = "$file_path/file/notice/".$_POST['noticeAttFolder'];
	$templf = new libfiletable("", $path,0,0,"");
	$temp_files = $templf->files;
	if(sizeof($temp_files)>0)
	{
		for($i=0;$i<sizeof($temp_files);$i++)
		{
			$des = $path."/".$temp_files[$i][0];
			$lf->file_remove($des);
		}
	}
	
	# copy file 
	$template_path = "$file_path/file/notice/$template_noticeAttFolder";
	$templf = new libfiletable("", $template_path,0,0,"");
	$template_files = $templf->files;
	if(sizeof($template_files)>0)
	{
		for($i=0;$i<sizeof($template_files);$i++)
		{
			$loc = $template_path."/".$template_files[$i][0];
			$des = $path."/".$template_files[$i][0];
			$lf->lfs_copy($loc, $des);
		}
	}
}

$now = time();
$defaultEnd = $now + ($lnotice->defaultNumDays*24*3600);
$start = date('Y-m-d',$now);
$end = date('Y-m-d',$defaultEnd);

### Type
$type_selection = "
<SELECT name='type' onChange='selectAudience()'>
<OPTION value='' > -- $button_select -- </OPTION>
<OPTION value='1'>$i_Notice_RecipientTypeAllStudents</OPTION>
<OPTION value='2'>$i_Notice_RecipientTypeLevel</OPTION>
<OPTION value='3'>$i_Notice_RecipientTypeClass</OPTION>
<OPTION value='4'>$i_Notice_RecipientTypeIndividual</OPTION>
</SELECT>";

### Template
$notice_templates = $lnotice->returnTemplates();
$NAoption = array(-1, $i_Notice_NotUseTemplate);
if (sizeof($notice_templates)!=0) {
    array_unshift($notice_templates, $NAoption);
}
else {
    $notice_templates = array($NAoption);
}
$template_selection = getSelectByArray($notice_templates, "name='templateID' onChange=\"selectTemplate()\"", $templateID, "", 1);

### User Login
$UserPICTextarea = '';
$UserPICTextarea .= '<div style="float:left;" class="pic_textarea">';
$UserPICTextarea .= '<textarea style="height:100px;" id="PICAreaSearchTb" name="PICAreaSearchTb"></textarea><span id="textarea_msg"></span><br><br>';
$UserPICTextarea .= $linterface->GET_SMALL_BTN($Lang['AppNotifyMessage']['searchAndInsertPIC'], "button" , "");
$UserPICTextarea .= '</div>';

### CSV sample files    [2017-1107-1707-25207]
$sample_csv_file1 = GET_CSV("sample_data.csv", '', false);
$sample_csv_file1_userlogin = GET_CSV("sample_data_userlogin.csv", '', false);
$sample_csv_file2 = GET_CSV("sample_data_format2.csv", '', false);
$sample_csv_file2_userlogin = GET_CSV("sample_data_format2_userlogin.csv", '', false);

$STUDENT_INPUT = "<tr valign='top' class='selectStudentArea'>
					<td nowrap class='field_title'><span class='tabletextrequire'>*</span>" . $i_Notice_RecipientType . "</td>
					<td>" . $type_selection . "<br><span id='div_audience_err_msg'></span>
					<div id='div_audience'></div>
					</td>
				</tr>
				<!-- Select PICs of eNotice -->
				<tr valign='top'>
					<td nowrap class='field_title'>" . $i_Profile_PersonInCharge . "</td>
					<td><table border='0' cellpadding='0' cellspacing='0'>
					<tr>
						<td class='tablerow2'>" . $Lang['AppNotifyMessage']['msgSearchAndInsertPICInfo'] . "<br>" . $UserPICTextarea. "</td>
						<td class='tabletext' ><img src='". $image_path.'/'.$LAYOUT_SKIN."/10x10.gif' width='10' height='1'></td>
						<td>" . $linterface->GET_SELECTION_BOX($PICUser, "id='target_PIC' name='target_PIC[]' size='6' multiple", "") . "</td>
						<td valign='bottom'>" . $linterface->GET_BTN($Lang['Btn']['Select'], "button", "newWindow('/home/common_choose/index.php?fieldname=target_PIC[]&permitted_type=1&DisplayGroupCategory=1&DisplayInternalRecipientGroup=1&Disable_AddGroup_Button=1', 16)")
						. "<br>" . $linterface->GET_BTN($Lang['Btn']['Remove'], "button", "checkOptionRemove(document.form1.elements['target_PIC[]'])") . "</td>
					</tr>
					</table></td>
				</tr>";
$TEMPLATE_SELECCTION = "<tr>
	<td class='field_title'>" . $i_Notice_FromTemplate . "</td>
	<td>" . $template_selection . "</td>
</tr>"; //<!--TEMPLATE_SELECTION-->
$formBody = $lnotice->return_FormContent($start, $end, $isIssuedNotice, $Title, $Description, $Question, '', $AllFieldsReq, '', $template_noticeAttFolder, $DisplayQuestionNumber, '', ($plugin['eClassApp'] ? 1 : 0), '', '', 1, 1, 1, "", 0, $isTemplate = true);
$formBody = str_replace("<!--TEMPLATE_SELECTION-->", $TEMPLATE_SELECCTION, $formBody);
echo str_replace("<!--___STUDENT_INPUT___-->", $STUDENT_INPUT, $formBody);

intranet_closedb();
?>