<?php
# using:
/*
 *  2020-07-16 [Bill]   [2020-0630-1400-45066]
 *  - apply bulk mode to send push msg
 *  - show no. of msg - sent out successfully + waiting to send out
 *
 *  2019-06-06  Bill
 *  - add CSRF token checking
 * 
 *  2016-02-22	Kenneth
 * 	- Improvement: show 0 message sent once no push message is sent
 * 
 *	2015-08-03 [Roy] (ip.2.5.6.7.1)
 *	- Added $fromModule and $moduleRecordID to link push message and notice
 */

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");
include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp.php");

if(!verifyCsrfToken($_GET['csrf_token'], $_GET['csrf_token_key'], $do_not_remove_token_data=true)) {
    echo "<font color='red'>".$Lang['AppNotifyMessage']['eNotice']['send_failed']."</font>";
    exit;
}

intranet_auth();
intranet_opendb();

$lu = new libuser($UserID);
$lnotice = new libnotice($NoticeID);
$leClassApp = new libeClassApp();

$UnsignedStudents = $lnotice->getUnsignList($NoticeID, $class);
$notifyuser = $lu->UserNameLang();

$NoticeNumber = "".intranet_undo_htmlspecialchars($lnotice->NoticeNumber);
$NoticeTitle = "".intranet_undo_htmlspecialchars($lnotice->Title);
$NoticeDeadline = "".$lnotice->DateEnd;

$MessageTitle = str_replace("[NoticeNumber]", $NoticeNumber, $Lang['AppNotifyMessage']['eNotice']['Title']['App']);
$MessageContent = str_replace("[NoticeNumber]", $NoticeNumber, $Lang['AppNotifyMessage']['eNotice']['Alert']['App']);
$MessageContent = str_replace("[NoticeTitle]", $NoticeTitle, $MessageContent);
$MessageContent = str_replace("[NoticeDeadline]", $NoticeDeadline, $MessageContent);

$messageInfoAry = array();

### Get parent student mapping
$sql = "SELECT 
				ip.ParentID, ip.StudentID
		FROM 
				INTRANET_PARENTRELATION AS ip 
				INNER JOIN INTRANET_USER AS iu ON (ip.ParentID=iu.UserID) 
		WHERE 
				ip.StudentID IN ('".implode("','", (array)$UnsignedStudents)."') 
				AND iu.RecordStatus = 1";
$parentStudentAssoAry = BuildMultiKeyAssoc($lnotice->returnResultSet($sql), 'ParentID', array('StudentID'), $SingleValue=1, $BuildNumericArray=1);

$messageInfoAry = array();
$messageInfoAry[0]['relatedUserIdAssoAry'] = $parentStudentAssoAry;

// [2020-0630-1400-45066] apply bulk mode to send push msg
 if ($leClassApp->isEnabledSendBulkPushMessageInBatches()) {
    $notifyMessageId = $leClassApp->sendPushMessageByBatch($messageInfoAry, $MessageTitle, $MessageContent, $isPublic='', $recordStatus=1, $appType='', $sendTimeMode='', $sendTimeString='', $parNotifyMessageId='', $parNotifyMessageTargetIdAry='', $fromModule='eNotice', $NoticeID);
} else {
    $notifyMessageId = $leClassApp->sendPushMessage($messageInfoAry, $MessageTitle, $MessageContent, $isPublic='', $recordStatus=1, $appType='', $sendTimeMode='', $sendTimeString='', $parNotifyMessageId='', $parNotifyMessageTargetIdAry='', $fromModule='eNotice', $NoticeID);
}
$statisticsAry = $leClassApp->getPushMessageStatistics($notifyMessageId);
$statisticsAry = $statisticsAry[$notifyMessageId];

intranet_closedb();

if ($notifyMessageId > 0)
{
	if($statisticsAry['numOfSendSuccess'] == '') {
		$statisticsAry['numOfSendSuccess'] = 0;
	}
    //echo "<font color='#DD5555'>".str_replace("[SentTotal]", $statisticsAry['numOfSendSuccess'], $Lang['AppNotifyMessage']['eNotice']['send_result']). "</font>";

    // [2020-0630-1400-45066] return msg > include no. of push msg - waiting to send
    if($statisticsAry['numOfWaitingToSend'] == '') {
        $statisticsAry['numOfWaitingToSend'] = 0;
    }
    $notifyMessageCount = $statisticsAry['numOfSendSuccess'] + $statisticsAry['numOfWaitingToSend'];
    echo "<font color='#DD5555'>".str_replace("[SentTotal]", $notifyMessageCount, $Lang['AppNotifyMessage']['eNotice']['send_msg_count']). "</font>";
}
else
{
	$ErrorMsg = $Lang['AppNotifyMessage']['eNotice']['send_failed'];
	if ($statisticsAry['numOfRecepient'] == 0) {
		$ErrorMsg .= $Lang['AppNotifyMessage']['eNotice']['send_result_no_parent'];
	}
	echo "<font color='red'>".$ErrorMsg."</font> <font color='grey'>(".$statisticsAry['numOfRecepient']."-".$statisticsAry['numOfSendSuccess'].")</font>";
}
?>