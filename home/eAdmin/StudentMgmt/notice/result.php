<?php
# Using: 

############ Change Log [Start]
#
#   Date:   2020-04-16  Bill    [2020-0310-1051-05235]
#           Special notice > access checking for teacher    ($sys_custom['eNotice']['SpecialNoticeForPICAndAdmin'])
#
#   Date:   2019-06-06  (Bill)
#           add CSRF token in html form + pass token to ajax page
#
#	Date:	2018-01-22 [Isaac]	[P133132]
#			Updated the label wording for the message parents buttons
#
#	Date:	2016-03-10 [Bill]	[2016-0310-0911-58085]
#			Added Export (Unsigned Notice List) button
#
#	Date:	2015-07-20 [Ivan] (ip.2.5.6.7.1)
#			Added sms confirmation page logic
#
#	Date:	2015-06-05	Omas
#			Add Export Mobile Button
#
#	Date:	2015-04-14 Bill
#			allow eNotice PIC to access [2015-0323-1602-46073] 
#
#	Date:	2015-01-06 Bill
#			Fixed: when "All staff can issue notices" & "All staff can view reply contents" & not in access group & not eNotice admin
#				- allow access but no push message button
#
#	Date:	2014-09-08 YatWoon
#			cater with the class name with null or empty (need display)
#
#	Date:	2014-01-03 Ivan
#			added $plugin['eClassApp'] logic
#
#	Date:	2013-11-18 Roy
#			 check sms admin $_SESSION["SSV_USER_ACCESS"]["eAdmin-SMS"]
#				& flag $sys_custom["notify_sms_disabled"] to control sms options
#
#	Date:	2013-10-08 Roy
#			added thickbox to show push message method options
#
#	Date:	2010-08-04	YatWoon
#			update layout to IP25 standard
#
############ Change Log [End] 


$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");

intranet_auth();
intranet_opendb();

$lnotice = new libnotice($NoticeID);
$lu = new libuser($UserID);

$Message = standardizeFormPostValue($_POST['Message']);
$directSendSMS = standardizeFormPostValue($_POST['directSendSMS']);

// [2020-0310-1051-05235] Special notice > access checking for teacher
$allowTeacherAccess = true;
if($sys_custom['eNotice']['SpecialNoticeForPICAndAdmin'] && $lnotice->SpecialNotice && $lu->isTeacherStaff())
{
    $allowTeacherAccess = $_SESSION["SSV_USER_ACCESS"]["eAdmin-eNotice"] || $lnotice->isNoticePIC($NoticeID);
}

//if (!$lu->isTeacherStaff())
if (!$lu->isTeacherStaff() || !$allowTeacherAccess)
{
     header("Location: /close.php");
     exit();
}

// [2015-0323-1602-46073] allow eNotice PIC to access
if (!$lnotice->hasViewRight() && !$lnotice->staffview && !$lnotice->isNoticePIC($NoticeID))
{
     header("Location: /close.php");
     exit();
}

if ($lnotice->RecordType == 4)
{
    header("Location: /home/eService/notice/tableview.php?NoticeID=$NoticeID");
    exit();
}

$MODULE_OBJ['title'] = $i_Notice_ResultForEachClass;
$linterface = new interface_html("popup.html");
$linterface->LAYOUT_START();

$array_total = $lnotice->returnTotalCountByClass();
$array_signed = $lnotice->returnSignedCountByClass();
$all_total = 0;
$all_signed = 0;
$classes = $lnotice->returnClassList();

$x .= "<table class='common_table_list view_table_list_v30'>\n";
$x .= "<tr>";
$x .= "<th>$i_ClassName</th>";
$x .= "<th width='80'>$i_Notice_Signed</th>";
$x .= "<th width='80'>$i_Notice_Total</th>";
$x .= "</tr>\n";

//for ($i=0; $i<sizeof($classes); $i++)
foreach($array_total as $name => $d)
{
     //$name = $classes[$i];
     ## check $class <>""
     //if(!$name) continue;
     
     $signed = $array_signed[$name]+0;
     $total = $array_total[$name]+0;
     //$displayName = ($name==""? "--":"$name");
     $displayName = ($name=="" || $name=="NULL"? "--":"$name");
     $link = "<a href='/home/eService/notice/tableview.php?NoticeID=$NoticeID&class=".urlencode($name)."'>$displayName</a>";
     
     $x .= "<tr>\n";
     $x .= "<td>$link</td>";
     $x .= "<td width='80'>$signed</td>";
     $x .= "<td width='80'>$total</td>";
     $x .= "</tr>\n";
     $all_total += $total;
     $all_signed += $signed;
}
// $x .= "</table>\n";

### All student
$link = "<a href='/home/eService/notice/tableview.php?NoticeID=$NoticeID&all=1'>$i_Notice_AllStudents</a>";
$x .="<tr class='total_row'>";
$x .= "<th>$link</th>";
$x .= "<td width='80'>$all_signed</td>";
$x .= "<td width='80'>$all_total</td>";
$x .= "</tr>\n";
$x .= "</table>\n";

# build thickbox
$thickBoxHeight = 180;
$thickBoxWidth = 600;

$pushMessageButton = $linterface->Get_Thickbox_Link($thickBoxHeight, $thickBoxWidth, "", 
										$Lang['AppNotifyMessage']['MessageToNotSignedParents'], "", 'divPushMessage', $Content='', 'pushMessageButton');

# thickbox layout
$thickboxContent = '';
$thickboxContent .= '<div id="divPushMessage" style="display:none">';
$thickboxContent .= '<form id="messageMethodForm" action="#">';
$thickboxContent .= '<table class="form_table_v30">';
$thickboxContent .= '<tr>';
$thickboxContent .= '<td class="field_title"><span>'.$Lang['AppNotifyMessage']['MessagingMethod'].'</span></td>';
$thickboxContent .= '<td>';
$smsChecked = 1;
if ($plugin['ASLParentApp'] || $plugin['eClassApp']) {
	$smsChecked = 0;
	$thickboxContent .= $linterface->Get_Radio_Button('pushMessage', 'messageMethod', 'pushMessage', $isChecked=1, $Class='', $Lang['AppNotifyMessage']['PushMessage'], $Onclick='', $Disabled='');
	$thickboxContent .= '<br/>';
}
if ($plugin['sms'] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-SMS"]) {
	$thickboxContent .= $linterface->Get_Radio_Button('SMS', 'messageMethod', 'SMS', $isChecked=$smsChecked, $Class='', $Lang['AppNotifyMessage']['SMS'], $Onclick='', $Disabled='');
	$thickboxContent .= '<br/>';
}
if (($plugin['ASLParentApp'] || $plugin['eClassApp']) && $plugin['sms'] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-SMS"]) {
	$thickboxContent .= $linterface->Get_Radio_Button('pushMessageThenSMS', 'messageMethod', 'pushMessageThenSMS', $isChecked=0, $Class='', $Lang['AppNotifyMessage']['PushMessageThenSMS'], $Onclick='', $Disabled='');
	$thickboxContent .= '</td>';
}
$thickboxContent .= '</tr>';
$thickboxContent .= '</table>';
$thickboxContent .= '<br/>';
$thickboxContent .= '<div align="center">';
$thickboxContent .= $linterface->GET_ACTION_BTN($Lang['AppNotifyMessage']['MessageToNotSignedParents'], "button", "js_send_message()");
if($plugin['eClassApp']){
	$thickboxContent .= '&nbsp;';
	$thickboxContent .= $linterface->GET_ACTION_BTN($Lang['MessageCenter']['ExportMobile'], "button", "goExportMobile()");
	$thickboxContent .= '<br /><br />';
}
$thickboxContent .= '</div>';
if($plugin['eClassApp']){
	$thickboxContent .= '<div>';
	$thickboxContent .= "<span class='tabletextremark'>".$Lang['MessageCenter']['ExportMobileRemarks']."</span>";
	$thickboxContent .= '</div>';
}	
$thickboxContent .= '</form>';
$thickboxContent .= '</div>';	

if ($plugin['eClassApp']) {
	$sendPushMessagePhp = 'notify_parent_via_eClassApp.php';
	$sendPushMessageThenSmsPhp = 'notify_parent_via_eClassApp_then_sms.php';
}
else {
	$sendPushMessagePhp = 'notify_parent_via_app.php';
	$sendPushMessageThenSmsPhp = 'notify_parent_via_app_then_sms.php';
}
?>

<script type="text/javascript" src= "<?=$PATH_WRT_ROOT?>templates/jquery/thickbox.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.blockUI.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.tablednd_0_5.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.cookies.2.2.0.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.scrollTo-min.js"></script>

<link rel="stylesheet" href="<?=$PATH_WRT_ROOT?>templates/jquery/thickbox.css" type="text/css" media="screen" />

<script language="javascript">
$(document).ready(function() {
	if ('<?=$directSend?>' == '1') {
		if ('<?=$sendMethod?>' == 'SMS') {
			notifyParentBySMS_send();
		}
		else if ('<?=$sendMethod?>' == 'pushMessageThenSMS') {
			notifyParentByAppThenSMS_send();
		}
	}
});

function csv_export(export_type)
{
	window.location="<?=$PATH_WRT_ROOT?>home/eService/notice/export_csv.php?NoticeID=<?php echo $NoticeID ?>&class=&all=1&unsignedList="+export_type;
}

function notifyParentByApp()
{
	$('#PreviewStatus').html('<?=$linterface->Get_Ajax_Loading_Image()?>');
	$.ajax({
	    url: '<?=$sendPushMessagePhp?>?NoticeID=<?=$NoticeID?>&csrf_token=' + $('input#csrf_token').val() + '&csrf_token_key='+$('input#csrf_token_key').val(),
	    error: function(xhr) {
			alert('request error');
	    },
	    success: function(response) {
	      	$('#PreviewStatus').html(response);
	    }
	});
}

function notifyParentBySMS()
{
//	$('#PreviewStatus').html('<?=$linterface->Get_Ajax_Loading_Image()?>');
//	$.ajax(
//		{
//			url: 'notify_parent_via_sms.php?NoticeID=<?=$NoticeID?>',
//			error: function(xhr) {
//				alert('request error');
//			},
//			success: function(response) {
//				$('#PreviewStatus').html(response);
//			}
//		}
//	);

	$('input#sendMethod').val('SMS');
	$('form#form1').attr('target', '_self').attr('action', 'confirm_send_sms.php').submit();
}

function notifyParentBySMS_send()
{
	$('#PreviewStatus').html('<?=$linterface->Get_Ajax_Loading_Image()?>');
	$.post(
		"notify_parent_via_sms.php", 
		{ 
			NoticeID: '<?=$NoticeID?>',
			SelectedClass: '<?=$SelectedClass?>',
			Message: $('input#Message').val(),
			csrf_token: $('input#csrf_token').val(),
			csrf_token_key: $('input#csrf_token_key').val()
		},
		function(response) {
			js_Hide_ThickBox();
			$('#PreviewStatus').html(response);
		}
	);
}

function notifyParentByAppThenSMS()
{
//	$('#PreviewStatus').html('<?=$linterface->Get_Ajax_Loading_Image()?>');
//	$.ajax(
//		{
//			url: '<?=$sendPushMessageThenSmsPhp?>?NoticeID=<?=$NoticeID?>',
//			error: function(xhr) {
//				alert('request error');
//			},
//			success: function(response) {
//				$('#PreviewStatus').html(response);
//			}
//		}
//	);

	$('input#sendMethod').val('pushMessageThenSMS');
	$('form#form1').attr('target', '_self').attr('action', 'confirm_send_sms.php').submit();
}

function notifyParentByAppThenSMS_send()
{
	$('#PreviewStatus').html('<?=$linterface->Get_Ajax_Loading_Image()?>');
	$.post(
		"<?=$sendPushMessageThenSmsPhp?>", 
		{ 
			NoticeID: '<?=$NoticeID?>',
			Message: $('input#Message').val(),
			csrf_token: $('input#csrf_token').val(),
			csrf_token_key: $('input#csrf_token_key').val()
		},
		function(response) {
			js_Hide_ThickBox();
			$('#PreviewStatus').html(response);
			Scroll_To_Element('PreviewStatus');
		}
	);
}

function js_send_message()
{
	var selectedVal = "";
	selectedVal = $('input:radio[name=messageMethod]:checked').val();
	
	if (selectedVal == 'pushMessage') {
		notifyParentByApp();
		js_Hide_ThickBox();
	} else if (selectedVal == 'SMS') {
		notifyParentBySMS();
	} else if (selectedVal == 'pushMessageThenSMS') {
		notifyParentByAppThenSMS();
	}
}

function js_show_thickbox()
{
	$('a#pushMessageButton').click();
}

function goExportMobile()
{
	var url = '<?=$PATH_WRT_ROOT?>' + '/home/eAdmin/GeneralMgmt/MessageCenter/export_mobile.php?task=byNoticeID';
	$('form#form1').attr('target', '' );
	$('form#form1').attr('action', url ).submit();
	$('form#form1').attr('target', '_blank' );
	$('form#form1').attr('action', 'result_print_preview.php' );
}
</script>

<form name="print_form" id="form1" action="result_print_preview.php" method="post" target = "_blank">

<table class="form_table_v30">
<tr valign='top'>
	<td class='field_title'><?=$i_Notice_NoticeNumber?></td>
	<td><?=$lnotice->NoticeNumber?></td>
</tr>

<tr valign='top'>
	<td class='field_title'><?=$i_Notice_Title?></td>
	<td><?=$lnotice->Title?></td>
</tr>
</table>

<br>
<?=$x?>

<!-- Button //-->
<div class="edit_bottom_v30">
<p class="spacer"></p>
	<?= $linterface->GET_ACTION_BTN($button_export_all, "button", "csv_export(0)") ?>
	<?= $linterface->GET_ACTION_BTN($Lang['Notice']['ExportUnsignedNoticeList'], "button", "csv_export(1)") ?>
	<?= $linterface->GET_ACTION_BTN($button_print . ($intranet_session_language=="en"?" ":"") . $button_preview, "button", "document.print_form.submit();") ?>
	<?php if (($plugin['ASLParentApp'] || $plugin['eClassApp'] || $plugin['sms']) && $all_signed!=$all_total && $lnotice->hasViewRight())
		{

			// echo $linterface->GET_ACTION_BTN($Lang['AppNotifyMessage']['eNotice']['button'], "button", "notifyParentByApp();");
			echo $linterface->GET_ACTION_BTN($Lang['AppNotifyMessage']['MessageToNotSignedParents'], "button", "js_show_thickbox();");
			echo $pushMessageButton;
		}
	?>
	<?= $linterface->GET_ACTION_BTN($button_close, "button", "self.close()","submit3") ?>
<p class="spacer"></p>
</div>
<p><center><span id='PreviewStatus'></span></center></p>

<input type="hidden" name="NoticeID" value="<?=$NoticeID?>">
<input type="hidden" name="Message" id="Message" value="<?=intranet_htmlspecialchars($Message)?>">
<input type="hidden" name="fromPage" id="fromPage" value="result">
<input type="hidden" name="sendMethod" id="sendMethod" value="">

<?php echo csrfTokenHtml(generateCsrfToken()); ?>
</form>

<?=$thickboxContent?>

<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>