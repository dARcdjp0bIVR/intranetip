<?php
# Using:

################# Change Log [Start] ############
#
# 	Date:   2020-09-07  Bill    [2020-0604-1821-16170]
#			access right checking > replace isApprovalUser() by isSchoolNoticeApprovalUser()
#
#   Date:   2020-05-04  Bill    [2020-0407-1445-44292]
#           - support class teacher audience limit checking + UI update ($sys_custom['eNotice']['ClassTeacherSelectOwnClassStudentOnly'])
#
#   Date:   2020-04-16  Bill    [2020-0310-1051-05235]
#           Special notice > access checking for teacher    ($sys_custom['eNotice']['SpecialNoticeForPICAndAdmin'])
#
#   Date:   2020-04-15 Bill    [2020-0310-1051-05235]
#           - added js_handle_special_notice_options_display()   ($sys_custom['eNotice']['SpecialNoticeForPICAndAdmin'])
#
#   Date:   2020-02-17 Bill
#           - replace php inline coding (if('<?=$plugin['eClassApp']>'=='1') { ... }) in js
#
#   Date:   2019-10-18 Bill     [2019-1018-0914-28066]
#           - disable submit button after submit the form
#
#   Date:   2019-10-15 Bill     [DM#3678 & DM#3679]
#           - support search suspended users using auto fill-in methods
#
#	Date:	2019-09-25	Philips [2019-0821-1143-58235]
#			- call checkedSendPushMessage() in $(document).ready()
#
#   Date:   2019-07-05 Bill     [2019-0412-1159-01235]
#           Improved: use POST instead of GET for ajax_search_user_by_classname_classnumber.php
#
#   Date:   2018-11-08  Bill    [DM#3470]
#           Fixed: support js action related to [Notify PIC using email] & [Notify Class Teachers using email]
#
#   Date:   2018-10-10  Bill    [2018-1008-1030-09073]
#           Fixed: cannot check send push message notification to PIC / Class Teacher checkbox
#
#   Date:   2018-09-05  Bill    [2018-0903-0945-20066]
#           - support js action related to [Notify Class Teacher] & [Notify PIC]
#
#	Date:	2017-10-13	Bill
#			Fixed: JS error due to not using Student App
#
#	Date:	2016-09-26	Villa
#			- add to module setting - "default using eclass App to notify"
#			- add to module setting - "default using eclass App to notify"
#
#	Date:	2016-09-21 Villa #T85436 
#			- added Push Message Setting box, add send push message method that only send to new added
#
#	Date:	2016-09-12	Bill
#			- modified js checkform(), send time must be equal to or later than issue time
#
#	Date:	2016-07-18	Tiffany	
# 			- modified function checkedSendPushMessage()
#
#	Date:	2016-05-19	Kenneth
#			- modified access right, allow approver to access
#
#	Date:	2016-03-23	Kenneth		
#			- add logic, if recordStatus = 4, get choices of email / push msg choices
#
#	Date:	2015-11-09	Kenneth		[2015-1020-1555-27066]
#			- allow auto count eNotice deadline for future eNotice (JS)
#
#	Date:	2015-10-28	Bill	[2015-0416-1040-06164]
#			- support merge notice content
#
#	Date:	2015-10-05	Bill	[2015-0930-1022-55071]
#			- added input date format checking
#
#	Date:	2015-06-26 	Omas
#			- added autocompleter
#
#	Date:	2015-06-18 	Roy
#			- modified reset_innerHtml(), add $plugin[eClassApp] checking in reset_innerHtml()
#
#	Date:	2015-06-10	Roy
#			- modified reset_innerHtml(), checkform(), added clickedSendTimeRadio(), checkedSendPushMessage() for scheduled push message
#			- add parameter to return_FormContent()
#
#	Date:	2015-04-14	Bill	[2015-0323-1602-46073]
#			- add field to edit PICs of eNotice
#
#	Date:	2014-10-14 (Roy)
#			- add push message option in JavaScript
#
#	Date:	2014-05-05	YatWoon
#			- add remark for audience and remove remove for status change
#
#	Date:	2013-09-06	YatWoon
#			Improved: update the upload attachment method
#
#	Date:	2012-06-20 YatWoon
#			Fixed: If edit the reply slip again, the question with "enter" will become "<br>"
#
#	Date:	2012-05-04 YatWoon
#			Improved: skip the start date checking [Case#2012-0504-1030-05066]
#
#	Date:	2011-09-30 YatWoon
#			Enhanced: add "Copy" function [Case#2011-0802-1242-05071]
#
#	Date:	2011-02-16	YatWoon
#			- add option "Display question number"
#
#	Date:	2010-10-12 YatWoon
#			- client can modify attachment for distributed notice [wish list]
#
#	Date:	2010-06-18	YatWoon
#			- admin can edit notice even if the notice is not created by own
#
#	Date:	2010-05-03	YatWoon
#			- can edit past notice (just only some fields)
#
################# Change Log [End] ############

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libform.php");
include_once($PATH_WRT_ROOT."includes/libfiletable.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();

$CurrentPageArr['eAdminNotice'] = 1;
$CurrentPage = "PageNotice";

$lnotice = new libnotice($NoticeID);

$lform = new libform();
$lclass = new libclass();
$li = new libfilesystem();

// [2020-0310-1051-05235] Special notice > access checking for teacher
$allowTeacherAccess = true;
if($sys_custom['eNotice']['SpecialNoticeForPICAndAdmin'] && $lnotice->SpecialNotice)
{
    include_once($PATH_WRT_ROOT."includes/libuser.php");
    $lu = new libuser($UserID);

    if($lu->isTeacherStaff()) {
        $allowTeacherAccess = $_SESSION["SSV_USER_ACCESS"]["eAdmin-eNotice"] || $lnotice->isNoticePIC($NoticeID);
    }
}

// [2020-0407-1445-44292]
$isAudienceLimited = $lnotice->isClassTeacherAudienceTargetLimited();

// [2020-0604-1821-16170]
//if ((!$plugin['notice'] && $_SESSION["SSV_PRIVILEGE"]["notice"]["disabled"] || !$lnotice->isNoticeEditable()) && !$_SESSION["SSV_USER_ACCESS"]["eAdmin-eNotice"] && !$lnotice->isApprovalUser())
//if (((!$plugin['notice'] && $_SESSION["SSV_PRIVILEGE"]["notice"]["disabled"] || !$lnotice->isNoticeEditable()) && !$_SESSION["SSV_USER_ACCESS"]["eAdmin-eNotice"] && !$lnotice->isApprovalUser()) || !$allowTeacherAccess)
if (((!$plugin['notice'] && $_SESSION["SSV_PRIVILEGE"]["notice"]["disabled"] || !$lnotice->isNoticeEditable()) && !$_SESSION["SSV_USER_ACCESS"]["eAdmin-eNotice"] && !$lnotice->isSchoolNoticeApprovalUser()) || !$allowTeacherAccess)
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$now = time();
$start = strtotime($lnotice->DateStart);
$isIssuedNotice = 0;
$signed = $lnotice->returnNoticeSignedCount($NoticeID);
$RecordStatus = $lnotice->RecordStatus;

//if ($start <= $now && $lnotice->RecordStatus==DISTRIBUTED_NOTICE_RECORD)
if($signed[$NoticeID] > 0 && $RecordStatus == 1)
{
	$isIssuedNotice = 1;
}

# Attachment Folder
/*
if(!session_is_registered("noticeAttFolder") || $noticeAttFolder==""){
     session_register("noticeAttFolder");
}
*/
$noticeAttFolder = $lnotice->Attachment;
$DisplayQuestionNumber = $lnotice->DisplayQuestionNumber;

$path = "$file_path/file/notice/$noticeAttFolder";
$displayAttachment = $lnotice->displayAttachmentEdit("file2delete[]", $noticeAttFolder);

/*
if (!is_dir($path))
{
     $li->folder_new($path);
}

if(!empty($noticeAttFolder))
{
	
	$tmp_path = $path."tmp";
	$li->folder_remove_recursive($tmp_path);
	$li->folder_new($tmp_path);
	
	
	$li->folder_content_copy($path, $tmp_path);
	$lo = new libfiletable("", $tmp_path, 0, 0, "");
	$files = $lo->files;
}
*/

$type = $lnotice->RecordType;
// $recipientNames = $lnotice->returnRecipientNames();

// [2020-0407-1445-44292] limited to 2 audience types only
if($isAudienceLimited)
{
    $type_selection = "
    <SELECT name='type' onChange='selectAudience()'>
        <OPTION value='3' ". ($type==3? "selected":"").">$i_Notice_RecipientTypeClass</OPTION>
        <OPTION value='4' ". ($type==4? "selected":"").">$i_Notice_RecipientTypeIndividual</OPTION>
    </SELECT>";
}
else
{
    $type_selection = "
    <SELECT name='type' onChange='selectAudience()'>
        <OPTION value='1' ". ($type==1? "selected":"").">$i_Notice_RecipientTypeAllStudents</OPTION>
        <OPTION value='2' ". ($type==2? "selected":"").">$i_Notice_RecipientTypeLevel</OPTION>
        <OPTION value='3' ". ($type==3? "selected":"").">$i_Notice_RecipientTypeClass</OPTION>
        <OPTION value='4' ". ($type==4? "selected":"").">$i_Notice_RecipientTypeIndividual</OPTION>
    </SELECT>";
}

// $type_selection = $recipientNames[0];
// if ($recipientNames[1]=="")
// {
//     $nextSelect = "";
// }
// else
// {
//     switch($type)
//     {
//            case 2: $cellTitle = $i_Notice_RecipientLevel; break;
//            case 3: $cellTitle = $i_Notice_RecipientClass; break;
//            case 4: $cellTitle = $i_Notice_RecipientIndividual; break;
//     }
//     $nextSelect = "<tr valign='top'>";
// $nextSelect .= "<td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'><span class='tabletext'>$cellTitle</span></td>";
//     $nextSelect .= "<td class='tabletext'>".$recipientNames[1]."</td></tr>\n";
// }

$start = $lnotice->DateStart;
$end = $lnotice->DateEnd;
$Title = $lnotice->Title;
$NoticeNumber = $lnotice->NoticeNumber;
// $Description = htmlspecialchars_decode($lnotice->Description);
$Description = $lnotice->Description;

$Question = $lnotice->Question;
$Question = str_replace("<br>","\r\n",$Question);
$Question = intranet_htmlspecialchars($Question);

$AllFieldsReq = $lnotice->AllFieldsReq;
$RecipientID = $lnotice->RecipientID;

// [2015-0323-1602-46073] - Get PICs of current eNotice
$PICUser = $lnotice->returnNoticePICNames();

// [2015-0416-1040-06164] get value for merge notice
$ContentType = $lnotice->ContentType;
$MergeFormat = $lnotice->MergeFormat;
$MergeType = $lnotice->MergeType;
$MergeFile = $lnotice->MergeFile;

// 2015-6-3
// 2016-10-18 Villa update the sql to get the Apptype setting in order to set
// [2018-1008-1030-09073] update sql to include module 'eNoticeS' 
$sql = "SELECT
            NotifyDateTime, SendTimeMode, Apptype
        FROM 
            INTRANET_APP_NOTIFY_MESSAGE_MODULE_RELATION as ianmmr
            INNER JOIN INTRANET_APP_NOTIFY_MESSAGE as ianm ON (ianmmr.NotifyMessageID = ianm.NotifyMessageID)
		WHERE 
            (ianmmr.ModuleName = 'eNotice' OR ianmmr.ModuleName = 'eNoticeS') AND 
            ianmmr.ModuleRecordID = '$NoticeID' AND
            ianm.RecordStatus = '1'
        GROUP BY Apptype
        ORDER BY ianm.NotifyMessageID";
$resultAry = $lnotice->returnResultSet($sql);

$appType = BuildMultiKeyAssoc($resultAry, "Apptype", array('Apptype'), 1);
if (count($resultAry) > 0) {
	$sendTimeMode = $resultAry[0]["SendTimeMode"];
	$sendTimeString = $resultAry[0]["NotifyDateTime"];
}
else {
	$sendTimeMode = "";
	$sendTimeString = "";
}

### Title ###
$TAGS_OBJ[] = array($Lang['eNotice']['SchoolNotice']);
$MODULE_OBJ = $lnotice->GET_MODULE_OBJ_ARR();

if($copied) {
	$xmsg = $Lang['General']['ReturnMessage']['Copied'] ;
}
$linterface->LAYOUT_START($xmsg);

$PAGE_NAVIGATION[] = array($i_Notice_Edit);

$thisAudience = $lnotice->returnRecipientNames();

echo $linterface->Include_AutoComplete_JS_CSS();
echo $linterface->Include_JS_CSS();

// [2015-0416-1040-06164] Merge CSV sample files
$sample_csv_file1 = GET_CSV("sample_data.csv", '', false);
$sample_csv_file1_userlogin = GET_CSV("sample_data_userlogin.csv", '', false);
$sample_csv_file2 = GET_CSV("sample_data_format2.csv", '', false);
$sample_csv_file2_userlogin = GET_CSV("sample_data_format2_userlogin.csv", '', false);

if($lnotice->needApproval){
	if($lnotice->hasApprovalRight()){
		$needApprove = false;
	}
	else{
		$needApprove = true;
	}
}

$submitBtnText = $Lang['Btn']['Submit'];

# Check if there is a schedule will be sending later	26-9-2016 Villa
$sendTimeMode = $resultAry[0]['SendTimeMode'];
$passScheduled = false;
$nowTime = strtotime("now");
if ($sendTimeMode == "scheduled" && $sendTimeString != "") {
	$sendTime = strtotime($sendTimeString);
	if ($sendTime > $nowTime) {
		$passScheduled = true;
	} else {
		$sendTimeString = "";
		$passScheduled = false;
	}
	
	// [2018-1008-1030-09073] special handling for checkbox
	$appTargetCT = false;
	$appTargetPIC = false;
	if($appType['T'] == 'T')
	{
	    $picIDAry = array_unique(array_filter(Get_Array_By_Key($PICUser, 'UserID')));
	    
	    $sql = "SELECT
                    CONCAT(ianmt.TargetType, ianmt.TargetID) as TeacherID
        	    FROM
            	    INTRANET_APP_NOTIFY_MESSAGE_MODULE_RELATION as ianmmr
            	    INNER JOIN INTRANET_APP_NOTIFY_MESSAGE as ianm ON (ianmmr.NotifyMessageID = ianm.NotifyMessageID)
            	    INNER JOIN INTRANET_APP_NOTIFY_MESSAGE_TARGET as ianmt ON (ianm.NotifyMessageID = ianmt.NotifyMessageID AND ianmt.TargetType = 'U')
        	    WHERE
            	    ianmmr.ModuleName = 'eNoticeS' AND
            	    ianmmr.ModuleRecordID = '$NoticeID' AND
            	    ianm.RecordStatus = '1' AND
            	    ianm.AppType = 'T' AND
            	    ianmt.TargetType = 'U'
        	    ORDER BY ianmt.TargetID";
	    $targetIDAry = $lnotice->returnVector($sql);
	    $targetIDAry = array_unique(array_filter($targetIDAry));
	    
	    $targetPICCount = 0;
	    foreach($targetIDAry as $thisTargetID) {
	        if(in_array($thisTargetID, $picIDAry)) {
	            $targetPICCount++;
	        }
	        else {
	            $appTargetCT = true;
	        }
	    }
	    if(!empty($picIDAry) && $targetPICCount > 0) {
	        $appTargetPIC = ($targetPICCount == count($picIDAry));
	    }
	}
}
?>

<script language="javascript">
var AutoCompleteObj_ClassNameClassNumber;
$(document).ready( function() {
	// 26-9-2016 Villa hide the setting div
    <?php if($plugin['eClassApp']) { ?>
		$('input#sendNewSelect').hide();
		$('label#sendNewSelect').hide();

        <?php if(!$passScheduled) { ?>
			$('div#PushMessageSetting').hide();
		<?php } else { ?>
            <?php if($appType['S'] == 'S') { ?>
				$('#pushmessagenotify_Students').attr("checked","checked");
			<?php } ?>
            <?php if($appType['P'] == 'P') { ?>
				$('#pushmessagenotify').attr("checked","checked");
            <?php } ?>
            <?php if($appType['T'] == 'T') { ?>
				<?php if($appTargetPIC) { ?>
					$('#pushmessagenotify_PIC').attr("checked","checked");
				<?php } ?>
				<?php if($appTargetCT) { ?>
					$('#pushmessagenotify_ClassTeachers').attr("checked","checked");
				<?php } ?>
			<?php } ?>
			$('div#PushMessageSetting').show();
			$('#sendTimeSettingDiv').show();
		<?php } ?>
	<?php } ?>
	
	<?php if(!$isIssuedNotice && $ContentType == 2){ ?>
		// display correct settings
		ShowMergeSetting(2);
		ShowCSVFormat(<?=$MergeFormat?>);
	<?php } ?>

    <?php if(!$isIssuedNotice) { ?>
		var initStartDate = getMillisecond(getCurrentDate());
		//var endDate = getMillisecond($("input#DateEnd").val());
		//var diff = endDate-initStartDate;
		var diff = <?=($lnotice->defaultNumDays*24*3600*1000)?>;
		var newStartDate;
		
		$(document).click(function(){
			newStartDate = getMillisecond(getCurrentDate());
			if(isNaN(newStartDate)){
				return;
			}

			if(initStartDate != newStartDate){
				var newDateEnd = new Date(newStartDate+diff);
				jQuery.datepick._selectDay('#DateEnd',newDateEnd,this);
				initStartDate = newStartDate;
			}
		});
	<?php } ?>

	eFormAppJS.func.init();

	<?php if ($plugin['eClassApp']){ ?>
        checkedSendPushMessage();
    <?php } ?>
});

function getMillisecond(dateString){
	var millisecond = Date.parse(dateString);
	if(isNaN(millisecond)){
		dateString = dateString.replace(/-/g, '/');
		millisecond = Date.parse(dateString);
	}

	return Date.parse(dateString);
}

function getCurrentDate(){
	var date = $("#DateStart").val();
	return date;
}

function Init_JQuery_AutoComplete(InputID_ClassNameClassNumber, InputID_UserLogin) {
	AutoCompleteObj_ClassNameClassNumber = $("input#" + InputID_ClassNameClassNumber).autocomplete(
		"ajax_search_user_by_classname_classnumber.php",
		{  			
			onItemSelect: function(li) {
				Add_Selected_User(li.extra[1], li.selectValue, InputID_ClassNameClassNumber);
			},
			formatItem: function(row) {
				return row[0];
			},
			maxItemsToShow: 100,
			minChars: 1,
			delay: 0,
			width: 200
		}
	);
	
	Update_Auto_Complete_Extra_Para();
}

function Add_Selected_User(UserID, UserName, InputID) {
	var UserID = UserID || "";
	var UserName = UserName || "";
	var UserSelected = document.getElementById('target[]');
	UserSelected.options[UserSelected.length] = new Option(UserName, UserID);
	
	Update_Auto_Complete_Extra_Para();
	
	// reset and refocus the textbox
	$('input#' + InputID).val('').focus();
}

function Update_Auto_Complete_Extra_Para(){
	checkOptionAll(document.getElementById('form1').elements["target[]"]);

	ExtraPara = new Array();
	ExtraPara['SelectedUserIDList'] = Get_Selection_Value('target[]', 'Array', true);
    ExtraPara['IncludeSuspenedAccount'] = '1';
	AutoCompleteObj_ClassNameClassNumber[0].autocompleter.setExtraParams(ExtraPara);
}

function js_Remove_Selected_Student(){
	checkOptionRemove(document.getElementById('target[]'));
	Update_Auto_Complete_Extra_Para();
}

function selectAudience()
{
	var tid = document.form1.type.value;
	
	$('#div_audience').load(
		'ajax_loadAudience.php', 
		{type: tid, RecipientID: '<?=$RecipientID?>', NoticeID: '<?=$NoticeID?>'}, 
		function (data){
			if(tid == 4){
				// initialize jQuery Auto Complete plugin
				eFormAppJS.func.init();
				Init_JQuery_AutoComplete('UserClassNameClassNumberSearchTb', 'UserLoginSearchTb');
				$('input#UserClassNameClassNumberSearchTb').focus();
			}
		}
	);
}

function reset_innerHtml()
{
	document.getElementById('div_audience_err_msg').innerHTML = "";
	<?php if(!$isIssuedNotice) {?>
		document.getElementById('div_DateStart_err_msg').innerHTML = "";
	<?php } ?>
 	document.getElementById('div_NoticeNumber_err_msg').innerHTML = "";
 	document.getElementById('div_Title_err_msg').innerHTML = "";
 	document.getElementById('div_DateEnd_err_msg').innerHTML = "";
 	<?php if($plugin['eClassApp']) { ?>
 		document.getElementById('div_push_message_date_err_msg').innerHTML = "";
 	<?php } ?>
}

function Big5FileUploadHandler() {
	 new_attach = parseInt(document.form1.attachment_size.value);
	 for(i=0; i<new_attach; i++){
	 	objFile = eval('document.form1.filea'+i);
	 	objUserFile = eval('document.form1.hidden_userfile_name'+i);
	 	if(typeof(objFile) != 'undefined' && objFile.value != '' && typeof(objUserFile) != 'undefined') {
		 	Ary = objFile.value.split('\\');
		 	if(Ary != null) {
			 	objUserFile.value = Ary[Ary.length - 1];
			}
		}
	 }

	 return true;
}

function checkform(obj)
{
	var error_no = 0;
	var focus_field = "";
	var csv_error = false;
	
	//// Reset div innerHtml
	reset_innerHtml();

	// disable submit button
    $('#submit2').attr('disabled', true);
    $('#submit2').removeClass("formbutton_v30").addClass('formbutton_disable_v30');

	// if (obj.ContentType.value != "2") {
	if ($("input[name='ContentType']:checked").val() != "2") {
		if(obj.type.value == 2)
		{
			//if(!countChecked(obj, "target[]")){ alert("<?=$i_alert_pleaseselect?><?=$i_Notice_RecipientLevel?>"); return false; }
			if(!countChecked(obj, "target[]"))
			{ 
				document.getElementById('div_audience_err_msg').innerHTML = '<font color="red"><?=$i_alert_pleaseselect?><?=$i_Notice_RecipientLevel?></font>';
				error_no++;
				focus_field = "type";
			}
		}
		
		if(obj.type.value == 3)
		{
			//if(!countChecked(obj, "target[]")){ alert("<?=$i_alert_pleaseselect?><?=$i_Notice_RecipientClass?>"); return false; }
			if(!countChecked(obj, "target[]"))
			{ 
				document.getElementById('div_audience_err_msg').innerHTML = '<font color="red"><?=$i_alert_pleaseselect?><?=$i_Notice_RecipientClass?></font>';
				error_no++;
				focus_field = "type";
			}
		}
		
		if(obj.type.value == 4)
	    {
			//if(obj.elements["target[]"].length==0){ alert("<?php echo $i_frontpage_campusmail_choose; ?>"); return false; }
			//checkOptionAll(obj.elements["target[]"]);
			
			checkOptionAll(obj.elements["target[]"]);
			if(obj.elements["target[]"].length == 0)
			{
				document.getElementById('div_audience_err_msg').innerHTML = '<font color="red"><?=$i_frontpage_campusmail_choose?></font>';
				error_no++;
				focus_field = "type";
			}
		}
	}
	
	if(!check_text_30(obj.NoticeNumber, "<?php echo $i_alert_pleasefillin.$i_Notice_NoticeNumber; ?>.", "div_NoticeNumber_err_msg"))
	{
		error_no++;
		if(focus_field == "")	focus_field = "NoticeNumber";
	}
	
	if(!check_text_30(obj.Title, "<?php echo $i_alert_pleasefillin.$i_Notice_Title; ?>.", "div_Title_err_msg"))
	{
		error_no++;
		if(focus_field == "")	focus_field = "Title";
	}
	 
	<?php if(!$isIssuedNotice) {?>
		if(!check_date_without_return_msg(obj.DateStart))
		{
			error_no++;
			if(focus_field == "")	focus_field = "DateStart";
		}
		
		if(!check_date_without_return_msg(obj.DateEnd))
		{
			error_no++;
			if(focus_field == "")	focus_field = "DateEnd";
		}
	
		if (compareDate(obj.DateStart.value, obj.DateEnd.value) > 0) 
		{
			document.getElementById('div_DateEnd_err_msg').innerHTML = '<font color="red"><?=$i_Homework_new_duedate_wrong?></font>';
			error_no++;
			if(focus_field == "")	focus_field = "DateEnd";
		}
		
		// check merge csv file for Content Type: Merge Content if CSV changed
		if ($('input#ContentType2').attr('checked') && $('input#changeCSV').val() == "1")
		{
			objCSVFile = eval('document.form1.FileCSV');
			objCSVFileName = eval('document.form1.FileCSV_Name');

			// assign filename to FileCSV_Name
			if(objCSVFile != null && objCSVFile.value != '' && objCSVFileName != null)
		 	{
			 	var fileAry = objCSVFile.value.split('\\');
			 	objCSVFileName.value = fileAry[fileAry.length - 1];

			 	// Empty filename or not CSV file
				if($.trim(objCSVFileName.value) == "" || objCSVFileName.value.indexOf(".csv") == -1)
				{
					alert('<?=$Lang['eNotice']['ProblemNoCSV']?>');
					csv_error = true;
				}
			}
			else
			{
				alert('<?=$Lang['eNotice']['ProblemNoCSV']?>');
				csv_error = true;
			}
		}
     <?php } else { ?>
		if(!check_date_without_return_msg(obj.DateEnd))
		{
			error_no++;
			if(focus_field == "")	focus_field = "DateEnd";
		}
		
		if (compareDate("<?=$lnotice->DateStart?>", obj.DateEnd.value) > 0) 
		{
			document.getElementById('div_DateEnd_err_msg').innerHTML = '<font color="red"><?=$i_Homework_new_duedate_wrong?></font>';
			error_no++;
			if(focus_field == "")	focus_field = "DateEnd";
		}
	<?php } ?>
	
	if(obj.elements["target_PIC[]"].length != 0)
	{
		checkOptionAll(obj.elements["target_PIC[]"]);
	}
	
	var scheduleString = '';
    if ($('input#sendTimeRadio_scheduled').attr('checked')) {
    	scheduleString = $('input#sendTime_date').val() + ' ' + str_pad($('select#sendTime_hour').val(),2) + ':' + str_pad($('select#sendTime_minute').val(),2) + ':00';
    }
    $('input#sendTimeString').val(scheduleString);

    var minuteValid = false;
	if (str_pad($('select#sendTime_minute').val(),2) == '00' || str_pad($('select#sendTime_minute').val(),2) == '15' || str_pad($('select#sendTime_minute').val(),2) == '30' || str_pad($('select#sendTime_minute').val(),2) == '45') {
		minuteValid = true;
	}
	
	// if scheduled message => check time is past or not
	var dateNow = new Date();
	var dateNowString = dateNow.getFullYear() + '-' + str_pad((dateNow.getMonth()+1),2) + '-' + str_pad(dateNow.getDate(),2) + ' ' + str_pad(dateNow.getHours(),2) + ':' + str_pad(dateNow.getMinutes(),2) + ':' + str_pad(dateNow.getSeconds(),2);
	if($('#pushmessagenotify').attr('checked') || $('#pushmessagenotify_Students').attr('checked') || $('#pushmessagenotify_PIC').attr('checked') || $('#pushmessagenotify_ClassTeachers').attr('checked')){
		if ($('input#sendTimeRadio_scheduled').attr('checked') && minuteValid && scheduleString <= dateNowString) {
			// alert('<?=$Lang['AppNotifyMessage']['WarningMsg']['SendTimeInvalid']?>');
			// return false;
			document.getElementById('div_push_message_date_err_msg').innerHTML = '<font color="red"><?=$Lang['AppNotifyMessage']['WarningMsg']['SendTimeInvalid']?></font>';

			error_no++;
			if(focus_field == "")	focus_field = "sendTime_date";
		}
	}

	// assign time into hidden field
	var dateStartTime = str_pad($('#DateStart_hour').val(),2)+':'+str_pad($('#DateStart_min').val(),2)+':00';
	$('#DateStartTime').val(dateStartTime);
	var dateEndTime = str_pad($('#DateEnd_hour').val(),2)+':'+str_pad($('#DateEnd_min').val(),2)+':59';
	$('#DateEndTime').val(dateEndTime);
	
	// if scheduled message => check time is within 30 days
	var maxDate = new Date();
	maxDate.setDate(dateNow.getDate() + 31);
	var maxDateString = maxDate.getFullYear() + '-' + str_pad((maxDate.getMonth()+1),2) + '-' + str_pad(maxDate.getDate(),2) + ' ' + str_pad(maxDate.getHours(),2) + ':' + str_pad(maxDate.getMinutes(),2) + ':' + str_pad(maxDate.getSeconds(),2);
	if($('#pushmessagenotify').attr('checked') || $('#pushmessagenotify_Students').attr('checked') || $('#pushmessagenotify_PIC').attr('checked') || $('#pushmessagenotify_ClassTeachers').attr('checked')){
		if ($('input#sendTimeRadio_scheduled').attr('checked') && minuteValid && scheduleString > maxDateString) {
			// alert('<?=$Lang['AppNotifyMessage']['WarningMsg']['PleaseSelectDateWithinRange']?>');
			$('input#sendTime_date').focus();
			// return false;
			document.getElementById('div_push_message_date_err_msg').innerHTML = '<font color="red"><?=$Lang['AppNotifyMessage']['WarningMsg']['PleaseSelectDateWithinRange']?></font>';
			error_no++;
			if(focus_field == "")	focus_field = "sendTime_date";
		}
	}
	
	<?php if(!$isIssuedNotice) { ?>
		// if scheduled message => check send time is earlier than issue time
		if($('#pushmessagenotify').attr('checked') || $('#pushmessagenotify_Students').attr('checked') || $('#pushmessagenotify_PIC').attr('checked') || $('#pushmessagenotify_ClassTeachers').attr('checked')){
			var DateStartString = $('input#DateStart').val() + ' ' + str_pad($('#DateStart_hour').val(),2)+':'+str_pad($('#DateStart_min').val(),2)+':00';
			if ($('input#sendTimeRadio_scheduled').attr('checked') && minuteValid && scheduleString < DateStartString) {
				$('input#sendTime_date').focus();
				// return false;
				document.getElementById('div_push_message_date_err_msg').innerHTML = '<font color="red"><?=$Lang['AppNotifyMessage']['WarningMsg']['SendTimeMustAfterStartDate']?></font>';
				error_no++;
				if(focus_field == "")	focus_field = "sendTime_date";
			}
		}
	<?php } ?>
	
	// check is type radio is checked
	var sus_status = $('input[name=sus_status]:checked').val();
	if(sus_status > 0){
		//do nothing
	}
	else{
		error_no++;
		if(focus_field == "")	focus_field = "status1";
	}

    var canSubmit = false;
	if(error_no > 0){
		eval("obj." + focus_field +".focus();");
        $('#submit2').attr('disabled', false);
        canSubmit = false;
	}
	else if(csv_error){
		$('#submit2').attr('disabled', false);
        canSubmit = false;
	}
	else
	{
		// var go_on = 1;
		/*
		// check status changed and display warning (changed from Distrubute only)
		if(obj.original_status.value==1 && !obj.sus_status[0].checked)
		{
			if(!confirm("<?=$Lang['eNotice']['UpdateStatusWarning']?>"))
				go_on = 0;
		}
		*/
		
		// if(go_on)
		// {
			//checkOptionAll(obj.elements["Attachment[]"]);

            canSubmit = Big5FileUploadHandler();
			
			// create a list of files to be deleted
	        objDelFiles = document.getElementsByName('file2delete[]');
	        files = obj.deleted_files;
	        if(objDelFiles == null) {
	            return true;
            }

	        x = "";
	        for(i=0; i<objDelFiles.length; i++){
		        if(objDelFiles[i].checked == true){
			 		x += objDelFiles[i].value + ":";
			    }
		    }
		    files.value = x.substr(0,x.length - 1);
	        // return true;
		// }
		// return false;
	}

    if (canSubmit) {
        $('form#form1').submit();
    }
    else {
        $('#submit2').attr('disabled', false);
        $('#submit2').removeClass("formbutton_disable_v30").addClass('formbutton_v30');
    }
}

var no_of_upload_file = <? echo $no_file=="" ? 1 : $no_file;?>;
function add_field()
{
	var table = document.getElementById("upload_file_list");
	var row = table.insertRow(no_of_upload_file);
	var cell = row.insertCell(0);
	x = '<input class="file" type="file" name="filea' + no_of_upload_file + '" size="40">';
	x += '<input type="hidden" name="hidden_userfile_name' + no_of_upload_file + '">';
	cell.innerHTML = x;

	no_of_upload_file++;
	document.form1.attachment_size.value = no_of_upload_file;
}

function setRemoveFile(index,v)
{
	obj = document.getElementById('a_'+index);
	obj.style.textDecoration = v?"line-through":"";
}

<?php if($plugin['eClassApp']) { ?>
	function clickedSendTimeRadio(targetType) {
		if (targetType == 'now') {
			$('div#specificSendTimeDiv').hide();
			$('input#sendNewSelect').show();
			$('label#sendNewSelect').show();
		}
		else {
			$('div#specificSendTimeDiv').show();
			$('input#sendNewSelect').hide();
			$('label#sendNewSelect').hide();
		}
	}

	function checkedSendPushMessage(checkBox) {
	//	if (checkBox.checked == true) {
	//		$('div#sendTimeSettingDiv').show();
	//	}
	//	else {
	//		$('div#sendTimeSettingDiv').hide();
	//	}
	
	    if($('#pushmessagenotify').attr('checked') || $('#pushmessagenotify_Students').attr('checked') || $('#pushmessagenotify_PIC').attr('checked') || $('#pushmessagenotify_ClassTeachers').attr('checked')){    	
	    	$('div#sendTimeSettingDiv').show();
	    	$('div#PushMessageSetting').show();
	    }
	    else{    	
	    	$('div#sendTimeSettingDiv').hide();
	    	$('div#PushMessageSetting').hide();
	    }
	}
<?php } ?>

function ShowCSVFormat(FormatNo)
{
	if (FormatNo == 1)
	{
		$('#CSV_Sample2').hide();
		$('#CSV_Sample1').show();
	}
	else
	{
		$('#CSV_Sample1').hide();
		$('#CSV_Sample2').show();
	}
}

function ShowMergeSetting(FormatNo)
{
	if (FormatNo == 1)
	{
		$('#MergeCSVTr').hide();
		$('#MergeTypeTr').hide();
		$('#mergeRemark').hide();
		$('tr.selectStudentArea').show();
	} 
	else
	{
		$('#MergeCSVTr').show();
		$('#MergeTypeTr').show();
		$('#mergeRemark').show();
		$('tr.selectStudentArea').hide();
		// $('#div_audience .select_studentlist').empty();
	}
}

function jsChangeCSV()
{
	$('#UploadCSV').show();
	// update parms
	$('input#changeCSV').val("1");
}

function previewCSVFile() {
	window.setTimeout(function(){
		var error_no = 0;
		var focus_field = "";
		
		// reset warning msg
		reset_innerHtml();
		
		$('#PreviewBtn').attr('disabled', true);
		
		// get form settings
		var formObj = document.form1;
		var oldTarget = document.form1.target;
		var oldAction = document.form1.action;

		// if (formObj.ContentType.value != "2") {
		if ($("input[name='ContentType']:checked").val() != "2") {
			// Audience
			if(formObj.type.value == 2)
			{
				if(!countChecked(formObj, "target[]"))
				{ 
					document.getElementById('div_audience_err_msg').innerHTML = '<font color="red"><?=$i_alert_pleaseselect?><?=$i_Notice_RecipientLevel?></font>';
					focus_field = "type";
				}
			}
			if(formObj.type.value == 3)
			{
				if(!countChecked(formObj, "target[]"))
				{ 
					document.getElementById('div_audience_err_msg').innerHTML = '<font color="red"><?=$i_alert_pleaseselect?><?=$i_Notice_RecipientClass?></font>';
					focus_field = "type";
				}
			}
		    if(formObj.type.value == 4)
		    {
			    checkOptionAll(formObj.elements["target[]"]);
			    
				if(formObj.elements["target[]"].length == 0)
				{ 
					document.getElementById('div_audience_err_msg').innerHTML = '<font color="red"><?=$i_frontpage_campusmail_choose?></font>';
					focus_field = "type";
				}
			}
		}
		
		// check merge csv file
		var withCSVFile = false;
		if ($('input#ContentType2').attr('checked'))
		{
			objCSVFile = eval('document.form1.FileCSV');
			objCSVFileName = eval('document.form1.FileCSV_Name');

			// assign filename to FileCSV_Name
			if(objCSVFile != null && objCSVFile.value != '' && objCSVFileName != null)
		 	{
			 	var fileAry = objCSVFile.value.split('\\');
			 	objCSVFileName.value = fileAry[fileAry.length - 1];

			 	// Not empty filename and CSV file
				if($.trim(objCSVFileName.value) != "" && objCSVFileName.value.indexOf(".csv") != -1)
				{
					withCSVFile = true;
				}
			}
			// preview current csv file
			if(objCSVFileName != null && $.trim(objCSVFileName.value) == "" && $('input#changeCSV').val() == 0 && document.form1.originalMergeFile.value != ""){
				withCSVFile = true;
			}
		}
		
		// valid csv file
		if(withCSVFile && focus_field == "")
		{
			// open thickbox
			$('#previewLink').click();
				
			// settings for preview
			formObj.target = 'csvPreviewFrame';
			formObj.action = 'preview_merge_csv.php';
			
			// form submit for preview
			$('form#form1').submit();
				
			// reset settings for normal submit
			formObj.target = oldTarget;
			formObj.action = oldAction;
			
			// update parms
			$('#previewCSV').val(1);
		}
		// Empty Audience
		else if(focus_field != "")
		{
			eval("formObj." + focus_field +".focus();");
		}
		// invalid or empty file
		else if(!withCSVFile)
		{
			alert('<?=$Lang['eNotice']['ProblemNoCSV']?>');
		}
		
		$('#PreviewBtn').attr('disabled', false);
	}, 500);
}
</script>

<?= $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION) ?>
<br>
<form name="form1" id="form1" method="post" action="edit_update.php" enctype="multipart/form-data" >

<?php
### UserLogin
$UserPICTextarea = '';
$UserPICTextarea .= '<div style="float:left;" class="pic_textarea">';
$UserPICTextarea .= '<textarea style="height:100px;" id="PICAreaSearchTb" name="PICAreaSearchTb"></textarea><span id="textarea_msg"></span><br><br>';
$UserPICTextarea .= $linterface->GET_SMALL_BTN($Lang['AppNotifyMessage']['searchAndInsertPIC'], "button" , "");
$UserPICTextarea .= '</div>';

// <table class="form_table_v30">
$STUDENT_INPUT = "<tr valign='top' class='selectStudentArea'>
					<td nowrap class='field_title'>";
if(!$isIssuedNotice) {
	$STUDENT_INPUT .= "<span class='tabletextrequire'>*</span>";
}
$STUDENT_INPUT .= $i_Notice_RecipientType . "</td>
		<td>" . $type_selection . "<br><span id='div_audience_err_msg'></span>
		<div id='div_audience'></div>
		<font color='red'># " . $Lang['eNotice']['UpdateAudienceRemark'] . "</font>
	</td>
</tr>
<!-- Select PICs of eNotice -->
<tr valign='top'>
	<td nowrap class='field_title'>" . $i_Profile_PersonInCharge . "</td>
	<td>
		<table border='0' cellpadding='0' cellspacing='0'>
			<tr>
				<td class='tablerow2'>" . $Lang['AppNotifyMessage']['msgSearchAndInsertPICInfo'] . "<br>" . $UserPICTextarea. "</td>
				<td class='tabletext' ><img src='". $image_path.'/'.$LAYOUT_SKIN."/10x10.gif' width='10' height='1'></td>
				<td>" . $linterface->GET_SELECTION_BOX($PICUser, "id='target_PIC' name='target_PIC[]' size='6' multiple", "") . "</td>
				<td valign='bottom'>" . $linterface->GET_BTN($Lang['Btn']['Select'], "button", "newWindow('/home/common_choose/index.php?fieldname=target_PIC[]&permitted_type=1&DisplayGroupCategory=1&DisplayInternalRecipientGroup=1&Disable_AddGroup_Button=1', 16)") . "<br>
					" . $linterface->GET_BTN($Lang['Btn']['Remove'], "button", "checkOptionRemove(document.form1.elements['target_PIC[]'])") . "
				</td>
			</tr>
		</table>
	</td>
</tr>";
// </table>
?>

<div id="div_content">
<?php
	$formBody = $lnotice->return_FormContent($start, $end, $isIssuedNotice, $Title, $Description, $Question, $NoticeNumber, $AllFieldsReq, $RecordStatus, $noticeAttFolder, $DisplayQuestionNumber, $copied, ($plugin['eClassApp'] ? 1 : 0), $sendTimeMode, $sendTimeString, $ContentType, $MergeFormat, $MergeType, $MergeFile, $NoticeID);
	echo str_replace("<!--___STUDENT_INPUT___-->", $STUDENT_INPUT, $formBody);
?>
</div>

<?=$linterface->MandatoryField();?>
<div class="edit_bottom_v30">
	<?
		$arrSignedCount = $lnotice->returnNoticeSignedCount($NoticeID);
		$SignedCount = $arrSignedCount[$NoticeID][0];
		// echo $linterface->GET_ACTION_BTN($submitBtnText, "submit", "return checkform(this.form);","submit2");
	    echo $linterface->GET_ACTION_BTN($submitBtnText, "button", "checkform(this.form);", "submit2")
	?>
	<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "setRemoveFile()") ?>
	<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='index.php'","cancelbtn") ?>
</div>	
	
<input type="hidden" name="NoticeID" value="<?=$NoticeID?>">
<input type="hidden" name="original_status" value="<?=$lnotice->RecordStatus?>">

<input type="hidden" name="pageNo" value="<?=$pageNo?>" />
<input type="hidden" name="field" value="<?=$field; ?>" />
<input type="hidden" name="page_size_change" value="<?=$page_size_change?>" />
<input type="hidden" name="numPerPage" value="<?=$numPerPage?>" />

<input type="hidden" name="noticeType" value="<?=$noticeType?>" />
<input type="hidden" name="status" value="<?=$status?>" />
<input type="hidden" name="year" value="<?=$year?>" />
<input type="hidden" name="month" value="<?=$month?>" />

<input type="hidden" name="deleted_files" value='' />
<input type="hidden" name='attachment_size' value='<? echo $no_file==""?1:$no_file;?>'>

<input type="hidden" name="originalMergeFile" value="<?=$MergeFile?>">

</form>

<script language="JavaScript1.2">
<? // if(!$isIssuedNotice) { ?>
selectAudience();
<? // } ?>

function js_show_email_notification(val)
{
	if(val == 1 || val == 4)
	{
        <?php if($plugin['eClassApp']) { ?>
			$('#pushmessagenotify').removeAttr('disabled');
			$('#pushmessagenotify_Students').removeAttr('disabled');
			$('#pushmessagenotify_PIC').removeAttr('disabled');
			$('#pushmessagenotify_ClassTeachers').removeAttr('disabled');
        <?php } ?>
		$('#emailnotify').removeAttr('disabled');
		$('#emailnotify_Students').removeAttr('disabled');
		$('#emailnotify_PIC').removeAttr('disabled');
		$('#emailnotify_ClassTeachers').removeAttr('disabled');
		
		//Villa 2016-10-18
        <?php if($plugin['eClassApp']) { ?>
			var parentCheck = document.getElementById("pushmessagenotify");
			var studnetCheck = document.getElementById("pushmessagenotify_Students");
			var picCheck = document.getElementById("pushmessagenotify_PIC");
			var classTeacherCheck = document.getElementById("pushmessagenotify_ClassTeachers");
			if((parentCheck && parentCheck.check==true) || (studnetCheck && studnetCheck.check==true) || (picCheck && picCheck.check==true) || (classTeacherCheck && classTeacherCheck.check==true)){
				$('div#sendTimeSettingDiv').show();
				$('div#PushMessageSetting').show();
			}
		<?php } ?>
	}
	else
	{
        <?php if($plugin['eClassApp']) { ?>
			$('#pushmessagenotify').removeAttr('checked');
			$('#pushmessagenotify_Students').removeAttr('checked');
			$('#pushmessagenotify_PIC').removeAttr('checked');
			$('#pushmessagenotify_ClassTeachers').removeAttr('checked');
			
			$('#pushmessagenotify').attr('disabled','true');
			$('#pushmessagenotify_Students').attr('disabled','true');
			$('#pushmessagenotify_PIC').attr('disabled','true');
			$('#pushmessagenotify_ClassTeachers').attr('disabled','true');
			
			$('div#sendTimeSettingDiv').hide();
			$('div#PushMessageSetting').hide();
        <?php } ?>

		$('#emailnotify').removeAttr('checked');
		$('#emailnotify_Students').removeAttr('checked');
		$('#emailnotify_PIC').removeAttr('checked');
		$('#emailnotify_ClassTeachers').removeAttr('checked');
		
		$('#emailnotify').attr('disabled','true');
		$('#emailnotify_Students').attr('disabled','true');
		$('#emailnotify_PIC').attr('disabled','true');
		$('#emailnotify_ClassTeachers').attr('disabled','true');
	}
}

<?php if($sys_custom['eNotice']['SpecialNoticeForPICAndAdmin']) { ?>
    function js_handle_special_notice_options_display()
    {
        var val = $('input[name=SpecialNotice]:checked').val();
        if(val == 1) {
            $('#emailnotify_ClassTeachers').removeAttr('checked');
            $('#emailnotify_ClassTeachers').attr('disabled','true');
            $('#span_Emailnotify_ClassTeachers').hide();

            <?php if($plugin['eClassApp']) { ?>
                $('#pushmessagenotify_ClassTeachers').removeAttr('checked');
                $('#pushmessagenotify_ClassTeachers').attr('disabled','true');
                $('#span_Pushmessagenotify_ClassTeachers').hide();
                checkedSendPushMessage($('#pushmessagenotify_ClassTeachers'));
            <?php } ?>
        }
        else {
            var sus_status = $('input[name=sus_status]:checked').val();
            if(sus_status == 1 || sus_status == 4) {
                $('#emailnotify_ClassTeachers').removeAttr('disabled');
            }
            $('#span_Emailnotify_ClassTeachers').show();

            <?php if($plugin['eClassApp']) { ?>
                if(sus_status == 1 || sus_status == 4) {
                    $('#pushmessagenotify_ClassTeachers').removeAttr('disabled');
                }
                $('#span_Pushmessagenotify_ClassTeachers').show();
            <?php } ?>
        }
    }
<?php } ?>

var eFormAppJS = {
		vars: {
			ajaxURL: "ajax_search_user_by_classname_classnumber.php",
			xhr: "",
			selectedUserContainer: "UserClassNameClassNumberSearchTb"
		},
		listener: {
			insertPICFromTextarea: function(e) {
				var textarea = $('div.pic_textarea textarea').eq(0);

				if (textarea.val() == "") {
					textarea.focus();
				} else {
					eFormAppJS.func.ajaxFromTextArea(textarea, "pic");
				}
			},
			insertFromTextarea: function(e) {
				var textarea = $('div.student_textarea textarea').eq(0);
				if (textarea.val() == "") {
					textarea.focus();
				} else {
					eFormAppJS.func.ajaxFromTextArea(textarea, "student");
				}
				e.preventDefault();
			}
		},
		func: {
			ajaxFromTextArea: function(textarea, contentType) {
				var targetSelectID = "target[]";
				var textareaMsgObj = '#textarea_studentmsg';
				switch (contentType) {
					case "pic":
						textareaMsgObj = '#textarea_msg';
						targetSelectID = "target_PIC";
						break;
				}
				if (textarea.val() == "") {
					textarea.focus();
					$(textareaMsgObj).css({"color":"#f00"}).html('<br>' + '<?php echo $Lang['eNotice']['WrongUserLogin']; ?>');
				}
				else {
					var xhr = eFormAppJS.vars.xhr;
					if (xhr != "") {
						xhr.abort();
					}

					var textareaVal = textarea.val();
					var getTargetListVal = "";
					if (contentType == "pic") {
						$('select#' + targetSelectID + ' option').each(function(ndex, obj) {
							if (getTargetListVal != "") {
								getTargetListVal += ",";
							}
							getTargetListVal += $(this).val();
						});
					}
					else {
						$('select[name="' + targetSelectID + '"] option').each(function(ndex, obj) {
							if (getTargetListVal != "") {
								getTargetListVal += ",";
							}
							getTargetListVal += $(this).val();
						});
					} 
					
					eFormAppJS.vars.xhr = $.ajax({
						url: eFormAppJS.vars.ajaxURL,
						type: 'post',
						data: { "q": textareaVal.split("\n").join("||"), "searchfrom":"textarea", "nt_userType" : contentType, "SelectedUserIDList" : getTargetListVal, "IncludeSuspenedAccount" : "1" },
						error: function() {
							textarea.focus();
						},
						success: function(data) {
							var lineRec = data.split("\n");
							if (lineRec.length > 0) {
								var UserSelected = "";
								if (contentType == "pic") {
									UserSelected = $('#' + targetSelectID);
								} else {
									UserSelected = $('select[name="' + targetSelectID + '"]').eq(0);
								}
								// console.log(UserSelected);

								var afterTextarea = "";
								$.each(lineRec, function(index, data) {
									if (data != "") {
										var tmpData = data.split("||");
										if (typeof tmpData[0] != "undefined" && typeof tmpData[1] != "undefined" && tmpData[1] != "NOT_FOUND") {  
											// Add_Selected_User(tmpData[1], tmpData[0], eFormAppJS.vars.selectedUserContainer);
											// UserSelected.options[UserSelected.length] = new Option(tmpData[0], tmpData[1]);
											UserSelected.append($("<option></option>")
								                    .attr("value", tmpData[1])
								                    .text(tmpData[0])); 
											
										} else {
											if (afterTextarea != "") {
												afterTextarea += "\n";
											}
											afterTextarea += tmpData[2];									
										}
									}
								});
								if (afterTextarea != "") {
									textarea.val(afterTextarea);
									$(textareaMsgObj).css({"color":"#f00"}).html('<br>' + '<?php echo $Lang['eNotice']['WrongUserLogin']; ?>');
								}
								else {
									textarea.val("");
									$(textareaMsgObj).html('');
								}
								checkOptionAll(document.getElementById('form1').elements[targetSelectID]);
							}
							else {
								textarea.focus();
								$(textareaMsgObj).css({"color":"#f00"}).html('<br>' + '<?php echo $Lang['eNotice']['WrongUserLogin']; ?>');
							}
						}
					});
				}
			},
			init: function() {
				$('.form_table_v30 tr td tr td').css({"border-bottom":"0px"});

				$('div.student_textarea input[type="button"]').unbind('click', eFormAppJS.listener.insertFromTextarea);
				$('div.pic_textarea input[type="button"]').unbind('click', eFormAppJS.listener.insertPICFromTextarea);
				
				$('div.student_textarea input[type="button"]').bind('click', eFormAppJS.listener.insertFromTextarea);
				$('div.pic_textarea input[type="button"]').bind('click', eFormAppJS.listener.insertPICFromTextarea);
			}
		}
	};
</script>

<div id="csvPreviewDiv" style="display: none;">
<iframe width="750px" height="450px" name="csvPreviewFrame" id="csvPreviewFrame" frameBorder="0" seamless='seamless'></iframe>
</div>

<?php
intranet_closedb();

print $linterface->FOCUS_ON_LOAD("form1.NoticeNumber");
$linterface->LAYOUT_STOP();
?>