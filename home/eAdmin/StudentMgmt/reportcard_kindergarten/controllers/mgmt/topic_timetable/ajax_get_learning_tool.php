<?php
// Using:  
/*
 * 	Date: 2019-09-24 (Philips)
 * 			Added Chapter Selection and seperate teaching tool checkbox into ajax_get_learning_tool2.php
 * 
 *  Date: 2018-03-22 (Bill)     [2018-0202-1046-39164]
 *          Added Class Zone Quota input fields
 * 	Date: 2017-12-22 (Bill)
 * 			Added Select All button
 */

### DATA Related START ##
$ZoneData = $indexVar["libreportcard"]->Get_Learning_Zone_Record($ZoneID);
// $ZoneCode = $ZoneData[0]['ZoneCode'];

// $searchByZoneCode = '';
// if($ZoneCode != '') {
//     $searchByZoneCode = mb_substr($ZoneCode, 0, 1, 'UTF-8');
// }
// $ToolData = $indexVar["libreportcard"]->Get_Equipment_Record($YearID, "", $ToolCatID, $searchByZoneCode);


$zoneQuotaSettings = str_replace("\\", "", $zoneQuotaData);
$zoneQuotaSettings = $indexVar["JSON"]->decode($zoneQuotaSettings);
### DATA Related END ##

$CategorySelection = $indexVar["libreportcard_ui"]->Get_Tool_Cat_Selection($ToolCatID, " onSelectCatInThickBox(\"$ZoneID\", this.value) ");
$ChapterSelection = $indexVar["libreportcard_ui"]->Get_Chapter_Selection($chapter, 'onSelectChapterInThickBox()', $YearID, $ZoneID);

// Get all Classes in Form
$allClassesInForm = $indexVar["libreportcard"]->GetAllClassesInTimeTable();
$allClassesInForm = BuildMultiKeyAssoc($allClassesInForm, array('YearID', 'YearClassID'));
$allClassesInForm = $allClassesInForm[$YearID];

$zoneQuota = $zoneQuotaSettings[$ZoneID][0];
$zoneQuota = $zoneQuota ? $zoneQuota : $ZoneData[0]["ZoneQuota"];
$QuotaInputField = "<input type='number' min='0' max='200' maxlength='4' class='textboxtext requiredField' name='Quota' id='Quota' value='".$zoneQuota."'>";

$display = "";
$display .= "<form id='form_learningTool' method='POST'>";
	$display .= "<div id='thickboxContentDiv' class='edit_pop_board_write'>";
		$display .= "<table class='form_table_v30 inside_form_table inside_form_table_v30'>";
			$display .= "<tbody>";
				$display .= "<tr>";
					$display .= "<td class='field_title'>";
						$display .= $Lang["eReportCardKG"]["Setting"]["LearningZone"]["Title"];
					$display .= "</td>";
					$display .= "<td>";
						$display .= Get_Lang_Selection($ZoneData[0]["ZoneNameB5"], $ZoneData[0]["ZoneNameEN"]);
					$display .= "</td>";
				$display .= "</tr>";
				$display .= "<tr>";
					$display .= "<td class='field_title'>";
						$display .= $Lang["eReportCardKG"]["Setting"]["ApplyForm"];
					$display .= "</td>";
					$display .= "<td>";
						$display .= $ZoneData[0]["YearName"];
					$display .= "</td>";
				$display .= "</tr>";
				$display .= "<tr>";
					$display .= "<td class='field_title'>";
						$display .= $Lang["eReportCardKG"]["Setting"]["ZoneQuota"];
					$display .= "</td>";
					$display .= "<td>";
						$display .= $QuotaInputField;
					$display .= "</td>";
				$display .= "</tr>";
				
				foreach((array)$allClassesInForm as $thisClassID => $thisClassInfo)
				{
				    $classZoneQuota = $zoneQuotaSettings[$ZoneID][$thisClassID];
				    $classZoneQuota = $classZoneQuota ? $classZoneQuota : $zoneQuota;
				    $QuotaInputField = "<input type='number' min='0' max='200' maxlength='4' class='textboxtext requiredField' name='ClassQuota[".$thisClassID."]' id='ClassQuota[".$thisClassID."]' value='".$classZoneQuota."'>";
				    
				    $display .= "<tr>";
				        $display .= "<td class='field_title'>";
				            $display .= $Lang["eReportCardKG"]["Setting"]["ZoneQuota"].' ('.$thisClassInfo['ClassName'].')';
    				    $display .= "</td>";
    				    $display .= "<td>";
    				        $display .= $QuotaInputField;
    				    $display .= "</td>";
				    $display .= "</tr>";
				}
				
// 				$display .= "<tr>";
// 					$display .= "<td class='field_title'>";
// 						$display .= $Lang["eReportCardKG"]["Setting"]["ToolCategory"];
// 					$display .= "</td>";
// 					$display .= "<td>";
// 						$display .= $CategorySelection;
// 					$display .= "</td>";
// 				$display .= "</tr>";
				$display .= "<tr>";
					$display .= "<td class='field_title'>";
						$display .= $Lang["eReportCardKG"]["Setting"]["TeachingTool"]["Title"];
					$display .= "</td>";
					$display .= "<td>" . $ChapterSelection ;
					$display .= "<div id='checkBoxArea'>";
						//$display .= $CheckBoxArr;
					$display .= "</div>";
					$display .= "</td>";
				$display .= "</tr>";
			$display .= "</tbody>";
		$display .= "</table>";
		$display .= "<input type='hidden' id='hiddenZone' name='hiddenZone' value='$ZoneID'>";
	$display .= "</div>";
$display .= "</form>";

$display .= "<div id='editBottomDiv' class='edit_bottom_v30'>";
	$display .= "<p class='spacer'></p>";
		$display .= "<input type='button' value='".$Lang["Btn"]["Submit"]."' class='formbutton_v30 print_hide' onclick='SaveTools()' id='submitBtn' name='submitBtn'>";
		$display .= "<input type='button' value='".$Lang["Btn"]["Back"]."' class='formbutton_v30 print_hide' onclick='CloseThickBox()' id='backBtn' name='backBtn'>";
	$display .= "<p class='spacer'></p>";
$display .= "</div>";

echo $display;
?>