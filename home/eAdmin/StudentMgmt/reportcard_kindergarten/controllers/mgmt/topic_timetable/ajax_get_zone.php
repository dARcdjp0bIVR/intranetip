<?php 
$ZoneData = $indexVar['libreportcard']->Get_Learning_Zone_Record('',$YearID);
$i = 0;
$count = sizeof($ZoneData);
$numberInRow = 5;
if(!$YearID){
	$CheckBoxArr = "No Result";
	$CheckBoxArr = $Lang['eReportCardKG']['Management']['TopicTimeTable']['PleaseEnterTopic'];
	echo $CheckBoxArr;
	return false;
}
if(!$ZoneData){
	$CheckBoxArr = "No Result";
	echo $CheckBoxArr;
	return false;
}
$CheckBoxArr = "<table class='form_table_v30 inside_form_table inside_form_table_v30'>";
foreach ($ZoneData as $_ZoneData){
	if($i%$numberInRow===0){
		$CheckBoxArr .= "<tr>";
	}
	$CheckBoxArr .= "<td>";
	$checked = '';
	$CheckBoxArr .= "<input type='checkbox' id='Zone_".$_ZoneData['ZoneID']."' name='ZoneID[]' value='".$_ZoneData['ZoneID']."' $checked>";
	$display = Get_Lang_Selection($_ZoneData['ZoneNameB5'], $_ZoneData['ZoneNameEN']);
	$CheckBoxArr .= "<a id='Zone_Click_".$_ZoneData['ZoneID']."' class='tablelink' href='javascript:void(0)' onClick='ZoneOnClick(".$_ZoneData['ZoneID'].")'>";
		$CheckBoxArr .= $display;
	$CheckBoxArr .= "</a>";
	$CheckBoxArr .= "</td>";
	if(($i%$numberInRow===0 && $count==1)|| $i+1==$count){
		$CheckBoxArr .= "</tr>";
	}
	$i++;
}
$CheckBoxArr .= "</table>";
echo $CheckBoxArr;
?>