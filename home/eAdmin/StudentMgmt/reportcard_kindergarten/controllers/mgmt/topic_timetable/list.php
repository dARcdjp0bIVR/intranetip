<?php 
// editing by 
/* 
 * Change Log:
 * Date 2019-10-31 Bill: class teacher > view own class level only
 * Date 2017-02-02 Villa: ADd Table
 * Date 2017-01-27 Villa: Open the File
 */

### Cookies setting
$arrCookies = array();
$arrCookies[] = array("eRCkg_Mgmt_topicTimetable_YearID","YearID");
if(isset($clearCoo) && $clearCoo == 1) {
	clearCookies($arrCookies);
} else {
	updateGetCookies($arrCookies);
}

if($success){
	$returnMsg = $Lang['General']['ReturnMessage']['UpdateSuccess'];
}else{
	$returnMsg = "";
}

### Page Title
$TAGS_OBJ[] = array($Lang['eReportCardKG']['Management']['TopicTimeTable']['Title']);
$indexVar['libreportcard_ui']->Echo_Module_Layout_Start($returnMsg);

$btnAry = array();
$btnAry[] = array('new', 'javascript: goNew();');
$htmlAry['contentTool'] = $indexVar['libreportcard_ui']->Get_Content_Tool_By_Array_v30($btnAry);

### DB table action buttons
$btnAry = array();
$btnAry[] = array('edit', 'javascript: Edit();');
$btnAry[] = array('delete', 'javascript: Delete();');
$htmlAry['dbTableActionBtn'] = $indexVar['libreportcard_ui']->Get_DBTable_Action_Button_IP25($btnAry);

## YearSelection Box
$YearSelection = $indexVar['libreportcard_ui']->Get_Year_Selection($YearID, $onChange="document.form1.submit()", $allKGOptions=false, $noFirstTitle=false, $firstTitleType=1);

## TimeTable Data
$filterByClassTeacher = !$indexVar["libreportcard"]->IS_KG_ADMIN_USER() && $indexVar["libreportcard"]->IS_KG_CLASS_TEACHER();
$temp = $indexVar['libreportcard']->GetTimeTable($YearID, "", false, "", $filterByClassTeacher);
if($temp){
	foreach ($temp as $_temp){
		$TimeTableData[$_temp['TopicTimeTableID']][] = $_temp;
	}
	foreach ($TimeTableData as $key=>$_TimeTableData){
		$zoneKey = Get_Array_By_Key($_TimeTableData, 'ZoneID');
// 		debug_pr($zoneKey);
		$zoneKey = array_unique($zoneKey);
		$TimeTableData[$key]['numberOfZone'] = ($zoneKey[0])?count($zoneKey):'0'; //avoid empty show 1
		$toolKey =  Get_Array_By_Key($_TimeTableData, 'ToopCodeID');
		$TimeTableData[$key]['numberOfTool'] = count($toolKey);
	}
}

$i = 1;
if(empty($TimeTableData)){
	$TimeTable_Table = '<table class="common_table_list_v30" id="ContentTable" >';
		$TimeTable_Table .= '<thead>';
			$TimeTable_Table .= '<tr>';
				$TimeTable_Table .= '<th style="width:3%;">';
					$TimeTable_Table .= '#';
				$TimeTable_Table .= '</th>';
			
				$TimeTable_Table .= '<th  style="width:10%;">';
					$TimeTable_Table .= $Lang['eReportCardKG']['Setting']['Code'];
				$TimeTable_Table .= '</th>';
				
				$TimeTable_Table .= '<th  style="width:10%;">';
					$TimeTable_Table .= $Lang['eReportCardKG']['Setting']['Name'];
				$TimeTable_Table .= '</th>';
				
				$TimeTable_Table .= '<th  style="width:10%;">';
					$TimeTable_Table .= $Lang['eReportCardKG']['Setting']['Year'];
				$TimeTable_Table .= '</th>';
				
				$TimeTable_Table .= '<th  style="width:15%;">';
					$TimeTable_Table .= $Lang['eReportCardKG']['Management']['TopicTimeTable']['StartDate'] ;
				$TimeTable_Table .= '</th>';
				
				$TimeTable_Table .= '<th  style="width:15%;">';
					$TimeTable_Table .= $Lang['eReportCardKG']['Management']['TopicTimeTable']['EndDate'] ;
				$TimeTable_Table .= '</th>';
				
				$TimeTable_Table .= '<th  style="width:10%;">';
					$TimeTable_Table .= $Lang['eReportCardKG']['Setting']['FormTopic']['Title'] ;
				$TimeTable_Table .= '</th>';
				
				$TimeTable_Table .= '<th  style="width:10%;">';
					$TimeTable_Table .= $Lang['eReportCardKG']['Management']['TopicTimeTable']['NumOfZones'] ;
				$TimeTable_Table .= '</th>';
				
				$TimeTable_Table .= '<th  style="width:3%;">';
					$TimeTable_Table .= '<input type="checkbox" id="checkmaster" name="checkmaster" onClick="CheckAll()">';
				$TimeTable_Table .= '</th>';
			
			$TimeTable_Table .= '</tr>';
		
		$TimeTable_Table .= '</thead>';
		
		$TimeTable_Table .= '<tbody>';
		$TimeTable_Table .= '<td align="center" colspan="9">';
			$TimeTable_Table .= $Lang['eReportCardKG']['General']['NoRecord'] ;
		$TimeTable_Table .= '</td>';
		
		$TimeTable_Table .= '</tbody>';
	$TimeTable_Table .= '</table>';
}else{
	$TimeTable_Table = '';
	$TimeTable_Table .= '<table class="common_table_list_v30" id="ContentTable" >';
		$TimeTable_Table .= '<thead>';

			$TimeTable_Table .= '<tr>';

				$TimeTable_Table .= '<th style="width:3%;">';
					$TimeTable_Table .= '#';
				$TimeTable_Table .= '</th>';
			
				$TimeTable_Table .= '<th  style="width:10%;">';
					$TimeTable_Table .= $Lang['eReportCardKG']['Setting']['Code'];
				$TimeTable_Table .= '</th>';
				
				$TimeTable_Table .= '<th  style="width:10%;">';
					$TimeTable_Table .= $Lang['eReportCardKG']['Setting']['Name'];
				$TimeTable_Table .= '</th>';
				
				$TimeTable_Table .= '<th  style="width:10%;">';
					$TimeTable_Table .= $Lang['eReportCardKG']['Setting']['Year'];
				$TimeTable_Table .= '</th>';
				
				$TimeTable_Table .= '<th  style="width:15%;">';
					$TimeTable_Table .= $Lang['eReportCardKG']['Management']['TopicTimeTable']['StartDate'] ;
				$TimeTable_Table .= '</th>';
				
				$TimeTable_Table .= '<th  style="width:15%;">';
					$TimeTable_Table .= $Lang['eReportCardKG']['Management']['TopicTimeTable']['EndDate'] ;
				$TimeTable_Table .= '</th>';
				
				$TimeTable_Table .= '<th  style="width:10%;">';
					$TimeTable_Table .= $Lang['eReportCardKG']['Setting']['FormTopic']['Title'] ;
				$TimeTable_Table .= '</th>';
				
				$TimeTable_Table .= '<th  style="width:10%;">';
					$TimeTable_Table .= $Lang['eReportCardKG']['Management']['TopicTimeTable']['NumOfZones'] ;
				$TimeTable_Table .= '</th>';
				
				$TimeTable_Table .= '<th  style="width:3%;">';
					$TimeTable_Table .= '<input type="checkbox" id="checkmaster" name="checkmaster" onClick="CheckAll()">';
				$TimeTable_Table .= '</th>';
			
			$TimeTable_Table .= '</tr>';
		
		$TimeTable_Table .= '</thead>';
		
		$TimeTable_Table .= '<tbody>';
		foreach ($TimeTableData as $_TimeTableData){
			$TimeTable_Table .= '<tr>';
			
				$TimeTable_Table .= '<td>';
					$TimeTable_Table .=  $i;
				$TimeTable_Table .= '</td>';
				
				$TimeTable_Table .= '<td>';
					$TimeTable_Table .=  $_TimeTableData[0]['TimeTableCode'];
				$TimeTable_Table .= '</td>';
				
				$TimeTable_Table .= '<td>';
					$TimeTable_Table .= '<a class="tablelink" href="javascript:goEdit('.$_TimeTableData[0]['TopicTimeTableID'].');">';
						$TimeTable_Table .=  Get_Lang_Selection($_TimeTableData[0]['CH_Name'], $_TimeTableData[0]['EN_Name']);
					$TimeTable_Table .= '</a>';
				$TimeTable_Table .= '</td>';
				
				$TimeTable_Table .= '<td>';
					$TimeTable_Table .=  $_TimeTableData[0]['YearName'];
				$TimeTable_Table .= '</td>';
				
				$TimeTable_Table .= '<td>';
					$TimeTable_Table .=  $_TimeTableData[0]['StartDate'];
				$TimeTable_Table .= '</td>';
				
				$TimeTable_Table .= '<td>';
					$TimeTable_Table .=  $_TimeTableData[0]['EndDate'];
				$TimeTable_Table .= '</td>';
				
				$TimeTable_Table .= '<td>';
					$TimeTable_Table .=  Get_Lang_Selection($_TimeTableData[0]['TopicNameB5'], $_TimeTableData[0]['TopicNameEN']);
				$TimeTable_Table .= '</td>';
				
				$TimeTable_Table .= '<td>';
					if($_TimeTableData['numberOfZone']){ //only allow edit if zone is set => prevent bug in count page
						$TimeTable_Table .=  "<a class='tablelink' href='index.php?task=mgmt.topic_timetable.zone_tool_view&timetableID=".$_TimeTableData[0]['TopicTimeTableID']."'>".$_TimeTableData['numberOfZone']."</a>";
					}else{
						$TimeTable_Table .= $_TimeTableData['numberOfZone'];
					}
				$TimeTable_Table .= '</td>';
				
				$TimeTable_Table .= '<td>';
					$TimeTable_Table .= "<input type='checkbox' class='checkbox' id='timeTable_checkbox_".$_TimeTableData[0]['TopicTimeTableID']."' name='timeTable_checkbox[]' value='".$_TimeTableData[0]['TopicTimeTableID']."'>";
				$TimeTable_Table .= '</td>';
			
			$TimeTable_Table .= '</tr>';
			$i++;
		}
		$TimeTable_Table .= '</tbody>';
	
	$TimeTable_Table .= '</table>';
}
?>