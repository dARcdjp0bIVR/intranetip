<?php 
/*
 * Change Log:
 * Date 2018-03-22 Bill:  Support Class Zone Quota Settings [2018-0202-1046-39164]
 * Date 2017-11-17 Bill:  support Quota Settings in Zone
 * Date 2017-02-03 Villa: Open the file
 */
 
### Page Title
if($success){
	$returnMsg = $Lang['General']['ReturnMessage']['UpdateSuccess'];
}
else{
	$returnMsg = "";
}

$TAGS_OBJ[] = array($Lang['eReportCardKG']['Management']['TopicTimeTable']['Title']);
$indexVar['libreportcard_ui']->Echo_Module_Layout_Start($returnMsg);
echo $indexVar['libreportcard_ui']->Include_Thickbox_JS_CSS();

//$sortByZone = true;
$temp = $indexVar['libreportcard']->GetTimeTable('', $timetableID);
foreach ($temp as $_temp)
{
	$Data[$_temp['ZoneID']]['ZoneID'] 	= $_temp['ZoneID'];
	$Data[$_temp['ZoneID']]['ZoneCode'] = $_temp['ZoneCode'];
	$Data[$_temp['ZoneID']]['B5_Name']  = $_temp['ZoneNameB5'];
	$Data[$_temp['ZoneID']]['EN_Name']  = $_temp['ZoneNameEN'];
	$Data[$_temp['ZoneID']]['LearningTool'][] = $_temp['ToolCodeID'];
	$zone_quota[$_temp['ZoneID']] = $_temp['ZoneQuota'];
	
	$YearID = $_temp['YearID'];
	$TopicID = $_temp['TopicID'];
}

// Class Zone Quota Mapping
$class_zone_quota = $indexVar["libreportcard"]->GetClassZoneQuotaFromMapping($timetableID);
$class_zone_quota = BuildMultiKeyAssoc((array)$class_zone_quota, array('ZoneID', 'YearClassID'), "ClassZoneQuota", 1);

$temp2 = $indexVar['libreportcard']->Get_Equipment_Record($YearID);
$ALLTools = BuildMultiKeyAssoc($temp2, 'CodeID');

$navigation = Get_Lang_Selection($temp[0]['CH_Name'], $temp[0]['EN_Name']);
$navigationAry[] = array($Lang['eReportCardKG']['Management']['TopicTimeTable']['Title'],'/home/eAdmin/StudentMgmt/reportcard_kindergarten/index.php?task=mgmt.topic_timetable.list');
$navigationAry[] = array($navigation);
$NivagationBar = $indexVar['libreportcard_ui']->GET_NAVIGATION_IP25($navigationAry);

$table = "";
foreach ($Data as $_zone)
{
	// Edit button
	$btnAry = array();
	$btnAry[] = array('edit', 'javascript: GoEdit('.$_zone['ZoneID'].');');
	$htmlAry['contentTool'] = $indexVar['libreportcard_ui']->Get_Content_Tool_By_Array_v30($btnAry);
	
	$table .= "<table class='common_table_list_v30' id='ContentTable'>";
		$table .= "<thead>";
			$table .= "<th colspan='2'>";
				$table .= "<div style='float:left'>";
				$table .= Get_Lang_Selection($_zone['B5_Name'], $_zone['EN_Name'])." (".$_zone['ZoneCode'].")"."&nbsp;";
				$table .= "</div>";
				$table .= $htmlAry['contentTool'];
			$table .= "</th>";
		$table .= "</thead>";
		
        $table .= "<tbody>";
	foreach ($_zone['LearningTool'] as $LearningTool)
	{
		if($LearningTool)
		{
			$table .= "<tr>";
				$table .= "<td>";
					$table .= "<table class='inside_form_table inside_form_table_v30'>";
						$table .= "<tr>";
							$table .= "<td width='80%'>";
							$table .= Get_Lang_Selection($ALLTools[$LearningTool]['CH_Name'], $ALLTools[$LearningTool]['EN_Name'])." (".$ALLTools[$LearningTool]['Code'].")";
							$table .= "</td>";
							$table .= "<td style='width:300px; height:225px;'>";
								if($ALLTools[$LearningTool]['PhotoPath']){
									$table .= "<img src='".$indexVar['thisImage'].$ALLTools[$LearningTool]['PhotoPath']."' style='width:300px;'";
								}else{
									$table .= "";
								}
							$table .= "</td>";
						$table .= "</tr>";
					$table .= "</table>";
				$table .= "</td>";
				$table .= "<td>";
					$table .= "<span class='table_row_tool row_content_tool'>";				
						$table .= "<a  class='delete' href='javascript:void(0)' onClick='GoDelete(".$LearningTool.",".$_zone['ZoneID'].")'>";
//	 						$table .= "X";
						$table .= "</a>";
					$table .= "</span>";
				$table .= "</td>";
			$table .= "</tr>";
		}
		else
		{
			$table .= "<tr>";
				$table .= "<td colspan='2' align='center'>";
					$table .= $Lang['eReportCardKG']['Management']['TopicTimeTable']['NoLearningTool'];
				$table .= "</td>";
			$table .= "</tr>";
		}
	}
	
	$table .= "</tbody>";
	$table .= "</table>";
	$table .= "<div style='height:200px;'></div>";
}
?>