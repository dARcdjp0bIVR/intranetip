<?php
// Using: 
/*
 *  Date: 2018-05-02 (Bill)
 *          Added Warning Box
 *  
 *  Date: 2018-03-22 (Bill)     [2018-0202-1046-39164]
 *          Support Class Zone Quota Settings
 *  
 * 	Date: 2017-12-20 (Bill)
 * 			Set Default End Time to "23:59:59"
 */
 
$TAGS_OBJ[] = array($Lang["eReportCardKG"]["Management"]["TopicTimeTable"]["Title"]);
$indexVar["libreportcard_ui"]->Echo_Module_Layout_Start($indexVar["returnMsgLang"]);

$navigation = $Lang["Btn"]["New"];
if($isEdit)
{
	// Build TimeTable Mapping 
	$temp = $indexVar["libreportcard"]->GetTimeTable("", $TimeTableID);
	foreach($temp as $_temp)
	{
		$teaching_tool[$_temp["ZoneID"]][] = $_temp["ToolCodeID"];
		$zone_quota[$_temp["ZoneID"]] = $_temp["ZoneQuota"];
		$zone[] = $_temp["ZoneID"];
	}
	
	// Class Zone Quota Mapping
	$class_zone_quota = $indexVar["libreportcard"]->GetClassZoneQuotaFromMapping($TimeTableID);
	$class_zone_quota = BuildMultiKeyAssoc((array)$class_zone_quota, array('ZoneID', 'YearClassID'), "ClassZoneQuota", 1);
	
	// TimeTable Info
	$TopicID = $temp[0]["TopicID"];
	$TimeTableID = $TimeTableID;
	$Code = $temp[0]["TimeTableCode"];
	$EN_Name = $temp[0]["EN_Name"];
	$CH_Name = $temp[0]["CH_Name"];
	$yearID = $temp[0]["YearID"];
	
	// TimeTable Start Date
	$StartDate = $temp[0]["StartDate"];
	if(empty($StartDate)) {
		$StartDate = date("Y-m-d")." 00:00:00";
	}
	$StartDate_Date = date("Y-m-d", strtotime($StartDate));
	$StartDate_Hour = date("H", strtotime($StartDate));
	$StartDate_Minutes = date("i", strtotime($StartDate));
	$StartDate_Second = date("s", strtotime($StartDate));
	
	// TimeTable End Date
	$EndDate = $temp[0]["EndDate"];
	if(empty($EndDate)) {
		$EndDate = date("Y-m-d")." 23:59:59";
	}
	$EndDate_Date = date("Y-m-d", strtotime($EndDate));
	$EndDate_Hour = date("H", strtotime($EndDate));
	$EndDate_Minutes = date("i", strtotime($EndDate));
	$EndDate_Second = date("s", strtotime($EndDate));
	
	$zoneIDArr = array_unique($zone);
	$navigation = $Lang["Btn"]["Edit"];
}
else
{
	$EndDate_Hour = "23";
	$EndDate_Minutes = "59";
	$EndDate_Second = "59";
}

### Loading Image [Start]
$LoadingImage = "";
if($isEdit) {
	$LoadingImage = "<img class='LoadingImg' src='/images/$LAYOUT_SKIN/indicator.gif'>";
}
### Loading Image [End]

# Year Selection
$YearSelection = $indexVar["libreportcard_ui"]->Get_Year_Selection($yearID, "YearOnChange()");

# Start Date
$DateStart = $indexVar["libreportcard_ui"]->GET_DATE_PICKER("DateStart")." ".$indexVar["libreportcard_ui"]->Get_Time_Selection("DateStart");
$DateStart .= $indexVar["libreportcard_ui"]->Get_Form_Warning_Msg("marksheetSubmission_inputDateWarnDiv", $Lang["General"]["WarningArr"]["InputDate"], "warningMsgDiv", $display=false);
$DateStart .= $indexVar["libreportcard_ui"]->Get_Form_Warning_Msg("marksheetSubmission_endDateEarlierWarnDiv", $Lang["General"]["WarningArr"]["EndDateCannotEarlierThanStartDate"], "warningMsgDiv", $display=false);

# End Date
$DateEnd = $indexVar["libreportcard_ui"]->GET_DATE_PICKER("DateEnd")." ".$indexVar["libreportcard_ui"]->Get_Time_Selection("DateEnd");
$DateEnd .= $indexVar["libreportcard_ui"]->Get_Form_Warning_Msg("marksheetSubmission_inputDateWarnDiv", $Lang["General"]["WarningArr"]["InputDate"], "warningMsgDiv", $display=false);
$DateEnd .= $indexVar["libreportcard_ui"]->Get_Form_Warning_Msg("marksheetSubmission_endDateEarlierWarnDiv", $Lang["General"]["WarningArr"]["EndDateCannotEarlierThanStartDate"], "warningMsgDiv", $display=false);

# Navigation
$navigationAry[] = array($Lang["eReportCardKG"]["Management"]["TopicTimeTable"]["Title"], "/home/eAdmin/StudentMgmt/reportcard_kindergarten/index.php?task=mgmt.topic_timetable.list");
$navigationAry[] = array($navigation);
$NivagationBar = $indexVar["libreportcard_ui"]->GET_NAVIGATION_IP25($navigationAry);

### Warning box
$WarningBox = $indexVar["libreportcard_ui"]->Get_Warning_Message_Box('<font color="red">'.$Lang['General']['Tips'].'</font>', $Lang['eReportCardKG']['Management']['TopicTimeTable']['PleaseConfirmChanges']);
$WarningBox_Static = $indexVar["libreportcard_ui"]->Get_Warning_Message_Box('<font color="red">'.$Lang['General']['Tips'].'</font>', $Lang['eReportCardKG']['General']['PleaseConfirmChanges']);

### Required JS CSS - ThickBox
echo $indexVar["libreportcard_ui"]->Include_Thickbox_JS_CSS();
?>