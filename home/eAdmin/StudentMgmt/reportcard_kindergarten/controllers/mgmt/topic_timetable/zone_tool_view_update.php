<?php
/*
 * Change Log:
 * Date 2018-03-22 Bill:  Support Class Zone Quota Settings [2018-0202-1046-39164]
 * Date 2017-11-17 Bill:  Support Quota update
 * Date 2017-10-18 Bill:  Support select tools using Category filtering
 * Date 2017-02-03 Villa: Open the file
 */
 
// Delete
if($isDelete)
{
	if($TimeTableID && $ZoneID && $LearningToolID) {
		$indexVar['libreportcard']->DeleteTimeToolMapping($TimeTableID, $ZoneID, $LearningToolID);
	}
}
// Edit
else
{
	if($TimeTableID)
	{
		// Speical Handling when user select tools using Category filtering
		$ZoneID = $hiddenZone;
		if($ToolCatID)
		{
			// Get all previous selected tools
			$ZoneToolList = array();
			$selectedZoneToolList = $indexVar['libreportcard']->GetTimeTable('', $TimeTableID);
			foreach ($selectedZoneToolList as $CurrentZoneTool)
			{
				if($CurrentZoneTool['ToolCodeID'] > 0 && $CurrentZoneTool['ZoneID'] == $ZoneID) {
					$ZoneToolList[] = $CurrentZoneTool['ToolCodeID'];
				}
			}
			
			// Exclude all tools in selected category
			$selectedCatToolList = $indexVar['libreportcard']->Get_Equipment_Record($YearID, "", $ToolCatID);
			$selectedCatToolList = Get_Array_By_Key($selectedCatToolList, "CodeID");
			$ZoneToolList = array_diff($ZoneToolList, $selectedCatToolList);
			$toolData_Timetable[$ZoneID] = $ZoneToolList;
		}
		
		// Assign selected categories
		foreach ((array)$ToolID as $_ToolID){
			$toolData_Timetable[$ZoneID][] = $_ToolID;
		}
		if(!empty($toolData_Timetable[$ZoneID])) {
			$toolData_Timetable[$ZoneID] = array_unique($toolData_Timetable[$ZoneID]);
			sort($toolData_Timetable[$ZoneID]);	
		}
		
		$ClassQuota[0] = $Quota;
		$indexVar['libreportcard']->UpdateTimeZoneQuota($TimeTableID, $TopicID, $ZoneID, $ClassQuota);
		$indexVar['libreportcard']->UpdateTimeToolMapping($TimeTableID, $TopicID, $ZoneID, $toolData_Timetable, 0);
	}
}

header("Location: /home/eAdmin/StudentMgmt/reportcard_kindergarten/index.php?task=mgmt.topic_timetable.zone_tool_view&timetableID=$TimeTableID&success=1");
?>