<?php
$TopicData = $indexVar['libreportcard']->Get_Form_Topic_Record('',$YearID);
$topicSelection = "<select id='topic' name='topic' OnChange='TopicOnChange()'>";
$topicSelection .= "<option value>";
$topicSelection .= $Lang['General']['PleaseSelect'];
$topicSelection .= "</option>";
foreach ($TopicData as $_TopicData){
	$selected = '';
	$topicSelection .= "<option value='".$_TopicData['TopicID']."' $selected>";
	$topicSelection .= Get_Lang_Selection($_TopicData['TopicNameB5'], $_TopicData['TopicNameEN']);
	$topicSelection .= "</option>";
}
$topicSelection .= "</select>";

echo $topicSelection;
?> 