<?php
// editing by Philips
/*
 * Change Log:
 * 
 */

/*
 * $IndexID, $IndexRemark, $YearID, $TermID
 */
if(!isset($IndexID) || !isset($IndexRemark)) {
    header("Location: index.php?task=mgmt.ability_grade_remark.list");
}
else {
    if($isResetRemark) {
        $IndexRemark = '';
    }
    $result = $indexVar['libreportcard']->updateAbilityRemarkCustom($IndexID, $IndexRemark, $YearID, $TermID);
    $result = $result? "1" : "0";
    
    header("Location: index.php?task=mgmt.ability_grade_remark.list&success=$result&Year=$YearID&Term=$TermID");
}
?>