<?php
// editing by Philips
/*
 * 	Date: 2019-10-11 Philips
 * 			- Added general warning box
 * 
 * Change Log:
 * 
 */

# Page Title
$TAGS_OBJ[] = array($Lang['eReportCardKG']['Setting']['TaiwanAbilityIndex']['Title']);
$indexVar['libreportcard_ui']->Echo_Module_Layout_Start($indexVar['returnMsgLang']);

# Get Category / Index Info
$sql = "SELECT
            raic.Name, ragr.Grade, ragr.LowerLimit, ragr.UpperLimit, gr.Remarks as Remarks,
            IF(agra.Remarks IS NULL, '--', agra.Remarks) as RemarksAll,
            IF(agrc.Remarks IS NULL,
                IF(agra.Remarks IS NULL, gr.Remarks, agra.Remarks),
                agrc.Remarks)
            as RemarksCustom,
            agrc.CustomRemarkID
        FROM
            ".$indexVar['thisDbName'].".RC_ABILITY_GRADE_REMARKS as gr
            INNER JOIN ".$indexVar['thisDbName'].".RC_ABILITY_INDEX_CATEGORY as raic ON raic.CatID = gr.CatID
            INNER JOIN ".$indexVar['thisDbName'].".RC_ABILITY_GRADING_RANGE as ragr ON ragr.GradingRangeID = gr.GradingRangeID
            LEFT OUTER JOIN ".$indexVar['thisDbName'].".RC_ABILITY_GRADE_REMARKS_ALL as agra ON agra.AbilityRemarkID = gr.AbilityRemarkID
            LEFT OUTER JOIN ( SELECT * FROM ".$indexVar['thisDbName'].".RC_ABILITY_GRADE_REMARKS_CUSTOM WHERE YearID = '".$_GET['YearID']."' AND YearTermID = '".$_GET['TermID']."') as agrc ON agrc.AbilityRemarkID = gr.AbilityRemarkID
        WHERE
            gr.AbilityRemarkID = '".$_GET["CodeID"]."'";
$result = $indexVar['libreportcard']->returnArray($sql);

list($IndexCat, $IndexGrade, $IndexLowerLimit, $IndexUpperLimit, $IndexRemarks, $IndexRemarksAll, $IndexRemarkCustom, $CustomIndexID) = $result[0];
$IndexID = $_GET["CodeID"];
$IndexTermID = $_GET['TermID'];
$IndexYearID = $_GET['YearID'];
$IndexYearName = $indexVar['libreportcard']->getYearName($IndexYearID);

$YearTermAry = getSemesters($indexVar["libreportcard"]->Get_Active_AcademicYearID());
$IndexTermName = $YearTermAry[$IndexTermID];

# NavigationBar
$navigationAry[] = array($Lang['eReportCardKG']['Setting']['TaiwanAbilityIndex']['Title'], 'index.php?task=settings.taiwan_ability_index.list');
$navigationAry[] = array($Lang['Btn']['Edit']);
$NavigationBar = $indexVar['libreportcard_ui']->GET_NAVIGATION_IP25($navigationAry);

### Warning box
$WarningBox = $indexVar["libreportcard_ui"]->Get_Warning_Message_Box('<font color="red">'.$Lang['General']['Tips'].'</font>', $Lang['eReportCardKG']['General']['PleaseConfirmChanges']);
?>