<?php
// Using:
/*
 * 	Date: 2019-10-11 Philips
 * 			- Added general warning box
 * 
 *  Date: 2018-08-14 (Bill)
 *          Support multiple upload type
 *          
 * 	Date: 2018-03-14 (Bill)     [2018-0202-1046-39164]
 *          Create File (Copy logic from eReportCard)
 */

### Page Title
$TAGS_OBJ = $indexVar['libreportcard']->getOtherInfoTabObjArr($UploadType);
$indexVar['libreportcard_ui']->Echo_Module_Layout_Start();

// $btnAry = array();
// $btnAry[] = array('import', 'javascript: goImport();');
// $btnAry[] = array('export', 'javascript: goExport();');
// $htmlAry['contentTool'] = $indexVar['libreportcard_ui']->Get_Content_Tool_By_Array_v30($btnAry);

$isInputLocked = $indexVar['libreportcard']->isInputScorePeriod($_POST['TermID']) ? false : true;

# Semester Selection
$htmlAry['TermSelection'] = $indexVar['libreportcard_ui']->Get_Semester_Selection($_POST['TermID'], $Onchange='reloadOtherInfoTableContent()', $allYearOption=false, $noFirstOption=true);

# Class Selection
$ClassID = is_array($_POST['ClassID']) ? $_POST['ClassID'][0] : $_POST['ClassID'];
$htmlAry['ClassSelection'] = $indexVar['libreportcard_ui']->Get_Class_Selection($ClassID, $Onchange='reloadOtherInfoTableContent()', $withYearOptGroup=false, $noFirstOtpion=true, $_POST['YearID']);

# Data Table
$htmlAry['TableContent'] = $indexVar['libreportcard_ui']->Get_Other_Info_Data_Table($_POST['UploadType'], $_POST['TermID'], $ClassID, $isInputLocked);

# Button
$htmlAry['button'] = '';
if(!$isInputLocked && $_POST['UploadType'] != '' && $_POST['TermID'] && $ClassID)
{
    $htmlAry['button'] .= $indexVar['libreportcard_ui']->GET_ACTION_BTN($Lang['Btn']['Submit'], 'button', 'formSubmit()', 'submitBtn');
    $htmlAry['button'] .= '&nbsp;&nbsp;';
}
$htmlAry['button'] .= $indexVar['libreportcard_ui']->GET_ACTION_BTN($Lang['Btn']['Cancel'], 'button', 'goBack()', 'backBtn');

# Hidden Input Fields
$htmlAry['hiddenInputField'] = '';
$htmlAry['hiddenInputField'] .= "<input type='hidden' name='YearID' id='YearID' value='".$_POST['YearID']."'>\r\n";
$htmlAry['hiddenInputField'] .= "<input type='hidden' name='UploadType' id='UploadType' value='".$_POST['UploadType']."'>\r\n";

### Required JS CSS - Paste Score from Excel
echo $indexVar["libreportcard_ui"]->Include_CopyPaste_JS_CSS("2016");
echo $indexVar["libreportcard_ui"]->Include_Excel_JS_CSS();

### Warning box
$WarningBox = $indexVar["libreportcard_ui"]->Get_Warning_Message_Box('<font color="red">'.$Lang['General']['Tips'].'</font>', $Lang['eReportCardKG']['General']['PleaseConfirmScoreUpdate']);
?>