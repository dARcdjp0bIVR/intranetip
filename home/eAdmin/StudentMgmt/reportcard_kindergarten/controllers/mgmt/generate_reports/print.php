<?php
// Using:
/*
 *  Date: 2019-11-01 (Bill)
 *          show all topics
 *
 *  Date: 2018-12-11 (Bill)
 *          support parent access
 *  
 * 	Date: 2018-08-14 (Bill)
 *          added Award display
 *
 * 	Date: 2018-03-14 (Bill)     [2018-0202-1046-39164]
 *          Support Other Info data display
 */

@SET_TIME_LIMIT(1200);
@ini_set(memory_limit, "800M");

if(isset($_POST["targetTermId"]) && isset($_POST["targetClassIdAry"]) && isset($_POST["targetStuIdAry"]) && isset($_POST["svg"]))
{
    # Create mPDF object
    include_once($indexVar["thisBasePath"]."includes/mpdf/mpdf.php");
    $mpdf = new mPDF($mode="zh", $format="A4", $default_font_size=0, $default_font="", $mgl=0, $mgr=0, $mgt=0, $mgb=0, $mgh=5, $mgf=22.5, $orientation="P");
    
    # Set mPDF Settings
    $mpdf->allow_charset_conversion = true;
    $mpdf->charset_in = "UTF-8";
    $mpdf->list_auto_mode = "mpdf";
    //	$mpdf->setAutoTopMargin = 'stretch';
    //	$mpdf->splitTableBorderWidth = 0.1; 		// split 1 table into 2 pages add border at the bottom
    
    // Form Level Info
    $allClassInfoAry = $indexVar["libreportcard"]->Get_All_KG_Class();
    $allClassInfoAry = BuildMultiKeyAssoc($allClassInfoAry, "YearClassID", "YearID", 1);
    $targetClassIdAry = $indexVar["JSON"]->decode($_POST["targetClassIdAry"]);
    $targetClassId = $targetClassIdAry[0];
    $targetLevelId = $allClassInfoAry[$targetClassId];
    
    // Semester Info
    $thisYearId = $indexVar["libreportcard"]->Get_Active_AcademicYearID();
    $SemesterNumber = $indexVar["libreportcard"]->Get_Semester_Seq_Number($_POST["targetTermId"]);
    $yearSemInfo = getTargetYearSemesterByIDs($thisYearId, $_POST["targetTermId"], "b5");
    $yearSemInfo['YearID'] = $thisYearId;
    $yearSemInfo['TermID'] = $_POST["targetTermId"];
    
    // Student Info
    $studentIdAry = $_POST["targetStuIdAry"];
    $studentIdAry = $indexVar["JSON"]->decode($studentIdAry);
    $studentInfoAry = $indexVar["libreportcard"]->Get_Student_By_Class($targetClassId, $studentIdAry, 0, 0, 0, 1);
    $studentIdSize = count((array)$studentIdAry);
    
    // Clas Teacher Name
    if($studentIdSize > 0)
    {
        include_once($PATH_WRT_ROOT."includes/libclass.php");
        $lclass = new libclass();
        
        $firstStudentInfo = reset($studentInfoAry);
        $thisClassName = $firstStudentInfo['ClassTitleEn'];
        $ClassTeacherAry = $lclass->returnClassTeacher($thisClassName, $indexVar["libreportcard"]->AcademicYearID);
        if(is_array($ClassTeacherAry) && count($ClassTeacherAry) > 0) {
            $ClassTeacherAry = Get_Array_By_Key($ClassTeacherAry, "ChineseName");
            $ClassTeacher = implode(",&nbsp;", (array)$ClassTeacherAry);
        } else {
            $ClassTeacher = $indexVar['emptySymbol'];
        }
    }
    
    // Student Highcharts Images
    $svgData = array();
    $svgList = $_POST["svg"];
    $svgList = $indexVar["JSON"]->decode($svgList);
    foreach((array)$svgList as $thisSvg)
    {
        //$svgData[$thisSvg->id] = str_replace ('"', '\'', $thisSvg->thisSvg);
        $thisStuId = str_replace("container_", "", $thisSvg["id"]);
        $svgData[$thisStuId] = $thisSvg["thisSvg"];
    }
    
    // Student Topics
    $topicsAry = $indexVar["libreportcard"]->getTopics("", "", $targetLevelId, true, $_POST["targetTermId"], false, true);
    foreach((array)$topicsAry as $thisTopicId => $thisTopicInfo)
    {
        $topicBypeLen = strlen($thisTopicInfo["NameCh"]);		// byte count
        $topicWordLen = mb_strlen($thisTopicInfo["NameCh"]);	// word count
        $topicLenDiff = (($topicWordLen * 3) - $topicBypeLen) / 2;
        
        $topicDisplayLen = 0;
        if($topicBypeLen == $topicWordLen) {					// English only
            $topicDisplayLen = $topicBypeLen;
        }
        else if (($topicWordLen * 3) == $topicBypeLen) {		// Chinese only
            $topicDisplayLen = $topicWordLen * 1.84;
        }
        else if (($topicWordLen * 3) > $topicBypeLen) {			// English + Chinese
            $topicDisplayLen = $topicLenDiff + (($topicWordLen - $topicLenDiff) * 1.84);
        }
        $topicsAry[$thisTopicId]["DisplayLen"] = $topicDisplayLen;
    }
    $topicsAry = BuildMultiKeyAssoc($topicsAry, array("CatTypeName", "TopicCatNameCh", "TopicID"));
    
    // Student Topic Scores
    $topicScoreAry = $indexVar["libreportcard"]->GET_STUDENT_TOPIC_SCORE($studentIdAry, $targetLevelId, "", "", $_POST["targetTermId"]);
    
    // Student Topic Score Summary
    $eachTopicScoreSummary = $indexVar["libreportcard"]->GetStudentScoreSummary($studentIdAry, $targetClassId, $_POST["targetTermId"], true);
    $eachTopicScoreSummary = $eachTopicScoreSummary[1];
    
    # Define Header & Footer
    $PageHeader = $indexVar["libreportcard_ui"]->Get_Report_Page_Header($yearSemInfo);
    $AwardPageHeader = $indexVar["libreportcard_ui"]->Get_Report_Page_Header($yearSemInfo, $isAwardPage=true);
    $mpdf->DefHTMLHeaderByName("pageHeader", $PageHeader);
    $mpdf->DefHTMLHeaderByName("awardPageHeader", $AwardPageHeader);
    $mpdf->DefHTMLHeaderByName("emptyHeader", "");
    $mpdf->SetHTMLHeaderByName("emptyHeader");
    $mpdf->DefHTMLFooterByName("emptyFooter", "");
    
    # Set Report CSS
    $styleContent = file_get_contents($indexVar["thisBasePath"]."/home/eAdmin/StudentMgmt/reportcard_kindergarten/asset/sth.css");
    $mpdf->WriteHTML($styleContent, 1);
    
    # Set Report Content
    $mpdf->WriteHTML("<div id='content'><div class='page-wrapper'>");
    foreach((array)$studentIdAry as $thisStudentIndex => $thisStudentId)
    {
        // Student Info Data
        $studentInfoData = $indexVar['libreportcard']->getReportOtherInfoData($_POST["targetTermId"], $UploadType = 'summary', $studentInfoAry[$thisStudentId]['YearClassID'], $thisStudentId, $targetLevelId);
        $studentInfoData = $studentInfoData[$thisStudentId][$_POST["targetTermId"]];
        
        /* 
        // Student Awards (Other Info)
        $studentAwardAry = array();
        $awardAry = $indexVar['libreportcard']->getReportOtherInfoData($_POST["targetTermId"], $UploadType = 'award', $studentInfoAry[$thisStudentId]['YearClassID'], $thisStudentId, $targetLevelId);
        $awardAry = $awardAry[$thisStudentId][$_POST["targetTermId"]]['Awards'];
        foreach((array)$awardAry as $thisAwardIndex => $awardStr)
        {
            $awardBypeLen = strlen($awardStr);		// byte count
            $awardWordLen = mb_strlen($awardStr);	// word count
            $awardLenDiff = (($awardWordLen * 3) - $awardBypeLen) / 2;
            
            $awardDisplayLen = 0;
            if($awardBypeLen == $awardWordLen) {					// English only
                $awardDisplayLen = $awardBypeLen;
            }
            else if (($awardWordLen * 3) == $awardBypeLen) {		// Chinese only
                $awardDisplayLen = $awardWordLen * 1.84;
            }
            else if (($awardWordLen * 3) > $awardBypeLen) {			// English + Chinese
                $awardDisplayLen = $awardLenDiff + (($awardWordLen - $awardLenDiff) * 1.84);
            }
            $studentAwardAry[$thisAwardIndex]["DisplayLen"] = $awardDisplayLen;
            $studentAwardAry[$thisAwardIndex]["DisplayStr"] = $awardStr;
        }
         */
        
        // Student Awards
        $studentAwardInfoArr = $indexVar['libreportcard']->Get_Student_Award_List(array($thisStudentId));
        
        // Cover Page
        $CoverPage = $indexVar["libreportcard_ui"]->Get_Report_Cover_Page($studentInfoAry[$thisStudentId], $yearSemInfo, $studentInfoData);
        $mpdf->WriteHTML($CoverPage);
        $mpdf->SetHTMLFooterByName("emptyFooter", "", true);
        
        // Set Header
        $mpdf->SetHTMLHeaderByName("pageHeader");
        $mpdf->writeHTML("<pagebreak />");
        
        // Ability Index Section
        $AbilitySection = $indexVar["libreportcard_ui"]->Get_Report_Ability_Index_Result($studentInfoAry[$thisStudentId], $svgData[$thisStudentId], $eachTopicScoreSummary[$thisStudentId], $yearSemInfo);
        $mpdf->WriteHTML($AbilitySection);
        
        // Indicator Score Section
        $IndicatorSection = $indexVar["libreportcard_ui"]->Get_Report_Indicator_Score_Result($studentInfoAry[$thisStudentId], $topicsAry, $topicScoreAry[$thisStudentId]);
        $mpdf->WriteHTML($IndicatorSection);
        
        // Signature Table (Footer)
        $SignatureFooter = $indexVar["libreportcard_ui"]->returnSignatureTable($ClassTeacher);
        $mpdf->DefHTMLFooterByName("studentFooter", $SignatureFooter);
        $mpdf->SetHTMLFooterByName("studentFooter", "", true);
        
        // Award Section
        if($SemesterNumber == 3)
        {
            $mpdf->writeHTML("<pagebreak resetpagenum='1' />");
            $mpdf->SetHTMLHeaderByName("awardPageHeader", "", true);
            
            // $AwardSection = $indexVar["libreportcard_ui"]->Get_Report_Student_Award($studentInfoAry[$thisStudentId], $studentAwardAry);
            $AwardSection = $indexVar["libreportcard_ui"]->Get_Report_Student_Award_2($yearSemInfo, $studentInfoAry[$thisStudentId], $studentAwardInfoArr);
            $mpdf->WriteHTML($AwardSection);
        }
        
        // Reset Page Number and Header
        if(($thisStudentIndex + 1) != $studentIdSize)
        {
            $mpdf->SetHTMLHeaderByName("emptyHeader");
            $mpdf->writeHTML("<pagebreak resetpagenum='1' />");
        }
    }
    $mpdf->WriteHTML("</div></div>");
    
    # Output PDF file
    $mpdf->Output("reportd.pdf", "I");
    
}

?>