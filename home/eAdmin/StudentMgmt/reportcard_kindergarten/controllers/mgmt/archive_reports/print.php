<?php
// Using :
/*
 * 	Date: 2019-10-11 (Bill)
 * 			Create file
 */

if($_POST['DBYearID'] && $_POST['TermID'] && $_POST['ClassID'] && $_POST['ClassID'][0] && $_POST['StudentID'])
{
    // Using target year db
    $indexVar["libreportcard"] = new libreportcardkindergarten($_POST['DBYearID']);
    $indexVar['libreportcard_ui'] = new libreportcardkindergarten_ui();

    // Check if archived before
    $studentLastArchived = $indexVar["libreportcard"]->GET_LASTEST_ARCHIVE_REPORT_INFO('', $_POST['ClassID'][0], $_POST['TermID'], $_POST['StudentID']);
    $hasArchivedReport = !empty($studentLastArchived) && $studentLastArchived[0] != '';
    if($hasArchivedReport)
    {
        // Rebuild file path
        $target_folderpath = $file_path."/"."file/reportcard_kindergarten/".$indexVar["libreportcard"]->Get_Active_AcademicYearName()."/reports/";
        $target_folderpath .= $indexVar["libreportcard"]->getEncryptedTextWithNoTimeChecking($_POST['TermID']."/".$_POST['ClassID'][0]);
        $target_filename = $indexVar["libreportcard"]->getEncryptedTextWithNoTimeChecking($_POST['StudentID']).".pdf";
        $target_filepath = $target_folderpath."/".$target_filename;
        if (file_exists($target_filepath))
        {
            //$download_url = getEncryptedText($target_filepath);
            //header("Location:/home/download_attachment.php?target_e=".$download_url);

            // Get Student Name
            $StudentInfo = $indexVar["libreportcard"]->Get_Student_By_Class($_POST['ClassID'][0], $_POST['StudentID'], 1, 1);
            $filename = $StudentInfo[0]['ClassTitleEn'].'-'.$StudentInfo[0]['ClassNumber'];

            // Header Content Type
            header("Content-type: application/pdf");
            header("Content-Disposition: inline; filename=".$filename.".pdf");
            header("Content-Length: ".filesize($target_filepath));

            // Send file to browser
            readfile($target_filepath);
        }
    }
}