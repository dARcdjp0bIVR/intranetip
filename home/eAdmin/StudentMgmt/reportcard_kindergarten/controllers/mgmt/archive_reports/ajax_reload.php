<?php
// Using :
/*
 * 	Date: 2019-10-11 (Bill)
 * 			Create file
 */

# Using target year db
if(isset($_POST['DBYearID'])) {
    $indexVar["libreportcard"] = new libreportcardkindergarten($_POST['DBYearID']);
    $indexVar['libreportcard_ui'] = new libreportcardkindergarten_ui();
}

$action = ($_POST["Action"])? $_POST["Action"] : $_GET["Action"];
if ($action == "Reload_Term_List")
{
    # Get Terms with archived reports
    $archivedReportTermInfo = $indexVar["libreportcard"]->GET_ARCHIVE_REPORT_RELATED_LIST($_POST["ClassID"], '');
    $archivedReportTermIDArr = Get_Array_By_Key($archivedReportTermInfo, 'YearTermID');

    if(empty($archivedReportTermIDArr)) {
        echo $Lang['eReportCardKG']['Management']['PrintArchiveReport']['WarningArr']['NoMatchTerm'];
    }
    else {
        echo $indexVar['libreportcard_ui']->Get_Semester_Selection($_POST["TermID"], $_POST['onChange'], false, false, 0, '', $archivedReportTermIDArr);
    }
}
else if ($action == "Reload_Class_List")
{
    # Get Classes with archived reports
    $archivedReportClassInfo = $indexVar["libreportcard"]->GET_ARCHIVE_REPORT_RELATED_LIST('', $_POST["TermID"]);
    $archivedReportClassIDArr = Get_Array_By_Key($archivedReportClassInfo, 'YearClassID');

    if(empty($archivedReportClassIDArr)) {
        echo $Lang['eReportCardKG']['Management']['PrintArchiveReport']['WarningArr']['NoMatchClass'];
    }
    else {
        echo $indexVar['libreportcard_ui']->Get_Class_Selection($_POST["ClassID"], $_POST['onChange'], true, false, '', 'ClassID', false, $archivedReportClassIDArr);
    }
}
else if($action == "StudentByClass")
{
    if($_POST["ClassID"] && $_POST["TermID"])
    {
        # Get Class Student
        $classStudentAry = $indexVar["libreportcard"]->Get_Student_By_Class($_POST["ClassID"]);

        # Get Student with archived reports
        $archivedReportStudentInfo = $indexVar["libreportcard"]->GET_ARCHIVE_REPORT_RELATED_LIST($_POST["ClassID"], $_POST["TermID"], true);
        $archivedReportStudentIDArr = Get_Array_By_Key($archivedReportStudentInfo, 'StudentID');

        $dataAry = array();
        foreach((array)$classStudentAry as $studentInfo)
        {
            if(empty($archivedReportStudentIDArr) || (!empty($archivedReportStudentIDArr) && !in_array($studentInfo["UserID"], (array)$archivedReportStudentIDArr))){
                continue;
            }

            $studentName = $studentInfo["StudentName"];
            $studentClassName = $studentInfo["ClassName"];
            $studentClassNumber = $studentInfo["ClassNumber"];
            $studentDisplay = $studentName." (".$studentClassName."-".$studentClassNumber.")";

            $dataAry[] = array($studentInfo["UserID"], $studentDisplay);
        }

        if(empty($dataAry)) {
            echo $Lang['eReportCardKG']['Management']['PrintArchiveReport']['WarningArr']['NoMatchStudent'];
        } else {
            echo getSelectByArray($dataAry, " id='StudentID' name='StudentID' ");
        }
    }
}
?>