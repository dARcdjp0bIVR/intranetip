<?
// Editing by 
/*
 * 	Date: 2018-01-11 (Bill)
 * 			Create file
 */

# Page Title
$TAGS_OBJ[] = array($Lang["eReportCardKG"]["Management"]["ExportPerformance"]["Title"], '', 1);
$TAGS_OBJ[] = array($Lang["eReportCardKG"]["Management"]["ExportYearlyPerformance"]["Title"], '?task=mgmt.export_performance.yearly', 0);
$indexVar["libreportcard_ui"]->Echo_Module_Layout_Start();

# Semester Selection
$htmlAry["SemesterSelection"] = $indexVar["libreportcard_ui"]->Get_Semester_Selection("", " ");

# Class Selection
$htmlAry["ClassSelection"] = $indexVar["libreportcard_ui"]->Get_Class_Selection("", " js_Reload_Student_Selection() ", true);
?>