<?
// Editing by 
/*
 * Change Log:
 * Date 2019-10-31 (Bill)
 *      class teacher > view own classes / class students only
 * Date 2019-06-06 (Bill)
 *      Create file
 */

# Page Title
$TAGS_OBJ[] = array($Lang['eReportCardKG']['Management']['AwardGeneration']['SelectAward'], '', 1);
$TAGS_OBJ[] = array($Lang['eReportCardKG']['Management']['AwardGeneration']['GenerateAward'], '?task=mgmt.award_generation.generate', 0);

$indexVar["libreportcard_ui"]->Echo_Module_Layout_Start($returnMsg);

$viewMode = $_POST['viewMode']? $_POST['viewMode'] : $_GET['viewMode'];
if($viewMode == '' || ($viewMode != 'CLASS' && $viewMode != 'AWARD')) {
    $viewMode = 'CLASS';
}

# Page View
$VIEWS_OBJ[] = array($Lang['eReportCardKG']['Management']['AwardGeneration']['StudentAward'], "javascript:js_Change_View_Mode('AWARD');", $img_src, $viewMode == 'AWARD');
$VIEWS_OBJ[] = array($Lang['SysMgr']['FormClassMapping']['Class'], "javascript:js_Change_View_Mode('CLASS');", $img_src, $viewMode == 'CLASS');
$htmlAry["ContentTopBtn"] = $indexVar["libreportcard_ui"]->GET_CONTENT_TOP_BTN($VIEWS_OBJ);

# Content Tool
$btnAry = array();
$btnAry[] = array('export', 'javascript: goExport();');
$htmlAry['contentTool'] = $indexVar['libreportcard_ui']->Get_Content_Tool_By_Array_v30($btnAry);

# Table Header
$Table_Content = "";
$Table_Content .= "<table class='common_table_list_v30' id='ContentTable'>";
$Table_Content .= "<thead>";
	$Table_Content .= "<tr>";
		$Table_Content .= "<th style='width:3%;'>#</th>";
	
		$Table_Content .= "<th style='width:10%;'>";
		if($viewMode == 'CLASS') {
			$Table_Content .= $Lang['eReportCardKG']['Management']['ToolScore']['ClassName'];
		} else {
		    $Table_Content .= $Lang['eReportCardKG']['Management']['AwardGeneration']['StudentAward'];
		}
		$Table_Content .= "</th>";
		
		if($viewMode == 'CLASS') {
		    $Table_Content .= "<th style='width:3%;'>&nbsp;</th>";
		} else {
		    $Table_Content .= "<th style='width:3%;'>".$Lang['eReportCardKG']['Management']['AwardGeneration']['ReceivedAwardCount']."</th>";
		}
		
	$Table_Content .= "</tr>";
$Table_Content .= "</thead>";
$Table_Content .= "<tbody>";

# Table Content
$i = 1;
if($viewMode == 'CLASS')
{
    $allClassArr = $indexVar["libreportcard"]->Get_All_KG_Class();
    if(!$indexVar["libreportcard"]->IS_KG_ADMIN_USER() && $indexVar["libreportcard"]->IS_KG_CLASS_TEACHER()) {
        $allClassArr = $indexVar["libreportcard"]->Get_Teaching_Class($UserID);
    }

    foreach($allClassArr as $thisClassInfo)
    {
    	// Content row
    	$Table_Content .= "<tr>";
    		$Table_Content .= "<td>".$i++."</td>";
    		$Table_Content .= "<td>".Get_Lang_Selection($thisClassInfo['ClassTitleB5'], $thisClassInfo['ClassTitleEN'])."</td>";
    		$Table_Content .= "<td>";
        		$Table_Content .= "<a href='javascript:goEdit(\"".$thisClassInfo["YearClassID"]."\")'>";
                    $Table_Content.= "<img src='/images/".$LAYOUT_SKIN."/icon_edit_b.gif' width='20' height='20' border='0' title='Edit'>";
    			$Table_Content .= "</a>";
    		$Table_Content .= "</td>";
    	$Table_Content .= "<tr>";
    }
}
else
{
    $filterStudentIdArr = '';
    if(!$indexVar["libreportcard"]->IS_KG_ADMIN_USER() && $indexVar["libreportcard"]->IS_KG_CLASS_TEACHER()) {
        $filterStudentIdArr = array();
        $filterClassArr = $indexVar["libreportcard"]->Get_Teaching_Class($UserID);
        foreach((array)$filterClassArr as $thisFilterClass)
        {
            $ClassStudentInfoAssoArr = $indexVar['libreportcard']->Get_Student_By_Class($thisFilterClass['YearClassID'], '', 0, 0, 0, $ReturnAsso=1);
            if(!empty($ClassStudentInfoAssoArr)) {
                $filterStudentIdArr = array_merge($filterStudentIdArr, array_keys($ClassStudentInfoAssoArr));
            }
        }
        if(empty($filterStudentIdArr)) {
            $filterStudentIdArr = array('');
        }
    }

    $allAwardsArr = $indexVar["libreportcard"]->Get_KG_Award_List();
    foreach($allAwardsArr as $thisAwardInfo)
    {
        $allAwardStudentArr = $indexVar["libreportcard"]->Get_Student_Award_List($filterStudentIdArr, array($thisAwardInfo["AwardID"]), $awardType='');
        $thisStudentCount = count((array)$allAwardStudentArr);
        
        // Content row
        $Table_Content .= "<tr>";
            $Table_Content .= "<td>".$i++."</td>";
            $Table_Content .= "<td>".Get_Lang_Selection($thisAwardInfo['AwardNameCh'], $thisAwardInfo['AwardNameEn'])."</td>";
            $Table_Content .= "<td>";
                $Table_Content .= "<a href='javascript:goEdit(\"".$thisAwardInfo["AwardID"]."\")'>".$thisStudentCount."</a>";
            $Table_Content .= "</td>";
        $Table_Content .= "<tr>";
    }
}
$Table_Content .= "</tbody>";
$Table_Content .= "</table>";
$htmlAry['dbTableContent'] = $Table_Content;

# Hidden Input Fields
$htmlAry["hiddenInputField"] = "";
$htmlAry["hiddenInputField"] .= "<input type='hidden' name='ClassID' id='ClassID' value=''>\r\n";
$htmlAry["hiddenInputField"] .= "<input type='hidden' name='AwardID' id='AwardID' value=''>\r\n";
$htmlAry["hiddenInputField"] .= "<input type='hidden' name='viewMode' id='viewMode' value='$viewMode'>\r\n";
?>