<?php 
// Using:
/*
 *  Date 2019-10-31 (Bill)
 *          class teacher > view own classes only
 * 	Date: 2019-06-17 (Bill)
 * 			Create file
 */

include_once($indexVar['thisBasePath']."includes/libexporttext.php");
$lexport = new libexporttext();

### Get Award Student
$StudentAwardInfoArr = $indexVar['libreportcard']->Get_Student_Award_List();
$StudentAwardInfoAssoc = BuildMultiKeyAssoc($StudentAwardInfoArr, array('StudentID', 'AwardID'));
$StudentIDArr = Get_Array_By_Key($StudentAwardInfoArr, 'StudentID');
if(empty($StudentIDArr)) {
    $StudentIDArr = array('');
}

### Get Class
$ClassInfoArr = $indexVar['libreportcard']->Get_All_KG_Class();
if(!$indexVar["libreportcard"]->IS_KG_ADMIN_USER() && $indexVar["libreportcard"]->IS_KG_CLASS_TEACHER()) {
    $ClassInfoArr = $indexVar["libreportcard"]->Get_Teaching_Class($UserID);
}
$YearClassIDArr = Get_Array_By_Key($ClassInfoArr, 'YearClassID');

### Get Form Student Info
$StudentInfoAssoArr = array();
foreach((array)$YearClassIDArr as $thisYearClassID)
{
    $ClassStudentInfoAssoArr = $indexVar['libreportcard']->Get_Student_By_Class(array($thisYearClassID), $StudentIDArr, 0, 0, 0, $ReturnAsso=1);
    if(!empty($ClassStudentInfoAssoArr)) {
        $StudentInfoAssoArr = $StudentInfoAssoArr + $ClassStudentInfoAssoArr;
    }
}

$ExportArr = array();
$ExportArr[0][] = '班別';
$ExportArr[0][] = '班號';
$ExportArr[0][] = '內聯網帳號';
$ExportArr[0][] = '學生姓名';
$ExportArr[0][] = '獎項';
$ExportArr[0][] = '獎項編號';

foreach((array)$StudentInfoAssoArr as $thisStudentInfo)
{
    foreach((array)$StudentAwardInfoAssoc[$thisStudentInfo['UserID']] as $thisStudentAward)
    {
        $dataAry = array();
        $dataAry[] = $thisStudentInfo['ClassName'];
        $dataAry[] = $thisStudentInfo['ClassNumber'];
        $dataAry[] = $thisStudentInfo['UserLogin'];
        $dataAry[] = $thisStudentInfo['StudentName'];
        $dataAry[] = Get_Lang_Selection($thisStudentAward['AwardNameCh'], $thisStudentAward['AwardNameEn']);
        $dataAry[] = $thisStudentAward['AwardCode'];
        
        $ExportArr[] = $dataAry;
    }
}

# Define column title
$ExportColumn = array();
$ExportColumn[] = 'Class Name';
$ExportColumn[] = 'Class Number';
$ExportColumn[] = 'User Login';
$ExportColumn[] = 'Student Name';
$ExportColumn[] = 'Award';
$ExportColumn[] = 'Award Code';

$export_content .= $lexport->GET_EXPORT_TXT($ExportArr, $ExportColumn, "\t", "\r\n", "\t", 0, "11");

intranet_closedb();

# Output the file to user browser
$filename = 'student_award.csv';
$lexport->EXPORT_FILE($filename, $export_content);

?>