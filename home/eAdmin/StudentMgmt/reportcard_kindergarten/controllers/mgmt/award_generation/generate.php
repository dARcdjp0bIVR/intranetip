<?
// Editing by 
/*
 * Change Log:
 * Date 2019-10-31 (Bill)
 *      class teacher > view own class students only
 * Date 2019-06-06 (Bill)
 *      Create file
 */

# Page Title
$TAGS_OBJ[] = array($Lang['eReportCardKG']['Management']['AwardGeneration']['SelectAward'], '?task=mgmt.award_generation.index', 0);
$TAGS_OBJ[] = array($Lang['eReportCardKG']['Management']['AwardGeneration']['GenerateAward'], '', 1);

if(isset($success)){
    $returnMsg = $success? $Lang['General']['ReturnMessage']['UpdateSuccess'] : $Lang['General']['ReturnMessage']['UpdateUnsuccess'];
}else{
    $returnMsg = "";
}
$indexVar["libreportcard_ui"]->Echo_Module_Layout_Start($returnMsg);

$viewMode = $_POST['viewMode']? $_POST['viewMode'] : $_GET['viewMode'];
if($viewMode == '' || ($viewMode != 'CLASS' && $viewMode != 'AWARD')) {
    $viewMode = 'CLASS';
}

# Page View
$VIEWS_OBJ[] = array($Lang['eReportCardKG']['Management']['AwardGeneration']['StudentAward'], "javascript:js_Change_View_Mode('AWARD');", $img_src, $viewMode == 'AWARD');
$VIEWS_OBJ[] = array($Lang['SysMgr']['FormClassMapping']['Class'], "javascript:js_Change_View_Mode('CLASS');", $img_src, $viewMode == 'CLASS');
$htmlAry["ContentTopBtn"] = $indexVar["libreportcard_ui"]->GET_CONTENT_TOP_BTN($VIEWS_OBJ);

if($viewMode == "AWARD")
{
    $filterStudentIdArr = '';
    if(!$indexVar["libreportcard"]->IS_KG_ADMIN_USER() && $indexVar["libreportcard"]->IS_KG_CLASS_TEACHER()) {
        $filterStudentIdArr = array();
        $filterClassArr = $indexVar["libreportcard"]->Get_Teaching_Class($UserID);
        foreach((array)$filterClassArr as $thisFilterClass)
        {
            $ClassStudentInfoAssoArr = $indexVar['libreportcard']->Get_Student_By_Class($thisFilterClass['YearClassID'], '', 0, 0, 0, $ReturnAsso=1);
            if(!empty($ClassStudentInfoAssoArr)) {
                $filterStudentIdArr = array_merge($filterStudentIdArr, array_keys($ClassStudentInfoAssoArr));
            }
        }
        if(empty($filterStudentIdArr)) {
            $filterStudentIdArr = array('');
        }
    }

    # Table Header
    $Table_Content = "";
    $Table_Content .= "<table class='common_table_list_v30' id='ContentTable'>";
    $Table_Content .= "<thead>";
    	$Table_Content .= "<tr>";
    		$Table_Content .= "<th style='width:3%;'>#</th>";
    		$Table_Content .= "<th style='width:10%;'>".$Lang['eReportCardKG']['Management']['AwardGeneration']['StudentAward']."</th>";
    	    $Table_Content .= "<th style='width:3%;'>".$Lang['eReportCardKG']['Management']['AwardGeneration']['ReceivedAwardCount']."</th>";
    	$Table_Content .= "</tr>";
    $Table_Content .= "</thead>";
    $Table_Content .= "<tbody>";
    
    # Table Content
    $i = 1;
    $allAwardsArr = $indexVar["libreportcard"]->Get_KG_Award_List('', $awardType=$ercKindergartenConfig['awardType']['Generate']);
    foreach($allAwardsArr as $thisAwardInfo)
    {
        $allAwardStudentArr = $indexVar["libreportcard"]->Get_Student_Award_List($filterStudentIdArr, array($thisAwardInfo["AwardID"]), $awardType='');
        $thisStudentCount = count((array)$allAwardStudentArr);
        
        // Content row
        $Table_Content .= "<tr>";
            $Table_Content .= "<td>".$i++."</td>";
            $Table_Content .= "<td>".Get_Lang_Selection($thisAwardInfo['AwardNameCh'], $thisAwardInfo['AwardNameEn'])."</td>";
            $Table_Content .= "<td>";
                $Table_Content .= "<a href='javascript:goEdit(\"".$thisAwardInfo["AwardID"]."\")'>".$thisStudentCount."</a>";
            $Table_Content .= "</td>";
        $Table_Content .= "<tr>";
    }
    $Table_Content .= "</tbody>";
    $Table_Content .= "</table>";
}
else
{
    # Class Selection
    $htmlAry["ClassSelection"] = $indexVar["libreportcard_ui"]->Get_Class_Selection("", " ", true);
}
$htmlAry['dbTableContent'] = $Table_Content;

# Buttons
$submitBtnLang = $viewMode == "CLASS"? $Lang['eReportCardKG']['Management']['AwardGeneration']['GenerateAward'] : $Lang['eReportCardKG']['Management']['AwardGeneration']['GenerateAll'];
$htmlAry["buttonDiv"] = "";
$htmlAry["buttonDiv"] .= '<div class="edit_bottom_v30">' . "\n";
$htmlAry["buttonDiv"] .= '<span class="tabletextremark" id="LastGeneratedSpan">'.$Lang['eReportCardKG']['Management']['AwardGeneration']['LastGenerate'].": ".$indexVar["libreportcard"]->Get_Award_Last_Generated_Date().'</span>' . "\n";
$htmlAry["buttonDiv"] .= '<br />' . "\n";
if(($viewMode == "CLASS") || ($viewMode == "AWARD" && $indexVar["libreportcard"]->IS_KG_ADMIN_USER()))
{
    $htmlAry["buttonDiv"] .= $indexVar["libreportcard_ui"]->GET_ACTION_BTN($submitBtnLang, "button", "js_Generate();", "submitBtn");
}
$htmlAry["buttonDiv"] .= '</div>' . "\n";

# Hidden Input Fields
$htmlAry["hiddenInputField"] = "";
$htmlAry["hiddenInputField"] .= "<input type='hidden' name='AwardID' id='AwardID' value=''>\r\n";
$htmlAry["hiddenInputField"] .= "<input type='hidden' name='viewMode' id='viewMode' value='$viewMode'>\r\n";
?>