<?php
// Using: 
/*
 * 	Date: 2017-12-19 (Bill)
 * 			Create file
 */

$action = ($_POST["Action"])? $_POST["Action"] : $_GET["Action"];
if ($action == "Add_Student_Award")
{
    $StudentID = IntegerSafe($_POST['StudentID']);
    $AwardID = IntegerSafe($_POST['AwardID']);
    
    $result = false;
    if($StudentID > 0 && $AwardID > 0)
    {
        $dataAry = array();
        $dataAry['StudentID'] = $StudentID;
        $dataAry['AwardID'] = $AwardID;
        
        $result = $indexVar["libreportcard"]->Insert_Student_Award($dataAry);
    }
    
    echo $result? 1 : 0;
}
else if ($action == "Add_Award_Student")
{
    $StudentIDArr = IntegerSafe($_POST['StudentIDArr']);
    $AwardID = IntegerSafe($_POST['AwardID']);
    
    $result = false;
    if($StudentIDArr != '' && $AwardID > 0)
    {
        $resultArr = array();
        $StudentIDArr = explode(',', $StudentIDArr);
        foreach((array)$StudentIDArr as $StudentID)
        {
            $dataAry = array();
            $dataAry['AwardID'] = $AwardID;
            $dataAry['StudentID'] = $StudentID;
            
            $resultArr[] = $indexVar["libreportcard"]->Insert_Student_Award($dataAry);
        }
        $result = (!empty($resultArr) && !in_array(false, (array)$resultArr));
    }
    
    echo $result? 1 : 0;
}
else if ($action == "Delete_Student_Award")
{
    $StudentID = IntegerSafe($_POST['StudentID']);
    $RecordID = IntegerSafe($_POST['RecordID']);
    
    $result = false;
    if($StudentID > 0 && $RecordID > 0)
    {
        $result = $indexVar["libreportcard"]->Delete_Student_Award(array($RecordID), array($StudentID));
    }
    
    echo $result? 1 : 0;
}

intranet_closedb();
?>