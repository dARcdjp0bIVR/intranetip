<?
// Using :
/*
 * 	Date: 2019-10-11 (Bill)
 * 			Create file
 */

# Page Title
$TAGS_OBJ[] = array($Lang["eReportCardKG"]["Management"]["ArchiveReport"]["Title"]);
$indexVar["libreportcard_ui"]->Echo_Module_Layout_Start($indexVar['returnMsgLang']);

# Semester Selection
$TermID = $_GET['TermID'];
$htmlAry["SemesterSelection"] = $indexVar["libreportcard_ui"]->Get_Semester_Selection($TermID, $onChange="js_Reload_Archive_Status()");

# Form Selection
$YearID = $_GET['YearID'];
$htmlAry["FormSelection"] = $indexVar["libreportcard_ui"]->Get_Year_Selection($YearID, $onChange="js_Reload_Class_Selection()");

# Class Selection
$ClassID = $_GET['ClassID'];
if($ClassID) {
    $htmlAry["ClassSelection"] = $indexVar["libreportcard_ui"]->Get_Class_Selection($ClassID, $onChange='js_Reload_Archive_Status()', $withYearOptGroup=false, $noFirstTitle=false, $YearID, $selectionName='ClassID', $adminFilterByYearID=true);
}

# Last Archive Info
if($TermID && $YearID && $ClassID)
{
    $htmlAry["LastArchiveInfo"] = $indexVar["libreportcard"]->GET_LASTEST_ARCHIVE_REPORT_INFO($YearID, $ClassID, $TermID, $studentID = '');
    if($htmlAry["LastArchiveInfo"][0]) {
        $htmlAry["LastArchiveInfo"] = $htmlAry["LastArchiveInfo"][0];
    }
}
?>