<?
// Editing by 
/*
 *  Date 2019-10-31 (Bill)
 *          class teacher & subject teacher > view related data only
 *  Date: 2018-06-14 (Bill)
 *          hide drop down list for subject teacher
 * 	Date: 2017-12-18 (Bill)
 * 			Create file
 */

### Page Title
$TAGS_OBJ[] = array($Lang["eReportCardKG"]["Management"]["LanguageBehavior"]["Title"]);

### Message
if($success) {
	$returnMsg = $Lang["General"]["ReturnMessage"]["UpdateSuccess"];
}
else {
	$returnMsg = "";
}
$indexVar["libreportcard_ui"]->Echo_Module_Layout_Start($returnMsg);

### Year Selection
$YearID = isset($_POST["YearID"])? $_POST["YearID"] : $_GET["YearID"];
if($indexVar["libreportcard"]->IS_KG_ADMIN_USER() || $indexVar["libreportcard"]->IS_KG_CLASS_TEACHER()){
    $filterByClassTeacherWithSubjects = !$indexVar["libreportcard"]->IS_KG_ADMIN_USER() && $indexVar["libreportcard"]->IS_KG_SUBJECT_TEACHER();
    $htmlAry["yearSelection"] = $indexVar["libreportcard_ui"]->Get_Year_Selection($YearID, $onChange="document.form1.submit()", $allKGOptions=false, $noFirstTitle=false, $firstTitleType=1, $filterByClassTeacherWithSubjects);
}

### Term Selection
$TermID = isset($_POST["TermID"])? $_POST["TermID"] : $_GET["TermID"];
if($indexVar["libreportcard"]->IS_KG_ADMIN_USER() || $indexVar["libreportcard"]->IS_KG_CLASS_TEACHER()){
    $htmlAry["termSelection"] = $indexVar["libreportcard_ui"]->Get_Semester_Selection($TermID, $onChange="document.form1.submit()", $allYearOption=false, $noFirstTitle=false, $firstTitleType=1);
}

### Topic DB table
$htmlAry["dbTableContent"] = $indexVar["libreportcard_ui"]->Get_Topic_Score_Table($YearID, $TermID);

# Hidden Input Fields
$htmlAry["hiddenInputField"] = "";
$htmlAry["hiddenInputField"] .= "<input type='hidden' name='ClassID' id='ClassID' value=''>\r\n";
$htmlAry["hiddenInputField"] .= "<input type='hidden' name='YearTermID' id='YearTermID' value=''>\r\n";
$htmlAry["hiddenInputField"] .= "<input type='hidden' name='TopicCatID' id='TopicCatID' value=''>\r\n";
?>