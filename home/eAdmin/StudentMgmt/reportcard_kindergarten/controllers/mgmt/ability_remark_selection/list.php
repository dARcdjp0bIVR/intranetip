<?php
// editing by
/*
 * 	Date: 2019-10-11 Philips
 * 			- Added general warning box
 * 
 *  Date: 2019-03-22 Bill
 *          - Added Last Modified Info Display
 *  Date: 2019-01-10 Philips
 *          - Create file
 */

### Cookies Setting
$arrCookies = array();
$arrCookies[] = array("eRCkg_mgmt_abilityRemarkCategory_TWCatID", "selectTWCat");
$arrCookies[] = array("eRCkg_mgmt_abilityRemarkCategory_TWLevel", "selectTWLevel");
$arrCookies[] = array("eRCkg_mgmt_abilityRemarkCategory_Year", "Year");
$arrCookies[] = array("eRCkg_mgmt_abilityRemarkCategory_Term", "Term");
if(isset($clearCoo) && $clearCoo == 1) {
    clearCookies($arrCookies);
} else {
    updateGetCookies($arrCookies);
}

// Page Title
$TAGS_OBJ[] = array($Lang['eReportCardKG']['Management']['AbilityRemarksSelections']['ModuleTitle']);
$indexVar['libreportcard_ui']->Echo_Module_Layout_Start($indexVar['returnMsgLang']);

### Warning box
$WarningBox = $indexVar["libreportcard_ui"]->Get_Warning_Message_Box('<font color="red">'.$Lang['General']['Tips'].'</font>', $Lang['eReportCardKG']['General']['PleaseConfirmChanges']);

// Get Search Value
if(isset($Term) && !isset($TermID)) {
    $TermID = $Term;
}
if(isset($Year) && !isset($YearID)) {
    $YearID = $Year;
}
$selectYear = $indexVar['libreportcard']->getYearName($YearID);
$searchYear = addslashes(trim($selectYear));
$searchTWCat = addslashes(trim($selectTWCat));
$searchTWLevel = addslashes(trim($selectTWLevel));
$searchMOLevel = addslashes(trim($selectMOCat));

### Ability
// Linked to Teaching Tools
$toolAbilityCode = $indexVar['libreportcard']->getFormReportAbilityCode($YearID, $TermID);
//$abilityCode = implode(',', $abilityCode);
// debug_pr($toolAbilityCode);

// Linked to Subjects
$subjectAbilityCode = $indexVar['libreportcard']->getFormReportSubjectAbilityCode($YearID, $TermID);
// debug_pr($subjectAbilityCode);

$abilityCode = array_values(array_unique(array_filter(array_merge((array)$toolAbilityCode, (array)$subjectAbilityCode))));
if(count($abilityCode) > 0)
{
    $abilityCode = implode(",", $abilityCode);
}

// DB Table Settings
// if (isset($ck_page_size) && $ck_page_size != "") {
//     $page_size = $ck_page_size;
// }

### Action buttons
// $btnAry = array();
// $btnAry[] = array('edit', 'javascript: checkEdit();');
// $htmlAry['dbTableActionBtn'] = $indexVar['libreportcard_ui']->Get_DBTable_Action_Button_IP25($btnAry);

// Linked Macau Category
$sortResult = array();
$lastModifiedInfo = array();
$macauArr = $indexVar['libreportcard']->Get_Macau_Category(true);
$resultArr = $indexVar['libreportcard']->view_ABILITY_REMARK_SELECTION($TermID, $YearID, $abilityCode, $searchTWCat, $searchTWLevel);
foreach($resultArr as $rs)
{
    if(!$sortResult[$rs['CatID']])
    {
        $sortResult[$rs['CatID']] = array(
            "RecordID" => $rs['RecordID'],
            "CatID" => $rs['CatID'],
            "TWItemCode" => $rs['TWItemCode'],
            "TargetMOCat" => $rs['TargetMOCat'],
            "MOItemCode" => array()
        );
        
        if(!is_date_empty($rs['DateModified']) && $rs['ModifiedBy'] != '')
        {
            if(empty($lastModifiedInfo) || (!empty($lastModifiedInfo) && (strtotime($rs['DateModified']) < strtotime($lastModifiedInfo['DateModified']))))
            {
                $lastModifiedInfo['DateModified'] = $rs['DateModified'];
                $lastModifiedInfo['ModifiedBy'] = $rs['ModifiedBy'];
            }
        }
    }
    
    //$twic = mb_substr($rs['TWItemCode'],0,1, 'utf-8');
    $moic = substr($rs['MOItemCode'],0,1);
    //$moicc = ord($moic) - 64;
    $sortResult[$rs['CatID']]["MOItemCode"][$moic] = 1;
    //$catArr[] = $twic .'-'.$moicc;
}
//$catArr = array_unique($catArr);
//$catAry = $indexVar['libreportcard']->find_ABILITY_INDEX_CATEGORY_TAIWAN_ARRAY($catArr);
//$catArr = array();
//if(!empty($catAry)){
//    foreach($catAry as $rs){
//        $catArr[$rs['TWItemCode']] = $rs['Name'];
//    }
//}

$column_list = '<thead>';
    $column_list .= "<th class='tabletop tabletopnolink' width='1'>#</td>\n";
    $column_list .= "<th class='tabletop tabletopnolink' width='30%'>".$Lang['eReportCardKG']['Setting']['AbilityIndexMapping']['TaiwanCode']."</td>\n";
    $column_list .= "<th class='tabletop tabletopnolink' width='69%' style='text-align: center'>".$Lang['eReportCardKG']['Management']['AbilityRemarksSelections']['Selections']."</td>\n"; 
$column_list .= '</thead>';

$rowCount = 0;
$tableBody = "<tbody>";
foreach($sortResult as $rs)
{
    if(sizeof($rs['MOItemCode']) > 1)
    {
        $rowCount++;
        
        $tableBody .= "<tr>";
            $tableBody .= "<td>" . $rowCount . "</td>";
            $tableBody .= "<td>" . $rs['TWItemCode'] . "</td>";
        
        //$twic = mb_substr($rs['TWItemCode'],0,1, 'utf-8');
        $checked = array(
            "none" => $rs['TargetMOCat'] == -1 ? 'checked' : '',
        );
        
        $selectionBox = "";
        ksort($rs['MOItemCode']);
        foreach($rs['MOItemCode'] as $k => $v)
        {
            if($selectionBox != '') {
                $selectionBox .= '&nbsp;&nbsp;';
            }
            
            //$moicc = ord($k) - 64;
            $checked[$k] = ($rs['TargetMOCat'] == $k) ? 'checked' : '';
            $selection_id = "selection_".$rs['CatID']."_$k";
            $selectionBox .= "<input type='radio' id='$selection_id' name='selection[".$rs['CatID']."]' value='$k' ".$checked[$k]."/> <label for='$selection_id'> ".$macauArr[$k]." ($k)</label>";
        }
        /*
        if($rs['MOItemCode']["A"]){
            $moicc = ord('A') - 64;
            $selectionBox .= "<input type='radio' name='selection[$rs[CatID]]' value='A' $checked[A]/> ".$catArr[$twic.'-'.$moicc] . "(A)";
        }
        if($rs['MOItemCode']["B"]){
            $moicc = ord('B') - 64;
            $selectionBox .= "<input type='radio' name='selection[$rs[CatID]]' value='B' $checked[B]/> ".$catArr[$twic.'-'.$moicc] . "(B)";
        }
        if($rs['MOItemCode']["C"]){
            $moicc = ord('C') - 64;
            $selectionBox .= "<input type='radio' name='selection[$rs[CatID]]' value='C' $checked[C]/> ".$catArr[$twic.'-'.$moicc] . "(C)";
            }*/
        $selectionBox .= '&nbsp;&nbsp;';
        
        $selection_id = "selection_".$rs['CatID']."_-1";
        $selectionBox .= "<input type='radio' id='$selection_id' name='selection[".$rs['CatID']."]' value='-1' $checked[none]/> <label for='$selection_id'> ".$Lang['eReportCardKG']['Management']['AbilityRemarksSelections']['None']."</label>";
        
            $tableBody .= "<td align='center'>" .$selectionBox. "</td>";
        $tableBody .= "</tr>";
    }
}
if($rowCount <= 0)
{
    $tableBody .= "<tr>";
        $tableBody .= "<td colspan='3' align='center'>" . $i_no_record_exists_msg . "</td>";
    $tableBody .= "</tr>";
}
$tableBody .= "</tbody>";

// Filter
$htmlAry['typeSelection'] = "";
$htmlAry['typeSelection'] .= $indexVar['libreportcard_ui']->Get_Taiwan_Category_Selection($selectTWCat);     // Taiwan Category
$htmlAry['typeSelection'] .= $indexVar['libreportcard_ui']->Get_Taiwan_ClassLevel_Selection($selectTWLevel); // Taiwan Class Level
$htmlAry['typeSelection'] .= $indexVar['libreportcard_ui']->Get_Year_Selection($YearID);
$htmlAry["typeSelection"] .= $indexVar["libreportcard_ui"]->Get_Semester_Selection($TermID);

// GET Table Content
$htmlAry['dataTable'] = "<table class='remarkSelectionTable common_table_list_v30'>";
$htmlAry['dataTable'] .= $column_list;
$htmlAry['dataTable'] .= $tableBody;
$htmlAry['dataTable'] .= "</table>";

// Table related Hidden Fields
$htmlAry['hiddenField'] = "";
$htmlAry['hiddenField'] .= "<input type='hidden' name='YearName value='$selectYear' />";

// hidden form2 value
$htmlAry['form2Hidden'] = "";
$htmlAry['form2Hidden'] .= "<input type='hidden' name='YearID' value='$YearID' />";
$htmlAry['form2Hidden'] .= "<input type='hidden' name='TermID' value='$TermID' />";

// form 2 sumbit btn
$htmlAry['form2Submit'] = "";
if($rowCount > 0) {
    $htmlAry['form2Submit'] .= $indexVar['libreportcard_ui']->GET_ACTION_BTN($button_submit, "submit");
}

// Last Modified Info
if (!empty($lastModifiedInfo))
{
    include_once ($indexVar['thisBasePath']."includes/libuser.php");
    $modifiedBy = new libuser($lastModifiedInfo['ModifiedBy']);
    
    $lastModifiedStrCh = $Lang['General']['LastUpdatedTime']." : " . $lastModifiedInfo['DateModified']. " (" .$iDiscipline['By'] . $modifiedBy->UserName() . ")";
    $lastModifiedStrEn = $Lang['General']['LastUpdatedTime']." : " . $lastModifiedInfo['DateModified']. " " . $iDiscipline['By'] . " " . $modifiedBy->UserName();
    $htmlAry['lastModifiedDiv'] = " <div>
                                        <span class='tabletextremark'>".Get_Lang_Selection($lastModifiedStrCh, $lastModifiedStrEn)."</span>
                                    </div><br/>";
}

?>