<?

# Page Title
$TAGS_OBJ[] = array($Lang['eReportCardKG']['Management']['DataTransition']['Title']);
$indexVar['libreportcard_ui']->Echo_Module_Layout_Start($returnMsg);

# Current Year
$currentYearID = Get_Current_Academic_Year_ID();
$currentYearName = getAYNameByAyId();

# Active Year
$activeYearID = $indexVar['libreportcard']->Get_Active_AcademicYearID();
$activeYearName = $indexVar['libreportcard']->Get_Active_AcademicYearName();

# All KG ReportCard DB
$sql = "SHOW DATABASES LIKE '".$intranet_db."_DB_REPORT_CARD_KINDERGARTEN_%'";
$DBYearList = $indexVar['libreportcard']->returnVector($sql);

# Existing Year Selection & Next Year	
$isGetNextYear = false;
$haveCurrentYear = false;
$existYearList = array();
$allYearList = $indexVar['libreportcard']->Get_All_AcademicYearID();
foreach($allYearList as $thisYearInfo)
{
	$thisYearID = $thisYearInfo["AcademicYearID"];
	$thisYearName = $thisYearInfo["AcademicYearName"];
	
	if(in_array($intranet_db."_DB_REPORT_CARD_KINDERGARTEN_".$thisYearID, $DBYearList)) {
		if($thisYearID == $currentYearID) {
			$haveCurrentYear = true;
		}
		if($thisYearID == $activeYearID) {
			$isGetNextYear = true;
		}
		else {
			$existYearList[] = array($thisYearID, $thisYearName);
		}
	}
	else if($isGetNextYear) {
		$NewYearID = $thisYearInfo["AcademicYearID"];
		$NewYearName = $thisYearInfo["AcademicYearName"];
		$isGetNextYear = false;
	}
}

if(!$haveCurrentYear) {
	$NewYearID = $currentYearID;
	$NewYearName = $currentYearName;
}

if ($NewYearID=="")
{
	// $noTermNextYear = 1;
	$disableNewYear = "disabled";
	$checkedNewYear = "";
	$checkedTransistion = "checked";
	$disableOtherYearSelect = '';
}
else
{
	//$noTermNextYear = 0;
	$disableNewYear = "";
	$checkedNewYear = "checked";
	$checkedTransistion = "";
	$disableOtherYearSelect = "disabled";
	
	$warning = "";
}

if(sizeof($existYearList) > 0) {
	if(sizeof($existYearList) > 0) {
		$yearRadioDisabled = "";
	}
	else {
		$yearRadioDisabled = "disabled";
		$disableOtherYearSelect = "disabled";
		$checkedNewYear = "checked";
		$checkedTransistion = "";
	}
	
	$otherYearSelect = "<br />
						<input type='radio' name='transitionAction' id='transitionAction2' value='other' onclick='toggleAction(this.value)' $checkedTransistion $yearRadioDisabled /> 
						<label for='transitionAction2'>".$Lang['eReportCardKG']['Management']['DataTransition']['OtherAcademicYears']."</label> ";
	$otherYearSelect .= $indexVar['libreportcard_ui']->GET_SELECTION_BOX($existYearList, "name='OtherYear' id='OtherYear' class='' $disableOtherYearSelect onchange=''", "", "");
}
$htmlAry["YearWarning"] = str_replace('<!--academicYearName-->', $activeYearName, $Lang['eReportCardKG']['Management']['DataTransition']['ConfirmCreateNewYearDatabase']);

$TransitionTable = "<table class='form_table_v30'>
						<tr id='WarningMsgRow'>
							<td align='' colspan='2' class='tabletextrequire'>
								<p>".$Lang['eReportCardKG']['Management']['DataTransition']['DataTransitionWarning1']."</p>
								<p>".$Lang['eReportCardKG']['Management']['DataTransition']['DataTransitionWarning2']."</p>
							</td>
						</tr>
						<tr id='FormContentRow1' style='display:none;'>
							<td valign='top' nowrap='nowrap' class='formfieldtitle tabletext'>".$Lang['eReportCardKG']['Management']['DataTransition']['ActiveAcademicYear']."</td>
							<td width='75%' class='tabletext'>".$activeYearName."</td>
						</tr>
						<tr id='FormContentRow2' style='display:none;'>
							<td valign='top' nowrap='nowrap' class='formfieldtitle tabletext'>".$Lang['eReportCardKG']['Management']['DataTransition']['CurrentAcademicYear']."</td>
							<td width='75%' class='tabletext'>".$currentYearName."</td>
						</tr>
						<tr id='FormContentRow3' style='display:none;'>
							<td valign='top' nowrap='nowrap' class='formfieldtitle tabletext'>".$Lang['eReportCardKG']['Management']['DataTransition']['Transition']."</td>
							<td width='75%' class='tabletext'>
								<input type='radio' name='transitionAction' id='transitionAction1' value='".$NewYearID."' ".$checkedNewYear." onclick='toggleAction(this.value)' ".$disableNewYear." /> 
								<label for='transitionAction1'>".$Lang['eReportCardKG']['Management']['DataTransition']['NewAcademicYear']." (".$NewYearName.") $warning </label>
								".$otherYearSelect."
							</td>
						</tr>
						<tr id='WarningBtnRow'>
							<td colspan='2' align='center'>
								".$indexVar['libreportcard_ui']->GET_ACTION_BTN($button_continue, "button", "toggleDisplay(1)")."
							</td>
						</tr>
						<tr id='FormBtnRow' style='display:none;'>
							<td colspan='2' align='center'>
								".$indexVar['libreportcard_ui']->GET_ACTION_BTN($button_submit, "button", "confirmSubmit()")."
								".$indexVar['libreportcard_ui']->GET_ACTION_BTN($button_cancel, "button", "toggleDisplay(0)")."
							</td>
						</tr>
					</table>";
$htmlAry["TransitionTable"] = "$TransitionTable";
?>