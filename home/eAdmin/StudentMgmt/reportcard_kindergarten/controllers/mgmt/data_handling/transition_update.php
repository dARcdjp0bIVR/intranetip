<?php

if (isset($_POST["transitionAction"]) && $_POST["transitionAction"] != "") {
	if ($_POST["transitionAction"] == "other" && (!isset($_POST["OtherYear"]) || !is_numeric($_POST["OtherYear"]))) {
		// do nothing
	}
	else if ($_POST["transitionAction"] != "other" && !is_numeric($_POST["transitionAction"])) {
	    // do nothing
	}
	else {
		# Case 1: Change active Database
		if ($_POST["transitionAction"] == "other") {
			$success = $indexVar['libreportcard']->Update_Active_AcademicYearID($_POST["OtherYear"]);
		}
		# Case 2: Create new Database
		else {
			$success = $indexVar['libreportcard']->Add_Active_AcademicYearID($_POST["transitionAction"]);
		}
	}
}

intranet_closedb();

$result = (!in_array(false, (array)$success)) ? "1" : "0";
header("Location: /home/eAdmin/StudentMgmt/reportcard_kindergarten/index.php?task=mgmt.data_handling.transition&success=$result");
?>