<?php
// editing by
/*
 * 	Date: 2019-10-11 Philips
 * 			- Added general warning box
 * 
 *  Date: 2019-02-14 Philips
 *          - Create file
 */

### Cookies Setting
// $arrCookies = array();
// if(isset($clearCoo) && $clearCoo == 1) {
//     clearCookies($arrCookies);
// }
// else {
//     updateGetCookies($arrCookies);
// }
//
// Include DB Table
// include_once ($indexVar['thisBasePath'] . "includes/libdbtable.php");
// include_once ($indexVar['thisBasePath'] . "includes/libdbtable2007a.php");

function dateTimeDivider($dt, $type)
{
//     $date = explode('-', $date);
//     $time = explode(':', $time);
    
//     return array(
//         'Year' => $date[0],
//         'Month' => $date[1],
//         'Day' => $date[2],
//         'Hour' => intval($time[0]),
//         'Minute' => intval($time[1]),
//         'Second' => intval($time[2])
//     );
    
    if($dt == '') {
        $type_time = $type == 'start'? '00:00:00' : '23:59:59';
        $dt = array('', $type_time);
    }
    else {
        $dt = explode(' ', $dt);
    }
    
    return $dt;
}

// function dateTimeInput($dtAry, $fieldname)
// {
//     global $indexVar;
//    
//    //$dateInput = "<input type='date' name='".$fieldname."_date' id='".$fieldname."_date' value='".$dtAry['Year']."-".$dtAry['Month']."-".$dtAry['Day']."' />";
//     $hourInput = "<select name='".$fieldname."_hour' id='".$fieldname."_hour'>";
//     for($i=0;$i<24;$i++){
//         $selected = ($dtAry['Hour']==$i) ? 'selected' : '';
//         $zero = ($i<10) ? '0' : '';
//         $hourInput .= "<option value='$zero$i' $selected>$zero$i</option>";
//     }
//     $hourInput .= '</select>';
//     $minuteInput = "<select name='".$fieldname."_minute' id='".$fieldname."_minute'>";
//     for($i=0;$i<60;$i++){
//         $selected = ($dtAry['Minute']==$i) ? 'selected' : '';
//         $zero = ($i<10) ? '0' : '';
//         $minuteInput .= "<option value='$zero$i' $selected>$zero$i</option>";
//     }
//     $minuteInput .= '</select>';
//     $secondInput = "<select name='".$fieldname."_second' id='".$fieldname."_second'>";
//     for($i=0;$i<60;$i++){
//         $selected = ($dtAry['Second']==$i) ? 'selected' : '';
//         $zero = ($i<10) ? '0' : '';
//         $secondInput .= "<option value='$zero$i' $selected>$zero$i</option>";
//     }
//     $secondInput .= '</select>';
//    
//     return array(
//         'Date' => $dateInput,
//         'Time' => $hourInput.':'.$minuteInput.':'.$secondInput
//     );
// }

// Page Title
$TAGS_OBJ[] = array($Lang['eReportCardKG']['Management']['ViewEditPeriodSetting']['Title']);
$indexVar['libreportcard_ui']->Echo_Module_Layout_Start($indexVar['returnMsgLang']);

# Get Year Term
$dataAry = array();
$YearTermAry = getSemesters($indexVar["libreportcard"]->Get_Active_AcademicYearID());
foreach ($YearTermAry as $thisTermId => $thisTermName) {
    $dataAry[$thisTermId] = $thisTermName;
}

# Get Period Settings
$yearTermID = $_GET['YearTermID'];
$inputScoreAry = $indexVar['libreportcard']->find_INPUT_SCORE_PERIOD_SETTING($yearTermID);
$viewReportAry = $indexVar['libreportcard']->find_VIEW_REPORT_PERIOD_SETTING($yearTermID);
$inputScore = $inputScoreAry[0];
$viewReport = $viewReportAry[0];
$isEdit = !empty($inputScore) || !empty($viewReport);

$inputStartTime = dateTimeDivider($inputScore['StartDate'], 'start');
$inputEndTime = dateTimeDivider($inputScore['EndDate'], 'end');
// $inputStartTimeInput = dateTimeInput($inputStartTime, 'inputStart');
// $inputEndTimeInput = dateTimeInput($inputEndTime, 'inputEnd');
$viewStartTime = dateTimeDivider($viewReport['StartDate'], 'start');
$viewEndTime = dateTimeDivider($viewReport['EndDate'], 'end');
// $viewStartTimeInput = dateTimeInput($viewStartTime, 'viewStart');
// $viewEndTimeInput = dateTimeInput($viewEndTime, 'viewEnd');

$WarningBox = $indexVar["libreportcard_ui"]->Get_Warning_Message_Box('<font color="red">'.$Lang['General']['Tips'].'</font>', $Lang['eReportCardKG']['General']['PleaseConfirmChanges']);

# Navigation
$PAGE_NAVIGATION[] = array($Lang['eReportCardKG']['Management']['ViewEditPeriodSetting']['Title'], "/home/eAdmin/StudentMgmt/reportcard_kindergarten/index.php?task=mgmt.view_edit_period_setting.list");
$PAGE_NAVIGATION[] = array(($isEdit? $Lang["Btn"]["Edit"] : $Lang["Btn"]["New"]));
$htmlAry['Navigation'] = $indexVar['libreportcard_ui']->GET_NAVIGATION_IP25($PAGE_NAVIGATION);

$htmlAry['formBody'] = "<tbody>";
$htmlAry['formBody'] .= "<tr>";
    $htmlAry['formBody'] .= "<td class='field_title'>".$Lang['eReportCardKG']['Management']['ViewEditPeriodSetting']['Term']."</td>";
    $htmlAry['formBody'] .= "<td>".$dataAry[($inputScore['YearTermID']? $inputScore['YearTermID'] : $yearTermID)]."</td>";
$htmlAry['formBody'] .= "</tr>";
$htmlAry['formBody'] .= "<tr>";
    $htmlAry['formBody'] .= "<td class='field_title'>".$Lang['eReportCardKG']['Management']['ViewEditPeriodSetting']['StartDate']." (".$Lang['eReportCardKG']['Management']['ViewEditPeriodSetting']['InputScorePeriod'].")</td>";
    $htmlAry['formBody'] .= "<td>".$indexVar["libreportcard_ui"]->GET_DATE_PICKER("inputStart", $inputStartTime[0])." ".$indexVar["libreportcard_ui"]->Get_Time_Selection("inputStart", $inputStartTime[1])."</td>";
$htmlAry['formBody'] .= "</tr>";
$htmlAry['formBody'] .= "<tr>";
    $htmlAry['formBody'] .= "<td class='field_title'>".$Lang['eReportCardKG']['Management']['ViewEditPeriodSetting']['EndDate']." (".$Lang['eReportCardKG']['Management']['ViewEditPeriodSetting']['InputScorePeriod'].")</td>";
    $htmlAry['formBody'] .= "<td>";
        $htmlAry['formBody'] .= $indexVar["libreportcard_ui"]->GET_DATE_PICKER("inputEnd", $inputEndTime[0])." ".$indexVar["libreportcard_ui"]->Get_Time_Selection("inputEnd", $inputEndTime[1]);
        $htmlAry['formBody'] .= $indexVar["libreportcard_ui"]->Get_Form_Warning_Msg("input_Warn", $Lang["General"]["WarningArr"]["EndDateCannotEarlierThanStartDate"], "warningMsgDiv", $display=false);
    $htmlAry['formBody'] .= "</td>";
$htmlAry['formBody'] .= "</tr>";
$htmlAry['formBody'] .= "<tr>";
    $htmlAry['formBody'] .= "<td class='field_title'>".$Lang['eReportCardKG']['Management']['ViewEditPeriodSetting']['StartDate']." (".$Lang['eReportCardKG']['Management']['ViewEditPeriodSetting']['ViewReportPeriod'].")</td>";
    $htmlAry['formBody'] .= "<td>".$indexVar["libreportcard_ui"]->GET_DATE_PICKER("viewStart", $viewStartTime[0])." ".$indexVar["libreportcard_ui"]->Get_Time_Selection("viewStart", $viewStartTime[1])."</td>";
$htmlAry['formBody'] .= "</tr>";
$htmlAry['formBody'] .= "<tr>";
    $htmlAry['formBody'] .= "<td class='field_title'>".$Lang['eReportCardKG']['Management']['ViewEditPeriodSetting']['EndDate']." (".$Lang['eReportCardKG']['Management']['ViewEditPeriodSetting']['ViewReportPeriod'].")</td>";
    $htmlAry['formBody'] .= "<td>";
        $htmlAry['formBody'] .= $indexVar["libreportcard_ui"]->GET_DATE_PICKER("viewEnd", $viewEndTime[0])." ".$indexVar["libreportcard_ui"]->Get_Time_Selection("viewEnd", $viewEndTime[1]);
        $htmlAry['formBody'] .= $indexVar["libreportcard_ui"]->Get_Form_Warning_Msg("view_Warn", $Lang["General"]["WarningArr"]["EndDateCannotEarlierThanStartDate"], "warningMsgDiv", $display=false);
    $htmlAry['formBody'] .= "</td>";
$htmlAry['formBody'] .= "</tr>";
$htmlAry['formBody'] .= "</tbody>";

$htmlAry['hiddenField'] = '';
$htmlAry['hiddenField'] = "<input type='hidden' name='YearTermID' value='$yearTermID' />";

# Button
$htmlAry['btn_submit'] = $indexVar['libreportcard_ui']->GET_ACTION_BTN($button_submit, "submit");
$htmlAry['btn_back'] = $indexVar['libreportcard_ui']->GET_ACTION_BTN($Lang['ePost']['Button']['Back'], "button", "self.location='?task=mgmt.view_edit_period_setting.list'");

?>