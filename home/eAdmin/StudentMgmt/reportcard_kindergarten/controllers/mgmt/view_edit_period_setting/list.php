<?php
// editing by
/*
 *  Date: 2019-02-14 Philips
 *          - Create file
 */

### Cookies Setting
// $arrCookies = array();
// if(isset($clearCoo) && $clearCoo == 1) {
//     clearCookies($arrCookies);
// } else {
//     updateGetCookies($arrCookies);
// }

// Page Title
$TAGS_OBJ[] = array($Lang['eReportCardKG']['Management']['ViewEditPeriodSetting']['Title']);
$indexVar['libreportcard_ui']->Echo_Module_Layout_Start($indexVar['returnMsgLang']);

### Table action buttons
$btnAry = array();
$btnAry[] = array('edit', 'javascript: checkEdit();');
$htmlAry['dbTableActionBtn'] = $indexVar['libreportcard_ui']->Get_DBTable_Action_Button_IP25($btnAry);

# Get Period Settings
// $inputScoreAry = $indexVar['libreportcard']->find_INPUT_SCORE_PERIOD_SETTING();
// $viewReportAry = $indexVar['libreportcard']->find_VIEW_REPORT_PERIOD_SETTING();

# Get Semester
$dataAry = array();
$TermIDAry = array();
$YearTermAry = getSemesters($indexVar["libreportcard"]->Get_Active_AcademicYearID());
foreach ($YearTermAry as $thisTermId => $thisTermName) {
    $dataAry[$thisTermId] = $thisTermName;
    $TermIDAry[] = $thisTermId;
}

$column_list = "<thead>";
$column_list .= "<tr>";
    $column_list .= "<th class='tabletop tabletopnolink' width='5%' rowspan='2'>". $Lang['eReportCardKG']['Management']['ViewEditPeriodSetting']['Term'] ."</td>\n";
    $column_list .= "<th class='tabletop tabletopnolink' width='47%' colspan='3'>". $Lang['eReportCardKG']['Management']['ViewEditPeriodSetting']['InputScorePeriod'] ."</td>\n";
    $column_list .= "<th class='tabletop tabletopnolink' width='47%' colspan='3'>". $Lang['eReportCardKG']['Management']['ViewEditPeriodSetting']['ViewReportPeriod'] ."</td>\n";
    $column_list .= "<th class='tabletop tabletopnolink' width='1' rowspan='2'>&nbsp;</td>\n";
$column_list .= "</tr>";
$column_list .= "<tr>";
    $column_list .= "<th class='tabletop tabletopnolink'>".$Lang['eReportCardKG']['Management']['ViewEditPeriodSetting']['StartDate']."</th>";
    $column_list .= "<th class='tabletop tabletopnolink'>".$Lang['eReportCardKG']['Management']['ViewEditPeriodSetting']['EndDate']."</th>";
    $column_list .= "<th class='tabletop tabletopnolink'>". $Lang['eReportCardKG']['Management']['ViewEditPeriodSetting']['ModifiedDate'] ."</td>\n";
    $column_list .= "<th class='tabletop tabletopnolink'>".$Lang['eReportCardKG']['Management']['ViewEditPeriodSetting']['StartDate']."</th>";
    $column_list .= "<th class='tabletop tabletopnolink'>".$Lang['eReportCardKG']['Management']['ViewEditPeriodSetting']['EndDate']."</th>";
    $column_list .= "<th class='tabletop tabletopnolink'>". $Lang['eReportCardKG']['Management']['ViewEditPeriodSetting']['ModifiedDate'] ."</td>\n";
$column_list .= "</tr>";
$column_list .= "</thead>";

// for($i=0; $i<sizeof($inputScoreAry); $i++)
// {
//     $tableBody .= "<tr>";
//     $tableBody .= "<td>" . $dataAry[$inputScoreAry[$i]['YearTermID']] . "</td>";
//         $tableBody .= "<td>" . $inputScoreAry[$i]['StartDate'] . "</td>";
//         $tableBody .= "<td>" . $inputScoreAry[$i]['EndDate'] . "</td>";
//         $tableBody .= "<td>" . $inputScoreAry[$i]['DateModified'] . "</td>";
//         $tableBody .= "<td>" . $viewReportAry[$i]['StartDate'] . "</td>";
//         $tableBody .= "<td>" . $viewReportAry[$i]['EndDate'] . "</td>";
//         $tableBody .= "<td>" . $viewReportAry[$i]['DateModified'] . "</td>";
//         $tableBody .= "<td> <input type='checkbox' class='checkbox' value='" . $inputScoreAry[$i]['YearTermID'] . "' /> </td>";
//     $tableBody .= "</tr>";
    
//     $tiIndex = array_search($inputScoreAry[$i]['YearTermID'], $TermIDAry);
//     unset($TermIDAry[$tiIndex]);
// }
$tableBody = "<tbody>";
if(!empty($TermIDAry))
{
    foreach($TermIDAry as $tId)
    {
        $inputScoreAry = $indexVar['libreportcard']->find_INPUT_SCORE_PERIOD_SETTING($tId);
        $viewReportAry = $indexVar['libreportcard']->find_VIEW_REPORT_PERIOD_SETTING($tId);
        
        if(!empty($inputScoreAry))
        {
            $tableBody .= "<tr>";
                $tableBody .= "<td>" . $dataAry[$tId] . "</td>";
                $tableBody .= "<td>" . $inputScoreAry[0]['StartDate'] . "</td>";
                $tableBody .= "<td>" . $inputScoreAry[0]['EndDate'] . "</td>";
                $tableBody .= "<td>" . $inputScoreAry[0]['DateModified'] . "</td>";
                $tableBody .= "<td>" . $viewReportAry[0]['StartDate'] . "</td>";
                $tableBody .= "<td>" . $viewReportAry[0]['EndDate'] . "</td>";
                $tableBody .= "<td>" . $viewReportAry[0]['DateModified'] . "</td>";
                $tableBody .= "<td> <input type='checkbox' class='checkbox' value='" . $tId. "' /> </td>";
            $tableBody .= "</tr>";
        }
        else
        {
            $tableBody .= "<tr>";
                $tableBody .= "<td>" . $dataAry[$tId] . "</td>";
                $tableBody .= "<td colspan='6' align='center'>" . $i_no_record_exists_msg . "</td>";
                $tableBody .= "<td> <input type='checkbox' class='checkbox' value='" . $tId . "' /> </td>";
            $tableBody .= "</tr>";
        }
    }
}
$tableBody .= "</tbody>";

$htmlAry['table'] = "<table class='common_table_list_v30'>" . $column_list . $tableBody . "</table>";

// $htmlAry['debugTable'] = '<table>';
// $htmlAry['debugTable'] .= '<tbody>';
// $htmlAry['debugTable'] .= "<tr>";
// $htmlAry['debugTable'] .= "<td>" . $dataAry[$inputScoreAry[0]['YearTermID']] . "</td>";
// $htmlAry['debugTable'] .= "<td>".$indexVar['libreportcard']->isInputScorePeriod($inputScoreAry[0]['YearTermID'])."</td>";
// $htmlAry['debugTable'] .= "<td>".$indexVar['libreportcard']->isViewReportPeriod($inputScoreAry[0]['YearTermID'])."</td>";
// $htmlAry['debugTable'] .= "</tr>";
// $htmlAry['debugTable'] .= "<tr>";
// $htmlAry['debugTable'] .= "<td>" . $dataAry[$inputScoreAry[1]['YearTermID']] . "</td>";
// $htmlAry['debugTable'] .= "<td>".$indexVar['libreportcard']->isInputScorePeriod($inputScoreAry[1]['YearTermID'])."</td>";
// $htmlAry['debugTable'] .= "<td>".$indexVar['libreportcard']->isViewReportPeriod($inputScoreAry[1]['YearTermID'])."</td>";
// $htmlAry['debugTable'] .= "</tr>";
// $htmlAry['debugTable'] .= '</table>';
// $htmlAry['debugTable'] .= '</tbody>';

?>