<?
// Using: 
/*
 * 	Date: 2019-10-11 Philips
 * 			- Added general warning box
 * 
 * 	Date: 2017-12-19 (Bill)
 * 			Create file
 */

### Page Title
$TAGS_OBJ[] = array($Lang["eReportCardKG"]["Setting"]["LanguageBehavior"]["Title"]);
$indexVar["libreportcard_ui"]->Echo_Module_Layout_Start();

### Main Paga Data
if ($_GET["isEdit"] && $topicCatIdAry[0] > 0)
{
	$TopicCatArr = $indexVar["libreportcard"]->getTopicCategory("", $topicCatIdAry[0]);
	$TopicCatArr = $TopicCatArr[0];
	$Code 	 = $TopicCatArr["Code"];
	$CatID 	 = $TopicCatArr["CatType"];
	$Name_CH = $TopicCatArr["NameCh"];
	$Name_EN = $TopicCatArr["NameEn"];
	
	$navigation = $Lang["Btn"]["Edit"];
}
else
{
	$navigation = $Lang["Btn"]["New"];
}
### Main Paga Data END

### Navigation
$navigationAry[] = array($Lang["eReportCardKG"]["Setting"]["LanguageBehavior"]["Title"],"index.php?task=settings.language_behavior.list");
$navigationAry[] = array($navigation);
$htmlAry["navigation"] = $indexVar["libreportcard_ui"]->GET_NAVIGATION_IP25($navigationAry);

### Category Type Selection
$catTypeAry = array();
$catTypeAry[] = array(TOPIC_CAT_SUBJECT, $Lang["eReportCardKG"]["Setting"]["LanguageBehavior"]["Category1"]);
$catTypeAry[] = array(TOPIC_CAT_PERSONAL, $Lang["eReportCardKG"]["Setting"]["LanguageBehavior"]["Category2"]);
$htmlAry["categorySelection"] = getSelectByArray($catTypeAry, " name='CatID' id='CatID' class='requiredField' " , $CatID, 0, 0);

### Warning box
$WarningBox = $indexVar["libreportcard_ui"]->Get_Warning_Message_Box('<font color="red">'.$Lang['General']['Tips'].'</font>', $Lang['eReportCardKG']['General']['PleaseConfirmChanges']);
?>