<?php
// Using: 
/*
 * 	Date: 2017-12-19 (Bill)
 * 			Create file
 */

### Cookies Setting
$arrCookies = array();
$arrCookies[] = array("eRCkg_settings_language_behavior_CatID", "CatID");
if(isset($clearCoo) && $clearCoo == 1) {
	clearCookies($arrCookies);
}
else {
	updateGetCookies($arrCookies);
}

### Page Title
$TAGS_OBJ[] = array($Lang["eReportCardKG"]["Setting"]["LanguageBehavior"]["Title"]);

### Message
if($success) {
	$returnMsg = $Lang["General"]["ReturnMessage"]["UpdateSuccess"];
}
else{
	$returnMsg = "";
}
$indexVar["libreportcard_ui"]->Echo_Module_Layout_Start($returnMsg);

### ToolBar Action Buttons
$btnAry = array();
$btnAry[] = array("new", "javascript: goNew();");
$htmlAry["contentTool"] = $indexVar["libreportcard_ui"]->Get_Content_Tool_By_Array_v30($btnAry);

### DB Table Action Buttons
$subBtnAry = array();
$subBtnAry[] = array("edit", "javascript: goEdit();");
$subBtnAry[] = array("delete", "javascript: goDelete();");
$htmlAry["dbTableActionBtn"] = $indexVar["libreportcard_ui"]->Get_DBTable_Action_Button_IP25($subBtnAry);

### Category Type Selection
$catTypeAry = array();
$catTypeAry[] = array(TOPIC_CAT_SUBJECT, $Lang["eReportCardKG"]["Setting"]["LanguageBehavior"]["Category1"]);
$catTypeAry[] = array(TOPIC_CAT_PERSONAL, $Lang["eReportCardKG"]["Setting"]["LanguageBehavior"]["Category2"]);
$htmlAry["categorySelection"] = getSelectByArray($catTypeAry, " name='CatID' id='CatID' onChange='loadDataTable()'" , $CatID, 0, 0);

### Required JS CSS - Drag and Drop Table Rows
echo $indexVar["libreportcard_ui"]->Include_JS_CSS();
?>