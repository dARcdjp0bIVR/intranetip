<?php 
// Using: 
/*
 * 	Date: 2017-12-19 (Bill)
 * 			Create file
 */

### Data Setting
$_topicCatId = $_POST["topicCatId"] ? $_POST["topicCatId"] : "";

$_dataAry = array();
$_dataAry["Code"] = $_POST["Code"];
$_dataAry["NameEn"] = $_POST["EN_Name"];
$_dataAry["NameCh"] = $_POST["CH_Name"];
$_dataAry["CatType"] = $_POST["CatID"];
$_dataAry["IsDeleted"] = $_POST["goDelete"];
if($_topicCatId == "") {
	$_dataAry["DisplayOrder"] = $indexVar["libreportcard"]->getTopicCategoryLastDisplayOrder($_POST["CatID"]) + 1;
}
$indexVar["libreportcard"]->updateTopicCategory($_topicCatId, $_dataAry, $updateLastModified=true);

header("Location: /home/eAdmin/StudentMgmt/reportcard_kindergarten/index.php?task=settings.language_behavior.list&success=1");
?>