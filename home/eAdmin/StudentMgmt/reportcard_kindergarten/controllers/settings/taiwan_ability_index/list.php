<?php
// editing by 
/*
 * 
 */

// Include DB Table
include_once($indexVar['thisBasePath']."includes/libdbtable.php");
include_once($indexVar['thisBasePath']."includes/libdbtable2007a.php");

// Update Result Message
if($success){
	$returnMsg = $Lang['General']['ReturnMessage']['UpdateSuccess'];
}
else if($success==="0"){
	$returnMsg = $Lang['General']['ReturnMessage']['UpdateUnsuccess'];
}

// Page Title
$TAGS_OBJ[] = array($Lang['eReportCardKG']['Setting']['TaiwanAbilityIndex']['Title']);
$indexVar['libreportcard_ui']->Echo_Module_Layout_Start($returnMsg);

// DB Table Settings
if (isset($ck_page_size) && $ck_page_size != "") {
	$page_size = $ck_page_size;
}
$field = ($field=='')? 0 : $field;
$order = ($order=='')? 1 : $order;
$page = ($page=='')? 1 : $page;
$pos = 0;

### DB table action buttons
$btnAry = array();
$btnAry[] = array('edit', 'javascript: checkEdit();');
$htmlAry['dbTableActionBtn'] = $indexVar['libreportcard_ui']->Get_DBTable_Action_Button_IP25($btnAry);

// Initiate DB Table
$li = new libdbtable2007($field, $order, $pageNo);
$li->sql = "SELECT Code, Name, CONCAT('<input type=\'checkbox\' class=\'checkbox\' name=\'CatID[]\' id=\'CatID_', CatID, '\' value=', CatID,'>') as edit_box FROM ".$indexVar['thisDbName'].".RC_ABILITY_INDEX_CATEGORY WHERE Type = ".TAIWAN_ABILITY;
$li->IsColOff = "IP25_table";
$li->field_array = array("Code", "Name", "edit_box");
$li->column_array = array(0, 0, 0);
$li->wrap_array = array(0, 0, 0);
$li->no_col = count($li->field_array)+1;
$li->column_list .= "<th class='tabletop tabletopnolink' width='1'>#</td>\n";
$li->column_list .= "<th class='tabletop tabletopnolink' width='40%'>".$li->column($pos++, $Lang['eReportCardKG']['Setting']['TaiwanAbilityIndex']['Code'])."</td>\n";
$li->column_list .= "<th class='tabletop tabletopnolink' width='60%'>".$li->column($pos++, $Lang['eReportCardKG']['Setting']['TaiwanAbilityIndex']['Name'])."</td>\n";
$li->column_list .= "<th class='tabletop tabletopnolink' width='1'>&nbsp;</td>\n";

// GET DB Table Content
$htmlAry['dataTable'] = $li->display();

// DB Table related Hidden Fields
$htmlAry['hiddenField'] = "";
$htmlAry['hiddenField'] .= "<input type='hidden' name='pageNo' value='".$li->pageNo."'>";
$htmlAry['hiddenField'] .= "<input type='hidden' name='order' value='1'>";
$htmlAry['hiddenField'] .= "<input type='hidden' name='field' value='".$li->field."'>";
$htmlAry['hiddenField'] .= "<input type='hidden' name='page_size_change' value=''>";
$htmlAry['hiddenField'] .= "<input type='hidden' name='numPerPage' value='".$li->page_size."'>";
?>