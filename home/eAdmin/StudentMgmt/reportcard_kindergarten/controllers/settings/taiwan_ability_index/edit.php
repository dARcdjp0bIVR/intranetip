<?php
// editing by 
/*
 * Change Log:
 * 
 * 	Date: 2019-10-11 Philips
 * 			- Added general warning box
 * 
 */

# Page Title
$TAGS_OBJ[] = array($Lang['eReportCardKG']['Setting']['TaiwanAbilityIndex']['Title']);
$indexVar['libreportcard_ui']->Echo_Module_Layout_Start($indexVar['returnMsgLang']);

# Get Category / Index Info
$sql = "SELECT Code, Name FROM ".$indexVar['thisDbName'].".RC_ABILITY_INDEX_CATEGORY WHERE Type = ".TAIWAN_ABILITY." AND CatID = '".$_GET["CodeID"]."'";
$result = $indexVar['libreportcard']->returnArray($sql);
list($IndexCode, $IndexName) = $result[0];
$IndexID = $_GET["CodeID"];

# NavigationBar
$navigationAry[] = array($Lang['eReportCardKG']['Setting']['TaiwanAbilityIndex']['Title'], 'index.php?task=settings.taiwan_ability_index.list');
$navigationAry[] = array($Lang['Btn']['Edit']);
$NavigationBar = $indexVar['libreportcard_ui']->GET_NAVIGATION_IP25($navigationAry);

### Warning box
$WarningBox = $indexVar["libreportcard_ui"]->Get_Warning_Message_Box('<font color="red">'.$Lang['General']['Tips'].'</font>', $Lang['eReportCardKG']['General']['PleaseConfirmChanges']);
?>