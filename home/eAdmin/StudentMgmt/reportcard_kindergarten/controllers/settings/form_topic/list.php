<?php
// editing by 
/*
 * Change Log:
 * Date 2019-10-31 Bill class teacher > view own class level only
 * Date 2017-02-09 Villa Add Cookie Setting
 * Date 2017-02-06 Villa Add Check All Function
 */

### Cookies setting
$arrCookies = array();
$arrCookies[] = array("eRCkg_Settings_learningZone_YearID","YearID");
if(isset($clearCoo) && $clearCoo == 1) {
	clearCookies($arrCookies);
} else {
	updateGetCookies($arrCookies);
}

if($success){
	$returnMsg = $Lang['General']['ReturnMessage']['UpdateSuccess'];
}else{
	$returnMsg = "";
}

# Page Title
$TAGS_OBJ[] = array($Lang['eReportCardKG']['Setting']['FormTopic']['Title']);
if($success){
	$returnMsg = $Lang['General']['ReturnMessage']['UpdateSuccess'];
}else{
	$returnMsg = "";
}
$indexVar['libreportcard_ui']->Echo_Module_Layout_Start($returnMsg);
// $indexVar['libreportcard_ui']->Echo_Module_Layout_Start($indexVar['returnMsgLang']);

# Action Buttons
// New
$btnAry = array();
$btnAry[] = array('new', 'javascript: goNew();');
$htmlAry['contentTool'] = $indexVar['libreportcard_ui']->Get_Content_Tool_By_Array_v30($btnAry);
// Edit & Delete
$btnAry = array();
$btnAry[] = array('edit', 'javascript: Edit();');
$btnAry[] = array('delete', 'javascript: Delete();');
$htmlAry['dbTableActionBtn'] = $indexVar['libreportcard_ui']->Get_DBTable_Action_Button_IP25($btnAry);

# Year Selection
$YearSelection = $indexVar['libreportcard_ui']->Get_Year_Selection($YearID);

# Table Data
$table = '<table class="common_table_list_v30" id="ContentTable">';
$table .= '<thead>';
$table .= '<tr>';
$table .= '<th style="width:3%;">#</th>';
$table .= '<th style="width:10%;">'.$Lang['eReportCardKG']['Setting']['Code'].'</th>';
$table .= '<th style="width:15%;">'.$Lang['eReportCardKG']['Setting']['Name'].'</th>';
$table .= '<th style="width:15%;">'.$Lang['eReportCardKG']['Setting']['ApplyForm'].'</th>';
$table .= '<th style="width:3%;"><input type="checkbox" id="checkmaster" name="checkmaster" onclick="CheckAll()"></th>';
$table .= '</tr>';
$table .= '</thead>';
$table .= '<tbody>';

$i = 1;

// loop Topics
$filterByClassTeacher = !$indexVar["libreportcard"]->IS_KG_ADMIN_USER() && $indexVar["libreportcard"]->IS_KG_CLASS_TEACHER();
$topics = $indexVar['libreportcard']->Get_Form_Topic_Record("", $YearID, $filterByClassTeacher);
if(!empty($topics)){
	foreach ((array)$topics as $this_topic){
		$table .= '<tr>';
		$table .= '<td><span class="rowNumSpan">'.$i.'</span></td>';
		$table .= '<td><span class="rowNumSpan">'.$this_topic['TopicCode'].'</span></td>';
		$table .= '<td><a class="tablelink" href="javascript:goEdit('.$this_topic['TopicID'].');">'.Get_Lang_Selection($this_topic['TopicNameB5'], $this_topic['TopicNameEN']).'</a></td>';
		$table .= '<td><span class="rowNumSpan">'.$this_topic['YearName'].'</span></td>';
		$table .= '<td><input type="checkbox" class="checkbox" name="checkbox[]" id="checkbox_'.$this_topic['TopicID'].'" value="'.$this_topic['TopicID'].'"></td>';
		$table .= '</tr>';
		$i++;
	}
}else{ //no form topic data 
	$table .= '<tr>';
	$table .= '<td colspan="5" align="center">';
	$table .= $Lang['General']['NoRecordAtThisMoment'] ;
	$table .= '</td>';
	$table .= '</tr>';
}
$table .= '</tbody>';
$table .= '</table>';

?>