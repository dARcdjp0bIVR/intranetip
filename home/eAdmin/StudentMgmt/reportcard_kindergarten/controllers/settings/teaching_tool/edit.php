<?php
// editing by 
/*
 * 	Date: 2019-10-11 Philips
 * 			- Added general warning box
 * 
 * 	Date: 2019-09-24 Philips Added Chapter Column
 * 	Date: 2017-10-17 Bill	Support Category Selection
 * 	Date: 2017-10-09 Bill	Use timestamp to prevent display cached images	[2017-0929-1827-43096]
 */

### Page Title
$TAGS_OBJ[] = array($Lang['eReportCardKG']['Setting']['TeachingTool']['Title']);
$indexVar['libreportcard_ui']->Echo_Module_Layout_Start($indexVar['returnMsgLang']);
echo $indexVar['libreportcard_ui']->Include_Thickbox_JS_CSS();

## Taiwan Category
$temp = $indexVar['libreportcard']->Get_Taiwan_Category();
$TWCata = BuildMultiKeyAssoc($temp, "CatID");

### Main Paga Data
if ($_GET['isEdit'])
{
	$data = $indexVar['libreportcard']->Get_Equipment_Record('', $codeID);
	$code = $data[0]['Code'];
	$nameEn = $data[0]['EN_Name'];
	$nameCh = $data[0]['CH_Name'];
	$photoPath = $data[0]['PhotoPath'];
	$yearID = $data[0]['YearID'];
	$yearName = $data[0]['YearName'];
	$toolCatID = $data[0]['CategoryID'];
	$zoneID = $data[0]['ZoneID'];
	$remarks = $data[0]['Remarks'];
	$chapter = $data[0]['Chapter'];
	
	$temp = $indexVar['libreportcard']->Get_Taiwan_Category_Fm_Mapping($codeID);
	$temp = Get_Array_By_Key($temp, 'CatID');
	$catagory = implode(',', $temp);
	
	if($photoPath) { 	// Have set photo
		$IsPhotoDispaly = true;
		$photoDispaly = "<img src='".$indexVar['thisImage'].$photoPath."?t=".time()."' style='width:300px'>";
	}
	else { 				// Not yet set photo
		$IsPhotoDispaly = false;
		$photoDispaly = ''; 
	}
	$navigation = $Lang['Btn']['Edit'];
}
else{
	$IsPhotoDispaly = false;
	$photoDispaly = '';
	$navigation = $Lang['Btn']['New'];
}

$YearSelection = $indexVar['libreportcard_ui']->Get_Year_Selection($yearID, 'YearOnChange()');
$CategorySelection = $indexVar['libreportcard_ui']->Get_Tool_Cat_Selection($toolCatID, 'ToolCatOnChange()');

$navigationAry[] = array($Lang['eReportCardKG']['Setting']['TeachingTool']['Title'],'index.php?task=settings.teaching_tool.list');
$navigationAry[] = array($navigation);
$NavigationBar = $indexVar['libreportcard_ui']->GET_NAVIGATION_IP25($navigationAry);
### Main Paga Data END 

### ThickBox Related Start
$thickBox = '';
$thickBox .= '<div id="CataThickBox" style="display:none">';
	$CataData = $indexVar['libreportcard']-> Get_Taiwan_Category($YearID);
	$thickBox .= $indexVar['libreportcard_ui']->Get_Taiwan_Category_Selection($selectTWCat, "Catagory_OnChange()");
	$thickBox .= "<br>";
	$thickBox .= "<br>";
	$thickBox .= "<br>";
	$thickBox .= "<div id=CataSelectionBox align='center'>";
		$thickBox .= "<select multiple name='cata[]' id='cata' size='10'>";
		foreach ($CataData as $_CataData){
			$thickBox .= "<option value= {$_CataData['CatID']}>";
			$thickBox .= "[".$_CataData['Code']."]"."&nbsp".$_CataData['Name'];
			$thickBox .= "</option>";
		}
		$thickBox .= "</select>";
	$thickBox .= "</div>";
	$thickBox .= "<br>";
	$thickBox .= "<div align='center'>";
		$thickBox .= '<input type="button" value="呈送" class="formbutton_v30 print_hide " onclick="ThickBoxGoSubmit()" id="submitBtn" name="submitBtn">		<input type="button" value="返回" class="formbutton_v30 print_hide " onclick="ThickBoxClose()" id="backBtn" name="backBtn">		<p class="spacer"></p>';
	$thickBox .= "</div>";
$thickBox .= "</div>";

$thickBox .= "<select multiple id='cata_hidden' size='10' style='display:none;'>";
foreach ($CataData as $_CataData){
	$thickBox .= "<option value= {$_CataData['CatID']}>";
	$thickBox .= "[".$_CataData['Code']."]"."&nbsp".$_CataData['Name'];
	$thickBox .= "</option>";
}
$thickBox .= "</select>";
### ThickBox Related End

# Build Thickbox
$thickBoxHeight = 300;
$thickBoxWidth = 620;

$AddCata = $indexVar['libreportcard_ui']->Get_Thickbox_Link($thickBoxHeight, $thickBoxWidth, "",
		$Lang['eReportCardKG']['Setting']['AbilityIndex'], "", 'CataThickBox', $Content='', 'pushMessageButton');

### Warning box
$WarningBox = $indexVar["libreportcard_ui"]->Get_Warning_Message_Box('<font color="red">'.$Lang['General']['Tips'].'</font>', $Lang['eReportCardKG']['General']['PleaseConfirmChanges']);
?>