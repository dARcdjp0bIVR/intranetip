<?php 
/*
 * Change Log:
 * Date 2019-09-17 Bill  Fixed: Image orientation problem
 * Date 2017-12-13 Bill  Fixed: incorrect Photo Path (".jpg") stored when add new Teaching Tool	[2017-1211-1715-10164]
 * Date 2017-11-08 Bill  Use Tool CodeID as Photo Name
 * Date 2017-10-17 Bill  Update Category
 * Date 2017-02-06 Villa Update the uploading image process - Resize the image size if > 500000 
 * Date 2017-01-25 Villa Open the file
 */

function handleImageOrientation($image_path, $image_type)
{
    # Create image object
    if($image_type == 'jpg' || $image_type == 'jpeg') {
        $image_obj = imagecreatefromjpeg($image_path);
    } else {
        $image_obj = imagecreatefrompng($image_path);
    }

    # Rotate image if orientation is incorrect
    if (function_exists('exif_read_data'))
    {
        $exif = @exif_read_data($image_path);
        if($exif && isset($exif['Orientation']))
        {
            $orientation = $exif['Orientation'];
            if($orientation != 1)
            {
                $deg = 0;
                switch ($orientation) {
                    case 3:
                        $deg = 180;
                        break;
                    case 6:
                        $deg = 270;
                        break;
                    case 8:
                        $deg = 90;
                        break;
                }
                if ($deg) {
                    $image_obj = imagerotate($image_obj, $deg, 0);
                }
            }
        }
    }

    # Create jpeg file
    if($image_type == 'jpg' || $image_type == 'jpeg') {
        imagejpeg($image_obj, $image_path, 100);
    } else {
        imagepng($image_obj, $image_path, 100);
    }

    //return $image_obj;
}

### Data Setting
$CodeID = $_POST["CodeID"];
// $Code = $_POST["Code"];
$EN_Name = $_POST["EN_Name"];
$CH_Name = $_POST["CH_Name"]; 
$YearID = $_POST["YearID"];
$ToolCatID = $_POST["ToolCatID"];
$ZoneID = $_POST["ZoneID"];
$Remarks = $_POST["Remarks"];
$Chapter = $_POST["chapter"];

$target_dir = $indexVar["thisImage"];
if($_FILES["fileToUpload"]["tmp_name"]) {
	$indexVar["libfilesystem"]->folder_new($target_dir);
	
	$file_ext = $indexVar["libfilesystem"]->file_ext($_FILES["fileToUpload"]["name"]);
	$PhotoPath = basename($CodeID).$file_ext;
	//$target_file = $target_dir.$PhotoPath;
}
else {
	$PhotoPath = $_POST["PhotoPath"];
	$GoDelete = $_POST["goDelete"];
	
	### Delete Photo > Reset Photo Path
	if($GoDelete)
	{
		$target_file = $target_dir.$PhotoPath;
		if(file_exists($target_file)) {
		    $indexVar["libfilesystem"]->file_remove($target_file);
        }

		$PhotoPath = "";
	}
}

$CataID = $_POST["CataGoryID"];
$AbilityCatID = explode(",", $CataID);

### Update [Start]
//$CodeID_temp = $indexVar["libreportcard"]->Update_Equipment_Record($CodeID, $Code, $EN_Name, $CH_Name, $YearID, $PhotoPath, $Remarks, $ToolCatID, $ZoneID);
//$CodeID_temp = $indexVar["libreportcard"]->Update_Equipment_Record($CodeID, '', $EN_Name, $CH_Name, $YearID, $PhotoPath, $Remarks, $ToolCatID, $ZoneID);
$CodeID_temp = $indexVar["libreportcard"]->Update_Equipment_Record($CodeID, '', $EN_Name, $CH_Name, $YearID, $PhotoPath, $Remarks, $ToolCatID, $ZoneID, $Chapter);
$indexVar["libreportcard"]->Updata_Equipment_Cata_Mapping($CodeID_temp, $AbilityCatID);
### Update [End]

if($_FILES["fileToUpload"]["tmp_name"] && $CodeID_temp > 0)
{
	$max_width = 300;
	$max_height = 300;
	
	### File [Start]
	$uploadOk = 1;
	$target_file = $target_dir.basename($CodeID_temp).$file_ext;
	$imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);
	
	// Check if image file is a actual image or fake image
	$check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
	if($check !== false) {
		$uploadOk = 1;
	}
	else {
		$UploadError[] = "File is not an image.";
		$uploadOk = 0;
	}

	// Define as update process and delete the orginal one
	if (file_exists($target_file)) {
		$indexVar["libfilesystem"]->file_remove($target_file);
		// unlink($target_file);
	}

    if($imageFileType == "jpg" || $imageFileType == "jpeg" || $imageFileType == "png")
    {
        // Check file size
        // if oversize => resize the image size
        if ($_FILES["fileToUpload"]["size"] > 500000)
        {
            if($imageFileType == 'jpg' || $imageFileType == 'jpeg') {
                $image = imagecreatefromjpeg($_FILES["fileToUpload"]["tmp_name"]);
            } else {
                $image = imagecreatefrompng($_FILES["fileToUpload"]["tmp_name"]);
            }

            $old_width  = imagesx($image);
            $old_height = imagesy($image);
            $scale      = min($max_width/$old_width, $max_height/$old_height);
            $new_width  = ceil($scale*$old_width);
            $new_height = ceil($scale*$old_height);

            // Create new empty image
            $new = imagecreatetruecolor($new_width, $new_height);

            // Keep the transparent background for png
            imagealphablending($new, false);
            imagesavealpha($new, true);
            $transparent = imagecolorallocatealpha($new, 255, 255, 255, 127);
            imagefilledrectangle($new, 0, 0, $old_width, $old_height, $transparent);

            // Resize old image into new
            imagecopyresampled($new, $image, 0, 0, 0, 0, $new_width, $new_height, $old_width, $old_height);

            // Catch the imagedata
            imagepng($new, $_FILES["fileToUpload"]["tmp_name"]); //update the fileToupload with the resize image

            // Destroy resources
            imagedestroy($image);
            imagedestroy($new);
        }
    }
	
	// Allow certain file formats
	if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif") {
		$UploadError[] = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
		$uploadOk = 0;
	}
	
	// Check if $uploadOk is set to 0 by an error
	if ($uploadOk == 0) {
		$UploadError[] = "Sorry, your file was not uploaded.";
	}
	// if everything is ok, try to upload file
	else {
		if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
			// echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";

            //if($_FILES["fileToUpload"]["tmp_name"] && ($imageFileType == "jpg" || $imageFileType == "jpeg" || $imageFileType == "png")) {
            //    handleImageOrientation($target_file, $imageFileType);
            //}
		}
		else{
			// $uploadOk = 0;
			$UploadError[] = "Sorry, there was an error uploading your file.";
		}
	}
	### File [End]
}

header("Location: /home/eAdmin/StudentMgmt/reportcard_kindergarten/index.php?task=settings.teaching_tool.list&success=1");
?>