<?php

// Include DB Table
include_once($indexVar['thisBasePath']."includes/libdbtable.php");
include_once($indexVar['thisBasePath']."includes/libdbtable2007a.php"); 

### Cookies handling
$arrCookies = array();
$arrCookies[] = array("kg_settings_admin_group_member_list_page_size", "numPerPage");
$arrCookies[] = array("kg_settings_admin_group_member_list_page_number", "pageNo");
$arrCookies[] = array("kg_settings_admin_group_member_list_page_order", "order");
$arrCookies[] = array("kg_settings_admin_group_member_list_page_field", "field");
$arrCookies[] = array("kg_settings_admin_group_member_list_page_keyword", "Keyword_Member_List");
$Keyword_Member_List = (isset($_POST['Keyword_Member_List']))? $_POST['Keyword_Member_List'] : $_COOKIE['Keyword_Member_List'];

if(isset($clearCoo) && $clearCoo == 1)
{
    clearCookies($arrCookies);
    $ck_settings_admin_group_member_list_page_size = '';
    $Keyword_Member_List = '';
}
else
{
    updateGetCookies($arrCookies);
}

$field = ($field=='')? 0 : $field;
$order = ($order=='')? 1 : $order;
$page = ($page=='')? 1 : $page;

$AdminGroupID = $_REQUEST['AdminGroupID'];

# Tag information
$MODULE_OBJ = $indexVar['libreportcard']->GET_MODULE_OBJ_ARR();
$TAGS_OBJ[] = array($Lang['eReportCardKG']['Setting']['AdminGroup']['Title']);

# Return msg
$ReturnMsgKey = $_REQUEST['ReturnMsgKey'];
$ReturnMsg = $Lang['eReportCardKG']['Settings']['AdminGroup']['ReturnMessage'][$ReturnMsgKey];
$indexVar['libreportcard_ui']->LAYOUT_START($ReturnMsg);

$htmlAry['thickbox'] = $indexVar['libreportcard_ui']->Include_JS_CSS();
//echo $lreportcard_ui->Get_Setting_Admin_Group_Member_List_UI($AdminGroupID, $pageNo, $order, $field, $Keyword_Member_List);

# Navigation
$PAGE_NAVIGATION[] = array($Lang['eReportCardKG']['Setting']['AdminGroup']['AdminGroupList'], "?task=settings.admin_group.list");
$PAGE_NAVIGATION[] = array($Lang['eReportCardKG']['Setting']['AdminGroup']['MemberList'], "");
$htmlAry['PAGENAV'] = $indexVar['libreportcard_ui']->GET_NAVIGATION_IP25($PAGE_NAVIGATION);

if ($FromNew == 1)
{
    $PAGE_NAVIGATION[] = array($Lang['Btn']['New'], "");
    
    ### Step Table
    $STEPS_OBJ[] = array($Lang['eReportCardKG']['Setting']['AdminGroup']['NewAdminGroupStep'][1], 0);
    $STEPS_OBJ[] = array($Lang['eReportCardKG']['Setting']['AdminGroup']['NewAdminGroupStep'][2], 1);
    $htmlAry['StepTable'] = $indexVar['libreportcard_ui']->GET_STEPS_IP25($STEPS_OBJ);
}
$AdminGroupInfoArr = $indexVar['libreportcard']->Get_Admin_Group_Info($AdminGroupID);

//$htmlAry['InfoTable'] = "";
$x = '<table width="100%" cellpadding="2" class="form_table_v30">'."\n";
    $x .= '<col class="field_title">';
    $x .= '<col class="field_c">';
    # Code
    $x .= '<tr>'."\n";
        $x .= '<td class="field_title">'.$Lang['eReportCardKG']['Setting']['AdminGroup']['AdminGroupCode'].'</td>'."\n";
        $x .= '<td>'.$AdminGroupInfoArr[0]['AdminGroupCode'].'</td>'."\n";
    $x .= '</tr>'."\n";
    # Name
    $x .= '<tr>'."\n";
        $x .= '<td class="field_title">'.$Lang['eReportCardKG']['Setting']['AdminGroup']['AdminGroupName'].'</td>'."\n";
        $x .= '<td>'.Get_Lang_Selection($AdminGroupInfoArr[0]['AdminGroupNameCh'], $AdminGroupInfoArr[0]['AdminGroupNameEn']).'</td>'."\n";
    $x .= '</tr>'."\n";
$x .= '</table>'."\n";
$htmlAry['InfoTable'] = $x;

$htmlAry['addLink'] = $indexVar['libreportcard_ui']->GET_LNK_EDIT("javascript:addNewMember();")."\n";
//$htmlAry['btn_delete'] = '<a class="tool_delete" href="javascript:checkRemove(document.form1,\'UserIDArr[]\',\'member_delete.php\')">'.$Lang['Btn']['Delete'].'</a>'."\n";

//$htmlAry['table'] = "";
if (isset($kg_settings_admin_group_member_list_page_size) && $kg_settings_admin_group_member_list_page_size != "") $page_size = $kg_settings_admin_group_member_list_page_size;

$li = new libdbtable2007($field, $order, $page);
$sql = $indexVar['libreportcard']->Get_Admin_Group_Member_List_DBTable_Sql($AdminGroupID, $Keyword);
//debug_pr($sql);die();
$li->sql = $sql;
$li->IsColOff = "IP25_table";

$li->field_array = array("iu.EnglishName", "iu.ChineseName");
$li->column_array = array(0, 0, 0);
$li->wrap_array = array(0, 0, 0);

$pos = 0;
$li->column_list .= "<th width='1' class='tabletoplink'>#</td>\n";
$li->column_list .= "<th width='47%' >".$li->column_IP25($pos++, $Lang['General']['EnglishName'])."</th>\n";
$li->column_list .= "<th width='47%'>".$li->column_IP25($pos++, $Lang['General']['ChineseName'])."</th>\n";
//$li->column_list .= "<th width='1'>".$li->check("UserIDArr[]")."</td>\n";
$li->no_col = $pos + 1;

$htmlAry['table'] = $li->display();

$htmlAry['hidden'] = "";
$htmlAry['hidden'] .= '<input type="hidden" name="pageNo" value="'.$li->pageNo.'" />';
$htmlAry['hidden'] .= '<input type="hidden" name="order" value="'.$li->order.'" />';
$htmlAry['hidden'] .= '<input type="hidden" name="field" value="'.$li->field.'" />';
$htmlAry['hidden'] .= '<input type="hidden" name="page_size_change" value="" />';
$htmlAry['hidden'] .= '<input type="hidden" name="numPerPage" value="'.$li->page_size.'" />';
$htmlAry['hidden'] .= '<input type="hidden" id="AdminGroupID" name="AdminGroupID" value="'.$AdminGroupID.'" />';
$htmlAry['hidden'] .= '<input type="hidden" id="FromNew" name="FromNew" value="'.$FromNew.'" />';

# Back Button
$htmlAry['btn_back'] = $indexVar['libreportcard_ui']->GET_ACTION_BTN($Lang['Btn']['Back'], "button", $onclick="js_Back_To_Admin_Group_List()", $id="Btn_Back");
?>