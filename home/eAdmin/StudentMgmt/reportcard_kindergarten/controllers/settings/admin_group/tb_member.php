<?php
//$indexVar['libreportcard_ui']->LAYOUT_START();
global $image_path, $LAYOUT_SKIN;

$AdminGroupID = $_POST['AdminGroupID'];
$AdminGroupMemberInfo = $indexVar['libreportcard']->Get_Admin_Group_Member($AdminGroupID);
$AdminGroupInfoArr = $indexVar['libreportcard']->Get_Admin_Group_Info($AdminGroupID);

$htmlAry['js_css'] = $indexVar['libreportcard_ui']->Include_JS_CSS(array('autocomplete'));
//$lreportcard_ui->Get_Setting_Admin_Group_Step2_UI($AdminGroupID, $FromNew);

//$htmlAry['InfoTable'] = "";
$x = '<table width="100%" cellpadding="2" class="form_table_v30">'."\n";
    $x .= '<col class="field_title">';
    $x .= '<col class="field_c">';
    # Code
    $x .= '<tr>'."\n";
        $x .= '<td class="field_title">'.$Lang['eReportCardKG']['Setting']['AdminGroup']['AdminGroupCode'].'</td>'."\n";
        $x .= '<td>'.$AdminGroupInfoArr[0]['AdminGroupCode'].'</td>'."\n";
    $x .= '</tr>'."\n";
    # Name
    $x .= '<tr>'."\n";
        $x .= '<td class="field_title">'.$Lang['eReportCardKG']['Setting']['AdminGroup']['AdminGroupName'].'</td>'."\n";
        $x .= '<td>'.Get_Lang_Selection($AdminGroupInfoArr[0]['AdminGroupNameCh'], $AdminGroupInfoArr[0]['AdminGroupNameEn']).'</td>'."\n";
    $x .= '</tr>'."\n";
$x .= '</table>'."\n";
$htmlAry['InfoTable'] = $x;

### Choose Member Btn
$permitted_type = 1;
$htmlAry['btn_choose'] = $indexVar['libreportcard_ui']->GET_SMALL_BTN($Lang['Btn']['Select'], "button", "javascript:newWindow('".$PATH_WRT_ROOT."home/common_choose/index.php?fieldname=UserIDArr[]&page_title=SelectMembers&permitted_type=$permitted_type&DisplayGroupCategory=1', 9)");

$htmlAry['icon'] = '<img src="'.$image_path.'/'.$LAYOUT_SKIN.'/10x10.gif" width="10" height="1">';

$htmlAry['box_selection'] = $indexVar['libreportcard_ui']->GET_SELECTION_BOX($AdminGroupMemberInfo, 'name="UserIDArr[]" id="UserIDArr[]" class="select_studentlist" size="8" multiple="multiple"', "");
$htmlAry['btn_remove'] = $indexVar['libreportcard_ui']->GET_SMALL_BTN($Lang['Btn']['RemoveSelected'], "button", "javascript:checkOptionRemove(document.getElementById('UserIDArr[]'))");

### Submit Button
$htmlAry['btn_submit'] = $indexVar['libreportcard_ui']->GET_ACTION_BTN($Lang['Btn']['Submit'], "button", $onclick="js_submit()", $id="Btn_Submit");

$htmlAry['div_warning'] = $indexVar['libreportcard_ui']->Get_Thickbox_Warning_Msg_Div("MemberSelectionWarningDiv", '* '.$Lang['eRC_Rubrics']['SettingsArr']['AdminGroupArr']['WarningArr']['SelectionContainsMember']);

$htmlAry['hidden'] = '<input type="hidden" id="AdminGroupID" name="AdminGroupID" value="'.$AdminGroupID.'" />';
$htmlAry['hidden'] .= '<input type="hidden" id="isUpdate" name="isUpdate" value=1 />';
$htmlAry['hidden'] .= '<input type="hidden" id="FromNew" name="FromNew" value="'.$FromNew.'" />';
?>