<?php
# Get data
$Action = $_POST['Action'];
$AdminGroupID = $_POST['AdminGroupID'];
$AccessiableMenuArr = $_POST['AccessiableMenuArr'];

$DataArr = array();
$DataArr['AdminGroupCode'] = trim(urldecode(stripslashes($_POST['AdminGroupCode'])));
$DataArr['AdminGroupNameEn'] = trim(urldecode(stripslashes($_POST['AdminGroupNameEn'])));
$DataArr['AdminGroupNameCh'] = trim(urldecode(stripslashes($_POST['AdminGroupNameCh'])));

if($Action == "AdminGroupCode") {
    // ajax Part
    $chkCode = $indexVar['libreportcard']->Is_Admin_Group_Code_Valid($DataArr['AdminGroupCode'], $AdminGroupID);

    header('Content-type: application/json');
    echo json_encode(array("err" => ($chkCode ? 0 : 1 )));
} else {
    // non ajax part
    if($AdminGroupID=='') {
        // New group
        $AdminGroupID = $indexVar['libreportcard']->Add_Admin_Group($DataArr);
        if($AdminGroupID == 0){
            // Failed Situation
            $ReturnMsgKey = 'AddFailed';
        }
        $ReturnMsgKey = 'AddSuccess';
        $FromNew = 1;
        
        $NextPage = "?task=settings.admin_group.member";
    } else {
        // Edit group
        $Success = $indexVar['libreportcard']->Update_Admin_Group($AdminGroupID, $DataArr);
        $ReturnMsgKey = ($Success)? 'EditSuccess' : 'EditFailed';
        
        $NextPage = "?task=settings.admin_group.list";
    }
    
    // Update Access Right
    $Success = $indexVar['libreportcard']->Update_Admin_Group_Access_Right($AdminGroupID, $AccessiableMenuArr);

    // redirect to next Page
    $para = "AdminGroupID=$AdminGroupID&ReturnMsgKey=$ReturnMsgKey&FromNew=$FromNew";
    header("Location: $NextPage&$para");
}