<?php

// Include DB Table
include_once($indexVar['thisBasePath']."includes/libdbtable.php");
include_once($indexVar['thisBasePath']."includes/libdbtable2007a.php"); 

### Cookies handling
$arrCookies = array();
$arrCookies[] = array("kg_setting_admin_group_page_size", "numPerPage");
$arrCookies[] = array("kg_setting_admin_group_page_number", "pageNo");
$arrCookies[] = array("kg_setting_admin_group_page_order", "order");
$arrCookies[] = array("kg_setting_admin_group_page_field", "field");
$arrCookies[] = array("kg_setting_admin_group_page_keyword", "Keyword");

$numPerPage = (isset($_POST['numPerPage']))? $_POST['numPerPage'] : $_COOKIE['numPerPage'];
$pageNo = (isset($_POST['pageNo']))? $_POST['pageNo'] : $_COOKIE['pageNo'];
$order = (isset($_POST['order']))? $_POST['order'] : $_COOKIE['order'];
$field = (isset($_POST['field']))? $_POST['field'] : $_COOKIE['field'];
$Keyword = (isset($_POST['Keyword']))? $_POST['Keyword'] : $_COOKIE['Keyword'];

if(isset($clearCoo) && $clearCoo == 1)
{
	clearCookies($arrCookies);

	$kg_settings_admin_group_page_size = '';
	$Keyword = '';
}
else
{
	updateGetCookies($arrCookies);
}
if (isset($kg_settings_admin_group_page_size) && $kg_settings_admin_group_page_size != "") $page_size = $kg_settings_admin_group_page_size;

$li = new libdbtable2007($field, $order, $page);
$li->sql = $indexVar['libreportcard']->Get_Admin_Group_DBTable_Sql($Keyword);
$li->IsColOff = "IP25_table";

$GroupNameField = Get_Lang_Selection('ag.AdminGroupNameCh', 'ag.AdminGroupNameEn');
$li->field_array = array("ag.AdminGroupCode", $GroupNameField, "Count(agu.UserID)", "ag.DateModified");
$li->column_array = array(0, 0, 0, 0, 0, 0);
$li->wrap_array = array(0, 0, 0, 0, 0, 0);

$pos = 0;
$li->column_list .= "<th width='1' class='tabletoplink'>#</td>\n";
$li->column_list .= "<th width='15%' >".$li->column_IP25($pos++, $Lang['General']['Code'])."</th>\n";
$li->column_list .= "<th width='40%'>".$li->column_IP25($pos++, $Lang['General']['Name'])."</th>\n";
$li->column_list .= "<th width='15%'>".$li->column_IP25($pos++, $Lang['eReportCardKG']['Setting']['AdminGroup']['NumOfMember'])."</th>\n";
$li->column_list .= "<th width='25%'>".$li->column_IP25($pos++, $Lang['eReportCardKG']['Setting']['AdminGroup']['ModifiedDate'])."</th>\n";
$li->column_list .= "<th width='1'>".$li->check("AdminGroupIDArr[]")."</td>\n";
$li->no_col = $pos + 2;

$htmlAry['table'] = $li->display();

$htmlAry['hidden'] = "";
$htmlAry['hidden'] .= '<input type="hidden" name="pageNo" value="'.$li->pageNo.'" />';
$htmlAry['hidden'] .= '<input type="hidden" name="order" value="'.$li->order.'" />';
$htmlAry['hidden'] .= '<input type="hidden" name="field" value="'.$li->field.'" />';
$htmlAry['hidden'] .= '<input type="hidden" name="page_size_change" value="" />';
$htmlAry['hidden'] .= '<input type="hidden" name="numPerPage" value="'.$li->page_size.'" />';

$TAGS_OBJ[] = array($Lang['eReportCardKG']['Setting']['AdminGroup']['Title']);
$indexVar['libreportcard_ui']->Echo_Module_Layout_Start($indexVar['returnMsgLang']);

# Search Box
$htmlAry['newLink'] = $indexVar['libreportcard_ui']->GET_LNK_NEW("?task=settings.admin_group.setting")."\n";
$htmlAry['box_search'] = $indexVar['libreportcard_ui']->Get_Search_Box_Div('Keyword', $Keyword);
$htmlAry['btn_edit'] = '<a class="tool_edit" href="javascript:checkEdit(document.form1,\'AdminGroupIDArr[]\',\'?task=settings.admin_group.setting\')">'.$Lang['Btn']['Edit'].'</a>'."\n";
$htmlAry['btn_delete'] = '<a class="tool_delete" href="javascript:checkRemove(document.form1,\'AdminGroupIDArr[]\',\'?task=settings.admin_group.delete\')">'.$Lang['Btn']['Delete'].'</a>'."\n";
?>