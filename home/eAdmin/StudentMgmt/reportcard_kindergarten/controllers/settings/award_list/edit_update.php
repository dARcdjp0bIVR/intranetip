<?php

# Data Setting
$AwardID = IntegerSafe($_POST['AwardID']);
$AwardCode = intranet_htmlspecialchars($_POST['Code']);
$AwardNameB5 = intranet_htmlspecialchars($_POST['CH_Name']);
$AwardNameEN = intranet_htmlspecialchars($_POST['EN_Name']);

$dataAry = array();
$dataAry['AwardCode'] = $AwardCode;
$dataAry['AwardNameCh'] = $AwardNameB5;
$dataAry['AwardNameEn'] = $AwardNameEN;

if($AwardID)
{
    $result = $indexVar['libreportcard']->Edit_KG_Award($AwardID, $dataAry);
}
else
{
    $dataAry['AwardType'] = $ercKindergartenConfig['awardType']['Input'];
    $result = $indexVar['libreportcard']->Insert_KG_Award($dataAry);
}

$success = $result? 1 : 0;
header("Location: index.php?task=settings.award_list.list&success=".$success);
?>