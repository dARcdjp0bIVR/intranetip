<?php 
/*
 * Change Log:
 * 
 */

# Data Setting
$AwardID = IntegerSafe($_POST['AwardID']);
$AwardCode = intranet_htmlspecialchars(trim($_POST['Code']));
$AwardNameB5 = intranet_htmlspecialchars(trim($_POST['CH_Name']));
$AwardNameEN = intranet_htmlspecialchars(trim($_POST['EN_Name']));

$fields = '';
if($AwardID) {
    $awards = $indexVar['libreportcard']->Get_KG_Award_List('', '', $AwardID);
}
else {
    $awards = $indexVar['libreportcard']->Get_KG_Award_List();
}
$awardCodeAssoc = BuildMultiKeyAssoc((array)$awards, 'AwardCode');
// $awardNameB5Assoc = BuildMultiKeyAssoc((array)$awards, 'AwardNameEn');
// $awardNameENAssoc = BuildMultiKeyAssoc((array)$awards, 'AwardNameCh');

$fieldArr = array();
if(isset($awardCodeAssoc[$AwardCode])) {
    $fieldArr[] = 'Code';
}
// if(isset($awardNameB5Assoc[$AwardNameB5])) {
//     $fieldArr[] = 'CH_Name';
// }
// if(isset($awardNameENAssoc[$AwardNameEN])) {
//     $fieldArr[] = 'EN_Name';
// }
if(!empty($fieldArr)) {
    $fields = implode(',', $fieldArr);
}

echo $fields;

?>