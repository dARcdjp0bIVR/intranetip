<?php 
// Using: 
/*
 * 	Date: 2017-12-20 (Bill)
 * 			Update Topics related Term Settings
 * 	Date: 2017-12-19 (Bill)
 * 			Create file
 */

### Data Setting
$_topicId = $_POST["topicId"] ? $_POST["topicId"] : "";

$_dataAry = array();
$_dataAry["Code"] = $_POST["Code"];
$_dataAry["NameEn"] = $_POST["EN_Name"];
$_dataAry["NameCh"] = $_POST["CH_Name"];
$_dataAry["CatID"] = $_POST["TopicCatID"];
$_dataAry["YearID"] = $_POST["YearID"];
$_dataAry["TermID"] = $_POST["TermID"];
$_dataAry["IsDeleted"] = $_POST["goDelete"];
if($_topicId == "") {
	$_dataAry["DisplayOrder"] = $indexVar["libreportcard"]->getTopicLastDisplayOrder($_POST["TopicCatID"], $_POST["YearID"]) + 1;
}
$indexVar["libreportcard"]->updateTopic($_topicId, $_dataAry, $updateLastModified=true);

header("Location: /home/eAdmin/StudentMgmt/reportcard_kindergarten/index.php?task=settings.langbehavior_item.list&success=1");
?>