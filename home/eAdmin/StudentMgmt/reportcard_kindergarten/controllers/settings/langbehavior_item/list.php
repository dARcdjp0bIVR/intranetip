<?php
// Using: 
/*
 * 	Date: 2017-12-19 (Bill)
 * 			Create file
 */

### Cookies Setting
$arrCookies = array();
$arrCookies[] = array("eRCkg_settings_langBehaviorItem_YearID", "YearID");
$arrCookies[] = array("eRCkg_settings_langBehaviorItem_TopicCatID", "TopicCatID");
if(isset($clearCoo) && $clearCoo == 1) {
	clearCookies($arrCookies);
}
else {
	updateGetCookies($arrCookies);
}

### Page Title
$TAGS_OBJ[] = array($Lang["eReportCardKG"]["Setting"]["LanguageBehaviorItem"]["Title"]);

### Message
if($success){
	$returnMsg = $Lang["General"]["ReturnMessage"]["UpdateSuccess"];
}
else{
	$returnMsg = "";
}
$indexVar["libreportcard_ui"]->Echo_Module_Layout_Start($returnMsg);

### ToolBar Action Buttons
$btnAry = array();
$btnAry[] = array("new", "javascript: goNew();");
$htmlAry["contentTool"] = $indexVar["libreportcard_ui"]->Get_Content_Tool_By_Array_v30($btnAry);

### DB Table Action Buttons
$subBtnAry = array();
$subBtnAry[] = array("edit", "javascript: goEdit();");
$subBtnAry[] = array("delete", "javascript: goDelete();");
$htmlAry["dbTableActionBtn"] = $indexVar["libreportcard_ui"]->Get_DBTable_Action_Button_IP25($subBtnAry);

### Year Selection
$htmlAry["yearSelection"] = $indexVar["libreportcard_ui"]->Get_Year_Selection($YearID);

### Topic Category Selection
$htmlAry["topicCategorySelection"] = $indexVar["libreportcard_ui"]->Get_Topic_Cat_Select($TopicCatID);

### Required JS CSS - Drag and Drop Table Rows
echo $indexVar["libreportcard_ui"]->Include_JS_CSS();
?>