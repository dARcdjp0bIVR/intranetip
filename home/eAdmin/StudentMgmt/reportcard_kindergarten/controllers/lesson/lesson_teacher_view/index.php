<?php
// editing by 
/*
 *  Date: 2018-03-22 (Bill) [2018-0202-1046-39164]
 *          Added back button - return to eReportCard (Kindergarten) index page
 *  
 * 	Date: 2017-10-06 (Bill)
 * 			Support Admin to access all classes
 */

# Initial Page Layout
$indexVar['libreportcard_ui']->Echo_Device_Layout_Start("teacher");

# Admin can access all classes - Get all Class Info
if($indexVar['libreportcard']->IS_KG_ADMIN_USER()) {
	$AccessibleClasses = $indexVar['libreportcard']->GetAllClassesInTimeTable();
}
# Get Teaching Classes
else {
	$AccessibleClasses = $indexVar['libreportcard']->Get_Teaching_Class($UserID);
}
$withAccessibleClasses = count($AccessibleClasses) > 0;

# Get Available Topics
$defaultSelectedClass = array();
$defaultAvailableTopic = array();
if($withAccessibleClasses)
{
	// Get default Class
	$defaultSelectedClass = $AccessibleClasses[0];
	
	// Get Topic in available TimeTable
	$AvailableTopics = $indexVar['libreportcard']->GetTimeTable($defaultSelectedClass["YearID"], "", 1);
	$AvailableTopics = BuildMultiKeyAssoc((array)$AvailableTopics, array("TopicID"));
	$withAvailableTopics = count($AvailableTopics) > 0;
	
	// Get default Topic
	if($withAvailableTopics) {
		$defaultAvailableTopic = array_shift(array_slice($AvailableTopics, 0, 1));
	}
}

# Class Selection Box
$htmlAry['ClassSelection'] = $indexVar['libreportcard_ui']->Get_Device_Class_Selection_Box($AccessibleClasses, " updateTopicSelection() ");

# Topic Selection Box
$htmlAry['TopicSelection'] = $indexVar['libreportcard_ui']->Get_Device_Topic_Selection_Box($AvailableTopics);

# Button
$htmlAry['submitBtn'] = $indexVar['libreportcard_ui']->Get_Device_Button("mark", "", " checkForm() ");
$htmlAry['backBtn'] = $indexVar['libreportcard_ui']->Get_Device_Button("back", "index.php");

# Hidden Fields
$htmlAry['hiddenInputField'] = "";
$htmlAry['hiddenInputField'] .= "<input type='hidden' name='class_input' id='class_input' value='".$defaultSelectedClass["YearClassID"]."'>\r\n";
$htmlAry['hiddenInputField'] .= "<input type='hidden' name='topic_input' id='topic_input' value='".$defaultAvailableTopic["TopicID"]."'>\r\n";
?>