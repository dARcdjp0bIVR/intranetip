<?php

# Data Checking
if(empty($_POST["classlevel_input"]) || empty($_POST["class_input"]) || empty($_POST["timetable_input"]) || empty($_POST["topic_input"]) || empty($_POST["zone_input"]) || empty($_POST["student_input"])) {
	// Go to index page if data invalid
	No_Access_Right_Pop_Up("", "index.php?task=lesson.lesson_teacher_view.index");
}

// Handle Student Score Data
$InputScore = $_POST["tool_score"];
$ScoreDataCount = count((array)$InputScore);
if($ScoreDataCount > 0)
{
	// loop Teaching Tools
	$ScoreArr = array();
	foreach($InputScore as $thisToolID => $thisToolScore)
	{
		if($thisToolScore > 0) {
			$thisScore = array();
			$thisScore["StudentID"] = $_POST["student_input"];
			$thisScore["ClassLevelID"] = $_POST["classlevel_input"];
			$thisScore["TimeTableID"] = $_POST["timetable_input"];
			$thisScore["TopicID"] = $_POST["topic_input"];
			$thisScore["ZoneID"] = $_POST["zone_input"];
			$thisScore["ToolID"] = $thisToolID;
			$thisScore["Score"] = $thisToolScore;
			$thisScore["ModifiedBy"] = $UserID;
			$ScoreArr[] = $thisScore;
		}
		
		// Insert Empty Score
		//$result['insert_overall_mark'] = $indexVar['libreportcard']->INSERT_STUDENT_SCORE((array)$_POST["student_input"], $_POST["classlevel_input"], $_POST["timetable_input"], $_POST["topic_input"], $_POST["zone_input"], $thisToolID, $UserID);
	}
	
	// Insert Student Score
	$result['insert_mark'] = $indexVar['libreportcard']->INSERT_STUDENT_SCORE($ScoreArr);
	
	// Update Student Score
	//$result['update_marksheet'] = $indexVar['libreportcard']->UPDATE_STUDENT_SCORE($ScoreArr);
}

// Redirect to page for Student Selection
header("Location: index.php?task=lesson.lesson_teacher_view.zone&class_input=".$_POST["class_input"]."&topic_input=".$_POST["topic_input"]."&zone_input=".$_POST["zone_input"]."&timetable_input=".$_POST["timetable_input"]."&classlevel_input=".$_POST["classlevel_input"]);

?>