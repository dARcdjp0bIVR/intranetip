<?php
// editing by 
/*
 * 	Date: 2017-11-17 (Bill)
 * 			Default Entry Status: Student In Zone
 * 	Date: 2017-10-17 (Bill)
 * 			Support Admin to access all classes
 */

# Initial Page Layout
$indexVar['libreportcard_ui']->Echo_Device_Layout_Start("teacher");

# Get GET / POST Value
$classlevel_input = $_POST["classlevel_input"]? $_POST["classlevel_input"] : $_GET["classlevel_input"];
$class_input = $_POST["class_input"]? $_POST["class_input"] : $_GET["class_input"];
$timetable_input = $_POST["timetable_input"]? $_POST["timetable_input"] : $_GET["timetable_input"];
$topic_input = $_POST["topic_input"]? $_POST["topic_input"] : $_GET["topic_input"];
$zone_input = $_POST["zone_input"]? $_POST["zone_input"] : $_GET["zone_input"];

# Data Checking
if(empty($classlevel_input) || empty($class_input) || empty($timetable_input) || empty($topic_input) || empty($zone_input)) {
	// Go to index page if data invalid
	No_Access_Right_Pop_Up("", "index.php?task=lesson.lesson_teacher_view.index");
}

# Get Class, Topic and Zone Info
$thisClassInfo = $indexVar['libreportcard']->Return_Class_Teacher_Class($UserID, "", "", $class_input);
$thisTopicInfo = $indexVar['libreportcard']->Get_Form_Topic_Record($topic_input);
$thisZoneInfo = $indexVar['libreportcard']->Get_Learning_Zone_Record($zone_input);

# Admin can access all classes - Get Target Class Info
if($indexVar['libreportcard']->IS_KG_ADMIN_USER() && empty($thisClassInfo))
{
	$AccessibleClasses = $indexVar['libreportcard']->GetAllClassesInTimeTable();
	$AccessibleClasses = BuildMultiKeyAssoc((array)$AccessibleClasses, array("YearClassID"));
	$targetClass = $AccessibleClasses[$class_input];
	
	$thisClassInfo = array();
	$thisClassInfo[0] = $targetClass;
}

# Data Checking
if(empty($thisClassInfo) || empty($thisTopicInfo) || empty($thisZoneInfo)) {
	// Go to index page if data invalid
	No_Access_Right_Pop_Up("", "index.php?task=lesson.lesson_teacher_view.index");
}

# Get Class Student
$ClassStudentList = $indexVar['libreportcard']->Get_Student_By_Class($class_input);

# Student Selection
$htmlAry['studentDisplay'] = $indexVar['libreportcard_ui']->Get_Class_Student_Selection_Area($ClassStudentList, $classlevel_input, $timetable_input, $topic_input, $zone_input, "all", STUDNET_IN_ZONE);

# Input Status Selection Box
$InputStatusAry = array();
$InputStatusAry[] = array("all", $Lang['eReportCardKG']['Management']['Device']['AllEntryStatus']);
$InputStatusAry[] = array(INPUT_SCORE_INCOMPLETE, $Lang['eReportCardKG']['Management']['Device']['IncompleteInputScore']);
$InputStatusAry[] = array(INPUT_SCORE_COMPLETE, $Lang['eReportCardKG']['Management']['Device']['CompleteInputScore']);
$htmlAry['InputStatusSelection'] = $indexVar['libreportcard_ui']->Get_Device_Selection_Box($InputStatusAry, "", "input_status", $Lang['eReportCardKG']['Management']['Device']['AllEntryStatus'], "updateStudentList()");

# Entry Status Selection Box
$EntryStatusAry = array();
$EntryStatusAry[] = array("all", $Lang['eReportCardKG']['Management']['Device']['AllEntryStatus']);
$EntryStatusAry[] = array(STUDNET_IN_ZONE, $Lang['eReportCardKG']['Management']['Device']['InZoneStatus']);
$EntryStatusAry[] = array(STUDNET_INCOMPLETE, $Lang['eReportCardKG']['Management']['Device']['IncompleteStatus']);
$EntryStatusAry[] = array(STUDENT_COMPLETE, $Lang['eReportCardKG']['Management']['Device']['CompleteStatus']);
$htmlAry['EntryStatusSelection'] = $indexVar['libreportcard_ui']->Get_Device_Selection_Box($EntryStatusAry, "", "entry_status", $Lang['eReportCardKG']['Management']['Device']['InZoneStatus'], "updateStudentList()");

# Title
$htmlAry['TitleInfo'] = $indexVar['libreportcard_ui']->Get_Device_Title_Info($thisZoneInfo[0]["ZoneNameB5"], $thisTopicInfo[0]["TopicNameB5"], $thisClassInfo[0]["ClassName"]);

# Button
$htmlAry['backBtn'] = $indexVar['libreportcard_ui']->Get_Device_Button("back", "", "backToTopic()");

# Hidden Fields
$htmlAry['hiddenInputField'] = "";
$htmlAry['hiddenInputField'] .= "<input type='hidden' name='classlevel_input' id='classlevel_input' value='".$classlevel_input."'>\r\n";
$htmlAry['hiddenInputField'] .= "<input type='hidden' name='class_input' id='class_input' value='".$class_input."'>\r\n";
$htmlAry['hiddenInputField'] .= "<input type='hidden' name='timetable_input' id='timetable_input' value='".$timetable_input."'>\r\n";
$htmlAry['hiddenInputField'] .= "<input type='hidden' name='topic_input' id='topic_input' value='".$topic_input."'>\r\n";
$htmlAry['hiddenInputField'] .= "<input type='hidden' name='zone_input' id='zone_input' value='".$zone_input."'>\r\n";
$htmlAry['hiddenInputField'] .= "<input type='hidden' name='student_input' id='student_input' value=''>\r\n";
$htmlAry['hiddenInputField'] .= "<input type='hidden' name='input_status_input' id='input_status_input' value='all'>\r\n";
$htmlAry['hiddenInputField'] .= "<input type='hidden' name='entry_status_input' id='entry_status_input' value='".STUDNET_IN_ZONE."'>\r\n";
?>