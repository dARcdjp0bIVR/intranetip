<?php
// editing by 
/*
 *  Date: 2019-10-31 (Bill)
 *          copy logic from lesson.get_ajax > access group member checking in IS_KG_ADMIN_USER()
 */

//if($_POST["Type"] == "TopicSelection")
//{
//	# Class Info
//	$TeachingClass = $indexVar['libreportcard']->Return_Class_Teacher_Class($UserID, "", "", $_POST["ClassID"]);
//	$thisClassLevel = $TeachingClass[0]["ClassLevelID"];
//
//	# Admin can access all classes - Get Target Class Info
//	if($indexVar['libreportcard']->IS_KG_ADMIN_USER() && empty($TeachingClass))
//	{
//		$AccessibleClasses = $indexVar['libreportcard']->GetAllClassesInTimeTable();
//		$AccessibleClasses = BuildMultiKeyAssoc((array)$AccessibleClasses, array("YearClassID"));
//		$targetClass = $AccessibleClasses[$_POST["ClassID"]];
//		$thisClassLevel = $targetClass["ClassLevelID"];
//	}
//
//	# Topic Selection Box
//	$TeachingClassTopics = $indexVar['libreportcard']->Get_Form_Topic_Record("", $thisClassLevel);
//	$withTeachingClassTopics = count($TeachingClassTopics) > 0;
//	if($thisClassLevel && $withTeachingClassTopics) {
//		for($i=0; $i<count($TeachingClassTopics); $i++) {
//			$TeachingClassTopics[$i] = array($TeachingClassTopics[$i]["TopicID"], $TeachingClassTopics[$i]["TopicNameB5"]);
//		}
//		$returnContent = $indexVar['libreportcard_ui']->Get_Device_Selection_Box($TeachingClassTopics, $Lang['eReportCardKG']['Management']['Device']['Topic'], $TeachingClassTopics[0][1], "topic");
//	}
//	else {
//		$returnContent = $indexVar['libreportcard_ui']->Get_Device_Selection_Box("", $Lang['eReportCardKG']['Management']['Device']['Topic'], $Lang['eReportCardKG']['Management']['Device']['NoTopics'], "topic");
//	}
//}

// Topic Selection
if($_POST["Type"] == "TopicSelection")
{
    if(empty($_POST["ClassID"])) {
        $returnContent = "";
    }
    else {
        # Class Info
        $TeachingClasses = $indexVar['libreportcard']->Return_Class_Teacher_Class($UserID, "", "", $_POST["ClassID"]);
        $defaultTeachingClass = $TeachingClasses[0];

        # Admin can access all classes - Get Target Class Info
        if($indexVar['libreportcard']->IS_KG_ADMIN_USER() && empty($defaultTeachingClass))
        {
            $AccessibleClasses = $indexVar['libreportcard']->GetAllClassesInTimeTable();
            $AccessibleClasses = BuildMultiKeyAssoc((array)$AccessibleClasses, array("YearClassID"));
            $defaultTeachingClass = $AccessibleClasses[$_POST["ClassID"]];
        }

        # Get Available Topics
        $AvailableTopics = array();
        if(!empty($defaultTeachingClass) && $defaultTeachingClass["ClassLevelID"] > 0) {
            $AvailableTopics = $indexVar['libreportcard']->GetTimeTable($defaultTeachingClass["ClassLevelID"], "", 1);
            $AvailableTopics = BuildMultiKeyAssoc((array)$AvailableTopics, array("TopicID"));
        }

        # Topic Selection Box
        $returnContent = $indexVar['libreportcard_ui']->Get_Device_Topic_Selection_Box($AvailableTopics, $_POST["onChangeFunction"]);
    }
}
# Zone Selection
else if($_POST["Type"] == "ZoneSelection")
{
    if(empty($_POST["ClassID"]) || empty($_POST["TopicID"])) {
        $returnContent = "";
    }
    else {
        # Class Info
        $TeachingClasses = $indexVar['libreportcard']->Return_Class_Teacher_Class($UserID, "", "", $_POST["ClassID"]);
        $defaultTeachingClass = $TeachingClasses[0];

        # Admin can access all zones - Get Target Zone Info
        if($indexVar['libreportcard']->IS_KG_ADMIN_USER() && empty($defaultTeachingClass))
        {
            $AccessibleClasses = $indexVar['libreportcard']->GetAllClassesInTimeTable();
            $AccessibleClasses = BuildMultiKeyAssoc((array)$AccessibleClasses, array("YearClassID"));
            $defaultTeachingClass = $AccessibleClasses[$_POST["ClassID"]];
        }

        # Topic Info
        $thisTeachingTopic = $_POST["TopicID"];

        # Get Available Zone
        $thisTimeTableInfo = $indexVar['libreportcard']->GetTimeTable($defaultTeachingClass["YearID"], "", 1);
        $thisTimeTableInfo = BuildMultiKeyAssoc((array)$thisTimeTableInfo, array("TopicID", "ZoneID"));
        $thisTopicRelatedZone = $thisTimeTableInfo[$thisTeachingTopic];

        # Zone Selection
        $returnContent = $indexVar['libreportcard_ui']->Get_Device_Zone_Selection_Area($thisTopicRelatedZone, $defaultTeachingClass["YearID"]);
    }
}
# Student Selection
else if($_POST["Type"] == "StudentListSelection")
{
    if(empty($_POST["DataList"])) {
        $returnContent = "";
    }
    else {
        # Get POST data
        $dataAry = array();
        parse_str($_POST["DataList"], $dataAry);

        # Get data after parse_str()
        $thisClassLevelID = $dataAry["classlevel_input"];
        $thisClassID = $dataAry["class_input"];
        $thisTimeTableID = $dataAry["timetable_input"];
        $thisTopicID = $dataAry["topic_input"];
        $thisZoneID = $dataAry["zone_input"];
        $thisStudentID = $dataAry["student_input"];
        $thisInputStatus = $dataAry["input_status_input"];
        $thisEntryStatus = $dataAry["entry_status_input"];

        # Get Class Student
        $ClassStudentList = $indexVar['libreportcard']->Get_Student_By_Class($thisClassID);

        # Student Selection
        if(!empty($ClassStudentList) && $thisClassLevelID > 0 && $thisTimeTableID > 0 && $thisTopicID > 0 && $thisZoneID > 0) {
            $returnContent = $indexVar['libreportcard_ui']->Get_Class_Student_Selection_Area($ClassStudentList, $thisClassLevelID, $thisTimeTableID, $thisTopicID, $thisZoneID, $thisInputStatus, $thisEntryStatus);
        }
    }
}
# Student in another zone checking
else if($_POST["Type"] == "checkInAnotherZone")
{
    $returnContent = '';
    if($_POST["StudentID"] != '' && $_POST["ZoneID"] != '')
    {
        $anotherZoneID = $indexVar['libreportcard']->checkStudentInAnotherZone($_POST["StudentID"], $_POST["ZoneID"]);
        if(!empty($anotherZoneID) && $anotherZoneID[0] > 0) {
            $anotherZone = $indexVar['libreportcard']->Get_Learning_Zone_Record($anotherZoneID[0]);
            if(!empty($anotherZone) && isset($anotherZone[0])) {
                $returnContent = $anotherZone[0]['ZoneNameB5'];
            }
        }
    }
}

echo $returnContent;
?>