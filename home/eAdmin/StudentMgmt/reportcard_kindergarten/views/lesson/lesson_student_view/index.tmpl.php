<script type="text/javascript">

$(document).ready( function() {
	// reset hidden input field when back
	$('input#class_input').val("<?=$defaultSelectedClass["YearClassID"]?>");
	$('input#topic_input').val("<?=$defaultAvailableTopic["TopicID"]?>");
});

function updateSelection(target_id, target_type, obj) {
	if(target_id==0 || target_type=="") return;
	
	// update selection display and id
	$('span#'+target_type+'title')[0].textContent = obj.textContent;
	$('input#'+target_type+'input').val(target_id);
}

function updateTopicSelection() {
	var ClassID = $('input#class_input').val();
	if(ClassID=="") return;
	
	// clear current topic id
	$('input#topic_input').val("");
	$('div#ZoneSelectionID').html("");
	
	$.ajax({
		type: "POST",
		url : "index.php?task=lesson.lesson_student_view.get_ajax",
		data:{
			"Type": "TopicSelection",
			"ClassID": ClassID,
			"onChangeFunction": "updateZoneSelection()"
		},
		success: function(topicSel){
			// update topic selection and set first topic as selected topic
			$('span#TopicSelectionID').html(topicSel);
			$("span#TopicSelectionID li:first-child a:first-child")[0].click()
		}
	});
}

function updateZoneSelection() {
	var ClassID = $('input#class_input').val();
	var TopicID = $('input#topic_input').val();
	if(ClassID=="" || TopicID=="") return;
	
	// clear current zone id
	$('input#zone_input').val("");
	$('div#ZoneSelectionID').html("");
	
	$.ajax({
		type: "POST",
		url : "index.php?task=lesson.lesson_student_view.get_ajax",
		data:{
			"Type": "ZoneSelection",
			"ClassID": ClassID,
			"TopicID": TopicID
		},
		success: function(zoneSel){
			// update zone selection
			$('div#ZoneSelectionID').html(zoneSel);
		}
	});
}

function selectZone(zid) {
	$('input#zone_input').val(zid);
	checkForm();
}

function checkForm() {
	if($('input#class_input').val()=="") {
		alert('<?=$Lang['eReportCardKG']['Management']['Device']['NotSelectAnyClasses']?>');
		return false;
	}
	else if($('input#topic_input').val()=="") {
		alert('<?=$Lang['eReportCardKG']['Management']['Device']['NotSelectAnyTopics']?>');
		return false;
	}
	
	$('form#form1').submit();
}
</script>

<form name="form1" id="form1" method="POST" action="index.php?task=lesson.lesson_student_view.student">

<div class="container student" role="main">
	<div class="col-md-1 col-lg-2"></div>
	<div class="center-area col-sm-12 col-md-10 col-lg-8">
		
		<div class="content-area">
			<div class="selection-area">
				<span id="ClassSelectionID"><?=$htmlAry['ClassSelection']?></span>&nbsp;
				<span id="TopicSelectionID"><?=$htmlAry['TopicSelection']?></span>
			</div>
			
			<div class="subject-button-area" id="ZoneSelectionID">
				<?=$htmlAry['zoneDisplay']?>
			</div>
		</div>
		
	</div>
	<div class="col-md-1 col-lg-2"></div>

<?=$htmlAry['hiddenInputField']?>

</form>