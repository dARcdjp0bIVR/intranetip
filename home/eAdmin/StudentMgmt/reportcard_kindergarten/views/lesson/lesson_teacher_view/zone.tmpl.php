<script type="text/javascript">
$(document).ready( function() {
	// reset hidden input field when back
	$('input#input_status_input').val("all");
	$('input#entry_status_input').val("<?=STUDNET_IN_ZONE?>");
});

function updateSelection(target_id, target_type, obj) {
	if(target_id=="" || target_type=="") return;
	
	// update selection display and id
	$('span#'+target_type+'title')[0].textContent = obj.textContent;
	$('input#'+target_type+'input').val(target_id);
}

function updateStudentList() {
	var ClassID = $('input#class_input').val();
	if(ClassID=="") return;
	
	$.ajax({
		type: "POST",
		url : "index.php?task=lesson.lesson_teacher_view.get_ajax",
		data:{
			"Type": "StudentListSelection",
			"DataList": $('form#form1').serialize()
		},
		success: function(studentSel){
			// update student selection
			$('ul#studentArea').html(studentSel);
		}
	});
}

function selectStudent(sid) {
	$('input#student_input').val(sid);
	$('form#form1')[0].action = 'index.php?task=lesson.lesson_teacher_view.student_score';
	$('form#form1').submit();
}

function backToTopic() {
	$('form#form1')[0].action = 'index.php?task=lesson.lesson_teacher_view.topic';
	$('form#form1').submit();
}
</script>

<form name="form1" id="form1" method="POST" action="index.php?task=lesson.lesson_teacher_view.student_score">

<div class="col-md-1 col-lg-2"></div>
<div class="center-area col-sm-12 col-md-10 col-lg-8">
	<div class="content-area">
		<div class="title-area">
			<?=$htmlAry['TitleInfo']?>
		</div>
		
		<div class="filter-area">
			<?=$htmlAry['InputStatusSelection']?>
			<?=$htmlAry['EntryStatusSelection']?>
		</div>
		
		<ul class="photo-area teacher" id="studentArea">
			<?=$htmlAry['studentDisplay']?>
		</ul>
		
		<div class="btn-area">
			<?=$htmlAry['backBtn']?>
		</div>
	</div>
</div>
<div class="col-md-1 col-lg-2"></div>

<?=$htmlAry['hiddenInputField']?>
</form>