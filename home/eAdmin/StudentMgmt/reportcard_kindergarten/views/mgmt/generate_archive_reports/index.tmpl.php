<script type="text/javascript">
function js_Reload_Class_Selection()
{
    $('div.warnMsgDiv').hide();
    $('div#LastArchivedDiv').html('<?=$indexVar['emptySymbol']?>');

    var yearId = $('select#YearID').val();
    if(yearId)
    {
        $.post(
            "index.php?task=mgmt<?=$ercKindergartenConfig["taskSeparator"]?>generate_archive_reports<?=$ercKindergartenConfig["taskSeparator"]?>ajax_reload",
            {
                Action: 'Reload_Class_List',
                YearID: yearId
            },
            function(ReturnData)
            {
                $("div#ClassSelectionDiv").html(ReturnData);
                js_Reload_Archive_Status();
            }
        )
    }
}

function js_Reload_Archive_Status()
{
    $('div.warnMsgDiv').hide();
    $('div#LastArchivedDiv').html('<?=$indexVar['emptySymbol']?>');

    var termId = $('select#TermID').val();
    var yearId = $('select#YearID').val();
    var classId = $('select#ClassID').val();
    if(termId && yearId && classId)
    {
        $.post(
            "index.php?task=mgmt<?=$ercKindergartenConfig["taskSeparator"]?>generate_archive_reports<?=$ercKindergartenConfig["taskSeparator"]?>ajax_reload",
            {
                Action: 'Reload_Archive_Date',
                TermID: termId,
                YearID: yearId,
                ClassID: classId
            },
            function(ReturnData)
            {
                $("div#LastArchivedDiv").html(ReturnData);
            }
        )
    }
}

function goSubmit()
{
    $('div.warnMsgDiv').hide();

    var canSubmit = true;
    var termId = $('select#TermID').val();
    if(termId == null || termId == '') {
        canSubmit = false;
        $('div#Term_Warn').show();
    }
    var yearId = $('select#YearID').val();
    if(yearId == null || yearId == '') {
        canSubmit = false;
        $('div#Year_Warn').show();
    }
    var classId = $('select#ClassID').val();
    if(classId == null || classId == '') {
        canSubmit = false;
        $('div#Class_Warn').show();
    }
	
	if(canSubmit) {
        var archivedRecordDate = $("div#LastArchivedDiv").html();
        var confirmOverwritten = archivedRecordDate != '' && archivedRecordDate != '<?=$indexVar['emptySymbol']?>';
        if(!confirmOverwritten || (confirmOverwritten && confirm('<?=$Lang['eReportCardKG']['Management']['ArchiveReport']['ConfirmArchiveReport']?>'))) {
            $('form#form1').submit();
        }
	}
}
</script>

<form id="form1" method="POST" action="index.php?task=mgmt<?=$ercKindergartenConfig["taskSeparator"]?>generate_reports<?=$ercKindergartenConfig["taskSeparator"]?>print_charts">
	<table class="form_table_v30">
		<tbody>
            <tr>
                <td class="field_title"><?=$Lang["General"]["Term"]?></td>
                <td>
                    <?=$htmlAry["SemesterSelection"]?>
                    <div style="display:none;" class="warnMsgDiv" id="Term_Warn">
                        <span class="tabletextrequire">*<?=$Lang["eReportCardKG"]["Setting"]["InputWarning"].$Lang["General"]["Term"]?></span>
                    </div>
                </td>
            </tr>
            <tr>
                <td class="field_title"><?=$Lang["General"]["Form"]?></td>
                <td>
                    <?=$htmlAry["FormSelection"]?>
                    <div style="display:none;" class="warnMsgDiv" id="Year_Warn">
                        <span class="tabletextrequire">*<?=$Lang["eReportCardKG"]["Setting"]["InputWarning"].$Lang["General"]["Form"]?></span>
                    </div>
                </td>
            </tr>
            <tr>
                <td class="field_title"><?=$Lang["General"]["Class"]?></td>
                <td>
                    <div id="ClassSelectionDiv"><?=$htmlAry["ClassSelection"]?></div>
                    <div style="display:none;" class="warnMsgDiv" id="Class_Warn">
                        <span class="tabletextrequire">*<?=$Lang["eReportCardKG"]["Setting"]["InputWarning"].$Lang["General"]["Class"]?></span>
                    </div>
                </td>
            </tr>
            <tr>
                <td class="field_title"><?=$Lang['eReportCardKG']['Management']['ArchiveReport']['LastArchived']?></td>
                <td>
                    <div id="LastArchivedDiv"><?=$htmlAry["LastArchiveInfo"]?></div>
                </td>
            </tr>
		</tbody>
	</table>

<!--    <input type="hidden" id="isArchivedBefore" value="" >-->
    <input type="hidden" name="isArchiveReport" value="1" >
</form>

<div class="edit_bottom_v30">
	<input type="button" value="<?=$Lang["Btn"]["Submit"]?>" class="formbutton_v30 print_hide " onclick="goSubmit()" id="submitBtn" name="submitBtn" <?php echo $disabled?>>
</div>