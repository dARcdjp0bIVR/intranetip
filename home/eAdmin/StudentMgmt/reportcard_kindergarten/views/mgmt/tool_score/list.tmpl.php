<script type="text/javascript">
$( document ).ready(function() {
	$('input#ClassID').val('');
	$('input#ZoneID').val('');
});

function goEdit(ClassID, ZoneID) {
	$('input#ClassID').val(ClassID);
	$('input#ZoneID').val(ZoneID);
	$('form#form1')[0].action = 'index.php?task=mgmt<?=$ercKindergartenConfig['taskSeparator']?>tool_score<?=$ercKindergartenConfig['taskSeparator']?>zone';
	$('form#form1').submit();
}
</script>

<form name="form1" id="form1" method="POST">
	<div class="table_board">
		<br style="clear:both;">
		<p class="spacer"></p>
		
		<div class="table_filter">
			<?=$htmlAry['YearSelection']?>
			<?=$htmlAry['TimeTableSelection']?>
			&nbsp;&nbsp;
			<?=$thisTopicName?>
		</div>
		<br style="clear:both;">
		<br style="clear:both;">
		<p class="spacer"></p>
		
		<?=$htmlAry['TimeTableContent']?>
		<br style="clear:both;" />
		<br style="clear:both;" />
	</div>
	
	<?=$htmlAry['hiddenInputField']?>
</form>