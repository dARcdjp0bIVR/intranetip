<?php
/*
 * Change Log:
 * Date: 2019-09-24 Philips Added onSelectChapterInThickBox()
 * Date: 2018-10-15 Bill    Disable submit button > prevent any unwanted data issues    [2018-1015-1724-00096]
 * Date: 2018-05-02 Bill    Display Warning Box after updated Zone Settings
 * Date: 2018-03-22 Bill    Support Class Zone Quota Settings   [2018-0202-1046-39164]
 * Date: 2017-12-20 Bill	Set Default End Time "23:59:59" when add TimeTable
 * Date: 2017-11-17 Bill  	support Quota settings in Zone
 * Date: 2017-10-17 Bill  	added js function onSelectCatInThickBox()
 * Date: 2017-02-09 Villa	modified GoBack()
 */
?>

<?=$NivagationBar?>
<br>

<?php if($isEdit) { ?>
    <div>
        <?=$WarningBox_Static?>
    </div>
<?php } ?>

<div style="display:none;" id="NeedConfirmMsg">
	<?=$WarningBox?>
</div>
<br>

<form id='form1' method="POST" action='index.php?task=mgmt.topic_timetable.new_update'>
<table class="form_table_v30">
	<tbody>
		<tr>
			<td class="field_title">
				<span class="tabletextrequire">*</span>
				<?=$Lang["eReportCardKG"]["Setting"]["Code"]?>
			</td>
			<td>
				<input type="text" maxlength="25" class="textboxnum requiredField" name="Code" id="Code" value="<?=$Code?>">
				<div style="display:none;" class="warnMsgDiv" id="Code_Warn">
					<span class="tabletextrequire">*<?=$Lang["eReportCardKG"]["Setting"]["InputWarning"].$Lang["eReportCardKG"]["Setting"]["Code"]?></span>
				</div>
				<div style="display:none;" class="warnMsgDiv" id="Code_Warn2">
					<span class="tabletextrequire">*<?=$Lang["eReportCardKG"]["Setting"]["Code"].$Lang["eReportCardKG"]["Setting"]["DuplicateWarning"]?></span>
				</div>
			</td>
		</tr>
		<tr> 
			<td class="field_title">
				<span class="tabletextrequire">*</span>
				<?=$Lang["eReportCardKG"]["Setting"]["NameB5"]?>
			</td>
			<td>
				<input type="text" maxlength="25" class="textboxnum requiredField" name="CH_Name" id="CH_Name" value="<?=$CH_Name?>">
				<div style="display:none;" class="warnMsgDiv" id="CH_Name_Warn">
					<span class="tabletextrequire">*<?=$Lang["eReportCardKG"]["Setting"]["InputWarning"].$Lang["eReportCardKG"]["Setting"]["NameB5"]?></span>
				</div>
				<div style="display:none;" class="warnMsgDiv" id="CH_Name_Warn2">
					<span class="tabletextrequire">*<?=$Lang["eReportCardKG"]["Setting"]["NameB5"].$Lang["eReportCardKG"]["Setting"]["DuplicateWarning"]?></span>
				</div>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<span class="tabletextrequire">*</span>
				<?=$Lang["eReportCardKG"]["Setting"]["NameEN"]?>
			</td>
			<td>
				<input type="text" maxlength="255" class="textboxnum requiredField" name="EN_Name" id="EN_Name" value="<?=$EN_Name?>">
				<div style="display:none;" class="warnMsgDiv" id="EN_Name_Warn">
					<span class="tabletextrequire">*<?=$Lang["eReportCardKG"]["Setting"]["InputWarning"].$Lang["eReportCardKG"]["Setting"]["NameEN"]?></span>
				</div>
				<div style="display:none;" class="warnMsgDiv" id="EN_Name_Warn2">
					<span class="tabletextrequire">*<?=$Lang["eReportCardKG"]["Setting"]["NameEN"].$Lang["eReportCardKG"]["Setting"]["DuplicateWarning"]?></span>
				</div>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<span class="tabletextrequire">*</span>
				<?=$Lang["eReportCardKG"]["Setting"]["Year"]?>
			</td>
			<td>
				<?=$YearSelection?>
				<div style="display:none;" class="warnMsgDiv" id="Year_Warn">
					<span class="tabletextrequire">*<?=$Lang["eReportCardKG"]["Setting"]["InputWarning"].$Lang["eReportCardKG"]["Setting"]["Year"]?></span>
				</div>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<span class="tabletextrequire">*</span>
				<?=$Lang["eReportCardKG"]["Management"]["TopicTimeTable"]["StartDate"]?>
			</td>
			<td>
				<?=$DateStart?>
				<?=$LoadingImage?>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<span class="tabletextrequire">*</span>
				<?=$Lang["eReportCardKG"]["Management"]["TopicTimeTable"]["EndDate"]?>
			</td>
			<td>
				<?=$DateEnd?>
				<div style="display:none;" class="warnMsgDiv" id="Date_Warn2">
					<span class="tabletextrequire">*<?=$Lang["General"]["JS_warning"]["InvalidDateFormat"]?></span>
				</div>
				<div style="display:none;" class="warnMsgDiv" id="Date2_Warn2">
					<span class="tabletextrequire">*<?=$Lang["General"]["JS_warning"]["InvalidDateRange"]?></span>
				</div>
				<?=$LoadingImage?>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<span class="tabletextrequire">*</span>
				<?=$Lang["eReportCardKG"]["Setting"]["FormTopic"]["Title"]?>
			</td>
			<td>
				<div id="TopicCell" style=" display: inline-block;">
					<?=$Lang["eReportCardKG"]["Management"]["TopicTimeTable"]["PleaseEnterYear"]?>
				</div>
				<div style="display:none;" class="warnMsgDiv" id="Topic_Warn">
					<span class="tabletextrequire">*<?=$Lang["eReportCardKG"]["Setting"]["InputWarning"].$Lang["eReportCardKG"]["Setting"]["FormTopic"]["Title"]?></span>
				</div>
				<?=$LoadingImage?>
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<span class="tabletextrequire">*</span>
				<?=$Lang["eReportCardKG"]["Setting"]["LearningZone"]["Title"]?>
			</td>
			<td>
				<div id="ZoneCell" style=" display: inline-block;">
					<?=$Lang["eReportCardKG"]["Management"]["TopicTimeTable"]["PleaseEnterTopic"]?>
				</div>
				<?=$LoadingImage?>
				<div style="display:none;" class="warnMsgDiv" id="Zone_Warn">
					<span class="tabletextrequire">*<?=$Lang["eReportCardKG"]["Setting"]["InputWarning"].$Lang["eReportCardKG"]["Setting"]["LearningZone"]["Title"]?></span>
				</div>
			</td>
		</tr>
	</tbody>
</table>

<input type="hidden" id="TimeTableID" name="TimeTableID" value="<?=$TimeTableID?>" >
<input type="hidden" id="toolData" name="toolData">
<input type="hidden" id="zoneQuotaData" name="zoneQuotaData">
</form>

<div class="edit_bottom_v30">
	<p class="spacer"></p>
	<input type="button" value="<?=$Lang["Btn"]["Submit"]?>" class="formbutton_v30 print_hide " onclick="CheckForm()" id="submitBtn" name="submitBtn">
	<input type="button" value="<?=$Lang["Btn"]["Back"]?>" class="formbutton_v30 print_hide " onclick="goBack()" id="backBtn" name="backBtn">
	<p class="spacer"></p>
</div>
<?=$thickBox?>

<script src="<?=$indexVar["thisBasePath"]?>templates/json2.js"></script>

<script>
var toolData = []; 			// saving tool data
var zoneQuotaArr = [];		// saving zone quota
var myJSON = '';

$(document).ready(function(){
	if('<?=$isEdit?>') { 	// input record to the form
		$('#YearID').change();

		disableSubmitBtn();
		setTimeout(intialize_Data, 1500);	// delay for letting finish the ajax first
	}
	else {					// set default end date time
		var DateEnd_Hour = '<?=$EndDate_Hour?>';
		$('#DateEnd_hour').val(DateEnd_Hour);
		var DateEnd_Minutes = '<?=$EndDate_Minutes?>';
		$('#DateEnd_min').val(DateEnd_Minutes);
		var DateEnd_Second = '<?=$EndDate_Second?>';
		$('#DateEnd_sec').val(DateEnd_Second);
	}
});

function intialize_Data()
{
	// Start Date
	var DateStart_Date = '<?=$StartDate_Date?>';
	$('#DateStart').val(DateStart_Date);
	var DateStart_Hour = '<?=$StartDate_Hour?>';
	$('#DateStart_hour').val(DateStart_Hour);
	var DateStart_Minutes = '<?=$StartDate_Minutes?>';
	$('#DateStart_min').val(DateStart_Minutes);
	var DateStart_Second = '<?=$StartDate_Second?>';
	$('#DateStart_sec').val(DateStart_Second);
	
	// End Date
	var DateEnd_Date = '<?=$EndDate_Date?>';
	$('#DateEnd').val(DateEnd_Date);
	var DateEnd_Hour = '<?=$EndDate_Hour?>';
	$('#DateEnd_hour').val(DateEnd_Hour);
	var DateEnd_Minutes = '<?=$EndDate_Minutes?>';
	$('#DateEnd_min').val(DateEnd_Minutes);
	var DateEnd_Second = '<?=$EndDate_Second?>';
	$('#DateEnd_sec').val(DateEnd_Second);
	
	// Form Topics
	var TopicID = '<?=$TopicID?>';
	$('#topic').val(TopicID);
	
	// Learning Zone
	<?php foreach ((array)$zoneIDArr as $_zoneID){ ?>
		var zoneId = '<?=$_zoneID?>';
		$('#Zone_'+zoneId).attr('checked','checked');
	<?php } ?>
	
	// Teaching Tool
	<?php foreach((array)$teaching_tool as $ZoneID => $ToolID){ ?>
		var ToolIDArr = []; // store checked id
		<?php foreach((array)$ToolID as $_ToolID){ ?>
			ToolIDArr.push('<?=$_ToolID?>');
		<?php } ?>
		toolData['<?=$ZoneID?>'] = ToolIDArr;
	<?php } ?>
	
	var key;
	var toolDataObj = {};
	for(key in toolData){
		if(key!='in_array'){
			toolDataObj[key] = toolData[key];
		}
	}
	myJSON = JSON.stringify(toolDataObj);
	$('#toolData').val(myJSON);
	
	// Zone Quota
	<?php foreach((array)$zone_quota as $ZoneID => $ToolZoneQuota) { ?>
		zoneQuotaArr['<?=$ZoneID?>'] = [];
		zoneQuotaArr['<?=$ZoneID?>'][0] = '<?=$ToolZoneQuota?>';
		<?php foreach((array)$class_zone_quota[$ZoneID] as $thisClassID => $ClassZoneQuota) { ?>
    		zoneQuotaArr['<?=$ZoneID?>'][<?=$thisClassID?>] = '<?=$ClassZoneQuota?>';
		<?php } ?>
	<?php } ?>
	
	var key;
	var zoneQuotaObj = {};
	for(key in zoneQuotaArr)
	{
		if(key != 'in_array') {
			zoneQuotaObj[key] = {};
			for(key2 in zoneQuotaArr[key])
			{
				if(key2 != 'in_array') {
					zoneQuotaObj[key][key2] = zoneQuotaArr[key][key2];
				}
			}
		}
	}
	myJSON = JSON.stringify(zoneQuotaObj);
	$('#zoneQuotaData').val(myJSON);
	
	$('.LoadingImg').hide();
	
	reEnableSubmitBtn();
}

function CheckForm()
{
	disableSubmitBtn();
	
	// Hide Warning Message
	$('#Code_Warn').hide();
	$('#Code_Warn2').hide();
	$('#CH_Name_Warn').hide();
	$('#CH_Name_Warn2').hide();
	$('#EN_Name_Warn').hide();
	$('#EN_Name_Warn2').hide();
	$('#Year_Warn').hide();
	$('#Date_Warn').hide();
	$('#Date2_Warn').hide();
	$('#Topic_Warn').hide();
	$('#Zone_Warn').hide();
	
	// Empty Checking
	var Check = true;
	if($.trim($('#Code').val())==''){
		Check = false;
		$('#Code_Warn').show();
		$('#Code').focus();
	}
	if($.trim($('#CH_Name').val())==''){
		Check = false;
		$('#CH_Name_Warn').show();
		$('#CH_Name').focus();
	}
	if($.trim($('#EN_Name').val())==''){
		Check = false;
		$('#EN_Name_Warn').show();
		$('#EN_Name').focus();
	}
	if($('#YearID').val()==''){
		Check = false;
		$('#Year_Warn').show();
		$('#YearID').focus();
	}
	if($('#topic').val()==undefined || $('#topic').val()==''){
		Check = false;
		$('#Topic_Warn').show();
		$('#topic').focus();
	}
	if($('[name=ZoneID[]]:checked').val()==undefined){
		Check = false;
		$('#Zone_Warn').show();
		$('#ZoneCell').focus();
	}
	
	// Data Input Checking
	if(Check){
		Check = formDataChecking();
	} else {
		reEnableSubmitBtn();
	}
}

function formDataChecking()
{
	$.ajax({
		type: "POST",
		url : "index.php?task=mgmt.topic_timetable.ajax_timetable_checking",
		data:{	
			"DataList": $('form#form1').serialize()
		},
		success: function(Type){
			// display message
			if(Type == "") {
				form1.submit();
			} else {
				$('#'+Type+'_Warn2').show();
				
				reEnableSubmitBtn();
				return false;
			}
		}
	});
}

function YearOnChange()
{
	var YearID = $('#YearID').val();
	$.ajax({
		type: "POST",
		url : "index.php?task=mgmt.topic_timetable.ajax_get_topic",
		data:{
			"YearID": YearID
		},
		success: function(msg){
			$('#TopicCell').html(msg);
			TopicOnChange();
		}
	});
}

function TopicOnChange()
{
	var YearID = $('#YearID').val();
	var TopicID = '<?=$TopicID?>';
	
	$.ajax({
		type: "POST",
		url : "index.php?task=mgmt.topic_timetable.ajax_get_zone",
		data:{
			"YearID": YearID,
			"TopicID": TopicID
		},
		success: function(msg){
			$('#ZoneCell').html(msg);
			$('#toolData').val('');
			$('#zoneQuotaData').val('');
			toolData = [];
			zoneQuotaArr = [];
		}
	});
}

function ZoneOnClick(ZoneID)
{
	load_dyn_size_thickbox_ip('<?=$Lang["eReportCardKG"]["Setting"]["TeachingTool"]["Title"]?>', 'onloadThickBox('+ZoneID+');');
}

function onloadThickBox(ZoneID)
{
	var YearID = $('#YearID').val();
	var toolDataID = $('#toolData').val();
	var zoneQuotaData = $('#zoneQuotaData').val();
	
	$.ajax({
		type: "POST",
		url: "index.php?task=mgmt.topic_timetable.ajax_get_learning_tool", 
		data:{ 
			"ZoneID": ZoneID,
			"YearID": YearID,
			"toolData" : toolDataID,
			"zoneQuotaData" : zoneQuotaData
		},
		success: function(ReturnData){
			$('div#TB_ajaxContent').html(ReturnData);
			adjust_dyn_size_thickbox_content_height_ip('thickboxContainerDiv', 'thickboxContentDiv', 'editBottomDiv');
			onSelectChapterInThickBox();
		}
	});
}

function onSelectChapterInThickBox(){
	var YearID = $('#YearID').val();
	var ZoneID = $('#hiddenZone').val();
	var chapter = $('#chapter').val();
	var toolDataID = $('#toolData').val();

	$.ajax({
		type: "POST",
		url: "index.php?task=mgmt.topic_timetable.ajax_get_learning_tool2",
		data:{
			"YearID": YearID,
			"ZoneID": ZoneID,
			"chapter": chapter,
			"toolData" : toolDataID
		},
		success: function(ReturnData){
			$('#checkBoxArea').html(ReturnData);
			adjust_dyn_size_thickbox_content_height_ip('thickboxContainerDiv', 'thickboxContentDiv', 'editBottomDiv');
		}
	});
}

// function onSelectCatInThickBox(ZoneID, ToolCatID)
// {
// 	var YearID = $('#YearID').val();
// 	var toolDataID = $('#toolData').val();
// 	var zoneQuotaData = $('#zoneQuotaData').val();
//
// 	$.ajax({
// 		type: "POST",
// 		url: "index.php?task=mgmt.topic_timetable.ajax_get_learning_tool",
// 		data:{
// 			"ToolCatID": ToolCatID,
// 			"ZoneID": ZoneID,
// 			"YearID": YearID,
// 			"toolData" : toolDataID,
// 			"zoneQuotaData" : zoneQuotaData
// 		},
// 		success: function(ReturnData){
// 			$('div#TB_ajaxContent').html(ReturnData);
// 			adjust_dyn_size_thickbox_content_height_ip('thickboxContainerDiv', 'thickboxContentDiv', 'editBottomDiv');
// 		}
// 	});
// }

function SaveTools()
{
	var hiddenZoneID = $('#hiddenZone').val();
	var ToolIDArr = []; // store checked id
	
	// Speical Handling when user select tools using Category filtering
	var TargetToolCatID = $('#ToolCatID').val();
	if(TargetToolCatID != "" && toolData[hiddenZoneID])
	{
		// Get tools from other categories
		$('input[name=ToolID[]]').each(function(){
			var taget_index = $.inArray($(this).val(), toolData[hiddenZoneID]);
			if(taget_index !== -1) {
				toolData[hiddenZoneID].splice(taget_index, 1);
			}
		});
		ToolIDArr = toolData[hiddenZoneID];
	}
	
	$('input[name=ToolID[]]:checked').each(function(){
		ToolIDArr.push($(this).val());
	});
	ToolIDArr.sort();
	toolData[hiddenZoneID] = ToolIDArr;
	
	// Packing as JSON array
	var key;
	var toolDataObj = {};
	for(key in toolData){
		if(key!='in_array'){
			toolDataObj[key] = toolData[key];
		}
	}
	myJSON = JSON.stringify(toolDataObj);
	$('#toolData').val(myJSON);
	
	// Zone Quota
	var zoneQuotaNum = $('#Quota').val();
	zoneQuotaArr[hiddenZoneID] = [];
	zoneQuotaArr[hiddenZoneID][0] = zoneQuotaNum;
	$("input[id^='ClassQuota']").each(function() {
	    var classid = this.id.match(/\d+/);
	    zoneQuotaArr[hiddenZoneID][classid] = this.value;
	});
	
	var key;
	var zoneQuotaObj = {};
	for(key in zoneQuotaArr)
	{
		if(key != 'in_array') {
			zoneQuotaObj[key] = {};
			for(key2 in zoneQuotaArr[key])
			{
				if(key2 != 'in_array') {
					zoneQuotaObj[key][key2] = zoneQuotaArr[key][key2];
				}
			}
		}
	}
	myJSON = JSON.stringify(zoneQuotaObj);
	$('#zoneQuotaData').val(myJSON);

	$('#NeedConfirmMsg').show();
	CloseThickBox();
}

function CloseThickBox(){
	$('#TB_window').fadeOut();
	tb_remove();
}

function goSubmit(){
	if(CheckForm()){
		form1.submit();
	}
}

function goBack(){
	 window.location.href = "index.php?task=mgmt.topic_timetable.list";
}

function disableSubmitBtn() {
	$('#submitBtn').attr('disabled','disabled');
	$('#submitBtn').removeClass('formbutton_v30').addClass('formbutton_disable_v30');
}

function reEnableSubmitBtn() {
	$('#submitBtn').removeAttr('disabled');
	$('#submitBtn').removeClass('formbutton_disable_v30').addClass('formbutton_v30');
}
</script>