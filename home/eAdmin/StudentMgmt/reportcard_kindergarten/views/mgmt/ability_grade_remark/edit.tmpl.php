<?=$NavigationBar?>
<br> 
<br>

<form id="form1" method="POST" action="index.php?task=mgmt.ability_grade_remark.edit_update" enctype="multipart/form-data">
	<div style="display:none;" id="NeedConfirmMsg">
		<?=$WarningBox?>
	</div>
	<table class="form_table_v30">
	<tbody>
		<tr>
			<td class="field_title"><?=$Lang['eReportCardKG']['Setting']['TaiwanAbilityIndex']['Name']?></td>
			<td><?=$IndexCat?></td>
		</tr>
		<tr>
			<td class="field_title"><?=$Lang['eReportCardKG']['Setting']['AbilityRemarks']['Grading']?></td>
			<td><?=$IndexGrade?></td>
		</tr>
		<tr>
			<td class="field_title"><?=$Lang['eReportCardKG']['Setting']['AbilityRemarks']['UpperLimit']?></td>
			<td><?=$IndexLowerLimit?> ~ <?=$IndexUpperLimit?></td>
		</tr>
		<tr>
			<td class="field_title"><?=$Lang['eReportCardKG']['Setting']['AbilityRemarks']['Year']?></td>
			<td><?=$IndexYearName?></td>
		</tr>
		<tr>
			<td class="field_title"><?=$Lang['eReportCardKG']['Setting']['AbilityRemarks']['Term']?></td>
			<td><?=$IndexTermName?></td>
		</tr>
		<tr>
			<td class="field_title"><?=$Lang['eReportCardKG']['Setting']['AbilityRemarks']['Comment']?></td>
			<td><?=$IndexRemarks?></td>
		</tr>
		<tr>
			<td class="field_title"><?=$Lang['eReportCardKG']['Setting']['AbilityRemarks']['OriComment']?></td>
			<td><?=$IndexRemarksAll?></td>
		</tr>
		<tr>
			<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang['eReportCardKG']['Setting']['AbilityRemarks']['NewComment']?></td>
			<td>
				<textarea maxlength="255" class="textboxtext requiredField" name="IndexRemark" id="IndexRemark" ><?=$IndexRemarkCustom?></textarea>
				<div style="display:none;" class="warnMsgDiv" id="IndexName_Warn">
					<span class="tabletextrequire">* <?=$Lang['eReportCardKG']['Setting']['PleaseInputContent']?></span>
				</div>
			</td>
		</tr>
	</tbody>
	</table>
	
	<input type='hidden' id='YearID' name='YearID' value='<?=$IndexYearID?>'/>
	<input type='hidden' id='TermID' name='TermID' value='<?=$IndexTermID?>'/>
	<input type='hidden' id='IndexID' name='IndexID' value='<?=$IndexID?>'/>
	<input type='hidden' id='CustomIndexID' name='CustomIndexID' value='<?=$CustomIndexID?>'/>
	<input type='hidden' id='isResetRemark' name='isResetRemark' value='0'>
</form>

<div class="edit_bottom_v30">
	<p class="spacer"></p>
	<input type="button" value="<?=$button_submit?>" class="formbutton_v30 print_hide " onclick="goSubmit()" id="submitBtn" name="submitBtn">
	<input type="button" value="<?=$button_back?>" class="formbutton_v30 print_hide " onclick="goBack()" id="backBtn" name="backBtn">
	<input type="button" value="<?=$Lang['eReportCardKG']['Management']['AbilityRemarks']['Reset']?>" class="formbutton_v30 print_hide " onclick="goReset()" id="resetBtn" name="resetBtn">
	<p class="spacer"></p>
</div>

<script>
function checkForm() {
	$('#IndexName_Warn').hide();

	var check = true;
	if(!$('#IndexRemark').val()) {
		$('#IndexName_Warn').show();
		$('#IndexRemark').focus();
		check = false;
	}
	
	return check;
}

function goSubmit() {
	if(checkForm()) {
		form1.submit();
	}
}

function goBack() {
	window.location.href = "index.php?task=mgmt.ability_grade_remark.list";
}

function goReset() {
	if(confirm('<?=$Lang['eReportCardKG']['Management']['AbilityRemarks']['ConfirmResetRemark']?>')) {
		$('#IndexRemark').val('');
		$('#isResetRemark').val(1);
		form1.submit();
	}
}

function goURL() {
	window.location = '?task=mgmt<?=$ercKindergartenConfig['taskSeparator']?>ability_grade_remark<?=$ercKindergartenConfig['taskSeparator']?>list';
}
$(document).ready(function(){
	$('#NeedConfirmMsg').show();
});
</script>