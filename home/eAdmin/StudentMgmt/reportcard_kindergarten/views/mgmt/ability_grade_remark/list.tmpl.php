<script type="text/javascript">
$(document).ready(function(){
 if($('#YearID').val()==''){
	$('#YearID option:nth-child(2)').attr('selected', true);
	document.form1.submit();
 }
 if($('#TermID').val()==''){
	$('#TermID option:nth-child(2)').attr('selected',true);
	document.form1.submit();
 }
});

function goEdit(CodeID,YearID,TermID) {
	window.location = '?task=mgmt<?=$ercKindergartenConfig['taskSeparator']?>ability_grade_remark<?=$ercKindergartenConfig['taskSeparator']?>edit&isEdit=1&CodeID='+CodeID +'&YearID=' + YearID + '&TermID=' + TermID;
}

function checkEdit(){
	var EditID = [];
	$('.checkbox:checked').each(function(){
		EditID.push($(this).val());
	});
	if(EditID.length==1){
		goEdit(EditID[0],'<?=$YearID?>', '<?=$TermID?>');
	}
	else if(EditID.length>1){
		alert("<?=$Lang['eReportCardKG']['Setting']['EditWarning']['NoMoreThan1'] ?>");
	}
	else{
		alert("<?=$Lang['eReportCardKG']['Setting']['EditWarning']['PleaseSelectEdit'] ?>");
	}
}
</script>
<form name="form1" id="form1" method="POST">
	<div class="table_board">
		<div class="content_top_tool">
			<br style="clear:both;">
			<!--<?=$htmlAry['contentTool']?>-->
			<?=$htmlAry['typeSelection']?>
			<?=$htmlAry['searchBox']?>
			<br style="clear:both;">
		</div>
		<br style="clear:both;">
		<p class="spacer"></p>
		<?=$htmlAry['dbTableActionBtn']?>
		
		<?=$htmlAry['dataTable']?>
	</div>
	
	<?=$htmlAry['hiddenField']?>
</form>