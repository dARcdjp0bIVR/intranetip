<script type="text/javascript">
// $(document).ready(function(){
//     if($('#YearID').val()==''){
//         $('#YearID option:nth-child(2)').attr('selected', true);
//         document.form1.submit();
//     }
//     if($('#TermID').val()==''){
//         $('#TermID option:nth-child(2)').attr('selected',true);
//         document.form1.submit();
//     }
// });

function goEdit(TermID) {
	window.location = '?task=mgmt<?=$ercKindergartenConfig['taskSeparator']?>view_edit_period_setting<?=$ercKindergartenConfig['taskSeparator']?>edit' + '&YearTermID=' + TermID;
}

function checkEdit(){
	var EditID = [];
	$('.checkbox:checked').each(function() {
		EditID.push($(this).val());
	});
	if(EditID.length == 1) {
		goEdit(EditID[0]);
	}
	else if(EditID.length > 1) {
		alert("<?=$Lang['eReportCardKG']['Setting']['EditWarning']['NoMoreThan1'] ?>");
	}
	else {
		alert("<?=$Lang['eReportCardKG']['Setting']['EditWarning']['PleaseSelectEdit'] ?>");
	}
}
</script>

<form name="form1" id="form1" method="POST">
	<div class="table_board">
		<br style="clear:both;">
		<p class="spacer"></p>
		<?=$htmlAry['dbTableActionBtn']?>
		<?=$htmlAry['table']?>
		<?php //echo $htmlAry['debugTable']?>
		
	</div>
	
	<?php //$htmlAry['hiddenField'] ?>
</form>