<style type="text/css">
div.container {
	display: none;
	width: 0px;
	height: 0px;
	margin: 0 auto;
}

div.tips {
	text-align:center;
	top: 40%;
	position: absolute;
	font-size: 300%;
}
</style>

<script src="<?=$indexVar["thisBasePath"]?>templates/highcharts_kg/highcharts.js"></script>
<script src="<?=$indexVar["thisBasePath"]?>templates/highcharts_kg/modules/series-label.js"></script>
<script src="<?=$indexVar["thisBasePath"]?>templates/highcharts_kg/modules/exporting.js"></script>

<?=$htmlAry["highchartsDiv"]?>
<div class="tips">Processing</div>

<script type="text/javascript">
/**
 * Build SVG objects array
 */
Highcharts.getSVG = function (charts) {
    var svgObjAry = [];
    Highcharts.each(charts, function (chart) {
		thisSvgObj = { "id" : chart.renderTo.id, "thisSvg" : chart.getSVG() },
        svgObjAry.push(thisSvgObj);
    });
	
    return svgObjAry;
};

/**
 * POST SVG to client site
 */
Highcharts.exportCharts = function (charts, options) {
    // Post to PDF page
    Highcharts.post('index.php?task=mgmt<?=$ercKindergartenConfig["taskSeparator"]?>generate_reports<?=$ercKindergartenConfig["taskSeparator"]?><?=$htmlAry["targetPage"]?>', {
        targetTermId: '<?=$htmlAry["TermID"]?>',
        targetClassIdAry: JSON.stringify(['<?=$htmlAry["ClassID"]?>']),
        targetStuIdAry: JSON.stringify(['<?=$htmlAry["StudentID"]?>']),
        svg: JSON.stringify(Highcharts.getSVG(charts))
    });
};

/**
 * Build Highcharts
 */
<?=$htmlAry["highchartsScript"]?>
Highcharts.exportCharts([<?=$htmlAry["highchartsAry"]?>]);
</script>