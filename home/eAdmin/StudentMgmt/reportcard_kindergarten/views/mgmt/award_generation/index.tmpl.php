<script type="text/javascript">
$(document).ready(function() {
	$('input#ClassID').val('');
	$('input#AwardID').val('');
});

function js_Change_View_Mode(viewMode) {
	$('input#viewMode').val(viewMode);

	$('form#form1')[0].action = 'index.php?task=mgmt<?=$ercKindergartenConfig["taskSeparator"]?>award_generation<?=$ercKindergartenConfig["taskSeparator"]?>index';
	$('form#form1').submit();
}

function goEdit(TargetID) {
	<?php if($viewMode == 'CLASS') { ?>
		$('input#ClassID').val(TargetID);
	<?php } else { ?>
		$('input#AwardID').val(TargetID);
	<?php } ?>
	
	$('form#form1')[0].action = 'index.php?task=mgmt<?=$ercKindergartenConfig["taskSeparator"]?>award_generation<?=$ercKindergartenConfig["taskSeparator"]?>edit';
	$('form#form1').submit();
}

function goExport() {
	$('form#form1')[0].action = 'index.php?task=mgmt<?=$ercKindergartenConfig["taskSeparator"]?>award_generation<?=$ercKindergartenConfig["taskSeparator"]?>export';
	$('form#form1').submit();
}
</script>

<form name="form1" id="form1" method="POST">
	<div class="table_board">
		<br style="clear:both;">
		<p class="spacer"></p>
		<?=$htmlAry['contentTool']?>
		<?=$htmlAry["ContentTopBtn"]?>
		
		<br style="clear:both;">
		<p class="spacer"></p>
		<?=$htmlAry["dbTableContent"]?>
		<br style="clear:both;" />
		<br style="clear:both;" />
	</div>
	
	<?=$htmlAry["hiddenInputField"]?>
</form>