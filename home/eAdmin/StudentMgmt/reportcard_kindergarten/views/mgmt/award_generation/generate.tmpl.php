<script type="text/javascript">
$(document).ready(function() {
	$('input#ClassID').val('');
	$('input#AwardID').val('');
});

function js_Change_View_Mode(viewMode) {
	$('input#viewMode').val(viewMode);

	$('form#form1')[0].action = 'index.php?task=mgmt<?=$ercKindergartenConfig["taskSeparator"]?>award_generation<?=$ercKindergartenConfig["taskSeparator"]?>generate';
	$('form#form1').submit();
}

function js_Generate() {
	$('#submitBtn').attr('disabled', true);
	$('#submitBtn').removeClass('formbutton_v30').addClass('formbutton_disable_v30');
	
	<?php if($viewMode == "CLASS") { ?>
		$('div.warnMsgDiv').hide();
    	var classIdList = $('select#ClassID').val();
    	if(classIdList == null || classIdList == '')
    	{
    		$('div#Class_Warn').show();
    		
    		$('#submitBtn').attr('disabled', '');
    		$('#submitBtn').removeClass('formbutton_disable_v30').addClass('formbutton_v30');
    		
    		return;
    	}
	<?php } ?>
	
	if(confirm('<?=$Lang['eReportCardKG']['Management']['AwardGeneration']['WarningArr']['ReGenerateAward']?>')) {
    	$('form#form1')[0].action = 'index.php?task=mgmt<?=$ercKindergartenConfig["taskSeparator"]?>award_generation<?=$ercKindergartenConfig["taskSeparator"]?>generate_update';
    	$('form#form1').submit();
	}
	else {
		$('#submitBtn').attr('disabled', '');
		$('#submitBtn').removeClass('formbutton_disable_v30').addClass('formbutton_v30');
	}
}

function goEdit(TargetID) {
	$('input#AwardID').val(TargetID);
	
	$('form#form1')[0].action = 'index.php?task=mgmt<?=$ercKindergartenConfig["taskSeparator"]?>award_generation<?=$ercKindergartenConfig["taskSeparator"]?>edit';
	$('form#form1').submit();
}
</script>

<form name="form1" id="form1" method="POST">
	<div class="table_board">
		<br style="clear:both;">
		<p class="spacer"></p>
		<?=$htmlAry["ContentTopBtn"]?>
		
		<br style="clear:both;">
		<p class="spacer"></p>
		<?php if($viewMode == "AWARD") { ?>
			<?=$htmlAry["dbTableContent"]?>
		<?php } else { ?>
			<table class="form_table_v30">
        		<tbody>
        		<tr>
        			<td class="field_title"><?=$Lang["General"]["Class"]?></td>
        			<td>
        				<?=$htmlAry["ClassSelection"]?>
        				<div style="display:none;" class="warnMsgDiv" id="Class_Warn">
        					<span class="tabletextrequire">*<?=$Lang["eReportCardKG"]["Setting"]["InputWarning"].$Lang["General"]["Class"]?></span>
        				</div>
        			</td>
        		</tr>
        		</tbody>
        	</table>
		<?php } ?>
		
		<br style="clear:both;" />
		<br style="clear:both;" />
		<?=$htmlAry["buttonDiv"]?>
	</div>
	</div>
	
	<?=$htmlAry["hiddenInputField"]?>
</form>