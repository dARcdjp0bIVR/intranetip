<script language="javascript">
$(document).ready( function() {
	js_Reload_Student_Table();
});

function js_Change_View_Mode(viewMode) {
	$('input#viewMode').val(viewMode);

	$('form#form1')[0].action = 'index.php?task=mgmt<?=$ercKindergartenConfig["taskSeparator"]?>award_generation<?=$ercKindergartenConfig["taskSeparator"]?>index';
	$('form#form1').submit();
}

function js_Go_Back() {
	window.location = '?task=mgmt.award_generation.index&viewMode=<?=$viewMode?>';
}

function js_Reload_Student_Table() {
	var jsClassID = $('input#ClassID').val();
	if (jsClassID == null) {
		jsClassID = $('input#ClassID').val();
	}
	
	var jsAwardID = $('input#AwardID').val();
	if (jsAwardID == null) {
		jsAwardID = $('input#AwardID').val();
	}
	
	Block_Element('StudentTableDiv');
	$('div#StudentTableDiv').html('<?=$indexVar["libreportcard_ui"]->Get_Ajax_Loading_Image()?>').load(
		"?task=mgmt.award_generation.ajax_reload",
		{
			Action: 'Edit_Award_Student_Table',
			viewMode: '<?=$viewMode?>',
			ClassID: jsClassID,
			AwardID: jsAwardID
		},
		function(ReturnData)
		{
			initThickBox();
			UnBlock_Element('StudentTableDiv');
		}
	);
}

function js_Delete_Award(jsStudentID, jsRecordID) {
	if (confirm('<?=$Lang['eReportCardKG']['Management']['AwardGeneration']['WarningArr']['DeleteAward']?>'))
	{
		Block_Element('StudentTableDiv');
		$.post(
			"?task=mgmt.award_generation.ajax_update", 
			{
				Action: 'Delete_Student_Award',
				RecordID: jsRecordID,
				StudentID: jsStudentID
			},
			function(ReturnData)
			{
				if (ReturnData == '1') {
					Get_Return_Message('<?=$Lang['eReportCardKG']['Management']['AwardGeneration']['ReturnMsgArr']['AwardDeleteSuccess']?>');
					js_Reload_Student_Table();
				}
				else {
					Get_Return_Message('<?=$Lang['eReportCardKG']['Management']['AwardGeneration']['ReturnMsgArr']['AwardDeleteFailed']?>');
				}

				UnBlock_Element('StudentTableDiv');
			}
		);
	}
}

function js_Show_Add_Student_Award_Layer(jsStudentID, jsReturnMsg) {
	var jsClassID = $('input#ClassID').val();
	if (jsClassID == null) {
		jsClassID = $('input#ClassID').val();
	}
	
	$.post(
		"?task=mgmt.award_generation.ajax_reload",
		{
			Action: 'Reload_Add_Student_Award_Layer',
			ClassID: jsClassID,
			StudentID: jsStudentID
		},
		function(ReturnData)
		{
			$('div#TB_ajaxContent').html(ReturnData);
// 			js_Reload_Subject_Selection();
//			
			if (jsReturnMsg != '' && jsReturnMsg != null) {
				Get_Return_Message(jsReturnMsg);
			}
		}
	);	
}

// function js_Changed_Award_Selection() {
// 	js_Reload_Subject_Selection();
// }

function js_Add_Student_Award(jsAddMore) {
	var jsAwardID = $('select#AwardID').val();
	var jsStudentID = $('input#StudentID').val();
	
	$('div.WarningDiv').hide();
	
	var jsCanSubmit = true;
	if (jsAwardID == '' || jsAwardID == null) {
		$('div#AwardEmptyWarningDiv').show();
		jsCanSubmit = false;
	}
// 	if ($('tr#SubjectTr').is(':visible') && (jsSubjectID == '' || jsSubjectID == null)) {
// 		$('div#SubjectEmptyWarningDiv').show();
// 		jsCanSubmit = false;
// 	}
	
	if (jsCanSubmit) {
		$.post(
			"?task=mgmt.award_generation.ajax_update", 
			{ 
				Action: 'Add_Student_Award',
				StudentID: jsStudentID,
				AwardID: jsAwardID
			},
			function(ReturnData)
			{
				var jsReturnMsg;
				if (ReturnData == '1') {
					jsReturnMsg = '<?=$Lang['eReportCardKG']['Management']['AwardGeneration']['ReturnMsgArr']['AwardAddSuccess']?>';
				}
				else {
					jsReturnMsg = '<?=$Lang['eReportCardKG']['Management']['AwardGeneration']['ReturnMsgArr']['AwardAddFailed']?>';
				}
				
				if (jsAddMore == 1) {
					js_Show_Add_Student_Award_Layer(jsStudentID, jsReturnMsg);
				}
				else {
					js_Hide_ThickBox();
					Scroll_To_Top();
					Get_Return_Message(jsReturnMsg);
				}
				
				js_Reload_Student_Table();
			}
		);
	}
}

function js_Show_Add_Award_Student_Layer(jsAwardID, jsReturnMsg) {
	var jsAwardID = $('input#AwardID').val();
	if (jsAwardID == null) {
		jsAwardID = $('input#AwardID').val();
	}
	
	$.post(
		"?task=mgmt.award_generation.ajax_reload", 
		{ 
			Action: 'Reload_Add_Award_Student_Layer',
			AwardID: jsAwardID
		},
		function(ReturnData)
		{
			$('div#TB_ajaxContent').html(ReturnData);
			
			if (jsReturnMsg != '' && jsReturnMsg != null) {
				Get_Return_Message(jsReturnMsg);
			}
		}
	);	
}

function js_Changed_Class_Selection(jsYearClassID) {
	if (jsYearClassID == '') {
		$('tr#StudentSelTr').hide();
	}
	else {
		$('tr#StudentSelTr').show();
		var excludeStudentList = $('input#AwardedStudentIDList').val();
		
		$('div#StudentSelDiv').html('<?=$indexVar["libreportcard_ui"]->Get_Ajax_Loading_Image()?>').load(
            "?task=mgmt.award_generation.ajax_reload",
			{
                Action: 'StudentByClass',
				ClassID: jsYearClassID,
				SelectionID: 'StudentIDArr',
				SelectionName: 'StudentIDArr[]',
				noFirst: 1,
				isAll: 0,
				isMultiple: 1,
				withSelectAll: 1,
				ExcludeStudentIDList: excludeStudentList
			},
			function(ReturnData) {
				// do nothing
			}
		);
	}
}

function js_Add_Award_Student(jsAddMore) {
	var jsAwardID = $('input#AwardID').val();
	if (jsAwardID == null) {
		jsAwardID = $('input#AwardID').val();
	}
	var jsYearClassID = $('select#ClassIDArr').val();

	var jsStudentIDList = '';
	var jsStudentIDArr = $('select#StudentIDArr').val();
	if (jsStudentIDArr != null && jsStudentIDArr != undefined) {
		var jsStudentIDList = jsStudentIDArr.toString();
	}
	
	$('div.WarningDiv').hide();
	var jsCanSubmit = true;
	
	if (jsYearClassID == '' || jsYearClassID == null) {
		$('div#ClassEmptyWarningDiv').show();
		jsCanSubmit = false;
	}
	
	if (!(jsYearClassID == '' || jsYearClassID == null) && (jsStudentIDList == '' || jsStudentIDList == null)) {
		$('div#StudentEmptyWarningDiv').show();
		jsCanSubmit = false;
	}
	
	if (jsCanSubmit) {
		$.post(
			"?task=mgmt.award_generation.ajax_update", 
			{
				Action: 'Add_Award_Student',
				StudentIDArr: jsStudentIDList,
				AwardID: jsAwardID
			},
			function(ReturnData)
			{
				var jsReturnMsg;
				if (ReturnData == '1') {
					jsReturnMsg = '<?=$Lang['eReportCardKG']['Management']['AwardGeneration']['ReturnMsgArr']['AwardAddSuccess']?>';
				}
				else {
					jsReturnMsg = '<?=$Lang['eReportCardKG']['Management']['AwardGeneration']['ReturnMsgArr']['AwardAddFailed']?>';
				}
				
				if (jsAddMore == 1) {
					js_Show_Add_Award_Student_Layer(jsAwardID, jsReturnMsg);
				}
				else {
					js_Hide_ThickBox();
					Scroll_To_Top();
					Get_Return_Message(jsReturnMsg);
				}
				
				js_Reload_Student_Table();
			}
		);
	}
}
</script>

<form name="form1" id="form1" method="POST">
	<div class="table_board">
		<br style="clear:both;">
		<p class="spacer"></p>
		<?=$htmlAry["ContentTopBtn"]?>
		<?=$htmlAry["infoTable"]?>
		
		<br style="clear:both;">
		<p class="spacer"></p>
		<div id="StudentTableDiv"></div>
		
		<br style="clear:both;" />
		<br style="clear:both;" />
		<?=$htmlAry["buttonDiv"]?>
	</div>
	
	<?=$htmlAry["hiddenInputField"]?>
</form>
<br />
<br />