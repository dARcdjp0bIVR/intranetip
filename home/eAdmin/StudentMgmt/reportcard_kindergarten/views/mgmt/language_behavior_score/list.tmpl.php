<script type="text/javascript">
$(document).ready(function() {
	$('input#ClassID').val('');
	$('input#YearTermID').val('');
	$('input#TopicCatID').val('');
});

function goEdit(ClassID, TermID, TopicCatID) {
	$('input#ClassID').val(ClassID);
	$('input#YearTermID').val(TermID);
	$('input#TopicCatID').val(TopicCatID);
	
	$('form#form1')[0].action = 'index.php?task=mgmt<?=$ercKindergartenConfig["taskSeparator"]?>language_behavior_score<?=$ercKindergartenConfig["taskSeparator"]?>score';
	$('form#form1').submit();
}
</script>

<form name="form1" id="form1" method="POST">
	<div class="table_board">
		<br style="clear:both;">
		<p class="spacer"></p>
		
		<div class="table_filter">
			<?=$htmlAry["yearSelection"]?>
			<?=$htmlAry["termSelection"]?>
		</div>
		<br style="clear:both;">
		<br style="clear:both;">
		<p class="spacer"></p>
		
		<?=$htmlAry["dbTableContent"]?>
		<br style="clear:both;" />
		<br style="clear:both;" />
	</div>
	
	<?=$htmlAry["hiddenInputField"]?>
</form>