<script type="text/javascript">
$(document).ready( function() {
	$('#Content_tab a').click(function(e) {
		e.preventDefault();
		if(this.attributes != undefined && this.attributes[0] != undefined) {
			if(this.attributes[0].value != 'null') {
				setUploadType(this.attributes[0].value);
			}
			// for IE usage
			else if(this.nameProp != undefined && this.nameProp != 'null') {
				setUploadType(this.nameProp);
			}
		}
		return false;
	});
	$('#NeedConfirmMsg').show();
});

function reloadOtherInfoTableContent()
{
	$('form#form1')[0].action = 'index.php?task=mgmt<?=$ercKindergartenConfig['taskSeparator']?>other_info<?=$ercKindergartenConfig['taskSeparator']?>edit';
	$('form#form1').submit();
}

function formSubmit()
{
	$("div.WarnMsgDiv").hide();
	var errObj = new Array();
	
	$("input.num").each(function()
	{
		var thisObj = $(this);
		if(thisObj.val() && !IsNumeric(thisObj.val()))
		{
			var targetDivId = thisObj.attr("id");
			targetDivId = targetDivId.replace(/\[/g, '\\[');
			targetDivId = targetDivId.replace(/\]/g, '\\]');
			
			$("div#WarnDiv" + targetDivId).show();
			errObj.push(thisObj);
		}
	});
	
	var patt=/^[a-zA-Z][+|\*|-]?$/;
	$("input.grade").each(function()
	{
		var thisObj = $(this);
		if(thisObj.val() && !patt.test(thisObj.val()))
		{
			var targetDivId = thisObj.attr("id");
			targetDivId = targetDivId.replace(/\[/g, '\\[');
			targetDivId = targetDivId.replace(/\]/g, '\\]');

			$("div#WarnDiv" + targetDivId).show();
			errObj.push(thisObj);
		}
	});
	
	if(errObj.length > 0)
	{
		errObj[0].focus();
		return false;
	}
	
	$('form#form1')[0].action = 'index.php?task=mgmt<?=$ercKindergartenConfig['taskSeparator']?>other_info<?=$ercKindergartenConfig['taskSeparator']?>edit_update';
	$('form#form1').submit();
}

function goImport()
{
	$('form#form1')[0].action = 'index.php?task=mgmt<?=$ercKindergartenConfig['taskSeparator']?>other_info<?=$ercKindergartenConfig['taskSeparator']?>import';
	$('form#form1').submit();
}

function goExport()
{
	$('form#form1')[0].action = 'index.php?task=mgmt<?=$ercKindergartenConfig['taskSeparator']?>other_info<?=$ercKindergartenConfig['taskSeparator']?>export';
	$('form#form1').submit();
}

function goBack()
{
	$('form#form1')[0].action = 'index.php?task=mgmt<?=$ercKindergartenConfig['taskSeparator']?>other_info<?=$ercKindergartenConfig['taskSeparator']?>index';
	$('form#form1').submit();
}

function setUploadType(type)
{
	$("input#UploadType").val(type);
	
	$('form#form1')[0].action = 'index.php?task=mgmt<?=$ercKindergartenConfig['taskSeparator']?>other_info<?=$ercKindergartenConfig['taskSeparator']?>index';
 	$('form#form1').submit();
}
</script>

<form name="form1" id="form1" method="POST">
	<div class="table_board">
		<div style="display:none;" id="NeedConfirmMsg">
			<?=$WarningBox?>
		</div>
		<div class="content_top_tool">
			<?=$htmlAry['contentTool']?>
			<br style="clear:both;">
		</div>
		<br style="clear:both;">
		<p class="spacer"></p>
		
		<div class="table_filter">
			<?=$htmlAry['ClassSelection']?>
			<?=$htmlAry['TermSelection']?>
		</div>
		<br style="clear:both;">
		<br style="clear:both;">
		<p class="spacer"></p>
		
		<div id='other_info_div'>
			<?=$htmlAry['TableContent']?>
		</div>
		<br style="clear:both;" />
		<br style="clear:both;" />
	</div>
	
	<?=$htmlAry['hiddenInputField']?>
</form>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td height="4" class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
	</tr>
	<tr>
	  	<td>
	    	<table width="100%" border="0" cellspacing="0" cellpadding="2">
	      		<tr><td align="center" valign="bottom"><?=$htmlAry["button"]?></td></tr>
	  		</table>
	  	</td>
	</tr>
</table>

<textarea style="height: 1px; width: 1px; visibility:hidden;" id="text1" name="text1"></textarea>