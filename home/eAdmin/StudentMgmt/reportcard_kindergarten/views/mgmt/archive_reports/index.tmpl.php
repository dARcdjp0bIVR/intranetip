<script type="text/javascript">
    function js_Reload_Term_Class_Selection()
    {
        $('div.warnMsgDiv').hide();
        $("div#studentSelDiv").html('');

        var dbYearId = $('select#DBYearID').val();
        if(dbYearId)
        {
            var termId = '';
            var classId = '';
            if($('select#TermID').length) {
                termId = $('select#TermID').val();
            }
            if($('select#ClassID').length) {
                classId = $('select#ClassID').val();
            }

            $.post(
                "index.php?task=mgmt<?=$ercKindergartenConfig["taskSeparator"]?>archive_reports<?=$ercKindergartenConfig["taskSeparator"]?>ajax_reload",
                {
                    Action: 'Reload_Term_List',
                    onChange: 'js_Reload_Term_Class_Selection()',
                    DBYearID: dbYearId,
                    TermID: termId,
                    ClassID: classId
                },
                function(ReturnData)
                {
                    $("div#SemesterSelectionDiv").html(ReturnData);
                    js_Reload_Student_Selection();
                }
            )

            $.post(
                "index.php?task=mgmt<?=$ercKindergartenConfig["taskSeparator"]?>archive_reports<?=$ercKindergartenConfig["taskSeparator"]?>ajax_reload",
                {
                    Action: 'Reload_Class_List',
                    onChange: 'js_Reload_Term_Class_Selection()',
                    DBYearID: dbYearId,
                    TermID: termId,
                    ClassID: classId
                },
                function(ReturnData)
                {
                    $("div#ClassSelectionDiv").html(ReturnData);
                    js_Reload_Student_Selection();
                }
            )
        }
    }

    function js_Reload_Student_Selection()
    {
        $('div.warnMsgDiv').hide();
        $("div#studentSelDiv").html('');

        var dbYearId = $('select#DBYearID').val();
        var termId = $("select#TermID").val();
        var classId = $("select#ClassID").val();
        if(dbYearId && termId && classId)
        {
            $.post(
                "index.php?task=mgmt<?=$ercKindergartenConfig["taskSeparator"]?>archive_reports<?=$ercKindergartenConfig["taskSeparator"]?>ajax_reload",
                {
                    Action: 'StudentByClass',
                    DBYearID: dbYearId,
                    TermID: termId,
                    ClassID: classId
                },
                function(ReturnData)
                {
                    $("div#studentSelDiv").html(ReturnData);
                }
            )
        }
    }

    function goSubmit()
    {
        $('div.warnMsgDiv').hide();

        var canSubmit = true;
        var dbYearId = $('select#DBYearID').val();
        if(dbYearId == null || dbYearId == '') {
            canSubmit = false;
            $('div#Year_Warn').show();
        }
        var termId = $('select#TermID').val();
        if(termId == null || termId == '') {
            canSubmit = false;
            $('div#Term_Warn').show();
        }
        var classId = $('select#ClassID').val();
        if(classId == null || classId == '') {
            canSubmit = false;
            $('div#Class_Warn').show();
        }
        var stduentId = $('select#StudentID').val();
        if(stduentId == null || stduentId == '') {
            canSubmit = false;
            $('div#Student_Warn').show();
        }

        if(canSubmit) {
            $('form#form1').submit();
        }
    }
</script>

<form id="form1" method="POST" target="_blank" action="index.php?task=mgmt<?=$ercKindergartenConfig["taskSeparator"]?>archive_reports<?=$ercKindergartenConfig["taskSeparator"]?>print">
    <table class="form_table_v30">
        <tbody>
        <tr>
            <td class="field_title"><?=$Lang["General"]["AcademicYear"]?></td>
            <td>
                <?=$htmlAry["DBYearSelection"]?>
                <div style="display:none;" class="warnMsgDiv" id="Year_Warn">
                    <span class="tabletextrequire">*<?=$Lang["eReportCardKG"]["Setting"]["InputWarning"].$Lang["General"]["AcademicYear"]?></span>
                </div>
            </td>
        </tr>
        <tr>
            <td class="field_title"><?=$Lang["General"]["Term"]?></td>
            <td>
                <div id="SemesterSelectionDiv"></div>
                <div style="display:none;" class="warnMsgDiv" id="Term_Warn">
                    <span class="tabletextrequire">*<?=$Lang["eReportCardKG"]["Setting"]["InputWarning"].$Lang["General"]["Term"]?></span>
                </div>
            </td>
        </tr>
            <tr>
                <td class="field_title"><?=$Lang["General"]["Class"]?></td>
                <td>
                    <div id="ClassSelectionDiv"></div>
                    <div style="display:none;" class="warnMsgDiv" id="Class_Warn">
                        <span class="tabletextrequire">*<?=$Lang["eReportCardKG"]["Setting"]["InputWarning"].$Lang["General"]["Class"]?></span>
                    </div>
                </td>
            </tr>
        <tr>
            <td class="field_title"><?=$Lang["Identity"]["Student"]?></td>
            <td>
                <div id="studentSelDiv"></div>
                <div style="display:none;" class="warnMsgDiv" id="Student_Warn">
                    <span class="tabletextrequire">*<?=$Lang["eReportCardKG"]["Setting"]["InputWarning"].$Lang["Identity"]["Student"]?></span>
                </div>
            </td>
        </tr>
        </tbody>
    </table>
</form>

<div class="edit_bottom_v30">
    <input type="button" value="<?=$Lang["Btn"]["Submit"]?>" class="formbutton_v30 print_hide " onclick="goSubmit()" id="submitBtn" name="submitBtn" <?php echo $disabled?>>
</div>