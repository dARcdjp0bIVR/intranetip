<meta Http-Equiv="Cache-Control" Content="no-cache">

<style>
.ability_box{
 display: block;
 max-height: 100px;
 height: auto;
 overflow-y: auto;
}
</style>

<script type="text/javascript">
$(document).ready(function() {
	$('span.rowNumSpan').each(function() {
		if($(this).text() == '--') {
			$(this).parent().css('display', 'table-cell');
		}
	});
	$('.checkbox').click(function() {
		if($(this).attr('checked')) {
			// do nothing
		}
		else{
			$('#checkmaster').removeAttr('checked');
		}
	});
});

function CheckAll(){
	if($('#checkmaster').attr('checked')) {
		$('.checkbox').attr('checked','checked');
	}
	else {
		$('.checkbox').removeAttr('checked');
	}
}

function goNew() {
	window.location = '?task=settings<?=$ercKindergartenConfig['taskSeparator']?>subject_mapping<?=$ercKindergartenConfig['taskSeparator']?>edit';
}

function goEdit(codeID) {
	window.location = '?task=settings<?=$ercKindergartenConfig['taskSeparator']?>subject_mapping<?=$ercKindergartenConfig['taskSeparator']?>edit&isEdit=1&codeID='+codeID;
}

function Edit(){
	var EditID = [];
	$('.checkbox:checked').each(function() {
		EditID.push ($(this).val());
	});
	
	if(EditID.length == 1) {
		goEdit(EditID[0]);
	}
	else if(EditID.length > 1) {
		alert("<?=$Lang['eReportCardKG']['Setting']['EditWarning']['NoMoreThan1'] ?>");
	}
	else {
		alert("<?=$Lang['eReportCardKG']['Setting']['EditWarning']['PleaseSelectEdit'] ?>");
	}
}

function Delete(){
	var DeleteID = [];
	$('.checkbox:checked').each(function() {
		DeleteID.push($(this).val());
	});
	
	if(DeleteID.length){
		if(confirm('<?= $Lang['General']['JS_warning']['ConfirmDelete'] ?>')){
			$('#form1').attr('action', 'index.php?task=settings.subject_mapping.delete');
			$('#form1').submit();
		}
	}else{ //no click check box
		alert("<?=$Lang['eReportCardKG']['Setting']['DeleteWarning']['PleaseSelectDelete']?>");
	}
}

function changeYear() {
	$('#sortBy').val('');
	$('#sortOrder').val('');
	document.form1.submit();
}

function tableSort(col) {
	var sortArr = ['<?=implode("','",$sortArr)?>'];
	if($('#sortBy').val() == sortArr[col]) {
		$('#sortOrder').val()=='asc'? $('#sortOrder').val('desc') : $('#sortOrder').val('asc');
	} else {
		$('#sortBy').val(sortArr[col]);
		$('#sortOrder').val('desc');
	}
	document.form1.submit();
}
</script>

<form name="form1" id="form1" method="POST">
	<div class="table_board">
		<div class="content_top_tool">
			<?=$htmlAry['contentTool']?>
			<br style="clear:both;">
		</div>
		<br style="clear:both;">
		<p class="spacer"></p>
		
		<div class="table_filter">
			<?=$htmlAry['yearSelection']?>
		</div>
		<p class="spacer"></p>
		
		<?=$htmlAry['dbTableActionBtn']?>
		<?=$htmlAry['dbTable']?>
		<br style="clear:both;" />
		<br style="clear:both;" />
	</div>
	
	<?=$htmlAry['hiddenField']?>
</form>