<?=$htmlAry["Navigation"]?>
<br />

<form id="form1" method="POST" action="index.php?task=settings.subject_mapping.edit_update" enctype="multipart/form-data">
	<div style="display:none;" id="NeedConfirmMsg">
		<?=$WarningBox?>
	</div>
	<table class="form_table_v30">
		<tbody>
		<tr>
			<td class="field_title"><?=$Lang['eReportCardKG']['Setting']['Year'] ?></td>
			<td>
				<?=$htmlAry["yearSelection"]?>
				<div style="display:none;" class="warnMsgDiv" id="YearID_Warn">
					<span class="tabletextrequire">*<?=$Lang['eReportCardKG']['Setting']['InputWarning'].$Lang['eReportCardKG']['Setting']['Year']?></span>
				</div>
			</td>
		</tr>
		<tr>
			<td class="field_title"><?=$Lang['eReportCardKG']['Setting']['SubjectMapping']['TermTimeTable'] ?></td>
			<td>
				<input type="radio" name="isTerm" id="isTerm_Y" value='1'  onclick="TypeOnChange()" <?=(!$TopicID? 'checked' : '')?>/>
				<label for="isTerm_Y"> <?=$Lang['eReportCardKG']['Setting']['SubjectMapping']['Term']?> </label>
				<input type="radio" name="isTerm" id="isTerm_N" value="0" onclick="TypeOnChange()" <?=($TopicID? 'checked' : '')?>/>
				<label for="isTerm_N"> <?=$Lang['eReportCardKG']['Setting']['SubjectMapping']['TimeTable']?> </label>
    			<div style="display:none;" class="warnMsgDiv" id="SubjectTT_Warn">
    				<span class="tabletextrequire" id="SubjectTT_Warn_Text"></span>
    			</div>
			</td>
		</tr>
		<tr id="TermTopic">
		</tr>
		<tr>
			<td class="field_title"><?=$Lang['eReportCardKG']['Setting']['SubjectMapping']['Subject'] ?></td>
			<td>
				<div id="subjectSelection"></div>
    			<div style="display:none;" class="warnMsgDiv" id="subjectID_Warn">
    				<span class="tabletextrequire">*<?=$Lang['eReportCardKG']['Setting']['InputWarning'].$Lang['eReportCardKG']['Setting']['SubjectMapping']['Subject']?></span>
    			</div>
			</td>
		</tr>
		<tr>
			<td class="field_title"><?=$Lang['eReportCardKG']['Setting']['AbilityIndex']?></td>
			<td>
				<div id="thickBox_Div">
					<span class="table_row_tool row_content_tool">
					<?=$htmlAry["ThickBoxLink"]?>
					</span>
				</div>
				<br style="clear:both;">
				
				<div id='cataList'></div>
				<br style="clear:both;">
    			<div style="display:none;" class="warnMsgDiv" id="CataGoryID_Warn">
    				<span class="tabletextrequire">*<?=$Lang['eReportCardKG']['Setting']['InputWarning'].$Lang['eReportCardKG']['Setting']['AbilityIndex']?></span>
    			</div>
			</td>
		</tr>
		</tbody>
	</table>
	
	<?=$htmlAry["hiddenInputField"]?>
</form>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td height="4" class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
	</tr>
	<tr>
	  	<td>
	    	<table width="100%" border="0" cellspacing="0" cellpadding="2">
	      		<tr><td align="center" valign="bottom"><?=$htmlAry["Button"]?></td></tr>
	  		</table>
	  	</td>
	</tr>
</table>

<?=$htmlAry["ThickBox"]?>

<script type="text/javascript">
var errorTypeKey = '';
var errorTypeLangArr = [];
<?php foreach($Lang['eReportCardKG']['Setting']['SubjectMappingJSArr'] as $thisKey => $thisLang) {
    echo "errorTypeLangArr['".$thisKey."'] = '".$thisLang."';\r\n "; 
} ?>
var yearId = $('#YearID').val();

$(document).ready(function(){
	YearOnChange();
	
	var CataID = [];
	if($('#CataGoryID').val()) {
		CataGoryID = $('#CataGoryID').val();
		CataGoryIDArr = CataGoryID.split(','); 
		NumCataGoryID = CataGoryIDArr.length;
		for(i=0; i<NumCataGoryID; i++) {
			CataID.push(CataGoryIDArr[i]);
		}
	}
	numOfCataID = CataID.length;
	$('#CataGoryID').val(CataID);
	
	document.getElementById("cataList").innerHTML = '';
	for(i=0; i<numOfCataID; i++) {
		var html =  "<div id='catagory_" + CataID[i] + "' align=left>" + 
						"<span>" + $("#cata option[value=" + CataID[i] + "]").text() + "</span>" +
						"<span class='table_row_tool row_content_tool'>" + 
							"<a onclick='deleteCata(" + CataID[i] + ")' title='刪除' class='delete' href='javascript:void(0);'></a>" + 
						"</span>" + 
					"</div>" +
					"<div id='br_" + CataID[i] + "'>" + 
						"<br>" + 
					"</div>" ;
		document.getElementById("cataList").innerHTML += html;
	}

    <?php if ($_GET['isEdit']) { ?>
        $('#NeedConfirmMsg').show();
    <?php } ?>
});

function ThickBoxGoSubmit(){
	// Transit data to Form
	var CataID = [];
	if($('#CataGoryID').val()) {
		CataGoryID = $('#CataGoryID').val();
		CataGoryIDArr = CataGoryID.split(','); 
		NumCataGoryID = CataGoryIDArr.length;
		for(i=0; i<NumCataGoryID; i++) {
			CataID.push(CataGoryIDArr[i]);
		}
	}
	
	var cata = $('#cata').val();
	NumOfCata = cata.length;
	for(i=0; i<NumOfCata; i++) {
		// avoid same id added 
		if($.inArray(cata[i],CataID) == '-1' && cata[i] > 0) { 
			CataID.push(cata[i]);
		}
	}
	
	// Displaying in form
	numOfCataID = CataID.length;
	document.getElementById("cataList").innerHTML = '';
	for(i=0; i<numOfCataID; i++) {
		var html =  "<div id='catagory_" + CataID[i] + "' align=left>" + 
						"<span>" + $("#cata_hidden option[value=" + CataID[i] + "]").text() + "</span>" + 
						"<span class='table_row_tool row_content_tool'>" +
							"<a onclick='deleteCata(" + CataID[i] + ")' title='刪除' class='delete' href='javascript:void(0);'></a>" + 
						"</span>" + 
					"</div>" + 
					"<div id='br_" + CataID[i] + "'>" + 
						"<br>" + 
					"</div>" ;
		document.getElementById("cataList").innerHTML += html;
	}
	$('#CataGoryID').val(CataID);
	
	$('#TB_window').fadeOut();
	tb_remove();
}

function ThickBoxClose(){
	 $('#TB_window').fadeOut();
	 tb_remove();
}

var deleting = 0;
function deleteCata(CatID){
	 if(deleting != 1)
	 {
		 deleting = 1;
		 
    	 $('#catagory_' + CatID).fadeOut();
    	 $('#br_' + CatID).fadeOut();
    	 
    	 var str = $('#CataGoryID').val(); //A,B,C
    	 //str = str.replace(CatID,','); // A,,,C
    	 //str = str.replace(',,',''); //A,C
    	 var allVal = str.split(',');
    	 var index = 0;
    	 for(var x in allVal) {
    		if(parseInt(allVal[x]) == parseInt(CatID)) {
    			index = x;
    		}
    	 }
    	 allVal.splice(index,1);
    	 
    	 str = allVal.join(',');
    	 if(str == ',') { 	// avoid only ,
    		 str = '';
    	 }
    	 $('#CataGoryID').val(str);
    	 
    	 setTimeout(function() {
        	 deleting = 0;
    	 }, 1000);
	 }
}

function checkSubjectTT(){
	var CodeID = $('#CodeID').val();
	var YearID = $('#YearID').val();
	var SubjectID = $('#subjectID').val();
	var isTargetTerm = $("input[name='isTerm']:checked").val();
	if(isTargetTerm == "1") {
		var TermID = $('#TermID').val();
		if(TermID != '' && SubjectID != '') {
			var data = {
				CodeID: CodeID,
				YearID: YearID,
				TermID: TermID,
				SubjectID: SubjectID
			};
		}
		if(TermID == '') {
			errorTypeKey = 'TermEmpty';
		}
	} else {
		var TopicID = $('#topicID').val();
		if(TopicID != '' && SubjectID != '') {
			var data = {
				CodeID: CodeID,
				YearID: YearID,
				TopicID: TopicID,
				SubjectID: SubjectID
			};
		}
		if(TopicID == '') {
			errorTypeKey = 'TopicEmpty';
		}
	}
	
	if(data) {
		data.action = 'checkSubjectTT';
    	$.ajax({
    		method: 'post',
    		url: 'index.php?task=settings.subject_mapping.ajax_subject',
    		data: data,
    		success: function(res){
    			errorTypeKey = res;
    		}
    	});
	}
}

function checkForm(){
	var check = true;
	if(!$('#YearID').val()) {
		$('#YearID').focus();
		check = false;
	}
	if(!$('#subjectID').val()) {
		$('#subjectID_Warn').show();
		$('#subjectID').focus();
		check = false;
	}
	if(!$('#CataGoryID').val()){
		$('#CataGoryID_Warn').show();
		check = false;
	}
	
	return check;
}

function goSubmit(){
	$('.warnMsgDiv').hide();
	$('#submitBtn').attr('disabled', true);
	$('#backBtn').attr('disabled', true);
	
	checkSubjectTT();
	setTimeout(function(){
		if(checkForm() && errorTypeKey == '') {
			form1.submit();
		} else {
			if(errorTypeKey != '') {
				$('#SubjectTT_Warn').show();
				$('#SubjectTT_Warn_Text').html('*' + errorTypeLangArr[errorTypeKey]);
			}
			
			$('#submitBtn').attr('disabled', false);
			$('#backBtn').attr('disabled', false);
			return false;
		}
	 }, 1000);
}

function goBack(){
	 window.location.href = "index.php?task=settings.subject_mapping.list";
}

function YearOnChange(){
	$('#thickBox_Div').hide();

	var catagoryListDiv = $('#cataList');
	var subjectSelection = $('#subjectSelection');
	subjectSelection.html('');
	
	var YearID = $('#YearID').val();
	if(YearID != "") {
		$('#thickBox_Div').show();
		if(yearId != YearID) {
    		catagoryListDiv.html('');
    		$('#CataGoryID').val('');
		}
		yearId = YearID;
		
    	$.ajax({
    		method: 'post',
    		url: 'index.php?task=settings.subject_mapping.ajax_subject',
    		data: {
    			"action" : 'yearSubject',
    			"yearID" : YearID,
    			"subjectID" : "<?=$subjectID?>"
    		},
    		success: function(response){
    			subjectSelection.html(response);
    			TypeOnChange();
    			CatagoryOnChange();
    		}
    	});
	}
}

function TypeOnChange(){
	var targetRow = $('#TermTopic');
	targetRow.html('');
	
	var YearID = $('#YearID').val();
	var isTargetTerm = $("input[name='isTerm']:checked").val();
	if(YearID != "" && isTargetTerm != undefined) {
		var targetAction = "yearTerm";
		if(isTargetTerm == "0") {
			targetAction = "yearTopic";
		}
		
		$.ajax({
			method: 'post',
			url: 'index.php?task=settings.subject_mapping.ajax_subject',
			data :{
				"action" : targetAction,
				"yearID" : YearID,
				"TermID" : "<?=$TermID?>",
				"TopicID" : "<?=$TopicID?>"
			},
			success: function(response){
				targetRow.html(response);
			}
		});
	}
}

function CatagoryOnChange(){
	var categorySelection = $("#CataSelectionBox");
	categorySelection.html('');
	
	var selectTWCat = $('#selectTWCat').val();
	var YearID = $('#YearID :selected').val();
	var YearText = $('#YearID :selected').text();
	if(YearID != "" && YearText != "") {
    	$.ajax({
    		type: "POST",
    		url: "index.php?task=settings.subject_mapping.ajax_cata",
    		data: {
    			"selectTWCat": selectTWCat,
    			"YearID": YearText
    		},
    		success: function (msg) {
    			$("#CataSelectionBox").html(msg);
    		}
    	});
	}
}
</script>