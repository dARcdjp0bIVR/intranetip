<?php
/*
 *  Change Log:
 *  Date: 2019-09-17 Bill : added js function goDelete() - to support delete photo
 *  Date: 2017-02-09 Villa: Modified GoBack();
 *  Date: 2017-01-26 Villa: Open the File
 */
?>

<?=$NavigationBar?>
<br> 
<br>

<form id="form1" method="POST" action="index.php?task=settings.teaching_tool.edit_update" enctype="multipart/form-data">
	<div style="display:none;" id="NeedConfirmMsg">
		<?=$WarningBox?>
	</div>
	<table class="form_table_v30">
		<tbody>
		
		<?php if($codeID > 0) { ?>
    		<tr>
    			<td class="field_title"><?=$Lang['eReportCardKG']['Setting']['Code']?></td>
    			<td><?=$code?></td>
    			<!--
    				<input type="text" maxlength="255" class="textboxnum requiredField" name="Code" id="Code" value="<?=$code?>">
    				<div style="display:none;" class="warnMsgDiv" id="Code_Warn">
    					<span class="tabletextrequire">*<?=$Lang['eReportCardKG']['Setting']['InputWarning'].$Lang['eReportCardKG']['Setting']['Code']?></span>
    				</div>
    			</td>
                -->
    		</tr>
		<?php } ?>
		
		<tr>
			<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang['eReportCardKG']['Setting']['NameB5']?></td>
			<td>
				<input type="text" maxlength="255" class="textboxtext requiredField" name=CH_Name id="CH_Name" value="<?=$nameCh?>">
				<div style="display:none;" class="warnMsgDiv" id="CH_Name_Warn">
					<span class="tabletextrequire">*<?=$Lang['eReportCardKG']['Setting']['InputWarning'].$Lang['eReportCardKG']['Setting']['NameB5']?></span>
				</div>
			</td>
		</tr>
		
		<tr>
			<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang['eReportCardKG']['Setting']['NameEN']?></td>
			<td>
				<input type="text" maxlength="255" class="textboxtext requiredField" name="EN_Name" id="EN_Name" value="<?=$nameEn?>">
				<div style="display:none;" class="warnMsgDiv" id="EN_Name_Warn">
					<span class="tabletextrequire">*<?=$Lang['eReportCardKG']['Setting']['InputWarning'].$Lang['eReportCardKG']['Setting']['NameEN']?></span>
				</div>
			</td>
		</tr>
		
		<tr>
			<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang['eReportCardKG']['Setting']['Year'] ?></td>
			<td>
				<?=$YearSelection?>
				<div style="display:none;" class="warnMsgDiv" id="YearID_Warn">
					<span class="tabletextrequire">*<?=$Lang['eReportCardKG']['Setting']['InputWarning'].$Lang['eReportCardKG']['Setting']['Year']?></span>
				</div>
			</td>
		</tr>
		
		<!-- 
		<tr>
			<td class="field_title"><?=$Lang['eReportCardKG']['Setting']['ToolCategory']?></td>
			<td>
				<?=$CategorySelection?>
			</td>
		</tr>
		 -->
		 
		 <tr>
			<td class="field_title"><?=$Lang['eReportCardKG']['Setting']['Chapter']?></td>
			<td>
				<input type="text" max-length="255" class="textboxtext requiredField" name="chapter" id="chapter" value="<?=$chapter?>" />
			</td>
		</tr>
		 
		<tr>
			<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang['eReportCardKG']['Setting']['LearningZone']['Title']?></td>
			<td>
				<div id='zoneSelect'><?=$ZoneSelection?></div>
				<div style="display:none;" class="warnMsgDiv" id="ZoneID_Warn">
					<span class="tabletextrequire">*<?=$Lang['eReportCardKG']['Setting']['InputWarning'].$Lang['eReportCardKG']['Setting']['LearningZone']['Title']?></span>
				</div>
			</td>
		</tr>
		
		<tr>
			<td class="field_title"><?=$Lang['eReportCardKG']['Setting']['Photo']?><br><span class="tabletextremark"><?=$Lang['eReportCardKG']['Setting']['PhotoRemarks']?></span></td>
			<td>
				<input type="file" accept="image/*" name="fileToUpload" id="fileToUpload">
				<br>
				<!-- Display Image -->
				<?php if($IsPhotoDispaly){?>
					<div id='imageDiv'>
						<?=$photoDispaly?>
						<br>
						<span class="table_row_tool row_content_tool"><a onclick="goDelete();" title="刪除" class="delete" href="javascript:void(0);"></a></span><br style="clear:both;">
					</div>
				<?php }?>
			</td>
		</tr>
		
		<tr>
			<td class="field_title"><?=$Lang['eReportCardKG']['Setting']['AbilityIndex']?></td>
			<td>
				<div id="thickBox_Div">
					<span class="table_row_tool row_content_tool">
					<?=$AddCata?>
					</span>
				</div>
				<br style="clear:both;">
				<div id='cataList'></div>
				<br style="clear:both;">
			</td>
		</tr>
		
		<tr>
			<td class="field_title"><?=$Lang['eReportCardKG']['Setting']['Remarks'] ?></td>
			<td>
				<textarea wrap="virtual" onfocus="this.rows=5; " rows="2" cols="70" id="Remarks" name="Remarks" class="tabletext requiredField"><?=$remarks?></textarea>
				<div style="display:none;" class="warnMsgDiv" id="textareaEmptyWarnDiv">
					<span class="tabletextrequire">*<?=$Lang['eReportCardKG']['Setting']['InputWarning'].$Lang['eReportCardKG']['Setting']['Remarks']?></span>
				</div>
			</td>
		</tr>
		
		</tbody>
	</table>
	
	<input type='hidden' id='CodeID' name='CodeID' value='<?=$codeID?>' >
	<input type='hidden' id='PhotoPath' name='PhotoPath' value='<?=$photoPath?>' >
	<input type='hidden' id='goDelete' name='goDelete' value='0' >
	<input type='hidden' id='CataGoryID' name='CataGoryID' value='<?=$catagory?>' >
</form>

<?=$thickBox?>

<div class="edit_bottom_v30">
	<p class="spacer"></p>
	<input type="button" value="<?=$Lang['Btn']['Submit']?>" class="formbutton_v30 print_hide " onclick="goSubmit()" id="submitBtn" name="submitBtn">
	<input type="button" value="<?=$Lang['Btn']['Back']?>" class="formbutton_v30 print_hide " onclick="goBack()" id="backBtn" name="backBtn">
	<p class="spacer"></p>
</div>

<script>
$(document).ready(function(){
	$('#YearID').change();
	
	var CataID = [];
	if($('#CataGoryID').val()) {
		CataGoryID = $('#CataGoryID').val();
		CataGoryIDArr = CataGoryID.split(','); 
		NumCataGoryID = CataGoryIDArr.length;
		for(i=0; i<NumCataGoryID; i++) {
			CataID.push(CataGoryIDArr[i]);
		}
	}
	numOfCataID = CataID.length;
	$('#CataGoryID').val(CataID);
	
	document.getElementById("cataList").innerHTML = '';
	for(i=0; i<numOfCataID; i++){
		var html =  "<div id='catagory_" + CataID[i] + "' align=left>" + 
						"<span>" + $("#cata option[value=" + CataID[i] + "]").text() + "</span>" +
						"<span class='table_row_tool row_content_tool'>" +
							"<a onclick='deleteCata(" + CataID[i] + ")' title='刪除' class='delete' href='javascript:void(0);'></a>"+
						"</span>" +
					"</div>" +
					"<div id='br_" + CataID[i] + "'>" +
						"<br>" +
					"</div>" ;
		document.getElementById("cataList").innerHTML += html;
	}

    <?php if ($_GET['isEdit']) { ?>
	    $('#NeedConfirmMsg').show();
	<?php } ?>
});

function checkForm(){
	$('.warnMsgDiv').hide();
// 	$('#EN_Name_Warn').hide();
// 	$('#B5_Name_Warn').hide();
// 	$('#Code_Warn').hide();

	var check = true;

	if(!$('#ZoneID').val()){
		$('#ZoneID_Warn').show();
		$('#ZoneID').focus();
		check = false;
	}
	if(!$('#YearID').val()){
		$('#YearID_Warn').show();
		$('#YearID').focus();
		check = false;
	}
	if(!$('#EN_Name').val()){
		$('#EN_Name_Warn').show();
		$('#EN_Name').focus();
		check = false;
	}
	if(!$('#CH_Name').val()){
		$('#CH_Name_Warn').show();
		$('#CH_Name').focus();
		check = false;
	}
// 	if(!$('#Code').val()){
// 		$('#Code_Warn').show();
// 		$('#Code').focus();
// 		check = false;
// 	}
	return check;
}

function goSubmit(){
	if(checkForm()){
		$('#submitBtn').attr('disabled', true);
		$('#backBtn').attr('disabled', true);
		
		form1.submit();
	}
}

function goBack(){
	 window.location.href = "index.php?task=settings.teaching_tool.list";
}

function goDelete(){
    $('div#imageDiv').hide();
    $('input#goDelete').val(1);
}

function ThickBoxGoSubmit(){
	// Transit data to Form
	var CataID = [];
	if($('#CataGoryID').val()){
		CataGoryID = $('#CataGoryID').val();
		CataGoryIDArr = CataGoryID.split(','); 
		NumCataGoryID = CataGoryIDArr.length;
		for(i=0; i<NumCataGoryID; i++) {
			CataID.push(CataGoryIDArr[i]);
		}
	}
	
	var cata = $('#cata').val();
	NumOfCata = cata.length;
	for(i=0; i<NumOfCata; i++){
		// avoid same id added 
		if($.inArray(cata[i],CataID) == '-1' && cata[i] > 0) { 
			CataID.push(cata[i]);
		}
	}
	
	// Displaying in form
	numOfCataID = CataID.length;
	document.getElementById("cataList").innerHTML = '';
	for(i=0;i<numOfCataID;i++){
		var html =  "<div id='catagory_" + CataID[i] + "' align=left>" +
						"<span>" + $("#cata_hidden option[value=" + CataID[i] + "]").text() + "</span>" +
						"<span class='table_row_tool row_content_tool'>" +
							"<a onclick='deleteCata(" + CataID[i] + ")' title='刪除' class='delete' href='javascript:void(0);'></a>"+
						"</span>" +
					"</div>" +
					"<div id='br_" + CataID[i] + "'>" +
						"<br>" +
					"</div>" ;
		document.getElementById("cataList").innerHTML += html;
	}
	$('#CataGoryID').val(CataID);
	
	$('#TB_window').fadeOut();
	tb_remove();
}

function ThickBoxClose(){
	 $('#TB_window').fadeOut();
	 tb_remove();
}

var deleting = 0;
function deleteCata(CatID){
	if(deleting != 1) {
        deleting = 1;
        
        $('#catagory_'+CatID).fadeOut();
        $('#br_'+CatID).fadeOut();
        
        var str = $('#CataGoryID').val(); //A,B,C
        //str = str.replace(CatID,','); // A,,,C
        //str = str.replace(',,',''); //A,C
        var allVal = str.split(',');
        var index = 0;
        for(var x in allVal) {
            if(parseInt(allVal[x]) == parseInt(CatID)) {
            	index = x;
            }
        }
        allVal.splice(index, 1);
        
        str = allVal.join(',');
        if(str == ',') { //avoid only ,
        	str = '';
        }
        $('#CataGoryID').val(str);
        
        setTimeout(function(){
        	deleting = 0;
        }, 1000);
	}
}

function Catagory_OnChange(){
	var selectTWCat = $('#selectTWCat').val();
	var YearID = $('#YearID :selected').val();
	var YearText = $('#YearID :selected').text();
	$.ajax({
		type: "POST",
		url: "index.php?task=settings.teaching_tool.ajax_cata",
		data: {
			"selectTWCat": selectTWCat,
			"YearID": YearText
		},
		success: function (msg) {
			$("#CataSelectionBox").html(msg);
		}
	});
}

function Zone_OnChange(){
	var YearID = $('#YearID :selected').val();
	if(YearID == '') {
		YearID = -1;
	}
	$.ajax({
		type: "POST",
		url: "index.php?task=settings.teaching_tool.ajax_zone",
		data: {
			"YearID": YearID,
			"ZoneID": '<?=$zoneID?>'
		},
		success: function (msg) {
			$("#zoneSelect").html(msg);
		}
	});
}

function YearOnChange(){
	Zone_OnChange();
	
	var YearID = $('#YearID').val();
	if(YearID){
		$('#thickBox_Div').show();
		Catagory_OnChange();
		return true;
	}
	else{
		$('#thickBox_Div').hide();
		return false;
	}
}
</script>