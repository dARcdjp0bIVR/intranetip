<script language="javascript">
    $(document).ready( function() {	
    	//
    });

    function addNewMember(){
    	load_dyn_size_thickbox_ip('<?php echo $Lang['Btn']['Edit']?>', 'onloadThickBox();', inlineID='', defaultHeight=500, defaultWidth=800);
    }

    function onloadThickBox() {
//    	$.post(
//    		"thickbox_content.php",
//    		{
//    			academicYearId: 1,
//    			yearTermId: 2
//    		},
//    		function(ReturnData) {
//    			$('div#TB_ajaxContent').html(ReturnData);
//    			adjust_dyn_size_thickbox_content_height_ip('thickboxContainerDiv', 'thickboxContentDiv', 'editBottomDiv');
//    		}
//    	);
		$('.tableForm').attr('name','').attr('id','');
    	let data = {
			AdminGroupID : $('#AdminGroupID').val(),
			FromNew : $('#FromNew').val()
		};
    	$('div#TB_ajaxContent').load(
    		"?task=settings.admin_group.tb_member", 
    		data,
    		function(ReturnData) {
    			adjust_dyn_size_thickbox_content_height_ip('thickboxContainerDiv', 'thickboxContentDiv', 'editBottomDiv');
    		}
    	);
    }

    function js_Back_To_Admin_Group_List()
    {
    	window.location = '?task=settings.admin_group.list';
    }
</script>

<form id="form1" name="form1" class="tableForm" method="post" action="?task=settings.admin_group.member">
	<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td align="left" class="navigation">
				<?php echo $htmlAry['PAGENAV']?>
			</td>
		</tr>
		<tr>
			<td>
				<?php echo $htmlAry['StepTable']?>
			</td>
		</tr>
	</table>
	
	<?php echo $htmlAry['InfoTable']?>
	<br style="clear:both;" />
	
	<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="2">
		<tr>
			<td>
				<table cellspacing="0" cellpadding="0" border="0" width="100%">
					<tr>
						<td>
							<div class="content_top_tool">
								<div class="Conntent_tool">
									<?php echo $htmlAry['addLink']?>
								</div>
							</div>
						</td>		
						<td align="right">
							<?php echo $htmlAry['box_search']?>
						</td>
					</tr>
					<tr>
						<td colspan="2" align="right">
							<div class="common_table_tool">
						        <?php echo $htmlAry['btn_delete']?>
							</div>		
						</td>		
					</tr>
					<tr>
						<td colspan="2">
							<div id="MemberDiv">
								<?php echo $htmlAry['table']?>
								<?php echo $htmlAry['hidden']?>
							</div>
						</td>		
					</tr>
				</table>
			</td>		
		</tr>
	</table>	
	
	<div class="edit_bottom_v30">
		<?php echo $htmlAry['btn_back']?>
	</div>
</form>

<?php echo $htmlAry['thickbox']?>