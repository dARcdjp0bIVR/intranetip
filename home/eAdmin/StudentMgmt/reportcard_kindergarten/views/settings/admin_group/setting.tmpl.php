<script type="text/javascript">
	$(document).ready(function(){
	    //
	});

	<?php echo $htmlAry['jsWarnMsgArr']?>
	
	function js_Update_Group_Info()
	{
		var jsAdminGroupID = $('input#AdminGroupID').val();
		var jsFormValid = true;

		// check info empty
		$("input.required").each(function(){
			var thisID = $(this).attr("id");
			var WarnMsgDivID = thisID + 'WarningDiv';

			if($(this).val().Trim() == '')
			{
				$("div#" + WarnMsgDivID).html(jsWarnMsgArr[thisID]).show();
				$("input#" + thisID).focus();
				jsFormValid = false;
			}
			else
			{
				$("div#" + WarnMsgDivID).hide();
			}
		});
		
		// ajax check code
		var jsCode = $("input#AdminGroupCode").val().Trim();
		if(jsFormValid && jsCode != '')
		{
			if(valid_code(jsCode) == false)
			{
				$("div#AdminGroupCodeWarningDiv").html('<?=$Lang['General']['JS_warning']['CodeMustBeginWithLetter']?>').show();
				$("input#AdminGroupCode").focus();
				jsFormValid = false;
			}
			else
			{
				$.post(
					"?task=settings.admin_group.setting_update",
					{
						Action: "AdminGroupCode",
						AdminGroupCode: jsCode,
						AdminGroupID: jsAdminGroupID
					},
					function (ReturnData)
					{
						ReturnData = JSON.parse(ReturnData);
						console.log(ReturnData);
						if(ReturnData.err == 0)
						{
							$("div#AdminGroupCodeWarningDiv").hide();
							$("form#form1").attr('action', '?task=settings.admin_group.setting_update').submit();
						}
						else
						{
							$("div#AdminGroupCodeWarningDiv").html(jsWarnMsgArr["CodeInUse"]).show();
							$("input#AdminGroupCode").focus();
							jsFormValid = false;
						}
					}
				);
			}
		}
	}

	function js_Back_To_Admin_Group_List()
	{
		window.location = '?task=settings.admin_group.list';
	}

	function js_Select_All_Checkboxes(jsClass)
	{
		$('input.' + jsClass).attr('checked', $('input#' + jsClass + '_Chk').attr('checked'));
	}
</script>

<form id="form1" name="form1" method="post">
    <table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
        <tr>
            <td align="left" class="navigation">
            	<?php echo $htmlAry['PageNav']?>
            </td>
        </tr>
        <tr>
            <td><?php echo $htmlAry['StepTable']?></td>
        </tr>
    </table>
    
    <?php echo $htmlAry['NAV']['AdminGroupInfo']?>
    <table width="100%" cellpadding="2" class="form_table_v30">
        <col class="field_title">
        <col class="field_c">
        	
        <?php echo $htmlAry['FormItem']?>
    </table>
	<br style="clear:both;" />

	<?php echo $htmlAry['NAV']['AccessRight']?>
	<br style="clear:both;" />

	<?php echo $htmlAry['AccessRightTable']?>

    <div class="edit_bottom_v30">
    	<?php echo $htmlAry['buttonBar']?>
    </div>
	
	<?php echo $htmlAry['hidden']?>
</form>