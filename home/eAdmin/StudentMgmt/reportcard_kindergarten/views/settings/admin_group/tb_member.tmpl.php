<?php echo $htmlAry['js_css']?>
<script language="javascript">

var jsAdminGroupID = '';
$(document).ready( function() {	
	// initialize jQuery Auto Complete plugin
	jsAdminGroupID = $('input#AdminGroupID').val();

	$('#TB_window').unload(function(){
		$('.tableForm').attr('name','form1').attr('id','form1');
	});
	
	//Init_JQuery_AutoComplete('UserSearchTb');
	
	//$('input#UserSearchTb').focus();
});

function js_submit(){
	$('select option').attr('selected','selected');
	document.form1.submit();
}

/*var AutoCompleteObj;
function Init_JQuery_AutoComplete(InputID) {
	AutoCompleteObj = $("input#" + InputID).autocomplete("ajax_search_user.php",
		{  			
			onItemSelect: function(li) {
				Add_Selected_User(li.extra[1],li.selectValue);
			},
			formatItem: function(row) {
				return row[0] + " (LoginID: " + row[1] + ")";
			},
			maxItemsToShow: 5,
			minChars: 1,
			delay: 0,
			width: 200
		}
	);
	
	Update_Auto_Complete_Extra_Para();
}

function Add_Selected_User(UserID, UserName) {
	var UserID = UserID || "";
	var UserName = UserName || "";
	var UserSelected = document.getElementById('UserIDArr[]');

	UserSelected.options[UserSelected.length] = new Option(UserName, UserID);
	Update_Auto_Complete_Extra_Para();
	
	$('input#UserSearchTb').val('').focus();
}

function Update_Auto_Complete_Extra_Para() {
	ExtraPara = new Array();
	ExtraPara['SelectedUserIDList'] = Get_Selection_Value('UserIDArr[]', 'Array', true);
	ExtraPara['AdminGroupID'] = jsAdminGroupID;
	AutoCompleteObj[0].autocompleter.setExtraParams(ExtraPara);
}

function js_Add_Member()
{
	var objForm = document.getElementById('form1');
	
	// Check if the selections contains current members of this group
	var jsSelectedUserList = Get_Selection_Value('UserIDArr[]', 'String', true);
	
	$.post(
		"ajax_validate.php", 
		{
			Action: "Validate_Selected_Member", 
			AdminGroupID : jsAdminGroupID,
			SelectedUserList : jsSelectedUserList
		},
		function(ReturnData)
		{
			if (ReturnData == '')
			{
				$('div#MemberSelectionWarningDiv').hide();
				checkOptionAll(objForm.elements["UserIDArr[]"]);
				objForm.action = 'new_step2_update.php';
				objForm.submit();
			}
			else
			{
				var OptionValueArr = ReturnData.split(',');
				var numOfOption = OptionValueArr.length;
				
				for (i=0; i<numOfOption; i++)
					$("select#UserIDArr\\[\\] option[value='" + OptionValueArr[i] + "']").attr('selected', 'selected');
					 
				$('div#MemberSelectionWarningDiv').show();
			}
		}
	);
}*/

function js_Back_To_Admin_Group_List()
{
	// close thickbox
}

function js_Back_To_Member_List()
{
	// submit
	var objForm = document.getElementById('form1');
	objForm.action = 'member_list.php';
	objForm.submit();
}
</script>

<form id="form1" name="form1" method="post" action="?task=settings.admin_group.member_update">
	<?php echo $htmlAry['InfoTable']?>
    <table width="99%" border="0" cellpadding"0" cellspacing="0" align="center">
        <tr>
            <td class="tabletext" width="40%"><?php echo $Lang['General']['ChooseUser'] ?></td>
            <td class="tabletext">
            	<?php echo $htmlAry['icon']?>
            </td>
            <td class="tabletext" width="60%"><?php echo $Lang['General']['SelectedUser'] ?></td>
        </tr>
        <tr>
            <br/>
            <td class="tablerow2" valign="top">
                <table width="100%" border="0" cellpadding="3" cellspacing="0">
                    <tr>
                    	<td class="tabletext"><?php echo $Lang['General']['FromGroup']?></td>
                    </tr>
                    <tr>
                    	<td class="tabletext"><?php echo $htmlAry['btn_choose']?></td>
                    </tr>
                    <!--<tr>
                    	<td class="tabletext"><i><?php echo strtolower($Lang['General']['Or'])?></i></td>
                    </tr>
                    <tr>
                    	<td class="tabletext">
                    		<?php echo $Lang['General']['SearchByLoginID']?>
                    	<br />
                    		<?php echo $htmlAry['input_userlogin']?>
                    	</td>
                    </tr>-->
                </table>
            </td>
            <td class="tabletext" ><?php echo $htmlAry['icon']?></td>
            <td align="left" valign="top">
                <table width="100%" border="0" cellpadding="5" cellspacing="0">
                    <tr>
                        <td align="left">
                            <?php echo $htmlAry['box_selection']?>
                            <?php echo $htmlAry['btn_remove']?>
                    	</td>
                    </tr>
                    <tr>
                        <td>
                        	<?php echo $htmlAry['div_warning']?>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr><td>&nbsp;</td></tr>
        <tr><td colspan="4"><div class="edit_bottom"><br /><?php echo $htmlAry['btn_submit']?>&nbsp;<?php $htmlAry['btn_back']?></div></td></tr>
    </table>

	<?php echo $htmlAry['hidden']?>
</form>