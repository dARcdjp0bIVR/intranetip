<script type="text/javascript">
$( document ).ready(function() {
	$('.checkbox').click(function(){
		if($(this).attr('checked')){
			// do nothing
		}
		else{
			$('#checkmaster').removeAttr('checked');
		}
	});
});

function CheckAll(){
	if($('#checkmaster').attr('checked')){
		$('.checkbox').attr('checked','checked');
	}
	else{
		$('.checkbox').removeAttr('checked');
	}
}

function goNew() {
	window.location = '?task=settings<?=$ercKindergartenConfig['taskSeparator']?>award_list<?=$ercKindergartenConfig['taskSeparator']?>edit';
}

function goEdit(awardID) {
	window.location = '?task=settings<?=$ercKindergartenConfig['taskSeparator']?>award_list<?=$ercKindergartenConfig['taskSeparator']?>edit&isEdit=1&awardID='+awardID;
}

function Edit(){
	var EditID = [];
	$('.checkbox:checked').each(function(){
		EditID.push($(this).val());
	});
	
	if(EditID.length==1){
		goEdit(EditID[0]);
	}
	else if(EditID.length>1){
		alert("<?=$Lang['eReportCardKG']['Setting']['PleaseOneOption']?>");
	}
	else{
		alert("0");
	}
}

function Delete(){
	var DeleteID = [];
	$('.checkbox:checked').each(function(){
		DeleteID.push ($(this).val());
	});
	
	if(DeleteID.length){
		if(confirm('<?= $Lang['General']['JS_warning']['ConfirmDelete'] ?>')){
			$('#form1').attr('action', 'index.php?task=settings.award_list.delete');
			$('#form1').submit();
		}
	}else{ //no click check box
		alert("<?=$Lang['eReportCardKG']['Setting']['DeleteWarning']['PleaseSelectDelete']?>");
	}
}
</script>

<form name="form1" id="form1" method="POST">
	<div class="table_board">
		<div class="content_top_tool">
			<?=$htmlAry['contentTool']?>
			<?=$htmlAry['searchBox']?>
			<br style="clear:both;">
		</div>
		<br style="clear:both;">
		<p class="spacer"></p>
		
		<?=$htmlAry['dbTableActionBtn']?>
		<?= $table?>
		<br style="clear:both;" />
		<br style="clear:both;" />
		
		<?=$htmlAry['dndTable']?>
	</div>
	
	<?=$htmlAry['hiddenField']?>
</form>