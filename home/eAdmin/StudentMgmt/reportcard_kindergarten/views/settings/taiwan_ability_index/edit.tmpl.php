<?=$NavigationBar?>
<br> 
<br>

<form id="form1" method="POST" action="index.php?task=settings.taiwan_ability_index.edit_update" enctype="multipart/form-data">
	<div style="display:none;" id="NeedConfirmMsg">
		<?=$WarningBox?>
	</div>
	<table class="form_table_v30">
		<tbody>
		<tr>
			<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang['eReportCardKG']['Setting']['TaiwanAbilityIndex']['Code']?></td>
			<td><?=$IndexCode?></td>
		</tr>
		<tr>
			<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang['eReportCardKG']['Setting']['TaiwanAbilityIndex']['Name']?></td>
			<td>
				<input type="text" maxlength="255" class="textboxtext requiredField" name="IndexName" id="IndexName" value="<?=$IndexName?>">
				<div style="display:none;" class="warnMsgDiv" id="IndexName_Warn">
					<span class="tabletextrequire">* <?=$Lang['eReportCardKG']['Setting']['PleaseInputContent']?></span>
				</div>
			</td>
		</tr>
	</table>
	<input type='hidden' id='IndexID' name='IndexID' value=<?=$IndexID?> >
</form>

<div class="edit_bottom_v30">
	<p class="spacer"></p>
	<input type="button" value="<?=$button_submit?>" class="formbutton_v30 print_hide " onclick="goSubmit()" id="submitBtn" name="submitBtn">
	<input type="button" value="<?=$button_back?>" class="formbutton_v30 print_hide " onclick="goBack()" id="backBtn" name="backBtn">
	<p class="spacer"></p>
</div>

<script>
function checkForm(){
	$('#IndexName_Warn').hide();

	var check = true;
	if(!$('#IndexName').val()){
		$('#IndexName_Warn').show();
		$('#IndexName').focus();
		check = false;
	}
	
	return check;
}

function goSubmit(){
	if(checkForm()){
		form1.submit();
	}
}

function goBack(){
	 window.location.href = "index.php?task=settings.taiwan_ability_index.list";
}

function goURL(){
	window.location = '?task=settings<?=$ercKindergartenConfig['taskSeparator']?>taiwan_ability_index<?=$ercKindergartenConfig['taskSeparator']?>list';
}
$(document).ready(function(){
	$('#NeedConfirmMsg').show();
});
</script>