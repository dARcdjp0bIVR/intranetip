<script type="text/javascript">
$(document).ready( function() {
	
});

//function goNew() {
//	window.location = '?task=settings<?=$ercKindergartenConfig['taskSeparator']?>teaching_tool<?=$ercKindergartenConfig['taskSeparator']?>edit';
//}

function goEdit(CodeID) {
	window.location = '?task=settings<?=$ercKindergartenConfig['taskSeparator']?>taiwan_ability_index<?=$ercKindergartenConfig['taskSeparator']?>edit&isEdit=1&CodeID='+CodeID;
}

function checkEdit(){
	var EditID = [];
	$('.checkbox:checked').each(function(){
		EditID.push($(this).val());
	});
	if(EditID.length==1){
		goEdit(EditID[0]);
	}
	else if(EditID.length>1){
		alert("<?=$Lang['eReportCardKG']['Setting']['EditWarning']['NoMoreThan1'] ?>");
	}
	else{
		alert("<?=$Lang['eReportCardKG']['Setting']['EditWarning']['PleaseSelectEdit'] ?>");
	}
}
</script>

<form name="form1" id="form1" method="POST">
	<div class="table_board">
		<div class="content_top_tool">
			<!--<?=$htmlAry['contentTool']?>-->
			<?=$htmlAry['searchBox']?>
			<br style="clear:both;">
		</div>
		<br style="clear:both;">
		<p class="spacer"></p>
		
		<?=$htmlAry['dbTableActionBtn']?>
		<?=$htmlAry['dataTable']?>
	</div>
	
	<?=$htmlAry['hiddenField']?>
</form>