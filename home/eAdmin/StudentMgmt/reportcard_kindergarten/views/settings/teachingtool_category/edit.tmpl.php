<?php 
/*
 *  Change Log:
 *  Date: 2017-10-17 Bill: Create File
 */
?>

<?=$NavigationBar?>
<br> 
<br>

<form id="form1" method="POST" action="index.php?task=settings.teachingtool_category.edit_update" enctype="multipart/form-data">
	<table class="form_table_v30">
		<tbody>
		<tr>
			<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang['eReportCardKG']['Setting']['Code']?></td>
			<td>
				<input type="text" maxlength="255" class="textboxnum requiredField" name="Code" id="Code" value="<?=$code?>">
				<div style="display:none;" class="warnMsgDiv" id="Code_Warn">
					<span class="tabletextrequire">*<?=$Lang['eReportCardKG']['Setting']['InputWarning'].$Lang['eReportCardKG']['Setting']['Code']?></span>
				</div>
			</td>
		</tr>
		
		<tr>
			<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang['eReportCardKG']['Setting']['NameB5']?></td>
			<td>
				<input type="text" maxlength="255" class="textboxtext requiredField" name=CH_Name id="CH_Name" value="<?=$nameCh?>">
				<div style="display:none;" class="warnMsgDiv" id="CH_Name_Warn">
					<span class="tabletextrequire">*<?=$Lang['eReportCardKG']['Setting']['InputWarning'].$Lang['eReportCardKG']['Setting']['NameB5']?></span>
				</div>
			</td>
		</tr>
		
		<tr>
			<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang['eReportCardKG']['Setting']['NameEN']?></td>
			<td>
				<input type="text" maxlength="255" class="textboxtext requiredField" name="EN_Name" id="EN_Name" value="<?=$nameEn?>">
				<div style="display:none;" class="warnMsgDiv" id="EN_Name_Warn">
					<span class="tabletextrequire">*<?=$Lang['eReportCardKG']['Setting']['InputWarning'].$Lang['eReportCardKG']['Setting']['NameEN']?></span>
				</div>
			</td>
		</tr>
		
		<!--
		<tr>
			<td class="field_title"><?=$Lang['eReportCardKG']['Setting']['Year'] ?></td>
			<td>
				<?=$YearSelection?>
			</td>
		</tr>
		-->
		
		<tr>
			<td class="field_title"><?=$Lang['eReportCardKG']['Setting']['Remarks'] ?></td>
			<td>
				<textarea wrap="virtual" onfocus="this.rows=5; " rows="2" cols="70" id="Remarks" name="Remarks" class="tabletext requiredField"><?=$remarks?></textarea>
				<div style="display:none;" class="warnMsgDiv" id="textareaEmptyWarnDiv">
					<span class="tabletextrequire">*<?=$Lang['eReportCardKG']['Setting']['InputWarning'].$Lang['eReportCardKG']['Setting']['Remarks']?></span>
				</div>
			</td>
		</tr>
		</tbody>
	</table>
	
	<input type='hidden' id='CatID' name='CatID' value=<?=$catID?> >
	<input type='hidden' id='goDelete' name='goDelete' value='0' >
</form>

<div class="edit_bottom_v30">
	<p class="spacer"></p>
	<input type="button" value="<?=$Lang['Btn']['Submit']?>" class="formbutton_v30 print_hide " onclick="goSubmit()" id="submitBtn" name="submitBtn">
	<input type="button" value="<?=$Lang['Btn']['Back']?>" class="formbutton_v30 print_hide " onclick="goBack()" id="backBtn" name="backBtn">
	<p class="spacer"></p>
</div>

<script>
$(document).ready(function(){
	//$('#YearID').change();
});

function checkForm(){
	$('#EN_Name_Warn').hide();
	$('#B5_Name_Warn').hide();
	$('#Code_Warn').hide();
	
	var check = true;
//	if(!$('#YearID').val()){
//		$('#YearID').focus();
//		check = false;
//	}
	if(!$('#EN_Name').val()){
		$('#EN_Name_Warn').show();
		$('#EN_Name').focus();
		check = false;
	}
	if(!$('#CH_Name').val()){
		$('#CH_Name_Warn').show();
		$('#CH_Name').focus();
		check = false;
	}
	if(!$('#Code').val()){
		$('#Code_Warn').show();
		$('#Code').focus();
		check = false;
	}
	return check;
}

function goSubmit(){
	if(checkForm()){
		form1.submit();
	}
}

function goBack(){
	 window.location.href = "index.php?task=settings.teachingtool_category.list";
}
</script>