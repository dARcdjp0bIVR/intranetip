<?php
/*
 * Change Log:
 * Date: 2019-09-24 Philips Added onSelectChapterInThickBox()
 * Date: 2018-10-15 Bill    Disable submit button > prevent any unwanted data issues    [2018-1015-1724-00096]
 * Date: 2018-05-02 Bill    Display Warning Box after updated Zone Settings
 * Date: 2018-03-22 Bill    Support Class Zone Quota Settings   [2018-0202-1046-39164]
 * Date: 2017-12-20 Bill	Set Default End Time "23:59:59" when add TimeTable
 * Date: 2017-11-17 Bill  	support Quota settings in Zone
 * Date: 2017-10-17 Bill  	added js function onSelectCatInThickBox()
 * Date: 2017-02-09 Villa	modified GoBack()
 */
?>

<?=$NivagationBar?>
<br>

<div style="display:none;" id="NeedConfirmMsg">
	<?=$WarningBox?>
</div>
<br>
<form id='form1' method="POST" action='index.php?task=settings.rubrics.update'>
<table class="form_table_v30">
	<tbody>
		<tr>
			<td class="field_title">
				<?=$Lang['PowerPortfolio']['Settings']['Rubrics']['Name']?>
                <span class="tabletextrequire">*</span>
			</td>
			<td>
				<input type="text" maxlength="25" class="textboxnum requiredField" name="Name" id="Name" value="<?=$name?>">
                <br/>
				<div style="display:none;" class="warnMsgDiv" id="Name_Warn">
					<span class="tabletextrequire">*<?=$Lang["PowerPortfolio"]["Setting"]["InputWarning"].$Lang['PowerPortfolio']['Settings']['Rubrics']['Name']?></span>
				</div>
			</td>
		</tr>
        <tr>
            <td class="field_title">
                <?=$Lang['PowerPortfolio']['Settings']['Rubrics']['TargetForm']?>
                <span class="tabletextrequire">*</span>
            </td>
            <td>
                <?=$YearSelection?>
                <br/>
                <div style="display:none;" class="warnMsgDiv" id="Year_Warn">
                    <span class="tabletextrequire">*<?=$Lang["PowerPortfolio"]["Setting"]["InputWarning"].$Lang['PowerPortfolio']['Settings']['Rubrics']['TargetForm']?></span>
                </div>
            </td>
        </tr>
        <tr>
            <td class="field_title">
                <?=$Lang['PowerPortfolio']['Settings']['Rubrics']['Levels']?>
                <span class="tabletextrequire">*</span>
            </td>
            <td>
                <?=$LevelSelection?>
                <br/>
                <div style="display:none;" class="warnMsgDiv" id="Level_Warn">
                    <span class="tabletextrequire">*<?=$Lang["PowerPortfolio"]["Setting"]["InputWarning"].$Lang['PowerPortfolio']['Settings']['Rubrics']['Levels']?></span>
                </div>
            </td>
        </tr>
        <tr>
            <td class="field_title">
                <?=$Lang['PowerPortfolio']['Settings']['Rubrics']['GradingNum']?>
                <span class="tabletextrequire">*</span>
            </td>
            <td>
                <?=$GradeNumSelection?>
                <br/>
                <div style="display:none;" class="warnMsgDiv" id="GradeNum_Warn">
                    <span class="tabletextrequire">*<?=$Lang["PowerPortfolio"]["Setting"]["InputWarning"].$Lang["PowerPortfolio"]["Settings"]['Rubrics']['GradingNum']?></span>
                </div>
            </td>
        </tr>
		<tr> 
			<td class="field_title">
                <?=$Lang['PowerPortfolio']['Settings']['Rubrics']["GradingDetails"]?>
				<span class="tabletextrequire">*</span>
			</td>
			<td id="GradeDetailsDiv">
			</td>
		</tr>
        <tr>
            <td class="field_title">
                <?=$Lang['PowerPortfolio']['Settings']['Rubrics']['TermResultGradingNum']?>
                <span class="tabletextrequire">*</span>
            </td>
            <td>
                <?=$ExamGradeNumSelection?>
                <br/>
                <div style="display:none;" class="warnMsgDiv" id="ExamGradeNum_Warn">
                    <span class="tabletextrequire">*<?=$Lang["PowerPortfolio"]["Setting"]["InputWarning"].$Lang["PowerPortfolio"]["Settings"]['Rubrics']['TermResultGradingNum']?></span>
                </div>
            </td>
        </tr>
        <tr>
            <td class="field_title">
                <?=$Lang['PowerPortfolio']['Settings']['Rubrics']["TermResultGrades"]?>
                <span class="tabletextrequire">*</span>
            </td>
            <td id="ExamGradeDetailsDiv">
                <!--
                <input type="text" maxlength="25" class="textboxnum requiredField" name="CH_Name" id="CH_Name" value="<?=$CH_Name?>">
                <div style="display:none;" class="warnMsgDiv" id="CH_Name_Warn">
                    <span class="tabletextrequire">*<?=$Lang["PowerPortfolio"]["Setting"]["InputWarning"].$Lang["PowerPortfolio"]["Setting"]["NameB5"]?></span>
                </div>
                <div style="display:none;" class="warnMsgDiv" id="CH_Name_Warn2">
                    <span class="tabletextrequire">*<?=$Lang["PowerPortfolio"]["Setting"]["NameB5"].$Lang["PowerPortfolio"]["Setting"]["DuplicateWarning"]?></span>
                </div>
                -->
            </td>
        </tr>
	</tbody>
</table>



<?php if(!$isEdit) { ?>
    <div>
        <?=$WarningBox_Static?>
    </div>
<?php } ?>

    <p><?=$indexVar["libpowerportfolio_ui"]->GET_NAVIGATION2_IP25($Lang['PowerPortfolio']['Settings']['Rubrics']['SubTitle'])?></p>
    <!-- Create Rubics -->
    <?php if(false):?>
    <table class="table table-multi-levels">
        <thead>
        <tr class="master_row">
            <th>&nbsp;</th>
            <th class="c-icon">&nbsp;</th>
            <th class="c-icon"><a href="#" class="btn-table-expand master"></a></th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>
                <span class="indent"></span>
                <div class="text">1 分類一</div>
            </td>
            <td class="c-icon"><a href="#" class="btn-edit"><i class="fas fa-pen"></i></a></td>
            <td class="c-icon"><a href="#" class="btn-table-expand maj_row1"></a></td> <!--add .collapsed if collapsed-->
        </tr>
        <tr class="level-1 collapsible show maj_row1"> <!--add .show if expanded-->
            <td>
                <span class="indent"></span>
                <div class="text">1.1 子分類一</div>
            </td>
            <td class="c-icon"><a href="#" class="btn-edit"><i class="fas fa-pen"></i></a></td>
            <td class="c-icon"><a href="#" class="btn-table-expand maj_row1 sub_row1"></a></td>
        </tr>
        <tr class="level-2 collapsible show maj_row1 sub_row1">
            <td>
                <span class="indent"></span>
                <div class="text">1.1.1 項目一</div>
            </td>
            <td class="c-icon"><a href="#" class="btn-edit"><i class="fas fa-pen"></i></a></td>
            <td class="c-icon">&nbsp;</td>
        </tr>
        <tr class="level-2 collapsible show maj_row1 sub_row1">
            <td>
                <span class="indent"></span>
                <div class="text">1.1.2 項目二</div>
            </td>
            <td class="c-icon"><a href="#" class="btn-edit"><i class="fas fa-pen"></i></a></td>
            <td class="c-icon">&nbsp;</td>
        </tr>
        <tr class="level-2 collapsible show maj_row1 sub_row1">
            <td><span class="indent"></span> <a href="#" class="btn-add-item"><i class="fas fa-plus"></i> 新增項目</a></td>
            <td class="c-icon">&nbsp;</td>
            <td class="c-icon">&nbsp;</td>
        </tr>
        <tr class="level-1 collapsible show maj_row1">
            <td>
                <span class="indent"></span>
                <div class="text">1.2 子分類二</div>
            </td>
            <td class="c-icon"><a href="#" class="btn-edit"><i class="fas fa-pen"></i></a></td>
            <td class="c-icon"><a href="#" class="btn-table-expand maj_row1 sub_row2"></a></td>
        </tr>
        <tr class="level-2 collapsible show maj_row1 sub_row2">
            <td>
                <span class="indent"></span>
                <div class="text">1.2.1 項目一</div>
            </td>
            <td class="c-icon"><a href="#" class="btn-edit"><i class="fas fa-pen"></i></a></td>
            <td>&nbsp;</td>
        </tr>
        <tr class="level-2 collapsible show maj_row1 sub_row2">
            <td>
                <span class="indent"></span>
                <div class="text">1.2.2 項目二</div>
            </td>
            <td class="c-icon"><a href="#" class="btn-edit"><i class="fas fa-pen"></i></a></td>
            <td class="c-icon">&nbsp;</td>
        </tr>
        <tr class="level-2 collapsible show maj_row1 sub_row2">
            <td><span class="indent"></span> <a href="#" class="btn-add-item"><i class="fas fa-plus"></i> 新增項目</a></td>
            <td class="c-icon">&nbsp;</td>
            <td class="c-icon">&nbsp;</td>
        </tr>
        <tr class="level-1 collapsible show maj_row1">
            <td><span class="indent"></span> <a href="#" class="btn-add-item"><i class="fas fa-plus"></i> 新增子分類</a></td>
            <td class="c-icon">&nbsp;</td>
            <td class="c-icon">&nbsp;</td>
        </tr>
        </tbody>
        <tbody>
        <tr>
            <td>
                <span class="indent"></span>
                <div class="text">2 分類二</div>
            </td>
            <td class="c-icon"><a href="javascript:alert('est')" class="btn-edit"><i class="fas fa-pen"></i></a></td>
            <td class="c-icon"><a href="#" class="btn-table-expand"></a></td>
        </tr>
        <tr class="level-1 collapsible show">
            <td>
                <span class="indent"></span>
                <div class="text">2.1 子分類一</div>
            </td>
            <td class="c-icon"><a href="#" class="btn-edit"><i class="fas fa-pen"></i></a></td>
            <td class="c-icon"><a href="#" class="btn-table-expand"></a></td>
        </tr>
        <tr class="level-2 collapsible show">
            <td>
                <span class="indent"></span>
                <div class="text">2.1.1 項目一</div>
            </td>
            <td class="c-icon"><a href="#" class="btn-edit"><i class="fas fa-pen"></i></a></td>
            <td class="c-icon">&nbsp;</td>
        </tr>
        <tr class="level-2 collapsible show">
            <td>
                <span class="indent"></span>
                <div class="text">2.1.2 項目二</div>
            </td>
            <td class="c-icon"><a href="#" class="btn-edit"><i class="fas fa-pen"></i></a></td>
            <td class="c-icon">&nbsp;</td>
        </tr>
        <tr class="level-2 collapsible show">
            <td><span class="indent"></span> <a href="#" class="btn-add-item"><i class="fas fa-plus"></i> 新增項目</a></td>
            <td class="c-icon">&nbsp;</td>
            <td class="c-icon">&nbsp;</td>
        </tr>
        <tr class="level-1 collapsible show">
            <td>
                <span class="indent"></span>
                <div class="text">2.2 子分類二</div>
            </td>
            <td class="c-icon"><a href="#" class="btn-edit"><i class="fas fa-pen"></i></a></td>
            <td class="c-icon"><a href="#" class="btn-table-expand"></a></td>
        </tr>
        <tr class="level-2 collapsible show">
            <td>
                <span class="indent"></span>
                <div class="text">2.2.1 項目一</div>
            </td>
            <td class="c-icon"><a href="#" class="btn-edit"><i class="fas fa-pen"></i></a></td>
            <td class="c-icon">&nbsp;</td>
        </tr>
        <tr class="level-2 collapsible show">
            <td>
                <span class="indent"></span>
                <div class="text">2.2.2 項目二</div>
            </td>
            <td class="c-icon"><a href="#" class="btn-edit"><i class="fas fa-pen"></i></a></td>
            <td class="c-icon">&nbsp;</td>
        </tr>
        <tr class="level-2 collapsible show">
            <td><span class="indent"></span> <a href="#" class="btn-add-item"><i class="fas fa-plus"></i> 新增項目</a></td>
            <td class="c-icon">&nbsp;</td>
            <td class="c-icon">&nbsp;</td>
        </tr>
        <tr class="level-1 collapsible show">
            <td><span class="indent"></span> <a href="#" class="btn-add-item"><i class="fas fa-plus"></i> 新增子分類</a></td>
            <td class="c-icon">&nbsp;</td>
            <td class="c-icon">&nbsp;</td>
        </tr>
        </tbody>
        <tbody>
        <tr>
            <td>
                <span class="indent"></span>
                <a href="#" class="btn-add-item"><i class="fas fa-plus"></i> 新增分類</a>
            </td>
            <td class="c-icon">&nbsp;</td>
            <td class="c-icon">&nbsp;</td>
        </tr>
        </tbody>
    </table>
    <?php endif;?>
    <div id="RubricIndexDiv"></div>
    <input type='hidden' name='RubricIndexValueJSON' id='RubricIndexValueJSON' value='' />
    <input type='hidden' name='SettingID' id='SettingID' value='<?=$SettingID?>' />
    <!-- /Create Rubics -->
</form>
<br/>
<p class='tabletextremark'><?=$Lang['General']['RequiredField']?></p>
<div class="edit_bottom_v30">
	<p class="spacer"></p>
	<input type="button" value="<?=$Lang["Btn"]["Submit"]?>" class="formbutton_v30 print_hide " onclick="goSubmit()" id="submitBtn" name="submitBtn">
	<input type="button" value="<?=$Lang["Btn"]["Back"]?>" class="formbutton_v30 print_hide " onclick="goBack()" id="backBtn" name="backBtn">
	<p class="spacer"></p>
</div>
<?=$thickBox?>

<!--<script src="<?=$indexVar["thisBasePath"]?>templates/json2.js"></script>-->
<link href="<?=$indexVar["thisBasePath"]?>templates/fontawesome/css/all.css" rel="stylesheet">
<link href="<?=$indexVar["thisBasePath"]?>templates/2009a/css/PowerPortfolio/style.css" rel="stylesheet">
<script src="<?=$indexVar["thisBasePath"]?>templates/2009a/js/PowerPortfolio/demo.js"></script>
<link rel="stylesheet" href="<?=$PATH_WRT_ROOT?>/templates/jquery/jquery.colorPicker.css">
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>/templates/jquery/jquery.colorPicker.js"></script>
<style>
a:hover{cursor: pointer}
</style>
<script>
var toolData = []; 			// saving tool data
var zoneQuotaArr = [];		// saving zone quota
var myJSON = '';

$(document).ready(function(){

	$.fn.colorPicker.defaultColors = ['fe555a','feab5c','fee55c','b9da00','61c11b','14bfbe','58a7fd','9676da','eb85c0','d29417','aab2bd','53647e'];
	
	if('<?=$isEdit?>') { 	// input record to the form
		$('#YearID').change();

		disableSubmitBtn();
		setTimeout(intialize_Data, 1500);	// delay for letting finish the ajax first
	}
	else {					// set default end date time
		var DateEnd_Hour = '<?=$EndDate_Hour?>';
		$('#DateEnd_hour').val(DateEnd_Hour);
		var DateEnd_Minutes = '<?=$EndDate_Minutes?>';
		$('#DateEnd_min').val(DateEnd_Minutes);
		var DateEnd_Second = '<?=$EndDate_Second?>';
		$('#DateEnd_sec').val(DateEnd_Second);
// 		loadRubricIndexTable(function(){});
	}
});

function intialize_Data()
{
	GradeNumOnChange();
	ExamGradeNumOnChange();
	loadRubricIndexTable(function(){
		resetRubricIndex();
	});
	reEnableSubmitBtn();
}

function formDataChecking()
{
	$.ajax({
		type: "POST",
		url : "index.php?task=mgmt.topic_timetable.ajax_timetable_checking",
		data:{	
			"DataList": $('form#form1').serialize()
		},
		success: function(Type){
			// display message
			if(Type == "") {
				form1.submit();
			} else {
				$('#'+Type+'_Warn2').show();
				
				reEnableSubmitBtn();
				return false;
			}
		}
	});
}

function YearOnChange()
{
	var YearID = $('#YearID').val();
	$.ajax({
		type: "POST",
		url : "index.php?task=mgmt.topic_timetable.ajax_get_topic",
		data:{
			"YearID": YearID
		},
		success: function(msg){
			$('#TopicCell').html(msg);
			TopicOnChange();
		}
	});
}

function TopicOnChange()
{
	var YearID = $('#YearID').val();
	var TopicID = '<?=$TopicID?>';
	
	$.ajax({
		type: "POST",
		url : "index.php?task=mgmt.topic_timetable.ajax_get_zone",
		data:{
			"YearID": YearID,
			"TopicID": TopicID
		},
		success: function(msg){
			$('#ZoneCell').html(msg);
			$('#toolData').val('');
			$('#zoneQuotaData').val('');
			toolData = [];
			zoneQuotaArr = [];
		}
	});
}

function LevelOnChange()
{
    loadRubricIndexTable(function(){
		$('a.btn-add-item').each(function(index){
				$(this).parent().parent().remove();
		});
        resetRubricIndex();
    });
}

function GradeNumOnChange()
{
	let gradeNum = $('#GradeNum').val();
	if(gradeNum != '') $('#GradeDetailsDiv').html("<?=$LoadingImage?>");
	$.ajax({
		url: 'index.php?task=settings.rubrics.ajax',
		data: {'Action': 'DisplayGradeDetails', 'SettingID': '<?=$SettingID?>', 'GradeNum': gradeNum},
		method: 'POST',
		success: function(html){
// 			console.log(html);
			$('#GradeDetailsDiv').html(html);
			$('#GradeDetailsDiv input.colorPickerBox').colorPicker();
		}
	});
	/*
    $('.GradeRow').hide();

    var GradeNum = $('#GradeNum').val();
    if(GradeNum != '' && GradeNum > 0) {
        $('#GradeRow_' + GradeNum + ' .grade_range').show();
        for (var i = GradeNum; i > 0; i--) {
            $('#GradeRow_' + i).show();
        }
    }
    */
}

function ExamGradeNumOnChange()
{
	let examGradeNum = $('#ExamGradeNum').val();
	if(examGradeNum != '') $('#ExamGradeDetailsDiv').html("<?=$LoadingImage?>");
	$.ajax({
		url: 'index.php?task=settings.rubrics.ajax',
		data: {'Action': 'DisplayExamGradeDetails', 'SettingID': '<?=$SettingID?>', 'ExamGradeNum': examGradeNum},
		method: 'POST',
		success: function(html){
// 			console.log(html);
			$('#ExamGradeDetailsDiv').html(html);
			$('#ExamGradeDetailsDiv input.colorPickerBox').colorPicker();
		}
	});
//     $('.ExamGradeRow').hide();

//     var GradeNum = $('#ExamGradeNum').val();
//     if(GradeNum != '' && GradeNum > 0) {
//         $('#ExamGradeRow_' + GradeNum + ' .grade_range').show();
//         for (var i = GradeNum; i > 0; i--) {
//             $('#ExamGradeRow_' + i).show();
//         }
//     }
}

function ZoneOnClick(ZoneID)
{
	load_dyn_size_thickbox_ip('<?=$Lang["PowerPortfolio"]["Setting"]["Rubric"]["SubTitle"]?>', 'onloadThickBox('+ZoneID+');');
}
function onloadThickBox(code)
{
// 	var YearID = $('#YearID').val();
// 	var toolDataID = $('#toolData').val();
// 	var zoneQuotaData = $('#zoneQuotaData').val();
	
// 	$.ajax({
// 		type: "POST",
// 		url: "index.php?task=mgmt.topic_timetable.ajax_get_learning_tool", 
// 		data:{ 
// 			"ZoneID": ZoneID,
// 			"YearID": YearID,
// 			"toolData" : toolDataID,
// 			"zoneQuotaData" : zoneQuotaData
// 		},
// 		success: function(ReturnData){
// 			$('div#TB_ajaxContent').html(ReturnData);
// 			adjust_dyn_size_thickbox_content_height_ip('thickboxContainerDiv', 'thickboxContentDiv', 'editBottomDiv');
// 			onSelectChapterInThickBox();
// 		}
// 	});

	let codeAry = code.toString().split('.');
	let value = '';
	let target = RubricIndexValue;
	for(let i=0;i<codeAry.length;i++){
		if(i==0)
			target = target[codeAry[i]];
		else
			target = target.child[codeAry[i]];
	}
	value = target.value;
	$.ajax({
		url: 'index.php?task=settings.rubrics.ajax',
		method: 'POST',
		data: {'Action': 'EditRubricIndex', 'Value': value, 'Code': code},
		success: function(html){
			$('div#TB_ajaxContent').html(html);
			adjust_dyn_size_thickbox_content_height_ip('thickboxContainerDiv', 'thickboxContentDiv', 'editBottomDiv');
		}
	});
}
// function onloadThickBox() {
	
// 	$('div#TB_ajaxContent').load(
// 		"index.php?task=settings.rubrics.ajax", 
// 		{ 
// 			'Action': 'EditRubricIndex',
// 			'Value': 'Test',
// 			'Code': '1'
// 		},
// 		function(ReturnData) {
// 			adjust_dyn_size_thickbox_content_height_ip('thickboxContainerDiv', 'thickboxContentDiv', 'editBottomDiv');
// 		}
// 	);
// }

function loadRubricIndexTable(callback){
    var Levels = $('#Levels').val();
	$('#RubricIndexDiv').html("<?=$LoadingImage?>");
	$.ajax({
		url: 'index.php?task=settings.rubrics.ajax',
		method: 'POST',
		data: {'Action': 'GetRubricIndexTable', 'SettingID' : '<?=$SettingID?>', 'MaxLevel': Levels},
		success: function(html){
			$('#RubricIndexDiv').html(html);
			initBtnTableExpand();
			callback();
		}
	});
}
function editRubricIndex(ele){
	let code = $(ele).parent().parent().find('div.text').attr('alt-code');
	load_dyn_size_thickbox_ip('<?=$Lang['PowerPortfolio']['Settings']['Rubrics']['Title']?>', 'onloadThickBox("'+code+'");');
}

function newRubricIndex(ele){

    var Levels = $('#Levels').val();
	let loc = $(ele).attr('alt-level');
	loc = loc.split('_');
	level = loc[0];
	maj = loc[1];
	sub = loc[2];
	let code = getNextRubricIndexCode(level, maj, sub);
	$.ajax({
		url: 'index.php?task=settings.rubrics.ajax',
		method: 'POST',
		data: {'Action': 'AddRubricIndexRow', 'Level': level, 'Major': maj, 'Minor': sub, 'Code': code, 'MaxLevel': Levels},
		success: function(html){
			$(html).insertBefore($(ele).parent().parent());
			let codeAry = code.toString().split('.');
			let target = RubricIndexValue;
			for(i=0;i<codeAry.length-1;i++){
				if(i==0)
					target = target[codeAry[i]];
				else
					target = target.child[codeAry[i]];
			}
			if(i==0){
				//target[codeAry[i]] = {'value': '<?=$newItemLang?>', 'child': [null], 'code': code};
                target[codeAry[i]] = {'value': '', 'child': [null], 'code': code};
			}else{
				//target.child[codeAry[i]] = {'value': '<?=$newItemLang?>', 'child': [null], 'code': code};
                target.child[codeAry[i]] = {'value': '', 'child': [null], 'code': code};
            }
			$(ele).parent().parent().remove();
		}
	});
}

function onchangeRubricIndex(code){
	let val = $('#thickBox_Text').val();
	let codeAry = code.toString().split('.');
	let value = '';
	let target = RubricIndexValue;
	for(let i=0;i<codeAry.length;i++){
		if(i==0)
			target = target[codeAry[i]];
		else
			target = target.child[codeAry[i]];
	}
	target.value = val;
	if(val==''){
		val = '<?=$Lang['PowerPortfolio']['Settings']['Rubrics']['PleaseInputName']?>';
	}
	$('div[alt-code="'+code+'"]').text(code + ' ' + val);
	js_Hide_ThickBox();
}

function deleteRubricIndex(ele){
	let code = $(ele).parent().parent().find('div.text').attr('alt-code');
	let codeAry = code.toString().split('.');
	let target = RubricIndexValue;
	let targetLevel = codeAry.length-1;
	for(i=0;i<targetLevel;i++){
		if(i==0)
			target = target[codeAry[i]];
		else
			target = target.child[codeAry[i]];
	}
	if(target instanceof Array)
		target.splice(codeAry[targetLevel], 1);
	else
		target.child.splice(codeAry[targetLevel], 1);
	$('div.text').each(function(index){
		let alt = $(this).attr('alt-code');
		if(alt.search(code)==0) 
			$(this).parent().parent().remove();
	});
	
	$('a.btn-add-item').each(function(index){
// 		let alt = $(this).attr('alt-level');
// 		let loc = alt.split('_');
// 		let _code = code.split('.');
// 		if(loc[0] > targetLevel && _code[0] == loc[1] && (_code[1] == loc[2] || _code[1] === undefined )){
			$(this).parent().parent().remove();
// 		}
	});
	resetRubricIndex();
}

function resetRubricIndex(){
    var Levels = $('#Levels').val();
    var emptyString = '<?=$Lang['PowerPortfolio']['Settings']['Rubrics']['PleaseInputName']?>';
	for(let x in RubricIndexValue){ // level 1
		if(x=='in_array') continue;

		newcode = x.toString();
		if(x!=0){
			target = RubricIndexValue[x];
			oldcode = target.code;

		if(oldcode != newcode){
			$('div[alt-code="'+oldcode+'"]')
			.text(newcode+' '+(target.value!=''?target.value:emptyString))
			.attr('alt-code', newcode);
			target.code = newcode;
		}
		
		if(RubricIndexValue[x].child.length!=1){
		for(let y in RubricIndexValue[x].child){ // level 2
			if(y=='in_array') continue;
			newcode = x.toString()+'.'+y.toString();
			if(y!=0){
				target = RubricIndexValue[x].child[y];
				oldcode = target.code;
			if(oldcode != newcode){
				oldclass = $('div[alt-code="'+oldcode+'"]').parent().parent().attr('class');
				newclassStr = ' maj_row' + x;
				newclass = oldclass.substring(0, oldclass.search('maj')) + newclassStr;
				
				$('div[alt-code="'+oldcode+'"]')
				.text(newcode+' '+ (target.value!=''?target.value:emptyString) )
				.attr('alt-code', newcode)
				.parent().parent().attr('class', newclass);
				target.code = newcode;
			}
			
			if(RubricIndexValue[x].child[y].child.length!=1 && Levels == 3){
			for(let z in RubricIndexValue[x].child[y].child){ // level 3
				if(z=='in_array') continue;
				newcode = x.toString()+'.'+y.toString()+'.'+z.toString();
				if(z!=0){
				target = RubricIndexValue[x].child[y].child[z];
				oldcode = target.code;
				if(oldcode != newcode){
					oldclass = $('div[alt-code="'+oldcode+'"]').parent().parent().attr('class');
					newclassStr = ' maj_row' + x + ' sub_row'+ y;
					newclass = oldclass.substring(0, oldclass.search('maj')) + newclassStr;
					
					$('div[alt-code="'+oldcode+'"]')
					.text(newcode+' '+(target.value!=''?target.value:emptyString))
					.attr('alt-code', newcode)
					.parent().parent().attr('class', newclass);
					target.code = newcode;
				}
				}

				if(z==RubricIndexValue[x].child[y].child.length-1) addInsertRow(newcode, 2, x, y);
			}
			} else if(RubricIndexValue[x].child[y].child.length==1 && Levels == 3){
				addInsertRow(newcode, 2, x, y);
			}
			}
			
			if(y==RubricIndexValue[x].child.length-1) addInsertRow(newcode, 1, x, parseInt(y)+1);
				
		}
		} else if(RubricIndexValue[x].child.length==1){
			addInsertRow(x, 1, x, 1);
		}
		}
		if(x==RubricIndexValue.length-1) setTimeout( function(){
			addInsertRow(newcode, 0, parseInt(x)+1, 1);
		}, 500); // Prevent too early append
	}
}

function addInsertRow(code, level, maj, sub){
	$.ajax({
		url: 'index.php?task=settings.rubrics.ajax',
		method: 'POST',
		data: {'Action': 'GetRubricIndexAddRow', 'Level': level, 'Major': maj, 'Minor': sub},
		success: function(html){
			if(level==2)
				$(html).insertAfter($('div[alt-code="'+code+'"]').parent().parent());
			else if(level==1){
				if($('div[alt-code="'+(parseInt(code)+1)+'"]').length != 0){
					$(html).insertBefore($('div[alt-code="'+(parseInt(code)+1)+'"]').parent().parent());
				} else {
					$(html).appendTo($('div#RubricIndexDiv table tbody'));
				}
			}else{
				$(html).appendTo($('div#RubricIndexDiv table tbody'));
			}
		}
	});
}

function getNextRubricIndexCode(level, maj, sub){
	let target = RubricIndexValue;
	let result = [];
	if(level==0)
		maj++;
	else if(level==1)
		sub++;
		
	for(let i=0; i<=level; i++){
		if(i==0)
			target = target;
		else{
			point = (i==1) ? maj : sub;
			target = target[point].child;
			result.push(point);
		}
	}
	let last = target.length == 0 ? 1 : target.length;
	result.push(last);
	return result.join('.');
}

function CloseThickBox(){
	$('#TB_window').fadeOut();
	tb_remove();
}

function goSubmit(){
	if(CheckForm()){
		genRubricIndexJSON();
		$('#form1').submit();
	}
}

function CheckForm(){
	let check = true;
	/*
	$('#Name_Warn').hide();
	$('#Year_Warn').hide();
	$('#Level_Warn').hide();
	$('#GradeNum_Warn').hide();
	$('#ExamGradeNum_Warn').hide();
	*/
	$('.warnMsgDiv').hide();

    if($('#ExamGradeNum').val()==''){
        $('#ExamGradeNum').focus();
        $('#ExamGradeNum_Warn').show();
        check = false;
    } else {
        $("input[name^='ExamGradeNumContent']").each(function(){
            if($(this).val()==''){
                $(this).focus();
                $('#' + this.id + '_Warn').show();
                check = false;
            }
        })
    }
    if($('#GradeNum').val()==''){
        $('#GradeNum').focus();
        $('#GradeNum_Warn').show();
        check = false;
    } else {
        $("input[name^='GradeNumContent']").each(function(){
            if($(this).val()==''){
                $(this).focus();
                $('#' + this.id + '_Warn').show();
                check = false;
            }
        })
    }
    if($('#Levels').val()==''){
        $('#Levels').focus();
        $('#Level_Warn').show();
        check = false;
    }
    if($('#YearID').val()==''){
        $('#YearID').focus();
        $('#Year_Warn').show();
        check = false;
    }
	if($('#Name').val()==''){
        $('#Name').focus();
		$('#Name_Warn').show();
		check = false;
	}

	return check;
}

function genRubricIndexJSON(){
	let val = JSON.stringify(RubricIndexValue);
	$('#RubricIndexValueJSON').val(val);
}

function goBack(){
	 window.location.href = "index.php?task=settings.rubrics.list";
}

function disableSubmitBtn() {
	$('#submitBtn').attr('disabled','disabled');
	$('#submitBtn').removeClass('formbutton_v30').addClass('formbutton_disable_v30');
}

function reEnableSubmitBtn() {
	$('#submitBtn').removeAttr('disabled');
	$('#submitBtn').removeClass('formbutton_disable_v30').addClass('formbutton_v30');
}
</script>