<script type="text/javascript">
$(document).ready( function() {
	
});

function goNew() {
	window.location = '?task=settings<?=$PowerPortfolioConfig['taskSeparator']?>activity<?=$PowerPortfolioConfig['taskSeparator']?>edit';
}

function goEdit(ActivitySettingID) {
	window.location = '?task=settings<?=$PowerPortfolioConfig['taskSeparator']?>activity<?=$PowerPortfolioConfig['taskSeparator']?>edit&isEdit=1&ActivitySettingID='+ActivitySettingID;
}

function checkEdit(){
	var EditID = [];
	$('.checkbox:checked').each(function(){
		EditID.push($(this).val());
	});
	if(EditID.length==1){
		goEdit(EditID[0]);
	}
	else if(EditID.length > 1){
		alert("<?=$Lang['PowerPortfolio']['Setting']['EditWarning']['NoMoreThan1'] ?>");
	}
	else{
		alert("<?=$Lang['PowerPortfolio']['Setting']['EditWarning']['PleaseSelectEdit'] ?>");
	}
}
function checkCopy(){
	
}
function checkExport(){
	
}
function checkDelete(){
	
}
</script>

<form name="form1" id="form1" method="POST">
	<div class="table_board">
		<div class="content_top_tool">
			<?=$htmlAry['contentTool']?>
			<!--<?=$htmlAry['searchBox']?>-->
			<br style="clear:both;">
		</div>
		<br style="clear:both;">
		<p class="spacer"></p>
		
		<?=$htmlAry['dbTableActionBtn']?>
		<?=$htmlAry['dataTable']?>
	</div>
	
	<?=$htmlAry['hiddenField']?>
</form>