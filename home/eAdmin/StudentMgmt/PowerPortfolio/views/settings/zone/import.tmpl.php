<br />

<form name="form1" method="POST" onsubmit="return checkForm();"  action="index.php?task=settings.zone.import_check" enctype="multipart/form-data">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td><?= $Navigation?></td>
		</tr>
	</table>
	
	<table id="html_body_frame" width="90%" border="0" cellspacing="2" cellpadding="4">
		
		<tr><td><?= $pageStep?></td></tr>
		<tr>
	    	<td colspan="2">
	    		<table width="89%" align="center">
		    		<tr>
			    		<td>
				    		<br>
					    		<table class="form_table_v30">
					    			<tr>
										<td class="field_title">
											<?=$Lang['PowerPortfolio']['Setting']['ActionType']?>
										</td>
										<td>
											<?=$Lang['Btn']['New']?>
										</td>
									</tr>
									<tr>
										<td class="field_title">
											<?= $indexVar['libpowerportfolio_ui']->RequiredSymbol().$i_select_file ?>
										</td>
										<td class="tabletext">
											<input class="file" type="file" name="ImportFile"><br>
											<div style="display:none;" class="warnMsgDiv" id="File_Warn">
												<span class="tabletextrequire">* <?=$Lang["PowerPortfolio"]["Setting"]["InputWarning"].$Lang['General']['File']?></span>
											</div>
										</td>
									</tr>
									<tr>
										<td class="field_title"><?=$Lang['General']['ClickHereToDownloadSample']?></td>
										<td><?=$sample_csv?></td>
									</tr>
									<tr>
										<td class="field_title"><?= $i_general_Format ?></td>
										<td class="tabletext"><?=$format_str?><div id="remarkDiv_type" class="selectbox_layer" style="width:400px"></div></td>
									</tr>
								</table>
							<?=$i_general_required_field2?>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		
		<br />
		
		<tr>
    		<td colspan="2">
		    	<table width="95%" border="0" cellpadding="0" cellspacing="0" align="center">
		    		<tr>
                		<td colspan="3" class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                	</tr>
		    	</table>
	    	</td>
		</tr>
		
		<br />
		
		<tr>
			<td colspan="2" align="center">
			<br />
				<?= $btnSubmit?>&nbsp;
				<?= $btnCancel?>&nbsp;
				
			</td>
		</tr>

			
		
	</table>
	<input type="hidden" name="EnrolGroupID" id="EnrolGroupID" value="<?=$EnrolGroupID?>"/>
	<input type="hidden" name="EnrolEventID" id="EnrolEventID" value="<?=$EnrolEventID?>"/>
	<input type="hidden" name="type" id="type" value="<?=$type?>"/>
	<input type="hidden" name="Semester" id="Semester" value="<?=$Semester?>"/>
	
	
</form>
<?=$RemarksLayer?>

<script type="text/javascript">
function checkForm(){
	$('#File_Warn').hide();
	if($('input[name="ImportFile"]').val()==""){
		$('#File_Warn').css('display','inline-block');
		return false;
	} else {
		return true;
	}
}
function Load_Reference(ref) {
	$('#remarkDiv_type').html('');
	$.ajax({
		url: 'index.php?task=settings.zone.ajax',
		method: 'POST',
		data:{'Action':'GetImportReference', 'Target': ref},
		success: function(html){
			$('#remarkDiv_type').html(html);
			
		}
	});
	
	displayReference(ref);
}
function displayReference(ref) {
	var p = $("#importRef_"+ref);
	var position = p.position();
	
	var t = position.top + 15;
	var l = position.left + p.width()+5;
	$("#remarkDiv_type").css({ "position": "absolute", "visibility": "visible", "top": t+"px", "left": l+"px" });
}
function hideAllRemarkLayer() {
	$('div.selectbox_layer').each( function() {
		MM_showHideLayers($(this).attr('id'), '', 'hide');
	});
}
function getRemarkDivIdByRemarkType(remarkType) {
	return 'remarkDiv_' + remarkType;
}
function hideRemarkLayer(remarkType) {
	var remarkDivId = getRemarkDivIdByRemarkType(remarkType);
	MM_showHideLayers(remarkDivId, '', 'hide');
}
function Hide_Window(ref) {
	$("#importRef_"+ref).css({ "visibility": "hidden" });
}
</script>