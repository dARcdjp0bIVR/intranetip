<?php
/*
 *  Change Log:
 *  Date: 2019-09-17 Bill : added js function goDelete() - to support delete photo
 *  Date: 2017-02-09 Villa: Modified GoBack();
 *  Date: 2017-01-26 Villa: Open the File
 */
?>

<?=$NavigationBar?>
<br> 
<br>

<form id="form1" method="POST" action="index.php?task=settings.tool.update" enctype="multipart/form-data">
	<div style="display:none;" id="NeedConfirmMsg">
		<?=$WarningBox?>
	</div>
	<table class="form_table_v30">
		<tbody>
		<tr>
			<td class="field_title"><?=$Lang['PowerPortfolio']['Settings']['Tool']['Name']?> <span class="tabletextrequire">*</span></td>
			<td>
				<input type="text" maxlength="255" class="textboxtext requiredField" name=Name id="Name" value="<?=$name?>">
                <br/>
				<div style="display:none;" class="warnMsgDiv" id="Name_Warn">
					<span class="tabletextrequire">* <?=$Lang["PowerPortfolio"]["Setting"]["InputWarning"].$Lang['PowerPortfolio']['Settings']['Tool']['Name']?></span>
				</div>
			</td>
		</tr>
		
        <tr>
            <td class="field_title">
                <?=$Lang['PowerPortfolio']['Settings']['Activity']['TargetForm']?>
                <span class="tabletextrequire">*</span>
            </td>
            <td>
                <?=$YearSelection?>
                <br/>
                <div style="display:none;" class="warnMsgDiv" id="Year_Warn">
					<span class="tabletextrequire">* <?=$Lang["PowerPortfolio"]["Setting"]["InputWarning"].$Lang['PowerPortfolio']['Settings']['Activity']['TargetForm']?></span>
				</div>
            </td>
        </tr>
		
		<tr>
			<td class="field_title"><?=$Lang['PowerPortfolio']['Settings']['Tool']['Topic']?> <span class="tabletextrequire">*</span></td>
			<td>
				<div id='topicSelect' style='inline-block'><?=$TopicSelection?></div>
				<div style="display:none;" class="warnMsgDiv" id="Topic_Warn">
					<span class="tabletextrequire">* <?=$Lang["PowerPortfolio"]["Setting"]["InputWarning"].$Lang['PowerPortfolio']['Settings']['Tool']['Topic']?></span>
				</div>
			</td>
		</tr>
		 
		<tr>
			<td class="field_title"><?=$Lang['PowerPortfolio']['Settings']['Tool']['Zone']?> <span class="tabletextrequire">*</span></td>
			<td>
				<div id='ZoneSelectDiv'><?=$ZoneSelection?></div>
				<div style="display:none;" class="warnMsgDiv" id="Zone_Warn">
					<span class="tabletextrequire">* <?=$Lang["PowerPortfolio"]["Setting"]["InputWarning"].$Lang['PowerPortfolio']['Settings']['Tool']['Zone']?></span>
				</div>
			</td>
		</tr>
		
		<tr>
			<td class="field_title"><?=$Lang['PowerPortfolio']['Settings']['Tool']['Rubrics']?> <span class="tabletextrequire">*</span></td>
			<td>
				<div id='RubricIndexDiv'></div>
                <div style="display:none;" class="warnMsgDiv" id="Rubrics_Warn">
                    <span class="tabletextrequire">* <?=$Lang["PowerPortfolio"]["Setting"]["InputWarning"].$Lang['PowerPortfolio']['Settings']['Tool']['Rubrics']?></span>
                </div>
				<hr/>
				<span class='content_top_tool'>
					<?=$addRubricIndexBtn?>
				</span>
			</td>
		</tr>
		
		<tr>
			<td class="field_title"><?=$Lang['PowerPortfolio']['Settings']['Tool']['Photo']?><br><span class="tabletextremark"><?=$Lang['PowerPortfolio']['Setting']['PhotoRemarks']?></span></td>
			<td>
				<input type="file" accept="image/*" name="fileToUpload" id="fileToUpload">
				<br>
				<!-- Display Image -->
				<?php if($IsPhotoDispaly){?>
					<div id='imageDiv'>
						<?=$photoDispaly?>
						<br>
						<span class="table_row_tool row_content_tool"><a onclick="goDelete();" title="刪除" class="delete" href="javascript:void(0);"></a></span><br style="clear:both;">
					</div>
				<?php }?>
			</td>
		</tr>
		
		</tbody>
	</table>
	
	<input type='hidden' id='DeletePhoto' name='DeletePhoto' value='' />
	<input type='hidden' id='ToolID' name='ToolID' value='<?=$ToolID?>' >
	<input type='hidden' id='PhotoSRC' name='PhotoSRC' value='<?=$photoPath?>' >
</form>

<div class="edit_bottom_v30">
	<p class="spacer"></p>
	<input type="button" value="<?=$Lang['Btn']['Submit']?>" class="formbutton_v30 print_hide " onclick="goSubmit()" id="submitBtn" name="submitBtn">
	<input type="button" value="<?=$Lang['Btn']['Back']?>" class="formbutton_v30 print_hide " onclick="goBack()" id="backBtn" name="backBtn">
	<p class="spacer"></p>
</div>

<script>
var rubricCount = 0;
$(document).ready(function(){
	if('<?=$isEdit?>') { 	// input record to the form
		disableSubmitBtn();
		setTimeout(intialize_Data, 1500);	// delay for letting finish the ajax first
	}
	else {
        YearOnChange();
		$('#ToolID').attr('disabled', true);
	}
});

function intialize_Data()
{
	
	$('.LoadingImg').hide();

	let _thisRubricCode = '<?=$rubricCode?>';
	_thisRubricCode.split(',')
	.forEach( function( val, index ){
		rubricCount++;
		$('#RubricIndexDiv').append(
			$('<div id="RubricIndexValue_'+rubricCount+'"></div><br/>')
		);
		initRubricIndexBox(rubricCount,val);
	});
	
	reEnableSubmitBtn();
}

function CheckForm()
{
	let check = true;
	/*
	$('#Name_Warn').hide();
	$('#Year_Warn').hide();
	$('#Topic_Warn').hide();
	$('#Zone_Warn').hide();
    */
    $('.warnMsgDiv').hide();

    if($("select[name^='RubricCode']").length == 0){
        $('#Rubrics_Warn').show();
        check = false;
    } else {
        let rubricDivCount = $('div#RubricIndexDiv').find('div').length;
        let rubricCount = $('div#RubricIndexDiv').find('select.bottomRubric').length;
        if(rubricDivCount != rubricCount){
            $('#Rubrics_Warn').show();
            check = false;
        } else {
        	$('select.bottomRubric').each(function(){
        		if($(this).val()==''){
                 $(this).focus();
                 $('#Rubrics_Warn').show();
                 check = false;
             }
        	});
        }
//         $("select[name^='RubricCode']").each(function(){
//             if($(this).val()==''){
//                 $(this).focus();
//                 $('#Rubrics_Warn').show();
//                 check = false;
//             }
//         })
    }
    if($('#ZoneID').val()==''){
        $('#ZoneID').focus();
        $('#Zone_Warn').show();
        check = false;
    }
    if($('#TopicSettingID').val()==''){
        $('#TopicSettingID').focus();
        $('#Topic_Warn').show();
        check = false;
    }
    if($('#YearID').val()==''){
        $('#YearID').focus();
        $('#Year_Warn').show();
        check = false;
    }
	if($('#Name').val()==''){
        $('#Name').focus();
		$('#Name_Warn').show();
		check = false;
	}

	return check;
}

function YearOnChange()
{
	let yearID = $('#YearID').val();
	if(yearID =='') return;
	$.ajax({
		url: 'index.php?task=settings.tool.ajax',
		method: 'POST',
		data: {'Action': 'GetTopicSelecct', 'YearID': yearID},
		success: function(html){
			$('#topicSelect').html(html);
		}
	});
}

function TopicOnChange(){
	let topicSettingID = $('#TopicSettingID').val();
	$.ajax({
		url: 'index.php?task=settings.tool.ajax',
		method: 'POST',
		data: {'Action':'GetZoneSelect', 'TopicSettingID': topicSettingID},
		success:function(html){
			$('#ZoneSelectDiv').html(html);
		}
	});
	$('#RubricIndexDiv').html('');
}
function ZoneOnChange(){
}
function goSubmit(){
	if(CheckForm()){
		$('#form1').submit();
	}
}
function AddRubricIndex(){
    $('#Topic_Warn').hide();
    if($('#TopicSettingID').val()==''){
        $('#Topic_Warn').show();
        return;
    }

	rubricCount++;
	$('#RubricIndexDiv').append(
		$('<div id="RubricIndexValue_'+rubricCount+'"></div><br/>')
	);
	initRubricIndexBox(rubricCount,'');
}
function RubricCodeOnChange(num, val){
	if(val=='') return;
	if(val.toString().split('.').length==3) return;
	initRubricIndexBox(num,val);
}
function initRubricIndexBox(num, code){
	let topicSettingID = $('#TopicSettingID').val();
	$.ajax({
		url: 'index.php?task=settings.tool.ajax',
		method: 'POST',
		data: {'Action': 'GetRubricSelectBox', 'TopicSettingID': topicSettingID, 'Number': num, 'Code': code},
		success: function(html){
			$('#RubricIndexValue_'+num).html(html);
		}
	});
}
function goDelete(){
    $('div#imageDiv').hide();
    $('input#DeletePhoto').val(1);
}

function goBack(){
	 window.location.href = "index.php?task=settings.activity.list";
}

function disableSubmitBtn() {
	$('#submitBtn').attr('disabled','disabled');
	$('#submitBtn').removeClass('formbutton_v30').addClass('formbutton_disable_v30');
}

function reEnableSubmitBtn() {
	$('#submitBtn').removeAttr('disabled');
	$('#submitBtn').removeClass('formbutton_disable_v30').addClass('formbutton_v30');
}
</script>