<meta Http-Equiv="Cache-Control" Content="no-cache">
<script type="text/javascript">
$(document).ready(function() {
	$('.checkbox').click(function(){
		if($(this).attr('checked')){
			// do nothing
		}
		else{
			$('#checkmaster').removeAttr('checked');
		}
	});
});

function goNew() {
	window.location = '?task=settings<?=$PowerPortfolioConfig['taskSeparator']?>tool<?=$PowerPortfolioConfig['taskSeparator']?>edit';
}

function goEdit(toolID) {
	window.location = '?task=settings<?=$PowerPortfolioConfig['taskSeparator']?>tool<?=$PowerPortfolioConfig['taskSeparator']?>edit&isEdit=1&ToolID='+toolID;
}

function checkEdit(){
	var EditID = [];
	$('.checkbox:checked').each(function(){
		EditID.push($(this).val());
	});
	if(EditID.length==1){
		goEdit(EditID[0]);
	}
	else if(EditID.length > 1){
		alert("<?=$Lang['PowerPortfolio']['Setting']['EditWarning']['NoMoreThan1'] ?>");
	}
	else{
		alert("<?=$Lang['PowerPortfolio']['Setting']['EditWarning']['PleaseSelectEdit'] ?>");
	}
}
function checkCopy(){
	
}
function checkExport(){
	
}
function checkDelete(){
	
}
</script>

<form name="form1" id="form1" method="POST">
	<div class="table_board">
		<div class="content_top_tool">
			<?=$htmlAry['contentTool']?>
			<?=$htmlAry['searchBox']?>
			<br style="clear:both;">
		</div>
		<br style="clear:both;">
		<p class="spacer"></p>
		<div class="table_filter">
			<?=$Lang['General']['Form']?>: <?=$YearSelection?>
			<?=$CatSelection?>
			<?=$Lang['PowerPortfolio']['Settings']['Zone']['Title']?>: <?=$ZoneSearchSelection?>
			<?=$ChapterSelection?>
		</div>
		<p class="spacer"></p>
		<?=$htmlAry['dbTableActionBtn']?>
		<?=$htmlAry['dataTable']?>
	</div>
	
	<?=$htmlAry['hiddenField']?>
</form>