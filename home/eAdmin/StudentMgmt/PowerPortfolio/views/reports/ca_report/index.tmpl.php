<script type="text/javascript">
function js_Reload_Class_Selection(){
	var reportIdList = $("select#ReportID").val();
	if(reportIdList == "") {
		return;
	}
	
	$.ajax({
        url:"index.php?task=reports.ca_report.ajax",
		method: 'post',
		data:{
			Action: 'ClassByReport',
			ReportID: reportIdList
		},
		success: function(ReturnData)
		{
			$("div#classSelDiv").html(ReturnData);
			js_Select_All('StudentID', 1);
		}
	})
}
function js_Reload_Topic_Selection(){
	var reportIdList = $("select#ReportID").val();
	if(reportIdList == "") {
		return;
	}
	
	$.ajax({
        url:"index.php?task=reports.ca_report.ajax",
		method: 'post',
		data:{
			Action: 'TopicByReport',
			ReportID: reportIdList
		},
		success: function(ReturnData)
		{
			$("div#topicSelDiv").html(ReturnData);
		}
	})
}
function js_Reload_Student_Selection()
{
	var classIdList = $("select#ClassID").val();
	if(classIdList == "") {
		return;
	}
	
	$.ajax({
        url:"index.php?task=reports.ca_report.ajax",
		method: 'post',
		data:{
			Action: 'StudentByClass',
			ClassID: classIdList,
			SelectionID: 'StudentID',
			SelectionName: 'StudentID[]',
			noFirst: 1,
			isMultiple: 1,
			isAll: 1,
			withSelectAll: 1
		},
		success: function(ReturnData)
		{
			$("div#stuSelDiv").html(ReturnData);
			js_Select_All('StudentID', 1);
		}
	})
}

function goSubmit()
{
	var stuIdList = $('select#StudentID').val();
	$('div.warnMsgDiv').hide();
	
	var canSubmit = true;
	if(stuIdList == null || stuIdList == '') {
		canSubmit = false;
		$('div#Student_Warn').show();
	}
	
	if(canSubmit) {
		$('form#form1').submit();
	}
}

</script>

<form id="form1" method="POST" target="_blank" action="index.php?task=reports.ca_report.print">
	<table class="form_table_v30">
		<tbody>
		<tr>
			<td class="field_title"><?=$Lang['PowerPortfolio']['Reprots']['CAReport']['Report']?></td>
			<td>
				<?=$htmlAry["ReportSelection"]?>
				<div style="display:none;" class="warnMsgDiv" id="Report_Warn">
					<span class="tabletextrequire">*<?=$Lang["eReportCardKG"]["Setting"]["InputWarning"].$Lang["General"]["Report"]?></span>
				</div>
			</td>
		</tr>
		
		<tr>
			<td class="field_title"><?=$Lang['PowerPortfolio']['Reprots']['CAReport']['Topic']?></td>
			<td>
				<?=$htmlAry["TopicSelection"]?>
				<div style="display:none;" class="warnMsgDiv" id="Topic_Warn">
					<span class="tabletextrequire">*<?=$Lang["eReportCardKG"]["Setting"]["InputWarning"].$Lang['PowerPortfolio']['Reprots']['CAReport']['Topic']?></span>
				</div>
			</td>
		</tr>
    	<tr>
    		<td class="field_title"><?=$Lang["General"]["Class"]?></td>
    		<td>
    			<?=$htmlAry["ClassSelection"]?>
    			<div style="display:none;" class="warnMsgDiv" id="Class_Warn">
    				<span class="tabletextrequire">*<?=$Lang["eReportCardKG"]["Setting"]["InputWarning"].$Lang["General"]["Class"]?></span>
    			</div>
    		</td>
    	</tr>
		<tr>
			<td class="field_title"><?=$Lang["Identity"]["Student"]?></td>
			<td>
				<div id="studentSelDiv"><?php echo $htmlAry['StudentSelection']?></div>
				<div style="display:none;" class="warnMsgDiv" id="Student_Warn">
					<span class="tabletextrequire">*<?=$Lang["eReportCardKG"]["Setting"]["InputWarning"].$Lang["Identity"]["Student"]?></span>
				</div>
			</td>
		</tr>
		<tr>
			<td class="field_title"><?=$Lang['PowerPortfolio']['Reprots']['CAReport']["ReleaseDate"]?></td>
			<td>
				<input type="text" name="ReleaseDate" id="ReleaseDate" value="<?=date('Y-m-d')?>" />
			</td>
		</tr>
		</tbody>
	</table>
</form>

<div class="edit_bottom_v30">
	<input type="button" value="<?=$Lang["Btn"]["Print"]?>" class="formbutton_v30 print_hide " onclick="goSubmit()" id="submitBtn" name="submitBtn" <?php echo $disabled?>>
</div>