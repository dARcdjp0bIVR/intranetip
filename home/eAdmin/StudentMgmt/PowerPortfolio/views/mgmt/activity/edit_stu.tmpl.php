<link href="<?=$indexVar["thisBasePath"]?>templates/fontawesome/css/all.css" rel="stylesheet">
<link href="<?=$indexVar["thisBasePath"]?>templates/2009a/css/PowerPortfolio/style.css" rel="stylesheet">
<script src="<?=$indexVar["thisBasePath"]?>templates/2009a/js/PowerPortfolio/demo.js"></script>
<script type="text/javascript">
$(document).ready( function() {
	$('.photoField .tablelink').attr('target','_blank');
});

function listClass(val){
	if(val != ''){
		window.location.href = "index.php?task=mgmt.activity.list_stu&YearClassID="+val+"&ActivitySettingID=<?=$ActivitySettingID?>";
	}
}

function goBack(){
	window.location.href = "<?=$backURL?>";
}

function submitForm(){
	if(checkForm()){
		document.form1.submit();
	} else {
		alert('<?=$Lang['PowerPortfolio']['Management']['InputScore']['FormatWarningMsg']?>');
	}
}
function checkForm(){
	let checking = true;
	/*
	$('input.textboxnum').css('background-color','none').each(function(index){
		let val = $(this).val();
		if(!(val == 'N.A.'|| (val >= 0 && val <= maxNum))){
			checking = false;
			$(this).css('background-color','#ff0000');
		}
	});
	*/
	return checking;
}

function goNew() {
}

function goEdit(ActivitySettingID) {
}

function checkEdit(){
}
function checkCopy(){
	
}
function checkExport(){
	
}
function checkDelete(){
	
}
function jCHANGE_STATUS(){
	
}
function jCHANGE_STATUS(){
// 	document.getElementById("isOutdated").value = 1;
}
</script>

<form name="form1" id="form1" method="POST" action='index.php?task=mgmt.activity.update_stu' enctype="multipart/form-data">
	<div class="table_board">
		<div class="">
			<?=$htmlAry['selectionBox']?>
		</div>
		<br style="clear:both;">
		<div>
			<?=$htmlAry['navBar']?>
		</div>
		<br style="clear:both;">
		<p class="spacer"></p>
		<div>
			<?=$htmlAry['topicBar']?>
		</div>
		<br style="clear:both;">
		<p class="spacer"></p>
		<div class="content_top_tool">
			<?=$htmlAry['contentTool']?>
		</div>
		
		<?=$htmlAry['InputTable']?>
		<hr/>
		<table width="100%" border="0" cellspacing="0" cellpadding="2">
			<tr>
                <td align="center" valign="bottom"><?=$htmlAry['ActionBtn']?></td>
            </tr>
		</table>
	</div>
	
	<?=$htmlAry['hiddenField']?>
</form>