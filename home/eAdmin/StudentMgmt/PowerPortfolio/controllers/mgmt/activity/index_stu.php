<?php
// editing by 
/*
 * 
 */

// Include DB Table
include_once($indexVar['thisBasePath']."includes/libdbtable.php");
include_once($indexVar['thisBasePath']."includes/libdbtable2007a.php");

// Update Result Message
if($success){
	$returnMsg = $Lang['General']['ReturnMessage']['UpdateSuccess'];
}
else if($success==="0"){
	$returnMsg = $Lang['General']['ReturnMessage']['UpdateUnsuccess'];
}

// Page Title
// $TAGS_OBJ[] = array($Lang['PowerPortfolio']['Management']['InputScore']['Title']);
$TAGS_OBJ = array(
		array($Lang['PowerPortfolio']['Management']['Activity']['AccordingClass'],'index.php?task=mgmt.activity.index_class'),
		array($Lang['PowerPortfolio']['Management']['Activity']['AccordingStudent'],"javascript: void(0);", true)
);
$indexVar['libpowerportfolio_ui']->Echo_Module_Layout_Start($returnMsg);

// DB Table Settings
if (isset($ck_page_size) && $ck_page_size != "") {
	$page_size = $ck_page_size;
}
$field = ($field=='')? 3 : $field;
$order = ($order=='')? 1 : $order;
$page = ($page=='')? 1 : $page;
$pos = 0;

if($indexVar['libpowerportfolio']->IS_KG_CLASS_TEACHER() && !$indexVar['libpowerportfolio']->IS_POWER_PORTFOLIO_ADMIN_USER()){
	$TeachingClassAry = $indexVar['libpowerportfolio']->Get_Teaching_Class($_SESSION['UserID']);
	$TeachingClassIDAry = Get_Array_By_Key($TeachingClassAry, 'YearClassID');
} else {
	$TeachingClassIDAry = array();
}

$YearClassID = $_REQUEST['YearClassID'];

$btnAry = array();
// $htmlAry['contentTool'] = $indexVar['libpowerportfolio_ui']->Get_Content_Tool_By_Array_v30($btnAry);
$htmlAry['contentTool'] = $indexVar['libpowerportfolio_ui']->Get_Class_Selection($YearClassID, $onChange='document.form1.submit()', $withYearOptGroup=true, $noFirstTitle=false, '', 'YearClassID',false, $TeachingClassIDAry);

if($YearClassID != ''){
$StudentAry = $indexVar['libpowerportfolio']->Get_Student_By_Class($YearClassID);
$StudentIDAry = Get_Array_By_Key($StudentAry,'UserID');
$joinStudentID = implode("','", $StudentIDAry);
$studentTotal = sizeof($StudentIDAry);

$conds = " yc.YearClassID = '{$YearClassID}' ";
	
$tickImage = $indexVar['libpowerportfolio_ui']->Get_Tick_Image();

$sql = "SELECT
			CONCAT('<a href=\"index.php?task=mgmt.activity.list_stu&YearClassID={$YearClassID}&ActivitySettingID=', ras.ActivitySettingID,'\">', ras.ActivityName, '</a>') AS ActivityName,
			ras.ReportName AS ReportName,
			IF(ras.ApplyTopic = 1, ts.Name, ras.ActivityTitle) AS Topic,
			ras.ActivityDate AS AcitivtyDate,
			IF(COUNT(DISTINCT ra.RecordID) = '{$studentTotal}', '{$tickImage}', '') AS Status
		FROM {$indexVar['thisDbName']}.RC_ACTIVITY_SETTING ras
		INNER JOIN {$intranet_db}.YEAR_CLASS yc
			ON ras.YearID = yc.YearID
		LEFT JOIN {$indexVar['thisDbName']}.RC_TOPIC_SETTING ts 
			ON (ras.TopicSettingID = ts.TopicSettingID)
		LEFT JOIN {$indexVar['thisDbName']}.RC_ACTIVITY ra
			ON ras.ActivitySettingID = ra.ActivitySettingID AND ra.StudentID IN ('{$joinStudentID}')
		WHERE $conds
		Group By ras.ActivitySettingID
			";
// debug_pr($sql);die();
// Initiate DB Table
$li = new libdbtable2007($field, $order, $pageNo);
$li->sql = $sql;
$li->IsColOff = "IP25_table";
$li->field_array = array("ActivityName", "ReportName", "Topic", "AcitivtyDate", "Status");
$li->column_array = array(0, 0, 0);
$li->wrap_array = array(0, 0, 0);
$li->no_col = count($li->field_array)+1;
// debug_pr($li->built_sql());
$li->column_list .= "<th class='tabletop tabletopnolink' width='1'>#</td>\n";
$li->column_list .= "<th class='tabletop tabletopnolink' width='25%'>".$li->column($pos++, $Lang['PowerPortfolio']['Settings']['Activity']['Name'])."</td>\n";
$li->column_list .= "<th class='tabletop tabletopnolink' width='25%'>".$li->column($pos++, $Lang['PowerPortfolio']['Settings']['Activity']['ReportName'])."</td>\n";
$li->column_list .= "<th class='tabletop tabletopnolink' width='25%'>".$li->column($pos++, $Lang['PowerPortfolio']['Settings']['Activity']['ActivityTopic'].'/'.$Lang['PowerPortfolio']['Settings']['Activity']['ActivityTitle'])."</td>\n";
$li->column_list .= "<th class='tabletop tabletopnolink' width='10%'>".$li->column($pos++, $Lang['PowerPortfolio']['Settings']['Activity']['Date'])."</td>\n";
$li->column_list .= "<th class='tabletop tabletopnolink' width='15%'>".$li->column($pos++, $Lang['PowerPortfolio']['Management']['Activity']['InputStatus'])."</td>\n";

// GET DB Table Content
$htmlAry['dataTable'] = $li->display();

// DB Table related Hidden Fields
$htmlAry['hiddenField'] = "";
$htmlAry['hiddenField'] .= "<input type='hidden' name='pageNo' value='".$li->pageNo."'>";
$htmlAry['hiddenField'] .= "<input type='hidden' name='order' value='1'>";
$htmlAry['hiddenField'] .= "<input type='hidden' name='field' value='".$li->field."'>";
$htmlAry['hiddenField'] .= "<input type='hidden' name='page_size_change' value=''>";
$htmlAry['hiddenField'] .= "<input type='hidden' name='numPerPage' value='".$li->page_size."'>";
}
$htmlAry['hiddenField'] .= "<input type='hidden' name='task' value='mgmt.activity.index_stu'>";
?>