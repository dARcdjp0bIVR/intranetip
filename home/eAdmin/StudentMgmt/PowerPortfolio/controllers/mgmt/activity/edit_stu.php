<?php
// editing by 
/*
 * 
 */

// Include DB Table
include_once($indexVar['thisBasePath']."includes/libdbtable.php");
include_once($indexVar['thisBasePath']."includes/libdbtable2007a.php");
include_once($indexVar['thisBasePath']."includes/libuser.php");

// Update Result Message
if($success){
	$returnMsg = $Lang['General']['ReturnMessage']['UpdateSuccess'];
}
else if($success==="0"){
	$returnMsg = $Lang['General']['ReturnMessage']['UpdateUnsuccess'];
}

// Page Title
$TAGS_OBJ = array(
		array($Lang['PowerPortfolio']['Management']['Activity']['AccordingClass'],'index.php?task=mgmt.input_score.index_class'),
		array($Lang['PowerPortfolio']['Management']['Activity']['AccordingStudent'],"javascript: void(0);", true)
);
$indexVar['libpowerportfolio_ui']->Echo_Module_Layout_Start($returnMsg);

// DB Table Settings

if($indexVar['libpowerportfolio']->IS_KG_CLASS_TEACHER() && !$indexVar['libpowerportfolio']->IS_POWER_PORTFOLIO_ADMIN_USER()){
	$TeachingClassAry = $indexVar['libpowerportfolio']->Get_Teaching_Class($_SESSION['UserID']);
	$TeachingClassIDAry = Get_Array_By_Key($TeachingClassAry, 'YearClassID');
} else {
	$TeachingClassIDAry = array();
}
$YearClassID = $_REQUEST['YearClassID'];
$ActivitySettingID = $_REQUEST['ActivitySettingID'];
$StudentID = $_REQUEST['StudentID'];

$lu = new libuser($StudentID);
$studentName = Get_Lang_Selection($lu->ChineseName, $lu->EnglishName);

$activitySettingInfo = $indexVar['libpowerportfolio']->Get_Activity_Setting($ActivitySettingID);
$activitySettingInfo = $activitySettingInfo[0];
$recordAry = $indexVar['libpowerportfolio']->Get_Activity_Record($ActivitySettingID, '', array($StudentID));

$_thisRecord = $recordAry[0];
// record Data here
$_thisDate = $_thisRecord['ActivityDate'] ? $_thisRecord['ActivityDate'] : $activitySettingInfo['ActivityDate'];
$_thisDuration = $_thisRecord ? $_thisRecord['Duration'] : $activitySettingInfo['ActivityTime'];
$_thisLocation = $_thisRecord ? $_thisRecord['Location'] : $activitySettingInfo['Location'];
$_thisImgSrc = $_thisRecord['imgSrc'];
$hiddenImg = "";
if($_thisImgSrc != ''){
	$hiddenImg = "<br/><input type='hidden' name='existImgSrc' id='existImgSrc' value='{$_thisImgSrc}' />";
	$_filename = substr($_thisImgSrc, strrpos($_thisImgSrc, '/'));
	$hiddenImg .= $indexVar['libpowerportfolio_ui']->Get_Photo_Icon($indexVar['thisImage'].'/activity_record/'.$_thisImgSrc,'',$_filename);
}
$_thisDescription = $_thisRecord['Description'];
$_thisDatePick = $indexVar['libpowerportfolio_ui']->GET_DATE_PICKER("date",$_thisDate);
$_thisTextArea = $indexVar['libpowerportfolio_ui']->GET_TEXTAREA("description",$_thisDescription, 20);
// debug_pr($recordAry);

$backURL = "index.php?task=mgmt.activity.list_stu&YearClassID={$YearClassID}&ActivitySettingID={$ActivitySettingID}";
$pages_arr = array();
$pages_arr[] = array($Lang['PowerPortfolio']['Management']['Activity']['Title'], 'index.php?task=mgmt.activity.index_stu&YearClassID='.$YearClassID);
$pages_arr[] = array($activitySettingInfo['ActivityName'], $backURL);
$pages_arr[] = array($studentName, '');


$htmlAry['selectionBox'] = $indexVar['libpowerportfolio_ui']->Get_Class_Selection($YearClassID, $onChange='document.form1.submit()', $withYearOptGroup=true, $noFirstTitle=false, $targetYearID, 'YearClassID',false, $TeachingClassIDAry);

$htmlAry['navBar'] = $indexVar['libpowerportfolio_ui']->GET_NAVIGATION_IP25($pages_arr);



$html = "<table class='form_table_v30 '>";
	$html.= "<tbody>";
		$html .= "<tr>";
			$html .= "<td class='field_title'>{$Lang['PowerPortfolio']['Settings']['Activity']['Date']}</td>";
			$html .= "<td>{$_thisDatePick}</td>";
		$html .= "</tr>";
		$html .= "<tr>";
			$html .= "<td class='field_title'>{$Lang['PowerPortfolio']['Settings']['Activity']['Duration']}</td>";
			$html .= "<td><input type='text' class='textboxnum' name='duration' id='duration' value='{$_thisDuration}' /></td>";
		$html .= "</tr>";
		$html .= "<tr>";
			$html .= "<td class='field_title'>{$Lang['PowerPortfolio']['Settings']['Activity']['Location']}</td>";
			$html .= "<td><input type='text' class='textboxnum' name='location' id='location' value='{$_thisLocation}' /></td>";
		$html .= "</tr>";
		$html .= "<tr>";
			$html .= "<td class='field_title'>{$Lang['PowerPortfolio']['Management']['Activity']['ActivityPhoto']}</td>";
			$html .= "<td class='photoField'><input type='file' name='imageUpload' id='imageUpload' />{$hiddenImg}</td>";
		$html .= "</tr>";
		$html .= "<tr>";
			$html .= "<td class='field_title'>{$Lang['PowerPortfolio']['Management']['Activity']['Description']}</td>";
			$html .= "<td>{$_thisTextArea}</td>";
		$html .= "</tr>";
	$html.= "</tbody>";
$html .= "</table>";

$htmlAry['InputTable'] = $html;


$html = "";
$html .= $indexVar['libpowerportfolio_ui']->GET_ACTION_BTN($Lang['Btn']['Submit'], 'button', $ParOnClick="submitForm()");
$html .= $indexVar['libpowerportfolio_ui']->GET_ACTION_BTN($Lang['Btn']['Back'], 'button', $ParOnClick="goBack()") . '&nbsp;';

$htmlAry['ActionBtn'] = $html;

$htmlAry['hiddenField'] .= "<input type='hidden' name='ActivitySettingID' value='{$ActivitySettingID}'>";
$htmlAry['hiddenField'] .= "<input type='hidden' name='YearClassID' value='{$YearClassID}'>";
$htmlAry['hiddenField'] .= "<input type='hidden' name='StudentID' value='{$StudentID}'>";
?>