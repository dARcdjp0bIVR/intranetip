<?php
// editing by 
/*
 * 
 */

// Include DB Table
include_once($indexVar['thisBasePath']."includes/libdbtable.php");
include_once($indexVar['thisBasePath']."includes/libdbtable2007a.php");

// Update Result Message
if($success){
	$returnMsg = $Lang['General']['ReturnMessage']['UpdateSuccess'];
}
else if($success==="0"){
	$returnMsg = $Lang['General']['ReturnMessage']['UpdateUnsuccess'];
}

// Page Title
$TAGS_OBJ = array(
		array($Lang['PowerPortfolio']['Management']['Activity']['AccordingClass'],'index.php?task=mgmt.activity.index_class'),
		array($Lang['PowerPortfolio']['Management']['Activity']['AccordingStudent'],"javascript: void(0);", true)
);
$indexVar['libpowerportfolio_ui']->Echo_Module_Layout_Start($returnMsg);

// DB Table Settings

if($indexVar['libpowerportfolio']->IS_KG_CLASS_TEACHER() && !$indexVar['libpowerportfolio']->IS_POWER_PORTFOLIO_ADMIN_USER()){
	$TeachingClassAry = $indexVar['libpowerportfolio']->Get_Teaching_Class($_SESSION['UserID']);
	$TeachingClassIDAry = Get_Array_By_Key($TeachingClassAry, 'YearClassID');
} else {
	$TeachingClassIDAry = array();
}

$YearClassID = $_REQUEST['YearClassID'];
$ActivitySettingID = $_REQUEST['ActivitySettingID'];

$StudentAry = $indexVar['libpowerportfolio']->Get_Student_By_Class($YearClassID);
$StudentIDAry = Get_Array_By_Key($StudentAry,'UserID');
// $StudentAry = BuildMultiKeyAssoc($StudentAry, $UserID);
$activitySettingInfo = $indexVar['libpowerportfolio']->Get_Activity_Setting($ActivitySettingID);
$activitySettingInfo = $activitySettingInfo[0];
$recordAry = $indexVar['libpowerportfolio']->Get_Activity_Record($ActivitySettingID, '', $StudentIDAry);
$recordAry = BuildMultiKeyAssoc($recordAry, 'StudentID');

$tickImage = $indexVar['libpowerportfolio_ui']->Get_Tick_Image();

$pages_arr = array();
$pages_arr[] = array($Lang['PowerPortfolio']['Management']['Activity']['Title'], 'index.php?task=mgmt.activity.index_stu&YearClassID='.$YearClassID);
$pages_arr[] = array($activitySettingInfo['ActivityName'], '');

$htmlAry['selectionBox'] = $indexVar['libpowerportfolio_ui']->Get_Class_Selection($YearClassID, $onChange='document.form1.submit()', $withYearOptGroup=true, $noFirstTitle=false, $targetYearID, 'YearClassID',false, $TeachingClassIDAry);

$htmlAry['navBar'] = $indexVar['libpowerportfolio_ui']->GET_NAVIGATION_IP25($pages_arr);

$html = '<table class="common_table_list_v30 ">';
	$html .= "<thead>";
		$html .= "<tr class='tabletop'>";
			$html .= "<th class='tabletop tabletopnolink' width='10%'>#</th>";
			$html .= "<th class='tabletop tabletopnolink' width='60%'>{$Lang['PowerPortfolio']['Management']['InputScore']['Student']}</th>";
			$html .= "<th class='tabletop tabletopnolink' width='30%'>{$Lang['PowerPortfolio']['Management']['InputScore']['InputStatus']}</th>";
		$html .= "</tr>";
	$html .= "</thead>";
	$html .= "<tbody>";
	$i=1;
	foreach($StudentAry as $stu){
		$_thisStudentName = $stu['StudentName'];
		$_thisStudentID = $stu['UserID'];
		$_thisStatus = isset($recordAry[$_thisStudentID]);
		if($_thisStatus){
			$_img = $tickImage;
		} else {
			$_img = '';
		}
		$html .= "<tr>";
			$html .= "<td>{$i}</td>";
			$html .= "<td><a href='index.php?task=mgmt.activity.edit_stu&ActivitySettingID={$ActivitySettingID}&YearClassID={$YearClassID}&StudentID={$_thisStudentID}'>{$_thisStudentName}</a></td>";
			$html .= "<td>{$_img}</td>";
		$html .= "</tr>";
		$i++;
	}
	$html .= "</tbody>";
$html .= "</table>";

$htmlAry['RubricTable'] = $html;

$htmlAry['hiddenField'] .= "<input type='hidden' name='task' value='mgmt.activity.list_stu'>";
?>