<?php
// editing by 
/*
 * 
 */

// Include DB Table
include_once($indexVar['thisBasePath']."includes/libdbtable.php");
include_once($indexVar['thisBasePath']."includes/libdbtable2007a.php");

// Update Result Message
if($success){
	$returnMsg = $Lang['General']['ReturnMessage']['UpdateSuccess'];
}
else if($success==="0"){
	$returnMsg = $Lang['General']['ReturnMessage']['UpdateUnsuccess'];
}

// Page Title
$TAGS_OBJ = array(
		array($Lang['PowerPortfolio']['Management']['Activity']['AccordingClass'],"javascript: void(0);", true),
		array($Lang['PowerPortfolio']['Management']['Activity']['AccordingStudent'], 'index.php?task=mgmt.activity.index_stu')
);
$indexVar['libpowerportfolio_ui']->Echo_Module_Layout_Start($returnMsg);

// DB Table Settings

if($indexVar['libpowerportfolio']->IS_KG_CLASS_TEACHER() && !$indexVar['libpowerportfolio']->IS_POWER_PORTFOLIO_ADMIN_USER()){
	$TeachingClassAry = $indexVar['libpowerportfolio']->Get_Teaching_Class($_SESSION['UserID']);
	$TeachingClassIDAry = Get_Array_By_Key($TeachingClassAry, 'YearClassID');
} else {
	$TeachingClassIDAry = array();
}
$YearClassID = $_REQUEST['YearClassID'];
$ActivitySettingID = $_REQUEST['ActivitySettingID'];

$StudentAry = $indexVar['libpowerportfolio']->Get_Student_By_Class($YearClassID);
$StudentIDAry = Get_Array_By_Key($StudentAry,'UserID');
// $StudentAry = BuildMultiKeyAssoc($StudentAry, $UserID);
$activitySettingInfo = $indexVar['libpowerportfolio']->Get_Activity_Setting($ActivitySettingID);
$activitySettingInfo = $activitySettingInfo[0];
$recordAry = $indexVar['libpowerportfolio']->Get_Activity_Record($ActivitySettingID, '', $StudentIDAry);
$recordAry = BuildMultiKeyAssoc($recordAry, 'StudentID');
// debug_pr($recordAry);

$pages_arr = array();
$pages_arr[] = array($Lang['PowerPortfolio']['Management']['Activity']['Title'], 'index.php?task=mgmt.activity.index_class&YearClassID='.$YearClassID);
$pages_arr[] = array($activitySettingInfo['ActivityName'], '');

$backURL = "index.php?task=mgmt.activity.index_class&YearClassID={$YearClassID}";

$htmlAry['selectionBox'] = $indexVar['libpowerportfolio_ui']->Get_Class_Selection($YearClassID, $onChange='document.form1.submit()', $withYearOptGroup=true, $noFirstTitle=false, $targetYearID, 'YearClassID',false, $TeachingClassIDAry);

$htmlAry['navBar'] = $indexVar['libpowerportfolio_ui']->GET_NAVIGATION_IP25($pages_arr);



$html = "<table class='common_table_list_v30 '>";
	$html.= "<thead>";
		$html .= "<tr>";
			$html .= "<th width='1'>#</th>";
			$html .= "<th width='10%'>{$Lang['PowerPortfolio']['Management']['Activity']['Student']}</th>";
			$html .= "<th width='25%'>{$Lang['PowerPortfolio']['Settings']['Activity']['Date']}</th>";
			$html .= "<th width='15%'>{$Lang['PowerPortfolio']['Settings']['Activity']['Duration']}</th>";
			$html .= "<th width='15%'>{$Lang['PowerPortfolio']['Settings']['Activity']['Location']}</th>";
			$html .= "<th width='15%'>{$Lang['PowerPortfolio']['Management']['Activity']['ActivityPhoto']}</th>";
			$html .= "<th width='20%'>{$Lang['PowerPortfolio']['Management']['Activity']['Description']}</th>";
		$html .= "</tr>";
	$html.= "</thead>";
	$html.= "<tbody>";
	$i = 1;
	foreach($StudentAry as $student){
		$_thisClassNumber = $student['ClassNumber'];
		$_thisClassName = $student['ClassName'];
		$_thisStudentID = $student['UserID'];
		$_thisStudentName = $student['StudentName'];
		$_thisRecord = $recordAry[$_thisStudentID];
		// record Data here
		$_thisDate = $_thisRecord['ActivityDate'] ? $_thisRecord['ActivityDate'] : $activitySettingInfo['ActivityDate'];
		$_thisDuration = $_thisRecord ? $_thisRecord['Duration'] : $activitySettingInfo['ActivityTime'];
		$_thisLocation = $_thisRecord ? $_thisRecord['Location'] : $activitySettingInfo['Location'];
		$_thisImgSrc = $_thisRecord['imgSrc'];
		$hiddenImg = "";
		if($_thisImgSrc != ''){
			$hiddenImg = "<input type='hidden' name='existImgSrc[{$_thisStudentID}]' id='existImgSrc[{$_thisStudentID}]' value='{$_thisImgSrc}' />";
			$_filename = substr($_thisImgSrc, strrpos($_thisImgSrc, '/'));
			$hiddenImg .= $indexVar['libpowerportfolio_ui']->Get_Photo_Icon($indexVar['thisImage'].'/activity_record/'.$_thisImgSrc,'',$_filename);
		}
		$_thisDescription = $_thisRecord['Description'];
		$_thisDatePick = $indexVar['libpowerportfolio_ui']->GET_DATE_PICKER("date_{$_thisStudentID}",$_thisDate);
		$_thisTextArea = $indexVar['libpowerportfolio_ui']->GET_TEXTAREA("description[{$_thisStudentID}]",$_thisDescription, 20);
		$html .= "<tr class='dataRow'>";
			$html .= "<td>{$i}</td>";
			$html .= "<td>{$_thisStudentName}</td>";
			$html .= "<td>{$_thisDatePick}</td>";
			$html .= "<td><input type='text' class='textboxnum' name='duration[{$_thisStudentID}]' id='duration[{$_thisStudentID}]' value='{$_thisDuration}' /></td>";
			$html .= "<td><input type='text' class='textboxnum' name='location[{$_thisStudentID}]' id='location[{$_thisStudentID}]' value='{$_thisLocation}' /></td>";
			$html .= "<td class='photoField'><input type='file' name='imageUpload[{$_thisStudentID}]' id='imageUpload[{$_thisStudentID}]' />{$hiddenImg}</td>";
			$html .= "<td>{$_thisTextArea}</td>";
			$j = 0;
		$html .= "<tr>";
		$i++;
	}
	$html.= "</tbody>";
$html .= "</table>";

$htmlAry['InputScoreTable'] = $html;


$html = "";
$html .= $indexVar['libpowerportfolio_ui']->GET_ACTION_BTN($Lang['Btn']['Submit'], 'button', $ParOnClick="submitForm()");
$html .= $indexVar['libpowerportfolio_ui']->GET_ACTION_BTN($Lang['Btn']['Back'], 'button', $ParOnClick="goBack()") . '&nbsp;';
$htmlAry['ActionBtn'] = $html;

$htmlAry['hiddenField'] .= "<input type='hidden' name='ActivitySettingID' value='{$ActivitySettingID}'>";
$htmlAry['hiddenField'] .= "<input type='hidden' name='YearClassID' value='{$YearClassID}'>";
?>