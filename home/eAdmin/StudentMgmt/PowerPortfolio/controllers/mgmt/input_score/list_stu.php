<?php
// editing by 
/*
 * 
 */

// Include DB Table
include_once($indexVar['thisBasePath']."includes/libdbtable.php");
include_once($indexVar['thisBasePath']."includes/libdbtable2007a.php");

// Update Result Message
if($success){
	$returnMsg = $Lang['General']['ReturnMessage']['UpdateSuccess'];
}
else if($success==="0"){
	$returnMsg = $Lang['General']['ReturnMessage']['UpdateUnsuccess'];
}

// Page Title
$TAGS_OBJ = array(
		array($Lang['PowerPortfolio']['Management']['InputScore']['AccordingClass'], 'index.php?task=mgmt.input_score.index_class'),
		array($Lang['PowerPortfolio']['Management']['InputScore']['AccordingStudent'],"javascript: void(0);", true)
);
$indexVar['libpowerportfolio_ui']->Echo_Module_Layout_Start($returnMsg);

// DB Table Settings

if($indexVar['libpowerportfolio']->IS_KG_CLASS_TEACHER() && !$indexVar['libpowerportfolio']->IS_POWER_PORTFOLIO_ADMIN_USER()){
	$TeachingClassAry = $indexVar['libpowerportfolio']->Get_Teaching_Class($_SESSION['UserID']);
	$TeachingClassIDAry = Get_Array_By_Key($TeachingClassAry, 'YearClassID');
} else {
	$TeachingClassIDAry = array();
}

$YearClassID = $_REQUEST['YearClassID'];
$TopicSettingID = $_REQUEST['TopicSettingID'];
$TopicSettingInfo = $indexVar['libpowerportfolio']->Get_Rubric_Topic_Setting($TopicSettingID);
$TopicSettingInfo = $TopicSettingInfo[0];
$rubricSettingID = $TopicSettingInfo['RubricSettingID'];
$targetYearID = $TopicSettingInfo['YearID'];

$rubricSettingInfo = $indexVar['libpowerportfolio']->Get_Rubric_Setting($rubricSettingID);
$maxLevel = $rubricSettingInfo[0]['LevelNum'];
$rubricIndexValueAry = $indexVar['libpowerportfolio']->Get_Rubric_Ability_Index_Item($rubricSettingID);
$valueList = $TopicSettingInfo['RubricList'];
$valueListAry = explode(',', $valueList);

$StudentAry = $indexVar['libpowerportfolio']->Get_Student_By_Class($YearClassID);

$tickImage = $indexVar['libpowerportfolio_ui']->Get_Tick_Image();

$pages_arr = array();
$pages_arr[] = array($Lang['PowerPortfolio']['Management']['InputScore']['Topic'], 'index.php?task=mgmt.input_score.index_stu&YearClassID='.$YearClassID);
$pages_arr[] = array($TopicSettingInfo['Name'], '');

$htmlAry['selectionBox'] = $indexVar['libpowerportfolio_ui']->Get_Class_Selection($YearClassID, $onChange='document.form1.submit()', $withYearOptGroup=true, $noFirstTitle=false, $targetYearID, 'YearClassID',false, $TeachingClassIDAry);

$htmlAry['navBar'] = $indexVar['libpowerportfolio_ui']->GET_NAVIGATION_IP25($pages_arr);

$html = '<table class="common_table_list_v30 ">';
	$html .= "<thead>";
		$html .= "<tr class='tabletop'>";
			$html .= "<th class='tabletop tabletopnolink' width='10%'>#</th>";
			$html .= "<th class='tabletop tabletopnolink' width='60%'>{$Lang['PowerPortfolio']['Management']['InputScore']['Student']}</th>";
			$html .= "<th class='tabletop tabletopnolink' width='30%'>{$Lang['PowerPortfolio']['Management']['InputScore']['InputStatus']}</th>";
		$html .= "</tr>";
	$html .= "</thead>";
	$html .= "<tbody>";
	$i=1;
	foreach($StudentAry as $stu){
		$_thisStudentName = $stu['StudentName'];
		$_thisStudentID = $stu['UserID'];
		$_thisScoreStatus= $indexVar['libpowerportfolio']->Check_Student_Score_Status($TopicSettingID, $_thisStudentID);
		if($_thisScoreStatus){
			$_img = $tickImage;
		} else {
			$_img = '';
		}
		$html .= "<tr>";
			$html .= "<td>{$i}</td>";
			$html .= "<td><a href='index.php?task=mgmt.input_score.edit_stu&TopicSettingID={$TopicSettingID}&YearClassID={$YearClassID}&StudentID={$_thisStudentID}'>{$_thisStudentName}</a></td>";
			$html .= "<td>{$_img}</td>";
		$html .= "</tr>";
		$i++;
	}
	$html .= "</tbody>";
$html .= "</table>";

$htmlAry['RubricTable'] = $html;

$htmlAry['hiddenField'] .= "<input type='hidden' name='task' value='mgmt.input_score.list_class'>";
?>