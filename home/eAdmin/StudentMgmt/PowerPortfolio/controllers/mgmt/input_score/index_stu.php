<?php
// editing by 
/*
 * 
 */

// Include DB Table
include_once($indexVar['thisBasePath']."includes/libdbtable.php");
include_once($indexVar['thisBasePath']."includes/libdbtable2007a.php");

// Update Result Message
if($success){
	$returnMsg = $Lang['General']['ReturnMessage']['UpdateSuccess'];
}
else if($success==="0"){
	$returnMsg = $Lang['General']['ReturnMessage']['UpdateUnsuccess'];
}

// Page Title
// $TAGS_OBJ[] = array($Lang['PowerPortfolio']['Management']['InputScore']['Title']);
$TAGS_OBJ = array(
		array($Lang['PowerPortfolio']['Management']['InputScore']['AccordingClass'], 'index.php?task=mgmt.input_score.index_class'),
		array($Lang['PowerPortfolio']['Management']['InputScore']['AccordingStudent'],"javascript: void(0);", true)
);
$indexVar['libpowerportfolio_ui']->Echo_Module_Layout_Start($returnMsg);

// DB Table Settings
if (isset($ck_page_size) && $ck_page_size != "") {
	$page_size = $ck_page_size;
}
$field = ($field=='')? 0 : $field;
$order = ($order=='')? 1 : $order;
$page = ($page=='')? 1 : $page;
$pos = 0;
if($indexVar['libpowerportfolio']->IS_KG_CLASS_TEACHER() && !$indexVar['libpowerportfolio']->IS_POWER_PORTFOLIO_ADMIN_USER()){
	$TeachingClassAry = $indexVar['libpowerportfolio']->Get_Teaching_Class($_SESSION['UserID']);
	$TeachingClassIDAry = Get_Array_By_Key($TeachingClassAry, 'YearClassID');
} else {
	$TeachingClassIDAry = array();
}

if($indexVar['libpowerportfolio']->IS_KG_CLASS_TEACHER() && !isset($_REQUEST['YearClassID'])){
	$YearClassID = $TeachingClassAry[0]['YearClassID'];
} else {
	$YearClassID = $_REQUEST['YearClassID'];
}

$btnAry = array();
// $htmlAry['contentTool'] = $indexVar['libpowerportfolio_ui']->Get_Content_Tool_By_Array_v30($btnAry);
$htmlAry['contentTool'] = $indexVar['libpowerportfolio_ui']->Get_Class_Selection($YearClassID, $onChange='document.form1.submit()', $withYearOptGroup=true, $noFirstTitle=false, '', 'YearClassID',false, $TeachingClassIDAry);

if($YearClassID){
	$yearClassCond = "AND yc.YearClassID = '{$YearClassID}'";
/*
$sql = "SELECT
			CONCAT('<a href=\"index.php?task=mgmt.input_score.list_stu&YearClassID={$YearClassID}&TopicSettingID=',ts.TopicSettingID, '\">', ts.Name,'</a>') AS TopicName,
			CONCAT(ts.ExamPeriodStart,' {$Lang['General']['To']} ',ts.ExamPeriodEnd) AS AssessmentPeriod,
			'' AS Status
		FROM
			".$indexVar['thisDbName'].".RC_TOPIC_SETTING ts
		INNER JOIN {$intranet_db}.YEAR y
			ON ts.YearID = y.YearID
		INNER JOIN {$intranet_db}.YEAR_CLASS yc
			ON yc.YearID = y.YearID
		WHERE 1
			$yearClassCond
		Group By ts.TopicSettingID
			";
*/
$tickImage = $indexVar['libpowerportfolio_ui']->Get_Tick_Image();

$sql = "SELECT
			CONCAT('<a href=\"index.php?task=mgmt.input_score.list_stu&YearClassID={$YearClassID}&TopicSettingID=',ts.TopicSettingID, '\">', ts.Name,'</a>') AS TopicName,
			CONCAT(ts.ExamPeriodStart,' {$Lang['General']['To']} ',ts.ExamPeriodEnd, '  ') AS AssessmentPeriod,
			IF( COUNT(DISTINCT ss.InputScoreID) = (COUNT(DISTINCT raii.ItemID) * COUNT(DISTINCT ycu.UserID)), '{$tickImage}', '') AS Status
		FROM
			".$indexVar['thisDbName'].".RC_TOPIC_SETTING ts
		INNER JOIN {$intranet_db}.YEAR y
			ON ts.YearID = y.YearID
		INNER JOIN {$intranet_db}.YEAR_CLASS yc
			ON yc.YearID = y.YearID
		INNER JOIN {$intranet_db}.YEAR_CLASS_USER ycu
			ON ycu.YearClassID = yc.YearClassID
		INNER JOIN {$indexVar['libpowerportfolio']->DBName}.RC_ABILITY_INDEX_ITEM raii
			ON raii.RubricSettingID = ts.RubricSettingID AND FIND_IN_SET(raii.Code, ts.RubricList)
		LEFT OUTER JOIN {$indexVar['libpowerportfolio']->DBName}.RC_STUDENT_SCORE ss
			ON ycu.UserID = ss. StudentID AND FIND_IN_SET(ss.RubricCode, ts.RubricList) AND ((ss.isNA = '1' AND Score = '0') OR (ss.Score <> '0' AND ss.isNA = '0'))
		WHERE 1
			$yearClassCond
		Group By ts.TopicSettingID
";
// Initiate DB Table
$li = new libdbtable2007($field, $order, $pageNo);
$li->sql = $sql;
$li->IsColOff = "IP25_table";
$li->field_array = array("TopicName", "AssessmentPeriod", "Status");
$li->column_array = array(0, 0, 0);
$li->wrap_array = array(0, 0, 0);
$li->no_col = count($li->field_array)+1;
// debug_pr($li->built_sql());
$li->column_list .= "<th class='tabletop tabletopnolink' width='1'>#</td>\n";
$li->column_list .= "<th class='tabletop tabletopnolink' width='45%'>".$li->column($pos++, $Lang['PowerPortfolio']['Settings']['Topic']['Name'])."</td>\n";
$li->column_list .= "<th class='tabletop tabletopnolink' width='45%'>".$li->column($pos++, $Lang['PowerPortfolio']['Settings']['Topic']['AssessmentPeriod'])."</td>\n";
$li->column_list .= "<th class='tabletop tabletopnolink' width='10%'>".$li->column($pos++, $Lang['PowerPortfolio']['Management']['InputScore']['InputStatus'])."</td>\n";

// GET DB Table Content
$htmlAry['dataTable'] = $li->display();

// DB Table related Hidden Fields
$htmlAry['hiddenField'] = "";
$htmlAry['hiddenField'] .= "<input type='hidden' name='pageNo' value='".$li->pageNo."'>";
$htmlAry['hiddenField'] .= "<input type='hidden' name='order' value='1'>";
$htmlAry['hiddenField'] .= "<input type='hidden' name='field' value='".$li->field."'>";
$htmlAry['hiddenField'] .= "<input type='hidden' name='page_size_change' value=''>";
$htmlAry['hiddenField'] .= "<input type='hidden' name='numPerPage' value='".$li->page_size."'>";
}
$htmlAry['hiddenField'] .= "<input type='hidden' name='task' value='mgmt.input_score.index_stu'>";
?>