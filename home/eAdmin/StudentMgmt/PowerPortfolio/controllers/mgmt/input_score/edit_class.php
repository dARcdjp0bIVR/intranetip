<?php
// editing by 
/*
 * 
 */

// Include DB Table
include_once($indexVar['thisBasePath']."includes/libdbtable.php");
include_once($indexVar['thisBasePath']."includes/libdbtable2007a.php");

// Update Result Message
if($success){
	$returnMsg = $Lang['General']['ReturnMessage']['UpdateSuccess'];
}
else if($success==="0"){
	$returnMsg = $Lang['General']['ReturnMessage']['UpdateUnsuccess'];
}

// Page Title
$TAGS_OBJ = array(
		array($Lang['PowerPortfolio']['Management']['InputScore']['AccordingClass'],"javascript: void(0);", true),
		array($Lang['PowerPortfolio']['Management']['InputScore']['AccordingStudent'], 'index.php?task=mgmt.input_score.index_stu')
);
$indexVar['libpowerportfolio_ui']->Echo_Module_Layout_Start($returnMsg);

// DB Table Settings

if($indexVar['libpowerportfolio']->IS_KG_CLASS_TEACHER() && !$indexVar['libpowerportfolio']->IS_POWER_PORTFOLIO_ADMIN_USER()){
	$TeachingClassAry = $indexVar['libpowerportfolio']->Get_Teaching_Class($_SESSION['UserID']);
	$TeachingClassIDAry = Get_Array_By_Key($TeachingClassAry, 'YearClassID');
} else {
	$TeachingClassIDAry = array();
}

$YearClassID = $_REQUEST['YearClassID'];
$TopicSettingID = $_REQUEST['TopicSettingID'];
$RubricCode = $_REQUEST['RubricCode'];

$TopicSettingInfo = $indexVar['libpowerportfolio']->Get_Rubric_Topic_Setting($TopicSettingID);
$TopicSettingInfo = $TopicSettingInfo[0];
$isDuringPeriod = date('Y-m-d') >= $TopicSettingInfo['MarkPeriodStart'] && date('Y-m-d') <= $TopicSettingInfo['MarkPeriodEnd'];
$rubricSettingID = $TopicSettingInfo['RubricSettingID'];
$targetYearID = $TopicSettingInfo['YearID'];

$rubricSettingInfo = $indexVar['libpowerportfolio']->Get_Rubric_Setting($rubricSettingID);
$rubricSettingInfo = $rubricSettingInfo[0];
$rubricIndexValueAry = $indexVar['libpowerportfolio']->Get_Rubric_Ability_Index_Item($rubricSettingID);
$valueList = $TopicSettingInfo['RubricList'];
$valueListAry = explode(',', $valueList);

$gradeNum = $rubricSettingInfo['GradeNum'];
$GradeNumContentAry = $indexVar['libpowerportfolio']->Get_Rubric_Setting_Item($rubricSettingID, 'GradeNumContent');
$GradeNumContentAry = BuildMultiKeyAssoc($GradeNumContentAry, 'Level');

$availableRubricAry = array();
foreach($rubricIndexValueAry as $rubricIndexValue){
	$_thisUpperCat = $rubricIndexValue['UpperCat'];
	$_thisCode = $rubricIndexValue['Code'];
	if($_thisCode == $RubricCode){
		$rubricInfo = $rubricIndexValue;
	}
	if($_thisUpperCat == $RubricCode &&in_array($_thisCode, $valueListAry)){
		$availableRubricAry[] = $rubricIndexValue;
	}
}
$codeAry = Get_Array_By_Key($availableRubricAry, 'Code');

$StudentAry = $indexVar['libpowerportfolio']->Get_Student_By_Class($YearClassID);
$StudentIDAry = Get_Array_By_Key($StudentAry,'UserID');

$scoreAry = $indexVar['libpowerportfolio']->Get_Student_Score($StudentIDAry, array($TopicSettingID), $codeAry);
// debug_pr($scoreAry);
$studentScoreAry = array();
foreach($scoreAry as $score){
	$_studentID = $score['StudentID'];
	$_code = $score['RubricCode'];
	if($score['isNA'] == 1){
		$studentScoreAry[$_studentID][$_code] = 'N.A.';
	} else {
		$studentScoreAry[$_studentID][$_code] = $score['Score'];
	}
}

$backURL = "index.php?task=mgmt.input_score.list_class&YearClassID={$YearClassID}&TopicSettingID={$TopicSettingID}";

$pages_arr = array();
$pages_arr[] = array($Lang['PowerPortfolio']['Management']['InputScore']['Topic'], 'index.php?task=mgmt.input_score.index_class&YearClassID='.$YearClassID);
$pages_arr[] = array($TopicSettingInfo['Name'], $backURL);
$pages_arr[] = array($rubricInfo['Code'].' '.$rubricInfo['Name'], '');

$htmlAry['selectionBox'] = $indexVar['libpowerportfolio_ui']->Get_Class_Selection($YearClassID, $onChange='document.form1.submit()', $withYearOptGroup=true, $noFirstTitle=false, $targetYearID, 'YearClassID',false, $TeachingClassIDAry);

$htmlAry['navBar'] = $indexVar['libpowerportfolio_ui']->GET_NAVIGATION_IP25($pages_arr);

$gradeDisplayAry = array();
for($i=$gradeNum;$i>0;$i--){
	$gradeDisplayAry[] = $i .' - '.$GradeNumContentAry[$i]['Content'];
}
$html = "<table class='form_table_v30'>";
	$html .= "<tr>";
		$html .= "<td class='field_title'>{$Lang['PowerPortfolio']['Management']['InputScore']['GradeDescription']}</td>";
		$html .= "<td>".implode(", ",$gradeDisplayAry)."</td>";
	$html .= "</tr>";
$html.= "</table>";
$htmlAry['GradeDisplay'] = $html;

$gradeSelection = array();
for($i=$gradeNum;$i>=1;$i--){
	$gradeSelection[] = array($i,$i);
}
$gradeSelection[] = array('0','--');
$gradeSelection[] = array('N.A.',$Lang['PowerPortfolio']['Management']['InputScore']['N.A.']);

$html = "<table class='common_table_list_v30 '>";
	$html.= "<thead>";
		$html .= "<tr>";
			$html .= "<th width='10%'>{$Lang['PowerPortfolio']['Management']['InputScore']['ClassAndClassNo']}</th>";
			$html .= "<th width='20%'>{$Lang['PowerPortfolio']['Management']['InputScore']['StudentName']}</th>";
			$i = 0 ;
			foreach($availableRubricAry as $availableRubric){
				$html .= "<th>{$availableRubric['Code']} {$availableRubric['Name']}";
				if($isDuringPeriod){
					$html .= "<br/>";
					$html .= getSelectByArray($gradeSelection, 'id="AP_'.$i.'"', $gradeNum, 0, true);
					$html .= $indexVar['libpowerportfolio_ui']->Get_Apply_All_Icon('javascript:js_Apply_All('.$i.')', '', 'style="float:none;display:inline-block;"');
				}
				$html .= "</th>";
				$i++;
			}
			unset($i);
		$html .= "</tr>";
	$html.= "</thead>";
	$html.= "<tbody>";
	$i = 0;
	foreach($StudentAry as $student){
		$_thisClassNumber = $student['ClassNumber'];
		$_thisClassName = $student['ClassName'];
		$_thisStudentID = $student['UserID'];
		$_thisStudentName = $student['StudentName'];
		$html .= "<tr class='dataRow'>";
			$html .= "<td>{$_thisClassName}-{$_thisClassNumber}</td>";
			$html .= "<td>{$_thisStudentName}</td>";
			$j = 0;
			foreach($codeAry as $code){
				$score = $studentScoreAry[$_thisStudentID][$code] ? $studentScoreAry[$_thisStudentID][$code] : '0';
				$_fieldName = "score[$_thisStudentID][$code]";
// 				$score_tag = 'id="mark['.$i.']['.$j.']" name="'.$_fieldName.'"  onchange="jCHANGE_STATUS()" onpaste="isPasteContent(event, '.$j.', '.$i.')" onkeyup="isPasteContent(event, '.$j.', '.($i).')" onkeydown="isPasteContent(event, '.$j.', '.($i).')" onfocusout="jCHANGE_STATUS()" class="tabletexttoptext textboxnum"';
// 				$score_tag = 'id="mark['.$i.']['.$j.']" name="'.$_fieldName.'"  class="tabletexttoptext textboxnum"';
				
				if($isDuringPeriod){
					$_inputBox = getSelectByArray($gradeSelection, 'alt-i="'.$j.'" id="mark['.$i.']['.$j.']" name="'.$_fieldName.'"', $score, 0, true);
				} else {
					$_inputBox = ( $score != 'N.A.') ? ( $score != 0 ? $score : '--')   : $Lang['PowerPortfolio']['Management']['InputScore']['N.A.'];
				}
// 				$_inputBox = getSelectByArray($gradeSelection, 'id="mark['.$i.']['.$j.']" name="'.$_fieldName.'"', $score, 0, true);
// 				$html .= "<td><input type='text' value='{$score}' {$score_tag} /></td>";
				$html .= "<td>{$_inputBox}</td>";
				$j++;
			}
		$html .= "<tr>";
		$i++;
	}
	$html.= "</tbody>";
$html .= "</table>";

$htmlAry['InputScoreTable'] = $html;


$html = "";
if($isDuringPeriod){
	$html .= $indexVar['libpowerportfolio_ui']->GET_ACTION_BTN($Lang['Btn']['Submit'], 'button', $ParOnClick="submitForm()");
}
$html .= $indexVar['libpowerportfolio_ui']->GET_ACTION_BTN($Lang['Btn']['Back'], 'button', $ParOnClick="goBack()") . '&nbsp;';
	
$htmlAry['ActionBtn'] = $html;

$htmlAry['hiddenField'] .= "<input type='hidden' name='TopicSettingID' value='{$TopicSettingID}'>";
$htmlAry['hiddenField'] .= "<input type='hidden' name='RubricSettingID' value='{$rubricSettingID}'>";
$htmlAry['hiddenField'] .= "<input type='hidden' name='YearClassID' value='{$YearClassID}'>";
?>