<?php
// Using: 
/*
 */
$topicSettingID = $_POST['TopicSettingID'];
$yearClassID = $_POST['YearClassID'];
$rubricSettingID = $_POST['RubricSettingID'];
$scoreAry = $_POST['score'];
$dataAry = array();
$result = array();
if(!empty($scoreAry)){
	#############
	# 1. Clear Old Record
	#############
	$studentIDAry = array_keys($scoreAry);
	$codeAry = array_keys($scoreAry[$studentIDAry[0]]);
	$result['delete'] = $indexVar['libpowerportfolio']->Delete_Student_Score($topicSettingID, $studentIDAry, $codeAry);
	foreach($scoreAry as $studentID => $studentScoreAry){
		foreach($studentScoreAry as $rubricCode => $_score){
			$row = array(
					'StudentID' => $studentID,
					'TopicSettingID' => $topicSettingID,
					'RubricCode' => $rubricCode,
			);
			if($_score == 'N.A.'){
				$row['isNA'] = '1';
				$row['Score'] = '0';
			} else {
				$row['isNA'] = '0';
				$row['Score'] = $_score;
			}
			$dataAry[] = $row;
		}
	}
	$result['Insert'] = $indexVar['libpowerportfolio']->Insert_Student_Score($dataAry);
}
header('location: index.php?task=mgmt.input_score.list_stu&TopicSettingID='.$topicSettingID.'&YearClassID='.$yearClassID);
?>