<?php
// editing by 
/*
 * 
 */

// Include DB Table
include_once($indexVar['thisBasePath']."includes/libdbtable.php");
include_once($indexVar['thisBasePath']."includes/libdbtable2007a.php");

// Update Result Message
if($success){
	$returnMsg = $Lang['General']['ReturnMessage']['UpdateSuccess'];
}
else if($success==="0"){
	$returnMsg = $Lang['General']['ReturnMessage']['UpdateUnsuccess'];
}

// Page Title
$TAGS_OBJ = array(
		array($Lang['PowerPortfolio']['Management']['InputScore']['AccordingClass'],"javascript: void(0);", true),
		array($Lang['PowerPortfolio']['Management']['InputScore']['AccordingStudent'], 'index.php?task=mgmt.input_score.index_stu')
);
$indexVar['libpowerportfolio_ui']->Echo_Module_Layout_Start($returnMsg);

// DB Table Settings

if($indexVar['libpowerportfolio']->IS_KG_CLASS_TEACHER() && !$indexVar['libpowerportfolio']->IS_POWER_PORTFOLIO_ADMIN_USER()){
	$TeachingClassAry = $indexVar['libpowerportfolio']->Get_Teaching_Class($_SESSION['UserID']);
	$TeachingClassIDAry = Get_Array_By_Key($TeachingClassAry, 'YearClassID');
} else {
	$TeachingClassIDAry = array();
}

$YearClassID = $_REQUEST['YearClassID'];
$TopicSettingID = $_REQUEST['TopicSettingID'];
$TopicSettingInfo = $indexVar['libpowerportfolio']->Get_Rubric_Topic_Setting($TopicSettingID);
$TopicSettingInfo = $TopicSettingInfo[0];
$rubricSettingID = $TopicSettingInfo['RubricSettingID'];
$targetYearID = $TopicSettingInfo['YearID'];

$rubricSettingInfo = $indexVar['libpowerportfolio']->Get_Rubric_Setting($rubricSettingID);
$maxLevel = $rubricSettingInfo[0]['LevelNum'];
$rubricIndexValueAry = $indexVar['libpowerportfolio']->Get_Rubric_Ability_Index_Item($rubricSettingID);
$valueList = $TopicSettingInfo['RubricList'];
$valueListAry = explode(',', $valueList);

$availableUpperCatAry = array();
foreach($rubricIndexValueAry as $rubricIndexValue){
	$_thisCode = $rubricIndexValue['Code'];
	if(in_array($_thisCode, $valueListAry)){
		$availableUpperCatAry[] = $rubricIndexValue['UpperCat'];
	}
}
$availableUpperCatAry = array_unique($availableUpperCatAry);

$tickImage = $indexVar['libpowerportfolio_ui']->Get_Tick_Image();

$pages_arr = array();
$pages_arr[] = array($Lang['PowerPortfolio']['Management']['InputScore']['Topic'], 'index.php?task=mgmt.input_score.index_class&YearClassID='.$YearClassID);
$pages_arr[] = array($TopicSettingInfo['Name'], '');

$htmlAry['selectionBox'] = $indexVar['libpowerportfolio_ui']->Get_Class_Selection($YearClassID, $onChange='document.form1.submit()', $withYearOptGroup=true, $noFirstTitle=false, $targetYearID, 'YearClassID',false, $TeachingClassIDAry);

$htmlAry['navBar'] = $indexVar['libpowerportfolio_ui']->GET_NAVIGATION_IP25($pages_arr);

$html = '<table class="common_table_list_v30 ">';
	$html .= "<thead>";
		$html .= "<tr class='tabletop'>";
			$html .= "<th class='tabletop tabletopnolink' width='10%'>#</th>";
			$html .= "<th class='tabletop tabletopnolink' width='60%'>{$Lang['PowerPortfolio']['Management']['InputScore']['Rubric']}</th>";
			$html .= "<th class='tabletop tabletopnolink' width='30%'>{$Lang['PowerPortfolio']['Management']['InputScore']['InputStatus']}</th>";
		$html .= "</tr>";
	$html .= "</thead>";
	$html .= "<tbody>";
	foreach($rubricIndexValueAry as $rubricIndexValue){
		// RubricSettingID,Code,Name,Type,Level,UpperCat
		$_thisCode = $rubricIndexValue['Code'];
		$_thisName = $rubricIndexValue['Name'];
		$_thisLevel = $rubricIndexValue['Level'];
		
		if($_thisLevel != $maxLevel-1){
			$html .= "<tr style='font-weight: bold'>";
				$html .= "<td class='tableContent'>{$_thisCode}</td>";
				if(in_array($_thisCode, $availableUpperCatAry)){
					$html .= "<td class='tableContent'><a href='index.php?task=mgmt.input_score.edit_class&YearClassID={$YearClassID}&TopicSettingID={$TopicSettingID}&RubricCode={$_thisCode}'>{$_thisName}</a></td>";
				} else {
					$html .= "<td class='tableContent'>{$_thisName}</td>";
				}
				$html .= "<td class='tableContent'></td>";
			$html .= "</tr>";
		} else {
			if(in_array($_thisCode,$valueListAry)){
				$status = $indexVar['libpowerportfolio']->Check_Class_Student_Score_Status($TopicSettingID, $YearClassID, $_thisCode);
				if($status){
					$_img = $tickImage;
				} else{
					$_img = '';
				}
				$html .= "<tr>";
					$html .= "<td class='tableContent'>&nbsp;&nbsp;{$_thisCode}</td>";
// 					$html .= "<td class='tableContent'><a href='index.php?task=mgmt.input_score.edit_class&YearClassID={$YearClassID}&TopicSettingID={$TopicSettingID}&RubricCode={$_thisCode}'>{$_thisName}</a></td>";
					$html .= "<td class='tableContent'>&nbsp;&nbsp;{$_thisName}</td>";
					$html .= "<td class='tableContent'>{$_img}</td>";
				$html .= "</tr>";
			}
		}
	}
	$html .= "</tbody>";
$html .= "</table>";

$htmlAry['RubricTable'] = $html;

$htmlAry['hiddenField'] .= "<input type='hidden' name='task' value='mgmt.input_score.list_class'>";
?>