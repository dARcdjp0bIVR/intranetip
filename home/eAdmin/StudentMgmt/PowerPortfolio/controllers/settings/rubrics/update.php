<?php
// Using: 
/*
 */
$gradeNumContent = $_POST['GradeNumContent'];
$gradeNumColor = $_POST['GradeNumColor'];
$examGradeNumContent = $_POST['ExamGradeNumContent'];
$examGradeNumColor = $_POST['ExamGradeNumColor'];
$colAry = array(
		'Name' => 'Name',
		'YearID' => 'YearID',
		'Levels' => 'LevelNum',
		'GradeNum' => 'GradeNum',
		'ExamGradeNum' => 'ExamGradeNum'
);
$dataAry = array();
foreach($colAry as $key => $col){
	$dataAry[$col] = $_POST[$key];
}

if($isDelete){
	foreach((array)$SettingID as $sid){
		$indexVar['libpowerportfolio']->Delete_Rubric_Setting($sid);
		$indexVar['libpowerportfolio']->Delete_Rubric_Setting_Item($sid);
		$indexVar['libpowerportfolio']->Delete_Ability_Index_Item($sid);
	}
} else {
	if($SettingID != ''){
		// edit
		$indexVar['libpowerportfolio']->Update_Rubric_Setting($SettingID, $dataAry);
		$indexVar['libpowerportfolio']->Delete_Rubric_Setting_Item($SettingID);
		$indexVar['libpowerportfolio']->Delete_Ability_Index_Item($SettingID);
	// 	$indexVar['libpowerportfolio']->Delete_Rubric_Ability_Index_Mapping($SettingID);
	} else {
		// new
		$SettingID = $indexVar['libpowerportfolio']->Insert_Rubric_Setting($dataAry);
	}
	$contentAry = array();
	foreach($gradeNumContent as $index => $row){
		$contentAry[] = array(
				'Type' => 'GradeNumContent',
				'Level' => $index,
				'Content' => $row
		);
	}
	foreach($gradeNumColor as $index => $row){
		$contentAry[] = array(
				'Type' => 'GradeNumColor',
				'Level' => $index,
				'Content' => $row
		);
	}
	foreach($examGradeNumContent as $index => $row){
		$contentAry[] = array(
				'Type' => 'ExamGradeNumContent',
				'Level' => $index,
				'Content' => $row
		);
	}
	foreach($examGradeNumColor as $index => $row){
		$contentAry[] = array(
				'Type' => 'ExamGradeNumColor',
				'Level' => $index,
				'Content' => $row
		);
	}
	$indexVar['libpowerportfolio']->Insert_Rubric_Setting_Item($SettingID, $contentAry);
	$itemAry = array();
	$rubricValueAry = $indexVar['JSON']->decode($_POST['RubricIndexValueJSON']);
	foreach($rubricValueAry as $lv1){
		if($lv1==null) continue;
		
		$itemAry[] = array(
			'Code' => $lv1['code'],
			'Name' => $lv1['value'],
			'Type' => 'RubricIndex',
			'Level' => '0',
			'UpperCat' => ''
		);
		foreach($lv1['child'] as $lv2){
			if($lv2==null) continue;
			
			$itemAry[] = array(
					'Code' => $lv2['code'],
					'Name' => $lv2['value'],
					'Type' => 'RubricIndex',
					'Level' => '1',
					'UpperCat' => $lv1['code']
			);
			foreach($lv2['child'] as $lv3){
				if($lv3==null) continue;
				
				$itemAry[] = array(
						'Code' => $lv3['code'],
						'Name' => $lv3['value'],
						'Type' => 'RubricIndex',
						'Level' => '2',
						'UpperCat' => $lv2['code']
				);
			}
		}
	}
	$indexVar['libpowerportfolio']->Upsert_Rubric_Ability_Index_Item($SettingID,$itemAry);
}
header('location: index.php?task=settings.rubrics.list');
?>