<?php 
$Action = $_REQUEST['Action'];
switch($Action){
	case 'DisplayGradeDetails':
		$gradeNum = $_REQUEST['GradeNum'];
		$settingID = $_REQUEST['SettingID'];
		$html = $indexVar['libpowerportfolio_ui']->Get_Grade_Details_Div($gradeNum, $settingID);
		echo $html;
		break;
	case 'DisplayExamGradeDetails':
		$gradeNum = $_REQUEST['ExamGradeNum'];
		$settingID = $_REQUEST['SettingID'];
		$html = $indexVar['libpowerportfolio_ui']->Get_Exam_Grade_Details_Div($ExamGradeNum, $settingID);
		echo $html;
		break;
	case 'GetRubricIndexTable':
		$maxLevel = $_REQUEST['MaxLevel'];
		$settingID = $_REQUEST['SettingID'];
		$html = $indexVar['libpowerportfolio_ui']->Get_Rubric_Index_Setting_Table($settingID, false, '', $maxLevel);
		echo $html;
		break;
	case 'EditRubricIndex':
		$value = $_REQUEST['Value'];
		$code = $_REQUEST['Code'];
		$html = $indexVar['libpowerportfolio_ui']->Get_Rubric_Index_Edit_Box($code, $value);
		echo $html;
		break;
	case 'AddRubricIndexRow':
		$maxLevel = $_REQUEST['MaxLevel'];
		$level = $_REQUEST['Level'];
		$maj = $_REQUEST['Major'];
		$sub = $_REQUEST['Minor'];
		$code = $_REQUEST['Code'];
		$html = $indexVar['libpowerportfolio_ui']->Get_Rubric_Index_Setting_Row($level,$maj,$sub,$code, '', true, false, $maxLevel);
		echo $html;
		break;
	case 'GetRubricIndexAddRow':
		$level = $_REQUEST['Level'];
		$maj = $_REQUEST['Major'];
		$sub = $_REQUEST['Minor'];
		$html = $indexVar['libpowerportfolio_ui']->Get_Rubric_Index_Setting_AddRow($level,$maj,$sub);
		echo $html;
		break;
	case 'GetImportReference':
		$targetType = $_REQUEST['Target'];
		$RemarkDetailsArr = array();
		switch($targetType){
			case 'Name':
				$title = '<th>';
				$title .= $Lang['PowerPortfolio']['Settings']['Rubrics']['Name'];
				$title .= '</th>';
				$title .= '<th>';
				$title .= $Lang['PowerPortfolio']['Settings']['Rubrics']['Levels'];
				$title .= '</th>';
				$rubricSettingAry = $indexVar['libpowerportfolio']->Get_Rubric_Setting();
				foreach($rubricSettingAry as $rubricSetting){
					$indexItemInfo = $indexVar['libpowerportfolio']->Get_Rubric_Ability_Index_Item($rubricSetting['RubricSettingID']);
					if(sizeof($indexItemInfo)>1) continue;
					$RemarkDetailsArr[] = '<td>'.$rubricSetting['Name'].'</td><td>'.$rubricSetting['LevelNum'].'</td>';
				}
				if(empty($RemarkDetailsArr)){
					$RemarkDetailsArr[] = '<td colspan="2" style="text-align:center">'.$i_no_record_exists_msg.'</td>';
				}
				break;
			default:
				break;
		}
		$html .= '';
		$thisRemarksType = 'type';
		$html .='<div class = "stickyHeader" id= "stickyRemarks" style = "background-color: #f3f3f3;position: sticky; top: 0; margin-bottom: 1px;">';
		$html .= '<div style="display: inline;">';
		$html .= '&nbsp';
		$html .= '</div>';
		$html .= '<div style="display: inline; float: right; padding-right: 2px;">';
		$html .= '<a href="javascript:hideRemarkLayer(\''.$thisRemarksType.'\');" ><img border="0" src="'.$PATH_WRT_ROOT.$image_path.'/'.$LAYOUT_SKIN.'/ecomm/btn_mini_off.gif" style="float:right"></a>'."\r\n";
		$html .= '</div>';
		$html .= '</div>';
		$html .= '<div id="tableDiv" style="overflow-y: auto; max-height: 250px;">';
		$html .= '<table cellspacing="0" cellpadding="3" border="0" width="100%">'."\r\n";
		$html .= '<tbody>'."\r\n";
		$html .= '<tr>'."\r\n";
		$html .= '<td align="left" style="border-bottom: medium none;">'."\r\n";
		$html .= '<table class="common_table_list_v30 view_table_list_v30">'."\r\n";
		$html .= '<thead>'."\n";
		$html .= '<tr>'."\n";
		$html .= $title."\n";
		$html .= '</tr>'."\n";
		$html .= '</thead>'."\n";
		$html .= '<tbody>'."\n";
		foreach ($RemarkDetailsArr as $RemarkDetail){
			$html .= '<tr>'."\n";
			$html .= $RemarkDetail."\n";
			$html .= '</tr>'."\n";
		}
		$html .= '</tbody>'."\n";
		$html .= '</table>'."\r\n";
		$html .= '</td>'."\r\n";
		$html .= '</tr>'."\r\n";
		$html .= '</tbody>'."\r\n";
		$html .= '</table>'."\r\n";
		$html .= '</div>';
		
		echo $html;
		break;
	case 'GetImportSample':
		include_once($PATH_WRT_ROOT.'includes/libexporttext.php');
		$libexport = new libexporttext();
		$fileName = "Import_Sample_RubricIndex.csv";
		$ColumnDef = array(
				$Lang['PowerPortfolio']['Settings']['Rubrics']['Title'],
				$Lang['PowerPortfolio']['Settings']['Rubrics']['Category'],
				$Lang['PowerPortfolio']['Settings']['Rubrics']['SubCategory'],
				$Lang['PowerPortfolio']['Settings']['Rubrics']['Item']
		);
		$data = array();
		$data[] = array('[Rubric Setting Name]',
				'[Category Name]',
				'[SubCategory Name]',
				'[Item Name]'
		);
        $data[] = array('評量指標 (甲)',
                '藝術與設計',
                '繪畫',
                '能夠自由創作繪畫'
        );
		
		$content = $libexport->GET_EXPORT_TXT($data, $ColumnDef);
		$libexport->EXPORT_FILE($fileName, $content);
		break;
	default:
		break;
}

?>