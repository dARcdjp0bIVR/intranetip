<?php
// Using: 
/*
 */

$TAGS_OBJ[] = array($Lang["PowerPortfolio"]["Settings"]["Rubrics"]["Title"]);
$indexVar["libpowerportfolio_ui"]->Echo_Module_Layout_Start($indexVar["returnMsgLang"]);
if($isEdit)
{
	$RubricSetting = $indexVar['libpowerportfolio']->Get_Rubric_Setting($SettingID);
	if(!empty($RubricSetting)) $RubricSetting = $RubricSetting[0];
	$name = $RubricSetting['Name'];
	$yearID = $RubricSetting['YearID'];
	$levelNum = $RubricSetting['LevelNum'];
	$gradeNum = $RubricSetting['GradeNum'];
	$examGradeNum = $RubricSetting['ExamGradeNum'];
	$navigation = $Lang["Btn"]["Edit"];
}
else
{
	$navigation = $Lang["Btn"]["New"];
}

### Loading Image [Start]
$LoadingImage = "";
$LoadingImage = "<img class='LoadingImg' src='/images/$LAYOUT_SKIN/indicator.gif'>";
### Loading Image [End]

# Year Selection
$YearSelection = $indexVar["libpowerportfolio_ui"]->Get_Year_Selection($yearID, "YearOnChange()");

if($isEdit) {
    $LevelSelection = str_replace('<!--level_num-->', $levelNum, $Lang['PowerPortfolio']['General']['Level']);
    $LevelSelection .= "<input type='hidden' name='Levels' id='Levels' value='$levelNum' />";
} else {
	$LevelSelection = $indexVar['libpowerportfolio_ui']->Get_Level_Selection('', 'LevelOnChange()');
}

$GradeNumSelection = $indexVar["libpowerportfolio_ui"]->Get_Grade_Num_Selection($gradeNum, 'GradeNumOnChange()');
// $GradeDetailsDev = $indexVar["libpowerportfolio_ui"]->Get_Grade_Details_Div($gradeNum);

$ExamGradeNumSelection = $indexVar["libpowerportfolio_ui"]->Get_Exam_Grade_Num_Selection($examGradeNum, 'ExamGradeNumOnChange()');
$ExamGradeDetailsDev = $indexVar["libpowerportfolio_ui"]->Get_Exam_Grade_Details_Div($gradeNum);

$newItemLang = $Lang['PowerPortfolio']['Settings']['Rubrics']['PleaseInputName'];

// # Start Date
// $DateStart = $indexVar["libpowerportfolio_ui"]->GET_DATE_PICKER("DateStart")." ".$indexVar["libpowerportfolio_ui"]->Get_Time_Selection("DateStart");
// $DateStart .= $indexVar["libpowerportfolio_ui"]->Get_Form_Warning_Msg("marksheetSubmission_inputDateWarnDiv", $Lang["General"]["WarningArr"]["InputDate"], "warningMsgDiv", $display=false);
// $DateStart .= $indexVar["libpowerportfolio_ui"]->Get_Form_Warning_Msg("marksheetSubmission_endDateEarlierWarnDiv", $Lang["General"]["WarningArr"]["EndDateCannotEarlierThanStartDate"], "warningMsgDiv", $display=false);

// # End Date
// $DateEnd = $indexVar["libpowerportfolio_ui"]->GET_DATE_PICKER("DateEnd")." ".$indexVar["libpowerportfolio_ui"]->Get_Time_Selection("DateEnd");
// $DateEnd .= $indexVar["libpowerportfolio_ui"]->Get_Form_Warning_Msg("marksheetSubmission_inputDateWarnDiv", $Lang["General"]["WarningArr"]["InputDate"], "warningMsgDiv", $display=false);
// $DateEnd .= $indexVar["libpowerportfolio_ui"]->Get_Form_Warning_Msg("marksheetSubmission_endDateEarlierWarnDiv", $Lang["General"]["WarningArr"]["EndDateCannotEarlierThanStartDate"], "warningMsgDiv", $display=false);

# Navigation
$navigationAry[] = array($Lang["eReportCardKG"]["Management"]["TopicTimeTable"]["Title"], "index.php?task=settingrs.rubrics.list");
$navigationAry[] = array($navigation);
$NivagationBar = $indexVar["libpowerportfolio_ui"]->GET_NAVIGATION_IP25($navigationAry);

### Warning box
/*
$WarningBox = $indexVar["libpowerportfolio_ui"]->Get_Warning_Message_Box('<font color="red">'.$Lang['General']['Tips'].'</font>', $Lang['eReportCardKG']['Management']['TopicTimeTable']['PleaseConfirmChanges']);
$WarningBox_Static = $indexVar["libpowerportfolio_ui"]->Get_Warning_Message_Box('<font color="red">'.$Lang['General']['Tips'].'</font>', $Lang['eReportCardKG']['General']['PleaseConfirmChanges']);
*/
$WarningBox_Static = $indexVar["libpowerportfolio_ui"]->Get_Warning_Message_Box('<font color="red">'.$Lang['General']['Tips'].'</font>', $Lang['PowerPortfolio']['Settings']['Rubrics']['RubricIndexHint']);

### Required JS CSS - ThickBox
echo $indexVar["libpowerportfolio_ui"]->Include_Thickbox_JS_CSS();
?>