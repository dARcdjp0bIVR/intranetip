<?php
$STEPS_OBJ[] = array($i_general_select_csv_file, 1);
$STEPS_OBJ[] = array($i_general_confirm_import_data, 0);
$STEPS_OBJ[] = array($i_general_imported_result, 0);
$TAGS_OBJ[] = array($Lang['PowerPortfolio']['Settings']['Rubrics']['SubTitle']);
$PAGE_NAVIGATION[] = array($Lang['Btn']['Import']." ".$Lang['PowerPortfolio']['Settings']['Rubrics']['Title'], "");

$indexVar['libpowerportfolio_ui']->Echo_Module_Layout_Start($returnMsg);

$ColumnTitleArr = array();
$ColumnTitleArr[] = $Lang['PowerPortfolio']['Settings']['Rubrics']['Title'];
$ColumnTitleArr[] = $Lang['PowerPortfolio']['Settings']['Rubrics']['Category'];
$ColumnTitleArr[] = $Lang['PowerPortfolio']['Settings']['Rubrics']['SubCategory'];
$ColumnTitleArr[] = $Lang['PowerPortfolio']['Settings']['Rubrics']['Item'];

$ColumnPropertyArr = array(1,1,1,1);
$RemarksArr = array();

$RemarksArr[0] = " [<a href='javascript:Load_Reference(\"Name\")' class='tablelink' id='importRef_Name'>".$Lang['PowerPortfolio']['Settings']['Rubrics']['ImportReference']['Title']."</a>]";
$RemarksArr[2] = $Lang['PowerPortfolio']['Settings']['Rubrics']['ImportReference']['SubCatRemark'];

$format_str.= $indexVar['libpowerportfolio_ui']->Get_Import_Page_Column_Display($ColumnTitleArr, $ColumnPropertyArr, $RemarksArr);

$sample_csv = "<a class=\"tablelink\" href=\"index.php?task=settings.rubrics.ajax&Action=GetImportSample\">[". $i_general_clickheredownloadsample ."]</a><br>";

$Navigation = $indexVar['libpowerportfolio_ui']->GET_NAVIGATION($PAGE_NAVIGATION);
$pageStep = $indexVar['libpowerportfolio_ui']->GET_STEPS($STEPS_OBJ);
$btnSubmit = $indexVar['libpowerportfolio_ui']->GET_ACTION_BTN($button_submit, "submit", "");
$btnCancel = $indexVar['libpowerportfolio_ui']->GET_ACTION_BTN($button_cancel, "button", "window.location.href = 'index.php?task=settings.rubrics.list'");
?>