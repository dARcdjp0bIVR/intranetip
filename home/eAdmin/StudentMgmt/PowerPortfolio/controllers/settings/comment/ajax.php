<?php 
$Action = $_REQUEST['Action'];
switch($Action){
	case 'GetImportReference':
		$targetType = $_REQUEST['Target'];
		$RemarkDetailsArr = array();
		switch($targetType){
			case 'CategoryName':
				$title .= '<th>';
				$title .= $Lang['PowerPortfolio']['Settings']['Comment']['CategoryName'];
				$title .= '</th>';
				$CatAry = $indexVar['libpowerportfolio']->Get_Comment('','',false,true);
				foreach($CatAry as $cat){
					$RemarkDetailsArr[] = '<td>'.$cat['Content'].'</td>';
				}
				if(empty($RemarkDetailsArr)){
					$RemarkDetailsArr[] = '<td style="text-align:center">'.$i_no_record_exists_msg.'</td>';
				}
				break;
			default:
				break;
		}
		$html .= '';
		$thisRemarksType = 'type';
		$html .='<div class = "stickyHeader" id= "stickyRemarks" style = "background-color: #f3f3f3;position: sticky; top: 0; margin-bottom: 1px;">';
		$html .= '<div style="display: inline;">';
		$html .= '&nbsp';
		$html .= '</div>';
		$html .= '<div style="display: inline; float: right; padding-right: 2px;">';
		$html .= '<a href="javascript:hideRemarkLayer(\''.$thisRemarksType.'\');" ><img border="0" src="'.$PATH_WRT_ROOT.$image_path.'/'.$LAYOUT_SKIN.'/ecomm/btn_mini_off.gif" style="float:right"></a>'."\r\n";
		$html .= '</div>';
		$html .= '</div>';
		$html .= '<div id="tableDiv" style="overflow-y: auto; max-height: 250px;">';
		$html .= '<table cellspacing="0" cellpadding="3" border="0" width="100%">'."\r\n";
		$html .= '<tbody>'."\r\n";
		$html .= '<tr>'."\r\n";
		$html .= '<td align="left" style="border-bottom: medium none;">'."\r\n";
		$html .= '<table class="common_table_list_v30 view_table_list_v30">'."\r\n";
		$html .= '<thead>'."\n";
		$html .= '<tr>'."\n";
		$html .= $title."\n";
		$html .= '</tr>'."\n";
		$html .= '</thead>'."\n";
		$html .= '<tbody>'."\n";
		foreach ($RemarkDetailsArr as $RemarkDetail){
			$html .= '<tr>'."\n";
			$html .= $RemarkDetail."\n";
			$html .= '</tr>'."\n";
		}
		$html .= '</tbody>'."\n";
		$html .= '</table>'."\r\n";
		$html .= '</td>'."\r\n";
		$html .= '</tr>'."\r\n";
		$html .= '</tbody>'."\r\n";
		$html .= '</table>'."\r\n";
		$html .= '</div>';
		
		echo $html;
		break;
	case 'GetImportSample':
		include_once($PATH_WRT_ROOT.'includes/libexporttext.php');
		$libexport = new libexporttext();
		$fileName = "Import_Sample_Comment.csv";
		$ColumnDef = array(
				$Lang['PowerPortfolio']['Settings']['Comment']['Category'],
				$Lang['PowerPortfolio']['Settings']['Comment']['Content']
		);
		$data = array();
		$data[] = array('Category Example 1','Comment Example 1');
		
		$content = $libexport->GET_EXPORT_TXT($data, $ColumnDef);
		$libexport->EXPORT_FILE($fileName, $content);
		break;
	case 'EditComment':
		$commentID = $_REQUEST['CommentID'];
		$isCat = $_REQUEST['IsCat'];
		$html = $indexVar['libpowerportfolio_ui']->Get_Comment_Edit_Box($commentID, $isCat);
		echo $html;
		break;
	default:
		break;
}
?>