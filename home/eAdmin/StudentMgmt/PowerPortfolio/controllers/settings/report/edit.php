<?php
// Using: 
/*
 *  Date: 2018-05-02 (Bill)
 *          Added Warning Box
 *  
 *  Date: 2018-03-22 (Bill)     [2018-0202-1046-39164]
 *          Support Class Zone Quota Settings
 *  
 * 	Date: 2017-12-20 (Bill)
 * 			Set Default End Time to "23:59:59"
 */
 
$TAGS_OBJ[] = array($Lang['PowerPortfolio']['Settings']['Report']['Title']);
$indexVar["libpowerportfolio_ui"]->Echo_Module_Layout_Start($indexVar["returnMsgLang"]);

if($isEdit)
{
	$navigation = $Lang["Btn"]["Edit"];

	$ReportSetting = $indexVar['libpowerportfolio']->Get_Report_Setting($SettingID);
	if(!empty($ReportSetting)) $ReportSetting = $ReportSetting[0];

    $ReportID = $ReportSetting['ReportID'];
	$Name = $ReportSetting['Name'];
	$YearID = $ReportSetting['YearID'];
	$StartDate = $ReportSetting['StartDate'];
	$EndDate = $ReportSetting['EndDate'];
	$CoverIncludeInfo = $ReportSetting['CoverIncludeInfo'];
	$CoverTitle = $ReportSetting['CoverTitle'];
	$CoverSubTitle = $ReportSetting['CoverSubTitle'];
	$CAReportTitle = $ReportSetting['CAReportTitle'];
	$TopicID = $ReportSetting['TopicID'];
    if(!empty($TopicID)) $TopicID = explode(',', $TopicID);
	$ActivityID = $ReportSetting['ActivityID'];
    if(!empty($ActivityID)) $ActivityID = explode(',', $ActivityID);
	$TermAssessmentIncluded = $ReportSetting['TermAssessmentIncluded'];
	$TermAssessmentTitle = $ReportSetting['TermAssessmentTitle'];
	$TermCommentIncluded = $ReportSetting['TermCommentIncluded'];
	$TermCommentTitle = $ReportSetting['TermCommentTitle'];
	$PageLastPageIncludeInfo = $ReportSetting['PageLastPageIncludeInfo'];

}
else
{
	$navigation = $Lang["Btn"]["New"];
}

### Loading Image [Start]
$LoadingImage = "";
if($isEdit) {
	$LoadingImage = "<img class='LoadingImg' src='/images/$LAYOUT_SKIN/indicator.gif'>";
}
### Loading Image [End]

# Year Selection
$YearSelection = $indexVar["libpowerportfolio_ui"]->Get_Year_Selection($YearID, "YearOnChange()", false, true, 0, false, true);

# Start Date
$InputDateStart = $indexVar["libpowerportfolio_ui"]->GET_DATE_PICKER("StartDate");
$Defined = 1;
$InputDateStart .= $indexVar["libpowerportfolio_ui"]->Get_Form_Warning_Msg("marksheetSubmission_inputDateWarnDiv", $Lang["General"]["WarningArr"]["InputDate"], "warningMsgDiv", $display=false);
$InputDateStart .= $indexVar["libpowerportfolio_ui"]->Get_Form_Warning_Msg("marksheetSubmission_endDateEarlierWarnDiv", $Lang["General"]["WarningArr"]["EndDateCannotEarlierThanStartDate"], "warningMsgDiv", $display=false);

# End Date
$InputDateEnd = $indexVar["libpowerportfolio_ui"]->GET_DATE_PICKER("EndDate");
$InputDateEnd .= $indexVar["libpowerportfolio_ui"]->Get_Form_Warning_Msg("marksheetSubmission_inputDateWarnDiv", $Lang["General"]["WarningArr"]["InputDate"], "warningMsgDiv", $display=false);
$InputDateEnd .= $indexVar["libpowerportfolio_ui"]->Get_Form_Warning_Msg("marksheetSubmission_endDateEarlierWarnDiv", $Lang["General"]["WarningArr"]["EndDateCannotEarlierThanStartDate"], "warningMsgDiv", $display=false);

# Navigation
$navigationAry[] = array($Lang['PowerPortfolio']['Settings']['Report']['Title'], "index.php?task=settings.report.list");
$navigationAry[] = array($navigation);
$NivagationBar = $indexVar["libpowerportfolio_ui"]->GET_NAVIGATION_IP25($navigationAry);

### Required JS CSS - ThickBox
echo $indexVar["libpowerportfolio_ui"]->Include_Thickbox_JS_CSS();
?>