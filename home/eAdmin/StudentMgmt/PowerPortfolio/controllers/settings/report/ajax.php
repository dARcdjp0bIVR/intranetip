<?php 
$Action = $_REQUEST['Action'];
if($ReportID) {
    $ReportSettings = $indexVar['libpowerportfolio']->Get_Report_Setting($ReportID);
    $ReportSettings = $ReportSettings[0];
}

switch($Action)
{
	case 'DisplayTopicSelect':
		$YearID = $_REQUEST['YearID'];
        $ReportID = $_REQUEST['ReportID'];

        $topics = array();
        if($YearID) {
		    $topics = $indexVar['libpowerportfolio']->Get_Rubric_Topics('', $YearID);
        }

        $selectArr = array();
        for ($i = 0; $i < count($topics); $i ++) {
            $thisTopicID = $topics[$i]['TopicSettingID'];
            $thisTopicName = $topics[$i]['Name'];
            $selectArr[] = array($thisTopicID, $thisTopicName);
        }

        $SelectedTopicID = array();
        if($ReportID && $YearID && $ReportSettings['YearID'] == $YearID) {
            $TopicID = $ReportSettings['TopicID'];
            if($TopicID != '') {
                $SelectedTopicID = explode(',', $TopicID);
            }
        }

        $ID_Name = 'TopicID[]';
        $onchange = '';
        $Multiple = ' multiple size=10 ';
        $selectionTags = ' id="'.$ID_Name.'" name="'.$ID_Name.'" '.$onchange.' '.$Multiple;
        //echo getSelectByAssoArray($selectArr, $selectionTags, $SelectedTopicID, $IsAll = 0, 1);
        echo getSelectByArray($selectArr,$selectionTags,$SelectedTopicID,$all=0,$noFirst=1);

		break;

	case 'DisplayActivitySelect':
        $YearID = $_REQUEST['YearID'];
        $ReportID = $_REQUEST['ReportID'];

        $activites = array();
        if($YearID) {
            $activites = $indexVar['libpowerportfolio']->Get_Activities('', $YearID);
        }

        $selectArr = array();
        for ($i = 0; $i < count($activites); $i ++) {
            $thisActivityID = $activites[$i]['ActivitySettingID'];
            $thisActivityName = $activites[$i]['ActivityName'];
            $selectArr[] = array($thisActivityID, $thisActivityName);
        }

        $SelectedActivityID = array();
        if($ReportID && $YearID && $ReportSettings['YearID'] == $YearID) {
            $ActivityID = $ReportSettings['ActivityID'];
            if($ActivityID != '') {
                $SelectedActivityID = explode(',', $ActivityID);
            }
        }

        $ID_Name = 'ActivityID[]';
        $onchange = '';
        $Multiple = ' multiple size=10 ';
        $selectionTags = ' id="'.$ID_Name.'" name="'.$ID_Name.'" '.$onchange.' '.$Multiple;
        //echo getSelectByAssoArray($selectArr, $selectionTags, $SelectedActivityID, $IsAll = 0, 1);
        echo getSelectByArray($selectArr,$selectionTags,$SelectedActivityID,$all=0,$noFirst=1);

        break;

	default:
		break;
}

?>