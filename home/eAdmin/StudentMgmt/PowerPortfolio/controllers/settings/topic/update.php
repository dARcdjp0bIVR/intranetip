<?php
// Using: 
/*
 */
$RubricIndexList = $_POST['RubricIndexList'];
$colAry = array(
	'Name' => 'Name',
	'YearID' => 'YearID',
	'RubricSettingID' => 'RubricSettingID',
	'ExamPeriodStart' => 'ExamPeriodStart',
	'ExamPeriodEnd' => 'ExamPeriodEnd',
	'MarkPeriodStart' => 'MarkPeriodStart',
	'MarkPeriodEnd' => 'MarkPeriodEnd',
	'RubricIndexList' => 'RubricList'
);
$dataAry = array();
foreach($colAry as $key => $col){
	$dataAry[$col] = $_POST[$key];
}
if($isDelete){
	foreach((array)$SettingID as $sid){
		$indexVar['libpowerportfolio']->Delete_Rubric_Topic_Setting($sid);
	}
} else {
	if($SettingID != ''){
		//edit
		$indexVar['libpowerportfolio']->Update_Rubric_Topic_Setting($SettingID, $dataAry);
	} else {
		//new
		$SettingID = $indexVar['libpowerportfolio']->Insert_Rubric_Topic_Setting($dataAry);
	}
}
header('location: index.php?task=settings.topic.list');