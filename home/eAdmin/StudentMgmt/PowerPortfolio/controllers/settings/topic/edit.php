<?php
// Using: 
/*
 *  Date: 2018-05-02 (Bill)
 *          Added Warning Box
 *  
 *  Date: 2018-03-22 (Bill)     [2018-0202-1046-39164]
 *          Support Class Zone Quota Settings
 *  
 * 	Date: 2017-12-20 (Bill)
 * 			Set Default End Time to "23:59:59"
 */
 
$TAGS_OBJ[] = array($Lang['PowerPortfolio']['Settings']['Topic']['Title']);
$indexVar["libpowerportfolio_ui"]->Echo_Module_Layout_Start($indexVar["returnMsgLang"]);

if($isEdit)
{
	$navigation = $Lang["Btn"]["Edit"];
	$TopicSetting = $indexVar['libpowerportfolio']->Get_Rubric_Topic_Setting($SettingID);
	if(!empty($TopicSetting)) $TopicSetting= $TopicSetting[0];
	$name = $TopicSetting['Name'];
	$yearID = $TopicSetting['YearID'];
	$rubricSettingID = $TopicSetting['RubricSettingID'];
	$ExamPeriodStart = $TopicSetting['ExamPeriodStart'];
	$ExamPeriodEnd = $TopicSetting['ExamPeriodEnd'];
	$MarkPeriodStart = $TopicSetting['MarkPeriodStart'];
	$MarkPeriodEnd = $TopicSetting['MarkPeriodEnd'];
}
else
{
	$navigation = $Lang["Btn"]["New"];
}

### Loading Image [Start]
$LoadingImage = "";
if($isEdit) {
	$LoadingImage = "<img class='LoadingImg' src='/images/$LAYOUT_SKIN/indicator.gif'>";
}
### Loading Image [End]

# Year Selection
$YearSelection = $indexVar["libpowerportfolio_ui"]->Get_Year_Selection($yearID, "YearOnChange()");

if($isEdit){
	$RubricSettingSelection = $indexVar['libpowerportfolio_ui']->Get_Rubric_Setting_Selection($rubricSettingID, "RubricSettingOnChange()", $yearID);
}

# Start Date
$AssessmentDateStart = $indexVar["libpowerportfolio_ui"]->GET_DATE_PICKER("ExamPeriodStart");
$Defined = 1;
$AssessmentDateStart .= $indexVar["libpowerportfolio_ui"]->Get_Form_Warning_Msg("marksheetSubmission_inputDateWarnDiv", $Lang["General"]["WarningArr"]["InputDate"], "warningMsgDiv", $display=false);
$AssessmentDateStart .= $indexVar["libpowerportfolio_ui"]->Get_Form_Warning_Msg("marksheetSubmission_endDateEarlierWarnDiv", $Lang["General"]["WarningArr"]["EndDateCannotEarlierThanStartDate"], "warningMsgDiv", $display=false);

# End Date
$AssessmentDateEnd = $indexVar["libpowerportfolio_ui"]->GET_DATE_PICKER("ExamPeriodEnd");
$AssessmentDateEnd .= $indexVar["libpowerportfolio_ui"]->Get_Form_Warning_Msg("marksheetSubmission_inputDateWarnDiv", $Lang["General"]["WarningArr"]["InputDate"], "warningMsgDiv", $display=false);
$AssessmentDateEnd .= $indexVar["libpowerportfolio_ui"]->Get_Form_Warning_Msg("marksheetSubmission_endDateEarlierWarnDiv", $Lang["General"]["WarningArr"]["EndDateCannotEarlierThanStartDate"], "warningMsgDiv", $display=false);


# Start Date
$InputDateStart = $indexVar["libpowerportfolio_ui"]->GET_DATE_PICKER("MarkPeriodStart");
$InputDateStart .= $indexVar["libpowerportfolio_ui"]->Get_Form_Warning_Msg("marksheetSubmission_inputDateWarnDiv", $Lang["General"]["WarningArr"]["InputDate"], "warningMsgDiv", $display=false);
$InputDateStart .= $indexVar["libpowerportfolio_ui"]->Get_Form_Warning_Msg("marksheetSubmission_endDateEarlierWarnDiv", $Lang["General"]["WarningArr"]["EndDateCannotEarlierThanStartDate"], "warningMsgDiv", $display=false);

# End Date
$InputDateEnd = $indexVar["libpowerportfolio_ui"]->GET_DATE_PICKER("MarkPeriodEnd");
$InputDateEnd .= $indexVar["libpowerportfolio_ui"]->Get_Form_Warning_Msg("marksheetSubmission_inputDateWarnDiv", $Lang["General"]["WarningArr"]["InputDate"], "warningMsgDiv", $display=false);
$InputDateEnd .= $indexVar["libpowerportfolio_ui"]->Get_Form_Warning_Msg("marksheetSubmission_endDateEarlierWarnDiv", $Lang["General"]["WarningArr"]["EndDateCannotEarlierThanStartDate"], "warningMsgDiv", $display=false);


# Navigation
$navigationAry[] = array($Lang['PowerPortfolio']['Settings']['Topic']['Title'], "index.php?task=settings.topic.list");
$navigationAry[] = array($navigation);
$NivagationBar = $indexVar["libpowerportfolio_ui"]->GET_NAVIGATION_IP25($navigationAry);

### Warning box
/*
$WarningBox = $indexVar["libpowerportfolio_ui"]->Get_Warning_Message_Box('<font color="red">'.$Lang['General']['Tips'].'</font>', $Lang['eReportCardKG']['Management']['TopicTimeTable']['PleaseConfirmChanges']);
$WarningBox_Static = $indexVar["libpowerportfolio_ui"]->Get_Warning_Message_Box('<font color="red">'.$Lang['General']['Tips'].'</font>', $Lang['eReportCardKG']['General']['PleaseConfirmChanges']);
*/

### Required JS CSS - ThickBox
echo $indexVar["libpowerportfolio_ui"]->Include_Thickbox_JS_CSS();
?>