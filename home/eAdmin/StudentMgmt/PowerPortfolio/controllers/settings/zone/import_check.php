<?php 
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

$limport = new libimporttext();
$lo = new libfilesystem();

$importFile = $_FILES['ImportFile'];
$filename = $importFile['name'];
$tmpFilePath = $importFile['tmp_name'];

$ext = strtoupper($lo->file_ext($filename));

$alertMsg = array();

if($ext != ".CSV")
{
	$alertMsg[] = array(0, $Lang['PowerPortfolio']['Setting']['ImportWarning']['CSVFormatError']);
}

if($limport->CHECK_FILE_EXT($filename)) {
	# read file into array
	# return 0 if fail, return csv array if success
	$data = $limport->GET_IMPORT_TXT($tmpFilePath);
}
# check column of import file
if(is_array($data))
{
	$col_name = array_shift($data);
}

if(sizeof($data)==0) {
	$alertMsg[] = array(0, $Lang['PowerPortfolio']['Setting']['ImportWarning']['EmptyContent']);
}

$current_tab = 1;
$STEPS_OBJ[] = array($i_general_select_csv_file, 0);
$STEPS_OBJ[] = array($i_general_confirm_import_data, 1);
$STEPS_OBJ[] = array($i_general_imported_result, 0);
$pageStep = $indexVar['libpowerportfolio_ui']->GET_STEPS($STEPS_OBJ);
$PAGE_NAVIGATION[] = array($Lang['Btn']['Import']." ".$Lang['PowerPortfolio']['Settings']['Zone']['Title'], "");

$Navigation = $indexVar['libpowerportfolio_ui']->GET_NAVIGATION($PAGE_NAVIGATION);

$indexVar['libpowerportfolio_ui']->Echo_Module_Layout_Start();

if(empty($alertMsg)){
	$topicSettingAry = $indexVar['libpowerportfolio']->Get_Rubric_Topics();
	$topicSettingIDAry = Get_Array_By_Key($topicSettingAry, "TopicSettingID");
	$rowNum = 1;
	foreach($data as $row){
		list($name, $topicSettingID, $quota, $pictureType) = $row;
		if($name==''){
			$alertMsg[] = array($rowNum, $Lang['PowerPortfolio']['Setting']['ImportWarning']['FieldEmpty']['Name']);
		}
		if($topicSettingID==''){
			$alertMsg[] = array($rowNum, $Lang['PowerPortfolio']['Setting']['ImportWarning']['FieldEmpty']['Topic']);
		}
		if(!in_array($topicSettingID,$topicSettingIDAry) ){
			$alertMsg[] = array($rowNum, $Lang['PowerPortfolio']['Setting']['ImportWarning']['ItemNotFound']['Topic']);
		}
		if($quota==''){
			$alertMsg[] = array($rowNum, $Lang['PowerPortfolio']['Setting']['ImportWarning']['FieldEmpty']['Quota']);
		}
		if(!($pictureType>=1 && $pictureType<6) ){
			$alertMsg[] = array($rowNum, $Lang['PowerPortfolio']['Setting']['ImportWarning']['ZonePicture'].'1 '.$Lang['General']['To'].' 5');
		}
		$rowNum++;
	}
}
if(empty($alertMsg)){
	$amount = sizeof($data);
	$htmlAry['Result'] = "<p>{$Lang['PowerPortfolio']['Setting']['ImportWarning']['RecordCount']}: " . $amount . "</p>";
	
	//buttons at the bottom
	$buttons = "";
	$buttons .= $indexVar['libpowerportfolio_ui']->GET_ACTION_BTN($Lang['Btn']['Submit'], "submit", "")."&nbsp;";
	$buttons .= $indexVar['libpowerportfolio_ui']->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "window.location.href = 'index.php?task=settings.zone.import'");
	$htmlAry['Btn'] = $buttons;
	
	$_SESSION['PowerportfolioImportData'] = $data;
	
} else {
	$amount = sizeof($alertMsg);
	$fontColorStart = "<font color='red'>";
	$fontColorEnd = "</font>";
	
	$errorTableHeader = "<thead>";
		$errorTableHeader .= "<th width='20%' class='tablebluetop tabletopnolink'>".$Lang['General']['ImportArr']['Row'].'</th>';
		$errorTableHeader .= "<th width='80%' class='tablebluetop tabletopnolink'>".$Lang['General']['Error'].'</th>';
	$errorTableHeader .= "</thead>";
	$errorTableBody = "<tbody>";
	foreach($alertMsg as $msg){
		list($index, $content) = $msg;
		$errorTableBody .= "<tr>";
		if($index == 0 ){
			$errorTableBody .= "<td></td><td class='tabletext' style=''>" . $fontColorStart. $content. $fontColorEnd."</td>";
		} else {
			$errorTableBody .= "<td class='tabletext'>" . $index . "</td>";
			$errorTableBody .= "<td class='tabletext' style=''>" . $fontColorStart. $content. $fontColorEnd."</td>";
		}
		$errorTableBody .= "</tr>";
	}
	$errorTableBody .= "</tbody>";
	
	$errorTable = "<table align='center' width='60%' border='0' cellpadding='5' cellspacing='0'>" . $errorTableHeader .$errorTableBody ."</table>";
	
	$htmlAry['Table'] = $errorTable;
	
	//buttons at the bottom
	$buttons = "";
	$buttons .= $indexVar['libpowerportfolio_ui']->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "window.location.href = 'index.php?task=settings.zone.import'");
	$htmlAry['Btn'] = $buttons;
	
	
	$htmlAry['Result'] = "<p>{$Lang['PowerPortfolio']['Setting']['ImportWarning']['ErrorCount']}: ". $fontColorStart. $amount . $fontColorEnd."</p>";
}

?>