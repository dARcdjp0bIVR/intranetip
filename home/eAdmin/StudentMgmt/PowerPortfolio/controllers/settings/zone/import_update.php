<?php 

// Not allow to access if data not existed
if(isset($_SESSION['PowerportfolioImportData'])){
	$data = $_SESSION['PowerportfolioImportData'];
	$result = array('Insert'=>0);
	foreach($data as $row){
		list($name, $topicSettingID, $quota, $pictureType) = $row;
		$contentAry = array(
				'Name' => $name,
				'TopicSettingID' => $topicSettingID,
				'Quota' => $quota, 
				'PictureType' => $pictureType
		);
		$zoneID = $indexVar['libpowerportfolio']->Insert_Zone_Setting($contentAry);
		$result['Insert'] += ($zoneID != null);
	}
	$htmlAry['ResultMsg'] = "<p>{$Lang['PowerPortfolio']['Setting']['ImportWarning']['InsertCount']}: {$result['Insert']}</p>";
	
	$buttons .= "";
	$buttons .= $indexVar['libpowerportfolio_ui']->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "window.location.href = 'index.php?task=settings.zone.list'");
	$htmlAry['Btn'] = $buttons;
	
	$STEPS_OBJ[] = array($i_general_select_csv_file, 0);
	$STEPS_OBJ[] = array($i_general_confirm_import_data, 0);
	$STEPS_OBJ[] = array($i_general_imported_result, 1);
	$pageStep = $indexVar['libpowerportfolio_ui']->GET_STEPS($STEPS_OBJ);
	$PAGE_NAVIGATION[] = array($Lang['Btn']['Import']." ".$Lang['PowerPortfolio']['Settings']['Zone']['Title'], "");
	
	$Navigation = $indexVar['libpowerportfolio_ui']->GET_NAVIGATION($PAGE_NAVIGATION);
	
	$indexVar['libpowerportfolio_ui']->Echo_Module_Layout_Start();
} else {
	No_Access_Right_Pop_Up();
}

// clear data
unset($_SESSION['PowerportfolioImportData']);

?>