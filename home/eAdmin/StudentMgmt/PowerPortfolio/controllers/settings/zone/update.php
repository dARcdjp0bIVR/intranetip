<?php
// Using: 
/*
 */
$colAry = array(
	'Name' => 'Name',
	'Quota' => 'Quota',
	'PictureType' => 'PictureType',
	'TopicSettingID' => 'TopicSettingID'
);
$dataAry = array();
foreach($colAry as $key => $col){
	$dataAry[$col] = $_POST[$key];
}
$dataAry['RubricCode'] = $code;
if($isDelete){
	foreach((array)$ZoneID as $sid){
		$indexVar['libpowerportfolio']->Delete_Zone_Setting($sid);
	}
} else {
	if($ZoneID != ''){
		//edit
		$indexVar['libpowerportfolio']->Update_Zone_Setting($ZoneID, $dataAry);
	} else {
		//new
		$ZoneID = $indexVar['libpowerportfolio']->Insert_Zone_Setting($dataAry);
	}
}
header('location: index.php?task=settings.zone.list');