<?php
// editing by 
/*
 * Change Log:
 * 	Date: 2019-10-11 Philips
 * 			- Added general warning box
 * 
 * Date	2017-02-09 Villa Add navigation Bar
 * Date 2017-02-06 Villa Fix En Name/ CH name exchanged
 */

# Page Title
$TAGS_OBJ[] = array($Lang['PowerPortfolio']['Settings']['Zone']['Title']);
$indexVar["libpowerportfolio_ui"]->Echo_Module_Layout_Start($indexVar["returnMsgLang"]);

if($isEdit)
{
	$navigation = $Lang["Btn"]["Edit"];
    $ZoneSetting = $indexVar['libpowerportfolio']->Get_Zone_Setting($ZoneID);
	if(!empty($ZoneSetting)) $ZoneSetting= $ZoneSetting[0];
	$name = $ZoneSetting['Name'];
	$quota = $ZoneSetting['Quota'];
	$topicSettingID = $ZoneSetting['TopicSettingID'];
    $pictureType = $ZoneSetting['PictureType'];
}
else
{
	$navigation = $Lang["Btn"]["New"];
	$pictureType = 1;
}

### Loading Image [Start]
$LoadingImage = "";
if($isEdit) {
	$LoadingImage = "<img class='LoadingImg' src='/images/$LAYOUT_SKIN/indicator.gif'>";
}
### Loading Image [End]

$topicSelecctor = $indexVar['libpowerportfolio_ui']->Get_TopicSetting_Selection($topicSettingID, "TopicOnChange()");

# Navigation
$navigationAry[] = array($Lang['PowerPortfolio']['Settings']['Zone']['Title'], "index.php?task=settings.zone.list");
$navigationAry[] = array($navigation);
$NivagationBar = $indexVar["libpowerportfolio_ui"]->GET_NAVIGATION_IP25($navigationAry);

# Picture Selection
$PICSelection = "";
$PICSelection .= "<table width='100%'>";
$PICSelection .= "<tr>";
for($i=1; $i<6; $i++)
{
	$isChecked = $pictureType==$i? "checked" : "";;
	$PICSelection .= "<td style='text-align: center'>";
	$PICSelection .= "<label for='picture_".$i."'><img src='".$PowerPortfolioConfig['imageFilePath']."/btn_subj_".$i.".png' width='100px'/></label><br>";
	$PICSelection .= "<input type='radio' value='".$i."' id='picture_".$i."' name='PictureType' ".$isChecked."/>";
	$PICSelection .= "</td>";
}
$PICSelection .= "</tr>";
$PICSelection .= "</table>";
?>