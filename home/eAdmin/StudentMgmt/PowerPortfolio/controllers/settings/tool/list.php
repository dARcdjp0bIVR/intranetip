<?php
// editing by
/*
 *
 */

// Include DB Table
include_once($indexVar['thisBasePath']."includes/libdbtable.php");
include_once($indexVar['thisBasePath']."includes/libdbtable2007a.php");

// Update Result Message
if($success){
	$returnMsg = $Lang['General']['ReturnMessage']['UpdateSuccess'];
}
else if($success==="0"){
	$returnMsg = $Lang['General']['ReturnMessage']['UpdateUnsuccess'];
}

// Page Title
$TAGS_OBJ[] = array($Lang['PowerPortfolio']['Settings']['Tool']['Title']);
$indexVar['libpowerportfolio_ui']->Echo_Module_Layout_Start($returnMsg);

// DB Table Settings
if (isset($ck_page_size) && $ck_page_size != "") {
	$page_size = $ck_page_size;
}
$field = ($field=='')? 1 : $field;
$order = ($order=='')? 1 : $order;
$page = ($page=='')? 1 : $page;
$pos = 0;

### ToolBar Action Buttons
$btnAry = array();
$btnAry[] = array('new', 'javascript: goNew();');
$htmlAry['contentTool'] = $indexVar['libpowerportfolio_ui']->Get_Content_Tool_By_Array_v30($btnAry);

### DB table action buttons
$btnAry = array();
// $btnAry[] = array('copy', 'javascript: checkCopy();');
// $btnAry[] = array('export', 'javascript: checkExport();');
$btnAry[] = array('delete', 'javascript: checkRemove(document.form1,\'ToolID[]\',\'index.php?task=settings.tool.update&isDelete=1\');');
$htmlAry['dbTableActionBtn'] = $indexVar['libpowerportfolio_ui']->Get_DBTable_Action_Button_IP25($btnAry);

$filterByClassTeacher = !$indexVar["libpowerportfolio"]->IS_POWER_PORTFOLIO_ADMIN_USER() && $indexVar["libpowerportfolio"]->IS_KG_CLASS_TEACHER();

## Year Selection
$YearSelection = $indexVar['libpowerportfolio_ui']->Get_Year_Selection($YearID, 'document.form1,submit()',false, false, 1);

## Zone Selection
$ZoneSearchSelection = $indexVar['libpowerportfolio_ui']->Get_Zone_Selection($ZoneID, 'document.form1.submit()','',false,1);

## Search Bar
$htmlAry['searchBox'] = $indexVar['libpowerportfolio_ui']->Get_Search_Box_Div('searchStr', stripslashes($searchStr));
$conds = ' 1 ';
if($YearID != ''){
	$conds .= " AND tl.YearID = '$YearID' ";
}
if($ZoneID != ''){
	$conds .= " AND tl.ZoneID = '$ZoneID' ";
}

### Table Data
$searchText = addslashes(trim($searchStr));

$sql = "SELECT 
            CONCAT('<img src=\"{$indexVar['thisImage']}', tl.PhotoSRC, '\" />') AS PhotoSRC,
            CONCAT('<a href=\"javascript:goEdit(', tl.ToolID,')\">', tl.Name, '</a>') as Name, 
            y.YearName, 
            ts.name as TopicName, 
            ze.Name as ZoneName, 
            GROUP_CONCAT(DISTINCT CONCAT(aii.Code, ' ', aii.Name) ORDER BY aii.Code ASC SEPARATOR '<br/>') as RubricCode, 
            CONCAT('<input type=\'checkbox\' class=\'checkbox\' name=\'ToolID[]\' id=\'ToolID', tl.ToolID, '\' value=', tl.ToolID,'>') as edit_box 
        FROM 
            ".$indexVar['thisDbName'].".RC_TOOL tl
            INNER JOIN YEAR y ON (tl.YearID = y.YearID)
            INNER JOIN ".$indexVar['thisDbName'].".RC_TOPIC_SETTING ts ON (tl.TopicSettingID = ts.TopicSettingID)
            INNER JOIN ".$indexVar['thisDbName'].".RC_ZONE ze ON (tl.ZoneID = ze.ZoneID) 
            LEFT JOIN ".$indexVar['thisDbName'].".RC_ABILITY_INDEX_ITEM aii ON (aii.RubricSettingID = ts.RubricSettingID AND FIND_IN_SET(aii.Code, tl.RubricCode) > 0)
        WHERE
            $conds
        GROUP BY 
            tl.ToolID
            ";

// Initiate DB Table
$li = new libdbtable2007($field, $order, $pageNo);
$li->sql = $sql;
$li->IsColOff = "IP25_table";
$li->field_array = array("PhotoSRC", "Name", "YearName", "TopicName", "ZoneName", "RubricCode", "edit_box");
$li->column_array = array(0, 0, 0, 0, 0, 0);
$li->wrap_array = array(0, 0, 0, 0, 0, 0);
$li->no_col = count($li->field_array)+1;
$li->column_list .= "<th class='tabletop tabletopnolink' width='1'>#</td>\n";
$li->column_list .= "<th class='tabletop tabletopnolink' width='25%'>".$li->column($pos++, '')."</td>\n";
$li->column_list .= "<th class='tabletop tabletopnolink' width='10%'>".$li->column($pos++, $Lang['PowerPortfolio']['Settings']['Tool']['Name'])."</td>\n";
$li->column_list .= "<th class='tabletop tabletopnolink' width='10%'>".$li->column($pos++, $Lang['PowerPortfolio']['Settings']['Tool']['ApplyForm'])."</td>\n";
$li->column_list .= "<th class='tabletop tabletopnolink' width='15%'>".$li->column($pos++, $Lang['PowerPortfolio']['Settings']['Tool']['Topic'])."</td>\n";
$li->column_list .= "<th class='tabletop tabletopnolink' width='10%'>".$li->column($pos++, $Lang['PowerPortfolio']['Settings']['Tool']['Zone'])."</td>\n";
$li->column_list .= "<th class='tabletop tabletopnolink' width='30%'>".$li->column($pos++, $Lang['PowerPortfolio']['Settings']['Tool']['Rubrics'])."</td>\n";
$li->column_list .= "<th class='tabletop tabletopnolink' width='1'>" . $indexVar['libpowerportfolio_ui']->Get_Checkbox('checkAll', 'checkAll', '', false, 'checkbox', '',"Check_All_Options_By_Class('checkbox', this.checked);") . "</td>\n";

// GET DB Table Content
$htmlAry['dataTable'] = $li->display();

// DB Table related Hidden Fields
$htmlAry['hiddenField'] = "";
$htmlAry['hiddenField'] .= "<input type='hidden' name='pageNo' value='".$li->pageNo."'>";
$htmlAry['hiddenField'] .= "<input type='hidden' name='order' value='1'>";
$htmlAry['hiddenField'] .= "<input type='hidden' name='field' value='".$li->field."'>";
$htmlAry['hiddenField'] .= "<input type='hidden' name='page_size_change' value=''>";
$htmlAry['hiddenField'] .= "<input type='hidden' name='numPerPage' value='".$li->page_size."'>";
?>