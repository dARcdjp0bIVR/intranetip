<?php
// Using: 
/*
 */
$rubricCode = $_POST['RubricCode'];
if(empty($rubricCode)) 
	$code = '';
else 
	$code = $rubricCode[2] != '' ? $rubricCode[2] : ($rubricCode[1] != '' ? $rubricCode[1] : $rubricCode[0]);
$colAry = array(
	'ReportName' => 'ReportName',
	'ApplyTopic' => 'ApplyTopic',
	'ActivityTitle' => 'ActivityTitle',
	'ActivityName' => 'ActivityName',
	'TopicSettingID' => 'TopicSettingID',
	'YearID' => 'YearID',
	'ActivityDate' => 'ActivityDate',
	'ActivityTime' => 'ActivityTime',
	'Location' => 'Location',
	'ContentType' => 'ContentType'
);
$dataAry = array();
foreach($colAry as $key => $col){
	$dataAry[$col] = $_POST[$key];
}
$dataAry['RubricCode'] = $code;
if($isDelete){
	foreach((array)$ActivitySettingID as $sid){
		$indexVar['libpowerportfolio']->Delete_Activity_Setting($sid);
	}
} else {
	if($ActivitySettingID != ''){
		//edit
		$indexVar['libpowerportfolio']->Update_Activity_Setting($ActivitySettingID, $dataAry);
	} else {
		//new
		$ActivitySettingID= $indexVar['libpowerportfolio']->Insert_Activity_Setting($dataAry);
	}
}
header('location: index.php?task=settings.activity.list');