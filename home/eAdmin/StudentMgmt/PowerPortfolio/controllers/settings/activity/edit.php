<?php
// Using: 
/*
 */
 
$TAGS_OBJ[] = array($Lang['PowerPortfolio']['Settings']['Activity']['Title']);
$indexVar["libpowerportfolio_ui"]->Echo_Module_Layout_Start($indexVar["returnMsgLang"]);

if($isEdit)
{
	$navigation = $Lang["Btn"]["Edit"];
	$ActivitySetting = $indexVar['libpowerportfolio']->Get_Activity_Setting($ActivitySettingID);
	if(!empty($ActivitySetting)) $ActivitySetting = $ActivitySetting[0];
	$activityName = $ActivitySetting['ActivityName'];
	$yearID = $ActivitySetting['YearID'];
	$applyTopic = $ActivitySetting['ApplyTopic'];
	$topicSettingID = $ActivitySetting['TopicSettingID'];
	$rubricCode = $ActivitySetting['RubricCode'];
	$activityTitle= $ActivitySetting['ActivityTitle'];
	$reportName= $ActivitySetting['ReportName'];
	$activityDate = $ActivitySetting['ActivityDate'];
	$activityTime = $ActivitySetting['ActivityTime'];
	$location = $ActivitySetting['Location'];
	$contentType = $ActivitySetting['ContentType'];
}
else
{
	$navigation = $Lang["Btn"]["New"];
	$useTopic = false;
	$contentType = 'Photo';
}

### Loading Image [Start]
$LoadingImage = "";
if($isEdit) {
	$LoadingImage = "<img class='LoadingImg' src='/images/$LAYOUT_SKIN/indicator.gif'>";
}
### Loading Image [End]

# Year Selection
$YearSelection = $indexVar["libpowerportfolio_ui"]->Get_Year_Selection($yearID, "YearOnChange()", false, true, 0, false, true);

$topicSelecctor = $indexVar['libpowerportfolio_ui']->Get_TopicSetting_Selection($topicSettingID, "TopicOnChange()", $yearID);

$contentTypeSelector = $indexVar['libpowerportfolio_ui']->Get_TopicSetting_ContentType_Selection($contentType, 'ContentTypeOnChange()');

$activityDateBox = $indexVar['libpowerportfolio_ui']->GET_DATE_PICKER("ActivityDate");

# Navigation
$navigationAry[] = array($Lang['PowerPortfolio']['Settings']['Activity']['Title'], "index.php?task=settings.activity.list");
$navigationAry[] = array($navigation);
$NivagationBar = $indexVar["libpowerportfolio_ui"]->GET_NAVIGATION_IP25($navigationAry);
