<?php
@SET_TIME_LIMIT(1200);
@ini_set(memory_limit, "800M");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

$studentIDAry = $_REQUEST['StudentID'];
$yearClassID = $_REQUEST['ClassID'];
$reportID = $_REQUEST['ReportID'];
$releaseDate = $_REQUEST['ReleaseDate'];
$topicID = $_REQUEST['topicID'];
// debug_pr($_REQUEST);die();
if(!empty($studentIDAry)){
# Create mPDF object

	$lclass = new year_class($yearClassID);
	$className = Get_Lang_Selection($lclass->ClassTitleB5,$lclass->ClassTitleEN);

	$imgPath = "asset/images/";
	
	$ReportSettings = $indexVar['libpowerportfolio']->Get_Report_Setting($ReportID);
	$ReportSettings = $ReportSettings[0];
	
// 	$topicAry = $ReportSettings['TopicID'];
// 	$topicAry = explode(',', $topicAry);
	$topicInfoAry = array();
	$_info= $indexVar['libpowerportfolio']->Get_Rubric_Topic_Setting($topicID);
	$topicInfoAry[] = $_info[0];
// 	foreach($topicAry as $topic){
// 		$_info = $indexVar['libpowerportfolio']->Get_Rubric_Topic_Setting($topic);
// 		$topicInfoAry[] = $_info[0];
// 	}
	$ca_reportTitleText = $ReportSettings['CAReportTitle'];
	
	include_once($indexVar["thisBasePath"]."includes/mpdf/mpdf.php");
	$mpdf = new mPDF($mode="zh", $format="A4", $default_font_size=0, $default_font="", $mgl=0, $mgr=0, $mgt=10, $mgb=3, $mgh=0, $mgf=1, $orientation="P");
	
	# Set mPDF Settings
	$mpdf->allow_charset_conversion = true;
	$mpdf->charset_in = "UTF-8";
	$mpdf->list_auto_mode = "mpdf";
// 	$mpdf->shrink_tables_to_fit = 1;
	$mpdf->use_kwt = true;
	
	# Set Report CSS
	$styleContent = file_get_contents($indexVar["thisBasePath"]."/home/eAdmin/StudentMgmt/PowerPortfolio/asset/ca_report.css");
	$styleContent = str_replace('images/', $imgPath,$styleContent);
	$mpdf->WriteHTML($styleContent, 1);
	
// 	$mpdf->DefHTMLHeaderByName("awardPageHeader", '');
	$mpdf->DefHTMLHeaderByName("emptyHeader", "");
// 	$mpdf->SetHTMLHeaderByName("emptyHeader");
	$mpdf->DefHTMLFooterByName("emptyFooter", "");
	
	# Set Report Content
	$htmlStart = "<div id='content'><div class='page-wrapper'><div class='inner'>";
	
	foreach($studentIDAry as $studentID){
		
		$lu = new libuser($studentID);
		$stuName = Get_Lang_Selection($lu->ChineseName, $lu->EnglishName);
		
		$sizeAry = array();
		$sizeAry['Total'] = 35;
		$sizeAry['TableHeader'] = 1.5;
		$sizeAry['Title1'] = 2;
		$sizeAry['Title2'] = 1;
		$sizeAry['row'] = 1;
		$sizeAry['rowLength'] = 34 * 3 * 1.84;
		$sizeAry['TableFooter'] = 0.5;
		$sizeAry['gradeDisplay'] = 1;
		$sizeAry['ReportTitle'] = 3.5;
		$sizeAry['wordLimit'] = 34;
		$isFirstPage = true;
		$currentSize = 0;
		
		$HTMLcontent = array();
		
		$HTMLcontent['pageHeader'] = <<< EOD
<table class="tbl_pageHead" cellpadding="0" cellspacing="0">
	<tbody>
		<tr>
		<td>
			<table>
				<tbody>
					<tr>
						<td class="variObj header_L"><img src="{$imgPath}vari_header_L.png"/></td>
						<td class="variObj header_C">2019 - 2020 幼兒班評量紀錄 下學期</td>
						<td class="variObj header_L"><img src="{$imgPath}vari_header_R.png"/></td>
					</tr>
				</tbody>
			</table>
		</td>
		</tr>
	</tbody>
</table>
EOD;
// 		$mpdf->DefHTMLHeaderByName("pageHeader", $HTMLcontent['pageHeader']);
// 		$mpdf->SetHTMLHeaderByName("pageHeader");
/*		
		$HTMLcontent['title'] = <<< EOD
<table class="tbl_header_title" cellpadding="0" cellspacing="0">
	<tbody>
		<tr>
			<td>
				<table>
					<tbody>
						<tr>
							<td class="icon_header_title"><img width="14mm" src="{$imgPath}icon_header_title.png" /></td>
							<td>{$ca_reportTitleText}</td>
						</tr>
					</tbody>
				</table>
			</td>
		</tr>
	</tbody>
</table>
EOD;*/

		$HTMLcontent['title'] = <<< EOD
<table class="tbl_header_title" cellpadding="0" cellspacing="0">
	<tbody>
		<tr>
			<td width="50%">
				<table>
					<tbody>
						<tr>
							<td class="icon_header_title"><img width="14mm" src="{$imgPath}icon_header_title.png" /></td>
							<td>{$ca_reportTitleText}</td>
						</tr>
					</tbody>
				</table>
			</td>
			<td width="50%">
				<table>
					<tbody>
						<tr>
							<td width="30%" class="header_field" style="font-weight: normal;font-size:1.6em;">姓名﹕ </td> 
							<td width="70%" class="header_info" style="border-bottom:2px #3BACCD solid;line-height:1.1em;padding-bottom:0;text-align:center;font-size:1.2em;color:#333;}">{$stuName}</td> 
						</tr>
						<tr>
							<td width="30%" class="header_field" style="font-weight: normal;font-size:1.6em;">班級﹕ </td>
							<td width="70%" class="header_info" style="border-bottom:2px #3BACCD solid;line-height:1.1em;padding-bottom:0;text-align:center;font-size:1.2em;color:#333;}">{$className}</td>
						</tr>
					</tbody>
				</table>
			</td>
		</tr>
	</tbody>
</table>
EOD;

		$HTMLcontent['TopicTables'] = array();
		foreach($topicInfoAry as $topicInfo){
			$topicStart = true;
			
			$studentScoreAry = $indexVar['libpowerportfolio']->Get_Student_Score(array($studentID), array($topicInfo['TopicSettingID']));
			$studentScoreAry = BuildMultiKeyAssoc($studentScoreAry, 'RubricCode');
			
			$ExamPeriodStart = $topicInfo['ExamPeriodStart'];
			$ExamPeriodEnd = $topicInfo['ExamPeriodEnd'];
// 			$scoreChk = false;
// 			foreach($studentScoreAry as $studentScore){
				
// 			}
// 			if(!$scoreChk){
// 				continue;
// 			}
			$_topicName = $topicInfo['Name'];
			
			// content here
			$_rubricList = $topicInfo['RubricList'];
			$_rubricListAry = explode(',',$_rubricList);
			$_rubricID = $topicInfo['RubricSettingID'];
			$_rubricSetting = $indexVar['libpowerportfolio']->Get_Rubric_Setting($_rubricID);
			$_rubricSetting = $_rubricSetting[0];
			$_colorSettingAry = $indexVar['libpowerportfolio']->Get_Rubric_Setting_Item($_rubricID, 'ExamGradeNumColor');
			$_colorSettingAry = BuildMultiKeyAssoc($_colorSettingAry, 'Level');
			$_titleSettingAry = $indexVar['libpowerportfolio']->Get_Rubric_Setting_Item($_rubricID, 'ExamGradeNumContent');
			$_titleSettingAry = BuildMultiKeyAssoc($_titleSettingAry, 'Level');
			$_settingAry = array('color' => $_colorSettingAry, 'title' => $_titleSettingAry);
			
			$tableHeader = '<table class="tbl_evaluation" cellpadding="0" cellspacing="0" style="">';
				// topic header
				$tableHeader.= '<thead><tr class="tbl_head type_2">
							<td style="width:132mm; height:12.5mm; color:#2DADB6; font-size:1.3em; font-weight:bold; padding-top:1mm; padding-left:17mm;">'.$_topicName.'</td>
							<td style="color:#FFF; font-size:1.2em; font-weight:bold;padding:2mm 7mm 0mm 5mm; text-align:center; line-height:1em;">學習評估表現</td>
						</tr>
					</thead>';
				$tableHeader .= '<tbody>
					<tr style="background-color:#FFF; border-color:#FFF;
background:url('.$imgPath.'vari_whitebox_M.png) repeat-y left top; background-size: 195mm auto;background-image-resize:1;">
						<td colspan="2" style="padding-left:5mm;">';
				
				
				
				// table footer
				$tableFooter = '</td>
					</tr>
				</tbody>
				<tbody ><!-- important : bottom of the table -->
					<tr style="background:url('.$imgPath.'vari_whitebox_B.png) no-repeat left top; background-size: 195mm auto;background-image-resize:1;">
						<td colspan="2">&nbsp;</td>
					</tr>
				</tbody>
			</table>';
				
				$gradeDisplay = '
<table class="tbl_legend" cellpadding="0" cellspacing="0">
	<thead>
		<tr class="tbl_head">
			<td style="width:49mm;">&nbsp;</td>
			<td style="width:132mm;">&nbsp;</td>
		</tr>
	</thead>
	<tbody>
		<tr class="tbl_body">
			<td style="height:0px; line-height:1mm; padding:0 3mm;">
				<div class="title_area">學習評估說明</div>
			</td>
			<td style="height:0px; line-height:1mm; padding:0 3mm;text-align:right;">
				<table class="legend_area">
					<tbody>
					<tr style="text-align=right">'."\n";
				for($i=1;$i<=$_rubricSetting['ExamGradeNum']; $i++){
					$_color = $_colorSettingAry[$i]['Content'];
					$_text = $_titleSettingAry[$i]['Content'];
					$gradeDisplay .= '<td>
												<table>
													<tbody>
														<tr>
															<td class="legend_img" style="background-color:'.$_color.';"><img width="6mm" height="6mm" src="'.$imgPath.'ball_color_base.png" /></td>
															<td class="legend_text" style="height:0px; line-height:1mm; padding:0 3mm;">'.$_text.'</td>
														</tr>
													</tbody>
												</table>
											  </td>'."\n";
				}
				$gradeDisplay .= '</tr>
					</tbody>
				</table>
			</td>
		</tr>
	</tbody>
	<tbody><!-- important : bottom of the table -->
		<tr class="tbl_foot">
			<td colspan="2">&nbsp;</td>
		</tr>
	</tbody>
</table>';
				//<td class="" width="20%" style="">&nbsp;</td>
				$pageFooter = <<< EOD
<table class="tbl_pageFoot" cellpadding="0" cellspacing="0" style="width:100%;margin-left:5mm;color:#FFFFFF;font-size:1em;">
	<tbody>
		<tr>
		<td>
			<table>
				<tbody>
					<tr>
						<td class="" width="65%" style="">
							<table><tbody><tr>
							<td class="variObj footer_L" width="5mm"><img src="{$imgPath}vari_transGreen_L.png"/></td>
							<td class="variObj footer_C" >觀察評量時段‧{$ExamPeriodStart} {$Lang['General']['To']} {$ExamPeriodEnd}</td>
							<td class="variObj footer_L" width="5mm"><img src="{$imgPath}vari_transGreen_R.png"/></td>
							</tr></tbody></table>
						</td>
						
						<td class="" width="35%" style="text-align: right">
							<table><tbody><tr>
							<td class="variObj footer_L" width="5mm"><img src="{$imgPath}vari_transGreen_L.png"/></td>
							<td class="variObj footer_C" >發出日期‧{$releaseDate}</td>
							<td class="variObj footer_L" width="5mm"><img src="{$imgPath}vari_transGreen_R.png"/></td>
							</tr></tbody></table>
						</td>
					</tr>
				</tbody>
			</table>
		</td>
		</tr>
	</tbody>
</table>
EOD;
				$mpdf->DefHTMLFooterByName("pageFooter", $pageFooter);
				$mpdf->SetHTMLFooterByName("pageFooter");
				$partsAry = array(
						'TableHeader' => $tableHeader,
						'TableFooter' => $tableFooter,
						'gradeDisplay' =>$gradeDisplay,
						'sizeAry' => $sizeAry
				);
				
				$sql = "SELECT Code, Name, Type, Level, UpperCat
						FROM {$indexVar['thisDbName']}.RC_ABILITY_INDEX_ITEM
						WHERE Code IN ('" . implode("','", $_rubricListAry). "')
							  AND RubricSettingID = '{$_rubricID}'
						ORDER BY Code ASC";
				$rubricItemAry = $indexVar['libpowerportfolio']->returnArray($sql);
				unset($sql);
				$sql = "SELECT item.Code, item.Name, item.Type, item.Level, item.UpperCat
						FROM {$indexVar['thisDbName']}.RC_ABILITY_INDEX_ITEM rait
						INNER JOIN {$indexVar['thisDbName']}.RC_ABILITY_INDEX_ITEM item
							ON rait.UpperCat = item.Code AND rait.RubricSettingID = item.RubricSettingID
						WHERE rait.Code IN ('" . implode("','", $_rubricListAry). "')
							AND rait.RubricSettingID = '{$_rubricID}'
						GROUP BY item.ItemID
						ORDER BY item.Code ASC";
				$upperCatAry = $indexVar['libpowerportfolio']->returnArray($sql);
				unset($sql);
				// Check for 3 Level
				$_tempChk = false;
				foreach($upperCatAry as $upperCat){
					if($upperCat['UpperCat'] != '') $_tempChk = true; 
				}
				if($_tempChk){
					$_tempUpperCatAry = Get_Array_By_Key($upperCatAry, 'UpperCat');
					$_tempUpperCatAry = array_unique($_tempUpperCatAry);
					$sql = "SELECT DISTINCT Code, Name, Type, Level, UpperCat
							FROM {$indexVar['thisDbName']}.RC_ABILITY_INDEX_ITEM
							WHERE Code IN ('" . implode("','", $_tempUpperCatAry). "')
								AND RubricSettingID = '{$_rubricID}'
							ORDER BY Code ASC";
					$topCatAry = $indexVar['libpowerportfolio']->returnArray($sql);
					$topCatAry = BuildMultiKeyAssoc($topCatAry, 'Code');
					$upperCatAry = BuildMultiKeyAssoc($upperCatAry, 'Code');
					foreach($rubricItemAry as $rubricItem){
						$upperCatAry[$rubricItem['UpperCat']]['Children'][] = $rubricItem;
					}
					foreach($upperCatAry as $upperCat){
						$topCatAry[$upperCat['UpperCat']]['Children'][] = $upperCat;
					}
					$CatAry = $topCatAry;
					unset($sql);
				} else {
					$upperCatAry = BuildMultiKeyAssoc($upperCatAry, 'Code');
					foreach($rubricItemAry as $rubricItem){
						$upperCatAry[$rubricItem['UpperCat']]['Children'][] = $rubricItem;
					}
					$CatAry = $upperCatAry;
				}
				foreach($CatAry as $cat){
					if($isFirstPage){
						$tableContent = "";
						$tableContent .= $HTMLcontent['title'];
						$currentSize += $sizeAry['ReportTitle'];
						
						$tableContent .= $partsAry['TableHeader'];
						$currentSize += $sizeAry['TableHeader'];
						
						$isFirstPage = false;
						$topicStart = false;
					} else if($topicStart){
						$tableContent = "";
						if( ($sizeAry['Total'] - $currentSize) < 
							($sizeAry['TableHeader'] + $sizeAry['TableFooter'] + $sizeAry['gradeDisplay'] + $sizeAry['Title1'] + $sizeAry['Title2'])
						){
							$tableContent .= '<pagebreak />';
							$currentSize = 0;
						}
						$tableContent .= $partsAry['TableHeader'];
						$currentSize += $sizeAry['TableHeader'];
						$topicStart = false;
					} else {
						if(($sizeAry['Total'] - $currentSize) < ($sizeAry['Title1'] + $sizeAry['TableFooter'] + $sizeAry['gradeDisplay'] + $sizeAry['row'])){
							$tableContent .= $partsAry['TableFooter'] . $partsAry['gradeDisplay'];
							$tableContent .= '<pagebreak />';
							$tableContent .= $partsAry['TableHeader'];
							$currentSize = $sizeAry['TableHeader'];
						}
					}
					
					$_children = $cat['Children'];
					$tableContent .= '<table><tbody><tr><td class="eval_subtitle_1">'.$cat['Code'].' '.$cat['Name'] . '</td></tr></tbody></table>';
					$currentSize += $sizeAry['Title1'];
					
					$tableContent .= displayRubricTable($imgPath, $_children, $studentScoreAry,$_rubricSetting['ExamGradeNum'],$_settingAry, $currentSize, $partsAry);
// 					$x .= '<span style="height: 0px; line-height:6mm">&nbsp;</span>';
				}
				$tableContent .= $partsAry['TableFooter'] . $partsAry['gradeDisplay'];
				$currentSize += $sizeAry['TableFooter'] + $sizeAry['gradeDisplay'];
				$HTMLcontent['TopicTables'][] = $tableContent;
		}
// 		$ca_reportHTML = $htmlStart . $HTMLcontent['title'];
// 		foreach($HTMLcontent['TopicTables'] as $html) $ca_reportHTML .= $html;
// 		$ca_reportHTML .= "</div></div></div>";
		foreach($HTMLcontent['TopicTables'] as $html) $mpdf->WriteHTML($html);
// 		$ca_reportHTML= $htmlStart . $HTMLcontent['pageHeader']. $HTMLcontent['title']. "</div></div></div>";
		# html Test Code
// 		echo '<style>'.$styleContent.'</style>';
// 		echo "<div id='content'><div class='page-wrapper'><div class='inner'>";
// 		echo "</div></div></div>";
// 		echo $ca_reportHTML;
// 		die();
// 		$mpdf->WriteHTML($ca_reportHTML);
		
		if($studentID != end($studentIDAry)){
// 			$mpdf->SetHTMLHeaderByName("emptyHeader");
			$mpdf->writeHTML("<pagebreak resetpagenum='1' />");
		}
	}
	
// 	$mpdf->WriteHTML("</div></div></div>");
	
	# Output PDF file
	$mpdf->Output("reportd.pdf", "I");
}

function displayRubricTable($imgPath, $itemAry, $scoreAry, $maxLevel, $settingAry, &$currentSize, $partsAry){
	$defaultColor = '#EFEFEF';
	$content = '';
	$sizeAry = $partsAry['sizeAry'];
	foreach($itemAry as $item){
		$_code = $item['Code'];
		if($item['Children']){
// 			$content .= '<span style="height: 0px; line-height:1mm">&nbsp;</span>';
			if(($sizeAry['Total'] - $currentSize) < ($sizeAry['Title1'] + $sizeAry['TableFooter'] + $sizeAry['gradeDisplay'] + $sizeAry['row'])){
				$content .= $partsAry['TableFooter'] . $partsAry['gradeDisplay'];
				$content .= '<pagebreak />';
				$content .= $partsAry['TableHeader'];
				$currentSize = $sizeAry['TableHeader'];
			}
			$content .= '<table><tbody><tr><td class="eval_subtitle_2" style=" line-height:1.8em;">'.$_code.' '.$item['Name'] . '</td></tr></tbody></table>'."\n";
			$currentSize += $sizeAry['title2'];
			$content .= displayRubricTable($imgPath, $item['Children'], $scoreAry, $maxLevel, $settingAry,$currentSize, $partsAry);
		} else {
			$length = getDisplayLength($item['Name']);
			$rowCount = ceil($length / $sizeAry['rowLength']);
			$_score = $scoreAry[$_code]['Score'];
			$_isNA = $scoreAry[$_code]['isNA'];
			if($_isNA) continue;
			if(($sizeAry['Total'] - $currentSize) < ( ($sizeAry['row']*$rowCount) + $sizeAry['TableFooter'] + $sizeAry['gradeDisplay'])){
				$content .= $partsAry['TableFooter'] . $partsAry['gradeDisplay'];
				$content .= '<pagebreak />';
				$content .= $partsAry['TableHeader'];
				$currentSize = $sizeAry['TableHeader'];
			}
			$content .= '<div class="eval_item_1">';
			$content .= '<table style="overflow:wrap"><tbody><tr>';
			$content .= '<td  style="width:132mm; padding-top:0.5mm; line-height:1.8em;" class="details">'.$item['Name'].'</td>'."\n";
			$content .= '<td style="width:54mm; line-height:1em; text-align:right;vertical-align:top;" class="marking">'."\n";
			$content .= '<table><tbody><tr>';
			for($i=1;$i<=$maxLevel;$i++){
				$_color = ($_score == $i) ? $settingAry['color'][$i]['Content'] : $defaultColor;
				$content .= '<td style="width:5mm"></td>';
				$content .= '<td class="ball" style="background-color:'.$_color.'"><img height="6mm" width="6mm" src="'.$imgPath.'ball_color_base.png" /></td>'."\n";
			}
			$content .= '</tr></tbody></table>';
			$content .= '</td>';
			$content .= '</tr></tbody></table>';
			$content .= '</div>'."\n";
			$currentSize += $sizeAry['row'] * $rowCount;
		}
	}
	return $content;
}
function getDisplayLength($text){
	# Handle prefix display length
	$prefixBypeLen = strlen($text);		        // byte count
	$prefixWordLen = mb_strlen($text);           // word count
	$prefixLenDiff = (($prefixWordLen * 3) - $prefixBypeLen) / 2;
	
	$prefixDisplayLen = 0;
	if($prefixWordLen == $prefixBypeLen) {					// English only
		$prefixDisplayLen = $prefixBypeLen;
	}
	else if (($prefixWordLen * 3) == $prefixBypeLen) {		// Chinese only
		$prefixDisplayLen = $prefixWordLen * 1.84;
	}
	else if (($prefixWordLen * 3) > $prefixBypeLen) {			// English + Chinese
		$prefixDisplayLen = $prefixLenDiff + (($prefixWordLen - $prefixLenDiff) * 1.84);
	}
	return $prefixDisplayLen;
}
?>