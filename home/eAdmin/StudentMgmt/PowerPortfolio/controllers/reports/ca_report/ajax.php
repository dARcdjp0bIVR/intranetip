<?php 
$Action = $_REQUEST['Action'];

switch($Action)
{
	case 'TopicByReport':
		$ReportID = $_REQUEST['ReportID'];
		$ReportSettings = $indexVar['libpowerportfolio']->Get_Report_Setting($ReportID);
		$ReportSettings = $ReportSettings[0];
		$topicAry = $ReportSettings['TopicID'];
		$topicAry = explode(',', $topicAry);
		
		$sql = "SELECT TopicSettingID, Name FROM {$indexVar['thisDbName']}.RC_TOPIC_SETTING WHERE TopicSettingID IN('" . implode("','",$topicAry) . "')";
		$topicArr = $indexVar['libpowerportfolio']->returnArray($sql);
		
		echo getSelectByArray($topicArr, "id='topicID' name='topicID' required", '', 0, 0);
		break;
	case 'ClassByReport':
		$ReportID = $_REQUEST['ReportID'];
		$ReportSettings = $indexVar['libpowerportfolio']->Get_Report_Setting($ReportID);
		$ReportSettings = $ReportSettings[0];
		$targetYear = $ReportSettings['YearID'];
		echo $indexVar['libpowerportfolio_ui']->Get_Class_Selection('', 'js_Reload_Student_Selection()', $withYearOptGroup=false, $noFirstTitle=false, $targetYear, 'ClassID');
		break;
	case 'StudentByClass':
		# Get Class Student
		$classStudentAry = array();
		$classStudentAry = $indexVar["libpowerportfolio"]->Get_Student_By_Class($_REQUEST["ClassID"]);
		
		# Get Excluded Student List
		$excludeStudentAry = array();
		if($_REQUEST["ExcludeStudentIDList"] != '') {
			$excludeStudentAry = explode(',', $_REQUEST["ExcludeStudentIDList"]);
		}
		
		$dataAry = array();
		foreach((array)$classStudentAry as $studentInfo)
		{
			if(!empty($excludeStudentAry) && in_array($studentInfo["UserID"], (array)$excludeStudentAry)){
				continue;
			}
			
			$studentName = $studentInfo["StudentName"];
			$studentClassName = $studentInfo["ClassName"];
			$studentClassNumber = $studentInfo["ClassNumber"];
			$studentDisplay = $studentName." (".$studentClassName."-".$studentClassNumber.")";
			
			$dataAry[] = array($studentInfo["UserID"], $studentDisplay);
		}
		
		$selectTags = " id='".$_REQUEST["SelectionID"]."' name='".$_REQUEST["SelectionName"]."' ".($_REQUEST["isMultiple"] ? "multiple" : "")." size=10 ";
		$studentSelection = getSelectByArray($dataAry, $selectTags, "", $_REQUEST["isAll"], $_REQUEST["noFirst"]);
		if($_REQUEST["withSelectAll"]) {
			$selectAllBtn = $indexVar["libpowerportfolio_ui"]->GET_SMALL_BTN($button_select_all, "button", "js_Select_All('".$_REQUEST["SelectionID"]."', 1);", "selectAllBtn");
		}
		$returnContent = $studentSelection.$selectAllBtn;
		$returnContent .= $indexVar["libpowerportfolio_ui"]->MultiSelectionRemark();
		echo $returnContent;
	default:
		break;
}

?>