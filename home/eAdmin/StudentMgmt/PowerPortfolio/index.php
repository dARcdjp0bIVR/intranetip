<?php
// Using : 
/*
 * Change Log:
 */
//  print_r($_FILES);die();
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/json.php");
include_once($PATH_WRT_ROOT."lang/powerportfolio_lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/PowerPortfolio/config.inc.php");
include_once($PATH_WRT_ROOT."includes/PowerPortfolio/libpowerportfolio.php");
include_once($PATH_WRT_ROOT."includes/PowerPortfolio/libpowerportfolio_ui.php");


# Initial objects
//$indexVar['libreportcard'] = new libreportcardkindergarten();
//$indexVar['libreportcard_ui'] = new libreportcardkindergarten_ui();
$indexVar['libpowerportfolio'] = new libpowerportfolio();
$indexVar['libpowerportfolio_ui'] = new libpowerportfolio_ui();
$indexVar['libfilesystem'] = new libfilesystem();
$indexVar['JSON'] = new JSON_obj();


# Get Basic Data
$indexVar['curUserId'] = $_SESSION['UserID'];
$indexVar['thisBasePath'] = $PATH_WRT_ROOT;
$indexVar['thisDbName'] = $indexVar['libpowerportfolio']->DBName;
$indexVar['thisYearId'] = $indexVar['libpowerportfolio']->AcademicYearID;
// $indexVar['thisImage'] = $PATH_WRT_ROOT."file/reportcard_kindergarten/images/";
$thisSchoolYearName = getAYNameByAyId($indexVar['thisYearId']);
$indexVar['thisImage'] = $PATH_WRT_ROOT."file/power_portfolio/".$thisSchoolYearName."/images/";
$indexVar['saveImage'] = $intranet_root ."/file/power_portfolio/".$thisSchoolYearName."/images/";
$indexVar['thisArchiveReport'] = $PATH_WRT_ROOT."file/power_portfolio/".$thisSchoolYearName."/reports/";
$indexVar['emptySymbol'] = '---';


# Handle all POST / GET values by urldecode(), stripslashes(), trim()
array_walk_recursive($_POST, 'handleFormPost');
array_walk_recursive($_GET, 'handleFormPost'); 


# Get System Message
$returnMsgKey = $_GET['returnMsgKey'];
$indexVar['returnMsgLang'] = $Lang['General']['ReturnMessage'][$returnMsgKey];


# Set Page Tab
$indexVar['task'] = $_POST['task']? $_POST['task'] : $_GET['task'];
if ($indexVar['task'] == '') {
	// Go to module index page if not defined
	//$indexVar['task'] = 'mgmt'.$PowerPortfolioConfig['taskSeparator'].'topic_timetable'.$PowerPortfolioConfig['taskSeparator'].'list';
    $indexVar['task'] = $indexVar['libpowerportfolio']->Get_User_Default_Page();
}
$CurrentPage = $indexVar['task'];
$CurrentPageArr['PowerPortfolio_eAdmin'] = 1;


# Special handling for Parent Access
/*
if($indexVar['libpowerportfolio']->IS_KG_PARENT_ACCESS()) {
    unset($CurrentPageArr['eReportCardKindergarten_eAdmin']);
    $CurrentPageArr['eServiceeReportCardKindergarten'] = 1;
}
*/


# Check Access Right
$indexVar['libpowerportfolio']->Check_Access_Right($indexVar['task']);


# Check Tablet Page
$taskStructureAry = explode($PowerPortfolioConfig['taskSeparator'], $indexVar['task']);
$isDeviceView = $taskStructureAry[0]=="lesson";


# Load Controller
$indexVar['controllerScript'] = 'controllers/'.str_replace($PowerPortfolioConfig['taskSeparator'], '/', $indexVar['task']).'.php';
if (file_exists($indexVar['controllerScript']))
{
	// Include Controller Script
	include_once($indexVar['controllerScript']);
	
	# Load Template
	$indexVar['viewScript'] = 'views/'.str_replace($PowerPortfolioConfig['taskSeparator'], '/', $indexVar['task']).'.tmpl.php';
	if (file_exists($indexVar['viewScript']))
	{
		// Include Template Script
		include_once($indexVar['viewScript']);

		# Page Footer
		if( strpos($indexVar['task'], 'print') !== false ||
            strpos($indexVar['task'], 'thickbox') !== false
            /*||
            strpos($indexVar['task'], 'mgmt.generate_reports.archive') !== false ||
            strpos($indexVar['task'], 'settings.admin_group.tb_member') !== false
            */
        ) {
			// Print Page and Thickbox => no footer
		}
		else if ($isDeviceView) {
			$indexVar['libpowerportfolio_ui']->Echo_Device_Layout_Stop();
		}
		else {
			$indexVar['libpowerportfolio_ui']->Echo_Module_Layout_Stop();
		}
	}
	else {
		// Update or Ajax Script => without template script
	}
}
else {
	No_Access_Right_Pop_Up();
}

intranet_closedb();
?>