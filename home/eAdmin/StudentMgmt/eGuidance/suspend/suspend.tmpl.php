<?
/*
 * 	Log
 * 
 *  2018-05-25 [Cameron]
 *      - show student info and disable student selection for edit [case #M138226]
 *      
 *	2017-08-04 [Cameron]
 *		- add confidential field
 *		- add save and notify button
 *	
 * 	2017-07-24 [Cameron]
 * 		- add field EndDate
 * 
 * 	2017-06-29 [Cameron]
 * 		- add label for radio button item so that clicking lable is the same as clicking the radio button
 *  	- add loadingImg
 * 
 * 	2017-05-19 [Cameron]
 * 		fix bug: disable button after submit to avoid adding duplicate record
 */
?>
<?=$linterface->Include_Thickbox_JS_CSS();?>
<script type="text/javascript">

<?include($PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eGuidance/common_js.php");?>

function checkForm(obj) 
{
	if(!check_text(obj.StudentID,'<?=$Lang['eGuidance']['Warning']['SelectStudent']?>'))
	{
		return false;
	}
	
	if ($(":input[name='SuspendType']:checked").length == 0) {
		alert('<?=$Lang['eGuidance']['suspend']['Warning']['SelectSuspendType']?>');
		$("#InSchool").focus();
		return false;
	}
	
 	if (!check_date(obj.StartDate,"<?php echo $i_invalid_date; ?>")) {
 		return false;
 	}

	if ( ($('#StartDate').val() != '' ) && ($('#EndDate').val() != '' ) && (compareDate($('#EndDate').val(), $('#StartDate').val()) < 0) ) {
		alert('<?=$Lang['General']['WarningArr']['EndDateCannotEarlierThanStartDate']?>');
		return false;
 	}

 	if ($('#IsConfidentialYes').is(':checked')) {
 		if ($('#ConfidentialViewerID option').length == 0) {
		    alert('<?=$Lang['eGuidance']['Warning']['SelectConfidentialViewer']?>');
		    return false;
 		} 
 	}

	return true;

}

$(document).ready(function(){
	var isLoading = true;
	var loadingImg = '<?=$linterface->Get_Ajax_Loading_Image() ?>';
	
	$('#ClassName').change(function(){
        isLoading = true;
		$('#StudentNameSpan').html(loadingImg);
		
		$.ajax({
			dataType: "json",
			type: "POST",
			url: '../ajax/ajax.php',
			data : {
				'action': 'getStudentNameByClass',
				'ClassName': $('#ClassName').val()
			},		  
			success: update_student_list,
			error: show_ajax_error
		});
		
	});

	
	$('#btnSubmit').click(function(e) {
	    e.preventDefault();
	    $('input.actionBtn').attr('disabled', 'disabled');
	    $('#TeacherID option').attr('selected',true);
		if ($('#IsConfidentialYes').is(':checked')) {
			$('#ConfidentialViewerID option').attr('selected',true);
		}
	    
		if (checkForm(document.form1)) {
	        $('#form1').submit();
		}
		else {
			$('input.actionBtn').attr('disabled', '');
		}
	});
	
});

function notify_colleague_update(recordID) {
	if ($('#IsConfidentialYes').is(':checked')) {
		$('#ConfidentialViewerID option').attr('selected',true);
	}
	if (checkForm(document.form1)) {
		tb_show('<?=$Lang['eGuidance']['NotifyUpdate']?>','../ajax/ajax_layout.php?action=NotifyColleagueUpdate&RecordID='+$('#'+recordID).val()+'&RecordType='+recordID+'&height=450&width=650');
	}
}

function show_history_push_message() {
	tb_show('<?=$Lang['eGuidance']['PushMessage']['ViewStatus']?>','../ajax/ajax_layout.php?action=GetHistoryNotification&RecordID='+$('#SuspendID').val()+'&RecordType=Suspend&height=450&width=650');		
}

function update_student_list(ajaxReturn) {
	if (ajaxReturn != null && ajaxReturn.success){
		$('#StudentNameSpan').html(ajaxReturn.html);
		isLoading = false;
	}
}	

</script>

<form name="form1" id="form1" method="post" action="<?=$form_action?>">
<table id="main_table" width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td class="navigation"><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
				</tr>
			</table>
			
			<table width="88%" border="0" cellpadding="5" cellspacing="0">
				<tr>
					<td>
						<table align="center" class="form_table_v30">
							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['Identity']['Student']?>
									<span class="tabletextrequire">*</span></td>
								<td class="tabletext">
								<?php if ($form_action == 'edit_update.php'):?>
									<?php echo $studentInfo;?>
								<?php else:?>
									<span><?=$Lang['eGuidance']['Class']?></span>&nbsp;<?=$classSelection?> &nbsp;
									<span><?=$Lang['eGuidance']['StudentName']?></span>&nbsp;<span id="StudentNameSpan"><?=$studentSelection?></span> 
									<?=$linterface->GET_SMALL_BTN($Lang['eGuidance']['personal']['ViewPersonalRecord'],'button','ViewStudentInfo();','BtnViewStudentInfo')?></td>
								<?php endif;?>	
							</tr>

							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['eGuidance']['TeacherName']?></td>
								<td class="tabletext"><?=$libguidance_ui->getTeacherSelection($rs_suspend['Teacher'])?>
								</td>
							</tr>
						
							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['eGuidance']['suspend']['SuspendType']?>
									<span class="tabletextrequire">*</span></td>
								<td class="tabletext">
									<input type="radio" name="SuspendType" id="InSchool" value="in"<?=($rs_suspend['SuspendType']=='in'?' checked':'')?>><label for="InSchool"><?=$Lang['eGuidance']['suspend']['InSchool']?></label><br>
									<input type="radio" name="SuspendType" id="OutSchool" value="out"<?=($rs_suspend['SuspendType']=='out'?' checked':'')?>><label for="OutSchool"><?=$Lang['eGuidance']['suspend']['OutSchool']?></label>
								</td>
							</tr>
							
							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['General']['StartDate']?>
									<span class="tabletextrequire">*</span></td>
								<td class="tabletext"><?=$linterface->GET_DATE_PICKER("StartDate",(empty($rs_suspend['StartDate'])?date('Y-m-d'):$rs_suspend['StartDate']))?> </td>
							</tr>

							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['General']['EndDate']?></td>
								<td class="tabletext"><?=$linterface->GET_DATE_PICKER("EndDate",((empty($rs_suspend['EndDate']) || $rs_suspend['EndDate'] == '0000-00-00')?'':$rs_suspend['EndDate']),$OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="",$ID="",$SkipIncludeJS=0, $CanEmptyField=1)?></td>
							</tr>
							
							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['eGuidance']['suspend']['Result']?>
									</td>
								<td class="tabletext"><?=$linterface->GET_TEXTAREA("Result", $rs_suspend['Result'], 80, 10)?></td>
							</tr>

							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['eGuidance']['Confidential']?><img id="ConfidentialIcon" src="/images/<?=$LAYOUT_SKIN?>/icalendar/icon_lock.gif" style="display:<?=($rs_suspend['IsConfidential']?'':'none')?>"></td>
								<td class="tabletext">
									<input type="radio" name="IsConfidential" id="IsConfidentialYes" value="1"<?=$rs_suspend['IsConfidential']?' checked':''?>><label for="IsConfidentialYes"><?=$Lang['General']['Yes'].'('.$Lang['eGuidance']['Warning']['SelectAuthorizedUser'].')'?></label>
									<input type="radio" name="IsConfidential" id="IsConfidentialNo" value="0"<?=$rs_suspend['IsConfidential']?'':' checked'?>><label for="IsConfidentialNo"><?=$Lang['General']['No']?></label>
									
									<div id="ConfidentialSection" style="display:<?=($rs_suspend['IsConfidential']?'':'none')?>">
										<?=$libguidance_ui->getConfidentialViewerSelection($rs_suspend['ConfidentialViewerID'])?>
									</div>
								</td>
							</tr>

							<?=$libguidance_ui->getAttachmentLayout('Suspend',$suspendID)?>
							
							<tr>
								<td valign="top" nowrap="nowrap" class="tabletextremark"><?=$i_general_required_field?></td>
								<td width="80%">&nbsp;</td>
							</tr>
							
						<? if ($suspendID): ?>
							<tr valign="top">
								<td class="tabletext" colspan="2"><a href="javascript:void(0)" id="HistoryNotify" onClick="show_history_push_message()"><?=$Lang['eGuidance']['PushMessage']['ViewStatus']?></a></td>
							</tr>
						<? endif;?>
							
						</table>
					</td>
				</tr>
				<tr>
					<td height="1" class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
				</tr>
				<tr>
					<td align="right">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="center">
									<span>
									<?= $linterface->GET_ACTION_BTN($button_submit, "button","","btnSubmit", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn")?>&nbsp;
									<?= $linterface->GET_ACTION_BTN($Lang['eGuidance']['SubmitAndNotifyUpdate'], "button", "notify_colleague_update('SuspendID')","btnSubmitAndNotify", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn")?>&nbsp;
									<?= $linterface->GET_ACTION_BTN($button_reset, "reset","","btnReset", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn")?>&nbsp;
									<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='index.php'","btnCancel", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn")?>
									</span></td>
							</tr>
						</table>
					</td>
				</tr>
			</table><br>
			
		</td>
	</tr>
</table>
<input type="hidden" id="SuspendID" name="SuspendID" value="<?=$suspendID?>">
<input type="hidden" id="hidNotifierID" name="hidNotifierID" value="">
<input type="hidden" id="submitMode" name="submitMode" value="submit">
<?php if ($form_action == 'edit_update.php'):?>
	<input type="hidden" id="StudentID" name="StudentID" value="<?php echo $rs_suspend['StudentID'];?>">
<?php endif;?>

</form>

<?=$libguidance_ui->getAttachmentUploadForm('Suspend',$suspendID)?>