<?php
/*
 * modifying:
 *
 * Log
 *
 * 2018-05-25 [Cameron]
 * - retrieve student info and disable student selection for edit [case #M138226]
 *
 * 2017-08-04 [Cameron]
 * - add IsConfidential and ConfidentialViewerID
 * - don't allow to edit if User is not in ConfidentialViewerID list of the record
 *
 * 2017-07-20 [Cameron]
 * - fix bug: should redirect to listview page if SuspendID is empty
 *
 * 2017-03-03 [Cameron]
 * - create this file
 *
 */
$PATH_WRT_ROOT = "../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "lang/eGuidance_lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libclass.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/eGuidance/libguidance.php");
include_once ($PATH_WRT_ROOT . "includes/eGuidance/libguidance_ui.php");

intranet_auth();
intranet_opendb();

$libguidance = new libguidance();
$permission = $libguidance->getUserPermission();
if (! $permission['admin'] && ! in_array('MGMT', (array) $permission['current_right']['MGMT']['SUSPEND'])) {
    $libguidance->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

if (is_array($SuspendID)) {
    $suspendID = (count($SuspendID) == 1) ? $SuspendID[0] : '';
} else {
    $suspendID = $SuspendID;
}

if (! $suspendID) {
    header("location: index.php");
}

$libguidance_ui = new libguidance_ui();
$linterface = new interface_html();
$lclass = new libclass();
$classSelection = $lclass->getSelectClass("name='ClassName' id='ClassName' ");
$studentSelection = getSelectByArray(array(), "name='StudentID' id='StudentID'");

$rs_suspend = $libguidance->getSuspend($suspendID);

if (count($rs_suspend) == 1) {
    $rs_suspend = $rs_suspend[0];
    $rs_suspend['Teacher'] = $libguidance->getSuspendTeacher($suspendID);
    
    if ($rs_suspend['IsConfidential']) {
        $confidentialViewerID = $rs_suspend['ConfidentialViewerID'];
        if (! in_array($_SESSION['UserID'], explode(',', $confidentialViewerID))) {
            header("location: index.php");
        }
        
        if ($confidentialViewerID) {
            $cond = ' AND UserID IN(' . $confidentialViewerID . ')';
            $rs_suspend['ConfidentialViewerID'] = $libguidance->getTeacher('-1', $cond);
        }
    }
    
    $rs_class_name = $libguidance->getClassByStudentID($rs_suspend['StudentID']);
    if (! empty($rs_class_name)) {
        // $classSelection = $lclass->getSelectClass("name='ClassName' id='ClassName' ",$rs_class_name);
        // $studentSelection = $libguidance->getStudentNameListWClassNumberByClassName($rs_class_name);
        // $studentSelection = getSelectByArray($studentSelection, "name='StudentID' id='StudentID'", $rs_suspend['StudentID']);
        $studentInfo = $libguidance->getStudentNameListWClassNumberByClassName($rs_class_name, array(
            $rs_suspend['StudentID']
        ), '', true);
        $studentInfo = count($studentInfo) ? $studentInfo[0]['StudentName'] : '';
    } else {
        $studentInfo = $libguidance->getStudent($rs_suspend['StudentID']);
        $studentInfo = count($studentInfo) ? $studentInfo[0]['Name'] : '';
    }
} else {
    header("location: index.php");
}

// menu highlight setting
$CurrentPageArr['eGuidance'] = 1;
$CurrentPage = "PageSuspend";
$CurrentPageName = $Lang['eGuidance']['name'];

// ## Title ###
$TAGS_OBJ = array();
$TAGS_OBJ[] = array(
    $Lang['eGuidance']['menu']['Management']['Suspend']
);

$MODULE_OBJ = $libguidance->GET_MODULE_OBJ_ARR();
$PAGE_NAVIGATION[] = array(
    $Lang['eGuidance']['menu']['Management']['Suspend'],
    "index.php"
);
$PAGE_NAVIGATION[] = array(
    $Lang['Btn']['Edit'],
    ""
);

$linterface->LAYOUT_START();

$form_action = "edit_update.php";
include ("suspend.tmpl.php");

$linterface->LAYOUT_STOP();
intranet_closedb();

?>


