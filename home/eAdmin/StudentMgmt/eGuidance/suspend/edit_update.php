<?php
/*
 * 	modifying:
 * 
 * 	Log
 * 
 *	2017-08-04 [Cameron]
 *		- handle submitAndNotify (send 'update' notification to colleague)
 * 		- add IsConfidential and ConfidentialViewerID
 *
 * 	2017-07-24 [Cameron]
 * 		- add field EndDate
 * 
 * 	2017-05-19 [Cameron]
 * 		- apply standardizeFormPostValue() to text field
 * 
 * 	2017-03-03 [Cameron]
 * 		- create this file
 * 
 */

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/eGuidance_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/libguidance.php");

intranet_auth();
intranet_opendb();

$libguidance = new libguidance();
$permission = $libguidance->getUserPermission();
if (!$permission['admin'] && !in_array('MGMT', (array)$permission['current_right']['MGMT']['SUSPEND'])) {
	$libguidance->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$ldb = new libdb();
$result = array();
	
$dataAry['StudentID'] = $StudentID;
$dataAry['SuspendType'] = $SuspendType;
$dataAry['StartDate'] = $StartDate;
$dataAry['EndDate'] = $EndDate;
$dataAry['Result'] = standardizeFormPostValue($Result);
$dataAry['IsConfidential'] = $IsConfidential;
if ($IsConfidential && !in_array($_SESSION['UserID'], (array)$ConfidentialViewerID)) {
	$ConfidentialViewerID[] = $_SESSION['UserID'];				// User who add the record are granted permission as default 
}
$dataAry['ConfidentialViewerID'] = implode(",",(array)$ConfidentialViewerID);
$dataAry['LastModifiedBy'] = $_SESSION['UserID'];
$sql = $libguidance->UPDATE2TABLE('INTRANET_GUIDANCE_SUSPEND',$dataAry,array('SuspendID'=>$SuspendID),false);

$ldb->Start_Trans();
## step 1: update to suspend
$result[]  = $ldb->db_db_query($sql);

## step 2: update suspend teacher
$sql = "DELETE FROM INTRANET_GUIDANCE_SUSPEND_TEACHER WHERE SuspendID='".$SuspendID."'";
if (count($TeacherID)) {
	$sql .= " AND TeacherID NOT IN ('".implode("','",(array)$TeacherID)."')";	
}
$result[] = $ldb->db_db_query($sql);
	
foreach((array)$TeacherID as $id) {
	unset($dataAry);
	$dataAry['SuspendID'] = $SuspendID;
	$dataAry['TeacherID'] = $id;
	$sql = $libguidance->INSERT2TABLE('INTRANET_GUIDANCE_SUSPEND_TEACHER',$dataAry,array(),false,true,false);	// insert ignore
	$result[] = $ldb->db_db_query($sql);
}

## step 3: update attachment
if (count($FileID) > 0) {
	$fileID = implode("','",$FileID);
	$sql = "UPDATE INTRANET_GUIDANCE_ATTACHMENT SET RecordID='".$SuspendID."' WHERE FileID IN ('".$fileID."') AND RecordID=0";
	$result[] = $ldb->db_db_query($sql);
}

if (!in_array(false,$result)) {
	$ldb->Commit_Trans();

	if ($submitMode == 'submitAndNotify') {
		$hidNotifierID = explode(',',$hidNotifierID);
		$notifyResult = $libguidance->sendNotifyToColleague($SuspendID, $recordType='SuspendID', $hidNotifierID);
		$returnMsgKey = $notifyResult ? 'UpdateAndNotifySuccess' : 'UpdateSuccessNotifyFail';
	}
	else {
		$returnMsgKey = 'UpdateSuccess';
	}
}
else {
	$ldb->RollBack_Trans();
	$returnMsgKey = 'UpdateUnsuccess';
}
	
header("location: index.php?returnMsgKey=".$returnMsgKey);


intranet_closedb();

?>
