<?php
/*
 * 	modifying:
 * 
 * 	Log
 * 
 * 	2017-02-21 [Cameron]
 * 		- create this file
 * 
 */

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/eGuidance_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/libguidance.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

intranet_auth();
intranet_opendb();

$libguidance = new libguidance();
$permission = $libguidance->getUserPermission();
if (!$permission['admin'] && !in_array('MGMT', (array)$permission['current_right']['MGMT']['CONTACT'])) {
	$libguidance->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$ldb = new libdb();
$lfs = new libfilesystem();
$result = array();

if (count($ContactID) > 0) {
	$ldb->Start_Trans();
	// delete contact
	$sql = "DELETE FROM INTRANET_GUIDANCE_CONTACT WHERE ContactID IN ('".implode("','",$ContactID)."')";
	$result[] = $ldb->db_db_query($sql);

	$sql = "DELETE FROM INTRANET_GUIDANCE_CONTACT_TEACHER WHERE ContactID IN ('".implode("','",$ContactID)."')";
	$result[] = $ldb->db_db_query($sql);
		
	$sql = "SELECT 
				FileID, EncodeFileName 
			FROM 
				INTRANET_GUIDANCE_ATTACHMENT
			WHERE 
				Target='Contact'
			AND	RecordID IN ('".implode("','",$ContactID)."')";
	$rs = $ldb->returnResultSet($sql);
		
	if (count($rs) > 0) {
		foreach((array)$rs as $r) {
			$FileID = $r['FileID'];
			$EncodeFileName = $r['EncodeFileName'];
			$file = $libguidance->eg_path.$EncodeFileName;
			$result[] = $lfs->file_remove($file);
		}

		$sql = "DELETE FROM INTRANET_GUIDANCE_ATTACHMENT WHERE Target='Contact' AND RecordID IN ('".implode("','",$ContactID)."')";
		$result[] = $ldb->db_db_query($sql);
	}
}

if (!in_array(false,$result)) {
	$ldb->Commit_Trans();
	$returnMsgKey = 'DeleteSuccess';
}
else {
	$ldb->RollBack_Trans();
	$returnMsgKey = 'DeleteUnsuccess';
}

header("location: index.php?returnMsgKey=".$returnMsgKey);

intranet_closedb();

?>


