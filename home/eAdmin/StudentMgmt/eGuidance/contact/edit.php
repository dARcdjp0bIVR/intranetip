<?php
/*
 * modifying:
 *
 * Log
 *
 * 2018-05-25 [Cameron]
 * - retrieve student info and disable student selection for edit [case #M138226]
 *
 * 2017-08-03 [Cameron]
 * - add IsConfidential and ConfidentialViewerID
 * - don't allow to edit if User is not in ConfidentialViewerID list of the record
 *
 * 2017-07-20 [Cameron]
 * - fix bug: should redirect to listview page if ContactID is empty
 *
 * 2017-02-21 [Cameron]
 * - create this file
 *
 */
$PATH_WRT_ROOT = "../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "lang/eGuidance_lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libclass.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/eGuidance/guidance_conf.php");
include_once ($PATH_WRT_ROOT . "includes/eGuidance/libguidance.php");
include_once ($PATH_WRT_ROOT . "includes/eGuidance/libguidance_ui.php");

intranet_auth();
intranet_opendb();

$libguidance = new libguidance();
$permission = $libguidance->getUserPermission();
if (! $permission['admin'] && ! in_array('MGMT', (array) $permission['current_right']['MGMT']['CONTACT'])) {
    $libguidance->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

if (is_array($ContactID)) {
    $contactID = (count($ContactID) == 1) ? $ContactID[0] : '';
} else {
    $contactID = $ContactID;
}

if (! $contactID) {
    header("location: index.php");
}

$libguidance_ui = new libguidance_ui();
$linterface = new interface_html();
$lclass = new libclass();

$studentSelection = '';
$classSelection = '';

$rs_contact = $libguidance->getContact($contactID);

if (count($rs_contact) == 1) {
    $rs_contact = $rs_contact[0];
    $rs_contact['Teacher'] = $libguidance->getContactTeacher($contactID);
    
    if ($rs_contact['IsConfidential']) {
        $confidentialViewerID = $rs_contact['ConfidentialViewerID'];
        if (! in_array($_SESSION['UserID'], explode(',', $confidentialViewerID))) {
            header("location: index.php");
        }
        
        if ($confidentialViewerID) {
            $cond = ' AND UserID IN(' . $confidentialViewerID . ')';
            $rs_contact['ConfidentialViewerID'] = $libguidance->getTeacher('-1', $cond);
        }
    }
    
    $rs_class_name = $libguidance->getClassByStudentID($rs_contact['StudentID']);
    if (! empty($rs_class_name)) {
        // $classSelection = $lclass->getSelectClass("name='ClassName' id='ClassName' ",$rs_class_name);
        // $studentSelection = $libguidance->getStudentNameListWClassNumberByClassName($rs_class_name);
        // $studentSelection = getSelectByArray($studentSelection, "name='StudentID' id='StudentID'", $rs_contact['StudentID']);
        $studentInfo = $libguidance->getStudentNameListWClassNumberByClassName($rs_class_name, array(
            $rs_contact['StudentID']
        ), '', true);
        $studentInfo = count($studentInfo) ? $studentInfo[0]['StudentName'] : '';
    } else {
        $studentInfo = $libguidance->getStudent($rs_contact['StudentID']);
        $studentInfo = count($studentInfo) ? $studentInfo[0]['Name'] : '';
    }
} else {
    header("location: index.php");
}

// menu highlight setting
$CurrentPageArr['eGuidance'] = 1;
$CurrentPage = "PageContact";
$CurrentPageName = $Lang['eGuidance']['name'];

// ## Title ###
$TAGS_OBJ = array();
$TAGS_OBJ[] = array(
    $Lang['eGuidance']['menu']['Management']['Contact']
);

$MODULE_OBJ = $libguidance->GET_MODULE_OBJ_ARR();
$PAGE_NAVIGATION[] = array(
    $Lang['eGuidance']['menu']['Management']['Contact'],
    "index.php"
);
$PAGE_NAVIGATION[] = array(
    $Lang['Btn']['Edit'],
    ""
);

$linterface->LAYOUT_START();

$form_action = "edit_update.php";
include ("contact.tmpl.php");

$linterface->LAYOUT_STOP();
intranet_closedb();

?>


