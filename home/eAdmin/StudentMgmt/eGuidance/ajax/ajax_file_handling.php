<?php
/*
 * 	Log
 * 
 * 	2017-02-20 [Cameron] create this file
 * 	
 */

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/eGuidance_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/libguidance.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/libguidance_ui.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

intranet_auth();
intranet_opendb();

$libguidance = new libguidance();
$permission = $libguidance->getUserPermission();
if (!$permission['admin'] && !$permission['current_right']) {
	$libguidance->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$libguidance_ui = new libguidance_ui();
$linterface = new interface_html();
$ldb = new libdb();
$lfs = new libfilesystem();

$action = $_GET['action']? $_GET['action']: $_POST['action'];
$RecordID  = (isset($_POST['RecordID']) && $_POST['RecordID'] != "") ? $_POST['RecordID'] : "";
$Target  = (isset($_POST['Target']) && $_POST['Target'] != "") ? $_POST['Target'] : "";
$BatchUploadTime = $_POST['BatchUploadTime'] ? $_POST['BatchUploadTime'] : date('Y-m-d H:i:s');


switch($action){
	case "uploadAttachment":
		$today = date('Ymd');		
		if (!is_dir($libguidance->eg_path.'d'.$today))
		{
		    $lfs->folder_new($libguidance->eg_path.'d'.$today);
		}
	
		$file 		= $_FILES['attachment'];
		$file_name	= str_replace(array("#"," ","'",'"',"&","?"),"_",stripslashes($file['name']));

		## Check file type
		if(!preg_match("/.\../",$file['name'])){
		?>
			<script language="javascript">
			alert('<?=$Lang['eGuidance']['Warning']['IllegalFileType']?>');
			window.parent.document.getElementById("attachment").innerHTML="";
			</script>
		<?
			intranet_closedb();
			exit;
		}

		if ($file_name != '') {
			$sql = "INSERT INTO INTRANET_GUIDANCE_ATTACHMENT 
						(UploadTime, OriginalFileName, BatchUploadTime, LastModifiedBy) 
					VALUES 
						(NOW(),'".$ldb->Get_Safe_Sql_Query($file_name)."','".$BatchUploadTime."','".$_SESSION['UserID']."')";
				 
			$ldb->db_db_query($sql);
			$insert_id = $ldb->db_insert_id();
			$EncodeFilePath = "d$today/".session_id().encrypt_string($insert_id.date('YmdHis'));	
			$eg_file_path = $libguidance->eg_path.$EncodeFilePath;
			if (!is_dir($eg_file_path))
			{
			    $lfs->folder_new($eg_file_path);
			}
			$EncodeFileName = $EncodeFilePath . '/'. $file_name;
			$sql = "UPDATE INTRANET_GUIDANCE_ATTACHMENT SET EncodeFileName = '".$EncodeFileName."'";
			if ($RecordID) {
				$sql .= ",RecordID='".$RecordID."'";
			}
			if ($Target) {
				$sql .= ",Target='".$Target."'";
			}
			
			$sql .= " WHERE FileID = '".$insert_id."'";
			$ldb->db_db_query($sql);
			
			$des = $libguidance->eg_path.$EncodeFileName;
			if (!is_file($file["tmp_name"])) {
				$sql = "DELETE FROM INTRANET_GUIDANCE_ATTACHMENT WHERE FileID='".$insert_id."'"; 
				$ldb->db_db_query($sql);
			}
			else {
			  	if(strpos($file_name, ".")==0){
			  		// do nothing	
			  	}
			  	else{
			  		move_uploaded_file($file["tmp_name"], $des);
			  	}
			}
			// get all uploaded files by the user of a batch
			$AttachList = $libguidance->geteGuidanceAttachment($UserID,$Target,$RecordID,$BatchUploadTime);
			$attachment_layout = $libguidance_ui->geteGuidanceAttachFileList($AttachList,true);	// 2nd para true: pass for script
			
			if ($callback){
			    echo "<script>$callback('".$attachment_layout."','$BatchUploadTime');</script>";
			}
		}
	break;
	
	case "removeAttachment":
		# Get File to delete
		$sql = "SELECT 
					FileID, EncodeFileName 
				FROM 
					INTRANET_GUIDANCE_ATTACHMENT
				WHERE 
					FileID='".$FileID."'";
		$rs = $ldb->returnResultSet($sql);
		
		if (sizeof($rs) > 0) {
			$FileID = $rs[0]['FileID'];
			$EncodeFileName = $rs[0]['EncodeFileName'];
			$file = $libguidance->eg_path.$EncodeFileName;
			$result[] = $lfs->file_remove($file);
			
			$sql = "DELETE FROM INTRANET_GUIDANCE_ATTACHMENT WHERE FileID='".$FileID."'";
			$result[] = $ldb->db_db_query($sql);
		}
		
		$AttachList = $libguidance->geteGuidanceAttachment($UserID,$Target,$RecordID,$BatchUploadTime);
		$attachment_layout = $libguidance_ui->geteGuidanceAttachFileList($AttachList);
			
		echo $attachment_layout;
	break;
	
}

intranet_closedb(); 
?>