<?php
/*
 * 	modifying:
 * 
 * 	Log
 *
 *  2018-05-25 [Cameron]
 *      - show student name without class
 *      
 * 	2017-07-28 [Cameron]
 * 		- add lock icon to indicate confidential record, don't allow to view if User is not in ConfidentialViewerID list of the record
 * 
 * 	2017-07-21 [Cameron]
 * 		- add FollowupAdvice field
 * 		- fix bug: should redirect to listview page if GuidanceID is empty
 * 
 * 	2017-03-21 [Cameron]
 * 		- create this file
 * 
 */

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/eGuidance_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/guidance_conf.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/libguidance.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/libguidance_ui.php");

intranet_auth();
intranet_opendb();

$libguidance = new libguidance();
$permission = $libguidance->getUserPermission();
if (!$permission['admin'] && !$permission['current_right']['MGMT']['GUIDANCE']) {
	$libguidance->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

if (!$GuidanceID) {
	header("location: index.php");
}

$libguidance_ui = new libguidance_ui();
$linterface = new interface_html();

$showOther = false;
$showFollowupAdviceOther = false;
$teacherInfo = '';
$studentInfo = '';
$guidanceID = $GuidanceID;

$rs_guidance = $libguidance->getGuidance($guidanceID);

if (count($rs_guidance) == 1) {
	$rs_guidance = $rs_guidance[0];
	$caseTypeAry = explode('^~',$rs_guidance['CaseType']);
	foreach((array)$caseTypeAry as $k=>$v) {
		if (strlen($v)>6 && substr($v,0,7)=="Other^:") {
			$caseTypeAry[$k] = 'Other';
			$caseTypeOther = substr($v,7);
			$showOther = true;
		}
	}
	
	$followupAdviceAry = explode('^~',$rs_guidance['FollowupAdvice']);
	foreach((array)$followupAdviceAry as $k=>$v) {
		if (strlen($v)>6 && substr($v,0,7)=="Other^:") {
			$followupAdviceAry[$k] = 'Other';
			$followupAdviceOther = substr($v,7);
			$showFollowupAdviceOther = true;
		}
	}
	
	$rs_guidance['Teacher'] = $libguidance->getGuidanceTeacher($guidanceID);
	$teacherInfo = $libguidance_ui->getUserNameList($rs_guidance['Teacher']);
	
	$confidentialViewerInfo = '';
	if ($rs_guidance['IsConfidential']) {
		$confidentialViewerID = $rs_guidance['ConfidentialViewerID'];
		if (!in_array($_SESSION['UserID'],explode(',',$confidentialViewerID))) {
			header("location: index.php");
		}
		
		if ($confidentialViewerID) {
			$cond = ' AND UserID IN('.$confidentialViewerID.')';
			$confidentialViewerID = $libguidance->getTeacher('-1',$cond);
			$confidentialViewerInfo = $libguidance_ui->getUserNameList($confidentialViewerID);
		}
	}

	$rs_class_name = $libguidance->getClassByStudentID($rs_guidance['StudentID']);
	if (!empty($rs_class_name)) {
		$studentInfo = $libguidance->getStudentNameListWClassNumberByClassName($rs_class_name,array($rs_guidance['StudentID']),'',true);
		$studentInfo = count($studentInfo) ? $studentInfo[0]['StudentName'] : '';
	}
	else {
	    $studentInfo = $libguidance->getStudent($rs_guidance['StudentID']);
	    $studentInfo = count($studentInfo) ? $studentInfo[0]['Name'] : '';
	}
}
else {
	header("location: index.php");
}

# menu highlight setting
$CurrentPageArr['eGuidance'] = 1;
$CurrentPage = "PageGuidance";
$CurrentPageName = $Lang['eGuidance']['name'];

### Title ###
$TAGS_OBJ = array();
$TAGS_OBJ[] = array($Lang['eGuidance']['menu']['Management']['Guidance']);

$MODULE_OBJ = $libguidance->GET_MODULE_OBJ_ARR();
$PAGE_NAVIGATION[] = array($Lang['eGuidance']['menu']['Management']['Guidance'], "index.php");
$PAGE_NAVIGATION[] = array($Lang['Btn']['View'], "");

$linterface->LAYOUT_START();
	
?>
<?=$linterface->Include_Thickbox_JS_CSS();?>
<script type="text/javascript">
<?include($PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eGuidance/common_js.php");?>
</script>

<table id="main_table" width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td class="navigation"><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
				</tr>
			</table>
			
			<table width="88%" border="0" cellpadding="5" cellspacing="0">
				<tr>
					<td>
						<table align="center" class="form_table_v30">
							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['Identity']['Student']?></td>
								<td class="tabletext"><?=$studentInfo?> <?=$linterface->GET_SMALL_BTN($Lang['eGuidance']['personal']['ViewPersonalRecord'],'button','ViewStudentInfo();','BtnViewStudentInfo')?></td>								
							</tr>
						
							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['eGuidance']['guidance']['CaseType']?></td>
								<td class="tabletext">
								<?
									$guidance_type = $libguidance->getGuidanceSettingItem('Guidance','CaseType');
									$x = '';									
									foreach((array)$guidance_type as $k=>$v) {
										$x .= '<input type="checkbox" name="CaseType[]" id="CaseType_'.$k.'" disabled value="'.$k.'"'. (in_array($k,(array)$caseTypeAry)?'checked':'').'>'.$v;
										$x .= ($k == 'Other') ? '':'<br>';  
									}
									echo $x;
									if ($showOther) {
										echo ':'.intranet_htmlspecialchars($caseTypeOther);
									}
								?>
								</td>
							</tr>
							
							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['eGuidance']['guidance']['ContactDate']?></td>
								<td class="tabletext"><?=((empty($rs_guidance['ContactDate']) || ($rs_guidance['ContactDate'] == '0000-00-00'))?'-':$rs_guidance['ContactDate'])?> &nbsp;<span><?=$Lang['eGuidance']['guidance']['ContactTime']?></span>
									<?=empty($rs_guidance['ContactTime'])?'-':substr($rs_guidance['ContactTime'],0,5)?>
									</td>
							</tr>
							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['eGuidance']['guidance']['ContactPlace']?></td>
								<td class="tabletext"><?=empty($rs_guidance['ContactPlace']) ? '-' : intranet_htmlspecialchars($rs_guidance['ContactPlace'])?></td>
							</tr>
							
							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['eGuidance']['TeacherName']?></td>
								<td class="tabletext"><?=$teacherInfo?></td>
							</tr>

							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['eGuidance']['Confidential']?><img src="/images/<?=$LAYOUT_SKIN?>/icalendar/icon_lock_own.gif" style="display:<?=($rs_guidance['IsConfidential']?'':'none')?>"></td>
								<td class="tabletext">
									<input type="radio" name="IsConfidential" id="IsConfidentialYes" value="1"<?=$rs_guidance['IsConfidential']?' checked':''?> disabled><label for="IsConfidentialYes"><?=$Lang['General']['Yes']?></label>
									<input type="radio" name="IsConfidential" id="IsConfidentialNo" value="0"<?=$rs_guidance['IsConfidential']?'':' checked'?> disabled><label for="IsConfidentialNo"><?=$Lang['General']['No']?></label>
									
									<div id="ConfidentialSection">
										<?=$confidentialViewerInfo?>
									</div>
								</td>
							</tr>
							
							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['eGuidance']['guidance']['ContactPurpose']?></td>
								<td class="tabletext"><?=empty($rs_guidance['ContactPurpose']) ? '-' : nl2br($rs_guidance['ContactPurpose'])?></td>
							</tr>

							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['eGuidance']['guidance']['ContactContent']?></td>
								<td class="tabletext"><?=empty($rs_guidance['ContactContent']) ? '-' : nl2br($rs_guidance['ContactContent'])?></td>
							</tr>

							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['eGuidance']['guidance']['ContactFinding']?></td>
								<td class="tabletext"><?=empty($rs_guidance['ContactFinding']) ? '-' : nl2br($rs_guidance['ContactFinding'])?></td>
							</tr>
							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['eGuidance']['guidance']['ContactStage']?></td>
								<td class="tabletext">
<?
	$contact_stage = $libguidance->getGuidanceSettingItem('Guidance','ContactStage');
	$nrStage = count($contact_stage);
	$i=1;
	foreach((array)$contact_stage as $k=>$v) {
		echo '<input type="radio" name="ContactStage" id="ContactStage_'.$k.'" disabled value="'.$k.'" '.($rs_guidance['ContactStage']==$k?'checked':'').'>'.$v;
		if ($i < $nrStage) {
			echo '<br>';
		}
		$i++;
	}
?>								
								</td>
							</tr>

							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['eGuidance']['FollowupAdvice']?></td>
								<td class="tabletext">
								<?
									$followup_advice = $libguidance->getGuidanceSettingItem('Guidance','FollowupAdvice');
									$x = '';
									foreach((array)$followup_advice as $k=>$v) {
										if ($k == 'Other') {
											$x .= '<input type="radio" name="FollowupAdvice" id="FollowupAdvice_'.$k.'" disabled value="'.$k.'"'. (!empty($rs_guidance['FollowupAdvice']) && substr($rs_guidance['FollowupAdvice'],0,5)==$k?'checked':'').'>';
											$x .= '<label for="FollowupAdvice_'.$k.'">'.$v.'</label>';
										}
										else {
											$x .= '<input type="radio" name="FollowupAdvice" id="FollowupAdvice_'.$k.'" disabled value="'.$k.'"'. ($rs_guidance['FollowupAdvice']==$k?'checked':'').'>';
											$x .= '<label for="FollowupAdvice_'.$k.'">'.$v.'</label><br>';
										}										  
									}
									echo $x;
									
									if ($showFollowupAdviceOther) {
										echo ':'.intranet_htmlspecialchars($followupAdviceOther);
									}
								?>
								</td>
							</tr>
														
							<?=$libguidance_ui->getAttachmentLayout('Guidance',$guidanceID,'view')?>
							
						</table>
					</td>
				</tr>
				<tr>
					<td height="1" class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
				</tr>
				<tr>
					<td align="right">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="center">
							<? 	if ($permission['admin'] || in_array('MGMT', (array)$permission['current_right']['MGMT']['GUIDANCE'])) {
									echo $linterface->GET_ACTION_BTN($button_edit, "button", "window.location='edit.php?GuidanceID=$guidanceID'").'&nbsp';
								}
								echo $linterface->GET_ACTION_BTN($button_back, "button", "window.location='index.php'");							
							?>									
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table><br>
			
		</td>
	</tr>
</table>

<input type="hidden" name="StudentID" id="StudentID" value="<?=$rs_guidance['StudentID']?>">

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>