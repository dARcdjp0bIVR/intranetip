<?php
/*
 * 	modifying:
 * 
 * 	Log
 * 
 *	2017-08-03 [Cameron]
 *		- handle submitAndNotify (send 'update' notification to colleague)
 *
 * 	2017-07-28 [Cameron]
 * 		- add IsConfidential and ConfidentialViewerID
 * 
 * 	2017-07-21 [Cameron]
 * 		- add FollowupAdvice field
 * 
 * 	2017-05-19 [Cameron]
 * 		- apply standardizeFormPostValue() to text field
 * 
 * 	2017-02-28 [Cameron]
 * 		- create this file
 * 
 */

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/eGuidance_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/libguidance.php");

intranet_auth();
intranet_opendb();

$libguidance = new libguidance();
$permission = $libguidance->getUserPermission();
if (!$permission['admin'] && !in_array('MGMT', (array)$permission['current_right']['MGMT']['GUIDANCE'])) {
	$libguidance->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$ldb = new libdb();
$result = array();

$dataAry['StudentID'] = $StudentID;
$caseType = implode("^~",(array)$CaseType);
if (in_array('Other',(array)$CaseType)) {
	$caseType .= "^:".$CaseTypeOther;
}
$dataAry['CaseType'] = standardizeFormPostValue($caseType);

$followupAdvice = $FollowupAdvice;
if ($followupAdvice == 'Other') {
	$followupAdvice .= "^:".$FollowupAdviceOther;
}
$dataAry['FollowupAdvice'] = standardizeFormPostValue($followupAdvice);

$dataAry['ContactDate'] = $ContactDate;
$contactTime = $ContactTimeHour.':'.$ContactTimeMin.':00';
$dataAry['ContactTime'] = $contactTime;
$dataAry['ContactPlace'] = standardizeFormPostValue($ContactPlace);
$dataAry['ContactPurpose'] = standardizeFormPostValue($ContactPurpose);
$dataAry['ContactContent'] = standardizeFormPostValue($ContactContent);
$dataAry['ContactFinding'] = standardizeFormPostValue($ContactFinding);
$dataAry['ContactStage'] = $ContactStage;
$dataAry['IsConfidential'] = $IsConfidential;
if ($IsConfidential && !in_array($_SESSION['UserID'], (array)$ConfidentialViewerID)) {
	$ConfidentialViewerID[] = $_SESSION['UserID'];				// User who add the record are granted permission as default 
}
$dataAry['ConfidentialViewerID'] = implode(",",(array)$ConfidentialViewerID);
$dataAry['LastModifiedBy'] = $_SESSION['UserID'];

$sql = $libguidance->UPDATE2TABLE('INTRANET_GUIDANCE_GUIDANCE',$dataAry,array('GuidanceID'=>$GuidanceID),false);

$ldb->Start_Trans();
## step 1: update guidance
$result[] = $ldb->db_db_query($sql);

## step 2: update guidance teacher
if (count($TeacherID)) {
	$sql = "DELETE FROM INTRANET_GUIDANCE_GUIDANCE_TEACHER WHERE GuidanceID='".$GuidanceID."' AND TeacherID NOT IN ('".implode("','",(array)$TeacherID)."')";
	$result[] = $ldb->db_db_query($sql);
}

foreach((array)$TeacherID as $id) {
	unset($dataAry);
	$dataAry['GuidanceID'] = $GuidanceID;
	$dataAry['TeacherID'] = $id;
	$sql = $libguidance->INSERT2TABLE('INTRANET_GUIDANCE_GUIDANCE_TEACHER',$dataAry,array(),false,true,false);	// insert ignore
	$result[] = $ldb->db_db_query($sql);
}

## step 3: update attachment
if (count($FileID) > 0) {
	$fileID = implode("','",$FileID);
	$sql = "UPDATE INTRANET_GUIDANCE_ATTACHMENT SET RecordID='".$GuidanceID."' WHERE FileID IN ('".$fileID."') AND RecordID=0";
	$result[] = $ldb->db_db_query($sql);
}

if (!in_array(false,$result)) {
	$ldb->Commit_Trans();
	
	if ($submitMode == 'submitAndNotify') {
		$hidNotifierID = explode(',',$hidNotifierID);
		$notifyResult = $libguidance->sendNotifyToColleague($GuidanceID, $recordType='GuidanceID', $hidNotifierID);
		$returnMsgKey = $notifyResult ? 'UpdateAndNotifySuccess' : 'UpdateSuccessNotifyFail';
	}
	else {
		$returnMsgKey = 'UpdateSuccess';
	}
}
else {
	$ldb->RollBack_Trans();
	$returnMsgKey = 'UpdateUnsuccess';
}

header("location: index.php?returnMsgKey=".$returnMsgKey);


intranet_closedb();

?>


