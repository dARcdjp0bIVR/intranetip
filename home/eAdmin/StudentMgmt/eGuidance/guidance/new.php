<?php
/*
 * 	modifying:
 * 
 * 	Log
 * 
 * 	2017-02-25 [Cameron]
 * 		- create this file
 * 
 */

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/eGuidance_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/guidance_conf.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/libguidance.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/libguidance_ui.php");

intranet_auth();
intranet_opendb();

$libguidance = new libguidance();
$permission = $libguidance->getUserPermission();
if (!$permission['admin'] && !in_array('MGMT', (array)$permission['current_right']['MGMT']['GUIDANCE'])) {
	$libguidance->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$libguidance_ui = new libguidance_ui();
$linterface = new interface_html();

$lclass = new libclass();
$classSelection = $lclass->getSelectClass("name='ClassName' id='ClassName' ");
$studentSelection = getSelectByArray(array(),"name='StudentID' id='StudentID'");

## initialize var
$showOther = false;	
$showFollowupAdviceOther = false;
$caseTypeOther = '';
$followupAdviceOther ='';
$caseTypeAry = array();
$followupAdviceAry = array();
$rs_guidance = array();
$guidanceID = '';


# menu highlight setting
$CurrentPageArr['eGuidance'] = 1;
$CurrentPage = "PageGuidance";
$CurrentPageName = $Lang['eGuidance']['name'];

### Title ###
$TAGS_OBJ = array();
$TAGS_OBJ[] = array($Lang['eGuidance']['menu']['Management']['Guidance']);

$MODULE_OBJ = $libguidance->GET_MODULE_OBJ_ARR();
$PAGE_NAVIGATION[] = array($Lang['eGuidance']['menu']['Management']['Guidance'], "index.php");
$PAGE_NAVIGATION[] = array($Lang['Btn']['New'], "");

$linterface->LAYOUT_START();

	
$form_action = "new_update.php";
include("guidance.tmpl.php");

$linterface->LAYOUT_STOP();
intranet_closedb();

?>


