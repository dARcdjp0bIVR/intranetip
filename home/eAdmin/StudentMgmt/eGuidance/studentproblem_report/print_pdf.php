<?
/**
 * modifying: 
 * 
 * Change Log:
 * 
 * 2017-11-13	Cameron
 * 		- fix meta charset and footer
 *  
 * 2017-10-18  	Cameron
 *  	- Create this file
 */

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/eGuidance_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/guidance_conf.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/libguidance.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/libguidance_ui.php");
include_once($PATH_WRT_ROOT."includes/mpdf/mpdf.php");


######## Init START ########
intranet_auth();
intranet_opendb();

$lclass = new libclass();
$libguidance = new libguidance();
$libguidance_ui = new libguidance_ui();
$resultData = array();
$reportDetails = array();

define('TABLE_TYPE_EMPTY', 'tableEmpty');
define('TABLE_TYPE_HORIZONTAL', 'tableHorizontal');
define('TABLE_TYPE_VERTICAL', 'tableVertical');
define('EMPTY_REPLACEMENT','-');

//debug_pr($_POST);
//exit;

######## Init END ########


######## Access Right START ########
$permission = $libguidance->getUserPermission();
if (!$permission['admin'] && !in_array('VIEW', (array)$permission['current_right']['REPORTS']['STUDENTPROBLEMREPORT'])) {
	$libguidance->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}
######## Access Right END ########



######## Init PDF START ########
$mpdf = new mPDF('','A4',0,'',5,5,5,15);
//$mpdf->mirrorMargins = 1;
######## Init PDF END ########

######## Get Problem Title START ########
$personalFilter = $libguidance->getGuidanceSettingItem('Personal');
$categories = array_keys($personalFilter['Category']);

######## Get Problem Title END ########


######## Load all Data START ########
#### Load Header Data START ####
$school_name = $_SESSION["SSV_PRIVILEGE"]["school"]["name"];
$report_title = "{$Lang['eGuidance']['report']['eGuidanceReport']} - {$Lang['eGuidance']['menu']['Reports']['StudentProblemReport']}";

#### Load Header Data END ####

#### Get Student Problem Report START ####
$studentInfo = $libguidance->getStudentProblemReport($_POST);

#### Get Student Problem Report END ####

######## Load all Data END ########


######## Pack data START ########
/**
 * Demo data
$table = 
	array(
		array(
		    'StudentRegNo' 	=> '#1511041',
		    'ClassName'		=> '5B',
		    'ClassNumber'	=> '1',
		    'StudentName'	=> 'Sally Tang',
		    'data' 			=> array(
		    						array('Illness', 'Heart Disease'),
		                			array('CriminalOffense', 'Smuggling'),
		                			array('OtherProblem', 'Un-married mother')
		    					)
		    ),
		array(
		    'StudentRegNo' 	=> '#1511042',
		    'ClassName'		=> '5B',
		    'ClassNumber'	=> '2',
		    'StudentName'	=> 'Dona Liu',
		    'data' 			=> array(
			                		array('Illness', 'Headache'),
			                		array('OtherProblem', 'Unsettle Home')
    							)
    		)    					
	);
 */
foreach($studentInfo as $rs){
    $data = array();
	foreach((array)$categories as $k) {
		if ($rs["Prob$k"]) {		// e.g. $k='Health'
			${"prob".$k."Ary"} = explode('^~',$rs["Prob$k"]);
			if (count(${"prob".$k."Ary"})) {
				foreach((array)${"prob".$k."Ary"} as $kk=>$vv) {
					if (strlen($vv)>7 && substr($vv,0,7)=='Illness') {
						${"prob".$k."Ary"}[$kk] = 'Illness';
						list($item,$illness) = explode('^:',$vv);
						$data[] = array($personalFilter['Health']['Illness'],$illness);
					}
					else if (strlen($vv)>15 && substr($vv,0,15)=='CriminalOffense') {
						${"prob".$k."Ary"}[$kk] = 'CriminalOffense';
						list($item,$criminal) = explode('^:',$vv);
						$data[] = array($personalFilter['Behavior']['CriminalOffense'],$criminal);
					}
					else if (strlen($vv)>12 && substr($vv,0,12)=='OtherProblem') {
						${"prob".$k."Ary"}[$kk] = 'OtherProblem';
						list($item,$otherProblem) = explode('^:',$vv);
						$data[] = array($personalFilter['Other']['OtherProblem'],$otherProblem);
					}
					else {
						$data[] = array($personalFilter[$k][$vv],EMPTY_REPLACEMENT);
					}
				}
			}
		}
	}

	$data_array = array_chunk($data,20);
	foreach($data_array as $da) {
	    $table = array(
	        'StudentRegNo' 	=> $rs['WebSAMSRegNo'],
	        'ClassName'		=> $rs['ClassName'],
	        'ClassNumber'	=> $rs['ClassNumber'],
	        'StudentName'	=> $rs['StudentName'],
	        'data' 			=> $da);
	        
	    $resultData[] = $table;
	}
}
$countResultData = count($resultData);

$tableTitle = array( 
 	$Lang['eGuidance']['student_problem']['StudentRegNo'],
    $Lang['eGuidance']['Class'],
    $Lang['General']['ClassNumber'],
    $Lang['eGuidance']['student_problem']['StudentName'],
    $Lang['eGuidance']['student_problem']['Type'],
    $Lang['eGuidance']['student_problem']['AdditionalMaterial']
);

######## Pack data END ########

################################ Load header to PDF START ################################
ob_start();
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?=($junior_mck) ? 'big5-hkscs' : 'utf-8'?>" />
<title></title>
<style type="text/css">

body{color: #000;  font-family:  msjh !important; font-size:0.9em; line-height:0.9em; margin:0; padding:0; -webkit-print-color-adjust: exact; font-stretch: condensed; /*font-size-adjust: 0.5;*/}

/**** Header style START ****/
.schoolName, .reportType, .reportDetails{
    text-align: center;
}
.reportDetails{
    margin-top: 0;
}
.printTime{
    text-align: right;
}
/**** Header style END ****/


/**** Footer style START ****/
.footer{
    width: 100%;
}
.footer td{
    width: 33%;
    vertical-align: bottom; 
    font-family:  msjh !important;
    font-size: 8pt; 
    color: #000000; 
    font-weight: bold;
    padding: 3px;
    border: 0;
    border-top: 1px solid black;
}
/**** Footer style END ****/


/**** Basic table style START ****/
table{
    width: 200mm;
    border-collapse: collapse;
    margin-top: 3mm;
    overflow: visible;
}
.noRecordTd{
    text-align: center;
}

th, td{
    border: 1px solid #ddd;
    text-align: left;
    vertical-align: top;
    padding: 3mm;
}
tr.headerRow th{
    font-size: 1.5em;
    background-color: #487db4;
    color: white;
    text-align: center;
}

.field{
    text-align: center;
    white-space: nowrap;
    color: white;
    background-color: #59afc6;
}

.tableIndex td, .tableEmpty td{
    border: 0;
}
.tableEmpty td{
    font-size:1px !important;
}
.tableIndex td{
    padding-bottom: 0;
}
/**** Basic table style END ****/

/**** filter table style START ****/
.filter_table {
	border: 0;
}
.filter_table .indentCol {
	width: 10mm;
}

/**** filter table style END ****/

/**** student problem table style START ****/
.student_problem .col_1{
    width: 15mm;
}
.student_problem .col_2{
    width: 18mm;
}
.student_problem .col_3{
    width: 8mm;
}
.student_problem .col_4{
    width: 30mm;
}
.student_problem .col_5{
    width: 80mm;
}

/**** student problem style END ****/

</style>
</head>

<?php if($countResultData == 0): ?>
    <h2 class="schoolName"><?=$school_name ?></h2>
    <h3 class="reportType"><?=$report_title ?></h3>
    
    <hr style="margin-top: 0;margin-bottom: 3mm;"/>
<?php endif; ?>


<?php
$pageHeader = ob_get_clean();
if($_GET['debug']){
    echo $pageHeader;
}else{
    $mpdf->WriteHTML($pageHeader);
}
@ob_end_clean();
################################ Load header to PDF END ################################


################################ Load Data to PDF START ################################
if($countResultData):
######## Header START ########
        ob_start();
?>
        <h2 class="schoolName"><?=$school_name ?></h2>
        <h3 class="reportType"><?=$report_title ?></h3>
        
        <hr style="margin-top: 0;margin-bottom: 3mm;"/>
<?php
        $header = ob_get_clean();
        $mpdf->WriteHTML($header);
        @ob_end_clean();
######## Header END ########


######## Footer START ########
        ob_start();
?>
        <table class="footer">
        	<tr>
                <td><?=($junior_mck ? convert2unicode($Lang['eGuidance']['menu']['Reports']['StudentProblemReport'],true,1) : $Lang['eGuidance']['menu']['Reports']['StudentProblemReport'])?></td>
                <td style="text-align: center;"></td>
        		<td style="text-align: right;">{PAGENO}</td>
        	</tr>
        </table>
<?php
        $footer = ob_get_clean();
        @ob_end_clean();
######## Footer END ########

    
######## Content START ########
    ob_start();
    
	// filter    
?>

<table class="student_problem">
	<thead>
		<tr>
	<? foreach($tableTitle as $fieldIndex => $title):?>
			<th class="field col_<?=$fieldIndex+1 ?>"><?=$title?></th>
	<? endforeach;?>
		</tr>
	</thead>
	
    <tbody class="tableVertical">
  <? if (count($resultData)): ?>
    <? foreach((array)$resultData as $index => $table):
		$dataCount = count($table['data']);		
	?>
		<tr>
			<td <?=($dataCount > 1 ? 'rowspan="'.$dataCount.'"' : '')?> class="value col_1" valign="top"><?=$table['StudentRegNo']?></td>
			<td <?=($dataCount > 1 ? 'rowspan="'.$dataCount.'"' : '')?> class="value col_1" valign="top"><?=$table['ClassName']?></td>
			<td <?=($dataCount > 1 ? 'rowspan="'.$dataCount.'"' : '')?> class="value col_1" valign="top"><?=$table['ClassNumber']?></td>
			<td <?=($dataCount > 1 ? 'rowspan="'.$dataCount.'"' : '')?> class="value col_2" valign="top"><?=$table['StudentName']?></td>
	<?			
	    if($dataCount):
	        foreach($table['data'] as $dataIndex => $data):
	        	if ($dataIndex > 0) {
	        		echo '<tr>';
	        	} 
				foreach($data as $fieldIndex => $value): 
				    $css = ' col_' . ($fieldIndex+5);
	?>
            		<td class="value <?=$css?>">
            			<?=nl2br($value)?>
            		</td>
    <?			endforeach;
    
        		echo '</tr>';
        		
    	    endforeach;
    	else:
    		echo '<td></td><td></td></tr>';
    	endif;
    	 
   	   endforeach;
   	   
	 else:
	?>
		<tr>
	    	<td colspan="<?=count($tableTitle)?>" class="noRecordTd"><?=$Lang['General']['NoRecordAtThisMoment'] ?></td>
	    </tr>
   	<?  endif;?>
   	
	</tbody>
</table>

<?php 
    $page = ob_get_clean();
    if($_GET['debug']){
        echo $page;
    }else{
        $mpdf->SetHTMLFooter($footer);
        $mpdf->WriteHTML($page);
    }
    @ob_end_clean();

######## Content END ########


######## Print time START ########
ob_start();
?>

<h4 class="printTime"><?=$Lang['eGuidance']['report']['PrintTime']?>: <?=date('Y-m-d H:i:s') ?></h4>


<?php
        $page = ob_get_clean();
        if($_GET['debug']){
            echo $page;
        }else{
            $mpdf->SetHTMLFooter($footer);
            $mpdf->WriteHTML($page);
            if($hasPageBreak){
                $mpdf->WriteHTML('<pagebreak resetpagenum="1" suppress="off" />');
            }
        }
        @ob_end_clean();
######## Print time END ########

else:
    $mpdf->WriteHTML("<h1>{$Lang['General']['NoRecordAtThisMoment']}</h1>");
endif;
################################ Load Data to PDF END ################################



if(!$_GET['debug']){
    $mpdf->Output();
}
@ob_end_clean();

