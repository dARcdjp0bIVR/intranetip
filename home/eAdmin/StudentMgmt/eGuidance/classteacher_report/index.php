<?
/*
 * 	Log
 *
 * 	2017-09-15 [Cameron] create this file
 */

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/eGuidance_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/guidance_conf.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/libguidance.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");

intranet_auth();
intranet_opendb();

$libguidance = new libguidance();
$permission = $libguidance->getUserPermission();
if (!$permission['admin'] && !in_array('VIEW', (array)$permission['current_right']['REPORTS']['CLASSTEACHERREPORT'])) {
	$libguidance->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}
    
# Page heading setting
$linterface = new interface_html();
$CurrentPageArr['eGuidance'] = 1;
$CurrentPage = "PageClassteacherReport";
$CurrentPageName = $Lang['eGuidance']['name'];

### Title ###
$TAGS_OBJ = array();
$TAGS_OBJ[] = array($Lang['eGuidance']['menu']['Reports']['ClassteacherReport'], "", true);

$MODULE_OBJ = $libguidance->GET_MODULE_OBJ_ARR();
$returnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];
$linterface->LAYOUT_START($returnMsg);




############# start Filters

# Class Filter of Current Academic Year
$lclass = new libclass();
$AcademicYearID = Get_Current_Academic_Year_ID();
$ClassName = '';
$firstOption = " -- " . $Lang['General']['PleaseSelect'] . " -- "; 
$classFilter = $lclass->getSelectClass("name='ClassName' id='ClassName'",$ClassName,"",$firstOption,$AcademicYearID);

############# end Filters

?>

<style>
.error_msg_hide{display:none;}
.error_msg_show{display:'block';color: #FF0000; font-weight: bold;}
</style>

<script type="text/javascript">
<?include($PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eGuidance/common_js.php");?>

function checkForm(obj) 
{
	var error = 0;
	var focusField = '';
	hideError();
	
	if ($('#ClassName').val() == '') {
 		error++;
 		if (focusField == '') focusField = 'ClassName';
 		$('#ErrClassName').addClass('error_msg_show').removeClass('error_msg_hide');
	}
	
	if (error > 0) {
		if (focusField != '') {
			$('#' + focusField).focus();
		}
		return false
	}
	else {		
		return true;
	}
}

$(document).ready(function(){

	var isLoading = true;
	var loadingImg = '<?=$linterface->Get_Ajax_Loading_Image() ?>';

	$('#ClassName').change(function(){
		if (($('#ClassName').val() != null) && ($('#ClassName').val() != '')) {
	        isLoading = true;
			$('#AcademicYear').html(loadingImg);
			$('#AcademicYearRow').css("display","");
			$.ajax({
				dataType: "json",
				type: "POST",
				url: '../ajax/ajax.php',
				data : {
					'action': 'getAcademicYearByClassName',
					'ClassName': $('#ClassName').val()
				},		  
				success: update_academic_year_list,
				error: show_ajax_error
			});
		}
		else {
			$('#AcademicYearRow').css("display","none");
		}		
	});

	$('#btnSubmit').click(function(e) {
	    e.preventDefault();
		if (checkForm(document.form1)) {
	        $('#form1').submit();
		}
	});
	
});

function update_academic_year_list(ajaxReturn) {
	if (ajaxReturn != null && ajaxReturn.success){
		$('#AcademicYearSpan').html(ajaxReturn.html);
		isLoading = false;
	}
}	

</script>

<form id="form1" method="post" action="print_pdf.php" target="_blank">
<table id="main_table" width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
			<table width="100%" border="0" cellpadding="5" cellspacing="0">
				<tr>
					<td>
						<table align="center" class="form_table_v30">
							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['eGuidance']['Class']?>
									<span class="tabletextrequire">*</span></td>
								<td class="tabletext"><?=$classFilter?>
									<span class="error_msg_hide" id="ErrClassName"><?=$Lang['eGuidance']['Warning']['SelectClass']?></span>
								</td>
							</tr>

							<tr valign="top" id="AcademicYearRow" style="display:none;">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['General']['SchoolYear']?>
									<span class="tabletextrequire">*</span></td>
								<td class="tabletext"><span id="AcademicYearSpan"></span>
									<span class="error_msg_hide" id="ErrClassName"><?=$Lang['eGuidance']['Warning']['SelectAcademicYear']?></span>
								</td>
							</tr>
							
							<tr>
								<td valign="top" nowrap="nowrap" class="tabletextremark"><?=$i_general_required_field?></td>
								<td width="80%">&nbsp;</td>
							</tr>
							
						</table>
					</td>
				</tr>
				<tr>
					<td height="1" class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
				</tr>
				<tr>
					<td align="right">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="center">
									<span>
									<?= $linterface->GET_ACTION_BTN($Lang['eGuidance']['report']['GenerateReport'], "button","","btnSubmit", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn")?>
									</span>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table><br>
		</td>
	</tr>
</table>

</form>

<?php
    

$linterface->LAYOUT_STOP();
intranet_closedb();
?>