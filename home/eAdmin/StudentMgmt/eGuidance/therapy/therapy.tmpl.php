<?
/*
 * 	Log
 * 
 *	2017-07-18 [Cameron]
 *		- support choosing teaching / non-teaching staff
 *
 * 	2017-06-29 [Cameron]
 * 		add loadingImg
 *  
 * 	2017-05-19 [Cameron]
 * 		fix bug: disable button after submit to avoid adding duplicate record
 */
?>
<script type="text/javascript">
<?include($PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eGuidance/common_js.php");?>

function checkForm(obj) 
{
	if (!check_text(obj.GroupName,"<?=$Lang['eGuidance']['therapy']['Warning']['InputGroupName']?>")) {
		return false;
	}
 	if (!check_date(obj.StartDate,"<?php echo $i_invalid_date; ?>")) {
 		return false;
 	}
	var studentObj = document.getElementById('StudentID');
	if(studentObj.length==0) {    
	    alert('<?=$Lang['eGuidance']['Warning']['SelectStudent']?>');
	    return false;
 	}
 	
	var teacherObj = document.getElementById('TeacherID');
	if(teacherObj.length==0) {    
	    alert('<?=$Lang['eGuidance']['Warning']['SelectTeacher']?>');
	    return false;
 	}
 	
	return true;
}

$(document).ready(function(){
	var isLoading = true;
	var loadingImg = '<?=$linterface->Get_Ajax_Loading_Image() ?>';
	
	$('#ClassName').change(function(){
		$('#StudentID').attr('checked',true);
		isLoading = true;
		$('#StudentNameSpan').html(loadingImg);
		var excludeStudentID = [];
		var i=0;
		$('#StudentID option').each(function() {
			if ($.trim($(this).val()) != '') {
				excludeStudentID[i++] = $(this).val();
			}
		});

		if ($('#ClassName').val() == '') {
			$('#AvailableStudentID option').remove();
			isLoading = false;
			var blankOptionList = '<select name=AvailableStudentID[] id=AvailableStudentID size=10 multiple>';
			blankOptionList += '<option>';
			for (i=0;i<20;i++) {
				blankOptionList += '&nbsp;';
			}
			blankOptionList += '</option>';
			blankOptionList += '</select>';
			$('#StudentNameSpan').html(blankOptionList);			
		}
		else {
			$.ajax({
				dataType: "json",
				type: "POST",
				url: '../ajax/ajax.php',
				data : {
					'action': 'getStudentNameByClassWithFilter',
					'ClassName': $('#ClassName').val(),
					'ExcludeStudentID[]':excludeStudentID
				},		  
				success: update_student_list,
				error: show_ajax_error
			});
		}		
	});
	


	$('#btnSubmit').click(function(e) {
		 
	    e.preventDefault();
	    $('input.actionBtn').attr('disabled', 'disabled');
		$('#StudentID option').attr('selected',true);
		$('#TeacherID option').attr('selected',true);

		if (checkForm(document.form1)) {
			
<? if ($form_action == "edit_update.php"):?>
        $.ajax({
        	dataType: "json", 
            type: 'post',
            url: '../ajax/ajax.php?action=checkBeforeUpdateTherapy', 
            data: $('#form1').serialize(),
            async: false,            
	        success: function(ajaxReturn) {
	        	if (ajaxReturn != null && ajaxReturn.success){
	        		if (ajaxReturn.html == '1') {
	        			var confirm = window.confirm('<?=$Lang['eGuidance']['therapy']['Warning']['DeleteStudentWithActivity']?>');
	        			if (confirm) {
	        				$('#form1').submit();
	        			}
	        			else {
	        				$('input.actionBtn').attr('disabled', '');
	        			}
	        		}
	        		else {
	        			$('#form1').submit();
	        		}
	        	}
	        	else {
	        		$('input.actionBtn').attr('disabled', '');
	        	}
	        },
			error: show_ajax_error
        });
<? else:?>
        $('#form1').submit();
<? endif;?>  
		}
		else {
			$('input.actionBtn').attr('disabled', '');
		}
				      
	});
		
});

function update_student_list(ajaxReturn) {
	if (ajaxReturn != null && ajaxReturn.success){
		$('#StudentNameSpan').html(ajaxReturn.html);
		isLoading = false;
	}
}	

</script>
<form name="form1" id="form1" method="post" action="<?=$form_action?>">
<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td class="navigation"><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="5" cellspacing="0">
				<tr>
					<td>
						<table align="center" class="form_table_v30">
							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['eGuidance']['therapy']['GroupName']?>
									<span class="tabletextrequire">*</span></td>
								<td class="tabletext"><input type="text" name="GroupName" id="GroupName" class="textboxtext" value="<?=intranet_htmlspecialchars($rs_therapy['GroupName'])?>"></td>
							</tr>
							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['General']['StartDate']?>
									<span class="tabletextrequire">*</span></td>
								<td class="tabletext"><?=$linterface->GET_DATE_PICKER("StartDate",(empty($rs_therapy['StartDate'])?date('Y-m-d'):$rs_therapy['StartDate']))?></td>
							</tr>
							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['Identity']['Student']?>
									<span class="tabletextrequire">*</span></td>
								<td class="tabletext"><?=$libguidance_ui->getStudentSelection($rs_therapy['Student'])?></td>
							</tr>
							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['eGuidance']['TeacherName']?>
									<span class="tabletextrequire">*</span></td>
								<td class="tabletext"><?=$libguidance_ui->getTeacherSelection($rs_therapy['Teacher'])?>
								</td>
							</tr>
						
							<?=$libguidance_ui->getAttachmentLayout('Therapy',$therapyID)?>
							
							<tr>
								<td valign="top" nowrap="nowrap" class="tabletextremark"><?=$i_general_required_field?></td>
								<td width="80%">&nbsp;</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td height="1" class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
				</tr>
				<tr>
					<td align="right">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="center">
									<span>
									<?= $linterface->GET_ACTION_BTN($button_submit, "button","","btnSubmit", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn")?>&nbsp;
									<?= $linterface->GET_ACTION_BTN($button_reset, "reset","","btnReset", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn")?>&nbsp;
									<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='index.php'","btnCancel", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn")?>
									</span></td>
							</tr>
						</table>
					</td>
				</tr>
			</table><br>
		</td>
	</tr>
</table>
<input type=hidden id="TherapyID" name="TherapyID" value="<?=$therapyID?>">
</form>

<?=$libguidance_ui->getAttachmentUploadForm('Therapy',$therapyID)?>