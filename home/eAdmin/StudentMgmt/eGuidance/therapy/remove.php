<?php
/*
 * 	modifying:
 * 
 * 	Log
 * 
 * 	2017-02-24 [Cameron]
 * 		- create this file
 * 
 */

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/eGuidance_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/libguidance.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

intranet_auth();
intranet_opendb();

$libguidance = new libguidance();
$permission = $libguidance->getUserPermission();
if (!$permission['admin'] && !in_array('MGMT', (array)$permission['current_right']['MGMT']['THERAPY'])) {
	$libguidance->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$ldb = new libdb();
$lfs = new libfilesystem();
$result = array();
$therapyExist = false;

if (count($TherapyID) > 0) {
	$rs_therapy = $libguidance->getTherapyActivity($TherapyID);
	$activityIDAry = array();
	
	$ldb->Start_Trans();
	// delete therapy
	$sql = "DELETE FROM INTRANET_GUIDANCE_THERAPY WHERE TherapyID IN ('".implode("','",$TherapyID)."')";
	$result[] = $ldb->db_db_query($sql);

	$sql = "DELETE FROM INTRANET_GUIDANCE_THERAPY_STUDENT WHERE TherapyID IN ('".implode("','",$TherapyID)."')";
	$result[] = $ldb->db_db_query($sql);

	$sql = "DELETE FROM INTRANET_GUIDANCE_THERAPY_TEACHER WHERE TherapyID IN ('".implode("','",$TherapyID)."')";
	$result[] = $ldb->db_db_query($sql);
	
	if (count($rs_therapy)) {
		foreach((array)$rs_therapy as $r) {
			$activityID = $r['ActivityID'];
			$activityIDAry[] = $activityID; 
			$sql = "DELETE FROM INTRANET_GUIDANCE_THERAPY_ACTIVITY_RESULT WHERE ActivityID='".$activityID."'";
			$result[] = $ldb->db_db_query($sql);

			$sql = "DELETE FROM INTRANET_GUIDANCE_THERAPY_ACTIVITY WHERE ActivityID='".$activityID."'";
			$result[] = $ldb->db_db_query($sql);
		}
	}
	
	## remove attachment in therapy
	$sql = "SELECT 
				FileID, EncodeFileName 
			FROM 
				INTRANET_GUIDANCE_ATTACHMENT
			WHERE 
				Target='Therapy'
			AND	RecordID IN ('".implode("','",(array)$TherapyID)."')";
	$rs = $ldb->returnResultSet($sql);
		
	if (count($rs) > 0) {
		foreach((array)$rs as $r) {
			$FileID = $r['FileID'];
			$EncodeFileName = $r['EncodeFileName'];
			$file = $libguidance->eg_path.$EncodeFileName;
			$result[] = $lfs->file_remove($file);
		}

		$sql = "DELETE FROM INTRANET_GUIDANCE_ATTACHMENT WHERE Target='Therapy' AND RecordID IN ('".implode("','",(array)$TherapyID)."')";
		$result[] = $ldb->db_db_query($sql);
	}


	## remove attachment in therapy activity
	if (count($activityIDAry)) {
		$sql = "SELECT 
					FileID, EncodeFileName 
				FROM 
					INTRANET_GUIDANCE_ATTACHMENT
				WHERE 
					Target='TherapyActivity'
				AND	RecordID IN ('".implode("','",(array)$activityIDAry)."')";
		$rs = $ldb->returnResultSet($sql);
			
		if (count($rs) > 0) {
			foreach((array)$rs as $r) {
				$FileID = $r['FileID'];
				$EncodeFileName = $r['EncodeFileName'];
				$file = $libguidance->eg_path.$EncodeFileName;
				$result[] = $lfs->file_remove($file);
			}
	
			$sql = "DELETE FROM INTRANET_GUIDANCE_ATTACHMENT WHERE Target='TherapyActivity' AND RecordID IN ('".implode("','",(array)$activityIDAry)."')";
			$result[] = $ldb->db_db_query($sql);
		}
	}	
	
}

if (!in_array(false,$result)) {
	$ldb->Commit_Trans();
	$returnMsgKey = 'DeleteSuccess';
}
else {
	$ldb->RollBack_Trans();
	$returnMsgKey = 'DeleteUnsuccess';
}

header("location: index.php?returnMsgKey=".$returnMsgKey);

intranet_closedb();

?>


