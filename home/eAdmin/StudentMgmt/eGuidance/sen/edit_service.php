<?php
/*
 * 	modifying:
 * 
 * 	Log
 * 
 * 	2017-07-20 [Cameron]
 * 		- fix bug: should redirect to listview page if ServiceID is empty
 * 
 * 	2017-02-28 [Cameron]
 * 		- create this file
 * 
 */

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/eGuidance_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/guidance_conf.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/libguidance.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/libguidance_ui.php");

intranet_auth();
intranet_opendb();

$libguidance = new libguidance();
$permission = $libguidance->getUserPermission();
if (!$permission['admin'] && !in_array('MGMT', (array)$permission['current_right']['MGMT']['SEN'])) {
	$libguidance->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

if (is_array($ServiceID)) {
	$serviceID = (count($ServiceID)==1) ? $ServiceID[0] : '';
}
else {
	$serviceID = $ServiceID;
}

if (!$serviceID) {
	header("location: index.php");
}

$libguidance_ui = new libguidance_ui();
$linterface = new interface_html();


$showOther = false;	
$rs_senservice = $libguidance->getSENService($serviceID);

if (count($rs_senservice) == 1) {
	$rs_senservice = $rs_senservice[0];
	if (!empty($rs_senservice['ServiceType']) && substr($rs_senservice['ServiceType'],0,5) == 'Other') {
		$serviceTypeOther = substr($rs_senservice['ServiceType'],7);
		$showOther = true;
	}
	
	$rs_senservice['Student'] = $libguidance->getSENServiceStudent($serviceID);
	
	$rs_senservice['Teacher'] = $libguidance->getSENServiceTeacher($serviceID);
}
else {
	header("location: index.php");
}


# menu highlight setting
$CurrentPageArr['eGuidance'] = 1;
$CurrentPage = "PageSEN";
$CurrentPageName = $Lang['eGuidance']['name'];

### Title ###
$TAGS_OBJ = $libguidance->getSENTabs("Service");

$MODULE_OBJ = $libguidance->GET_MODULE_OBJ_ARR();
$PAGE_NAVIGATION[] = array($Lang['eGuidance']['sen_service']['SenService'], "index.php");
$PAGE_NAVIGATION[] = array($Lang['Btn']['Edit'], "");

$linterface->LAYOUT_START();
	
$form_action = "edit_service_update.php";
include("sen_service.tmpl.php");

$linterface->LAYOUT_STOP();
intranet_closedb();

?>


