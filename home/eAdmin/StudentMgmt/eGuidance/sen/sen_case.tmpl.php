<?
/*
 * Log
 *
 * 2020-05-07 [Cameron]
 * - add SENTypeConfirmDate [case #F152883]
 * 
 * 2020-04-23 [Cameron]
 * - fix typo CheckAjdustment -> CheckAdjustment
 * 
 * 2020-03-31 [Cameron]
 * - add function resetStudentBasicInfo(), apply it when change class name
 * 
 * 2020-03-02 [Cameron]
 * - add line break before SEN case sub type in SENType with text
 * - retrieve $senItemTextAry according to the revised format [case #D178788]
 * 
 * 2018-05-25 [Cameron]
 * - show student info and disable student selection for edit [case #M138226]
 *      
 * 2018-04-13 [Cameron]
 * - use setup for SEN case type
 *
 * 2018-04-11 [Cameron]
 * - show STRN, Gender and DateOfBirth of a student [case #M130681]
 *
 * 2018-04-10 [Cameron]
 * - add quit option to IsSEN field [case #M130681]
 * - add ConfirmDate and NumberOfReport fields
 *
 * 2017-08-03 [Cameron]
 * - add confidential field
 * - add save and notify button
 *
 * 2017-07-24 [Cameron]
 * - add category to support service
 * - add field: Tier
 *
 * 2017-07-20 [Cameron]
 * - need to show SEN type no matter it's confirmed or not
 * - add SEN Type: RD, MH, break ADHD to AD and HD
 *
 * 2017-07-18 [Cameron]
 * - support choosing teaching / non-teaching staff
 *
 * 2017-06-29 [Cameron]
 * - add label for radio button and checkbox item so that clicking lable is the same as clicking the radio button
 * - add loadingImg
 *
 * 2017-05-19 [Cameron]
 * fix bug: disable button after submit to avoid adding duplicate record
 */
?>
<?echo $linterface->Include_Thickbox_JS_CSS();?>

<script type="text/javascript">
<?include($PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eGuidance/common_js.php");?>

function checkForm(obj) 
{
	if(!check_text(obj.StudentID,'<?=$Lang['eGuidance']['Warning']['SelectStudent']?>'))
	{
		return false;
	}

 	if ($('#ConfirmDate').val() != '' && !check_date(obj.ConfirmDate,"<?php echo $i_invalid_date; ?>")) {
 		return false;
 	}
	
	var teacherObj = document.getElementById('TeacherID');
	if(teacherObj.length==0) {    
	    alert('<?=$Lang['eGuidance']['Warning']['SelectTeacher']?>');
	    return false;
 	}

	if ($('#NumberOfReport').val() != '' && (isNaN($('#NumberOfReport').val()) || $('#NumberOfReport').val() < 0 || $('#NumberOfReport').val().indexOf('.') != -1) ) {
		alert('<?=$Lang['eGuidance']['Warning']['InputInteger']?>');
		$('#NumberOfReport').focus();
		return false;
	}
	
	if ($("input:radio[name='IsSEN']:checked").length == 0) {
		alert('<?=$Lang['eGuidance']['sen_case']['Warning']['SelectIsSEN']?>');
		$("input:radio[name='IsSEN']:first").focus();
		return false;
	}

 	if ($('#SENConfirm').is(':checked') && ($(':input[name="SENType\[\]"]:checked').length == 0)) {
 		alert('<?=$Lang['eGuidance']['sen_case']['Warning']['SelectCaseType']?>');
 		$(':input[name="SENType\[\]"]:first').focus();
 		return false;
 	} 
 	var ary, type_id, item_id;
 	$(':textarea[name^="Arrangement"]').each(function(){
 		ary = $(this).attr('id').split('\]\[');	// 2D array
 		type_id = ary[0].replace('Arrangement\[','');
 		item_id = ary[1].replace('\]','');
 		if (($.trim($(this).val()) != '') && (!$('#AdjustItem_'+type_id+'_'+item_id).attr('checked'))) {
 			$('#AdjustItem_'+type_id+'_'+item_id).attr('checked', true);		// auto check box if arrangement is not empty
 		} 	
 	});

 	$(':textarea[name^="Tier"]').each(function(){
 		var id = $(this).attr('id');
 		id = id.replace('Tier','');
 		id = id.replace('Remark','');
 		if (($.trim($(this).val()) != '') && (!$('#IsTier'+id).attr('checked'))) {
 			$('#IsTier'+id).attr('checked', true);		// auto check box if tier remark is not empty
 		} 	
 	});
 	
	return true;
}

$(document).ready(function(){

	var isLoading = true;
	var loadingImg = '<?=$linterface->Get_Ajax_Loading_Image() ?>';

	$('#ClassName').change(function(){
        isLoading = true;
		$('#StudentNameSpan').html(loadingImg);
		resetStudentBasicInfo();
		
		$.ajax({
			dataType: "json",
			type: "POST",
			url: '../ajax/ajax.php',
			data : {
				'action': '<?=($form_action=='new_case_update.php') ? 'getStudentNameByClassForNewSEN' : 'getStudentNameByClassForEditSEN'?>',
				'ClassName': $('#ClassName').val(),
				'OrgStudentID': $('#OrgStudentID').val()
			},		  
			success: update_student_list,
			error: show_ajax_error
		});
		
	});
	
	$("input:radio[name='IsSEN']").change(function(){
		if ($("input:radio[name='IsSEN']:checked").val() == '1') {
			$('#trSENType .tabletextrequire').html('*');
			$('#SENTypeConfirmDateTitle').css('display','');
			showSENTypeConfirmDateField();	
		}
		else {
			$('#trSENType .tabletextrequire').html('');
			if ($("input:radio[name='IsSEN']:checked").val() == '2') { 
				$('#SENTypeConfirmDateTitle').css('display','');
				showSENTypeConfirmDateField();
			}
			else {
				$('#SENTypeConfirmDateTitle').css('display','none');
				hideSENTypeConfirmDateField();
			}
		}
	});

	$(':input[name="SENType\[\]"]').click(function(){
		if ($(this).attr('checked') && ($("input:radio[name='IsSEN']:checked").val() != '0')) {
			$('#tdSENTypeConfirmDate_' + $(this).val()).css('display','');
		}
		else {
			$('#tdSENTypeConfirmDate_' + $(this).val()).css('display','none');
		}
	});

	$('.withSubType').click(function(){
		var typeCode = $(this).attr('data-TypeCode');

		if ($(this).attr('checked')) {
			$('#subTypeSpan_'+typeCode).css("display","");
		}
		else {
			$(':input[name="'+typeCode+'\[\]"]').attr('checked', false);
			$('#subTypeSpan_'+typeCode).css("display","none");
		}
	});


	$('.subTypeInput').click(function(){
		var allUnCheck = true;
		var parent = $(this).parent();
		var typeCode;
		parent.find('input:checkbox').each(function(){
			typeCode = $(this).attr('data-TypeCode')
			if ($(this).attr('checked')) {
				allUnCheck = false;				
				$('#SENType_' + typeCode).attr('checked', true);
			}
		});
		
		if (allUnCheck) {
			if ($('#SENType' + typeCode).length == 0) {
				$('#SENType_' + typeCode).attr('checked', false);
			}
			$('#subTypeSpan_' + typeCode).css("display","none");
		}
	});

	$('.withText').click(function(){
		typeCode = $(this).attr('data-TypeCode')
		if ($(this).attr('checked')) {
			$('#SENType'+typeCode).css("display","");
		}
		else {
			$('#SENType'+typeCode).css("display","none");
		}
	});
		

	$('#btnSubmit').click(function(e) {
	    e.preventDefault();
	    $('input.actionBtn').attr('disabled', 'disabled');
	    $('#TeacherID option').attr('selected',true);
		if ($('#IsConfidentialYes').is(':checked')) {
			$('#ConfidentialViewerID option').attr('selected',true);
		}
		if (checkForm(document.form1)) {
			
<? if ($form_action == "edit_case_update.php"):?>
		if ($('#StudentID').val() != $('#OrgStudentID').val()) {
	        $.ajax({
	        	dataType: "json", 
	            type: 'post',
	            url: '../ajax/ajax.php?action=checkBeforeUpdateSENCase', 
	            data: {'StudentID':$('#StudentID').val()},
	            async: false,            
		        success: function(ajaxReturn) {
		        	if (ajaxReturn != null && ajaxReturn.success){
		        		if (ajaxReturn.html == '1') {
		        			alert('<?=$Lang['eGuidance']['sen_case']['Warning']['DuplicateRecord']?>');
		        			$('input.actionBtn').attr('disabled', '');
		        		}
		        		else {
		        			$('#form1').submit();
		        		}
		        	}
		        	else {
		        		$('input.actionBtn').attr('disabled', '');
		        	}
		        },
				error: show_ajax_error
	        });
		}
	    else {
	    	$('#form1').submit();
	    }
<? else:?>
        $('#form1').submit();
<? endif;?>  
			
		}
		else {
			$('input.actionBtn').attr('disabled', '');
		}
	});

});

function showSENTypeConfirmDateField()
{
	$(':input[name="SENType\[\]"]').each(function(){
		if ($(this).attr('checked')) {
			$('#tdSENTypeConfirmDate_' + $(this).val()).css('display','');
		}
	});	
}

function hideSENTypeConfirmDateField()
{
	$(':input[name="SENType\[\]"]').each(function(){
		if ($(this).attr('checked')) {
			$('#tdSENTypeConfirmDate_' + $(this).val()).css('display','none');
		}
	});	
}

function resetStudentBasicInfo()
{
	$('#strnSpan').html('-');
	$('#genderSpan').html('-');
	$('#dateOfBirthSpan').html('-');
}

function changeStudent()
{
	if ($('#StudentID').val() == '') {
		resetStudentBasicInfo();
	}
	else {
		$.ajax({
			dataType: "json",
			type: "POST",
			url: '../ajax/ajax.php',
			data : {
				'action': 'getStudentInfo',
				'StudentID': $('#StudentID').val()
			},		  
			success: updateStudentInfo,
			error: show_ajax_error
		});
	}
}

function CheckAdjustment(type_id) {
	$('.AdjustType_'+type_id).attr('checked',$('#AdjustType_'+type_id).attr('checked'));
}

function CheckService(type_id) {
	$('.ServiceType_'+type_id).attr('checked',$('#ServiceType_'+type_id).attr('checked'));
}

function notify_colleague_update(recordID) {
	$('#TeacherID option').attr('selected',true);
	if ($('#IsConfidentialYes').is(':checked')) {
		$('#ConfidentialViewerID option').attr('selected',true);
	}
	if (checkForm(document.form1)) {
		tb_show('<?=$Lang['eGuidance']['NotifyUpdate']?>','../ajax/ajax_layout.php?action=NotifyColleagueUpdate&RecordID='+$('#'+recordID).val()+'&RecordType='+recordID+'&height=450&width=650');
	}
}

function show_history_push_message() {
	tb_show('<?=$Lang['eGuidance']['PushMessage']['ViewStatus']?>','../ajax/ajax_layout.php?action=GetHistoryNotification&RecordID='+$('#OrgStudentID').val()+'&RecordType=SENCase&height=450&width=650');		
}

function updateStudentInfo(ajaxReturn)
{
	if (ajaxReturn != null && ajaxReturn.success){
		$('#strnSpan').html(ajaxReturn.STRN);
		$('#genderSpan').html(ajaxReturn.Gender);
		$('#dateOfBirthSpan').html(ajaxReturn.DateOfBirth);
	}
}

function update_student_list(ajaxReturn) {
	if (ajaxReturn != null && ajaxReturn.success){
		$('#StudentNameSpan').html(ajaxReturn.html);
		isLoading = false;
	}
}	

function update_adjust_item_list(ajaxReturn) {
	if (ajaxReturn != null && ajaxReturn.success){
		$('#AdjustmentItemRow_'+ajaxReturn.typeID).before(ajaxReturn.html);
		$('input.actionBtn').attr('disabled', '');
	}
}	

function update_support_item_list(ajaxReturn) {
	if (ajaxReturn != null && ajaxReturn.success){
		$('#ServiceItemRow_'+ajaxReturn.typeID).before(ajaxReturn.html);
		
//		if (ajaxReturn.type != '' && $('#Support'+ajaxReturn.type+'Item').length ) {
//			$('#Support'+ajaxReturn.type+'Item').before(ajaxReturn.html);
			$('input.actionBtn').attr('disabled', '');
//		}
	}
}	

function AddAdjustmentItem(typeID) {
	tb_show('<?=$Lang['eGuidance']['sen_case']['AddAdjustmentItem']?>','../ajax/ajax_layout.php?action=AddAdjustmentItem&TypeID='+typeID+'&height=500&width=800');
}


function AddStudyServiceItem(typeID) {
	tb_show('<?=$Lang['eGuidance']['sen_case']['AddSupportStudyItem']?>','../ajax/ajax_layout.php?action=AddSupportItem&TypeID='+typeID+'&height=500&width=800');	
//	var title = (type == 'Study') ? '<?=$Lang['eGuidance']['sen_case']['AddSupportStudyItem']?>' : '<?=$Lang['eGuidance']['sen_case']['AddSupportOtherItem']?>';
//	tb_show(title,'../ajax/ajax_layout.php?action=AddSupportItem&Type='+type+'&height=500&width=800');
}

function AddOtherServiceItem(typeID) {
	tb_show('<?=$Lang['eGuidance']['sen_case']['AddSupportOtherItem']?>','../ajax/ajax_layout.php?action=AddSupportItem&TypeID='+typeID+'&height=500&width=800');
}

</script>

<form name="form1" id="form1" method="post" action="<?=$form_action?>">
	<table width="98%" border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td align="center">
				<table width="100%" border="0" cellspacing="0" cellpadding="5">
					<tr>
						<td class="navigation"><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
					</tr>
				</table>
				<table width="100%" border="0" cellpadding="5" cellspacing="0">
					<tr>
						<td>
							<table align="center" class="form_table_v30">

								<tr valign="top">
									<td width="20%" valign="top" nowrap="nowrap"
										class="field_title"><?=$Lang['Identity']['Student']?>
									<span class="tabletextrequire">*</span></td>
									<td class="tabletext">
    								<?php if ($form_action == 'edit_case_update.php'):?>
    									<?php echo $studentInfo;?>
    								<?php else:?>
    									<span><?=$Lang['eGuidance']['Class']?></span>&nbsp;<?=$classSelection?> &nbsp;
    									<span><?=$Lang['eGuidance']['StudentName']?></span>&nbsp;<span id="StudentNameSpan"><?=$studentSelection?></span> 
    								<?php endif;?>
    									<?=$linterface->GET_SMALL_BTN($Lang['eGuidance']['personal']['ViewPersonalRecord'],'button','ViewStudentInfo();','BtnViewStudentInfo')?>
									<div>
 										<?php echo $i_STRN;?>:&nbsp;&nbsp;&nbsp;<span id="strnSpan"><?php echo $rs_sencase['STRN'];?></span><br>
 										<?php echo $i_UserGender;?>:&nbsp;&nbsp;&nbsp;<span
												id="genderSpan"><?php echo $rs_sencase['Gender'];?></span><br>
 										<?php echo $i_UserDateOfBirth;?>:&nbsp;&nbsp;&nbsp;<span
												id="dateOfBirthSpan"><?php echo $rs_sencase['DateOfBirth'];?></span>
										</div></td>
								</tr>

								<tr valign="top">
									<td width="20%" valign="top" nowrap="nowrap"
										class="field_title"><?=$Lang['eGuidance']['TeacherName']?>
									<span class="tabletextrequire">*</span></td>
									<td class="tabletext"><?=$libguidance_ui->getTeacherSelection($rs_sencase['Teacher'])?>
								</td>
								</tr>

								<tr valign="top">
									<td width="20%" valign="top" nowrap="nowrap"
										class="field_title"><?=$Lang['eGuidance']['Confidential']?><img id="ConfidentialIcon" src="/images/<?=$LAYOUT_SKIN?>/icalendar/icon_lock.gif" style="display:<?=($rs_sencase['IsConfidential']?'':'none')?>"></td>
									<td class="tabletext"><input type="radio" name="IsConfidential"
										id="IsConfidentialYes" value="1"
										<?=$rs_sencase['IsConfidential']?' checked':''?>><label
										for="IsConfidentialYes"><?=$Lang['General']['Yes'].'('.$Lang['eGuidance']['Warning']['SelectAuthorizedUser'].')'?></label>
										<input type="radio" name="IsConfidential"
										id="IsConfidentialNo" value="0"
										<?=$rs_sencase['IsConfidential']?'':' checked'?>><label
										for="IsConfidentialNo"><?=$Lang['General']['No']?></label>

										<div id="ConfidentialSection" style="display:<?=($rs_sencase['IsConfidential']?'':'none')?>">
										<?=$libguidance_ui->getConfidentialViewerSelection($rs_sencase['ConfidentialViewerID'])?>
									</div></td>
								</tr>

								<tr valign="top">
									<td width="20%" valign="top" nowrap="nowrap"
										class="field_title"><?=$Lang['eGuidance']['sen']['SENStudent']?>
									<span class="tabletextrequire">*</span></td>
									<td class="tabletext"><input type="radio" name="IsSEN"
										id="SENConfirm" value="1"
										<?=($rs_sencase['IsSEN'] == 1?' checked':'')?>><label
										for="SENConfirm"><?=$Lang['eGuidance']['sen']['Confirm']?></label><br>
										<input type="radio" name="IsSEN" id="SENNotConfirm" value="0"
										<?=(isset($rs_sencase['IsSEN']) && $rs_sencase['IsSEN']==0?' checked':'')?>><label
										for="SENNotConfirm"><?=$Lang['eGuidance']['sen']['NotConfirm']?></label><br>
										<input type="radio" name="IsSEN" id="SENQuit" value="2"
										<?=($rs_sencase['IsSEN']==2?' checked':'')?>><label
										for="SENQuit"><?=$Lang['eGuidance']['sen']['Quit']?></label></td>
								</tr>

								<tr valign="top">
									<td width="20%" valign="top" nowrap="nowrap"
										class="field_title"><?=$Lang['eGuidance']['sen_case']['ConfirmDate']?></td>
									<td class="tabletext"><?=$linterface->GET_DATE_PICKER("ConfirmDate",((empty($rs_sencase['ConfirmDate']) || $rs_sencase['ConfirmDate'] == '0000-00-00')?'':$rs_sencase['ConfirmDate']),$OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="",$ID="",$SkipIncludeJS=0, $CanEmptyField=1)?></td>
								</tr>

								<tr valign="top" id="trSENType">
									<td width="20%" valign="top" nowrap="nowrap"
										class="field_title"><?=$Lang['eGuidance']['guidance']['CaseType']?>
									<span class="tabletextrequire"></span></td>
									<td class="tabletext">
<?
$sen_case_type = $libguidance->getAllSENCaseType();
$senCaseTypeAssoc = BuildMultiKeyAssoc($sen_case_type, array(
    'Code'
), $IncludedDBField = array(
    'Name',
    'IsWithText'
));
$sen_case_subtype = $libguidance->getAllSENCaseSubType();
$senCaseSubTypeAssoc = BuildMultiKeyAssoc($sen_case_subtype, array(
    'TypeCode',
    'Code'
), $IncludedDBField = array(
    'Name'
), $SingleValue = 1);

$x = '<table width="100%">';
$x .= '<tr><td>'.$Lang['eGuidance']['guidance']['CaseType'].'</td><td id="SENTypeConfirmDateTitle" class="SENTypeConfirmDate" style="display:'.($rs_sencase['IsSEN'] ? '' : 'none').'">'.$Lang['eGuidance']['sen_case']['SENTypeConfirmDate'].'</td></tr>';
foreach ((array) $senCaseTypeAssoc as $k => $v) {
    $senTypeClass = '';
    if (count($senCaseSubTypeAssoc[$k]) && $v['IsWithText']) {
        $senTypeClass = 'withSubType withText';
    } elseif (count($senCaseSubTypeAssoc[$k])) {
        $senTypeClass = 'withSubType';
    } elseif ($v['IsWithText']) {
        $senTypeClass = 'withText';
    } else {
        $senTypeClass = '';
    }
    
    $x .= '<tr><td>';   
    $x .= '<input type="checkbox" class="' . $senTypeClass . '" data-TypeCode="' . $k . '" name="SENType[]" id="SENType_' . $k . '" value="' . $k . '"' . (in_array($k, (array) $senTypeAry) ? 'checked' : '') . '>';
    $x .= '<label for="SENType_' . $k . '">' . $v['Name'] . '</label>';
    
    if ($v['IsWithText']) {
        $x .= ' <input type="text" name="SENType' . $k . '" id="SENType' . $k . '" value="' . intranet_htmlspecialchars($senItemTextAry[$k]) . '" style="width:60%; display:' . (in_array($k, (array) $senTypeAry) ? '' : 'none') . '">';
    }
    
    if (count($senCaseSubTypeAssoc[$k])) {
        $numOfSubType = 0;
        if ($senTypeClass == 'withSubType withText') {
            $x .= '<br>&nbsp;&nbsp;&nbsp;';
        }
        $x .= ' <span class="subTypeSpan" id="subTypeSpan_' . $k . '" style="display:' . (in_array($k, (array) $senTypeAry) ? '' : 'none') . '">(';
        
        foreach ((array) $senCaseSubTypeAssoc[$k] as $_subTypeCode => $_subTypeName) {
            if ($numOfSubType > 0) {
                $x .= ' / ';
            }
            $x .= '<input type="checkbox" class="subTypeInput" data-TypeCode="' . $k . '" name="' . $k . '[]" id="' . $_subTypeCode . '" value="' . $_subTypeCode . '"' . (in_array("$_subTypeCode", (array) $senItemAry[$k]) ? 'checked' : '') . '>';
            $x .= '<label for="' . $_subTypeCode . '">' . $_subTypeName . '</label>';
            
            $numOfSubType ++;
        }
        $x .= ')</span>';
    }
    $x .= '</td>';
    $x .= '<td id="tdSENTypeConfirmDate_'.$k.'" class="SENTypeConfirmDate" style="display:'.($rs_sencase['IsSEN'] && in_array($k, (array) $senTypeAry) ? '' : 'none').'">';
    $x .= $linterface->GET_DATE_PICKER("SENTypeConfirmDate_".$k,((empty($rsSENTypeConfirmDate[$k]) || $rsSENTypeConfirmDate[$k] == '0000-00-00')?'':$rsSENTypeConfirmDate[$k]),$OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="",$ID="",$SkipIncludeJS=0, $CanEmptyField=1);
    $x .= '</td></tr>';
//    $x .= '<br>';
}
$x .= '</table>';
echo $x;
?>
								</td>
								</tr>

								<tr valign="top">
									<td width="20%" valign="top" nowrap="nowrap"
										class="field_title"><?=$Lang['eGuidance']['sen_case']['Tier']['TierName']?></td>
									<td class="tabletext" valign="middle">
								<?
        $x = '';
        $x .= '<table class="no_bottom_border">';
        for ($i = 1; $i <= 3; $i ++) {
            $x .= '<tr><td style="vertical-align:middle; width:80px; "><input type="checkbox" name="IsTier' . $i . '" id="IsTier' . $i . '" value="1"' . ($rs_sencase["IsTier$i"] ? ' checked' : '') . '><label for="IsTier' . $i . '">' . $Lang['eGuidance']['sen_case']['Tier']["Tier$i"] . '</label></td>';
            $x .= '<td>' . $linterface->GET_TEXTAREA('Tier' . $i . 'Remark', $rs_sencase['Tier' . $i . 'Remark'], 60, 4) . '</td>';
            $x .= '</tr>';
        }
        $x .= '</table>';
        echo $x;
        ?>
								</td>
								</tr>

								<tr valign="top">
									<td width="20%" valign="top" nowrap="nowrap"
										class="field_title"><?=$Lang['eGuidance']['sen_case']['FineTuneArrangement']?>
									</td>
									<td class="tabletext">
										<table class="no_bottom_border">
									<?
        
        $x = '';
        foreach ((array) $rs_sen_adjust_type as $k => $v) {
            $x .= '<tr><td><input type="checkbox" name="TypeID[]" id="AdjustType_' . $k . '" onClick="CheckAdjustment(' . $k . ')"><label for="AdjustType_' . $k . '">' . $v . '</label></td></tr>';
            foreach ((array) $rs_sen_adjust_item[$k] as $kk => $vv) {
                if ($vv['WithText']) {
                    $x .= '<tr><td>';
                    $x .= '<table class="no_bottom_border">';
                    $x .= '<tr><td style="padding-left:17px; vertical-align:middle; width:80px; "><input type="checkbox" class="AdjustType_' . $k . '" name="ItemID[' . $k . '][]" id="AdjustItem_' . $k . '_' . $kk . '" value="' . $kk . '"' . ($rs_sencase_adjustment[$kk] ? ' checked' : '') . '><label for="AdjustItem_' . $k . '_' . $kk . '">' . $vv[$nameField] . '</label></td>';
                    $x .= '<td>' . $linterface->GET_TEXTAREA('Arrangement[' . $k . '][' . $kk . ']', $rs_sencase_adjustment[$kk]['Arrangement'], 60, 4) . '</td>';
                    $x .= '</tr>';
                    $x .= '</table>';
                    $x .= '</td></tr>';
                } else {
                    $x .= '<tr><td style="padding-left:20px;"><input type="checkbox" class="AdjustType_' . $k . '" name="ItemID[' . $k . '][]" id="AdjustItem_' . $k . '_' . $kk . '" value="' . $kk . '"' . ($rs_sencase_adjustment[$kk] ? ' checked' : '') . '><label for="AdjustItem_' . $k . '_' . $kk . '">' . $vv[$nameField] . '</label></td></tr>';
                }
            }
            $x .= '<tr id="AdjustmentItemRow_' . $k . '"><td style="padding-left:20px;"><span class="table_row_tool"><a class="add" onClick="AddAdjustmentItem(' . $k . ')" title="' . $Lang['eGuidance']['sen_case']['AddAdjustmentItem'] . '"></a></span></td></tr>';
        }
        echo $x;
        ?>
									</table>
									</td>
								</tr>

								<tr valign="top">
									<td width="20%" valign="top" nowrap="nowrap"
										class="field_title"><?=$Lang['eGuidance']['sen_case']['FineTuneSupportStudy']?>
									</td>
									<td class="tabletext">
										<table class="no_bottom_border">
									<?
        
        $x = '';
        foreach ((array) $rs_sen_study_service_type as $k => $v) {
            $x .= '<tr><td><input type="checkbox" name="ServiceTypeID[]" id="ServiceType_' . $k . '" onClick="CheckService(' . $k . ')"><label for="ServiceType_' . $k . '">' . $v . '</label></td></tr>';
            foreach ((array) $rs_sen_support_study[$k] as $kk => $vv) {
                if ($vv['WithText']) {
                    $x .= '<tr><td>';
                    $x .= '<table class="no_bottom_border">';
                    $x .= '<tr><td style="padding-left:17px; vertical-align:middle; width:80px; "><input type="checkbox" class="ServiceType_' . $k . '" name="ServiceItemID[' . $k . '][]" id="ServiceItem_' . $k . '_' . $kk . '" value="' . $kk . '"' . ($rs_sencase_support_study[$kk] ? ' checked' : '') . '><label for="ServiceItem_' . $k . '_' . $kk . '">' . $vv[$nameField] . '</label></td>';
                    $x .= '<td>' . $linterface->GET_TEXTAREA('ServiceArrangement[' . $k . '][' . $kk . ']', $rs_sencase_support_study[$kk]['Arrangement'], 60, 4) . '</td>';
                    $x .= '</tr>';
                    $x .= '</table>';
                    $x .= '</td></tr>';
                } else {
                    $x .= '<tr><td style="padding-left:20px;"><input type="checkbox" class="ServiceType_' . $k . '" name="ServiceItemID[' . $k . '][]" id="ServiceItem_' . $k . '_' . $kk . '" value="' . $kk . '"' . ($rs_sencase_support_study[$kk] ? ' checked' : '') . '><label for="ServiceItem_' . $k . '_' . $kk . '">' . $vv[$nameField] . '</label></td></tr>';
                }
            }
            $x .= '<tr id="ServiceItemRow_' . $k . '"><td style="padding-left:20px;"><span class="table_row_tool"><a class="add" onClick="AddStudyServiceItem(' . $k . ')" title="' . $Lang['eGuidance']['sen_case']['AddSupportStudyItem'] . '"></a></span></td></tr>';
        }
        echo $x;
        ?>
									</table>
									</td>
								</tr>

								<tr valign="top">
									<td width="20%" valign="top" nowrap="nowrap"
										class="field_title"><?=$Lang['eGuidance']['sen_case']['FineTuneSupportOther']?>
									</td>
									<td class="tabletext">
										<table class="no_bottom_border">
									<?
        
        $x = '';
        foreach ((array) $rs_sen_other_service_type as $k => $v) {
            $x .= '<tr><td><input type="checkbox" name="ServiceTypeID[]" id="ServiceType_' . $k . '" onClick="CheckService(' . $k . ')"><label for="ServiceType_' . $k . '">' . $v . '</label></td></tr>';
            foreach ((array) $rs_sen_support_other[$k] as $kk => $vv) {
                if ($vv['WithText']) {
                    $x .= '<tr><td>';
                    $x .= '<table class="no_bottom_border">';
                    $x .= '<tr><td style="padding-left:17px; vertical-align:middle; width:80px; "><input type="checkbox" class="ServiceType_' . $k . '" name="ServiceItemID[' . $k . '][]" id="ServiceItem_' . $k . '_' . $kk . '" value="' . $kk . '"' . ($rs_sencase_support_other[$kk] ? ' checked' : '') . '><label for="ServiceItem_' . $k . '_' . $kk . '">' . $vv[$nameField] . '</label></td>';
                    $x .= '<td>' . $linterface->GET_TEXTAREA('ServiceArrangement[' . $k . '][' . $kk . ']', $rs_sencase_support_other[$kk]['Arrangement'], 60, 4) . '</td>';
                    $x .= '</tr>';
                    $x .= '</table>';
                    $x .= '</td></tr>';
                } else {
                    $x .= '<tr><td style="padding-left:20px;"><input type="checkbox" class="ServiceType_' . $k . '" name="ServiceItemID[' . $k . '][]" id="ServiceItem_' . $k . '_' . $kk . '" value="' . $kk . '"' . ($rs_sencase_support_other[$kk] ? ' checked' : '') . '><label for="ServiceItem_' . $k . '_' . $kk . '">' . $vv[$nameField] . '</label></td></tr>';
                }
            }
            $x .= '<tr id="ServiceItemRow_' . $k . '"><td style="padding-left:20px;"><span class="table_row_tool"><a class="add" onClick="AddOtherServiceItem(' . $k . ')" title="' . $Lang['eGuidance']['sen_case']['AddSupportOtherItem'] . '"></a></span></td></tr>';
        }
        echo $x;
        ?>
									</table>
									</td>
								</tr>

								<tr valign="top">
									<td width="20%" valign="top" nowrap="nowrap"
										class="field_title"><?=$Lang['eGuidance']['sen_case']['NumberOfReport']?>
									</td>
									<td class="tabletext"><?php echo $linterface->GET_TEXTBOX_NUMBER('NumberOfReport', 'NumberOfReport', $rs_sencase['NumberOfReport'] ? $rs_sencase['NumberOfReport'] : '', $OtherClass='', $OtherPar=array());?></td>
								</tr>

								<tr valign="top">
									<td width="20%" valign="top" nowrap="nowrap"
										class="field_title"><?=$Lang['eGuidance']['sen_case']['Remark']?>
									</td>
									<td class="tabletext"><?=$linterface->GET_TEXTAREA("Remark", $rs_sencase['Remark'], 80, 10)?></td>
								</tr>
						
							<?=$libguidance_ui->getAttachmentLayout('SENCase',$studentID)?>
							
							<tr>
									<td valign="top" nowrap="nowrap" class="tabletextremark"><?=$i_general_required_field?></td>
									<td width="80%">&nbsp;</td>
								</tr>
							
						<? if ($studentID): ?>
							<tr valign="top">
									<td class="tabletext" colspan="2"><a href="javascript:void(0)"
										id="HistoryNotify" onClick="show_history_push_message()"><?=$Lang['eGuidance']['PushMessage']['ViewStatus']?></a></td>
								</tr>
						<? endif;?>
							
						</table>
						</td>
					</tr>
					<tr>
						<td height="1" class="dotline"><img
							src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10"
							height="1"></td>
					</tr>
					<tr>
						<td align="right">
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td align="center"><span>
									<?= $linterface->GET_ACTION_BTN($button_submit, "button","","btnSubmit", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn")?>&nbsp;
									<?= $linterface->GET_ACTION_BTN($Lang['eGuidance']['SubmitAndNotifyUpdate'], "button", "notify_colleague_update('SENCaseID')","btnSubmitAndNotify", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn")?>&nbsp;
									<?= $linterface->GET_ACTION_BTN($button_reset, "reset","","btnReset", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn")?>&nbsp;
									<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='case_index.php'","btnCancel", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn")?>
									</span></td>
								</tr>
							</table>
						</td>
					</tr>
				</table> <br>
			</td>
		</tr>
	</table>
	<input type="hidden" id="OrgStudentID" name="OrgStudentID"
		value="<?=$studentID?>"> <input type="hidden" id="hidNotifierID"
		name="hidNotifierID" value=""> <input type="hidden" id="submitMode"
		name="submitMode" value="submit">
<?php if ($form_action == 'edit_case_update.php'):?>
	<input type="hidden" id="StudentID" name="StudentID" value="<?php echo $rs_sencase['StudentID'];?>">
<?php endif;?>
		
</form>

<?=$libguidance_ui->getAttachmentUploadForm('SENCase',$studentID)?>