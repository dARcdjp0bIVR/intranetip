<?php
/*
 * 	modifying:
 * 
 * 	Log
 *
 *	2017-07-24 [Cameron]
 * 		- add category to support service
 *  
 * 	2017-03-02 [Cameron]
 * 		- create this file
 * 
 */

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/eGuidance_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/guidance_conf.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/libguidance.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/libguidance_ui.php");

intranet_auth();
intranet_opendb();

$libguidance = new libguidance();
$permission = $libguidance->getUserPermission();
if (!$permission['admin'] && !in_array('MGMT', (array)$permission['current_right']['MGMT']['SEN'])) {
	$libguidance->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$libguidance_ui = new libguidance_ui();
$linterface = new interface_html();

$lclass = new libclass();
$classSelection = $lclass->getSelectClass("name='ClassName' id='ClassName' ");
$studentSelection = getSelectByArray(array(),"name='StudentID' id='StudentID'");
$rs_sencase = array();
$senTypeAry = array();
$rs_sencase_adjustment = array();
$rs_sencase_support_study = array();
$rs_sencase_support_other = array();
$studentID = '';

$nameField = ($intranet_session_language == 'b5' || $intranet_session_language == 'gb') ? 'ChineseName' : 'EnglishName';
## get adjustment type
$rs_sen_adjust_type = $libguidance->getSENAdjustType();
$rs_sen_adjust_type = BuildMultiKeyAssoc($rs_sen_adjust_type, 'TypeID', array($nameField), 1);

## get adjustment item
$rs_sen_adjust_item = $libguidance->getSENAdjustItem();
$rs_sen_adjust_item = BuildMultiKeyAssoc($rs_sen_adjust_item, array('TypeID', 'ItemID'), array($nameField, 'WithText'));


## get study service type
$rs_sen_study_service_type = $libguidance->getServiceType('','Study');
$rs_sen_study_service_type = BuildMultiKeyAssoc($rs_sen_study_service_type, 'TypeID', array($nameField), 1);

## get adjustment support study service
$rs_sen_support_study = $libguidance->getSENSupportService('','','Study');
$rs_sen_support_study = BuildMultiKeyAssoc($rs_sen_support_study, array('TypeID','ServiceID'), array($nameField, 'WithText'));

## get other service type
$rs_sen_other_service_type = $libguidance->getServiceType('','Other');
$rs_sen_other_service_type = BuildMultiKeyAssoc($rs_sen_other_service_type, 'TypeID', array($nameField), 1);

## get adjustment support other service
$rs_sen_support_other = $libguidance->getSENSupportService('','','Other');
$rs_sen_support_other = BuildMultiKeyAssoc($rs_sen_support_other, array('TypeID','ServiceID'), array($nameField, 'WithText'));


# menu highlight setting
$CurrentPageArr['eGuidance'] = 1;
$CurrentPage = "PageSEN";
$CurrentPageName = $Lang['eGuidance']['name'];

### Title ###
$TAGS_OBJ = $libguidance->getSENTabs("Case");

$MODULE_OBJ = $libguidance->GET_MODULE_OBJ_ARR();
$PAGE_NAVIGATION[] = array('SEN'.$Lang['eGuidance']['sen']['Tab']['Case'], "case_index.php");
$PAGE_NAVIGATION[] = array($Lang['Btn']['New'], "");

$linterface->LAYOUT_START();
	
$form_action = "new_case_update.php";
include("sen_case.tmpl.php");

$linterface->LAYOUT_STOP();
intranet_closedb();

?>


