<?
/*
 * 	Log
 * 
 * 	Description: handle db action, then output json format data
 *
 *  2018-05-23 [Cameron]
 *      - add SeqNo to case "add_adjustment_item" and "add_support_item"
 *      
 * 	2017-07-24 [Cameron]
 * 		- modify add_support_item to support category for service
 *  
 * 	2017-06-29 [Cameron]
 * 		allow to update ActivityDate when update update_sen_service_activity
 * 
 * 	2017-05-19 [Cameron]
 * 		fix bug: 
 * 		1.	should apply standardizeFormPostValue to trim and stripslashes of $studentResult first,
 * 			then apply Get_Safe_Sql_Query to the field in sql before insert / update to db table, this will eliminate
 * 			the big5 character (HuiGongKoi problem)
 * 		2.  must specify charset as utf-8 for ej, hence must convert big5 to utf-8 for related fields  
 * 
 * 	2017-05-17 [Cameron] fix php5.4 json must pass data as utf-8
 *   
 * 	2017-03-10 [Cameron] create this file
 */

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/eGuidance_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/libguidance.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/libguidance_ui.php");
include_once($PATH_WRT_ROOT."includes/json.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

intranet_auth();
intranet_opendb();

$libguidance = new libguidance();
$permission = $libguidance->getUserPermission();
if (!$permission['admin'] && !in_array('MGMT', (array)$permission['current_right']['MGMT']['SEN'])) {	
	$libguidance->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

if ($junior_mck) {
	$characterset = 'utf-8';		// ej must specify charset as utf-8, ip not need to specify as it's based on utf-8, 
}
header('Content-Type: text/html; charset='.$characterset);

$ljson = new JSON_obj();

$json['success'] = false;
$x = '';
$action = $_GET['action'] ? $_GET['action'] : $_POST['action'];
$remove_dummy_chars = false;	// whether to remove new line, carriage return, tab and back slash

$lclass = new libclass();
$libguidance_ui = new libguidance_ui();
$ldb = new libdb();
$lfs = new libfilesystem();
$result = array();
$dataAry = array();
$condAry = array();

switch($action) {

	case 'add_sen_service_activity':
	
		$ldb->Start_Trans();
		
		## add activity
		$dataAry['ServiceID'] = $ServiceID;		
		$dataAry['ActivityDate'] = $ActivityDate;
		$dataAry['LastModifiedBy'] = $_SESSION['UserID'];
		$sql = $libguidance->INSERT2TABLE('INTRANET_GUIDANCE_SEN_SERVICE_ACTIVITY',$dataAry,array(),false);
		$res = $ldb->db_db_query($sql);
		$result[] = $res;
		
		if ($res) {
			$activityID = $ldb->db_insert_id();
			foreach((array)$ActivityInfoAry as $studentID=>$studentResult) {
				## add activity result
				unset($dataAry);
				$studentResult = standardizeFormPostValue($studentResult);
				$dataAry['ActivityID'] = $activityID;
				$dataAry['StudentID'] = $studentID;
				$dataAry['Result'] = ($junior_mck) ? convert2unicode($studentResult, 1, $direction=0) : $studentResult;
				$sql = $libguidance->INSERT2TABLE('INTRANET_GUIDANCE_SEN_SERVICE_ACTIVITY_RESULT',$dataAry,array(),false,false,false);
				$result[] = $ldb->db_db_query($sql);
			}
			
			## update attachment				
			if (count($FileID) > 0) {
				$fileID = implode("','",(array)$FileID);
				$sql = "UPDATE INTRANET_GUIDANCE_ATTACHMENT SET RecordID='".$activityID."' WHERE FileID IN ('".$fileID."')";
				$result[] = $ldb->db_db_query($sql);
			}
			
		}
						
		if (!in_array(false,$result)) {
			$ldb->Commit_Trans();
			$orderBy = "ClassName,ClassNumber";
			$student = $libguidance->getSENServiceStudent($ServiceID,$orderBy);
			$student_list = $libguidance_ui->getUserNameList($student);
			$student = BuildMultiKeyAssoc($student, 'UserID');
			$x = $libguidance_ui->getSENServiceActivityTable($ServiceID,$student);
			$json['success'] = true;
			$json['action_result'] = ($junior_mck) ? convert2unicode($Lang['General']['ReturnMessage']['AddSuccess'], 1, $direction=1) : $Lang['General']['ReturnMessage']['AddSuccess'];
		}
		else {
			$ldb->RollBack_Trans();
			$json['success'] = false;
			$json['action_result'] = ($junior_mck) ? convert2unicode($Lang['General']['ReturnMessage']['AddUnsuccess'], 1, $direction=1) : $Lang['General']['ReturnMessage']['AddUnsuccess'];
		}
		
		break;


	case 'update_sen_service_activity':
	
		$ldb->Start_Trans();

		## update activity
		unset($dataAry);
		unset($condAry);
		$dataAry['ActivityDate'] = $ActivityDate;
		$dataAry['LastModifiedBy'] = $_SESSION['UserID'];
		$condAry['ActivityID'] = $ActivityID;
		$sql = $libguidance->UPDATE2TABLE('INTRANET_GUIDANCE_SEN_SERVICE_ACTIVITY',$dataAry,$condAry,false);
		
		$result[] = $ldb->db_db_query($sql);
		
		## update activity result
		foreach((array)$ActivityInfoAry as $studentID=>$studentResult) {
			unset($dataAry);
			unset($condAry);
			## update activity result
			$studentResult = standardizeFormPostValue($studentResult);
			$dataAry['Result'] = ($junior_mck) ? convert2unicode($studentResult, 1, $direction=0) : $studentResult;
			$condAry['ActivityID'] = $ActivityID;
			$condAry['StudentID'] = $studentID;
			$sql = $libguidance->UPDATE2TABLE('INTRANET_GUIDANCE_SEN_SERVICE_ACTIVITY_RESULT',$dataAry,$condAry,false,false);
			$result[] = $ldb->db_db_query($sql);
		}

		## update attachment
		if (count($FileID) > 0) {
			$fileID = implode("','",(array)$FileID);
			$sql = "UPDATE INTRANET_GUIDANCE_ATTACHMENT SET RecordID='".$ActivityID."' WHERE FileID IN ('".$fileID."') AND RecordID=0";
			$result[] = $ldb->db_db_query($sql);
		}
		
		if (!in_array(false,$result)) {
			$ldb->Commit_Trans();
			$orderBy = "ClassName,ClassNumber";
			$student = $libguidance->getSENServiceStudent($ServiceID,$orderBy);
			$student_list = $libguidance_ui->getUserNameList($student);
			$student = BuildMultiKeyAssoc($student, 'UserID');
			$x = $libguidance_ui->getSENServiceActivityTable($ServiceID,$student);
			$json['success'] = true;
			$json['action_result'] = ($junior_mck) ? convert2unicode($Lang['General']['ReturnMessage']['UpdateSuccess'], 1, $direction=1) : $Lang['General']['ReturnMessage']['UpdateSuccess'];
		}
		else {
			$ldb->RollBack_Trans();
			$json['success'] = false;
			$json['action_result'] = ($junior_mck) ? convert2unicode($Lang['General']['ReturnMessage']['UpdateUnsuccess'], 1, $direction=1) : $Lang['General']['ReturnMessage']['UpdateUnsuccess'];
		}
	
		break;


	case 'delete_sen_service_activity':

		if (count($ActivityChk)) {

			$ldb->Start_Trans();
						
			$sql = "DELETE FROM INTRANET_GUIDANCE_SEN_SERVICE_ACTIVITY WHERE ActivityID IN ('".implode("','",(array)$ActivityChk)."')";
			$result[] = $ldb->db_db_query($sql);
			
			$sql = "DELETE FROM INTRANET_GUIDANCE_SEN_SERVICE_ACTIVITY_RESULT WHERE ActivityID IN ('".implode("','",(array)$ActivityChk)."')";
			$result[] = $ldb->db_db_query($sql);
			
			## remove attachment in SENService activity
			$sql = "SELECT 
						FileID, EncodeFileName 
					FROM 
						INTRANET_GUIDANCE_ATTACHMENT
					WHERE 
						Target='SENServiceActivity'
					AND	RecordID IN ('".implode("','",(array)$ActivityChk)."')";
			$rs = $ldb->returnResultSet($sql);
				
			if (count($rs) > 0) {
				foreach((array)$rs as $r) {
					$FileID = $r['FileID'];
					$EncodeFileName = $r['EncodeFileName'];
					$file = $libguidance->eg_path.$EncodeFileName;
					$result[] = $lfs->file_remove($file);
				}
		
				$sql = "DELETE FROM INTRANET_GUIDANCE_ATTACHMENT WHERE Target='SENServiceActivity' AND RecordID IN ('".implode("','",(array)$ActivityChk)."')";
				$result[] = $ldb->db_db_query($sql);
			}
			
			if (!in_array(false,$result)) {
				$ldb->Commit_Trans();
				$orderBy = "ClassName,ClassNumber";
				$student = $libguidance->getSENServiceStudent($ServiceID,$orderBy);
				$student_list = $libguidance_ui->getUserNameList($student);
				$student = BuildMultiKeyAssoc($student, 'UserID');
				$x = $libguidance_ui->getSENServiceActivityTable($ServiceID,$student);
				$json['success'] = true;
				$json['action_result'] = ($junior_mck) ? convert2unicode($Lang['General']['ReturnMessage']['DeleteSuccess'], 1, $direction=1) : $Lang['General']['ReturnMessage']['DeleteSuccess'];
			}
			else {
				$ldb->RollBack_Trans();
				$json['success'] = false;
				$json['action_result'] = ($junior_mck) ? convert2unicode($Lang['General']['ReturnMessage']['DeleteUnsuccess'], 1, $direction=1) : $Lang['General']['ReturnMessage']['DeleteUnsuccess'];
			}
		}
		else {
			$json['success'] = false;
			$json['action_result'] = ($junior_mck) ? convert2unicode($Lang['General']['ReturnMessage']['DeleteUnsuccess'], 1, $direction=1) : $Lang['General']['ReturnMessage']['DeleteUnsuccess'];
		}	
		break;
		
		
	case 'add_adjustment_item':
		
		$dataAry['TypeID'] = $_POST['TypeID'];
		$_POST['ChineseName'] = standardizeFormPostValue($_POST['ChineseName']);
		$_POST['EnglishName'] = standardizeFormPostValue($_POST['EnglishName']);
		$dataAry['ChineseName'] = ($junior_mck) ? convert2unicode($_POST['ChineseName'], 1, $direction=0) : $_POST['ChineseName'];	// Note: don't use $ChineseName and $EnglishName as they stored current login user name
		$dataAry['EnglishName'] = ($junior_mck) ? convert2unicode($_POST['EnglishName'], 1, $direction=0) : $_POST['EnglishName'];
		$dataAry['WithText'] = $_POST['WithText'];
		$nextSeqNo = $libguidance->getNexSeqNo('AdjustItem', $_POST['TypeID']);
		$dataAry['SeqNo'] = $nextSeqNo;		
		$sql = $libguidance->INSERT2TABLE('INTRANET_GUIDANCE_SETTING_ADJUST_ITEM',$dataAry,array(),false,false,false);
		
		$ldb->Start_Trans();
		$result[] = $ldb->db_db_query($sql);
		$itemID = $ldb->db_insert_id();
		$typeID = $dataAry['TypeID'];

		$itemName = ($intranet_session_language == 'b5' || $intranet_session_language == 'gb') ? $dataAry['ChineseName'] : $dataAry['EnglishName'];
		
		if (!in_array(false,$result)) {
			$ldb->Commit_Trans();
			if ($dataAry['WithText']) {
				$x .= '<tr><td>';
					$x .= '<table class="no_bottom_border">';
						$x .= '<tr><td style="padding-left:17px; vertical-align:middle; width:80px; "><input type="checkbox" class="AdjustType_'.$typeID.'" name="ItemID['.$typeID.'][]" id="AdjustItem_'.$typeID.'_'.$itemID.'" value="'.$itemID.'"><label for="AdjustItem_'.$typeID.'_'.$itemID.'">'.$itemName.'</label></td>';
							$x .= '<td>'.$libguidance_ui->GET_TEXTAREA('Arrangement['.$typeID.']['.$itemID.']', '', 60, 4).'</td>';
						$x .= '</tr>';
					$x .= '</table>';
				$x .= '</td></tr>';
			}
			else {
				$x .= '<tr><td style="padding-left:20px;"><input type="checkbox" class="AdjustType_'.$typeID.'" name="ItemID['.$typeID.'][]" id="AdjustItem_'.$typeID.'_'.$itemID.'" value="'.$itemID.'"><label for="AdjustItem_'.$typeID.'_'.$itemID.'">'.$itemName.'</label></td></tr>';
			}

			$json['success'] = true;
			$json['typeID'] = $typeID;
			$json['action_result'] = ($junior_mck) ? convert2unicode($Lang['General']['ReturnMessage']['AddSuccess'], 1, $direction=1) : $Lang['General']['ReturnMessage']['AddSuccess'];
		}
		else {
			$ldb->RollBack_Trans();
			$json['success'] = false;
			$json['action_result'] = ($junior_mck) ? convert2unicode($Lang['General']['ReturnMessage']['AddUnsuccess'], 1, $direction=1) : $Lang['General']['ReturnMessage']['AddUnsuccess'];
		}
	
		break;	

	case 'add_support_item':
	
		$dataAry['TypeID'] = $_POST['TypeID'];
		$_POST['ChineseName'] = standardizeFormPostValue($_POST['ChineseName']);
		$_POST['EnglishName'] = standardizeFormPostValue($_POST['EnglishName']);
		$dataAry['ChineseName'] = ($junior_mck) ? convert2unicode($_POST['ChineseName'], 1, $direction=0) : $_POST['ChineseName'];	// Note: don't use $ChineseName and $EnglishName as they stored current login user name
		$dataAry['EnglishName'] = ($junior_mck) ? convert2unicode($_POST['EnglishName'], 1, $direction=0) : $_POST['EnglishName'];
		$dataAry['WithText'] = $_POST['WithText'];
		$nextSeqNo = $libguidance->getNexSeqNo('ServiceItem', $_POST['TypeID']);
		$dataAry['SeqNo'] = $nextSeqNo;		
		$sql = $libguidance->INSERT2TABLE('INTRANET_GUIDANCE_SETTING_SUPPORT_SERVICE',$dataAry,array(),false,false,false);
		
		$ldb->Start_Trans();
		$result[] = $ldb->db_db_query($sql);
		$itemID = $ldb->db_insert_id();
		$typeID = $dataAry['TypeID'];

		$itemName = ($intranet_session_language == 'b5' || $intranet_session_language == 'gb') ? $dataAry['ChineseName'] : $dataAry['EnglishName'];
		
		if (!in_array(false,$result)) {
			$ldb->Commit_Trans();
			if ($dataAry['WithText']) {
				$x .= '<tr><td>';
					$x .= '<table class="no_bottom_border">';
						$x .= '<tr><td style="padding-left:17px; vertical-align:middle; width:80px; "><input type="checkbox" class="ServiceType_'.$typeID.'" name="ServiceItemID['.$typeID.'][]" id="ServiceItem_'.$typeID.'_'.$itemID.'" value="'.$itemID.'"><label for="ServiceItem_'.$typeID.'_'.$itemID.'">'.$itemName.'</label></td>';
							$x .= '<td>'.$libguidance_ui->GET_TEXTAREA('ServiceArrangement['.$typeID.']['.$itemID.']', '', 60, 4).'</td>';
						$x .= '</tr>';
					$x .= '</table>';
				$x .= '</td></tr>';
			}
			else {
				$x .= '<tr><td style="padding-left:20px;"><input type="checkbox" class="ServiceType_'.$typeID.'" name="ServiceItemID['.$typeID.'][]" id="ServiceItem_'.$typeID.'_'.$itemID.'" value="'.$itemID.'"><label for="ServiceItem_'.$typeID.'_'.$itemID.'">'.$itemName.'</label></td></tr>';
			}
			
			$json['success'] = true;
			$json['typeID'] = $typeID;			
			$json['action_result'] = ($junior_mck) ? convert2unicode($Lang['General']['ReturnMessage']['AddSuccess'], 1, $direction=1) : $Lang['General']['ReturnMessage']['AddSuccess'];
		}
		else {
			$ldb->RollBack_Trans();
			$json['success'] = false;
			$json['action_result'] = ($junior_mck) ? convert2unicode($Lang['General']['ReturnMessage']['AddUnsuccess'], 1, $direction=1) : $Lang['General']['ReturnMessage']['AddUnsuccess'];
		}
	
		break;		
			
}

if ($remove_dummy_chars) {
	$x = remove_dummy_chars_for_json($x);
}

if ($junior_mck) {
	$x = convert2unicode($x,true,1);		// json_encode must pass data as utf-8
}

$json['html'] = $x;
echo $ljson->encode($json);

intranet_closedb();
?>