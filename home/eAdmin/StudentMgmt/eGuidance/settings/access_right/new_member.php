<?php
/*
 * 	modifying:
 * 
 * 	Log
 * 
 * 	2017-08-04 [Cameron]
 * 		- include common_js.php
 * 
 * 	2017-05-19 [Cameron]
 * 		fix bug: disable button after submit to avoid adding duplicate record
 * 
 * 	2017-03-16 [Cameron]
 * 		- create this file
 * 
 */

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/eGuidance_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/guidance_conf.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/libguidance.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/libguidance_ui.php");

intranet_auth();
intranet_opendb();

$libguidance = new libguidance();
$permission = $libguidance->getUserPermission();
if (!$permission['admin'] && !in_array('MGMT', (array)$permission['current_right']['SETTINGS']['PERMISSIONSETTING'])) {
	$libguidance->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$linterface = new interface_html();
$libguidance_ui = new libguidance_ui();

if (count($GroupID)==1) {
	$rs_group = $libguidance->getAccessRightGroupInfo($GroupID);
	if (count($rs_group) == 1) {
		$rs_group = $rs_group[0];
	}	
}

# menu highlight setting
$CurrentPageArr['eGuidance'] = 1;
$CurrentPage = "PagePermissionSetting";
$CurrentPageName = $Lang['eGuidance']['name'];

### Title ###
$TAGS_OBJ = array();
$TAGS_OBJ[] = array($Lang['eGuidance']['menu']['Settings']['PermissionSetting']);

$MODULE_OBJ = $libguidance->GET_MODULE_OBJ_ARR();
$PAGE_NAVIGATION[] = array($Lang['eGuidance']['settings']['Permission']['GroupList'], "index.php");
$PAGE_NAVIGATION[] = array($rs_group['GroupTitle'], "view.php?GroupID=".$GroupID);
$PAGE_NAVIGATION[] = array($Lang['Btn']['New'], "");

$linterface->LAYOUT_START();

?>
<script type="text/javascript">
<?include($PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eGuidance/common_js.php");?>

$(document).ready(function(){
	
	$('#btnSubmit').click(function(e) {
	    e.preventDefault();
	    $('input.actionBtn').attr('disabled', 'disabled');
	    $('#TeacherID option').attr('selected',true);
	    if (($('#TeacherID option:selected').length <= 0) || (($('#TeacherID option:selected').length == 1) && ($.trim($('#TeacherID option:first').val()) == ''))) {
		    alert('<?=$Lang['eGuidance']['settings']['Permission']['SelectTeacher']?>');
		    $('input.actionBtn').attr('disabled', '');
		    return false;
	    }
		else {
			$('#form1').submit();
		}
	});	
});
</script>

<form name="form1" id="form1" method="post" action="new_member_update.php">
<table id="main_table" width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td class="navigation"><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="5" cellspacing="0">
				<tr>
					<td>
						<table align="center" class="form_table_v30">
							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['eGuidance']['settings']['Permission']['SelectTeacher']?>
									<span class="tabletextrequire">*</span></td>
								<td class="tabletext"><?=$libguidance_ui->getTeacherSelection()?>
								</td>
							</tr>
							<tr>
								<td valign="top" nowrap="nowrap" class="tabletextremark"><?=$i_general_required_field?></td>
								<td width="80%">&nbsp;</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td height="1" class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
				</tr>
				<tr>
					<td align="right">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="center">
									<span>
									<?= $linterface->GET_ACTION_BTN($button_submit, "button","","btnSubmit", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn")?>&nbsp;
									<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='member_list.php?GroupID=$GroupID'","btnCancel", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn")?>
									</span></td>
							</tr>
						</table>
					</td>
				</tr>
			</table><br>
		</td>
	</tr>
</table>
<input type="hidden" id="GroupID" name="GroupID" value="<?=$GroupID?>">
</form>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();

?>


