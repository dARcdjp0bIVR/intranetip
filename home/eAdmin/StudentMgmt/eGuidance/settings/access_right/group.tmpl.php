<?
/*
 * 	Log
 * 
 * 	2017-05-19 [Cameron]
 * 		fix bug: disable button after submit to avoid adding duplicate record
 */
?>
<script type="text/javascript">

function checkForm(obj) 
{
	if(!check_text(obj.GroupTitle,'<?=$Lang['eGuidance']['settings']['Warning']['InputGroupName']?>'))
	{
		return false;
	}
	return true;
}

$(document).ready(function(){
	
	$('#btnSubmit').click(function(e) {
		 
	    e.preventDefault();
	    $('input.actionBtn').attr('disabled', 'disabled');
	    var url;
<? if ($form_action == "edit_update.php"):?>
		url = '../../ajax/ajax.php?action=checkBeforeUpdateGroup'
<? else:?>
		url = '../../ajax/ajax.php?action=checkBeforeAddGroup'
<? endif;?>

		if (checkForm(document.form1)) {
	        $.ajax({
	        	dataType: "json", 
	            type: 'post',
	            url: url, 
	            data: $('#form1').serialize(),
	            async: false,            
		        success: function(ajaxReturn) {
		        	if (ajaxReturn != null && ajaxReturn.success){
		        		if (ajaxReturn.html == '1') {
		        			alert('<?=$Lang['eGuidance']['settings']['Warning']['DuplicateGroupName']?>');
		        			$('input.actionBtn').attr('disabled', '');
		        		}
		        		else {
		        			$('#form1').submit();
		        		}
		        	}
		        	else {
		        		$('input.actionBtn').attr('disabled', '');
		        	}
		        },
				error: show_ajax_error
	        });
		}
		else {
			$('input.actionBtn').attr('disabled', '');
		}
	});
		
});

function show_ajax_error() {
	alert('<?=$Lang['eGuidance']['error']['ajax']?>');
}
	
</script>

<form name="form1" id="form1" method="post" action="<?=$form_action?>">
<table id="main_table" width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td class="navigation"><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="5" cellspacing="0">
				<tr>
					<td>
						<table width="100%" cellpadding="5" cellspacing="0">
							<tr>
								<td width="60%"><?=$linterface->GET_NAVIGATION2($Lang['eGuidance']['settings']['Permission']['GroupInfo']);?></td>
								<td width="40%" align="right"><?=$linterface->GET_SYS_MSG('',$xmsg2) ?></td>
							</tr>
						</table>
					
						<table align="center" class="form_table_v30">
							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['eGuidance']['settings']['Permission']['GroupTitle']?>
									<span class="tabletextrequire">*</span></td>
								<td class="tabletext"><input type="text" name="GroupTitle" id="GroupTitle" class="textboxtext" value="<?=intranet_htmlspecialchars($rs_group['GroupTitle'])?>"></td>
							</tr>
							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['eGuidance']['settings']['Permission']['GroupDescription']?>
									</td>
								<td class="tabletext"><?=$linterface->GET_TEXTAREA("GroupDescription", $rs_group['GroupDescription'], 80, 10)?></td>
							</tr>
							<tr>
								<td valign="top" nowrap="nowrap" class="tabletextremark"><?=$i_general_required_field?></td>
								<td width="80%">&nbsp;</td>
							</tr>
						</table>
						
						<table width="88%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td><?=$linterface->GET_NAVIGATION2($Lang['eGuidance']['settings']['Permission']['GroupPermission']);?></td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td class="form_sep_title"><i>- <?=$Lang['eGuidance']['menu']['main']['Management'] ?> -</i>
									<table class="common_table_list_v30 ">
										<tr class="tabletop">
											<th width="30%"><?=$Lang['eGuidance']['Item']?></th>
											<th width="20%"><?=$Lang['eGuidance']['settings']['Permission']['View']?></th>
											<th width="20%"><?=$Lang['eGuidance']['settings']['Permission']['Management']?></th>
											<th width="20%"><?=$Lang['eGuidance']['settings']['Permission']['NoRights']?></th>
										</tr>
								<? 	foreach((array)$Lang['eGuidance']['menu']['Management'] as $k=>$v) {
										if (!$plugin['eGuidance_module']['SelfImprove'] && ($k == 'SelfImprove')) {
											$x = '';
										}
										else {
											$x = '<tr>';
												$x .= '<td>'.$v.'</td>';
												$x .= '<td><input type="radio" name="Perm[MGMT]['.$k.']" id="view_'.$k.'" value="view" '.(($rs_access_right[$k]['Action']=='view')?'checked':'').'></td>';
												$x .= '<td><input type="radio" name="Perm[MGMT]['.$k.']" id="mgmt_'.$k.'" value="mgmt" '.(($rs_access_right[$k]['Action']=='mgmt')?'checked':'').'></td>';
												$x .= '<td><input type="radio" name="Perm[MGMT]['.$k.']" id="none_'.$k.'" value="none" '.((!isset($rs_access_right[$k]['Action']) || empty($rs_access_right[$k]['Action']) || ($rs_access_right[$k]['Action'] == 'none'))?'checked':'').'></td>';
											$x .= '<tr>';
										}
										echo $x;
									}
								?>
									</table>									
								</td>
							</tr>
							
							<tr>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td class="form_sep_title"><i>- <?=$Lang['eGuidance']['menu']['main']['Reports'] ?> -</i>
									<table class="common_table_list_v30 ">
										<tr class="tabletop">
											<th width="30%"><?=$Lang['eGuidance']['Item']?></th>
											<th width="20%"><?=$Lang['eGuidance']['settings']['Permission']['View']?></th>
											<th width="20%"><?=$Lang['eGuidance']['settings']['Permission']['NoRights']?></th>
										</tr>
								<? 	foreach((array)$Lang['eGuidance']['menu']['Reports'] as $k=>$v) {
										$x = '<tr>';
											$x .= '<td>'.$v.'</td>';
											$x .= '<td><input type="radio" name="Perm[REPORTS]['.$k.']" id="view_'.$k.'" value="view" '.(($rs_access_right[$k]['Action']=='view')?'checked':'').'></td>';
											$x .= '<td><input type="radio" name="Perm[REPORTS]['.$k.']" id="none_'.$k.'" value="none" '.((!isset($rs_access_right[$k]['Action']) || empty($rs_access_right[$k]['Action']) || ($rs_access_right[$k]['Action'] == 'none'))?'checked':'').'></td>';
										$x .= '<tr>';
										echo $x;
									}
								?>
									</table>									
								</td>
							</tr>

							<tr>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td class="form_sep_title"><i>- <?=$Lang['eGuidance']['menu']['main']['Settings'] ?> -</i>
									<table class="common_table_list_v30 ">
										<tr class="tabletop">
											<th width="30%"><?=$Lang['eGuidance']['Item']?></th>
											<th width="20%"><?=$Lang['eGuidance']['settings']['Permission']['View']?></th>
											<th width="20%"><?=$Lang['eGuidance']['settings']['Permission']['Management']?></th>
											<th width="20%"><?=$Lang['eGuidance']['settings']['Permission']['NoRights']?></th>
										</tr>
								<? 	foreach((array)$Lang['eGuidance']['menu']['Settings'] as $k=>$v) {
										$x = '<tr>';
											$x .= '<td>'.$v.'</td>';
											$x .= '<td><input type="radio" name="Perm[SETTINGS]['.$k.']" id="view_'.$k.'" value="view" '.(($rs_access_right[$k]['Action']=='view')?'checked':'').'></td>';
											$x .= '<td><input type="radio" name="Perm[SETTINGS]['.$k.']" id="mgmt_'.$k.'" value="mgmt" '.(($rs_access_right[$k]['Action']=='mgmt')?'checked':'').'></td>';
											$x .= '<td><input type="radio" name="Perm[SETTINGS]['.$k.']" id="none_'.$k.'" value="none" '.((!isset($rs_access_right[$k]['Action']) || empty($rs_access_right[$k]['Action']) || ($rs_access_right[$k]['Action'] == 'none'))?'checked':'').'></td>';
										$x .= '<tr>';
										echo $x;
									}
								?>
									</table>									
								</td>
							</tr>
							
						</table>
					</td>
				</tr>
				<tr>
					<td height="1" class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
				</tr>
				<tr>
					<td align="right">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="center">
									<span>
									<?= $linterface->GET_ACTION_BTN($button_submit, "button","","btnSubmit", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn")?>&nbsp;
									<?= $linterface->GET_ACTION_BTN($button_reset, "reset","","btnReset", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn")?>&nbsp;
									<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='index.php'","btnCancel", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn")?>
									</span></td>
							</tr>
						</table>
					</td>
				</tr>
			</table><br>
		</td>
	</tr>
</table>
<input type=hidden id="GroupID" name="GroupID" value="<?=$groupID?>">
</form>
