<?php
/*
 * 	modifying:
 * 
 * 	Log
 * 
 * 	2017-05-19 [Cameron]
 * 		- apply standardizeFormPostValue() to text field
 * 
 * 	2017-03-15 [Cameron]
 * 		- create this file
 * 
 */

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/eGuidance_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/libguidance.php");

intranet_auth();
intranet_opendb();

$libguidance = new libguidance();
$permission = $libguidance->getUserPermission();
if (!$permission['admin'] && !in_array('MGMT', (array)$permission['current_right']['SETTINGS']['PERMISSIONSETTING'])) {
	$libguidance->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$ldb = new libdb();
$result = array();
$dataAry = array();
$condAry = array();
	
$dataAry['GroupTitle'] = standardizeFormPostValue($GroupTitle);
$dataAry['GroupDescription'] = standardizeFormPostValue($GroupDescription);
$sql = $libguidance->UPDATE2TABLE('ACCESS_RIGHT_GROUP',$dataAry,array('GroupID'=>$GroupID),false);

$ldb->Start_Trans();
## step 1: update to access right group
$res = $ldb->db_db_query($sql);
$result[] = $res;
if ($res) {
	## step 2: add/update access right group setting
	foreach((array)$Perm as $section=>$sec) {
		foreach((array)$sec as $function=>$action) {
			
			if ($libguidance->isAccessRightExist($GroupID,$section,$function)) {	// update
				if ($action != 'none') {
					unset($dataAry);
					unset($condAry);
					$dataAry['Action'] = $action;
					if (!$junior_mck) {
						$dataAry['LastModifiedBy'] = $_SESSION['UserID'];
					}
					
					$condAry['GroupID'] = $GroupID;
					$condAry['Module'] = $libguidance->Module;
					$condAry['Section'] = $section;
					$condAry['Function'] = $function;				
					$sql = $libguidance->UPDATE2TABLE('ACCESS_RIGHT_GROUP_SETTING',$dataAry,$condAry,false);
					$result[] = $ldb->db_db_query($sql);
				}
				else {
					$sql  = "DELETE FROM ACCESS_RIGHT_GROUP_SETTING WHERE GroupID='".$GroupID."'";
					$sql .= " AND Module='".$libguidance->Module."'";
					$sql .= " AND Section='".$section."' AND Function='".$function."'";
					
					$result[] = $ldb->db_db_query($sql);
				}
			}
			else {	// insert, case when add new item later
				if ($action != 'none') {
					unset($dataAry);
					$dataAry['GroupID'] = $GroupID;
					$dataAry['Module'] = $libguidance->Module;
					$dataAry['Section'] = $section;
					$dataAry['Function'] = $function;
					$dataAry['Action'] = $action;
					$dataAry['DateInput'] = 'now()';
					if (!$junior_mck) {
						$dataAry['LastModifiedBy'] = $_SESSION['UserID'];
					}
					$sql = $libguidance->INSERT2TABLE('ACCESS_RIGHT_GROUP_SETTING',$dataAry,array(),false);
					$result[] = $ldb->db_db_query($sql);
				}
			}
			
		}		
	}
}

if (!in_array(false,$result)) {
	$ldb->Commit_Trans();
	$returnMsgKey = 'UpdateSuccess';
}
else {
	$ldb->RollBack_Trans();
	$returnMsgKey = 'UpdateUnsuccess';
}
	
header("location: index.php?returnMsgKey=".$returnMsgKey);


intranet_closedb();

?>


