<?php
/*
 * 	modifying:
 * 
 * 	Log
 * 
 * 	2017-03-15 [Cameron]
 * 		- create this file
 * 
 */

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/eGuidance_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/guidance_conf.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/libguidance.php");

intranet_auth();
intranet_opendb();

$libguidance = new libguidance();
$permission = $libguidance->getUserPermission();
if (!$permission['admin'] && !in_array('MGMT', (array)$permission['current_right']['SETTINGS']['PERMISSIONSETTING'])) {	
	$libguidance->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$linterface = new interface_html();

# menu highlight setting
$CurrentPageArr['eGuidance'] = 1;
$CurrentPage = "PagePermissionSetting";
$CurrentPageName = $Lang['eGuidance']['name'];

### Title ###
$TAGS_OBJ = array();
$TAGS_OBJ[] = array($Lang['eGuidance']['menu']['Settings']['PermissionSetting']);

$MODULE_OBJ = $libguidance->GET_MODULE_OBJ_ARR();
$PAGE_NAVIGATION[] = array($Lang['eGuidance']['settings']['Permission']['GroupList'], "index.php");
$PAGE_NAVIGATION[] = array($Lang['Btn']['Edit'], "");


$linterface->LAYOUT_START();

if (is_array($GroupID)) {
	$groupID = $GroupID[0];
}
else {
	$groupID = $GroupID;
}

$rs_group = $libguidance->getAccessRightGroupInfo($groupID);

if (count($rs_group) == 1) {
	$rs_group = $rs_group[0];
	$rs_access_right = $libguidance->getAccessRight($groupID);
	$rs_access_right = BuildMultiKeyAssoc($rs_access_right, 'Function');
}

$form_action = "edit_update.php";
include("group.tmpl.php");

$linterface->LAYOUT_STOP();
intranet_closedb();

?>


