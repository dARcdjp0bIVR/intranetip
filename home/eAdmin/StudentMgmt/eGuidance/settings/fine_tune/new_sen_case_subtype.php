<?php
/*
 * modifying:
 *
 * Log
 *
 * 2018-04-13 [Cameron]
 * - create this file
 *
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "lang/eGuidance_lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/eGuidance/guidance_conf.php");
include_once ($PATH_WRT_ROOT . "includes/eGuidance/libguidance.php");

intranet_auth();
intranet_opendb();

$libguidance = new libguidance();
$permission = $libguidance->getUserPermission();
if (! $permission['admin'] && ! in_array('MGMT', (array) $permission['current_right']['SETTINGS']['FINETUNESETTING'])) {
    $libguidance->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

$settingID = $_GET['SettingID'];
if (is_array($settingID)) {
    $settingID = $settingID[0];
}

if (! $settingID) {
    header("location: sen_case_type_index.php");
}

$rs_sen_case_type = $libguidance->getSENCaseType($settingID);
if (count($rs_sen_case_type) == 1) {
    if ($intranet_session_language == 'b5' || $intranet_session_language == 'gb') {
        $sen_case_type_name = $rs_sen_case_type['ChineseName'];
    } else {
        $sen_case_type_name = $rs_sen_case_type['EnglishName'];
    }
} else {
    $sen_case_type_name = '';
}

$linterface = new interface_html();

// menu highlight setting
$CurrentPageArr['eGuidance'] = 1;
$CurrentPage = "PageFineTuneSetting";
$CurrentPageName = $Lang['eGuidance']['name'];

// ## Title ###
$TAGS_OBJ = $libguidance->getFineTuneTabs("CaseType");

$MODULE_OBJ = $libguidance->GET_MODULE_OBJ_ARR();
$PAGE_NAVIGATION[] = array(
    $sen_case_type_name,
    "sen_case_subtype_index.php?SettingID=$settingID"
);
$PAGE_NAVIGATION[] = array(
    $Lang['Btn']['New'],
    ""
);

$linterface->LAYOUT_START();

$form_action = "new_sen_case_subtype_update.php";
include ("sen_case_subtype.tmpl.php");

$linterface->LAYOUT_STOP();
intranet_closedb();

?>


