<?php
/*
 * modifying:
 *
 * Log
 *
 * 2018-05-23 [Cameron]
 * - add SeqNo
 *
 * 2017-07-21 [Cameron] change Type to WithText, add TypeID
 *
 * 2017-05-19 [Cameron]
 * - apply standardizeFormPostValue() to text field
 *
 * 2017-03-17 [Cameron]
 * - create this file
 *
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "lang/eGuidance_lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/eGuidance/libguidance.php");

intranet_auth();
intranet_opendb();

$libguidance = new libguidance();
$permission = $libguidance->getUserPermission();
if (! $permission['admin'] && ! in_array('MGMT', (array) $permission['current_right']['SETTINGS']['FINETUNESETTING'])) {
    $libguidance->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

$ldb = new libdb();
$result = array();
$dataAry = array();

$dataAry['TypeID'] = $_POST['TypeID'];
$dataAry['ChineseName'] = standardizeFormPostValue($_POST['ChineseName']); // Note: don't use $ChineseName and $EnglishName as they stored current login user name
$dataAry['EnglishName'] = standardizeFormPostValue($_POST['EnglishName']);
$dataAry['WithText'] = $_POST['WithText'];
$nextSeqNo = $libguidance->getNexSeqNo('ServiceItem', $_POST['TypeID']);
$dataAry['SeqNo'] = $nextSeqNo;
$sql = $libguidance->INSERT2TABLE('INTRANET_GUIDANCE_SETTING_SUPPORT_SERVICE', $dataAry, array(), false, false, false);

$ldb->Start_Trans();
// # step 1: add to support service
$res = $ldb->db_db_query($sql);
$result[] = $res;

if (! in_array(false, $result)) {
    $ldb->Commit_Trans();
    $returnMsgKey = 'AddSuccess';
} else {
    $ldb->RollBack_Trans();
    $returnMsgKey = 'AddUnsuccess';
}

header("location: support_index.php?TypeID=" . $TypeID . "&returnMsgKey=" . $returnMsgKey);

intranet_closedb();

?>


