<?php
/*
 * modifying:
 *
 * Log
 *
 * 2018-05-23 [Cameron]
 * - show scope in service type name
 *
 * 2017-07-21 [Cameron]
 * - modify to let item under a type
 *
 * 2017-03-17 [Cameron]
 * - create this file
 *
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "lang/eGuidance_lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/eGuidance/guidance_conf.php");
include_once ($PATH_WRT_ROOT . "includes/eGuidance/libguidance.php");

intranet_auth();
intranet_opendb();

$libguidance = new libguidance();
$permission = $libguidance->getUserPermission();
if (! $permission['admin'] && ! in_array('MGMT', (array) $permission['current_right']['SETTINGS']['FINETUNESETTING'])) {
    $libguidance->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

$linterface = new interface_html();

if (count($TypeID) == 1) {
    $rs_service_type = $libguidance->getServiceType($TypeID);
    if (count($rs_service_type) == 1) {
        $rs_service_type = current($rs_service_type);
        if ($intranet_session_language == 'b5' || $intranet_session_language == 'gb') {
            $service_type_name = $rs_service_type['ChineseName'];
        } else {
            $service_type_name = $rs_service_type['EnglishName'];
        }
        
        if ($rs_service_type['Type'] == 'Study') {
            $service_type_name .= ' - ' . $Lang['eGuidance']['settings']['FineTune']['SupportType']['Study'];
        } else {
            $service_type_name .= ' - ' . $Lang['eGuidance']['settings']['FineTune']['SupportType']['Other'];
        }
    }
}

// menu highlight setting
$CurrentPageArr['eGuidance'] = 1;
$CurrentPage = "PageFineTuneSetting";
$CurrentPageName = $Lang['eGuidance']['name'];

// ## Title ###
$TAGS_OBJ = $libguidance->getFineTuneTabs("Support");

$MODULE_OBJ = $libguidance->GET_MODULE_OBJ_ARR();
$PAGE_NAVIGATION[] = array(
    $service_type_name,
    "support_index.php?TypeID=$TypeID"
);
$PAGE_NAVIGATION[] = array(
    $Lang['Btn']['New'],
    ""
);

$linterface->LAYOUT_START();

$form_action = "new_support_update.php";
include ("support_service.tmpl.php");

$linterface->LAYOUT_STOP();
intranet_closedb();

?>


