<?php
/*
 * Log
 *
 * 2018-05-23 [Cameron] create this file
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "lang/eGuidance_lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/eGuidance/guidance_conf.php");
include_once ($PATH_WRT_ROOT . "includes/eGuidance/libguidance.php");
include_once ($PATH_WRT_ROOT . "includes/eGuidance/libguidance_ui.php");

intranet_auth();
intranet_opendb();

$libguidance = new libguidance();
$permission = $libguidance->getUserPermission();
if (! $permission['admin'] && ! $permission['current_right']['SETTINGS']['FINETUNESETTING']) {
    $libguidance->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

$typeID = $_GET['TypeID'];
if (! $typeID) {
    header("location: index.php");
}

$linterface = new interface_html();

if (count($typeID) == 1) {
    $rs_service_type = $libguidance->getServiceType($typeID);
    if (count($rs_service_type) == 1) {
        $rs_service_type = current($rs_service_type);
        if ($intranet_session_language == 'b5' || $intranet_session_language == 'gb') {
            $service_type_name = $rs_service_type['ChineseName'];
        } else {
            $service_type_name = $rs_service_type['EnglishName'];
        }
        
        if ($rs_service_type['Type'] == 'Study') {
            $service_type_name .= ' - ' . $Lang['eGuidance']['settings']['FineTune']['SupportType']['Study'];
        } else {
            $service_type_name .= ' - ' . $Lang['eGuidance']['settings']['FineTune']['SupportType']['Other'];
        }
    }
}

$libguidance_ui = new libguidance_ui();

// Page heading setting
$linterface = new interface_html();
$CurrentPageArr['eGuidance'] = 1;
$CurrentPage = "PageFineTuneSetting";
$CurrentPageName = $Lang['eGuidance']['name'];

// ## Title ###
$TAGS_OBJ = $libguidance->getFineTuneTabs("Support");
$MODULE_OBJ = $libguidance->GET_MODULE_OBJ_ARR();
$PAGE_NAVIGATION[] = array(
    $service_type_name,
    "support_index.php?TypeID=$typeID"
);
$PAGE_NAVIGATION[] = array(
    $Lang['eGuidance']['settings']['Reorder'],
    ""
);
$linterface->LAYOUT_START();

?>
<script type="text/javascript"
	src="<?php echo $PATH_WRT_ROOT;?>templates/jquery/jquery.tablednd_0_5.js"></script>

<script>
$(document).ready(function(){
	jsInitDNDTable();
});

function jsInitDNDTable() {
	var JQueryObj1 = $("#ServiceItemTable");
	JQueryObj1.tableDnD({
		onDrop: function(table, row) {
			var rows = table.tBodies[0].rows;
			var RecordOrder = "";
			for (var i=0; i<rows.length; i++) {
				if (rows[i].id != "")
				RecordOrder += rows[i].id+",";
			}
			
	        $.ajax({
	        	dataType: "json", 
	            type: 'post',
	            url: '../../ajax/ajax.php?action=reorderServiceItem', 
	            data: {
	            	DisplayOrderString: RecordOrder
	            },
		        success: function(ajaxReturn) {
		        	if (ajaxReturn != null && ajaxReturn.success){
		        		// do nothing
		        	}
		        	else {
		        		// do nothing
		        	}
		        },
				error: show_ajax_error
	        });
			
		},
		onDragStart: function(table, row) {
		},
		dragHandle: "Dragable", 
		onDragClass: "move_selected"
	});
}

function show_ajax_error() {
	alert('<?=$Lang['eGuidance']['error']['ajax']?>');
}
</script>

<form name="form1" id="form1" method="post" action="">
	<table id="main_table" width="98%" border="0" cellspacing="0"
		cellpadding="5">
		<tr>
			<td align="center">
				<table width="100%" border="0" cellspacing="0" cellpadding="5">
					<tr>
						<td class="navigation"><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
					</tr>
				</table>
			</td>
		</tr>

		<tr>
			<td>
				<div class="table_board">
            		<?php echo $libguidance_ui->getServiceItemBySequence($typeID);?>
            	</div>
			</td>
		</tr>
	</table>
</form>
<?php

$linterface->LAYOUT_STOP();
intranet_closedb();

?>