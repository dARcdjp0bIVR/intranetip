<?php 
/*
 * 	Log
 *
 *  2020-05-13 [Cameron] 
 *      - add reorder SEN case type [case #M151032]
 *      - show SeqNo in list 
 *      - default order by SeqNo 
 *  
 *  2020-03-02 [Cameron] allow catetory to contain subcategory even if it contains words input [case #D178788]
 *  
 * 	2018-04-12 [Cameron] create this file
 */

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/eGuidance_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/guidance_conf.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/libguidance.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

# set cookies
$arrCookies[] = array("ck_page_size", "numPerPage");
$arrCookies[] = array("ck_page_no", "pageNo");
$arrCookies[] = array("ck_page_order", "order");
$arrCookies[] = array("ck_page_field", "field");

$arrCookies[] = array("ck_keyword", "keyword");
if(isset($clearCoo) && $clearCoo == 1)
{
	clearCookies($arrCookies);
} 
else 
{
	updateGetCookies($arrCookies);
}


intranet_auth();
intranet_opendb();

$libguidance = new libguidance();
$permission = $libguidance->getUserPermission();
if (!$permission['admin'] && !$permission['current_right']['SETTINGS']['FINETUNESETTING']) {
	$libguidance->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

# Page heading setting
$linterface = new interface_html();
$CurrentPageArr['eGuidance'] = 1;
$CurrentPage = "PageFineTuneSetting";
$CurrentPageName = $Lang['eGuidance']['name'];

### Title ###
$TAGS_OBJ = $libguidance->getFineTuneTabs("CaseType");

$MODULE_OBJ = $libguidance->GET_MODULE_OBJ_ARR();
$returnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];
$linterface->LAYOUT_START($returnMsg);

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$pageNo = ($pageNo == "") ? 1 : $pageNo;
$order = ($order == "") ? 1 : $order;
$field = ($field == "") ? 5 : $field;       // default order by SeqNo

if ($intranet_session_language == 'b5' || $intranet_session_language == 'gb') {
	$nameFieldAry = array('ChineseName','EnglishName');  	
}
else {
	$nameFieldAry = array('EnglishName','ChineseName');
}
 
$cond = '';
$keyword = $_POST['keyword'] ? standardizeFormPostValue($_POST['keyword']) : standardizeFormGetValue($keyword);
if ($keyword != '') {
	$cond = " and (a.Code LIKE '%".$libguidance->Get_Safe_Sql_Like_Query($keyword)."%' OR a.ChineseName LIKE '%".$libguidance->Get_Safe_Sql_Like_Query($keyword)."%' OR a.EnglishName LIKE '%".$libguidance->Get_Safe_Sql_Like_Query($keyword)."%')" ;
}

$li = new libdbtable2007($field, $order, $pageNo);

$sql = "SELECT
				CONCAT('<a href=\"sen_case_subtype_index.php?SettingID=',a.SettingID,'\">',a.Code,'</a>'),
				a.".$nameFieldAry[0]. ",
                a.".$nameFieldAry[1]. ",
                IF(a.IsWithText=1,'".$Lang['General']['Yes']."','".$Lang['General']['No']."') AS IsWithText,
				CONCAT('<a class=\"tablelink\" href=\"sen_case_subtype_index.php?SettingID=',a.SettingID,'\">',COUNT(s.SettingID),'</a>'),
                a.SeqNo ";

if ($permission['admin'] || in_array('MGMT', (array)$permission['current_right']['SETTINGS']['FINETUNESETTING'])) {
	$sql .= ",CONCAT('<input type=\"checkbox\" name=\"SettingID[]\" id=\"SettingID[]\" value=\"', a.SettingID,'\">') ";
	$extra_column = 2;
	$checkbox_col = "<th width='1'>".$li->check("SettingID[]")."</th>\n";
	$toolbar = $linterface->GET_LNK_NEW("javascript:checkGet(document.form1,'new_sen_case_type.php')",$button_new,"","","",0);
	$toolbar .= "&nbsp;" . $linterface->GET_LNK_GENERATE("javascript:checkGet(document.form1,'reorder_sen_case_type.php')", $Lang['eGuidance']['settings']['Reorder'], "", "", "", 1);
	if (! $junior_mck) {
	    $toolbar = '<div class="Conntent_tool">' . $toolbar . '</div>';
	}
	$manage_record_bar  = '<a href="javascript:checkEdit(document.form1,\'SettingID[]\',\'edit_sen_case_type.php\')" class="tool_edit">'.$button_edit.'</a>';
	$manage_record_bar .= '<a id="btnRemove" href="" class="tool_delete">'.$button_delete.'</a>';
}
else {
	$extra_column = 1;
	$checkbox_col = "";
	$toolbar = "";
	$manage_record_bar  = '';
}
$sql .= ",a.Code, COUNT(s.SettingID) AS NumOfItem ";
$sql .= "FROM
				INTRANET_GUIDANCE_SETTING_ITEM a
         LEFT JOIN 
                INTRANET_GUIDANCE_SETTING_ITEM s ON s.ParentSettingID=a.SettingID
		 WHERE a.Form='SENCase' 
         AND   a.Type='SENType' ".$cond . " GROUP BY a.SettingID";


//debug_pr($sql);

$li->field_array = array('Code',"{$nameFieldAry[0]}", "{$nameFieldAry[1]}",'IsWithText','NumOfItem', 'SeqNo');
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+$extra_column;
$li->title = $Lang['General']['Record'];
$li->IsColOff = "IP25_table";

$pos = 0;
$li->column_list .= "<th width='1' class='tabletoplink'>#</th>\n";
$li->column_list .= "<th width='10%' >".$li->column($pos++, $Lang['eGuidance']['settings']['FineTune']['CaseTypeCode'])."</th>\n";
$li->column_list .= "<th width='29%' >".$li->column($pos++, $Lang['eGuidance']['settings']['FineTune']['CaseType'.$nameFieldAry[0]])."</th>\n";
$li->column_list .= "<th width='29%' >".$li->column($pos++, $Lang['eGuidance']['settings']['FineTune']['CaseType'.$nameFieldAry[1]])."</th>\n";
$li->column_list .= "<th width='10%'>".$li->column($pos++, $Lang['eGuidance']['settings']['FineTune']['WithText'])."</th>\n";
$li->column_list .= "<th width='10%'>".$li->column($pos++, $Lang['eGuidance']['settings']['FineTune']['NumberOfSubType'])."</th>\n";
$li->column_list .= "<th width='10%'>".$li->column($pos++, $Lang['eGuidance']['settings']['SeqNo'])."</th>\n";
$li->column_list .= $checkbox_col;


$htmlAry['searchBox'] = $linterface->Get_Search_Box_Div('keyword', $keyword);

?>
<script>
$(document).ready(function(){
	
	$('#btnRemove').click(function(e) {
	    e.preventDefault();
	    if ($('#SettingID\\\[\\\]:checked').length<=0) {
            alert(globalAlertMsg2);
	    }
        else {
	        $.ajax({
	        	dataType: "json", 
	            type: 'post',
	            url: '../../ajax/ajax.php?action=checkBeforeDeleteSenCaseType', 
	            data: $('#form1').serialize(),
	            async: false,            
		        success: function(ajaxReturn) {
		        	if (ajaxReturn != null && ajaxReturn.success){
		        		if (ajaxReturn.html == '1') {
		        			alert('<?=$Lang['eGuidance']['settings']['FineTune']['Warning']['CannotDeleteSenCaseType']?>');
		        		}
		        		else {
		        			if(confirm(globalAlertMsg3)){
			        			$('#form1').attr('action','remove_sen_case_type.php');
			        			$('#form1').submit();
		        			}
		        		}
		        	}
		        },
				error: show_ajax_error
	        });
        }
	});

});

function show_ajax_error() {
	alert('<?=$Lang['eGuidance']['error']['ajax']?>');
}
</script>

<form name="form1" id="form1" method="post" action="sen_case_type_index.php">
	<div class="content_top_tool">
		
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td width="20%"><?=$toolbar ?></td>
				<td width="50%" align="center">&nbsp;</td>
				<td width="30%" style="float: right;">
					<div class="content_top_tool"  style="float: right;">
						<?=$htmlAry['searchBox']?>     
						<br style="clear:both" />
					</div>
				</td>
			</tr>
		</table>
		
	</div>

	<div class="table_board">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td valign="bottom">
				<div class="table_filter">
				</div> 
			</td>
			<td valign="bottom">
				<div class="common_table_tool">
					<?=$manage_record_bar ?>
				</div>
			</td>
		</tr>
	</table>
	</div>

	<div class="table_board">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td valign="bottom">

				<?=$li->display()?>
 
			</td>
		</tr>
	</table>
	</div>

	<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
	<input type="hidden" name="order" value="<?php echo $li->order; ?>" />
	<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
	<input type="hidden" name="page_size_change" value="" />
	<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />

</form>
<?php

$linterface->LAYOUT_STOP();
intranet_closedb();

?>