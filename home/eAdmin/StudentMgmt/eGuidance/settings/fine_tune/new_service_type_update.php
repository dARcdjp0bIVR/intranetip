<?php
/*
 * modifying:
 *
 * Log
 *
 * 2018-05-23 [Cameron]
 * - add SeqNo
 *
 * 2017-07-21 [Cameron]
 * - create this file
 *
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "lang/eGuidance_lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/eGuidance/libguidance.php");

intranet_auth();
intranet_opendb();

$libguidance = new libguidance();
$permission = $libguidance->getUserPermission();
if (! $permission['admin'] && ! in_array('MGMT', (array) $permission['current_right']['SETTINGS']['FINETUNESETTING'])) {
    $libguidance->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

$ldb = new libdb();
$result = array();
$dataAry = array();

$dataAry['ChineseName'] = standardizeFormPostValue($_POST['ChineseName']); // Note: don't use $ChineseName and $EnglishName as they stored current login user name
$dataAry['EnglishName'] = standardizeFormPostValue($_POST['EnglishName']);
$dataAry['Type'] = $_POST['Type'];
$nextSeqNo = $libguidance->getNexSeqNo('ServiceType');
$dataAry['SeqNo'] = $nextSeqNo;
$sql = $libguidance->INSERT2TABLE('INTRANET_GUIDANCE_SETTING_SERVICE_TYPE', $dataAry, array(), false, false, false);

$ldb->Start_Trans();
// # step 1: add to service type
$res = $ldb->db_db_query($sql);
$result[] = $res;

if (! in_array(false, $result)) {
    $ldb->Commit_Trans();
    $returnMsgKey = 'AddSuccess';
} else {
    $ldb->RollBack_Trans();
    $returnMsgKey = 'AddUnsuccess';
}

header("location: service_type_index.php?returnMsgKey=" . $returnMsgKey);

intranet_closedb();

?>


