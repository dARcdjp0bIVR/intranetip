<?
/*
 * 	Log
 * 
 * 	2017-06-29 [Cameron]
 * 		- add label for radio button item so that clicking lable is the same as clicking the radio button
 *  
 */
?>
<script type="text/javascript">

function checkForm(obj) 
{
	if(!check_text(obj.ChineseName,'<?=$Lang['eGuidance']['settings']['FineTune']['Warning']['InputAdjustItemChineseName']?>'))
	{
		return false;
	}
	if(!check_text(obj.EnglishName,'<?=$Lang['eGuidance']['settings']['FineTune']['Warning']['InputAdjustItemEnglishName']?>'))
	{
		return false;
	}
	return true;
}

$(document).ready(function(){
	
	$('#btnSubmit').click(function(e) {
		 
	    e.preventDefault();
	    $('input.actionBtn').attr('disabled', 'disabled');
	    var url;
<? if ($form_action == "edit_item_update.php"):?>
		url = '../../ajax/ajax.php?action=checkBeforeUpdateAdjustItem'
<? else:?>
		url = '../../ajax/ajax.php?action=checkBeforeAddAdjustItem'
<? endif;?>

		if (checkForm(document.form1)) {
	        $.ajax({
	        	dataType: "json", 
	            type: 'post',
	            url: url, 
	            data: $('#form1').serialize(),
	            async: false,            
		        success: function(ajaxReturn) {
		        	if (ajaxReturn != null && ajaxReturn.success){
		        		if (ajaxReturn.html == '1') {
		        			alert('<?=$Lang['eGuidance']['settings']['FineTune']['Warning']['DuplicateAdjustItem']?>');
		        			$('input.actionBtn').attr('disabled', '');
		        		}
		        		else {
		        			$('#form1').submit();
		        		}
		        	}
		        	else {
		        		$('input.actionBtn').attr('disabled', '');
		        	}
		        },
				error: show_ajax_error
	        });
		}
		else {
			$('input.actionBtn').attr('disabled', '');
		}
	});
		
});

function show_ajax_error() {
	alert('<?=$Lang['eGuidance']['error']['ajax']?>');
}
	
</script>

<form name="form1" id="form1" method="post" action="<?=$form_action?>">
<table id="main_table" width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td class="navigation"><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="5" cellspacing="0">
				<tr>
					<td>
						<table align="center" class="form_table_v30">
							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['eGuidance']['settings']['FineTune']['AdjustItemChineseName']?>
									<span class="tabletextrequire">*</span></td>
								<td class="tabletext"><input type="text" name="ChineseName" id="ChineseName" class="textboxtext" value="<?=intranet_htmlspecialchars($rs_adjust_item['ChineseName'])?>"></td>
							</tr>
							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['eGuidance']['settings']['FineTune']['AdjustItemEnglishName']?>
									<span class="tabletextrequire">*</span></td>
								<td class="tabletext"><input type="text" name="EnglishName" id="EnglishName" class="textboxtext" value="<?=intranet_htmlspecialchars($rs_adjust_item['EnglishName'])?>"></td>
							</tr>
							
							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['eGuidance']['settings']['FineTune']['WithText']?>
									</td>
								<td class="tabletext">
									<input type="radio" name="WithText" id="WithTextYes" value="1"<?=($rs_adjust_item['WithText']?' checked':'')?>><label for="WithTextYes"><?=$Lang['General']['Yes']?></label><br>
									<input type="radio" name="WithText" id="WithTextNo" value="0"<?=(isset($rs_adjust_item['WithText']) && $rs_adjust_item['WithText']==0?' checked':'')?>><label for="WithTextNo"><?=$Lang['General']['No']?></label>
								</td>
							</tr>
							
							<tr>
								<td valign="top" nowrap="nowrap" class="tabletextremark"><?=$i_general_required_field?></td>
								<td width="80%">&nbsp;</td>
							</tr>
						</table>
						
					</td>
				</tr>
				<tr>
					<td height="1" class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
				</tr>
				<tr>
					<td align="right">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="center">
									<span>
									<?= $linterface->GET_ACTION_BTN($button_submit, "button","","btnSubmit", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn")?>&nbsp;
									<?= $linterface->GET_ACTION_BTN($button_reset, "reset","","btnReset", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn")?>&nbsp;
									<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='adjust_item_list.php?TypeID=$TypeID'","btnCancel", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn")?>
									</span></td>
							</tr>
						</table>
					</td>
				</tr>
			</table><br>
		</td>
	</tr>
</table>
<input type=hidden id="TypeID" name="TypeID" value="<?=$TypeID?>">
<input type=hidden id="ItemID" name="ItemID" value="<?=$itemID?>">
</form>
