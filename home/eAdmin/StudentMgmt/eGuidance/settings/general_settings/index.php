<?php
// using : 

############# Change Log (start) #################
#
#   Date:   2020-06-19 (Cameron)
#           add PrintSenCasePICinStudentReport
#
#   Date:   2019-08-13 (Cameron)
#           add AllowClassTeacherViewSEN and AllowSubjectTeacherViewSEN
#
#	Date:	2017-07-10 (Anna)
#			create file for input Allow Access IP
#
############## Change Log (end) ##################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/eGuidance_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/guidance_conf.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/libguidance.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");


intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$libguidance = new libguidance();
$permission = $libguidance->getUserPermission();

if (!$permission['admin'] && !$permission['current_right']['SETTINGS']['GENERALSETTING']) {
	$libguidance->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$CurrentPageArr['eGuidance'] = 1;
$CurrentPage = "GeneralSetting";
$CurrentPageName = $Lang['eGuidance']['name'];

### Title ###
$TAGS_OBJ = array();
$TAGS_OBJ[] = array($Lang['eGuidance']['menu']['Settings']['GeneralSetting']);


$MODULE_OBJ = $libguidance->GET_MODULE_OBJ_ARR();
$returnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];
$linterface->LAYOUT_START($returnMsg);


# Tag information

// $linterface->LAYOUT_START();

// get current IP settings

?>
<script type="text/javascript" language="Javascript">
function EditInfo()
{
	$('#EditBtn').attr('style', 'visibility: hidden'); 
	$('.Edit_Hide').attr('style', 'display: none');
	$('.Edit_Show').attr('style', '');
	
	$('#UpdateBtn').attr('style', '');
	$('#cancelbtn').attr('style', '');
}	
function ResetInfo()
{
	document.form1.reset();
	$('#EditBtn').attr('style', '');
	$('.Edit_Hide').attr('style', '');
	$('.Edit_Show').attr('style', 'display: none');
	
	$('#UpdateBtn').attr('style', 'display: none');
	$('#cancelbtn').attr('style', 'display: none');
}

</script><form name="form1" method="get" action="edit_update.php">

<div id="ref_list" style='position:absolute; height:150px; z-index:1; visibility: hidden;'></div>

<div class="table_board">
	<?php if ($permission['admin'] || in_array('MGMT', (array)$permission['current_right']['SETTINGS']['GENERALSETTING'])) {?>
	<div class="table_row_tool row_content_tool">
		<?= $linterface->GET_SMALL_BTN($button_edit, "button","javascript:EditInfo();","EditBtn")?>
	</div>
	<?php }?>
	<p class="spacer"></p>
	
	
	<div class="form_sub_title_v30"><em>- <span class="field_title"> <?=$Lang['eGuidance']['settings']['System']['General']?> </span>-</em>
		<p class="spacer"></p>
	</div>
	<table class="form_table_v30">
		<tr valign='top'>
			<td  class="field_title" nowrap><?= str_replace("<!--ModuleName-->", $Lang['eGuidance']['name'], $Lang['Security']['AuthenticationSetting']) ?></td>
			<td>
				<span class="Edit_Hide"><?=($libguidance->AuthenticationSetting? $i_general_yes:$i_general_no)?></span>
				<span class="Edit_Show" style="display:none">
					<input type="radio" name="AuthenticationSetting" value="1" <?=$libguidance->AuthenticationSetting ? "checked":"" ?> id="AuthenticationSetting1"> <label for="AuthenticationSetting1"><?=$i_general_yes?></label> 
					<input type="radio" name="AuthenticationSetting" value="0" <?=$libguidance->AuthenticationSetting ? "":"checked" ?> id="AuthenticationSetting0"> <label for="AuthenticationSetting0"><?=$i_general_no?></label>
				</span>
			</td>
		</tr>
		<tr valign='top'>
			<td  class="field_title" nowrap><?php echo $Lang['eGuidance']['settings']['System']['AllowClassTeacherViewSEN']; ?></td>
			<td>
				<span class="Edit_Hide"><?=($libguidance->AllowClassTeacherViewSEN? $i_general_yes:$i_general_no)?></span>
				<span class="Edit_Show" style="display:none">
					<input type="radio" name="AllowClassTeacherViewSEN" value="1" <?=$libguidance->AllowClassTeacherViewSEN ? "checked":"" ?> id="AllowClassTeacherViewSEN1"> <label for="AllowClassTeacherViewSEN1"><?=$i_general_yes?></label> 
					<input type="radio" name="AllowClassTeacherViewSEN" value="0" <?=$libguidance->AllowClassTeacherViewSEN ? "":"checked" ?> id="AllowClassTeacherViewSEN0"> <label for="AllowClassTeacherViewSEN0"><?=$i_general_no?></label>
				</span>
			</td>
		</tr>

		<tr valign='top'>
			<td  class="field_title" nowrap><?php echo $Lang['eGuidance']['settings']['System']['AllowSubjectTeacherViewSEN']; ?></td>
			<td>
				<span class="Edit_Hide"><?=($libguidance->AllowSubjectTeacherViewSEN? $i_general_yes:$i_general_no)?></span>
				<span class="Edit_Show" style="display:none">
					<input type="radio" name="AllowSubjectTeacherViewSEN" value="1" <?=$libguidance->AllowSubjectTeacherViewSEN ? "checked":"" ?> id="AllowSubjectTeacherViewSEN1"> <label for="AllowSubjectTeacherViewSEN1"><?=$i_general_yes?></label> 
					<input type="radio" name="AllowSubjectTeacherViewSEN" value="0" <?=$libguidance->AllowSubjectTeacherViewSEN ? "":"checked" ?> id="AllowSubjectTeacherViewSEN0"> <label for="AllowSubjectTeacherViewSEN0"><?=$i_general_no?></label>
				</span>
			</td>
		</tr>

		<tr valign='top'>
			<td  class="field_title" nowrap><?php echo $Lang['eGuidance']['settings']['System']['PrintSenCasePICinStudentReport']; ?></td>
			<td>
				<span class="Edit_Hide"><?=($libguidance->PrintSenCasePICinStudentReport? $i_general_yes:$i_general_no)?></span>
				<span class="Edit_Show" style="display:none">
					<input type="radio" name="PrintSenCasePICinStudentReport" value="1" <?=$libguidance->PrintSenCasePICinStudentReport ? "checked":"" ?> id="PrintSenCasePICinStudentReport1"> <label for="PrintSenCasePICinStudentReport1"><?=$i_general_yes?></label> 
					<input type="radio" name="PrintSenCasePICinStudentReport" value="0" <?=$libguidance->PrintSenCasePICinStudentReport ? "":"checked" ?> id="PrintSenCasePICinStudentReport0"> <label for="PrintSenCasePICinStudentReport0"><?=$i_general_no?></label>
				</span>
			</td>
		</tr>
		
	</table>
</div>
<div class="edit_bottom_v30">
		<p class="spacer"></p>
		<?= $linterface->GET_ACTION_BTN($button_update, "submit","","UpdateBtn", "style='display: none'") ?>
		<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "ResetInfo();","cancelbtn", "style='display: none'") ?>
		<p class="spacer"></p>
</div>
</form>

<br>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>