<?php
// using : 

############# Change Log (start) #################
#
#   Date:   2020-02-21 (Cameron)
#           fix: should update DateModified field instead of DateInput field when update
#
#	Date:	2017-07-07 (Anna)
#			create file for update Allow Access IP
#
############## Change Log (end) ##################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/guidance_conf.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/libguidance.php");

intranet_auth();
intranet_opendb();

$libguidance = new libguidance();
$permission = $libguidance->getUserPermission();


$allowed_IPs = trim($libguidance->getGuidanceGeneralSetting("AllowAccessIP"));
$ip_addresses = explode("\n", $allowed_IPs);
checkCurrentIP($ip_addresses, $Lang['eGuidance']['name']);


if (!$permission['admin'] && !$permission['current_right']['SETTINGS']['IPACCESSRIGHTSETTING']) {
	$libguidance->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

// post data
$AllowAccessIP = trim($_POST['AllowAccessIP']);

// get current IP settings
$currentIP = $libguidance->getGuidanceGeneralSetting("AllowAccessIP");

// update
if(sizeof($currentIP)>0){
	$sql = "Update GENERAL_SETTING 
				Set Module='eGuidance', SettingValue='$AllowAccessIP', DateModified=NOW(), ModifiedBy='".$_SESSION['UserID']."'
			WHERE SettingName='AllowAccessIP' AND Module='eGuidance'";
}
// insert
else{
	$sql = "INSERT INTO GENERAL_SETTING (Module, SettingName, SettingValue, DateInput,InputBy, DateModified, ModifiedBy)
				VALUES ('eGuidance', 'AllowAccessIP', '$AllowAccessIP', NOW(),'".$_SESSION['UserID']."', NOW(), '".$_SESSION['UserID']."')";
}
$result = $libguidance->db_db_query($sql);

if($result)
	$msg = "update";
else
	$msg = "update_failed";
	
intranet_closedb();	

header("Location: index.php?msg=$msg");
?>