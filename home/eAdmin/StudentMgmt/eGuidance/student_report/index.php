<?
/**
 * Modifying:  
 * 
 * Change Log:
 * 
 * 2020-05-14 Cameron
 *      - add report type option: NotShowSectionWithoutRecord [case #M151033]
 *      
 * 2020-04-23 Cameron
 *      - add following filters for SENCase: SEN type, Support Mode, Adjustment Arrangement, Support Service [case #E150107]
 *      
 * 2018-04-11 Cameron
 *      - add report option [case #M130681]
 * 
 * 2018-03-14 Cameron
 *      - set default semester to current semester
 *      
 * 2017-11-07 Cameron
 * 		- hide AcademicYear and Term selection for ej as it cannot determined the start and end date of past years' terms
 * 		- support $selectedYearClass in ej
 * 
 * 2017-08-18 Cameron
 * 		- add report type: SelfImprove
 * 
 * 2017-06-26 Pun
 *  - New file
 */
$PATH_WRT_ROOT = "../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "lang/eGuidance_lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libclass.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/form_class_manage.php");
include_once ($PATH_WRT_ROOT . "includes/eGuidance/guidance_conf.php");
include_once ($PATH_WRT_ROOT . "includes/eGuidance/libguidance.php");
include_once ($PATH_WRT_ROOT . "includes/eGuidance/libguidance_ui.php");

// ####### Init START ########
intranet_auth();
intranet_opendb();

$libguidance = new libguidance();
$linterface = new interface_html();
$libguidance_ui = new libguidance_ui();

$currentAcademicYearID = Get_Current_Academic_Year_ID();
// ####### Init END ########

// ####### Access Right START ########
$permission = $libguidance->getUserPermission();
if (! $permission['admin'] && ! in_array('VIEW', (array) $permission['current_right']['REPORTS']['STUDENTREPORT'])) {
    $libguidance->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}
// ####### Access Right END ########

// ####### Get Date selection START ########
if (! $junior_mck) {
    $academicYearHTML = getSelectAcademicYear($objName = 'AcademicYearID', $tag = '', $noFirst = 1, $noPastYear = 0, $targetYearID = $currentAcademicYearID, $displayAll = 0, $pastAndCurrentYearOnly = 0, $OrderBySequence = 1, $excludeCurrentYear = 0, $excludeYearIDArr = array());
    $currentSemesterID = $_POST['AcademicYearTermID'] ? $_POST['AcademicYearTermID'] : getCurrentSemesterID();
    $academicYearTermHTML = getSelectSemester2($tags = ' id="AcademicYearTermID" name="AcademicYearTermID"', $selected = $currentSemesterID, $ParQuoteValue = 1, $AcademicYearID = $currentAcademicYearID);
}
// ####### Get Date selection END ########

// ####### Get Start Date START ########
$StartDate = getStartDateOfAcademicYear($currentAcademicYearID);
$StartDate = substr($StartDate, 0, 10);
$EndDate = '';
// ####### Get Start Date END ########

// ####### Get Year selection START ########
if ($junior_mck) {
    $lclass = new libclass();
    $rs = $lclass->getClassList();
    $selectedYearClass = "{$rs[0]['ClassLevelID']}:{$rs[0]['ClassID']}";
} else {
    $objYearClass = new year_class();
    $rs = $objYearClass->Get_All_Class_List($currentAcademicYearID);
    $selectedYearClass = "{$rs[0]['YearID']}:{$rs[0]['YearClassID']}";
}
$yearSelection = $libguidance_ui->getSelectClassWithWholeForm(' id="YearAndClass" name="YearAndClass"', $selectedYearClass);
// ####### Get Year selection END ########

// ####### Report Type START ########
$allReportType = array();
$allReportType[] = array(
    'value' => 'Contact',
    'title' => $Lang['eGuidance']['menu']['Management']['Contact']
);
$allReportType[] = array(
    'value' => 'AdvancedClass',
    'title' => $Lang['eGuidance']['menu']['Management']['AdvancedClass']
);
$allReportType[] = array(
    'value' => 'Therapy',
    'title' => $Lang['eGuidance']['menu']['Management']['Therapy']
);
$allReportType[] = array(
    'value' => 'Guidance',
    'title' => $Lang['eGuidance']['menu']['Management']['Guidance']
);
$allReportType[] = array(
    'value' => 'SEN',
    'title' => $Lang['eGuidance']['menu']['Management']['SEN']
);
$allReportType[] = array(
    'value' => 'Suspend',
    'title' => $Lang['eGuidance']['menu']['Management']['Suspend']
);
$allReportType[] = array(
    'value' => 'Transfer',
    'title' => $Lang['eGuidance']['menu']['Management']['Transfer']
);

if ($plugin['eGuidance_module']['SelfImprove']) {
    $allReportType[] = array(
        'value' => 'SelfImprove',
        'title' => $Lang['eGuidance']['menu']['Management']['SelfImprove']
    );
}
// ####### Report Type END ########

$nameField = ($intranet_session_language == 'b5' || $intranet_session_language == 'gb') ? 'ChineseName' : 'EnglishName';
// # get adjustment type
$rs_sen_adjust_type = $libguidance->getSENAdjustType();
$rs_sen_adjust_type = BuildMultiKeyAssoc($rs_sen_adjust_type, 'TypeID', array(
    $nameField
), 1);

// # get adjustment item
$rs_sen_adjust_item = $libguidance->getSENAdjustItem();
$rs_sen_adjust_item = BuildMultiKeyAssoc($rs_sen_adjust_item, array(
    'TypeID',
    'ItemID'
), array(
    $nameField,
    'WithText'
));

// # get study service type
$rs_sen_study_service_type = $libguidance->getServiceType('', 'Study');
$rs_sen_study_service_type = BuildMultiKeyAssoc($rs_sen_study_service_type, 'TypeID', array(
    $nameField
), 1);

// # get adjustment support study service
$rs_sen_support_study = $libguidance->getSENSupportService('', '', 'Study');
$rs_sen_support_study = BuildMultiKeyAssoc($rs_sen_support_study, array(
    'TypeID',
    'ServiceID'
), array(
    $nameField,
    'WithText'
));

// # get other service type
$rs_sen_other_service_type = $libguidance->getServiceType('', 'Other');
$rs_sen_other_service_type = BuildMultiKeyAssoc($rs_sen_other_service_type, 'TypeID', array(
    $nameField
), 1);

// # get adjustment support other service
$rs_sen_support_other = $libguidance->getSENSupportService('', '', 'Other');
$rs_sen_support_other = BuildMultiKeyAssoc($rs_sen_support_other, array(
    'TypeID',
    'ServiceID'
), array(
    $nameField,
    'WithText'
));


// ####### UI START ########
$CurrentPageArr['eGuidance'] = 1;
$CurrentPage = "PageStudentReport";
$CurrentPageName = $Lang['eGuidance']['name'];

$TAGS_OBJ = array();
$TAGS_OBJ[] = array(
    $Lang['eGuidance']['menu']['Reports']['StudentReport'],
    "",
    true
);

$MODULE_OBJ = $libguidance->GET_MODULE_OBJ_ARR();
$returnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];
$linterface->LAYOUT_START($returnMsg);

$star = $linterface->RequiredSymbol();
?>


<style>
    .collapsible, .collapsibleImg {cursor: pointer;}
</style>

<form id="form1" method="post" action="print_pdf.php" target="_blank">
	<table width="88%" border="0" cellpadding="5" cellspacing="0">
		<tr>
			<td>
				<table class="form_table_v30">
					<!--tr>
					<td colspan="2" valign="middle" nowrap="nowrap" class="form_sep_title"><i> - <?=$Lang['Identity']['Student']?> -</i></td>
				</tr-->
					<tr>
						<td valign="top" nowrap="nowrap" class="field_title"><?=$Lang['eGuidance']['Date'] . $star ?></td>
						<td>
					<? if ($junior_mck):?>
						<div class="dateTypeDiv">
								<input type="hidden" id="dateTypeDateRange" name="DateType"
									value="DATE_RANGE" /> <label for="StartDate"><?=$Lang['General']['From']?></label>
    						<?=$linterface->GET_DATE_PICKER("StartDate",$StartDate)?>
    						<label for="EndDate"><?=$Lang['General']['To']?></label>
    						<?=$linterface->GET_DATE_PICKER("EndDate",$EndDate)?>
						</div>
					<? else: ?>
						<div class="dateTypeDiv">
								<input type="radio" id="dateTypeYear" name="DateType"
									value="YEAR" checked /> <label for="AcademicYearID"><?=$Lang['General']['SchoolYear'] ?></label>
								<span id="academicYearDiv"><?=$academicYearHTML ?></span> <label
									for="AcademicYearTermID"><?=$Lang['General']['Term'] ?></label>
								<span id="academicYearTermDiv"><?=$academicYearTermHTML ?></span>
							</div>

							<div class="dateTypeDiv" style="margin-top: 15px;">
								<input type="radio" id="dateTypeDateRange" name="DateType"
									value="DATE_RANGE" /> <label for="StartDate"><?=$Lang['General']['From']?></label>
    						<?=$linterface->GET_DATE_PICKER("StartDate",$StartDate)?>
    						<label for="EndDate"><?=$Lang['General']['To']?></label>
    						<?=$linterface->GET_DATE_PICKER("EndDate",$EndDate)?>
						</div>
					<? endif;?>
					</td>
					</tr>
					<tr>
						<td valign="top" rowspan="3" nowrap="nowrap" class="field_title"><?=$Lang['eGuidance']['Class'] . $star ?></td>
						<td>
						<?=$yearSelection?>
					</td>
					</tr>
					<tr valign="top">
						<td>
							<div id='studentID'>
								<select name="studentID" class="formtextbox"></select>
							</div>
						</td>
					</tr>
					<tr valign="top">
						<td><input type="checkbox" name="NotShowStudentWithoutRecord"
							id="NotShowStudentWithoutRecord" value="1" checked> <label
							for="NotShowStudentWithoutRecord"><?=$Lang['eGuidance']['report']['NotToShowStudentWithoutRecord']?></label>
						</td>
					</tr>

					<tr>
						<td valign="top" nowrap="nowrap" class="field_title" rowspan="2"><?=$Lang['eGuidance']['report']['ReportType'] . $star ?></td>
						<td><input type="checkbox" id="selectAllType" checked /> <label
							for="selectAllType"><?=$Lang['Btn']['SelectAll'] ?></label>

						<?php foreach($allReportType as $index=>$reportType): ?>
    						<?php if($index%3 == 0): ?>
    							<br />
    						<?php endif; ?>
    						
    						<input type="checkbox"
							id="reportType_<?=$reportType['value'] ?>" name="reportType[]"
							value="<?=$reportType['value'] ?>" checked /> <label
							for="reportType_<?=$reportType['value'] ?>"><?=$reportType['title'] ?></label>&nbsp;&nbsp;
						<?php endforeach; ?>
						</td>
					</tr>
					<tr>
						<td>
    						<input type="checkbox" name="NotShowSectionWithoutRecord" id="NotShowSectionWithoutRecord" value="1" checked> 
    						<label for="NotShowSectionWithoutRecord"><?=$Lang['eGuidance']['report']['NotShowSectionWithoutRecord']?></label>
						</td>
					</tr>

					<tr>
						<td valign="top" nowrap="nowrap" class="field_title"><?=$Lang['eGuidance']['report']['ReportOption']?></td>
						<td>
							<div>
								<?php echo $Lang['eGuidance']['report']['StudentPivateInfo'];?>
							</div>
							<div>
								<input type="checkbox" id="studentPrivateInfo"
									name="studentPrivateInfo" value="1" checked/> <label
									for="studentPrivateInfo"><?php echo $Lang['eGuidance']['report']['StudentPivateInfoDetails'];?></label>
							</div>
							<div class="senOptionDiv">
								<?php echo $Lang['eGuidance']['menu']['Management']['SEN'];?>
							</div>
							<div class="senOptionDiv">
								<input type="checkbox" id="optionSenService"
									name="optionSenService" value="senService" checked /> <label
									for="optionSenService"><?php echo $Lang['eGuidance']['sen']['Tab']['Service'];?></label>

								<input type="checkbox" id="optionSenCase" name="optionSenCase" value="senCase"
									checked /> <label for="optionSenCase"><?php echo $Lang['eGuidance']['sen']['Tab']['Case'];?></label>
						
								<?php echo $libguidance_ui->getSelectSenSatus($select='', $tag='id="senStatus" name="senStatus"');?>
							</div>
							
							<div id="sencaseOptionDiv" class="senOptionDiv">
								<div id="sencaseLogicalTypeDiv">
    								<table class="no_bottom_border" width="100%">
    									<tr>
                    						<td width="30%" valign="top" class="field_title">
                    							<table>
                    								<tr>
                    									<td style="vertical-align: middle;">
                    										<span id="sencaseLogicalTypeSpan" class="collapsible"><?php echo $Lang['eGuidance']['sen']['SENType'] . " " . $Lang['eGuidance']['sen_case']['FilterSelection'];?></span>
                    									</td>
                    									<td>
                    										<img src="<?php echo $image_path."/".$LAYOUT_SKIN."/leftmenu/btn_arrow_expand_off.gif";?>" class="collapsibleImg" name="sencaseLogicalTypeImg" id="sencaseLogicalTypeImg">
			                    						</td>
            		        						</tr>
                    							</table>
                    						
                    						</td>
                    						<td class="tabletext">
                    							<table border="0" id="sencaseLogicalTypeList">
                                                	<tr valign="top">
                                						<td class="tabletext">
                                                            <input type="radio" name="LogicalType" id="LogicalAnd" value="and" checked><label for="LogicalAnd"><?php echo $Lang['eGuidance']['sen_case']['Filter']['LogicAnd'];?></label><br>
                                                            <input type="radio" name="LogicalType" id="LogicalOr" value="or"><label for="LogicalOr"><?php echo $Lang['eGuidance']['sen_case']['Filter']['LogicOr'];?></label>
                                						</td>
                            						</tr>
                    							</table>
                    						</td>
    									</tr>
    								</table>
								</div>

								<div id="sencaseTypeDiv">
    								<table class="no_bottom_border" width="100%">
    									<tr>
                    						<td width="30%" valign="top" class="field_title">
                    							<table>
                    								<tr>
                    									<td style="vertical-align: middle;">
                    										<span id="sencaseTypeSpan" class="collapsible"><?php echo $Lang['eGuidance']['sen']['SENType'];?></span>
                    									</td>
                    									<td>
                    										<img src="<?php echo $image_path."/".$LAYOUT_SKIN."/leftmenu/btn_arrow_expand_off.gif";?>" class="collapsibleImg" name="sencaseTypeImg" id="sencaseTypeImg">
			                    						</td>
            		        						</tr>
                    							</table>
                    						</td>
                    						<td class="tabletext">
                    							<table border="0" id="sencaseTypeList">
            										<?php echo $libguidance_ui->getAllSENCaseType();?>        							
                    							</table>
                    						</td>
    									</tr>
    								</table>
								</div>
								
								<div id="sencaseTierDiv">
    								<table class="no_bottom_border" width="100%">
    									<tr>
                    						<td width="30%" valign="top" class="field_title">
                    							<table>
                    								<tr>
                    									<td style="vertical-align: middle;">
                    										<span id="sencaseTierSpan" class="collapsible"><?php echo $Lang['eGuidance']['sen_case']['Tier']['TierName'];?></span>
                    									</td>
                    									<td>
                    										<img src="<?php echo $image_path."/".$LAYOUT_SKIN."/leftmenu/btn_arrow_expand_off.gif";?>" class="collapsibleImg" name="sencaseTierImg" id="sencaseTierImg">
			                    						</td>
            		        						</tr>
                    							</table>
                    						</td>
                    						<td class="tabletext">
<?php 
    $x = '';
    $x .= '<table class="no_bottom_border" id="sencaseTierList">';
    for ($i = 1; $i <= 3; $i ++) {
        $x .= '<tr><td style="vertical-align:middle; width:80px; "><input type="checkbox" name="IsTier' . $i . '" id="IsTier' . $i . '" value="1"><label for="IsTier' . $i . '">' . $Lang['eGuidance']['sen_case']['Tier']["Tier$i"] . '</label></td>';
        $x .= '</tr>';
    }
    $x .= '</table>';
    echo $x;
?>                    						
                    						</td>
    									</tr>
    								</table>
								</div>

								<div id="sencaseAdjustTypeDiv">
    								<table class="form_table_v30" width="100%">
    									<tr>
                    						<td width="30%" valign="top" class="field_title">
                    							<table>
                    								<tr>
                    									<td style="vertical-align: middle;">
                    										<span id="sencaseAdjustTypeSpan" class="collapsible"><?php echo $Lang['eGuidance']['sen_case']['FineTuneArrangement'];?></span>
                    									</td>
                    									<td>
                    										<img src="<?php echo $image_path."/".$LAYOUT_SKIN."/leftmenu/btn_arrow_expand_off.gif";?>" class="collapsibleImg" name="sencaseAdjustTypeImg" id="sencaseAdjustTypeImg">
			                    						</td>
            		        						</tr>
                    							</table>
                    						</td>
                    						<td class="tabletext">
                    							<table class="no_bottom_border" id="sencaseAdjustTypeList">
<?php
    $x = '';
    foreach ((array) $rs_sen_adjust_type as $k => $v) {
        $x .= '<tr><td><input type="checkbox" name="TypeID[]" id="AdjustType_' . $k . '" onClick="CheckAdjustment(' . $k . ')"><label for="AdjustType_' . $k . '">' . $v . '</label></td></tr>';
        foreach ((array) $rs_sen_adjust_item[$k] as $kk => $vv) {
            $x .= '<tr><td style="padding-left:20px;"><input type="checkbox" class="AdjustType_' . $k . '" name="ItemID[' . $k . '][]" id="AdjustItem_' . $k . '_' . $kk . '" value="' . $kk . '"' . ($rs_sencase_adjustment[$kk] ? ' checked' : '') . '><label for="AdjustItem_' . $k . '_' . $kk . '">' . $vv[$nameField] . '</label></td></tr>';
        }
    }
    echo $x;
?>                    						
												</table>
                    						</td>
    									</tr>
    								</table>
								</div>

								<div id="sencaseSupportStudyDiv">
    								<table class="form_table_v30" width="100%">
    									<tr>
                    						<td width="30%" valign="top" class="field_title">
                    							<table>
                    								<tr>
                    									<td style="vertical-align: middle;">
                    										<span id="sencaseSupportStudySpan" class="collapsible"><?php echo $Lang['eGuidance']['sen_case']['FineTuneSupportStudy'];?></span>
                    									</td>
                    									<td>
                    										<img src="<?php echo $image_path."/".$LAYOUT_SKIN."/leftmenu/btn_arrow_expand_off.gif";?>" class="collapsibleImg" name="sencaseSupportStudyImg" id="sencaseSupportStudyImg">
			                    						</td>
            		        						</tr>
                    							</table>
                    						</td>
                    						<td class="tabletext">
                    							<table class="no_bottom_border" id="sencaseSupportStudyList">
<?php
    $x = '';
    foreach ((array) $rs_sen_study_service_type as $k => $v) {
        $x .= '<tr><td><input type="checkbox" name="ServiceTypeID[]" id="ServiceType_' . $k . '" onClick="CheckService(' . $k . ')"><label for="ServiceType_' . $k . '">' . $v . '</label></td></tr>';
        foreach ((array) $rs_sen_support_study[$k] as $kk => $vv) {
            $x .= '<tr><td style="padding-left:20px;"><input type="checkbox" class="ServiceType_' . $k . '" name="ServiceItemID[' . $k . '][]" id="ServiceItem_' . $k . '_' . $kk . '" value="' . $kk . '"' . ($rs_sencase_support_study[$kk] ? ' checked' : '') . '><label for="ServiceItem_' . $k . '_' . $kk . '">' . $vv[$nameField] . '</label></td></tr>';
        }
    }
    echo $x;
?>                    						
												</table>
                    						</td>
    									</tr>
    								</table>
								</div>

								<div id="sencaseSupportOtherDiv">
    								<table class="form_table_v30" width="100%">
    									<tr>
                    						<td width="30%" valign="top" class="field_title">
                    							<table>
                    								<tr>
                    									<td style="vertical-align: middle;">
                    										<span id="sencaseSupportOtherSpan" class="collapsible"><?php echo $Lang['eGuidance']['sen_case']['FineTuneSupportOther'];?></span>
                    									</td>
                    									<td>
                    										<img src="<?php echo $image_path."/".$LAYOUT_SKIN."/leftmenu/btn_arrow_expand_off.gif";?>" class="collapsibleImg" name="sencaseSupportOtherImg" id="sencaseSupportOtherImg">
			                    						</td>
            		        						</tr>
                    							</table>
                    						</td>
                    						<td class="tabletext">
                    							<table class="no_bottom_border" id="sencaseSupportOtherList">
<?php
    $x = '';
    foreach ((array) $rs_sen_other_service_type as $k => $v) {
        $x .= '<tr><td><input type="checkbox" name="ServiceTypeID[]" id="ServiceType_' . $k . '" onClick="CheckService(' . $k . ')"><label for="ServiceType_' . $k . '">' . $v . '</label></td></tr>';
        foreach ((array) $rs_sen_support_other[$k] as $kk => $vv) {
            $x .= '<tr><td style="padding-left:20px;"><input type="checkbox" class="ServiceType_' . $k . '" name="ServiceItemID[' . $k . '][]" id="ServiceItem_' . $k . '_' . $kk . '" value="' . $kk . '"' . ($rs_sencase_support_other[$kk] ? ' checked' : '') . '><label for="ServiceItem_' . $k . '_' . $kk . '">' . $vv[$nameField] . '</label></td></tr>';
        }
    }
    echo $x;
?>                    						
												</table>
                    						</td>
    									</tr>
    								</table>
								</div>
								
							</div>
							
						</td>
					</tr>

					<!--tr><td colspan="2">&nbsp;</td></tr-->
				</table>
			</td>
		</tr>

	</table>
	<div class="tabletextremark"><?=$i_general_required_field?></div>

	<div class="edit_bottom_v30">
	<?= $linterface->GET_ACTION_BTN($Lang['eGuidance']['report']['GenerateReport'], "submit")?>
</div>


	<input type="hidden" id="YearID" name="YearID" /> <input type="hidden"
		id="YearClassID" name="YearClassID" />
</form>







<script>
$(document).ready(function(){
	var isLoading = true;
	var loadingImg = '<?=$linterface->Get_Ajax_Loading_Image() ?>';

	//////// Date select START ////////
	/*$('[name="DateType"]').change(function(){
		var dateType = $(this).val();
		$('#AcademicYearID, #AcademicYearTermID, #StartDate, #EndDate').attr('disabled', 'disabled');
		if(dateType == 'YEAR'){
			$('#AcademicYearID, #AcademicYearTermID').removeAttr('disabled');
		}else{
			$('#StartDate, #EndDate').removeAttr('disabled');
		}
	});*/
	$('#AcademicYearID, #academicYearTermDiv, #StartDate, #EndDate').click(function(){
		$(this).closest('div').find('[name="DateType"]').click();
	});
	$('#AcademicYearID').change(function(){
        $('#academicYearTermDiv').html(loadingImg);

		var data = {
			'AcademicYearID': $(this).val(),
			'tags': " id='AcademicYearTermID' name='AcademicYearTermID'"
		};
		
		$.ajax({
    		dataType: "json", 
            type: 'post',
            url: '../ajax/ajax.php?action=getAcademicYearTermSelect', 
            data: data,
            success: function(ajaxReturn) {
                var academicYearTermSelect = ajaxReturn['html'];
                $('#academicYearTermDiv').html(academicYearTermSelect);
            },
            error: show_ajax_error
		})
	});
	//////// Date select END ////////
	
	
	//////// YearClass select START ////////
	$('#YearAndClass').change(function(){
		if($(this).val() == '#'){
			$('#studentID').html('<?=$Lang['eGuidance']['Warning']['SelectClass'] ?>');
			return false;
		}
        isLoading = true;
		$('#studentID').html(loadingImg);
		
		var value = $(this).val().split(':');
		
		var data = {};
		data['YearID'] = (typeof(value[0]) == 'undefined')?'':value[0];
		data['YearClassID'] = (typeof(value[1]) == 'undefined')?'':value[1];

		$('#YearID').val(data['YearID']);
		$('#YearClassID').val(data['YearClassID']);

		$.ajax({
    		dataType: "json", 
            type: 'post',
            url: '../ajax/ajax.php?action=getFromStudentNameByYearIdYearClassId', 
            data: data,
            success: function(ajaxReturn) {
                var studentSelect = ajaxReturn['html'];
                $('#studentID').html(studentSelect);
                isLoading = false;
            },
            error: show_ajax_error
		})
	}).change();
	//////// YearClass select END ////////

	
	//////// SelectAll toggle START ////////
	$('#selectAllType').change(function(){
		var checked = $(this).attr('checked');

		var $checkbox = $(this).parent().find('input[id^="reportType_"]');
		$checkbox.attr('checked', checked);
		changeSENOption();
	});
	$('input[id^="reportType_"]').change(function(){
		var checked = $(this).attr('checked');
		if(!checked){
			$('#selectAllType').attr('checked', false);
		}
		if ($(this).val() == 'SEN') {
			changeSENOption();
		}
	});
	//////// SelectAll toggle END ////////
	
	$('#optionSenCase').change(function(){
		var checked = $(this).attr('checked');
		if (!checked) {
			$('#senStatus').hide();
			$('#sencaseOptionDiv').hide();
		}
		else {
			$('#senStatus').show();
			$('#sencaseOptionDiv').show();
		}
	});
	
	//////// Form submit checking START ////////
	$('#form1').submit(function(e){
		if($('#YearAndClass').val() == '#'){
			alert('<?=$Lang['eGuidance']['Warning']['SelectClass'] ?>');
			e.preventDefault();
			return;
		}

		if(isLoading){
			alert('<?=$Lang['eGuidance']['Warning']['SelectStudent'] ?>');
			e.preventDefault();
			return;
		}

		if($('[name="reportType\[\]"]:checked').length == 0){
			alert('<?=$Lang['eGuidance']['Warning']['SelectReportType'] ?>');
			e.preventDefault();
			return;
		}
	});
	//////// Form submit checking END ////////

	$('.collapsible').click(function(e){
		var thisID = $(this).attr('id');
		var thisContent = thisID.replace("Span","List");
		$('#'+thisContent).slideToggle();
		var thisImg = thisID.replace("Span","Img");
		var src = $('#'+thisImg).attr('src');
		if (src.indexOf('btn_arrow_expand_off') == -1) {
			$('#'+thisImg).attr('src','<?php echo $image_path."/".$LAYOUT_SKIN."/leftmenu/btn_arrow_expand_off.gif";?>');
		}
		else {
			$('#'+thisImg).attr('src','<?php echo $image_path."/".$LAYOUT_SKIN."/leftmenu/btn_arrow_more_off.gif";?>');
		}
	});

	$('.collapsibleImg').click(function(e){
		var thisID = $(this).attr('id');
		var thisContent = thisID.replace("Img","List");
		$('#'+thisContent).slideToggle();
		var src = $(this).attr('src');
		if (src.indexOf('btn_arrow_expand_off') == -1) {
			$(this).attr('src','<?php echo $image_path."/".$LAYOUT_SKIN."/leftmenu/btn_arrow_expand_off.gif";?>');
		}
		else {
			$(this).attr('src','<?php echo $image_path."/".$LAYOUT_SKIN."/leftmenu/btn_arrow_more_off.gif";?>');
		}
	});
	
	$("input[id^='ServiceItem_']").click(function(){
		var thisName = $(this).attr('name');
		var num = thisName.replace("ServiceItemID[","").replace("][]","");
		if ($('.ServiceType_'+num+':checked').length <= 0) {
			$('#ServiceType_'+num).attr('checked', false);
		}		
	});

	$("input[id^='AdjustItem_']").click(function(){
		var thisName = $(this).attr('name');
		var num = thisName.replace("ItemID[","").replace("][]","");
		if ($('.AdjustType_'+num+':checked').length <= 0) {
			$('#AdjustType_'+num).attr('checked', false);
		}		
	});
	
});

function CheckAdjustment(type_id) {
	$('.AdjustType_'+type_id).attr('checked',$('#AdjustType_'+type_id).attr('checked'));
}

function CheckService(type_id) {
	$('.ServiceType_'+type_id).attr('checked',$('#ServiceType_'+type_id).attr('checked'));
}

function changeSENOption()
{
	var checked = $('#reportType_SEN').attr('checked');
	if (!checked) {
		$('.senOptionDiv').hide();
	}
	else {
		$('.senOptionDiv').show();
	}
}

function show_ajax_error() {
	alert('<?=$Lang['eGuidance']['error']['ajax']?>');
}

</script>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();