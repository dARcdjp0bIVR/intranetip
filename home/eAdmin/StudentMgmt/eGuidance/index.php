<?
/*
 * 	Log
 * 
 * 	2017-10-13 [Cameron]
 * 		- set $CurrentPageArr['eGuidance'] = 1;
 * 
 * 	2017-10-09 [Cameron]
 * 		- redirect to the page that user has permission
 */
 
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/eGuidance_lang.$intranet_session_language.php");
//include_once($PATH_WRT_ROOT."includes/libclass.php");
//include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/guidance_conf.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/libguidance.php");
//include_once($PATH_WRT_ROOT."includes/eGuidance/libguidance_ui.php");

intranet_auth();
intranet_opendb();

$libguidance = new libguidance();
$permission = $libguidance->getUserPermission();

$CurrentPageArr['eGuidance'] = 1;

$page = $libguidance->getDefaultPage();

if (empty($page)) {
//	$libguidance->NO_ACCESS_RIGHT_REDIRECT();
//	exit;
}

header("Location: {$page}");

intranet_closedb();
	
?>