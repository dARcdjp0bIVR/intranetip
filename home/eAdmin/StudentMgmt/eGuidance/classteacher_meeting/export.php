<?php
/*
 * 	Log
 * 
 * 	2017-11-17 Cameron
 * 		- create this file
 */

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/eGuidance_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/guidance_conf.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/libguidance.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
 
intranet_auth();
intranet_opendb();

$libguidance = new libguidance();
$permission = $libguidance->getUserPermission();
if (!$permission['admin'] && !$permission['current_right']['MGMT']['CLASSTEACHERMEETING']) {
	$libguidance->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$lexport = new libexporttext();


$joinClass = false;
$cond = '';

if ($junior_mck) {
	$libFCM = new form_class_manage();
	$yearInfoArr = $libFCM->Get_Academic_Year_List(Get_Current_Academic_Year_ID());
	$yearInfoArr = current($yearInfoArr);
	$currentYearName = $yearInfoArr['YearNameEN'];
	
	if (empty($YearName)) {
		$YearName = $currentYearName; 
	}
	
	$libAY = new academic_year();
	$ayInfo = $libAY->Get_Academic_Year_Info_By_YearName($YearName);
	if (count($ayInfo)) {
		$ay = $libFCM->Get_Academic_Year_List($ayInfo[0]['AcademicYearID']);
		if (count($ay)) {
			$cond .= " AND c.MeetingDate>='".$ay[0]['AcademicYearStart']."'";
			$cond .= " AND c.MeetingDate<='".$ay[0]['AcademicYearEnd']."'";
		}
	}
	
	if ($YearName == $currentYearName) {
		if ($ClassName) {
			$cond .= " AND u.ClassName='".$ClassName."'";
		}
		$joinClass = '';
		$classNameField = 'u.ClassName';
		$classNumberField = 'u.ClassNumber';
	}
	else {
		if ($ClassName) {
			$cond .= " AND h.ClassName='".$ClassName."'";
		}
		$joinClass = "INNER JOIN PROFILE_CLASS_HISTORY h ON h.UserID=u.UserID AND (h.AcademicYear='".$libguidance->Get_Safe_Sql_Query($YearName)."' OR LEFT(h.AcademicYear,4)='".$libguidance->Get_Safe_Sql_Query(substr($YearName,0,4))."')";
		$classNameField = 'h.ClassName';
		$classNumberField = 'h.ClassNumber';
	}
}
else {
	$currentAcademicYearID = Get_Current_Academic_Year_ID();
	$libFCM = new form_class_manage();
	$current_ay = $libFCM->Get_Academic_Year_List($currentAcademicYearID);
	$currentYearName = $current_ay[0]['YearNameEN'];
	
	$AcademicYearID = $AcademicYearID ? $AcademicYearID : $currentAcademicYearID;	
	if ($AcademicYearID) {
		
		$ay = $libFCM->Get_Academic_Year_List($AcademicYearID);
		if (count($ay)) {
			$cond .= " AND c.MeetingDate>='".$ay[0]['AcademicYearStart']."'";
			$cond .= " AND c.MeetingDate<='".$ay[0]['AcademicYearEnd']."'";
		}
		
		$cond .= " AND yc.AcademicYearID='".$AcademicYearID."'";
		$joinClass = true;
	}
	if ($ClassName) {
		$cond .= " AND yc.ClassTitleEN='".$ClassName."'";
		$joinClass = true;
	}
	if ($joinClass) {
		$joinClass = "INNER JOIN YEAR_CLASS_USER ycu ON ycu.UserID=u.UserID
						INNER JOIN YEAR_CLASS yc ON yc.YearClassID=ycu.YearClassID AND yc.AcademicYearID='".$AcademicYearID."'
						INNER JOIN YEAR y ON y.YearID=yc.YearID";
		$classNameField = Get_Lang_Selection('yc.ClassTitleB5', 'yc.ClassTitleEN');
		$classNumberField = 'ycu.ClassNumber';				
	}
	else {
		$joinClass = '';
		$classNameField = '';
		$classNumberField = '';
	}
}

$student_name = getNameFieldByLang("u.",$isTitleDisabled=true);

$keyword = standardizeFormPostValue($_POST['keyword']);
if($keyword!="")
{
	$kw = $libguidance->Get_Safe_Sql_Like_Query($keyword);
	$cond .= " AND ($student_name LIKE '%$kw%' OR c.MeetingDate LIKE '%$kw%' OR cmc.Comment LIKE '%$kw%' OR cmc.Followup LIKE '%$kw%')";
	unset($kw);
}


$sql = "SELECT	
				u.WebSAMSRegNo,				
				".$classNameField." AS ClassName,
				".$classNumberField." AS ClassNumber,
				".$student_name." AS StudentName,
				u.Gender,
				c.MeetingDate,
				cmc.Comment,
				cmc.Followup
		FROM 
				INTRANET_GUIDANCE_CLASSTEACHER_MEETING c
		INNER JOIN INTRANET_GUIDANCE_CLASSTEACHER_MEETING_COMMENT cmc ON cmc.MeetingID=c.MeetingID
		INNER JOIN
				INTRANET_USER u ON u.UserID=cmc.StudentID ".$joinClass ."
		WHERE 1 ".$cond."
		ORDER BY u.WebSAMSRegNo, ClassName, ClassNumber";

$ctc_rs = $libguidance->returnResultSet($sql);
$student_info = BuildMultiKeyAssoc($ctc_rs, 'WebSAMSRegNo', array('ClassName','ClassNumber','StudentName','Gender'));
$comment_info = BuildMultiKeyAssoc($ctc_rs, array('WebSAMSRegNo','MeetingDate'), array('Comment','Followup'));
$meeting_date_info = BuildMultiKeyAssoc($ctc_rs, array('MeetingDate'), array('MeetingDate'), 1);
sort($meeting_date_info);

$ExportColumn = array();
$ExportColumn[0] = $Lang['eGuidance']['ctmeeting']['Export']['WebSAMSRegNo'];
$ExportColumn[1] = sprintf($Lang['eGuidance']['ctmeeting']['Export']['ClassName'],substr($currentYearName,0,4));
$ExportColumn[2] = $Lang['eGuidance']['ctmeeting']['Export']['ClassNo'];
$ExportColumn[3] = $Lang['eGuidance']['ctmeeting']['Export']['StudentName'];
$ExportColumn[4] = $Lang['eGuidance']['ctmeeting']['Export']['Gender'];

$ExportArr = array();

$i = 0;
foreach((array)$student_info as $webSAMSRegNo=>$student) {
	$ExportArr[$i][0] = $webSAMSRegNo;
	$ExportArr[$i][1] = $student['ClassName'];
	$ExportArr[$i][2] = $student['ClassNumber'];
	$ExportArr[$i][3] = $student['StudentName'];
	$ExportArr[$i][4] = $student['Gender'];

	foreach((array)$meeting_date_info as $k=>$date) {
		if ($i == 0) {
			$ExportColumn[$k*2+5] = $date;
			$ExportColumn[$k*2+6] = sprintf($Lang['eGuidance']['ctmeeting']['Export']['Followup'],$date);
		}

		if ($comment_info[$webSAMSRegNo][$date] && $comment_info[$webSAMSRegNo][$date]['Comment']) {
			$ExportArr[$i][$k*2+5] = $comment_info[$webSAMSRegNo][$date]['Comment'];
			$ExportArr[$i][$k*2+6] = $comment_info[$webSAMSRegNo][$date]['Followup'];
		}
		else {
			$ExportArr[$i][$k*2+5] = '';
			$ExportArr[$i][$k*2+6] = '';
		}
	}
	$i++;
}

intranet_closedb();

$export_content = $lexport->GET_EXPORT_TXT($ExportArr, $ExportColumn, $Delimiter="", $LineBreak="\r\n", $ColumnDefDelimiter="", $DataSize=0, $Quoted="11");

$filename = 'ClassTeacherComments.csv';
$lexport->EXPORT_FILE($filename, $export_content);

?>