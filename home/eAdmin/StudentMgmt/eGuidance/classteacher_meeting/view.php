<?
/*
 * 	modifying:
 * 
 * 	Log
 *
 * 	2017-11-14 [Cameron]
 * 		- ej retrieve ClassName directly, ip uses getClassNameByLang()
 * 
 * 	2017-09-11 [Cameron]
 * 		- create this file
 * 
 */

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/eGuidance_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/guidance_conf.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/libguidance.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/libguidance_ui.php");

intranet_auth();
intranet_opendb();

$libguidance = new libguidance();
$permission = $libguidance->getUserPermission();
if (!$permission['admin'] && !$permission['current_right']['MGMT']['CLASSTEACHERMEETING']) {
	$libguidance->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

if (is_array($MeetingID)) {
	$meetingID = (count($MeetingID)==1) ? $MeetingID[0] : '';
}
else {
	$meetingID = $MeetingID;
}

if (!$meetingID) {
	header("location: index.php");
}

$libguidance_ui = new libguidance_ui();
$linterface = new interface_html();


$lclass = new libclass();
$rs_ctmeeting = array();
$rs_ctmeeting = $libguidance->getClassTeacherMeeting($meetingID);

if (count($rs_ctmeeting) == 1) {
	$rs_ctmeeting = $rs_ctmeeting[0];
	$meetingDate = $rs_ctmeeting['MeetingDate'];
	
	if ($junior_mck) {
		$yearName = $rs_ctmeeting['YearName'];
		$className = $rs_ctmeeting['ClassName'];
	}
	else {
		$yearClassID = $rs_ctmeeting['YearClassID'];
		$className = $lclass->getClassNameByLang($yearClassID);
	}
}
else {
	header("location: index.php");
}


# menu highlight setting
$CurrentPageArr['eGuidance'] = 1;
$CurrentPage = "PageClassteacherMeeting";
$CurrentPageName = $Lang['eGuidance']['name'];

### Title ###
$TAGS_OBJ = array();
$TAGS_OBJ[] = array($Lang['eGuidance']['menu']['Management']['ClassteacherMeeting']);

$MODULE_OBJ = $libguidance->GET_MODULE_OBJ_ARR();
$PAGE_NAVIGATION[] = array($Lang['eGuidance']['menu']['Management']['ClassteacherMeeting'], "index.php");
$PAGE_NAVIGATION[] = array($Lang['Btn']['View'], "");

$linterface->LAYOUT_START();

?>
<?=$linterface->Include_Thickbox_JS_CSS();?>
<script type="text/javascript">
<?include($PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eGuidance/common_js.php");?>

function click_print(){
	$('#form1').submit();
}
</script>

<form name="form1" id="form1" method="post" action="print.php" target="_blank">
<table id="main_table" width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td class="navigation"><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
					<td>
						<div class="Conntent_tool"  style="float: right;">
							<a href="javascript:click_print()" class="print"> <?=$Lang['Btn']['Print']?></a>     
							<br style="clear:both" />
						</div>
					</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="5" cellspacing="0">
				<tr>
					<td>
						<table align="center" class="form_table_v30">

							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['eGuidance']['Class']?></td>
								<td class="tabletext"><?=$className?></td>
							</tr>

							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['eGuidance']['ctmeeting']['MeetingDate']?></td>
								<td class="tabletext"><?=((empty($rs_ctmeeting['MeetingDate']) || ($rs_ctmeeting['MeetingDate'] == '0000-00-00'))?'-':$rs_ctmeeting['MeetingDate'])?></td>
							</tr>

							<tr valign="top">
								<td colspan="2"></td>
							</tr>
							
							<tr valign="top">
								<td colspan="2"><?=$linterface->GET_NAVIGATION2($Lang['eGuidance']['ctmeeting']['Comments'])?></td>
							</tr>
							<tr valign="top">
								<td colspan="2">
									<?=($junior_mck ? $libguidance_ui->getClassTeacherCommentTable($meetingID, $yearClassID='', $view=true, $yearName, $className) : $libguidance_ui->getClassTeacherCommentTable($meetingID, $yearClassID, $view=true))?>		
								</td>
							</tr>
							
							<?=$libguidance_ui->getAttachmentLayout('CTMeeting',$meetingID,'view')?>
							
						</table>
					</td>
				</tr>
				<tr>
					<td height="1" class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
				</tr>
				<tr>
					<td align="right">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="center">
									
							<? 	if ($permission['admin'] || in_array('MGMT', (array)$permission['current_right']['MGMT']['CLASSTEACHERMEETING'])) {
									echo $linterface->GET_ACTION_BTN($button_edit, "button", "window.location='edit.php?MeetingID=$meetingID'").'&nbsp';
								}
								echo $linterface->GET_ACTION_BTN($button_back, "button", "window.location='index.php'");							
							?>									
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>

<input type="hidden" name="MeetingID" id="MeetingID" value="<?=$rs_ctmeeting['MeetingID']?>">
</form>
<?
	$linterface->LAYOUT_STOP();
	intranet_closedb();
?>