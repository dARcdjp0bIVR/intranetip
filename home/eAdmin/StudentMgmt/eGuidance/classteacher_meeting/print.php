<?
/**
 * modifying: 
 * 
 * Change Log:
 * 
 * 2017-11-13 	Cameron
 * 		- add followup column
 * 
 * 2017-11-06 	Cameron
 * 		- should include libinterface.php before libguidance_ui.php
 * 		- ej should use getClassName(), ip uses getClassNameByLang()
 * 		- fix meta charset
 * 		
 * 2017-09-15  	Cameron
 *  	- Create this file
 */

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/eGuidance_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/guidance_conf.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/libguidance.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/libguidance_ui.php");
include_once($PATH_WRT_ROOT."includes/mpdf/mpdf.php");

######## Init START ########
intranet_auth();
intranet_opendb();

$lclass = new libclass();
$libguidance = new libguidance();
$libguidance_ui = new libguidance_ui();
$resultData = array();
$reportDetails = array();

define('TABLE_TYPE_EMPTY', 'tableEmpty');
define('TABLE_TYPE_HORIZONTAL', 'tableHorizontal');
define('TABLE_TYPE_VERTICAL', 'tableVertical');


$MeetingID = $_POST['MeetingID'];

######## Init END ########


######## Access Right START ########
$permission = $libguidance->getUserPermission();
if (!$permission['admin'] && !$permission['current_right']['MGMT']['CLASSTEACHERMEETING']) {
	$libguidance->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}
######## Access Right END ########



######## Init PDF START ########
$mpdf = new mPDF('','A4',0,'',5,5,5,15);
//$mpdf->mirrorMargins = 1;
######## Init PDF END ########


######## Load all Data START ########
#### Load Header Data START ####
$school_name = $_SESSION["SSV_PRIVILEGE"]["school"]["name"];
$report_title = "{$Lang['eGuidance']['report']['eGuidanceReport']} - {$Lang['eGuidance']['menu']['Management']['ClassteacherMeeting']}";

#### Load Header Data END ####

#### Get Class Teacher Meeting START ####
$rs_ctmeeting = $libguidance->getClassTeacherMeeting($MeetingID);


#### Get Class Teacher Meeting END ####

######## Load all Data END ########


######## Pack data START ########
/**
 * Demo data
$table = array(
    'class' => 'ctmeeting',
    'colspan' => 4,
    'tbody' => array(
        array(
            'type' => TABLE_TYPE_HORIZONTAL,
            'data-cell-colspan' => 3,
            'field' => array( 'ClassName', 'MeetingDate' ),
            'data' => array('F2A', '2017-09-15')
        ),
        array(
            'type' => TABLE_TYPE_VERTICAL,
            'field' => array( 'WebSAMSRegNo', 'ClassNumber', 'StudentName', 'Comment', 'Followup' ),
            'data' => array(
                array('#1511041', '1', 'Sally Tang', 'Always looks sad', 'Refer to social worker'),
                array('#1511042', '2', 'Dona Liu', 'Angry all the times', 'plan to have a meeting')
            )
        ),
    )
);
 */
foreach($rs_ctmeeting as $rs){
    
    $meetingDate = $rs['MeetingDate'];
	if ($junior_mck) {
		$yearName = $rs['YearName'];
		$className = $rs['ClassName'];
		$comments = $libguidance->getClassTeacherComments($MeetingID,$yearClassID='',$yearName,$className);
	}
	else {
		$yearClassID = $rs['YearClassID'];
		$className = $lclass->getClassNameByLang($yearClassID);
		$comments = $libguidance->getClassTeacherComments($MeetingID,$yearClassID);
	}
    
    
    $allTable = array();
    
    #### Basic information START ####
    $reportDetails[] = array(
    	'meetingDate' => $meetingDate,
        'className' => $className
    );
    #### Basic information END ####
    
    $data = array();
    foreach((array)$comments as $com){
    	if ($com['Comment']) {		// don't show if no comment
	        $data[] = array(
	            $com['WebSAMSRegNo'],
	            $com['ClassNumber'],
	            $com['StudentName'],
	            $com['Comment'],
	            $com['Followup']?$com['Followup']:'-'
	        );
    	}
    }

    $table = array(
        'class' => 'ctmeeting',
        'colspan' => 5,
        'tbody' => array(
            array(
                    'type' => TABLE_TYPE_HORIZONTAL,
                    'data-cell-colspan' => 4,
                    'field' => array( 
                        $Lang['eGuidance']['Class'], 
                        $Lang['eGuidance']['ctmeeting']['MeetingDate']
                    ),
                    'data' => array(
                        $className,
                        $meetingDate
                    )
            ),        
            array(
                'type' => TABLE_TYPE_VERTICAL,
                'field' => array(
                    $Lang['eGuidance']['ctmeeting']['StudentRegNo'],
                    $Lang['General']['ClassNumber'],
                    $Lang['eGuidance']['ctmeeting']['StudentName'],
                    $Lang['eGuidance']['ctmeeting']['Comments'],
                    $Lang['eGuidance']['ctmeeting']['Followup']
                ),
                'data' => $data
            ),
        )
    );
    $allTable[] = $table;
    
    $resultData[] = $allTable;
}
$countResultData = count($resultData);
######## Pack data END ########

################################ Load header to PDF START ################################
ob_start();
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?=($junior_mck) ? 'big5-hkscs':'utf-8'?>" />
<title></title>
<style type="text/css">

body{color: #000;  font-family:  msjh !important; font-size:0.9em; line-height:0.9em; margin:0; padding:0; -webkit-print-color-adjust: exact; font-stretch: condensed; /*font-size-adjust: 0.5;*/}

/**** Header style START ****/
.schoolName, .reportType, .reportDetails{
    text-align: center;
}
.reportDetails{
    margin-top: 0;
}
.printTime{
    text-align: right;
}
/**** Header style END ****/


/**** Footer style START ****/
.footer{
    width: 100%;
}
.footer td{
    width: 33%;
    vertical-align: bottom; 
    font-family:  msjh !important;
    font-size: 8pt; 
    color: #000000; 
    font-weight: bold;
    padding: 3px;
    border: 0;
    border-top: 1px solid black;
}
/**** Footer style END ****/


/**** Basic table style START ****/
table{
    width: 200mm;
    border-collapse: collapse;
    margin-top: 3mm;
    overflow: visible;
}
.noRecordTd{
    text-align: center;
}

th, td{
    border: 1px solid #ddd;
    text-align: left;
    vertical-align: top;
    padding: 3mm;
}
tr.headerRow th{
    font-size: 1.5em;
    background-color: #487db4;
    color: white;
    text-align: center;
}

.tableVertical .field{
    text-align: center;
    white-space: nowrap;
    color: white;
    background-color: #59afc6;
}

.tableIndex td, .tableEmpty td{
    border: 0;
}
.tableEmpty td{
    font-size:1px !important;
}
.tableIndex td{
    padding-bottom: 0;
}
/**** Basic table style END ****/


/**** Class Teacher Meeting table style START ****/
.ctmeeting .col_1{
    width: 12mm;
}
.ctmeeting .col_2{
    width: 8mm;
}
.ctmeeting .col_3{
    width: 30mm;
}
.ctmeeting .col_4{
    width: 100mm;
}
/**** Class Teacher Meeting style END ****/

</style>
</head>

<?php if($countResultData == 0): ?>
    <h2 class="schoolName"><?=$school_name ?></h2>
    <h3 class="reportType"><?=$report_title ?></h3>
    
    <hr style="margin-top: 0;margin-bottom: 3mm;"/>
<?php endif; ?>


<?php
$pageHeader = ob_get_clean();
if($_GET['debug']){
    echo $pageHeader;
}else{
    $mpdf->WriteHTML($pageHeader);
}
@ob_end_clean();
################################ Load header to PDF END ################################


################################ Load Data to PDF START ################################
if($countResultData):
    foreach($resultData as $resultIndex => $meetingRecord): 
        $hasPageBreak = $resultIndex < $countResultData - 1;
######## Header START ########
        ob_start();
?>
        <h2 class="schoolName"><?=$school_name ?></h2>
        <h3 class="reportType"><?=$report_title ?></h3>
        
        <hr style="margin-top: 0;margin-bottom: 3mm;"/>
<?php
        $header = ob_get_clean();
        $mpdf->WriteHTML($header);
        @ob_end_clean();
######## Header END ########


######## Footer START ########
        ob_start();
?>
        <table class="footer">
        	<tr>
                <td><?=($junior_mck ? convert2unicode($reportDetails[$resultIndex]['className'],true,1) : $reportDetails[$resultIndex]['className']).' '.$reportDetails[$resultIndex]['meetingDate'] ?></td>
                <td style="text-align: center;"></td>
        		<td style="text-align: right;">{PAGENO}</td>
        	</tr>
        </table>
<?php
        $footer = ob_get_clean();
        @ob_end_clean();
######## Footer END ########

    
######## Content START ########
foreach($meetingRecord as $index => $table):
    ob_start();
?>

<table class="<?=$table['class'] ?>">
    
    <?php 
    if(count($table['tbody'])):
        foreach($table['tbody'] as $tbody): 
        	if($tbody['type'] == TABLE_TYPE_EMPTY): 
	?>
            	<tbody class="tableEmpty">
            		<tr><td></td></tr>
            	</tbody>
        	<?php
        	elseif($tbody['type'] == TABLE_TYPE_HORIZONTAL): 
        	//// Horizontal table START ////
        	?>
                <tbody class="tableHorizontal">
        			<?php foreach($tbody['field'] as $fieldIndex => $field): ?>
                    	<tr>
                    		<th class="field col_1">
                    			<?=nl2br($field) ?>
                    		</th>
                    		<td class="value col_2" colspan="<?=($tbody['data-cell-colspan'])?$tbody['data-cell-colspan']:1 ?>">
                    			<?=nl2br($tbody['data'][$fieldIndex]) ?>
                    		</td>
                    	</tr>
                	<?php endforeach; ?>
            	</tbody>
        	<?php  
        	//// Horizontal table END ////
        	elseif($tbody['type'] == TABLE_TYPE_VERTICAL): 
        	//// Vertical table START //// 
        	?>
                <tbody class="tableVertical">
                	<tr>
            			<?php foreach($tbody['field'] as $fieldIndex => $field): ?>
                    		<th class="field col_<?=$fieldIndex+1 ?>">
                    			<?=nl2br($field) ?>
                    		</th>
                    	<?php endforeach; ?>
                	</tr>
                	
        			<?php 
    				$dataCount = count($tbody['data']);
    			    if($dataCount):
    			        foreach($tbody['data'] as $dataIndex => $data): 
        			?>
                    	<tr>
            				<?php 
            				foreach($data as $fieldIndex => $value): 
            				    $css = ' col_' . ($fieldIndex+1);
            				    if($dataIndex == $dataCount-1){
            				        $css .= ' last';
            				    }
            				?>
                        		<td class="value <?=$css?>">
                        			<?=nl2br($value); ?>
                        		</td>
                        	<?php 
                        	endforeach; 
                        	?>
                    	</tr>
                	<?php 
                	    endforeach; 
            	    else:
            	    ?>
            	    	<tr>
            	    		<td colspan="<?=count($tbody['field']) ?>" class="noRecordTd"><?=$Lang['General']['NoRecordAtThisMoment'] ?></td>
        	    		</tr>
            	    <?php
        	        endif;
                	?>
            	</tbody>
        	<?php  
        	//// Vertical table END ////
        	endif;
    	endforeach; 
	else:
	?>
    	<tr>
    		<td colspan="<?=($table['colspan'])?$table['colspan']:2 ?>" class="noRecordTd"><?=$Lang['General']['NoRecordAtThisMoment'] ?></td>
		</tr>
	<?php
	endif;
	?>
    
</table>
<?php 
    $page = ob_get_clean();
    if($_GET['debug']){
        echo $page;
    }else{
        $mpdf->SetHTMLFooter($footer);
        $mpdf->WriteHTML($page);
    }
    @ob_end_clean();

endforeach; // foreach($meetingRecord as $index => $table)
######## Content END ########


######## Print time START ########
ob_start();
?>

<h4 class="printTime"><?=$Lang['eGuidance']['report']['PrintTime']?>: <?=date('Y-m-d H:i:s') ?></h4>


<?php
        $page = ob_get_clean();
        if($_GET['debug']){
            echo $page;
        }else{
            $mpdf->SetHTMLFooter($footer);
            $mpdf->WriteHTML($page);
            if($hasPageBreak){
                $mpdf->WriteHTML('<pagebreak resetpagenum="1" suppress="off" />');
            }
        }
        @ob_end_clean();
######## Print time END ########
    endforeach; // foreach($resultData as $resultIndex => $meetingRecord)

else:
    $mpdf->WriteHTML("<h1>{$Lang['General']['NoRecordAtThisMoment']}</h1>");
endif;
################################ Load Data to PDF END ################################



if(!$_GET['debug']){
    $mpdf->Output();
}
@ob_end_clean();

