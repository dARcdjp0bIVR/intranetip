<?php
/*
 * 	Log
 * 	
 * 	Purpose: import class teacher meeting comments validation
 * 	!!! Note: ignore empty row (there's no comment for the student)
 *
 * 	Date:	2017-12-11 [Cameron]
 * 			- fix bug: check isset($ctStudentAry[$_meetingDate][$_userID]) before use
 * 		 	- handle case studentNotRegisterInTheYear
 * 
 * 	Date:	2017-11-15 [Cameron]
 * 			create this file
 * 
 */
 
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/eGuidance_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/guidance_conf.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/libguidance.php");
 

intranet_auth();
intranet_opendb();

$libguidance = new libguidance();
$permission = $libguidance->getUserPermission();
if (!$permission['admin'] && !$permission['current_right']['MGMT']['CLASSTEACHERMEETING']) {
	$libguidance->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$limport = new libimporttext();

# Initialization
$result = array();

### get header
//$format_array = array("Ad No", "Name");

### get csv data
$filepath = $_FILES['userfile']['tmp_name'];
$data = $limport->GET_IMPORT_TXT($filepath, $incluedEmptyRow=0, $lineBreakReplacement='<!--LineBreak-->');
$meetingDateAry = array();
$meetingAcademicYearID = array();
$meetingAcademicYearName = array();

if (count($data)) {
	$header_row = $data[0];
	$nr_col = count($header_row);
	$header_error = false;	// initial

	if (($nr_col % 2 != 0) || ($nr_col < 4)|| ($header_row[0] != 'Ad No') || ($header_row[1] != 'Name')) {	// number of column must be even and at least 4 cols
		$header_error = true;
	}
	else {
		for($i=2;$i<$nr_col;$i+=2) {
			$meetingDate = getDefaultDateFormat($header_row[$i]);
			if (!checkDateIsValid($meetingDate)) {
				$header_error = true;
				break;
			}
			else {
				$academicYearTermInfo = getAcademicYearInfoAndTermInfoByDate($meetingDate);
				if (count($academicYearTermInfo)) {
					$meetingDateAry[] = $meetingDate;
					if (!in_array($academicYearTermInfo[0],$meetingAcademicYearID)) {
						$meetingAcademicYearID[] = $academicYearTermInfo[0];  
						$meetingAcademicYearName[] = $academicYearTermInfo[1];
					}
				}
				else {	// academic year not found in database
					header("Location: import.php?returnMsgKey=DateNotExist&MeetingDate=$meetingDate");
					exit();
				}
			}
		}
	}
	
	if ($header_error) {
		header("Location: import.php?returnMsgKey=WrongCSVHeader");
		exit();
	}
		
	$limport->SET_CORE_HEADER($header_row);	
}

//if (!$limport->VALIDATE_HEADER($data, true) || !$limport->VALIDATE_DATA($data)) {
//	header("Location: import.php?returnMsgKey=WrongCSVHeader");
//	exit();
//}


# Page heading setting
$linterface = new interface_html();
$CurrentPageArr['eGuidance'] = 1;
$CurrentPage = "PageClassteacherMeeting";
$CurrentPageName = $Lang['eGuidance']['name'];

### Title ###
$TAGS_OBJ = array();
$TAGS_OBJ[] = array($Lang['eGuidance']['menu']['Management']['ClassteacherMeeting']);

# handle return message
$MODULE_OBJ = $libguidance->GET_MODULE_OBJ_ARR();
$returnMsgKey = standardizeFormGetValue($_GET['returnMsgKey']);
if ($returnMsgKey == "DateNotExist") {
	$returnMsg = $Lang['eGuidance']['ctmeeting']['ImportReturnMsg'][$returnMsgKey].": ".standardizeFormGetValue($_GET['MeetingDate']);	
}
else {
	$returnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];	
}
$linterface->LAYOUT_START($returnMsg);

# page navigation (leave the array empty if no need)
$PAGE_NAVIGATION[] = array();
$PAGE_NAVIGATION[] = array($Lang['eGuidance']['menu']['Management']['ClassteacherMeeting'], "index.php");
$PAGE_NAVIGATION[] = array($Lang['Btn']['Import'], "");
$htmlAry['navigation'] = $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION);


### step display
$htmlAry['generalImportStepTbl'] = $linterface->GET_IMPORT_STEPS($CurrStep=2,$Lang['eGuidance']['Import']['Steps']);


### delete old temp data
$table = "INTRANET_GUIDANCE_CLASSTEACHER_MEETING_COMMENT_IMPORT";
$sql = "Delete From ".$table." Where LastModifiedBy = '".$_SESSION['UserID']."'";
$successAry['deleteOldTempData'] = $libguidance->db_db_query($sql);

## get student_id associate array
$studentInfo = $libguidance->getStudentID();
$studentIDAry = BuildMultiKeyAssoc($studentInfo, 'WebSAMSRegNo', 'UserID', 1);
$ctStudentAry = $libguidance->getClassTeacherMeetingStudent();		// existing record in class teacher meeting comments by MeetingDate

### analyse data
array_shift($data);
$numOfData = count($data);
$errorMsgAssoAry = array();
$errorNotRegisterDate = array();
$insertAry = array();
$curentAcademicYearID = Get_Current_Academic_Year_ID();

if ($junior_mck) {
	$userAcademicYearHistory = $libguidance->getUserAcademicYearHistory($meetingAcademicYearName);
}
else {
	$userAcademicYearHistory = $libguidance->getUserAcademicYearHistory('',$meetingAcademicYearID);
}
$userAcademicYearHistoryAry = BuildMultiKeyAssoc($userAcademicYearHistory, array('UserID','AcademicYear'), 'AcademicYear',1);

for ($i=0; $i<$numOfData; $i++) {
	$_rowNum = $i+2;
	$_row = $data[$i];
	$_webSAMSRegNo = trim($_row[0]);	// webSAMSRegNo
	if (substr($_webSAMSRegNo,0,1) != '#') {
		$_webSAMSRegNo = '#'.$_webSAMSRegNo;
	}
	
	$_studentName = trim($_row[1]);

	if (($_webSAMSRegNo != '#') && isset($studentIDAry[$_webSAMSRegNo]['UserID'])) {
		$_userID = $studentIDAry[$_webSAMSRegNo];
		if ($_userID == '') {
			$errorMsgAssoAry[$_rowNum][] = 'studentNotFound';
		}
	}
	else {
		$errorMsgAssoAry[$_rowNum][] = 'webSAMSRegNoNotFound';
	}

	for($j=2;$j<$nr_col;$j+=2) {
		$_meetingDate = $meetingDateAry[$j/2-1];
		if (isset($ctStudentAry[$_meetingDate][$_userID]) && $ctStudentAry[$_meetingDate][$_userID]) {
			$errorMsgAssoAry[$_rowNum][] = 'duplicateRecord';
		}
		else {
			$yearInfo = getAcademicYearInfoAndTermInfoByDate($_meetingDate);
			$academicYearID = $yearInfo[0];
			$yearNameEN = $yearInfo[1];		// get YearNameEN
			
			$userExistInTheYear = false;
			if ($junior_mck) {
				if (isset($userAcademicYearHistoryAry[$_userID][$yearNameEN]) && $userAcademicYearHistoryAry[$_userID][$yearNameEN] == $yearNameEN) {
					$userExistInTheYear = true;
				}
			}
			else {
				if (isset($userAcademicYearHistoryAry[$_userID][$academicYearID]) && $userAcademicYearHistoryAry[$_userID][$academicYearID] == $academicYearID) {
					$userExistInTheYear = true;
				}
			}
			
			$_comment = trim($_row[$j]);
			$_followup = trim($_row[$j+1]);
		
			if ($_comment != '') {
				if ($userExistInTheYear == true ) {
					$insertAry[] = " ('".$_meetingDate."', '".
										$libguidance->Get_Safe_Sql_Query($_webSAMSRegNo)."', '".
										$_userID."', '".
										$libguidance->Get_Safe_Sql_Query($_studentName)."', '".
										$libguidance->Get_Safe_Sql_Query($_comment)."', '".
										$libguidance->Get_Safe_Sql_Query($_followup)."', now(), '".
										$libguidance->Get_Safe_Sql_Query($_SESSION['UserID'])."') ";
				}
				else {
					$errorMsgAssoAry[$_rowNum][] = 'studentNotRegisterInTheYear';
					$errorNotRegisterDate[$_rowNum][] = $_meetingDate;
				}
			}
		}
	}
	
}

### simple statistics
$numOfErrorRow = count($errorMsgAssoAry);
$numOfSuccessRow = $numOfData - $numOfErrorRow;


### insert csv data to temp table
$numOfInsert = count($insertAry);
if ($numOfInsert > 0) {
	$insertChunkAry = array_chunk($insertAry, 1000);
	$numOfChunk = count($insertChunkAry);
	
	for ($i=0; $i<$numOfChunk; $i++) {
		$_insertAry = $insertChunkAry[$i];
		
		$sql = "Insert Into $table
					(MeetingDate,WebSAMSRegNo,StudentID,Name,Comment,Followup,DateModified,LastModifiedBy)
				Values ".implode(', ', (array)$_insertAry);
		$successAry['insertData'][] = $libguidance->db_db_query($sql);
	}
}


# validation result to display
if ($numOfErrorRow > 0) {
	$numOfErrorDisplay = '<span class="tabletextrequire">'.$numOfErrorRow.'</span>';
}
else {
	$numOfErrorDisplay = 0;
}

$x = '';
$x .= '<table class="form_table_v30">'."\r\n";
	$x .= '<tr>'."\r\n";
		$x .= '<td class="field_title">'.$Lang['eGuidance']['Import']['TotalRecord'].'</td>'."\r\n";
		$x .= '<td>'.$numOfData.'</td>'."\n";
	$x .= '</tr>'."\r\n";
	$x .= '<tr>'."\n";
		$x .= '<td class="field_title">'.$Lang['eGuidance']['Import']['ValidRecord'].'</td>'."\n";
		$x .= '<td>'.$numOfSuccessRow.'</td>'."\n";
	$x .= '</tr>'."\n";
	$x .= '<tr>'."\n";
		$x .= '<td class="field_title">'.$Lang['eGuidance']['Import']['InvalidRecord'].'</td>'."\n";
		$x .= '<td>'.$numOfErrorDisplay.'</td>'."\n";
	$x .= '</tr>'."\n";
$x .= '</table>'."\r\n";
$htmlAry['importInfoTbl'] = $x;


# error display
$x = '';
if ($numOfErrorRow > 0) {
	$x .= '<table class="common_table_list_v30 view_table_list_v30">'."\n";
		$x .= '<thead>'."\n";
			$x .= '<tr>'."\n";
				$x .= '<th>'.$Lang['General']['ImportArr']['Row'].'</th>'."\n";
				$x .= '<th>'.$Lang['eGuidance']['ctmeeting']['Import']['WebSAMSRegNo'].'</th>'."\n";
				$x .= '<th>'.$Lang['eGuidance']['ctmeeting']['Import']['StudentName'].'</th>'."\n";
				$x .= '<th>'.$Lang['General']['Remark'].'</th>'."\n";
			$x .= '</tr>'."\n";
		$x .= '</thead>'."\n";
		$x .= '<tbody>'."\n";
		
			foreach ((array)$errorMsgAssoAry as $_rowNum => $_errorAry) {
				$aryIndex = $_rowNum - 2;
				$_row = $data[$aryIndex];

				$_webSAMSRegNo = trim($_row[0]);	// webSAMSRegNo
				if (substr($_webSAMSRegNo,0,1) != '#') {
					$_webSAMSRegNo = '#'.$_webSAMSRegNo;
				}
				$_studentName = trim($_row[1]);

				$_errorDisplayAry = array();

				if (in_array('studentNotFound', $_errorAry)) {
					$_studentName = '<span class="tabletextrequire">'.$_studentName.'</span>';
					$_errorDisplayAry[] = $Lang['eGuidance']['ctmeeting']['ImportError']['StudentNotFound'];
				}
				
				if (in_array('webSAMSRegNoNotFound', $_errorAry)) {
					$_webSAMSRegNo = '<span class="tabletextrequire">'.$_webSAMSRegNo.'</span>';
					$_errorDisplayAry[] = $Lang['eGuidance']['ctmeeting']['ImportError']['WebSAMSRegNoNotFound'];
				}

				if (in_array('duplicateRecord', $_errorAry)) {
					$_studentName = '<span class="tabletextrequire">'.$_studentName.'</span>';
					$_errorDisplayAry[] = $Lang['eGuidance']['ctmeeting']['ImportError']['DuplicateRecord'];
				}

				if (in_array('studentNotRegisterInTheYear', $_errorAry)) {
					$_studentName = '<span class="tabletextrequire">'.$_studentName.'</span>';
					$_errorDisplayAry[] = $Lang['eGuidance']['ctmeeting']['ImportError']['StudentNotRegisterInTheYear'] .": ". implode(",",$errorNotRegisterDate[$_rowNum]);
				}
				
				$_errorDisplay = '- '.implode('<br />- ', $_errorDisplayAry);
				
				$x .= '<tr>'."\n";
					$x .= '<td>'.$_rowNum.'</td>'."\n";
					$x .= '<td>'.$_webSAMSRegNo.'</td>'."\n";
					$x .= '<td>'.$_studentName.'</td>'."\n";
					$x .= '<td>'.$_errorDisplay.'</td>'."\n";
				$x .= '</tr>'."\n";
			}
		$x .= '</tbody>'."\n";
	$x .= '</table>'."\n";
}
$htmlAry['errorTbl'] = $x;



### action buttons
$htmlAry['submitBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Import'], "button", "goSubmit()", 'submitBtn', $ParOtherAttribute="", ($numOfErrorRow>0)? true : false, $ParClass="", $ParExtraClass="actionBtn");
$htmlAry['backBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Back'], "button", "goBack()", 'backBtn', $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");
$htmlAry['cancelBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Cancel'], "button", "goCancel()", 'cancelBtn', $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");

?>
<script type="text/JavaScript" language="JavaScript">
function trim(str) {
	return str.replace(/^\s+|\s+$/g,"");
}

function goSubmit() {
	$('form#form1').attr('action', 'import_update.php').submit();
}

function goBack() {
	window.location = 'import.php';
}
function goCancel() {
	window.location = 'index.php';
}

</script>
<form id="form1" name="form1" method="POST">
	<?=$htmlAry['navigation']?>
	<p class="spacer"></p>
	<br />
	
	<div style="text-align:left">
		<?=$htmlAry['generalImportStepTbl']?>
	</div>
		
	<div class="table_board">
		<?=$htmlAry['importInfoTbl']?>
		<?=$htmlAry['errorTbl']?>
		<br style="clear:both;" />
		
		<div class="edit_bottom_v30">
			<p class="spacer"></p>
			<?=$htmlAry['submitBtn']?>
			<?=$htmlAry['backBtn']?>
			<?=$htmlAry['cancelBtn']?>
			<p class="spacer"></p>
		</div>
	</div>
</form>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>