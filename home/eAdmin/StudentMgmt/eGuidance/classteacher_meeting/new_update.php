<?php
/*
 * 	modifying:
 * 
 * 	Log
 *
 *	2017-11-13 [Cameron]
 *		- retrieve YearName and ClassName to support ej 
 *
 * 	2017-10-25 [Cameron]
 * 		- add Followup field
 *
 * 	2017-09-11 [Cameron]
 * 		- create this file
 * 
 */

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/eGuidance_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/libguidance.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

intranet_auth();
intranet_opendb();

$libguidance = new libguidance();
$permission = $libguidance->getUserPermission();
if (!$permission['admin'] && !in_array('MGMT', (array)$permission['current_right']['MGMT']['CLASSTEACHERMEETING'])) {
	$libguidance->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$ldb = new libdb();
$result = array();

$dataAry['MeetingDate'] = $MeetingDate;

if ($junior_mck) {
	$libFCM = new form_class_manage();
	$yearInfoArr = $libFCM->Get_Academic_Year_List(Get_Current_Academic_Year_ID());
	$yearInfoArr = current($yearInfoArr);
	$dataAry['YearName'] = $yearInfoArr['YearNameEN'];
	$dataAry['ClassName'] = $ClassName;
}
else {
	$dataAry['YearClassID'] = $YearClassID;	
}

$dataAry['LastModifiedBy'] = $_SESSION['UserID'];
$sql = $libguidance->INSERT2TABLE('INTRANET_GUIDANCE_CLASSTEACHER_MEETING',$dataAry,array(),false);

$ldb->Start_Trans();
## step 1: add to classteacher meeting
$res = $ldb->db_db_query($sql);
$result[] = $res;
if ($res) {
	$MeetingID = $ldb->db_insert_id();
	## step 2: add class teacher comments
	foreach((array)$Comment as $k=>$v) {
		unset($dataAry);
		$dataAry['MeetingID'] = $MeetingID;
		$dataAry['StudentID'] = $k;
		$dataAry['Comment'] = standardizeFormPostValue($v);
		$dataAry['Followup'] = standardizeFormPostValue(trim($Followup[$k]));
		$sql = $libguidance->INSERT2TABLE('INTRANET_GUIDANCE_CLASSTEACHER_MEETING_COMMENT',$dataAry,array(),false,false,false);	// no DateModified field
		$result[] = $ldb->db_db_query($sql);
	}

	## step 3: add attachment				
	if (count($FileID) > 0) {
		$fileID = implode("','",$FileID);
		$sql = "UPDATE INTRANET_GUIDANCE_ATTACHMENT SET RecordID='".$MeetingID."' WHERE FileID IN ('".$fileID."')";
		$result[] = $ldb->db_db_query($sql);
	}
	
}

if (!in_array(false,$result)) {
	$ldb->Commit_Trans();
	$returnMsgKey = 'AddSuccess';
}
else {
	$ldb->RollBack_Trans();
	$returnMsgKey = 'AddUnsuccess';
}
	
header("location: index.php?returnMsgKey=".$returnMsgKey);


intranet_closedb();

?>


