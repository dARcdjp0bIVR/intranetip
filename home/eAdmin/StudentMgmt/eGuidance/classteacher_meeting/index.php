<?
/*
 * 	Log
 *	
 *	### !!! important: ej ACADEMIC_YEAR.YearNameEN and PROFILE_CLASS_HISTORY.AcademicYear format: yyyy-yyyy or yyyy, 
 *
 *	2017-11-21 [Cameron]
 *		- fix AcademicYearID filter for IP
 *		- fix Import / Export linking for IP 
 *
 *	2017-11-20 [Cameron]
 *		- fix cookie and keyword search, set default sorting to descending by date
 *
 *	2017-11-15 [Cameron]
 *		- add Import & Export button
 *
 * 	2017-09-11 [Cameron] create this file
 */

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/eGuidance_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/guidance_conf.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/libguidance.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/libguidance_ui.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

# set cookies
$arrCookies[] = array("ck_page_size", "numPerPage");
$arrCookies[] = array("ck_page_no", "pageNo");
$arrCookies[] = array("ck_page_order", "order");
$arrCookies[] = array("ck_page_field", "field");

$arrCookies[] = $junior_mck ? array("ck_YearName", "YearName") : array("ck_AcademicYearID", "AcademicYearID");
$arrCookies[] = array("ck_ClassName", "ClassName");
$arrCookies[] = array("ck_keyword", "keyword");
if(isset($clearCoo) && $clearCoo == 1)
{
	clearCookies($arrCookies);
} 
else 
{
	updateGetCookies($arrCookies);
}

intranet_auth();
intranet_opendb();

$libguidance = new libguidance();
$permission = $libguidance->getUserPermission();
if (!$permission['admin'] && !$permission['current_right']['MGMT']['CLASSTEACHERMEETING']) {
	$libguidance->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}
    
# Page heading setting
$linterface = new interface_html();
$CurrentPageArr['eGuidance'] = 1;
$CurrentPage = "PageClassteacherMeeting";
$CurrentPageName = $Lang['eGuidance']['name'];

### Title ###
$TAGS_OBJ = array();
$TAGS_OBJ[] = array($Lang['eGuidance']['menu']['Management']['ClassteacherMeeting']);

$MODULE_OBJ = $libguidance->GET_MODULE_OBJ_ARR();
if ($returnMsgKey == 'AddSuccessNotifyFail' || $returnMsgKey == 'UpdateSuccessNotifyFail' || $returnMsgKey == 'AddAndNotifySuccess' || $returnMsgKey == 'UpdateAndNotifySuccess') {
	$returnMsg = $Lang['eGuidance']['ReturnMessage'][$returnMsgKey];
}
else {
	$returnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];
}
$linterface->LAYOUT_START($returnMsg);

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$pageNo = ($pageNo == "") ? 1 : $pageNo;
$order = ($order == "") ? 0 : $order;		// default descending
$field = ($field == "") ? 0 : $field;

$joinClass = false;
$cond = '';

if ($junior_mck) {
	$libFCM = new form_class_manage();
	$yearInfoArr = $libFCM->Get_Academic_Year_List(Get_Current_Academic_Year_ID());
	$yearInfoArr = current($yearInfoArr);
	$currentYearName = $yearInfoArr['YearNameEN'];
	
	if (empty($YearName)) {
		$YearName = $currentYearName; 
	}
	
	$libAY = new academic_year();
	$ayInfo = $libAY->Get_Academic_Year_Info_By_YearName($YearName);
	if (count($ayInfo)) {
		$ay = $libFCM->Get_Academic_Year_List($ayInfo[0]['AcademicYearID']);
		if (count($ay)) {
			$cond .= " AND c.MeetingDate>='".$ay[0]['AcademicYearStart']."'";
			$cond .= " AND c.MeetingDate<='".$ay[0]['AcademicYearEnd']."'";
		}
	}
	
	if ($YearName == $currentYearName) {
		if ($ClassName) {
			$cond .= " AND u.ClassName='".$ClassName."'";
		}
		$joinClass = '';
		$classNameField = 'u.ClassName';
	}
	else {
		if ($ClassName) {
			$cond .= " AND h.ClassName='".$ClassName."'";
		}
		$joinClass = "INNER JOIN PROFILE_CLASS_HISTORY h ON h.UserID=u.UserID AND (h.AcademicYear='".$libguidance->Get_Safe_Sql_Query($YearName)."' OR LEFT(h.AcademicYear,4)='".$libguidance->Get_Safe_Sql_Query(substr($YearName,0,4))."')";
		$classNameField = 'h.ClassName';
	}
}
else {
	$AcademicYearID = $AcademicYearID ? $AcademicYearID : Get_Current_Academic_Year_ID();	
	if ($AcademicYearID) {
		$libFCM = new form_class_manage();
		$ay = $libFCM->Get_Academic_Year_List($AcademicYearID);
		if (count($ay)) {
			$cond .= " AND c.MeetingDate>='".$ay[0]['AcademicYearStart']."'";
			$cond .= " AND c.MeetingDate<='".$ay[0]['AcademicYearEnd']."'";
		}
		
		$cond .= " AND yc.AcademicYearID='".$AcademicYearID."'";
		$joinClass = true;
	}
	if ($ClassName) {
		$cond .= " AND yc.ClassTitleEN='".$ClassName."'";
		$joinClass = true;
	}
	if ($joinClass) {
		$joinClass = "INNER JOIN YEAR_CLASS_USER ycu ON ycu.UserID=u.UserID
						INNER JOIN YEAR_CLASS yc ON yc.YearClassID=ycu.YearClassID AND yc.AcademicYearID='".$AcademicYearID."'
						INNER JOIN YEAR y ON y.YearID=yc.YearID";
		$classNameField = Get_Lang_Selection('yc.ClassTitleB5', 'yc.ClassTitleEN');				
	}
	else {
		$joinClass = '';
		$classNameField = '';
	}
}

$student_name = getNameFieldByLang("u.",$isTitleDisabled=true);

$keyword = $_POST['keyword'] ? standardizeFormPostValue($_POST['keyword']) : standardizeFormGetValue($keyword);
if($keyword!="")
{
	$kw = $libguidance->Get_Safe_Sql_Like_Query($keyword);
	$cond .= " AND ($student_name LIKE '%$kw%' OR c.MeetingDate LIKE '%$kw%' OR cmc.Comment LIKE '%$kw%' OR cmc.Followup LIKE '%$kw%')";
	unset($kw);
}

$li = new libdbtable2007($field, $order, $pageNo);

$sql = "SELECT	
				c.MeetingDate,
				CONCAT('<a href=\"view.php?MeetingID=',c.MeetingID,'\">',".$classNameField.",'</a>') ";
				
if ($permission['admin'] || in_array('MGMT', (array)$permission['current_right']['MGMT']['CLASSTEACHERMEETING'])) {
	$sql .= ",CONCAT('<input type=\'checkbox\' name=\'MeetingID[]\' id=\'MeetingID[]\' value=\'', c.`MeetingID`,'\'>') ";
	$extra_column = 2;
	$checkbox_col = "<th width='1'>".$li->check("MeetingID[]")."</th>\n";
	$toolbar = $linterface->GET_LNK_NEW("javascript:checkGet(document.form1,'new.php')",$button_new,"","","",0) . "&nbsp;";
	$toolbar .= $junior_mck ? $linterface->GET_LNK_IMPORT("javascript:do_import()",$button_import) : $linterface->GET_LNK_IMPORT("javascript:do_import()",$button_import,"","","",0);
	$toolbar .= "&nbsp;";
	$toolbar .= $junior_mck ? $linterface->GET_LNK_EXPORT("#","","do_export();") : $linterface->GET_LNK_EXPORT("javascript:do_export()",$button_export,"","","",0);
	$manage_record_bar  = '<a href="javascript:checkEdit(document.form1,\'MeetingID[]\',\'edit.php\')" class="tool_edit">'.$button_edit.'</a>';
	$manage_record_bar .= '<a href="javascript:checkRemove(document.form1,\'MeetingID[]\',\'remove.php\')" class="tool_delete">'.$button_delete.'</a>';
}		
else {
	$extra_column = 1;
	$checkbox_col = "";
	$toolbar = "";
	$manage_record_bar  = '';
}
$sql .= "FROM 
				INTRANET_GUIDANCE_CLASSTEACHER_MEETING c
		INNER JOIN INTRANET_GUIDANCE_CLASSTEACHER_MEETING_COMMENT cmc ON cmc.MeetingID=c.MeetingID
		INNER JOIN
				INTRANET_USER u ON u.UserID=cmc.StudentID ".$joinClass ."
		WHERE 1 ".$cond."
		GROUP BY c.MeetingID";
//debug_pr($sql);
$li->field_array = array("c.MeetingDate", "$classNameField");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+$extra_column;
$li->title = $Lang['General']['Record'];
$li->IsColOff = "IP25_table";

$pos = 0;
$li->column_list .= "<th width='1' class='tabletoplink'>#</th>\n";
$li->column_list .= "<th width='20%' >".$li->column($pos++, $Lang['eGuidance']['ctmeeting']['MeetingDate'])."</th>\n";
$li->column_list .= "<th width='75%' >".$li->column($pos++, $Lang['eGuidance']['Class'])."</th>\n";
$pos++;
$li->column_list .= $checkbox_col;


$htmlAry['searchBox'] = $linterface->Get_Search_Box_Div('keyword', $keyword);

############# start Filters

# academic year filter
if ($junior_mck){
	$libguidance_ui = new libguidance_ui();
	$academicYearFilter = $libguidance_ui->getSelectAcademicYear($YearName, $tag='name="YearName" id="YearName" onChange="submit_form();"');
	
	# Class Filter
	if ($YearName == $currentYearName) {
		$lclass = new libclass();
		$classFilter = $lclass->getSelectClass("name='ClassName' id='ClassName' onChange='this.form.submit();'",$ClassName,"",$Lang['eGuidance']['AllClass'],$AcademicYearID);
	}
	else {
		$classFilter = $libguidance_ui->getSelectHistoryClass($YearName, $tag='name="ClassName" id="ClassName" onChange="this.form.submit();"', $ClassName, $all=0, $noFirst=0, $Lang['eGuidance']['AllClass']);
	}	
}
else {
	$academicYearFilter = getSelectAcademicYear($objName='AcademicYearID', $tag=' onChange="submit_form();"', $noFirst=1, $noPastYear=0, $AcademicYearID, $displayAll=0, $pastAndCurrentYearOnly=0, $OrderBySequence=1, $excludeCurrentYear=0, $excludeYearIDArr=array());
	
	# Class Filter
	$lclass = new libclass();
	$classFilter = $lclass->getSelectClass("name='ClassName' onChange='this.form.submit();'",$ClassName,"",$Lang['eGuidance']['AllClass'],$AcademicYearID);
}

############# end Filters

?>
<script language="javascript">
function submit_form() {
	$('#ClassName').val('');
	$('#form1').submit();
}

function do_import() {
	var original_action = document.form1.action;
	document.form1.action = "import.php";
	$('#form1').submit();
	document.form1.action = original_action;
}

function do_export() {
	var original_action = document.form1.action;
	document.form1.action = "export.php";
	$('#form1').submit();
	document.form1.action = original_action;
}
</script>

<form name="form1" id="form1" method="POST" action="index.php">
	<div class="content_top_tool">
		
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td width="40%"><?=$toolbar ?></td>
				<td width="30%" align="center">&nbsp;</td>
				<td width="30%">
					<div class="content_top_tool"  style="float: right;">
						<?=$htmlAry['searchBox']?>     
						<br style="clear:both" />
					</div>
				</td>
			</tr>
		</table>
		
	</div>

	<div class="table_board">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr class="table-action-bar">
			<td valign="bottom">
				<div class="table_filter">
					<?=$academicYearFilter?>
					<?=$classFilter?>
				</div> 
			</td>
			<td valign="bottom">
				<div class="common_table_tool">
					<?=$manage_record_bar?>					
				</div>
			</td>
		</tr>
	</table>
	</div>

	<div class="table_board">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td valign="bottom">

				<?=$li->display()?>
 
			</td>
		</tr>
	</table>
	</div>

	<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
	<input type="hidden" name="order" value="<?php echo $li->order; ?>" />
	<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
	<input type="hidden" name="page_size_change" value="" />
	<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />

</form>

<?php
    

$linterface->LAYOUT_STOP();
intranet_closedb();
?>