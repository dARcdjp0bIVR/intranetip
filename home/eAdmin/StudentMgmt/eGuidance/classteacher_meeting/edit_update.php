<?php
/*
 * 	modifying:
 * 
 * 	Log
 *
 *	2017-11-13 [Cameron]
 *		- retrieve ClassName to support ej ( YearName not need to update as not given it to change) 
 *
 * 	2017-10-25 [Cameron]
 * 		- add Followup field
 *
 * 	2017-09-11 [Cameron]
 * 		- create this file
 * 
 */

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/eGuidance_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/libguidance.php");

intranet_auth();
intranet_opendb();

$libguidance = new libguidance();
$permission = $libguidance->getUserPermission();
if (!$permission['admin'] && !in_array('MGMT', (array)$permission['current_right']['MGMT']['CLASSTEACHERMEETING'])) {
	$libguidance->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$ldb = new libdb();
$result = array();

$dataAry['MeetingDate'] = $MeetingDate;
if ($junior_mck) {
	$dataAry['ClassName'] = $ClassName;
}
else {
	$dataAry['YearClassID'] = $YearClassID;	
}
$dataAry['LastModifiedBy'] = $_SESSION['UserID'];
$sql = $libguidance->UPDATE2TABLE('INTRANET_GUIDANCE_CLASSTEACHER_MEETING',$dataAry,array('MeetingID'=>$MeetingID),false);

$ldb->Start_Trans();
## step 1: update to classteacher meeting
$result[] = $ldb->db_db_query($sql);

## step 2: update class teacher comments
foreach((array)$Comment as $k=>$v) {
	$v = trim($v);
	if (!empty($v)) {
		unset($dataAry);
		$dataAry['MeetingID'] = $MeetingID;
		$dataAry['StudentID'] = $k;
		$dataAry['Comment'] = standardizeFormPostValue($v);
		$dataAry['Followup'] = standardizeFormPostValue(trim($Followup[$k]));
		
		if ($libguidance->isClassTeacherCommentExist($MeetingID,$k)) {
			$sql = $libguidance->UPDATE2TABLE('INTRANET_GUIDANCE_CLASSTEACHER_MEETING_COMMENT',$dataAry,array('MeetingID'=>$MeetingID, 'StudentID'=>$k),false,false);	// no DateModified field
		}
		else {
			$sql = $libguidance->INSERT2TABLE('INTRANET_GUIDANCE_CLASSTEACHER_MEETING_COMMENT',$dataAry,array(),false,false,false);	// no DateModified field
		}
		$result[] = $ldb->db_db_query($sql);
	}
	else {
		if ($libguidance->isClassTeacherCommentExist($MeetingID,$k)) {
			$sql = "DELETE FROM	INTRANET_GUIDANCE_CLASSTEACHER_MEETING_COMMENT WHERE MeetingID='".$MeetingID."' AND StudentID='".$k."'";
			$result[] = $ldb->db_db_query($sql);
		}
	}
}

## step 3: add attachment				
if (count($FileID) > 0) {
	$fileID = implode("','",$FileID);
	$sql = "UPDATE INTRANET_GUIDANCE_ATTACHMENT SET RecordID='".$MeetingID."' WHERE FileID IN ('".$fileID."')";
	$result[] = $ldb->db_db_query($sql);
}



if (!in_array(false,$result)) {
	$ldb->Commit_Trans();
	$returnMsgKey = 'UpdateSuccess';
}
else {
	$ldb->RollBack_Trans();
	$returnMsgKey = 'UpdateUnsuccess';
}
	
header("location: index.php?returnMsgKey=".$returnMsgKey);


intranet_closedb();

?>


