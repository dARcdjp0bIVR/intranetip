<?php
/*
 * 	Log
 * 	
 * 	Purpose: import class teacher meeting comments
 *	!!! Note: 
 *			1. Column Title for comments must be valid date format
 *			2. There's follow-up column next to each comment column
 *			3. Ignore record which comment filed is empty (no matter follow-up field is empty or not)
 *			4. download sample will auto append _unicode as sample file name for IP, thus physical file must be end with _unicode 
 *
 *	Date:	2017-11-21 [Cameron]
 *			fix download sample for ip
 *
 * 	Date:	2017-11-15 [Cameron]
 * 			create this file
 * 
 */
 
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/eGuidance_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/guidance_conf.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/libguidance.php");
 
intranet_auth();
intranet_opendb();

$libguidance = new libguidance();
$permission = $libguidance->getUserPermission();
if (!$permission['admin'] && !$permission['current_right']['MGMT']['CLASSTEACHERMEETING']) {
	$libguidance->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}
    
# Page heading setting
$linterface = new interface_html();
$CurrentPageArr['eGuidance'] = 1;
$CurrentPage = "PageClassteacherMeeting";
$CurrentPageName = $Lang['eGuidance']['name'];

### Title ###
$TAGS_OBJ = array();
$TAGS_OBJ[] = array($Lang['eGuidance']['menu']['Management']['ClassteacherMeeting']);

$MODULE_OBJ = $libguidance->GET_MODULE_OBJ_ARR();
$returnMsgKey = standardizeFormGetValue($_GET['returnMsgKey']);
if ($returnMsgKey == "DateNotExist") {
	$returnMsg = $Lang['eGuidance']['ctmeeting']['ImportReturnMsg'][$returnMsgKey].": ".standardizeFormGetValue($_GET['MeetingDate']);	
}
else {
	$returnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];	
}
$linterface->LAYOUT_START($returnMsg);


# page navigation (leave the array empty if no need)
$PAGE_NAVIGATION[] = array();
$PAGE_NAVIGATION[] = array($Lang['eGuidance']['menu']['Management']['ClassteacherMeeting'], "index.php");
$PAGE_NAVIGATION[] = array($Lang['Btn']['Import'], "");
$htmlAry['navigation'] = $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION);

### step display
$htmlAry['generalImportStepTbl'] = $linterface->GET_IMPORT_STEPS($CurrStep=1,$Lang['eGuidance']['Import']['Steps']);

# import column explanation and remarks
$csv_format = "";
$delim = "<br>";
for($i=0, $iMax=count($Lang['eGuidance']['ctmeeting']['ImportColumns']); $i<$iMax; $i++){
	if($i!=0) $csv_format .= $delim;
	$csv_format .= $Lang['General']['ImportColumn']." ".numberToLetter($i+1, true)." : ".$Lang['eGuidance']['ctmeeting']['ImportColumns'][$i];
}
$csv_remarks = "";
for($i=0, $iMax=count($Lang['eGuidance']['ctmeeting']['ImportRemarks']); $i<$iMax; $i++){
	if($i!=0) $csv_remarks .= $delim;
	$csv_remarks .= $Lang['eGuidance']['ctmeeting']['ImportRemarks'][$i];
}

# import file interface
$x = '';
$x .= '<table class="form_table_v30">'."\r\n";
	$x .= '<tr>'."\r\n";
		$x .= '<td class="field_title">'.$i_select_file.'</td>'."\r\n";
		$x .= '<td>'."\r\n";
			$x .= '<input class="file" type="file" name="userfile">'."\r\n";
			$x .= '<br><br>'."\r\n";
			$x .= '<a class="tablelink" href="'.GET_CSV('class_teacher_meeting_sample.csv').'" target="_self">'."\r\n";
				$x .= '<img src="'.$PATH_WRT_ROOT.'/images/'.$LAYOUT_SKIN.'/icon_files/xls.gif" border="0" align="absmiddle" hspace="3">'."\r\n";
				$x .= $i_general_clickheredownloadsample."\r\n";
			$x .= '</a>'."\r\n";
		$x .= '</td>'."\r\n";
	$x .= '</tr>'."\r\n";
	
	$x .= '<tr>'."\r\n";			
		$x .= '<td class="field_title">'.$Lang['General']['ImportArr']['DataColumn'].'</td>'."\r\n";
		$x .= '<td>'.$csv_format."</td>\r\n";
	$x .= '</tr>'."\r\n";
	
	$x .= '<tr>'."\r\n";		
		$x .= '<td colspan=2>'.$csv_remarks."</td>\r\n";
	$x .= '</tr>'."\r\n";		
	
$x .= '</table>'."\r\n";
$htmlAry['importInfoTbl'] = $x;


### action buttons
$htmlAry['submitBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Submit'], "button", "goSubmit()", 'submitBtn', $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");
$htmlAry['cancelBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Cancel'], "button", "goCancel()", 'cancelBtn', $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");

?>
<script type="text/JavaScript" language="JavaScript">
function trim(str) {
	return str.replace(/^\s+|\s+$/g,"");
}

function goCancel() {
	window.location = 'index.php';
}

function goSubmit() {
	var canSubmit = true;
	
	obj = document.form1;
	if (trim(obj.elements["userfile"].value) == "") {
		canSubmit = false;
		alert('<?=$Lang['eGuidance']['PleaseSelectFiles']?>');
		obj.elements["userfile"].focus();
	}
	
	if (canSubmit) {
		obj.submit();
	}
}
</script>
<form id="form1" name="form1" action="import_validate.php" method="POST" enctype="multipart/form-data">
<?=$htmlAry['navigation']?>
	<p class="spacer"></p>
	<br />
	
	<div style="text-align:left">
		<?=$htmlAry['generalImportStepTbl']?>
	</div>
	
	<div class="table_board">
		<?=$htmlAry['importInfoTbl']?>
		<?=$htmlAry['errorTbl']?>
		<br style="clear:both;" />
		
		<div class="edit_bottom_v30">
			<p class="spacer"></p>
			<?=$htmlAry['submitBtn']?>
			<?=$htmlAry['cancelBtn']?>
			<p class="spacer"></p>
		</div>
	</div>
</form>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>