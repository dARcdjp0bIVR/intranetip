<?php
/*
 * 	Log
 * 	
 * 	Purpose: save import class teacher meeting comments
 * 
 * 	Date:	2017-11-16 [Cameron]
 * 			create this file
 * 
 */

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/eGuidance_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/guidance_conf.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/libguidance.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/libguidance_ui.php");
 
intranet_auth();
intranet_opendb();

$libguidance = new libguidance();
$permission = $libguidance->getUserPermission();
if (!$permission['admin'] && !$permission['current_right']['MGMT']['CLASSTEACHERMEETING']) {
	$libguidance->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

# Page heading setting
$linterface = new interface_html();
$CurrentPageArr['eGuidance'] = 1;
$CurrentPage = "PageClassteacherMeeting";
$CurrentPageName = $Lang['eGuidance']['name'];

### Title ###
$TAGS_OBJ = array();
$TAGS_OBJ[] = array($Lang['eGuidance']['menu']['Management']['ClassteacherMeeting']);

# handle return message
$MODULE_OBJ = $libguidance->GET_MODULE_OBJ_ARR();
$returnMsgKey = standardizeFormGetValue($_GET['returnMsgKey']);
if ($returnMsgKey == "DateNotExist") {
	$returnMsg = $Lang['eGuidance']['ctmeeting']['ImportReturnMsg'][$returnMsgKey].": ".standardizeFormGetValue($_GET['MeetingDate']);	
}
else {
	$returnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];	
}
$linterface->LAYOUT_START($returnMsg);

# page navigation (leave the array empty if no need)
$PAGE_NAVIGATION[] = array();
$PAGE_NAVIGATION[] = array($Lang['eGuidance']['menu']['Management']['ClassteacherMeeting'], "index.php");
$PAGE_NAVIGATION[] = array($Lang['Btn']['Import'], "");
$htmlAry['navigation'] = $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION);


### step display
$htmlAry['generalImportStepTbl'] = $linterface->GET_IMPORT_STEPS($CurrStep=3,$Lang['eGuidance']['Import']['Steps']);


$nrRecord = $libguidance->doClassTeacherImport();


# result display
$htmlAry['numOFSuccessDisplay'] = $nrRecord.' '.$Lang['eGuidance']['Import']['Successful'];


### action buttons
$htmlAry['backBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Back'], "button", "goBack()", 'backBtn', $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");

?>
<script type="text/JavaScript" language="JavaScript">
function goBack() {
	window.location = 'import.php';
}
</script>
<form id="form1" name="form1" method="POST">
	<?=$htmlAry['navigation']?>
	<p class="spacer"></p>
	<br />
	
	<div style="text-align:left">
		<?=$htmlAry['generalImportStepTbl']?>
	</div>
	
	<div class="table_board">
		<br style="clear:both;" />
		
		<div style="width:100%; text-align:center;">
			<?=$htmlAry['numOFSuccessDisplay']?>
		</div>
		<br style="clear:both;" />
		
		<div class="edit_bottom_v30">
			<p class="spacer"></p>
			<?=$htmlAry['backBtn']?>
			<p class="spacer"></p>
		</div>
	</div>
</form>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>