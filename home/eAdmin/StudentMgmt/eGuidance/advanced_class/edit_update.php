<?php
/*
 * 	modifying:
 * 
 * 	Log
 * 
 * 	2017-05-18 [Cameron]
 * 		- apply standardizeFormPostValue() to text field
 * 
 * 	2017-02-22 [Cameron]
 * 		- create this file
 * 
 */

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/eGuidance_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/libguidance.php");

intranet_auth();
intranet_opendb();

$libguidance = new libguidance();
$permission = $libguidance->getUserPermission();
if (!$permission['admin'] && !in_array('MGMT', (array)$permission['current_right']['MGMT']['ADVANCEDCLASS'])) {
	$libguidance->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$ldb = new libdb();
$result = array();

$dataAry['StartDate'] = $StartDate;
$dataAry['Remark'] = standardizeFormPostValue($Remark);
$dataAry['LastModifiedBy'] = $_SESSION['UserID'];
$sql = $libguidance->UPDATE2TABLE('INTRANET_GUIDANCE_ADVANCED_CLASS',$dataAry,array('ClassID'=>$ClassID),false);

$ldb->Start_Trans();

## step 1: update advanced class
$result[] = $ldb->db_db_query($sql);


## step 2: get advanced class student that's not in updated student list
$rs_student = $libguidance->getAdvancedClassStudentID($ClassID,$StudentID);	// not in list student
if (count($rs_student)) {
	
	## step 3: delete advanced class student that's not in updated list
	$sql = "DELETE FROM INTRANET_GUIDANCE_ADVANCED_CLASS_STUDENT WHERE ClassID='".$ClassID."' AND StudentID IN ('".implode("','",(array)$rs_student)."')";
	$result[] = $ldb->db_db_query($sql);
	
	## step 4: delete advanced class student lesson result that's not in updated student list
	$sql = "SELECT 
					r.LessonID, r.StudentID 
			FROM 
					INTRANET_GUIDANCE_ADVANCED_CLASS_LESSON_RESULT r
			INNER JOIN 
					INTRANET_GUIDANCE_ADVANCED_CLASS_LESSON n ON n.LessonID=r.LessonID
			WHERE 	
					n.ClassID='".$ClassID."'
			AND 	StudentID IN ('".implode("','",(array)$rs_student)."')";
			
	$rs_lesson_result = $ldb->returnResultSet($sql);
	if (count($rs_lesson_result)) {
		foreach((array)$rs_lesson_result as $r) {
			$sql = "DELETE FROM INTRANET_GUIDANCE_ADVANCED_CLASS_LESSON_RESULT WHERE LessonID='".$r['LessonID']."' AND StudentID='".$r['StudentID']."'";
			$result[] = $ldb->db_db_query($sql);
		}
	}
}

$currentStudentList = $libguidance->getAdvancedClassStudent($ClassID);
$currentStudentList = BuildMultiKeyAssoc($currentStudentList, 'UserID',array('UserID'),1);

$lessonAry = $libguidance->getAdvancedClassLesson($ClassID);

## step 5: update advanced class student list
foreach((array)$StudentID as $id) {
	if (!in_array($id,$currentStudentList)) {	// add new class student record
		unset($dataAry);
		$dataAry['ClassID'] = $ClassID;
		$dataAry['StudentID'] = $id;
		$sql = $libguidance->INSERT2TABLE('INTRANET_GUIDANCE_ADVANCED_CLASS_STUDENT',$dataAry,array(),false,true,false);	// insert ignore
		$result[] = $ldb->db_db_query($sql);

		## step 6:
		## case when add new student to advanced class, but there's already lesson created
 		## add empty result for the student		
		foreach((array)$lessonAry as $ls) {
			unset($dataAry);
			$dataAry['LessonID'] = $ls['LessonID'];
			$dataAry['StudentID'] = $id;
			$dataAry['Result'] = '';
			$sql = $libguidance->INSERT2TABLE('INTRANET_GUIDANCE_ADVANCED_CLASS_LESSON_RESULT',$dataAry,array(),false,false,false);
			$result[] = $ldb->db_db_query($sql);
		}
	}
}


## step 7: get advanced class teacher that's not in updated teacher list
$rs_teacher = $libguidance->getAdvancedClassTeacherID($ClassID,$TeacherID);
if (count($rs_teacher)) {
	## step 8: delete advanced class teacher that's not in updated teacher list
	$sql = "DELETE FROM INTRANET_GUIDANCE_ADVANCED_CLASS_TEACHER WHERE ClassID='".$ClassID."' AND TeacherID IN ('".implode("','",$rs_teacher)."')";
	$result[] = $ldb->db_db_query($sql);
}

## step 9: update advanced class teacher
foreach((array)$TeacherID as $id) {
	unset($dataAry);
	$dataAry['ClassID'] = $ClassID;
	$dataAry['TeacherID'] = $id;
	$sql = $libguidance->INSERT2TABLE('INTRANET_GUIDANCE_ADVANCED_CLASS_TEACHER',$dataAry,array(),false,true,false);	// insert ignore
	$result[] = $ldb->db_db_query($sql);
}

## step 10: update attachment
if (count($FileID) > 0) {
	$fileID = implode("','",$FileID);
	$sql = "UPDATE INTRANET_GUIDANCE_ATTACHMENT SET RecordID='".$ClassID."' WHERE FileID IN ('".$fileID."') AND RecordID=0";
	$result[] = $ldb->db_db_query($sql);
}

if (!in_array(false,$result)) {
	$ldb->Commit_Trans();
	$returnMsgKey = 'UpdateSuccess';
}
else {
	$ldb->RollBack_Trans();
	$returnMsgKey = 'UpdateUnsuccess';
}

header("location: index.php?returnMsgKey=".$returnMsgKey);


intranet_closedb();

?>


