<?php
/*
 * 	modifying: 
 * 
 * 	Log
 *
 * 	2017-11-06 [Cameron]
 * 		- adjust "Add Lesson" button to left position (next to plus sign)
 *  
 * 	2017-10-09 [Cameron]
 * 		- fix: restrict "Add Lesson" button permission
 *   
 * 	2017-06-29 [Cameron]
 * 		- move LessonDate to Add / Edit Lesson interface (thickbox), it can be changed
 * 		- remove LessonDate parameter in addRow 
 * 		- add 'Add Lesson' button
 * 
 * 	2017-02-14 [Cameron]
 * 		- create this file
 * 
 */

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/eGuidance_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/guidance_conf.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/libguidance.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/libguidance_ui.php");

intranet_auth();
intranet_opendb();

$libguidance = new libguidance();
$permission = $libguidance->getUserPermission();
if (!$permission['admin'] && !$permission['current_right']['MGMT']['ADVANCEDCLASS']) {
	$libguidance->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

if (!$ClassID) {
	header("location: index.php");
}

$libguidance_ui = new libguidance_ui();
$linterface = new interface_html();
				

## get record details

$rs_class = $libguidance->getAdvancedClass($ClassID);
$rs_class = $rs_class[0];

$orderBy = "ClassName,ClassNumber";
$student = $libguidance->getAdvancedClassStudent($ClassID,$orderBy);
$student_list = $libguidance_ui->getUserNameList($student);
$student = BuildMultiKeyAssoc($student, 'UserID');

$teacher = $libguidance->getAdvancedClassTeacher($ClassID);
$teacher_list = $libguidance_ui->getUserNameList($teacher);

$h_LessonNavigation = $linterface->GET_NAVIGATION2($Lang['eGuidance']['advanced_class']['LessonList']);

### Lesson Action Button
$BtnArr = array();
$BtnArr[] = array('delete', 'javascript:deleteLesson();');
$h_actionBtn = $linterface->Get_DBTable_Action_Button_IP25($BtnArr);


if ($permission['admin'] || in_array('MGMT', (array)$permission['current_right']['MGMT']['ADVANCEDCLASS'])) {
	$layoutMode = 'edit';
}
else { 
	$layoutMode = 'view';
}

$today = date('Y-m-d');


# menu highlight setting
$CurrentPageArr['eGuidance'] = 1;
$CurrentPage = "PageAdvancedClass";
$CurrentPageName = $Lang['eGuidance']['name'];

### Title ###
$TAGS_OBJ = array();
$TAGS_OBJ[] = array($Lang['eGuidance']['menu']['Management']['AdvancedClass']);

$MODULE_OBJ = $libguidance->GET_MODULE_OBJ_ARR();
$PAGE_NAVIGATION[] = array($Lang['eGuidance']['menu']['Management']['AdvancedClass'], "index.php");
$PAGE_NAVIGATION[] = array($Lang['eGuidance']['advanced_class']['LessonManagement'], "");

$linterface->LAYOUT_START();


echo $linterface->Include_Thickbox_JS_CSS();
?>

<script type="text/javascript">

function addRow() {
	tb_show('<?=$Lang['eGuidance']['advanced_class']['AddLesson']?>','../ajax/ajax_layout.php?action=NewLesson&ClassID='+$('#ClassID').val()+'&height=500&width=800');
}

function edit_lesson(lessonID) {
	tb_show('<?=$Lang['eGuidance']['advanced_class']['EditLesson']?>','../ajax/ajax_layout.php?action=EditLesson&LessonID='+lessonID+'&height=500&width=800');
}

function deleteLesson() {
	if ($('input.LessonChk:checked').length == 0) {
		alert(globalAlertMsg2);
	}
	else if (confirm(globalAlertMsg3)){
		$.ajax({
			dataType: "json",
			type: "POST",
			url: 'ajax_update.php?action=delete_advanced_class_lesson',
			data : $('#form1').serialize(),		  
			success: update_lesson_list,
			error: show_ajax_error
		});
	}
}

function update_lesson_list(ajaxReturn) {
	if (ajaxReturn != null) {
		if (ajaxReturn.action_result != null) {
			Get_Return_Message(ajaxReturn.action_result);
		}
		if (ajaxReturn.success) {
			$('#LessonDiv').html(ajaxReturn.html);
		}
	}
}	

function show_ajax_error() {
	alert('<?=$Lang['eGuidance']['error']['ajax']?>');
}

function Show_Edit_Background(Obj) {
	Obj.style.backgroundImage = "url(<?=$PATH_WRT_ROOT?>images/<?=$LAYOUT_SKIN?>/icon_edit_b.gif)";
	Obj.style.backgroundPosition = "center right";
	Obj.style.backgroundRepeat = "no-repeat";
}

function Hide_Edit_Background(Obj) {
	Obj.style.backgroundImage = "";
	Obj.style.backgroundPosition = "";
	Obj.style.backgroundRepeat = "";
}

</script>
<form name="form1" id="form1" method="post">

<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td class="navigation"><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
				</tr>
			</table>
			<br style="clear:both;" />
			<table width="100%" border="0" cellpadding="5" cellspacing="0">
				<tr>
					<td>
						<table align="center" class="form_table_v30">
							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['General']['StartDate']?>
									</td>
								<td class="tabletext"><?=$rs_class['StartDate']?></td>
							</tr>
							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['Identity']['Student']?>
									</td>
								<td class="tabletext"><?=$student_list?></td>
							</tr>
							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['eGuidance']['TeacherName']?>
									</td>
								<td class="tabletext"><?=$teacher_list?></td>
							</tr>
							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['eGuidance']['Remark']?>
									</td>
								<td class="tabletext" colspan="2"><?=$rs_class['Remark'] ? nl2br($rs_class['Remark']) : '-'?></td>
							</tr>
						
							<?=$libguidance_ui->getAttachmentLayout('AdvancedClass',$ClassID,'view','view_current_attachment')?>
							
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>

<br style="clear:both;" />
	
	<div style="float:left"><?=$h_LessonNavigation?></div>
	<br style="clear:both;" />
	<?=($layoutMode == 'edit' ? $h_actionBtn : '')?>
	<div id="LessonDiv"><?=$libguidance_ui->getAdvancedClassLessonTable($ClassID,$student,$layoutMode)?></div>
	<br style="clear:both;" />

<? if ($permission['admin'] || in_array('MGMT', (array)$permission['current_right']['MGMT']['ADVANCEDCLASS'])):?>
	<div><label>
		<span class="table_row_tool"><a class="newBtn add" onclick="javascript:addRow()" title="<?=$Lang['eGuidance']['advanced_class']['AddLesson']?>"></a></span>
		<span><a href="#" style="float: left" onclick="javascript:addRow()"><?=$Lang['eGuidance']['advanced_class']['AddLesson']?></a></span></label>
	</div>
<? endif;?>	
	<input type="hidden" id="ClassID" name="ClassID" value="<?=$ClassID?>" />
</form>
<?php

$linterface->LAYOUT_STOP();
intranet_closedb();

?>


