<?php
/*
 * 	modifying:
 * 
 * 	Log
 * 
 * 	2017-02-22 [Cameron]
 * 		- create this file
 * 
 */

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/eGuidance_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/libguidance.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

intranet_auth();
intranet_opendb();

$libguidance = new libguidance();
$permission = $libguidance->getUserPermission();
if (!$permission['admin'] && !in_array('MGMT', (array)$permission['current_right']['MGMT']['ADVANCEDCLASS'])) {
	$libguidance->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$ldb = new libdb();
$lfs = new libfilesystem();
$result = array();
$lessonExist = false;

if (count($ClassID) > 0) {
	$rs_lesson = $libguidance->getAdvancedClassLesson($ClassID);
	$lessonIDAry = array();
	
	$ldb->Start_Trans();
	// delete advanced class
	$sql = "DELETE FROM INTRANET_GUIDANCE_ADVANCED_CLASS WHERE ClassID IN ('".implode("','",$ClassID)."')";
	$result[] = $ldb->db_db_query($sql);

	$sql = "DELETE FROM INTRANET_GUIDANCE_ADVANCED_CLASS_STUDENT WHERE ClassID IN ('".implode("','",$ClassID)."')";
	$result[] = $ldb->db_db_query($sql);

	$sql = "DELETE FROM INTRANET_GUIDANCE_ADVANCED_CLASS_TEACHER WHERE ClassID IN ('".implode("','",$ClassID)."')";
	$result[] = $ldb->db_db_query($sql);
	
	if (count($rs_lesson)) {
		foreach((array)$rs_lesson as $r) {
			$lessonID = $r['LessonID'];
			$lessonIDAry[] = $lessonID; 
			$sql = "DELETE FROM INTRANET_GUIDANCE_ADVANCED_CLASS_LESSON_RESULT WHERE LessonID='".$lessonID."'";
			$result[] = $ldb->db_db_query($sql);

			$sql = "DELETE FROM INTRANET_GUIDANCE_ADVANCED_CLASS_LESSON WHERE LessonID='".$lessonID."'";
			$result[] = $ldb->db_db_query($sql);
		}
	}
	
	## remove attachment in advanced class
	$sql = "SELECT 
				FileID, EncodeFileName 
			FROM 
				INTRANET_GUIDANCE_ATTACHMENT
			WHERE 
				Target='AdvancedClass'
			AND	RecordID IN ('".implode("','",(array)$ClassID)."')";
	$rs = $ldb->returnResultSet($sql);
		
	if (count($rs) > 0) {
		foreach((array)$rs as $r) {
			$FileID = $r['FileID'];
			$EncodeFileName = $r['EncodeFileName'];
			$file = $libguidance->eg_path.$EncodeFileName;
			$result[] = $lfs->file_remove($file);
		}

		$sql = "DELETE FROM INTRANET_GUIDANCE_ATTACHMENT WHERE Target='AdvancedClass' AND RecordID IN ('".implode("','",(array)$ClassID)."')";
		$result[] = $ldb->db_db_query($sql);
	}


	## remove attachment in advanced class lesson
	if (count($lessonIDAry)) {
		$sql = "SELECT 
					FileID, EncodeFileName 
				FROM 
					INTRANET_GUIDANCE_ATTACHMENT
				WHERE 
					Target='AdvancedClassLesson'
				AND	RecordID IN ('".implode("','",(array)$lessonIDAry)."')";
		$rs = $ldb->returnResultSet($sql);
			
		if (count($rs) > 0) {
			foreach((array)$rs as $r) {
				$FileID = $r['FileID'];
				$EncodeFileName = $r['EncodeFileName'];
				$file = $libguidance->eg_path.$EncodeFileName;
				$result[] = $lfs->file_remove($file);
			}
	
			$sql = "DELETE FROM INTRANET_GUIDANCE_ATTACHMENT WHERE Target='AdvancedClassLesson' AND RecordID IN ('".implode("','",(array)$lessonIDAry)."')";
			$result[] = $ldb->db_db_query($sql);
		}
	}	
}

if (!in_array(false,$result)) {
	$ldb->Commit_Trans();
	$returnMsgKey = 'DeleteSuccess';
}
else {
	$ldb->RollBack_Trans();
	$returnMsgKey = 'DeleteUnsuccess';
}

header("location: index.php?returnMsgKey=".$returnMsgKey);

intranet_closedb();

?>


