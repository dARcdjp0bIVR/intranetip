<?
/*
 * 	Log
 * 
 * 	Description: handle db action, then output json format data
 * 
 * 	2017-06-29 [Cameron]
 * 		allow to update LessonDate when update update_advanced_class_lesson
 * 
 * 	2017-05-18 [Cameron]
 * 		fix bug: 
 * 		1.	should apply standardizeFormPostValue to trim and stripslashes of $studentResult first,
 * 			then apply Get_Safe_Sql_Query to the field in sql before insert / update to db table, this will eliminate
 * 			the big5 character (HuiGongKoi problem)
 * 		2.  must specify charset as utf-8 for ej, hence must convert big5 to utf-8 for related fields  
 * 
 * 	2017-05-17 [Cameron] fix php5.4 json must pass data as utf-8
 *   
 * 	2017-03-09 [Cameron] create this file
 */

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/eGuidance_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/libguidance.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/libguidance_ui.php");
include_once($PATH_WRT_ROOT."includes/json.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

intranet_auth();
intranet_opendb();

$libguidance = new libguidance();
$permission = $libguidance->getUserPermission();
if (!$permission['admin'] && !in_array('MGMT', (array)$permission['current_right']['MGMT']['ADVANCEDCLASS'])) {	
	$libguidance->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

if ($junior_mck) {
	$characterset = 'utf-8';		// ej must specify charset as utf-8, ip not need to specify as it's based on utf-8, 
}
header('Content-Type: text/html; charset='.$characterset);

$ljson = new JSON_obj();

$json['success'] = false;
$x = '';
$action = $_GET['action'] ? $_GET['action'] : $_POST['action'];
$remove_dummy_chars = false;	// whether to remove new line, carriage return, tab and back slash

$lclass = new libclass();
$libguidance_ui = new libguidance_ui();
$ldb = new libdb();
$lfs = new libfilesystem();
$result = array();
$dataAry = array();
$condAry = array();

switch($action) {

	case 'add_advanced_class_lesson':
	
		$ldb->Start_Trans();
		
		## add lesson
		$dataAry['ClassID'] = $ClassID;		
		$dataAry['LessonDate'] = $LessonDate;
		$dataAry['LastModifiedBy'] = $_SESSION['UserID'];
		$sql = $libguidance->INSERT2TABLE('INTRANET_GUIDANCE_ADVANCED_CLASS_LESSON',$dataAry,array(),false);

		$res = $ldb->db_db_query($sql);
		$result[] = $res;
		
		if ($res) {
			$lessonID = $ldb->db_insert_id();
			foreach((array)$LessonInfoAry as $studentID=>$studentResult) {
				## add lesson result
				unset($dataAry);
				$studentResult = standardizeFormPostValue($studentResult);
				$dataAry['LessonID'] = $lessonID;
				$dataAry['StudentID'] = $studentID;
				$dataAry['Result'] = ($junior_mck) ? convert2unicode($studentResult, 1, $direction=0) : $studentResult;		// need to convert from utf-8 back to b5 for ej
				$sql = $libguidance->INSERT2TABLE('INTRANET_GUIDANCE_ADVANCED_CLASS_LESSON_RESULT',$dataAry,array(),false,false,false);
				$result[] = $ldb->db_db_query($sql);
			}
			
			## update attachment				
			if (count($FileID) > 0) {
				$fileID = implode("','",(array)$FileID);
				$sql = "UPDATE INTRANET_GUIDANCE_ATTACHMENT SET RecordID='".$lessonID."' WHERE FileID IN ('".$fileID."')";
				$result[] = $ldb->db_db_query($sql);
			}
			
		}
						
		if (!in_array(false,$result)) {
			$ldb->Commit_Trans();
			$orderBy = "ClassName,ClassNumber";
			$student = $libguidance->getAdvancedClassStudent($ClassID,$orderBy);
			$student_list = $libguidance_ui->getUserNameList($student);
			$student = BuildMultiKeyAssoc($student, 'UserID');
			$x = $libguidance_ui->getAdvancedClassLessonTable($ClassID,$student);
			$json['success'] = true;
			$json['action_result'] = ($junior_mck) ? convert2unicode($Lang['General']['ReturnMessage']['AddSuccess'], 1, $direction=1) : $Lang['General']['ReturnMessage']['AddSuccess'];
		}
		else {
			$ldb->RollBack_Trans();
			$json['success'] = false;
			$json['action_result'] = ($junior_mck) ? convert2unicode($Lang['General']['ReturnMessage']['AddUnsuccess'], 1, $direction=1) : $Lang['General']['ReturnMessage']['AddUnsuccess'];
		}
		
		break;


	case 'update_advanced_class_lesson':

		$ldb->Start_Trans();
		
		foreach((array)$LessonInfoAry as $studentID=>$studentResult) {
			unset($dataAry);
			unset($condAry);
			## update lesson result
			$studentResult = standardizeFormPostValue($studentResult);
			$dataAry['Result'] = ($junior_mck) ? convert2unicode($studentResult, 1, $direction=0) : $studentResult;
			$condAry['LessonID'] = $LessonID;
			$condAry['StudentID'] = $studentID;
			$sql = $libguidance->UPDATE2TABLE('INTRANET_GUIDANCE_ADVANCED_CLASS_LESSON_RESULT',$dataAry,$condAry,false,false);
			$result[] = $ldb->db_db_query($sql);
		}
		
		## update modify time and modify by person
		unset($dataAry);
		unset($condAry);
		$dataAry['LessonDate'] = $LessonDate;
		$dataAry['LastModifiedBy'] = $_SESSION['UserID'];
		$condAry['LessonID'] = $LessonID;
		$sql = $libguidance->UPDATE2TABLE('INTRANET_GUIDANCE_ADVANCED_CLASS_LESSON',$dataAry,$condAry,false);
		$result[] = $ldb->db_db_query($sql);
		
		## update attachment
		if (count($FileID) > 0) {
			$fileID = implode("','",(array)$FileID);
			$sql = "UPDATE INTRANET_GUIDANCE_ATTACHMENT SET RecordID='".$LessonID."' WHERE FileID IN ('".$fileID."') AND RecordID=0";
			$result[] = $ldb->db_db_query($sql);
		}
		
		if (!in_array(false,$result)) {
			$ldb->Commit_Trans();
			$orderBy = "ClassName,ClassNumber";
			$student = $libguidance->getAdvancedClassStudent($ClassID,$orderBy);
			$student_list = $libguidance_ui->getUserNameList($student);
			$student = BuildMultiKeyAssoc($student, 'UserID');
			$x = $libguidance_ui->getAdvancedClassLessonTable($ClassID,$student);
			$json['success'] = true;
			$json['action_result'] = ($junior_mck) ? convert2unicode($Lang['General']['ReturnMessage']['UpdateSuccess'], 1, $direction=1) : $Lang['General']['ReturnMessage']['UpdateSuccess'];
		}
		else {
			$ldb->RollBack_Trans();
			$json['success'] = false;
			$json['action_result'] = ($junior_mck) ? convert2unicode($Lang['General']['ReturnMessage']['UpdateUnsuccess'], 1, $direction=1) : $Lang['General']['ReturnMessage']['UpdateUnsuccess'];
		}
	
		break;


	case 'delete_advanced_class_lesson':

		if (count($LessonChk)) {

			$ldb->Start_Trans();
						
			$sql = "DELETE FROM INTRANET_GUIDANCE_ADVANCED_CLASS_LESSON WHERE LessonID IN ('".implode("','",(array)$LessonChk)."')";
			$result[] = $ldb->db_db_query($sql);
			
			$sql = "DELETE FROM INTRANET_GUIDANCE_ADVANCED_CLASS_LESSON_RESULT WHERE LessonID IN ('".implode("','",(array)$LessonChk)."')";
			$result[] = $ldb->db_db_query($sql);

			## remove attachment in advanced class lesson
			$sql = "SELECT 
						FileID, EncodeFileName 
					FROM 
						INTRANET_GUIDANCE_ATTACHMENT
					WHERE 
						Target='AdvancedClassLesson'
					AND	RecordID IN ('".implode("','",(array)$LessonChk)."')";
			$rs = $ldb->returnResultSet($sql);
				
			if (count($rs) > 0) {
				foreach((array)$rs as $r) {
					$FileID = $r['FileID'];
					$EncodeFileName = $r['EncodeFileName'];
					$file = $libguidance->eg_path.$EncodeFileName;
					$result[] = $lfs->file_remove($file);
				}
		
				$sql = "DELETE FROM INTRANET_GUIDANCE_ATTACHMENT WHERE Target='AdvancedClassLesson' AND RecordID IN ('".implode("','",(array)$LessonChk)."')";
				$result[] = $ldb->db_db_query($sql);
			}

			
			if (!in_array(false,$result)) {
				$ldb->Commit_Trans();
				$orderBy = "ClassName,ClassNumber";
				$student = $libguidance->getAdvancedClassStudent($ClassID,$orderBy);
				$student_list = $libguidance_ui->getUserNameList($student);
				$student = BuildMultiKeyAssoc($student, 'UserID');
				$x = $libguidance_ui->getAdvancedClassLessonTable($ClassID,$student);
				$json['success'] = true;
				$json['action_result'] = ($junior_mck) ? convert2unicode($Lang['General']['ReturnMessage']['DeleteSuccess'], 1, $direction=1) : $Lang['General']['ReturnMessage']['DeleteSuccess'];
			}
			else {
				$ldb->RollBack_Trans();
				$json['success'] = false;
				$json['action_result'] = ($junior_mck) ? convert2unicode($Lang['General']['ReturnMessage']['DeleteUnsuccess'], 1, $direction=1) : $Lang['General']['ReturnMessage']['DeleteUnsuccess'];
			}
			
		}
		else {
			$json['success'] = false;
			$json['action_result'] = ($junior_mck) ? convert2unicode($Lang['General']['ReturnMessage']['DeleteUnsuccess'], 1, $direction=1) : $Lang['General']['ReturnMessage']['DeleteUnsuccess'];
		}	
		break;
}

if ($remove_dummy_chars) {
	$x = remove_dummy_chars_for_json($x);
}

if ($junior_mck) {
	$x = convert2unicode($x,true,1);		// json_encode must pass data as utf-8
}

$json['html'] = $x;
echo $ljson->encode($json);

intranet_closedb();
?>