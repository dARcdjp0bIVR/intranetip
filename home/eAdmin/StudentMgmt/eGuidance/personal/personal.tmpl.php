<?
/*
 * 	Log
 *
 *  2018-05-25 [Cameron]
 *      - show student info and disable student selection for edit [case #M138226]
 *      
 *	2017-08-07 [Cameron]
 *		- create this file
 */

 
$teachingStaffAry = $libguidance->getTeacher($teacherType='1');
$teachingStaffAry = build_assoc_array($teachingStaffAry);
$nonTeachingStaffAry = $libguidance->getTeacher($teacherType='0');
$nonTeachingStaffAry = build_assoc_array($nonTeachingStaffAry);
$teacherTypeAry = array (	$Lang['Identity']['TeachingStaff'] => $teachingStaffAry,
							$Lang['Identity']['NonTeachingStaff'] => $nonTeachingStaffAry
						);
 
?>
<style>
.error_msg_hide{display:none;}
.error_msg_show{display:'block';color: #FF0000; font-weight: bold;}
</style>
<?=$linterface->Include_Thickbox_JS_CSS();?>
<script type="text/javascript">

<?include($PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eGuidance/common_js.php");?>

function checkForm(obj) 
{
	var error = 0;
	var focusField = '';
	hideError();
	
	if ($('#StudentID').val() == '') {
 		error++;
 		if (focusField == '') focusField = 'StudentID';
 		$('#ErrStudentID').addClass('error_msg_show').removeClass('error_msg_hide');
	}
	
	$(".integer").each(function(){

		if (($.trim($(this).val()) != '') && ((isNaN($(this).val())) || ($(this).val() < 0) || ($(this).val() > 100) || ($(this).val().indexOf('.') != -1))) {
	
			error++;
			if (focusField == '') $(this).focus();
			
			id = $(this).attr('id');
			
			$('#Err'+id).addClass('error_msg_show').removeClass('error_msg_hide');

		}
	});

	$(".number").each(function(){

		if (($.trim($(this).val()) != '') && ((isNaN($(this).val())) || ($(this).val() < 0) || ($(this).val() > 100))) {
	
			error++;
			if (focusField == '') $(this).focus();
			
			id = $(this).attr('id');
			
			$('#Err'+id).addClass('error_msg_show').removeClass('error_msg_hide');

		}
	});
	
	if (error > 0) {
		if (focusField != '') {
			$('#' + focusField).focus();
		}
		return false
	}
	else {		
		return true;
	}
	
}

$(document).ready(function(){
	
	var isLoading = true;
	var loadingImg = '<?=$linterface->Get_Ajax_Loading_Image() ?>';
	
	$('#ClassName').change(function(){
        isLoading = true;
		$('#StudentNameSpan').html(loadingImg);
		
		$.ajax({
			dataType: "json",
			type: "POST",
			url: '../ajax/ajax.php',
			data : {
				'action': '<?=($form_action=='new_update.php') ? 'getStudentNameByClassForNewPersonal' : 'getStudentNameByClassForEditPersonal'?>',
				'ClassName': $('#ClassName').val()
			},		  
			success: update_student_list,
			error: show_ajax_error
		});
		
	});

	$(':input[name="ReferralFromType"]').change(function() {

		var from = $(this).attr('id');
		hideReferralFrom();
		
		$('#'+from+'_Span').css("display","");
		
	});
	
	$('#RefAcaReason_AcaOther').click(function(){
		if ($('#RefAcaReason_AcaOther').attr('checked')) {
			$('#AcaOther').css("display","");
		}
		else {
			$('#AcaOther').css("display","none");
		}
	});

	$('#RefDisReason_DisOther').click(function(){
		if ($('#RefDisReason_DisOther').attr('checked')) {
			$('#DisOther').css("display","");
		}
		else {
			$('#DisOther').css("display","none");
		}
	});

	$(':input[name="RadioPlaceOfBirth"]').change(function() {

		var id = $(this).attr('id');
		if (id == 'InOther') {
			$('#PlaceOfBirth').css("display","");	
		}
		else {
			$('#PlaceOfBirth').css("display","none");
		}
	});

	$("input:radio[name='ParentStatus']").change(function(){
		if ($('#ParentStatus_PSOther').attr('checked')) {
			$('#ParentStatusOther').css("display","");
		}
		else {
			$('#ParentStatusOther').css("display","none");
		}
	});
	
	$(':input[name^="Nbr"]').keyup(function() {
		var total = 0;
		$(':input[name^="Nbr"]').each(function() {
			if ($(this).val() && !isNaN($(this).val())) {
				total += parseInt($(this).val());
			}
		});
		if (total > 0) {
			$('#TotalNbrBrotherSister').val(total);
		}
		else {
			$('#TotalNbrBrotherSister').val('');
		}
	});

	$('#LiveWith_BrotherAndSister').click(function(){
		if ($('#LiveWith_BrotherAndSister').attr('checked')) {
			$('#LwBrotherAndSister').css("display","");
		}
		else {
			$('#LwBrotherAndSister').css("display","none");
		}
	});

	$('#LiveWith_Relative').click(function(){
		if ($('#LiveWith_Relative').attr('checked')) {
			$('#LwRelative').css("display","");
		}
		else {
			$('#LwRelative').css("display","none");
		}
	});

	$(':input[name="FatherWorkingStatus"]').change(function() {
		if ($('#FatherWorkingStatus_FullTime').attr('checked')) {
			$('#FatherOccupation').css("display","");
			$('#FatherOccupationLabel').css("display","");
		}
		else {
			$('#FatherOccupation').css("display","none");
			$('#FatherOccupationLabel').css("display","none");
		}
	});

	$(':input[name="MotherWorkingStatus"]').change(function() {
		if ($('#MotherWorkingStatus_FullTime').attr('checked')) {
			$('#MotherOccupation').css("display","");
			$('#MotherOccupationLabel').css("display","");
		}
		else {
			$('#MotherOccupation').css("display","none");
			$('#MotherOccupationLabel').css("display","none");
		}
	});

	$('#Prob_Health_Illness').click(function(){
		if ($('#Prob_Health_Illness').attr('checked')) {
			$('#Illness').css("display","");
		}
		else {
			$('#Illness').css("display","none");
		}
	});

	$('#Prob_Behavior_CriminalOffense').click(function(){
		if ($('#Prob_Behavior_CriminalOffense').attr('checked')) {
			$('#CriminalOffense').css("display","");
		}
		else {
			$('#CriminalOffense').css("display","none");
		}
	});

	$('#Prob_Other_OtherProblem').click(function(){
		if ($('#Prob_Other_OtherProblem').attr('checked')) {
			$('#OtherProblem').css("display","");
		}
		else {
			$('#OtherProblem').css("display","none");
		}
	});
	
	$(':input[name="IsReferralToSocialWorker"]').change(function() {
		var id = $(this).attr('id');
		if (id == 'IsReferralToSocialWorkerYes') {
			$('#RefToReasonLabel').css("display","");
			$('#RefToReasonSpan').css("display","");
		}
		else {
			$('#RefToReasonLabel').css("display","none");
			$('#RefToReasonSpan').css("display","none");
			$(':input[name="ReferralToReason\[\]"]').each(function() {
				$(this).attr('checked', false);
			});
			$('#RefToOther').val('');	
		}
	});
	
	$('#RefToReason_RefToOther').click(function(){
		if ($('#RefToReason_RefToOther').attr('checked')) {
			$('#RefToOther').css("display","");
		}
		else {
			$('#RefToOther').css("display","none");
		}
	});
	
	$('#btnSubmit').click(function(e) {
	    e.preventDefault();
	    $('input.actionBtn').attr('disabled', 'disabled');
		if ($('#IsConfidentialYes').is(':checked')) {
			$('#ConfidentialViewerID option').attr('selected',true);
		}
		
		if (checkForm(document.form1)) {
	        $('#form1').submit();
		}
		else {
			$('input.actionBtn').attr('disabled', '');
		}
	});

});


function notify_colleague_update(recordID) {
	if ($('#IsConfidentialYes').is(':checked')) {
		$('#ConfidentialViewerID option').attr('selected',true);
	}
	if (checkForm(document.form1)) {
		tb_show('<?=$Lang['eGuidance']['NotifyUpdate']?>','../ajax/ajax_layout.php?action=NotifyColleagueUpdate&RecordID='+$('#'+recordID).val()+'&RecordType='+recordID+'&height=450&width=650');
	}
}

function show_history_push_message() {
	tb_show('<?=$Lang['eGuidance']['PushMessage']['ViewStatus']?>','../ajax/ajax_layout.php?action=GetHistoryNotification&RecordID='+$('#OrgStudentID').val()+'&RecordType=Personal&height=450&width=650');		
}

function update_student_list(ajaxReturn) {
	if (ajaxReturn != null && ajaxReturn.success){
		$('#StudentNameSpan').html(ajaxReturn.html);
		isLoading = false;
	}
}	

function hideReferralFrom() {
	$(':input[name="ReferralFromType"]').each(function(){
		var from = $(this).attr('id');
		$('#'+from+'_Span').css("display","none");
	});
}

</script>

<form name="form1" id="form1" method="post" action="<?=$form_action?>">
<table id="main_table" width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td class="navigation"><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="5" cellspacing="0">
				<tr>
					<td>
						<table align="center" class="form_table_v30">
							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['Identity']['Student']?>
									<span class="tabletextrequire"><?=$disabled?'':'*'?></span></td>
								<td class="tabletext">
								<?php if ($form_action == 'edit_update.php' || $showStudentNameOnly):?>
									<?php echo $studentInfo;?>
								<?php else:?>
									<span><?=$Lang['eGuidance']['Class']?></span>&nbsp;<?=$classSelection?> &nbsp;
									<span><?=$Lang['eGuidance']['StudentName']?></span>&nbsp;<span id="StudentNameSpan"><?=$studentSelection?></span>
								<?php endif;?>	
									<span class="error_msg_hide" id="ErrStudentID"><?=$Lang['eGuidance']['Warning']['SelectStudent']?></span>
								</td>
							</tr>

						<? if ($disabled):?>
							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['eGuidance']['personal']['Gender']?></td>
								<td class="tabletext">
									<input type="radio" name="Gender" id="GenderM" value="M" disabled <?=$rs_personal['Gender']=='M'?' checked':''?>><label for="GenderM"><?=$Lang['eGuidance']['personal']['GenderMale']?></label>
									<input type="radio" name="Gender" id="GenderF" value="F" disabled <?=$rs_personal['Gender']=='F'?' checked':''?>><label for="GenderF"><?=$Lang['eGuidance']['personal']['GenderFemale']?></label>
								</td>
							</tr>
						<? endif;?>
							
							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['eGuidance']['personal']['ContactTel']?></td>
								<td class="tabletext">
									<table class="no_bottom_border">
										<tr>
											<td width="20%"><?=$Lang['eGuidance']['personal']['Father']?></td>
											<td><input type="text" name="FatherTelNo" id="FatherTelNo" class="textboxtext" style="width:150px;" value="<?=intranet_htmlspecialchars($rs_personal['FatherTelNo'])?>" <?=$disabled?>></td>
										</tr>
										<tr>
											<td width="20%"><?=$Lang['eGuidance']['personal']['Mother']?></td>
											<td><input type="text" name="MotherTelNo" id="MotherTelNo" class="textboxtext" style="width:150px;" value="<?=intranet_htmlspecialchars($rs_personal['MotherTelNo'])?>" <?=$disabled?>></td>
										</tr>
										<tr>
											<td width="20%"><?=$Lang['eGuidance']['personal']['Student']?></td>
											<td><input type="text" name="StudentTelNo" id="StudentTelNo" class="textboxtext" style="width:150px;" value="<?=intranet_htmlspecialchars($rs_personal['StudentTelNo'])?>" <?=$disabled?>></td>
										</tr>
									</table>
								</td>
							</tr>
							
							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['eGuidance']['personal']['GuidanceTeacher']?></td>
								<td class="tabletext">
									<?=getSelectByAssoArray($teacherTypeAry,"name='GuidanceTeacher' id='GuidanceTeacher' $disabled", $rs_personal['GuidanceTeacher'])?>
								</td>
							</tr>
							
							<tr valign="top">
								<td colspan="2" valign="top" nowrap="nowrap"><?=$Lang['eGuidance']['personal']['BasicInfo']?></td>
							</tr>

							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title">1. <?=$Lang['eGuidance']['personal']['ReferralFromAndReason']?></td>
								<td class="tabletext">
								<?	$referral_from = $libguidance->getGuidanceSettingItem('Personal','ReferralFrom');
									$x = '<table class="no_bottom_border" width="100%">';										
									foreach((array)$referral_from as $k=>$v) {
										$x .= '<tr>';
											$x .= '<td width="22%">';
												$x .= '<input type="radio" name="ReferralFromType" id="ReferralFrom_'.$k.'" value="'.$k.'"'. (($rs_personal['ReferralFromType'] == $k)?'checked':'').' '.$disabled.'>';
												$x .= '<label for="ReferralFrom_'.$k.'">'.$v.'</label>';
											$x .= '</td>';
											$x .= '<td>';
											
											$display = ($rs_personal['ReferralFromType']== $k)?'':'none';
												
											switch ($k) {
												case 'Teacher':
													$x .= '<span id="ReferralFrom_'.$k.'_Span" style="display:'.$display.'">';
													$x .= getSelectByAssoArray($teacherTypeAry,"name='FromTeacher' id='FromTeacher' $disabled", $rs_personal['ReferralFrom']);
													$x .= '&nbsp;<input type="text" name="FromTeacherReason" id="FromTeacherReason" '.$disabled.'  value="'.intranet_htmlspecialchars($refFromReasonAry[$k]).'" style="width:60%;">';
													$x .= '</span>';
													break;
													
												case 'AcademicGroup':
													$refaca_reason = $libguidance->getGuidanceSettingItem('Personal','RefAcaReason');
													$i = 0;
													$x .= '<span id="ReferralFrom_'.$k.'_Span" style="display:'.$display.'">';
													foreach((array)$refaca_reason as $kk=>$vv) {
														$x .= ($i > 0) ? '<br>' : '';
														$x .= '<input type="checkbox" name="RefAcaReason[]" id="RefAcaReason_'.$kk.'" value="'.$kk.'"'. (in_array($kk,(array)$refFromReasonAry)?'checked':'').' '.$disabled.'>';
														$x .= '<label for="RefAcaReason_'.$kk.'">'.$vv.'</label>';
														if ($kk == 'AcaOther') {
															$x .= '&nbsp;<input type="text" name="AcaOther" id="AcaOther" '.$disabled.'  value="'.intranet_htmlspecialchars($refFromReasonAry[$kk]).'" style="width:60%; display:'.(in_array($kk,(array)$refFromReasonAry)?'':'none').'">';
														}
														$i++;
													}
													$x .= '</span>';
													break;
													
												case 'DisciplineGroup':
													$refdis_reason = $libguidance->getGuidanceSettingItem('Personal','RefDisReason');
													$i = 0;
													$x .= '<span id="ReferralFrom_'.$k.'_Span" style="display:'.$display.'">';
													foreach((array)$refdis_reason as $kk=>$vv) {
														$x .= ($i > 0) ? '<br>' : '';
														$x .= '<input type="checkbox" name="RefDisReason[]" id="RefDisReason_'.$kk.'" value="'.$kk.'"'. (in_array($kk,(array)$refFromReasonAry)?'checked':'').' '.$disabled.'>';
														$x .= '<label for="RefDisReason_'.$kk.'">'.$vv.'</label>';
														if ($kk == 'DisOther') {
															$x .= '&nbsp;<input type="text" name="DisOther" id="DisOther" '.$disabled.'  value="'.intranet_htmlspecialchars($refFromReasonAry[$kk]).'" style="width:60%; display:'.(in_array($kk,(array)$refFromReasonAry)?'':'none').'">';
														}
														$i++;
													}
													$x .= '</span>';
													break;
												
												case 'FromOther':
													$x .= '<span id="ReferralFrom_'.$k.'_Span" style="display:'.$display.'">';
														$x .= '&nbsp;<input type="text" name="FromOther" id="FromOther" '.$disabled.'  value="'.intranet_htmlspecialchars($refFromReasonAry[$k]).'" style="width:60%;">';
													$x .= '</span>';											
													break;
											}
											$x .= '</td>';
										$x .= '</tr>';
									}
									$x .= '</table>';
									echo $x;
								?>
								</td>
							</tr>

							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title">2. <?=$Lang['eGuidance']['personal']['PlaceOfBirth']?></td>
								<td class="tabletext">
									<input type="radio" name="RadioPlaceOfBirth" id="InHongKong" value="Hong Kong" <?=($rs_personal['PlaceOfBirth'] == 'Hong Kong')?'checked':''?> <?=$disabled?>>
									<label for="InHongKong"><?=$Lang['eGuidance']['personal']['BornInHongKong']?></label>
									<input type="radio" name="RadioPlaceOfBirth" id="InOther" value="Other" <?=($rs_personal['PlaceOfBirth'] && $rs_personal['PlaceOfBirth'] != 'Hong Kong')?'checked':''?> <?=$disabled?>>
									<label for="InOther"><?=$Lang['eGuidance']['personal']['BornInOther']?></label>
									<input type="text" name="PlaceOfBirth" id="PlaceOfBirth" <?=$disabled?> value="<?=intranet_htmlspecialchars(($rs_personal['PlaceOfBirth'] && $rs_personal['PlaceOfBirth'] != 'Hong Kong')?$rs_personal['PlaceOfBirth']:'')?>" style="width:200px; display:<?=(!isset($rs_personal['PlaceOfBirth']) || $rs_personal['PlaceOfBirth'] == 'Hong Kong') ? 'none':''?>">
									<div>&nbsp;<?=$Lang['eGuidance']['personal']['YearInHongKong']?>
										<input type="text" class="number" name="YearInHongKong" id="YearInHongKong" <?=$disabled?> value="<?=$rs_personal['YearInHongKong'] ? $rs_personal['YearInHongKong'] : ''?>" style="width:40px;" maxlength="5">
										<span class="error_msg_hide" id="ErrYearInHongKong"><?=$Lang['eGuidance']['Warning']['InputNumber']?></span>
									</div>
								</td>
							</tr>

							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title">3. <?=$Lang['eGuidance']['personal']['FamilyStatus']?></td>
								<td class="tabletext">
									<table class="no_bottom_border">
										<tr>
											<td width="20%"><?=$Lang['eGuidance']['personal']['ParentStatus']?></td>
											<td>
									<? 	$parent_status = $libguidance->getGuidanceSettingItem('Personal','ParentStatus');
										$i = 0;
										$x = '';
										foreach((array)$parent_status as $k=>$v) {
											$x .= ($i > 0) ? '<br>' : '';
											if ($k == 'PSOther') {
												if (!empty($rs_personal['ParentStatus']) && substr($rs_personal['ParentStatus'],0,7)==$k) {
													$selParentStatusOther = true;
													$strAry = explode("^:",$rs_personal['ParentStatus']);
													if (count($strAry) > 1) {
														$parentStatusOther = $strAry[1];
													}
													else {
														$parentStatusOther = '';
													}
												}
												else {
													$selParentStatusOther = false;
													$parentStatusOther = '';
												}
												$x .= '<input type="radio" name="ParentStatus" id="ParentStatus_'.$k.'" value="'.$k.'"'. ($selParentStatusOther?' checked':'').' '.$disabled.'>';
												$x .= '<label for="ParentStatus_'.$k.'">'.$v.'</label> ';
												$x .= '<input type="text" name="ParentStatusOther" id="ParentStatusOther" value="'.intranet_htmlspecialchars($parentStatusOther).'" style="width:60%; display:'.($selParentStatusOther?'':'none').'" '.$disabled.'>';
											}
											else {
												$x .= '<input type="radio" name="ParentStatus" id="ParentStatus_'.$k.'" value="'.$k.'"'. ($rs_personal['ParentStatus']==$k?'checked':'').' '.$disabled.'>';
												$x .= '<label for="ParentStatus_'.$k.'">'.$v.'</label>';
											}										  
											$i++;
										}
										echo $x;
									?>
											</td>
										</tr>
										
										<tr>
											<td width="20%"><?=$Lang['eGuidance']['personal']['BrotherAndSister']?></td>
											<td>
												<input type="text" style="width: 40px;" maxlength="1" class="integer" name="NbrElderBrother" id="NbrElderBrother" <?=$disabled?> value="<?=$rs_personal['NbrElderBrother'] ? $rs_personal['NbrElderBrother'] : ''?>">
												<span class="error_msg_hide" id="ErrNbrElderBrother"><?=$Lang['eGuidance']['Warning']['InputInteger']?></span>
												<?=$Lang['eGuidance']['personal']['ElderBrother']?><br>
												<input type="text" style="width: 40px;" maxlength="1" class="integer" name="NbrElderSister" id="NbrElderSister" <?=$disabled?> value="<?=$rs_personal['NbrElderSister'] ? $rs_personal['NbrElderSister'] : ''?>">
												<span class="error_msg_hide" id="ErrNbrElderSister"><?=$Lang['eGuidance']['Warning']['InputInteger']?></span>
												<?=$Lang['eGuidance']['personal']['ElderSister']?><br>
												<input type="text" style="width: 40px;" maxlength="1" class="integer" name="NbrYoungerBrother" id="NbrYoungerBrother" <?=$disabled?> value="<?=$rs_personal['NbrYoungerBrother'] ? $rs_personal['NbrYoungerBrother'] : ''?>">
												<span class="error_msg_hide" id="ErrNbrYoungerBrother"><?=$Lang['eGuidance']['Warning']['InputInteger']?></span>
												<?=$Lang['eGuidance']['personal']['YoungerBrother']?><br>
												<input type="text" style="width: 40px;" maxlength="1" class="integer" name="NbrYoungerSister" id="NbrYoungerSister" <?=$disabled?> value="<?=$rs_personal['NbrYoungerSister'] ? $rs_personal['NbrYoungerSister'] : ''?>">
												<span class="error_msg_hide" id="ErrNbrYoungerSister"><?=$Lang['eGuidance']['Warning']['InputInteger']?></span>
												<?=$Lang['eGuidance']['personal']['YoungerSister']?>
											</td>
										</tr>										
										<tr>
											<td width="20%"><?=$Lang['eGuidance']['personal']['Total']?></td>
											<td>	
												<input type="text" style="width: 40px;" name="TotalNbrBrotherSister" id="TotalNbrBrotherSister" disabled value="<?=$rs_personal['TotalNbrBrotherSister'] ? $rs_personal['TotalNbrBrotherSister'] : ''?>">
												<?=$Lang['eGuidance']['personal']['People']?>
											</td>
										</tr>										
									</table>
								</td>
							</tr>
						
							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title">4. <?=$Lang['eGuidance']['personal']['PersonLivedWithStudent']?></td>
								<td class="tabletext">
								<?
									$live_with = $libguidance->getGuidanceSettingItem('Personal','LiveWith');
									$i = 0;
									$x = '';
									foreach((array)$live_with as $k=>$v) {
										$x .= ($i > 0) ? '<br>' : '';
										$x .= '<input type="checkbox" name="LiveWith[]" id="LiveWith_'.$k.'" value="'.$k.'"'. (in_array($k,(array)$liveWithAry)?'checked':'').' '.$disabled.'>';
										$x .= '<label for="LiveWith_'.$k.'">'.$v.'</label> ';
										if ($k == 'BrotherAndSister') {
											$x .= '<table class="no_bottom_border" id="LwBrotherAndSister" style="display:'.(in_array($k,(array)$liveWithAry)?'':'none').'"><tr><td width="20%"></td><td>';
											$x .= '<input type="text" style="width: 40px;" maxlength="1" class="integer" name="LwNbrElderBrother" id="LwNbrElderBrother" '.$disabled.' value="'.($rs_personal['LwNbrElderBrother'] ? $rs_personal['LwNbrElderBrother'] : '').'"><span class="error_msg_hide" id="ErrLwNbrElderBrother">'.$Lang['eGuidance']['Warning']['InputInteger'].'</span> '.$Lang['eGuidance']['personal']['ElderBrother'].'<br>';
											$x .= ' <input type="text" style="width: 40px;" maxlength="1" class="integer" name="LwNbrElderSister" id="LwNbrElderSister" '.$disabled.' value="'.($rs_personal['LwNbrElderSister'] ? $rs_personal['LwNbrElderSister'] : '').'"><span class="error_msg_hide" id="ErrLwNbrElderSister">'.$Lang['eGuidance']['Warning']['InputInteger'].'</span> '.$Lang['eGuidance']['personal']['ElderSister'].'<br>';
											$x .= ' <input type="text" style="width: 40px;" maxlength="1" class="integer" name="LwNbrYoungerBrother" id="LwNbrYoungerBrother" '.$disabled.' value="'.($rs_personal['LwNbrYoungerBrother'] ? $rs_personal['LwNbrYoungerBrother'] : '').'"><span class="error_msg_hide" id="ErrLwNbrYoungerBrother">'.$Lang['eGuidance']['Warning']['InputInteger'].'</span> '.$Lang['eGuidance']['personal']['YoungerBrother'].'<br>';
											$x .= ' <input type="text" style="width: 40px;" maxlength="1" class="integer" name="LwNbrYoungerSister" id="LwNbrYoungerSister" '.$disabled.' value="'.($rs_personal['LwNbrYoungerSister'] ? $rs_personal['LwNbrYoungerSister'] : '').'"><span class="error_msg_hide" id="ErrLwNbrYoungerSister">'.$Lang['eGuidance']['Warning']['InputInteger'].'</span> '.$Lang['eGuidance']['personal']['YoungerSister'];
											$x .= '</td></tr></table>';
										}
										if ($k == 'Relative') {
											$x .= '&nbsp;<input type="text" name="LwRelative" id="LwRelative" '.$disabled.'  value="'.intranet_htmlspecialchars($liveWithAry[$k]).'" style="width:60%; display:'.(($liveWithAry[$k])?'':'none').'">';
										}
										$i++;
									}
									echo $x;
								?>
								</td>
							</tr>

							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title">5. <?=$Lang['eGuidance']['personal']['FatherWorkingStatus']?></td>
								<td class="tabletext">
								<? 	$working_status = $libguidance->getGuidanceSettingItem('Personal','WorkingStatus');
									$i = 0;
									$x = '';
									foreach((array)$working_status as $k=>$v) {
										$x .= ($i > 0) ? '<br>' : '';
										if ($k == 'FullTime') {
											if (!empty($rs_personal['FatherWorkingStatus']) && substr($rs_personal['FatherWorkingStatus'],0,8)==$k) {
												$selFatherFullTime = true;
												$strAry = explode("^:",$rs_personal['FatherWorkingStatus']);
												if (count($strAry) > 1) {
													$fatherOccupation = $strAry[1];
												}
												else {
													$fatherOccupation = '';
												}
											}
											else {
												$selFatherFullTime = false;
												$fatherOccupation = '';
											}
												
											$x .= '<input type="radio" name="FatherWorkingStatus" id="FatherWorkingStatus_'.$k.'" value="'.$k.'"'. ($selFatherFullTime?'checked':'').' '.$disabled.'>';
											$x .= '<label for="FatherWorkingStatus_'.$k.'">'.$v.'</label> ';
											$x .= '<input type="text" name="FatherOccupation" id="FatherOccupation" value="'.intranet_htmlspecialchars($fatherOccupation).'" style="width:60%; display:'.($selFatherFullTime?'':'none').'" '.$disabled.'>';
											$x .= '<span id="FatherOccupationLabel" style="display:'.($selFatherFullTime?'':'none').'">('.$Lang['eGuidance']['personal']['Occupatiion'].')</span>';
										}
										else {
											$x .= '<input type="radio" name="FatherWorkingStatus" id="FatherWorkingStatus_'.$k.'" value="'.$k.'"'. ($rs_personal['FatherWorkingStatus']==$k?'checked':'').' '.$disabled.'>';
											$x .= '<label for="FatherWorkingStatus_'.$k.'">'.$v.'</label>';
										}										  
										$i++;
									}
									echo $x;
								?>
								</td>
							</tr>

							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title">6. <?=$Lang['eGuidance']['personal']['MotherWorkingStatus']?></td>
								<td class="tabletext">
								<? 	$working_status = $libguidance->getGuidanceSettingItem('Personal','WorkingStatus');
									$i = 0;
									$x = '';
									foreach((array)$working_status as $k=>$v) {
										$x .= ($i > 0) ? '<br>' : '';
										if ($k == 'FullTime') {
											if (!empty($rs_personal['MotherWorkingStatus']) && substr($rs_personal['MotherWorkingStatus'],0,8)==$k) {
												$selMotherFullTime = true;
												$strAry = explode("^:",$rs_personal['MotherWorkingStatus']);
												if (count($strAry) > 1) {
													$motherOccupation = $strAry[1];
												}
												else {
													$motherOccupation = '';
												}
											}
											else {
												$selMotherFullTime = false;
												$motherOccupation = '';
											}
											
											$x .= '<input type="radio" name="MotherWorkingStatus" id="MotherWorkingStatus_'.$k.'" value="'.$k.'"'. ($selMotherFullTime?'checked':'').' '.$disabled.'>';
											$x .= '<label for="MotherWorkingStatus_'.$k.'">'.$v.'</label> ';
											$x .= '<input type="text" name="MotherOccupation" id="MotherOccupation" value="'.intranet_htmlspecialchars($motherOccupation).'" style="width:60%; display:'.($selMotherFullTime?'':'none').'" '.$disabled.'>';
											$x .= '<span id="MotherOccupationLabel" style="display:'.($selMotherFullTime?'':'none').'">('.$Lang['eGuidance']['personal']['Occupatiion'].')</span>';
										}
										else {
											$x .= '<input type="radio" name="MotherWorkingStatus" id="MotherWorkingStatus_'.$k.'" value="'.$k.'"'. ($rs_personal['MotherWorkingStatus']==$k?'checked':'').' '.$disabled.'>';
											$x .= '<label for="MotherWorkingStatus_'.$k.'">'.$v.'</label>';
										}										  
										$i++;
									}
									echo $x;
								?>
								</td>
							</tr>


							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title">7. <?=$Lang['eGuidance']['personal']['MainProblem']?></td>
								<td class="tabletext">
								<? 	$category = $libguidance->getGuidanceSettingItem('Personal','Category');
									$i = 1;
									$x = '';
									foreach((array)$category as $k=>$v) {
										$j = 0;
										$letter = numberToLetter($i,true);	// upper case
										$x .= '<table width="100%" class="no_bottom_border">';
											$x .= '<tr><td colspan="2" style="font-weight:bold;">'.$letter.'. '.$v.'</td></tr>';
											$x .= '<tr>';
												$x .= '<td width="20px;">';
												$x .= '<td>';
												$sub_item = $libguidance->getGuidanceSettingItem('Personal',$k);
												foreach((array)$sub_item as $kk=>$vv) {
													$x .= ($j > 0) ? '<br>' : '';
													$x .= '<input type="checkbox" name="Prob_'.$k.'[]" id="Prob_'.$k.'_'.$kk.'" value="'.$kk.'"'. (in_array($kk,(array)${"prob".$k."Ary"})?'checked':'').' '.$disabled.'>';
													$x .= '<label for="Prob_'.$k.'_'.$kk.'">'.$vv.'</label>';
													if ($kk == 'Illness') {
														$x .= '&nbsp;<input type="text" name="Illness" id="Illness" '.$disabled.'  value="'.intranet_htmlspecialchars($probHealthAry[$kk]).'" style="width:80%; display:'.(($probHealthAry[$kk])?'':'none').'" '.$disabled.'>';
													}
													else if ($kk == 'CriminalOffense') {
														$x .= '&nbsp;<input type="text" name="CriminalOffense" id="CriminalOffense" '.$disabled.'  value="'.intranet_htmlspecialchars($probBehaviorAry[$kk]).'" style="width:80%; display:'.(($probBehaviorAry[$kk])?'':'none').'" '.$disabled.'>';
													}
													else if ($kk == 'OtherProblem') {
														$x .= '&nbsp;<input type="text" name="OtherProblem" id="OtherProblem" '.$disabled.'  value="'.intranet_htmlspecialchars($probOtherAry[$kk]).'" style="width:80%; display:'.(($probOtherAry[$kk])?'':'none').'" '.$disabled.'>';
													}
													$j++;
												}
												$x .= '</td>';
											$x .= '</tr>';
										$x .= '</table>';						
										$i++;
									}
									echo $x;
								?>
								</td>
							</tr>
						
							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title">8. <?=$Lang['eGuidance']['personal']['FollowupStatus']?></td>
								<td class="tabletext">
									<table class="no_bottom_border" width="100%">
										<tr>
											<td width="20%"><?=$Lang['eGuidance']['personal']['ReferredToSocialWorker']?></td>
											<td>
												<input type="radio" name="IsReferralToSocialWorker" id="IsReferralToSocialWorkerYes" value="1"<?=$rs_personal['IsReferralToSocialWorker']?' checked':''?> <?=$disabled?>><label for="IsReferralToSocialWorkerYes"><?=$Lang['General']['Yes']?></label>
												<input type="radio" name="IsReferralToSocialWorker" id="IsReferralToSocialWorkerNo" value="0"<?=$rs_personal['IsReferralToSocialWorker']?'':' checked'?> <?=$disabled?>><label for="IsReferralToSocialWorkerNo"><?=$Lang['General']['No']?></label>
											</td>
										</tr>
										<tr>
											<td width="20%" id="RefToReasonLabel" style="display:<?=$rs_personal['IsReferralToSocialWorker'] ? '':'none'?>"><?=$Lang['eGuidance']['personal']['ReferredToReason']?></td>
											<td>
											<?
												$refto_reason = $libguidance->getGuidanceSettingItem('Personal','RefToReason');
												$i = 0;
												$x = '<span id="RefToReasonSpan" style="display:'.($rs_personal['IsReferralToSocialWorker'] ? '':'none').'">';
												foreach((array)$refto_reason as $k=>$v) {
													$x .= ($i > 0) ? '<br>' : '';
													$x .= '<input type="checkbox" name="ReferralToReason[]" id="RefToReason_'.$k.'" value="'.$k.'"'. (in_array($k,(array)$referralToReasonAry)?'checked':'').' '.$disabled.'>';
													$x .= '<label for="RefToReason_'.$k.'">'.$v.'</label>';
													if ($k == 'RefToOther') {
														$x .= '&nbsp;<input type="text" name="RefToOther" id="RefToOther" '.$disabled.'  value="'.intranet_htmlspecialchars($referralToReasonAry[$k]).'" style="width:80%; display:'.(in_array($k,(array)$referralToReasonAry)?'':'none').'" '.$disabled.'>';
													}
													$i++;
												}
												$x .= '</span>';
												echo $x;
											?>
											</td>
										</tr>
									</table>
								</td>
							</tr>																	
						
							<?=$libguidance_ui->getAttachmentLayout('Personal',$studentID, ($disabled ? 'view' : 'edit'))?>
							
						<? if (!$disabled):?>
							<tr>
								<td valign="top" nowrap="nowrap" class="tabletextremark"><?=$i_general_required_field?></td>
								<td width="80%">&nbsp;</td>
							</tr>
							
							<? if ($studentID): ?>
								<tr valign="top">
									<td class="tabletext" colspan="2"><a href="javascript:void(0)" id="HistoryNotify" onClick="show_history_push_message()"><?=$Lang['eGuidance']['PushMessage']['ViewStatus']?></a></td>
								</tr>
							<? endif;?>
							
						<? endif;?>
							
						</table>
					</td>
				</tr>
				<tr>
					<td height="1" class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
				</tr>
				<tr>
					<td align="right">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="center">
									<span>
								<? 	if ($disabled) {
										if ($fromThickbox) {
											echo $linterface->GET_ACTION_BTN($Lang['Btn']['Close'], "button", "js_Hide_ThickBox();");
										}
										else {
		 									if ($permission['admin'] || in_array('MGMT', (array)$permission['current_right']['MGMT']['PERSONAL'])) {
												echo $linterface->GET_ACTION_BTN($button_edit, "button", "window.location='edit.php?StudentID=$studentID'").'&nbsp';
											}
											echo $linterface->GET_ACTION_BTN($button_back, "button", "window.location='index.php'");
										}									
									}
									else {
										echo $linterface->GET_ACTION_BTN($button_submit, "button","","btnSubmit", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn")."&nbsp;";
										echo $linterface->GET_ACTION_BTN($Lang['eGuidance']['SubmitAndNotifyUpdate'], "button", "notify_colleague_update('PersonalID')","btnSubmitAndNotify", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn")."&nbsp;";
										echo $linterface->GET_ACTION_BTN($button_reset, "reset","","btnReset", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn")."&nbsp;";
										echo $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='index.php'","btnCancel", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");
									}
								?>
									</span></td>
							</tr>
						</table>
					</td>
				</tr>
			</table><br>
		</td>
	</tr>
</table>
<input type="hidden" id="OrgStudentID" name="OrgStudentID" value="<?=$studentID?>">
<input type="hidden" id="hidNotifierID" name="hidNotifierID" value="">
<input type="hidden" id="submitMode" name="submitMode" value="submit">
<?php if ($form_action == 'edit_update.php'):?>
	<input type="hidden" id="StudentID" name="StudentID" value="<?php echo $studentID;?>">
<?php endif;?>

</form>

<?=$libguidance_ui->getAttachmentUploadForm('Personal',$studentID)?>
