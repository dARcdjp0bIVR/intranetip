<?php
/*
 * 	modifying:
 * 
 * 	2017-08-16 [Cameron]
 * 		- create this file
 * 
 */

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/eGuidance_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/libguidance.php");

intranet_auth();
intranet_opendb();

$libguidance = new libguidance();
$permission = $libguidance->getUserPermission();
if (!$permission['admin'] && !in_array('MGMT', (array)$permission['current_right']['MGMT']['PERSONAL'])) {
	$libguidance->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$ldb = new libdb();
$result = array();
$dataAry = array();
$condAry = array();

if ($OrgStudentID && $StudentID) {
		
	if ($StudentID!=$OrgStudentID) {
		$dataAry['StudentID'] = $StudentID;
	}
		
	$dataAry['FatherTelNo'] = standardizeFormPostValue($FatherTelNo);
	$dataAry['MotherTelNo'] = standardizeFormPostValue($MotherTelNo);
	$dataAry['StudentTelNo'] = standardizeFormPostValue($StudentTelNo);
	$dataAry['GuidanceTeacher'] = $GuidanceTeacher;
	
	switch($ReferralFromType) {
		case 'Teacher':
			$ReferralFromType .= "^:". $FromTeacher;
			$referralFromReason = $FromTeacherReason;
			break;
		case 'AcademicGroup':
			$referralFromReason = implode("^~",(array)$RefAcaReason);
			if (in_array('AcaOther',(array)$RefAcaReason)) {
				$referralFromReason .= "^:".$AcaOther;
			}
			break;
		case 'DisciplineGroup':
			$referralFromReason = implode("^~",(array)$RefDisReason);
			if (in_array('DisOther',(array)$RefDisReason)) {
				$referralFromReason .= "^:".$DisOther;
			}
			break;
		case 'FromOther':
			$referralFromReason = $FromOther;
			break;
	}
	$dataAry['ReferralFromType'] = $ReferralFromType;
	$dataAry['ReferralFromReason'] = standardizeFormPostValue($referralFromReason);
	
	if ($RadioPlaceOfBirth == 'Hong Kong') {
		$placeOfBirth = $RadioPlaceOfBirth;
	}
	else {
		$placeOfBirth = $PlaceOfBirth;
	}
	
	$dataAry['PlaceOfBirth'] = standardizeFormPostValue($placeOfBirth);
	$dataAry['YearInHongKong'] = $YearInHongKong;
	
	$parentStatus = $ParentStatus;
	if ($parentStatus == 'PSOther') {
		$parentStatus .= "^:".$ParentStatusOther;
	}
	$dataAry['ParentStatus'] = standardizeFormPostValue($parentStatus);
	
	$dataAry['NbrElderBrother'] = $NbrElderBrother; 
	$dataAry['NbrYoungerBrother'] = $NbrYoungerBrother;
	$dataAry['NbrElderSister'] = $NbrElderSister;
	$dataAry['NbrYoungerSister'] = $NbrYoungerSister;
	
	foreach((array)$LiveWith as $k=>$v) {
		if ($v == 'Relative') {
			$LiveWith[$k] = $v."^:".$LwRelative;
		}
	}
	$liveWith = implode("^~",(array)$LiveWith);
	$dataAry['LiveWith'] = standardizeFormPostValue($liveWith);
	$dataAry['LwNbrElderBrother'] = $LwNbrElderBrother; 
	$dataAry['LwNbrYoungerBrother'] = $LwNbrYoungerBrother;
	$dataAry['LwNbrElderSister'] = $LwNbrElderSister;
	$dataAry['LwNbrYoungerSister'] = $LwNbrYoungerSister;
	
	$fatherWorkingStatus = $FatherWorkingStatus;
	if ($fatherWorkingStatus == 'FullTime') {
		$fatherWorkingStatus .= "^:".$FatherOccupation;
	}
	$dataAry['FatherWorkingStatus'] = standardizeFormPostValue($fatherWorkingStatus);
	
	$motherWorkingStatus = $MotherWorkingStatus;
	if ($motherWorkingStatus == 'FullTime') {
		$motherWorkingStatus .= "^:".$MotherOccupation;
	}
	$dataAry['MotherWorkingStatus'] = standardizeFormPostValue($motherWorkingStatus);
	
	$category = $libguidance->getGuidanceSettingItem('Personal','Category');
	foreach((array)$category as $k=>$v) {
		foreach((array)${"Prob_".$k} as $kk=>$vv) {
			switch ($vv) {
				case 'Illness':
					${"Prob_".$k}[$kk] = $vv."^:".$Illness;
					break;
				case 'CriminalOffense':
					${"Prob_".$k}[$kk] = $vv."^:".$CriminalOffense;
					break;
				case 'OtherProblem':
					${"Prob_".$k}[$kk] = $vv."^:".$OtherProblem;
					break;	
			}
		}
			
		$problem = implode("^~",(array)${"Prob_".$k});
		$dataAry['Prob'.$k] = standardizeFormPostValue($problem);
	}
										
	
	$dataAry['IsReferralToSocialWorker'] = $IsReferralToSocialWorker;
	
	$referralToReason = implode("^~",(array)$ReferralToReason);
	if (in_array('RefToOther',(array)$ReferralToReason)) {
		$referralToReason .= "^:".$RefToOther;
	}	 
	$dataAry['ReferralToReason'] = standardizeFormPostValue($referralToReason);
	$dataAry['LastModifiedBy'] = $_SESSION['UserID'];
	
	$condAry['StudentID'] = $OrgStudentID;
	$sql = $libguidance->UPDATE2TABLE('INTRANET_GUIDANCE_PERSONAL',$dataAry,$condAry,false,true);
	
	$ldb->Start_Trans();	
	## step 1: update to personal
	$result[] = $ldb->db_db_query($sql);

	## step 2: update attachment
	if (count($FileID) > 0) {
		$fileID = implode("','",$FileID);
		$sql = "UPDATE INTRANET_GUIDANCE_ATTACHMENT SET RecordID='".$StudentID."' WHERE FileID IN ('".$fileID."') AND RecordID=0";
		$result[] = $ldb->db_db_query($sql);
	}

}
else {		// no StudentID
	$result[] = false;	
}


if (!in_array(false,$result)) {
	$ldb->Commit_Trans();
	
	if ($submitMode == 'submitAndNotify') {
		$hidNotifierID = explode(',',$hidNotifierID);
		$notifyResult = $libguidance->sendNotifyToColleague($StudentID, $recordType='PersonalID', $hidNotifierID);
		$returnMsgKey = $notifyResult ? 'UpdateAndNotifySuccess' : 'UpdateSuccessNotifyFail';
	}
	else {
		$returnMsgKey = 'UpdateSuccess';
	}
}
else {
	$ldb->RollBack_Trans();
	$returnMsgKey = 'UpdateUnsuccess';
}
	
header("location: index.php?returnMsgKey=".$returnMsgKey);


intranet_closedb();

?>
