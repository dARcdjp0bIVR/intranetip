<?
/*
 * 	Log
 *
 *  2018-05-25 [Cameron]
 *      - fix: should retrieve all records (use left join instead of inner join that filter currrent academic year only) 
 *      
 *	2017-11-20 [Cameron]
 *		- fix cookie and keyword search
 *
 * 	2017-08-11 [Cameron] create this file
 */

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/eGuidance_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/libguidance.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

# set cookies
$arrCookies[] = array("ck_page_size", "numPerPage");
$arrCookies[] = array("ck_page_no", "pageNo");
$arrCookies[] = array("ck_page_order", "order");
$arrCookies[] = array("ck_page_field", "field");

$arrCookies[] = array("ck_ClassName", "ClassName");
$arrCookies[] = array("ck_keyword", "keyword");
if(isset($clearCoo) && $clearCoo == 1)
{
	clearCookies($arrCookies);
} 
else 
{
	updateGetCookies($arrCookies);
}

intranet_auth();
intranet_opendb();

$libguidance = new libguidance();
$permission = $libguidance->getUserPermission();
if (!$permission['admin'] && !$permission['current_right']['MGMT']['PERSONAL']) {
	$libguidance->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}
    
# Page heading setting
$linterface = new interface_html();
$CurrentPageArr['eGuidance'] = 1;
$CurrentPage = "PagePersonal";
$CurrentPageName = $Lang['eGuidance']['name'];

### Title ###
$TAGS_OBJ = array();
$TAGS_OBJ[] = array($Lang['eGuidance']['menu']['Management']['Personal']);

$MODULE_OBJ = $libguidance->GET_MODULE_OBJ_ARR();
if ($returnMsgKey == 'AddSuccessNotifyFail' || $returnMsgKey == 'UpdateSuccessNotifyFail' || $returnMsgKey == 'AddAndNotifySuccess' || $returnMsgKey == 'UpdateAndNotifySuccess') {
	$returnMsg = $Lang['eGuidance']['ReturnMessage'][$returnMsgKey];
}
else {
	$returnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];
}
$linterface->LAYOUT_START($returnMsg);

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$pageNo = ($pageNo == "") ? 1 : $pageNo;
$order = ($order == "") ? 1 : $order;
$field = ($field == "") ? 0 : $field;

$currentAcademicYearID = Get_Current_Academic_Year_ID();

$joinClass = false;
$cond = '';

if ($junior_mck) {
	if ($ClassName) {
		$cond .= " AND u.ClassName='".$ClassName."'";
	}
	$joinClass = '';
	$AcademicYearID = $AcademicYearID ? $AcademicYearID : $currentAcademicYearID;
}
else {
    $joinClass = true;
	if ($AcademicYearID) {
		$cond .= " AND ui.AcademicYearID='".$AcademicYearID."'";
	}
	if ($ClassName) {
		$cond .= " AND ui.ClassTitleEN='".$ClassName."'";
	}
	
	$AcademicYearID = $AcademicYearID ? $AcademicYearID : $currentAcademicYearID;
	$joinClass = "LEFT JOIN (
                        SELECT  ycu.UserID,
                                yc.AcademicYearID,
                                yc.ClassTitleEN
                        FROM
                                YEAR_CLASS_USER ycu
			            INNER JOIN
                                YEAR_CLASS yc ON yc.YearClassID=ycu.YearClassID AND yc.AcademicYearID='".$AcademicYearID."'
                  ) AS ui ON ui.UserID=u.UserID";      // ui - user info
}

$student_name = getNameFieldWithClassNumberByLang('u.');

$keyword = $_POST['keyword'] ? standardizeFormPostValue($_POST['keyword']) : standardizeFormGetValue($keyword);
if($keyword!="")
{
	$kw = $libguidance->Get_Safe_Sql_Like_Query($keyword);
	$cond .= " AND (EnglishName LIKE '%$kw%' OR ChineseName LIKE '%$kw%')";
	unset($kw);
}

$li = new libdbtable2007($field, $order, $pageNo);

$sql = "SELECT
				CONCAT('<a href=\"view.php?StudentID=',c.StudentID,'\">',".$student_name.",'</a>')";
if ($permission['admin'] || in_array('MGMT', (array)$permission['current_right']['MGMT']['PERSONAL'])) {
	$sql .= ",CONCAT('<input type=\'checkbox\' name=\'StudentID[]\' id=\'StudentID[]\' value=\'', c.`StudentID`,'\'>') ";
	$extra_column = 2;
	$checkbox_col = "<th width='1'>".$li->check("StudentID[]")."</th>\n";
	$toolbar = $linterface->GET_LNK_NEW("javascript:checkGet(document.form1,'new.php')",$button_new,"","","",0);
	$manage_record_bar  = '<a href="javascript:checkEdit(document.form1,\'StudentID[]\',\'edit.php\')" class="tool_edit">'.$button_edit.'</a>';
	$manage_record_bar .= '<a href="javascript:checkRemove(document.form1,\'StudentID[]\',\'remove.php\')" class="tool_delete">'.$button_delete.'</a>';
}		
else {
	$extra_column = 1;
	$checkbox_col = "";
	$toolbar = "";
	$manage_record_bar  = '';
}

$sql .= " FROM 
				INTRANET_GUIDANCE_PERSONAL c
		INNER JOIN
				INTRANET_USER u ON u.UserID=c.StudentID ".$joinClass ."
		WHERE 1 ".$cond;

$li->field_array = array("$student_name");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+$extra_column;
$li->title = $eDiscipline["Record"];
$li->column_array = array(0,0);
$li->wrap_array = array(0,0);
$li->IsColOff = "IP25_table";

$pos = 0;
$li->column_list .= "<th width='1' class='tabletoplink'>#</th>\n";
$li->column_list .= "<th width='95%' >".$li->column($pos++, $Lang['eGuidance']['StudentName'])."</th>\n";
$li->column_list .= $checkbox_col;


$htmlAry['searchBox'] = $linterface->Get_Search_Box_Div('keyword', $keyword);

############# start Filters
# Class Filter
$lclass = new libclass();
$classFilter = $lclass->getSelectClass("name='ClassName' onChange='this.form.submit();'",$ClassName,"",$Lang['eGuidance']['AllClass'],$AcademicYearID);
############# end Filters

?>

<form name="form1" method="POST" action="index.php">
	<div class="content_top_tool">
		
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td width="20%"><?=$toolbar ?></td>
				<td width="50%" align="center">&nbsp;</td>
				<td width="30%">
					<div class="content_top_tool"  style="float: right;">
						<?=$htmlAry['searchBox']?>     
						<br style="clear:both" />
					</div>
				</td>
			</tr>
		</table>
		
	</div>

	<div class="table_board">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr class="table-action-bar">
			<td valign="bottom">
				<div class="table_filter">
					<?=$classFilter?>
				</div> 
			</td>
			<td valign="bottom">
				<div class="common_table_tool">
					<?=$manage_record_bar?>
				</div>
			</td>
		</tr>
	</table>
	</div>

	<div class="table_board">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td valign="bottom">

				<?= $li->display() ?>
 
			</td>
		</tr>
	</table>
	</div>

	<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
	<input type="hidden" name="order" value="<?php echo $li->order; ?>" />
	<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
	<input type="hidden" name="page_size_change" value="" />
	<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />

</form>

<?php
    

$linterface->LAYOUT_STOP();
intranet_closedb();
?>