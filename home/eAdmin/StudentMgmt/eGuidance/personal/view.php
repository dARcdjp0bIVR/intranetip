<?php
/*
 * 	modifying:
 * 
 * 	Log
 *
 *  2018-05-25 [Cameron]
 *      - show student name without class
 *      
 * 	2017-10-19 [Cameron]
 * 		- fix: should be view but not edit in navigation path
 * 
 * 	2017-08-24 [Cameron]
 * 		- don't redirect to index.php where there's no personal record, it's handled in code below 
 * 
 * 	2017-08-15 [Cameron]
 * 		- create this file
 * 
 */

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/eGuidance_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/guidance_conf.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/libguidance.php");
include_once($PATH_WRT_ROOT."includes/eGuidance/libguidance_ui.php");

intranet_auth();
intranet_opendb();

$libguidance = new libguidance();
$permission = $libguidance->getUserPermission();

if (!$permission['admin'] && !$permission['current_right']['MGMT']['PERSONAL'] && !$fromThickbox) {	
	$libguidance->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

if (is_array($StudentID)) {
	$studentID = (count($StudentID)==1) ? $StudentID[0] : '';
}
else {
	$studentID = $StudentID;
}

if (!$studentID) {
	header("location: index.php");
}

$disabled = 'disabled';
$libguidance_ui = new libguidance_ui();
$linterface = new interface_html();
$lclass = new libclass();


$rs_personal = $libguidance->getPersonal($studentID);
$liveWithAry = array();		
$classSelection = array();
$studentSelection = array();


if (count($rs_personal) == 1) {
	$rs_personal = $rs_personal[0];

	$studentInfo = $libguidance->getStudentInfo($studentID);
	$rs_personal['Gender'] = count($studentInfo) ? $studentInfo['Gender'] : null;

	if (strlen($rs_personal['ReferralFromType'])>7 && substr($rs_personal['ReferralFromType'],0,7)=='Teacher') {
		list($referralFromType,$referralFrom) = explode('^:',$rs_personal['ReferralFromType']);
		$rs_personal['ReferralFromType'] = $referralFromType;
		$rs_personal['ReferralFrom'] = $referralFrom;
	}

	$refFromReasonAry = explode('^~',$rs_personal['ReferralFromReason']);
	foreach((array)$refFromReasonAry as $k=>$v) {
		if (strpos($v, '^:') !== false) {
			list($reasonType,$otherReason) = explode('^:',$v);
			$refFromReasonAry[$k] = $reasonType;
			$refFromReasonAry[$reasonType] = $otherReason;
		}
	}
	if ($rs_personal['ReferralFromType'] == 'Teacher' || $rs_personal['ReferralFromType'] == 'FromOther') {
		$refFromReasonAry[$rs_personal['ReferralFromType']] = $refFromReasonAry[0];
	}

	$liveWithAry = explode('^~',$rs_personal['LiveWith']);
	foreach((array)$liveWithAry as $k=>$v) {
		if (strpos($v, '^:') !== false) {
			list($liveWithType,$liveWithPerson) = explode('^:',$v);
			$liveWithAry[$k] = $liveWithType;
			$liveWithAry[$liveWithType] = $liveWithPerson;
		}
	}

	$rs_personal['TotalNbrBrotherSister'] = $rs_personal['NbrElderBrother'] + $rs_personal['NbrElderSister'] + $rs_personal['NbrYoungerBrother'] + $rs_personal['NbrYoungerSister'];

	$category = $libguidance->getGuidanceSettingItem('Personal','Category');
	foreach((array)$category as $k=>$v) {
		
		${"prob".$k."Ary"} = explode('^~',$rs_personal["Prob$k"]);
		
		foreach((array)${"prob".$k."Ary"} as $kk=>$vv) {
			if (strlen($vv)>7 && substr($vv,0,7)=='Illness') {
				${"prob".$k."Ary"}[$kk] = 'Illness';
				list($item,$illness) = explode('^:',$vv);
				${"prob".$k."Ary"}['Illness'] = $illness;
			}
			else if (strlen($vv)>15 && substr($vv,0,15)=='CriminalOffense') {
				${"prob".$k."Ary"}[$kk] = 'CriminalOffense';
				list($item,$criminal) = explode('^:',$vv);
				${"prob".$k."Ary"}['CriminalOffense'] = $criminal;
			}
			else if (strlen($vv)>12 && substr($vv,0,12)=='OtherProblem') {
				${"prob".$k."Ary"}[$kk] = 'OtherProblem';
				list($item,$otherProblem) = explode('^:',$vv);
				${"prob".$k."Ary"}['OtherProblem'] = $otherProblem;
			}
		}
	}

	$referralToReasonAry =  explode('^~',$rs_personal['ReferralToReason']);
	foreach((array)$referralToReasonAry as $k=>$v) {
		if (strpos($v, '^:') !== false) {
			list($refToType,$refToOther) = explode('^:',$v);
			$referralToReasonAry[$k] = $refToType;
			$referralToReasonAry[$refToType] = $refToOther;
		}
	}
	
	
	$rs_class_name = $libguidance->getClassByStudentID($studentID);
	if (!empty($rs_class_name)) {
		$classSelection = $lclass->getSelectClass("name='ClassName' id='ClassName' $disabled ",$rs_class_name);
		
		$excludeUserIdAry = $libguidance->getSENCaseByClass($rs_class_name,$studentID);
		$studentSelection = $libguidance->getStudentNameListWClassNumberByClassName($rs_class_name,'',$excludeUserIdAry);
		$studentSelection = getSelectByArray($studentSelection, "name='StudentID' id='StudentID' $disabled", $studentID);
	}
	else {
	    $studentInfo = $libguidance->getStudent($studentID);
	    $studentInfo = count($studentInfo) ? $studentInfo[0]['Name'] : '';
	    $showStudentNameOnly = true;
	}
	
}
else {
	// handle below
}


$form_action = "";

if ($fromThickbox) {
	if ($rs_personal) {
		include("personal.tmpl.php");
	}
	else {
		echo '<br><div align="Center">'.$Lang['General']['NoRecordAtThisMoment'].'</div>';
		echo '<br><div align="Center">'.$linterface->GET_ACTION_BTN($Lang['Btn']['Close'], "button", "js_Hide_ThickBox();").'</div>';
	}
}
else {
	# menu highlight setting
	$CurrentPageArr['eGuidance'] = 1;
	$CurrentPage = "PagePersonal";
	$CurrentPageName = $Lang['eGuidance']['name'];
	
	### Title ###
	$TAGS_OBJ = array();
	$TAGS_OBJ[] = array($Lang['eGuidance']['menu']['Management']['Personal']);
	
	$MODULE_OBJ = $libguidance->GET_MODULE_OBJ_ARR();
	$PAGE_NAVIGATION[] = array($Lang['eGuidance']['menu']['Management']['Personal'], "index.php");
	$PAGE_NAVIGATION[] = array($Lang['Btn']['View'], "");
	
	$linterface->LAYOUT_START();
	
	include("personal.tmpl.php");
	
	$linterface->LAYOUT_STOP();
}
intranet_closedb();

?>


