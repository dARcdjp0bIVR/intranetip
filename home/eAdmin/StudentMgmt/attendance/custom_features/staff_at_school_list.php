<?php

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");


intranet_auth();
intranet_opendb();

if (!(($_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || $_SESSION['UserType'] == USERTYPE_STAFF) && $_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent'] && $sys_custom['StudentAttendance']['HostelAttendance']) ) {
	intranet_closedb();
	header ("Location: /");
	exit();
}

$CurrentPage = "PageCustomFeatures_StaffAtSchoolList";


$lc = new libcardstudentattend2();
$linterface = new interface_html('popup.html');

$TAGS_OBJ[] = array($Lang['StudentAttendance']['StaffAtSchoolList'], "", 0);
$MODULE_OBJ = $lc->GET_MODULE_OBJ_ARR();
$MODULE_OBJ['title'] = $Lang['StudentAttendance']['StaffAtSchoolList'];
$linterface->LAYOUT_START();

$TargetDate = date("Y-m-d");
$ts = strtotime($TargetDate);
$year = date("Y", $ts);
$month = date("m", $ts);

$present_records = array();
$absent_records = array();

$StaffAttend3 = new libstaffattend3();
$LatestSetting = $StaffAttend3->Get_Real_Time_Staff_Status("","", "Staff", "1");
foreach($LatestSetting as $data) {
    $temp = array();
    $temp['name'] = $data['StaffName'];
    $temp['time'] = $data['LatestRecordTime'];
    if($data['StaffStatus'] == 'NotInSchool') {
		$absent_records[] = $temp;
	} else {
		$present_records[] = $temp;
	}
}
?>
	<style>
		.main_content { height: auto; }
	</style>
	<h1><?=$Lang['StudentAttendance']['StaffAtSchoolList']?></h1>
    <h2><?=date("Y-m-d H:i:s")?></h2>

	<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tr>
			<td width="45%" valign="top">
				<h3><?=$Lang['StudentAttendance']['StaffAtSchoolNumber']?>: <?=count($present_records)?></h3>
				<table width="100%" border="1" cellpadding="5" cellspacing="0" align="left">
					<tr>
						<td>
							<?php foreach($present_records as $temp) {
								echo $temp['name'];
								echo '<br/>';
							} ?>
						</td>
					</tr>
				</table>
			</td>
			<td width="10%"></td>
			<td width="45%" valign="top">
				<h3><?=$Lang['StudentAttendance']['StaffNotAtSchoolNumber']?>: <?=count($absent_records)?></h3>
				<table width="100%" border="1" cellpadding="5" cellspacing="0" align="left">
					<tr>
						<td>
							<?php foreach($absent_records as $temp) {
								$out_time = '';
								if($temp['time'] != '') {
									$out_time = ' (' . $temp['time'] . ')';
								}
								echo $temp['name'];
								echo $out_time;
								echo '<br/>';
							} ?>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

	<script>
        $(document).ready(function() {
            setTimeout(function() {
                location.reload();
            }, (60*1000));
        });

	</script>
<?php

$linterface->LAYOUT_STOP();
intranet_closedb();

?>