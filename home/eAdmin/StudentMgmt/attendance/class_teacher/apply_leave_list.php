<?php
## using: 

#####  Changing Log ########
#   Date:   2019-09-10 (Ray)
#       - Added missing enableApplyLeaveHomeworkPassSetting
#       - Added reject button
#
#   Date:	2017-09-18 	(Isaac)
#       - Added conditon to allow user access from KIS
#
#	Date:	2017-09-18 	(Bill)	[2017-0908-1248-12235]
#		- Copy from EJ
#		- Support Class Teacher to manage apply leave list
# 
############################


$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libteaching.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libapplyleave.php");

### Get parameters
$returnMsgKey = standardizeFormGetValue($_GET["returnMsgKey"]);
$keyword = standardizeFormPostValue($_POST["keyword"]);

### Cookies handling to preserve selection
//$arrCookies[] = array("{$moduleName}_{$leftMenu}_{$pageName}_{$variableName}", "$variableName");
// e.g. $arrCookies[] = array("eEnrolment_Mgmt_clubList_field", "field");
$arrCookies[] = array("eAtt_DailyOperation_ApplyLeave_keyword", "keyword");
$arrCookies[] = array("eAtt_DailyOperation_ApplyLeave_applicationTimeType", "applicationTimeType");
$arrCookies[] = array("eAtt_DailyOperation_ApplyLeave_applicationStatus", "applicationStatus");
$arrCookies[] = array("eAtt_DailyOperation_ApplyLeave_documentStatus", "documentStatus");
if(isset($clearCoo) && $clearCoo == 1) {
	clearCookies($arrCookies);
}
else {
	updateGetCookies($arrCookies);
}


include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$lcardstudentattend2 = new libcardstudentattend2();
$lapplyleave = new libapplyleave();


### Get Teaching Classes
$lteaching = new libteaching();
$TeachingClasses = $lteaching->returnTeacherClass($UserID);
$classCount = count((array)$TeachingClasses);


### Check Access Right
$canAccess = false;
$isClassTeacher = $classCount > 0;
//debug_pr($sys_custom["StudentAttendance"]["Class_Teacher_Approve_Leave_Request"]);
//debug_pr($isClassTeacher);
//debug_pr($classCount);
//die();
if (($_SESSION["platform"]=="KIS"||$plugin["eClassApp"]) && $_SESSION["SSV_PRIVILEGE"]["plugin"]["attendancestudent"] && $sys_custom["StudentAttendance"]["Class_Teacher_Approve_Leave_Request"] && $isClassTeacher) {
	$canAccess = true;
}
if (!$canAccess) {
	No_Access_Right_Pop_Up();
}


### Show return message
if($Msg!="") {
	if($Msg==1) {
		$returnMsg = $Lang['StudentAttendance']['PresetAbsenceUpdateSuccess'];
	}
	if($Msg==2) {
		$returnMsg = $Lang['StudentAttendance']['PresetAbsenceUpdateFail'];
	}
}
else {
	$returnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];
}


### Tab Display
// $TAGS_OBJ[] = array($displayLang, $onclickJs, $isSelectedTab);
$TAGS_OBJ[] = array($Lang['StudentAttendance']['ApplyLeaveAry']['ApplyLeave(App)'].' ('.$i_Teaching_ClassTeacher.')');
$MODULE_OBJ['title'] = $ip20TopMenu['eAttendance'];
$linterface->LAYOUT_START($returnMsg);
echo $linterface->Include_Thickbox_JS_CSS();


### Search Box
$htmlAry['searchBox'] = $linterface->Get_Search_Box_Div('keyword', $keyword);


### Drop down list filtering
//$applicationTimeType = ($applicationTimeType=='')? 3 : $applicationTimeType;		// default "today record(s)"
$applicationTimeType = ($applicationTimeType=='')? 1 : $applicationTimeType;		// default "all application time"
$htmlAry['applicationTimeTypeSel'] = $lapplyleave->getApplyLeaveApplicationTimeTypeSelection('applicationTimeTypeSel', 'applicationTimeType', $applicationTimeType, "changedApplicationTimeTypeSeletion(this.value);");

$applicationStatus = ($applicationStatus=='')? 2 : $applicationStatus;		// default "pending"
$htmlAry['acknowledgmentStatusSel'] = $lapplyleave->getApplyLeaveAcknowledgmentStatusSelection('applicationStatusSel', 'applicationStatus', $applicationStatus, "changedApplicationStatusSeletion(this.value);");

$documentStatus = ($documentStatus=='')? 1 : $documentStatus;		// default "all"
$htmlAry['documentStatusSel'] = $lapplyleave->getApplyLeaveDocumentStatusSelection('documentStatusSel', 'documentStatus', $documentStatus, "changedDocumentStatusSeletion(this.value);");

//get settings
$settingAssoAry = $lapplyleave->getSettingsAry();

### DB Table Action Buttons
#	Hide "acknowledge" button if the filtering status is "acknowledged"
// $btnAry[] = array($btnClass, $btnHref, $displayLang);
$btnAry = array();
if ($applicationStatus != 3) {
	$btnAry[] = array('approve', 'javascript: goApproveRecord();', $Lang['StudentAttendance']['ApplyLeaveAry']['Acknowledge']);
	$btnAry[] = array('reject', 'javascript: goReject();');
}
$btnAry[] = array('delete', 'javascript: goDelete();');
$htmlAry['dbTableActionBtn'] = $linterface->Get_DBTable_Action_Button_IP25($btnAry);


# Get list SQL
$TeachingClasses = Get_Array_By_Key($TeachingClasses, "ClassID");
$sql = $lapplyleave->getApplyLeaveRecordListPageSql($keyword, $applicationTimeType, $applicationStatus, $documentStatus,false,$settingAssoAry['enableApplyLeaveHomeworkPassSetting']);
$sql .= " AND yc.YearClassID IN ('".implode("','", (array)$TeachingClasses)."')";


### DB Table
$order = ($order == '') ? 0 : $order;	// 1 => asc, 0 => desc
$field = ($field == '') ? 3 : $field;
$pageNo = ($pageNo == '') ? 1 : $pageNo;
$page_size = ($numPerPage == '') ? 50 : $numPerPage;

$li = new libdbtable2007($field, $order, $pageNo);
if ($settingAssoAry['enableApplyLeaveHomeworkPassSetting']==1) {
	$li->field_array = array("ClassName", "ClassNumber", "StudentName", "r.InputDate", "r.StartDate", "r.EndDate", "r.Duration", "r.Reason", "Method","r.ModifiedDate");
} else {
	$li->field_array = array("ClassName", "ClassNumber", "StudentName", "r.InputDate", "r.StartDate", "r.EndDate", "r.Duration", "r.Reason","r.ModifiedDate");
}
$li->sql = $sql;
$li->no_col = sizeof($li->field_array) + 5;
$li->column_array = array(0,0,0,0,0,0,0,18);	
$li->fieldorder2 = ", ClassNumber";
$li->IsColOff = "IP25_table";


$pos = 0;
$li->column_list .= "<th width='1' class='tabletoplink'>#</th>\n";
$li->column_list .= "<th width='5%' >".$li->column($pos++, $Lang['General']['Class'])."</th>\n";
$li->column_list .= "<th width='5%' >".$li->column($pos++, $Lang['SysMgr']['FormClassMapping']['ClassNo'])."</th>\n";
$li->column_list .= "<th width='14%' >".$li->column($pos++, $Lang['Identity']['Student'])."</th>\n";
$li->column_list .= "<th width='9%' >".$li->column($pos++, $Lang['StudentAttendance']['ApplyLeaveAry']['ApplicationTime'])."</th>\n";
$li->column_list .= "<th width='9%' >".$li->column($pos++, $Lang['General']['StartDate'])."</th>\n";
$li->column_list .= "<th width='9%' >".$li->column($pos++, $Lang['General']['EndDate'])."</th>\n";
$li->column_list .= "<th width='5%' >".$li->column($pos++, $Lang['StudentAttendance']['ApplyLeaveAry']['Duration'])."</th>\n";
$li->column_list .= "<th width='20%' >".$li->column($pos++, $Lang['General']['Reason'])."</th>\n";
if ($settingAssoAry['enableApplyLeaveHomeworkPassSetting']==1) {
	$li->column_list .= "<th width='20%' >".$li->column($pos++, $Lang['StudentAttendance']['ApplyLeaveAry']['homeworkDeliveryMethod'])."</th>\n";
}
$li->column_list .= "<th width='5%' >".$Lang['StudentAttendance']['ApplyLeaveAry']['Document']."</th>\n";
$li->column_list .= "<th width='9%' >".$Lang['StudentAttendance']['ApplyLeaveAry']['RecordAcknowledgeStatus']."</th>\n";
$li->column_list .= "<th width='10%' >".$Lang['StudentAttendance']['ApplyLeaveAry']['DocumentApprovalStatus']."</th>\n";
$li->column_list .= "<th width='9%' >".$li->column($pos++, $Lang['General']['LastUpdatedTime'])."</th>\n";
$li->column_list .= "<th width='1'>".$li->check("recordIdAry[]")."</th>\n";
$htmlAry['dataTable'] = $li->display();

//debug_pr($li->built_sql());
?>

<script type="text/javascript">
var approveImg = '<?=$linterface->Get_Approved_Image()?>';
var rejectImg = '<?=$linterface->Get_Rejected_Image()?>';

$(document).ready( function() {
	$('input#keyword').keydown( function(evt) {
		if (Check_Pressed_Enter(evt)) {
			// pressed enter
			goSearch();
		}
	});
});

function goSearch() {
	$('form#form1').submit();
}

/*
function goExport() {
	$('form#form1').attr('action', 'export.php').submit().attr('action', 'list.php');
}
*/

function changedApplicationTimeTypeSeletion() {
	$('form#form1').submit();
}

function changedApplicationStatusSeletion() {
	$('form#form1').submit();
}

function changedDocumentStatusSeletion() {
	$('form#form1').submit();
}

function goChangeApprovalStatus(targetStatus) {
	if ($('input.recordIdChk:checked').length == 0) {
		alert(globalAlertMsg1);
	}
	else {
		$('input#targetDbField').val('ApprovalStatus');
		$('input#targetStatus').val(targetStatus);
		$('form#form1').attr('action', '../dailyoperation/apply_leave/change_status.php?cteach=1').submit();
	}
}

function goApproveRecord() {
	if ($('input.recordIdChk:checked').length == 0) {
		alert(globalAlertMsg1);
	}
	else {
		var targetStatus = 1;//1 is approve statu;
		$('input#targetDbField').val('ApprovalStatus');
		$('input#targetStatus').val(targetStatus);
		$('form#form1').attr('action', '../dailyoperation/apply_leave/approve_leave.php?cteach=1').submit();
	}
}

function goReject() {
    if ($('input.recordIdChk:checked').length == 0) {
        alert(globalAlertMsg1);
    }
    else {
        var targetStatus = 2;//2 is reject statu; 3 is for cancel;
        $('input#targetDbField').val('ApprovalStatus');
        $('input#targetStatus').val(targetStatus);
        $('form#form1').attr('action', '../dailyoperation/apply_leave/reject_leave.php?cteach=1').submit();
    }
}

function viewDocument(recordId) {
	load_dyn_size_thickbox_ip('<?=$Lang['StudentAttendance']['ApplyLeaveAry']['Document']?>', 'onloadDocumentThickbox(' + recordId + ');', '', 500, 800);
	//load_dyn_size_thickbox_ip('<?=$Lang['StudentAttendance']['ApplyLeaveAry']['Document']?>', 'onloadDocumentThickbox(' + recordId + ');');
}

function onloadDocumentThickbox(recordId) {
	$('div#TB_ajaxContent').load(
		"../dailyoperation/apply_leave/ajax_view_document.php", 
		{ 
			recordId: recordId
		},
		function(ReturnData) {
			adjust_dyn_size_thickbox_content_height_ip('thickboxContainerDiv', 'thickboxContentDiv', 'editBottomDiv');
		}
	);
}

function goDelete() {
	checkRemove(document.form1,'recordIdAry[]','../dailyoperation/apply_leave/delete.php?cteach=1');
}

function changeDocumentStatus(leaveRecordId, documentStatus) {
	$('input#approveBtn_tb, input#rejectBtn_tb').attr('disabled', 'disabled');
	
	$.post(
		"../dailyoperation/apply_leave/ajax_update.php", 
		{ 
			actionType: 'changeDocumentStatus',
			leaveRecordId: leaveRecordId,
			documentStatus: documentStatus
		},
		function(ReturnData) {
			var returnMessage = '';
			$('input#approveBtn_tb, input#rejectBtn_tb').attr('disabled', '');
			
			if (ReturnData == '1') {
				js_Hide_ThickBox();
				returnMessage = '<?=$Lang['General']['ReturnMessage']['UpdateSuccess']?>';
				
				var targetSpanId = 'documentStatusSpan_' + leaveRecordId;
				var targetImg = '';
				if (documentStatus == 2) {
					targetImg = approveImg;
				}
				else {
					targetImg = rejectImg;
				}
				$('span#' + targetSpanId).html(targetImg);
			}
			else {
				returnMessage = '<?=$Lang['General']['ReturnMessage']['UpdateUnSuccess']?>';
			}
			Get_Return_Message(returnMessage);
		}
	);
}
</script>
<form name="form1" id="form1" method="POST" action="apply_leave_list.php">
	<?=$htmlAry['subTab']?>
	
	<div class="content_top_tool">
		<?=$htmlAry['contentTool']?>
		<?=$htmlAry['searchBox']?>
		<br style="clear:both;">
	</div>
	
	<div class="table_board">
		<div class="table_filter">
			<?=$htmlAry['applicationTimeTypeSel']?>
			<?=$htmlAry['acknowledgmentStatusSel']?>
			<?=$htmlAry['documentStatusSel']?>
		</div>
		<p class="spacer"></p>
		
		<?=$htmlAry['dbTableActionBtn']?>
		<?=$htmlAry['dataTable']?>
	</div>
	
	<input type="hidden" id="pageNo" name="pageNo" value="<?=$li->pageNo?>" />
	<input type="hidden" id="order" name="order" value="<?=$li->order?>" />
	<input type="hidden" id="field" name="field" value="<?=$li->field?>" />
	<input type="hidden" id="page_size_change" name="page_size_change" value="" />
	<input type="hidden" id="numPerPage" name="numPerPage" value="<?=$li->page_size?>" />
	
	<input type="hidden" id="targetDbField" name="targetDbField" value="" />
	<input type="hidden" id="targetStatus" name="targetStatus" value="" />
</form>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>