<?
//using by kenneth chung

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	echo 'die';
	intranet_closedb();
	exit();
}

$StudentAttend = new libcardstudentattend2();

$RecordID = $_REQUEST['RecordID'];
$ReasonText = stripslashes(urldecode(trim($_REQUEST['ReasonText'])));

if ($StudentAttend->Check_Outing_Reason($ReasonText,$RecordID)) {
	echo "0";
}
else {
	echo "1";
}

intranet_closedb();
?>