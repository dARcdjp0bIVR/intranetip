<?php
/*
 * 2019-06-19 (Ray): Created.
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	intranet_closedb();
	header ("Location: /");
	exit();
}

$lc = new libcardstudentattend2();
$linterface = new interface_html();

$task = $_POST['task'];

switch($task)
{
	case 'getModalForm':
		
		$RecordID = IntegerSafe($_POST['RecordID']);
		$ReasonText = '';
		$record = array();
		if($RecordID > 0){
		    $record = $lc->GetTeacherRemarkPresetRecords(array('RecordID'=>$RecordID));
			$ReasonText = $record[0]['ReasonText'];
		}
		
		
		$x .= '<form name="modalForm" id="modalForm" action="" method="post" onsubmit="return false;">'."\n";
		$x .= '<div id="modalContent" style="overflow-y:auto;">'."\n";
		$x .= '<br />'."\n";
		$x .= $linterface->Get_Thickbox_Warning_Msg_Div("Overall_Error","", "WarnMsgDiv")."\n";
		$x .= '<table width="100%" cellpadding="2" class="form_table_v30">'."\n";
			$x .= '<col class="field_title">'."\n";
			$x .= '<col class="field_c">'."\n";
			
			$x .= '<tr>'."\n";
			$x .= '<td class="field_title">'.$Lang['General']['Remark'].'<span class="tabletextrequire">*</span></td>'."\n";
				$x .= '<td>'."\n";
				$x .= $linterface->GET_TEXTBOX_NAME('ReasonText', "ReasonText", $ReasonText);
					$x .= $linterface->Get_Thickbox_Warning_Msg_Div("Reason_Error","", "WarnMsgDiv")."\n";
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
			
			
		$x .= '</table>'."\n";
		if($record[0]['RecordID'] !='' && $record[0]['RecordID'] > 0){
			$x .= '<input type="hidden" name="RecordID" id="RecordID" value="'.$RecordID.'" />'."\n";
		}
		$x .= '<input type="hidden" name="task" id="task" value="" />';
		$x .= '</div>'."\n";
		
		$x .= $linterface->MandatoryField();		
		
		$x .= '<div class="edit_bottom_v30">'."\n";
			$x .= $linterface->GET_ACTION_BTN($Lang['Btn']['Submit'],"button","checkEditForm();").'&nbsp;';
			$x .= $linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'],"button","tb_remove();")."\n";
		$x .= '</div>'."\n";
		
		$x .= '</form>'."\n";
		
		echo $x;
	break;
	
	case 'upsertRecord':
		
		$values = $_POST;
		$success = $lc->UpsertTeacherRemarkPresetRecords($values);
		$msg = $success? $Lang['General']['ReturnMessage']['UpdateSuccess'] : $Lang['General']['ReturnMessage']['UpdateUnsuccess'];
		$_SESSION['TEACHER_REMARK_PRESET_SETTINGS_RESULT'] = $msg;
		echo $success ? '1':'0';
		
	break;
	case 'deleteRecord':
		
	    $success = $lc->DeleteTeacherRemarkPresetRecords($RecordID);
		$msg = $success ? $Lang['General']['ReturnMessage']['DeleteSuccess'] : $Lang['General']['ReturnMessage']['DeleteUnsuccess'];
		$_SESSION['TEACHER_REMARK_PRESET_SETTINGS_RESULT'] = $msg;
		echo $success?'1':'0';
		
	break;
}

intranet_closedb();
?>