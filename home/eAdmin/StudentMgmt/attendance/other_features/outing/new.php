<?php
// Editing by 
/*
 * 2015-01-28 (Carlos): Improved can select multiple dates
 * 2013-12-17 (Carlos): Added duplication checking and warning of outing records
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libstudentattendance_ui.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();
$StudentAttendUI = new libstudentattendance_ui();
$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageOtherFeatures_Outing";

$linterface = new interface_html();

$lclass = new libclass();

$TAGS_OBJ[] = array($i_StudentAttendance_Menu_OtherFeatures_Outing, "", 0);

$PAGE_NAVIGATION[] = array($button_new);

$MODULE_OBJ = $lc->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

if ($msg == 1) $SysMsg = $linterface->GET_SYS_MSG("add");
if ($msg == 2) $SysMsg = $linterface->GET_SYS_MSG("update");
if ($msg == 3) $SysMsg = $linterface->GET_SYS_MSG("delete");

$OutingID = $_REQUEST['OutingID'];

?>
<link href="/templates/jquery/jquery.alerts.css" rel="stylesheet" type="text/css">
<script language="JavaScript" src="/templates/jquery/jquery.alerts.js"></script>

<script language="javascript">
function addPIC(obj, pic)
{
         if (Trim(obj.PIC.value) != '')
         {
             obj.PIC.value += ', ';
         }
         obj.PIC.value += pic;
}
function checkform(obj)
{
	selectionObj = document.getElementById("SelectedUserIDArr[]");
	Remove_All_Selection_Option(selectionObj,"");
	Select_All_Options("SelectedUserIDArr[]",true);
	if (document.getElementById("SelectedUserIDArr[]").length == 0)
	{
		alert('<?=$i_Profile_SelectUser?>');
		return false;
	}
	
	var dateAry = getValuesByName('OutingDate[]');
	if(dateAry.length == 0){
		alert('<?=$Lang['StudentAttendance']['Warning']['PleaseSelectAtLeastOneDay']?>');
		return false;
	}
	//if (!check_date(obj.OutingDate,'<?=$i_invalid_date?>'))  return false;
	
	if (document.getElementById('OutTime').value != '') {
		if (!check_time(document.getElementById('OutTime'),'<?=$Lang['StudentAttendance']['InvalidTimeWarning']?>')) 
			return false;
	}
	
	if (document.getElementById('BackTime').value != '') {
		if (!check_time(document.getElementById('BackTime'),'<?=$Lang['StudentAttendance']['InvalidTimeWarning']?>'))
			return false;
	}
	
	if (!document.getElementById('dateTypeAM').checked && !document.getElementById('dateTypePM').checked) {
		alert('<?=$Lang['StudentAttendance']['SelectSessionWarning']?>');
		
		return false;
	}
	
	//return true;
	Check_Duplicate_Outing_Records(obj);
}
function Get_Student_Selection(ClassName) {
	$('td#StudentSelectionLayer').load('ajax_get_student_selection.php',{"ClassName":encodeURIComponent(ClassName)},
		function(data) {
			if (data == "die") 
				window.top.location = "/";
		});
}

var AutoCompleteObj_ClassNameClassNumber;
var AutoCompleteObj_UserLogin;
$(document).ready( function() {
	
	// initialize jQuery Auto Complete plugin
	Init_JQuery_AutoComplete('UserClassNameClassNumberSearchTb', 'UserLoginSearchTb');
	
	$('input#UserLoginSearchTb').focus();
});

function Init_JQuery_AutoComplete(InputID_ClassNameClassNumber, InputID_UserLogin) {
	
	AutoCompleteObj_ClassNameClassNumber = $("input#" + InputID_ClassNameClassNumber).autocomplete(
		"ajax_get_student_list_by_class_name_class_number_name.php",
		{  			
			onItemSelect: function(li) {
				Add_Selected_User(li.extra[1], li.selectValue, InputID_ClassNameClassNumber);
			},
			formatItem: function(row) {
				return row[0];
			},
			maxItemsToShow: 5,
			minChars: 1,
			delay: 0,
			width: 200
		}
	);
	
	AutoCompleteObj_UserLogin = $("input#" + InputID_UserLogin).autocomplete(
		"ajax_get_student_list_by_login.php",
		{  			
			onItemSelect: function(li) {
				Add_Selected_User(li.extra[1], li.selectValue, InputID_UserLogin);
			},
			formatItem: function(row) {
				return row[0];
			},
			maxItemsToShow: 5,
			minChars: 1,
			delay: 0,
			width: 200
		}
	);
	
	Update_Auto_Complete_Extra_Para();
}

function Add_Selected_User(UserID, UserName, InputID) {
	var UserID = UserID || "";
	var UserName = UserName || "";
	var UserSelected = document.getElementById('SelectedUserIDArr[]');

	UserSelected.options[UserSelected.length] = new Option(UserName, UserID);
	
	Update_Auto_Complete_Extra_Para();
	
	// reset and refocus the textbox
	$('input#' + InputID).val('').focus();
}

function Update_Auto_Complete_Extra_Para() {
	ExtraPara = new Array();
	ExtraPara['SelectedUserIDList'] = Get_Selection_Value('SelectedUserIDArr[]', 'Array', true);
	
	AutoCompleteObj_ClassNameClassNumber[0].autocompleter.setExtraParams(ExtraPara);
	AutoCompleteObj_UserLogin[0].autocompleter.setExtraParams(ExtraPara);
}

function Get_Student_Selection_Pop_Up() {
	newWindow("/home/common_choose/index.php?fieldname=SelectedUserIDArr[]&page_title=SelectTarget&permitted_type=2",9);
}

function js_Remove_Selected_Student()
{
	checkOptionRemove(document.getElementById('SelectedUserIDArr[]'));
	Update_Auto_Complete_Extra_Para();
}

function Check_Duplicate_Outing_Records(formObj)
{
	Block_Element('form1','<?=$Lang['StudentAttendance']['Checking']?>');
	
	//var RecordDate = $.trim($('input#OutingDate').val());
	var RecordDates = getValuesByName('OutingDate[]');
	var UserSelection = document.getElementById('SelectedUserIDArr[]');
	var TargetUserID = [];
	var DayTypeCheckbox = $('input[name=DayType[]]:checked');
	var DayType = [];
	
	for(var i=0;i<UserSelection.options.length;i++){
		TargetUserID.push(UserSelection.options[i].value.replace('U',''));
	}
	for(var i=0;i<DayTypeCheckbox.length;i++){
		DayType.push(DayTypeCheckbox.get(i).value);
	}
	
	$.post(
		'ajax_get_outing_check_result.php',
		{
			'RecordDate[]':RecordDates,
			'TargetUserID[]':TargetUserID,
			'DayType[]':DayType
		},
		function(data){
			UnBlock_Element('form1');
			if(data != ''){
				$('#WarningLayer').html(data);
				jConfirm('<?=$Lang['StudentAttendance']['Warning']['ConfirmOverwriteDuplicatedOutingRecords']?>', '<?=$Lang['Btn']['Confirm']?>', function(r) {
				    if(r){
				    	// OK
			        	formObj.submit();
				    }else{
				    	// Cancel
				    	
				    }
				});
			}else{
				$('#WarningLayer').html('');
				formObj.submit();
			}
		}
	);
}

var table_row_template = '<tr class="#TABLEROW_CLASS# data-row">';
table_row_template += '<td>#ROW_NUMBER#</td>';
table_row_template += '<td class="data-col">#DATE#<input type="hidden" name="OutingDate[]" value="#DATE#" /></td>';
table_row_template += '<td><?=$linterface->GET_LNK_DELETE("javascript:void(0);", $Lang['Btn']['Delete'], "onDateDeleted(this);", "", 1)?></td>';
table_row_template += '</tr>';

function onDateDeleted(objA)
{
	var row = $(objA).closest('tr');
	$(row).remove();
	onDatePickerSelected('');
}

function onDatePickerSelected(dateText)
{
	var dates = getValuesByName('OutingDate[]');
	if(dateText !='' && dates.indexOf(dateText)==-1){
		dates.push(dateText);
	}
	dates.sort();
	var row_css = 1;
	var rows_html = '';
	for(var i=0;i<dates.length;i++){
		row_css = i % 2 == 0 ? 1:2;
		var row_html = table_row_template.replace(/#TABLEROW_CLASS#/gi, 'tablerow'+row_css);
		row_html = row_html.replace(/#ROW_NUMBER#/gi, i+1);
		row_html = row_html.replace(/#DATE#/gi, dates[i]);
		rows_html += row_html;
	}
	$('#TableDate tr.data-row').remove();
	$('#TableDate tr.tabletop').after(rows_html);
}

function getValuesByName(objName)
{
	var objAry = document.getElementsByName(objName);
	var ary = [];
	for(var i=0;i<objAry.length;i++){
		ary.push(objAry[i].value);
	}
	return ary;
}
</script>

<?
echo $StudentAttendUI->Get_Preset_Outing_Form($OutingID);

$linterface->LAYOUT_STOP();
intranet_closedb();
?>
