<?
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();

$li = new libdb();
$Location = intranet_htmlspecialchars($Location);
$Remark = intranet_htmlspecialchars($Remark);
$Objective = intranet_htmlspecialchars($Objective);
$PIC = intranet_htmlspecialchars($PIC);

$outTimeStr = ($OutTime ==""? "NULL":"'$OutTime'");
$backTimeStr = ($BackTime ==""? "NULL":"'$BackTime'");

$sql = "UPDATE CARD_STUDENT_OUTING SET
               RecordDate = '$OutingDate',
               OutTime = $outTimeStr,
               BackTime = $backTimeStr,
               Location = '$Location',
               Objective = '$Objective',
               FromWhere = '$FromWhere',
               Detail = '$Remark',
               PIC = '$PIC',
               DateModified = now()
        WHERE OutingID = $OutingID
               ";
if ($li->db_db_query($sql)) {
	$Msg = $Lang['StudentAttendance']['OutingRecordUpdateSuccess'];
	
}
else {
	$Msg = $Lang['StudentAttendance']['OutingRecordUpdateFail'];
}
header ("Location: index.php?Msg=".urlencode($Msg));
intranet_closedb();
?>
