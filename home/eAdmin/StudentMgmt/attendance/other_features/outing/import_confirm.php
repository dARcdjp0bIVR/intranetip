<?php
// Editing by 
/*
 * 2016-06-24 (Carlos): Added import fields [LeavesAt], [Location] and [Remark].
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libstudentattendance_ui.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();
$StudentAttendUI = new libstudentattendance_ui();
$limport = new libimporttext();

$format_array = array("ClassName","ClassNumber","Date","TimeSlot","Teacher","StartTime","EndTime","LeavesAt","Location","Reason","Remark");

$li = new libdb();
$lf = new libfilesystem();
$filepath = $userfile;
$filename = $userfile_name;
if($filepath=="none" || $filepath == ""){          # import failed
  header("Location: import.php?Msg=".urlencode($Lang['StudentAttendance']['DataImportFail']));
  exit();
} else {
	$ext = strtoupper($lf->file_ext($filename));
	if($limport->CHECK_FILE_EXT($filename))
	{
	  # read file into array
	  # return 0 if fail, return csv array if success
	  //$data = $lf->file_read_csv($filepath);
	  $data = $limport->GET_IMPORT_TXT($filepath);
	  if(sizeof($data)>0)
	  {
	  	$toprow = array_shift($data);                   # drop the title bar
	  }else
	  {
	  	header("Location: import.php?Msg=".urlencode($Lang['StudentAttendance']['DataImportFail']));
	    intranet_closedb();
	    exit();
	  }
	}
	for ($i=0; $i<sizeof($format_array); $i++)
	{
	 if ($toprow[$i] != $format_array[$i])
	 {
     header("Location: import.php?Msg=".urlencode($Lang['StudentAttendance']['DataImportFail']));
     intranet_closedb();
     exit();
	 }
	}
	
	$CurrentPageArr['StudentAttendance'] = 1;
	$CurrentPage = "PageOtherFeatures_Outing";
	$linterface = new interface_html();
	
	$TAGS_OBJ[] = array($i_StudentAttendance_Menu_OtherFeatures_Outing, "", 0);
	
	$MODULE_OBJ = $lc->GET_MODULE_OBJ_ARR();
	$linterface->LAYOUT_START(urldecode($_REQUEST['Msg']));
  echo $StudentAttendUI->Get_Preset_Outing_Import_confirm($data);
}

$linterface->LAYOUT_STOP();
intranet_closedb();
?>