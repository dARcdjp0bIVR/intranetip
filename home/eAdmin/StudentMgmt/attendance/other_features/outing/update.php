<?php
// Editing by 
/*
 * 2015-01-28 (Carlos): changed $OutingDate as array type
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();

$li = new libdb();
$OutingID = $_REQUEST['OutingID'];
$StudentID = Get_User_Array_From_Common_Choose($_REQUEST['SelectedUserIDArr'],array(2));
$OutingDate = (array)$_REQUEST['OutingDate'];
$DayType = $_REQUEST['DayType'];
$OutTime = $_REQUEST['OutTime'];
$BackTime = $_REQUEST['BackTime'];
$PIC = trim(urldecode(stripslashes($_REQUEST['PIC'])));
$FromWhere = trim(urldecode(stripslashes($_REQUEST['FromWhere'])));
$Location = trim(urldecode(stripslashes($_REQUEST['Location'])));
$Objective = trim(urldecode(stripslashes($_REQUEST['Objective'])));
$Detail = trim(urldecode(stripslashes($_REQUEST['Detail'])));

$resultAry = array();
for($i=0;$i<sizeof($OutingDate);$i++){
	$resultAry[] = $lc->Save_Preset_Outing($OutingID,$StudentID,$OutingDate[$i],$DayType,$OutTime,$BackTime,$PIC,$FromWhere,$Location,$Objective,$Detail);
}

//if ($lc->Save_Preset_Outing($OutingID,$StudentID,$OutingDate,$DayType,$OutTime,$BackTime,$PIC,$FromWhere,$Location,$Objective,$Detail)) {
if(!in_array(false,$resultAry)){
	$Msg = $Lang['StudentAttendance']['OutingRecordUpdateSuccess'];
	
}
else {
	$Msg = $Lang['StudentAttendance']['OutingRecordUpdateFail'];
}
header ("Location: index.php?Msg=".urlencode($Msg));
intranet_closedb();
?>
