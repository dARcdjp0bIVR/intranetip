<?php
// Editing by 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libstudentattendance_ui.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();
$StudentAttendUI = new libstudentattendance_ui();
$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageOtherFeatures_Outing";

$linterface = new interface_html();

$OutingID = (is_array($OutingID)? $OutingID[0]:$OutingID);

$TAGS_OBJ[] = array($i_StudentAttendance_Menu_OtherFeatures_Outing, "", 0);

$PAGE_NAVIGATION[] = array($button_edit);

$MODULE_OBJ = $lc->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

if ($msg == 1) $SysMsg = $linterface->GET_SYS_MSG("add");
if ($msg == 2) $SysMsg = $linterface->GET_SYS_MSG("update");
if ($msg == 3) $SysMsg = $linterface->GET_SYS_MSG("delete");	
?>
<script language="javascript">
function addPIC(obj, pic)
{
	if (obj.PIC.value != '')
	{
	   obj.PIC.value += ', ';
	}
	obj.PIC.value += pic;
}

function checkform(obj) {
	document.form1.submit();
	return true;
}
</script>
<?

echo $StudentAttendUI->Get_Preset_Outing_Form($OutingID);

$linterface->LAYOUT_STOP();
intranet_closedb();
?>