<?php
// kenneth chung
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

# preserve table view
if ($ck_attend_features_reminder_page_number!=$pageNo && $pageNo!="")
{
	setcookie("ck_attend_features_reminder_page_number", $pageNo, 0, "", "", 0);
	$ck_attend_features_reminder_page_number = $pageNo;
} else if (!isset($pageNo) && $ck_attend_features_reminder_page_number!="")
{
	$pageNo = $ck_attend_features_reminder_page_number;
}

if ($ck_attend_features_reminder_page_order!=$order && $order!="")
{
	setcookie("ck_attend_features_reminder_page_order", $order, 0, "", "", 0);
	$ck_attend_features_reminder_page_order = $order;
} else if (!isset($order) && $ck_attend_features_reminder_page_order!="")
{
	$order = $ck_attend_features_reminder_page_order;
}

if ($ck_attend_features_reminder_page_field!=$field && $field!="")
{
	setcookie("ck_attend_features_reminder_page_field", $field, 0, "", "", 0);
	$ck_attend_features_reminder_page_field = $field;
} else if (!isset($field) && $ck_attend_features_reminder_page_field!="")
{
	$field = $ck_attend_features_reminder_page_field;
}

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
 
intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();
$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageOtherFeatures_Reminder";

$linterface = new interface_html();

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$order = ($order == 1) ? 1 : 0;

if (isset($targetID) && $targetID != "")
{
    $conds = "AND a.UserID = $targetID";
}
$user_field = getNameFieldWithClassNumberByLang("b.");
$teacher_field = getNameFieldByLang("c.");

if ($filter == 1)
{
    $conds .= " AND a.DateOfReminder >= CURDATE()";
}
else if ($filter == 2)
{
     $conds .= " AND a.DateOfReminder < CURDATE()";
}

$sql  = "SELECT
						a.DateOfReminder, 
						$user_field, 
						$teacher_field,
						a.Reason,
            CONCAT('<input type=''checkbox'' name=''ReminderID[]'' value=''', a.ReminderID ,'''>')
         FROM
            CARD_STUDENT_REMINDER as a ";
if ($lc->EnableEntryLeavePeriod) {
	$sql .= "
					INNER JOIN 
          CARD_STUDENT_ENTRY_LEAVE_PERIOD as selp 
          on a.StudentID = selp.UserID 
          	AND 
          	a.DateOfReminder between selp.PeriodStart and CONCAT(selp.PeriodEnd,' 23:59:59') ";
}
$sql .= "
            LEFT OUTER JOIN 
            INTRANET_USER as b 
            ON a.StudentID = b.UserID AND b.RecordType = 2 AND b.RecordStatus IN(0,1,2)
						LEFT OUTER JOIN 
						INTRANET_USER as c 
						ON a.TeacherID = c.UserID AND c.RecordType = 1 AND c.RecordStatus = 1
         WHERE
              b.UserID IS NOT NULL AND c.UserID IS NOT NULL AND
              (b.ChineseName like '%$keyword%'
               OR b.EnglishName like '%$keyword%'
               )
               $conds
                ";

//debug_r($sql);
# TABLE INFO
$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("a.DateOfReminder","b.EnglishName","c.EnglishName","a.Reason");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;
$li->title = "";
$li->column_array = array(0,0,0,0);
$li->wrap_array = array(0,0,0,0);
$li->IsColOff = 2;

// TABLE COLUMN
$pos = 0;
$li->column_list .= "<td width='1' class='tabletoplink'>#</td>\n";
$li->column_list .= "<td width='25%'>".$li->column($pos++, $i_StudentAttendance_Reminder_Date)."</td>\n";
$li->column_list .= "<td width='25%'>".$li->column($pos++, $i_UserStudentName)."</td>\n";
$li->column_list .= "<td width='25%'>".$li->column($pos++, $i_StudentAttendance_Reminder_Teacher)."</td>\n";
$li->column_list .= "<td width='25%'>".$li->column($pos++, $i_StudentAttendance_Reminder_Reason)."</td>\n";
$li->column_list .= "<td width='1'>".$li->check("ReminderID[]")."</td>\n";

$firstname_status = "$i_status_all";
$array_status_name = array($i_StudentAttendance_Reminder_Status_Coming, $i_StudentAttendance_Reminder_Status_Past);
$array_status_data = array("1", "2");
$select_status = getSelectByValueDiffName($array_status_data,$array_status_name,"name='filter' onChange='this.form.submit()'",$filter,1,0, $firstname_status);

$filterbar = $select_status;

$toolbar = $linterface->GET_LNK_NEW("javascript:checkNew('new.php')","","","","",0);
$toolbar .= $linterface->GET_LNK_IMPORT("javascript:checkNew('import.php')","","","","",0);
$toolbar .= $linterface->GET_LNK_EXPORT("javascript:checkGet(document.form1,'export.php')","","","","",0);

$table_tool .= "<td nowrap=\"nowrap\">
					<a href=\"javascript:checkRemove(document.form1,'ReminderID[]','remove.php')\" class=\"tabletool\">
						<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_delete.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">
						$button_remove
					</a>
				</td>";
$table_tool .= "<td><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"5\"></td>";
$table_tool .= "<td nowrap=\"nowrap\">
					<a href=\"javascript:checkEdit(document.form1,'ReminderID[]','edit.php')\" class=\"tabletool\">
						<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_edit.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">
						$button_edit
					</a>
				</td>";

$searchbar = "<input class=\"textboxnum\" type=\"text\" name=\"keyword\" maxlength=\"50\" value=\"".stripslashes($keyword)."\" onFocus=\"this.form.pageNo.value=1;\">\n";
$searchbar .= $linterface->GET_SMALL_BTN($button_find, "button", "document.form1.action = 'index.php'; document.form1.submit();","submit2");

$TAGS_OBJ[] = array($i_StudentAttendance_Reminder, "", 0);

$MODULE_OBJ = $lc->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START(urldecode($Msg));

if ($msg == 1) $SysMsg = $linterface->GET_SYS_MSG("add");
if ($msg == 2) $SysMsg = $linterface->GET_SYS_MSG("update");
if ($msg == 3) $SysMsg = $linterface->GET_SYS_MSG("delete");
?>
<br />
<form name="form1" method="get">
<table width="95%" border="0" cellpadding="3" cellspacing="0" align="center">
	<tr>
		<td align="left"><?=$toolbar?></td>
		<td align="right"><?=$SysMsg?></td>
	</tr>
</table>
<table width="95%" border="0" cellpadding="0" cellspacing="0" align="center">
	<tr class="table-action-bar">
		<td align="left"><?=$filterbar?><?=$searchbar?></td>
		<td valign="bottom" align="right">
			<table border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="21"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_01.gif" width="21" height="23"></td>
					<td background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_02.gif">
						<table border="0" cellspacing="0" cellpadding="2">
							<tr>
								<?=$table_tool?>
							</tr>
						</table>
					</td>
					<td width="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_03.gif" width="6" height="23"></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<?php echo $li->display("95%"); ?>
<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>">
<input type="hidden" name="order" value="<?php echo $li->order; ?>">
<input type="hidden" name="field" value="<?php echo $li->field; ?>">
<input type="hidden" name="page_size_change" value="">
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>">
</form>
<br />
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>