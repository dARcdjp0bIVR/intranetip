<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();

$li = new libdb();

if ($filter == 1)
{
    $conds .= " AND a.DateOfReminder >= CURDATE()";
}
else if ($filter == 2)
{
     $conds .= " AND a.DateOfReminder < CURDATE()";
}

$sql = "SELECT b.ClassName, b.ClassNumber, a.DateOfReminder, c.UserLogin, a.Reason
        FROM 
        	CARD_STUDENT_REMINDER as a ";
if ($lc->EnableEntryLeavePeriod) {
	$sql .= "
					INNER JOIN 
          CARD_STUDENT_ENTRY_LEAVE_PERIOD as selp 
          on a.StudentID = selp.UserID 
          	AND 
          	a.DateOfReminder between selp.PeriodStart and CONCAT(selp.PeriodEnd,' 23:59:59') ";
}
$sql .= "
					LEFT OUTER JOIN INTRANET_USER as b
					    ON a.StudentID = b.UserID AND b.RecordType = 2 AND b.RecordStatus IN (0,1,2)
					LEFT OUTER JOIN INTRANET_USER as c
					    ON a.TeacherID = c.UserID AND c.RecordType = 1 AND c.RecordStatus = 1
         WHERE
              b.UserID IS NOT NULL AND c.UserID IS NOT NULL AND
              (b.ChineseName like '%$keyword%'
               OR b.EnglishName like '%$keyword%'
               )
               $conds
                  ";
                  
$result = $li->returnArray($sql,5);

$lexport = new libexporttext();

$exportColumn = array("ClassName", "ClassNumber", "Date", "Teacher", "Reason");

$export_content = $lexport->GET_EXPORT_TXT($result, $exportColumn);

// Output the file to user browser
$filename = "reminders.csv";

intranet_closedb();
$lexport->EXPORT_FILE($filename, $export_content);
?>