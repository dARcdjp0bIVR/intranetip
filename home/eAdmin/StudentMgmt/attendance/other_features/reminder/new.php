<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lcard = new libcardstudentattend2();
$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageOtherFeatures_Reminder";

$linterface = new interface_html();

/*
$student = $_POST['student'];
$flag = $_POST['flag'];
$targetClass = $_POST['targetClass'];
$targetNum = $_POST['targetNum'];
$student_login = $_POST['student_login'];
*/

# Class selection
if ($flag != 2)
{
    $targetClass = "";
}
$lc = new libclass();
$select_class = $lc->getSelectClass("name=\"targetClass\" onChange=\"changeClass(this.form)\"", $targetClass);

if ($flag==2 && $targetClass != "")
{
    # Get Class Num
    $target_class_name = $targetClass;
    $sql = "SELECT DISTINCT ClassNumber FROM INTRANET_USER WHERE RecordType = 2 AND RecordStatus IN (1) AND ClassName = '$target_class_name' ORDER BY ClassNumber";
    $classnum = $lc->returnVector($sql);
    $select_classnum = getSelectByValue($classnum, "name=\"targetNum\"",$targetNum);
    $select_classnum .= $linterface->GET_BTN($button_add, "button", "addByClassNum()");
}

if ($flag == 1 && $student_login != '')           # Login
{
    $sql = "SELECT UserID FROM INTRANET_USER WHERE RecordType = 2 AND RecordStatus IN (1) AND UserLogin = '$student_login'";
    $temp = $lc->returnVector($sql);
    $target_new_id = $temp[0];
    if ($target_new_id != "")
    {
        $student[] = $target_new_id;
    }
}
else if ($flag == 2 && $target_class_name != '' && $targetNum != '')
{
     $sql = "SELECT UserID FROM INTRANET_USER WHERE RecordType = 2 AND ClassName = '$target_class_name' AND ClassNumber = '$targetNum'";
     $temp = $lc->returnVector($sql);
     $target_new_id = $temp[0];
    if ($target_new_id != "")
    {
        $student[] = $target_new_id;
    }
}

$allow_item_number=false;  // allow user to input item numbers

# Last selection
if (is_array($student) && sizeof($student)>0)
{
    $list = implode(",", $student);
    $namefield = getNameFieldWithClassNumberByLang();
    $sql = "SELECT UserID , $namefield FROM INTRANET_USER WHERE RecordType = 2 AND UserID IN ($list)";
    $array_students = $lc->returnArray($sql,2);
    if(sizeof($student)==1)
   		$allow_item_number=true;
}

if($allow_item_number){
	$select_item_num = "<select name=itemcount>";
	for($i=1;$i<=20;$i++){
		$select_item_num .="<option value=$i>$i</option>";
	}
	$select_item_num .= "</select>";
}

$TAGS_OBJ[] = array($i_StudentAttendance_Reminder, "", 0);

$PAGE_NAVIGATION[] = array($button_new);

$MODULE_OBJ = $lcard->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

if ($msg == 1) $SysMsg = $linterface->GET_SYS_MSG("add");
if ($msg == 2) $SysMsg = $linterface->GET_SYS_MSG("update");
if ($msg == 3) $SysMsg = $linterface->GET_SYS_MSG("delete");
?>
<script language="Javascript">
function addByLogin()
{
         obj = document.form1;
         obj.flag.value = 1;
         generalFormSubmitCheck(obj);
}
function removeStudent(){
		checkOptionRemove(document.form1.elements["student[]"]);
 		submitForm();
}
function submitForm(){
         obj = document.form1;
         obj.flag.value =0;
         generalFormSubmitCheck(obj);
}
function changeClass(obj)
{
         obj.flag.value = 2;
         if (obj.targetNum != undefined)
             obj.targetNum.selectedIndex = 0;
         generalFormSubmitCheck(obj);
}
function addByClassNum()
{
         obj = document.form1;
         obj.flag.value = 2;
         generalFormSubmitCheck(obj);
}
function finishSelection()
{
         obj = document.form1;
         obj.action = 'new2.php';
         checkOptionAll(obj.elements["student[]"]);
         obj.submit();
         return true;
}
function generalFormSubmitCheck(obj)
{
         checkOptionAll(obj.elements["student[]"]);
         obj.submit();
}
function formSubmit(obj)
{
         if (obj.flag.value == 0)
         {
             obj.flag.value = 1;
             generalFormSubmitCheck(obj);
             return true;
         }
         else
         {
             return finishSelection();
         }
}
function checkForm()
{
        obj = document.form1;

        if(obj.elements["student[]"].length != 0)
                return formSubmit(obj);
        else
        {
                alert('<?=$i_Discipline_System_alert_PleaseSelectStudent?>');
                return false;
        }
}

function openTarget(form, features, windowName) {
	if (!windowName)
		windowName = "formTarget" + (new Date().getTime());
	form.target = windowName;
	open ("", windowName, features);
}
</script>
<br />
<form name="form1" action="" method="POST" onsubmit="return checkForm();">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	</tr>
	<tr><td>&nbsp;</td></tr>
<table>
<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
			<?=$i_general_students_selected?> <span class="tabletextrequire">*</span>
		</td>
		<td width="70%" class="tabletext">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="1">
						<select name="student[]" size="5" multiple>
						<?
						for ($i=0; $i<sizeof($array_students); $i++)
						{
						     list ($id, $name) = $array_students[$i];
						?>
						<option value="<?=$id?>"><?=$name?></option>
						<?
						}
						?>
						</select>
					</td>
					<td align="left" valign="bottom">
						&nbsp;<?= $linterface->GET_BTN($button_select, "button", "newWindow('choose/index.php?fieldname=student[]', 9)") ?><br />
						&nbsp;<?= $linterface->GET_BTN($button_remove, "button", "removeStudent()") ?>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="tabletext">&nbsp;</td>
		<td width="70%" class="tablerow2 tabletext">
			<span class="tabletextremark">(<?=$i_general_alternative?>)</span><br />
			<?=$i_UserLogin?><br />
			<input type="text" name="student_login" class="textboxnum" maxlength="100">
			<?= $linterface->GET_BTN($button_add, "button", "addByLogin()") ?><br />
			<?=$i_ClassNameNumber?><br />
			<?=$select_class?><?=$select_classnum?>
		</td>
	</tr>
	<tr>
		<td class="tabletextremark">
			<br /><?=$i_general_required_field?>
		</td>
	</tr>
</table>
<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
    	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
    </tr>
</table>
<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
	    <td align="center" colspan="2">
			<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "this.form.flag.value=3") ?>
			<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location = 'index.php';","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
		</td>
	</tr>
</table>
<input type="hidden" name="flag" value="0">
<input type="hidden" name="type" value="<?=$type?>">
</form>
<br />
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>