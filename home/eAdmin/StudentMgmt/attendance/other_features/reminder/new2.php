<?
# using by: yat

###################################################
#
#	Date:	2012-12-11	YatWoon
#			improved: allow select non-teaching staff [Case#2012-1128-1829-53071]
#
###################################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();
$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageOtherFeatures_Reminder";

$linterface = new interface_html();

$lclass = new libclass();
$x = "";

# Print Out The Selected Student
if(($student_list == '') && (sizeof($student)>0))
{
	$student_list = implode(",",$student);
}
$namefield = getNameFieldWithClassNumberByLang();
$sql = "SELECT UserID, $namefield FROM INTRANET_USER WHERE UserID IN ($student_list)";
$result = $lclass->returnArray($sql,2);
if(sizeof($result)>0)
{
	for($i=0; $i<sizeof($result); $i++)
	{
		list ($user_id, $student_name) = $result[$i];
		$x .= $student_name."<br />";
	}
}

# Generate Teacher Selection Box
$namefield = getNameFieldByLang();
$sql = "SELECT UserID, $namefield, Teaching FROM INTRANET_USER WHERE RecordType = 1 and RecordStatus = 1 order by EnglishName";
$temp = $lclass->returnArray($sql);
// $select_teacher = getSelectByArray($temp, "name=\"TargetTeacher\"", $TargetTeacher);

foreach($temp as $k=>$d)
{
	$t_type = $d['Teaching'] ==1 ? $Lang['Identity']['TeachingStaff'] : $Lang['Identity']['NonTeachingStaff'];
	
	$t[$t_type][$d[0]] = $d[1];
}
$select_teacher = getSelectByAssoArray($t, "name=\"TargetTeacher\"", $TargetTeacher);

$repeat_type = array(
	array(0,"$i_StudentAttendance_Reminder_RepeatSelection[0]"), #No Repeat
	array(1,"$i_StudentAttendance_Reminder_RepeatSelection[1]"), #Daily
	array(2,"$i_StudentAttendance_Reminder_RepeatSelection[2]"), #Weekly Day
	array(3,"$i_StudentAttendance_Reminder_RepeatSelection[3]") #Cycle Day
);
$repeat = getSelectByArray($repeat_type,"name=\"repeat_status\" onChange=\"this.form.submit(); this.form.flag.value=1\"",$repeat_status,0,1);

if(($repeat_status == 0)||($repeat_status == ''))
{
	$repeat_status = "";
}

if(($repeat_status != 0)||($repeat_status != ''))
{
	$repeat_selection = "<tr><td width=\"30%\" valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_StudentAttendance_Reminder_FinishDate</td>";
	$repeat_selection .= "<td width=\"70%\"class=\"tabletext\">".$linterface->GET_DATE_PICKER("ReminderEndDate", $ReminderEndDate);
	$repeat_selection .= "</td></tr>";
	
	if($repeat_status == 2)
	{
		$days = array(0,1,2,3,4,5,6);
		$repeat_selection .= "<tr><td width=\"30%\" valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_StudentAttendance_Reminder_WeekdaySelection</td>";
		$repeat_selection .= "<td width=\"70%\" class=\"tabletext\"><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
	    for ($i=0; $i<sizeof($days); $i++)
	    {
	         $word = $i_DayType0[$days[$i]];
	         $repeat_selection .= "<td class=\"tabletext\"><input type=\"checkbox\" name=\"DayValue[]\" id=\"Day$i\" value=\"$i\">";
	         $repeat_selection .= "<label for=\"Day$i\">".$word."</label>&nbsp;</td>";
	         if($i==3)
	         	$repeat_selection .= "</tr><tr>";
	    }
	    $repeat_selection .= "</tr></table></td></tr>";
    }
	
	if($repeat_status == 3)
	{
		$sql = "SELECT DISTINCT TextShort FROM INTRANET_CYCLE_DAYS";
		$temp = $lclass->returnArray($sql,1);
		if(sizeof($temp)>0)
		{
			$repeat_selection .= "<tr><td width=\"30%\" valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_StudentAttendance_Reminder_CycledaySelection</td>";
			$repeat_selection .= "<td width=\"70%\"class=\"tabletext\">";
			for($i=0; $i<sizeof($temp); $i++)
			{
				list($cycle_day) = $temp[$i];
				$repeat_selection .= $cycle_day."<input type=\"checkbox\" name=\"DayValue[]\" value=\"$cycle_day\">&nbsp;&nbsp;&nbsp;";
			}
			$repeat_selection .= "</td></tr>";
		}
	}
}


$TAGS_OBJ[] = array($i_StudentAttendance_Reminder, "", 0);

$PAGE_NAVIGATION[] = array($button_new);

$MODULE_OBJ = $lc->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

if ($msg == 1) $SysMsg = $linterface->GET_SYS_MSG("add");
if ($msg == 2) $SysMsg = $linterface->GET_SYS_MSG("update");
if ($msg == 3) $SysMsg = $linterface->GET_SYS_MSG("delete");
?>
<script Language="JavaScript">
function checkForm()
{
	var obj = document.form1;
	var temp = document.form1['DayValue[]'];
	var checking = 0;
	
	if(obj.flag.value==0)
	{
		<? 
		if($repeat_status!=0) 
		{
		?>
			if(obj.ReminderStartDate.value!="" && obj.ReminderStartDate.value!="yyyy-mm-dd") 
			{
				if(obj.ReminderEndDate.value!="" && obj.ReminderEndDate.value!="yyyy-mm-dd")
				{
					if(obj.ReminderEndDate.value > obj.ReminderStartDate.value)
					{
						<?if(($repeat_status==2) || ($repeat_status==3)){?>
							for (i=0; i<temp.length; i++)
							{
								if(temp[i].checked)
									checking = 1;
							}
							if(checking == 1)
							{
								if(obj.TargetTeacher.value != "")
								{
									if(obj.Reason.value != "")
									{
										obj.action = "new_update.php";
										obj.submit();
									}
									else
									{
										alert("<?=$i_StudentAttendance_Reminder_InputReasonWarning?>");
										return false;
									}
								}
								else
								{
									alert("<?=$i_StudentAttendance_Reminder_TeacherSelectionWarning?>");
									return false;
								}
							}
							else
							{
								<?if($repeat_status==2){?>
									alert ("<?=$i_StudentAttendance_Reminder_WeekdaySelectionWarning?>");
									return false;
								<?}?>
								<?if($repeat_status==3){?>
									alert ("<?=$i_StudentAttendance_Reminder_CycleDaySelectionWarning?>");
									return false;
								<?}?>
							}
							<?
							}
							else
							{
							?>
							if(obj.TargetTeacher.value != "")
							{
								if(obj.Reason.value != "")
								{
									obj.action = "new_update.php";
									obj.submit();
								}
								else
								{
									alert("<?=$i_StudentAttendance_Reminder_InputReasonWarning?>");
									return false;
								}
							}
							else
							{
								alert("<?=$i_StudentAttendance_Reminder_TeacherSelectionWarning?>");
								return false;
							}
							<?
							}
							?>
					}
					else
					{
						alert("<?=$i_StudentAttendance_Reminder_WrongDateWarning?>");
						return false;
					}
				}
				else
				{
					alert("<?=$i_StudentAttendance_Reminder_FinishDateEmptyWarning?>");
					return false;
				}
			}
			else
			{
				alert("<?=$i_StudentAttendance_Reminder_StartDateEmptyWarning?>");
				return false;
			}
		<?
		}
		else
		{
		?>
			if(obj.ReminderStartDate.value!="" && obj.ReminderStartDate.value!="yyyy-mm-dd")
			{
				obj.action = "new_update.php";
				obj.submit();
			}
			else
			{
				alert("<?=$i_StudentAttendance_Reminder_StartDateEmptyWarning?>");
				return false;
			}
		<?
		}
		?>
	}
}
</script>
<br />
<form name="form1" action="" method="POST" onSubmit="checkForm()">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	</tr>
	<tr><td>&nbsp;</td></tr>
<table>
<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_UserStudentName?></td>
		<td width="70%" class="tabletext"><?=$x?></td>
	</tr>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_StudentAttendance_Reminder_RepeatFrequency?></td>
		<td width="70%" class="tabletext"><?=$repeat?></td>
	</tr>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_StudentAttendance_Reminder_StartDate?></td>
		<td width="70%" class="tabletext"><?=$linterface->GET_DATE_PICKER("ReminderStartDate", $ReminderStartDate);?></td>
	</tr>
	<?=$repeat_selection?>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_StudentAttendance_Reminder_Teacher?></td>
		<td width="70%" class="tabletext"><?=$select_teacher?></td>
	</tr>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_StudentAttendance_Reminder_Reason?></td>
		<td width="70%" class="tabletext"><?=$linterface->GET_TEXTAREA("Reason", $Reason)?></td>
	</tr>
</table>
<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
    	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
    </tr>
</table>
<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
	    <td align="center" colspan="2">
			<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "") ?>
			<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location = 'new.php'","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
		</td>
	</tr>
</table>
<input type="hidden" name="student_list" value="<?=$student_list?>">
<input type="hidden" name="flag" value="0">
</form>
<br />
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>