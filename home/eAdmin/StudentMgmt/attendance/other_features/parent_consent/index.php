<?php
// Editing by 
/*
 * 2017-07-07 (Carlos): created.
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent'] || !$sys_custom['eClassApp']['HKUSPH']) {
	intranet_closedb();
	header ("Location: /");
	exit();
}

# set cookies
$arrCookies[] = array("ck_page_size", "numPerPage");
$arrCookies[] = array("ck_page_no", "pageNo");
$arrCookies[] = array("ck_page_order", "order");
$arrCookies[] = array("ck_page_field", "field");

$arrCookies[] = array("ck_parentconsent_targetclass", "TargetClass");
$arrCookies[] = array("ck_parentconsent_consentstatus", "ConsentStatus");
$arrCookies[] = array("ck_parentconsent_keyword", "Keyword");
if(isset($clearCoo) && $clearCoo == 1)
{
	clearCookies($arrCookies);
} 
else 
{
	updateGetCookies($arrCookies);
}

$lc = new libcardstudentattend2();
$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageOtherFeatures_HKUSPHParentConsentRecords";

$linterface = new interface_html();

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
if($order == "") $order = 1;
if($field == "") $field = 2;
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;


$li = new libdbtable2007($field, $order, $pageNo);

$filterMap = array('GetDBQuery'=>1,'libdbtable'=>$li);

$AcademicYearID = Get_Current_Academic_Year_ID();
$filterMap['AcademicYearID'] = $AcademicYearID;
if(isset($TargetClass) && $TargetClass != '' && $TargetClass != '0'){
	if(strpos($TargetClass,'::')!==false){
		$filterMap['YearClassID'] = str_replace('::','', $TargetClass);
	}else{
		$filterMap['YearID'] = $TargetClass;
	}
}
if(isset($ConsentStatus) && $ConsentStatus != ''){
	$filterMap['ConsentStatus'] = $ConsentStatus;
}
if(isset($Keyword) && trim($Keyword)!=''){
	$filterMap['Keyword'] = $Keyword;
}
$db_query_info = $lc->getHKUSPHParentConsentRecords($filterMap);

$li->field_array = $db_query_info[0];
$li->sql = $db_query_info[1];
$li->no_col = sizeof($li->field_array)+2;
$li->title = "";
$li->column_array = $db_query_info[3];
$li->wrap_array = $db_query_info[4];
$li->IsColOff = "IP25_table";
if($field == 2){
	$li->fieldorder2 = ",".$db_query_info[0][$field+1];
}

foreach($db_query_info[2] as $column_def)
{
	$li->column_list .= $column_def;
}

$toolbar .= $linterface->GET_LNK_IMPORT("import.php",$Lang['Btn']['Import'],"","","",0);
$toolbar .= $linterface->GET_LNK_EXPORT("javascript:GoExport()",$Lang['Btn']['Export'],"","","",0);

$target_class_selection = $lc->getSelectClassWithWholeForm("name=\"TargetClass\" onchange=\"document.form1.submit();\"", $TargetClass, $Lang['AccountMgmt']['AllClassesParent'], "");

$consent_status_ary = array(
						array('',$Lang['StudentAttendance']['AllConsentStatus']),
						array('1',$Lang['StudentAttendance']['Agreed']),
						array('0',$Lang['StudentAttendance']['Denied']),
						array('-1',$Lang['StudentAttendance']['Unsigned']));
$consent_status_selection = $linterface->GET_SELECTION_BOX($consent_status_ary, ' name="ConsentStatus" id="ConsentStatus" onchange="document.form1.submit();" ', '', $ConsentStatus);

$tool_buttons = array();
$tool_buttons[] = array('set',"javascript:checkEditMultiple(document.form1,'RecordID[]','update_consent_status.php?agree=1')",$Lang['StudentAttendance']['SetAsAgreed']);
$tool_buttons[] = array('set',"javascript:checkEditMultiple(document.form1,'RecordID[]','update_consent_status.php?agree=0')",$Lang['StudentAttendance']['SetAsDenied']);
$tool_buttons[] = array('set',"javascript:checkEditMultiple(document.form1,'RecordID[]','update_consent_status.php?agree=-1')",$Lang['StudentAttendance']['SetAsUnsigned']);

$TAGS_OBJ[] = array($Lang['StudentAttendance']['HKUSPHParentConsentRecords'],'', 1);

if(isset($_SESSION['STUDENT_ATTENDANCE_PARENT_CONSENT_RETURN_MSG']))
{
	$ReturnMsg = $_SESSION['STUDENT_ATTENDANCE_PARENT_CONSENT_RETURN_MSG'];
	unset($_SESSION['STUDENT_ATTENDANCE_PARENT_CONSENT_RETURN_MSG']);
}
if($xmsg != '' &&  isset($Lang['General']['ReturnMessage'][$xmsg]))
{
	$ReturnMsg = $Lang['General']['ReturnMessage'][$xmsg];
}
$MODULE_OBJ = $lc->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START($ReturnMsg);
?>
<br />
<form name="form1" method="post" action="index.php">
<div class="content_top_tool">
	<?=$toolbar?>
	<?=$linterface->Get_Search_Box_Div("Keyword", trim($lc->is_magic_quotes_active? stripslashes($Keyword): $Keyword), ' onkeyup="GoSearch(event);" ')?>
	<br style="clear:both" />
</div>
<div class="table_board">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr class="table-action-bar">
	<td valign="bottom">
		<?=$target_class_selection.'&nbsp;'.$consent_status_selection?>
	</td>
	<td valign="bottom">
		<?=$linterface->Get_DBTable_Action_Button_IP25($tool_buttons)?>
	</td>
</tr>
</table>
<?= $li->display()?>
<br style="clear:both" />
<input type="hidden" name="pageNo" id="pageNo" value="<?=$li->pageNo; ?>" />
<input type="hidden" name="order" id="order" value="<?=$li->order; ?>" />
<input type="hidden" name="field" id="field" value="<?=$li->field; ?>" />
<input type="hidden" name="page_size_change" id="page_size_change" value="" />
<input type="hidden" name="numPerPage" id="numPerPage" value="<?=$li->page_size?>" />
</div>
</form>
<script type="text/javascript" language="javascript">
function GoSearch(evt)
{
	var key = evt.which || evt.charCode || evt.keyCode;
	
	if (key == 13) // enter
		document.form1.submit();
	else
		return false;
}

function GoExport()
{
	var form_obj = document.form1;
	var old_action = form_obj.action;
	var old_target = form_obj.target;
	form_obj.action = 'export.php';
	form_obj.target = '_blank';
	form_obj.submit();
	form_obj.action = old_action;
	form_obj.target = old_target;
}
</script>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>