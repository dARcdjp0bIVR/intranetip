<?php
// Editing by 
/*
 * 2017-09-08 (Carlos): For eNotice exported csv, use Signer name as parent name key to match intranet user record. Consent time also accept format DD/MM/YYYY hh:mm:ss.
 * 2017-07-07 (Carlos): created.
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent'] || !$sys_custom['eClassApp']['HKUSPH']) {
	intranet_closedb();
	header ("Location: /");
	exit();
}

$limport = new libimporttext();
$lfs = new libfilesystem();
$lc = new libcardstudentattend2();
$linterface = new interface_html();

if(!isset($step) && $step == '' && !in_array($step,array(0,1,2))){
	$step = 0;
}

if((!isset($type) && $type == '') || !in_array($type,array(1,2))){
	$type = 2;
}

$hidden_fields = '';

// Step 1 - Upload csv file
if($step == 0 || $step == '')
{	
	$sample_csv_link = '';
	$sample_csv_link.= '<a class="tablelink general_type" href="sample.php?type=1" '.($type!=2?'':'style="display:none"').'>'.$Lang['General']['ClickHereToDownloadSample'].'</a>';
	$sample_csv_link.= '<a class="tablelink notice_type" href="sample.php?type=2" '.($type==2?'':'style="display:none"').'>'.$Lang['General']['ClickHereToDownloadSample'].'</a>';
	$csv_format = '';
	$csv_format.= '<div class="general_type" '.($type!=2?'':'style="display:none"').'>';
	for($i=0;$i<count($Lang['StudentAttendance']['HKUSPHParentConsentImportColumns']);$i++){
		$csv_format .= $Lang['General']['ImportArr']['Column'].' '.($i+1).' : '.($Lang['StudentAttendance']['HKUSPHParentConsentImportColumns'][$i][2]?'<span class="red">*</span>':''). $Lang['StudentAttendance']['HKUSPHParentConsentImportColumns'][$i][0].' - '.$Lang['StudentAttendance']['HKUSPHParentConsentImportColumns'][$i][1];
		if($Lang['StudentAttendance']['HKUSPHParentConsentImportColumns'][$i][3] != ''){
			$csv_format .= $Lang['StudentAttendance']['HKUSPHParentConsentImportColumns'][$i][3];
		}
		$csv_format .= '<br />'."\n";
	}
	$csv_format.= '</div>';
	$csv_format.='<div class="notice_type" '.($type==2?'':'style="display:none"').'>';
	for($i=0;$i<count($Lang['StudentAttendance']['HKUSPHParentConsentNoticeImportColumns']);$i++){
		$csv_format .= $Lang['General']['ImportArr']['Column'].' '.($i+1).' : '.($Lang['StudentAttendance']['HKUSPHParentConsentNoticeImportColumns'][$i][2]?'<span class="red">*</span>':''). $Lang['StudentAttendance']['HKUSPHParentConsentNoticeImportColumns'][$i][0].' - '.$Lang['StudentAttendance']['HKUSPHParentConsentNoticeImportColumns'][$i][1];
		if($Lang['StudentAttendance']['HKUSPHParentConsentNoticeImportColumns'][$i][3] != ''){
			$csv_format .= $Lang['StudentAttendance']['HKUSPHParentConsentNoticeImportColumns'][$i][3];
		}
		$csv_format .= '<br />'."\n";
	}
	$csv_format.='</div>';
}

// Step 2 - Check data and confirm
if($step == 1)
{
	### CSV Checking
	$name = $_FILES['userfile']['name'];
	$ext = strtoupper($lfs->file_ext($name));
	
	if(!($ext == ".CSV" || $ext == ".TXT"))
	{
		$_SESSION['STUDENT_ATTENDANCE_PARENT_CONSENT_RETURN_MSG'] = $Lang['General']['ReturnMessage']['WrongFileFormat'];
		intranet_closedb();
		header("Location: ".$_SERVER['SCRIPT_NAME']);
		exit();
	}
	$header_map = $lc->getHKUSPHParentConsentCsvHeaderMap($type);
	$file_format = array_keys($header_map);
	$data = $limport->GET_IMPORT_TXT($userfile);
	$csv_header = array_shift($data);
	$data_size = count($data);
	
	# check csv header
	$format_wrong = false;
	for($i=0; $i<sizeof($file_format); $i++)
	{
		if ($csv_header[$i]!=$file_format[$i])
		{
			$format_wrong = true;
			break;
		}
	}
	
	if($format_wrong)
	{
		$_SESSION['STUDENT_ATTENDANCE_PARENT_CONSENT_RETURN_MSG'] = $Lang['General']['ReturnMessage']['WrongCSVHeader'];
		intranet_closedb();
		header("Location: ".$_SERVER['SCRIPT_NAME']);
		exit();
	}
	if(empty($data))
	{
		$_SESSION['STUDENT_ATTENDANCE_PARENT_CONSENT_RETURN_MSG'] = $Lang['General']['ReturnMessage']['CSVFileNoData'];
		intranet_closedb();
		header("Location: ".$_SERVER['SCRIPT_NAME']);
		exit();
	}
	
	$data_ary = array(); // valid data rows
	$error_ary = array(); // line number to error msg
	//$added_ary = array(); // added parent student record array
	
	$AcademicYearID = Get_Current_Academic_Year_ID();
	$records = $lc->getHKUSPHParentConsentRecords(array('AcademicYearID'=>$AcademicYearID));
	$record_size = count($records);
	
	if($type == 2) // eNotice exported format
	{
		$consent_keywords = array("1"=>"同意","0"=>"不同意");
		$classInfoParentNameRecords = array(); // [ClassName][ClassNumber][ParentName]
		for($i=0;$i<$record_size;$i++){
			$class_name = $records[$i]['ClassName'];
			$class_number = $records[$i]['ClassNumber'];
			$parent_name = $records[$i]['ParentName'];
			
			if(!isset($classInfoParentNameRecords[$class_name])){
				$classInfoParentNameRecords[$class_name] = array();
			}
			if(!isset($classInfoParentNameRecords[$class_name][$class_number])){
				$classInfoParentNameRecords[$class_name][$class_number] = array();
			}
			if(!isset($classInfoParentNameRecords[$class_name][$class_number][$parent_name])){
				$classInfoParentNameRecords[$class_name][$class_number][$parent_name] = $records[$i];
			}
		}
		
		for($i=0;$i<$data_size;$i++){
			list($class_name,$class_number,$student_name,$consent,$parent_name,$signer,$consent_time) = $data[$i];
			$line = $i+2;
			$errors = array();
			$class_name = trim($class_name);
			$class_number = trim($class_number);
			$parent_name = trim($parent_name);
			$signer = trim($signer);
			$parent_name_key = $signer;
			$consent = trim($consent);
			$consent_time = trim($consent_time);
			
			if($parent_name == ''){
				$parent_name = $signer;
			}
			if($class_name == ''){
				$errors[] = $Lang['StudentAttendance']['HKUSPHParentConsentImportErrors']['BlankClass'];
			}
			if($class_number == ''){
				$errors[] = $Lang['StudentAttendance']['HKUSPHParentConsentImportErrors']['BlankClassNumber'];
			}
			if($signer == ''){
				$errors[] = $Lang['StudentAttendance']['HKUSPHParentConsentImportErrors']['BlankSignatureName'];
			}
			if($class_name != '' && $class_number != ''){
				if(!isset($classInfoParentNameRecords[$class_name][$class_number])){
					$errors[] = str_replace(array('<!--CLASSNAME-->','<!--CLASSNUMBER-->'),array($class_name,$class_number), $Lang['StudentAttendance']['HKUSPHParentConsentImportErrors']['CannotFoundStudentWithClassClassNumber']);
				}else 
				{
					$parent_names = array_keys($classInfoParentNameRecords[$class_name][$class_number]);
					$match_parent_name = isset($classInfoParentNameRecords[$class_name][$class_number][$signer]);
					if(!$match_parent_name){
						for($j=0;$j<count($parent_names);$j++){
							if(strpos($parent_names[$i],$signer)!==false){
								$match_parent_name = true;
								$parent_name_key = $parent_names[$i];
							}
							if(strpos($signer,$parent_names[$i])!==false){
								$match_parent_name = true;
								$parent_name_key = $parent_names[$i];
							}
						}
					}
					if(!$match_parent_name){
						$errors[] = str_replace('<!--PARENTNAME-->',$signer,$Lang['StudentAttendance']['HKUSPHParentConsentImportErrors']['CannotFoundParent']);
					}
				}
			}
			if($consent == '' || (strpos($consent,$consent_keywords["1"])===false && strpos($consent,$consent_keywords["0"])===false) ){
				$errors[] = $Lang['StudentAttendance']['HKUSPHParentConsentImportErrors']['ConsentValueNoKeyword'];
			}
			if(preg_match('/^(\d{1,2})\/(\d{1,2})\/(\d\d\d\d)\s(\d{1,2}):(\d{1,2})(:\d{1,2})*$/',$consent_time,$matches)){
				$consent_time = sprintf('%04d-%02d-%02d %02d:%02d:%02d', $matches[3], $matches[2], $matches[1], $matches[4], $matches[5], str_replace(':','',$matches[6]));
			}
			$ts = strtotime($consent_time);
			$datetime_str = date("Y-m-d H:i:s",$ts);
			if(!(preg_match('/^\d\d\d\d-\d\d-\d\d\s\d\d:\d\d:\d\d$/',$consent_time) || preg_match('/^\d{1,2}\/\d{1,2}\/\d\d\d\d\s\d{1,2}:\d{1,2}(:\d{1,2})*$/',$consent_time)) || $consent_time != $datetime_str){
				$errors[] = $Lang['StudentAttendance']['HKUSPHParentConsentImportErrors']['InvalidConsentTime'];
			}
			
			if(count($errors)>0){
				$error_ary[$line] = $errors;
			}else{
				$valid_row = $classInfoParentNameRecords[$class_name][$class_number][$parent_name_key];
				$valid_row['IsAgreed'] = strpos($consent,$consent_keywords["0"])!==false ? "0" : "1";
				$valid_row['SignDate'] = $consent_time;
				$data_ary[] = $valid_row;
			}
			
			$rowcss = " class=\"".($line %2 == 0? "tablebluerow2":"tablebluerow1")."\" ";
			$x .= "<tr $rowcss>";
			$x .= "<td class=\"tabletext\" valign=\"top\">".($line)."</td>";
			foreach($data[$i] as $val){
				$x .= "<td class=\"tabletext\" valign=\"top\">".($val)."</td>";
			}
			$x .= "<td class=\"tabletext red\">";
			$x .= isset($error_ary[$line]) ? implode("<br />",$error_ary[$line]) : "&nbsp;";
			$x .= "</td>";
			$x .= "</tr>";
		}
		
	}else // General format
	{
		$parentLoginStudentLoginRecords = array(); // [ParentUserLogin][StudentUserLogin]
		$studentLoginParentLoginRecords = array(); // [StudentUserLogin][ParentUserLogin]
		$consent_valid_values = array("1","0","");
		
		for($i=0;$i<$record_size;$i++){
			$parent_userlogin = $records[$i]['ParentUserLogin'];
			$student_userlogin = $records[$i]['StudentUserLogin'];
			
			if(!isset($parentLoginStudentLoginRecords[$parent_userlogin])){
				$parentLoginStudentLoginRecords[$parent_userlogin] = array();
			}
			$parentLoginStudentLoginRecords[$parent_userlogin][$student_userlogin] = $records[$i];
			if(!isset($studentLoginParentLoginRecords[$student_userlogin])){
				$studentLoginParentLoginRecords[$student_userlogin] = array();
			}
			$studentLoginParentLoginRecords[$student_userlogin][$parent_userlogin] = $records[$i];
		}
		
		for($i=0;$i<$data_size;$i++){
			
			list($parent_userlogin,$parent_name,$student_userlogin,$student_classname,$student_classnumber,$student_name,$consent,$consent_time) = $data[$i];
			$line = $i+2;
			$errors = array();
			$parent_userlogin = trim($parent_userlogin);
			$student_userlogin = trim($student_userlogin);
			$consent = trim($consent);
			$consent_time = trim($consent_time);
			
			if($parent_userlogin == ''){
				$errors[] = $Lang['StudentAttendance']['HKUSPHParentConsentImportErrors']['BlankParentLoginID'];
			}else if(!isset($parentLoginStudentLoginRecords[$parent_userlogin])){
				$errors[] = str_replace('<!--USERLOGIN-->',$parent_userlogin,$Lang['StudentAttendance']['HKUSPHParentConsentImportErrors']['ParentUserCannotBeFound']);
			}else if(!isset($parentLoginStudentLoginRecords[$parent_userlogin][$student_userlogin])){
				$errors[] = str_replace(array('<!--PARENT_USERLOGIN-->','<!--STUDENT_USERLOGIN-->'),array($parent_userlogin,$student_userlogin),$Lang['StudentAttendance']['HKUSPHParentConsentImportErrors']['CannotFoundStudentForParent']);
			}
			
			if($student_userlogin == ''){
				$errors[] = $Lang['StudentAttendance']['HKUSPHParentConsentImportErrors']['BlankStudentLoginID'];
			}else if(!isset($studentLoginParentLoginRecords[$student_userlogin])){
				$errors[] = str_replace('<!--USERLOGIN-->',$student_userlogin,$Lang['StudentAttendance']['HKUSPHParentConsentImportErrors']['StudentUserCannotBeFound']);
			}else if(!isset($studentLoginParentLoginRecords[$student_userlogin][$parent_userlogin])){
				$errors[] = str_replace(array('<!--PARENT_USERLOGIN-->','<!--STUDENT_USERLOGIN-->'),array($parent_userlogin,$student_userlogin),$Lang['StudentAttendance']['HKUSPHParentConsentImportErrors']['CannotFoundParentForStudent']);
			}
			
			if(!in_array($consent,$consent_valid_values)){
				$errors[] = $Lang['StudentAttendance']['HKUSPHParentConsentImportErrors']['InvalidConsentValue'];
			}
			
			if($consent_time != ''){
				if(preg_match('/^(\d{1,2})\/(\d{1,2})\/(\d\d\d\d)\s(\d{1,2}):(\d{1,2})(:\d{1,2})*$/',$consent_time,$matches)){
					$consent_time = sprintf('%04d-%02d-%02d %02d:%02d:%02d', $matches[3], $matches[2], $matches[1], $matches[4], $matches[5], str_replace(':','',$matches[6]));
				}
				$ts = strtotime($consent_time);
				$datetime_str = date("Y-m-d H:i:s",$ts);
				if(!(preg_match('/^\d\d\d\d-\d\d-\d\d\s\d\d:\d\d:\d\d$/',$consent_time) || preg_match('/^\d{1,2}\/\d{1,2}\/\d\d\d\d\s\d{1,2}:\d{1,2}(:\d{1,2})*$/',$consent_time)) || $consent_time != $datetime_str){
					$errors[] = $Lang['StudentAttendance']['HKUSPHParentConsentImportErrors']['InvalidConsentTime'];
				}
			}
			
			if(count($errors)>0){
				$error_ary[$line] = $errors;
			}else{
				$valid_row = $parentLoginStudentLoginRecords[$parent_userlogin][$student_userlogin];
				$valid_row['IsAgreed'] = $consent;
				$valid_row['SignDate'] = $consent_time;
				if($parent_name == '') $data[$i][1] = $valid_row['ParentName'];
				if($student_name == '') $data[$i][5] = $valid_row['StudentName'];
				if($class_name == '') $data[$i][3] = $valid_row['ClassName'];
				if($class_number == '') $data[$i][4] = $valid_row['ClassNumber'];
				$data_ary[] = $valid_row;
				//$added_ary[$parent_userlogin][$student_userlogin] = $data[$i];
			}
			
			$rowcss = " class=\"".($line %2 == 0? "tablebluerow2":"tablebluerow1")."\" ";
			$x .= "<tr $rowcss>";
			$x .= "<td class=\"tabletext\" valign=\"top\">".($line)."</td>";
			foreach($data[$i] as $val){
				$x .= "<td class=\"tabletext\" valign=\"top\">".($val)."</td>";
			}
			$x .= "<td class=\"tabletext red\">";
			$x .= isset($error_ary[$line]) ? implode("<br />",$error_ary[$line]) : "&nbsp;";
			$x .= "</td>";
			$x .= "</tr>";
		}
	}
	$serialized_data = serialize($data_ary);
	$base64encoded_data = base64_encode($serialized_data);
	$hidden_fields .= '<input type="hidden" name="data" id="data" value="'.$base64encoded_data.'" />';
	$hidden_fields .= '<input type="hidden" name="type" id="type" value="'.$type.'" />';
}

// Step 3 - Do update
if($step == 2)
{
	$data_ary = unserialize(base64_decode($data));
	$data_size = count($data_ary);
	$successAry = array();
	$failAry = array();
	
	for($i=0;$i<$data_size;$i++){
		$agreement_id = $data_ary[$i]['AgreementID'];
		$consent = $data_ary[$i]['IsAgreed'];
		$consent_time = $data_ary[$i]['SignDate'];
		$success = true;
		if($agreement_id != '' && $agreement_id > 0){
			if($consent == ''){
				$sql = "DELETE FROM HKUSPH_AGREEMENT WHERE AgreementID='$agreement_id'";
				$success = $lc->db_db_query($sql);
			}else{
				$sql = "UPDATE HKUSPH_AGREEMENT SET IsAgreed='$consent',SignDate=".($consent_time !=''? "'$consent_time'" : "NOW()")." WHERE AgreementID='$agreement_id'";
				$success = $lc->db_db_query($sql);
			}
		}else if($consent != ''){
			$student_id = $data_ary[$i]['StudentID'];
			$parent_id = $data_ary[$i]['ParentID'];
			$parent_name = $data_ary[$i]['ParentName'];
			$sql = "INSERT INTO HKUSPH_AGREEMENT (StudentID,ParentID,IsAgreed,ParentName,SignDate,Status) VALUES ('$student_id','$parent_id','$consent','".$lc->Get_Safe_Sql_Query($parent_name)."',".($consent_time !=''? "'$consent_time'" : "NOW()").",'a')";
			$success = $lc->db_db_query($sql);
		}
		if($success){
			$successAry[] = $data_ary[$i];
		}else{
			$failAry[] = $i+2; // display failure row numbers
		}
	}
}

$PAGE_NAVIGATION[] = array($Lang['StudentAttendance']['HKUSPHParentConsentRecords'], "index.php");
$PAGE_NAVIGATION[] = array($Lang['Btn']['Import'], "");

$STEPS_OBJ[] = array($i_general_select_csv_file, $step==0 || $step==''? 1 : 0);
$STEPS_OBJ[] = array($iDiscipline['Confirmation'], $step==1?1: 0);
$STEPS_OBJ[] = array($i_general_imported_result, $step==2?1:0);

$TAGS_OBJ[] = array($Lang['StudentAttendance']['HKUSPHParentConsentRecords'],'', 1);

if(isset($_SESSION['STUDENT_ATTENDANCE_PARENT_CONSENT_RETURN_MSG']))
{
	$ReturnMsg = $_SESSION['STUDENT_ATTENDANCE_PARENT_CONSENT_RETURN_MSG'];
	unset($_SESSION['STUDENT_ATTENDANCE_PARENT_CONSENT_RETURN_MSG']);
}
if($xmsg != '' &&  isset($Lang['General']['ReturnMessage'][$xmsg]))
{
	$ReturnMsg = $Lang['General']['ReturnMessage'][$xmsg];
}
$MODULE_OBJ = $lc->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START($ReturnMsg);
?>
<br />
<form id="form1" name="form1" method="post" action="" onsubmit="return false;" enctype="multipart/form-data">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
    	<td height="40"><?=$linterface->GET_NAVIGATION($PAGE_NAVIGATION)?></td>
    </tr>
    <tr>
		<td colspan="2"><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
	</tr>
<?php if($step == 0){ ?>
	<tr>
		<td colspan="2">
	        <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
	            <tr> 
	            	<td><br />
						<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
							<tr>
								<td class="formfieldtitle" align="left" width="30%"><?=$Lang['General']['SourceFile']?> <span class="tabletextremark"><?=$Lang['General']['CSVFileFormat']?></span></td>
								<td class="tabletext"><input class="file" type="file"  id = "userfile" name="userfile"></td>
							</tr>
							<tr>
								<td class="formfieldtitle" align="left" width="30%"><?=$Lang['StudentAttendance']['ImportType']?></td>
								<td class="tabletext">
									<?=$linterface->Get_Radio_Button("general_type", "type", "1", $type != 2, $___Class="", $____Display=$Lang['StudentAttendance']['General'], $____Onclick="onTypeChanged(this);",$____isDisabled=0)?>&nbsp;
									<?=$linterface->Get_Radio_Button("notice_type", "type", "2", $type == 2, $___Class="", $____Display=$Lang['StudentAttendance']['From_eNotice'], $____Onclick="onTypeChanged(this);",$____isDisabled=0)?>
								</td>
							</tr>
							<tr>
								<td class="formfieldtitle" align="left" width="30%"><?=$Lang['General']['CSVSample']?> </td>
								<td class="tabletext">
									<?=$sample_csv_link?>
								</td>
							</tr>
							<tr>
								<td class="formfieldtitle" align="left" width="30%"><?=$Lang['General']['ImportArr']['DataColumn']?></td>
								<td class="tabletext">
									<?=$csv_format?>
								</td>
							</tr>
						</table>
						<span class="tabletextremark"><?=$i_general_required_field?></span>
					</td>
		        </tr>
		    </table>
		</td>
	</tr>
    <tr>
		<td colspan="2">        
			<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
				<tr>
	                <td class="dotline"><img src="<?=$image_path."/".$LAYOUT_SKIN?>/10x10.gif" width="10" height="1" /></td>
	            </tr>
	            <tr>
					<td align="center">
						<?= $linterface->GET_ACTION_BTN($Lang['Btn']['Import'], "button", "CheckForm();","submit_btn"," class=\"formbutton\" onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
						&nbsp;
						<?= $linterface->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "window.location.href='index.php'","back_btn"," class='formbutton' "); ?>				
					</td>
				</tr>
	        </table>                                
		</td>
	</tr>
<?php } ?>

<?php if($step == 1){ ?>
	<tr>
		<td colspan="2">
			<input type="hidden" name="type" id="type" value="<?=$type?>" />
			<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr>
					<td class="formfieldtitle" width="30%"><?=$Lang['General']['SuccessfulRecord']?></td>
					<td class="tabletext" width="70%"><?=count($data)-count($error_ary)?></td>
				</tr>
				<tr>
					<td class="formfieldtitle" width="30%"><?=$Lang['General']['FailureRecord']?></td>
					<td class="tabletext <?=count($error_ary)?"red":""?>" width="70%"><?=count($error_ary)?></td>
				</tr>
			</table>
			<?php //if(!empty($error_ary)){ ?>
	        <table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr>
					<td class="tablebluetop tabletopnolink" width="1"><?=$Lang['General']['ImportArr']['Row']?></td>
			<?php foreach($file_format as $header_val){ ?>
					<td class="tablebluetop tabletopnolink"><?=$header_val?></td>
			<?php } ?>
					<td class="tablebluetop tabletopnolink"><?=$Lang['General']['Error']?></td>
				</tr>
				<?=$x?>
			</table>
			<?php //} ?>
		</td>
	</tr>
	<tr>
		<td colspan="2">        
			<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
				<tr>
	                <td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
	            </tr>
	            <tr>
					<td align="center">
					<?php if(count($data_ary) > 0 && count($error_ary)==0){ ?>
						<?=$linterface->GET_ACTION_BTN($Lang['Btn']['Import'], "button", "ConfirmSubmit();","submit_btn"," class='formbutton'  onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"")?>
						&nbsp;
					<?php } ?>
						<?=$linterface->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "CancelConfirm();","back_btn"," class='formbutton' ")?>				
					</td>
				</tr>
	        </table>                                
		</td>
	</tr>
<?php } ?>

<?php if($step == 2){ ?>
	<tr>
		<td colspan="2">
			<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr>
					<td class="formfieldtitle" width="30%"><?=$Lang['General']['SuccessfulRecord']?></td>
					<td class="tabletext" width="70%"><?=count($successAry)?></td>
				</tr>
				<tr>
					<td class="formfieldtitle" width="30%"><?=$Lang['General']['FailureRecord']?></td>
					<td class="tabletext <?=count($failAry)?"red":""?>" width="70%"><?=count($failAry)?></td>
				</tr>
				<?php 
				if(count($failAry)>0){
					$x = '<tr>';
						$x.= '<td class="formfieldtitle" width="30%">'.$Lang['General']['ImportArr']['Row'].'</td>';
						$x.= '<td class="tabletext red" width="70%">';
							$x .= implode("<br />",$failAry);
						$x .= '</td>';
					$x.= '</tr>';
					echo $x;
				}
				?>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="2">        
			<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
				<tr>
	                <td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
	            </tr>
	            <tr>
					<td align="center">
						<?=$linterface->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "window.location.href='index.php'","back_btn"," class='formbutton' ")?>				
					</td>
				</tr>
	        </table>                                
		</td>
	</tr>
<?php } ?>
</table>
<input type="hidden" name="step" id="step" value="<?=$step?>" />
<?=$hidden_fields?>
</form>
<script type="text/javascript" language="javascript">
function onTypeChanged(obj)
{
	var type_class = obj.id;
	$('.general_type').hide();
	$('.notice_type').hide();
	$('.'+type_class).show();
}

function CheckForm()
{
	var filename = $("#userfile").val()
	var fileext = filename.substring(filename.lastIndexOf(".")).toLowerCase()
	if(fileext!=".csv" && fileext!=".txt")
	{	
		alert("<?=$Lang['AccountMgmt']['WarnMsg']['PleaseSelectCSVorTXT']?>");
		return false;
	}
	document.getElementById('step').value='1';
	document.form1.submit();
}

function ConfirmSubmit()
{
	document.getElementById('step').value='2';
	document.form1.submit();
}

function CancelConfirm()
{
	document.getElementById('step').value='0';
	document.form1.submit();
}
</script>
<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>