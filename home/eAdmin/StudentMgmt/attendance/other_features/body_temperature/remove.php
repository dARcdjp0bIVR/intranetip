<?php
// Editing by 
/*
 * 2017-05-24 (Carlos): created.
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent'] || !$sys_custom['StudentAttendance']['RecordBodyTemperature']) {
	intranet_closedb();
	$_SESSION['STUDENT_ATTENDANCE_BODYTEMPERATURE_RETURN_MSG'] = $Lang['General']['ReturnMessage']['DeleteUnsuccess'];
	exit();
}

if(!isset($_POST['RecordID']) || empty($_POST['RecordID']) || (is_array($_POST['RecordID']) && count($_POST['RecordID'])==0))
{
	intranet_closedb();
	$_SESSION['STUDENT_ATTENDANCE_BODYTEMPERATURE_RETURN_MSG'] = $Lang['General']['ReturnMessage']['DeleteUnsuccess'];
	exit();
}

$lc = new libcardstudentattend2();

$record_id_ary = IntegerSafe( $_POST['RecordID'] );
$records = $lc->getBodyTemperatureRecords(array('RecordID'=>$record_id_ary));
$record_size = count($records);

$log = date('Y-m-d H:i:s').' '.$_SERVER['REQUEST_URI'].' '.$_SESSION['UserID'].' ';
$success = false;
for($i=0;$i<$record_size;$i++)
{
	$log .= '['.http_build_query($records[$i],'',',').']';
}

$sql = "DELETE FROM CARD_STUDENT_BODY_TEMPERATURE_RECORD WHERE RecordID IN (".implode(",",$record_id_ary).")";
$success = $lc->db_db_query($sql);

$lc->log($log);

intranet_closedb();
$_SESSION['STUDENT_ATTENDANCE_BODYTEMPERATURE_RETURN_MSG'] = $success? $Lang['General']['ReturnMessage']['DeleteSuccess'] : $Lang['General']['ReturnMessage']['DeleteUnsuccess'];
header("Location: index.php");
exit;
?>