<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();
$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageOtherFeatures_Detention";

$linterface = new interface_html();

$lclass = new libclass();

$TAGS_OBJ[] = array($i_StudentAttendance_Menu_OtherFeatures_Detention, "", 0);

$PAGE_NAVIGATION[] = array($button_new);

$MODULE_OBJ = $lc->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

if ($msg == 1) $SysMsg = $linterface->GET_SYS_MSG("add");
if ($msg == 2) $SysMsg = $linterface->GET_SYS_MSG("update");
if ($msg == 3) $SysMsg = $linterface->GET_SYS_MSG("delete");

if (!isset($ClassName) || $ClassName == "")
{
        $select_class = $lclass->getSelectClass("name=\"ClassName\" onChange=\"this.form.submit()\"");
        $x = "<br /><form name=\"form1\" action=\"\" method=\"GET\">\n";
        $x .= "<table width=\"95%\" border=\"0\" cellspacing=\"0\" cellpadding=\"5\" align=\"center\">";
        $x .= "<tr><td width=\"30%\" valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">\n";
        $x .= "$i_UserParentLink_SelectClass</td><td width=\"70%\" class=\"tabletext\">$select_class</td>";
        $x .= "</td></tr></table></form><br />\n";
        echo $x;
}
else
{
        $select_class = $lclass->getSelectClass("name=\"ClassName\" onChange=\"this.form.action='';this.form.submit()\"",$ClassName);
        $select_student = $lclass->getStudentSelectByClass($ClassName,"name=\"StudentID\"");

        $currentDate = date('Y-m-d');
        $currentTime = date('H:i:s');
        
        $lcard = new libcardstudentattend2();
        
        $locationTemplate = getSelectByValue($lcard->getWordList(4),"onChange=\"this.form.Location.value=this.value\"");
        $reasonTemplate = getSelectByValue($lcard->getWordList(5),"onChange=\"this.form.Reason.value=this.value\"");
        
?>

<script Language="JavaScript">
function checkForm()
{
	if(document.form1.ClassName.value != "")
	{
		if(document.form1.StudentID.value != "")
		{
			return true;
		}
		else
		{
			alert ("<?=$i_StudentAttendance_Student_Select_Instruction?>");
			return false;
		}
	}
	else
	{
		alert ("<?=$i_StudentAttendance_Class_Select_Instruction?>");
		return false;
	}
}
</script>
<br />
<form name="form1" action="new_update.php" method="POST" onSubmit="return checkForm()">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	</tr>
	<tr><td>&nbsp;</td></tr>
<table>
<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_SmartCard_ClassName?></td>
		<td width="70%"class="tabletext"><?=$select_class?></td>
	</tr>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_UserStudentName?></td>
		<td width="70%"class="tabletext"><?=$select_student?></td>
	</tr>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_SmartCard_DetentionDate?></td>
		<td width="70%"class="tabletext"><?=$linterface->GET_DATE_PICKER("RecordDate", $currentDate)?></td>
	</tr>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_SmartCard_DetentionArrivalTime?></td>
		<td width="70%"class="tabletext">
			<input style="margin-right: 10px" type="text" name="ArrivalTime" class="textboxnum" maxlength="10" value="<?=$currentTime?>">(HH:mm:ss 24-hr)
		</td>
	</tr>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_SmartCard_DetentionDepartureTime?></td>
		<td width="70%"class="tabletext">
			<input style="margin-right: 10px" type="text" name="DepartureTime" class="textboxnum" maxlength="10">(HH:mm:ss 24-hr)
		</td>
	</tr>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_SmartCard_DetentionLocation?></td>
		<td width="70%"class="tabletext">
			<input style="margin-right: 20px" type="text" class="textboxnum" name="Location"><?=$locationTemplate?>
		</td>
	</tr>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_SmartCard_DetentionReason?></td>
		<td width="70%"class="tabletext">
			<input style="margin-right: 20px" type="text" class="textboxnum" name="Reason"><?=$reasonTemplate?>
		</td>
	</tr>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_SmartCard_Remark?></td>
		<td width="70%"class="tabletext">
			<?=$linterface->GET_TEXTAREA("Remark", "")?>
		</td>
	</tr>
</table>
<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
    	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
    </tr>
</table>
<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
	    <td align="center" colspan="2">
			<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "") ?>
			<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
			<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:history.back()","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
		</td>
	</tr>
</table>
</form>
<br />
<?
}
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
