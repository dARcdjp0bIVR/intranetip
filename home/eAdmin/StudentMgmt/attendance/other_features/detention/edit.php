<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();
$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageOtherFeatures_Detention";

$linterface = new interface_html();

$DetentionID = (is_array($DetentionID)? $DetentionID[0]:$DetentionID);

$li = new libdb();

$user_field = getNameFieldByLang("b.");
$sql = "SELECT a.StudentID, a.RecordDate, IF(a.ArrivalTime IS NULL,'',a.ArrivalTime),
        IF(a.DepartureTime IS NULL,'',a.DepartureTime), a.Location,a.Reason,a.Remark,
        CONCAT($user_field,' (',b.ClassName,'-', b.ClassNumber,')')
        FROM CARD_STUDENT_DETENTION as a
             LEFT OUTER JOIN INTRANET_USER as b ON a.StudentID = b.UserID
        WHERE DetentionID = $DetentionID";
$result = $li->returnArray($sql,8);
list($uid,$recordDate,$arrivalTime,$departureTime,$location,$reason,$remark,$name) = $result[0];

$lc = new libcardstudentattend2();

$locationTemplate = getSelectByValue($lc->getWordList(4),"onChange=\"this.form.Location.value=this.value\"",$location);
$reasonTemplate = getSelectByValue($lc->getWordList(5),"onChange=\"this.form.Reason.value=this.value\"",$reason);

$TAGS_OBJ[] = array($i_StudentAttendance_Menu_OtherFeatures_Detention, "", 0);

$PAGE_NAVIGATION[] = array($button_edit);

$MODULE_OBJ = $lc->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

if ($msg == 1) $SysMsg = $linterface->GET_SYS_MSG("add");
if ($msg == 2) $SysMsg = $linterface->GET_SYS_MSG("update");
if ($msg == 3) $SysMsg = $linterface->GET_SYS_MSG("delete");
?>
<br />
<form name="form1" action="edit_update.php" method="POST">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	</tr>
	<tr><td>&nbsp;</td></tr>
<table>
<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_UserStudentName?></td>
		<td width="70%"class="tabletext"><?=$name?></td>
	</tr>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_SmartCard_DetentionDate?></td>
		<td width="70%"class="tabletext"><?=$linterface->GET_DATE_PICKER("RecordDate", $recordDate)?></td>
	</tr>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_SmartCard_DetentionArrivalTime?></td>
		<td width="70%"class="tabletext">
			<input type="text" name="ArrivalTime" class="textboxnum" maxlength="10" value="<?=$arrivalTime?>">(HH:mm:ss 24-hr)
		</td>
	</tr>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_SmartCard_DetentionDepartureTime?></td>
		<td width="70%"class="tabletext">
			<input type="text" name="DepartureTime" class="textboxnum" maxlength="10" value="<?=$departureTime?>">(HH:mm:ss 24-hr)
		</td>
	</tr>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_SmartCard_DetentionLocation?></td>
		<td width="70%"class="tabletext">
			<input type="text" class="textboxnum" name="Location"  value="<?=$location?>"><?=$locationTemplate?>
		</td>
	</tr>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_SmartCard_DetentionReason?></td>
		<td width="70%"class="tabletext">
			<input type="text" class="textboxnum" name="Reason" value="<?=$reason?>"><?=$reasonTemplate?>
		</td>
	</tr>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_SmartCard_Remark?></td>
		<td width="70%"class="tabletext">
			<?=$linterface->GET_TEXTAREA("Remark", $remark)?>
		</td>
	</tr>
</table>
<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
    	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
    </tr>
</table>
<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
	    <td align="center" colspan="2">
			<?= $linterface->GET_ACTION_BTN($button_save, "submit", "") ?>
			<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
			<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:history.back()","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
		</td>
	</tr>
</table>
<input type="hidden" name="DetentionID" value="<?=$DetentionID?>">
</form>
<br/ >
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
