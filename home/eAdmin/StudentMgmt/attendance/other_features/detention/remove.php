<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}


$lc = new libcardstudentattend2();

$li = new libdb();

$list = implode(",",$DetentionID);
$sql = "DELETE FROM CARD_STUDENT_DETENTION WHERE DetentionID IN ($list)";
if ($li->db_db_query($sql)) {
	$Msg = $Lang['StudentAttendance']['DetentionRecordDeleteSuccess'];
}
else {
	$Msg = $Lang['StudentAttendance']['DetentionRecordDeleteFail'];
}

header ("Location: index.php?Msg=".urlencode($Msg));
?>
