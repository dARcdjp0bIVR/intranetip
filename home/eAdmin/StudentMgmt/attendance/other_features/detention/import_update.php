<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();

$limport = new libimporttext();

$format_array = array("ClassName","ClassNumber","Date","Arrival","Departure","Location","Reason","Remark");

$li = new libdb();
$filepath = $userfile;
$filename = $userfile_name;
if($filepath=="none" || $filepath == ""){          # import failed
        header("Location: import.php?msg=5");
        exit();
} else {
        if($limport->CHECK_FILE_EXT($filename)) {
                # read file into array
                # return 0 if fail, return csv array if success
                $data = $limport->GET_IMPORT_TXT($filepath);
                $toprow = array_shift($data);                   # drop the title bar
        }
        for ($i=0; $i<sizeof($format_array); $i++)
        {
             if ($toprow[$i] != $format_array[$i])
             {
                 header("Location: import.php?msg=4");
                 exit();
             }
        }
        $sql = "CREATE TEMPORARY TABLE TEMP_IMPORT_STUDENT_DETENTION (
                 ClassName varchar(20),
                 ClassNumber varchar(20),
                 RecordDate date,
                 Arrival varchar(20),
                 Departure varchar(20),
                 Location varchar(255),
                 Reason text,
                 Remark text
        ) ENGINE=InnoDB charset=utf8";
        $sql = $li->db_db_query($sql);
        $values = "";
        $delim = "";
        for ($i=0; $i<sizeof($data); $i++)
        {
             list($class,$classnum,$recordDate,$arrival,$departure,$location,$reason,$remark) = $data[$i];
             if ($class=="" || $classnum=="") continue;
             if ($recordDate == "")
                 $recordDateStr = "CURDATE()";
             else
                 $recordDateStr = "'$recordDate'";
             if ($arrival == "")
                 $arrivalStr = "NULL";
             else
                 $arrivalStr = "'$arrival'";
             if ($departure == "")
                 $departureStr = "NULL";
             else
                 $departureStr = "'$departure'";

             $values .= "$delim ('$class','$classnum',$recordDateStr,$arrivalStr,$departureStr,'$location','$reason','$remark')";
             $delim = ",";
        }

        $sql = "INSERT INTO TEMP_IMPORT_STUDENT_DETENTION (ClassName,ClassNumber,RecordDate,Arrival,Departure,Location,Reason,Remark)
                       VALUES $values";
        $li->db_db_query($sql);

        # Insert to Reminder table
        $sql = "INSERT INTO CARD_STUDENT_DETENTION (StudentID, RecordDate, ArrivalTime, DepartureTime,
                            Location, Reason, Remark,DateInput, DateModified)
                       SELECT
                             b.UserID, a.RecordDate, a.Arrival, a.Departure, a.Location, a.Reason,
                               a.Remark, now(), now()
                       FROM TEMP_IMPORT_STUDENT_DETENTION as a
                            LEFT OUTER JOIN INTRANET_USER as b
                                 ON a.ClassName = b.ClassName AND a.ClassNumber = b.ClassNumber AND b.RecordType = 2
                       WHERE b.UserID IS NOT NULL
                       ";
        $li->db_db_query($sql);
}
intranet_closedb();

header("Location: index.php?msg=1");
?>