<?
// Using: Bill

################################################
#
#	Date:	2016-08-26	Bill
#			- Create file
#			Copy layout from /home/eAdmin/StudentMgmt/attendance/other_features/reason_code_view/index.php
#			Copy logic from /home/portfolio/teacher/settings/slp/preset_item_template.php
#
################################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libstudentattendance_ui.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

// Access right
if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();
$linterface = new interface_html();

// Get preset reason from txt file
$file_path = $intranet_root."/file/student_attendance/preset_absent_reason2.txt";
if (!file_exists($file_path))
	$data = "";
else
	$data = get_file_content($file_path);

// Instruction
$Instruction = $Lang['StudentAttendance']['AbsentReasonManageInstruction'];
		
// Tab Display
$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageOtherFeatures_ViewWebSAMSReasonCode";

$TAGS_OBJ[] = array($Lang['StudentAttendance']['ViewWebsamsReasonCode'],"../reason_code_view/", 0);
$TAGS_OBJ[] = array($Lang['StudentAttendance']['OutingReasonManage'],"../outing_reason_manage/", 0);
$TAGS_OBJ[] = array($Lang['StudentAttendance']['AbsentReasonManage'],"", 1);

$MODULE_OBJ = $lc->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();
?>

<form name="form1" action="update.php" method="POST">
	<table width="100%"  border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td align="right">
					<? echo $linterface->GET_SYS_MSG($msg); ?>
				</td></tr></table>
			</td>
		</tr>
	</table>
	<table width="50%" border="0" cellspacing="0" cellpadding="4" align="center">
		<tr><td valign="top"><?= $Instruction ?></td></tr>
		<tr>
			<td>
				<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
				<tr><td>
					<textarea class="textboxtext" name="data" ROWS="20"><?= $data ?></textarea>
				</td></tr>
				</table>
			</td>
		</tr>
	</table>
	<table width="95%" border="0" cellspacing="0" cellpadding="1" align="center">
		<tr><td class="dotline" colspan="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
		<tr><td align="center" colspan="6">
		<div style="padding-top: 5px">
			<?= $linterface->GET_ACTION_BTN($button_submit, "button", "javascript:form1.submit()")?>&nbsp;
			<?= $linterface->GET_ACTION_BTN($button_reset, "reset")?>&nbsp;
		</div>
		</td></tr>
	</table>
</form>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>