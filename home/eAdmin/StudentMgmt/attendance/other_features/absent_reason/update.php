<?php
// Using by Bill

################################################
#
#	Date:	2016-08-26	Bill
#			- Create file
#			Copy from /home/portfolio/teacher/settings/slp/preset_item_update.php
#
################################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

intranet_auth();
intranet_opendb();

// Access right
if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lf = new libfilesystem();
		
// Create folder if not exist
$txt_folder_path  = $intranet_root."/file/student_attendance";
if(!is_dir($txt_folder_path)){
	$lf->folder_new($txt_folder_path);
}

// Store data into txt file
$data = stripslashes(htmlspecialchars($data));
$txt_file_path = $txt_folder_path."/preset_absent_reason2.txt";
$lf->file_write($data, $txt_file_path);

intranet_closedb();

header("Location: index.php?msg=update");
?>