<?php
// Editing by 
/*
 * 2017-11-14 (Carlos): $sys_custom['StudentAttendance']['HKUGAC_LateAbsentEmailNotification'] created.
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/json.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent'] || !$sys_custom['StudentAttendance']['HKUGAC_LateAbsentEmailNotification']) {
	intranet_closedb();
	//header ("Location: /");
	exit();
}

$lc = new libcardstudentattend2();
$linterface = new interface_html();

$task = $_REQUEST['task'];

switch($task)
{
	case "getDetailRecords":
		$params = array('GetDetailRecords'=>1,'DetailRecordType'=>$RecordType,'StudentID'=>$SelectedStudentID,'AcademicYearID'=>$AcademicYearID,'YearTermID'=>$YearTermID,'YearClassID'=>$YearClassID,'LateCount'=>$LateCount,'AbsentCount'=>$AbsentCount,'AndOr'=>$AndOr,'Keyword'=>$Keyword);
		if($ByDateRange && preg_match('/^\d\d\d\d-\d\d-\d\d$/',$StartDate) && preg_match('/^\d\d\d\d-\d\d-\d\d$/',$EndDate)){
			$params['StartDate'] = $StartDate;
			$params['EndDate'] = $EndDate;
		}
		$records = $lc->getStudentLateAbsentCountingRecords($params);
		$record_size = count($records);
		
		$x .= '<table class="common_table_list_v30">';
		$x .= '<tbody>';
		$x .= '<tr class="tabletop"><th>'.$Lang['General']['RecordDate'].'</th><th>'.$i_Attendance_Reason.'</th><th>'.$Lang['StudentAttendance']['OfficeRemark'].'</th>'.($RecordType==2?'<th>'.$Lang['StudentAttendance']['LateTime'].'</th>':'').'</tr>';
		if($record_size == 0){
			$x .= '<tr><td colspan="4" align="center">'.$i_no_record_exists_msg.'</td></tr>';
		}
		for($i=0;$i<$record_size;$i++){
			$x .= '<tr><td>'.$records[$i]['RecordDate'].'</td><td>'.Get_String_Display($records[$i]['Reason']).'</td><td>'.Get_String_Display($records[$i]['OfficeRemark']).'</td>'.($RecordType==2?'<td>'.$records[$i]['LateTime'].'</td>':'').'</tr>';
		}
		$x .= '</tbody>';
		$x .= '</table>';
		
		echo $x;
		
	break;
	
	case "getStudentEmailTable":
		
		$params = array('StudentID'=>$StudentID,'AcademicYearID'=>$AcademicYearID,'YearTermID'=>$YearTermID,'YearClassID'=>$YearClassID,'LateCount'=>$LateCount,'AbsentCount'=>$AbsentCount,'AndOr'=>$AndOr,'Keyword'=>$Keyword);
		if($ByDateRange && preg_match('/^\d\d\d\d-\d\d-\d\d$/',$StartDate) && preg_match('/^\d\d\d\d-\d\d-\d\d$/',$EndDate)){
			$params['StartDate'] = $StartDate;
			$params['EndDate'] = $EndDate;
		}
		$records = $lc->getStudentLateAbsentCountingRecords($params);
		
		$x .= '<div style="text-align:center;margin:1em;font-size:1.5em;color:green;">
				<span id="Spinner">'.$linterface->Get_Ajax_Loading_Image(1).'</span> '.str_replace(array('<!--CURRENT-->','<!--TOTAL-->'),array('<span id="SentCount">0</span>',count($records)),$Lang['StudentAttendance']['NumberOfStudentsSentingProgressStatement']);;
		$x .= $linterface->GET_SMALL_BTN($Lang['StudentAttendance']['CancelSending'], "button", "CancelSending();", "CancelBtn", $____ParOtherAttribute="", $___OtherClass="", $___ParTitle='');
		$x .= '</div>';
		$x .= '<div id="FeedbackContainer" style="display:none;text-align:center;margin:1em;font-size:1.5em;color:red;"></div>';
		//$x .= '<div style="max-height:480px;overflow:y;">';
		$x .= '<table class="common_table_list_v30">';
			$x .= '<tbody>';
			$x .= '<tr class="tabletop">
					<th>'.$Lang['StudentAttendance']['ClassName'].'</th>
					<th>'.$Lang['StudentAttendance']['ClassNumber'].'</th>
					<th>'.$Lang['StudentAttendance']['StudentName'].'</th>
					<th>'.$Lang['StudentAttendance']['NumberOfLate'].'</th>
					<th>'.$Lang['StudentAttendance']['NumberOfAbsence'].'</th>
					<th>'.$Lang['StudentAttendance']['SendStatus'].'</th>
				 </tr>';
			for($i=0;$i<count($records);$i++){
				$x .= '<tr>
						<td>'.$records[$i]['ClassName'].'</td>
						<td>'.$records[$i]['ClassNumber'].'</td>
						<td>'.$records[$i]['StudentName'].'</td>
						<td>'.$records[$i]['LateCount'].'</td>
						<td>'.$records[$i]['AbsentCount'].'</td>
						<td id="SendStatus_'.$records[$i]['UserID'].'"><span style="color:blue">'.$Lang['StudentAttendance']['Pending'].'</span></td>
					   </tr>';
			}
			$x .= '</tbody>';
		$x .= '</table><br />';
		//$x .= '</div>';
		
		echo $x;
		
	break;
	
	case "sendEmail":
		
		include_once($PATH_WRT_ROOT."includes/libwebmail.php");
		include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
		
		$lwebmail = new libwebmail();
		
		$params = array('StudentID'=>$TargetStudentID,'StudentIDToRecords'=>1,'AcademicYearID'=>$AcademicYearID,'YearTermID'=>$YearTermID,'YearClassID'=>$YearClassID,'LateCount'=>$LateCount,'AbsentCount'=>$AbsentCount,'AndOr'=>$AndOr,'Keyword'=>$Keyword);
		if($ByDateRange && preg_match('/^\d\d\d\d-\d\d-\d\d$/',$StartDate) && preg_match('/^\d\d\d\d-\d\d-\d\d$/',$EndDate)){
			$params['StartDate'] = $StartDate;
			$params['EndDate'] = $EndDate;
		}
		$studentIdToRecords = $lc->getStudentLateAbsentCountingRecords($params);
		
		// get total count of late/absent in this academic year term
		//$params = array('StudentID'=>$TargetStudentID,'StudentIDToRecords'=>1,'AcademicYearID'=>$AcademicYearID,'YearTermID'=>$YearTermID,'YearClassID'=>$YearClassID,'LateCount'=>1,'AbsentCount'=>1,'AndOr'=>$AndOr,'Keyword'=>$Keyword);
		//$studentIdToTotalRecords = $lc->getStudentLateAbsentCountingRecords($params);
		
		// find HOY group users which to be sent when number of late or number of absent reach 6 times
		$sql = "SELECT 
					u.UserID 
				FROM INTRANET_GROUP as g 
				INNER JOIN INTRANET_USERGROUP as ug ON ug.GroupID=g.GroupID 
				INNER JOIN INTRANET_USER as u ON u.UserID=ug.UserID 
				WHERE u.RecordStatus='1' AND g.AcademicYearID='$AcademicYearID' AND g.Title='HOY'";
		$HOY_group_user_ids = $lc->returnVector($sql);
		
		ob_start();
		include($intranet_root.'/home/eAdmin/StudentMgmt/attendance/other_features/late_absent_records/email_template.php');
		$template = ob_get_clean();
		$template = str_replace(array("\r\n","\n"),"",$template);
		
		$email_title = $Lang['StudentAttendance']['HKUGACLateAbsentEmailTitle'];
		$place_holders = array('<!--STUDENT_NAME-->','<!--CLASS_NAME-->','<!--CLASS_NUMBER-->','<!--EVENT_DATE-->','<!--LATE_COUNT-->','<!--ABSENT_COUNT-->','<!--RECORD-->');
		$event_date = date("Y-m-d");
		//debug_pr($studentIdToRecords);
		$result = array();
		if(count($studentIdToRecords)>0){
			foreach($studentIdToRecords as $student_id => $record){
				$year_class_id = $record[0]['YearClassID'];
				if($record[0]['LateCount'] == 0 && $record[0]['AbsentCount']==0){
					continue;
				}
				$year_class_obj = new year_class($year_class_id,$GetYearDetail=false,$GetClassTeacherList=true,$GetClassStudentList=false,$GetLockedSGArr=false);
				$teacher_list = $year_class_obj->ClassTeacherList;
				//debug_pr($year_class_obj);
				if(count($teacher_list)>0)
				{
					$teacher_user_ids = Get_Array_By_Key($teacher_list,'UserID');
					$to_array = $teacher_user_ids;
					if(($record[0]['LateCount'] >= 6 || $record[0]['AbsentCount'] >= 6) && count($HOY_group_user_ids)>0){
						$to_array = array_merge($to_array, $HOY_group_user_ids);
					}
					$record_detail = '';
					if($record[0]['LateCount'] > 0){
						$params = array('GetDetailRecords'=>1,'DetailRecordType'=>2,'StudentID'=>$TargetStudentID,'StudentIDToRecords'=>0,'AcademicYearID'=>$AcademicYearID,'YearTermID'=>$YearTermID,'YearClassID'=>$YearClassID,'LateCount'=>$LateCount,'AbsentCount'=>$AbsentCount,'AndOr'=>$AndOr,'Keyword'=>$Keyword);
						if($ByDateRange && preg_match('/^\d\d\d\d-\d\d-\d\d$/',$StartDate) && preg_match('/^\d\d\d\d-\d\d-\d\d$/',$EndDate)){
							$params['StartDate'] = $StartDate;
							$params['EndDate'] = $EndDate;
						}
						$late_records = $lc->getStudentLateAbsentCountingRecords($params);
						for($i=0;$i<count($late_records);$i++){
							$record_detail .= 'Lateness ('.$late_records[$i]['RecordDate'].' '.$late_records[$i]['LateTime'].')<br />';
						}
					}
					if($record[0]['AbsentCount'] > 0){
						$params = array('GetDetailRecords'=>1,'DetailRecordType'=>1,'StudentID'=>$TargetStudentID,'StudentIDToRecords'=>0,'AcademicYearID'=>$AcademicYearID,'YearTermID'=>$YearTermID,'YearClassID'=>$YearClassID,'LateCount'=>$LateCount,'AbsentCount'=>$AbsentCount,'AndOr'=>$AndOr,'Keyword'=>$Keyword);
						if($ByDateRange && preg_match('/^\d\d\d\d-\d\d-\d\d$/',$StartDate) && preg_match('/^\d\d\d\d-\d\d-\d\d$/',$EndDate)){
							$params['StartDate'] = $StartDate;
							$params['EndDate'] = $EndDate;
						}
						$absent_records = $lc->getStudentLateAbsentCountingRecords($params);
						for($i=0;$i<count($absent_records);$i++){
							$record_detail .= 'Absence ('.$absent_records[$i]['RecordDate'].')<br />';
						}
					}
					
					$replace_data = array($record[0]['StudentName'],$record[0]['ClassName'],$record[0]['ClassNumber'],$event_date,sprintf("%02d", $record[0]['LateCount']),sprintf("%02d",$record[0]['AbsentCount']),$record_detail);
					$email_title_student = ' - '.$record[0]['ClassName'].'('.$record[0]['ClassNumber'].') '.$record[0]['StudentName'];
					$email_content = $template;
					$email_content = str_replace($place_holders, $replace_data, $email_content);
					//echo $email_content;
					$result['send_'.$student_id] = $lwebmail->sendModuleMail($to_array,$email_title.$email_title_student,$email_content);
				}
			}
		}
		
		echo !in_array(false,$result) && count($result)>0?"1":"0";
		
	break;
}

intranet_closedb();
?>