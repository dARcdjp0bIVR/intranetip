<?php

$PATH_WRT_ROOT = "../../../../../../";
	
//include_once("../../../functions.php");
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libattendance.php");

intranet_opendb();

$lattend = new libattendance();
$lattend->Start_Trans();
$sql = "SELECT UserID, ClassName, ClassNumber FROM INTRANET_USER
                   WHERE RecordType IN (2) AND RecordStatus = 1
                         AND ClassName!='' AND ClassNumber!=''";
$temp = $lattend->returnArray($sql,3);

for ($i=0; $i<sizeof($temp); $i++)
{
     list($targetUserID, $targetClassName, $targetClassNumber) = $temp[$i];
     $student[$targetClassName][$targetClassNumber] = $targetUserID;
}

$name_field = getNameFieldByLang("b.");

$sql = "SELECT 
				$name_field, a.ClassName, a.ClassNumber, a.Weekday0, a.Weekday1, a.Weekday2, a.Weekday3, a.Weekday4, a.Weekday5, a.Weekday6
        FROM 
        		TEMP_STUDENT_LEAVE_OPTION as a
             	LEFT OUTER JOIN INTRANET_USER as b ON b.RecordType = 2 AND a.UserID = b.UserID 
		ORDER By 
				b.ClassName, b.ClassNumber, b.EnglishName";

$result = $lattend->returnArray($sql,10);

if(sizeof($result)>0)
{	
	for($i=0; $i<sizeof($result); $i++)
	{
		list($name, $class_name, $class_number, $opt_sunday, $opt_monday, $opt_tuesday, $opt_wednesday, $opt_thursday, $opt_friday, $opt_saturday) = $result[$i];
		
		$targetUserID = $student[$class_name][$class_number];
		
		$sql = "INSERT IGNORE INTO CARD_STUDENT_LEAVE_OPTION 
							(StudentID, Weekday1, Weekday2, Weekday3, Weekday4, Weekday5, Weekday6, LastUpdateUser, DateInput, DateModified) 
						VALUES 
							($targetUserID, $opt_monday, $opt_tuesday, $opt_wednesday, $opt_thursday, $opt_friday, $opt_saturday, '$PHP_AUTH_USER', NOW(), NOW()) 
						ON DUPLICATE KEY UPDATE 
							Weekday1 = $opt_monday,
							Weekday2 = $opt_tuesday,
							Weekday3 = $opt_wednesday,
							Weekday4 = $opt_thursday,
							Weekday5 = $opt_friday,
							Weekday6 = $opt_saturday,
							LastUpdateUser = '".$_SESSION['UserID']."',
							DateModified = NOW()
					";
		
		$Result[] = $lattend->db_db_query($sql);
	}
}

if (in_array(false,$Result)) {
	$Msg = $Lang['StudentAttendance']['LeaveOptionSaveFail'];
}
else {
	$Msg = $Lang['StudentAttendance']['LeaveOptionSaveSuccess'];
}

Header("Location: import.php?Msg=".urlencode($Msg));
?>