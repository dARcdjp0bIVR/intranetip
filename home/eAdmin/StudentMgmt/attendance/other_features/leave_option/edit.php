<?php

$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageOtherFeatures_LeaveSchoolOption";

$linterface = new interface_html();

$lc = new libcardstudentattend2();

$classlist = $lc->getSelectClass("name=TargetClass onChange='this.form.submit();'", $TargetClass);

if($TargetClass != "")
{
	$namefield = getNameFieldWithClassNumberByLang("a.");
	$sql = "SELECT
					a.UserID,
					$namefield,
					b.Weekday1,
					b.Weekday2,
					b.Weekday3,
					b.Weekday4,
					b.Weekday5,
					b.Weekday6
			FROM
					INTRANET_USER AS a LEFT OUTER JOIN
					CARD_STUDENT_LEAVE_OPTION AS b ON (a.UserID = b.StudentID)
			WHERE
					a.ClassName = '$TargetClass' AND 
					a.RecordType=2 AND 
					a.RecordStatus=1
			ORDER BY
					a.ClassName, a.ClassNumber, a.EnglishName";
	//echo $sql;
	$result = $lc->returnArray($sql,8);
	
	$table_content .= '<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">';
	$table_content .= "<tr class=\"tabletop\">
							<td class=\"tabletoplink\" width=100>$i_UserStudentName</td>
							<td class=\"tabletoplink\" width=60>$i_StudentAttendance_LeaveOption_Weekday[1]<input type=checkbox name=all_1 onClick='(this.checked)?setAll(1,1):setAll(1,0)'><br>".getSelectByArray($attendst_leave_option,'name=opt_all_1 DISABLED onChange=changeAll(1)','',0,1)."</td>
							<td class=\"tabletoplink\" width=60>$i_StudentAttendance_LeaveOption_Weekday[2]<input type=checkbox name=all_2 onClick='(this.checked)?setAll(2,1):setAll(2,0)'><br>".getSelectByArray($attendst_leave_option,'name=opt_all_2 DISABLED onChange=changeAll(2)','',0,1)."</td>
							<td class=\"tabletoplink\" width=60>$i_StudentAttendance_LeaveOption_Weekday[3]<input type=checkbox name=all_3 onClick='(this.checked)?setAll(3,1):setAll(3,0)'><br>".getSelectByArray($attendst_leave_option,'name=opt_all_3 DISABLED onChange=changeAll(3)','',0,1)."</td>
							<td class=\"tabletoplink\" width=60>$i_StudentAttendance_LeaveOption_Weekday[4]<input type=checkbox name=all_4 onClick='(this.checked)?setAll(4,1):setAll(4,0)'><br>".getSelectByArray($attendst_leave_option,'name=opt_all_4 DISABLED onChange=changeAll(4)','',0,1)."</td>
							<td class=\"tabletoplink\" width=60>$i_StudentAttendance_LeaveOption_Weekday[5]<input type=checkbox name=all_5 onClick='(this.checked)?setAll(5,1):setAll(5,0)'><br>".getSelectByArray($attendst_leave_option,'name=opt_all_5 DISABLED onChange=changeAll(5)','',0,1)."</td>
							<td class=\"tabletoplink\" width=60>$i_StudentAttendance_LeaveOption_Weekday[6]<input type=checkbox name=all_6 onClick='(this.checked)?setAll(6,1):setAll(6,0)'><br>".getSelectByArray($attendst_leave_option,'name=opt_all_6 DISABLED onChange=changeAll(6)','',0,1)."</td>
							<td class=\"tabletoplink\" width=100>&nbsp;</td></tr>\n";
	if(sizeof($result)>0)
	{
		for($i=0; $i<sizeof($result); $i++)
		{
			list($uid, $student_name, $opt_mon, $opt_tue, $opt_wed, $opt_thur, $opt_fri, $opt_sat) = $result[$i];
			$table_content .= "<tr><td>$student_name</td>\n";
			$table_content .= "<td>".getSelectByArray($attendst_leave_option,"name=opt_".$uid."_1",$opt_mon,0,1)."</td>\n";
			$table_content .= "<td>".getSelectByArray($attendst_leave_option,"name=opt_".$uid."_2",$opt_tue,0,1)."</td>\n";
			$table_content .= "<td>".getSelectByArray($attendst_leave_option,"name=opt_".$uid."_3",$opt_wed,0,1)."</td>\n";
			$table_content .= "<td>".getSelectByArray($attendst_leave_option,"name=opt_".$uid."_4",$opt_thur,0,1)."</td>\n";
			$table_content .= "<td>".getSelectByArray($attendst_leave_option,"name=opt_".$uid."_5",$opt_fri,0,1)."</td>\n";
			$table_content .= "<td>".getSelectByArray($attendst_leave_option,"name=opt_".$uid."_6",$opt_sat,0,1)."</td>\n";
			$table_content .= "<td>".getSelectByArray($attendst_leave_option,"name=opt_".$uid."_all DISABLED onChange=changeAllUserOnly($uid)",'',0,1)."<input type=checkbox name=all_".$uid." onClick=\"(this.checked)?setAllUserOnly($uid,1):setAllUserOnly($uid,0)\"></td>\n";
			$table_content .= "<input type=hidden name=student[] value=$uid>\n";
		}
	}
	$table_content .= "</table>\n";
	$table_content .= '<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
											<tr>
											    <td align="center" colspan="2">
													'.$linterface->GET_ACTION_BTN($button_save, "submit", "").'
													'.$linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"").'
													'.$linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:window.location = 'edit.php';","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"").'
												</td>
											</tr>
										</table>';
}

$TAGS_OBJ[] = array($button_edit, "edit.php", 1);
$TAGS_OBJ[] = array($button_import, "import.php", 0);

$MODULE_OBJ = $lc->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START(urldecode($Msg));
?>

<SCRIPT LANGUAGE="JavaScript">
function checkForm()
{
	var obj = document.form1;
	if(obj.TargetClass.value != "")
	{
		obj.action = "edit_confirm.php";
		obj.submit();
	}
	else
	{
		alert("<?=$i_staffAttendance_LeaveOption_ClassSelection_Warning?>");
	}
}
function setAll(weekday,checked)
{
	var opt_all = eval("document.form1.opt_all_"+weekday);
	if(checked == 1)
	{
		opt_all.disabled = false;
		
		<? 
		for($i=0; $i<sizeof($result); $i++) 
		{
			list($uid, $student_name, $opt_mon, $opt_tue, $opt_wed, $opt_thur, $opt_fri, $opt_sat) = $result[$i];
		?>
			var temp = eval("document.form1.opt_"+<?=$uid?>+"_"+weekday);
			temp.value = eval("document.form1.opt_all_"+weekday+".value");
		<?
		}
		?>
	}
	if(checked == 0)
	{
		opt_all.disabled = true;
		
		<? 
		for($i=0; $i<sizeof($result); $i++) 
		{
			list($uid, $student_name, $opt_mon, $opt_tue, $opt_wed, $opt_thur, $opt_fri, $opt_sat) = $result[$i];
		?>
			var temp = eval("document.form1.opt_"+<?=$uid?>+"_"+weekday);
		<?
		}
		?>
	}
}
function setAllUserOnly(user_id,checked)
{
	var opt_AllUserOnly = eval("document.form1.opt_"+user_id+"_all");
	
	if(checked == 1)
	{
		opt_AllUserOnly.disabled = false;
		
		for(i=1; i<7; i++)
		{
			var temp = eval("document.form1.opt_"+user_id+"_"+i);
			temp.value = eval("document.form1.opt_"+user_id+"_all.value");
		}
	}
	if(checked == 0)
	{
		opt_AllUserOnly.disabled = true;
		
		for(i=1;i<7;i++)
		{
			var temp = eval("document.form1.opt_"+user_id+"_"+i);
		}
	}	
}
function changeAll(weekday)
{
	var temp = eval("document.form1.all_"+weekday);
	if(temp.checked)
	{
		<?
		for($i=0; $i<sizeof($result); $i++) 
		{
			list($uid, $student_name, $opt_mon, $opt_tue, $opt_wed, $opt_thur, $opt_fri, $opt_sat) = $result[$i];
		?>
			var temp = eval("document.form1.opt_"+<?=$uid?>+"_"+weekday);
			temp.value = eval("document.form1.opt_all_"+weekday+".value");
		<?
		}
		?>
	}
}
function changeAllUserOnly(user_id)
{
	var temp = eval("document.form1.all_"+user_id);
	if(temp.checked)
	{
		for(i=1;i<7;i++)
		{
			var temp = eval("document.form1.opt_"+user_id+"_"+i);
			temp.value = eval("document.form1.opt_"+user_id+"_all.value");
		}
	}
}
</SCRIPT>

<form name="form1" action="" method="POST" onSubmit="checkForm()">

<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_ClassName?></td>
		<td width="70%"class="tabletext"><?=$classlist?></td>
	</tr>
</table>
<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
    	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
    </tr>
</table>
<?=$table_content?>

</form>

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>