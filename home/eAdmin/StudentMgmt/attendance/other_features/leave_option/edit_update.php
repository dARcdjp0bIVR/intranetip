<?php
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lclass = new libclass();
$lclass->Start_Trans();
if(sizeof($student)>0)
{
	for($i=0; $i<sizeof($student); $i++)
	{
		$sql = "INSERT IGNORE INTO CARD_STUDENT_LEAVE_OPTION 
							(StudentID, Weekday1, Weekday2, Weekday3, Weekday4, Weekday5, Weekday6, LastUpdateUser, DateInput, DateModified)
						VALUES
							($student[$i], 
							".${"opt_".$student[$i]."_1"}.", 
							".${"opt_".$student[$i]."_2"}.", 
							".${"opt_".$student[$i]."_3"}.", 
							".${"opt_".$student[$i]."_4"}.", 
							".${"opt_".$student[$i]."_5"}.", 
							".${"opt_".$student[$i]."_6"}.", 
							'".$_SESSION['UserID']."', NOW(), NOW())
						ON DUPLICATE KEY UPDATE 
							Weekday1 = ".${"opt_".$student[$i]."_1"}.", 
							Weekday2 = ".${"opt_".$student[$i]."_2"}.", 
							Weekday3 = ".${"opt_".$student[$i]."_3"}.", 
							Weekday4 = ".${"opt_".$student[$i]."_4"}.", 
							Weekday5 = ".${"opt_".$student[$i]."_5"}.", 
							Weekday6 = ".${"opt_".$student[$i]."_6"}.", 
							LastUpdateUser = '".$_SESSION['UserID']."', 
							DateModified = NOW() 
							";
		//echo $sql."<BR>";
		$Result[] = $lclass->db_db_query($sql);
	}
}

if (in_array(false,$Result)) {
	$Msg = $Lang['StudentAttendance']['LeaveOptionSaveFail'];
	$lclass->RollBack_Trans();
}
else {
	$Msg = $Lang['StudentAttendance']['LeaveOptionSaveSuccess'];
	$lclass->Commit_Trans();
}

intranet_closedb();
header("Location: edit.php?TargetClass=$TargetClass&Msg=".urlencode($Msg));
?>