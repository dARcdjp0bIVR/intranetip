<?

$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageOtherFeatures_LeaveSchoolOption";

$linterface = new interface_html();

$lc = new libcardstudentattend2();

$TAGS_OBJ[] = array($button_edit, "edit.php", 0);
$TAGS_OBJ[] = array($button_import, "import.php", 1);

$MODULE_OBJ = $lc->GET_MODULE_OBJ_ARR();

$limport = new libimporttext();
$li = new libdb();
$lf = new libfilesystem();

$filepath = $userfile;
$filename = $userfile_name;

if($filepath=="none" || $filepath == "" || !is_uploaded_file($filepath) || !$limport->CHECK_FILE_EXT($filename))
{
	header("Location: import.php");
}
else
{
	$linterface->LAYOUT_START(urldecode($Msg));
	$ext = strtoupper($lf->file_ext($filename));
	if($limport->CHECK_FILE_EXT($filename))
	{
		# read file into array
		# return 0 if fail, return csv array if success
		//$data = $lf->file_read_csv($filepath);
		$data = $limport->GET_IMPORT_TXT($filepath);
		array_shift($data);                   # drop the title bar
	}
	
	// Create temp table
	$sql = "DROP TABLE TEMP_STUDENT_LEAVE_OPTION";
	$li->db_db_query($sql);
	$sql = "CREATE TABLE TEMP_STUDENT_LEAVE_OPTION (
			 UserID int,
			 ClassName varchar(20),
			 ClassNumber varchar(20),
			 Weekday0 int(11),
			 Weekday1 int(11),
			 Weekday2 int(11),
			 Weekday3 int(11),
			 Weekday4 int(11),
			 Weekday5 int(11),
			 Weekday6 int(11)
		    ) ENGINE=InnoDB charset=utf8";
	$li->db_db_query($sql);
	
	# Get UserID
    $sql = "SELECT UserID, ClassName, ClassNumber FROM INTRANET_USER
                   WHERE RecordType IN (2) AND RecordStatus = 1
                         AND ClassName!='' AND ClassNumber!=''";
    $temp = $li->returnArray($sql,3);

    for ($i=0; $i<sizeof($temp); $i++)
    {
         list($targetUserID, $targetClassName, $targetClassNumber) = $temp[$i];
         $student[$targetClassName][$targetClassNumber] = $targetUserID;
    }
   	
	$values = "";
    $delim = "";
    
	// Retrieve data from file
	for ($i=0; $i<sizeof($data); $i++)
	{
		list ($class_name, $class_number, $opt_monday, $opt_tuesday, $opt_wednesday, $opt_thursday, $opt_friday, $opt_saturday) = $data[$i];
		$targetUserID = $student[$class_name][$class_number];
 
		if($targetUserID != "")
		{
			if((($opt_monday >= 0) && ($opt_monday <=3)) && (is_numeric($opt_monday)))
			{
				if((($opt_tuesday >= 0) && ($opt_tuesday <=3)) && (is_numeric($opt_monday)))
				{
					if((($opt_wednesday >= 0) && ($opt_wednesday <=3)) && (is_numeric($opt_monday)))
					{
						if((($opt_thursday >= 0) && ($opt_thursday <=3)) && (is_numeric($opt_monday)))
						{
							if((($opt_friday >= 0) && ($opt_friday <=3)) && (is_numeric($opt_monday)))
							{
								if((($opt_saturday >= 0) && ($opt_saturday <=3)) && (is_numeric($opt_monday)))
								{
									continue;
								}
								else
								{
									$wrong_content[$i][error_code] = 6;
								}
							}
							else
							{
								$wrong_content[$i][error_code] = 5;
							}
						}
						else
						{
							$wrong_content[$i][error_code] = 4;
						}
					}
					else
					{
						$wrong_content[$i][error_code] = 3;
					}
				}
				else
				{
					$wrong_content[$i][error_code] = 2;
				}
			}
			else
			{
				$wrong_content[$i][error_code] = 1;
			}
		}
		else
		{
			$wrong_content[$i][error_code] = 7;
		}
	}

	if(sizeof($wrong_content) == 0)
	{
		for($i=0; $i<sizeof($data); $i++)
		{
			list ($class_name, $class_number, $opt_monday, $opt_tuesday, $opt_wednesday, $opt_thursday, $opt_friday, $opt_saturday) = $data[$i];
			$targetUserID = $student[$class_name][$class_number];
			
			if (($targetUserID != '') && ($opt_monday != '') && ($opt_tuesday != '') && ($opt_wednesday != '') && ($opt_thursday != '') && ($opt_friday != '') && ($opt_saturday != ''))
			{
				$values .= "$delim('$targetUserID','$class_name','$class_number','$opt_monday','$opt_tuesday','$opt_wednesday','$opt_thursday','$opt_friday','$opt_saturday')";
				$delim = ",";
			}
		}
		$sql = "INSERT INTO TEMP_STUDENT_LEAVE_OPTION (UserID, ClassName, ClassNumber, Weekday1, Weekday2, Weekday3, Weekday4, Weekday5, Weekday6) VALUES $values";

		$result = $li->db_db_query($sql);
	
		$name_field = getNameFieldByLang("b.");
		
		$sql = "SELECT 
						$name_field, a.ClassName, a.ClassNumber, a.Weekday0, a.Weekday1, a.Weekday2, a.Weekday3, a.Weekday4, a.Weekday5, a.Weekday6
		        FROM 
		        		TEMP_STUDENT_LEAVE_OPTION as a
		             	LEFT OUTER JOIN INTRANET_USER as b ON b.RecordType = 2 AND a.UserID = b.UserID ";
		
		$result = $li->returnArray($sql,10);
		
		$table_content .= '<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">';
		$table_content .= "<tr class=\"tabletop\">
												<td width=1>#</td>
												<td class=\"tabletoplink\">$i_UserStudentName</td>
												<td class=\"tabletoplink\" width=50>$i_ClassName</td>
												<td class=\"tabletoplink\" width=50>$i_ClassNumber</td>
												<td class=\"tabletoplink\" width=50>$i_StudentAttendance_LeaveOption_Weekday[1]</td>
												<td class=\"tabletoplink\" width=50>$i_StudentAttendance_LeaveOption_Weekday[2]</td>
												<td class=\"tabletoplink\" width=50>$i_StudentAttendance_LeaveOption_Weekday[3]</td>
												<td class=\"tabletoplink\" width=50>$i_StudentAttendance_LeaveOption_Weekday[4]</td>
												<td class=\"tabletoplink\" width=50>$i_StudentAttendance_LeaveOption_Weekday[5]</td>
												<td class=\"tabletoplink\" width=50>$i_StudentAttendance_LeaveOption_Weekday[6]</td>
											</tr>";
		
		for($i=0; $i<sizeof($result); $i++)
		{
			list($name, $class_name, $class_number, $opt_sunday, $opt_monday, $opt_tuesday, $opt_wednesday, $opt_thursday, $opt_friday, $opt_saturday) = $result[$i];
			$row = $i+1;
			$table_content .= "<tr class=TableContent><td>$row</td>";
			$table_content .= "<td>$name</td>";
			$table_content .= "<td>$class_name</td>";
			$table_content .= "<td>$class_number</td>";
			$table_content .= "<td>".$attendst_leave_option[$opt_monday][1]."</td>";
			$table_content .= "<td>".$attendst_leave_option[$opt_tuesday][1]."</td>";
			$table_content .= "<td>".$attendst_leave_option[$opt_wednesday][1]."</td>";
			$table_content .= "<td>".$attendst_leave_option[$opt_thursday][1]."</td>";
			$table_content .= "<td>".$attendst_leave_option[$opt_friday][1]."</td>";
			$table_content .= "<td>".$attendst_leave_option[$opt_saturday][1]."</td></tr>";
		}
		
		$table_content .= "</table>";
	}
	else
	{
		$table_content .= "<table width=560 border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align='center'>\n";
		$table_content .= "<tr class=TableTitle><td width=1>#</td><td width=70>$i_UserStudentName</td><td width=30>$i_ClassName</td><td width=30>$i_ClassNumber</td><td width=40>$i_StudentAttendance_LeaveOption_Weekday[1]</td><td width=40>$i_StudentAttendance_LeaveOption_Weekday[2]</td><td width=40>$i_StudentAttendance_LeaveOption_Weekday[3]</td><td width=40>$i_StudentAttendance_LeaveOption_Weekday[4]</td><td width=40>$i_StudentAttendance_LeaveOption_Weekday[5]</td><td width=40>$i_StudentAttendance_LeaveOption_Weekday[6]</td><td>$i_studentAttendance_ImportFailed_Reason</td></tr>";
		for($i=0; $i<sizeof($data); $i++)
		{
			list ($class_name, $class_number, $opt_monday, $opt_tuesday, $opt_wednesday, $opt_thursday, $opt_friday, $opt_saturday) = $data[$i];
			$targetUserID = $student[$class_name][$class_number];
			
			if((($opt_monday < 0) || ($opt_monday > 3)) || (!is_numeric($opt_monday)))
				$opt_monday = '-1';
			if((($opt_tuesday < 0) || ($opt_tuesday > 3)) || (!is_numeric($opt_tuesday)))
				$opt_monday = '-1';
			if((($opt_wednesday < 0) || ($opt_wednesday > 3)) || (!is_numeric($opt_wednesday)))
				$opt_monday = '-1';
			if((($opt_thursday < 0) || ($opt_thursday > 3)) || (!is_numeric($opt_thursday)))
				$opt_monday = '-1';
			if((($opt_friday < 0) || ($opt_friday > 3)) || (!is_numeric($opt_friday)))
				$opt_monday = '-1';
			if((($opt_monday < 0) || ($opt_saturday > 3)) || (!is_numeric($opt_saturday)))
				$opt_monday = '-1';
			
			$values .= "$delim('$targetUserID','$class_name','$class_number','$opt_monday','$opt_tuesday','$opt_wednesday','$opt_thursday','$opt_friday','$opt_saturday')";
			$delim = ",";
		}	
		$sql = "INSERT INTO TEMP_STUDENT_LEAVE_OPTION (UserID, ClassName, ClassNumber, Weekday1, Weekday2, Weekday3, Weekday4, Weekday5, Weekday6) VALUES $values";

		$result = $li->db_db_query($sql);
		
		$name_field = getNameFieldByLang("b.");
	
		$sql = "SELECT 
						IF($name_field IS NULL,'-',$name_field),
						IF(a.ClassName IS NULL,'-',a.ClassName),
						IF(a.ClassNumber IS NULL,'-',a.ClassNumber),
						a.Weekday0,
						a.Weekday1,
						a.Weekday2,
						a.Weekday3,
						a.Weekday4,
						a.Weekday5,
						a.Weekday6
		        FROM 
		        		TEMP_STUDENT_LEAVE_OPTION as a
		             	LEFT OUTER JOIN INTRANET_USER as b ON b.RecordType = 2 AND a.UserID = b.UserID ";
		
		$result = $li->returnArray($sql,10);

		for ($i=0; $i<sizeof($data); $i++)
		{
			if($wrong_content[$i][error_code]!='')
			{
				$row = $i+1;
				
				if ($wrong_content[$i][error_code] == 1)
					$error_msg = $i_studentAttendance_LeaveOption_Import_ErrorMsg[1];
				if ($wrong_content[$i][error_code] == 2)
					$error_msg = $i_studentAttendance_LeaveOption_Import_ErrorMsg[2];
				if ($wrong_content[$i][error_code] == 3)
					$error_msg = $i_studentAttendance_LeaveOption_Import_ErrorMsg[3];
				if ($wrong_content[$i][error_code] == 4)
					$error_msg = $i_studentAttendance_LeaveOption_Import_ErrorMsg[4];
				if ($wrong_content[$i][error_code] == 5)
					$error_msg = $i_studentAttendance_LeaveOption_Import_ErrorMsg[5];
				if ($wrong_content[$i][error_code] == 6)
					$error_msg = $i_studentAttendance_LeaveOption_Import_ErrorMsg[6];
				if ($wrong_content[$i][error_code] == 7)
					$error_msg = $i_studentAttendance_LeaveOption_Import_ErrorMsg[7];
					
				$sub_table_content .= $i_studentAttendance_Import_Row." ".$row." - ".$error_msg."<br>";
			}
		}
		for($i=0; $i<sizeof($result); $i++)
		{
			list($name, $class_name, $class_number, $opt_sunday, $opt_monday, $opt_tuesday, $opt_wednesday, $opt_thursday, $opt_friday, $opt_saturday) = $result[$i];
			$row = $i+1;
			$table_content .= "<tr class=TableContent><td>$row</td>";
			$table_content .= "<td>$name</td>";
			$table_content .= "<td>$class_name</td>";
			$table_content .= "<td>$class_number</td>";
			if($attendst_leave_option[$opt_monday][1] != "")
				$table_content .= "<td>".$attendst_leave_option[$opt_monday][1]."</td>";
			else
				$table_content .= "<td> - </td>";
				
			if($attendst_leave_option[$opt_tuesday][1] != "")
				$table_content .= "<td>".$attendst_leave_option[$opt_tuesday][1]."</td>";
			else
				$table_content .= "<td> - </td>";
				
			if($attendst_leave_option[$opt_wednesday][1] != "")
				$table_content .= "<td>".$attendst_leave_option[$opt_wednesday][1]."</td>";
			else
				$table_content .= "<td> - </td>";
				
			if($attendst_leave_option[$opt_thursday][1] != "")
				$table_content .= "<td>".$attendst_leave_option[$opt_thursday][1]."</td>";
			else
				$table_content .= "<td> - </td>";
				
			if($attendst_leave_option[$opt_friday][1] != "")
				$table_content .= "<td>".$attendst_leave_option[$opt_friday][1]."</td>";
			else
				$table_content .= "<td> - </td>";
				
			if($attendst_leave_option[$opt_saturday][1] != "")
				$table_content .= "<td>".$attendst_leave_option[$opt_saturday][1]."</td>";
			else
				$table_content .= "<td> - </td>";
			
			if ($wrong_content[$i][error_code] == 1)
			{
				$error_msg = $i_studentAttendance_LeaveOption_Import_ErrorMsg[1];
				$table_content .= "<td>".$error_msg."</td></tr>";
			}
			elseif ($wrong_content[$i][error_code] == 2)
			{
				$error_msg = $i_studentAttendance_LeaveOption_Import_ErrorMsg[2];
				$table_content .= "<td>".$error_msg."</td></tr>";
			}
			elseif ($wrong_content[$i][error_code] == 3)
			{
				$error_msg = $i_studentAttendance_LeaveOption_Import_ErrorMsg[3];
				$table_content .= "<td>".$error_msg."</td></tr>";
			}
			elseif ($wrong_content[$i][error_code] == 4)
			{
				$error_msg = $i_studentAttendance_LeaveOption_Import_ErrorMsg[4];
				$table_content .= "<td>".$error_msg."</td></tr>";
			}
			elseif ($wrong_content[$i][error_code] == 5)
			{
				$error_msg = $i_studentAttendance_LeaveOption_Import_ErrorMsg[5];
				$table_content .= "<td>".$error_msg."</td></tr>";
			}
			elseif ($wrong_content[$i][error_code] == 6)
			{
				$error_msg = $i_studentAttendance_LeaveOption_Import_ErrorMsg[6];
				$table_content .= "<td>".$error_msg."</td></tr>";
			}
			elseif ($wrong_content[$i][error_code] == 7)
			{
				$error_msg = $i_studentAttendance_LeaveOption_Import_ErrorMsg[7];
				$table_content .= "<td>".$error_msg."</td></tr>";
			}
			else
			{
				$table_content .= "<td> - </td></tr>";
			}
		}
		
		//$table_content .= "<tr class=TableContent><td colspan=10 align=left>
		//					$i_studentAttendance_InvalidImportFormatWarning<br><br>$i_general_ImportFailed<br>$sub_table_content
		//					</td></tr>";
		$table_content .= "</table>";
	}
}
?>

<form name=form1 action="import_confirm.php" method="POST">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	</tr>
	<tr><td>&nbsp;</td></tr>
<table>
<table border="0" cellpadding="0" cellspacing="0" align=center>
<tr><td><?=$table_content?></td></tr>
</table>

<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
	    <td align="center" colspan="2">
			<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "") ?>
			<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:history.back()","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
		</td>
	</tr>
</table>

</form>

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>