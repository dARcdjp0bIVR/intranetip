<?php

$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();

$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageOtherFeatures_LeaveSchoolOption";
$linterface = new interface_html();

$lclass = new libclass();

if($TargetClass != "")
{
	$namefield = getNameFieldWithClassNumberByLang("a.");
	$sql = "SELECT
					a.UserID,
					$namefield
			FROM
					INTRANET_USER AS a 
			WHERE
					a.ClassName = '$TargetClass' AND 
					a.RecordType=2 AND 
					a.RecordStatus=1
			ORDER BY
					a.ClassName, a.ClassNumber, a.EnglishName";
	//echo $sql;
	$result = $lclass->returnArray($sql,2);
	
	for($i=0; $i<sizeof($result); $i++)
	{
		list($user_id, $name) = $result[$i];
		$student_name[$user_id] = $name;
	}
	
	$table_content .= '<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">';
	$table_content .= "<tr><td>$i_studentAttendance_ConfirmMsg</td></tr>";
	$table_content .= "</table>";
	$table_content .= "<br>";
	$table_content .= '<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">';
	$table_content .= "<tr class=\"tabletop\">
							<td class=\"tabletoplink\" width=100>$i_UserStudentName</td>
							<td class=\"tabletoplink\" width=60>$i_StudentAttendance_LeaveOption_Weekday[1]</td>
							<td class=\"tabletoplink\" width=60>$i_StudentAttendance_LeaveOption_Weekday[2]</td>
							<td class=\"tabletoplink\" width=60>$i_StudentAttendance_LeaveOption_Weekday[3]</td>
							<td class=\"tabletoplink\" width=60>$i_StudentAttendance_LeaveOption_Weekday[4]</td>
							<td class=\"tabletoplink\" width=60>$i_StudentAttendance_LeaveOption_Weekday[5]</td>
							<td class=\"tabletoplink\" width=60>$i_StudentAttendance_LeaveOption_Weekday[6]</td></tr>\n";
							
	if(sizeof($student)>0)
	{
		for($i=0; $i<sizeof($student); $i++)
		{
			$opt_1 = $attendst_leave_option[${"opt_$student[$i]_1"}][1];
			$opt_2 = $attendst_leave_option[${"opt_$student[$i]_2"}][1];
			$opt_3 = $attendst_leave_option[${"opt_$student[$i]_3"}][1];
			$opt_4 = $attendst_leave_option[${"opt_$student[$i]_4"}][1];
			$opt_5 = $attendst_leave_option[${"opt_$student[$i]_5"}][1];
			$opt_6 = $attendst_leave_option[${"opt_$student[$i]_6"}][1];
	
			$uid = $student[$i];
			$table_content .= "<tr><td>$student_name[$uid]</td>\n";
			$table_content .= "<td>$opt_1<input type=hidden name=opt_".$student[$i]."_1 value=".${"opt_$student[$i]_1"}."></td>\n";
			$table_content .= "<td>$opt_2<input type=hidden name=opt_".$student[$i]."_2 value=".${"opt_$student[$i]_2"}."></td>\n";
			$table_content .= "<td>$opt_3<input type=hidden name=opt_".$student[$i]."_3 value=".${"opt_$student[$i]_3"}."></td>\n";
			$table_content .= "<td>$opt_4<input type=hidden name=opt_".$student[$i]."_4 value=".${"opt_$student[$i]_4"}."></td>\n";
			$table_content .= "<td>$opt_5<input type=hidden name=opt_".$student[$i]."_5 value=".${"opt_$student[$i]_5"}."></td>\n";
			$table_content .= "<td>$opt_6<input type=hidden name=opt_".$student[$i]."_6 value=".${"opt_$student[$i]_6"}."></td>\n";
			$table_content .= "<input type=hidden name=student[] value=$student[$i]>";
		}
	}
	$table_content .= "</table>\n";
	$table_content .= '<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
											<tr>
											    <td align="center" colspan="2">
													'.$linterface->GET_ACTION_BTN($button_save, "submit", "").'
													'.$linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:window.location = 'edit.php';","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"").'
												</td>
											</tr>
										</table>';
}

$TAGS_OBJ[] = array($button_edit, "edit.php", 1);
$TAGS_OBJ[] = array($button_import, "import.php", 0);

$MODULE_OBJ = $lc->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();
?>
<SCRIPT LANGUAGE="JavaScript">
function setAll(weekday,checked)
{
	if(checked == 1)
	{
		<? 
		for($i=0; $i<sizeof($result); $i++) 
		{
			list($uid, $student_name, $opt_mon, $opt_tue, $opt_wed, $opt_thur, $opt_fri, $opt_sat) = $result[$i];
		?>
			var temp = eval("document.form1.opt_"+<?=$uid?>+"_"+weekday);
			temp.value = eval("document.form1.opt_all_"+weekday+".value");
		<?
		}
		?>
	}
	if(checked == 0)
	{
		<? 
		for($i=0; $i<sizeof($result); $i++) 
		{
			list($uid, $student_name, $opt_mon, $opt_tue, $opt_wed, $opt_thur, $opt_fri, $opt_sat) = $result[$i];
		?>
			var temp = eval("document.form1.opt_"+<?=$uid?>+"_"+weekday);
			temp.value = 0;
		<?
		}
		?>
	}
}
function setAllUserOnly(user_id,checked)
{
	if(checked == 1)
	{
		for(i=1; i<7; i++)
		{
			var temp = eval("document.form1.opt_"+user_id+"_"+i);
			temp.value = eval("document.form1.opt_"+user_id+"_all.value");
		}
	}
	if(checked == 0)
	{
		for(i=1;i<7;i++)
		{
			var temp = eval("document.form1.opt_"+user_id+"_"+i);
			temp.value = 0;
		}
	}	
}
function changeAll(weekday)
{
	var temp = eval("document.form1.all_"+weekday);
	if(temp.checked)
	{
		<?
		for($i=0; $i<sizeof($result); $i++) 
		{
			list($uid, $student_name, $opt_mon, $opt_tue, $opt_wed, $opt_thur, $opt_fri, $opt_sat) = $result[$i];
		?>
			var temp = eval("document.form1.opt_"+<?=$uid?>+"_"+weekday);
			temp.value = eval("document.form1.opt_all_"+weekday+".value");
		<?
		}
		?>
	}
}
function changeAllUserOnly(user_id)
{
	var temp = eval("document.form1.all_"+user_id);
	if(temp.checked)
	{
		for(i=1;i<7;i++)
		{
			var temp = eval("document.form1.opt_"+user_id+"_"+i);
			temp.value = eval("document.form1.opt_"+user_id+"_all.value");
		}
	}
}
</SCRIPT>

<form name="form1" action="edit_update.php" method="POST">
<?=$table_content?>
</form>

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>	