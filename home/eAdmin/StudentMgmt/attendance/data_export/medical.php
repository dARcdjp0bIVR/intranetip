<?php
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();
$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageDataExport_Medical";

 # Retrieve Class Level List
 $sql = "Select 
 					yc.ClassTitleEN as ClassName, 
 					y.WEBSAMSCode as WebSAMSLevel 
 				From 
 					YEAR as y 
 					inner join 
 					YEAR_CLASS as yc 
 					on y.YearID = yc.YearID and yc.AcademicYearID = '".Get_Current_Academic_Year_ID()."' 
        ";
 $classes = $lc->returnArray($sql,2);
 
 $no_websamslevel = true;
 
$available_level = $lc->returnAvailableWebSamsLevel();
 for ($i=0; $i<sizeof($classes); $i++)
 {
      list($t_classname, $t_webSamsClassLevel) = $classes[$i];
      if (in_array($t_webSamsClassLevel, $available_level))
      {
          $no_websamslevel = false;
      }
 }

# default date range  = 1 Month
$currentMonth = date('n');
$FromDate = date('Y-m-d',mktime(0,0,0,$currentMonth,1));  // first day of current month
$ToDate   = date('Y-m-d',mktime(0,0,0,$currentMonth+1,0)); // last day of current month



$linterface = new interface_html();



$TAGS_OBJ[] = array($i_MedicalReason_Export2, "", 0);



$MODULE_OBJ = $lc->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();


?>
<br />
<form name="form1" method="GET" action="medical_export.php">
<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
<?php
		if($no_websamslevel){
	 			echo "<tr><td colspan='2' align='right' class='tabletext'><font color=red>$i_MedicalExportError</font></td></tr>";
	 	}
?>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_general_startdate?></td>
		<td width="70%"class="tabletext"><?=$linterface->GET_DATE_PICKER("FromDate",$FromDate)?></td>
	</tr>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_general_enddate?></td>
		<td width="70%"class="tabletext"><?=$linterface->GET_DATE_PICKER("ToDate",$ToDate)?></td>
	</tr>
</table>
<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
    	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
    </tr>
</table>
<?php if(!$no_websamslevel){?>
<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
	    <td align="center" colspan="2">
			<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "") ?>
		</td>
	</tr>
</table>
<?php } ?>
</form>
<br />
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
