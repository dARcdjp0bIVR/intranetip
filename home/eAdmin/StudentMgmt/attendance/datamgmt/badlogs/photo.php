<?php
// Editing by 
/*
 * 2017-05-17 (Carlos): created.
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent'] || !$sys_custom['StudentAttendance']['NoCardPhoto']) {
	intranet_closedb();
	header ("Location: /");
	exit();
}

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

# preserve table view
if ($ck_attend_datamgmt_badlogs_browse_page_number!=$pageNo && $pageNo!="")
{
	setcookie("ck_attend_datamgmt_badlogs_browse_page_number", $pageNo, 0, "", "", 0);
	$ck_attend_datamgmt_badlogs_browse_page_number = $pageNo;
} else if (!isset($pageNo) && $ck_attend_datamgmt_badlogs_browse_page_number!="")
{
	$pageNo = $ck_attend_datamgmt_badlogs_browse_page_number;
}

if ($ck_attend_datamgmt_badlogs_browse_page_order!=$order && $order!="")
{
	setcookie("ck_attend_datamgmt_badlogs_browse_page_order", $order, 0, "", "", 0);
	$ck_attend_datamgmt_badlogs_browse_page_order = $order;
} else if (!isset($order) && $ck_attend_datamgmt_badlogs_browse_page_order!="")
{
	$order = $ck_attend_datamgmt_badlogs_browse_page_order;
}

if ($ck_attend_datamgmt_badlogs_browse_page_field!=$field && $field!="")
{
	setcookie("ck_attend_datamgmt_badlogs_browse_page_field", $field, 0, "", "", 0);
	$ck_attend_datamgmt_badlogs_browse_page_field = $field;
} else if (!isset($field) && $ck_attend_datamgmt_badlogs_browse_page_field!="")
{
	$field = $ck_attend_datamgmt_badlogs_browse_page_field;
}

if ($ck_attend_datamgmt_badlogs_browse_startdate_field!=$StartDate && $StartDate!="")
{
	setcookie("ck_attend_datamgmt_badlogs_browse_startdate_field", $StartDate, 0, "", "", 0);
	$ck_attend_datamgmt_badlogs_browse_startdate_field = $StartDate;
} else if (!isset($StartDate) && $ck_attend_datamgmt_badlogs_browse_startdate_field!="")
{
	$StartDate = $ck_attend_datamgmt_badlogs_browse_startdate_field;
}

if ($ck_attend_datamgmt_badlogs_browse_enddate_field!=$EndDate && $EndDate!="")
{
	setcookie("ck_attend_datamgmt_badlogs_browse_enddate_field", $EndDate, 0, "", "", 0);
	$ck_attend_datamgmt_badlogs_browse_enddate_field = $EndDate;
} else if (!isset($EndDate) && $ck_attend_datamgmt_badlogs_browse_enddate_field!="")
{
	$EndDate = $ck_attend_datamgmt_badlogs_browse_enddate_field;
}

if (isset($Keyword) && $ck_attend_datamgmt_badlogs_browse_keyword_field!=$Keyword)
{
	setcookie("ck_attend_datamgmt_badlogs_browse_keyword_field", $Keyword, 0, "", "", 0);
	$ck_attend_datamgmt_badlogs_browse_keyword_field = $Keyword;
} else if (!isset($Keyword) && isset($ck_attend_datamgmt_badlogs_browse_keyword_field))
{
	$Keyword = $ck_attend_datamgmt_badlogs_browse_keyword_field;
}

$lc = new libcardstudentattend2();
$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageDataManagement_BadLogs";

$linterface = new interface_html();

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$li = new libdbtable2007($field, $order, $pageNo);

$today = date("Y-m-d");
if($StartDate == '') $StartDate = $today;
if($EndDate == '') $EndDate = $today;

$filterMap = array('GetDBQuery'=>1,'libdbtable'=>$li);
$filterMap['StartDate'] = $StartDate;
$filterMap['EndDate'] = $EndDate;
if(isset($Keyword) && trim($Keyword)!=''){
	$filterMap['Keyword'] = $Keyword;
}
$db_query_info = $lc->getNoCardPhotoRecords($filterMap);

$li->field_array = $db_query_info[0];
$li->sql = $db_query_info[1];
$li->no_col = sizeof($li->field_array)+2;
$li->title = "";
$li->column_array = $db_query_info[3];
$li->wrap_array = $db_query_info[4];
$li->IsColOff = "IP25_table";

if ($field=="" && $order=="") {
	$li->field = 0;
	$li->order = 0;
}

foreach($db_query_info[2] as $column_def)
{
	$li->column_list .= $column_def;
}

//$toolbar .= $linterface->GET_LNK_EXPORT("javascript:goExport()",$button_export,"","","",0);

$tool_buttons = array();
$tool_buttons[] = array('delete',"javascript:checkRemove(document.form1,'RecordID[]','delete_photo.php')");

$TAGS_OBJ[] = array($i_StudentAttendance_BadLogs_Type_NotInLunchList, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/datamgmt/badlogs/browse.php?type=1", ($type==CARD_BADACTION_LUNCH_NOTINLIST)?1:0);
$TAGS_OBJ[] = array($i_StudentAttendance_BadLogs_Type_GoLunchAgain, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/datamgmt/badlogs/browse.php?type=2", ($type==CARD_BADACTION_LUNCH_BACKALREADY)?1:0);
$TAGS_OBJ[] = array($i_StudentAttendance_BadLogs_Type_FakedCardAM, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/datamgmt/badlogs/browse.php?type=3", ($type==CARD_BADACTION_FAKED_CARD_AM)?1:0);
$TAGS_OBJ[] = array($i_StudentAttendance_BadLogs_Type_FakedCardPM, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/datamgmt/badlogs/browse.php?type=4", ($type==CARD_BADACTION_FAKED_CARD_PM)?1:0);
$TAGS_OBJ[] = array($i_StudentAttendance_BadLogs_Type_NoCardRecord, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/datamgmt/badlogs/browse.php?type=5", ($type==CARD_BADACTION_NO_CARD_ENTRANCE)?1:0);
$TAGS_OBJ[] = array($Lang['StudentAttendance']['NoBringCardPhoto'],'photo.php', 1);

if(isset($_SESSION['STUDENT_ATTENDANCE_NO_CARD_PHOTO_RETURN_MSG']))
{
	$ReturnMsg = $_SESSION['STUDENT_ATTENDANCE_NO_CARD_PHOTO_RETURN_MSG'];
	unset($_SESSION['STUDENT_ATTENDANCE_NO_CARD_PHOTO_RETURN_MSG']);
}
if($xmsg != '' &&  isset($Lang['General']['ReturnMessage'][$xmsg]))
{
	$ReturnMsg = $Lang['General']['ReturnMessage'][$xmsg];
}
$MODULE_OBJ = $lc->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START($ReturnMsg);
?>
<link rel="stylesheet" href="/templates/jquery/fancybox-1.3.4/jquery.fancybox-1.3.4.css" type="text/css" media="screen" />
<script type="text/javascript" src="/templates/jquery/fancybox-1.3.4/jquery.easing-1.3.pack.js"></script>
<script type="text/javascript" src="/templates/jquery/fancybox-1.3.4/jquery.mousewheel-3.0.4.pack.js"></script>
<script type="text/javascript" src="/templates/jquery/fancybox-1.3.4/jquery.fancybox-1.3.4.js"></script>
<br />
<form name="form1" method="post" action="">
<div class="content_top_tool">
	<?=$toolbar?>
	<?=$linterface->Get_Search_Box_Div("Keyword", trim($lc->is_magic_quotes_active? stripslashes($Keyword): $Keyword), ' onkeyup="GoSearch(event);" ')?>
	<br style="clear:both" />
</div>
<div class="table_board">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
	<td valign="bottom">
		<?=$Lang['General']['From'].'&nbsp;'.$linterface->GET_DATE_PICKER("StartDate",$StartDate).$Lang['General']['To'].'&nbsp;'.$linterface->GET_DATE_PICKER("EndDate",$EndDate).$linterface->GET_SMALL_BTN($Lang['Btn']['Apply'], "button", "document.form1.submit();","apply_btn")?>
	</td>
	<td valign="bottom">
		<?=$linterface->Get_DBTable_Action_Button_IP25($tool_buttons)?>
	</td>
</tr>
</table>
<?= $li->display()?>
<input type="hidden" name="pageNo" id="pageNo" value="<?=$li->pageNo; ?>" />
<input type="hidden" name="order" id="order" value="<?=$li->order; ?>" />
<input type="hidden" name="field" id="field" value="<?=$li->field; ?>" />
<input type="hidden" name="page_size_change" id="page_size_change" value="" />
<input type="hidden" name="numPerPage" id="numPerPage" value="<?=$li->page_size?>" />
</form>
<script type="text/javascript" language="javascript">
function GoSearch(evt)
{
	var key = evt.which || evt.charCode || evt.keyCode;
	
	if (key == 13) // enter
		document.form1.submit();
	else
		return false;
}
$(document).ready(function(){
	$('a.fancybox').fancybox();
});
</script>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>