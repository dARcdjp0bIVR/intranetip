<?php
include_once("../../../../includes/global.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libclass.php");
include_once("../../../../includes/libcardstudentattend2.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../templates/adminheader_setting.php");

$lcard = new libcardstudentattend2();
$lcard->retrieveSettings();

# Get Lunch Settings
$content_lunch_misc = trim(get_file_content("$intranet_root/file/stattend_lunch_misc.txt"));
$lunch_misc_settings = explode("\n",$content_lunch_misc);
list($lunch_misc_no_need_record,$lunch_misc_once_only,$lunch_misc_all_allowed) = $lunch_misc_settings;


?>

<?= displayNavTitle($i_adminmenu_plugin, '', $i_StudentAttendance_System, '../../',$i_StudentAttendance_Menu_DataManagement,'../',$i_StudentAttendance_Menu_DataMgmt_BadLogs,'') ?>
<?= displayTag("head_smartcardattendence_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align=center>
<td class=tableContent height=300 align=left>
<blockquote>
<?= displayOption(
                  $i_StudentAttendance_BadLogs_Type_NotInLunchList,'browse.php?type='.CARD_BADACTION_LUNCH_NOTINLIST,($lcard->attendance_mode==2 && !$lunch_misc_no_need_record),
                  $i_StudentAttendance_BadLogs_Type_GoLunchAgain,'browse.php?type='.CARD_BADACTION_LUNCH_BACKALREADY,($lcard->attendance_mode==2 && $lunch_misc_once_only && !$lunch_misc_no_need_record),
                  $i_StudentAttendance_BadLogs_Type_FakedCardAM,'browse.php?type='.CARD_BADACTION_FAKED_CARD_AM,1,
                  $i_StudentAttendance_BadLogs_Type_FakedCardPM,'browse.php?type='.CARD_BADACTION_FAKED_CARD_PM,1,
                  $i_StudentAttendance_BadLogs_Type_NoCardRecord,'browse.php?type='.CARD_BADACTION_NO_CARD_ENTRANCE,1
                                ) ?>
</blockquote>
</td></tr>
</table>

<?
include_once("../../../../templates/adminfooter.php");
?>