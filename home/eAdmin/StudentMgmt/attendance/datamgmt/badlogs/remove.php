<?php
// Editing by
/*
 * 2019-07-03 Ray:    Created
 *
 */

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$can_delete_log = false;
if($sys_custom['StudentAttendance']['CanDeletebadRecordLog'] == true) {
	if ($type == CARD_BADACTION_LUNCH_NOTINLIST || $type == CARD_BADACTION_LUNCH_BACKALREADY || $type == CARD_BADACTION_FAKED_CARD_AM || $type == CARD_BADACTION_FAKED_CARD_PM || $type == CARD_BADACTION_NO_CARD_ENTRANCE) {
		$can_delete_log = true;
	}
}

$li = new libcardstudentattend2();
$li->Start_Trans();

$Result = false;

if($can_delete_log == true) {
	if ($TargetIds != '') {
		$delete_list = str_replace(",", "','", $TargetIds);
		$sql = "DELETE FROM CARD_STUDENT_BAD_ACTION
				WHERE RecordID IN ('$delete_list') AND RecordType='$type'";
		$Result = $li->db_db_query($sql);
	}
}

if ($Result) {
	$Msg = $Lang['StudentAttendance']['SettingApplySuccess'];
	$li->Commit_Trans();
}
else {
	$Msg = $Lang['StudentAttendance']['SettingApplyFail'];
	$li->RollBack_Trans();
}

intranet_closedb();
header("Location: browse.php?type=".$type."&Msg=".urlencode($Msg));
?>