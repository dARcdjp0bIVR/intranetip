<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();

if ($type > 5 || $type < 1) $type = 1;
$order = ($order == 1) ? 1 : 0;
$field = ($field == "") ? 0 : $field;
$field_array = array("a.RecordDate","b.ClassName","b.ClassNumber","b.EnglishName");
if ($type==CARD_BADACTION_LUNCH_NOTINLIST || $type==CARD_BADACTION_LUNCH_BACKALREADY || $type==CARD_BADACTION_FAKED_CARD_AM || $type==CARD_BADACTION_FAKED_CARD_PM) 
	$field_array[] = "a.RecordTime";

$today_ts = strtotime(date('Y-m-d'));
$StartDate = ($_REQUEST['StartDate'])? $_REQUEST['StartDate']:date('Y-m-d',getStartOfAcademicYear($today_ts));
$EndDate = ($_REQUEST['EndDate'])? $_REQUEST['EndDate']:date('Y-m-d',getEndOfAcademicYear($today_ts));

$user_field = getNameFieldByLang("b.");

$sql  = "SELECT
        	a.RecordDate, 
        	b.ClassName, 
        	b.ClassNumber, 
        	$user_field ";
if ($type==CARD_BADACTION_LUNCH_NOTINLIST || $type==CARD_BADACTION_LUNCH_BACKALREADY || $type==CARD_BADACTION_FAKED_CARD_AM || $type==CARD_BADACTION_FAKED_CARD_PM) {
	$sql .= "
					,a.RecordTime ";
}
$sql .= "
     		FROM
        	CARD_STUDENT_BAD_ACTION as a ";
if ($lc->EnableEntryLeavePeriod) {
	$sql .= "
					INNER JOIN 
          CARD_STUDENT_ENTRY_LEAVE_PERIOD as selp 
          on a.StudentID = selp.UserID 
          	AND 
          	a.RecordDate between selp.PeriodStart and CONCAT(selp.PeriodEnd,' 23:59:59') ";
}
$sql .= "       	
					LEFT OUTER JOIN 
        	INTRANET_USER as b 
        	ON a.StudentID = b.UserID AND b.RecordType = 2
     		WHERE
          a.RecordType = '$type' 
          AND 
          a.RecordDate between '".$StartDate."' and '".$EndDate." 23:59:59' 
          AND 
          (b.ChineseName like '%$keyword%'
           OR b.EnglishName like '%$keyword%'
           OR b.ClassName like '$keyword'
           OR b.ClassNumber like '%$keyword%'
           OR a.RecordDate = '$keyword'
           )
          ORDER BY ".$field_array[$field].( ($order==0) ? " DESC" : " ASC");
$li = new libdb();
$result = $li->returnArray($sql, sizeof($field_array));

$lexport = new libexporttext();

if (sizeof($field_array) == 5)
	$exportColumn = array("Record Date", "Class Name", "Class No", "Student Name", "Record Time");
else
	$exportColumn = array("Record Date", "Class Name", "Class No", "Student Name");

$export_content = $lexport->GET_EXPORT_TXT($result, $exportColumn);

// Output the file to user browser
$filename = "badlogs_".$type.".csv";

intranet_closedb();
$lexport->EXPORT_FILE($filename, $export_content);
?>