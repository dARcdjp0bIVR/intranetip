<?php
include_once("../../../../includes/global.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libclass.php");
include_once("../../../../includes/libcardstudentattend2.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../templates/adminheader_setting.php");
intranet_opendb();

#class used
$LICS = new libcardstudentattend2();
$LICS->retrieveSettings();

### Set Date from previous page
$ts_record = strtotime($TargetDate);
if ($ts_record == -1)
{
    $ts_record = strtotime(date('Y-m-d'));
}
$txt_year = date('Y',$ts_record);
$txt_month = date('m',$ts_record);
$txt_day = date('d',$ts_record);

### create table if needed
#$LICS->createTable_LogAndConfirm($txt_year,$txt_month);
$sql = "SELECT RecordID, LateConfirmed, LateConfirmTime,
               AbsenceConfirmed, AbsenceConfirmTime,
               EarlyConfirmed, EarlyConfirmTime,  RecordType
               FROM CARD_STUDENT_DAILY_DATA_CONFIRM
               WHERE RecordDate = '$TargetDate' ORDER BY RecordType";
$temp = $LICS->returnArray($sql,8);
for ($i=0; $i<sizeof($temp); $i++)
{
     list($recordid, $lateConfirm, $lateTime, $absConfirm, $absTime,$earlyConfirm,$earlyTime, $daytype) = $temp[$i];
     if ($daytype == PROFILE_DAY_TYPE_AM)
     {
         $amLateConfirm = $lateConfirm;
         $amLateTime = $lateTime;
         $amAbsConfirm = $absConfirm;
         $amAbsTime = $absTime;
         $amEarlyConfirm = $earlyConfirm;
         $amEarlyTime = $earlyTime;
     }
     else
     {
         $pmLateConfirm = $lateConfirm;
         $pmLateTime = $lateTime;
         $pmAbsConfirm = $absConfirm;
         $pmAbsTime = $absTime;
         $pmEarlyConfirm = $earlyConfirm;
         $pmEarlyTime = $earlyTime;
     }
}

if ($allClass)        # All Classes
{
    $txt_class = "$i_status_all $i_SmartCard_ClassName";
    # Get Profile Records
    $sql = "SELECT RecordType, COUNT(StudentAttendanceID)
                   FROM PROFILE_STUDENT_ATTENDANCE WHERE AttendanceDate = '$TargetDate'
                   GROUP BY RecordType";
    $profile_count = $LICS->returnArray($sql,2);
}
else
{
    # Get Class names
    $list = implode(",",$ClassID);
    $sql = "SELECT ClassID, ClassName, ClassLevelID FROM INTRANET_CLASS WHERE ClassID IN ($list)";
    $classes = $LICS->returnArray($sql,3);
    $txt_class = "";
    $curr_lvl = "";
    $delim = "";
    $classname_list = "";
    $classname_delim = "";
    for ($i=0; $i<sizeof($classes); $i++)
    {
         list($classID, $classname, $classlevel) = $classes[$i];
         if ($classlevel!=$curr_lvl && $curr_lvl!="")
         {
             $txt_class .= "<br>\n";
             $delim = "";
         }
         $curr_lvl = $classlevel;
         $txt_class .= "$delim$classname &nbsp;\n";
         $delim = ",";

         $classname_list .= "$classname_delim '$classname'";
         $classname_delim = ',';
    }
    # Get Student List
    $sql = "SELECT UserID FROM INTRANET_USER WHERE RecordType = 2 AND ClassName IN ($classname_list)";
    $temp_students = $LICS->returnVector($sql);
    if (sizeof($temp_students)!=0)
    {
        $temp_student_list = implode(",",$temp_students);
        # Get Profile Records Count
        $sql = "SELECT RecordType, COUNT(StudentAttendanceID)
                       FROM PROFILE_STUDENT_ATTENDANCE WHERE AttendanceDate = '$TargetDate'
                       AND UserID IN ($temp_student_list)
                       GROUP BY RecordType";

        $profile_count = $LICS->returnArray($sql,2);
    }
}

for ($i=0; $i<sizeof($profile_count); $i++)
{
     list ($type, $count) = $profile_count[$i];
     switch ($type)
     {
             case PROFILE_TYPE_ABSENT: $count_abs = $count; break;
             case PROFILE_TYPE_LATE: $count_late = $count; break;
             case PROFILE_TYPE_EARLY: $count_early = $count; break;
             default:
     }
}
?>

<form name="form1" method="post" action="show_update.php">
<?= displayNavTitle($i_adminmenu_plugin, '', $i_StudentAttendance_System, '../../',$i_StudentAttendance_Menu_DataManagement,'../',$i_StudentAttendance_Menu_DataMgmt_UndoPastProfileRecord, 'index.php',$TargetDate,'') ?>
<?= displayTag("head_smartcardattendence_$intranet_session_language.gif", $msg) ?>
<table width=560 border=0 cellpadding=4 cellspacing=1 align="center">
<tr><td align=right><?=$i_StudentAttendance_Field_Date?>:</td><td><?=$TargetDate?></td></tr>
<tr><td align=right><?=$i_SmartCard_ClassName?>:</td><td><?=$txt_class?></td></tr>
<? if ($LICS->attendance_mode != 1) { ?>
<tr><td align=right><?=$i_StudentAttendance_Field_LastConfirmedTime." ($i_Profile_Absent)"?>:</td><td><?=($amAbsConfirm?$amAbsTime:"$i_SmartCard_Frontend_Take_Attendance_No_Confirm_Record")?></td></tr>
<tr><td align=right><?=" ($i_Profile_Late)"?>:</td><td><?=($amLateConfirm?$amLateTime:"$i_SmartCard_Frontend_Take_Attendance_No_Confirm_Record")?></td></tr>
<tr><td align=right><?=" ($i_Profile_EarlyLeave)"?>:</td><td><?=($amEarlyConfirm?$amEarlyTime:"$i_SmartCard_Frontend_Take_Attendance_No_Confirm_Record")?></td></tr>
<? } ?>

<? if ($LICS->attendance_mode != 0) { ?>
<tr><td align=right><?=$i_StudentAttendance_Field_LastConfirmedTime." ($i_Profile_Absent)"?>:</td><td><?=($pmAbsConfirm?$pmAbsTime:"$i_SmartCard_Frontend_Take_Attendance_No_Confirm_Record")?></td></tr>
<tr><td align=right><?=" ($i_Profile_Late)"?>:</td><td><?=($pmLateConfirm?$pmLateTime:"$i_SmartCard_Frontend_Take_Attendance_No_Confirm_Record")?></td></tr>
<tr><td align=right><?=" ($i_Profile_EarlyLeave)"?>:</td><td><?=($pmEarlyConfirm?$pmEarlyTime:"$i_SmartCard_Frontend_Take_Attendance_No_Confirm_Record")?></td></tr>
<? } ?>

<tr><td align=right><?=$i_Profile_Absent?>:</td><td><?=($count_abs+0)?></td></tr>
<tr><td align=right><?=$i_Profile_Late?>:</td><td><?=($count_late+0)?></td></tr>
<tr><td align=right><?=$i_Profile_EarlyLeave?>:</td><td><?=($count_early+0)?></td></tr>
</table>
<blockquote>
<?=$i_StudentAttendance_UndoProfile_Warning?>
</blockquote>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><hr size=1></td></tr>
<tr><td align="right">
<input type=image src="<?=$image_path?>/admin/button/s_btn_continue_<?=$intranet_session_language?>.gif">
<a href="javascript:history.back()"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>
<?
if ($allClass)        # All Classes
{
?>
<input type=hidden name=allClass value=1>
<?
}
else
{
    for ($i=0; $i<sizeof($ClassID); $i++)
    {
    ?>
<input type=hidden name=ClassID[] value="<?=$ClassID[$i]?>">
    <?
    }
}
?>
<input type=hidden name=TargetDate value="<?=$TargetDate?>">
</form>
<?
include_once("../../../../templates/adminfooter.php");
?>