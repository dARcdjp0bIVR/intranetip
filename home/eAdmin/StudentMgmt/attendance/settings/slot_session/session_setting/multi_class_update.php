<?
// using 
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lclass = new libcardstudentattend2();

$lclass->Start_Trans();
//if($SetOtherClasses == 1)
//{
	if(($selected_type == "") || ($selected_type == 0))
	{
		$class_list = implode(",",$class);
		if(sizeof($class)>0)
		{
			for($i=0; $i<sizeof($class); $i++)
			{
				//list($class[$i]) = $class[$i];
								
				$sql = "INSERT IGNORE INTO CARD_STUDENT_CLASS_SPECIFIC_MODE (ClassID, Mode, DateInput, DateModified) VALUES ('".$lclass->Get_Safe_Sql_Query($class[$i])."', 1, 'NOW()', 'NOW()')";
				$Result['InsertClassSpecificMode'] = $lclass->db_db_query($sql);
				
				$sql = "UPDATE CARD_STUDENT_CLASS_SPECIFIC_MODE SET Mode = 1, DateModified = 'NOW()' WHERE ClassID = '".$lclass->Get_Safe_Sql_Query($class[$i])."' ";
				$Result['UpdateClassSpecificMode'] = $lclass->db_db_query($sql);
				
				$sql = "INSERT IGNORE INTO CARD_STUDENT_TIME_SESSION_REGULAR VALUES ('', '".$lclass->Get_Safe_Sql_Query($class[$i])."', 0, 0, '".$lclass->Get_Safe_Sql_Query($SessionID)."', '', '', NOW(), NOW())";
				$Result['InsertTimeSessionRegular'] = $lclass->db_db_query($sql);
				
				if ($lclass->db_affected_rows()==0)
				{
					$sql = "UPDATE CARD_STUDENT_TIME_SESSION_REGULAR SET SessionID = '".$lclass->Get_Safe_Sql_Query($SessionID)."', DateModified = NOW() WHERE ClassID = '".$lclass->Get_Safe_Sql_Query($class[$i])."' AND DayType = 0 AND DayValue = 0";
					$Result['UpdateTimeSessionRegular'] = $lclass->db_db_query($sql);
				}
			}
		}
	}

	if(($selected_type == 1)||($selected_type == 2))
	{
		$class_list = implode(",",$class);
		if(sizeof($class)>0)
		{
			for($i=0; $i<sizeof($class); $i++)
			{
				//list($class[$i]) = $class[$i];
				$sql = "INSERT IGNORE INTO CARD_STUDENT_CLASS_SPECIFIC_MODE (ClassID, Mode, DateInput, DateModified) VALUES ('".$lclass->Get_Safe_Sql_Query($class[$i])."', 1, 'NOW()', 'NOW()')";
				$Result['InsertClassSpecificMode'.$i] = $lclass->db_db_query($sql);
				$sql = "UPDATE CARD_STUDENT_CLASS_SPECIFIC_MODE SET Mode = 1, DateModified = 'NOW()' WHERE ClassID = '".$lclass->Get_Safe_Sql_Query($class[$i])."'";
				$Result['UpdateClassSpecificMode'.$i] = $lclass->db_db_query($sql);
				
				$sql = "SELECT RecordID, ClassID FROM CARD_STUDENT_TIME_SESSION_REGULAR WHERE ClassID = '".$lclass->Get_Safe_Sql_Query($class[$i])."' AND DayType = '".$lclass->Get_Safe_Sql_Query($selected_type)."' AND DayValue = '".$lclass->Get_Safe_Sql_Query($DayValue)."'";
				$temp_class = $lclass->returnArray($sql,2);
				
				$sql = "INSERT IGNORE INTO CARD_STUDENT_TIME_SESSION_REGULAR (ClassID, DayType, DayValue, SessionID, DateInput, DateModified) VALUES ('".$lclass->Get_Safe_Sql_Query($class[$i])."', '".$lclass->Get_Safe_Sql_Query($selected_type)."', '".$lclass->Get_Safe_Sql_Query($DayValue)."', '".$lclass->Get_Safe_Sql_Query($SessionID)."', 'NOW()', 'NOW()')";
				$Result['InsertTimeSessionRegular'.$i] = $lclass->db_db_query($sql);
				if(sizeof($temp_class)>0)
				{
					for($j=0; $j<sizeof($temp_class); $j++)
					{
						list($r_id, $c_id) = $temp_class[$j];
						$sql = "UPDATE CARD_STUDENT_TIME_SESSION_REGULAR SET SessionID = '".$lclass->Get_Safe_Sql_Query($SessionID)."' , DateModified = 'NOW()' WHERE RecordID = '".$lclass->Get_Safe_Sql_Query($r_id)."'";
						$Result['UpdateTimeSessionRegular'.$i.$j] = $lclass->db_db_query($sql);
					}
				}
			}
		}
	}
	if($selected_type == 3)
	{
		$class_list = implode(",",$class);
		if(sizeof($class)>0)
		{
			for($i=0; $i<sizeof($class); $i++)
			{
				//list($class[$i]) = $class[$i];
				$sql = "INSERT IGNORE INTO CARD_STUDENT_CLASS_SPECIFIC_MODE (ClassID, Mode, DateInput, DateModified) VALUES ('".$lclass->Get_Safe_Sql_Query($class[$i])."', 1, 'NOW()', 'NOW()')";
				$Result['InsertClassSpecificMode'.$i] = $lclass->db_db_query($sql);
				$sql = "UPDATE CARD_STUDENT_CLASS_SPECIFIC_MODE SET Mode = 1, DateModified = 'NOW()' WHERE ClassID = '".$lclass->Get_Safe_Sql_Query($class[$i])."'";
				$Result['UpdateClassSpecificMode'.$i] = $lclass->db_db_query($sql);
				
				$sql = "SELECT RecordID, ClassID FROM CARD_STUDENT_TIME_SESSION_DATE WHERE ClassID = '".$lclass->Get_Safe_Sql_Query($class[$i])."' AND RecordDate = '".$lclass->Get_Safe_Sql_Query($TargetDate)."'";
				$temp_class = $lclass->returnArray($sql,2);
				
				$sql = "INSERT IGNORE INTO CARD_STUDENT_TIME_SESSION_DATE (ClassID, RecordDate, SessionID, DateInput, DateModified) VALUES ('".$lclass->Get_Safe_Sql_Query($class[$i])."', '".$lclass->Get_Safe_Sql_Query($TargetDate)."', '".$lclass->Get_Safe_Sql_Query($SessionID)."', 'NOW()', 'NOW()')";
				$Result['InsertTimeSessionDate'.$i] = $lclass->db_db_query($sql);
				
				if(sizeof($temp_class)>0)
				{
					for($j=0; $j<sizeof($temp_class); $j++)
					{
						list($r_id, $c_id) = $temp_class[$j];
						$sql = "UPDATE CARD_STUDENT_TIME_SESSION_DATE SET SessionID = '".$lclass->Get_Safe_Sql_Query($SessionID)."', DateModified = 'NOW()' WHERE RecordID = '".$lclass->Get_Safe_Sql_Query($r_id)."'";
						$Result['UpdateTimeSessionDate'.$i.$j] = $lclass->db_db_query($sql);
					}
				}
			}
		}
	}
//}

if (in_array(false,$Result)) {
	$Msg = $Lang['StudentAttendance']['SettingApplyFail'];
	$lclass->RollBack_Trans();
}
else {
	$Msg = $Lang['StudentAttendance']['SettingApplySuccess'];
	$lclass->Commit_Trans();
}

Header ("Location: index.php?Msg=".urlencode($Msg));
intranet_closedb();
?>