<?
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lcardstudentattend2 = new libcardstudentattend2();
$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageSystemSetting_TimeSessionSettings";

$linterface = new interface_html();

$lclass = new libclass();

$sql = "SELECT 
			SessionID, SessionName, RecordType, RecordStatus
		FROM
			CARD_STUDENT_TIME_SESSION
		ORDER BY
			SessionID";

$temp = $lclass->returnArray($sql,4);

$table_content = "";

if(sizeof($temp)>0)
{
	for($i=0; $i<sizeof($temp); $i++)
	{
		list ($s_id, $s_name, $record_type, $record_status) = $temp[$i];
		$css =$i%2==0?"tablerow1 tabletext":"tablerow2 tabletext";
		
		$table_content .= "<tr class=\"$css\"><td><a class=\"tablelink\" href=\"edit.php?s_id=$s_id\">$s_name</a></td>";
		
		if(($record_status == "")||($record_status == 0))
		{
			$table_content .= "<td align=\"center\"><input type=\"checkbox\" name=\"record_status[]\" value=\"$s_id\"></td></tr>";
		}
		if ($record_status == 1)
		{
			$table_content .= "<td align=\"center\"><input type=\"checkbox\" name=\"record_status[]\" value=\"$s_id\" checked></td></tr>";
		}

	}
}
if(sizeof($temp)==0)
{
	$table_content .= "<tr><td colspan=\"3\" align=\"center\" class=\"tabletext\">$i_no_record_exists_msg</td></tr>";
}

$toolbar = $linterface->GET_LNK_NEW("javascript:checkNew('new.php')","","","","",0);

$table_tool .= "<td nowrap=\"nowrap\">
					<a href=\"javascript:submitForm()\" class=\"tabletool\">
						<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_update.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">
						$button_update
					</a>
				</td>";


$TAGS_OBJ[] = array($i_StudentAttendance_Menu_Slot_Session_Setting, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/settings/slot_session/session_setting/", 1);
$TAGS_OBJ[] = array($i_StudentAttendance_Menu_Slot_Session_School, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/settings/slot_session/school/", 0);
$TAGS_OBJ[] = array($i_StudentAttendance_Menu_Slot_Session_Class, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/settings/slot_session/class/", 0);
$TAGS_OBJ[] = array($i_StudentAttendance_Menu_Slot_Session_Group, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/settings/slot_session/group/", 0);
		
$MODULE_OBJ = $lcardstudentattend2->GET_MODULE_OBJ_ARR();
$Msg = urldecode($Msg);
$linterface->LAYOUT_START($Msg);

if ($msg == 2) $SysMsg = $linterface->GET_SYS_MSG("update");
?>
<script language="JavaScript">
function setChecked(val, obj, element_name){
    len=obj.elements.length;
    var i=0;
    var temp;
    
    if (val == 1)
    {
        for( i=0 ; i<len ; i++) 
        {
            if (obj.elements[i].name==element_name)
            {
            	obj.elements[i].checked=val;
        	}
        }
    }
    else
    {
        for( i=0 ; i<len ; i++) 
        {
            if (obj.elements[i].name==element_name)
            {
            	obj.elements[i].checked=val;
        	}
        }
    }
}

function submitForm()
{
	document.form1.submit();
}
</script>
<br />
<form name="form1" action="update.php" method="post">
<table width="95%" border="0" cellpadding="0" cellspacing="0" align="center">
	<tr>
		<td align="left"><?=$toolbar?></td>
		<td class="tabletext" align="right"><?=$SysMsg?></td>
	</tr>
</table>
<table width="95%" border="0" cellpadding="0" cellspacing="0" align="center">
	<tr class="table-action-bar">
		<td align="right">
			<table border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="21"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_01.gif" width="21" height="23"></td>
					<td background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_02.gif">
						<table border="0" cellspacing="0" cellpadding="2">
							<tr>
								<?=$table_tool?>
							</tr>
						</table>
					</td>
					<td width="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_03.gif" width="6" height="23"></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr class="tabletop">
		<td class="tabletoplink"><?=$i_StudentAttendance_Menu_Slot_Session_Name?></td>
		<td class="tabletoplink" align="center">
			<?=$i_general_active?><input type="checkbox" name="active" onClick="(this.checked)?setChecked(1,this.form,'record_status[]'):setChecked(0,this.form,'record_status[]')">
		</td>
	</tr>
	<?=$table_content?>
</table>
</form>
<br />
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>