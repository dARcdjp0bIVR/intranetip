<?
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lclass = new libcardstudentattend2();

$cnt = 0;

$Result = array();
$lclass->Start_Trans();
for($i=0; $i<$no_of_element; $i++)
{
	if((${"non_school_day_$i"}==on)||(${"non_school_day_$i"}==ON)||(${"non_school_day_$i"}==1))
	{
		$temp = 1;
		${"am_time_$i"} = "";
		${"lunch_time_$i"} = "";
		${"pm_time_$i"} = "";
		${"leave_time_$i"} = "";
	}
	else
	{
		$temp = 0;
		${"am_time_$i"} = ${"am_hours_$i"}.":".${"am_minutes_$i"};
		${"lunch_time_$i"} = ${"lunch_hours_$i"}.":".${"lunch_minutes_$i"};
		${"pm_time_$i"} = ${"pm_hours_$i"}.":".${"pm_minutes_$i"};
		${"leave_time_$i"} = ${"leave_hours_$i"}.":".${"leave_minutes_$i"};
	}
	
	$sql = "INSERT INTO CARD_STUDENT_TIME_SESSION VALUES ('', '".${"s_name_$i"}."', '".${"am_time_$i"}."', '".${"lunch_time_$i"}."', '".${"pm_time_$i"}."', '".${"leave_time_$i"}."', '$temp', '', 1, NOW(), NOW())";
	//echo $sql;
	
	$Result[$i] = $lclass->db_db_query($sql);
	
	if($Result[$i])
	{
		$cnt++;
	}
}

$sql = "SELECT RecordID, SessionID FROM CARD_STUDENT_TIME_SESSION_REGULAR WHERE ClassID = 0 AND DayType = 0 AND DayValue = 0";
$temp = $lclass->returnArray($sql,2);
if(sizeof($temp)==0)
{
	$sql = "SELECT SessionID FROM CARD_STUDENT_TIME_SESSION WHERE NonSchoolDay = 0 AND RecordStatus = 1 ORDER BY SessionID LIMIT 0,1";
	$default = $lclass->returnVector($sql);
	if(sizeof($default)>0)
		list ($default_session) = $default[0];
	$sql = "INSERT INTO CARD_STUDENT_TIME_SESSION_REGULAR VALUES ('', 0, 0, 0, '$default_session', '', '', NOW(), NOW())";
	$Result['InsertRegularTimeSession'] = $lclass->db_db_query($sql);
}

if (in_array(false,$Result)) {
	$Msg = $Lang['StudentAttendance']['SettingApplyFail'];
	$lclass->RollBack_Trans();
}
else {
	$Msg = $Lang['StudentAttendance']['SettingApplySuccess'];
	$lclass->Commit_Trans();
}

header ("Location: index.php?Msg=".urlencode($Msg));
intranet_closedb();
?>