<?
// editing by 
############################################## Change Log ##############################################
# 2010-06-11 by Carlos : Add checking on session time(prev session time must smaller than 
#                        next session time) and time session mode(display depending on attendance mode).
########################################### End of Change Log ##########################################
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();
$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageSystemSetting_TimeSessionSettings";

$linterface = new interface_html();

$lc->retrieveSettings();
$hasAM = ($lc->attendance_mode != 1);
$hasLunch = ($lc->attendance_mode != 0 && $lc->attendance_mode != 1);
$hasPM = ($lc->attendance_mode != 0);
$attendance_mode = $lc->attendance_mode;

$no_of_fields = 3;
if($hasAM) $no_of_fields++;
if($hasLunch) $no_of_fields++;
if($hasPM) $no_of_fields++;
$hours = array();
$minutes = array();

for($i=0; $i<24; $i++)
{
	if($i<10)
		$i='0'.$i;
	array_push($hours, $i);
}

for($j=0; $j<60; $j++)
{
	if($j<10)
		$j='0'.$j;
	array_push($minutes, $j);
}

$TAGS_OBJ[] = array($i_StudentAttendance_Menu_Slot_Session_Setting, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/settings/slot_session/session_setting/", 0);
$TAGS_OBJ[] = array($i_StudentAttendance_Menu_Slot_Session_School, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/settings/slot_session/school/", 1);
$TAGS_OBJ[] = array($i_StudentAttendance_Menu_Slot_Session_Class, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/settings/slot_session/class/", 0);
$TAGS_OBJ[] = array($i_StudentAttendance_Menu_Slot_Session_Group, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/settings/slot_session/group/", 0);
		
$MODULE_OBJ = $lc->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

$PAGE_NAVIGATION[] = array($button_new);
?>

<script language="JavaScript">
var no_of_time_session = <? echo $no_of_fields==""?6:$no_of_fields;?>;
var no_of_element = 1;

function add_field(){
	var table = document.getElementById("time_session");
	var row = table.insertRow(no_of_time_session);
	if (document.getElementById)
	{
		row = table.insertRow(no_of_time_session);
		
		var newCell1  = row.insertCell(0);
		var newCell2  = row.insertCell(1);
		
		var newText  = document.createTextNode("\u00a0");
		
		newCell1.appendChild(newText);
		newCell2.appendChild(newText);
		
		no_of_time_session++;
	}
	if (document.getElementById)
	{
		row = table.insertRow(no_of_time_session);
		
		newCell1  = row.insertCell(0);
		newCell1.vAlign = "top";
		newCell1.className = "formfieldtitle tabletext";
		
		newCell2  = row.insertCell(1);
		newCell2.className = "tabletext";
		newCell2.width = "70%";
		
		newCell1.innerHTML = '<?=$i_StudentAttendance_Menu_Slot_Session_Name?>';
		newCell2.innerHTML = '<input class="textboxnum" type="text" name="s_name_'+no_of_element+'">';
		
		no_of_time_session++;
	}
	if (document.getElementById)
	{
		var row = table.insertRow(no_of_time_session);
		
		newCell1  = row.insertCell(0);
		newCell1.vAlign = "top";
		newCell1.className = "formfieldtitle tabletext";
		
		newCell2  = row.insertCell(1);
		newCell2.className = "tabletext";
		newCell2.width = "70%";

		newText1 = '<?=$i_StudentAttendance_Menu_Time_Session_NonSchoolDay?>';
		newText2 = '<input type="checkbox" onclick="checkDisable('+no_of_element+')" name="non_school_day_'+no_of_element+'" id="non_school_day_'+no_of_element+'" onClick="this.checked? this.value=1 : this.value=0;">';
		
		newCell1.innerHTML = newText1;
		newCell2.innerHTML = newText2;
		
		no_of_time_session++;
	}
<?if($hasAM){?>
	if (document.getElementById)
	{
		row = table.insertRow(no_of_time_session);
		
		newCell1  = row.insertCell(0);
		newCell1.vAlign = "top";
		newCell1.className = "formfieldtitle tabletext";
		
		newCell2  = row.insertCell(1);
		newCell2.className = "tabletext";
		newCell2.width = "70%";
		
		newText1 = '<?=$i_StudentAttendance_Menu_Time_Session_MorningTime?>';
		
		newText2 = '<select name="am_hours_'+no_of_element+'" id="am_hours_'+no_of_element+'">';
		for(i=0;i<24;i++)
		{
			if(i<10)
				i='0'+i;
			newText2 += '<option value='+i+'>'+i+'</option>';
		}
		newText2 += '</select>';
		newText2 += ':';
		newText2 += '<select name="am_minutes_'+no_of_element+'" id="am_minutes_'+no_of_element+'">';
		for(j=0;j<60;j++)
		{
			if(j<10)
				j='0'+j;
			newText2 += '<option value='+j+'>'+j+'</option>';
		}
		newText2 += '</select>';
		
		newCell1.innerHTML = newText1;
		newCell2.innerHTML = newText2;
		
		no_of_time_session++;
	}
<?}?>
<?if($hasLunch){?>
	if (document.getElementById)
	{
		var row = table.insertRow(no_of_time_session);
		
		newCell1  = row.insertCell(0);
		newCell1.vAlign = "top";
		newCell1.className = "formfieldtitle tabletext";
		
		newCell2  = row.insertCell(1);
		newCell2.className = "tabletext";
		newCell2.width = "70%";
		
		newText1 = '<?=$i_StudentAttendance_Menu_Time_Session_LunchStartTime?>';
		newText2 = '<select name="lunch_hours_'+no_of_element+'" id="lunch_hours_'+no_of_element+'">';
		for(i=0;i<24;i++)
		{
			if(i<10)
				i='0'+i;
			newText2 += '<option value='+i+'>'+i+'</option>';
		}
		newText2 += '</select>';
		newText2 += ':';
		newText2 += '<select name="lunch_minutes_'+no_of_element+'" id="lunch_minutes_'+no_of_element+'">';
		for(j=0;j<60;j++)
		{
			if(j<10)
				j='0'+j;
			newText2 += '<option value='+j+'>'+j+'</option>';
		}
		newText2 += '</select>';
		
		newCell1.innerHTML = newText1;
		newCell2.innerHTML = newText2;
		
		no_of_time_session++;
	}
<?}?>
<?if($hasPM){?>
	if (document.getElementById)
	{
		var row = table.insertRow(no_of_time_session);
		
		newCell1  = row.insertCell(0);
		newCell1.vAlign = "top";
		newCell1.className = "formfieldtitle tabletext";
		
		newCell2  = row.insertCell(1);
		newCell2.className = "tabletext";
		newCell2.width = "70%";
		
		newText1 = '<?=$i_StudentAttendance_Menu_Time_Session_LunchEndTime?>';
		newText2 = '<select name="pm_hours_'+no_of_element+'" id="pm_hours_'+no_of_element+'">';
		for(i=0;i<24;i++)
		{
			if(i<10)
				i='0'+i;
			newText2 += '<option value='+i+'>'+i+'</option>';
		}
		newText2 += '</select>';
		newText2 += ':';
		newText2 += '<select name="pm_minutes_'+no_of_element+'" id="pm_minutes_'+no_of_element+'">';
		for(j=0;j<60;j++)
		{
			if(j<10)
				j='0'+j;
			newText2 += '<option value='+j+'>'+j+'</option>';
		}
		newText2 += '</select>';
		
		newCell1.innerHTML = newText1;
		newCell2.innerHTML = newText2;
		
		no_of_time_session++;
	}
<?}?>
	if (document.getElementById)
	{
		var row = table.insertRow(no_of_time_session);
		
		newCell1  = row.insertCell(0);
		newCell1.vAlign = "top";
		newCell1.className = "formfieldtitle tabletext";
		
		newCell2  = row.insertCell(1);
		newCell2.className = "tabletext";
		newCell2.width = "70%";
		
		newText1 = '<?=$i_StudentAttendance_Menu_Time_Session_LeaveSchoolTime?>';
		newText2 = '<select name="leave_hours_'+no_of_element+'" id="leave_hours_'+no_of_element+'">';
		for(i=0;i<24;i++)
		{
			if(i<10)
				i='0'+i;
			newText2 += '<option value='+i+'>'+i+'</OPTION>';
		}
		newText2 += '</select>';
		newText2 += ':';
		newText2 += '<select name="leave_minutes_'+no_of_element+'" id="leave_minutes_'+no_of_element+'">';
		for(j=0;j<60;j++)
		{
			if(j<10)
				j='0'+j;
			newText2 += '<option value='+j+'>'+j+'</option>';
		}
		newText2 += '</select>';
		
		newCell1.innerHTML = newText1;
		newCell2.innerHTML = newText2;
		
		no_of_time_session++;
	}
	
	no_of_element++;
	document.form1.no_of_element.value = no_of_element;
}

function checkForm()
{
	var cnt = 0;
	var checkTimeFormat = 0;
		
	for(i=0; i<no_of_element; i++)
	{
		if(eval("document.form1.s_name_"+i+".value") == "")
		{
			alert ("<?=$i_StudentAttendance_TimeSessionSettings_SessionName_Warning?>");
			return false;
		}else if(!check_time_session(i))
		{
<?if($attendance_mode==0){?>
			alert("<?=$i_StudentAttendance_Input_Correct_Time?>\n<?=$i_StudentAttendance_Warn_AM_Must_Smaller_Than_SchoolEnd?>");
<?}else if($attendance_mode==1){?>
			alert("<?=$i_StudentAttendance_Input_Correct_Time?>\n<?=$i_StudentAttendance_Warn_PMStart_Must_Smaller_Than_SchoolEnd?>");
<?}else{?>
			alert("<?=$i_StudentAttendance_Input_Correct_Time?>\n<?=$i_StudentAttendance_Warn_AM_Must_Smaller_Than_LunchStart?>\n<?=$i_StudentAttendance_Warn_LunchStart_Must_Smaller_Than_PMStart?>\n<?=$i_StudentAttendance_Warn_PMStart_Must_Smaller_Than_SchoolEnd?>");
<?}?>
			return false;
		}
		else
		{
			cnt++;
		}
	}
	if((cnt==no_of_element))
	{
		return true;
	}
}

function checkDisable(eid)
{
	var non_school_day = 'document.getElementById("non_school_day_'+eid+'")';
<?if($hasAM){?>	
	var am_hours = 'document.getElementById("am_hours_'+eid+'")';
	var am_minutes = 'document.getElementById("am_minutes_'+eid+'")';
<?}?>
<?if($hasLunch){?>
	var lunch_hours = 'document.getElementById("lunch_hours_'+eid+'")';
	var lunch_minutes = 'document.getElementById("lunch_minutes_'+eid+'")';
<?}?>
<?if($hasPM){?>
	var pm_hours = 'document.getElementById("pm_hours_'+eid+'")';
	var pm_minutes = 'document.getElementById("pm_minutes_'+eid+'")';
<?}?>
	var leave_hours = 'document.getElementById("leave_hours_'+eid+'")';
	var leave_minutes = 'document.getElementById("leave_minutes_'+eid+'")';
	
	if(eval(non_school_day+'.checked'))
	{
<?if($hasAM){?>
		eval(am_hours+'.disabled = true');
		eval(am_minutes+'.disabled = true');
<?}?>
<?if($hasLunch){?>
		eval(lunch_hours+'.disabled = true');
		eval(lunch_minutes+'.disabled = true');
<?}?>
<?if($hasPM){?>
		eval(pm_hours+'.disabled = true');
		eval(pm_minutes+'.disabled = true');
<?}?>
		eval(leave_hours+'.disabled = true');
		eval(leave_minutes+'.disabled = true');
	}
	else
	{
<?if($hasAM){?>
		eval(am_hours+'.disabled = false');
		eval(am_minutes+'.disabled = false');
<?}?>
<?if($hasLunch){?>
		eval(lunch_hours+'.disabled = false');
		eval(lunch_minutes+'.disabled = false');
<?}?>
<?if($hasPM){?>
		eval(pm_hours+'.disabled = false');
		eval(pm_minutes+'.disabled = false');
<?}?>
		eval(leave_hours+'.disabled = false');
		eval(leave_minutes+'.disabled = false');
	}
}

function check_time_session(element_no)
{
	if($('input#non_school_day_'+element_no).attr('checked')) return true;
<?if($hasAM){?>
	$am_time = $('select#am_hours_'+element_no).val() + ':' + $('select#am_minutes_'+element_no).val();
<?}?>
<?if($hasLunch){?>
	$lunch_time = $('select#lunch_hours_'+element_no).val() + ':' + $('select#lunch_minutes_'+element_no).val();
<?}?>
<?if($hasPM){?>
	$pm_time = $('select#pm_hours_'+element_no).val() + ':' + $('select#pm_minutes_'+element_no).val();
<?}?>
	$leave_time = $('select#leave_hours_'+element_no).val() + ':' + $('select#leave_minutes_'+element_no).val();
	
<?if($attendance_mode==0){?>
	if($am_time<$leave_time)
<?}else if($attendance_mode==1){?>
	if($pm_time<$leave_time)
<?}else{?>
	if($am_time<$lunch_time && $lunch_time<$pm_time && $pm_time<$leave_time)
<?}?>
		return true;
	else
		return false;
}
</script>
<br />
<form name="form1" action="new_update.php" method="post" onSubmit="return checkForm()">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	</tr>
	<tr>
    	<td>
    		<table id="time_session" width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
			    <tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?= $i_StudentAttendance_Menu_Slot_Session_Name ?>
					</td>
					<td class="tabletext" width="70%">
						<input class="textboxnum" type="text" name="s_name_0">
					</td>
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?= $i_StudentAttendance_Menu_Time_Session_NonSchoolDay ?>
					</td>
					<td class="tabletext" width="70%">
						<input type="checkbox" name="non_school_day_0" id="non_school_day_0" onClick="checkDisable(0); this.checked? this.value=1 : this.value=0;" >
					</td>
				</tr>
				<?
				if($hasAM) {
				?>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?= $i_StudentAttendance_Menu_Time_Session_MorningTime ?>
					</td>
					<td class="tabletext" width="70%">
						<?=getSelectByValue($hours,"name=\"am_hours_0\" id=\"am_hours_0\" ",0,0,1)?>:<?=getSelectByValue($minutes,"name=\"am_minutes_0\" id=\"am_minutes_0\" ",0,0,1)?>
					</td>
				</tr>
				<?
				}
				if($hasLunch) {
				?>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?= $i_StudentAttendance_Menu_Time_Session_LunchStartTime ?>
					</td>
					<td class="tabletext" width="70%">
						<?=getSelectByValue($hours,"name=\"lunch_hours_0\" id=\"lunch_hours_0\" ",0,0,1)?>:<?=getSelectByValue($minutes,"name=\"lunch_minutes_0\" id=\"lunch_minutes_0\" ",0,0,1)?>
					</td>
				</tr>
				<?
				}
				if($hasPM) {
				?>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?= $i_StudentAttendance_Menu_Time_Session_LunchEndTime ?>
					</td>
					<td class="tabletext" width="70%">
						<?=getSelectByValue($hours,"name=\"pm_hours_0\" id=\"pm_hours_0\" ",0,0,1)?>:<?=getSelectByValue($minutes,"name=\"pm_minutes_0\" id=\"pm_minutes_0\" ",0,0,1)?>
					</td>
				</tr>
				<?
				}
				?>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?= $i_StudentAttendance_Menu_Time_Session_LeaveSchoolTime ?>
					</td>
					<td class="tabletext" width="70%">
						<?=getSelectByValue($hours,"name=\"leave_hours_0\" id=\"leave_hours_0\" ",0,0,1)?>:<?=getSelectByValue($minutes,"name=\"leave_minutes_0\" id=\"leave_minutes_0\" ",0,0,1)?>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table width="95%" border="0" cellpadding="5" cellspacing="0">
				<tr>
					<td align="right">
						<?= $linterface->GET_ACTION_BTN(" + ", "button", "add_field()") ?>
					</td>
				</tr>
				<tr>
					<td class="tabletextremark"><?=$i_StudentAttendance_Slot_SettingsDescription?></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
    	<td>
		    <table width="96%" border="0" cellpadding="0" cellspacing="0" align="center">
		    	<tr>
                	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
		    </table>
	    </td>
	</tr>
    <tr>
	    <td align="center" colspan="2">
			<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "") ?>
			<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
			<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:window.location = 'index.php';","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
		</td>
	</tr>
</table>
<input type="hidden" name="no_of_element" value="1">
</form>

<?
$linterface->LAYOUT_STOP();
print $linterface->FOCUS_ON_LOAD("form1.s_name_0");
?>