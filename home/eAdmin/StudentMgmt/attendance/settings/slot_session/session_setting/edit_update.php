<?
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");

intranet_opendb();
$lclass = new libcardstudentattend2();

$lclass->Start_Trans();
if($s_id!="")
{
	if(($non_school_day==1)||($non_school_day==on)||($non_school_day==ON))
	{
		$temp = 1;
		$am_time = "";
		$lunch_time = "";
		$pm_time = "";
		$leave_time = "";
	}
	else
	{
		$temp = 0;
		if(($am_hours == "") && ($am_minutes == ""))
		{
			$am_time = "";
		}
		else
		{
			$am_time = $am_hours.":".$am_minutes;
		}
		if(($lunch_hours == "") && ($lunch_minutes == ""))
		{
			$lunch_time = "";
		}
		else
		{
			$lunch_time = $lunch_hours.":".$lunch_minutes;
		}
		if(($pm_hours == "") && ($pm_minutes == ""))
		{
			$pm_time = "";;
		}
		else
		{
			$pm_time = $pm_hours.":".$pm_minutes;
		}
		if(($leave_hours == "") && ($leave_minutes == ""))
		{
			$leave_time = "";
		}
		else
		{
			$leave_time = $leave_hours.":".$leave_minutes;
		}
	}
	
	//echo $s_id;
	$sql = "UPDATE 
					CARD_STUDENT_TIME_SESSION 
			SET
					SessionName = '".$lclass->Get_Safe_Sql_Query($s_name)."',
					MorningTime = '".$lclass->Get_Safe_Sql_Query($am_time)."',
					LunchStart = '".$lclass->Get_Safe_Sql_Query($lunch_time)."',
					LunchEnd = '".$lclass->Get_Safe_Sql_Query($pm_time)."',
					LeaveSchoolTime = '".$lclass->Get_Safe_Sql_Query($leave_time)."',
					NonSchoolDay = '".$lclass->Get_Safe_Sql_Query($temp)."'
			WHERE
					SessionID = '".$lclass->Get_Safe_Sql_Query($s_id)."'
			";
	//echo $sql;
	$Result['UpdateTimeSession'] = $lclass->db_db_query($sql);

	if(($non_school_day==1)||($non_school_day==on)||($non_school_day==ON))
	{
		$sql = "SELECT SessionID FROM CARD_STUDENT_TIME_SESSION_REGULAR WHERE ClassID = 0 AND DayType = 0 AND DayValue = 0";
		$result = $lclass->returnVector($sql);
		if(sizeof($result)>0)
		{
			list ($curr_session) = $result[0];
			if($curr_session == $s_id)
			{
				$sql = "SELECT SessionID FROM CARD_STUDENT_TIME_SESSION WHERE NonSchoolDay = 0 AND RecordStatus = 1 ORDER BY SessionID LIMIT 0,1";
				$temp= $lclass->returnVector($sql);
				if(sizeof($temp)>0)
					list($new_session) = $temp[0];
				$sql = "UPDATE CARD_STUDENT_TIME_SESSION_REGULAR SET SessionID = '$new_session' WHERE ClassID = 0 AND DayType = 0 AND DayValue = 0";
				$Result['UpdateTimeSessionRegular'] = $lclass->db_db_query($sql); 
			}
		}
	}
}

if (in_array(false,$Result)) {
	$Msg = $Lang['StudentAttendance']['SettingApplyFail'];
	$lclass->RollBack_Trans();
}
else {
	$Msg = $Lang['StudentAttendance']['SettingApplySuccess'];
	$lclass->Commit_Trans();
}

Header ("Location: index.php?Msg=".urlencode($Msg));
intranet_closedb();
?>