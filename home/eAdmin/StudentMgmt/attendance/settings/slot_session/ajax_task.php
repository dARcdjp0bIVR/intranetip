<?php
// Editing by 
/*
 * 2017-09-06 (Carlos): Created for loading special day settings table.
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();

switch($task)
{
	case "get_special_day_table":
		$hasAM = ($lc->attendance_mode != 1);
		$hasLunch = ($lc->attendance_mode != 0 && $lc->attendance_mode != 1);
		$hasPM = ($lc->attendance_mode != 0);
		
		$today = date("Y-m-d");
		$cond = $viewPast=='1'? "" : " AND a.RecordDate>='$today' ";
		if($type == 'school')
		{
			$InactiveSession = array();
			$sql = "SELECT a.SessionID 
						FROM CARD_STUDENT_TIME_SESSION as a 
						INNER JOIN CARD_STUDENT_TIME_SESSION_DATE as b ON a.SessionID=b.SessionID 
						WHERE b.ClassID=0 AND a.RecordStatus=0";
			$InactiveSession = $lc->returnVector($sql);
			
			$sql = "SELECT 
							a.RecordID, b.SessionID, b.SessionName, a.RecordDate ,IF(NonSchoolDay=1,'-',TIME_FORMAT(b.MorningTime,'%H:%i')), IF(NonSchoolDay=1,'-',TIME_FORMAT(b.LunchStart,'%H:%i')), IF(NonSchoolDay=1,'-',TIME_FORMAT(b.LunchEnd,'%H:%i')), IF(NonSchoolDay=1,'-',TIME_FORMAT(b.LeaveSchoolTime,'%H:%i')), b.NonSchoolDay
					FROM 
							CARD_STUDENT_TIME_SESSION_DATE AS a LEFT OUTER JOIN CARD_STUDENT_TIME_SESSION AS b ON (a.SessionID = b.SessionID)
					WHERE 
							a.ClassID = 0 $cond
					";
			$temp = $lc->returnArray($sql);
			
			$special_day_table = "<table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">";
			$special_day_table.= "<tr class=\"tabletop\"><td class=\"tabletoplink\">$i_StudentAttendance_Slot_Special</td>";
			if($hasAM)
				$special_day_table .= "<td class=\"tabletoplink\">$i_StudentAttendance_Menu_Time_Session_MorningTime</td>";
			if($hasLunch)
				$special_day_table .= "<td class=\"tabletoplink\">$i_StudentAttendance_Menu_Time_Session_LunchStartTime</td>";
			if($hasPM)
				$special_day_table .= "<td class=\"tabletoplink\">$i_StudentAttendance_Menu_Time_Session_LunchEndTime</td>";
			$special_day_table .= "<td class=\"tabletoplink\">$i_StudentAttendance_Menu_Time_Session_LeaveSchoolTime</td></tr>";
			
			if(sizeof($temp)>0)
			{	
				for($i=0; $i<sizeof($temp); $i++)
				{
					list($record_id, $s_id, $s_name, $record_date, $am_time, $lunch_time, $pm_time, $leave_school_time, $non_school_day) = $temp[$i];
					$special_date = $record_date;
					$editlink = "<a href=\"edit.php?type=3&value=$special_date\"><img src=\"$image_path/$LAYOUT_SKIN/icon_edit.gif\" alt=\"$button_edit\" border=\"0\"></a>";
					$editlink .= "<a href=\"javascript:removeSpecialDaySetting('$special_date')\"><img src=\"$image_path/$LAYOUT_SKIN/icon_delete.gif\" alt=\"$button_remove\" border=\"0\"></a>";
					if(in_array($s_id,$InactiveSession))
					{
						$special_day_table .= "<tr bgcolor=\"red\"><td class=\"tabletext\">$record_date $editlink</td>";
						$special_warning = 1;
					}
					else
					{
						$special_day_table .= "<tr><td class=\"tabletext\">$record_date $editlink</td>";
					}
					if($non_school_day == 1)
					{
						
						$special_day_table .= "<td class=\"tabletext\" colspan=\"4\">$i_StudentAttendance_NonSchoolDay</td></tr>";
					}
					else
					{
						if($hasAM)
							$special_day_table .= "<td class=\"tabletext\">$am_time</td>";
						if($hasLunch)
							$special_day_table .= "<td class=\"tabletext\">$lunch_time</td>";
						if($hasPM)
							$special_day_table .= "<td class=\"tabletext\">$pm_time</td>";
						$special_day_table .= "<td class=\"tabletext\">$leave_school_time</td>";
					}
				}
			}
			if(sizeof($temp)==0)
			{
				$special_day_table .= "<tr><td class=\"tabletext\" colspan=\"7\" align=\"center\">$i_no_record_exists_msg</td></tr>";
			}
			$special_day_table .= "</table>";
			
		}else if($type == 'class'){
			
			$InactiveSession = array();
			$sql = "SELECT a.SessionID 
					FROM CARD_STUDENT_TIME_SESSION as a 
					INNER JOIN CARD_STUDENT_TIME_SESSION_DATE as b ON a.SessionID=b.SessionID 
					WHERE b.ClassID='$ClassID' AND a.RecordStatus=0 ";
			$InactiveSession = $lc->returnVector($sql);
			
			$sql = "SELECT 
							a.RecordID, b.SessionID, b.SessionName, a.RecordDate ,TIME_FORMAT(b.MorningTime,'%H:%i'), TIME_FORMAT(b.LunchStart,'%H:%i'), TIME_FORMAT(b.LunchEnd,'%H:%i'), TIME_FORMAT(b.LeaveSchoolTime,'%H:%i'), b.NonSchoolDay
					FROM 
							CARD_STUDENT_TIME_SESSION_DATE AS a LEFT OUTER JOIN CARD_STUDENT_TIME_SESSION AS b ON (a.SessionID = b.SessionID)
					WHERE
							ClassID = '$ClassID' $cond
					";
			
			$temp = $lc->returnArray($sql);
			
			$special_day_table = "<table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">";
			$special_day_table.= "<tr class=\"tabletop\"><td class=\"tabletoplink\">$i_StudentAttendance_Slot_Special</td>";
			if($hasAM)
				$special_day_table .= "<td class=\"tabletoplink\">$i_StudentAttendance_Menu_Time_Session_MorningTime</td>";
			if($hasLunch)
				$special_day_table .= "<td class=\"tabletoplink\">$i_StudentAttendance_Menu_Time_Session_LunchStartTime</td>";
			if($hasPM)
				$special_day_table .= "<td class=\"tabletoplink\">$i_StudentAttendance_Menu_Time_Session_LunchEndTime</td>";
			$special_day_table .= "<td class=\"tabletoplink\">$i_StudentAttendance_Menu_Time_Session_LeaveSchoolTime</td></tr>";
			
			if(sizeof($temp)>0)
			{	
				for($i=0; $i<sizeof($temp); $i++)
				{
					list($record_id, $s_id, $s_name, $record_date, $am_time, $lunch_time, $pm_time, $leave_school_time, $non_school_day) = $temp[$i];
					$css = ($i%2==0)?"tablerow1":"tablerow2";
					$special_date = $record_date;
					$editlink = "<a class=\"tablelink\" href=\"class_session_edit.php?type=3&ClassID=$ClassID&value=$special_date\"><img src=\"$image_path/$LAYOUT_SKIN/icon_edit.gif\" alt=\"$button_edit\" border=\"0\"></a>";
					$editlink .= "<a class=\"tablelink\" href=\"javascript:removeSpecialDaySetting($ClassID,'$special_date')\"><img src=\"$image_path/$LAYOUT_SKIN/icon_delete.gif\" alt=\"$button_remove\" border=\"0\"></a>";
					if(in_array($s_id,$InactiveSession))
					{
						$special_day_table .= "<tr bgcolor=\"red\"><td class=\"tabletext\">$record_date $editlink</td>";
						$special_warning = 1;
					}
					else
					{
						$special_day_table .= "<tr class=\"$css\"><td class=\"tabletext\">$record_date $editlink</td>";
					}
					if($non_school_day == 1)
					{
						$special_day_table .= "<td class=\"tabletext\" colspan=\"4\">$i_StudentAttendance_NonSchoolDay</td></tr>";
					}
					else{
						if($hasAM)
							$special_day_table .= "<td class=\"tabletext\">$am_time</td>";
						if($hasLunch)
							$special_day_table .= "<td class=\"tabletext\">$lunch_time</td>";
						if($hasPM)
							$special_day_table .= "<td class=\"tabletext\">$pm_time</td>";
						$special_day_table .= "<td class=\"tabletext\">$leave_school_time</td>";
					}
				}
			}
			if(sizeof($temp)==0)
			{
				$special_day_table .= "<tr class=\"tablerow2\"><td class=\"tabletext\" colspan=\"7\" align=\"center\">$i_no_record_exists_msg</td></tr>";
			}
			$special_day_table .= "</table>";
			
		}else if($type == 'group'){
			$InactiveSession = array();
			$sql = "SELECT a.SessionID 
					FROM CARD_STUDENT_TIME_SESSION as a 
					INNER JOIN CARD_STUDENT_TIME_SESSION_DATE_GROUP as b ON a.SessionID=b.SessionID 
					WHERE b.GroupID='$ClassID' AND a.RecordStatus=0";
			$InactiveSession = $lc->returnVector($sql);
			
			$sql = "SELECT 
							a.RecordID, b.SessionID, b.SessionName, a.RecordDate ,TIME_FORMAT(b.MorningTime,'%H:%i'), TIME_FORMAT(b.LunchStart,'%H:%i'), TIME_FORMAT(b.LunchEnd,'%H:%i'), TIME_FORMAT(b.LeaveSchoolTime,'%H:%i'), b.NonSchoolDay
					FROM 
							CARD_STUDENT_TIME_SESSION_DATE_GROUP AS a LEFT OUTER JOIN CARD_STUDENT_TIME_SESSION AS b ON (a.SessionID = b.SessionID)
					WHERE
							GROUPID = '$ClassID' $cond 
					ORDER BY	
							a.RecordDate 
					";
			
			$temp = $lc->returnArray($sql);
			
			$special_day_table = "<table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">";
			$special_day_table.= "<tr class=\"tabletop\"><td class=\"tabletoplink\">$i_StudentAttendance_Slot_Special</td>";
			if($hasAM)
				$special_day_table .= "<td class=\"tabletoplink\">$i_StudentAttendance_Menu_Time_Session_MorningTime</td>";
			if($hasLunch)
				$special_day_table .= "<td class=\"tabletoplink\">$i_StudentAttendance_Menu_Time_Session_LunchStartTime</td>";
			if($hasPM)
				$special_day_table .= "<td class=\"tabletoplink\">$i_StudentAttendance_Menu_Time_Session_LunchEndTime</td>";
			$special_day_table .= "<td class=\"tabletoplink\">$i_StudentAttendance_Menu_Time_Session_LeaveSchoolTime</td></tr>";
			
			if(sizeof($temp)>0)
			{	
				for($i=0; $i<sizeof($temp); $i++)
				{
					list($record_id, $s_id, $s_name, $record_date, $am_time, $lunch_time, $pm_time, $leave_school_time, $non_school_day) = $temp[$i];
					$css = ($i%2==0)?"tablerow1":"tablerow2";
					$special_date = $record_date;
					$editlink = "<a class=functionlink href=\"group_session_edit.php?type=3&ClassID=$ClassID&value=$special_date\"><img src=\"$image_path/icon_edit.gif\" border=0></a>";
					$editlink .= "<a class=functionlink href=\"javascript:removeSpecialDaySetting($ClassID,'$special_date')\"><img src=\"$image_path/icon_erase.gif\" border=0></a>";
					if(in_array($s_id,$InactiveSession))
					{
						$special_day_table .= "<tr bgcolor=red><td class=\"tabletext\">$record_date $editlink</td>";
						$special_warning = 1;
					}
					else
					{
						$special_day_table .= "<tr class=\"$css\"><td class=\"tabletext\">$record_date $editlink</td>";
					}
					//$special_day_table .= "<tr><td>$record_date $editlink</td>";
					if($non_school_day==1){
						$special_day_table .= "<td class=\"tabletext\" colspan=5>$i_StudentAttendance_NonSchoolDay</td>";
					}
					else
					{
						if($hasAM)
							$special_day_table .= "<td class=\"tabletext\">$am_time</td>";
						if($hasLunch)
							$special_day_table .= "<td class=\"tabletext\">$lunch_time</td>";
						if($hasPM)
							$special_day_table .= "<td class=\"tabletext\">$pm_time</td>";
						$special_day_table .= "<td class=\"tabletext\">$leave_school_time</td>";
					}
				}
			}
			if(sizeof($temp)==0)
			{
				$special_day_table .= "<tr class=\"tablerow2\"><td class=\"tabletext\" colspan=7 align=center>$i_no_record_exists_msg</td></tr>";
			}
			$special_day_table .= "</table>";
		}
		
		echo $special_day_table;
	break;
}

intranet_closedb();
?>