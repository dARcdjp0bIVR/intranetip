<?
include_once("../../../../../includes/global.php");
include_once("../../../../../includes/libdb.php");
include_once("../../../../../includes/libfilesystem.php");
include_once("../../../../../includes/libaccount.php");
include_once("../../../../../includes/libclass.php");
include_once("../../../../../includes/libcardstudentattend2.php");
include_once("../../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../../templates/adminheader_setting.php");

intranet_opendb();

$lclass = new libclass();
$lc = new libcardstudentattend2();
$lc->retrieveSettings();
$hasAM = ($lc->attendance_mode != 1);
$hasLunch = ($lc->attendance_mode != 0 && $lc->attendance_mode != 1);
$hasPM = ($lc->attendance_mode != 0);
$days = $lc->getSessionDayValueWithoutSpecificForClass($ClassID, $type);
$classname = $lc->getClassName($ClassID);

if($type==1)
{
	$select_title = $i_StudentAttendance_WeekDay;
	$select_day = "<SELECT name=DayValue>\n";
    $select_day .= "<OPTION value=''>-- $button_select --</OPTION>";
    for ($i=0; $i<sizeof($days); $i++)
    {
         $word = $i_DayType0[$days[$i]];
         $select_day .= "<OPTION value='".$days[$i]."'>".$word."</OPTION>\n";
    }
    $select_day .= "</SELECT>\n";
    $sql = "SELECT SessionID, SessionName, IF(NonSchoolDay=1, '-', Time_Format(MorningTime,'%H:%i')), IF(NonSchoolDay=1,'-',Time_Format(LunchStart,'%H:%i')), IF(NonSchoolDay=1,'-',Time_Format(LunchEnd,'%H:%i')), IF(NonSchoolDay=1,'-',Time_Format(LeaveSchoolTime,'%H:%i')), IF(NonSchoolDay = 0, 'N', 'Y') FROM CARD_STUDENT_TIME_SESSION WHERE RecordStatus = 1";
    //$sql = "SELECT b.SessionName, b.MorningTime, b.LunchStart, b.LunchEnd, b.LeaveSchoolTime, b.NonSchoolDay FROM CARD_STUDENT_TIME_SESSION_REGULAR AS a LEFT OUTER JOIN CARD_STUDENT_TIME_SESSION AS b ON (a.SessionID = b.SessionID) WHERE a.DayType = 1 AND a.DayValue IN (0,1,2,3,4,5,6)";
    $result = $lc->returnArray($sql,7);
    if(sizeof($result)>0)
    {
	    for($i=0; $i<sizeof($result); $i++)
	    {
		    list($s_id, $s_name, $AM_start_time, $lunch_time, $PM_start_time, $leave_time, $non_school_day) = $result[$i];
		    $table_content .= "<tr><td>$s_name</td>";
		    if($hasAM)
		    	$table_content .= "<td>$AM_start_time</td>";
			if($hasLunch)
		    	$table_content .= "<td>$lunch_time</td>";
		    if($hasPM)
		    	$table_content .= "<td>$PM_start_time</td>";
		    $table_content .= "<td>$leave_time</td><td>$non_school_day</td>";
		    $table_content .= "<td><input type=radio name=weekday_session value=$s_id></td></tr>";
	    }
	    $table_content .= "<input type=hidden name=type value=$type>";
    }
    if(sizeof($result)==0)
    {
	    $table_content .= "<tr><td colspan=7 align=center>$i_no_record_exists_msg</td></tr>";
    }
}

if($type==2)
{
	$select_title = $i_StudentAttendance_CycleDay;
    $select_day = getSelectByValue($days,"name=DayValue");
    $sql = "SELECT SessionID, SessionName, IF(NonSchoolDay=1,'-',Time_Format(MorningTime, '%H:%i')), IF(NonSchoolDay=1,'-',Time_Format(LunchStart, '%H:%i')), IF(NonSchoolDay=1,'-',Time_Format(LunchEnd, '%H:%i')), IF(NonSchoolDay=1,'-',Time_Format(LeaveSchoolTime, '%H:%i')), IF(NonSchoolDay = 0, 'N', 'Y') FROM CARD_STUDENT_TIME_SESSION WHERE RecordStatus = 1";
    $result = $lc->returnArray($sql,7);
    if(sizeof($result)>0)
    {
	    for($i=0; $i<sizeof($result); $i++)
	    {
		    list($s_id, $s_name, $AM_start_time, $lunch_time, $PM_start_time, $leave_time, $non_school_day) = $result[$i];
		    $table_content .= "<tr><td>$s_name</td>";
		    if($hasAM)
		    	$table_content .= "<td>$AM_start_time</td>";
			if($hasLunch)
		    	$table_content .= "<td>$lunch_time</td>";
		    if($hasPM)
		    	$table_content .= "<td>$PM_start_time</td>";
		    $table_content .= "<td>$leave_time</td><td>$non_school_day</td>";
		    $table_content .= "<td><input type=radio name=cycle_session value=$s_id></td></tr>";
	    }
	    $table_content .= "<input type=hidden name=type value=$type>";
    }
    if(sizeof($result)==0)
    {
	    $table_content .= "<tr><td colspan=7 align=center>$i_no_record_exists_msg</td></tr>";
    }
}

if($type==3)
{
	$legend = "<table width=100% border=0><tr><td align=center class=dynCalendar_card_not_confirmed>$i_StudentAttendance_CalendarLegend_RecordWithData</td></tr>";
	
	$select_title = $i_StudentAttendance_TimeSlot_SpecialDay;
	$select_day = "<input type=text name=TargetDate value='".date('Y-m-d')."' size=10>\n";
	$select_day .= "<script language=\"JavaScript\" type=\"text/javascript\">\n";
	$select_day .= "<!--\n";
    $select_day .= "startCal = new dynCalendar('startCal', 'calendarCallback', '/templates/calendar/images/');\n";
    $select_day .= "startCal.setLegend('$legend');\n";
    $select_day .= "startCal.differentDisplay = true;\n";
    $select_day .= "//-->\n";
	$select_day .= "</script>&nbsp;<span class=extraInfo>(yyyy-mm-dd)</span>\n";

	$sql = "SELECT SessionID, SessionName, IF(NonSchoolDay=1,'-',Time_Format(MorningTime,'%H:%i')), IF(NonSchoolDay=1,'-',Time_Format(LunchStart,'%H:%i')), IF(NonSchoolDay=1,'-',Time_Format(LunchEnd,'%H:%i')), IF(NonSchoolDay=1,'-',Time_Format(LeaveSchoolTime,'%H:%i')), IF(NonSchoolDay = 0, 'N', 'Y') FROM CARD_STUDENT_TIME_SESSION WHERE RecordStatus = 1";

	$result = $lc->returnArray($sql,7);
	if(sizeof($result)>0)
	{
	    for($i=0; $i<sizeof($result); $i++)
	    {
		    list($s_id, $s_name, $AM_start_time, $lunch_time, $PM_start_time, $leave_time, $non_school_day) = $result[$i];
		    $table_content .= "<tr><td>$s_name</td>";
		    if($hasAM)
		    	$table_content .= "<td>$AM_start_time</td>";
			if($hasLunch)
		    	$table_content .= "<td>$lunch_time</td>";
		    if($hasPM)
		    	$table_content .= "<td>$PM_start_time</td>";
		    $table_content .= "<td>$leave_time</td><td>$non_school_day</td>";
		    $table_content .= "<td><input type=radio name=special_session value=$s_id></td></tr>";
	    }
	    $table_content .= "<input type=hidden name=type value=$type>";
	}
	if(sizeof($result)==0)
	{
	    $table_content .= "<tr><td colspan=7 align=center>$i_no_record_exists_msg</td></tr>";
	}
	
	$records_with_data = array();
	$ts = time();
	$iteration = 0;
	
	# 20061113 (Kenneth): Change the method of retrival of which dates have data
	# Count late/early leave/absence
	# Count 3 months
	
	while($iteration < 3)
	{
		$year = date('Y',$ts);
		$month = date('m',$ts);
		
		$sql = "SELECT 
						RecordDate 
				FROM
						CARD_STUDENT_TIME_SESSION_DATE
				WHERE
						ClassID = $ClassID";

		$temp = @$lc->returnVector($sql);
		
		for($i=0; $i<sizeof($temp); $i++)
		{
			$day = substr($temp[$i],-2);
			$day = ($day < 10? "0".$day : $day);
			$entry_date = $year.$month.$day; #$year."-".$month."-".$day;
			$records_with_data[] = $entry_date;
		}
		$ts = mktime(0,0,0,$month-1,1,$year);
		$iteration++;
	}
}



?>

<script language="JavaScript" src='/templates/tooltip.js'></script>
<SCRIPT Language="JavaScript">
isMenu = true;
function checkForm()
{
	var obj = document.form1;
	var cnt = 0;
	if(obj.type.value == 1)
	{
		if(obj.DayValue.value == "")
		{
			alert("<?=$i_StudentAttendance_TimeSessionSettings_WeekdaySelection_Warning?>");
			return false;
		}
		else
		{
			if(typeof(obj.weekday_session.length) == "undefined")
			{
				if(obj.weekday_session.checked == true)
				{
					cnt = 1;
				}
			}
			else
			{
				for(i=0; i<obj.weekday_session.length; i++)
				{
					if(obj.weekday_session[i].checked == true)
					{
						cnt = 1;
						break;
					}
				}
			}
			if(cnt==1)
			{
				return true;
			}
			else
			{
				alert("<?=$i_StudentAttendance_TimeSessionSettings_SessionSelection_Warning?>");
				return false;
			}
		}
	}
	if(obj.type.value == 2)
	{
		if(obj.DayValue.value == "")
		{
			alert("<?=$i_StudentAttendance_TimeSessionSettings_CycledaySelection_Warning?>");
			return false;
		}
		else
		{
			if(typeof(obj.cycle_session.length) == "undefined")
			{
				if(obj.cycle_session.checked == true)
				{
					cnt = 1;
				}
			}
			else
			{
				for(i=0; i<obj.cycle_session.length; i++)
				{
					if(obj.cycle_session[i].checked == true)
					{
						cnt = 1;
						break;
					}
				}
			}
			if(cnt==1)
			{
				return true;
			}
			else
			{
				alert("<?=$i_StudentAttendance_TimeSessionSettings_SessionSelection_Warning?>");
				return false;
			}
		}
	}
	if(obj.type.value == 3)
	{
		if(typeof(obj.special_session.length) == "undefined")
		{
			if(obj.special_session.checked == true)
			{
				cnt = 1;
			}
		}
		else
		{
			for(i=0; i<obj.special_session.length; i++)
			{
				if(obj.special_session[i].checked == true)
				{
					cnt = 1;
					break;
				}
			}
		}
		if(cnt==1)
		{
			return true;
		}
		else
		{
			alert("<?=$i_StudentAttendance_TimeSessionSettings_SessionSelection_Warning?>");
			return false;
		}
	}
}
</SCRIPT>
<link rel="stylesheet" href="/templates/calendar/dynCalendar.css" type="text/css" media="screen">
<script LANGUAGE="javascript">
     var css_array = new Array;
     css_array[0] = "dynCalendar_card_no_data";
     css_array[1] = "dynCalendar_card_not_confirmed";
     css_array[2] = "dynCalendar_card_confirmed";
     var date_array = new Array;
     <?
     for ($i=0; $i<sizeof($records_with_data); $i++)
     {
          $date_string = $records_with_data[$i];
          
          //$isConfirmed = $confirmed_dates[$date_string];
          ?>
          date_array[<?=$date_string?>] = <?=($isConfirmed?2:1)?>;
          <?
     }
     ?>
</script>

<script src="/templates/calendar/browserSniffer.js" type="text/javascript" language="javascript"></script>
<script src="/templates/calendar/dynCalendar.js" type="text/javascript" language="javascript"></script>
<script type="text/javascript">
<!--
      // Calendar callback. When a date is clicked on the calendar
      // this function is called so you can do as you want with it
      function calendarCallback(date, month, year)
      {
                       if (String(month).length == 1) {
                               month = '0' + month;
                       }

                       if (String(date).length == 1) {
                               date = '0' + date;
                       }
                       dateValue =year + '-' + month + '-' + date;
                       document.forms['form1'].TargetDate.value = dateValue;
      }
// -->
</script>

<SCRIPT Language="JavaScript">
<!--
// AJAX follow-up
var callback = {
        success: function ( o )
        {
                jChangeContent( "Specialday_Info", o.responseText );
        }
}
// start AJAX
function retrieveSpecialdayInfo(session_id)
{
        //FormObject.testing.value = 1;
        obj = document.form1;
        var myElement = document.getElementById("Specialday_Info");
        
        showMenu("Specialday_Info","<table border=1 width=300 bordercolorlight=#FBFDEA bordercolordark=#B3B692 cellpadding=2 cellspacing=0 bgcolor=#FBFDEA><tr><td>Loading</td></tr></table>");
        YAHOO.util.Connect.setForm(obj);

        // page for processing and feedback
        var path = "getSpecialdayInfo.php?session_id=" + session_id;
        var connectionObject = YAHOO.util.Connect.asyncRequest("POST", path, callback);
}
-->
</SCRIPT>

<style type="text/css">
     #Specialday_Info{position:absolute; top:0px; left:0px; z-index:5; visibility:show; width:450px;}
</style>

<div id="Specialday_Info" style="position:absolute; width=0px; height=0px; visibility:hidden"></div>

<?= displayNavTitle($i_adminmenu_plugin, '', $i_StudentAttendance_System, '../../../',$i_SmartCard_SystemSettings,'../../',$i_StudentAttendance_TimeSlotSettings,'../', $i_StudentAttendance_Menu_Slot_Class,'index.php', $button_add, '') ?>
<?= displayTag("head_smartcardattendence_$intranet_session_language.gif", $msg) ?>

<form name=form1 action="class_add_update.php" method="POST" onSubmit="return checkForm()">

<table width=340 border=0 cellspacing=0 cellpadding=0 align=center>
<tr><td align=right><?=$i_ClassName?>:</td><td><?=$classname?></td></tr>
<tr><td height=5></td></tr>
<tr><td align=right><?=$select_title?>:</td><td><?=$select_day?></td></tr>
</table>
<br>
<table border=1 width=560 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align=center>
<tr><tr>
<td class=tableTitle><?=$i_StudentAttendance_Menu_Slot_Session_Name?></td>
<?
if($hasAM)
{
?>
<td class=tableTitle><?=$i_StudentAttendance_Menu_Time_Session_MorningTime?></td>
<?
}
if($hasLunch)
{
?>
<td class=tableTitle><?=$i_StudentAttendance_Menu_Time_Session_LunchStartTime?></td>
<?
}
if($hasPM)
{
?>
<td class=tableTitle><?=$i_StudentAttendance_Menu_Time_Session_LunchEndTime?></td>
<?
}
?>
<td class=tableTitle><?=$i_StudentAttendance_Menu_Time_Session_LeaveSchoolTime?></td>
<td class=tableTitle><?=$i_StudentAttendance_Menu_Time_Session_NonSchoolDay?></td>
<td class=tableTitle><?=$i_StudentAttendance_Menu_Time_Session_School_Use?></td>
</tr>
<?=$table_content?>
</table>
<?
if($type==1)
{
	$sql = "SELECT RecordDate, SessionID FROM CARD_STUDENT_TIME_SESSION_DATE AS a INNER JOIN INTRANET_CLASS AS b ON a.ClassID = b.ClassID WHERE a.ClassID = $ClassID";
	
	$result = $lc->returnArray($sql,2);
	if(sizeof($result)>0)
	{
		$info_table .= "<tr><td>";
		for ($i=0; $i<sizeof($result); $i++)
		{
			list ($record_date, $s_id) = $result[$i];
			$info_table .= "<a onMouseMove='overhere()' href='javascript:retrieveSpecialdayInfo($s_id)'>$record_date</a>&nbsp;&nbsp;&nbsp;";
			//$info_table .= "$record_date&nbsp;&nbsp;&nbsp;";
		}
		$info_table .= "</td></tr>";
	}
	if(sizeof($result)==0)
	{
		$info_table .= "<tr><td align=center>$i_no_record_exists_msg</td></tr>";
	}
?>
<br><br>
<table border=1 width=560 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align=center>
<tr class=tableTitle><td><?=$i_StudentAttendance_TimeSessionSettings_AlreadyHaveSpecialDaySession?></td></tr>
<?=$info_table?>
</table>
<?
}
?>
<input type=hidden name=ClassID value=<?=$ClassID?>>
<table border=0 width=560 align=center>
<tr><td height=20></td></tr>
<tr><td><hr size=1></td></tr> 
<tr><td align=right><?=btnSubmit()?><?=btnReset()?><a href="javascript:history.back()"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a></td></tr>
</table>
</form>

<?
include_once("../../../../../templates/adminfooter.php");
?>