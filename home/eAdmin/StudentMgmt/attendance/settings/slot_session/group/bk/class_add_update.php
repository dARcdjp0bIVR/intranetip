<?
include_once("../../../../../includes/global.php");
include_once("../../../../../includes/libfilesystem.php");
include_once("../../../../../includes/libaccount.php");
include_once("../../../../../includes/libdb.php");
include_once("../../../../../includes/libclass.php");
include_once("../../../../../includes/libcardstudentattend2.php");
include_once("../../../../../lang/lang.$intranet_session_language.php");

intranet_opendb();
$lclass = new libclass();

$lc = new libcardstudentattend2();
$lc->retrieveSettings();
$hasAM = ($lc->attendance_mode != 1);
$hasLunch = ($lc->attendance_mode != 0 && $lc->attendance_mode != 1);
$hasPM = ($lc->attendance_mode != 0);
$days = $lc->getSessionDayValueWithoutSpecific($type);

if($type==1)
{
	$sql = "INSERT IGNORE INTO CARD_STUDENT_TIME_SESSION_REGULAR VALUES ('', $ClassID, 1, '$DayValue', $weekday_session, '', '', NOW(), NOW())";
	$lc->db_db_query($sql);
	$x=1;
	if ($lc->db_affected_rows()==0)
	{
		$sql = "UPDATE CARD_STUDENT_TIME_SESSION_REGULAR SET SessionID = $weekday_session, DateModified = NOW() WHERE ClassID = $ClassID AND DayType = 1 AND DayValue = '$DayValue'";
		$lc->db_db_query($sql);
		$x=2;
	}
}
if($type==2)
{
	$sql = "INSERT IGNORE INTO CARD_STUDENT_TIME_SESSION_REGULAR VALUES ('', $ClassID, 2, '$DayValue', $cycle_session, '', '', NOW(), NOW())";
	$lc->db_db_query($sql);
	$x=1;
	if ($lc->db_affected_rows()==0)
	{
		$sql = "UPDATE CARD_STUDENT_TIME_SESSION_REGULAR SET SessionID = $cycle_session, DateModified = NOW() WHERE ClassID = $ClassID AND DayType = 2 AND DayValue = '$DayValue'";
		$lc->db_db_query($sql);
		$x=2;
	}	
}
if($type==3)
{
	$sql = "INSERT IGNORE INTO CARD_STUDENT_TIME_SESSION_DATE VALUES ('', $ClassID, '$TargetDate', $special_session, '', '', NOW(), NOW())";
	$lc->db_db_query($sql);
	$x=1;
	if ($lc->db_affected_rows()==0)
	{
		$sql = "UPDATE CARD_STUDENT_TIME_SESSION_DATE SET SessionID = $special_session, DateModified=NOW() WHERE ClassID = $ClassID AND RecordDate = '$TargetDate'";
		$lc->db_db_query($sql);
		$x=2;
	}
}

Header ("Location: class_edit.php?ClassID=$ClassID&msg=$x");
intranet_closedb();
?>