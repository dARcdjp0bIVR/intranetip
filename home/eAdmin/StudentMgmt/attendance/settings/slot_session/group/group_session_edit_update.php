<?
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();

if($type == 1)
{
	$sql = "UPDATE CARD_STUDENT_TIME_SESSION_REGULAR_GROUP SET SessionID = $weekday_session, DateModified = 'NOW()' WHERE groupid= $ClassID AND DayType = $type AND DayValue = '$value'";
	$lc->db_db_query($sql);
}

if($type == 2)
{
	$sql = "UPDATE CARD_STUDENT_TIME_SESSION_REGULAR_GROUP SET SessionID = $cycleday_session, DateModified = 'NOW()' WHERE groupid = $ClassID AND DayType = $type AND DayValue = '$value'";
	$lc->db_db_query($sql);
}

if($type == 3)
{
	$sql = "UPDATE CARD_STUDENT_TIME_SESSION_DATE_GROUP SET SessionID = $specialday_session, DateModified = 'NOW()' WHERE groupid = $ClassID AND RecordDate = '$value'";
	$lc->db_db_query($sql);
}

Header("Location: group_edit.php?ClassID=$ClassID&msg=2");
intranet_closedb();
?>