<?
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}
$lc = new libcardstudentattend2();

//echo "class_edit_update.php";

//echo $ClassID;
//echo $normal_session_id;

$sql = "INSERT IGNORE INTO CARD_STUDENT_TIME_SESSION_REGULAR_GROUP VALUES ('', $ClassID, 0, 0, $normal_session_id, '', '', NOW(), NOW())";
$lc->db_db_query($sql);
$x=1;
if ($lc->db_affected_rows()==0)
{
	$sql = "UPDATE CARD_STUDENT_TIME_SESSION_REGULAR_GROUP SET SessionID = $normal_session_id, DateModified = NOW() WHERE GROUPID = $ClassID AND DayType = 0 AND DayValue = 0";
	$lc->db_db_query($sql);
	$x=2;
}

Header ("Location: group_edit.php?ClassID=$ClassID&msg=$x");
intranet_closedb();

?>