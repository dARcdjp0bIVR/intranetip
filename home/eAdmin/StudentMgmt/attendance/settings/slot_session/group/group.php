<?
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();
$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageSystemSetting_TimeSessionSettings";

$linterface = new interface_html();
$classname = $lc->getSpecialGroupName($ClassID);

$class_mode = $lc->getSpecialGroupAttendanceMode($ClassID);

$select_mode = "<SELECT name=mode>\n";
$select_mode .= "<OPTION value=0 ".($class_mode==0?"SELECTED":"").">$i_StudentAttendance_GroupMode_UseSchoolTimetable</OPTION>\n";
$select_mode .= "<OPTION value=1 ".($class_mode==1?"SELECTED":"").">$i_StudentAttendance_GroupMode_UseGroupTimetable</OPTION>\n";
$select_mode .= "<OPTION value=2 ".($class_mode==2?"SELECTED":"").">$i_StudentAttendance_GroupMode_NoNeedToTakeAttendance</OPTION>\n";
$select_mode .= "</SELECT>\n";

$TAGS_OBJ[] = array($i_StudentAttendance_Menu_Slot_Session_Setting, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/settings/slot_session/session_setting/", 0);
$TAGS_OBJ[] = array($i_StudentAttendance_Menu_Slot_Session_School, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/settings/slot_session/school/", 0);
$TAGS_OBJ[] = array($i_StudentAttendance_Menu_Slot_Session_Class, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/settings/slot_session/class/", 0);
$TAGS_OBJ[] = array($i_StudentAttendance_Menu_Slot_Session_Group, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/settings/slot_session/group/", 1);
		
$MODULE_OBJ = $lc->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START(urldecode($Msg));

$PAGE_NAVIGATION[] = array($classname);
?>

<form name=form1 action='group_update.php' method=POST>
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	</tr>
	<tr>
    	<td>
    		<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_ClassName?></td>
					<td class="tabletext" width="70%"><?=$classname?></td>
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_StudentAttendance_ClassMode?></td>
					<td class="tabletext" width="70%"><?=$select_mode?></td>
				</tr>
				<? if ($class_mode == 1) { ?>
				<tr>
					<td valign="top" nowrap="nowrap" class="tabletext">&nbsp;</td>
					<td class="tabletext" width="70%">
						<a class="tablelink" href="group_edit.php?ClassID=<?=$ClassID?>"><?=$i_StudentAttendance_ClassMode_Edit?></a>
					</td>
				</tr>
				<? } ?>
			</table>
		</td>
	</tr>
	<tr>
    	<td>
		    <table width="96%" border="0" cellpadding="0" cellspacing="0" align="center">
		    	<tr>
                	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
		    </table>
	    </td>
	</tr>
    <tr>
	    <td align="center" colspan="2">
			<?= $linterface->GET_ACTION_BTN($button_save, "submit", "") ?>
			<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
			<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:window.location = 'index.php';","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
		</td>
	</tr>
</table>
<input type=hidden name=ClassID value=<?=$ClassID?>>

</form>
<?
$linterface->LAYOUT_STOP();
print $linterface->FOCUS_ON_LOAD("form1.mode");
intranet_closedb();
?>