<?php
// Editing by 
/*
 * 2017-09-06 (Carlos): Added [View Past Records] for [Special Day Settings].
 * 2015-08-31 (Carlos): Fixed inactive time session checking. 
 */
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lb=  new libdb();
$lc = new libcardstudentattend2();
$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageSystemSetting_TimeSessionSettings";

$linterface = new interface_html();

$lc->retrieveSettings();
//$lc->attendance_mode;
$hasAM = ($lc->attendance_mode != 1);
$hasLunch = ($lc->attendance_mode != 0 && $lc->attendance_mode != 1);
$hasPM = ($lc->attendance_mode != 0);

//$objSpecialGroup = new libcardstudentattendgroup();
//$classname = $objSpecialGroup->getSpecialGroupName($ClassID);
$classname = $lc->getSpecialGroupName($ClassID);

$normal_warning = 0;
$weekly_warning = 0;
$cycle_warning = 0;
$special_warning = 0;

### Find if has the Non School Day Session ###

$sql = "SELECT 
				a.SessionID,a.SessionName
		FROM 
				CARD_STUDENT_TIME_SESSION AS a 
		WHERE 
			   a.RecordStatus = 1 AND a.NonSchoolDay = 1 
        Order by 
               a.SessionID asc
		";
				
$NonSchoolDaySession_array=$lb->returnArray($sql);

if(sizeof($NonSchoolDaySession_array)!=0){
	
$synchronize_holidays="<div class='Conntent_tool'><a href='#TB_inline?height=450&width=750&inlineId=div1' title='$i_StudentAttendance_TimeSessionSettings_SessionSelection_Warning' class='generate thickbox' >".$Lang['StudentAttendance']['Button']['SynchronizeHolidays']."</a></div>";

//$synchronize_holidays=$linterface->GET_LNK_GENERATE("synchronize_holidays.php","$SynchronizeHolidays","","","",0);
}

$show_choose.="<div id='div1' style='display:none' class='edit_pop_board'>";
$show_choose.="<form name='form2' action='group_synchronize_holidays.php' method='post' ><input type='hidden' name='ClassID' id='ClassID' value='$ClassID'><div class='edit_pop_board_write' style='height:374px;'>";
for($i=0; $i<sizeof($NonSchoolDaySession_array); $i++){
	list ($session_id,$session_name) = $NonSchoolDaySession_array[$i];
//    $show_choose .= "<tr><td width='30px'><input type='radio' name='NonSchoolDaySession' value='$session_id'></td><td>$session_name</td></tr>";
    if($i==0) {$show_choose.="<p>".$linterface->Get_Radio_Button($session_id, "NonSchoolDaySession", $session_id,"1","",$session_name, "","")."</p>";}
    else{ $show_choose.="<p>".$linterface->Get_Radio_Button($session_id, "NonSchoolDaySession", $session_id,"","",$session_name, "","")."</p>";}
 
}

$show_choose.="</div>";
$show_choose.="<div class='edit_bottom' style='height:10px;'>
               <p class='spacer'></p>
			   <input name='submit_button' type='submit' class='formbutton' onclick='tb_remove();' onmouseover='this.className='formbuttonon'' onmouseout='this.className='formbutton'' value='".$Lang['Btn']['Submit']."' />
			   <input name='cancel_button' type='button' class='formbutton' onclick='tb_remove();' onmouseover='this.className='formbuttonon'' onmouseout='this.className='formbutton'' value='".$Lang['Btn']['Cancel']."' />
               <p class='spacer'></p>
               </div></form></div>
               ";


### Weekday Table ###
### Get The Inactive Session BUT Using By Class ###
$InactiveSession = array();
/*
$sql = "SELECT 
				a.SessionID
		FROM 
				CARD_STUDENT_TIME_SESSION AS a 
				LEFT OUTER JOIN 
				CARD_STUDENT_TIME_SESSION_REGULAR_GROUP AS b 
				ON (a.SessionID = b.SessionID AND b.GroupID = $ClassID AND b.DayType = 0)
		WHERE 
				a.RecordStatus = 0 AND a.NonSchoolDay = 0 
		GROUP BY 
				a.SessionID";

$result = $lc->returnArray($sql,1);
if(sizeof($result)>0)
{
	for($i=0;$i<sizeof($result);$i++)
	{
		list($inactive_session) = $result[$i];
		array_push($InactiveSession,$inactive_session);
	}
}
*/
$sql = "SELECT 
				a.SessionID
		FROM 
				CARD_STUDENT_TIME_SESSION AS a INNER JOIN CARD_STUDENT_TIME_SESSION_REGULAR_GROUP AS b ON (a.SessionID = b.SessionID AND b.GroupID ='$ClassID' AND b.DayType = 1)
		WHERE 
				a.RecordStatus = 0 
		GROUP BY 
				a.SessionID";
$InactiveSession = $lc->returnVector($sql);

//$result = $lc->getSessionTimeArrayList(1,$ClassID);
//$result = $objSpecialGroup->getSessionTimeArrayList(1,$ClassID);
$result = $lc->getGroupSessionTimeArrayList(1,$ClassID);

$weekday_table_content = "";    
$weekday_table_content .= "<tr class=\"tabletop\"><td class=\"tabletoplink\">$i_BookingType_Weekdays</td>";
if($hasAM)
	$weekday_table_content .= "<td class=\"tabletoplink\">$i_StudentAttendance_Menu_Time_Session_MorningTime</td>";
if($hasLunch)
	$weekday_table_content .= "<td class=\"tabletoplink\">$i_StudentAttendance_Menu_Time_Session_LunchStartTime</td>";
if($hasPM)
	$weekday_table_content .= "<td class=\"tabletoplink\">$i_StudentAttendance_Menu_Time_Session_LunchEndTime</td>";
$weekday_table_content .= "<td class=\"tabletoplink\">$i_StudentAttendance_Menu_Time_Session_LeaveSchoolTime</td></tr>";

if(sizeof($result)>0)
{
	for($i=0; $i<sizeof($result); $i++)
	{
		list ($s_id, $am_time, $lunch_time, $pm_time, $leave_school_time, $non_school_day, $day_value) = $result[$i];
		$css = ($i%2==0)?"tablerow1":"tablerow2";
		$weekday = $i_DayType0[$day_value];
		$editlink = "<a class=\"functionlink\" href=\"group_session_edit.php?type=1&ClassID=$ClassID&value=$day_value\"><img src=\"$image_path/icon_edit.gif\" border=0></a>";
		$editlink .= "<a class=functionlink href=\"javascript:removeDaySetting(1,$ClassID,'$day_value')\"><img src=\"$image_path/icon_erase.gif\" border=0></a>";

		if(in_array($s_id,$InactiveSession))
		{
			$weekday_table_content .= "<tr bgcolor=red><td class=\"tabletext\">$weekday $editlink</td>";
			$weekly_warning = 1;
		}
		else
		{
			$weekday_table_content .= "<tr class=\"$css\"><td class=\"tabletext\">$weekday $editlink</td>";
		}
		if($non_school_day==1){
			$weekday_table_content .= "<td class=\"tabletext\" colspan=5>$i_StudentAttendance_NonSchoolDay</td>";
		}
		else
		{
			if($hasAM)
				$weekday_table_content .= "<td class=\"tabletext\">$am_time</td>";
			if($hasLunch)
				$weekday_table_content .= "<td class=\"tabletext\">$lunch_time</td>";
			if($hasPM)
				$weekday_table_content .= "<td class=\"tabletext\">$pm_time</td>";
			$weekday_table_content .= "<td class=\"tabletext\">$leave_school_time</td>";		
		}	
	}
}
if(sizeof($result)==0)
{
	$weekday_table_content .= "<tr class=\"tablerow2\"><td class=\"tabletext\" colspan=7 align=center>$i_no_record_exists_msg</td></tr>";
}
### End Of Weekday Table ###

### Cycle Day Table ###
### Get The Inactive Session BUT Using By Class ###
$InactiveSession = array();
/*
$sql = "SELECT 
				a.SessionID
		FROM 
				CARD_STUDENT_TIME_SESSION AS a 
				LEFT OUTER JOIN 
				CARD_STUDENT_TIME_SESSION_REGULAR_GROUP AS b 
				ON (a.SessionID = b.SessionID AND b.GroupID = $ClassID AND b.DayType = 0)
		WHERE 
				a.RecordStatus = 0 AND a.NonSchoolDay = 0 
		GROUP BY 
				a.SessionID";

$result = $lc->returnArray($sql,1);
if(sizeof($result)>0)
{
	for($i=0;$i<sizeof($result);$i++)
	{
		list($inactive_session) = $result[$i];
		array_push($InactiveSession,$inactive_session);
	}
}
*/
$sql = "SELECT 
				a.SessionID
		FROM 
				CARD_STUDENT_TIME_SESSION AS a INNER JOIN CARD_STUDENT_TIME_SESSION_REGULAR_GROUP AS b ON (a.SessionID = b.SessionID AND b.GroupID='$ClassID' AND b.DayType = 2)
		WHERE 
				a.RecordStatus = 0 
		GROUP BY 
				a.SessionID";		
$InactiveSession = $lc->returnVector($sql);

//$result = $lc->getSessionTimeArrayList(2,$ClassID);
//$result = $objSpecialGroup->getSessionTimeArrayList(2,$ClassID);
$result = $lc->getGroupSessionTimeArrayList(2,$ClassID);

$cycle_table_content = "";    
$cycle_table_content .= "<tr class=\"tabletop\"><td class=\"tabletoplink\">$i_BookingType_Cycleday</td>";
if($hasAM)
	$cycle_table_content .= "<td class=\"tabletoplink\">$i_StudentAttendance_Menu_Time_Session_MorningTime</td>";
if($hasLunch)
	$cycle_table_content .= "<td class=\"tabletoplink\">$i_StudentAttendance_Menu_Time_Session_LunchStartTime</td>";
if($hasPM)
	$cycle_table_content .= "<td class=\"tabletoplink\">$i_StudentAttendance_Menu_Time_Session_LunchEndTime</td>";
$cycle_table_content .= "<td class=\"tabletoplink\">$i_StudentAttendance_Menu_Time_Session_LeaveSchoolTime</td></tr>";

if(sizeof($result)>0)
{
	for($i=0; $i<sizeof($result); $i++)
	{
		list ($s_id, $am_time, $lunch_time, $pm_time, $leave_school_time, $non_school_day, $day_value) = $result[$i];
		$css = ($i%2==0)?"tablerow1":"tablerow2";
		$cycleday = $day_value;
		$editlink = "<a class=functionlink href=\"group_session_edit.php?type=2&ClassID=$ClassID&value=$cycleday\"><img src=\"$image_path/icon_edit.gif\" border=0></a>";
		$editlink .= "<a class=functionlink href=\"javascript:removeDaySetting(2,$ClassID,'$day_value')\"><img src=\"$image_path/icon_erase.gif\" border=0></a>";
		if(in_array($s_id,$InactiveSession))
		{
			$cycle_table_content .= "<tr bgcolor=red><td class=\"tabletext\">$cycleday $editlink</td>";
			$cycle_warning = 1;
		}
		else
		{
			$cycle_table_content .= "<tr class=\"$css\"><td class=\"tabletext\">$cycleday $editlink</td>";
		}
		if($non_school_day==1){
                 $cycle_table_content .="<td class=\"tabletext\" colspan=5>$i_StudentAttendance_NonSchoolDay</td>";
        }
        else
        {
			//$cycle_table_content .= "<tr><td>$cycleday $editlink</td>";
			if($hasAM)
				$cycle_table_content .= "<td class=\"tabletext\">$am_time</td>";
			if($hasLunch)
				$cycle_table_content .= "<td class=\"tabletext\">$lunch_time</td>";
			if($hasPM)
				$cycle_table_content .= "<td class=\"tabletext\">$pm_time</td>";
			$cycle_table_content .= "<td class=\"tabletext\">$leave_school_time</td></tr>";
		}
	}
}
if(sizeof($result)==0)
{
	$cycle_table_content .= "<tr class=\"tablerow2\"><td class=\"tabletext\" colspan=7 align=center>$i_no_record_exists_msg</td></tr>";
}
### End Of Cycle Day Table ###

### Special Day Table ###
$InactiveSession = array();
/*
$sql = "SELECT 
				a.SessionID
		FROM 
				CARD_STUDENT_TIME_SESSION AS a 
				LEFT OUTER JOIN 
				CARD_STUDENT_TIME_SESSION_REGULAR_GROUP AS b 
				ON (a.SessionID = b.SessionID AND b.GroupID = $ClassID AND b.DayType = 0)
		WHERE 
				a.RecordStatus = 0 AND a.NonSchoolDay = 0 
		GROUP BY 
				a.SessionID";

$result = $lc->returnArray($sql,1);
if(sizeof($result)>0)
{
	for($i=0;$i<sizeof($result);$i++)
	{
		list($inactive_session) = $result[$i];
		array_push($InactiveSession,$inactive_session);
	}
}
*/
$sql = "SELECT a.SessionID 
		FROM CARD_STUDENT_TIME_SESSION as a 
		INNER JOIN CARD_STUDENT_TIME_SESSION_DATE_GROUP as b ON a.SessionID=b.SessionID 
		WHERE b.GroupID='$ClassID' AND a.RecordStatus=0";
$InactiveSession = $lc->returnVector($sql);

$sql = "SELECT 
				a.RecordID, b.SessionID, b.SessionName, a.RecordDate ,TIME_FORMAT(b.MorningTime,'%H:%i'), TIME_FORMAT(b.LunchStart,'%H:%i'), TIME_FORMAT(b.LunchEnd,'%H:%i'), TIME_FORMAT(b.LeaveSchoolTime,'%H:%i'), b.NonSchoolDay
		FROM 
				CARD_STUDENT_TIME_SESSION_DATE_GROUP AS a LEFT OUTER JOIN CARD_STUDENT_TIME_SESSION AS b ON (a.SessionID = b.SessionID)
		WHERE
				GROUPID = '$ClassID' AND a.RecordDate >= '".date("Y-m-d")."'
		ORDER BY	
				a.RecordDate 
		";

$temp = $lc->returnArray($sql,9);

$special_day_table = " ";
$special_day_table = "<tr class=\"tabletop\"><td class=\"tabletoplink\">$i_StudentAttendance_Slot_Special</td>";
if($hasAM)
	$special_day_table .= "<td class=\"tabletoplink\">$i_StudentAttendance_Menu_Time_Session_MorningTime</td>";
if($hasLunch)
	$special_day_table .= "<td class=\"tabletoplink\">$i_StudentAttendance_Menu_Time_Session_LunchStartTime</td>";
if($hasPM)
	$special_day_table .= "<td class=\"tabletoplink\">$i_StudentAttendance_Menu_Time_Session_LunchEndTime</td>";
$special_day_table .= "<td class=\"tabletoplink\">$i_StudentAttendance_Menu_Time_Session_LeaveSchoolTime</td></tr>";

if(sizeof($temp)>0)
{	
	for($i=0; $i<sizeof($temp); $i++)
	{
		list($record_id, $s_id, $s_name, $record_date, $am_time, $lunch_time, $pm_time, $leave_school_time, $non_school_day) = $temp[$i];
		$css = ($i%2==0)?"tablerow1":"tablerow2";
		$special_date = $record_date;
		$editlink = "<a class=functionlink href=\"group_session_edit.php?type=3&ClassID=$ClassID&value=$special_date\"><img src=\"$image_path/icon_edit.gif\" border=0></a>";
		$editlink .= "<a class=functionlink href=\"javascript:removeSpecialDaySetting($ClassID,'$special_date')\"><img src=\"$image_path/icon_erase.gif\" border=0></a>";
		if(in_array($s_id,$InactiveSession))
		{
			$special_day_table .= "<tr bgcolor=red><td class=\"tabletext\">$record_date $editlink</td>";
			$special_warning = 1;
		}
		else
		{
			$special_day_table .= "<tr class=\"$css\"><td class=\"tabletext\">$record_date $editlink</td>";
		}
		//$special_day_table .= "<tr><td>$record_date $editlink</td>";
		if($non_school_day==1){
			$special_day_table .= "<td class=\"tabletext\" colspan=5>$i_StudentAttendance_NonSchoolDay</td>";
		}
		else
		{
			if($hasAM)
				$special_day_table .= "<td class=\"tabletext\">$am_time</td>";
			if($hasLunch)
				$special_day_table .= "<td class=\"tabletext\">$lunch_time</td>";
			if($hasPM)
				$special_day_table .= "<td class=\"tabletext\">$pm_time</td>";
			$special_day_table .= "<td class=\"tabletext\">$leave_school_time</td>";
		}
	}
}
if(sizeof($temp)==0)
{
	$special_day_table .= "<tr class=\"tablerow2\"><td class=\"tabletext\" colspan=7 align=center>$i_no_record_exists_msg</td></tr>";
}
### End Of Special Day Table ###

### Start Of Normal Table ###
### Get The Current Using Session ###
$sql = "SELECT
				SessionID
		FROM
				CARD_STUDENT_TIME_SESSION_REGULAR_GROUP
		WHERE
				GROUPID = $ClassID AND DayType = 0 AND DayValue = 0
		ORDER BY
				SessionID";
$result = $lc->returnArray($sql,1);
if(sizeof($result)>0)
{
	list($using_session) = $result[0];
}

### Get The Inactive Session BUT Using By Class ###
$InactiveSession = array();
/*
$sql = "SELECT 
				a.SessionID
		FROM 
				CARD_STUDENT_TIME_SESSION AS a 
				LEFT OUTER JOIN 
				CARD_STUDENT_TIME_SESSION_REGULAR_GROUP AS b 
				ON (a.SessionID = b.SessionID AND b.GroupID = $ClassID AND b.DayType = 0)
		WHERE 
				a.RecordStatus = 0 AND a.NonSchoolDay = 0 
		GROUP BY 
				a.SessionID";
$result = $lc->returnArray($sql,1);
if(sizeof($result)>0)
{
	for($i=0;$i<sizeof($result);$i++)
	{
		list($inactive_session) = $result[$i];
		array_push($InactiveSession,$inactive_session);
	}
}
*/
$sql = "SELECT 
				a.SessionID
		FROM 
				CARD_STUDENT_TIME_SESSION AS a INNER JOIN CARD_STUDENT_TIME_SESSION_REGULAR_GROUP AS b ON (a.SessionID = b.SessionID AND b.GroupID='$ClassID' AND b.DayType = 0)
		WHERE 
				a.RecordStatus = 0 AND a.NonSchoolDay = 0
		GROUP BY 
				a.SessionID";
$InactiveSession = $lc->returnVector($sql);

### List ALL Sessions ###
if(!in_array($using_session,$InactiveSession))
{
	$sql = "SELECT 
					SessionID, SessionName, TIME_FORMAT(MorningTime,'%H:%i'), TIME_FORMAT(LunchStart,'%H:%i'), TIME_FORMAT(LunchEnd,'%H:%i'), TIME_FORMAT(LeaveSchoolTime,'%H:%i'), NonSchoolDay
			FROM 
					CARD_STUDENT_TIME_SESSION
			WHERE
					RecordStatus = 1 AND NonSchoolDay != 1
			ORDER BY
					SessionID";
	$temp = $lc->returnArray($sql,7);
}
else
{
	$sql = "SELECT 
					SessionID, SessionName, TIME_FORMAT(MorningTime,'%H:%i'), TIME_FORMAT(LunchStart,'%H:%i'), TIME_FORMAT(LunchEnd,'%H:%i'), TIME_FORMAT(LeaveSchoolTime,'%H:%i'), NonSchoolDay
			FROM 
					CARD_STUDENT_TIME_SESSION
			WHERE
					(RecordStatus = 1 OR SessionID = $using_session) AND NonSchoolDay != 1
			ORDER BY
					SessionID";
	$temp = $lc->returnArray($sql,7);
}

if(sizeof($temp)>0)
{
	$normal_table_content .= "";
	
	for($i=0; $i<sizeof($temp); $i++)
	{
		list ($s_id, $s_name, $am_time, $lunch_time, $pm_time, $leave_school_time, $non_school_day, $day_type, $day_value) = $temp[$i];
		
		if(($using_session == $s_id)&&(in_array($using_session,$InactiveSession)))
		{
			$normal_table_content .= "<tr bgcolor=red><td>* $s_name</td>";
			$normal_warning = 1;
		}
		else
		{
			$normal_table_content .= "<tr><td>$s_name</td>";
		}
		if($hasAM)
			$normal_table_content .= "<td>$am_time</td>";
		if($hasLunch)
			$normal_table_content .= "<td>$lunch_time</td>";
		if($hasPM)
			$normal_table_content .= "<td>$pm_time</td>";
		$normal_table_content .= "<td>$leave_school_time</td>";

		$checked = " ";
		//if(($day_type!="")&&($day_value!="")&&($day_type==0)&&($day_value==0))
		if($s_id == $using_session)
		{
			$checked = "checked=1";
		}
		else
		{
			$checked = " ";
		}
		$normal_table_content .= "<td><input type=radio name=normal_session_id value=$s_id $checked></td></tr>";
		$normal_table_content .= "<input type=hidden name=ClassID value=$ClassID>";
	}
	
}
### End Of Normal Table ###

$TAGS_OBJ[] = array($i_StudentAttendance_Menu_Slot_Session_Setting, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/settings/slot_session/session_setting/", 0);
$TAGS_OBJ[] = array($i_StudentAttendance_Menu_Slot_Session_School, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/settings/slot_session/school/", 0);
$TAGS_OBJ[] = array($i_StudentAttendance_Menu_Slot_Session_Class, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/settings/slot_session/class/", 0);
$TAGS_OBJ[] = array($i_StudentAttendance_Menu_Slot_Session_Group, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/settings/slot_session/group/", 1);
		
$MODULE_OBJ = $lc->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START(urldecode($Msg));

$PAGE_NAVIGATION[] = array($classname);

$PAGE_NAVIGATION1 = $i_StudentAttendance_NormalDays;
$PAGE_NAVIGATION2 = $i_StudentAttendance_Weekday_Specific;
$PAGE_NAVIGATION3 = $i_StudentAttendance_Cycleday_Specific;
$PAGE_NAVIGATION4 = $i_StudentAttendance_SpecialDay;

if ($msg == 1) $SysMsg = $linterface->GET_SYS_MSG("add");
if ($msg == 2) $SysMsg = $linterface->GET_SYS_MSG("update");
if ($msg == 3) $SysMsg = $linterface->GET_SYS_MSG("delete");
?>
<script type="text/javascript" src="/templates/jquery/thickbox.js"></script>
<link rel="stylesheet" href="/templates/jquery/thickbox.css" type="text/css" />
<SCRIPT Language="JavaScript">
function removeDaySetting(Type, ClassID ,DayValue)
{
	if (confirm('<?=$i_Usage_RemoveConfirm?>'))
	{
		location.href = "group_session_remove.php?type="+Type+"&ClassID="+ClassID+"&value="+DayValue;
	}
}
function removeSpecialDaySetting(ClassID, targetDate)
{
	if (confirm('<?=$i_Usage_RemoveConfirm?>'))
	{
		location.href = "group_session_remove.php?ClassID="+ClassID+"&TargetDate="+targetDate;
	}
}

function reloadSpecialDayTable(viewPast)
{
	$('#SpecialDayTable').html('<?=$linterface->Get_Ajax_Loading_Image()?>');
	$('#ReloadSpecialDayTableBtn').remove();
	$.post(
		'../ajax_task.php',
		{
			"task":"get_special_day_table",
			"type":"group",
			"ClassID":'<?=$ClassID?>',
			"viewPast":viewPast
		},
		function(returnHtml){
			$('#SpecialDayTable').html(returnHtml);
		}
	);
}
</SCRIPT>

<form name=form1 action="group_edit_update.php" method=POST>
<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
		<td class="tabletext" align="right" colspan="2"><?=$SysMsg?></td>
	</tr>
</table>
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	</tr
</table>
<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
		<td><?= $linterface->GET_NAVIGATION2($PAGE_NAVIGATION1) ?></td>
	</tr>
	<tr>
		<td align="left">
		<?
			$rx = "<a class=\"contenttool\" href=\"javascript:removeDaySetting(0,'".$ClassID."',0)\">";
			$rx .= "<img src=\"".$PATH_WRT_ROOT."/images/2009a/icon_delete.gif\" border=\"0\" align=\"absmiddle\"> ";
			$rx .= $button_clear;
			$rx .= "</a>";
			echo $rx;
		?>
		</td>
	</tr>
</table>
<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr class="tabletop">
		<td class="tabletoplink"><?=$i_StudentAttendance_Menu_Slot_Session_Name?></td>
		<?
		if($hasAM) {
		?>
		<td class="tabletoplink"><?=$i_StudentAttendance_Menu_Time_Session_MorningTime?></td>
		<?
		}
		if($hasLunch) {
		?>
		<td class="tabletoplink"><?=$i_StudentAttendance_Menu_Time_Session_LunchStartTime?></td>
		<?
		}
		if($hasPM) {
		?>
		<td class="tabletoplink"><?=$i_StudentAttendance_Menu_Time_Session_LunchEndTime?></td>
		<?
		}
		?>
		<td class="tabletoplink"><?=$i_StudentAttendance_Menu_Time_Session_LeaveSchoolTime?></td>
		<td class="tabletoplink"><?=$i_StudentAttendance_Menu_Time_Session_Using?></td>
	</tr>
	<?=$normal_table_content?>
</table>


<?
if($normal_warning==1)
{
?>
<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
		<td class="tabletext"><span class="tabletextrequire"><?=$i_StudentAttendance_TimeSession_UsingInactiveSession_Warning?></span></td>
	</tr>
</table>
<?
}
?>
<table width="96%" border="0" cellpadding="0" cellspacing="0" align="center">
	<tr>
    	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
    </tr>
</table>
<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
    <tr>
	    <td align="center">
			<?= $linterface->GET_ACTION_BTN($button_save, "submit", "") ?>
			<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
		</td>
	</tr>
</table>
<br />

<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
		<td><?= $linterface->GET_NAVIGATION2($PAGE_NAVIGATION2) ?></td>
	</tr>
	<tr>
		<td align="left"><?= $linterface->GET_LNK_NEW("group_add.php?type=1&ClassID=$ClassID","","","","",0) ?></tr>
	</tr>
</table>
<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<?=$weekday_table_content?>
</table>
<?
if($weekly_warning==1)
{
?>
<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
		<td align="left" class="tabletext"><span class="tabletextrequire"><?=$i_StudentAttendance_TimeSession_UsingInactiveSession_Warning?></span></td>
	</tr>
</table>
<?
}
?>
<table width="96%" border="0" cellpadding="0" cellspacing="0" align="center">
	<tr>
    	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
    </tr>
</table>
<br />
<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
		<td><?= $linterface->GET_NAVIGATION2($PAGE_NAVIGATION3) ?></td>
	</tr>
	<tr>
		<td align="left"><?= $linterface->GET_LNK_NEW("group_add.php?type=2&ClassID=$ClassID","","","","",0) ?></tr>
	</tr>
</table>
<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<?=$cycle_table_content?>
</table>
<?
if($cycle_warning==1)
{
?>
<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
		<td align="left" class="tabletext"><span class="tabletextrequire"><?=$i_StudentAttendance_TimeSession_UsingInactiveSession_Warning?></span></td>
	</tr>
</table>
<?
}
?>
<table width="96%" border="0" cellpadding="0" cellspacing="0" align="center">
	<tr>
    	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
    </tr>
</table>
<br />
<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
		<td><?= $linterface->GET_NAVIGATION2($PAGE_NAVIGATION4) ?></td>
	</tr>
	<tr>
		<td align="left">
			<?= $linterface->GET_LNK_NEW("group_add.php?type=3&ClassID=$ClassID","","","","",0) ?>
			<?=$synchronize_holidays?>
			<a id="ReloadSpecialDayTableBtn" class="contenttool" href="javascript:reloadSpecialDayTable(1);" style="display:block;padding:2px;">
				<img src="<?=$image_path.'/'.$LAYOUT_SKIN?>/icon_view.gif" width="20" height="20" border="0" align="absmiddle">
				<?=$i_StudentAttendance_ViewPastRecords?>
			</a>
		</td>
	</tr>
</table>
<div id="SpecialDayTable" style="text-align:center">
<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<?=$special_day_table?>
</table>
</div>
<?
if($special_warning==1)
{
?>
<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
		<td align="left" class="tabletext"><span class="tabletextrequire"><?=$i_StudentAttendance_TimeSession_UsingInactiveSession_Warning?></span></td>
	</tr>
</table>
<?
}
?>
<table width="96%" border="0" cellpadding="0" cellspacing="0" align="center">
	<tr>
    	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
    </tr>
</table>
</form>
<?=$show_choose?>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>