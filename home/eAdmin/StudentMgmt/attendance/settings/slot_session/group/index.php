<?php
## Using By : Max
##########################################################
## Modification Log
## 2010-02-04: Max (201001221711)
## - support multiple classes change
##########################################################
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();
$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageSystemSetting_TimeSessionSettings";

$linterface = new interface_html();

$_recordTypeID = 3;  #3 ==> class type)
//$lc->setSpecialGroupRecordType(3);
$classList = $lc->getGroupListMode($_recordTypeID);

# selection table
if ($_POST["showAll"] == "" && $_GET["showAll"] == "") {
	$showAll = -1;
} else if ($_POST["showAll"] == "") {
	$showAll = $_GET["showAll"];
} else {
	$showAll = $_POST["showAll"];
}
if($showAll == "1")
{
	$_SESSION['groupShowAll'] = "1";   
}
else if($showAll == "0")
{
	unset($_SESSION['groupShowAll']);
}

$selectionTable = $lc->getSelectionTableGroupMode($showAll);


$table_content = " ";
$table_content .= "<table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">";
$table_content .= "<tr class=\"tabletop\"><td class=\"tabletoplink\">$i_GroupName</td><td class=\"tabletoplink\">$i_StudentAttendance_GroupMode</td>
<td><input type='checkbox' name='ClassIDsMaster' onClick=\"(this.checked)?setChecked(1,this.form,'ClassIDs[]'):setChecked(0,this.form,'ClassIDs[]')\" /></td>
</tr>";
if(sizeof($classList)>0)
{
	for($i=0; $i<sizeof($classList); $i++)
	{
		list($class_id, $class_name, $mode) = $classList[$i];
		if ($mode == 2)
		{
			$word_mode = $i_StudentAttendance_GroupMode_NoNeedToTakeAttendance;			
		}
		else if ($mode == 1)
		{
			$word_mode = $i_StudentAttendance_GroupMode_UseGroupTimetable;
		}
		else
		{
			$word_mode = $i_StudentAttendance_GroupMode_UseSchoolTimetable;
		}
		$css = ($i%2? "tablerow1":"tablerow2");
     	$editlink = "<a class=functionlink href=group.php?ClassID=$class_id><img src=\"$image_path/icon_edit.gif\" border=0></a>";
     	$table_content .= "<tr class=\"$css\">\n";
     	$table_content .= "<td class=\"tabletext\">$class_name $editlink</td>
     										 <td class=\"tabletext\">$word_mode</td>";
     	$table_content .= "<td><input type='checkbox' name='ClassIDs[]' value='{$class_id}' /></td>";
     	$table_content .= "</tr>\n";
	}
}
$table_content .= "</table>";


$TAGS_OBJ[] = array($i_StudentAttendance_Menu_Slot_Session_Setting, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/settings/slot_session/session_setting/", 0);
$TAGS_OBJ[] = array($i_StudentAttendance_Menu_Slot_Session_School, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/settings/slot_session/school/", 0);
$TAGS_OBJ[] = array($i_StudentAttendance_Menu_Slot_Session_Class, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/settings/slot_session/class/", 0);
$TAGS_OBJ[] = array($i_StudentAttendance_Menu_Slot_Session_Group, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/settings/slot_session/group/", 1);
		
$MODULE_OBJ = $lc->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();
?>
<script type="text/javascript" src="../../settings_common_script.js"></script>
<form name="form1" action="index.php" method="POST">
	<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	</tr>
	<tr>
  	<td>
<?=$selectionTable?>
		</td>
	</tr>
	</table>
<?
if ($msg == 2) $SysMsg = $linterface->GET_SYS_MSG("update");
?>
<br />
<?=$table_content?>
</form>
<br />
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
