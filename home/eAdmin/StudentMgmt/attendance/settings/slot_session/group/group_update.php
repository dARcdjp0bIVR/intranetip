<?php
## Using By 
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();

$ClassIDsArray = $_POST['ClassIDs'];
$showAll = $_POST['showAll'];

$updateSuccess = true;
$url = "index.php";

if (is_array($ClassIDsArray) && count($ClassIDsArray) > 0) {
	foreach($ClassIDsArray as $key => $ClassID) {
		$updateSuccess *= updateRecords($mode, $ClassID);
	}
	$url = "index.php";
} else {
	$updateSuccess = updateRecords($mode, $ClassID);
	if ($mode != 1)
	{ 
		$url = "group.php";
	}
	else
	{
	    $url = "group_edit.php";
	}
}

if ($updateSuccess) {
	$Msg = $Lang['StudentAttendance']['SettingApplySuccess'];
} else {
	$Msg = $Lang['StudentAttendance']['SettingApplyFail'];
}

function updateRecords($mode, $ClassID) {
	$li = new libdb();
	
	if ($mode != 1 && $mode != 2)
	{
	    $mode = 0;
	}
	
	$li = new libdb();
	$sql = "INSERT INTO CARD_STUDENT_ATTENDANCE_GROUP (GroupID, mode, DateInput, DateModified)
	               VALUES ('$ClassID','$mode',NOW(),NOW())";
	$Result["CARD_STUDENT_ATTENDANCE_GROUP"] = $li->db_db_query($sql);
	
	if ($li->db_affected_rows()!=1)
	{
	    $sql = "UPDATE CARD_STUDENT_ATTENDANCE_GROUP
	                   SET mode = '$mode', DateModified = NOW() WHERE GroupID = '$ClassID'";
	
	    $Result["CARD_STUDENT_ATTENDANCE_GROUP"] = $li->db_db_query($sql);
	}
	
	if ($mode != 1)
	{
	    # Clear detail timetable
	    $sql = "DELETE FROM CARD_STUDENT_TIME_SESSION_REGULAR_GROUP WHERE GroupID = '$ClassID'";
	    # Clear special timetable
	    $sql2= "DELETE FROM CARD_STUDENT_TIME_SESSION_DATE_GROUP WHERE GroupID='$ClassID' ";
	    $Result["CARD_STUDENT_TIME_SESSION_REGULAR_GROUP"] = $li->db_db_query($sql);
	    $Result["CARD_STUDENT_TIME_SESSION_DATE_GROUP"] = $li->db_db_query($sql2);
	//    $url = "group.php";
	}
	else
	{
		// do nothing
	//    $url = "group_edit.php";
	}
	
	$success = false;
	if (in_array(false,$Result)) {
		$li->RollBack_Trans();
	}
	else {
		$li->Commit_Trans();
		$success = true;
	}
	return $success;
}
intranet_closedb();
header("Location: $url?ClassID=$ClassID&Msg=".urlencode($Msg)."&showAll=$showAll");
?>