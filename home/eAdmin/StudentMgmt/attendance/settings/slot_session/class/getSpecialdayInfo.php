<?
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();
$CurrentPageArr['StudentAttendance'] = 1;

$li = new libdb();
$lc->retrieveSettings();

$linterface = new interface_html();

$hasAM = ($lc->attendance_mode != 1);
$hasLunch = ($lc->attendance_mode != 0 && $lc->attendance_mode != 1);
$hasPM = ($lc->attendance_mode != 0);

intranet_opendb();

$sql = "SELECT 
				SessionName, IF(NonSchoolDay=1,'-',TIME_FORMAT(MorningTime,'%H:%i')), IF(NonSchoolDay=1,'-',TIME_FORMAT(LunchStart,'%H:%i')), IF(NonSchoolDay=1,'-',TIME_FORMAT(LunchEnd,'%H:%i')), IF(NonSchoolDay=1,'-',Time_Format(LeaveSchoolTime,'%H:%i')), IF(NonSchoolDay=1, 'Y', 'N')
		FROM
				CARD_STUDENT_TIME_SESSION
		WHERE
				SessionID = $session_id
		";
		
$result = $li->returnArray($sql);

$layer_content = "<table border=\"0\" width=\"300\" cellpadding=\"3\" cellspacing=\"0\">";
$layer_content .= "<tr class=\"tablebluebottom\"><td class=\"tabletoplink\" colspan=\"8\" align=\"right\">".$linterface->GET_BTN("x", "button", "hideMenu('Specialday_Info')")."</td></tr>";
$layer_content .= "<tr class=\"tablebluetop\"><td class=\"tabletoplink\" width=\"1\">#</td><td class=\"tabletoplink\">Session Name</td>";
if($hasAM)
	$layer_content .= "<td class=\"tabletoplink\">$i_StudentAttendance_SetTime_AMStart</td>";
if($hasLunch)
	$layer_content .= "<td class=\"tabletoplink\">$i_StudentAttendance_SetTime_LunchStart</td>";
if($hasPM)
	$layer_content .= "<td class=\"tabletoplink\">$i_StudentAttendance_SetTime_PMStart</td>";
$layer_content .= "<td class=\"tabletoplink\">$i_StudentAttendance_SetTime_SchoolEnd</td><td class=\"tabletoplink\">$i_StudentAttendance_NonSchoolDay</td></tr>";

if (sizeof($result)==0)
	$layer_content .= "<tr class=\"tablebluerow2\"><td class=\"tabletext\" colspan=\"8\" align=\"center\">$i_no_record_exists_msg</td></tr>";
else
{
	for($i=0; $i<sizeof($result); $i++)
	{
		list($s_name, $am_time, $lunch_time, $pm_time, $leave_time, $non_school_day) = $result[$i];
		$css = ($i%2==0)?"tablebluerow1":"tablebluerow2";
		$no = $i+1;
		$layer_content .= "<tr class=\"$css\"><td class=\"tabletext\">$no</td><td class=\"tabletext\">$s_name</td>";
		if($hasAM)
			$layer_content .= "<td class=\"tabletext\">$am_time</td>";
		if($hasLunch)
			$layer_content .= "<td class=\"tabletext\">$lunch_time</td>";
		if($hasPM)
			$layer_content .= "<td class=\"tabletext\">$pm_time</td>";
		$layer_content .= "<td class=\"tabletext\">$leave_time</td><td class=\"tabletext\">$non_school_day</td></tr>";
	}
}

$layer_content .= "<tr class=\"tablebluerow2\"><td class=\"tabletext\" colspan=\"8\" align=\"center\"><span style=\"color:red;\">$i_StudentAttendance_TimeSession_GetSpecialdayInfo_Warning</span></td></tr>";
$layer_content .= "</table>";

/*$response = iconv("Big5","UTF-8",$layer_content);
echo $response;*/

echo $layer_content;
intranet_closedb();
?>