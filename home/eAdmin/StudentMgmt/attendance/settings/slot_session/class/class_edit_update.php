<?
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();

$sql = "INSERT IGNORE INTO CARD_STUDENT_TIME_SESSION_REGULAR 
				VALUES 
					('', $ClassID, 0, 0, $normal_session_id, '', '', NOW(), NOW()) 
				ON DUPLICATE KEY UPDATE 
					SessionID = $normal_session_id, 
					DateModified = NOW()";
$Result = $lc->db_db_query($sql);

if ($Result) {
	$Msg = $Lang['StudentAttendance']['SettingApplySuccess'];
}
else {
	$Msg = $Lang['StudentAttendance']['SettingApplyFail'];
}

Header ("Location: class_edit.php?ClassID=$ClassID&Msg=".urlencode($Msg));
intranet_closedb();

?>