<?
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");

intranet_auth();
intranet_opendb();

$lc = new libcardstudentattend2();
$lc->Start_Trans();
if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$ApplyAlsoList = $_REQUEST['ApplyAlsoList'];
$ApplyAlsoList[] = $ClassID;

for ($j=0; $j< sizeof($ApplyAlsoList); $j++) {
	if($type==1)
	{
		$sql = "INSERT IGNORE INTO CARD_STUDENT_TIME_SESSION_REGULAR 
						VALUES 
							('', '".$ApplyAlsoList[$j]."', 1, '$DayValue', $weekday_session, '', '', NOW(), NOW()) 
						ON DUPLICATE KEY UPDATE 
							SessionID = $weekday_session, 
							DateModified = NOW()";
		$Result['InsertTimeSessionRegular-ClassID:'.$ApplyAlsoList[$j]] = $lc->db_db_query($sql);
	}
	if($type==2)
	{
		$sql = "INSERT IGNORE INTO CARD_STUDENT_TIME_SESSION_REGULAR 
						VALUES 
							('', '".$ApplyAlsoList[$j]."', 2, '$DayValue', $cycle_session, '', '', NOW(), NOW()) 
						ON DUPLICATE KEY UPDATE 
							SessionID = $cycle_session, 
							DateModified = NOW()
						";
		$Result['InsertTimeSessionRegular-ClassID'.$ApplyAlsoList[$j]] = $lc->db_db_query($sql);
	}
	if($type==3)
	{
		$TargetDate = $_REQUEST['target_date'];
		for ($i=0; $i< sizeof($TargetDate); $i++) {
			$sql = "INSERT INTO CARD_STUDENT_TIME_SESSION_DATE VALUES 
							('', '".$ApplyAlsoList[$j]."', '".$TargetDate[$i]."', '".$special_session."', '', '', NOW(), NOW()) 
							on duplicate key update 
								SessionID = '".$special_session."', 
								DateModified = NOW()
							";
			$Result['InsertTimeSessionDate-ClassID:'.$ApplyAlsoList[$j].'-TargetDate:'.$TargetDate[$i]] = $lc->db_db_query($sql);
		}
	}
}

if (in_array(false,$Result)) {
	$Msg = $Lang['StudentAttendance']['SettingApplyFail'];
	$lc->RollBack_Trans();
}
else {
	$Msg = $Lang['StudentAttendance']['SettingApplySuccess'];
	$lc->Commit_Trans();
}

Header ("Location: class_edit.php?ClassID=$ClassID&Msg=".urlencode($Msg));
intranet_closedb();
?>