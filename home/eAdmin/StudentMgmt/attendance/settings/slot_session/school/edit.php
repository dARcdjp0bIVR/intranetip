<?
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();
$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageSystemSetting_TimeSessionSettings";

$linterface = new interface_html();

$lclass = new libclass();

$lc->retrieveSettings();
$hasAM = ($lc->attendance_mode != 1);
$hasLunch = ($lc->attendance_mode != 0 && $lc->attendance_mode != 1);
$hasPM = ($lc->attendance_mode != 0);
$days = $lc->getSessionDayValueWithoutSpecific($type);

if($type == 1)
{
	$select_title = $i_StudentAttendance_WeekDay;
    $select_day = $i_DayType0[$value];
    $sql = "SELECT SessionID FROM CARD_STUDENT_TIME_SESSION_REGULAR WHERE DayType = $type AND DayValue = '$value'";
    $result = $lc->returnVector($sql);
    $using_session = $result[0];
    
    $sql = "SELECT SessionID, SessionName, IF(NonSchoolDay=1,'-',TIME_FORMAT(MorningTime,'%H:%i')), IF(NonSchoolDay=1,'-',TIME_FORMAT(LunchStart,'%H:%i')), IF(NonSchoolDay=1,'-',TIME_FORMAT(LunchEnd,'%H:%i')), IF(NonSchoolDay=1,'-',TIME_FORMAT(LeaveSchoolTime,'%H:%i')), IF(NonSchoolDay = 0, 'N', 'Y') FROM CARD_STUDENT_TIME_SESSION WHERE RecordStatus = 1"; 
    
    $result = $lc->returnArray($sql,7);
    if(sizeof($result)>0)
    {
	    for($i=0; $i<sizeof($result); $i++)
	    {
		    list($s_id, $s_name, $AM_start_time, $lunch_time, $PM_start_time, $leave_time, $non_school_day) = $result[$i];
		    $css = ($i%2==0)?"tablerow1":"tablerow2";
		    $table_content .= "<tr class=\"$css\"><td class=\"tabletext\">$s_name</td>";
		    if($hasAM)
		    	$table_content .= "<td class=\"tabletext\">$AM_start_time</td>";
			if($hasLunch)
		    	$table_content .= "<td class=\"tabletext\">$lunch_time</td>";
		    if($hasPM)
		    	$table_content .= "<td class=\"tabletext\">$PM_start_time</td>";
		    $table_content .= "<td class=\"tabletext\">$leave_time</td><td class=\"tabletext\">$non_school_day</td>";
		    
			if($using_session == $s_id)
			{
				$checked = "checked=\"1\"";
			}
			else
			{
				$checked = " ";
			}
		    
		    $table_content .= "<td class=\"tabletext\"><input type=\"radio\" name=\"weekday_session\" value=\"$s_id\" $checked></td></tr>";
	    }
	    $table_content .= "<input type=\"hidden\" name=\"type\" value=\"$type\">";
	    $table_content .= "<input type=\"hidden\" name=\"value\" value=\"$value\">";
    }
    if(sizeof($result)==0)
    {
	    $table_content .= "<tr class=\"tablerow2\"><td class=\"tabletext\" colspan=\"7\" align=\"center\">$i_no_record_exists_msg</td></tr>";
    }
}

if($type == 2)
{
	$select_title = $i_StudentAttendance_CycleDay;
    $select_day = $value;
    $sql = "SELECT SessionID FROM CARD_STUDENT_TIME_SESSION_REGULAR WHERE DayType = $type AND DayValue = '$value'";
    $result = $lc->returnVector($sql);
    $using_session = $result[0];
    
    $sql = "SELECT SessionID, SessionName, IF(NonSchoolDay=1,'-',TIME_FORMAT(MorningTime,'%H:%i')), IF(NonSchoolDay=1,'-',TIME_FORMAT(LunchStart,'%H:%i')), IF(NonSchoolDay=1,'-',TIME_FORMAT(LunchEnd,'%H:%i')), IF(NonSchoolDay=1,'-',TIME_FORMAT(LeaveSchoolTime,'%H:%i')), IF(NonSchoolDay = 0, 'N', 'Y') FROM CARD_STUDENT_TIME_SESSION WHERE RecordStatus = 1";
    
    $result = $lc->returnArray($sql,9);
    if(sizeof($result)>0)
    {
	    for($i=0; $i<sizeof($result); $i++)
	    {
		    list($s_id, $s_name, $AM_start_time, $lunch_time, $PM_start_time, $leave_time, $non_school_day, $day_type, $day_value) = $result[$i];
		    $css = ($i%2==0)?"tablerow1":"tablerow2";
		    $table_content .= "<tr class=\"$css\"><td class=\"tabletext\">$s_name</td>";
		    if($hasAM)
		    	$table_content .= "<td class=\"tabletext\">$AM_start_time</td>";
			if($hasLunch)
		    	$table_content .= "<td class=\"tabletext\">$lunch_time</td>";
		    if($hasPM)
		    	$table_content .= "<td class=\"tabletext\">$PM_start_time</td>";
		    $table_content .= "<td class=\"tabletext\">$leave_time</td><td class=\"tabletext\">$non_school_day</td>";
		    
			if($using_session == $s_id)
			{
				$checked = "checked=\"1\"";
			}
			else
			{
				$checked = " ";
			}
		    
		    $table_content .= "<td class=\"tabletext\"><input type=\"radio\" name=\"cycleday_session\" value=\"$s_id\" $checked></td></tr>";
	    }
	    $table_content .= "<input type=\"hidden\" name=\"type\" value=\"$type\">";
	    $table_content .= "<input type=\"hidden\" name=\"value\" value=\"$value\">";
    }
    if(sizeof($result)==0)
    {
	    $table_content .= "<tr class=\"tablerow2\"><td class=\"tabletext\" colspan=\"7\" align=\"center\">$i_no_record_exists_msg</td></tr>";
    }
}
if($type==3)
{
	$select_title = $i_StudentAttendance_TimeSlot_SpecialDay;
    $select_day = $value;
	
    $sql = "SELECT SessionID FROM CARD_STUDENT_TIME_SESSION_DATE WHERE RecordDate = '$value'";
	$result = $lc->returnVector($sql);
	$using_session = $result[0];
	
	$sql = "SELECT SessionID, SessionName, IF(NonSchoolDay=1,'-',TIME_FORMAT(MorningTime,'%H:%i')), IF(NonSchoolDay=1,'-',TIME_FORMAT(LunchStart,'%H:%i')), IF(NonSchoolDay=1,'-',TIME_FORMAT(LunchEnd,'%H:%i')), IF(NonSchoolDay=1,'-',TIME_FORMAT(LeaveSchoolTime,'%H:%i')), IF(NonSchoolDay = 0, 'N', 'Y') FROM CARD_STUDENT_TIME_SESSION WHERE RecordStatus = 1";
	$result = $lc->returnArray($sql,9);
    if(sizeof($result)>0)
    {
	    for($i=0; $i<sizeof($result); $i++)
	    {
		    list($s_id, $s_name, $am_time, $lunch_time, $pm_time, $leave_school_time, $non_school_date) = $result[$i];
		    $css = ($i%2==0)?"tablerow1":"tablerow2";
		    if($using_session == $s_id)
		    	$table_content .= "<tr bgcolor=\"red\"><td class=\"tabletext\">$s_name</td>";
		    else
		    	$table_content .= "<tr class=\"$css\"><td class=\"tabletext\">$s_name</td>";
		    if($hasAM)
		    	$table_content .= "<td class=\"tabletext\">$am_time</td>";
			if($hasLunch)
		    	$table_content .= "<td class=\"tabletext\">$lunch_time</td>";
		    if($hasPM)
		    	$table_content .= "<td class=\"tabletext\">$pm_time</td>";
		    $table_content .= "<td class=\"tabletext\">$leave_school_time</td><td class=\"tabletext\">$non_school_date</td>";
		    
			if($using_session == $s_id)
			{
				$checked = "checked=\"1\"";
			}
			else
			{
				$checked = " ";
			}
		    
		    $table_content .= "<td class=\"tabletext\"><input type=\"radio\" name=\"specialday_session\" value=\"$s_id\" $checked></td></tr>";
	    }
	    $table_content .= "<input type=\"hidden\" name=\"type\" value=\"$type\">";
	    $table_content .= "<input type=\"hidden\" name=\"value\" value=\"$value\">";
	    $table_content .= "<tr class=\"tablebottom\"><td class=\"tabletext\" colspan=\"7\" align=\"left\"><font color=\"red\">red - now using</font></td></tr>";
    }
    if(sizeof($result)==0)
    {
	    $table_content .= "<tr class=\"tablerow2\"><td class=\"tabletext\" colspan=\"7\" align=\"center\">$i_no_record_exists_msg</td></tr>";
    }
}

$TAGS_OBJ[] = array($i_StudentAttendance_Menu_Slot_Session_Setting, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/settings/slot_session/session_setting/", 0);
$TAGS_OBJ[] = array($i_StudentAttendance_Menu_Slot_Session_School, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/settings/slot_session/school/", 1);
$TAGS_OBJ[] = array($i_StudentAttendance_Menu_Slot_Session_Class, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/settings/slot_session/class/", 0);
$TAGS_OBJ[] = array($i_StudentAttendance_Menu_Slot_Session_Group, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/settings/slot_session/group/", 0);
		
$MODULE_OBJ = $lc->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

$PAGE_NAVIGATION[] = array($button_edit);

if ($msg == 2) $SysMsg = $linterface->GET_SYS_MSG("update");
?>
<br />
<form name="form1" action="edit_update.php" method="POST" onSubmit="return checkForm()">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	</tr>
	<tr>
		<td>
			<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $select_title ?></td>
					<td class="tabletext" width="70%"><?=$select_day?></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr class="tabletop">
					<td class="tabletoplink"><?=$i_StudentAttendance_Menu_Slot_Session_Name?></td>
					<?
					if($hasAM) {
					?>
					<td class="tabletoplink"><?=$i_StudentAttendance_Menu_Time_Session_MorningTime?></td>
					<?
					}
					if($hasLunch) {
					?>
					<td class="tabletoplink"><?=$i_StudentAttendance_Menu_Time_Session_LunchStartTime?></td>
					<?
					}
					if($hasPM) {
					?>
					<td class="tabletoplink"><?=$i_StudentAttendance_Menu_Time_Session_LunchEndTime?></td>
					<?
					}
					?>
					<td class="tabletoplink"><?=$i_StudentAttendance_Menu_Time_Session_LeaveSchoolTime?></td>
					<td class="tabletoplink"><?=$i_StudentAttendance_Menu_Time_Session_NonSchoolDay?></td>
					<td class="tabletoplink"><?=$i_StudentAttendance_Menu_Time_Session_School_Use?></td>
				</tr>
				<?=$table_content?>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table width="96%" border="0" cellpadding="0" cellspacing="0" align="center">
		    	<tr>
                	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
		    </table>	
		</td>
	</tr>
	<tr>
	    <td align="center" colspan="2">
			<?= $linterface->GET_ACTION_BTN($button_save, "submit", "") ?>
			<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
			<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:history.back()","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
		</td>
	</tr>
</table>
</form>
<br />
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>