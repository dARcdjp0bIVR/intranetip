<?
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$li = new libcardstudentattend2();
$li->Start_Trans();
if($type == 1 || $type == 2)
{
	$sql = "DELETE FROM CARD_STUDENT_TIME_SESSION_REGULAR WHERE ClassID = 0 AND DayType = $type AND DayValue = '$value'";
	$Result = $li->db_db_query($sql);
}
# Delete more than one special day settings
else if($type == 7)
{
	$TargetDate = str_replace(",", "','", $TargetDate);
	$sql = "DELETE FROM CARD_STUDENT_TIME_SESSION_DATE WHERE ClassID = 0 AND RecordDate IN ('$TargetDate')";
	$Result = $li->db_db_query($sql);
} else
{
	$sql = "DELETE FROM CARD_STUDENT_TIME_SESSION_DATE WHERE ClassID = 0 AND RecordDate = '$TargetDate'";
	$Result = $li->db_db_query($sql);
}

if ($Result) {
	$Msg = $Lang['StudentAttendance']['SettingApplySuccess'];
	$li->Commit_Trans();
}
else {
	$Msg = $Lang['StudentAttendance']['SettingApplyFail'];
	$li->RollBack_Trans();
}

intranet_closedb();
header("Location: index.php?Msg=".urlencode($Msg));
?>