<?
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();
$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageSystemSetting_TimeSessionSettings";

$linterface = new interface_html();

$lclass = new libclass();

$lc->retrieveSettings();
$hasAM = ($lc->attendance_mode != 1);
$hasLunch = ($lc->attendance_mode != 0 && $lc->attendance_mode != 1);
$hasPM = ($lc->attendance_mode != 0);
$days = $lc->getSessionDayValueWithoutSpecific($type);


if ($type==1)
{
	$select_title = $i_StudentAttendance_WeekDay;
	$select_day = "<select name=\"DayValue\">\n";
    $select_day .= "<option value=''>-- $button_select --</option>";
    for ($i=0; $i<sizeof($days); $i++)
    {
         $word = $i_DayType0[$days[$i]];
         $select_day .= "<option value='".$days[$i]."'>".$word."</option>\n";
    }
    $select_day .= "</select>\n";
    
    $sql = "SELECT SessionID, SessionName, IF(NonSchoolDay=1,'-',Time_Format(MorningTime,'%H:%i')), IF(NonSchoolDay=1,'-',Time_Format(LunchStart,'%H:%i')), IF(NonSchoolDay=1,'-',Time_Format(LunchEnd,'%H:%i')), IF(NonSchoolDay=1,'-',Time_Format(LeaveSchoolTime,'%H:%i')), IF(NonSchoolDay = 0, 'N', 'Y') FROM CARD_STUDENT_TIME_SESSION WHERE RecordStatus = 1";
    $result = $lc->returnArray($sql,7);
    
    if(sizeof($result)>0)
    {
	    for($i=0; $i<sizeof($result); $i++)
	    {
		    list($s_id, $s_name, $AM_start_time, $lunch_time, $PM_start_time, $leave_time, $non_school_day) = $result[$i];
		    $css = $i%2==0?"tablerow1 tabletext":"tablerow2 tabletext";
		    $table_content .= "<tr class=\"$css\"><td class=\"tabletext\">$s_name</td>";
		    if($hasAM)
		    	$table_content .= "<td class=\"tabletext\">$AM_start_time</td>";
			if($hasLunch)
		    	$table_content .= "<td class=\"tabletext\">$lunch_time</td>";
		    if($hasPM)
		    	$table_content .= "<td class=\"tabletext\">$PM_start_time</td>";
		    $table_content .= "<td class=\"tabletext\">$leave_time</td><td class=\"tabletext\">$non_school_day</td>";
		    $table_content .= "<td class=\"tabletext\"><input type=\"radio\" name=\"weekday_session\" value=\"$s_id\"></td></tr>";
	    }
	    $table_content .= "<input type=\"hidden\" name=\"type\" value=\"$type\">";
    }
    if(sizeof($result)==0)
    {
	    $table_content .= "<tr class=\"tablerow1 tabletext\"><td colspan=\"7\" align=\"center\">$i_no_record_exists_msg</td></tr>";
    }
}

if($type==2)
{
	$select_title = $i_StudentAttendance_CycleDay;
    $select_day = getSelectByValue($days,"name=DayValue");
    $sql = "SELECT SessionID, SessionName, IF(NonSchoolDay=1,'-',TIME_FORMAT(MorningTime,'%H:%i')), IF(NonSchoolDay=1,'-',TIME_FORMAT(LunchStart,'%H:%i')), IF(NonSchoolDay=1,'-',TIME_FORMAT(LunchEnd,'%H:%i')), IF(NonSchoolDay=1,'-',TIME_FORMAT(LeaveSchoolTime,'%H:%i')), IF(NonSchoolDay = 0, 'N', 'Y') FROM CARD_STUDENT_TIME_SESSION WHERE RecordStatus = 1";
    $result = $lc->returnArray($sql,7);
    if(sizeof($result)>0)
    {
	    for($i=0; $i<sizeof($result); $i++)
	    {
		    list($s_id, $s_name, $AM_start_time, $lunch_time, $PM_start_time, $leave_time, $non_school_day) = $result[$i];
		    $css=($i%2==0)?"tablerow1":"tablerow2";
		    $table_content .= "<tr class=\"$css\"><td class=\"tabletext\">$s_name</td>";
		    if($hasAM)
		    	$table_content .= "<td class=\"tabletext\">$AM_start_time</td>";
			if($hasLunch)
		    	$table_content .= "<td class=\"tabletext\">$lunch_time</td>";
		    if($hasPM)
		    	$table_content .= "<td class=\"tabletext\">$PM_start_time</td>";
		    $table_content .= "<td class=\"tabletext\">$leave_time</td><td class=\"tabletext\">$non_school_day</td>";
		    $table_content .= "<td class=\"tabletext\"><input type=\"radio\" name=\"cycle_session\" value=\"$s_id\"></td></tr>";
	    }
	    $table_content .= "<input type=\"hidden\" name=\"type\" value=\"$type\">";
    }
    if(sizeof($result)==0)
    {
	    $table_content .= "<tr class=\"tablerow1 tabletext\"><td colspan=\"7\" align=\"center\">$i_no_record_exists_msg</td></tr>";
    }
}

if($type==3)
{
	$legend = "<table width=100% border=0><tr><td align=center class=dynCalendar_card_not_confirmed>$i_StudentAttendance_CalendarLegend_RecordWithData</td></tr>";
	
	$select_title = $i_StudentAttendance_TimeSlot_SpecialDay;
	$select_day = '<span id="dates">';
	$select_day .= $linterface->GET_DATE_PICKER("target_date[]", date('Y-m-d'),"","yy-mm-dd","","","","","target_date0");
	$select_day .= "<br></span>\n";
	$select_day .= $linterface->GET_SMALL_BTN(" + ", "button", "addDate(document.getElementById('dateFieldCount').value)")."<br />";

	$sql = "SELECT SessionID, SessionName, IF(NonSchoolDay=1,'-',TIME_FORMAT(MorningTime,'%H:%i')), IF(NonSchoolDay=1,'-',TIME_FORMAT(LunchStart,'%H:%i')), IF(NonSchoolDay=1,'-',TIME_FORMAT(LunchEnd,'%H:%i')), IF(NonSchoolDay=1,'-',TIME_FORMAT(LeaveSchoolTime,'%H:%i')), IF(NonSchoolDay = 0, 'N', 'Y') FROM CARD_STUDENT_TIME_SESSION WHERE RecordStatus = 1";
	$result = $lc->returnArray($sql,7);
	if(sizeof($result)>0)
	{
	    for($i=0; $i<sizeof($result); $i++)
	    {
		    list($s_id, $s_name, $AM_start_time, $lunch_time, $PM_start_time, $leave_time, $non_school_day) = $result[$i];
		    $css=($i%2==0)?"tablerow1":"tablerow2";
		    $table_content .= "<tr class=\"$css\"><td class=\"tabletext\">$s_name</td>";
		    if($hasAM)
		    	$table_content .= "<td class=\"tabletext\">$AM_start_time</td>";
			if($hasLunch)
		    	$table_content .= "<td class=\"tabletext\">$lunch_time</td>";
		    if($hasPM)
		    	$table_content .= "<td class=\"tabletext\">$PM_start_time</td>";
		    $table_content .= "<td class=\"tabletext\">$leave_time</td><td class=\"tabletext\">$non_school_day</td>";
		    $table_content .= "<td class=\"tabletext\"><input type=\"radio\" name=\"special_session\" value=\"$s_id\"></td></tr>";
	    }
	    $table_content .= "<input type=\"hidden\" name=\"type\" value=\"$type\">";
	}
	if(sizeof($result)==0)
	{
	    $table_content .= "<tr class=\"tablerow1 tabletext\"><td colspan=\"7\" align=\"center\">$i_no_record_exists_msg</td></tr>";
	}
	
	$records_with_data = array();
	$ts = time();
	$iteration = 0;
	
	# 20061113 (Kenneth): Change the method of retrival of which dates have data
	# Count late/early leave/absence
	# Count 3 months
	
	while($iteration < 3)
	{
		$year = date('Y',$ts);
		$month = date('m',$ts);
		
		$sql = "SELECT 
						RecordDate 
				FROM
						CARD_STUDENT_TIME_SESSION_DATE
				WHERE
						ClassID = 0";

		$temp = @$lc->returnVector($sql);
		
		for($i=0; $i<sizeof($temp); $i++)
		{
			$day = substr($temp[$i],-2);
			$day = ($day < 10? "0".$day : $day);
			$entry_date = $year.$month.$day; #$year."-".$month."-".$day;
			$records_with_data[] = $entry_date;
		}
		$ts = mktime(0,0,0,$month-1,1,$year);
		$iteration++;
	}
}

$TAGS_OBJ[] = array($i_StudentAttendance_Menu_Slot_Session_Setting, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/settings/slot_session/session_setting/", 0);
$TAGS_OBJ[] = array($i_StudentAttendance_Menu_Slot_Session_School, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/settings/slot_session/school/", 1);
$TAGS_OBJ[] = array($i_StudentAttendance_Menu_Slot_Session_Class, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/settings/slot_session/class/", 0);
$TAGS_OBJ[] = array($i_StudentAttendance_Menu_Slot_Session_Group, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/settings/slot_session/group/", 0);
		
$MODULE_OBJ = $lc->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

$PAGE_NAVIGATION[] = array($button_new);
?>

<script language="JavaScript" src='/templates/tooltip.js'></script>
<script language="JavaScript" src="<?=$intranet_httppath?>/templates/ajax_yahoo.js"></script>
<script language="JavaScript" src="<?=$intranet_httppath?>/templates/ajax_connection.js"></script>
<script Language="JavaScript">
isMenu = true;
function checkForm()
{
	var obj = document.form1;
	var cnt = 0;
	if(obj.type.value == 1)
	{
		if(obj.DayValue.value == "")
		{
			alert("<?=$i_StudentAttendance_TimeSessionSettings_WeekdaySelection_Warning?>");
			return false;
		}
		else
		{
			if(typeof(obj.weekday_session.length) == "undefined")
			{
				if(obj.weekday_session.checked == true)
				{
					cnt = 1;
				}
			}
			else
			{
				for(i=0; i<obj.weekday_session.length; i++)
				{				
					if(obj.weekday_session[i].checked == true)
					{
						cnt = 1;
						break;
					}
				}
			}
			if(cnt==1)
			{
				return true;
			}
			else
			{
				alert("<?=$i_StudentAttendance_TimeSessionSettings_SessionSelection_Warning?>");
				return false;
			}
		}
	}
	if(obj.type.value == 2)
	{
		if(obj.DayValue.value == "")
		{
			alert("<?=$i_StudentAttendance_TimeSessionSettings_CycledaySelection_Warning?>");
			return false;
		}
		else
		{
			if(typeof(obj.cycle_session.length) == "undefined")
			{
				if(obj.cycle_session.checked == true)
				{
					cnt = 1;
				}
			}
			else
			{
				for(i=0; i<obj.cycle_session.length; i++)
				{
					if(obj.cycle_session[i].checked == true)
					{
						cnt = 1;
						break;
					}
				}
			}
			if(cnt==1)
			{
				return true;
			}
			else
			{
				alert("<?=$i_StudentAttendance_TimeSessionSettings_SessionSelection_Warning?>");
				return false;
			}
		}
	}
	if(obj.type.value == 3)
	{
		if(typeof(obj.special_session.length) == "undefined")
		{
			if(obj.special_session.checked == true)
			{
				cnt = 1;
			}
		}
		else
		{
			for(i=0; i<obj.special_session.length; i++)
			{
				if(obj.special_session[i].checked == true)
				{
					cnt = 1;
					break;
				}
			}
		}
		if(cnt==1)
		{
			return true;
		}
		else
		{
			alert("<?=$i_StudentAttendance_TimeSessionSettings_SessionSelection_Warning?>");
			return false;
		}
	}
}
</script>
<script language="javascript">
	var css_array = new Array;
	css_array[0] = "dynCalendar_card_no_data";
	css_array[1] = "dynCalendar_card_not_confirmed";
	css_array[2] = "dynCalendar_card_confirmed";
	var date_array = new Array;
	<?
     for ($i=0; $i<sizeof($records_with_data); $i++)
     {
          $date_string = $records_with_data[$i];
          
          ?>
          date_array[<?=$date_string?>] = <?=($isConfirmed?2:1)?>;
          <?
     }
     ?>
</script>
<script Language="JavaScript">
<!--
// AJAX follow-up
var callback = {
        success: function ( o )
        {
                jChangeContent( "Specialday_Info", o.responseText );
        }
}
// start AJAX
function retrieveSpecialdayInfo(SessionID)
{
        //FormObject.testing.value = 1;
        obj = document.form1;
        var myElement = document.getElementById("Specialday_Info");
        
        jChangeContent( "Specialday_Info", "<table border='0' width='300' cellpadding='3' cellspacing='0'><tr class='tablebluetop'><td class='tabletoplink'>Loading</td></tr></table>" );
        myElement.style.display = 'block';
				myElement.style.visibility = 'visible';
        YAHOO.util.Connect.setForm(obj);

        // page for processing and feedback
        var path = "getSpecialdayInfo.php?session_id=" + SessionID;
        var connectionObject = YAHOO.util.Connect.asyncRequest("POST", path, callback);
}

function moveObject( obj, e, moveLeft ) {
	var tempX = 0;
	var tempY = 0;
	var offset = 8;
	var objHolder = obj;
	var moveDistance = 0;

	obj = document.getElementById(obj);
	if (obj==null) {return;}

	if (document.all) {
		tempX = event.clientX + document.body.scrollLeft;
		tempY = event.clientY + document.body.scrollTop;
	} else {
		tempX = e.pageX;
		tempY = e.pageY;
	}

	if(moveLeft == undefined) {
		moveDistance = 300;		
	} else {
		moveDistance = moveLeft;
	}
	
	if (tempX < 0){tempX = 0} else {tempX = tempX - moveDistance}
	if (tempY < 0){tempY = 0} else {tempY = tempY}

	obj.style.top  = (tempY + offset) + 'px';
	obj.style.left = (tempX + offset) + 'px';
}

function closeLayer(LayerID) {
	var obj = document.getElementById(LayerID);
	obj.style.display = 'none';
	obj.style.visibility = 'hidden';
}

function addDate(count){
	document.form1.dateFieldCount.value = count+1;
	obj = document.getElementById('dates');
	if(obj==null) return;
	$.post('../../../dailyoperation/preset_absence/ajax_get_new_date_picker.php',{"DateCount":count},function(data){
			if (data == "die") 
				window.top.location = '/';
			else {
				$('span#dates').append(data);
			}
		});
}
-->
</script>

<style type="text/css">
     #Specialday_Info{position:absolute; top:0px; left:0px; z-index:5; visibility:show; width:450px;}
</style>

<div id="Specialday_Info" style="position:absolute; width=0px; height=0px; visibility:hidden"></div>

<form name="form1" action="add_update.php" method="POST" onSubmit="return checkForm()">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	</tr>
	<tr>
    	<td>
    		<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$select_title?></td>
					<td class="tabletext" width="70%">
						<?=$select_day?>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
    	<td>
    		<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr class="tabletop">
					<td class="tabletoplink"><?=$i_StudentAttendance_Menu_Slot_Session_Name?></td>
					<?
					if($hasAM) {
					?>
					<td class="tabletoplink"><?=$i_StudentAttendance_Menu_Time_Session_MorningTime?></td>
					<?
					}
					if($hasLunch) {
					?>
					<td class="tabletoplink"><?=$i_StudentAttendance_Menu_Time_Session_LunchStartTime?></td>
					<?
					}
					if($hasPM) {
					?>
					<td class="tabletoplink"><?=$i_StudentAttendance_Menu_Time_Session_LunchEndTime?></td>
					<?
					}
					?>
					<td class="tabletoplink"><?=$i_StudentAttendance_Menu_Time_Session_LeaveSchoolTime?></td>
					<td class="tabletoplink"><?=$i_StudentAttendance_Menu_Time_Session_NonSchoolDay?></td>
					<td class="tabletoplink"><?=$i_StudentAttendance_Menu_Time_Session_School_Use?></td>
				</tr>
				<?= $table_content ?>
			</table>
		</td>
	</tr>
<?
if($type==1)
{
	$sql = "SELECT RecordDate, SessionID FROM CARD_STUDENT_TIME_SESSION_DATE WHERE ClassID = 0";
	$result = $lc->returnArray($sql,2);
	if(sizeof($result)>0)
	{
		$info_table = "<tr><td class='tabletext'>";
		for ($i=0; $i<sizeof($result); $i++)
		{
			list ($record_date,$s_id) = $result[$i];
			$info_table .= "<a class='tablelink' onMouseMove=\"moveObject('Specialday_Info',event);\" onmouseover=\"retrieveSpecialdayInfo($s_id);\" onmouseout=\"closeLayer('Specialday_Info');\" href='#'>$record_date</a>&nbsp;&nbsp;&nbsp;";
		}
		$info_table .= "</td></tr>";
	}
	if(sizeof($result)==0)
	{
		$info_table .= "<tr><td class='tabletext' align='center'>$i_no_record_exists_msg</td></tr>";
	}
?>
	<tr>
    	<td>
			<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr class="tabletop"><td class="tabletoplink"><?=$i_StudentAttendance_TimeSessionSettings_AlreadyHaveSpecialDaySession?></td></tr>
				<?=$info_table?>
			</table>
    	</td>
    </tr>
<?
}
?>
    <tr>
    	<td>
		    <table width="96%" border="0" cellpadding="0" cellspacing="0" align="center">
		    	<tr>
                	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
		    </table>
	    </td>
	</tr>
    <tr>
	    <td align="center" colspan="2">
			<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "") ?>
			<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
			<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:window.location = 'index.php';","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
		</td>
	</tr>
</table>
<input type="hidden" name="dateFieldCount" id="dateFieldCount" value="1">
</form>

<?
$linterface->LAYOUT_STOP();
if ($type == 3)
	print $linterface->FOCUS_ON_LOAD("form1.TargetDate");
else
	print $linterface->FOCUS_ON_LOAD("form1.DayValue");
	
intranet_closedb();
?>