<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libapplyleave.php");

### get parameters
$returnMsgKey = standardizeFormGetValue($_GET['returnMsgKey']);

include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$lcardstudentattend2 = new libcardstudentattend2();
$lapplyleave = new libapplyleave();


### check access right
$canAccess = false;
if ($_SESSION["SSV_PRIVILEGE"]["plugin"]['attendancestudent'] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] && $plugin['eClassApp']) {
	$canAccess = true;
}
if (!$canAccess) {
	No_Access_Right_Pop_Up();
}


### show return message
$returnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];


### tab display
// $TAGS_OBJ[] = array($displayLang, $onclickJs, $isSelectedTab);
$TAGS_OBJ[] = array($Lang['StudentAttendance']['ApplyLeaveAry']['ApplyLeave(App)']);
$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageSystemSetting_ApplyLeaveAppSettings";
$MODULE_OBJ = $lcardstudentattend2->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START($returnMsg);


### settings table
$settingAssoAry = $lapplyleave->getSettingsAry();

$AM_time_parts = explode(':',$settingAssoAry['applyLeaveCutOffTimingAM']);
$AM_time_hour = $AM_time_parts[0];
$AM_time_minute = $AM_time_parts[1];

$PM_time_parts = explode(':',$settingAssoAry['applyLeaveCutOffTimingPM']);
$PM_time_hour = $PM_time_parts[0];
$PM_time_minute = $PM_time_parts[1];


$x .= '<table class="form_table_v30">'."\r\n";
	// document required
	if ($settingAssoAry['isRequireDocument'] == 1) {
		$checkedYes = true;
		$checkedNo = false;
		$detailsSettingDivStyle = '';
	}
	else {
		$checkedYes = false;
		$checkedNo = true;
		$detailsSettingDivStyle = 'display:none;';
	}
	$x .= '<tr>'."\r\n";
		$x .= '<td class="field_title">'.$Lang['StudentAttendance']['ApplyLeaveAry']['DocumentRequired'].'</td>'."\r\n";
		$x .= '<td>'."\r\n";
			$x .= '<div>'."\r\n";
				$x .= $linterface->Get_Radio_Button('documentRequiredRadio_yes', 'isRequireDocument', 1, $checkedYes, $Class="", $Lang['General']['Yes'], "clickedDocumentRequired(this.value);");
				$x .= '&nbsp;';
				$x .= $linterface->Get_Radio_Button('documentRequiredRadio_no', 'isRequireDocument', 0, $checkedNo, $Class="", $Lang['General']['No'], "clickedDocumentRequired(this.value);");
			$x .= '</div>'."\r\n";
			$x .= '<div id="documentDetailsSettingDiv" style="'.$detailsSettingDivStyle.'">'."\r\n";
				$x .= '<br />'."\r\n";
				$x .= '<table class="common_table_list_v30">'."\r\n";
					// Submission Deadline
					$selectedDay = ($settingAssoAry['documentSubmitWithinDay'])? $settingAssoAry['documentSubmitWithinDay'] : 1;
					$dayNumSel = $linterface->Get_Number_Selection('documentSubmitWithinDay', 1, 31, $selectedDay, $Onchange='', $noFirst=1);
					$dayDisplay = str_replace('<!--dayNum-->', $dayNumSel, $Lang['StudentAttendance']['ApplyLeaveAry']['NumberOfDayToSubmitDocument']);
					$x .= '<tr>'."\r\n";
						$x .= '<td class="rights_not_select_sub" style="width:30%;">'.$Lang['StudentAttendance']['ApplyLeaveAry']['SubmissionDeadline'].'</td>'."\r\n";
						$x .= '<td>'.$dayDisplay.'</td>'."\r\n";
					$x .= '</tr>'."\r\n";
					
					// Document Remarks
					$x .= '<tr>'."\r\n";
						$x .= '<td class="rights_not_select_sub">'.$Lang['StudentAttendance']['ApplyLeaveAry']['DocumentRemarks'].'</td>'."\r\n";
						$x .= '<td>'.$linterface->GET_TEXTAREA('documentSubmitRemarks', $settingAssoAry['documentSubmitRemarks']).'</td>'."\r\n";
					$x .= '</tr>'."\r\n";
				$x .= '</table>'."\r\n";
			$x .= '</div>'."\r\n";
		$x .= '</td>'."\r\n";
	$x .= '</tr>'."\r\n";
	

	if($sys_custom['eClassApp']['applyLeaveReasonRequired']) {
		if ($settingAssoAry['reasonMustInput'] != '0') {
			$checkedYes = true;
			$checkedNo = false;
		} else {
			$checkedYes = false;
			$checkedNo = true;
		}

		$x .= '<tr>' . "\r\n";
		$x .= '<td class="field_title">' . $Lang['StudentAttendance']['ApplyLeaveAry']['ReasonMustInput'] . '</td>' . "\r\n";
		$x .= '<td>' . "\r\n";
		$x .= $linterface->Get_Radio_Button('ReasonMustInputRadio_yes', 'reasonMustInput', 1, $checkedYes, $Class = "", $Lang['General']['Yes']);
		$x .= '&nbsp;';
		$x .= $linterface->Get_Radio_Button('ReasonMustInputRadio_no', 'reasonMustInput', 0, $checkedNo, $Class = "", $Lang['General']['No']);
		$x .= '</td>' . "\r\n";
		$x .= '</tr>' . "\r\n";
	}

	// send push message to class teacher
	if ($plugin['eClassTeacherApp']) {
		if ($settingAssoAry['sendPushMessageToClassTeacher'] == 1) {
			$checkedYes = true;
			$checkedNo = false;
		}
		else {
			$checkedYes = false;
			$checkedNo = true;
		}
		
		$x .= '<tr>'."\r\n";
			$x .= '<td class="field_title">'.$Lang['StudentAttendance']['ApplyLeaveAry']['SendPushMessageToClassTeacher'].'</td>'."\r\n";
			$x .= '<td>'."\r\n";
				$x .= $linterface->Get_Radio_Button('sendPushMessageToClassTeacherRadio_yes', 'sendPushMessageToClassTeacher', 1, $checkedYes, $Class="", $Lang['General']['Yes']);
					$x .= '&nbsp;';
				$x .= $linterface->Get_Radio_Button('sendPushMessageToClassTeacherRadio_no', 'sendPushMessageToClassTeacher', 0, $checkedNo, $Class="", $Lang['General']['No']);
			$x .= '</td>'."\r\n";
		$x .= '</tr>'."\r\n";
	}

if($sys_custom['eClassApp']['applyLeaveCutOffTimeSetting']) {

    //Cut-off time enable
    if ($settingAssoAry['enableApplyLeaveCutOffTiming'] == 1) {
        $checkedYes = true;
        $checkedNo = false;
        $applyLeaveCutOffTimingDivStyle = '';
    } else {
        $checkedYes = false;
        $checkedNo = true;
        $applyLeaveCutOffTimingDivStyle = 'display:none;';
    }

    $x .= '<tr>' . "\r\n";
    $x .= '<td class="field_title">' . $Lang['StudentAttendance']['ApplyLeaveAry']['enableApplyLeaveCutOffTiming'] . '</td>' . "\r\n";
    $x .= '<td>' . "\r\n";
    $x .= '<div>' . "\r\n";
    $x .= $linterface->Get_Radio_Button('enableApplyLeaveCutOffTiming_yes', 'enableApplyLeaveCutOffTiming', 1, $checkedYes, $Class = "", $Lang['General']['Yes'], "clickedCutOffTiming(this.value);");
    $x .= '&nbsp;';
    $x .= $linterface->Get_Radio_Button('enableApplyLeaveCutOffTiming_no', 'enableApplyLeaveCutOffTiming', 0, $checkedNo, $Class = "", $Lang['General']['No'], "clickedCutOffTiming(this.value);");
    $x .= '</div>' . "\r\n";
    $x .= '<div id="applyLeaveCutOffTimingDiv" style="' . $applyLeaveCutOffTimingDivStyle . '">' . "\r\n";
    $x .= '<br />' . "\r\n";
    $x .= '<table class="common_table_list_v30">' . "\r\n";
    //Cut-off time set
    $x .= '<tr>' . "\r\n";
    $x .= '<td class="rights_not_select_sub" style="width:30%;">' . $Lang['StudentAttendance']['ApplyLeaveAry']['applyLeaveCutOffTiming'] . '</td>' . "\r\n";
    if ($sys_custom['Class']['ClassGroupSettings']) {
        //$x .= '<td>AM:<input type="text" name="applyLeaveCutOffTimingAM" value="'.$settingAssoAry['applyLeaveCutOffTimingAM'].'"/>'."\r\n";
        //$x .= 'PM:<input type="text" name="applyLeaveCutOffTimingPM" value="'.$settingAssoAry['applyLeaveCutOffTimingPM'].'"/>'.'</td>'."\r\n";
        $x .= '<td>'.$Lang['StudentAttendance']['ApplyLeaveAry']['AMWDClasses'].' : ' . $linterface->Get_Time_Selection_Box('applyLeaveCutOffTimingAM_hour', 'hour', $AM_time_hour, $__others_tab = '', $__interval = 1, 'applyLeaveCutOffTimingAM_hour') . ':' . $linterface->Get_Time_Selection_Box('applyLeaveCutOffTimingAM_minute', 'min', $AM_time_minute, $__others_tab = '', $__interval = 1, 'applyLeaveCutOffTimingAM_minute') . "\r\n";
        $x .= '&nbsp;&nbsp;&nbsp;&nbsp;'.$Lang['StudentAttendance']['ApplyLeaveAry']['PMClasses'].' : ' . $linterface->Get_Time_Selection_Box('applyLeaveCutOffTimingPM_hour', 'hour', $PM_time_hour, $__others_tab = '', $__interval = 1, 'applyLeaveCutOffTimingPM_hour') . ':' . $linterface->Get_Time_Selection_Box('applyLeaveCutOffTimingPM_minute', 'min', $PM_time_minute, $__others_tab = '', $__interval = 1, 'applyLeaveCutOffTimingPM_minute') . '</td>' . "\r\n";
    } else {
        //$x .= '<td><input type="text" name="applyLeaveCutOffTimingAM" value="'.$settingAssoAry['applyLeaveCutOffTimingAM'].'"/>'.'</td>'."\r\n";
        $x .= '<td>' . $linterface->Get_Time_Selection_Box('applyLeaveCutOffTimingAM_hour', 'hour', $AM_time_hour, $__others_tab = '', $__interval = 1, 'applyLeaveCutOffTimingAM_hour') . ':' . $linterface->Get_Time_Selection_Box('applyLeaveCutOffTimingAM_minute', 'min', $AM_time_minute, $__others_tab = '', $__interval = 1, 'applyLeaveCutOffTimingAM_minute') . '</td>' . "\r\n";
    }
    $x .= '</tr>' . "\r\n";
    $x .= '</table>' . "\r\n";
    $x .= '</div>' . "\r\n";
    $x .= '</td>' . "\r\n";
    $x .= '</tr>' . "\r\n";

    //days Before Apply Leave
    $x .= '<tr>' . "\r\n";
    $x .= '<td class="field_title">' . $Lang['StudentAttendance']['ApplyLeaveAry']['daysBeforeApplyLeave'] . '</td>' . "\r\n";
    $x .= '<td><input type="number" name="daysBeforeApplyLeave" value="' . $settingAssoAry['daysBeforeApplyLeave'] . '"  min="0"/>' . '</td>' . "\r\n";
    $x .= '</tr>' . "\r\n";
}

	//max Apply Leave Day
	$x .= '<tr>'."\r\n";
		$x .= '<td class="field_title">'.$Lang['StudentAttendance']['ApplyLeaveAry']['maxApplyLeaveDay'].'</td>'."\r\n";
		$x .= '<td><input type="number" name="maxApplyLeaveDay" value="'.$settingAssoAry['maxApplyLeaveDay'].'" min="0"/>'.'</td>'."\r\n";							
	$x .= '</tr>'."\r\n";
	
	
    //homework pass 
	if ($settingAssoAry['enableApplyLeaveHomeworkPassSetting'] == 1) {
		$checkedYes = true;
		$checkedNo = false;
		$settingInsertDivStyle = '';
	}
	else {
		$checkedYes = false;
		$checkedNo = true;
		$settingInsertDivStyle = 'display:none;';
	}
	$RowID = 0;
	
	$sql = "Select * From APPLY_LEAVE_HOMEWORK_PASS_METHOD_SETTING where IsDelete = 0";
	$methodAry = $lapplyleave->returnArray($sql);
	$x .= '<tr>'."\r\n";
		$x .= '<td class="field_title">'.$Lang['StudentAttendance']['ApplyLeaveAry']['homeworkDeliveryMethodSet'].'</td>'."\r\n";
		$x .= '<td>'."\r\n";
			$x .= '<div>'."\r\n";
			$x .= $linterface->Get_Radio_Button('enableApplyLeaveHomeworkPassSetting_yes', 'enableApplyLeaveHomeworkPassSetting', 1, $checkedYes, $Class="", $Lang['General']['Yes'], "clickedEnableApplyLeaveHomeworkPassSetting(this.value);");
				$x .= '&nbsp;';
			$x .= $linterface->Get_Radio_Button('enableApplyLeaveHomeworkPassSetting_no', 'enableApplyLeaveHomeworkPassSetting', 0, $checkedNo, $Class="", $Lang['General']['No'], "clickedEnableApplyLeaveHomeworkPassSetting(this.value);");
			$x .= '</div>'."\r\n";
			$x .= '<div id="settingInsertDiv" style="'.$settingInsertDivStyle.'">'."\r\n";
				$x .= '<br />'."\r\n";
				//setting insert
				$x .= '<table id="homeworkPassTable">'."\r\n";
				  $x .= '<tbody>'."\r\n";
				  for($i=0;$i<sizeof($methodAry);$i++)
				  {
					$x.="<tr id=\"Row".$RowID."\">";
					$x.="<td class=\"tablerow1\">".$Lang['StudentAttendance']['ApplyLeaveAry']['homeworkDeliveryMethodChi'].": <input type='text' name='Method_".$RowID."' id='Method_".$RowID."' width='100%' maxlength='255' value='".$methodAry[$i]['Method']."' /><input type='hidden' name='MethodID_".$RowID."' id='MethodID_".$RowID."' value='".$methodAry[$i]['MethodID']."' />&nbsp;</td>";
					$x.="<td class=\"tablerow1\">".$Lang['StudentAttendance']['ApplyLeaveAry']['homeworkDeliveryMethodEng'].": <input type='text' name='MethodEng_".$RowID."' id='MethodEng_".$RowID."' width='100%' maxlength='255' value='".$methodAry[$i]['MethodEng']."' />&nbsp;</td>";
					$x.="<td class=\"tablerow1\" align=\"right\"><span class=\"table_row_tool\" style=\"float:right;\"><a class=\"delete\" href=\"javascript:deleteRow('Row".$RowID."')\" alt=\"".$Lang['SmartCard']['StudentAttendance']['DeleteRow']."\" title=\"".$Lang['SmartCard']['StudentAttendance']['DeleteRow']."\" ></a></span></td>";
					$x.="</tr>\n";
					$RowID += 1;
			      }	
				  $x .= '</tbody>'."\r\n";							
				$x .= '</table>'."\r\n";	
				$x .= '<span class="table_row_tool" style="float:left;"><a class="add" href="javascript:addRow()"></a></span>'."\r\n";						
			$x .= '</div>'."\r\n";
		$x .= '</td>'."\r\n";
	$x .= '</tr>'."\r\n";
$x .= '</table>'."\r\n";
$htmlAry['contentTable'] = $x;


### buttons
$htmlAry['submitButton'] = $linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], 'button', 'goSubmit();');
$htmlAry['cancelButton'] = $linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], 'button', 'goCancel()');

?>
<script type="text/javascript">
$(document).ready( function() {
	
});

function clickedDocumentRequired(isRequired) {
	if (isRequired == 1) {
		$('div#documentDetailsSettingDiv').show();
	}
	else {
		$('div#documentDetailsSettingDiv').hide();
	}
}

function clickedCutOffTiming(isEnabled) {
	if (isEnabled == 1) {
		$('div#applyLeaveCutOffTimingDiv').show();
	}
	else {
		$('div#applyLeaveCutOffTimingDiv').hide();
	}
}

function goSubmit() {    
     var div = document.getElementById("div1");
     var input = document.createElement("input");
     input.type = "hidden";
     input.name = "RowID";
     input.value = RowID;
     div.appendChild(input);
     
	$('form#form1').attr('action', 'edit_update.php').submit();
}

function goCancel() {
	$('form#form1').attr('action', 'list.php').submit();
}

function clickedEnableApplyLeaveHomeworkPassSetting(isEnabled) {
	if (isEnabled == 1) {
		$('div#settingInsertDiv').show();
	}
	else {
		$('div#settingInsertDiv').hide();
	}
}

var RowID = <?=$RowID?>;

function deleteRow(rowID)
{
	var tableBody = document.getElementById("homeworkPassTable").getElementsByTagName("tbody")[0];
	tableBody.removeChild(document.getElementById(rowID));
}

function addRow()
{
	
    var tableBody = document.getElementById("homeworkPassTable").getElementsByTagName("tbody")[0];
    var rowObj = document.createElement("tr");
    rowObj.setAttribute("id","Row"+RowID);

    var txtMethod="";
    var txtMethodEng="";
    
    txtMethod += "<?=$Lang['StudentAttendance']['ApplyLeaveAry']['homeworkDeliveryMethodChi']?>: <input type='text' name='Method_"+RowID+"' id='Method_"+RowID+"' width='100%' maxlength='255' value='' /><input type='hidden' name='MethodID_"+RowID+"' id='MethodID_"+RowID+"' value='-1' />&nbsp;";
    txtMethodEng += "<?=$Lang['StudentAttendance']['ApplyLeaveAry']['homeworkDeliveryMethodEng']?>: <input type='text' name='MethodEng_"+RowID+"' id='MethodEng_"+RowID+"' width='100%' maxlength='255' value='' />&nbsp;";

    var td1 = document.createElement("td");
    td1.setAttribute("class","tablerow1");
    td1.innerHTML = txtMethod;
    
    var td2 = document.createElement("td");
    td2.setAttribute("class","tablerow1");
    td2.innerHTML = txtMethodEng;
    
    var delete_js = '';
    
    delete_js = "javascript:deleteRow('Row"+RowID+"');";
    
    var deleteCell = '<span class="table_row_tool" style="float:right;"><a class="delete" href="'+delete_js+'" alt="<?=$Lang['SmartCard']['StudentAttendance']['DeleteRow']?>" title="<?=$Lang['SmartCard']['StudentAttendance']['DeleteRow']?>"></a></span>';
    
    var td3 = document.createElement("td");
    td3.innerHTML = deleteCell;
   	td3.setAttribute("class","tablerow1");
    td3.setAttribute("align","right");
    
    rowObj.appendChild(td1);
    rowObj.appendChild(td2);
    rowObj.appendChild(td3);
    
    tableBody.appendChild(rowObj);
    
    RowID += 1;
}
</script>
<form name="form1" id="form1" method="POST">
	<div class="table_board">
		<?=$htmlAry['contentTable']?>
	</div>
	<br />
	<div class="edit_bottom">
		<p class="spacer"></p>
		<?=$htmlAry['submitButton']?>
		<?=$htmlAry['cancelButton']?>
		<p class="spacer"></p>
	</div>
	<div id="div1">		<p class="spacer"></p>
	
	</div>
</form>
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>