<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libapplyleave.php");

intranet_auth();
intranet_opendb();

$lapplyleave = new libapplyleave();

### check access right
$canAccess = false;
if ($_SESSION["SSV_PRIVILEGE"]["plugin"]['attendancestudent'] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] && $plugin['eClassApp']) {
	$canAccess = true;
}
if (!$canAccess) {
	No_Access_Right_Pop_Up();
}


$isRequireDocument = $_POST['isRequireDocument'];
$documentSubmitWithinDay = $_POST['documentSubmitWithinDay'];
$documentSubmitRemarks = trim(stripslashes($_POST['documentSubmitRemarks']));
$sendPushMessageToClassTeacher = ($_POST['sendPushMessageToClassTeacher'])? 1 : 0;
$enableApplyLeaveCutOffTiming = ($_POST['enableApplyLeaveCutOffTiming'])? 1 : 0;
//$applyLeaveCutOffTimingAM = trim(stripslashes($_POST['applyLeaveCutOffTimingAM']));
$applyLeaveCutOffTimingAM = sprintf("%02d", $_REQUEST['applyLeaveCutOffTimingAM_hour']).':'.sprintf("%02d", $_REQUEST['applyLeaveCutOffTimingAM_minute']);
//$applyLeaveCutOffTimingPM = trim(stripslashes($_POST['applyLeaveCutOffTimingPM']));
$applyLeaveCutOffTimingPM = sprintf("%02d", $_REQUEST['applyLeaveCutOffTimingPM_hour']).':'.sprintf("%02d", $_REQUEST['applyLeaveCutOffTimingPM_minute']);
$daysBeforeApplyLeave = trim(stripslashes($_POST['daysBeforeApplyLeave']));
$maxApplyLeaveDay = trim(stripslashes($_POST['maxApplyLeaveDay']));
$reasonMustInput = ($_POST['reasonMustInput'])? 1 : 0;

$successAry = array();
$lapplyleave->Start_Trans();
$successAry['isRequireDocument'] = $lapplyleave->updateSetting('isRequireDocument', $isRequireDocument);
$successAry['documentSubmitWithinDay'] = $lapplyleave->updateSetting('documentSubmitWithinDay', $documentSubmitWithinDay);
$successAry['documentSubmitRemarks'] = $lapplyleave->updateSetting('documentSubmitRemarks', $documentSubmitRemarks);
if($sys_custom['eClassApp']['applyLeaveReasonRequired']) {
	$successAry['ReasonMustInput'] = $lapplyleave->updateSetting('reasonMustInput', $reasonMustInput);
}

if ($plugin['eClassTeacherApp']) {
	$successAry['sendPushMessageToClassTeacher'] = $lapplyleave->updateSetting('sendPushMessageToClassTeacher', $sendPushMessageToClassTeacher);
}

if($sys_custom['eClassApp']['applyLeaveCutOffTimeSetting']) {
    $successAry['enableApplyLeaveCutOffTiming'] = $lapplyleave->updateSetting('enableApplyLeaveCutOffTiming', $enableApplyLeaveCutOffTiming);
    if ($sys_custom['Class']['ClassGroupSettings']) {
        $successAry['applyLeaveCutOffTimingAM'] = $lapplyleave->updateSetting('applyLeaveCutOffTimingAM', $applyLeaveCutOffTimingAM);
        $successAry['applyLeaveCutOffTimingPM'] = $lapplyleave->updateSetting('applyLeaveCutOffTimingPM', $applyLeaveCutOffTimingPM);
    } else {
        $successAry['applyLeaveCutOffTimingAM'] = $lapplyleave->updateSetting('applyLeaveCutOffTimingAM', $applyLeaveCutOffTimingAM);
        $successAry['applyLeaveCutOffTimingPM'] = $lapplyleave->updateSetting('applyLeaveCutOffTimingPM', "");
    }
    $successAry['daysBeforeApplyLeave'] = $lapplyleave->updateSetting('daysBeforeApplyLeave', $daysBeforeApplyLeave);
}

$successAry['maxApplyLeaveDay'] = $lapplyleave->updateSetting('maxApplyLeaveDay', $maxApplyLeaveDay);

//homework pass 
$successAry['enableApplyLeaveHomeworkPassSetting'] = $lapplyleave->updateSetting('enableApplyLeaveHomeworkPassSetting', $enableApplyLeaveHomeworkPassSetting);

if($enableApplyLeaveHomeworkPassSetting==1){
	$sql = "Update APPLY_LEAVE_HOMEWORK_PASS_METHOD_SETTING SET IsDelete = 1";
	$successAry['SetMethodDelete'] = $lapplyleave->db_db_query($sql);
		
	for($i=0;$i<=$RowID;$i++)
	{
		if($_POST[MethodID_.$i]==-1){
			$sql = "INSERT INTO APPLY_LEAVE_HOMEWORK_PASS_METHOD_SETTING (Method, MethodEng, IsDelete, CreateDate, LastModified) ";
			$sql .= "VALUES('".$_POST[Method_.$i]."','".$_POST[MethodEng_.$i]."',0,NOW(),NOW()) ";
			$successAry['InsertMethod'] = $lapplyleave->db_db_query($sql);
		}else if($_POST[MethodID_.$i]>0){
			$sql = "Update APPLY_LEAVE_HOMEWORK_PASS_METHOD_SETTING SET IsDelete = 0, Method = '".$_POST[Method_.$i]."', MethodEng = '".$_POST[MethodEng_.$i]."', LastModified = NOW() WHERE MethodID = ".$_POST[MethodID_.$i]."";
			$successAry['UpdateMethod'] = $lapplyleave->db_db_query($sql);
		}
	
	}
}

if (in_array(false, $successAry)) {
	$lapplyleave->RollBack_Trans();
	$returnMsgKey = 'UpdateUnsuccess';
}
else {
	$lapplyleave->Commit_Trans();
	$returnMsgKey = 'UpdateSuccess';
}
intranet_closedb();


header('Location: list.php?returnMsgKey='.$returnMsgKey);
?>