<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libapplyleave.php");

### get parameters
$returnMsgKey = standardizeFormGetValue($_GET['returnMsgKey']);


### cookies handling to preserve selection
//$arrCookies[] = array("{$moduleName}_{$leftMenu}_{$pageName}_{$variableName}", "$variableName");
// e.g. $arrCookies[] = array("eEnrolment_Mgmt_clubList_field", "field");

if(isset($clearCoo) && $clearCoo == 1) {
	clearCookies($arrCookies);
}
else {
	updateGetCookies($arrCookies);
}

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();


$linterface = new interface_html();
$lcardstudentattend2 = new libcardstudentattend2();
$lapplyleave = new libapplyleave();


### check access right
$canAccess = false;
if ($_SESSION["SSV_PRIVILEGE"]["plugin"]['attendancestudent'] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] && $plugin['eClassApp']) {
	$canAccess = true;
}
if (!$canAccess) {
	No_Access_Right_Pop_Up();
}


### show return message
$returnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];


### tab display
// $TAGS_OBJ[] = array($displayLang, $onclickJs, $isSelectedTab);
$TAGS_OBJ[] = array($Lang['StudentAttendance']['ApplyLeaveAry']['ApplyLeave(App)']);
$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageSystemSetting_ApplyLeaveAppSettings";
$MODULE_OBJ = $lcardstudentattend2->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START($returnMsg);


### settings table
$settingAssoAry = $lapplyleave->getSettingsAry();
$x .= '<table class="form_table_v30">'."\r\n";
	// document required
	$display = ($settingAssoAry['isRequireDocument'] == 1)? $Lang['General']['Yes'] : $Lang['General']['No'];
	$x .= '<tr>'."\r\n";
		$x .= '<td class="field_title">'.$Lang['StudentAttendance']['ApplyLeaveAry']['DocumentRequired'].'</td>'."\r\n";
		$x .= '<td>'."\r\n";
			$x .= '<div>'."\r\n";
				$x .= $display."\r\n";
			$x .= '</div>'."\r\n";
			if ($settingAssoAry['isRequireDocument']) {
				$x .= '<br />'."\r\n";
				$x .= '<div>'."\r\n";
					$x .= '<table class="common_table_list_v30">'."\r\n";
						// Submission Deadline
						$dayDisplay = str_replace('<!--dayNum-->', $settingAssoAry['documentSubmitWithinDay'], $Lang['StudentAttendance']['ApplyLeaveAry']['NumberOfDayToSubmitDocument']);
						$x .= '<tr>'."\r\n";
							$x .= '<td class="rights_not_select_sub" style="width:30%;">'.$Lang['StudentAttendance']['ApplyLeaveAry']['SubmissionDeadline'].'</td>'."\r\n";
							$x .= '<td>'.$dayDisplay.'</td>'."\r\n";
						$x .= '</tr>'."\r\n";
						
						// Document Remarks
						$x .= '<tr>'."\r\n";
							$x .= '<td class="rights_not_select_sub">'.$Lang['StudentAttendance']['ApplyLeaveAry']['DocumentRemarks'].'</td>'."\r\n";
							$x .= '<td>'.nl2br($settingAssoAry['documentSubmitRemarks']).'</td>'."\r\n";
						$x .= '</tr>'."\r\n";
					$x .= '</table>'."\r\n";
				$x .= '</div>'."\r\n";
			}
		$x .= '</td>'."\r\n";
	$x .= '</tr>'."\r\n";

	if($sys_custom['eClassApp']['applyLeaveReasonRequired']) {
		$x .= '<tr>' . "\r\n";
		$x .= '<td class="field_title">' . $Lang['StudentAttendance']['ApplyLeaveAry']['ReasonMustInput'] . '</td>' . "\r\n";
		$x .= '<td>' . ($settingAssoAry['reasonMustInput'] != '0' ? $Lang['General']['Yes'] : $Lang['General']['No']) . '</td>' . "\r\n";
		$x .= '</tr>' . "\r\n";
	}
	
	// send push message to class teacher
	if ($plugin['eClassTeacherApp']) {
		$x .= '<tr>'."\r\n";
			$x .= '<td class="field_title">'.$Lang['StudentAttendance']['ApplyLeaveAry']['SendPushMessageToClassTeacher'].'</td>'."\r\n";
			$x .= '<td>'.($settingAssoAry['sendPushMessageToClassTeacher']? $Lang['General']['Yes'] : $Lang['General']['No']).'</td>'."\r\n";
		$x .= '</tr>'."\r\n";
	}

if($sys_custom['eClassApp']['applyLeaveCutOffTimeSetting']) {

    //Cut-off time enable
    $x .= '<tr>' . "\r\n";
    $x .= '<td class="field_title">' . $Lang['StudentAttendance']['ApplyLeaveAry']['enableApplyLeaveCutOffTiming'] . '</td>' . "\r\n";
    $x .= '<td>' . "\r\n";
    $x .= '<div>' . "\r\n";
    $x .= ($settingAssoAry['enableApplyLeaveCutOffTiming'] ? $Lang['General']['Yes'] : $Lang['General']['No']) . "\r\n";
    $x .= '</div>' . "\r\n";
    if ($settingAssoAry['enableApplyLeaveCutOffTiming']) {
        $x .= '<br />' . "\r\n";
        $x .= '<div>' . "\r\n";
        $x .= '<table class="common_table_list_v30">' . "\r\n";
        //Cut-off time set
        $x .= '<tr>' . "\r\n";
        $x .= '<td class="rights_not_select_sub" style="width:30%;">' . $Lang['StudentAttendance']['ApplyLeaveAry']['applyLeaveCutOffTiming'] . '</td>' . "\r\n";
        if ($sys_custom['Class']['ClassGroupSettings']) {
            $x .= '<td>'.$Lang['StudentAttendance']['ApplyLeaveAry']['AMWDClasses'].' : ' . nl2br($settingAssoAry['applyLeaveCutOffTimingAM']) . '&nbsp;&nbsp;&nbsp;&nbsp;'.$Lang['StudentAttendance']['ApplyLeaveAry']['PMClasses'].' : ' . nl2br($settingAssoAry['applyLeaveCutOffTimingPM']) . '</td>' . "\r\n";

        } else {
            $x .= '<td>' . nl2br($settingAssoAry['applyLeaveCutOffTimingAM']) . '</td>' . "\r\n";
        }
        $x .= '</tr>' . "\r\n";
        $x .= '</table>' . "\r\n";
        $x .= '</div>' . "\r\n";
    }
    $x .= '</td>' . "\r\n";
    $x .= '</tr>' . "\r\n";

    //days Before Apply Leave
    $x .= '<tr>' . "\r\n";
    $x .= '<td class="field_title">' . $Lang['StudentAttendance']['ApplyLeaveAry']['daysBeforeApplyLeave'] . '</td>' . "\r\n";
    $x .= '<td>' . nl2br($settingAssoAry['daysBeforeApplyLeave']) . '</td>' . "\r\n";
    $x .= '</tr>' . "\r\n";
}
	//max Apply Leave Day
	$x .= '<tr>'."\r\n";
		$x .= '<td class="field_title">'.$Lang['StudentAttendance']['ApplyLeaveAry']['maxApplyLeaveDay'].'</td>'."\r\n";
		$x .= '<td>'.nl2br($settingAssoAry['maxApplyLeaveDay']).'</td>'."\r\n";
	$x .= '</tr>'."\r\n";
		
    //homework pass 
    $sql = "Select * From APPLY_LEAVE_HOMEWORK_PASS_METHOD_SETTING where IsDelete = 0";
	$methodAry = $lapplyleave->returnArray($sql);
	$x .= '<tr>'."\r\n";
		$x .= '<td class="field_title">'.$Lang['StudentAttendance']['ApplyLeaveAry']['homeworkDeliveryMethodSet'].'</td>'."\r\n";
		$x .= '<td>'."\r\n";
			$x .= '<div>'."\r\n";
				$x .= ($settingAssoAry['enableApplyLeaveHomeworkPassSetting']? $Lang['General']['Yes'] : $Lang['General']['No'])."\r\n";
			$x .= '</div>'."\r\n";
			if ($settingAssoAry['enableApplyLeaveHomeworkPassSetting']==1) {
				$x .= '<br />'."\r\n";
				$x .= '<div>'."\r\n";
					$x .= '<table>'."\r\n";
	                  for($i=0;$i<sizeof($methodAry);$i++)
					  {
						$x.="<tr>";
						$x.="<td>".$Lang['StudentAttendance']['ApplyLeaveAry']['homeworkDeliveryMethodChi'].": ".$methodAry[$i]['Method']." &nbsp;&nbsp;&nbsp;&nbsp;</td>";
						$x.="<td>".$Lang['StudentAttendance']['ApplyLeaveAry']['homeworkDeliveryMethodEng'].": ".$methodAry[$i]['MethodEng']." &nbsp;</td>";
						$x.="</tr>\n";
				      }	
					$x .= '</table>'."\r\n";
				$x .= '</div>'."\r\n";
			}
		$x .= '</td>'."\r\n";
	$x .= '</tr>'."\r\n";	
	
$x .= '</table>'."\r\n";
$htmlAry['contentTable'] = $x;


### edit button
$htmlAry['editButton'] = $linterface->GET_ACTION_BTN($Lang['Btn']['Edit'], 'button', 'goEdit();');
?>
<script type="text/javascript">
$(document).ready( function() {
	
});

function goEdit() {
	window.location = "edit.php";
}
</script>
<form name="form1" id="form1" method="POST" action="page.php?canAccess=1">
	<div class="table_board">
		<?=$htmlAry['contentTable']?>
	</div>
	<br />

	<div class="edit_bottom">
		<p class="spacer"></p>
		<?=$htmlAry['editButton']?>
		<p class="spacer"></p>
	</div>
</form>
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>