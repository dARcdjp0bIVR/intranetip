<?php
// Editing by 
/*
 * 2019-07-05  (Ray):   Added send to admin, send to teacher
 * 2018-05-07 (Carlos): Created.
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libcrontab.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	intranet_closedb();
	$_SESSION['STUDENT_ATTENDANCE_CONTINUOUS_ABSENCE_ALERT_RESULT'] = $Lang['StudentAttendance']['SettingApplyFail'];
	header("Location:index.php");
	exit();
}

$GeneralSetting = new libgeneralsettings();
$SettingList['ContinuousAbsenceAlertAbsentDays'] = $_REQUEST['AbsentDays'];
$SettingList['ContinuousAbsenceAlertJobStatus'] = $_REQUEST['ContinuousAbsenceAlertJobStatus'];
$SettingList['ContinuousAbsenceAlertJobExecutionTime'] = sprintf("%02d", $_REQUEST['ContinuousAbsenceAlertJobExecutionTime_hour']).':'.sprintf("%02d", $_REQUEST['ContinuousAbsenceAlertJobExecutionTime_minute']);

$SettingList['ContinuousAbsenceAlertJobSendToAdmin'] = $_REQUEST['ContinuousAbsenceAlertJobSendToAdmin'];
$SettingList['ContinuousAbsenceAlertJobSendToAdminAll'] = $_REQUEST['ContinuousAbsenceAlertJobSendToAdminAll'];
$SettingList['ContinuousAbsenceAlertJobSendToAdminIds'] = is_array($adminIDSelected) ?implode(',', $adminIDSelected) : '';

$SettingList['ContinuousAbsenceAlertJobSendToTeacher'] = $_REQUEST['ContinuousAbsenceAlertJobSendToTeacher'];
$SettingList['ContinuousAbsenceAlertJobSendToTeacherAll'] = $_REQUEST['ContinuousAbsenceAlertJobSendToTeacherAll'];
$SettingList['ContinuousAbsenceAlertJobSendToTeacherIds'] =  is_array($teacherIDSelected) ?implode(',', $teacherIDSelected) : '';

$SettingList['ContinuousAbsenceAlertJobDateRangeAcademicYear'] = $_REQUEST['ContinuousAbsenceAlertJobDateRangeAcademicYear'];
$SettingList['ContinuousAbsenceAlertJobDateRangeStartDate'] = $_REQUEST['DateStart'];
$SettingList['ContinuousAbsenceAlertJobDateRangeEndDate'] = $_REQUEST['DateEnd'];

$GeneralSetting->Start_Trans();
if ($GeneralSetting->Save_General_Setting('StudentAttendance',$SettingList)) {
	$GeneralSetting->Commit_Trans();
	
	$crontab = new libcrontab();
	$site = "http".(isSecureHttps()?"s":"")."://".$_SERVER['SERVER_NAME'].($_SERVER["SERVER_PORT"]!= 80?":".$_SERVER["SERVER_PORT"]:"");
	$script = $site."/schedule_task/student_attendance_continuous_absence_alert.php";
	$crontab->removeJob($script);
	
	if($_REQUEST['ContinuousAbsenceAlertJobStatus'] == 1){
		$crontab->setJob($_REQUEST['ContinuousAbsenceAlertJobExecutionTime_minute'], $_REQUEST['ContinuousAbsenceAlertJobExecutionTime_hour'], '*', '*', '*', $script);
	}
	
	$_SESSION['STUDENT_ATTENDANCE_CONTINUOUS_ABSENCE_ALERT_RESULT'] = $Lang['StudentAttendance']['SettingApplySuccess'];
}else {
	$GeneralSetting->RollBack_Trans();
	$_SESSION['STUDENT_ATTENDANCE_CONTINUOUS_ABSENCE_ALERT_RESULT'] = $Lang['StudentAttendance']['SettingApplyFail'];
}

intranet_closedb();
header("Location:index.php");
?>