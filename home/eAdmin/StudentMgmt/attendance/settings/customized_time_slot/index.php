<?php
// Editing by 
/*
 * 2016-12-07 Carlos: Created
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent'] || !$sys_custom['StudentAttendance']['CustomizedTimeSlotSettings']) {
	intranet_closedb();
	header ("Location: /");
	exit();
}

$lcardstudentattend2 = new libcardstudentattend2();
$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageSystemSetting_CustomizedTimeSlotSettings";
$linterface = new interface_html();


$field = trim(urldecode(stripslashes($_REQUEST['field'])));
$order = trim(urldecode(stripslashes($_REQUEST['order'])));
$pageNo = trim(urldecode(stripslashes($_REQUEST['pageNo'])));
$numPerPage = trim(urldecode(stripslashes($_REQUEST['numPerPage'])));

$arrCookies[] = array("ck_student_attendance_customizedtimeslot_page_size", "numPerPage");
$arrCookies[] = array("ck_student_attendance_customizedtimeslot_page_number", "pageNo");
$arrCookies[] = array("ck_student_attendance_customizedtimeslot_page_order", "order");
$arrCookies[] = array("ck_student_attendance_customizedtimeslot_page_field", "field");	
updateGetCookies($arrCookies);

if(!isset($field) || $field == '' || !in_array($field,array(0,1,2,3,4,5))) $field = 0; 
if(!isset($order) || $order == '' || !in_array($order,array(0,1))) $order = 1;
if(!isset($pageNo) || $pageNo == '') $pageNo = 1;

$pageSizeChangeEnabled = true;
if (isset($ck_student_attendance_customizedtimeslot_page_size) && $ck_student_attendance_customizedtimeslot_page_size != "") $page_size = $ck_student_attendance_customizedtimeslot_page_size;

if(!in_array($RecordStatus,array('','1','0'))) $RecordStatus = '';

$status_ary = array();
//$status_ary[] = array('',$Lang['General']['All']); 
$status_ary[] = array('1',$Lang['General']['Enabled']);
$status_ary[] = array('0',$Lang['General']['Disabled']); 
$status_selection = $linterface->GET_SELECTION_BOX($status_ary, ' id="RecordStatus" name="RecordStatus" onchange="document.form1.submit();" ', $Lang['General']['All'], $RecordStatus, true);


$li = new libdbtable2007($field,$order,$pageNo);

$modifier_name_field = getNameFieldByLang2("u.");

$sql = "SELECT 
			s.StartTime,
			s.EndTime,
			CASE s.Status 
		 	WHEN '".CARD_STATUS_PRESENT."' THEN '".$Lang['StudentAttendance']['Present']."' 
			WHEN '".CARD_STATUS_ABSENT."' THEN '".$Lang['StudentAttendance']['Absent']."' 
			WHEN '".CARD_STATUS_LATE."' THEN '".$Lang['StudentAttendance']['Late']."' 
			END as Status,
			IF(s.RecordStatus='1','".$Lang['General']['Enabled']."','".$Lang['General']['Disabled']."') as RecordStatus,
			s.DateModified,
			$modifier_name_field as ModifiedBy,
			CONCAT('<input type=\"checkbox\" name=\"RecordID[]\" value=\"',s.RecordID,'\">') as CheckBox 
		FROM CARD_STUDENT_CUSTOMIZED_TIME_SLOT_SETTINGS as s 
		LEFT JOIN INTRANET_USER as u ON s.ModifiedBy=u.UserID 
		WHERE 1 ";
if($RecordStatus != ''){
	$sql .= " AND s.RecordStatus='".$RecordStatus."' ";
}

$li->sql = $sql;
$li->IsColOff = "IP25_table";

$li->field_array = array("s.StartTime","s.EndTime","Status","RecordStatus","s.DateModified","ModifiedBy");
$li->column_array = array(22,22,22,22,22,22);
$li->wrap_array = array(0,0,0,0,0,0);

$pos = 0;
$li->column_list .= "<th width=\"1\" class=\"num_check\">#</th>\n";
$li->column_list .= "<th>".$li->column($pos++, $Lang['StudentAttendance']['StartTime'])."</th>\n";
$li->column_list .= "<th>".$li->column($pos++, $Lang['StudentAttendance']['EndTime'])."</th>\n";
$li->column_list .= "<th>".$li->column($pos++, $Lang['StudentAttendance']['AttendanceStatus'])."</th>\n";
$li->column_list .= "<th>".$li->column($pos++, $Lang['General']['Status'])."</th>\n";
$li->column_list .= "<th>".$li->column($pos++, $Lang['General']['LastUpdatedTime'])."</th>\n";
$li->column_list .= "<th>".$li->column($pos++, $Lang['General']['LastUpdatedBy'])."</th>\n";
$li->column_list .= "<th width=\"1\">".$li->check("RecordID[]")."</th>\n";
$li->no_col = $pos+2;


$tool_buttons = array();
$tool_buttons[] = array('approve',"javascript:checkEditMultiple2(document.form1,'RecordID[]','enableRecord(1);')", $Lang['Btn']['Enable']);
$tool_buttons[] = array('reject',"javascript:checkEditMultiple2(document.form1,'RecordID[]','enableRecord(0);')", $Lang['Btn']['Disable']);
$tool_buttons[] = array('edit',"javascript:checkEdit2(document.form1,'RecordID[]','editRecord();')");
$tool_buttons[] = array('delete',"javascript:checkRemove2(document.form1,'RecordID[]','deleteRecord();')");


$TAGS_OBJ[] = array($Lang['StudentAttendance']['CustomizedTimeSlotSettings'], "", 0);
$MODULE_OBJ = $lcardstudentattend2->GET_MODULE_OBJ_ARR();

if(isset($_SESSION['STUDENT_ATTENDANCE_CUSTOMIZED_TIME_SLOT_SETTINGS_RESULT']))
{
	$Msg = $_SESSION['STUDENT_ATTENDANCE_CUSTOMIZED_TIME_SLOT_SETTINGS_RESULT'];
	unset($_SESSION['STUDENT_ATTENDANCE_CUSTOMIZED_TIME_SLOT_SETTINGS_RESULT']);
}
$linterface->LAYOUT_START($Msg);
?>
<?=$linterface->Include_JS_CSS()?>
<br />
<form name="form1" id="form1" method="post" action="">
<div class="table_board">
	<div class="content_top_tool">
		<div class="Conntent_tool">
			<?=$linterface->Get_Thickbox_Link(480, 750,  'new', $Lang['Btn']['New'], 'showModalForm(0)', $InlineID="FakeLayer", $Lang['Btn']['New']);?>
		</div>
		<br style="clear:both;">
	</div>
	<br style="clear:both;">
	<div class="table_filter">
	<?=$Lang['General']['Status'].': '.$status_selection?>
	</div>
	<br style="clear:both">	
	<?=$linterface->Get_DBTable_Action_Button_IP25($tool_buttons)?>
	<br style="clear:both">			
	<?=$li->display();?>
	<br style="clear:both;">
	<div class="tabletextremark"><?=$Lang['StudentAttendance']['CustomizedTimeSlotRemark']?></div>
	<input type="hidden" id="pageNo" name="pageNo" value="<?=$li->pageNo?>">
	<input type="hidden" id="order" name="order" value="<?=$li->order?>">
	<input type="hidden" id="field" name="field" value="<?=$li->field?>">
	<input type="hidden" id="page_size_change" name="page_size_change" value="">
	<input type="hidden" id="numPerPage" name="numPerPage" value="<?=$li->page_size?>">
</div>
</form>
<script type="text/javascript" language="javascript">
function showModalForm(id)
{
	var is_edit = id? true: false;
	
	tb_show(is_edit?'<?=$Lang['Btn']['Edit']?>':'<?=$Lang['Btn']['New']?>',"#TB_inline?height=480&width=750&inlineId=FakeLayer");
	$.post(
		'ajax.php',
		{
			'task':'getModalForm',
			'RecordID': id 
		},
		function(returnHtml){
			$("div#TB_ajaxContent").html(returnHtml);
		}
	);
}

function editRecord()
{
	var objs = document.getElementsByName('RecordID[]');
	var selected_id = 0;
	for(var i=0;i<objs.length;i++)
	{
		if(objs[i].checked){
			selected_id = objs[i].value;
		}
	}
	
	showModalForm(selected_id);
}

function checkEditForm()
{
	var valid = true;
	var start_hr = parseInt($('#StartTime_hour').val());
	var start_min = parseInt($('#StartTime_min').val());
	var start_sec = parseInt($('#StartTime_sec').val());
	var start_time = (start_hr < 10? '0'+start_hr : start_hr) + ':' + (start_min < 10? '0'+start_min : start_min) + ':' + (start_sec < 10? '0'+start_sec : start_sec);
	var end_hr = parseInt($('#EndTime_hour').val());
	var end_min = parseInt($('#EndTime_min').val());
	var end_sec = parseInt($('#EndTime_sec').val());
	var end_time = (end_hr < 10? '0'+end_hr : end_hr) + ':' + (end_min < 10? '0'+end_min : end_min) + ':' + (end_sec < 10? '0'+end_sec : end_sec);
	
	$('.WarnMsgDiv').hide();
	
	if(start_time >= end_time){
		$('#Overall_Error').html('<?=$Lang['StudentAttendance']['InvalidTimeWarning']?>').show();
		valid = false;
	}
	
	var upsertForm = function(){
		$('#task').val('upsertRecord');
		var formData = $('#modalForm').serialize();
		$.post(
			'ajax.php',
			formData,
			function(returnCode)
			{
				tb_remove();
				window.location.reload();
			}
		);
	};
	
	var checkTimeOverlap = function(){
		Block_Thickbox();
		$('#task').val('checkTimeOverlap');
		var formData = $('#modalForm').serialize();
		$.post(
			'ajax.php',
			formData,
			function(returnData)
			{
				var records = [];
				if(JSON && JSON.parse){
					records = JSON.parse(returnData);
				}else{
					eval('records = ' + returnData);
				}
				if(records.length <= 0){
					upsertForm();
				}else{
					var msg = '<?=$Lang['StudentAttendance']['TimePeriodOverlapWarning']?>';
					msg = msg.replace(/START_TIME/,records[0]['StartTime']).replace(/END_TIME/,records[0]['EndTime']);
					$('#Overall_Error').html(msg).show();
					UnBlock_Thickbox();
				}
			}
		);
	};
	
	if(valid){
		checkTimeOverlap();
	}
}

function enableRecord(enable)
{
	Block_Thickbox();
	
	var recordIdAry = [];
	var objs = document.getElementsByName('RecordID[]');
	for(var i=0;i<objs.length;i++)
	{
		if(objs[i].checked){
			recordIdAry.push(objs[i].value);
		}
	}
	
	$.post(
		'ajax.php',
		{
			'task':'enableRecord',
			'RecordID[]': recordIdAry,
			'RecordStatus': enable 
		},
		function(returnData)
		{
			window.location.reload();
		}
	);
}

function deleteRecord()
{
	Block_Thickbox();
	
	var recordIdAry = [];
	var objs = document.getElementsByName('RecordID[]');
	for(var i=0;i<objs.length;i++)
	{
		if(objs[i].checked){
			recordIdAry.push(objs[i].value);
		}
	}
	
	$.post(
		'ajax.php',
		{
			'task':'deleteRecord',
			'RecordID[]': recordIdAry 
		},
		function(returnData)
		{
			window.location.reload();
		}
	);
}
</script>
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>