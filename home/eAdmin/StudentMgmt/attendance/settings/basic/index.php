<?php
// editing by 
##################################### Change Log ###############################################
# 2020-09-22 by Ray: Add StudentBodyTemperatureAbnormalValue
# 2018-08-03 by Carlos: Add setting [iSmartCardDisableModifyStatusAfterConfirmed].
# 2018-03-12 by Carlos: Add setting [TeacherCanManageProveDocumentStatus].
# 2017-11-30 by Carlos: Hide [Class teacher to edit student profile reason on iAccount.] for KIS.
# 2017-04-12 by Carlos: Add setting [TeacherCanManageWaiveStatus], [TeacherCanManageReason] and [TeacherCanManageTeacherRemark].
# 2016-10-18 by Carlos: Add setting DisallowNonTeachingStaffTakeAttendance
# 2015-06-12 by Tiffany: Add setting EditDaysBeforeRecord and EditDaysBeforeRecordSelect
# 2014-07-11 by Carlos: Add setting CannotTakeFutureDateRecord
# 2012-05-22 by Carlos: Add Student Entry/Leave Date remark
# 2012-03-22 by Carlos: Add setting OnlySynceDisLateRecordWhenConfirm
# 2011-12-15 by Carlos: Add setting iSmartCardOnlyDisplaySchoolDayAttendanceRecord
# 2010-02-09 by Carlos: Add setting Default PM status not to follow AM status
################################################################################################
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libwordtemplates.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lcardstudentattend2 = new libcardstudentattend2();
$lword = new libwordtemplates();
//$GeneralSetting = new libgeneralsettings();
$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageSystemSetting_BasicSettings";

$linterface = new interface_html();

/*
$SettingList[] = "'AttendanceMode'";
$SettingList[] = "'TimeTableMode'";
$SettingList[] = "'DisallowProfileInput'";
$SettingList[] = "'DefaultAttendanceStatus'";
$SettingList[] = "'ProfileAttendCount'";
$SettingList[] = "'TeacheriAccountProfileAllowEdit'";
$SettingList[] = "'PMStatusNotFollowAMStatus'";
$SettingList[] = "'EnableEntryLeavePeriod'";
$SettingList[] = "'ClassTeacherTakeOwnClassOnly'";
$SettingList[] = "'DisableISmartCardPastDate'";
$SettingList[] = "'CannotTakeFutureDateRecord'";
$SettingList[] = "'IgnoreLateTapCardMins'";
$SettingList[] = "'iSmartCardOnlyDisplaySchoolDayAttendanceRecord'";
$SettingList[] = "'OnlySynceDisLateRecordWhenConfirm'";
$SettingList[] = "'DisallowNonTeachingStaffTakeAttendance'";
$SettingList[] = "'EditDaysBeforeRecord'";
$SettingList[] = "'EditDaysBeforeRecordSelect'";
$Settings = $GeneralSetting->Get_General_Setting('StudentAttendance',$SettingList);
*/
$Settings = $lcardstudentattend2->Settings;
$attendance_mode = $Settings['AttendanceMode'];
$time_table_mode = $Settings['TimeTableMode'];
if(!isset($Settings['EditDaysBeforeRecord'])||$Settings['EditDaysBeforeRecord']==""){
	$Settings['EditDaysBeforeRecord'] = 0;
}
if (($time_table_mode == 0)||($time_table_mode == ''))
{
	$table_content  = "<tr>";
	$table_content .= "<td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_StudentAttendance_Menu_Time_Mode</td>";
	$table_content .= "<td class=\"tabletext\" width=\"70%\"><input type=\"radio\" name=\"time_table\" id=\"time_table0\" value=\"0\" checked><label for=\"time_table0\">$i_StudentAttendance_Menu_Time_Input_Mode</label></td>";
	$table_content .= "</tr>";
	$table_content .= "<tr>";
	$table_content .= "<td>&nbsp;</td>";
	$table_content .= "<td class=\"tabletext\" width=\"70%\"><input type=\"radio\" name=\"time_table\" id=\"time_table1\" value=\"1\"><label for=\"time_table1\">$i_StudentAttendance_Menu_Time_Session_Mode</label></td>";
	$table_content .= "</tr>";
}

if ($time_table_mode == 1)
{
	$table_content  = "<tr>";
	$table_content .= "<td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_StudentAttendance_Menu_Time_Mode</td>";
	$table_content .= "<td class=\"tabletext\" width=\"70%\"><input type=\"radio\" name=\"time_table\" id=\"time_table0\" value=\"0\"><label for=\"time_table0\">$i_StudentAttendance_Menu_Time_Input_Mode</label></td>";
	$table_content .= "</tr>";
	$table_content .= "<tr>";
	$table_content .= "<td>&nbsp;</td>";
	$table_content .= "<td class=\"tabletext\" width=\"70%\"><input type=\"radio\" name=\"time_table\" id=\"time_table1\" value=\"1\" checked><label for=\"time_table1\">$i_StudentAttendance_Menu_Time_Session_Mode</label></td>";
	$table_content .= "</tr>";
}

## select current/past
$array_mode_name = array($i_StudentAttendance_AttendanceMode_AM_Only, $i_StudentAttendance_AttendanceMode_PM_Only, $i_StudentAttendance_AttendanceMode_WD_Lunch, $i_StudentAttendance_AttendanceMode_WD_NoLunch);
$array_mode_data = array("0", "1", "2", "3");
$select_mode = getSelectByValueDiffName($array_mode_data,$array_mode_name,"name='attendance_mode'",$attendance_mode,0,1);

// default attendance status 
$student_default_status = "<SELECT name='student_default_status'>\n
														<OPTION value='".CARD_STATUS_ABSENT."' ".($Settings['DefaultAttendanceStatus']==CARD_STATUS_ABSENT? "SELECTED":"").">$i_StudentAttendance_Status_PreAbsent</OPTION>\n
														<OPTION value='".CARD_STATUS_PRESENT."' ".($Settings['DefaultAttendanceStatus']==CARD_STATUS_PRESENT? "SELECTED":"").">$i_StudentAttendance_Status_OnTime</OPTION>\n
										      </SELECT>\n
													";

// Lesson session count as 1 day absent
$LessonSessionCount = '<select name="LessonSessionCountAsDay">';
for ($i=1; $i< 21; $i++) {
	$selected = ($Settings['LessonSessionCountAsDay'] == $i)? "selected":"";
	$LessonSessionCount .= '<option value="'.$i.'" '.$selected.'>'.$i.'</option>';
}
$LessonSessionCount .= '</select>';

//EditDaysBeforeRecord Select
$EditDaysBeforeRecordSelect = "<select name='EditDaysBeforeRecordSelect' id='EditDaysBeforeRecordSelect' disabled>";
for ($i=1; $i<=30; $i++) {
	$selected_1 = ($Settings['EditDaysBeforeRecordSelect'] == $i)? "selected":"";
	$EditDaysBeforeRecordSelect .= '<option value="'.$i.'" '.$selected_1.'>'.$i.'</option>';
}
$EditDaysBeforeRecordSelect .= '</select>';

$TAGS_OBJ[] = array($i_general_BasicSettings, "", 0);
$MODULE_OBJ = $lcardstudentattend2->GET_MODULE_OBJ_ARR();

if(isset($_SESSION['STUDENTATTENDANCE_BASIC_SETTINGS_RESULT'])){
	$Msg = $_SESSION['STUDENTATTENDANCE_BASIC_SETTINGS_RESULT'];
	unset($_SESSION['STUDENTATTENDANCE_BASIC_SETTINGS_RESULT']);
}else if(isset($Msg)){
	$Msg = urldecode($Msg);
}
$linterface->LAYOUT_START($Msg);

if ($msg == 2) $SysMsg = $linterface->GET_SYS_MSG("update");

$tempLayer = returnRemarkLayer($Lang['StudentAttendance']['MinsToIgnoreLateTapCardRecordRemark'],"hideMenu('ToolMenu2');", 500);

?>
<script language="Javascript" src='/templates/tooltip.js'></script>
<script language="javascript">
jQuery(document).ready( function() {
   if($("#EditDaysBeforeRecord0").attr("checked")){
      $("#EditDaysBeforeRecordSelect").attr('disabled', true);
   }
   if($("#EditDaysBeforeRecord1").attr("checked")){
      $("#EditDaysBeforeRecordSelect").attr('disabled', false);
   }
});


function showhideselect(status){
	if(status==0){
		$("#EditDaysBeforeRecordSelect").attr('disabled', true);
	}else if(status==1){
		$("#EditDaysBeforeRecordSelect").attr('disabled', false);
	}
	
}

<!--
function hideMenu2(menuName)
{
	objMenu = document.getElementById(menuName);
	if(objMenu!=null)
		objMenu.style.visibility='hidden';
}

function showMenu2(objName,menuName)
{
	objIMG = document.getElementById(objName);
	offsetX = (objIMG==null)?0:objIMG.width;
	offsetY =0;             	 	
	var pos_left = getPostion(objIMG,"offsetLeft");
	var pos_top  = getPostion(objIMG,"offsetTop");
	
	objDiv = document.getElementById(menuName);
	
	if(objDiv!=null){
		objDiv.style.visibility='visible';
		objDiv.style.top = pos_top+offsetY+"px";
		objDiv.style.left = pos_left+offsetX+"px";
	}
}
//-->
</script>

<div id="ToolMenu2" style="position:absolute; width=0px; height=0px; visibility:hidden; z-index:999999"><?=$tempLayer?></div>

<br />
<form name="form1" method="post" action="update.php">
<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
		<td colspan="2" align="right" class="tabletext">
			<?=$SysMsg?>
		</td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
			<?=$i_StudentAttendance_AttendanceMode?>
		</td>
		<td class="tabletext" width="70%">
			<?=$select_mode?>
		</td>
	</tr>
	<?=$table_content?>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
			<?=$i_StudentAttendance_default_attend_status?>
		</td>
		<td class="tabletext" width="70%">
			<?=$student_default_status?>
		</td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
			<?=$i_StudentAttendance_Setting_DisallowStudentProfileInput?>
		</td>
		<td class="tabletext" width="70%">
			<input type=checkbox name='student_profile_input'  value="1" <?=($Settings['DisallowProfileInput']==1? " CHECKED":"")?>>
		</td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
			<?=$Lang['StudentAttendance']['ProfileAttendCount']?>
		</td>
		<td class="tabletext" width="70%">
			<select name="ProfileAttendCount">
				<option value=0 <?=(($Settings['ProfileAttendCount'] == 0) ? "SELECTED"
				 : "") ?>><?=$i_Profile_AttendanceStatistic_Method1?></option>
				<option value=1 <?=(($Settings['ProfileAttendCount'] == 1) ? "SELECTED"
				 : "") ?>><?=$i_Profile_AttendanceStatistic_Method2?></option>
			</select>
		</td>
	</tr>
	<?php if($_SESSION["platform"]!='KIS'){ ?>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
			<?=$Lang['StudentAttendance']['TeacherIAccountStudentProfileEdit']?>
		</td>
		<td class="tabletext" width="70%">
			<input type=checkbox name='TeacheriAccountProfileAllowEdit'  value="1" <?=($Settings['TeacheriAccountProfileAllowEdit']==1? " CHECKED":"")?>>
		</td>
	</tr>
	<?php } ?>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
			<?=$Lang['StudentAttendance']['PMNotFollowAM']?>
		</td>
		<td class="tabletext" width="70%">
			<input type=checkbox name='PMStatusNotFollowAMStatus' value="1" <?=($Settings['PMStatusNotFollowAMStatus']==1?" CHECKED":"")?>>
		</td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
			<?=$Lang['StudentAttendance']['EnableEntryLeavePeriodSetting']?>
		</td>
		<td class="tabletext" width="70%">
			<input type=checkbox name='EnableEntryLeavePeriod' value="1" <?=($Settings['EnableEntryLeavePeriod']==1?" CHECKED":"")?>>
		</td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="tabletextremark" colspan="2">
			<?=$Lang['StudentAttendance']['Warning']['SystemSettingsStudentEntryLeaveDate']?>
		</td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
			<?=$Lang['StudentAttendance']['ClassTeacherTakeOwnClassOnlySetting']?>
		</td>
		<td class="tabletext" width="70%">
			<input type=checkbox name='ClassTeacherTakeOwnClassOnly' value="1" <?=($Settings['ClassTeacherTakeOwnClassOnly']==1?" CHECKED":"")?>>
		</td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
			<?=$Lang['StudentAttendance']['DisallowNonTeachingStaffTakeAttendance']?>
		</td>
		<td class="tabletext" width="70%">
			<input type=checkbox name='DisallowNonTeachingStaffTakeAttendance' value="1" <?=($Settings['DisallowNonTeachingStaffTakeAttendance']==1?" CHECKED":"")?>>
		</td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
			<?=$Lang['StudentAttendance']['ISmartCardDisableTakePastRecord']?>
		</td>
		<td class="tabletext" width="70%">
			<input type=checkbox name='DisableISmartCardPastDate' value="1" <?=($Settings['DisableISmartCardPastDate']==1?" CHECKED":"")?>>
		</td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
			<?=$Lang['StudentAttendance']['CannotTakeFutureDateRecord']?>
		</td>
		<td class="tabletext" width="70%">
			<input type=checkbox name='CannotTakeFutureDateRecord' value="1" <?=($Settings['CannotTakeFutureDateRecord']==1?" CHECKED":"")?>>
		</td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
			<?=$Lang['StudentAttendance']['OnlySynceDisLateRecordWhenConfirm']?>
		</td>
		<td class="tabletext" width="70%">
			<input type=checkbox name='OnlySynceDisLateRecordWhenConfirm' value="1" <?=($Settings['OnlySynceDisLateRecordWhenConfirm']==1?" CHECKED":"")?>>
		</td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
			<?=$Lang['StudentAttendance']['iSmartCardOnlyDisplaySchoolDayAttendanceRecord']?>
		</td>
		<td class="tabletext" width="70%">
			<input type=checkbox name='iSmartCardOnlyDisplaySchoolDayAttendanceRecord' value="1" <?=($Settings['iSmartCardOnlyDisplaySchoolDayAttendanceRecord']==1?" CHECKED":"")?>>
		</td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
			<?=$Lang['StudentAttendance']['iSmartCardDoNotAllowTeacherModifyAttendanceStatusAfterAdminConfirmedRecord']?>
		</td>
		<td class="tabletext" width="70%">
			<input type=checkbox name='iSmartCardDisableModifyStatusAfterConfirmed' value="1" <?=($Settings['iSmartCardDisableModifyStatusAfterConfirmed']==1?" CHECKED":"")?>>
		</td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
			<?=$Lang['StudentAttendance']['DefaultAbsentReason']?>
		</td>
		<td class="tabletext" width="70%">
			<?
			// prepare preset WEBSAMS reason list
			$words_absence = $lword->getWordListAttendance(PROFILE_TYPE_ABSENT);
			$AbsentHasWord = sizeof($words_absence)!=0;
			foreach($words_absence as $key=>$word)
				$words_absence[$key]= htmlspecialchars($word);
			?>
			<?=$linterface->CONVERT_TO_JS_ARRAY($words_absence, "AbsentArrayWords", 1, 1)?>
			<input class="textboxnum" type="text" name="DefaultAbsentReason" id="DefaultAbsentReason" maxlength="255" value="<?=htmlspecialchars($lcardstudentattend2->DefaultAbsentReason,ENT_QUOTES)?>">
			<?=$linterface->GET_PRESET_LIST("AbsentArrayWords", 0, "DefaultAbsentReason")?>
		</td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
			<?=$Lang['StudentAttendance']['DefaultLateReason']?>
		</td>
		<td class="tabletext" width="70%">
			<?
			// prepare preset WEBSAMS reason list
			$words_late = $lword->getWordListAttendance(PROFILE_TYPE_LATE);
			$LateHasWord = sizeof($words_late)!=0;
			foreach($words_late as $key=>$word)
				$words_late[$key]= htmlspecialchars($word,ENT_QUOTES);
			?>
			<?=$linterface->CONVERT_TO_JS_ARRAY($words_late, "LateArrayWords", 1, 1)?>
			<input class="textboxnum" type="text" name="DefaultLateReason" id="DefaultLateReason" maxlength="255" value="<?=htmlspecialchars($lcardstudentattend2->DefaultLateReason,ENT_QUOTES)?>">
			<?=$linterface->GET_PRESET_LIST("LateArrayWords", 1, "DefaultLateReason")?>
		</td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
			<?=$Lang['StudentAttendance']['DefaultEarlyReason']?>
		</td>
		<td class="tabletext" width="70%">
			<?
			// prepare preset WEBSAMS reason list
			$words_early = $lword->getWordListAttendance(PROFILE_TYPE_EARLY);
			$EarlyHasWord = sizeof($words_early)!=0;
			foreach($words_early as $key=>$word)
				$words_early[$key]= htmlspecialchars($word);
			?>
			<?=$linterface->CONVERT_TO_JS_ARRAY($words_early, "EarlyArrayWords", 1, 1)?>
			<input class="textboxnum" type="text" name="DefaultEarlyReason" id="DefaultEarlyReason" maxlength="255" value="<?=htmlspecialchars($lcardstudentattend2->DefaultEarlyReason,ENT_QUOTES)?>">
			<?=$linterface->GET_PRESET_LIST("EarlyArrayWords", 2, "DefaultEarlyReason")?>
		</td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
			<?=$Lang['StudentAttendance']['DefaultOutingReason']?>
		</td>
		<td class="tabletext" width="70%">
			<?
			// prepare preset WEBSAMS reason list
			$words_outing = $lcardstudentattend2->Get_Outing_Reason();
			foreach($words_outing as $key=>$word)
				$words_outing[$key]= htmlspecialchars($word);
			?>
			<?=$linterface->CONVERT_TO_JS_ARRAY($words_outing, "OutingArrayWords", 1, 1)?>
			<input class="textboxnum" type="text" name="DefaultOutingReason" id="DefaultOutingReason" maxlength="255" value="<?=htmlspecialchars($lcardstudentattend2->DefaultOutingReason,ENT_QUOTES)?>">
			<?=$linterface->GET_PRESET_LIST("OutingArrayWords", 2, "DefaultOutingReason")?>
		</td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
			<?=$Lang['StudentAttendance']['MinsToIgnoreLateTapCardRecord']?>
			<a href="javascript:showMenu2('img_3','ToolMenu2');"><img id='img_3' src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/inventory/icon_help.gif" border=0>
		</td>
		<td class="tabletext" width="70%">
			<input class="textboxnum" type="text" name="IgnoreLateTapCardMins" id="IgnoreLateTapCardMins" maxlength="3" value="<?=($Settings['IgnoreLateTapCardMins']+0)?>">
			<? /* ?>
			<span class="tabletextremark">
				<?=$Lang['StudentAttendance']['MinsToIgnoreLateTapCardRecordNote']?>
			</span>
			<? */ ?>
		</td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
			<?=$Lang['StudentAttendance']['JustCanEditRecordWithinNDaysBefore']?>
		</td>
		<td class="tabletext" width="70%">
		    <table>
		      <tr><td>
		         <input type="radio" name="EditDaysBeforeRecord" id="EditDaysBeforeRecord0" value="0" onclick="javascript:showhideselect(0);" <?=($Settings['EditDaysBeforeRecord']==0?" CHECKED":"")?>><label for="EditDaysBeforeRecord0"><?=$Lang['StudentAttendance']['Unlimited']?></label>
		      </td></tr>
		      <tr><td>
		         <input type="radio" name="EditDaysBeforeRecord" id="EditDaysBeforeRecord1" value="1" onclick="javascript:showhideselect(1);" <?=($Settings['EditDaysBeforeRecord']==1?" CHECKED":"")?>><label for="EditDaysBeforeRecord1"><?=$Lang['StudentAttendance']['Within']?> <?=$EditDaysBeforeRecordSelect?> <?=$Lang['StudentAttendance']['days']?></label>
		      </td></tr>
		    </table>
		</td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
			<?=$Lang['StudentAttendance']['TeacherCanManageDataAtMySmartCard']?>
		</td>
		<td class="tabletext" width="70%">
			<input type="checkbox" name="TeacherCanManageWaiveStatus" id="TeacherCanManageWaiveStatus" value="1" <?=$Settings['TeacherCanManageWaiveStatus']=='1'?'checked':''?>/> <label for="TeacherCanManageWaiveStatus"><?=$button_waive?></label><br />
			<input type="checkbox" name="TeacherCanManageProveDocumentStatus" id="TeacherCanManageProveDocumentStatus" value="1" <?=$Settings['TeacherCanManageProveDocumentStatus']=='1'?'checked':''?>/> <label for="TeacherCanManageProveDocumentStatus"><?=$Lang['StudentAttendance']['ProveDocument']?></label><br />
			<input type="checkbox" name="TeacherCanManageReason" id="TeacherCanManageReason" value="1" <?=$Settings['TeacherCanManageReason']=='1'?'checked':''?>/> <label for="TeacherCanManageReason"><?=$i_Attendance_Reason?></label><br />
			<input type="checkbox" name="TeacherCanManageTeacherRemark" id="TeacherCanManageTeacherRemark" value="1" <?=$Settings['TeacherCanManageTeacherRemark']=='1'?'checked':''?>/> <label for="TeacherCanManageTeacherRemark"><?=$Lang['StudentAttendance']['iSmartCardRemark']?></label>
		</td>
	</tr>
    <tr>
        <td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
			<?=$Lang['StudentAttendance']['AllowClassTeacherManagePresetStudentAbsence']?>
        </td>
        <td class="tabletext" width="70%">
            <input type=checkbox name='AllowClassTeacherManagePresetStudentAbsence' value="1" <?=($Settings['AllowClassTeacherManagePresetStudentAbsence']==1?" CHECKED":"")?>>
        </td>
    </tr>
    <?php if($sys_custom['StudentAttendance']['RecordBodyTemperature']) { ?>
    <tr>
        <td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
            <?=$Lang['StudentAttendance']['StudentBodyTemperatureAbnormalValue']?>
        </td>
        <td class="tabletext" width="70%">
			<?=$Lang['StudentAttendance']['Above']?>
                <?php
                if($Settings['StudentBodyTemperatureAbnormalValue'] == '') {
					$Settings['StudentBodyTemperatureAbnormalValue'] = 37.2;
				}
				$temperature_array = array();
                for($i=360;$i<=380;$i++) {
                    $value = number_format($i/10, 1);
					$temperature_array[] = array($value, $value);
				}
				echo getSelectByArray($temperature_array, 'name="StudentBodyTemperatureAbnormalValue" id="StudentBodyTemperatureAbnormalValue"', $Settings['StudentBodyTemperatureAbnormalValue'], 0, 1);
                ?>
                &#8451;
        </td>
    </tr>
    <?php } ?>
</table>
<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
    	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
    </tr>
    <tr>
	    <td align="center">
			<?= $linterface->GET_ACTION_BTN($button_save, "submit", "") ?>
			<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
		</td>
	</tr>
</table>
</form>
<?
$linterface->LAYOUT_STOP();
print $linterface->FOCUS_ON_LOAD("form1.attendance_mode");
intranet_closedb();
?>