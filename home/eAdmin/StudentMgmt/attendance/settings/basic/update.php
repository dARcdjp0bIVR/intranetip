<?php
// editing by 
##################################### Change Log ###############################################
# 2020-09-22 by Ray: Add StudentBodyTemperatureAbnormalValue
# 2018-08-03 by Carlos: Add setting [iSmartCardDisableModifyStatusAfterConfirmed].
# 2018-03-12 by Carlos: Add setting [TeacherCanManageProveDocumentStatus].
# 2017-04-12 by Carlos: Add setting [TeacherCanManageWaiveStatus], [TeacherCanManageReason] and [TeacherCanManageTeacherRemark].
# 2016-10-18 by Carlos: Add setting DisallowNonTeachingStaffTakeAttendance
# 2015-06-12 by Tiffany: Add setting EditDaysBeforeRecord and EditDaysBeforeRecordSelect
# 2014-07-11 by Carlos: Add setting CannotTakeFutureDateRecord
# 2012-03-22 by Carlos: Add setting OnlySynceDisLateRecordWhenConfirm
# 2011-12-15 by Carlos: Add setting iSmartCardOnlyDisplaySchoolDayAttendanceRecord
# 2010-02-09 by Carlos: Add setting Default PM status not to follow AM status
################################################################################################
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$SettingList['AttendanceMode'] = $_REQUEST['attendance_mode'];
$SettingList['TimeTableMode'] = $_REQUEST['time_table'];
$SettingList['DisallowProfileInput'] = $_REQUEST['student_profile_input'];
$SettingList['DefaultAttendanceStatus'] = $_REQUEST['student_default_status'];
$SettingList['ProfileAttendCount'] = $_REQUEST['ProfileAttendCount'];
$SettingList['TeacheriAccountProfileAllowEdit'] = $_REQUEST['TeacheriAccountProfileAllowEdit'];
$SettingList['PMStatusNotFollowAMStatus'] = $_REQUEST['PMStatusNotFollowAMStatus'];
$SettingList['EnableEntryLeavePeriod'] = $_REQUEST['EnableEntryLeavePeriod'];
$SettingList['ClassTeacherTakeOwnClassOnly'] = $_REQUEST['ClassTeacherTakeOwnClassOnly'];
$SettingList['DisableISmartCardPastDate'] = $_REQUEST['DisableISmartCardPastDate'];
$SettingList['CannotTakeFutureDateRecord']= $_REQUEST['CannotTakeFutureDateRecord'];
$SettingList['DefaultAbsentReason'] = trim(htmlspecialchars_decode(stripslashes($_REQUEST['DefaultAbsentReason']),ENT_QUOTES));
$SettingList['DefaultLateReason'] = trim(htmlspecialchars_decode(stripslashes($_REQUEST['DefaultLateReason']),ENT_QUOTES));
$SettingList['DefaultEarlyReason'] = trim(htmlspecialchars_decode(stripslashes($_REQUEST['DefaultEarlyReason']),ENT_QUOTES));
$SettingList['DefaultOutingReason'] = trim(htmlspecialchars_decode(stripslashes($_REQUEST['DefaultOutingReason']),ENT_QUOTES));
$SettingList['IgnoreLateTapCardMins'] = $_REQUEST['IgnoreLateTapCardMins'];
$SettingList['iSmartCardOnlyDisplaySchoolDayAttendanceRecord'] = $_REQUEST['iSmartCardOnlyDisplaySchoolDayAttendanceRecord'];
$SettingList['iSmartCardDisableModifyStatusAfterConfirmed'] = $_REQUEST['iSmartCardDisableModifyStatusAfterConfirmed'];
$SettingList['OnlySynceDisLateRecordWhenConfirm'] = $_REQUEST['OnlySynceDisLateRecordWhenConfirm'];
$SettingList['DisallowNonTeachingStaffTakeAttendance'] = $_REQUEST['DisallowNonTeachingStaffTakeAttendance'];
$SettingList['EditDaysBeforeRecord'] = $_REQUEST['EditDaysBeforeRecord'];
if($SettingList['EditDaysBeforeRecord']==0){
	$SettingList['EditDaysBeforeRecordSelect'] = "";
}else if($SettingList['EditDaysBeforeRecord']==1){
	$SettingList['EditDaysBeforeRecordSelect'] = $_REQUEST['EditDaysBeforeRecordSelect'];
}
$SettingList['TeacherCanManageWaiveStatus'] = $_REQUEST['TeacherCanManageWaiveStatus']?1:0;
$SettingList['TeacherCanManageProveDocumentStatus'] = $_REQUEST['TeacherCanManageProveDocumentStatus']?1:0;
$SettingList['TeacherCanManageReason'] = $_REQUEST['TeacherCanManageReason']?1:0;
$SettingList['TeacherCanManageTeacherRemark'] = $_REQUEST['TeacherCanManageTeacherRemark']?1:0;
$SettingList['AllowClassTeacherManagePresetStudentAbsence'] = $_REQUEST['AllowClassTeacherManagePresetStudentAbsence']?1:0;
if($sys_custom['StudentAttendance']['RecordBodyTemperature']) {
	$SettingList['StudentBodyTemperatureAbnormalValue'] = $_REQUEST['StudentBodyTemperatureAbnormalValue'];
}

$GeneralSetting = new libgeneralsettings();
$lc = new libcardstudentattend2();

$GeneralSetting->Start_Trans();
if ($GeneralSetting->Save_General_Setting('StudentAttendance',$SettingList)) {
	$GeneralSetting->Commit_Trans();
	$Msg = $Lang['StudentAttendance']['SettingApplySuccess'];
}
else {
	$GeneralSetting->RollBack_Trans();
	$Msg = $Lang['StudentAttendance']['SettingApplyFail'];
}

intranet_closedb();
$_SESSION['STUDENTATTENDANCE_BASIC_SETTINGS_RESULT'] = $Msg;
header("Location: index.php");
exit;
?>