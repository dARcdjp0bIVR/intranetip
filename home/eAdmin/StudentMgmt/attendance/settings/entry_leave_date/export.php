<?php
// editing by 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lexporttext = new libexporttext();
$lc = new libcardstudentattend2();

$headerColumn = array();
$headerColumn[] = "UserLogin";
$headerColumn[] = "EntryDate";
$headerColumn[] = "LeaveDate";

$exportContent = array();

$records = $lc->Get_Entry_Leave_Export_Records();
$numOfRecords = count($records);
for($i=0;$i<$numOfRecords;$i++) {
	list($userlogin, $entrydate, $leavedate) = $records[$i];
	$exportRow = array();
	$exportRow[] = $userlogin;
	$exportRow[] = $entrydate;
	$exportRow[] = $leavedate;
	$exportContent[] = $exportRow;
}

$CSVFileName = $Lang['StudentAttendance']['EntryLeaveDate'].".csv";
$export_data = $lexporttext->GET_EXPORT_TXT($exportContent, $headerColumn);
$lexporttext->EXPORT_FILE($CSVFileName, $export_data);

intranet_closedb();
?>