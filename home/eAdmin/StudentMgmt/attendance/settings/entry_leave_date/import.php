<?php
// editing by Carlos
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libstudentattendance_ui.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$linterface = new interface_html();
$StudentAttendUI = new libstudentattendance_ui();

$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage['StudentEntryLeaveSetting'] = 1;
$TAGS_OBJ[] = array($Lang['StudentAttendance']['EntryLeaveDate'], "", 0);
$MODULE_OBJ = $StudentAttendUI->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START(urldecode($_REQUEST['Msg']));

echo $StudentAttendUI->Get_Entry_Period_Import_Form();

$linterface->LAYOUT_STOP();
intranet_closedb();
?>