<?php
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$li = new libcardstudentattend2();
$nonSchoolDay=$non_school_day==""?0:1;
if($DayType==1 || $DayType==2||$DayType==0){
$sql = "UPDATE CARD_STUDENT_CLASS_PERIOD_TIME
               SET MorningTime = '$preset_am', LunchStart = '$preset_lunch',
                   LunchEnd = '$preset_pm', LeaveSchoolTime = '$preset_leave',NonSchoolDay='$nonSchoolDay',
                   DateModified= now()
               WHERE ClassID = '$ClassID' AND DayType='$DayType' AND DayValue ='$DayValue'";
}
else{
	$sql = "UPDATE CARD_STUDENT_SPECIFIC_DATE_TIME SET MorningTime = '$preset_am', LunchStart = '$preset_lunch',
                   LunchEnd = '$preset_pm', LeaveSchoolTime = '$preset_leave',NonSchoolDay='$nonSchoolDay',
                   DateModified= now()
               WHERE ClassID = '$ClassID' AND RecordDate='$DayValue'";

}
if ($li->db_db_query($sql)) {
	$Msg = $Lang['StudentAttendance']['SettingApplySuccess'];
}
else {
	$Msg = $Lang['StudentAttendance']['SettingApplyFail'];
}


intranet_closedb();
header("Location: class_edit.php?ClassID=$ClassID&Msg=".urlencode($Msg));
?>