<?php
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$li = new libcardstudentattend2();
$nonSchoolDay=$non_school_day==""?0:1;
$sql = "UPDATE CARD_STUDENT_CLASS_PERIOD_TIME
             SET MorningTime = '$normal_am', LunchStart = '$normal_lunch',
                 LunchEnd = '$normal_pm', LeaveSchoolTime = '$normal_leave',NonSchoolDay='$nonSchoolDay',
                 DateModified= now()
             WHERE DayType=0 AND DayValue=0 AND ClassID = '$ClassID'";

if ($li->db_db_query($sql)) {
	$Msg = $Lang['StudentAttendance']['SettingApplySuccess'];
}
else {
	$Msg = $Lang['StudentAttendance']['SettingApplyFail'];
}


intranet_closedb();
header("Location: class_edit.php?ClassID=$ClassID&Msg=".urlencode($Msg));
?>