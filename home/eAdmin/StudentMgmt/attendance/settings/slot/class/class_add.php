<?php
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libstudentattendance_ui.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();
$StudentAttendUI = new libstudentattendance_ui();
$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageSystemSetting_TimeSlotSettings";

$linterface = new interface_html();

$lc->retrieveSettings();
$normal_time_array = $lc->getClassTimeArray($ClassID,0,0);
list($normal_am, $normal_lunch, $normal_pm, $normal_leave) = $normal_time_array;
$hasAM = ($lc->attendance_mode != 1);
$hasLunch = ($lc->attendance_mode != 0 && $lc->attendance_mode != 1);
$hasPM = ($lc->attendance_mode != 0);
$days = $lc->getDayValueWithoutSpecificForClass($ClassID,$type);
$classname = $lc->getClassName($ClassID);

if ($type==2)  # Cycle
{
  $select_title = $i_StudentAttendance_CycleDay.":";
  $select_day = getSelectByValue($days,"name=\"DayValue\"");
}
else if ($type==1)  # Week
{
  $select_title = $i_StudentAttendance_WeekDay.":";
  $select_day = "<SELECT name=\"DayValue\">\n";
  $select_day .= "<OPTION value=\"\">-- $button_select --</OPTION>";
  for ($i=0; $i<sizeof($days); $i++)
  {
       $word = $i_DayType0[$days[$i]];
       $select_day .= "<OPTION value='".$days[$i]."'>".$word."</OPTION>\n";
  }
  $select_day .= "</SELECT>\n";
}
else if($type==0) { # Normal
	$select_title="$i_StudentAttendance_NormalDays";
	$select_day="";
}
else # Special
{
  $select_title = "$i_StudentAttendance_TimeSlot_SpecialDay";
  $select_day = '<span id="dates">';
	$select_day .= $linterface->GET_DATE_PICKER("target_date[]", date('Y-m-d'),"","yy-mm-dd","","","","","target_date0");
	$select_day .= "<br></span>\n";
	$select_day .= $linterface->GET_SMALL_BTN(" + ", "button", "addDate(document.getElementById('dateFieldCount').value)")."<br />";
}

if($type==0)
	$PAGE_NAVIGATION[] = array($button_edit);
else
	$PAGE_NAVIGATION[] = array($button_add);

$TAGS_OBJ[] = array($i_StudentAttendance_Menu_Slot_School, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/settings/slot/school/", 0);
$TAGS_OBJ[] = array($i_StudentAttendance_Menu_Slot_Class, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/settings/slot/class/", 1);
$TAGS_OBJ[] = array($i_StudentAttendance_Menu_Slot_Group, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/settings/slot/group/", 0);
$MODULE_OBJ = $lc->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

if ($msg == 1) $SysMsg = $linterface->GET_SYS_MSG("add");
if ($msg == 2) $SysMsg = $linterface->GET_SYS_MSG("update");
if ($msg == 3) $SysMsg = $linterface->GET_SYS_MSG("delete");
?>
<script language="javascript">
<!--
function checkForm(formObj){
	if(checkForm1(formObj))
		return checkForm2(formObj);
	return false;
}
function checkForm1(formObj){
	targetDayObj = document.getElementsByName('target_date[]');
	if(targetDayObj!=null && targetDayObj.length > 0){
		ReturnVal = true;
		for (var i=0; i< targetDayObj.length; i++) {
			obj=targetDayObj[i];
			if(!check_date(obj,"<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>")) {
				ReturnVal = false;
				break;
			}
		}
		
		return ReturnVal;
	}
	else {
		dayValueObj =formObj.DayValue;
		if(dayValueObj!=null){
			i= formObj.DayValue.selectedIndex;
			if(i==0){
				alert("<?if($type==2) echo $i_StudentAttendance_Warn_Please_Select_CycleDay;else if($type==1) echo $i_StudentAttendance_Warn_Please_Select_WeekDay;?>");
				return false;
			}else{
				return true;
			}
		}
	}
	
	return false;	
}
function checkForm2(formObj){
	if(formObj.non_school_day!=null && formObj.non_school_day.checked==true)
		return true;

	amObj = formObj.normal_am;
	lunchObj=formObj.normal_lunch;
	pmObj = formObj.normal_pm;
	leaveObj = formObj.normal_leave;

	if(amObj!=null){
		am = amObj.value;
		if(!isValidTimeFormat(am)){
			amObj.focus();
			return false;
		}
	}
	if(lunchObj!=null){
		lunch = lunchObj.value;
		if(!isValidTimeFormat(lunch)){
			lunchObj.focus();
			return false;
		}
	}
	if(pmObj!=null){
		pm = pmObj.value;
		if(!isValidTimeFormat(pm)){
			pmObj.focus();
			return false;
		}
	}
	if(leaveObj!=null){
		leave = leaveObj.value;
		if(!isValidTimeFormat(leave)){
			leaveObj.focus();
			return false;
		}
	}

	if(isValidValues(amObj,lunchObj,pmObj,leaveObj))
		return true;
	return false;
}

function isValidTimeFormat(timeVal) {
	var re = new RegExp("^(([0-1][0-9])|2[0-3]):[0-5][0-9]$");
	if (!timeVal.match(re)) {
			alert("<?=$i_StudentAttendance_Warn_Invalid_Time_Format?>");
			return false;
	}
	return true;
}

function isValidValues(amObj,lunchObj,pmObj,leaveObj) {
	if(amObj!=null){
		if(lunchObj!=null && amObj.value>=lunchObj.value){ 
			alert("<?=$i_StudentAttendance_Warn_AM_Must_Smaller_Than_LunchStart?>");
			amObj.focus();
			return false;
		}
		else if(pmObj!=null && amObj.value>=pmObj.value){
			alert("<?=$i_StudentAttendance_Warn_AM_Must_Smaller_Than_PMStart?>");
			amObj.focus();
			return false;
		}
		else if(leaveObj!=null && amObj.value>=leaveObj.value){
			alert("<?=$i_StudentAttendance_Warn_AM_Must_Smaller_Than_SchoolEnd?>");
			amObj.focus();
			return false;
		}
	}
	if(lunchObj!=null){
		if(pmObj!=null && lunchObj.value>=pmObj.value){
			alert("<?=$i_StudentAttendance_Warn_LunchStart_Must_Smaller_Than_PMStart?>");
			lunchObj.focus();
			return false;
		}
		else if(leaveObj!=null && lunchObj.value>=leaveObj.value){
			alert("<?=$i_StudentAttendance_Warn_LunchStart_Must_Smaller_Than_SchoolEnd?>");
			lunchObj.focus();
			return false;
		}
	}
	if(pmObj!=null){
		if(leaveObj!=null && pmObj.value>=leaveObj.value){
			alert("<?=$i_StudentAttendance_Warn_PMStart_Must_Smaller_Than_SchoolEnd?>");
			pmObj.focus();
			return false;
		}
	}
	return true;
}

function setForm(formObj){
	nonSchoolDayObj = formObj.non_school_day;
	amObj = formObj.normal_am;
	lunchObj=formObj.normal_lunch;
	pmObj = formObj.normal_pm;
	leaveObj = formObj.normal_leave;
	if(nonSchoolDayObj!=null){
		if(nonSchoolDayObj.checked){
			if(amObj!=null) amObj.disabled=true;
			if(lunchObj!=null) lunchObj.disabled=true;
			if(pmObj!=null) pmObj.disabled=true;
			if(leaveObj!=null) leaveObj.disabled=true;
		}else{
			if(amObj!=null) amObj.disabled=false;
			if(lunchObj!=null) lunchObj.disabled=false;
			if(pmObj!=null) pmObj.disabled=false;
			if(leaveObj!=null) leaveObj.disabled=false;
		}
	}
}

function resetForm(formObj) {
	formObj.reset();
	setForm(formObj);
}

function addDate(count){
	document.form1.dateFieldCount.value = count+1;
	obj = document.getElementById('dates');
	if(obj==null) return;
	$.post('../../../dailyoperation/preset_absence/ajax_get_new_date_picker.php',{"DateCount":count},function(data){
			if (data == "die") 
				window.top.location = '/';
			else {
				$('span#dates').append(data);
			}
		});
}
// -->
</script>
<br />
<form name="form1" method="POST" ACTION="class_add_update.php" onSubmit="return checkForm(this)">
<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
	<tr>
		<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	</tr>
</table>
<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $i_ClassName ?></td>
		<td class="tabletext" width="70%"><?=$classname?></td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$select_title?></td>
		<td class="tabletext" width="70%"><?=$select_day?></td>
	</tr>
	<?
	### Mode 1,3,4
	if ($hasAM) {
	?>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_StudentAttendance_SetTime_AMStart?></td>
		<td class="tabletext" width="70%"><input type="text" class="textboxnum" maxlength="5" name="normal_am" value="<?=$normal_am?>"></td>
	</tr>
	<? } ?>
	<?
	### Mode 3,4
	if ($hasLunch) {
	?>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_StudentAttendance_SetTime_LunchStart?></td>
		<td class="tabletext" width="70%"><input type="text" class="textboxnum" maxlength="5" name="normal_lunch" value="<?=$normal_lunch?>"></td>
	</tr>
	<? } ?>
	<?
	### Mode 2,3,4
	if ($hasPM) {
	?>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_StudentAttendance_SetTime_PMStart?></td>
		<td class="tabletext" width="70%"><input type="text" class="textboxnum" maxlength="5" name="normal_pm" value="<?=$normal_pm?>"></td>
	</tr>
	<? } ?>
	<?
	### Mode 1,2,3,4
	?>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_StudentAttendance_SetTime_SchoolEnd?></td>
		<td class="tabletext" width="70%"><input type="text" class="textboxnum" maxlength="5" name="normal_leave" value="<?=$normal_leave?>"></td>
	</tr>
	<? if($type!=0) { ?>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_StudentAttendance_NonSchoolDay?></td>
		<td class="tabletext" width="70%"><input type="checkbox" onClick="setForm(this.form)" name="non_school_day" value="1"></td>
	</tr>
	<? } ?>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$Lang['StudentAttendance']['ApplyToTheseClassAlso']?></td>
		<td class="tabletext" width="70%"><?=$StudentAttendUI->Get_Specified_Attendance_Mode_Selection_List("Class",$ClassID)?></td>
	</tr>
</table>
<table width="96%" border="0" cellpadding="0" cellspacing="0" align="center">
	<tr>
    	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
    </tr>
</table>
<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
    <tr>
	    <td align="center">
			<?= $linterface->GET_ACTION_BTN($button_save, "submit", "") ?>
			<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
			<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:window.location='class_edit.php?ClassID=".$ClassID."';","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
		</td>
	</tr>
	<tr>
	    <td class="tabletextremark"><?=$i_StudentAttendance_Slot_SettingsDescription?></td>
	</tr>
</table>
<input type="hidden" name="ClassID" value="<?=$ClassID?>">
<input type="hidden" name="DayType" value="<?=$type?>">
<input type="hidden" name="dateFieldCount" id="dateFieldCount" value="1">
<? if ($type==0) { ?>
<input type="hidden" name="DayValue" value="0">
<? } ?>
</form>
<br />
<?
$linterface->LAYOUT_STOP();
if ($type==1 || $type==2)
	print $linterface->FOCUS_ON_LOAD("form1.DayValue");

if ($type==3)
	print $linterface->FOCUS_ON_LOAD("form1.TargetDate");
	
if ($type==0) {
	if ($hasAM)
		print $linterface->FOCUS_ON_LOAD("form1.normal_am");
	else if ($hasLunch)
		print $linterface->FOCUS_ON_LOAD("form1.normal_lunch");
	else if ($hasPM)
		print $linterface->FOCUS_ON_LOAD("form1.normal_pm");
}
intranet_closedb();
?>