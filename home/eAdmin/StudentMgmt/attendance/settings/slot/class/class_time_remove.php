<?php
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");

intranet_auth();
intranet_opendb();

$li = new libcardstudentattend2();

# Delete SQL
// Type 0-3: Delete one item
// Type 4-7: Delete more than one items
if($type==1||$type==2||$type==0) {
	$sql = "DELETE FROM CARD_STUDENT_CLASS_PERIOD_TIME WHERE ClassID='$ClassID' AND DayType='$type' AND DayValue ='$value'";
}else if($type==3) {
	$sql = "DELETE FROM CARD_STUDENT_SPECIFIC_DATE_TIME WHERE ClassID='$ClassID' AND RecordDate='$value'";
} else if($type==7){
	$value = str_replace(",","','",$value);
	$sql = "DELETE FROM CARD_STUDENT_SPECIFIC_DATE_TIME WHERE ClassID='$ClassID' AND RecordDate IN ('$value')";
}
if ($li->db_db_query($sql)) {
	$Msg = $Lang['StudentAttendance']['SettingApplySuccess'];
}
else {
	$Msg = $Lang['StudentAttendance']['SettingApplyFail'];
}


intranet_closedb();
header("Location: class_edit.php?ClassID=$ClassID&Msg=".urlencode($Msg));
?>