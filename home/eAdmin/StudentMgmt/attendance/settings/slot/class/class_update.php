<?php
## Using By : 
##########################################################
## Modification Log
## 2009-12-04: Max (200912041605)
## - support multiple classes/groups change
##########################################################

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();

$ClassIDsArray = $_POST['ClassIDs'];

$updateSuccess = true;
$url = "index.php";
if (is_array($ClassIDsArray) && count($ClassIDsArray) > 0) {
	foreach($ClassIDsArray as $key => $ClassID) {
		$updateSuccess *= updateRecords($mode, $ClassID);
	}
	$url = "index.php";
} else {
	$updateSuccess = updateRecords($mode, $ClassID);
	if ($mode != 1)
	{ 
		$url = "class.php";
	}
	else
	{
	    $url = "class_edit.php";
	}
}

if ($updateSuccess) {
	$Msg = $Lang['StudentAttendance']['SettingApplySuccess'];
//	$Msg = $Lang['ePayment']['SettingApplySuccess'];
} else {
	$Msg = $Lang['StudentAttendance']['SettingApplyFail'];
//	$Msg = $Lang['ePayment']['SettingApplyFail'];
}
function updateRecords($mode, $ClassID) {
	global $Lang;
	if ($mode != 1 && $mode != 2)
	{
	    $mode = 0;
	}
	
	$li = new libdb();
	$li->Start_Trans();
	$sql = 'INSERT INTO CARD_STUDENT_CLASS_SPECIFIC_MODE (ClassID, Mode, DateInput, DateModified)
	               VALUES (\''.$ClassID.'\',\''.$mode.'\',now(),now()) 
					ON DUPLICATE KEY UPDATE 
						Mode = \''.$mode.'\', DateModified = now()';
	$Result['UpdateClassSpecificMode'] = $li->db_db_query($sql);
	
	if ($mode != 1)
	{
	    # Clear detail timetable
	    $sql = "DELETE FROM CARD_STUDENT_CLASS_PERIOD_TIME
	                   WHERE ClassID = '$ClassID'";
	    # Clear special timetable
	    $sql2= "DELETE FROM CARD_STUDENT_SPECIFIC_DATE_TIME WHERE ClassID='$ClassID' ";
	    $Result['DeleteOldPeriodTime'] = $li->db_db_query($sql);
	    $Result['DeleteOldSpecificDateTime'] = $li->db_db_query($sql2);
	}
	else
	{
	    // do nothing
	}
	
	$success = false;
	if (in_array(false,$Result)) {
		$li->RollBack_Trans();
	}
	else {
		$li->Commit_Trans();
		$success = true;
	}
	return $success;
}

intranet_closedb();
header("Location: $url?ClassID=$ClassID&Msg=".urlencode($Msg)."&showAll=$showAll");
?>