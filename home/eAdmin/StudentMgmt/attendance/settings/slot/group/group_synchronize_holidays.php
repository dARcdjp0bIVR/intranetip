<?php
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}
$today = date('Y-m-d',time());

$nonSchoolDay=1;

$li = new libdb();
$li->Start_Trans();

$sqlh="Select 
								DATE_FORMAT(ie.EventDate,'%Y-%m-%d')								
						From 
								INTRANET_EVENT as ie
						Where 
								ie.RecordType In (3, 4)	and	ie.EventDate>='$today'					
				";

$holiday_array=$li->returnArray($sqlh);

	for ($i=0; $i<sizeof($holiday_array); $i++) {
		list($holiday_date) = $holiday_array[$i];		
		$sql = "INSERT IGNORE INTO CARD_STUDENT_SPECIFIC_DATE_TIME_GROUP (
							GroupID,
							RecordDate,
							MorningTime,
							LunchStart,
							LunchEnd,
							LeaveSchoolTime,
							NonSchoolDay,
							DateInput,
							DateModified) 
						VALUES (
							'".$ClassID."',
							'".$holiday_date."',
							'00:00:00',
							'00:00:00',
							'00:00:00',
							'00:00:00',
							'".$nonSchoolDay."',
							now(),
							now()) 
				        on duplicate key update 						
							DateModified = NOW()
			                 ";
			$Result['InsertGroupSpecificDateTime-GroupID:'.$ClassID.'-TargetDate:'.$holiday_date] = $li->db_db_query($sql);
		    $count_rows +=$li->db_affected_rows(); 
		}

if ($count_rows==0){
    $li->Commit_Trans();
	$Msg = $Lang['StudentAttendance']['SettingApplySuccess'];
}
else{
if (in_array(false,$Result)) {
	$li->RollBack_Trans();
	$Msg = $Lang['StudentAttendance']['SettingApplyFail'];
}
else {
	$li->Commit_Trans();
	$Msg = $Lang['StudentAttendance']['SettingApplySuccess'];
}}

intranet_closedb();
header("Location: group_edit.php?ClassID=$ClassID&Msg=".urlencode($Msg));
?>