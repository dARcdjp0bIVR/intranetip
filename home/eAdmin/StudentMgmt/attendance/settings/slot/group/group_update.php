<?php
## Using By : 
##########################################################
## Modification Log
## 2009-12-04: Max (200912041605)
## - support multiple classes/groups change
##########################################################

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
intranet_opendb();

$lc = new libcardstudentattend2();

$ClassIDsArray = $_POST['ClassIDs'];
$showAll = $_POST['showAll'];

$updateSuccess = true;
$url = "index.php";
if (is_array($ClassIDsArray) && count($ClassIDsArray) > 0) {
	foreach($ClassIDsArray as $key => $ClassID) {
		$updateSuccess *= updateRecords($mode, $ClassID);
	}
	$url = "index.php";
} else {
	$updateSuccess = updateRecords($mode, $ClassID);
	if ($mode != 1)
	{ 
		$url = "group.php";
	}
	else
	{
	    $url = "group_edit.php";
	}
}

if ($updateSuccess) {
	$Msg = $Lang['StudentAttendance']['SettingApplySuccess'];
} else {
	$Msg = $Lang['StudentAttendance']['SettingApplyFail'];
}

function updateRecords($mode, $ClassID) {
	if ($mode != 1 && $mode != 2)
	{
	    $mode = 0;
	}
	$li = new libdb();
	$sql = "INSERT INTO CARD_STUDENT_ATTENDANCE_GROUP (GROUPID, Mode, DateInput, DateModified)
	               VALUES ('$ClassID','$mode',now(),now())";
	$Result['InsertGroupAttendance'] = $li->db_db_query($sql);
	
	if ($li->db_affected_rows()!=1)
	{
	    $sql = "UPDATE CARD_STUDENT_ATTENDANCE_GROUP
	                   SET Mode = '$mode', DateModified = now() WHERE GROUPID = '$ClassID'";
	    $Result['InsertGroupAttendance'] = $li->db_db_query($sql);
	}
	
	if ($mode != 1)
	{
	    # Clear detail timetable
	    $sql = "DELETE FROM CARD_STUDENT_GROUP_PERIOD_TIME
	                   WHERE GROUPID = '$ClassID'";
	    # Clear special timetable
	    $sql2= "DELETE FROM CARD_STUDENT_SPECIFIC_DATE_TIME_GROUP WHERE GROUPID='$ClassID' ";
	    $Result['DeleteOldPeriodTime'] = $li->db_db_query($sql);
	    $Result['DeleteOldSpecificDateTime'] = $li->db_db_query($sql2);
	}
	else
	{
	    // do nothing
	}
	$success = false;
	if (in_array(false,$Result)) {
		$li->RollBack_Trans();
	}
	else {
		$li->Commit_Trans();
		$success = true;
	}
	return $success;
}

intranet_closedb();
header("Location: $url?ClassID=$ClassID&Msg=".urlencode($Msg)."&showAll=$showAll");
?>