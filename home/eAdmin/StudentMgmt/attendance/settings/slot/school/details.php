<?php
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();
$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageSystemSetting_TimeSlotSettings";

$linterface = new interface_html();

$lc->retrieveSettings();
$hasAM = ($lc->attendance_mode != 1);
$hasLunch = ($lc->attendance_mode != 0 && $lc->attendance_mode != 1);
$hasPM = ($lc->attendance_mode != 0);

$field=$field==""?"RecordDate":$field;
$orderBy=$order==1?"asc":"desc";
$filter = trim($viewby);
$filter=$filter==""?0:$filter;
switch($filter){
	case 0: $condition = "";break;
	case 1: $condition = " AND RecordDate>='".date('Y-m-d',time())."'"; break;
	case 2: $condition = " AND RecordDate<'".date('Y-m-d',time())."'";break;
	default:$condition ="";
}

$today_ts = strtotime(date('Y-m-d'));
if($FromDate=="")
	$FromDate = date('Y-m-d',getStartOfAcademicYear($today_ts));
if($ToDate=="")
	$ToDate = date('Y-m-d',getEndOfAcademicYear($today_ts));

$condition2= " AND RecordDate BETWEEN '$FromDate' AND '$ToDate' ";
$sql=" SELECT RecordDate,IF(NonSchoolDay=1,'--',MorningTime) AS MorningTime,IF(NonSchoolDay=1,'--',LunchStart) AS LunchStart,IF(NonSchoolDay=1,'--',LunchEnd) AS LunchEnd,IF(NonSchoolDay=1,'--',LeaveSchoolTime) AS LeaveSchoolTime,NonSchoolDay FROM CARD_STUDENT_SPECIFIC_DATE_TIME  WHERE ClassID=0 $condition $condition2 order by $field $orderBy";

$specialResults = $lc->returnArray($sql,6);

## select view
$array_view_name = array($i_status_all, $i_StudentAttendance_TodayOnward, $i_StudentAttendance_Past);
$array_view_data = array("0", "1", "2");
$select_view = getSelectByValueDiffName($array_view_data,$array_view_name,"name='viewby' onChange='javascript:viewBy(this.form)'",$filter,0,1);

$filterbar = $select_view;

$table_content = "<table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">";
$table_content.="<tr class=\"tablebluetop\">";
$table_content.="<td width='1' class='tabletoplink'>#</td>\n";
$table_content.="<td class=\"tabletoplink\">$i_StaffAttendance_Field_RecordDate</td>\n";
if($hasAM)
	$table_content.="<td class=\"tabletoplink\">$i_StudentAttendance_SetTime_AMStart</td>\n";
if($hasLunch)
	$table_content.="<td class=\"tabletoplink\">$i_StudentAttendance_SetTime_LunchStart</a></td>\n";
if($hasPM)
	$table_content.="<td class=\"tabletoplink\">$i_StudentAttendance_SetTime_PMStart</a></td>\n";
$table_content.="<td class=\"tabletoplink\">$i_StudentAttendance_SetTime_SchoolEnd</a></td>\n";


$table_content.="</tr>\n";

if (sizeof($specialResults) == 0){
	$table_content.="<tr class=\"tablebluerow2\"><td colspan=\"6\" align=\"center\" height=\"40\" class=\"tabletext\">$i_no_record_exists_msg</td></tr>\n";
}else{
	for($i=0;$i<sizeof($specialResults);$i++){
		list($recordDate,$am,$lunch,$pm,$leave,$off) = $specialResults[$i];
		$css = $i%2==0?"tablerow1 tabletext":"tablerow2 tabletext";
		$table_content .= "<tr class=\"$css\">";
		$table_content .= "<td class=\"tabletext\">".($i+1)."</td>\n";
		$table_content .= "<td class=\"tabletext\">$recordDate</td>\n";
		if($off==1){
				$table_content.="<td class=\"tabletext\" colspan=\"4\">$i_StudentAttendance_NonSchoolDay</td></tr>\n";
				continue;
		}else{
				if($hasAM)
					$table_content.="<td class=\"tabletext\">$am</td>\n";
				if($hasLunch)
					$table_content.="<td class=\"tabletext\">$lunch</td>\n";
				if($hasPM)
					$table_content.="<td class=\"tabletext\">$pm</td>\n";
				$table_content.="<td class=\"tabletext\">$leave</td>\n";
		}
		$table_content.="</tr>\n";
	}
}
$table_content.="</table>\n";

$TAGS_OBJ[] = array($i_StudentAttendance_Menu_Slot_School, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/settings/slot/school/", 1);
$TAGS_OBJ[] = array($i_StudentAttendance_Menu_Slot_Class, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/settings/slot/class/", 0);
$TAGS_OBJ[] = array($i_StudentAttendance_Menu_Slot_Group, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/settings/slot/group/", 0);
$MODULE_OBJ = $lc->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

$PAGE_NAVIGATION[] = array($i_StudentAttendance_SpecialDay);

$PAGE_NAVIGATION1 =  $i_Profile_SelectSemester;

if ($msg == 1) $SysMsg = $linterface->GET_SYS_MSG("add");
if ($msg == 2) $SysMsg = $linterface->GET_SYS_MSG("update");
if ($msg == 3) $SysMsg = $linterface->GET_SYS_MSG("delete");
?>
<script language="javascript">
function viewBy(formObj){
	if(checkForm(formObj))
		formObj.submit();
}

function checkForm(formObj){
	fromV = formObj.FromDate;
	toV= formObj.ToDate;
	if(!checkDate(fromV)){
			//formObj.FromDate.focus();
			return false;
	}
	else if(!checkDate(toV)){
				//formObj.ToDate.focus();
				return false;
	}
		return true;
}

function checkDate(obj){
	if(!check_date(obj,"<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>")) return false;
		return true;
}

function sortByField(f){
	orderOld = document.form1.order.value;
	fieldOld = document.form1.field.value;
	if(fieldOld ==f) {
		orderNew = orderOld==1?2:1;
	} else
		orderNew = orderOld;
	document.form1.field.value = f;
	document.form1.order.value = orderNew;
	if(checkForm(document.form1))
		document.form1.submit();
}

function submitForm(obj) {
	if(checkForm(obj))
		obj.submit();	
}
</script>
<br />
<form name="form1" method="GET" onSubmit="return checkForm(this)">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	</tr>
	<tr>
    	<td>
    		<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
    			<tr>
    				<td><?= $linterface->GET_NAVIGATION2($PAGE_NAVIGATION1) ?></td>
    			</tr>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_Profile_From?></td>
					<td class="tabletext" width="70%"><?= $linterface->GET_DATE_PICKER("FromDate", $FromDate)?></td>
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_Profile_To?></td>
					<td class="tabletext" width="70%"><?= $linterface->GET_DATE_PICKER("ToDate", $ToDate)?></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
    	<td>
		    <table width="96%" border="0" cellpadding="3" cellspacing="0" align="center">
		    	<tr>
                	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
		    </table>
	    </td>
	</tr>
    <tr>
    	<td>
	    	<table width="96%" border="0" cellpadding="3" cellspacing="0" align="center">
			    <tr>
				    <td align="center">
						<?= $linterface->GET_ACTION_BTN($button_submit, "button", "submitForm(document.form1)") ?>
						<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
						<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:window.location = 'index.php';","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
	    <td>
	    	<table width="95%" border="0" cellpadding="0" cellspacing="0" align="center">
				<tr>
					<td align="left"><?=$filterbar?></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
	    <td>
	    	<?=$table_content?>
		</td>
	</tr>
</table>
<input type="hidden" name="field" value="<?=$field?>">
<input type="hidden" name="order" value="<?=$order?>">
</form>
<br />
<?
$linterface->LAYOUT_STOP();
print $linterface->FOCUS_ON_LOAD("form1.FromDate");
intranet_closedb();
?>