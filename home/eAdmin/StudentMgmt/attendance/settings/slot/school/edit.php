<?php
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();


if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageSystemSetting_TimeSlotSettings";
$linterface = new interface_html();

$lc = new libcardstudentattend2();
$lc->retrieveSettings();
/*
$normal_time_array = $lc->getTimeArray(0,0);
list($normal_am, $normal_lunch, $normal_pm, $normal_leave) = $normal_time_array;
*/

/*
$preset_time_array = $lc->getTimeArray($type,$value);
list($preset_am, $preset_lunch, $preset_pm, $preset_leave) = $preset_time_array;
*/

if($type==1 || $type==2 || $type==0){
	$preset_time_array = $lc->getTimeArray($type,$value);
	list($preset_am, $preset_lunch, $preset_pm, $preset_leave,$non_school_day) = $preset_time_array;
}else{
	if($TargetDate<date('Y-m-d')){
		echo "<br />";
		echo "<table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">";
		echo "<tr><td class=\"tabletext\">$i_StudentAttendance_Warn_Cannot_Modify_Past_Records</td></tr>";
		echo "</table>";
		echo "<table width=\"96%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" align=\"center\">
				<tr>
    				<td class=\"dotline\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"10\" height=\"1\" /></td>
				</tr>
			</table>";
		echo "<table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">
			    <tr>
				    <td align=\"center\">".
						$linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:window.location = 'index.php#$type'","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"").
					"</td>
				</tr>
			</table>";
		$linterface->LAYOUT_STOP();
		intranet_closedb();
		exit;
	}
	$value = $TargetDate;
	$preset_time_array=$lc->getClassSpecialTimeArray(0,$value);
	list($record_date,$preset_am,$preset_lunch,$preset_pm,$preset_leave,$non_school_day)=$preset_time_array;
}
$hasAM = ($lc->attendance_mode != 1);
$hasLunch = ($lc->attendance_mode != 0 && $lc->attendance_mode != 1);
$hasPM = ($lc->attendance_mode != 0);
$days = $lc->getDayValueWithoutSpecific($type);

if ($type==2)  # Cycle
{
    $select_title = $i_StudentAttendance_CycleDay;
    $word = $value;
}
else if($type==1) # Week
{
    $select_title = $i_StudentAttendance_WeekDay;
    $word = $i_DayType0[$value];
}else if($type==3){ # Special
    $select_title = "$i_StudentAttendance_TimeSlot_SpecialDay";
    $word = $value;
}

$TAGS_OBJ[] = array($i_StudentAttendance_Menu_Slot_School, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/settings/slot/school/", 1);
$TAGS_OBJ[] = array($i_StudentAttendance_Menu_Slot_Class, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/settings/slot/class/", 0);
$TAGS_OBJ[] = array($i_StudentAttendance_Menu_Slot_Group, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/settings/slot/group/", 0);
$MODULE_OBJ = $lc->GET_MODULE_OBJ_ARR();

$PAGE_NAVIGATION[] = array($button_edit);

$Msg = urldecode($Msg);
$linterface->LAYOUT_START($Msg);
?>
<script language="javascript">
	<!--
function checkForm(formObj){
	if(formObj.non_school_day!=null && formObj.non_school_day.checked==true)
			return true;

	amObj = formObj.preset_am;
	lunchObj=formObj.preset_lunch;
	pmObj = formObj.preset_pm;
	leaveObj = formObj.preset_leave;

	if(amObj!=null){
		am = amObj.value;
		if(!isValidTimeFormat(am)){
			amObj.focus();
			return false;
		}
	}
	if(lunchObj!=null){
		lunch = lunchObj.value;
		if(!isValidTimeFormat(lunch)){
			lunchObj.focus();
			return false;
		}
	}
	if(pmObj!=null){
		pm = pmObj.value;
		if(!isValidTimeFormat(pm)){
			pmObj.focus();
			return false;
		}
	}
	if(leaveObj!=null){
		leave = leaveObj.value;
		if(!isValidTimeFormat(leave)){
			leaveObj.focus();
			return false;
		}
	}
	if(isValidValues(amObj,lunchObj,pmObj,leaveObj))
		return true;
	return false;
	}
function isValidTimeFormat(timeVal){
  	var re = new RegExp("^(([0-1][0-9])|2[0-3]):[0-5][0-9]$");
		if (!timeVal.match(re)) {
				alert("<?=$i_StudentAttendance_Warn_Invalid_Time_Format?>");
				return false;
		}
		return true;
}

function isValidValues(amObj,lunchObj,pmObj,leaveObj){
			if(amObj!=null){
					if(lunchObj!=null && amObj.value>=lunchObj.value){ 
						alert("<?=$i_StudentAttendance_Warn_AM_Must_Smaller_Than_LunchStart?>");
						amObj.focus();
						return false;
					}
					else if(pmObj!=null && amObj.value>=pmObj.value){
						alert("<?=$i_StudentAttendance_Warn_AM_Must_Smaller_Than_PMStart?>");
						amObj.focus();
						return false;
					}
					else if(leaveObj!=null && amObj.value>=leaveObj.value){
						alert("<?=$i_StudentAttendance_Warn_AM_Must_Smaller_Than_SchoolEnd?>");
						amObj.focus();
						return false;
					}
			}
			if(lunchObj!=null){
					if(pmObj!=null && lunchObj.value>=pmObj.value){
						alert("<?=$i_StudentAttendance_Warn_LunchStart_Must_Smaller_Than_PMStart?>");
						lunchObj.focus();
						return false;
					}
					else if(leaveObj!=null && lunchObj.value>=leaveObj.value){
						alert("<?=$i_StudentAttendance_Warn_LunchStart_Must_Smaller_Than_SchoolEnd?>");
						lunchObj.focus();
						return false;
					}
			}
			if(pmObj!=null){
					if(leaveObj!=null && pmObj.value>=leaveObj.value){
							alert("<?=$i_StudentAttendance_Warn_PMStart_Must_Smaller_Than_SchoolEnd?>");
							pmObj.focus();
							return false;
					}
			}
			return true;
}

	function setForm(formObj){
		nonSchoolDayObj = formObj.non_school_day;
		amObj = formObj.preset_am;
		lunchObj=formObj.preset_lunch;
		pmObj = formObj.preset_pm;
		leaveObj = formObj.preset_leave;
		if(nonSchoolDayObj!=null){
			if(nonSchoolDayObj.checked){
				if(amObj!=null) amObj.disabled=true;
				if(lunchObj!=null) lunchObj.disabled=true;
				if(pmObj!=null) pmObj.disabled=true;
				if(leaveObj!=null) leaveObj.disabled=true;
			}else{
				if(amObj!=null) amObj.disabled=false;
				if(lunchObj!=null) lunchObj.disabled=false;
				if(pmObj!=null) pmObj.disabled=false;
				if(leaveObj!=null) leaveObj.disabled=false;
			}
		}
	}
	function resetForm(formObj){
			formObj.reset();
			setForm(formObj);
	}
	// -->
	</script>
<form name="form1" method="POST" ACTION="edit_update.php" onSubmit="return checkForm(this)">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	</tr>
	<tr>
    	<td>
    		<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$select_title?></td>
					<td class="tabletext" width="70%"><?=$word?></td>
				</tr>
				<?
				### Mode 1,3,4
				if ($hasAM) {
				?>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_StudentAttendance_SetTime_AMStart?></td>
					<td class="tabletext" width="70%"><input type="text" class="textboxnum" <?=($non_school_day==1?"disabled=\"true\"":"")?> maxlength="5" name="preset_am" value="<?=$preset_am?>"></td>
				</tr>
				<? } 
				### Mode 3,4
				if ($hasLunch) {
				?>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_StudentAttendance_SetTime_LunchStart?></td>
					<td class="tabletext" width="70%"><input type="text" class="textboxnum" <?=($non_school_day==1?"disabled=\"true\"":"")?> maxlength="5" name="preset_lunch" value="<?=$preset_lunch?>"></td>
				</tr>
				<? } 
				### Mode 2,3,4
				if ($hasPM) {
				?>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_StudentAttendance_SetTime_PMStart?></td>
					<td class="tabletext" width="70%"><input type="text" class="textboxnum" <?=($non_school_day==1?"disabled=\"true\"":"")?> maxlength="5" name="preset_pm" value="<?=$preset_pm?>"></td>
				</tr>
				<? } 
				### Mode 1,2,3,4
				?>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_StudentAttendance_SetTime_SchoolEnd?></td>
					<td class="tabletext" width="70%"><input type="text" class="textboxnum" <?=($non_school_day==1?"disabled=\"true\"":"")?> maxlength="5" name="preset_leave" value="<?=$preset_leave?>"></td>
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_StudentAttendance_NonSchoolDay?></td>
					<td class="tabletext" width="70%"><input type="checkbox" onClick="setForm(this.form)" name="non_school_day" <?=($non_school_day==1?"CHECKED":"")?>></td>
				</tr>
				<tr>
					<td class="tabletextremark" colspan="2"><br /><?=$i_StudentAttendance_Slot_SettingsDescription?></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
    	<td>
		    <table width="96%" border="0" cellpadding="0" cellspacing="0" align="center">
		    	<tr>
                	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
		    </table>
	    </td>
	</tr>
    <tr>
	    <td align="center" colspan="2">
			<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "") ?>
			<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
			<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:history.back()","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
		</td>
	</tr>
</table>
<input type=hidden name=DayType value="<?=$type?>">
<input type=hidden name=DayValue value="<?=$value?>">
</form>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>