<?php
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$nonSchoolDay=($non_school_day==""||$non_school_day==0)?0:1;
$li = new libcardstudentattend2();
$li->Start_Trans();
if($DayType!=3){
		$sql = "INSERT IGNORE INTO CARD_STUDENT_PERIOD_TIME
		(DayType, DayValue, MorningTime, LunchStart, LunchEnd, LeaveSchoolTime, NonSchoolDay,DateInput, DateModified)
		VALUES ('$DayType','$DayValue','$normal_am','$normal_lunch','$normal_pm','$normal_leave','$nonSchoolDay',now(),now())";
		$Result['InsertPeriodTime'] = $li->db_db_query($sql);
		
		if ($li->db_affected_rows()==0)
		{
		    $sql = "UPDATE CARD_STUDENT_PERIOD_TIME
		                   SET MorningTime = '$normal_am', LunchStart = '$normal_lunch',
		                       LunchEnd = '$normal_pm', LeaveSchoolTime = '$normal_leave',
		                       NonSchoolDay='$nonSchoolDay',
		                       DateModified= now()
		                   WHERE DayType='$DayType' AND DayValue ='$DayValue'";
		    $Result['UpdatePeriodTime'] = $li->db_db_query($sql);
		}
}
else{
	$TargetDate = $_REQUEST['target_date'];
	for ($i=0; $i< sizeof($TargetDate); $i++) {
		$sql = "INSERT IGNORE INTO CARD_STUDENT_SPECIFIC_DATE_TIME (
							ClassID,
							RecordDate,
							MorningTime,
							LunchStart,
							LunchEnd,
							LeaveSchoolTime,
							NonSchoolDay,
							DateInput,
							DateModified) 
						VALUES (
							0,
							'".$TargetDate[$i]."',
							'".$normal_am."',
							'".$normal_lunch."',
							'".$normal_pm."',
							'".$normal_leave."',
							'".$nonSchoolDay."',
							now(),
							now()) 
						on duplicate key update 
							MorningTime = '".$normal_am."', 
							LunchStart = '".$normal_lunch."',
			        LunchEnd = '".$normal_pm."', 
			        LeaveSchoolTime = '".$normal_leave."',
			        NonSchoolDay='".$nonSchoolDay."',
			        DateModified= now()";
		$Result['InsertSpecificDateTime'.$i] = $li->db_db_query($sql);
	}
}

if (in_array(false,$Result)) {
	$li->RollBack_Trans();
	$Msg = $Lang['StudentAttendance']['SettingApplyFail'];
}
else {
	$li->Commit_Trans();
	$Msg = $Lang['StudentAttendance']['SettingApplySuccess'];
}

intranet_closedb();
header("Location: index.php?Msg=".urlencode($Msg));
?>