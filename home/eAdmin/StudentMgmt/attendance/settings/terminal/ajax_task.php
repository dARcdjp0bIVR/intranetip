<?php
// Editing by 
/*
 * 2015-05-14 (Carlos): Created for monitor terminal function.
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	//header ("Location: /");
	intranet_closedb();
	exit();
}

$lcardstudentattend2 = new libcardstudentattend2();
$GeneralSetting = new libgeneralsettings();
$linterface = new interface_html();

$task = $_REQUEST['task'];

function getTimeSelection($ID_Name, $DefaultTime='', $others_tab='')
{
	global $linterface;
	list($hr, $min) = explode(":", $DefaultTime);
	if(!is_array($others_tab))
	{
		$tmp = $others_tab;
		$others_tab = array();
		for($i=0; $i<2; $i++) $others_tab[$i] = $tmp;			
	}
	
	$time_sel[] = $linterface->Get_Time_Selection_Box($ID_Name."_hour", "hour", $hr, $others_tab[0]);
	$time_sel[] = $linterface->Get_Time_Selection_Box($ID_Name."_min", "min", $min, $others_tab[1]);
	
	return implode(":", $time_sel);
}

switch($task)
{
	case 'getTerminalTimeSlotTable':
	
	$TerminalPingTimeSlotAry = array();
	if($reload){
		$time_slot = $_REQUEST['TimeSlot'];
	}else{
		$SettingList[] = "'TerminalPingTimeSlot'"; // hh:mm:ss,hh:mm:ss|hh:mm:ss,hh:mm:ss|hh:mm:ss,hh:mm:ss
		$Settings = $GeneralSetting->Get_General_Setting('StudentAttendance',$SettingList);
		$time_slot = trim($Settings['TerminalPingTimeSlot']);
	}
	if($time_slot != '')
	{
		$time_slot_ary = explode("|", $time_slot);
		sort($time_slot_ary);
		for($i=0;$i<count($time_slot_ary);$i++){
			$time_parts = explode(",",$time_slot_ary[$i]);
			$TerminalPingTimeSlotAry[] = array($time_parts[0], $time_parts[1]);
		}
	}
	
	$TerminalPingTimeSlotAryCount = count($TerminalPingTimeSlotAry);
	
	$x = '<table class="common_table_list" id="TerminalTimeSlotTable">
			<thead>
				<tr>
					<th width="45%">'.$Lang['SysMgr']['Timetable']['StartTime'].'</th>
					<th width="45%">'.$Lang['SysMgr']['Timetable']['EndTime'].'</th>
					<th width="10%"><span class="table_row_tool"><a class="add" href="javascript:void(0);" onclick="getTerminalTimeSlotEditForm(null);" title="'.$Lang['Btn']['Add'].'"></a></span></th>
				</tr>
			</thead>
			<tbody>';
	if($TerminalPingTimeSlotAryCount == 0){
		$x .= '<tr>
					<td colspan="3" align="center">'.$Lang['General']['NoRecordAtThisMoment'].'</td>
				</tr>';
	}else{
		for($i=0;$i<$TerminalPingTimeSlotAryCount;$i++)
		{	
			$x .= '<tr>
						<td>'.$TerminalPingTimeSlotAry[$i][0].'</td>
						<td>'.$TerminalPingTimeSlotAry[$i][1].'</td>
						<td>
							<input type="hidden" name="StartTime[]" value="'.$TerminalPingTimeSlotAry[$i][0].'" />
							<input type="hidden" name="EndTime[]" value="'.$TerminalPingTimeSlotAry[$i][1].'" />
							<span class="table_row_tool">
								<a class="edit" href="javascript:void(0);" onclick="getTerminalTimeSlotEditForm($(this).closest(\'tr\'));" title="'.$Lang['Btn']['Edit'].'"></a>
								<a class="delete" href="javascript:void(0);" onclick="removeTerminalTimeSlot($(this).closest(\'tr\'));" title="'.$Lang['Btn']['Delete'].'"></a>
							</span>
						</td>
					</tr>';
		}
	}
	$x .= '</tbody>
		</table>';
	
	echo $x;
	
	break;
	
	case 'getTerminalTimeSlotEditForm':
		$start_time = $_REQUEST['StartTime'];
		$end_time = $_REQUEST['EndTime'];
		//$start_time_edit = $linterface->Get_Time_Selection("StartTimeEdit", $start_time, '');
		//$end_time_edit = $linterface->Get_Time_Selection("EndTimeEdit", $end_time, '');
		
		$start_time_edit = getTimeSelection("StartTimeEdit", $start_time, '');
		$end_time_edit = getTimeSelection("EndTimeEdit", $end_time, '');
		
		$x .= '<div class="edit_pop_board" style="height:395px;">
				<div class="edit_pop_board_write" style="height:340px;">
					<form name="EditForm" id="EditForm" onsubmit="return false;">				
					<div class="edit_pop_board_write" id="EditLayer" name="EditLayer" style="height:300px;">
						<table class="form_table">
							<col class="field_title" />
							<col class="field_c" />
							<tr>
								<td>' .$Lang['SysMgr']['Timetable']['StartTime'] . '</td>
								<td>:</td>
								<td>
									' . $start_time_edit . '&nbsp;<span class="tabletextremark">(hh:mm)</span>&nbsp;<span id="StartTimeEditWarnLayer" style="display:none;color:red;"></span>
								</td> 
							</tr>
							<tr>
								<td>' . $Lang['SysMgr']['Timetable']['EndTime'] . '</td>
								<td>:</td>
								<td>
									' . $end_time_edit . '&nbsp;<span class="tabletextremark">(hh:mm)</span><br /><span id="EndTimeEditWarnLayer" style="display:none;color:red;"></span>
								</td> 
							</tr>
						</table>
						
					</div>
					</form>
				</div>
				<div class="edit_bottom" style="height:10px;">
					<p class="spacer"></p>
					<input name="submitEditTimeSlotBtn" type="button" class="formbutton" onclick="updateTerminalTimeSlot();" onmouseover="this.className=\'formbuttonon\'" onmouseout="this.className=\'formbutton\'" value="' . $Lang['Btn']['Submit'] . '" />
					<input name="cancelEditTimeSlotBtn" type="button" class="formbutton" onclick="window.top.tb_remove();" onmouseover="this.className=\'formbuttonon\'" onmouseout="this.className=\'formbutton\'" value="' . $Lang['Btn']['Cancel'] . '" />
				</div>
			</div>';
			
		echo $x;
	break;
}

?>