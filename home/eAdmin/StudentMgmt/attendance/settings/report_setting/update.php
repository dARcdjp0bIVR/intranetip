<?
//editing by 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$db = new libdb();

//$StatusNormal = str_replace("'","\'",trim(stripslashes(urldecode($StatusNormal))));
//$StatusAbsent = str_replace("'","\'",trim(stripslashes(urldecode($StatusAbsent))));
//$StatusLate = str_replace("'","\'",trim(stripslashes(urldecode($StatusLate))));
//$StatusEarlyLeave = str_replace("'","\'",trim(stripslashes(urldecode($StatusEarlyLeave))));
//$StatusWaived = str_replace("'","\'",trim(stripslashes(urldecode($StatusWaived))));
//$StatusNormal = addslashes(trim(stripslashes(urldecode($StatusNormal))));
$StatusNormal = (stristr($StatusNormal,"+") === false)? addslashes(trim(stripslashes(urldecode($StatusNormal)))):addslashes(trim(stripslashes($StatusNormal)));
//$StatusAbsent = addslashes(trim(stripslashes(urldecode($StatusAbsent))));
$StatusAbsent = (stristr($StatusAbsent,"+") === false)? addslashes(trim(stripslashes(urldecode($StatusAbsent)))):addslashes(trim(stripslashes($StatusAbsent)));
//$StatusLate = addslashes(trim(stripslashes(urldecode($StatusLate))));
$StatusLate = (stristr($StatusLate,"+") === false)? addslashes(trim(stripslashes(urldecode($StatusLate)))):addslashes(trim(stripslashes($StatusLate)));
//$StatusEarlyLeave = addslashes(trim(stripslashes(urldecode($StatusEarlyLeave))));
$StatusEarlyLeave = (stristr($StatusEarlyLeave,"+") === false)? addslashes(trim(stripslashes(urldecode($StatusEarlyLeave)))):addslashes(trim(stripslashes($StatusEarlyLeave)));
//$StatusWaived = addslashes(trim(stripslashes(urldecode($StatusWaived))));
$StatusWaived = (stristr($StatusWaived,"+") === false)? addslashes(trim(stripslashes(urldecode($StatusWaived)))):addslashes(trim(stripslashes($StatusWaived)));

$AbsentNoneDeleteReasonsArr = array();
$LateNoneDeleteReasonsArr = array();
$EarlyLeaveNoneDeleteReasonsArr = array();

$result = array();

for($i=0;$i<sizeof($AbsentReasons);$i++)
{
	$AbsentReasons[$i] = addslashes(trim(stripslashes(urldecode($AbsentReasons[$i]))));
	$AbsentSymbols[$i] = (stristr($AbsentSymbols[$i],"+") === false)? addslashes(trim(stripslashes(urldecode($AbsentSymbols[$i])))):addslashes(trim(stripslashes($AbsentSymbols[$i])));
	if($AbsentReasons[$i] != "" && $AbsentSymbols[$i] != "")
	{
		$AbsentNoneDeleteReasonsArr[] = "'".$AbsentReasons[$i]."'";
		$sql = "REPLACE INTO CARD_STUDENT_ATTENDANCE_REASON_SYMBOL (ReasonType, Reason, StatusSymbol, CreateDate, LastModified) ";
		$sql .= "VALUES('1','".$AbsentReasons[$i]."','".$AbsentSymbols[$i]."',NOW(),NOW()) ";
		//$sql .= " ON DUPLICATE KEY UPDATE StatusSymbol='".$AbsentSymbols[$i]."', LastModified = NOW()";
		$result[] = $db->db_db_query($sql);
	}
}
if(sizeof($AbsentNoneDeleteReasonsArr)>0)
{
	$AbsentNoneDeleteReasons = implode(",",$AbsentNoneDeleteReasonsArr);
	$sql = "DELETE FROM CARD_STUDENT_ATTENDANCE_REASON_SYMBOL WHERE ReasonType = '1' AND Reason NOT IN (".$AbsentNoneDeleteReasons.")";
	$result[] = $db->db_db_query($sql);
}else if(sizeof($AbsentReasons)==0)
{
	$sql = "DELETE FROM CARD_STUDENT_ATTENDANCE_REASON_SYMBOL WHERE ReasonType = '1' ";
	$result[] = $db->db_db_query($sql);
}

for($i=0;$i<sizeof($LateReasons);$i++)
{
	$LateReasons[$i] = addslashes(trim(stripslashes(urldecode($LateReasons[$i]))));
	$LateSymbols[$i] = (stristr($LateSymbols[$i],"+") === false)? addslashes(trim(stripslashes(urldecode($LateSymbols[$i])))):addslashes(trim(stripslashes($LateSymbols[$i])));
	if($LateReasons[$i] != "" && $LateSymbols[$i] != "")
	{
		$LateNoneDeleteReasonsArr[] = "'".$LateReasons[$i]."'";
		$sql = "REPLACE INTO CARD_STUDENT_ATTENDANCE_REASON_SYMBOL (ReasonType, Reason, StatusSymbol, CreateDate, LastModified) ";
		$sql .= "VALUES('2','".$LateReasons[$i]."','".$LateSymbols[$i]."',NOW(),NOW()) ";
		//$sql .= " ON DUPLICATE KEY UPDATE StatusSymbol = '".$LateSymbols[$i]."'";
		$result[] = $db->db_db_query($sql);
	}
}
if(sizeof($LateNoneDeleteReasonsArr)>0)
{
	$LateNoneDeleteReasons = implode(",",$LateNoneDeleteReasonsArr);
	$sql = "DELETE FROM CARD_STUDENT_ATTENDANCE_REASON_SYMBOL WHERE ReasonType = '2' AND Reason NOT IN (".$LateNoneDeleteReasons.")";
	$result[] = $db->db_db_query($sql);
}else if(sizeof($LateReasons)==0)
{
	$sql = "DELETE FROM CARD_STUDENT_ATTENDANCE_REASON_SYMBOL WHERE ReasonType = '2' ";
	$result[] = $db->db_db_query($sql);
}

for($i=0;$i<sizeof($EarlyLeaveReasons);$i++)
{
	$EarlyLeaveReasons[$i] = addslashes(trim(stripslashes(urldecode($EarlyLeaveReasons[$i]))));
	$EarlyLeaveSymbols[$i] = (stristr($EarlyLeaveSymbols[$i],"+") === false)? addslashes(trim(stripslashes(urldecode($EarlyLeaveSymbols[$i])))):addslashes(trim(stripslashes($EarlyLeaveSymbols[$i])));
	if($EarlyLeaveReasons[$i] != "" && $EarlyLeaveSymbols[$i] != "")
	{
		$EarlyLeaveNoneDeleteReasonsArr[] = "'".$EarlyLeaveReasons[$i]."'";
		$sql = "REPLACE INTO CARD_STUDENT_ATTENDANCE_REASON_SYMBOL (ReasonType, Reason, StatusSymbol, CreateDate, LastModified) ";
		$sql .= "VALUES('3','".$EarlyLeaveReasons[$i]."','".$EarlyLeaveSymbols[$i]."',NOW(),NOW()) ";
		//$sql .= " ON DUPLICATE KEY UPDATE StatusSymbol = '".$EarlyLeaveSymbols[$i]."'";
		$result[] = $db->db_db_query($sql);
	}
}
if(sizeof($EarlyLeaveNoneDeleteReasonsArr)>0)
{
	$EarlyLeaveNoneDeleteReasons = implode(",",$EarlyLeaveNoneDeleteReasonsArr);
	$sql = "DELETE FROM CARD_STUDENT_ATTENDANCE_REASON_SYMBOL WHERE ReasonType = '3' AND Reason NOT IN (".$EarlyLeaveNoneDeleteReasons.")";
	$result[] = $db->db_db_query($sql);
}else if(sizeof($EarlyLeaveReasons)==0)
{
	$sql = "DELETE FROM CARD_STUDENT_ATTENDANCE_REASON_SYMBOL WHERE ReasonType = '3' ";
	$result[] = $db->db_db_query($sql);
}

$sql = "UPDATE CARD_STUDENT_ATTENDANCE_STATUS_SYMBOL SET StatusSymbol='$StatusNormal' WHERE StatusID=0 ";
$result[] = $db->db_db_query($sql);
$sql = "UPDATE CARD_STUDENT_ATTENDANCE_STATUS_SYMBOL SET StatusSymbol='$StatusAbsent' WHERE StatusID=1 ";
$result[] = $db->db_db_query($sql);
$sql = "UPDATE CARD_STUDENT_ATTENDANCE_STATUS_SYMBOL SET StatusSymbol='$StatusLate' WHERE StatusID=2 ";
$result[] = $db->db_db_query($sql);
$sql = "UPDATE CARD_STUDENT_ATTENDANCE_STATUS_SYMBOL SET StatusSymbol='$StatusEarlyLeave' WHERE StatusID=3 ";
$result[] = $db->db_db_query($sql);
$sql = "UPDATE CARD_STUDENT_ATTENDANCE_STATUS_SYMBOL SET StatusSymbol='$StatusWaived' WHERE StatusID=4 ";
$result[] = $db->db_db_query($sql);

intranet_closedb();
if(!in_array(false, $result))
{
	$Msg = $Lang['StudentAttendance']['SettingApplySuccess'];
	header("Location: index.php?Msg=2".urlencode($Msg));
}
else
{
	$Msg = $Lang['StudentAttendance']['SettingApplyFail'];
	header("Location: index.php?Msg=".urlencode($Msg));
}
?>