<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libstudentattendance_ui.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libwordtemplates.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-LessonAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent'] || !$sys_custom['PowerClass']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$linterface = new interface_html();
$lword = new libwordtemplates();

$LessonAttendUI = new libstudentattendance_ui();
$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageLessonAttendance_AttendanceWordTemplate";


### Title ###
$TAGS_OBJ[] = array($i_wordtemplates_attendance,"",0);
$MODULE_OBJ = $LessonAttendUI->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START($Msg);

$type = 0;
$base_dir = "$intranet_root/file/templates/";
if (!is_dir($base_dir))
{
	$lword->folder_new($base_dir);
}

$file_array = $lword->file_array;
$word_array = $lword->word_array;

$data = get_file_content($base_dir.$file_array[$type]);

echo $LessonAttendUI->Include_JS_CSS();
?>

<br />
<form name="form1" method="post" action="update.php">
    <table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
        <tr>
            <td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
				<?=$i_wordtemplates_attendance?>
            </td>
            <td class="tabletext" width="70%">
			<span class="tabletextremark">
				<?=$linterface->GET_TEXTAREA("worddata", $data, 60, 20)?>
            </td>
        </tr>
    </table>
    <table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
        <tr>
            <td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
        </tr>
        <tr>
            <td align="center">
				<?= $linterface->GET_ACTION_BTN($button_save, "submit", "") ?>
				<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
            </td>
        </tr>
    </table>
</form>


<?php
$linterface->LAYOUT_STOP();

intranet_closedb();

?>

