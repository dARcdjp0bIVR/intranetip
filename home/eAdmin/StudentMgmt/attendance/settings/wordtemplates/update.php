<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libwordtemplates.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-LessonAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent'] || !$sys_custom['PowerClass']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lword = new libwordtemplates();
$base_dir = "$intranet_root/file/templates/";
if (!is_dir($base_dir))
{
	$lword->folder_new($base_dir);
}

$file_array = $lword->file_array;
$type = 0;
if ($type >= sizeof($file_array) || $type < 0)
{
	$Msg = $Lang['StudentAttendance']['SettingApplyFail'];
}
else
{
	$result = explode("\n",$worddata);
	$temp = array();
	for ($i=0; $i<sizeof($result); $i++)
	{
		$str = trim($result[$i]);
		if(strlen($str) > 0) {
			$temp[] = $str;
		}
	}
	$worddata = implode("\n",$temp);
	$file_target = $file_array[$type];
	$data = stripslashes($worddata);
	$lword->file_write($data,"$base_dir$file_target");

	$Msg = $Lang['StudentAttendance']['SettingApplySuccess'];
}

header("Location: index.php?Msg=".urlencode($Msg));