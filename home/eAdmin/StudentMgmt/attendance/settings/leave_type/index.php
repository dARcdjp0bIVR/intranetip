<?php
/*
 * 2020-02-07 (Ray): Created.
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libstudentattendance_ui.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_PRIVILEGE"]["plugin"]['attendancestudent'] || !$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !($sys_custom['StudentAttendance']['LeaveType'] || get_client_region() == 'zh_TW')) {
	header ("Location: /");
	intranet_closedb();
	exit();
}


$LessonAttendUI = new libstudentattendance_ui();
$linterface = new interface_html();


$field = trim(urldecode(stripslashes($_REQUEST['field'])));
$order = trim(urldecode(stripslashes($_REQUEST['order'])));
$pageNo = trim(urldecode(stripslashes($_REQUEST['pageNo'])));
$numPerPage = trim(urldecode(stripslashes($_REQUEST['numPerPage'])));

$arrCookies[] = array("ck_student_attendance_leavetype_page_size", "numPerPage");
$arrCookies[] = array("ck_student_attendance_leavetype_page_number", "pageNo");
$arrCookies[] = array("ck_student_attendance_leavetype_page_order", "order");
$arrCookies[] = array("ck_student_attendance_leavetype_page_field", "field");
updateGetCookies($arrCookies);

if(!isset($field) || $field == '' || !in_array($field,array(1,2,3,4,5,6))) $field = 0;
if(!isset($order) || $order == '' || !in_array($order,array(0,1))) $order = 0;
if(!isset($pageNo) || $pageNo == '') $pageNo = 1;

$pageSizeChangeEnabled = true;
if (isset($ck_student_attendance_leavetype_page_size) && $ck_student_attendance_leavetype_page_size != "") $page_size = $ck_student_attendance_leavetype_page_size;
$li = new libdbtable2007($field,$order,$pageNo);


$modifier_name_field = getNameFieldByLang2("u.");

$sql = "SELECT 
			r.TypeSymbol,
			CONCAT('<a href=\"javascript:showModalForm(',r.TypeID,')\">',r.TypeName,'</a>') as TypeName,
			r.ModifiedDate,
			$modifier_name_field as ModifiedName,
			CONCAT('<input type=\"checkbox\" name=\"TypeID[]\" value=\"',r.TypeID,'\">') as CheckBox 
		FROM CARD_STUDENT_LEAVE_TYPE as r 
		LEFT JOIN INTRANET_USER as u ON r.ModifiedBy=u.UserID 
		WHERE 1 ";


$li->sql = $sql;
$li->IsColOff = "IP25_table";

$li->field_array = array("TypeSymbol","r.TypeName","r.ModifiedDate","ModifiedName");
$li->column_array = array(22,18,22,22);
$li->wrap_array = array(0,0,0,0,0);

$pos = 0;
$li->column_list .= "<th width=\"1\" class=\"num_check\">#</th>\n";
$li->column_list .= "<th>".$li->column($pos++, $Lang['StudentAttendance']['LeaveTypeCode'])."</th>\n";
$li->column_list .= "<th>".$li->column($pos++, $Lang['StudentAttendance']['LeaveTypeName'])."</th>\n";
$li->column_list .= "<th>".$li->column($pos++, $Lang['General']['LastUpdatedTime'])."</th>\n";
$li->column_list .= "<th>".$li->column($pos++, $Lang['General']['LastUpdatedBy'])."</th>\n";
$li->column_list .= "<th width=\"1\">".$li->check("TypeID[]")."</th>\n";
$li->no_col = $pos+2;


$tool_buttons = array();
$tool_buttons[] = array('delete',"javascript:checkRemove2(document.form1,'TypeID[]','deleteType();')");

$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageSystemSetting_LeaveType";

### Title ###
$TAGS_OBJ[] = array($Lang['StudentAttendance']['LeaveType'],"",0);
$MODULE_OBJ = $LessonAttendUI->GET_MODULE_OBJ_ARR();
//$Msg = urldecode($Msg);
if(isset($_SESSION['STUDENT_ATTENDANCE_LEAVETYPE_RETURN_MSG']))
{
	$Msg = $_SESSION['STUDENT_ATTENDANCE_LEAVETYPE_RETURN_MSG'];
	unset($_SESSION['STUDENT_ATTENDANCE_LEAVETYPE_RETURN_MSG']);
}
$linterface->LAYOUT_START($Msg);
?>
	<link rel="stylesheet" href="/templates/jquery/thickbox.css" type="text/css" media="screen" />
	<script type="text/javascript" src="/templates/jquery/thickbox.js"></script>
	<br />
	<form id="form1" name="form1" method="post" action="">
		<div class="content_top_tool">
			<div class="Conntent_tool">
				<?=$linterface->Get_Thickbox_Link(480, 750,  'new', $Lang['Btn']['New'], 'showModalForm(0)', $InlineID="FakeLayer", $Lang['Btn']['New']);?>
			</div>
			<br style="clear:both;">
		</div>
		<br style="clear:both">
		<?=$linterface->Get_DBTable_Action_Button_IP25($tool_buttons)?>
		<br style="clear:both">
		<?=$li->display();?>
		<input type="hidden" id="pageNo" name="pageNo" value="<?=$li->pageNo?>">
		<input type="hidden" id="order" name="order" value="<?=$li->order?>">
		<input type="hidden" id="field" name="field" value="<?=$li->field?>">
		<input type="hidden" id="page_size_change" name="page_size_change" value="">
		<input type="hidden" id="numPerPage" name="numPerPage" value="<?=$li->page_size?>">
	</form>
	<br /><br />
	<script type="text/javascript">
        function showModalForm(id)
        {
            var is_edit = id? true: false;

            tb_show(is_edit?'<?=$Lang['Btn']['Edit']?>':'<?=$Lang['Btn']['New']?>',"#TB_inline?height=480&width=750&inlineId=FakeLayer");
            $.post(
                'ajax.php',
                {
                    'task':'getModalForm',
                    'TypeID': id
                },
                function(returnHtml){
                    $("div#TB_ajaxContent").html(returnHtml);
                }
            );
        }

        function checkEditForm()
        {
            var valid = true;
            var type_name = $.trim($('#TypeName').val());
            var type_symbol = $.trim($('#TypeSymbol').val());

            if(type_name == ''){
                $('#TypeName_Error').html('<?=$Lang['LessonAttendance']['RequestInputThisData']?>').show();
                valid = false;
            }else{
                $('#TypeName_Error').html('').hide();
            }

            if(type_symbol == ''){
                $('#TypeSymbol_Error').html('<?=$Lang['LessonAttendance']['RequestInputThisData']?>').show();
                valid = false;
            }else{
                $('#TypeSymbol_Error').html('').hide();
            }

            var upsertForm = function(){
                $('#task').val('upsertType');
                var formData = $('#modalForm').serialize();
                $.post(
                    'ajax.php',
                    formData,
                    function(returnCode)
                    {
                        tb_remove();
                        window.location.reload();
                    }
                );
            };

            var checkTypeSymbol = function(){
                Block_Thickbox();
                $('#task').val('checkTypeSymbol');
                var formData = $('#modalForm').serialize();
                $.post(
                    'ajax.php',
                    formData,
                    function(returnData)
                    {
                        var count = parseInt(returnData);
                        if(count <= 0){
                            $('#TypeSymbol_Error').html('').hide();
                            upsertForm();
                        }else{
                            $('#TypeSymbol_Error').html('<?=$Lang['StudentAttendance']['SameTypeCodeExists']?>').show();
                            UnBlock_Thickbox();
                        }
                    }
                );
            };

            var checkTypeName = function(){
                Block_Thickbox();
                $('#task').val('checkTypeName');
                var formData = $('#modalForm').serialize();
                $.post(
                    'ajax.php',
                    formData,
                    function(returnData)
                    {
                        var count = parseInt(returnData);
                        if(count <= 0){
                            $('#TypeName_Error').html('').hide();
                            checkTypeSymbol();
                        }else{
                            $('#TypeName_Error').html('<?=$Lang['StudentAttendance']['SameTypeNameExists']?>').show();
                            UnBlock_Thickbox();
                        }
                    }
                );
            }

            if(valid){
                checkTypeName();
            }
        }

        function deleteType()
        {
            Block_Thickbox();

            var typeIdAry = [];
            var objs = document.getElementsByName('TypeID[]');
            for(var i=0;i<objs.length;i++)
            {
                if(objs[i].checked){
                    typeIdAry.push(objs[i].value);
                }
            }

            $.post(
                'ajax.php',
                {
                    'task':'deleteType',
                    'TypeID[]': typeIdAry
                },
                function(returnData)
                {
                    window.location.reload();
                }
            );
        }
	</script>
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>