<?php
/*
 * 2020-02-07 (Ray): Created.
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libstudentattendance_ui.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_PRIVILEGE"]["plugin"]['attendancestudent'] || !$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !($sys_custom['StudentAttendance']['LeaveType'] || get_client_region() == 'zh_TW')) {
	header ("Location: /");
	intranet_closedb();
	exit();
}


$LessonAttendUI = new libstudentattendance_ui();
$linterface = new interface_html();

$task = $_POST['task'];

switch($task)
{
	case 'getModalForm':

		$TypeID = IntegerSafe($_POST['TypeID']);
		$record = array();
		if($TypeID > 0){
			$record = $LessonAttendUI->GetStudentAttendanceLeaveType($TypeID);
		}

		$x .= '<form name="modalForm" id="modalForm" action="" method="post" onsubmit="return false;">'."\n";
		$x .= '<div id="modalContent" style="overflow-y:auto;">'."\n";
		$x .= '<br />'."\n";
		$x .= '<table width="100%" cellpadding="2" class="form_table_v30">'."\n";
		$x .= '<col class="field_title">'."\n";
		$x .= '<col class="field_c">'."\n";


		$x .= '<tr>'."\n";
		$x .= '<td class="field_title">'.$Lang['StudentAttendance']['LeaveTypeCode'].'<span class="tabletextrequire">*</span></td>'."\n";
		$x .= '<td>'."\n";
		$x .= $linterface->GET_TEXTBOX_NAME("TypeSymbol", "TypeSymbol", $record[0]['TypeSymbol'], '', array('maxlength'=>'5'))."\n";
		$x .= $linterface->Get_Thickbox_Warning_Msg_Div("TypeSymbol_Error","", "WarnMsgDiv")."\n";
		$x .= '</td>'."\n";
		$x .= '</tr>'."\n";

		$x .= '<tr>'."\n";
		$x .= '<td class="field_title">'.$Lang['StudentAttendance']['LeaveTypeName'].'<span class="tabletextrequire">*</span></td>'."\n";
		$x .= '<td>'."\n";
		$x .= $linterface->GET_TEXTBOX_NAME("TypeName", "TypeName", $record[0]['TypeName'], $__OtherClass='', $__OtherPar=array());
		$x .= $linterface->Get_Thickbox_Warning_Msg_Div("TypeName_Error","", "WarnMsgDiv")."\n";
		$x .= '</td>'."\n";
		$x .= '</tr>'."\n";


		$x .= '</table>'."\n";
		if($record[0]['TypeID'] !='' && $record[0]['TypeID'] > 0){
			$x .= '<input type="hidden" name="TypeID" id="TypeID" value="'.$TypeID.'" />'."\n";
		}
		$x .= '<input type="hidden" name="task" id="task" value="" />';
		$x .= '</div>'."\n";

		$x .= $linterface->MandatoryField();

		$x .= '<div class="edit_bottom_v30">'."\n";
		$x .= $linterface->GET_ACTION_BTN($Lang['Btn']['Submit'],"button","checkEditForm();").'&nbsp;';
		$x .= $linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'],"button","tb_remove();")."\n";
		$x .= '</div>'."\n";

		$x .= '</form>'."\n";

		$x .= '<script type="text/javascript">'."\n";
		$x .= '$(document).ready(function(){'."\n";
		//$x .= '$("#modalContent").height($("#TB_ajaxContent").height() * 0.8 + "px");'."\n";
		$x .= '$("#TypeSymbol").focus();'."\n";
		$x .= '});'."\n";
		$x .= '</script>'."\n";

		echo $x;

		break;

	case 'upsertType':

		$success = $LessonAttendUI->UpsertStudentAttendanceType($_POST);
		$msg = $success? $Lang['General']['ReturnMessage']['UpdateSuccess'] : $Lang['General']['ReturnMessage']['UpdateUnsuccess'];
		$_SESSION['STUDENT_ATTENDANCE_LEAVETYPE_RETURN_MSG'] = $msg;
		echo $success ? '1':'0';

		break;

	case 'checkTypeName':
		$CheckText = trim($_POST['TypeName']);
		$TypeID = trim($_POST['TypeID']);
		$exist = $LessonAttendUI->IsStudentAttendanceTypeExist($CheckText, $TypeID, 'TypeName');
		echo $exist? '1':'0';
		break;

	case 'checkTypeSymbol':
		$CheckText = trim($_POST['TypeSymbol']);
		$TypeID = trim($_POST['TypeID']);
		$exist = $LessonAttendUI->IsStudentAttendanceTypeExist($CheckText, $TypeID, 'TypeSymbol');
		echo $exist? '1':'0';
		break;

	case 'deleteType':

		$success = $LessonAttendUI->DeleteStudentAttendanceType($TypeID);
		$msg = $success ? $Lang['General']['ReturnMessage']['DeleteSuccess'] : $Lang['General']['ReturnMessage']['DeleteUnsuccess'];
		$_SESSION['STUDENT_ATTENDANCE_LEAVETYPE_RETURN_MSG'] = $msg;
		echo $success?'1':'0';

		break;
}



intranet_closedb();
?>