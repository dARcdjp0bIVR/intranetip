<?php
// Editing by 
/*
 * 2017-11-10 (Carlos): Created.
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent'] || !$sys_custom['StudentAttendance']['HKUGAC_LateAbsentEmailNotification']) {
	intranet_closedb();
	header ("Location: /");
	exit();
}

$lcardstudentattend2 = new libcardstudentattend2();
$GeneralSetting = new libgeneralsettings();
$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageSystemSetting_HKUGACCustomSettings";
$linterface = new interface_html();

$Settings = $lcardstudentattend2->Settings;

$late_time = $Settings['HKUGAC_LateTime'];
if($late_time == ''){
	$late_time = '09:30';
}
$late_time_parts = explode(':',$late_time);
$late_time_hour = $late_time_parts[0];
$late_time_minute = $late_time_parts[1];

$job_status = $Settings['HKUGAC_JobStatus'];
$job_execution_time = $Settings['HKUGAC_JobExecutionTime'];
if($job_execution_time == ''){
	$job_execution_time = '00:00';
}
$job_execution_time_parts = explode(':',$job_execution_time);
$job_execution_time_hour = $job_execution_time_parts[0];
$job_execution_time_minute = $job_execution_time_parts[1];


$TAGS_OBJ[] = array($Lang['StudentAttendance']['CustomSettings'], "", 0);
$MODULE_OBJ = $lcardstudentattend2->GET_MODULE_OBJ_ARR();

if(isset($_SESSION['STUDENT_ATTENDANCE_HKUGAC_CUSTOM_SETTINGS_RESULT']))
{
	$Msg = $_SESSION['STUDENT_ATTENDANCE_HKUGAC_CUSTOM_SETTINGS_RESULT'];
	unset($_SESSION['STUDENT_ATTENDANCE_HKUGAC_CUSTOM_SETTINGS_RESULT']);
}
$linterface->LAYOUT_START($Msg);
?>
<?=$linterface->Include_JS_CSS()?>
<script type="text/javascript" language="javascript">
function editForm(edit)
{
	if(edit){	
		$('.edit').show();
		$('.display').hide();
	}else{
		$('.edit').hide();
		$('.display').show();
	}
}

function submitForm()
{
	document.form1.submit();
}
</script>
<br />
<form name="form1" id="form1" method="post" action="update.php" onsubmit="return false;">
	<div class="table_board">
		<table class="form_table_v30">
			<tr>
				<td class="field_title"><?=$Lang['StudentAttendance']['LateTime']?></td>
				<td>
					<span class="edit" style="display:none">
						<?=$linterface->Get_Time_Selection_Box('HKUGAC_LateTime_hour', 'hour', $late_time_hour, $__others_tab='', $__interval=1, 'HKUGAC_LateTime_hour').':'.$linterface->Get_Time_Selection_Box('HKUGAC_LateTime_minute', 'min', $late_time_minute, $__others_tab='', $__interval=1, 'HKUGAC_LateTime_minute')?>
					</span>
					<span class="display"><?=$Settings['HKUGAC_LateTime']==''?Get_String_Display($Settings['HKUGAC_LateTime']):($Settings['HKUGAC_LateTime'])?></span>
				</td>
			</tr>
			<tr>
				<td class="field_title"><?=$Lang['StudentAttendance']['EmailNotificationJobStatus']?></td>
				<td>
					<span class="edit" style="display:none">
						<?=$linterface->Get_Radio_Button('HKUGAC_JobStatus_Enabled', 'HKUGAC_JobStatus', 1, $Settings['HKUGAC_JobStatus']==1, $__Class="", $__Display=$Lang['General']['Enabled'], $__Onclick="",$__isDisabled=0)?>
						<?=$linterface->Get_Radio_Button('HKUGAC_JobStatus_Disabled', 'HKUGAC_JobStatus', 0, $Settings['HKUGAC_JobStatus']!=1, $__Class="", $__Display=$Lang['General']['Disabled'], $__Onclick="",$__isDisabled=0)?>
					</span>
					<span class="display"><?=$Settings['HKUGAC_JobStatus']==1?$Lang['General']['Enabled']:$Lang['General']['Disabled']?></span>
				</td>
			</tr>
			<tr>
				<td class="field_title"><?=$Lang['StudentAttendance']['EmailNotificationJobExecutionTime']?></td>
				<td>
					<span class="edit" style="display:none">
						<?=$Lang['StudentAttendance']['DailyOn'].$linterface->Get_Time_Selection_Box('HKUGAC_JobExecutionTime_hour', 'hour', $job_execution_time_hour, $__others_tab='', $__interval=1, 'HKUGAC_JobExecutionTime_hour').':'.$linterface->Get_Time_Selection_Box('HKUGAC_JobExecutionTime_minute', 'min', $job_execution_time_minute, $__others_tab='', $__interval=1, 'HKUGAC_JobExecutionTime_minute')?>
					</span>
					<span class="display"><?=$Settings['HKUGAC_JobExecutionTime']==''?Get_String_Display($Settings['HKUGAC_JobExecutionTime']):($Lang['StudentAttendance']['DailyOn'].$Settings['HKUGAC_JobExecutionTime'])?></span>
				</td>
			</tr>
		</table>
	</div>
	<div class="edit_bottom">
		<?=$linterface->Spacer()?>
		<div class="edit" style="display:none">
			<?=$linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], 'button', 'submitForm();','submit_btn','');?>
			<?=$linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], 'button', 'editForm(0);','cancel_btn','');?>
		</div>
		<div class="display">
			<?=$linterface->GET_ACTION_BTN($Lang['Btn']['Edit'], 'button', 'editForm(1);','edit_btn','');?>
		</div>
		<?=$linterface->Spacer()?>
	</div>
</form>
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>