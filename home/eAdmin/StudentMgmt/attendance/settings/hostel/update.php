<?php

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent'] || !$sys_custom['StudentAttendance']['HostelAttendance']) {
	intranet_closedb();
	$_SESSION['STUDENT_ATTENDANCE_HOSTEL_ATTENDANCE_SETTINGS_RESULT'] = $Lang['StudentAttendance']['SettingApplyFail'];
	header("Location:index.php");
	exit();
}

$lcardstudentattend2 = new libcardstudentattend2();
$GeneralSetting = $lcardstudentattend2->GetLibGeneralSettings();

$SettingList = array();
$SettingList['HostelAttendanceGroupCategory'] = $_POST['HostelAttendanceGroupCategory'];
$SettingList['HostelAttendanceAdmin'] = count($_POST['HostelAttendanceAdmin'])>0? str_replace('U','',implode(",",array_values(array_unique(array_diff($_POST['HostelAttendanceAdmin'],array('')))))) : "";
if($sys_custom['StudentAttendance']['HostelAttendance_Reason']) {
	$SettingList['HostelAttendanceLocationIn'] = $_POST['LocationIn'];
	$SettingList['HostelAttendanceLocationOut'] = $_POST['LocationOut'];
	$SettingList['HostelAttendanceDisablePastDate'] = $_POST['DisablePastDate'];
	$SettingList['HostelAttendanceDisableFutureDate'] = $_POST['DisableFutureDate'];
}
$success = $GeneralSetting->Save_General_Setting('StudentAttendance',$SettingList);

$_SESSION['STUDENT_ATTENDANCE_HOSTEL_ATTENDANCE_SETTINGS_RESULT'] = $success ? $Lang['StudentAttendance']['SettingApplySuccess'] : $Lang['StudentAttendance']['SettingApplyFail'];

intranet_closedb();
header("Location:index.php");
?>