<?php
# using: 

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");

intranet_auth();
intranet_opendb();

$lnotice = new libnotice();

if (!$lnotice->disabled)
{
    $MODULE_OBJ['title'] = $i_Notice_ReplyContent;
	$linterface = new interface_html("popup.html");
	$linterface->LAYOUT_START();
?>

<script language="javascript">
<!--
function check_form()
{
	if(document.form1.ReplySlipType[0].checked)
	{
		document.form1.action="editform.php";
	}	
	else
	{
		document.form1.action="editReplySlip.php";
	}
	document.form1.submit();
}
//-->
</script>

<br />
	<form name="form1" action="" method="post">
	
	<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
	<tr valign="top">
		<td width="20%" valign="top" nowrap="nowrap" class="formfieldtitle"><?=$eNotice['ReplySlipType']?></td>
		<td class="tabletext">
			<INPUT type="radio" value="question" name="ReplySlipType" id="ReplySlipType1" checked> <label for="ReplySlipType1"><?=$eNotice['QuestionBase']?></label>
			<INPUT type="radio" value="content" name="ReplySlipType" id="ReplySlipType2"> <label for="ReplySlipType2"><?=$eNotice['ContentBase']?></label>
		</td>
	</tr>
	<tr>
		<td class="dotline" colspan="2"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
	</tr>
	<tr>
		<td align="center" colspan="2">
			<?= $linterface->GET_ACTION_BTN($button_continue, "button", "check_form();") ?>
		</td>
	</tr>
	</table>                   
	
	<input type="hidden" name="cStr" value="">
	</form>
	
	<script language="javascript">
	<!--
	if(window.opener.document.form1.qStr.value != "" || window.opener.document.form1.cStr.value != "")
	{
		if(window.opener.document.form1.qStr.value != "")
			window.location="editform.php";
		else	
		{
			document.form1.cStr.value = window.opener.document.form1.cStr.value;
			document.form1.action="editReplySlip.php";
			document.form1.submit();
		}
	}
	//-->	
	</script>

<?php
}
else
{
    header ("Location: /");
}

intranet_closedb();
$linterface->LAYOUT_STOP();
?>