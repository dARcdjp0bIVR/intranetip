<?php
// Modifying by: henry 

###### Change Log [Start] ######
#
#	Date	:	2011-09-21 (Henry Chow)
#				handle checkbox "SendReplySlip" value
#
####### Change Log [End] #######

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$StudentAttend = new libcardstudentattend2();

$qStr = htmlspecialchars(trim($_REQUEST['qStr']));
$Title = htmlspecialchars(trim($_REQUEST['Title']));
$Content = htmlspecialchars(trim($_REQUEST['Content']));
$Subject = htmlspecialchars(trim($_REQUEST['Subject']));
$CategoryID = $_REQUEST['CategoryID'];
$Content = str_replace("http://".$_SERVER['HTTP_HOST'],'',$Content);
$TemplateID = $_REQUEST['TemplateID'];
$cStr = htmlspecialchars(trim($_REQUEST['cStr']));
$cStr = str_replace("http://".$_SERVER['HTTP_HOST'],'',$cStr);
$sendReplySlip = (isset($ReplySlip) && $ReplySlip==1) ? 1 : 0;

if($StudentAttend->Update_Template($CategoryID,$Title,$Content,$Subject,$RecordStatus,$qStr,$cStr,$TemplateID, $sendReplySlip))
{
	$SysMsg = $Lang['StudentAttendance']['eNotice']['TemplateSaveSuccess'];
}
else
{
	$SysMsg = $Lang['StudentAttendance']['eNotice']['TemplateSaveFail'];
}	
		
$ReturnPage = "index.php?Msg=$SysMsg";


intranet_closedb();
header("Location: $ReturnPage");
?>
