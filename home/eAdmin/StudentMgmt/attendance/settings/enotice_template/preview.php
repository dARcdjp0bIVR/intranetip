<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libform.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
//include_once($PATH_WRT_ROOT."includes/libucc.php");

intranet_auth();
intranet_opendb();

$lform = new libform();

$MODULE_OBJ['title'] = $i_Notice_ReplySlip;
$linterface = new interface_html("popup.html");
$linterface->LAYOUT_START();

/*
$lc = new libucc();
$lc->setTemplateID($tid);
$eNoticeData = $lc->getNoticeDetails();
$noticeReplySlipContent = stripslashes(undo_htmlspecialchars($eNoticeData['ReplySlipContent']));
debug_r($noticeReplySlipContent);
*/
//cStr

?>
<script language="javascript" src="/templates/forms/layer.js"></script>
<script language="javascript" src="/templates/forms/form_edit.js"></script>

<form name="ansForm">
<input type="hidden" name="qStr" value="">
<input type="hidden" name="aStr" value="">
<input type="hidden" name="cStr" value="">

<script language="javascript">
var replyslip = '<?=$Lang['eNotice']['ReplySlip']?>';
document.ansForm.cStr.value = window.opener.document.form1.cStr.value;
</script>

<br />
<p>
<table width="80%" align="center" border="0">
<tr id="qbase" style="display:none">
	<td align="left" height="0">
                <script language="Javascript">
                <?=$lform->getWordsInJS()?>
                
                var sheet= new Answersheet();
                // attention: MUST replace '"' to '&quot;'
                sheet.qString=window.opener.document.form1.qStr.value;
                sheet.aString="";
                //edit submitted application
                sheet.mode=1;
                sheet.answer=sheet.sheetArr();
                
                Part3 = '';
                document.write(editPanel());
                </script>
	</td>
</tr>
<tr id="cbase" style="display:none">
	<td align="left">
                <script language="Javascript">
                document.write(document.ansForm.cStr.value);
                </script>
	</td>
</tr>
<tr>
	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
</tr>

<tr>
	<td align="center">
		<?= $linterface->GET_ACTION_BTN($button_close, "button", "window.close(); return false;","cancelbtn") ?>
	</td>
</tr>
</table>
</form>

<script language="javascript">
	
if(document.ansForm.cStr.value != "")
{
 	document.getElementById("qbase").style.display = "none";
	document.getElementById("cbase").style.display = "inline";
}
else
{
	document.getElementById("qbase").style.display = "inline";
	document.getElementById("cbase").style.display = "none";
}
</script>

<br />

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>