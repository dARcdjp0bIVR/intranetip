<?php
// Modifying by:

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$StudentAttend = new libcardstudentattend2();

$IDs = implode(",",$TemplateID);
$sql = "DELETE FROM INTRANET_NOTICE_MODULE_TEMPLATE WHERE TemplateID in ($IDs) ";

if($StudentAttend->db_db_query($sql))
{
	$SysMsg = $Lang['StudentAttendance']['eNotice']['TemplateDeleteSuccess'];
}
else
{
	$SysMsg = $Lang['StudentAttendance']['eNotice']['TemplateDeleteFail'];
}	
		
$ReturnPage = "index.php?Msg=".$SysMsg;

intranet_closedb();
header("Location: $ReturnPage");
?>