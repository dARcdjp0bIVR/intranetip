<?php
# using: yat

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libstudentattendance_ui.php");

intranet_auth();
intranet_opendb();


if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

DisAllowiPadAndriod();

$linterface = new interface_html();
$StudentAttendUI = new libstudentattendance_ui();

$TemplateID = (is_array($_REQUEST['TemplateID']))? $_REQUEST['TemplateID'][0]:$_REQUEST['TemplateID'];

# menu highlight setting
$CurrentPage['eNoticTemplateSetting'] = 1;
$CurrentPageArr['StudentAttendance'] = 1;
$TAGS_OBJ[] = array($Lang['StudentAttendance']['eNoticeTemplate']);

# Left menu 
$MODULE_OBJ = $StudentAttendUI->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();

echo $StudentAttendUI->Get_Edit_eNotice_Template_Form($TemplateID);

$linterface->LAYOUT_STOP();
intranet_closedb();
?>
<script type="text/javascript">
function changeGenVariables(CategoryID)
{
  $("#genVariableDiv").load("ajax_genvariable.php",{"CategoryID":CategoryID},
  	function (data) {
  		if (data == "die") {
  			window.location = "/";
  		}
  	});
}

function checkForm(obj) 
{
	if(!check_text(obj.Title,'<?=$Lang['StudentAttendance']['eNotice']['TemplateNameWarning']?>'))
	{
		return false
	}
	if(!check_text(obj.Subject,'<?=$Lang['StudentAttendance']['eNotice']['SubjectWarning']?>'))
	{
		return false
	}
	/*
	if((obj.Content.value==""))
	{
		alert('<?=$i_Form_pls_fill_in?>');
		return false
	}
	*/
	var field = FCKeditorAPI.GetInstance('Content');
	var content_var = field.GetHTML(true);
	if(content_var=="")
	{
		alert('<?=$i_Form_pls_fill_in?>');
		return false
	}
	
	return true
}
						
function FillIn()
{
	var field = FCKeditorAPI.GetInstance('Content');
	field.InsertHtml(document.form1.genVariable.value);
}
</script>