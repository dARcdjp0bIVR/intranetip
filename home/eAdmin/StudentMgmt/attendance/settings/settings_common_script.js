function submitForm()
{
	 document.form1.submit();
}
function isValidForm() {
	var classIDsFields = document.getElementsByName('ClassIDs[]');
	for(i=0;i<classIDsFields.length;i++){
		if (classIDsFields[i].checked) {
			return true;
		}
	}
	return false;
}
function updateStatus(ParAction, ParWarningMsg) {
	try {
	if (document.getElementsByName('mode')[0].value > -1) {
		if (isValidForm()) {
			document.form1.action = ParAction;
			document.form1.submit();
		} else {
			document.getElementsByName('mode')[0].selectedIndex = 0;	// change the selection back to default value
			alert(ParWarningMsg);
		}
	}
	} catch (e){
		alert("Err: updateStatus: "+e.description);
	}
}