<?
// Editing by 
/*
 * 2016-08-11 (Carlos): $sys_custom['LessonAttendance_LaSalleCollege'] - added session range settings for classifying AM and PM.
 * 2015-09-18 (Carlos): add new settings [Default status] and [Lesson attendance status follow school attendance status].
 * 2013-08-28 (Carlos): add option [iSmartCard - teacher can take all lessons]
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_PRIVILEGE"]["plugin"]['attendancelesson'] || !$_SESSION["SSV_USER_ACCESS"]["eAdmin-LessonAttendance"]) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lcardstudentattend2 = new libcardstudentattend2();
$GeneralSetting = new libgeneralsettings();
$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageSystemSetting_LessonSettings";

$linterface = new interface_html();

/*
$SettingList[] = "'LessonSessionCountAsDay'"; // absent
$SettingList[] = "'LessonSessionLateCountAsDay'"; // late
$SettingList[] = "'EnablePSLAD'";
$SettingList[] = "'DaysBeforeToOpenData'";
$SettingList[] = "'TeacherCanTakeAllLessons'"; // iSmartCard - teacher can take all lessons
$SettingList[] = "'DefaultStatus'";
$SettingList[] = "'FollowSchoolStatus'";
if($sys_custom['LessonAttendance_LaSalleCollege']){
	$SettingList[] = "'StartSessionToCountAsAM'";
	$SettingList[] = "'EndSessionToCountAsAM'";
	$SettingList[] = "'StartSessionToCountAsPM'";
	$SettingList[] = "'EndSessionToCountAsPM'";
}
*/
//$Settings = $GeneralSetting->Get_General_Setting('LessonAttendance',$SettingList);
$Settings = $lcardstudentattend2->Get_General_Settings('LessonAttendance', $lcardstudentattend2->GetLessonAttendanceSettingsList());

$statusAry = array(
					array(0,$Lang['LessonAttendance']['Present']),
					array(3,$Lang['LessonAttendance']['Absent'])
				);
$defaultStatusSelection = $linterface->GET_SELECTION_BOX($statusAry, ' id="DefaultStatus" name="DefaultStatus" ', '', $Settings['DefaultStatus'], false);


// Lesson session count as 1 day absent
$LessonSessionCount = '<select name="LessonSessionCountAsDay">';
for ($i=1; $i< 21; $i++) {
	$selected = ($Settings['LessonSessionCountAsDay'] == $i)? "selected":"";
	$LessonSessionCount .= '<option value="'.$i.'" '.$selected.'>'.$i.'</option>';
}
$LessonSessionCount .= '</select>';

// Lesson session count as 1 day late
$LessonSessionLateCount = '<select name="LessonSessionLateCountAsDay">';
for ($i=1; $i< 21; $i++) {
	$selected = ($Settings['LessonSessionLateCountAsDay'] == $i)? "selected":"";
	$LessonSessionLateCount .= '<option value="'.$i.'" '.$selected.'>'.$i.'</option>';
}
$LessonSessionLateCount .= '</select>';

$checked = "";
$disable= "disabled";
if($Settings['EnablePSLAD'] == 1){
	$checked = "checked";
	$disable= "";
}
$EnablePSLAD = '<input type="checkbox" name="chk_EnablePSLAD" id="chk_EnablePSLAD" onclick="swap_textbox()" '.$checked.'></input>';


$DaysBeforeToOpenData = '<input type="text" style="width: 38px" name="txt_DaysBeforeToOpenData" id="txt_DaysBeforeToOpenData" value="'.$Settings['DaysBeforeToOpenData'].'" '.$disable.'></input>';


if($sys_custom['LessonAttendance_LaSalleCollege']){
	$start_session_to_count_as_am = $Settings['StartSessionToCountAsAM']!=''? $Settings['StartSessionToCountAsAM']: 1;
	$end_session_to_count_as_am = $Settings['EndSessionToCountAsAM']!=''? $Settings['EndSessionToCountAsAM'] : 6;
	$start_session_to_count_as_pm = $Settings['StartSessionToCountAsPM'] != ''? $Settings['StartSessionToCountAsPM'] : 7;
	$end_session_to_count_as_pm = $Settings['EndSessionToCountAsPM'] != ''? $Settings['EndSessionToCountAsPM'] : 9;
	
	$sessionNumAry = array();
	for($i=0;$i<21;$i++){
		$sessionNumAry[] = array($i+1,$i+1);
	}
	
	$start_session_am_selection = $linterface->GET_SELECTION_BOX($sessionNumAry, ' id="StartSessionToCountAsAM" name="StartSessionToCountAsAM" ', '', $start_session_to_count_as_am, false);
	$end_session_am_selection = $linterface->GET_SELECTION_BOX($sessionNumAry, ' id="EndSessionToCountAsAM" name="EndSessionToCountAsAM" ', '', $end_session_to_count_as_am, false);
	$start_session_pm_selection = $linterface->GET_SELECTION_BOX($sessionNumAry, ' id="StartSessionToCountAsPM" name="StartSessionToCountAsPM" ', '', $start_session_to_count_as_pm, false);
	$end_session_pm_selection = $linterface->GET_SELECTION_BOX($sessionNumAry, ' id="EndSessionToCountAsPM" name="EndSessionToCountAsPM" ', '', $end_session_to_count_as_pm, false);
	
}

//if(get_client_region() == 'zh_TW') {
	$TAGS_OBJ[] = array($Lang['LessonAttendance']['LessionAttendanceSetting'], $PATH_WRT_ROOT . "home/eAdmin/StudentMgmt/attendance/settings/lesson", 1);
	$TAGS_OBJ[] = array($Lang['LessonAttendance']['LessonAttendanceReasons'], $PATH_WRT_ROOT . "home/eAdmin/StudentMgmt/attendance/settings/lesson_attendance_reasons", 0);
//} else {
//	$TAGS_OBJ[] = array($Lang['LessonAttendance']['LessionAttendanceSetting'], "", 0);
//}

$MODULE_OBJ = $lcardstudentattend2->GET_MODULE_OBJ_ARR();

$Msg = urldecode($Msg);
$linterface->LAYOUT_START($Msg);

if ($msg == 2) $SysMsg = $linterface->GET_SYS_MSG("update");
?>
<br />
<form name="form1" method="post" action="update.php">
<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
		<td colspan="2" align="right" class="tabletext">
			<?=$SysMsg?>
		</td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
			<?=$Lang['LessonAttendance']['DefaultStatus']?>
		</td>
		<td class="tabletext" width="70%">
			<?=$defaultStatusSelection?>
		</td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
			<?=$Lang['LessonAttendance']['LessonStatusFollowSchoolStatus']?>
		</td>
		<td class="tabletext" width="70%">
			<?=$linterface->Get_Radio_Button("FollowSchoolStatusYes", "FollowSchoolStatus", 1, $Settings['FollowSchoolStatus']==1, "", $Lang['General']['Yes'], "", 0)?>&nbsp;
			<?=$linterface->Get_Radio_Button("FollowSchoolStatusNo", "FollowSchoolStatus", 0, $Settings['FollowSchoolStatus']!=1, "", $Lang['General']['No'], "", 0)?>
		</td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
			<?=$Lang['LessonAttendance']['LessonSessionCountAsDay']?>
		</td>
		<td class="tabletext" width="70%">
			<?=$LessonSessionCount?>
		</td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
			<?=$Lang['LessonAttendance']['LessonSessionLateCountAsDay']?>
		</td>
		<td class="tabletext" width="70%">
			<?=$LessonSessionLateCount?>
		</td>
	</tr>
<?php if($sys_custom['LessonAttendance_LaSalleCollege']){ ?>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
			<?=$Lang['LessonAttendance']['SessionsToCountAsAM']?>
		</td>
		<td class="tabletext" width="70%">
			<?=str_replace(array('<!--START-->','<!--END-->'),array($start_session_am_selection,$end_session_am_selection), $Lang['LessonAttendance']['FromSessionToSession'])?>
			<?=$linterface->Get_Form_Warning_Msg('SessionToCountAsAM_Error', $Lang['LessonAttendance']['InvalidSessionRange'], 'Error')?>
		</td>
	</tr>
		<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
			<?=$Lang['LessonAttendance']['SessionsToCountAsPM']?>
		</td>
		<td class="tabletext" width="70%">
			<?=str_replace(array('<!--START-->','<!--END-->'),array($start_session_pm_selection,$end_session_pm_selection), $Lang['LessonAttendance']['FromSessionToSession'])?>
			<?=$linterface->Get_Form_Warning_Msg('SessionToCountAsPM_Error', $Lang['LessonAttendance']['InvalidSessionRange'], 'Error')?>
		</td>
	</tr>
<?php } ?>	
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
			<?=$Lang['LessonAttendance']['EnablePSLAD']?>
		</td>
		<td class="tabletext" width="70%">
			<?=$EnablePSLAD?>
		</td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
			<?=$Lang['LessonAttendance']['DaysBeforeToOpenData']?>
		</td>
		<td class="tabletext" width="70%">
			<?=$DaysBeforeToOpenData?>
		</td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
			<?=$Lang['LessonAttendance']['iSmartCardTeacherCanTakeAllLessons']?>
		</td>
		<td class="tabletext" width="70%">
			<?=$linterface->Get_Checkbox("TeacherCanTakeAllLessons", "TeacherCanTakeAllLessons", "1", $Settings['TeacherCanTakeAllLessons'], '', '', '', '')?>
		</td>
	</tr>
</table>
<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
    	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
    </tr>
    <tr>
	    <td align="center">
			<?= $linterface->GET_ACTION_BTN($button_save, "button", "goSubmit()") ?>
			<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
		</td>
	</tr>
</table>
</form>
<script>
function swap_textbox(){
	if(document.getElementById('chk_EnablePSLAD').checked)
		document.getElementById('txt_DaysBeforeToOpenData').disabled = false;
	else
		document.getElementById('txt_DaysBeforeToOpenData').disabled = true;
}
function goSubmit() {
	var obj = document.form1;
	var intRegex = /^\d+$/;
	if (document.getElementById('chk_EnablePSLAD').checked && obj.txt_DaysBeforeToOpenData.value==""){
		alert ("<?=$Lang['General']['JS_warning']['CannotBeBlank']?>");
		obj.txt_DaysBeforeToOpenData.select();
		return false;
	}
	else if (document.getElementById('chk_EnablePSLAD').checked && (obj.txt_DaysBeforeToOpenData.value=="" || !IsNumeric(obj.txt_DaysBeforeToOpenData.value) || !intRegex.test(obj.txt_DaysBeforeToOpenData.value) || parseInt(obj.txt_DaysBeforeToOpenData.value) < 0)){
		alert ("<?=$Lang['General']['JS_warning']['MustBePositiveNumber']?>");
		obj.txt_DaysBeforeToOpenData.select();
		return false;
	}
<?php if($sys_custom['LessonAttendance_LaSalleCollege']){ ?>
	var session_valid = true;
	var am_start_session = parseInt($('#StartSessionToCountAsAM').val());
	var am_end_session = parseInt($('#EndSessionToCountAsAM').val());
	var pm_start_session = parseInt($('#StartSessionToCountAsPM').val());
	var pm_end_session = parseInt($('#EndSessionToCountAsPM').val());
	$('#SessionToCountAsAM_Error').hide();
	$('#SessionToCountAsPM_Error').hide();
	if(am_end_session < am_start_session){
		session_valid = false;
		$('#SessionToCountAsAM_Error').show();
	}
	if(pm_end_session < pm_start_session){
		session_valid = false;
		$('#SessionToCountAsPM_Error').show();
	}
	if(pm_start_session <= am_end_session){
		session_valid = false;
		$('#SessionToCountAsPM_Error').show();
	}
	if(!session_valid){
		return false;
	}
<?php } ?>
	obj.submit();
}
</script>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>