<?
// Editing by 
/*
 * 2016-08-11 (Carlos): $sys_custom['LessonAttendance_LaSalleCollege'] - added session range settings for classifying AM and PM.
 * 2015-09-18 (Carlos): add new settings [Default status] and [Lesson attendance status follow school attendance status].
 * 2013-08-28 (Carlos): add option [iSmartCard - teacher can take all lessons]
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_PRIVILEGE"]["plugin"]['attendancelesson'] && !$_SESSION["SSV_USER_ACCESS"]["eAdmin-LessonAttendance"]) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$SettingList = array();
$SettingList['DefaultStatus'] = $_REQUEST['DefaultStatus'];
$SettingList['FollowSchoolStatus'] = $_REQUEST['FollowSchoolStatus'];
$SettingList['LessonSessionCountAsDay'] = $_REQUEST['LessonSessionCountAsDay'];
$SettingList['LessonSessionLateCountAsDay'] = $_REQUEST['LessonSessionLateCountAsDay'];
if($_REQUEST['chk_EnablePSLAD'] == 'on')
	$SettingList['EnablePSLAD'] = 1;
else
	$SettingList['EnablePSLAD'] = 0;
if($_REQUEST['txt_DaysBeforeToOpenData'])
	$SettingList['DaysBeforeToOpenData'] = $_REQUEST['txt_DaysBeforeToOpenData'];

$SettingList['TeacherCanTakeAllLessons'] = $_REQUEST['TeacherCanTakeAllLessons'] == 1 ? 1 : 0;

if($sys_custom['LessonAttendance_LaSalleCollege']){
	$SettingList['StartSessionToCountAsAM'] = $_REQUEST['StartSessionToCountAsAM'];
	$SettingList['EndSessionToCountAsAM'] = $_REQUEST['EndSessionToCountAsAM'];
	$SettingList['StartSessionToCountAsPM'] = $_REQUEST['StartSessionToCountAsPM'];
	$SettingList['EndSessionToCountAsPM'] = $_REQUEST['EndSessionToCountAsPM'];
}

$GeneralSetting = new libgeneralsettings();

$GeneralSetting->Start_Trans();
if ($GeneralSetting->Save_General_Setting('LessonAttendance',$SettingList)) {
	$GeneralSetting->Commit_Trans();
	$Msg = $Lang['StudentAttendance']['SettingApplySuccess'];
}
else {
	$GeneralSetting->RollBack_Trans();
	$Msg = $Lang['StudentAttendance']['SettingApplyFail'];
}

header("Location: index.php?Msg=".urlencode($Msg));
?>