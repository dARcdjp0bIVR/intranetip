<?php
// Editing by 
/*
 * 2016-08-31 (Carlos): Created.
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libcrontab.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent'] || !$sys_custom['eClassApp']['HKUSPH']) {
	intranet_closedb();
	$_SESSION['STUDENT_ATTENDANCE_HKUSPH_JOB_SETTINGS_RESULT'] = $Lang['StudentAttendance']['SettingApplyFail'];
	header("Location:index.php");
	exit();
}

$GeneralSetting = new libgeneralsettings();
$SettingList['HKUSPH_JobStatus'] = $_REQUEST['HKUSPH_JobStatus'];
$SettingList['HKUSPH_JobExecutionTime'] = sprintf("%02d", $_REQUEST['hour']).':'.sprintf("%02d", $_REQUEST['minute']);

$GeneralSetting->Start_Trans();
if ($GeneralSetting->Save_General_Setting('StudentAttendance',$SettingList)) {
	$GeneralSetting->Commit_Trans();
	
	$crontab = new libcrontab();
	$site = "http://".$_SERVER['SERVER_NAME'].($_SERVER["SERVER_PORT"]!= 80?":".$_SERVER["SERVER_PORT"]:"");
	$script = $site."/schedule_task/hkusph.php";
	$crontab->removeJob($script);
	
	if($_REQUEST['HKUSPH_JobStatus'] == 1){
		$crontab->setJob($_REQUEST['minute'], $_REQUEST['hour'], '*', '*', '*', $script);
	}
	
	$_SESSION['STUDENT_ATTENDANCE_HKUSPH_JOB_SETTINGS_RESULT'] = $Lang['StudentAttendance']['SettingApplySuccess'];
}else {
	$GeneralSetting->RollBack_Trans();
	$_SESSION['STUDENT_ATTENDANCE_HKUSPH_JOB_SETTINGS_RESULT'] = $Lang['StudentAttendance']['SettingApplyFail'];
}

intranet_closedb();
header("Location:index.php");
?>
