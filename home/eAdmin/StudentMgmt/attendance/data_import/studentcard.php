<?php
# using: 

#########################################
# 									
#	Date:	2014-12-10 (Bill)
#			$sys_custom['SupplementarySmartCard'] - Added [Smart Card ID 4]
#
#	2013-11-13 (Carlos): $sys_custom['SupplementarySmartCard'] - Added [Smart Card ID 2] and [Smart Card ID3]
#
#	Date:	2012-11-30	YatWoon
#			Add flag $special_feature['eAttendance']['RFID'] to display RFID if client request [Case#2012-1011-1045-50071]
#
#########################################

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

# preserve table view
if ($ck_attend_import_studentcard_page_number!=$pageNo && $pageNo!="")
{
	setcookie("ck_attend_import_studentcard_page_number", $pageNo, 0, "", "", 0);
	$ck_attend_import_studentcard_page_number = $pageNo;
} else if (!isset($pageNo) && $ck_attend_import_studentcard_page_number!="")
{
	$pageNo = $ck_attend_import_studentcard_page_number;
}

if ($ck_attend_import_studentcard_page_order!=$order && $order!="")
{
	setcookie("ck_attend_import_studentcard_page_order", $order, 0, "", "", 0);
	$ck_attend_import_studentcard_page_order = $order;
} else if (!isset($order) && $ck_attend_import_studentcard_page_order!="")
{
	$order = $ck_attend_import_studentcard_page_order;
}

if ($ck_attend_import_studentcard_page_field!=$field && $field!="")
{
	setcookie("ck_attend_import_studentcard_page_field", $field, 0, "", "", 0);
	$ck_attend_import_studentcard_page_field = $field;
} else if (!isset($field) && $ck_attend_import_studentcard_page_field!="")
{
	$field = $ck_attend_import_studentcard_page_field;
}

$lcard = new libcardstudentattend2();
$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageDataImport_CardID";

$linterface = new interface_html();

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$order = ($order == 1) ? 1 : 0;

if (isset($targetClass) && $targetClass != "")
{
    $conds = "AND a.ClassName = '$targetClass'";
}
$user_field = getNameFieldByLang("a.");

if($special_feature['eAttendance']['RFID'])
{
	$sql_select = 	"a.RFID,";
}
$sql = "SELECT 
					$user_field, 
					a.ClassName, 
					a.ClassNumber, 
					a.CardID,";
if($sys_custom['SupplementarySmartCard']){
	$sql .= "		a.CardID2,
					a.CardID3,
					a.CardID4,";
}
$sql .= 		"$sql_select
          CONCAT('<input type=checkbox name=StudentID[] value=', a.UserID ,'>')
       	FROM 
       		INTRANET_USER as a 
       	WHERE 
       		a.RecordType = 2 
       		AND 
       		a.RecordStatus IN(0,1,2) 
       		$conds
          ";

# TABLE INFO
$li = new libdbtable2007($field, $order, $pageNo);
if($sys_custom['SupplementarySmartCard']){
	$li->field_array = array("a.EnglishName","a.ClassName","a.ClassNumber","a.CardID","a.CardID2","a.CardID3","a.CardID4","a.RFID");
}else{
	$li->field_array = array("a.EnglishName","a.ClassName","a.ClassNumber","a.CardID","a.RFID");
}
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+ ($special_feature['eAttendance']['RFID']?2:1);
$li->title = "";
$li->column_array = array(0,0,0,0,0,0,0);
$li->wrap_array = array(0,0,0,0,0,0,0);
$li->IsColOff = 2;

// TABLE COLUMN
$pos = 0;
$li->column_list .= "<td width='1' class='tabletoplink'>#</td>\n";
$li->column_list .= "<td width='". ($special_feature['eAttendance']['RFID']?30:50) ."%'>".$li->column($pos++, $i_UserStudentName)."</td>\n";
$li->column_list .= "<td width='10%'>".$li->column($pos++, $i_UserClassName)."</td>\n";
$li->column_list .= "<td width='10%'>".$li->column($pos++, $i_UserClassNumber)."</td>\n";
$li->column_list .= "<td width='".($sys_custom['SupplementarySmartCard']?"8":"25")."%'>".$li->column($pos++, $i_SmartCard_CardID)."</td>\n";
if($sys_custom['SupplementarySmartCard']){
	$li->column_list .= "<td width='8%'>".$li->column($pos++, $Lang['AccountMgmt']['SmartCardID2'])."</td>\n";
	$li->column_list .= "<td width='8%'>".$li->column($pos++, $Lang['AccountMgmt']['SmartCardID3'])."</td>\n";
	$li->column_list .= "<td width='8%'>".$li->column($pos++, $Lang['AccountMgmt']['SmartCardID4'])."</td>\n";
}
if($special_feature['eAttendance']['RFID'])
	$li->column_list .= "<td width='".($sys_custom['SupplementarySmartCard']?"18":"25")."%'>".$li->column($pos++, $Lang['StudentAttendance']['RFID'])."</td>\n";
$li->column_list .= "<td width='1'>".$li->check("StudentID[]")."</td>\n";

$lc = new libclass();
$select_classes = $lc->getSelectClass("name=\"targetClass\" onChange=\"this.form.submit()\"", $targetClass, 0, $i_general_all_classes);

$filterbar = $select_classes;

$toolbar = $linterface->GET_LNK_IMPORT("javascript:checkNew('studentcard_import.php')","","","","",0);
$toolbar .= $linterface->GET_LNK_EXPORT("javascript:checkGet(document.form1, 'studentcard_export.php')","","","","",0);

$table_tool .= "<td nowrap=\"nowrap\">
					<a href=\"javascript:checkEdit(document.form1,'StudentID[]','studentcard_edit.php')\" class=\"tabletool\">
						<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_edit.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">
						$button_edit
					</a>
				</td>";


# add more tags if sub sections will be added
$TAGS_OBJ[] = array($i_StudentAttendance_ImportCardID, "", 0);

$MODULE_OBJ = $lcard->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START($Msg);

if ($msg == 1) $SysMsg = $linterface->GET_SYS_MSG("add");
if ($msg == 2) $SysMsg = $linterface->GET_SYS_MSG("update");
if ($msg == 3) $SysMsg = $linterface->GET_SYS_MSG("delete");
?>
<br />
<form name="form1" method="get">
<table width="95%" border="0" cellpadding="3" cellspacing="0" align="center">
	<tr>
		<td align="left"><?=$toolbar?></td>
		<td align="right"><?=$SysMsg?></td>
	</tr>
</table>
<table width="95%" border="0" cellpadding="0" cellspacing="0" align="center">
	<tr class="table-action-bar">
		<td align="left"><?=$filterbar?></td>
		<td valign="bottom" align="right">
			<table border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="21"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_01.gif" width="21" height="23"></td>
					<td background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_02.gif">
						<table border="0" cellspacing="0" cellpadding="2">
							<tr>
								<?=$table_tool?>
							</tr>
						</table>
					</td>
					<td width="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_03.gif" width="6" height="23"></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<?php echo $li->display("95%"); ?>
<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>">
<input type="hidden" name="order" value="<?php echo $li->order; ?>">
<input type="hidden" name="field" value="<?php echo $li->field; ?>">
<input type="hidden" name="page_size_change" value="">
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>">
</form>
<br />
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
