<?
# using by: 

#########################################
# 									
#	Date:	2014-12-11 (Bill)
#			$sys_custom['SupplementarySmartCard'] - Added SmartCardID4
#
#	2013-11-13 (Carlos): $sys_custom['SupplementarySmartCard'] - Added SmartCardID2 and SmartCardID3
#
#	Date:	2012-11-30	YatWoon
#			Add flag $special_feature['eAttendance']['RFID'] to display RFID if client request [Case#2012-1011-1045-50071]
#
#########################################
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();

$li = new libdb();

$count = 0;

if($special_feature['eAttendance']['RFID'])
{
	$rfid_sql = ", RFID";
}

if ($_GET['format']==1)
{
    $sql = "SELECT UserLogin, CardID ".($sys_custom['SupplementarySmartCard']?", CardID2, CardID3, CardID4":"")." $rfid_sql FROM INTRANET_USER WHERE RecordType = 2 AND RecordStatus IN(0,1,2) ORDER BY UserLogin";
    $count = 2;
    $exportColumn = array("UserLogin", "CardID");
    if($sys_custom['SupplementarySmartCard']){
    	$exportColumn = array_merge($exportColumn,array("CardID2","CardID3","CardID4"));
    	$count += 3;
    }
}
else if ($_GET['format']==2)
{
     $sql = "SELECT ClassName, ClassNumber, CardID ".($sys_custom['SupplementarySmartCard']?", CardID2, CardID3, CardID4":"")." $rfid_sql FROM INTRANET_USER WHERE RecordType = 2 AND RecordStatus IN(0,1,2) AND ClassName != '' ORDER BY ClassName, ClassNumber";
     $count = 3;
     $exportColumn = array("ClassName", "ClassNumber", "CardID");
     if($sys_custom['SupplementarySmartCard']){
     	$exportColumn = array_merge($exportColumn,array("CardID2","CardID3","CardID4"));
     	$count += 3;
     }
}
if($special_feature['eAttendance']['RFID'])
{
	$exportColumn[] = "RFID";
	$count++;
}
if ($count < 1)
{
    header("Location: studentcard_export.php");
    exit();
}

$result = $li->returnArray($sql,$count);

$lexport = new libexporttext();

$export_content = $lexport->GET_EXPORT_TXT($result, $exportColumn);

// Output the file to user browser
$filename = "studentcard_id_$format.csv";
intranet_closedb();
$lexport->EXPORT_FILE($filename, $export_content);
?>
