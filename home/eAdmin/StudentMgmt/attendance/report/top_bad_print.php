<?php
//editing by 
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();

if (!isset($BadType) || !isset($TopNumber) || !isset($StartDate) || !isset($EndDate)) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$linterface = new interface_html();

include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_header.php");

switch ($BadType)
{
    case CARD_BADACTION_LUNCH_NOTINLIST: $nav_title = $i_StudentAttendance_BadLogs_Type_NotInLunchList; break;
    case CARD_BADACTION_LUNCH_BACKALREADY: $nav_title = $i_StudentAttendance_BadLogs_Type_GoLunchAgain; break;
    case CARD_BADACTION_FAKED_CARD_AM: $nav_title = $i_StudentAttendance_BadLogs_Type_FakedCardAM; break;
    case CARD_BADACTION_FAKED_CARD_PM: $nav_title = $i_StudentAttendance_BadLogs_Type_FakedCardPM; break;
    case CARD_BADACTION_NO_CARD_ENTRANCE: $nav_title = $i_StudentAttendance_BadLogs_Type_NoCardRecord; break;
}

$i_title = "<div class=\"eSportprinttext\">";
$i_title .= "$i_StudentAttendance_Report_StudentBadRecords<br />";
$i_title .= "$nav_title - $i_StudentAttendance_Top $TopNumber $i_StudentAttendance_Top_student_suffix ($StartDate to $EndDate)</div>";

$lc->retrieveSettings();

if($ShowAllColumns == "0") # Data Columns are Optionally Shown
{
	if($ColumnStudentName == "1")
	{
		$show_studentname = true;
	}
	else
	{
		$show_studentname = false;
	}
	if($ColumnClass == "1")
	{
		$show_class = true;
	}
	else
	{
		$show_class = false;
	}
}else # Default All Columns are shown
{
	$show_studentname = true;
	$show_class = true;
}

$ts = strtotime($StartDate);
if ($ts==-1 || $StartDate =="")
{
    $StartDate = date('Y-m-d');
}
$ts = strtotime($EndDate);
if ($ts==-1 || $EndDate =="")
{
    $EndDate = date('Y-m-d');
}

$result = $lc->retrieveTopBadRecordsCountByStudent($StartDate, $EndDate, $BadType, $TopNumber);

$display = "<table class=\"eSporttableborder\" width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">";
$display .= "<tr>";
$display .= "<td class=\"eSporttdborder eSportprinttabletitle\">#</td>";
if($show_studentname) $display .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_UserStudentName</td>\n";
if($show_class)
{
	$display .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_SmartCard_ClassName</td>\n";
	$display .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_ClassNameNumber</td>\n";
}
$display .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_StudentAttendance_ReportHeader_NumRecords</td>\n";
$display .= "</tr>\n";

$curr_pos = 1;
$last_count = 0;

if (sizeof($result)==0) {
	$display .= "<tr><td class=\"eSporttdborder eSportprinttext\" height=\"40\" colspan=\"5\" align=\"center\">$i_no_record_exists_msg</td></tr>";
} else {
	for ($i=0; $i<sizeof($result); $i++)
	{
	     list($studentid,$student_name,$student_class, $student_classnum, $count) = $result[$i];
	     if ($count != $last_count)
	     {
	         if ($i==0)
	         {
	         }
	         else
	         {
	             $curr_pos = $i+1;
	         }
	     }
	     else
	     {
	         $curr_pos = "&nbsp;";
	     }
	     $last_count = $count;
	     $css = ($i%2==0)?"tablebluerow1":"tablebluerow2";
	     $display .= "<tr>";
	     $display .= "<td class=\"eSporttdborder eSportprinttext\">$curr_pos</td>";
	     if($show_studentname) $display .= "<td class=\"eSporttdborder eSportprinttext\">$student_name&nbsp;</td>";
	     if($show_class)
	     {
	     	$display .= "<td class=\"eSporttdborder eSportprinttext\">$student_class&nbsp;</td>";
	     	$display .= "<td class=\"eSporttdborder eSportprinttext\">$student_classnum&nbsp;</td>";
	     }
	     $display .= "<td class=\"eSporttdborder eSportprinttext\">$count</td></tr>\n";
	}
}
$display .= "</table>\n";

?>
<table width='100%' align='center' class='print_hide' border=0>
<tr>
	<td align='right'><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></td>
</tr>
</table>
<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
<tr>
	<td class="eSportprinttext"><?=$i_title?></td>
</tr>
</table>
<?=$display?>
<br />
<?
include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_footer.php");
intranet_closedb();
?>