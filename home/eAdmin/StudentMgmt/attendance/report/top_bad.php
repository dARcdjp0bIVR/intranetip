<?
//using by 
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();
$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageReport_StudentBadRecords";

$linterface = new interface_html();

$select_type = "<SELECT name=\"BadType\">\n";
$select_type .= "<OPTION value=\"".CARD_BADACTION_LUNCH_NOTINLIST."\"".(($BadType==1)?" SELECTED ":"").">$i_StudentAttendance_BadLogs_Type_NotInLunchList</OPTION>\n";
$select_type .= "<OPTION value=\"".CARD_BADACTION_LUNCH_BACKALREADY."\"".(($BadType==2)?" SELECTED ":"").">$i_StudentAttendance_BadLogs_Type_GoLunchAgain</OPTION>\n";
$select_type .= "<OPTION value=\"".CARD_BADACTION_FAKED_CARD_AM."\"".(($BadType==3)?" SELECTED ":"").">$i_StudentAttendance_BadLogs_Type_FakedCardAM</OPTION>\n";
$select_type .= "<OPTION value=\"".CARD_BADACTION_FAKED_CARD_PM."\"".(($BadType==4)?" SELECTED ":"").">$i_StudentAttendance_BadLogs_Type_FakedCardPM</OPTION>\n";
$select_type .= "<OPTION value=\"".CARD_BADACTION_NO_CARD_ENTRANCE."\"".(($BadType==5)?" SELECTED ":"").">$i_StudentAttendance_BadLogs_Type_NoCardRecord</OPTION>\n";
$select_type .= "</SELECT>\n";

$array_top = array(1,5,10,20,50);
$select_top = getSelectByValue($array_top,"name=\"TopNumber\"",($TopNumber==""?10:$TopNumber),0,1);

$TAGS_OBJ[] = array($i_StudentAttendance_Report_StudentBadRecords, "", 0);

$MODULE_OBJ = $lc->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();
?>
<br />
<form name="form1" action="" method="GET">
<?php
if (isset($BadType) && isset($TopNumber) && isset($StartDate) && isset($EndDate)) {
	switch ($BadType)
	{
        case CARD_BADACTION_LUNCH_NOTINLIST: $nav_title = $i_StudentAttendance_BadLogs_Type_NotInLunchList; break;
        case CARD_BADACTION_LUNCH_BACKALREADY: $nav_title = $i_StudentAttendance_BadLogs_Type_GoLunchAgain; break;
        case CARD_BADACTION_FAKED_CARD_AM: $nav_title = $i_StudentAttendance_BadLogs_Type_FakedCardAM; break;
        case CARD_BADACTION_FAKED_CARD_PM: $nav_title = $i_StudentAttendance_BadLogs_Type_FakedCardPM; break;
        case CARD_BADACTION_NO_CARD_ENTRANCE: $nav_title = $i_StudentAttendance_BadLogs_Type_NoCardRecord; break;
	}
	$i_title = "$nav_title - $i_StudentAttendance_Top $TopNumber $i_StudentAttendance_Top_student_suffix ($StartDate to $EndDate)";
	$PAGE_NAVIGATION[] = array($i_title);
?>
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	</tr>
	<tr><td>&nbsp;</td></tr>
<table>
<?php } ?>
<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_general_Type?></td>
		<td width="70%"class="tabletext">
			<?=$select_type?>
		</td>
	</tr>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_StudentAttendance_DisplayTop?></td>
		<td width="70%"class="tabletext">
			<?=$select_top?>
		</td>
	</tr>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_general_startdate?></td>
		<td width="70%"class="tabletext">
			<?=$linterface->GET_DATE_PICKER("StartDate", $StartDate==""?date('Y-m-d'):$StartDate)?>
		</td>
	</tr>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_general_enddate?></td>
		<td width="70%"class="tabletext">
			<?=$linterface->GET_DATE_PICKER("EndDate", $EndDate==""?date('Y-m-d'):$EndDate)?>
		</td>
	</tr>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$Lang['SmartCard']['StaffAttendence']['ShowAllColumns']?></td>
		<td width="70%"class="tabletext">
		<input type="radio" name="ShowAllColumns" value="1" onclick="document.getElementById('OptionsLayer').style.display='none';this.checked=true;" <?if($ShowAllColumns=="" || $ShowAllColumns=="1"){echo "checked";}else{echo "";}?>><?=$i_general_yes?>
		&nbsp;
		<input type="radio" name="ShowAllColumns" value="0" onclick="document.getElementById('OptionsLayer').style.display='block';this.checked=true;" <?if($ShowAllColumns=="0"){echo "checked";}else{echo "";}?>><?=$i_general_no?>
		</td>
	</tr>
	<?
	if($ShowAllColumns == "0")
		$OptionsLayerVisibility = "style='display:block;'";
	else
		$OptionsLayerVisibility = "style='display:none;'";
	?>
	<tr>
		<td width="30%">&nbsp;</td>
		<td width="70%"class="tabletext">
		<span id="OptionsLayer" <?=$OptionsLayerVisibility?>>
			<input type="checkbox" name="ColumnStudentName" <?if($ColumnStudentName=="1"){echo "value=\"1\" checked";}else{echo "value=\"0\"";}?> onclick="if(this.value=='1'){this.checked=false;this.value='0';}else{this.checked=true;this.value='1';}"><?=$i_UserStudentName?><br />
			<input type="checkbox" name="ColumnClass" <?if($ColumnClass=="1"){echo "value=\"1\" checked";}else{echo "value=\"0\"";}?> onclick="if(this.value=='1'){this.checked=false;this.value='0';}else{this.checked=true;this.value='1';}"><?=$i_ClassNameNumber?>
		</span>
		</td>
	</tr>
</table>
<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
    	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
    </tr>
</table>
<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
	    <td align="center" colspan="2">
			<?= $linterface->GET_ACTION_BTN($button_view, "submit", "") ?>
		</td>
	</tr>
</table>
</form>
<?php
if (isset($BadType) && isset($TopNumber) && isset($StartDate) && isset($EndDate)) {
	$lc->retrieveSettings();
	
	if($ShowAllColumns == "0") # Data Columns are Optionally Shown
	{
		if($ColumnStudentName == "1")
		{
			$show_studentname = true;
		}
		else
		{
			$show_studentname = false;
		}
		if($ColumnClass == "1")
		{
			$show_class = true;
		}
		else
		{
			$show_class = false;
		}
		
	}else # Default All Columns are shown
	{
		$show_studentname = true;
		$show_class = true;
	}
	
	$ts = strtotime($StartDate);
	if ($ts==-1 || $StartDate =="")
	{
	    $StartDate = date('Y-m-d');
	}
	$ts = strtotime($EndDate);
	if ($ts==-1 || $EndDate =="")
	{
	    $EndDate = date('Y-m-d');
	}
	
	$result = $lc->retrieveTopBadRecordsCountByStudent($StartDate, $EndDate, $BadType, $TopNumber);
	
	$display = "<table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">";
	$display .= "<tr class=\"tablebluetop\">";
	$display .= "<td class=\"tabletoplink\">#</td>";
	if($show_studentname) $display .= "<td class=\"tabletoplink\">$i_UserStudentName</td>\n";
	if($show_class)
	{
		$display .= "<td class=\"tabletoplink\">$i_SmartCard_ClassName</td>\n";
		$display .= "<td class=\"tabletoplink\">$i_ClassNameNumber</td>\n";
	}
	$display .= "<td class=\"tabletoplink\">$i_StudentAttendance_ReportHeader_NumRecords</td>\n";
	$display .= "</tr>\n";
	
	$curr_pos = 1;
	$last_count = 0;
	
	if (sizeof($result)==0) {
		$display .= "<tr class=\"tablebluerow2\"><td class=\"tabletext\" height=\"40\" colspan=\"5\" align=\"center\">$i_no_record_exists_msg</td></tr>";
	} else {
		for ($i=0; $i<sizeof($result); $i++)
		{
		     list($studentid,$student_name,$student_class, $student_classnum, $count) = $result[$i];
		     if ($count != $last_count)
		     {
		         if ($i==0)
		         {
		         }
		         else
		         {
		             $curr_pos = $i+1;
		         }
		     }
		     else
		     {
		         $curr_pos = "&nbsp;";
		     }
		     $last_count = $count;
		     $css = ($i%2==0)?"tablebluerow1":"tablebluerow2";
		     $display .= "<tr class=\"$css\">";
		     $display .= "<td class=\"tabletext\">$curr_pos</td>";
		     if($show_studentname) $display .= "<td class=\"tabletext\">$student_name</td>";
		     if($show_class)
		     {
		     	$display .= "<td class=\"tabletext\">$student_class</td>";
		     	$display .= "<td class=\"tabletext\">$student_classnum</td>";
		     }
		     $display .= "<td class=\"tabletext\">$count</td></tr>\n";
		}
	}
	$display .= "</table>\n";
	
	$toolbar .= $linterface->GET_LNK_PRINT("javascript:openPrintPage()","","","","",0);
?>
<script language="JavaScript" type="text/javascript">
function openPrintPage()
{
    newWindow("top_bad_print.php?BadType=<?=$BadType?>&TopNumber=<?=$TopNumber?>&StartDate=<?=$StartDate?>&EndDate=<?=$EndDate?>&ShowAllColumns=<?=$ShowAllColumns?>&ColumnStudentName=<?=$ColumnStudentName?>&ColumnClass=<?=$ColumnClass?>",10);
}
</script>
<table width="95%" border="0" cellpadding="0" cellspacing="0" align="center">
	<tr>
		<td align="left"><?= $toolbar ?></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
</table>
<?=$display?>
<?php } ?>
<br />
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>