<?php
# Using:

################################################# Change log ####################################################
# 2013-08-05 by Henry Chan: file created
#################################################################################################################
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libstudentattendance_ui.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-LessonAttendance"] || !$_SESSION["SSV_PRIVILEGE"]["plugin"]["attendancelesson"]) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$ClassID = $_REQUEST['ClassID'];


$linterface = new interface_html();

$LessonAttendUI = new libstudentattendance_ui();

$class_name = $LessonAttendUI->getClassName($ClassID);

echo $LessonAttendUI->getStudentSelectByClass($class_name, "id='opt_student[]' name='opt_student[]' size='10' MULTIPLE","",0);

intranet_closedb();
?>