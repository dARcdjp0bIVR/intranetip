<?php
// using 
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libstudentattendance_ui.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-LessonAttendance"] || !$_SESSION["SSV_PRIVILEGE"]["plugin"]["attendancelesson"]) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$FromDate = $_REQUEST['FromDate'];
$ToDate = $_REQUEST['ToDate'];
$ClassID = $_REQUEST['ClassID'];
$StudentID=$_REQUEST['StudentID'];

$StudentIDArray = explode(',',$StudentID);

$AttendStatus = array();
if($tempAttendStatus == 3){
	$AttendStatus[2] = 2;
	$AttendStatus[3] = 3;
}
else if($tempAttendStatus == 2){
	$AttendStatus[3] = 3;
}
else if($tempAttendStatus == 1){
	$AttendStatus[2] = 2;

}

$LessonAttendUI = new libstudentattendance_ui();

$LessonAttendUI->Get_Lesson_Summary_Report($FromDate,$ToDate,$ClassID,"export",$AttendStatus, $StudentIDArray);

intranet_closedb();
?>