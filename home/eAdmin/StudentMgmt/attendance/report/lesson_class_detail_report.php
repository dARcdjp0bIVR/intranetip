<?php
// using 
/*
 * 2013-08-30 (Carlos): Export csv report if more than one day is selected
 */
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libstudentattendance_ui.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-LessonAttendance"] || !$_SESSION["SSV_PRIVILEGE"]["plugin"]["attendancelesson"]) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$TargetDate = $_REQUEST['TargetDate'];

$linterface = new interface_html();

$LessonAttendUI = new libstudentattendance_ui();
$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageLessonAttendance_ClassDetailAttendReport";

### Title ###
$TAGS_OBJ[] = array($Lang['LessonAttendance']['LessonClassDetailReport'],"",0);
$MODULE_OBJ = $LessonAttendUI->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START(); 

echo $LessonAttendUI->Get_Class_Lesson_Daily_Report_Form();

$linterface->LAYOUT_STOP();

intranet_closedb();
?>
<script>
function View_Lesson_Daily_Overview() {
	var TargetDate = document.getElementById('TargetDate').value;
	var TargetEndDate = document.getElementById('TargetEndDate').value;
	var isValid = true;
	//var re = /\b\d{4}[\/-]\d{1,2}[\/-]\d{1,2}\b/;
	
	if(!check_date_without_return_msg(document.getElementById('TargetDate'))){
		isValid = false;
		$("span#TargetDateWarningLayer").html('<?=$Lang['LessonAttendance']['InvalidDateFormat']?>').show();
	}else{
		$("span#TargetDateWarningLayer").html('').hide;
	}
	
	if(!check_date_without_return_msg(document.getElementById('TargetEndDate'))){
		isValid = false;
		$("span#TargetEndDateWarningLayer").html('<?=$Lang['LessonAttendance']['InvalidDateFormat']?>').show();
	}else{
		$("span#TargetEndDateWarningLayer").html('').hide();
	}
	
	if (isValid) {
		Block_Element('DetailTableLayer');
		
		PostVar = {
			"TargetDate":TargetDate,
			"TargetEndDate":TargetEndDate,
			"AttendStatus[]":Get_Check_Box_Value('AttendStatus[]','Array')
		}
		if(TargetEndDate > TargetDate) {
			var formObj = document.form1;
			var oldAction = formObj.action;
			var oldTarget = formObj.target;
			
			formObj.action = "ajax_get_lesson_class_detail_report.php";
			formObj.target = "_blank";
			formObj.submit();
			
			$("div#DetailTableLayer").html('');
			UnBlock_Element('DetailTableLayer');
				
			formObj.action = oldAction;
			formObj.target = oldTarget;
		}else{
			$("div#DetailTableLayer").load('ajax_get_lesson_class_detail_report.php',PostVar,function() {UnBlock_Element('DetailTableLayer');});
		}
	}
}

function startDateChanged()
{
	var startDate = $.trim($('#TargetDate').val());
	$('#TargetEndDate').val(startDate);
}

function endDateChanged()
{
	var startDate = $.trim($('#TargetDate').val());
	var endDate = $.trim($('#TargetEndDate').val());
	
	if(startDate != endDate && endDate > startDate) {
		$('#submitButton').val('<?=$Lang['Btn']['Export']?>');
	}else{
		$('#submitButton').val('<?=$Lang['Btn']['View']?>');
	}
}
</script>