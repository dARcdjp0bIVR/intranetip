<?php
//editing by 
/*
 * 2020-10-05 (Ray)    : Add ColumnClassName, ColumnClassNumber
 * 2015-05-27 (Carlos) : Add [Report Type] to separate the detail report, sumamry stat and class summary stat. Added display column [Office Remark].
 * 2014-01-28 (Carlos): Added column [Remark]
 */
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libstudentattendance_ui.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$StudentAttendUI = new libstudentattendance_ui();

$class_name = $_REQUEST['class_name'];
$StudentList = $_REQUEST['StudentList'];
$ShowAllColumns = $_REQUEST['ShowAllColumns'];
$ColumnLoginID = $_REQUEST['ColumnLoginID'];
$ColumnStudentName = $_REQUEST['ColumnStudentName'];
$ColumnClassName = $_REQUEST['ColumnClassName'];
$ColumnClassNumber = $_REQUEST['ColumnClassNumber'];
$ColumnReason = $_REQUEST['ColumnReason'];
$ColumnRemark = $_REQUEST['ColumnRemark'];
$ColumnOfficeRemark = $_REQUEST['ColumnOfficeRemark'];
$ColumnWaived = $_REQUEST['ColumnWaived'];
//$ColumnSummaryStat = $_REQUEST['ColumnSummaryStat'];
//$ColumnClassSummaryStat = $_REQUEST['ColumnClassSummaryStat'];
$startStr = $_REQUEST['startStr'];
$endStr = $_REQUEST['endStr'];
$attendance_type = $_REQUEST['attendance_type'];
$session = $_REQUEST['session'];
$reason = $_REQUEST['reason'];
$waived = $_REQUEST['waived'];
$match = $_REQUEST['match'];
$ReportType = $_REQUEST['ReportType'];

$ColumnOptions = $_REQUEST;

//$StudentAttendUI->Get_Search_Report($startStr,$endStr,$attendance_type,$session,$reason,$match,$waived,$class_name,$StudentList,$format,$ShowAllColumns,$ColumnLoginID,$ColumnStudentName,$ColumnClass,$ColumnReason,$ColumnWaived,$ColumnSummaryStat,$ColumnClassSummaryStat,$ColumnRemark);
$StudentAttendUI->Get_Search_Report($startStr,$endStr,$attendance_type,$session,$reason,$match,$waived,$class_name,$StudentList,$format,$ReportType, $ColumnOptions);

intranet_closedb();
?>