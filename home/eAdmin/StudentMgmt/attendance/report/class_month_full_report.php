<?php
//editing by 
####################################### Change Log #################################################
# 2012-12-28 by Carlos: Added continuous absent day option
# 2012-06-14 by Carlos: fix openPrintPage() path optional parameters 
# 2012-01-10 by YatWOon: add print button
# 2011-12-19 by Carlos: Added parameter StudentStatus
# 2010-06-01 by Carlos: Check attendance mode to determine showing AM/PM/WholeDay data
# 2010-05-06 by Carlos: Add Late Session Statistic and Late session reason statistic
# 2010-02-05 by Carlos: Add show main data distribution option
#
####################################################################################################
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libstudentattendance_ui.php");

intranet_auth();
intranet_opendb();

if ( !($_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || $_SESSION["platform"]=="KIS") || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	intranet_closedb();
	header ("Location: /");
	exit();
}

$MODULE_OBJ['title'] = $i_StudentAttendance_Report_ClassMonth;
$linterface = new interface_html("popup.html");
$lstudentattendance_ui = new libstudentattendance_ui();

$lclass = new libclass();
$AcademicYear = $_REQUEST['AcademicYear'];
if ($ClassID != "") {
	$classes[0]['ClassID'] = $ClassID;
	$ClassName = $lclass->getClassName($ClassID);
	$classes[0]['ClassName'] = ($ClassName == "")? $ClassID:$ClassName;
}
else {
	if ($AcademicYear == "" || ($lstudentattendance_ui->Platform == "IP" && $AcademicYear == Get_Current_Academic_Year_ID())) {
		$classes = $lclass->getClassList();
	}
	else {
		$classes = $lstudentattendance_ui->Get_Class_List_By_Academic_Year($AcademicYear);
	}
}

#Get Date Range
$startDate = trim($_REQUEST['StartDate']);
$endDate = trim($_REQUEST['EndDate']);


# Get Year & Month
if ($Year == "")
{
	$Year = date('Y');
}
if ($Month == "")
{
	$Month = date('m');
}
if (strlen($Month)==1)
{
	$Month = "0".$Month;
}

# Get Optional Data Columns
$Columns=array();
if($ShowAllColumns == "0") # Data Columns are Optionally Shown
{
	$Columns['ChineseName'] = $ColumnChineseName;
	$Columns['EnglishName'] = $ColumnEnglishName;
	$Columns['Gender'] = $ColumnGender;
	$Columns['Data'] = $ColumnData;
	$Columns['DailyStat'] = $ColumnDailyStat;
	$Columns['Schooldays'] = $ColumnSchoolDays;
	$Columns['MonthlyStat'] = $ColumnMonthlyStat;
	$Columns['Remark'] = $ColumnRemark;
	$Columns['ReasonStat'] = $ColumnReasonStat;
	$Columns['BirthDate'] = $ColumnBirthDate;
	$Columns['EntryDate'] = $ColumnEntryDate;
	$Columns['NonSchoolDayReason'] = $ColumnNonSchoolDayReason;
	if($sys_custom['SmartCardAttendance_StudentAbsentSession'])
	{
		$Columns['Session'] = $ColumnSession;
	}
}else # Default All Columns are shown
{
	$Columns['ChineseName'] = "1";
	$Columns['EnglishName'] = "1";
	$Columns['Gender'] = "1";
	$Columns['Data'] = "1";
	$Columns['DailyStat'] = "1";
	$Columns['Schooldays'] = "1";
	$Columns['MonthlyStat'] = "1";
	$Columns['Remark'] = "1";
	$Columns['ReasonStat'] = "1";
	$Columns['NonSchoolDayReason'] = "1";
	if ($_SESSION['platform'] == "KIS"){
		$Columns['BirthDate'] = "1";
		$Columns['EntryDate'] = "1";
	}
	if($sys_custom['SmartCardAttendance_StudentAbsentSession'])
	{
		$Columns['Session'] = "1";
	}
}

$params = "";
foreach($Columns as $k => $v){
	if($v==1){
		if($k == 'Schooldays'){
			$k = 'SchoolDays';
		}
		$params .= "&Column".$k."=".urlencode($v);
	}
}

$StudentStatus = trim($StudentStatus)==""?"0,1,2":$StudentStatus;
$ContinuousAbsentDay = round($_REQUEST['AbsentDay']);

# Get Report Content
if($UseDateRange == 0){
$content = $lstudentattendance_ui->Get_Class_Monthly_Attendance_Report($classes,$Year,$Month,$Columns,$_REQUEST['HideNoData'],false,$AcademicYear,$StudentStatus,0,$ContinuousAbsentDay);
}
else{
$content = $lstudentattendance_ui->Get_Class_Monthly_Attendance_Report_By_Date_Range($classes,$Year, $Month,$Columns,$_REQUEST['HideNoData'],false,$AcademicYear,$StudentStatus,$startDate,$endDate,'',$ContinuousAbsentDay);
}
if ($format == 1)     # Excel
{
	# Get template
	$template_content = get_file_content("$file_path/home/eAdmin/StudentMgmt/attendance/report/template.html");
	$output = str_replace("__MAIN_CONTENT__",$content,$template_content);
	$output_filename = "class_attend_".$classes[sizeof($classes)-1]['ClassName'].".xls";
	output2browser($output,$output_filename);
	flush();
}else {           # Web
$linterface->LAYOUT_START();

if($UseDateRange==0){
$toolbar = $linterface->GET_LNK_PRINT("javascript:openPrintPage()","","","","",0);
}
else{
$toolbar = $linterface->GET_LNK_PRINT("javascript:openPrintPageByDateRange()","","","","",0);		
}



?>
<style type="text/css">
#contentTable1, #contentTable2 {
	border:1px solid black;
}

#contentTable1 td, #contentTable2 td {
	border:1px solid black;
}

#contentTable3 td, #contentTable4 td {
	border:0px solid black;
}
</style>
<script language="JavaScript" type="text/javascript">
function openPrintPage()
{

	newWindow("class_month_full_report_print.php?AcademicYear=<?=urlencode($AcademicYear)?>&ClassID=<?=urlencode($ClassID)?>&Year=<?=urlencode($Year)?>&Month=<?=urlencode($Month)?>&ShowAllColumns=<?=urlencode($ShowAllColumns).$params?>&AbsentDay=<?=urlencode($ContinuousAbsentDay)?>&StudentStatus=<?=urlencode($StudentStatus)?>",12);

}

function openPrintPageByDateRange()
{

	newWindow("class_month_full_report_print.php?AcademicYear=<?=urlencode($AcademicYear)?>&ClassID=<?=urlencode($ClassID)?>&Year=<?=urlencode($Year)?>&Month=<?=urlencode($Month)?>&ShowAllColumns=<?=urlencode($ShowAllColumns).$params?>&startDate=<?=urlencode($startDate)?>&endDate=<?=urlencode($endDate)?>&UseDateRange=<?=urlencode($UseDateRange)?>&AbsentDay=<?=urlencode($ContinuousAbsentDay)?>&StudentStatus=<?=urlencode($StudentStatus)?>",12);

}
</script>
<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td><?= $toolbar ?></td>
	</tr>
	
</table>
<?php
echo $content;
$linterface->LAYOUT_STOP();
}
intranet_closedb();
?>