<?php
// Editing by
/*
 * 2019-07-26 (Ray): Created.
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libstudentattendance_ui.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");

intranet_auth();
intranet_opendb();

$StartDate = $_POST['StartDate'];
$EndDate = $_POST['EndDate'];
$TargetType = $_POST['TargetType'];
$TargetID = $_POST['TargetID'];
$Format = $_POST['Format']; // ""/"web", "csv", "print"

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION["SSV_PRIVILEGE"]["plugin"]["attendancestudent"] || !in_array($Format,array("","web","csv","print"))) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$linterface = new interface_html();
$StudentAttendUI = new libstudentattendance_ui();

$attendance_code = array(CARD_STATUS_PRESENT, CARD_STATUS_LATE, CARD_STATUS_OUTING);
$attendance_code = implode(',', $attendance_code);

$target_result = $StudentAttendUI->GetTargetSelectionResult($TargetType, $TargetID);
$UserList = $target_result[0];
$UserList = (sizeof($UserList) > 0)? "'".implode("','",$StudentAttendUI->Get_Safe_Sql_Query($UserList))."'":'-1';

if($StudentAttendUI->attendance_mode == '2' || $StudentAttendUI->attendance_mode == '3') {
	$status_field = "IF(a.AMStatus IN($attendance_code), 0.5, 0) AS amPresent,
					IF(a.PMStatus IN($attendance_code), 0.5, 0) AS pmPresent,
					IF(a.AMStatus IS NOT NULL, 0.5, 0) AS amSchoolDay,
					IF(a.PMStatus IS NOT NULL, 0.5, 0) as pmSchoolDay";
} else if($StudentAttendUI->attendance_mode == '0') {
	$status_field = "IF(a.AMStatus IN($attendance_code), 1, 0) AS amPresent,
					0 AS pmPresent,
					IF(a.AMStatus IS NOT NULL, 1, 0) AS amSchoolDay,
					0 as pmSchoolDay";
} else if($StudentAttendUI->attendance_mode == '1') {
	$status_field = "0 AS amPresent,
					IF(a.PMStatus IN($attendance_code), 1, 0) AS pmPresent,
					0 AS amSchoolDay,
					IF(a.PMStatus IS NOT NULL, 1, 0) as pmSchoolDay";
}

$student_name_field = getNameFieldByLang2("b.");

$student_attendance_data = array();
$data_classname = array();
$data_classnumber = array();

$start_day = date('d', strtotime($StartDate));
$end_day = date('d', strtotime($EndDate));
$month_diff = getMonthDifferenceBetweenDates($StartDate, $EndDate);
$AcademicYearID = Get_Current_Academic_Year_ID();

for($i=0;$i<=$month_diff;$i++) {
	$table = "CARD_STUDENT_DAILY_LOG_".date("Y_m", strtotime($StartDate." +$i month"));
	$sql = "SHOW TABLES LIKE '$table'";
	$records = $StudentAttendUI->returnResultSet($sql);

	if(count($records) == 0) {
		continue;
	}

	$day_cond = '';
	if($i == 0) {
		$day_cond .= " AND a.DayNumber >= '$start_day' ";
	}
	if($i == $month_diff) {
		$day_cond .= " AND a.DayNumber <= '$end_day' ";
	}
	$sql = "SELECT 
			YearName,
			UserID,
			ClassName,
			ClassNumber,
			StudentName,
			(SUM(amPresent) + SUM(pmPresent)) AS totalPresent,
			(SUM(amSchoolDay) + SUM(pmSchoolDay)) AS totalSchoolDay
			FROM (
				SELECT
					y.YearName,
					b.UserID,
					b.ClassName,
					b.ClassNumber,
					$student_name_field AS StudentName,
					$status_field
					FROM $table AS a
					INNER JOIN 
					INTRANET_USER AS b ON a.UserID = b.UserID 
					AND b.RecordType = 2
					AND b.RecordStatus IN (0,1,2)
					AND a.UserID IN (".$UserList.") 
					$day_cond
					INNER JOIN
					YEAR_CLASS_USER as ycu ON ycu.UserID = a.UserID
					INNER Join YEAR_CLASS as yc On (ycu.YearClassID = yc.YearClassID)
					AND yc.AcademicYearID = '" . $AcademicYearID . "'
					INNER Join YEAR as y On (yc.YearID = y.YearID)
			) z
			GROUP BY UserID";

	$tmp_records = $StudentAttendUI->returnResultSet($sql);

	foreach($tmp_records as $temp) {
		if(empty($student_attendance_data[$temp['UserID']])) {
			$data_classname[$temp['UserID']] = $temp['ClassName'];
			$data_classnumber[$temp['UserID']] = $temp['ClassNumber'];

			$student_attendance_data[$temp['UserID']] = array("YearName"=>$temp['YearName'],
																"UserID"=>$temp['UserID'],
																"ClassName"=>$temp['ClassName'],
																"ClassNumber"=>$temp['ClassNumber'],
																"StudentName"=>$temp['StudentName'],
																"totalPresent"=>0,
																"totalSchoolDay"=>0);

		}
		$student_attendance_data[$temp['UserID']]['totalPresent'] += $temp['totalPresent'];
		$student_attendance_data[$temp['UserID']]['totalSchoolDay'] += $temp['totalSchoolDay'];
	}
}

if(count($student_attendance_data) > 0) {
	array_multisort($data_classname, SORT_ASC, $data_classnumber, SORT_ASC, $student_attendance_data);
}

$form_attendance_data = array();
if($TargetType == 'form') {
	$form_attendance_data = array();
	foreach ($student_attendance_data as $record) {
		$form_attendance_data[$record['YearName']][] = $record;
	}
} else if($TargetType == 'class') {
	$form_attendance_data = array();
	foreach ($student_attendance_data as $record) {
		$form_attendance_data[$record['ClassName']][] = $record;
	}
}

$x = '';

if(in_array($Format,array("","web")))
{
	$x .= '<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">' . "\n";
	$x .= '<tr>' . "\n";
	$x .= '<td>' . "\n";
	$x .= '<div class="content_top_tool">' . "\n";
	$x .= '<div class="Conntent_tool">' . "\n";
	$x .= $linterface->GET_LNK_PRINT("javascript:submitForm('print');", "", "", "", "", 1);
	$x .= $linterface->GET_LNK_EXPORT("javascript:submitForm('csv');", "", "", "", "", 1);
	$x .= '</div>' . "\n";
	$x .= '<br style="clear: both;">' . "\n";
	$x .= '</div>' . "\n";
	$x .= '</td>' . "\n";
	$x .= '</tr>' . "\n";
	$x .= '</table>' . "\n";


	$x.= '<table class="common_table_list_v30">'."\n";
	$x.='<thead>';
	$x.= '<tr>';
	$colspan = 0;
	if($TargetType == 'form') {
		$x.='<th class="num_check" width="1">#</th>';
		$x.='<th style="width:20%;">'.$Lang['AccountMgmt']['Form'].'</th>';
		$x.='<th style="width:20%;">'.$i_StudentAttendance_ReportHeader_NumStudents.'</th>';
		$x.='<th style="width:20%;">'.$Lang['StudentAttendance']['NumberOfStudendsAttended'] .'<sup>1</sup></th>';
		$x.='<th style="width:20%;">'.$Lang['StudentAttendance']['SchoolDayKPM'].'<sup>2</sup></th>';
		$x.='<th style="width:20%;">'.$Lang['LessonAttendance']['AttendanceRate'].'<sup>3</sup></th>';
		$colspan = 6;
	} else if($TargetType == 'class') {
		$x.='<th class="num_check" width="1">#</th>';
		$x.='<th style="width:20%;">'.$Lang['StudentAttendance']['Class'].'</th>';
		$x.='<th style="width:20%;">'.$i_StudentAttendance_ReportHeader_NumStudents.'</th>';
		$x.='<th style="width:20%;">'.$Lang['StudentAttendance']['NumberOfStudendsAttended'] .'<sup>1</sup></th>';
		$x.='<th style="width:20%;">'.$Lang['StudentAttendance']['SchoolDayKPM'].'<sup>2</sup></th>';
		$x.='<th style="width:20%;">'.$Lang['LessonAttendance']['AttendanceRate'].'<sup>3</sup></th>';
		$colspan = 6;
	} else if($TargetType == 'student') {
		$x.='<th class="num_check" width="1">#</th>';
		$x.='<th style="width:20%;">'.$Lang['StudentAttendance']['Class'].'</th>';
		$x.='<th style="width:15%;">'.$Lang['StudentAttendance']['ClassNumber'].'</th>';
		$x.='<th style="width:20%;">'.$i_UserStudentName.'</th>';
		$x.='<th style="width:15%;">'.$Lang['StudentAttendance']['NumberOfDaysAttended'] .'<sup>1</sup></th>';
		$x.='<th style="width:15%;">'.$Lang['StudentAttendance']['SchoolDayKPM'].'<sup>2</sup></th>';
		$x.='<th style="width:15%;">'.$Lang['LessonAttendance']['AttendanceRate'].'<sup>3</sup></th>';
		$colspan = 7;
	}
	$x.= '</tr>'."\n";
	$x.='</thead>';
	$x.='<tbody>';

	$reminder_text = '';
	if(count($student_attendance_data)==0){
		$x.='<tr>';
		$x.='<td colspan="'.$colspan.'" style="text-align:center;">'.$Lang['General']['NoRecordAtThisMoment'].'</td>';
		$x.='</tr>'."\n";
	}else{
		if($TargetType == 'form' || $TargetType == 'class') {
			$reminder_text = $Lang['StudentAttendance']['DaysReminderTextFormClass'];
			$i = 0;
			foreach($form_attendance_data as $key=>$records) {
				$totalPresent = 0;
				$totalSchoolDay = 0;
				$attendance_rate = 0;
				$total_students = 0;
				foreach ($records as $record) {
					$totalPresent += $record['totalPresent'];
					$totalSchoolDay += $record['totalSchoolDay'];
					$total_students++;
				}

				if($totalSchoolDay > 0) {
					$attendance_rate = ($totalPresent / $totalSchoolDay) * 100;
				} else {
					$attendance_rate = 0;
				}
				$attendance_rate = number_format($attendance_rate, 2);
				$x .= '<tr>';
				$x .= '<td>' . ($i + 1) . '</td>';
				$x .= '<td>' . Get_String_Display($key, 1) . '</td>';
				$x .= '<td>' . Get_String_Display($total_students, 1) . '</td>';
				$x .= '<td>' . Get_String_Display($totalPresent, 1) . '</td>';
				$x .= '<td>' . Get_String_Display($totalSchoolDay, 1) . '</td>';
				$x .= '<td>' . Get_String_Display($attendance_rate, 1) . '%</td>';
				$x .= '</tr>' . "\n";
				$i++;
			}
		} else if($TargetType == 'student') {
			$reminder_text = $Lang['StudentAttendance']['DaysReminderText'];
			$i = 0;
			foreach ($student_attendance_data as $record) {
				if ($record['totalSchoolDay'] > 0) {
					$attendance_rate = ($record['totalPresent'] / $record['totalSchoolDay']) * 100;
				} else {
					$attendance_rate = 0;
				}
				$attendance_rate = number_format($attendance_rate, 2);
				$x .= '<tr>';
				$x .= '<td>' . ($i + 1) . '</td>';
				$x .= '<td>' . Get_String_Display($record['ClassName'], 1) . '</td>';
				$x .= '<td>' . Get_String_Display($record['ClassNumber'], 1) . '</td>';
				$x .= '<td>' . Get_String_Display($record['StudentName'], 1) . '</td>';
				$x .= '<td>' . Get_String_Display($record['totalPresent'], 1) . '</td>';
				$x .= '<td>' . Get_String_Display($record['totalSchoolDay'], 1) . '</td>';
				$x .= '<td>' . Get_String_Display($attendance_rate, 1) . '%</td>';
				$x .= '</tr>' . "\n";
				$i++;
			}
		}
	}

	$x .= '<tr><td class="tabletextremark" colspan="'.$colspan.'"><span>'.$reminder_text.'</span></td></tr>';
	$x .= '</tbody>';
	$x .= '</table>'."\n";

	echo $x;
}

if($Format == "print")
{
	$x = '<h3>'.$Lang['StudentAttendance']['StudentAttendanceRate'].' ('.$StartDate.' '.$Lang['StaffAttendance']['To'].' '.$EndDate.')</h3>';

	$x .= '<table width="98%" align="center" class="print_hide" border="0">
			<tr>
				<td align="right">'.$linterface->GET_BTN($Lang['Btn']['Print'], "button", "javascript:window.print();","printBtn").'</td>
			</tr>
		</table>';

	$x .= '<table width="98%" border="0" cellspacing="0" cellpadding="5" align="center">
			 <tr>
				<td align="center"><h2>'.$Lang['StudentAttendance']['StudentAttendanceRate'].'</h2></td>
			 </tr>
			 <tr>
				<td align="right">'.$i_general_report_creation_time.': '.date("Y-m-d H:i:s").'</td>
			 </tr>
		   </table>';
	$x .= '<table align="center" class="eSporttableborder" border="0" cellpadding="2" cellspacing="0" width="98%">
		  	<tbody>
		  	<tr class="tabletop">';

	if($TargetType == 'form') {
		$x .= '<th class="eSporttdborder eSportprinttabletitle" style="width:1;">#</th>
			    <th class="eSporttdborder eSportprinttabletitle" style="width:20%;">' . $Lang['AccountMgmt']['Form'] . '</th>
				<th class="eSporttdborder eSportprinttabletitle" style="width:20%;">' . $i_StudentAttendance_ReportHeader_NumStudents . '</th>
				<th class="eSporttdborder eSportprinttabletitle" style="width:20%;">' . $Lang['StudentAttendance']['NumberOfStudendsAttended'] . '</th>
				<th class="eSporttdborder eSportprinttabletitle" style="width:20%;">' . $Lang['StudentAttendance']['SchoolDayKPM'] . '</th>
				<th class="eSporttdborder eSportprinttabletitle" style="width:20%;">' . $Lang['LessonAttendance']['AttendanceRate'] . '</th>';
		$colspan = 6;
	} else if($TargetType == 'class') {
		$x .= '<th class="eSporttdborder eSportprinttabletitle" style="width:1;">#</th>
			    <th class="eSporttdborder eSportprinttabletitle" style="width:20%;">' . $Lang['StudentAttendance']['Class'] . '</th>
				<th class="eSporttdborder eSportprinttabletitle" style="width:20%;">' . $i_StudentAttendance_ReportHeader_NumStudents . '</th>
				<th class="eSporttdborder eSportprinttabletitle" style="width:20%;">' . $Lang['StudentAttendance']['NumberOfStudendsAttended'] . '</th>
				<th class="eSporttdborder eSportprinttabletitle" style="width:20%;">' . $Lang['StudentAttendance']['SchoolDayKPM'] . '</th>
				<th class="eSporttdborder eSportprinttabletitle" style="width:20%;">' . $Lang['LessonAttendance']['AttendanceRate'] . '</th>';
		$colspan = 6;
	} else if($TargetType == 'student') {
		$x .= '<th class="eSporttdborder eSportprinttabletitle" style="width:1;">#</th>
			    <th class="eSporttdborder eSportprinttabletitle" style="width:20%;">' . $Lang['StudentAttendance']['Class'] . '</th>
				<th class="eSporttdborder eSportprinttabletitle" style="width:15%;">' . $Lang['StudentAttendance']['ClassNumber'] . '</th>
				<th class="eSporttdborder eSportprinttabletitle" style="width:20%;">' . $i_UserStudentName . '</th>
				<th class="eSporttdborder eSportprinttabletitle" style="width:15%;">' . $Lang['StudentAttendance']['NumberOfDaysAttended'] . '</th>
				<th class="eSporttdborder eSportprinttabletitle" style="width:15%;">' . $Lang['StudentAttendance']['SchoolDayKPM'] . '</th>
				<th class="eSporttdborder eSportprinttabletitle" style="width:15%;">' . $Lang['LessonAttendance']['AttendanceRate'] . '</th>';
		$colspan = 7;
	}
	$x .= '</tr>';

	if(count($student_attendance_data) == 0){
		$x.='<tr>';
		$x.='<td class="eSporttdborder eSportprinttext" colspan="'.$colspan.'" style="text-align:center;">'.$Lang['General']['NoRecordAtThisMoment'].'</td>';
		$x.='</tr>'."\n";
	}

	if($TargetType == 'form' || $TargetType == 'class') {
		$i = 0;
		foreach($form_attendance_data as $key=>$records) {
			$totalPresent = 0;
			$totalSchoolDay = 0;
			$attendance_rate = 0;
			$total_students = 0;
			foreach ($records as $record) {
				$totalPresent += $record['totalPresent'];
				$totalSchoolDay += $record['totalSchoolDay'];
				$total_students++;
			}

			if($totalSchoolDay > 0) {
				$attendance_rate = ($totalPresent / $totalSchoolDay) * 100;
			} else {
				$attendance_rate = 0;
			}
			$attendance_rate = number_format($attendance_rate, 2);
			$x .= '<tr>';
			$x .= '<td class="eSporttdborder eSportprinttext">' . ($i + 1) . '</td>';
			$x .= '<td class="eSporttdborder eSportprinttext">' . Get_String_Display($key, 1) . '</td>';
			$x .= '<td class="eSporttdborder eSportprinttext">' . Get_String_Display($total_students, 1) . '</td>';
			$x .= '<td class="eSporttdborder eSportprinttext">' . Get_String_Display($totalPresent, 1) . '</td>';
			$x .= '<td class="eSporttdborder eSportprinttext">' . Get_String_Display($totalSchoolDay) . '</td>';
			$x .= '<td class="eSporttdborder eSportprinttext">' . Get_String_Display($attendance_rate, 1) . '%</td>';
			$x .= '</tr>' . "\n";
			$i++;
		}
	} else if($TargetType == 'student') {
		$i = 0;
		foreach ($student_attendance_data as $record) {
			if ($record['totalSchoolDay'] > 0) {
				$attendance_rate = ($record['totalPresent'] / $record['totalSchoolDay']) * 100;
			} else {
				$attendance_rate = 0;
			}
			$attendance_rate = number_format($attendance_rate, 2);

			$x .= '<tr>';
			$x .= '<td class="eSporttdborder eSportprinttext">' . ($i + 1) . '</td>';
			$x .= '<td class="eSporttdborder eSportprinttext">' . Get_String_Display($record['ClassName'], 1) . '</td>';
			$x .= '<td class="eSporttdborder eSportprinttext">' . Get_String_Display($record['ClassNumber'], 1) . '</td>';
			$x .= '<td class="eSporttdborder eSportprinttext">' . Get_String_Display($record['StudentName'], 1) . '</td>';
			$x .= '<td class="eSporttdborder eSportprinttext">' . Get_String_Display($record['totalPresent']) . '</td>';
			$x .= '<td class="eSporttdborder eSportprinttext">' . Get_String_Display($record['totalSchoolDay'], 1) . '</td>';
			$x .= '<td class="eSporttdborder eSportprinttext">' . Get_String_Display($attendance_rate, 1) . '%</td>';
			$x .= '</tr>' . "\n";
			$i++;
		}
	}

	$x .= '</tbody>';
	$x .= '</table>';
	$x .= '<br />';


	include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_header.php");
	echo $x;
	include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_footer.php");

}

if($Format == "csv")
{
	include_once ($PATH_WRT_ROOT."includes/libexporttext.php");
	$lexport = new libexporttext();

	$rows = array();
	$headers = array();
	$headers[] = array($Lang['StudentAttendance']['StudentAttendanceRate'].' ('.$StartDate.' '.$Lang['StaffAttendance']['To'].' '.$EndDate.')', $i_general_report_creation_time.': '.date("Y-m-d H:i:s"));

	$csv_header = array();
	if($TargetType == 'form') {
		$csv_header[] = $Lang['AccountMgmt']['Form'];
		$csv_header[] = $i_StudentAttendance_ReportHeader_NumStudents;
		$csv_header[] = $Lang['StudentAttendance']['NumberOfStudendsAttended'];
		$csv_header[] = $Lang['StudentAttendance']['SchoolDayKPM'];
		$csv_header[] = $Lang['LessonAttendance']['AttendanceRate'];
	} else if($TargetType == 'class') {
		$csv_header[] = $Lang['StudentAttendance']['Class'];
		$csv_header[] = $i_StudentAttendance_ReportHeader_NumStudents;
		$csv_header[] = $Lang['StudentAttendance']['NumberOfStudendsAttended'];
		$csv_header[] = $Lang['StudentAttendance']['SchoolDayKPM'];
		$csv_header[] = $Lang['LessonAttendance']['AttendanceRate'];
	} else if($TargetType == 'student') {
		$csv_header[] = $Lang['StudentAttendance']['Class'];
		$csv_header[] = $Lang['StudentAttendance']['ClassNumber'];
		$csv_header[] = $i_UserStudentName;
		$csv_header[] = $Lang['StudentAttendance']['NumberOfDaysAttended'];
		$csv_header[] = $Lang['StudentAttendance']['SchoolDayKPM'];
		$csv_header[] = $Lang['LessonAttendance']['AttendanceRate'];
	}
	$headers[] = $csv_header;

	if($TargetType == 'form' || $TargetType == 'class') {
		$i = 0;
		foreach($form_attendance_data as $key=>$records) {
			$row = array();
			$totalPresent = 0;
			$totalSchoolDay = 0;
			$attendance_rate = 0;
			$total_students = 0;
			foreach ($records as $record) {
				$totalPresent += $record['totalPresent'];
				$totalSchoolDay += $record['totalSchoolDay'];
				$total_students++;
			}

			if($totalSchoolDay > 0) {
				$attendance_rate = ($totalPresent / $totalSchoolDay) * 100;
			} else {
				$attendance_rate = 0;
			}
			$attendance_rate = number_format($attendance_rate, 2);
			$row[] = $key;
			$row[] = $total_students;
			$row[] = $totalPresent;
			$row[] = $totalSchoolDay;
			$row[] = $attendance_rate . '%';

			$rows[] = $row;
		}

	} else if($TargetType == 'student') {
		foreach ($student_attendance_data as $record) {
			$row = array();
			if ($record['totalSchoolDay'] > 0) {
				$attendance_rate = ($record['totalPresent'] / $record['totalSchoolDay']) * 100;
			} else {
				$attendance_rate = 0;
			}
			$attendance_rate = number_format($attendance_rate, 2);

			$row[] = $record['ClassName'];
			$row[] = $record['ClassNumber'];
			$row[] = $record['StudentName'];
			$row[] = $record['totalPresent'];
			$row[] = $record['totalSchoolDay'];
			$row[] = $attendance_rate . '%';

			$rows[] = $row;
		}
	}
	$exportContent = $lexport->GET_EXPORT_TXT($rows, $headers);
	$lexport->EXPORT_FILE('student_attendance_rate_records.csv', $exportContent);
}

intranet_closedb();
?>