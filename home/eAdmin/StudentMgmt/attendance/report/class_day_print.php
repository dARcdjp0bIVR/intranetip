<?php
//using by 
/*
 * 2019-10-31 (Ray)   : Added date range select
 * 2017-02-02 (Carlos): Added display options [Login ID], [Reason], [Teacher's remarks], [Office Remark].
 * 2013-11-06 (Carlos): Modified to show class or group attendance data
 */
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();

if (!(isset($ClassID) || isset($GroupID)) || !isset($TargetDate)) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

function getAttendanceReasonsRemarks($targetType, $targetId, $targetDate)
{
	global $lc;
	
	$sql = "SELECT 
				b.StudentID,
				b.RecordDate,
				b.DayType,
				b.RecordType,
				b.Reason,
				b.OfficeRemark 
			FROM INTRANET_USER as a ";
	if($targetType == 'GROUP'){
		$sql .= " INNER JOIN INTRANET_USERGROUP as ug ON ug.UserID=a.UserID ";
	}		
	$sql.= " INNER JOIN CARD_STUDENT_PROFILE_RECORD_REASON as b ON b.StudentID=a.UserID 
			WHERE ".($targetType == 'GROUP'? " ug.GroupID='$targetId' " : " a.ClassName = '$targetId' ")." 
				AND a.RecordType = 2 AND a.RecordStatus IN (0,1,2) AND b.RecordDate='$targetDate' AND (b.RecordStatus IS NULL OR b.RecordStatus='0') 
           ORDER BY a.ClassName, a.ClassNumber+0, a.EnglishName";
	$reason_records = $lc->returnResultSet($sql);
	$reason_size = count($reason_records);
	
	$sql = "SELECT 
				b.StudentID,
				b.RecordDate,
				b.DayType,
				b.Remark 
			FROM INTRANET_USER as a ";
	if($targetType == 'GROUP'){
		$sql .= " INNER JOIN INTRANET_USERGROUP as ug ON ug.UserID=a.UserID ";
	}		
	$sql.= " INNER JOIN CARD_STUDENT_DAILY_REMARK as b ON b.StudentID=a.UserID 
			WHERE ".($targetType == 'GROUP'? " ug.GroupID='$targetId' " : " a.ClassName = '$targetId' ")." 
				AND a.RecordType = 2 AND a.RecordStatus IN (0,1,2) AND b.RecordDate='$targetDate' 
           ORDER BY a.ClassName, a.ClassNumber+0, a.EnglishName";
	$remark_records = $lc->returnResultSet($sql);
	$remark_size = count($remark_records);
	
	$assocAry = array();
	for($i=0;$i<$reason_size;$i++){
		$student_id = $reason_records[$i]['StudentID'];
		$record_date = $reason_records[$i]['RecordDate'];
		$day_type = $reason_records[$i]['DayType'];
		$record_type = $reason_records[$i]['RecordType'];
		$reason = $reason_records[$i]['Reason'];
		$office_remark = $reason_records[$i]['OfficeRemark'];
		if(!isset($assocAry[$student_id])){
			$assocAry[$student_id] = array();
		}
		if(!isset($assocAry[$student_id][$record_date])){
			$assocAry[$student_id][$record_date] = array();
		}
		if(!isset($assocAry[$student_id][$record_date][$day_type])){
			$assocAry[$student_id][$record_date][$day_type] = array();
		}
		if(!isset($assocAry[$student_id][$record_date][$day_type][$record_type])){
			$assocAry[$student_id][$record_date][$day_type][$record_type] = array();
		}
		
		$assocAry[$student_id][$record_date][$day_type][$record_type]['Reason'] = $reason;
		$assocAry[$student_id][$record_date][$day_type][$record_type]['OfficeRemark'] = $office_remark;
	}
	
	$assocAry2 = array();
	for($i=0;$i<$remark_size;$i++){
		$student_id = $remark_records[$i]['StudentID'];
		$record_date = $remark_records[$i]['RecordDate'];
		$day_type = $remark_records[$i]['DayType'];
		$remark = $remark_records[$i]['Remark'];
		if($remark == '') continue;
		if(!isset($assocAry2[$student_id])){
			$assocAry2[$student_id] = array();
		}
		if(!isset($assocAry2[$student_id][$record_date])){
			$assocAry2[$student_id][$record_date] = array();
		}
		if(!isset($assocAry2[$student_id][$record_date][$day_type])){
			$assocAry2[$student_id][$record_date][$day_type] = array();
		}
		
		$assocAry2[$student_id][$record_date][$day_type]['Remark'] = $remark;
	}
	
	return array($assocAry,$assocAry2);
}

$linterface = new interface_html();

include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_header.php");

$lclass = new libclass();

if($ShowAllColumns == "0") # Data Columns are Optionally Shown
{
	$show_loginid = $ColumnLoginID == "1";
	$show_studentname = $ColumnStudentName == "1";
	$show_classno = $ColumnClassNo == "1";
	if($sys_custom['SmartCardAttendance_StudentAbsentSession'])
	{
		$show_session_info = $ColumnSession == "1";
	}
	$show_reason = $ColumnReason == "1";
	$show_remark = $ColumnRemark == "1";
	$show_office_remark = $ColumnOfficeRemark == "1";
}else # Default All Columns are shown
{
	$show_loginid = true;
	$show_studentname = true;
	$show_classno = true;
	if($sys_custom['SmartCardAttendance_StudentAbsentSession']) $show_session_info = true;
	$show_reason = true;
	$show_remark = true;
	$show_office_remark = true;
}


$TargetType = $_REQUEST['TargetType'];
if($TargetType == 'CLASS'){
	$TargetIDArr = explode(",", $ClassID);
}else if($TargetType == 'GROUP'){
	$TargetIDArr = explode(",", $GroupID);
}

$lc->retrieveSettings();

$ts = strtotime($TargetDate);
if ($ts == -1 || $TargetDate == "") {
	$TargetDate = date('Y-m-d');
}
$ts = strtotime($StartDate);
if ($ts == -1 || $StartDate == "") {
	$StartDate = date('Y-m-d');
}

$StartDate = $TargetDate;
$datediff = strtotime($EndDate) - strtotime($StartDate);
$NoOfDate = floor($datediff / (60 * 60 * 24));
$display = "";
for($_i=0;$_i<=$NoOfDate;$_i++) {
	$ts = strtotime($StartDate) + ($_i * 60 * 60 * 24);
	$TargetDate = date("Y-m-d", $ts);

	if ($ts == -1 || $TargetDate == "") {
		$TargetDate = date('Y-m-d');
		$year = date('Y');
		$month = date('m');
		$day = date('d');
	} else {
		$year = date('Y', $ts);
		$month = date('m', $ts);
		$day = date('d', $ts);
	}

	for ($j = 0; $j < sizeof($TargetIDArr); $j++) {
		if ($TargetType == 'CLASS') {
			$TargetName = $lclass->getClassName($TargetIDArr[$j]);
			$need_to_take_attendance = $lc->isRequiredToTakeAttendanceByDate($TargetName, $TargetDate);
		} else if ($TargetType = 'GROUP') {
			$lgroup = new libgroup($TargetIDArr[$j]);
			$TargetName = $lgroup->TitleDisplay;
			$need_to_take_attendance = $lc->groupIsRequiredToTakeAttendanceByDate($TargetIDArr[$j], $TargetDate);
		}


		if ($need_to_take_attendance) {

			if ($TargetType == 'CLASS') {
				$result = $lc->retrieveClassDayData($TargetName, $year, $month, $day);
			} else if ($TargetType == 'GROUP') {
				$result = $lc->retrieveGroupDayData($TargetIDArr[$j], $year, $month, $day);
			}
			if ($show_reason || $show_remark || $show_office_remark) {
				$reasons_remarks = getAttendanceReasonsRemarks($TargetType, $TargetType == 'GROUP' ? $TargetID[$j] : $TargetName, $TargetDate);
				$reason_records = $reasons_remarks[0];
				$remark_records = $reasons_remarks[1];
			}
			$space = $intranet_session_language == "en" ? " " : "";

			$display .= "<table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">
				<tr>
					<td class=\"eSportprinttext\">$TargetName ($TargetDate)</td>
				</tr>
				</table>";
			$display .= "<table class=\"eSporttableborder\" width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">";
			$display .= "<tr>";
			if ($show_loginid) $display .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_UserLogin</td>";
			if ($show_studentname) $display .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_UserStudentName</td>";
			if ($show_classno) $display .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_ClassNameNumber</td>";
			$display .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_StudentAttendance_Field_InSchoolTime</td>";
			$display .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_StudentAttendance_Field_CardStation</td>";

			if ($lc->attendance_mode == 0 || $lc->attendance_mode == 2 || $lc->attendance_mode == 3) {
				$display .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_StudentAttendance_Slot_AM$space$i_SmartCard_Frontend_Take_Attendance_PreStatus</td>\n";
			} else {
				$display .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_StudentAttendance_Slot_PM$space$i_SmartCard_Frontend_Take_Attendance_PreStatus</td>\n";
			}

			if ($sys_custom['SmartCardAttendance_StudentAbsentSession'] && $show_session_info) {
				$display .= "<td class=\"eSporttdborder eSportprinttabletitle\">" . $Lang['StudentAttendance']['Late'] . "</td>";
				$display .= "<td class=\"eSporttdborder eSportprinttabletitle\">" . $Lang['StudentAttendance']['RequestLeave'] . "</td>";
				$display .= "<td class=\"eSporttdborder eSportprinttabletitle\">" . $Lang['StudentAttendance']['PlayTruant'] . "</td>";
				$display .= "<td class=\"eSporttdborder eSportprinttabletitle\">" . $Lang['StudentAttendance']['OfficalLeave'] . "</td>";
			}

			// AM
			if ($lc->attendance_mode == 0 || $lc->attendance_mode == 2 || $lc->attendance_mode == 3) {
				if ($show_reason) {
					$display .= "<td class=\"eSporttdborder eSportprinttabletitle\">" . $i_StudentAttendance_Slot_AM . $space . $i_Attendance_Reason . "</td>\n";
				}
				if ($show_remark) {
					$display .= "<td class=\"eSporttdborder eSportprinttabletitle\">" . $i_StudentAttendance_Slot_AM . $space . $Lang['StudentAttendance']['iSmartCardRemark'] . "</td>\n";
				}
				if ($show_office_remark) {
					$display .= "<td class=\"eSporttdborder eSportprinttabletitle\">" . $i_StudentAttendance_Slot_AM . $space . $Lang['StudentAttendance']['OfficeRemark'] . "</td>\n";
				}
			}

			if ($lc->attendance_mode == 2)      # With Lunch Out
			{
				$display .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_StudentAttendance_Field_LunchOutTime</td>";
				$display .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_StudentAttendance_Field_CardStation</td>\n";
				$display .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_StudentAttendance_Field_LunchBackTime</td>";
				$display .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_StudentAttendance_Field_CardStation</td>\n";
			}
			if ($lc->attendance_mode == 2 || $lc->attendance_mode == 3) {
				$display .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_StudentAttendance_Slot_PM$space$i_SmartCard_Frontend_Take_Attendance_PreStatus</td>\n";
			}

			// PM
			if ($lc->attendance_mode == 1 || $lc->attendance_mode == 2 || $lc->attendance_mode == 3) {
				if ($show_reason) {
					$display .= "<td class=\"eSporttdborder eSportprinttabletitle\">" . $i_StudentAttendance_Slot_PM . $space . $i_Attendance_Reason . "</td>\n";
				}
				if ($show_remark) {
					$display .= "<td class=\"eSporttdborder eSportprinttabletitle\">" . $i_StudentAttendance_Slot_PM . $space . $Lang['StudentAttendance']['iSmartCardRemark'] . "</td>\n";
				}
				if ($show_office_remark) {
					$display .= "<td class=\"eSporttdborder eSportprinttabletitle\">" . $i_StudentAttendance_Slot_PM . $space . $Lang['StudentAttendance']['OfficeRemark'] . "</td>\n";
				}
			}

			if ($sys_custom['SmartCardAttendance_StudentAbsentSession'] && $show_session_info) {
				$display .= "<td class=\"eSporttdborder eSportprinttabletitle\">" . $Lang['StudentAttendance']['Late'] . "</td>";
				$display .= "<td class=\"eSporttdborder eSportprinttabletitle\">" . $Lang['StudentAttendance']['RequestLeave'] . "</td>";
				$display .= "<td class=\"eSporttdborder eSportprinttabletitle\">" . $Lang['StudentAttendance']['PlayTruant'] . "</td>";
				$display .= "<td class=\"eSporttdborder eSportprinttabletitle\">" . $Lang['StudentAttendance']['OfficalLeave'] . "</td>";
			}

			$display .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_StudentAttendance_Field_LeaveTime</td>";
			$display .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_StudentAttendance_Field_CardStation</td>\n";
			$display .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_StudentAttendance_Type_LeaveSchool$space$i_SmartCard_Frontend_Take_Attendance_PreStatus</td>\n";

			// Leave
			if ($show_reason) {
				$display .= "<td class=\"eSporttdborder eSportprinttabletitle\">" . $i_StudentAttendance_Type_LeaveSchool . $space . $i_Attendance_Reason . "</td>\n";
			}
			if ($show_remark) {
				$display .= "<td class=\"eSporttdborder eSportprinttabletitle\">" . $i_StudentAttendance_Type_LeaveSchool . $space . $Lang['StudentAttendance']['iSmartCardRemark'] . "</td>\n";
			}
			if ($show_office_remark) {
				$display .= "<td class=\"eSporttdborder eSportprinttabletitle\">" . $i_StudentAttendance_Type_LeaveSchool . $space . $Lang['StudentAttendance']['OfficeRemark'] . "</td>\n";
			}

			$display .= "</tr>\n";

			for ($i = 0; $i < sizeof($result); $i++) {
				list($studentid, $student_name, $student_class, $classnum, $inTime, $inStation,
					$am, $lunchOutTime, $lunchOutStation, $lunchBackTime, $lunchBackStation,
					$pm, $leaveSchoolTime, $leaveSchoolStation, $leave,
					$amLateWaive, $pmLateWaive, $amAbsentWaive, $pmAbsentWaive, $amEarlyWaive, $pmEarlyWaive,
					$am_late_session, $am_request_leave_session, $am_play_truant_session, $am_offical_leave_session,
					$pm_late_session, $pm_request_leave_session, $pm_play_truant_session, $pm_offical_leave_session) = $result[$i];
				$am_late_session = $am_late_session == "" ? 0 : $am_late_session;
				$am_request_leave_session = $am_request_leave_session == "" ? 0 : $am_request_leave_session;
				$am_play_truant_session = $am_play_truant_session == "" ? 0 : $am_play_truant_session;
				$am_offical_leave_session = $am_offical_leave_session == "" ? 0 : $am_offical_leave_session;
				$pm_late_session = $pm_late_session == "" ? 0 : $pm_late_session;
				$pm_request_leave_session = $pm_request_leave_session == "" ? 0 : $pm_request_leave_session;
				$pm_play_truant_session = $pm_play_truant_session == "" ? 0 : $pm_play_truant_session;
				$pm_offical_leave_session = $pm_offical_leave_session == "" ? 0 : $pm_offical_leave_session;
				$raw_am = $am;
				if ($raw_am == CARD_STATUS_OUTING) $raw_am = CARD_STATUS_ABSENT;
				$raw_pm = $pm;
				if ($raw_pm == CARD_STATUS_OUTING) $raw_pm = CARD_STATUS_ABSENT;
				$raw_leave = $leave;
				switch ($am) {
					case "0" :
						$am = $i_StudentAttendance_Status_OnTime;
						break;
					case "1" :
						$am = $i_StudentAttendance_Status_Absent;
						break;
					case "2" :
						$am = $i_StudentAttendance_Status_Late;
						break;
					case "3" :
						$am = $i_StudentAttendance_Status_Outing;
						break;
					default :
						$am = $i_StudentAttendance_Status_Absent;
				}

				switch ($pm) {
					case "0" :
						$pm = $i_StudentAttendance_Status_OnTime;
						break;
					case "1" :
						$pm = $i_StudentAttendance_Status_Absent;
						break;
					case "2" :
						$pm = $i_StudentAttendance_Status_Late;
						break;
					case "3" :
						$pm = $i_StudentAttendance_Status_Outing;
						break;
					default :
						$pm = $i_StudentAttendance_Status_Absent;
				}
				switch ($leave) {
					case "0" :
						$leave = $Lang['StudentAttendance']['NormalLeave'];
						break;
					case "1":
						$leave = $i_StudentAttendance_Status_EarlyLeave;
						break;
					case "2":
						$leave = $i_StudentAttendance_Status_EarlyLeave;
						break;
				}

				$display .= "<tr>";
				if ($show_loginid) $display .= "<td class=\"eSporttdborder eSportprinttext\">" . $result[$i]['UserLogin'] . "</td>";
				if ($show_studentname) $display .= "<td class=\"eSporttdborder eSportprinttext\">$student_name</td>";
				if ($show_classno) $display .= "<td class=\"eSporttdborder eSportprinttext\">" . ($TargetType == 'CLASS' ? $classnum : $student_class . "/" . $classnum) . "</td>\n";

				$display .= "<td class=\"eSporttdborder eSportprinttext\">$inTime&nbsp;</td>";
				$display .= "<td class=\"eSporttdborder eSportprinttext\">$inStation&nbsp;</td>\n";
				if ($lc->attendance_mode == 0 || $lc->attendance_mode == 2 || $lc->attendance_mode == 3) {
					$display .= "<td class=\"eSporttdborder eSportprinttext\">$am&nbsp;</td>\n";
				} else {
					$display .= "<td class=\"eSporttdborder eSportprinttext\">$pm&nbsp;</td>\n";
				}

				if ($sys_custom['SmartCardAttendance_StudentAbsentSession'] && $show_session_info) {
					$display .= "<td class=\"eSporttdborder eSportprinttext\">" . $am_late_session . "</td>";
					$display .= "<td class=\"eSporttdborder eSportprinttext\">" . $am_request_leave_session . "</td>";
					$display .= "<td class=\"eSporttdborder eSportprinttext\">" . $am_play_truant_session . "</td>";
					$display .= "<td class=\"eSporttdborder eSportprinttext\">" . $am_offical_leave_session . "</td>";
				}

				// AM
				if ($lc->attendance_mode == 0 || $lc->attendance_mode == 2 || $lc->attendance_mode == 3) {
					if ($show_reason) {
						$display_reason = '';
						if (isset($reason_records[$studentid]) && isset($reason_records[$studentid][$TargetDate])
							&& isset($reason_records[$studentid][$TargetDate][PROFILE_DAY_TYPE_AM]) && isset($reason_records[$studentid][$TargetDate][PROFILE_DAY_TYPE_AM][$raw_am])) {
							$display_reason = $reason_records[$studentid][$TargetDate][PROFILE_DAY_TYPE_AM][$raw_am]['Reason'];
						}
						$display .= "<td class=\"eSporttdborder eSportprinttext\">" . $display_reason . "&nbsp;</td>\n";
					}
					if ($show_remark) {
						$display_remark = '';
						if (isset($remark_records[$studentid]) && isset($remark_records[$studentid][$TargetDate])
							&& isset($remark_records[$studentid][$TargetDate][PROFILE_DAY_TYPE_AM])) {
							$display_remark = $remark_records[$studentid][$TargetDate][PROFILE_DAY_TYPE_AM]['Remark'];
						}
						$display .= "<td class=\"eSporttdborder eSportprinttext\">" . $display_remark . "&nbsp;</td>\n";
					}
					if ($show_office_remark) {
						$display_office_remark = '';
						if (isset($reason_records[$studentid]) && isset($reason_records[$studentid][$TargetDate])
							&& isset($reason_records[$studentid][$TargetDate][PROFILE_DAY_TYPE_AM]) && isset($reason_records[$studentid][$TargetDate][PROFILE_DAY_TYPE_AM][$raw_am])) {
							$display_office_remark = $reason_records[$studentid][$TargetDate][PROFILE_DAY_TYPE_AM][$raw_am]['OfficeRemark'];
						}
						$display .= "<td class=\"eSporttdborder eSportprinttext\">" . $display_office_remark . "&nbsp;</td>\n";
					}
				}

				if ($lc->attendance_mode == 2)      # With Lunch Out
				{
					$display .= "<td class=\"eSporttdborder eSportprinttext\">$lunchOutTime&nbsp;</td>";
					$display .= "<td class=\"eSporttdborder eSportprinttext\">$lunchOutStation&nbsp;</td>\n";
					$display .= "<td class=\"eSporttdborder eSportprinttext\">$lunchBackTime&nbsp;</td>";
					$display .= "<td class=\"eSporttdborder eSportprinttext\">$lunchBackStation&nbsp;</td>\n";
				}
				if ($lc->attendance_mode == 2 || $lc->attendance_mode == 3) {
					$display .= "<td class=\"eSporttdborder eSportprinttext\">$pm&nbsp;</td>\n";
				}

				// PM
				if ($lc->attendance_mode == 1 || $lc->attendance_mode == 2 || $lc->attendance_mode == 3) {
					if ($show_reason) {
						$display_reason = '';
						if (isset($reason_records[$studentid]) && isset($reason_records[$studentid][$TargetDate])
							&& isset($reason_records[$studentid][$TargetDate][PROFILE_DAY_TYPE_PM]) && isset($reason_records[$studentid][$TargetDate][PROFILE_DAY_TYPE_PM][$raw_pm])) {
							$display_reason = $reason_records[$studentid][$TargetDate][PROFILE_DAY_TYPE_PM][$raw_pm]['Reason'];
						}
						$display .= "<td class=\"eSporttdborder eSportprinttext\">" . $display_reason . "&nbsp;</td>\n";
					}
					if ($show_remark) {
						$display_remark = '';
						if (isset($remark_records[$studentid]) && isset($remark_records[$studentid][$TargetDate])
							&& isset($remark_records[$studentid][$TargetDate][PROFILE_DAY_TYPE_PM])) {
							$display_remark = $remark_records[$studentid][$TargetDate][PROFILE_DAY_TYPE_PM]['Remark'];
						}
						$display .= "<td class=\"eSporttdborder eSportprinttext\">" . $display_remark . "&nbsp;</td>\n";
					}
					if ($show_office_remark) {
						$display_office_remark = '';
						if (isset($reason_records[$studentid]) && isset($reason_records[$studentid][$TargetDate])
							&& isset($reason_records[$studentid][$TargetDate][PROFILE_DAY_TYPE_PM]) && isset($reason_records[$studentid][$TargetDate][PROFILE_DAY_TYPE_PM][$raw_pm])) {
							$display_office_remark = $reason_records[$studentid][$TargetDate][PROFILE_DAY_TYPE_PM][$raw_pm]['OfficeRemark'];
						}
						$display .= "<td class=\"eSporttdborder eSportprinttext\">" . $display_office_remark . "&nbsp;</td>\n";
					}
				}

				if ($sys_custom['SmartCardAttendance_StudentAbsentSession'] && $show_session_info) {
					$display .= "<td class=\"eSporttdborder eSportprinttext\">" . $pm_late_session . "</td>";
					$display .= "<td class=\"eSporttdborder eSportprinttext\">" . $pm_request_leave_session . "</td>";
					$display .= "<td class=\"eSporttdborder eSportprinttext\">" . $pm_play_truant_session . "</td>";
					$display .= "<td class=\"eSporttdborder eSportprinttext\">" . $pm_offical_leave_session . "</td>";
				}

				$display .= "<td class=\"eSporttdborder eSportprinttext\">$leaveSchoolTime&nbsp;</td>";
				$display .= "<td class=\"eSporttdborder eSportprinttext\">$leaveSchoolStation&nbsp;</td>\n";
				$display .= "<td class=\"eSporttdborder eSportprinttext\">$leave&nbsp;</td>\n";

				// Leave
				if ($show_reason) {
					$display_reason = '';
					if ($raw_leave == CARD_LEAVE_AM && isset($reason_records[$studentid]) && isset($reason_records[$studentid][$TargetDate])
						&& isset($reason_records[$studentid][$TargetDate][PROFILE_DAY_TYPE_AM]) && isset($reason_records[$studentid][$TargetDate][PROFILE_DAY_TYPE_AM][PROFILE_TYPE_EARLY])) {
						$display_reason = $reason_records[$studentid][$TargetDate][PROFILE_DAY_TYPE_AM][PROFILE_TYPE_EARLY]['Reason'];
					}
					if ($raw_leave == CARD_LEAVE_PM && isset($reason_records[$studentid]) && isset($reason_records[$studentid][$TargetDate])
						&& isset($reason_records[$studentid][$TargetDate][PROFILE_DAY_TYPE_PM]) && isset($reason_records[$studentid][$TargetDate][PROFILE_DAY_TYPE_PM][PROFILE_TYPE_EARLY])) {
						$display_reason = $reason_records[$studentid][$TargetDate][PROFILE_DAY_TYPE_PM][PROFILE_TYPE_EARLY]['Reason'];
					}
					$display .= "<td class=\"eSporttdborder eSportprinttext\">" . $display_reason . "&nbsp;</td>\n";
				}
				if ($show_remark) {
					$display_remark = '';
					if ($raw_leave == CARD_LEAVE_AM && isset($remark_records[$studentid]) && isset($remark_records[$studentid][$TargetDate])
						&& isset($remark_records[$studentid][$TargetDate][PROFILE_DAY_TYPE_AM])) {
						$display_remark = $remark_records[$studentid][$TargetDate][PROFILE_DAY_TYPE_AM]['Remark'];
					}
					if ($raw_leave == CARD_LEAVE_PM && isset($remark_records[$studentid]) && isset($remark_records[$studentid][$TargetDate])
						&& isset($remark_records[$studentid][$TargetDate][PROFILE_DAY_TYPE_PM])) {
						$display_remark = $remark_records[$studentid][$TargetDate][PROFILE_DAY_TYPE_PM]['Remark'];
					}
					$display .= "<td class=\"eSporttdborder eSportprinttext\">" . $display_remark . "&nbsp;</td>\n";
				}
				if ($show_office_remark) {
					$display_office_remark = '';
					if ($raw_leave == CARD_LEAVE_AM && isset($reason_records[$studentid]) && isset($reason_records[$studentid][$TargetDate])
						&& isset($reason_records[$studentid][$TargetDate][PROFILE_DAY_TYPE_AM]) && isset($reason_records[$studentid][$TargetDate][PROFILE_DAY_TYPE_AM][PROFILE_TYPE_EARLY])) {
						$display_office_remark = $reason_records[$studentid][$TargetDate][PROFILE_DAY_TYPE_AM][PROFILE_TYPE_EARLY]['OfficeRemark'];
					}
					if ($raw_leave == CARD_LEAVE_PM && isset($reason_records[$studentid]) && isset($reason_records[$studentid][$TargetDate])
						&& isset($reason_records[$studentid][$TargetDate][PROFILE_DAY_TYPE_PM]) && isset($reason_records[$studentid][$TargetDate][PROFILE_DAY_TYPE_PM][PROFILE_TYPE_EARLY])) {
						$display_office_remark = $reason_records[$studentid][$TargetDate][PROFILE_DAY_TYPE_PM][PROFILE_TYPE_EARLY]['OfficeRemark'];
					}
					$display .= "<td class=\"eSporttdborder eSportprinttext\">" . $display_office_remark . "&nbsp;</td>\n";
				}

				$display .= "</tr>\n";
			}
			$display .= "</table><br />\n";
		} else {
			$display .= "<table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">
				<tr>
					<td class=\"eSportprinttext\">$TargetName ($TargetDate)</td>
				</tr>
				</table>";
			$display .= "<table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">";
			$display .= "<tr>";
			$display .= "<td class=\"eSporttdborder eSportprinttext\" align=\"center\">$i_StudentAttendance_ClassMode_NoNeedToTakeAttendance</td>";
			$display .= "</tr>";
			$display .= "</table><br />\n";
		}
	}#end of for loop
}
$i_title = "<div class=\"eSportprinttext\">$i_StudentAttendance_Report_ClassDaily</div>";#$ClassName ($TargetDate)</div>";
?>
<table width='100%' align='center' class='print_hide' border=0>
	<tr>
		<td align='right'><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></td>
	</tr>
</table>
<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
		<td class="eSportprinttext"><?=$i_title?></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
</table>
<?=$display?>
<br />
<?
include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_footer.php");
intranet_closedb();
?>