<?php
// using 
/*
 * 2020-09-29 (Ray): add reason
 * 2016-06-16 (Carlos): Modified query to only get consolidated lesson attendance records. 
 */
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libstudentattendance_ui.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-LessonAttendance"] || !$_SESSION["SSV_PRIVILEGE"]["plugin"]["attendancelesson"]) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$FromDate = $_REQUEST['FromDate'];
$ToDate = $_REQUEST['ToDate'];
$ClassID = $_REQUEST['ClassID'];

$filter = $filter==""?2:$filter;
switch($filter){
	case 4: $cond_filter = " and sgsa1.AttendStatus = 1 "; break;
	case 2: $cond_filter = " and sgsa1.AttendStatus = 2 "; break;
	case 3: $cond_filter = " and sgsa1.AttendStatus = 3 "; break;
	case 1: $cond_filter = " and (sgsa1.AttendStatus = 2 or sgsa1.AttendStatus = 3) "; break;
	default: $cond_filter =" and (sgsa1.AttendStatus = 2 or sgsa1.AttendStatus = 3) ";
}

$LessonAttendUI = new libstudentattendance_ui();
$linterface = new interface_html();

include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_header.php");
$StudentIDArray = explode(',',$studentID);
if($studentID!=""){
	$lb = new libdb();
	//$namefield = getNameFieldWithClassNumberByLang("b.");
	/*
	$sql=" SELECT CONCAT('<a class=\'tablelink\' href=\'edit.php?RecordID=',a.RecordID,'\'>',a.RecordDate,'</a>'),
				 IF(a.DayPeriod=2,'$i_DayTypeAM','$i_DayTypePM'),
				 a.Reason,
				 IF(a.Waive = '1' AND a.Waive IS NOT NULL,'".$Lang['General']['Yes']."','".$Lang['General']['No']."') as Waive,
				 a.Remark,
				 CONCAT('<input type=''checkbox'' name=''RecordID[]'' value=', a.RecordID ,'>')
				FROM CARD_STUDENT_PRESET_LEAVE AS a WHERE a.StudentID IN ($studentID)  $cond_filter AND 
				(a.RecordDate LIKE '%$keyword%' OR a.Reason LIKE '%$keyword%' OR a.Remark LIKE '%$keyword%' )
				";
				*/
	//new sql statemant [Start]
	$cond ='';
	  	if($studentID != '')
	  		$cond = " AND a1.StudentID IN (".$studentID.") ";
	  	
	  	$StudentList = $StudentIDArray;
	  	$NameField = getNameFieldWithClassNumberByLang('u.');
		$NameField1 = getNameFieldWithClassNumberByLang('u1.');
	  	$TermInfo = getAcademicYearInfoAndTermInfoByDate(date('Y-m-d'));
		list($AcademicYearID,$AcademicYearName,$YearTermID,$YearTermName) = $TermInfo;
	  	//debug_pr($AcademicYearID." ".$studentID);
	  	/*
	  	$sql = 'select distinct
                a1.StudentID
              from
                SUBJECT_GROUP_ATTEND_OVERVIEW_'.$AcademicYearID.' as o1
                inner join
                SUBJECT_GROUP_STUDENT_ATTENDANCE_'.$AcademicYearID.' as a1
                on o1.AttendOverviewID = a1.AttendOverviewID '.$cond;
//                o1.LessonDate = \''.$TargetDate.'\''.$cond.'
//                and o1.AttendOverviewID = a1.AttendOverviewID
//                and a1.AttendStatus in ('.implode(',',$AttendStatus).')';
      $StudentList = $LessonAttendUI->returnVector($sql);
      */
      //debug_pr($sql);
      $ClassTitle = Get_Lang_Selection("stc.ClassTitleB5","stc.ClassTitleEN");
      $SubjectName = Get_Lang_Selection("s.CH_DES", "s.EN_DES");
      
      if (sizeof($StudentList) <= 0){
      	$StudentList = array(-1);
      }
      
      if (sizeof($StudentList) > 0) {
      	$tempField = " ORDER BY ";
		if($field == 1){
			$tempField .= "ClassName";
		}else if($field == 2){
			$tempField .= "ClassNumber";
		}else if($field == 3){
			$tempField .= "LessonDate1";
		}else if($field == 4){
			$tempField .= "DayPeriod1";
		}else if($field == 5){
			$tempField .= "SubjectGroupTitle";
		}else if($field == 6){
			$tempField .= "AttendStatus1";
		}else if($field == 7){
			$tempField .= "Reason";
		}else if($field == 0){
			$tempField .= "StudentName";
		}else
			$tempField ='';

		$tempOrder = "";
		if($order == 0){
			$tempOrder = " DESC";
		}
		  	$StudenCondition = implode(',',$StudentList);
		  	
				$NameField3 = getNameFieldByLang('u.');
		  		//debug_pr($NameField);
		  		/*
				$sql = "select DISTINCT ".$NameField3." as StudentName,
						IF(u.ClassNumber IS NULL OR u.ClassNumber = '','".$Lang['General']['EmptySymbol']."',u.ClassName) as ClassName,
								IF(u.ClassNumber IS NULL OR u.ClassNumber = '','".$Lang['General']['EmptySymbol']."',u.ClassNumber) as ClassNumber,
								  IF(sgao1.LessonDate IS NULL, '', sgao1.LessonDate) as LessonDate1,
								  CONCAT(itt.TimeSlotName,' (',itt.StartTime,' - ',itt.EndTime,')') as DayPeriod1,
								  CONCAT($SubjectName,' - ',$ClassTitle) as SubjectGroupTitle,
								  (IF(sgsa1.AttendStatus = 2, '<span style=\"color:#FF9900\"><b>".$Lang['LessonAttendance']['Late']."</b></span>', IF(sgsa1.AttendStatus = 3, '<span style=\"color:red\"><b>".$Lang['LessonAttendance']['Absent']."</b></span>', IF(sgsa1.AttendStatus = 1, '".$Lang['LessonAttendance']['Outing']."', '')))) as AttendStatus1
								from
								  INTRANET_CYCLE_GENERATION_PERIOD as icgp
									inner join
									INTRANET_PERIOD_TIMETABLE_RELATION as iptr
									on
									  
										icgp.PeriodID = iptr.PeriodID
								  inner join
								  INTRANET_TIMETABLE_ROOM_ALLOCATION as itra
								  on
								    itra.TimeTableID = iptr.TimeTableID
								   
								  inner join
								  SUBJECT_TERM_CLASS as stc
								  on stc.SubjectGroupID = itra.SubjectGroupID
								  inner join 
								  SUBJECT_TERM as st
								  on st.SubjectGroupID = stc.SubjectGroupID and st.YearTermID = '$YearTermID' 
								  inner join 
								  ASSESSMENT_SUBJECT as s 
								  on st.SubjectID = s.RecordID 
								  inner join
								  SUBJECT_GROUP_ATTEND_OVERVIEW_$AcademicYearID as sgao1
								  on
								    sgao1.SubjectGroupID = itra.SubjectGroupID
								    and sgao1.LessonDate between '$FromDate' and '$ToDate'
								    and sgao1.RoomAllocationID = itra.RoomAllocationID
								  inner join
								  SUBJECT_GROUP_STUDENT_ATTENDANCE_$AcademicYearID as sgsa1
								  on
								    sgsa1.AttendOverviewID = sgao1.AttendOverviewID
								    and
								    sgsa1.StudentID in ($StudenCondition)
								  inner JOIN
								  INTRANET_USER as u
								  on u.UserID = sgsa1.StudentID
								  inner JOIN 
								  SUBJECT_TERM_CLASS_USER as stcu
								  on 
								  	stcu.SubjectGroupID = stc.SubjectGroupID
								  	and 
								  	stcu.UserID in ($StudenCondition) 
								  inner JOIN 
								  INTRANET_USER as u1 
								  on u1.UserID = stcu.UserID
								  inner join
								  INTRANET_TIMETABLE_TIMESLOT as itt
								  on itra.TimeSlotID = itt.TimeSlotID
								where 
									true $cond_filter
											AND 
				(sgao1.LessonDate LIKE '%$keyword%' OR $SubjectName LIKE '%$keyword%' OR $ClassTitle LIKE '%$keyword%' ) ".$tempField.$tempOrder;
//								order by 
//									IF(u.UserID IS NULL, u1.ClassName, u.ClassName), IF(u.UserID IS NULL, u1.ClassNumber+0, u.ClassNumber+0)";
				*/
				$sql = "select DISTINCT ".$NameField3." as StudentName,
						IF(u.ClassNumber IS NULL OR u.ClassNumber = '','".$Lang['General']['EmptySymbol']."',u.ClassName) as ClassName,
								IF(u.ClassNumber IS NULL OR u.ClassNumber = '','".$Lang['General']['EmptySymbol']."',u.ClassNumber) as ClassNumber,
								  IF(sgao1.LessonDate IS NULL, '', sgao1.LessonDate) as LessonDate1,
								  CONCAT(itt.TimeSlotName,' (',itt.StartTime,' - ',itt.EndTime,')') as DayPeriod1,
								  CONCAT($SubjectName,' - ',$ClassTitle) as SubjectGroupTitle,
								  (IF(sgsa1.AttendStatus = 2, '<span style=\"color:#FF9900\"><b>".$Lang['LessonAttendance']['Late']."</b></span>', IF(sgsa1.AttendStatus = 3, '<span style=\"color:red\"><b>".$Lang['LessonAttendance']['Absent']."</b></span>', IF(sgsa1.AttendStatus = 1, '".$Lang['LessonAttendance']['Outing']."', '')))) as AttendStatus1,
								  sgsa1.Reason
								from 
								SUBJECT_GROUP_ATTEND_OVERVIEW_$AcademicYearID as sgao1 
								inner join SUBJECT_GROUP_STUDENT_ATTENDANCE_$AcademicYearID as sgsa1 
								  on sgsa1.AttendOverviewID = sgao1.AttendOverviewID 
								INNER JOIN INTRANET_USER as u ON u.UserID=sgsa1.StudentID 
								INNER JOIN SUBJECT_TERM_CLASS as stc ON sgao1.SubjectGroupID=stc.SubjectGroupID 
								INNER JOIN SUBJECT_TERM as st ON st.SubjectGroupID=stc.SubjectGroupID 
								INNER JOIN ASSESSMENT_SUBJECT as s ON st.SubjectID=s.RecordID 
								LEFT JOIN INTRANET_TIMETABLE_ROOM_ALLOCATION as itra ON itra.RoomAllocationID=sgao1.RoomAllocationID 
								LEFT JOIN INTRANET_TIMETABLE_TIMESLOT as itt ON itt.TimeSlotID=itra.TimeSlotID 
							where (sgao1.LessonDate between '$FromDate' and '$ToDate') and sgsa1.StudentID in ($StudenCondition) $cond_filter
								AND (sgao1.LessonDate LIKE '%$keyword%' OR $SubjectName LIKE '%$keyword%' OR $ClassTitle LIKE '%$keyword%' ) ".$tempField.$tempOrder;

      //debug_pr($sql);
      $temp = $lb->returnArray($sql,6);
      
      $display= "<table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\" class=\"eSporttableborder\">";
	$display.="<tr>";
	$display.= "<td width=\"1\" class=\"eSporttdborder eSportprinttabletitle\">#</td>\n";
	$display.= "<td width=\"10%\" class=\"eSporttdborder eSportprinttabletitle\">".$Lang['StudentAttendance']['StudentName']."</td>\n";
	$display.= "<td width=\"5%\" class=\"eSporttdborder eSportprinttabletitle\">".$i_ClassName."</td>\n";
	$display.= "<td width=\"5%\" class=\"eSporttdborder eSportprinttabletitle\">".$i_ClassNumber."</td>\n";
	$display.= "<td width=\"10%\" class=\"eSporttdborder eSportprinttabletitle\">".$i_Attendance_Date."</td>\n";
	$display.= "<td width=\"30%\" class=\"eSporttdborder eSportprinttabletitle\">".$Lang['eBooking']['Settings']['DefaultSettings']['BookingPeriod']['FieldTitle']['Period']."</td>\n";
	$display.= "<td width=\"30%\" class=\"eSporttdborder eSportprinttabletitle\">".$Lang['SysMgr']['SubjectClassMapping']['SubjectGroupTitle']."</td>\n";
	$display.= "<td width=\"10%\" class=\"eSporttdborder eSportprinttabletitle\">".$Lang['LessonAttendance']['Reason']."</td>\n";
	$display.= "<td width=\"10%\" class=\"eSporttdborder eSportprinttabletitle\">".$Lang['General']['Status2']."</td>\n";
//	$display.= "<td width=\"10%\" class=\"eSporttdborder eSportprinttabletitle\">".$i_Attendance_DayType."</td>\n";
//	$display.= "<td width=\"23%\" class=\"eSporttdborder eSportprinttabletitle\">".$i_Attendance_Reason."</td>\n";
//	$display.= "<td width=\"2%\" class=\"eSporttdborder eSportprinttabletitle\">".$Lang['StudentAttendance']['Waived']."</td>\n";
//	$display.= "<td width=\"25%\" class=\"eSporttdborder eSportprinttabletitle\">".$i_Attendance_Remark."</td>\n";
	for($i=0;$i<sizeof($temp);$i++){
		list($studentName,$className, $classNumber,$username,$lesson,$classname,$classnum,$reason) = $temp[$i];
		$css = $i%2==0?"tableContent":"tableContent2";
		$row = "<tr>";
		$row.= "<td class=\"eSporttdborder eSportprinttext\">".($i+1)."</td>";
		$row.= "<td class=\"eSporttdborder eSportprinttext\">$studentName</td>";
		$row.= "<td class=\"eSporttdborder eSportprinttext\">$className</td>";
		$row.= "<td class=\"eSporttdborder eSportprinttext\">$classNumber</td>";		
		$row.= "<td class=\"eSporttdborder eSportprinttext\">$username</td>";
		$row.= "<td class=\"eSporttdborder eSportprinttext\">$lesson&nbsp;</td>";
		$row.= "<td class=\"eSporttdborder eSportprinttext\">$classname&nbsp;</td>";
		$row.= "<td class=\"eSporttdborder eSportprinttext\">$classnum&nbsp;</td>";
		$row.= "<td class=\"eSporttdborder eSportprinttext\">$reason&nbsp;</td>";
//		$row.= "<td class=\"eSporttdborder eSportprinttext\">$daytype</td>";		
//		$row.= "<td class=\"eSporttdborder eSportprinttext\">$reason&nbsp;</td>";		
//		$row.= "<td class=\"eSporttdborder eSportprinttext\">$Waive&nbsp;</td>";		
//		$row.= "<td class=\"eSporttdborder eSportprinttext\">$remark&nbsp;</td>";		
		$row.="</tr>";
		$display.=$row;
	}
	if(sizeof($temp)<=0){
		$row = "<tr><td class=\"eSporttdborder eSportprinttext\" height=\"40\" colspan=\"9\" align=\"center\">$i_no_record_exists_msg</td></tr>";
		$display.=$row;
	}
	$display.="</table>";
      }
}
//$lu = new libuser($studentID);
//$studentName = Get_Lang_Selection($lu->ChineseName,$lu->EnglishName);
//$studentName ="";
//foreach($StudentIDArray as $aStudentID){
//	$lu = new libuser($aStudentID);
//	$studentName .= $Lang['StudentAttendance']['StudentName']." : ".Get_Lang_Selection($lu->ChineseName,$lu->EnglishName)." | ".$Lang['StudentAttendance']['Class']." : ".$lu->ClassName." | ".$Lang['StudentAttendance']['ClassNumber']." : ".$lu->ClassNumber;
//	$studentName .= "<br/>";
//}
?>
<table width='100%' align='center' class='print_hide' border=0>
	<tr>
		<td align='right'><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></td>
	</tr>
</table>
<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
		<td class="eSportprinttext"><b><?=$Lang['LessonAttendance']['LessonSummaryReport']?> <br/></b></td>
	</tr>
</table>
<?
echo $display;

include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_footer.php");

intranet_closedb();
?>