<?php
// using kenneth chung
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libstudentattendance_ui.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	echo 'die';
	intranet_closedb();
	exit();
}

$AcademicYear = $_REQUEST['AcademicYear'];

$linterface = new interface_html();

$StudentAttendUI = new libstudentattendance_ui();

echo $StudentAttendUI->Get_Class_Selection_List_By_Academic_Year($AcademicYear,"ClassID","ClassID");

intranet_closedb();
?>