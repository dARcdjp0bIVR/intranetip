<?php
// Editing by 
/*
 * 2016-06-07 (Carlos): Created.
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libstudentattendance_ui.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-LessonAttendance"] || !$_SESSION["SSV_PRIVILEGE"]["plugin"]["attendancelesson"]) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$linterface = new interface_html();
$LessonAttendUI = new libstudentattendance_ui();
$scm = new subject_class_mapping();

$academic_year_startdate = date("Y-m-d",getStartOfAcademicYear());
$academic_year_enddate = date("Y-m-d",getEndOfAcademicYear());
$today = date("Y-m-d");

$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageLessonAttendance_LessonSummary";

### Title ###
$TAGS_OBJ[] = array($Lang['LessonAttendance']['AbsentSummary'],"absent_summary.php",0);
$TAGS_OBJ[] = array($Lang['LessonAttendance']['SkipLessonSummary'],"",1);
$TAGS_OBJ[] = array($Lang['LessonAttendance']['LateSummary'],"late_summary.php",0);
$MODULE_OBJ = $LessonAttendUI->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START(); 
?>
<br />
<form id="form1" name="form1" action="" method="POST" onsubmit="return false;">
<table class="form_table_v30" width="96%" border="0" cellspacing="0" cellpadding="5" align="center">
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="field_title"><label for="StartDate"><?=$Lang['General']['Date']?></label><span class="tabletextrequire">*</span></td>
		<td width="70%">
			<?=$Lang['General']['From'].'&nbsp;'.$linterface->GET_DATE_PICKER("StartDate", $academic_year_startdate).$Lang['General']['To'].'&nbsp;'.$linterface->GET_DATE_PICKER("EndDate", $today)?>
			<?=$linterface->Get_Form_Warning_Msg('Date_Error', $Lang['General']['JS_warning']['InvalidDateRange'], 'Date_Error')?>
		</td>
	</tr>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="field_title"><label for="Type"><?=$Lang['LessonAttendance']['Type']?></label><span class="tabletextrequire">*</span></td>
		<td width="70%" class="tabletext">
			<?=$linterface->Get_Radio_Button("Type_Daily", "Type", "1", $__isChecked=0, $__class='', $__display=$Lang['LessonAttendance']['Daily'], $__onclick='', $__disabled='');?>
			<?=$linterface->Get_Radio_Button("Type_Weekly", "Type", "2", $__isChecked=0, $__class='', $__display=$Lang['LessonAttendance']['Weekly'], $__onclick='', $__disabled='');?>
			<?=$linterface->Get_Radio_Button("Type_Monthly", "Type", "3", $__isChecked=1, $__class='', $__display=$Lang['LessonAttendance']['Monthly'], $__onclick='', $__disabled='');?>
		</td>
	</tr>
	<tr>
	    <td width="30%" valign="top" nowrap="nowrap" class="field_title"><label for="GroupBy"><?=$Lang['LessonAttendance']['GroupBy']?></label><span class="tabletextrequire">*</span></td>
	    <td width="70%" class="tabletext">
	    	<?=$linterface->Get_Radio_Button("GroupBy_Teacher", "GroupBy", "1", $__isChecked=0, $__class='', $__display=$Lang['General']['Teacher'], $__onclick='', $__disabled='');?>
	    	<?=$linterface->Get_Radio_Button("GroupBy_Subject", "GroupBy", "2", $__isChecked=0, $__class='', $__display=$Lang['LessonAttendance']['Subject'], $__onclick='', $__disabled='');?>
			<?=$linterface->Get_Radio_Button("GroupBy_Class", "GroupBy", "3", $__isChecked=1, $__class='', $__display=$Lang['StudentAttendance']['Class'], $__onclick='', $__disabled='');?>
			<?=$linterface->Get_Radio_Button("GroupBy_Student", "GroupBy", "4", $__isChecked=0, $__class='', $__display=$Lang['Identity']['Student'], $__onclick='', $__disabled='');?>
			<?=$linterface->Get_Radio_Button("GroupBy_Period", "GroupBy", "5", $__isChecked=0, $__class='', $__display=$Lang['LessonAttendance']['Period'], $__onclick='', $__disabled='');?>
	    </td>
	</tr>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="field_title"><label for="TargetType"><?=$Lang['LessonAttendance']['Filter']?></label><span class="tabletextrequire">*</span></td>
		<td width="70%" class="tabletext">
			<table class="inside_form_table">
				<tr>
					<td valign="top">
						<select name="TargetType" id="TargetType" onchange="targetTypeChanged(this);">
							<option value="" selected="selected">-- <?=$Lang['General']['NA']?> --</option>
							<option value="form"><?=$Lang['SysMgr']['FormClassMapping']['Form']?></option>
							<option value="subject"><?=$Lang['LessonAttendance']['Subject']?></option>
						</select>
					</td>
					<td>
						<span id='DivRankTargetDetail'></span>
						<?=$linterface->GET_BTN($Lang['Btn']['SelectAll'], "button", "if(document.getElementById('TargetID')){Select_All_Options('TargetID', true);}return false;","selectAllTargetBtn",' style="display:none;"')?>
					</td>
				</tr>
			</table>
			<?=$linterface->Get_Form_Warning_Msg('TargetID_Error', $Lang['eDiscipline']['WarningMsgArr']['SelectTarget'], 'TargetID_Error', false);?>
			<div class="tabletextremark" id="DivSelectAllRemark" style="display:none;"><?=$Lang['SysMgr']['FormClassMapping']['CtrlMultiSelectMessage']?></div>
		</td>
	</tr>
	<tr>
	    <td width="30%" valign="top" nowrap="nowrap" class="field_title"><label for="WithReason"><?=$Lang['LessonAttendance']['WithReason']?></label><span class="tabletextrequire">*</span></td>
	    <td width="70%" class="tabletext">
			<?=$linterface->Get_Radio_Button("WithReason_Yes", "WithReason", "1", $__isChecked=0, $__class='', $__display=$Lang['General']['Yes'], $__onclick='', $__disabled='');?>
			<?=$linterface->Get_Radio_Button("WithReason_No", "WithReason", "2", $__isChecked=0, $__class='', $__display=$Lang['General']['No'], $__onclick='', $__disabled='');?>
			<?=$linterface->Get_Radio_Button("WithReason_All", "WithReason", "3", $__isChecked=1, $__class='', $__display=$Lang['General']['All'], $__onclick='', $__disabled='');?>
	    </td>
	</tr>
</table>
<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr><td align="left" class="tabletextremark"><?=$i_general_required_field?></td></tr>
</table>
<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
    	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
    </tr>
</table>
<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
	    <td align="center" colspan="2">
			<?= $linterface->GET_ACTION_BTN($Lang['Btn']['View'], "button", "submitForm('');","submitBtn") ?>
		</td>
	</tr>
</table>
<input type="hidden" name="ReportType" id="ReportType" value="skip_lesson" />
<input type="hidden" name="Format" id="Format" value="" />
</form>
<br />
<div id="ReportLayer"></div>
<br />
<script type="text/javascript" language="javascript">
var loadingImg = '<?=$linterface->Get_Ajax_Loading_Image();?>';
var academicYearId = '<?=Get_Current_Academic_Year_ID()?>';

function targetTypeChanged(obj)
{
	var selectedTargetType = obj.value;
	if(selectedTargetType == 'form' || selectedTargetType == 'subject'){
		$('#DivRankTargetDetail').html(loadingImg).load(
			'../../common/ajax_load_target_selection.php',
			{
				'target':selectedTargetType,
				'academicYearId':academicYearId,
				'fieldId':'TargetID',
				'fieldName':'TargetID[]'
			},
			function(data){
				if(document.getElementById('TargetID')){
					Select_All_Options('TargetID', true);
				}
				$('#selectAllTargetBtn').show();
				$('#DivSelectAllRemark').show();
			}
		);
	}else{
		$('#DivRankTargetDetail').html('');
		$('#selectAllTargetBtn').hide();
		$('#DivSelectAllRemark').hide();
	}
}

function submitForm(format)
{
	$('#submitBtn').attr('disabled',true);
	var is_valid = true;
	var startdateObj = document.getElementById('StartDate');
	var enddateObj = document.getElementById('EndDate');
	var target_type = $('#TargetType').val();
	var target_obj = document.getElementById('TargetID');
	var target_values = [];

	if(!check_date_without_return_msg(startdateObj)){
		is_valid = false;
	}
	if(!check_date_without_return_msg(enddateObj)){
		is_valid = false;
	}
	if(startdateObj.value > enddateObj.value){
		is_valid = false;
		$('#Date_Error').show();
	}else{
		$('#Date_Error').hide();
	}
	
	if(target_type != '' && target_obj)
	{
		for(var i=0;i<target_obj.options.length;i++)
		{
			if(target_obj.options[i].selected) target_values.push(target_obj.options[i].value);
		}
		
		if(target_values.length <= 0)
		{
		 	is_valid = false;
			$('#TargetID_Error').show();
		}else{
			$('#TargetID_Error').hide();
		}
	}else{
		$('#TargetID_Error').hide();
	}
	
	if(!is_valid) $('#submitBtn').attr('disabled',false);
	
	if(is_valid)
	{
		$('#Format').val(format);
		if(format == '' || format == 'web'){
			$('#ReportLayer').html(loadingImg);
			$.post(
				'report.php',
				$('#form1').serialize(),
				function(returnHtml)
				{
					$('#ReportLayer').html(returnHtml);
					$('#submitBtn').attr('disabled',false);
				}
			);
		}else if(format == 'print' || format == 'pdf')
		{
			var url = '';
			var winType = '10';
			var win_name = 'intranet_popup'+winType;
			
			var oldAction = document.form1.action;
			var oldTarget = document.form1.target;
			
			document.form1.action = 'report.php';
			document.form1.target = win_name;
			
			newWindow(url, winType);
			document.form1.submit();
			
			document.form1.action = oldAction;
			document.form1.target = oldTarget;
			$('#submitBtn').attr('disabled',false);
		}else if(format == 'csv'){
			
			var oldAction = document.form1.action;
			var oldTarget = document.form1.target;
			
			document.form1.action = 'report.php';
			document.form1.target = '_blank';
			document.form1.submit();
			
			document.form1.action = oldAction;
			document.form1.target = oldTarget;
			$('#submitBtn').attr('disabled',false);
		}
	}
}
</script>
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>