<?
//editing by 
####################################### Change Log #################################################
# 2012-12-28 by Carlos: Added continuous absent day option
####################################################################################################
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libstudentattendance_ui.php");
include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_header.php");

intranet_auth();
intranet_opendb();

if ( !($_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || $_SESSION["platform"]=="KIS") || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	intranet_closedb();
	header ("Location: /");
	exit();
}

$MODULE_OBJ['title'] = $i_StudentAttendance_Report_ClassMonth;
$linterface = new interface_html("");
$lstudentattendance_ui = new libstudentattendance_ui();

$lclass = new libclass();
$AcademicYear = $_REQUEST['AcademicYear'];
if ($ClassID != "") {
	$classes[0]['ClassID'] = $ClassID;
	$ClassName = $lclass->getClassName($ClassID);
	$classes[0]['ClassName'] = ($ClassName == "")? $ClassID:$ClassName;
}
else {
	if ($AcademicYear == "" || ($lstudentattendance_ui->Platform == "IP" && $AcademicYear == Get_Current_Academic_Year_ID())) {
		$classes = $lclass->getClassList();
	}
	else {
		$classes = $lstudentattendance_ui->Get_Class_List_By_Academic_Year($AcademicYear);
	}
}

# Get Year & Month
if ($Year == "")
{
	$Year = date('Y');
}
if ($Month == "")
{
	$Month = date('m');
}
if (strlen($Month)==1)
{
	$Month = "0".$Month;
}

# Get Optional Data Columns
$Columns=array();
if($ShowAllColumns == "0") # Data Columns are Optionally Shown
{
	$Columns['ChineseName'] = $ColumnChineseName;
	$Columns['EnglishName'] = $ColumnEnglishName;
	$Columns['Gender'] = $ColumnGender;
	$Columns['Data'] = $ColumnData;
	$Columns['DailyStat'] = $ColumnDailyStat;
	$Columns['Schooldays'] = $ColumnSchoolDays;
	$Columns['MonthlyStat'] = $ColumnMonthlyStat;
	$Columns['Remark'] = $ColumnRemark;
	$Columns['ReasonStat'] = $ColumnReasonStat;
	$Columns['BirthDate'] = $ColumnBirthDate;
	$Columns['EntryDate'] = $ColumnEntryDate;
	$Columns['NonSchoolDayReason'] = $ColumnNonSchoolDayReason;
	if($sys_custom['SmartCardAttendance_StudentAbsentSession'])
	{
		$Columns['Session'] = $ColumnSession;
	}
}else # Default All Columns are shown
{
	$Columns['ChineseName'] = "1";
	$Columns['EnglishName'] = "1";
	$Columns['Gender'] = "1";
	$Columns['Data'] = "1";
	$Columns['DailyStat'] = "1";
	$Columns['Schooldays'] = "1";
	$Columns['MonthlyStat'] = "1";
	$Columns['Remark'] = "1";
	$Columns['ReasonStat'] = "1";
	$Columns['NonSchoolDayReason'] = "1";
	if ($_SESSION['platform'] == "KIS"){
		$Columns['BirthDate'] = "1";
		$Columns['EntryDate'] = "1";
	}
	if($sys_custom['SmartCardAttendance_StudentAbsentSession'])
	{
		$Columns['Session'] = "1";
	}
}

$StudentStatus = trim($StudentStatus)==""?"0,1,2":$StudentStatus;
$ContinuousAbsentDay = round($_REQUEST['AbsentDay']);

# Get Report Content
//$content = $lstudentattendance_ui->Get_Class_Monthly_Attendance_Report($classes,$Year,$Month,$Columns,$_REQUEST['HideNoData'],false,$AcademicYear,$StudentStatus,1);
if($UseDateRange == 0){
$content = $lstudentattendance_ui->Get_Class_Monthly_Attendance_Report($classes,$Year,$Month,$Columns,$_REQUEST['HideNoData'],false,$AcademicYear,$StudentStatus,1,$ContinuousAbsentDay);
}
else{
$content = $lstudentattendance_ui->Get_Class_Monthly_Attendance_Report_By_Date_Range($classes,$Year, $Month,$Columns,$_REQUEST['HideNoData'],false,$AcademicYear,$StudentStatus,$startDate,$endDate,'',$ContinuousAbsentDay);
}

?>
<style type="text/css">
#contentTable1, #contentTable2 {
	border:1px solid black;
}

#contentTable1 td, #contentTable2 td {
	border:1px solid black;
}

#contentTable3 td, #contentTable4 td {
	border:0px solid black;
}
</style>
<table width='100%' align='center' class='print_hide' border=0>
	<tr>
		<td align='left'><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></td>
	</tr>
</table>
<?php
echo $content;
intranet_closedb();
include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_footer.php");

?>

