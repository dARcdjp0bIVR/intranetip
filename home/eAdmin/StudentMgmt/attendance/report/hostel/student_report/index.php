<?php
// Editing by 
/*
 * 2017-08-10 (Carlos): $sys_custom['StudentAttendance']['HostelAttendance'] created.
 */
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libgrouping.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent'] || !$sys_custom['StudentAttendance']['HostelAttendance']) {
	intranet_closedb();
	header ("Location: /");
	exit();
}

$lc = new libcardstudentattend2();

//if($lc->HostelAttendanceGroupCategory == ''){
//	intranet_closedb();
//	header ("Location: ../../");
//	exit();
//}

$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageReport_HostelAttendanceStudentReport";

$linterface = new interface_html();
$libgrouping = new libgrouping();

$academic_year_startdate = date("Y-m-d", getStartOfAcademicYear());
$today = date("Y-m-d");
if(!isset($StartDate)) $StartDate = $academic_year_startdate;
if(!isset($EndDate)) $EndDate = $today;
$word_space = $_SESSION['intranet_session_language'] == 'en'? '&nbsp;' : '';

$groups = $libgrouping->returnCategoryGroups($lc->HostelAttendanceGroupCategory);
$group_selection = $linterface->GET_SELECTION_BOX($groups, ' id="GroupID" name="GroupID" onchange="document.form1.submit();" ', '-- '.$Lang['General']['PleaseSelect'].' --', $GroupID, false);

if($GroupID != ''){
	$target_date = $today;
	$year = date("Y");
	$month = date("m");
	$records = $lc->getHostelAttendanceRecords($year, $month, array('GroupID'=>$GroupID,'RecordDate'=>$target_date));
	$students = array();
	for($i=0;$i<count($records);$i++){
		$students[] = array($records[$i]['UserID'],$records[$i]['StudentName']);
	}
	$student_selection = $linterface->GET_SELECTION_BOX($students, ' id="StudentID" name="StudentID" ', '-- '.$Lang['General']['PleaseSelect'].' --', $StudentID, false);
}

$TAGS_OBJ[] = array($Lang['StudentAttendance']['HostelStudentReport'], "", 0);
$MODULE_OBJ = $lc->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();
?>
<br />
<form id="form1" name="form1" action="" method="POST" onsubmit="return false;">
<table class="form_table_v30" width="96%" border="0" cellspacing="0" cellpadding="5" align="center">
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="field_title"><label for="GroupID"><?=$Lang['StudentAttendance']['HostelGroup']?></label><span class="tabletextrequire">*</span></td>
		<td width="70%" class="tabletext">
			<?=$group_selection?>
			<?=$linterface->Get_Form_Warning_Msg('GroupID_Error', $Lang['General']['PleaseSelect'].$word_space.$Lang['StudentAttendance']['HostelGroup'], 'GroupID_Error WarnMsg', false);?>
		</td>
	</tr>
	<?php if($GroupID != ''){ ?>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="field_title"><label for="StudentID"><?=$Lang['Identity']['Student']?></label><span class="tabletextrequire">*</span></td>
		<td width="70%" class="tabletext">
			<?=$student_selection?>
			<?=$linterface->Get_Form_Warning_Msg('StudentID_Error', $Lang['General']['PleaseSelect'].$word_space.$Lang['Identity']['Student'], 'StudentID_Error WarnMsg', false);?>
		</td>
	</tr>
	<?php } ?>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="field_title"><label for="StartDate"><?=$Lang['General']['Date']?></label><span class="tabletextrequire">*</span></td>
		<td width="70%">
			<?=$Lang['General']['From'].'&nbsp;'.$linterface->GET_DATE_PICKER("StartDate", $StartDate).$Lang['General']['To'].'&nbsp;'.$linterface->GET_DATE_PICKER("EndDate", $EndDate)?>
			<?=$linterface->Get_Form_Warning_Msg('Date_Error', $Lang['General']['JS_warning']['InvalidDateRange'], 'Date_Error WarnMsg')?>
		</td>
	</tr>
</table>
<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr><td align="left" class="tabletextremark"><?=$i_general_required_field?></td></tr>
</table>
<?php if($GroupID != '' && $student_selection !=''){ ?>
<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
    	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
    </tr>
</table>
<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
	    <td align="center" colspan="2">
			<?= $linterface->GET_ACTION_BTN($Lang['Btn']['View'], "button", "submitForm('');","submitBtn") ?>
		</td>
	</tr>
</table>
<?php } ?>
<input type="hidden" name="Format" id="Format" value="" />
</form>
<br />
<div id="ReportLayer">
<?php
$x = '<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">' . "\n";
	$x .= '<tr>' . "\n";
	$x .= '<td>' . "\n";
		$x .= '<div class="content_top_tool">' . "\n";
			$x .= '<div class="Conntent_tool">' . "\n";
			$x .= $linterface->GET_LNK_PRINT("javascript:submitForm('print');", "", "", "", "", 1);
			$x .= $linterface->GET_LNK_EXPORT("javascript:submitForm('csv');", "", "", "", "", 1);
			$x .= '</div>' . "\n";
			$x .= '<br style="clear: both;">' . "\n";
		$x .= '</div>' . "\n";
	$x .= '</td>' . "\n";
	$x .= '</tr>' . "\n";
$x .= '</table>' . "\n";

$x.= 'Hostel Group: XXXXX &nbsp;&nbsp; Student: XXX';
$x.= '<table class="common_table_list_v30">'."\n";
		$x.='<thead>';
		$x.= '<tr>';
			$x.='<th class="num_check" width="1">#</th>';
			$x.='<th style="width:20%;">'.'Record Date'.'</th>';
			$x.='<th style="width:15%;">'.'In Status'.'</th>';
			$x.='<th style="width:15%;">'.'In Time'.'</th>';
			$x.='<th style="width:15%;">'.'Out Status'.'</th>';
			$x.='<th style="width:15%;">'.'Out Time'.'</th>';
			$x.='<th style="width:20%;">'.'Remark'.'</th>';
		$x.= '</tr>'."\n";
		$x.='</thead>';
		$x.='<tbody>';
		
		$x.='<tr>';
			$x.='<td colspan="7" style="text-align:center;">'.$Lang['General']['NoRecordAtThisMoment'].'</td>';
		$x.='</tr>'."\n";
		
		$x.='</tbody>';
$x .= '</table>';
//echo $x;
?>
</div>
<br />
<script type="text/javascript" language="javascript">
var loadingImg = '<?=$linterface->Get_Ajax_Loading_Image();?>';

function submitForm(format)
{
	$('#submitBtn').attr('disabled',true);
	$('.WarnMsg').hide();
	var is_valid = true;
	var startdateObj = document.getElementById('StartDate');
	var enddateObj = document.getElementById('EndDate');

	if($('#GroupID').val() == ''){
		is_valid = false;
		$('#GroupID_Error').show();
	}
	
	if($('#StudentID').length == 0 || ($('#StudentID').length>0 && $('#StudentID').val()=='')){
		is_valid = false;
		$('#StudentID_Error').show();
	}
	
	if(!check_date_without_return_msg(startdateObj)){
		is_valid = false;
	}
	if(!check_date_without_return_msg(enddateObj)){
		is_valid = false;
	}
	if(startdateObj.value > enddateObj.value){
		is_valid = false;
		$('#Date_Error').show();
	}

	if(!is_valid) $('#submitBtn').attr('disabled',false);
	
	if(is_valid)
	{
		$('#Format').val(format);
		if(format == '' || format == 'web'){
			$('#ReportLayer').html(loadingImg);
			$.post(
				'report.php',
				$('#form1').serialize(),
				function(returnHtml)
				{
					$('#ReportLayer').html(returnHtml);
					$('#submitBtn').attr('disabled',false);
				}
			);
		}else if(format == 'print' || format == 'pdf')
		{
			var url = '';
			var winType = '10';
			var win_name = 'intranet_popup'+winType;
			
			var oldAction = document.form1.action;
			var oldTarget = document.form1.target;
			
			document.form1.action = 'report.php';
			document.form1.target = win_name;
			
			newWindow(url, winType);
			document.form1.submit();
			
			document.form1.action = oldAction;
			document.form1.target = oldTarget;
			$('#submitBtn').attr('disabled',false);
		}else if(format == 'csv'){
			
			var oldAction = document.form1.action;
			var oldTarget = document.form1.target;
			
			document.form1.action = 'report.php';
			document.form1.target = '_blank';
			document.form1.submit();
			
			document.form1.action = oldAction;
			document.form1.target = oldTarget;
			$('#submitBtn').attr('disabled',false);
		}
	}
}
</script>
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>