<?php
// Editing by 
/*
 * 2017-08-10 (Carlos): $sys_custom['StudentAttendance']['HostelAttendance'] created.
 */
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libgrouping.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent'] || !$sys_custom['StudentAttendance']['HostelAttendance']
	|| count($GroupID)==0 || $StartDate=='' || $EndDate=='' || count($InStatus)==0 || count($OutStatus)==0) {
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();

if($lc->HostelAttendanceGroupCategory == ''){
	intranet_closedb();
	exit();
}

$linterface = new interface_html();

$in_status_ary = array(
	CARD_STATUS_PRESENT=>$Lang['StudentAttendance']['HostelAttend'],
	CARD_STATUS_ABSENT=>$Lang['StudentAttendance']['HostelAbsent']
);

$out_status_ary = array(
	''=>'N.A.',
	(string)CARD_STATUS_PRESENT=>$Lang['StudentAttendance']['HostelNormalLeave'],
	(string)PROFILE_TYPE_EARLY=>$Lang['StudentAttendance']['HostelEarlyLeave']
);

$raw_start_ts = strtotime($StartDate);
$raw_end_ts = strtotime($EndDate);
$columns = array();

$start_date = date("Y-m-01",$raw_start_ts);
$start_ts = strtotime($start_date);
$days_last_month = intval(date("t",$raw_end_ts));
$end_ts = strtotime(date("Y-m-01",$raw_end_ts)) + $days_last_month * 86399;
for($cur_ts=$start_ts;$cur_ts<=$end_ts;$cur_ts=mktime(0,0,1,intval(date("m",$cur_ts))+1,date("d",$cur_ts),date("Y",$cur_ts)))
{
	$days_this_month = intval(date("t", $cur_ts));
	$begin = max($cur_ts,$raw_start_ts);
	$end = min($cur_ts+$days_this_month*86399,$raw_end_ts);
	$columns[] = array(date("Y-m-d",$begin), date("Y-m-d,",$end));
}

$dateToRecords = array();
$dateAry = array();
for($i=0;$i<count($columns);$i++){
	$startdate = $columns[$i][0];
	$enddate = $columns[$i][1];
	$ts = strtotime($startdate);
	$year = date("Y", $ts);
	$month = date("m", $ts);
	$params = array('StartDate'=>$startdate,'EndDate'=>$enddate,'GroupID'=>$GroupID,'InStatus'=>$InStatus,'OutStatus'=>$OutStatus);
	$tmp_records = $lc->getHostelAttendanceRecords($year,$month,$params);
	$tmp_record_size = count($tmp_records);
	for($j=0;$j<$tmp_record_size;$j++){
		if(!isset($dateToRecords[$tmp_records[$j]['RecordDate']])){
			$dateToRecords[$tmp_records[$j]['RecordDate']] = array();
			$dateAry[] = $tmp_records[$j]['RecordDate'];
		}
		$dateToRecords[$tmp_records[$j]['RecordDate']][] = $tmp_records[$j];
	}
}

if(in_array($Format,array("","web")))
{
	$x .= '<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">' . "\n";
		$x .= '<tr>' . "\n";
		$x .= '<td>' . "\n";
			$x .= '<div class="content_top_tool">' . "\n";
				$x .= '<div class="Conntent_tool">' . "\n";
				$x .= $linterface->GET_LNK_PRINT("javascript:submitForm('print');", "", "", "", "", 1);
				$x .= $linterface->GET_LNK_EXPORT("javascript:submitForm('csv');", "", "", "", "", 1);
				$x .= '</div>' . "\n";
				$x .= '<br style="clear: both;">' . "\n";
			$x .= '</div>' . "\n";
		$x .= '</td>' . "\n";
		$x .= '</tr>' . "\n";
	$x .= '</table>' . "\n";
	
	for($i=0;$i<count($dateAry);$i++){
		$ary = $dateToRecords[$dateAry[$i]];
		$ary_size = count($ary);
		$x.= '<h3>'.$Lang['General']['Date'].': '.$dateAry[$i].'</h3>';
			$x.= '<table class="common_table_list_v30">'."\n";
					$x.='<thead>';
					$x.= '<tr>';
						$x.='<th class="num_check" width="1">#</th>';
						$x.='<th style="width:15%;">'.$Lang['StudentAttendance']['HostelGroup'].'</th>';
						$x.='<th style="width:15%;">'.$Lang['Identity']['Student'].'</th>';
						$x.='<th style="width:15%;">'.$Lang['StudentAttendance']['InStatus'].'</th>';
						$x.='<th style="width:15%;">'.$Lang['StudentAttendance']['InTime'].'</th>';
						$x.='<th style="width:10%;">'.$Lang['StudentAttendance']['OutStatus'].'</th>';
						$x.='<th style="width:15%;">'.$Lang['StudentAttendance']['OutTime'].'</th>';
						$x.='<th style="width:15%;">'.$Lang['General']['Remark'].'</th>';
					$x.= '</tr>'."\n";
					$x.='</thead>';
					$x.='<tbody>';
		
					//$x.='<tr>';
					//	$x.='<td colspan="8" style="text-align:center;">'.$Lang['General']['NoRecordAtThisMoment'].'</td>';
					//$x.='</tr>'."\n";
					for($j=0;$j<$ary_size;$j++){
						$x .= '<tr>';
						$x .= '<td>'.($j+1).'</td>';
						$x .= '<td>'.Get_String_Display($ary[$j]['GroupName']).'</td>';
						$x .= '<td>'.Get_String_Display($ary[$j]['StudentName']).'</td>';
						$in_status = $ary[$j]['InStatus']!=''?$ary[$j]['InStatus']:$lc->DefaultAttendanceStatus;
						$x .= '<td>'.$in_status_ary[$in_status].'</td>';
						$in_time = $ary[$j]['InTime']!=''? date("H:i",strtotime($ary[$j]['InTime'])) : '';
						$x .= '<td>'.Get_String_Display($in_time).'</td>';
						$x .= '<td>'.$out_status_ary[$ary[$j]['OutStatus']].'</td>';
						$out_time = $ary[$j]['OutTime']!=''? date("H:i",strtotime($ary[$j]['OutTime'])) : '';
						$x .= '<td>'.Get_String_Display($out_time).'</td>';
						$x .= '<td>'.Get_String_Display($ary[$j]['Remark']).'</td>';
						$x .= '</tr>';
					}
				$x.='</tbody>';
		$x .= '</table>';
		$x .= '<br />';
	}
	
	echo $x;
}

if($Format == "print")
{
	$x = '';
	$x .= '<table width="100%" align="center" class="print_hide" border="0">
				<tr>
					<td align="right">'.$linterface->GET_BTN($Lang['Btn']['Print'], "button", "javascript:window.print();","printBtn").'</td>
				</tr>
			</table>';
	$x .= '<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
			 <tr>
				<td align="center"><h2>'.$Lang['StudentAttendance']['HostelStudentList'].'</h2></td>
			 </tr>
		   </table>';
	
	for($i=0;$i<count($dateAry);$i++){
		$ary = $dateToRecords[$dateAry[$i]];
		$ary_size = count($ary);
		$x.= '<h3>'.$Lang['General']['Date'].': '.$dateAry[$i].'</h3>';
			$x.= '<table align="center" class="eSporttableborder" border="0" cellpadding="2" cellspacing="0" width="100%">'."\n";
					$x.='<thead>';
					$x.= '<tr class="tabletop">';
						$x.='<th class="eSporttdborder eSportprinttabletitle" width="1">#</th>';
						$x.='<th class="eSporttdborder eSportprinttabletitle" style="width:15%;">'.$Lang['StudentAttendance']['HostelGroup'].'</th>';
						$x.='<th class="eSporttdborder eSportprinttabletitle" style="width:15%;">'.$Lang['Identity']['Student'].'</th>';
						$x.='<th class="eSporttdborder eSportprinttabletitle" style="width:15%;">'.$Lang['StudentAttendance']['InStatus'].'</th>';
						$x.='<th class="eSporttdborder eSportprinttabletitle" style="width:15%;">'.$Lang['StudentAttendance']['InTime'].'</th>';
						$x.='<th class="eSporttdborder eSportprinttabletitle" style="width:10%;">'.$Lang['StudentAttendance']['OutStatus'].'</th>';
						$x.='<th class="eSporttdborder eSportprinttabletitle" style="width:15%;">'.$Lang['StudentAttendance']['OutTime'].'</th>';
						$x.='<th class="eSporttdborder eSportprinttabletitle" style="width:15%;">'.$Lang['General']['Remark'].'</th>';
					$x.= '</tr>'."\n";
					$x.='</thead>';
					$x.='<tbody>';
					for($j=0;$j<$ary_size;$j++){
						$x .= '<tr>';
						$x .= '<td class="eSporttdborder eSportprinttext">'.($j+1).'</td>';
						$x .= '<td class="eSporttdborder eSportprinttext">'.Get_String_Display($ary[$j]['GroupName']).'</td>';
						$x .= '<td class="eSporttdborder eSportprinttext">'.Get_String_Display($ary[$j]['StudentName']).'</td>';
						$in_status = $ary[$j]['InStatus']!=''?$ary[$j]['InStatus']:$lc->DefaultAttendanceStatus;
						$x .= '<td class="eSporttdborder eSportprinttext">'.$in_status_ary[$in_status].'</td>';
						$in_time = $ary[$j]['InTime']!=''? date("H:i",strtotime($ary[$j]['InTime'])) : '';
						$x .= '<td class="eSporttdborder eSportprinttext">'.Get_String_Display($in_time).'</td>';
						$x .= '<td class="eSporttdborder eSportprinttext">'.$out_status_ary[$ary[$j]['OutStatus']].'</td>';
						$out_time = $ary[$j]['OutTime']!=''? date("H:i",strtotime($ary[$j]['OutTime'])) : '';
						$x .= '<td class="eSporttdborder eSportprinttext">'.Get_String_Display($out_time).'</td>';
						$x .= '<td class="eSporttdborder eSportprinttext">'.Get_String_Display($ary[$j]['Remark']).'</td>';
						$x .= '</tr>';
					}
				$x.='</tbody>';
		$x .= '</table>';
		$x .= '<br />';
	}
		   
	include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_header.php");
	echo $x;
	include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_footer.php");
}

if($Format == "csv")
{
	include_once ($PATH_WRT_ROOT."includes/libexporttext.php");
	$lexport = new libexporttext();

	$rows = array();
	$row = array();
	$headers = array($Lang['General']['Date'],
					$Lang['StudentAttendance']['HostelGroup'],
					$Lang['Identity']['Student'],
					$Lang['StudentAttendance']['InStatus'],
					$Lang['StudentAttendance']['InTime'],
					$Lang['StudentAttendance']['OutStatus'],
					$Lang['StudentAttendance']['OutTime'],
					$Lang['General']['Remark']
				);
	
	for($i=0;$i<count($dateAry);$i++){
		$ary = $dateToRecords[$dateAry[$i]];
		$ary_size = count($ary);
		
		for($j=0;$j<$ary_size;$j++){
			$in_status = $ary[$j]['InStatus']!=''?$ary[$j]['InStatus']:$lc->DefaultAttendanceStatus;
			$in_time = $ary[$j]['InTime']!=''? date("H:i",strtotime($ary[$j]['InTime'])) : '';
			$out_time = $ary[$j]['OutTime']!=''? date("H:i",strtotime($ary[$j]['OutTime'])) : '';
			
			$row = array($ary[$j]['RecordDate'],
						$ary[$j]['GroupName'],
						$ary[$j]['StudentName'],
						$in_status_ary[$in_status],
						$in_time,
						$out_status_ary[$ary[$j]['OutStatus']],
						$out_time,
						$ary[$j]['Remark']);
			$rows[] = $row;
		}
	}
	
	$csv_filename = $Lang['StudentAttendance']['HostelStudentList'].".csv";
	$exportContent = $lexport->GET_EXPORT_TXT($rows, $headers);
	$lexport->EXPORT_FILE($csv_filename, $exportContent);
}

intranet_closedb();
?>