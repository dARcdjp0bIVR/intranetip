<?php
// Editing by 
##################################
# change log:
#	Date:	2017-12-04  Carlos
#			Filter students that are active or suspended, do not include left students.
#
#	Date:	2016-08-26	Bill
#			Display Processed in column Unsubmmited Date(s) if record is processed by school
#
#	Date:	2016-06-30	Carlos
#			Added Early Leave record type.
#
#	Date:	2015-10-15	Omas
#			Added remarks for red star
#
#	Date:	2014-12-10	Omas
#			Created this file - New Report for eAttendance
#
##################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp_template.php");


intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	intranet_closedb();
	header ("Location: /");
	exit();
}

if($_SERVER['REQUEST_METHOD']!='POST'){
	intranet_closedb();
	header ("Location: index.php");
	exit();
}

# db table info
$order = ($order == '') ? 1 : $order;	// 1 => asc, 0 => desc
$field = ($field == '') ? 1 : $field;
$pageNo = ($pageNo == '') ? 1 : $pageNo;
$page_size = ($numPerPage == '') ? 50 : $numPerPage;

# get class from lib
$lc = new libcardstudentattend2();
$linterface = new interface_html();
//$lclass= new libclass();
$li = new libdbtable2007($field, $order, $pageNo);
$libAppTemplate = new libeClassApp_template();

# Default sorting ordder
if ($field == 1){
	$li->fieldorder2 = " , iu.ClassNumber asc ";
}
else{
	$li->fieldorder2 = ", iu.ClassName asc, iu.ClassNumber asc ";
}

# Init libeClassApp_template
$libAppTemplate->setModule('StudentAttendance');
//$libAppTemplate->setSection('NotSubmitDocument');
$libAppTemplate->setSection('');

# Define Page and Layout
$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = 'PageReport_ProveDocReport';
$TAGS_OBJ[] = array($Lang['StudentAttendance']['AbsentProveReport']['AbsentProveReport'], "", 0);
$MODULE_OBJ = $lc->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

# Get Student by ClassName
if(!empty($_POST['ClassName'])){
	$ClassNameAry = $_POST['ClassName'];
}
else{
	$ClassNameAry = unserialize(rawurldecode($_POST['target_student']));
}

$sql = "SELECT UserID FROM INTRANET_USER WHERE RecordType=2 AND RecordStatus IN (0,1,2) AND ClassName IN ('".implode("','",$lc->Get_Safe_Sql_Query((array)$ClassNameAry))."')";
$UserIdAry = $lc->returnVector($sql);
$UserIdAry[] = -1;
//$li->db_db_query("SET SESSION group_concat_max_len = 10000;");

# Build table
$li->field_array = array("WebSAMSRegNo", "ClassName", "ClassNumber" ,"Name", "RecordType", "countall","countnot");
$li->sql = $lc->getNotSumttedDocumentStudentDataSQL($UserIdAry,$DateStart,$DateEnd,$document_status,$record_type,true);
$li->no_col = 9;
$li->title = $eDiscipline["Record"]; 
$li->column_array = array(0,0,0,0,0,0,0);
$li->wrap_array = array(0,0,0,0,0,0,0);
$li->IsColOff = "IP25_table";
$li->count_mode = 1;
$cell_width = sprintf("%d%%",80 / 6);
$pos = 0;
$li->column_list .= "<th width='1' class='num_check'>#</th>\n";
$li->column_list .= "<th width='$cell_width' >".$li->column($pos++, "WebSAMS No.")."</th>\n";
$li->column_list .= "<th width='$cell_width' >".$li->column($pos++, $i_ClassName)."</th>\n";
$li->column_list .= "<th width='$cell_width' >".$li->column($pos++, $i_ClassNumber)."</th>\n";
$li->column_list .= "<th width='$cell_width' >".$li->column($pos++, $i_UserStudentName)."</th>\n";
$li->column_list .= "<th width='$cell_width' >".$li->column($pos++, $Lang['General']['Type'])."</th>\n";
$li->column_list .= "<th width='10%' >".$li->column($pos++, $Lang['StudentAttendance']['AbsentProveReport']['NumberOfTimes'])."</th>\n";
$li->column_list .= "<th width='10%' >".$li->column($pos++, $Lang['StudentAttendance']['AbsentProveReport']['UnsubmmitedNum'])."</th>\n";
$li->column_list .= "<th width='$cell_width' >".$Lang['StudentAttendance']['AbsentProveReport']['UnsubmmitedDates']."</th>\n";

# Button
$Backbutton = $linterface->GET_ACTION_BTN($button_back, "button", "window.location='index.php'","backbtn");

# Push Message ThickBox
if ($plugin['eClassApp']) {
	# hidden field for submit to ajax_send_reminder_update.php
	$hiddenFieldAry = array ();
	$hiddenFieldAry[] = array('StudentList' , rawurlencode(serialize($UserIdAry)) );
	$hiddenFieldAry[] = array('DateStart' , $DateStart );
	$hiddenFieldAry[] = array('DateEnd' , $DateEnd );
	//$hiddenFieldAry[] = array('document_status' , $document_status );
	for($i=0;$i<count($record_type);$i++)
	{
		$hiddenFieldAry[] = array('record_type[]' , $record_type[$i] );
	}
	$thickbox = $libAppTemplate->getPushMessageThickBox($hiddenFieldAry);
}

?>

<script type="text/javascript" src= "<?=$PATH_WRT_ROOT?>templates/jquery/thickbox.js"></script>
<link rel="stylesheet" href="<?=$PATH_WRT_ROOT?>templates/jquery/thickbox.css" type="text/css" media="screen" />
<? include_once($PATH_WRT_ROOT."home/eClassApp/common/pushMessageTemplate/js_AppMessageReminder.php") ?>

<script language="javascript">

$(document).ready(function(){
	AppMessageReminder.initInsertAtTextarea();
	AppMessageReminder.TargetAjaxScript = 'ajax_send_reminder_update.php';
});

</script>

<div class="content_top_tool">
	<div class="Conntent_tool"><?=$PrintBtn?></div>
	<br style="clear:both" />
</div>

<table class="form_table_v30">
	<tr valign='top'>
		<td class='field_title'><?=$Lang['Header']['Menu']['Period']?></td>
		<td><?=intranet_htmlspecialchars($DateStart)?> <?=$Lang['General']['To']?> <?=intranet_htmlspecialchars($DateEnd)?></td>
	</tr>
	
	<tr valign='top'>
		<td class='field_title'><?=$Lang['StaffAttendance']['Target']?></td>
		<td>
		<?php 
		$sep = "";
		foreach($ClassNameAry as $DisplayClassName){
			echo $sep.intranet_htmlspecialchars($DisplayClassName);
			$sep = ", ";
		}
		?>
		</td>
	</tr>
	
	<tr>
		<td class='field_title'><?=$Lang['General']['Type']?></td>
		<td>
		<?php
		if(in_array(PROFILE_TYPE_ABSENT,$record_type)){
			echo $Lang['StudentAttendance']['Absent'];
		}
		if(in_array(PROFILE_TYPE_EARLY,$record_type)){
			echo (count($record_type)>1? ', ':'').$Lang['StudentAttendance']['EarlyLeave'];
		}
		?>
		</td>
	</tr>
	<tr>
		<td class='field_title'><?=$Lang['StudentAttendance']['AbsentProveReport']['Hand-inStatus']?></td>
		<td><?= $document_status==1 ? $Lang['StudentAttendance']['AbsentProveReport']['AllHand-inStatus'] : $Lang['eNotice']['UnsignedOnly'] ?></td>
	</tr>
</table>

<form name="form1" id="form1" method="POST" action="" >

	<div>
		<?= $li->display(); ?>
	</div>
	<div>
		<span class="tabletextremark"><?=$Lang['StudentAttendance']['AbsentProveReport']['RemarksAMPMInconsistency']?></span>
	</div>
	<div class="edit_bottom_v30">
		<?= $thickbox['Button'] ?>
		<?= $Backbutton ?>
	</div>
	
	<p><center><span id='PreviewStatus'></span></center></p>
	<input type="hidden" name="target_student" value="<?=rawurlencode(serialize($ClassNameAry))?>">
	<input type="hidden" name="DateStart" value="<?=escape_double_quotes($DateStart)?>">
	<input type="hidden" name="DateEnd" value="<?=escape_double_quotes($DateEnd)?>">
	<?php
	for($i=0;$i<count($record_type);$i++){
		echo '<input type="hidden" name="record_type[]" value="'.escape_double_quotes($record_type[$i]).'" />'."\n";
	}
	?>
	<input type="hidden" name="document_status" value="<?=escape_double_quotes($document_status)?>">
	<input type="hidden" name="pageNo" id="pageNo" value="<?=escape_double_quotes($li->pageNo)?>" />
	<input type="hidden" name="order" id="order" value="<?=escape_double_quotes($li->order)?>" />
	<input type="hidden" name="field" id="field" value="<?=escape_double_quotes($li->field)?>" />
	<input type="hidden" name="page_size_change" id="page_size_change" value="" />
	<input type="hidden" name="numPerPage" id="numPerPage" value="<?=escape_double_quotes($li->page_size)?>" />

</form>
<?=$pushMessageButton?>
<?= $thickbox['Content']?>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>