<?php
//using by: 

##################################
# change log:
#	Date:	2016-11-16	Omas
#			Modified getNotSumttedDocumentStudentDataSQL() - fixing showing <br> problem
#
#	Date:	2016-10-17 	Carlos
#			Modified the record array to be associated with student id and record type in order to split absent and early leave records.
#
#	Date:	2016-06-30	Carlos
#			Added record_type parameter to filter absent/early leave records.
#
#	Date:	2014-12-10	Omas
#			Created this file - for ajax sending push message to eClass App
#
##################################

@set_time_limit(21600);
@ini_set('max_input_time', 21600);
@ini_set('memory_limit', -1);

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp.php");
include_once($PATH_WRT_ROOT."includes/libsmsv2.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");

intranet_auth();
intranet_opendb();

$luser = new libuser();
$libeClassApp = new libeClassApp();
$ldbsms 	= new libsmsv2();
$lc = new libcardstudentattend2();

# DataReady
$StudentList = unserialize(rawurldecode($_POST['StudentList']));
$DateStart = $_POST['DateStart'];
$DateEnd = $_POST['DateEnd'];
//$document_status = $_POST['document_status'];
$record_type = $_POST['record_type'];
$messageTitle = standardizeFormPostValue($_POST['PushMessageTitle']);
$messageContent = standardizeFormPostValue($_POST['MessageContent']);
$studentWithParentUsingAppAry = $luser->getStudentWithParentUsingParentApp();

# get Send target ID
$getDataSQL = $lc->getNotSumttedDocumentStudentDataSQL($StudentList,$DateStart,$DateEnd, 0, $record_type, false, ', ');
$sendTargetAry = $lc->ReturnResultSet($getDataSQL);
$sendTargetNum = count($sendTargetAry);
$sendTargetIdAry = array_values(array_unique(Get_Array_By_Key($sendTargetAry, 'StudentID')));


## Select students whose parent using App
$TargetUsers = $sendTargetIdAry;
$numOfUser = count($TargetUsers);
$lu = new libuser('', '', $TargetUsers);

for ($i = 0; $i < $numOfUser; $i++) {
	$studentId = $TargetUsers[$i];
	
	$lu->loadUserData($studentId);
	
	$appParentIdAry = $lu->getParentUsingParentApp($studentId);
	if (in_array($studentId, $studentWithParentUsingAppAry)) {
		$studentIds[] = $studentId;
	}

}

$isPublic = "N";
$parentStudentAssoAry = BuildMultiKeyAssoc($luser->getParentStudentMappingInfo($studentIds), 'ParentID', $IncludedDBField=array('StudentID'), $SingleValue=1, $BuildNumericArray=1);

## Build Data of unsigned notice for string replace
$TargetMessageDataAry = $sendTargetAry;
$TargetMessageDataAssoAry = BuildMultiKeyAssoc($TargetMessageDataAry, array('StudentID','RecordType'));

## Build Data for send message
$appType = $eclassAppConfig['appType']['Parent'];
$sendTimeMode = "";
$sendTimeString = "";
$individualMessageInfoAry = array();
$i = 0;
if(!empty($studentIds)){
	foreach ($studentIds as $studentId) {
		$appParentIdAry = $luser->getParentUsingParentApp($studentId);
	
		foreach($TargetMessageDataAssoAry[$studentId] as $profile_type => $recordAry)
		{
		
			$_individualMessageInfoAry = array();
			foreach ($appParentIdAry as $parentId) {
				$_individualMessageInfoAry['relatedUserIdAssoAry'][$parentId] = (array)$studentId;
			}
			$_individualMessageInfoAry['messageTitle'] = $messageTitle;
			$_individualMessageInfoAry['messageContent'] = $ldbsms->replace_content($studentId, $messageContent, '');
		
			$_individualMessageInfoAry['messageContent'] = str_replace('($Dates)',$TargetMessageDataAssoAry[$studentId][$profile_type]['Dates'],$_individualMessageInfoAry['messageContent']);
			$_individualMessageInfoAry['messageContent'] = str_replace('($Num_Of_Not_Submitted)',$TargetMessageDataAssoAry[$studentId][$profile_type]['countnot'],$_individualMessageInfoAry['messageContent']);
		
			$individualMessageInfoAry[$i] = $_individualMessageInfoAry;
		
			$i++;
		
		}
	}
}

$NumOfRecepient = $i;
## send message
$notifyMessageId = $libeClassApp->sendPushMessage($individualMessageInfoAry, $messageTitle, 'MULTIPLE MESSAGES', $isPublic, $recordStatus=1, $appType, $sendTimeMode, $sendTimeString);

//$msg = ($notifyMessageId > 0)? $Lang['AppNotifyMessage']['ReturnMsg']['SendSuccess']:$Lang['AppNotifyMessage']['ReturnMsg']['SendFail'];

$sent =  ($notifyMessageId > 0) ? 1 : 0;
$statisticsAry = $libeClassApp->getPushMessageStatistics($notifyMessageId);
$statisticsAry = $statisticsAry[$notifyMessageId];

if($sent == 1){
	echo "<font color='#DD5555'>".str_replace("[SentTotal]", $statisticsAry['numOfSendSuccess'], $Lang['AppNotifyMessage']['eNotice']['send_result']). "</font>";
}
else{
	$ErrorMsg = $Lang['AppNotifyMessage']['eNotice']['send_failed'];
	if ($statisticsAry['numOfRecepient'] == 0) {
		$ErrorMsg .= $Lang['AppNotifyMessage']['eNotice']['send_result_no_parent'];
	}
	echo "<font color='red'>".$ErrorMsg."</font> <font color='grey'>(".$statisticsAry['numOfRecepient']."-".$statisticsAry['numOfSendSuccess'].")</font>";
}
intranet_closedb();
?>