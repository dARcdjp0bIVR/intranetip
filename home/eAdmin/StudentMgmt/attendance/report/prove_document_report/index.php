<?php
// Editing by 
##################################
# change log:
#	Date:	2016-06-30 	Carlos
#			Added Early Leave record type.
#
#	Date:	2014-12-10	Omas
#			Created this file - New Report for eAttendance
#
##################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();
$linterface = new interface_html();
$lclass= new libclass();

$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = 'PageReport_ProveDocReport';

$TAGS_OBJ[] = array($Lang['StudentAttendance']['AbsentProveReport']['AbsentProveReport'] , "", 0);

# class list
$select_class = $lc->getSelectClass('name="ClassName[]" id="ClassName" multiple class="class_list" size="10" style="min-width:150px; width:200px;"',$class_name,1);

# start / end date
$start_date = date("Y-m-d",getStartOfAcademicYear());
$end_date = date("Y-m-d",getEndOfAcademicYear());

$MODULE_OBJ = $lc->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

?>
<script>

$(document).ready(function(){
	
});

function checkform(){
	var error_no = 0;

	if (compareDate($('#DateStart').val(),$('#DateEnd').val()) > 0){
		$('#div_DateEnd_err_msg').html('<?=$i_Homework_new_duedate_wrong?>');
		error_no ++;
	}
	
	if ( $("#DPWL-DateStart").html() != '' || $("#DPWL-DateEnd").html() != ''){
		error_no ++;
	}
	
	if ( $('select#ClassName').val() == null ){
		$('div#ClassWarning').show();
		error_no ++;
	}
	
	$('div#TypeWarning').hide();
	if($('input[name="record_type\\[\\]"]:checked').length == 0)
	{	
		$('div#TypeWarning').show();
		error_no++;
	}
		
	if (error_no == 0){
		$('#form1').submit();
		return true;
	}
	else{
		return false;
	}

}

</script>

<form name="form1" id="form1" method="post" action="result.php">
<br>
<table class="form_table_v30">

<tr valign='top'>
	<td class='formfieldtitle tabletext'><?=$iDiscipline['Period']?></td>
	<td>
		<?=$iDiscipline['Period_Start']?> <?=$linterface->GET_DATE_PICKER("DateStart",$start_date)?>
		<?=$iDiscipline['Period_End']?> <?=$linterface->GET_DATE_PICKER("DateEnd",$end_date)?>
		<br><span id='div_DateEnd_err_msg'></span>
	</td>
</tr>

<tr>
	<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><label for="ClassName[]"><?=$button_select.' '.$Lang['StaffAttendance']['Target']?></label></td>
	<td width="70%"class="tabletext">
		<?=$select_class?>
		&nbsp;<?=$linterface->GET_SMALL_BTN($Lang['Btn']['SelectAll'], "button", "Select_All_Options('ClassName',true);return false;")?>
		<div class="tabletextremark"><?=$Lang['SysMgr']['FormClassMapping']['CtrlMultiSelectMessage']?></div>
		<?=$linterface->Get_Form_Warning_Msg('ClassWarning', $Lang['StudentAttendance']['PleaseSelectAtLeastOnClass'], 'ClassWarning')?>
	</td>
</tr>

<tr>
	<td class='formfieldtitle tabletext'><?=$Lang['General']['Type']?></td>
	<td>
		<?=$linterface->Get_Checkbox("record_type[1]", "record_type[]", 1, $isChecked=1,"", $Lang['StudentAttendance']['Absent'])?>
		<?=$linterface->Get_Checkbox("record_type[3]", "record_type[]", 3, $isChecked=0,"", $Lang['StudentAttendance']['EarlyLeave'])?>
		<?=$linterface->Get_Form_Warning_Msg('TypeWarning', $Lang['General']['PleaseSelect'], 'TypeWarning')?>
	</td>
</tr>

<tr>
	<td class='formfieldtitle tabletext'><?=$Lang['StudentAttendance']['AbsentProveReport']['Hand-inStatus']?></td>
	<td><?=$linterface->Get_Radio_Button("document_status1", "document_status", 1, $isChecked=1,"", $Lang['StudentAttendance']['AbsentProveReport']['AllHand-inStatus'])?>
		<?=$linterface->Get_Radio_Button("document_status3", "document_status", 0, $isChecked=0,"", $Lang['StudentAttendance']['AbsentProveReport']['StatusNotHandin'])?>
	</td>
</tr>

</table>
<br>
<div class="edit_bottom_v30">
	<?= $linterface->GET_ACTION_BTN($button_submit, "button", "checkform()" , "submitbtn") ?>
	<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='index.php'","cancelbtn") ?>
</div>	

</form>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>