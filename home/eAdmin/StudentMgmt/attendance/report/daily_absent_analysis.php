<?
//using by 
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();
$lc->retrieveSettings();
if($lc->attendance_mode==0 || $lc->attendance_mode==1)//AM Only or PM Only
{
	header("Location: $PATH_WRT_ROOT/home/eAdmin/StudentMgmt/attendance/");
	intranet_closedb();
	exit();
}

$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageReport_DailyAbsentAnalysis";

$linterface = new interface_html();

if($TargetDate=='' || strtotime($TargetDate)==-1) $TargetDate=date("Y-m-d");

# Get Classes List
$classes = $lc->getClassList();
$select_class = '<select name="ClassID[]" id="ClassID" class="inputfield" size="10" multiple>';
for ($i=0; $i<sizeof($classes); $i++)
{
	$selected = (is_array($ClassID) && in_array($classes[$i][0],$ClassID))?"selected":"";
	$select_class .= "<option value=\"".$classes[$i][0]."\" $selected>".intranet_htmlspecialchars($classes[$i][1])."</option>\n";
}
$select_class.= '</select>';

# absent session
$select_absent_session = "<select name='session' id='session'>";
$select_absent_session .= "<option value='' ".(($session=='')?"selected":"").">".$Lang['Btn']['All']."</option>";
$select_absent_session .= "<option value='".PROFILE_DAY_TYPE_WD."' ".(($session==PROFILE_DAY_TYPE_WD)?"selected":"").">".$Lang['StudentAttendance']['BothAMPMAbsent']."</option>";//1
$select_absent_session .= "<option value='".PROFILE_DAY_TYPE_AM."' ".(($session==PROFILE_DAY_TYPE_AM)?"selected":"").">".$Lang['StudentAttendance']['OnlyAMAbsent']."</option>";//2
$select_absent_session .= "<option value='".PROFILE_DAY_TYPE_PM."' ".(($session==PROFILE_DAY_TYPE_PM)?"selected":"").">".$Lang['StudentAttendance']['OnlyPMAbsent']."</option>";//3
$select_absent_session .= "</select>";

# format
$select_format ="<select name='format' id='format'>";
$select_format .="<option value='WEB' ".(($format=='' || $format=='WEB')?"selected":"").">WEB</option>";
$select_format .="<option value='CSV' ".(($format=='CSV')?"selected":"").">CSV</option>";
$select_format .="</select>";

$TAGS_OBJ[] = array($Lang['StudentAttendance']['DailyAbsentAnalysisReport'], "", 0);

$MODULE_OBJ = $lc->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();
?>
<script language="JavaScript" type="text/javascript">
function SelectAll(obj)
{
	for (i=0; i<obj.length; i++)
	{
		obj.options[i].selected = true;
	}
}

function submitForm(obj)
{
  var countSelectedClass = 0;
  var classids=document.getElementById("ClassID");
  for(var i=0;i<classids.length;i++)
  {
  	if(classids.options[i].selected)
  	{
  		countSelectedClass += 1;
  		break;
  	}
  }
  if(countSelectedClass == 0)
  {
  	  alert('<?=$i_Discipline_System_alert_PleaseSelectClass?>');
  	  return false;
  }
  if(check_date(obj.TargetDate,'<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>'))
  {
  	  if(obj.format.value=='CSV')
  	  	obj.action='daily_absent_analysis_export.php';
  	  else
  	  	obj.action='daily_absent_analysis.php';
  	  obj.submit();
  }
}
</script>
<br />
<form name="form1" id="form1" action="daily_absent_analysis.php" method="POST">
<table width="96%" border="0" cellspacing="0" cellpadding="5" align="center">
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?="$button_select $i_ClassName"?></td>
		<td width="70%"class="tabletext">
			<?=$select_class?>&nbsp;
			<span><?= $linterface->GET_SMALL_BTN($button_select_all, "button", "SelectAll(document.form1.elements['ClassID[]']);return false;")?></span>
		</td>
	</tr>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_StudentAttendance_View_Date?></td>
		<td width="70%"class="tabletext">
			<?=$linterface->GET_DATE_PICKER("TargetDate",$TargetDate)?>
		</td>
	</tr>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$Lang['StudentAttendance']['AbsentSessionType']?></td>
		<td width="70%"class="tabletext">
			<?=$select_absent_session?>
		</td>
	</tr>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_general_Format?></td>
		<td width="70%"class="tabletext">
			<?=$select_format?>
		</td>
	</tr>
</table>
<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
    	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
    </tr>
</table>
<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
	    <td align="center" colspan="2">
			<?= $linterface->GET_ACTION_BTN($button_view, "button", "submitForm(document.form1)") ?>
		</td>
	</tr>
</table>
</form>
<script language="JavaScript" type="text/javascript">
function openPrintPage()
{
    newWindow("daily_absent_analysis_print.php?ClassID=<?=implode(',',IntegerSafe($ClassID))?>&TargetDate=<?=urlencode($TargetDate)?>&session=<?=urlencode($session)?>",10);
}
</script>
<?
if(isset($ClassID) && isset($TargetDate) && isset($session))
{
	$lclass = new libclass();
	$classnames=array();
	for($i=0;$i<sizeof($ClassID);$i++)
	{
		$classnames[]="'".$lc->Get_Safe_Sql_Query($lclass->getClassName($ClassID[$i]))."'";
	}
	$cond_class =" AND u.ClassName IN (".implode(',',$classnames).") ";
	
	$toolbar .= $linterface->GET_LNK_PRINT("javascript:openPrintPage()","","","","",0);
	
	$display = '<table width="96%" border="0" cellpadding="0" cellspacing="0" align="center">
					<tr>
						<td align="left">'.$toolbar.'</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
				</table>';
	
	$date_arr = explode('-',$TargetDate);
	$txt_year = $date_arr[0];
	$txt_month = $date_arr[1];
	$txt_day = $date_arr[2];
	
	$CARD_STATUS_EARLY = 4;
	$STATUS['--'] = "--";
	$STATUS[CARD_STATUS_PRESENT] = $i_StudentAttendance_Status_OnTime;
	$STATUS[CARD_STATUS_ABSENT] = $i_StudentAttendance_Status_Absent;
	$STATUS[CARD_STATUS_LATE] = $i_StudentAttendance_Status_Late;
	$STATUS[CARD_STATUS_OUTING] = $i_StudentAttendance_Status_Outing;
	$STATUS[$CARD_STATUS_EARLY] = $i_StudentAttendance_Status_EarlyLeave;
	
	$WAIVED['--'] = '--';
	$WAIVED[0] = $Lang['General']['No'];
	$WAIVED[1] = $Lang['General']['Yes'];
	
	if($session==PROFILE_DAY_TYPE_AM)//Find students that only AM are absent
	{
		$cond_status = " AND (AMStatus = '".CARD_STATUS_ABSENT."' AND (PMStatus != '".CARD_STATUS_ABSENT."' OR PMStatus IS NULL)) ";
	}else if($session==PROFILE_DAY_TYPE_PM)//Find students that only PM are absent
	{
		$cond_status = " AND ((AMStatus != '".CARD_STATUS_ABSENT."' OR AMStatus IS NULL) AND PMStatus = '".CARD_STATUS_ABSENT."') ";
	}else if($session==PROFILE_DAY_TYPE_WD)// Find students that both AM and PM are absent
	{
		$cond_status = " AND (AMStatus = '".CARD_STATUS_ABSENT."' AND PMStatus = '".CARD_STATUS_ABSENT."') ";
	}else
	{
		$cond_status = " AND (AMStatus = '".CARD_STATUS_ABSENT."' OR PMStatus = '".CARD_STATUS_ABSENT."') ";
	}
	$lc->createTable_Card_Student_Daily_Log($txt_year,$txt_month);
	$card_log_table_name = "CARD_STUDENT_DAILY_LOG_".$txt_year."_".$txt_month;
	$namefield = getNameFieldByLang('u.');
	/*
	$sql="SELECT
				u.UserID,
				u.ClassName,
				u.ClassNumber,
				$namefield as StudentName,
				IF(c.LeaveStatus = '".CARD_LEAVE_AM."',$CARD_STATUS_EARLY,IF(c.AMStatus IS NULL,'--',c.AMStatus)) as AMStatus,
				IF(c.LeaveStatus = '".CARD_LEAVE_PM."',$CARD_STATUS_EARLY,IF(c.PMStatus IS NULL,'--',c.PMStatus)) as PMStatus,
				IF(ram.Reason IS NULL OR ram.Reason='', '--', ram.Reason) as AMReason,
				IF(rpm.Reason IS NULL OR rpm.Reason='', '--', rpm.Reason) as PMReason,
				IF(pam.RecordStatus IS NULL, '--', pam.RecordStatus) as AMWaive,
				IF(ppm.RecordStatus IS NULL, '--', ppm.RecordStatus) as PMWaive,
				IF(c.AMStatus = '".CARD_STATUS_ABSENT."',IF(ram.AbsentSession IS NULL, '--', ram.AbsentSession),'--') as AMAbsentSession,
				IF(c.PMStatus = '".CARD_STATUS_ABSENT."',IF(rpm.AbsentSession IS NULL, '--', rpm.AbsentSession),'--') as PMAbsentSession
		  FROM
				INTRANET_USER AS u
				INNER JOIN $card_log_table_name AS c ON c.UserID = u.UserID AND c.DayNumber = '$txt_day' 
						   $cond_status 
				LEFT JOIN PROFILE_STUDENT_ATTENDANCE as pam ON pam.UserID = c.UserID 
							AND pam.RecordType IN ('".CARD_STATUS_ABSENT."', '".CARD_STATUS_LATE."')
							AND pam.AttendanceDate LIKE '$TargetDate%' 
							AND pam.DayType = '".PROFILE_DAY_TYPE_AM."'
				LEFT JOIN PROFILE_STUDENT_ATTENDANCE as ppm ON ppm.UserID = c.UserID 
							AND ppm.RecordType IN ('".CARD_STATUS_ABSENT."', '".CARD_STATUS_LATE."') 
							AND ppm.AttendanceDate LIKE '$TargetDate%' 
							AND ppm.DayType = '".PROFILE_DAY_TYPE_PM."'
				LEFT JOIN CARD_STUDENT_PROFILE_RECORD_REASON as ram ON ram.ProfileRecordID = pam.StudentAttendanceID 
							AND ram.StudentID = pam.UserID
							AND ram.RecordDate = '$TargetDate' 
							AND ram.DayType = '".PROFILE_DAY_TYPE_AM."' 
				LEFT JOIN CARD_STUDENT_PROFILE_RECORD_REASON as rpm ON rpm.ProfileRecordID = ppm.StudentAttendanceID 
							AND rpm.StudentID = ppm.UserID
							AND rpm.RecordDate = '$TargetDate' 
							AND rpm.DayType = '".PROFILE_DAY_TYPE_PM."' 
		  WHERE
				u.RecordType=2 AND u.RecordStatus IN (0,1,2) 
				$cond_class
		  ORDER BY
				u.ClassName, u.ClassNumber ";
	*/
	$sql="SELECT 
				u.UserID,
				u.ClassName,
				u.ClassNumber,
				$namefield as StudentName,
				(CASE 
					WHEN c.AMStatus IS NOT NULL AND c.AMStatus = '".CARD_STATUS_PRESENT."' THEN '".CARD_STATUS_PRESENT."'
					WHEN c.AMStatus IS NOT NULL AND c.AMStatus = '".CARD_STATUS_OUTING."' THEN '".CARD_STATUS_OUTING."' 
					WHEN ram.RecordID IS NOT NULL AND ram.RecordType = '".PROFILE_TYPE_ABSENT."' THEN '".CARD_STATUS_ABSENT."'
					WHEN ram.RecordID IS NOT NULL AND ram.RecordType = '".PROFILE_TYPE_LATE."' THEN '".CARD_STATUS_LATE."'
					WHEN ram.RecordID IS NOT NULL AND ram.RecordType = '".PROFILE_TYPE_EARLY."' THEN '".$CARD_STATUS_EARLY."' 
					WHEN c.LeaveStatus = '".CARD_LEAVE_AM."' THEN '".$CARD_STATUS_EARLY."' 
					WHEN c.AMStatus IS NOT NULL AND c.AMStatus <> '' THEN c.AMStatus
					ELSE '--' 
				END) as AMStatus,
				(CASE 
					WHEN c.PMStatus IS NOT NULL AND c.PMStatus = '".CARD_STATUS_PRESENT."' THEN '".CARD_STATUS_PRESENT."'
					WHEN c.PMStatus IS NOT NULL AND c.PMStatus = '".CARD_STATUS_OUTING."' THEN '".CARD_STATUS_OUTING."' 
					WHEN rpm.RecordID IS NOT NULL AND rpm.RecordType = '".PROFILE_TYPE_ABSENT."' THEN '".CARD_STATUS_ABSENT."'
					WHEN rpm.RecordID IS NOT NULL AND rpm.RecordType = '".PROFILE_TYPE_LATE."' THEN '".CARD_STATUS_LATE."'
					WHEN rpm.RecordID IS NOT NULL AND rpm.RecordType = '".PROFILE_TYPE_EARLY."' THEN '".$CARD_STATUS_EARLY."' 
					WHEN c.LeaveStatus = '".CARD_LEAVE_PM."' THEN '".$CARD_STATUS_EARLY."' 
					WHEN c.PMStatus IS NOT NULL AND c.PMStatus <> '' THEN c.PMStatus 
					ELSE '--' 
				END) as PMStatus,
				IF(ram.Reason IS NULL OR ram.Reason='', '--', ram.Reason) as AMReason,
				IF(rpm.Reason IS NULL OR rpm.Reason='', '--', rpm.Reason) as PMReason,
				IF(ram.RecordStatus IS NULL, '--', ram.RecordStatus) as AMWaive,
				IF(rpm.RecordStatus IS NULL, '--', rpm.RecordStatus) as PMWaive,
				IF(c.AMStatus = '".CARD_STATUS_ABSENT."',IF(ram.AbsentSession IS NULL, '--', ram.AbsentSession),'--') as AMAbsentSession,
				IF(c.PMStatus = '".CARD_STATUS_ABSENT."',IF(rpm.AbsentSession IS NULL, '--', rpm.AbsentSession),'--') as PMAbsentSession,
				c.AMStatus as StatusAM, c.PMStatus as StatusPM, c.LeaveStatus as StatusLeave  
		  FROM
				INTRANET_USER AS u
				INNER JOIN $card_log_table_name AS c ON c.UserID = u.UserID AND c.DayNumber = '".$lc->Get_Safe_Sql_Query($txt_day)."' 
						   $cond_status 
				LEFT JOIN CARD_STUDENT_PROFILE_RECORD_REASON as ram ON ram.StudentID = c.UserID
							AND ram.RecordDate = '".$lc->Get_Safe_Sql_Query($TargetDate)."' 
							AND ram.DayType = '".PROFILE_DAY_TYPE_AM."' 
				LEFT JOIN CARD_STUDENT_PROFILE_RECORD_REASON as rpm ON rpm.StudentID = c.UserID
							AND rpm.RecordDate = '".$lc->Get_Safe_Sql_Query($TargetDate)."' 
							AND rpm.DayType = '".PROFILE_DAY_TYPE_PM."' 
		  WHERE
				u.RecordType=2 AND u.RecordStatus IN (0,1,2) 
				$cond_class 
		  ORDER BY
				u.ClassName, u.ClassNumber ";
	$result = $lc->returnArray($sql);
	
	$cols=10;
	$display .= "<table width=\"96%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">";
	$display .= "<tr class=\"tablebluetop\">";
	$display .= "<td class=\"tabletoplink\">$i_ClassName</td>";
	$display .= "<td class=\"tabletoplink\">$i_ClassNumber</td>";
	$display .= "<td class=\"tabletoplink\">$i_UserStudentName</td>";
	$display .= "<td class=\"tabletoplink\">$i_DayTypeAM $i_StudentAttendance_Status</td>";
	if($sys_custom['SmartCardAttendance_StudentAbsentSession'])
	{
		$display .= "<td class=\"tabletoplink\">$i_DayTypeAM ".$Lang['StudentAttendance']['AbsentSessions']."</td>";
		$cols++;
	}
	$display .= "<td class=\"tabletoplink\">$i_DayTypeAM $i_SmartCard_Frontend_Take_Attendance_Waived</td>";
	$display .= "<td class=\"tabletoplink\">$i_DayTypeAM $i_Attendance_Reason</td>";
	$display .= "<td class=\"tabletoplink\">$i_DayTypePM $i_StudentAttendance_Status</td>";
	if($sys_custom['SmartCardAttendance_StudentAbsentSession'])
	{
		$display .= "<td class=\"tabletoplink\">$i_DayTypepM ".$Lang['StudentAttendance']['AbsentSessions']."</td>";
		$cols++;
	}
	$display .= "<td class=\"tabletoplink\">$i_DayTypePM $i_SmartCard_Frontend_Take_Attendance_Waived</td>";
	$display .= "<td class=\"tabletoplink\">$i_DayTypePM $i_Attendance_Reason</td>";
	$display .= "<td class=\"tabletoplink\">".$i_StudentGuardian['MenuInfo']."</td>";
	$display .= "</tr>";
	
	if(sizeof($result)==0)
	{
		$display.="<tr class=\"tablebluerow2\">";
		$display.="<td class=\"tabletext\" colspan=\"$cols\" align=\"center\">$i_no_record_exists_msg</td>";
		$display.="</tr>";
	}
	
	for($i=0;$i<sizeof($result);$i++)
	{
		list($user_id,$class_name,$class_number,$student_name,$am_status,$pm_status,$am_reason,$pm_reason,$am_waive,$pm_waive,$am_absentsession,$pm_absentsession) = $result[$i];
		
		$guardian_row = "<td class=\"tabletext\">";
		// Get guardian information
		$main_content_field = "IF(EnName IS NOT NULL AND EnName !='' AND ChName IS NOT NULL AND ChName !='', IF(IsMain = 1, CONCAT('* ', EnName,' / ',ChName), CONCAT(EnName,' / ',ChName)), IF(EnName IS NOT NULL AND EnName!='',EnName,ChName))";
		$main_content_field = "IF($main_content_field='' OR $main_content_field IS NULL,'-',$main_content_field)";
		$sql = "SELECT
				  $main_content_field,
				  Relation,
				  Phone,
				  EmPhone,
				  IsMain
				FROM
				  $eclass_db.GUARDIAN_STUDENT
				WHERE
				  UserID = '".$lc->Get_Safe_Sql_Query($user_id)."'
				ORDER BY
				  IsMain DESC, Relation ASC
				";
		$guardian_result = $lc->returnArray($sql,5);
		if (sizeof($guardian_result)==0)
			$guardian_row .= "--";
		else
		{
		    for($j=0; $j<sizeof($guardian_result); $j++)
		    {
				list($name, $relation, $phone, $em_phone) = $guardian_result[$j];
				$no = $j+1;
				$guardian_row .= "$name ($ec_guardian[$relation]) ($i_StudentGuardian_Phone) $phone ($i_StudentGuardian_EMPhone) $em_phone <br>\n";
		  	}
		}
		$guardian_row .= '</td>';
		
		$css = ($i % 2)==0?"tablebluerow1":"tablebluerow2";
		$display .= "<tr class=\"$css\">";
		$display .= "<td class=\"tabletext\">".intranet_htmlspecialchars($class_name)."</td>";
		$display .= "<td class=\"tabletext\">$class_number</td>";
		$display .= "<td class=\"tabletext\">".intranet_htmlspecialchars($student_name)."</td>";
		$display .= "<td class=\"tabletext\">".$STATUS[$am_status]."</td>";
		if($sys_custom['SmartCardAttendance_StudentAbsentSession'])
		{
			$display .= "<td class=\"tabletext\">".$am_absentsession."</td>";
		}
		$display .= "<td class=\"tabletext\">".$WAIVED[$am_waive]."</td>";
		$display .= "<td class=\"tabletext\">".intranet_htmlspecialchars($am_reason)."</td>";
		$display .= "<td class=\"tabletext\">".$STATUS[$pm_status]."</td>";
		if($sys_custom['SmartCardAttendance_StudentAbsentSession'])
		{
			$display .= "<td class=\"tabletext\">".$pm_absentsession."</td>";
		}
		$display .= "<td class=\"tabletext\">".$WAIVED[$pm_waive]."</td>";
		$display .= "<td class=\"tabletext\">".intranet_htmlspecialchars($pm_reason)."</td>";
		$display .= $guardian_row;
		$display .= "</tr>";
	}
	
	$display.="</table>";
	$display.="<br />";
	
	echo $display;
}

$linterface->LAYOUT_STOP();
intranet_closedb();
?>