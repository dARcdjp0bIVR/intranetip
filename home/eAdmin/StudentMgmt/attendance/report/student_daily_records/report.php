<?php
// Editing by 
/*
 * 2016-06-10 (Carlos): Created.
 */
set_time_limit(60*60);
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libstudentattendance_ui.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$StartDate = $_POST['StartDate'];
$EndDate = $_POST['EndDate'];
$TargetType = $_POST['TargetType'];
$TargetID = $_POST['TargetID'];
$Format = $_POST['Format']; // ""/"web", "pdf", "print"

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-LessonAttendance"] || !$_SESSION["SSV_PRIVILEGE"]["plugin"]["attendancelesson"] || !$sys_custom['LessonAttendance_LaSalleCollege'] || count($TargetID)==0 || !in_array($Format,array("","web","pdf","print"))) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$linterface = new interface_html();
$LessonAttendUI = new libstudentattendance_ui();
$scm = new subject_class_mapping();

$academic_year_id = $LessonAttendUI->CurAcademicYearID;
$statusMap = $LessonAttendUI->LessonAttendanceStatusMapping();

//$student_id = IntegerSafe($TargetID[0]);
//$luser = new libuser($student_id);

$reasons = $LessonAttendUI->GetLessonAttendanceReasons();
$reason_size = count($reasons);
$reasonToSymbolMap = array();
for($i=0;$i<$reason_size;$i++){
	if(!isset($reasonToSymbolMap[$reasons[$i]['ReasonType']])){
		$reasonToSymbolMap[$reasons[$i]['ReasonType']] = array();
	}
	$reasonToSymbolMap[$reasons[$i]['ReasonType']][$reasons[$i]['ReasonText']] = $reasons[$i]['ReasonSymbol'];
}

//debug_pr($reasonToSymbolMap);

// prepare date columns
$start_ts = strtotime($StartDate);
$end_ts = strtotime($EndDate);
$weeks = array();
$firstday_weekday = intval(date("w",$start_ts));
$lastday_weekday = intval(date("w",$end_ts));
$startday_ts = $start_ts - $firstday_weekday * 86399;
$endday_ts = $end_ts + $lastday_weekday * 86399;
for($cur_ts=$startday_ts;$cur_ts<=$endday_ts && $cur_ts<=$end_ts;$cur_ts += 86400*7)
{
	$monday_cur_ts = $cur_ts + 86399;
	$friday_cur_ts = $cur_ts + 86399*6;
	$start_date = date("Y-m-d",$monday_cur_ts);
	$end_date = date("Y-m-d",$friday_cur_ts);
	$start_weekday = date("w",$monday_cur_ts);
	$end_weekday = date("w",$friday_cur_ts);
	$weeks[] = array($start_date, $end_date, $monday_cur_ts, $friday_cur_ts);
}

//debug_pr($weeks);

$report_title = $Lang['LessonAttendance']['StudentDailyRecords'];

function getReportContent($student_id)
{	
	global $Lang, $linterface, $LessonAttendUI, $scm, $academic_year_id, $statusMap, $StartDate, $EndDate, $Format, $weeks, $reasonToSymbolMap, $reasons, $reason_size;
	
	$luser = new libuser($student_id);
	$student_name_field = getNameFieldByLang2("u1.");
	$subject_name = Get_Lang_Selection("s.CH_DES","s.EN_DES");
	
	$sql = "SELECT 
				sgao.LessonDate,
				itt.TimeSlotName,
				sgao.StartTime,
				sgsa.StudentID,
				sgsa.AttendStatus,
				sgsa.Reason,
				st.SubjectID,
				$subject_name as Subject,
				u1.ClassName,
				u1.ClassNumber,
				$student_name_field as StudentName 
			FROM 
			SUBJECT_GROUP_ATTEND_OVERVIEW_".$academic_year_id." as sgao 
			INNER JOIN SUBJECT_GROUP_STUDENT_ATTENDANCE_".$academic_year_id." as sgsa ON sgsa.AttendOverviewID=sgao.AttendOverviewID 
			INNER JOIN INTRANET_USER as u1 ON u1.UserID=sgsa.StudentID 
			INNER JOIN SUBJECT_TERM_CLASS as stc ON sgao.SubjectGroupID=stc.SubjectGroupID 
			INNER JOIN SUBJECT_TERM as st ON st.SubjectGroupID=stc.SubjectGroupID 
			INNER JOIN ASSESSMENT_SUBJECT as s ON st.SubjectID=s.RecordID 
			LEFT JOIN SUBJECT_TERM_CLASS_TEACHER as stct ON stct.SubjectGroupID=st.SubjectGroupID 
			LEFT JOIN INTRANET_TIMETABLE_ROOM_ALLOCATION as itra ON itra.RoomAllocationID=sgao.RoomAllocationID 
			LEFT JOIN INTRANET_TIMETABLE_TIMESLOT as itt ON itt.TimeSlotID=itra.TimeSlotID 
			WHERE sgsa.StudentID='$student_id' AND (sgao.LessonDate BETWEEN '$StartDate' AND '$EndDate')  ";
	$sql .= " GROUP BY sgao.AttendOverviewID,sgao.LessonDate,sgsa.StudentID ";
	$sql.= " ORDER BY sgao.LessonDate,sgao.StartTime ";
	
	$records = $LessonAttendUI->returnResultSet($sql);
	$record_size = count($records);
	$dateToRecords = array();
	
	for($i=0;$i<$record_size;$i++)
	{
		$date = $records[$i]['LessonDate'];
		
		if(!isset($dateToRecords[$date])){
			$dateToRecords[$date] = array();
		}
		$dateToRecords[$date][] = $records[$i];
	}
	
	$x .= '<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">' . "\n";
		$x .= '<tr style="background:#f3f3f3;">' . "\n";
			$x .= '<td colspan="2" style="width:50%;">' . "\n";
				$x .= $Lang['Identity']['Student'].': <span style="font-weight:bold;">'.$luser->StandardDisplayName.' '.$luser->ClassName.'-'.$luser->ClassNumber.'</span>';
			$x .= '</td>'."\n";
			//$x .= '<td style="width:2%">&nbsp;</td>'."\n";
			$x .= '<td colspan="2" style="text-align:right;width:50%;">'."\n";
				$x .= $Lang['LessonAttendance']['Period'].': <span style="font-weight:bold;">'.$StartDate.' '.$Lang['General']['To'].' '.$EndDate.'</span>';
			$x .= '</td>'."\n";
		$x .= '</tr>'."\n";
		
		$x .= '<tr>'."\n";
			$x .= '<td colspan="4" style="padding-top:16px;">'."\n";
				$x .= '<table class="common_table_list_v30">'."\n";
					$x .= '<tr>'."\n";
						$x .= '<th>'.$Lang['LessonAttendance']['WeekStart'].'</th>'."\n";
				for($i=1;$i<=5;$i++)
				{
					$x .= '<th style="text-align:center;">'.$Lang['LessonAttendance']['DisplayWeekday'][$i].'</th>'."\n";
				}
					$x .= '</tr>'."\n";
					
					for($i=0;$i<count($weeks);$i++)
					{
						$week_start = date("d M Y", $weeks[$i][2]);
						$x .= '<tr>'."\n";
							$x .= '<td width="10%" style="text-align:right">'.$week_start.'</td>'."\n";
							for($ts=$weeks[$i][2];$ts<$weeks[$i][3];$ts+=86400)
							{
								$x .= '<td width="18%">'."\n";
									$date = date("Y-m-d",$ts);
									$cell = '';
									if(isset($dateToRecords[$date])){
										
										for($k=0;$k<count($dateToRecords[$date]);$k++){
											$r = $dateToRecords[$date][$k]['Reason'];
											$status = $dateToRecords[$date][$k]['AttendStatus'];
											
											if($r != '' && isset($reasonToSymbolMap[$status]) && isset($reasonToSymbolMap[$status][$r])){
												$cell .= $reasonToSymbolMap[$status][$r].' ';
											}else {
												$cell .= '* ';
											}
										}
									}else{
										$x .= '&nbsp;';
									}
									$x .= $cell;
								$x .= '</td>'."\n";
							}
						$x .= '</tr>'."\n";
					}
					
				$x .= '</table>'."\n";
			$x .= '</td>'."\n";
		$x .= '</tr>'."\n";
		
		$x .= '<tr>'."\n";
			$x .= '<td colspan="4" style="padding-top:16px;">'."\n";
				$x .= '<div style="font-weight:bold;padding-bottom:8px;">'.$Lang['LessonAttendance']['Key'].'</div>'."\n";
				$x .= '<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">'."\n";
					for($i=0;$i<$reason_size;$i+=2)
					{
						$x .= '<tr>'."\n";
							$x .= '<td>'.$reasons[$i]['ReasonSymbol'].'</td><td>'.$reasons[$i]['ReasonText'].'</td>'."\n";
							if(isset($reasons[$i+1])){
								$x .= '<td>'.$reasons[$i+1]['ReasonSymbol'].'</td><td>'.$reasons[$i+1]['ReasonText'].'</td>'."\n";
							}else{
								$x .= '<td>&nbsp;</td><td>&nbsp;</td>'."\n";
							}
						$x .= '</tr>'."\n";
					}
				$x .= '</table>'."\n";
			$x .= '</td>'."\n";
		$x .= '</tr>'."\n";
		
	$x .= '</table>'."\n";

	$x .= '<br /><br />'."\n";
	if(in_array($Format,array('print'))) {
		$x .= '<div class="page_break"></div>';
	}

	return $x;
}

//debug_pr($dateToRecords);

if(in_array($Format,array("","web","print","pdf")))
{
	if(in_array($Format,array("","web")))
	{
	/*
	$x .= '<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">' . "\n";
		$x .= '<tr>' . "\n";
		$x .= '<td>' . "\n";
			$x .= '<div class="content_top_tool">' . "\n";
				$x .= '<div class="Conntent_tool">' . "\n";
				$x .= $linterface->GET_LNK_PRINT("javascript:submitForm('print');", "", "", "", "", 1);
				$x .= $linterface->GET_LNK_PRINT("javascript:submitForm('pdf');", $Lang['Btn']['Print']." PDF", "", "", "", 1);
				$x .= '</div>' . "\n";
				$x .= '<br style="clear: both;">' . "\n";
			$x .= '</div>' . "\n";
		$x .= '</td>' . "\n";
		$x .= '</tr>' . "\n";
	$x .= '</table>' . "\n";
	*/
		$MODULE_OBJ['title'] = $Lang['LessonAttendance']['StudentDailyRecords'];
		$linterface = new interface_html("popup.html");
	}
	
	if(in_array($Format,array("print","pdf"))){
		$x .= '<style type="text/css">'."\n";
		$x .= '@media print { .page_break {display:block; page-break-after: always !important; } }'."\n";
		$x .= '</style>'."\n";
	}
	
	if($Format == "print")
	{
	$x .= '<table width="100%" align="center" class="print_hide" border="0">
				<tr>
					<td align="right">'.$linterface->GET_BTN($Lang['Btn']['Print'], "button", "javascript:window.print();","printBtn").'</td>
				</tr>
			</table>'."\n";
	}
	
	if($Format != "pdf")
	{
		for($i=0;$i<count($TargetID);$i++){
			
			$x .= getReportContent($TargetID[$i]);
			
		}
	}
	/*
	$x .= '<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">' . "\n";
		$x .= '<tr style="background:#f3f3f3;">' . "\n";
			$x .= '<td colspan="2" style="width:50%;">' . "\n";
				$x .= $Lang['Identity']['Student'].': <span style="font-weight:bold;">'.$luser->StandardDisplayName.' '.$luser->ClassName.'-'.$luser->ClassNumber.'</span>';
			$x .= '</td>'."\n";
			//$x .= '<td style="width:2%">&nbsp;</td>'."\n";
			$x .= '<td colspan="2" style="text-align:right;width:50%;">'."\n";
				$x .= $Lang['LessonAttendance']['Period'].': <span style="font-weight:bold;">'.$StartDate.' '.$Lang['General']['To'].' '.$EndDate.'</span>';
			$x .= '</td>'."\n";
		$x .= '</tr>'."\n";
		
		$x .= '<tr>'."\n";
			$x .= '<td colspan="4" style="padding-top:16px;">'."\n";
				$x .= '<table class="common_table_list_v30">'."\n";
					$x .= '<tr>'."\n";
						$x .= '<th>'.$Lang['LessonAttendance']['WeekStart'].'</th>'."\n";
				for($i=1;$i<=5;$i++)
				{
					$x .= '<th style="text-align:center;">'.$Lang['LessonAttendance']['DisplayWeekday'][$i].'</th>'."\n";
				}
					$x .= '</tr>'."\n";
					
					for($i=0;$i<count($weeks);$i++)
					{
						$week_start = date("d M Y", $weeks[$i][2]);
						$x .= '<tr>'."\n";
							$x .= '<td width="10%" style="text-align:right">'.$week_start.'</td>'."\n";
							for($ts=$weeks[$i][2];$ts<$weeks[$i][3];$ts+=86400)
							{
								$x .= '<td width="18%">'."\n";
									$date = date("Y-m-d",$ts);
									$cell = '';
									if(isset($dateToRecords[$date])){
										
										for($k=0;$k<count($dateToRecords[$date]);$k++){
											$r = $dateToRecords[$date][$k]['Reason'];
											$status = $dateToRecords[$date][$k]['AttendStatus'];
											
											if($r != '' && isset($reasonToSymbolMap[$status]) && isset($reasonToSymbolMap[$status][$r])){
												$cell .= $reasonToSymbolMap[$status][$r].' ';
											}else {
												$cell .= '* ';
											}
										}
									}else{
										$x .= '&nbsp;';
									}
									$x .= $cell;
								$x .= '</td>'."\n";
							}
						$x .= '</tr>'."\n";
					}
					
				$x .= '</table>'."\n";
			$x .= '</td>'."\n";
		$x .= '</tr>'."\n";
		
		$x .= '<tr>'."\n";
			$x .= '<td colspan="4" style="padding-top:16px;">'."\n";
				$x .= '<div style="font-weight:bold;padding-bottom:8px;">'.$Lang['LessonAttendance']['Key'].'</div>'."\n";
				$x .= '<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">'."\n";
					for($i=0;$i<$reason_size;$i+=2)
					{
						$x .= '<tr>'."\n";
							$x .= '<td>'.$reasons[$i]['ReasonSymbol'].'</td><td>'.$reasons[$i]['ReasonText'].'</td>'."\n";
							if(isset($reasons[$i+1])){
								$x .= '<td>'.$reasons[$i+1]['ReasonSymbol'].'</td><td>'.$reasons[$i+1]['ReasonText'].'</td>'."\n";
							}else{
								$x .= '<td>&nbsp;</td><td>&nbsp;</td>'."\n";
							}
						$x .= '</tr>'."\n";
					}
				$x .= '</table>'."\n";
			$x .= '</td>'."\n";
		$x .= '</tr>'."\n";
		
	$x .= '</table>'."\n";
	*/
	if($Format == 'print')
	{
		include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_header.php");
		echo $x;
		include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_footer.php");
	}
	
	if(in_array($Format,array("","web")))
	{
		$linterface->LAYOUT_START();
		echo $x;
		$linterface->LAYOUT_STOP();
	}
	
	if($Format == 'pdf')
	{
		require_once($PATH_WRT_ROOT."includes/mpdf/mpdf.php");
		
		$css_file_path = $intranet_root.'/templates/2009a/css/content_30.css';
		$css_content = "<style type=\"text/css\">\n".file_get_contents($css_file_path)."</style>\n";
		$css_file_path = $intranet_root.'/templates/2009a/css/print_25.css';
		$css_content .= "<style type=\"text/css\">\n".file_get_contents($css_file_path)."</style>\n";
		
		//$x = $css_content.$x;
		
		$margin= 12.7; // mm
		$margin_top = $margin;
		$margin_bottom = $margin;
		$margin_left = $margin;
		$margin_right = $margin;
		$pdf = new mPDF('','A4',0,'',$margin_left,$margin_right,$margin_top,$margin_bottom,$margin_header,$margin_footer);
		
		//pdf metadata setting
		$pdf->SetTitle($report_title);
		$pdf->SetAuthor('BroadLearning Education (Asia) Ltd.');
		$pdf->SetCreator('BroadLearning Education (Asia) Ltd.');
		$pdf->SetSubject($report_title);
		$pdf->SetKeywords($report_title);
		
		// Chinese use mingliu 
		$pdf->backupSubsFont = array('mingliu');
		$pdf->useSubstitutions = true;
		
		$pdf->DefHTMLFooterByName('htmlFooter','<div style="text-align:center;">{PAGENO} / {nbpg}</div>');
		$pdf->SetHTMLFooterByName('htmlFooter');
		
		for($i=0;$i<count($TargetID);$i++){
			
			$x = $css_content.getReportContent($TargetID[$i]);
			
			$pos = 0;
			$len = mb_strlen($x);
			$perlen = 100000;
			for($p=$pos;$p<$len;$p+=$perlen)
			{
				$y = mb_substr($x, $p, $perlen);
				$pdf->WriteHTML($y);
			}
			
			$pdf->WriteHTML('<pagebreak resetpagenum="1" pagenumstyle="1" suppress="off" />');
		}

		//$pdf->Output('student_daily_records('.$luser->StandardDisplayName.' '.$luser->ClassName.'-'.$luser->ClassNumber.')('.$StartDate.' - '.$EndDate.').pdf', 'I');
		$pdf->Output('student_daily_records.pdf', 'I');
	}
}

intranet_closedb();
?>