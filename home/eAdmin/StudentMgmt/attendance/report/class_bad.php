<?
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();
$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageReport_ClassBadRecords";

$linterface = new interface_html();

$select_type = "<SELECT name=\"BadType\">\n";
$select_type .= "<OPTION value=\"".CARD_BADACTION_LUNCH_NOTINLIST."\" ".(($BadType == CARD_BADACTION_LUNCH_NOTINLIST)? "selected":"").">$i_StudentAttendance_BadLogs_Type_NotInLunchList</OPTION>\n";
$select_type .= "<OPTION value=\"".CARD_BADACTION_LUNCH_BACKALREADY."\" ".(($BadType == CARD_BADACTION_LUNCH_BACKALREADY)? 'selected':'').">$i_StudentAttendance_BadLogs_Type_GoLunchAgain</OPTION>\n";
$select_type .= "<OPTION value=\"".CARD_BADACTION_FAKED_CARD_AM."\" ".(($BadType == CARD_BADACTION_FAKED_CARD_AM)? 'selected':'').">$i_StudentAttendance_BadLogs_Type_FakedCardAM</OPTION>\n";
$select_type .= "<OPTION value=\"".CARD_BADACTION_FAKED_CARD_PM."\" ".(($BadType == CARD_BADACTION_FAKED_CARD_PM)? 'selected':'').">$i_StudentAttendance_BadLogs_Type_FakedCardPM</OPTION>\n";
$select_type .= "<OPTION value=\"".CARD_BADACTION_NO_CARD_ENTRANCE."\" ".(($BadType == CARD_BADACTION_NO_CARD_ENTRANCE)? 'selected':'').">$i_StudentAttendance_BadLogs_Type_NoCardRecord</OPTION>\n";
$select_type .= "</SELECT>\n";

$TAGS_OBJ[] = array($i_StudentAttendance_Report_ClassBadRecords, "", 0);

$MODULE_OBJ = $lc->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();
?>
<br />
<form name="form1" action="" method="GET">
<?php
if (isset($BadType) && isset($StartDate) && isset($EndDate)) {
	switch ($BadType)
	{
	    case CARD_BADACTION_LUNCH_NOTINLIST: $nav_title = $i_StudentAttendance_BadLogs_Type_NotInLunchList; break;
	    case CARD_BADACTION_LUNCH_BACKALREADY: $nav_title = $i_StudentAttendance_BadLogs_Type_GoLunchAgain; break;
	    case CARD_BADACTION_FAKED_CARD_AM: $nav_title = $i_StudentAttendance_BadLogs_Type_FakedCardAM; break;
	    case CARD_BADACTION_FAKED_CARD_PM: $nav_title = $i_StudentAttendance_BadLogs_Type_FakedCardPM; break;
	    case CARD_BADACTION_NO_CARD_ENTRANCE: $nav_title = $i_StudentAttendance_BadLogs_Type_NoCardRecord; break;
	}
	
	$i_title = "$nav_title ($StartDate to $EndDate)";
	$PAGE_NAVIGATION[] = array($i_title);
?>
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	</tr>
	<tr><td>&nbsp;</td></tr>
<table>
<?php } ?>
<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_general_Type?></td>
		<td width="70%"class="tabletext">
			<?=$select_type?>
		</td>
	</tr>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_general_startdate?></td>
		<td width="70%"class="tabletext">
			<?=$linterface->GET_DATE_PICKER("StartDate", ($StartDate != "")? $StartDate:date('Y-m-d'))?>
		</td>
	</tr>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_general_enddate?></td>
		<td width="70%"class="tabletext">
			<?=$linterface->GET_DATE_PICKER("EndDate", ($EndDate != "")? $EndDate:date('Y-m-d'))?>
		</td>
	</tr>
</table>
<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
    	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
    </tr>
</table>
<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
	    <td align="center" colspan="2">
			<?= $linterface->GET_ACTION_BTN($button_view, "submit", "") ?>
		</td>
	</tr>
</table>
</form>
<?php
if (isset($BadType) && isset($StartDate) && isset($EndDate)) {
	
	$lc->retrieveSettings();

	$ts = strtotime($StartDate);
	if ($ts==-1 || $StartDate =="")
	{
	    $StartDate = date('Y-m-d');
	}
	$ts = strtotime($EndDate);
	if ($ts==-1 || $EndDate =="")
	{
	    $EndDate = date('Y-m-d');
	}
	
	$result = $lc->retrieveBadRecordsCountByClass($StartDate, $EndDate, $BadType);
	
	$display = "<table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">";
	$display .= "<tr class=\"tablebluetop\">";
	$display .= "<td class=\"tabletoplink\">$i_SmartCard_ClassName</td>\n";
	$display .= "<td class=\"tabletoplink\">$i_StudentAttendance_ReportHeader_NumStudents</td>\n";
	$display .= "<td class=\"tabletoplink\">$i_StudentAttendance_ReportHeader_NumRecords</td>\n";
	$display .= "</tr>\n";
	
	for ($i=0; $i<sizeof($result); $i++)
	{
	     list ($class_name, $count_student, $count_record) = $result[$i];
	     $css = ($i%2==0)?"tablebluerow1":"tablebluerow2";
	     $display .= "<tr class=\"$css\">";
	     $display .= "<td class=\"tabletext\">$class_name</td>";
	     $display .= "<td class=\"tabletext\">$count_student</td>\n";
	     $display .= "<td class=\"tabletext\">$count_record</td></tr>\n";
	}
	$display .= "</table>\n";
	
	$toolbar .= $linterface->GET_LNK_PRINT("javascript:openPrintPage()","","","","",0);
?>
<script language="JavaScript" type="text/javascript">
function openPrintPage()
{
    newWindow("class_bad_print.php?BadType=<?=$BadType?>&StartDate=<?=$StartDate?>&EndDate=<?=$EndDate?>",10);
}
</script>
<table width="95%" border="0" cellpadding="0" cellspacing="0" align="center">
	<tr>
		<td align="left"><?= $toolbar ?></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
</table>
<?=$display?>
<?php } ?>
<br />
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>