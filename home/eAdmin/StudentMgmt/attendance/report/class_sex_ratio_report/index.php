<?
//editing by kenneth chung

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();
$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage['ClassSexRatioReport'] = 1;


$linterface = new interface_html();

$lclass= new libclass();

# date range
$TargetDate = ($TargetDate != "")? $TargetDate:date('Y-m-d');

# format
$select_format ="<SELECT name=\"Format\">\n";
$select_format .="<OPTION value=\"1\">Web</OPTION>\n";
$select_format.="<OPTION VALUE=\"2\">CSV</OPTION>\n";
$select_format.="</SELECT>\n";

# class list
$select_class = $lc->getSelectClass('name="class_name[]" id="class_name" multiple class="class_list" size="10" style="min-width:150px; width:200px;"',$class_name,1);

# session
$select_session ="<SELECT name=\"DayType\">\n";
if ($lc->attendance_mode != 1) 
	$select_session.="<OPTION value=\"".PROFILE_DAY_TYPE_AM."\">$i_StudentAttendance_Slot_AM</OPTION>\n";
if ($lc->attendance_mode != 0) 
	$select_session.="<OPTION value=\"".PROFILE_DAY_TYPE_PM."\">$i_StudentAttendance_Slot_PM</OPTION>\n";
$select_session.="</SELECT>\n";

$TAGS_OBJ[] = array($Lang['StudentAttendance']['ClassSexRatioReport'], "", 0);

$MODULE_OBJ = $lc->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();
?>
<style type="text/css">
.class_list{width:100px; height=150px;}
</style>
<script type="text/javascript">
<!--
function Check_Form() {
	var ClassName = Get_Selection_Value("class_name","Array");
	
	if (ClassName.length > 0) {
		document.getElementById('form1').submit();
		$('#ClassNameSelectWarningLayer').html('');
	}
	else {
		$('#ClassNameSelectWarningLayer').html('<?=$Lang['StudentAttendance']['PleaseSelectAtLeastOnClass']?>');
	}
}
// -->
</script>
<br />
<form name="form1" id="form1" action="class_sex_ratio_report.php" method="POST" target="_blank">
<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$Lang['General']['Date']?></td>
		<td width="70%"class="tabletext">
			<?=$linterface->GET_DATE_PICKER("TargetDate", $TargetDate)?>
		</td>
	</tr>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_Attendance_DayType?></td>
		<td width="70%"class="tabletext">
			<?=$select_session?>
		</td>
	</tr>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?="$button_select $i_ClassName"?></td>
		<td width="70%"class="tabletext">
			<?=$select_class?>
			<?=$linterface->GET_SMALL_BTN($Lang['Btn']['SelectAll'], "button", "Select_All_Options('class_name',true);")?>
			<span style="color:red" id="ClassNameSelectWarningLayer"></span>
		</td>
	</tr>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_general_Format?></td>
		<td width="70%"class="tabletext">
			<?=$select_format?>
		</td>
	</tr>
</table>
<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
    	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
    </tr>
</table>
<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
	    <td align="center" colspan="2">
			<?= $linterface->GET_ACTION_BTN($button_submit, "button", "Check_Form();") ?>
			<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2"," class='formbutton' onclick=\"document.getElementById('OptionsLayer').style.display='none';\" onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
		</td>
	</tr>
</table>
</form>
<br />
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>