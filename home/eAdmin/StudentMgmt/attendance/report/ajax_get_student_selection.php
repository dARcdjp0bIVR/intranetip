<?php
// Editing by 
/*
 * 2018-09-06 (Carlos): Added access right checking.
 */
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libstudentattendance_ui.php");

intranet_auth();
intranet_opendb();

$luser = new libuser($_SESSION['UserID']);
$is_teacher = $luser->isTeacherStaff();

if (!($_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-LessonAttendance"] || $is_teacher) || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	echo 'die';
	intranet_closedb();
	exit();
}

$ClassList = $_REQUEST['ClassList'];

$linterface = new interface_html();

$StudentAttendUI = new libstudentattendance_ui();

echo $StudentAttendUI->Get_Student_Selection_List($ClassList);

intranet_closedb();
?>