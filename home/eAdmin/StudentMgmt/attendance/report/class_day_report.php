<?
//using by 
/*
 * 2019-10-31 (Ray)   : Added date range
 * 2017-02-02 (Carlos): Added display options [Login ID], [Reason], [Teacher's remarks], [Office Remark].
 * 2013-11-06 (Carlos): Modified to show class or group attendance data
 */
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();
$lclass = new libclass();
$linterface = new interface_html();

function getAttendanceReasonsRemarks($targetType, $targetId, $targetDate)
{
	global $lc;
	
	$sql = "SELECT 
				b.StudentID,
				b.RecordDate,
				b.DayType,
				b.RecordType,
				b.Reason,
				b.OfficeRemark 
			FROM INTRANET_USER as a ";
	if($targetType == 'GROUP'){
		$sql .= " INNER JOIN INTRANET_USERGROUP as ug ON ug.UserID=a.UserID ";
	}		
	$sql.= " INNER JOIN CARD_STUDENT_PROFILE_RECORD_REASON as b ON b.StudentID=a.UserID 
			WHERE ".($targetType == 'GROUP'? " ug.GroupID='".$lc->Get_Safe_Sql_Query($targetId)."' " : " a.ClassName = '".$lc->Get_Safe_Sql_Query($targetId)."' ")." 
				AND a.RecordType = 2 AND a.RecordStatus IN (0,1,2) AND b.RecordDate='".$lc->Get_Safe_Sql_Query($targetDate)."' AND (b.RecordStatus IS NULL OR b.RecordStatus='0') 
           ORDER BY a.ClassName, a.ClassNumber+0, a.EnglishName";
	$reason_records = $lc->returnResultSet($sql);
	$reason_size = count($reason_records);
	
	$sql = "SELECT 
				b.StudentID,
				b.RecordDate,
				b.DayType,
				b.Remark 
			FROM INTRANET_USER as a ";
	if($targetType == 'GROUP'){
		$sql .= " INNER JOIN INTRANET_USERGROUP as ug ON ug.UserID=a.UserID ";
	}		
	$sql.= " INNER JOIN CARD_STUDENT_DAILY_REMARK as b ON b.StudentID=a.UserID 
			WHERE ".($targetType == 'GROUP'? " ug.GroupID='".$lc->Get_Safe_Sql_Query($targetId)."' " : " a.ClassName = '".$lc->Get_Safe_Sql_Query($targetId)."' ")." 
				AND a.RecordType = 2 AND a.RecordStatus IN (0,1,2) AND b.RecordDate='".$lc->Get_Safe_Sql_Query($targetDate)."' 
           ORDER BY a.ClassName, a.ClassNumber+0, a.EnglishName";
	$remark_records = $lc->returnResultSet($sql);
	$remark_size = count($remark_records);
	
	$assocAry = array();
	for($i=0;$i<$reason_size;$i++){
		$student_id = $reason_records[$i]['StudentID'];
		$record_date = $reason_records[$i]['RecordDate'];
		$day_type = $reason_records[$i]['DayType'];
		$record_type = $reason_records[$i]['RecordType'];
		$reason = $reason_records[$i]['Reason'];
		$office_remark = $reason_records[$i]['OfficeRemark'];
		if(!isset($assocAry[$student_id])){
			$assocAry[$student_id] = array();
		}
		if(!isset($assocAry[$student_id][$record_date])){
			$assocAry[$student_id][$record_date] = array();
		}
		if(!isset($assocAry[$student_id][$record_date][$day_type])){
			$assocAry[$student_id][$record_date][$day_type] = array();
		}
		if(!isset($assocAry[$student_id][$record_date][$day_type][$record_type])){
			$assocAry[$student_id][$record_date][$day_type][$record_type] = array();
		}
		
		$assocAry[$student_id][$record_date][$day_type][$record_type]['Reason'] = $reason;
		$assocAry[$student_id][$record_date][$day_type][$record_type]['OfficeRemark'] = $office_remark;
	}
	
	$assocAry2 = array();
	for($i=0;$i<$remark_size;$i++){
		$student_id = $remark_records[$i]['StudentID'];
		$record_date = $remark_records[$i]['RecordDate'];
		$day_type = $remark_records[$i]['DayType'];
		$remark = $remark_records[$i]['Remark'];
		if($remark == '') continue;
		if(!isset($assocAry2[$student_id])){
			$assocAry2[$student_id] = array();
		}
		if(!isset($assocAry2[$student_id][$record_date])){
			$assocAry2[$student_id][$record_date] = array();
		}
		if(!isset($assocAry2[$student_id][$record_date][$day_type])){
			$assocAry2[$student_id][$record_date][$day_type] = array();
		}
		
		$assocAry2[$student_id][$record_date][$day_type]['Remark'] = $remark;
	}
	
	return array($assocAry,$assocAry2);
}

if($ShowAllColumns == "0") # Data Columns are Optionally Shown
{
	$show_loginid = $ColumnLoginID == "1";
	$show_studentname = $ColumnStudentName == "1";
	$show_classno = $ColumnClassNo == "1";
	if($sys_custom['SmartCardAttendance_StudentAbsentSession'])
	{
		$show_session_info = $ColumnSession == "1";
	}
	$show_reason = $ColumnReason == "1";
	$show_remark = $ColumnRemark == "1";
	$show_office_remark = $ColumnOfficeRemark == "1";
}else # Default All Columns are shown
{
	$show_loginid = true;
	$show_studentname = true;
	$show_classno = true;
	if($sys_custom['SmartCardAttendance_StudentAbsentSession']) $show_session_info = true;
	$show_reason = true;
	$show_remark = true;
	$show_office_remark = true;
}

$TargetType = $_REQUEST['TargetType']; // CLASS | GROUP
$TargetID = $TargetType == 'CLASS' ? $ClassID : $GroupID;

if($format == "web")
{

    $ts = strtotime($TargetDate);
	if ($ts == -1 || $TargetDate == "") {
		$TargetDate = date('Y-m-d');
	}
	$ts = strtotime($StartDate);
	if ($ts == -1 || $StartDate == "") {
		$StartDate = date('Y-m-d');
	}

    $StartDate = $TargetDate;
	$datediff = strtotime($EndDate) - strtotime($StartDate);
	$NoOfDate = floor($datediff / (60 * 60 * 24));
	$display = "";

	for($_i=0;$_i<=$NoOfDate;$_i++) {
		$ts = strtotime($StartDate) + ($_i * 60 * 60 *24);
		$TargetDate = date("Y-m-d", $ts);

		if ($ts == -1 || $TargetDate == "") {
			$TargetDate = date('Y-m-d');
			$year = date('Y');
			$month = date('m');
			$day = date('d');
		} else {
			$year = date('Y', $ts);
			$month = date('m', $ts);
			$day = date('d', $ts);
		}

		for ($j = 0; $j < sizeof($TargetID); $j++) {

			if ($TargetType == 'GROUP') {
				$lgroup = new libgroup($TargetID[$j]);
				$TargetName = $lgroup->TitleDisplay;

			} else if ($TargetType == 'CLASS' && $TargetID[$j] != "") {
				$TargetName = $lclass->getClassName($TargetID[$j]);
			}
			$display .= "<table width=\"100%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">
			<tr>
				<td width=\"30%\" valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">" . ($TargetType == 'CLASS' ? $i_ClassName : $i_GroupName) . "</td>
				<td width=\"70%\" class=\"tabletext\">$TargetName</td>
			</tr>
			<tr>
				<td width=\"30%\" valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_StudentAttendance_View_Date</td>
				<td width=\"70%\" class=\"tabletext\">$TargetDate</td>
			</tr>
			</table><br />";

			$i_title = $i_StudentAttendance_Report_ClassDaily;

			$lc->retrieveSettings();

			if ($TargetType == 'CLASS') {
				$need_to_take_attendance = $lc->isRequiredToTakeAttendanceByDate($TargetName, $TargetDate);
			} else if ($TargetType == 'GROUP') {
				$need_to_take_attendance = $lc->groupIsRequiredToTakeAttendanceByDate($TargetID[$j], $TargetDate);
			}

			if ($need_to_take_attendance) {
				if ($TargetType == 'CLASS') {
					$result = $lc->retrieveClassDayData($TargetName, $year, $month, $day);
				} else if ($TargetType == 'GROUP') {
					$result = $lc->retrieveGroupDayData($TargetID[$j], $year, $month, $day);
				}
				if ($show_reason || $show_remark || $show_office_remark) {
					$reasons_remarks = getAttendanceReasonsRemarks($TargetType, $TargetType == 'GROUP' ? $TargetID[$j] : $TargetName, $TargetDate);
					$reason_records = $reasons_remarks[0];
					$remark_records = $reasons_remarks[1];
				}
				$space = $intranet_session_language == "en" ? " " : "";

				$display .= "<table width=\"100%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">";
				$display .= "<tr class=\"tablebluetop\">";
				if ($show_loginid) $display .= "<td class=\"tabletoplink\">" . $i_UserLogin . "</td>";
				if ($show_studentname) $display .= "<td class=\"tabletoplink\">$i_UserStudentName</td>";
				if ($show_classno) $display .= "<td class=\"tabletoplink\">$i_ClassNameNumber</td>";
				$display .= "<td class=\"tabletoplink\">$i_StudentAttendance_Field_InSchoolTime</td>";
				$display .= "<td class=\"tabletoplink\">$i_StudentAttendance_Field_CardStation</td>";

				if ($lc->attendance_mode == 0 || $lc->attendance_mode == 2 || $lc->attendance_mode == 3) {
					$display .= "<td class=\"tabletoplink\">$i_StudentAttendance_Slot_AM$space$i_SmartCard_Frontend_Take_Attendance_PreStatus</td>\n";
				} else {
					$display .= "<td class=\"tabletoplink\">$i_StudentAttendance_Slot_PM$space$i_SmartCard_Frontend_Take_Attendance_PreStatus</td>\n";
				}

				if ($sys_custom['SmartCardAttendance_StudentAbsentSession'] && $show_session_info) {
					$display .= "<td class=\"tabletoplink\">" . $Lang['StudentAttendance']['Late'] . "</td>";
					$display .= "<td class=\"tabletoplink\">" . $Lang['StudentAttendance']['RequestLeave'] . "</td>";
					$display .= "<td class=\"tabletoplink\">" . $Lang['StudentAttendance']['PlayTruant'] . "</td>";
					$display .= "<td class=\"tabletoplink\">" . $Lang['StudentAttendance']['OfficalLeave'] . "</td>";
				}

				// AM
				if ($lc->attendance_mode == 0 || $lc->attendance_mode == 2 || $lc->attendance_mode == 3) {
					if ($show_reason) {
						$display .= "<td class=\"tabletoplink\">" . $i_StudentAttendance_Slot_AM . $space . $i_Attendance_Reason . "</td>\n";
					}
					if ($show_remark) {
						$display .= "<td class=\"tabletoplink\">" . $i_StudentAttendance_Slot_AM . $space . $Lang['StudentAttendance']['iSmartCardRemark'] . "</td>\n";
					}
					if ($show_office_remark) {
						$display .= "<td class=\"tabletoplink\">" . $i_StudentAttendance_Slot_AM . $space . $Lang['StudentAttendance']['OfficeRemark'] . "</td>\n";
					}
				}

				if ($lc->attendance_mode == 2)      # With Lunch Out
				{
					$display .= "<td class=\"tabletoplink\">$i_StudentAttendance_Field_LunchOutTime</td>";
					$display .= "<td class=\"tabletoplink\">$i_StudentAttendance_Field_CardStation</td>\n";
					$display .= "<td class=\"tabletoplink\">$i_StudentAttendance_Field_LunchBackTime</td>";
					$display .= "<td class=\"tabletoplink\">$i_StudentAttendance_Field_CardStation</td>\n";
				}
				if ($lc->attendance_mode == 2 || $lc->attendance_mode == 3) {
					$display .= "<td class=\"tabletoplink\">$i_StudentAttendance_Slot_PM$space$i_SmartCard_Frontend_Take_Attendance_PreStatus</td>\n";
				}

				// PM
				if ($lc->attendance_mode == 1 || $lc->attendance_mode == 2 || $lc->attendance_mode == 3) {
					if ($show_reason) {
						$display .= "<td class=\"tabletoplink\">" . $i_StudentAttendance_Slot_PM . $space . $i_Attendance_Reason . "</td>\n";
					}
					if ($show_remark) {
						$display .= "<td class=\"tabletoplink\">" . $i_StudentAttendance_Slot_PM . $space . $Lang['StudentAttendance']['iSmartCardRemark'] . "</td>\n";
					}
					if ($show_office_remark) {
						$display .= "<td class=\"tabletoplink\">" . $i_StudentAttendance_Slot_PM . $space . $Lang['StudentAttendance']['OfficeRemark'] . "</td>\n";
					}
				}

				if ($sys_custom['SmartCardAttendance_StudentAbsentSession'] && $show_session_info) {
					$display .= "<td class=\"tabletoplink\">" . $Lang['StudentAttendance']['Late'] . "</td>";
					$display .= "<td class=\"tabletoplink\">" . $Lang['StudentAttendance']['RequestLeave'] . "</td>";
					$display .= "<td class=\"tabletoplink\">" . $Lang['StudentAttendance']['PlayTruant'] . "</td>";
					$display .= "<td class=\"tabletoplink\">" . $Lang['StudentAttendance']['OfficalLeave'] . "</td>";
				}

				$display .= "<td class=\"tabletoplink\">$i_StudentAttendance_Field_LeaveTime</td>";
				$display .= "<td class=\"tabletoplink\">$i_StudentAttendance_Field_CardStation</td>\n";
				$display .= "<td class=\"tabletoplink\">$i_StudentAttendance_Type_LeaveSchool$space$i_SmartCard_Frontend_Take_Attendance_PreStatus</td>\n";

				// Leave
				if ($show_reason) {
					$display .= "<td class=\"tabletoplink\">" . $i_StudentAttendance_Type_LeaveSchool . $space . $i_Attendance_Reason . "</td>\n";
				}
				if ($show_remark) {
					$display .= "<td class=\"tabletoplink\">" . $i_StudentAttendance_Type_LeaveSchool . $space . $Lang['StudentAttendance']['iSmartCardRemark'] . "</td>\n";
				}
				if ($show_office_remark) {
					$display .= "<td class=\"tabletoplink\">" . $i_StudentAttendance_Type_LeaveSchool . $space . $Lang['StudentAttendance']['OfficeRemark'] . "</td>\n";
				}

				$display .= "</tr>\n";

				for ($i = 0; $i < sizeof($result); $i++) {
					list($studentid, $student_name, $student_class, $classnum, $inTime, $inStation,
						$am, $lunchOutTime, $lunchOutStation, $lunchBackTime, $lunchBackStation,
						$pm, $leaveSchoolTime, $leaveSchoolStation, $leave,
						$amLateWaive, $pmLateWaive, $amAbsentWaive, $pmAbsentWaive, $amEarlyWaive, $pmEarlyWaive,
						$am_late_session, $am_request_leave_session, $am_play_truant_session, $am_offical_leave_session,
						$pm_late_session, $pm_request_leave_session, $pm_play_truant_session, $pm_offical_leave_session) = $result[$i];

					$am_late_session = $am_late_session == "" ? 0 : $am_late_session;
					$am_request_leave_session = $am_request_leave_session == "" ? 0 : $am_request_leave_session;
					$am_play_truant_session = $am_play_truant_session == "" ? 0 : $am_play_truant_session;
					$am_offical_leave_session = $am_offical_leave_session == "" ? 0 : $am_offical_leave_session;
					$pm_late_session = $pm_late_session == "" ? 0 : $pm_late_session;
					$pm_request_leave_session = $pm_request_leave_session == "" ? 0 : $pm_request_leave_session;
					$pm_play_truant_session = $pm_play_truant_session == "" ? 0 : $pm_play_truant_session;
					$pm_offical_leave_session = $pm_offical_leave_session == "" ? 0 : $pm_offical_leave_session;
					$raw_am = $am;
					if ($raw_am == CARD_STATUS_OUTING) $raw_am = CARD_STATUS_ABSENT;
					$raw_pm = $pm;
					if ($raw_pm == CARD_STATUS_OUTING) $raw_pm = CARD_STATUS_ABSENT;
					$raw_leave = $leave;
					switch ($am) {
						case "0" :
							$am = $i_StudentAttendance_Status_OnTime;
							break;
						case "1" :
							$am = $i_StudentAttendance_Status_Absent;
							break;
						case "2" :
							$am = $i_StudentAttendance_Status_Late;
							break;
						case "3" :
							$am = $i_StudentAttendance_Status_Outing;
							break;
						default :
							$am = $i_StudentAttendance_Status_Absent;
					}

					switch ($pm) {
						case "0" :
							$pm = $i_StudentAttendance_Status_OnTime;
							break;
						case "1" :
							$pm = $i_StudentAttendance_Status_Absent;
							break;
						case "2" :
							$pm = $i_StudentAttendance_Status_Late;
							break;
						case "3" :
							$pm = $i_StudentAttendance_Status_Outing;
							break;
						default :
							$pm = $i_StudentAttendance_Status_Absent;
					}
					switch ($leave) {
						case "0" :
							$leave = $Lang['StudentAttendance']['NormalLeave'];
							break;
						case "1":
							$leave = $i_StudentAttendance_Status_EarlyLeave;
							break;
						case "2":
							$leave = $i_StudentAttendance_Status_EarlyLeave;
							break;
					}

					$css = ($i % 2 == 0) ? "tablebluerow1" : "tablebluerow2";
					$display .= "<tr class=\"$css\">";
					if ($show_loginid) $display .= "<td class=\"tabletext\">" . $result[$i]['UserLogin'] . "</td>";
					if ($show_studentname) $display .= "<td class=\"tabletext\">$student_name</td>";
					if ($show_classno) $display .= "<td class=\"tabletext\">" . ($TargetType == 'CLASS' ? $classnum : $student_class . "/" . $classnum) . "</td>\n";

					$display .= "<td class=\"tabletext\">$inTime</td>";
					$display .= "<td class=\"tabletext\">$inStation</td>\n";
					if ($lc->attendance_mode == 0 || $lc->attendance_mode == 2 || $lc->attendance_mode == 3) {
						$display .= "<td class=\"tabletext\">$am</td>\n";
					} else {
						$display .= "<td class=\"tabletext\">$pm</td>\n";
					}

					if ($sys_custom['SmartCardAttendance_StudentAbsentSession'] && $show_session_info) {
						$display .= "<td class=\"tabletext\">" . $am_late_session . "</td>";
						$display .= "<td class=\"tabletext\">" . $am_request_leave_session . "</td>";
						$display .= "<td class=\"tabletext\">" . $am_play_truant_session . "</td>";
						$display .= "<td class=\"tabletext\">" . $am_offical_leave_session . "</td>";
					}

					// AM
					if ($lc->attendance_mode == 0 || $lc->attendance_mode == 2 || $lc->attendance_mode == 3) {
						if ($show_reason) {
							$display_reason = '';
							if (isset($reason_records[$studentid]) && isset($reason_records[$studentid][$TargetDate])
								&& isset($reason_records[$studentid][$TargetDate][PROFILE_DAY_TYPE_AM]) && isset($reason_records[$studentid][$TargetDate][PROFILE_DAY_TYPE_AM][$raw_am])) {
								$display_reason = $reason_records[$studentid][$TargetDate][PROFILE_DAY_TYPE_AM][$raw_am]['Reason'];
							}
							$display .= "<td class=\"tabletext\">" . $display_reason . "&nbsp;</td>\n";
						}
						if ($show_remark) {
							$display_remark = '';
							if (isset($remark_records[$studentid]) && isset($remark_records[$studentid][$TargetDate])
								&& isset($remark_records[$studentid][$TargetDate][PROFILE_DAY_TYPE_AM])) {
								$display_remark = $remark_records[$studentid][$TargetDate][PROFILE_DAY_TYPE_AM]['Remark'];
							}
							$display .= "<td class=\"tabletext\">" . $display_remark . "&nbsp;</td>\n";
						}
						if ($show_office_remark) {
							$display_office_remark = '';
							if (isset($reason_records[$studentid]) && isset($reason_records[$studentid][$TargetDate])
								&& isset($reason_records[$studentid][$TargetDate][PROFILE_DAY_TYPE_AM]) && isset($reason_records[$studentid][$TargetDate][PROFILE_DAY_TYPE_AM][$raw_am])) {
								$display_office_remark = $reason_records[$studentid][$TargetDate][PROFILE_DAY_TYPE_AM][$raw_am]['OfficeRemark'];
							}
							$display .= "<td class=\"tabletext\">" . $display_office_remark . "&nbsp;</td>\n";
						}
					}

					if ($lc->attendance_mode == 2)      # With Lunch Out
					{
						$display .= "<td class=\"tabletext\">$lunchOutTime</td>";
						$display .= "<td class=\"tabletext\">$lunchOutStation</td>\n";
						$display .= "<td class=\"tabletext\">$lunchBackTime</td>";
						$display .= "<td class=\"tabletext\">$lunchBackStation</td>\n";
					}
					if ($lc->attendance_mode == 2 || $lc->attendance_mode == 3) {
						$display .= "<td class=\"tabletext\">$pm</td>\n";
					}

					// PM
					if ($lc->attendance_mode == 1 || $lc->attendance_mode == 2 || $lc->attendance_mode == 3) {
						if ($show_reason) {
							$display_reason = '';
							if (isset($reason_records[$studentid]) && isset($reason_records[$studentid][$TargetDate])
								&& isset($reason_records[$studentid][$TargetDate][PROFILE_DAY_TYPE_PM]) && isset($reason_records[$studentid][$TargetDate][PROFILE_DAY_TYPE_PM][$raw_pm])) {
								$display_reason = $reason_records[$studentid][$TargetDate][PROFILE_DAY_TYPE_PM][$raw_pm]['Reason'];
							}
							$display .= "<td class=\"tabletext\">" . $display_reason . "&nbsp;</td>\n";
						}
						if ($show_remark) {
							$display_remark = '';
							if (isset($remark_records[$studentid]) && isset($remark_records[$studentid][$TargetDate])
								&& isset($remark_records[$studentid][$TargetDate][PROFILE_DAY_TYPE_PM])) {
								$display_remark = $remark_records[$studentid][$TargetDate][PROFILE_DAY_TYPE_PM]['Remark'];
							}
							$display .= "<td class=\"tabletext\">" . $display_remark . "&nbsp;</td>\n";
						}
						if ($show_office_remark) {
							$display_office_remark = '';
							if (isset($reason_records[$studentid]) && isset($reason_records[$studentid][$TargetDate])
								&& isset($reason_records[$studentid][$TargetDate][PROFILE_DAY_TYPE_PM]) && isset($reason_records[$studentid][$TargetDate][PROFILE_DAY_TYPE_PM][$raw_pm])) {
								$display_office_remark = $reason_records[$studentid][$TargetDate][PROFILE_DAY_TYPE_PM][$raw_pm]['OfficeRemark'];
							}
							$display .= "<td class=\"tabletext\">" . $display_office_remark . "&nbsp;</td>\n";
						}
					}

					if ($sys_custom['SmartCardAttendance_StudentAbsentSession'] && $show_session_info) {
						$display .= "<td class=\"tabletext\">&nbsp;" . $pm_late_session . "</td>";
						$display .= "<td class=\"tabletext\">&nbsp;" . $pm_request_leave_session . "</td>";
						$display .= "<td class=\"tabletext\">&nbsp;" . $pm_play_truant_session . "</td>";
						$display .= "<td class=\"tabletext\">&nbsp;" . $pm_offical_leave_session . "</td>";
					}

					$display .= "<td class=\"tabletext\">$leaveSchoolTime</td>";
					$display .= "<td class=\"tabletext\">$leaveSchoolStation</td>\n";
					$display .= "<td class=\"tabletext\">$leave</td>\n";

					// Leave
					if ($show_reason) {
						$display_reason = '';
						if ($raw_leave == CARD_LEAVE_AM && isset($reason_records[$studentid]) && isset($reason_records[$studentid][$TargetDate])
							&& isset($reason_records[$studentid][$TargetDate][PROFILE_DAY_TYPE_AM]) && isset($reason_records[$studentid][$TargetDate][PROFILE_DAY_TYPE_AM][PROFILE_TYPE_EARLY])) {
							$display_reason = $reason_records[$studentid][$TargetDate][PROFILE_DAY_TYPE_AM][PROFILE_TYPE_EARLY]['Reason'];
						}
						if ($raw_leave == CARD_LEAVE_PM && isset($reason_records[$studentid]) && isset($reason_records[$studentid][$TargetDate])
							&& isset($reason_records[$studentid][$TargetDate][PROFILE_DAY_TYPE_PM]) && isset($reason_records[$studentid][$TargetDate][PROFILE_DAY_TYPE_PM][PROFILE_TYPE_EARLY])) {
							$display_reason = $reason_records[$studentid][$TargetDate][PROFILE_DAY_TYPE_PM][PROFILE_TYPE_EARLY]['Reason'];
						}
						$display .= "<td class=\"tabletext\">" . $display_reason . "&nbsp;</td>\n";
					}
					if ($show_remark) {
						$display_remark = '';
						if ($raw_leave == CARD_LEAVE_AM && isset($remark_records[$studentid]) && isset($remark_records[$studentid][$TargetDate])
							&& isset($remark_records[$studentid][$TargetDate][PROFILE_DAY_TYPE_AM])) {
							$display_remark = $remark_records[$studentid][$TargetDate][PROFILE_DAY_TYPE_AM]['Remark'];
						}
						if ($raw_leave == CARD_LEAVE_PM && isset($remark_records[$studentid]) && isset($remark_records[$studentid][$TargetDate])
							&& isset($remark_records[$studentid][$TargetDate][PROFILE_DAY_TYPE_PM])) {
							$display_remark = $remark_records[$studentid][$TargetDate][PROFILE_DAY_TYPE_PM]['Remark'];
						}
						$display .= "<td class=\"tabletext\">" . $display_remark . "&nbsp;</td>\n";
					}
					if ($show_office_remark) {
						$display_office_remark = '';
						if ($raw_leave == CARD_LEAVE_AM && isset($reason_records[$studentid]) && isset($reason_records[$studentid][$TargetDate])
							&& isset($reason_records[$studentid][$TargetDate][PROFILE_DAY_TYPE_AM]) && isset($reason_records[$studentid][$TargetDate][PROFILE_DAY_TYPE_AM][PROFILE_TYPE_EARLY])) {
							$display_office_remark = $reason_records[$studentid][$TargetDate][PROFILE_DAY_TYPE_AM][PROFILE_TYPE_EARLY]['OfficeRemark'];
						}
						if ($raw_leave == CARD_LEAVE_PM && isset($reason_records[$studentid]) && isset($reason_records[$studentid][$TargetDate])
							&& isset($reason_records[$studentid][$TargetDate][PROFILE_DAY_TYPE_PM]) && isset($reason_records[$studentid][$TargetDate][PROFILE_DAY_TYPE_PM][PROFILE_TYPE_EARLY])) {
							$display_office_remark = $reason_records[$studentid][$TargetDate][PROFILE_DAY_TYPE_PM][PROFILE_TYPE_EARLY]['OfficeRemark'];
						}
						$display .= "<td class=\"tabletext\">" . $display_office_remark . "&nbsp;</td>\n";
					}

					$display .= "</tr>\n";
				}
				$display .= "</table>\n";
			} else {
				$display .= "<table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">";
				$display .= "<tr class=\"tablebluerow2\">";
				$display .= "<td class=\"tabletext\" align=\"center\">$i_StudentAttendance_ClassMode_NoNeedToTakeAttendance</td>";
				$display .= "</tr>";
				$display .= "</table>\n";
			}
			$display .= "<br /><br />";
		} # end of for loop
	}

$MODULE_OBJ['title'] = $i_title;
$linterface = new interface_html("popup.html");
$linterface->LAYOUT_START();
$toolbar = $linterface->GET_LNK_PRINT("javascript:openPrintPage()","","","","",0);
$ClassIDs = implode(",",IntegerSafe((array)$ClassID));
$GroupIDs = implode(",",IntegerSafe((array)$GroupID));
?>
<script language="JavaScript" type="text/javascript">
function openPrintPage()
{
<?php
$js = 'newWindow("class_day_print.php?TargetType='.urlencode($TargetType).'&TargetDate='.urlencode($StartDate).'&EndDate='.urlencode($EndDate);
if($ClassIDs != ''){
	$js .= '&ClassID='.$ClassIDs;
}
if($GroupIDs != ''){
	$js .= '&GroupID='.$GroupIDs;
}
foreach($_REQUEST as $key => $val){
	if(preg_match('/Column*/',$key)){
		$js .= '&'.$key.'='.urlencode($val);
	}
}
$js .= '",10);'."\n";
echo $js;
?>  
}
</script>
<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td><?= $toolbar ?></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
</table>
<?
echo $display;
$linterface->LAYOUT_STOP();
} # end of if($format == "web")...
else if($format == "csv")
{
	include_once($PATH_WRT_ROOT."includes/libexporttext.php");
	$lexporttext = new libexporttext();

	$ts = strtotime($TargetDate);
	if ($ts == -1 || $TargetDate == "") {
		$TargetDate = date('Y-m-d');
	}
	$ts = strtotime($StartDate);
	if ($ts == -1 || $StartDate == "") {
		$StartDate = date('Y-m-d');
	}

	$space = $intranet_session_language=="en"?" ":"";
	$CSVFileName = "Class_Daily_Attendance_Report_".$TargetDate."_".$EndDate.".CSV";
	
	/*
	$headerColumn = array($i_ClassName,$i_UserStudentName,$i_ClassNameNumber,$i_StudentAttendance_Field_InSchoolTime,$i_StudentAttendance_Field_CardStation,
						$i_StudentAttendance_Slot_AM.$space.$i_SmartCard_Frontend_Take_Attendance_PreStatus,$i_StudentAttendance_Field_LunchOutTime,
						$i_StudentAttendance_Field_CardStation,$i_StudentAttendance_Field_LunchBackTime,$i_StudentAttendance_Field_CardStation,
						$i_StudentAttendance_Slot_PM.$space.$i_SmartCard_Frontend_Take_Attendance_PreStatus,$i_StudentAttendance_Field_LeaveTime,
						$i_StudentAttendance_Field_CardStation,$i_StudentAttendance_Type_LeaveSchool.$space.$i_SmartCard_Frontend_Take_Attendance_PreStatus);
	*/
	$headerColumn = array();
	//if ($sys_custom["StudentAttendance"]['LateAbsentEarlyShowUserLogin'])
	if($show_loginid) $headerColumn[] = $i_UserLogin;
	$headerColumn[] = $TargetType=='CLASS'?$i_ClassName:$i_GroupName;
	if($show_studentname) $headerColumn[] = $i_UserStudentName;
	if($show_classno) $headerColumn[] = $i_ClassNameNumber;
	$headerColumn[] = $i_Attendance_Date;
	$headerColumn[] = $i_StudentAttendance_Field_InSchoolTime;
	$headerColumn[] = $i_StudentAttendance_Field_CardStation;
	$headerColumn[] = $i_StudentAttendance_Slot_AM.$space.$i_SmartCard_Frontend_Take_Attendance_PreStatus;
	if($sys_custom['SmartCardAttendance_StudentAbsentSession'] && $show_session_info)
	{
		$headerColumn[] = $Lang['StudentAttendance']['Late'];
		$headerColumn[] = $Lang['StudentAttendance']['RequestLeave'];
		$headerColumn[] = $Lang['StudentAttendance']['PlayTruant'];
		$headerColumn[] = $Lang['StudentAttendance']['OfficalLeave'];
	}
	
	// AM
	if($lc->attendance_mode == 0 || $lc->attendance_mode == 2 || $lc->attendance_mode==3)
	{
		if($show_reason){
			$headerColumn[] = $i_StudentAttendance_Slot_AM.$space.$i_Attendance_Reason;
		}
		if($show_remark){
			$headerColumn[] = $i_StudentAttendance_Slot_AM.$space.$Lang['StudentAttendance']['iSmartCardRemark'];
		}
		if($show_office_remark){
			$headerColumn[] = $i_StudentAttendance_Slot_AM.$space.$Lang['StudentAttendance']['OfficeRemark'];
		}
	}
	
	$headerColumn[] = $i_StudentAttendance_Field_LunchOutTime;
	$headerColumn[] = $i_StudentAttendance_Field_CardStation;
	$headerColumn[] = $i_StudentAttendance_Field_LunchBackTime;
	$headerColumn[] = $i_StudentAttendance_Field_CardStation;
	$headerColumn[] = $i_StudentAttendance_Slot_PM.$space.$i_SmartCard_Frontend_Take_Attendance_PreStatus;
	
	// PM
	if($lc->attendance_mode == 1 || $lc->attendance_mode == 2 || $lc->attendance_mode==3)
	{
		if($show_reason){
			$headerColumn[] = $i_StudentAttendance_Slot_PM.$space.$i_Attendance_Reason;
		}
		if($show_remark){
			$headerColumn[] = $i_StudentAttendance_Slot_PM.$space.$Lang['StudentAttendance']['iSmartCardRemark'];
		}
		if($show_office_remark){
			$headerColumn[] = $i_StudentAttendance_Slot_PM.$space.$Lang['StudentAttendance']['OfficeRemark'];
		}
	}
	
	if($sys_custom['SmartCardAttendance_StudentAbsentSession'] && $show_session_info)
	{
		$headerColumn[] = $Lang['StudentAttendance']['Late'];
		$headerColumn[] = $Lang['StudentAttendance']['RequestLeave'];
		$headerColumn[] = $Lang['StudentAttendance']['PlayTruant'];
		$headerColumn[] = $Lang['StudentAttendance']['OfficalLeave'];
	}
	$headerColumn[] = $i_StudentAttendance_Field_LeaveTime;
	$headerColumn[] = $i_StudentAttendance_Field_CardStation;
	$headerColumn[] = $i_StudentAttendance_Type_LeaveSchool.$space.$i_SmartCard_Frontend_Take_Attendance_PreStatus;
	
	// Leave
	if($show_reason){
		$headerColumn[] = $i_StudentAttendance_Type_LeaveSchool.$space.$i_Attendance_Reason;
	}
	if($show_remark){
		$headerColumn[] = $i_StudentAttendance_Type_LeaveSchool.$space.$Lang['StudentAttendance']['iSmartCardRemark'];
	}
	if($show_office_remark){
		$headerColumn[] = $i_StudentAttendance_Type_LeaveSchool.$space.$Lang['StudentAttendance']['OfficeRemark'];
	}
	
	$TargetID = $TargetType=='CLASS'?$ClassID : $GroupID;

	$exportContent = array();


	$StartDate = $TargetDate;
	$datediff = strtotime($EndDate) - strtotime($StartDate);
	$NoOfDate = floor($datediff / (60 * 60 * 24));
	$display = "";
	for($_i=0;$_i<=$NoOfDate;$_i++) {
		$ts = strtotime($StartDate) + ($_i * 60 * 60 * 24);
		$TargetDate = date("Y-m-d", $ts);

		if ($ts == -1 || $TargetDate == "") {
			$TargetDate = date('Y-m-d');
			$year = date('Y');
			$month = date('m');
			$day = date('d');
		} else {
			$year = date('Y', $ts);
			$month = date('m', $ts);
			$day = date('d', $ts);
		}

		for ($j = 0; $j < sizeof($TargetID); $j++) {
			if ($TargetType == 'GROUP') {
				$lgroup = new libgroup($TargetID[$j]);
				$TargetName = $lgroup->TitleDisplay;
			}
			if ($TargetType == 'CLASS' && $TargetID[$j] != "") {
				$TargetName = $lclass->getClassName($TargetID[$j]);
			}
			$lc->retrieveSettings();

			if ($TargetType == 'CLASS') {
				$need_to_take_attendance = $lc->isRequiredToTakeAttendanceByDate($TargetName, $TargetDate);
			} else {
				$need_to_take_attendance = $lc->groupIsRequiredToTakeAttendanceByDate($TargetID[$j], $TargetDate);
			}
			if ($need_to_take_attendance) {
				if ($TargetType == 'CLASS') {
					$result = $lc->retrieveClassDayData($TargetName, $year, $month, $day);
				} else if ($TargetType == 'GROUP') {
					$result = $lc->retrieveGroupDayData($TargetID[$j], $year, $month, $day);
				}
				if ($show_reason || $show_remark || $show_office_remark) {
					$reasons_remarks = getAttendanceReasonsRemarks($TargetType, $TargetType == 'GROUP' ? $TargetID[$j] : $TargetName, $TargetDate);
					$reason_records = $reasons_remarks[0];
					$remark_records = $reasons_remarks[1];
				}
				for ($i = 0; $i < sizeof($result); $i++) {
					$exportRow = array();

					list($studentid, $student_name, $student_class, $classnum, $inTime, $inStation,
						$am, $lunchOutTime, $lunchOutStation, $lunchBackTime, $lunchBackStation,
						$pm, $leaveSchoolTime, $leaveSchoolStation, $leave,
						$amLateWaive, $pmLateWaive, $amAbsentWaive, $pmAbsentWaive, $amEarlyWaive, $pmEarlyWaive,
						$am_late_session, $am_request_leave_session, $am_play_truant_session, $am_offical_leave_session,
						$pm_late_session, $pm_request_leave_session, $pm_play_truant_session, $pm_offical_leave_session, $UserLogin) = $result[$i];
					$raw_am = $am;
					if ($raw_am == CARD_STATUS_OUTING) $raw_am = CARD_STATUS_ABSENT;
					$raw_pm = $pm;
					if ($raw_pm == CARD_STATUS_OUTING) $raw_pm = CARD_STATUS_ABSENT;
					$raw_leave = $leave;
					switch ($am) {
						case "0" :
							$am = $i_StudentAttendance_Status_OnTime;
							break;
						case "1" :
							$am = $i_StudentAttendance_Status_Absent;
							break;
						case "2" :
							$am = $i_StudentAttendance_Status_Late;
							break;
						case "3" :
							$am = $i_StudentAttendance_Status_Outing;
							break;
						default :
							$am = $i_StudentAttendance_Status_Absent;
					}

					switch ($pm) {
						case "0" :
							$pm = $i_StudentAttendance_Status_OnTime;
							break;
						case "1" :
							$pm = $i_StudentAttendance_Status_Absent;
							break;
						case "2" :
							$pm = $i_StudentAttendance_Status_Late;
							break;
						case "3" :
							$pm = $i_StudentAttendance_Status_Outing;
							break;
						default :
							$pm = $i_StudentAttendance_Status_Absent;
					}
					switch ($leave) {
						case "0" :
							$leave = $i_StudentAttendance_Status_Present;
							break;
						case "1":
							$leave = $i_StudentAttendance_Status_EarlyLeave;
							break;
						case "2":
							$leave = $i_StudentAttendance_Status_EarlyLeave;
							break;
					}

					//if ($sys_custom["StudentAttendance"]['LateAbsentEarlyShowUserLogin']) $exportRow[] = $UserLogin;
					if ($show_loginid) $exportRow[] = $UserLogin;
					$exportRow[] = $TargetName;
					if ($show_studentname) $exportRow[] = $student_name;
					if ($show_classno) $exportRow[] = $TargetType == 'CLASS' ? $classnum : $student_class . "/$classnum";
					$exportRow[] = $TargetDate;
					$exportRow[] = $inTime;
					$exportRow[] = $inStation;

					if ($lc->attendance_mode == 0 || $lc->attendance_mode == 2 || $lc->attendance_mode == 3) {
						$exportRow[] = $am;
					} else {
						$exportRow[] = $pm;
					}

					if ($sys_custom['SmartCardAttendance_StudentAbsentSession'] && $show_session_info) {
						$exportRow[] = $am_late_session;
						$exportRow[] = $am_request_leave_session;
						$exportRow[] = $am_play_truant_session;
						$exportRow[] = $am_offical_leave_session;
					}

					// AM
					if ($lc->attendance_mode == 0 || $lc->attendance_mode == 2 || $lc->attendance_mode == 3) {
						if ($show_reason) {
							$display_reason = '';
							if (isset($reason_records[$studentid]) && isset($reason_records[$studentid][$TargetDate])
								&& isset($reason_records[$studentid][$TargetDate][PROFILE_DAY_TYPE_AM]) && isset($reason_records[$studentid][$TargetDate][PROFILE_DAY_TYPE_AM][$raw_am])) {
								$display_reason = $reason_records[$studentid][$TargetDate][PROFILE_DAY_TYPE_AM][$raw_am]['Reason'];
							}
							$exportRow[] = $display_reason;
						}
						if ($show_remark) {
							$display_remark = '';
							if (isset($remark_records[$studentid]) && isset($remark_records[$studentid][$TargetDate])
								&& isset($remark_records[$studentid][$TargetDate][PROFILE_DAY_TYPE_AM])) {
								$display_remark = $remark_records[$studentid][$TargetDate][PROFILE_DAY_TYPE_AM]['Remark'];
							}
							$exportRow[] = $display_remark;
						}
						if ($show_office_remark) {
							$display_office_remark = '';
							if (isset($reason_records[$studentid]) && isset($reason_records[$studentid][$TargetDate])
								&& isset($reason_records[$studentid][$TargetDate][PROFILE_DAY_TYPE_AM]) && isset($reason_records[$studentid][$TargetDate][PROFILE_DAY_TYPE_AM][$raw_am])) {
								$display_office_remark = $reason_records[$studentid][$TargetDate][PROFILE_DAY_TYPE_AM][$raw_am]['OfficeRemark'];
							}
							$exportRow[] = $display_office_remark;
						}
					}

					if ($lc->attendance_mode == 2)      # With Lunch Out
					{
						$exportRow[] = $lunchOutTime;
						$exportRow[] = $lunchOutStation;
						$exportRow[] = $lunchBackTime;
						$exportRow[] = $lunchBackStation;
					} else {
						$exportRow[] = "";
						$exportRow[] = "";
						$exportRow[] = "";
						$exportRow[] = "";
					}
					if ($lc->attendance_mode == 2 || $lc->attendance_mode == 3) {
						$exportRow[] = $pm;
					} else {
						$exportRow[] = "";
					}

					if ($sys_custom['SmartCardAttendance_StudentAbsentSession'] && $show_session_info) {
						$exportRow[] = $pm_late_session;
						$exportRow[] = $pm_request_leave_session;
						$exportRow[] = $pm_play_truant_session;
						$exportRow[] = $pm_offical_leave_session;
					}

					// PM
					if ($lc->attendance_mode == 1 || $lc->attendance_mode == 2 || $lc->attendance_mode == 3) {
						if ($show_reason) {
							$display_reason = '';
							if (isset($reason_records[$studentid]) && isset($reason_records[$studentid][$TargetDate])
								&& isset($reason_records[$studentid][$TargetDate][PROFILE_DAY_TYPE_PM]) && isset($reason_records[$studentid][$TargetDate][PROFILE_DAY_TYPE_PM][$raw_pm])) {
								$display_reason = $reason_records[$studentid][$TargetDate][PROFILE_DAY_TYPE_PM][$raw_pm]['Reason'];
							}
							$exportRow[] = $display_reason;
						}
						if ($show_remark) {
							$display_remark = '';
							if (isset($remark_records[$studentid]) && isset($remark_records[$studentid][$TargetDate])
								&& isset($remark_records[$studentid][$TargetDate][PROFILE_DAY_TYPE_PM])) {
								$display_remark = $remark_records[$studentid][$TargetDate][PROFILE_DAY_TYPE_PM]['Remark'];
							}
							$exportRow[] = $display_remark;
						}
						if ($show_office_remark) {
							$display_office_remark = '';
							if (isset($reason_records[$studentid]) && isset($reason_records[$studentid][$TargetDate])
								&& isset($reason_records[$studentid][$TargetDate][PROFILE_DAY_TYPE_PM]) && isset($reason_records[$studentid][$TargetDate][PROFILE_DAY_TYPE_PM][$raw_pm])) {
								$display_office_remark = $reason_records[$studentid][$TargetDate][PROFILE_DAY_TYPE_PM][$raw_pm]['OfficeRemark'];
							}
							$exportRow[] = $display_office_remark;
						}
					}

					$exportRow[] = $leaveSchoolTime;
					$exportRow[] = $leaveSchoolStation;
					$exportRow[] = $leave;

					// Leave
					if ($show_reason) {
						$display_reason = '';
						if ($raw_leave == CARD_LEAVE_AM && isset($reason_records[$studentid]) && isset($reason_records[$studentid][$TargetDate])
							&& isset($reason_records[$studentid][$TargetDate][PROFILE_DAY_TYPE_AM]) && isset($reason_records[$studentid][$TargetDate][PROFILE_DAY_TYPE_AM][PROFILE_TYPE_EARLY])) {
							$display_reason = $reason_records[$studentid][$TargetDate][PROFILE_DAY_TYPE_AM][PROFILE_TYPE_EARLY]['Reason'];
						}
						if ($raw_leave == CARD_LEAVE_PM && isset($reason_records[$studentid]) && isset($reason_records[$studentid][$TargetDate])
							&& isset($reason_records[$studentid][$TargetDate][PROFILE_DAY_TYPE_PM]) && isset($reason_records[$studentid][$TargetDate][PROFILE_DAY_TYPE_PM][PROFILE_TYPE_EARLY])) {
							$display_reason = $reason_records[$studentid][$TargetDate][PROFILE_DAY_TYPE_PM][PROFILE_TYPE_EARLY]['Reason'];
						}
						$exportRow[] = $display_reason;
					}
					if ($show_remark) {
						$display_remark = '';
						if ($raw_leave == CARD_LEAVE_AM && isset($remark_records[$studentid]) && isset($remark_records[$studentid][$TargetDate])
							&& isset($remark_records[$studentid][$TargetDate][PROFILE_DAY_TYPE_AM])) {
							$display_remark = $remark_records[$studentid][$TargetDate][PROFILE_DAY_TYPE_AM]['Remark'];
						}
						if ($raw_leave == CARD_LEAVE_PM && isset($remark_records[$studentid]) && isset($remark_records[$studentid][$TargetDate])
							&& isset($remark_records[$studentid][$TargetDate][PROFILE_DAY_TYPE_PM])) {
							$display_remark = $remark_records[$studentid][$TargetDate][PROFILE_DAY_TYPE_PM]['Remark'];
						}
						$exportRow[] = $display_remark;
					}
					if ($show_office_remark) {
						$display_office_remark = '';
						if ($raw_leave == CARD_LEAVE_AM && isset($reason_records[$studentid]) && isset($reason_records[$studentid][$TargetDate])
							&& isset($reason_records[$studentid][$TargetDate][PROFILE_DAY_TYPE_AM]) && isset($reason_records[$studentid][$TargetDate][PROFILE_DAY_TYPE_AM][PROFILE_TYPE_EARLY])) {
							$display_office_remark = $reason_records[$studentid][$TargetDate][PROFILE_DAY_TYPE_AM][PROFILE_TYPE_EARLY]['OfficeRemark'];
						}
						if ($raw_leave == CARD_LEAVE_PM && isset($reason_records[$studentid]) && isset($reason_records[$studentid][$TargetDate])
							&& isset($reason_records[$studentid][$TargetDate][PROFILE_DAY_TYPE_PM]) && isset($reason_records[$studentid][$TargetDate][PROFILE_DAY_TYPE_PM][PROFILE_TYPE_EARLY])) {
							$display_office_remark = $reason_records[$studentid][$TargetDate][PROFILE_DAY_TYPE_PM][PROFILE_TYPE_EARLY]['OfficeRemark'];
						}
						$exportRow[] = $display_office_remark;
					}

					$exportContent[] = $exportRow;
				}# end of for loop

			}#end of if(need_to_take_attendance)...

		}# end of for loop
	}
	$export_data = $lexporttext->GET_EXPORT_TXT($exportContent, $headerColumn);
	$lexporttext->EXPORT_FILE($CSVFileName, $export_data);
}#end of if($format == "csv")...

intranet_closedb();
?>