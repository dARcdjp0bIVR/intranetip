<?
// Using:

##################################
# 
#	Date:	2016-09-23	(Bill)	[2016-0704-1456-16054]
#			Create file
#			Copy logic from /includes/biba_cn.php
#
################################## 

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

// Access right
if (!$_SESSION["SSV_PRIVILEGE"]["plugin"]["attendancestudent"] || !$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$sys_custom["StudentAttendance"]["ReportStudentAttendanceInfo"]) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();
$lc->retrieveSettings();

// Setting: Count (0.5/1)
$absentCount = $lc->ProfileAttendCount==1? 0.5 : 1;

// POST data
$StartDate = $_POST["DateStart"];
$EndDate = $_POST["DateEnd"];
$ClassIDAry = $_POST["ClassID"];

// Get AcademicYearID
$thisYear = getAcademicYearAndYearTermByDate($StartDate);
$thisYearID = $thisYear["AcademicYearID"];

// Get Start date and End date
$thisYearPeriod = getPeriodOfAcademicYear($thisYearID);
$thisYearStartDate = substr($thisYearPeriod["StartDate"], 0, 10);
$thisYearEndDate = substr($thisYearPeriod["EndDate"], 0, 10);
list($start_year, $start_month, $start_date) = explode("-", $thisYearStartDate);
list($end_year, $end_month, $end_date) = explode("-", $thisYearEndDate);

// Build year and month array for getting data in CARD_STUDENT_DAILY_LOG_y_m
$month_year_Ary = array();

// loop year
while("$start_year" <= "$end_year")
{
	$month_year_Ary[$start_year] = array();
	
	// loop month
	$check_month = "$start_year"=="$end_year"? $end_month : "12";
	while("$start_year$start_month" <= "$start_year$check_month"){
		$month_year_Ary[$start_year][] = $start_month;
		
		// Update month
		$start_month++;
		$start_month = $start_month<10? "0$start_month" : $start_month;
		if(count($month_year_Ary[$start_year]) > 12)
			break;
	}
	
	// Update month and year
	$start_month = "01";
	$start_year++;
}

// Get Absent Reason Mapping
$sql = "Select CodeID, ReasonText From INTRANET_WEBSAMS_ATTENDANCE_REASON_CODE Where ReasonType = '".CARD_STATUS_ABSENT."'";
$absent_reason = $lc->returnArray($sql);
$absent_reason = BuildMultiKeyAssoc($absent_reason, "CodeID", "ReasonText");
$absent_reason_type = array();
$absent_reason_type[trim($absent_reason[3]["ReasonText"])] = "SickLeave";
$absent_reason_type[trim($absent_reason[1]["ReasonText"])] = "CasualLeave";
$absent_reason_type[trim($absent_reason[4]["ReasonText"])] = "CasualLeave";
$absent_reason_type[trim($absent_reason[5]["ReasonText"])] = "CasualLeave";
$absent_reason_type[trim($absent_reason[7]["ReasonText"])] = "CasualLeave";
$absent_reason_type[trim($absent_reason[9]["ReasonText"])] = "Disapproved";
$absent_reason_type[trim($absent_reason[2]["ReasonText"])] = "Truancy";
$absent_reason_type[trim($absent_reason[6]["ReasonText"])] = "Truancy";

// Get attendance result
$result = array();
$period_result = array();
if(count($month_year_Ary) > 0)
{
	// loop year
	foreach($month_year_Ary as $year => $month_Ary){
		// loop month
		foreach($month_Ary as $month)
		{
			// log table and fields
			$day_number_field = "IF(card_log.DayNumber < 10, CONCAT('0', card_log.DayNumber), card_log.DayNumber)";
			$card_log_table_name = "CARD_STUDENT_DAILY_LOG_".$year."_".$month;
			$card_log_date_field = "CONCAT('".$year."', '-', '".$month."', '-', $day_number_field)";
			
		  	$sql = "SELECT 
						iu.UserID,
						/*
						yc.ClassTitleEN as ClassName, 
						ycu.ClassNumber as ClassNumber, 
						iu.ChineseName as StudentName,
						$day_number_field as DayNumber, 
						*/
						$card_log_date_field as LogDate,
			            card_log.AMStatus,
						am_late.Reason as am_late_reason,
						am_late.RecordStatus as am_late_waive,
						am_absent.Reason as am_absent_reason,
						am_absent.RecordStatus as am_absent_waive,
						am_outing.Reason as am_outing_reason,
						am_outing.RecordStatus as am_outing_waive,
			            card_log.PMStatus,
						pm_late.Reason as pm_late_reason,
						pm_late.RecordStatus as pm_late_waive,
						pm_absent.Reason as pm_absent_reason,
						pm_absent.RecordStatus as pm_absent_waive,
						pm_outing.Reason as pm_outing_reason,
						pm_outing.RecordStatus as pm_outing_waive,
			            card_log.LeaveStatus
					FROM 
						YEAR_CLASS_USER as ycu 
						INNER JOIN 
							YEAR_CLASS as yc 
								ON (yc.YearClassID = ycu.YearClassID) 
						INNER JOIN 
							INTRANET_USER as iu
								ON (ycu.UserID = iu.UserID)
						INNER JOIN 
							$card_log_table_name as card_log 
								ON (card_log.UserID = iu.UserID) 
						LEFT JOIN 
							CARD_STUDENT_PROFILE_RECORD_REASON as am_late 
								ON (am_late.StudentID = card_log.UserID AND 
									am_late.RecordDate = $card_log_date_field AND 
									am_late.DayType = '".PROFILE_DAY_TYPE_AM."' AND am_late.RecordType = '".CARD_STATUS_LATE."') 
						LEFT JOIN 
							CARD_STUDENT_PROFILE_RECORD_REASON as pm_late 
								ON (pm_late.StudentID = card_log.UserID AND 
									pm_late.RecordDate = $card_log_date_field AND 
									pm_late.DayType = '".PROFILE_DAY_TYPE_PM."' AND pm_late.RecordType = '".CARD_STATUS_LATE."') 
						LEFT JOIN 
							CARD_STUDENT_PROFILE_RECORD_REASON as am_absent 
								ON (am_absent.StudentID = card_log.UserID AND 
									am_absent.RecordDate = $card_log_date_field AND 
									am_absent.DayType = '".PROFILE_DAY_TYPE_AM."' AND am_absent.RecordType = '".CARD_STATUS_ABSENT."') 
						LEFT JOIN 
							CARD_STUDENT_PROFILE_RECORD_REASON as pm_absent 
								ON (pm_absent.StudentID = card_log.UserID AND 
									pm_absent.RecordDate = $card_log_date_field AND 
									pm_absent.DayType = '".PROFILE_DAY_TYPE_PM."' AND pm_absent.RecordType = '".CARD_STATUS_ABSENT."') 
						LEFT JOIN 
							CARD_STUDENT_PROFILE_RECORD_REASON as am_outing 
								ON (am_outing.StudentID = card_log.UserID AND 
									am_outing.RecordDate = $card_log_date_field AND 
									am_outing.DayType = '".PROFILE_DAY_TYPE_AM."' AND am_outing.RecordType = '".CARD_STATUS_OUTING."') 
						LEFT JOIN 
							CARD_STUDENT_PROFILE_RECORD_REASON as pm_outing 
								ON (pm_outing.StudentID = card_log.UserID AND 
									pm_outing.RecordDate = $card_log_date_field AND 
									pm_outing.DayType = '".PROFILE_DAY_TYPE_PM."' AND pm_outing.RecordType = '".CARD_STATUS_OUTING."') 
					WHERE 
						yc.YearClassID IN ('".implode("', '", (array)$ClassIDAry)."') AND 
						iu.RecordType=2 AND iu.RecordStatus IN (0,1,2) AND 
						$card_log_date_field >= '".$thisYearStartDate."' AND $card_log_date_field <= '".$thisYearEndDate."' 
					ORDER BY 
						card_log.DayNumber, iu.ClassName, iu.ClassNumber";
			$log_data = $lc->returnArray($sql);
			
			// loop log records
			for($j=0; $j<count($log_data); $j++)
			{
				// Get log info
				$log_records = $log_data[$j];
				$thisUserID = $log_records['UserID'];
				$thisRecordDate = $log_records['LogDate'];
				
				// Late (AM)
				if($log_records["AMStatus"]==CARD_STATUS_LATE && $log_records["am_late_waive"]!=1){
					$result[$thisUserID]["Late"] += 1;
					
					if($thisRecordDate >= $StartDate && $thisRecordDate <= $EndDate){
						$period_result[$thisUserID]["Late"] += 1;
					}
				}
				// Late (PM)
				if($log_records["PMStatus"]==CARD_STATUS_LATE && $log_records["pm_late_waive"]!=1){
					$result[$thisUserID]["Late"] += 1;
					
					if($thisRecordDate >= $StartDate && $thisRecordDate <= $EndDate){
						$period_result[$thisUserID]["Late"] += 1;
					}
				}
				
				// Outing (AM)
				if($log_records["LeaveStatus"]==CARD_LEAVE_AM && $log_records["am_outing_waive"]!=1){
					$result[$thisUserID]["Outing"] += 1;
					
					if($thisRecordDate >= $StartDate && $thisRecordDate <= $EndDate){
						$period_result[$thisUserID]["Outing"] += 1;
					}
				}
				// Outing (PM)
				if($log_records["LeaveStatus"]==CARD_LEAVE_PM && $log_records["pm_outing_waive"]!=1){
					$result[$thisUserID]["Outing"] += 1;
					
					if($thisRecordDate >= $StartDate && $thisRecordDate <= $EndDate){
						$period_result[$thisUserID]["Outing"] += 1;
					}
				}
				
				// Absent (AM)
				if($log_records["AMStatus"]==CARD_STATUS_ABSENT && $log_records["am_absent_waive"]!=1){
					$result[$thisUserID]["Absent"] += $absentCount;
					
					if($thisRecordDate >= $StartDate && $thisRecordDate <= $EndDate){
						$period_result[$thisUserID]["Absent"] += $absentCount;
					}
					
					// Absent Type
					$thisAbsentType = $absent_reason_type[trim($log_records["am_absent_reason"])];
					if(trim($log_records["am_absent_reason"])!="" && $thisAbsentType != "") {
						$result[$thisUserID][$thisAbsentType] += $absentCount;
						
						if($thisRecordDate >= $StartDate && $thisRecordDate <= $EndDate){
							$period_result[$thisUserID][$thisAbsentType] += $absentCount;
						}
					}
				}
				// Absent (PM)
				if($log_records["PMStatus"]==CARD_STATUS_ABSENT && $log_records["pm_absent_waive"]!=1){
					$result[$thisUserID]["Absent"] += $absentCount;
					
					if($thisRecordDate >= $StartDate && $thisRecordDate <= $EndDate){
						$period_result[$thisUserID]["Absent"] += $absentCount;
					}
					
					// Absent Type
					$thisAbsentType = $absent_reason_type[trim($log_records["pm_absent_reason"])];
					if(trim($log_records["pm_absent_reason"]) && $thisAbsentType != "") {
						$result[$thisUserID][$thisAbsentType] += $absentCount;
						
						if($thisRecordDate >= $StartDate && $thisRecordDate <= $EndDate){
							$period_result[$thisUserID][$thisAbsentType] += $absentCount;
						}
					}
				}
			}
		}
	}
}

// CSV Title
$headerColumn = array();
$headerColumn[] = $Lang['StudentAttendance']['StudentAttendanceInfo']['Title']." ($StartDate - $EndDate)";

// CSV Header
$exportRow[] = "Class Name";
$exportRow[] = "Class No";
$exportRow[] = "Student Name";
$exportRow[] = "遲到(N)";
$exportRow[] = "遲到(TN)";
$exportRow[] = "早退(N)";
$exportRow[] = "早退(TN)";
$exportRow[] = "缺席(N)";
$exportRow[] = "缺席(TN)";
$exportRow[] = "病假(N)";
$exportRow[] = "病假(TN)";
$exportRow[] = "事假(N)";
$exportRow[] = "事假(TN)";
$exportRow[] = "不獲批核缺課(N)";
$exportRow[] = "不獲批核缺課(TN)";
$exportRow[] = "曠課(N)";
$exportRow[] = "曠課(TN)";

$exportContent = array();
$exportContent[] = $exportRow;

// Empty records
if(sizeof($result)==0)
{
	$exportRow = array();
	$exportRow[] = $i_no_record_exists_msg;
	$exportContent[] = $exportRow;
}
else
{
	// loop Classes
	foreach((array)$ClassIDAry as $thisClassID)
	{
		// Get Class students
		$ClassStudentList = $lc->Get_Student_List_By_Class_Academic_Year($thisClassID, $thisYearID);
		for($i=0; $i<count((array)$ClassStudentList); $i++)
		{
			// Get Student info
			$thisStudent = $ClassStudentList[$i];
			$thisUserID = $thisStudent["UserID"];
			
			// Student records
			$exportRow = array();
			$exportRow[] = $thisStudent["ClassName"];
			$exportRow[] = $thisStudent["ClassNumber"];
			$exportRow[] = $thisStudent["EnglishName"];
			$exportRow[] = $period_result[$thisUserID]["Late"];
			$exportRow[] = $result[$thisUserID]["Late"];
			$exportRow[] = $period_result[$thisUserID]["Outing"];
			$exportRow[] = $result[$thisUserID]["Outing"];
			$exportRow[] = $period_result[$thisUserID]["Absent"];
			$exportRow[] = $result[$thisUserID]["Absent"];
			$exportRow[] = $period_result[$thisUserID]["SickLeave"];
			$exportRow[] = $result[$thisUserID]["SickLeave"];
			$exportRow[] = $period_result[$thisUserID]["CasualLeave"];
			$exportRow[] = $result[$thisUserID]["CasualLeave"];
			$exportRow[] = $period_result[$thisUserID]["Disapproved"];
			$exportRow[] = $result[$thisUserID]["Disapproved"];
			$exportRow[] = $period_result[$thisUserID]["Truancy"];
			$exportRow[] = $result[$thisUserID]["Truancy"];
			
			$exportContent[] = $exportRow;
		}
	}
}

// Export file
$lexporttext = new libexporttext();
$CSVFileName = str_replace(" ", "_", $Lang['StudentAttendance']['StudentAttendanceInfo']['Title']).".csv";
$export_data = $lexporttext->GET_EXPORT_TXT($exportContent, $headerColumn);
$lexporttext->EXPORT_FILE($CSVFileName, $export_data);

intranet_closedb();
?>