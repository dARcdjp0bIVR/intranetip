<?php
// Using: 

##################################
# 
#	Date:	2016-09-23	(Bill)	[2016-0704-1456-16054]
#			Create file
#			Copy layout from /report/prove_document_report/index.php
#
##################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libstudentattendance_ui.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

// Access right
if (!$_SESSION["SSV_PRIVILEGE"]["plugin"]["attendancestudent"] || !$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$sys_custom["StudentAttendance"]["ReportStudentAttendanceInfo"]) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$linterface = new interface_html();
$lclass= new libclass();
$lc = new libstudentattendance_ui();

# Year
$AcademicYearID = $AcademicYearID? $AcademicYearID : Get_Current_Academic_Year_ID();
$year_list = $lc->Get_Academic_Year_List();
$select_year = $linterface->GET_SELECTION_BOX($year_list, 'name="AcademicYearID" id="AcademicYearID" onchange="this.form.target=\'\'; this.form.action=\'index.php\'; this.form.submit();"', "", $AcademicYearID);

# Class
$select_class = $lc->getSelectClassID('name="ClassID[]" id="ClassID" multiple class="class_list" size="10" style="min-width:150px; width:200px;"', "", 2, $AcademicYearID);

# Start / End Date
$start_date = date("Y-m-d", getStartOfAcademicYear());
$end_date = date("Y-m-d", getEndOfAcademicYear());

// Page Tag
$CurrentPageArr["StudentAttendance"] = 1;
$CurrentPage = "PageReport_StudentAttendanceInfo";

$TAGS_OBJ[] = array($Lang['StudentAttendance']['StudentAttendanceInfo']['Title'], "", 0);
$MODULE_OBJ = $lc->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();
?>

<script>
function checkform(){
	var error_no = 0;

	if (compareDate($('#DateStart').val(), $('#DateEnd').val()) > 0){
		$('#div_DateEnd_err_msg').html('<?=$i_Homework_new_duedate_wrong?>');
		error_no ++;
	}
	
	if ( $("#DPWL-DateStart").html() != '' || $("#DPWL-DateEnd").html() != ''){
		error_no ++;
	}
	
	if ( $('select#ClassID').val() == null){
		$('div#ClassWarning').show();
		error_no ++;
	}
		
	if (error_no == 0){
		$('#form1').submit();
		return true;
	}
	else{
		return false;
	}
}
</script>

<form name="form1" id="form1" method="post" action="export.php" target="_blank">
<br>
<table class="form_table_v30">
<tr valign='top'>
	<td width='30%' nowrap='nowrap' class='formfieldtitle tabletext'><?=$Lang['StudentAttendance']['ClassMemberAsInAcademicYear']?></td>
	<td width='70%' class='tabletext'>
		<?=$select_year?>
	</td>
</tr>

<tr valign='top'>
	<td class='formfieldtitle tabletext'><?=$iDiscipline['Period']?></td>
	<td>
		<?=$iDiscipline['Period_Start']?> <?=$linterface->GET_DATE_PICKER("DateStart", $start_date)?>
		<?=$iDiscipline['Period_End']?> <?=$linterface->GET_DATE_PICKER("DateEnd", $end_date)?>
		<br><span id='div_DateEnd_err_msg'></span>
	</td>
</tr>

<tr valign='top'>
	<td valign='top' nowrap='nowrap' class='formfieldtitle tabletext'><label for='ClassID[]'><?=$button_select.' '.$Lang['StaffAttendance']['Target']?></label></td>
	<td class='tabletext'>
		<?=$select_class?>
		&nbsp;<?=$linterface->GET_SMALL_BTN($Lang['Btn']['SelectAll'], "button", "Select_All_Options('ClassID', true); return false;")?>
		<div class='tabletextremark'><?=$Lang['SysMgr']['FormClassMapping']['CtrlMultiSelectMessage']?></div>
		<?=$linterface->Get_Form_Warning_Msg("ClassWarning", $Lang['StudentAttendance']['PleaseSelectAtLeastOnClass'], "ClassWarning")?>
	</td>
</tr>
</table>
<br>
<div class='edit_bottom_v30'>
	<?= $linterface->GET_ACTION_BTN($button_submit, "button", "checkform()" , "submitbtn") ?>
	<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='index.php'","cancelbtn") ?>
</div>
</form>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>