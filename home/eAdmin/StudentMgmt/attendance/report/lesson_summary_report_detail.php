<?php

// Modifying by 

/*
 * Change Log:
 * 2020-09-28 (Ray): add reason
 * 2016-06-16 (Carlos): Modified query to only get consolidated lesson attendance records. 
 * 2013-08-07 (Henry):File Created
 */

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

# preserve table view
if ($ck_attend_daily_absence_browse_student_page_number!=$pageNo && $pageNo!="")
{
	setcookie("ck_attend_daily_absence_browse_student_page_number", $pageNo, 0, "", "", 0);
	$ck_attend_daily_absence_browse_student_page_number = $pageNo;
} else if (!isset($pageNo) && $ck_attend_daily_absence_browse_student_page_number!="")
{
	$pageNo = $ck_attend_daily_absence_browse_student_page_number;
}

if ($ck_attend_daily_absence_browse_student_page_order!=$order && $order!="")
{
	setcookie("ck_attend_daily_absence_browse_student_page_order", $order, 0, "", "", 0);
	$ck_attend_daily_absence_browse_student_page_order = $order;
} else if (!isset($order) && $ck_attend_daily_absence_browse_student_page_order!="")
{
	$order = $ck_attend_daily_absence_browse_student_page_order;
}

if ($ck_attend_daily_absence_browse_student_page_field!=$field && $field!="")
{
	setcookie("ck_attend_daily_absence_browse_student_page_field", $field, 0, "", "", 0);
	$ck_attend_daily_absence_browse_student_page_field = $field;
} else if (!isset($field) && $ck_attend_daily_absence_browse_student_page_field!="")
{
	$field = $ck_attend_daily_absence_browse_student_page_field;
}

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libstudentattendance_ui.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");


intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-LessonAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancelesson']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();
$LessonAttendUI = new libstudentattendance_ui();
//$CurrentPageArr['StudentAttendance'] = 1;
//$CurrentPage = "PageDailyOperation_PresetAbsence";
$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageLessonAttendance_LessonAttendSummary";

$linterface = new interface_html();

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

if(!$FromDate)
	$FromDate = date('Y-m-d');
if(!$ToDate)
	$ToDate = date('Y-m-d');

$filter = $filter==""?1:$filter;
$today = date('Y-m-d');
$cond_filter ="";
switch($filter){
	case 4: $cond_filter = " and sgsa1.AttendStatus = 1 "; break;
	case 2: $cond_filter = " and sgsa1.AttendStatus = 2 "; break;
	case 3: $cond_filter = " and sgsa1.AttendStatus = 3 "; break;
	case 1: $cond_filter = " and (sgsa1.AttendStatus = 2 or sgsa1.AttendStatus = 3) "; break;
	default: $cond_filter =" and (sgsa1.AttendStatus = 2 or sgsa1.AttendStatus = 3) ";
}

$order = $order == "" ? 1 : $order;
if ($field == "") $field = 0;

$lclass = new libclass();
//change to select group
//$select_class = $lclass->getSelectClass("name='class' onChange='changeClass()'",$class,0);
$select_class = $lclass->getSelectClassWithWholeForm("name='class' onChange='changeClass()'", $class, "-- ". $button_select." --","","","","",1);

if($class!="" && $class!='0'){
	
	//New Modified [Start]
	if (strpos($class,'::') !== false) {
	    $classID = str_replace("::","",$class);
		$class = $lclass->getClassName($classID);
		$tempClassName = '"'.$class.'"';
	}
	else{
		$AcademicYearID = Get_Current_Academic_Year_ID();
		$sql="SELECT YearClassID FROM YEAR_CLASS WHERE AcademicYearID = '$AcademicYearID' AND YearID='$class'";
		//debug_pr($sql);
		$temp3 = $lclass->returnArray($sql,3);
		//debug_pr($temp);
		$class = '';
		$tempClassName = '"';
		for($i=0;  $i<count($temp3);$i++){
			$classID = $temp3[$i]["YearClassID"];
			//debug_pr($lclass->getClassName($classID));
			if($lclass->getClassName($classID) != NULL){
				if($i>0){
					$class .=', ';
					$tempClassName .='","';
				}
				//debug_pr($lclass->getClassName($classID));
				$class .= $lclass->getClassName($classID);
				$tempClassName .= $lclass->getClassName($classID);
			}
		}
		$tempClassName .= '"';
		//debug_pr($tempClassName);
		//debug_pr($temp3);
	}

	//New Modified [End]
	//debug_pr($class);
	$namefield = getNameFieldWithClassNumberByLang("");
	//$sql ="SELECT UserID,$namefield FROM INTRANET_USER WHERE RecordType=2 AND RecordStatus IN (0,1,2) AND ClassName='$class' ORDER BY ClassName,ClassNumber";
	$sql ="SELECT UserID,$namefield FROM INTRANET_USER WHERE RecordType=2 AND RecordStatus IN (0,1,2) AND ClassName IN ($tempClassName) ORDER BY ClassName,ClassNumber";
	$temp = $lclass->returnArray($sql,2);
	//$select_student=getSelectByArray($temp,"name='studentID' id='studentID'",$StudentList,0,0);
	
	$tempClass = explode(", ", $class);
	//debug_pr($StudentList);
	$select_student = $LessonAttendUI->Get_Student_Selection_List($tempClass, true, ($StudentList?false:true), ($StudentList?$StudentList:array()));
	//$select_student = $LessonAttendUI->Get_Student_Selection_List($tempClass, true, true);
}

if($StudentList!=""){
	//$namefield = getNameFieldWithClassNumberByLang("b.");
	
//	$sql=" SELECT CONCAT('<a class=\'tablelink\' href=\'edit.php?RecordID=',a.RecordID,'\'>',a.RecordDate,'</a>'),
//				 IF(a.DayPeriod=2,'$i_DayTypeAM','$i_DayTypePM'),
//				 a.Reason,
//				 IF(a.Waive = '1' AND a.Waive IS NOT NULL,'".$Lang['General']['Yes']."','".$Lang['General']['No']."') as Waive,
//				 a.Remark,
//				 CONCAT('<input type=''checkbox'' name=''RecordID[]'' value=', a.RecordID ,'>')
//				FROM CARD_STUDENT_PRESET_LEAVE AS a WHERE a.StudentID='$StudentList'  $cond_filter AND 
//				(a.RecordDate LIKE '%$keyword%' OR a.Reason LIKE '%$keyword%' OR a.Remark LIKE '%$keyword%' )
//				";
	//new sql statemant [Start]
	$cond ='';
	  	if($StudentList != ''){
	  		$StudentListStr = implode("','",$StudentList);
	  		//debug_pr($StudentListStr);
	  	//	$cond = " AND a1.StudentID IN ('".$StudentListStr."') ";
	  		$StudentList1 = $StudentList;
	  	}
	  	$NameField = getNameFieldWithClassNumberByLang('u.');
		$NameField1 = getNameFieldWithClassNumberByLang('u1.');
	  	$TermInfo = getAcademicYearInfoAndTermInfoByDate(date('Y-m-d'));
		list($AcademicYearID,$AcademicYearName,$YearTermID,$YearTermName) = $TermInfo;
	  	//debug_pr($AcademicYearID." ".$StudentList);
	  	/*
	  	$sql = 'select distinct
                a1.StudentID
              from 
                SUBJECT_GROUP_ATTEND_OVERVIEW_'.$AcademicYearID.' as o1
                inner join
                SUBJECT_GROUP_STUDENT_ATTENDANCE_'.$AcademicYearID.' as a1
                on o1.AttendOverviewID = a1.AttendOverviewID '.$cond;
                
//                o1.LessonDate = \''.$TargetDate.'\''.$cond.'
//                and o1.AttendOverviewID = a1.AttendOverviewID
//                and a1.AttendStatus in ('.implode(',',$AttendStatus).')';
      $StudentList1 = $LessonAttendUI->returnVector($sql);
      */
      //debug_pr($sql);
      $ClassTitle = Get_Lang_Selection("stc.ClassTitleB5","stc.ClassTitleEN");
      $SubjectName = Get_Lang_Selection("s.CH_DES", "s.EN_DES");
      
      if (sizeof($StudentList1) <= 0){
      	$StudentList1 = array(-1);
      }
      	
      if (sizeof($StudentList1) > 0) {
		  	$StudenCondition = implode("','",$StudentList1);
		  		$NameField3 = getNameFieldByLang('u.');
		  		//debug_pr($NameField);
		  		/*
				$sql = "select DISTINCT ".$NameField3." as StudentName1,
						IF(u.ClassNumber IS NULL OR u.ClassNumber = '','".$Lang['General']['EmptySymbol']."',u.ClassName) as ClassName1,
								IF(u.ClassNumber IS NULL OR u.ClassNumber = '','".$Lang['General']['EmptySymbol']."',u.ClassNumber) as ClassNumber1,
								  IF(sgao1.LessonDate IS NULL, '', sgao1.LessonDate) as LessonDate1,
								  CONCAT(itt.TimeSlotName,' (',itt.StartTime,' - ',itt.EndTime,')') as DayPeriod1,
								  CONCAT($SubjectName,' - ',$ClassTitle) as SubjectGroupTitle,
								  (IF(sgsa1.AttendStatus = 2, '<span style=\"color:#FF9900\"><b>".$Lang['LessonAttendance']['Late']."</b></span>', IF(sgsa1.AttendStatus = 3, '<span style=\"color:red\"><b>".$Lang['LessonAttendance']['Absent']."</b></span>', IF(sgsa1.AttendStatus = 1, '<span style=\"color:#0066FF\"><b>".$Lang['LessonAttendance']['Outing']."</b></span>', '')))) as AttendStatus1
								from
								  INTRANET_CYCLE_GENERATION_PERIOD as icgp
									inner join
									INTRANET_PERIOD_TIMETABLE_RELATION as iptr
									on
									  
										icgp.PeriodID = iptr.PeriodID
								  inner join
								  INTRANET_TIMETABLE_ROOM_ALLOCATION as itra
								  on
								    itra.TimeTableID = iptr.TimeTableID
								   
								  inner join
								  SUBJECT_TERM_CLASS as stc
								  on stc.SubjectGroupID = itra.SubjectGroupID
								  inner join 
								  SUBJECT_TERM as st
								  on st.SubjectGroupID = stc.SubjectGroupID and st.YearTermID = '$YearTermID' 
								  inner join 
								  ASSESSMENT_SUBJECT as s 
								  on st.SubjectID = s.RecordID 
								  inner join
								  SUBJECT_GROUP_ATTEND_OVERVIEW_$AcademicYearID as sgao1
								  on
								    sgao1.SubjectGroupID = itra.SubjectGroupID
								    and sgao1.LessonDate between '$FromDate' and '$ToDate'
								    and sgao1.RoomAllocationID = itra.RoomAllocationID
								  inner join
								  SUBJECT_GROUP_STUDENT_ATTENDANCE_$AcademicYearID as sgsa1
								  on
								    sgsa1.AttendOverviewID = sgao1.AttendOverviewID
								    and
								    sgsa1.StudentID in ('$StudenCondition')
								  inner JOIN
								  INTRANET_USER as u
								  on u.UserID = sgsa1.StudentID
								  inner JOIN 
								  SUBJECT_TERM_CLASS_USER as stcu
								  on 
								  	stcu.SubjectGroupID = stc.SubjectGroupID
								  	and 
								  	stcu.UserID in ('$StudenCondition')								  	
								  inner JOIN 
								  INTRANET_USER as u1 
								  on u1.UserID = stcu.UserID
								  inner join
								  INTRANET_TIMETABLE_TIMESLOT as itt
								  on itra.TimeSlotID = itt.TimeSlotID
								where 
									 true $cond_filter
											AND 
				(sgao1.LessonDate LIKE '%$keyword%' OR $SubjectName LIKE '%$keyword%' OR $ClassTitle LIKE '%$keyword%' )";
//								order by 
//									IF(u.UserID IS NULL, u1.ClassName, u.ClassName), IF(u.UserID IS NULL, u1.ClassNumber+0, u.ClassNumber+0)";

			*/
			$sql = "select DISTINCT ".$NameField3." as StudentName1,
						IF(u.ClassNumber IS NULL OR u.ClassNumber = '','".$Lang['General']['EmptySymbol']."',u.ClassName) as ClassName1,
								IF(u.ClassNumber IS NULL OR u.ClassNumber = '','".$Lang['General']['EmptySymbol']."',u.ClassNumber) as ClassNumber1,
								  IF(sgao1.LessonDate IS NULL, '', sgao1.LessonDate) as LessonDate1,
								  CONCAT(itt.TimeSlotName,' (',itt.StartTime,' - ',itt.EndTime,')') as DayPeriod1,
								  CONCAT($SubjectName,' - ',$ClassTitle) as SubjectGroupTitle,
								  (IF(sgsa1.AttendStatus = 2, '<span style=\"color:#FF9900\"><b>".$Lang['LessonAttendance']['Late']."</b></span>', IF(sgsa1.AttendStatus = 3, '<span style=\"color:red\"><b>".$Lang['LessonAttendance']['Absent']."</b></span>', IF(sgsa1.AttendStatus = 1, '<span style=\"color:#0066FF\"><b>".$Lang['LessonAttendance']['Outing']."</b></span>', '')))) as AttendStatus1,
                                  sgsa1.Reason  								
								from 
								SUBJECT_GROUP_ATTEND_OVERVIEW_$AcademicYearID as sgao1 
								inner join SUBJECT_GROUP_STUDENT_ATTENDANCE_$AcademicYearID as sgsa1 
								  on sgsa1.AttendOverviewID = sgao1.AttendOverviewID 
								INNER JOIN INTRANET_USER as u ON u.UserID=sgsa1.StudentID 
								INNER JOIN SUBJECT_TERM_CLASS as stc ON sgao1.SubjectGroupID=stc.SubjectGroupID 
								INNER JOIN SUBJECT_TERM as st ON st.SubjectGroupID=stc.SubjectGroupID 
								INNER JOIN ASSESSMENT_SUBJECT as s ON st.SubjectID=s.RecordID 
								LEFT JOIN INTRANET_TIMETABLE_ROOM_ALLOCATION as itra ON itra.RoomAllocationID=sgao1.RoomAllocationID 
								LEFT JOIN INTRANET_TIMETABLE_TIMESLOT as itt ON itt.TimeSlotID=itra.TimeSlotID 
							where (sgao1.LessonDate between '$FromDate' and '$ToDate') and sgsa1.StudentID in ('$StudenCondition') $cond_filter
								AND (sgao1.LessonDate LIKE '%$keyword%' OR $SubjectName LIKE '%$keyword%' OR $ClassTitle LIKE '%$keyword%' ) ";
			
      //debug_pr($sql);
      }
	//new sql statement [End]
	$li = new libdbtable2007($field, $order, $pageNo);
	$li->field_array = array("StudentName1","ClassName1","ClassNumber1","LessonDate1","DayPeriod1","SubjectGroupTitle","AttendStatus1","Reason");
	$li->sql = $sql;
	$li->no_col = sizeof($li->field_array)+1;
	$li->title = "";
	$li->column_array = array(0,0,0,0,0,0,0);
	$li->wrap_array = array(0,0,0,0,0,0,0);
	$li->IsColOff = 2;
	//allow the use of DISTINCT in the sql and get the correct number of row
	$li->count_mode = 1;
	
	// TABLE COLUMN
	$pos = 0;
	$li->column_list .= "<td width='1' class='tabletoplink'>#</td>\n";
	$li->column_list .= "<td width='20%'>".$li->column($pos++, $Lang['StudentAttendance']['StudentName'])."</td>\n";
	$li->column_list .= "<td width='5%'>".$li->column($pos++, $i_ClassName)."</td>\n";
	$li->column_list .= "<td width='5%'>".$li->column($pos++, $i_ClassNumber)."</td>\n";
	$li->column_list .= "<td width='10%'>".$li->column($pos++, $i_Attendance_Date)."</td>\n";
	$li->column_list .= "<td width='20%'>".$li->column($pos++, $Lang['SysMgr']['Timetable']['Period'])."</td>\n";
	$li->column_list .= "<td width='30%'>".$li->column($pos++, $Lang['SysMgr']['SubjectClassMapping']['SubjectGroupTitle'])."</td>\n";
	$li->column_list .= "<td width='10%'>".$li->column($pos++, $Lang['General']['Status2'])."</td>\n";
	$li->column_list .= "<td width='10%'>".$li->column($pos++, $Lang['LessonAttendance']['Reason'])."</td>\n";
//	$li->column_list .= "<td width='2%'>".$li->column($pos++, $Lang['StudentAttendance']['Waived'])."</td>\n";
//	$li->column_list .= "<td width='25%'>".$li->column($pos++, $i_Attendance_Remark)."</td>\n";
//	$li->column_list .= "<td width='1'>".$li->check("RecordID[]")."</td>\n";

}

//$TAGS_OBJ[] = array($button_new, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/dailyoperation/preset_absence/new.php", 0);
//$TAGS_OBJ[] = array($i_SmartCard_DailyOperation_Preset_Absence_BrowseByStudent, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/dailyoperation/preset_absence/browse_by_student.php", 1);
//$TAGS_OBJ[] = array($i_SmartCard_DailyOperation_Preset_Absence_BrowseByDate, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/dailyoperation/preset_absence/browse_by_date.php", 0);
//
//#$PAGE_NAVIGATION[] = array("$TargetDate ($display_period)");
//
$MODULE_OBJ = $lc->GET_MODULE_OBJ_ARR();

//$TAGS_OBJ[] = array($Lang['LessonAttendance']['LessonSummaryReport'],"",0);
//$MODULE_OBJ = $LessonAttendUI->GET_MODULE_OBJ_ARR();

$TAGS_OBJ[] = array($Lang['LessonAttendance']['LessonSummaryReport'], "lesson_summary_report.php", 0);
$TAGS_OBJ[] = array($i_SmartCard_DailyOperation_Preset_Absence_BrowseByStudent,"lesson_summary_report_detail.php",1);

$linterface->LAYOUT_START(urldecode($Msg));



if ($msg == 1) $SysMsg = $linterface->GET_SYS_MSG("add");
if ($msg == 2) $SysMsg = $linterface->GET_SYS_MSG("update");
if ($msg == 3) $SysMsg = $linterface->GET_SYS_MSG("delete");
?>
<script language='javascript'>
<!--
function changeClass(){
var action_list=document.getElementById("StudentList[]");
if(action_list){
//x.remove(x.selectedIndex);


// Remember selected items.
var is_selected = [];
for (var i = 0; i < action_list.options.length; ++i)
{
    is_selected[i] = action_list.options[i].selected;
}

// Remove selected items.
i = action_list.options.length;
while (i--)
{
    if (is_selected[i])
    {
        action_list.remove(i);
    }
}
}
	submitForm();
}
function submitForm(){
	obj = document.form1;
	if(obj!=null) obj.submit();
}
function openPrintPage()
{
        newWindow("lesson_summary_report_detail_print.php?class=<?=$class?>&studentID=<?=($StudentList?implode(',', $StudentList):$StudentList)?>&filter=<?=$filter?>&keyword=<?=$keyword?>&order=<?=$order?>&field=<?=$field?>&FromDate=<?=$FromDate?>&ToDate=<?=$ToDate?>", 12);
}
function exportPage(newurl){
	old_url = document.form1.action;
	document.form1.action = newurl;
	document.form1.submit();
	document.form1.action = old_url;
}
function Date_Validate() {
	var FromDate = document.getElementById('FromDate').value;
	var ToDate = document.getElementById('ToDate').value;
	//date validation
	if(FromDate > ToDate/* || ToDate > '<?=date('Y-m-d')?>'*/){
		//alert("<?=$Lang['StaffAttendance']['InvalidDateRange']?>");
		$('#ToDateWarningLayer').html('<?=$Lang['General']['JS_warning']['InvalidDateRange']?>').show();
		return false;
	}else if(document.getElementById('StudentList[]').options[document.getElementById('StudentList[]').selectedIndex].value ==""){
			alert("<?=$Lang['StudentAttendance']['PleaseSelectOneStudent']?>");
		return;
	}
	else
		document.form1.submit();
	}
-->
</script>

	<!--<div class="navigation_v30 navigation_2">
		<a href="lesson_summary_report.php"><?=$Lang['LessonAttendance']['LessonSummaryReport']?></a>
		<span><?=$Lang['StaffAttendance']['RecordDetails']?></span>
	</div>
	<br style="clear:both"><br />-->
<form name="form1" method="GET">
<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext" width="30%"><?= $i_UserClassName ?></td>
		<td valign="top" class="tabletext" width="70%"><?=$select_class?></td>
	</tr>
	<?php if($select_student!=""){?>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext" width="30%"><?= $i_UserStudentName ?></td>
		<td valign="top" class="tabletext" width="70%">
            <span style="vertical-align: bottom;"><?=$select_student?></span>
		    <?=$linterface->GET_SMALL_BTN($Lang['StaffAttendance']['SelectDeSelectAll'],"button","var SelectedAlready = false; var SelectObj = document.getElementById('StudentList[]'); for (var i=0; i< SelectObj.length; i++) {if (SelectObj.options[i].selected) {SelectedAlready = true;}} for (var i=0; i< SelectObj.length; i++) {SelectObj.options[i].selected = !(SelectedAlready);}");?>
		</td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext" width="30%">
			<?= $Lang['LessonAttendance']['FromDate'] ?>
		</td>
		<td class="tabletext" width="70%">
			<?= $linterface->GET_DATE_PICKER("FromDate", $FromDate, "", "yy-mm-dd", "", "", "", "$('#ToDateWarningLayer').html('" . $Lang['General']['JS_warning']['InvalidDateRange'] . "').hide();")?>
			<span style="color:red" id="FromDateWarningLayer"></span>
		</td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext" width="30%">
			<?= $Lang['LessonAttendance']['ToDate'] ?>
		</td>
		<td class="tabletext" width="70%">
			<?= $linterface->GET_DATE_PICKER("ToDate", $ToDate, "", "yy-mm-dd", "", "", "", "$('#ToDateWarningLayer').html('" . $Lang['General']['JS_warning']['InvalidDateRange'] . "').hide();")?>
			<span style="color:red" id="ToDateWarningLayer"></span>
		</td>
	</tr>
	<tr>
		<td colspan="2" class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
	</tr>
	<tr>
		<td colspan="2" align="center" class="tabletext">
			<?= $linterface->GET_ACTION_BTN($button_view, "button", "Date_Validate()","submit2") ?>
		</td>
	</tr>
	<?php } ?>
</table>
<br />
<?php if($StudentList!=""){
	
	//$toolbar = $linterface->GET_LNK_NEW("javascript:checkGet(document.form1,'new.php')","","","","",0);
	$toolbar = $linterface->GET_LNK_PRINT("javascript:openPrintPage()","","","","",0);
	$toolbar .= $linterface->GET_LNK_EXPORT("javascript:exportPage('lesson_summary_report_detail_export.php')","","","","",0);
	
	$table_tool .= "<td nowrap=\"nowrap\">
					<a href=\"javascript:checkRemove(document.form1,'RecordID[]','remove.php')\" class=\"tabletool\">
						<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_delete.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">
						$button_remove
					</a>
				</td>";
	$table_tool .= "<td><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"5\"></td>";
	$table_tool .= "<td nowrap=\"nowrap\">
						<a href=\"javascript:checkEdit(document.form1,'RecordID[]','edit.php')\" class=\"tabletool\">
							<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_edit.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">
							$button_edit
						</a>
					</td>";
	
	
	//$array_status_name = array($i_StudentAttendance_Reminder_Status_Past, $i_StudentAttendance_Reminder_Status_Coming, $i_status_all);
	$array_status_name = array($Lang['LessonAttendance']['Late&Absent'], $Lang['LessonAttendance']['Late'], $Lang['LessonAttendance']['Absent'], $Lang['LessonAttendance']['Outing']);
	$array_status_data = array("1", "2", "3", "4");
	$status_select = getSelectByValueDiffName($array_status_data,$array_status_name,"name='filter' onChange='submitForm()'",$filter,0,1);
	
	$filterbar = $status_select;
	
	$searchbar = "<input class=\"textboxnum\" type=\"text\" name=\"keyword\" maxlength=\"50\" value=\"".stripslashes($keyword)."\" onFocus=\"this.form.pageNo.value=1;\">\n";
	$searchbar .= $linterface->GET_SMALL_BTN($button_find, "button", "document.form1.submit();","submit2");

?>
<table width="95%" border="0" cellpadding="3" cellspacing="0" align="center">
	<tr>
		<td align="left"><?=$toolbar?></td>
		<td align="right"><?=$SysMsg?></td>
	</tr>
</table>
<table width="95%" border="0" cellpadding="0" cellspacing="0" align="center">
	<tr>
		<td align="left"><?= $filterbar; ?><?=$searchbar?></td>
		<!--<td valign="bottom" align="right">
			<table border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="21"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_01.gif" width="21" height="23"></td>
					<td background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_02.gif">
						<table border="0" cellspacing="0" cellpadding="2">
							<tr>
								<?=$table_tool?>
							</tr>
						</table>
					</td>
					<td width="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_03.gif" width="6" height="23"></td>
				</tr>
			</table>
		</td>-->
	</tr>
</table>
<br/>
<?php echo $li->display("95%"); ?>
<?php } ?>
<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>">
<input type="hidden" name="order" value="<?php echo $li->order; ?>">
<input type="hidden" name="field" value="<?php echo $li->field; ?>">
<input type="hidden" name="page_size_change" value="">
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>">
</form>
<br />
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>