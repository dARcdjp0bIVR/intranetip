<?
//editing by kenneth chung

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();
$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage['NoMissConduct'] = 1;


$linterface = new interface_html();

$lclass= new libclass();

# date range
$ts = strtotime(date('Y-m-d'));
$startDate = ($startDate != "")? $startDate:date('Y-m-d',getStartOfAcademicYear($ts));
$endDate = ($endDate != "")? $endDate:date('Y-m-d',getEndOfAcademicYear($ts));

# format
$select_format ="<SELECT name=\"Format\">\n";
$select_format .="<OPTION value=\"1\">Web</OPTION>\n";
$select_format.="<OPTION VALUE=\"2\">CSV</OPTION>\n";
$select_format.="</SELECT>\n";

# attendance type
$select_attend = "<SELECT name=\"RecordType\">\n";
$select_attend .= "<OPTION value=\"\" SELECTED>$i_status_all</OPTION>";
$select_attend .= "<OPTION value=\"".CARD_STATUS_ABSENT."\">$i_StudentAttendance_Status_Absent</OPTION>";
$select_attend .= "<OPTION value=\"".CARD_STATUS_LATE."\">$i_StudentAttendance_Status_Late</OPTION>";
$select_attend .= "<OPTION value=\"".PROFILE_TYPE_EARLY."\">$i_StudentAttendance_Status_EarlyLeave</OPTION>";
$select_attend .="</SELECT>\n";

$TAGS_OBJ[] = array($Lang['StudentAttendance']['NoMissConductReport'], "", 0);

$MODULE_OBJ = $lc->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();
?>
<style type="text/css">
.class_list{width:100px; height=150px;}
</style>
<script type="text/javascript">
<!--
function isValidDate(obj){
	if(!check_date(obj,"<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>")) return false;
	return true;
}
// -->
</script>
<br />
<form name="form1" action="no_miss_conduct_report.php" method="POST" target="_blank">
<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_general_startdate?></td>
		<td width="70%"class="tabletext">
			<?=$linterface->GET_DATE_PICKER("StartDate", $startDate)?>
		</td>
	</tr>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_general_enddate?></td>
		<td width="70%"class="tabletext">
			<?=$linterface->GET_DATE_PICKER("EndDate", $endDate)?>
		</td>
	</tr>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_Attendance_attendance_type?></td>
		<td width="70%"class="tabletext">
			<?=$select_attend?>
		</td>
	</tr>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$Lang['StudentAttendance']['IncludeWaive']?></td>
		<td width="70%"class="tabletext">
			<input type="checkbox" id="IncludeWaive" name="IncludeWaive" value="1">
		</td>
	</tr>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_general_Format?></td>
		<td width="70%"class="tabletext">
			<?=$select_format?>
		</td>
	</tr>
</table>
<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
    	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
    </tr>
</table>
<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
	    <td align="center" colspan="2">
			<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "") ?>
			<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2"," class='formbutton' onclick=\"document.getElementById('OptionsLayer').style.display='none';\" onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
		</td>
	</tr>
</table>
</form>
<br />
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>