<?php
//editing by 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libstudentattendance_ui.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$StudentAttendUI = new libstudentattendance_ui();

$StartDate = $_REQUEST['StartDate'];
$EndDate = $_REQUEST['EndDate'];
$IncludeWaive = $_REQUEST['IncludeWaive'];
$Format = $_REQUEST['Format'];
$RecordType = $_REQUEST['RecordType'];

//debug_r($_REQUEST);
$StudentAttendUI->Get_No_Miss_Conduct_Report($StartDate,$EndDate,$IncludeWaive,$RecordType,$Format);

intranet_closedb();
?>