<?php
// Editing by 
/*
 * 2016-06-06 (Carlos): Created.
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libstudentattendance_ui.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-LessonAttendance"] || !$_SESSION["SSV_PRIVILEGE"]["plugin"]["attendancelesson"]) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$linterface = new interface_html();
$LessonAttendUI = new libstudentattendance_ui();
$scm = new subject_class_mapping();

$academic_year_startdate = date("Y-m-d",getStartOfAcademicYear());
$academic_year_enddate = date("Y-m-d",getEndOfAcademicYear());
$today = date("Y-m-d");

$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageLessonAttendance_LateSkipLessonRecords";

$name_field = getNameFieldByLang2();
$sql = "SELECT UserID,$name_field as UserName FROM INTRANET_USER WHERE RecordStatus=1 AND RecordType='".USERTYPE_STAFF."' ORDER BY UserName";
$teacher_ary = $LessonAttendUI->returnArray($sql);
$teacher_selection = getSelectByArray($teacher_ary, ' id="TeacherID" name="TeacherID"  ', $__selected="", $__all=1, $__noFirst=0, $__FirstTitle="", $__ParQuoteValue=1);

$subject_list = $scm->Get_Subject_List();
$subject_ary = array();
$subject_name_field = Get_Lang_Selection("SubjectDescB5","SubjectDescEN");
$subject_shortname_field = Get_Lang_Selection("SubjectShortNameB5","SubjectShortNameEN");
for($i=0;$i<count($subject_list);$i++)
{
	$subject_ary[] = array($subject_list[$i]['SubjectID'],$subject_list[$i][$subject_name_field]);
}
$subject_selection = getSelectByArray($subject_ary, ' id="SubjectID" name="SubjectID"  ', $__selected="", $__all=1, $__noFirst=0, $__FirstTitle="", $__ParQuoteValue=1);

### Title ###
$TAGS_OBJ[] = array($Lang['LessonAttendance']['LateSkipLessonRecords'],"",0);
$MODULE_OBJ = $LessonAttendUI->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START(); 
?>
<br />
<form id="form1" name="form1" action="" method="POST" onsubmit="return false;">
<table class="form_table_v30" width="96%" border="0" cellspacing="0" cellpadding="5" align="center">
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="field_title"><label for="StartDate"><?=$Lang['General']['Date']?></label><span class="tabletextrequire">*</span></td>
		<td width="70%">
			<?=$Lang['General']['From'].'&nbsp;'.$linterface->GET_DATE_PICKER("StartDate", $academic_year_startdate).$Lang['General']['To'].'&nbsp;'.$linterface->GET_DATE_PICKER("EndDate", $today)?>
			<?=$linterface->Get_Form_Warning_Msg('Date_Error', $Lang['General']['JS_warning']['InvalidDateRange'], 'Date_Error')?>
		</td>
	</tr>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="field_title"><label for="TargetType"><?=$Lang['AccountMgmt']['Form']." / ".$Lang['AccountMgmt']['Class']." / ".$Lang['Identity']['Student']?></label><span class="tabletextrequire">*</span></td>
		<td width="70%" class="tabletext">
			<table class="inside_form_table">
				<tr>
					<td valign="top">
						<select name="TargetType" id="TargetType" onchange="targetTypeChanged(this);">
							<option value="" selected="selected">-- <?=$Lang['General']['PleaseSelect']?> --</option>
							<option value="form"><?=$Lang['SysMgr']['FormClassMapping']['Form']?></option>
							<option value="class"><?=$Lang['SysMgr']['FormClassMapping']['Class']?></option>
							<option value="student"><?=$Lang['Identity']['Student']?></option>
						</select>
					</td>
					<td>
						<span id='DivRankTargetDetail'></span>
						<?=$linterface->GET_BTN($Lang['Btn']['SelectAll'], "button", "if(document.getElementById('TargetID')){Select_All_Options('TargetID', true);}return false;","selectAllTargetBtn",' style="display:none;"')?>
					</td>
				</tr>
			</table>
			<?=$linterface->Get_Form_Warning_Msg('TargetID_Error', $Lang['eDiscipline']['WarningMsgArr']['SelectTarget'], 'TargetID_Error', false);?>
			<div class="tabletextremark" id="DivSelectAllRemark" style="display:none;"><?=$Lang['SysMgr']['FormClassMapping']['CtrlMultiSelectMessage']?></div>
		</td>
	</tr>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="field_title"><label for="TeacherID"><?=$Lang['General']['Teacher']?></label><span class="tabletextrequire">*</span></td>
		<td width="70%" class="tabletext">
			<?=$teacher_selection?>
		</td>
	</tr>
	<tr>
	    <td width="30%" valign="top" nowrap="nowrap" class="field_title"><label for="SubjectID"><?=$Lang['LessonAttendance']['Subject']?></label><span class="tabletextrequire">*</span></td>
	    <td width="70%" class="tabletext">
			<?=$subject_selection?>
	    </td>
	</tr>
	<tr>
	    <td width="30%" valign="top" nowrap="nowrap" class="field_title"><label for="ViewType"><?=$Lang['Btn']['View']?></label><span class="tabletextrequire">*</span></td>
	    <td width="70%" class="tabletext">
			<?=$linterface->Get_Radio_Button("ViewType_Late", "ViewType", "1", $__isChecked=1, $__class='', $__display=$Lang['LessonAttendance']['Late'], $__onclick='', $__disabled='');?>
			<?=$linterface->Get_Radio_Button("ViewType_SkipLesson", "ViewType", "2", $__isChecked=0, $__class='', $__display=$Lang['LessonAttendance']['SkipLesson'], $__onclick='', $__disabled='');?>
	    </td>
	</tr>
</table>
<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr><td align="left" class="tabletextremark"><?=$i_general_required_field?></td></tr>
</table>
<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
    	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
    </tr>
</table>
<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
	    <td align="center" colspan="2">
			<?= $linterface->GET_ACTION_BTN($Lang['Btn']['View'], "button", "submitForm('');","submitBtn") ?>
		</td>
	</tr>
</table>
<input type="hidden" name="Format" id="Format" value="" />
</form>
<br />
<div id="ReportLayer"></div>
<br />
<script type="text/javascript" language="javascript">
var loadingImg = '<?=$linterface->Get_Ajax_Loading_Image();?>';
var academicYearId = '<?=Get_Current_Academic_Year_ID()?>';

function targetTypeChanged(obj)
{
	var selectedTargetType = obj.value;
	if(selectedTargetType == 'form' || selectedTargetType == 'class'){
		$('#DivRankTargetDetail').html(loadingImg).load(
			'../../common/ajax_load_target_selection.php',
			{
				'target':selectedTargetType,
				'academicYearId':academicYearId,
				'fieldId':'TargetID',
				'fieldName':'TargetID[]'
			},
			function(data){
				$('#selectAllTargetBtn').show();
				$('#DivSelectAllRemark').show();
			}
		);
	}else if(selectedTargetType == 'student'){
		$('#DivRankTargetDetail').html(loadingImg).load(
			'../../common/ajax_load_target_selection.php',
			{
				'target':'student',
				'academicYearId':academicYearId,
				'fieldId':'studentTargetID',
				'fieldName':'studentTargetID[]',
				'onchange':'getStudentSelection()',
				'divStudentSelection':'DivStudentSelection'
			},
			function(data){
				$('#selectAllTargetBtn').hide();
				$('#DivSelectAllRemark').show();
				getStudentSelection();
			}
		);
	}else{
		$('#DivRankTargetDetail').html('');
		$('#selectAllTargetBtn').hide();
		$('#DivSelectAllRemark').hide();
	}
}

function getStudentSelection()
{
	var selectedYearClassId = [];
	var yearClassObj = document.getElementById('studentTargetID');
	for(var i=0;i<yearClassObj.options.length;i++) {
		if(yearClassObj.options[i].selected) {
			selectedYearClassId.push(yearClassObj.options[i].value);
		}
	}
	
	$('#DivStudentSelection').html(loadingImg);
	$.post(
		'../../common/ajax_load_target_selection.php',
		{
			'target':'student2ndLayer',
			'academicYearId':academicYearId,
			'fieldId':'studentTargetID',
			'fieldName':'studentTargetID[]',
			'studentFieldName':'TargetID[]',
			'studentFieldId':'TargetID',
			'YearClassID[]':selectedYearClassId 
		},
		function(data){
			$('#DivStudentSelection').html(data);
		}
	);
}

function submitForm(format)
{
	$('#submitBtn').attr('disabled',true);
	var is_valid = true;
	var startdateObj = document.getElementById('StartDate');
	var enddateObj = document.getElementById('EndDate');
	var target_obj = document.getElementById('TargetID');
	var target_values = [];

	if(!check_date_without_return_msg(startdateObj)){
		is_valid = false;
	}
	if(!check_date_without_return_msg(enddateObj)){
		is_valid = false;
	}
	if(startdateObj.value > enddateObj.value){
		is_valid = false;
		$('#Date_Error').show();
	}else{
		$('#Date_Error').hide();
	}

	if(target_obj)
	{
		for(var i=0;i<target_obj.options.length;i++)
		{
			if(target_obj.options[i].selected) target_values.push(target_obj.options[i].value);
		}
	}

	if(target_values.length <= 0)
	{
	 	is_valid = false;
		$('#TargetID_Error').show();
	}else{
		$('#TargetID_Error').hide();
	}
	
	if(!is_valid) $('#submitBtn').attr('disabled',false);
	
	if(is_valid)
	{
		$('#Format').val(format);
		if(format == '' || format == 'web'){
			$('#ReportLayer').html(loadingImg);
			$.post(
				'report.php',
				$('#form1').serialize(),
				function(returnHtml)
				{
					$('#ReportLayer').html(returnHtml);
					$('#submitBtn').attr('disabled',false);
				}
			);
		}else if(format == 'print' || format == 'pdf')
		{
			var url = '';
			var winType = '10';
			var win_name = 'intranet_popup'+winType;
			
			var oldAction = document.form1.action;
			var oldTarget = document.form1.target;
			
			document.form1.action = 'report.php';
			document.form1.target = win_name;
			
			newWindow(url, winType);
			document.form1.submit();
			
			document.form1.action = oldAction;
			document.form1.target = oldTarget;
			$('#submitBtn').attr('disabled',false);
		}else if(format == 'csv'){
			
			var oldAction = document.form1.action;
			var oldTarget = document.form1.target;
			
			document.form1.action = 'report.php';
			document.form1.target = '_blank';
			document.form1.submit();
			
			document.form1.action = oldAction;
			document.form1.target = oldTarget;
			$('#submitBtn').attr('disabled',false);
		}
	}
}
</script>
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>