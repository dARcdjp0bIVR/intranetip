<?php

# Using: Henry Chan

################################################# Change log ####################################################
# 2013-08-02 by Henry: Adding status filter [Late, Absent, Late & Absent]
#################################################################################################################

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libstudentattendance_ui.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-LessonAttendance"] || !$_SESSION["SSV_PRIVILEGE"]["plugin"]["attendancelesson"]) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$TargetDate = $_REQUEST['TargetDate'];

$linterface = new interface_html();

$LessonAttendUI = new libstudentattendance_ui();
$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageLessonAttendance_LessonAttendSummary";

### Title ###
$TAGS_OBJ[] = array($Lang['LessonAttendance']['LessonSummaryReport'],"lesson_summary_report.php",1);
$TAGS_OBJ[] = array($i_SmartCard_DailyOperation_Preset_Absence_BrowseByStudent, "lesson_summary_report_detail.php", 0);

$MODULE_OBJ = $LessonAttendUI->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START(); 

echo $LessonAttendUI->Get_Lesson_Summary_Form();

$linterface->LAYOUT_STOP();

intranet_closedb();
?>
<script>
//for getting the students of the selected class
function changeClass(){
	submitForm(); //!!!!!!!!!!should be changed to ajax to get the student list
}
function submitForm(){
	obj = document.form1;
	if(obj!=null) obj.submit();
}
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

//function Get_Student_List() {
//	var PersonalSelect = document.getElementById('PersonalSelection').checked;
//	
//	if (PersonalSelect) {
//		document.getElementById('StudentListRow').style.display = '';
//		$('#ClassSummaryLayer').hide();
//		document.getElementById('ColumnClassSummaryStat').checked = false;
//		var ClassSelected = Get_Selection_Value("class_name[]","Array");
//		
//		var PostVar = {
//			"ClassList[]":ClassSelected
//		};
//		
//		$('#StudentListLayer').load("ajax_get_student_selection.php",PostVar,
//			function(data) {
//				if (data == "die") {
//					window.top.location = "/";
//				}
//			}
//		);
//	}
//	else {
//		document.getElementById('StudentListRow').style.display = 'none';
//		$('#StudentListLayer').html('');
//		$('#ClassSummaryLayer').show();
//	}
//}

function View_Student_In_Class() {
	var ClassID = document.getElementById('ClassID').options[document.getElementById('ClassID').selectedIndex].value;
	var PersonalSelect = document.getElementById('PersonalSelection').checked;
	if (PersonalSelect && ClassID) {
		document.getElementById('StudentListRow').style.display = '';
	//if (ClassID) {
	//if (document.getElementById('selectStudent').checked && ClassID){
		//alert(ClassID);
		//$("span#TargetDateWarningLayer").hide();
		Block_Element('StudentListLayer');
		
		PostVar = {
			"ClassID":ClassID
		}
		
		$("div#StudentListLayer").load('ajax_get_class_student.php',PostVar,function() {UnBlock_Element('StudentListLayer');});
	}
	else {
		document.getElementById('PersonalSelection').checked = false;
		document.getElementById('StudentListRow').style.display = 'none';
		$("div#StudentListLayer").html('');
		//$("span#TargetDateWarningLayer").show();
	}
}

function View_Lesson_Summary() {
	var FromDate = document.getElementById('FromDate').value;
	var ToDate = document.getElementById('ToDate').value;
	//date validation
	if(FromDate > ToDate/* || ToDate > '<?=date('Y-m-d')?>'*/){
		//alert("<?=$Lang['StaffAttendance']['InvalidDateRange']?>");
		$('#ToDateWarningLayer').html('<?=$Lang['General']['JS_warning']['InvalidDateRange']?>').show();
		return;
	}
	var ClassID = document.getElementById('ClassID').options[document.getElementById('ClassID').selectedIndex].value;
	var StudentID = '';
	if(document.getElementById('opt_student[]') != null && document.getElementById('PersonalSelection').checked){
		if(Get_Selection_Value("opt_student[]","Array") ==""){
			alert("<?=$Lang['StudentAttendance']['PleaseSelectAtLeastOneStudent']?>");
		return;
		}
		StudentID = Get_Selection_Value("opt_student[]","Array");
	}
	if(Get_Check_Box_Value('AttendStatus[]','Array') == ""){
		alert("<?=$Lang['StudentAttendance']['PleaseSelectAtLeastOneStatus']?>");
		return;
	}
	//var StudentID = '';
	//alert(StudentID);
	var re = /\b\d{4}[\/-]\d{1,2}[\/-]\d{1,2}\b/;
	
	if (re.test(FromDate) && re.test(ToDate)) {
		$("span#TargetDateWarningLayer").hide();
		Block_Element('DetailTableLayer');
		
		PostVar = {
			"FromDate":FromDate,
			"ToDate":ToDate,
			"ClassID":ClassID,
			"AttendStatus[]":Get_Check_Box_Value('AttendStatus[]','Array'),
			"StudentID[]":StudentID
		}
		
		$("div#DetailTableLayer").load('ajax_get_lesson_summary_report.php',PostVar,function() {UnBlock_Element('DetailTableLayer');});
	}
	else {
		$("span#TargetDateWarningLayer").html('<?=$Lang['LessonAttendance']['InvalidDateFormat']?>');
		$("span#TargetDateWarningLayer").show();
	}
}
</script>