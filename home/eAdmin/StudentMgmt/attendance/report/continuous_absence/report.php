<?php
// Editing by 
/*
 * 2018-04-30 (Carlos): Created.
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");

intranet_auth();
intranet_opendb();

$Task = $_POST['Task'];

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	intranet_closedb();
	//header ("Location: /");
	exit();
}

$lc = new libcardstudentattend2();

if($Task == 'prepare'){
	
	$linterface = new interface_html();
	
	$TargetType = $_POST['TargetType'];
	$TargetID = $_POST['TargetID'];
	$TargetStartDate = $_POST['StartDate'];
	$TargetEndDate = $_POST['EndDate'];
	
	$target_result = $lc->GetTargetSelectionResult($TargetType, $TargetID);
	$targetStudentIdAry = $target_result[0];
	$targetStudentIdCsv = implode(',',IntegerSafe($targetStudentIdAry));
	
	$x = '<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">' . "\n";
		$x .= '<tr>' . "\n";
		$x .= '<td>' . "\n";
			$x .= '<div class="content_top_tool" style="display:none;">' . "\n";
				$x .= '<div class="Conntent_tool">' . "\n";
				//$x .= $linterface->GET_LNK_PRINT("javascript:submitForm('print');", "", "", "", "", 1);
				//$x .= $linterface->GET_LNK_PRINT("javascript:submitForm('pdf');", $Lang['Btn']['Print']." PDF", "", "", "", 1);
				$x .= $linterface->GET_LNK_EXPORT("javascript:exportReport();", "", "", "", "", 1);
				$x .= '</div>' . "\n";
				$x .= '<br style="clear: both;">' . "\n";
			$x .= '</div>' . "\n";
		$x .= '</td>' . "\n";
		$x .= '</tr>' . "\n";
	$x .= '</table>' . "\n";
	
	$x .= '<table class="common_table_list_v30" id="ReportTable">'."\n";
			$x.='<thead>'."\n";
			$x.= '<tr>'."\n";
				//$x.='<th class="num_check" width="1">#</th>'."\n";
				$x.='<th style="width:10%;">'.$Lang['StudentAttendance']['Class'].'</th>'."\n";
				$x.='<th style="width:10%;">'.$Lang['StudentAttendance']['ClassNumber'].'</th>'."\n";
				$x.='<th style="width:25%;">'.$Lang['Identity']['Student'].'</th>'."\n";
				$x.='<th style="width:55%;">'.$Lang['StudentAttendance']['AbsentDates'].'</th>'."\n";
			$x.= '</tr>'."\n";
			$x.='</thead>'."\n";
			$x.='<tbody></tbody>'."\n";
			
	$x.= '</table>'."\n";
	$x.= '<input type="hidden" name="StudentID" id="StudentID" value="'.escape_double_quotes($targetStudentIdCsv).'" />';
	$x.= '<input type="hidden" name="TargetStartDate" value="'.escape_double_quotes($TargetStartDate).'" />';
	$x.= '<input type="hidden" name="TargetEndDate" value="'.escape_double_quotes($TargetEndDate).'" />';
	$x.= '<div id="SpinnerContainer">'.$linterface->Get_Ajax_Loading_Image().'</div>'."\n";
	
	echo $x;
	
}elseif($Task == 'process'){
	
	$StartDate = $_POST['StartDate'];
	$EndDate = $_POST['EndDate'];
	$StudentIdAry = IntegerSafe($_POST['StudentID']);
	$AbsenceDays = intval($_POST['AbsenceDays']);
	
	$student_size = count($StudentIdAry);
	
	$x = '';
	for($i=0;$i<$student_size;$i++){
		$data = $lc->calculateContinuousAbsentStudent($StudentIdAry[$i], $StartDate, $EndDate, $AbsenceDays);
		$absent_date_size = count($data['AbsentDates']);
		if($absent_date_size>0){
			$x .= '<tr>';
			$x .= '<td>'.Get_String_Display($data['ClassName'],1).'</td>';
			$x .= '<td>'.Get_String_Display($data['ClassNumber'],1).'</td>';
			$x .= '<td>'.Get_String_Display($data['StudentName'],1).'</td>';
			$x .= '<td>';
			$x .= '<input type="hidden" name="AbsentStudentID[]" value="'.escape_double_quotes($StudentIdAry[$i]).'" />';
			$sep = '';
			for($j=0;$j<$absent_date_size;$j++){
				$x .= $sep.'['.implode(', ',$data['AbsentDates'][$j]).']';
				$sep = '<br />';
				$x .= '<input type="hidden" name="AbsentDates['.$StudentIdAry[$i].'][]" value="'.implode(', ',$data['AbsentDates'][$j]).'" />';
			}
			$x .= '</td>';
			$x .= '</tr>';
		}
	}
	
	echo $x;
}

if($Format == 'export'){
	include_once ($PATH_WRT_ROOT."includes/libuser.php");
	include_once ($PATH_WRT_ROOT."includes/libexporttext.php");
	$lexport = new libexporttext();

	$rows = array();
	$row = array();
	$headers = array();
	
	$headers[] = $Lang['StudentAttendance']['Class'];
	$headers[] = $Lang['StudentAttendance']['ClassNumber'];
	$headers[] = $Lang['Identity']['Student'];
	$headers[] = $Lang['StudentAttendance']['AbsentDates'];
	
	$start_date = $_POST['TargetStartDate'];
	$end_date = $_POST['TargetEndDate'];
	$student_id_ary = $_POST['AbsentStudentID'];
	$student_size = count($student_id_ary);
	
	for($i=0;$i<$student_size;$i++){
		
		$student_id = $student_id_ary[$i];
		$lu = new libuser($student_id);
		$class_name = $lu->ClassName;
		$class_number = $lu->ClassNumber;
		$student_name = $lu->StandardDisplayName;
		$absent_dates = $_POST['AbsentDates'][$student_id];
		
		for($j=0;$j<count($absent_dates);$j++){
			unset($row);
			
			if($j == 0){
				$row[] = $class_name;
				$row[] = $class_number;
				$row[] = $student_name;
			}else{
				$row[] = '';
				$row[] = '';
				$row[] = '';
			}
			$row[] = $absent_dates[$j];
			
			$rows[] = $row;
		}
	}
	
	$exportContent = $lexport->GET_EXPORT_TXT($rows, $headers);
	$lexport->EXPORT_FILE('continuous_absence_records('.(date('Ymd',strtotime($start_date))).'-'.(date('Ymd',strtotime($end_date))).').csv', $exportContent);
}

intranet_closedb();
?>