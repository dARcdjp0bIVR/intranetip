<?php
// Editing by 
/*
 * 2019-10-31 (Carlos): Change report.php to ajax_report.php.
 * 2018-04-30 (Carlos): Created.
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	intranet_closedb();
	header ("Location: /");
	exit();
}

$linterface = new interface_html();
$lc = new libcardstudentattend2();

$academic_year_startdate = date("Y-m-d",getStartOfAcademicYear());
$academic_year_enddate = date("Y-m-d",getEndOfAcademicYear());
$today = date("Y-m-d");

$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageReport_ContinuousAbsenceReport";

### Title ###
$TAGS_OBJ[] = array($Lang['StudentAttendance']['ContinuousAbsenceReport'],"",0);
$MODULE_OBJ = $lc->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START(); 
?>
<br />
<form id="form1" name="form1" action="" method="POST" onsubmit="return false;">
<table class="form_table_v30" width="96%" border="0" cellspacing="0" cellpadding="5" align="center">
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="field_title"><label for="StartDate"><?=$Lang['General']['Date']?></label><span class="tabletextrequire">*</span></td>
		<td width="70%">
			<?=$Lang['General']['From'].'&nbsp;'.$linterface->GET_DATE_PICKER("StartDate", $academic_year_startdate).$Lang['General']['To'].'&nbsp;'.$linterface->GET_DATE_PICKER("EndDate", $today)?>
			<?=$linterface->Get_Form_Warning_Msg('Date_Error', $Lang['General']['JS_warning']['InvalidDateRange'], 'Date_Error')?>
		</td>
	</tr>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="field_title"><label for="TargetType"><?=$Lang['AccountMgmt']['Form']." / ".$Lang['AccountMgmt']['Class']." / ".$Lang['Identity']['Student']?></label><span class="tabletextrequire">*</span></td>
		<td width="70%" class="tabletext">
			<table class="inside_form_table">
				<tr>
					<td valign="top">
						<select name="TargetType" id="TargetType" onchange="targetTypeChanged(this);">
							<option value="" selected="selected">-- <?=$Lang['General']['PleaseSelect']?> --</option>
							<option value="form"><?=$Lang['SysMgr']['FormClassMapping']['Form']?></option>
							<option value="class"><?=$Lang['SysMgr']['FormClassMapping']['Class']?></option>
							<option value="student"><?=$Lang['Identity']['Student']?></option>
						</select>
					</td>
					<td>
						<span id='DivRankTargetDetail'></span>
						<?=$linterface->GET_BTN($Lang['Btn']['SelectAll'], "button", "if(document.getElementById('TargetID')){Select_All_Options('TargetID', true);}return false;","selectAllTargetBtn",' style="display:none;"')?>
					</td>
				</tr>
			</table>
			<?=$linterface->Get_Form_Warning_Msg('TargetID_Error', $Lang['eDiscipline']['WarningMsgArr']['SelectTarget'], 'TargetID_Error', false);?>
			<div class="tabletextremark" id="DivSelectAllRemark" style="display:none;"><?=$Lang['SysMgr']['FormClassMapping']['CtrlMultiSelectMessage']?></div>
		</td>
	</tr>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="field_title"><label for="AbsenceDays"><?=$Lang['StudentAttendance']['ContinuousAbsentDays']?></label><span class="tabletextrequire">*</span></td>
		<td width="70%" class="tabletext">
			<?=$linterface->GET_TEXTBOX_NUMBER("AbsenceDays", "AbsenceDays", 7, '', array('onchange'=>'absenceDaysChanged(this);'))?> <?=$Lang['StudentAttendance']['Days']?>
		</td>
	</tr>
</table>
<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr><td align="left" class="tabletextremark"><?=$i_general_required_field?></td></tr>
</table>
<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
    	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
    </tr>
</table>
<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
	    <td align="center" colspan="2">
			<?= $linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "button", "submitForm();","submitBtn") ?>
		</td>
	</tr>
</table>
<input type="hidden" name="Task" id="Task" value="" />
</form>
<br />
<form id="form2" name="form2" method="post" action="">
<div id="ReportLayer"></div>
<input type="hidden" name="Format" id="Format" value="export" />
</form>
<br />
<script type="text/javascript" language="javascript">
var loadingImg = '<?=$linterface->Get_Ajax_Loading_Image();?>';
var academicYearId = '<?=Get_Current_Academic_Year_ID()?>';
var studentIdAry = [];

function targetTypeChanged(obj)
{
	var selectedTargetType = obj.value;
	if(selectedTargetType == 'form' || selectedTargetType == 'class'){
		$('#DivRankTargetDetail').html(loadingImg).load(
			'../../common/ajax_load_target_selection.php',
			{
				'target':selectedTargetType,
				'academicYearId':academicYearId,
				'fieldId':'TargetID',
				'fieldName':'TargetID[]'
			},
			function(data){
				$('#selectAllTargetBtn').show();
				$('#DivSelectAllRemark').show();
			}
		);
	}else if(selectedTargetType == 'student'){
		$('#DivRankTargetDetail').html(loadingImg).load(
			'../../common/ajax_load_target_selection.php',
			{
				'target':'student',
				'academicYearId':academicYearId,
				'fieldId':'studentTargetID',
				'fieldName':'studentTargetID[]',
				'onchange':'getStudentSelection()',
				'divStudentSelection':'DivStudentSelection'
			},
			function(data){
				$('#selectAllTargetBtn').hide();
				$('#DivSelectAllRemark').show();
				getStudentSelection();
			}
		);
	}else{
		$('#DivRankTargetDetail').html('');
		$('#selectAllTargetBtn').hide();
		$('#DivSelectAllRemark').hide();
	}
}

function getStudentSelection()
{
	var selectedYearClassId = [];
	var yearClassObj = document.getElementById('studentTargetID');
	for(var i=0;i<yearClassObj.options.length;i++) {
		if(yearClassObj.options[i].selected) {
			selectedYearClassId.push(yearClassObj.options[i].value);
		}
	}
	
	$('#DivStudentSelection').html(loadingImg);
	$.post(
		'../../common/ajax_load_target_selection.php',
		{
			'target':'student2ndLayer',
			'academicYearId':academicYearId,
			'fieldId':'studentTargetID',
			'fieldName':'studentTargetID[]',
			'studentFieldName':'TargetID[]',
			'studentFieldId':'TargetID',
			'YearClassID[]':selectedYearClassId 
		},
		function(data){
			$('#DivStudentSelection').html(data);
		}
	);
}

function process()
{
	if(studentIdAry.length == 0){
		$('#SpinnerContainer').html('');
		if($('#ReportTable tbody').html() == ''){
			$('#ReportTable tbody').html('<tr><td colspan="4" style="text-align:center;"><?=$Lang['General']['NoRecordFound']?></td></tr>');
		}
		$('input').attr('disabled',false);
		$('select').attr('disabled',false);
		$('.content_top_tool').show();
		return;
	}
	
	var student_ids = studentIdAry.splice(0,20);
	var params = {'Task':'process'};
	params['StudentID[]'] = student_ids;
	params['StartDate'] = $('#StartDate').val();
	params['EndDate'] = $('#EndDate').val();
	params['AbsenceDays'] = $('#AbsenceDays').val();
	$.post('ajax_report.php',params,function(returnHtml){
		$('#ReportTable tbody').append(returnHtml);
		setTimeout(function(){
			process();
		},200);
	});
}

function submitForm()
{
	$('#submitBtn').attr('disabled',true);
	var is_valid = true;
	var startdateObj = document.getElementById('StartDate');
	var enddateObj = document.getElementById('EndDate');
	var target_obj = document.getElementById('TargetID');
	var target_values = [];

	if(!check_date_without_return_msg(startdateObj)){
		is_valid = false;
	}
	if(!check_date_without_return_msg(enddateObj)){
		is_valid = false;
	}
	if(startdateObj.value > enddateObj.value){
		is_valid = false;
		$('#Date_Error').show();
	}else{
		$('#Date_Error').hide();
	}

	if(target_obj)
	{
		for(var i=0;i<target_obj.options.length;i++)
		{
			if(target_obj.options[i].selected) target_values.push(target_obj.options[i].value);
		}
	}

	if(target_values.length <= 0)
	{
	 	is_valid = false;
		$('#TargetID_Error').show();
	}else{
		$('#TargetID_Error').hide();
	}
	
	if(!is_valid) $('#submitBtn').attr('disabled',false);
	
	if(is_valid)
	{
		$('#Task').val('prepare');
		var formData = $('#form1').serialize();
		$('input').attr('disabled',true);
		$('select').attr('disabled',true);
		
		$('#ReportLayer').html(loadingImg);
		$.post(
			'ajax_report.php',
			formData,
			function(returnHtml)
			{
				$('#ReportLayer').html(returnHtml);
				studentIdAry = $('#StudentID').val().split(',');
				process();
			}
		);
		
	}
}

function exportReport()
{
	var oldAction = document.form2.action;
	var oldTarget = document.form2.target;
	
	document.form2.action = 'ajax_report.php';
	document.form2.target = '_blank';
	document.form2.submit();
	
	document.form2.action = oldAction;
	document.form2.target = oldTarget;
}

function absenceDaysChanged(obj)
{
	var text_val = $.trim(obj.value);
	var num_val = parseInt(text_val);
	if(isNaN(num_val) || text_val == '' || num_val <= 0){
		obj.value = '7';
		return;
	}
	obj.value = num_val;
}
</script>
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>