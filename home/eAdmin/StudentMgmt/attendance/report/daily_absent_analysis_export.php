<?
//using by 
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();
$lc->retrieveSettings();
if($lc->attendance_mode==0 || $lc->attendance_mode==1)//AM Only or PM Only
{
	header("Location: $PATH_WRT_ROOT/home/eAdmin/StudentMgmt/attendance/");
	intranet_closedb();
	exit();
}
$lexporttext = new libexporttext();

if($TargetDate=='' || strtotime($TargetDate)==-1) $TargetDate=date("Y-m-d");
	
$date_arr = explode('-',$TargetDate);
$txt_year = $date_arr[0];
$txt_month = $date_arr[1];
$txt_day = $date_arr[2];

$CARD_STATUS_EARLY = 4;
$STATUS['--'] = "--";
$STATUS[CARD_STATUS_PRESENT] = $i_StudentAttendance_Status_OnTime;
$STATUS[CARD_STATUS_ABSENT] = $i_StudentAttendance_Status_Absent;
$STATUS[CARD_STATUS_LATE] = $i_StudentAttendance_Status_Late;
$STATUS[CARD_STATUS_OUTING] = $i_StudentAttendance_Status_Outing;
$STATUS[$CARD_STATUS_EARLY] = $i_StudentAttendance_Status_EarlyLeave;

$WAIVED['--'] = '--';
$WAIVED[0] = $Lang['General']['No'];
$WAIVED[1] = $Lang['General']['Yes'];

$lclass = new libclass();
$classnames=array();
for($i=0;$i<sizeof($ClassID);$i++)
{
	$classnames[]="'".$lc->Get_Safe_Sql_Query($lclass->getClassName($ClassID[$i]))."'";
}
$cond_class =" AND u.ClassName IN (".implode(',',$classnames).") ";

if($session==PROFILE_DAY_TYPE_AM)//Find students that only AM are absent
{
	$cond_status = " AND (AMStatus = '".CARD_STATUS_ABSENT."' AND (PMStatus != '".CARD_STATUS_ABSENT."' OR PMStatus IS NULL)) ";
}else if($session==PROFILE_DAY_TYPE_PM)//Find students that only PM are absent
{
	$cond_status = " AND ((AMStatus != '".CARD_STATUS_ABSENT."' OR AMStatus IS NULL) AND PMStatus = '".CARD_STATUS_ABSENT."') ";
}else if($session==PROFILE_DAY_TYPE_WD)// Find students that both AM and PM are absent
{
	$cond_status = " AND (AMStatus = '".CARD_STATUS_ABSENT."' AND PMStatus = '".CARD_STATUS_ABSENT."') ";
}else
{
	$cond_status = " AND (AMStatus = '".CARD_STATUS_ABSENT."' OR PMStatus = '".CARD_STATUS_ABSENT."') ";
}
$lc->createTable_Card_Student_Daily_Log($txt_year,$txt_month);
$card_log_table_name = "CARD_STUDENT_DAILY_LOG_".$txt_year."_".$txt_month;
$namefield = getNameFieldByLang('u.');
/*
$sql="SELECT
			u.UserID,
			u.ClassName,
			u.ClassNumber,
			$namefield as StudentName,
			IF(c.LeaveStatus = '".CARD_LEAVE_AM."',$CARD_STATUS_EARLY,IF(c.AMStatus IS NULL,'--',c.AMStatus)) as AMStatus,
			IF(c.LeaveStatus = '".CARD_LEAVE_PM."',$CARD_STATUS_EARLY,IF(c.PMStatus IS NULL,'--',c.PMStatus)) as PMStatus,
			IF(ram.Reason IS NULL OR ram.Reason='', '--', ram.Reason) as AMReason,
			IF(rpm.Reason IS NULL OR rpm.Reason='', '--', rpm.Reason) as PMReason,
			IF(pam.RecordStatus IS NULL, '--', pam.RecordStatus) as AMWaive,
			IF(ppm.RecordStatus IS NULL, '--', ppm.RecordStatus) as PMWaive,
			IF(c.AMStatus = '".CARD_STATUS_ABSENT."',IF(ram.AbsentSession IS NULL, '--', ram.AbsentSession),'--') as AMAbsentSession,
			IF(c.PMStatus = '".CARD_STATUS_ABSENT."',IF(rpm.AbsentSession IS NULL, '--', rpm.AbsentSession),'--') as PMAbsentSession
	  FROM
			INTRANET_USER AS u
			INNER JOIN $card_log_table_name AS c ON c.UserID = u.UserID AND c.DayNumber = '$txt_day' 
					   $cond_status 
			LEFT JOIN PROFILE_STUDENT_ATTENDANCE as pam ON pam.UserID = c.UserID 
						AND pam.RecordType IN ('".CARD_STATUS_ABSENT."', '".CARD_STATUS_LATE."')
						AND pam.AttendanceDate LIKE '$TargetDate%' 
						AND pam.DayType = '".PROFILE_DAY_TYPE_AM."'
			LEFT JOIN PROFILE_STUDENT_ATTENDANCE as ppm ON ppm.UserID = c.UserID 
						AND ppm.RecordType IN ('".CARD_STATUS_ABSENT."', '".CARD_STATUS_LATE."') 
						AND ppm.AttendanceDate LIKE '$TargetDate%' 
						AND ppm.DayType = '".PROFILE_DAY_TYPE_PM."'
			LEFT JOIN CARD_STUDENT_PROFILE_RECORD_REASON as ram ON ram.ProfileRecordID = pam.StudentAttendanceID 
						AND ram.StudentID = pam.UserID
						AND ram.RecordDate = '$TargetDate' 
						AND ram.DayType = '".PROFILE_DAY_TYPE_AM."' 
			LEFT JOIN CARD_STUDENT_PROFILE_RECORD_REASON as rpm ON rpm.ProfileRecordID = ppm.StudentAttendanceID 
						AND rpm.StudentID = ppm.UserID
						AND rpm.RecordDate = '$TargetDate' 
						AND rpm.DayType = '".PROFILE_DAY_TYPE_PM."' 
	  WHERE
			u.RecordType=2 AND u.RecordStatus IN (0,1,2) 
			$cond_class
	  ORDER BY
			u.ClassName, u.ClassNumber ";
*/
$sql="SELECT
			u.UserID,
			u.ClassName,
			u.ClassNumber,
			$namefield as StudentName,
			(CASE 
				WHEN c.AMStatus IS NOT NULL AND c.AMStatus = '".CARD_STATUS_PRESENT."' THEN '".CARD_STATUS_PRESENT."'
				WHEN c.AMStatus IS NOT NULL AND c.AMStatus = '".CARD_STATUS_OUTING."' THEN '".CARD_STATUS_OUTING."' 
				WHEN ram.RecordID IS NOT NULL AND ram.RecordType = '".PROFILE_TYPE_ABSENT."' THEN '".CARD_STATUS_ABSENT."'
				WHEN ram.RecordID IS NOT NULL AND ram.RecordType = '".PROFILE_TYPE_LATE."' THEN '".CARD_STATUS_LATE."'
				WHEN ram.RecordID IS NOT NULL AND ram.RecordType = '".PROFILE_TYPE_EARLY."' THEN '".$CARD_STATUS_EARLY."' 
				WHEN c.LeaveStatus = '".CARD_LEAVE_AM."' THEN '".$CARD_STATUS_EARLY."' 
				WHEN c.AMStatus IS NOT NULL AND c.AMStatus <> '' THEN c.AMStatus
				ELSE '--' 
			END) as AMStatus,
			(CASE 
				WHEN c.PMStatus IS NOT NULL AND c.PMStatus = '".CARD_STATUS_PRESENT."' THEN '".CARD_STATUS_PRESENT."'
				WHEN c.PMStatus IS NOT NULL AND c.PMStatus = '".CARD_STATUS_OUTING."' THEN '".CARD_STATUS_OUTING."' 
				WHEN rpm.RecordID IS NOT NULL AND rpm.RecordType = '".PROFILE_TYPE_ABSENT."' THEN '".CARD_STATUS_ABSENT."'
				WHEN rpm.RecordID IS NOT NULL AND rpm.RecordType = '".PROFILE_TYPE_LATE."' THEN '".CARD_STATUS_LATE."'
				WHEN rpm.RecordID IS NOT NULL AND rpm.RecordType = '".PROFILE_TYPE_EARLY."' THEN '".$CARD_STATUS_EARLY."' 
				WHEN c.LeaveStatus = '".CARD_LEAVE_PM."' THEN '".$CARD_STATUS_EARLY."' 
				WHEN c.PMStatus IS NOT NULL AND c.PMStatus <> '' THEN c.PMStatus 
				ELSE '--' 
			END) as PMStatus,
			IF(ram.Reason IS NULL OR ram.Reason='', '--', ram.Reason) as AMReason,
			IF(rpm.Reason IS NULL OR rpm.Reason='', '--', rpm.Reason) as PMReason,
			IF(ram.RecordStatus IS NULL, '--', ram.RecordStatus) as AMWaive,
			IF(rpm.RecordStatus IS NULL, '--', rpm.RecordStatus) as PMWaive,
			IF(c.AMStatus = '".CARD_STATUS_ABSENT."',IF(ram.AbsentSession IS NULL, '--', ram.AbsentSession),'--') as AMAbsentSession,
			IF(c.PMStatus = '".CARD_STATUS_ABSENT."',IF(rpm.AbsentSession IS NULL, '--', rpm.AbsentSession),'--') as PMAbsentSession
	  FROM
			INTRANET_USER AS u
			INNER JOIN $card_log_table_name AS c ON c.UserID = u.UserID AND c.DayNumber = '$txt_day' 
					   $cond_status 
			LEFT JOIN CARD_STUDENT_PROFILE_RECORD_REASON as ram ON ram.StudentID = c.UserID
						AND ram.RecordDate = '$TargetDate' 
						AND ram.DayType = '".PROFILE_DAY_TYPE_AM."' 
			LEFT JOIN CARD_STUDENT_PROFILE_RECORD_REASON as rpm ON rpm.StudentID = c.UserID
						AND rpm.RecordDate = '$TargetDate' 
						AND rpm.DayType = '".PROFILE_DAY_TYPE_PM."' 
	  WHERE
			u.RecordType=2 AND u.RecordStatus IN (0,1,2) 
			$cond_class
	  ORDER BY
			u.ClassName, u.ClassNumber ";
$result = $lc->returnArray($sql);

$exportContent = array();

$exportRow[] = $i_ClassName;
$exportRow[] = $i_ClassNumber;
$exportRow[] = $i_UserStudentName;
$exportRow[] = $i_DayTypeAM." ".$i_StudentAttendance_Status;
if($sys_custom['SmartCardAttendance_StudentAbsentSession'])
{
	$exportRow[] = $i_DayTypeAM." ".$Lang['StudentAttendance']['AbsentSessions'];
}
$exportRow[] = $i_DayTypeAM." ".$i_SmartCard_Frontend_Take_Attendance_Waived;
$exportRow[] = $i_DayTypeAM." ".$i_Attendance_Reason;
$exportRow[] = $i_DayTypePM." ".$i_StudentAttendance_Status;
if($sys_custom['SmartCardAttendance_StudentAbsentSession'])
{
	$exportRow[] = $i_DayTypePM." ".$Lang['StudentAttendance']['AbsentSessions'];
}
$exportRow[] = $i_DayTypePM." ".$i_SmartCard_Frontend_Take_Attendance_Waived;
$exportRow[] = $i_DayTypePM." ".$i_Attendance_Reason;
$exportRow[] = $i_StudentGuardian['MenuInfo'];
$exportContent[] = $exportRow;

$headerColumn = array();
$headerColumn[] = $Lang['StudentAttendance']['DailyAbsentAnalysisReport']." ($TargetDate)";

if(sizeof($result)==0)
{
	$exportRow = array();
	$exportRow[] = $i_no_record_exists_msg;
	$exportContent[] = $exportRow;
}

for($i=0;$i<sizeof($result);$i++)
{
	list($user_id,$class_name,$class_number,$student_name,$am_status,$pm_status,$am_reason,$pm_reason,$am_waive,$pm_waive,$am_absentsession,$pm_absentsession) = $result[$i];
	
	$exportRow = array();
	
	$guardian_row = "";
	// Get guardian information
	$main_content_field = "IF(EnName IS NOT NULL AND EnName !='' AND ChName IS NOT NULL AND ChName !='', IF(IsMain = 1, CONCAT('* ', EnName,' / ',ChName), CONCAT(EnName,' / ',ChName)), IF(EnName IS NOT NULL AND EnName!='',EnName,ChName))";
	$main_content_field = "IF($main_content_field='' OR $main_content_field IS NULL,'-',$main_content_field)";
	$sql = "SELECT
			  $main_content_field,
			  Relation,
			  Phone,
			  EmPhone,
			  IsMain
			FROM
			  $eclass_db.GUARDIAN_STUDENT
			WHERE
			  UserID = $user_id
			ORDER BY
			  IsMain DESC, Relation ASC
			";
	$guardian_result = $lc->returnArray($sql,5);
	if (sizeof($guardian_result)==0)
		$guardian_row .= "--";
	else
	{
	    for($j=0; $j<sizeof($guardian_result); $j++)
	    {
			list($name, $relation, $phone, $em_phone) = $guardian_result[$j];
			$no = $j+1;
			$guardian_row .= "$name ($ec_guardian[$relation]) ($i_StudentGuardian_Phone) $phone ($i_StudentGuardian_EMPhone) $em_phone\n";
	  	}
	}
	
	$exportRow[] = $class_name;
	$exportRow[] = $class_number;
	$exportRow[] = $student_name;
	$exportRow[] = $STATUS[$am_status];
	if($sys_custom['SmartCardAttendance_StudentAbsentSession']) $exportRow[] = $am_absentsession;
	$exportRow[] = $WAIVED[$am_waive];
	$exportRow[] = $am_reason;
	$exportRow[] = $STATUS[$pm_status];
	if($sys_custom['SmartCardAttendance_StudentAbsentSession']) $exportRow[] = $pm_absentsession;
	$exportRow[] = $WAIVED[$pm_waive];
	$exportRow[] = $pm_reason;
	$exportRow[] = $guardian_row;
	
	$exportContent[] = $exportRow;
}

$CSVFileName = str_replace(' ','_', $Lang['StudentAttendance']['DailyAbsentAnalysisReport'])."_".$TargetDate.".csv";
$export_data = $lexporttext->GET_EXPORT_TXT($exportContent, $headerColumn);
$lexporttext->EXPORT_FILE($CSVFileName, $export_data);

intranet_closedb();
?>