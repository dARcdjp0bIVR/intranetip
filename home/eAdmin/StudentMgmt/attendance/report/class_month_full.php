<?
//editing by Paul
####################################### Change Log #################################################
# 
# 2012-12-28 by Carlos: Added continuous absent day option
# 2011-12-19 by Carlos: Added option StudentStatus
# 2010-05-06 by Carlos: Add Late Session Statistic and Late session reason statistic
# 2010-02-05 by Carlos: Add show main data distribution option
#
####################################################################################################
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libstudentattendance_ui.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();
$StudentAttendUI = new libstudentattendance_ui();
$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageReport_ClassMonth";

$linterface = new interface_html();


# Get Classes List
/*$classes = $lc->getClassList();
$select_class = getSelectByArray($classes,"name=\"ClassID\"","",0,1);*/

# Get Classes List
$button_select = $i_status_all;
$select_class = $lc->getSelectClassID("name=ClassID","");

# Get Year List
$years = $lc->getRecordYear();
$select_year = getSelectByValue($years, "name=\"Year\"",date('Y'),0,1);

# Month List
$months = $i_general_MonthShortForm;
$select_month = "<select name=\"Month\">";
$currMon = date('n')-1;
for ($i=0; $i<sizeof($months); $i++)
{
     $month_name = $months[$i];
     $string_selected = ($currMon==$i? "SELECTED":"");
     $select_month .= "<OPTION value=".($i+1)." $string_selected>".$month_name."</OPTION>\n";
}
$select_month .= "</SELECT>\n";


$format_array = array(
                      array(0,"Web"),
                      array(1,"Excel"));
$select_format = getSelectByArray($format_array, "name=\"format\"",0,0,1);

$TAGS_OBJ[] = array($i_StudentAttendance_Report_ClassMonth, "", 0);

$MODULE_OBJ = $lc->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

?>
<script LANGUAGE="Javascript">

function submitForm(obj){
	obj.action = 'class_month_full_report.php';
	// auto correct
	var absent_day = parseInt(document.getElementById('AbsentDay').value.Trim());
	if(!is_positive_int(absent_day)) {
		document.getElementById('AbsentDay').value = "0";
	}else{
		document.getElementById('AbsentDay').value = absent_day;
	}
	
	if(obj.format.value=='0') {
		obj.target='intranet_popup10';
    	newWindow('about:blank', 10);
		obj.submit();
	} else {
		obj.target='_blank';
		obj.submit();
	}
}

function Get_Class_Selection() {
	$('#ClassListCell').load('ajax_get_class_list_by_academic_year.php',{AcademicYear:$('#AcademicYear').val()},
		function (data) {
			if (data == "die") 
				window.top.location = "/";
		});
}




function Use_Month_Year(){
	$('tr#RowStartDate').hide();
	$('tr#RowEndDate').hide();
	$('tr#RowSelectYear').show();
	$('tr#RowSelectMonth').show();
}


function Use_Date_Range(){
	$('tr#RowSelectYear').hide();
	$('tr#RowSelectMonth').hide();
	$('tr#RowStartDate').show();
	$('tr#RowEndDate').show();
}

function checkAllColumn() {
    $("#OptionsLayer input[type='checkbox']").attr("checked",true);
    $("#OptionsLayer input[type='checkbox']").val('1');
}

</script>
<br />
<form name="form1" action="" method="GET">
<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$Lang['StudentAttendance']['ClassMemberAsInAcademicYear']?></td>
		<td width="70%" class="tabletext">
			<?=$StudentAttendUI->Get_Academic_Year_Selection_List("AcademicYear","AcademicYear","Get_Class_Selection();")?>
		</td>
	</tr>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?="$i_ClassName"?></td>
		<td width="70%" class="tabletext" id="ClassListCell">
			<?=$select_class?>
		</td>
	</tr>

 


	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$Lang['StudentAttendance']['UserDateRange']?></td>
		<td width="70%" class="tabletext">
			<label for="useDateRangeRadio1"><input type="radio" id="useDateRangeRadio1" name="UseDateRange" value="1" onclick="Use_Date_Range()" <?=($UseDateRange==1?"checked":"")?>><?=$i_general_yes?></label>
			&nbsp;&nbsp;
			<label for="useDateRangeRadio2"><input type="radio" id="useDateRangeRadio2" name="UseDateRange" value="0" onclick="Use_Month_Year()" <?=($UseDateRange!=1?"checked":"")?>><?=$i_general_no?></label>
		</td>
	</tr>
	
 

	<tr id="RowStartDate" <?=($UseDateRange==1?'':'style="display:none;"')?>>
		<td  id="dateRange1" width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_general_startdate?></td>
		<td  id="dateRange2" width="70%" class="tabletext">
			<?=$linterface->GET_DATE_PICKER("StartDate", $startDate)?>
		</td>
	</tr>
	<tr id="RowEndDate" <?=($UseDateRange==1?'':'style="display:none;"')?>>
		<td  id="dateRange3" width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_general_enddate?></td>
		<td  id="dateRange4" width="70%" class="tabletext">
			<?=$linterface->GET_DATE_PICKER("EndDate", $endDate)?>
		</td>
	</tr>
	
	
	<tr id="RowSelectYear" <?=($UseDateRange!=1?'':'style="display:none;"')?>>
		<td id="UseYear1" width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_general_Year?></td>
		<td id="UseYear2" width="70%" class="tabletext">
			<?=$select_year?>
		</td>
	</tr>
	<tr id="RowSelectMonth" <?=($UseDateRange!=1?'':'style="display:none;"')?>>
		<td id="UseMonth1" width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_general_Month?></td>
		<td id="UseMonth2" width="70%" class="tabletext">
			<?=$select_month?>
		</td>
	</tr>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_general_Format?></td>
		<td width="70%" class="tabletext">
			<?=$select_format?>
		</td>
	</tr>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$Lang['StudentAttendance']['FilterNoDataDate']?></td>
		<td width="70%" class="tabletext">
			<input type="checkbox" name="HideNoData" id="HideNoData" value="1">
		</td>
	</tr>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$Lang['StudentAttendance']['StudentStatus']?></td>
		<td width="70%" class="tabletext">
            <input type="radio" id="StudentStatusAll" name="StudentStatus" value="0,1,2,3" ><label for="StudentStatusAll"><?=$Lang['General']['All']?></label>
			&nbsp;
			<input type="radio" id="StudentStatusActive" name="StudentStatus" value="0,1,2" checked="checked"><label for="StudentStatusActive"><?=$Lang['Status']['Active'].' + '.$Lang['Status']['Suspended']?></label></td>
	</tr>
	<?php
	echo '<tr>'."\n";
	echo '<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext">'."\n";
	echo str_replace('<!--NUMBER-->','</td><td width="70%" class="tabletext"><input type="text" class="textboxnum" name="AbsentDay" id="AbsentDay" value="0" maxlength="5">',$Lang['StudentAttendance']['DisplayInRedForContinuousAbsent'])."\n";
	echo '&nbsp;<span class="tabletextremark">('.$Lang['StudentAttendance']['ZeroToDisable'].')</span>'."\n";
	echo '</td>'."\n";
	echo '</tr>'."\n";
	?>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$Lang['SmartCard']['StaffAttendence']['ShowAllColumns']?></td>
		<td width="70%" class="tabletext">
			<input type="radio" id="ShowAllColumnsYes" name="ShowAllColumns" value="1" onclick="document.getElementById('OptionsLayer').style.display='none';this.checked=true;" <?if($ShowAllColumns=="" || $ShowAllColumns=="1"){echo "checked";}else{echo "";}?>><label for="ShowAllColumnsYes"><?=$i_general_yes?></label>
			&nbsp;
			<input type="radio" id="ShowAllColumnsNo" name="ShowAllColumns" value="0" onclick="document.getElementById('OptionsLayer').style.display='block';this.checked=true;checkAllColumn();" <?if($ShowAllColumns=="0"){echo "checked";}else{echo "";}?>><label for="ShowAllColumnsNo"><?=$i_general_no?></label></td>
	</tr>
	<?
	if($ShowAllColumns == "0")
		$OptionsLayerVisibility = "style='display:block;'";
	else
		$OptionsLayerVisibility = "style='display:none;'";
	?>
	<tr>
		<td width="30%">&nbsp;</td>
		<td width="70%" class="tabletext">
		<span id="OptionsLayer" <?=$OptionsLayerVisibility?>>
			<input type="checkbox" id="ColumnChineseName" name="ColumnChineseName" value="0" onclick="if(this.value=='1'){this.checked=false;this.value='0';}else{this.checked=true;this.value='1';}"><label for="ColumnChineseName"><?=$i_UserChineseName?></label><br />
			<input type="checkbox" id="ColumnEnglishName" name="ColumnEnglishName" value="0" onclick="if(this.value=='1'){this.checked=false;this.value='0';}else{this.checked=true;this.value='1';}"><label for="ColumnEnglishName"><?=$i_UserEnglishName?></label><br />
			<input type="checkbox" id="ColumnGender" name="ColumnGender" value="0" onclick="if(this.value=='1'){this.checked=false;this.value='0';}else{this.checked=true;this.value='1';}"><label for="ColumnGender"><?=$i_UserGender?></label><br />
			<input type="checkbox" id="ColumnData" name="ColumnData" value="0" onclick="if(this.value=='1'){this.checked=false;this.value='0';}else{this.checked=true;this.value='1';}"><label for="ColumnData"><?=$Lang['StudentAttendance']['DataDistribution']?></label><br />
			<input type="checkbox" id="ColumnDailyStat" name="ColumnDailyStat" value="0" onclick="if(this.value=='1'){this.checked=false;this.value='0';}else{this.checked=true;this.value='1';}"><label for="ColumnDailyStat"><?=$i_StudentAttendance_Daily_Stat?></label><br />
			<input type="checkbox" id="ColumnSchoolDays" name="ColumnSchoolDays" value="0" onclick="if(this.value=='1'){this.checked=false;this.value='0';}else{this.checked=true;this.value='1';}"><label for="ColumnSchoolDays"><?=$Lang['SmartCard']['StudentAttendence']['SchoolDaysStat']?></label><br />
			<input type="checkbox" id="ColumnMonthlyStat" name="ColumnMonthlyStat" value="0" onclick="if(this.value=='1'){this.checked=false;this.value='0';}else{this.checked=true;this.value='1';}"><label for="ColumnMonthlyStat"><?=$i_StudentAttendance_Monthly_Level_Stat?></label><br />
			<input type="checkbox" id="ColumnRemark" name="ColumnRemark" value="0" onclick="if(this.value=='1'){this.checked=false;this.value='0';}else{this.checked=true;this.value='1';}"><label for="ColumnRemark"><?=$i_Discipline_System_general_remark?></label><br />
			<input type="checkbox" id="ColumnReasonStat" name="ColumnReasonStat" value="0" onclick="if(this.value=='1'){this.checked=false;this.value='0';}else{this.checked=true;this.value='1';}"><label for="ColumnReasonStat"><?=$Lang['SmartCard']['StudentAttendance']['CustomizedReasonStat']?></label><br />
			<?if($sys_custom['SmartCardAttendance_StudentAbsentSession'] ){?>
			<input type="checkbox" id="ColumnSession" name="ColumnSession" value="0" onclick="if(this.value=='1'){this.checked=false;this.value='0';}else{this.checked=true;this.value='1';}"><label for="ColumnSession"><?=$Lang['StudentAttendance']['SessionsStat']?></label><br />
			<?}?>
			<?if($platform =="KIS"){?>
			<input type="checkbox" id="ColumnBirthDate" name="ColumnBirthDate" value="0" onclick="if(this.value=='1'){this.checked=false;this.value='0';}else{this.checked=true;this.value='1';}"><label for="ColumnBirthDate"><?=$Lang['StudentRegistry']['BirthDate']?></label><br />
			<input type="checkbox" id="ColumnEntryDate" name="ColumnEntryDate" value="0" onclick="if(this.value=='1'){this.checked=false;this.value='0';}else{this.checked=true;this.value='1';}"><label for="ColumnEntryDate"><?=$Lang['StudentRegistry']['EntryDate']?></label><br />
			<?}?>
			<input type="checkbox" id="ColumnNonSchoolDayReason" name="ColumnNonSchoolDayReason" value="0" onclick="if(this.value=='1'){this.checked=false;this.value='0';}else{this.checked=true;this.value='1';}"><label for="ColumnNonSchoolDayReason"><?=$Lang['StudentAttendance']['NonSchoolDayReason']?></label><br />
		</span>
		</td>
	</tr>
</table>
<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
    	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
    </tr>
</table>
<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
	    <td align="center" colspan="2">
			<?= $linterface->GET_ACTION_BTN($button_submit, "button", "submitForm(document.form1);", "submit2") ?>
			<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2"," class='formbutton' onclick=\"document.getElementById('OptionsLayer').style.display='none';\" onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
		</td>
	</tr>
</table>
</form>
<br />
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>