<?php
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();
$hasAM = ($lc->attendance_mode != 1);
$hasLunch = ($lc->attendance_mode != 0 && $lc->attendance_mode != 1);
if ($hasLunch) {
	$hasLunchOut = !($lc->NoRecordLunchOut == 1);
}
$hasPM = ($lc->attendance_mode != 0);
$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageReport_NoTappingCard";
$linterface = new interface_html();

$select_class = $lc->getSelectClass('name="class_name[]" id="class_name[]" multiple size="10" class="class_list" style="min-width:150px; width:200px;"',$ClassName,1);

# date range
$current_month=date('n');
$current_year =date('Y');
if($current_month>=9){
        $startDate = date('Y-m-d',mktime(0, 0, 0, 9, 1, $current_year));
        //$endDate   = date('Y-m-d',mktime(0, 0, 0, 8, 31,$current_year+1));
}else{
        $startDate = date('Y-m-d',mktime(0, 0, 0, 9, 1, $current_year-1));
        //$endDate   = date('Y-m-d',mktime(0, 0, 0, 8, 31,$current_year));
}
$endDate=date('Y-m-d');

$TAGS_OBJ[] = array($i_StudentAttendance_Report_NoCardTab, "", 0);

$MODULE_OBJ = $lc->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();
?>
<script language="javascript">
</script>
<style type="text/css">
.class_list{width:100px; height=150px;}
</style>
<link rel="stylesheet" href="/templates/calendar/dynCalendar.css" type="text/css" media="screen">
<script LANGUAGE="javascript">
        var css_array = new Array;
        css_array[0] = "dynCalendar_free";
        css_array[1] = "dynCalendar_half";
        css_array[2] = "dynCalendar_full";

        var date_array = new Array;
</script>

<script src="/templates/calendar/browserSniffer.js" type="text/javascript" language="javascript"></script>
<script src="/templates/calendar/dynCalendar.js" type="text/javascript" language="javascript"></script>
<script type="text/javascript">
<!--
// Calendar callback. When a date is clicked on the calendar
// this function is called so you can do as you want with it
function calendarCallback(date, month, year)
{
               if (String(month).length == 1) {
                       month = '0' + month;
               }

               if (String(date).length == 1) {
                       date = '0' + date;
               }
               dateValue =year + '-' + month + '-' + date;
               document.forms['form1'].startStr.value = dateValue;
}
// Calendar callback. When a date is clicked on the calendar
// this function is called so you can do as you want with it
function calendarCallback2(date, month, year)
{
               if (String(month).length == 1) {
                       month = '0' + month;
               }

               if (String(date).length == 1) {
                       date = '0' + date;
               }
               dateValue =year + '-' + month + '-' + date;
               document.forms['form1'].endStr.value = dateValue;
}
function isValidDate(obj){
                               if(!check_date(obj,"<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>")) return false;
                               return true;
}

function checkform(formObj){
	if(formObj==null)return false;
	if(formObj.startStr==null || formObj.endStr==null) return false;
	if(!isValidDate(formObj.startStr) || !isValidDate(formObj.endStr)) return false;
	if(formObj.startStr.value>formObj.endStr.value){
		alert('<?=$i_con_msg_date_startend_wrong_alert?>');
		return false;
	}
	if (!formObj.AM.checked && !formObj.LunchOut.checked && !formObj.PM.checked && !formObj.slot_out.checked)
	{
	  alert('<?="$i_alert_pleaseselect $i_StudentAttendance_InSchool / $i_StudentAttendance_LeaveSchool"?>');
	  return false;
	}
	
	ClassSelected=false;
	ClassSelection = document.getElementById('class_name[]');
	for(i=0;i<ClassSelection.options.length;i++){
		if(ClassSelection.options[i].selected) {
			ClassSelected = true;
			break;
		}
	}
	
	if(!ClassSelected){
		alert('<?=$i_Discipline_System_alert_PleaseSelectClass?>');
		return false;
	}
	
	// OK
	obj2 = document.getElementById("format_csv");
	if (obj2==null)
	{
	}
	else
	{
		if (obj2.checked)
		{
			formObj.target = "";
		}
		else
		{
			formObj.target = "_blank";
		}
	}
	return true;
}

function checkDetails(formObj)
{
       if (formObj.targetType[0].checked)
       {
           formObj.details.disabled = true;
       }
       else
       {
           formObj.details.disabled = false;
       }
}

// -->
</script>
<br>
<form name=form1 action='nocard_result.php' method=POST target="_blank" onSubmit="return checkform(this)">
<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_general_startdate?></td>
		<td width="70%"class="tabletext">
			<?=$linterface->GET_DATE_PICKER('startStr',$startDate)?>
			&nbsp;<span class=staff_extraInfo>(yyyy-mm-dd)
		</td>
	</tr>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_general_enddate?></td>
		<td width="70%"class="tabletext">
			<?=$linterface->GET_DATE_PICKER('endStr',$endDate)?>
			&nbsp;<span class=staff_extraInfo>(yyyy-mm-dd)
		</td>
	</tr>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?="$button_select $i_ClassName"?></td>
		<td width="70%"class="tabletext">
			<?=$select_class?>
			<?=$linterface->GET_SMALL_BTN("Select/ Deselect All","button","var SelectedAlready = false; var SelectObj = document.getElementById('class_name[]'); for (var i=0; i< SelectObj.length; i++) {if (SelectObj.options[i].selected) {SelectedAlready = true;}} for (var i=0; i< SelectObj.length; i++) {SelectObj.options[i].selected = !(SelectedAlready);}");?>
		</td>
	</tr>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_general_target?></td>
		<td width="70%"class="tabletext">
			<input id="target_class" type="radio" name="targetType" value="Class" CHECKED onClick="checkDetails(this.form)"><label for="target_class"><?=$i_general_class?></label>
			<input id="target_student" type="radio" name="targetType" value="Student" onClick="checkDetails(this.form)"><label for="target_student"><?=$i_identity_student?></label>
		</td>
	</tr>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_general_orderby?></td>
		<td width="70%"class="tabletext">
			<input id="orderby_class" type="radio" name="orderby" value="name" CHECKED><label for="orderby_class"><?=$i_general_orderby_class?></label>
			<input id="orderby_count" type="radio" name="orderby" value="count"><label for="orderby_count"><?=$i_general_count?></label>
		</td>
	</tr>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_general_Format?></td>
		<td width="70%"class="tabletext">
			<input id="format_print" type="radio" name="format" value="print" CHECKED><label for="format_print"><?=$i_PrinterFriendlyPage?></label>
			<input id="format_csv" type="radio" name="format" value="csv"><label for="format_csv">CSV</label>
		</td>
	</tr>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_StudentAttendance_InSchool?> / <?=$i_StudentAttendance_LeaveSchool?></td>
		<td width="70%"class="tabletext">
<?
			if ($hasAM) {
?>
				<input id="AM" type="checkbox" name="AM" value="1" CHECKED><label for="AM"><?=$Lang['StudentAttendance']['DayTypeAM']?></label>
<?
			}
			if ($hasLunchOut) {
?>
				<input id="LunchOut" type="checkbox" name="LunchOut" value="1" CHECKED><label for="LunchOut"><?=$Lang['StudentAttendance']['LunchOut']?></label>
<?
			}
			if ($hasPM) {
?>
				<input id="PM" type="checkbox" name="PM" value="1" CHECKED><label for="PM"><?=$Lang['StudentAttendance']['DayTypePM']?></label>
<?
			}
?>
			<input id="slot_out" type="checkbox" name="slot_out" value="1" CHECKED><label for="slot_out"><?=$i_StudentAttendance_LeaveSchool?></label>
			<input id="details" type="checkbox" name="details" value=1 disabled><label for="details"><?=$i_general_display_details?></label>
		</td>
	</tr>
</table>

<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
    	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
    </tr>
</table>
<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
	    <td align="center" colspan="2">
			<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "", "submit2") ?>
			<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
		</td>
	</tr>
</table>
</form>
<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>