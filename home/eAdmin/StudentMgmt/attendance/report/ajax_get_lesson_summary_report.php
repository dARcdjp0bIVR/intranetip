<?php
# Using: Henry Chan

################################################# Change log ####################################################
# 2013-08-06 by Henry: add one more parameter of Get_Lesson_Summary_Report to support attendance filtering
#################################################################################################################
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libstudentattendance_ui.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-LessonAttendance"] || !$_SESSION["SSV_PRIVILEGE"]["plugin"]["attendancelesson"]) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$FromDate = $_REQUEST['FromDate'];
$ToDate = $_REQUEST['ToDate'];
$ClassID = $_REQUEST['ClassID'];
$AttendStatus = $_REQUEST['AttendStatus'];
$StudentID = $_REQUEST['StudentID'];

//if($StudentID == -1)
//	$StudentID = "";

$linterface = new interface_html();

$LessonAttendUI = new libstudentattendance_ui();

echo $LessonAttendUI->Get_Lesson_Summary_Report($FromDate,$ToDate,$ClassID,"web",$AttendStatus, $StudentID);

intranet_closedb();
?>