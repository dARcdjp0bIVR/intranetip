<?php
//editing by 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();
/*
if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent'])
{
	header ("Location: /");
	intranet_closedb();
	exit();
}
*/
$lc = new libcardstudentattend2();
$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageDailyOperation_ImportAttendanceData";

$linterface = new interface_html();
$li = new libdb();
$limport = new libimporttext();

$filepath = $userfile;
$filename = $userfile_name;

if($filepath=="none" || $filepath == "")
{
	# import failed
	$Msg = $Lang['StudentAttendance']['OfflineRecordImportFail'];
  	header("Location: import.php?Msg=".urlencode($Msg));
}else
{
    if($limport->CHECK_FILE_EXT($filename))
    {
        # read file into array
        # return 0 if fail, return csv array if success
        $data = $limport->GET_IMPORT_TXT($filepath);
        array_shift($data); # drop the title bar
    }else
    {
        header("Location: import.php?msg=5");
    }
    // Create temp table for storing records
    $sql = "DROP TABLE TEMP_CARD_STUDENT_DAILY_LOG";
    $li->db_db_query($sql);
    $sql = "CREATE TABLE TEMP_CARD_STUDENT_DAILY_LOG (
             UserID int(11),
			 ClassName varchar(255),
             ClassNumber varchar(100),
             DayNumber int(11),
			 RecordDate date,
             AMInStatus int(11),
			 AMInTime time,
			 AMInWaive int(11),
			 AMInReason mediumtext,
			 AMOutStatus int(11),
			 AMOutWaive int(11),
			 AMOutReason mediumtext,
			 PMInStatus int(11),
			 PMInTime time,
			 PMInWaive int(11),
			 PMInReason mediumtext,
			 PMOutStatus int(11),
			 PMOutWaive int(11),
			 PMOutReason mediumtext
            ) ENGINE=InnoDB CHARSET=utf8";
    $li->db_db_query($sql);
	
    # Get Student data
    # ClassName, ClassNumber -> UserID
    $sql = "SELECT UserID, ClassName, ClassNumber 
			FROM INTRANET_USER
            WHERE RecordType = 2 AND RecordStatus IN (0,1)
            	  AND ClassName != '' AND ClassNumber != '' ";
    $temp = $li->returnArray($sql,3);
    for ($i=0; $i<sizeof($temp); $i++)
    {
         list($targetUserID, $targetClass, $targetNum) = $temp[$i];
         $students[$targetClass][$targetNum] = $targetUserID;
    }
    
    $error_array = array();// store error messages
    $values = "";
    $delim = "";
    for ($i=0; $i<sizeof($data); $i++)
    {
         list($Class,$ClassNum,$Date,
         	  $AMInStatus,$AMInTime,$AMInWaive,$AMInReason,
			  $AMOutStatus,$AMOutWaive,$AMOutReason,
			  $PMInStatus,$PMInTime,$PMInWaive,$PMInReason,
			  $PMOutStatus,$PMOutWaive,$PMOutReason) = $data[$i];
		 $AMInReason = trim($AMInReason);
		 $AMOutReason = trim($AMOutReason);
		 $PMInReason = trim($PMInReason);
		 $PMOutReason = trim($PMOutReason);
		 // Check errors. If has error, skip the current record
		 $HasError = false;
		 $rowi = $i + 2; //offset 2 since 0 based & row 1 is header
		 if($Class=="")
		 {
		 	$HasError = true;
		 	if($intranet_session_language=="b5")
		 		$error_array[] = "資料行$rowi: 沒有班別";
		 	else
		 		$error_array[] = "Data Row $rowi doesn't have Class Name";
		 }
		 if($ClassNum=="")
		 {
		 	$HasError = true;
		 	if($intranet_session_language=="b5")
		 		$error_array[] = "資料行$rowi: 沒有班號";
		 	else
		 		$error_array[] = "Data Row $rowi doesn't have Class Number";
		 }
		 if($Date=="" || preg_match('/^\d\d\d\d-\d\d-\d\d$/',$Date)==0)
		 {
		 	$HasError = true;
		 	if($intranet_session_language=="b5")
		 		$error_array[] = "資料行$rowi: 沒有日期";
		 	else
		 		$error_array[] = "Data Row $rowi doesn't have valid date";
		 }
		 if(trim($AMInStatus)!="" && (!is_numeric($AMInStatus) || intval($AMInStatus)>=3))
		 {
		 	$HasError = true;
		 	if($intranet_session_language=="b5")
		 		$error_array[] = "資料行$rowi: 上午進入狀況不合理";
		 	else
		 		$error_array[] = "AMInStatus of data Row $rowi is not valid";
		 }
		 if(trim($AMOutStatus)!="" && intval($AMOutStatus)!=3)
		 {
		 	$HasError = true;
		 	if($intranet_session_language=="b5")
		 		$error_array[] = "資料行$rowi: 上午離開狀況不合理";
		 	else
		 		$error_array[] = "AMOutStatus of data Row $rowi is not valid";
		 }
		 if(trim($PMInStatus)!="" && (!is_numeric($PMInStatus) || intval($PMInStatus)>=3))
		 {
		 	$HasError = true;
		 	if($intranet_session_language=="b5")
		 		$error_array[] = "資料行$rowi: 下午進入狀況不合理";
		 	else
		 		$error_array[] = "PMInStatus of data Row $rowi is not valid";
		 }
		 if(trim($PMOutStatus)!="" && intval($PMOutStatus)!=3)
		 {
		 	$HasError = true;
		 	if($intranet_session_language=="b5")
		 		$error_array[] = "資料行$rowi: 下午離開狀況不合理";
		 	else
		 		$error_array[] = "PMOutStatus of data Row $rowi is not valid";
		 }
		 if(trim($AMInTime)=="")
		 {
		 	$AMInTimeVal = "NULL";
		 }else
		 {
		 	$AMInTimeVal = "'$AMInTime'";
		 }
		 if(trim($PMInTime)=="")
		 {
		 	$PMInTimeVal = "NULL";
		 }else
		 {
		 	$PMInTimeVal = "'$PMInTime'";
		 }
		 if(intval($PMInStatus)==CARD_STATUS_LATE)
		 {
		 	// Special case: PMInTime==6 means some students with Late status do not have PMInTime
		 	if(trim($PMInTime)=="6")
		 	{
		 		$PMInTimeVal="NULL";
		 	}
		 }
		 if($HasError) continue;
         $datestr = explode('-', $Date);
         $DayNumber = intval($datestr[2]);
         $targetUserID = $students[$Class][$ClassNum];
         if ($targetUserID != '')
         {
             $values .= "$delim('$targetUserID','$Class','$ClassNum','$DayNumber','$Date',
						'$AMInStatus',$AMInTimeVal,'$AMInWaive','$AMInReason',
			  			'$AMOutStatus','$AMOutWaive','$AMOutReason',
			  			'$PMInStatus',$PMInTimeVal,'$PMInWaive','$PMInReason',
			  			'$PMOutStatus','$PMOutWaive','$PMOutReason')";
             $delim = ",";
         }else
         {
         	if($intranet_session_language=="b5")
		 		$error_array[] = "資料行$rowi: 班別為".$Class."和學號為".$ClassNum."的學生不存在";
		 	else
         		$error_array[] = "Student with Class $Class and Class No. $ClassNum of data Row $rowi doesn't exist";
         }
    }

    $sql = "INSERT INTO TEMP_CARD_STUDENT_DAILY_LOG (UserID,ClassName,ClassNumber,DayNumber,RecordDate,
			AMInStatus,AMInTime,AMInWaive,AMInReason,AMOutStatus,AMOutWaive,AMOutReason,
			PMInStatus,PMInTime,PMInWaive,PMInReason,PMOutStatus,PMOutWaive,PMOutReason) 
			VALUES $values ";
    $li->db_db_query($sql);
}
// Get back the record data with Student Name to be shown
$name_field = getNameFieldByLang("b.");
$sql = "SELECT 
			$name_field as Name, 
			b.ClassName, 
			b.ClassNumber,  
			a.RecordDate,
			IF(a.AMInStatus IS NULL,0,a.AMInStatus) as AMInStatus,
			IF(a.AMInTime IS NULL,'-',a.AMInTime) as AMInTime,
			a.AMInWaive,
			a.AMInReason,
			IF(a.AMOutStatus IS NULL,0,a.AMOutStatus) as AMOutStatus,
			a.AMOutWaive,
			a.AMOutReason,
			IF(a.PMInStatus IS NULL,0,a.PMInStatus) as PMInStatus,
			IF(a.PMInTime IS NULL,'-',a.PMInTime) as PMInTime,
			a.PMInWaive,
			a.PMInReason,
			IF(a.PMOutStatus IS NULL,0,a.PMOutStatus) as PMOutStatus,
			a.PMOutWaive,
			a.PMOutReason
        FROM 
			TEMP_CARD_STUDENT_DAILY_LOG as a
        	LEFT OUTER JOIN INTRANET_USER as b ON b.RecordType = 2 AND a.UserID = b.UserID ";
$result = $li->returnArray($sql,18);

//Table header
$display = "<table width=\"95%\" border=\"0\" cellspacing=\"0\" cellpadding=\"5\" align=\"center\">";
$display .= "<tr class=\"tablebluetop\">";
$display .= "<td class=\"tabletoplink\" nowrap>".$Lang['StudentAttendance']['StudentName']."</td>";
$display .= "<td class=\"tabletoplink\" nowrap>".$Lang['StudentAttendance']['Class']."</td>";
$display .= "<td class=\"tabletoplink\" nowrap>".$Lang['StudentAttendance']['ClassNumber']."</td>";
$display .= "<td class=\"tabletoplink\" nowrap>".$Lang['General']['Date']."</td>";
$display .= "<td class=\"tabletoplink\" nowrap>".$Lang['StudentAttendance']['AMInStatus']."</td>";
$display .= "<td class=\"tabletoplink\" nowrap>".$Lang['StudentAttendance']['AMInTime']."</td>";
$display .= "<td class=\"tabletoplink\" nowrap>".$Lang['StudentAttendance']['AMInWaive']."</td>";
$display .= "<td class=\"tabletoplink\" nowrap>".$Lang['StudentAttendance']['AMInReason']."</td>";
$display .= "<td class=\"tabletoplink\" nowrap>".$Lang['StudentAttendance']['AMOutStatus']."</td>";
$display .= "<td class=\"tabletoplink\" nowrap>".$Lang['StudentAttendance']['AMOutWaive']."</td>";
$display .= "<td class=\"tabletoplink\" nowrap>".$Lang['StudentAttendance']['AMOutReason']."</td>";
$display .= "<td class=\"tabletoplink\" nowrap>".$Lang['StudentAttendance']['PMInStatus']."</td>";
$display .= "<td class=\"tabletoplink\" nowrap>".$Lang['StudentAttendance']['PMInTime']."</td>";
$display .= "<td class=\"tabletoplink\" nowrap>".$Lang['StudentAttendance']['PMInWaive']."</td>";
$display .= "<td class=\"tabletoplink\" nowrap>".$Lang['StudentAttendance']['PMInReason']."</td>";
$display .= "<td class=\"tabletoplink\" nowrap>".$Lang['StudentAttendance']['PMOutStatus']."</td>";
$display .= "<td class=\"tabletoplink\" nowrap>".$Lang['StudentAttendance']['PMOutWaive']."</td>";
$display .= "<td class=\"tabletoplink\" nowrap>".$Lang['StudentAttendance']['PMOutReason']."</td>";
$display .= "</tr>";

$StatusArr = array(CARD_STATUS_PRESENT => $Lang['StudentAttendance']['Present'], CARD_STATUS_ABSENT => $Lang['StudentAttendance']['Absent'], CARD_STATUS_LATE => $Lang['StudentAttendance']['Late'], PROFILE_TYPE_EARLY => $Lang['StudentAttendance']['EarlyLeave']);
$WaiveArr = array(0 => $Lang['StudentAttendance']['NotWaived'], 1 => $Lang['StudentAttendance']['Waived']);
//Record rows
for ($i=0; $i<sizeof($result); $i++)
{
     list($Name,$Class,$ClassNum,$Date,
          $AMInStatus,$AMInTime,$AMInWaive,$AMInReason,
		  $AMOutStatus,$AMOutWaive,$AMOutReason,
		  $PMInStatus,$PMInTime,$PMInWaive,$PMInReason,
		  $PMOutStatus,$PMOutWaive,$PMOutReason) = $result[$i];
     $css=($i%2==0)?"tablebluerow1":"tablebluerow2";
     $display .= "<tr class=\"$css\">";
     $display .= "<td class=\"tabletext\">$Name</td>";
     $display .= "<td class=\"tabletext\">$Class</td>";
     $display .= "<td class=\"tabletext\">$ClassNum</td>";
     $display .= "<td class=\"tabletext\">$Date</td>";
     $display .= "<td class=\"tabletext\">".$StatusArr[$AMInStatus]."</td>";
     $display .= "<td class=\"tabletext\">$AMInTime</td>";
     $display .= "<td class=\"tabletext\">".$WaiveArr[$AMInWaive]."</td>";
     $display .= "<td class=\"tabletext\">$AMInReason</td>";
     $display .= "<td class=\"tabletext\">".$StatusArr[$AMOutStatus]."</td>";
     $display .= "<td class=\"tabletext\">".$WaiveArr[$AMOutWaive]."</td>";
     $display .= "<td class=\"tabletext\">$AMOutReason</td>";
     $display .= "<td class=\"tabletext\">".$StatusArr[$PMInStatus]."</td>";
     $display .= "<td class=\"tabletext\">$PMInTime</td>";
     $display .= "<td class=\"tabletext\">".$WaiveArr[$PMInWaive]."</td>";
     $display .= "<td class=\"tabletext\">$PMInReason</td>";
     $display .= "<td class=\"tabletext\">".$StatusArr[$PMOutStatus]."</td>";
     $display .= "<td class=\"tabletext\">".$WaiveArr[$PMOutWaive]."</td>";
     $display .= "<td class=\"tabletext\">$PMOutReason</td>";
	 $display .= "</tr>";
}
$display .= "</table>";

$TAGS_OBJ[] = array($Lang['StudentAttendance']['ImportAttendenceData'], "", 0);

$MODULE_OBJ = $lc->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

$PAGE_NAVIGATION[] = array($subHeader);

?>
<br />
<form name="form1" method="POST" action="import_update_confirm.php">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td><?echo "From: ".$linterface->GET_DATE_PICKER("StartDate",date("Y-m-d"));echo "To: ".$linterface->GET_DATE_PICKER("EndDate",date("Y-m-d"));?></td>
	</tr>
	<tr>
		<td>
			<?=$display?>
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>
			<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
		    	<tr>
		    		<td class="tabletextremark">
		    			<div id="WarningMsgLayer" name="WarningMsgLayer" style="color:red;"><?=((sizeof($error_array)>0)?implode("<br />", $error_array):"")?></div>
		    		</td>
		    	</tr>
		    </table>
		</td>
	</tr>
	<tr>
		<td>
			<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
			<tr>
				<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
			</tr>
			</table>	
		</td>
	</tr>
	<tr>
		<td>
			<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
		    	<tr>
				    <td align="center" colspan="2">
						<?= $linterface->GET_ACTION_BTN($button_import, "submit", "",""," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
						<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:history.back()","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</form>
<br />
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>