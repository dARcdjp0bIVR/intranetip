<?php
// Editing by 
/*
 * 2016-09-02 (Carlos): Added [Reason].
 * 2016-03-02 (Carlos): Cater the issue of partial importing subject group students to lessons, 
 * 						the non-imported students would be missing because no SUBJECT_GROUP_ATTEND_OVERVIEW_[ID] records generated. 
 * 						Solved by padding the non-imported students into SUBJECT_GROUP_ATTEND_OVERVIEW_[ID] with default status. 
 */
set_time_limit(60*60);
ini_set("memory_limit", "1024M");
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libstudentattendance_ui.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-LessonAttendance"] || !$_SESSION["SSV_PRIVILEGE"]["plugin"]["attendancelesson"]) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$linterface = new interface_html();
$limport = new libimporttext();
$lf = new libfilesystem();
$LessonAttendUI = new libstudentattendance_ui();
$CurrentPageArr['StudentAttendance'] = 1;
//$CurrentPage = "PageLessonAttendance_ImportLessonAttendanceRecords";
$CurrentPage = "PageLessonAttendance_DailyOverview";

$format_array = array("Date","Time Slot Name","Class Name","Class Number","Status","Reason","Remarks");
$filepath = $userfile;
$filename = $userfile_name;


if($filepath=="none" || $filepath == "")
{
	# import failed
    header("Location: index.php?xmsg=ImportUnsuccess");
    intranet_closedb();
    exit();
}

$settingsAry = $LessonAttendUI->Get_General_Settings('LessonAttendance', array("'DefaultStatus'"));
$default_status = $settingsAry['DefaultStatus'] == ''? 0 : $settingsAry['DefaultStatus'];

$ext = strtoupper($lf->file_ext($filename));
if($limport->CHECK_FILE_EXT($filename))
{
    # read file into array
    # return 0 if fail, return csv array if success
    //$data = $lf->file_read_csv($filepath);
    $data = $limport->GET_IMPORT_TXT($filepath);
    if(sizeof($data)>0)
    {
    	$toprow = array_shift($data);                   # drop the title bar
    }else
    {
    	header("Location: index.php?xmsg=ImportUnsuccess");
	    intranet_closedb();
	    exit();
    }
}

$record_count = count($data);
$column_count = count($data[0]);
$recordAry = $data;
$validRecordAry = array();
$errorAry = array();

$sql = "DROP TABLE IF EXISTS TEMP_IMPORT_LESSON_ATTENDANCE_RECORD";
$LessonAttendUI->db_db_query($sql);

$sql = "CREATE TABLE IF NOT EXISTS TEMP_IMPORT_LESSON_ATTENDANCE_RECORD (
			RecordID int(11) NOT NULL auto_increment,
			AttendOverviewID int(11) default NULL,
			LessonDate date NOT NULL,
			SubjectGroupID int(11) default NULL,
			SubjectGroupCode varchar(50) NOT NULL,
			SubjectGroupTitleEN varchar(255) default NULL,
			SubjectGroupTitleB5 varchar(255) default NULL,
			RoomAllocationID int(11) default NULL,
			TimeSlotID int(11) default NULL,
			TimeSlotName varchar(255) NOT NULL,
			StartTime time default NULL,
			EndTime time default NULL,
			ClassName varchar(255) NOT NULL,
			ClassNumber varchar(50) NOT NULL,
			StudentID int(11) default NULL,
			AttendStatus tinyint NOT NULL,
			Reason text default NULL,
			Remarks text default NULL,
			InputDate datetime default NULL,
			InputBy int(11) default NULL,
			PRIMARY KEY (RecordID),
			UNIQUE KEY CompositeUniqueKeys(LessonDate,SubjectGroupCode,TimeSlotName,ClassName,ClassNumber)
		)ENGINE=InnoDB DEFAULT CHARSET=utf8 ";

$LessonAttendUI->db_db_query($sql);

$TotalRecord = $record_count;
$TotalValidRecord = 0;
$TotalInvalidRecord = 0;
$queryResult = array();

$current_academic_year_id = Get_Current_Academic_Year_ID();
$statusMap = $LessonAttendUI->LessonAttendanceStatusMapping();
foreach($statusMap as $map)
{
	$attendStatusAry[] = $map['code'];
	$attendStatusDisplayAry[$map['code']] = ' ('.$map['text'].')';
}
//$attendStatusAry = array(0,1,2,3);
//$attendStatusNameAry = array();
//$attendStatusDisplayAry[0] = ' ('.$Lang['LessonAttendance']['Present'].')';
//$attendStatusDisplayAry[1] = ' ('.$Lang['LessonAttendance']['Outing'].')';
//$attendStatusDisplayAry[2] = ' ('.$Lang['LessonAttendance']['Late'].')';
//$attendStatusDisplayAry[3] = ' ('.$Lang['LessonAttendance']['Absent'].')';


$name_field = getNameFieldByLang("u.");
// Build ClassName+ClassNumber to student info array
$sql = "SELECT yc.ClassTitleEN as ClassTitle,ycu.ClassNumber,ycu.UserID, $name_field as StudentName 
		FROM YEAR_CLASS as yc 
		INNER JOIN YEAR_CLASS_USER as ycu ON yc.YearClassID=ycu.YearClassID 
		INNER JOIN INTRANET_USER as u ON u.UserID=ycu.UserID 
		WHERE yc.AcademicYearID='$current_academic_year_id' AND u.RecordStatus='1' AND u.RecordType='".USERTYPE_STUDENT."'";
$records = $LessonAttendUI->returnResultSet($sql);
$ClassToStudentAry = array();
$records_count = count($records);

for($i=0;$i<$records_count;$i++) {
	$class_title = $records[$i]['ClassTitle'];
	$class_number = $records[$i]['ClassNumber'];
	$user_id = $records[$i]['UserID'];
	$student_name = $records[$i]['StudentName'];
	
	if(!isset($ClassToStudentAry[$class_title])) {
		$ClassToStudentAry[$class_title] = array();
	}
	$ClassToStudentAry[$class_title][$class_number] = array('UserID' => $user_id, 'StudentName' => $student_name);
}

// Build Subject Group Code to Subject Group info array
$sql = "SELECT SubjectGroupID,ClassCode,ClassTitleEN,ClassTitleB5 FROM SUBJECT_TERM_CLASS";
$records = $LessonAttendUI->returnResultSet($sql);
$records_count = count($records);
$SubjectGroupCodeToInfoAry = array();

for($i=0;$i<$records_count;$i++) {
	$subject_group_code = $records[$i]['ClassCode'];
	$SubjectGroupCodeToInfoAry[$subject_group_code] = $records[$i];
}

//debug_r($SubjectGroupCodeToInfoAry);

// Cache date to cycle day array
$cacheCycleDayAry = array();

// Cache date to term info array
$cacheTermInfoAry = array();

// Cache date to lesson info array
$cacheLessonAry = array();

$dateTimeslotUserIdToRecord = array(); // array[DATE][TIMESLOT][USERID] = lesson record
$dateTimeslotToAddedUserId = array(); // array[DATE][TIMESLOT] = array(...imported user_ids...)
$dateTimeslotToAllUserId = array(); // array[DATE][TIMESLOT] = array(...user_ids get from time table...)
// find the not imported or missing users with the set operation ($dateTimeslotToAllUserId - $dateTimeslotToAddedUserId)

// Check validity of import data
for($i=0;$i<$record_count;$i++) {
	list($lesson_date, $time_slot_name, $class_name, $class_number, $status, $reason, $remarks) = $recordAry[$i];
	
	$lesson_date= trim($lesson_date);
	//$subject_group_code = trim($subject_group_code);
	$time_slot_name = trim($time_slot_name);
	$class_name = trim($class_name);
	$class_number = trim($class_number);
	$status = trim($status);
	$reason = trim($reason);
	$remarks = trim($remarks);
	
	$is_data_valid = true;
	$row_num = $i+2; // dropped header row and count from 0
	
	$record_date = getDefaultDateFormat($lesson_date);
	
	// Check date 
	$ts = strtotime($record_date);             
    if($ts==-1 || preg_match('/^\d\d\d\d-\d\d-\d\d$/', $record_date)==0)
    { 
     	## invalid date format
     	if(!isset($errorAry[$row_num])) {
     		$errorAry[$row_num] = array();
     	}
 		$errorAry[$row_num][] = $Lang['General']['InvalidDateFormat'];
 		//$TotalInvalidRecord++;
    	//continue;
    	$is_data_valid = false;
    }
    # Further check date format
    $tempYear = substr($record_date,0,4);
    $tempMonth = substr($record_date,5,2);
    $tempDay = substr($record_date,8,2);
     
    if($is_data_valid && !checkdate(intval($tempMonth), intval($tempDay), intval($tempYear)))
    {
     	if(!isset($errorAry[$row_num])) {
     		$errorAry[$row_num] = array();
     	}
     	$errorAry[$row_num][] = $Lang['General']['InvalidDateFormat'];
     	//$TotalInvalidRecord++;
    	//continue;
    	$is_data_valid = false;
    }
	/*
	// Check subject group
	if($subject_group_code=='' || !isset($SubjectGroupCodeToInfoAry[$subject_group_code])){
		
		if(!isset($errorAry[$row_num])) {
     		$errorAry[$row_num] = array();
     	}
     	$errorAry[$row_num][] = $Lang['LessonAttendance']['ImportLessonAttendanceRecords']['ErrorSubjectGroupIsNotFound'];
     	//$TotalInvalidRecord++;
		//continue;
		$is_data_valid = false;
	}
	*/
	// Check time slot 
	if($time_slot_name == '') {
		if(!isset($errorAry[$row_num])) {
     		$errorAry[$row_num] = array();
     	}
     	$errorAry[$row_num][] = $Lang['LessonAttendance']['ImportLessonAttendanceRecords']['ErrorTimeSlotIsNotFound'];
     	//$TotalInvalidRecord++;
		//continue;
		$is_data_valid = false;
	}
	
	if($class_name == '' || !isset($ClassToStudentAry[$class_name])){
		if(!isset($errorAry[$row_num])) {
     		$errorAry[$row_num] = array();
     	}
     	$errorAry[$row_num][] = $Lang['LessonAttendance']['ImportLessonAttendanceRecords']['ErrorInvalidClassName'];
     	$TotalInvalidRecord++;
     	//continue;
     	$is_data_valid = false;
	}
	
	if($class_number == '' || !isset($ClassToStudentAry[$class_name][$class_number])){
		if(!isset($errorAry[$row_num])) {
     		$errorAry[$row_num] = array();
     	}
     	$errorAry[$row_num][] = $Lang['LessonAttendance']['ImportLessonAttendanceRecords']['ErrorInvalidClassNumber'] ;
     	//$TotalInvalidRecord++;
     	//continue;
     	$is_data_valid = false;
	}
	
	if($status == '' || !in_array($status,$attendStatusAry)) {
		if(!isset($errorAry[$row_num])) {
     		$errorAry[$row_num] = array();
     	}
     	$errorAry[$row_num][] = $Lang['LessonAttendance']['ImportLessonAttendanceRecords']['ErrorInvalidAttendanceStatus'];
     	//continue;
     	$is_data_valid = false;
	}
	
	if(!$is_data_valid) {
		continue;
	}
	
	if(!isset($cacheCycleDayAry[$record_date])) {
		// not cached yet, get fresh data
		$TermInfo = getAcademicYearInfoAndTermInfoByDate($record_date);
		list($AcademicYearID,$AcademicYearName,$YearTermID,$YearTermName) = $TermInfo;
		$CycleDayInfo = $LessonAttendUI->Get_Attendance_Cycle_Day_Info($record_date);
		$cacheTermInfoAry[$record_date] = $YearTermID;
		if ($CycleDayInfo[0] == 'Cycle') {
			$DayNumber = $LessonAttendUI->Convert_Cycle_Day_Value_To_Day_Number($CycleDayInfo[1],$CycleDayInfo[2]);
			$cacheCycleDayAry[$record_date] = $DayNumber;
		}else{
			$cacheCycledayAry[$record_date] = -1;
		}
	}else{
		// get from cache
		$DayNumber = $cacheCycleDayAry[$record_date];
		$YearTermID = $cacheTermInfoAry[$record_date];
	}
	
	if(!isset($cacheLessonAry[$record_date])) {
	
		// Check time slot on that day and related subject group 
		$sql = "SELECT 
				  '$record_date' as RecordDate,
				  yc.YearClassID,
				  stc.SubjectGroupID,
				  stc.ClassCode as SubjectGroupCode,
				  stc.ClassTitleB5 as SubjectGroupTitleB5,
				  stc.ClassTitleEN as SubjectGroupTitleEN,
				  itra.RoomAllocationID,
				  itra.TimeSlotID,
				  itt.TimeSlotName,
				  itt.StartTime,
				  itt.EndTime,
				  yc.ClassTitleEN as ClassName,
				  ycu.ClassNumber,
				  ycu.UserID,
				  sgato1.AttendOverviewID 
				FROM 
				  INTRANET_CYCLE_GENERATION_PERIOD as icgp
				  inner join
				  INTRANET_PERIOD_TIMETABLE_RELATION as iptr
				  on
				    '".$record_date."' between icgp.PeriodStart and icgp.PeriodEnd
				    and icgp.PeriodID = iptr.PeriodID
				  inner join
				  INTRANET_TIMETABLE_ROOM_ALLOCATION as itra
				  on
				    iptr.TimeTableID = itra.TimeTableID
				    and itra.Day = IF(icgp.PeriodType=1,'".$DayNumber."', DAYOFWEEK('".$record_date."')-1) 
				  inner join INTRANET_TIMETABLE_TIMESLOT as itt ON itt.TimeSlotID=itra.TimeSlotID 
				  inner join
				  SUBJECT_TERM_CLASS as stc
				  on itra.SubjectGroupID = stc.SubjectGroupID
				  inner join 
				  SUBJECT_TERM as st
				  on st.SubjectGroupID = stc.SubjectGroupID and st.YearTermID = '".$YearTermID."' 
				  inner join 
				  ASSESSMENT_SUBJECT as s 
				  on st.SubjectID = s.RecordID 
				  inner join
				  SUBJECT_TERM_CLASS_USER as stcu
				  on stc.SubjectGroupID = stcu.SubjectGroupID
				  inner join
				  YEAR_CLASS_USER as ycu
				  on stcu.UserID = ycu.UserID
				  inner join
				  YEAR_CLASS as yc
				  on ycu.YearClassID = yc.YearClassID
				    and yc.AcademicYearID = '".$current_academic_year_id."' 
				  left join 
				  SUBJECT_GROUP_ATTEND_OVERVIEW_".$current_academic_year_id." as sgato1 
				  on 
				    stc.SubjectGroupID = sgato1.SubjectGroupID 
				    and itra.RoomAllocationID = sgato1.RoomAllocationID
				    and sgato1.LessonDate = '".$record_date."'";
				  
		$lessonAry = $LessonAttendUI->returnResultSet($sql);
		//debug_r($lessonAry);
		$cacheLessonAry[$record_date] = $lessonAry;
		
		$lessonAry_size = count($lessonAry);
		for($j=0;$j<$lessonAry_size;$j++){
			$tmp_date = $lessonAry[$j]['RecordDate'];
			$tmp_timeslot = $lessonAry[$j]['TimeSlotName'];
			$tmp_userid = $lessonAry[$j]['UserID'];
			if(!isset($dateTimeslotUserIdToRecord[$tmp_date]))
			{
				$dateTimeslotUserIdToRecord[$tmp_date] = array();
			}
			if(!isset($dateTimeslotUserIdToRecord[$tmp_date][$tmp_timeslot])){
				$dateTimeslotUserIdToRecord[$tmp_date][$tmp_timeslot] = array();
			}
			if(!isset($dateTimeslotUserIdToRecord[$tmp_date][$tmp_timeslot][$tmp_userid]))
			{
				$dateTimeslotUserIdToRecord[$tmp_date][$tmp_timeslot][$tmp_userid] = array();
			}
			$dateTimeslotUserIdToRecord[$tmp_date][$tmp_timeslot][$tmp_userid] = $lessonAry[$j];
			
			if(!isset($dateTimeslotToAllUserId[$tmp_date])){
				$dateTimeslotToAllUserId[$tmp_date] = array();
			}
			if(!isset($dateTimeslotToAllUserId[$tmp_date][$tmp_timeslot])){
				$dateTimeslotToAllUserId[$tmp_date][$tmp_timeslot] = array();
			}
			$dateTimeslotToAllUserId[$tmp_date][$tmp_timeslot][] = $tmp_userid;
		}
		
	}else{
		$lessonAry = $cacheLessonAry[$record_date];
	}
	
	$lesson_count = count($lessonAry);
	$student_id = $ClassToStudentAry[$class_name][$class_number]['UserID'];
	
	//debug_r('ClassName='.$class_name.' ClassNumber='.$class_number.' UserID='.$student_id);
	
	$lesson_found = false;
	$lesson_record = array();
	/*
	for($j=0;$j<$lesson_count;$j++) {
		
		if($student_id == $lessonAry[$j]['UserID'] && $time_slot_name == $lessonAry[$j]['TimeSlotName']) {
			$lesson_found = true;
			$lesson_record = $lessonAry[$j];
			break;
		}
		
	}
	*/
	if(isset($dateTimeslotUserIdToRecord[$record_date]) && isset($dateTimeslotUserIdToRecord[$record_date][$time_slot_name]) && 
		isset($dateTimeslotUserIdToRecord[$record_date][$time_slot_name][$student_id]))
	{
		$lesson_found = true;
		$lesson_record = $dateTimeslotUserIdToRecord[$record_date][$time_slot_name][$student_id];
		
		if(!isset($dateTimeslotToAddedUserId[$record_date])){
			$dateTimeslotToAddedUserId[$record_date] = array();
		}
		if(!isset($dateTimeslotToAddedUserId[$record_date][$time_slot_name])){
			$dateTimeslotToAddedUserId[$record_date][$time_slot_name] = array();
		}
		$dateTimeslotToAddedUserId[$record_date][$time_slot_name][] = $student_id;
	}
	
	
	if(!$lesson_found) {
		if(!isset($errorAry[$row_num])) {
     		$errorAry[$row_num] = array();
     	}
     	$errorAry[$row_num][] = $Lang['LessonAttendance']['ImportLessonAttendanceRecords']['ErrorLessonIsNotFound'];
     	//$TotalInvalidRecord++;
     	continue;
	}
	
	$validRecordAry[$i] = $lesson_record;
	
	$temp_attend_overview_id = trim($lesson_record['AttendOverviewID']);
	$temp_subject_group_code = $LessonAttendUI->Get_Safe_Sql_Query($lesson_record['SubjectGroupCode']);
	$temp_subject_group_title_en = $LessonAttendUI->Get_Safe_Sql_Query( $lesson_record['SubjectGroupTitleEN']);
	$temp_subject_group_title_b5 = $LessonAttendUI->Get_Safe_Sql_Query( $lesson_record['SubjectGroupTitleB5']);
	$temp_time_slot_name = $LessonAttendUI->Get_Safe_Sql_Query( $lesson_record['TimeSlotName']);
	$temp_class_name = $LessonAttendUI->Get_Safe_Sql_Query( $lesson_record['ClassName']);
	$temp_class_number = $LessonAttendUI->Get_Safe_Sql_Query( $lesson_record['ClassNumber']);
	$temp_reason = $LessonAttendUI->Get_Safe_Sql_Query($reason);
	$temp_remarks = $LessonAttendUI->Get_Safe_Sql_Query($remarks);
	
	
	$sql = "INSERT INTO TEMP_IMPORT_LESSON_ATTENDANCE_RECORD (
				AttendOverviewID, LessonDate, SubjectGroupID, SubjectGroupCode, SubjectGroupTitleEN, SubjectGroupTitleB5,
				RoomAllocationID, TimeSlotID, TimeSlotName, StartTime, EndTime,
				ClassName, ClassNumber, StudentID, AttendStatus, Reason, Remarks, InputDate, InputBy) VALUES 
				('$AttendOverviewID','$record_date','".$lesson_record['SubjectGroupID']."','".$temp_subject_group_code."','".$temp_subject_group_title_en."','".$temp_subject_group_title_b5."',
				'".$lesson_record['RoomAllocationID']."','".$lesson_record['TimeSlotID']."','".$temp_time_slot_name."','".$lesson_record['StartTime']."','".$lesson_record['EndTime']."',
				'$temp_class_name','$temp_class_number','".$lesson_record['UserID']."','$status','$temp_reason','$temp_remarks',NOW(),'".$_SESSION['UserID']."')";
	$queryResult[] = $LessonAttendUI->db_db_query($sql);
	
	$TotalValidRecord++;
	
}

// find missing students in lesson attendance during the import
if(count($dateTimeslotToAllUserId)>0)
{
	foreach($dateTimeslotToAllUserId as $tmp_date => $tmp_ary)
	{
		foreach($tmp_ary as $tmp_timeslot => $all_userid_ary)
		{
			if(isset($dateTimeslotToAddedUserId[$tmp_date]) && isset($dateTimeslotToAddedUserId[$tmp_date][$tmp_timeslot]))
			{
				$missing_userid_ary = array_values(array_diff($all_userid_ary, $dateTimeslotToAddedUserId[$tmp_date][$tmp_timeslot]));
				for($i=0;$i<count($missing_userid_ary);$i++){
					$lesson_record = $dateTimeslotUserIdToRecord[$tmp_date][$tmp_timeslot][$missing_userid_ary[$i]];
					
					$temp_attend_overview_id = trim($lesson_record['AttendOverviewID']);
					if($temp_attend_overview_id == '')
					{
						$temp_subject_group_code = $LessonAttendUI->Get_Safe_Sql_Query($lesson_record['SubjectGroupCode']);
						$temp_subject_group_title_en = $LessonAttendUI->Get_Safe_Sql_Query( $lesson_record['SubjectGroupTitleEN']);
						$temp_subject_group_title_b5 = $LessonAttendUI->Get_Safe_Sql_Query( $lesson_record['SubjectGroupTitleB5']);
						$temp_time_slot_name = $LessonAttendUI->Get_Safe_Sql_Query( $lesson_record['TimeSlotName']);
						$temp_class_name = $LessonAttendUI->Get_Safe_Sql_Query( $lesson_record['ClassName']);
						$temp_class_number = $LessonAttendUI->Get_Safe_Sql_Query( $lesson_record['ClassNumber']);
						
						$sql = "INSERT INTO TEMP_IMPORT_LESSON_ATTENDANCE_RECORD (
									AttendOverviewID, LessonDate, SubjectGroupID, SubjectGroupCode, SubjectGroupTitleEN, SubjectGroupTitleB5,
									RoomAllocationID, TimeSlotID, TimeSlotName, StartTime, EndTime,
									ClassName, ClassNumber, StudentID, AttendStatus, InputDate, InputBy) VALUES 
									('$temp_attend_overview_id','$tmp_date','".$lesson_record['SubjectGroupID']."','".$temp_subject_group_code."','".$temp_subject_group_title_en."','".$temp_subject_group_title_b5."',
									'".$lesson_record['RoomAllocationID']."','".$lesson_record['TimeSlotID']."','".$temp_time_slot_name."','".$lesson_record['StartTime']."','".$lesson_record['EndTime']."',
									'$temp_class_name','$temp_class_number','".$lesson_record['UserID']."','$default_status',NOW(),'".$_SESSION['UserID']."')";
						$queryResult[] = $LessonAttendUI->db_db_query($sql);
					}
				}
			}
		}
	}
}

$TotalInvalidRecord = count($errorAry);

### Title ###
//$TAGS_OBJ[] = array($Lang['LessonAttendance']['DailyLessonStatus'],$PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/dailyoperation/daily_lesson_attendance/index2.php",0);
//$TAGS_OBJ[] = array($Lang['LessonAttendance']['ImportLessonAttendanceRecords']['Title'],$PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/dailyoperation/import_lesson_attendance/import.php",1);
$TAGS_OBJ[] = array($Lang['LessonAttendance']['DailyLessonStatus'],"",0);
$MODULE_OBJ = $LessonAttendUI->GET_MODULE_OBJ_ARR();

if($xmsg != ''){
	if(isset($Lang['General']['ReturnMessage'][$xmsg])){
		$msg = $Lang['General']['ReturnMessage'][$xmsg];
	}else{
		$msg = $xmsg;
	}
}

$PAGE_NAVIGATION[] = array($Lang['LessonAttendance']['DailyLessonStatus'],$PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/dailyoperation/daily_lesson_attendance/index2.php");
$PAGE_NAVIGATION[] = array($Lang['Btn']['Import']);
# step information
$STEPS_OBJ[] = array($Lang['General']['ImportArr']['ImportStepArr']['SelectCSVFile'], 0);
$STEPS_OBJ[] = array($Lang['General']['ImportArr']['ImportStepArr']['CSVConfirmation'], 1);
$STEPS_OBJ[] = array($Lang['General']['ImportArr']['ImportStepArr']['ImportResult'], 0);

$linterface->LAYOUT_START($msg);
?>
<br />
<div>
	<div class="table_board">
		<form name="form1" method="POST" action="import_update.php">
			<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td><?=$linterface->GET_NAVIGATION($PAGE_NAVIGATION)?></td>
				</tr>
				<tr>
					<td><?=$linterface->GET_STEPS($STEPS_OBJ)?></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
			</table>
			<table width="90%" border="0" cellpadding="3" cellspacing="0" align="center">
				<tr>
					<td class="formfieldtitle" width="30%" align="left"><?=$Lang['General']['TotalRecord']?></td>
					<td class="tabletext"><?=$TotalRecord?></td>
				</tr>
				<tr>
					<td class="formfieldtitle" width="30%" align="left"><?=$Lang['General']['SuccessfulRecord']?></td>
					<td class="tabletext"><?=$TotalValidRecord?></td>
				</tr>
				<tr>
					<td class="formfieldtitle" width="30%" align="left"><?=$Lang['General']['FailureRecord']?></td>
					<td class="tabletext"><?=$TotalInvalidRecord?></td>
				</tr>
			</table>
			<table width="90%" border="0" cellpadding="3" cellspacing="0" align="center">
			<tbody>
				<tr>
					<th class="tablebluetop tabletopnolink" width="10">#</th>
					<th class="tablebluetop tabletopnolink"><?=$Lang['General']['Date']?></th>
					<!--<th class="tablebluetop tabletopnolink"><?=$Lang['SysMgr']['SubjectClassMapping']['SubjectGroupCode']?></th>-->
					<th class="tablebluetop tabletopnolink"><?=$Lang['SysMgr']['SubjectClassMapping']['ClassTitle'] ?></th>
					<th class="tablebluetop tabletopnolink"><?=$Lang['SysMgr']['Timetable']['Period']?></th>
					<th class="tablebluetop tabletopnolink"><?=$Lang['SysMgr']['Timetable']['StartTime']?></th>
					<th class="tablebluetop tabletopnolink"><?=$Lang['SysMgr']['Timetable']['EndTime']?></th>
					<th class="tablebluetop tabletopnolink"><?=$i_ClassName?></th>
					<th class="tablebluetop tabletopnolink"><?=$i_ClassNumber?></th>
					<th class="tablebluetop tabletopnolink"><?=$Lang['SysMgr']['FormClassMapping']['StudentName']?></th>
					<th class="tablebluetop tabletopnolink"><?=$Lang['LessonAttendance']['Status']?></th>
					<th class="tablebluetop tabletopnolink"><?=$Lang['LessonAttendance']['Reason']?></th>
					<th class="tablebluetop tabletopnolink"><?=$Lang['General']['Remark']?></th>
					<th class="tablebluetop tabletopnolink"><?=$Lang['General']['Error']?></th>
				</tr>
<?php
	$x = '';
	if($record_count > 0){
		for($i=0;$i<$record_count;$i++)
		{
			$row_num = $i+2;
			$css_i = ($i % 2) ? "2" : "";
			$error_detail = '';
			if(isset($errorAry[$row_num]) && count($errorAry[$row_num])>0) {
				$error_detail = implode("<br>",$errorAry[$row_num]);
			}
			
			$display_class_name = $recordAry[$i][2];
			$display_class_number = $recordAry[$i][3];
			$display_student_name = $ClassToStudentAry[$display_class_name][$display_class_number]['StudentName'];
			
			$x .= '<tr>';
				$x .= '<td valign="top" class="tablebluerow'.$css_i.'" width="10">'.($i+1).'</td>';
				$x .= '<td valign="top" class="tablebluerow'.$css_i.'">'.$recordAry[$i][0].'</td>'; // Date
				//$x .= '<td valign="top" class="tablebluerow'.$css_i.'">'.$recordAry[$i][1].'</td>'; // Subject Group Code
				$x .= '<td valign="top" class="tablebluerow'.$css_i.'">'.Get_Lang_Selection($validRecordAry[$i]['SubjectGroupTitleB5'],$validRecordAry[$i]['SubjectGroupTitleEN']).'</td>'; // subject group title
				$x .= '<td valign="top" class="tablebluerow'.$css_i.'">'.$recordAry[$i][1].'</td>'; // time slot name
				$x .= '<td valign="top" class="tablebluerow'.$css_i.'">'.$validRecordAry[$i]['StartTime'].'</td>'; // start time
				$x .= '<td valign="top" class="tablebluerow'.$css_i.'">'.$validRecordAry[$i]['EndTime'].'</td>'; // end time
				$x .= '<td valign="top" class="tablebluerow'.$css_i.'">'.$recordAry[$i][2].'</td>'; // class name
				$x .= '<td valign="top" class="tablebluerow'.$css_i.'">'.$recordAry[$i][3].'</td>'; // class number
				$x .= '<td valign="top" class="tablebluerow'.$css_i.'">'.$display_student_name.'</td>'; // student name
				$x .= '<td valign="top" class="tablebluerow'.$css_i.'">'.$recordAry[$i][4].$attendStatusDisplayAry[$recordAry[$i][4]].'</td>'; // status code
				$x .= '<td valign="top" class="tablebluerow'.$css_i.'">'.$recordAry[$i][5].'</td>'; // reason
				$x .= '<td valign="top" class="tablebluerow'.$css_i.'">'.$recordAry[$i][6].'</td>'; // remarks
				$x .= '<td valign="top" class="tablebluerow'.$css_i.'"><font style="color:red;">'.$error_detail.'</font></td>';
			$x .= '</tr>';
		}
	}else{
		$x .= '<tr>';
			$x .= '<td valign="top" class="tablebluerow" colspan="13" align="center">'.$Lang['General']['NoRecordAtThisMoment'].'</td>';
		$x .= '</tr>';
	}
	echo $x;
?>
			</tbody>
			</table>
			<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr>
			    	<td class="dotline"><img src="<?=$PATH_WRT_ROOT."images/".$LAYOUT_SKIN.'/10x10.gif'?>" width="10" height="1" /></td>
			    </tr>
			</table>
			<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr>
				    <td align="center" colspan="2">
<?php
			$x = '';
			if($TotalValidRecord > 0 && $TotalInvalidRecord==0)
			{
				$x .= $linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "submit", "", "", " class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"").'&nbsp;';
			}
			$x .= $linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "javascript:window.location.href='".$PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/dailyoperation/daily_lesson_attendance/index2.php'","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"");
			echo $x;
?>			
					</td>
				</tr>
			</table>
		</form>
  	</div>
</div>
<br />
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>