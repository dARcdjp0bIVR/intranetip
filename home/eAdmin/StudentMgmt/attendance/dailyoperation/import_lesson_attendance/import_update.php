<?php
// Editing by 
/*
 * 2016-09-02 (Carlos): Added [Reason].
 */
set_time_limit(60*60);
ini_set("memory_limit", "1024M");
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libstudentattendance_ui.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-LessonAttendance"] || !$_SESSION["SSV_PRIVILEGE"]["plugin"]["attendancelesson"]) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$linterface = new interface_html();
$LessonAttendUI = new libstudentattendance_ui();
$CurrentPageArr['StudentAttendance'] = 1;
//$CurrentPage = "PageLessonAttendance_ImportLessonAttendanceRecords";
$CurrentPage = "PageLessonAttendance_DailyOverview";

$current_academic_year_id = Get_Current_Academic_Year_ID();

$sql = "SELECT * FROM TEMP_IMPORT_LESSON_ATTENDANCE_RECORD";
$records = $LessonAttendUI->returnResultSet($sql);
$record_count = count($records);

$attend_overview_table = "SUBJECT_GROUP_ATTEND_OVERVIEW_".$current_academic_year_id;
$attend_student_table = "SUBJECT_GROUP_STUDENT_ATTENDANCE_".$current_academic_year_id;

$success_count = 0;

for($i=0;$i<$record_count;$i++){
	$attend_overview_id = trim($records[$i]['AttendOverviewID']);
	$lesson_date = $records[$i]['LessonDate'];
	$subject_group_id = $records[$i]['SubjectGroupID'];
	$room_allocation_id = $records[$i]['RoomAllocationID'];
	$start_time = $records[$i]['StartTime'];
	$end_time = $records[$i]['EndTime'];
	
	$student_id = $records[$i]['StudentID'];
	$attend_status = $records[$i]['AttendStatus'];
	$reason = $LessonAttendUI->Get_Safe_Sql_Query($records[$i]['Reason']);
	$remarks = $LessonAttendUI->Get_Safe_Sql_Query($records[$i]['Remarks']);
	
	$sql = "SELECT AttendOverviewID FROM $attend_overview_table WHERE SubjectGroupID='$subject_group_id' AND LessonDate='$lesson_date' AND RoomAllocationID='$room_allocation_id'";
	$overview_count = $LessonAttendUI->returnVector($sql);
	if(count($overview_count)>0) {
		$attend_overview_id = $overview_count[0];
	}else
	{
		$sql = "INSERT INTO $attend_overview_table (SubjectGroupID,LessonDate,RoomAllocationID,StartTime,EndTime,CreateBy,CreateDate,LastModifiedBy,LastModifiedDate) 
				VALUES ('$subject_group_id','$lesson_date','$room_allocation_id','$start_time','$end_time','".$_SESSION['UserID']."',NOW(),'".$_SESSION['UserID']."',NOW())";
		
		$insert_overview_success = $LessonAttendUI->db_db_query($sql);
		if($insert_overview_success) {
			$attend_overview_id = $LessonAttendUI->db_insert_id();
		}
	}
	
	if($attend_overview_id > 0) {
		$sql = "SELECT COUNT(*) FROM $attend_student_table WHERE AttendOverviewID='$attend_overview_id' AND StudentID='$student_id'";
		$student_count = $LessonAttendUI->returnVector($sql);
		
		if($student_count[0] == 0){
			$sql = "INSERT INTO $attend_student_table (AttendOverviewID,StudentID,AttendStatus,Reason,Remarks,CreateBy,CreateDate,LastModifiedBy,LastModifiedDate) 
					VALUES ('$attend_overview_id','$student_id','$attend_status','$reason','$remarks','".$_SESSION['UserID']."',NOW(),'".$_SESSION['UserID']."',NOW())";
					
			$insert_student_success = $LessonAttendUI->db_db_query($sql);
			if($insert_student_success){
				$success_count++;
			}
		}else{
			$sql = "UPDATE $attend_student_table 
					SET AttendStatus='$attend_status',Reason='$reason',Remarks='$remarks',LastModifiedBy='".$_SESSION['UserID']."',LastModifiedDate=NOW() 
					WHERE AttendOverviewID='$attend_overview_id' AND StudentID='$student_id'";
			$update_student_success = $LessonAttendUI->db_db_query($sql);
			if($update_student_success){
				$success_count++;
			}
		}
	}
}

if($success_count < $total_count) {
	$xmsg = "ImportUnsuccess";
}else{
	$xmsg = "ImportSuccess";
}


### Title ###
//$TAGS_OBJ[] = array($Lang['LessonAttendance']['DailyLessonStatus'],$PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/dailyoperation/daily_lesson_attendance/index2.php",0);
//$TAGS_OBJ[] = array($Lang['LessonAttendance']['ImportLessonAttendanceRecords']['Title'],$PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/dailyoperation/import_lesson_attendance/import.php",1);
$TAGS_OBJ[] = array($Lang['LessonAttendance']['DailyLessonStatus'],"",0);
$MODULE_OBJ = $LessonAttendUI->GET_MODULE_OBJ_ARR();

if($xmsg != ''){
	if(isset($Lang['General']['ReturnMessage'][$xmsg])){
		$msg = $Lang['General']['ReturnMessage'][$xmsg];
	}else{
		$msg = $xmsg;
	}
}

$PAGE_NAVIGATION[] = array($Lang['LessonAttendance']['DailyLessonStatus'],$PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/dailyoperation/daily_lesson_attendance/index2.php");
$PAGE_NAVIGATION[] = array($Lang['Btn']['Import']);
# step information
$STEPS_OBJ[] = array($Lang['General']['ImportArr']['ImportStepArr']['SelectCSVFile'], 0);
$STEPS_OBJ[] = array($Lang['General']['ImportArr']['ImportStepArr']['CSVConfirmation'], 0);
$STEPS_OBJ[] = array($Lang['General']['ImportArr']['ImportStepArr']['ImportResult'], 1);

$linterface->LAYOUT_START($msg);

?>
<br />
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td><?=$linterface->GET_NAVIGATION($PAGE_NAVIGATION)?></td>
	</tr>
	<tr>
		<td><?=$linterface->GET_STEPS($STEPS_OBJ)?></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td align="center">
		<?=$success_count.'&nbsp;'.$Lang['General']['ImportArr']['RecordsImportedSuccessfully']?>
		</td>
	</tr>
	<tr>
		<td class="dotline">
			<img src="<?=$image_path.'/'.$LAYOUT_SKIN.'/10x10.gif'?>" width="10" height="1" />
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td align="center">
			<?=$linterface->GET_ACTION_BTN($Lang['Btn']['Back'],"button","window.location='".$PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/dailyoperation/daily_lesson_attendance/index2.php';")?>
		</td>
	</tr>
</table>
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>