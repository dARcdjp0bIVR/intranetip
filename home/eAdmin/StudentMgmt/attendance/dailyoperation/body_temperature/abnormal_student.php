<?php
// Editing by
##################################### Change Log #####################################################

######################################################################################################
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent'] || $sys_custom['StudentAttendance']['RecordBodyTemperature'] != true) {
	header ("Location: /");
	intranet_closedb();
	exit();
}



if ($page_size_change == 1)
{
	setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
	$ck_page_size = $numPerPage;
}

# preserve table view
if ($ck_attend_bodytemperature_page_number!=$pageNo && $pageNo!="")
{
	setcookie("ck_attend_bodytemperature_page_number", $pageNo, 0, "", "", 0);
	$ck_attend_bodytemperature_page_number = $pageNo;
} else if (!isset($pageNo) && $ck_attend_bodytemperature_page_number!="")
{
	$pageNo = $ck_attend_bodytemperature_page_number;
}

if ($ck_attend_bodytemperature_page_order!=$order && $order!="")
{
	setcookie("ck_attend_bodytemperature_page_order", $order, 0, "", "", 0);
	$ck_attend_bodytemperature_page_order = $order;
} else if (!isset($order) && $ck_attend_bodytemperature_page_order!="")
{
	$order = $ck_attend_bodytemperature_page_order;
}

if ($ck_attend_bodytemperature_page_field!=$field && $field!="")
{
	setcookie("ck_attend_bodytemperature_page_field", $field, 0, "", "", 0);
	$ck_attend_bodytemperature_page_field = $field;
} else if (!isset($field) && $ck_attend_bodytemperature_page_field!="")
{
	$field = $ck_attend_bodytemperature_page_field;
}

if ($ck_attend_bodytemperature_startdate_field!=$StartDate && $StartDate!="")
{
	setcookie("ck_attend_bodytemperature_startdate_field", $StartDate, 0, "", "", 0);
	$ck_attend_bodytemperature_startdate_field = $StartDate;
} else if (!isset($StartDate) && $ck_attend_bodytemperature_startdate_field!="")
{
	$StartDate = $ck_attend_bodytemperature_startdate_field;
}

if ($ck_attend_bodytemperature_enddate_field!=$EndDate && $EndDate!="")
{
	setcookie("ck_attend_bodytemperature_enddate_field", $EndDate, 0, "", "", 0);
	$ck_attend_bodytemperature_enddate_field = $EndDate;
} else if (!isset($EndDate) && $ck_attend_bodytemperature_enddate_field!="")
{
	$EndDate = $ck_attend_bodytemperature_enddate_field;
}


$lc = new libcardstudentattend2();
$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageDailyOperation_BodyTemperature";


$linterface = new interface_html();

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$li = new libdbtable2007($field, $order, $pageNo);

$today = date("Y-m-d");
if($StartDate == '') $StartDate = $today;
if($EndDate == '') $EndDate = $today;

$filterMap = array('GetYKHQuery'=>1,'libdbtable'=>$li);
$filterMap['StartDate'] = $StartDate;
$filterMap['EndDate'] = $EndDate;
$filterMap['TemperatureStatus'] = '1';

$db_query_info = $lc->getBodyTemperatureRecords($filterMap);

$li->field_array = $db_query_info[0];
$li->sql = $db_query_info[1];
$li->no_col = sizeof($li->field_array)+1;
$li->title = "";
$li->column_array = $db_query_info[3];
$li->wrap_array = $db_query_info[4];
$li->IsColOff = "IP25_table";

if ($field=="" && $order=="") {
	$li->field = 0;
	$li->order = 0;
}

foreach($db_query_info[2] as $column_def)
{
	$li->column_list .= $column_def;
}


$TAGS_OBJ[] = array($Lang['StudentAttendance']['StudentBodyTemperatureStatus'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/dailyoperation/body_temperature/index.php", 0);
$TAGS_OBJ[] = array($Lang['StudentAttendance']['DailyAbnormalBodyTemperature'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/dailyoperation/body_temperature/abnormal_student.php", 1);
$TAGS_OBJ[] = array($Lang['StudentAttendance']['StudentBodyTemperatureRecords'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/dailyoperation/body_temperature/browse_by_student.php", 0);

$MODULE_OBJ = $lc->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

?>
<br />
<form name="form1" method="post" action="" onsubmit="return submitForm();">
    <table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
        <tr>
            <td valign="top" nowrap="nowrap" class="formfieldtitle tabletext" width="30%"><?= $i_StudentAttendance_Field_Date ?></td>
            <td valign="top" class="tabletext" width="70%">
				<?=$Lang['General']['From'].'&nbsp;'.$linterface->GET_DATE_PICKER("StartDate", $StartDate).$Lang['General']['To'].'&nbsp;'.$linterface->GET_DATE_PICKER("EndDate", $EndDate)?>
				<?=$linterface->Get_Form_Warning_Msg('Date_Error', $Lang['General']['JS_warning']['InvalidDateRange'], 'Error')?>
            </td>
        </tr>
        <tr>
            <td colspan="2" class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
        </tr>
        <tr>
            <td colspan="2" align="center" class="tabletext">
                <?= $linterface->GET_ACTION_BTN($button_view, "submit", "","submit2") ?>
            </td>
        </tr>

    </table>

	<div class="table_board">
		<?= $li->display()?>
		<br style="clear:both" />
		<input type="hidden" name="pageNo" id="pageNo" value="<?=$li->pageNo; ?>" />
		<input type="hidden" name="order" id="order" value="<?=$li->order; ?>" />
		<input type="hidden" name="field" id="field" value="<?=$li->field; ?>" />
		<input type="hidden" name="page_size_change" id="page_size_change" value="" />
		<input type="hidden" name="numPerPage" id="numPerPage" value="<?=$li->page_size?>" />
	</div>
</form>
<script type="text/javascript" language="javascript">
    function submitForm(){
        obj = document.form1;
        if(obj==null) return;
        StartDate = document.form1.StartDate;
        if(!check_date(StartDate,'<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>')){
            StartDate.focus();
            return false;
        }
        EndDate = document.form1.EndDate;
        if(!check_date(EndDate,'<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>')){
            EndDate.focus();
            return false;
        }
        if(compareDate(StartDate.value,EndDate.value,1) == 1) {
            alert("<?=$eDiscipline['Setting_JSWarning_StartDateLargerThanEndDate'];?>");
            return false;
        }

        return true;
    }
</script>
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>