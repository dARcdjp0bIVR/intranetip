<?php
// Editing by
##################################### Change Log #####################################################

######################################################################################################
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libcycleperiods.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent'] || $sys_custom['StudentAttendance']['RecordBodyTemperature'] != true) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();
$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageDailyOperation_BodyTemperature";


$libcycleperiods = new libcycleperiods();

$today = date("Y-m-d");
$start_date = strtotime($today);
$school_date = array();
$school_date[] = $today;
$k = 1;
while(true) {
    $current_date = date("Y-m-d", $start_date - ($k*60*60*24));
    $k++;

    $non_school_day = false;
	$arrEvents = $libcycleperiods->retriveEventsByDate($current_date);
	if(sizeof($arrEvents)>0) {
		for ($i = 0; $i < sizeof($arrEvents); $i++) {
			list($event_id, $event_title, $event_type, $skip_cycle_day) = $arrEvents[$i];
			if($skip_cycle_day == 1) {
				$non_school_day = true;
				break;
			}
		}
	}

	if($non_school_day) {
		continue;
	}
	$school_date[] = $current_date;
	if(count($school_date) >= 14) {
		break;
	}
}

$total_temperature_records_today = 0;
$total_abnormal_temperature_records_today = 0;
$max_value = 0;

$school_date = array_reverse($school_date);


$str_BarChart_Xaxis = array();
$str_BarChart_value = array();
$i = 0;
foreach($school_date as $date) {
    if($i == (count($school_date)-1)) {

		$sql = "SELECT count(*) as count FROM  
        CARD_STUDENT_BODY_TEMPERATURE_RECORD b
        WHERE
        RecordDate='$date'";

		$temp = $lc->returnArray($sql);
		$total_temperature_records_today = $temp[0]['count'];
	}

	$sql = "SELECT count(*) as count FROM  
        CARD_STUDENT_BODY_TEMPERATURE_RECORD b
        WHERE
        TemperatureStatus='1'
        AND RecordDate='$date'";

	$temp = $lc->returnArray($sql);
	$abnormal_count = $temp[0]['count'];

	if($abnormal_count > $max_value) {
		$max_value = $abnormal_count;
	}

	if($i == (count($school_date)-1)) {
		$total_abnormal_temperature_records_today = $abnormal_count;
	}

	$i++;
	$str_BarChart_Xaxis[] = $date;
	if($abnormal_count > 0) {
		$str_BarChart_value[] = $abnormal_count;
	} else {
		$str_BarChart_value[] = 'null';
	}
}

$max_value_str = '';
if($max_value < 40) {
	$max_value_str = 'max: 40,';
} elseif($max_value < 60) {
	$max_value_str = 'max: 60,';
}

$str_BarChart_Xaxis = "'".implode("','", $str_BarChart_Xaxis)."'";
$str_BarChart_value = implode(',', $str_BarChart_value);

$linterface = new interface_html();

$TAGS_OBJ[] = array($Lang['StudentAttendance']['StudentBodyTemperatureStatus'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/dailyoperation/body_temperature/index.php", 1);
$TAGS_OBJ[] = array($Lang['StudentAttendance']['DailyAbnormalBodyTemperature'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/dailyoperation/body_temperature/abnormal_student.php", 0);
$TAGS_OBJ[] = array($Lang['StudentAttendance']['StudentBodyTemperatureRecords'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/dailyoperation/body_temperature/browse_by_student.php", 0);

$MODULE_OBJ = $lc->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

?>



<script src="<?php echo $PATH_WRT_ROOT?>templates/highchart/highcharts.js"></script>
<script src="<?php echo $PATH_WRT_ROOT?>templates/highchart/modules/exporting.js"></script>

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td>
            <table width="95%" border="0" cellpadding="5" cellspacing="0" align="center" style="margin-bottom:16px;">
                <tbody>
                <tr>
                    <td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
                        <span class="sectiontitle"><?=$Lang['StudentAttendance']['BodyTemperatureTodayStatus']?> (<?=date("Y-m-d")?>)</span>
		            </td>
                    <td class="tabletext" width="70%"></td>
                </tr>
                <tr>
                    <td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?=$Lang['StudentAttendance']['BodyTemperatureRecord']?>
                    </td>
                    <td class="tabletext" width="70%"><?=$total_temperature_records_today?></td>
                </tr>
                <tr>
                    <td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?=$Lang['StudentAttendance']['AbnormalBodyTemperature']?>
                    </td>
                    <td class="tabletext" width="70%">
                        <?php
                        if($total_abnormal_temperature_records_today > 0) {
                            echo '<a class="tablelink" href="abnormal_student.php?StartDate='.date('Y-m-d').'&EndDate='.date('Y-m-d').'">'.$total_abnormal_temperature_records_today.'</a>';
                        } else {
                            echo $total_abnormal_temperature_records_today;
                        }
                        ?>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table width="95%" cellspacing="0" cellpadding="5" border="0" align="center">
                <tr>
                    <td>
                        <span class="sectiontitle"><?=$Lang['StudentAttendance']['BodyTemperatureLastSevenDaysRecord']?></span>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div id="container" style="min-width: 310px; max-width: 800px; height: 400px; margin: 0 auto"></div>
                    </td>
                </tr>
            </table>
        </td>

    </tr>
</table>



<script type="text/javascript">
    Highcharts.chart('container', {
        chart: {
            type: 'column'
        },
        credits: {
            enabled: false
        },
        title: {
            text: '<?=$Lang['StudentAttendance']['AbnormalBodyTemperatureStudentCount']?>'
        },
        subtitle: {
            text: null
        },
        xAxis: {
            categories: [<?php echo ($str_BarChart_Xaxis);?>],
            crosshair: true,
            title: {
                text: 'Date'
            }
        },
        yAxis: {
            allowDecimals: false,
            min: 0,
            <?=$max_value_str?>
            title: {
                text: ''
            }
        },
        tooltip: {
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            },
            column:{
                dataLabels: {
                    enabled: true
                }
            }
        },
        series: [{
            name: '<?=$eEnrollmentMenu['head_count']?>',
            data: [<?php echo (($str_BarChart_value));?>],
        },]
    });
</script>


<?php
$linterface->LAYOUT_STOP();
intranet_closedb();

?>