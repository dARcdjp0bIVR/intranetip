<?php
// Editing by
##################################### Change Log #####################################################

######################################################################################################
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent'] || $sys_custom['StudentAttendance']['RecordBodyTemperature'] != true) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

ini_set("memory_limit", "500M");

$lc = new libcardstudentattend2();
$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageDailyOperation_BodyTemperature";

$linterface = new interface_html();


$lclass = new libclass();


$select_class = $lclass->getSelectClassWithWholeForm("id='formClassSel' name='formClassId' onChange='changeClass(this)'", $formClassId, $Lang['SysMgr']['FormClassMapping']['AllClass'],$button_select, "", '', 0);

$class = "";
$select_single_class = false;
$studentIDs = array();
$select_student = "";
if($formClassId != '' && $formClassId != '#') {
	$targetYearClassIdAry = array();
    if($formClassId == '0') {
		$sql = "SELECT YearClassID FROM YEAR_CLASS WHERE AcademicYearID = '".Get_Current_Academic_Year_ID()."'";
		$targetYearClassIdAry = $lclass->returnVector($sql);
	} else if (is_numeric($formClassId)) {
		$sql = "SELECT YearClassID FROM YEAR_CLASS WHERE YearID = '$formClassId' And AcademicYearID = '".Get_Current_Academic_Year_ID()."'";
		$targetYearClassIdAry = $lclass->returnVector($sql);
	} else {
		$targetYearClassIdAry[] = substr($formClassId, 2);
		$select_single_class = true;
	}

	$namefield = getNameFieldWithClassNumberByLang("u.");
	$sql = "SELECT u.UserID,$namefield as name FROM INTRANET_USER u
            INNER JOIN YEAR_CLASS_USER as ycu ON (u.UserID = ycu.UserID)
            WHERE u.RecordType=2 AND u.RecordStatus IN (0,1,2) AND
            ycu.YearClassID IN ('".implode("','", (array)$targetYearClassIdAry)."')
            ORDER BY u.ClassName, u.ClassNumber";
	$temp = $lclass->returnArray($sql,2);
	$studentids_temp = array();
	foreach($temp as $temp_data) {
		$studentids_temp[] = $temp_data[0];
	}

	if($select_single_class) {
	    $title_select = "-- $button_select --</option><option value='0' ".(($studentID == '0')? "SELECTED":"").">".$Lang['SysMgr']['RoleManagement']['AllStudent'];
		$select_student = getSelectByArray($temp, "name='studentID'", $studentID, 0, 0, $title_select);
		if($studentID != '') {
		    if($studentID == '0') {
				$studentIDs = $studentids_temp;
			} else {
				$studentIDs[] = $studentID;
			}
		}
	} else {
		$studentIDs = $studentids_temp;
	}
}


# Get Year List
$years = $lc->getRecordYear();
$select_year = getSelectByValue($years, "name=\"Year\"",date('Y'),0,1);

# Month List
$months = $i_general_MonthShortForm;
$select_month = "<select name=\"Month\">";
if(isset($Month)) {
	$currMon = $Month - 1;
} else {
	$currMon = date('n') - 1;
}
for ($i=0; $i<sizeof($months); $i++)
{
	$month_name = $months[$i];
	$string_selected = "";
    if ($currMon == $i) {
		$string_selected = "SELECTED";
	}
	$select_month .= "<OPTION value=".($i+1)." $string_selected>".$month_name."</OPTION>\n";
}
$select_month .= "</SELECT>\n";


$li = new libdbtable2007($field, $order, $pageNo);


$TAGS_OBJ[] = array($Lang['StudentAttendance']['StudentBodyTemperatureStatus'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/dailyoperation/body_temperature/index.php", 0);
$TAGS_OBJ[] = array($Lang['StudentAttendance']['DailyAbnormalBodyTemperature'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/dailyoperation/body_temperature/abnormal_student.php", 0);
$TAGS_OBJ[] = array($Lang['StudentAttendance']['StudentBodyTemperatureRecords'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/dailyoperation/body_temperature/browse_by_student.php", 1);

$MODULE_OBJ = $lc->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

?>
	<br />
	<form name="form1" method="post" action="">
		<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
            <tr>
                <td valign="top" nowrap="nowrap" class="formfieldtitle tabletext" width="30%"><?= $i_UserClassName ?></td>
                <td valign="top" class="tabletext" width="70%"><?=$select_class?></td>
            </tr>
			<?php if($select_student!=""){?>
                <tr>
                    <td valign="top" nowrap="nowrap" class="formfieldtitle tabletext" width="30%"><?= $i_UserStudentName ?></td>
                    <td valign="top" class="tabletext" width="70%"><?=$select_student?></td>
                </tr>
			<?php } ?>
            <tr id="RowSelectYear">
                <td id="UseYear1" width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_general_Year?></td>
                <td id="UseYear2" width="70%" class="tabletext">
                    <?=$select_year?>
                </td>
            </tr>
            <tr id="RowSelectMonth">
                <td id="UseMonth1" width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_general_Month?></td>
                <td id="UseMonth2" width="70%" class="tabletext">
                    <?=$select_month?>
                </td>
            </tr>
            <tr>
                <td colspan="2" class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
            </tr>

            <tr>
                <td colspan="2" align="center" class="tabletext">
					<?= $linterface->GET_ACTION_BTN($button_view, "submit", "","submit2") ?>
                </td>
            </tr>

		</table>
    </form>

    <?php
        $ldiscipline = new libdisciplinev12();
        foreach($studentIDs as $studentID) { ?>
		<div class="table_board" style="width:95%">
			<?php

				$stu = $ldiscipline->getStudentNameByID($studentID);
				$stu = $stu[0];

				$filterMap = array('GetYKHStudentQuery' => 1, 'libdbtable' => $li);
				$filterMap['Year'] = $Year;
				$filterMap['Month'] = $Month;
				$filterMap['UserID'] = $studentID;

				$db_query = $lc->getBodyTemperatureRecords($filterMap);

				$rs = $lc->returnArray($db_query);
				$table_title_text = $Lang['StudentAttendance']['StudentName'].": ".$stu[1]."<br/>";
				$table_title_text .= $Lang['StudentAttendance']['ClassName'].": ".$stu['ClassName']."<br/>";
				$table_title_text .= $Lang['StudentAttendance']['ClassNumber'].": ".$stu['ClassNumber']."<br/>";
				$table_title_text .= $i_StaffAttendance_Month.": ".$i_general_MonthShortForm[$Month-1];

				$x = '<table class="common_table_list">
				  	<tbody>
				  	<tr>
				  		<td colspan="5">'.$table_title_text.'</td>
				  	</tr>
				  	<tr class="tabletop">
					    <th width="20%">'.$Lang['General']['Date'].'</th>
					    <th width="20%">'.$Lang['StudentAttendance']['BodyTemperature'].'</th>
						<th width="20%">'.$Lang['StudentAttendance']['AbnormalBodyTemperature'].'</th>
						<th width="20%">'.$Lang['StudentAttendance']['BodyTemperatureTime'].'</th>
						<th width="20%">'.$Lang['eDiscipline']['Detention']['AttendanceStatus'].'</th>
				    </tr>'."\n";
				if(empty($rs)) {
					$x .= '<tr><td colspan="5" align="center">'.$i_StudentAttendance_Report_NoRecord.'</td></tr>';
				}
				foreach($rs as $temp) {
					$temp_value = Get_String_Display($temp['TemperatureValue']);
					if($temp_value != '--') {
						$temp_value .= " &#8451;";
					}

					$x .= '<tr>';
					$x .= '<td>'.Get_String_Display($temp['RecordDate']).'</td>';
					$x .= '<td>'.$temp_value.'</td>';
					$x .= '<td>'.(($temp['TemperatureStatus'] == '1') ? '<font color="red">*</font>' : '').'</td>';
					$x .= '<td>'.Get_String_Display($temp['ModifiedDate']).'</td>';
					$x .= '<td>'.Get_String_Display($temp['Status_txt']).'</td>';
					$x .= '</tr>';
				}

				$x .= "</tbody></table>";
                echo $x;

				$year_data_array = array();
				$year_data = getAcademicYearAndYearTermByDate($Year."-".$Month."-01");
				$selectYear = $year_data['AcademicYearID'];
				$textFromDate = strtotime(getStartDateOfAcademicYear($selectYear, ''));
				$textToDate = strtotime(getEndDateOfAcademicYear($selectYear, ''));
				$start_year = date("Y", $textFromDate);
				$start_month = date("m", $textFromDate);
				$end_year = date("Y", $textToDate);
				$end_month = date("m", $textToDate);

				$i = 0;
				while(true) {
					$current_month = $start_month + $i;
					$current_year = $start_year;
					$i++;
					if ($current_month > 12) {
						$current_month = $current_month - 12;
						$current_year++;
					}
					if ($current_year >= $end_year) {
						if ($current_month > $end_month) {
							break;
						}
					}
					$year_data_array[] = date("Y-m-d", strtotime($current_year . "-" . $current_month . "-01"));
				}

				$student_year_data_array = array();
				foreach($year_data_array as $year_data) {
					$start_date = date("Y-m-d", strtotime($year_data));
					$end_date = date("Y-m-t", strtotime($year_data));
					$sql = "SELECT count(*) as count FROM CARD_STUDENT_BODY_TEMPERATURE_RECORD b 
                            WHERE
                            StudentID='".$studentID."' AND TemperatureStatus='1'
                            AND RecordDate>='$start_date' AND RecordDate<='$end_date'
                            ";
					$rs = $lc->returnArray($sql);
					if($rs) {
						if($rs[0]['count'] > 0) {
							$student_year_data_array[] = array('year'=>$year_data,
								'count'=>$rs[0]['count']);
						}
					}
				}

			?>
			<br style="clear:both" />
		</div>

        <table width="95%" cellspacing="0" cellpadding="5" border="0" align="center">
            <tbody><tr>
                <td align="left"><span class="sectiontitle"><?=$Lang['StudentAttendance']['BodyTemperatureYearReference']?></span></td>
            </tr>
            </tbody>
        </table>


        <?php
        if(count($student_year_data_array) > 0) {
            $width_size = 100/count($student_year_data_array);
            $width_size = round($width_size);
            ?>
            <table class="common_table_list" style="width: 95%">
                <tbody>
                <tr class="tabletop">
                    <?php foreach($student_year_data_array as $temp) { ?>
                        <th width="<?=$width_size?>%"><?=date("Y-m", strtotime($temp['year']))?></th>
                    <?php } ?>
                </tr>
                <tr>
                    <?php foreach($student_year_data_array as $temp) { ?>
                        <td><?=$temp['count']?><?=$Lang['eDiscipline']['Times']?></td>
                    <?php } ?>
                </tr>
                </tbody>
            </table>
        <?php } else { ?>
            <table class="common_table_list" style="width: 95%">
                <tbody>
                <tr>
                    <td><?=$i_StudentAttendance_Report_NoRecord?></td>
                </tr>
                </tbody>
            </table>
        <?php } ?>
        <br/><br/>
        <?php } ?>




<script type="text/javascript" language="javascript">
function changeClass(obj){
    var value = $(obj).val();
    if(value == '#' || value == '0' || isNaN(value) == false) {
        return;
    }
    document.form1.submit();
}
</script>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>