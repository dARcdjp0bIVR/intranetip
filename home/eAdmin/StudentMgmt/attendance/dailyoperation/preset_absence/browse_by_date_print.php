<?php
// Editing by 
##################################### Change Log #####################################################
# 2020-06-03 Ray: Add TW
# 2019-10-15 Ray: Added session From/To
# 2019-08-06 Ray: Added date range
# 2019-01-21 Carlos: Added [Approval Time] and [Approval User].
# 2019-01-02 Carlos: Added [Last Updated Time].
# 2014-12-08 Omas:	Add new column Hand-in Prove Document Status
#
######################################################################################################
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();

$linterface = new interface_html();

include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_header.php");

$filter = $filter==""?2:$filter;

$academic_year_startdate = date("Y-m-d",getStartOfAcademicYear());
$academic_year_enddate = date("Y-m-d",getEndOfAcademicYear());
$StartDate = $StartDate==""?$academic_year_startdate:$StartDate;
$EndDate = $EndDate==""?$academic_year_enddate:$EndDate;
$date_cond = " ((DATE_FORMAT(a.RecordDate,'%Y-%m-%d')) BETWEEN '$StartDate' AND '$EndDate')";

$order = ($order == 1) ? 1 : 0;
if ($field == "") $field = 0;

$has_apply_leave = $plugin['eClassApp']? true : false;

$namefield = getNameFieldByLang("b.");

if(get_client_region() == 'zh_TW') {
	$field_array = array("$namefield", "b.ClassName", "b.ClassNumber", "AttendanceType", "a.RecordDate", "EndDate","NumberOfDays","SessionFrom","SessionTo","SessionDiff","LeaveTypeName","a.Reason","a.Remark", "a.DateModified");
} else {
if($sys_custom['StudentAttendance']['SubmitAbsentLateRecordsToExternalSystem']) {
	$field_array = array("$namefield", "b.ClassName", "b.ClassNumber", "a.RecordDate", "a.DayPeriod", "a.SessionFrom", "a.SessionTo", "a.Reason", "Waive", "DocumentStatus", "a.Remark", "a.DateModified");
} else {
	$field_array = array("$namefield", "b.ClassName", "b.ClassNumber", "a.RecordDate", "a.DayPeriod", "a.Reason", "Waive", "DocumentStatus", "a.Remark", "a.DateModified");
}
}
if($has_apply_leave){
	$field_array[] = "a.ApprovedAt";
	$field_array[] = "ApprovedBy";
}
$order_str = " ORDER BY ".$field_array[$field];
$order_str.= ($order==1)?" ASC " : " DESC ";

if($StartDate!="" && $EndDate!=""){
	//$namefield = getNameFieldWithClassNumberByLang("b.");
	$lb = new libdb();
	
	$more_fields = "";
	$more_joins = "";
	if($has_apply_leave){
		$more_joins .= " LEFT JOIN INTRANET_USER as u ON u.UserID=a.ApprovedBy ";
		$approved_by_namefield = getNameFieldByLang("u.");
		$more_fields .= " ,a.ApprovedAt,$approved_by_namefield as ApprovedBy ";
	}

	if(get_client_region() == 'zh_TW') {
		$EmptySymbol = $Lang['General']['EmptySymbol'];
		$cond = '';
		if($attendanceType != '') {
			$cond .= " AND a.AttendanceType='".$lc->Get_Safe_Sql_Query($attendanceType)."'";
		}

		$sql = "SELECT RelatedRecordID FROM CARD_STUDENT_PRESET_LEAVE a
                LEFT OUTER JOIN INTRANET_USER AS b ON (a.StudentID = b.UserID) 
                WHERE $date_cond AND 
				($namefield LIKE '%$keyword%' OR a.Reason LIKE '%$keyword%' OR a.Remark LIKE '%$keyword%' )
				AND a.RelatedRecordID IS NOT NULL
				$cond GROUP BY RelatedRecordID";
		$rs = $lb->returnArray($sql);
		$relatedRecordIdAry = Get_Array_By_Key($rs, 'RelatedRecordID');
		$conds_recordId = '';
		if(!empty($relatedRecordIdAry)) {
			$conds_recordId = " OR a.RecordID IN (" . implode(',', $relatedRecordIdAry) . ")  ";
		}

		$lc->createCountSchoolDayFunction();
		$sql = " SELECT $namefield,
				b.ClassName,
				b.ClassNumber,
				CASE 
				    WHEN a.AttendanceType = 0 THEN '$i_DayTypeWholeDay'
				    WHEN a.AttendanceType = 1 THEN '$i_DayTypeAM'
				    WHEN a.AttendanceType = 2 THEN '$i_DayTypePM'
				    WHEN a.AttendanceType = 3 THEN '$i_DayTypeSession'
				END as AttendanceType,
			    a.RecordDate,
			    IF(p.RecordDate IS NULL, '$EmptySymbol', p.RecordDate) as EndDate,
		        IF(p.RecordDate IS NULL,'$EmptySymbol', count_schooldays(a.RecordDate, p.RecordDate)) as NumberOfDays,
				IF(a.AttendanceType = 3, a.SessionFrom, '$EmptySymbol') as SessionFrom,
				IF(a.AttendanceType = 3, a.SessionTo, '$EmptySymbol') as SessionTo, 
				IF(a.AttendanceType = 3, a.SessionTo - a.SessionFrom + 1, '$EmptySymbol') as SessionDiff, 
				lt.TypeName as LeaveTypeName, 
				 a.Reason,
				 a.Remark,
				 a.DateModified
				$more_fields 
				FROM CARD_STUDENT_PRESET_LEAVE AS a 
				LEFT OUTER JOIN INTRANET_USER AS b ON (a.StudentID = b.UserID) 
				$more_joins 
				LEFT JOIN CARD_STUDENT_PRESET_LEAVE p ON p.RecordID = (SELECT RecordID FROM CARD_STUDENT_PRESET_LEAVE WHERE RelatedRecordID=a.RecordID AND a.AttendanceType=0 ORDER BY RecordDate DESC LIMIT 1)
				LEFT JOIN CARD_STUDENT_LEAVE_TYPE lt ON a.LeaveTypeID = lt.TypeID
				WHERE ($date_cond AND 
				($namefield LIKE '%$keyword%' OR a.Reason LIKE '%$keyword%' OR a.Remark LIKE '%$keyword%' )
				AND a.RelatedRecordID IS NULL
				$cond)
				$conds_recordId
				$order_str
				";
	} else {
	$session_fields = "";
	if($sys_custom['StudentAttendance']['SubmitAbsentLateRecordsToExternalSystem']) {
		$session_fields .= " a.SessionFrom, a.SessionTo, ";
	}

		$leavetype_fields = '';
		$leavetype_join = '';
		if($sys_custom['StudentAttendance']['LeaveType']) {
			$leavetype_fields = ", lt.TypeName as LeaveTypeName ";
			$leavetype_join = "LEFT JOIN CARD_STUDENT_LEAVE_TYPE lt ON a.LeaveTypeID = lt.TypeID";
		}

	$sql=" SELECT $namefield, b.ClassName,b.ClassNumber,
	             a.RecordDate,
				 IF(a.DayPeriod=2,'$i_DayTypeAM','$i_DayTypePM') as daytype,
				 $session_fields
				 a.Reason,
				 IF(a.Waive = '1' AND a.Waive IS NOT NULL,'".$Lang['General']['Yes']."','".$Lang['General']['No']."') as Waive,
 				 IF(a.DocumentStatus = '1' AND a.DocumentStatus IS NOT NULL,'".$Lang['General']['Yes']."','".$Lang['General']['No']."') as DocumentStatus,
				 a.Remark,
				 a.DateModified 
				 $more_fields
				 $leavetype_fields
				FROM CARD_STUDENT_PRESET_LEAVE AS a 
				LEFT OUTER JOIN INTRANET_USER AS b ON (a.StudentID = b.UserID) 
				$more_joins 
				$leavetype_join
				WHERE $date_cond AND 
				($namefield LIKE '%$keyword%' OR a.Reason LIKE '%$keyword%' OR a.Remark LIKE '%$keyword%' )
				 $order_str ";
	}

	$temp = $lb->returnArray($sql);
	
	$display= "<table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\" class=\"eSporttableborder\">";
	$display.="<tr>";
	$display.= "<td width=\"1\" class=\"eSporttdborder eSportprinttabletitle\">#</td>\n";
	$display.= "<td width=\"10%\" class=\"eSporttdborder eSportprinttabletitle\">".$i_UserStudentName."</td>\n";
	$display.= "<td width=\"5%\" class=\"eSporttdborder eSportprinttabletitle\">".$i_ClassName."</td>\n";
	$display.= "<td width=\"5%\" class=\"eSporttdborder eSportprinttabletitle\">".$i_ClassNumber."</td>\n";
	if(get_client_region() == 'zh_TW') {
		$display.= "<td width=\"4%\" class=\"eSporttdborder eSportprinttabletitle\">".$i_Attendance_Type."</td>\n";
		$display.= "<td width=\"8%\" class=\"eSporttdborder eSportprinttabletitle\">".$i_general_startdate."</td>\n";
		$display.= "<td width=\"8%\" class=\"eSporttdborder eSportprinttabletitle\">".$i_general_enddate."</td>\n";
		$display.= "<td width=\"4%\" class=\"eSporttdborder eSportprinttabletitle\">".$i_StaffAttendance_Report_Number_Of_Date."</td>\n";
		$display.= "<td width=\"5%\" class=\"eSporttdborder eSportprinttabletitle\">".$Lang['StudentAttendance']['SessionFrom']."</td>\n";
		$display.= "<td width=\"5%\" class=\"eSporttdborder eSportprinttabletitle\">".$Lang['StudentAttendance']['SessionTo']."</td>\n";
		$display.= "<td width=\"5%\" class=\"eSporttdborder eSportprinttabletitle\">".$Lang['StudentAttendance']['TotalSession']."</td>\n";
		$display.= "<td width=\"4%\" class=\"eSporttdborder eSportprinttabletitle\">".$Lang['StudentAttendance']['LeaveType']."</td>\n";
	} else {
	$display.= "<td width=\"17%\" class=\"eSporttdborder eSportprinttabletitle\">".$i_StudentAttendance_Field_Date."</td>\n";
	$display.= "<td width=\"5%\" class=\"eSporttdborder eSportprinttabletitle\">".$i_Attendance_DayType."</td>\n";
	if($sys_custom['StudentAttendance']['SubmitAbsentLateRecordsToExternalSystem']) {
		$display.= "<td class=\"eSporttdborder eSportprinttabletitle\">".$Lang['StudentAttendance']['SessionFrom']."</td>\n";
		$display.= "<td class=\"eSporttdborder eSportprinttabletitle\">".$Lang['StudentAttendance']['SessionTo']."</td>\n";
	}
		if($sys_custom['StudentAttendance']['LeaveType']) {
			$display .= "<td class=\"eSporttdborder eSportprinttabletitle\">" . $Lang['StudentAttendance']['LeaveType'] . "</td>\n";
		}
	}

	$display.= "<td width=\"10%\" class=\"eSporttdborder eSportprinttabletitle\">".$i_Attendance_Reason."</td>\n";
	if(get_client_region() != 'zh_TW') {
	$display.= "<td width=\"5%\" class=\"eSporttdborder eSportprinttabletitle\">".$Lang['StudentAttendance']['Waived']."</td>\n";
	$display.= "<td width=\"5%\" class=\"eSporttdborder eSportprinttabletitle\">".$Lang['StudentAttendance']['ProveDocument']."</td>\n";
	}
	$display.= "<td width=\"10%\" class=\"eSporttdborder eSportprinttabletitle\">".$Lang['StudentAttendance']['OfficeRemark']."</td>\n";
	$display.= "<td width=\"10%\" class=\"eSporttdborder eSportprinttabletitle\">".$Lang['General']['LastUpdatedTime']."</td>\n";
	if($has_apply_leave){
		$display.= "<td width=\"10%\" class=\"eSporttdborder eSportprinttabletitle\">".$Lang['StudentAttendance']['ApprovalTime']."</td>\n";
		$display.= "<td width=\"8%\" class=\"eSporttdborder eSportprinttabletitle\">".$Lang['StudentAttendance']['ApprovalUser']."</td>\n";
	}
	for($i=0;$i<sizeof($temp);$i++){
		if(get_client_region() == 'zh_TW') {
			list($username, $classname, $classnum, $attendancetype, $startdate, $enddate, $no_of_days, $session_from, $sesion_to, $session_diff, $leave_type_name, $reason, $remark, $last_updated_time) = $temp[$i];
		} else {
		if($sys_custom['StudentAttendance']['SubmitAbsentLateRecordsToExternalSystem']) {
				list($username, $classname, $classnum, $record_date, $daytype, $session_from, $sesion_to, $reason, $Waive, $Handin_Prove, $remark, $last_updated_time) = $temp[$i];
		} else {
				list($username, $classname, $classnum, $record_date, $daytype, $reason, $Waive, $Handin_Prove, $remark, $last_updated_time) = $temp[$i];
			}
		}
		$css = $i%2==0?"tableContent":"tableContent2";
		$row = "<tr>";
		$row.= "<td class=\"eSporttdborder eSportprinttext\">".($i+1)."</td>";		
		$row.= "<td class=\"eSporttdborder eSportprinttext\">$username</td>";
		$row.= "<td class=\"eSporttdborder eSportprinttext\">$classname&nbsp;</td>";
		$row.= "<td class=\"eSporttdborder eSportprinttext\">$classnum&nbsp;</td>";
		if(get_client_region() == 'zh_TW') {
			$row.= "<td class=\"eSporttdborder eSportprinttext\">$attendancetype</td>";
			$row.= "<td class=\"eSporttdborder eSportprinttext\">$startdate</td>";
			$row.= "<td class=\"eSporttdborder eSportprinttext\">$enddate</td>";
			$row.= "<td class=\"eSporttdborder eSportprinttext\">$no_of_days</td>";
			$row.= "<td class=\"eSporttdborder eSportprinttext\">$session_from</td>";
			$row.= "<td class=\"eSporttdborder eSportprinttext\">$sesion_to</td>";
			$row.= "<td class=\"eSporttdborder eSportprinttext\">$session_diff</td>";
			$row.= "<td class=\"eSporttdborder eSportprinttext\">$leave_type_name</td>";
		} else {
			$row .= "<td class=\"eSporttdborder eSportprinttext\">$record_date</td>";
		$row.= "<td class=\"eSporttdborder eSportprinttext\">$daytype</td>";
		if($sys_custom['StudentAttendance']['SubmitAbsentLateRecordsToExternalSystem']) {
			$row.= "<td class=\"eSporttdborder eSportprinttext\">$session_from</td>";
			$row.= "<td class=\"eSporttdborder eSportprinttext\">$sesion_to</td>";
		}
			if($sys_custom['StudentAttendance']['LeaveType']) {
				$row.= "<td class=\"eSporttdborder eSportprinttext\">".$temp[$i]['LeaveTypeName']."</td>";
			}
		}

		$row.= "<td class=\"eSporttdborder eSportprinttext\">$reason&nbsp;</td>";		
		if(get_client_region() != 'zh_TW') {
		$row.= "<td class=\"eSporttdborder eSportprinttext\">$Waive&nbsp;</td>";
		$row.= "<td class=\"eSporttdborder eSportprinttext\">$Handin_Prove&nbsp;</td>";		
		}
		$row.= "<td class=\"eSporttdborder eSportprinttext\">$remark&nbsp;</td>";
		$row.= "<td class=\"eSporttdborder eSportprinttext\">$last_updated_time</td>";
		if($has_apply_leave){
			$row.= "<td class=\"eSporttdborder eSportprinttext\">".$temp[$i]['ApprovedAt']."&nbsp;</td>";
			$row.= "<td class=\"eSporttdborder eSportprinttext\">".$temp[$i]['ApprovedBy']."&nbsp;</td>";
		}
		$row.="</tr>";
		$display.=$row;
	}
	if(sizeof($temp)<=0){
		$row = "<tr><td class=\"eSportprinttext\" height=\"40\" colspan=\"".count($field_array)."\" align=\"center\">$i_no_record_exists_msg</td></tr>";
		$display.=$row;
	}
	$display.="</table>";
}

?>
<table width='100%' align='center' class='print_hide' border=0>
	<tr>
		<td align='right'><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></td>
	</tr>
</table>
<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
		<td class="eSportprinttext"><b><?=$i_SmartCard_DailyOperation_Preset_Absence?> (<?=$StartDate?> - <?=$EndDate?>)</b></td>
	</tr>
</table>
<?=$display?>
<?php
include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_footer.php");
intranet_closedb();
?>