<?php
##################################### Change Log #####################################################
# 2020-06-03 Ray:	Add TW
# 2020-02-11 Ray:   Added leaveType
# 2019-08-09 Ray:   Add session from/to
# 2014-12-08 Omas:	Add new column Hand-in Prove Document Status
#
######################################################################################################
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libcycleperiods.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}


if ($return_url=="") $return_url="new.php";


$values_session = "";
$fields_session = "";


if(get_client_region() == 'zh_TW') {
	if(sizeof($studentID)<=0 || in_array($dateType, array(0,1,2,3)) == false)
		header("Location: $return_url");


	$studentID = array_unique($studentID);
	$li = new libcardstudentattend2();

	$values_session = ", '".$li->Get_Safe_Sql_Query($leaveType)."','".$li->Get_Safe_Sql_Query($dateType)."'";
	$fields_session = ",LeaveTypeID, AttendanceType";

	$Result = array();
	$li->Start_Trans();

	if($dateType == 0) {
		$target_date = array();
		$date_diff = strtotime($target_end_date) - strtotime($target_start_date);
		$date_diff = round($date_diff/(60*60*24));
		for($i=0;$i<=$date_diff;$i++) {
			$current_date = date("Y-m-d", strtotime($target_start_date) + ($i*60*60*24));
			$target_date[] = $current_date;
		}

		if(count($target_date) == 1) {
			$td = $target_date[0];
			for ($i = 0; $i < sizeof($studentID); $i++) {
				$sid = $studentID[$i];

				$ret = $li->checkPresetLeaveOverlap($sid, $dateType, $td);
				if($ret[0] == true) {
					continue;
				}

				$values = "($sid,'$td',2,'$reason','$Waive','$HandIn_prove','$remark',NOW(),NOW(), NULL $values_session)";
				$sql = "INSERT IGNORE INTO CARD_STUDENT_PRESET_LEAVE (StudentID,RecordDate,DayPeriod,Reason,Waive,DocumentStatus,Remark,DateInput,DateModified, RelatedRecordID $fields_session) VALUES $values";
				$Result[] = $li->db_db_query($sql);
				$record_id = $li->db_insert_id();
				$related_record_id = $record_id;

				$values = "($sid,'$td',3,'$reason','$Waive','$HandIn_prove','$remark',NOW(),NOW(), '$related_record_id' $values_session)";
				$sql = "INSERT IGNORE INTO CARD_STUDENT_PRESET_LEAVE (StudentID,RecordDate,DayPeriod,Reason,Waive,DocumentStatus,Remark,DateInput,DateModified, RelatedRecordID $fields_session) VALUES $values";
				$Result[] = $li->db_db_query($sql);
			}
		} else if(count($target_date) > 1) {
			for ($i = 0; $i < sizeof($studentID); $i++) {
				$sid = $studentID[$i];
				$related_record_id = 'NULL';

				$ret = $li->checkPresetLeaveOverlap($sid, $dateType, $target_date[0], end($target_date));
				if($ret[0] == true) {
					continue;
				}

				foreach($target_date as $date_index=>$date) {
					$td = $date;
					if($date_index == 0) {
						$values = "($sid,'$td',2,'$reason','$Waive','$HandIn_prove','$remark',NOW(),NOW(), NULL $values_session)";
						$sql = "INSERT IGNORE INTO CARD_STUDENT_PRESET_LEAVE (StudentID,RecordDate,DayPeriod,Reason,Waive,DocumentStatus,Remark,DateInput,DateModified,RelatedRecordID $fields_session) VALUES $values";
						$Result[] = $li->db_db_query($sql);
						$record_id = $li->db_insert_id();
						$related_record_id = $record_id;

						$values = "($sid,'$td',3,'$reason','$Waive','$HandIn_prove','$remark',NOW(),NOW(), '$related_record_id' $values_session)";
						$sql = "INSERT IGNORE INTO CARD_STUDENT_PRESET_LEAVE (StudentID,RecordDate,DayPeriod,Reason,Waive,DocumentStatus,Remark,DateInput,DateModified,RelatedRecordID $fields_session) VALUES $values";
						$Result[] = $li->db_db_query($sql);
					} else {
						$values = "($sid,'$td',2,'$reason','$Waive','$HandIn_prove','$remark',NOW(),NOW(), '$related_record_id' $values_session)";
						$values .= ", ($sid,'$td',3,'$reason','$Waive','$HandIn_prove','$remark',NOW(),NOW(), '$related_record_id' $values_session)";
						$sql = "INSERT IGNORE INTO CARD_STUDENT_PRESET_LEAVE (StudentID,RecordDate,DayPeriod,Reason,Waive,DocumentStatus,Remark,DateInput,DateModified,RelatedRecordID $fields_session) VALUES $values";
						$Result[] = $li->db_db_query($sql);
					}
				}
			}
		}
	} else if($dateType == 1 || $dateType == 2) {
		$values = "";
		$delim = "";
		$td = $target_start_date;
		for ($i = 0; $i < sizeof($studentID); $i++) {
			$sid = $studentID[$i];

			$ret = $li->checkPresetLeaveOverlap($sid, $dateType, $td);
			if($ret[0] == true) {
				continue;
			}

			if($dateType == 1) {
				$values .= $delim . "($sid,'$td',2,'$reason','$Waive','$HandIn_prove','$remark',NOW(),NOW() $values_session)";
			} else if($dateType == 2) {
				$values .= $delim . "($sid,'$td',3,'$reason','$Waive','$HandIn_prove','$remark',NOW(),NOW() $values_session)";
			}
			$delim = ",";
		}
		if($values != "") {
			$sql = "INSERT IGNORE INTO CARD_STUDENT_PRESET_LEAVE (StudentID,RecordDate,DayPeriod,Reason,Waive,DocumentStatus,Remark,DateInput,DateModified $fields_session) VALUES $values";
			$Result[] = $li->db_db_query($sql);
		}
	} else if($dateType == 3) {
		if($SessionTo >= $SessionFrom) {
			$td = $target_start_date;

			for ($i = 0; $i < sizeof($studentID); $i++) {
				$sid = $studentID[$i];
				$ret = $li->checkPresetLeaveOverlap($sid, $dateType, $td, $td, $SessionFrom, $SessionTo);
				if ($ret[0] == true) {
					continue;
				}
				$target_date_period = $ret[1];

				$related_record_id = '';
				if (in_array('am', $target_date_period)) {
					$values = "($sid,'$td',2,'$reason','$Waive','$HandIn_prove','$remark',NOW(),NOW(), '" . $li->Get_Safe_Sql_Query($SessionFrom) . "','" . $li->Get_Safe_Sql_Query($SessionTo) . "' $values_session)";
					$sql = "INSERT IGNORE INTO CARD_STUDENT_PRESET_LEAVE (StudentID,RecordDate,DayPeriod,Reason,Waive,DocumentStatus,Remark,DateInput,DateModified,SessionFrom,SessionTo $fields_session) VALUES $values";
					$Result[] = $li->db_db_query($sql);
					$record_id = $li->db_insert_id();
					$related_record_id = $record_id;
				}

				if (in_array('pm', $target_date_period)) {
					$values = "($sid,'$td',3,'$reason','$Waive','$HandIn_prove','$remark',NOW(),NOW(), '" . $li->Get_Safe_Sql_Query($SessionFrom) . "','" . $li->Get_Safe_Sql_Query($SessionTo) . "' $values_session)";
					$sql = "INSERT IGNORE INTO CARD_STUDENT_PRESET_LEAVE (StudentID,RecordDate,DayPeriod,Reason,Waive,DocumentStatus,Remark,DateInput,DateModified,SessionFrom,SessionTo $fields_session) VALUES $values";
					$Result[] = $li->db_db_query($sql);
					$record_id = $li->db_insert_id();
					if ($related_record_id != '' && $record_id != '') {
						$sql = "UPDATE CARD_STUDENT_PRESET_LEAVE SET RelatedRecordID='$related_record_id' WHERE RecordID='$record_id'";
						$Result[] = $li->db_db_query($sql);
					}
				}
			}
		}
	}

	if(in_array(false, $Result)) {
		$Result = false;
		$li->RollBack_Trans();
	} else {
		if(count($Result) == 0) {
			$Result = false;
		} else {
			$Result = true;
		}
		$li->Commit_Trans();
	}

} else {
	if(sizeof($studentID)<=0 || sizeof($target_date)<=0 || in_array($dateType, array(0,1,2)) == false)
		header("Location: $return_url");

if($sys_custom['StudentAttendance']['SubmitAbsentLateRecordsToExternalSystem']) {
		if($SessionFrom == '') {
			$SessionFrom_value = "NULL";
		} else {
			$SessionFrom_value = "'$SessionFrom'";
		}

		if($SessionTo == '') {
			$SessionTo_value = "NULL";
		} else {
			$SessionTo_value = "'$SessionTo'";
		}
		$values_session .= ", $SessionFrom_value,$SessionTo_value";
		$fields_session .= ",SessionFrom,SessionTo";
	}


	if($sys_custom['StudentAttendance']['LeaveType']) {
		$values_session .= ", '$leaveType'";
		$fields_session .= ",LeaveTypeID";
}

$studentID = array_unique($studentID);
$target_date = array_unique($target_date);

$values="";
$delim="";
$li = new libcardstudentattend2();

$late_records_ids = array();
for($i=0;$i<sizeof($studentID);$i++){
	$sid =$studentID[$i];
	for($j=0;$j<sizeof($target_date);$j++){
		$td = $target_date[$j];
			if ($dateType == 1 || $dateType == 0) {
			$DayPeriod = PROFILE_DAY_TYPE_AM;
				$sql = "SELECT count(*) as count FROM CARD_STUDENT_PRESET_LEAVE WHERE StudentID='$sid' AND RecordDate='$td' AND DayPeriod='2'";
				$rs = $li->returnArray($sql);
				if($rs[0]['count'] == 0) {
			$values.= $delim."($sid,'$td',2,'$reason','$Waive','$HandIn_prove','$remark',NOW(),NOW() $values_session)";
			$delim=",";
		}
			}
			if ($dateType == 2 || $dateType == 0) {
			$DayPeriod = PROFILE_DAY_TYPE_PM;
				$sql = "SELECT count(*) as count FROM CARD_STUDENT_PRESET_LEAVE WHERE StudentID='$sid' AND RecordDate='$td' AND DayPeriod='3'";
				$rs = $li->returnArray($sql);
				if($rs[0]['count'] == 0) {
			$values.= $delim."($sid,'$td',3,'$reason','$Waive','$HandIn_prove','$remark',NOW(),NOW() $values_session)";
			$delim=",";
		}
			}
			if ($dateType == 0) {
			$DayPeriod = PROFILE_DAY_TYPE_WD;
		}

		if($sys_custom['StudentAttendance']['SubmitAbsentLateRecordsToExternalSystem']) {
			$ret = $li->InsertSubmittedAbsentLateRecord($sid, $td, $DayPeriod, '', '', CARD_LEAVE_NORMAL, $SessionFrom, $SessionTo);
			if($ret) {
				$late_records_ids[] = $li->db_insert_id();
			}
		}
	}
}


if($sys_custom['StudentAttendance']['SubmitAbsentLateRecordsToExternalSystem']) {
	if(count($late_records_ids) > 0) {
		$site = (checkHttpsWebProtocol()?"https":"http")."://".$_SERVER['SERVER_NAME'].($_SERVER["SERVER_PORT"]!= 80?":".$_SERVER["SERVER_PORT"]:"");
		$cmd = "/usr/bin/nohup wget --no-check-certificate -q '$site/schedule_task/send_student_absent_late_record.php?RecordIDs=".OsCommandSafe(implode(',', $late_records_ids))."' > /dev/null &";
		shell_exec($cmd);
	}
}

$sql=" INSERT IGNORE INTO CARD_STUDENT_PRESET_LEAVE (StudentID,RecordDate,DayPeriod,Reason,Waive,DocumentStatus,Remark,DateInput,DateModified $fields_session) VALUES $values";


$Result = $li->db_db_query($sql);

}

intranet_closedb();

/*
$t = explode("?",$return_url);
$filename = $t[0];
$str = $t[1];
$ary = explode("&",$str);
$found = false;
for($i=0;$i<sizeof($ary);$i++){
	if(strpos($ary[$i],"msg=")!==false){
			$ary[$i]="msg=1";
			$found = true;
			break;
	}
}
if(!$found){
	array_push($ary,"msg=1");
}
$return_url = $filename."?".implode("&",$ary);
*/

$t = explode("?",$return_url);
$filename = $t[0];
$str = $t[1];
$ary = explode("&",$str);
$found = false;

if ($Result) 
	$Msg = $Lang['StudentAttendance']['PresetAbsenceCreateSuccess'];
else 
	$Msg = $Lang['StudentAttendance']['PresetAbsenceCreateFail'];

for($i=0;$i<sizeof($ary);$i++){
	if(strpos($ary[$i],"Msg=") != false) {
			$ary[$i]="Msg=".urlencode($Msg);
			$found = true;
			break;
	}
}
if(!$found){
	array_push($ary,"Msg=".urlencode($Msg));
}
$return_url = $filename."?".implode("&",$ary);
header("Location: $return_url");
?>
