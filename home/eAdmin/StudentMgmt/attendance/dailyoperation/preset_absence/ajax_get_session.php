<?php
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");

intranet_opendb();

if ($_SESSION["UserID"] == "") {
	echo '0';
	intranet_closedb();
	exit();
}

$libcardstudentattend = new libcardstudentattend2();
$res = $libcardstudentattend->getDateTimeTableID($targetDate);
echo $libcardstudentattend->getDateSessionCount($res);

intranet_closedb();
