<?php // editing by 
##################################### Change Log #####################################################
# 2020-06-03 Ray:   Add TW
# 2020-02-11 Ray:   Added leaveType
# 2019-08-09 Ray:   Add session from/to
# 2014-12-08 Omas:	Add new column Hand-in Prove Document Status
#
######################################################################################################
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libwordtemplates.php");
include_once($PATH_WRT_ROOT."includes/libcycleperiods.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();
$lword = new libwordtemplates();
$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageDailyOperation_PresetAbsence";

$linterface = new interface_html();

$return_url = $return_url==""?$_SERVER['HTTP_REFERER']:$return_url;
if (strstr($return_url,'browse_by_date.php') === FALSE && strstr($return_url,'browse_by_student.php') === FALSE) 
	$return_url = "new.php";

$lclass = new libclass();
$lc = new libcardstudentattend2();
$lc->retrieveSettings();

if($studentID=="")
	$studentID = array();
else if(!is_array($studentID))
	$studentID = array($studentID);

if($target_date=="")
	$target_date = array();
else if(!is_array($target_date))
	$target_date = array($target_date);
	
// prepare preset WEBSAMS reason list
$words_absence = $lword->getWordListAttendance(1);
$AbsentHasWord = sizeof($words_absence)!=0;
foreach($words_absence as $key=>$word)
	$words_absence[$key]= htmlspecialchars($word);

# submitType 1 : add by class name ( class number)
# submitType 2 : add by user login 
if($submitType==1 && $user_id!=""){
	if(!in_array($user_id,$studentID))
		array_push($studentID,$user_id);
}
else if($submitType==2 && $user_login !=""){
	$sql ="SELECT UserID FROM INTRANET_USER WHERE UserLogin='".$lc->Get_Safe_Sql_Query($user_login)."' AND RecordType=2 AND RecordStatus IN (0,1,2)";
	
	$temp = $lclass->returnVector($sql);
	if($temp[0]!="" && !in_array($temp,$studentID))
		array_push($studentID,$temp[0]);
}

if(sizeof($studentID)>0){
	$selected_student_list = implode(",",IntegerSafe($studentID));
	$namefield = getNameFieldWithClassNumberByLang("");
	$sql ="SELECT UserID,$namefield FROM INTRANET_USER WHERE RecordType=2 AND RecordStatus IN (0,1,2) AND UserID IN($selected_student_list) ORDER BY ClassName,ClassNumber";
	
	$temp = $lclass->returnArray($sql,2);
}else{
	 $temp = array();
	 $selected_student_list="''";
}

$select_student = getSelectByArray($temp," name=\"studentID[]\" size=\"5\" multiple","",0,1);


$select_class = $lclass->getSelectClass("name=\"class\" onChange=\"changeClass()\"",$class,0);

if($class!=""){
	$namefield = getNameFieldWithClassNumberByLang("");
	$sql ="SELECT UserID,$namefield FROM INTRANET_USER WHERE RecordType=2 AND RecordStatus IN (0,1,2) AND ClassName='".$lc->Get_Safe_Sql_Query($class)."' AND UserID NOT IN ($selected_student_list) ORDER BY ClassName,ClassNumber";
	$temp = $lclass->returnArray($sql,2);
	$select_classnum=getSelectByArray($temp,"name='user_id'",$user_id,0,0);
	$btn = $linterface->GET_BTN($button_add, "button", "addStudent(1)");
	$select_classnum.="&nbsp$btn";
}

# select day type
if(get_client_region() == 'zh_TW') {
if($lc->attendance_mode==2 || $lc->attendance_mode == 3){
		$select_datetype = "<input type=\"radio\" name=\"dateType\" id=\"dateTypeWholeDay\" value=\"0\"" . ($dateType == 0 ? " CHECKED" : "") . "><label for=\"dateTypeWholeDay\">$i_DayTypeWholeDay</label>&nbsp;&nbsp;";
		$select_datetype .= "<input type=\"radio\" name=\"dateType\" id=\"dateTypeAM\" value=\"1\"" . ($dateType == 1 ? " CHECKED" : "") . "><label for=\"dateTypeAM\">$i_DayTypeAM</label>&nbsp;&nbsp;";
		$select_datetype .= "<input type=\"radio\" name=\"dateType\" id=\"dateTypePM\" value=\"2\"" . ($dateType == 2 ? " CHECKED" : "") . "><label for=\"dateTypePM\">$i_DayTypePM</label>&nbsp;&nbsp;";
		$select_datetype .= "<input type=\"radio\" name=\"dateType\" id=\"dateTypeSession\" value=\"3\"" . ($dateType == 3 ? " CHECKED" : "") . "><label for=\"dateTypeSession\">$i_DayTypeSession</label>";
}else if($lc->attendance_mode==1){
		$select_datetype .= "<input type=\"radio\" name=\"dateType\" id=\"dateTypePM\" value=\"2\"" . ($dateType == 2 ? " CHECKED" : "") . "><label for=\"dateTypePM\">$i_DayTypePM</label>&nbsp;&nbsp;";
		$select_datetype .= "<input type=\"radio\" name=\"dateType\" id=\"dateTypeSession\" value=\"3\"" . ($dateType == 3 ? " CHECKED" : "") . "><label for=\"dateTypeSession\">$i_DayTypeSession</label>";
}else {
		$select_datetype .= "<input type=\"radio\" name=\"dateType\" id=\"dateTypeAM\" value=\"1\"" . ($dateType == 1 ? " CHECKED" : "") . "><label for=\"dateTypeAM\">$i_DayTypeAM</label>&nbsp;&nbsp;";
		$select_datetype .= "<input type=\"radio\" name=\"dateType\" id=\"dateTypeSession\" value=\"3\"" . ($dateType == 3 ? " CHECKED" : "") . "><label for=\"dateTypeSession\">$i_DayTypeSession</label>";
	}
} else {
	if ($lc->attendance_mode == 2 || $lc->attendance_mode == 3) {
		$select_datetype = "<input type=\"radio\" name=\"dateType\" id=\"dateTypeWholeDay\" value=\"0\"" . ($dateType == 0 ? " CHECKED" : "") . "><label for=\"dateTypeWholeDay\">$i_DayTypeWholeDay</label>&nbsp;&nbsp;";
		$select_datetype .= "<input type=\"radio\" name=\"dateType\" id=\"dateTypeAM\" value=\"1\"" . ($dateType == 1 ? " CHECKED" : "") . "><label for=\"dateTypeAM\">$i_DayTypeAM</label>&nbsp;&nbsp;";
		$select_datetype .= "<input type=\"radio\" name=\"dateType\" id=\"dateTypePM\" value=\"2\"" . ($dateType == 2 ? " CHECKED" : "") . "><label for=\"dateTypePM\">$i_DayTypePM</label>";
	} else if ($lc->attendance_mode == 1) {
		$select_datetype = "<input type=\"hidden\" name=\"dateType\" value=\"2\" CHECKED>$i_DayTypePM";
	} else {
		$select_datetype = "<input type=\"hidden\" name=\"dateType\" value=\"1\" CHECKED>$i_DayTypeAM";
	}
}

if(isset($reason)){
	$reason = stripslashes($reason);
}
if(isset($remark)){
	$remark = stripslashes($remark);
}

$selSessionFrom = "<select name=\"SessionFrom\" id=\"SessionFrom\"/>";
$selSessionTo = "<select name=\"SessionTo\" id=\"SessionTo\"/>";

if(get_client_region() == 'zh_TW') {
    if($target_start_date == '') {
		$target_start_date = date("Y-m-d");
	}

	$res = $lc->getDateTimeTableID($target_start_date);
	$session_count = $lc->getDateSessionCount($res);

	for($i=1;$i<=$session_count;$i++) {
		$selSessionFrom .= "<option value=\"" . $i . "\" >" . $i . "</option>";
		$selSessionTo .= "<option value=\"" . $i . "\" >" . $i . "</option>";	}
} else {
    if($sys_custom['StudentAttendance']['SubmitAbsentLateRecordsToExternalSystem']) {
		$selSessionFrom .= "<option value=\"\" >-</option>";
		$selSessionTo .= "<option value=\"\" >-</option>";
	}
$k=0.0;
while($k<=CARD_STUDENT_MAX_SESSION) {
	$selSessionFrom .= "<option value=\"".$k."\" >".$k."</option>";
	$selSessionTo .= "<option value=\"".$k."\" >".$k."</option>";
	$k+= $sys_custom['SmartCardAttendance_StudentAbsentSessionStep'] > 0 ? $sys_custom['SmartCardAttendance_StudentAbsentSessionStep'] : 0.5;
}
}
$selSessionFrom .= "</select>\n";
$selSessionTo .= "</select>\n";

$selLeaveType = '';
if($sys_custom['StudentAttendance']['LeaveType'] || get_client_region() == 'zh_TW') {
	$sql ="SELECT TypeID,TypeName FROM CARD_STUDENT_LEAVE_TYPE ORDER BY TypeSymbol,TypeName";
	$temp = $lc->returnArray($sql,2);
	$selLeaveType = getSelectByArray($temp," name=\"leaveType\"", $leaveType,0,0);
}

$TAGS_OBJ[] = array($button_new, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/dailyoperation/preset_absence/new.php", 1);
$TAGS_OBJ[] = array($i_SmartCard_DailyOperation_Preset_Absence_BrowseByStudent, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/dailyoperation/preset_absence/browse_by_student.php", 0);
$TAGS_OBJ[] = array($i_SmartCard_DailyOperation_Preset_Absence_BrowseByDate, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/dailyoperation/preset_absence/browse_by_date.php", 0);

#$PAGE_NAVIGATION[] = array("$TargetDate ($display_period)");

$MODULE_OBJ = $lc->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START(urldecode($Msg));

if ($msg == 1) $SysMsg = $linterface->GET_SYS_MSG("add");
if ($msg == 2) $SysMsg = $linterface->GET_SYS_MSG("update");
?>
<style type="text/css">
/*
.select_group{width:200px; height=150px;}
*/
</style>
<script language="javascript">
$(document).ready(function() {
    <?php if(get_client_region() == 'zh_TW') { ?>
    $("input[name='dateType']").change(function() {
        dateTypeChange(this);
    });
    dateTypeChange($("input[name='dateType']:checked"));
    <?php } ?>
});

function TargetStartDateChanged(obj) {

    var PostVar = {
        "targetDate": $(obj).val()
    };

    $.post('<?=$PATH_WRT_ROOT?>home/eAdmin/StudentMgmt/attendance/dailyoperation/preset_absence/ajax_get_session.php',PostVar,
        function (data) {
            displaySession(data);
        });
}

function displaySession(value) {
    var text = '';
    for(var i=1;i<=value;i++) {
        text += '<option value="'+i+'">'+i+'</option>';
    }
    $("#SessionFrom").html(text);
    $("#SessionTo").html(text);
}

function dateTypeChange(obj) {
    $("input[name='target_start_date']").closest('tr').hide();
    $("input[name='target_end_date']").closest('tr').hide();
    $("select[name='SessionFrom']").closest('tr').hide();
    $("select[name='SessionTo']").closest('tr').hide();
    if($(obj).val() == '0') {
        $("input[name='target_start_date']").closest('tr').show();
        $("input[name='target_end_date']").closest('tr').show();
    } else if($(obj).val() == '1') {
        $("input[name='target_start_date']").closest('tr').show();
    } else if($(obj).val() == '2') {
        $("input[name='target_start_date']").closest('tr').show();
    }  else if($(obj).val() == '3') {
        $("input[name='target_start_date']").closest('tr').show();
        $("select[name='SessionFrom']").closest('tr').show();
        $("select[name='SessionTo']").closest('tr').show();
    }
}

function changeClass(){
	submitForm('new.php');
}

function submitForm(newurl){
	obj = document.form1;
	studentObjs = document.getElementsByName('studentID[]');
	studentObj = studentObjs[0];
	if(obj==null || studentObj==null) return;
	for(i=0;i<studentObj.options.length;i++){
		studentObj.options[i].selected = true;
	}
	obj.action=newurl;
	obj.submit();
}
function addStudent(stype){
	obj = document.form1.submitType;
	if(obj==null) return;
	obj.value = stype;
	submitForm('new.php');
}
function addDate(count){
	document.form1.dateFieldCount.value = count+1;
	obj = document.getElementById('dates');
	if(obj==null) return;
	$.post('ajax_get_new_date_picker.php',{"DateCount":count},function(data){
			if (data == "die") 
				window.top.location = '/';
			else {
				$('span#dates').append(data);
			}
		});
}

<?php if(get_client_region() == 'zh_TW') { ?>
function checkform(){
    objStudent = document.getElementsByName('studentID[]')[0];
    var objDateType = $("#form1 input[name='dateType']:checked");
    var objLeaveType  = $("#form1 select[name='leaveType']");

    if(objStudent==null || objDateType.size() == 0) return;

    if(objStudent.options.length<=0){
        alert('<?=$i_Discipline_System_alert_PleaseSelectStudent?>');
        return;
    }
	
    if($(objDateType).val() == '0') {
        StartDate = document.form1.target_start_date;
        if(!check_date(StartDate,'<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>')){
            StartDate.focus();
            return;
        }
        EndDate = document.form1.target_end_date;
        if(!check_date(EndDate,'<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>')){
            EndDate.focus();
            return;
        }
        if(compareDate(StartDate.value,EndDate.value,1) == 1) {
            alert("<?=$eDiscipline['Setting_JSWarning_StartDateLargerThanEndDate'];?>");
            return;
        }
    } else if($(objDateType).val() == '1') {

    }  else if($(objDateType).val() == '2') {

    }  else if($(objDateType).val() == '3') {
        objSessionFrom = document.form1.SessionFrom;
        objSessionTo = document.form1.SessionTo;
        if(objSessionFrom!=null && objSessionTo!=null) {
            if(Number(objSessionFrom.value) > Number(objSessionTo.value)) {
                alert('<?=$Lang['StudentAttendance']['PlsSelectSession']?>');
                return;
            }
        }
    }

    if($(objLeaveType).size() > 0) {
        if($(objLeaveType).val() == '') {
            alert('<?=$i_StudentAttendance_Report_PlsSelectLeaveType?>');
            return;
        }
    }

    if($("#reason").val() == '') {
        alert('<?=$i_StudentAttendance_Reminder_InputReasonWarning?>');
        return;
    }

    submitForm('new_update.php');
}
<?php } else { ?>
function checkform(){
	objStudent = document.getElementsByName('studentID[]')[0];
	objTargetDate = document.getElementsByName('target_date[]');
	var objDateType = $("#form1 input[name='dateType']");
    var objLeaveType  = $("#form1 select[name='leaveType']");

	if(objStudent==null || objTargetDate ==null || objDateType.size() == 0) return;
	
	if(objStudent.options.length<=0){
		alert('<?=$i_Discipline_System_alert_PleaseSelectStudent?>');
		 return;
	}
	for(i=0;i<objTargetDate.length;i++){
		if(!check_date(objTargetDate[i],'<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>')){
			objTargetDate[i].focus();
			return;
		}
	}
	if($(objDateType+":checked").size() == 0) {
		alert('<?=$i_StudentAttendance_Report_PlsSelectSlot?>');
		return;
	}

	<?php if($sys_custom['StudentAttendance']['SubmitAbsentLateRecordsToExternalSystem']) { ?>
    objSessionFrom = document.form1.SessionFrom;
    objSessionTo = document.form1.SessionTo;
    if(objSessionFrom!=null && objSessionTo!=null) {
        if(objSessionFrom.value == '' && objSessionFrom.value != objSessionTo.value) {
            alert('<?=$Lang['StudentAttendance']['PlsSelectSession']?>');
            return;
        }
        if(Number(objSessionFrom.value) > Number(objSessionTo.value)) {
            alert('<?=$Lang['StudentAttendance']['PlsSelectSession']?>');
            return;
        }
    }
    <?php } ?>

    if($(objLeaveType).size() > 0) {
        if($(objLeaveType).val() == '') {
            alert('<?=$i_StudentAttendance_Report_PlsSelectLeaveType?>');
            return;
        }
    }

	submitForm('new_update.php');
}
<?php } ?>
</script>
<br />
<form name="form1" id="form1" method="POST">
<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
	<tr>
		<td colspan="2">
			<?=$linterface->GET_LNK_IMPORT("import.php",$Lang['Btn']['Import'],"","","",0)?>
		</td>
	</tr>
	<tr>
		<td colspan="2" align="right"><?=$SysMsg?></td>
	</tr>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="tabletext">
			<?=$i_general_students_selected?> <span class="tabletextrequire">*</span>
		</td>
		<td width="70%" class="tabletext">
			<table border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td class="tabletext"><?=$select_student?></td>
					<td class="tabletext">&nbsp;</td>
					<td class="tabletext" valign="bottom">
						<?= $linterface->GET_BTN($button_select, "button", "newWindow('choose/index.php?fieldname=studentID[]', 9)") ?><br />
						<?= $linterface->GET_BTN($button_remove, "button", "checkOptionRemove(document.form1.elements['studentID[]']);submitForm('new.php');") ?>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext">&nbsp;</td>
		<td class="tablerow2" width="70%" class="tabletext">
			<span class="tabletextremark">(<?=$i_general_alternative?>)</span>
			<table cellpadding="1" cellspacing="0" border="0">
				<tr><td class="tabletext" nowrap valign="bottom"><?=$i_UserLogin; ?></td></tr>
				<tr><td class="tabletext" nowrap valign="bottom">
					<input type="text" class="textboxnum" name="user_login" maxlength="100">&nbsp;
					<?= $linterface->GET_BTN($button_add, "button", "addStudent(2)") ?><br />
				</td></tr>
				<tr><td class="tabletext" nowrap valign="bottom"><?=$i_ClassNameNumber; ?></td></tr>
				<tr><td class="tabletext" nowrap valign="bottom">
					<?=$select_class?><?=$select_classnum?>
				</td></tr>
			</table>
		</td>
	</tr>

    <?php if(get_client_region() != 'zh_TW') { ?>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
			<?=$i_Attendance_Date?> <span class="tabletextrequire">*</span>
		</td>
		<td width="70%" class="tabletext">
		<span id="dates">
			<?php 
			if (sizeof($target_date) > 0) {
				$dateFieldCount = sizeof($target_date);
				for($i=0;$i<sizeof($target_date);$i++) {
					echo $linterface->GET_DATE_PICKER("target_date[]", $target_date[$i],"","yy-mm-dd","","","","","target_date".$i)."<br />";
				}
			}
			else {
				$dateFieldCount = 1;
				echo $linterface->GET_DATE_PICKER("target_date[]", date('Y-m-d'),"","yy-mm-dd","","","","",'target_date0')."<br />";
			}
			?>
		</span>
			<?= $linterface->GET_BTN(" + ", "button", "addDate(document.getElementById('dateFieldCount').value)") ?><br />
		</td>
	</tr>
    <?php } ?>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
			<?php
            if(get_client_region() == 'zh_TW') {
                echo $i_Attendance_Type;
			} else {
				echo $i_Attendance_DayType;
			}
            ?>
			<span class="tabletextrequire">*</span>
		</td>
		<td width="70%"class="tabletext"><?=$select_datetype?></td>
	</tr>


    <?php if(get_client_region() == 'zh_TW') { ?>
    <tr>
        <td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
            <?=$i_general_startdate?> <span class="tabletextrequire">*</span>
        </td>
        <td width="70%" class="tabletext">
        <span id="dates">
            <?php
            echo $linterface->GET_DATE_PICKER("target_start_date", $target_start_date,"","yy-mm-dd","","","","TargetStartDateChanged(this);",'target_start_date')."<br />";
            ?>
        </span>
        </td>
    </tr>
    <tr>
        <td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
            <?=$i_general_enddate?> <span class="tabletextrequire">*</span>
        </td>
        <td width="70%" class="tabletext">
        <span id="dates">
            <?php
            echo $linterface->GET_DATE_PICKER("target_end_date", $target_end_date,"","yy-mm-dd","","","","",'target_end_date')."<br />";
            ?>
        </span>
        </td>
    </tr>
    <?php } ?>


    <?php if($sys_custom['StudentAttendance']['SubmitAbsentLateRecordsToExternalSystem'] || get_client_region() == 'zh_TW') { ?>
    <tr>
        <td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
			<?=$Lang['StudentAttendance']['SessionFrom']?> <span class="tabletextrequire">*</span>
        </td>
        <td width="70%"class="tabletext">
            <?=$selSessionFrom?>
        </td>
    </tr>
    <tr>
        <td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
			<?=$Lang['StudentAttendance']['SessionTo']?> <span class="tabletextrequire">*</span>
        </td>
        <td width="70%"class="tabletext">
			<?=$selSessionTo?>
        </td>
    </tr>
    <?php } ?>
    <?php if($sys_custom['StudentAttendance']['LeaveType'] || get_client_region() == 'zh_TW') { ?>
        <tr>
            <td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
				<?=$Lang['StudentAttendance']['LeaveType']?> <span class="tabletextrequire">*</span>
            </td>
            <td width="70%"class="tabletext">
                <?=$selLeaveType?>
            </td>
        </tr>
    <?php } ?>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
			<?=$i_Attendance_Reason?>
			<?php if(get_client_region() == 'zh_TW') { ?>
                <span class="tabletextrequire">*</span>
            <?php } ?>
		</td>
		<td width="70%"class="tabletext">
			<?=$linterface->CONVERT_TO_JS_ARRAY($words_absence, "AbsentArrayWords", 1, 1)?>
			<input class="textboxnum" type="text" name="reason" id="reason" maxlength="255" value="<?=htmlspecialchars($reason,ENT_QUOTES)?>">
			<?=$linterface->GET_PRESET_LIST("AbsentArrayWords", $i, "reason")?>
		</td>
	</tr>
    <?php  if(get_client_region() != 'zh_TW') { ?>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
			<?=$Lang['StudentAttendance']['Waived']?>
		</td>
		<td width="70%"class="tabletext">
			<input type="checkbox" name="Waive" id="Waive" value="1">
		</td>
	</tr>
    <?php } ?>
    <?php  if(get_client_region() != 'zh_TW') { ?>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
			<?= $Lang['StudentAttendance']['ProveDocument'] ?>
		</td>
		<td width="70%"class="tabletext">
			<input type="checkbox" name="HandIn_prove" id="HandIn_prove" value="1">
		</td>
	</tr>
    <?php } ?>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
			<?=$i_Attendance_Remark?>
		</td>
		<td width="70%"class="tabletext">
			<?=$linterface->GET_TEXTAREA("remark", $remark)?>
		</td>
	</tr>
	<tr>
		<td class="tabletextremark">
			<br /><?=$i_general_required_field?>
		</td>
	</tr>
</table>
<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
    	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
    </tr>
</table>
<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
	    <td align="center" colspan="2">
			<?= $linterface->GET_ACTION_BTN($button_submit, "button", "checkform()") ?>
			<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:window.location='".urlencode($return_url)."'","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
		</td>
	</tr>
</table>
<input type="hidden" name="submitType" value="">
<input type="hidden" name="dateFieldCount" id="dateFieldCount" value="<?=intranet_htmlspecialchars($dateFieldCount)?>">
<input type="hidden" name="return_url" value="<?=intranet_htmlspecialchars($return_url)?>">
</form>
<br />
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>