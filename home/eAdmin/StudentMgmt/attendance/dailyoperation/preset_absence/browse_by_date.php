<?php
// Editing by YatWoon
##################################### Change Log #####################################################
# 2020-06-03 Ray: Add TW
# 2019-12-02 YatWoon: Set defalt start/End date as Today (Case#U174942)
# 2019-10-15 Ray: Added session From/To
# 2019-08-06 Ray: Added date range
# 2019-01-21 Carlos: Added [Approval Time] and [Approval User].
# 2019-01-02 Carlos: Added [Last Updated Time].
# 2014-12-08 Omas:	Add new column Hand-in Prove Document Status
#
######################################################################################################
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

# preserve table view
if ($ck_attend_daily_absence_browse_date_page_number!=$pageNo && $pageNo!="")
{
	setcookie("ck_attend_daily_absence_browse_date_page_number", $pageNo, 0, "", "", 0);
	$ck_attend_daily_absence_browse_date_page_number = $pageNo;
} else if (!isset($pageNo) && $ck_attend_daily_absence_browse_date_page_number!="")
{
	$pageNo = $ck_attend_daily_absence_browse_date_page_number;
}

if ($ck_attend_daily_absence_browse_date_page_order!=$order && $order!="")
{
	setcookie("ck_attend_daily_absence_browse_date_page_order", $order, 0, "", "", 0);
	$ck_attend_daily_absence_browse_date_page_order = $order;
} else if (!isset($order) && $ck_attend_daily_absence_browse_date_page_order!="")
{
	$order = $ck_attend_daily_absence_browse_date_page_order;
}

if ($ck_attend_daily_absence_browse_date_page_field!=$field && $field!="")
{
	setcookie("ck_attend_daily_absence_browse_date_page_field", $field, 0, "", "", 0);
	$ck_attend_daily_absence_browse_date_page_field = $field;
} else if (!isset($field) && $ck_attend_daily_absence_browse_date_page_field!="")
{
	$field = $ck_attend_daily_absence_browse_date_page_field;
}

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();
$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageDailyOperation_PresetAbsence";

$linterface = new interface_html();

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$filter = $filter==""?2:$filter;

/*
$academic_year_startdate = date("Y-m-d",getStartOfAcademicYear());
$academic_year_enddate = date("Y-m-d",getEndOfAcademicYear());
$StartDate = $StartDate==""?$academic_year_startdate:$StartDate;
$EndDate = $EndDate==""?$academic_year_enddate:$EndDate;
*/
$StartDate = $StartDate==""? date("Y-m-d") :$StartDate;
$EndDate = $EndDate==""? date("Y-m-d") :$EndDate;

$date_cond = " ((DATE_FORMAT(a.RecordDate,'%Y-%m-%d')) BETWEEN '$StartDate' AND '$EndDate')";

$order = ($order == 1) ? 1 : 0;
if ($field == "") $field = 0;

$has_apply_leave = $plugin['eClassApp']? true : false;

$selAttendanceType = '';
if(get_client_region() == 'zh_TW') {
	$temp = array();
	$temp[] = array('',$i_Community_EventAll);
	$temp[] = array('0',$i_DayTypeWholeDay);
	$temp[] = array('1',$i_DayTypeAM);
	$temp[] = array('2',$i_DayTypePM);
	$temp[] = array('3',$i_DayTypeSession);
	$selAttendanceType = getSelectByArray($temp," name=\"attendanceType\"", $attendanceType,0,1);
}

if($StartDate!="" && $EndDate!=""){
	//$namefield = getNameFieldWithClassNumberByLang("b.");
	$namefield = getNameFieldByLang("b.");
	
	$more_fields = "";
	$more_joins = "";
	if($has_apply_leave){
		$more_joins .= " LEFT JOIN INTRANET_USER as u ON u.UserID=a.ApprovedBy ";
		$approved_by_namefield = getNameFieldByLang("u.");
		$more_fields .= " a.ApprovedAt,$approved_by_namefield as ApprovedBy, ";
	}


	$li = new libdbtable2007($field, $order, $pageNo);
	if(get_client_region() == 'zh_TW') {
		$EmptySymbol = $Lang['General']['EmptySymbol'];
		$cond = '';
		if($attendanceType != '') {
            $cond .= " AND a.AttendanceType='".$lc->Get_Safe_Sql_Query($attendanceType)."'";
		}

		$sql = "SELECT RelatedRecordID FROM CARD_STUDENT_PRESET_LEAVE a
                LEFT OUTER JOIN INTRANET_USER AS b ON (a.StudentID = b.UserID) 
                WHERE $date_cond AND 
				($namefield LIKE '%$keyword%' OR a.Reason LIKE '%$keyword%' OR a.Remark LIKE '%$keyword%' )
				AND a.RelatedRecordID IS NOT NULL
				$cond GROUP BY RelatedRecordID";
		$rs = $li->returnArray($sql);
		$relatedRecordIdAry = Get_Array_By_Key($rs, 'RelatedRecordID');
		$conds_recordId = '';
		if(!empty($relatedRecordIdAry)) {
			$conds_recordId = " OR a.RecordID IN (" . implode(',', $relatedRecordIdAry) . ")  ";
		}

		$lc->createCountSchoolDayFunction();
		$sql = " SELECT CONCAT('<a class=\'tablelink\' href=\'edit.php?RecordID=',a.RecordID,'\'>',$namefield,'</a>'),
				b.ClassName,
				b.ClassNumber,
				CASE 
				    WHEN a.AttendanceType = 0 THEN '$i_DayTypeWholeDay'
				    WHEN a.AttendanceType = 1 THEN '$i_DayTypeAM'
				    WHEN a.AttendanceType = 2 THEN '$i_DayTypePM'
				    WHEN a.AttendanceType = 3 THEN '$i_DayTypeSession'
				END as AttendanceType,
			    a.RecordDate,
			    IF(p.RecordDate IS NULL, '$EmptySymbol', p.RecordDate) as EndDate,
		        IF(p.RecordDate IS NULL,'$EmptySymbol', count_schooldays(a.RecordDate, p.RecordDate)) as NumberOfDays,
				IF(a.AttendanceType = 3, a.SessionFrom, '$EmptySymbol') as SessionFrom,
				IF(a.AttendanceType = 3, a.SessionTo, '$EmptySymbol') as SessionTo, 
				IF(a.AttendanceType = 3, a.SessionTo - a.SessionFrom + 1, '$EmptySymbol') as SessionDiff, 
				lt.TypeName as LeaveTypeName, 
				 a.Reason,
				 a.Remark,
				 a.DateModified,
				$more_fields 
				 CONCAT('<input type=checkbox name=RecordID[] value=', a.RecordID ,'>')
				FROM CARD_STUDENT_PRESET_LEAVE AS a 
				LEFT OUTER JOIN INTRANET_USER AS b ON (a.StudentID = b.UserID) 
				$more_joins 
				LEFT JOIN CARD_STUDENT_PRESET_LEAVE p ON p.RecordID = (SELECT RecordID FROM CARD_STUDENT_PRESET_LEAVE WHERE RelatedRecordID=a.RecordID AND a.AttendanceType=0 ORDER BY RecordDate DESC LIMIT 1)
				LEFT JOIN CARD_STUDENT_LEAVE_TYPE lt ON a.LeaveTypeID = lt.TypeID
				WHERE
				($date_cond AND 
				($namefield LIKE '%$keyword%' OR a.Reason LIKE '%$keyword%' OR a.Remark LIKE '%$keyword%' )
				AND a.RelatedRecordID IS NULL
				$cond)
				$conds_recordId				
				";

		$field_array = array("$namefield", "b.ClassName", "b.ClassNumber", "AttendanceType", "a.RecordDate", "EndDate","NumberOfDays","SessionFrom","SessionTo","SessionDiff","LeaveTypeName","a.Reason","a.Remark", "a.DateModified");
		$column_array = array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		$wrap_array = array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

	} else {
	$session_fields = "";
	if($sys_custom['StudentAttendance']['SubmitAbsentLateRecordsToExternalSystem']) {
		$session_fields .= " a.SessionFrom, a.SessionTo, ";
	}

		$leavetype_fields = '';
		$leavetype_join = '';
		if($sys_custom['StudentAttendance']['LeaveType']) {
			$leavetype_fields = " lt.TypeName as LeaveTypeName, ";
			$leavetype_join = "LEFT JOIN CARD_STUDENT_LEAVE_TYPE lt ON a.LeaveTypeID = lt.TypeID";
		}

	$sql=" SELECT CONCAT('<a class=\'tablelink\' href=\'edit.php?RecordID=',a.RecordID,'\'>',$namefield,'</a>'),
				b.ClassName,b.ClassNumber,
			    a.RecordDate,
				 IF(a.DayPeriod=2,'$i_DayTypeAM','$i_DayTypePM'),
				 $session_fields
				 $leavetype_fields
				 a.Reason,
				 IF(a.Waive = '1' AND a.Waive IS NOT NULL,'".$Lang['General']['Yes']."','".$Lang['General']['No']."') as Waive,
				IF(a.DocumentStatus = '1' AND a.DocumentStatus IS NOT NULL,'".$Lang['General']['Yes']."','".$Lang['General']['No']."') as DocumentStatus,
				 a.Remark,
				 a.DateModified,
				$more_fields 
				 CONCAT('<input type=checkbox name=RecordID[] value=', a.RecordID ,'>')
				FROM CARD_STUDENT_PRESET_LEAVE AS a 
				LEFT OUTER JOIN INTRANET_USER AS b ON (a.StudentID = b.UserID) 
				$more_joins 
				$leavetype_join
				WHERE $date_cond AND 
				($namefield LIKE '%$keyword%' OR a.Reason LIKE '%$keyword%' OR a.Remark LIKE '%$keyword%' )
				";

	if($sys_custom['StudentAttendance']['SubmitAbsentLateRecordsToExternalSystem']) {
			$field_array = array("$namefield", "b.ClassName", "b.ClassNumber", "a.RecordDate", "a.DayPeriod", "a.SessionFrom", "a.SessionTo");
			if($sys_custom['StudentAttendance']['LeaveType']) {
				$field_array[] = 'LeaveTypeName';
			}
			$field_array = array_merge($field_array, array("a.Reason", "Waive", "DocumentStatus", "a.Remark", "a.DateModified"));
		$column_array = array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		$wrap_array = array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
	} else {
			$field_array = array("$namefield", "b.ClassName", "b.ClassNumber", "a.RecordDate", "a.DayPeriod");
			if($sys_custom['StudentAttendance']['LeaveType']) {
				$field_array[] = 'LeaveTypeName';
			}
			$field_array = array_merge($field_array, array("a.Reason", "Waive", "DocumentStatus", "a.Remark", "a.DateModified"));
		$column_array = array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		$wrap_array = array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
	}
	}


	if($has_apply_leave){
		$field_array[] = "a.ApprovedAt";
		$field_array[] = "ApprovedBy";
		$column_array[] = 0;
		$column_array[] = 0;
		$wrap_array[] = 0;
		$wrap_array[] = 0;
	}
	$li->field_array = $field_array;
	$li->sql = $sql;
	$li->no_col = sizeof($li->field_array)+2;
	$li->title = "";
	
	$li->column_array = $column_array;
	$li->wrap_array = $wrap_array;
	$li->IsColOff = 2;

	// TABLE COLUMN
	$pos = 0;
	$li->column_list .= "<td width='1' class='tabletoplink'>#</td>\n";
	$li->column_list .= "<td width='10%'>".$li->column($pos++, $i_UserStudentName)."</td>\n";
	$li->column_list .= "<td width='3%'>".$li->column($pos++, $i_ClassName)."</td>\n";
	$li->column_list .= "<td width='3%'>".$li->column($pos++, $i_ClassNumber)."</td>\n";
	if(get_client_region() == 'zh_TW') {
		$li->column_list .= "<td width='4%'>".$li->column($pos++, $i_Attendance_Type)."</td>\n";
		$li->column_list .= "<td width='8%'>".$li->column($pos++, $i_general_startdate)."</td>\n";
		$li->column_list .= "<td width='8%'>".$li->column($pos++, $i_general_enddate)."</td>\n";
		$li->column_list .= "<td width='4%'>".$li->column($pos++, $i_StaffAttendance_Report_Number_Of_Date)."</td>\n";
		$li->column_list .= "<td width='5%'>".$li->column($pos++, $Lang['StudentAttendance']['SessionFrom'])."</td>\n";
		$li->column_list .= "<td width='5%'>".$li->column($pos++, $Lang['StudentAttendance']['SessionTo'])."</td>\n";
		$li->column_list .= "<td width='5%'>".$li->column($pos++, $Lang['StudentAttendance']['TotalSession'])."</td>\n";
		$li->column_list .= "<td width='4%'>".$li->column($pos++, $Lang['StudentAttendance']['LeaveType'])."</td>\n";
	} else {
	$li->column_list .= "<td width='17%'>".$li->column($pos++, $i_StudentAttendance_Field_Date)."</td>\n";
	$li->column_list .= "<td width='5%'>".$li->column($pos++, $i_Attendance_DayType)."</td>\n";
	if($sys_custom['StudentAttendance']['SubmitAbsentLateRecordsToExternalSystem']) {
		$li->column_list .= "<td width='5%'>".$li->column($pos++, $Lang['StudentAttendance']['SessionFrom'])."</td>\n";
		$li->column_list .= "<td width='5%'>".$li->column($pos++, $Lang['StudentAttendance']['SessionTo'])."</td>\n";
	}
		if($sys_custom['StudentAttendance']['LeaveType']) {
			$li->column_list .= "<td width='4%'>".$li->column($pos++, $Lang['StudentAttendance']['LeaveType'])."</td>\n";
		}
	}

	$li->column_list .= "<td width='10%'>".$li->column($pos++, $i_Attendance_Reason)."</td>\n";
	if(get_client_region() != 'zh_TW') {
	$li->column_list .= "<td width='5%'>".$li->column($pos++, $Lang['StudentAttendance']['Waived'])."</td>\n";
	$li->column_list .= "<td width='5%'>".$li->column($pos++, $Lang['StudentAttendance']['ProveDocument'])."</td>\n";
	}
	$li->column_list .= "<td width='10%'>".$li->column($pos++, $Lang['StudentAttendance']['OfficeRemark'])."</td>\n";
	$li->column_list .= "<td width='10%'>".$li->column($pos++, $Lang['General']['LastUpdatedTime'])."</td>\n";
	if($has_apply_leave){
		$li->column_list .= "<td width='10%'>".$li->column($pos++, $Lang['StudentAttendance']['ApprovalTime'])."</td>\n";
		$li->column_list .= "<td width='8%'>".$li->column($pos++, $Lang['StudentAttendance']['ApprovalUser'])."</td>\n";
	}
	$li->column_list .= "<td width='1'>".$li->check("RecordID[]")."</td>\n";

}

$TAGS_OBJ[] = array($button_new, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/dailyoperation/preset_absence/new.php", 0);
$TAGS_OBJ[] = array($i_SmartCard_DailyOperation_Preset_Absence_BrowseByStudent, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/dailyoperation/preset_absence/browse_by_student.php", 0);
$TAGS_OBJ[] = array($i_SmartCard_DailyOperation_Preset_Absence_BrowseByDate, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/dailyoperation/preset_absence/browse_by_date.php", 1);

$MODULE_OBJ = $lc->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START(urldecode($Msg));

if ($msg == 1) $SysMsg = $linterface->GET_SYS_MSG("add");
if ($msg == 2) $SysMsg = $linterface->GET_SYS_MSG("update");
if ($msg == 3) $SysMsg = $linterface->GET_SYS_MSG("delete");
?>
<script language='javascript'>
<!--
function changeClass(){
	submitForm();
}
function submitForm(){
	obj = document.form1;
	if(obj==null) return;
	objTargetDate = document.form1.target_date;
	if(!check_date(objTargetDate,'<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>')){
			objTargetDate.focus();
			return;
	}
	obj.submit();
}
function openPrintPage()
{
        newWindow("browse_by_date_print.php?class=<?=$class?>&StartDate=<?=$StartDate?>&EndDate=<?=$EndDate?>&filter=<?=$filter?>&keyword=<?=$keyword?>&order=<?=$order?>&field=<?=$field?>&attendanceType=<?=$attendanceType?>", 12);
}
function exportPage(newurl){
	old_url = document.form1.action;
	document.form1.action = newurl;
	document.form1.submit();
	document.form1.action = old_url;
}
-->
</script>
<br />
<form name="form1" method="GET">
<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext" width="30%"><?= $i_StudentAttendance_Field_Date ?></td>
		<td valign="top" class="tabletext" width="70%">
			<?=$Lang['General']['From'].'&nbsp;'.$linterface->GET_DATE_PICKER("StartDate", $StartDate).$Lang['General']['To'].'&nbsp;'.$linterface->GET_DATE_PICKER("EndDate", $EndDate)?>
			<?=$linterface->Get_Form_Warning_Msg('Date_Error', $Lang['General']['JS_warning']['InvalidDateRange'], 'Error')?>
		</td>
	</tr>
	<?php if(get_client_region() == 'zh_TW') { ?>
    <tr>
        <td valign="top" nowrap="nowrap" class="formfieldtitle tabletext" width="30%"><?=$i_Attendance_Type?></td>
        <td valign="top" class="tabletext" width="70%">
            <?=$selAttendanceType?>
        </td>
    </tr>
    <?php } ?>
	<tr>
		<td colspan="2" class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
	</tr>
	<tr>
		<td colspan="2" align="center" class="tabletext">
			<?= $linterface->GET_ACTION_BTN($button_view, "submit", "submitForm()","submit2") ?>
		</td>
	</tr>
</table>
<br />
<?php if($StartDate!="" && $EndDate!="") {
	$toolbar = $linterface->GET_LNK_NEW("javascript:checkGet(document.form1,'new.php')","","","","",0);
	$toolbar .= $linterface->GET_LNK_EXPORT("javascript:exportPage('browse_by_date_export.php')","","","","",0);
	$toolbar .= $linterface->GET_LNK_PRINT("javascript:openPrintPage()","","","","",0);
	
	$table_tool .= "<td nowrap=\"nowrap\">
					<a href=\"javascript:checkRemove(document.form1,'RecordID[]','remove.php')\" class=\"tabletool\">
						<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_delete.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">
						$button_remove
					</a>
				</td>";
	$table_tool .= "<td><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"5\"></td>";
	$table_tool .= "<td nowrap=\"nowrap\">
						<a href=\"javascript:checkEdit(document.form1,'RecordID[]','edit.php')\" class=\"tabletool\">
							<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_edit.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">
							$button_edit
						</a>
					</td>";
	
	$searchbar = "<input class=\"textboxnum\" type=\"text\" name=\"keyword\" maxlength=\"50\" value=\"".stripslashes($keyword)."\" onFocus=\"this.form.pageNo.value=1;\">\n";
	$searchbar .= $linterface->GET_SMALL_BTN($button_find, "button", "document.form1.submit();","submit2");
	
?>
<table width="95%" border="0" cellpadding="3" cellspacing="0" align="center">
	<tr>
		<td align="left"><?=$toolbar?></td>
		<td align="right"><?=$SysMsg?></td>
	</tr>
</table>
<table width="95%" border="0" cellpadding="0" cellspacing="0" align="center">
	<tr class="table-action-bar">
		<td align="left"><?=$searchbar?></td>
		<td valign="bottom" align="right">
			<table border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="21"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_01.gif" width="21" height="23"></td>
					<td background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_02.gif">
						<table border="0" cellspacing="0" cellpadding="2">
							<tr>
								<?=$table_tool?>
							</tr>
						</table>
					</td>
					<td width="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_03.gif" width="6" height="23"></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<?php echo $li->display("95%"); ?>
<?php } ?>
<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>">
<input type="hidden" name="order" value="<?php echo $li->order; ?>">
<input type="hidden" name="field" value="<?php echo $li->field; ?>">
<input type="hidden" name="page_size_change" value="">
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>">
</form>
<br />
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>