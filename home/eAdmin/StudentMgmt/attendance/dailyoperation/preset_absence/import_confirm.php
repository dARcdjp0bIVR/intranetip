<?php
// editing by 
##################################### Change Log #####################################################
# 2020-06-16 Ray:	Add TW
# 2014-12-08 Omas:	Add new column Hand-in Prove Document Status
#
######################################################################################################
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libstudentattendance_ui.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$linterface = new interface_html();
$StudAttendUI = new libstudentattendance_ui();

$limport = new libimporttext();
$lf = new libfilesystem();

if(get_client_region() == 'zh_TW') {
	$format_array = array("Class Name","Class No.","Attendance Type","Start Date","End Date","Session From","Session To","Leave Type Code","Reason","Remark");
} else {
$format_array = array("Class Name","Class No.","Date","Time Slot","Reason","Waive","Submitted Prove Document","Remark");
}
$filepath = $userfile;
$filename = $userfile_name;

if($filepath=="none" || $filepath == "")
{
	# import failed
    header("Location: import.php?Msg=".urlencode($Lang['StaffAttendance']['DataImportFail']));
    intranet_closedb();
    exit();
}else
{
	$ext = strtoupper($lf->file_ext($filename));
	if($limport->CHECK_FILE_EXT($filename))
	{
	  # read file into array
	  # return 0 if fail, return csv array if success
	  //$data = $lf->file_read_csv($filepath);
	  $data = $limport->GET_IMPORT_TXT($filepath);
	  if(sizeof($data)>0)
	  {
	  	$toprow = array_shift($data);                   # drop the title bar
	  }else
	  {
	  	header("Location: import.php?Msg=".urlencode($Lang['StaffAttendance']['DataImportFail']));
	    intranet_closedb();
	    exit();
	  }
	}
	for ($i=0; $i<sizeof($format_array); $i++)
	{
	 if ($toprow[$i] != $format_array[$i])
	 {
	     header("Location: import.php?Msg=".urlencode($Lang['StaffAttendance']['DataImportFail']));
	     intranet_closedb();
	     exit();
	 }
	}
	
	$CurrentPageArr['StudentAttendance'] = 1;
	$CurrentPage = "PageDailyOperation_PresetAbsence";
	$TAGS_OBJ[] = array($button_new, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/dailyoperation/preset_absence/new.php", 1);
	$TAGS_OBJ[] = array($i_SmartCard_DailyOperation_Preset_Absence_BrowseByStudent, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/dailyoperation/preset_absence/browse_by_student.php", 0);
	$TAGS_OBJ[] = array($i_SmartCard_DailyOperation_Preset_Absence_BrowseByDate, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/dailyoperation/preset_absence/browse_by_date.php", 0);
	
	$MODULE_OBJ = $StudAttendUI->GET_MODULE_OBJ_ARR();
	$linterface->LAYOUT_START(urldecode($Msg));
  echo $StudAttendUI->Get_Preset_Absence_Import_Confirm_Page($data);
}

$linterface->LAYOUT_STOP();
intranet_closedb();
?>