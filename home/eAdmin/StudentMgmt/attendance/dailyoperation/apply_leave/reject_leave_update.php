<?php
## using: 

##################################### Change Log #####################################################
#
######################################################################################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libapplyleave.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");

intranet_auth();
intranet_opendb();

$lapplyleave = new libapplyleave();

// [2017-0908-1248-12235] Handling for Apply Leave (App) (Class Teacher)
if($sys_custom["StudentAttendance"]["Class_Teacher_Approve_Leave_Request"] && $_POST["cteach"]==1)
{
	include_once($PATH_WRT_ROOT."includes/libteaching.php");
	$lteaching = new libteaching();
	$TeachingClasses = $lteaching->returnTeacherClass($UserID);
	$isClassTeacher = count((array)$TeachingClasses) > 0;
}

// [2017-0908-1248-12235]
if (!($_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || $isClassTeacher) || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$values="";
$delim="";
if($sys_custom['eClassApp']['HKUSPH']){
	$array_record_id = array();
	
	include_once($PATH_WRT_ROOT."includes/HKUFlu/HKUFlu.php");
	include_once($PATH_WRT_ROOT."includes/HKUFlu/libHKUFlu_db.php");
	
	$db_hkuFlu = new libHKUFlu_db();
}

for($i=0;$i<$item_count;$i++){
	$studentID = $_POST[studentID_.$i];
	$recordDate = $_POST[recordDate_.$i];
	$reason = $_POST[reason_.$i];
	$recordId = $_POST[recordID_.$i];
	$rejectReason = $_POST[reject_.$i];
	$successAry['updateReason'][$i] = $lapplyleave->updateApplyLeaveRejectReason($recordId, $rejectReason);
}

intranet_closedb();

if (in_array(false, $successAry)) {
	$Msg = 2;
}else{
	$Msg = 1;
}

$return_url = "list.php?Msg=".$Msg;
if($isClassTeacher)
	$return_url = "../../class_teacher/apply_leave_list.php?Msg=".$Msg;
header("Location: $return_url");
?>