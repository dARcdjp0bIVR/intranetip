<?php
## using: 

##################################### Change Log #####################################################
# 2020-10-21 Ray: allow tick am & pm
# 2020-06-05 Ray: Add TW
# 2019-11-05 Tiffany: Show alert if both AM and PM not checked
# 2018-11-05 Cameron: Apply intranet_htmlspecialchars to reason field
# 2017-09-18 Bill:	Support Class Teacher to manage apply leave list 	[2017-0908-1248-12235]
# 2014-12-08 Omas:	Add new column Hand-in Prove Document Status
#
######################################################################################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libapplyleave.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libwordtemplates.php");
include_once($PATH_WRT_ROOT."includes/libcycleperiods.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$lcardstudentattend2 = new libcardstudentattend2();
$lapplyleave = new libapplyleave();
$lword = new libwordtemplates();
$libdb = new libdb();

// [2017-0908-1248-12235] Handling for Apply Leave (App) (Class Teacher)
if($sys_custom["StudentAttendance"]["Class_Teacher_Approve_Leave_Request"] && $_GET["cteach"]==1)
{
	include_once($PATH_WRT_ROOT."includes/libteaching.php");
	$lteaching = new libteaching();
	$TeachingClasses = $lteaching->returnTeacherClass($UserID);
	$isClassTeacher = count((array)$TeachingClasses) > 0;
}

### check access right
$canAccess = false;
if ($_SESSION["SSV_PRIVILEGE"]["plugin"]['attendancestudent'] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] && $plugin['eClassApp']) {
	$canAccess = true;
}
// [2017-0908-1248-12235]
if ($plugin['eClassApp'] && $_SESSION["SSV_PRIVILEGE"]["plugin"]['attendancestudent'] && $isClassTeacher) {
	$canAccess = true;
	$classTeacherAccess = true;
}
if (!$canAccess) {
	No_Access_Right_Pop_Up();
}

$recordIdAry = IntegerSafe($_POST['recordIdAry']);

### tab display
// $TAGS_OBJ[] = array($displayLang, $onclickJs, $isSelectedTab);
$TAGS_OBJ[] = array($Lang['StudentAttendance']['ApplyLeaveAry']['ApplyLeave(App)']);
$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageDailyOperation_ApplyLeaveApp";
$MODULE_OBJ = $lcardstudentattend2->GET_MODULE_OBJ_ARR();

// [2017-0908-1248-12235] Tab Display for Apply Leave (App) (Class Teacher)
if($classTeacherAccess)
{
	unset($TAGS_OBJ);
	unset($MODULE_OBJ);
	$MODULE_OBJ = array();
	$MODULE_OBJ['title'] = $ip20TopMenu['eAttendance'];
	$TAGS_OBJ[] = array($Lang['StudentAttendance']['ApplyLeaveAry']['ApplyLeave(App)'].' ('.$i_Teaching_ClassTeacher.')');
}

$linterface->LAYOUT_START(urldecode($Msg));


### navigation
// $navigationAry[] = array($displayLang, $onclickJs);
$navigationAry[] = array($Lang['StudentAttendance']['ApplyLeaveAry']['ApplyLeaveList'], 'javascript: goBack();');
$navigationAry[] = array($Lang['StudentAttendance']['ApplyLeaveAry']['Acknowledge']);
$htmlAry['navigation'] = $linterface->GET_NAVIGATION_IP25($navigationAry);


### get apply leave records
$leaveRecordAry = $lapplyleave->getApplyLeaveDataOrderByStudent($recordIdAry);
$numOfLeaveRecord = count($leaveRecordAry);

### get student info
$studentIdAry = Get_Array_By_Key($leaveRecordAry, 'StudentID');
$userObj = new libuser('', '', $studentIdAry);

### get absent reason 
$words_absence = $lword->getWordListAttendance(1);
foreach($words_absence as $key=>$word)
	$words_absence[$key]= htmlspecialchars($word);


$selSessionFrom = "";
$selSessionTo = "";

if($sys_custom['StudentAttendance']['SubmitAbsentLateRecordsToExternalSystem']) {
	$selSessionFrom .= "<option value=\"\" >-</option>";
	$selSessionTo .= "<option value=\"\" >-</option>";
}

$k=0.0;
while($k<=CARD_STUDENT_MAX_SESSION) {
	$selSessionFrom .= "<option value=\"".$k."\" >".$k."</option>";
	$selSessionTo .= "<option value=\"".$k."\" >".$k."</option>";
	$k+= $sys_custom['SmartCardAttendance_StudentAbsentSessionStep'] > 0 ? $sys_custom['SmartCardAttendance_StudentAbsentSessionStep'] : 0.5;
}
$selSessionFrom .= "</select>\n";
$selSessionTo .= "</select>\n";

if($sys_custom['StudentAttendance']['LeaveType'] || get_client_region() == 'zh_TW') {
	$sql = "SELECT TypeID,TypeName FROM CARD_STUDENT_LEAVE_TYPE ORDER BY TypeSymbol,TypeName";
	$leave_type_rs = $libdb->returnArray($sql, 2);
}

### build table
$x = '';
$x .= '<table class="common_table_list_v30 edit_table_list_v30" id="ContentTable">'."\r\n";
	$x .= '<thead>'."\r\n";
		$x .= '<tr>'."\r\n";
			$x .= '<th>#</th>'."\r\n";
			$x .= '<th>'.$Lang['General']['Class'].'</th>'."\r\n";
			$x .= '<th>'.$Lang['SysMgr']['FormClassMapping']['ClassNo'].'</th>'."\r\n";
			$x .= '<th>'.$Lang['Identity']['Student'].'</th>'."\r\n";
            if(get_client_region() == 'zh_TW') {
				$x .= '<th>'.$Lang['General']['StartDate'].'</th>'."\r\n";
				$x .= '<th>'.$Lang['General']['EndDate'].'</th>'."\r\n";
                $x .= '<th>'.$i_Attendance_Type.'</th>'."\r\n";
                $x .= '<th>'.$Lang['StudentAttendance']['SessionFrom'].'</th>'."\r\n";
                $x .= '<th>'.$Lang['StudentAttendance']['SessionTo'].'</th>'."\r\n";
                $x .= '<th>'.$Lang['StudentAttendance']['TotalSession'].'</th>'."\r\n";
                $x .= '<th>'.$Lang['StudentAttendance']['LeaveType'].'</th>'."\r\n";
            } else {
				$x .= '<th>' . $Lang['General']['Date'] . '</th>' . "\r\n";
				if($sys_custom['StudentAttendance']['LeaveType']) {
					$x .= '<th>'.$Lang['StudentAttendance']['LeaveType'].'</th>'."\r\n";
				}
			}
			$x .= '<th>'.$Lang['StudentAttendance']['ApplicationReason'].'</th>'."\r\n";	
			$x .= '<th>'.$Lang['StudentAttendance']['ApplyLeave']['UpdateAcknowledgmentStatus'].'</th>'."\r\n";				
			$x .= '<th>'.$Lang['StudentAttendance']['ApplyLeave']['SendPushNotification'].'</th>'."\r\n";
			$x .= '<th>'.$Lang['StudentAttendance']['TransferToAbsentRecord'].'</th>'."\r\n";
            if(get_client_region() != 'zh_TW') {
                $x .= '<th>' . $Lang['StudentAttendance']['TimeSlot'] . '</th>' . "\r\n";
            }
            if($sys_custom['StudentAttendance']['SubmitAbsentLateRecordsToExternalSystem']) {
                $x .= '<th>' . $Lang['StudentAttendance']['SessionFrom'] . '</th>' . "\r\n";
                $x .= '<th>' . $Lang['StudentAttendance']['SessionTo'] . '</th>' . "\r\n";
            }
            if($sys_custom['StudentAttendance']['LeaveType'] || get_client_region() == 'zh_TW') {
                $x .= '<th>'.$Lang['StudentAttendance']['LeaveType'].'</th>'."\r\n";
            }
            $x .= '<th>'.$Lang['General']['Reason'].'</th>'."\r\n";
            if(get_client_region() != 'zh_TW') {
                $x .= '<th>' . $Lang['StaffAttendance']['Waived'] . '</th>' . "\r\n";
                $x .= '<th>' . $Lang['StudentAttendance']['ProveDocument'] . '</th>' . "\r\n";
            }
			$x .= '<th>'.$Lang['StudentAttendance']['OfficeRemark'].'</th>'."\r\n";
			
			
		$x .= '</tr>'."\r\n";
	$x .= '</thead>'."\r\n";
	$x .= '<tbody>'."\r\n";
		$x .= '<tr class="edit_table_head_bulk">'."\r\n";
			$x .= '<th></th>'."\r\n";
			$x .= '<th></th>'."\r\n";
			$x .= '<th></th>'."\r\n";
			$x .= '<th></th>'."\r\n";
            if(get_client_region() == 'zh_TW') {
				$x .= '<th></th>'."\r\n";
                $x .= '<th></th>'."\r\n";
                $x .= '<th></th>'."\r\n";
                $x .= '<th></th>'."\r\n";
                $x .= '<th></th>'."\r\n";
				$x .= '<th></th>'."\r\n";
            } else {
				if ($sys_custom['StudentAttendance']['LeaveType']) {
					$x .= '<th></th>' . "\r\n";
				}
			}
			$x .= '<th></th>'."\r\n";
			$x .= '<th></th>'."\r\n";			
			$x .= '<th style="text-align:center"><input type="checkbox" id="All_updateAS" onclick="$(\'.updateAS\').attr(\'checked\',this.checked);"></th>'."\r\n";				
			$x .= '<th style="text-align:center"><input type="checkbox" id="All_send" onclick="$(\'.send\').attr(\'checked\',this.checked);"></th>'."\r\n";				
			$x .= '<th style="text-align:center"><input type="checkbox" id="AlltoAbsent" onclick="alltoAbsent();"></th>'."\r\n";
            if(get_client_region() != 'zh_TW') {
				$x .= '<th style="text-align:center"><input type="checkbox" id="All_am" onclick="$(\'.am\').attr(\'checked\',this.checked);">' . $Lang['StudentAttendance']['AM'] . '&nbsp;<input type="checkbox" id="All_pm" onclick="$(\'.pm\').attr(\'checked\',this.checked);">' . $Lang['StudentAttendance']['PM'] . '</th>' . "\r\n";
			}
			if($sys_custom['StudentAttendance']['SubmitAbsentLateRecordsToExternalSystem']) {
                $x .= '<th></th>' . "\r\n";
                $x .= '<th></th>' . "\r\n";
            }
            if($sys_custom['StudentAttendance']['LeaveType'] || get_client_region() == 'zh_TW') {
                $x .= '<th></th>' . "\r\n";
            }
			$x .= '<th></th>'."\r\n";
            if(get_client_region() != 'zh_TW') {
                $x .= '<th style="text-align:center"><input type="checkbox" id="All_waived" onclick="$(\'.waived\').attr(\'checked\',this.checked);"></th>' . "\r\n";
                $x .= '<th style="text-align:center"><input type="checkbox" id="All_HandIn_prove_" onclick="$(\'.HandIn_prove\').attr(\'checked\',this.checked);"></th>' . "\r\n";
            }
			$x .= '<th></th>'."\r\n";
				
		$x .= '</tr>'."\r\n";
		$item_count = 0;
		$head_count=0;

		$hasPendding = false;

		for ($i=0; $i<$numOfLeaveRecord; $i++) {
			
			if($head_count==10){
				$x .= '</tbody>'."\r\n";
				$x .= '<thead>'."\r\n";
				$x .= '<tr>'."\r\n";
				$x .= '<th>#</th>'."\r\n";
				$x .= '<th>'.$Lang['General']['Class'].'</th>'."\r\n";
				$x .= '<th>'.$Lang['SysMgr']['FormClassMapping']['ClassNo'].'</th>'."\r\n";
				$x .= '<th>'.$Lang['Identity']['Student'].'</th>'."\r\n";
				if(get_client_region() == 'zh_TW') {
					$x .= '<th>'.$Lang['General']['StartDate'].'</th>'."\r\n";
					$x .= '<th>'.$Lang['General']['EndDate'].'</th>'."\r\n";
					$x .= '<th>'.$i_Attendance_Type.'</th>'."\r\n";
					$x .= '<th>'.$Lang['StudentAttendance']['SessionFrom'].'</th>'."\r\n";
					$x .= '<th>'.$Lang['StudentAttendance']['SessionTo'].'</th>'."\r\n";
					$x .= '<th>'.$Lang['StudentAttendance']['TotalSession'].'</th>'."\r\n";
					$x .= '<th>'.$Lang['StudentAttendance']['LeaveType'].'</th>'."\r\n";
				} else {
				    $x .= '<th>' . $Lang['General']['Date'] . '</th>' . "\r\n";
					if($sys_custom['StudentAttendance']['LeaveType']) {
						$x .= '<th>'.$Lang['StudentAttendance']['LeaveType'].'</th>'."\r\n";
					}
				}
				$x .= '<th>'.$Lang['StudentAttendance']['ApplicationReason'].'</th>'."\r\n";
				$x .= '<th>'.$Lang['StudentAttendance']['ApplyLeave']['UpdateAcknowledgmentStatus'].'</th>'."\r\n";
				$x .= '<th>'.$Lang['StudentAttendance']['ApplyLeave']['SendPushNotification'].'</th>'."\r\n";				
				$x .= '<th>'.$Lang['StudentAttendance']['TransferToAbsentRecord'].'</th>'."\r\n";
				if(get_client_region() != 'zh_TW') {
					$x .= '<th>' . $Lang['StudentAttendance']['TimeSlot'] . '</th>' . "\r\n";
				}
				if($sys_custom['StudentAttendance']['SubmitAbsentLateRecordsToExternalSystem']) {
					$x .= '<th>' . $Lang['StudentAttendance']['SessionFrom'] . '</th>' . "\r\n";
					$x .= '<th>' . $Lang['StudentAttendance']['SessionTo'] . '</th>' . "\r\n";
				}
				if($sys_custom['StudentAttendance']['LeaveType'] || get_client_region() == 'zh_TW') {
					$x .= '<th>'.$Lang['StudentAttendance']['LeaveType'].'</th>'."\r\n";
				}
				$x .= '<th>'.$Lang['General']['Reason'].'</th>'."\r\n";
				if(get_client_region() != 'zh_TW') {
					$x .= '<th>' . $Lang['StaffAttendance']['Waived'] . '</th>' . "\r\n";
					$x .= '<th>' . $Lang['StudentAttendance']['ProveDocument'] . '</th>' . "\r\n";
				}
				$x .= '<th>'.$Lang['StudentAttendance']['OfficeRemark'].'</th>'."\r\n";
				$x .= '</tr>'."\r\n";
				$x .= '</thead>'."\r\n";
				$x .= '<tbody>'."\r\n";				
				$head_count=0;
			}
			
			$_recordId = $leaveRecordAry[$i]['RecordID'];
			$_studentId = $leaveRecordAry[$i]['StudentID'];
			$_startDate = $leaveRecordAry[$i]['StartDate'];
			$_startDateType = $leaveRecordAry[$i]['StartDateType'];
			$_endDate = $leaveRecordAry[$i]['EndDate'];
			$_endDateType = $leaveRecordAry[$i]['EndDateType'];
			$_duration = $leaveRecordAry[$i]['Duration'];
			$_reason = $leaveRecordAry[$i]['Reason'];
			$_approvalStatus = $leaveRecordAry[$i]['ApprovalStatus'];
			$_documentStatus = $leaveRecordAry[$i]['DocumentStatus'];
			$_leaveTypeId = $leaveRecordAry[$i]['LeaveTypeID'];

			if ( $_documentStatus == 2){
				$_documentStatusChk = 'checked';
			}
			else{
				$_documentStatusChk = '';
			}
			
			if ($_approvalStatus != 0) {
				// if not pendding => skip
				continue;
			}

			$hasPendding = true;
			$_numOfRow = ceil($_duration);
			$userObj->LoadUserData($_studentId);
			
			if($_startDateType=="PM"&&$_endDateType=="AM")$_numOfRow++;

			if(get_client_region() == 'zh_TW') {
				$_numOfRow = 1;
			}

			for ($j=0; $j<$_numOfRow; $j++) {
				$_recordDate = date('Y-m-d', strtotime('+'.$j.' day', strtotime($_startDate)));
				
				$isNonSchoolDay = $lcardstudentattend2->Get_Class_Attend_Time_Setting($_recordDate,$_studentId)=="NonSchoolDay"?true:false;
                $NonSchoolDayDisable = $isNonSchoolDay==1?'disabled="disabled"':'';
				
				// determine leave date type
				$_leaveDateType = '';
				if($_startDateType=="AM"&&$_endDateType=="PM"){				
					$_leaveDateType = 'WD';					
				}
				else{					
					if ($j==0) {
							
						$_leaveDateType = $_startDateType;
					}
					else if ($j == ($_numOfRow-1)) {
						$_leaveDateType = $_endDateType;
					}
					else {
						$_leaveDateType = 'WD';
					}
				}
				
				$isAM = "";
				$isPM = "";
				if($_leaveDateType=="AM"){
					$isAM = "checked";						
				}elseif($_leaveDateType=="PM"){
					$isPM = "checked";						
				}else{					
					$isAM = "checked";
					$isPM = "checked";						
				}

				$target_date_period = array();
				if(get_client_region() == 'zh_TW') {
					$ret = $lcardstudentattend2->checkPresetLeaveOverlap($_studentId, $leaveRecordAry[$i]['AttendanceType'], $_recordDate, $_endDate, $leaveRecordAry[$i]['SessionFrom'], $leaveRecordAry[$i]['SessionTo']);
					if($ret[0] == false || $isNonSchoolDay == 1) {
						$exist_record = false;
					} else {
						$exist_record = true;
					}

				} else {
					$sql = "select RecordID from CARD_STUDENT_PRESET_LEAVE where StudentID='" . $libdb->Get_Safe_Sql_Query($_studentId) . "' and RecordDate='" . $libdb->Get_Safe_Sql_Query($_recordDate) . "'";
					$result = $libdb->returnArray($sql);
					if (count($result) == 0 || $isNonSchoolDay == 1) $exist_record = false;
					else $exist_record = true;
				}
                //debug_pr($exist_record);
				$x .= '<tr class="'.$_trClass.'">'."\r\n";
				
				    $x.='<input type="hidden" name="studentID_'.$item_count.'" value="'.$_studentId.'">';
				    $x.='<input type="hidden" name="recordDate_'.$item_count.'" value="'.$_recordDate.'">';
				    $x.='<input type="hidden" name="recordID_'.$item_count.'" value="'.$_recordId.'">';

                    if(get_client_region() == 'zh_TW') {
						$x.='<input type="hidden" name="AttendanceType_'.$item_count.'" value="'.$leaveRecordAry[$i]['AttendanceType'].'">';
						$x.='<input type="hidden" name="SessionFrom_'.$item_count.'" value="'.$leaveRecordAry[$i]['SessionFrom'].'">';
						$x.='<input type="hidden" name="SessionTo_'.$item_count.'" value="'.$leaveRecordAry[$i]['SessionTo'].'">';
						$x.='<input type="hidden" name="EndDate_'.$item_count.'" value="'.$_endDate.'">';
                    }

					if ($j==0) {
						$x .= '<td rowspan="'.$_numOfRow.'">'.($i+1).'</td>'."\r\n";
						$x .= '<td rowspan="'.$_numOfRow.'">'.$userObj->ClassName.'</td>'."\r\n";
						$x .= '<td rowspan="'.$_numOfRow.'">'.$userObj->ClassNumber.'</td>'."\r\n";
						$x .= '<td rowspan="'.$_numOfRow.'">'.Get_Lang_Selection($userObj->ChineseName, $userObj->EnglishName).'</td>'."\r\n";
					}


                    if(get_client_region() == 'zh_TW') {
						$endDate_text = $Lang['General']['EmptySymbol'];
						if($leaveRecordAry[$i]['AttendanceType'] == 0) {
							$endDate_text = $_endDate;
						}

						$x .= '<td>'.$_startDate.'</td>'."\r\n";
						$x .= '<td>'.$endDate_text.'</td>'."\r\n";

						$x .= '<td>'.$leaveRecordAry[$i]['AttendanceTypeText'].'</td>'."\r\n";
						$x .= '<td>'.$leaveRecordAry[$i]['SessionFromText'].'</td>'."\r\n";
						$x .= '<td>'.$leaveRecordAry[$i]['SessionToText'].'</td>'."\r\n";
						$x .= '<td>'.$leaveRecordAry[$i]['SessionDiff'].'</td>'."\r\n";
						$x .= '<td>'.$leaveRecordAry[$i]['LeaveTypeName'].'</td>'."\r\n";
                    } else {
						$x .= '<td>'.$_recordDate.'('.$_leaveDateType.')</td>'."\r\n";
						if($sys_custom['StudentAttendance']['LeaveType']) {
							$x .= '<td>'.$leaveRecordAry[$i]['LeaveTypeName'].'</td>'."\r\n";
						}
					}
					$x .= '<td>'.$_reason.'</td>'."\r\n";
					if ($j==0) {
					    $x .= '<td style="text-align:center" rowspan="'.$_numOfRow.'"><input type="checkbox" class="updateAS" id="updateAS_'.$item_count.'" name="updateAS_'.$item_count.'[]" checked="checked" value="checked"></td>'."\r\n";
				     	$x .= '<td style="text-align:center" rowspan="'.$_numOfRow.'"><input type="checkbox" class="send" id="send_'.$item_count.'" name="send_'.$item_count.'[]" checked="checked" value="checked"></td>'."\r\n";
					}

				    $_recordDate_extra = '';
				    $_recordDate_extra_other = '';
					if($exist_record==false){
					    if(get_client_region() == 'zh_TW') {
							$exist_date = false;
							if($leaveRecordAry[$i]['AttendanceType'] == 0) {
								$date_diff = strtotime($_endDate) - strtotime($_startDate);
								$date_diff = round($date_diff/(60*60*24));
								for($j=0;$j<=$date_diff;$j++) {
									$current_date = date("Y-m-d", strtotime($_startDate) + ($j*60*60*24));
									if($existAry[$_studentId][$current_date]['am'] || $existAry[$_studentId][$current_date]['pm']) {
										$exist_date = true;
									}
									if($j > 0) {
										$_recordDate_extra .= ' '.$_studentId.'_'. $current_date;
										$_recordDate_extra_other .= ' other_'.$_studentId.'_'. $current_date;
									}
								}

							} else if($leaveRecordAry[$i]['AttendanceType'] == 1) {
								$exist_date = $existAry[$_studentId][$_recordDate]['am'];
							} else if($leaveRecordAry[$i]['AttendanceType'] == 2) {
								$exist_date = $existAry[$_studentId][$_recordDate]['pm'];
							} else if($leaveRecordAry[$i]['AttendanceType'] == 3) {
								$exist_date = ($existAry[$_studentId][$_recordDate]['am'] || $existAry[$_studentId][$_recordDate]['pm']);
							}
							if($isNonSchoolDay == 1) {
								$exist_date = true;
							}
						} else {
							$exist_date = ($existAry[$_studentId][$_recordDate] == true || $isNonSchoolDay == 1);
						}

					    $data_value = $_studentId.'_'.$_recordDate;
						if($exist_date == true){
							$x .= '<td style="text-align:center"><input type="checkbox" class="toAbsent_uncheck '.$_studentId.'_'.$_recordDate.' '.$_recordDate_extra.'" id="'.$item_count.'" name="toAbsent_'.$item_count.'[]" value="'.$_studentId.'_'.$_recordDate.' '.$_recordDate_extra.'" '.$NonSchoolDayDisable.'></td>'."\r\n";
							if(get_client_region() != 'zh_TW') {
								$x .= '<td style="text-align:center"><input type="checkbox" class="am other_' . $_studentId . '_' . $_recordDate . ' default_uncheck '.$_recordDate_extra_other.'" id="am_' . $item_count . '" name="am_' . $item_count . '[]" value="checked" ' . $isAM . ' disabled="disabled">' . $Lang['StudentAttendance']['AM'] . '&nbsp;<input type="checkbox" class="pm other_' . $_studentId . '_' . $_recordDate . ' default_uncheck" id="pm_' . $item_count . '" name="pm_' . $item_count . '[]" value="checked" ' . $isPM . ' disabled="disabled">' . $Lang['StudentAttendance']['PM'] . '</td>' . "\r\n";
							}
							if($sys_custom['StudentAttendance']['SubmitAbsentLateRecordsToExternalSystem']) {
								$x .= '<td style="text-align:center"><select name="SessionFrom_'.$item_count.'" id="SessionFrom_'.$item_count.'">' . $selSessionFrom . '</td>';
								$x .= '<td style="text-align:center"><select name="SessionTo_'.$item_count.'" id="SessionTo_'.$item_count.'">' . $selSessionTo . '</td>';
							}
							if($sys_custom['StudentAttendance']['LeaveType'] || get_client_region() == 'zh_TW') {
								$selLeaveType = getSelectByArray($leave_type_rs," name=\"leaveType_".$item_count."\" id=\"leaveType_".$item_count."\" class=\"other_".$_studentId."_".$_recordDate." ".$_recordDate_extra_other." default_uncheck\" disabled=\"disabled\"", $_leaveTypeId,0,1);
								$x .= '<td>'.$selLeaveType.'</td>';
							}
							$x .= '<td>'.$linterface->CONVERT_TO_JS_ARRAY($words_absence, "jArrayWordsAbsence", 1).'<input type="text" class="other_'.$_studentId.'_'.$_recordDate.' '.$_recordDate_extra_other.' default_uncheck" id="reason_'.$item_count.'" name="reason_'.$item_count.'" value="'.intranet_htmlspecialchars($_reason).'" disabled="disabled">'.$linterface->GET_PRESET_LIST("jArrayWordsAbsence", '_'.$item_count, "reason_$item_count").'</td>'."\r\n";
							if(get_client_region() != 'zh_TW') {
								$x .= '<td style="text-align:center"><input type="checkbox" class="waived other_' . $_studentId . '_' . $_recordDate . ' default_uncheck" id="waived_' . $item_count . '" name="waived_' . $item_count . '[]" value="checked" disabled="disabled"></td>' . "\r\n";
								$x .= '<td style="text-align:center"><input type="checkbox" class="HandIn_prove_' . $_studentId . '_' . $_recordDate . '" id="HandIn_prove_' . $item_count . '" name="HandIn_prove_' . $item_count . '[]" value="1" ' . $_documentStatusChk . '></td>' . "\r\n";
							}
							$x .= '<td><textarea class="other_'.$_studentId.'_'.$_recordDate.' '.$_recordDate_extra_other.' default_uncheck" id="remarks_'.$item_count.'" name="remarks_'.$item_count.'" rows="1" cols="30" disabled="disabled"></textarea></td>'."\r\n";
						}else{							
							$x .= '<td style="text-align:center"><input type="checkbox" class="toAbsent '.$_studentId.'_'.$_recordDate.' '.$_recordDate_extra.'" checked="checked" id="'.$item_count.'" name="toAbsent_'.$item_count.'[]" value="'.$_studentId.'_'.$_recordDate.' '.$_recordDate_extra.'"></td>'."\r\n";
							if(get_client_region() != 'zh_TW') {
								$x .= '<td style="text-align:center"><input type="checkbox" class="am other_' . $_studentId . '_' . $_recordDate . ' default_check" id="am_' . $item_count . '" name="am_' . $item_count . '[]" value="checked" ' . $isAM . '>' . $Lang['StudentAttendance']['AM'] . '&nbsp;<input type="checkbox" class="pm other_' . $_studentId . '_' . $_recordDate . ' default_check" id="pm_' . $item_count . '" name="pm_' . $item_count . '[]" value="checked" ' . $isPM . '>' . $Lang['StudentAttendance']['PM'] . '</td>' . "\r\n";
							}
							if($sys_custom['StudentAttendance']['SubmitAbsentLateRecordsToExternalSystem']) {
								$x .= '<td style="text-align:center"><select name="SessionFrom_'.$item_count.'" id="SessionFrom_'.$item_count.'">' . $selSessionFrom . '</td>';
								$x .= '<td style="text-align:center"><select name="SessionTo_'.$item_count.'" id="SessionTo_'.$item_count.'">' . $selSessionTo . '</td>';
							}
							if($sys_custom['StudentAttendance']['LeaveType'] || get_client_region() == 'zh_TW') {
								$selLeaveType = getSelectByArray($leave_type_rs," name=\"leaveType_".$item_count."\" id=\"leaveType_".$item_count."\" class=\"other_".$_studentId."_".$_recordDate." ".$_recordDate_extra_other." default_check\"", $_leaveTypeId,0,1);
								$x .= '<td>'.$selLeaveType.'</td>';
							}
							$x .= '<td>'.$linterface->CONVERT_TO_JS_ARRAY($words_absence, "jArrayWordsAbsence", 1).'<input type="text" class="other_'.$_studentId.'_'.$_recordDate.' '.$_recordDate_extra_other.' default_check" id="reason_'.$item_count.'" name="reason_'.$item_count.'" value="'.intranet_htmlspecialchars($_reason).'">'.$linterface->GET_PRESET_LIST("jArrayWordsAbsence", '_'.$item_count, "reason_$item_count").'</td>'."\r\n";
							if(get_client_region() != 'zh_TW') {
								$x .= '<td style="text-align:center"><input type="checkbox" class="waived other_' . $_studentId . '_' . $_recordDate . ' default_check" id="waived_' . $item_count . '" name="waived_' . $item_count . '[]" value="checked"></td>' . "\r\n";
								$x .= '<td style="text-align:center"><input type="checkbox" class="HandIn_prove other_' . $_studentId . '_' . $_recordDate . '" id="HandIn_prove_' . $item_count . '" name="HandIn_prove_' . $item_count . '[]" value="1" ' . $_documentStatusChk . '></td>' . "\r\n";
							}
							$x .= '<td><textarea class="other_'.$_studentId.'_'.$_recordDate.' '.$_recordDate_extra_other.' default_check" id="remarks_'.$item_count.'" name="remarks_'.$item_count.'" rows="1" cols="30"></textarea></td>'."\r\n";
						}

					}else{
						if(get_client_region() == 'zh_TW') {
                            $colspan = 4;
						} else {
							$colspan = 6;
							if($sys_custom['StudentAttendance']['SubmitAbsentLateRecordsToExternalSystem']) {
								$colspan++;
							}
							if($sys_custom['StudentAttendance']['LeaveType']) {
								$colspan++;
							}
						}
						$x .= '<td style="text-align:center" colspan="'.$colspan.'">'.$Lang['StudentAttendance']['ApplyLeave']['ExistingRecord'].'</td>'."\r\n";
					}

				if(get_client_region() == 'zh_TW') {
					if($leaveRecordAry[$i]['AttendanceType'] == 0) {
						$target_date = array();
						$date_diff = strtotime($_endDate) - strtotime($_startDate);
						$date_diff = round($date_diff/(60*60*24));
						for($j=0;$j<=$date_diff;$j++) {
							$current_date = date("Y-m-d", strtotime($_startDate) + ($j*60*60*24));
							$existAry[$_studentId][$current_date]['am'] = true;
							$existAry[$_studentId][$current_date]['pm'] = true;
						}
					} else if($leaveRecordAry[$i]['AttendanceType'] == 1) {
						$existAry[$_studentId][$_recordDate]['am'] = true;
					} else if($leaveRecordAry[$i]['AttendanceType'] == 2) {
						$existAry[$_studentId][$_recordDate]['pm'] = true;
					} else if($leaveRecordAry[$i]['AttendanceType'] == 3) {
						$existAry[$_studentId][$_recordDate]['am'] = true;
						$existAry[$_studentId][$_recordDate]['pm'] = true;
					}
				} else {
					$existAry[$_studentId][$_recordDate] = true;
				}
				$x .= '</tr>'."\r\n";
				$item_count++;
			}
			$head_count++;
		}
	$x .= '</tbody>'."\r\n";
$x .= '</table>'."\r\n";
$htmlAry['inputTbl'] = $x;
	
?>
<script type="text/javascript">
$(document).ready( function() {
	
	$(".toAbsent").change(function() {	
		if($(this).attr('checked')==true){
			choose_class = $(this).val();
			<?php if(get_client_region() == 'zh_TW') { ?>
            $.each(choose_class.split(' '), function(index, v) {
                v = $.trim(v);
                if(v != '') {
                    $("." + v).attr('checked', false);
                    $(".other_" + v).attr('disabled', true);
                }
            });
            $(this).attr('checked',true);
			<?php } else { ?>
			//$("."+choose_class).attr('checked',false);
			//$(this).attr('checked',true);
			//$(".other_"+choose_class).attr('disabled',true);
			<?php } ?>

			id = this.id;
			//$("#send_"+id).attr('disabled',false);
			$("#am_"+id).attr('disabled',false);
			$("#pm_"+id).attr('disabled',false);
			$("#reason_"+id).attr('disabled',false);
			$("#waived_"+id).attr('disabled',false);
			$("#HandIn_prove_"+id).attr('disabled',false);
			$("#remarks_"+id).attr('disabled',false);
            $("#leaveType_"+id).attr('disabled',false);
			
		}
		if($(this).attr('checked')==false){
			id = this.id;
			//$("#send_"+id).attr('disabled',true);
			$("#am_"+id).attr('disabled',true);
			$("#pm_"+id).attr('disabled',true);
			$("#reason_"+id).attr('disabled',true);
			$("#waived_"+id).attr('disabled',true);
			$("#HandIn_prove_"+id).attr('disabled',true);
			$("#remarks_"+id).attr('disabled',true);
            $("#leaveType_"+id).attr('disabled',true);
		}
	}); 
	
	$(".toAbsent_uncheck").change(function() { 		
		if($(this).attr('checked')==true){
			choose_class = $(this).val();
			<?php if(get_client_region() == 'zh_TW') { ?>
               $.each(choose_class.split(' '), function(index, v) {
                v = $.trim(v);
                if(v != '') {
                    $("." + v).attr('checked', false);
                    $(".other_" + v).attr('disabled', true);
                }
            });
            $(this).attr('checked',true);
            <?php } else { ?>
			//$("."+choose_class).attr('checked',false);
			//$(this).attr('checked',true);
			//$(".other_"+choose_class).attr('disabled',true);
			<?php } ?>

			id = this.id;
			//$("#send_"+id).attr('disabled',false);
			$("#am_"+id).attr('disabled',false);
			$("#pm_"+id).attr('disabled',false);
			$("#reason_"+id).attr('disabled',false);
			$("#waived_"+id).attr('disabled',false);
			$("#HandIn_prove_"+id).attr('disabled',false);
			$("#remarks_"+id).attr('disabled',false);
            $("#leaveType_"+id).attr('disabled',false);
		}	
		if($(this).attr('checked')==false){
			id = this.id;
			//$("#send_"+id).attr('disabled',true);
			$("#am_"+id).attr('disabled',true);
			$("#pm_"+id).attr('disabled',true);
			$("#reason_"+id).attr('disabled',true);
			$("#waived_"+id).attr('disabled',true);
			$("#HandIn_prove_"+id).attr('disabled',true);
			$("#remarks_"+id).attr('disabled',true);
            $("#leaveType_"+id).attr('disabled',true);
		}
	});

    $(".toAbsent_uncheck").trigger("click").trigger("change");
});

function alltoAbsent(){
	if($("#AlltoAbsent").attr('checked')==true){
		$(".toAbsent").attr('checked',true);
		$(".default_check").attr('disabled',false);		
		$(".toAbsent_uncheck").attr('checked',false);
		$(".default_uncheck").attr('disabled',true);		
		
    }
    
	if($("#AlltoAbsent").attr('checked')==false){
		$(".toAbsent").attr('checked',false);
		$(".default_check").attr('disabled',true);				
		$(".toAbsent_uncheck").attr('checked',false);	
		$(".default_uncheck").attr('disabled',true);		
			
    }
}
function goBack() {
	//window.location = "list.php";
	window.location = "<?=($classTeacherAccess? "../../class_teacher/apply_leave_list.php" : "list.php")?>";
}
var form_submited = false;
function check(){
	item_count =<?=$item_count?>;
	checked_count=0;
    checked_ampm_empty_count=0;
    for(i=0;i<item_count;i++){
       if($("#"+i).attr('checked')==true){
           var date_value = $("#"+i).val().trim();
           var am_count = $('.am.other_'+date_value+':checked').not(":disabled").size();
           var pm_count = $('.pm.other_'+date_value+':checked').not(":disabled").size();
           if(am_count > 1) {
               alert('<?=$Lang['StudentAttendance']['ApplyLeave']['AMPMDuplicate']?>');
               return;
           }
           if(pm_count > 1) {
               alert('<?=$Lang['StudentAttendance']['ApplyLeave']['AMPMDuplicate']?>');
               return;
           }
    	   checked_count++;
       }
       if($("#updateAS_"+i).attr('checked')==true){
    	   checked_count++;
       }
       if($("#send_"+i).attr('checked')==true){
    	   checked_count++;
       }

        if($("#am_"+i).attr('checked')==false&&$("#pm_"+i).attr('checked')==false){
            alert("<?=$Lang['StudentAttendance']['ApplyLeave']['AMPMChooseAlert']?>");
            return;
        }

        <?php if($sys_custom['StudentAttendance']['SubmitAbsentLateRecordsToExternalSystem']) { ?>
        if($("#SessionFrom_"+i).val() == '' && $("#SessionFrom_"+i).val() != $("#SessionTo_"+i).val()) {
            alert("<?=$Lang['StudentAttendance']['PlsSelectSession']?>");
            return;
        }

        if(Number($("#SessionFrom_"+i).val()) > Number($("#SessionTo_"+i).val())) {
            alert("<?=$Lang['StudentAttendance']['PlsSelectSession']?>");
            return;
        }
        <?php } ?>


    }

    if(checked_count==0)
        alert("<?=$Lang['StudentAttendance']['ApplyLeave']['ApproveLeaveAlert']?>");
    else{
        if(form_submited == false) {
            form_submited = true;
            form1.submit();
        }
    }
}
</script>
<form name="form1" id="form1" method="POST" action="approve_leave_update.php">
	<table width="100%" border="0" cellspacing="0" cellpadding="5">
		<tr>
		   <td>
			  <?=$htmlAry['navigation']?>
		   </td>
		</tr>
		<tr>		
		   <td>
              <p class="spacer"></p>
		   </td>
		</tr>
		<tr>	
		   <td>
			  <div class="table_board">
				 <?=$htmlAry['inputTbl']?>		
			  </div>
		   </td>
		</tr>
        <tr>
           <td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
        </tr>
        <tr>
           <td align="center">
               <?php if($hasPendding){?>
              <?= $linterface->GET_ACTION_BTN($button_submit, "button", "javascript:check();","submit2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
     	       <?php } ?>
               <?//= $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:window.location='list.php'","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
     	      <?= $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:window.location='".($classTeacherAccess? "../../class_teacher/apply_leave_list.php" : "list.php")."'","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
           </td>
        </tr>		       		
	</table>	
	<input type="hidden" name="item_count" id="item_count" value="<?=escape_double_quotes($item_count)?>">   
	<input type="hidden" id="targetDbField" name="targetDbField" value="<?=escape_double_quotes($targetDbField)?>" />
	<input type="hidden" id="targetStatus" name="targetStatus" value="<?=escape_double_quotes($targetStatus)?>" />
	<input type="hidden" id="cteach" name="cteach" value="<?=escape_double_quotes($classTeacherAccess)?>" />
</form>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>