<?php
## using: 

#####  Changing Log ########
# 
#	Date:	2017-09-18 	(Bill)	[2017-0908-1248-12235]
#		- Support Class Teacher to manage apply leave list
#		- Redirect to correct page after update
# 
############################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libapplyleave.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp.php");

intranet_auth();
intranet_opendb();

$lapplyleave = new libapplyleave();

// [2017-0908-1248-12235] Handling for Apply Leave (App) (Class Teacher)
if($sys_custom["StudentAttendance"]["Class_Teacher_Approve_Leave_Request"] && $_GET["cteach"]==1)
{
	include_once($PATH_WRT_ROOT."includes/libteaching.php");
	$lteaching = new libteaching();
	$TeachingClasses = $lteaching->returnTeacherClass($UserID);
	$isClassTeacher = count((array)$TeachingClasses) > 0;
}

### check access right
$canAccess = false;
if ($_SESSION["SSV_PRIVILEGE"]["plugin"]['attendancestudent'] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] && $plugin['eClassApp']) {
	$canAccess = true;
}
// [2017-0908-1248-12235]
if ($plugin['eClassApp'] && $_SESSION["SSV_PRIVILEGE"]["plugin"]['attendancestudent'] && $isClassTeacher) {
	$canAccess = true;
	$classTeacherAccess = true;
}
if (!$canAccess) {
	No_Access_Right_Pop_Up();
}


$recordIdAry = $_POST['recordIdAry'];
$targetDbField = $_POST['targetDbField'];
$targetStatus = $_POST['targetStatus'];


$successAry = array();
$lapplyleave->Start_Trans();

$successAry['updateStatus'] = $lapplyleave->updateApplyLeaveStatus($recordIdAry, $targetDbField, $targetStatus);

if ($targetStatus == 1) {
	// send push notification if acknowledged
	$libeClassApp = new libeClassApp();
	
	$leaveInfoAry = $lapplyleave->getApplyLeaveData($recordIdAry);
	$numOfLeave = count($leaveInfoAry);
	$studentIdAry = Get_Array_By_Key($leaveInfoAry, 'StudentID');
	
	$objUser = new libuser('', '', $studentIdAry);
	$studentParentMappingAry = BuildMultiKeyAssoc($objUser->getParentStudentMappingInfo($studentIdAry), 'StudentID', array('ParentID'), $SingleValue=0, $BuildNumericArray=1);
	
	$messageInfoAry = array();
	for ($i=0; $i<$numOfLeave; $i++) {
		$_studentId = $leaveInfoAry[$i]['StudentID'];
		$_dateInput = $leaveInfoAry[$i]['InputDate'];
		$objUser->LoadUserData($_studentId);
		
		// build message parent student relationship
		$_relatedParentIdAry = Get_Array_By_Key($studentParentMappingAry[$_studentId], 'ParentID');
		$_numOfParent = count($_relatedParentIdAry);
		for ($j=0; $j<$_numOfParent; $j++) {
			$__parentId = $_relatedParentIdAry[$j];
			
			$messageInfoAry[$i]['relatedUserIdAssoAry'][$__parentId] = array($_studentId);
		}
		
		// build message title and content
		$_messageTitle = $Lang['StudentAttendance']['ApplyLeaveAry']['NotificationAry']['AcceptApplicationTitle'];
		$_messageContent = $Lang['StudentAttendance']['ApplyLeaveAry']['NotificationAry']['AcceptApplicationContent'];
		$_messageContent = str_replace('<!--studentNameWithClass-->', Get_Lang_Selection($objUser->ChineseName, $objUser->EnglishName).' ('.$objUser->ClassName.'-'.$objUser->ClassNumber.')', $_messageContent);
		$_messageContent = str_replace('<!--datetime-->', date('Y-m-d H:i', strtotime($_dateInput)), $_messageContent);
		
		$messageInfoAry[$i]['messageTitle'] = $_messageTitle;
		$messageInfoAry[$i]['messageContent'] = $_messageContent;
	}
	
	// return success even if failed to send push message
	$success['sendPushMessage'] = $libeClassApp->sendPushMessage($messageInfoAry, $Lang['StudentAttendance']['ApplyLeaveAry']['NotificationAry']['AcceptApplicationTitle'], 'MULTIPLE MESSAGES', $isPublic='', $recordStatus=0);
}


if (in_array(false, $successAry)) {
	$lapplyleave->RollBack_Trans();
	$returnMsgKey = 'UpdateUnsuccess';
}
else {
	$lapplyleave->Commit_Trans();
	$returnMsgKey = 'UpdateSuccess';
}
intranet_closedb();


//header('Location: list.php?returnMsgKey='.$returnMsgKey);
$return_url = "list.php?returnMsgKey=".$returnMsgKey;
if($classTeacherAccess)
	$return_url = "../../class_teacher/apply_leave_list.php?returnMsgKey=".$returnMsgKey;
header("Location: $return_url");
?>