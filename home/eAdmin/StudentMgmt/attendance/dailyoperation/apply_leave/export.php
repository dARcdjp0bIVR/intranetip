<?php
// editing by Kenneth
/*
 *  2015-10-23 Kenneth Yau 
 * 		- Add Timestemp in csv file name
 * 		- Export CSV with orders and filter and keywords search
 * 
 *  2015-07-29(Shan): open this page
 */
 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libapplyleave.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();
$ldb = new libdb();
$linterface = new interface_html();
$lapplyleave = new libapplyleave();
$settingAssoAry = $lapplyleave->getSettingsAry();

### db table
$order = standardizeFormPostValue($_POST['order']);	// 1 => asc, 0 => desc
$field = standardizeFormPostValue($_POST['field']);
$applicationTimeType = standardizeFormPostValue($_POST['applicationTimeType']);
$applicationStatus = standardizeFormPostValue($_POST['applicationStatus']);
$documentStatus = standardizeFormPostValue($_POST['documentStatus']);
$keyword = standardizeFormPostValue($_POST['keyword']);
$pageNo = standardizeFormPostValue($_POST['pageNo']);
$isLimit = ''; //prase into $li->built_sql(); setting no limit

$li = new libdbtable2007($field, $order, $pageNo);

if(get_client_region() == 'zh_TW') {
	$temp =	array("ClassName", "ClassNumber", "StudentName", "r.InputDate", "AttendanceType","StartDate", "EndDate", "Duration", "SessionFrom","SessionTo","SessionDiff","LeaveTypeName","r.Reason");
} else {
	$temp = array("ClassName", "ClassNumber", "StudentName", "r.InputDate", "r.StartDate", "r.EndDate", "r.Duration", "r.Reason");
	if ($sys_custom['StudentAttendance']['LeaveType']) {
		$temp[] = 'LeaveTypeName';
	}
if ($settingAssoAry['enableApplyLeaveHomeworkPassSetting']==1) {
		$temp[] = "Method";
}else{

	}
}

$li->field_array = $temp;
$li->sql = $lapplyleave->getApplyLeaveRecordListPageSql($keyword, $applicationTimeType, $applicationStatus, $documentStatus, $forExport=true,$settingAssoAry['enableApplyLeaveHomeworkPassSetting']);
$listArr = $ldb->returnResultSet($li->built_sql($isLimit));  //$sql


$numOfData = count($listArr);
for ($i=0;$i<$numOfData;$i++){

	if(get_client_region() == 'zh_TW') {
		$temp = array();
		$temp[] = $listArr[$i]['ClassName'];
		$temp[] = $listArr[$i]['ClassNumber'];
		$temp[] = $listArr[$i]['StudentName'];
		$temp[] = $listArr[$i]['InputDate'];
		$temp[] = $listArr[$i]['AttendanceType'];
		$temp[] = $listArr[$i]['StartDate'];
		$temp[] = $listArr[$i]['EndDate'];
		$temp[] = $listArr[$i]['Duration'];

		$temp[] = $listArr[$i]['SessionFrom'];
		$temp[] = $listArr[$i]['SessionTo'];
		$temp[] = $listArr[$i]['SessionDiff'];
		$temp[] = $listArr[$i]['LeaveTypeName'];
		$temp[] = $listArr[$i]['Reason'];

	} else {
		$temp = array($listArr[$i][ClassName], $listArr[$i][ClassNumber], $listArr[$i][StudentName], $listArr[$i][InputDate], $listArr[$i][StartDate], $listArr[$i][EndDate], $listArr[$i][Duration]);
		if ($sys_custom['StudentAttendance']['LeaveType']) {
			$temp[] = $listArr[$i]['LeaveTypeName'];
		}
		$temp[] = $listArr[$i][Reason];
    if ($settingAssoAry['enableApplyLeaveHomeworkPassSetting']==1) {
			$temp[] = $listArr[$i][Method];
    }else{

		}
		$temp[] = $listArr[$i][DocumentLink];
		$temp[] = $listArr[$i][ApprovalStatus];
		$temp[] = $listArr[$i][DocumentStatus];
    }
	$exportArr[] = $temp;
}


$lexport = new libexporttext();

if(get_client_region() == 'zh_TW') {
	$temp = array();
	$temp[] = $Lang['General']['Class'];
	$temp[] = $Lang['SysMgr']['FormClassMapping']['ClassNo'];
	$temp[] = $Lang['Identity']['Student'];
	$temp[] = $Lang['StudentAttendance']['ApplyLeaveAry']['ApplicationTime'];
	$temp[] = $i_Attendance_Type;
	$temp[] = $Lang['General']['StartDate'];
	$temp[] = $Lang['General']['EndDate'];
	$temp[] = $Lang['StudentAttendance']['ApplyLeaveAry']['Duration'];
	$temp[] = $Lang['StudentAttendance']['SessionFrom'];
	$temp[] = $Lang['StudentAttendance']['SessionTo'];
	$temp[] = $Lang['StudentAttendance']['TotalSession'];
	$temp[] = $Lang['StudentAttendance']['LeaveType'];
	$temp[] = $Lang['General']['Reason'];
} else {
	$temp = array($Lang['General']['Class'], $Lang['SysMgr']['FormClassMapping']['ClassNo'], $Lang['Identity']['Student'], $Lang['StudentAttendance']['ApplyLeaveAry']['ApplicationTime'], $Lang['General']['StartDate'], $Lang['General']['EndDate'], $Lang['StudentAttendance']['ApplyLeaveAry']['Duration']);
	if ($sys_custom['StudentAttendance']['LeaveType']) {
		$temp[] = $Lang['StudentAttendance']['LeaveType'];
	}
	$temp[] = $Lang['General']['Reason'];
if ($settingAssoAry['enableApplyLeaveHomeworkPassSetting']==1) {
		$temp[] = $Lang['StudentAttendance']['ApplyLeaveAry']['homeworkDeliveryMethod'];
}else{

	}
	$temp[] = $Lang['StudentAttendance']['ApplyLeaveAry']['Document'];
	$temp[] = $Lang['StudentAttendance']['ApplyLeaveAry']['RecordAcknowledgeStatus'];
	$temp[] = $Lang['StudentAttendance']['ApplyLeaveAry']['DocumentApprovalStatus'];
}

$exportColumn = $temp;
$export_content = $lexport->GET_EXPORT_TXT($exportArr, $exportColumn);
$date = date("Ymd_His");
$filename = "Apply_Leave_".$date.".csv";
$lexport->EXPORT_FILE($filename, $export_content);

intranet_closedb();
?>