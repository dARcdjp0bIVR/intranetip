<?php
## using:

##################################### Change Log #####################################################
# 2020-06-05 Ray:     Add TW
# 2019-03-01 Cameron: - fix: should also sync eAttendance apply leave to eSchoolBus if the student has applied before, then delete and apply again
# 2019-02-12 Cameron: - overwrite $timeSection to whole day for eSchoolBus if $sys_custom['eSchoolBus']['syncAttendance']['fromSchoolOnly'] or $classTimeMode is half day [case #Y154315]
# 2019-02-01 Cameron: - don't sync apply leave record to eSchoolBus if the student is not taking school bus student.
# 2019-01-21 Carlos: added ApprovedAt and ApprovedBy to CARD_STUDENT_PRESET_LEAVE and sync from apply leave record. 
# 2018-11-05 Cameron: fix - apply standardizeFormPostValue to remark field
# 2018-10-26 Cameron: implement syncApplyLeaveRecordToeSchoolBus
# 2017-09-18 Bill:	Support Class Teacher to update apply leave list 	[2017-0908-1248-12235]
# 2016-08-15 Henry HM: Add HKUSPH Leave Logic
# 2014-12-08 Omas:	Add new column Hand-in Prove Document Status
#
######################################################################################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libapplyleave.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");

intranet_auth();
intranet_opendb();

$lapplyleave = new libapplyleave();

// [2017-0908-1248-12235] Handling for Apply Leave (App) (Class Teacher)
if($sys_custom["StudentAttendance"]["Class_Teacher_Approve_Leave_Request"] && $_POST["cteach"]==1)
{
	include_once($PATH_WRT_ROOT."includes/libteaching.php");
	$lteaching = new libteaching();
	$TeachingClasses = $lteaching->returnTeacherClass($UserID);
	$isClassTeacher = count((array)$TeachingClasses) > 0;
}

// [2017-0908-1248-12235]
if (!($_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || $isClassTeacher) || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}


$absent_late_record = array();

$values="";
$delim="";
if($sys_custom['eClassApp']['HKUSPH']){
	$array_record_id = array();
	
	include_once($PATH_WRT_ROOT."includes/HKUFlu/HKUFlu.php");
	include_once($PATH_WRT_ROOT."includes/HKUFlu/libHKUFlu_db.php");
	
	$db_hkuFlu = new libHKUFlu_db();
}

$updater_user_id = $_SESSION['UserID'];

for($i=0;$i<$item_count;$i++){

	if(get_client_region() == 'zh_TW') {
		if (count($_POST['toAbsent_' . $i]) == 1) {

			$studentID = $_POST['studentID_' . $i];
			$recordDate = $_POST['recordDate_' . $i];
			$reason = $_POST['reason_' . $i];
			$waive = 0;
			$HandIn_prove = 0;
			$remark = $_POST['remarks_' . $i];
			$am = $_POST['am_' . $i];
			$pm = $_POST['pm_' . $i];
			$recordId = $_POST['recordID_' . $i];

			$li = new libcardstudentattend2();
			$attendancetype = $_POST['AttendanceType_' . $i];
			$session_from = $_POST['SessionFrom_' . $i];
			$session_to = $_POST['SessionTo_' . $i];
			$leavetype_id = $_POST['leaveType_' . $i];
			$endDate = $_POST['EndDate_' . $i];

			$values_session = ", '".$li->Get_Safe_Sql_Query($leavetype_id)."','".$li->Get_Safe_Sql_Query($attendancetype)."'";
			$fields_session = ",LeaveTypeID, AttendanceType";

			if($attendancetype == 0) {
				$target_date = array();
				$date_diff = strtotime($endDate) - strtotime($recordDate);

				$date_diff = round($date_diff/(60*60*24));
				for($j=0;$j<=$date_diff;$j++) {
					$current_date = date("Y-m-d", strtotime($recordDate) + ($j*60*60*24));
					$target_date[] = $current_date;
				}

				if(count($target_date) == 1) {
					$td = $target_date[0];

					$ret = $li->checkPresetLeaveOverlap($studentID, $attendancetype, $td);
					if($ret[0] == true) {
						continue;
					}

					$values = "($studentID,'$td',2,'$reason','$Waive','$HandIn_prove','$remark',NOW(),NOW(), NOW(),'$updater_user_id', NULL $values_session)";
					$sql = "INSERT IGNORE INTO CARD_STUDENT_PRESET_LEAVE (StudentID,RecordDate,DayPeriod,Reason,Waive,DocumentStatus,Remark,DateInput,DateModified, ApprovedAt,ApprovedBy, RelatedRecordID $fields_session) VALUES $values";
					$successAry[] = $li->db_db_query($sql);
					$record_id = $li->db_insert_id();
					$related_record_id = $record_id;

					$values = "($studentID,'$td',3,'$reason','$Waive','$HandIn_prove','$remark',NOW(),NOW(), NOW(),'$updater_user_id', '$related_record_id' $values_session)";
					$sql = "INSERT IGNORE INTO CARD_STUDENT_PRESET_LEAVE (StudentID,RecordDate,DayPeriod,Reason,Waive,DocumentStatus,Remark,DateInput,DateModified, ApprovedAt,ApprovedBy, RelatedRecordID $fields_session) VALUES $values";
					$Result[] = $li->db_db_query($sql);

				} else if(count($target_date) > 1) {
					$related_record_id = 'NULL';
					$ret = $li->checkPresetLeaveOverlap($studentID, $attendancetype, $target_date[0], end($target_date));
					if($ret[0] == true) {
						continue;
					}

					foreach($target_date as $date_index=>$date) {
						$td = $date;
						if($date_index == 0) {
							$values = "($studentID,'$td',2,'$reason','$Waive','$HandIn_prove','$remark',NOW(),NOW(), NOW(),'$updater_user_id', NULL $values_session)";
							$sql = "INSERT IGNORE INTO CARD_STUDENT_PRESET_LEAVE (StudentID,RecordDate,DayPeriod,Reason,Waive,DocumentStatus,Remark,DateInput,DateModified,ApprovedAt,ApprovedBy,RelatedRecordID $fields_session) VALUES $values";
							$successAry[] = $li->db_db_query($sql);
							$record_id = $li->db_insert_id();
							$related_record_id = $record_id;

							$values = "($studentID,'$td',3,'$reason','$Waive','$HandIn_prove','$remark',NOW(),NOW(), NOW(),'$updater_user_id', '$related_record_id' $values_session)";
							$sql = "INSERT IGNORE INTO CARD_STUDENT_PRESET_LEAVE (StudentID,RecordDate,DayPeriod,Reason,Waive,DocumentStatus,Remark,DateInput,DateModified,ApprovedAt,ApprovedBy,RelatedRecordID $fields_session) VALUES $values";
							$Result[] = $li->db_db_query($sql);
						} else {
							$values = "($studentID,'$td',2,'$reason','$Waive','$HandIn_prove','$remark',NOW(),NOW(), NOW(),'$updater_user_id', '$related_record_id' $values_session)";
							$values .= ", ($studentID,'$td',3,'$reason','$Waive','$HandIn_prove','$remark',NOW(),NOW(), NOW(),'$updater_user_id',  '$related_record_id' $values_session)";
							$sql = "INSERT IGNORE INTO CARD_STUDENT_PRESET_LEAVE (StudentID,RecordDate,DayPeriod,Reason,Waive,DocumentStatus,Remark,DateInput,DateModified,ApprovedAt,ApprovedBy,RelatedRecordID $fields_session) VALUES $values";
							$successAry[] = $li->db_db_query($sql);
						}
					}
				}
			} else if($attendancetype == 1 || $attendancetype == 2) {
				$values = "";
				$td = $recordDate;

				$ret = $li->checkPresetLeaveOverlap($studentID, $attendancetype, $td);
				if($ret[0] == true) {
					continue;
				}

				if($attendancetype == 1) {
					$values = "($studentID,'$td',2,'$reason','$Waive','$HandIn_prove','$remark',NOW(),NOW(), NOW(),'$updater_user_id' $values_session)";
				} else if($attendancetype == 2) {
					$values = "($studentID,'$td',3,'$reason','$Waive','$HandIn_prove','$remark',NOW(),NOW(), NOW(),'$updater_user_id' $values_session)";
				}

				if($values != "") {
					$sql = "INSERT IGNORE INTO CARD_STUDENT_PRESET_LEAVE (StudentID,RecordDate,DayPeriod,Reason,Waive,DocumentStatus,Remark,DateInput,DateModified,ApprovedAt,ApprovedBy $fields_session) VALUES $values";
					$successAry[] = $li->db_db_query($sql);
				}
			}  else if($attendancetype == 3) {
				$td = $recordDate;

				$ret = $li->checkPresetLeaveOverlap($studentID, $attendancetype, $td, $td, $session_from, $session_to);
				if ($ret[0] == true) {
					continue;
				}
				$target_date_period = $ret[1];

				$related_record_id = '';
				if (in_array('am', $target_date_period)) {
					$values = "($studentID,'$td',2,'$reason','$Waive','$HandIn_prove','$remark',NOW(),NOW(), NOW(),'$updater_user_id', '" . $li->Get_Safe_Sql_Query($session_from) . "','" . $li->Get_Safe_Sql_Query($session_to) . "' $values_session)";
					$sql = "INSERT IGNORE INTO CARD_STUDENT_PRESET_LEAVE (StudentID,RecordDate,DayPeriod,Reason,Waive,DocumentStatus,Remark,DateInput,DateModified,ApprovedAt,ApprovedBy,SessionFrom,SessionTo $fields_session) VALUES $values";
					$successAry[] = $li->db_db_query($sql);
					$record_id = $li->db_insert_id();
					$related_record_id = $record_id;
				}

				if (in_array('pm', $target_date_period)) {
					$values = "($studentID,'$td',3,'$reason','$Waive','$HandIn_prove','$remark',NOW(),NOW(), NOW(),'$updater_user_id', '" . $li->Get_Safe_Sql_Query($session_from) . "','" . $li->Get_Safe_Sql_Query($session_to) . "' $values_session)";
					$sql = "INSERT IGNORE INTO CARD_STUDENT_PRESET_LEAVE (StudentID,RecordDate,DayPeriod,Reason,Waive,DocumentStatus,Remark,DateInput,DateModified,ApprovedAt,ApprovedBy,SessionFrom,SessionTo $fields_session) VALUES $values";
					$successAry[] = $li->db_db_query($sql);
					$record_id = $li->db_insert_id();
					if ($related_record_id != '' && $record_id != '') {
						$sql = "UPDATE CARD_STUDENT_PRESET_LEAVE SET RelatedRecordID='$related_record_id' WHERE RecordID='$record_id'";
						$successAry[] = $li->db_db_query($sql);
					}
				}

			}
		}

	} else {
		if (count($_POST[toAbsent_ . $i]) == 1) {

			$studentID = $_POST[studentID_ . $i];
			$recordDate = $_POST[recordDate_ . $i];
			$reason = $_POST[reason_ . $i];
			$waive = count($_POST[waived_ . $i]);
			$HandIn_prove = $_POST[HandIn_prove_ . $i][0];
			$remark = $_POST[remarks_ . $i];
			$am = count($_POST[am_ . $i]);
			$pm = count($_POST[pm_ . $i]);
			$recordId = $_POST[recordID_ . $i];
			//$send_push_notification = count($_POST[send_.$i]);

			// 		if($send_push_notification==1){
			// 			$recordIdAry[] = $recordId;
			// 		}

			$values_session = "";
			$fields_session = "";
			if ($sys_custom['StudentAttendance']['SubmitAbsentLateRecordsToExternalSystem']) {
				$SessionFrom = $_POST['SessionFrom_' . $i];
				$SessionTo = $_POST['SessionTo_' . $i];
				if($SessionFrom == '') {
					$SessionFrom_value = "NULL";
				} else {
					$SessionFrom_value = "'$SessionFrom'";
				}

				if($SessionTo == '') {
					$SessionTo_value = "NULL";
				} else {
					$SessionTo_value = "'$SessionTo'";
				}
				$values_session = ", $SessionFrom_value, $SessionTo_value";
				$fields_session = ",SessionFrom,SessionTo";
			}

			if($sys_custom['StudentAttendance']['LeaveType']) {
				$leavetype_id = $_POST['leaveType_' . $i];
				$values_session .= ", '$leavetype_id'";
				$fields_session .= ",LeaveTypeID";
			}


			$timeSection = array();
			if ($am == 1) {
				$timeSection[] = 'AM';
				$values .= $delim . "($studentID,'$recordDate',2,'$reason','$waive','$HandIn_prove' ,'$remark',NOW(),NOW(),NOW(),'$updater_user_id' $values_session)";
				$delim = ",";
				if ($sys_custom['eClassApp']['HKUSPH']) {
					if (count($db_hkuFlu->getPresetLeaveByStudentDatePeriod($studentID, $recordDate, 2)) <= 0) {
						array_push($array_record_id, $recordId);
					}
				}
				$DayPeriod = PROFILE_DAY_TYPE_AM;
			}
			if ($pm == 1) {
				$timeSection[] = 'PM';
				$values .= $delim . "($studentID,'$recordDate',3,'$reason','$waive','$HandIn_prove' ,'$remark',NOW(),NOW(),NOW(),'$updater_user_id' $values_session)";
				$delim = ",";
				if ($sys_custom['eClassApp']['HKUSPH']) {
					if (count($db_hkuFlu->getPresetLeaveByStudentDatePeriod($studentID, $recordDate, 3)) <= 0) {
						array_push($array_record_id, $recordId);
					}
				}
				$DayPeriod = PROFILE_DAY_TYPE_PM;
			}

			if ($am == 1 && $pm == 1) {
				$DayPeriod = PROFILE_DAY_TYPE_WD;
			}

			if ($sys_custom['StudentAttendance']['SubmitAbsentLateRecordsToExternalSystem']) {
				$absent_late_record[] = array("StudentID" => $studentID,
					"RecordDate" => $recordDate,
					"DayPeriod" => $DayPeriod,
					"EnrolGroupID" => '',
					"EnrolEventID" => '',
					"TargetStatus" => CARD_LEAVE_NORMAL,
					"SessionFrom" => $SessionFrom,
					"SessionTo" => $SessionTo);
			}

			// sync record to eSchoolBus
			if ($plugin['eSchoolBus'] && count($timeSection)) {
				include_once($PATH_WRT_ROOT . "includes/eSchoolBus/libSchoolBus_db.php");
				$leSchoolBus = new libSchoolBus_db();
				$isTakingBusStudent = $leSchoolBus->isTakingBusStudent($studentID);
				if ($isTakingBusStudent) {

					if ($sys_custom['eSchoolBus']['syncAttendance']['fromSchoolOnly']) {
						$timeSection = array('AM', 'PM');      // overwrite
					} else {
						$classTimeMode = $leSchoolBus->getClassTimeMode('', $studentID);
						if ($classTimeMode != 'WD') {         // AM or PM mode (half day class)
							$timeSection = array('AM', 'PM');  // overwrite, apply leave is regarded as whole day
						}
					}

					$remark = standardizeFormPostValue($remark);
					$successAry['syncApplyLeaveRecordToeSchoolBus'] = $leSchoolBus->syncApplyLeaveRecordFromAttendance($recordId, $recordDate, $remark, $timeSection);
				}
			}

		} else {
			// sync record to eSchoolBus
			if ($plugin['eSchoolBus']) {
				$studentID = $_POST[studentID_ . $i];
				$recordDate = $_POST[recordDate_ . $i];
				$timeSection = array();
				$lcardattend = new libcardstudentattend2();
				$amAppliedLeave = $lcardattend->checkStudentApplyLeave($studentID, $recordDate, PROFILE_DAY_TYPE_AM);
				if (count($amAppliedLeave)) {
					$recordId = $amAppliedLeave[$studentID];
					$timeSection[] = 'AM';
					$remarkAry = $lcardattend->getStudentApplyLeaveRemark($studentID, $recordDate, PROFILE_DAY_TYPE_AM);
					$remark = $remarkAry[$studentID]['Remark'];
				}

				$pmAppliedLeave = $lcardattend->checkStudentApplyLeave($studentID, $recordDate, PROFILE_DAY_TYPE_PM);
				if (count($pmAppliedLeave)) {
					$recordId = $pmAppliedLeave[$studentID];
					$timeSection[] = 'PM';
					$remarkAry = $lcardattend->getStudentApplyLeaveRemark($studentID, $recordDate, PROFILE_DAY_TYPE_PM);
					$remark = $remarkAry[$studentID]['Remark'];
				}
				include_once($PATH_WRT_ROOT . "includes/eSchoolBus/libSchoolBus_db.php");
				$leSchoolBus = new libSchoolBus_db();
				$isTakingBusStudent = $leSchoolBus->isTakingBusStudent($studentID);
				if ($isTakingBusStudent && count($timeSection)) {

					if ($sys_custom['eSchoolBus']['syncAttendance']['fromSchoolOnly']) {
						$timeSection = array('AM', 'PM');      // overwrite
					} else {
						$classTimeMode = $leSchoolBus->getClassTimeMode('', $studentID);
						if ($classTimeMode != 'WD') {         // AM or PM mode (half day class)
							$timeSection = array('AM', 'PM');  // overwrite, apply leave is regarded as whole day
						}
					}

					$successAry['syncApplyLeaveRecordToeSchoolBus'] = $leSchoolBus->syncApplyLeaveRecordFromAttendance($recordId, $recordDate, $remark, $timeSection);

				}
			}


		}
	}

	if(count($_POST[send_.$i])==1){
		$recordIdAry_send[] = $_POST[recordID_.$i];
	}
	
	if(count($_POST[updateAS_.$i])==1){
		$recordIdAry_updateAS[] = $_POST[recordID_.$i];
	}
}

//update acknowledgment status
if($recordIdAry_updateAS!=""){
	$successAry['updateStatus'] = $lapplyleave->updateApplyLeaveStatus($recordIdAry_updateAS, $targetDbField, $targetStatus);
}

// send push notification
if($recordIdAry_send!=""){
	$libeClassApp = new libeClassApp();
	
	$leaveInfoAry = $lapplyleave->getApplyLeaveData($recordIdAry_send);
	$numOfLeave = count($leaveInfoAry);
	$studentIdAry = Get_Array_By_Key($leaveInfoAry, 'StudentID');
	
	$objUser = new libuser('', '', $studentIdAry);
	$studentParentMappingAry = BuildMultiKeyAssoc($objUser->getParentStudentMappingInfo($studentIdAry), 'StudentID', array('ParentID'), $SingleValue=0, $BuildNumericArray=1);
	
	$messageInfoAry = array();
	for ($i=0; $i<$numOfLeave; $i++) {
		$_studentId = $leaveInfoAry[$i]['StudentID'];
		$_dateInput = $leaveInfoAry[$i]['InputDate'];
		$_startDate = $leaveInfoAry[$i]['StartDate'];
		$_startDateType = $leaveInfoAry[$i]['StartDateType'];
		$_endDate = $leaveInfoAry[$i]['EndDate'];
		$_endDateType = $leaveInfoAry[$i]['EndDateType'];
		$objUser->LoadUserData($_studentId);
	
		// build message parent student relationship
		$_relatedParentIdAry = Get_Array_By_Key($studentParentMappingAry[$_studentId], 'ParentID');
		$_numOfParent = count($_relatedParentIdAry);
		for ($j=0; $j<$_numOfParent; $j++) {
			$__parentId = $_relatedParentIdAry[$j];
	
			$messageInfoAry[$i]['relatedUserIdAssoAry'][$__parentId] = array($_studentId);
		}
	
		// build message title and content
		$_messageTitle = $Lang['StudentAttendance']['ApplyLeaveAry']['NotificationAry']['AcceptApplicationTitle'];
		$_messageContent = $Lang['StudentAttendance']['ApplyLeaveAry']['NotificationAry']['AcceptApplicationContent'];
		$_messageContent = str_replace('<!--studentNameEnWithClass-->', $objUser->EnglishName.' ('.$objUser->ClassName.'-'.$objUser->ClassNumber.')', $_messageContent);
		$_messageContent = str_replace('<!--studentNameChWithClass-->', $objUser->ChineseName.' ('.$objUser->ClassName.'-'.$objUser->ClassNumber.')', $_messageContent);
		$_messageContent = str_replace('<!--datetime-->', date('Y-m-d H:i', strtotime($_dateInput)), $_messageContent);
		$_messageContent = str_replace ( '<!--startDateTime-->', date ( 'Y-m-d', strtotime ( $_startDate) ) . ' (' . $_startDateType. ')', $_messageContent);
		$_messageContent = str_replace ( '<!--endDateTime-->', date ( 'Y-m-d', strtotime ( $_endDate) ) . ' (' . $_endDateType. ')', $_messageContent);
		
		$messageInfoAry[$i]['messageTitle'] = $_messageTitle;
		$messageInfoAry[$i]['messageContent'] = $_messageContent;
	}
	
	// return success even if failed to send push message
	$successAry['sendPushMessage'] = $libeClassApp->sendPushMessage($messageInfoAry, $Lang['StudentAttendance']['ApplyLeaveAry']['NotificationAry']['AcceptApplicationTitle'], 'MULTIPLE MESSAGES', $isPublic='', $recordStatus=0);
}


//insert into db

if(get_client_region() == 'zh_TW') {

} else {
	if ($values != "") {
		$late_records_ids = array();
		foreach ((array)$values as $value) {
			$sql = " INSERT IGNORE INTO CARD_STUDENT_PRESET_LEAVE (StudentID,RecordDate,DayPeriod,Reason,Waive,DocumentStatus,Remark,DateInput,DateModified,ApprovedAt,ApprovedBy $fields_session) VALUES $values";
			$li = new libcardstudentattend2();
			$Result = $li->db_db_query($sql);
			if ($Result) {
				if ($sys_custom['StudentAttendance']['SubmitAbsentLateRecordsToExternalSystem']) {
					foreach ($absent_late_record as $temp) {
						$ret = $li->InsertSubmittedAbsentLateRecord($temp['StudentID'], $temp['RecordDate'], $temp['DayPeriod'], $temp['EnrolGroupID'], $temp['EnrolEventID'], $temp['TargetStatus'], $temp['SessionFrom'], $temp['SessionTo']);
						if ($ret) {
							$late_records_ids[] = $li->db_insert_id();
						}
					}
				}

				if ($sys_custom['eClassApp']['HKUSPH']) {
					$last_preset_leave_id = $li->db_insert_id();
					for ($i = 0; $i < count($array_record_id); $i++) {
//					print_r(array($array_record_id[$i], $last_preset_leave_id + $i));
						$db_hkuFlu->updatePresetLeaveIDOfSickLeaveByLeaveID($array_record_id[$i], $last_preset_leave_id + $i);
					}
				}
				$successAry['insertSQL'] = true;
			} else {
				$successAry['insertSQL'] = false;
			}
		}

		if ($sys_custom['StudentAttendance']['SubmitAbsentLateRecordsToExternalSystem']) {
			if (count($late_records_ids) > 0) {
				$site = (checkHttpsWebProtocol() ? "https" : "http") . "://" . $_SERVER['SERVER_NAME'] . ($_SERVER["SERVER_PORT"] != 80 ? ":" . $_SERVER["SERVER_PORT"] : "");
				$cmd = "/usr/bin/nohup wget --no-check-certificate -q '$site/schedule_task/send_student_absent_late_record.php?RecordIDs=" . OsCommandSafe(implode(',', $late_records_ids)) . "' > /dev/null &";
				shell_exec($cmd);
			}
		}
	}
}

intranet_closedb();

if (in_array(false, $successAry)) {
	$Msg = 2;
}else{
	$Msg = 1;
}

$return_url = "list.php?Msg=".$Msg;
if($isClassTeacher)
	$return_url = "../../class_teacher/apply_leave_list.php?Msg=".$Msg;
header("Location: $return_url");
?>