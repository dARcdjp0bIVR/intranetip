<?php
## using: 

##################### Change Log #####################
#
#   Date:   2019-10-14  (Bill)      [DM#3684]
#           - merge rows for more than 1 day records
#
######################################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libapplyleave.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libwordtemplates.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$lcardstudentattend2 = new libcardstudentattend2();
$lapplyleave = new libapplyleave();
$lword = new libwordtemplates();
$libdb = new libdb();

// [2017-0908-1248-12235] Handling for Apply Leave (App) (Class Teacher)
if($sys_custom["StudentAttendance"]["Class_Teacher_Approve_Leave_Request"] && $_GET["cteach"]==1)
{
	include_once($PATH_WRT_ROOT."includes/libteaching.php");
	$lteaching = new libteaching();
	$TeachingClasses = $lteaching->returnTeacherClass($UserID);
	$isClassTeacher = count((array)$TeachingClasses) > 0;
}

### check access right
$canAccess = false;
if ($_SESSION["SSV_PRIVILEGE"]["plugin"]['attendancestudent'] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] && $plugin['eClassApp']) {
	$canAccess = true;
}
// [2017-0908-1248-12235]
if ($plugin['eClassApp'] && $_SESSION["SSV_PRIVILEGE"]["plugin"]['attendancestudent'] && $isClassTeacher) {
	$canAccess = true;
	$classTeacherAccess = true;
}
if (!$canAccess) {
	No_Access_Right_Pop_Up();
}

$recordIdAry = $_POST['recordIdAry'];

### Tab display
// $TAGS_OBJ[] = array($displayLang, $onclickJs, $isSelectedTab);
$TAGS_OBJ[] = array($Lang['StudentAttendance']['ApplyLeaveAry']['ApplyLeave(App)']);
$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageDailyOperation_ApplyLeaveApp";
$MODULE_OBJ = $lcardstudentattend2->GET_MODULE_OBJ_ARR();

// [2017-0908-1248-12235] Tab Display for Apply Leave (App) (Class Teacher)
if($classTeacherAccess)
{
	unset($TAGS_OBJ);
	unset($MODULE_OBJ);
	
	$MODULE_OBJ = array();
	$MODULE_OBJ['title'] = $ip20TopMenu['eAttendance'];
	$TAGS_OBJ[] = array($Lang['StudentAttendance']['ApplyLeaveAry']['ApplyLeave(App)'].' ('.$i_Teaching_ClassTeacher.')');
}

$linterface->LAYOUT_START(urldecode($Msg));

### Navigation
// $navigationAry[] = array($displayLang, $onclickJs);
$navigationAry[] = array($Lang['StudentAttendance']['ApplyLeaveAry']['ApplyLeaveList'], 'javascript: goBack();');
$navigationAry[] = array($Lang['StudentAttendance']['ApplyLeaveAry']['Reject']);
$htmlAry['navigation'] = $linterface->GET_NAVIGATION_IP25($navigationAry);

### Get apply leave records
$leaveRecordAry = $lapplyleave->getApplyLeaveDataOrderByStudent($recordIdAry);
$numOfLeaveRecord = count($leaveRecordAry);

### Get student info
$studentIdAry = Get_Array_By_Key($leaveRecordAry, 'StudentID');
$userObj = new libuser('', '', $studentIdAry);

### Get absent reason 
$words_absence = $lword->getWordListAttendance(1);
foreach($words_absence as $key=>$word) {
	$words_absence[$key]= htmlspecialchars($word);
}

### Build table
$x = '';
$x .= '<table class="common_table_list_v30 edit_table_list_v30" id="ContentTable">'."\r\n";
	$x .= '<thead>'."\r\n";
		$x .= '<tr>'."\r\n";
			$x .= '<th>#</th>'."\r\n";
			$x .= '<th>'.$Lang['General']['Class'].'</th>'."\r\n";
			$x .= '<th>'.$Lang['SysMgr']['FormClassMapping']['ClassNo'].'</th>'."\r\n";
			$x .= '<th>'.$Lang['Identity']['Student'].'</th>'."\r\n";
            if(get_client_region() == 'zh_TW') {
                $x .= '<th>'.$Lang['General']['StartDate'].'</th>'."\r\n";
                $x .= '<th>'.$Lang['General']['EndDate'].'</th>'."\r\n";
                $x .= '<th>'.$i_Attendance_Type.'</th>'."\r\n";
                $x .= '<th>'.$Lang['StudentAttendance']['SessionFrom'].'</th>'."\r\n";
                $x .= '<th>'.$Lang['StudentAttendance']['SessionTo'].'</th>'."\r\n";
                $x .= '<th>'.$Lang['StudentAttendance']['TotalSession'].'</th>'."\r\n";
                $x .= '<th>'.$Lang['StudentAttendance']['LeaveType'].'</th>'."\r\n";
            } else {
			$x .= '<th>'.$Lang['General']['Date'].'</th>'."\r\n";
            }
			$x .= '<th>'.$Lang['StudentAttendance']['ApplicationReason'].'</th>'."\r\n";			
			$x .= '<th>'.$Lang['StudentAttendance']['RejectReason'].'</th>'."\r\n";			
		$x .= '</tr>'."\r\n";
	$x .= '</thead>'."\r\n";
	$x .= '<tbody>'."\r\n";
	
	    $item_count = 0;
        $hasPendding = false;
        for ($i=0; $i<$numOfLeaveRecord; $i++)
        {
			$_recordId = $leaveRecordAry[$i]['RecordID'];
			$_studentId = $leaveRecordAry[$i]['StudentID'];
			$_startDate = $leaveRecordAry[$i]['StartDate'];
			$_startDateType = $leaveRecordAry[$i]['StartDateType'];
			$_endDate = $leaveRecordAry[$i]['EndDate'];
			$_endDateType = $leaveRecordAry[$i]['EndDateType'];
			$_duration = $leaveRecordAry[$i]['Duration'];
			$_reason = $leaveRecordAry[$i]['Reason'];
			$_approvalStatus = $leaveRecordAry[$i]['ApprovalStatus'];
			if ($_approvalStatus != 0) {
				// if not pendding => skip
				continue;
			}
            $hasPendding = true;

            $_numOfRow = ceil($_duration);
			$userObj->LoadUserData($_studentId);
			
			if($_startDateType=="PM" && $_endDateType=="AM")   $_numOfRow++;

			if(get_client_region() == 'zh_TW') {
				$_numOfRow = 1;
			}

			for ($j=0; $j<$_numOfRow; $j++)
			{
				$_recordDate = date('Y-m-d', strtotime('+'.$j.' day', strtotime($_startDate)));
				
				// determine leave date type
				$_leaveDateType = '';
				if($_startDateType=="AM"&&$_endDateType=="PM"){				
					$_leaveDateType = 'WD';					
				}
				else{
					if ($j==0) {
						$_leaveDateType = $_startDateType;
					}
					else if ($j == ($_numOfRow-1)) {
						$_leaveDateType = $_endDateType;
					}
					else {
						$_leaveDateType = 'WD';
					}
				}
				
				$x .= '<tr class="'.$_trClass.'">'."\r\n";
    				if($j == 0)
    				{
    				    $x.='<input type="hidden" name="studentID_'.$item_count.'" value="'.$_studentId.'">';
    				    $x.='<input type="hidden" name="recordDate_'.$item_count.'" value="'.$_recordDate.'">';
    				    $x.='<input type="hidden" name="recordID_'.$item_count.'" value="'.$_recordId.'">';
				    
    				    $x .= '<td rowspan="'.$_numOfRow.'">'.($i+1).'</td>'."\r\n";
    					$x .= '<td rowspan="'.$_numOfRow.'">'.$userObj->ClassName.'</td>'."\r\n";
    					$x .= '<td rowspan="'.$_numOfRow.'">'.$userObj->ClassNumber.'</td>'."\r\n";
    					$x .= '<td rowspan="'.$_numOfRow.'">'.Get_Lang_Selection($userObj->ChineseName, $userObj->EnglishName).'</td>'."\r\n";
				    }

                    if(get_client_region() == 'zh_TW') {
                        $endDate_text = $Lang['General']['EmptySymbol'];
                        if($leaveRecordAry[$i]['AttendanceType'] == 0) {
							$endDate_text = $_endDate;
						}

                        $x .= '<td>'.$_startDate.'</td>'."\r\n";
                        $x .= '<td>'.$endDate_text.'</td>'."\r\n";
                        $x .= '<td>'.$leaveRecordAry[$i]['AttendanceTypeText'].'</td>'."\r\n";
                        $x .= '<td>'.$leaveRecordAry[$i]['SessionFromText'].'</td>'."\r\n";
                        $x .= '<td>'.$leaveRecordAry[$i]['SessionToText'].'</td>'."\r\n";
                        $x .= '<td>'.$leaveRecordAry[$i]['SessionDiff'].'</td>'."\r\n";
                        $x .= '<td>'.$leaveRecordAry[$i]['LeaveTypeName'].'</td>'."\r\n";
                    } else {
					$x .= '<td>'.$_recordDate.'('.$_leaveDateType.')</td>'."\r\n";
                    }
					if($j == 0)
					{
					    $x .= '<td rowspan="'.$_numOfRow.'">'.$_reason.'</td>'."\r\n";
					    $x .= '<td rowspan="'.$_numOfRow.'"><textarea class="other_'.$_studentId.'_'.$_recordDate.' default_check" id="reject_'.$item_count.'" name="reject_'.$item_count.'" rows="'.$_numOfRow.'" cols="30"></textarea></td>'."\r\n";
					}
				$x .= '</tr>'."\r\n";
				
				if($j == 0)
				{
				    $item_count++;
				}
			}
		}
	$x .= '</tbody>'."\r\n";
$x .= '</table>'."\r\n";
$htmlAry['inputTbl'] = $x;

?>
<script type="text/javascript">
$(document).ready( function() {
	
});
function goBack() {
	//window.location = "list.php";
	window.location = "<?=($classTeacherAccess? "../../class_teacher/apply_leave_list.php" : "list.php")?>";
}
</script>

<form name="form1" id="form1" method="POST" action="reject_leave_update.php">
	<table width="100%" border="0" cellspacing="0" cellpadding="5">
		<tr>
		   <td>
			  <?=$htmlAry['navigation']?>
		   </td>
		</tr>
		<tr>		
		   <td>
              <p class="spacer"></p>
		   </td>
		</tr>
		<tr>	
		   <td>
			  <div class="table_board">
				 <?=$htmlAry['inputTbl']?>		
			  </div>
		   </td>
		</tr>
        <tr>
           <td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
        </tr>
        <tr>
           <td align="center">
               <?php if($hasPendding){?>
               <?= $linterface->GET_ACTION_BTN($button_submit, "submit", "","submit2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
               <?php } ?>
               <?//= $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:window.location='list.php'","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
     	      <?= $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:window.location='".($classTeacherAccess? "../../class_teacher/apply_leave_list.php" : "list.php")."'","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
           </td>
        </tr>		       		
	</table>	
	<input type="hidden" name="item_count" id="item_count" value="<?=$item_count?>">   
	<input type="hidden" id="cteach" name="cteach" value="<?=$classTeacherAccess?>" />
</form>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>