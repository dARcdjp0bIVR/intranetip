<?php
// Editing by 
/*
 * 2019-09-23 Cameron
 * - as change RecordStatus, AddedFrom, AttendanceRecordID, AttendanceApplyLeaveID to timeslot table,
 * should apply updateSchoolBusTimeSlotApplyLeaveToDelete() to related logic for eSchoolBus
 * 2019-02-11 Cameron:
 *  - apply parameter $attendanceType to syncAbsentRecordFromAttendance();
 * 2019-02-01 Cameron: [ip.2.5.10.2.1] 
 *  - sync record to eSchoolBus for early leave status: check if apply leave record already exist, add record if not exit, else update reason and teacher's remark 
 *  - don't sync apply leave record to eSchoolBus if the student is not taking school bus student.
 * 2018-12-17 Cameron: [ip.2.5.10.2.1] sync early leave record to eSchoolBus ( as absent of taking school bus) 
 * 2017-05-31 Carlos: [ip.2.5.8.7.1] Check and skip outdated record if attendance record last modified time is later than page load time. 
 * 2017-02-24 Carlos: [ip2.5.8.3.1] added status change log for tracing purpose. required new method libcardstudentattend2->log()
 * 2016-06-29 Carlos: Update Submitted Prove Document Status field.
 * 2015-04-23 Carlos: Fix office remark array index shift problem after disable the input. 
 * 2015-04-22 Carlos: Cater AM/PM modify by and date modified fields.
 * 2015-02-11: Carlos - Added teacher's remark and office remark.
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

# class used
$lcardattend = new libcardstudentattend2();
$has_magic_quotes = $lcardattend->is_magic_quotes_active;

/*
if(sizeof($user_id) >0)
{
	# check page load time 
	if($lcardattend->isDataOutDated($user_id,$TargetDate,$PageLoadTime)){
		$error = 1;  // data outdated
		
		$return_page = "show.php";
		
		$return_page = $return_page."?DayType=$DayType&TargetDate=$TargetDate&error=$error&Msg=".urlencode($Lang['StudentAttendance']['DataOutdatedWarning']);
		header("Location: $return_page");
		exit();	
	}

}
*/
### Set Date from previous page
$ts_record = strtotime($TargetDate);
if ($ts_record == -1 || ($DayType!=PROFILE_DAY_TYPE_PM && $DayType!=PROFILE_DAY_TYPE_AM) )
{
    header("Location: index.php");
    exit();
}

if ($plugin['eSchoolBus']) {
    include ($PATH_WRT_ROOT. "includes/eSchoolBus/schoolBusConfig.php");
    include_once($PATH_WRT_ROOT."includes/eSchoolBus/libSchoolBus_db.php");
    $leSchoolBus = new libSchoolBus_db();
}

//$use_magic_quotes = get_magic_quotes_gpc();
$use_magic_quotes = $lcardattend->is_magic_quotes_active;

$txt_year = date('Y',$ts_record);
$txt_month = date('m',$ts_record);
$txt_day = date('d',$ts_record);
/*$year = getCurrentAcademicYear();
$semester = getCurrentSemester();*/

list($school_year_id, $school_year, $semester_id, $semester) = getAcademicYearInfoAndTermInfoByDate($TargetDate);

### update daily records
$card_log_table_name = "CARD_STUDENT_DAILY_LOG_".$txt_year."_".$txt_month;

$log_row = date("Y-m-d H:i:s").' '.$_SERVER['REQUEST_URI'].' '.$_SESSION['UserID'].' ';

$studentIdToDailyLog = $lcardattend->getDailyLogRecords(array('RecordDate'=>$TargetDate,'StudentID'=>$user_id,'StudentIDToRecord'=>1));

for($i=0; $i<sizeOf($user_id); $i++)
{
  $my_user_id = $user_id[$i];
  $my_day = $txt_day;
  $my_drop_down_status = $drop_down_status[$i];
  $my_record_id = $record_id[$i];

	// check and skip outdated record if attendance record last modified time is later than page load time
	if(isset($studentIdToDailyLog[$my_user_id])){
		if($studentIdToDailyLog[$my_user_id]['DateModified'] != '')
		{
			$am_modified_ts = strtotime($studentIdToDailyLog[$my_user_id]['DateModified']);
			if($am_modified_ts >= $PageLoadTime) continue;
		}
		if($studentIdToDailyLog[$my_user_id]['PMDateModified'] != '')
		{
			$pm_modified_ts = strtotime($studentIdToDailyLog[$my_user_id]['PMDateModified']);
			if($pm_modified_ts >= $PageLoadTime) continue;
		}
	}

  // Retrieve Waived only if early leave
  ($my_drop_down_status == 3) ? ($my_record_status = (${"waived_".$my_user_id} == '') ? 0 : 1) : "";
	
	// KENNETH CHUNG CODE
	if ($DayType == PROFILE_DAY_TYPE_AM) {
		$InSchoolTimeField = "InSchoolTime";
		$StatusField = "AMStatus";
		$DateModifiedField = "DateModified";
		$ModifyByField = "ModifyBy";
	}
	else {
		$lcardattend->retrieveSettings();
		if($lcardattend->attendance_mode==1)	# retrieve InSchoolTime
		{
			$InSchoolTimeField = "InSchoolTime";
		}
		else # retrieve LunchBackTime
		{
			$InSchoolTimeField = "LunchBackTime";
		}
		$StatusField = "PMStatus";
		$DateModifiedField = "PMDateModified";
		$ModifyByField = "PMModifyBy";
	}
  
  if ($my_drop_down_status == 3)       # Early Leave
  {
    # Reason input
    $txtReason = trim(htmlspecialchars_decode(stripslashes(${"reason$i"}), ENT_QUOTES));
    
    $Result[$my_user_id.'SetProfile'] = $lcardattend->Set_Profile($my_user_id,$TargetDate,$DayType,$my_drop_down_status,$txtReason,"|**NULL**|","|**NULL**|","|**NULL**|","",false,$my_record_status,true);
  
  	$document_status = $_POST['DocumentStatus_'.$my_user_id];
  	$sql = "UPDATE CARD_STUDENT_PROFILE_RECORD_REASON SET DocumentStatus='$document_status' WHERE RecordDate='$TargetDate' AND StudentID='$my_user_id' AND DayType='$DayType' AND RecordType='".PROFILE_TYPE_EARLY."'";
  	$lcardattend->db_db_query($sql);
  	
  	if ($plugin['eSchoolBus']) {
  	    $isTakingBusStudent = $leSchoolBus->isTakingBusStudent($my_user_id);
  	    if ($isTakingBusStudent) {
      	    $teacherRemark = trim($remark[$i]);
      	    if($has_magic_quotes){
      	        $teacherRemark = stripslashes($teacherRemark);
      	    }
      	    $input_reason = stripslashes(trim(${"reason$i"}));
      	    $dayTypeStr = 'PM';
      	    
      	    // check if apply leave record already exist
      	    $isAppliedLeave = $leSchoolBus->isTimeSlotAppliedLeave($my_user_id, $TargetDate, $TargetDate, $dayTypeStr, $dayTypeStr);
      	    if ($isAppliedLeave) {
      	        // update reason and teacher's remark only
      	        $Result['UpdateReasonAndRemark_'.$my_record_id] = $leSchoolBus->updateReasonAndRemark($my_user_id, $TargetDate, $my_record_id, $input_reason, $teacherRemark);
      	    }
      	    else {    // add record
      	        $Result['SyncAbsentRecordToeSchoolBus_'.$my_record_id] = $leSchoolBus->syncAbsentRecordFromAttendance($my_user_id, $TargetDate, $input_reason, $teacherRemark, $dayTypeStr, $my_record_id, $attendanceType=3);
      	    }
  	    }
  	}
  	
  }
  else if ($my_drop_down_status == 0)    # On Time
  {
		# Remove Reason record
		$sql = "DELETE FROM CARD_STUDENT_PROFILE_RECORD_REASON
		       WHERE RecordDate = '$TargetDate'
		             AND StudentID = '$my_user_id'
		             AND DayType = '".$DayType."'
		             AND RecordType = '".PROFILE_TYPE_EARLY."'";
		$lcardattend->db_db_query($sql);
		# Remove Profile record
		$sql = "DELETE FROM PROFILE_STUDENT_ATTENDANCE
		       WHERE AttendanceDate = '$TargetDate'
		             AND UserID = '$my_user_id'
		             AND DayType = '".$DayType."'
		             AND RecordType = '".PROFILE_TYPE_EARLY."'";
		$lcardattend->db_db_query($sql);
		# Set to Daily Record Table
		# Set LeaveStatus and InSchoolTime
		$sql = "UPDATE $card_log_table_name
		      SET 
			      LeaveStatus = NULL,
			      ".$DateModifiedField." = NOW(),
			      ".$ModifyByField." = '".$_SESSION['UserID']."' 
		      WHERE DayNumber = '$txt_day'
		            AND UserID = $my_user_id";
		//echo "sql [".$sql."]<br>";
		$lcardattend->db_db_query($sql);
		
		if ($DayType == PROFILE_DAY_TYPE_AM && $lcardattend->attendance_mode == 3 && $lcardattend->PMStatusNotFollowAMStatus != 1) {
			$Result[$my_user_id.'SetPMStatus'] = $lcardattend->Auto_Set_PM_Status(CARD_STATUS_PRESENT,$my_user_id,$TargetDate);
		}
		
		
		// Early Leave -> Present on Time
		if ($plugin['eSchoolBus']) {
		    $isTakingBusStudent = $leSchoolBus->isTakingBusStudent($my_user_id);
		    if ($isTakingBusStudent) {
                $conditionAry = array();
        		$conditionAry['AttendanceRecordID'] = $my_record_id;
        		$conditionAry['StudentID'] = $my_user_id;
        		$conditionAry['StartDate'] = $TargetDate;
        		$conditionAry['RecordStatus'] = $schoolBusConfig['ApplyLeaveStatus']['Absent'];
        		$Result['ChangeFromAbsentToOnTime_'.$my_record_id] = $leSchoolBus->updateSchoolBusApplyLeaveToDelete($conditionAry);

                unset($conditionAry);
                $conditionAry['t.AttendanceRecordID'] = $my_record_id;
                $conditionAry['t.LeaveDate'] = $TargetDate;
                $conditionAry['t.RecordStatus'] = $schoolBusConfig['ApplyLeaveStatus']['Absent'];
                $Result['ChangeFromAbsentToOthers_Timeslot_'.$my_record_id] = $leSchoolBus->updateSchoolBusTimeSlotApplyLeaveToDelete($my_user_id, $conditionAry);
		    }
		}		
		
  }
  else # Unknown action
  {
       # Do nthg
  }
  // End of Kenneth Chung Code
	
	// handle Teacher's remark
	$remark_value = trim($remark[$i]);
	if(!$use_magic_quotes){
		$remark_value = addslashes($remark_value);
	}
	$lcardattend->updateTeacherRemark($my_user_id,$TargetDate,$DayType,$remark_value);
	
	// handle Office remark
	if($my_drop_down_status != CARD_STATUS_PRESENT){
		$office_remark_value = trim($_REQUEST['office_remark'.$i]);
		if(!$use_magic_quotes){
			$office_remark_value = addslashes($office_remark_value);
		}
		$RealProfileType = $my_drop_down_status;
		$lcardattend->updateOfficeRemark($my_user_id,$TargetDate,$DayType,$RealProfileType,$office_remark_value);
	}
	
	$log_row .= '['.$TargetDate.' '.$DayType.' '.$my_user_id.' 3 '.$my_drop_down_status.']';
	
	/*
        if( $period == "1")        # AM
        {
            if ($my_drop_down_status == 3)       # Early Leave
            {
                # Reason input
                $txtReason = ${"reason$i"};
                $txtReason = intranet_htmlspecialchars($txtReason);
                # Get ProfileRecordID
                $sql = "SELECT RecordID, ProfileRecordID FROM CARD_STUDENT_PROFILE_RECORD_REASON
                               WHERE RecordDate = '$TargetDate'
                                     AND StudentID = '$my_user_id'
                                     AND DayType = '".PROFILE_DAY_TYPE_AM."'
                                     AND RecordType = '".PROFILE_TYPE_EARLY."'";
                $temp = $lcardattend->returnArray($sql,2);
                list($reason_record_id, $reason_profile_id) = $temp[0];

                if ($reason_record_id == "")           # Reason record not exists
                {
                    # Search whether attendance exists by date, student and type
                    $sql = "SELECT StudentAttendanceID FROM PROFILE_STUDENT_ATTENDANCE
                                   WHERE AttendanceDate = '$TargetDate'
                                         AND UserID = '$my_user_id'
                                         AND DayType = '".PROFILE_DAY_TYPE_AM."'
                                         AND RecordType = '".PROFILE_TYPE_EARLY."'";
                    $temp = $lcardattend->returnVector($sql);
                    $attendance_id = $temp[0];
                    if ($attendance_id == "")          # Record not exists
                    {
                            if ($my_record_status != 1)
                            {
                                # Insert profile record
                                $sql = "SELECT ClassName, ClassNumber FROM INTRANET_USER WHERE UserID = '$my_user_id'";
                                $temp = $lcardattend->returnArray($sql,2);
                                list ($user_classname, $user_classnum) = $temp[0];
                                $fieldname = "UserID, AttendanceDate,Year, Semester, RecordType, DayType, DateInput,DateModified,ClassName,ClassNumber,Reason,AcademicYearID,YearTermID";
                                $fieldvalue = "'$my_user_id','$TargetDate','".$lcardattend->Get_Safe_Sql_Query($school_year)."','".$lcardattend->Get_Safe_Sql_Query($semester)."','".PROFILE_TYPE_EARLY."','".PROFILE_DAY_TYPE_AM."',now(),now(),'$user_classname','$user_classnum','$txtReason','$school_year_id','$semester_id'";
                                $sql = "INSERT INTO PROFILE_STUDENT_ATTENDANCE ($fieldname) VALUES ($fieldvalue)";
                                $lcardattend->db_db_query($sql);
                                $attendance_id = $lcardattend->db_insert_id();
                        }
                    }
                    else
                    {
                            if ($my_record_status == 1)
                                                {
                                                        // Delete Record if waived
                                                        $sql = "DELETE FROM PROFILE_STUDENT_ATTENDANCE WHERE StudentAttendanceID = '$attendance_id'";
                                                        $lcardattend->db_db_query($sql);
                                                        $attendance_id = NULL;
                                                }
                                                else
                                                {
                                # Update Reason in profile record By AttendanceID
                                $sql = "UPDATE PROFILE_STUDENT_ATTENDANCE SET Reason = '$txtReason'
                                       WHERE StudentAttendanceID = '$attendance_id'";
                                $lcardattend->db_db_query($sql);
                            }
                    }
                    # Insert to Reason table
                    $fieldname = "RecordDate, StudentID, ProfileRecordID, Reason, RecordType, DayType, RecordStatus, DateInput, DateModified";
                    $fieldsvalues = "'$TargetDate', '$my_user_id', '$attendance_id','$txtReason', '".PROFILE_TYPE_EARLY."', '".PROFILE_DAY_TYPE_AM."', '$my_record_status', now(), now() ";
                    $sql = "INSERT INTO CARD_STUDENT_PROFILE_RECORD_REASON ($fieldname)
                                   VALUES ($fieldsvalues)";
                    $lcardattend->db_db_query($sql);
                }
                else  # Reason record exists
                {
                    if ($reason_profile_id == "")    # Profile ID not exists
                    {
                        # Search whether attendance exists by date, student and type
                        $sql = "SELECT StudentAttendanceID FROM PROFILE_STUDENT_ATTENDANCE
                                       WHERE AttendanceDate = '$TargetDate'
                                             AND UserID = '$my_user_id'
                                             AND DayType = '".PROFILE_DAY_TYPE_AM."'
                                             AND RecordType = '".PROFILE_TYPE_EARLY."'";
                        $temp = $lcardattend->returnVector($sql);
                        $attendance_id = $temp[0];

                        if ($attendance_id == "")          # Record not exists
                        {
                                if ($my_record_status != 1)
                                {
                                    # Insert profile record
                                    $sql = "SELECT ClassName, ClassNumber FROM INTRANET_USER WHERE UserID = '$my_user_id'";
                                    $temp = $lcardattend->returnArray($sql,2);
                                    list ($user_classname, $user_classnum) = $temp[0];
                                    $fieldname = "UserID, AttendanceDate,Year, Semester, RecordType, DayType, DateInput,DateModified,ClassName,ClassNumber,Reason,AcademicYearID,YearTermID";
                                    $fieldvalue = "'$my_user_id','$TargetDate','".$lcardattend->Get_Safe_Sql_Query($school_year)."','".$lcardattend->Get_Safe_Sql_Query($semester)."','".PROFILE_TYPE_EARLY."','".PROFILE_DAY_TYPE_AM."',now(),now(),'$user_classname','$user_classnum','$txtReason','$school_year_id','$semester_id'";
                                    $sql = "INSERT INTO PROFILE_STUDENT_ATTENDANCE ($fieldname) VALUES ($fieldvalue)";
                                    $lcardattend->db_db_query($sql);
                                    $attendance_id = $lcardattend->db_insert_id();
                            }
                        }
                        else
                        {
                                if ($my_record_status == 1)
                                                        {
                                                                // Delete Record if waived
                                                                $sql = "DELETE FROM PROFILE_STUDENT_ATTENDANCE WHERE StudentAttendanceID = '$attendance_id'";
                                                                $lcardattend->db_db_query($sql);
                                                                $attendance_id = "";
                                                        }
                                                        else
                                                        {
                                    # Update Reason in profile record By AttendanceID
                                    $sql = "UPDATE PROFILE_STUDENT_ATTENDANCE SET Reason = '$txtReason'
                                           WHERE StudentAttendanceID = '$attendance_id'";
                                    $lcardattend->db_db_query($sql);
                                }
                        }
                    }
                    else  # Has Profile ID
                    {
                        # Search Attendance By ID
                        $sql = "SELECT StudentAttendanceID FROM PROFILE_STUDENT_ATTENDANCE
                                       WHERE StudentAttendanceID = '$reason_profile_id'";
                        $temp = $lcardattend->returnVector($sql);
                        $attendance_id = $temp[0];
                        if ($attendance_id == "")          # Record not exists
                        {
                            # Search attendance by date, student and type
                            $sql = "SELECT StudentAttendanceID FROM PROFILE_STUDENT_ATTENDANCE
                                           WHERE AttendanceDate = '$TargetDate'
                                                 AND UserID = '$my_user_id'
                                                 AND DayType = '".PROFILE_DAY_TYPE_AM."'
                                                 AND RecordType = '".PROFILE_TYPE_EARLY."'";
                            $temp = $lcardattend->returnVector($sql);
                            $attendance_id = $temp[0];

                            if ($attendance_id == "")          # Record not exists
                            {
                                    if ($my_record_status != 1)
                                    {
                                        # insert reason in profile record
                                        $sql = "SELECT ClassName, ClassNumber FROM INTRANET_USER WHERE UserID = '$my_user_id'";
                                        $temp = $lcardattend->returnArray($sql,2);
                                        list ($user_classname, $user_classnum) = $temp[0];
                                        $fieldname = "UserID, AttendanceDate,Year, Semester, RecordType, DayType, DateInput,DateModified,ClassName,ClassNumber,Reason,AcademicYearID,YearTermID";
                                        $fieldvalue = "'$my_user_id','$TargetDate','".$lcardattend->Get_Safe_Sql_Query($school_year)."','".$lcardattend->Get_Safe_Sql_Query($semester)."','".PROFILE_TYPE_EARLY."','".PROFILE_DAY_TYPE_AM."',now(),now(),'$user_classname','$user_classnum','$txtReason','$school_year_id','$semester_id'";
                                        $sql = "INSERT INTO PROFILE_STUDENT_ATTENDANCE ($fieldname) VALUES ($fieldvalue)";
                                        $lcardattend->db_db_query($sql);
                                        $attendance_id = $lcardattend->db_insert_id();
                                }
                            }
                            else
                            {
                                    if ($my_record_status == 1)
                                    {
                                            // Delete Record if waived
                                                                        $sql = "DELETE FROM PROFILE_STUDENT_ATTENDANCE WHERE StudentAttendanceID = '$attendance_id'";
                                                                        $lcardattend->db_db_query($sql);
                                                                        $attendance_id = NULL;
                                    }
                                    else
                                    {
                                        # Update Reason in profile record By AttendanceID
                                        $sql = "UPDATE PROFILE_STUDENT_ATTENDANCE SET Reason = '$txtReason'
                                                       WHERE StudentAttendanceID = '$attendance_id'";
                                        $lcardattend->db_db_query($sql);
                                }
                            }
                        }
                        else # profile record exists and valid
                        {
                                if ($my_record_status == 1)
                            {
                                    // Delete Record if waived
                                                                $sql = "DELETE FROM PROFILE_STUDENT_ATTENDANCE WHERE StudentAttendanceID = '$attendance_id'";
                                                                $lcardattend->db_db_query($sql);
                                                                $attendance_id = NULL;
                            }
                            else
                            {
                                    # Update reason in profile record
                                    $sql = "UPDATE PROFILE_STUDENT_ATTENDANCE SET Reason = '$txtReason'
                                           WHERE StudentAttendanceID = '$attendance_id'";
                                    $lcardattend->db_db_query($sql);
                                }

                        }
                    }
                    # Update reason table record
                    $sql = "UPDATE CARD_STUDENT_PROFILE_RECORD_REASON SET Reason = '$txtReason',
                                   ProfileRecordID = '$attendance_id',
                                   RecordStatus = '$my_record_status'
                                   WHERE RecordID= '$reason_record_id'";
                    $lcardattend->db_db_query($sql);
                }
            }
            else if ($my_drop_down_status == 0)    # On Time
            {
                 # Remove Reason record
                 $sql = "DELETE FROM CARD_STUDENT_PROFILE_RECORD_REASON
                               WHERE RecordDate = '$TargetDate'
                                     AND StudentID = '$my_user_id'
                                     AND DayType = '".PROFILE_DAY_TYPE_AM."'
                                     AND RecordType = '".PROFILE_TYPE_EARLY."'";
                 $lcardattend->db_db_query($sql);
                 # Remove Profile record
                 $sql = "DELETE FROM PROFILE_STUDENT_ATTENDANCE
                               WHERE AttendanceDate = '$TargetDate'
                                     AND UserID = '$my_user_id'
                                     AND DayType = '".PROFILE_DAY_TYPE_AM."'
                                     AND RecordType = '".PROFILE_TYPE_EARLY."'";
                 $lcardattend->db_db_query($sql);
                 # Set to Daily Record Table
                 # Set LeaveStatus and InSchoolTime
                 $sql = "UPDATE $card_log_table_name
                                SET LeaveStatus = '".CARD_LEAVE_NORMAL."',
                                DateModified = NOW()
                                WHERE DayNumber = '$txt_day'
                                      AND UserID = '$my_user_id'";
//echo "sql [".$sql."]<br>";
                 $lcardattend->db_db_query($sql);
            }
            else # Unknown action
            {
                 # Do nthg
            }
        }
        else if ($period == "2") # PM
        {
            if ($my_drop_down_status == 3)       # Early Leave
            {
                # Reason input
                $txtReason = ${"reason$i"};
                $txtReason = intranet_htmlspecialchars($txtReason);
                # Get ProfileRecordID
                $sql = "SELECT RecordID, ProfileRecordID FROM CARD_STUDENT_PROFILE_RECORD_REASON
                               WHERE RecordDate = '$TargetDate'
                                     AND StudentID = '$my_user_id'
                                     AND DayType = '".PROFILE_DAY_TYPE_PM."'
                                     AND RecordType = '".PROFILE_TYPE_EARLY."'";
                $temp = $lcardattend->returnArray($sql,2);
                list($reason_record_id, $reason_profile_id) = $temp[0];
                if ($reason_record_id == "")           # Reason record not exists
                {
                    # Search whether attendance exists by date, student and type
                    $sql = "SELECT StudentAttendanceID FROM PROFILE_STUDENT_ATTENDANCE
                                   WHERE AttendanceDate = '$TargetDate'
                                         AND UserID = '$my_user_id'
                                         AND DayType = '".PROFILE_DAY_TYPE_PM."'
                                         AND RecordType = '".PROFILE_TYPE_EARLY."'";
                    $temp = $lcardattend->returnVector($sql);
                    $attendance_id = $temp[0];
                    if ($attendance_id == "")          # Record not exists
                    {
                            if ($my_record_status != 1)
                            {
                                # Insert profile record
                                $sql = "SELECT ClassName, ClassNumber FROM INTRANET_USER WHERE UserID = '$my_user_id'";
                                $temp = $lcardattend->returnArray($sql,2);
                                list ($user_classname, $user_classnum) = $temp[0];
                                $fieldname = "UserID, AttendanceDate,Year, Semester, RecordType, DayType, DateInput,DateModified,ClassName,ClassNumber,Reason,AcademicYearID,YearTermID";
                                $fieldvalue = "'$my_user_id','$TargetDate','".$lcardattend->Get_Safe_Sql_Query($school_year)."','".$lcardattend->Get_Safe_Sql_Query($semester)."','".PROFILE_TYPE_EARLY."','".PROFILE_DAY_TYPE_PM."',now(),now(),'$user_classname','$user_classnum','$txtReason','$school_year_id','$semester_id'";
                                $sql = "INSERT INTO PROFILE_STUDENT_ATTENDANCE ($fieldname) VALUES ($fieldvalue)";
                                $lcardattend->db_db_query($sql);
                                $attendance_id = $lcardattend->db_insert_id();
                        }
                    }
                    else
                    {
                            if ($my_record_status == 1)
                                                {
                                                        // Delete Record if waived
                                                        $sql = "DELETE FROM PROFILE_STUDENT_ATTENDANCE WHERE StudentAttendanceID = '$attendance_id'";
                                                        $lcardattend->db_db_query($sql);
                                                        $attendance_id = NULL;
                                                }
                                                else
                                                {
                                # Update Reason in profile record By AttendanceID
                                $sql = "UPDATE PROFILE_STUDENT_ATTENDANCE SET Reason = '$txtReason'
                                       WHERE StudentAttendanceID = '$attendance_id'";
                                $lcardattend->db_db_query($sql);
                            }
                    }
                    # Insert to Reason table
                    $fieldname = "RecordDate, StudentID, ProfileRecordID, Reason, RecordType, DayType, RecordStatus, DateInput, DateModified";
                    $fieldsvalues = "'$TargetDate', '$my_user_id', '$attendance_id','$txtReason', '".PROFILE_TYPE_EARLY."', '".PROFILE_DAY_TYPE_PM."', '$attendance_id', now(), now() ";
                    $sql = "INSERT INTO CARD_STUDENT_PROFILE_RECORD_REASON ($fieldname)
                                   VALUES ($fieldsvalues)";
                    $lcardattend->db_db_query($sql);
                }
                else  # Reason record exists
                {
                    if ($reason_profile_id == "")    # Profile ID not exists
                    {
                        # Search whether attendance exists by date, student and type
                        $sql = "SELECT StudentAttendanceID FROM PROFILE_STUDENT_ATTENDANCE
                                       WHERE AttendanceDate = '$TargetDate'
                                             AND UserID = '$my_user_id'
                                             AND DayType = '".PROFILE_DAY_TYPE_PM."'
                                             AND RecordType = '".PROFILE_TYPE_EARLY."'";
                        $temp = $lcardattend->returnVector($sql);
                        $attendance_id = $temp[0];
                        if ($attendance_id == "")          # Record not exists
                        {
                                if ($my_record_status != 1)
                                {
                                    # Insert profile record
                                    $sql = "SELECT ClassName, ClassNumber FROM INTRANET_USER WHERE UserID = '$my_user_id'";
                                    $temp = $lcardattend->returnArray($sql,2);
                                    list ($user_classname, $user_classnum) = $temp[0];
                                    $fieldname = "UserID, AttendanceDate,Year, Semester, RecordType, DayType, DateInput,DateModified,ClassName,ClassNumber,Reason,AcademicYearID,YearTermID";
                                    $fieldvalue = "'$my_user_id','$TargetDate','".$lcardattend->Get_Safe_Sql_Query($school_year)."','".$lcardattend->Get_Safe_Sql_Query($semester)."','".PROFILE_TYPE_EARLY."','".PROFILE_DAY_TYPE_PM."',now(),now(),'$user_classname','$user_classnum','$txtReason','$school_year_id','$semester_id'";
                                    $sql = "INSERT INTO PROFILE_STUDENT_ATTENDANCE ($fieldname) VALUES ($fieldvalue)";
                                    $lcardattend->db_db_query($sql);
                                    $attendance_id = $lcardattend->db_insert_id();
                            }
                        }
                        else
                        {
                                if ($my_record_status == 1)
                                                        {
                                                                // Delete Record if waived
                                                                $sql = "DELETE FROM PROFILE_STUDENT_ATTENDANCE WHERE StudentAttendanceID = '$attendance_id'";
                                                                $lcardattend->db_db_query($sql);
                                                                $attendance_id = NULL;
                                                        }
                                                        else
                                                        {
                                    # Update Reason in profile record By AttendanceID
                                    $sql = "UPDATE PROFILE_STUDENT_ATTENDANCE SET Reason = '$txtReason'
                                                   WHERE StudentAttendanceID = '$attendance_id'";
                                    $lcardattend->db_db_query($sql);
                            }
                        }
                    }
                    else  # Has Profile ID
                    {
                        # Search Attendance By ID
                        $sql = "SELECT StudentAttendanceID FROM PROFILE_STUDENT_ATTENDANCE
                                       WHERE StudentAttendanceID = '$reason_profile_id'";
                        $temp = $lcardattend->returnVector($sql);
                        $attendance_id = $temp[0];
                        if ($attendance_id == "")          # Record not exists
                        {
                            # Search attendance by date, student and type
                            $sql = "SELECT StudentAttendanceID FROM PROFILE_STUDENT_ATTENDANCE
                                           WHERE AttendanceDate = '$TargetDate'
                                                 AND UserID = '$my_user_id'
                                                 AND DayType = '".PROFILE_DAY_TYPE_PM."'
                                                 AND RecordType = '".PROFILE_TYPE_EARLY."'";
                            $temp = $lcardattend->returnVector($sql);
                            $attendance_id = $temp[0];
                            if ($attendance_id == "")          # Record not exists
                            {
                                    if ($my_record_status != 1)
                                    {
                                        # insert reason in profile record
                                        $sql = "SELECT ClassName, ClassNumber FROM INTRANET_USER WHERE UserID = '$my_user_id'";
                                        $temp = $lcardattend->returnArray($sql,2);
                                        list ($user_classname, $user_classnum) = $temp[0];
                                        $fieldname = "UserID, AttendanceDate,Year, Semester, RecordType, DayType, DateInput,DateModified,ClassName,ClassNumber,Reason,AcademicYearID,YearTermID";
                                        $fieldvalue = "'$my_user_id','$TargetDate','".$lcardattend->Get_Safe_Sql_Query($school_year)."','".$lcardattend->Get_Safe_Sql_Query($semester)."','".PROFILE_TYPE_EARLY."','".PROFILE_DAY_TYPE_PM."',now(),now(),'$user_classname','$user_classnum','$txtReason','$school_year_id','$semester_id'";
                                        $sql = "INSERT INTO PROFILE_STUDENT_ATTENDANCE ($fieldname) VALUES ($fieldvalue)";
                                        $lcardattend->db_db_query($sql);
                                        $attendance_id = $lcardattend->db_insert_id();
                                }
                            }
                            else
                            {
                                    if ($my_record_status == 1)
                                    {
                                            // Delete Record if waived
                                                                        $sql = "DELETE FROM PROFILE_STUDENT_ATTENDANCE WHERE StudentAttendanceID = '$attendance_id'";
                                                                        $lcardattend->db_db_query($sql);
                                                                        $attendance_id = NULL;
                                    }
                                    else
                                    {
                                        # Update Reason in profile record By AttendanceID
                                        $sql = "UPDATE PROFILE_STUDENT_ATTENDANCE SET Reason = '$txtReason'
                                                       WHERE StudentAttendanceID = '$attendance_id'";
                                        $lcardattend->db_db_query($sql);
                                }
                            }
                        }
                        else # profile record exists and valid
                        {
                                if ($my_record_status == 1)
                            {
                                    // Delete Record if waived
                                                                $sql = "DELETE FROM PROFILE_STUDENT_ATTENDANCE WHERE StudentAttendanceID = '$attendance_id'";
                                                                $lcardattend->db_db_query($sql);
                                                                $attendance_id = NULL;
                            }
                            else
                            {
                                    # Update reason in profile record
                                    $sql = "UPDATE PROFILE_STUDENT_ATTENDANCE SET Reason = '$txtReason'
                                                   WHERE StudentAttendanceID = '$attendance_id'";
                                    $lcardattend->db_db_query($sql);
                            }
                        }
                    }
                    # Update reason table record
                    $sql = "UPDATE CARD_STUDENT_PROFILE_RECORD_REASON SET Reason = '$txtReason',
                                   ProfileRecordID = '$attendance_id',
                                   RecordStatus = '$my_record_status'
                                   WHERE RecordID= '$reason_record_id'";
                    $lcardattend->db_db_query($sql);
                }
            }
            else if ($my_drop_down_status == 0)    # On Time
            {
                 # Remove Reason record
                 $sql = "DELETE FROM CARD_STUDENT_PROFILE_RECORD_REASON
                               WHERE RecordDate = '$TargetDate'
                                     AND StudentID = '$my_user_id'
                                     AND DayType = '".PROFILE_DAY_TYPE_PM."'
                                     AND RecordType = '".PROFILE_TYPE_EARLY."'";
                 $lcardattend->db_db_query($sql);
                 # Remove Profile record
                 $sql = "DELETE FROM PROFILE_STUDENT_ATTENDANCE
                               WHERE AttendanceDate = '$TargetDate'
                                     AND UserID = '$my_user_id'
                                     AND DayType = '".PROFILE_DAY_TYPE_PM."'
                                     AND RecordType = '".PROFILE_TYPE_EARLY."'";
                 $lcardattend->db_db_query($sql);
                 # Set to Daily Record Table
                 # Set LeaveStatus
                 $sql = "UPDATE $card_log_table_name
                                SET LeaveStatus = '".CARD_LEAVE_NORMAL."',
                                DateModified = NOW()
                                WHERE DayNumber = '$txt_day'
                                      AND UserID = '$my_user_id'";
//echo "sql [".$sql."]<br>";
                 $lcardattend->db_db_query($sql);
            }
            else # Unknown action
            {
                 # Do nthg
            }

        }
        else # Unknown action
        {
               # Do nthg
        }
  */
} # End of For-Loop

if ($DayType==PROFILE_DAY_TYPE_AM)
{
	$period_type = PROFILE_DAY_TYPE_AM;
}
else if ($DayType==PROFILE_DAY_TYPE_PM)
{
  $period_type = PROFILE_DAY_TYPE_PM;
}
else
{
  $period_type = "";
}
if ($period_type != "")
{
  # Update Confirm Record
  $sql = "UPDATE CARD_STUDENT_DAILY_DATA_CONFIRM
         SET EarlyConfirmed = 1, EarlyConfirmTime = now(), EarlyConfirmUserID='$UserID', DateModified = now()
         WHERE RecordDate = '$TargetDate' AND RecordType = '$period_type'";
  $lcardattend->db_db_query($sql);
  if ($lcardattend->db_affected_rows()!=1)         # Not Exists
  {
    # Not exists
    $sql = "INSERT INTO CARD_STUDENT_DAILY_DATA_CONFIRM (RecordDate, EarlyConfirmed, RecordType, EarlyConfirmTime, EarlyConfirmUserID, DateInput, DateModified)
            VALUES ('$TargetDate',1,'$period_type',now(),'$UserID',now(),now())";
    $lcardattend->db_db_query($sql);
  }
}

$lcardattend->log($log_row);

$Msg = $Lang['StudentAttendance']['EarlyLeaveListConfirmSuccess'];

intranet_closedb();
header("Location: show.php?DayType=".urlencode($DayType)."&TargetDate=".urlencode($TargetDate)."&Msg=".urlencode($Msg));
?>