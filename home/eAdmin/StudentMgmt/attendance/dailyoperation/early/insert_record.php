<?php
// editing by 
/************************************************* Change Log *******************************************************
 * 2017-02-02 (Carlos): Display student names with class number in drop down selection.
 * 2016-06-29 (Carlos): Added Submit Prove Document Status checkbox.
 * 2011-05-17 (Carlos): Modified checking condition of non-leave records as (LeaveStatus IS NULL OR LeaveStatus = 0) 
 ********************************************************************************************************************/
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libwordtemplates.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lcard = new libcardstudentattend2();

$MODULE_OBJ['title'] = $i_StudentAttendance_Early_Leave_Insert_Record;
$linterface = new interface_html("popup.html");
$linterface->LAYOUT_START();

$lbdb = new libdb();

$lc = new libclass();
$select_class = $lc->getSelectClass("name=\"targetClass\" onChange=\"changeClass(this.form)\"", $targetClass);

$ts_record = strtotime($TargetDate);
		if ($ts_record == -1)
		{
		    $TargetDate = date('Y-m-d');
		    $ts_record = strtotime($TargetDate);
		}
		$txt_year = date('Y',$ts_record);
		$txt_month = date('m',$ts_record);
		$txt_day = date('d',$ts_record);
		
		$card_student_daily_log = "CARD_STUDENT_DAILY_LOG_";
		$card_student_daily_log .= $txt_year."_";
		$card_student_daily_log .= $txt_month;
		
		switch ($DayType)
		{
	        case PROFILE_DAY_TYPE_AM: $InSchool = " AND b.AMStatus IN (0,2) ";
	        		  $LeaveStatus = " AND (b.LeaveStatus IS NULL OR b.LeaveStatus = 0)";
	                                                break;
	        case PROFILE_DAY_TYPE_PM: $InSchool = " AND b.PMStatus IN (0,2) ";
	        		  $LeaveStatus = " AND (b.LeaveStatus IS NULL OR b.LeaveStatus = 0)";
	                                                break;
	        default : $InSchool = " AND b.AMStatus IN (0,2) ";
	        		  $LeaveStatus = " AND (b.LeaveStatus IS NULL OR b.LeaveStatus = 0)";
	                                                break;
		}


# Class selection
if ($flag != 2)
{
    $targetClass = "";
}
$lc = new libclass();
$select_class = $lc->getSelectClass("name=\"targetClass\" onChange=\"changeClass(this.form)\"", $targetClass);

if ($flag==2 && $targetClass != "")
{
    # Get Class Num
    $target_class_name = $targetClass;
    $namefield = getNameFieldWithClassNumberByLang("a.");
    //$sql = "SELECT DISTINCT ClassNumber FROM INTRANET_USER WHERE RecordType = 2 AND RecordStatus IN (1) AND ClassName = '$target_class_name' ORDER BY ClassNumber";
    $sql = "SELECT 
					a.ClassNumber,$namefield 
			FROM 
					INTRANET_USER AS a INNER JOIN 
					$card_student_daily_log AS b ON (a.UserID = b.UserID) 
			WHERE 
					b.DayNumber = $txt_day
					$InSchool
					$LeaveStatus
					AND a.RecordType = 2 
					AND a.RecordStatus IN (0,1,2)
					AND a.ClassName = '$target_class_name' 
			ORDER BY
					a.ClassName, a.ClassNumber, a.EnglishName
			";
		//debug_r($sql);
    $classnum = $lc->returnArray($sql);
    $select_classnum = getSelectByArray($classnum, "name=targetNum",$targetNum);
    $select_classnum .= $linterface->GET_BTN($button_add, "button", "addByClassNum()");
}

if ($flag == 1 && $student_login != '')           # Login
{    
	$sql = "SELECT 
					a.UserID
			FROM 
					INTRANET_USER AS a INNER JOIN 
					$card_student_daily_log AS b ON (a.UserID = b.UserID) 
			WHERE 
					b.DayNumber = $txt_day
					$InSchool
					$LeaveStatus
					AND a.UserLogin = '$student_login'
					AND a.RecordType = 2 
					AND a.RecordStatus IN (0,1,2)
			ORDER BY
					a.ClassName, a.ClassNumber, a.EnglishName
			";    				
        
    $temp = $lc->returnVector($sql);
    $target_new_id = $temp[0];
    if ($target_new_id != "")
    {
        $student[] = $target_new_id;
    }
}
else if ($flag == 2 && $target_class_name != '' && $targetNum != '')
{
     $sql = "SELECT 
					a.UserID
			FROM 
					INTRANET_USER AS a INNER JOIN 
					$card_student_daily_log AS b ON (a.UserID = b.UserID) 
			WHERE 
					b.DayNumber = $txt_day
					$InSchool
					$LeaveStatus 
					AND a.ClassName = '$target_class_name' 
					AND a.ClassNumber = '$targetNum'
					AND a.RecordType = 2 
					AND a.RecordStatus IN (0,1,2)
			ORDER BY
					a.ClassName, a.ClassNumber, a.EnglishName
			";
			
     $temp = $lc->returnVector($sql);
     $target_new_id = $temp[0];
    if ($target_new_id != "")
    {
        $student[] = $target_new_id;
    }
}

# Last selection
if (is_array($student) && sizeof($student)>0)
{
    $list = implode(",", $student);
    $namefield = getNameFieldWithClassNumberByLang();
    $sql = "SELECT UserID , $namefield FROM INTRANET_USER WHERE RecordType = 2 AND UserID IN ($list)";
    $array_students = $lc->returnArray($sql,2);
    if(sizeof($student)==1)
   		$allow_item_number=true;
}

?>

<script language="Javascript">
var form_submit = false;
function addByLogin()
{
         obj = document.form1;
         obj.flag.value = 1;
         generalFormSubmitCheck(obj);
}
function removeStudent(){
		checkOptionRemove(document.form1.elements["student[]"]);
 		submitForm();
}
function submitForm(){
         obj = document.form1;
         obj.flag.value =0;
         generalFormSubmitCheck(obj);
}
function changeClass(obj)
{
         obj.flag.value = 2;
         if (obj.targetNum != undefined)
             obj.targetNum.selectedIndex = 0;
         generalFormSubmitCheck(obj);
}
function addByClassNum()
{
         obj = document.form1;
         obj.flag.value = 2;
         generalFormSubmitCheck(obj);
}
function finishSelection()
{
         if(form_submit) {
             return true;
         }
         form_submit = true;
         obj = document.form1;
         obj.action = 'insert_update.php';
         checkOptionAll(document.form1.elements["student[]"]);
         obj.submit();
         return true;
}
function generalFormSubmitCheck(obj)
{
         if(form_submit) {
             return true;
         }
         form_submit = true;
         checkOptionAll(obj.elements["student[]"]);
         obj.submit();
}
function formSubmit(obj)
{
         if (obj.flag.value == 0)
         {
             obj.flag.value = 1;
             generalFormSubmitCheck(obj);
             return true;
         }
         else
         {
             return finishSelection();
         }
}
function checkForm()
{
        obj = document.form1;
        var cnt = 0;
        
        if(obj.elements["student[]"].length != 0)
                cnt++;
        else
        {
                alert('<?=$i_Discipline_System_alert_PleaseSelectStudent?>');
                return false;
        }
        
        if(obj.early_leave_time.value != '')
        {
	        	if(obj.early_leave_time.value.length == 5)
	        	{
        				var time = obj.early_leave_time.value.split(":");
        				if((time[0].length == 2) && (time[1].length == 2))
        				{
	        				if(((time[0]>=00)&&(time[0]<=23)) && ((time[1]>=00)&&(time[1]<=59)))
	        						cnt++
	        				else
	        				{
	        					alert('<?=$i_StudentAttendance_Input_Correct_Time?>');
	        					return false;
        					}
        				}
        				else
        				{
        						alert('<?=$i_StudentAttendance_Input_Correct_Time?>');
        						return false;
    					}
	    		}
	        	else
	        	{
	        			alert('<?=$i_StudentAttendance_Input_Correct_Time?>');
	        			return false;
        		}
		}
		else
				alert('<?=$i_StudentAttendance_Input_Time?>');
        
        if(cnt==2)
        		return formSubmit(obj);
        else
        		return false;
}

</script>
<br />
<form name="form1" action="" method="post" ONSUBMIT="return checkForm()">
<?
	if($student!='')
	{
		$ts_record = strtotime($TargetDate);
		if ($ts_record == -1)
		{
		    $TargetDate = date('Y-m-d');
		    $ts_record = strtotime($TargetDate);
		}
		$txt_year = date('Y',$ts_record);
		$txt_month = date('m',$ts_record);
		$txt_day = date('d',$ts_record);
		
		$card_student_daily_log = "CARD_STUDENT_DAILY_LOG_";
		$card_student_daily_log .= $txt_year."_";
		$card_student_daily_log .= $txt_month;
		
		switch ($DayType)
		{
	        case PROFILE_DAY_TYPE_AM: $InSchool = " AND b.AMStatus IN (0,2) ";
	        		  $LeaveStatus = " AND (b.LeaveStatus IS NULL OR b.LeaveStatus = 0) ";
	                                                break;
	        case PROFILE_DAY_TYPE_PM: $InSchool = " AND b.PMStatus IN (0,2) ";
	        		  $LeaveStatus = " AND (b.LeaveStatus IS NULL OR b.LeaveStatus = 0)";
	                                                break;
	        default : $InSchool = " AND b.AMStatus IN (0,2) ";
	        		  $LeaveStatus = " AND (b.LeaveStatus IS NULL OR b.LeaveStatus = 0) ";
	                                                break;
		}
		
		$student_list = implode(",", $student);
		
		$sql = "
				SELECT 
						".getNameFieldByLang("a.").", b.UserID, b.LeaveSchoolTime, b.LeaveSchoolStation, b.LeaveStatus
				FROM 
						INTRANET_USER AS a LEFT OUTER JOIN
						$card_student_daily_log AS b ON (a.UserID = b.UserID)
				WHERE
						b.DayNumber = $txt_day AND
						AND b.UserID IN ($student_list)
						$InSchool
						$LeaveStatus
				ORDER BY
					a.ClassName, a.ClassNumber, a.EnglishName
				";

		$result = $lbdb->returnArray($sql, 5);
		
		if(sizeof($result) > 0)
		{
			$warning = "<table width=\"90%\" border=\"0\" cellspacing=\"0\" cellpadding=\"5\" align=\"center\">";
			$warning .= "<tr><td class=\"tabletext\">$i_StudentAttendance_Early_Leave_Warning</td></tr>";
			$warning .= "<tr><td class=\"tabletext\">&nbsp;</td></tr>";
			
			for($i=0; $i<sizeof($result); $i++)
			{
				list($name, $student_id, $leave_school_time, $leave_school_station, $leave_status) = $result[$i];
				$warning .= "<tr>";
				$warning .= "<td class=\"tabletext\">$name</td>";
				$warning .= "</tr>";
			}
			$warning .= "</table><br />";
			echo $warning;
		}
	}
?>

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td>
			<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">
				<tr>
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?=$i_general_students_selected?> <span class="tabletextrequire">*</span>
					</td>
					<td width="70%"class="tabletext">
						<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
							<tr>
								<td width="1" class="tabletext">
									<select name="student[]" size="5" multiple>
									<?
									for ($i=0; $i<sizeof($array_students); $i++)
									{
									     list ($id, $name) = $array_students[$i];
									?>
									<option value="<?=$id?>"><?=$name?></option>
									<?
									}
									?>
									</select>
								</td>
								<td valign="bottom">
									&nbsp;<?= $linterface->GET_BTN($button_select, "button", "newWindow('choose/index.php?fieldname=student[]&DayType=$DayType&TargetDate=$TargetDate', 9)") ?><br />
									&nbsp;<?= $linterface->GET_BTN($button_remove, "button", "removeStudent()") ?>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext">&nbsp;</td>
					<td width="70%" class="tablerow2 tabletext">
						<span class="tabletextremark">(<?=$i_general_alternative?>)</span><br />
						<?=$i_UserLogin?><br />
						<input type="text" name="student_login" class="textboxnum" maxlength="100">
						<?= $linterface->GET_BTN($button_add, "button", "addByLogin()") ?><br />
						<?=$i_ClassNameNumber?><br />
						<?=$select_class?><?=$select_classnum?>
					</td>
				</tr>
				<tr>
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?=$i_StudentAttendance_Time_Departure?> <span class="tabletextrequire">*</span>
					</td>
					<td width="70%" class="tabletext">
						<input type="text" name="early_leave_time" class="textboxnum"> 
						<span class="tabletextremark">(HH:mm) (e.g. 07:30, 16:30)</span>
					</td>
				</tr>
				<tr>
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?=$i_StudentAttendance_Field_CardStation?>
					</td>
					<td width="70%" class="tabletext">
						<input type="text" name="leave_station" class="textboxnum">
					</td>
				</tr>
				<tr>
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?= $Lang['StudentAttendance']['ProveDocument'] ?>
					</td>
					<td width="70%"class="tabletext">
						<?=$linterface->Get_Radio_Button("DocumentStatusY", "DocumentStatus", "1", 0, $__Class='', $Lang['General']['Yes'])?>
						<?=$linterface->Get_Radio_Button("DocumentStatusN", "DocumentStatus", "0", 1, $__Class='', $Lang['General']['No'])?>
					</td>
				</tr>
				<tr>
					<td colspan="2" class="tabletextremark">
						<br /><?=$i_general_required_field?>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table width="95%" border="0" cellspacing="0" cellpadding="2" align="center">
				<tr>
					<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
				</tr>
				<tr>
					<td align="center">
					<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "this.form.flag.value=3") ?>
					<?= $linterface->GET_ACTION_BTN($button_close, "button", "self.close()","submit3") ?>
				</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<input type="hidden" name="flag" value="0">
<input type="hidden" name="student_list" value="<?=$student_list?>">
<input type="hidden" name="TargetDate" value="<?=$TargetDate?>">
<input type="hidden" name="DayType" value="<?=$DayType?>">
</form>
<br />
<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>