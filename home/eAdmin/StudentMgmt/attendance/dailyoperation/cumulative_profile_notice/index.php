<?php
//editing by 
/******************* Changes ***********************
 * 2019-07-02 (Ray): add print, export
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libwordtemplates.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();
$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage['CumulativeProfileNotice'] = 1;

$linterface = new interface_html();

# attendance type
$select_attend = "<SELECT id=\"attendance_type\" name=\"attendance_type\">\n";
$select_attend .= "<OPTION value=\"".CARD_STATUS_ABSENT."\">$i_StudentAttendance_Status_Absent</OPTION>";
$select_attend .= "<OPTION value=\"".CARD_STATUS_LATE."\">$i_StudentAttendance_Status_Late</OPTION>";
$select_attend .="</SELECT>\n";

# date range
$current_month=date('n');
$current_year =date('Y');
if($current_month>=9){
	$startDate = date('Y-m-d',mktime(0, 0, 0, 9, 1, $current_year));
	//$endDate   = date('Y-m-d',mktime(0, 0, 0, 8, 31,$current_year+1));
}else{
	$startDate = date('Y-m-d',mktime(0, 0, 0, 9, 1, $current_year-1));
	//$endDate   = date('Y-m-d',mktime(0, 0, 0, 8, 31,$current_year));
}
$endDate=date('Y-m-d');

/*
# waived
$select_waived = "<SELECT name=\"waived\">\n";
$select_waived.="<OPTION value=\"\" SELECTED>$i_status_all</OPTION>\n";
$select_waived.="<OPTION value=\"1\">$i_general_yes</OPTION>\n";
$select_waived.="<OPTION value=\"2\">$i_general_no</OPTION>\n";
$select_waived.="</SELECT>\n";
*/
$TAGS_OBJ[] = array($Lang['StudentAttendance']['CumulativeProfileNotice'], "", 0);

$MODULE_OBJ = $lc->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START(urldecode($_REQUEST['Msg']));
?>
<style type="text/css">
.class_list{width:100px; height=150px;}
</style>
<script type="text/javascript">
<!--
function Check_Count_More_Than(CountMoreThan) {
	var jsShowHideSpeed = "100";
	
	if (CountMoreThan == '' || CountMoreThan < 0)
	{
		$('div#CountMoreThanWarning').html('<?=$Lang['StudentAttendance']['CountMoreThanWarning']?>').show(jsShowHideSpeed);
	}
	else
	{
		var isInt = (CountMoreThan.toString().search(/^-?[0-9]+$/) == 0);
		if(isInt == true)
		{
			$('div#CountMoreThanWarning').html('').hide(jsShowHideSpeed);
		}
		else
		{
			$('div#CountMoreThanWarning').html('<?=$Lang['StudentAttendance']['CountMoreThanWarning']?>').show(jsShowHideSpeed);
		}
	}
}
	      
function Get_Notice_List() {
	if ($('#DPWL-StartDate').html() == "" && $('#DPWL-EndDate').html() == "" && $('#CountMoreThanWarning').html() == "") {
		Block_Element('ResultLayer');
		var PostVar = {
			"StartDate":$('#StartDate').val(),
			"EndDate":$('#EndDate').val(),
			"attendance_type":$('#attendance_type').val(),
			"CountMoreThan":$('#CountMoreThan').val()
		};
		$('div#ResultLayer').load('ajax_get_result.php',PostVar,function (data) {
				if (data == "die") {
					window.location = '/';
				}
				UnBlock_Element('ResultLayer');
			});
	}
}

function Send_Notice() {
	var StudentObj = document.getElementsByName('StudentID[]');
	var StudentSelected = false;
	for (var i=0; i< StudentObj.length; i++) {
		if (StudentObj[i].checked) {
			StudentSelected = true;
			break;
		}
	}
	
	if (StudentSelected) {
		document.getElementById('SendNoticeForm').submit();
	}
	else {
		alert("<?=$Lang['StudentAttendance']['CheckSendNotice']?>");
	}
}

function submitReport(formId, format)
{
    var formObj = $('form#'+formId);
    formObj.attr('action','ajax_get_result.php');
    formObj.attr('target','_blank');
    $('#'+formId+' .report_format').remove();
    formObj.append('<input type="hidden" class="report_format" name="Format" value="'+format+'" />');
    formObj.submit();
}

function PrintPage(formObj)
{
    submitReport(formObj.name, 'print');
}

function ExportPage(formObj)
{
    submitReport(formObj.name, 'csv');
}

       // -->
</script>
<br />
<form id="PrintExportForm" name="PrintExportForm" method="POST" action="">
<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_general_startdate?></td>
		<td width="70%"class="tabletext">
			<?=$linterface->GET_DATE_PICKER("StartDate", $startDate)?>
		</td>
	</tr>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_general_enddate?></td>
		<td width="70%"class="tabletext">
			<?=$linterface->GET_DATE_PICKER("EndDate", $endDate)?>
		</td>
	</tr>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_Attendance_attendance_type?></td>
		<td width="70%"class="tabletext">
			<?=$select_attend?>
		</td>
	</tr>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$Lang['StudentAttendance']['ShowIfCountMoreThan']?></td>
		<td width="70%"class="tabletext">
			<input name="CountMoreThan" id="CountMoreThan" value="0" onkeyup="Check_Count_More_Than(this.value);" maxlength="3" style="width:50px;">
			<div id="CountMoreThanWarning" style="color:red;"></div>
		</td>
	</tr>
</table>
</form>
<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
  	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
  </tr>
</table>
<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
	  <td align="center">
			<?= $linterface->GET_BTN($Lang['Btn']['View'], "button", "Get_Notice_List();") ?>
		</td>
	</tr>
	<tr>
		<td align="center">
			<form id="SendNoticeForm" name="SendNoticeForm" method="POST" action="send_notice_process.php">
			<div id="ResultLayer"></div>
			</form>
		</td>
	</tr>
</table>
<br />
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>