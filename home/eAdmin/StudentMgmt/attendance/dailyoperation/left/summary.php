<?php
//
################# Change Log [Start] #####
#
#	Date	:	2015-12-17	Omas
# 			Moved if($targetClass <> "" && $TargetDate <> "" && $OrderBy <> "") after global.php for php 5.4
#
################## Change Log [End] ######

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
if($targetClass <> "" && $TargetDate <> "" && $OrderBy <> "")
{
	$URL = "?TargetDate=$TargetDate&targetClass=".$targetClass."&OrderBy=$OrderBy";
	header("Location: student_leaving_time.php".$URL);
	exit();
}
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();
$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageDailyOperation_ViewLeftStudents";

$linterface = new interface_html();

#class used
$type += 0;
$summary = $lc->getSummaryCount($type);
$display = "";


# Get class list
$lclass = new libclass();
$select_class = $lclass->getSelectClass("name='targetClass'",$targetClass,1);

switch($type)
{
       case 1:
            $type_string = $i_StudentAttendance_LeftStatus_Type_Lunch;
            list($gone_out, $back, $not_out) = $summary;

            $link1 = ($gone_out != 0? "<a class=\"tablelink\" href=\"javascript:newWindow('list.php?type=$type&subtype=0',12)\">[$i_StudentAttendance_ViewStudentList]</a>":"");
            $link2 = ($back != 0? "<a class=\"tablelink\" href=\"javascript:newWindow('list.php?type=$type&subtype=1',12)\">[$i_StudentAttendance_ViewStudentList]</a>":"");
            $link3 = ($not_out != 0? "<a class=\"tablelink\" href=\"javascript:newWindow('list.php?type=$type&subtype=2',12)\">[$i_StudentAttendance_ViewStudentList]</a>":"");
            
            $display  = "<table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">";
            $display .= "<tr><td width=\"30%\" valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_StudentAttendance_Lunch_GoneOut $link1</td>";
            $display .= "<td width=\"70%\" class=\"tabletext\">$gone_out</td></tr>\n";
            $display .= "<tr><td width=\"30%\" valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_StudentAttendance_Lunch_Back $link2</td>";
            $display .= "<td width=\"70%\" class=\"tabletext\">$back</td></tr>\n";
            $display .= "<tr><td width=\"30%\" valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_StudentAttendance_Lunch_NotOutYet $link3</td>";
            $display .= "<td width=\"70%\" class=\"tabletext\">$not_out</td></tr>\n";
            $display .= "</table>";

            break;
       case 2:
            $type_string = $i_StudentAttendance_LeftStatus_Type_AfterSchool;
            list($left, $stay) = $summary;

            $link1 = ($left != 0? "<a class=\"tablelink\" href=\"javascript:newWindow('list.php?type=$type&subtype=0',12)\">[$i_StudentAttendance_ViewStudentList]</a>":"");
            $link2 = ($stay != 0? "<a class=\"tablelink\" href=\"javascript:newWindow('list.php?type=$type&subtype=1',12)\">[$i_StudentAttendance_ViewStudentList]</a>":"");
            
            $display  = "<table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">";
			$display .= "<tr><td width=\"30%\" valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_StudentAttendance_AfterSchool_Left $link1</td>";
			$display .= "<td width=\"70%\" class=\"tabletext\">$left</td></tr>\n";
            $display .= "<tr><td width=\"30%\" valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_StudentAttendance_AfterSchool_Stay $link2</td>";
            $display .= "<td width=\"70%\" class=\"tabletext\">$stay</td></tr>\n";
            $display .= "</table>";
            break;
       case 3:
       		$type_string = $i_StudentAttendance_LeftStatus_Type_LeavingTime;       		
       		$date_str = date('Y-m-d');
       		
       		$display  = "<table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">";
       		$display .= "<tr><td width=\"30%\" valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_StudentAttendance_Field_Date</td>";
       		$display .= "<td width=\"70%\" class=\"tabletext\">";
       		$display .= $linterface->GET_DATE_FIELD("TargetDate", "form1", "TargetDate", "$date_str");
			$display .= "</td></tr>";
			$display .= "<tr><td width=\"30%\" valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_ClassName</td>";
			$display .= "<td width=\"70%\" class=\"tabletext\">$select_class</td></tr>";
			$display .= "<tr><td width=\"30%\" valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_LinkSortBy</td>";
			$display .= "<td width=\"70%\" class=\"tabletext\"><select name=\"OrderBy\">";
			$display .= "<option value=\"1\">".$i_SmartCard_DailyOperation_OrderBy_ClassNameNumber."</option>";
			$display .= "<option value=\"2\">".$i_SmartCard_DailyOperation_OrderBy_LeaveSchoolTime."</option>";
			$display .= "</select>";
			$display .= "</td></tr>";
			$display .= "</table>";
			
			/*
			if ($OrderBy != '')
				echo $OrderBy;
			*/
				
			$display .= "<table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">";
			$display .= "<tr>";
            $display .= "<td class=\"dotline\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"10\" height=\"1\" /></td>";
            $display .= "</tr>";
			$display .= "<tr><td align=\"center\">";
			$display .= $linterface->GET_ACTION_BTN($button_view, "submit", "")." ";
			$display .= $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"");
			$display .= "</td></tr>";
			$display .= "</table>";
       		break;
       default:
            $type_string = $i_StudentAttendance_LeftStatus_Type_InSchool;
            list($back, $not_yet) = $summary;

            $link1 = ($back != 0? "<a class=\"tablelink\" href=\"javascript:newWindow('list.php?type=$type&subtype=0',12)\">[$i_StudentAttendance_ViewStudentList]</a>":"");
            $link2 = ($not_yet != 0? "<a class=\"tablelink\" href=\"javascript:newWindow('list.php?type=$type&subtype=1',12)\">[$i_StudentAttendance_ViewStudentList]</a>":"");
			
            $display  = "<table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">";
            $display .= "<tr><td width=\"30%\" valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_StudentAttendance_InSchool_HaveBeenToSchool $link1</td>";
            $display .= "<td width=\"70%\" class=\"tabletext\">$back</td></tr>\n";
            $display .= "<tr><td width=\"30%\" valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_StudentAttendance_InSchool_NotBackYet $link2</td>";
            $display .= "<td width=\"70%\" class=\"tabletext\">$not_yet</td></tr>\n";
            $display .= "</table>";
            break;
}

$TAGS_OBJ[] = array($i_StudentAttendance_LeftStatus_Type_InSchool, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/dailyoperation/left/summary.php?type=0", ($type==0)?1:0);
$TAGS_OBJ[] = array($i_StudentAttendance_LeftStatus_Type_Lunch, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/dailyoperation/left/summary.php?type=1", ($type==1)?1:0);
$TAGS_OBJ[] = array($i_StudentAttendance_LeftStatus_Type_AfterSchool, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/dailyoperation/left/summary.php?type=2", ($type==2)?1:0);
$TAGS_OBJ[] = array($i_StudentAttendance_LeftStatus_Type_LeavingTime, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/dailyoperation/left/summary.php?type=3", ($type==3)?1:0);

$MODULE_OBJ = $lc->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

if ($msg == 2) $SysMsg = $linterface->GET_SYS_MSG("update");
?>
<br />
<form name="form1" method="post" action="">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>
			<?=$display?>
		</td>
	</tr>
</table>
</form>
<?
$linterface->LAYOUT_STOP();
if ($type==3)
	print $linterface->FOCUS_ON_LOAD("form1.TargetDate");
intranet_closedb();
?>