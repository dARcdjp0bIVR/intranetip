<?php
// using by
##################################### Change Log ######################################

#######################################################################################
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libstudentattendance_ui.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-LessonAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancelesson'] || get_client_region() != 'zh_TW') {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$linterface = new interface_html();

include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_header.php");

### class used
$lc = new libcardstudentattend2();

if($TargetDate == '') {
	$TargetDate = date('Y-m-d');
}

$TermInfo = getAcademicYearInfoAndTermInfoByDate($TargetDate);

list($AcademicYearID,$AcademicYearName,$YearTermID,$YearTermName) = $TermInfo;

$LastModifyUserNameField = getNameFieldByLang2("u.");

$LessonAttendUI = new libstudentattendance_ui();
$statusMapping = $LessonAttendUI->LessonAttendanceStatusMapping();


$display_period = '';
$cond = '';
if($TimeSlotID != '') {
	$cond .= " AND itra.TimeSlotID='".$lc->Get_Safe_Sql_Query($TimeSlotID)."'";
}

$sql = "SELECT
sgsa.AttendOverviewID,
sgsa.StudentID,
".getNameFieldByLang("a.")." as name,
a.ClassName,
a.ClassNumber,
a.Gender,
itt.TimeSlotName,
stc.ClassTitleB5 as SubjectGroupTitleB5,
stc.ClassTitleEN as SubjectGroupTitleEN,
ass_sub.CH_DES as SubjectNameB5, 
ass_sub.EN_DES as SubjectNameEN,
sgsa.AttendStatus,
sgsa.Reason,
sgsa.Remarks,
sgsa.LastModifiedDate, 
$LastModifyUserNameField as LastModifyUser
FROM SUBJECT_GROUP_STUDENT_ATTENDANCE_$AcademicYearID AS sgsa
LEFT JOIN SUBJECT_GROUP_ATTEND_OVERVIEW_$AcademicYearID AS sgto ON sgsa.AttendOverviewID=sgto.AttendOverviewID 
LEFT JOIN SUBJECT_TERM_CLASS AS stc ON stc.SubjectGroupID=sgto.SubjectGroupID
LEFT JOIN SUBJECT_TERM AS st ON st.SubjectGroupID = stc.SubjectGroupID AND st.YearTermID='$YearTermID'
LEFT JOIN ASSESSMENT_SUBJECT AS ass_sub ON st.SubjectID = ass_sub.RecordID 
LEFT JOIN INTRANET_TIMETABLE_ROOM_ALLOCATION AS itra ON itra.RoomAllocationID=sgto.RoomAllocationID
LEFT join INTRANET_TIMETABLE_TIMESLOT AS itt ON itt.TimeSlotID=itra.TimeSlotID 
LEFT OUTER JOIN INTRANET_USER AS a ON sgsa.StudentID = a.UserID
LEFT JOIN INTRANET_USER AS u ON u.UserID=sgsa.LastModifiedBy
WHERE sgto.LessonDate='".$lc->Get_Safe_Sql_Query($TargetDate)."'
$cond
AND sgsa.AttendStatus='".$statusMapping['Late']['code']."'
ORDER BY sgsa.CreateDate ASC
";


$result = $lc->returnArray($sql);





$table_attend = "";
$col_span = 11;
$x = "<table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\" class=\"eSporttableborder\">";
$x .= "<tr>";
$x .= "<td class=\"eSporttdborder eSportprinttabletitle\">#</td>";
$x .= "	<td class=\"eSporttdborder eSportprinttabletitle\" nowrap>$i_UserName</td>
				<td class=\"eSporttdborder eSportprinttabletitle\" nowrap>$i_ClassName</td>
				<td class=\"eSporttdborder eSportprinttabletitle\" nowrap>$i_UserClassNumber</td>
				<td class=\"eSporttdborder eSportprinttabletitle\" nowrap>".$Lang['StudentAttendance']['Gender']."</td>
				<td class=\"eSporttdborder eSportprinttabletitle\" nowrap>".$Lang['SysMgr']['SubjectClassMapping']['SubjectGroupTitle']."</td>
				<td class=\"eSporttdborder eSportprinttabletitle\" nowrap>".$Lang['eBooking']['Settings']['DefaultSettings']['BookingPeriod']['FieldTitle']['Period']."</td>
				<td class=\"eSporttdborder eSportprinttabletitle\" nowrap>$i_SmartCard_Frontend_Take_Attendance_Status</td>";
$x .=  "<td class=\"eSporttdborder eSportprinttabletitle\">$i_Attendance_Reason</td>
		<td class=\"eSporttdborder eSportprinttabletitle\">".$Lang['StudentAttendance']['iSmartCardRemark']."</td>
		<td class=\"eSporttdborder eSportprinttabletitle\">".$i_StudentGuardian['MenuInfo']."</td>
		</tr>";

$gender_word['M'] = $Lang['General']['Male'];
$gender_word['F'] = $Lang['General']['Female'];

for($i=0; $i<sizeOf($result); $i++)
{
	//list($record_id, $user_id, $name,$class_name,$class_number,$gender, $in_school_time, $in_school_station, $reason,$record_status, $remark, $office_remark) = $result[$i];
	list($attend_overview_id, $user_id, $name, $class_name,$class_number, $gender, $timeslot_name, $subject_group_titleB5, $subject_group_titleEN, $subject_nameB5, $subject_nameEN, $attend_status, $reason, $remarks, $date_modified, $last_modify_user) = $result[$i];

	if($TimeSlotID != '') {
		if($display_period == '') {
			$display_period = '('.$timeslot_name.')';
		}
	}

	$x .= "<tr>";
	$x .= "<td class=\"eSporttdborder eSportprinttext\">".($i+1)."</td>";
	$x .= "<td class=\"eSporttdborder eSportprinttext\" nowrap>$name</td>";
	$x .= "<td class=\"eSporttdborder eSportprinttext\" nowrap>$class_name</td>";
	$x .= "<td class=\"eSporttdborder eSportprinttext\" nowrap>$class_number</td>";
	$x .= "<td class=\"eSporttdborder eSportprinttext\" nowrap>".$gender_word[$gender]."</td>";
	$x .= "<td class=\"eSporttdborder eSportprinttext\" nowrap>".Get_Lang_Selection($subject_nameB5,$subject_nameEN) . ' - ' . Get_Lang_Selection($subject_group_titleB5,$subject_group_titleEN)."</td>";
	$x .= "<td class=\"eSporttdborder eSportprinttext\" nowrap>$timeslot_name</td>";
	$x .= "<td class=\"eSporttdborder eSportprinttext\" nowrap>".$Lang['StudentAttendance']['Late']."</td>";
	$x .= "<td class=\"eSporttdborder eSportprinttext\">$reason</td>";
	$x .= "<td class=\"eSporttdborder eSportprinttext\">$remarks</td>";
	$x .= "<td class=\"eSporttdborder eSportprinttext\">";

	// Get guardian information
	$main_content_field = "IF(EnName IS NOT NULL AND EnName !='' AND ChName IS NOT NULL AND ChName !='', IF(IsMain = 1, CONCAT('* ', EnName,' / ',ChName), CONCAT(EnName,' / ',ChName)), IF(EnName IS NOT NULL AND EnName!='',EnName,ChName))";
	$main_content_field = "IF($main_content_field='' OR $main_content_field IS NULL,'-',$main_content_field)";

	$sql = "SELECT
                  $main_content_field,
                  Relation,
                  Phone,
                  EmPhone,
                  IsMain
								FROM
								  $eclass_db.GUARDIAN_STUDENT
								WHERE
								  UserID = '".$lc->Get_Safe_Sql_Query($user_id)."'
								ORDER BY
								  IsMain DESC, Relation ASC
		                ";
	$temp = $lc->returnArray($sql,4);

	unset($layer_content);
	if (sizeof($temp)==0)
	{
		$layer_content = "--";
	}
	else
	{
		for($j=0; $j<sizeof($temp); $j++)
		{
			list($name, $relation, $phone, $em_phone) = $temp[$j];
			$no = $j+1;
			$layer_content .= "$name ($ec_guardian[$relation]) ($i_StudentGuardian_Phone) $phone ($i_StudentGuardian_EMPhone) $em_phone <br>\n";
		}
	}

	$x .= $layer_content;
	$x .= "</td>";
	$x .= "</tr>";
}
if (sizeof($result)==0)
{
	$x .= "<tr class=\"tablerow2\">";
	$x .= "<td class=\"tabletext\" colspan=\"$col_span\" align=\"center\">$i_StudentAttendance_NoLateStudents</td>";
	$x .= "</tr>";
}
$x .= "</table>\n";
$table_attend = $x;

$i_title = $Lang['LessonAttendance']['DailyLessonLateOverview']." $TargetDate $display_period";
?>
	<table width='100%' align='center' class='print_hide' border=0>
		<tr>
			<td align='right'><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></td>
		</tr>
	</table>
	<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tr>
			<td class="eSportprinttext"><?=$i_title?></td>
		</tr>
	</table>
<?=$table_attend?>
<?
include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_footer.php");
intranet_closedb();
?>