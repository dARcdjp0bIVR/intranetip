<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_opendb();

### class used
$lcardattend = new libcardstudentattend2();

$page_breaker = "<P CLASS='breakhere'>";
$pagefooter = "</table>";
$prevClass = "";

if (!$sys_custom['SmartCardAttendance_LunchBoxOption'])
{
    die("You don't have privilege to access this page.");
}




### Set Date from previous page
$ts_record = strtotime($TargetDate);
if ($ts_record == -1)
{
    $ts_record = strtotime(date('Y-m-d'));
}
$txt_year = date('Y',$ts_record);
$txt_month = date('m',$ts_record);
$txt_day = date('d',$ts_record);


####################################################################
# Data retrival
$lunch_tablename = $lcardattend->createTable_Card_Student_Lunch_Box_Option($txt_year, $txt_month);
$namefield = getNameFieldByLang("a.");
$sql = "SELECT $namefield, a.ClassName, a.ClassNumber FROM
               $lunch_tablename as b LEFT OUTER JOIN INTRANET_USER as a ON b.StudentID = a.UserID
               WHERE b.DayNumber = '$txt_day' AND b.CancelOption = 1
               ";
$result = $lcardattend->returnArray($sql,3);

$x = "";
$x .= "<table width='100%' border='0'>";
$x .= "<tr><td colspan='2'><u>$i_StudentAttendance_Menu_OtherFeatures_AttendanceList_LunchboxCancel</u></td></tr>";
$x .= "<tr><td>$i_StudentAttendance_View_Date</td><td>$TargetDate</td></tr>";
$x .= "<tr><td>$i_StudentAttendance_Menu_OtherFeatures_AttendanceList_LunchboxCancel_Count</td><td>".(sizeof($result)+0)."</td></tr>";



$x .= "<table width=100% border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0>";
$x .= "<tr class='tableTitle'><td>$i_UserStudentName</td><td>$i_ClassName</td><td>$i_ClassNumber</td></tr>\n";

$csv_x = "";
$csv_x .= "\"$i_StudentAttendance_Menu_OtherFeatures_AttendanceList_LunchboxCancel\"\r\n";
$csv_x .= "\"$i_StudentAttendance_View_Date\",\"$TargetDate\"\r\n";
$csv_x .= "\"$i_StudentAttendance_Menu_OtherFeatures_AttendanceList_LunchboxCancel_Count\",\"".(sizeof($result)+0)."\r\n";
$csv_x .= "\"$i_UserStudentName\",\"$i_ClassName\",\"$i_ClassNumber\"\r\n";

for ($i=0; $i<sizeof($result); $i++)
{
     list($t_name, $t_class, $t_classnum) = $result[$i];
     $css = ($i%2?"":"2");
     $x .= "<tr class=tableContent$css><td>$t_name</td><td>$t_class</td><td>$t_classnum</td></tr>\n";
     $csv_x .= "\"$t_name\",\"$t_class\",\"$t_classnum\"\r\n";
}

$x .= "</table\n";


####################################################################
############


$x .= "<br><table width='560' border='0'><tr><td align='center'><a href='javascript:window.print()'><img class=print_hide src='$image_path/admin/button/s_btn_print_$intranet_session_language.gif' border='0'></a>&nbsp;<a href='?TargetDate=$TargetDate&export=1'><img class=print_hide src='$image_path/admin/button/s_btn_export_$intranet_session_language.gif' border='0'></a></td></tr></table>";

if ($export==1)
{
    output2browser($csv_x, "lunch_cancel_list_$TargetDate.csv");
    # $csv_x
}
else
{

$i_title = "$i_StudentAttendance_System ($i_StudentAttendance_Menu_OtherFeatures_AttendanceList_LunchboxCancel)";
include_once($PATH_WRT_ROOT."templates/fileheader.php");
?>
<STYLE TYPE="text/css">
     P.breakhere {page-break-before: always}
</STYLE>

<?

echo $x;
}
intranet_closedb();
?>