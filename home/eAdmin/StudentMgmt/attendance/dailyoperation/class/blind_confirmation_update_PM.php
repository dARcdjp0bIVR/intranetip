<?php
//using: Kenneth Chung
############################################# Change Log #############################################
#
#  Date: 2010-02-02 (By Carlos)
#  Details: Add checking on confirm for all class to skip class which has been confirmed already
#
######################################################################################################
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");

intranet_auth();
intranet_opendb();

$lc = new libcardstudentattend2();
if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

### Set Date from previous page
$ts_record = strtotime($TargetDate);
if ($ts_record == -1 || $ts_record == FALSE)
{
    $ts_record = strtotime(date('Y-m-d'));
    $TargetDate = date('Y-m-d');
}
$txt_year = date('Y',$ts_record);
$txt_month = date('m',$ts_record);
$txt_day = date('j',$ts_record);
$day_of_week = date('w',$ts_record);

# create confirm log table if not exists
$lcardattend = new libcardstudentattend2();
$lcardattend->retrieveSettings();
$lcardattend->createTable_LogAndConfirm($txt_year, $txt_month);
$daily_log_table_name="CARD_STUDENT_DAILY_LOG_".$txt_year."_".$txt_month;
$attendanceMode = $lcardattend->attendance_mode;
$year = getCurrentAcademicYear();
$semester = getCurrentSemester();
$LIDB = new libdb();

$GeneralSetting = new libgeneralsettings();
$SettingList[] = "'DefaultAttendanceStatus'";
$Settings = $GeneralSetting->Get_General_Setting('StudentAttendance',$SettingList);
$student_attend_default_status = ($Settings['DefaultAttendanceStatus'] == "")? CARD_STATUS_ABSENT:$Settings['DefaultAttendanceStatus'];

###period
switch ($DayType)
{
        case PROFILE_DAY_TYPE_AM: break;
        case PROFILE_DAY_TYPE_PM: break;
        default : $DayType = PROFILE_DAY_TYPE_AM;break;
}

### for class confirm
$card_student_daily_class_confirm = "CARD_STUDENT_DAILY_CLASS_CONFIRM_".$txt_year."_".$txt_month;

# select confirmed classes to skip
$sqlClassConfirmed = "SELECT ClassID, RecordID 
											FROM $card_student_daily_class_confirm 
											WHERE DayNumber = '$txt_day' AND DayType = '$DayType' ";
$temp = $lcardattend->returnArray($sqlClassConfirmed);
for($i=0;$i<sizeof($temp);$i++)
{
	$resultClassConfirmed[$temp[$i]['ClassID']] = $temp[$i]['RecordID'];
}

# select the timetable mode for each class
$resultClassMode = $lcardattend->getClassListMode();
$confirmCount=0;

	$off_class_list=array();

	for($i=0;$i<sizeof($resultClassMode);$i++){
		$off=false;
		$done=false;
		$classID = $resultClassMode[$i][0];
		$className = $resultClassMode[$i][1];
		$classMode = $resultClassMode[$i][3];
		$specialClassID= $classMode==0?0:$classID;
		
		if (!$lcardattend->isRequiredToTakeAttendanceByDate($className,$TargetDate) || $resultClassConfirmed[$classID]) {
				$off_class_list[] = $classID;
		}else{
				$confirmClasses[$confirmCount]['ClassID']=$classID;
				$confirmClasses[$confirmCount++]['ClassName']=$className;
		}
	}

	# determining the pm status
	$pm_expected_field = $lcardattend->Get_PM_Expected_Field("d.","f.");
	/*if($lcardattend->attendance_mode==2 || $lcardattend->attendance_mode==3){
		if($sys_custom['SmartCardAttendance_PM_BLANK'])
		{
			$pm_expected_field = $student_attend_default_status;
		}else
		{
	    $pm_expected_field = "CASE 
	    												WHEN d.PMStatus IS NOT NULL THEN d.PMStatus 
	    												WHEN (b.AMStatus IN (".CARD_STATUS_PRESENT.",".CARD_STATUS_LATE.") AND b.LeaveStatus IN ('".CARD_LEAVE_AM."','".CARD_LEAVE_PM."') AND b.PMStatus IS NULL AND b.LeaveSchoolTime IS NOT NULL) THEN '".CARD_STATUS_ABSENT."' 
                              WHEN (d.AMStatus IN (".CARD_STATUS_PRESENT.",".CARD_STATUS_LATE.") AND d.LunchOutTime IS NULL) OR (d.LunchBackTime IS NOT NULL)
                                   THEN '".CARD_STATUS_PRESENT."'
                              WHEN (d.AMStatus IN (".CARD_STATUS_PRESENT.",".CARD_STATUS_LATE.") AND d.LunchOutTime IS NOT NULL) AND (d.LunchBackTime IS NULL)
                                   THEN '".CARD_STATUS_ABSENT."'
                              WHEN (d.AMStatus IS NULL OR d.AMStatus = '".CARD_STATUS_ABSENT."') AND f.OutingID IS NULL THEN '".CARD_STATUS_ABSENT."'
                              WHEN (d.AMStatus IS NULL OR d.AMStatus = '".CARD_STATUS_ABSENT."') AND f.OutingID IS NOT NULL THEN '".CARD_STATUS_OUTING."'
	                         END";			
		}
	}else{
		
		$pm_expected_field = "CASE 
														WHEN d.PMStatus IS NOT NULL THEN d.PMStatus 
														WHEN (b.AMStatus IN (".CARD_STATUS_PRESENT.",".CARD_STATUS_LATE.") AND b.LeaveStatus IN ('".CARD_LEAVE_AM."','".CARD_LEAVE_PM."') AND b.PMStatus IS NULL AND b.LeaveSchoolTime IS NOT NULL) THEN '".CARD_STATUS_ABSENT."' 
                            WHEN (d.AMStatus IN (".CARD_STATUS_PRESENT.",".CARD_STATUS_LATE.") AND d.LunchOutTime IS NULL) OR (d.LunchBackTime IS NOT NULL)
                                 THEN '".CARD_STATUS_PRESENT."'
                            WHEN d.AMStatus IS NULL AND f.OutingID IS NULL
                                 THEN '".CARD_STATUS_ABSENT."'
                            WHEN d.AMStatus IS NULL AND f.OutingID IS NOT NULL
                                 THEN '".CARD_STATUS_OUTING."'
                            ELSE '".CARD_STATUS_ABSENT."'
                          END";
	}*/


 $sqlAllStudent =   "SELECT 
 							c.UserID,
 							e.YearClassID,
 							e.ClassTitleEN,
 							d.RecordID,
							$pm_expected_field 
						FROM 
  						INTRANET_USER AS c 
  						LEFT JOIN 
  						$daily_log_table_name AS d 
  						ON(c.UserID=d.UserID AND d.DayNumber='$txt_day') 
  						INNER JOIN 
  						YEAR_CLASS AS e 
  						on 
  							c.ClassName = e.ClassTitleEN 
  							and e.AcademicYearID = '".GET_CURRENT_ACADEMIC_YEAR_ID()."' ";
	if(sizeof($off_class_list)>0){
  	$sqlAllStudent .=" AND e.YearClassID NOT IN(".implode(",",$off_class_list).")";
  }
	$sqlAllStudent .= "	LEFT JOIN 
							CARD_STUDENT_OUTING AS f 
							ON (c.UserID=f.UserID AND f.RecordDate = '$TargetDate' AND f.DayType = '".PROFILE_DAY_TYPE_PM."') 
							LEFT JOIN 
		          CARD_STUDENT_ENTRY_LEAVE_PERIOD AS g 
		          ON 
		          	c.UserID = g.UserID 
		          	and 
		          	'".$TargetDate."' between g.PeriodStart and CONCAT(g.PeriodEnd,' 23:59:59')
						Where 
							c.RecordType=2 AND c.RecordStatus IN(0,1) AND c.ClassName IS NOT NULL 
							";
if ($lcardattend->EnableEntryLeavePeriod) 
	$sqlAllStudent .= " AND g.RecordID IS NOT NULL ";

//debug_r($sqlAllStudent); die;
	$allStudentList = $lcardattend->returnArray($sqlAllStudent,5);

for($i=0; $i<sizeOf($allStudentList); $i++)
{
	$my_user_id = $allStudentList[$i][0];
	$my_record_id= $allStudentList[$i][3];
	$my_day = $txt_day;
	$my_status = $allStudentList[$i][4];
	
	if( $DayType == PROFILE_DAY_TYPE_PM)            # PM
	{
	  # insert if not exist
	  if($my_record_id=="")
	  {
	    $sql = "INSERT INTO $daily_log_table_name
		            (UserID,DayNumber,PMStatus,PMIsConfirmed,PMConfirmedUserID,DateInput,InputBy,DateModified,ModifyBy) 
		          VALUES
		            ($my_user_id,$my_day,$my_status,'1','".$_SESSION['UserID']."',NOW(),'".$_SESSION['UserID']."',NOW(),'".$_SESSION['UserID']."')
		          ";
	    $LIDB->db_db_query($sql);
	  }
	  else  # update if exist
	  {
			$sql ="UPDATE $daily_log_table_name SET 
							PMStatus = '".$my_status."',
							PMIsConfirmed = '1',
							PMConfirmedUserID = '".$_SESSION['UserID']."',
							ModifyBy = '".$_SESSION['UserID']."',
              DateModified = NOW() 
						WHERE 
							RecordID = '".$my_record_id."'";
			$LIDB->db_db_query($sql);
	  }
	}
}

### for class confirm
$card_student_daily_class_confirm = "CARD_STUDENT_DAILY_CLASS_CONFIRM_".$txt_year."_".$txt_month;

for($i=0;$i<sizeof($confirmClasses);$i++){
		$class_id = $confirmClasses[$i]['ClassID'];
		$class_name = $confirmClasses[$i]['ClassName'];	
		# insert if record not exist
    $sql = "INSERT INTO $card_student_daily_class_confirm
            (
              ClassID,
              ConfirmedUserID,
              DayNumber,
              DayType,
              DateInput,
              DateModified
            ) VALUES
            (
              '$class_id',
              '".$_SESSION['UserID']."',
              '$txt_day',
              '$DayType',
              NOW(),
              NOW()
						)
            ON DUPLICATE KEY UPDATE 
            	ConfirmedUserID='".$_SESSION['UserID']."', 
            	DateModified = NOW()
					";
    $LIDB->db_db_query($sql);
    //echo "<p>$sql</p>";
    $msg = 1;
}

// remove the confirm cache status
$sql = "replace into CARD_STUDENT_CACHED_CONFIRM_STATUS (
					TargetDate,
					DayType,
					Type,
					ConfirmStatus) 
				value (
				 '".$TargetDate."',
				 '".$DayType."',
				 'Class',
				 'F'
				)";
$Result["ClearConfirmCache"] = $lcardattend->db_db_query($sql);

$return_url = "class_status.php?TargetDate=$TargetDate&DayType=$DayType&msg=$msg";

header( "Location: $return_url");

?>