<?php
############################################# Change Log #############################################
#
#  Date: 2010-02-02 (By Carlos)
#  Details: Add checking on confirm for all class to skip class which has been confirmed already
#
######################################################################################################
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");

intranet_auth();
intranet_opendb();

$lc = new libcardstudentattend2();
if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

### Set Date from previous page
$ts_record = strtotime($TargetDate);
if ($ts_record == -1 || $ts_record == FALSE)
{
    $ts_record = strtotime(date('Y-m-d'));
    $TargetDate = date('Y-m-d');
}
$txt_year = date('Y',$ts_record);
$txt_month = date('m',$ts_record);
$txt_day = date('j',$ts_record);
$day_of_week = date('w',$ts_record);

# create confirm log table if not exists
$lcardattend = new libcardstudentattend2();
$lcardattend->retrieveSettings();
$lcardattend->createTable_LogAndConfirm($txt_year, $txt_month);
$daily_log_table_name="CARD_STUDENT_DAILY_LOG_".$txt_year."_".$txt_month;
$attendanceMode = $lcardattend->attendance_mode;
$year = getCurrentAcademicYear();
$semester = getCurrentSemester();
$LIDB = new libdb();

$GeneralSetting = new libgeneralsettings();
$SettingList[] = "'DefaultAttendanceStatus'";
$Settings = $GeneralSetting->Get_General_Setting('StudentAttendance',$SettingList);
$student_attend_default_status = ($Settings['DefaultAttendanceStatus'] == "")? CARD_STATUS_ABSENT:$Settings['DefaultAttendanceStatus'];

###period
switch ($DayType)
{
        case PROFILE_DAY_TYPE_AM: break;
        case PROFILE_DAY_TYPE_PM: break;
        default : $DayType = PROFILE_DAY_TYPE_AM;break;
}

### for class confirm
$card_student_daily_class_confirm = "CARD_STUDENT_DAILY_CLASS_CONFIRM_".$txt_year."_".$txt_month;

# select confirmed classes to skip
	$sqlClassConfirmed = "SELECT ClassID, RecordID FROM $card_student_daily_class_confirm WHERE DayNumber = '$txt_day' AND DayType = '$DayType' ";
	$temp = $lcardattend->returnArray($sqlClassConfirmed);
	for($i=0;$i<sizeof($temp);$i++)
	{
		$resultClassConfirmed[$temp[$i]['ClassID']] = $temp[$i]['RecordID'];
	}	
	
# select the timetable mode for each class
	$resultClassMode = $lcardattend->getClassListMode();
	$confirmCount=0;
	$sqlStudentList = "SELECT c.UserID,e.YearClassID,e.ClassTitleEN,d.RecordID 
										FROM 
											INTRANET_USER AS c 
											INNER JOIN 
											YEAR_CLASS AS e 
											ON c.ClassName=e.ClassTitleEN and e.AcademicYearID = '".GET_CURRENT_ACADEMIC_YEAR_ID()."' 
											LEFT JOIN 
											$daily_log_table_name AS d 
											ON(c.UserID=d.UserID AND d.DayNumber='$txt_day') 
											LEFT JOIN 
						          CARD_STUDENT_ENTRY_LEAVE_PERIOD AS g 
						          ON 
						          	c.UserID = g.UserID 
						          	and 
						          	'".$TargetDate."' between g.PeriodStart and CONCAT(g.PeriodEnd,' 23:59:59')
										WHERE 
											d.AMStatus IS NULL ";
if ($lcardattend->EnableEntryLeavePeriod) 
	$sqlStudentList .= " AND g.RecordID IS NOT NULL ";
											
	$sqlOutingList = "SELECT c.UserID 
										FROM 
											INTRANET_USER AS c 
											INNER JOIN 
											YEAR_CLASS AS e 
											ON c.ClassName=e.ClassTitleEN and e.AcademicYearID = '".GET_CURRENT_ACADEMIC_YEAR_ID()."' 
											INNER JOIN 
											CARD_STUDENT_OUTING as o 
											on o.UserID=c.UserID AND o.RecordDate = '".$TargarDate."' AND o.DayType = '".PROFILE_DAY_TYPE_AM."' 
											LEFT JOIN 
											$daily_log_table_name AS d 
											ON(c.UserID=d.UserID AND d.DayNumber='$txt_day')
										WHERE 
											d.AMStatus IS NULL";
											

	for($i=0;$i<sizeof($resultClassMode);$i++){
		$off=false;
		$done=false;
		$classID = $resultClassMode[$i][0];
		$className = $resultClassMode[$i][1];
		$classMode = $resultClassMode[$i][3];
		$specialClassID= $classMode==0?0:$classID;
		# check if NonSchoolDay for Speical Date
		/*if($resultSpecial[$specialClassID]==1){
				//echo "<p>speical date off=$className</p>";
				$off=true;
				$done=true;
		}else if($resultSpecial[$specialClassID]==2){
				//echo "<p>speical date on=$className</p>";
				$off=false;
				$done=true;
		}
		# check if NonSchoolDay for Cycle Day 
		if(!$done){
					if($classMode == 1){ # Class Cycle Day
							if($resultClassCycle[$classID]==1){
									//echo "<p>class cycle date off=$className</p>";
									$off = true;
									$done= true;
							}else if($resultClassCycle[$classID]==2){
									//echo "<p>class cycle date on=$className</p>";
									$off = false;
									$done= true;
							}
					}else if($classMode !=2){  # School Cycle Day
							if(is_array($resultSchoolCycle) &&$resultSchoolCycle[0]==1){
											//echo "<p>school cycle date off=$className</p>";
											$off = true;
											$done = true;
							} else if($resultSchoolCycle[0]==2){
											//echo "<p>school cycle date on=$className</p>";
											$off = false;
											$done = true;
							}
					} 
		}
		# check if NonSchoolDay for Week Day
		if(!$done){
				if($classMode==1){ # Class Week Day
							if($resultClassWeek[$classID]==1){
									//echo "<p>class week date off=$className</p>";
									$done = true;
									$off = true;
							}else if($resultClassWeek[$classID]==2){
									//echo "<p>class week date on=$className</p>";
									$done = true;
									$off = false;
							}


				}else if($classMode!=2){ # School Week Day
							if(is_array($resultSchoolWeek) && $resultSchoolWeek[0]==1){
											//echo "<p>school week date off=$className</p>";		
											$off = true;
											$done=true;
							} else 	if($resultSchoolWeek[0]==2){
											//echo "<p>school week date on=$className</p>";		
											$off = false;
											$done=true;
							} 

				}
		}
		if($classMode==2 || $off){
				$sqlStudentList.=" AND e.ClassID<>'$classID'";
				$sqlOutingList.=" AND e.ClassID<>'$ClassID'";
		}else{
				$confirmClasses[$confirmCount]['ClassID']=$classID;
				$confirmClasses[$confirmCount++]['ClassName']=$className;
		}*/
		if (!$lcardattend->isRequiredToTakeAttendanceByDate($className,$TargetDate) || $resultClassConfirmed[$classID]) {
				$sqlStudentList.=" AND e.YearClassID<>'$classID'";
				$sqlOutingList.=" AND e.YearClassID<>'$classID'";
		}else{
				$confirmClasses[$confirmCount]['ClassID']=$classID;
				$confirmClasses[$confirmCount++]['ClassName']=$className;
		}
	}
  $sqlStudentList.=" AND c.RecordType=2 AND c.RecordStatus IN(0,1) AND c.ClassName IS NOT NULL";
  $sqlOutingList.=" AND c.RecordType=2 AND c.RecordStatus IN(0,1) AND c.ClassName IS NOT NULL AND o.RecordDate='$TargetDate'";
	
	# select the students who 
		# 1. are not in the daily log table on TargetDate or who's AMStatus is NULL and
		# 2. who is belong to the class	which needs to take attendance on TargetDate
	//debug_r($sqlStudentList); die;
	$absentStudentList = $lcardattend->returnArray($sqlStudentList,4);
//  echo "<p>$sqlStudentList</p>"; die;
//	echo "<p>$sqlOutingList</p>";
	
### for student confirm
$outingStudentList = $lcardattend->returnVector($sqlOutingList,1);
for($i=0; $i<sizeOf($absentStudentList); $i++)
{
        $my_user_id = $absentStudentList[$i][0];
  			$my_record_id= $absentStudentList[$i][3];
        $my_day = $txt_day;

        if(is_array($outingStudentList)&& in_array($my_user_id,$outingStudentList))
		        $my_status = CARD_STATUS_OUTING;
		    else $my_status= $student_attend_default_status;

        if( $DayType == PROFILE_DAY_TYPE_AM)            # AM
        {
                # insert if not exist
                if($my_record_id=="")
                {                    
                  $sql = "INSERT INTO $daily_log_table_name
                          (UserID,DayNumber,AMStatus,IsConfirmed,ConfirmedUserID,DateInput,InputBy,DateModified,ModifyBy) VALUES
                          ($my_user_id,$my_day,$my_status,'1','".$_SESSION['UserID']."',NOW(),'".$_SESSION['UserID']."',NOW(),'".$_SESSION['UserID']."')
                          ";
                  //echo "<p>$sql</p>";
                  $LIDB->db_db_query($sql);
                }
                else  # update if exist
                {
              		$sql ="UPDATE $daily_log_table_name SET 
              						AMStatus = '".$my_status."',
              						IsConfirmed = '1',
              						ConfirmedUserID = '".$_SESSION['UserID']."',
              						ModifyBy = '".$_SESSION['UserID']."',
              						DateModified = NOW() 
              					WHERE RecordID = $my_record_id";
              		$LIDB->db_db_query($sql);
              		//echo "<p>$sql</p>";
                }
        }
}
//die;
### for class confirm
$card_student_daily_class_confirm = "CARD_STUDENT_DAILY_CLASS_CONFIRM_".$txt_year."_".$txt_month;

for($i=0;$i<sizeof($confirmClasses);$i++){
		$class_id = $confirmClasses[$i]['ClassID'];
		$class_name = $confirmClasses[$i]['ClassName'];	
    # insert if record not exist
    $sql = "INSERT INTO $card_student_daily_class_confirm
            (
              ClassID,
              ConfirmedUserID,
              DayNumber,
              DayType,
              DateInput,
              DateModified
            ) VALUES
            (
              '$class_id',
              '".$_SESSION['UserID']."',
              '$txt_day',
              '$DayType',
              NOW(),
              NOW()
						)
            ON DUPLICATE KEY UPDATE 
            	ConfirmedUserID='".$_SESSION['UserID']."', 
            	DateModified = NOW()
					";
    $LIDB->db_db_query($sql);
    //echo "<p>$sql</p>";
    $msg = 1;
}

// remove the confirm cache status
$sql = "replace into CARD_STUDENT_CACHED_CONFIRM_STATUS (
					TargetDate,
					DayType,
					Type,
					ConfirmStatus) 
				value (
				 '".$TargetDate."',
				 '".$DayType."',
				 'Class',
				 'F'
				)";
$Result["ClearConfirmCache"] = $lcardattend->db_db_query($sql);

$return_url = "class_status.php?TargetDate=$TargetDate&DayType=$DayType&msg=$msg";

header( "Location: $return_url");

?>