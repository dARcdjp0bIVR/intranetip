<?php
############################################# Change Log #############################################
#
#  Date: 2010-02-02 (By Carlos)
#  Details: Add checking on confirm for all class to skip class which has been confirmed already
#
######################################################################################################
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$lc = new libcardstudentattend2();
$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageDailyOperation_ViewClassStatus";

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$linterface = new interface_html();

### Set Date from previous page
$ts_record = strtotime($TargetDate);
if ($ts_record == -1 || $ts_record == FALSE)
{
    $ts_record = strtotime(date('Y-m-d'));
    $TargetDate = date('Y-m-d');
}
$txt_year = date('Y',$ts_record);
$txt_month = date('m',$ts_record);
$txt_day = date('j',$ts_record);
$day_of_week = date('w',$ts_record);

# create confirm log table if not exists
$lc = new libcardstudentattend2();
$lc->retrieveSettings();
$lc->createTable_LogAndConfirm($txt_year, $txt_month);
$daily_log_table_name="CARD_STUDENT_DAILY_LOG_".$txt_year."_".$txt_month;
$attendanceMode = $lc->attendance_mode;

###period
switch ($DayType)
{
  case PROFILE_DAY_TYPE_AM:
		$display_period = $i_DayTypeAM;
    $link_page = "AM";
    $expect_field = $lc->Get_AM_Expected_Field("b.","c.","f.");
    break;
  case PROFILE_DAY_TYPE_PM: 
  	$display_period = $i_DayTypePM;
    $link_page = "PM";
    $expect_field = $lc->Get_PM_Expected_Field("b.","c.","f.");
    break;
  default :
  	$display_period = $i_DayTypeAM;
    $DayType = PROFILE_DAY_TYPE_AM;
    $expect_field = $lc->Get_AM_Expected_Field("b.","c.","f.");
    $link_page = "AM";
    break;
}

### for class confirm
$card_student_daily_class_confirm = "CARD_STUDENT_DAILY_CLASS_CONFIRM_".$txt_year."_".$txt_month;

# select confirmed classes to skip
$sqlClassConfirmed = "SELECT ClassID, RecordID FROM $card_student_daily_class_confirm WHERE DayNumber = '$txt_day' AND DayType = '$DayType' ";
$temp = $lc->returnArray($sqlClassConfirmed);
for($i=0;$i<sizeof($temp);$i++)
{
	$resultClassConfirmed[$temp[$i]['ClassID']] = $temp[$i]['RecordID'];
}
	
# select the timetable mode for each class
$resultClassMode = $lc->getClassListMode();
for($i=0;$i<sizeof($resultClassMode);$i++){
	$classID = $resultClassMode[$i][0];
	$className = $resultClassMode[$i][1];
	$classMode = $resultClassMode[$i][3];
	$specialClassID= $classMode==0?0:$classID;
	
	if (!$lc->isRequiredToTakeAttendanceByDate($className,$TargetDate) || $resultClassConfirmed[$classID]) {
			$SkipClass[] = $classID;
	}
}

if ($lc->EnableEntryLeavePeriod) {
	$FilterInActiveStudent .= "
        INNER JOIN 
        CARD_STUDENT_ENTRY_LEAVE_PERIOD as selp 
        on a.UserID = selp.UserID 
        	AND 
        	'".$TargetDate."' between selp.PeriodStart and CONCAT(selp.PeriodEnd,' 23:59:59') 
        ";
}
### build card log table
$card_log_table_name = "CARD_STUDENT_DAILY_LOG_".$txt_year."_".$txt_month;
$sql = "SELECT
        	".$expect_field." as Status, 
        	count(1) as StatusCount
      FROM
        INTRANET_USER AS a 
        ".$FilterInActiveStudent ." 
        INNER JOIN
				YEAR_CLASS AS e
				ON
          a.RecordType=2
          AND a.RecordStatus IN (0,1,2)
          AND a.ClassName != '' 
          and a.ClassName=e.ClassTitleEN
          and e.AcademicYearID = '".GET_CURRENT_ACADEMIC_YEAR_ID()."' ";
if (sizeof($SkipClass) > 0) {          
	$sql .= "
	        and e.YearClassID not in ('".implode("','",$SkipClass)."') ";
}
$sql .= "
        LEFT JOIN 
        ".$card_log_table_name." AS b
        ON a.UserID=b.UserID 
        	AND b.DayNumber = ".$txt_day." 
        LEFT JOIN CARD_STUDENT_OUTING AS c ON (a.UserID=c.UserID AND c.RecordDate = '".$TargetDate."' AND c.DayType = '".$DayType."')
        LEFT JOIN CARD_STUDENT_PRESET_LEAVE AS f ON (a.UserID = f.StudentID AND f.DayPeriod='".$DayType."' AND f.RecordDate='$txt_year-$txt_month-$txt_day') 
      Group By
        ".$expect_field."
      ";
//debug_r($sql);
$Temp = $lc->returnArray($sql);
for ($i=0; $i< sizeof($Temp); $i++) {
	$StatusCount[$Temp[$i]['Status']] = $Temp[$i]['StatusCount'];
}

$TAGS_OBJ[] = array($i_SmartCard_DailyOperation_ViewClassStatus, "", 0);
$MODULE_OBJ = $lc->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

$PAGE_NAVIGATION[] = array($i_SmartCard_DailyOperation_BlindConfirmation);

$PAGE_NAVIGATION1 = $i_StudentPromotion_Summary;
$PAGE_NAVIGATION2 = $i_SmartCard_DailyOperation_BlindConfirmation_Notes1;

if ($msg == 1) $SysMsg = $linterface->GET_SYS_MSG("add");
if ($msg == 2) $SysMsg = $linterface->GET_SYS_MSG("update");
?>
<br />
<form name="form1" method="GET" action="blind_confirmation_update.php">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	</tr>
	<tr>
		<td>
			<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
		    	<tr>
                	<td><?=$linterface->GET_NAVIGATION2($PAGE_NAVIGATION1)?></td>
                </tr>
		    </table>	
		</td>
	</tr>
	<tr>
		<td>
			<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
		    	<tr>
                	<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $i_StaffAttendance_IntermediateRecord_Date ?></td>
					<td class="tabletext" width="70%"><?=$TargetDate?></td>
                </tr>
                <tr>
                	<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $i_Attendance_DayType ?></td>
					<td class="tabletext" width="70%"><?=$display_period?></td>
                </tr>
                <tr>
                	<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $i_SmartCard_DailyOperation_Number_Of_Absent_Student ?></td>
					<td class="tabletext" width="70%"><?=$StatusCount[CARD_STATUS_ABSENT]?></td>
                </tr>
                 <tr>
                	<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $i_SmartCard_DailyOperation_Number_Of_Outing_Student ?></td>
					<td class="tabletext" width="70%"><?=$StatusCount[CARD_STATUS_OUTING]?></td>
                </tr>
                 <tr>
                	<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $Lang['StudentAttendance']['NumberOfStudentInPreset'] ?></td>
					<td class="tabletext" width="70%"><?=$StatusCount[CARD_STATUS_PRESENT]?></td>
                </tr>
                 <tr>
                	<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $Lang['StudentAttendance']['NumberOfStudentInLate'] ?></td>
					<td class="tabletext" width="70%"><?=$StatusCount[CARD_STATUS_LATE]?></td>
                </tr>
		    </table>	
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>
			<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
		    	<tr>
                	<td><?=$linterface->GET_NAVIGATION2($PAGE_NAVIGATION2)?></td>
                </tr>
                <tr>
                	<td class="tabletext"><ul><li><?=$i_SmartCard_DailyOperation_BlindConfirmation_Notes2?></li></ul></td>
                </tr>
		    </table>	
		</td>
	</tr>
	<tr>
		<td>
			<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
		    	<tr>
                	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
		    </table>	
		</td>
	</tr>
	<tr>
		<td>
			<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
		    	<tr>
				    <td align="center" colspan="2">
						<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "") ?>
						<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location = 'class_status.php?DayType=".$DayType."&TargetDate=".$TargetDate."';","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<input type="hidden" name="TargetDate" value="<?=$TargetDate?>">
<input type="hidden" name="DayType" value="<?=$DayType?>">
</form>
<br />
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>