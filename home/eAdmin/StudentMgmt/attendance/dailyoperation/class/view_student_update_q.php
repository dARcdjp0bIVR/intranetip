<?php
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libclass.php");
include_once("../../../../includes/libcardstudentattend2.php");
include_once("../../../../includes/libdiscipline.php");

intranet_opendb();

# class used
$LIDB = new libdb();
$LICLASS = new libclass();
$lcardattend = new libcardstudentattend2();
$ldiscipline = new libdiscipline();


$confirmed_user_id = -1;

### Set Date from previous page
$ts_record = strtotime($TargetDate);
if ($ts_record == -1)
{
    $TargetDate = date('Y-m-d');
    $ts_record = strtotime($TargetDate);
}

$txt_year = date('Y',$ts_record);
$txt_month = date('m',$ts_record);
$txt_day = date('d',$ts_record);
$year = getCurrentAcademicYear();
$semester = getCurrentSemester();

### for student confirm
$card_log_table_name = "CARD_STUDENT_DAILY_LOG_".$txt_year."_".$txt_month;

for($i=0; $i<sizeOf($user_id); $i++)
{
        $my_user_id = $user_id[$i];
        $my_day = $txt_day;
        $my_drop_down_status = $drop_down_status[$i];
        $my_record_id = $record_id[$i];
        $late_type = (isset($late_status)) ? $late_status[$i] : "";        

        if( $period == "1")            # AM
        {
                # insert if not exist
                if($my_record_id=="")
                {
                        $sql = "INSERT INTO $card_log_table_name
                                                        (UserID,DayNumber,AMStatus,DateInput,DateModified) VALUES
                                                        ($my_user_id,$my_day,$my_drop_down_status,NOW(),NOW())
                                                ";
                        $LIDB->db_db_query($sql);
                        # Check Bad actions
                        if ($my_drop_down_status==CARD_STATUS_PRESENT)           # empty -> present
                        {
                            # Forgot to bring card
                            $lcardattend->addBadActionNoCardEntrance($my_user_id,$TargetDate,"");
                        }
                        else if ($my_drop_down_status==CARD_STATUS_LATE)
                        {
                            # Forgot to bring card
                            $lcardattend->addBadActionNoCardEntrance($my_user_id,$TargetDate,"");

                            # Add late to student profile
                            $sql = "SELECT ClassName, ClassNumber FROM INTRANET_USER WHERE UserID = $my_user_id";
                            $temp = $lcardattend->returnArray($sql,2);
                            list($targetClass, $targetClassNumber) = $temp[0];
                            $fieldname = "UserID, AttendanceDate,Year, Semester, RecordType, DayType, DateInput,DateModified,ClassName,ClassNumber";
                            $fieldvalue = "'$my_user_id','$TargetDate','$year','$semester','".PROFILE_TYPE_LATE."','".PROFILE_DAY_TYPE_AM."',now(),now(),'$targetClass','$targetClassNumber'";
                            $sql = "INSERT INTO PROFILE_STUDENT_ATTENDANCE ($fieldname) VALUES ($fieldvalue)";
                            $lcardattend->db_db_query($sql);
                            $insert_id = $lcardattend->db_insert_id();

							# Calculate upgrade items
							if ($plugin['Discipline'])
							{
								$student_id = $my_user_id;
								$ldiscipline->calculateUpgradeLateToDemerit($student_id);
								$ldiscipline->calculateUpgradeLateToDetention($student_id);
							}

                            if (!$insert_id)
                            {
                                 $sql = "SELECT StudentAttendanceID FROM PROFILE_STUDENT_ATTENDANCE
                                                WHERE UserID = $my_user_id AND AttendanceDate = '$TargetDate'
                                                AND RecordType = '".PROFILE_TYPE_LATE."'
                                                AND DayType = '".PROFILE_DAY_TYPE_AM."'";
                                 $temp = $lcardattend->returnVector($sql);
                                 $insert_id = $temp[0];
                            }

                            # Update to reason table
                            $fieldname = "RecordDate, StudentID, ProfileRecordID, RecordType, DayType, LateType, DateInput, DateModified";
                            $fieldsvalues = "'$TargetDate', '$my_user_id', '$insert_id', '".PROFILE_TYPE_LATE."', '".PROFILE_DAY_TYPE_AM."', '$late_type', now(), now() ";
                            $sql = "INSERT INTO CARD_STUDENT_PROFILE_RECORD_REASON ($fieldname)
                                           VALUES ($fieldsvalues)";
                            $lcardattend->db_db_query($sql);
                        }

                }
                else  # update if exist
                {
                     # Grab original status
                     $sql = "SELECT AMStatus, InSchoolTime, RecordID FROM $card_log_table_name WHERE RecordID = $my_record_id";
                     $temp = $LIDB->returnArray($sql,3);                                          
                     list($old_status, $old_inTime, $old_record_id) = $temp[0];                     
                     if ($old_status != $my_drop_down_status)
                     {
                         # Check bad actions
                         if ( ($old_status==CARD_STATUS_PRESENT || $old_status==CARD_STATUS_LATE) && $my_drop_down_status==CARD_STATUS_ABSENT)
                         {
                               # Has card record but not present in classroom
                               $lcardattend->addBadActionFakedCardAM($my_user_id,"",$old_inTime);
                         }
                         # Update Daily table
                         $sql = "UPDATE $card_log_table_name SET AMStatus=$my_drop_down_status, InSchoolTime = NULL WHERE RecordID=$my_record_id";
                         $LIDB->db_db_query($sql);

                         if ($old_status!=CARD_STATUS_LATE && $my_drop_down_status==CARD_STATUS_LATE)
                         {
                             # Add late to student profile
                             $sql = "SELECT ClassName, ClassNumber FROM INTRANET_USER WHERE UserID = $my_user_id";
                             $temp = $lcardattend->returnArray($sql,2);
                             list($targetClass, $targetClassNumber) = $temp[0];
                             $fieldname = "UserID, AttendanceDate,Year, Semester, RecordType, DayType, DateInput,DateModified,ClassName,ClassNumber";
                             $fieldvalue = "'$my_user_id','$TargetDate','$year','$semester','".PROFILE_TYPE_LATE."','".PROFILE_DAY_TYPE_AM."',now(),now(),'$targetClass','$targetClassNumber'";
                             $sql = "INSERT INTO PROFILE_STUDENT_ATTENDANCE ($fieldname) VALUES ($fieldvalue)";
                             $lcardattend->db_db_query($sql);
                             $insert_id = $lcardattend->db_insert_id();

							 # Calculate upgrade items
							if ($plugin['Discipline'])
							{
								 $student_id = $my_user_id;
								$ldiscipline->calculateUpgradeLateToDemerit($student_id);
								$ldiscipline->calculateUpgradeLateToDetention($student_id);
							}

                             if (!$insert_id)
                             {
                                  $sql = "SELECT StudentAttendanceID FROM PROFILE_STUDENT_ATTENDANCE
                                                 WHERE UserID = $my_user_id AND AttendanceDate = '$TargetDate'
                                                 AND RecordType = '".PROFILE_TYPE_LATE."'
                                                 AND DayType = '".PROFILE_DAY_TYPE_AM."'";
                                  $temp = $lcardattend->returnVector($sql);
                                  $insert_id = $temp[0];
                             }
                             # Update to reason table
                             $fieldname = "RecordDate, StudentID, ProfileRecordID, RecordType, DayType, LateType, DateInput, DateModified";
                             $fieldsvalues = "'$TargetDate', '$my_user_id', '$insert_id', '".PROFILE_TYPE_LATE."', '".PROFILE_DAY_TYPE_AM."', '$late_type', now(), now() ";
                             $sql = "INSERT INTO CARD_STUDENT_PROFILE_RECORD_REASON ($fieldname)
                                            VALUES ($fieldsvalues)";
                             $lcardattend->db_db_query($sql);
                         }
                         if ($old_status==CARD_STATUS_LATE && $my_drop_down_status!=CARD_STATUS_LATE)
                         {	                         
                             # Remove Profile Record
                             $sql = "SELECT RecordID, ProfileRecordID FROM CARD_STUDENT_PROFILE_RECORD_REASON
                                            WHERE StudentID = '$my_user_id' AND RecordDate = '$TargetDate'
                                                  AND DayType = '".PROFILE_DAY_TYPE_AM."' AND RecordType = '".PROFILE_TYPE_LATE."'";
                             $temp = $lcardattend->returnArray($sql,2);
                             list($tmp_record_id , $tmp_profile_id) = $temp[0];
                             if ($tmp_record_id != '')
                             {
                                 $sql = "DELETE FROM CARD_STUDENT_PROFILE_RECORD_REASON WHERE RecordID = '$tmp_record_id'";
                                 $lcardattend->db_db_query($sql);
                                 if ($tmp_profile_id != '')
                                 {
									  # Reset upgrade items
									 if ($plugin['Discipline'])
									 {
										 $target_date = $TargetDate;
										 $student_id = $my_user_id;
										 $ldiscipline->resetUpgradeLateToDemerit($student_id,$target_date);
										 $ldiscipline->resetUpgradeLateToDetention($student_id,$target_date);
									 }

                                     $sql = "DELETE FROM PROFILE_STUDENT_ATTENDANCE WHERE StudentAttendanceID = '$tmp_profile_id'";
                                     $lcardattend->db_db_query($sql);

									 # Calculate upgrade items
									 if ($plugin['Discipline'])
									 {
										 $student_id = $my_user_id;
										 $ldiscipline->calculateUpgradeLateToDemerit($student_id);
										 $ldiscipline->calculateUpgradeLateToDetention($student_id);
									 }
                                 }
                             }
                         }
                     }
                }
        }
        else if( $period == "2")
        {
                # insert if not exist
                if($my_record_id=="")
                {
                        $sql = "INSERT INTO $card_log_table_name
                                                        (UserID,DayNumber,PMStatus,DateInput,DateModified) VALUES
                                                        ($my_user_id,$my_day,$my_drop_down_status,NOW(),NOW())
                                                ";
                        $LIDB->db_db_query($sql);
                        # Check Bad actions
                        if ($my_drop_down_status==CARD_STATUS_PRESENT)           # empty -> present
                        {
                            # Forgot to bring card
                            $lcardattend->addBadActionNoCardEntrance($my_user_id,"",$old_inTime);
                        }
                        else if ($my_drop_down_status==CARD_STATUS_LATE)
                        {
                            # Forgot to bring card
                            $lcardattend->addBadActionNoCardEntrance($my_user_id,$TargetDate,"");

                            # Add late to student profile
                            $sql = "SELECT ClassName, ClassNumber FROM INTRANET_USER WHERE UserID = $my_user_id";
                            $temp = $lcardattend->returnArray($sql,2);
                            list($targetClass, $targetClassNumber) = $temp[0];
                            $fieldname = "UserID, AttendanceDate,Year, Semester, RecordType, DayType, DateInput,DateModified,ClassName,ClassNumber";
                            $fieldvalue = "'$my_user_id','$TargetDate','$year','$semester','".PROFILE_TYPE_LATE."','".PROFILE_DAY_TYPE_PM."',now(),now(),'$targetClass','$targetClassNumber'";
                            $sql = "INSERT INTO PROFILE_STUDENT_ATTENDANCE ($fieldname) VALUES ($fieldvalue)";
                            $lcardattend->db_db_query($sql);
                            $insert_id = $lcardattend->db_insert_id();

							# Calculate upgrade items
							if ($plugin['Discipline'])
							{
								 $student_id = $my_user_id;
								$ldiscipline->calculateUpgradeLateToDemerit($student_id);
								$ldiscipline->calculateUpgradeLateToDetention($student_id);
							}

                            if (!$insert_id)
                            {
                                 $sql = "SELECT StudentAttendanceID FROM PROFILE_STUDENT_ATTENDANCE
                                                WHERE UserID = $my_user_id AND AttendanceDate = '$TargetDate'
                                                AND RecordType = '".PROFILE_TYPE_LATE."'
                                                AND DayType = '".PROFILE_DAY_TYPE_PM."'";
                                 $temp = $lcardattend->returnVector($sql);
                                 $insert_id = $temp[0];
                            }

                            # Update to reason table
                            $fieldname = "RecordDate, StudentID, ProfileRecordID, RecordType, DayType, LateType, DateInput, DateModified";
                            $fieldsvalues = "'$TargetDate', '$my_user_id', '$insert_id', '".PROFILE_TYPE_LATE."', '".PROFILE_DAY_TYPE_PM."', '$late_type', now(), now() ";
                            $sql = "INSERT INTO CARD_STUDENT_PROFILE_RECORD_REASON ($fieldname)
                                           VALUES ($fieldsvalues)";
                            $lcardattend->db_db_query($sql);
                        }



                }
                # update if exist
                else
                {
                     # Grab original status
                     $sql = "SELECT PMStatus, InSchoolTime, LunchOutTime, LunchBackTime, RecordID FROM $card_log_table_name WHERE RecordID = $my_record_id";
                     $temp = $LIDB->returnArray($sql,5);
                     list($old_status, $old_inTime,$old_lunchOutTime, $old_lunchBackTime, $old_record_id) = $temp[0];
                     if ($old_status != $my_drop_down_status)
                     {
                         # No need to check Faked Card

                         # Update Daily table
                         $sql = "UPDATE $card_log_table_name SET PMStatus=$my_drop_down_status WHERE RecordID=$my_record_id";
                         $LIDB->db_db_query($sql);
                     }

                         if ($old_status!=CARD_STATUS_LATE && $my_drop_down_status==CARD_STATUS_LATE)
                         {
                             # Add late to student profile
                             $sql = "SELECT ClassName, ClassNumber FROM INTRANET_USER WHERE UserID = $my_user_id";
                             $temp = $lcardattend->returnArray($sql,2);
                             list($targetClass, $targetClassNumber) = $temp[0];
                             $fieldname = "UserID, AttendanceDate,Year, Semester, RecordType, DayType, DateInput,DateModified,ClassName,ClassNumber";
                             $fieldvalue = "'$my_user_id','$TargetDate','$year','$semester','".PROFILE_TYPE_LATE."','".PROFILE_DAY_TYPE_PM."',now(),now(),'$targetClass','$targetClassNumber'";
                             $sql = "INSERT INTO PROFILE_STUDENT_ATTENDANCE ($fieldname) VALUES ($fieldvalue)";
                             $lcardattend->db_db_query($sql);
                             $insert_id = $lcardattend->db_insert_id();

							 # Calculate upgrade items
							if ($plugin['Discipline'])
							{
								 $student_id = $my_user_id;
								$ldiscipline->calculateUpgradeLateToDemerit($student_id);
								$ldiscipline->calculateUpgradeLateToDetention($student_id);
							}

                             if (!$insert_id)
                             {
                                  $sql = "SELECT StudentAttendanceID FROM PROFILE_STUDENT_ATTENDANCE
                                                 WHERE UserID = $my_user_id AND AttendanceDate = '$TargetDate'
                                                 AND RecordType = '".PROFILE_TYPE_LATE."'
                                                 AND DayType = '".PROFILE_DAY_TYPE_PM."'";
                                  $temp = $lcardattend->returnVector($sql);
                                  $insert_id = $temp[0];
                             }
                             # Update to reason table
                             $fieldname = "RecordDate, StudentID, ProfileRecordID, RecordType, DayType, LateType, DateInput, DateModified";
                             $fieldsvalues = "'$TargetDate', '$my_user_id', '$insert_id', '".PROFILE_TYPE_LATE."', '".PROFILE_DAY_TYPE_PM."', '$late_type', now(), now() ";
                             $sql = "INSERT INTO CARD_STUDENT_PROFILE_RECORD_REASON ($fieldname)
                                            VALUES ($fieldsvalues)";
                             $lcardattend->db_db_query($sql);
                         }
                         if ($old_status==CARD_STATUS_LATE && $my_drop_down_status!=CARD_STATUS_LATE)
                         {
                             # Remove Profile Record
                             $sql = "SELECT RecordID, ProfileRecordID FROM CARD_STUDENT_PROFILE_RECORD_REASON
                                            WHERE StudentID = '$my_user_id' AND RecordDate = '$TargetDate'
                                                  AND DayType = '".PROFILE_DAY_TYPE_PM."' AND RecordType = '".PROFILE_TYPE_LATE."'";
                             $temp = $lcardattend->returnArray($sql,2);
                             list($tmp_record_id , $tmp_profile_id) = $temp[0];
                             if ($tmp_record_id != '')
                             {
                                 $sql = "DELETE FROM CARD_STUDENT_PROFILE_RECORD_REASON WHERE RecordID = '$tmp_record_id'";
                                 $lcardattend->db_db_query($sql);
                                 if ($tmp_profile_id != '')
                                 {
									  # Reset upgrade items
									 if ($plugin['Discipline'])
									 {
										 $target_date = $TargetDate;
										 $student_id = $my_user_id;
										 $ldiscipline->resetUpgradeLateToDemerit($student_id,$target_date);
										 $ldiscipline->resetUpgradeLateToDetention($student_id,$target_date);
									 }

                                     $sql = "DELETE FROM PROFILE_STUDENT_ATTENDANCE WHERE StudentAttendanceID = '$tmp_profile_id'";
                                     $lcardattend->db_db_query($sql);

									 # Calculate upgrade items
									 if ($plugin['Discipline'])
									 {
										 $student_id = $my_user_id;
										 $ldiscipline->calculateUpgradeLateToDemerit($student_id);
										 $ldiscipline->calculateUpgradeLateToDetention($student_id);
									 }
                                 }
                             }
                         }


                }
        }
}

### for class confirm
$card_student_daily_class_confirm = "CARD_STUDENT_DAILY_CLASS_CONFIRM_".$txt_year."_".$txt_month;

if( $confirmed_id <> "" )
{
        # update if record exist
        $sql = "UPDATE $card_student_daily_class_confirm SET ConfirmedUserID=$confirmed_user_id, DateModified = NOW() WHERE RecordID = '$confirmed_id'";
        $LIDB->db_db_query($sql);
        $msg = 2;
}
else
{
        # insert if record not exist
        $class_id = $LICLASS->getClassID($class_name);

        switch($period)
        {
                case "1": $DayType = 2; break;
                case "2": $DayType = 3; break;
                default : $DayType = 2; break;
        }

        $sql = "INSERT INTO $card_student_daily_class_confirm
                                        (
                                                ClassID,
                                                ConfirmedUserID,
                                                DayNumber,
                                                DayType,
                                                DateInput,
                                                DateModified
                                        ) VALUES
                                        (
                                                '$class_id',
                                                '$confirmed_user_id',
                                                '$txt_day',
                                                '$DayType',
                                                NOW(),
                                                NOW()
                                        )
                                        ";
        $LIDB->db_db_query($sql);
        $msg = 1;
}

if($period=="1")
        $display_period="AM";
else if($period=="2")
        $display_period="PM";

$return_url = "view_student_{$display_period}_q.php?class_name=$class_name&class_id=$class_id&period=$period&TargetDate=$TargetDate&msg=$msg";

header( "Location: $return_url");
?>