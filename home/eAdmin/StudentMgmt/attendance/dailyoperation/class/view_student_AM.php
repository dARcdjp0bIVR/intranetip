<?php
//editing by kenneth chung
##################################### Change Log ###############################################
#
# 2010-09-09 by Carlos: Change session interval from 1 to 0.5
#
################################################################################################
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libstudentattendance_ui.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();
$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageDailyOperation_ViewClassStatus";

$GeneralSetting = new libgeneralsettings();
$SettingList[] = "'DefaultAttendanceStatus'";
$Settings = $GeneralSetting->Get_General_Setting('StudentAttendance',$SettingList);
$student_attend_default_status = ($Settings['DefaultAttendanceStatus'] == "")? CARD_STATUS_ABSENT:$Settings['DefaultAttendanceStatus'];

$StudentAttendUI = new libstudentattendance_ui();
$linterface = new interface_html();

### class used
$LIDB = new libdb();

### Set Date from previous page
$ts_record = strtotime($TargetDate);
if ($ts_record == -1)
{
    $ts_record = strtotime(date('Y-m-d'));
}
$txt_year = date('Y',$ts_record);
$txt_month = date('m',$ts_record);
$txt_day = date('d',$ts_record);

# create confirm log table if not exists
$lc = new libcardstudentattend2();

if(!$lc->isRequiredToTakeAttendanceByDate($class_name,$TargetDate)){
	header("Location: ../");
	exit();
}

$lc->createTable_LogAndConfirm($txt_year, $txt_month);

/*
### Short-cut for Daily Late / Daily Absent/ Daily Early Leave ###
$arr_page = array(
					array(1,$i_SmartCard_DailyOperation_ViewLateStatus),
					array(2,$i_SmartCard_DailyOperation_ViewAbsenceStatus),
					array(3,$i_SmartCard_DailyOperation_ViewEarlyLeaveStatus)
				);
$pageShortCut_Selection = getSelectByArray($arr_page," name=\"pageShortCut\" onChange=\"changePage(this.value)\" ");*/
$ShortCutTable = "<table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">\n
									".$StudentAttendUI->Get_Class_Status_Shortcut("Class")."
									</table>";

###period
switch ($DayType)
{
  case PROFILE_DAY_TYPE_AM: 
  	$display_period = $i_DayTypeAM;
    break;
  case PROFILE_DAY_TYPE_PM: 
  	$display_period = $i_DayTypePM;
    break;
  default : 
  	$display_period = $i_DayTypeAM;
    $DayType = PROFILE_DAY_TYPE_AM;
    break;
}

### build confirm table exists
$card_student_daily_class_confirm = "CARD_STUDENT_DAILY_CLASS_CONFIRM_".$txt_year."_".$txt_month;
$sql = "SELECT
            a.RecordID,
            IF(a.ConfirmedUserID <> -1, ".getNameFieldWithClassNumberByLang("c.").", CONCAT('$i_general_sysadmin')),
            a.DateModified
        FROM
            $card_student_daily_class_confirm as a
            LEFT OUTER JOIN 
            YEAR_CLASS as b 
            ON (a.ClassID=b.YearClassID)
            LEFT OUTER JOIN 
            INTRANET_USER as c 
            ON (a.ConfirmedUserID=c.UserID || a.ConfirmedUserID=-1)
        WHERE b.ClassTitleEN = \"".$LIDB->Get_Safe_Sql_Query($class_name)."\" AND a.DayNumber = ".$txt_day." AND a.DayType = $DayType";

$result = $LIDB->returnArray($sql,3);
$col_width = 150;
$table_confirm = "";
$x = "<table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">\n";
$x .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_StudentAttendance_Field_Date</td>";
$x .= "<td class=\"tabletext\" width=\"70%\">$TargetDate</td></tr>\n";
$x .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_ClassName</td>";
$x .= "<td class=\"tabletext\" width=\"70%\">$class_name</td></tr>\n";
$x .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_StudentAttendance_Slot</td>";
$x .= "<td class=\"tabletext\" width=\"70%\">$display_period</td></tr>\n";

list($confirmed_id, $confirmed_user_name, $last_confirmed_date) = $result[0];
if($confirmed_id=="")
{
	$x .= "<tr><td>&nbsp;</td><td class=\"tabletext\" width=\"70%\">$i_SmartCard_Frontend_Take_Attendance_No_Confirm_Record</td></tr>";
}
else
{
	$x .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_StudentAttendance_Field_LastConfirmedTime</td>";
	$x .= "<td class=\"tabletext\" width=\"70%\">$last_confirmed_date ($confirmed_user_name)</td></tr>";
}
$x .= "</table>\n";
$table_confirm = $x;

### build card log table
$card_log_table_name = "CARD_STUDENT_DAILY_LOG_".$txt_year."_".$txt_month;

if ($lc->EnableEntryLeavePeriod) {
	$FilterInActiveStudent .= "
        INNER JOIN 
        CARD_STUDENT_ENTRY_LEAVE_PERIOD as selp 
        on a.UserID = selp.UserID 
        	AND 
        	'".$TargetDate."' between selp.PeriodStart and CONCAT(selp.PeriodEnd,' 23:59:59') 
        ";
}

$am_expect_field = $lc->Get_AM_Expected_Field("b.","c.");
$sql  = "SELECT        
					b.RecordID,
          a.UserID,
          ".getNameFieldByLang("a.")." as name,
          a.ClassNumber,
          IF(b.InSchoolTime IS NULL, '-', b.InSchoolTime),
          IF(b.InSchoolStation IS NULL, '-', b.InSchoolStation),
          ".$am_expect_field.", 
          IF(d.Remark IS NULL, '-',d.Remark),
          IF(f.RecordID IS NOT NULL,1,0),
          f.Reason,
					f.Remark, 
					IF(c.OutingID IS NOT NULL,1,0),
					c.RecordDate,
					c.OutTime,
					c.BackTime,
					c.Location,
					c.Objective,
					c.Detail,
					IFNULL(b.DateModified,'--'),
					IFNULL(".getNameFieldByLang("u.").",'--') as ModifyName, 
					b.IsConfirmed as IsConfirmed,
					IFNULL(".getNameFieldByLang("u1.").",'--') as ConfirmUserName, 
					ssc.LateSession,
					ssc.RequestLeaveSession,
					ssc.PlayTruantSession,
					ssc.OfficalLeaveSession 
        FROM
          INTRANET_USER AS a 
          ".$FilterInActiveStudent ." 
          LEFT OUTER JOIN $card_log_table_name AS b
          ON (a.UserID=b.UserID AND
             (b.DayNumber = ".$txt_day." ||  b.DayNumber IS NULL)
						 )
					LEFT JOIN INTRANET_USER as u on b.ModifyBy = u.UserID 
					LEFT JOIN INTRANET_USER as u1 on b.ConfirmedUserID = u1.UserID 
          LEFT OUTER JOIN CARD_STUDENT_OUTING AS c ON (a.UserID=c.UserID AND c.RecordDate = '".$TargetDate."' AND c.DayType = '".PROFILE_DAY_TYPE_AM."')
          LEFT OUTER JOIN CARD_STUDENT_DAILY_REMARK AS d ON (a.UserID = d.StudentID AND d.RecordDate='$txt_year-$txt_month-$txt_day')
          LEFT OUTER JOIN CARD_STUDENT_PRESET_LEAVE AS f ON (a.UserID = f.StudentID AND f.DayPeriod='".PROFILE_DAY_TYPE_AM."' AND f.RecordDate='$txt_year-$txt_month-$txt_day') 
          LEFT JOIN CARD_STUDENT_STATUS_SESSION_COUNT as ssc 
						ON ssc.StudentID = a.UserID 
							AND ssc.RecordDate = '".$TargetDate."' 
							AND ssc.DayType = '".$DayType."' 
        WHERE
          (b.DayNumber = ".$txt_day." ||  b.DayNumber IS NULL) AND
          a.RecordType=2 AND
          a.RecordStatus IN (0,1,2) AND
          a.ClassName = \"$class_name\" 
        ORDER BY a.ClassNumber ASC
        ";
$result = $LIDB->returnArray($sql,26);

$table_attend = "";

if ($sys_custom['SmartCardAttendance_LunchBoxOption'])
{
	$LunchBoxOption_header ="<td class=td_center_middle_h1 width=1%><img alt=\"$i_SmartCard_Frontend_Take_Attendance_CancelLunch\" src=\"$image_path/icon_cancel_lunchbox.gif\"><br><input type=checkbox name=\"allCancelLunch\" onClick=checkAllLunchBox(this.form)></td>";
}

$x = "<table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">";
$x .= "<tr class=\"tabletop\">";
$x .= "<td class=\"tabletoplink\">$i_UserClassNumber</td>";
$x .= "<td class=\"tabletoplink\">$i_UserName</td>";
$x .= "<td class=\"tabletoplink\">$i_SmartCard_Frontend_Take_Attendance_In_School_Time</td>";
$x .= "<td class=\"tabletoplink\">$i_SmartCard_Frontend_Take_Attendance_In_School_Station</td>";
$x .= "<td class=\"tabletoplink\">$i_SmartCard_Frontend_Take_Attendance_PreStatus</td>";
$x .= "<td class=\"tabletoplink\">&nbsp;</td>";
$x .= "<td class=\"tabletoplink\">$i_SmartCard_Frontend_Take_Attendance_Status</td>";
if($sys_custom['SmartCardAttendance_StudentAbsentSession'])
{
	$x .= "<td class=\"tabletoplink\">".$Lang['StudentAttendance']['Late']."</td>";
	$x .= "<td class=\"tabletoplink\">".$Lang['StudentAttendance']['RequestLeave']."</td>";
	$x .= "<td class=\"tabletoplink\">".$Lang['StudentAttendance']['PlayTruant']."</td>";
	$x .= "<td class=\"tabletoplink\">".$Lang['StudentAttendance']['OfficalLeave']."</td>";
}
$x .= "<td class=\"tabletoplink\">$i_Attendance_Remark</td>
			<td class=\"tabletoplink\">".$Lang['General']['LastModified']."</td>
			<td class=\"tabletoplink\">".$Lang['General']['LastModifiedBy']."</td>
			<td class=\"tabletoplink\">".$Lang['StudentAttendance']['LastConfirmPerson']."</td>
			 $LunchBoxOption_header
			 </tr>\n";

# Lunch cancellation option
if ($sys_custom['SmartCardAttendance_LunchBoxOption'])
{
	$lunch_tablename = $lc->createTable_Card_Student_Lunch_Box_Option($txt_year, $txt_month);
	$sql = "SELECT 
						b.StudentID, 
						b.CancelOption 
					FROM
	         	INTRANET_USER as a 
	         	LEFT OUTER JOIN 
	         	$lunch_tablename as b 
	         	ON b.DayNumber = ".$txt_day." AND a.UserID = b.StudentID
	        WHERE
	         	b.DayNumber = ".$txt_day." AND
	          a.RecordType=2 AND
	          a.RecordStatus IN (0,1,2) AND
	          a.ClassName = \"$class_name\"
	         ";
	$temp = $lc->returnArray($sql,2);
	$lunch_option_result = build_assoc_array($temp);
}

for($i=0; $i<sizeOf($result); $i++)
{
	list($record_id, $user_id, $name,$class_number, $in_school_time, $in_school_station, $attend_status,$remark,$preset_record,$preset_reason,$preset_remark,$outing_record, $outing_date, $outing_time, $back_time, $outing_location, $outing_obj, $outing_detail,$DateModified,$ModifyName,$IsConfirmed,$ConfirmedUser,$LateSession,$RequestLeaveSession,$PlayTruantSession,$OfficalLeaveSession) = $result[$i];
	$attend_status += 0;
	$select_status = "<select name=\"drop_down_status[]\" onchange=\"ChangeDefaultSessionSelect(".$i.");\">\n";
	$select_status .= "<option value='".CARD_STATUS_PRESENT."' ".($attend_status==CARD_STATUS_PRESENT? "SELECTED":"").">".$Lang['StudentAttendance']['Present']."</option>\n";
  $select_status .= "<option value='".CARD_STATUS_ABSENT."' ".($attend_status==CARD_STATUS_ABSENT? "SELECTED":"").">".$Lang['StudentAttendance']['Absent']."</option>\n";
  $select_status .= "<option value='".CARD_STATUS_LATE."' ".($attend_status==CARD_STATUS_LATE? "SELECTED":"").">".$Lang['StudentAttendance']['Late']."</option>\n";
  $select_status .= "<option value='".CARD_STATUS_OUTING."' ".($attend_status==CARD_STATUS_OUTING? "SELECTED":"").">".$Lang['StudentAttendance']['Outing']."</option>\n";
	$select_status .= "</select>\n";
	
	$select_status .= "<input name=\"record_id[]\" type=\"hidden\" value=\"$record_id\">\n";
	$select_status .= "<input name=\"user_id[]\" type=\"hidden\" value=\"$user_id\">\n";
	$select_status .= "<input name=\"Confirmed[".$i."]\" id=\"Confirmed[".$i."]\" type=hidden value=\"".$IsConfirmed."\">\n";
	
	switch ($attend_status){
	  case CARD_STATUS_ABSENT: $note = $i_StudentAttendance_Status_PreAbsent;
	    $css = "attendance_norecord";
	    break;
	  case CARD_STATUS_LATE: $note = $i_StudentAttendance_Status_Late;
	    $css = "attendance_late";
	    break;
	  case CARD_STATUS_OUTING: $note = $i_StudentAttendance_Status_Outing;
	    $css = "attendance_outing";
	    break;
	  default: $note = "&nbsp;";
			//$css = "tableContent";
			//$css = ($i%2==0)?"tablerow1":"tablerow2";
			$css = "row_approved";
			break;
	}
	// use below style if record not confirmed
	if (!$IsConfirmed) {
		$css = "row_drafted";
	}
	# Preset Absence
	$preset_abs_info ="&nbsp;";
	if($preset_record==1) {
		$preset_table = "<table width=\'200\' border=\'0\' cellspacing=\'0\' cellpadding=\'1\'><tr>";
		$preset_table .= "<tr class=\'tablebluetop\'>";
		$preset_table .= "<td class=\'tabletoplink\'>$i_SmartCard_DailyOperation_Preset_Absence</td></tr>";
		$preset_table .= "<tr class=\'tablebluerow1\'><td class=\'tabletext\' valign=\'top\'>$i_UserName: $name ($class_number)</td></tr>";
		$preset_table .= "<tr class=\'tablebluerow2\'><td class=\'tabletext\' valign=\'top\'>$i_Attendance_Date: ".date('Y-m-d')."</td></tr>";
		$preset_table .= "<tr class=\'tablebluerow1\'><td class=\'tabletext\' valign=\'top\'>$i_Attendance_DayType: $i_DayTypeAM</td></tr>";
		$preset_table .= "<tr class=\'tablebluerow2\'><td class=\'tabletext\' valign=\'top\'>$i_Attendance_Reason: ".str_replace(array("'","\r\n", "\n", "\r"),array("\'","<br>", "<br>", "<br>"),$preset_reason)."</td></tr>";
		$preset_table .= "<tr class=\'tablebluerow1\'><td class=\'tabletext\' valign=\'top\'>$i_Attendance_Remark: ".str_replace(array("'","\r\n", "\n", "\r"),array("\'","<br>", "<br>", "<br>"),$preset_remark)."</td></tr>";
		$preset_table .= "</table>";
		$preset_abs_info = "<img onMouseOut=\"hidePresetAbs()\" onMouseOver=\"showPresetAbs(this,'$preset_table')\" src=\"{$image_path}/{$LAYOUT_SKIN}/preset_abs.gif\" align=\"absmiddle\">";
	} 
 	
 	# Preset Outing
 	if ($outing_record==1) {
		$preset_table = "<table border=0 cellspacing=0 cellpadding=1 class=attendancestudentphoto><tr><td>";
		$preset_table .= "<table border=0 cellspacing=0 cellpadding=3>";
		$preset_table .= "<tr><Td class=\'tablebluetop tabletext\' nowrap><B>[$i_StudentAttendance_Outing]</b></td></tr>";
		$preset_table .= "<tr><td class=\'tablebluerow1 tabletext\' nowrap valign=top><font size=2>$i_UserName: $name ($class_number)</font></td></tr>";
		$preset_table .= "<tr><td class=\'tablebluerow2 tabletext\' nowrap valign=top><font size=2>$i_SmartCard_StudentOutingDate: $outing_date</font></td></tr>";
		$preset_table .= "<tr><td class=\'tablebluerow1 tabletext\' nowrap valign=top><font size=2>$i_SmartCard_StudentOutingOutTime: $outing_time</font></td></tr>";
		$preset_table .= "<tr><td class=\'tablebluerow2 tabletext\' nowrap valign=top><font size=2>$i_SmartCard_StudentOutingBackTime: $back_time</font></td></tr>";
		$preset_table .= "<tr><td class=\'tablebluerow1 tabletext\' nowrap valign=top><font size=2>$i_SmartCard_StudentOutingLocation: $outing_location</font></td></tr>";
		$preset_table .= "<tr><td class=\'tablebluerow2 tabletext\' nowrap valign=top><font size=2>$i_SmartCard_StudentOutingReason: $outing_obj</font></td></tr>";
		$preset_table .= "<tr><td class=\'tablebluerow1 tabletext\' nowrap valign=top><font size=2>$i_Attendance_Remark: ".str_replace(array("\n","\r"),array('<br>',''),$outing_detail)."</font></td></tr>";
		$preset_table .="</table></td></tr></table>";
		$preset_abs_info = "<img onMouseOut='hidePresetAbs();' onMouseOver=\"showPresetAbs(this,'$preset_table')\" src=\"{$image_path}/{$LAYOUT_SKIN}/preset_abs.gif\" align=\"absmiddle\">";
 	}
 	
  # Lunchbox option
	if ($sys_custom['SmartCardAttendance_LunchBoxOption'])
	{
		$lunch_checked = ($lunch_option_result[$user_id]==1)?" CHECKED":"";
		$lunchbox_option = "<td><input type=checkbox name=\"LunchBoxOption_".$user_id."\" value=1 $lunch_checked></td>";
		$js_all_option .= "obj.LunchBoxOption_$user_id.checked = status;\n";
	}
	else
	{
		$lunchbox_option = "";
	}
	
	//$css = ($i%2==0)?"tablerow1":"tablerow2";
	$x .= "<tr class=\"$css\">";
	$x .= "<td>".$class_number."</td>";
	$x .= "<td>".$name."</td>";
	$x .= "<td>".$in_school_time."</td>";
	$x .= "<td>".$in_school_station."</td>";
	$x .= "<td>".$note."</td>";
	$x .= "<td>".$preset_abs_info."</td>";
	$x .= "<td>".$select_status."</td>";
	if($sys_custom['SmartCardAttendance_StudentAbsentSession'])
  {
  	if ($LateSession == "" && $RequestLeaveSession == "" && $PlayTruantSession == "" && $OfficalLeaveSession == "") {
  		if ($attend_status == CARD_STATUS_ABSENT) {
  			$LateSession = $PlayTruantSession = $OfficalLeaveSession = 0;
  			$RequestLeaveSession = 1;
  		}
  		else if ($attend_status==CARD_STATUS_LATE) {
  			$LateSession = 1;
  			$RequestLeaveSession = $PlayTruantSession = $OfficalLeaveSession = 0;
  		}
  		else {
  			$LateSession = $RequestLeaveSession = $PlayTruantSession = $OfficalLeaveSession = 0;
  		}
  	}
  	
  	$selOfficalLeaveSession = "<select name=\"offical_leave_session[]\" id=\"offical_leave_session$i\"/>";
		$selLateSession = "<select name=\"late_session[]\" id=\"late_session$i\"/>";
		$selRequestLeaveSession = "<select name=\"request_leave_session[]\" id=\"request_leave_session$i\"/>";
		$selPlayTruantSession = "<select name=\"play_truant_session[]\" id=\"play_truant_session$i\"/>";
		/*for($k=0;$k<=20;$k++)
		{
			$LateSelected = ($k == $LateSession)? 'selected':'';
			$OfficalLeaveSelected = ($k == $OfficalLeaveSession)? 'selected':'';
			$RequestLeaveSelected = ($k == $RequestLeaveSession)? 'selected':'';
			$PlayTruantSelected = ($k == $PlayTruantSession)? 'selected':'';
			
			$selOfficalLeaveSession .= "<option value=\"$k\" ".$OfficalLeaveSelected." >".$k."</option>";
			$selLateSession .= "<option value=\"".$k."\" ".$LateSelected." >".$k."</option>";
			$selRequestLeaveSession .= "<option value=\"".$k."\" ".$RequestLeaveSelected." >".$k."</option>";
			$selPlayTruantSession .= "<option value=\"".$k."\" ".$PlayTruantSelected." >".$k."</option>";
		}*/
		$k=0.0;
		while($k<=CARD_STUDENT_MAX_SESSION){
			$LateSelected = ($k == $LateSession)? 'selected':'';
			$OfficalLeaveSelected = ($k == $OfficalLeaveSession)? 'selected':'';
			$RequestLeaveSelected = ($k == $RequestLeaveSession)? 'selected':'';
			$PlayTruantSelected = ($k == $PlayTruantSession)? 'selected':'';
			
			$selOfficalLeaveSession .= "<option value=\"$k\" ".$OfficalLeaveSelected." >".$k."</option>";
			$selLateSession .= "<option value=\"".$k."\" ".$LateSelected." >".$k."</option>";
			$selRequestLeaveSession .= "<option value=\"".$k."\" ".$RequestLeaveSelected." >".$k."</option>";
			$selPlayTruantSession .= "<option value=\"".$k."\" ".$PlayTruantSelected." >".$k."</option>";
			$k+=0.5;
		}
		$selOfficalLeaveSession .= "</select>\n";
		$selLateSession .= "</select>\n";
		$selRequestLeaveSession .= "</select>\n";
		$selPlayTruantSession .= "</select>\n";
		
		$x .= "<td>".$selLateSession."</td>";
		$x .= "<td>".$selRequestLeaveSession."</td>";
		$x .= "<td>".$selPlayTruantSession."</td>";
		$x .= "<td>".$selOfficalLeaveSession."</td>";
  }
	$x .= "<td>".$remark."</td>
				<td>".$DateModified."</td>
				<td>".$ModifyName."</td>
				<td>".$ConfirmedUser."</td>
				$lunchbox_option
				</tr>";
}
$x .= "</table>\n";
$table_attend = $x;

$toolbar = $linterface->GET_LNK_PRINT("javascript:openPrintPage()","","","","",0);


$table_tool .= "<td nowrap=\"nowrap\">
					<a href=\"javascript:AbsToPre()\" class=\"tabletool\">
						<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_import.gif\" alt=\"button_import\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">
						$i_StudentAttendance_Action_SetAbsentToOntime
					</a>
				</td>";

$TAGS_OBJ[] = array($i_SmartCard_DailyOperation_ViewClassStatus, "", 0);
$MODULE_OBJ = $lc->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START(urldecode($Msg));

$PAGE_NAVIGATION[] = array($display_period,'class_status.php?DayType='.$DayType.'&TargetDate='.$TargetDate);
$PAGE_NAVIGATION[] = array($class_name);

if ($msg == 1) $SysMsg = $linterface->GET_SYS_MSG("add");
if ($msg == 2) $SysMsg = $linterface->GET_SYS_MSG("update");
?>
<div id="tooltip3" style="position:absolute; left:100px; top:30px; z-index:1; visibility: hidden"></div>
<script language="Javascript" src="/templates/tooltip.js"></script>
<script language="JavaScript" type="text/javascript">
// for Preset Absence
function showPresetAbs(obj,reason){
	var pos_left = getPostion(obj,"offsetLeft");
	var pos_top  = getPostion(obj,"offsetTop");
	
	offsetX = (obj==null)?0:obj.width;
	//offsetY = (obj==null)?0:obj.height;
	offsetY =0;
	objDiv = document.getElementById('tooltip3');
	if(objDiv!=null){
		objDiv.innerHTML = reason;
		objDiv.style.visibility='visible';
		objDiv.style.top = pos_top+offsetY+"px";
		objDiv.style.left = pos_left+offsetX+"px";
		setDivVisible(true, "tooltip3", "lyrShim");
	}
}

// for preset absence
function hidePresetAbs(){
	obj = document.getElementById('tooltip3');
	if(obj!=null)
		obj.style.visibility='hidden';
	setDivVisible(false, "tooltip3", "lyrShim");
}

function openPrintPage()
{
	newWindow("view_student_print.php?class_name=<?=$class_name?>&DayType=<?=$DayType?>&TargetDate=<?=$TargetDate?>",4);
}

function AbsToPre()
{
    obj = document.form1;
    len=obj.elements.length;
    var i=0;
    for( i=0 ; i<len ; i++) {
        if (obj.elements[i].name=="drop_down_status[]")
        {
            obj2 = obj.elements[i];
            if (obj2.selectedIndex == 1)
            {
                obj2.selectedIndex = 0;
            }
        }
        ChangeDefaultSessionSelect(i);
    }
}

function changePage(val)
{
	var obj = document.form1;
	var path = "/home/eAdmin/StudentMgmt/attendance/dailyoperation";
	var period = document.form1.period.value;
	var date = document.form1.TargetDate.value;
	
	if(val == 1){
		obj.action = path+"/late/showlate.php?TargetDate="+date+"&period="+period;
		obj.submit();
	}
	if(val == 2){
		obj.action = path+"/absence/show.php?TargetDate="+date+"&period="+period;
		obj.submit();
	}
	if(val == 3){
		obj.action = path+"/early/show.php?TargetDate="+date+"&period="+period;
		obj.submit();
	}
}

function ChangeDefaultSessionSelect(i) {
	var IsConfirmedAlready = document.getElementById('Confirmed['+i+']');
	if (IsConfirmedAlready && IsConfirmedAlready.value == "") {
		var drop_down_list = document.getElementsByName('drop_down_status[]');
		if(drop_down_list==null)return;
		var StatusObj = drop_down_list[i];
		var LateSessionObj = document.getElementById('late_session'+i);
		var OfficalLeaveSessionObj = document.getElementById('offical_leave_session'+i);
		var RequestLeaveSessionObj = document.getElementById('request_leave_session'+i);
		var PlayTruantSessionObj = document.getElementById('play_truant_session'+i);
		if (LateSessionObj) {
			if (StatusObj.selectedIndex == 1) {
				OfficalLeaveSessionObj.selectedIndex = 0;
				LateSessionObj.selectedIndex = 0;
				RequestLeaveSessionObj.selectedIndex = 2;
				PlayTruantSessionObj.selectedIndex = 0;
			}
			else if (StatusObj.selectedIndex == 2) {
				OfficalLeaveSessionObj.selectedIndex = 0;
				LateSessionObj.selectedIndex = 2;
				RequestLeaveSessionObj.selectedIndex = 0;
				PlayTruantSessionObj.selectedIndex = 0;
			}
			else {
				OfficalLeaveSessionObj.selectedIndex = 0;
				LateSessionObj.selectedIndex = 0;
				RequestLeaveSessionObj.selectedIndex = 0;
				PlayTruantSessionObj.selectedIndex = 0;
			}
		}
	}
}
</script>
<br />
<form name="form1" method="post" action="" method="post">
<iframe id="lyrShim" src="javascript:false;" scrolling="no" frameborder="0" style="position:absolute; top:0px; left:0px; display:none;"></iframe>
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	</tr>
	<tr>
		<td>
			<?=$table_confirm?>
		</td>
	</tr>
	<tr>
		<td>
			<?=$ShortCutTable?>
		</td>
	</tr>
	<tr>
		<td>
			<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
	    	<tr>
        	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
        </tr>
	    </table>	
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>
			<table width="95%" border="0" cellpadding="0" cellspacing="0" align="center" onMouseMove="overhere()">
				<tr>
					<td align="left"><?= $toolbar ?></td>
					<td align="right"><?= $SysMsg ?></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table width="95%" border="0" cellpadding="0" cellspacing="0" align="center">
				<tr>
					<td valign="bottom" align="left">
						<table border=0>
							<tr>
								<td class="attendance_late" width="15px">
									&nbsp;
								</td>
								<td class="tabletext">
									<?=$Lang['StudentAttendance']['Late']?>
								</td>
								<td class="attendance_norecord" width="15px">
									&nbsp;
								</td>
								<td class="tabletext">
									<?=$Lang['StudentAttendance']['Absent']?>
								</td>
								<td class="attendance_outing" width="15px">
									&nbsp;
								</td>
								<td class="tabletext">
									<?=$Lang['StudentAttendance']['Outgoing']?>
								</td>
								<td class="row_drafted" width="15px">
									&nbsp;
								</td>
								<td class="tabletext">
									<?=$Lang['StudentAttendance']['UnConfirmed']?>
								</td>
							</tr>
						</table>
					</td>
					<td valign="bottom" align="right">
						<table border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td width="21"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_01.gif" width="21" height="23"></td>
								<td background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_02.gif">
									<table border="0" cellspacing="0" cellpadding="2">
										<tr>
											<?=$table_tool?>
										</tr>
									</table>
								</td>
								<td width="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_03.gif" width="6" height="23"></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<?= $table_attend ?>
		</td>
	</tr>
	<tr>
		<td>
			<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
		    	<tr>
                	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
		    </table>	
		</td>
	</tr>
	<tr>
		<td>
			<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
		    	<tr>
				    <td align="center" colspan="2">
						<?= $linterface->GET_ACTION_BTN($button_save, "button", "AlertPost(document.form1,'view_student_update.php','$i_SmartCard_Confirm_Update_Attend?')") ?>
						<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
						<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:window.location='class_status.php?DayType=$DayType&TargetDate=$TargetDate'","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<input name="class_name" type="hidden" value="<?=$class_name?>">
<input id="DayType" name="DayType" type="hidden" value="<?=$DayType?>">
<input name="confirmed_id" type="hidden" value="<?=$confirmed_id?>">
<input id="TargetDate" name="TargetDate" type="hidden" value="<?=$TargetDate?>">
<input name=PageLoadTime type=hidden value="<?=time()+1?>">
</form>
<br />
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>