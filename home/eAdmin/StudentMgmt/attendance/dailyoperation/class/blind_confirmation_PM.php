<?php
############################################# Change Log #############################################
#
#  Date: 2010-02-02 (By Carlos)
#  Details: Add checking on confirm for all class to skip class which has been confirmed already
#
######################################################################################################
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$lc = new libcardstudentattend2();
$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageDailyOperation_ViewClassStatus";

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$linterface = new interface_html();

### Set Date from previous page
$ts_record = strtotime($TargetDate);
if ($ts_record == -1 || $ts_record == FALSE)
{
    $ts_record = strtotime(date('Y-m-d'));
    $TargetDate = date('Y-m-d');
}
$txt_year = date('Y',$ts_record);
$txt_month = date('m',$ts_record);
$txt_day = date('j',$ts_record);
$day_of_week = date('w',$ts_record);

# create confirm log table if not exists
$lc = new libcardstudentattend2();
$lc->retrieveSettings();
$lc->createTable_LogAndConfirm($txt_year, $txt_month);
$daily_log_table_name="CARD_STUDENT_DAILY_LOG_".$txt_year."_".$txt_month;
$attendanceMode = $lc->attendance_mode;

###period
switch ($DayType)
{
  case PROFILE_DAY_TYPE_AM: 
		$display_period = $i_DayTypeAM;
    break;
  case PROFILE_DAY_TYPE_PM: 
		$display_period = $i_DayTypePM;
  	break;
  default : 
		$display_period = $i_DayTypeAM;
    $DayType = PROFILE_DAY_TYPE_AM;
    break;
}


# select classes with non school day on TargetDate from CARD_STUDENT_SPECIFIC_DATE_TIME
	$sqlSpecial = "SELECT ClassID,IF(NonSchoolDay=1,1,2) FROM CARD_STUDENT_SPECIFIC_DATE_TIME WHERE RecordDate='$TargetDate'";
	$temp = $lc->returnArray($sqlSpecial,2);
	for($i=0;$i<sizeof($temp);$i++){
			$resultSpecial[$temp[$i][0]]=$temp[$i][1];
	}

# select classes with non school day on Target Cycle Day from CARD_STUDENT_CLASS_PERIOD_TIME
  $sqlClassCycle ="select a.ClassID,IF(NonSchoolDay=1,1,2) from CARD_STUDENT_CLASS_PERIOD_TIME AS a, INTRANET_CYCLE_DAYS AS b WHERE a.DayType=2 and a.DayValue=b.TextShort AND b.RecordDate='$TargetDate'";
	$temp=$lc->returnArray($sqlClassCycle,2);
	for($i=0;$i<sizeof($temp);$i++){
			$resultClassCycle[$temp[$i][0]]=$temp[$i][1];
	}

# select classes with non school day on Target Cycle Day from CARD_STUDENT_PERIOD_TIME
  $sqlSchoolCycle ="select IF(a.NonSchoolDay=1,1,2) from CARD_STUDENT_PERIOD_TIME AS a,INTRANET_CYCLE_DAYS AS b WHERE a.DayType=2 and a.DayValue=b.TextShort AND b.RecordDate='$TargetDate'";
	$resultSchoolCycle=$lc->returnVector($sqlSchoolCycle);

# select classes with non school day on Target Week Day from CARD_STUDENT_PERIOD_TIME
  $sqlSchoolWeek ="select IF(NonSchoolDay=1,1,2) from CARD_STUDENT_PERIOD_TIME WHERE DayType=1 and DayValue='$day_of_week'";
	$resultSchoolWeek = $lc->returnVector($sqlSchoolWeek);

# select classes with non school day on Target Week Day from CARD_STUDENT_CLASS_PERIOD_TIME
  $sqlClassWeek ="select ClassID,IF(NonSchoolDay=1,1,2) from CARD_STUDENT_CLASS_PERIOD_TIME WHERE DayType=1 and DayValue='$day_of_week'";
	$temp=$lc->returnArray($sqlClassWeek,2);
	for($i=0;$i<sizeof($temp);$i++){
			$resultClassWeek[$temp[$i][0]]=$temp[$i][1];
	}

### for class confirm
$card_student_daily_class_confirm = "CARD_STUDENT_DAILY_CLASS_CONFIRM_".$txt_year."_".$txt_month;

# select confirmed classes to skip
	$sqlClassConfirmed = "SELECT ClassID, RecordID FROM $card_student_daily_class_confirm WHERE DayNumber = '$txt_day' AND DayType = '$DayType' ";
	$temp = $lc->returnArray($sqlClassConfirmed);
	for($i=0;$i<sizeof($temp);$i++)
	{
		$resultClassConfirmed[$temp[$i]['ClassID']] = $temp[$i]['RecordID'];
	}
	
# select the timetable mode for each class
	$resultClassMode = $lc->getClassListMode();
	$count=0;

	$off_class_list = array();

# determine the mode for each class 
	for($i=0;$i<sizeof($resultClassMode);$i++){
		$off=false;
		$done=false;
		$classID = $resultClassMode[$i][0];
		$className = $resultClassMode[$i][1];
		$classMode = $resultClassMode[$i][3];
		$specialClassID= $classMode==0?0:$classID;
		# check if NonSchoolDay for Speical Date
		if($resultSpecial[$specialClassID]==1){
				$off=true;
				$done=true;
		}else if($resultSpecial[$specialClassID]==2){
				$off=false;
				$done=true;
		}
		# check if NonSchoolDay for Cycle Day 
		if(!$done){
			if($classMode == 1){ # Class Cycle Day
				if($resultClassCycle[$classID]==1){
						$off = true;
						$done= true;
				}else if($resultClassCycle[$classID]==2){
						$off = false;
						$done= true;
				}
			}else if($classMode !=2){  # School Cycle Day
				if(is_array($resultSchoolCycle) &&$resultSchoolCycle[0]==1){
								$off = true;
								$done = true;
				} else if($resultSchoolCycle[0]==2){
								$off = false;
								$done = true;
				}
			} 
		}
		# check if NonSchoolDay for Week Day
		if(!$done){
			if($classMode==1){ # Class Week Day
				if($resultClassWeek[$classID]==1){
						$done = true;
						$off = true;
				}else if($resultClassWeek[$classID]==2){
						$done = true;
						$off = false;
				}

			} else if($classMode!=2){ # School Week Day
				if(is_array($resultSchoolWeek) && $resultSchoolWeek[0]==1){
								$off = true;
								$done=true;
				} else 	if($resultSchoolWeek[0]==2){
								$off = false;
								$done=true;
				} 

			}
		}
		if($classMode==2 || $off || $resultClassConfirmed[$classID]){
			$off_class_list[] = $classID;
		}
	}

# determining the pm status 
$pm_expected_field = $lc->Get_PM_Expected_Field("d.","f.");
/*
if($lc->attendance_mode==2 || $lc->attendance_mode==3){
    $pm_expected_field = "CASE
                              WHEN (d.AMStatus IN (".CARD_STATUS_PRESENT.",".CARD_STATUS_LATE.") AND d.LunchOutTime IS NULL) OR (d.LunchBackTime IS NOT NULL)
                                   THEN '".CARD_STATUS_PRESENT."'
                              WHEN (d.AMStatus IN (".CARD_STATUS_PRESENT.",".CARD_STATUS_LATE.") AND d.LunchOutTime IS NOT NULL) AND (d.LunchBackTime IS NULL)
                                   THEN '".CARD_STATUS_ABSENT."'
                              WHEN (d.AMStatus IS NULL OR d.AMStatus = '".CARD_STATUS_ABSENT."') AND f.OutingID IS NULL THEN '".CARD_STATUS_ABSENT."'
                              WHEN (d.AMStatus IS NULL OR d.AMStatus = '".CARD_STATUS_ABSENT."') AND f.OutingID IS NOT NULL THEN '".CARD_STATUS_OUTING."'
                         END";			
}else{
	
	$pm_expected_field = "CASE
                              WHEN (d.AMStatus IN (".CARD_STATUS_PRESENT.",".CARD_STATUS_LATE.") AND d.LunchOutTime IS NULL) OR (d.LunchBackTime IS NOT NULL)
                                   THEN '".CARD_STATUS_PRESENT."'
                              WHEN d.AMStatus IS NULL AND f.OutingID IS NULL
                                   THEN '".CARD_STATUS_ABSENT."'
                              WHEN d.AMStatus IS NULL AND f.OutingID IS NOT NULL
                                   THEN '".CARD_STATUS_OUTING."'
                              ELSE '".CARD_STATUS_ABSENT."'
                          END";
}
*/

  $sqlStudentList = "SELECT 
  										c.UserID,$pm_expected_field 
  									FROM 
					  					INTRANET_USER AS c 
					  					LEFT JOIN 
					  					$daily_log_table_name AS d 
					  					ON (c.UserID=d.UserID AND d.DayNumber='$txt_day') 
					  					INNER JOIN 
					  					YEAR_CLASS AS e
					  					on c.ClassName=e.ClassTitleEN AND e.AcademicYearID = '".GET_CURRENT_ACADEMIC_YEAR_ID()."' 
											LEFT JOIN 
											CARD_STUDENT_OUTING AS f 
											ON (c.UserID=f.UserID AND f.RecordDate = '$TargetDate' AND f.DayType = '".PROFILE_DAY_TYPE_PM."')
					  				WHERE 
					  					($pm_expected_field='".CARD_STATUS_ABSENT."' 
					  					AND d.PMStatus IS NULL)";

  $sqlOutingList = "SELECT 
  										c.UserID,$pm_expected_field 
  									FROM 
					  					INTRANET_USER AS c 
					  					LEFT JOIN 
					  					$daily_log_table_name AS d 
					  					ON (c.UserID=d.UserID AND d.DayNumber='$txt_day') 
					  					INNER JOIN 
					  					YEAR_CLASS AS e 
					  					on c.ClassName=e.ClassTitleEN AND e.AcademicYearID = '".GET_CURRENT_ACADEMIC_YEAR_ID()."' 
											LEFT JOIN CARD_STUDENT_OUTING AS f 
											ON (c.UserID=f.UserID AND f.RecordDate = '".$TargetDate."' AND f.DayType = '".PROFILE_DAY_TYPE_PM."' )
					  				WHERE 
					  					($pm_expected_field='".CARD_STATUS_OUTING."' 
					  					AND d.PMStatus IS NULL)";  					  					
  		
  if(sizeof($off_class_list)>0){
  		$sqlStudentList .=" AND e.YearClassID NOT IN(".implode(",",$off_class_list).")";
  		$sqlOutingList.=" AND e.YearClassID NOT IN(".implode(",",$off_class_list).")";
  }
  $sqlStudentList.=" AND c.RecordType=2 AND c.RecordStatus IN(0,1,2) AND c.ClassName IS NOT NULL";
  $sqlOutingList.=" AND c.RecordType=2 AND c.RecordStatus IN(0,1,2) AND c.ClassName IS NOT NULL AND f.RecordDate='$TargetDate'";
	//debug_r($sqlStudentList);
 $studentList = $lc->returnVector($sqlStudentList,2);                         
 
 # count the no. of absent student and outing student
 if(sizeof($studentList)>0){
 		//debug_r($sqlOutingList);
		$temp= $lc->returnVector($sqlOutingList,2);
		$outingCount=sizeof($temp)+0;
		$absentCount = sizeof($studentList);
	}else{
		 $outingCount=0;
		 $absentCount=0;
	}

$TAGS_OBJ[] = array($i_SmartCard_DailyOperation_ViewClassStatus, "", 0);
$MODULE_OBJ = $lc->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

$PAGE_NAVIGATION[] = array($i_SmartCard_DailyOperation_BlindConfirmation);

$PAGE_NAVIGATION1 = $i_StudentPromotion_Summary;
$PAGE_NAVIGATION2 = $i_SmartCard_DailyOperation_BlindConfirmation_Notes1;

if ($msg == 1) $SysMsg = $linterface->GET_SYS_MSG("add");
if ($msg == 2) $SysMsg = $linterface->GET_SYS_MSG("update");
?>
<br />
<form name="form1" method="GET" action="blind_confirmation_update_PM.php">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	</tr>
	<tr>
		<td>
			<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
		    	<tr>
                	<td><?=$linterface->GET_NAVIGATION2($PAGE_NAVIGATION1)?></td>
                </tr>
		    </table>	
		</td>
	</tr>
	<tr>
		<td>
			<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
		    	<tr>
                	<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $i_StaffAttendance_IntermediateRecord_Date ?></td>
					<td class="tabletext" width="70%"><?=$TargetDate?></td>
                </tr>
                <tr>
                	<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $i_Attendance_DayType ?></td>
					<td class="tabletext" width="70%"><?=$display_period?></td>
                </tr>
                <tr>
                	<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $i_SmartCard_DailyOperation_Number_Of_Absent_Student ?></td>
					<td class="tabletext" width="70%"><?=$absentCount?></td>
                </tr>
                 <tr>
                	<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $i_SmartCard_DailyOperation_Number_Of_Outing_Student ?></td>
					<td class="tabletext" width="70%"><?=$outingCount?></td>
                </tr>
		    </table>	
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>
			<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
		    	<tr>
                	<td><?=$linterface->GET_NAVIGATION2($PAGE_NAVIGATION2)?></td>
                </tr>
                <tr>
                	<td class="tabletext"><ul><li><?=$i_SmartCard_DailyOperation_BlindConfirmation_Notes2?></li></ul></td>
                </tr>
		    </table>	
		</td>
	</tr>
	<tr>
		<td>
			<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
		    	<tr>
                	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
		    </table>	
		</td>
	</tr>
	<tr>
		<td>
			<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
		    	<tr>
				    <td align="center" colspan="2">
						<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "") ?>
						<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:history.back()","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<input type="hidden" name="TargetDate" value="<?=$TargetDate?>">
<input type="hidden" name="DayType" value="<?=$DayType?>">
</form>
<br />
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>