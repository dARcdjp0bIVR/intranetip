<?php
include_once("../../../../../includes/global.php");
include_once("../../../../../includes/libdb.php");
include_once("../../../../../includes/libfilesystem.php");
include_once("../../../../../includes/libaccount.php");
include_once("../../../../../includes/libclass.php");
include_once("../../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../../templates/fileheader.php");

intranet_opendb();


$lclass = new libclass();
$select_class = $lclass->getSelectClass("name=\"targetClass\" onChange=\"this.form.action='';this.form.submit()\"",$targetClass);

if ($targetClass != "")
    $select_students = $lclass->getStudentSelectByClass($targetClass,"size=\"21\" multiple=\"multiple\" name=\"targetID[]\"");
    
################################################################

?>

<script language="javascript">
function isExists(obj2,val){
	for(j=0;j<obj2.options.length;j++)
		if(obj2.options[j].value==val)
			return true;
	return false;
}
function AddOptions(obj){
     par = opener.window;
     parObj = opener.window.document.form1.elements["<?php echo $fieldname; ?>"];

     checkOption(obj);
     par.checkOption(parObj);
     i = obj.selectedIndex;

     while(i!=-1)
        {
         addtext = obj.options[i].text;
		 
          if(!isExists(parObj,obj.options[i].value)){
          	par.checkOptionAdd(parObj, addtext, obj.options[i].value);
          }
          obj.options[i] = null;
          i = obj.selectedIndex;
     }
}

function SelectAll(obj)
{
         for (i=0; i<obj.length; i++)
         {
              obj.options[i].selected = true;
         }
}

</script>

<form name="form1" action="index.php" method="post">
<table border="0" cellpadding="0" cellspacing="0" align="center" width="90%">
<tr>
<td align="center">

<table width="95%" border="0" cellpadding="5" cellspacing="1">
<tr>
	<td>
	<table width="100%" cellpadding="0" cellspacing="0" border="0">
        <tr>
			<td valign="top" nowrap="nowrap"><?=$i_ClassName?>: </td>
			<td width="80%"><?php echo $select_class; ?></td>
		</tr>
		<tr><td colspan="2" height="5"></td></tr>		
		<?php if($targetClass != "")
		{ ?>
		<tr><td colspan="2"><hr size=1 class="hr_sub_separator"></td></tr>
		<tr><td colspan="2" height="5"></td></tr>
		<tr>
			<td valign="top" nowrap="nowrap" ><?=$i_UserStudentName?>: </td>
			<td width="80%" style="align: left">
		       	<table border="0" cellpadding="0" cellspacing="0" align="left">
					<tr> 
						<td>
		       				<?php echo $select_students; ?>
		       			</td>
		       			<td style="vertical-align:bottom">        
					        <table width="100%" border="0" cellspacing="0" cellpadding="6">
								<tr> 
									<td align="left"> 
										<a href="javascript:checkOption(document.form1.elements['targetID[]']);AddOptions(document.form1.elements['targetID[]'])"><img src='<?=$image_path?>/admin/button/s_btn_add_<?=$intranet_session_language?>.gif' border=0></a>
									</td>
								</tr>
								<tr> 
									<td align="left"> 
										<a href="javascript:SelectAll(document.form1.elements['targetID[]']); "><img src='<?=$image_path?>/admin/button/s_btn_select_all_<?=$intranet_session_language?>.gif' border=0></a>
									</td>
								</tr>
							</table>
		       			</td>
		       		</tr>
		       	</table>
			</td>
		</tr>
		<?php
		} ?>
	</table>
</tr>

</table>


</td>
</tr>

<tr><td>

<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
<tr><td><hr size=1 class="hr_sub_separator"></td></tr>
<tr>
	<td align="center"><a href="javascript:self.close()"><img src='<?=$image_path?>/admin/button/s_btn_close_<?=$intranet_session_language?>.gif' border=0></a>
	</td>
</tr>
</table>
<br/>

</td></tr>

</table>
<input type="hidden" name="fieldname" value="<?php echo $fieldname; ?>" />
</form>
<?php
intranet_closedb();
include_once("../../../../../templates/filefooter.php");
?>