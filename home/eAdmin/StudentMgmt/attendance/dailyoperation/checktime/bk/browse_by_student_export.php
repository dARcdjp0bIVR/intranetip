<?php
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_opendb();

$lc = new libcardstudentattend2();


$filter = $filter==""?2:$filter;
$today = date('Y-m-d');
switch($filter){
	case 1: $cond_filter = " AND a.RecordDate <='$today' "; break;
	case 2: $cond_filter = " AND a.RecordDate >'$today' "; break;
	case 3: $cond_filter = ""; break;
	default: $cond_filter =" AND a.RecordDate > '$today' ";
}

$order = ($order == 1) ? 1 : 0;
if ($field == "") $field = 0;
$field_array = array("a.RecordDate","a.DayPeriod","a.Reason","a.Remark");
$order_str = " ORDER BY ".$field_array[$field];
$order_str.= ($order==1)?" ASC " : " DESC ";

if($studentID!=""){
	$lb = new libdb();
	$namefield = getNameFieldWithClassNumberByLang("");
	$sql ="SELECT $namefield FROM INTRANET_USER WHERE UserID='$studentID' AND RecordType=2 AND RecordStatus IN (0,1,2)";
	$temp = $lb->returnVector($sql);
	$student_name = $temp[0];
	
	$sql=" SELECT a.RecordDate,
				 IF(a.DayPeriod=2,'$i_DayTypeAM','$i_DayTypePM'),
				 a.Reason,
				 a.Remark
				FROM CARD_STUDENT_PRESET_LEAVE AS a WHERE a.StudentID='$studentID'  $cond_filter AND 
				(a.RecordDate LIKE '%$keyword%' OR a.Reason LIKE '%$keyword%' OR a.Remark LIKE '%$keyword%' )
				 $order_str ";

	$temp = $lb->returnArray($sql,4);
	
	$lexport = new libexporttext();
	
	$exportColumn = array("$i_Attendance_Date", "$i_Attendance_DayType", "$i_Attendance_Reason", "$i_Attendance_Remark");
	$export_content = $lexport->GET_EXPORT_TXT($temp, $exportColumn);
	
	$export_content_final = "$i_SmartCard_DailyOperation_Preset_Absence - $student_name\n\n";
	$export_content_final .= $export_content;
	
}

intranet_closedb();

$filename = "preset_absence_".$student_name.".csv";
$lexport->EXPORT_FILE($filename, $export_content_final);
?>