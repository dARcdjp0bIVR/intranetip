<?php
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libclass.php");
include_once("../../../../includes/libcardstudentattend2.php");

intranet_opendb();

$return_url = $HTTP_SERVER_VARS['HTTP_REFERER'];

if($RecordID=="" || sizeof($RecordID)<=0)
	header("Location: $return_url");

if(!is_array($RecordID))
	$RecordID = array($RecordID);

$idlist=implode(",",$RecordID);

$sql="DELETE FROM CARD_STUDENT_PRESET_LEAVE WHERE RecordID IN($idlist)";

$li = new libcardstudentattend2();
$li->db_db_query($sql);	

intranet_closedb();

$t = explode("?",$return_url);
$filename = $t[0];
$str = $t[1];
$ary = explode("&",$str);
$found = false;
for($i=0;$i<sizeof($ary);$i++){
	if(strpos($ary[$i],"msg=")!==false){
			$ary[$i]="msg=3";
			$found = true;
			break;
	}
}
if(!$found){
	array_push($ary,"msg=3");
}
$return_url = $filename."?".implode("&",$ary);	
//echo $return_url;
header("Location: $return_url");
?>
