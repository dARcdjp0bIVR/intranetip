<?php

include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libdbtable.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../includes/libclass.php");
include_once("../../../../includes/libcardstudentattend2.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../templates/fileheader.php");

intranet_opendb();


$filter = $filter==""?2:$filter;
$today = date('Y-m-d');
$target_date = $target_date==""?$today:$target_date;
$date_cond = " a.RecordDate = '$target_date' ";

$order = ($order == 1) ? 1 : 0;
if ($field == "") $field = 0;

$namefield = getNameFieldByLang("b.");

$field_array = array("$namefield","b.ClassName","b.ClassNumber","a.DayPeriod","a.Reason","a.Remark");
$order_str = " ORDER BY ".$field_array[$field];
$order_str.= ($order==1)?" ASC " : " DESC ";

if($target_date!=""){
	//$namefield = getNameFieldWithClassNumberByLang("b.");
	$lb = new libdb();
	$sql=" SELECT $namefield, b.ClassName,b.ClassNumber,
				 IF(a.DayPeriod=2,'$i_DayTypeAM','$i_DayTypePM'),
				 a.Reason,
				 a.Remark
				FROM CARD_STUDENT_PRESET_LEAVE AS a 
				LEFT OUTER JOIN INTRANET_USER AS b ON (a.StudentID = b.UserID)
				WHERE $date_cond AND 
				($namefield LIKE '%$keyword%' OR a.Reason LIKE '%$keyword%' OR a.Remark LIKE '%$keyword%' )
				 $order_str ";
	$temp = $lb->returnArray($sql,6);
	
	$display= "	<table width=90% border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align='center'>";
	$display.="<tr>";
	$display.= "<td width=1 class=tableTitle>#</td>\n";
	$display.= "<td width=20% class=tableTitle>".$i_UserStudentName."</td>\n";
	$display.= "<td width=10% class=tableTitle>".$i_ClassName."</td>\n";
	$display.= "<td width=10% class=tableTitle>".$i_ClassNumber."</td>\n";
	$display.= "<td width=10% class=tableTitle>".$i_Attendance_DayType."</td>\n";
	$display.= "<td width=25% class=tableTitle>".$i_Attendance_Reason."</td>\n";
	$display.= "<td width=25% class=tableTitle>".$i_Attendance_Remark."</td>\n";
	for($i=0;$i<sizeof($temp);$i++){
		list($username,$classname,$classnum,$daytype,$reason,$remark) = $temp[$i];
		$css = $i%2==0?"tableContent":"tableContent2";
		$row = "<tr>";
		$row.= "<td class='$css'>".($i+1)."</td>";		
		$row.= "<td class='$css'>$username</td>";
		$row.= "<td class='$css'>$classname</td>";
		$row.= "<td class='$css'>$classnum</td>";
		$row.= "<td class='$css'>$daytype</td>";		
		$row.= "<td class='$css'>$reason</td>";		
		$row.= "<td class='$css'>$remark</td>";		
		$row.="</tr>";
		$display.=$row;
	}
	if(sizeof($temp)<=0){
		$row = "<tr><td colspan=5 align=center height=40 class='tableContent'>$i_no_record_exists_msg</td></tr>";
		$display.=$row;
	}
	$display.="</table>";
}

?>
<table width=90% border=0 cellpadding=3 cellspacing=0 align=center>
	<tr><Td><b><?=$i_SmartCard_DailyOperation_Preset_Absence?> (<?=$target_date?>) </b></td></tr>
</table>
<?=$display?>

<?php
intranet_closedb();
include_once("../../../../templates/filefooter.php");
?>