<?php
//Modifying by: kenenth chung

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libstudentattendance_ui.php");
include_once($PATH_WRT_ROOT."includes/libnotice_ui.php");

intranet_auth();
intranet_opendb();

if ($page_size_change == 1)
{
  setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
  $ck_page_size = $numPerPage;
}

$NoticeUI = new libnotice_ui();
$StudentAttendUI = new libstudentattendance_ui();

$linterface         = new interface_html();
$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage['PrintNotice'] = 1;

### Title ###
$TAGS_OBJ[] = array($Lang['eDiscipline']['PrintNotice']);
$MODULE_OBJ = $StudentAttendUI->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();

$status = $_REQUEST['status'];
$sign_status = $_REQUEST['sign_status'];
$print_status = $_REQUEST['print_status'];
$StartDate = $_REQUEST['StartDate'];
$EndDate = $_REQUEST['EndDate'];
$keyword = $_REQUEST['keyword'];
$title = $_REQUEST['title'];

echo $NoticeUI->Get_Print_Module_Notice_UI('StudentAttendance',$title,$status,$sign_status,$print_status,$StartDate,$EndDate,$keyword);

intranet_closedb();
$linterface->LAYOUT_STOP();
?>

