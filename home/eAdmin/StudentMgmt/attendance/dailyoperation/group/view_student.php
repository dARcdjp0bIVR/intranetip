<?php
// Editing by 
##################################### Change Log ###############################################
# 2019-11-08 by Ray: Add $sys_custom['SmartCardAttendance_StudentAbsentSessionToggleSessionIgnoreConfirm']
# 2019-09-23 by Ray: Add preset absent tooltip mouse over not close
# 2019=06-19 by Ray: Add teacher remark preset select box
# 2018-06-22 by Carlos: Display group name to follow UI language.
# 2017-05-26 by Carlos: [ip.2.5.8.7.1] $sys_custom['StudentAttendance']['RecordBodyTemperature'] display body temperature.
# 2017-05-18 by Carlos: [ip.2.5.8.4.1] $sys_custom['StudentAttendance']['NoCardPhoto'] display no bring card photo.
# 2016-10-17 by Carlos: [ip2.5.7.10.1] Improved to pre-display outing remark if have preset outing record.
# 2016-07-05 by Carlos: [ip2.5.7.7.1] $sys_custom['StudentAttendance']['AllowEditTime'] - allow edit in time.
# 2016-03-23 by Carlos: [ip2.5.7.4.1] Added change all status tool button.
# 2016-03-11 by Carlos: [ip2.5.7.4.1] $sys_custom['StudentAttendance']['PMStatusFollowLunchSetting'] - customized to make students that do not go out for lunch 
#										to set PM status to follow AM status because these students do not tap card in lunch time to trigger the Pm in status. 
# 2015-11-06 by Carlos: [ip2.5.7.1.1] Fixed Outing and Early Leave duplicated records bug.
# 2015-03-31 by Carlos: [ip2.5.6.5.1] If have preset absence remark and record not confirmed yet, display preset absence remark to office remark.
# 2015-02-13 by Carlos: [ip2.5.6.3.1] Added [Waived], [Reason], [Office Remark]. 
# 2014-10-17 by Carlos: [ip2.5.5.10.1] Change Remark field to text input
# 2014-07-17 by Bill;	change drop down list to radio button and add student count
# 2014-06-20 by Carlos: add $sys_custom['SmartCardAttendance_StudentAbsentSessionStep'] for the session step value
# 2014-03-14 by Carlos: $sys_custom['SmartCardAttendance_StudentAbsentSession2'] - added Absent Session
# 2013-11-06 by Henry:	apply the default session selection using the settings
# 2013-04-16 by Carlos: Add CARD_STUDENT_DAILY_REMARK.DayType
# 2012-10-25 by Carlos: Improve performance by not joining INTRANET_USER to get ModifyBy/PMModifyBy username and ConfirmedUserID/PMConfirmedUserID username
#						User assoc array to map username(assume records are only managed by staff users, only build assoc array with staff users to save memory usage)
# 2012-06-07 by Carlos: Join CARD_STUDENT_ATTENDANCE_GROUP instead of CARD_STUDENT_CLASS_SPECIFIC_MODE
# 2012-05-02 by Carlos: Treat htmlspecialchars for Preset Absent/Outing reason and remark
# 2011-12-16 by Carlos: Modified to separate AM and PM Modify fields
# 2011-11-16 by Carlos: Added early leave small icon with info shown as tooltip
# 2010-09-09 by Carlos: Change session interval from 1 to 0.5
################################################################################################
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."includes/libwordtemplates.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();
$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageDailyOperation_ViewGroupStatus";

$GeneralSetting = new libgeneralsettings();
$SettingList[] = "'DefaultAttendanceStatus'";
$Settings = $GeneralSetting->Get_General_Setting('StudentAttendance',$SettingList);
$student_attend_default_status = ($Settings['DefaultAttendanceStatus'] == "")? CARD_STATUS_ABSENT:$Settings['DefaultAttendanceStatus'];

$linterface = new interface_html();

$setting_noNeedTakeAttendace = 2;
### class used
$LIDB = new libdb();
### Set Date from previous page
$ts_record = strtotime($TargetDate);
if ($ts_record == -1)
{
    $ts_record = strtotime(date('Y-m-d'));
}
$txt_year = date('Y',$ts_record);
$txt_month = date('m',$ts_record);
$txt_day = date('d',$ts_record);

list($school_year_id, $school_year, $semester_id, $semester) = getAcademicYearInfoAndTermInfoByDate($TargetDate);

$class_id = isset($_POST['class_id'])? $_POST['class_id'] : $_GET['class_id'];
if(!$lc->groupIsRequiredToTakeAttendanceByDate($class_id,$TargetDate)){
	header("Location: ../");
	exit();
}

$lc->createTable_LogAndConfirm($txt_year, $txt_month);

###period
$DayType = isset($_POST['DayType'])? $_POST['DayType'] : $_GET['DayType'];
switch ($DayType)
{
  case PROFILE_DAY_TYPE_AM: 
  	$display_period = $i_DayTypeAM;
    break;
  case PROFILE_DAY_TYPE_PM: 
  	$display_period = $i_DayTypePM;
    break;
  default : 
  	$display_period = $i_DayTypeAM;
    $DayType = PROFILE_DAY_TYPE_AM;
    break;
}

### build confirm table exists
//$card_student_daily_class_confirm = "CARD_STUDENT_DAILY_CLASS_CONFIRM_".$txt_year."_".$txt_month;
$card_student_daily_group_confirm = "CARD_STUDENT_DAILY_GROUP_CONFIRM_".$txt_year."_".$txt_month;

$sql = "SELECT
          a.RecordID,
          IF(a.ConfirmedUserID <> -1, ".getNameFieldWithClassNumberByLang("c.").", CONCAT('$i_general_sysadmin')),
          a.DateModified
	      FROM
          $card_student_daily_group_confirm as a
          LEFT OUTER JOIN INTRANET_GROUP as b ON (a.groupid=b.groupid)
          LEFT OUTER JOIN INTRANET_USER as c ON (a.ConfirmedUserID=c.UserID)
       	WHERE b.groupid = \"$class_id\" AND a.DayNumber = ".$txt_day." AND a.DayType = $DayType";

$result = $LIDB->returnArray($sql,3);
$col_width = 150;
$table_confirm = "";

// get Group name
$libgroup = new libgroup($class_id);
$class_name = $libgroup->Title;
$display_group_name = Get_Lang_Selection($libgroup->TitleChinese, $libgroup->Title);

if ($lc->attendance_mode != 1)
{
  $period[] = array(PROFILE_DAY_TYPE_AM,$i_DayTypeAM);
}
if ($lc->attendance_mode != 0)
{
  $period[] = array(PROFILE_DAY_TYPE_PM,$i_DayTypePM);
}

//$selection_period = getSelectByArray($period,' name="" id="" onchange="Switch_Period(this.value);" ', $DayType,0,1);

# Build radio button
$dataTypeLength = sizeof($period);
$selection_period = "";

// Only 1 button is required: Display AM/PK Only
if($dataTypeLength == 1){
//	$selection_period .= $linterface->Get_Radio_Button('radio_1_checked', 'Daytime', $period[0][0], true, $Class="", $period[0][1], $Onclick="Switch_Period(this.value)",$isDisabled=1);
	$selection_period .= $period[0][1];
} else {
		for ($i=0; $i<$dataTypeLength; $i++){	
			$radioSelected = false;
			$startChecked = '';
			// Set target radio button to checked
			if($DayType == $period[$i][0]){
				$radioSelected = true;
				$startChecked = ' checked';
			}
			$selection_period .= $linterface->Get_Radio_Button("radio_$i$startChecked", 'DayType', $period[$i][0], $radioSelected, $Class="", $period[$i][1], $Onclick="Switch_Period(this.value)",$isDisabled=0);
		}
}


### Group select
$group_list = $lc->getGroupListToTakeAttendanceByDate($TargetDate);
$group_select = "<SELECT id=\"group_id\" name=\"group_id\" onChange=\"Switch_Group(this.value);\">\n";
for($i=0; $i<sizeOf($group_list); $i++)
{
        list($itr_group_id, $itr_group_name) = $group_list[$i];
        $selected = ($itr_group_id==$class_id)?"SELECTED":"";
        $group_select .= "<OPTION value=\"".$itr_group_id."\" $selected>".$itr_group_name."</OPTION>\n";
}
$group_select .= "</SELECT>\n";
$x = "<table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">\n";
$x .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_StudentAttendance_Field_Date</td>";
$x .= "<td class=\"tabletext\" width=\"70%\">$TargetDate</td></tr>\n";
$x .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_GroupName</td>";
$x .= "<td class=\"tabletext\" width=\"70%\">".$group_select."</td></tr>\n";
$x .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_StudentAttendance_Slot</td>";
$x .= "<td class=\"tabletext\" width=\"70%\">".$selection_period."</td></tr>\n";

list($confirmed_id, $confirmed_user_name, $last_confirmed_date) = $result[0];
if($confirmed_id=="")
{
	$x .= "<tr><td>&nbsp;</td><td class=\"tabletext\" width=\"70%\">$i_SmartCard_Frontend_Take_Attendance_No_Confirm_Record</td></tr>";
}
else
{
	$x .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_StudentAttendance_Field_LastConfirmedTime</td>";
	$x .= "<td class=\"tabletext\" width=\"70%\">$last_confirmed_date ($confirmed_user_name)</td></tr>";
}
$x .= "</table>\n";
$table_confirm = $x;


### check if Preset Absence exists
$sql="SELECT COUNT(*) 
		FROM 
			INTRANET_USER AS A ,
			CARD_STUDENT_PRESET_LEAVE AS B,
			INTRANET_USERGROUP AS C
		WHERE 
			C.GROUPID = '$class_id' AND
			C.USERID = A.USERID AND
			A.RECORDTYPE=2 AND 
			A.RECORDSTATUS IN(0,1,2) AND 
			A.USERID = B.STUDENTID  AND
			B.RECORDDATE='$txt_year-$txt_month-$txt_day' AND 
			B.DayPeriod=".$DayType;

$temp = $LIDB->returnVector($sql);
$preset_count = $temp[0];

### build card log table
$card_log_table_name = "CARD_STUDENT_DAILY_LOG_".$txt_year."_".$txt_month;

if($sys_custom['StudentAttendance']['NoCardPhoto']){
	$studentIdToNoCardPhotoRecords = $lc->getNoCardPhotoRecords(array('RecordDate'=>$TargetDate,'StudentIDToRecord'=>1));
}

if ($lc->EnableEntryLeavePeriod) {
	$FilterInActiveStudent .= "
        INNER JOIN 
        CARD_STUDENT_ENTRY_LEAVE_PERIOD as selp 
        on a.UserID = selp.UserID 
        	AND 
        	'".$TargetDate."' between selp.PeriodStart and CONCAT(selp.PeriodEnd,' 23:59:59') 
        ";
}

if ($DayType == PROFILE_DAY_TYPE_AM) {
	$expect_field = $lc->Get_AM_Expected_Field("b.","c.","f.");
	$AbsentExpectedReasonField = $lc->Get_Expected_Reason($DayType,PROFILE_TYPE_ABSENT,'j.',"","d.","f.");
	$LateExpectedReasonField = $lc->Get_Expected_Reason($DayType,PROFILE_TYPE_LATE,'j.');
	$OutingExpectedReasonField = $lc->Get_Expected_Reason($DayType,CARD_STATUS_OUTING,'j.',"","d.","c.",true);
	
	$StatusField = "AMStatus";
	$time_field = "InSchoolTime";
	$station_field = "InSchoolStation";
	
	$IsConfirmField = "IsConfirmed";
	$ConfirmUserField = "ConfirmedUserID";
	
	$DateModifiedField = "DateModified";
  	$ModifyByField = "ModifyBy";
  	
  	$daily_remark_field = "d.Remark as TeacherRemark ";
  	$office_remark_field = "IF(j.RecordID IS NOT NULL,j.OfficeRemark,IF(TRIM(f.Remark)!='',f.Remark,c.Detail)) as OfficeRemark ";
}
else {
	if($sys_custom['StudentAttendance']['PMStatusFollowLunchSetting']){
		if($lc->attendance_mode == 2 && $lc->PMStatusNotFollowAMStatus){ // whole day with lunch, PM status does not follow AM status
			$LUNCH_ALLOW_LIST_TABLE = "CARD_STUDENT_LUNCH_ALLOW_LIST";
			$join_lunch_setting_table = " LEFT JOIN $LUNCH_ALLOW_LIST_TABLE ON $LUNCH_ALLOW_LIST_TABLE.StudentID=a.UserID ";
		}
	}
	
	$expect_field = "IF(b.PMStatus IS NOT NULL,
												b.PMStatus,
												".$lc->Get_PM_Expected_Field("b.","c.","f.").")";
	$AbsentExpectedReasonField = $lc->Get_Expected_Reason($DayType,PROFILE_TYPE_ABSENT,'j.','k.','d.','f.');
	$LateExpectedReasonField = $lc->Get_Expected_Reason($DayType,PROFILE_TYPE_LATE,'j.');
	$OutingExpectedReasonField = $lc->Get_Expected_Reason($DayType,CARD_STATUS_OUTING,'j.',"","d.","c.",true);
	$StatusField = "PMStatus";
	if ($lc->attendance_mode==1)
	{
    $time_field = "InSchoolTime";
	  $station_field = "InSchoolStation";
	}
	else
	{
    $time_field = "LunchBackTime";
    $station_field = "LunchBackStation";
	}
	
	$IsConfirmField = "PMIsConfirmed";
	$ConfirmUserField = "PMConfirmedUserID";
	
	$DateModifiedField = "PMDateModified";
  	$ModifyByField = "PMModifyBy";
  	
  	$AMJoinTable = "LEFT JOIN CARD_STUDENT_PROFILE_RECORD_REASON as k 
						ON 
							k.StudentID = a.UserID 
							AND k.RecordDate = '".$TargetDate."' 
							AND k.DayType = '".PROFILE_DAY_TYPE_AM."'
							AND k.RecordType = '".PROFILE_TYPE_ABSENT."' ";
  	$daily_remark_field = "IF(d.RecordID IS NULL AND TRIM(d2.Remark)<>'',d2.Remark,d.Remark) as TeacherRemark ";
	$join_am_daily_remark_table = " LEFT JOIN CARD_STUDENT_DAILY_REMARK as d2 ON d2.StudentID=a.UserID AND d2.RecordDate='$TargetDate' AND d2.DayType='".PROFILE_DAY_TYPE_AM."' ";
	$office_remark_field = "IF(j.RecordID IS NOT NULL,j.OfficeRemark,IF(TRIM(f.Remark)!='',f.Remark,IF(c.Detail IS NOT NULL,c.Detail,k.OfficeRemark))) as OfficeRemark ";
}

# build assoc array UserID => UserName with staff and student type only
$sql = "SELECT UserID,".getNameFieldByLang()." as UserName FROM INTRANET_USER WHERE RecordStatus=1 AND RecordType IN ('".USERTYPE_STAFF."','".USERTYPE_STUDENT."')";
$modify_user_list = $LIDB->returnResultSet($sql);
$UserIdToNameAry = array();
for($i=0;$i<count($modify_user_list);$i++){
	$UserIdToNameAry[$modify_user_list[$i]['UserID']] = $modify_user_list[$i]['UserName'];
}

$sql  = "SELECT        
			b.RecordID,
          	a.UserID,
          	".getNameFieldByLang("a.")." as name,
          	a.ClassNumber,
          	IF(b.".$time_field." IS NULL, '-', b.".$time_field."),
          	IF(b.".$station_field." IS NULL, '-', b.".$station_field."),
          	".$expect_field.", 
			$daily_remark_field,
         	IF(f.RecordID IS NOT NULL,1,0),
 			f.Reason,
          	f.Remark, 
          	IF(c.OutingID IS NOT NULL,1,0),
			c.RecordDate,
			c.OutTime,
			c.BackTime,
			c.Location,
			c.Objective,
			c.Detail,
			a.classname,
			m.Mode,
			IFNULL(b.".$DateModifiedField.",'--'),
			b.".$ModifyByField." as ModifyBy,
			b.".$IsConfirmField." as IsConfirmed,
			b.".$ConfirmUserField." as ConfirmedUserID,
			ssc.LateSession,
			ssc.RequestLeaveSession,
			ssc.PlayTruantSession,
			ssc.OfficalLeaveSession,
			".($sys_custom['SmartCardAttendance_StudentAbsentSession2']?"ssc.AbsentSession,":"'' as AbsentSession,")." 
			b.LeaveStatus,
			b.LeaveSchoolTime,
			if (".$expect_field." = '".CARD_STATUS_ABSENT."',
				".$AbsentExpectedReasonField.",
				IF(".$expect_field." = '".CARD_STATUS_LATE."',
					".$LateExpectedReasonField.",
					IF(".$expect_field." = '".CARD_STATUS_OUTING."',
						".$OutingExpectedReasonField.",
						j.Reason))) as Reason,
			j.RecordStatus as Waived,
			f.Waive as PresetWaive,
			$office_remark_field,
			".$AbsentExpectedReasonField." as DefaultLateReason,
			".$LateExpectedReasonField." as DefaultAbsentReason, 
			".$OutingExpectedReasonField." as DefaultOutingReason 
        FROM
         	INTRANET_USER AS a
         	inner join 
					INTRANET_USERGROUP AS x 
					on a.UserID = x.UserID 
						AND x.GROUPID = '".$class_id."'
					".$FilterInActiveStudent ." 
          LEFT JOIN $card_log_table_name AS b
          ON (a.UserID=b.UserID AND
             (b.DayNumber = ".$txt_day." ||  b.DayNumber IS NULL)
             )
          LEFT JOIN CARD_STUDENT_OUTING AS c ON (a.UserID=c.UserID AND c.RecordDate = '".$TargetDate."' AND c.DayType = '".$DayType."')
          LEFT JOIN CARD_STUDENT_DAILY_REMARK AS d ON(a.UserID = d.StudentID AND d.RecordDate='$txt_year-$txt_month-$txt_day' AND d.DayType='$DayType') 
		  $join_am_daily_remark_table 
          LEFT JOIN CARD_STUDENT_PRESET_LEAVE AS f ON (a.UserID = f.StudentID AND f.DayPeriod=".$DayType." AND f.RecordDate='$txt_year-$txt_month-$txt_day')
			LEFT JOIN YEAR_CLASS as cl on cl.ClassTitleEN = a.classname and cl.AcademicYearID = '".$school_year_id."' 
			LEFT JOIN CARD_STUDENT_ATTENDANCE_GROUP as m on m.GroupID = cl.YearClassID 
			LEFT JOIN CARD_STUDENT_STATUS_SESSION_COUNT as ssc 
						ON ssc.StudentID = a.UserID 
							AND ssc.RecordDate = '".$TargetDate."' 
							AND ssc.DayType = '".$DayType."' 
			LEFT JOIN CARD_STUDENT_PROFILE_RECORD_REASON as j
						ON j.StudentID = a.UserID 
							AND j.RecordDate = '".$TargetDate."' 
							AND j.DayType = '".$DayType."' 
							AND 
							(
							(j.RecordType = b.".$StatusField." AND j.RecordType<>'".PROFILE_TYPE_EARLY."') 
							OR  
							(j.RecordType = '".PROFILE_TYPE_ABSENT."' 
							AND b.".$StatusField." = '".CARD_STATUS_OUTING."' 
							)
							)
						".$AMJoinTable." $join_lunch_setting_table 
        WHERE
          (b.DayNumber = ".$txt_day." ||  b.DayNumber IS NULL) AND
          a.RecordType=2 AND
          a.RecordStatus IN (0,1,2) 
				order by a.classname asc, a.classnumber asc
        ";
//debug_r($sql);
$result = $LIDB->returnArray($sql);

if($sys_custom['StudentAttendance']['RecordBodyTemperature']){
	$student_id_ary = Get_Array_By_Key($result, 'UserID');
	$bodyTemperatureRecords = $lc->getBodyTemperatureRecords(array('RecordDate'=>$TargetDate,'StudentID'=>$student_id_ary,'StudentIDToRecord'=>1));
}

$setAllAttend = "<br />
					<select id=\"drop_down_status_all\" name=\"drop_down_status_all\">
						<option value=\"".CARD_STATUS_PRESENT."\"".($lc->DefaultAttendanceStatus == CARD_STATUS_PRESENT?" selected":"").">".$Lang['StudentAttendance']['Present']."</option>
						<option value=\"".CARD_STATUS_ABSENT."\"".($lc->DefaultAttendanceStatus == CARD_STATUS_ABSENT?" selected":"").">".$Lang['StudentAttendance']['Absent']."</option>						
						<option value=\"".CARD_STATUS_LATE."\">".$Lang['StudentAttendance']['Late']."</option>
						<option value=\"".CARD_STATUS_OUTING."\">".$Lang['StudentAttendance']['Outing']."</option>
					</select><br />
					".$linterface->Get_Apply_All_Icon("javascript:SetAllAttend();");

$colspan = 14;
$table_attend = "";
# <td class=tableTitle>#</td>
$x = "<table width=\"100%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">";
$x .= "<tr class=\"tabletop\">";
$x .= "<td class=\"tabletoplink\">#</td>";
$x .= "<td class=\"tabletoplink\">$i_UserName</td>";
$x .= "<td class=\"tabletoplink\">$i_SmartCard_Frontend_Take_Attendance_In_School_Time</td>";
$x .= "<td class=\"tabletoplink\">$i_SmartCard_Frontend_Take_Attendance_In_School_Station</td>";
if($sys_custom['StudentAttendance']['RecordBodyTemperature']){
	$x .= "<td class=\"tabletoplink\">".$Lang['StudentAttendance']['BodyTemperature']."</td>";
}
$x .= "<td class=\"tabletoplink\">$i_SmartCard_Frontend_Take_Attendance_PreStatus</td>";
$x .= "<td class=\"tabletoplink\">&nbsp;</td>";
$x .= "<td class=\"tabletoplink\">$i_SmartCard_Frontend_Take_Attendance_Status".$setAllAttend."</td>";
if($sys_custom['SmartCardAttendance_StudentAbsentSession'])
{
	$x .= "<td class=\"tabletoplink\">".$Lang['StudentAttendance']['Late']."</td>";
	$x .= "<td class=\"tabletoplink\">".$Lang['StudentAttendance']['RequestLeave']."</td>";
	$x .= "<td class=\"tabletoplink\">".$Lang['StudentAttendance']['PlayTruant']."</td>";
	$x .= "<td class=\"tabletoplink\">".$Lang['StudentAttendance']['OfficalLeave']."</td>";
	$colspan+=4;
}
if($sys_custom['SmartCardAttendance_StudentAbsentSession2']){
	$x .= "<td class=\"tabletoplink\">".$Lang['StudentAttendance']['Absent']."</td>";
	$colspan+=1;
}
$x .= "<td class=\"tabletoplink\">
  		".$i_SmartCard_Frontend_Take_Attendance_Waived."
  		<br />
  		<input type=\"checkbox\" name=\"all_wavied\" onClick=\"$('.WaiveBtn').attr('checked',this.checked);\"></td>\n";
$x .= "<td class=\"tabletoplink\">". $i_Attendance_Reason ."</td>\n";
$x .= "<td class=\"tabletoplink\">".$Lang['StudentAttendance']['iSmartCardRemark']."</td>
		<td class=\"tabletoplink\">". $Lang['StudentAttendance']['OfficeRemark'] ."</td>
		<td class=\"tabletoplink\">".$Lang['General']['LastModified']."</td>
		<td class=\"tabletoplink\">".$Lang['General']['LastModifiedBy']."</td>
		<td class=\"tabletoplink\">".$Lang['StudentAttendance']['LastConfirmPerson']."</td>
	</tr>\n";
			
if($sys_custom['SmartCardAttendance_StudentAbsentSession'] || $sys_custom['SmartCardAttendance_StudentAbsentSession2']){
	$Settings = $lc->getDefaultNumOfSessionSettings();
}			

# Reason Words

$lword = new libwordtemplates();
$words = $lword->getWordListAttendance();
$hasWord = sizeof($words)!=0;
$reason_js = "";
$disable_color="#DFDFDF";  # background color for disabled text field
$enable_color="#FFFFFF";  # background color for enabled text field
if($hasWord)
{
	$words_absence = $lword->getWordListAttendance(1);
	$words_late = $lword->getWordListAttendance(2);
	foreach($words_absence as $key=>$word)
		$words_absence[$key]= htmlspecialchars($word);
	foreach($words_late as $key=>$word)
		$words_late[$key]= htmlspecialchars($word);
	$reason_js .= $linterface->CONVERT_TO_JS_ARRAY($words_absence, "jArrayWordsAbsence", 1);
	$reason_js .= $linterface->CONVERT_TO_JS_ARRAY($words_late, "jArrayWordsLate", 1);
	$words_outing = $lc->Get_Outing_Reason();
	foreach($words_outing as $key=>$word)
		$words_outing[$key]= htmlspecialchars($word);
	$reason_js .= $linterface->CONVERT_TO_JS_ARRAY($words_outing, "jArrayWordsOuting", 1);
}

# Teacher Remark Preset
$teacher_remark_js = "";
$teacher_remark_words = $lc->GetTeacherRemarkPresetRecords(array());
$hasTeacherRemarkWord = sizeof($teacher_remark_words)!=0;
if($hasTeacherRemarkWord)
{
    $teacher_remark_words = Get_Array_By_Key($teacher_remark_words, 'ReasonText');
    foreach($teacher_remark_words as $key=>$word)
        $teacher_remark_words_temp[$key]= htmlspecialchars($word);
        $teacher_remark_js = $linterface->CONVERT_TO_JS_ARRAY($teacher_remark_words_temp, "jArrayWordsTeacherRemark", 1);
}

//$col_count=7;
//$col_count+=$preset_count>0?1:0;
//echo "preset count=$preset_count<br>";
if(sizeOf($result) == 0)
{
	$x .= "<tr><td align=center colspan=\"$colspan\" class=body><br>$i_no_record_exists_msg<br><br></td></tr>\n";
}
else
{
	for($i=0; $i<sizeOf($result); $i++)
	{
		list($record_id, $user_id, $name,$class_number, $in_school_time, $in_school_station, $attend_status,$remark,$preset_record,$preset_reason,$preset_remark,$outing_record, $outing_date, $outing_time, $back_time, $outing_location, $outing_obj, $outing_detail,$user_class_name,$attendanceMode,$DateModified,$ModifyBy,$IsConfirmed,$ConfirmedUserID,$LateSession,$RequestLeaveSession,$PlayTruantSession,$OfficalLeaveSession,$AbsentSession,$LeaveStatus,$LeaveSchoolTime, $reason, $waived, $preset_waive, $office_remark, $DefaultAbsentReason,$DefaultLateReason,$DefaultOutingReason) = $result[$i];
		
		$ModifyName = Get_String_Display($UserIdToNameAry[$ModifyBy], 1, '--');
		$ConfirmedUser = Get_String_Display($UserIdToNameAry[$ConfirmedUserID], 1, '--');
		
		$attend_status += 0;
		$select_status = "<SELECT id=\"drop_down_status_$i\" name=drop_down_status[] onchange=\"clearReason(".$i.");setEditAllowed(".$i.");ChangeDefaultSessionSelect(".$i.");\">\n";
		
		/*
		if ($attend_status == 0 || $attend_status == 2)
		{
			$present_status = $attend_status;
		}
		else
		{
			$present_status = 0;
		}
		*/
		#$select_status .= "<OPTION value='$present_status' ".($attend_status=="0" || $attend_status=="2"? "SELECTED":"").">$i_StudentAttendance_Status_Present</OPTION>\n";
		$select_status .= "<option value='".CARD_STATUS_PRESENT."' ".($attend_status==CARD_STATUS_PRESENT? "SELECTED":"").">".$Lang['StudentAttendance']['Present']."</option>\n";
	  $select_status .= "<option value='".CARD_STATUS_ABSENT."' ".($attend_status==CARD_STATUS_ABSENT? "SELECTED":"").">".$Lang['StudentAttendance']['Absent']."</option>\n";
	  $select_status .= "<option value='".CARD_STATUS_LATE."' ".($attend_status==CARD_STATUS_LATE? "SELECTED":"").">".$Lang['StudentAttendance']['Late']."</option>\n";
	  $select_status .= "<option value='".CARD_STATUS_OUTING."' ".($attend_status==CARD_STATUS_OUTING? "SELECTED":"").">".$Lang['StudentAttendance']['Outing']."</option>\n";
		$select_status .= "</SELECT>\n";


		$select_status .= "<input name=record_id[] type=hidden value=\"$record_id\">\n";
		$select_status .= "<input name=user_id[] type=hidden value=\"$user_id\">\n";
		$select_status .= "<input name=\"Confirmed[".$i."]\" id=\"Confirmed[".$i."]\" type=hidden value=\"".$IsConfirmed."\">\n";
		$select_status .= "<input name=\"AbsentDefaultReason[".$i."]\" id=\"AbsentDefaultReason[".$i."]\" type=hidden value=\"".htmlspecialchars($DefaultAbsentReason,ENT_QUOTES)."\">\n";
		$select_status .= "<input name=\"LateDefaultReason[".$i."]\" id=\"LateDefaultReason[".$i."]\" type=hidden value=\"".htmlspecialchars($DefaultLateReason,ENT_QUOTES)."\">\n";
		$select_status .= "<input name=\"OutingDefaultReason[".$i."]\" id=\"OutingDefaultReason[".$i."]\" type=hidden value=\"".htmlspecialchars($DefaultOutingReason,ENT_QUOTES)."\">\n";
		$select_status .= "<input name=\"PrevStatus[".$i."]\" id=\"PrevStatus[".$i."]\" type=hidden value=\"".$attend_status."\">\n";
		
		switch ($attend_status){
			case CARD_STATUS_ABSENT: 
				$note = $i_StudentAttendance_Status_PreAbsent;
			  $css = "attendance_norecord";
			  break;
			case CARD_STATUS_LATE: 
				$note = $i_StudentAttendance_Status_Late;
			  $css = "attendance_late";
				break;
			case CARD_STATUS_OUTING: 
				$note = $i_StudentAttendance_Status_Outing;
			  $css = "attendance_outing";
			  break;
			default: 
				$note = "&nbsp;";
				$note = $Lang['StudentAttendance']['Present'];
				//$css = "tableContent";
				//$css = ($i%2==0)?"tablerow1":"tablerow2";
				$css = "row_approved";
				break;
		}
		if($LeaveStatus == 1 || $LeaveStatus == 2){ // AM early leave
	    	$leave_tooltip  = $Lang['StudentAttendance']['EarlyLeave']."(";
	    	$leave_tooltip .= $LeaveStatus==1?$Lang['StudentAttendance']['AM']:$Lang['StudentAttendance']['PM'];
	    	$leave_tooltip .= ") ";
	    	if(trim($LeaveSchoolTime) != "") $leave_tooltip .= $LeaveSchoolTime;
	    	$note .= "<img src=\"$image_path/{$LAYOUT_SKIN}/attendance/icon_early_leave_bw.gif\" width=\"12\" height=\"12\" title=\"$leave_tooltip\" />";
	    }
		// use below style if record not confirmed
		if (!$IsConfirmed) {
			$css = "attendance_not_confirmed";
		}
		//$css = ($attendanceMode == $setting_noNeedTakeAttendace)? "tableContent": $css;
		
		$preset_abs_info ="&nbsp;";
		# Preset Absence
		if($preset_record==1){
			/*$preset_table = "<table width=200 border=0 cellspacing=0 cellpadding=1><tr><td class=tipborder><table border=0 cellspacing=0 cellpadding=3 width=100%><tr><Td class=tipbg><B>[$i_SmartCard_DailyOperation_Preset_Absence]</b></td></tr><tr><td class=tipbg valign=top><font size=2>$i_UserName: $name ($class_number)</font></td></tr><tr><td class=tipbg valign=top><font size=2>$i_Attendance_Date: ".date('Y-m-d')."</font></td></tr><tr><td class=tipbg valign=top><font size=2>$i_Attendance_DayType: $i_DayTypeAM</font></td></tr>";
			$preset_table .= "<tr><td class=tipbg valign=top><font size=2>$i_Attendance_Reason: $preset_reason</font></td></tr><tr><td class=tipbg valign=top><font size=2>$i_Attendance_Remark: $preset_remark</font></td></tr>";
			$preset_table.="</table></td></tr></table>";*/
			$preset_table = "<table width=\'200\' border=\'0\' cellspacing=\'0\' cellpadding=\'1\'><tr>";
			$preset_table .= "<tr class=\'tablebluetop\'>";
			$preset_table .= "<td class=\'tabletoplink\'>$i_SmartCard_DailyOperation_Preset_Absence</td></tr>";
			$preset_table .= "<tr class=\'tablebluerow1\'><td class=\'tabletext\' valign=\'top\'>$i_UserName: $name ($class_number)</td></tr>";
			$preset_table .= "<tr class=\'tablebluerow1\'><td class=\'tabletext\' valign=\'top\'>$i_Attendance_DayType: ".$display_period."</td></tr>";
			$preset_table .= "<tr class=\'tablebluerow2\'><td class=\'tabletext\' valign=\'top\'>$i_Attendance_Reason: ".str_replace(array("'","\r\n", "\n", "\r"),array("\'","<br>", "<br>", "<br>"),$preset_reason)."</td></tr>";
			$preset_table .= "<tr class=\'tablebluerow1\'><td class=\'tabletext\' valign=\'top\'>$i_Attendance_Remark: ".str_replace(array("'","\r\n", "\n", "\r"),array("\'","<br>", "<br>", "<br>"),$preset_remark)."</td></tr>";
			$preset_table .= "</table>";
			$preset_table = htmlspecialchars($preset_table,ENT_QUOTES);
			$preset_abs_info = "<img onMouseOut='hidePresetAbs();' onMouseOver=\"showPresetAbs(this,'$preset_table')\" src=\"{$image_path}/{$LAYOUT_SKIN}/preset_abs.gif\" align=absmiddle>";
		}
		
		# Preset Outing
	 	if ($outing_record==1) {
			$preset_table = "<table border=0 cellspacing=0 cellpadding=1 class=attendancestudentphoto><tr><td>";
			$preset_table .= "<table border=0 cellspacing=0 cellpadding=3>";
			$preset_table .= "<tr><Td class=\'tablebluetop tabletext\' nowrap><B>[$i_StudentAttendance_Outing]</b></td></tr>";
			$preset_table .= "<tr><td class=\'tablebluerow1 tabletext\' nowrap valign=top><font size=2>$i_UserName: $name ($class_number)</font></td></tr>";
			$preset_table .= "<tr><td class=\'tablebluerow1 tabletext\' nowrap valign=top><font size=2>$i_SmartCard_StudentOutingOutTime: $outing_time</font></td></tr>";
			$preset_table .= "<tr><td class=\'tablebluerow2 tabletext\' nowrap valign=top><font size=2>$i_SmartCard_StudentOutingBackTime: $back_time</font></td></tr>";
			$preset_table .= "<tr><td class=\'tablebluerow1 tabletext\' nowrap valign=top><font size=2>$i_SmartCard_StudentOutingLocation: $outing_location</font></td></tr>";
			$preset_table .= "<tr><td class=\'tablebluerow2 tabletext\' nowrap valign=top><font size=2>$i_SmartCard_StudentOutingReason: $outing_obj</font></td></tr>";
			$preset_table .= "<tr><td class=\'tablebluerow1 tabletext\' nowrap valign=top><font size=2>$i_Attendance_Remark: ".str_replace(array("'","\r\n", "\n", "\r"),array("\'","<br>", "<br>", "<br>"),$outing_detail)."</font></td></tr>";
			$preset_table .="</table></td></tr></table>";
			$preset_table = htmlspecialchars($preset_table,ENT_QUOTES);
			$preset_abs_info = "<img onMouseOut='hidePresetAbs();' onMouseOver=\"showPresetAbs(this,'$preset_table')\" src=\"{$image_path}/{$LAYOUT_SKIN}/preset_abs.gif\" align=\"absmiddle\">";
	 	}
			
		if($sys_custom['StudentAttendance']['AllowEditTime'] && $in_school_time != '-'){
			$in_school_time_val = $in_school_time;
			$in_school_time = '<span id="InSchoolTime_Display'.$user_id.'">'.$in_school_time_val.'&nbsp;'.$linterface->GET_SMALL_BTN($Lang['Btn']['Edit'], "button", "onRevealEditTime($user_id,'".$in_school_time_val."')", "editTimeBtn".$user_id, "", "", '').'</span>';
			$in_school_time.= '<span id="InSchoolTime_Edit'.$user_id.'" style="display:none;">'.$linterface->GET_SMALL_BTN($Lang['Btn']['Cancel'], "button", "onCancelEditTime($user_id)", "cancelTimeBtn".$user_id, "", "", '').'</span>';
		}
		if($sys_custom['StudentAttendance']['NoCardPhoto'] && isset($studentIdToNoCardPhotoRecords[$user_id])){
			$photo_path = '/file/student_attendance/no_card_photo/'.$studentIdToNoCardPhotoRecords[$user_id][count($studentIdToNoCardPhotoRecords[$user_id])-1]['PhotoPath'];
			$take_photo_time = $studentIdToNoCardPhotoRecords[$user_id][count($studentIdToNoCardPhotoRecords[$user_id])-1]['TakePhotoTime'];
			$photo_userinfo = $studentIdToNoCardPhotoRecords[$user_id][count($studentIdToNoCardPhotoRecords[$user_id])-1]['StudentClassName'].'#'.$studentIdToNoCardPhotoRecords[$user_id][count($studentIdToNoCardPhotoRecords[$user_id])-1]['StudentClassNumber'].' '.$studentIdToNoCardPhotoRecords[$user_id][count($studentIdToNoCardPhotoRecords[$user_id])-1][Get_Lang_Selection('StudentChineseName','StudentEnglishName')];
			$in_school_time .= '<a href="'.$photo_path.'" rel="photos" class="tablelink fancybox" title="'.$take_photo_time.' '.$photo_userinfo.' '.$Lang['StudentAttendance']['ForgotBringCard'].'"><img src="'.$photo_path.'" width="80" height="60" border="0" align="absmiddle"></a>';
		}	
			//$css = ($i%2==0)?"tablerow1":"tablerow2";
			$x .= "<tr class=$css>";
			$x .= "	<td>".($i+1)."</td> <td>$name($user_class_name - $class_number)</td>
							<td>$in_school_time</td>
							<td>$in_school_station</td>";
			if($sys_custom['StudentAttendance']['RecordBodyTemperature']){
				$body_temperature = '-';
				if(isset($bodyTemperatureRecords[$user_id])){
					$temperature_status = $bodyTemperatureRecords[$user_id][count($bodyTemperatureRecords[$user_id])-1]['TemperatureStatus'];
					$temperature_value = $bodyTemperatureRecords[$user_id][count($bodyTemperatureRecords[$user_id])-1]['TemperatureValue'];
					$body_temperature = '<span style="color:'.($temperature_status==1?'red':'green').'" title="'.($temperature_status==1?$Lang['StudentAttendance']['BodyTemperatureAbnormal']:$Lang['StudentAttendance']['BodyTemperatureNormal']).'">'.$temperature_value.' &#8451;</span>';
				}
				$x .= "<td>".$body_temperature."</td>";
			}
		//if($attendanceMode == $setting_noNeedTakeAttendace)
		//{
		//	$x .= "<td align=\"center\" colspan=\"".($colspan-6)."\" class=\"body\">".$i_StudentAttendance_ClassMode_NoNeedToTakeAttendance."</td>";
		//}
		//else
		//{
			$x .= "<td>$note</td>
						<td>$preset_abs_info</td>
						<td>".$select_status."</td>";
			if($sys_custom['SmartCardAttendance_StudentAbsentSession'])
		  	{
		  		if ($LateSession == "" && $RequestLeaveSession == "" && $PlayTruantSession == "" && $OfficalLeaveSession == "") {
//		  		if ($attend_status == CARD_STATUS_ABSENT) {
//		  			$LateSession = $PlayTruantSession = $OfficalLeaveSession = 0;
//		  			$RequestLeaveSession = 1;
//		  		}
//		  		else if ($attend_status==CARD_STATUS_LATE) {
//		  			$LateSession = 1;
//		  			$RequestLeaveSession = $PlayTruantSession = $OfficalLeaveSession = 0;
//		  		}
//		  		else {
//		  			$LateSession = $RequestLeaveSession = $PlayTruantSession = $OfficalLeaveSession = 0;
//		  		}
				//--- Henry Added 20131106 [start]
				/*
				$SettingList[] = "'DefaultNumOfSessionForLate'";
				$SettingList[] = "'DefaultNumOfSessionForLeave'";
				$SettingList[] = "'DefaultNumOfSessionForAbsenteesism'";
				$SettingList[] = "'DefaultNumOfSessionForHoliday'";
				$Settings = $GeneralSetting->Get_General_Setting('StudentAttendance',$SettingList);
				*/
					if ($attend_status == CARD_STATUS_ABSENT) {
			  			$LateSession = $PlayTruantSession = $OfficalLeaveSession = 0;
			  			$RequestLeaveSession = 1;
			  			if(isset($Settings['DefaultNumOfSessionForAbsenteesism']) && $Settings['DefaultNumOfSessionForAbsenteesism'] != -1){
			  				$PlayTruantSession = $Settings['DefaultNumOfSessionForAbsenteesism'];
			  			}
			  			if(isset($Settings['DefaultNumOfSessionForLeave']) && $Settings['DefaultNumOfSessionForLeave'] != -1){
			  				$RequestLeaveSession = $Settings['DefaultNumOfSessionForLeave'];
			  			}
			  			
			  		}
			  		else if ($attend_status==CARD_STATUS_LATE) {
			  			$LateSession = 1;
			  			$RequestLeaveSession = $PlayTruantSession = $OfficalLeaveSession = 0;
			  			if(isset($Settings['DefaultNumOfSessionForLate']) && $Settings['DefaultNumOfSessionForLate'] != -1){
			  				$LateSession = $Settings['DefaultNumOfSessionForLate'];
			  			}
			  		}
			  		else {
			  			$LateSession = $RequestLeaveSession = $PlayTruantSession = $OfficalLeaveSession = 0;
			  		}
		  		//--- Henry Added 20131106 [end]
		  		}
		  		
		  		$selOfficalLeaveSession = "<select name=\"offical_leave_session[]\" id=\"offical_leave_session$i\"/>";
				$selLateSession = "<select name=\"late_session[]\" id=\"late_session$i\"/>";
				$selRequestLeaveSession = "<select name=\"request_leave_session[]\" id=\"request_leave_session$i\"/>";
				$selPlayTruantSession = "<select name=\"play_truant_session[]\" id=\"play_truant_session$i\"/>";
				/*for($k=0;$k<=20;$k++)
				{
					$LateSelected = ($k == $LateSession)? 'selected':'';
					$OfficalLeaveSelected = ($k == $OfficalLeaveSession)? 'selected':'';
					$RequestLeaveSelected = ($k == $RequestLeaveSession)? 'selected':'';
					$PlayTruantSelected = ($k == $PlayTruantSession)? 'selected':'';
					
					$selOfficalLeaveSession .= "<option value=\"$k\" ".$OfficalLeaveSelected." >".$k."</option>";
					$selLateSession .= "<option value=\"".$k."\" ".$LateSelected." >".$k."</option>";
					$selRequestLeaveSession .= "<option value=\"".$k."\" ".$RequestLeaveSelected." >".$k."</option>";
					$selPlayTruantSession .= "<option value=\"".$k."\" ".$PlayTruantSelected." >".$k."</option>";
				}*/
				$k=0.0;
				while($k<=CARD_STUDENT_MAX_SESSION){
					$LateSelected = ($k == $LateSession)? 'selected':'';
					$OfficalLeaveSelected = ($k == $OfficalLeaveSession)? 'selected':'';
					$RequestLeaveSelected = ($k == $RequestLeaveSession)? 'selected':'';
					$PlayTruantSelected = ($k == $PlayTruantSession)? 'selected':'';
					
					$selOfficalLeaveSession .= "<option value=\"$k\" ".$OfficalLeaveSelected." >".$k."</option>";
					$selLateSession .= "<option value=\"".$k."\" ".$LateSelected." >".$k."</option>";
					$selRequestLeaveSession .= "<option value=\"".$k."\" ".$RequestLeaveSelected." >".$k."</option>";
					$selPlayTruantSession .= "<option value=\"".$k."\" ".$PlayTruantSelected." >".$k."</option>";
					$k+= $sys_custom['SmartCardAttendance_StudentAbsentSessionStep'] > 0? $sys_custom['SmartCardAttendance_StudentAbsentSessionStep'] : 0.5;
				}
				$selOfficalLeaveSession .= "</select>\n";
				$selLateSession .= "</select>\n";
				$selRequestLeaveSession .= "</select>\n";
				$selPlayTruantSession .= "</select>\n";
				
				$x .= "<td>".$selLateSession."</td>";
				$x .= "<td>".$selRequestLeaveSession."</td>";
				$x .= "<td>".$selPlayTruantSession."</td>";
				$x .= "<td>".$selOfficalLeaveSession."</td>";
		  }
		  	if($sys_custom['SmartCardAttendance_StudentAbsentSession2']){
		  		if($AbsentSession == ""){
		  			$AbsentSession = 0;
		  			if ($attend_status == CARD_STATUS_ABSENT) {
			  			if(isset($Settings['DefaultNumOfSessionForAbsent']) && $Settings['DefaultNumOfSessionForAbsent'] != -1){
			  				$AbsentSession = $Settings['DefaultNumOfSessionForAbsent'];
			  			}
			  		}
		  		}
		  		$selAbsentSession = "<select name=\"absent_session[]\" id=\"absent_session$i\"/>";
		  		
		  		$k=0.0;
				while($k<=CARD_STUDENT_MAX_SESSION){
					$AbsentSelected = ($k == $AbsentSession)? 'selected':'';
					$selAbsentSession .= "<option value=\"$k\" ".$AbsentSelected." >".$k."</option>";
					$k+= $sys_custom['SmartCardAttendance_StudentAbsentSessionStep'] > 0 ? $sys_custom['SmartCardAttendance_StudentAbsentSessionStep'] : 0.5;
				}
				$selAbsentSession .= "</select>\n";
				$x .= "<td>".$selAbsentSession."</td>";
		  	}
		  	
		  	$waive_display = $attend_status==CARD_STATUS_PRESENT || $attend_status==CARD_STATUS_OUTING? 'style="display:none"' : '';
		  	$x .= '<td><input type="checkbox" class="WaiveBtn" name="Waived'.$i.'" id="Waived'.$i.'" value="1" '.(($waived == 1 || ($attend_status=="1" && $preset_waive == "1"))? 'checked':'').' '.$waive_display.'/></td>';
		  	$style_display = $attend_status==CARD_STATUS_PRESENT ? 'style="visibility:hidden"':'style="visibility:visible"';
		  	if ($hasWord)
		    {
		  		# Begin of Staff Input Reason Selection List
		  		$select_input_word = $linterface->GET_PRESET_LIST("getReasons($i)", '_'.$i, "input_reason".$i);
				# End of Staff Input Reason Selection List
		    }
		  	$input_reason = "<div id=\"div_reason_$i\" $style_display><input type=\"text\" class=\"tabletext\" id=\"input_reason$i\" name=\"input_reason$i\" style=\"background:#ffffff\" value=\"".htmlspecialchars($reason,ENT_QUOTES)."\">$select_input_word</div>";
		  	$x .= "<td nowrap>$input_reason</td>";
		  	$remark_input = $linterface->GET_TEXTBOX_NUMBER('TeacherRemark['.$i.']', 'TeacherRemark[]', $remark);
		  	if($hasTeacherRemarkWord)
		  	{
		  	    $remark_input .= $linterface->GET_PRESET_LIST("getTeacherRemarkReasons($i)", '_teacher_remark_'.$i, 'TeacherRemark['.$i.']');
		  	}
		  	$office_remark_display = $attend_status == CARD_STATUS_PRESENT ? array('style'=>"display:none;") : array();
		  	$office_remark_input = $linterface->GET_TEXTBOX_NUMBER('OfficeRemark'.$i, 'OfficeRemark[]', $office_remark,'', $office_remark_display);
		  	
			$x .= "<td nowrap>$remark_input</td>
					<td>".$office_remark_input."</td>";
		//}
		$x .= "
						<td>".$DateModified."</td>
						<td>".$ModifyName."</td>
						<td>".$ConfirmedUser."</td>
					</tr>\n";

	}
}
$x .= "</table>\n";
$table_attend = $x;


# check eDis LATE category
if($plugin['Disciplinev12']) {	# check whether eDis "Late" category is set to "Not In Use" , and with any GM Late record in Target Date

	include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
	$ldiscipline = new libdisciplinev12();
	
	$CanPassGmLateChecking = $ldiscipline->Check_Can_Pass_Gm_Late_Checking($TargetDate);
	
} else {
	$CanPassGmLateChecking = 1;
}


$toolbar = $linterface->GET_LNK_PRINT("javascript:openPrintPage()","","","","",0);

$table_tool .= "<td nowrap=\"nowrap\">
					<a href=\"javascript:AbsToPre()\" class=\"tabletool\">
						<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_import.gif\" alt=\"button_import\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">
						$i_StudentAttendance_Action_SetAbsentToOntime
					</a>
				</td>";

$TAGS_OBJ[] = array($i_SmartCard_DailyOperation_ViewGroupStatus, "", 0);
$MODULE_OBJ = $lc->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START(urldecode($Msg));

$PAGE_NAVIGATION[] = array($display_period,'group_status.php?DayType='.$DayType.'&TargetDate='.$TargetDate);
$PAGE_NAVIGATION[] = array($display_group_name);

if ($msg == 1) $SysMsg = $linterface->GET_SYS_MSG("add");
if ($msg == 2) $SysMsg = $linterface->GET_SYS_MSG("update");
?>
<?php if($sys_custom['StudentAttendance']['NoCardPhoto']){ ?>
<link rel="stylesheet" href="/templates/jquery/fancybox-1.3.4/jquery.fancybox-1.3.4.css" type="text/css" media="screen" />
<script type="text/javascript" src="/templates/jquery/fancybox-1.3.4/jquery.easing-1.3.pack.js"></script>
<script type="text/javascript" src="/templates/jquery/fancybox-1.3.4/jquery.mousewheel-3.0.4.pack.js"></script>
<script type="text/javascript" src="/templates/jquery/fancybox-1.3.4/jquery.fancybox-1.3.4.js"></script>
<?php } ?>
<div id="tooltip3" style='position:absolute; left:100px; top:30px; z-index:1; visibility: hidden'></div>
<script language="Javascript" src='/templates/tooltip.js'></script>
<?=$reason_js?>
<?=$teacher_remark_js?>
<script language="JavaScript" type="text/javascript">
// for Preset Absence
function showPresetAbs(obj,reason){
			var pos_left = getPostion(obj,"offsetLeft");
			var pos_top  = getPostion(obj,"offsetTop");
			
			offsetX = (obj==null)?0:obj.width;
			//offsetY = (obj==null)?0:obj.height;
			offsetY =0;
			objDiv = document.getElementById('tooltip3');
			if(objDiv!=null){
				objDiv.innerHTML = reason;
				objDiv.style.visibility='visible';
				objDiv.style.top = pos_top+offsetY+"px";
				objDiv.style.left = pos_left+offsetX+"px";
                $("#tooltip3").stop(true, true);
				setDivVisible(true, "tooltip3", "lyrShim");
			}

}
$(document).ready(function() {
    $("#tooltip3").hover(function() {
        $(this).stop(true).fadeTo(400, 1);
    }, function() {
        $("#lyrShim").hide();
        $("#tooltip3").fadeOut(400);
    });
});
// for preset absence
function hidePresetAbs(){
		obj = document.getElementById('tooltip3');
		//if(obj!=null)
		//	obj.style.visibility='hidden';
		//setDivVisible(false, "tooltip3", "lyrShim");
    $("#lyrShim").hide();
    $("#tooltip3").fadeOut(400);
}
function openPrintPage()
{
        newWindow("view_student_print.php?class_name=<?=$class_name?>&class_id=<?=$class_id?>&DayType=<?=$DayType?>&TargetDate=<?=$TargetDate?>",4);
}
function AbsToPre()
{
	obj = document.getElementsByName("drop_down_status[]");
  len=obj.length;
  var i=0;
  for( i=0 ; i<len ; i++) {
  	if (obj[i].selectedIndex == 1) {
  		obj[i].selectedIndex = 0; 
  	}
		
    ChangeDefaultSessionSelect(i);
  }
   setEditAllowed();
}

function ChangeDefaultSessionSelect(i) {
	var IsConfirmedAlready = document.getElementById('Confirmed['+i+']');
    var ignore_confirm_check = <?=(($sys_custom['SmartCardAttendance_StudentAbsentSessionToggleSessionIgnoreConfirm'] == true) ? 'true' : 'false')?>;
    if ((IsConfirmedAlready && IsConfirmedAlready.value == "") || ignore_confirm_check == true) {
		var drop_down_list = document.getElementsByName('drop_down_status[]');
		if(drop_down_list==null)return;
		var StatusObj = drop_down_list[i];
		var LateSessionObj = document.getElementById('late_session'+i);
		var OfficalLeaveSessionObj = document.getElementById('offical_leave_session'+i);
		var RequestLeaveSessionObj = document.getElementById('request_leave_session'+i);
		var PlayTruantSessionObj = document.getElementById('play_truant_session'+i);
		if (LateSessionObj) {
			if (StatusObj.selectedIndex == 1) {
				OfficalLeaveSessionObj.selectedIndex = 0;
				LateSessionObj.selectedIndex = 0;
				//RequestLeaveSessionObj.selectedIndex = 2;
				//PlayTruantSessionObj.selectedIndex = 0;
				<?if($sys_custom['SmartCardAttendance_StudentAbsentSession'] && isset($Settings['DefaultNumOfSessionForLeave']) && $Settings['DefaultNumOfSessionForLeave'] != -1){?>
					RequestLeaveSessionObj.selectedIndex = <?=($Settings['DefaultNumOfSessionForLeave']*2)?>;
				<?}else{?>
					RequestLeaveSessionObj.selectedIndex = 2;
				<?}?>
				<?if($sys_custom['SmartCardAttendance_StudentAbsentSession'] && isset($Settings['DefaultNumOfSessionForAbsenteesism']) && $Settings['DefaultNumOfSessionForAbsenteesism'] != -1){?>
					PlayTruantSessionObj.selectedIndex = <?=($Settings['DefaultNumOfSessionForAbsenteesism']*2)?>;
				<?}else{?>
					PlayTruantSessionObj.selectedIndex = 0;
				<?}?>
			}
			else if (StatusObj.selectedIndex == 2) {
				OfficalLeaveSessionObj.selectedIndex = 0;
				//LateSessionObj.selectedIndex = 2;
				<?if($sys_custom['SmartCardAttendance_StudentAbsentSession'] && isset($Settings['DefaultNumOfSessionForLate']) && $Settings['DefaultNumOfSessionForLate'] != -1){?>
					LateSessionObj.selectedIndex = <?=($Settings['DefaultNumOfSessionForLate']*2)?>;
				<?}else{?>
					LateSessionObj.selectedIndex = 2;
				<?}?>
				RequestLeaveSessionObj.selectedIndex = 0;
				PlayTruantSessionObj.selectedIndex = 0;
			}
			else {
				OfficalLeaveSessionObj.selectedIndex = 0;
				LateSessionObj.selectedIndex = 0;
				RequestLeaveSessionObj.selectedIndex = 0;
				PlayTruantSessionObj.selectedIndex = 0;
			}
		}
		<?php if($sys_custom['SmartCardAttendance_StudentAbsentSession2']){ ?>
		var AbsentSessionObj = document.getElementById('absent_session'+i);
		if(AbsentSessionObj){
			AbsentSessionObj.selectedIndex = 0;
			if(StatusObj.selectedIndex == 1) {
				<?php if(isset($Settings['DefaultNumOfSessionForAbsent']) && $Settings['DefaultNumOfSessionForAbsent'] != -1){ ?>
				AbsentSessionObj.selectedIndex = <?=($Settings['DefaultNumOfSessionForAbsent']*2)?>;
				<?php } ?>
			}
		}
		<?php } ?>
	}
}

function Switch_Group(ToClass) {
	$('#class_id').val(ToClass);
	
	document.getElementById('form1').submit();
}

function Switch_Period($radioValue) {

	// check if radio button is checked
	$previousID = $('#radio-previous').val();
	
	if(($previousID == '0' && $radioValue.indexOf('checked') == -1)||($previousID != '0' && $radioValue != $previousID)){
		$('#radio-previous').val($radioValue);
		$('#DayType').val($radioValue);
		document.getElementById('form1').submit();
	}
}

function Check_Form(obj, url, msg) {
	<? if(!$CanPassGmLateChecking) { ?>
		alert('<?=$Lang['eAttendance']['eDisciplineCategoryNotInUseWarning']?>');
		return false;
	<? 
	} else {
		echo "AlertPost(obj,url,msg);\n";	
	}
	?>
	
}


function getReasons(pos)
{
         var jArrayTemp = new Array();
             obj2 = document.getElementById("drop_down_status_"+pos);
             if (obj2.selectedIndex == 1)
             {
                 return jArrayWordsAbsence;
             }
             else if (obj2.selectedIndex == 2)
             {
                  return jArrayWordsLate;
             }
             else if (obj2.selectedIndex == 3)
             {
                  return jArrayWordsOuting;
             }
             else return jArrayTemp;
}

function getTeacherRemarkReasons()
{
	return jArrayWordsTeacherRemark;
}

function putBack(obj, value)
{
         obj.value = value;
         hideMenu('ToolMenu');
}
function clearReason(i){
	if (document.getElementById('input_reason'+i)) {
		var CurStatus = document.getElementById('drop_down_status_'+i).value;
		var PrevStatus = document.getElementById('PrevStatus['+i+']').value;
		
		if (PrevStatus == 1) {
			document.getElementById('AbsentDefaultReason['+i+']').value = document.getElementById('input_reason'+i).value;
		}
		else if (PrevStatus == 2) {
			document.getElementById('LateDefaultReason['+i+']').value = document.getElementById('input_reason'+i).value;
		}
		else if (PrevStatus == 3) {
			document.getElementById('OutingDefaultReason['+i+']').value = document.getElementById('input_reason'+i).value;
		}
		
		if (CurStatus == 1) { // absent
			document.getElementById('input_reason'+i).value = document.getElementById('AbsentDefaultReason['+i+']').value;
		}
		else if (CurStatus == 2) { // late
			document.getElementById('input_reason'+i).value = document.getElementById('LateDefaultReason['+i+']').value;
		}
		else if (CurStatus == 3) { // Outing
			document.getElementById('input_reason'+i).value = document.getElementById('OutingDefaultReason['+i+']').value;
		} 
		
		if (CurStatus == 0)	// On Time
		{
			document.getElementById('div_reason_'+i).style.visibility='hidden';
		}
		else
		{
			document.getElementById('div_reason_'+i).style.visibility='visible';
		}
		
		document.getElementById('PrevStatus['+i+']').value = CurStatus;
	}
}

function setEditAllowed(i){
	var drop_down_list = document.getElementsByName('drop_down_status[]');
	if(drop_down_list==null)return;
	
	var i = i || "";
	if (i == "") {
		var LoopStart = 0;
		var LoopTo = drop_down_list.length;
	}
	else {
		var LoopStart = i;
		var LoopTo = (i+1);
	}
  for(i=LoopStart;i<LoopTo;i++){
    var statusObj = drop_down_list[i];
    var statusVal = $(statusObj).val();

	if (document.getElementById('Waived'+i)) {
			if(statusVal != '<?=CARD_STATUS_PRESENT?>' && statusVal!='<?=CARD_STATUS_OUTING?>'){
				document.getElementById('Waived'+i).style.display = "";
			}
			else 
				document.getElementById('Waived'+i).style.display = "none";
	}
    if (document.getElementById('input_reason'+i))
	{
      inputReasonObj = document.getElementById('input_reason'+i)
      inputReasoniconObj = document.getElementById('posimg_'+i);
      if(statusVal != '<?=CARD_STATUS_PRESENT?>'){
        inputReasonObj.style.display="";
        if (inputReasoniconObj)
			inputReasoniconObj.style.display="";
      }else{
        inputReasonObj.style.display="none";
        if (inputReasoniconObj)
					inputReasoniconObj.style.display="none";
      }
    }
	
    if(document.getElementById('listContent'))
    {
    	obj2 = document.getElementById('listContent').style.display='none';
    	setDivVisible(false, 'listContent', 'lyrShim');
	}
    
    <?if($sys_custom['SmartCardAttendance_StudentAbsentSession']){?>
    	ChangeDefaultSessionSelect(i);
    <?}?>
    
	var office_remark_obj = $('#OfficeRemark'+i);
	if(office_remark_obj.length>0){
		statusVal != '<?=CARD_STATUS_PRESENT?>'? office_remark_obj.show(): office_remark_obj.hide();
	}
  }
}

function SetAllAttend()
{
	var apply_all_value = $('#drop_down_status_all').val();
	var status_selections = document.getElementsByName('drop_down_status[]');
	for(var i=0;i<status_selections.length;i++){
		$(status_selections[i]).val(apply_all_value);
		clearReason(i);
		setEditAllowed(i);
		ChangeDefaultSessionSelect(i);
	}
}
<?php if($sys_custom['StudentAttendance']['AllowEditTime']){ ?>
function onRevealEditTime(uid, time)
{
	var text_field = '<input type="text" id="InSchoolTime'+uid+'" name="InSchoolTime'+uid+'" value="'+time+'" size="10" onchange="onEditTimeChanged('+uid+')" />';
	
	var text_obj = $('#InSchoolTime'+uid);
	if(text_obj && text_obj.length > 0)
	{
		text_obj.val(time);
	}else{
		$('#cancelTimeBtn'+uid).before(text_field);
	}
	
	$('#InSchoolTime_Display'+uid).hide();
	$('#InSchoolTime_Edit'+uid).show();
}

function onCancelEditTime(uid)
{
	$('#InSchoolTime_Display'+uid).show();
	$('#InSchoolTime_Edit'+uid).hide();
	$('#InSchoolTime'+uid).remove();
}

function onEditTimeChanged(uid)
{
	var obj = $('#InSchoolTime'+uid);
	var val = $.trim(obj.val());
	if(val != ''){
		if(!val.match(/^\d\d:\d\d:\d\d$/g)){
			obj.val('');
			return;
		}
		var parts = val.split(':');
		var hr = Math.max(0, Math.min(24, parseInt(parts[0])));
		var min = Math.max(0, Math.min(60, parseInt(parts[1])));
		var sec = Math.max(0, Math.min(60, parseInt(parts[2])));
		
		var val_str = hr < 10? '0' + hr : '' + hr;
		val_str += ':';
		val_str += min < 10? '0' + min : '' + min;
		val_str += ':';
		val_str += sec < 10? '0' + sec : '' + sec;
		obj.val(val_str);
	}
}
<?php } ?>
<?php if($sys_custom['StudentAttendance']['NoCardPhoto']){ ?>
$(document).ready(function(){
	$('a.fancybox').fancybox();
});
<?php } ?>
</script>

<form name="form1" id="form1" method="post" action="" method="post">
<?php // data expired error message
	if($error==1){
		echo "<table width=560 border=0 cellpadding=0 cellspacing=0 align='center'>";
		echo "<tr><td align=right><font color=red>".$i_StudentAttendance_Warning_Data_Outdated."</font></td></tr>";
		echo "</table>";
	}
?>

<iframe id='lyrShim' src='javascript:false;' scrolling='no' frameborder='0' style='position:absolute; top:0px; left:0px; display:none;'></iframe>
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	</tr>
	<tr>
		<td>
			<?=$table_confirm?>
		</td>
	</tr>
	<tr>
		<td>
			<?=$ShortCutTable?>
		</td>
	</tr>
	<tr>
		<td>
			<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
	    	<tr>
        	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
        </tr>
	    </table>	
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>
			<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" onMouseMove="overhere()">
				<tr>
					<td align="left"><?= $toolbar ?></td>
					<td align="right"><?= $SysMsg ?></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
				<tr class="table-action-bar">
					<td valign="bottom" align="left">
						<table border=0>
							<tr>
								<td class="attendance_late" width="15px">
									&nbsp;
								</td>
								<td class="tabletext">
									<?=$Lang['StudentAttendance']['Late']?>
								</td>
								<td class="attendance_norecord" width="15px">
									&nbsp;
								</td>
								<td class="tabletext">
									<?=$Lang['StudentAttendance']['Absent']?>
								</td>
								<td class="attendance_outing" width="15px">
									&nbsp;
								</td>
								<td class="tabletext">
									<?=$Lang['StudentAttendance']['Outgoing']?>
								</td>
								<td class="attendance_not_confirmed" width="15px">
									&nbsp;
								</td>
								<td class="tabletext">
									<?=$Lang['StudentAttendance']['UnConfirmed']?>
								</td>
							</tr>
						</table>
					</td>
					<td valign="bottom" align="right">
						<table border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td width="21"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_01.gif" width="21" height="23"></td>
								<td background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_02.gif">
									<table border="0" cellspacing="0" cellpadding="2">
										<tr>
											<?=$table_tool?>
										</tr>
									</table>
								</td>
								<td width="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_03.gif" width="6" height="23"></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<?= $table_attend ?>
		</td>
	</tr>
	<tr>
		<td>
			<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
		    	<tr>
                	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
		    </table>	
		</td>
	</tr>
	<tr>
		<td>
			<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
		    	<tr>
				    <td align="center" colspan="2">
						<?= $linterface->GET_ACTION_BTN($button_save, "button", "Check_Form(document.form1,'view_student_update.php','$i_SmartCard_Confirm_Update_Attend?')") ?>
						<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
						<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:window.location='group_status.php?DayType=$DayType&TargetDate=$TargetDate'","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>

<input id=class_name name=class_name type=hidden value="<?=$class_name?>">
<input id="class_id" name="class_id" type="hidden" value="<?=$class_id?>">
<input id=DayType name=DayType type=hidden value="<?=$DayType?>">
<input name=PageLoadTime type=hidden value="<?=time()+1?>">
<input name=confirmed_id type=hidden value="<?=$confirmed_id?>">
<input id=TargetDate name=TargetDate type=hidden value="<?=$TargetDate?>">
<input id="radio-previous" name="radio-previous" type="hidden" value='0'/>

</form>
<br><br>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>