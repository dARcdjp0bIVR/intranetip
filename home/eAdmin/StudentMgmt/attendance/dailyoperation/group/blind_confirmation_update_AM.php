<?php
include_once("../../../../includes/global.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libclass.php");
include_once("../../../../includes/libcardstudentattend2.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
//include_once("../../../../templates/adminheader_setting.php");
include_once("../../../../includes/libdiscipline.php");
intranet_opendb();

### Set Date from previous page
$ts_record = strtotime($TargetDate);
if ($ts_record == -1)
{
    $ts_record = strtotime(date('Y-m-d'));
}
$txt_year = date('Y',$ts_record);
$txt_month = date('m',$ts_record);
$txt_day = date('j',$ts_record);
$day_of_week = date('w',$ts_record);

# create confirm log table if not exists
$lcardattend = new libcardstudentattend2();
$lcardattend->retrieveSettings();
$lcardattend->createTable_LogAndConfirm($txt_year, $txt_month);
$daily_log_table_name="CARD_STUDENT_DAILY_LOG_".$txt_year."_".$txt_month;
$attendanceMode = $lcardattend->attendance_mode;
$year = getCurrentAcademicYear();
$semester = getCurrentSemester();
$LIDB = new libdb();


###period
switch ($period)
{
        case "1": $DayType = 2;break;
        case "2": $DayType = 3;break;
        default : $DayType = 2;break;
}


# select classes with non school day on TargetDate from CARD_STUDENT_SPECIFIC_DATE_TIME
	$sqlSpecial = "SELECT ClassID,IF(NonSchoolDay=1,1,2) FROM CARD_STUDENT_SPECIFIC_DATE_TIME WHERE RecordDate='$TargetDate'";
	$temp = $lcardattend->returnArray($sqlSpecial,2);
	for($i=0;$i<sizeof($temp);$i++){
			$resultSpecial[$temp[$i][0]]=$temp[$i][1];
	}
	//echo "<p>specialMode=$sqlSpecial</p>";

# select classes with non school day on Target Cycle Day from CARD_STUDENT_CLASS_PERIOD_TIME
  $sqlClassCycle ="select a.ClassID,IF(NonSchoolDay=1,1,2) from CARD_STUDENT_CLASS_PERIOD_TIME AS a, INTRANET_CYCLE_DAYS AS b WHERE a.DayType=2 and a.DayValue=b.TextShort AND b.RecordDate='$TargetDate'";
	$temp=$lcardattend->returnArray($sqlClassCycle,2);
	for($i=0;$i<sizeof($temp);$i++){
			$resultClassCycle[$temp[$i][0]]=$temp[$i][1];
	}
	//echo "<p>cycle=$sqlClassCycle</p>";

# select classes with non school day on Target Cycle Day from CARD_STUDENT_PERIOD_TIME
  $sqlSchoolCycle ="select IF(a.NonSchoolDay=1,1,2) from CARD_STUDENT_PERIOD_TIME AS a,INTRANET_CYCLE_DAYS AS b WHERE a.DayType=2 and a.DayValue=b.TextShort AND b.RecordDate='$TargetDate'";
	$resultSchoolCycle=$lcardattend->returnVector($sqlSchoolCycle);
	//echo "<p>cycle=$sqlSchoolCycle</p>";

# select classes with non school day on Target Week Day from CARD_STUDENT_PERIOD_TIME
  $sqlSchoolWeek ="select IF(NonSchoolDay=1,1,2) from CARD_STUDENT_PERIOD_TIME WHERE DayType=1 and DayValue='$day_of_week'";
	$resultSchoolWeek = $lcardattend->returnVector($sqlSchoolWeek);
	//echo "<p>week=$sqlSchoolWeek</p>";
	
# select classes with non school day on Target Week Day from CARD_STUDENT_CLASS_PERIOD_TIME
  $sqlClassWeek ="select ClassID,IF(NonSchoolDay=1,1,2) from CARD_STUDENT_CLASS_PERIOD_TIME WHERE DayType=1 and DayValue='$day_of_week'";
	$temp=$lcardattend->returnArray($sqlClassWeek,2);
	for($i=0;$i<sizeof($temp);$i++){
			$resultClassWeek[$temp[$i][0]]=$temp[$i][1];
	}
	//echo "<p>week=$sqlClassWeek</p>";
	
	
# select the timetable mode for each class
	$resultClassMode = $lcardattend->getClassListMode();
	$confirmCount=0;
	$sqlStudentList = "SELECT c.UserID,e.ClassID,e.ClassName,d.RecordID FROM INTRANET_USER AS c LEFT JOIN $daily_log_table_name AS d ON(c.UserID=d.UserID AND d.DayNumber='$txt_day'),INTRANET_CLASS AS e WHERE d.AMStatus IS NULL AND c.ClassName=e.ClassName";
	$sqlOutingList = "SELECT c.UserID FROM INTRANET_USER AS c LEFT JOIN $daily_log_table_name AS d ON(c.UserID=d.UserID AND d.DayNumber='$txt_day'),INTRANET_CLASS AS e,CARD_STUDENT_OUTING as o WHERE d.AMStatus IS NULL AND c.ClassName=e.ClassName AND o.UserID=c.UserID";

	for($i=0;$i<sizeof($resultClassMode);$i++){
		$off=false;
		$done=false;
		$classID = $resultClassMode[$i][0];
		$className = $resultClassMode[$i][1];
		$classMode = $resultClassMode[$i][2];
		$specialClassID= $classMode==0?0:$classID;
		# check if NonSchoolDay for Speical Date
		if($resultSpecial[$specialClassID]==1){
				//echo "<p>speical date off=$className</p>";
				$off=true;
				$done=true;
		}else if($resultSpecial[$specialClassID]==2){
				//echo "<p>speical date on=$className</p>";
				$off=false;
				$done=true;
		}
		# check if NonSchoolDay for Cycle Day 
		if(!$done){
					if($classMode == 1){ # Class Cycle Day
							if($resultClassCycle[$classID]==1){
									//echo "<p>class cycle date off=$className</p>";
									$off = true;
									$done= true;
							}else if($resultClassCycle[$classID]==2){
									//echo "<p>class cycle date on=$className</p>";
									$off = false;
									$done= true;
							}
					}else if($classMode !=2){  # School Cycle Day
							if(is_array($resultSchoolCycle) &&$resultSchoolCycle[0]==1){
											//echo "<p>school cycle date off=$className</p>";
											$off = true;
											$done = true;
							} else if($resultSchoolCycle[0]==2){
											//echo "<p>school cycle date on=$className</p>";
											$off = false;
											$done = true;
							}
					} 
		}
		# check if NonSchoolDay for Week Day
		if(!$done){
				if($classMode==1){ # Class Week Day
							if($resultClassWeek[$classID]==1){
									//echo "<p>class week date off=$className</p>";
									$done = true;
									$off = true;
							}else if($resultClassWeek[$classID]==2){
									//echo "<p>class week date on=$className</p>";
									$done = true;
									$off = false;
							}


				}else if($classMode!=2){ # School Week Day
							if(is_array($resultSchoolWeek) && $resultSchoolWeek[0]==1){
											//echo "<p>school week date off=$className</p>";		
											$off = true;
											$done=true;
							} else 	if($resultSchoolWeek[0]==2){
											//echo "<p>school week date on=$className</p>";		
											$off = false;
											$done=true;
							} 

				}
		}
		if($classMode==2 || $off){
				$sqlStudentList.=" AND e.ClassID<>'$classID'";
				$sqlOutingList.=" AND e.ClassID<>'$ClassID'";
		}else{
				$confirmClasses[$confirmCount]['ClassID']=$classID;
				$confirmClasses[$confirmCount++]['ClassName']=$className;
		}
	}
  $sqlStudentList.=" AND e.RecordStatus=1 AND c.RecordType=2 AND c.RecordStatus IN(0,1) AND c.ClassName IS NOT NULL";
  $sqlOutingList.=" AND e.RecordStatus=1 AND c.RecordType=2 AND c.RecordStatus IN(0,1) AND c.ClassName IS NOT NULL AND o.RecordDate='$TargetDate'";
	
	# select the students who 
		# 1. are not in the daily log table on TargetDate or who's AMStatus is NULL and
		# 2. who is belong to the class	which needs to take attendance on TargetDate
	$absentStudentList = $lcardattend->returnArray($sqlStudentList,4);
//	echo "<p>$sqlStudentList</p>";
//	echo "<p>$sqlOutingList</p>";
	
### for student confirm
$outingStudentList = $lcardattend->returnVector($sqlOutingList,1);
for($i=0; $i<sizeOf($absentStudentList); $i++)
{
        $my_user_id = $absentStudentList[$i][0];
  			$my_record_id= $absentStudentList[$i][3];
        $my_day = $txt_day;

        if(is_array($outingStudentList)&& in_array($my_user_id,$outingStudentList))
		        $my_status = CARD_STATUS_OUTING;
		    else $my_status= CARD_STATUS_ABSENT;

        if( $period == "1")            # AM
        {
                # insert if not exist
                if($my_record_id=="")
                {
                    
                        $sql = "INSERT INTO $daily_log_table_name
                                                        (UserID,DayNumber,AMStatus,DateInput,DateModified) VALUES
                                                        ($my_user_id,$my_day,$my_status,NOW(),NOW())
                                                ";
                        //echo "<p>$sql</p>";
                        $LIDB->db_db_query($sql);
                }
                else  # update if exist
                {
                		$sql ="UPDATE $daily_log_table_name SET AMStatus = $my_status WHERE RecordID = $my_record_id";
                		$LIDB->db_db_query($sql);
                		//echo "<p>$sql</p>";
                }
        }
}

### for class confirm
$card_student_daily_class_confirm = "CARD_STUDENT_DAILY_CLASS_CONFIRM_".$txt_year."_".$txt_month;

$confirmed_user_id = -1;

for($i=0;$i<sizeof($confirmClasses);$i++){
		$class_id = $confirmClasses[$i]['ClassID'];
		$class_name = $confirmClasses[$i]['ClassName'];	
		$sql = "SELECT
						a.RecordID
	          FROM $card_student_daily_class_confirm as a
	               LEFT OUTER JOIN INTRANET_CLASS as b ON (a.ClassID=b.ClassID)
	               WHERE b.ClassName = \"$class_name\" AND a.DayNumber = ".$txt_day." AND a.DayType = $DayType";
		$result = $LIDB->returnArray($sql,1);
		$confirmed_id = $result[0][0];
		if( $confirmed_id <> "" )
		{
		        # update if record exist
		        $sql = "UPDATE $card_student_daily_class_confirm SET ConfirmedUserID=$confirmed_user_id, DateModified = NOW() WHERE RecordID = '$confirmed_id'";
		        //echo "<p>$sql</p>";
		        $LIDB->db_db_query($sql);
		        $msg = 2;
		}
		else
		{
		        # insert if record not exist
	
		        $sql = "INSERT INTO $card_student_daily_class_confirm
		                                        (
		                                                ClassID,
		                                                ConfirmedUserID,
		                                                DayNumber,
		                                                DayType,
		                                                DateInput,
		                                                DateModified
		                                        ) VALUES
		                                        (
		                                                '$class_id',
		                                                '$confirmed_user_id',
		                                                '$txt_day',
		                                                '$DayType',
		                                                NOW(),
		                                                NOW()
		                                        )
		                                        ";
		        $LIDB->db_db_query($sql);
		        //echo "<p>$sql</p>";
		        $msg = 1;
		}
}

$return_url = "class_status.php?TargetDate=$TargetDate&period=$period&msg=$msg";

header( "Location: $return_url");

?>


