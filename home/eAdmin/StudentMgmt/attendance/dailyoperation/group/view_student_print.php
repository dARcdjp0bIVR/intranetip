<?php
//ini_set('display_errors',1);
//error_reporting(E_ALL ^ E_NOTICE);
// using by 
##################################### Change Log ###############################################
# 2018-11-22 by Carlos: Modified to make Office Remark to follow the take attendance page fetch logic.
# 2018-06-22 by Carlos: Display group name to follow UI language.
# 2015-02-13 by Carlos: [ip2.5.6.3.1] Added [Waived], [Reason], [Office Remark]. 
# 2014-07-17 by Bill:	Add student count
# 2013-04-16 by Carlos: Add CARD_STUDENT_DAILY_REMARK.DayType
# 2010-02-09 by Carlos: Add setting Default PM status not to follow AM status
################################################################################################
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");

intranet_auth();
intranet_opendb();

$GeneralSetting = new libgeneralsettings();
$SettingList[] = "'DefaultAttendanceStatus'";
$SettingList[] = "'PMStatusNotFollowAMStatus'";
$Settings = $GeneralSetting->Get_General_Setting('StudentAttendance',$SettingList);
$student_attend_default_status = ($Settings['DefaultAttendanceStatus'] == "")? CARD_STATUS_ABSENT:$Settings['DefaultAttendanceStatus'];

$header_filepath = "$intranet_root/templates/reportheader/viewstudentattendance.php";
$header_inclusion = is_file($header_filepath);

### internal function
function printHeader($period_slot,$target_date,$ClassName, $HasPresetRecord){
        global $intranet_root, $i_StudentAttendance_Menu_DailyOperation;
        global $i_StudentAttendance_System, $i_StudentAttendance_ViewTodayRecord;
        global $i_StudentAttendance_Slot, $i_StudentAttendance_Slot_AM, $i_StudentAttendance_Slot_PM, $i_StudentAttendance_Slot_AfterSchool;
        global $i_StudentAttendance_View_Date, $i_StudentAttendance_ToSchoolTime;
        global $i_GroupName, $i_UserStudentName, $i_SmartCard_Frontend_Take_Attendance_In_School_Time, $i_StudentAttendance_Status;
        global $header_inclusion,$header_filepath;
        global $i_Attendance_Remark, $i_Attendance_Reason, $i_SmartCard_Frontend_Take_Attendance_Waived;
        global $i_Payment_Receipt_Payment_StaffInCharge_Signature;
        global $i_SmartCard_Frontend_Take_Attendance_CancelLunch;
		global $Lang, $sys_custom;
		
        if ($header_inclusion)
        {
            include($header_filepath);
        }
        $pageheader = "";
        $pageheader .= "<table width='560' border='0'>";
        $pageheader .= "<tr><td colspan='2'><u>$i_StudentAttendance_System ($i_StudentAttendance_Menu_DailyOperation)</u></td></tr>";

        $pageheader .= "<tr><td>$i_GroupName</td><td align=left>$ClassName</td></tr>";
        $pageheader .= "<tr><td>$i_StudentAttendance_Slot</td><td>";
        switch ($period_slot)
        {
                        case PROFILE_DAY_TYPE_AM: $pageheader .= "$i_StudentAttendance_Slot_AM"; break;
                        case PROFILE_DAY_TYPE_PM: $pageheader .= "$i_StudentAttendance_Slot_PM"; break;
                        default : $pageheader .= ""; break;
        }
        $pageheader .= "</tr>";
        $pageheader .= "<tr><td>$i_StudentAttendance_View_Date</td><td>$target_date</td></tr>";
        $pageheader .= "</table>";

        if ($sys_custom['SmartCardAttendance_LunchBoxOption'])
        {
            $LunchBoxOption_header ="<td width=1%>$i_SmartCard_Frontend_Take_Attendance_CancelLunch</td>";
        }
        
        $pageheader .= "<table width=100% border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0>";
        $pageheader .= "<tr class='tableTitle'><td>#</td>";
		$pageheader .= "<td>$i_UserStudentName</td>";
		$pageheader .= "<td>$i_SmartCard_Frontend_Take_Attendance_In_School_Time</td>";
		$pageheader .= "<td>$i_StudentAttendance_Status</td>";
		$pageheader .= ($HasPresetRecord>0?"<td>".$Lang['SmartCard']['StudentAttendance']['PresetAbsenceInfo']."</td>":"");
		$pageheader .= "<td>$i_SmartCard_Frontend_Take_Attendance_Waived</td>";
        $pageheader .= "<td>$i_Attendance_Reason</td>";
		$pageheader .= "<td>".$Lang['StudentAttendance']['iSmartCardRemark']."</td>";
		$pageheader .= "<td>".$Lang['StudentAttendance']['OfficeRemark']."</td>";
		$pageheader .= "$LunchBoxOption_header</tr>\n";
        echo $pageheader;
        #return $pageheader;
}

### class used
$li = new libdb();
$lcardattend = new libcardstudentattend2();

$libgroup = new libgroup($class_id);
$display_group_name = Get_Lang_Selection($libgroup->TitleChinese, $libgroup->Title);

$page_breaker = "<P CLASS='breakhere'>";
$pagefooter = "</table>";
$prevClass = "";

$i_title = "$i_StudentAttendance_System ($i_StudentAttendance_Menu_DailyOperation)";
include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_header.php");
//include_once("../../../../templates/fileheader.php");
?>
<STYLE TYPE="text/css">
     P.breakhere {page-break-before: always}
</STYLE>

<?
### Set Date from previous page
$ts_record = strtotime($TargetDate);
if ($ts_record == -1)
{
    $ts_record = strtotime(date('Y-m-d'));
}
$txt_year = date('Y',$ts_record);
$txt_month = date('m',$ts_record);
$txt_day = date('d',$ts_record);

// get default status from basic settings
$student_attend_default_status = trim(get_file_content("$intranet_root/file/studentattend_default_status.txt"));
$student_attend_default_status = ($student_attend_default_status == "")? CARD_STATUS_ABSENT:$student_attend_default_status;

### build table
$card_log_table_name = "CARD_STUDENT_DAILY_LOG_".$txt_year."_".$txt_month;

if ($lcardattend->EnableEntryLeavePeriod) {
	$FilterInActiveStudent .= "
        INNER JOIN 
        CARD_STUDENT_ENTRY_LEAVE_PERIOD as selp 
        on a.UserID = selp.UserID 
        	AND 
        	'".$TargetDate."' between selp.PeriodStart and CONCAT(selp.PeriodEnd,' 23:59:59') 
        ";
}

/*
$conds = "";

if($class_name<>"")
{
        $conds = " AND a.ClassName = \"$class_name\" ";
}
*/
$db_status_field = ($DayType==PROFILE_DAY_TYPE_PM?$lcardattend->Get_PM_Expected_Field("b.","c.","f."):$lcardattend->Get_AM_Expected_Field("b.","c.","f."));

if ($option == 1)
{
}
else if ($option == 2)
{
     $conds .= " AND $db_status_field = ".CARD_STATUS_ABSENT;
}
else if ($option == 3)
{
     $conds .= " AND $db_status_field = ".CARD_STATUS_LATE;
}
else if ($option == 4)
{
     $conds .= " AND $db_status_field in ('".CARD_STATUS_LATE."','".CARD_STATUS_ABSENT."')";
}

if($DayType==PROFILE_DAY_TYPE_AM)        // AM
{
	$expect_field = $lcardattend->Get_AM_Expected_Field("b.","c.","f.");
	$AbsentExpectedReasonField = $lcardattend->Get_Expected_Reason($DayType,PROFILE_TYPE_ABSENT,'j.',"","d.","f.");
	$LateExpectedReasonField = $lcardattend->Get_Expected_Reason($DayType,PROFILE_TYPE_LATE,'j.');
	$OutingExpectedReasonField = $lcardattend->Get_Expected_Reason($DayType,CARD_STATUS_OUTING,'j.',"","d.","c.",true);
	
	$StatusField = "AMStatus";
	
	$office_remark_field = "IF(j.RecordID IS NOT NULL,j.OfficeRemark,IF(TRIM(f.Remark)!='',f.Remark,c.Detail)) as OfficeRemark ";
	
	$sql  = "SELECT        
						".getNameFieldByLang("a.")." as name,
	          a.ClassName,
	          a.ClassNumber,
	          IF(b.InSchoolTime IS NULL, '-', b.InSchoolTime),
	          IF(b.InSchoolStation IS NULL, '-', b.InSchoolStation),
	          ".$lcardattend->Get_AM_Expected_Field("b.","c.","f.").", 
						IF(d.Remark IS NULL, '-',d.Remark),
						a.UserID,
						IF(f.Reason IS NULL,'',f.Reason),
						IF(f.Remark IS NULL,'',f.Remark),
				if (".$expect_field." = '".CARD_STATUS_ABSENT."',
				".$AbsentExpectedReasonField.",
				IF(".$expect_field." = '".CARD_STATUS_LATE."',
					".$LateExpectedReasonField.",
					IF(".$expect_field." = '".CARD_STATUS_OUTING."',
						".$OutingExpectedReasonField.",
						j.Reason))) as Reason,
			j.RecordStatus as Waived,
			f.Waive as PresetWaive,
			$office_remark_field,
			".$AbsentExpectedReasonField." as DefaultLateReason,
			".$LateExpectedReasonField." as DefaultAbsentReason, 
			".$OutingExpectedReasonField." as DefaultOutingReason 
	        FROM
	        	INTRANET_USER AS a 
				".$FilterInActiveStudent ." 
	        	inner join 
						INTRANET_USERGROUP AS x 
						on 
							x.GROUPID = '$class_id' and 
							a.UserID = x.UserID and 
							a.RecordType=2 AND
							a.RecordStatus IN (0,1,2)
	          LEFT OUTER JOIN $card_log_table_name AS b
	          ON	(
	          	a.UserID=b.UserID AND
	          	(b.DayNumber = ".$txt_day." ||  b.DayNumber IS NULL)
	            )
	          LEFT OUTER JOIN CARD_STUDENT_OUTING AS c
	          ON (a.UserID=c.UserID AND c.RecordDate = '$TargetDate' AND c.DayType = '".$DayType."')
	          LEFT OUTER JOIN CARD_STUDENT_DAILY_REMARK AS d ON(a.UserID=d.StudentID AND d.RecordDate='$TargetDate' AND d.DayType='$DayType')
	          LEFT OUTER JOIN CARD_STUDENT_PRESET_LEAVE AS f ON (a.UserID = f.StudentID AND f.DayPeriod=2 AND f.RecordDate='$txt_year-$txt_month-$txt_day') 
				LEFT JOIN CARD_STUDENT_PROFILE_RECORD_REASON as j
						ON j.StudentID = a.UserID 
							AND j.RecordDate = '".$TargetDate."' 
							AND j.DayType = '".$DayType."' 
							AND 
							(
							j.RecordType = b.".$StatusField." 
							OR  
							(j.RecordType = '".PROFILE_TYPE_ABSENT."' 
							AND b.".$StatusField." = '".CARD_STATUS_OUTING."' 
							)
							)
						".$AMJoinTable." 
	        WHERE
	        	(b.DayNumber = ".$txt_day." ||  b.DayNumber IS NULL) 
					order by a.classname asc, a.classnumber+0 asc";
								
	### check if Preset Absence exists
	$sqlPresetCount="SELECT COUNT(*) 
					FROM 
						INTRANET_USER AS A ,
						CARD_STUDENT_PRESET_LEAVE AS B,
						INTRANET_USERGROUP AS C
					WHERE 
						C.GROUPID = '$class_id' AND
						C.USERID = A.USERID AND
						A.RECORDTYPE=2 AND 
						A.RECORDSTATUS IN(0,1,2) AND 
						A.USERID = B.STUDENTID  AND
						B.RECORDDATE='$txt_year-$txt_month-$txt_day' AND 
						B.DayPeriod=2";
}
else if($DayType==PROFILE_DAY_TYPE_PM)         //PM
{      
	$expect_field = "IF(b.PMStatus IS NOT NULL,
												b.PMStatus,
												".$lcardattend->Get_PM_Expected_Field("b.","c.","f.").")";
	$AbsentExpectedReasonField = $lcardattend->Get_Expected_Reason($DayType,PROFILE_TYPE_ABSENT,'j.','k.','d.','f.');
	$LateExpectedReasonField = $lcardattend->Get_Expected_Reason($DayType,PROFILE_TYPE_LATE,'j.');
	$OutingExpectedReasonField = $lcardattend->Get_Expected_Reason($DayType,CARD_STATUS_OUTING,'j.',"","d.","c.",true);
	$StatusField = "PMStatus";
	
	$AMJoinTable = "LEFT JOIN CARD_STUDENT_PROFILE_RECORD_REASON as k 
					ON 
						k.StudentID = a.UserID 
						AND k.RecordDate = '".$TargetDate."' 
						AND k.DayType = '".PROFILE_DAY_TYPE_AM."'
						AND k.RecordType = '".PROFILE_TYPE_ABSENT."' ";
	
	$office_remark_field = "IF(j.RecordID IS NOT NULL,j.OfficeRemark,IF(TRIM(f.Remark)!='',f.Remark,IF(c.Detail IS NOT NULL,c.Detail,k.OfficeRemark))) as OfficeRemark ";
	
        $sql  = "SELECT        
        					".getNameFieldByLang("a.")."as name,
                  a.ClassName,
                  a.ClassNumber,
                  IF(b.LunchBackTime IS NULL,'-',b.LunchBackTime),
									IF(b.LunchBackStation IS NULL,'-',b.LunchBackStation),
                  ".$lcardattend->Get_PM_Expected_Field("b.","c.","f.").", 
                  IF(d.Remark IS NULL,'-',d.Remark),
                  a.UserID,
                  IF(f.Reason IS NULL,'',f.Reason),
									IF(f.Remark IS NULL,'',f.Remark),
					if (".$expect_field." = '".CARD_STATUS_ABSENT."',
				".$AbsentExpectedReasonField.",
				IF(".$expect_field." = '".CARD_STATUS_LATE."',
					".$LateExpectedReasonField.",
					IF(".$expect_field." = '".CARD_STATUS_OUTING."',
						".$OutingExpectedReasonField.",
						j.Reason))) as Reason,
				j.RecordStatus as Waived,
				f.Waive as PresetWaive,
				$office_remark_field,
				".$AbsentExpectedReasonField." as DefaultLateReason,
				".$LateExpectedReasonField." as DefaultAbsentReason, 
				".$OutingExpectedReasonField." as DefaultOutingReason 
                FROM
                  INTRANET_USER AS a 
				  ".$FilterInActiveStudent ." 
				  inner join INTRANET_USERGROUP AS x 
					on 
										x.GROUPID = '$class_id' and 
										a.UserID = x.UserID and 
										a.RecordType=2 AND
										a.RecordStatus IN (0,1,2)
				  LEFT JOIN $card_log_table_name AS b
				          ON	(
				          	a.UserID=b.UserID AND
				          	(b.DayNumber = ".$txt_day." ||  b.DayNumber IS NULL)
				            )
				  LEFT JOIN CARD_STUDENT_OUTING AS c
				          ON (a.UserID=c.UserID AND c.RecordDate = '$TargetDate' AND c.DayType = '".$DayType."')
				  LEFT JOIN CARD_STUDENT_DAILY_REMARK AS d ON (a.UserID=d.StudentID AND d.RecordDate='$TargetDate' AND d.DayType='$DayType')
				  LEFT JOIN CARD_STUDENT_PRESET_LEAVE AS f ON (a.UserID = f.StudentID AND f.DayPeriod=2 AND f.RecordDate='$txt_year-$txt_month-$txt_day')
				  LEFT JOIN CARD_STUDENT_PROFILE_RECORD_REASON as j
						ON j.StudentID = a.UserID 
							AND j.RecordDate = '".$TargetDate."' 
							AND j.DayType = '".$DayType."' 
							AND 
							(
							j.RecordType = b.".$StatusField." 
							OR  
							(j.RecordType = '".PROFILE_TYPE_ABSENT."' 
							AND b.".$StatusField." = '".CARD_STATUS_OUTING."' 
							)
							)
					".$AMJoinTable." 
			        WHERE
			        	(b.DayNumber = ".$txt_day." ||  b.DayNumber IS NULL) 
							order by a.classname asc, a.classnumber+0 asc
                                        ";
                                        
	### check if Preset Absence exists
	$sqlPresetCount="SELECT COUNT(*) 
					FROM 
						INTRANET_USER AS A ,
						CARD_STUDENT_PRESET_LEAVE AS B,
						INTRANET_USERGROUP AS C
					WHERE 
						C.GROUPID = '$class_id' AND
						C.USERID = A.USERID AND
						A.RECORDTYPE=2 AND 
						A.RECORDSTATUS IN(0,1,2) AND 
						A.USERID = B.STUDENTID  AND
						B.RECORDDATE='$txt_year-$txt_month-$txt_day' AND 
						B.DayPeriod=3";
}

//echo "sql [".$sql."]<br>";
$result = $li->returnArray($sql,10);

# Lunch cancellation option
if ($sys_custom['SmartCardAttendance_LunchBoxOption'])
{
    $lunch_tablename = $lcardattend->createTable_Card_Student_Lunch_Box_Option($txt_year, $txt_month);
    $sql = "SELECT b.StudentID, b.CancelOption FROM
                   INTRANET_USER as a LEFT OUTER JOIN $lunch_tablename as b ON b.DayNumber = ".$txt_day." AND a.UserID = b.StudentID
                   WHERE
                   b.DayNumber = ".$txt_day." AND
                    a.RecordType=2 AND
                    a.RecordStatus IN (0,1,2) AND
                    a.ClassName IS NOT NULL AND a.ClassName !=\"\"
                    $conds
                   ";
    $temp = $lcardattend->returnArray($sql,2);
    $lunch_option_result = build_assoc_array($temp);
}

# Count Number of Preset Absence Students in this group
$PresetCountResult = $lcardattend->returnVector($sqlPresetCount);
$PresetCount = $PresetCountResult[0];

if(sizeof($result) == 0)
{
	printHeader($DayType,$TargetDate ,$display_group_name, $PresetCount);
	$colspan = 4;
	if($PresetCount>0) $colspan += 1;
	echo "<tr><td align=center colspan=\"$colspan\" class=\"tableContent\"><br>$i_no_record_exists_msg<br><br></td></tr>\n";
    echo "\n";    
}
else
{	
	for ($i=0; $i<sizeof($result); $i++)
	{
			list ($name,$ClassName,$ClassNumber,$InSchoolTime,$InSchoolStation,$attend_status,$remark, $t_studentID,  $PresetReason, $PresetRemark, $reason, $waived, $preset_waive, $office_remark, $DefaultAbsentReason,$DefaultLateReason,$DefaultOutingReason) = $result[$i];
			
			if ($i==0)
			{		
					$noOfStudent = 0;
					printHeader($DayType,$TargetDate ,$display_group_name, $PresetCount);
					echo "\n";
			}

	/*        if ($i!=0 && $prevClass != $ClassName)
			{
					#$display_class_name = "<tr colspan=3><td>$ClassName</td></tr></table>\n";

					echo "$pagefooter\n$page_breaker\n";
					printHeader($period,$TargetDate,$ClassName);
					echo "\n";
					$prevClass = $ClassName;
			}

			$prevClass = $ClassName;
	*/
			switch ($attend_status)
			{
							case CARD_STATUS_PRESENT : $note = $i_StudentAttendance_Status_Present;
											  break;
							case CARD_STATUS_ABSENT : $note = $i_StudentAttendance_Status_PreAbsent;
											  break;
							case CARD_STATUS_LATE : $note = $i_StudentAttendance_Status_Late;
											  break;
							case CARD_STATUS_OUTING : $note = $i_StudentAttendance_Status_Outing;
											  break;
							default: $note = "";
											 break;
			}

		 $class_str = ($ClassName != "" && $ClassNumber != "")? "($ClassName - $ClassNumber)":"";
		 if ($sys_custom['SmartCardAttendance_LunchBoxOption'])
		 {
			 $lunch_cancel_status_text = "<td>".(($lunch_option_result[$t_studentID]==1)?"$button_cancel":"")."&nbsp;</td>";
		 }
		 else
		 {
			 $lunch_cancel_status_text = "";
		 }
		 if($PresetReason != "" || $PresetRemark != "")
     		$preset_info = "<td>$i_Attendance_Reason: $PresetReason<br>$i_Attendance_Remark: $PresetRemark</td>";
     	 else
     		$preset_info = "<td>-</td>";
		echo "<tr bgcolor='$background'><td><font color='$textcolor'>".++$noOfStudent."</font></td>";
		echo "<td><font color='$textcolor'>$name $class_str</font></td>";
		echo "<td><font color='$textcolor'>$InSchoolTime</font></td>";
		echo "<td><font color='$textcolor'>$note</font></td>".($PresetCount>0?$preset_info:"");
		echo "<td>".(!in_array($attend_status, array(CARD_STATUS_PRESENT,CARD_STATUS_OUTING)) && $waived==1?$Lang['General']['Yes']:'&nbsp;')."</td>\n";
        echo "<td>$reason&nbsp;</td>\n";
		echo "<td>$remark&nbsp;</td>";
		echo "<td>$office_remark&nbsp;</td>";
		echo "$lunch_cancel_status_text</tr>\n";
	}
}
echo "</table>\n";

echo "<br><table width='560' border='0'><tr><td align='center'><a href='javascript:window.print()'><img class=print_hide src='$image_path/admin/button/s_btn_print_$intranet_session_language.gif' border='0'></a></td></tr></table>";

intranet_closedb();
?>