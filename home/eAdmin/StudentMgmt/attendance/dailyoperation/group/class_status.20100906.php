<?php
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$lc = new libcardstudentattend2();
$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageDailyOperation_ViewClassStatus";

if (!$lc->IS_ADMIN_USER($_SESSION['UserID'])) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$linterface = new interface_html();

#class used
$LIDB = new libdb();
$lc->retrieveSettings();
$attendanceMode=$lc->attendance_mode;
### Set Date from previous page
$ts_record = strtotime($TargetDate);
if ($ts_record == -1)
{
    $ts_record = strtotime(date('Y-m-d'));
}
$txt_year = date('Y',$ts_record);
$txt_month = date('m',$ts_record);
$txt_day = date('d',$ts_record);

### create table if needed
$lc->createTable_LogAndConfirm($txt_year,$txt_month);

###period
switch ($period)
{
    case "1":
		$display_period = $i_DayTypeAM;
        $DayType = 2;
        $link_page = "AM";
        $confirm_page="AM";
        break;
    case "2": 
    	$display_period = $i_DayTypePM;
        $DayType = 3;
        //if($attendanceMode==2||$attendanceMode==3){
        //        $confirm_page="PM_S";
        //}else{
                $confirm_page="PM";
        //}
               $link_page="PM";
        break;
    default : $display_period = $i_DayTypeAM;
        $DayType = 2;
        $link_page = "AM";
        $confirm_page="AM";
        break;
}

### build student table
$card_student_daily_class_confirm = "CARD_STUDENT_DAILY_CLASS_CONFIRM_".$txt_year."_".$txt_month;
$sql = "SELECT DISTINCT
            a.ClassID,
            a.ClassName,
            IF(b.ConfirmedUserID IS NULL,
                '-',
                IF(b.ConfirmedUserID <> -1, ".getNameFieldByLang("c.").", CONCAT('$i_general_sysadmin'))
               ),
            IF(b.DateModified IS NULL, '-', b.DateModified)
                    FROM INTRANET_CLASS as a
                            LEFT OUTER JOIN $card_student_daily_class_confirm as b
                                    ON (                a.ClassID=b.ClassID AND
                                                            (b.DayNumber = ".$txt_day." || b.DayNumber IS NULL) AND
                                                            (b.DayType = $DayType || b.DayType IS NULL)        )
                            LEFT OUTER JOIN INTRANET_USER as c ON (b.ConfirmedUserID=c.UserID || b.ConfirmedUserID=-1) WHERE a.RecordStatus=1
                            ORDER BY a.ClassName

                        ";
$result = $LIDB->returnArray($sql, 4);

$x = "<table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">\n";
$x .= "<tr class=\"tabletop\"><td class=\"tabletoplink\">$i_ClassName</td><td class=\"tabletoplink\">$i_StudentAttendance_Field_ConfirmedBy</td><td class=\"tabletoplink\">$i_StudentAttendance_Field_LastConfirmedTime</td></tr>\n";
for($i=0; $i<sizeOf($result); $i++)
{
        list($class_id, $class_name, $confirmed_username, $confirmed_date) = $result[$i];
        $css=($i%2==0)?"tablerow1":"tablerow2";
        if($lc->isRequiredToTakeAttendanceByDate($class_name,$TargetDate)){
	        $class_link = (!$sys_custom['QualiEd_StudentAttendance']) ? "<a class=\"tablelink\" href=\"view_student_$link_page.php?class_name=$class_name&class_id=$class_id&TargetDate=$TargetDate&period=$period\">$class_name</a>" : "<a class=functionlink_new href=\"view_student_{$link_page}_q.php?class_name=$class_name&class_id=$class_id&TargetDate=$TargetDate&period=$period\">$class_name</a>";
	        $x .= "<tr class=\"$css\"><td class=\"tabletext\">$class_link</td><td class=\"tabletext\">$confirmed_username</td><td class=\"tabletext\">$confirmed_date</td></tr>\n";
        }else{
	        $class_link = "<a class=functionlink_new>$class_name</a>";
	        $x .="<tr class=\"$css\"><td class=\"tabletext\">$class_link</td><td class=\"tabletext\" colspan=\"2\">$i_StudentAttendance_ClassMode_NoNeedToTakeAttendance</td>";
	    }
}
$x .= "</table>\n";


$data = Array(
	        Array("1",$i_SmartCard_DailyOperation_ViewClassStatus_PrintAll),
	        Array("2",$i_SmartCard_DailyOperation_ViewClassStatus_PrintAbs),
	        Array("3",$i_SmartCard_DailyOperation_ViewClassStatus_PrintLate),
	        Array("4",$i_SmartCard_DailyOperation_ViewClassStatus_PrintAbsAndLate)
        );


#$toolbar = "<table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">\n";
$toolbar .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">";
$toolbar .= "$i_SmartCard_DailyOperation_viewClassStatus_PrintOption</td>";
$toolbar .= "<td>".getSelectByArray($data, " name=print_option", $print_option,0,1)."</td></tr>";
$toolbar .= "<tr><td colspan=\"2\" class=\"dotline\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"10\" height=\"1\" /></td></tr>";
$toolbar .= "<tr><td colspan=\"2\" align=\"center\">".$linterface->GET_ACTION_BTN($button_print, "button", "", "print2", "onclick='openPrintPage()'")."</td></tr>";

$TAGS_OBJ[] = array($i_SmartCard_DailyOperation_ViewClassStatus, "", 0);
$MODULE_OBJ = $lc->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

$PAGE_NAVIGATION[] = array($display_period);

if ($msg == 1) $SysMsg = $linterface->GET_SYS_MSG("add");
if ($msg == 2) $SysMsg = $linterface->GET_SYS_MSG("update");
?>
<script language="JavaScript" type="text/javascript">
<!--
function openPrintPage()
{
        var option = document.form1.print_option.value;
        if(option=="")
        {
                alert("<?=$i_alert_pleaseselect?>");
        }
        else
        {
            newWindow("view_student_print.php?period=<?=$period?>&TargetDate=<?=$TargetDate?>&option="+option,4);
        }
}
-->
</script>
<br />
<form name="form1" method="post" action="">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	</tr>
	<tr>
		<td>
			<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $i_StudentAttendance_Field_Date ?></td>
					<td class="tabletext" width="70%"><?=$TargetDate?></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table width="95%" border="0" cellpadding="0" cellspacing="0" align="center">
				<tr>
					<td align="left"><?=$absent_count?></td>
					<td align="right"><?=$SysMsg?></td>
				</tr>
				<tr>
					<td align="left">&nbsp;</td>
					<td align="right">&nbsp;</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<?=$x?>
		</td>
	</tr>
	<tr>
		<td>
			<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr>
			    	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
			    </tr>
			    <tr>
				    <td align="center">
						<?= $linterface->GET_ACTION_BTN($i_SmartCard_DailyOperation_BlindConfirmation, "button", "window.location.href='blind_confirmation_$confirm_page.php?TargetDate=$TargetDate&period=$period'") ?>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</form>
<br />
<?
$linterface->LAYOUT_STOP();
print $linterface->FOCUS_ON_LOAD("form1.print_option");
intranet_closedb();
?>
