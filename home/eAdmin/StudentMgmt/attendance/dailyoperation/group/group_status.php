<?php
// Editing by 

/*
 * 2018-06-22 (Carlos): Display group name to follow UI language.
 * 2014-07-16 (Bill): Display radio button
 * 2010-10-07 (Kenneth Chung): Unified view_student_AM.php/ view_student_PM.php to view_student.php
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libstudentattendance_ui.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();
$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageDailyOperation_ViewGroupStatus";

$StudentAttendUI = new libstudentattendance_ui();
$linterface = new interface_html();

#class used
$LIDB = new libdb();
$lc->retrieveSettings();
$attendanceMode=$lc->attendance_mode;
### Set Date from previous page
if ($TargetDate == "")
{
	$TargetDate = date('Y-m-d');
}
$ts_record = strtotime($TargetDate);
$txt_year = date('Y',$ts_record);
$txt_month = date('m',$ts_record);
$txt_day = date('d',$ts_record);

### create table if needed
$lc->createTable_LogAndConfirm($txt_year,$txt_month);

###period
switch ($DayType)
{
    case PROFILE_DAY_TYPE_AM:
			$display_period = $i_DayTypeAM;    
      $link_page = "AM";
      $confirm_page="AM";
      break;
    case PROFILE_DAY_TYPE_PM: 
    	$display_period = $i_DayTypePM;
      $confirm_page="PM";
     	$link_page="PM";
      break;
    default : 
    	if ($lc->attendance_mode == 1) {
    		$display_period = $i_DayTypePM;      
    		$DayType = PROFILE_DAY_TYPE_PM;
	      $confirm_page="PM";
	      $link_page="PM";
    	}
    	else {
	    	$display_period = $i_DayTypeAM;
		    $DayType = PROFILE_DAY_TYPE_AM;
		    $link_page = "AM";
		    $confirm_page="AM";
		  }
      break;
}

### build student table
//$card_student_daily_class_confirm = "CARD_STUDENT_DAILY_CLASS_CONFIRM_".$txt_year."_".$txt_month;
$card_student_daily_group_confirm = "CARD_STUDENT_DAILY_GROUP_CONFIRM_".$txt_year."_".$txt_month;

$group_title_field  = Get_Lang_Selection("a.TitleChinese","a.Title");

$sql = "SELECT DISTINCT
          a.GROUPID,
          a.Title,
          e.mode,
          IF(b.ConfirmedUserID IS NULL,
	          '-',
	          IF(b.ConfirmedUserID <> -1, ".getNameFieldByLang("c.").", CONCAT('$i_general_sysadmin'))
          ),
          IF(b.DateModified IS NULL, '-', b.DateModified),
			$group_title_field as GroupTitle 
        FROM 
        	INTRANET_GROUP as a 
					left outer join 
					YEAR_CLASS as d 
					on a.groupid = d.groupid
          LEFT OUTER JOIN 
          $card_student_daily_group_confirm as b
          ON (a.groupid=b.groupid AND
             (b.DayNumber = ".$txt_day." || b.DayNumber IS NULL) AND
             (b.DayType = $DayType || b.DayType IS NULL))
          LEFT OUTER JOIN INTRANET_USER as c 
					ON (b.ConfirmedUserID=c.UserID || b.ConfirmedUserID=-1) 
					LEFT OUTER JOIN CARD_STUDENT_ATTENDANCE_GROUP as e
					on (e.groupid = a.groupid)
				WHERE 
					a.AcademicYearID = '".Get_Current_Academic_Year_ID()."' 
					and a.RecordType = 3
					AND d.groupid is null
    	  ORDER BY a.Title
				";

//echo "sql [".$sql."]<br>";
$result = $LIDB->returnArray($sql, 5);

$x = "<table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">\n";
$x .= "<tr class=\"tabletop\">
				<td class=\"tabletoplink\">$i_GroupName</td>
				<td class=\"tabletoplink\">$i_StudentAttendance_Field_ConfirmedBy</td>
				<td class=\"tabletoplink\">$i_StudentAttendance_Field_LastConfirmedTime</td>
			</tr>\n";
			
for($i=0; $i<sizeOf($result); $i++)
{
	list($group_id, $class_name, $mode ,$confirmed_username, $confirmed_date, $display_group_title) = $result[$i];
	if ($mode == 2) # mode equal to 2(no need to take attandace), don't show this record;
	{
		continue;
	}
	$css=($i%2==0)?"tablerow1":"tablerow2";
	if($lc->groupIsRequiredToTakeAttendanceByDate($group_id,$TargetDate)){
	  $class_link = "<a class=\"tablelink\" href=\"view_student.php?class_name=$class_name&class_id=$group_id&TargetDate=$TargetDate&DayType=$DayType\">$display_group_title</a>";
	  $x .= "<tr class=\"$css\">
	  				<td class=\"tabletext\">$class_link</td>
	  				<td class=\"tabletext\">$confirmed_username</td>
	  				<td class=\"tabletext\">$confirmed_date</td>
	  			</tr>\n";
	}else{
	  $class_link = "<a class=functionlink_new>$display_group_title</a>";
	  $x .="<tr class=\"$css\">
	  				<td class=\"tabletext\">$class_link</td>
	  				<td class=\"tabletext\" colspan=\"2\">$i_StudentAttendance_ClassMode_NoNeedToTakeAttendance</td>
	  			</tr>\n";
	}
}
$x .= "</table>\n";


$data = Array(
	        Array("1",$i_SmartCard_DailyOperation_ViewClassStatus_PrintAll),
	        Array("2",$i_SmartCard_DailyOperation_ViewClassStatus_PrintAbs),
	        Array("3",$i_SmartCard_DailyOperation_ViewClassStatus_PrintLate),
	        Array("4",$i_SmartCard_DailyOperation_ViewClassStatus_PrintAbsAndLate)
        );


#$toolbar = "<table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">\n";
$toolbar .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">";
$toolbar .= "$i_SmartCard_DailyOperation_viewClassStatus_PrintOption</td>";
$toolbar .= "<td>".getSelectByArray($data, " name=print_option", $print_option,0,1)."</td></tr>";
$toolbar .= "<tr><td colspan=\"2\" class=\"dotline\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"10\" height=\"1\" /></td></tr>";
$toolbar .= "<tr><td colspan=\"2\" align=\"center\">".$linterface->GET_ACTION_BTN($button_print, "button", "", "print2", "onclick='openPrintPage()'")."</td></tr>";

$TAGS_OBJ[] = array($i_SmartCard_DailyOperation_ViewGroupStatus, "", 0);
$MODULE_OBJ = $lc->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

$PAGE_NAVIGATION[] = array($display_period);

if ($msg == 1) $SysMsg = $linterface->GET_SYS_MSG("add");
if ($msg == 2) $SysMsg = $linterface->GET_SYS_MSG("update");
?>
<script language="JavaScript" type="text/javascript">
<!--
function openPrintPage()
{
        var option = document.form1.print_option.value;
        if(option=="")
        {
                alert("<?=$i_alert_pleaseselect?>");
        }
        else
        {
            newWindow("view_student_print.php?DayType=<?=$DayType?>&TargetDate=<?=$TargetDate?>&option="+option,4);
        }
}
-->
</script>
<br />
<?
// Display radio button
echo $StudentAttendUI->Get_Confirm_Selection_Form("form1","group_status.php","Group",$TargetDate,$DayType, 1);
?>
<form name="form2" method="post" action="">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<!--
	<tr>
		<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	</tr>
	-->
	<tr>
		<td>
			<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $i_StudentAttendance_Field_Date ?></td>
					<td class="tabletext" width="70%"><?=$TargetDate?> (<?=$display_period?>)</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table width="95%" border="0" cellpadding="0" cellspacing="0" align="center">
				<tr>
					<td align="left"><?=$absent_count?></td>
					<td align="right"><?=$SysMsg?></td>
				</tr>
				<tr>
					<td align="left">&nbsp;</td>
					<td align="right">&nbsp;</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<?=$x?>
		</td>
	</tr>
	<!--<tr>
		<td>
			<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr>
			    	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
			    </tr>
			    <tr>
				    <td align="center">
						<?= $linterface->GET_ACTION_BTN($i_SmartCard_DailyOperation_BlindConfirmation, "button", "window.location.href='blind_confirmation_$confirm_page.php?TargetDate=$TargetDate&period=$period'") ?>
					</td>
				</tr>
			</table>
		</td>
	</tr>-->
</table>
</form>
<br />
<?
$linterface->LAYOUT_STOP();
print $linterface->FOCUS_ON_LOAD("form1.print_option");
intranet_closedb();
?>
