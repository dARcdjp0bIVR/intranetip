<?php
//using by 
##################################### Change Log ######################################
# 2018-11-15 Cameron: add filter $routeID for eSchoolBus
# 2015-02-11 Carlos: Added column [Office Remark]
# 2011-12-21: Carlos - added data column Gender
# 2010-04-22: Carlos - Show late session if flag is switched on
#######################################################################################
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

### class used
$lc = new libcardstudentattend2();

### Set Date from previous page
$ts_record = strtotime($TargetDate);
if ($ts_record == -1)
{
    $TargetDate = date('Y-m-d');
    $ts_record = strtotime($TargetDate);
}
$txt_year = date('Y',$ts_record);
$txt_month = date('m',$ts_record);
$txt_day = date('d',$ts_record);

### period
switch ($DayType)
{
    case PROFILE_DAY_TYPE_AM: 
    	$display_period = $i_DayTypeAM;
        break;
    case PROFILE_DAY_TYPE_PM: 
    	$display_period = $i_DayTypePM;
        break;
    default : 
    	$display_period = $i_DayTypeAM;
        $DayType = PROFILE_DAY_TYPE_AM;
        break;
}


# order information
$default_order_by = " a.ClassName, a.ClassNumber, a.EnglishName";

if($order_by_time!=1){
	$order_by = $default_order_by ;
}
else{
	$order_str= $order== 1?" DESC ":" ASC ";

	if($DayType==PROFILE_DAY_TYPE_AM){
		$order_by = "b.InSchoolTime $order_str";
	}
	else {
		$order_by = "b.LunchBackTime $order_str";
	}
}

### build card log table
$card_log_table_name = "CARD_STUDENT_DAILY_LOG_".$txt_year."_".$txt_month;

if ($DayType==PROFILE_DAY_TYPE_AM)               # Check AM Late
{
	$InSchoolTime = "InSchoolTime";
	$InSchoolStation = "InSchoolStation";
	$StatusField = "AMStatus";

}
else     # Check PM Late
{
	$InSchoolTime = "LunchBackTime";
	$InSchoolStation = "LunchBackStation";
	$StatusField = "PMStatus";
}

$busRouteJoin = '';     // default value
$busRouteFilter = '';

if ($plugin['eSchoolBus']) {
    $academicYearTermAry = getAcademicYearAndYearTermByDate($TargetDate);
    $academicYearID = $academicYearTermAry['AcademicYearID'];
    if ($DayType == PROFILE_DAY_TYPE_AM) {
        $amPm = 'AM';
    }
    else if ($DayType == PROFILE_DAY_TYPE_PM) {
        $amPm = 'PM';
    }
    if ($routeID) {
        $busRouteJoin = " LEFT JOIN INTRANET_SCH_BUS_USER_ROUTE ur ON ur.UserID=a.UserID AND ur.AcademicYearID='$academicYearID'
        LEFT JOIN INTRANET_SCH_BUS_ROUTE_COLLECTION rc ON rc.RouteCollectionID=ur.RouteCollectionID ";
        $busRouteFilter = " AND ur.DateStart<='$TargetDate' AND ur.DateEnd>='$TargetDate' AND rc.RouteID='$routeID' AND rc.AmPm='$amPm' AND rc.IsDeleted='0' ";
    }
}

$sql  = "SELECT 
					a.UserLogin,
					b.RecordID, 
					a.UserID,
          ".getNameFieldByLang("a.")."as name,
          a.ClassName,
          a.ClassNumber,
		  a.Gender,
          b.".$InSchoolTime.",
          IF(b.".$InSchoolStation." IS NULL, '-', b.".$InSchoolStation."),
          c.Reason,
          c.RecordStatus,
		  c.AbsentSession as LateSession,
		  d.Remark,
		  c.OfficeRemark  
        FROM
        	$card_log_table_name as b
					LEFT OUTER JOIN INTRANET_USER as a ON b.UserID = a.UserID
					LEFT OUTER JOIN CARD_STUDENT_PROFILE_RECORD_REASON as c
					ON c.StudentID = a.UserID AND c.RecordDate = '$TargetDate' AND c.DayType = $DayType
					AND c.RecordType = '".PROFILE_TYPE_LATE."' 
					LEFT JOIN CARD_STUDENT_DAILY_REMARK AS d 
					ON b.UserID = d.StudentID AND d.RecordDate ='".$TargetDate."' AND d.DayType='$DayType'
                    {$busRouteJoin} 
        WHERE 
        	b.DayNumber = '$txt_day' AND b.".$StatusField." = '".CARD_STATUS_LATE."'
          AND a.RecordType = 2 AND a.RecordStatus IN (0,1)
            {$busRouteFilter}
        ORDER BY $order_by
";
$result = $lc->returnArray($sql,14);

$lexport = new libexporttext();

$exportColumn = array();
if ($sys_custom["StudentAttendance"]['LateAbsentEarlyShowUserLogin'])
	$exportColumn[] = $i_UserLogin;
$exportColumn[] = $i_UserName;
$exportColumn[] = $i_ClassName;
$exportColumn[] = $i_UserClassNumber;
$exportColumn[] = $Lang['StudentAttendance']['Gender'];
$exportColumn[] = $i_SmartCard_Frontend_Take_Attendance_In_School_Time;
$exportColumn[] = $i_SmartCard_Frontend_Take_Attendance_In_School_Station;
$exportColumn[] = $i_SmartCard_Frontend_Take_Attendance_Status;
$exportColumn[] = $i_SmartCard_Frontend_Take_Attendance_Waived;
$exportColumn[] = $i_Attendance_Reason;
$exportColumn[] = $Lang['StudentAttendance']['iSmartCardRemark'];
$exportColumn[] = $Lang['StudentAttendance']['OfficeRemark'];

$exportColumn[] = $i_StudentGuardian['MenuInfo'];
$export_result = array();

$gender_word['M'] = $Lang['General']['Male'];
$gender_word['F'] = $Lang['General']['Female'];

for($i=0; $i<sizeOf($result); $i++)
{
    list($UserLogin,$record_id, $user_id, $UserName,$class_name,$class_number,$gender, $in_school_time, $in_school_station, $reason,$record_status,$late_session, $remark, $office_remark) = $result[$i];
    // get guardian info
		$main_content_field = "IF(EnName IS NOT NULL AND EnName !='' AND ChName IS NOT NULL AND ChName !='', IF(IsMain = 1, CONCAT('* ', EnName,' / ',ChName), CONCAT(EnName,' / ',ChName)), IF(EnName IS NOT NULL AND EnName!='',EnName,ChName))";
		$main_content_field = "IF($main_content_field='' OR $main_content_field IS NULL,'-',$main_content_field)";
		$sql = "SELECT
						  $main_content_field,
						  Relation,
						  Phone,
						  EmPhone,
						  IsMain
						FROM
						  $eclass_db.GUARDIAN_STUDENT
						WHERE
						  UserID = '".$user_id."'
						ORDER BY
						  IsMain DESC, Relation ASC
						";
		//debug_r($sql);
		$temp = $lc->returnArray($sql,4);
		if (sizeof($temp)==0)
			$GuardianInfo = "--";
		else
		{
			$GuardianInfo = "";
	    for($j=0; $j<sizeof($temp); $j++)
	    {
				list($name, $relation, $phone, $em_phone) = $temp[$j];
				$GuardianInfo .= ($j+1).". ".$name." (".$ec_guardian[$relation].") (".$i_StudentGuardian_Phone.") ".$phone." (".$i_StudentGuardian_EMPhone.") ".$em_phone." ";
		  }
		}
    
    $str_status = $i_StudentAttendance_Status_Late;
    $str_reason = $reason;
    $waived_option = $record_status == 1? $i_general_yes:$i_general_no;
    //$export_result[$i] = array($name, $class_name, $class_number, $in_school_time, $in_school_station, $str_status, $waived_option, $str_reason);
    $export_result[$i]=array();
    if ($sys_custom["StudentAttendance"]['LateAbsentEarlyShowUserLogin'])
			$export_result[$i][] = $UserLogin;
    $export_result[$i][]=$UserName;
    $export_result[$i][]=$class_name;
    $export_result[$i][]=$class_number;
    $export_result[$i][]=$gender_word[$gender];
    $export_result[$i][]=$in_school_time;
    $export_result[$i][]=$in_school_station;
    $export_result[$i][]=$str_status;
    $export_result[$i][]=$waived_option;
    $export_result[$i][]=$str_reason;
    $export_result[$i][]=$remark;
    $export_result[$i][]=$office_remark;
    $export_result[$i][]=$GuardianInfo;
}

$export_content = $lexport->GET_EXPORT_TXT($export_result, $exportColumn);

$export_content_final = $i_SmartCard_DailyOperation_ViewLateStatus;
$export_content_final .= " $TargetDate ($display_period)\r\n";

if (sizeof($result)==0)
	$export_content_final .= "$i_StudentAttendance_NoLateStudents\r\n";
else
	$export_content_final .= $export_content;


intranet_closedb();

$filename = "showlate-".time().".csv";
$lexport->EXPORT_FILE($filename, $export_content_final);
?>