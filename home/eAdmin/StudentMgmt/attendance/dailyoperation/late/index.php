<?php
// Editing by 
# 20061113 (Kenneth): Change logic of confirmation indicator
/*
 * 2014-07-24 Carlos: Change Period selection to radio checkboxes
 */

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libstudentattendance_ui.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();
$StudentAttendUI = new libstudentattendance_ui();
$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageDailyOperation_ViewLateStatus";

$linterface = new interface_html();

$lc->retrieveSettings();

# Status of dates
$records_with_data = array();
$ts = time();
$iteration = 0;

// Deprecate on 2010-07-02 by kenneth chung
/*
# 20061113 (Kenneth): Change the method of retrival of which dates have data
# Count late/early leave/absence
# Count 3 months
if ($period == 1)
{
    $field_status = "AMStatus";
    $confirm_conds = " AND RecordType = ".PROFILE_DAY_TYPE_AM;
}
else
{
    $field_status = "PMStatus";
    $confirm_conds = " AND RecordType = ".PROFILE_DAY_TYPE_PM;
}

while($iteration < 12)
{

      $year = date('Y',$ts);
      $month = date('m',$ts);
      $table_name = "CARD_STUDENT_DAILY_LOG_".$year."_".$month;
      $sql = "SELECT DISTINCT DayNumber FROM $table_name
                     WHERE $field_status = '".CARD_STATUS_LATE."'
                     ORDER BY DayNumber";
      $temp = $lc->returnVector($sql);
      for($i=0; $i<sizeof($temp); $i++)
      {
          $day = $temp[$i];
          $day = ($day < 10? "0".$day : $day);
          $entry_date = $year."-".$month."-".$day;
          $records_with_data[] = $entry_date;
      }
      $ts = mktime(0,0,0,$month-1,1,$year);
      $iteration++;
}

# Get Confirmed date
$date_string = $year."-".$month."-01";
$sql = "SELECT DISTINCT(DATE_FORMAT(RecordDate,'%Y-%m-%d')) FROM CARD_STUDENT_DAILY_DATA_CONFIRM
               WHERE LateConfirmed = 1 AND RecordDate >= '$date_string'
               $confirm_conds ORDER BY RecordDate";
$temp = $lc->returnVector($sql);
for ($i=0; $i<sizeof($temp); $i++)
{
     $confirmed_dates[$temp[$i]] = 1;
}
*/

$TAGS_OBJ[] = array($i_SmartCard_DailyOperation_ViewLateStatus, "", 0);
$MODULE_OBJ = $lc->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

$PAGE_NAVIGATION[] = array($class_name);

if ($msg == 1) $SysMsg = $linterface->GET_SYS_MSG("add");
if ($msg == 2) $SysMsg = $linterface->GET_SYS_MSG("update");


?>
<br />
<?
echo $StudentAttendUI->Get_Confirm_Selection_Form("form1","showlate.php",CARD_STATUS_LATE,"","",1);
?>
<br />
<?
$linterface->LAYOUT_STOP();
print $linterface->FOCUS_ON_LOAD("form1.TargetDate");
intranet_closedb();
?>