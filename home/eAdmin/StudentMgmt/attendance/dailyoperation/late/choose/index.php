<?php
// editing by 
/************************************************* Change Log *******************************************************
 * 2017-04-11 (Carlos): Created. 
 ********************************************************************************************************************/
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();

$MODULE_OBJ['title'] = $iDiscipline['select_students'];
$linterface = new interface_html("popup.html");
$linterface->LAYOUT_START();

$lclass = new libclass();
$select_class = $lclass->getSelectClass("name=\"targetClass\" onChange=\"this.form.action='';this.form.submit()\"",$targetClass);

if ($targetClass != "")
{
    
    $ts_record = strtotime($TargetDate);
	if ($ts_record == -1)
	{
	    $TargetDate = date('Y-m-d');
	    $ts_record = strtotime($TargetDate);
	}
	$txt_year = date('Y',$ts_record);
	$txt_month = date('m',$ts_record);
	$txt_day = date('d',$ts_record);
	
	$card_student_daily_log = "CARD_STUDENT_DAILY_LOG_";
	$card_student_daily_log .= $txt_year."_";
	$card_student_daily_log .= $txt_month;
	
    switch ($DayType)
	{
        case  PROFILE_DAY_TYPE_AM: $InSchool = " AND (b.RecordID IS NULL OR b.AMStatus IS NULL OR b.AMStatus IN (0,1,3)) ";
        		  //$LeaveStatus = " AND (b.LeaveStatus IS NULL OR b.LeaveStatus = 0) ";
                                                break;
        case  PROFILE_DAY_TYPE_PM: $InSchool = " AND (b.RecordID IS NULL OR b.PMStatus IS NULL OR b.PMStatus IN (0,1,3))";
        		  //$LeaveStatus = " AND (b.LeaveStatus IS NULL OR b.LeaveStatus = 0) ";
                                                break;
        default : $InSchool = " AND (b.RecordID IS NULL OR b.AMStatus IS NULL OR b.AMStatus IN (0,1,3)) ";
        		  //$LeaveStatus = " AND (b.LeaveStatus IS NULL OR b.LeaveStatus = 0) ";
                                                break;
	}
	
	$name_field = getNameFieldWithClassNumberByLang("a.");    
	$sql = "SELECT 
					a.UserID, $name_field 
			FROM 
					INTRANET_USER AS a LEFT JOIN 
					$card_student_daily_log AS b ON a.UserID = b.UserID AND b.DayNumber = '$txt_day'
			WHERE 
					1 
					$InSchool
					$LeaveStatus
					AND a.ClassName = '$targetClass' AND a.RecordType = 2 
					AND a.RecordStatus IN (0,1,2)
			ORDER BY
					a.ClassName, a.ClassNumber+0, a.EnglishName
			";

	$list = $lclass->returnArray($sql,2);
	$select_students = getSelectByArray($list, "size=25 multiple name=targetID[]","",0,1);
}
?>

<script language="javascript">
function AddOptions(obj){
     par = opener.window;
     parObj = opener.window.document.form1.elements["<?php echo $fieldname; ?>"];

     checkOption(obj);
     par.checkOption(parObj);
     i = obj.selectedIndex;

     while(i!=-1)
        {
         addtext = obj.options[i].text;

          par.checkOptionAdd(parObj, addtext, obj.options[i].value);
          obj.options[i] = null;
          i = obj.selectedIndex;
     }
     par.checkOptionAdd(parObj, "<?php for($i = 0; $i < 40; $i++) echo " "; ?>", "");
     par.submitForm();
}

function SelectAll(obj)
{
         for (i=0; i<obj.length; i++)
         {
              obj.options[i].selected = true;
         }
}
</script>

<form name="form1" action="index.php" method="post">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td></td>
	</tr>
	<tr>
		<td>
			<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr>
					<td valign="top" nowrap="nowrap" class='tabletext'>
						<?=$iDiscipline['class']?>:
					</td>
					<td width="80%"><?=$select_class?></td>
				</tr>
				<?php if($targetClass != "") { ?>
				<tr>
					<td class="dotline" colspan="2"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" class='tabletext'>
						<span class="tabletext"><?= $iDiscipline['students']?>:</span>
					</td>
					<td width="80%">
						<table width="100%" cellpadding="0" cellspacing="0">
							<tr>
								<td><?= $select_students ?></td>
								<td valign="bottom">
									<table width="100%" border="0" cellspacing="0" cellpadding="6">
										<tr> 
											<td align="left"> 
												<?= $linterface->GET_BTN($button_add, "button", "checkOption(this.form.elements['targetID[]']);AddOptions(this.form.elements['targetID[]'])")?>
											</td>
										</tr>
										<tr> 
											<td align="left"> 
												<?= $linterface->GET_BTN($button_select_all, "button", "SelectAll(this.form.elements['targetID[]']); return false;")?>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<?php } ?>
				<tr>
                	<td class="dotline" colspan="2"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
				<tr>
					<td align="center" colspan="2">
						<?= $linterface->GET_ACTION_BTN($button_close, "button", "self.close();") ?>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<input type="hidden" name="fieldname" value="<?php echo $fieldname; ?>">
</form>
<br />
<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>