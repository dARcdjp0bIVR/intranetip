<?
### Show error message in red if session timeout instead of redirecting to the login page ###
session_start();
if (!isset($_SESSION['UserID'])) {
	$error = "<table border=\"0\" width=\"300\" cellpadding=\"3\" cellspacing=\"0\">";
	$error .= "<tr class=\"tablebluebottom\"><td class=\"tabletoplink\" colspan=\"8\" align=\"right\"><input class=\"formsubbutton\" type=\"button\" value=\"x\" onclick=\"hideMenu('ToolMenu2')\"></td></tr>";
	$error .= "<tr class=\"tablebluetop\">";
	$error .= "<td class=\"tabletoplink\"><span style=\"color:red;\">";
	$error .= "Error encountered, please login again<br /> 錯誤發生，請重新登入</span></td>";
	$error .= "</tr></table>";
	/*$response = iconv("Big5","UTF-8",$error);
	echo $response;*/
	echo $error;
	exit;
}

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

$lidb = new libdb();

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();

$linterface = new interface_html();

$main_content_field = "IF(EnName IS NOT NULL AND EnName !='' AND ChName IS NOT NULL AND ChName !='', IF(IsMain = 1, CONCAT('* ', EnName,' / ',ChName), CONCAT(EnName,' / ',ChName)), IF(EnName IS NOT NULL AND EnName!='',EnName,ChName))";
$main_content_field = "IF($main_content_field='' OR $main_content_field IS NULL,'-',$main_content_field)";

$sql = "SELECT 
				$main_content_field,
				Relation,
				Phone, 
				EmPhone,
				IsMain
		FROM
				$eclass_db.GUARDIAN_STUDENT
		WHERE 
				UserID = '$targetUserID'
		ORDER BY 
				IsMain DESC, Relation ASC
		";
//debug_r($sql);
$temp = $lidb->returnArray($sql,5);

$layer_content = "<table border=\"0\" width=\"300\" cellpadding=\"5\" cellspacing=\"0\">";
$layer_content .= "<tr class=\"tablebluebottom\"><td class=\"tabletoplink\" colspan=\"8\" align=\"right\">".$linterface->GET_BTN("x", "button", "hideMenu('ToolMenu2')")."</td></tr>";
$layer_content .= "<tr class=\"tablebluetop\">";
$layer_content .= "<td class=\"tabletoplink\" width=\"1\">#</td>";
$layer_content .= "<td class=\"tabletoplink\" width=\"35%\" nowrap>$i_UserName</td>";
$layer_content .= "<td class=\"tabletoplink\" width=\"25%\" nowrap>$ec_iPortfolio[relation]</td>";
$layer_content .= "<td class=\"tabletoplink\" width=\"20%\" nowrap>$i_StudentGuardian_Phone</td>";
$layer_content .= "<td class=\"tabletoplink\" width=\"20%\" nowrap>$i_StudentGuardian_EMPhone</td></tr>";


if (sizeof($temp)==0)
	$layer_content .= "<tr><td class=\"tablebluerow2 tabletext\" colspan=\"5\" align=\"center\">$i_no_record_exists_msg</td></tr>";
else
{
	for($i=0; $i<sizeof($temp); $i++)
	{
		list($name, $relation, $phone, $em_phone, $IsMain) = $temp[$i];
		$no = $i+1;
		if($IsMain) {
			$layer_content .= "<tr><td class=\"tablebluerow2 tabletext\"><font color=\"red\">$no</font></td>";
			$layer_content .= "<td class=\"tablebluerow2 tabletext\" nowrap><font color=\"red\">$name</font></td>";
			$layer_content .= "<td class=\"tablebluerow2 tabletext\" nowrap><font color=\"red\">$ec_guardian[$relation]</font></td>";
			$layer_content .= "<td class=\"tablebluerow2 tabletext\"><font color=\"red\">$phone</font></td>";
			$layer_content .= "<td class=\"tablebluerow2 tabletext\"><font color=\"red\">$em_phone</font></td></tr>";
		} else {
			$layer_content .= "<tr><td class=\"tablebluerow2 tabletext\">$no</td>";
			$layer_content .= "<td class=\"tablebluerow2 tabletext\">$name</td>";
			$layer_content .= "<td class=\"tablebluerow2 tabletext\">$ec_guardian[$relation]</td>";
			$layer_content .= "<td class=\"tablebluerow2 tabletext\">$phone</td>";
			$layer_content .= "<td class=\"tablebluerow2 tabletext\">$em_phone</td></tr>";
		}
	}
}

$layer_content .= "<tr><td class=\"tablebluerow2 tabletext\" colspan=\"5\" style='padding-top: 10px'><font color=\"red\">* - $i_StudentGuardian_MainContent</font></td></tr>";
$layer_content .= "</table>";


/*$response = iconv("Big5","UTF-8",$layer_content);

echo $response;*/
echo $layer_content;
intranet_closedb();
?>