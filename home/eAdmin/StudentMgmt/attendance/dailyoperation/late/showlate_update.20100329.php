<?php
# using: kenneth chung

############################################## Change Log ############################################
//
//	Date 	: 20100105 (By Ronald)
//	Details : if it is a late record, will detect is it a serious late record or not.
//
############################################ End of Change Log ########################################

### also need to update /cardapi/attendance/receiver.php 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

# class used
$LIDB = new libdb();

if ($plugin['Discipline']){
	include_once($PATH_WRT_ROOT."includes/libdiscipline.php");

        $ldiscipline = new libdiscipline();
        $LateCtgInUsed = $ldiscipline->CategoryInUsed(1);
}

if ($plugin['Disciplinev12']){
	include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
	include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
    
	$ldisciplinev12 = new libdisciplinev12();
    $LateCtgInUsed = $ldisciplinev12->CategoryInUsed(PRESET_CATEGORY_LATE);
    
    ## Only For IP25 eDis1.2 Only ##
    if($sys_custom['Discipline_SeriousLate']){	
	    $sql = "SELECT CategoryID FROM DISCIPLINE_ACCU_CATEGORY WHERE MeritType = -1 AND LateMinutes IS NOT NULL";
	    $targetCatID = $ldisciplinev12->returnVector($sql);
	    if(sizeof($targetCatID)>0){
	    	for($i=0; $i<sizeof($targetCatID); $i++){
	    		$result = $ldisciplinev12->CategoryInUsed($targetCatID[$i]);
	    		
	    		if($result){
	    			$cnt++;
	    		}
	    	}
	    }
	    if($cnt > 0){
	    	$SeriousLateUsed = 1;
	    }
	}
}

$lcardattend = new libcardstudentattend2();

### Set Date from previous page
$ts_record = strtotime($TargetDate);
if ($ts_record == -1 || ($period!=1 && $period!=2) )
{
    header("Location: index.php");
    exit();
}

$txt_year = date('Y',$ts_record);
$txt_month = date('m',$ts_record);
$txt_day = date('d',$ts_record);
/*$year = getCurrentAcademicYear();
$semester = getCurrentSemester();*/

list($school_year_id, $school_year, $semester_id, $semester) = getAcademicYearInfoAndTermInfoByDate($TargetDate);

### update daily records
$card_log_table_name = "CARD_STUDENT_DAILY_LOG_".$txt_year."_".$txt_month;

$warning_student_ids = array();


for($i=0; $i<sizeOf($user_id); $i++)
{
        $my_user_id = $user_id[$i];
        $student_id = $my_user_id;
        $my_day = $txt_day;
        $my_drop_down_status = $drop_down_status[$i];
        $my_record_id = $record_id[$i];
        // Retrieve Waived only if late
        ($my_drop_down_status == 2) ? ($my_record_status = (${"waived_".$my_user_id} == '') ? 0 : 1) : "";

        # $my_record_status <== waive?

        if( $period == "1")        # AM
        {
            if ($my_drop_down_status == 2)       # Late
            {
	            $inTimesql = "select 
	            		InSchoolTime
	            		from 
	            		$card_log_table_name
						WHERE DayNumber = '$txt_day'
						AND UserID = $my_user_id";
	            $inTimeResult = $lcardattend->returnVector($inTimesql);
	            $InSchoolTime = $inTimeResult[0];
						
                # Reason input
                $txtReason = ${"reason$i"};
                $txtReason = intranet_htmlspecialchars($txtReason);

                # Get ProfileRecordID
                $sql = "SELECT RecordID, ProfileRecordID FROM CARD_STUDENT_PROFILE_RECORD_REASON
                               WHERE RecordDate = '$TargetDate'
                                     AND StudentID = $my_user_id
                                     AND DayType = '".PROFILE_DAY_TYPE_AM."'
                                     AND RecordType = '".PROFILE_TYPE_LATE."'";
                $temp = $lcardattend->returnArray($sql,2);
                
                list($reason_record_id, $reason_profile_id) = $temp[0];
                
                if ($reason_record_id == "") {          # Reason record not exists
                    # Search whether attendance exists by date, student and type
                    $sql = "SELECT StudentAttendanceID FROM PROFILE_STUDENT_ATTENDANCE
                                   WHERE AttendanceDate = '$TargetDate'
                                         AND UserID = $my_user_id
                                         AND DayType = '".PROFILE_DAY_TYPE_AM."'
                                         AND RecordType = '".PROFILE_TYPE_LATE."'";
                    $temp = $lcardattend->returnVector($sql);
                    $attendance_id = $temp[0];
                    
                    if ($attendance_id == "")          # Record not exists
                    {
                        if ($my_record_status != 1) {
                            # Insert profile record
                            $sql = "SELECT ClassName, ClassNumber FROM INTRANET_USER WHERE UserID = $my_user_id";
                            $temp = $lcardattend->returnArray($sql,2);
                            list ($user_classname, $user_classnum) = $temp[0];

                            $fieldname = "UserID, AttendanceDate,Year, Semester, RecordType, DayType, DateInput,DateModified,ClassName,ClassNumber,Reason,AcademicYearID,YearTermID";
                            $fieldvalue = "'$my_user_id','$TargetDate','$school_year','$semester','".PROFILE_TYPE_LATE."','".PROFILE_DAY_TYPE_AM."',now(),now(),'$user_classname','$user_classnum','$txtReason','$school_year_id','$semester_id'";
                            $sql = "INSERT INTO PROFILE_STUDENT_ATTENDANCE ($fieldname) VALUES ($fieldvalue)";
                            $lcardattend->db_db_query($sql);
                            $attendance_id = $lcardattend->db_insert_id();
                    	}
                    	
                        # Calculate upgrade items
                        if ($plugin['Disciplinev12'] && $LateCtgInUsed){
	                        if($attendance_id) {
		                        $semInfo = getAcademicYearAndYearTermByDate($TargetDate);
		                        $acy = new academic_year_term($semInfo[0]);
		                        
		                        $dataAry = array();
								$dataAry['StudentID'] = $my_user_id;
								$dataAry['RecordDate'] = $TargetDate;
								$school_year = GET_ACADEMIC_YEAR_WITH_FORMAT($TargetDate);
								$dataAry['Year'] = $acy->YearNameEN;
								$dataAry['Semester'] = $acy->YearTermNameEN;
								$dataAry['StudentAttendanceID'] = $attendance_id;
								$dataAry['AcademicYearID'] = $acy->AcademicYearID;
								$dataAry['YearTermID'] = $acy->YearTermID;
								$dataAry['Remark'] = $InSchoolTime;
								
								if($SeriousLateUsed == 1) 
								{
									if($InSchoolTime != "")
									{
										$arr_time_table = $lcardattend->Get_Student_Attend_Time_Setting($TargetDate,$my_user_id);
										$school_start_time = $arr_time_table['MorningTime'];
										
										$str_late_minutes = strtotime($InSchoolTime) - strtotime($school_start_time);
										$late_minutes = ceil($str_late_minutes/60);
												
										$sql = "SELECT LateMinutes FROM DISCIPLINE_ACCU_CATEGORY WHERE LateMinutes <= '$late_minutes' ORDER BY LateMinutes DESC LIMIT 0,1";
										$min_late_minutes = $lcardattend->returnVector($sql);
										
										if(sizeof($min_late_minutes)>0)
										{
											$dataAry['SeriousLate'] = 1;
											$dataAry['LateMinutes'] = $late_minutes;
										}
										else
										{
											$dataAry['SeriousLate'] = 0;
										}
									}
									else
									{
										$dataAry['SeriousLate'] = 0;
									}
								}
								//debug("A : ");
								//debug_r($dataAry);
								$ldisciplinev12->INSERT_LATE_MISCONDUCT_RECORD($dataAry);								
							}
                        } 
                        elseif ($plugin['Discipline'] && $LateCtgInUsed) {
                                         $t_date = $TargetDate;
                                     $s_id = $my_user_id;
                                     
                                if($ldiscipline->isUseAccumulativeLateSetting($t_date)){
                                     $ldiscipline->resetAccumulativeUpgradeLateToDemerit($s_id,$t_date);
                                     $ldiscipline->resetAccumulativeUpgradeLateToDetention($s_id,$t_date);
		                            if ($ldiscipline->calculateAccumulativeUpgradeLateToDemerit($student_id,$t_date))
		                            {
		                                $warning_student_ids[] = $student_id;
		                            }
		                            $ldiscipline->calculateAccumulativeUpgradeLateToDetention($student_id,$t_date);
	                                
	                            }
                                else{
                                     $ldiscipline->resetUpgradeLateToDemerit($s_id,$t_date);
                                     $ldiscipline->resetUpgradeLateToDetention($s_id,$t_date);
		                            if ($ldiscipline->calculateUpgradeLateToDemerit($student_id))
		                            {
		                                $warning_student_ids[] = $student_id;
		                            }
		                            $ldiscipline->calculateUpgradeLateToDetention($student_id);
		                        }
                        }
                    }
                    else
                    {
	                    # [PROFILE_STUDENT_ATTENDANCE] exists
                            if ($my_record_status == 1) {
                                     # Added by peter 2006/10/11
                                     # Reset Upgrade items
                                     if ($plugin['Disciplinev12'] && $LateCtgInUsed)
                                     {
											$ldisciplinev12->DELETE_LATE_MISCONDUCT_RECORD($attendance_id);
                                     }
                                     else if ($plugin['Discipline'] && $LateCtgInUsed)
                                     {
                                         $t_date = $TargetDate;
                                         $s_id = $my_user_id;
                                         if($ldiscipline->isUseAccumulativeLateSetting($t_date)){
		                                     $ldiscipline->resetAccumulativeUpgradeLateToDemerit($s_id,$t_date);
		                                     $ldiscipline->resetAccumulativeUpgradeLateToDetention($s_id,$t_date);
		                                
		                            	 }

                                         else{
                                             $ldiscipline->resetUpgradeLateToDemerit($s_id,$t_date);
                                             $ldiscipline->resetUpgradeLateToDetention($s_id,$t_date);
                                         }
                                     }
                                     
                                    // Delete Record if waived
                                    $sql = "DELETE FROM PROFILE_STUDENT_ATTENDANCE WHERE StudentAttendanceID = '$attendance_id'";
                                    $lcardattend->db_db_query($sql);
                                    $attendance_id = "";
                                    # added by peter 2006/10/11
                                     # Calculate upgrade items
                                     if ($plugin['Disciplinev12'] && $LateCtgInUsed)
									{
										# calculation flow done in function DELETE_LATE_MISCONDUCT_RECORD
			                    	}
			                    	else if ($plugin['Discipline'] && $LateCtgInUsed)
                                     {
                                         $t_date = $TargetDate;
                                         if($ldiscipline->isUseAccumulativeLateSetting($t_date)){
					                            if ($ldiscipline->calculateAccumulativeUpgradeLateToDemerit($s_id,$t_date))
					                            {
					                                $warning_student_ids[] = $s_id;
					                            }
					                            $ldiscipline->calculateAccumulativeUpgradeLateToDetention($s_id,$t_date);
				                                
				                         }
                                         else{
                                                 if ($ldiscipline->calculateUpgradeLateToDemerit($s_id))
                                                 {
                                                     $warning_student_ids[] = $s_id;
                                                 }
                                                 $ldiscipline->calculateUpgradeLateToDetention($s_id);
                                         }
                                     }
                            }
                            else {
                                    # Update Reason in profile record By AttendanceID
                                    $sql = "UPDATE PROFILE_STUDENT_ATTENDANCE SET Reason = '$txtReason' WHERE StudentAttendanceID = '$attendance_id'";
                                    $lcardattend->db_db_query($sql);
                            }
                    }

                    # added on 2007-04-30
                    # remove previous absent record from Reason table ( if exists ) 
                    $sql="DELETE FROM CARD_STUDENT_PROFILE_RECORD_REASON WHERE RecordDate='$TargetDate' AND StudentID='$my_user_id' AND DayType='".PROFILE_DAY_TYPE_AM."' AND RecordType='".PROFILE_TYPE_ABSENT."'";
                    $lcardattend->db_db_query($sql);
                    
                    # added on 2007-07-10
                    # remove previous absent record from PROFILE table ( if exists ) 
                    $sql="DELETE FROM PROFILE_STUDENT_ATTENDANCE WHERE AttendanceDate='$TargetDate' AND UserID='$my_user_id' AND DayType='".PROFILE_DAY_TYPE_AM."' AND RecordType='".PROFILE_TYPE_ABSENT."'";
                    $lcardattend->db_db_query($sql);

                    
                    # Insert to Reason table
                    $fieldname = "RecordDate, StudentID, ProfileRecordID, Reason, RecordType, DayType, RecordStatus, DateInput, DateModified";
                    $fieldsvalues = "'$TargetDate', '$my_user_id', '$attendance_id','$txtReason', '".PROFILE_TYPE_LATE."', '".PROFILE_DAY_TYPE_AM."', '$my_record_status', now(), now() ";
                    $sql = "INSERT INTO CARD_STUDENT_PROFILE_RECORD_REASON ($fieldname)
                                   VALUES ($fieldsvalues)";
                    $lcardattend->db_db_query($sql);
                }
                else  # Reason record exists
                {
                    if ($reason_profile_id == "")    # Profile ID not exists
                    {
                        # Search whether attendance exists by date, student and type
                        $sql = "SELECT StudentAttendanceID FROM PROFILE_STUDENT_ATTENDANCE
                                       WHERE AttendanceDate = '$TargetDate'
                                             AND UserID = $my_user_id
                                             AND DayType = '".PROFILE_DAY_TYPE_AM."'
                                             AND RecordType = '".PROFILE_TYPE_LATE."'";
                        $temp = $lcardattend->returnVector($sql);
                        $attendance_id = $temp[0];

                        if ($attendance_id == "")          # Record not exists
                        {
                            if ($my_record_status != 1)	# not waive
                            {
                                # Insert profile record
                                $sql = "SELECT ClassName, ClassNumber FROM INTRANET_USER WHERE UserID = $my_user_id";
                                $temp = $lcardattend->returnArray($sql,2);
                                list ($user_classname, $user_classnum) = $temp[0];
                                $fieldname = "UserID, AttendanceDate,Year, Semester, RecordType, DayType, DateInput,DateModified,ClassName,ClassNumber,Reason,AcademicYearID,YearTermID";
                                $fieldvalue = "'$my_user_id','$TargetDate','$school_year','$semester','".PROFILE_TYPE_LATE."','".PROFILE_DAY_TYPE_AM."',now(),now(),'$user_classname','$user_classnum','$txtReason','$school_year_id','$semester_id'";
                                $sql = "INSERT INTO PROFILE_STUDENT_ATTENDANCE ($fieldname) VALUES ($fieldvalue)";
                                $lcardattend->db_db_query($sql);
                                $attendance_id = $lcardattend->db_insert_id();
                        	}
                        	
                            # Calculate upgrade items
                            if ($plugin['Disciplinev12'] && $LateCtgInUsed)
	                        {
		                        if($attendance_id)
		                        {
									$semInfo = getAcademicYearAndYearTermByDate($TargetDate);
			                        $acy = new academic_year_term($semInfo[0]);
			                        
			                        $dataAry = array();
									$dataAry['StudentID'] = $my_user_id;
									$dataAry['RecordDate'] = $TargetDate;
									$dataAry['Year'] = $acy->YearNameEN;
									$dataAry['Semester'] = $acy->YearTermNameEN;
									$dataAry['StudentAttendanceID'] = $attendance_id;
									$dataAry['AcademicYearID'] = $acy->AcademicYearID;
									$dataAry['YearTermID'] = $acy->YearTermID;
									$dataAry['Remark'] = $InSchoolTime;
									
									if($SeriousLateUsed == 1) {
										if($InSchoolTime != "")
										{
											$arr_time_table = $lcardattend->Get_Student_Attend_Time_Setting($TargetDate,$my_user_id);
											$school_start_time = $arr_time_table['MorningTime'];
											
											$str_late_minutes = strtotime($InSchoolTime) - strtotime($school_start_time);
											$late_minutes = ceil($str_late_minutes/60);
												
											$sql = "SELECT LateMinutes FROM DISCIPLINE_ACCU_CATEGORY WHERE LateMinutes <= '$late_minutes' ORDER BY LateMinutes DESC LIMIT 0,1";
											$min_late_minutes = $lcardattend->returnVector($sql);		
											
											if(sizeof($min_late_minutes)>0)
											{
												$dataAry['SeriousLate'] = 1;
												$dataAry['LateMinutes'] = $late_minutes;
											}
											else
											{
												$dataAry['SeriousLate'] = 0;
											}
										}
										else
										{
											$dataAry['SeriousLate'] = 0;
										}
									}
									//debug("B : ");
									//debug_r($dataAry);
									$ldisciplinev12->INSERT_LATE_MISCONDUCT_RECORD($dataAry);
								}
	                        } 
	                        else if ($plugin['Discipline'] && $LateCtgInUsed)
                            {
                                $t_date = $TargetDate;
                                $s_id = $my_user_id;
	                                if($ldiscipline->isUseAccumulativeLateSetting($t_date)){
	                                     $ldiscipline->resetAccumulativeUpgradeLateToDemerit($s_id,$t_date);
	                                     $ldiscipline->resetAccumulativeUpgradeLateToDetention($s_id,$t_date);
			                            if ($ldiscipline->calculateAccumulativeUpgradeLateToDemerit($student_id,$t_date))
			                            {
			                                $warning_student_ids[] = $student_id;
			                            }
			                            $ldiscipline->calculateAccumulativeUpgradeLateToDetention($student_id,$t_date);
		                                
		                            }                                    
									else{                                                             
                                                 $ldiscipline->resetUpgradeLateToDemerit($s_id,$t_date);
                                                 $ldiscipline->resetUpgradeLateToDetention($s_id,$t_date);
				                                if ($ldiscipline->calculateUpgradeLateToDemerit($student_id))
				                                {
				                                    $warning_student_ids[] = $student_id;
				                                }
				                                $ldiscipline->calculateUpgradeLateToDetention($student_id);
			                       }
                            }
                        }
                        else
                        {
                            if ($my_record_status == 1)	# waive
                            {
                                   # Added by peter 2006/10/11
                                     # Reset Upgrade items
                                     if ($plugin['Disciplinev12'] && $LateCtgInUsed)
                                     {
											$ldisciplinev12->DELETE_LATE_MISCONDUCT_RECORD($attendance_id);
                                     }
                                     else if ($plugin['Discipline'] && $LateCtgInUsed)
                                     {
                                         $t_date = $TargetDate;
                                         $s_id = $my_user_id;
                                         if($ldiscipline->isUseAccumulativeLateSetting($t_date)){
		                                     $ldiscipline->resetAccumulativeUpgradeLateToDemerit($s_id,$t_date);
		                                     $ldiscipline->resetAccumulativeUpgradeLateToDetention($s_id,$t_date);
	                                     }else{
                                             $ldiscipline->resetUpgradeLateToDemerit($s_id,$t_date);
                                             $ldiscipline->resetUpgradeLateToDetention($s_id,$t_date);
                                         }
                                     }
                                                             
                                    // Delete Record if waived
                                    $sql = "DELETE FROM PROFILE_STUDENT_ATTENDANCE WHERE StudentAttendanceID = '$attendance_id'";
                                    $lcardattend->db_db_query($sql);
                                    $attendance_id = "";
                                    # added by peter 2006/10/11
                                     # Calculate upgrade items
                                     if ($plugin['Disciplinev12'] && $LateCtgInUsed)
									{
										# calculation flow done in function DELETE_LATE_MISCONDUCT_RECORD
			                    	}
			                    	else if ($plugin['Discipline'] && $LateCtgInUsed)
                                     {
                                         $t_date = $TargetDate;
                                         if($ldiscipline->isUseAccumulativeLateSetting($t_date)){
					                            if ($ldiscipline->calculateAccumulativeUpgradeLateToDemerit($s_id,$t_date))
					                            {
					                                $warning_student_ids[] = $s_id;
					                            }
					                            $ldiscipline->calculateAccumulativeUpgradeLateToDetention($s_id,$t_date);
		                                
	                            		}else{
                                         
                                             if ($ldiscipline->calculateUpgradeLateToDemerit($s_id))
                                             {
                                                 $warning_student_ids[] = $s_id;
                                             }
                                             $ldiscipline->calculateUpgradeLateToDetention($s_id);
                                        }
                                     }
                            }
                            else
                            {
                                    # Update Reason in profile record By AttendanceID
        $sql = "UPDATE PROFILE_STUDENT_ATTENDANCE SET Reason = '$txtReason' WHERE StudentAttendanceID = '$attendance_id'";
        $lcardattend->db_db_query($sql);
                                }
                        }
                    }
                    else  # Has Profile ID
                    {
                        # Search Attendance By ID
                        $sql = "SELECT StudentAttendanceID, RecordType FROM PROFILE_STUDENT_ATTENDANCE
                                       WHERE StudentAttendanceID = $reason_profile_id";
                        $temp = $lcardattend->returnArray($sql);
                        list($attendance_id, $ori_record_type) = $temp[0];
                        //$attendance_id = $temp[0];
                        
                        if ($attendance_id == "")          # Record not exists
                        {
                            # Search attendance by date, student and type
                            $sql = "SELECT StudentAttendanceID FROM PROFILE_STUDENT_ATTENDANCE
                                           WHERE AttendanceDate = '$TargetDate'
                                                 AND UserID = $my_user_id
                                                 AND DayType = '".PROFILE_DAY_TYPE_AM."'
                                                 AND RecordType = '".PROFILE_TYPE_LATE."'";
                            $temp = $lcardattend->returnVector($sql);
                            $attendance_id = $temp[0];

                            if ($attendance_id == "")          # Record not exists
                            {
                                    if ($my_record_status != 1)	# not waive
                                    {
                                        # insert reason in profile record
                                        $sql = "SELECT ClassName, ClassNumber FROM INTRANET_USER WHERE UserID = $my_user_id";
                                        $temp = $lcardattend->returnArray($sql,2);
                                        list ($user_classname, $user_classnum) = $temp[0];
                                        $fieldname = "UserID, AttendanceDate,Year, Semester, RecordType, DayType, DateInput,DateModified,ClassName,ClassNumber,Reason,AcademicYearID,YearTermID";
                                        $fieldvalue = "'$my_user_id','$TargetDate','$school_year','$semester','".PROFILE_TYPE_LATE."','".PROFILE_DAY_TYPE_AM."',now(),now(),'$user_classname','$user_classnum','$txtReason','$school_year_id','$semester_id'";
                                        $sql = "INSERT INTO PROFILE_STUDENT_ATTENDANCE ($fieldname) VALUES ($fieldvalue)";
                                        $lcardattend->db_db_query($sql);
                                        $attendance_id = $lcardattend->db_insert_id();
                                	}

                                # Calculate upgrade items
                                if ($plugin['Disciplinev12'] && $LateCtgInUsed)
		                        {
			                        if($attendance_id)
			                        {
										$semInfo = getAcademicYearAndYearTermByDate($TargetDate);
				                        $acy = new academic_year_term($semInfo[0]);
				                        
				                        $dataAry = array();
										$dataAry['StudentID'] = $my_user_id;
										$dataAry['RecordDate'] = $TargetDate;
										$school_year = GET_ACADEMIC_YEAR_WITH_FORMAT($TargetDate);
										$dataAry['Year'] = $acy->YearNameEN;
										$dataAry['Semester'] = $acy->YearTermNameEN;
										$dataAry['StudentAttendanceID'] = $attendance_id;
										$dataAry['AcademicYearID'] = $acy->AcademicYearID;
										$dataAry['YearTermID'] = $acy->YearTermID;
										$dataAry['Remark'] = $InSchoolTime;
										
										if($SeriousLateUsed == 1) {
											if($InSchoolTime != "")
											{
												$arr_time_table = $lcardattend->Get_Student_Attend_Time_Setting($TargetDate,$my_user_id);
												$school_start_time = $arr_time_table['MorningTime'];
												
												$str_late_minutes = strtotime($InSchoolTime) - strtotime($school_start_time);
												$late_minutes = ceil($str_late_minutes/60);
																	
												$sql = "SELECT LateMinutes FROM DISCIPLINE_ACCU_CATEGORY WHERE LateMinutes <= '$late_minutes' ORDER BY LateMinutes DESC LIMIT 0,1";
												$min_late_minutes = $lcardattend->returnVector($sql);		
												
												if(sizeof($min_late_minutes)>0)
												{
													$dataAry['SeriousLate'] = 1;
													$dataAry['LateMinutes'] = $late_minutes;
												}
												else
												{
													$dataAry['SeriousLate'] = 0;
												}
											}
											else
											{
												$dataAry['SeriousLate'] = 0;
											}
										}
										//debug("C : ");
										//debug_r($dataAry);
										$ldisciplinev12->INSERT_LATE_MISCONDUCT_RECORD($dataAry);
									}
		                        } 
		                        else if ($plugin['Discipline'] && $LateCtgInUsed)
                                {
                                     $t_date = $TargetDate;
                                     $s_id = $my_user_id;
                                     
                                    if($ldiscipline->isUseAccumulativeLateSetting($t_date)){
	                                    
		                                     $ldiscipline->resetAccumulativeUpgradeLateToDemerit($s_id,$t_date);
		                                     $ldiscipline->resetAccumulativeUpgradeLateToDetention($s_id,$t_date);
				                            if ($ldiscipline->calculateAccumulativeUpgradeLateToDemerit($s_id,$t_date))
				                            {
				                                $warning_student_ids[] = $s_id;
				                            }
				                            $ldiscipline->calculateAccumulativeUpgradeLateToDetention($s_id,$t_date);
	                                
	                            	}
                                      else{
                                             $ldiscipline->resetUpgradeLateToDemerit($s_id,$t_date);
                                             $ldiscipline->resetUpgradeLateToDetention($s_id,$t_date);
		                                    if ($ldiscipline->calculateUpgradeLateToDemerit($student_id))
		                                    {
		                                        $warning_student_ids[] = $student_id;
		                                    }
		                                    $ldiscipline->calculateUpgradeLateToDetention($student_id);
                                    }
                                }
                            }
                            else
                            {
                                    if ($my_record_status == 1)	# waive
                                    {
                                        # Added by peter 2006/10/11
                                         # Reset Upgrade items
                                         if ($plugin['Disciplinev12'] && $LateCtgInUsed)
	                                     {
												$ldisciplinev12->DELETE_LATE_MISCONDUCT_RECORD($attendance_id);
	                                     }
	                                     else if ($plugin['Discipline'] && $LateCtgInUsed)
                                         {
                                             $t_date = $TargetDate;
                                             $s_id = $my_user_id;
                                             if($ldiscipline->isUseAccumulativeLateSetting($t_date)){
				                                     $ldiscipline->resetAccumulativeUpgradeLateToDemerit($s_id,$t_date);
				                                     $ldiscipline->resetAccumulativeUpgradeLateToDetention($s_id,$t_date);
    
											}else{

                                             $ldiscipline->resetUpgradeLateToDemerit($s_id,$t_date);
                                             $ldiscipline->resetUpgradeLateToDetention($s_id,$t_date);
                                           }
                                         }
                                                    // Delete Record if waived
                                            $sql = "DELETE FROM PROFILE_STUDENT_ATTENDANCE WHERE StudentAttendanceID = '$attendance_id'";
                                            $lcardattend->db_db_query($sql);
                                            $attendance_id = NULL;
                                            # added by peter 2006/10/11
                                             # Calculate upgrade items
                                             if ($plugin['Disciplinev12'] && $LateCtgInUsed)
											{
												# calculation flow done in function DELETE_LATE_MISCONDUCT_RECORD
					                    	}
					                    	else if ($plugin['Discipline'] && $LateCtgInUsed)
                                             {
                                                 $t_date = $TargetDate;
                                                if($ldiscipline->isUseAccumulativeLateSetting($t_date)){
						                            if ($ldiscipline->calculateAccumulativeUpgradeLateToDemerit($s_id,$t_date))
						                            {
						                                $warning_student_ids[] = $s_id;
						                            }
						                            $ldiscipline->calculateAccumulativeUpgradeLateToDetention($s_id,$t_date);
					                                
					                            }else{
                                                     if ($ldiscipline->calculateUpgradeLateToDemerit($s_id))
                                                     {
                                                         $warning_student_ids[] = $s_id;
                                                     }
                                                     $ldiscipline->calculateUpgradeLateToDetention($s_id);
                                                 }
                                             }
                                    }
                                    else
                                    {
                                        # Update Reason in profile record By AttendanceID
                                        $sql = "UPDATE PROFILE_STUDENT_ATTENDANCE SET Reason = '$txtReason'
                                               WHERE StudentAttendanceID = '$attendance_id'";
                                        $lcardattend->db_db_query($sql);
                                    }
                            }
                        }
                        else # profile record exists and valid
                        {
                                if ($my_record_status == 1)		# waive late record
                            {
                                    # Added by peter 2006/10/11
                                     # Reset Upgrade items
                                     if ($plugin['Disciplinev12'] && $LateCtgInUsed)
                                     {
	                                     # delete late record and misconduct record
	                                     $ldisciplinev12->DELETE_LATE_MISCONDUCT_RECORD($attendance_id);
                                     }
                                     else if ($plugin['Discipline'] && $LateCtgInUsed)
                                     {
                                         $t_date = $TargetDate;
                                         $s_id = $my_user_id;
                                        if($ldiscipline->isUseAccumulativeLateSetting($t_date)){
		                                     $ldiscipline->resetAccumulativeUpgradeLateToDemerit($s_id,$t_date);
		                                     $ldiscipline->resetAccumulativeUpgradeLateToDetention($s_id,$t_date);
                
            							}
                                         else{
                                                 $ldiscipline->resetUpgradeLateToDemerit($s_id,$t_date);
                                                 $ldiscipline->resetUpgradeLateToDetention($s_id,$t_date);
                                         }
                                     }
                                    // Delete Record if waived
                                                                $sql = "DELETE FROM PROFILE_STUDENT_ATTENDANCE WHERE StudentAttendanceID = '$attendance_id'";
                                                                $lcardattend->db_db_query($sql);
                                                                $attendance_id = NULL;
                                         # added by peter 2006/10/11
                                         # Calculate upgrade items
                                         if ($plugin['Disciplinev12'] && $LateCtgInUsed)
                                         {
	                                         # calculation flow done in function DELETE_LATE_MISCONDUCT_RECORD
                                         }
                                         else if ($plugin['Discipline'] && $LateCtgInUsed)
                                         {
                                             $t_date = $TargetDate;
                                               if($ldiscipline->isUseAccumulativeLateSetting($t_date)){
						                            if ($ldiscipline->calculateAccumulativeUpgradeLateToDemerit($s_id,$t_date))
						                            {
						                                $warning_student_ids[] = $s_id;
						                            }
						                            $ldiscipline->calculateAccumulativeUpgradeLateToDetention($s_id,$t_date);
            
            									}
                                             else{
                                                     if ($ldiscipline->calculateUpgradeLateToDemerit($s_id))
                                                     {
                                                         $warning_student_ids[] = $s_id;
                                                     }
                                                     $ldiscipline->calculateUpgradeLateToDetention($s_id);
                                             }
                                         }
                            }
                            else
                            {
                                    # Update reason in profile record
                                    $sql = "UPDATE PROFILE_STUDENT_ATTENDANCE SET Reason = '$txtReason'
                                           WHERE StudentAttendanceID = '$attendance_id'";
                                    $lcardattend->db_db_query($sql);
                                    
                                    # 2008/05/07
                                    # Reset Upgrade items
                                    if ($plugin['Disciplinev12'] && $LateCtgInUsed)
                                    {
	                                    # late > late, no need to re-group
                                    }
                                    else if ($plugin['Discipline'] && $LateCtgInUsed)
                                     {
                                         $t_date = $TargetDate;
                                         $s_id = $my_user_id;
                                        if($ldiscipline->isUseAccumulativeLateSetting($t_date)){
		                                     $ldiscipline->resetAccumulativeUpgradeLateToDemerit($s_id,$t_date);
		                                     $ldiscipline->resetAccumulativeUpgradeLateToDetention($s_id,$t_date);
                
            							}
                                         else{
                                                 $ldiscipline->resetUpgradeLateToDemerit($s_id,$t_date);
                                                 $ldiscipline->resetUpgradeLateToDetention($s_id,$t_date);
                                         }
                                     }
                                                   
                                     # Calculate upgrade items      
                                     if ($plugin['Disciplinev12'] && $LateCtgInUsed)
                                     {
	                                     # late > late, no need to re-group
                                     }
                                    else if ($plugin['Discipline'] && $LateCtgInUsed)
                                     {
                                         $t_date = $TargetDate;
                                         $s_id = $my_user_id;
                                           if($ldiscipline->isUseAccumulativeLateSetting($t_date)){
					                            if ($ldiscipline->calculateAccumulativeUpgradeLateToDemerit($s_id,$t_date))
					                            {
					                                $warning_student_ids[] = $s_id;
					                            }
					                            $ldiscipline->calculateAccumulativeUpgradeLateToDetention($s_id,$t_date);
        
        									}
                                         else{
                                                 if ($ldiscipline->calculateUpgradeLateToDemerit($s_id))
                                                 {
                                                     $warning_student_ids[] = $s_id;
                                                 }
                                                 $ldiscipline->calculateUpgradeLateToDetention($s_id);
                                         }
                                     }
                                }
                        }
                    }
                    
                    # added on 2007-04-30
                    # remove previous absent record from Reason table ( if exists ) 
                    $sql="DELETE FROM CARD_STUDENT_PROFILE_RECORD_REASON WHERE RecordDate='$TargetDate' AND StudentID='$my_user_id' AND DayType='".PROFILE_DAY_TYPE_AM."' AND RecordType='".PROFILE_TYPE_ABSENT."'";
                    $lcardattend->db_db_query($sql);
                    
                    # added on 2007-07-10
                    # remove previous absent record from PROFILE table ( if exists ) 
                    $sql="DELETE FROM PROFILE_STUDENT_ATTENDANCE WHERE AttendanceDate='$TargetDate' AND UserID='$my_user_id' AND DayType='".PROFILE_DAY_TYPE_AM."' AND RecordType='".PROFILE_TYPE_ABSENT."'";
                    $lcardattend->db_db_query($sql);

                    
                    # Update reason table record
                    $attendance_id = ($my_record_status == 1) ? NULL : $attendance_id;
                    $sql = "UPDATE CARD_STUDENT_PROFILE_RECORD_REASON SET Reason = '$txtReason',
                                   ProfileRecordID = '$attendance_id',
                                   RecordStatus = '$my_record_status'
                                   WHERE RecordID= '$reason_record_id'";
                    $lcardattend->db_db_query($sql);
                }
            }
            else if ($my_drop_down_status == 0)    # On Time
            {
	            
	            # Search whether attendance exists by date, student and type
                $sql = "SELECT StudentAttendanceID FROM PROFILE_STUDENT_ATTENDANCE
                               WHERE AttendanceDate = '$TargetDate'
                                     AND UserID = $my_user_id
                                     AND DayType = '".PROFILE_DAY_TYPE_AM."'
                                     AND RecordType = '".PROFILE_TYPE_LATE."'";
                $temp = $lcardattend->returnVector($sql);
                $attendance_id = $temp[0];
                
                 # Remove Reason record
                 $sql = "DELETE FROM CARD_STUDENT_PROFILE_RECORD_REASON
                               WHERE RecordDate = '$TargetDate'
                                     AND StudentID = $my_user_id
                                     AND DayType = '".PROFILE_DAY_TYPE_AM."'
                                     AND RecordType = '".PROFILE_TYPE_LATE."'";
                 $lcardattend->db_db_query($sql);

                 # Reset Upgrade items
                 if ($plugin['Disciplinev12'] && $LateCtgInUsed)
                 {
						$ldisciplinev12->DELETE_LATE_MISCONDUCT_RECORD($attendance_id);
                 }
                 else if ($plugin['Discipline'] && $LateCtgInUsed)
                 {
                     $target_date = $TargetDate;
                     $student_id = $my_user_id;
                     if($ldiscipline->isUseAccumulativeLateSetting($target_date)){
                                     $ldiscipline->resetAccumulativeUpgradeLateToDemerit($student_id,$target_date);
                                     $ldiscipline->resetAccumulativeUpgradeLateToDetention($student_id,$target_date);
		                            if ($ldiscipline->calculateAccumulativeUpgradeLateToDemerit($student_id,$target_date))
		                            {
		                                $warning_student_ids[] = $student_id;
		                            }
		                            $ldiscipline->calculateAccumulativeUpgradeLateToDetention($student_id,$target_date);
	                                
	                 }                     
                     else{
	                     $ldiscipline->resetUpgradeLateToDemerit($student_id,$target_date);
	                     $ldiscipline->resetUpgradeLateToDetention($student_id,$target_date);
                     }
                 }

                 # Remove Profile record
                 $sql = "DELETE FROM PROFILE_STUDENT_ATTENDANCE
                               WHERE AttendanceDate = '$TargetDate'
                                     AND UserID = $my_user_id
                                     AND DayType = '".PROFILE_DAY_TYPE_AM."'
                                     AND RecordType = '".PROFILE_TYPE_LATE."'";
                 $lcardattend->db_db_query($sql);
                 # Calculate upgrade items
                 if ($plugin['Disciplinev12'] && $LateCtgInUsed)
				{
					# calculation flow done in function DELETE_LATE_MISCONDUCT_RECORD
            	}
            	else if ($plugin['Discipline'] && $LateCtgInUsed)
                 {  
	                 $t_date = $TargetDate;
                     if($ldiscipline->isUseAccumulativeLateSetting($t_date)){
		                            if ($ldiscipline->calculateAccumulativeUpgradeLateToDemerit($student_id,$t_date))
		                            {
		                                $warning_student_ids[] = $student_id;
		                            }
		                            $ldiscipline->calculateAccumulativeUpgradeLateToDetention($student_id,$t_date);
                     }	                 
	                 else{
	                     if ($ldiscipline->calculateUpgradeLateToDemerit($student_id))
	                     {
	                         $warning_student_ids[] = $student_id;
	                     }
	                     $ldiscipline->calculateUpgradeLateToDetention($student_id);
                     }
                 }
                 # Set to Daily Record Table
                 # Set AMStatus and InSchoolTime
                 /*
                 $sql = "UPDATE $card_log_table_name
                                SET AMStatus = '".CARD_STATUS_PRESENT."',
                                InSchoolTime = NULL, DateModified = NOW()
                                WHERE DayNumber = '$txt_day'
                                      AND UserID = $my_user_id";
                  */
                  $sql = "UPDATE $card_log_table_name
                                SET AMStatus = '".CARD_STATUS_PRESENT."',
                                 DateModified = NOW()
                                WHERE DayNumber = '$txt_day'
                                      AND UserID = $my_user_id";

                 $lcardattend->db_db_query($sql);
            }
            else # Unknown action
            {
                 # Do nthg
            }
        }
        else if ($period == "2") # PM
        {
            if ($my_drop_down_status == 2)       # Late
            {
				$lcardattend->retrieveSettings();
				if($lcardattend->attendance_mode==1)	# retrieve InSchoolTime
				{
					$field = "InSchoolTime";
				}
				else # retrieve LunchBackTime
				{
					$field = "LunchBackTime";
				}
				$inTimesql = "select $field from $card_log_table_name WHERE DayNumber = '$txt_day' AND UserID = $my_user_id";
	            $inTimeResult = $lcardattend->returnVector($inTimesql);
	            $InSchoolTime = $inTimeResult[0];
				
                # Reason input
                $txtReason = ${"reason$i"};
                $txtReason = intranet_htmlspecialchars($txtReason);

                # Get ProfileRecordID
                $sql = "SELECT RecordID, ProfileRecordID FROM CARD_STUDENT_PROFILE_RECORD_REASON
                               WHERE RecordDate = '$TargetDate'
                                     AND StudentID = $my_user_id
                                     AND DayType = '".PROFILE_DAY_TYPE_PM."'
                                     AND RecordType = '".PROFILE_TYPE_LATE."'";
                $temp = $lcardattend->returnArray($sql,2);
                //debug_r($temp); 
                list($reason_record_id, $reason_profile_id) = $temp[0];

                if ($reason_record_id == "")           # Reason record not exists
                {
                    # Search whether attendance exists by date, student and type
                    $sql = "SELECT StudentAttendanceID FROM PROFILE_STUDENT_ATTENDANCE
                                   WHERE AttendanceDate = '$TargetDate'
                                         AND UserID = $my_user_id
                                         AND DayType = '".PROFILE_DAY_TYPE_PM."'
                                         AND RecordType = '".PROFILE_TYPE_LATE."'";
                    $temp = $lcardattend->returnVector($sql);
                    //debug_r($sql); die;
                    $attendance_id = $temp[0];
                    if ($attendance_id == "")          # Record not exists
                    {
                            if ($my_record_status != 1)	# not waive
                            {
                                # Insert profile record
                                $sql = "SELECT ClassName, ClassNumber FROM INTRANET_USER WHERE UserID = $my_user_id";
                                $temp = $lcardattend->returnArray($sql,2);
                                list ($user_classname, $user_classnum) = $temp[0];
                                $fieldname = "UserID, AttendanceDate,Year, Semester, RecordType, DayType, DateInput,DateModified,ClassName,ClassNumber,Reason,AcademicYearID,YearTermID";
                                $fieldvalue = "'$my_user_id','$TargetDate','$school_year','$semester','".PROFILE_TYPE_LATE."','".PROFILE_DAY_TYPE_PM."',now(),now(),'$user_classname','$user_classnum','$txtReason','$school_year_id','$semester_id'";
                                $sql = "INSERT INTO PROFILE_STUDENT_ATTENDANCE ($fieldname) VALUES ($fieldvalue)";
                                $lcardattend->db_db_query($sql);
                                $attendance_id = $lcardattend->db_insert_id();
                        	}

                        # Calculate upgrade items
                        if ($plugin['Disciplinev12'] && $LateCtgInUsed)
                        {
	                        if($attendance_id)
	                        {
								$semInfo = getAcademicYearAndYearTermByDate($TargetDate);
		                        $acy = new academic_year_term($semInfo[0]);
		                        
		                        $dataAry = array();
								$dataAry['StudentID'] = $my_user_id;
								$dataAry['RecordDate'] = $TargetDate;
								$school_year = GET_ACADEMIC_YEAR_WITH_FORMAT($TargetDate);
								$dataAry['Year'] = $acy->YearNameEN;
								$dataAry['Semester'] = $acy->YearTermNameEN;
								$dataAry['StudentAttendanceID'] = $attendance_id;
								$dataAry['AcademicYearID'] = $acy->AcademicYearID;
								$dataAry['YearTermID'] = $acy->YearTermID;
								$dataAry['Remark'] = $InSchoolTime;
								
								if($SeriousLateUsed == 1) {
									if($InSchoolTime != "")
									{
										$arr_time_table = $lcardattend->Get_Student_Attend_Time_Setting($TargetDate,$my_user_id);
										
										if($lcardattend->attendance_mode==1)	# retrieve InSchoolTime
										{
											##$field = "InSchoolTime";
											$school_start_time = $arr_time_table['MorningTime'];
										}
										else # retrieve LunchBackTime
										{
											##$field = "LunchBackTime";
											$school_start_time = $arr_time_table['LunchEnd'];
										}
										
										$str_late_minutes = strtotime($InSchoolTime) - strtotime($school_start_time);
										$late_minutes = ceil($str_late_minutes/60);
												
										$sql = "SELECT LateMinutes FROM DISCIPLINE_ACCU_CATEGORY WHERE LateMinutes <= '$late_minutes' ORDER BY LateMinutes DESC LIMIT 0,1";
										$min_late_minutes = $lcardattend->returnVector($sql);
										
										if(sizeof($min_late_minutes)>0)
										{
											$dataAry['SeriousLate'] = 1;
											$dataAry['LateMinutes'] = $late_minutes;
										}
										else
										{
											$dataAry['SeriousLate'] = 0;
										}
									}
									else
									{
										$dataAry['SeriousLate'] = 0;
									}
								}
								//debug("D : ");
								//debug_r($dataAry);
								$ldisciplinev12->INSERT_LATE_MISCONDUCT_RECORD($dataAry);
							}
                        } 
                        else if ($plugin['Discipline'] && $LateCtgInUsed)
                        {
                                 $t_date = $TargetDate;
                                 $s_id = $my_user_id;
                            if($ldiscipline->isUseAccumulativeLateSetting($t_date)){
                                     $ldiscipline->resetAccumulativeUpgradeLateToDemerit($s_id,$t_date);
                                     $ldiscipline->resetAccumulativeUpgradeLateToDetention($s_id,$t_date);
		                            if ($ldiscipline->calculateAccumulativeUpgradeLateToDemerit($s_id,$t_date))
		                            {
		                                $warning_student_ids[] = $s_id;
		                            }
		                            $ldiscipline->calculateAccumulativeUpgradeLateToDetention($s_id,$t_date);
	                                
	                         }                            
                            else{         
                                     $ldiscipline->resetUpgradeLateToDemerit($s_id,$t_date);
                                     $ldiscipline->resetUpgradeLateToDetention($s_id,$t_date);
		                            if ($ldiscipline->calculateUpgradeLateToDemerit($student_id))
		                            {
		                                $warning_student_ids[] = $student_id;
		                            }
		                            $ldiscipline->calculateUpgradeLateToDetention($student_id);
                            }
                        }
                    }
                    else
                    {
                            if ($my_record_status == 1)	# waive
                            {
                                # Added by peter 2006/10/11
                                 # Reset Upgrade items
                                 if ($plugin['Disciplinev12'] && $LateCtgInUsed)
                                 {
										$ldisciplinev12->DELETE_LATE_MISCONDUCT_RECORD($attendance_id);
                                 }
                                 else if ($plugin['Discipline'] && $LateCtgInUsed)
                                 {
                                     $t_date = $TargetDate;
                                     $s_id = $my_user_id;
                                   if($ldiscipline->isUseAccumulativeLateSetting($t_date)){
	                                     $ldiscipline->resetAccumulativeUpgradeLateToDemerit($s_id,$t_date);
	                                     $ldiscipline->resetAccumulativeUpgradeLateToDetention($s_id,$t_date);

									}
                                     else{
                                         $ldiscipline->resetUpgradeLateToDemerit($s_id,$t_date);
                                         $ldiscipline->resetUpgradeLateToDetention($s_id,$t_date);
                                     }
                                 }
                                            // Delete Record if waived
                                    $sql = "DELETE FROM PROFILE_STUDENT_ATTENDANCE WHERE StudentAttendanceID = '$attendance_id'";
                                    $lcardattend->db_db_query($sql);
                                    $attendance_id = "";
                                    # added by peter 2006/10/11
                                     # Calculate upgrade items
                                     if ($plugin['Disciplinev12'] && $LateCtgInUsed)
									{
										# calculation flow done in function DELETE_LATE_MISCONDUCT_RECORD
			                    	}
			                    	else if ($plugin['Discipline'] && $LateCtgInUsed)
                                     {
                                         $t_date = $TargetDate;
			                               if($ldiscipline->isUseAccumulativeLateSetting($t_date)){
					                            if ($ldiscipline->calculateAccumulativeUpgradeLateToDemerit($s_id,$t_date))
					                            {
					                                $warning_student_ids[] = $s_id;
					                            }
					                            $ldiscipline->calculateAccumulativeUpgradeLateToDetention($s_id,$t_date);
				                                
				                            }	                                                                                                 
                                         else{
                                                 if ($ldiscipline->calculateUpgradeLateToDemerit($s_id))
                                                 {
                                                     $warning_student_ids[] = $s_id;
                                                 }
                                                 $ldiscipline->calculateUpgradeLateToDetention($s_id);
                                         }
                                     }
                            }
                            else
                            {
                                # Update Reason in profile record By AttendanceID
                                $sql = "UPDATE PROFILE_STUDENT_ATTENDANCE SET Reason = '$txtReason'
                                       WHERE StudentAttendanceID = '$attendance_id'";
                                $lcardattend->db_db_query($sql);
                            }
                    }
                    
                    # added on 2007-04-30
                    # remove previous absent record from Reason table ( if exists ) 
                    $sql="DELETE FROM CARD_STUDENT_PROFILE_RECORD_REASON WHERE RecordDate='$TargetDate' AND StudentID='$my_user_id' AND DayType='".PROFILE_DAY_TYPE_PM."' AND RecordType='".PROFILE_TYPE_ABSENT."'";
                    $lcardattend->db_db_query($sql);
                    
                    # added on 2007-07-10
                    # remove previous absent record from PROFILE table ( if exists ) 
                    $sql="DELETE FROM PROFILE_STUDENT_ATTENDANCE WHERE AttendanceDate='$TargetDate' AND UserID='$my_user_id' AND DayType='".PROFILE_DAY_TYPE_PM."' AND RecordType='".PROFILE_TYPE_ABSENT."'";
                    $lcardattend->db_db_query($sql);

                    
                    # Insert to Reason table
                    $fieldname = "RecordDate, StudentID, ProfileRecordID, Reason, RecordType, DayType, RecordStatus, DateInput, DateModified";
                    $fieldsvalues = "'$TargetDate', '$my_user_id', '$attendance_id','$txtReason', '".PROFILE_TYPE_LATE."', '".PROFILE_DAY_TYPE_PM."', '$attendance_id', now(), now() ";
                    $sql = "INSERT INTO CARD_STUDENT_PROFILE_RECORD_REASON ($fieldname)
                                   VALUES ($fieldsvalues)";
                    $lcardattend->db_db_query($sql);
                }
                else  # Reason record exists
                {
                    if ($reason_profile_id == "")    # Profile ID not exists
                    {
                        # Search whether attendance exists by date, student and type
                        $sql = "SELECT StudentAttendanceID FROM PROFILE_STUDENT_ATTENDANCE
                                       WHERE AttendanceDate = '$TargetDate'
                                             AND UserID = $my_user_id
                                             AND DayType = '".PROFILE_DAY_TYPE_PM."'
                                             AND RecordType = '".PROFILE_TYPE_LATE."'";
                        $temp = $lcardattend->returnVector($sql);
                        $attendance_id = $temp[0];
                        if ($attendance_id == "")          # Record not exists
                        {
                                if ($my_record_status != 1)	# not waive
                                {
                                    # Insert profile record
                                    $sql = "SELECT ClassName, ClassNumber FROM INTRANET_USER WHERE UserID = $my_user_id";
                                    $temp = $lcardattend->returnArray($sql,2);
                                    list ($user_classname, $user_classnum) = $temp[0];
                                    $fieldname = "UserID, AttendanceDate,Year, Semester, RecordType, DayType, DateInput,DateModified,ClassName,ClassNumber,Reason,AcademicYearID,YearTermID";
                                    $fieldvalue = "'$my_user_id','$TargetDate','$school_year','$semester','".PROFILE_TYPE_LATE."','".PROFILE_DAY_TYPE_PM."',now(),now(),'$user_classname','$user_classnum','$txtReason','$school_year_id','$semester_id'";
                                    $sql = "INSERT INTO PROFILE_STUDENT_ATTENDANCE ($fieldname) VALUES ($fieldvalue)";
                                    $lcardattend->db_db_query($sql);
                                    $attendance_id = $lcardattend->db_insert_id();
                            	}

                            # Calculate upgrade items
                            if ($plugin['Disciplinev12'] && $LateCtgInUsed)
	                        {
		                        if($attendance_id)
		                        {
									$semInfo = getAcademicYearAndYearTermByDate($TargetDate);
			                        $acy = new academic_year_term($semInfo[0]);
			                        
			                        $dataAry = array();
									$dataAry['StudentID'] = $my_user_id;
									$dataAry['RecordDate'] = $TargetDate;
									$school_year = GET_ACADEMIC_YEAR_WITH_FORMAT($TargetDate);
									$dataAry['Year'] = $acy->YearNameEN;
									$dataAry['Semester'] = $acy->YearTermNameEN;
									$dataAry['StudentAttendanceID'] = $attendance_id;
									$dataAry['AcademicYearID'] = $acy->AcademicYearID;
									$dataAry['YearTermID'] = $acy->YearTermID;
									$dataAry['Remark'] = $InSchoolTime;
									
									if($SeriousLateUsed == 1) {
										if($InSchoolTime != "")
										{
											$arr_time_table = $lcardattend->Get_Student_Attend_Time_Setting($TargetDate,$my_user_id);
											
											if($lcardattend->attendance_mode==1)	# retrieve InSchoolTime
											{
												##$field = "InSchoolTime";
												$school_start_time = $arr_time_table['MorningTime'];
											}
											else # retrieve LunchBackTime
											{
												##$field = "LunchBackTime";
												$school_start_time = $arr_time_table['LunchEnd'];
											}
											
											$str_late_minutes = strtotime($InSchoolTime) - strtotime($school_start_time);
											$late_minutes = ceil($str_late_minutes/60);
												
											$sql = "SELECT LateMinutes FROM DISCIPLINE_ACCU_CATEGORY WHERE LateMinutes <= '$late_minutes' ORDER BY LateMinutes DESC LIMIT 0,1";
											$min_late_minutes = $lcardattend->returnVector($sql);
											
											if(sizeof($min_late_minutes)>0)
											{
												$dataAry['SeriousLate'] = 1;
												$dataAry['LateMinutes'] = $late_minutes;
											}
											else
											{
												$dataAry['SeriousLate'] = 0;
											}
										}
										else
										{
											$dataAry['SeriousLate'] = 0;
										}
									}
									//debug("E : ");
									//debug_r($dataAry);
									$ldisciplinev12->INSERT_LATE_MISCONDUCT_RECORD($dataAry);
								}
	                        } 
	                        else if ($plugin['Discipline'] && $LateCtgInUsed)
                            {
                                    $t_date = $TargetDate;
                                     $s_id = $my_user_id;
                                    if($ldiscipline->isUseAccumulativeLateSetting($t_date)){
		                                     $ldiscipline->resetAccumulativeUpgradeLateToDemerit($s_id,$t_date);
		                                     $ldiscipline->resetAccumulativeUpgradeLateToDetention($s_id,$t_date);
				                            if ($ldiscipline->calculateAccumulativeUpgradeLateToDemerit($s_id,$t_date))
				                            {
				                                $warning_student_ids[] = $s_id;
				                            }
				                            $ldiscipline->calculateAccumulativeUpgradeLateToDetention($s_id,$t_date);
	                                
	                            	}
                                     else{
	                                     $ldiscipline->resetUpgradeLateToDemerit($s_id,$t_date);
	                                     $ldiscipline->resetUpgradeLateToDetention($s_id,$t_date);
		                                if ($ldiscipline->calculateUpgradeLateToDemerit($student_id))
		                                {
		                                    $warning_student_ids[] = $student_id;
		                                }
		                                $ldiscipline->calculateUpgradeLateToDetention($student_id);
                                	}
                            }
                        }
                        else
                        {
                                if ($my_record_status == 1)	# waive
                                {
                                        # Added by peter 2006/10/11
                                         # Reset Upgrade items
                                         if ($plugin['Disciplinev12'] && $LateCtgInUsed)
	                                     {
												$ldisciplinev12->DELETE_LATE_MISCONDUCT_RECORD($attendance_id);
	                                     }
	                                     else if ($plugin['Discipline'] && $LateCtgInUsed)
                                         {
                                             $t_date = $TargetDate;
                                             $s_id = $my_user_id;
                                             if($ldiscipline->isUseAccumulativeLateSetting($t_date)){
			                                     $ldiscipline->resetAccumulativeUpgradeLateToDemerit($s_id,$t_date);
			                                     $ldiscipline->resetAccumulativeUpgradeLateToDetention($s_id,$t_date);
											}
                                             else{
                                                 $ldiscipline->resetUpgradeLateToDemerit($s_id,$t_date);
                                                 $ldiscipline->resetUpgradeLateToDetention($s_id,$t_date);
                                             }
                                         }
                                        // Delete Record if waived
                                        $sql = "DELETE FROM PROFILE_STUDENT_ATTENDANCE WHERE StudentAttendanceID = '$attendance_id'";
                                        $lcardattend->db_db_query($sql);
                                        $attendance_id = "";
                                         # added by peter 2006/10/11
                                         # Calculate upgrade items
                                         if ($plugin['Disciplinev12'] && $LateCtgInUsed)
										{
											# calculation flow done in function DELETE_LATE_MISCONDUCT_RECORD
				                    	}
				                    	else if ($plugin['Discipline'] && $LateCtgInUsed)
                                         {
                                             $t_date = $TargetDate;
                                             if($ldiscipline->isUseAccumulativeLateSetting($t_date)){
						                            if ($ldiscipline->calculateAccumulativeUpgradeLateToDemerit($s_id,$t_date))
						                            {
						                                $warning_student_ids[] = $s_id;
						                            }
						                            $ldiscipline->calculateAccumulativeUpgradeLateToDetention($s_id,$t_date);

											 }

                                             else{
                                                 if ($ldiscipline->calculateUpgradeLateToDemerit($s_id))
                                                 {
                                                     $warning_student_ids[] = $s_id;
                                                 }
                                                 $ldiscipline->calculateUpgradeLateToDetention($s_id);
                                             }
                                         }
								}
                                else
                                {
                                    # Update Reason in profile record By AttendanceID
                                    $sql = "UPDATE PROFILE_STUDENT_ATTENDANCE SET Reason = '$txtReason'
                                           WHERE StudentAttendanceID = '$attendance_id'";
                                    $lcardattend->db_db_query($sql);
                                }
                        }
                    }
                    else  # Has Profile ID
                    {
                        # Search Attendance By ID
                        $sql = "SELECT StudentAttendanceID FROM PROFILE_STUDENT_ATTENDANCE
                                       WHERE StudentAttendanceID = $reason_profile_id";
                        $temp = $lcardattend->returnVector($sql);
                        //debug_r($sql);
                        $attendance_id = $temp[0];
                        if ($attendance_id == "")          # Record not exists
                        {
                            # Search attendance by date, student and type
                            $sql = "SELECT StudentAttendanceID FROM PROFILE_STUDENT_ATTENDANCE
                                           WHERE AttendanceDate = '$TargetDate'
                                                 AND UserID = $my_user_id
                                                 AND DayType = '".PROFILE_DAY_TYPE_PM."'
                                                 AND RecordType = '".PROFILE_TYPE_LATE."'";
                            $temp = $lcardattend->returnVector($sql);
                            
                            $attendance_id = $temp[0];
                            if ($attendance_id == "")          # Record not exists
                            {
                                    if ($my_record_status != 1)	# not waive
                                    {
                                        # insert reason in profile record
                                        $sql = "SELECT ClassName, ClassNumber FROM INTRANET_USER WHERE UserID = $my_user_id";
                                        $temp = $lcardattend->returnArray($sql,2);
                                        list ($user_classname, $user_classnum) = $temp[0];
                                        $fieldname = "UserID, AttendanceDate,Year, Semester, RecordType, DayType, DateInput,DateModified,ClassName,ClassNumber,Reason,AcademicYearID,YearTermID";
                                        $fieldvalue = "'$my_user_id','$TargetDate','$school_year','$semester','".PROFILE_TYPE_LATE."','".PROFILE_DAY_TYPE_PM."',now(),now(),'$user_classname','$user_classnum','$txtReason','$school_year_id','$semester_id'";
                                        $sql = "INSERT INTO PROFILE_STUDENT_ATTENDANCE ($fieldname) VALUES ($fieldvalue)";
                                        $lcardattend->db_db_query($sql);
                                        $attendance_id = $lcardattend->db_insert_id();
                                	}

                                # Calculate upgrade items
                                if ($plugin['Disciplinev12'] && $LateCtgInUsed)
		                        {
			                        if($attendance_id)
			                        {
										$semInfo = getAcademicYearAndYearTermByDate($TargetDate);
				                        $acy = new academic_year_term($semInfo[0]);
				                        
				                        $dataAry = array();
										$dataAry['StudentID'] = $my_user_id;
										$dataAry['RecordDate'] = $TargetDate;
										$school_year = GET_ACADEMIC_YEAR_WITH_FORMAT($TargetDate);
										$dataAry['Year'] = $acy->YearNameEN;
										$dataAry['Semester'] = $acy->YearTermNameEN;
										$dataAry['StudentAttendanceID'] = $attendance_id;
										$dataAry['AcademicYearID'] = $acy->AcademicYearID;
										$dataAry['YearTermID'] = $acy->YearTermID;
										$dataAry['Remark'] = $InSchoolTime;
										
										if($SeriousLateUsed == 1) {
											if($InSchoolTime != "")
											{
												$arr_time_table = $lcardattend->Get_Student_Attend_Time_Setting($TargetDate,$my_user_id);
												if($lcardattend->attendance_mode==1)	# retrieve InSchoolTime
												{
													##$field = "InSchoolTime";
													$school_start_time = $arr_time_table['MorningTime'];
												}
												else # retrieve LunchBackTime
												{
													##$field = "LunchBackTime";
													$school_start_time = $arr_time_table['LunchEnd'];
												}
												
												$str_late_minutes = strtotime($InSchoolTime) - strtotime($school_start_time);
												$late_minutes = ceil($str_late_minutes/60);
												
												$sql = "SELECT LateMinutes FROM DISCIPLINE_ACCU_CATEGORY WHERE LateMinutes <= '$late_minutes' ORDER BY LateMinutes DESC LIMIT 0,1";
												$min_late_minutes = $lcardattend->returnVector($sql);
												
												if(sizeof($min_late_minutes)>0)
												{
													$dataAry['SeriousLate'] = 1;
													$dataAry['LateMinutes'] = $late_minutes;
												}
												else
												{
													$dataAry['SeriousLate'] = 0;
												}
											}
											else
											{
												$dataAry['SeriousLate'] = 0;
											}
										}
										//debug("F : ");
										//debug_r($dataAry);
										$ldisciplinev12->INSERT_LATE_MISCONDUCT_RECORD($dataAry);
									}
		                        } 
		                        else if ($plugin['Discipline'] && $LateCtgInUsed)
                                {
                                     $t_date = $TargetDate;
                                     $s_id = $my_user_id;
	                                if($ldiscipline->isUseAccumulativeLateSetting($t_date)){
	                                     $ldiscipline->resetAccumulativeUpgradeLateToDemerit($s_id,$t_date);
	                                     $ldiscipline->resetAccumulativeUpgradeLateToDetention($s_id,$t_date);
			                            if ($ldiscipline->calculateAccumulativeUpgradeLateToDemerit($s_id,$t_date))
			                            {
			                                $warning_student_ids[] = $s_id;
			                            }
			                            $ldiscipline->calculateAccumulativeUpgradeLateToDetention($s_id,$t_date);
		                                
		                            }
                                     
                                     else{
		                                     $ldiscipline->resetUpgradeLateToDemerit($s_id,$t_date);
		                                     $ldiscipline->resetUpgradeLateToDetention($s_id,$t_date);
		                                    if ($ldiscipline->calculateUpgradeLateToDemerit($student_id))
		                                    {
		                                        $warning_student_ids[] = $student_id;
		                                    }
		                                    $ldiscipline->calculateUpgradeLateToDetention($student_id);
		                             }
                                }
                            }
                            else
                            {
                                    if ($my_record_status == 1)	# waive
                                    {
                                         # Added by peter 2006/10/11
                                             # Reset Upgrade items
                                             if ($plugin['Disciplinev12'] && $LateCtgInUsed)
		                                     {
													$ldisciplinev12->DELETE_LATE_MISCONDUCT_RECORD($attendance_id);
		                                     }
		                                     else if ($plugin['Discipline'] && $LateCtgInUsed)
                                             {
                                                 $t_date = $TargetDate;
                                                 $s_id = $my_user_id;
				                                if($ldiscipline->isUseAccumulativeLateSetting($t_date)){
				                                     $ldiscipline->resetAccumulativeUpgradeLateToDemerit($s_id,$t_date);
				                                     $ldiscipline->resetAccumulativeUpgradeLateToDetention($s_id,$t_date);
					                                
					                            }                                                                                     
                                                 else{
                                                     $ldiscipline->resetUpgradeLateToDemerit($s_id,$t_date);
                                                     $ldiscipline->resetUpgradeLateToDetention($s_id,$t_date);
                                                 }
                                             }
                                            // Delete Record if waived
                                                                        $sql = "DELETE FROM PROFILE_STUDENT_ATTENDANCE WHERE StudentAttendanceID = '$attendance_id'";
                                                                        $lcardattend->db_db_query($sql);
                                                                        $attendance_id = "";
                                                                  # added by peter 2006/10/11
                                                                 # Calculate upgrade items
                                                                 if ($plugin['Disciplinev12'] && $LateCtgInUsed)
							                                     {
																		$ldisciplinev12->DELETE_LATE_MISCONDUCT_RECORD($attendance_id);
							                                     }
							                                     else if ($plugin['Discipline'] && $LateCtgInUsed)
                                                                 {
                                                                     $t_date = $TargetDate;
                                                                     if($ldiscipline->isUseAccumulativeLateSetting($t_date)){
											                            if ($ldiscipline->calculateAccumulativeUpgradeLateToDemerit($s_id,$t_date))
											                            {
											                                $warning_student_ids[] = $s_id;
											                            }
											                            $ldiscipline->calculateAccumulativeUpgradeLateToDetention($s_id,$t_date);
										                                
										                            }

                                                                     else{
                                                                         if ($ldiscipline->calculateUpgradeLateToDemerit($s_id))
                                                                         {
                                                                             $warning_student_ids[] = $s_id;
                                                                         }
                                                                         $ldiscipline->calculateUpgradeLateToDetention($s_id);
                                                                     }
                                                                 }
                                    }
                                    else
                                    {
                                        # Update Reason in profile record By AttendanceID
                                        $sql = "UPDATE PROFILE_STUDENT_ATTENDANCE SET Reason = '$txtReason'
                                               WHERE StudentAttendanceID = '$attendance_id'";
                                        $lcardattend->db_db_query($sql);
                                    }
                            }
                        }
                        else # profile record exists and valid
                        {
                                if ($my_record_status == 1)	# waive
                            {
                                 	# Added by peter 2006/10/11
                                     # Reset Upgrade items
                                     if ($plugin['Disciplinev12'] && $LateCtgInUsed)
                                     {
											$ldisciplinev12->DELETE_LATE_MISCONDUCT_RECORD($attendance_id);
                                     }
                                     else if ($plugin['Discipline'] && $LateCtgInUsed)
                                     {
                                         $t_date = $TargetDate;
                                         $s_id = $my_user_id;
		                                if($ldiscipline->isUseAccumulativeLateSetting($t_date)){
		                                     $ldiscipline->resetAccumulativeUpgradeLateToDemerit($s_id,$t_date);
		                                     $ldiscipline->resetAccumulativeUpgradeLateToDetention($s_id,$t_date);
			                            }
                                         
                                         else{
                                             $ldiscipline->resetUpgradeLateToDemerit($s_id,$t_date);
                                             $ldiscipline->resetUpgradeLateToDetention($s_id,$t_date);
                                         }
                                     }
        // Delete Record if waived
                                    $sql = "DELETE FROM PROFILE_STUDENT_ATTENDANCE WHERE StudentAttendanceID = '$attendance_id'";
                                    $lcardattend->db_db_query($sql);
                                    $attendance_id = "";
                             # added by peter 2006/10/11
                     # Calculate upgrade items
                     				if ($plugin['Disciplinev12'] && $LateCtgInUsed)
									{
										# calculation flow done in function DELETE_LATE_MISCONDUCT_RECORD
			                    	}
			                    	else if ($plugin['Discipline'] && $LateCtgInUsed)
                                     {
                                         $t_date = $TargetDate;
                                       if($ldiscipline->isUseAccumulativeLateSetting($t_date)){
					                            if ($ldiscipline->calculateAccumulativeUpgradeLateToDemerit($s_id,$t_date))
					                            {
					                                $warning_student_ids[] = $s_id;
					                            }
					                            $ldiscipline->calculateAccumulativeUpgradeLateToDetention($s_id,$t_date);
			                                
			                            }

                                         else{
                                                 if ($ldiscipline->calculateUpgradeLateToDemerit($s_id))
                                                 {
                                                     $warning_student_ids[] = $s_id;
                                                 }
                                                 $ldiscipline->calculateUpgradeLateToDetention($s_id);
                                         }
                                     }
                            }
                            else
                            {
                                    # Update reason in profile record
                                    $sql = "UPDATE PROFILE_STUDENT_ATTENDANCE SET Reason = '$txtReason'
                                           WHERE StudentAttendanceID = '$attendance_id'";
                                    $lcardattend->db_db_query($sql);
                                }

                        }
                    }
                    
                    # added on 2007-04-30
                    # remove previous absent record from Reason table ( if exists ) 
                    $sql="DELETE FROM CARD_STUDENT_PROFILE_RECORD_REASON WHERE RecordDate='$TargetDate' AND StudentID='$my_user_id' AND DayType='".PROFILE_DAY_TYPE_PM."' AND RecordType='".PROFILE_TYPE_ABSENT."'";
                    $lcardattend->db_db_query($sql);
                    
                    # added on 2007-07-10
                    # remove previous absent record from PROFILE table ( if exists ) 
                    $sql="DELETE FROM PROFILE_STUDENT_ATTENDANCE WHERE AttendanceDate='$TargetDate' AND UserID='$my_user_id' AND DayType='".PROFILE_DAY_TYPE_PM."' AND RecordType='".PROFILE_TYPE_ABSENT."'";
                    $lcardattend->db_db_query($sql);

                    
                    
                    # Update reason table record
                    $sql = "UPDATE CARD_STUDENT_PROFILE_RECORD_REASON SET Reason = '$txtReason',
                                   ProfileRecordID = '$attendance_id',
                                   RecordStatus = '$my_record_status'
                                   WHERE RecordID= '$reason_record_id'";
                    $lcardattend->db_db_query($sql);
                }

                                # Calculate upgrade items
				if ($plugin['Disciplinev12'] && $LateCtgInUsed)
	            {
	                # late > late, no need to re-group
	            }
	            else if ($plugin['Discipline'] && $LateCtgInUsed)
                 {
                      $t_date = $TargetDate;
                     $s_id = $my_user_id;
                    if($ldiscipline->isUseAccumulativeLateSetting($t_date)){
                         $ldiscipline->resetAccumulativeUpgradeLateToDemerit($s_id,$t_date);
                         $ldiscipline->resetAccumulativeUpgradeLateToDetention($s_id,$t_date);
                        if ($ldiscipline->calculateAccumulativeUpgradeLateToDemerit($s_id,$t_date))
                        {
                            $warning_student_ids[] = $s_id;
                        }
                        $ldiscipline->calculateAccumulativeUpgradeLateToDetention($s_id,$t_date);
                        
                    }
                     
                     else{
                                 $ldiscipline->resetUpgradeLateToDemerit($s_id,$t_date);
                                 $ldiscipline->resetUpgradeLateToDetention($s_id,$t_date);
			                     if($ldiscipline->calculateUpgradeLateToDemerit($student_id))
			                     {
			                        $warning_student_ids[] = $student_id;
			                     }
			                     $ldiscipline->calculateUpgradeLateToDetention($student_id);
                     }
                 }
            }
            else if ($my_drop_down_status == 0)    # On Time
            {
	            # Search whether attendance exists by date, student and type
                $sql = "SELECT StudentAttendanceID FROM PROFILE_STUDENT_ATTENDANCE
                               WHERE AttendanceDate = '$TargetDate'
                                     AND UserID = $my_user_id
                                     AND DayType = '".PROFILE_DAY_TYPE_PM."'
                                     AND RecordType = '".PROFILE_TYPE_LATE."'";
                $temp = $lcardattend->returnVector($sql);
                $attendance_id = $temp[0];
                
                 # Remove Reason record
                 $sql = "DELETE FROM CARD_STUDENT_PROFILE_RECORD_REASON
                               WHERE RecordDate = '$TargetDate'
                                     AND StudentID = $my_user_id
                                     AND DayType = '".PROFILE_DAY_TYPE_PM."'
                                     AND RecordType = '".PROFILE_TYPE_LATE."'";
                 $lcardattend->db_db_query($sql);

                 # Reset upgrade items
				if ($plugin['Disciplinev12'] && $LateCtgInUsed)
                 {
						$ldisciplinev12->DELETE_LATE_MISCONDUCT_RECORD($attendance_id);
                 }
                 else if ($plugin['Discipline'] && $LateCtgInUsed)
                 {
                     $target_date = $TargetDate;
                     $student_id = $my_user_id;
                    if($ldiscipline->isUseAccumulativeLateSetting($target_date)){
                         $ldiscipline->resetAccumulativeUpgradeLateToDemerit($student_id,$target_date);
                         $ldiscipline->resetAccumulativeUpgradeLateToDetention($student_id,$target_date);
                        
                    }
                     
                     else{
	                     $ldiscipline->resetUpgradeLateToDemerit($student_id,$target_date);
	                     $ldiscipline->resetUpgradeLateToDetention($student_id,$target_date);
                     }
                 }

                 # Remove Profile record
                 $sql = "DELETE FROM PROFILE_STUDENT_ATTENDANCE
                               WHERE AttendanceDate = '$TargetDate'
                                     AND UserID = $my_user_id
                                     AND DayType = '".PROFILE_DAY_TYPE_PM."'
                                     AND RecordType = '".PROFILE_TYPE_LATE."'";
                 $lcardattend->db_db_query($sql);

                 # Calculate upgrade items
                 if ($plugin['Disciplinev12'] && $LateCtgInUsed)
				{
					# calculation flow done in function DELETE_LATE_MISCONDUCT_RECORD
            	}
            	else if ($plugin['Discipline'] && $LateCtgInUsed)
                 {
	                 $t_date = $TargetDate;
                    if($ldiscipline->isUseAccumulativeLateSetting($t_date)){
                        if ($ldiscipline->calculateAccumulativeUpgradeLateToDemerit($student_id,$t_date))
                        {
                            $warning_student_ids[] = $student_id;
                        }
                        $ldiscipline->calculateAccumulativeUpgradeLateToDetention($student_id,$t_date);
                    }
	                 else{
		                     if ($ldiscipline->calculateUpgradeLateToDemerit($student_id))
		                     {
		                         $warning_student_ids[] = $student_id;
		                     }
		                     $ldiscipline->calculateUpgradeLateToDetention($student_id);
                     }
                 }

                 # Set to Daily Record Table
                 # Set AMStatus and InSchoolTime
                 /*
                 $sql = "UPDATE $card_log_table_name
                                SET PMStatus = '".CARD_STATUS_PRESENT."',
                                LunchBackTime = NULL, DateModified = NOW()
                                WHERE DayNumber = '$txt_day'
                                      AND UserID = $my_user_id";
                  */
                  $sql = "UPDATE $card_log_table_name
                                SET PMStatus = '".CARD_STATUS_PRESENT."',
                                 DateModified = NOW()
                                WHERE DayNumber = '$txt_day'
                                      AND UserID = $my_user_id";

                 $lcardattend->db_db_query($sql);
            }
            else # Unknown action
            {
                 # Do nthg
            }

        }
        else # Unknown action
        {
               # Do nthg
        }
} # End of For-Loop

if ($period==1)
{
    $period_type = PROFILE_DAY_TYPE_AM;
}
else if ($period==2)
{
    $period_type = PROFILE_DAY_TYPE_PM;
}
else
{
    $period_type = "";
}
if ($period_type != "")
{
    # Update Confirm Record
    $sql = "UPDATE CARD_STUDENT_DAILY_DATA_CONFIRM
                   SET LateConfirmed = 1, LateConfirmTime = now(), DateModified = now()
                   WHERE RecordDate = '$TargetDate' AND RecordType = $period_type";
    $lcardattend->db_db_query($sql);
    if ($lcardattend->db_affected_rows()!=1)         # Not Exists
    {
        # Not exists
        $sql = "INSERT INTO CARD_STUDENT_DAILY_DATA_CONFIRM (RecordDate, LateConfirmed,RecordType, LateConfirmTime, DateInput, DateModified)
                       VALUES ('$TargetDate',1,$period_type,now(),now(),now())";
        $lcardattend->db_db_query($sql);
    }
}

if (sizeof($warning_student_ids)!=0)
{
        $body_tags = "onLoad=document.form1.submit()";
        include_once($PATH_WRT_ROOT."templates/fileheader.php");
        ?>
        <form name=form1 action=showlate_msg_prompt.php method=POST>
        <?
        for ($i=0; $i<sizeof($warning_student_ids); $i++)
        {
             ?>
             <input type=hidden name=WarningStudentID[] value="<?=$warning_student_ids[$i]?>">
             <?
        }
        ?>
        <input type=hidden name=period value="<?=$period?>">
        <input type=hidden name=TargetDate value="<?=$TargetDate?>">
        </form>
        <?
        include_once($PATH_WRT_ROOT."templates/filefooter.php");

}
else
{
	$Msg = $Lang['StudentAttendance']['LateListConfirmSuccess'];
  	header("Location: showlate.php?period=$period&TargetDate=$TargetDate&order_by_time=$order_by_time&Msg=".urlencode($Msg));
}

intranet_closedb();
?>