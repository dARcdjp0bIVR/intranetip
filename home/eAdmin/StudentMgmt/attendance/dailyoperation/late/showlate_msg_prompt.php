<?php
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."includes/libdiscipline.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$lc = new libcardstudentattend2();
$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageDailyOperation_ViewLateStatus";

if (!$lc->IS_ADMIN_USER($_SESSION['UserID'])) {
	header ("Location: /");
	intranet_closedb();
	exit();
}


if ($msg == 1)
{
    $response_msg = $i_Discipline_System_alert_RecordAddSuccessful;
}
else if ($msg == 2)
{
     $response_msg = $i_Discipline_System_alert_RecordWaitForApproval;
}
$warning_table = "";
if ( (isset($WarningStudentID) && is_array($WarningStudentID) && sizeof($WarningStudentID)!=0)
     || (isset($WarningStudentID_Subscore_1) && is_array($WarningStudentID_Subscore_1) && sizeof($WarningStudentID_Subscore_1)!=0)
     )
{
    intranet_opendb();
    $ldiscipline = new libdiscipline();

		if($ldiscipline->isUseAccumulativeLateSetting($TargetDate)){
			$temp = $ldiscipline->returnAccumulativePeriodInfoByDate($TargetDate);
			list($period_id,$start_date,$end_date,$target_year,$target_semester) = $temp;
			$year = $target_year;
			$semester = $target_semester;
		}else{
		        if ($year == "")
		        {
		            $year = getCurrentAcademicYear();
		        }
		        if ($semester == "")
		        {
		            $semester = getCurrentSemester();
		        }
		}

        # Conduct Score checking
        $warning_table = "";
        if (sizeof($WarningStudentID))
        {
            # Get Student info
            $list = implode(",", $WarningStudentID);
            $namefield = getNameFieldWithClassNumberByLang("a.");
            $sql = "SELECT a.UserID, $namefield, b.ConductScore FROM INTRANET_USER as a
                           LEFT OUTER JOIN DISCIPLINE_STUDENT_CONDUCT_BALANCE as b
                                ON a.UserID = b.StudentID AND b.Year = '$year' AND b.Semester = '$semester'
                           WHERE a.UserID IN ($list)";
            $student_info = $ldiscipline->returnArray($sql,3);
            
            $warning_table .= "<table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">";
            $warning_table .= "<tr class=\"tablebluetop\">";
            $warning_table .= "<td class=\"tabletoplink\">$i_UserStudentName</td>";
            $warning_table .= "<td class=\"tabletoplink\">$i_Discipline_System_Conduct_DroppedPassWarningPoint</td>";
            $warning_table .= "<td class=\"tabletoplink\">$i_Discipline_System_Conduct_UpdatedScore</td>";
            $warning_table .= "</tr>";

            for ($i=0; $i<sizeof($student_info); $i++)
            {
                 list($t_id, $t_name, $t_conduct_score) = $student_info[$i];
                 $css = ($i%2?"tablebluerow1":"tablebluerow2");
                 $t_warning_level = $ldiscipline->getDroppedConductWarningLevel($t_conduct_score);
                 $warning_table .= "<tr class=\"$css\">";
                 $warning_table .= "<td class=\"tabletext\">$t_name</td>";
                 $warning_table .= "<td class=\"tabletext\">$t_warning_level</td>";
                 $warning_table .= "<td class=\"tabletext\">$t_conduct_score</td>";
                 $warning_table .= "</tr>\n";
                 $warning_table .= "<input type=\"hidden\" name=\"WarningStudentID[]\" value=\"$t_id\">\n";
            }

            $warning_table .= "</table>";
        }


        # Sub Score Type 1 checking
        $warning_table_subscore_1 = "";
        if (sizeof($WarningStudentID_Subscore_1))
        {
            # Get Student info
            $list = implode(",", $WarningStudentID_Subscore_1);
            $namefield = getNameFieldWithClassNumberByLang("a.");
            $sql = "SELECT a.UserID, $namefield, b.SubScore FROM INTRANET_USER as a
                           LEFT OUTER JOIN DISCIPLINE_STUDENT_SUBSCORE_BALANCE as b
                           ON a.UserID = b.StudentID AND b.Year = '$year' AND b.Semester = '$semester'
                           AND b.RecordType = 1
                    WHERE a.UserID IN ($list)";
            $student_info = $ldiscipline->returnArray($sql,3);
            
            $warning_table_subscore_1 .= "<table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">";
            $warning_table_subscore_1 .= "<tr class=\"tablebluetop\">";
            $warning_table_subscore_1 .= "<td class=\"tabletoplink\">$i_UserStudentName</td>";
            $warning_table_subscore_1 .= "<td class=\"tabletoplink\">$i_Discipline_System_Conduct_DroppedPassWarningPoint</td>";
            $warning_table_subscore_1 .= "<td class=\"tabletoplink\">$i_Discipline_System_Subscore1_UpdatedScore</td>";
            $warning_table_subscore_1 .= "</tr>";

            for ($i=0; $i<sizeof($student_info); $i++)
            {
                 list($t_id, $t_name, $t_sub_score) = $student_info[$i];
                 $css = ($i%2?"tablebluerow1":"tablebluerow2");
                 $t_warning_level = $ldiscipline->getDroppedSubscoreWarningLevel(1,$t_sub_score);
                 $warning_table_subscore_1 .= "<tr class=\"$css\">";
                 $warning_table_subscore_1 .= "<td class=\"tabletext\">$t_name</td>";
                 $warning_table_subscore_1 .= "<td class=\"tabletext\">$t_warning_level</td>";
                 $warning_table_subscore_1 .= "<td class=\"tabletext\">$t_sub_score</td>";
                 $warning_table_subscore_1 .= "</tr>";
                 $warning_table_subscore_1 .= "<input type=\"hidden\" name=\"WarningStudentID_Subscore_1[]\" value=\"$t_id\">\n";
            }
        }
     intranet_closedb();
}

$linterface = new interface_html();

$TAGS_OBJ[] = array($i_SmartCard_DailyOperation_ViewLateStatus, "", 0);
$MODULE_OBJ = $lc->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

$PAGE_NAVIGATION1 = $i_Discipline_System_Conduct_ShowNotice;
$PAGE_NAVIGATION2 = $i_Discipline_System_Conduct_List_ConductScore_Dropped;

if ($msg == 1) $SysMsg = $linterface->GET_SYS_MSG("add");
if ($msg == 2) $SysMsg = $linterface->GET_SYS_MSG("update");
?>
<script language="Javascript">
function showNotice()
{
     checkPost(document.form1, '<?=$intranet_httppath?>/home/admin/discipline/notice/shownotice_conduct.php');
}
function showNotice_subscore_1()
{
     checkPost(document.form_subscore_1, '<?=$intranet_httppath?>/home/notice/shownotice_subscore_1.php');
}

</script>
<br />
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>
			<table width="95%" border="0" cellpadding="0" cellspacing="0" align="center">
				<tr>
					<td align="right"><?= $SysMsg ?></td>
				</tr>
			</table>
		</td>
	</tr>
<?php
	if (isset($WarningStudentID) && is_array($WarningStudentID) && sizeof($WarningStudentID)!=0)
	{
?>
    <form name="form1" action="" method="post" target="_blank">
    <tr>
		<td>
			<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr>
					<td><?= $linterface->GET_NAVIGATION2($PAGE_NAVIGATION2) ?></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td><?=$warning_table?></td>
	</tr>
	<tr>
		<td>
			<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr>
					<td><?= $linterface->GET_NAVIGATION2($PAGE_NAVIGATION1) ?></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_Discipline_System_Notice_AttachItem?></td>
					<td class="tabletext" width="70%"><input type="checkbox" name="AttachItem" value="1"></td>
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_general_Format?></td>
					<td class="tabletext" width="70%">
						<select name="format">
							<option value="0">Web</option>
							<option value="1">Word</option>
						</select>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
		    	<tr>
                	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
		    </table>	
		</td>
	</tr>
	<tr>
		<td>
			<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
		    	<tr>
				    <td align="center">
						<?= $linterface->GET_ACTION_BTN($button_submit, "button", "showNotice()") ?>
					</td>
				</tr>
			</table>
		</td>
	</tr>
    <input type="hidden" name="year" value="<?=$year?>">
    <input type="hidden" name="semester" value="<?=$semester?>">
    </form>
<?php
}
?>

	<tr>
		<td>
			<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
		    	<tr>
				    <td align="center">
						<?= $linterface->GET_ACTION_BTN($button_continue, "submit", "location.href='showlate.php?DayType=$DayType&TargetDate=$TargetDate'") ?>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<br />
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>