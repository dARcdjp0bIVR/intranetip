<?php
//using by :  

###################### Change Log Start ########################
# 2019-06-27 Ray: Add teacher remark preset select box
# 2018-11-15 Cameron: add getRouteSelection() for eSchoolBus
# 2017-04-11 Carlos: Added New late student function.
# 2015-06-18 Carlos: Do not display the Send SMS(no template) tool button if flag is off.
# 2015-06-05 Omas: Add Export Mobile Button
# 2015-04-23 Carlos: Fix office remark array index shift problem after disable the input. 
# 2015-02-11 Carlos: Confirm time added confirm user name. Can edit teachers' remark. Added office remark. Added column [Last Modify].
# 2014-10-09 Roy:	Added send push message option
# 2014-09-05 Bill:	 Add apply all to attendance status, fix short cut problem
# 2014-07-24 Carlos: Change Period selection to radio checkboxes
# 2014-01-28 Carlos: Added table column [Remark] 
# 2013-04-16: Carlos - Add CARD_STUDENT_DAILY_REMARK.DayType
# 2011-12-21: Carlos - added data column Gender
# 2011-11-04: Carlos - add togggle display of waive checkbox when status changes
# 2011-05-17: Henry Chow - add checking on status of eDis LATE category->display warning msg
# 2010-04-22: Carlos - Allow input late session if student is late
# 2010-03-29: kenneth chung - Add function to allow user to input in time for late of there isn't any
#	Date:	2010-01-27	YatWoon
#			add "In School Time" value to the send sms function 
#
###################### Change Log End ########################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libstudentattendance_ui.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libwordtemplates.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$TargetDate = isset($_POST['TargetDate'])? $_POST['TargetDate'] : $_GET['TargetDate'];
$DayType = isset($_POST['DayType'])? $_POST['DayType'] : $_GET['DayType'];
$routeID = isset($_POST['routeID'])? $_POST['routeID'] : $_GET['routeID'];

$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageDailyOperation_ViewLateStatus";

$linterface = new interface_html();
$StudentAttendUI = new libstudentattendance_ui();

### class used
$lc = new libcardstudentattend2();
$lc->retrieveSettings();
$luser = new libuser();

### Short-cut for Daily Late / Daily Absent/ Daily Early Leave ###
/*
$arr_page = array(
					array(1,$i_SmartCard_DailyOperation_ViewClassStatus),
					array(2,$i_SmartCard_DailyOperation_ViewAbsenceStatus),
					array(3,$i_SmartCard_DailyOperation_ViewEarlyLeaveStatus)
				);
$pageShortCut_Selection = getSelectByArray($arr_page," name=\"pageShortCut\" onChange=\"changePage(this.value)\" ");
*/

$lword = new libwordtemplates();

### Set Date from previous page
if ($TargetDate == "")
{
    $TargetDate = date('Y-m-d');
}
$ts_record = strtotime($TargetDate);
$txt_year = date('Y',$ts_record);
$txt_month = date('m',$ts_record);
$txt_day = date('d',$ts_record);

###period
switch ($DayType)
{
  case PROFILE_DAY_TYPE_AM: 
  	$display_period = $i_DayTypeAM;
    break;
  case PROFILE_DAY_TYPE_PM: 
  	$display_period = $i_DayTypePM;
    break;
  default: 
  	if ($lc->attendance_mode == 1) {
  		$display_period = $i_DayTypePM;      
  		$DayType = PROFILE_DAY_TYPE_PM;
  	}
  	else {
    	$display_period = $i_DayTypeAM;
	    $DayType = PROFILE_DAY_TYPE_AM;
	  }
    break;
}

# get student array with parent using parent app
$studentWithParentUsingParentAppAry = $luser->getStudentWithParentUsingParentApp();

# order information
$default_order_by = " a.ClassName, a.ClassNumber+0, a.EnglishName";

if($order_by_time!=1){
	$order_by = $default_order_by ;
}
else{
	#$order_str= $order== 1?" DESC ":" ASC ";
	$order_str= " ASC ";

	if($DayType==PROFILE_DAY_TYPE_AM || $lc->attendance_mode==1){
		$order_by = "b.InSchoolTime $order_str";
	}
	else {
		$order_by = "b.LunchBackTime $order_str";
	}
}

# order info
$table_tool_time .= "<td nowrap=\"nowrap\">
					<a href=\"javascript:orderByTime(1)\" class=\"tabletool\">
						<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_sort_d_off.gif\" alt=\"$i_SmartCard_DailyOperation_OrderBy_InSchoolTime\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">
						$i_SmartCard_DailyOperation_OrderBy_InSchoolTime
					</a>
				</td>
				";

$table_tool_class .= "<td nowrap=\"nowrap\">
					<a href=\"javascript:orderByTime(0)\" class=\"tabletool\">
						<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_sort_d_off.gif\" alt=\"$i_SmartCard_DailyOperation_OrderBy_ClassNameNumber\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">
						$i_SmartCard_DailyOperation_OrderBy_ClassNameNumber
					</a>
				</td>";


$table_tool = $order_by_time==1?$table_tool_class:$table_tool_time;

$confirmNameField = getNameFieldByLang2("u.");
# Get Confirmation status
$sql = "SELECT c.RecordID, c.LateConfirmed, c.LateConfirmTime, $confirmNameField as ConfirmUser FROM CARD_STUDENT_DAILY_DATA_CONFIRM as c 
		left join INTRANET_USER as u ON u.UserID=c.LateConfirmUserID 
               WHERE c.RecordDate = '".$lc->Get_Safe_Sql_Query($TargetDate)."' AND c.RecordType = '".$lc->Get_Safe_Sql_Query($DayType)."'";
$temp = $lc->returnArray($sql,4);
list ($recordID, $confirmed , $confirmTime, $confirmUser) = $temp[0];


$col_width = 150;
$table_confirm = "";
/*
$x  = "<form name=\"form2\" id=\"form2\" method=\"post\" action=\"\" style=\"margin: 0px; padding: 0px;\">";
$x .= "<table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">";
$x .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"tabletext\">$i_StudentAttendance_Field_Date:</td>";
$x .= "<td class=\"tabletext\" width=\"70%\">".$linterface->GET_DATE_PICKER("TargetDate", $TargetDate,"","yy-mm-dd","Mask_Confirmed_Date")."</td></tr>";
$x .= '<tr>
				<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">&nbsp;</td>
				<td class="tabletext" width="70%">
					<table border=0>
						<tr>
							<td class="dynCalendar_card_not_confirmed" width="15px">
								&nbsp;
							</td>
							<td class="tabletext">
								'.$Lang['LessonAttendance']['LessonStatusAllNotConfirmed'].'
							</td>
							<td class="dynCalendar_card_partial_confirmed" width="15px">
								&nbsp;
							</td>
							<td class="tabletext">
								'.$Lang['LessonAttendance']['LessonStatusPartialConfirmed'].'
							</td>
							<td class="dynCalendar_card_confirmed" width="15px">
								&nbsp;
							</td>
							<td class="tabletext">
								'.$Lang['LessonAttendance']['LessonStatusAllConfirmed'].'
							</td>
						</tr>
					</table>
				</td>
			</tr>';
$x .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_StudentAttendance_Slot:</td>";
$x .= "<td class=\"tabletext\" width=\"70%\">".$selection_period."</td></tr>\n";

if($confirmed==1)
{
        $x .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_StudentAttendance_Field_LastConfirmedTime:</td>";
        $x .= "<td class=\"tabletext\" width=\"70%\">$confirmTime</td></tr>";
}
else
{
        $x .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\"></td>";
        $x .= "<td class=\"tabletext\" width=\"70%\">$i_SmartCard_Frontend_Take_Attendance_No_Confirm_Record</td></tr>";
}
$x .= "</table>\n";
$x .= "<input type=\"hidden\" name=\"flag\" value=\"1\">";
$x .= "</form>";
*/

$table_confirm = $StudentAttendUI->Get_Confirm_Selection_Form("form2","",PROFILE_TYPE_LATE,$TargetDate,$DayType, 1);

### build card log table
$card_log_table_name = "CARD_STUDENT_DAILY_LOG_".$txt_year."_".$txt_month;
if ($DayType==PROFILE_DAY_TYPE_AM)               # Check AM Late
{
	$StatusField = 'b.AMStatus';
	$InSchoolTime = 'b.InSchoolTime';
	$InSchoolStation = 'b.InSchoolStation';
}
else if ($lc->attendance_mode==1)    # Special for PM only
{
	$StatusField = 'b.PMStatus';
	$InSchoolTime = 'b.InSchoolTime';
	$InSchoolStation = 'b.InSchoolStation';
}
else     # Check PM Late
{
	$StatusField = 'b.PMStatus';
	$InSchoolTime = 'b.LunchBackTime';
	$InSchoolStation = 'b.LunchBackStation';
}
$ExpectedReasonField = $lc->Get_Expected_Reason($DayType,PROFILE_TYPE_LATE,'c.');

$LastModifyUserNameField = getNameFieldByLang2("u.");

$busRouteJoin = '';     // default value
$busRouteFilter = '';

if ($plugin['eSchoolBus']) {
    $academicYearTermAry = getAcademicYearAndYearTermByDate($TargetDate);
    $academicYearID = $academicYearTermAry['AcademicYearID'];
    if ($DayType == PROFILE_DAY_TYPE_AM) {        
        $amPm = 'AM';
    }
    else if ($DayType == PROFILE_DAY_TYPE_PM) {   
        $amPm = 'PM';
    }
    if ($routeID) {
        $busRouteJoin = " LEFT JOIN INTRANET_SCH_BUS_USER_ROUTE ur ON ur.UserID=a.UserID AND ur.AcademicYearID='".$lc->Get_Safe_Sql_Query($academicYearID)."'
        LEFT JOIN INTRANET_SCH_BUS_ROUTE_COLLECTION rc ON rc.RouteCollectionID=ur.RouteCollectionID ";
        $busRouteFilter = " AND ur.DateStart<='".$lc->Get_Safe_Sql_Query($TargetDate)."' AND ur.DateEnd>='".$lc->Get_Safe_Sql_Query($TargetDate)."' AND rc.RouteID='".$lc->Get_Safe_Sql_Query($routeID)."' AND rc.AmPm='".$lc->Get_Safe_Sql_Query($amPm)."' AND rc.IsDeleted='0' ";
    }
}

$sql  = "SELECT 
					b.RecordID, 
					a.UserID,
          ".getNameFieldWithClassNumberByLang("a.")." as name,
		  a.Gender,
          ".$InSchoolTime.",
          IF(".$InSchoolStation." IS NULL, '-', ".$InSchoolStation."),
          c.Reason,
          c.RecordStatus,
          d.Remark,
		  c.OfficeRemark,
			".$ExpectedReasonField." as ExpectedReason,
			c.DateModified, 
			$LastModifyUserNameField as LastModifyUser
  			FROM
          $card_log_table_name as b
					LEFT OUTER JOIN INTRANET_USER as a 
					ON b.UserID = a.UserID
					LEFT OUTER JOIN CARD_STUDENT_PROFILE_RECORD_REASON as c
					ON c.StudentID = a.UserID 
						AND c.RecordDate = '".$lc->Get_Safe_Sql_Query($TargetDate)."' 
						AND c.DayType = '".$lc->Get_Safe_Sql_Query($DayType)."'
					  AND c.RecordType = '".PROFILE_TYPE_LATE."'
					LEFT OUTER JOIN CARD_STUDENT_DAILY_REMARK AS d 
					ON (a.UserID = d.StudentID AND d.RecordDate ='".$lc->Get_Safe_Sql_Query($TargetDate)."' AND d.DayType='".$lc->Get_Safe_Sql_Query($DayType)."') 
					LEFT JOIN INTRANET_USER as u ON u.UserID=c.ModifyBy
                {$busRouteJoin}
				WHERE 
					b.DayNumber = '".$lc->Get_Safe_Sql_Query($txt_day)."' 
					AND ".$StatusField." = '".CARD_STATUS_LATE."'
          AND a.RecordType = 2 
          AND a.RecordStatus IN (0,1,2)
          {$busRouteFilter}
        ORDER BY 
        	".$order_by;
//debug_r($sql);
$result = $lc->returnArray($sql,11);
$StudentIDArr = Get_Array_By_Key($result,'UserID');

$col_span = 12;
$table_attend = "";
$words_absence = $lword->getWordListAttendance(PROFILE_TYPE_ABSENT);
$AbsentHasWord = sizeof($words_absence)!=0;
$words_late = $lword->getWordListAttendance(PROFILE_TYPE_LATE);
$LateHasWord = sizeof($words_late)!=0;
foreach($words_absence as $key=>$word)
	$words_absence[$key]= htmlspecialchars($word);
foreach($words_late as $key=>$word)
	$words_late[$key]= htmlspecialchars($word);

$x = $linterface->CONVERT_TO_JS_ARRAY($words_absence, "AbsentArrayWords", 1, 1);
$x .= $linterface->CONVERT_TO_JS_ARRAY($words_late, "LateArrayWords", 1, 1);

# Teacher Remark Preset
$teacher_remark_js = "";
$teacher_remark_words = $lc->GetTeacherRemarkPresetRecords(array());
$hasTeacherRemarkWord = sizeof($teacher_remark_words)!=0;
if($hasTeacherRemarkWord)
{
    $teacher_remark_words = Get_Array_By_Key($teacher_remark_words, 'ReasonText');
    foreach($teacher_remark_words as $key=>$word)
        $teacher_remark_words_temp[$key]= htmlspecialchars($word);
    $x .= $linterface->CONVERT_TO_JS_ARRAY($teacher_remark_words_temp, "jArrayWordsTeacherRemark", 1);
}

$x .= "<table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">";
$x .= "<tr class=\"tabletop\">";
$x .= "<td width=\"1\" class=\"tabletoplink\">#</td>";
$x .= "<td width=\"10%\" class=\"tabletoplink\">$i_UserName</td>";
$x .= "<td width=\"5%\" class=\"tabletoplink\">".$Lang['StudentAttendance']['Gender']."</td>";
$x .= "<td width=\"10%\" class=\"tabletoplink\">$i_SmartCard_Frontend_Take_Attendance_In_School_Time</td>";
$x .= "<td width=\"10%\" class=\"tabletoplink\">$i_SmartCard_Frontend_Take_Attendance_In_School_Station</td>";
$x .= "<td width=\"5%\" class=\"tabletoplink\">$i_SmartCard_Frontend_Take_Attendance_Status
					<br>
					<select id=\"drop_down_status_all\" name=\"drop_down_status_all\">
		        		<option value=\"".CARD_STATUS_PRESENT."\">".$Lang['StudentAttendance']['Present']."</option>
		        		<option value=\"".CARD_STATUS_LATE."\" SELECTED>".$Lang['StudentAttendance']['Late']."</option>
		        		<option value=\"".CARD_STATUS_ABSENT."\">".$Lang['StudentAttendance']['Absent']."</option>
		        		<option value=\"".CARD_STATUS_OUTING."\">".$Lang['StudentAttendance']['Outing']."</option>
	        		</select>
					<br>
					".$linterface->Get_Apply_All_Icon("javascript:SetAllAttend();")."
				</td>";
$x .= "<td width=\"5%\" class=\"tabletoplink\" align=\"center\">$i_SmartCard_Frontend_Take_Attendance_Waived<br /><input type=\"checkbox\" name=\"all_wavied\" onClick=\"(this.checked)?setAllWaived(document.form1,1):setAllWaived(document.form1,0)\"></td>";
$x .= "<td width=\"10%\" class=\"tabletoplink\" nowrap>
				$i_Attendance_Reason
				<br>
				<input class=\"textboxnum\" type=\"text\" name=\"SetAllReason\" id=\"SetAllReason\" maxlength=\"255\" value=\"\">
				".$linterface->GET_PRESET_LIST("LateArrayWords", "SetAllReasonIcon", "SetAllReason")."
				<br>
				".$linterface->Get_Apply_All_Icon("javascript:SetAllReason();")."
			</td>";
$x .= "<td width=\"10%\" class=\"tabletoplink\">".$Lang['StudentAttendance']['iSmartCardRemark']."</td>";
$x .= "<td width=\"10%\" class=\"tabletoplink\">".$Lang['StudentAttendance']['OfficeRemark']."</td>";
$x .= "<td width=\"5%\" class=\"tabletoplink\">$i_StudentGuardian[MenuInfo]</td>";

if($sys_custom['send_sms'] && $plugin['sms'] && $bl_sms_version >= 2)
{
	include_once($PATH_WRT_ROOT."includes/libsmsv2.php");
	$lsms        = new libsmsv2();
	
	//check sms system template status
	$template_status = $lsms->returnTemplateStatus("","STUDENT_ATTEND_LATE");
	$template_content = $lsms->returnSystemMsg("STUDENT_ATTEND_LATE");
	$template_status = $template_status and trim($template_content);
	if($template_status)
	{
		$x .= "<td width=\"5%\">$i_SMS_Send <input type='checkbox' name='all_sms' onClick=\"(this.checked)?setAllSend(document.form1,1):setAllSend(document.form1,0)\"></td>";
		$col_span+=1;
	}
}

if ($plugin['eClassApp']) {
	$x .= "<td width=\"5%\" class=\"tabletoplink\">".$Lang['AppNotifyMessage']['PushMessage']."<br /><input type=\"checkbox\" name=\"all_send_push_message\" onClick=\"$('.PushMessageCheckbox').attr('checked',this.checked);\"></td>";
	$col_span+=1;
}

$x .= "<td width=\"10%\" class=\"tabletoplink\">".$Lang['StudentAttendance']['LastModify']."</td>";
$x .= "</tr>\n";

$reason_js_array = "";

$disable_color="#DFDFDF";  # background color for disabled text field
$enable_color="#FFFFFF";  # background color for enabled text field

$select_word = "";

$gender_word['M'] = $Lang['General']['Male'];
$gender_word['F'] = $Lang['General']['Female'];

for($i=0; $i<sizeOf($result); $i++)
{
        list($record_id, $user_id, $name, $gender, $in_school_time, $in_school_station, $reason, $record_status, $remark, $office_remark, $ExpectedReason, $date_modified, $last_modify_user) = $result[$i];

        $css = ($i%2==0)?"tablerow1":"tablerow2";
        $css = (trim($record_status) == "")? $css:"attendance_late";
        $select_status = "<select id=\"drop_down_status[$i]\" name=\"drop_down_status[]\" onChange=\"displayCheckbox(this.value,'$user_id');setReasonComp(this.value, this.form.reason$i, this.form.editAllowed$i, $i)\">\n";
        $select_status .= "<option value=\"".CARD_STATUS_PRESENT."\">".$Lang['StudentAttendance']['Present']."</option>\n";
        $select_status .= "<option value=\"".CARD_STATUS_LATE."\" SELECTED>".$Lang['StudentAttendance']['Late']."</option>\n";
        $select_status .= "<option value=\"".CARD_STATUS_ABSENT."\">".$Lang['StudentAttendance']['Absent']."</option>\n";
        $select_status .= "<option value=\"".CARD_STATUS_OUTING."\">".$Lang['StudentAttendance']['Outing']."</option>\n";
        $select_status .= "</select>\n";
        $select_status .= "<input name=\"record_id[$i]\" type=\"hidden\" value=\"$record_id\">\n";
        $select_status .= "<input name=\"user_id[$i]\" type=\"hidden\" value=\"$user_id\">\n";

        $reason_comp = "<input type=\"text\" name=\"reason$i\" id=\"reason$i\" class=\"textboxnum\" maxlength=\"255\" value=\"".htmlspecialchars($ExpectedReason,ENT_QUOTES)."\" style=\"background:$enable_color\">";
        
        // Added on 2006-06-20
        $waived_option = "<input type=\"checkbox\" name=\"waived_{$user_id}\"".(($record_status == 1) ? " CHECKED" : " ") .">";
        $sms_option = "<input type=checkbox name=sms_{$user_id}>";
        $hidden_in_school_time = "<input type='hidden' name='in_school_time_{$user_id}' value='{$in_school_time}'>";
				
				// added on 2010-03-29, allow user input inschool time if Inschool time is null
				$in_school_time = ($in_school_time == "")? '<input maxlength="8" size="8" type="text" class="textbox" name="LateTime['.$user_id.']" id="LateTime['.$i.']" onkeyup="Check_Time_Format(this.value,\'LateTimeWarningLayer-'.$user_id.'\');"><br><div style="color:red" class="LateTimeWarningLayer" id="LateTimeWarningLayer-'.$user_id.'"></div>':$in_school_time;
				
        $x .= "<tr class=\"$css\"><td class=\"tabletext\">".($i+1)."</td>";
        $x .= "<td class=\"tabletext\">$name</td>";
        $x .= "<td class=\"tabletext\">".$gender_word[$gender]."</td>";
        $x .= "<td class=\"tabletext\">$in_school_time</td>";
        $x .= "<td class=\"tabletext\">$in_school_station</td>";
        $x .= "<td class=\"tabletext\">$select_status</td>";
        
        $x .= "<td class=\"tabletext\" align=\"center\"><span id=\"cb_{$user_id}\">$waived_option</span></td>";
        $x .= "<td class=\"tabletext\" nowrap>$reason_comp";
        $x .= $linterface->GET_PRESET_LIST("getReasons($i)", $i, "reason$i");
        $x .= "</td>";
        
        $remark_input = "<input type=\"text\" name=\"remark[]\" id=\"remark$i\" maxlength=\"255\" value=\"".htmlspecialchars($remark, ENT_QUOTES)."\" />";
        if($hasTeacherRemarkWord)
        {
            $remark_input .= $linterface->GET_PRESET_LIST("getTeacherRemarkReasons($i)", '_teacher_remark_'.$i, 'remark'.$i);
        }

  		$office_remark_input = "<input type=\"text\" name=\"office_remark$i\" id=\"office_remark$i\" maxlength=\"255\" value=\"".htmlspecialchars($office_remark, ENT_QUOTES)."\" />";
        $x .= "<td class=\"tabletext\" nowrap>";
        $x .= $remark_input;
        $x .= "</td>";
        $x .= "<td class=\"tabletext\">$office_remark_input</td>";
        $x .= "<td class=\"tabletext\" align=\"center\">";
        $x .= "<a onMouseMove=\"moveObject('ToolMenu2', event);\" onmouseover=\"retrieveGuardianInfo($user_id);\" onmouseout=\"closeLayer('ToolMenu2');\" href=\"#\">";
        $x .= "<img src=\"$image_path/icons_guardian_info.gif\" border=\"0\" alt=\"$button_select\"></a></td>";
        if($sys_custom['send_sms'] and $plugin['sms'] and $bl_sms_version >= 2 and $template_status)        $x .= "<td>$sms_option$hidden_in_school_time</td>";
        if ($plugin['eClassApp']) {
		  	$x .= "<td class=\"tabletext\">";
		  	if (in_array($user_id, $studentWithParentUsingParentAppAry)) {
		  		$x .= "<input type=\"checkbox\" name=\"SendPushMessage[]\" id=\"SendPushMessage_".$i."\" class=\"PushMessageCheckbox\" value=\"".$user_id."\">";
		  	} else {
		  		$x .= "---";
		  	}
			$x .= "</td>";
  		}
  		$x .= "<td class=\"tabletext\">";
		if($date_modified != '' && $last_modify_user!=''){
		  	$x .= $last_modify_user.$Lang['General']['On'].$date_modified;
		}else{
		  	$x .= Get_String_Display('');
		}
		$x .= "</td>";
        $x .= "</tr>\n";
        
}
if (sizeof($result)==0)
{
    $x .= "<tr class=\"tablerow2\"><td class=\"tabletext\" height=\"40\" colspan=\"$col_span\" align=\"center\">$i_StudentAttendance_NoLateStudents</td></tr>\n";
}
if($plugin['eClassApp']){
	$x .= '<tr><td class="tabletextremark" colspan = "'.$col_span.'"><span>'.$Lang['MessageCenter']['ExportMobileRemarks'].'</span></td></tr>';
}
$x .= "</table>\n";
$table_attend = $x;


if($sys_custom['send_sms'] and $plugin['sms'] and $bl_sms_version >= 2 and $template_status)
	$sms_button = "<a href=\"javascript:sendSMS(document.form1)\" class=\"tabletool\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_sms.gif\" border=\"0\" align=\"absmiddle\">". $i_SMS_Send  ."</a>&nbsp;";
else if($sys_custom['send_sms'] && $plugin['sms'] && $bl_sms_version >= 2)
{
	if (!$template_status)
		$sms_button = "<a href=\"javascript:alert('".$Lang['StudentAttendance']['SMSTemplateNotSet']."')\" class=\"tabletool\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_sms.gif\" border=\"0\" align=\"absmiddle\">". $i_SMS_Send  ."</a>&nbsp;";
	else
		$sms_button = "<a href=\"javascript:Prompt_No_SMS_Plugin_Warning()\" class=\"tabletool\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_sms.gif\" border=\"0\" align=\"absmiddle\">". $i_SMS_Send  ."</a>&nbsp;";
}

$table_tool .= "<td nowrap=\"nowrap\">
					$sms_button
				</td>";

if ($plugin['eClassApp']) {
	$push_message_button = "<a href=\"javascript:sendPushMessage()\" class=\"tabletool\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_sms.gif\" border=\"0\" align=\"absmiddle\">". $Lang['AppNotifyMessage']['PushMessage']  ."</a>&nbsp;";
	$table_tool .= "<td nowrap=\"nowrap\">
					$push_message_button
				</td>";
}

if($plugin['Disciplinev12']) {	# check whether eDis "Late" category is set to "Not In Use" , and with any GM Late record in Target Date

	include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
	$ldiscipline = new libdisciplinev12();
	
	$CanPassGmLateChecking = $ldiscipline->Check_Can_Pass_Gm_Late_Checking($TargetDate);

	/*
	if(!$CanPassGmLateChecking) {
		$GmWarningMessage = '<tr>
				<td height="40" style="clear:both"><br style="clear:both">'.$linterface->GET_WARNING_TABLE($Lang['eAttendance']['eDisciplineCategoryNotInUseWarning']).'<br style="clear:both"></td>
			</tr>';					
	}
	*/
} else {
	$CanPassGmLateChecking = 1;	
}

$toolbar  = $linterface->GET_LNK_NEW("javascript:newWindow('insert_record.php?TargetDate=".urlencode($TargetDate)."&DayType=".urlencode($DayType)."&routeID=".urlencode($routeID)."',12)","","","","",0);
$toolbar .= $linterface->GET_LNK_EXPORT("export.php?TargetDate=".urlencode($TargetDate)."&DayType=".urlencode($DayType)."&routeID=".urlencode($routeID)."&order_by_time=".urlencode($order_by_time)."&order=".urlencode($order),"","","","",0);
$toolbar .= $linterface->GET_LNK_PRINT("javascript:openPrintPage()","","","","",0);
if ($plugin['eClassApp']) {
	$toolbar .= $linterface->GET_LNK_EXPORT("javascript:goExportMobile()",$Lang['MessageCenter']['ExportMobile'],"","","",0)."&nbsp;&nbsp;";
}

$TAGS_OBJ[] = array($i_SmartCard_DailyOperation_ViewLateStatus, "", 0);
$MODULE_OBJ = $lc->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START(intranet_htmlspecialchars(urldecode($Msg)));

$PAGE_NAVIGATION[] = array("$TargetDate ($display_period)");

if ($msg == 1) $SysMsg = $linterface->GET_SYS_MSG("add");
if ($msg == 2) $SysMsg = $linterface->GET_SYS_MSG("update");
?>
<script language="Javascript" src='/templates/tooltip.js'></script>
<script language="JavaScript" src="<?=$intranet_httppath?>/templates/ajax_yahoo.js"></script>
<script language="JavaScript" src="<?=$intranet_httppath?>/templates/ajax_connection.js"></script>
<style type="text/css">
     #ToolMenu{position:absolute; top: 0px; left: 0px; z-index:4; visibility:show;}
	 #ToolMenu2{position:absolute; top: 0px; left: 0px; z-index:5; visibility:show;  width: 450px;}
</style>
<script language="JavaScript">
	isMenu = true;
</script>
<div id="ToolMenu" style="position:absolute; width=0px; height=0px; visibility:hidden"></div>
<div id="ToolMenu2" style="position:absolute; width=0px; height=0px; visibility:hidden"></div>
<script language="JavaScript" type="text/javascript">
$(document).ready(function(){
<?php 	
	if ($plugin['eSchoolBus']) {
	    echo "getRouteSelection();\n";    
	}
?>
});

function getTeacherRemarkReasons()
{
    return jArrayWordsTeacherRemark;
}

function getRouteSelection()
{
	var date = $('#TargetDate').val();
	var amPm = $('input:radio[name="DayType"]:checked').val() == '2' ? 'AM' : 'PM';
	var routeSearchSpan = $('#ajax_route_selection');
	routeSearchSpan.html('<img src="<?php echo "{$image_path}/{$LAYOUT_SKIN}/indicator.gif";?>">');
	$.ajax({
		url: "ajax.php",
		type: "POST",
		data: {
			action: 'routeSelection_dateSearch',
			date : date,
			routeID: '<?php echo $routeID;?>',
			amPm: amPm
		},
		dataType: "html",
		success: function(response){
			routeSearchSpan.html(response);
		}
	});
}

function getReasons(pos)
{
	var jArrayTemp = new Array();
	var obj2 = document.getElementById("drop_down_status["+pos+"]");
	if (obj2.selectedIndex == 2)
	{
	  return AbsentArrayWords;
	}
	else if (obj2.selectedIndex == 1)
	{
		return LateArrayWords;
	}
	else 
		return jArrayTemp;
}

function Check_Time_Format(InputTime,WarningLayer) {
	if (InputTime.Trim() == "" || check_time_without_return_msg(InputTime.Trim())) {
		$('div#'+WarningLayer).html('');
	}
	else {
		$('div#'+WarningLayer).html('<?=$Lang['StudentAttendance']['SlotTimeFormatWarning']?>');
	}
}
	
function Prompt_No_SMS_Plugin_Warning()
{
	alert("<?=$i_SMS['jsWarning']['NoPluginWarning']?>");
}

function setAllWaived(formObj,val){
        if(formObj==null) return;
        for(i=0;i<formObj.elements.length;i++){
                obj = formObj.elements[i];
                if(typeof(obj)!="undefined"){
                        if(obj.name.indexOf("waived_")>-1){
                                obj.checked = val==1?true:false;
                        }
                }
        }
}
function setAllSend(formObj,val){
        if(formObj==null) return;
        for(i=0;i<formObj.elements.length;i++){
                obj = formObj.elements[i];
                if(typeof(obj)!="undefined"){
                        if(obj.name.indexOf("sms_")>-1){
                                obj.checked = val==1?true:false;
                        }
                }
        }
}

function sendSMS(formObj)
{
        var sms_no=0;
        for(i=0;i<formObj.elements.length;i++)
        {
                obj = formObj.elements[i];
                if(typeof(obj)!="undefined"){
                        if(obj.name.indexOf("sms_")>-1){
                                if(obj.checked)        sms_no = 1;
                        }
                }
        }

        if(sms_no)
        {
                formObj.action = "../confirm_send_sms.php";
                formObj.submit();
        }
        else
        {
                alert("<?=$i_SMS_no_student_select?>");
        }
}

function sendPushMessage() {
	var SendPushMessage = document.getElementsByName('SendPushMessage[]');
	var SendPushMessageChecked = false;
	for (var i=0; i< SendPushMessage.length; i++) {
		if (SendPushMessage[i].checked) {
			SendPushMessageChecked = true;
			break;
		}
	}
	
	if (SendPushMessageChecked == true) {
		document.getElementById('form1').action = "../send_push_message.php";
		document.getElementById('form1').submit();
	}
	else 
		alert("<?=$i_SMS_no_student_select?>");
}

function orderByTime(v){
        fObj = document.form1;
        if(fObj==null ) return;
        orderByTimeObj = fObj.order_by_time;
        if(orderByTimeObj==null) return;
        orderByTimeObj.value = v;
        fObj.action="";
        fObj.submit();

}
var reasonSelection = Array();
function temp_reasonSelection(i, remark)
{
        str = "<table width=100% border=0 cellpadding=1 cellspacing=1>";
    if(remark!="")
    {
            // remark = remark.replace(/&#039;/g, "\\'");
            remark = remark.replace("'", "&#039;");
        str += "<tr><td><font color=red><b>[<?=$i_Attendance_Others?>]</b></font></td></tr>";
            str += "<tr><td><table border=0 cellspacing=0 cellpadding=0 width=100%><tr><td valign=top width=8><font color=red> - </font></td><td align=left><a href='javascript:putBack(document.form1.reason"+i+",\"rrr_remark_"+i+"\");'><font color=red>"+remark+"</font></a><input type='hidden' id='rrr_remark_"+i+"' value='"+remark+"'></td></tr></table><Br></td></tr>";
           }
           str += "<tr><td><b>[<?=$i_Attendance_Standard?>]</b></td></tr>";

           <?
    for ($j=0; $j<sizeof($words); $j++)
    {
                $temp = addslashes($words[$j]);
                $temp2 = addslashes($temp);
        ?>
                str += "<tr><td><table border=0 cellpaddin=0 cellspacing=0 width=100%><tr><td valign=top width=8> - </td><td align=left><a href='javascript:putBack(document.form1.reason"+i+",\"rrr_"+i+"_<?=$j?>\");'><?=$temp?></a><input type='hidden' id='rrr_"+i+"_<?=$j?>' value='<?=$temp?>'></td></tr></table></td></tr>";
    <?
        }
        ?>
    str += "</table>";

    reasonSelection[i] = "<table border=0 cellspacing=0 cellpadding=1><tr><td class=tipborder><table width=100% border=0 cellspacing=0 cellpadding=3><Td class=tipbg><table border=0 cellspacing=0 cellpadding=0><tr><td><input type=button value=' X ' onClick=hideMenu('ToolMenu')></td></tr></table></td></tr><tr><td class=tipbg valign=top><font size=-2>"+str+"</font></td></tr></table></td></tr></table>";

    return reasonSelection[i];
}
<?//=$reason_js_array?>
<!--
function openPrintPage()
{
        newWindow("showlate_print.php?DayType=<?=urlencode($DayType)?>&TargetDate=<?=urlencode($TargetDate)?>&routeID=<?=urlencode($routeID)?>&order_by_time=<?=urlencode($order_by_time)?>&order=<?=urlencode($order)?>",4);
}
function showSelection(i, allowed, remarkObjName)
{
                remarkObj = document.getElementById(remarkObjName);
        if(remarkObj!=null)
                remark = remarkObj.value;
        else remark = "";
         if (allowed == 1)
         {
                        writeToLayer('ToolMenu',temp_reasonSelection(i,remark));

                 halfLayerHeight = parseInt(eval(doc + "ToolMenu" +".offsetHeight"))/2;
                 if(mouse.y-halfLayerHeight >0)
                         t = mouse.y-halfLayerHeight;
                 else t = mouse.y-halfLayerHeight;

                 l = mouse.x;
            moveToolTip2('ToolMenu', t, l);
                     showLayer('ToolMenu');
         }
}
function moveToolTip2(lay, FromTop, FromLeft){

     if (tooltip_ns6) {

         var myElement = document.getElementById(lay);

          myElement.style.left = (FromLeft + 10) + "px";

          myElement.style.top = (FromTop + document.body.scrollTop) + "px";

     }

     else if(tooltip_ie4) {
          eval(doc + lay + sty + ".top = "  + (FromTop + document.body.scrollTop))
     }
         else if(tooltip_ns4){eval(doc + lay + sty + ".top = "  +  FromTop)}

     if (!tooltip_ns6) eval(doc + lay + sty + ".left = " + (FromLeft + 10));

}
/*
function putBack(obj, value)
{
         obj.value = value;
         hideMenu('ToolMenu');
}
*/
function putBack(obj, valueObjName)
{
             valueObj = document.getElementById(valueObjName);

                 if(valueObj!=null)
                 obj.value = valueObj.value;
         else obj.value="";
         hideMenu('ToolMenu');
}

function displayCheckbox(status, user_id)
{
	if(status==<?=CARD_STATUS_OUTING?> || status==<?=CARD_STATUS_PRESENT?>)	// hidden the checkbox
	{
		eval("document.getElementById('cb_"+ user_id +"').innerHTML = '';");
	}
	else
	{
		eval("document.getElementById('cb_"+ user_id +"').innerHTML = '<input type=\"checkbox\" name=\"waived_"+ user_id +"\">';");
	}
}

function setReasonComp(value, txtComp, hiddenFlag, index)
{
         if (value==<?=CARD_STATUS_LATE?> || value==<?=CARD_STATUS_ABSENT?>)
         {
             txtComp.disabled = false;
             txtComp.style.background='<?=$enable_color?>';
             hiddenFlag.value = 1;
           <?if($sys_custom['SmartCardAttendance_StudentAbsentSession']){?>
	       		lateSessionObj = document.getElementById('late_session'+index);
	       		if(lateSessionObj != null)
	       		{
	       			lateSessionObj.style.display = "inline";
	       		}
	         <?}?>
	         
	         if (value==<?=CARD_STATUS_ABSENT?>) {
	         	if (document.getElementById('LateTime['+index+']')) {
	         		document.getElementById('LateTime['+index+']').value = "";
	         		document.getElementById('LateTime['+index+']').style.display = "none";
	         	}
	        }
	        else {
	        	if (document.getElementById('LateTime['+index+']')) {
	         		document.getElementById('LateTime['+index+']').style.display = "";
	         	}
	        }
	        if($('#office_remark'+index).length>0){
				$('#office_remark'+index).attr('disabled','');
			}
         }
         else
         {
             txtComp.disabled = true;
             txtComp.style.background='<?=$disable_color?>';
                         txtComp.value="";
             hiddenFlag.value = 0;
             hideMenu('ToolMenu');
             <?if($sys_custom['SmartCardAttendance_StudentAbsentSession']){?>
	       		lateSessionObj = document.getElementById('late_session'+index);
	       		if(lateSessionObj != null)
	       		{
	       			lateSessionObj.style.display = "none";
	       		}
	         <?}?>
	         
	         if (document.getElementById('LateTime['+index+']')) {
	         		document.getElementById('LateTime['+index+']').value = "";
	         		document.getElementById('LateTime['+index+']').style.display = "none";
	         }
	         	
	         if($('#office_remark'+index).length>0){
				$('#office_remark'+index).attr('disabled','disabled');
			 }
         }
}
function resetForm(formObj){
        formObj.reset();
          hideMenu('ToolMenu');
        resetFields(formObj);
}
function resetFields(formObj){
        status_list = document.getElementsByTagName('SELECT');
        if(status_list==null) return;

        for(i=0;i<status_list.length;i++){
                s = status_list[i];
                if(s==null) continue;
                reasonObj = eval('formObj.reason'+i);
                if(s.selectedIndex==1){
                        if(reasonObj!=null){
                                reasonObj.disabled=false;
                                reasonObj.style.background='<?=$enable_color?>';
								<?if($sys_custom['SmartCardAttendance_StudentAbsentSession']){?>
					           		lateSessionObj = document.getElementById('late_session'+i);
					           		if(lateSessionObj != null)
					           		{
					           			lateSessionObj.style.display = "inline";
					           		}
				           	  	<?}?>
                        }
                }else{
                        if(reasonObj!=null){
                                reasonObj.value="";
                                reasonObj.disabled=true;
                                reasonObj.style.background='<?=$disable_color?>';
                        }
                        
                        <?if($sys_custom['SmartCardAttendance_StudentAbsentSession']){?>
			           		lateSessionObj = document.getElementById('late_session'+i);
			           		if(lateSessionObj != null)
			           		{
			           			lateSessionObj.style.display = "none";
			           		}
		           	  	<?}?>
                }

        }
}
// AJAX follow-up
var callback = {
        success: function ( o )
        {
                jChangeContent( "ToolMenu2", o.responseText );
        }
}
// start AJAX
function retrieveGuardianInfo(UserID)
{
        //FormObject.testing.value = 1;
        obj = document.form1;
        var myElement = document.getElementById("ToolMenu2");

        showMenu("ToolMenu2","<table border=1 width=300 bordercolorlight=#FBFDEA bordercolordark=#B3B692 cellpadding=2 cellspacing=0 bgcolor=#FBFDEA><tr><td>Loading</td></tr></table>");
        myElement.style.display = 'block';
				myElement.style.visibility = 'visible';
        YAHOO.util.Connect.setForm(obj);

        // page for processing and feedback
        var path = "getGuardianInfo.php?targetUserID=" + UserID;
        var connectionObject = YAHOO.util.Connect.asyncRequest("POST", path, callback);
}
/*
function changePage(val)
{
	var obj = document.form1;
	var path = "/home/eAdmin/StudentMgmt/attendance/dailyoperation";
	var period = document.form1.period.value;
	var date = document.form1.TargetDate.value;
	
	if(val == 1){
		obj.action = path+"/class/class_status.php?TargetDate="+date+"&period="+period;
		obj.submit();
	}
	if(val == 2){
		obj.action = path+"/absence/show.php?TargetDate="+date+"&period="+period;
		obj.submit();
	}
	if(val == 3){
		obj.action = path+"/early/show.php?TargetDate="+date+"&period="+period;
		obj.submit();
	}
}
*/
function moveObject( obj, e, moveLeft ) {
	var tempX = 0;
	var tempY = 0;
	var offset = 8;
	var objHolder = obj;
	var moveDistance = 0;
	
	obj = document.getElementById(obj);
	if (obj==null) {return;}
	
	if (document.all) {
		var ScrollLeft = document.documentElement.scrollLeft || document.body.scrollLeft;
		var ScrollHeight = document.documentElement.scrollTop || document.body.scrollTop;
		tempX = event.clientX + ScrollLeft;
		tempY = event.clientY + ScrollHeight;
	}
	else {
		tempX = e.pageX
		tempY = e.pageY
	}

	if(moveLeft == undefined) {
		moveDistance = 300;		
	} else {
		moveDistance = moveLeft;
	}
	
	if (tempX < 0){tempX = 0} else {tempX = tempX - moveDistance}
	if (tempY < 0){tempY = 0} else {tempY = tempY}

	obj.style.top  = (tempY + offset) + 'px';
	obj.style.left = (tempX + offset) + 'px';
}

function closeLayer(LayerID) {
	var obj = document.getElementById(LayerID);
	obj.style.display = 'none';
	obj.style.visibility = 'hidden';
}

function Check_View_Form() {
	if ($('#DPWL-TargetDate').html() == "") {
		document.getElementById('form2').submit();
	}
}

function Check_Form() {
	<? if(!$CanPassGmLateChecking) {?>
		alert('<?=$Lang['eAttendance']['eDisciplineCategoryNotInUseWarning']?>');
		return false;
	<?}?>
	var LateOK = true;
	$('div.LateTimeWarningLayer').each(function() {
		if ($(this).html() != "") {
			LateOK = false;
		}
	});
	
	if (LateOK) {
		AlertPost(document.form1,'showlate_update.php','<?=$i_SmartCard_Confirm_Update_Attend?>');
	}
}

function SetAllReason() {
	var StatusSelection = document.getElementsByName("drop_down_status[]");
	var ApplyReason = document.getElementById("SetAllReason").value;
	for (var i=0; i< StatusSelection.length; i++) {
		if (StatusSelection[i].selectedIndex == 1) // late
		{
		  document.getElementById('reason'+i).value = ApplyReason;
		} 
	}
}
function SetAllAttend() {
	var AttendValue = document.getElementById("drop_down_status_all").value;
	var AttendSelection = document.getElementsByName('drop_down_status[]').length;
	for (var i=0; i< AttendSelection; i++) {
 		document.getElementById('drop_down_status['+i+']').value = AttendValue;
 		
 		var targetUserID = document.getElementsByName('user_id['+i+']')[0].value;
 		var targetReasonEle = document.getElementById('reason'+i);
 		var targetEditEle = document.getElementsByName('editAllowed'+i)[0];
 		
 		displayCheckbox(AttendValue, targetUserID);
 		setReasonComp(AttendValue, targetReasonEle, targetEditEle, i);
	}
}

function goExportMobile(){
	var url = '<?=$PATH_WRT_ROOT?>' + '/home/eAdmin/GeneralMgmt/MessageCenter/export_mobile.php';
	$('form#form2').attr('action', url ).submit();
	$('form#form2').attr('action', '' );
}
-->
</script>
<br />
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	</tr>
	<?=$GmWarningMessage?>
	<tr>
		<td>
			<?=$table_confirm?>
		</td>
	</tr>
	<tr>
		<td>
			<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
<?
if($confirmed==1)
{
?>
        <tr>
        	<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_StudentAttendance_Field_LastConfirmedTime?>:</td>
        	<td class="tabletext" width="70%"><?=$confirmTime.($confirmUser!=''?'&nbsp;('.$confirmUser.')':'')?></td>
        </tr>
<?
}
else
{
?>
				<tr>
        	<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_StudentAttendance_Field_LastConfirmedTime?>:</td>
        	<td class="tabletext" width="70%"><?=$i_SmartCard_Frontend_Take_Attendance_No_Confirm_Record?></td>
        </tr>
<?
}
?>
			<?=$StudentAttendUI->Get_Class_Status_Shortcut(PROFILE_TYPE_LATE)?>
			</table>	
		</td>
	</tr>
</table>
<form name="form1" id="form1" method="post" action="showlate_update.php" >
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>
			<table width="95%" border="0" cellpadding="0" cellspacing="0" align="center" onMouseMove="overhere()">
				<tr>
					<td align="left"><?= $toolbar ?></td>
					<td align="right"><?= $SysMsg ?></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table width="95%" border="0" cellpadding="0" cellspacing="0" align="center">
				<tr class="table-action-bar">
					<td valign="bottom" align="left">
						<table border=0>
							<tr>
								<td class="attendance_late" width="15px">
									&nbsp;
								</td>
								<td class="tabletext">
									<?=$Lang['StudentAttendance']['Confirmed']?>
								</td>
							</tr>
						</table>
					</td>
					<td valign="bottom" align="right">
						<table border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td width="21"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_01.gif" width="21" height="23"></td>
								<td background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_02.gif">
									<table border="0" cellspacing="0" cellpadding="2">
										<tr>
											<?=$table_tool?>
										</tr>
									</table>
								</td>
								<td width="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_03.gif" width="6" height="23"></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<?=$table_attend?>
		</td>
	</tr>
	<tr>
		<td>
			<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
		    	<tr>
                	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
		    </table>	
		</td>
	</tr>
	<tr>
		<td>
			<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
		    	<tr>
				    <td align="center" colspan="2">
				    <? if (sizeof($result)!=0) { ?>
						<?= $linterface->GET_ACTION_BTN($button_save, "button", "Check_Form();") ?>
						<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
					<? } ?>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<input id="DayType" name="DayType" type="hidden" value="<?=escape_double_quotes($DayType)?>">
<input name="TargetDate" type="hidden" value="<?=escape_double_quotes($TargetDate)?>">
<input id="routeID" name="routeID" type="hidden" value="<?=escape_double_quotes($routeID)?>">
<input name=PageLoadTime type=hidden value="<?=time()+1?>">
<input name="order" type="hidden" value="<?=escape_double_quotes($order)?>">
<input name="order_by_time" type="hidden" value="<?=escape_double_quotes($order_by_time)?>">
<? for ($i=0; $i<sizeof($result); $i++) { ?>
<input type="hidden" name="editAllowed<?=$i?>" value="1">
<? } ?>
<input type="hidden" name="this_page_title" value="<?=$i_SmartCard_DailyOperation_ViewLateStatus?>">
<input type="hidden" name="this_page_nav" value="PageDailyOperation_ViewLateStatus">
<input type="hidden" name="this_page" value="late/showlate.php?TargetDate=<?=urlencode($TargetDate)?>&DayType=<?=urlencode($DayType)?>&routeID=<?php echo urlencode($routeID);?>">
<input type="hidden" name="TemplateCode" value="STUDENT_ATTEND_LATE">
</form>
<form id="form2" name="form2" method="post" action="" >
<input type="hidden" name="StudentIDArr" value="<?=rawurlencode(serialize($StudentIDArr))?>">
</form>
<br />
<?
$linterface->LAYOUT_STOP();
print $linterface->FOCUS_ON_LOAD("form2.TargetDate");
intranet_closedb();
?>