<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");

intranet_auth();
intranet_opendb();

$lcardstudentattend2 = new libcardstudentattend2();

### check access right
$canAccess = false;
if ($_SESSION["SSV_PRIVILEGE"]["plugin"]['attendancestudent'] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] && $plugin['eClassApp']) {
    $canAccess = true;
}
if (!$canAccess) {
    No_Access_Right_Pop_Up();
}


$recordID = $_POST['recordID'];

$successAry = array();

$sql = "Update 
                REPRINT_CARD_RECORD
        Set
                CardStatus = '1',
                ModifiedDate = now(),
                ModifiedBy = '" . $_SESSION['UserID'] . "'
        Where
                RecordID = '" . $recordID . "'
        ";


$successAry['Approve'] = $lcardstudentattend2->db_db_query($sql);

if (in_array(false, $successAry)) {
    $returnMsgKey = 'RecordApproveUnSuccess';
}
else {
    $returnMsgKey = 'RecordApproveSuccess';
}

$sql1= "Select
                r.RecordID
        From 
                REPRINT_CARD_RECORD as r
                Inner Join INTRANET_USER as iu ON (r.StudentID = iu.UserID)
                Inner Join YEAR_CLASS_USER as ycu ON (iu.UserID = ycu.UserID)
                Inner Join YEAR_CLASS as yc ON (ycu.YearClassID = yc.YearClassID)
        Where
                r.CardStatus = 0
                And yc.AcademicYearID = '".Get_Current_Academic_Year_ID()."'
        Order By r.ApplyDate desc
		limit 1  
        ";
$result = $lcardstudentattend2->returnArray ( $sql1 );

intranet_closedb();

$numOfRecord = count ( $result );

if($numOfRecord==0){
    header('Location: list.php?returnMsgKey='.$returnMsgKey);
}else{
    header('Location: content_list.php?recordID='.$result[0]['RecordID'].'&returnMsgKey='.$returnMsgKey);

}
?>