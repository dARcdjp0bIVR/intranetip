<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");

### get parameters
$returnMsgKey = standardizeFormGetValue($_GET['returnMsgKey']);

include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$lcardstudentattend2 = new libcardstudentattend2();

$recordID = $_POST['recordID'];

### check access right
$canAccess = false;
if ($_SESSION["SSV_PRIVILEGE"]["plugin"]['attendancestudent'] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] && $plugin['eClassApp']) {
    $canAccess = true;
}
if (!$canAccess) {
    No_Access_Right_Pop_Up();
}

### show return message
$returnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];


### tab display
$TAGS_OBJ[] = array($Lang['StudentAttendance']['ReprintCard']['ReprintCard(App)']);
$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageDailyOperation_ReprintCard";
$MODULE_OBJ = $lcardstudentattend2->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START($returnMsg);

### SQL part
$classNameField = Get_Lang_Selection('yc.ClassTitleB5', 'yc.ClassTitleEN');
$CardStatus ="CASE r.CardStatus
				   WHEN 0 THEN '".$Lang['StudentAttendance']['ReprintCard']['PendingConfirm']."'
				   WHEN 1 THEN '".$Lang['StudentAttendance']['ReprintCard']['PendingPayment']."'
				   WHEN 2 THEN '".$Lang['StudentAttendance']['ReprintCard']['AwaitingCardReprint']."'
				   WHEN 3 THEN '".$Lang['StudentAttendance']['ReprintCard']['Sent']."'
			       WHEN 4 THEN '".$Lang['StudentAttendance']['ReprintCard']['ApplicationCompleted']."'
				   WHEN 5 THEN '".$Lang['StudentAttendance']['ReprintCard']['Rejected']."'
				   WHEN 6 THEN '".$Lang['StudentAttendance']['ReprintCard']['Cancelled']."'
			   END";

$ShippingMethod = "CASE r.DeliveryMethod
                        WHEN 1 THEN '".$Lang['StudentAttendance']['ReprintCard']['PickUp']."'
                        WHEN 2 THEN '".$Lang['StudentAttendance']['ReprintCard']['CourierToApplicant']."'
                   END";

$sql = "Select
                r.PhotoPath,
                iu.ChineseName as StudentNameChi,
                iu.EnglishName as StudentNameEng,
                $classNameField as ClassName, 
                ycu.ClassNumber, 
                r.ReprintCardReason,
                $ShippingMethod as ShippingMethod,
                r.ApplyDate,
                $CardStatus as CardStatus,
                r.StudentSTRN,
                r.StudentBarcode,
                r.StudentSociety,
                r.StudentPPSAccountNo,
                r.IssueDate,
                r.ExpiryDate,
                r.YearName1,
                r.YearName2,
                r.YearName3,
                r.YearName4,
                r.YearName5,
                r.YearName6,
                r.YearName7,
                r.YearName8,
                r.YearName9,
                r.YearName10,
                r.YearName11,
                r.YearName12,
                r.ClassName1,
                r.ClassName2,
                r.ClassName3,
                r.ClassName4,
                r.ClassName5,
                r.ClassName6,
                r.ClassName7,
                r.ClassName8,
                r.ClassName9,
                r.ClassName10,
                r.ClassName11,
                r.ClassName12,
                r.TeacherRemark
        From 
                REPRINT_CARD_RECORD as r
                Inner Join INTRANET_USER as iu ON (r.StudentID = iu.UserID)
                Inner Join YEAR_CLASS_USER as ycu ON (iu.UserID = ycu.UserID)
                Inner Join YEAR_CLASS as yc ON (ycu.YearClassID = yc.YearClassID)
		Where
			r.RecordID = '" . $recordID . "'
		     And yc.AcademicYearID = '".Get_Current_Academic_Year_ID()."'
		";
$result = $lcardstudentattend2->returnArray ( $sql );

$x .= '<table class="form_table_v30">'."\r\n";

//student photo
$x .= '<tr >'."\r\n";
$x .= '<td class="field_title">'.$Lang['StudentAttendance']['ReprintCard']['StudentPhoto'].'</td>'."\r\n";
$x .= '<td><img src="'.$result[0]['PhotoPath'].'" border="0" align="absmiddle" width="100px" /><br><br><input name="NewPhoto" id="NewPhoto" type="file" accept=".png, .jpg, .jpeg"/></td>'."\r\n";
$x .= '</tr>'."\r\n";

//student name chi
$x .= '<tr>'."\r\n";
$x .= '<td class="field_title">'.$Lang['General']['StudentNameChi'].'</td>'."\r\n";
$x .= '<td>'.$result[0]['StudentNameChi'].'</td>'."\r\n";
$x .= '</tr>'."\r\n";

//student name eng
$x .= '<tr>'."\r\n";
$x .= '<td class="field_title">'.$Lang['General']['StudentNameEng'].'</td>'."\r\n";
$x .= '<td>'.$result[0]['StudentNameEng'].'</td>'."\r\n";
$x .= '</tr>'."\r\n";

//student class
$x .= '<tr>'."\r\n";
$x .= '<td class="field_title">'.$Lang['General']['Class'].'</td>'."\r\n";
$x .= '<td>'.$result[0]['ClassName'].'</td>'."\r\n";
$x .= '</tr>'."\r\n";

//student class number
$x .= '<tr>'."\r\n";
$x .= '<td class="field_title">'.$Lang['StudentAttendance']['ReprintCard']['ClassNumber'].'</td>'."\r\n";
$x .= '<td>'.$result[0]['ClassNumber'].'</td>'."\r\n";
$x .= '</tr>'."\r\n";

//student id
$x .= '<tr>'."\r\n";
$x .= '<td class="field_title">'.$Lang['StudentAttendance']['ReprintCard']['StudentID'].'</td>'."\r\n";
$x .= '<td>'.$linterface->GET_TEXTBOX('StudentSTRN','StudentSTRN', $result[0]['StudentSTRN']).'</td>'."\r\n";
$x .= '</tr>'."\r\n";

//student barcode
$x .= '<tr>'."\r\n";
$x .= '<td class="field_title">'.$Lang['AccountMgmt']['Barcode'].'</td>'."\r\n";
$x .= '<td>'.$linterface->GET_TEXTBOX('StudentBarcode','StudentBarcode', $result[0]['StudentBarcode']).'</td>'."\r\n";
$x .= '</tr>'."\r\n";

//student Society
$x .= '<tr>'."\r\n";
$x .= '<td class="field_title">'.$Lang['StudentAttendance']['ReprintCard']['Society'].'</td>'."\r\n";
$x .= '<td>'.$linterface->GET_TEXTBOX('StudentSociety','StudentSociety', $result[0]['StudentSociety']).'</td>'."\r\n";
$x .= '</tr>'."\r\n";

//student PPS
$x .= '<tr>'."\r\n";
$x .= '<td class="field_title">'.$Lang['StudentAttendance']['ReprintCard']['PPS'].'</td>'."\r\n";
$x .= '<td>'.$linterface->GET_TEXTBOX('StudentPPSAccountNo','StudentPPSAccountNo', $result[0]['StudentPPSAccountNo']).'</td>'."\r\n";
$x .= '</tr>'."\r\n";

// YEAR 12

$x .= '<tr>'."\r\n";
$x .= '<td class="field_title">'.$Lang['StudentAttendance']['ReprintCard']['Year(1-12)'].'</td>'."\r\n";
$x .= '<td><table>
       <tr><td style="border:0px">1</td>
       <td style="border:0px">2</td>
       <td style="border:0px">3</td>
       <td style="border:0px">4</td>
       <td style="border:0px">5</td>
       <td style="border:0px">6</td>
       <td style="border:0px">7</td>
       <td style="border:0px">8</td>
       <td style="border:0px">9</td>
       <td style="border:0px">10</td>
       <td style="border:0px">11</td>
       <td style="border:0px">12</td></tr>
       <tr><td style="border:0px">'.$linterface->GET_TEXTBOX('YearName1','YearName1', $result[0]['YearName1']).'</td>
       <td style="border:0px">'.$linterface->GET_TEXTBOX('YearName2','YearName2', $result[0]['YearName2']).'</td>
       <td style="border:0px">'.$linterface->GET_TEXTBOX('YearName3','YearName3', $result[0]['YearName3']).'</td>
       <td style="border:0px">'.$linterface->GET_TEXTBOX('YearName4','YearName4', $result[0]['YearName4']).'</td>
       <td style="border:0px">'.$linterface->GET_TEXTBOX('YearName5','YearName5', $result[0]['YearName5']).'</td>
       <td style="border:0px">'.$linterface->GET_TEXTBOX('YearName6','YearName6', $result[0]['YearName6']).'</td>
       <td style="border:0px">'.$linterface->GET_TEXTBOX('YearName7','YearName7', $result[0]['YearName7']).'</td>
       <td style="border:0px">'.$linterface->GET_TEXTBOX('YearName8','YearName8', $result[0]['YearName8']).'</td>
       <td style="border:0px">'.$linterface->GET_TEXTBOX('YearName9','YearName9', $result[0]['YearName9']).'</td>
       <td style="border:0px">'.$linterface->GET_TEXTBOX('YearName10','YearName10', $result[0]['YearName10']).'</td>
       <td style="border:0px">'.$linterface->GET_TEXTBOX('YearName11','YearName11', $result[0]['YearName11']).'</td>
       <td style="border:0px">'.$linterface->GET_TEXTBOX('YearName12','YearName12', $result[0]['YearName12']).'</td></tr></table></td>'."\r\n";
$x .= '</tr>'."\r\n";

// CLASS 12

$x .= '<tr>'."\r\n";
$x .= '<td class="field_title">'.$Lang['StudentAttendance']['ReprintCard']['Class(1-12)'].'</td>'."\r\n";
$x .= '<td><table>
       <tr><td style="border:0px">1</td>
       <td style="border:0px">2</td>
       <td style="border:0px">3</td>
       <td style="border:0px">4</td>
       <td style="border:0px">5</td>
       <td style="border:0px">6</td>
       <td style="border:0px">7</td>
       <td style="border:0px">8</td>
       <td style="border:0px">9</td>
       <td style="border:0px">10</td>
       <td style="border:0px">11</td>
       <td style="border:0px">12</td></tr>
       <tr><td style="border:0px">'.$linterface->GET_TEXTBOX('ClassName1','ClassName1', $result[0]['ClassName1']).'</td>
       <td style="border:0px">'.$linterface->GET_TEXTBOX('ClassName2','ClassName2', $result[0]['ClassName2']).'</td>
       <td style="border:0px">'.$linterface->GET_TEXTBOX('ClassName3','ClassName3', $result[0]['ClassName3']).'</td>
       <td style="border:0px">'.$linterface->GET_TEXTBOX('ClassName4','ClassName4', $result[0]['ClassName4']).'</td>
       <td style="border:0px">'.$linterface->GET_TEXTBOX('ClassName5','ClassName5', $result[0]['ClassName5']).'</td>
       <td style="border:0px">'.$linterface->GET_TEXTBOX('ClassName6','ClassName6', $result[0]['ClassName6']).'</td>
       <td style="border:0px">'.$linterface->GET_TEXTBOX('ClassName7','ClassName7', $result[0]['ClassName7']).'</td>
       <td style="border:0px">'.$linterface->GET_TEXTBOX('ClassName8','ClassName8', $result[0]['ClassName8']).'</td>
       <td style="border:0px">'.$linterface->GET_TEXTBOX('ClassName9','ClassName9', $result[0]['ClassName9']).'</td>
       <td style="border:0px">'.$linterface->GET_TEXTBOX('ClassName10','ClassName10', $result[0]['ClassName10']).'</td>
       <td style="border:0px">'.$linterface->GET_TEXTBOX('ClassName11','ClassName11', $result[0]['ClassName11']).'</td>
       <td style="border:0px">'.$linterface->GET_TEXTBOX('ClassName12','ClassName12', $result[0]['ClassName12']).'</td></tr></table></td>'."\r\n";
$x .= '</tr>'."\r\n";$x .= '</tr>'."\r\n";

//Issue Date
$x .= '<tr>'."\r\n";
$x .= '<td class="field_title">'.$Lang['StudentAttendance']['ReprintCard']['IssueDate'].'</td>'."\r\n";
$x .= '<td>'.$linterface->GET_DATE_PICKER("IssueDate",$result[0]['IssueDate']?date('Y-m-d',strtotime($result[0]['IssueDate'])):date('Y-m-d')).'</td>'."\r\n";
$x .= '</tr>'."\r\n";

//Expiry Date
$x .= '<tr>'."\r\n";
$x .= '<td class="field_title">'.$Lang['StudentAttendance']['ReprintCard']['ExpiryDate'].'</td>'."\r\n";
$x .= '<td>'.$linterface->GET_DATE_PICKER("ExpiryDate", $result[0]['ExpiryDate']?date('Y-m-d',strtotime($result[0]['ExpiryDate'])):date('Y-m-d')).'</td>'."\r\n";
$x .= '</tr>'."\r\n";

//Replacement Reason
$x .= '<tr>'."\r\n";
$x .= '<td class="field_title">'.$Lang['StudentAttendance']['ReprintCard']['ReplacementReason'].'</td>'."\r\n";
$x .= '<td>'.$result[0]['ReprintCardReason'].'</td>'."\r\n";
$x .= '</tr>'."\r\n";

//Delivery Method
$x .= '<tr>'."\r\n";
$x .= '<td class="field_title">'.$Lang['StudentAttendance']['ReprintCard']['DeliveryMethod'].'</td>'."\r\n";
$x .= '<td>'.$result[0]['ShippingMethod'].'</td>'."\r\n";
$x .= '</tr>'."\r\n";

//Apply Date
$x .= '<tr>'."\r\n";
$x .= '<td class="field_title">'.$Lang['StudentAttendance']['ReprintCard']['ApplyDate'].'</td>'."\r\n";
$x .= '<td>'.$result[0]['ApplyDate'].'</td>'."\r\n";
$x .= '</tr>'."\r\n";

//Status
$x .= '<tr>'."\r\n";
$x .= '<td class="field_title">'.$Lang['StudentAttendance']['ReprintCard']['Status'].'</td>'."\r\n";
$x .= '<td>'.$result[0]['CardStatus'].'</td>'."\r\n";
$x .= '</tr>'."\r\n";

//Teacher Remark
$x .= '<tr>'."\r\n";
$x .= '<td class="field_title">'.$Lang['StudentAttendance']['ReprintCard']['TeacherRemark'].'</td>'."\r\n";
$x .= '<td>'.$linterface->GET_TEXTBOX('TeacherRemark','TeacherRemark', $result[0]['TeacherRemark']).'</td>'."\r\n";
$x .= '</tr>'."\r\n";

$x .= '</table>'."\r\n";
$htmlAry['contentTable'] = $x;


### buttons
$htmlAry['submitButton'] = $linterface->GET_ACTION_BTN($Lang['StudentAttendance']['Confirm'], 'button', 'goSubmit()');
$htmlAry['cancelButton'] = $linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], 'button', 'goCancel()');

?>
    <script type="text/javascript">
        $(document).ready( function() {

        });

        function goSubmit() {
            $('form#form1').submit();
        }

        function goCancel() {
            $('form#form1').attr('action', 'list.php').submit();
        }

    </script>
    <form name="form1" id="form1" enctype="multipart/form-data" method="POST" action="content_edit_update.php">
        <div class="table_board">
            <?=$htmlAry['contentTable']?>
        </div>
        <br />
        <div class="edit_bottom">
            <p class="spacer"></p>
            <?=$htmlAry['submitButton']?>
            <?=$htmlAry['cancelButton']?>
            <p class="spacer"></p>
        </div>
        <input type="hidden" name="recordID" id="recordID" value="<?=$recordID?>">
    </form>
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>