<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");

### get parameters
$returnMsgKey = standardizeFormGetValue($_GET['returnMsgKey']);

include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$lcardstudentattend2 = new libcardstudentattend2();

$recordID = $_POST['recordID'];
$studentID = $_POST['studentID'];

### check access right
$canAccess = false;
if ($_SESSION["SSV_PRIVILEGE"]["plugin"]['attendancestudent'] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] && $plugin['eClassApp']) {
    $canAccess = true;
}
if (!$canAccess) {
    No_Access_Right_Pop_Up();
}

### show return message
$returnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];


### tab display
$TAGS_OBJ[] = array($Lang['StudentAttendance']['ReprintCard']['ReprintCard(App)']);
$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageDailyOperation_ReprintCard";
$MODULE_OBJ = $lcardstudentattend2->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START($returnMsg);

$x .= '<table class="form_table_v30">'."\r\n";

//student id
$x .= '<tr>'."\r\n";
$x .= '<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['StudentAttendance']['ReprintCard']['SmartCardID'].'</td>'."\r\n";
$x .= '<td>'.$linterface->GET_TEXTBOX('SmartCardID','SmartCardID','').'</td>'."\r\n";
$x .= '</tr>'."\r\n";

$x .= '</table>'."\r\n";
$htmlAry['contentTable'] = $x;


### buttons
$htmlAry['finishButton'] = $linterface->GET_ACTION_BTN($Lang['StudentAttendance']['ReprintCard']['TurnToCompleted'], 'button', 'goFinish()');
$htmlAry['cancelButton'] = $linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], 'button', 'goCancel()');

?>
    <script type="text/javascript">
        $(document).ready( function() {

        });

        function goFinish() {

            var SmartCardID = document.getElementById('SmartCardID').value;

            if(SmartCardID==""){
                alert("<?=$Lang['StudentAttendance']['ReprintCard']['PleaseInsertSmartCardID']?>");
            }else{
                $('form#form1').submit();
            }
        }

        function goCancel() {
            $('form#form1').attr('action', 'list.php').submit();
        }

    </script>
    <form name="form1" id="form1" enctype="multipart/form-data" method="POST" action="smartcardID_insert_update.php">
        <div class="table_board">
            <?=$htmlAry['contentTable']?>
        </div>
        <br />
        <div class="edit_bottom">
            <p class="spacer"></p>
            <?=$htmlAry['finishButton']?>
            <?=$htmlAry['cancelButton']?>
            <p class="spacer"></p>
        </div>
        <input type="hidden" name="recordID" id="recordID" value="<?=$recordID?>">
        <input type="hidden" name="studentID" id="studentID" value="<?=$studentID?>">
    </form>
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>