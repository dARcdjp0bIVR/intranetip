<?php

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");

### get parameters
$returnMsgKey = standardizeFormGetValue($_GET['returnMsgKey']);
$keyword = standardizeFormPostValue($_POST['keyword']);

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();


$linterface = new interface_html();
$lcardstudentattend2 = new libcardstudentattend2();


### check access right
$canAccess = false;
if ($_SESSION["SSV_PRIVILEGE"]["plugin"]['attendancestudent'] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] && $plugin['eClassApp']) {
    $canAccess = true;
}
if (!$canAccess) {
    No_Access_Right_Pop_Up();
}


### show return message
if($Msg!=""){
    if($Msg==1){
        $returnMsg = $Lang['StudentAttendance']['PresetAbsenceUpdateSuccess'];
    }
    if($Msg==2){
        $returnMsg = $Lang['StudentAttendance']['PresetAbsenceUpdateFail'];
    }
}
else $returnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];


### tab display
// $TAGS_OBJ[] = array($displayLang, $onclickJs, $isSelectedTab);
$TAGS_OBJ[] = array($Lang['StudentAttendance']['ReprintCard']['ReprintCard(App)']);
$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageDailyOperation_ReprintCard";
$MODULE_OBJ = $lcardstudentattend2->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START($returnMsg);
echo $linterface->Include_Thickbox_JS_CSS();


### drop down list filtering

$selectCardStatus = ($selectCardStatus=='')? 0 : $selectCardStatus;		// default "pending"
$htmlAry['acknowledgmentStatusSel'] = getCardStatusSelection('selectcardStatusSel', 'selectCardStatus', $selectCardStatus, "changedCardStatusSelection(this.value);");

### db table
$order = ($order == '') ? 0 : $order;	// 1 => asc, 0 => desc
$field = ($field == '') ? 3 : $field;
$pageNo = ($pageNo == '') ? 1 : $pageNo;
$page_size = ($numPerPage == '') ? 50 : $numPerPage;


$li = new libdbtable2007($field, $order, $pageNo);

$li->field_array = array("StudentNameChi","StudentNameEng", "ClassName", "ClassNumber", "ReprintCardReason", "ShippingMethod", "ApplyDate", "CardStatus");

//$studentNameField = getNameFieldByLang('iu.');
$classNameField = Get_Lang_Selection('yc.ClassTitleB5', 'yc.ClassTitleEN');

$CardStatus ="CASE r.CardStatus
				   WHEN 0 THEN '".$Lang['StudentAttendance']['ReprintCard']['PendingConfirm']."'
				   WHEN 1 THEN '".$Lang['StudentAttendance']['ReprintCard']['PendingPayment']."'
				   WHEN 2 THEN '".$Lang['StudentAttendance']['ReprintCard']['AwaitingCardReprint']."'
				   WHEN 3 THEN '".$Lang['StudentAttendance']['ReprintCard']['Sent']."'
			       WHEN 4 THEN '".$Lang['StudentAttendance']['ReprintCard']['ApplicationCompleted']."'
				   WHEN 5 THEN '".$Lang['StudentAttendance']['ReprintCard']['Rejected']."'
				   WHEN 6 THEN '".$Lang['StudentAttendance']['ReprintCard']['Cancelled']."'
			   END";

$ShippingMethod = "CASE r.DeliveryMethod
                        WHEN 1 THEN '".$Lang['StudentAttendance']['ReprintCard']['PickUp']."'
                        WHEN 2 THEN '".$Lang['StudentAttendance']['ReprintCard']['CourierToApplicant']."'
                   END";

$cond_CardStatus = '';
if ($selectCardStatus == "-1") {
    // all status
} else {
    $cond_CardStatus = " AND r.CardStatus = '".$selectCardStatus."' ";
}

$li->sql = "Select
                        CONCAT('<img src=\"', r.PhotoPath ,'\" border=\"0\" align=\"absmiddle\" width=\"100px\" />'),
                        CONCAT('<a href=\"javascript:void(0);\" class=\"tablelink\" onclick=\"goContent(', r.RecordID, ');\">',iu.ChineseName,'</a>') as StudentNameChi, 
						iu.EnglishName as StudentNameEng,
						$classNameField as ClassName, 
						ycu.ClassNumber, 
						r.ReprintCardReason,
						$ShippingMethod as ShippingMethod,
						r.ApplyDate,
						$CardStatus as CardStatus
				From 
						REPRINT_CARD_RECORD as r
						Inner Join INTRANET_USER as iu ON (r.StudentID = iu.UserID)
						Inner Join YEAR_CLASS_USER as ycu ON (iu.UserID = ycu.UserID)
						Inner Join YEAR_CLASS as yc ON (ycu.YearClassID = yc.YearClassID)
				Where
						yc.AcademicYearID = '".Get_Current_Academic_Year_ID()."'
						$cond_CardStatus
				";
$li->no_col = sizeof($li->field_array)+2;

$li->column_array = array(0,0,0,0,0,0,0,18);
$li->fieldorder2 = ", ClassNumber";
$li->IsColOff = "IP25_table";

$pos = 0;
$li->column_list .= "<th width='1' class='tabletoplink'>#</th>\n";
$li->column_list .= "<th width='5%' >".$li->column($pos++, $Lang['StudentAttendance']['ReprintCard']['StudentPhoto'])."</th>\n";
$li->column_list .= "<th width='10%' >".$li->column($pos++, $Lang['General']['StudentNameChi'])."</th>\n";
$li->column_list .= "<th width='10%' >".$li->column($pos++, $Lang['General']['StudentNameEng'])."</th>\n";
$li->column_list .= "<th width='9%' >".$li->column($pos++, $Lang['General']['Class'])."</th>\n";
$li->column_list .= "<th width='9%' >".$li->column($pos++, $Lang['StudentAttendance']['ReprintCard']['ClassNumber'])."</th>\n";
$li->column_list .= "<th width='15%' >".$li->column($pos++, $Lang['StudentAttendance']['ReprintCard']['ReplacementReason'])."</th>\n";
$li->column_list .= "<th width='10%' >".$li->column($pos++, $Lang['StudentAttendance']['ReprintCard']['DeliveryMethod'])."</th>\n";
$li->column_list .= "<th width='15%' >".$li->column($pos++, $Lang['StudentAttendance']['ReprintCard']['ApplyDate'])."</th>\n";
$li->column_list .= "<th width='20%' >".$li->column($pos++, $Lang['StudentAttendance']['ReprintCard']['Status'])."</th>\n";
$htmlAry['dataTable'] = $li->display();

function getCardStatusSelection($id, $name, $selected, $onchange="") {
    global $Lang;

    $selectArr = array();
    $selectArr['-1'] = $Lang['StudentAttendance']['ReprintCard']['AllStatus'];
    $selectArr['0'] = $Lang['StudentAttendance']['ReprintCard']['PendingConfirm'];
    $selectArr['1'] = $Lang['StudentAttendance']['ReprintCard']['PendingPayment'];
    $selectArr['2'] = $Lang['StudentAttendance']['ReprintCard']['AwaitingCardReprint'];
    $selectArr['3'] = $Lang['StudentAttendance']['ReprintCard']['Sent'];
    $selectArr['4'] = $Lang['StudentAttendance']['ReprintCard']['ApplicationCompleted'];
    $selectArr['5'] = $Lang['StudentAttendance']['ReprintCard']['Rejected'];
    $selectArr['6'] = $Lang['StudentAttendance']['ReprintCard']['Cancelled'];

    $onchangeAttr = '';
    if ($onchange != "")
        $onchangeAttr = 'onchange="'.$onchange.'"';

    $selectionTags = ' id="'.$id.'" name="'.$name.'" '.$onchangeAttr;

    return getSelectByAssoArray($selectArr, $selectionTags, $selected, $all=0, $noFirst=1);

}

?>
    <script type="text/javascript">

        $(document).ready( function() {
        });

        function goContent(recordID) {
            var div = document.getElementById("div1");
            var input = document.createElement("input");
            input.type = "hidden";
            input.name = "recordID";
            input.value = recordID;
            div.appendChild(input);
            $('form#form1').attr('action', 'content_list.php').submit();
        }

        function changedCardStatusSelection() {
            $('form#form1').submit();
        }
    </script>
    <form name="form1" id="form1" method="post" action="list.php">
        <?=$htmlAry['subTab']?>

        <div class="content_top_tool">
            <?=$htmlAry['contentTool']?>
            <?=$htmlAry['searchBox']?>
            <br style="clear:both;">
        </div>

        <div class="table_board">
            <div class="table_filter">
                <?=$htmlAry['applicationTimeTypeSel']?>
                <?=$htmlAry['acknowledgmentStatusSel']?>
                <?=$htmlAry['documentStatusSel']?>
            </div>
            <p class="spacer"></p>

            <?=$htmlAry['dbTableActionBtn']?>
            <?=$htmlAry['dataTable']?>
        </div>
        <div id="div1">

        </div>
        <input type="hidden" id="pageNo" name="pageNo" value="<?=$li->pageNo?>" />
        <input type="hidden" id="order" name="order" value="<?=$li->order?>" />
        <input type="hidden" id="field" name="field" value="<?=$li->field?>" />
        <input type="hidden" id="page_size_change" name="page_size_change" value="" />
        <input type="hidden" id="numPerPage" name="numPerPage" value="<?=$li->page_size?>" />

        <input type="hidden" id="targetDbField" name="targetDbField" value="" />
        <input type="hidden" id="targetStatus" name="targetStatus" value="" />
    </form>
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
