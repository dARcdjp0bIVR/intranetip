<?php
//editing by kenneth chung
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();
/*
if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent'])
{
	header ("Location: /");
	intranet_closedb();
	exit();
}
*/
$lc = new libcardstudentattend2();
$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageDailyOperation_ImportAttendanceData";

$linterface = new interface_html();
$li = new libdb();
$limport = new libimporttext();

$filepath = $userfile;
$filename = $userfile_name;

if($filepath=="none" || $filepath == "")
{
	# import failed
	$Msg = $Lang['StudentAttendance']['OfflineRecordImportFail'];
  header("Location: import.php?Msg=".urlencode($Msg));
}else
{
    if($limport->CHECK_FILE_EXT($filename))
    {
        # read file into array
        # return 0 if fail, return csv array if success
        $data = $limport->GET_IMPORT_TXT($filepath);
        array_shift($data); # drop the title bar
    }else
    {
        header("Location: import.php?msg=5");
    }
		
		$sql = 'Drop table TEMP_LESSON_ATTENDANCE_STUDENT';
		$li->db_db_query($sql);
		
		$sql = "CREATE TABLE TEMP_LESSON_ATTENDANCE_STUDENT (
             UserID int(11),
			 			 ClassName varchar(255),
             ClassNumber varchar(100),
			 			 RecordDate date,
             Period int(11),
             AttendStatus int(11)
            ) ENGINE=InnoDB CHARSET=utf8";
    $li->db_db_query($sql);
		
    # Get Student data
    # ClassName, ClassNumber -> UserID
    $sql = "SELECT UserID, ClassName, ClassNumber 
			FROM INTRANET_USER
            WHERE RecordType = 2 AND RecordStatus IN (0,1)
            	  AND ClassName != '' AND ClassNumber != '' ";
    $temp = $li->returnArray($sql,3);
    for ($i=0; $i<sizeof($temp); $i++)
    {
      list($targetUserID, $targetClass, $targetNum) = $temp[$i];
      $students[$targetClass][$targetNum] = $targetUserID;
    }
    
    $values = "";
    $delim = "";
    $j=0;
    $StatusArray = array('2'=>$Lang['StudentAttendance']['Late'],'3'=>$Lang['StudentAttendance']['Absent']);
    for ($i=0; $i<sizeof($data); $i++)
    {
			list($ClassName,$ClassNumber,$TargetDate,$Period,$AttendStatus) = $data[$i];

		 // Check errors. If has error, skip the current record
		 $error_array = array();// store error messages
		 $HasError = false;
		 if(trim($ClassName)=="")
		 {
		 	$HasError = true;
		 	if($intranet_session_language=="b5")
		 		$error_array[] = "班別名稱不符";
		 	else
		 		$error_array[] = "Invalid Class Name";
		 }
		 if(trim($ClassNumber)=="")
		 {
		 	$HasError = true;
		 	if($intranet_session_language=="b5")
		 		$error_array[] = "沒有班號";
		 	else
		 		$error_array[] = "Doesn't have Class Number";
		 }
		 if(trim($TargetDate)=="" || preg_match('/^\d\d\d\d-\d\d-\d\d$/',$TargetDate)==0)
		 {
		 	$HasError = true;
		 	if($intranet_session_language=="b5")
		 		$error_array[] = "日期格式不符";
		 	else
		 		$error_array[] = "Invalid date";
		 }
		 if(trim($Period)=="" || (!is_numeric($Period)))
		 {
		 	$HasError = true;
		 	if($intranet_session_language=="b5")
		 		$error_array[] = "時段不符";
		 	else
		 		$error_array[] = "Invalid Period";
		 }
		 if(trim($AttendStatus)=="" || (!is_numeric($AttendStatus)))
		 {
		 	$HasError = true;
		 	if($intranet_session_language=="b5")
		 		$error_array[] = "考勤狀況不符";
		 	else
		 		$error_array[] = "Invalid Attendance Status";
		 }
		 
     $DayNumber = intval($datestr[2]);
     $targetUserID = $students[$ClassName][$ClassNumber];
     if ($targetUserID == '')
     {
     	$HasError = true;
     	if($intranet_session_language=="b5")
		 		$error_array[] = "學生不存在";
		 	else
     		$error_array[] = "Student Not Found";
     }
		 
		 $CycleDayInfo = $lc->Get_Attendance_Cycle_Day_Info($TargetDate);
			if ($CycleDayInfo[0] == 'Cycle') {
				$DayNumber = $lc->Convert_Cycle_Day_Value_To_Day_Number($CycleDayInfo[1],$CycleDayInfo[2]);
			}
		 
		 if (!$HasError) {
		 	$sql = 'select distinct
						  stc.ClassTitleEN,
						  stc.ClassTitleB5 
						from
						  INTRANET_CYCLE_GENERATION_PERIOD as icgp
							inner join
							INTRANET_PERIOD_TIMETABLE_RELATION as iptr
							on
							  \''.$TargetDate.'\' between icgp.PeriodStart and icgp.PeriodEnd
								and icgp.PeriodID = iptr.PeriodID
						  inner join
						  INTRANET_TIMETABLE_ROOM_ALLOCATION as itra
						  on
						    itra.TimeTableID = iptr.TimeTableID
						    and itra.Day = IF(icgp.PeriodType=1,\''.$DayNumber.'\', DAYOFWEEK(\''.$TargetDate.'\')-1)
						  inner join 
						  INTRANET_TIMETABLE_TIMESLOT as itt 
						  on 
						  	itra.TimeSlotID = itt.TimeSlotID 
						  	and 
						  	SUBSTR(itt.TimeSlotName,-1) = \''.$Period.'\'
						  inner join
						  SUBJECT_TERM_CLASS as stc
						  on stc.SubjectGroupID = itra.SubjectGroupID
						  inner join 
						  SUBJECT_TERM_CLASS_USER as stcu
						  on 
						  	stcu.SubjectGroupID = stc.SubjectGroupID
						  	and 
						  	stcu.UserID = \''.$students[$ClassName][$ClassNumber].'\' 
							';
			 $PeriodLessonLink = $lc->returnArray($sql);
			 
			 if (sizeof($PeriodLessonLink) == 0) {
			 	$HasError = true;
			 	if($intranet_session_language=="b5")
			 		$error_array[] = "時段不符";
			 	else
			 		$error_array[] = "Invalid Period";
			 }
			}
		 
		 if($HasError) {
		 	$name_field = getNameFieldByLang("u.");
		 	$sql = 'select 
		 						'.$name_field.' as Name 
		 					from 
		 						INTRANET_USER as u 
		 					where 
		 						u.UserId = \''.$targetUserID.'\'';
		 	$StudentName = $li->returnArray($sql);
		 	
		 	$css=($j%2==0)?"tablebluerow1":"tablebluerow2";
			$ErrorTable .= "<tr class=\"$css\">";
			$ErrorTable .= "<td class=\"tabletext\">".(($StudentName[0]['Name'])? $StudentName[0]['Name']:"--")."</td>";
			$ErrorTable .= "<td class=\"tabletext\">$ClassName</td>";
			$ErrorTable .= "<td class=\"tabletext\">$ClassNumber</td>";
			$ErrorTable .= "<td class=\"tabletext\">$TargetDate</td>";
			$ErrorTable .= "<td class=\"tabletext\">$Period</td>";
			$ErrorTable .= "<td class=\"tabletext\">".$StatusArray[$AttendStatus]."</td>";
			$ErrorTable .= "<td class=\"tabletext\"><font style=\"red\">";
			foreach ($error_array as $key => $Msg) {
				if ($ErrorMsg != "") 
					$ErrorMsg .= '/ '.$Msg;
				else 
					$ErrorMsg .= $Msg;
			}
			$ErrorTable .= $ErrorMsg;
			$ErrorTable .= "</font></td>";
			$ErrorTable .= "</tr>";
			
			$ErrorMsg = "";
			$j++;
		 }
		 else {
		 	$sql = 'insert into TEMP_LESSON_ATTENDANCE_STUDENT 
		 						(
		 						UserID,
		 						ClassName,
		 						ClassNumber,
		 						RecordDate,
		 						Period,
		 						AttendStatus
		 						)
		 					values (
		 						\''.$targetUserID.'\',
		 						\''.$ClassName.'\',
		 						\''.$ClassNumber.'\',
		 						\''.$TargetDate.'\',
		 						\''.$Period.'\',
		 						\''.$AttendStatus.'\'
		 						)';
		 	$li->db_db_query($sql);
		 }
    }
}

//Table header
$display = "<table width=\"95%\" border=\"0\" cellspacing=\"0\" cellpadding=\"5\" align=\"center\">";
$display .= "<tr class=\"tablebluetop\">";
$display .= "<td class=\"tabletoplink\" nowrap>".$Lang['StudentAttendance']['StudentName']."</td>";
$display .= "<td class=\"tabletoplink\" nowrap>".$Lang['StudentAttendance']['Class']."</td>";
$display .= "<td class=\"tabletoplink\" nowrap>".$Lang['StudentAttendance']['ClassNumber']."</td>";
$display .= "<td class=\"tabletoplink\" nowrap>".$Lang['General']['Date']."</td>";
$display .= "<td class=\"tabletoplink\" nowrap>".$Lang['SysMgr']['Timetable']['Period']."</td>";
$display .= "<td class=\"tabletoplink\" nowrap>".$Lang['LessonAttendance']['Status']."</td>";
$display .= "<td class=\"tabletoplink\" nowrap>".$Lang['General']['Error']."</td>";
$display .= "</tr>";

$display .= $ErrorTable;
$display .= "</table>";

$TAGS_OBJ[] = array($Lang['LessonAttendance']['ImportAttendenceData'], "", 0);

$MODULE_OBJ = $lc->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

$PAGE_NAVIGATION[] = array($subHeader);

?>
<br />
<form name="form1" method="POST" action="import_update_confirm.php">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	</tr>
	<tr>
	<td>
		Start Date: <?= $linterface->Get_Date_Picker('StartDate',date('Y-m-d'))?> 
		End Date: <?= $linterface->Get_Date_Picker('EndDate',date('Y-m-d'))?> 
	</td>
	</tr>
	<tr>
		<td>
			<div style="color:red;">
			<?=$Lang['LessonAttendance']['ImportAttendanceDataWarningMsg']?>
			</div>
			</td>
	</tr>
	<tr>
		<td>
			<?=$display?>
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>
			<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
			<tr>
				<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
			</tr>
			</table>	
		</td>
	</tr>
	<tr>
		<td>
			<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
		    	<tr>
				    <td align="center" colspan="2">
						<?= $linterface->GET_ACTION_BTN($button_import, "submit", "",""," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
						<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:history.back()","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</form>
<br />
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>