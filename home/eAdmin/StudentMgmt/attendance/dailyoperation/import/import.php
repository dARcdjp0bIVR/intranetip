<?php
// using 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();
$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageDailyOperation_ImportOfflineRecords";

$linterface = new interface_html();

$lc->retrieveSettings();

$TAGS_OBJ[] = array($i_SmartCard_DailyOperation_ImportOfflineRecords, "", 1);
if($sys_custom['StudentAttendance']['ImportCardlog']){
	$TAGS_OBJ[] = array($Lang['StudentAttendance']['ImportTapCardRecords'], "import_cardlog.php", 0);
}

$MODULE_OBJ = $lc->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START(urldecode($Msg));

if ($msg == 1) $SysMsg = $linterface->GET_SYS_MSG("add");
if ($msg == 2) $SysMsg = $linterface->GET_SYS_MSG("update");
if ($msg == 3) $SysMsg = $linterface->GET_SYS_MSG("add_failed");
if ($msg == 4) $SysMsg = $linterface->GET_SYS_MSG("import_failed2");
if ($msg == 5) $SysMsg = $linterface->GET_SYS_MSG("import_failed");
?>
<script language="Javascript">
function checkform(obj)
{
         return true;
}
</script>
<br />
<form name="form1" action="import_update.php" method="post" enctype="multipart/form-data" onSubmit="return checkform(this)">
<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
	<tr>
		<td colspan="2" align="right"><?= $SysMsg ?></td>
	</tr>	
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
			<?=$i_select_file?>
		</td>
		<td width="70%"class="tabletext">
			<input type="file" class="file" name="userfile"><br />
			<?=$linterface->GET_IMPORT_CODING_CHKBOX()?>
		</td>
	</tr>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
			<?=$i_general_status?>
		</td>
		<td width="70%"class="tabletext">
			<?
			if ($lc->attendance_mode == 2)      # Full Lunch Control Mode
			{
			?>
			<input type="radio" name="datatype" id="datatype1" value="1" CHECKED>
			<label for="datatype1"><?=$i_StudentAttendance_Offline_Import_DataType_InSchool?></label>
			<input type="radio" name="datatype" id="datatype2" value="2" >
			<label for="datatype2"><?=$i_StudentAttendance_Offline_Import_DataType_LunchOut?></label>
			<input type="radio" name="datatype" id="datatype3" value="3" >
			<label for="datatype3"><?=$i_StudentAttendance_Offline_Import_DataType_LunchIn?></label>
			<input type="radio" name="datatype" id="datatype4" value="4" >
			<label for="datatype4"><?=$i_StudentAttendance_Offline_Import_DataType_AfterSchool?></label>
			<?
			}
			else     # AM or PM or WD w/o Lunch
			{
			?>
			<input type="radio" name="datatype" id="datatype1" value="1" CHECKED>
			<label for="datatype1"><?=$i_StudentAttendance_Offline_Import_DataType_InSchool?></label>
			<input type="radio" name="datatype" id="datatype4" value="4" >
			<label for="datatype4"><?=$i_StudentAttendance_Offline_Import_DataType_AfterSchool?></label>
			<?
			}
			?>
		</td>
	</tr>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
			<?=$i_general_Format?>
		</td>
		<td width="70%"class="tabletext">
			<input type="radio" name="format" id="format1" value="1" CHECKED>
			<label for="format1"><?=$i_StudentAttendance_ImportFormat_CardID?></label> 
			<a class="tablelink" href="<?=GET_CSV("format1.csv")?>" target="_blank">[<?=$i_general_clickheredownloadsample?>]</a><br />
			<input type="radio" name="format" id="format2" value="2">
			<label for="format2"><?=$i_StudentAttendance_ImportFormat_ClassNumber?></label> 
			<a class="tablelink" href="<?=GET_CSV("format2.csv")?>" target="_blank">[<?=$i_general_clickheredownloadsample?>]</a><br />
			<input type="radio" name="format" id="format3" value="3">
			<label for="format3"><?=$i_StudentAttendance_ImportFormat_From_OfflineReader." (950e)"?></label><br />
			<input type="radio" name="format" id="format4" value="4">
			<label for="format4"><?=$Lang['StudentAttendance']['ImportOldTerminalOfflineRecord']?></label><br />
			<br /><span class="tabletextremark"><?=$i_StudentAttendance_ImportTimeFormat?></span>
			<br /><span class="tabletextremark"><?=$i_StudentAttendance_Import_Instruction_OneDayOnly?></span>
		</td>
	</tr>
</table>
<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
    	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
    </tr>
</table>
<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
	    <td align="center" colspan="2">
			<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "") ?>
			<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
		</td>
	</tr>
</table>
</form>
<br />
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>