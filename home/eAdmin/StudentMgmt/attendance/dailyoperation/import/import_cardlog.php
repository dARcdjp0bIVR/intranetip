<?php
// using 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent'] || !$sys_custom['StudentAttendance']['ImportCardlog']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();
$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageDailyOperation_ImportOfflineRecords";

$linterface = new interface_html();

$TAGS_OBJ[] = array($i_SmartCard_DailyOperation_ImportOfflineRecords, "import.php", 0);
if($sys_custom['StudentAttendance']['ImportCardlog']){
	$TAGS_OBJ[] = array($Lang['StudentAttendance']['ImportTapCardRecords'], "import_cardlog.php", 1);
}

$MODULE_OBJ = $lc->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START(urldecode($Msg));

if ($msg == 1) $SysMsg = $linterface->GET_SYS_MSG("add");
if ($msg == 2) $SysMsg = $linterface->GET_SYS_MSG("update");
if ($msg == 3) $SysMsg = $linterface->GET_SYS_MSG("add_failed");
if ($msg == 4) $SysMsg = $linterface->GET_SYS_MSG("import_failed2");
if ($msg == 5) $SysMsg = $linterface->GET_SYS_MSG("import_failed");

?>
<script language="Javascript">
function checkform(formObj)
{
	if(document.getElementById('userfile').value == ""){
		alert("Please select file.");
		return false;
	}
	return true;
}
</script>
<br />
<form name="form1" action="import_cardlog_confirm.php" method="post" enctype="multipart/form-data" onSubmit="return checkform(this);">
<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
	<tr>
		<td colspan="2" align="right"><?= $SysMsg ?></td>
	</tr>	
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
			<?=$i_select_file?>
		</td>
		<td width="70%"class="tabletext">
			<input type="file" class="file" id="userfile" name="userfile"><br />
			<?=$linterface->GET_IMPORT_CODING_CHKBOX()?>
		</td>
	</tr>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
			<?=$i_general_Format?>
		</td>
		<td width="70%"class="tabletext">
			<span class="tabletextremark"><?=$i_StudentAttendance_ImportFormat_CardID?></span> 
		</td>
	</tr>
</table>
<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
    	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
    </tr>
</table>
<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
	    <td align="center" colspan="2">
			<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "") ?>
			<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
		</td>
	</tr>
</table>
</form>
<br />
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>