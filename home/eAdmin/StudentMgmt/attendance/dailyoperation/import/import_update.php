<?php
// Editing by 
/************************************************** Changes **************************************************
 * 2015-12-03 (Carlos): Do datetime field format conversion with the global function convertDateTimeToStandardFormat(). (Need to update this page with lib.php)
 * 2013-10-15 (Carlos): Added warning checking
 * 2012-02-17 (Carlos): TRIM() ClassName, ClassNumber and CardID to compare with != '', because fail to compare with 0
 * 2012-02-13 (Carlos): added no valid record warning msg if records can be imported is 0 
 * 2011-11-03 (Carlos): force format CardID to 10 digits
 *************************************************************************************************************/
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
if ($format == 4)
	include_once($PATH_WRT_ROOT."includes/libimporttext-kiosk.php");
else
	include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();
$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageDailyOperation_ImportOfflineRecords";

$linterface = new interface_html();

$li = new libdb();

$limport = new libimporttext();

$filepath = $userfile;
$filename = $userfile_name;

$warning_count = 0;

if($filepath=="none" || $filepath == ""){          # import failed
	$Msg = $Lang['StudentAttendance']['OfflineRecordImportFail'];
  header("Location: import.php?Msg=".urlencode($Msg));
} else {
    if($limport->CHECK_FILE_EXT($filename)) {
        # read file into array
        # return 0 if fail, return csv array if success
        $data = $limport->GET_IMPORT_TXT($filepath);
        //debug_r($data);
        if ($format != 3) {
            array_shift($data);                # drop the title bar
        }
    } else {
        header("Location: import.php?msg=5");
    }
    
    $sql = "DROP TABLE TEMP_CARD_STUDENT_LOG";
    $li->db_db_query($sql);
    $sql = "CREATE TABLE TEMP_CARD_STUDENT_LOG (
             RecordDate date,
             RecordedTime datetime,
             UserID int,
             CardID varchar(255),
             ClassName varchar(255),
             ClassNumber varchar(100),
             SiteName varchar(255)
            ) ENGINE=InnoDB CHARSET=utf8";
    $li->db_db_query($sql);

    # Get Student and Time table data
    if ($format == 2)
    {
        # ClassName, ClassNumber -> UserID
        $sql = "SELECT UserID, ClassName, ClassNumber FROM INTRANET_USER
                   WHERE RecordType = 2 AND RecordStatus IN (0,1)
                        AND TRIM(ClassName) != '' AND TRIM(ClassNumber) != ''";
        $temp = $li->returnArray($sql,3);
        for ($i=0; $i<sizeof($temp); $i++)
        {
             list($targetUserID, $targetClass, $targetNum) = $temp[$i];
             $students[$targetClass][$targetNum] = $targetUserID;
        }
    }
    else
    {
        # CardID -> ClassName
        # CardID -> UserID
        $sql = "SELECT CardID, UserID, ClassName FROM INTRANET_USER
                       WHERE RecordType = 2 AND RecordStatus IN (0,1)
                             AND TRIM(ClassName) != '' 
                             AND TRIM(CardID) != ''";
        $temp = $li->returnArray($sql,3);
        for ($i=0; $i<sizeof($temp); $i++)
        {
             list($targetCardID, $targetUserID, $targetClass) = $temp[$i];
             $targetCardID = FormatSmartCardID($targetCardID);
             $students[$targetCardID] = $targetUserID;
             $classes[$targetCardID] = $targetClass;
        }
    }

    $values = "";
    $delim = "";
    for ($i=0; $i<sizeof($data); $i++)
    {
         if ($format == 2)
         {
             list($time,$site,$class,$classnum) = $data[$i];
             $time = convertDateTimeToStandardFormat($time);
             $ts = strtotime($time);
			 if($ts == false) {
				 continue;
			 }
             $date = date('Y-m-d',$ts);
             $targetUserID = $students[$class][$classnum];
             if ($targetUserID != '')
             {
                 $values .= "$delim('$date','$time','$targetUserID','$site','$class','$classnum')";
                 $delim = ",";
             }
         }
         else if ($format == 3) # Reader 950e
         {
              // for offline reader format
              // format:   time,site,student id
              // e.g:      2004-11-03 11:26:55,Office,21013
              if (is_array($data[$i]))
              {
                  list($time,$site,$targetUserID) = $data[$i];
                  $time = convertDateTimeToStandardFormat($time);
                  if ($time!="" && $targetUserID != "")
                  {
                      $ts = strtotime($time);
					  if($ts == false) {
						  continue;
					  }
                      $date = date('Y-m-d',$ts);
                      $values .= "$delim('$date','$time','$site','$targetUserID')";
                      $delim = ",";
                  }
              }
         }
         else
         {
             list($time,$site,$cardid) = $data[$i];
			 $cardid = FormatSmartCardID($cardid);
			 $time = convertDateTimeToStandardFormat($time);
             $ts = strtotime($time);
			 if($ts == false) {
				 continue;
			 }
             $date = date('Y-m-d',$ts);
             $targetUserID = $students[$cardid];
             if ($targetUserID != '')
             {
                 $values .= "$delim('$date','$time','$site','$cardid','$targetUserID')";
                 $delim = ",";
             }
         }
    }
	
	
	
    if ($format==2)
    {
        $sql = "INSERT INTO TEMP_CARD_STUDENT_LOG (RecordDate, RecordedTime,UserID,SiteName,ClassName,ClassNumber) VALUES $values";
    }
    else if ($format==3)
    {
    	$sql = "INSERT INTO TEMP_CARD_STUDENT_LOG (RecordDate,RecordedTime,SiteName,UserID) VALUES $values";
    }
    else
    {
        $sql = "INSERT INTO TEMP_CARD_STUDENT_LOG (RecordDate,RecordedTime,SiteName,CardID, UserID) VALUES $values";
    }
    //debug_r($sql);
    $li->db_db_query($sql);
}

$name_field = getNameFieldByLang("b.");
$sql = "SELECT a.RecordDate, DATE_FORMAT(a.RecordedTime,'%H:%i:%s'), a.SiteName, IF(a.CardID IS NULL, b.CardID, a.CardID), $name_field, b.ClassName, b.ClassNumber, a.UserID 
        FROM TEMP_CARD_STUDENT_LOG as a
             LEFT OUTER JOIN INTRANET_USER as b ON b.RecordType = 2 AND
                  a.UserID = b.UserID
                  ";
$result = $li->returnArray($sql,8);

$display = "<table width=\"95%\" border=\"0\" cellspacing=\"0\" cellpadding=\"5\" align=\"center\">";
$display .= "<tr class=\"tablebluetop\">";
$display .= "<td class=\"tabletoplink\">$i_SmartCard_DateRecorded</td>";
$display .= "<td class=\"tabletoplink\">$i_SmartCard_TimeRecorded</td>";
$display .= "<td class=\"tabletoplink\">$i_SmartCard_Site</td>";
$display .= "<td class=\"tabletoplink\">$i_SmartCard_CardID</td>";
$display .= "<td class=\"tabletoplink\">$i_UserStudentName</td>";
$display .= "<td class=\"tabletoplink\">$i_UserClassName</td>";
$display .= "<td class=\"tabletoplink\">$i_UserClassNumber</td>";
$display .= "<td class=\"tabletoplink\">".$Lang['General']['Warning']."</td>";
$display .= "</tr>";

for ($i=0; $i<sizeof($result); $i++)
{
     list ($date,$time,$site,$cardid,$name,$class,$classnum,$user_id) = $result[$i];
     
     // Do possible error checking
     $warning_ary = array();
     $time_setting = $lc->Get_Student_Attend_Time_Setting($date,$user_id);
     
     if($time_setting == "NonSchoolDay"){
     	$warning_ary[] = $Lang['StudentAttendance']['OfflineImportWarning']['NonSchoolDay'];
     }else{
     	$morning_time =  $time_setting['MorningTime'];
     	$lunch_start = $time_setting['LunchStart'];
     	$lunch_end = $time_setting['LunchEnd'];
     	$leave_school_time = $time_setting['LeaveSchoolTime'];
     	
     	if($datatype == "1"){ // Go to School
     		if($time > $leave_school_time){
     			$warning_ary[] = str_replace("<!--LEAVE_TIME-->",$leave_school_time,$Lang['StudentAttendance']['OfflineImportWarning']['InTimeLaterThanLeaveTime']);
     			$warning_count++;
     		}
     	}else if($datatype == "2" || $datatype == "4"){ // 2:Out for Lunch; 3:Back from Lunch; 4:Leave School
     		if($lc->attendance_mode == 1){ // PM only
     			if($time < $lunch_end) {
	     			$warning_ary[] = str_replace("<!--START_TIME-->",$morning_time,$Lang['StudentAttendance']['OfflineImportWarning']['TimeEarlierThanSchoolStartTime']);
	     			$warning_count++;
	     		}
     		}else{
	     		if($time < $morning_time) {
	     			$warning_ary[] = str_replace("<!--START_TIME-->",$morning_time,$Lang['StudentAttendance']['OfflineImportWarning']['TimeEarlierThanSchoolStartTime']);
	     			$warning_count++;
	     		}
     		}
     	}
     }
     
     $warning_span = "&nbsp;";
     if(sizeof($warning_ary)>0) {
     	$warning_span = '<div style="color:red">'.implode('<br>',$warning_ary).'</div>';
     }
     // end of error checking
     
     $css=($i%2==0)?"tablebluerow1":"tablebluerow2";
     $display .= "<tr class=\"$css\">";
     $display .= "<td class=\"tabletext\">$date</td>";
     $display .= "<td class=\"tabletext\">$time</td>";
     $display .= "<td class=\"tabletext\">$site</td>";
     $display .= "<td class=\"tabletext\">$cardid</td>";
     $display .= "<td class=\"tabletext\">$name</td>";
     $display .= "<td class=\"tabletext\">$class</td>";
     $display .= "<td class=\"tabletext\">$classnum</td>";
     $display .= "<td class=\"tabletext\">$warning_span</td>";
	 $display .= "</tr>";
}
$display .= "</table>";

$sql = "SELECT DISTINCT RecordDate FROM TEMP_CARD_STUDENT_LOG";
$temp = $li->returnVector($sql);

$numberOfDays = sizeof($temp);
$oneDayOnly = (sizeof($temp)==1);

$subHeader = $button_import;
if ($datatype == "1") {
	$subHeader .= " ($i_StudentAttendance_Offline_Import_DataType_InSchool)";
} else if ($datatype == "2") {
	$subHeader .= " ($i_StudentAttendance_Offline_Import_DataType_LunchOut)";
} else if ($datatype == "3") {
	$subHeader .= " ($i_StudentAttendance_Offline_Import_DataType_LunchIn)";
} else if ($datatype == "4") {
	$subHeader .= " ($i_StudentAttendance_Offline_Import_DataType_AfterSchool)";
} 

$TAGS_OBJ[] = array($i_SmartCard_DailyOperation_ImportOfflineRecords, "", 1);
if($sys_custom['StudentAttendance']['ImportCardlog']){
	$TAGS_OBJ[] = array($Lang['StudentAttendance']['ImportTapCardRecords'], "import_cardlog.php", 0);
}

$MODULE_OBJ = $lc->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

$PAGE_NAVIGATION[] = array($subHeader);

?>
<script type="text/javascript" language="JavaScript">
function checkSubmit(formObj)
{
	if(<?=$warning_count?> > 0) {
		if(confirm('<?=$Lang['StudentAttendance']['OfflineImportWarning']['IgnoreWarningContinue']?>')){
			return true;
		}else{
			return false;
		}
	}
	return true;
}
</script>
<br />
<form name="form1" method="POST" action="import_update_confirm.php" onsubmit="return checkSubmit(this);">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>
			<?=$display?>
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>
			<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
		    	<tr>
		    		<td class="tabletextremark">
		    		<? 
		    		if ($numberOfDays > 1){ // error: more than one day records
		    			echo $i_StudentAttendance_Import_Warning_OneDayOnly;
		    		}else if ($numberOfDays == 0){ // error: no valid records
		    			echo $Lang['StudentAttendance']['Warning']['NoValidRecordsInImportFile'];
		    		}else { // exactly one day records
		    			echo $i_StudentAttendance_ImportConfirm.'<br />'.$i_StudentAttendance_ImportCancel;
		    		}
		    		?>
		    		</td>
		    	</tr>
		    </table>
		</td>
	</tr>
	<tr>
		<td>
			<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
			<tr>
				<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
			</tr>
			</table>	
		</td>
	</tr>
	<tr>
		<td>
			<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
		    	<tr>
				    <td align="center" colspan="2">
				    <? if ($oneDayOnly) { ?>
						<?= $linterface->GET_ACTION_BTN($button_import, "submit", "") ?>
					<? } ?>
						<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:history.back()","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<input type=hidden name=RecordDate value="<?=$temp[0]?>">
<input type="hidden" name="datatype" value="<?=$datatype?>">
</form>
<br />
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>