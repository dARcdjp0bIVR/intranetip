<?php
## Using : 
/*
 * 2019-10-25 (Carlos): Remove fake tap card and no card entrance records for AM/PM in. 
 * 2013-12-02 (Carlos): For normal leave condition, if $day_type is PM, also remove AM early leave record.
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$li = new libcardstudentattend2();
$li->retrieveSettings();

if ($plugin['Discipline']){
	include_once($PATH_WRT_ROOT."includes/libdiscipline.php");
    $ldiscipline = new libdiscipline();
}

if ($plugin['Disciplinev12']){
	include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
    $ldisciplinev12 = new libdisciplinev12();
    
    $LateCtgInUsed = $ldisciplinev12->CategoryInUsed(PRESET_CATEGORY_LATE);
  
  	## Only For IP25 eDis1.2 Only ##
  	if($sys_custom['Discipline_SeriousLate']){	
	    $sql = "SELECT CategoryID FROM DISCIPLINE_ACCU_CATEGORY WHERE MeritType = -1 AND LateMinutes IS NOT NULL";
	    $targetCatID = $ldisciplinev12->returnVector($sql);
	    if(sizeof($targetCatID)>0){
	    	for($i=0; $i<sizeof($targetCatID); $i++){
	    		$result = $ldisciplinev12->CategoryInUsed($targetCatID[$i]);
	    		
	    		if($result){
	    			$cnt++;
	    		}
	    	}
	    }
	    if($cnt > 0){
	    	$SeriousLateUsed = 1;
	    }
	}
}

if ($datatype==1)
{
    if ($li->attendance_mode==1)
    {
        $day_type = PROFILE_DAY_TYPE_PM;
        $db_field_status = "PMStatus";
    }
    else
    {
        $day_type = PROFILE_DAY_TYPE_AM;
        $db_field_status = "AMStatus";
    }
    $bad_record_card_status = CARD_STATUS_LATE;
    $profile_type = PROFILE_TYPE_LATE;
}
else if ($datatype==2)
{
     $day_type = PROFILE_DAY_TYPE_AM;
     $db_field_status = "LeaveStatus";
     $bad_record_card_status = CARD_LEAVE_AM;
     $profile_type = PROFILE_TYPE_EARLY;
}
else if ($datatype==3)
{
     $day_type = PROFILE_DAY_TYPE_PM;
     $db_field_status = "PMStatus";
     $bad_record_card_status = CARD_STATUS_LATE;
     $profile_type = PROFILE_TYPE_LATE;
}
else if ($datatype==4)
{
    if ($li->attendance_mode==0)
    {
        $day_type = PROFILE_DAY_TYPE_AM;
        $bad_record_card_status = CARD_LEAVE_AM;
    }
    else
    {
        $day_type = PROFILE_DAY_TYPE_PM;
        $bad_record_card_status = CARD_LEAVE_PM;
    }
    $db_field_status = "LeaveStatus";
    $profile_type = PROFILE_TYPE_EARLY;
}
else
{
    header("Location: index.php?msg=3");
    exit();
}


/*$txt_year = getCurrentAcademicYear();
$txt_semester = getCurrentSemester();*/

list($school_year_id, $school_year, $semester_id, $semester) = getAcademicYearInfoAndTermInfoByDate($RecordDate);

$ts = strtotime($RecordDate);
$year = date('Y',$ts);
$month = date('m',$ts);
$day = date('d',$ts);

$li->createTable_Card_Student_Daily_Log($year, $month);

/*
# Get Class List
$sql = "SELECT DISTINCT c.YearClassID
				FROM 
					TEMP_CARD_STUDENT_LOG as a
			    LEFT OUTER JOIN 
			    INTRANET_USER as b 
			    ON a.UserID = b.UserID
			    LEFT OUTER JOIN 
			    YEAR_CLASS as c 
			    ON b.ClassName = c.ClassTitleEN 
			    LEFT OUTER JOIN 
			    CARD_STUDENT_CLASS_SPECIFIC_MODE as d 
			    ON c.YearClassID = d.ClassID
				WHERE 
					c.AcademicYearID = '".$school_year_id."' 
					and 
					d.Mode = 1";
// debug_r($sql);
$classes = $li->returnVector($sql);

# Cycle and Weekday
$sql = "SELECT TextShort FROM INTRANET_CYCLE_DAYS WHERE RecordDate = '$RecordDate'";
// debug_r($sql);
$array_cycles = $li->returnVector($sql);
$cycle_day = $array_cycles[0];
$weekday = date('w',$ts);


$today = date('Y-m-d');
# Get Class-specific timetable
if (sizeof($classes)!= 0)
{
    $class_list = implode(",",$classes);

    # Get Class-Special Day Specific
    $sql = "SELECT ClassID, TIME_TO_SEC(MorningTime), TIME_TO_SEC(LunchStart),
                   TIME_TO_SEC(LunchEnd), TIME_TO_SEC(LeaveSchoolTime),
                   NonSchoolDay
                   FROM CARD_STUDENT_SPECIFIC_DATE_TIME
                   WHERE RecordDate = '$RecordDate'
                         AND ClassID IN ($class_list)
                   ";
		// debug_r($sql);
    $temp = $li->returnArray($sql,6);
    for ($i=0; $i<sizeof($temp); $i++)
    {
         list($class_id, $time1, $time2, $time3, $time4, $ts_nonSchoolDay) = $temp[$i];
         $classes_day_time[$class_id] = array($time1, $time2, $time3, $time4, $ts_nonSchoolDay);
    }


    # Get Class-Cycle Day specific
    if ($cycle_day!="")
    {
        $sql = "SELECT ClassID, TIME_TO_SEC(MorningTime), TIME_TO_SEC(LunchStart), TIME_TO_SEC(LunchEnd), TIME_TO_SEC(LeaveSchoolTime),
                       NonSchoolDay
                       FROM CARD_STUDENT_CLASS_PERIOD_TIME WHERE ClassID IN ($class_list)
                        AND DayType = 2 AND DayValue = '$cycle_day'";
        // debug_r($sql);
        $array_classes_cycle_time = $li->returnArray($sql,6);
        for ($i=0; $i<sizeof($array_classes_cycle_time); $i++)
        {
             list($class_id, $time1, $time2, $time3, $time4,$ts_nonSchoolDay) = $array_classes_cycle_time[$i];
             $classes_cycle_time[$class_id] = array($time1, $time2, $time3, $time4,$ts_nonSchoolDay);
        }
    }
    # Get Class-WeekDay specific
    $sql = "SELECT ClassID, TIME_TO_SEC(MorningTime), TIME_TO_SEC(LunchStart), TIME_TO_SEC(LunchEnd), TIME_TO_SEC(LeaveSchoolTime),
                   NonSchoolDay
                   FROM CARD_STUDENT_CLASS_PERIOD_TIME WHERE ClassID IN ($class_list)
                   AND DayType = 1 AND DayValue = '$weekday'";
    // debug_r($sql);
    $array_classes_week_time = $li->returnArray($sql,6);
    for ($i=0; $i<sizeof($array_classes_week_time); $i++)
    {
         list($class_id, $time1, $time2, $time3, $time4, $ts_nonSchoolDay) = $array_classes_week_time[$i];
         $classes_week_time[$class_id] = array($time1, $time2, $time3, $time4, $ts_nonSchoolDay);
    }

    $sql = "SELECT ClassID, TIME_TO_SEC(MorningTime), TIME_TO_SEC(LunchStart), TIME_TO_SEC(LunchEnd), TIME_TO_SEC(LeaveSchoolTime),
                   NonSchoolDay
                   FROM CARD_STUDENT_CLASS_PERIOD_TIME WHERE ClassID IN ($class_list)
                   AND DayType = 0";
    // debug_r($sql);
    $array_classes_normal_time = $li->returnArray($sql,6);
    for ($i=0; $i<sizeof($array_classes_normal_time); $i++)
    {
         list($class_id, $time1, $time2, $time3, $time4, $ts_nonSchoolDay) = $array_classes_normal_time[$i];
         $classes_normal_time[$class_id] = array($time1, $time2, $time3, $time4, $ts_nonSchoolDay);
    }
}

# Get Special Day based on School
$sql = "SELECT RecordID, TIME_TO_SEC(MorningTime), TIME_TO_SEC(LunchStart),
               TIME_TO_SEC(LunchEnd), TIME_TO_SEC(LeaveSchoolTime),
               NonSchoolDay
        FROM CARD_STUDENT_SPECIFIC_DATE_TIME
        WHERE RecordDate = '$RecordDate'
              AND ClassID = 0
              ";
// debug_r($sql);
$temp = $li->returnArray($sql,6);
list($ts_recordID, $ts_morningTime, $ts_lunchStart, $ts_lunchEnd, $ts_leaveSchool, $ts_nonSchoolDay) = $temp[0];
if ($ts_recordID > 0)
{
    $school_time = array($ts_morningTime, $ts_lunchStart, $ts_lunchEnd, $ts_leaveSchool, $ts_nonSchoolDay);
    #echo "School day <Br>";
}
else
{
    # Get Normal school timetable
    $sql = "SELECT TIME_TO_SEC(MorningTime), TIME_TO_SEC(LunchStart), TIME_TO_SEC(LunchEnd), TIME_TO_SEC(LeaveSchoolTime),
                   NonSchoolDay
                   FROM CARD_STUDENT_PERIOD_TIME
                    WHERE (DayType = 2 AND DayValue = '$cycle_day')
                           OR (DayType = 1 AND DayValue = '$weekday')
                           OR DayType = 0
                    ORDER BY DayType DESC";
    // debug_r($sql);
    $temp = $li->returnArray($sql,5);
    $school_time = $temp[0];
    #echo "School setting";
    #echo "$sql";
    #print_r($school_time);
}


# Get Current Records
$sql = "SELECT DISTINCT UserID FROM TEMP_CARD_STUDENT_LOG";
// debug_r($sql);
$users = $li->returnVector($sql);
$user_list = implode(",",$users);

$null_record_variable = "NULL";

$table_name = "CARD_STUDENT_DAILY_LOG_".$year."_".$month;
$sql = "SELECT 
					a.RecordID, 
					a.UserID, 
					TIME_TO_SEC(a.InSchoolTime),
					IF(a.AMStatus IS NULL,'$null_record_variable',a.AMStatus), 
					TIME_TO_SEC(a.LunchOutTime), 
					TIME_TO_SEC(a.LunchBackTime),
					IF(a.PMStatus IS NULL,'$null_record_variable',a.PMStatus), 
					TIME_TO_SEC(a.LeaveSchoolTime), 
					a.LeaveStatus, 
					c.YearClassID, 
					b.ClassName, 
					b.ClassNumber
        FROM 
        	$table_name as a
					LEFT OUTER JOIN INTRANET_USER as b ON a.UserID = b.UserID
					LEFT OUTER JOIN YEAR_CLASS as c ON b.ClassName = c.ClassTitleEN 
        WHERE a.DayNumber = '$day' AND a.UserID IN ($user_list)";
// debug_r($sql);
$temp = $li->returnArray($sql,12);

for ($i=0; $i<sizeof($temp); $i++)
{
     list($record_id, $uid, $ts_inTime, $am, $ts_lunch_out, $ts_lunch_back, $pm, $ts_leave_time, $leave, $classID, $class_name, $class_number) = $temp[$i];
     $current_records[$uid] = array($record_id, $ts_inTime, $am, $ts_lunch_out, $ts_lunch_back, $pm, $ts_leave_time, $leave, $classID, $class_name, $class_number);
}

$sql = "SELECT a.UserID, b.YearClassID FROM INTRANET_USER as a
               LEFT OUTER JOIN YEAR_CLASS as b ON a.ClassName = b.ClassTitleEN 
               WHERE a.UserID IN ($user_list)";
// debug_r($sql);
$temp = $li->returnArray($sql,2);

$assoc_classes = build_assoc_array($temp);
*/

# Get Current Records
$sql = "SELECT DISTINCT UserID FROM TEMP_CARD_STUDENT_LOG";
// debug_r($sql);
$users = $li->returnVector($sql);
$user_list = implode(",",$users);

$null_record_variable = "NULL";

$table_name = "CARD_STUDENT_DAILY_LOG_".$year."_".$month;
$sql = "SELECT 
					a.RecordID, 
					a.UserID, 
					TIME_TO_SEC(a.InSchoolTime),
					IF(a.AMStatus IS NULL,'$null_record_variable',a.AMStatus), 
					TIME_TO_SEC(a.LunchOutTime), 
					TIME_TO_SEC(a.LunchBackTime),
					IF(a.PMStatus IS NULL,'$null_record_variable',a.PMStatus), 
					TIME_TO_SEC(a.LeaveSchoolTime), 
					a.LeaveStatus, 
					c.YearClassID, 
					b.ClassName, 
					b.ClassNumber
        FROM 
        	$table_name as a
					LEFT OUTER JOIN INTRANET_USER as b ON a.UserID = b.UserID
					LEFT OUTER JOIN YEAR_CLASS as c ON b.ClassName = c.ClassTitleEN 
        WHERE a.DayNumber = '$day' AND a.UserID IN ($user_list)";
// debug_r($sql);
$temp = $li->returnArray($sql,12);

for ($i=0; $i<sizeof($temp); $i++)
{
	list($record_id, $uid, $ts_inTime, $am, $ts_lunch_out, $ts_lunch_back, $pm, $ts_leave_time, $leave, $classID, $class_name, $class_number) = $temp[$i];
	$current_records[$uid] = array($record_id, $ts_inTime, $am, $ts_lunch_out, $ts_lunch_back, $pm, $ts_leave_time, $leave, $classID, $class_name, $class_number);
}

# Daily Records: $current_records
# Class Timetable: $classes_cycle_time, $classes_week_time, $classes_normal_time
# Timetable: $school_time
$no_bad_id_list = "";
$no_bad_delim = "";

# Process import records
$sql = "SELECT 
					UserID, 
					RecordDate, 
					TIME_TO_SEC(TIME_FORMAT(RecordedTime,'%H:%i:%s')), 
					SiteName, 
					TIME_FORMAT(RecordedTime,'%H:%i:%s')
				FROM 
					TEMP_CARD_STUDENT_LOG";
// debug_r($sql);
$import_records = $li->returnArray($sql,5);
for ($i=0; $i<sizeof($import_records); $i++)
{
     list ($uid, $record_date, $ts_record_time, $record_site, $ImportTimeText) = $import_records[$i];
     list ($record_id, $ts_inTime, $am, $ts_lunch_out, $ts_lunch_back, $pm, $ts_leave_time, $leave, $classID, $class_name, $class_number) = $current_records[$uid];

     /*
     if ($classID == "")
     {
         $classID = $assoc_classes[$uid];
     }*/

     #echo "UserID : $uid <br>\n";
     #echo "ClassID : $classID <br>\n";
     # Check In School time
     /*$timetable = $classes_day_time[$classID];
     #echo "Class day<br>\n";
     if (!is_array($timetable) || sizeof($timetable)==0 || $timetable[0]=="")
     {
          #echo "Class cycle<br>\n";
          $timetable = $classes_cycle_time[$classID];
          if (!is_array($timetable) || sizeof($timetable)==0 || $timetable[0]=="")
          {
               #echo "Class week<br>\n";
               $timetable = $classes_week_time[$classID];
               if (!is_array($timetable) || sizeof($timetable)==0 || $timetable[0]=="")
               {
                    #echo "Class normal<br>\n";
                    $timetable = $classes_normal_time[$classID];
                    if (!is_array($timetable) || sizeof($timetable)==0 || $timetable[0]=="")
                    {
                         #echo "School timetable<br>\n";
                         $timetable = $school_time;
                    }
                    else
                    
                    {
                        #echo "Class timetable<br>\n";
                    }
               }
          }
     }*/
     # Check need to take attendance or not
		$FinalSetting = $li->Get_Student_Attend_Time_Setting($record_date,$uid);
		
		$bound_inschool = $li->timeToSec($FinalSetting['MorningTime']);
		$bound_lunch_start = $li->timeToSec($FinalSetting['LunchStart']);
		$bound_lunch_end = $li->timeToSec($FinalSetting['LunchEnd']);
		$bound_endschool = $li->timeToSec($FinalSetting['LeaveSchoolTime']);
		$bound_nonschoolday = ($FinalSetting === "NonSchoolDay");
     //list($bound_inschool, $bound_lunch_start, $bound_lunch_end, $bound_endschool, $bound_nonschoolday) = $timetable;
     #print_r($timetable);
     #echo "RecordTime : $ts_record_time <br>\n";
     #echo "Intime : $ts_inTime<br>\n";
     #echo "Bound in school : $bound_inschool<br>\n";
     #echo "<hr>\n";
     #continue;
     if ($bound_nonschoolday)
     {
         continue;
     }
     
     if ($record_id == "")
     {
         # Insert a record
         $sql = "INSERT INTO $table_name (UserID, DayNumber, DateInput, InputBy)
                        VALUES ('$uid','$day',now(),'".$_SESSION['UserID']."')";
         // debug_r($sql);
         $li->db_db_query($sql);
         $new_daily_record_id = $li->db_insert_id();
         # Refill the student record
         $current_records[$uid][0] = $new_daily_record_id;
     }
     
     if ($datatype == 1)        # In School
     {
       if ($ts_inTime=="" || $ts_record_time < $ts_inTime) # Check with original time
       {
         if ($ts_record_time > $bound_inschool)
         {
         	// ignore the tap card record if late more than the "Minutes to ignore late" in basic setting
					if (($ts_record_time - $bound_inschool) > ($li->IgnoreLateTapCardMins * 60) && (($li->IgnoreLateTapCardMins+0)  != 0)) { 
						continue;
					}
					else {
           # 1. Remove Absent  Record
           # 2. Reset discipline upgrade items
           # 3. Insert Late Record
           # 4. Recalculate discipline upgrade items

						$new_status = CARD_STATUS_LATE;	            
             
          	$Result[$uid."SetProfile"] = $li->Set_Profile($uid,$RecordDate,$day_type,CARD_STATUS_LATE,"|**NULL**|","|**NULL**|","|**NULL**|","|**NULL**|",$ImportTimeText,true,"|**NULL**|");
          }
         }
         else
         {
           # 1. Reset discipline upgrade items
           # 2. Remove Late / Absent Record
           # 3. Recalculate discipline upgrade items
           # 4. Update daily log table
           
           $new_status = CARD_STATUS_PRESENT;
           $no_bad_id_list = "$no_bad_delim $uid";
           $no_bad_delim = ",";
           
           $Result[$uid."SetProfile"] = $li->Clear_Profile($uid,$RecordDate,$day_type);
         }
         $sql = "UPDATE $table_name SET 
         					$db_field_status = '$new_status', 
         					InSchoolTime = SEC_TO_TIME('$ts_record_time'),
                  InSchoolStation = '$record_site', 
                  DateModified = now(), 
                  ModifyBy = '".$_SESSION['UserID']."' 
                WHERE UserID = '$uid' AND DayNumber = '$day'";
         // debug_r($sql);
         $li->db_db_query($sql);
         # Update Retrieved record
         $current_records[$uid][1] = $ts_record_time;
         $current_records[$uid][2] = $new_status;
         

       }
       else          # Nothing to do if import time is later
       {
       		
       }
       
        $li->removeBadActionFakedCard($uid,$RecordDate,$day_type);
        $li->removeBadActionNoCardEntrance($uid,$RecordDate);
     }
     else if ($datatype==2)		# Out For Lunch
     {
         if ($ts_lunch_out=="" || $ts_record_time > $ts_lunch_out) # Check with original time
         {
             if ($ts_record_time < $bound_lunch_start)    # Earlier than official lunch starts
             {
	             $sql_select="SELECT RecordID,ProfileRecordID FROM CARD_STUDENT_PROFILE_RECORD_REASON WHERE StudentID='".$uid."' AND RecordType='".PROFILE_TYPE_EARLY."' AND DayType='".$day_type."' AND RecordDate='".$RecordDate."'";
	             $temp_early = $li->returnArray($sql_early,2);
	             if($temp_early[0]!="" && $temp_early[0]>0 && $temp_early[1]!="" && $temp_early[1]>0){
		             # early leave record exists
		         }
		         else{
	                 # Insert EARLY LEAVE Student profile record
	                 $fields = "UserID, AttendanceDate, Year, Semester, RecordType, DayType, DateInput, DateModified, ClassName, ClassNumber,AcademicYearID,YearTermID";
	                 $values = "$uid, '$RecordDate', '".$li->Get_Safe_Sql_Query($school_year)."','".$li->Get_Safe_Sql_Query($semester)."','".PROFILE_TYPE_EARLY."','".$day_type."', now(), now(), '$class_name', '$class_number','$school_year_id','$semester_id'";
	                 $sql = "INSERT INTO PROFILE_STUDENT_ATTENDANCE ($fields) VALUES ($values)";
	                 // debug_r($sql);
	                 $li->db_db_query($sql);
	                 $insert_id = $li->db_insert_id();
	                 # Update to reason table
	                 $fieldname = "RecordDate, StudentID, ProfileRecordID, RecordType, DayType, DateInput, DateModified";
	                 $fieldsvalues = "'$RecordDate', '$uid', '$insert_id', '".PROFILE_TYPE_EARLY."', '".$day_type."', now(), now() ";
	                 $sql = "INSERT INTO CARD_STUDENT_PROFILE_RECORD_REASON ($fieldname)
	                                VALUES ($fieldsvalues)";
	                 // debug_r($sql);
	                 $li->db_db_query($sql);
	             }
                 # Insert to Daily Log
                 $sql = "UPDATE $table_name SET 
                 					LeaveStatus = '".CARD_LEAVE_AM."', 
                 					LunchOutTime = SEC_TO_TIME('$ts_record_time'),
                          LunchOutStation = '$record_site', 
                          DateModified = now(), 
                  				ModifyBy = '".$_SESSION['UserID']."' 
                        WHERE UserID = '$uid' AND DayNumber = '$day'";
                 // debug_r($sql);
                 $li->db_db_query($sql);
             }
             else
             {
	             # remove early leave record
	             $sql_remove="DELETE FROM PROFILE_STUDENT_ATTENDANCE WHERE UserID='".$uid."' AND DATE_FORMAT(AttendanceDate,'%Y-%m-%d')='".$RecordDate."' AND RecordType='".PROFILE_TYPE_EARLY."' AND DayType='".$day_type."'";
	             $sql_remove2="DELETE FROM CARD_STUDENT_PROFILE_RECORD_REASON WHERE StudentID='".$uid."' AND RecordDate='".$RecordDate."' AND RecordType='".PROFILE_TYPE_EARLY."' AND DayType='".$day_type."'";
	             $li->db_db_query($sql_remove);
	             $li->db_db_query($sql_remove2);
	             
                 $no_bad_id_list = "$no_bad_delim $uid";
                 $no_bad_delim = ",";
                 # Insert to Daily Log
                 $sql = "UPDATE $table_name SET 
                 					LeaveStatus = NULL, 
                 					LunchOutTime = SEC_TO_TIME('$ts_record_time'),
                          LunchOutStation = '$record_site', 
                          DateModified = now(), 
                  				ModifyBy = '".$_SESSION['UserID']."' 
                        WHERE UserID = '$uid' AND DayNumber = '$day'";
                 // debug_r($sql);
                 $li->db_db_query($sql);
             }

             # Update Retrieved record
             $current_records[$uid][3] = $ts_record_time;
         }
         else
         {
             # Nothing to do
         }
     }
     else if ($datatype==3)		# PM In School
     {
		if ($am==CARD_STATUS_ABSENT || $am==CARD_STATUS_OUTING) # PM Back School
		{
				  if ($ts_lunch_back == "" || $ts_lunch_back > $ts_record_time)
				  {
				      if ($ts_record_time > $bound_lunch_end)        # Late
				      {
				      	// ignore the tap card record if late more than the "Minutes to ignore late" in basic setting
								if (($ts_record_time - $bound_lunch_end) > ($li->IgnoreLateTapCardMins * 60) && (($li->IgnoreLateTapCardMins+0)  != 0)) { 
									continue;
								}
								else {
					        # 1. Remove Absent Record
					        # 2. Reset discipline upgrade items
					        # 3. Insert Late Record
					        # 4. Recalculate discipline upgrade items
					        # 5. Update daily log table
					        
					     		$NewStatus = CARD_STATUS_LATE;
					        
					        $Result[$uid."SetProfile"] = $li->Set_Profile($uid,$RecordDate,$day_type,CARD_STATUS_LATE,"|**NULL**|","|**NULL**|","|**NULL**|","|**NULL**|",$ImportTimeText,true,"|**NULL**|");
				      	}
				      }
				      else # On Time
				      {
					       # 1. Reset discipline upgrade items
				         # 2. Remove Late / Absent Record
				         # 3. Recalculate discipline upgrade items
				         # 4. Update daily log table
				        
				         $no_bad_id_list = "$no_bad_delim $uid";
				         $no_bad_delim = ",";
				         
				         $NewStatus = CARD_STATUS_PRESENT;
				         $Result[$uid."SetProfile"] = $li->Clear_Profile($uid,$RecordDate,$day_type);
				      }
				      
				      $sql = "UPDATE $table_name SET $db_field_status = '".$NewStatus."', 
				      					InSchoolTime = SEC_TO_TIME('$ts_record_time'),
				                LunchBackTime = SEC_TO_TIME('$ts_record_time'),
				                LunchBackStation = '$record_site', DateModified = now(), 
                  			ModifyBy = '".$_SESSION['UserID']."' 
				             WHERE UserID = '$uid' AND DayNumber = '$day'";
				      // debug_r($sql);
				      $li->db_db_query($sql);
				      # Update Retrieved record
				      $current_records[$uid][1] = $ts_record_time;
				      $current_records[$uid][4] = $ts_record_time;
				 }
				 else
				 {
				     # ignore later record
				 }
			
				$li->removeBadActionFakedCard($uid,$RecordDate,$day_type);
        		$li->removeBadActionNoCardEntrance($uid,$RecordDate);
		}
        else    # AM present case
        {
          if ($ts_lunch_back == "" || $ts_lunch_back > $ts_record_time)
          {
              if ($ts_record_time > $bound_lunch_end)        # Late
              {
              	// ignore the tap card record if late more than the "Minutes to ignore late" in basic setting
								if (($ts_record_time - $bound_lunch_end) > ($li->IgnoreLateTapCardMins * 60) && (($li->IgnoreLateTapCardMins+0)  != 0)) { 
									continue;
								}
								else {
	                # 1. Remove absent record
	                # 2. reset discipline upgrade items
	                # 3. insert late record
	                # 4. recalcualte discipline upgrade items
	                # 5. update daily log table
	                
	               	$NewStatus = CARD_STATUS_LATE;
	                $Result[$uid."SetProfile"] = $li->Set_Profile($uid,$RecordDate,$day_type,CARD_STATUS_LATE,"|**NULL**|","|**NULL**|","|**NULL**|","|**NULL**|",$ImportTimeText,true,"|**NULL**|");
	              }
              }
              else # Present
              {
                # 1. Reset discipline Upgrade items
                # 2. remove late /absent record
                # 3. recalculate discipline upgrade item
                # 4. update daily log table

                $no_bad_id_list = "$no_bad_delim $uid";
                $no_bad_delim = ",";
                  
                $NewStatus = CARD_STATUS_PRESENT;
              	$Result[$uid."SetProfile"] = $li->Clear_Profile($uid,$RecordDate,$day_type);
              }
              
              $sql = "UPDATE $table_name SET 
              					$db_field_status = '".$NewStatus."', LunchBackTime = SEC_TO_TIME('$ts_record_time'),
	                     	LunchBackStation = '$record_site', DateModified = now(), 
                  			ModifyBy = '".$_SESSION['UserID']."' 
                     WHERE UserID = '$uid' AND DayNumber = '$day'";
              // debug_r($sql);
              $li->db_db_query($sql);
              # Update Retrieved record
              $current_records[$uid][4] = $ts_record_time;
          }
          else
          {
              # ignore later records
          }
          
          	$li->removeBadActionFakedCard($uid,$RecordDate,$day_type);
        	$li->removeBadActionNoCardEntrance($uid,$RecordDate);
        }
     }
     else # datatype==4			# Leave School
     {
         if ($ts_leave_time=="" || $ts_record_time > $ts_leave_time) # Check with original time
         {
             if ($ts_record_time < $bound_endschool)    # Earlier than official school end
             {

	             $sql_select="SELECT RecordID,ProfileRecordID FROM CARD_STUDENT_PROFILE_RECORD_REASON WHERE StudentID='".$uid."' AND RecordType='".PROFILE_TYPE_EARLY."' AND DayType='".$day_type."' AND RecordDate='".$RecordDate."'";
	             $temp_early = $li->returnArray($sql_select,2);

	             if($temp_early[0]!="" && $temp_early[0]>0 && $temp_early[1]!="" && $temp_early[1]>0){
		             # early leave record exists
		         }
		         else{
	                 # Insert EARLY LEAVE Student profile record
	                 $fields = "UserID, AttendanceDate, Year, Semester, RecordType, DayType, DateInput, DateModified, ClassName, ClassNumber,AcademicYearID,YearTermID";
	                 $values = "$uid, '$RecordDate', '".$li->Get_Safe_Sql_Query($school_year)."','".$li->Get_Safe_Sql_Query($semester)."','".PROFILE_TYPE_EARLY."','".$day_type."', now(), now(), '$class_name', '$class_number','$school_year_id','$semester_id'";
	                 $sql = "INSERT INTO PROFILE_STUDENT_ATTENDANCE ($fields) VALUES ($values)";
	                 // debug_r($sql);
	                 $li->db_db_query($sql);
	                 $insert_id = $li->db_insert_id();
	                 # Update to reason table
	                 $fieldname = "RecordDate, StudentID, ProfileRecordID, RecordType, DayType, DateInput, DateModified";
	                 $fieldsvalues = "'$RecordDate', '$uid', '$insert_id', '".PROFILE_TYPE_EARLY."', '".$day_type."', now(), now() ";
	                 $sql = "INSERT INTO CARD_STUDENT_PROFILE_RECORD_REASON ($fieldname)
	                                VALUES ($fieldsvalues)";
	                 // debug_r($sql);
	                 $li->db_db_query($sql);
	             }
                 # Insert to Daily Log
                 $sql = "UPDATE $table_name SET 
                 					LeaveStatus = '".$bad_record_card_status."', LeaveSchoolTime = SEC_TO_TIME('$ts_record_time'),
                          LeaveSchoolStation = '$record_site', DateModified = now(), 
            							ModifyBy = '".$_SESSION['UserID']."' 
												WHERE UserID = '$uid' AND DayNumber = '$day'";
                 // debug_r($sql);
                 $li->db_db_query($sql);
             }
             else
             {
	             # remove early leave record
	             $sql_remove="DELETE FROM PROFILE_STUDENT_ATTENDANCE WHERE UserID='".$uid."' AND DATE_FORMAT(AttendanceDate,'%Y-%m-%d')='".$RecordDate."' AND RecordType='".PROFILE_TYPE_EARLY."' AND DayType='".$day_type."'";
	             $sql_remove2="DELETE FROM CARD_STUDENT_PROFILE_RECORD_REASON WHERE StudentID='".$uid."' AND RecordDate='".$RecordDate."' AND RecordType='".PROFILE_TYPE_EARLY."' AND DayType='".$day_type."'";
	             $li->db_db_query($sql_remove);
	             $li->db_db_query($sql_remove2);
	             
	             if($day_type == PROFILE_DAY_TYPE_PM){
	             	# remove AM early leave record if PM is not early leave
	             	$sql_remove3="DELETE FROM PROFILE_STUDENT_ATTENDANCE WHERE UserID='".$uid."' AND DATE_FORMAT(AttendanceDate,'%Y-%m-%d')='".$RecordDate."' AND RecordType='".PROFILE_TYPE_EARLY."' AND DayType='".PROFILE_DAY_TYPE_AM."'";
	            	$sql_remove4="DELETE FROM CARD_STUDENT_PROFILE_RECORD_REASON WHERE StudentID='".$uid."' AND RecordDate='".$RecordDate."' AND RecordType='".PROFILE_TYPE_EARLY."' AND DayType='".PROFILE_DAY_TYPE_AM."'";
	             	$li->db_db_query($sql_remove3);
	            	$li->db_db_query($sql_remove4);
	             }
	             
                 $no_bad_id_list = "$no_bad_delim $uid";
                 $no_bad_delim = ",";
                 # Insert to Daily Log
                 $sql = "UPDATE $table_name SET 
                 					LeaveStatus = '".CARD_LEAVE_NORMAL."', LeaveSchoolTime = SEC_TO_TIME('$ts_record_time'),
                          LeaveSchoolStation = '$record_site', DateModified = now(), 
                					ModifyBy = '".$_SESSION['UserID']."' 
                        WHERE UserID = '$uid' AND DayNumber = '$day'";
                 // debug_r($sql);
                 $li->db_db_query($sql);
             }
         }
         else
         {
             # Nothing to do
         }
     }
}
#exit();
if ($no_bad_id_list != "")
{
    # Try to remove previous profile and reason record
    $sql = "DELETE FROM PROFILE_STUDENT_ATTENDANCE WHERE DayType = '".$day_type."'
                        AND RecordType = '".$profile_type."' AND UserID IN ($no_bad_id_list)";
                        // debug_r($sql);
    $li->db_db_query($sql);

    $sql = "DELETE FROM CARD_STUDENT_PROFILE_RECORD_REASON WHERE DayType = '".$day_type."'
                        AND RecordType = '".$profile_type."' AND StudentID IN ($no_bad_id_list)";
                        // debug_r($sql);
    $li->db_db_query($sql);

}



$sql = "DROP TABLE TEMP_CARD_STUDENT_LOG";
$li->db_db_query($sql);
intranet_closedb();

$Msg = $Lang['StudentAttendance']['OfflineRecordImportSuccess'];
header("Location: import.php?Msg=".urlencode($Msg));
?>