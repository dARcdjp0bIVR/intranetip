<?php
// Editing by 
/*
 * 2019-07-26 (Ray): Add subject_id
 * 2014-09-23 (Carlos): Created
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libtimetable.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libstudentattendance_ui.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping_ui.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();

if($task == 'get_subject_group_table')
{
	/*
	 * @params
	 * $TargetDate
	 * $DayType
	 * $SubjectID
	 */
	$result = $lc->Get_Subject_Group_Attendance_List_Without_Timetable($TargetDate,$DayType,'',$SubjectID);
	
	$x = "<table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">\n";
	$x .= "<tr class=\"tabletop\">
					<td class=\"tabletoplink\">".$Lang['SysMgr']['SubjectClassMapping']['Group']."</td>
					<td class=\"tabletoplink\">$i_StudentAttendance_Field_ConfirmedBy</td>
					<td class=\"tabletoplink\">$i_StudentAttendance_Field_LastConfirmedTime</td>
				</tr>\n";
	
	for($i=0; $i<sizeOf($result); $i++)
	{
		list($subject_group_id, $subject_group_name, $confirmed_username, $confirmed_date, $subject_id) = $result[$i];
		
		$css=($i%2==0)?"tablerow1":"tablerow2";
		
		  $class_link = "<a class=\"tablelink\" href=\"view_student.php?SubjectID=".urlencode($subject_id)."&SubjectGroupID=".urlencode($subject_group_id)."&TargetDate=".urlencode($TargetDate)."&DayType=".urlencode($DayType)."\">$subject_group_name</a>";
		  $x .= "<tr class=\"$css\">
		  				<td class=\"tabletext\">$class_link</td>
		  				<td class=\"tabletext\">$confirmed_username</td>
		  				<td class=\"tabletext\">$confirmed_date</td>
		  			</tr>\n";
	}
	$x .= "</table>\n";
	
	echo $x;
}

intranet_closedb();
?>