<?php
# Editing by 

/********************************************** Change Log ***************************************************
 * 2019-08-09 (Ray): added SubmittedAbsentLateRecord
 * 2018-08-03 (Carlos): [ip.2.5.9.10.1] Mark attendance records confirmed by admin.
 * 2017-05-31 (Carlos): [ip.2.5.8.7.1] Check and skip outdated record if attendance record last modified time is later than page load time. 
 * 2017-02-24 (Carlos): [ip2.5.8.3.1] added status change log for tracing purpose. required new method libcardstudentattend2->log()
 * 2016-07-05 (Carlos): [ip2.5.7.7.1] $sys_custom['StudentAttendance']['AllowEditTime'] - update in time if time is being edited.
 * 2015-02-13 (Carlos): [ip2.5.6.3.1] Added [Waived], [Reason], [Office Remark].
 * 2014-10-17 (Carlos): [ip2.5.5.10.1] Update/insert remark records
 * 2014-09-23 (carlos): Added SubjectID(if set) to return url 
 * 2014-03-26 (Carlos): Use 'NULL' as Waive value for Set_Profile()
 * 2014-03-14 (Carlos): $sys_custom['SmartCardAttendance_StudentAbsentSession2'] - added Absent Session
 * Created on 2013-05-14
 *************************************************************************************************************/

$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lcardattend = new libcardstudentattend2();
$lcardattend->retrieveSettings();
/*
# check page load time 
if($lcardattend->isDataOutDated($user_id,$TargetDate,$PageLoadTime)){
	$error = 1;  // data outdated
	
	$return_page = "view_student.php?SubjectGroupID=$SubjectGroupID".(isset($SubjectID)?"&SubjectID=".$SubjectID:"")."&DayType=$DayType&TargetDate=$TargetDate&Msg=".urlencode($Lang['StudentAttendance']['DataOutOfDate']);
	header("Location: $return_page");
	exit();	
}
*/
//$has_magic_quotes = get_magic_quotes_gpc();
$has_magic_quotes = $lcardattend->is_magic_quotes_active;

# LOCK TABLES
//$lcardattend->lockTablesAdmin();
$upadteeDisLateRecord = $lcardattend->OnlySynceDisLateRecordWhenConfirm != '1';

################################################
$directProfileInput = false;
################################################

$lcardattend->Start_Trans();
$Result = array();
if ($plugin['Disciplinev12']){
	include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
  	$ldisciplinev12 = new libdisciplinev12();
  	$LateCtgInUsed = $ldisciplinev12->CategoryInUsed(PRESET_CATEGORY_LATE);
  
  	## Only For IP25 eDis1.2 Only ##
  	if($sys_custom['Discipline_SeriousLate']){	
	    $sql = "SELECT CategoryID FROM DISCIPLINE_ACCU_CATEGORY WHERE MeritType = -1 AND LateMinutes IS NOT NULL";
	    $targetCatID = $ldisciplinev12->returnVector($sql);
	    if(sizeof($targetCatID)>0){
	    	for($i=0; $i<sizeof($targetCatID); $i++){
	    		$result = $ldisciplinev12->CategoryInUsed($targetCatID[$i]);
	    		
	    		if($result){
	    			$cnt++;
	    		}
	    	}
	    }
	    if($cnt > 0){
	    	$SeriousLateUsed = 1;
	    }
	}
}

### Set Date from previous page
$ts_record = strtotime($TargetDate);
if ($ts_record == -1)
{
    $TargetDate = date('Y-m-d');
    $ts_record = strtotime($TargetDate);
}

$txt_year = date('Y',$ts_record);
$txt_month = date('m',$ts_record);
$txt_day = date('d',$ts_record);
// $year = getCurrentAcademicYear();
// $semester = getCurrentSemester();

//$school_year = GET_ACADEMIC_YEAR_WITH_FORMAT($TargetDate);
//debug_pr($school_year);

list($school_year_id, $school_year, $semester_id, $semester) = getAcademicYearInfoAndTermInfoByDate($TargetDate);

### for student confirm
$card_log_table_name = "CARD_STUDENT_DAILY_LOG_".$txt_year."_".$txt_month;


### Bug tracing
if($bug_tracing['smartcard_student_attend_status']){
	include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
	if(!is_dir($file_path."/file/log_student_attend_status")){
		$lf = new libfilesystem();
		$lf->folder_new($file_path."/file/log_student_attend_status");
	}
	$log_filepath = "$file_path/file/log_student_attend_status/log_student_attend_status_".($txt_year.$txt_month.$txt_day).".txt";
}

$log_row = date("Y-m-d H:i:s").' '.$_SERVER['REQUEST_URI'].' '.$_SESSION['UserID'].' ';

$csv_student_list = "'".implode("','",$user_id)."'";
$sql = "SELECT RecordID,StudentID FROM CARD_STUDENT_DAILY_REMARK WHERE RecordDate='$TargetDate' AND StudentID IN ($csv_student_list) AND DayType='$DayType'";
$remark_records = $lcardattend->returnResultSet($sql);
$RemarkStudentRecords = array();
$remark_record_count = count($remark_records);
for($i=0;$i<$remark_record_count;$i++){
	$RemarkStudentRecords[$remark_records[$i]['StudentID']] = $remark_records[$i]['RecordID'];
}

$studentIdToDailyLog = $lcardattend->getDailyLogRecords(array('RecordDate'=>$TargetDate,'StudentID'=>$user_id,'StudentIDToRecord'=>1));

for($i=0; $i<sizeOf($user_id); $i++)
{
	$my_user_id = $user_id[$i];
	
	$my_day = $txt_day;
	$my_drop_down_status = $drop_down_status[$i];
	$my_record_id = $record_id[$i];
	
	// check and skip outdated record if attendance record last modified time is later than page load time
	if(isset($studentIdToDailyLog[$my_user_id])){
		if($studentIdToDailyLog[$my_user_id]['DateModified'] != '')
		{
			$am_modified_ts = strtotime($studentIdToDailyLog[$my_user_id]['DateModified']);
			if($am_modified_ts >= $PageLoadTime) continue;
		}
		if($studentIdToDailyLog[$my_user_id]['PMDateModified'] != '')
		{
			$pm_modified_ts = strtotime($studentIdToDailyLog[$my_user_id]['PMDateModified']);
			if($pm_modified_ts >= $PageLoadTime) continue;
		}
	}
	
	// Kenneth Code Start
	if ($DayType == PROFILE_DAY_TYPE_AM) {
		$InSchoolTimeField = "InSchoolTime";
		$AttendanceArrayTime = "MorningTime";
		$StatusField = "AMStatus";
		$ConfirmField = "IsConfirmed";
		$ConfirmByField = "ConfirmedUserID";
		
		$DateModifiedField = "DateModified";
  		$ModifyByField = "ModifyBy";
	}
	else {
		$lcardattend->retrieveSettings();
		if($lcardattend->attendance_mode==1)	# retrieve InSchoolTime
		{
			$InSchoolTimeField = "InSchoolTime";
			$AttendanceArrayTime = 'MorningTime';
		}
		else # retrieve LunchBackTime
		{
			$InSchoolTimeField = "LunchBackTime";
			$AttendanceArrayTime = 'LunchEnd';
		}
		$StatusField = "PMStatus";
		$ConfirmField = "PMIsConfirmed";
		$ConfirmByField = "PMConfirmedUserID";
		
		$DateModifiedField = "PMDateModified";
  		$ModifyByField = "PMModifyBy";
	}
	
	$inTimesql = "select 
	            		".$InSchoolTimeField."
	            	from 
	            		$card_log_table_name
								WHERE 
									DayNumber = '$txt_day'
									AND 
									UserID = '$my_user_id'";
  $inTimeResult = $lcardattend->returnVector($inTimesql);
  $InSchoolTime = $inTimeResult[0];
      
    $waived = $_REQUEST['Waived'.$i] == ''?0:1;
    $input_reason = trim($_REQUEST['input_reason'.$i]); 
    if($has_magic_quotes){
    	$input_reason = stripslashes($input_reason); 
    }  
      
	# insert if not exist
	if($my_record_id=="")
	{
    $sql = "INSERT INTO $card_log_table_name (
    					UserID,
    					DayNumber,
    					".$StatusField.",
    					".$ConfirmField.",
    					".$ConfirmByField.",
    					DateInput,
    					InputBy,
    					".$DateModifiedField.",
    					".$ModifyByField."
    				) VALUES (
    					'$my_user_id',
    					'$my_day',
    					'$my_drop_down_status',
    					'1',
    					'".$_SESSION['UserID']."',
    					NOW(),
    					'".$_SESSION['UserID']."',
    					NOW(),
    					'".$_SESSION['UserID']."'
    				)
            ";
    //debug_r($sql);
    $Result[$i.'_InsertCardlog'] = $lcardattend->db_db_query($sql);
    
    ## Bug Tracing
    if($bug_tracing['smartcard_student_attend_status']){
      $log_date = date('Y-m-d H:i:s');
      $log_target_date = $TargetDate;
      $log_student_id = $my_user_id;
      $log_old_status = "";
      $log_new_status = $my_drop_down_status;
      $log_sql = $sql;
      $log_admin_user = $PHP_AUTH_USER;
                        
			$log_page = 'view_student_update.php';
      $log_content = get_file_content($log_filepath);
      $log_entry = "\"$log_page\",\"$log_student_id\",\"$log_target_date\",\"$log_old_status\",\"$log_new_status\",\"$log_sql\",\"".$_SESSION['UserID']."\",\"$log_date\"\n";
      $log_content .= $log_entry;
      write_file_content($log_content, $log_filepath);	                        
    }
    
    $log_row .= '['.$TargetDate.' '.$DayType.' '.$my_user_id.' NULL '.$my_drop_down_status.']';
    
    // skip processing profile if fail to insert cardlog, otherwise may cause data problem
    if(!$Result[$i.'_InsertCardlog']) {
    	continue;
    }
                      
    # Check Bad actions
    if ($my_drop_down_status==CARD_STATUS_PRESENT)           # empty -> present
    {
      $lcardattend->removeBadActionFakedCard($my_user_id,$TargetDate,$DayType);
      # Forgot to bring card
      $lcardattend->addBadActionNoCardEntrance($my_user_id,$TargetDate,"");
      
      $Result[$my_user_id.'ClearProfile'] = $lcardattend->Clear_Profile($my_user_id,$TargetDate,$DayType,true);
    }
		else if ($my_drop_down_status==CARD_STATUS_LATE)
		{
	    $lcardattend->removeBadActionFakedCardAM($my_user_id,$TargetDate);
	    # Forgot to bring card
	    $lcardattend->addBadActionNoCardEntrance($my_user_id,$TargetDate,"");
			
			//$Result[$my_user_id.'SetProfile'] = $lcardattend->Set_Profile($my_user_id,$TargetDate,$DayType,$my_drop_down_status,"|**NULL**|","|**NULL**|","|**NULL**|","|**NULL**|",$InSchoolTime,false,'NULL',false,$upadteeDisLateRecord);
			$Result[$my_user_id.'SetProfile'] = $lcardattend->Set_Profile($my_user_id,$TargetDate,$DayType,$my_drop_down_status,$input_reason,"|**NULL**|","|**NULL**|","|**NULL**|",$InSchoolTime,false,$waived,false,$upadteeDisLateRecord);

			if($sys_custom['StudentAttendance']['SubmitAbsentLateRecordsToExternalSystem']) {
				$Result[$my_user_id.'SubmittedAbsentLateRecord'] = $lcardattend->InsertSubmittedAbsentLateRecord($my_user_id, $TargetDate, $DayType,'','', PROFILE_TYPE_LATE , '','');
			}
		}else if ($my_drop_down_status==CARD_STATUS_ABSENT || $my_drop_down_status==CARD_STATUS_OUTING){ # No Daily Record and Absent
			//$Result[$my_user_id.'SetProfile'] = $lcardattend->Set_Profile($my_user_id,$TargetDate,$DayType,$my_drop_down_status,"|**NULL**|","|**NULL**|","|**NULL**|","|**NULL**|","|**NULL**|",false,'NULL',false,$upadteeDisLateRecord);
			$Result[$my_user_id.'SetProfile'] = $lcardattend->Set_Profile($my_user_id,$TargetDate,$DayType,$my_drop_down_status,$input_reason,"|**NULL**|","|**NULL**|","|**NULL**|","|**NULL**|",false,$waived,false,$upadteeDisLateRecord);
		}
  }
  else  # update if exist
  {
		# Grab original status
		$sql = "SELECT ".$StatusField.", InSchoolTime, RecordID, InSchoolTime, LunchBackTime FROM $card_log_table_name WHERE RecordID = '$my_record_id'";
		$temp = $lcardattend->returnArray($sql,3);
		list($old_status, $old_inTime, $old_record_id, $old_inSchoolTime, $old_lunchBackTime) = $temp[0];
				 
		if ($old_status != $my_drop_down_status)
		{
			# Check bad actions
			# Late / Present -> Absent
			if (($old_status==CARD_STATUS_PRESENT || $old_status==CARD_STATUS_LATE) && $my_drop_down_status==CARD_STATUS_ABSENT)
			{
				$lcardattend->removeBadActionNoCardEntrance($my_user_id,$TargetDate);
				if($old_inTime!=""){
					# Has card record but not present in classroom
					$lcardattend->addBadActionFakedCard($my_user_id,$TargetDate,$old_inTime,$DayType);
				}
			}
			
			# Absent -> Late / Present
			if (($old_status==CARD_STATUS_ABSENT || trim($old_status) == "") && ($my_drop_down_status==CARD_STATUS_PRESENT || $my_drop_down_status==CARD_STATUS_LATE))
			{
				$lcardattend->removeBadActionFakedCard($my_user_id,$TargetDate,$DayType);
				# forgot to bring card
				if(trim($old_inSchoolTime)=='' && trim($old_lunchBackTime)==''){ // add no card entrance log if and only if no both AM in time and PM in time
					$lcardattend->addBadActionNoCardEntrance($my_user_id,$TargetDate,$old_inTime);
				}
			}

			if(trim($old_status) == "" && $my_drop_down_status==CARD_STATUS_LATE) {
				if($sys_custom['StudentAttendance']['SubmitAbsentLateRecordsToExternalSystem']) {
					$Result[$my_user_id.'SubmittedAbsentLateRecord'] = $lcardattend->InsertSubmittedAbsentLateRecord($my_user_id, $TargetDate, $DayType,'','', PROFILE_TYPE_LATE , '','');
				}
			}
			
			$LastModifyUpdate = ", ".$DateModifiedField."=now(),
													 ".$ModifyByField."='".$_SESSION['UserID']."' ";
		}
		else {
			$LastModifyUpdate = "";
		}
			
		# Update Daily table
		$sql = "UPDATE $card_log_table_name 
							SET ".$StatusField."='".$my_drop_down_status."',  
							".$ConfirmField." = '1',
  						".$ConfirmByField." = '".$_SESSION['UserID']."'
  						".$LastModifyUpdate." 
						WHERE RecordID='".$my_record_id."'";
		$Result[$i.'4'] = $lcardattend->db_db_query($sql);
		 
		 ## Bug Tracing
		if($bug_tracing['smartcard_student_attend_status']){
		  $log_date = date('Y-m-d H:i:s');
		  $log_target_date = $TargetDate;
		  $log_student_id = $my_user_id;
		  $log_old_status = $old_status;
		  $log_new_status = $my_drop_down_status;
		  $log_sql = $sql;
		  $log_admin_user = $PHP_AUTH_USER;
		    
			$log_page = 'view_student_update.php';
			$log_content = get_file_content($log_filepath);
			$log_entry = "\"$log_page\",\"$log_student_id\",\"$log_target_date\",\"$log_old_status\",\"$log_new_status\",\"$log_sql\",\"".$_SESSION['UserID']."\",\"$log_date\"\n";
			$log_content .= $log_entry;
			write_file_content($log_content, $log_filepath);	                        
		}
		
		$log_row .= '['.$TargetDate.' '.$DayType.' '.$my_user_id.' '.$old_status.' '.$my_drop_down_status.']';
		
		if ($my_drop_down_status==CARD_STATUS_LATE) {
			//$Result[$my_user_id.'SetProfile'] = $lcardattend->Set_Profile($my_user_id,$TargetDate,$DayType,$my_drop_down_status,"|**NULL**|","|**NULL**|","|**NULL**|","|**NULL**|",$InSchoolTime,false,'NULL',false,$upadteeDisLateRecord);
			$Result[$my_user_id.'SetProfile'] = $lcardattend->Set_Profile($my_user_id,$TargetDate,$DayType,$my_drop_down_status,$input_reason,"|**NULL**|","|**NULL**|","|**NULL**|",$InSchoolTime,false,$waived,false,$upadteeDisLateRecord);
		}
		else if ($my_drop_down_status==CARD_STATUS_ABSENT || $my_drop_down_status==CARD_STATUS_OUTING) {
			//$Result[$my_user_id.'SetProfile'] = $lcardattend->Set_Profile($my_user_id,$TargetDate,$DayType,$my_drop_down_status,"|**NULL**|","|**NULL**|","|**NULL**|","|**NULL**|","",false,'NULL',false,true);
			$Result[$my_user_id.'SetProfile'] = $lcardattend->Set_Profile($my_user_id,$TargetDate,$DayType,$my_drop_down_status,$input_reason,"|**NULL**|","|**NULL**|","|**NULL**|","",false,$waived,false,true);
		}
		else if ($my_drop_down_status==CARD_STATUS_PRESENT) {
			$Result[$my_user_id.'ClearProfile'] = $lcardattend->Clear_Profile($my_user_id,$TargetDate,$DayType,true);
		}
  }
	// end Kenneth Code Start
	
	// Handle teacher's remark records
	$remark_value = trim($TeacherRemark[$i]);
	if(!$has_magic_quotes){
		$remark_value = addslashes($remark_value);
	}
	$Result[$my_user_id.'UpdateRemark'] = $lcardattend->updateTeacherRemark($my_user_id,$TargetDate,$DayType,$remark_value);
	
	// handle Office remark
	if($my_drop_down_status != CARD_STATUS_PRESENT){
		$office_remark_value = trim($OfficeRemark[$i]);
		if(!$has_magic_quotes){
			$office_remark_value = addslashes($office_remark_value);
		}
		$RealProfileType = $my_drop_down_status;
		if($my_drop_down_status == CARD_STATUS_OUTING){
			$RealProfileType = CARD_STATUS_ABSENT;
		}
		$lcardattend->updateOfficeRemark($my_user_id,$TargetDate,$DayType,$RealProfileType,$office_remark_value);
	}
	
	if($sys_custom['StudentAttendance']['AllowEditTime'] && isset($_POST['InSchoolTime'.$my_user_id]))
	{
		$lcardattend->updateAttendanceInTime($my_user_id,$TargetDate,$DayType,$_POST['InSchoolTime'.$my_user_id]);
	}
	
	/*
	if(isset($RemarkStudentRecords[$my_user_id]) && $RemarkStudentRecords[$my_user_id]!='' && $RemarkStudentRecords[$my_user_id]>0){
		// Update remark
		$sql = "UPDATE CARD_STUDENT_DAILY_REMARK SET Remark='$remark_value' WHERE StudentID='$my_user_id' AND RecordDate='$TargetDate' AND DayType='$DayType'";
		$Result[$my_user_id.'UpdateRemark'] = $lcardattend->db_db_query($sql);
	}else{
		// Insert remark
		$sql = "INSERT INTO CARD_STUDENT_DAILY_REMARK (StudentID,RecordDate,DayType,Remark) VALUES ('$my_user_id','$TargetDate','$DayType','$remark_value')";
		$Result[$my_user_id.'InsertRemark'] = $lcardattend->db_db_query($sql);
	}
	*/
	if($sys_custom['SmartCardAttendance_StudentAbsentSession'] || $sys_custom['SmartCardAttendance_StudentAbsentSession2']) {
		$sql = "select 
							LateSession,
							RequestLeaveSession,
							PlayTruantSession,
							OfficalLeaveSession 
							".($sys_custom['SmartCardAttendance_StudentAbsentSession2']?",AbsentSession ":"")."  
						from 
							CARD_STUDENT_STATUS_SESSION_COUNT 
						where 
							RecordDate = '".$TargetDate."' 
							and 
							StudentID = '".$my_user_id."' 
							and 
							DayType = '".$DayType."'";
		$OrgRecord = $lcardattend->returnArray($sql);
		
		if ($OrgRecord[0]['LateSession'] != $late_session[$i] 
				|| $OrgRecord[0]['RequestLeaveSession'] != $request_leave_session[$i] 
				|| $OrgRecord[0]['PlayTruantSession'] != $play_truant_session[$i] 
				|| $OrgRecord[0]['OfficalLeaveSession'] != $offical_leave_session[$i]
				|| $OrgRecord[0]['AbsentSession'] != $absent_session[$i]) {
			# Update Daily table
			$sql = "UPDATE ".$card_log_table_name." SET 
  							".$DateModifiedField."=now(),
								".$ModifyByField."='".$_SESSION['UserID']."' 
						WHERE 
							DayNumber = '".$my_day."' 
    					and 
							UserID='".$my_user_id."'";
			$Result['UpdateLastModify:'.$my_user_id] = $lcardattend->db_db_query($sql);
		}
		
		// insert/update the number of sessions to db
		$sql = "insert into CARD_STUDENT_STATUS_SESSION_COUNT (
							RecordDate,
							StudentID,
							DayType,
							LateSession,
							RequestLeaveSession,
							PlayTruantSession,
							OfficalLeaveSession,
							".($sys_custom['SmartCardAttendance_StudentAbsentSession2']?"AbsentSession,":"")."
							DateInput,
							InputBy,
							DateModify,
							ModifyBy
							)
						values (
							'".$TargetDate."',
							'".$my_user_id."',
							'".$DayType."',
							'".$late_session[$i]."',
							'".$request_leave_session[$i]."',
							'".$play_truant_session[$i]."',
							'".$offical_leave_session[$i]."',
							".($sys_custom['SmartCardAttendance_StudentAbsentSession2']?"'".$absent_session[$i]."',":"")."
							NOW(),
						  '".$_SESSION['UserID']."',
						  NOW(),
						  '".$_SESSION['UserID']."'
						 ) 
						on duplicate key update 
							LateSession = '".$late_session[$i]."',
							RequestLeaveSession = '".$request_leave_session[$i]."',
							PlayTruantSession = '".$play_truant_session[$i]."',
							OfficalLeaveSession = '".$offical_leave_session[$i]."',
							".($sys_custom['SmartCardAttendance_StudentAbsentSession2']?"AbsentSession = '".$absent_session[$i]."',":"")."
							DateModify = NOW(),
							ModifyBy = '".$_SESSION['UserID']."'";
		$Result['UpdateSessions'] = $lcardattend->db_db_query($sql);
	}
}

## mark as confirmed by admin user
if(count($user_id)>0){
	$confirmed_by_admin_field = $DayType == PROFILE_DAY_TYPE_PM? "PMConfirmedByAdmin" : "AMConfirmedByAdmin";
	$sql = "UPDATE $card_log_table_name SET $confirmed_by_admin_field='1' WHERE DayNumber='$txt_day' AND UserID IN (".implode(",",$user_id).")";
	$lcardattend->db_db_query($sql);
}

### for class confirm
$card_student_daily_subject_group_confirm = "CARD_STUDENT_DAILY_SUBJECT_GROUP_CONFIRM_".$txt_year."_".$txt_month;

if( $confirmed_id <> "" )
{
        # update if record exist
        $sql = "UPDATE $card_student_daily_subject_group_confirm SET ConfirmedUserID='".$_SESSION['UserID']."', DateModified = NOW() WHERE RecordID = '$confirmed_id'";
        $Result[$i.'26'] = $lcardattend->db_db_query($sql);
        $msg = 2;
}
else
{
        # insert if record not exist

        $sql = "INSERT INTO $card_student_daily_subject_group_confirm
                                        (
                                                SubjectGroupID,
                                                ConfirmedUserID,
                                                DayNumber,
                                                DayType,
                                                DateInput,
                                                DateModified
                                        ) VALUES
                                        (
                                                '$SubjectGroupID',
                                                '".$_SESSION['UserID']."',
                                                '$txt_day',
                                                '$DayType',
                                                NOW(),
                                                NOW()
                                        )
                ON DUPLICATE KEY UPDATE 
                	ConfirmedUserID='".$_SESSION['UserID']."', DateModified = NOW()
                                        ";
        $Result[$i.'27'] = $lcardattend->db_db_query($sql);
        $msg = 1;
}

// remove the confirm cache status
$sql = "delete from CARD_STUDENT_CACHED_CONFIRM_STATUS where TargetDate = '".$TargetDate."' and DayType = '".$DayType."' and Type = 'Subject_Group'";
$Result["ClearConfirmCache"] = $lcardattend->db_db_query($sql);

## UNLOCK TABLES
//$lcardattend->unlockTables();

### Lunch box cancel status
if ($DayType == PROFILE_DAY_TYPE_AM && $sys_custom['SmartCardAttendance_LunchBoxOption'])
{
	$lunch_tablename = $lcardattend->createTable_Card_Student_Lunch_Box_Option($txt_year, $txt_month);
	$dayNumber = $txt_day;
	for ($i=0; $i<sizeof($user_id); $i++)
	{
		$t_student_id = $user_id[$i];
		$option = ${"LunchBoxOption_$t_student_id"};
		$option += 0;
		$sql = "INSERT IGNORE INTO $lunch_tablename (StudentID, DayNumber, CancelOption, DateInput, DateModified)
		              VALUES ('$t_student_id', '$dayNumber', '".$option."', now(), now())";
		$Result[$i.'28'] = $lcardattend->db_db_query($sql);
	if ($lcardattend->db_affected_rows()!=1)
	{
		$sql = "UPDATE $lunch_tablename SET CancelOption = '$option' WHERE StudentID = '$t_student_id' AND DayNumber = '$dayNumber'";
		$Result[$i.'29'] = $lcardattend->db_db_query($sql);
	}
	
	
	}
}

if (!in_array(false,$Result)) {
	$Msg = $Lang['StudentAttendance']['DailyStatusConfirmSuccess'];
	$lcardattend->Commit_Trans($sql);
	$log_row .= " COMMITTED";
}
else {
	$Msg = $Lang['StudentAttendance']['DailyStatusConfirmFail'];
	$lcardattend->RollBack_Trans($sql);
	$log_row .= " ROLLBACKED";
}

$lcardattend->log($log_row);
intranet_closedb();
$return_url = "view_student.php?SubjectGroupID=".urlencode($SubjectGroupID).(isset($SubjectID)?"&SubjectID=".urlencode($SubjectID):"")."&DayType=".urlencode($DayType)."&TargetDate=".urlencode($TargetDate)."&Msg=".urlencode($Msg);
header("Location: $return_url");
?>