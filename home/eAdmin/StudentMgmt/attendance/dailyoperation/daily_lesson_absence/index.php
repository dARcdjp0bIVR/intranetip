<?php
//using by :

###################### Change Log Start ########################
#
###################### Change Log End ########################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libstudentattendance_ui.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libwordtemplates.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-LessonAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancelesson'] || get_client_region() != 'zh_TW') {
	header ("Location: /");
	intranet_closedb();
	exit();
}


$TargetDate = $_REQUEST['TargetDate'];
if($TargetDate == '') {
	$TargetDate = date('Y-m-d');
}

$linterface = new interface_html();
$lword = new libwordtemplates();

$lc = new libcardstudentattend2();
$lc->retrieveSettings();
$luser = new libuser();
$studentWithParentUsingParentAppAry = $luser->getStudentWithParentUsingParentApp();

$LessonAttendUI = new libstudentattendance_ui();
$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageLessonAttendance_DailyAbsenceOverview";

$statusMapping = $LessonAttendUI->LessonAttendanceStatusMapping();

$lesson = array();
$rs = $LessonAttendUI->Get_Time_Slot_List($TargetDate);
foreach($rs as $temp) {
	$lesson[] = array($temp['TimeSlotID'],$temp['TimeSlotName']);
}
$selectLesson = getSelectByArray($lesson, 'name="TimeSlotID" id="TimeSlotID"', $TimeSlotID, 0, 0, $i_general_all);


### Title ###
$TAGS_OBJ[] = array($Lang['LessonAttendance']['DailyLessonAbsenceOverview'],"",0);
$MODULE_OBJ = $LessonAttendUI->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

$TargetDate = ($TargetDate != "") ? $TargetDate : date('Y-m-d');

$TermInfo = getAcademicYearInfoAndTermInfoByDate($TargetDate);

list($AcademicYearID,$AcademicYearName,$YearTermID,$YearTermName) = $TermInfo;

$LastModifyUserNameField = getNameFieldByLang2("u.");

$cond = '';
if($TimeSlotID != '') {
	$cond .= " AND itra.TimeSlotID='".$lc->Get_Safe_Sql_Query($TimeSlotID)."'";
}

$sql = "SELECT
sgsa.AttendOverviewID,
sgsa.StudentID,
".getNameFieldWithClassNumberByLang("a.")." AS name,
a.Gender,
itt.TimeSlotName,
stc.ClassTitleB5 as SubjectGroupTitleB5,
stc.ClassTitleEN as SubjectGroupTitleEN,
ass_sub.CH_DES as SubjectNameB5, 
ass_sub.EN_DES as SubjectNameEN,
sgsa.AttendStatus,
sgsa.Reason,
sgsa.Remarks,
sgsa.LastModifiedDate, 
$LastModifyUserNameField as LastModifyUser,
sgsa.LeaveTypeID
FROM SUBJECT_GROUP_STUDENT_ATTENDANCE_$AcademicYearID AS sgsa
LEFT JOIN SUBJECT_GROUP_ATTEND_OVERVIEW_$AcademicYearID AS sgto ON sgsa.AttendOverviewID=sgto.AttendOverviewID 
LEFT JOIN SUBJECT_TERM_CLASS AS stc ON stc.SubjectGroupID=sgto.SubjectGroupID
LEFT JOIN SUBJECT_TERM AS st ON st.SubjectGroupID = stc.SubjectGroupID AND st.YearTermID='$YearTermID'
LEFT JOIN ASSESSMENT_SUBJECT AS ass_sub ON st.SubjectID = ass_sub.RecordID 
LEFT JOIN INTRANET_TIMETABLE_ROOM_ALLOCATION AS itra ON itra.RoomAllocationID=sgto.RoomAllocationID
LEFT join INTRANET_TIMETABLE_TIMESLOT AS itt ON itt.TimeSlotID=itra.TimeSlotID 
LEFT OUTER JOIN INTRANET_USER AS a ON sgsa.StudentID = a.UserID
LEFT JOIN INTRANET_USER AS u ON u.UserID=sgsa.LastModifiedBy
WHERE sgto.LessonDate='".$lc->Get_Safe_Sql_Query($TargetDate)."'
$cond
AND sgsa.AttendStatus='".$statusMapping['Absent']['code']."'
ORDER BY sgsa.CreateDate ASC
";


$result = $lc->returnArray($sql);

$StudentIDArr = Get_Array_By_Key($result,'StudentID');

//$toolbar  = $linterface->GET_LNK_NEW("javascript:newWindow('insert_record.php?TargetDate=".urlencode($TargetDate)."&DayType=".urlencode($DayType)."&routeID=".urlencode($routeID)."',12)","","","","",0);
$toolbar = $linterface->GET_LNK_EXPORT("export.php?TargetDate=".urlencode($TargetDate)."&TimeSlotID=".urlencode($TimeSlotID),"","","","",0);
$toolbar .= $linterface->GET_LNK_PRINT("javascript:openPrintPage()","","","","",0);
if ($plugin['eClassApp']) {
	$toolbar .= $linterface->GET_LNK_EXPORT("javascript:goExportMobile()",$Lang['MessageCenter']['ExportMobile'],"","","",0)."&nbsp;&nbsp;";
}


$table_attend = "";
$col_span = 11;
$words_absence = $lword->getWordListAttendance(PROFILE_TYPE_ABSENT);
$AbsentHasWord = sizeof($words_absence)!=0;
$words_late = $lword->getWordListAttendance(PROFILE_TYPE_LATE);
$LateHasWord = sizeof($words_late)!=0;
foreach($words_absence as $key=>$word)
	$words_absence[$key]= htmlspecialchars($word);
foreach($words_late as $key=>$word)
	$words_late[$key]= htmlspecialchars($word);

$x = $linterface->CONVERT_TO_JS_ARRAY($words_absence, "AbsentArrayWords", 1, 1);
$x .= $linterface->CONVERT_TO_JS_ARRAY($words_late, "LateArrayWords", 1, 1);

# Teacher Remark Preset
$teacher_remark_js = "";
$teacher_remark_words = $lc->GetTeacherRemarkPresetRecords(array());
$hasTeacherRemarkWord = sizeof($teacher_remark_words)!=0;
if($hasTeacherRemarkWord)
{
	$teacher_remark_words = Get_Array_By_Key($teacher_remark_words, 'ReasonText');
	foreach($teacher_remark_words as $key=>$word)
		$teacher_remark_words_temp[$key]= htmlspecialchars($word);
	$x .= $linterface->CONVERT_TO_JS_ARRAY($teacher_remark_words_temp, "jArrayWordsTeacherRemark", 1);
}

$x .= $linterface->GET_PRESET_LINK("ArrayWordsHide", "'hide_preset'", "","","''").'</div>';

$x .= "<table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">";
$x .= "<tr class=\"tabletop\">";
$x .= "<td width=\"1\" class=\"tabletoplink\">#</td>";
$x .= "<td width=\"15%\" class=\"tabletoplink\">$i_UserName</td>";
$x .= "<td width=\"10%\" class=\"tabletoplink\">".$Lang['StudentAttendance']['Gender']."</td>";
$x .= "<td width=\"15%\" class=\"tabletoplink\">".$Lang['SysMgr']['SubjectClassMapping']['SubjectGroupTitle']."</td>";
$x .= "<td width=\"10%\" class=\"tabletoplink\">".$Lang['eBooking']['Settings']['DefaultSettings']['BookingPeriod']['FieldTitle']['Period']."</td>";
$x .= "<td width=\"10%\" class=\"tabletoplink\">$i_SmartCard_Frontend_Take_Attendance_Status
					<br>
					<select id=\"drop_down_status_all\" name=\"drop_down_status_all\">
		        		<option value=\"".$statusMapping['Present']['code']."\">".$Lang['StudentAttendance']['Present']."</option>
		        		<option value=\"".$statusMapping['Late']['code']."\">".$Lang['StudentAttendance']['Late']."</option>
		        		<option value=\"".$statusMapping['Absent']['code']."\" SELECTED>".$Lang['StudentAttendance']['Absent']."</option>
		        		<option value=\"".$statusMapping['Outing']['code']."\">".$Lang['StudentAttendance']['Outing']."</option>
	        		</select>
					<br>
					".$linterface->Get_Apply_All_Icon("javascript:SetAllAttend();")."
				</td>";

$x .= "<td width=\"10%\" class=\"tabletoplink\">
				".$Lang['StudentAttendance']['LeaveType']."
			</td>";

$x .= "<td width=\"10%\" class=\"tabletoplink\">
				$i_Attendance_Reason
				<br>
				<input class=\"textboxnum\" type=\"text\" name=\"SetAllReason\" id=\"SetAllReason\" maxlength=\"255\" value=\"\">
				".$linterface->GET_PRESET_LIST("AbsentArrayWords", "SetAllReasonIcon", "SetAllReason")."
				<br>
				".$linterface->Get_Apply_All_Icon("javascript:SetAllReason();")."
			</td>";
$x .= "<td width=\"15%\" class=\"tabletoplink\">".$Lang['StudentAttendance']['iSmartCardRemark']."</td>";
$x .= "<td width=\"10%\" class=\"tabletoplink\">$i_StudentGuardian[MenuInfo]</td>";

if ($plugin['eClassApp']) {
	$x .= "<td width=\"5%\" class=\"tabletoplink\">".$Lang['AppNotifyMessage']['PushMessage']."<br /><input type=\"checkbox\" name=\"all_send_push_message\" onClick=\"$('.PushMessageCheckbox').attr('checked',this.checked);\"></td>";
	$col_span+=1;
}
$x .= "<td width=\"10%\" class=\"tabletoplink\">".$Lang['StudentAttendance']['LastModify']."</td>";
$x .= "</tr>\n";


if ($plugin['eClassApp']) {
	$push_message_button = "<a href=\"javascript:sendPushMessage()\" class=\"tabletool\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_sms.gif\" border=\"0\" align=\"absmiddle\">". $Lang['AppNotifyMessage']['PushMessage']  ."</a>&nbsp;";
	$table_tool .= "<td nowrap=\"nowrap\">
					$push_message_button
				</td>";
}

$reason_js_array = "";

$disable_color="#DFDFDF";  # background color for disabled text field
$enable_color="#FFFFFF";  # background color for enabled text field

$select_word = "";

$gender_word['M'] = $Lang['General']['Male'];
$gender_word['F'] = $Lang['General']['Female'];


$sql ="SELECT TypeID,TypeName FROM CARD_STUDENT_LEAVE_TYPE ORDER BY TypeSymbol,TypeName";
$rs_leavetype = $lc->returnArray($sql,2);



for($i=0; $i<sizeOf($result); $i++) {
	list($attend_overview_id, $user_id, $name, $gender, $timeslot_name, $subject_group_titleB5, $subject_group_titleEN, $subject_nameB5, $subject_nameEN, $attend_status, $reason, $remarks, $date_modified, $last_modify_user, $leave_type_id) = $result[$i];

	$css = ($i%2==0)?"tablerow1":"tablerow2";
	$select_status = "<select id=\"drop_down_status[$i]\" name=\"drop_down_status[]\" onChange=\"setReasonComp(this.value, this.form.reason$i, this.form.editAllowed$i, $i)\">\n";
	$select_status .= "<option value=\"".$statusMapping['Present']['code']."\">".$Lang['StudentAttendance']['Present']."</option>\n";
	$select_status .= "<option value=\"".$statusMapping['Late']['code']."\">".$Lang['StudentAttendance']['Late']."</option>\n";
	$select_status .= "<option value=\"".$statusMapping['Absent']['code']."\" SELECTED>".$Lang['StudentAttendance']['Absent']."</option>\n";
	$select_status .= "<option value=\"".$statusMapping['Outing']['code']."\">".$Lang['StudentAttendance']['Outing']."</option>\n";
	$select_status .= "</select>\n";
	$select_status .= "<input name=\"attend_overview_id[$i]\" type=\"hidden\" value=\"$attend_overview_id\">\n";
	$select_status .= "<input name=\"user_id[$i]\" type=\"hidden\" value=\"$user_id\">\n";

	$reason_comp = "<input type=\"text\" name=\"reason$i\" id=\"reason$i\" class=\"textboxnum\" maxlength=\"255\" value=\"".htmlspecialchars($reason,ENT_QUOTES)."\" style=\"background:$enable_color\">";

	$x .= "<tr class=\"$css\"><td class=\"tabletext\">".($i+1)."</td>";
	$x .= "<td class=\"tabletext\">$name</td>";
	$x .= "<td class=\"tabletext\">".$gender_word[$gender]."</td>";
	$x .= "<td class=\"tabletext\">".Get_Lang_Selection($subject_nameB5,$subject_nameEN) . ' - ' . Get_Lang_Selection($subject_group_titleB5,$subject_group_titleEN) ."</td>";
	$x .= "<td class=\"tabletext\">$timeslot_name</td>";
	$x .= "<td class=\"tabletext\">$select_status</td>";

	$selLeaveType = getSelectByArray($rs_leavetype," name=\"leaveType[$i]\" id=\"leaveType[$i]\"", $leave_type_id,0,0);
	$x .= "<td class=\"tabletext\">$selLeaveType</td>";

	$x .= "<td class=\"tabletext\">$reason_comp";
	$x .= $linterface->GET_PRESET_LIST("getReasons($i)", $i, "reason$i");
	$x .= "</td>";

	$remark_input = "<input type=\"text\" name=\"remark[]\" id=\"remark$i\" maxlength=\"255\" value=\"".htmlspecialchars($remarks, ENT_QUOTES)."\" />";
	if($hasTeacherRemarkWord)
	{
		$remark_input .= $linterface->GET_PRESET_LIST("getTeacherRemarkReasons($i)", '_teacher_remark_'.$i, 'remark'.$i);
	}

	$x .= "<td class=\"tabletext\">";
	$x .= $remark_input;
	$x .= "</td>";
	$x .= "<td class=\"tabletext\" align=\"center\">";
	$x .= "<a onMouseMove=\"moveObject('ToolMenu2', event);\" onmouseover=\"retrieveGuardianInfo($user_id);\" onmouseout=\"closeLayer('ToolMenu2');\" href=\"#\">";
	$x .= "<img src=\"$image_path/icons_guardian_info.gif\" border=\"0\" alt=\"$button_select\"></a></td>";
	if ($plugin['eClassApp']) {
		$x .= "<td class=\"tabletext\">";
		if (in_array($user_id, $studentWithParentUsingParentAppAry)) {
			$x .= "<input type=\"checkbox\" name=\"SendPushMessage[]\" id=\"SendPushMessage_".$i."\" class=\"PushMessageCheckbox\" value=\"".$user_id."\">";
		} else {
			$x .= "---";
		}
		$x .= "</td>";
	}
	$x .= "<td class=\"tabletext\">";
	if($date_modified != '' && $last_modify_user!=''){
		$x .= $last_modify_user.$Lang['General']['On'].$date_modified;
	}else{
		$x .= Get_String_Display('');
	}
	$x .= "</td>";
	$x .= "</tr>\n";
}


if (sizeof($result)==0)
{
	$x .= "<tr class=\"tablerow2\"><td class=\"tabletext\" height=\"40\" colspan=\"$col_span\" align=\"center\">$i_StudentAttendance_NoAbsentStudents</td></tr>\n";
}
if($plugin['eClassApp']){
	$x .= '<tr><td class="tabletextremark" colspan = "'.$col_span.'"><span>'.$Lang['MessageCenter']['ExportMobileRemarks'].'</span></td></tr>';
}

$x .= "</table>";

$table_attend = $x;

echo $LessonAttendUI->Include_JS_CSS();
?>

<script language="Javascript" src='/templates/tooltip.js'></script>
<script language="JavaScript" src="<?=$intranet_httppath?>/templates/ajax_yahoo.js"></script>
<script language="JavaScript" src="<?=$intranet_httppath?>/templates/ajax_connection.js"></script>
<style type="text/css">
	#ToolMenu{position:absolute; top: 0px; left: 0px; z-index:4; visibility:show;}
	#ToolMenu2{position:absolute; top: 0px; left: 0px; z-index:5; visibility:show;  width: 450px;}
</style>
<script language="JavaScript">
    isMenu = true;
</script>
<div id="ToolMenu" style="position:absolute; width=0px; height=0px; visibility:hidden"></div>
<div id="ToolMenu2" style="position:absolute; width=0px; height=0px; visibility:hidden"></div>

<form name="form3" method="post" action="" id="form3">
	<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tr>
			<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext" width="30%">
				<?=$Lang['General']['Date']?>
			</td>
			<td class="tabletext" width="80%">
				<?=$linterface->GET_DATE_PICKER("TargetDate", $TargetDate, "","yy-mm-dd","","","","OnDateChange(this);")?>
				<span style="color:red" id="TargetDateWarningLayer"></span>
			</td>
		</tr>

		<tr>
			<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext" width="30%">
				<?=$Lang['eBooking']['Settings']['DefaultSettings']['BookingPeriod']['FieldTitle']['Period']?>
			</td>
			<td class="tabletext" width="30%">
				<?=$selectLesson?>
			</td>
		</tr>
	</table>

	<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tr>
			<td class="dotline"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1" /></td>
		</tr>
		<tr>
			<td align="center">
				<?=$linterface->GET_ACTION_BTN($Lang['Btn']['View'], "button","Check_View_Form();")?>
			</td>
		</tr>
	</table>
</form>


<form name="form1" id="form1" method="post" action="absence_update.php" >
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>
				<table width="95%" border="0" cellpadding="0" cellspacing="0" align="center" onMouseMove="overhere()">
					<tr>
						<td align="left"><?= $toolbar ?></td>
						<td align="right"><?= $SysMsg ?></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td>
				<table width="95%" border="0" cellpadding="0" cellspacing="0" align="center">
					<tr>
						<td valign="bottom" align="left">

						</td>
						<td valign="bottom" align="right">
							<table border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td width="21"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_01.gif" width="21" height="23"></td>
									<td background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_02.gif">
										<table border="0" cellspacing="0" cellpadding="2">
											<tr>
												<?=$table_tool?>
											</tr>
										</table>
									</td>
									<td width="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_03.gif" width="6" height="23"></td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td>
				<?=$table_attend?>
			</td>
		</tr>
		<tr>
			<td>
				<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
					<tr>
						<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td>
				<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
					<tr>
						<td align="center" colspan="2">
							<? if (sizeof($result)!=0) { ?>
								<?= $linterface->GET_ACTION_BTN($button_save, "button", "Check_Form();") ?>
								<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
							<? } ?>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<input name="TargetDate" type="hidden" value="<?=escape_double_quotes($TargetDate)?>">
	<input name=PageLoadTime type=hidden value="<?=time()+1?>">
	<input type="hidden" name="TimeSlotID" value="<?=$TimeSlotID?>">
	<input type="hidden" name="this_page_title" value="<?=$Lang['LessonAttendance']['DailyLessonAbsenceOverview']?>">
	<input type="hidden" name="this_page_nav" value="PageLessonAttendance_DailyLessonAbsenceOverview">
	<input type="hidden" name="this_page" value="daily_lesson_absence/index.php?TimeSlotID=<?=urlencode($TimeSlotID)?>&TargetDate=<?php echo urlencode($TargetDate);?>">
	<input type="hidden" name="TemplateCode" value="STUDENT_ATTEND_ABSENCE">
	<?php for ($i=0; $i<sizeof($result); $i++) { ?>
		<input type="hidden" name="editAllowed<?=$i?>" value="1">
	<?php } ?>
</form>

<form id="form2" name="form2" method="post" action="" >
	<input type="hidden" name="StudentIDArr" value="<?=rawurlencode(serialize($StudentIDArr))?>">
</form>

<script language="JavaScript" type="text/javascript">
    function openPrintPage()
    {
        newWindow("absence_print.php?TimeSlotID=<?=urlencode($TimeSlotID)?>&TargetDate=<?=urlencode($TargetDate)?>",4);
    }
    function goExportMobile(){
        var url = '<?=$PATH_WRT_ROOT?>' + '/home/eAdmin/GeneralMgmt/MessageCenter/export_mobile.php';
        $('form#form2').attr('action', url ).submit();
        $('form#form2').attr('action', '' );
    }

    function sendPushMessage() {
        var SendPushMessage = document.getElementsByName('SendPushMessage[]');
        var SendPushMessageChecked = false;
        for (var i=0; i< SendPushMessage.length; i++) {
            if (SendPushMessage[i].checked) {
                SendPushMessageChecked = true;
                break;
            }
        }

        if (SendPushMessageChecked == true) {
            document.getElementById('form1').action = "../send_push_message.php";
            document.getElementById('form1').submit();
        }
        else
            alert("<?=$i_SMS_no_student_select?>");
    }


    function Check_Form() {
        AlertPost(document.form1,'absence_update.php','<?=$i_SmartCard_Confirm_Update_Attend?>');
    }

    function getReasons(pos)
    {
        var jArrayTemp = new Array();
        var obj2 = document.getElementById("drop_down_status["+pos+"]");
        if (obj2.selectedIndex == 2)
        {
            return AbsentArrayWords;
        }
        else if (obj2.selectedIndex == 1)
        {
            return LateArrayWords;
        }
        else
            return jArrayTemp;
    }

    function getTeacherRemarkReasons()
    {
        return jArrayWordsTeacherRemark;
    }

    function Check_View_Form() {
        if ($('#DPWL-TargetDate').html() == "") {
            document.getElementById('form3').submit();
        }
    }
    function OnDateChange(obj) {
        Check_View_Form();
    }
    function SetAllReason() {
        var StatusSelection = document.getElementsByName("drop_down_status[]");
        var ApplyReason = document.getElementById("SetAllReason").value;
        for (var i=0; i< StatusSelection.length; i++) {
            if (StatusSelection[i].selectedIndex == 2) // absence
            {
                document.getElementById('reason'+i).value = ApplyReason;
            }
        }
    }
    function SetAllAttend() {
        var AttendValue = document.getElementById("drop_down_status_all").value;
        var AttendSelection = document.getElementsByName('drop_down_status[]').length;
        for (var i=0; i< AttendSelection; i++) {
            document.getElementById('drop_down_status['+i+']').value = AttendValue;

            var targetUserID = document.getElementsByName('user_id['+i+']')[0].value;
            var targetReasonEle = document.getElementById('reason'+i);
            var targetEditEle = document.getElementsByName('editAllowed'+i)[0];

            setReasonComp(AttendValue, targetReasonEle, targetEditEle, i);
        }
    }

    function setReasonComp(value, txtComp, hiddenFlag, index)
    {
        if (value==<?=$statusMapping['Late']['code']?> || value==<?=$statusMapping['Absent']['code']?>)
        {
            txtComp.disabled = false;
            txtComp.style.background='<?=$enable_color?>';
            hiddenFlag.value = 1;

            if (value==<?=$statusMapping['Absent']['code']?>) {
                if (document.getElementById('leaveType['+index+']')) {
                    document.getElementById('leaveType['+index+']').style.display = "";
                }
            }
            else {
                if (document.getElementById('leaveType['+index+']')) {
                    document.getElementById('leaveType['+index+']').value = "";
                    document.getElementById('leaveType['+index+']').style.display = "none";
                }
            }
            if($('#office_remark'+index).length>0){
                $('#office_remark'+index).attr('disabled','');
            }
        }
        else
        {
            txtComp.disabled = true;
            txtComp.style.background='<?=$disable_color?>';
            txtComp.value="";
            hiddenFlag.value = 0;
            hideMenu('ToolMenu');

            if (document.getElementById('leaveType['+index+']')) {
                document.getElementById('leaveType['+index+']').value = "";
                document.getElementById('leaveType['+index+']').style.display = "none";
            }

            if($('#office_remark'+index).length>0){
                $('#office_remark'+index).attr('disabled','disabled');
            }
        }
    }
    var callback = {
        success: function ( o )
        {
            jChangeContent( "ToolMenu2", o.responseText );
        }
    }
    function retrieveGuardianInfo(UserID)
    {
        //FormObject.testing.value = 1;
        obj = document.form1;
        var myElement = document.getElementById("ToolMenu2");

        showMenu("ToolMenu2","<table border=1 width=300 bordercolorlight=#FBFDEA bordercolordark=#B3B692 cellpadding=2 cellspacing=0 bgcolor=#FBFDEA><tr><td>Loading</td></tr></table>");
        myElement.style.display = 'block';
        myElement.style.visibility = 'visible';
        YAHOO.util.Connect.setForm(obj);

        // page for processing and feedback
        var path = "/home/eAdmin/StudentMgmt/attendance/dailyoperation/absence/getGuardianInfo.php?targetUserID=" + UserID;
        var connectionObject = YAHOO.util.Connect.asyncRequest("POST", path, callback);
    }
    function moveObject( obj, e, moveLeft ) {
        var tempX = 0;
        var tempY = 0;
        var offset = 8;
        var objHolder = obj;
        var moveDistance = 0;

        obj = document.getElementById(obj);
        if (obj==null) {return;}

        if (document.all) {
            var ScrollLeft = document.documentElement.scrollLeft || document.body.scrollLeft;
            var ScrollHeight = document.documentElement.scrollTop || document.body.scrollTop;
            tempX = event.clientX + ScrollLeft;
            tempY = event.clientY + ScrollHeight;
        }
        else {
            tempX = e.pageX
            tempY = e.pageY
        }

        if(moveLeft == undefined) {
            moveDistance = 300;
        } else {
            moveDistance = moveLeft;
        }

        if (tempX < 0){tempX = 0} else {tempX = tempX - moveDistance}
        if (tempY < 0){tempY = 0} else {tempY = tempY}

        obj.style.top  = (tempY + offset) + 'px';
        obj.style.left = (tempX + offset) + 'px';
    }

    function closeLayer(LayerID) {
        var obj = document.getElementById(LayerID);
        obj.style.display = 'none';
        obj.style.visibility = 'hidden';
    }

</script>



<?php
$linterface->LAYOUT_STOP();

intranet_closedb();

?>

