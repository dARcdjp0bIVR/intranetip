<?php
//using by : henry
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

if(sizeof($_POST)==0) {
	header("Location: index.php");
	exit;
}

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();
$lc = new libcardstudentattend2();

if(sizeof($RecordID)>0)
	$list = implode(',',$RecordID);
	
$currentAcademicYear = Get_Current_Academic_Year_ID();
$name_field = getNameFieldByLang('USR.');	
$clsName = Get_Lang_Selection('yc.ClassTitleB5', 'yc.ClassTitleEN');

$sql = "SELECT 
			CONCAT($clsName,' - ',ycu.ClassNumber) as ClassNameNum,
			$name_field as name,
			RR.RecordDate,
			RR.StudentID
		FROM 
			CARD_STUDENT_PROFILE_RECORD_REASON RR INNER JOIN 
			INTRANET_USER USR ON (USR.UserID=RR.StudentID) LEFT OUTER JOIN
			YEAR_CLASS_USER ycu ON (ycu.UserID=RR.StudentID) LEFT OUTER JOIN
			YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID AND yc.AcademicYearID=$currentAcademicYear) 
		WHERE 
			RR.RecordID in ($list) AND 
			yc.AcademicYearID=$currentAcademicYear
		ORDER BY
			RR.RecordDate DESC
		";
$result = $lc->returnArray($sql, 3);

# Number of Demerit (selection menu)
$sql = "SELECT NumOfMerit, ConductScore FROM DISCIPLINE_MERIT_ITEM WHERE ItemID=178";
$temp = $ldiscipline->returnArray($sql,2);
$numMerit = ($temp[0][0]) ? $temp[0][0] : 0;
$conductScore = ($temp[0][1]) ? $temp[0][1] : 0;

if($intranet_session_language=="en") $s = "(s)";

# Merit Num "Standard" menu
$selectMeritNum0 .= "<SELECT name='meritNum0' id='meritNum0'>";
$AP_Interval = $ldiscipline->AP_Interval_Value ? $ldiscipline->AP_Interval_Value : 1;
for ($i=0; $i<=$ldiscipline->AwardPunish_MAX; $i=$i+$AP_Interval)
{
	$selectMeritNum0 .= "<OPTION value='".$i."'";
	$selectMeritNum0 .= ($i==$numMerit) ? " selected" : "";
	$selectMeritNum0 .= ">".$i."</OPTION>";
}
$selectMeritNum0 .= "</select> ";
$selectMeritNum0 .= $i_Merit_BlackMark.$s;
$selectMeritNum0 .= " <input type='button' name='applyAll' id='applyAll' value='$i_Discipline_Apply_To_All' onClick='applyToAll()'>";


# Merit Num menu
$selectMeritNum .= "<SELECT name='meritNum[]' id='meritNum[]'>";
$AP_Interval = $ldiscipline->AP_Interval_Value ? $ldiscipline->AP_Interval_Value : 1;
for ($i=0; $i<=$ldiscipline->AwardPunish_MAX; $i=$i+$AP_Interval)
{
	$selectMeritNum .= "<OPTION value='".$i."'";
	$selectMeritNum .= ($i==$numMerit) ? " selected" : "";
	$selectMeritNum .= ">".$i."</OPTION>";
}
$selectMeritNum .= "</select>";

# record content


$table = "<table width='95%' cellpadding='4' cellspacing='0' border='0' align='center'>";
$table .= "	<tr class='tablebluetop'>";
$table .= "		<td class='tabletopnolink' width='20%'>".$i_general_class."</td>";
$table .= "		<td class='tabletopnolink' width='20%'>".$i_general_name."</td>";
$table .= "		<td class='tabletopnolink' width='10%'>".$i_general_Days."</td>";
$table .= "		<td class='tabletopnolink' width='50%'>".$i_general_receive."</td>";
$table .= "	</tr>";

$stdID = "";

if(sizeof($RecordID)>1) {
	$table .= "	<tr class='tablerow2'>";
	$table .= "		<td class='tabletext'>&nbsp;</td>";
	$table .= "		<td class='tabletext'>&nbsp;</td>";
	$table .= "		<td class='tabletext'>&nbsp;</td>";
	$table .= "		<td class='tabletext'>$selectMeritNum0</td>";
	$table .= "	</tr>";
}

for($i=0; $i<sizeof($result); $i++) {
	$css = ($i%2==0) ? 1 : 2;
	$table .= "	<tr class='tablebluerow$css'>";
	$table .= "		<td class='tabletext'>".$result[$i][0]."</td>";
	$table .= "		<td class='tabletext'>".$result[$i][1]."</td>";
	$table .= "		<td class='tabletext'>".$lc->countSchoolDayDiff($result[$i][2], date("Y-m-d"))." $i_general_Days</td>";
	$table .= "		<td class='tabletext'>$selectMeritNum $i_Merit_BlackMark$s</td>";
	$table .= "	</tr>";
	$stdID .= ($stdID=="") ? $result[$i][3] : ",".$result[$i][3];
}
$table .= "</table>";


//$toolbar = $linterface->GET_LNK_EXPORT("export.php?form=$form&amount=$amount&on=$on","","","","",0);

$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageDailyOperation_MissParentLetter";

$TAGS_OBJ[] = array($Lang['StudentAttendance']['DailyOperation_MissParentLetter'], "", 0);

$MODULE_OBJ = $lc->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();

?>
<script>
<!--
function checkform() {
	var obj = document.form1;
	<? if(sizeof($RecordID)>1) { ?>
	var selectLength = document.form1.elements['meritNum[]'].length;
	var i;
	var total = 0;
	for(i=0; i<selectLength; i++) {
		if(document.form1.elements['meritNum[]'][i].value=='0') {
			total++;	
		}
	}	
	
	if(total!=0) {
		if(confirm("<?=$Lang['eAttendance']['PunishmentNullAlertMsg']?>")) {
			obj.action = "addConfirm_update.php";
			obj.submit();
		}
	} else {
	<? } ?>
		obj.action = "addConfirm_update.php";
		obj.submit();
	<? if(sizeof($RecordID)>1) { ?>
	}
	<? } ?>
}

function goBack() {
	document.form1.action = "index.php";
	document.form1.submit();
}

function applyToAll() {
	var selectLength = document.form1.elements['meritNum[]'].length;
	var i;
	
	for(i=0; i<selectLength; i++) {
		document.form1.elements['meritNum[]'][i].selectedIndex = document.form1.meritNum0.selectedIndex;
	}	
}
-->
</script>

<br />
<form name="form1" method="post" action="">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	</tr>
	<tr>
		<td>
			<?=$table?>
		</td>
	</tr>
	<tr>
		<td>
			<table width="98%" border="0" cellpadding="5" cellspacing="0" align="center">
	    	<tr>
        	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
        </tr>
		  </table>	
		</td>
	</tr>
	<tr>
		<td>
			<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr>
				    <td align="center" colspan="2">
						<?= $linterface->GET_ACTION_BTN($button_submit, "button", "checkform()") ?>
						<?= $linterface->GET_ACTION_BTN($button_reset, "reset") ?>
						<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "goBack()") ?>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<br />

<input type="hidden" name="submit_flag" id="submit_flag" value="1">
<input type="hidden" name="RecordID" id="RecordID" value="<?=$list?>">
<input type="hidden" name="stdID" id="stdID" value="<?=$stdID?>">
<input type="hidden" name="conductScore" id="conductScore" value="<?=$conductScore?>">
<input type="hidden" name="form" id="form" value="<?=$form?>">
<input type="hidden" name="amount" id="amount" value="<?=$amount?>">
<input type="hidden" name="on" id="on" value="<?=$on?>">
<input type="hidden" name="pageNo" value="<?=$li->pageNo; ?>" />
<input type="hidden" name="order" value="<?=$li->order; ?>" />
<input type="hidden" name="field" value="<?=$li->field; ?>" />
<input type="hidden" name="page_size_change" value="" />
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>"/>
</form>
<br />
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
