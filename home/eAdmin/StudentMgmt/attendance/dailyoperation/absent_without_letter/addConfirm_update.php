<?php
//using by : henry
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

if(sizeof($_POST)==0) {
	header("Location: index.php");
	exit;
}

$linterface = new interface_html();
$lc = new libcardstudentattend2();
$ldiscipline = new libdisciplinev12();

$semInfo = getAcademicYearAndYearTermByDate(date("Y-m-d"));

$itemInfo = $ldiscipline->getAPItemInfoBYItemID(178);
$itemText = $itemInfo['ItemName'];


$stdIDAry = explode(',', $stdID);
$RecordIDAry = explode(',', $RecordID);

# Semester
$semester = $semInfo[1];


for($i=0; $i<sizeof($stdIDAry); $i++) {
	$dataAry = array();
	$dataAry2 = array();
	
	# insert demerit record
	$dataAry['RecordDate'] = date("Y-m-d");
	$dataAry['AcademicYearID'] = Get_Current_Academic_Year_ID();
	$dataAry['Year'] = $ldiscipline->getAcademicYearNameByYearID($dataAry['AcademicYearID']);
	$dataAry['YearTermID'] = $semInfo[0];
	$dataAry['Semester'] = $semInfo[1];
	$dataAry['StudentID'] = $stdIDAry[$i];
	$dataAry['ItemID'] = 178;
	$dataAry['ItemText'] = $itemText;
	$dataAry['MeritType'] = -1;
	$dataAry['ProfileMeritType'] = -1;
	$dataAry['ProfileMeritCount'] = $meritNum[$i];
	$dataAry['ConductScoreChange'] = $conductScore;
	$dataAry['PICID'] = $UserID;
	$dataAry['RecordStatus'] = DISCIPLINE_STATUS_APPROVED;
	$dataAry['ApprovedDate'] = date("Y-m-d H:i:s");
	$dataAry['ApprovedBy'] = $UserID;

	$MeritRecordID = $ldiscipline->INSERT_MERIT_RECORD($dataAry);
	
	if($MeritRecordID) {
		# update CARD_STUDENT_PROFILE_RECORD_REASON
		$sql = "SELECT GM_RecordID_Str FROM CARD_STUDENT_PROFILE_RECORD_REASON WHERE RecordID=".$RecordIDAry[$i];
		$temp = $ldiscipline->returnVector($sql);
			
		
		$dataAry2['Last_Discipline_RecordDate'] = date("Y-m-d H:i:s");
		$dataAry2['GM_RecordID_Str'] = ($temp[0]=="") ? $MeritRecordID : $temp[0].",".$MeritRecordID;
		
		$lc->updateAttendanceRecordReason($RecordIDAry[$i], $dataAry2);
	}
}

intranet_closedb();

header("Location: index.php?xmsg=update&form=$form&amount=$amount&on=$on&submit_flag=1");
?>
<!--
<body onload="document.form1.submit()">
<form name="form1" action="index.php" method="post">
<input type="hidden" name="xmsg" id="xmsg" value="<?=$update?>">
<input type="hidden" name="form" id="form" value="<?=$form?>">
<input type="hidden" name="amount" id="amount" value="<?=$amount?>">
<input type="hidden" name="on" id="on" value="<?=$on?>">
<input type="hidden" name="submit_flag" id="submit_flag" value="1">
</form>
</body>
-->