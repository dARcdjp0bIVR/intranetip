<?php
// Editing by 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libstudentattendance_ui.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_PRIVILEGE"]["plugin"]['attendancelesson'] || !$_SESSION["SSV_USER_ACCESS"]["eAdmin-LessonAttendance"]) {
	intranet_closedb();
	header ("Location: /");
	exit();
}

$lc = new libcardstudentattend2();

if($_SERVER['REQUEST_METHOD'] != 'POST' || count($_POST['RecordDate'])==0 || count($_POST['StudentID'])==0)
{
	intranet_closedb();
	$_SESSION['LESSON_ATTENDANCE_LEAVE_RECORDS_RETURN_MSG'] = $Lang['General']['ReturnMessage']['UpdateUnsuccess'];
	header ("Location:index.php");
	exit();
}

$data = $_POST;
$RecordDate = $_POST['RecordDate'];
$StudentID = $_POST['StudentID'];

if($sys_custom['LessonAttendance_LaSalleCollege']){
	$leave_sessions = $_POST['LeaveSessions'];
	$dateToSessionAry = array();
	//if(isset($_POST['RecordID']) && $_POST['RecordID']>0 ){
	//	for($i=0;$i<count($RecordDate);$i++){
	//		$dateToSessionAry[$RecordDate[$i]] = explode(',',$leave_sessions[$i]);
	//	}
	//}else{
		for($i=0;$i<count($leave_sessions);$i++){
			$part = explode("_",$leave_sessions[$i]);
			if(!isset($dateToSessionAry[$part[0]])){
				$dateToSessionAry[$part[0]] = array();
			}
			$dateToSessionAry[$part[0]][] = $part[1];
		}
	//}
}

$lc->Start_Trans();

$resultAry = array();
foreach($RecordDate as $date){
	foreach($StudentID as $student_id)
	{
		$data['RecordDate'] = $date;
		$data['StudentID'] = str_replace('U','', trim($student_id));
		if($sys_custom['LessonAttendance_LaSalleCollege']){
			if(isset($dateToSessionAry[$date])){
				$data['LeaveSessions'] = implode(',',$dateToSessionAry[$date]);
			}else{
				$data['LeaveSessions'] = '';
			}
		}
		if($data['StudentID'] != ''){
			$resultAry[] = $lc->UpsertLessonAttendanceLeaveRecords($data);
		}
	}
}

if(!in_array(false, $resultAry)){
	$lc->Commit_Trans();
	$_SESSION['LESSON_ATTENDANCE_LEAVE_RECORDS_RETURN_MSG'] = $Lang['General']['ReturnMessage']['UpdateSuccess'];
}else{
	$lc->RollBack_Trans();
	$_SESSION['LESSON_ATTENDANCE_LEAVE_RECORDS_RETURN_MSG'] = $Lang['General']['ReturnMessage']['UpdateUnsuccess'];
}

intranet_closedb();
header("Location:index.php");
exit;
?>