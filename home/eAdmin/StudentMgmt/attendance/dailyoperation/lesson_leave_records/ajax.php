<?php
// Editing by 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libstudentattendance_ui.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_PRIVILEGE"]["plugin"]['attendancelesson'] || !$_SESSION["SSV_USER_ACCESS"]["eAdmin-LessonAttendance"]) {
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();

switch($task)
{
	case 'checkDuplicateRecords':
		
		$statusMap = $lc->LessonAttendanceStatusMapping();
		$records = $lc->GetLessonAttendanceLeaveRecords($_POST);
		$record_count = count($records);
		if($record_count > 0){
			$linterface = new interface_html();
			$x = '<table class="common_table_list_v30">';
				$x.= '<thead>';
					$x.= '<tr>
							<th class="num_check">#</th>';
						$x.= '<th style="width:10%">'.$Lang['General']['RecordDate'].'</th>';
						$x.= '<th style="width:20%">'.$Lang['Identity']['Student'].'</th>';
						$x.= '<th style="width:10%">'.$Lang['General']['Type'].'</th>';
					if($sys_custom['LessonAttendance_LaSalleCollege']){
						$x.= '<th style="width:10%">'.$Lang['LessonAttendance']['Lesson'].'</th>';
					}	
						$x.= '<th style="width:20%">'.$Lang['LessonAttendance']['Reason'].'</th>';
						$x.= '<th style="width:20%">'.$Lang['General']['LastUpdatedTime'].'</th>';
						$x.= '<th style="width:10%">'.$Lang['General']['LastUpdatedBy'].'</th>';
					$x.= '</tr>';
				$x.= '</thead>';
				$x.= '<tbody>';
				for($i=0;$i<$record_count;$i++) {
					$x .= '<tr>';
						$x .= '<td>'.($i+1).'</td>';
						$x .= '<td>'.$records[$i]['RecordDate'].'</td>';
						$x .= '<td>'.$records[$i]['StudentName'].'</td>';
						$x .= '<td>'.($records[$i]['LeaveType']==$statusMap['Absent']['code']? $statusMap['Absent']['text']:$statusMap['Outing']['text']).'</td>';
					if($sys_custom['LessonAttendance_LaSalleCollege']){
						$x .= '<td>'.Get_String_Display(str_replace(',',', ',$records[$i]['LeaveSessions'])).'</td>';
					}	
						$x .= '<td>'.$records[$i]['Reason'].'&nbsp;</td>';
						$x .= '<td>'.$records[$i]['ModifiedDate'].'&nbsp;</td>';
						$x .= '<td>'.$records[$i]['ModifiedName'].'&nbsp;</td>';
					$x .= '</tr>';
				}
				$x.= '</tbody>';
			$x.= '</table>';
			
			$warning_box = $linterface->Get_Warning_Message_Box('<span class="red">'.$Lang['LessonAttendance']['OverlappingRecords'].'</span>',$x,'');
			
			echo $warning_box;
		}
		
	break;
	
	
}

intranet_closedb();
?>