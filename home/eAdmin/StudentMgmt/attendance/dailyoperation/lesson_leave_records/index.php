<?php
// Editing by 
/*
 * 2016-08-11 (Carlos): Created.
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libstudentattendance_ui.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_PRIVILEGE"]["plugin"]['attendancelesson'] || !$_SESSION["SSV_USER_ACCESS"]["eAdmin-LessonAttendance"]) {
	intranet_closedb();
	header ("Location: /");
	exit();
}

$LessonAttendUI = new libstudentattendance_ui();
$linterface = new interface_html();

$field = trim(urldecode(stripslashes($_REQUEST['field'])));
$order = trim(urldecode(stripslashes($_REQUEST['order'])));
$pageNo = trim(urldecode(stripslashes($_REQUEST['pageNo'])));
$numPerPage = trim(urldecode(stripslashes($_REQUEST['numPerPage'])));

$arrCookies[] = array("ck_lesson_attendance_leave_records_page_size", "numPerPage");
$arrCookies[] = array("ck_lesson_attendance_leave_records_page_number", "pageNo");
$arrCookies[] = array("ck_lesson_attendance_leave_records_page_order", "order");
$arrCookies[] = array("ck_lesson_attendance_leave_records_page_field", "field");	
updateGetCookies($arrCookies);

if(!isset($field) || $field == '' || !in_array($field,array(1,2,3,4,5,6,7))) $field = 0; 
if(!isset($order) || $order == '' || !in_array($order,array(0,1))) $order = 0;
if(!isset($pageNo) || $pageNo == '') $pageNo = 1;

$pageSizeChangeEnabled = true;
if (isset($ck_lesson_attendance_leave_records_page_size) && $ck_lesson_attendance_leave_records_page_size != "") $page_size = $ck_lesson_attendance_leave_records_page_size;
$li = new libdbtable2007($field,$order,$pageNo);


$keyword = trim($keyword);
if(!isset($FromDate)){
	$FromDate = date("Y-m-d");
}
if(!isset($ToDate)){
	$ToDate = date("Y-m-d", getEndOfAcademicYear());
}

$statusMap = $LessonAttendUI->LessonAttendanceStatusMapping();
$type_field = "CASE r.LeaveType ";
foreach($statusMap as $value){
	$type_field .= "WHEN '".$value['code']."' THEN '".$value['text']."' ";
}
$type_field .= "END ";

$student_name_field = getNameFieldWithClassNumberByLang("u.");
$modifier_name_field = getNameFieldByLang2("u2.");
$archived_student_name_field = getNameFieldWithClassNumberByLang("au.");
//$archived_student_name_field_with_marker = getNameFieldInactiveUserIndicator($archived_student_name_field, "au.");

$sql = "SELECT 
			r.RecordDate,
			IF(u.UserID IS NULL,CONCAT($archived_student_name_field,'<span class=\"red\">^</span>'),$student_name_field) as StudentName,
			$type_field as LeaveType,";
if($sys_custom['LessonAttendance_LaSalleCollege']){
	$sql .= " REPLACE(r.LeaveSessions,',',', ') as LeaveSessions,";
}
  $sql .= " r.Reason,
			r.ModifiedDate,
			$modifier_name_field as ModifiedName,
			CONCAT('<input type=\"checkbox\" name=\"RecordID[]\" value=\"',r.RecordID,'\">') as CheckBox 
		FROM SUBJECT_GROUP_ATTENDANCE_LEAVE_RECORDS as r  
		LEFT JOIN INTRANET_USER as u ON u.UserID=r.StudentID 
		LEFT JOIN INTRANET_USER as u2 ON u2.UserID=r.ModifiedBy 
		LEFT JOIN INTRANET_ARCHIVE_USER as au ON au.UserID=r.StudentID 
		WHERE 1 ";
if($_POST['LeaveType']!=''){
	$sql .= " AND r.Leavetype='".$_POST['LeaveType']."' ";
}
if($_POST['FilterDate']==1){
	$sql .= " AND (r.RecordDate BETWEEN '".trim($_POST['FromDate'])."' AND '".trim($_POST['ToDate'])."') ";
}
if($keyword!='')
{
	$esc_keyword = $li->Get_Safe_Sql_Like_Query($keyword);
	$sql .= " AND (".Get_Lang_Selection("u.ChineseName","u.EnglishName")." LIKE '%$esc_keyword%' 
					OR u.ClassName LIKE '%$esc_keyword%' OR r.Reason LIKE '%$esc_keyword%') ";
}

$li->sql = $sql;
$li->IsColOff = "IP25_table";

$li->field_array = array("r.RecordDate","StudentName","LeaveType");
if($sys_custom['LessonAttendance_LaSalleCollege']){
	$li->field_array = array_merge($li->field_array, array("LeaveSessions"));
}
$li->field_array = array_merge($li->field_array, array("r.Reason","r.ModifiedDate","ModifiedName"));
$li->column_array = array(22,22,22);
if($sys_custom['LessonAttendance_LaSalleCollege']){
	$li->column_array = array_merge($li->column_array, array(22));
}
$li->column_array = array_merge($li->column_array, array(18,22,22));
$li->wrap_array = array(0,0,0,0,0,0,0,0);

$pos = 0;
$li->column_list .= "<th width=\"1\" class=\"num_check\">#</th>\n";
$li->column_list .= "<th>".$li->column($pos++, $Lang['General']['RecordDate'])."</th>\n";
$li->column_list .= "<th>".$li->column($pos++, $Lang['Identity']['Student'])."</th>\n";
$li->column_list .= "<th>".$li->column($pos++, $Lang['General']['Type'])."</th>\n";
if($sys_custom['LessonAttendance_LaSalleCollege']){
	$li->column_list .= "<th>".$li->column($pos++, $Lang['LessonAttendance']['Lesson'])."</th>\n";
}
$li->column_list .= "<th>".$li->column($pos++, $Lang['LessonAttendance']['Reason'])."</th>\n";
$li->column_list .= "<th>".$li->column($pos++, $Lang['General']['LastUpdatedTime'])."</th>\n";
$li->column_list .= "<th>".$li->column($pos++, $Lang['General']['LastUpdatedBy'])."</th>\n";
$li->column_list .= "<th width=\"1\">".$li->check("RecordID[]")."</th>\n";
$li->no_col = $pos+2;


$date_range_filter  = "&nbsp;";
$date_range_filter .= $linterface->Get_Checkbox("FilterDate", "FilterDate", 1, $FilterDate,'','','filterDateToggled(this.checked);');
$date_range_filter .= "&nbsp;";
$date_range_filter .= $Lang['General']['From'];
$date_range_filter .= "&nbsp;";
$date_range_filter .= $linterface->GET_DATE_PICKER("FromDate", $FromDate, ($FilterDate==1?'':' disabled'));
$date_range_filter .= "&nbsp;";
$date_range_filter .= $Lang['General']['To'];
$date_range_filter .= "&nbsp;";
$date_range_filter .= $linterface->GET_DATE_PICKER("ToDate", $ToDate, ($FilterDate==1?'':' disabled'));
$date_range_filter .= "&nbsp;";
$date_range_filter .= $linterface->GET_SMALL_BTN($Lang['Btn']['Apply'], "button", "document.form1.submit();","submitDateBtn",($FilterDate==1?'':' disabled'));

$leave_types = array(
	array($statusMap['Absent']['code'],$statusMap['Absent']['text']),
	array($statusMap['Outing']['code'],$statusMap['Outing']['text'])
);

$leave_type_selection = $linterface->GET_SELECTION_BOX($leave_types, ' id="LeaveType" name="LeaveType" onchange="document.form1.submit();" ', $Lang['General']['All'], $LeaveType, false);

$tool_buttons = array();
$tool_buttons[] = array('edit',"javascript:checkEdit(document.form1,'RecordID[]','edit.php')");
$tool_buttons[] = array('delete',"javascript:checkRemove(document.form1,'RecordID[]','delete.php')");

$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageDailyOperation_LessonAttendanceLeaveRecords";

### Title ###
$TAGS_OBJ[] = array($Lang['LessonAttendance']['StudentAbsenceAndOutingRecordsForLessonAttendance'],"",0);
$MODULE_OBJ = $LessonAttendUI->GET_MODULE_OBJ_ARR();
//$Msg = urldecode($Msg);
if(isset($_SESSION['LESSON_ATTENDANCE_LEAVE_RECORDS_RETURN_MSG']))
{
	$Msg = $_SESSION['LESSON_ATTENDANCE_LEAVE_RECORDS_RETURN_MSG'];
	unset($_SESSION['LESSON_ATTENDANCE_LEAVE_RECORDS_RETURN_MSG']);
}
$linterface->LAYOUT_START($Msg); 
?>
<br />
<form id="form1" name="form1" method="post" action="">
	<div class="content_top_tool">
		<div class="Conntent_tool">
			<?=$linterface->GET_LNK_NEW("edit.php", $Lang['Btn']['New'], $__ParOnClick="", $__ParOthers="", $__ParClass="", $__useThickBox=0);?>
			<?=$linterface->GET_LNK_IMPORT("import.php",$Lang['Btn']['Import'],"","","",0)?>
		</div>
		<div class="Conntent_search"><input type="text" id="keyword" name="keyword" value="<?=intranet_htmlspecialchars(stripslashes($keyword))?>" onkeyup="GoSearch(event);"></div>
		<br style="clear:both;">
	</div>
	<br style="clear:both;">
	<div class="table_filter">
		<?=$leave_type_selection?>
		<?=$date_range_filter?>
	</div>
	<br style="clear:both">
    <div style="height: 15px"></div>
	<?=$linterface->Get_DBTable_Action_Button_IP25($tool_buttons)?>
	<?=$li->display();?>
	<input type="hidden" id="pageNo" name="pageNo" value="<?=$li->pageNo?>">
	<input type="hidden" id="order" name="order" value="<?=$li->order?>">
	<input type="hidden" id="field" name="field" value="<?=$li->field?>">
	<input type="hidden" id="page_size_change" name="page_size_change" value="">
	<input type="hidden" id="numPerPage" name="numPerPage" value="<?=$li->page_size?>">
</form>	
<br /><br />
<script type="text/javascript">
function GoSearch(evt)
{
	var key = evt.which || evt.charCode || evt.keyCode;
	
	if (key == 13) // enter
		document.form1.submit();
	else
		return false;
}

function filterDateToggled(checked)
{
	if(checked){
		$('#FromDate').attr('disabled',false);
		$('#ToDate').attr('disabled',false);
		$('#submitDateBtn').attr('disabled',false);
	}else{
		$('#FromDate').attr('disabled',true);
		$('#ToDate').attr('disabled',true);
		$('#submitDateBtn').attr('disabled',true);
	}
}
</script>
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>