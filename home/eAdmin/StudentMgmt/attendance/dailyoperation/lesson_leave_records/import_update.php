<?php
// Editing by 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libstudentattendance_ui.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_PRIVILEGE"]["plugin"]['attendancelesson'] || !$_SESSION["SSV_USER_ACCESS"]["eAdmin-LessonAttendance"]) {
	intranet_closedb();
	header ("Location: /");
	exit();
}

$LessonAttendUI = new libstudentattendance_ui();
$linterface = new interface_html();
$limport = new libimporttext();
$lf = new libfilesystem();

$cur_sessionid = md5(session_id());

$sql = "INSERT INTO SUBJECT_GROUP_ATTENDANCE_LEAVE_RECORDS (RecordDate,StudentID,LeaveType".($sys_custom['LessonAttendance_LaSalleCollege']?",LeaveSessions":"").",Reason,InputDate,ModifiedDate,InputBy,ModifiedBy) SELECT RecordDate,StudentID,LeaveType".($sys_custom['LessonAttendance_LaSalleCollege']?",LeaveSessions":"").",Reason,NOW() as InputDate,NOW() as ModifiedDate,'".$_SESSION['UserID']."' as InputBy,'".$_SESSION['UserID']."' as ModifiedBy FROM SUBJECT_GROUP_ATTENDANCE_IMPORT_LEAVE_RECORDS WHERE SessionID='$cur_sessionid'";
$success = $LessonAttendUI->db_db_query($sql);

$rows_affected = $LessonAttendUI->db_affected_rows();

$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageDailyOperation_LessonAttendanceLeaveRecords";

$PAGE_NAVIGATION = array();
$PAGE_NAVIGATION[] = array (
			$Lang['LessonAttendance']['StudentAbsenceAndOutingRecordsForLessonAttendance'],'index.php'
		);
$PAGE_NAVIGATION[] = array (
			$Lang['Btn']['Import'],''
		);

# step information
$STEPS_OBJ[] = array($Lang['General']['ImportArr']['ImportStepArr']['SelectCSVFile'], 0);
$STEPS_OBJ[] = array($Lang['General']['ImportArr']['ImportStepArr']['CSVConfirmation'], 0);
$STEPS_OBJ[] = array($Lang['General']['ImportArr']['ImportStepArr']['ImportResult'], 1);


### Title ###
$TAGS_OBJ[] = array($Lang['LessonAttendance']['StudentAbsenceAndOutingRecordsForLessonAttendance'],"",0);
$MODULE_OBJ = $LessonAttendUI->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();
?>
<br />
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td><?=$linterface->GET_NAVIGATION($PAGE_NAVIGATION)?></td>
	</tr>
	<tr>
		<td><?=$linterface->GET_STEPS($STEPS_OBJ) ?></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td align="center">
			<?=$rows_affected.' ' . $Lang['SysMgr']['FormClassMapping']['RecordsImportedSuccessfully'] ?>
		</td>
	</tr>
	<tr>
		<td class="dotline">
			<img src="/images/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1" />
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td align="center">
			<?=$linterface->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "window.location='import.php';")?>
		</td>
	</tr>
</table>
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>