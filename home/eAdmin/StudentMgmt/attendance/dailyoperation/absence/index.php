<?php
// using 
# 20061113 (Kenneth): Add confirmation indicator
/*
 * 2014-07-24 Carlos: Change Period selection to radio checkboxes
 */

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libstudentattendance_ui.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();
$StudentAttendUI = new libstudentattendance_ui();
$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageDailyOperation_ViewAbsenceStatus";

$linterface = new interface_html();

$lc->retrieveSettings();

$TAGS_OBJ[] = array($i_SmartCard_DailyOperation_ViewAbsenceStatus, "", 0);
$MODULE_OBJ = $lc->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

#$PAGE_NAVIGATION[] = array($display_period);

if ($msg == 1) $SysMsg = $linterface->GET_SYS_MSG("add");
if ($msg == 2) $SysMsg = $linterface->GET_SYS_MSG("update");
?>
<br />
<?
echo $StudentAttendUI->Get_Confirm_Selection_Form("form1","show.php",CARD_STATUS_ABSENT,"","",1);
?>
<br />
<?
$linterface->LAYOUT_STOP();
print $linterface->FOCUS_ON_LOAD("form1.TargetDate");
intranet_closedb();
?>