<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();


### Set Date from previous page
$ts_record = strtotime($TargetDate);
if ($ts_record == -1)
{
    $TargetDate = date('Y-m-d');
    $ts_record = strtotime($TargetDate);
}
$txt_year = date('Y',$ts_record);
$txt_month = date('m',$ts_record);
$txt_day = date('d',$ts_record);

###period
switch ($DayType)
{
	case PROFILE_DAY_TYPE_AM: $display_period = $i_DayTypeAM;
	          break;
	case PROFILE_DAY_TYPE_PM: $display_period = $i_DayTypePM;
	          break;
	default : $display_period = $i_DayTypeAM;
	          $DayType = PROFILE_DAY_TYPE_AM;
	          break;
}

$lexport = new libexporttext();
$export_content_final = $lc->generateMedicalReasonSummary($TargetDate);
if($export_content_final!=-1){
	intranet_closedb();
	$filename = "medical_report_".$BroadlearningClientID."-".$TargetDate.".csv";
	$lexport->EXPORT_FILE($filename, $export_content_final);
}else{
intranet_closedb();
header("Location: show.php?TargetDate=".$TargetDate."&DayType=".$DayType."&medical_msg=1");
}
?>
