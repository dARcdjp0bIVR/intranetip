<?php
// using 
##################################### Change Log #####################################################
# 2015-02-11 Carlos: Added column [Office Remark]
# 2014-12-08 Omas:	Add new column Hand-in Prove Document Status
# 2014-03-17 Carlos: Display [Waive Absence] for $sys_custom['StudentAttendance_WaiveAbsent']
# 2013-04-16 Carlos: Add CARD_STUDENT_DAILY_REMARK.DayType
# 2013-03-11 Carlos: Add back teacher's remark field
# 2011-12-21 Carlos: Added data column Gender
# 2010-03-25 Carlos: Customization - Added textbox Remark and checkbox Has handin parent letter
######################################################################################################
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();

$linterface = new interface_html();

include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_header.php");


### Set Date from previous page
$ts_record = strtotime($TargetDate);
if ($ts_record == -1)
{
    $TargetDate = date('Y-m-d');
    $ts_record = strtotime($TargetDate);
}
$txt_year = date('Y',$ts_record);
$txt_month = date('m',$ts_record);
$txt_day = date('d',$ts_record);

###period
switch ($DayType)
{
    case PROFILE_DAY_TYPE_AM: $display_period = $i_DayTypeAM;
              break;
    case PROFILE_DAY_TYPE_PM: $display_period = $i_DayTypePM;
              break;
    default : $display_period = $i_DayTypeAM;
              $DayType = PROFILE_DAY_TYPE_AM;
              break;
}

### build card log table
$card_log_table_name = "CARD_STUDENT_DAILY_LOG_".$txt_year."_".$txt_month;

if ($DayType==PROFILE_DAY_TYPE_AM)               # Check AM Late
{
$sql  = "SELECT b.RecordID, a.UserID,
		      	".getNameFieldByLang("a.")."as name,
		      	a.ClassName,
		      	a.ClassNumber,
			  	a.Gender,
		      	b.InSchoolTime,
		      	IF(b.InSchoolStation IS NULL, '-', b.InSchoolStation),
		      	b.AMStatus,
		      	c.Reason,
		      	c.RecordStatus,
				d.Remark,
				IF(c.OfficeRemark IS NULL AND TRIM(f.Remark)!='',f.Remark,c.OfficeRemark) as OfficeRemark,
				c.Remark as AbsenceRemark,
				c.HandinLetter, 
				IF(c.DocumentStatus =  1 AND c.DocumentStatus IS NOT NULL,'".$Lang['General']['Yes']."','".$Lang['General']['No']."') as DocumentStatus 
				";
if($sys_custom['StudentAttendance_WaiveAbsent']){
	$sql .= ",c.WaiveAbsent ";
}
$sql.="FROM 
          	$card_log_table_name as b
			LEFT OUTER JOIN INTRANET_USER as a ON b.UserID = a.UserID
			LEFT OUTER JOIN CARD_STUDENT_PROFILE_RECORD_REASON as c
				ON c.StudentID = a.UserID AND c.RecordDate = '$TargetDate' AND c.DayType = $DayType
				AND c.RecordType = '".PROFILE_TYPE_ABSENT."' 
			LEFT JOIN CARD_STUDENT_DAILY_REMARK AS d 
				ON (b.UserID = d.StudentID AND d.RecordDate='$TargetDate' AND d.DayType='$DayType') 
			LEFT JOIN CARD_STUDENT_PRESET_LEAVE f 
					ON 
						f.StudentID = a.UserID 
						AND f.RecordDate = '$TargetDate' 
						AND f.DayPeriod = '".$DayType."' 
        WHERE b.DayNumber = '$txt_day'
     			AND (b.AMStatus = '".CARD_STATUS_ABSENT."' OR b.AMStatus = '".CARD_STATUS_OUTING."')
          AND a.RecordType = 2 AND a.RecordStatus IN (0,1,2)
        ORDER BY a.ClassName, a.ClassNumber+0, a.EnglishName
";
}
else     # Check PM Late
{
$sql  = "SELECT 
			b.RecordID, a.UserID,
          	".getNameFieldByLang("a.")."as name,
          	a.ClassName,
          	a.ClassNumber,
		  	a.Gender,
          	b.LunchBackTime,
          	IF(b.LunchBackStation IS NULL, '-', b.LunchBackStation),
          	b.PMStatus,
          	c.Reason,
          	c.RecordStatus,
			d.Remark,
			IF(c.OfficeRemark IS NULL AND TRIM(f.Remark)!='',f.Remark,c.OfficeRemark) as OfficeRemark,
			c.Remark as AbsenceRemark,
			c.HandinLetter,
			IF(c.DocumentStatus =  1 AND c.DocumentStatus IS NOT NULL,'".$Lang['General']['Yes']."','".$Lang['General']['No']."') as DocumentStatus 
			 ";
if($sys_custom['StudentAttendance_WaiveAbsent']){
	$sql .= ",c.WaiveAbsent ";
}
$sql .= "FROM
          	$card_log_table_name as b
			LEFT OUTER JOIN INTRANET_USER as a ON b.UserID = a.UserID
			LEFT OUTER JOIN CARD_STUDENT_PROFILE_RECORD_REASON as c
          		ON c.StudentID = a.UserID AND c.RecordDate = '$TargetDate' AND c.DayType = $DayType
             		AND c.RecordType = '".PROFILE_TYPE_ABSENT."' 
			LEFT JOIN CARD_STUDENT_DAILY_REMARK AS d 
				ON (b.UserID = d.StudentID AND d.RecordDate='$TargetDate' AND d.DayType='$DayType') 
			LEFT JOIN CARD_STUDENT_PRESET_LEAVE f 
					ON 
						f.StudentID = a.UserID 
						AND f.RecordDate = '$TargetDate' 
						AND f.DayPeriod = '".$DayType."' 
		WHERE 
					b.DayNumber = '$txt_day'
          AND (b.PMStatus = '".CARD_STATUS_ABSENT."' OR b.PMStatus = '".CARD_STATUS_OUTING."')
          AND a.RecordType = 2 AND a.RecordStatus IN (0,1,2) 
			
        ORDER BY a.ClassName, a.ClassNumber+0, a.EnglishName
";

}

$result = $lc->returnArray($sql,16);
$table_attend = "";
$col_span = 12;
$x = "<table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\" class=\"eSporttableborder\">";
$x .= "<tr>";
$x .= "<td class=\"eSporttdborder eSportprinttabletitle\">#</td>";
$x .= "	<td class=\"eSporttdborder eSportprinttabletitle\" nowrap>$i_UserName</td>
				<td class=\"eSporttdborder eSportprinttabletitle\" nowrap>$i_ClassName</td>
				<td class=\"eSporttdborder eSportprinttabletitle\" nowrap>$i_UserClassNumber</td>
				<td class=\"eSporttdborder eSportprinttabletitle\" nowrap>".$Lang['StudentAttendance']['Gender']."</td>
				<td class=\"eSporttdborder eSportprinttabletitle\" nowrap>$i_SmartCard_Frontend_Take_Attendance_Status</td>";
$x .= "	<td class=\"eSporttdborder eSportprinttabletitle\" nowrap>$i_SmartCard_Frontend_Take_Attendance_Waived</td>";
$x .= "	<td class=\"eSporttdborder eSportprinttabletitle\" nowrap>".$Lang['StudentAttendance']['ProveDocument']." </td>";
if($sys_custom['StudentAttendance_WaiveAbsent']){
	$x .= "<td class=\"eSporttdborder eSportprinttabletitle\" nowrap>".$Lang['StudentAttendance']['WaiveAbsence']."</td>";
	$col_span += 1;
}
$x .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_Attendance_Reason</td>";
$x .= "<td class=\"eSporttdborder eSportprinttabletitle\">".$Lang['StudentAttendance']['iSmartCardRemark']."</td>";	
$x .= "<td class=\"eSporttdborder eSportprinttabletitle\">".$Lang['StudentAttendance']['OfficeRemark']."</td>";	
if($sys_custom['StudentAttendance_AbsenceRemark'])
{
	$x .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_Attendance_Remark</td>";
	$col_span+=1;
}
$x .= "<td class=\"eSporttdborder eSportprinttabletitle\">".$i_StudentGuardian['MenuInfo']."</td>";

if($sys_custom['hku_medical_research'])
{
	$x .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_MedicalReasonTitle</td>";
	
	# Retrieve Stored Reason
	# $DayType
	$sql  = "SELECT a.UserID, b.MedicalReasonType ";
	$sql .= "FROM INTRANET_USER as a ";
	$sql .= "LEFT OUTER JOIN SPECIAL_STUDENT_ABSENCE_MEDICAL_REASON as b ";
	$sql .= "ON b.RecordDate = '$TargetDate' AND b.DayType = '$DayType' AND a.UserID = b.StudentID";
	
	$temp = $lc->returnArray($sql,2);
	$data_medical = build_assoc_array($temp);
	
	$col_span+=1;
}
if($sys_custom['StudentAttendance_AbsenceHandinLetter'])
{
	$x .= "<td class=\"eSporttdborder eSportprinttabletitle\">".$Lang['StudentAttendance']['HasHandinParentLetter']."</td>";
	$col_span+=1;
}
$x .= "</tr>";

$gender_word['M'] = $Lang['General']['Male'];
$gender_word['F'] = $Lang['General']['Female'];

for($i=0; $i<sizeOf($result); $i++)
{
	list($record_id, $user_id, $name,$class_name,$class_number, $gender, $in_school_time, $in_school_station, $old_status, $reason,$record_status, $remark, $office_remark, $absence_remark, $handin_letter,$DocumentStatus) = $result[$i];
	$str_status = ($old_status==CARD_STATUS_ABSENT?$i_StudentAttendance_Status_Absent:$i_StudentAttendance_Status_Outing);
	$str_reason = $reason;
	$waived_option = $record_status == 1? $i_general_yes:$i_general_no;
	
	
	$x .= "<tr>";
	$x .= "<td class=\"eSporttdborder eSportprinttext\">".($i+1)."</td>";
	$x .= "<td class=\"eSporttdborder eSportprinttext\" nowrap>$name</td>";
	$x .= "<td class=\"eSporttdborder eSportprinttext\" nowrap>$class_name</td>";
	$x .= "<td class=\"eSporttdborder eSportprinttext\" nowrap>$class_number&nbsp;</td>";
	$x .= "<td class=\"eSporttdborder eSportprinttext\" nowrap>".$gender_word[$gender]."</td>";
	$x .= "<td class=\"eSporttdborder eSportprinttext\" nowrap>$str_status</td>";
	$x .= "<td class=\"eSporttdborder eSportprinttext\" nowrap>$waived_option</td>";
	$x .= "<td class=\"eSporttdborder eSportprinttext\" nowrap>$DocumentStatus</td>";
	if($sys_custom['StudentAttendance_WaiveAbsent']){
		$waive_absent = $result[$i]['WaiveAbsent'] == 1? $Lang['General']['Yes'] : $Lang['General']['No'];
		$x .= "<td class=\"eSporttdborder eSportprinttext\" nowrap>$waive_absent</td>";
	}
	$x .= "<td class=\"eSporttdborder eSportprinttext\">$str_reason&nbsp;</td>";
	$x .= "<td class=\"eSporttdborder eSportprinttext\">$remark&nbsp;</td>";
	$x .= "<td class=\"eSporttdborder eSportprinttext\">$office_remark&nbsp;</td>";
	if($sys_custom['StudentAttendance_AbsenceRemark']) $x .= "<td class=\"eSporttdborder eSportprinttext\">".trim(htmlspecialchars($absence_remark,ENT_QUOTES))."&nbsp;</td>";
	$layer_content = "<td class=\"eSporttdborder eSportprinttext\">";

	// Get guardian information
	
	$main_content_field = "IF(EnName IS NOT NULL AND EnName !='' AND ChName IS NOT NULL AND ChName !='', IF(IsMain = 1, CONCAT('* ', EnName,' / ',ChName), CONCAT(EnName,' / ',ChName)), IF(EnName IS NOT NULL AND EnName!='',EnName,ChName))";
	$main_content_field = "IF($main_content_field='' OR $main_content_field IS NULL,'-',$main_content_field)";
	
	$sql = "SELECT
					  $main_content_field,
					  Relation,
					  Phone,
					  EmPhone,
					  IsMain
					FROM
					  $eclass_db.GUARDIAN_STUDENT
					WHERE
					  UserID = $user_id
					ORDER BY
					  IsMain DESC, Relation ASC
					";
	$temp = $lc->returnArray($sql,4);
	
	if (sizeof($temp)==0)
		$layer_content .= "--";
	else
	{
		
    for($j=0; $j<sizeof($temp); $j++)
    {
			list($name, $relation, $phone, $em_phone) = $temp[$j];
			$no = $j+1;
			$layer_content .= "$name ($ec_guardian[$relation]) ($i_StudentGuardian_Phone) $phone ($i_StudentGuardian_EMPhone) $em_phone <br>\n";
	  }
	}
	$layer_content .= '</td>';
	
    $x .= $layer_content;
	
	if ($sys_custom['hku_medical_research']) {
		$x .= "<td class=\"eSporttdborder eSportprinttext\">";
		for ($j=0; $j<sizeof($i_MedicalReasonName); $j++) {
			list($t_reasonType, $t_reasonName) = $i_MedicalReasonName[$j];
			$t_selected_value = $data_medical[$user_id];
			if ($t_selected_value==$t_reasonType) {
				$x .= $t_reasonName;
				break;
			}
		}
		$x .= "</td>";
	}
	if($sys_custom['StudentAttendance_AbsenceHandinLetter']) $x .= "<td class=\"eSporttdborder eSportprinttext\">".($handin_letter==1?$i_general_yes:$i_general_no)."</td>";
	$x .= "</tr>";    
}
if (sizeof($result)==0)
{
    $x .= "<tr>";
    $x .= "<td class=\"eSportprinttext\" colspan=\"$col_span\" align=\"center\">$i_StudentAttendance_NoAbsentStudents</td>";
    $x .= "</tr>";
}
$x .= "</table>\n";
$table_attend = $x;
$i_title = "$i_SmartCard_DailyOperation_ViewAbsenceStatus $TargetDate ($display_period)";
?>
<table width='100%' align='center' class='print_hide' border=0>
	<tr>
		<td align='right'><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></td>
	</tr>
</table>
<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
		<td class="eSportprinttext"><?=$i_title?></td>
	</tr>
</table>
<?=$table_attend?>
<?
include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_footer.php");
intranet_closedb();
?>