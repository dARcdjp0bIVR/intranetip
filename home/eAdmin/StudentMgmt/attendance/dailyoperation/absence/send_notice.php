<?php
// Modifying by: 
/*
 * 2016-01-11 (Carlos): Added the missing cancel button action. 
 */
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libstudentattendance_ui.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

###period
switch ($DayType)
{
  case PROFILE_DAY_TYPE_AM: 
  	$display_period = $i_DayTypeAM;
    break;
  case PROFILE_DAY_TYPE_PM: 
  	$display_period = $i_DayTypePM;
    break;
  default : 
  	$display_period = $i_DayTypeAM;
  	$DayType = PROFILE_DAY_TYPE_AM;
    break;
}

$TargetUsers = $_REQUEST['SendNotice'];

$linterface = new interface_html();
$StudentAttendUI = new libstudentattendance_ui();


$TAGS_OBJ[] = array($i_SmartCard_DailyOperation_ViewAbsenceStatus, "", 0);
$MODULE_OBJ = $StudentAttendUI->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

$PAGE_NAVIGATION[] = array("$TargetDate ($display_period)");
$PAGE_NAVIGATION[] = array($Lang['StudentAttendance']['SendNotice']);

$TemplateList = $StudentAttendUI->Get_eNotice_Template_By_Category("Absent");
$TemplateSelect = '<select name="TemplateID" id="TemplateID">';
for ($i=0; $i< sizeof($TemplateList); $i++) {
	List($TemplateID,$Title) = $TemplateList[$i];
	$TemplateSelect .= '<option value="'.$TemplateID.'">'.$Title.'</option>';
}
$TemplateSelect .= '</select>';
$TemplateSelect .= '&nbsp;<a href="#" class="tablelink" target="_blank" onclick="newWindow(\''.$PATH_WRT_ROOT.'home/eAdmin/StudentMgmt/attendance/dailyoperation/absence/preview_notice.php?TemplateID=\'+document.getElementById(\'TemplateID\').options[document.getElementById(\'TemplateID\').selectedIndex].value,10); return false;">'.$Lang['StudentAttendance']['eNoticeTemplatePreview'].'</a>';

############################################################
## Student List
############################################################
$StudentInfoTable = "<table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">\n";
$StudentInfoTable .= "<tr class=\"tabletop\">
<td class=\"tabletoplink\" width=\"25%\">$i_UserName</td>
<td class=\"tabletoplink\" width=\"20%\">$i_ClassName</td>
<td class=\"tabletoplink\" width=\"5%\">$i_ClassNumber</td>
</tr>\n";

$css = "tablerow1";
$UserDetail = $StudentAttendUI->Get_UserInfo_By_ID($TargetUsers);
for ($i=0; $i< sizeof($UserDetail); $i++) {
	$css = ($css=="tablerow2")?"tablerow1":"tablerow2";
	$StudentInfoTable .= "<tr class=$css>";
	$StudentInfoTable .= "<td class=\"tabletext\">". $UserDetail[$i]['Name'] ."</td>";
	$StudentInfoTable .= "<td class=\"tabletext\">". $UserDetail[$i]['ClassName'] ."</td>";
	$StudentInfoTable .= "<td class=\"tabletext\">". $UserDetail[$i]['ClassNumber'] ."</td>";
	$StudentInfoTable .= "</tr>\n";
	$StudentHiddenField .= '<input type="hidden" name="StudentID[]" value="'.$UserDetail[$i]['UserID'].'">';
}
$StudentInfoTable .= "</table>\n";
?>
<form name="form1" method="POST" action="send_notice_process.php">

<br />
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
<tr>
	<td class="navigation">
		<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tr><td><?=$linterface->GET_NAVIGATION($PAGE_NAVIGATION)?></td></tr>
		</table>
	</td>
</tr>

<tr>
	<td>
		<br />
		<table align="center" width="95%" border="0" cellpadding="5" cellspacing="0">
			<tr>
				<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle">
					<?=$Lang['StudentAttendance']['eNoticeTemplateName']?>
				</td>
				<td class="tabletext">
					<?=$TemplateSelect?>
				</td>
			</tr>
			<tr>
				<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle">
					<?=$Lang['StudentAttendance']['AdditionInfo']?>
				</td>
				<td class="tabletext">
					<?
					include_once($PATH_WRT_ROOT."templates/html_editor/fckeditor.php");
					$oFCKeditor = new FCKeditor('AdditionalInfo',"100%", "320", "", "");
					$oFCKeditor->Create();
					?>
				</td>
			</tr>
		</table>
	</td>
</tr>

<tr>
	<td>
		<?=$StudentInfoTable?>
	</td>
</tr>

<tr>
	<td><table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
		<tr>
			<td align="center">
				<?= $linterface->GET_ACTION_BTN($button_continue, "submit")?>&nbsp;
				<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "","cancel_btn")?>
			</td>
		</tr>
	</table>
	</td>
</tr>
</table>

<br />

<?=$StudentHiddenField?>
<input type="hidden" name="TargetDate" value="<?=$_REQUEST['TargetDate']?>">
<input type="hidden" name="DayType" value="<?=$_REQUEST['DayType']?>">
</form>
<script type="text/javascript" language="javascript">
$(document).ready(function(){
	$('#cancel_btn').click(function(){
		window.location.href = 'show.php?TargetDate=<?=$_REQUEST['TargetDate']?>&DayType=<?=$_REQUEST['DayType']?>';
	});
});
</script>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();

?>