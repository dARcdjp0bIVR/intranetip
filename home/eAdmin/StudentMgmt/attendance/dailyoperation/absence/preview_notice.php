<?php
// Modifying by: henry
###### Change Log [Start] ######
#
#	Date	:	2011-09-21 (Henry Chow)
#				add "replyslip" in javascript
#
####### Change Log [End] #######

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");
include_once($PATH_WRT_ROOT."includes/libucc.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	echo "die";
	intranet_closedb();
	exit();
}

$lc = new libucc();
$lc->setTemplateID($_REQUEST['TemplateID']);
$eNoticeData = $lc->getNoticeDetails();

$noticeTitle = stripslashes(intranet_htmlspecialchars($eNoticeData['Subject']));
$noticeContent = str_replace("<IMG src=\"$image_path/$LAYOUT_SKIN/discipline/StudentName.gif\">", $pv_studentname, $eNoticeData['Description']);
$noticeContent = str_replace("<IMG src=\"$image_path/$LAYOUT_SKIN/discipline/AdditionalInfo.gif\">", stripslashes(intranet_htmlspecialchars($pv_additionalinfo)), $noticeContent);
$noticeQuestion = stripslashes(intranet_htmlspecialchars($eNoticeData['Question']));
$noticeReplySlipContent = stripslashes(undo_htmlspecialchars($eNoticeData['ReplySlipContent']));

if($eNoticeData['SendReplySlip']==0) $noticeReplySlipContent = "";

$linterface = new interface_html();
?>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title><?=$i_home_title?></title>
		<link href="<?=$PATH_WRT_ROOT?>templates/<?=$LAYOUT_SKIN?>/css/content.css" rel="stylesheet" type="text/css">
	</head>
	<body background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/bg.gif" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td width="21">
					<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/popup/top_01.gif" width="21" height="31"></td>
				<td align="left" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/popup/top_02.gif">
					<table border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/popup/top_01.gif" class="popuptitle"><?=$ip20_enotice?></td>
							<td width="30">
								<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/popup/top_02b.gif" width="30" height="31"></td>
						</tr>
					</table>
				</td>
				<td width="22">
					<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/popup/top_03.gif" width="22" height="31"></td>
			</tr>
			<tr>
				<td background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/popup/board04.gif">
					<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/popup/board04.gif" width="21" height="16"></td>
				<td align="center" bgcolor="#FFFFFF">
					<table width="88%" border="0" cellpadding="5" cellspacing="0">
						<tr><td>
								<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
									<tr>
										<td valign="top" nowrap="nowrap" class="formfieldtitle">
											<span class="tabletext"><?=$i_Notice_Title?></span></td>
										<td width="80%" valign="top" class="tabletext"><?=$noticeTitle?></td>
									</tr>
									<tr>
										<td valign="top" nowrap="nowrap" class="formfieldtitle">
											<span class="tabletext"><?=$i_Notice_Description?></span></td>
										<td valign="top" class="tabletext">&nbsp;</td>
									</tr>
								</table>
								<table width="100%" border="0" cellspacing="0" cellpadding="3" class="result_box">
									<tr>
										<td valign="top"><?=$noticeContent?></td>
									</tr>
								</table></td>
						</tr>
					</table>
					<br>
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eOffice/scissors_line.gif">
								<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eOffice/icons_scissors.gif"></td>
						</tr>
					</table>
					<table width="90%" border="0" cellpadding="5" cellspacing="0">
						<tr>
							<td align="center" class="eNoticereplytitle"><?=$i_Notice_ReplySlip?></td>
						</tr>
						
						<? if($noticeReplySlipContent) {?>
						<!-- Content Base Start //-->
							<tr valign='top'>
								<td colspan="2">
								<?=$noticeReplySlipContent?>
								</td>
							</tr>
						<? } else { 
								include_once($PATH_WRT_ROOT."includes/libform.php");
								$lform = new libform();
								$temp_que =  str_replace("&amp;", "&", $noticeQuestion);
								$queString = $lform->getConvertedString($temp_que);
								$queString =trim($queString)." ";
							?>
							<!-- Question Base Start //-->
							<tr valign='top'>
							<td colspan="2">
		                        <table width="100%" border="0" cellspacing="0" cellpadding="5">
		                        <tr>
		                        	<td>
		                        	<script language="javascript" src="/templates/forms/layer.js"></script>
		                        	<script language="javascript" src="/templates/forms/form_edit.js"></script>
		                        
		                                <form name="ansForm" method="post" action="update.php">
		                                        <input type=hidden name="qStr" value="">
		                                        <input type=hidden name="aStr" value="">
		                                </form>
		                                
		                                <script language="Javascript">
		                                var replyslip = '';
		                                <?=$lform->getWordsInJS()?>
		                                
		                                var sheet= new Answersheet();
		                                // attention: MUST replace '"' to '&quot;'
		                                sheet.qString="<?=$queString?>";
		                                sheet.aString="<?=$ansString?>";
		                                //edit submitted application
		                                sheet.mode=1;
		                                sheet.answer=sheet.sheetArr();
		                                Part3 = '';
		                                document.write(editPanel());
		                                </script>
		                                
		                                <SCRIPT LANGUAGE=javascript>
		                                function copyback()
		                                {
		                                         finish();
		                                         document.form1.qStr.value = document.ansForm.qStr.value;
		                                         document.form1.aStr.value = document.ansForm.aStr.value;
		                                }
		                                </SCRIPT>
		                                
		                        	</td>
							</tr>
		                    </table>
		                    </td>
						</tr>
						
						<? } ?>
						
						
						<tr>
							<td height="1" class="dotline">
								<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
						</tr>
						<tr>
							<td align="center"><?=$linterface->GET_ACTION_BTN($button_close, "button", "self.close();", "cancelbtn")?></td>
						</tr>
					</table></td>
				<td background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/popup/board06.gif">
					<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/popup/board06.gif" width="22" height="10"></td>
			</tr>
			<tr>
				<td height="10">
					<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/popup/board07.gif" width="21" height="10"></td>
				<td  height="10" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/popup/board08.gif">
					<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/popup/board08.gif" width="21" height="10"></td>
				<td height="10">
					<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/popup/board09.gif" width="22" height="10"></td>
			</tr>
		</table>
	</body>
</html>
<?
intranet_closedb();
?>