<?php
// Modifying by: 
/*
 * 2016-01-11 (Carlos): Handle backward slashes of post request parameter AdditionalInfo.
 */
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");
include_once($PATH_WRT_ROOT."includes/libucc.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$enabled_magic_quotes = function_exists("get_magic_quotes_gpc") &&  get_magic_quotes_gpc();

$TargetUsers = $_REQUEST['StudentID'];
$DayType = $_REQUEST['DayType'];
$TargetDate = $_REQUEST['TargetDate'];
$TemplateID = $_REQUEST['TemplateID'];
$AdditionalInfo = $enabled_magic_quotes? stripslashes($_REQUEST['AdditionalInfo']) : $_REQUEST['AdditionalInfo'];

###period
switch ($DayType)
{
  case PROFILE_DAY_TYPE_AM: 
  	$display_period = $i_DayTypeAM;
    break;
  case PROFILE_DAY_TYPE_PM: 
  	$display_period = $i_DayTypePM;
    break;
  default : 
  	$display_period = $i_DayTypeAM;
  	$DayType = PROFILE_DAY_TYPE_AM;
    break;
}

$StudentAttend = new libcardstudentattend2();
$lnotice = new libnotice();
$lucc = new libucc();

$TemplateDetail = $StudentAttend->Get_Template_Detail($TemplateID);

$template_category = $TemplateDetail[0]['CategoryID'];
$SpecialData = array();
$IssuerInfo = $StudentAttend->Get_UserInfo_By_ID(array($_SESSION['UserID']));
$IssueUserName = $IssuerInfo[0]['Name'];
$IssueUserID = $_SESSION['UserID'];
$NoticeNumber = time();
$TargetRecipientType = "U";
$RecordType = NOTICE_TO_SOME_STUDENTS;

for ($i=0; $i< sizeof($TargetUsers); $i++) {
	$RecipientID = array($TargetUsers[$i]);
	$AbsentInfo = $StudentAttend->Get_Student_Profile_Info($TargetUsers[$i],$TargetDate,$DayType,PROFILE_TYPE_ABSENT);
	$data['StudentName'] = $AbsentInfo['Name'];
	$data['ClassName'] = $AbsentInfo['ClassName'];
	$data['ClassNumber'] = $AbsentInfo['ClassNumber'];
	$data['AdditionalInfo'] = $AdditionalInfo;
	$data['RecordDate'] = $TargetDate;
	$data['Session'] = $display_period;
	$data['Reason'] = $AbsentInfo['Reason'];
	if (!$lnotice->disabled)
	{				
		$ReturnArr = $lucc->setNoticeParameter('StudentAttendance',$NoticeNumber,$TemplateID,
																						$data,$IssueUserID,$IssueUserName,$TargetRecipientType,
																						$RecipientID,$RecordType);
		$NoticeID = $lucc->sendNotice();
		if($NoticeID != -1)
		{
			# update Merit Record
			$updateDataAry = array();
			
			$StudentAttend->Set_Notice_ID_To_Profile($AbsentInfo['RecordID'],$NoticeID);
		}
	}
}

header("Location: show.php?DayType=".$DayType."&TargetDate=".$TargetDate);
intranet_closedb();
?>