<?php

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/HKUFlu/HKUFlu.php");
include_once($PATH_WRT_ROOT."includes/HKUFlu/libHKUFlu_db.php");
$lib_ui = new interface_html();

$hkuFlu = new HKUFlu();
$db_hkuFlu = new libHKUFlu_db();
$sick_code = $hkuFlu->getSickCode();
$symptom_code = $hkuFlu->getSymptomCode();

$preset_leave_id = $_POST['preset_leave_id'];
$sick_leave_id = $_POST['sick_leave_id'];
$have_record = $_POST['have_record'];
$student_id = $_POST['student_id'];
$leave_date = $_POST['leave_date'];
$student_info = $_POST['student_info'];

//DB
$sick_date = $leave_date;
$array_sick_index = array();
$array_symptom_index = array();
$sick_other = '';
$symptom_other = '';

if($have_record == 'T'){
	intranet_auth();
	intranet_opendb();
	
	if($preset_leave_id != ''){
		$rows_full_sick_leave = $db_hkuFlu->getSickLeaveByPresentLeaveID($preset_leave_id);
	}else{
		$rows_full_sick_leave = $db_hkuFlu->getFullSickLeaveBySickLeaveID($sick_leave_id);
	}

	foreach((array)$rows_full_sick_leave as $sick_leave_id => $row_full_sick_leave){
		$sick_date = substr($row_full_sick_leave['sick_leave']['SickDate'],0,10);
		
		foreach((array)$row_full_sick_leave['sick_leave_sick'] as $row_sick){
			array_push($array_sick_index, $hkuFlu->getIndexFromSickCode($row_sick['SickCode']));
			$sick_other = $row_sick['Other'];
		}
		
		foreach((array)$row_full_sick_leave['sick_leave_symptom'] as $row_symptom){
			array_push($array_symptom_index, $hkuFlu->getIndexFromSymptomCode($row_symptom['SymptomCode']));
			$symptom_other = $row_symptom['Other'];
		}
	}
}

//UI
$no_of_column = 2;

$table_sick = '<table style="width:100%;"><tr><td style="width:'.(100/$no_of_column).'%;">';
foreach((array)$Lang['HKUSPH']['array_sick'] as $key => $lang_sick){
	$table_sick .= $lib_ui->Get_Checkbox('SickCheckBox'.$key, 'SickCheckBox'.$key, $Value=1, $isChecked=(in_array($key,$array_sick_index)?1:0), $Class='', $Display='', $Onclick='', $Disabled='');
	$table_sick .= ' ' . $lang_sick . '<br/>';

	if($key == HKUFlu::SICK_OTHER){
		$table_sick .= $lib_ui->GET_TEXTBOX('SickOther', 'SickOther', $sick_other, $OtherClass='requiredField', $OtherPar=array('maxlength'=>128));
	}

	if(($key+1)%floor(count($Lang['HKUSPH']['array_sick'])/$no_of_column) == 0 && $key != count($Lang['HKUSPH']['array_sick']) - 1){
		$table_sick .= '</td><td>';
	}
}
$table_sick .= '</td></tr></table>';

$table_symptom = '<table style="width:100%;"><tr><td style="width:'.(100/$no_of_column).'%;">';
foreach((array)$Lang['HKUSPH']['array_symptom'] as $key => $lang_symptom){
	$table_symptom .= $lib_ui->Get_Checkbox('SymptomCheckBox'.$key, 'SymptomCheckBox'.$key, $Value=1, $isChecked=(in_array($key,$array_symptom_index)?1:0), $Class='', $Display='', $Onclick='', $Disabled='');
	$table_symptom .= ' ' . $lang_symptom . '<br/>';

	if($key == HKUFlu::SYMPTOM_OTHER){
		$table_symptom .= $lib_ui->GET_TEXTBOX('SymptomOther', 'SymptomOther', $symptom_other, $OtherClass='requiredField', $OtherPar=array('maxlength'=>128));
	}

	if(($key+1)%floor(count($Lang['HKUSPH']['array_symptom'])/$no_of_column) == 0 && $key != count($Lang['HKUSPH']['array_symptom']) - 1){
		$table_symptom .= '</td><td>';
	}
}
$table_symptom .= '</td></tr></table>';


$fields=array(
    array(
    	'id'=>'StudentInfo',
    	'label'=>$i_UserName,
    	'control'=>$student_info,
    	'display'=>$student_info,
    	'error'=>'',
    ),
	array(
		'id'=>'SickDate',
		'label'=>$Lang['HKUSPH']['SickDate'] . ' ' . $lib_ui->RequiredSymbol(),
		'control'=>$lib_ui->GET_DATE_PICKER('SickDate', $sick_date),
		'display'=>$value['SickDate'],
		'error'=>array(
			'EmptyWarnDiv'=>$lib_ui->Get_Form_Warning_Msg('SupplierNameEmptyWarnDiv', $Lang['ePCM']['SettingsArr']['SupplierArr']['ControlArr']['Please input Supplier Name'], $Class='warnMsgDiv')
		),
	),
	array(
		'id'=>'Symptom',
		'label'=>$Lang['HKUSPH']['Symptom'] . ' ' . $lib_ui->RequiredSymbol(),
		'control'=>$table_symptom,
		'display'=>$value['Symptom'],
		'error'=>array(
			'EmptyWarnDiv'=>$lib_ui->Get_Form_Warning_Msg('SupplierNameEmptyWarnDiv', $Lang['ePCM']['SettingsArr']['SupplierArr']['ControlArr']['Please input Supplier Name'], $Class='warnMsgDiv')
		),
	),
	array(
		'id'=>'Sick',
		'label'=>$Lang['HKUSPH']['Sick'] . ' ' . $lib_ui->RequiredSymbol(),
		'control'=>$table_sick,
		'display'=>$value['Sick'],
		'error'=>array(
			'EmptyWarnDiv'=>$lib_ui->Get_Form_Warning_Msg('SupplierNameEmptyWarnDiv', $Lang['ePCM']['SettingsArr']['SupplierArr']['ControlArr']['Please input Supplier Name'], $Class='warnMsgDiv')
		),
	),
);

### action buttons
$htmlAry['submitBtn'] = $lib_ui->Get_Action_Btn($Lang['Btn']['Submit'], "button", "goSubmit()", 'submitBtn', $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");
$htmlAry['cancelBtn'] = $lib_ui->Get_Action_Btn($Lang['Btn']['Cancel'], "button", "goCancel()", 'cancelBtn', $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");
?>


<script>
//JavaScript
function goSubmit(){
	var can_submit_sick_date = true;
	if(document.getElementById('SickDate').value == ''){
		can_submit_sick_date = false;
		alert('<?php echo $Lang['HKUSPH']['Please fill in the sick date']; ?>');
	}
	if(document.getElementById('SickDate').value > document.getElementById('TargetDate').value){
		can_submit_sick_date = false;
		alert('<?php echo $Lang['HKUSPH']['Sick date must be on or earlier than leave date']; ?>');
	}
	
	var can_submit_sick = false;
	var str_sick_index_list = '';
	
	for(var i = 0; i < <?php echo count($sick_code);?>; i++){
//		console.log(i,document.getElementById('SickCheckBox'+i).checked);
		if(document.getElementById('SickCheckBox'+i).checked){
			can_submit_sick = true;
			str_sick_index_list += (str_sick_index_list==''?'':';') + i;
		}
	}
	if(!can_submit_sick){
		alert('<?php echo $Lang['HKUSPH']['Please choose at least one sickless']; ?>');
	}
	if(document.getElementById('SickCheckBox<?php echo HKUFlu::SICK_OTHER; ?>').checked){
		if(document.getElementById('SickOther').value == ''){
			can_submit_sick = false;
			alert('<?php echo $Lang['HKUSPH']['Please fill in the other sickless']; ?>');
		}
	}
	
	var can_submit_symptom = false;
	var str_symptom_index_list = '';
	
	for(var i = 0; i < <?php echo count($symptom_code);?>; i++){
		if(document.getElementById('SymptomCheckBox'+i).checked){
			can_submit_symptom = true;
			str_symptom_index_list += (str_symptom_index_list==''?'':';') + i;
		}
	}
	if(!can_submit_symptom){
		alert('<?php echo $Lang['HKUSPH']['Please choose at least one symptom']; ?>');
	}
	if(document.getElementById('SymptomCheckBox<?php echo HKUFlu::SYMPTOM_OTHER; ?>').checked){
		if(document.getElementById('SymptomOther').value == ''){
			can_submit_symptom = false;
			alert('<?php echo $Lang['HKUSPH']['Please fill in the other symptom']; ?>');
		}
	}
	
	if(can_submit_sick && can_submit_symptom && can_submit_sick_date){
		var data = {
			'preset_leave_id':'<?php echo $preset_leave_id; ?>',
			'sick_leave_id':'<?php echo $sick_leave_id; ?>',
			'have_record':'<?php echo $have_record;?>',
			'sick_date':document.getElementById('SickDate').value,
			'sick_index_list':str_sick_index_list,
			'sick_other':document.getElementById('SickOther').value,
			'symptom_index_list':str_symptom_index_list,
			'symptom_other':document.getElementById('SymptomOther').value,
			'student_id':'<?php echo $student_id;?>',
			'leave_date':'<?php echo $leave_date;?>'
		}
		console.log(data);
	
		$.ajax({
			'type':'POST',
			'url':'show_hkusph_ajax.php',
			'data':data,
			'success':function(return_data){
				console.log(return_data);
				if(return_data != 'FAIL'){
//					window.location.hash = '#HKUFLU_' + return_data;
//					window.location.reload(true);
					$('#btn_submit').trigger('click');
				}
			}
		});
	}
}

function goCancel(){
	$('#TB_closeWindowButton').click();
}

//jQuery
$(function(){
	for(var i=0; i<<?php echo count($sick_code);?>;i++){
		$('#SickCheckBox'+i).bind('click',function(){
			if($(this).attr('checked')){
				for(var j=0; j<<?php echo count($sick_code);?>;j++){
					if($(this).attr('id') != 'SickCheckBox'+j){
						$('#SickCheckBox'+j).removeAttr('checked');
						if(j == <?php echo count($sick_code)-1;?>){
							$('#SickOther').val('');
						}
					}
				}
			}
		});
	}
	$('#SickCheckBox<?php echo count($sick_code)-1;?>').bind('click',function(){
		if(!$(this).attr('checked')){
			$('#SickOther').val('');
		}
	});
	$('#SymptomCheckBox<?php echo count($symptom_code)-1;?>').bind('click',function(){
		if(!$(this).attr('checked')){
			$('#SymptomOther').val('');
		}
	});
	$('#SickOther').bind('keyup',function(){
		if($(this).val() == ''){
			$('#SickCheckBox<?php echo count($sick_code)-1;?>').removeAttr('checked');
		}else{
			$('#SickCheckBox<?php echo count($sick_code)-1;?>').attr('checked','checked');
			for(var j=0; j<<?php echo count($sick_code)-1;?>;j++){
				$('#SickCheckBox'+j).removeAttr('checked');
			}
		}
	});
	$('#SymptomOther').bind('keyup',function(){
		if($(this).val() == ''){
			$('#SymptomCheckBox<?php echo count($symptom_code)-1;?>').removeAttr('checked');
		}else{
			$('#SymptomCheckBox<?php echo count($symptom_code)-1;?>').attr('checked','checked');
		}
	});
});
</script>


<table class="form_table_v30">
	<?php if(count($fields)>0){?>
		<?php foreach($fields as $field){ ?>
			<tr>
				<td class="field_title"><label for="<?php echo $field['id']; ?>"><?php echo $field['label']; ?></label></td>
				<td>
					<?php echo $field['control']; ?>
					<?php foreach((array)$field['error'] as $error){?>
						<?php echo $error ?>
					<?php } ?>
				</td>
			</tr>
		<?php } ?>
	<?php } ?>
</table>
<br style="clear:both;" />
<?=$lib_ui->MandatoryField()?>
<div class="edit_bottom_v30">
	<p class="spacer"></p>
	<?=$htmlAry['submitBtn']?>
	<?=$htmlAry['cancelBtn']?>
	<p class="spacer"></p>
</div>