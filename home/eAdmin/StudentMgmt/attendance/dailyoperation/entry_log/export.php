<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();

### class used
$lc = new libcardstudentattend2();
$lclass = new libclass();

### Set Date from previous page
$ts_record = strtotime($TargetDate);
if ($ts_record == -1)
{
    $TargetDate = date('Y-m-d');
    $ts_record = strtotime($TargetDate);
}
$txt_year = date('Y',$ts_record);
$txt_month = date('m',$ts_record);
$txt_day = date('d',$ts_record);

$card_log_table_name = "CARD_STUDENT_ENTRY_LOG_".$txt_year."_".$txt_month;
$namefield = getNameFieldWithClassNumberByLang("a.");
$cond = ($TargetClass == "") ? "" : " AND a.ClassName = '$TargetClass'";

if ($TargetClass != "")
{
	$student_list = $lclass->returnStudentListByClass($TargetClass);
	$arr_student = array();
	for($i=0; $i<sizeof($student_list) ;$i++)
	{
		list($user_id, $student_name) = $student_list[$i];
		array_push($arr_student, $user_id);
	}
	$student_list = implode(",", $arr_student);
}


$namefield = getNameFieldWithClassNumberByLang("a.");
if($TargetClass == "")
{
	$sql = "SELECT 
						a.UserID,
						$namefield,
						a.ClassName, 
						a.ClassNumber,
						TIME_FORMAT(b.RecordTime, '%H:%i'),
						c.Reason
					FROM 
						INTRANET_USER AS a INNER JOIN 
						$card_log_table_name AS b ON (a.UserID = b.UserID AND b.DayNumber = '$txt_day') LEFT OUTER JOIN 
						CARD_STUDENT_ENTRY_LOG_REASON AS c ON (a.UserID = c.StudentID AND c.RecordDate = '$TargetDate')
					WHERE
						a.RecordType = 2 AND a.RecordStatus IN (0,1,2)
					ORDER BY
						a.ClassName, a.ClassNumber, a.EnglishName
			";
}
else
{
	
	$sql = "SELECT 
					a.UserID, $namefield, a.ClassName, 
						a.ClassNumber, TIME_FORMAT(b.RecordTime, '%H:%i'), c.Reason
			FROM 
					INTRANET_USER AS a LEFT OUTER JOIN 
					$card_log_table_name AS b ON (a.UserID = b.UserID AND b.DayNumber = '$txt_day') LEFT OUTER JOIN
					CARD_STUDENT_ENTRY_LOG_REASON as c ON (a.UserID = c.StudentID AND c.RecordDate = '$TargetDate')
			WHERE 
					a.RecordType = 2 AND a.RecordStatus IN (0,1,2)
					AND a.UserID IN ($student_list)
			ORDER BY 
					a.ClassName, a.ClassNumber, a.EnglishName
			";
}

$data = $lc->returnArray($sql,4);

// Format result
for($i=0; $i<sizeof($data); $i++)
{
	list($sid, $sname, $ClassName, $ClassNumber, $record_time, $reason) = $data[$i];
	$StudentName[$sid] = $sname;
	$StudentClassName[$sid] = $ClassName;
  $StudentClassNumber[$sid] = $ClassNumber;
	$time_array[$sid][] = $record_time;
	$EntryLogReason[$sid] = $reason;
}

#export_content = "";

$lexport = new libexporttext();

$exportColumn = array("$i_UserStudentName", "$i_ClassName", "$i_ClassNumber", "$i_StaffAttendnace_Leave_TimeSlot", "$i_Attendance_Reason");

// Table Header
$export_content_final = $i_ClassName."\t".(($TargetClass == "") ? $i_StudentAttendance_AllStudentsWithRecords : $TargetClass)."\n";
$export_content_final .= $i_StudentAttendance_Field_Date."\t".$TargetDate."\n";

if (sizeof($StudentName) > 0)
{
	$counter_i = 0;
	foreach ($StudentName AS $student_id => $student_name)
	{	
		$export_data_array[$counter_i][0] = $student_name;
		$export_data_array[$counter_i][1] = $StudentClassName[$student_id];
		$export_data_array[$counter_i][2] = $StudentClassNumber[$student_id];
		$export_data_array[$counter_i][3] = "";
		
		for($j=0; $j<sizeof($time_array[$student_id]); $j++)		
		{
			$export_data_array[$counter_i][3] .= ($time_array[$student_id][$j]) ? "".$time_array[$student_id][$j]."  " : "" ." -  ". "";
			if($time_array[$student_id][$j] != "")
				$cnt = 1;
			else
				$cnt = 0;
		}
		if($cnt == 1)
			$export_data_array[$counter_i][4] = "$EntryLogReason[$student_id]";
		else
			$export_data_array[$counter_i][4] = "" ." -  ". "";
		
		$counter_i++;
	}
}

$export_content = $lexport->GET_EXPORT_TXT($export_data_array, $exportColumn);

$export_content_final .= $export_content;

$filename = "entry_log_".$txt_year."_".$txt_month.".csv";

// Output the file to user browser
$lexport->EXPORT_FILE($filename, $export_content_final);

?>