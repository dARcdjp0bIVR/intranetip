<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();

$linterface = new interface_html();

include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_header.php");

### Set Date from previous page
$ts_record = strtotime($TargetDate);
if ($ts_record == -1)
{
    $TargetDate = date('Y-m-d');
    $ts_record = strtotime($TargetDate);
}
$txt_year = date('Y',$ts_record);
$txt_month = date('m',$ts_record);
$txt_day = date('d',$ts_record);

// Retrieve data
$card_log_table_name = "CARD_STUDENT_ENTRY_LOG_".$txt_year."_".$txt_month;

### Create CARD_STUDENT_ENTRY_LOG If Not Exists
$sql = "CREATE TABLE IF NOT EXISTS $card_log_table_name
		(
          RecordID int(11) NOT NULL auto_increment,
          UserID int(11) NOT NULL,
          DayNumber int(11) NOT NULL,
          RecordTime time,
          RecordType int(11),
          RecordStatus int(11),
          DateInput datetime,
          DateModified datetime,
          PRIMARY KEY (RecordID)
        ) ENGINE=InnoDB charset=utf8";

$lidb = new libdb();
$lclass = new libclass();

$result = $lidb->db_db_query($sql);

if ($TargetClass != "")
{
	$student_list = $lclass->returnStudentListByClass($TargetClass);
	$arr_student = array();
	for($i=0; $i<sizeof($student_list) ;$i++)
	{
		list($user_id, $student_name) = $student_list[$i];
		array_push($arr_student, $user_id);
	}
	$student_list = implode(",", $arr_student);
}

$namefield = getNameFieldWithClassNumberByLang("a.");
if($TargetClass == "")
{
	$sql = "SELECT 
					a.UserID,
                    $namefield,
                    a.ClassName, 
						a.ClassNumber,
                    TIME_FORMAT(b.RecordTime, '%H:%i'),
                    c.Reason
			FROM 
					INTRANET_USER AS a INNER JOIN 
					$card_log_table_name AS b ON (a.UserID = b.UserID AND b.DayNumber = '$txt_day') LEFT OUTER JOIN 
					CARD_STUDENT_ENTRY_LOG_REASON AS c ON (a.UserID = c.StudentID AND c.RecordDate = '$TargetDate')
			WHERE
					a.RecordType = 2 AND a.RecordStatus IN (0,1,2)
			ORDER BY
					a.ClassName, a.ClassNumber, a.EnglishName
			";
}
else
{
	
	$sql = "SELECT 
					a.UserID, $namefield, a.ClassName, 
						a.ClassNumber, TIME_FORMAT(b.RecordTime, '%H:%i'), c.Reason
			FROM 
					INTRANET_USER AS a LEFT OUTER JOIN 
					$card_log_table_name AS b ON (a.UserID = b.UserID AND b.DayNumber = '$txt_day') LEFT OUTER JOIN
					CARD_STUDENT_ENTRY_LOG_REASON as c ON (a.UserID = c.StudentID AND c.RecordDate = '$TargetDate')
			WHERE 
					a.RecordType = 2 AND a.RecordStatus IN (0,1,2)
					AND a.UserID IN ($student_list)
			ORDER BY 
					a.ClassName, a.ClassNumber, a.EnglishName
			";
}

$data = $lc->returnArray($sql,4);

// Format result array
for($i=0; $i<sizeof($data); $i++)
{
        list($sid, $sname, $ClassName, $ClassNumber, $record_time, $reason) = $data[$i];
        $StudentName[$sid] = $sname;
        $StudentClassName[$sid] = $ClassName;
  			$StudentClassNumber[$sid] = $ClassNumber;
        $time_array[$sid][] = $record_time;
        $EntryLogReason[$sid] = $reason;
}

$table_attend = "";

$x  = "<table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\" class=\"eSporttableborder\">";
$x .= "<tr>";
$x .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_UserStudentName</td>";
$x .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_ClassName</td>";
$x .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_ClassNumber</td>";
$x .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_StaffAttendnace_Leave_TimeSlot</td>";
$x .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_Attendance_Reason</td>";
$x .= "</tr>";

// Display Result
if (sizeof($StudentName) > 0)
{
    foreach ($StudentName AS $student_id => $student_name)
    {
        $x .= "<tr>";
        $x .= "<td class=\"eSporttdborder eSportprinttext\">$student_name</td>\n";
        $x .= "<td class=\"eSporttdborder eSportprinttext\">".$StudentClassName[$student_id]."</td>\n";
        $x .= "<td class=\"eSporttdborder eSportprinttext\">".$StudentClassNumber[$student_id]."</td>\n";
        for($j=0; $j<sizeof($time_array[$student_id]); $j++)
        {
            $font_color = ($j%2 == 0) ? "red" : "green";
            $x .= ($j==0) ? "<td class=\"eSporttdborder eSportprinttext\">" : "";
            $x .= ($time_array[$student_id][$j]) ? "<font color=\"$font_color\">".$time_array[$student_id][$j]."</font>" : "-";
            $x .= (sizeof($time_array[$student_id]) > ($j+1)) ? " | " : "</td>\n";
            if($time_array[$student_id][$j] != "") {
            	$cnt = 1;
            	$cnt_flag = 1;
        	} else
            	$cnt = 0;
        }
        
		if($cnt == 1)
		{
        	$reason_comp = "$EntryLogReason[$student_id]";
    	}
        else
        {
        	$reason_comp = "-";
    	}
        
        $x .= "<td class=\"eSporttdborder eSportprinttext\">".$reason_comp."&nbsp;</td>";
        $x .= "</tr>\n";
    }
}
else
{
	$x .= "<tr><td class=\"eSporttdborder eSportprinttext\" colspan=\"3\" align=\"center\">$i_no_record_exists_msg</td></tr>";
}
$x .= "</table>\n";
$table_attend .= $x;

$i_title = "$i_SmartCard_DailyOperation_ViewEntryLog - $TargetDate (".(($TargetClass == "") ? $i_StudentAttendance_AllStudentsWithRecords : $TargetClass).")";
?>
<table width='100%' align='center' class='print_hide' border=0>
	<tr>
		<td align='right'><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></td>
	</tr>
</table>
<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
		<td class="eSportprinttext"><?=$i_title?></td>
	</tr>
</table>
<?=$table_attend?>
<?
include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_footer.php");
intranet_closedb();
?>