<?php
// Editing by 
/**************************************************** Change Log ********************************************************************
 * 2019-02-11 (carlos)[ip.2.5.10.2.1]: added csv format.
 * 2016-08-30 (Carlos)[ip.2.5.7.10.1]: created
 ************************************************************************************************************************************/
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	intranet_closedb();
	header ("Location: /");
	exit();
}

$lc = new libcardstudentattend2();
$linterface = new interface_html();

function formatDisplayTime($ts)
{
	global $Lang, $intranet_session_language;
	
	$hours = sprintf("%d", $ts / 60 / 60);
	$minutes = sprintf("%d", $ts / 60 % 60);
	$seconds = sprintf("%d", $ts % 60 % 60);
	
	if($hours > 0){
		$h = $Lang['StudentAttendance']['Hours'];
		if($intranet_session_language == 'en'){
			if($hours > 1){
				$h = str_replace('(s)','s',$h);
			}else{
				$h = str_replace('(s)','',$h);
			}
		}
		$display_time .= $hours.' '.$h.' ';
	}
	if($minutes > 0){
		$m = $Lang['StudentAttendance']['Minutes'];
		if($intranet_session_language == 'en'){
			if($minutes > 1){
				$m = str_replace('(s)','s',$m);
			}else{
				$m = str_replace('(s)','',$m);
			}
		}
		$display_time .= $minutes.' '.$m.' ';
	}
	if($seconds > 0 || ($hours==0 && $minutes == 0)){
		$s = $Lang['StudentAttendance']['Seconds'];
		if($intranet_session_language == 'en'){
			if($seconds > 1){
				$s = str_replace('(s)','s',$s);
			}else{
				$s = str_replace('(s)','',$s);
			}
		}
		$display_time .= $seconds.' '.$s.' ';
	}
	
	return $display_time;
}

$target_result = $lc->GetTargetSelectionResult($TargetType, $TargetID);
$targetStudentIdAry = $target_result[0];

$start_ts = strtotime($StartDate);
$end_ts = strtotime($EndDate);

$Location = trim(urldecode(stripslashes($Location)));

$name_field = getNameFieldWithClassNumberByLang("u.");

$records = array();
for($cur_ts=$start_ts;$cur_ts<=$end_ts;$cur_ts=mktime(0,0,0,intval(date("m",$cur_ts)+1),intval(date("d",$cur_ts)),intval(date("Y",$cur_ts))))
{
	$cur_year = date("Y",$cur_ts);
	$cur_month = date("m",$cur_ts);
	$cur_day = date("d",$cur_ts);
	
	$entry_log_table = "CARD_STUDENT_ENTRY_LOG_".$cur_year."_".$cur_month;
	$lc->createTable_Card_Student_Entry_Log($cur_year,$cur_month);
	
	$sql = "SELECT 
				u.UserID,
				$name_field as StudentName,
				u.ClassName,
				u.ClassNumber,
				DATE_FORMAT(CONCAT('$cur_year-$cur_month-',r.DayNumber),'%Y-%m-%d') as RecordDate,
				r.RecordTime,
				r.RecordStation 
			FROM $entry_log_table as r 
			INNER JOIN INTRANET_USER as u ON r.UserID=u.UserID 
			WHERE r.UserID IN ('".implode("','",$targetStudentIdAry)."') AND r.RecordStation='".$lc->Get_Safe_Sql_Query($Location)."' AND (DATE_FORMAT(CONCAT('$cur_year-$cur_month-',r.DayNumber),'%Y-%m-%d') BETWEEN '$StartDate' AND '$EndDate') 
			ORDER BY RecordDate,u.ClassName,u.ClassNumber+0,r.RecordTime";
	$ary = $lc->returnResultSet($sql);
	$records = array_merge($records,$ary);
}

//debug_pr($records);

$record_count = count($records);

$data = array();
if($GroupBy == 'Class' || $Format == 'csv'){
	
	for($i=0;$i<$record_count;$i++){
		$user_id = $records[$i]['UserID'];
		$student_name = $records[$i]['StudentName'];
		$class_name = $records[$i]['ClassName'];
		$class_number = $records[$i]['ClassNumber'];
		$date = $records[$i]['RecordDate'];
		$time = $records[$i]['RecordTime'];
		$location = trim($records[$i]['RecordStation']);
		
		if(!isset($data[$class_name])){
			$data[$class_name] = array();
		}
		if(!isset($data[$class_name][$date])){
			$data[$class_name][$date] = array();
		}
		if(!isset($data[$class_name][$date][$user_id])){
			$data[$class_name][$date][$user_id] = array();
			$data[$class_name][$date][$user_id]['Time'] = array();
		}
		
		$data[$class_name][$date][$user_id]['StudentName'] = $student_name;
		$data[$class_name][$date][$user_id]['ClassName'] = $class_name;
		$data[$class_name][$date][$user_id]['ClassNumber'] = $class_number;
		$data[$class_name][$date][$user_id]['Time'][] = $time;
	}
	
}else if($GroupBy == 'Date'){
	
	for($i=0;$i<$record_count;$i++){
		$user_id = $records[$i]['UserID'];
		$student_name = $records[$i]['StudentName'];
		$class_name = $records[$i]['ClassName'];
		$class_number = $records[$i]['ClassNumber'];
		$date = $records[$i]['RecordDate'];
		$time = $records[$i]['RecordTime'];
		$location = trim($records[$i]['RecordStation']);
		
		if(!isset($data[$date])){
			$data[$date] = array();
		}
		if(!isset($data[$date][$user_id])){
			$data[$date][$user_id] = array();
			$data[$date][$user_id]['Time'] = array();
		}
		
		$data[$date][$user_id]['StudentName'] = $student_name;
		$data[$date][$user_id]['ClassName'] = $class_name;
		$data[$date][$user_id]['ClassNumber'] = $class_number;
		$data[$date][$user_id]['Time'][] = $time;
	}
}

//debug_pr($data);

if($Format == 'print'){
	$table_attr = ' align="center" class="eSporttableborder" border="0" cellpadding="2" cellspacing="0" width="96%" ';
	$th_attr = ' class="eSporttdborder eSportprinttabletitle" ';
	$td_attr = ' class="eSporttdborder eSportprinttext" ';
}else{
	$table_attr = ' class="common_table_list" ';
	$th_attr = '';
	$td_attr = '';
}

$x = '';
$rows = array();
$header_row = array($Lang['General']['Date'],$Lang['StudentAttendance']['ClassName'],$Lang['StudentAttendance']['ClassNumber'],$Lang['StudentAttendance']['StudentName'],$Lang['StudentAttendance']['RecordTime']);
// Web fomat [Print] and [Export] buttons 
if($Format != 'csv' && $Format != 'print')
{
	$x .= '<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">' . "\n";
		$x .= '<tr>' . "\n";
		$x .= '<td>' . "\n";
			$x .= '<div class="content_top_tool">' . "\n";
				$x .= '<div class="Conntent_tool">' . "\n";
				$x .= $linterface->GET_LNK_PRINT("javascript:submitForm('print');", "", "", "", "", 1);
				$x .= $linterface->GET_LNK_EXPORT("javascript:submitForm('csv');", "", "", "", "", 1);
				$x .= '</div>' . "\n";
				$x .= '<br style="clear: both;">' . "\n";
			$x .= '</div>' . "\n";
		$x .= '</td>' . "\n";
		$x .= '</tr>' . "\n";
	$x .= '</table>' . "\n";
}

// Print format [Print] button
// Report title
if($Format == 'print'){
	$x .= '<table width="96%" align="center" class="print_hide" border="0">
				<tr>
					<td align="right">'.$linterface->GET_BTN($Lang['Btn']['Print'], "button", "javascript:window.print();","print_button").'</td>
				</tr>
			</table>'."\n";
	$x .= '<table width="96%" border="0" cellspacing="0" cellpadding="5" align="center">
			 <tr>
				<td align="left"><span style="font-weight:bold;">'.$Lang['Header']['Menu']['eAttednance'].' - '.$Lang['StudentAttendance']['EntryLogByLocation'].'</span></td>
			 </tr>
			 <tr>
				<td align="left"><span style="font-weight:bold;">'.$Lang['General']['From'].' '.$StartDate.' '.$Lang['General']['To'].' '.$EndDate.'</span></td>
			 </tr>
			 <tr>
				<td align="left"><span style="font-weight:bold;">'.$Lang['StudentAttendance']['TapCardLocation'].': '.Get_String_Display($Location).'</span></td>
			 </tr>
		   </table>
		   <p>&nbsp;</p>'."\n";
}

if($record_count == 0){
	$x = '<div class="no_record_find_v30">'.$Lang['General']['NoRecordFound'].'</div>';
}else 
{

	if($GroupBy == 'Date' && $Format != 'csv')
	{
		foreach($data as $date => $user_ary){
			
			$x .= '<table '.$table_attr.'>
					  	<tbody>
					  	<tr>
					  		<td colspan="4" '.$td_attr.'>'.Get_String_Display($date).'</td>
					  	</tr>
					  	<tr class="tabletop">
						    <th width="10%" nowrap="nowrap" '.$th_attr.'>'.$Lang['StudentAttendance']['ClassName'].'</th>
						    <th width="5%" nowrap="nowrap" '.$th_attr.'>'.$Lang['StudentAttendance']['ClassNumber'].'</th>
							<th width="15%" nowrap="nowrap" '.$th_attr.'>'.$Lang['StudentAttendance']['StudentName'].'</th>
							<th width="70%" '.$th_attr.'>'.$Lang['StudentAttendance']['RecordTime'].'</th>
					    </tr>'."\n";
			
			foreach($user_ary as $user_id => $info_ary){
				
				$x .= '<tr>';
					$x .= '<td nowrap="nowrap" '.$td_attr.'>'.Get_String_Display($info_ary['ClassName']).'</td>';
					$x .= '<td nowrap="nowrap" '.$td_attr.'>'.Get_String_Display($info_ary['ClassNumber']).'</td>';
					$x .= '<td nowrap="nowrap" '.$td_attr.'>'.Get_String_Display($info_ary['StudentName']).'</td>';
					$x .= '<td '.$td_attr.'>';
						for($i=0;$i<count($info_ary['Time']);$i+=2){
							$t1 = $info_ary['Time'][$i];
							$t2 = $info_ary['Time'][$i+1];
							if($t2 == ''){
								$color = 'red';
								$v = '['.$t1.' - ?]';
							}else{
								$color = 'green';
								$duration = strtotime($t2) - strtotime($t1);
								$v = '['.$t1.' - '.$t2.'] '.formatDisplayTime($duration).'';
							}
							$x .= '<div style="color:'.$color.'">'.$v.'</div>';
						}
					$x .= '</td>';
				$x .= '</tr>'."\n";
				
			}
			
				$x .= '</tbody>';
			$x .= '</table><br />'."\n";
		}
	}else{
		
		foreach($data as $class => $date_ary){
			
			$x .= '<table '.$table_attr.'>
					  	<tbody>
					  	<tr>
					  		<td colspan="5" '.$td_attr.'>'.Get_String_Display($class).'</td>
					  	</tr>
					  	<tr class="tabletop">
							<th width="10%" nowrap="nowrap" '.$th_attr.'>'.$Lang['General']['Date'].'</th>
						    <th width="10%" nowrap="nowrap" '.$th_attr.'>'.$Lang['StudentAttendance']['ClassName'].'</th>
						    <th width="5%" nowrap="nowrap" '.$th_attr.'>'.$Lang['StudentAttendance']['ClassNumber'].'</th>
							<th width="15%" nowrap="nowrap" '.$th_attr.'>'.$Lang['StudentAttendance']['StudentName'].'</th>
							<th width="60%" '.$th_attr.'>'.$Lang['StudentAttendance']['RecordTime'].'</th>
					    </tr>'."\n";
			foreach($date_ary as $date => $user_ary){
				
				foreach($user_ary as $user_id => $info_ary){
					
					$time_values = array();
					
					$x .= '<tr>';
						$x .= '<td nowrap="nowrap" '.$td_attr.'>'.Get_String_Display($date).'</td>';
						$x .= '<td nowrap="nowrap" '.$td_attr.'>'.Get_String_Display($info_ary['ClassName']).'</td>';
						$x .= '<td nowrap="nowrap" '.$td_attr.'>'.Get_String_Display($info_ary['ClassNumber']).'</td>';
						$x .= '<td nowrap="nowrap" '.$td_attr.'>'.Get_String_Display($info_ary['StudentName']).'</td>';
						$x .= '<td '.$td_attr.'>';
							for($i=0;$i<count($info_ary['Time']);$i+=2){
								$t1 = $info_ary['Time'][$i];
								$t2 = $info_ary['Time'][$i+1];
								if($t2 == ''){
									$color = 'red';
									$v = '['.$t1.' - ?]';
								}else{
									$color = 'green';
									$duration = strtotime($t2) - strtotime($t1);
									$v = '['.$t1.' - '.$t2.'] '.formatDisplayTime($duration).'';
								}
								$x .= '<div style="color:'.$color.'">'.$v.'</div>';
								$time_values[] = $v;
							}
						$x .= '</td>';
					$x .= '</tr>'."\n";
					
					if($Format == 'csv'){
						$row = array($date,$info_ary['ClassName'],$info_ary['ClassNumber'],$info_ary['StudentName'],implode(' | ',$time_values));
						$rows[] = $row;
					}
				}
			}
				$x .= '</tbody>';
			$x .= '</table><br />'."\n";
		}
	}

}

intranet_closedb();

if(in_array($Format,array("","web")))
{
	echo $x;
}

if($Format == 'print')
{
	include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_header.php");
	echo $x;
	include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_footer.php");
}

if($Format == 'csv'){
	$lexport = new libexporttext();
	$export_content = $lexport->GET_EXPORT_TXT($rows, $header_row);
	$filename = "entry_log_by_location_".date("YmdHis").".csv";
	$lexport->EXPORT_FILE($filename, $export_content);
}

?>