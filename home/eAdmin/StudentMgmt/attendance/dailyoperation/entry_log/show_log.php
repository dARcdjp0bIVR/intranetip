<?php
// Editing by 
/*
 * 2016-08-30 Carlos : added [Entry by location] tab.
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libwordtemplates.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

$tempUserID = $_SESSION['UserID'];

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();

$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageDailyOperation_ViewEntryLog";

$linterface = new interface_html();

### class used
$lc = new libcardstudentattend2();
$lword = new libwordtemplates();
$lidb = new libdb();
$lclass = new libclass();

### Set Date from previous page
$ts_record = strtotime($TargetDate);
if ($ts_record == -1)
{
    $TargetDate = date('Y-m-d');
    $ts_record = strtotime($TargetDate);
}
$txt_year = date('Y',$ts_record);
$txt_month = date('m',$ts_record);
$txt_day = date('d',$ts_record);

$lc->createTable_Card_Student_Entry_Log($txt_year,$txt_month);

// Retrieve data
$card_log_table_name = "CARD_STUDENT_ENTRY_LOG_".$txt_year."_".$txt_month;
/*
### Create CARD_STUDENT_ENTRY_LOG If Not Exists
$sql = "CREATE TABLE IF NOT EXISTS $card_log_table_name
		(
          RecordID int(11) NOT NULL auto_increment,
          UserID int(11) NOT NULL,
          DayNumber int(11) NOT NULL,
          RecordTime time,
          RecordType int(11),
          RecordStatus int(11),
          DateInput datetime,
          DateModified datetime,
          PRIMARY KEY (RecordID)
        ) ENGINE=InnoDB charset=utf8";

$result = $lidb->db_db_query($sql);
*/

if ($TargetClass != "")
{
	$student_list = $lclass->returnStudentListByClass($TargetClass);
	$arr_student = array();
	for($i=0; $i<sizeof($student_list) ;$i++)
	{
		list($user_id, $student_name) = $student_list[$i];
		array_push($arr_student, $user_id);
	}
	$student_list = implode(",", $arr_student);
}


$namefield = getNameFieldWithClassNumberByLang("a.");
if($TargetClass == "")
{
	$sql = "SELECT 
					a.UserID,
                    $namefield,
                    TIME_FORMAT(b.RecordTime, '%H:%i'),
                    c.Reason
			FROM 
					INTRANET_USER AS a INNER JOIN 
					$card_log_table_name AS b ON (a.UserID = b.UserID AND b.DayNumber = '$txt_day') LEFT OUTER JOIN 
					CARD_STUDENT_ENTRY_LOG_REASON AS c ON (a.UserID = c.StudentID AND c.RecordDate = '$TargetDate')
			WHERE
					a.RecordType = 2 AND a.RecordStatus IN (0,1,2)
			ORDER BY
					a.ClassName, a.ClassNumber, a.EnglishName
			";
}
else
{
	
	$sql = "SELECT 
					a.UserID, $namefield, TIME_FORMAT(b.RecordTime, '%H:%i'), c.Reason
			FROM 
					INTRANET_USER AS a LEFT OUTER JOIN 
					$card_log_table_name AS b ON (a.UserID = b.UserID AND b.DayNumber = '$txt_day') LEFT OUTER JOIN
					CARD_STUDENT_ENTRY_LOG_REASON as c ON (a.UserID = c.StudentID AND c.RecordDate = '$TargetDate')
			WHERE 
					a.RecordType = 2 AND a.RecordStatus IN (0,1,2)
					AND a.UserID IN ($student_list)
			ORDER BY 
					a.ClassName, a.ClassNumber, a.EnglishName
			";
}

$data = $lc->returnArray($sql,4);


// Format result array
for($i=0; $i<sizeof($data); $i++)
{
        list($sid, $sname, $record_time, $reason) = $data[$i];
        $StudentName[$sid] = $sname;
        $time_array[$sid][] = $record_time;
        $EntryLogReason[$sid] = $reason;
}


$table_attend = "";

// Table Header
$x = "<table width=\"95%\" border=\"0\" cellspacing=\"0\" cellpadding=\"5\" align=\"center\">";
$x .= "<tr class=\"tablebluetop\">";
$x .= "<td class=\"tabletoplink\">$i_UserStudentName</td>";
$x .= "<td class=\"tabletoplink\">$i_StaffAttendnace_Leave_TimeSlot</td>";
$x .= "<td class=\"tabletoplink\">$i_Attendance_Reason</td>";
$x .= "</tr>";

$words = $lword->getWordListAttendance();
$hasWord = sizeof($words)!=0;
$reason_js_array = "";

$disable_color="#DFDFDF";  # background color for disabled text field
$enable_color="#FFFFFF";  # background color for enabled text field

$select_word = "";

// Display Result
if (sizeof($StudentName) > 0)
{
	$counter = 0;
  foreach ($StudentName AS $student_id => $student_name)
  {
  	$counter++;
  	$css=($counter%2==0)?"tablebluerow1":"tablebluerow2";
    $x .= "<tr class=\"$css\">\n";
    $x .= "<td class=\"tabletext\">$student_name</td>\n";
    for($j=0; $j<sizeof($time_array[$student_id]); $j++)
    {
      $font_color = ($j%2 == 0) ? "red" : "green";
      $x .= ($j==0) ? "<td class=\"tabletext\">" : "";
      $x .= ($time_array[$student_id][$j]) ? "<font color=\"$font_color\">".$time_array[$student_id][$j]."</font>" : "-";
      $x .= (sizeof($time_array[$student_id]) > ($j+1)) ? " | " : "</td>\n";
      if($time_array[$student_id][$j] != "") {
      	$cnt = 1;
      	$cnt_flag = 1;
    	} else
        $cnt = 0; ######
    }
    if ($cnt == 1)
    {
      $HiddenField = "<input type=\"hidden\" name=\"record_time$student_id\" value=\"$cnt\">";
    }
    else
    {
      $HiddenField = "<input type=\"hidden\" name=\"record_time$student_id\" value=\"$cnt\">";
    }
                 
    $HiddenField .= "<input type=\"hidden\" name=\"user_id[$student_id]\" value=\"$student_id\">\n";
          
		if($cnt == 1)
		{
			$reason_comp = "<input type=\"text\" name=\"reason$student_id\" id=\"reason$student_id\" class=\"textboxnum\" maxlength=\"255\" value=\"$EntryLogReason[$student_id]\" style=\"background:$enable_color\">";
		}
		else
		{
			$reason_comp = "-";
		}
          	
    /*if ($hasWord)
    {
      if($cnt == 1)
      {
        	$select_word = "<a onMouseMove=\"overhere()\" href=\"javascript:showSelection($student_id,$cnt)\"><img src=\"$image_path/icon_alt.gif\" border=\"0\" alt=\"$i_Profile_SelectReason\"></a>";
      	}
        else
        {
        	$select_word = "";
      	}
        
        $txtContent = "<table border='0' cellpadding='1' cellspacing='0'>";
        if($remark!=""){
		$txtContent.="<tr class='tablebluetop'><td class='tabletoplink'><font color='red'>$i_Attendance_Others</font></td></tr>";
		$txtContent.="<tr><td class='tabletext'>";
		$txtContent.="<table border='0' cellspacing='0' cellpadding='0' width='100%'>";
		$txtContent.="<tr><td class='tabletext' valign='top' width='8'><font color='red'> - </font></td>";
		$txtContent.="<td class='tabletext' align='left'><a href=\\'javascript:putBack(document.form1.reason$student_id,'$remark');\\'><font color='red'>$remark</font></a></td>";
		$txtContent.="</tr></table><br></td></tr>";

       	}
       	$txtContent.="<tr class='tablebluetop'><td class='tabletoplink' width='100%'>$i_Attendance_Standard</td></tr>";
        for ($j=0; $j<sizeof($words); $j++)
        {
           $cssReason = ($j%2==0) ? "tablebluerow1":"tablebluerow2";
             $temp = addslashes($words[$j]);
             $temp2 = addslashes($temp);
             $txtContent .= "<tr class='$cssReason'><td>";
             $txtContent .= "<table border='0' cellpaddin='0' cellspacing='0' width='100%'>";
             $txtContent .= "<tr><td class='tabletext' valign='top' width='8'> - </td>";
             $txtContent .= "<td class='tabletext' align='left'><a class='tablelink' href=\\\"javascript:putBack(document.form1.reason$student_id,'$temp2');\\\">$temp</a></td></tr>";
             $txtContent .= "</table></td></tr>";
        }
        $txtContent .= "</table>";
        $reason_js_array .= "reasonSelection[$student_id] = \"<table border='0' cellspacing='0' cellpadding='0'>";
        $reason_js_array .= "<tr><td>";
        $reason_js_array .= "<table border='0' cellspacing='0' cellpadding='0'>";
        $reason_js_array .= "<tr><td>";
        $reason_js_array .= "<table border='0' cellspacing='0' cellpadding='0' width='100%'>";
        $reason_js_array .= "<tr class='tablebluebottom'><td class='tabletext' align='right'>";
	$xButton = str_replace("\"", "\\\"", $linterface->GET_BTN("x", "button", "hideMenu('ToolMenu')"));
        $reason_js_array .= $xButton;
        $reason_js_array .= "</td></tr>";
        $reason_js_array .= "</table></td></tr>";
        $reason_js_array .= "<tr><td class='tipbg' valign='top'><font size='-2'>$txtContent</font></td></tr>";
        $reason_js_array .= "</table></td></tr></table>\";";

    }*/
    $x .= "<td class=\"tabletext\">".$reason_comp;
    if($cnt == 1)
    {
    	$x .= $HiddenField;
	    $x .= $linterface->CONVERT_TO_JS_ARRAY($words, "jArrayWords", 1, 1);
			$x .= $linterface->GET_PRESET_LIST("jArrayWords", $student_id, "reason$student_id");
		}
    //					$x .= $select_word;
    $x .= "</td></tr>\n";
  }
}
else
{
        $x .= "<tr class=\"tablebluerow2\"><td class=\"tabletext\" colspan=\"3\" align=\"center\">$i_no_record_exists_msg</td></tr>\n";
}
$x .= "</table>\n";
$table_attend .= $x;

$toolbar = $linterface->GET_LNK_EXPORT("javascript:checkPost(document.form1,'export.php')","","","","",0);
$toolbar .= $linterface->GET_LNK_PRINT("javascript:openPrintPage()","","","","",0);

$TAGS_OBJ[] = array($i_SmartCard_DailyOperation_ViewEntryLog, "", 1);
$TAGS_OBJ[] = array($Lang['StudentAttendance']['EntryLogByLocation'], "by_location.php", 0);

$PAGE_NAVIGATION[] = array("$TargetDate (".(($TargetClass == "") ? $i_StudentAttendance_AllStudentsWithRecords : $TargetClass).")");

$MODULE_OBJ = $lc->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START(urldecode($Msg));

if ($msg == 1) $SysMsg = $linterface->GET_SYS_MSG("add");
if ($msg == 2) $SysMsg = $linterface->GET_SYS_MSG("update");
?>
<script language="Javascript" src='/templates/tooltip.js'></script>
<style type="text/css">
	#ToolMenu{position:absolute; top: 0px; left: 0px; z-index:4; visibility:show;}
</style>
<script language="JavaScript">
	isMenu = true;
</script>
<div id="ToolMenu" style="position:absolute; width=200; height=100; visibility:hidden"></div>
<script language="JavaScript" type="text/javascript">
var reasonSelection = Array();
<?=$reason_js_array?>
<!--
function openPrintPage()
{
	newWindow("show_print.php?TargetClass=<?=$TargetClass?>&TargetDate=<?=$TargetDate?>",4);
}
function showSelection(i, allowed)
{
	if (allowed == 1)
	{
		writeToLayer('ToolMenu',reasonSelection[i]);
				
		halfLayerHeight = parseInt(eval(doc + "ToolMenu" +".offsetHeight"))/2;

		if(mouse.y-halfLayerHeight >0)
			t = mouse.y-halfLayerHeight;
		else t = mouse.y-halfLayerHeight;
		
		l = mouse.x;
		moveToolTip2('ToolMenu', t, l);
		showLayer('ToolMenu');
	}
}
function moveToolTip2(lay, FromTop, FromLeft){

     if (tooltip_ns6) {

         var myElement = document.getElementById(lay);

          myElement.style.left = (FromLeft + 10) + "px";

          myElement.style.top = (FromTop + document.body.scrollTop) + "px";

     }

     else if(tooltip_ie4) {
          eval(doc + lay + sty + ".top = "  + (FromTop + document.body.scrollTop))
     }
 	 else if(tooltip_ns4){eval(doc + lay + sty + ".top = "  +  FromTop)}

     if (!tooltip_ns6) eval(doc + lay + sty + ".left = " + (FromLeft + 10));
   
}
function putBack(obj, value)
{
         obj.value = value;
         hideMenu('ToolMenu');
}
function setReasonComp(value, txtComp, hiddenFlag)
{
         if (value==1)
         {
             txtComp.disabled = false;
             hiddenFlag.value = 1;
         }
         else
         {
             txtComp.disabled = true;
             hiddenFlag.value = 0;
         }
}
function resetForm(formObj){
	formObj.reset();
  	hideMenu('ToolMenu');
	resetFields(formObj);	
}
function resetFields(formObj){
	status_list = document.getElementsByTagName('SELECT');
	if(status_list==null) return;
	
	for(i=0;i<status_list.length;i++){
		s = status_list[i];
		if(s==null) continue;
		reasonObj = eval('formObj.reason'+i);
		if(s.selectedIndex==1){
			if(reasonObj!=null){
				reasonObj.disabled=false;
				reasonObj.style.background='<?=$enable_color?>';
			}
		}else{
			if(reasonObj!=null){
				reasonObj.value="";
				reasonObj.disabled=true;
				reasonObj.style.background='<?=$disable_color?>';
			}
		}
	}
}

-->
</script>
<br />
<form name="form1" method="post" action="show_log_update.php">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>
			<table width="95%" border="0" cellpadding="0" cellspacing="0" align="center">
				<tr>
					<td align="left"><?= $toolbar ?></td>
					<td align="right"><?= $SysMsg ?></td>
				</tr>
				<tr>
					<td colspan="2">&nbsp;</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<?=$table_attend?>
		</td>
	</tr>
	<tr>
		<td>
			<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
		    	<tr>
                	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
		    </table>	
		</td>
	</tr>
	<tr>
		<td>
			<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
		    	<tr>
				    <td align="center" colspan="2">
				    <? if($cnt_flag==1) { ?>
						<?= $linterface->GET_ACTION_BTN($button_save, "submit", "AlertPost(document.form1,'show_log_update.php','$i_SmartCard_Confirm_Update_Attend?')") ?>
						<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
					<? } ?>
						<?= $linterface->GET_ACTION_BTN($button_back, "button", "javascript:window.location='index.php'","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<input type="hidden" name="TargetDate" value="<?php echo $TargetDate; ?>">
<input type="hidden" name="TargetClass" value="<?php echo $TargetClass; ?>">
<input type="hidden" name="tempUserID" value="<?php echo $tempUserID; ?>">
</form>
<br />
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>