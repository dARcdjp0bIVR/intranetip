<?
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");

$tempUserID = $_SESSION['UserID'];

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();

$lidb = new libdb();

if(is_array($user_id))
{
	$lidb->Start_Trans();
	foreach ($user_id AS $key => $UserID)
	{
		if(${"reason$UserID"} != "")
		{
			$sql = "SELECT RecordID FROM CARD_STUDENT_ENTRY_LOG_REASON WHERE StudentID = $UserID AND RecordDate = '$TargetDate'";
			$temp = $lidb->returnArray($sql,1);
			list($reason_id) = $temp[0];
			
			if ($reason_id == "")
			{
				$sql = "INSERT INTO	CARD_STUDENT_ENTRY_LOG_REASON VALUES ('', $UserID, '$TargetDate', '".${"reason$UserID"}."', '', '', NOW(), NOW())";
				$Result = $lidb->db_db_query($sql);
			}
			else
			{
				$sql = "Update CARD_STUDENT_ENTRY_LOG_REASON SET RecordDate = '$TargetDate', Reason = '".${"reason$UserID"}."', DateModified = NOW() WHERE StudentID = $UserID";
				$Result = $lidb->db_db_query($sql);
			}
		}
	}
	
	if ($Result) {
		$lidb->Commit_Trans();
		$Msg = $Lang['StudentAttendance']['EntryReasonUpdatedSuccess'];
	}
	else {
		$lidb->RollBack_Trans();
		$Msg = $Lang['StudentAttendance']['EntryReasonUpdatedFail'];
	}
	
	intranet_closedb();
	$_SESSION['UserID'] = $tempUserID;
	header("Location: show_log.php?TargetDate=$TargetDate&TargetClass=$TargetClass&Msg=".urlencode($Msg));
}
else
{
	$Msg = $Lang['StudentAttendance']['EntryReasonUpdatedSuccess'];
	intranet_closedb();
	$_SESSION['UserID'] = $tempUserID;
	header("Location: show_log.php?TargetDate=$TargetDate&TargetClass=$TargetClass&Msg=".urlencode($Msg));
}

?>