<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libaccount.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/adminheader_setting.php");

?>

<?= displayNavTitle($i_adminmenu_plugin, '', $i_StudentAttendance_System, '../',$i_StudentAttendance_Menu_DailyOperation,'') ?>
<?= displayTag("head_smartcardattendence_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align=center>
<td class=tableContent height=300 align=left>
<blockquote>
<?= displayOption(
                  $i_SmartCard_DailyOperation_ViewClassStatus,'class/',1,
                  $i_SmartCard_DailyOperation_ViewLateStatus,'late/',(!$sys_custom['QualiEd_StudentAttendance']),
                  $i_SmartCard_DailyOperation_ViewLateStatus,'late_q/',($sys_custom['QualiEd_StudentAttendance']),
                  $i_SmartCard_DailyOperation_ViewAbsenceStatus,'absence/',1,
                  $i_SmartCard_DailyOperation_ViewEarlyLeaveStatus, 'early/',1,
                  $i_SmartCard_DailyOperation_ViewLeftStudents,'left/',1,
                  $i_SmartCard_DailyOperation_ImportOfflineRecords,'import/',1,
                  $i_SmartCard_DailyOperation_ViewEntryLog,'entry_log/',($sys_custom['Student_Attendance_Entry_Log']),
                  $i_SmartCard_DailyOperation_Preset_Absence,'preset_absence/',1
                
                                ) ?>
</blockquote>
</td></tr>
</table>

<?
include_once("../../../templates/adminfooter.php");
?>