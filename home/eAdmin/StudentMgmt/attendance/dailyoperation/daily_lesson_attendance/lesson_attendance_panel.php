<?php
// Modifying by 
/*
 * 2020-06-03 Ray: Add TW
 * 2016-06-01 (Carlos): Modified status selection to cater more status for customization. And added reason input.
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libstudentattendance_ui.php");
//include_once($PATH_WRT_ROOT."includes/flashservices_ver_1_2/services/liblessonattendance.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-LessonAttendance"] || !$_SESSION["SSV_PRIVILEGE"]["plugin"]["attendancelesson"]) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$TargetDate = $_REQUEST['TargetDate'];

$linterface = new interface_html();
//$lla = new liblessonattendance();
$LessonAttendUI = new libstudentattendance_ui();
$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageLessonAttendance_DailyOverview";

### Title ###
$TAGS_OBJ[] = array($Lang['General']['Record'],"",0);
$MODULE_OBJ = $LessonAttendUI->GET_MODULE_OBJ_ARR();

//$MODULE_OBJ['title'] = $Lang['LessonAttendance']['DailyLessonStatus'];

$SubjectGroupID = $_REQUEST['SubjectGroupID'];
$SubjectGroupName = urldecode($_REQUEST['SubjectGroupName']);
$LessonDate = $_REQUEST['LessonDate'];
$RoomAllocationID = $_REQUEST['RoomAllocationID'];
$StartTime = $_REQUEST['StartTime'];
$EndTime = $_REQUEST['EndTime'];
$ServerPath = 'http://'.$_SERVER['SERVER_NAME'].':'.$_SERVER['SERVER_PORT'];
$AttendOverviewID = $_REQUEST['AttendOverviewID'];

$reason_js = '';
$statusMap = $LessonAttendUI->LessonAttendanceStatusMapping();
foreach($statusMap as $key => $value)
{
	$reason_ary = $LessonAttendUI->GetLessonAttendanceReasons('', $value['code']);
	$reason_text_ary = Get_Array_By_Key($reason_ary, 'ReasonText');
	$reason_js .= $linterface->CONVERT_TO_JS_ARRAY($reason_text_ary, "jArrayLessonReasons".$value['code'], 1);
}

$Msg = urldecode($Msg);
$linterface->LAYOUT_START($Msg);

echo '<div class="navigation_v30 navigation_2">
		<a href="index2.php?TargetDate='.$LessonDate.'">'.$Lang['LessonAttendance']['DailyLessonStatus'].'</a>
		<span>'.$Lang['General']['Record'].'</span>
	</div><br style="clear:both" />';
//debug_pr($SubjectGroupID."\n".$LessonDate);
echo $LessonAttendUI->Get_Lesson_Attendance_Panel_Form($SubjectGroupID, "SubjectGroup", $LessonDate, $AttendOverviewID);
//debug_pr($LessonAttendUI->Get_Lesson_Attendance_Panel_Form($SubjectGroupID, "SubjectGroup", $LessonDate, $AttendOverviewID));
//$linterface->LAYOUT_STOP();

intranet_closedb();
?>
<link href="/templates/<?=$LAYOUT_SKIN?>/css/lesson_attendance.css" rel="stylesheet" type="text/css" />
<?=$reason_js?>
<script>
var TargetDate = '<?=date("Y-m-d")?>';
function View_Lesson_Daily_Overview() {
	var TempTargetDate = document.getElementById('TargetDate').value;
	var re = /\b\d{4}[\/-]\d{1,2}[\/-]\d{1,2}\b/;
	
	if (re.test(TempTargetDate)) {
		$("span#TargetDateWarningLayer").hide();
		TargetDate = TempTargetDate;
		Block_Element('DetailTableLayer');
		$("div#DetailTableLayer").load('ajax_get_lesson_attendance_detail.php',{"TargetDate":TargetDate},function() {UnBlock_Element('DetailTableLayer');});
	}
	else {
		$("span#TargetDateWarningLayer").html('<?=$Lang['LessonAttendance']['InvalidDateFormat']?>');
		$("span#TargetDateWarningLayer").show();
	}
}

function Call_Lesson_Attendance_Flash(SubjectGroupID,SubjectGroupName,RoomAllocationID,StartTime,EndTime) {
	var QueryString = 'SubjectGroupID='+SubjectGroupID;
	QueryString += '&SubjectGroupName='+encodeURIComponent(SubjectGroupName);
	QueryString += '&RoomAllocationID='+RoomAllocationID;
	QueryString += '&StartTime='+StartTime;
	QueryString += '&EndTime='+EndTime;
	QueryString += '&LessonDate='+TargetDate;
	
	window.open('pop_lesson_attendance_flash_panel.php?'+QueryString,'LessonAttendanceFlashPopUp');
}

function Change_Single_Status(index, obj)
{
	obj.className=obj.options[obj.selectedIndex].className;
	var status = $(obj).val();
	if(status > 0)
	{
		$('#reasonSpan'+index).css('visibility','visible');
	}else{
		$('#reasonSpan'+index).css('visibility','hidden');
		<?php if (get_client_region() != 'zh_TW') { ?>
		$('#reason'+index).val('');
		<?php } ?>
	}

    if(status == 3) {
        $('#leaveTypeSpan'+index).css('visibility','visible');
    } else {
        $('#leaveTypeSpan'+index).css('visibility','hidden');
	}
}

function Change_All_Status(status){
	//if(status >= 0){
		/*
		var class1 = '';
		var class2 = '';
		if(status == 0){
			class1 = 'greenText';
			class2 = 'blueText orangeText redText';
		}
		else if(status == 1){
			class1 = 'blueText';
			class2 = 'greenText orangeText redText';
		}
		else if(status == 2){
			class1 = 'orangeText';
			class2 = 'blueText greenText redText';
		}
		else{
			class1 = 'redText';
			class2 = 'blueText orangeText greenText';
		}
		$(".drop_down_status").val(status);
		$("#all_drop_down_status").val('-1');
		$(".drop_down_status").removeClass(class2).addClass(class1);
		*/
		//$("#all_drop_down_status").val('-1');
		var objs = document.getElementsByName('drop_down_status[]');
<?php if($sys_custom['LessonAttendance_LaSalleCollege']){ ?>
		var cbs = document.getElementsByName('checkbox[]');
		for(var i=0;i<objs.length;i++)
		{
			if(cbs[i].checked)
			{
				var obj = $(objs[i]);
				obj.val(status);
				Change_Single_Status(i, objs[i]);
			}
		}
<?php }else{ ?>
		for(var i=0;i<objs.length;i++)
		{
			var obj = $(objs[i]);
			obj.val(status);
			//objs[i].className = objs[i].options[objs[i].selectedIndex].className;
			Change_Single_Status(i, objs[i]);
		}
<?php } ?>
	//}
}

function Check_Form() {
// Must be applied the input checking
//	var LateOK = true;
//	$('div.LateTimeWarningLayer').each(function() {
//		if ($(this).html() != "") {
//			LateOK = false;
//		}
//	});
//	
//	if (LateOK) {
<?php if($sys_custom['LessonAttendance_LaSalleCollege']){ ?>
		var selection_objs = document.getElementsByName('drop_down_status[]');
		var all_status_selected = true; 
		for(var i=0;i<selection_objs.length;i++){
			if(selection_objs[i].options[selection_objs[i].selectedIndex].value == '-1'){
				all_status_selected = false;
				break;
			}
		}
		if(!all_status_selected){
			alert('<?=$Lang['LessonAttendance']['RequestSelectAllStatus']?>');
			return;
		}
<?php } ?>

<?php if (get_client_region() == 'zh_TW') { ?>
    var selection_objs = document.getElementsByName('drop_down_status[]');
    var all_leave_type_selected = true;
    for(var i=0;i<selection_objs.length;i++){
        if(selection_objs[i].options[selection_objs[i].selectedIndex].value == '3'){
            if($("select[name='leaveType"+i+"']").val() == '') {
                all_leave_type_selected = false;
                break;
            }
        }
    }
    if(!all_leave_type_selected){
        alert('<?=$i_StudentAttendance_Report_PlsSelectLeaveType?>');
        return;
    }
<?php } ?>

		var qs = "<?=remove_qs_key($_SERVER['QUERY_STRING'], "Msg")?>";
		AlertPost(document.form3,'lesson_attendance_panel_update.php?'+qs,'<?=$i_SmartCard_Confirm_Update_Attend?>');
//	}
}
<?function remove_qs_key($url, $key) {
	$url = preg_replace('/(?:&|(\?))' . $key . '=[^&]*(?(1)&|)?/i', "$1", $url);
	return $url;
}?>
function drop_down_status_onchange() {
	setTimeout(function(){
        $(".drop_down_status").change();
    }, 50);
   
}
function Show_Time_Slot_Layer(LayerID,e) {
	var tempX = 0;
	var tempY = 0;
	
	if (document.all) {
		tempX = event.clientX + document.body.scrollLeft;
		tempY = event.clientY + document.body.scrollTop;
	} else {
		tempX = e.pageX;
		tempY = e.pageY;
	}
	
	if (tempX < 0){tempX = 0} else {tempX = tempX}
	if (tempY < 0){tempY = 0} else {tempY = tempY}
	
	$('span.member_list_group_layer').slideUp(); 
	$('span#'+LayerID).css('left',(tempX-200)+'px');
	//$('span#'+LayerID).css('top',tempY+'px');
	$('span#'+LayerID).slideDown();
}
function PrintPage($AttendOverviewID, LessonDate)
{
        newWindow("lesson_attendance_panel_print.php?SubjectGroupID=<?=$SubjectGroupID?>&LessonDate=<?=$LessonDate?>&AttendOverviewID=<?=$AttendOverviewID?>", 12);
}

function getLessonAttendanceReasons(pos)
{
	var returnArray = new Array();
	var status = $('#drop_down_status\\['+pos+'\\]').val();
	returnArray = eval('jArrayLessonReasons'+status);
	return returnArray;
}
</script>
<?php
$linterface->LAYOUT_STOP();
?>