<?php
// using kenneth chung
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-LessonAttendance"] || !$_SESSION["SSV_PRIVILEGE"]["plugin"]["attendancelesson"]) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$MODULE_OBJ['title'] = $Lang['LessonAttendance']['DailyLessonStatus'];

$linterface = new interface_html("popup.html");

$SubjectGroupID = $_REQUEST['SubjectGroupID'];
$SubjectGroupName = urldecode($_REQUEST['SubjectGroupName']);
$LessonDate = $_REQUEST['LessonDate'];
$RoomAllocationID = $_REQUEST['RoomAllocationID'];
$StartTime = $_REQUEST['StartTime'];
$EndTime = $_REQUEST['EndTime'];
$ServerPath = 'http://'.$_SERVER['SERVER_NAME'].':'.$_SERVER['SERVER_PORT'];

$linterface->LAYOUT_START();
?>
<div id="LessonAttendanceFlashLayer" name="LessonAttendanceFlashLayer" width="100%" style="height=800px;"></div>
<center>
	<input type="button" value="<?=$Lang['Btn']['Close']?>" onclick="window.close();">
</center>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.scrollTo-min.js"></script>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT?>templates/swf_object/swfobject.js"></script>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT?>templates/<?=$LAYOUT_SKIN?>/js/lesson_attendance.js"></script>
<?
$linterface->LAYOUT_STOP();
?>
<script>
var LessonAttendReturnMessage = '<?=$Lang['LessonAttendance']['LessonAttendanceDataSavedSuccess']?>';
	
function View_Session() {
	LessonAttendFlashVars = {
		RecordID: '<?=$SubjectGroupID?>',
		Name: '<?=$SubjectGroupName?>',
		LessonDate: '<?=$LessonDate?>',
		RoomAllocationID: '<?=$RoomAllocationID?>',
		StartTime: '<?=$StartTime?>',
		EndTime: '<?=$EndTime?>', 
		ServerPath: "<?=$ServerPath?>", 
		RecordType:"SubjectGroup",
		DefaultLang:"<?=$intranet_session_language?>"
	}
	
	embedFlash("LessonAttendanceFlashLayer", "/home/smartcard/attendance/lesson_attendance/main1.swf", LessonAttendFlashVars, "#869ca7", "100%", "900px", "9.0.0","NO");
}
View_Session();
</script>