<?php
// Editing by 
/*
 * 2018-05-02 (Carlos): Added summary statistics of student counting. 
 * 2017-08-09 (Carlos): $sys_custom['StudentAttendance']['HostelAttendance'] Created.
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libstudentattendance_ui.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libgrouping.php");
//include_once($PATH_WRT_ROOT."includes/libgrouping.php");
//include_once($PATH_WRT_ROOT."includes/libwordtemplates.php");

intranet_auth();
intranet_opendb();

if (!(($_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || $_SESSION['UserType'] == USERTYPE_STAFF) && $_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent'] && $sys_custom['StudentAttendance']['HostelAttendance']) ) {
	intranet_closedb();
	header ("Location: /");
	exit();
}

$lc = new libcardstudentattend2();

if($lc->HostelAttendanceGroupCategory == ''){
	$lc->HostelAttendanceGroupCategory = -1;
	//intranet_closedb();
	//header ("Location: ../../");
	//exit();
}

if(!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"]){
	$CurrentPageArr['eServiceHostelAttendance'] = 1;
}else{
	$CurrentPageArr['StudentAttendance'] = 1;
}
$CurrentPage = "PageDailyOperation_ViewHostelGroupStatus";

$StudentAttendUI = new libstudentattendance_ui();
$linterface = new interface_html();

if ($TargetDate == "")
{
	$TargetDate = date('Y-m-d');
}

$ts = strtotime($TargetDate);
$year = date("Y",$ts);
$month = date("m",$ts);

$hostel_admins = $lc->getHostelAttendanceAdminUsers(array('OnlyUserID'=>1));
$can_take_all_groups = in_array($_SESSION['UserID'],$hostel_admins) || $_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"];

$create_table_success = $lc->createTableHostelAttendanceDailyLog($year,$month);
$params = array('AcademicYearID'=>Get_Current_Academic_Year_ID(),'RecordDate'=>$TargetDate,'CategoryID'=>$lc->HostelAttendanceGroupCategory);
if(!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] && !in_array($_SESSION['UserID'],$hostel_admins)){
	$params['IsGroupStaff'] = true;
	$params['GroupUserID'] = $_SESSION['UserID'];
}
$groups = $lc->getHostelAttendanceGroupsConfirmedRecords($params);

$student_records = $lc->getHostelAttendanceRecords($year,$month,array('AcademicYearID'=>Get_Current_Academic_Year_ID(),'CategoryID'=>$lc->HostelAttendanceGroupCategory,'RecordDate'=>$TargetDate,'UserStatus'=>1));
$num_of_student = count($student_records);
$num_of_present_student = 0;
$num_of_absent_student = 0;
$num_of_nonconfirmed_student = 0;
for($i=0;$i<$num_of_student;$i++){
	$in_status = $student_records[$i]['InStatus'];
	if($in_status == ''){
		$num_of_nonconfirmed_student+=1;
	}else if($in_status == CARD_STATUS_PRESENT){
		$num_of_present_student += 1;
	}else if($in_status == CARD_STATUS_ABSENT){
		$num_of_absent_student += 1;
	}
}

$show_hostel_student_count = false;
$num_of_student_in_hostel = 0;
$num_of_student_not_in_hostel = 0;
if($sys_custom['StudentAttendance']['HostelAttendance_Reason']) {
	if ($TargetDate == date("Y-m-d")) {
		$show_hostel_student_count = true;
		$libgrouping = new libgrouping();

		$hostel_groups = $libgrouping->returnCategoryGroups($lc->HostelAttendanceGroupCategory);
		$hostel_groups = Get_Array_By_Key($hostel_groups, 'GroupID');
		$params = array('StartDate'=>$year.'-'.$month.'-01','EndDate'=>$TargetDate,'GroupID'=>$hostel_groups);
		$attendance_records = $lc->getStudentLastHostelAttendanceRecords($year,$month,$params);

		$month = $month - 1;
		if($month == 0) {
			$month = 12;
			$year = $year - 1;
		}
		$EndDate = date("Y-m-t", strtotime($year.'-'.$month.'-01'));
		$params = array('StartDate'=>date("Y-m-d", strtotime($year.'-'.$month.'-01')),'EndDate'=>$EndDate,'GroupID'=>$hostel_groups);
		$previous_records = $lc->getStudentLastHostelAttendanceRecords($year,$month,$params);
		$previous_records = BuildMultiKeyAssoc($previous_records, 'UserID');

		$present_records = array();
		$absent_records = array();
		foreach($attendance_records as $temp) {
			if($temp['InStatus'] == '') {
				if(isset($previous_records[$temp['UserID']])) {
					$temp = $previous_records[$temp['UserID']];
				}
			}

			if($temp['InStatus'] == CARD_STATUS_PRESENT) {
				$present_records[] = $temp;
			} else {
				$absent_records[] = $temp;
			}
		}

		$num_of_student_in_hostel = count($present_records);
		$num_of_student_not_in_hostel = count($absent_records);
	}
}

$TAGS_OBJ[] = array($Lang['StudentAttendance']['ViewHostelGroupStatus'], "", 0);
$MODULE_OBJ = $lc->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START(intranet_htmlspecialchars(urldecode($Msg)));

echo $StudentAttendUI->Get_Confirm_Selection_Form("form1","index.php","HostelGroup",$TargetDate,$DayType, 1);
?>
<br />
<form name="form2" method="post" action="">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
<!--
	<tr>
		<td>
			<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $i_StudentAttendance_Field_Date ?></td>
					<td class="tabletext" width="70%"><?=intranet_htmlspecialchars($TargetDate)?></td>
				</tr>
			</table>
		</td>
	</tr>
-->
	<tr>
		<td>
		<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center" style="margin-bottom:16px;">
			<tbody>
			<tr>
				<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$Lang['StudentAttendance']['TotalNumberOfStudent']?></td>
				<td class="tabletext" width="70%"><?=$num_of_student?></td>
			</tr>
            <?php if($sys_custom['StudentAttendance']['HostelAttendance_Reason']) { ?>
                <?php if($show_hostel_student_count) { ?>
                <tr>
                    <td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$Lang['StudentAttendance']['HostelGroupStudentPresentNumber']?></td>
                    <td class="tabletext" width="70%"><?=$num_of_student_in_hostel?></td>
                </tr>
                <tr>
                    <td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$Lang['StudentAttendance']['HostelGroupStudentAbsentNumber']?></td>
                    <td class="tabletext" width="70%"><?=$num_of_student_not_in_hostel?></td>
                </tr>
                <?php } ?>
            <?php } else { ?>
			<tr>
				<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$Lang['StudentAttendance']['TotalNumberOfPresentStudent']?></td>
				<td class="tabletext" width="70%"><?=$num_of_present_student?></td>
			</tr>
			<tr>
				<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$Lang['StudentAttendance']['TotalNumberOfAbsentStudent']?></td>
				<td class="tabletext" width="70%"><?=$num_of_absent_student?></td>
			</tr>
            <?php } ?>
			<tr>
				<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$Lang['StudentAttendance']['TotalNumberOfNonConfirmedStudent']?></td>
				<td class="tabletext" width="70%"><?=$num_of_nonconfirmed_student?></td>
			</tr>
			</tbody>
		</table>
		</td>
	</tr>
	
	<tr>
		<td>
			<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tbody>
					<tr class="tabletop">
						<td class="tabletop" width="30%"><?=$Lang['StudentAttendance']['HostelGroup']?></td>
						<td class="tabletop" width="35%"><?=$i_StudentAttendance_Field_ConfirmedBy?></td>
						<td class="tabletop" width="35%"><?=$i_StudentAttendance_Field_LastConfirmedTime?></td>
					</tr>
					<?php 
					for($i=0;$i<count($groups);$i++){
						echo '<tr class="tablerow'.($i % 2==0?'1':'2').'">
				  				<td class="tabletext"><a class="tablelink" href="view_student.php?GroupID='.urlencode($groups[$i]['GroupID']).'&TargetDate='.urlencode($TargetDate).'">'.$groups[$i]['Title'].'</a></td>
				  				<td class="tabletext">'.$groups[$i]['ConfirmedUser'].'</td>
				  				<td class="tabletext">'.($groups[$i]['ConfirmedDate']!=''? $groups[$i]['ConfirmedDate']:'-').'</td>
				  			</tr>';
					}
					?>
				</tbody>
			</table>
		</td>
	</tr>
	<?php if($can_take_all_groups){ ?>
	<tr>
		<td>
			<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr>
			    	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
			    </tr>
			    <tr>
				    <td align="center">
						<?= $linterface->GET_ACTION_BTN($Lang['StudentAttendance']['TakeAttendanceForAllHostelGroups'], "button", "window.location.href='view_student.php?TargetDate=".urlencode($TargetDate)."'");?>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<?php } ?>
</table>
</form>
<br />
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>