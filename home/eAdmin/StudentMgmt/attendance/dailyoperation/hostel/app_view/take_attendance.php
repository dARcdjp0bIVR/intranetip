<?php
/**
 * editing by: 
 *
 * Change log:
 * 2018-05-02 (Carlos): Added UserStatus=1 to getHostelAttendanceRecords() params to only fetch active students.
 *
 */
$PATH_WRT_ROOT = "../../../../../../../";



switch (strtolower($_GET['parLang'])) {
    case 'en':
        $intranet_hardcode_lang = 'en';
        break;
    default:
        $intranet_hardcode_lang = 'b5';
}

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libsmartcard.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libadminjob.php");
include_once($PATH_WRT_ROOT."includes/libteaching.php");
include_once($PATH_WRT_ROOT."includes/libstudentattendance_ui.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_hardcode_lang.php");
include_once($PATH_WRT_ROOT."lang/eclassapp_lang.$intranet_hardcode_lang.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT.'includes/eClassApp/libeClassApp.php');
include_once($PATH_WRT_ROOT."includes/libwordtemplates.php");
include_once($PATH_WRT_ROOT.'includes/libeclass_ws.php');
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

$UserID = $uid;
$_SESSION['UserID'] = $uid;
$_SESSION['intranet_session_language'] = $intranet_hardcode_lang;
$libeClassApp = new libeClassApp();
$isTokenValid = $libeClassApp->isTokenValid($token, $uid, $ul);

$charactor = standardizeFormGetValue($_GET['charactor']);

if(!$isTokenValid) {
    echo $i_general_no_access_right	;
    exit;
}



 if(!isset($charactor)||$charactor==""||!isset($date)||$date==""||!isset($timeslots)||$timeslots=="")header("Location:attendance_list.php?date=$date&charactor=$charactor&token=$token&uid=$uid&ul=$ul&parLang=$parLang");

$show_date = date("Y/m/d",strtotime($date));
$ToDate = date("Y-m-d",strtotime($date));
$ts_record = strtotime($date);
$txt_year = date('Y',$ts_record);
$txt_month = date('m',$ts_record);
$txt_day = date('d',$ts_record);
$today = date('m/d/Y');
$today_record = strtotime($today);
$page_load_time = time()+1;

if(!isset($status_switch) || $status_switch=="")
{
    $status_switch = "all";
}

//intranet_auth();
intranet_opendb();
$libdb = new libdb();
$lu = new libuser();
$SettingList = array();
$GeneralSetting = new libgeneralsettings();
if($sys_custom['StudentAttendance']['HostelAttendance_Reason']) {
	$SettingList[] = "'HostelAttendanceDisablePastDate'";
	$SettingList[] = "'HostelAttendanceDisableFutureDate'";
}
$Settings = $GeneralSetting->Get_General_Setting('StudentAttendance',$SettingList);
$libws = new libeclass_ws();
$lcardattend = new libcardstudentattend2();
$lcardattend->retrieveSettings();
$lcardattend->createTable_LogAndConfirm();
$linterface = new interface_html();
$card_log_table_name = "CARD_STUDENT_DAILY_LOG_".$txt_year."_".$txt_month;
$conds = "";
$TABLES = "";
$group_name = "";
$student_list = "";
$reason = "";
$present_count = 0;
$absent_count = 0;
$NA_count = 0;
$normalLeave_count = 0;
$earlyLeave_count = 0;
$order = "";
$status_array = "";
$student_array = array();



$lword = new libwordtemplates();


if(!isset($msg) || $msg=="")
{
    $msg=0;
}

#Get student list fron group
$lc = new libcardstudentattend2();
$hostel_admins = $lc->getHostelAttendanceAdminUsers(array('OnlyUserID'=>1));
$can_take_all_groups = in_array($_SESSION['UserID'],$hostel_admins) || $_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"];


#group name display
$params = array('AcademicYearID'=>Get_Current_Academic_Year_ID(),'RecordDate'=>$ToDate,'CategoryID'=>$lc->HostelAttendanceGroupCategory);
if(!$can_take_all_groups){
    $params['GroupID'] = $GroupID;
    $params['IsGroupStaff'] = true;
    $params['GroupUserID'] = $_SESSION['UserID'];
}else if($GroupID!=''){
    $params['GroupID'] = $GroupID;
}
$groups = $lc->getHostelAttendanceGroupsConfirmedRecords($params);
$GroupID = $_GET['id'];
if ($GroupID != ''){
    $group_name = $groups[0]['Title'];
} else {
    $space = '';
    if ($_SESSION['intranet_session_language'] == 'en'){
        $space = ' ';
    }
    $group_name = $Lang['General']['All'].$space.$Lang['StudentAttendance']['HostelGroup'];
//     $group_name_arry = array();
//     for($i=0;$i<count($groups);$i++){
//         $group_name_arry[$i] = $groups[$i]['Title'];
//     }
//     $group_name = implode(",",$group_name_arry);
}

// #get the settings for show student info or not
$sql="select * from APP_USER_SETTING where UserID=".$UserID." and SettingName='eAttendance_ShowStudentInfoClassAndNo'";
$getShowStudentNameArray = $libdb->returnArray($sql);
if(sizeof($getShowStudentNameArray)==0){
    $isShowStudentInfo = '0';
}else{
    $isShowStudentInfo = $getShowStudentNameArray[0]['SettingValue'];
}

#get the settings for show student name or not
$sql="select * from APP_USER_SETTING where UserID=".$UserID." and SettingName='eAttendance_ShowStudentInfoName'";
$getShowStudentNameArray = $libdb->returnArray($sql);
if(sizeof($getShowStudentNameArray)==0){
    $isShowStudentInfoName = '0';
}else{
    $isShowStudentInfoName = $getShowStudentNameArray[0]['SettingValue'];
}

#get the settings for show Group name or not
$sql="select * from APP_USER_SETTING where UserID=".$UserID." and SettingName='eAttendance_ShowHostelGroup'";
$getShowStudentNameArray = $libdb->returnArray($sql);
if(sizeof($getShowStudentNameArray)==0){
    $isShowHostelGroup = '0';
}else{
    $isShowHostelGroup = $getShowStudentNameArray[0]['SettingValue'];
}

#get the settings for show student class and class Number or not
$sql="select * from APP_USER_SETTING where UserID=".$UserID." and SettingName='eAttendance_ShowStudentInfoClassAndNo'";
$getShowStudentNameArray = $libdb->returnArray($sql);
if(sizeof($getShowStudentNameArray)==0){
    $isShowStudentInfoClassAndNo = '0';
}else{
    $isShowStudentInfoClassAndNo = $getShowStudentNameArray[0]['SettingValue'];
}


//disable submit
$disable=false;
if ($Settings['EnableEntryLeavePeriod']==1) {
    $FilterInActiveStudent .= "
        INNER JOIN
        CARD_STUDENT_ENTRY_LEAVE_PERIOD as selp
        on a.UserID = selp.UserID
        	AND
        	'".$ToDate."' between selp.PeriodStart and CONCAT(selp.PeriodEnd,' 23:59:59')
        ";
}

if($Settings['EditDaysBeforeRecord']==1&&($ts_record<=$today_record)){
    
    $daycount=ceil(($today_record-$ts_record)/86400)+1;
    if($daycount>$Settings['EditDaysBeforeRecordSelect']){
        $disable=true;
    }
}

for($i=0;$i<sizeof($words_absence);$i++){
    $reason_absent .= addslashes(" <li data-icon='false' id=rea_pool_$i><a href='javascript:input_reason($i);'>".$words_absence[$i]."</a></li>");
}


if($charactor=="hostelGroup"){
    $record_params = array('RecordDate'=>$ToDate,'AcademicYearID'=>Get_Current_Academic_Year_ID(),'CategoryID'=>$lc->HostelAttendanceGroupCategory,'UserStatus'=>1);
    if(!$can_take_all_groups){
        $record_params['GroupID'] = $GroupID;
    }else if($GroupID!=''){
        $record_params['GroupID'] = $GroupID;
    }
    
    $records = $lc->getHostelAttendanceRecords($txt_year, $txt_month, $record_params);
//     debug_pr($records);
//     die();
    if($timeslots=="in"){
        $popupStatusLabel = $Lang['StudentAttendance']['InTime']." (hh:mm)";
    }else {
        $popupStatusLabel =  $Lang['StudentAttendance']['OutTime']." (hh:mm)";
    }

   
            
}
$all_count = count($records);

    for($j=0;$j<sizeof($records);$j++){
        

            $user_id =$records[$j][UserID];
            $studentClassName=$records[$j][ClassName];
            $studentClassNumber=$records[$j][ClassNumber];
            $Student_name=str_replace(" (".$studentClassName."-".$studentClassNumber.")", "", $records[$j][StudentName]);
            $group_id=$records[$j][GroupID];
            $group_name=$records[$j][GroupName];
            $record_id=$records[$j][RecordID];
            $inStatus=$records[$j][InStatus];
            $inTime=$records[$j][InTime]!=""?date("H:i",strtotime($records[$j][InTime])):"";
            $outStatus=$records[$j][OutStatus];
            $outTime=$records[$j][OutTime]!=""?date("H:i",strtotime($records[$j][OutTime])):"";
            $remark=$records[$j][Remark];
            
            
            if($timeslots=="in"){
                if(($taken==1||$GroupID=="") && isset($inStatus)){
                    $status=$inStatus;
                } else{
                    $status=1;
                }
            }else if($timeslots=="out"){
//                 if($inStatus==1){
//                     $changeStasusJS ='';
//                 } else {
                    $changeStasusJS ='javascript:choose('.$user_id.');';
//                 }
                
                if(($taken==1||$GroupID=="") && isset($outStatus)){
                    $status=$outStatus;
                }else{
                    $status=0;
                }
            }
                
            $status_array.="<input type='hidden' id='status_".$user_id."' name='status_".$user_id."' value='".$status."'>";
             
            if($status==0) $present_count++;
            if($status==1) $absent_count++;
            
            if($status==0) $NA_count++;
            if($status==1) $normalLeave_count++;
            if($status==2) $earlyLeave_count++;

            $recordID_array.="<input type='hidden' id='recordID_".$user_id."' name='recordID_".$user_id."' value='".$record_id."'>";
            
            $arrival_time.="<input type='hidden' id='arrival_time_".$user_id."' name='arrival_time_".$user_id."' value='".$inTime."'>";
            $leave_time.="<input type='hidden' id='leave_time_".$user_id."' name='leave_time_".$user_id."' value='".$outTime."'>";
            $redordID.= "<input type='hidden' id='redordID_".$user_id."' name='redordID_".$user_id."' value='".$redord_id."'>";
            
            $student_array[] = $user_id;
            $photo = $lu->GET_OFFICIAL_PHOTO_BY_USER_ID($user_id);

            
//             //default the $RequestLeaveSession,$PlayTruantSession,$OfficalLeaveSession,$AbsentSession
//             if($sys_custom['SmartCardAttendance_StudentAbsentSession'])
//             {
//                 if ($LateSession==""&&$AbsentSession == "" && $RequestLeaveSession == "" && $PlayTruantSession == "" && $OfficalLeaveSession == "") {
                    
//                     if ($status == CARD_STATUS_ABSENT) {
                        
// //                         $LateSession = $PlayTruanstatustSession = $OfficalLeaveSession = $AbsentSession = 0;
//                         $RequestLeaveSession = 1;
//                         if(isset($Settings['DefaultNumOfSessionForAbsenteesism']) && $Settings['DefaultNumOfSessionForAbsenteesism'] != -1){
//                             $PlayTruantSession = $Settings['DefaultNumOfSessionForAbsenteesism'];
//                         }
//                         if(isset($Settings['DefaultNumOfSessionForLeave']) && $Settings['DefaultNumOfSessionForLeave'] != -1){
//                             $RequestLeaveSession = $Settings['DefaultNumOfSessionForLeave'];
//                         }
//                         if(isset($Settings['DefaultNumOfSessionForAbsent']) && $Settings['DefaultNumOfSessionForAbsent'] != -1){
//                             $AbsentSession = $Settings['DefaultNumOfSessionForAbsent'];
//                         }
//                     }
               
//                     else {
//                         $LateSession = $RequestLeaveSession = $PlayTruantSession = $OfficalLeaveSession = $AbsentSession = 0;
//                     }
                    
//                 }
//             }
            
            $StudentTags = "<div class='tagArea'><p class='StudentName'>".$Student_name."</p>
                         <p class='ClassAndNum'><span class='openBracket'>(</span>".$studentClassName." - ".$studentClassNumber."<span class='closeBracket'>)</span></p>
                        <p class='Group'>".$group_name."</p></div>";
          
            
            if($timeslots=="in"){
                if($status==0){
                    $student_list .= "<li id='li_".$user_id."'><input type='checkbox' name='students_status[]' value='".$user_id."' id='c_".$user_id."' style='display:none' >
    		    		              <a href='javascript:choose(".$user_id.");' id='a_".$user_id."'>
    		    		                <img src='".$photo[1]."' class=ui-li-thumb style='width:80px;height:104px;'>
    					                ".$StudentTags."
    					                <h2 style='color:#309100;' id='h2_".$user_id."'>".$Lang['StudentAttendance']['HostelAttend']."</h2>
    					              </a>
    					          </li>";
                } else if ($status==1){
                    $student_list .= "<li id='li_".$user_id."'><input type='checkbox' name='students_status[]' value='".$user_id."' id='c_".$user_id."' style='display:none'>
    					              <a href='javascript:choose(".$user_id.");' style='border:2px;border-style: solid; border-color:#fe5353;' id='a_".$user_id."'>
    			                        <img src='".$photo[1]."' class=ui-li-thumb style='width:80px;height:104px;image-orientation:90deg'>
    				                    ".$StudentTags."
    				                    <p class='absent' id='ab_".$user_id."'></p>
    				                    <h2 style='color:#d92b2b;' id='h2_".$user_id."'>".$Lang['StudentAttendance']['HostelAbsent']."</h2>
    			                      </a>
    			                  </li>";
                }
            }else {
                if($inStatus ==1){
                    $attendance = "<p class='absent' id='ab_".$user_id."'></p>";
                    $status = 0;
                } else {
                    $attendance = "";
                }
                if($status==0){
                    $student_list .= "<li id='li_".$user_id."'><input type='checkbox' name='students_status[]' value='".$user_id."' id='c_".$user_id."' style='display:none' >
    		    		              <a href='".$changeStasusJS."' style='border:2px;border-style: solid; border-color:#fe5353;' id='a_".$user_id."'>
    		    		                <img src='".$photo[1]."' class=ui-li-thumb style='width:80px;height:104px;'>
    					                ".$StudentTags.
    					                $attendance."
    					                <h2 style='color:#d92b2b;' id='h2_".$user_id."'>N.A.</h2>
    					              </a>
    					          </li>";
                } else if($status==1){
                    $student_list .= "<li id='li_".$user_id."'><input type='checkbox' name='students_status[]' value='".$user_id."' id='c_".$user_id."' style='display:none'>
    					              <a href='javascript:choose(".$user_id.");' style='border:2px;border-style: solid; border-color:#329700;' id='a_".$user_id."'>
    	    		                    <img src='".$photo[1]."' class=ui-li-thumb style='width:80px;height:104px;'>
                                        ".$StudentTags."
    				                    <h2 style='color:#309100;' id='h2_".$user_id."'>".$Lang['StudentAttendance']['HostelNormalLeave']."</h2>
    				                  </a>
    				              </li>";
                } else if ($status==2){
                    $student_list .= "<li id='li_".$user_id."'><input type='checkbox' name='students_status[]' value='".$user_id."' id='c_".$user_id."' style='display:none'>
    					              <a href='javascript:choose(".$user_id.");' style='border:2px;border-style: solid; border-color:#00a7d8;' id='a_".$user_id."'>
    			                        <img src='".$photo[1]."' class=ui-li-thumb style='width:80px;height:104px;image-orientation:90deg'>
    				                    ".$StudentTags."
    				                    <h2 style='color:#0094bf;' id='h2_".$user_id."'>".$Lang['StudentAttendance']['HostelEarlyLeave']."</h2>
    			                      </a>
    			                  </li>";
                }
            }
            
            $reason_submit .="<input type='hidden' value='".$remark."' id='rmk_".$user_id."' name='rmk_".$user_id."'>";
    }
//     $inoutTimeTextfield =  '<input type="text" id="'.$timeslots.'Time'" name="'.$timeslots.'Time[]" value="'.$time.'" size="12" onchange="onEditTimeChanged('.$user_id.',\''.$timeslots.'\')" />';
    $popup = "<div data-role='popup' id='popup1' data-theme='a' class='ui-corner-all'>
    <div style='padding:10px 20px;'>
    <p id='statusLabel'>".$popupStatusLabel."</p><input  type='text' value = '' id='input_".$timeslots."Time' data-theme='a'><p id= 'remarkLabel'>
    <p id='worn_timeFomat' style='color:#d92b2b;'></p>".$Lang['StudentAttendance']['Remark'].
    "</p><input type='text' value = '' id='remark1' data-theme='a'><p>
						  <div  align='center'>
							   <a href='' data-role='button' data-inline='true' id='cancle_btn1'>".$Lang['Btn']['Cancel']."</a>
							   <a href='' data-role='button' data-inline='true' id='confirm_btn1'>".$Lang['Btn']['Confirm']."</a>
						  </div>
		             </div>
		        </div>";
    
    if($timeslots=="in")
    {
        $StatusField = "inStatus";
        $statusSwitch ='<select name="status_switch" id="status_switch" data-mini="true" onchange="select_filter()">
            
				        <option value="all" id="all">'.$Lang["Btn"]["All"].'('.$all_count.')</option>
				            
				        <option value="present" id="Present">'.$Lang['StudentAttendance']['HostelAttend'].' ('.$present_count.')</option>
				            
						<option value="absent" id="Absent">'.$Lang['StudentAttendance']['HostelAbsent']. '('.$absent_count.')</option>
						    
				    </select>';
        
        $statusSwitchMultiselect = '<select  name="status_switch_multiselect" id="status_switch_multiselect" data-mini="true" onchange="select_change()">
            
                                <option selected disabled value="" id="turn_to">'.$Lang["StudentAttendance"]["TurnTo"].'</option>
                                    
    					        <option value="present" id="Present">'.$Lang['StudentAttendance']['HostelAttend'].'</option>
    					            
    					        <option value="absent" id="Absent">'.$Lang['StudentAttendance']['HostelAbsent'].'</option>
    					            
					       	    </select>';
        
        
        
        
        
        
    } else{
        
        $StatusField = "outStatus";
        $statusSwitch ='<select name="status_switch" id="status_switch" data-mini="true" onchange="select_filter()">
            
								        <option value="all" id="all">'.$Lang["Btn"]["All"]. '('.$all_count.')</option>
								            
                                        <option value="NA" id="NA">N.A. ('.$NA_count.')</option>
                                            
								        <option value="NormalLeave" id="NormalLeave">'.$Lang["StudentAttendance"]["NormalLeave"].'('.$normalLeave_count.')</option>
								            
								        <option value="EarlyLeave" id="EarlyLeave">'.$Lang["StudentAttendance"]["EarlyLeave"].'('.$earlyLeave_count.')</option>
								            
								    </select>	';
        
        
        $statusSwitchMultiselect = '<select  name="status_switch_multiselect" id="status_switch_multiselect" data-mini="true" onchange="select_change()">
            
                                <option selected disabled value="" id="turn_to">'.$Lang["StudentAttendance"]["TurnTo"].'</option>
                                    
    					        <option value="NA" id="NA">N.A.</option>
                                    
    					        <option value="NormalLeave" id="NormalLeave">'.$Lang["StudentAttendance"]["NormalLeave"].'</option>
    					            
								<option value="EarlyLeave" id="EarlyLeave">'.$Lang["StudentAttendance"]["EarlyLeave"].'</option>
								    
					       	    </select>';
        
    }
    
    

$student_array_string = implode(";",$student_array);
//  debug_pr($student_array);
//  die();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<script src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery-1.11.1.min.js"></script>
<script src="<?=$PATH_WRT_ROOT?>templates/jquery.mobile/jquery.mobile.min.js"></script>

<script>
    var array_string = '<?= $student_array_string?>' ;
    var student_array = array_string.split(';');
    
	jQuery(document).ready( function() {
	     	     	     
	    var disable = '<?=$disable?1:0;?>';
	    var charactor = '<?=$charactor?>';
	    var isShowStudentInfo = '<?=$isShowStudentInfo?>';
	    var isShowStudentInfoName = '<?=$isShowStudentInfoName?>';
	    var isShowHostelGroup = '<?=$isShowHostelGroup?>';
	    var isShowStudentInfoClassAndNo = '<?=$isShowStudentInfoClassAndNo?>';
        if(disable==1){			 	
		   $("#mutiple_select").hide(); 
		   $("#header").height('60px');
		   $("#footer").hide();
		}
		$("#status_select").hide();
		change_dropdown_icon();
		//defult not show tags
		$(".openBracket").hide(); 
		$(".closeBracket").hide();
        $(".Group").hide();
        $(".StudentName").hide();


        
        if(isShowStudentInfo=='1'){
			$('#SelectAll').prop("checked", true).checkboxradio('refresh');
		} else{
			$('#SelectAll').prop("checked", false).checkboxradio('refresh');		
		}    

		if(isShowStudentInfoName=='1'){
			$('#showStudentName').prop("checked", true).checkboxradio('refresh');			
		} else{
			$('#showStudentName').prop("checked", false).checkboxradio('refresh');	
		}	
		
		if(isShowHostelGroup =='1'){
			$('#showGroup').prop("checked", true).checkboxradio('refresh');
		} else{
			$('#showGroup').prop("checked", false).checkboxradio('refresh');
		}

		if(isShowStudentInfoClassAndNo =='1'){
			$('#showClassAndNum').prop("checked", true).checkboxradio('refresh');
		} else{
			$('#showClassAndNum').prop("checked", false).checkboxradio('refresh');
		}

		CheckingCheckboxes();

		$("#mode").val("single");
		document.getElementById("<?=$status_switch?>").selected=true;
		$('select').selectmenu('refresh', true);
		
		var msg = <?=$msg?>;
		if(msg=="99") {
			 alert("<?=$i_StudentAttendance_Warning_Data_Outdated?>");
	    }
	    
		$("a").on("taphold",function(){
			document.getElementById("worn_timeFomat").innerHTML="";
			 var timeslot = <?= "'".$timeslots."'" ?>;
             var id = $(this).attr("id").substr(2);
             var status = $('#status_'+id).val();
             var inputTimefield = "input_"+timeslot+"Time";
             if((timeslot=='in'&& status==1)||(timeslot=='out'&& status==0)){
            	 document.getElementById(inputTimefield).readOnly = true;
             } else {
                 
            	 document.getElementById(inputTimefield).readOnly = false;	
             }  
             var arrivaltime = $('#arrival_time_'+id).val();
             var leavetime = $('#leave_time_'+id).val();
             if(document.getElementById(inputTimefield).readOnly){
            	 $('#'+inputTimefield).val(''); 
             }else if(timeslot=='in' && arrivaltime!="" &&  document.getElementById(inputTimefield).readOnly==false){
        	 	$('#'+inputTimefield).val(arrivaltime);            	 	
        	 }else if(timeslot=='out' && leavetime!="" && document.getElementById(inputTimefield).readOnly==false){
        		$('#'+inputTimefield).val(leavetime); 
        	 }else{
        		 $('#'+inputTimefield).val('');  
             }  

              
             
        	 var remark = $('#rmk_'+id).val()            	 
        	 $('#remark1').val(remark);     
        	 
        	 

        
        	 $('ul').listview('refresh');
        	  
             $('#cancle_btn1').attr('href','javascript:cancle1('+id+')');
        	 $('#confirm_btn1').attr('href','javascript:reason_confirm1('+id+')');                    	 
             $('#link1').trigger("click");
                            
		});        

// 	    $( "#popup1" ).popup({
// 	        afteropen: function() {
//             	  $("#reason1").focus();
// 	        }
// 	    }); 
		 
// 		$( "#popup2" ).popup({
// 	        afteropen: function(event, ui) {
// 	        }
// 		});   

	});

	function show_status_select(){
	     $("#mode").val("single");
	     $("img.selected").remove();
		 $("#status_select").hide();
		 $("#mutiple_select").show(); 
		 $(".custom_collapsible").show();
	}

	function show_mutiple_select(){
		$("#mode").val("multiple");
		 $("#status_select").show();
	     $("#mutiple_select").hide();
	     $(".custom_collapsible").hide();
	}
		
	function refresh(){
		
		   document.form1.action="take_attendance.php";
		   document.form1.submit();
	}
	
	
    function choose(userID){
        var timeslot = '<?= $timeslots?>';
          if($("#mode").val()=="multiple"){
		        if(!$("#c_"+userID)[0].checked){
		    	   $("#c_"+userID).prop("checked",true).checkboxradio("refresh");
		    	   $("#a_"+userID).append("<img src='<?=$image_path?>/<?=$LAYOUT_SKIN?>/smartcard/icon_selected.png' class='selected' id='s_"+userID+"'>");
		        }
		        else{
			       $("#c_"+userID).prop("checked",false).checkboxradio("refresh");	
			       $("#s_"+userID).remove();
			       
			    }
          } 
          else if (timeslot=='in'){
        	  var taken = "<?=$taken?>";
              if($("#status_"+userID).val()==0){
                 $("#ab_"+userID).remove();

                 $("#h2_"+userID).remove();
             	 $("#a_"+userID).css({"border":"2px","border-style":"solid","border-color":"#fe5353"}).append("<h2 style='color:#d92b2b;' id='h2_"+userID+"'><?=$Lang['StudentAttendance']['HostelAbsent']?></h2> <p class='absent' id='ab_"+userID+"'></p>");
             	             	 
                 $("#status_"+userID).val(1);
                 
                 var present_count = parseInt($("#present_count").val())-1;
                 var absent_count = parseInt($("#absent_count").val())+1;
                 $("#Absent").text("<?=$Lang['StudentAttendance']['HostelAbsent']?> ("+absent_count+")");
                 $("#Present").text("<?=$Lang['StudentAttendance']['HostelAttend']?> ("+present_count+")");
                 $("#absent_count").val(absent_count);
                 $("#present_count").val(present_count);                 
                 $('select').selectmenu('refresh', true);
                 
                 var confirmed = $("#confirmed_"+userID).val(); 
                          	 	             
 	             } else if($("#status_"+userID).val()==1){

                  $("#ab_"+userID).remove();
  				
			      $("#h2_"+userID).remove();
                  $("#a_"+userID).css({"border":"2px","border-style":"solid","border-color":"#75b70d"}).append("<h2 style='color:#309100;' id='h2_"+userID+"'><?=$Lang['StudentAttendance']['HostelAttend']?></h2>");


                  $("#status_"+userID).val(0);

                  var absent_count = parseInt($("#absent_count").val())-1;
                  var present_count = parseInt($("#present_count").val())+1;
                  $("#Present").text("<?=$Lang['StudentAttendance']['HostelAttend']?> ("+present_count+")");
                  $("#Absent").text("<?=$Lang['StudentAttendance']['HostelAbsent']?> ("+absent_count+")");
                  $("#present_count").val(present_count);
                  $("#absent_count").val(absent_count);                 
                  $('select').selectmenu('refresh', true);

                 
                       	             
  	              }
               }else if (timeslot=='out'){
            	   var taken = "<?=$taken?>"; 
            	   if($("#status_"+userID).val()==0){
            		   $("#h2_"+userID).remove();
            		   $("#a_"+userID).css({"border":"2px","border-style":"solid","border-color":"#329700"}).append("<h2 style='color:#309100;' id='h2_"+userID+"'><?=$Lang['StudentAttendance']['HostelNormalLeave']?></h2>");
                   	             	 
                     $("#status_"+userID).val(1);

                     var NA_count = parseInt($("#NA_count").val())-1;
                     var normalLeave_count = parseInt($("#normalLeave_count").val())+1;
                     $("#NA").text("N.A. ("+NA_count+")");
                     $("#NormalLeave").text("<?=$Lang['StudentAttendance']['HostelNormalLeave']?> ("+normalLeave_count+")");
                     $("#NA_count").val(NA_count);
                     $("#normalLeave_count").val(normalLeave_count);                 
                     $('select').selectmenu('refresh', true);
                       
                     var confirmed = $("#confirmed_"+userID).val(); 
                                	 	             
   	             }else if($("#status_"+userID).val()==1){
   	            	$("#h2_"+userID).remove();
   	            	$("#a_"+userID).css({"border":"2px","border-style":"solid","border-color":"#00a7d8"}).append("<h2 style='color:#0094bf;' id='h2_"+userID+"'><?=$Lang['StudentAttendance']['HostelEarlyLeave']?></h2>");
                 	 
                 	             	 
                     $("#status_"+userID).val(2);

                     var normalLeave_count = parseInt($("#normalLeave_count").val())-1;
                     var earlyLeave_count = parseInt($("#earlyLeave_count").val())+1;
                     $("#NormalLeave").text("<?=$Lang['StudentAttendance']['HostelNormalLeave']?> ("+normalLeave_count+")");
                     $("#EarlyLeave").text("<?=$Lang['StudentAttendance']['HostelEarlyLeave']?> ("+earlyLeave_count+")");
                     $("#normalLeave_count").val(normalLeave_count);
                     $("#earlyLeave_count").val(earlyLeave_count);                 
                     $('select').selectmenu('refresh', true);
                     
                     var confirmed = $("#confirmed_"+userID).val(); 
                              	 	             
 	             }else if($("#status_"+userID).val()==2){
 	            	$("#h2_"+userID).remove();
 	            	$("#a_"+userID).css({"border":"2px","border-style":"solid","border-color":"#fe5353"}).append("<h2 style='color:#d92b2b;' id='h2_"+userID+"'>N.A.</h2>");
                	 
                   	             	 
                     $("#status_"+userID).val(0);
                     
                     var earlyLeave_count = parseInt($("#earlyLeave_count").val())-1;
                     var NA_count = parseInt($("#NA_count").val())+1;
                     $("#NA").text("N.A. ("+NA_count+")");
                     $("#EarlyLeave").text("<?=$Lang['StudentAttendance']['HostelEarlyLeave']?> ("+earlyLeave_count+")");
                     $("#NA_count").val(NA_count);
                     $("#earlyLeave_count").val(earlyLeave_count);                 
                     $('select').selectmenu('refresh', true);
                       
                     var confirmed = $("#confirmed_"+userID).val(); 
                                	 	             
   	             }
            	   
               }
          }  
    

    function choose_all(){

		 for(var i=0; i<student_array.length;i++){
			 var userID = student_array[i];
             if($("#li_"+userID).css("display")=="block"){
				 if(!$("#c_"+userID)[0].checked){
	            	 $("#c_"+userID).prop("checked",true).checkboxradio("refresh");
		             $("#a_"+userID).append("<img src='<?=$image_path?>/<?=$LAYOUT_SKIN?>/smartcard/icon_selected.png' class='selected' id='s_"+userID+"'>");
	             }
             }
         }
    }

    function turn_present(){
         var present_count = 0;
         var absent_count = 0;
         var taken = "<?=$taken?>";
         
		 for(var i=0; i<student_array.length;i++){
			 var userID = student_array[i];
			 
             if($("#c_"+userID)[0].checked){

                 $("#h2_"+userID).remove();
                 $("#ab_"+userID).remove();
                 $("#a_"+userID).css({"border":"2px","border-style":"solid","border-color":"#75b70d"}).append("<h2 style='color:#309100;' id='h2_"+userID+"'><?=$Lang['StudentAttendance']['HostelAttend']?></h2>");


                 $("#status_"+userID).val(0);
            	 $("#c_"+student_array[i]).prop("checked",false).checkboxradio("refresh");
			     $("#s_"+userID).remove();	  

                  var confirmed = $("#confirmed_"+userID).val();
 	                                        
             }

             if($('#status_'+userID).val()==0) present_count++;
             else if($('#status_'+userID).val()==1) absent_count++;
             
         }
		 $("#Absent").text("<?=$Lang['StudentAttendance']['HostelAbsent']?> ("+absent_count+")");
         $("#Present").text("<?=$Lang['StudentAttendance']['HostelAttend']?> ("+present_count+")");
         $("#absent_count").val(absent_count);
         $("#present_count").val(present_count);                 
         $('select').selectmenu('refresh', true);
         
    }

    function turn_absent(){
    	 var present_count = 0;
         var absent_count = 0;
         var taken = "<?=$taken?>";
         
		 for(var i=0; i<student_array.length;i++){
			 var userID = student_array[i];
             if($("#c_"+userID)[0].checked){
                 $("#h2_"+userID).remove();
                 $("#ab_"+userID).remove();


                 $("#a_"+userID).css({"border":"2px","border-style":"solid","border-color":"#fe5353"}).append("<p class='absent' id='ab_"+userID+"'></p><h2 style='color:#d92b2b;' id='h2_"+userID+"'><?=$Lang['StudentAttendance']['HostelAbsent']?></h2> ");
                 

              	          	 		             
                 $("#status_"+userID).val(1);                 
            	 $("#c_"+student_array[i]).prop("checked",false).checkboxradio("refresh");
			     $("#s_"+userID).remove();

		         var confirmed = $("#confirmed_"+userID).val();
	 	         
             }

             if($('#status_'+userID).val()==0) present_count++;
             else if($('#status_'+userID).val()==1) absent_count++;
         }
         $("#Present").text("<?=$Lang['StudentAttendance']['HostelAttend']?> ("+present_count+")");
         $("#Absent").text("<?=$Lang['StudentAttendance']['HostelAbsent']?> ("+absent_count+")");
         $("#present_count").val(present_count);
         $("#absent_count").val(absent_count);
         $('select').selectmenu('refresh', true);
    
    }

    function turn_NA(){
    	 var NA_count = 0;
         var normalLeave_count = 0;
         var earlyLeave_count = 0;
         var taken = "<?=$taken?>";
         
 		 for(var i=0; i<student_array.length;i++){
			 var userID = student_array[i];
             if($("#c_"+userID)[0].checked){
                 
                 
            	 	$("#h2_"+userID).remove();
            	 	$("#a_"+userID).css({"border":"2px","border-style":"solid","border-color":"#fe5353"}).append("<h2 style='color:#d92b2b;' id='h2_"+userID+"'>N.A.</h2>");
                  	$("#ab_"+userID).remove();           	 		                      	 		                              	                         	                   	
                 

                 $("#status_"+userID).val(0);                 
            	 $("#c_"+student_array[i]).prop("checked",false).checkboxradio("refresh");
			     $("#s_"+userID).remove();

			     var confirmed = $("#confirmed_"+userID).val();
             }
             
             
             if($('#status_'+userID).val()==0) NA_count++;
             else if($('#status_'+userID).val()==1) normalLeave_count++;
             else if($('#status_'+userID).val()==2) earlyLeave_count++;
 		 }
 		$("#NA").text("N.A. ("+NA_count+")");                    
         $("#NormalLeave").text("<?=$Lang['StudentAttendance']['HostelNormalLeave']?> ("+normalLeave_count+")");
         $("#EarlyLeave").text("<?=$Lang['StudentAttendance']['HostelEarlyLeave']?> ("+earlyLeave_count+")");
         $("#NA_count").val(NA_count);
         $("#normalLeave_count").val(normalLeave_count);
         $("#earlyLeave_count").val(earlyLeave_count);
         $('select').selectmenu('refresh', true);   
    }

    function turn_normalLeave(){
    	 var NA_count = 0;
         var normalLeave_count = 0;
         var earlyLeave_count = 0;
         var taken = "<?=$taken?>";
         
 		 for(var i=0; i<student_array.length;i++){
			 var userID = student_array[i];
             if($("#c_"+userID)[0].checked){
                 
                 
            	 	$("#h2_"+userID).remove();
            	 	$("#a_"+userID).css({"border":"2px","border-style":"solid","border-color":"#329700"}).append("<h2 style='color:#309100;' id='h2_"+userID+"'><?=$Lang['StudentAttendance']['HostelNormalLeave']?></h2>");
                	$("#ab_"+userID).remove();           	 		                      	 		                              	                         	                   	
                 

                 $("#status_"+userID).val(1);                 
            	 $("#c_"+student_array[i]).prop("checked",false).checkboxradio("refresh");
			     $("#s_"+userID).remove();

			     var confirmed = $("#confirmed_"+userID).val();
 	             
             }
             if($('#status_'+userID).val()==0) NA_count++;
             else if($('#status_'+userID).val()==1) normalLeave_count++;
             else if($('#status_'+userID).val()==2) earlyLeave_count++;
         }
 		$("#NA").text("N.A. ("+NA_count+")");                   
        $("#NormalLeave").text("<?=$Lang['StudentAttendance']['HostelNormalLeave']?> ("+normalLeave_count+")");
        $("#EarlyLeave").text("<?=$Lang['StudentAttendance']['HostelEarlyLeave']?> ("+earlyLeave_count+")");
        $("#NA_count").val(NA_count);
        $("#normalLeave_count").val(normalLeave_count);
        $("#earlyLeave_count").val(earlyLeave_count);
        $('select').selectmenu('refresh', true);                
    }

    function turn_earlyLeave(){
   	 	var NA_count = 0;
        var normalLeave_count = 0;
        var earlyLeave_count = 0;
        var taken = "<?=$taken?>";
        
		 for(var i=0; i<student_array.length;i++){
			 var userID = student_array[i];
            if($("#c_"+userID)[0].checked){
                
                
            		$("#h2_"+userID).remove();
            		$("#a_"+userID).css({"border":"2px","border-style":"solid","border-color":"#00a7d8"}).append("<h2 style='color:#0094bf;' id='h2_"+userID+"'><?=$Lang['StudentAttendance']['HostelEarlyLeave']?></h2>");
                	$("#ab_"+userID).remove();           	 		                      	 		                              	                         	                   	
                

                $("#status_"+userID).val(2);                 
           	 $("#c_"+student_array[i]).prop("checked",false).checkboxradio("refresh");
			     $("#s_"+userID).remove();

			     var confirmed = $("#confirmed_"+userID).val();
	          }   
            
            if($('#status_'+userID).val()==0) NA_count++;
            else if($('#status_'+userID).val()==1) normalLeave_count++;
            else if($('#status_'+userID).val()==2) earlyLeave_count++;
        }
		$("#NA").text("N.A. ("+NA_count+")");                 
       	$("#NormalLeave").text("<?=$Lang['StudentAttendance']['HostelNormalLeave']?> ("+normalLeave_count+")");
       	$("#EarlyLeave").text("<?=$Lang['StudentAttendance']['HostelEarlyLeave']?> ("+earlyLeave_count+")");
       	$("#NA_count").val(NA_count);
       	$("#normalLeave_count").val(normalLeave_count);
       	$("#earlyLeave_count").val(earlyLeave_count);
       	$('select').selectmenu('refresh', true);                
   }
	
    function submit(){
    	 if (confirm("<?=$Lang['StudentAttendance']['TeacherApp']['AreYouSureToSubmit'] ?>"))
    	   {
    	        document.form1.action = "handle_attendance.php";
    	        document.form1.submit(); 
    	   }
       
    }
    
    function back(){
		 window.location.href="attendance_list.php?date=<?=$date?>&charactor=<?=$charactor?>&token=<?=$token?>&uid=<?=$uid?>&ul=<?=$ul?>&parLang=<?=$parLang?>&timeslots=<?=$timeslots?>";			 			 
    }
    
    function cancle1(userID){
    	$("#popup1").popup("close");	
    }  

    function reason_confirm1(userID){
    	var timeslot = '<?=$timeslots ?>';
    	var status = $('#status_'+userID).val();
    	var inputTimefield = "input_"+timeslot+"Time";
    	var time = $('#'+inputTimefield).val();
        if(!(/^([0-1]?[0-9]|2[0-4]):([0-5][0-9])?$/.test(time))&&!document.getElementById(inputTimefield).readOnly){
        	document.getElementById("worn_timeFomat").innerHTML = '<?=$Lang['StudentAttendance']['SlotTimeFormatWarning']?>';
			return;
        }
    	  	
    	$("#popup1").popup("close");
    	
    	var remark = $('#remark1').val();
    	$('#rmk_'+userID).val(remark);
        	if(timeslot == 'in' && status!= 1){
        		$('#arrival_time_'+userID).val(time);
           	}else if(timeslot == 'out' && status!= 0){
           		$('#leave_time_'+userID).val(time);
           	}
    	$('#confirmed_'+userID).val(1);
    }

    function select_change(){
       	var timeslot = '<?=$timeslots ?>';
    	var selected = document.getElementById("status_switch_multiselect");
    	var selectedID = selected.selectedIndex;
    	console.log(timeslot);
    	if (timeslot == 'in'){
        	if(selectedID== 1){
        		turn_present();
            }else if(selectedID==2){
        		turn_absent();	          
        	}
    	}else { 
        	if(selectedID==1){
    			turn_NA();
        	}else if(selectedID==2){
        		turn_normalLeave();
        	}else if(selectedID==3){
        		turn_earlyLeave();
        	}
    	}
//    	$('#turn_to').prop('selected', true);
//     	$('#status_switch_multiselect').selectmenu('refresh');
//     	$('#turn_to').selectmenu('enable');
// 		var elements = selected.options;
//     	for(var i = 0; i < elements.length; i++){
//     	      elements[i].selected = false;
//     	    }
  	$('#turn_to').attr('selected', true);
 	$('#turn_to').prop('selected', true);	    
// 		$('#turn_to').selectmenu('disable');
//    	$('#turn_to').selectmenu('refresh');
    }

    function select_filter(){
    	var selected=document.getElementById("status_switch");
    	var selectedID = selected.selectedIndex;
    	if(selectedID==0){
	   	    for(var i=0; i<student_array.length;i++){
				var userID = student_array[i];
			    $('#li_'+userID).show();
	        }   
        }
    	else if(selectedID==1){
	   		for(var i=0; i<student_array.length;i++){
				var userID = student_array[i];
				if($('#status_'+userID).val()==0){
					$('#li_'+userID).show();				
			    }
				else{
					$('#li_'+userID).hide();								
			    }
	        }   
    	}
    	else if(selectedID==2){
	   		for(var i=0; i<student_array.length;i++){
				var userID = student_array[i];
				if($('#status_'+userID).val()==1){
					$('#li_'+userID).show();				
			    }
				else{
					$('#li_'+userID).hide();								
			    }
	        }   
    	}
    	else if(selectedID==3){
	   		for(var i=0; i<student_array.length;i++){
				var userID = student_array[i];
				if($('#status_'+userID).val()==2){
					$('#li_'+userID).show();				
			    }
				else{
					$('#li_'+userID).hide();								
			    }
	        }   
    	}    	
    	else if(selectedID==4){
	   		for(var i=0; i<student_array.length;i++){
				var userID = student_array[i];
				if($('#status_'+userID).val()==3){
					$('#li_'+userID).show();				
			    }
				else{
					$('#li_'+userID).hide();								
			    }
	        }   
    	}
    }
    
    function reason_pool(){
	    $('#reason_pool_div').toggle();
    }   
    
     function input_reason(id){
        $('#reason1').val($('#rea_pool_'+id).text());
	    $('#reason_pool_div').toggle();
    }   

     function CheckingCheckboxes(){
		 tag_class_and_no();
		 tag_name();
		 tag_group_name();
		 checked_all_or_not();
     }
 	     
     function tag_name(){
       var NameisChecked = $('#showStudentName:checked').val()?true:false;
    	if(NameisChecked){
    		$('p.StudentName').show();
    		$("#isShowStudentInfoName").val("1");
        }  else {
        	$('p.StudentName').hide();
        	$("#isShowStudentInfoName").val("0");
        } 
    		
     }


     function nameClassClassNoChecked(){
      	var showNameChecked = false;
    	         if ($('#showClassAndNum:checked').val() && $('#showStudentName:checked').val()){
    	        		$('span.openBracket').show();
    	        		$('span.closeBracket').show();
    	         }else{
    	        	 	$('span.openBracket').hide();
    	         		$('span.closeBracket').hide();
    	         }
     }

     function checked_all_or_not(){
       	var numberOfChecked = $("input[name='tagIds[]']:checkbox:checked").length;
       	var totalCheckboxes = $("input[name='tagIds[]']:checkbox").length;
//        	console.log(numberOfChecked + ', ' + totalCheckboxes);
       	if (!$('#SelectAll:checked').val() && numberOfChecked == totalCheckboxes -1){
       		$('#SelectAll').prop("checked", true).checkboxradio('refresh');
       		$("#isShowStudentInfo").val("1");	
       	} else{
       	$("input[name='tagIds[]']").each(function() {
     	         if (!this.checked){
     	        	$('#SelectAll').prop("checked", false).checkboxradio('refresh');	
     	         }
     	     });
	     		if(!$('#SelectAll:checked').val()){
	     			$("#isShowStudentInfo").val("0");
	     		}
       	}
      }
     function tag_group_name(){
    	 var GroupisChecked = $('#showGroup:checked').val()?true:false;
    	 if(GroupisChecked){
      		$('p.Group').show();
      		$("#isShowHostelGroup").val("1");
    	 }else{
      		$('p.Group').hide();
      		$("#isShowHostelGroup").val("0");
    	 }
     }
     
     function tag_class_and_no(){
     	var ClassAndNumisChecked = $('#showClassAndNum:checked').val()?true:false;
      	if(ClassAndNumisChecked){
          		$('p.ClassAndNum').show();
          		nameClassClassNoChecked();
      			$("#isShowStudentInfoClassAndNo").val("1");
          } 	else {
          	$('p.ClassAndNum').hide();
          	$('span.openBracket').hide();
     		$('span.closeBracket').hide();
          	$("#isShowStudentInfoClassAndNo").val("0");
          } 
      }

     function full_tag(){
    	 var SelectAllisChecked = $('#SelectAll:checked').val()?true:false;
    	 if (SelectAllisChecked){
        	 clickAll(1);
        	 $("#isShowStudentInfo").val("1");
    	 }else{
        	 clickAll(0);
        	 $("#isShowStudentInfo").val("0");
    	 }
    	 CheckingCheckboxes();
     }

     function clickAll(val){
// 		$(".clickAll").click(function() {
 			  // if(document.getElementsByName("checkAll")[0].checked) {
 			
 			   if(val == 1){
 			    $("input[name='tagIds[]']").each(function() {
// 				$("input[name='studentIds[]']").prop("checked",true);
 			
 			        $(this).prop("checked", true).checkboxradio('refresh');
 			    });
 			   } else {
 			     $("input[name='tagIds[]']").each(function() {
 			         $(this).prop("checked", false).checkboxradio('refresh');
 			     });           
 			//	   $("input[name='studentIds[]']").prop("checked", false);
 			   }
// 			});
 		
 	}

 	function change_dropdown_icon(){
 		$( ".custom_collapsible" ).collapsible({
 			  collapsedIcon: "arrow-d",
 			  expandedIcon: "arrow-u" 
 			});

 	}
//  	function onRevealEditTime(uid, time, inout)
//  	{
//  		var text_field = '<input type="text" id="'+inout+'Time'+uid+'" name="'+inout+'Time['+uid+']" value="'+time+'" size="12" onchange="onEditTimeChanged('+uid+',\''+inout+'\')" />';
//  		return text_field;
 		
 		
//  	}

 	function onEditTimeChanged(uid,inout)
 	{
 		var obj = $('#'+inout+'Time'+uid);
 		var val = $.trim(obj.val());
 		if(val != ''){
 			if(!val.match(/^\d\d:\d\d$/g)){
 				obj.val('');
 				if($('#'+inout+'Time_Warn'+uid).length == 0){
 					$('#'+inout+'Time_Edit'+uid).append('<div id="'+inout+'Time_Warn'+uid+'" class="red"><?=$Lang['StudentAttendance']['SlotTimeFormatWarning']?></div>');
 				}
 				$('#'+inout+'Time_Warn'+uid).show();
 				setTimeout(function(){
 					$('#'+inout+'Time_Warn'+uid).hide();
 				},3000);
 				return;
 			}
 			var parts = val.split(':');
 			var hr = Math.max(0, Math.min(24, parseInt(parts[0])));
 			var min = Math.max(0, Math.min(59, parseInt(parts[1])));
 			//var sec = Math.max(0, Math.min(59, parseInt(parts[2])));
 			
 			var val_str = hr < 10? '0' + hr : '' + hr;
 			val_str += ':';
 			val_str += min < 10? '0' + min : '' + min;
 			//val_str += ':';
 			//val_str += sec < 10? '0' + sec : '' + sec;
 			obj.val(val_str);
 		}
 	}
 	
 	
</script>
<link rel="stylesheet" href="<?=$PATH_WRT_ROOT?>templates/jquery.mobile/jquery.mobile.min.css">

<style type="text/css">
     ul.ui-listview.ui-listview-inset.ui-corner-all.ui-shadow {
    margin-top: 0px;
    }
       
     .tagArea{
        position: absolute;
        bottom:0;
     }

     .ui-controlgroup{ 
      margin: 0!important; 
     } 
	.ui-collapsible-content {
		padding: 0 !important;
	}
	
	label.ui-btn.ui-corner-all.ui-btn-inherit.ui-btn-icon-left {
        width: 120px;
        
    }
    

	
	.custom_collapsible{
	   width: 150px;
	    margin-right: 30px!important;
	   
	}
	
	
    #body{
        
         background:#ffffff;
         text-shadow:none;
         -webkit-touch-callout: none !important;
         padding-top: 80px; 
         padding-bottom: 72px;
    }
    #show_class{
    
         text-shadow:none;
    }
    a:link { text-decoration: none; color: black;}
    a:active {text-decoration: none; color: black !important;} 
    a:hover {text-decoration: none;color: black !important;}  
    a:visited {text-decoration: none;color: black !important;} 
   
   	.header a:link { text-decoration: none; color: white;}
    .header a:active {text-decoration: none; color: white !important;} 
    .header a:hover {text-decoration: none;color: white !important;}  
    .header a:visited {text-decoration: none;color: white !important;} 
		
    .student_list {
    }
	.student_list .ui-content{
       padding:0;

    }
	.student_list .ui-listview li {
		float: left;
        width: 80px;
		height: 104px;
        margin: auto;
        margin-top:10px ;
	    margin-left:10px ;		
	}
	.student_list .ui-listview li > .ui-btn {
		-webkit-box-sizing: border-box; /* include padding and border in height so we can set it to 100% */
		-moz-box-sizing: border-box;
		-ms-box-sizing: border-box;
		box-sizing: border-box;
		height: 100%;
		text-shadow:none;/*no shadow*/
		width:80px;
		padding-right:0;
 		border:2px; 
 		border-style: solid;  
        border-color:#75b70d;
        
	}
	.student_list .ui-listview li.ui-li-has-thumb .ui-li-thumb {
		height: auto; /* To keep aspect ratio. */
		max-width: 100%;
		max-height: none;
	}
	/* Make all list items and anchors inherit the border-radius from the UL. */
	.student_list .ui-listview li,
	.student_list .ui-listview li .ui-btn,
	.student_list .ui-listview .ui-li-thumb {
		-webkit-border-radius: inherit;
		border-radius: inherit;
		padding:0 !important;
		
	}
	/* Hide the icon */
	.student_list .ui-listview .ui-btn-icon-right:after {
		display: none;
	}
	/* Make text wrap. */
	.student_list .ui-listview p {
		white-space: normal;
		overflow: visible;
		position: relative;
		left: 0;
		right: 0;
	}
	/* Text position */
	.student_list .ui-listview p {
		font-size: 0.8em;
		margin: 0;
        text-align:center;
		min-height: 20%;
		bottom:0;
		width:80px !important;
	}
	/* Semi transparent background and different position if there is a thumb. The button has overflow hidden so we don't need to set border-radius. */
	.student_list .ui-listview .ui-li-has-thumb p {
		background: rgba(255,255,255,.7);
	}

	.student_list .ui-listview .ui-li-has-thumb p {
		min-height: 20%;
	}
	.student_list .ui-listview .absent {
		width: auto;
        height: 100%;
		top: 0;
		left: 0;
		bottom: 0;
		background: rgba(255,255,255,.7);
		-webkit-border-top-right-radius: inherit;
		border-top-right-radius: inherit;
		-webkit-border-bottom-left-radius: inherit;
		border-bottom-left-radius: inherit;
		-webkit-border-bottom-right-radius: 0;
		border-bottom-right-radius: 0;
		
	}

	/* Animate focus and hover style, and resizing. */
	.student_list .ui-listview li,
	.student_list .ui-listview .ui-btn {
		-webkit-transition: all 500ms ease;
		-moz-transition: all 500ms ease;
		-o-transition: all 500ms ease;
		-ms-transition: all 500ms ease;
		transition: all 500ms ease;
	}
    .student_list .ui-listview .selected {
    
		white-space: normal;
		overflow: visible;
		position: absolute;
		right: 0;
    	bottom: 0;
    	opacity:0.8;
    	padding-right:2px;
        padding-bottom:2px;    	
        width:30%;
        z-index:1;   
    }
    
    .student_list .ui-listview h2 {
    
		white-space: normal;
		overflow: visible;
		position: absolute;
		top: 0;
		right: 0; 
	    font-size: 1em;
 	    margin-top:0.3em; 
 	    margin-right:0.3em; 
		text-shadow:0 0 2px #FFFFFF;
		z-index:1;
    }
</style>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
		
</head>
	<body>
	
		<div data-role="page" id="body">			

			<form name="form1" method="post" id="form1">
			
			    <div id="header" data-role="header" data-position="fixed" style="border:0;height:110px; background-color:#ffffff;">
			       
			        <div id="show_class" >	                
			
			           <table width="100%" style ="background-color:#429DEA;height:60px;font-size:1em;font-weight:normal;">
			
			             <tr>
			
			                <td style="padding-left:0em;">
			                   <table> 
			                     <tr><td rowspan="2">
			                    <a href="javascript:back();"> <img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/iMail/app_view/white/icon_outbox_white.png" style="width:2em; height:2em;" /></a>
			                     </td><td><?=$group_name?> (<?php echo($timeslots=='in'?$Lang['StudentAttendance']['InStatus']:$Lang['StudentAttendance']['OutStatus'])?>)
			                     </td></tr>
			                     <tr><td><?=$show_date?></td></tr>
			                   </table>
			                </td>
			
			                <td  align= "right" style="padding-right:0.5em;">
								
								    <?=$statusSwitch?>							
								
			                </td>	              
			
			              </tr>
			
			            </table>
			
			          </div>	
			           <div id ="header_menu" >			
			          <div id="mutiple_select" align="left" style=" float:left; background-color:white;height: 30px;font-size:1em;">     
			            <table style="margin-top: .8em">
			               <tr>
			                 <td>
			                    <a href="javascript:show_mutiple_select();" ><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/smartcard/icon_change.png" style="width:1.4em; height:1.4em;" /> </a>
			                 </td>
			                 <td style="padding-right: .5em">
			                    <a href="javascript:show_mutiple_select();" ><?=$Lang['AccountMgmt']['MultiSelect']?></a>
			                 </td>
			               </tr>
			               
			            </table>
			          </div>		          			
			          <div id="status_select" style=" float:left; background-color:white;height: 30px;" >
			            <table >
			               <tr>
			                 <td style="padding-left: 0.3em; padding-right: 0.3em;">
			                    <a style="white-space: nowrap;" href="javascript:choose_all();" ><?=$Lang['Btn']['SelectAll']?></a>
			                 </td>
			                 <td>
				             	<a href="javascript:show_status_select()" >
				                	<table>
					               		<tr>
						           			<td>
						            			<img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/smartcard/icon_change_select.png" style="width:1.4em; height:1.4em;" />
						            		</td> 
						            		<td>	
						            			<?=$Lang['Btn']['Cancel']?> 
					          				</td>
					          			</tr>
				              		</table>
				              	</a>      
			                  </td>
			              <td>
			                 <?=$statusSwitchMultiselect?>	
							</td>
			               </tr>			               
			            </table>			
			          </div>		
			          <div id="ShowStudentInfo" align="left" style=" background-color:white;height: 30px;font-size:1em;" >     
			            <table> 
			               <tr>
			                 <td>
			                 <div data-role='collapsible' class='custom_collapsible'>
			                 	<h1 id='showTagDropdpwnLabel'><?=$Lang['eClassApp']['StudentAttendance']['ShowStudentTag']?></h1>		       
			                   <fieldset  data-role='controlgroup' name="tag_select" id="tag_select" data-mini="true">
			 			                 
			 			                <label for="SelectAll"><?=$Lang['Btn']['SelectAll'] ?></label>		
			                			<input type="checkbox" value="showAllInfo" id="SelectAll" name="tagIds[]" onchange="full_tag();">               			
			                			
			                			<label for="showStudentName"><?=$Lang['eClassApp']['StudentAttendance']['ShowNames']?></label>				 						
								        <input type="checkbox" value="showStudentName" id="showStudentName" name="tagIds[]" onchange="CheckingCheckboxes();">
								        
								        <label for="showClassAndNum"><?=$Lang['eClassApp']['StudentAttendance']['ShowClassAndNum']?></label>
										<input type="checkbox" value="showClassAndNum" id="showClassAndNum" name="tagIds[]" onchange="CheckingCheckboxes();">
								        
								        <label for="showGroup"><?=$Lang['eClassApp']['StudentAttendance']['ShowGroup']?></label>
										<input type="checkbox" value="showGroup" id="showGroup" name="tagIds[]" onchange="CheckingCheckboxes();">
								    </fieldset>	
								 </div>
			                 </td>
			               </tr>			               
			            </table>
			          </div>
			       </div>
			     </div>
	            <div data-role="content" class="student_list">
	                 <ul data-role="listview" data-inset="true">	                 
	                    <?=$student_list?>           	            	            
			         </ul>
	            </div>	            	           
	            <a id='link1' href='#popup1'  data-rel='popup' data-position-to='window' class='ui-btn ui-corner-all' data-transition='pop' style='display: none;'></a>
			    <?= $popup?>
  	    	    <input type="hidden" value = "<?=$id?>" name="id">
			    <input type="hidden" value = "<?=$charactor?>" name="charactor">
			    <input type="hidden" value = "<?=$date?>" name="date">
			    <input type="hidden" value = "<?=$timeslots?>" name="timeslots">
			    <input type="hidden" value = "<?=$student_array_string?>" name="student_list">
			    <input type="hidden" value = "<?=$GroupID?>" name="GroupID">
			    <input type="hidden" value = "" name="mode" id="mode"> 
			    <input type="hidden" value=<?=$token?> name="token">
                <input type="hidden" value=<?=$uid?> name="uid">
                <input type="hidden" value=<?=$ul?> name="ul">
                <input type="hidden" value=<?=$parLang?> name="parLang">
                <input type="hidden" value=<?=$present_count?> name="present_count" id="present_count">
                <input type="hidden" value=<?=$absent_count?> name="absent_count" id="absent_count">
                <input type="hidden" value=<?=$NA_count?> name="NA_count" id="NA_count">
                <input type="hidden" value=<?=$normalLeave_count?> name="normalLeave_count" id="normalLeave_count">
                <input type="hidden" value=<?=$earlyLeave_count?> name="earlyLeave_count" id="earlyLeave_count">
                <input type="hidden" value=<?=$taken?> name="taken" id="taken">         
                <input type="hidden" value=<?=$isShowStudentInfoName?> name="isShowStudentInfoName" id="isShowStudentInfoName">
				<input type="hidden" value='<?=$isShowStudentInfoClassAndNo?>' name="isShowStudentInfoClassAndNo" id="isShowStudentInfoClassAndNo">
                <input type="hidden" value='<?=$isShowHostelGroup?>' id="isShowHostelGroup" name="isShowHostelGroup" >                                 
                <input type="hidden" value='<?=$isShowStudentInfo?>'  id="isShowStudentInfo" name="isShowStudentInfo"> 
                <input name="PageLoadTime" type="hidden" value="<?=$page_load_time?>">   
                 <?=$recordID_array?>
                 <?=$reason_submit?>
                 <?=$redordID?>
			     <?=$status_array?> 
			     <?=$arrival_time?>
			     <?=$leave_time?>
                 <?=$post_info?>               
                 <div id="footer" data-role="footer" data-position="fixed"  style ="background-color:white;height:70px" align="center">

					 <?php
					 $show_save_button = true;
					 if($sys_custom['StudentAttendance']['HostelAttendance_Reason']) {
						 if ($Settings['HostelAttendanceDisablePastDate'] == 1) {
							 if (strtotime(date("Y-m-d")) > $ts_record) {
								 $show_save_button = false;
							 }
						 }
						 if ($Settings['HostelAttendanceDisableFutureDate'] == 1) {
							 if ($ts_record > strtotime(date("Y-m-d"))) {
								 $show_save_button = false;
							 }
						 }
					 }
					 ?>

                     <?php if($show_save_button == true) { ?>
	                    <a href="javascript:submit();" class="ui-btn ui-corner-all" style ="background-color:#429DEA;display:block;margin-left:10px;margin-right:10px;
	                     padding-top:11px;padding-bottom:11px;font-size:1.3em;font-weight:normal;color:white !important;text-shadow:none;" ><?=$Lang['StudentAttendance']['Confirm']?></a>
                     <?php } ?>
	            </div>	 
			</form>

		</div>
		
	</body>
</html>

<?php

intranet_closedb();

?>