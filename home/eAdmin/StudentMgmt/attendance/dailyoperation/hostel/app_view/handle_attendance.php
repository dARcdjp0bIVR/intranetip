<?php
// Editing by Isaac
/*
 * 2017-13-26 by Isaac: created haandle_attendance.php
 */

$PATH_WRT_ROOT = "../../../../../../../";

switch (strtolower($parLang)) {
    case 'en':
        $intranet_hardcode_lang = 'en';
        break;
    default:
        $intranet_hardcode_lang = 'b5';
}

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libsmartcard.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libadminjob.php");
include_once($PATH_WRT_ROOT."includes/libteaching.php");
include_once($PATH_WRT_ROOT."includes/libstudentattendance_ui.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_hardcode_lang.php");
include_once($PATH_WRT_ROOT."lang/eclassapp_lang.$intranet_hardcode_lang.php");
include_once($PATH_WRT_ROOT.'includes/eClassApp/libeClassApp.php');
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libgrouping.php");

$_SESSION['UserID'] = $uid;


$isShowStudentInfo = standardizeFormPostValue($_POST['isShowStudentInfo']);
$isShowStudentInfoName = standardizeFormPostValue($_POST['isShowStudentInfoName']);
$isShowStudentInfoClassAndNo = standardizeFormPostValue($_POST['isShowStudentInfoClassAndNo']);
$isShowHostelGroup = standardizeFormPostValue($_POST['isShowHostelGroup']);

$libeClassApp = new libeClassApp();
$isTokenValid = $libeClassApp->isTokenValid($token, $uid, $ul);

if(!$isTokenValid) {
    echo $i_general_no_access_right	;
    exit;
}
if(!isset($id)||$id==""||!isset($charactor)||$charactor==""||!isset($date)||$date==""||!isset($timeslots)||$timeslots==""||!isset($mode)||$mode=="")header('Location:attendance_list.php');

$use_magic_quotes = $lc->is_magic_quotes_active;
$TargetDate = date("Y-m-d",strtotime($date));
$today = date("Y-m-d");
$ts = strtotime($TargetDate);
$tomorrow = date("Y-m-d",$ts+86400);
$year = date("Y",$ts);
$month = date("m",$ts);
$day = date("d",$ts);

//intranet_auth();
intranet_opendb();
$libdb = new libdb();
$lu = new libuser($UserID);
$lc = new libcardstudentattend2();
$lc->retrieveSettings(); 
$log_row = date("Y-m-d H:i:s").' '.$_SERVER['REQUEST_URI'].' '.$_SESSION['UserID'].' ';
$StudentID = student_list;

#store the settings for what student info to be shown
$settingsAssoAry = array();
$settingsAssoAry['eAttendance_ShowStudentName'] = $isShowStudentInfo;
$settingsAssoAry['eAttendance_ShowStudentInfoName'] = $isShowStudentInfoName;
$settingsAssoAry['eAttendance_ShowStudentInfoClassAndNo'] = $isShowStudentInfoClassAndNo;
$settingsAssoAry['eAttendance_ShowHostelGroup'] = $isShowHostelGroup;
foreach ((array)$settingsAssoAry as $_key => $_val) {
    $sql="select * from APP_USER_SETTING where UserID='".$UserID."' and SettingName='".$_key."'";
    $getShowStudentNameArray = $libdb->returnArray($sql);
    if(sizeof($getShowStudentNameArray)==0){
        $insertSettingSQL="INSERT INTO APP_USER_SETTING (UserID,SettingName,SettingValue,DateModified,ModifiedBy) VALUES ('".$UserID."','".$_key."','".$_val."',NOW(),".$UserID.") ";
        $libdb->db_db_query($insertSettingSQL);
        
    }else{
        $updateSettingSQL="UPDATE APP_USER_SETTING set SettingValue ='".$_val."', DateModified = NOW(), ModifiedBy='".$UserID."' where UserID='".$UserID."' and SettingName = '".$_key."'";
        $libdb->db_db_query($updateSettingSQL);
    }
    
}
$hostel_admins = $lc->getHostelAttendanceAdminUsers(array('OnlyUserID'=>1));
$can_take_all_groups = in_array($_SESSION['UserID'],$hostel_admins) || $_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"];

if (!(($_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || $_SESSION['UserType'] == USERTYPE_STAFF) && $sys_custom['StudentAttendance']['HostelAttendance'])
    || ($GroupID=='' && !$can_take_all_groups) || $date=='' || count($StudentID)==0) {
        intranet_closedb();
        $_SESSION['STUDENT_ATTENDANCE_HOSTEL_ATTENDANCE_RESULT'] = $Lang['StudentAttendance']['DailyStatusConfirmFail'];
        
        header('Location:attendance_list.php?msg='.$msg.'&date='.$TargetDate.'&charactor='.$charactor.'&timeslots='.$timeslots.'&token='.$token.'&uid='.$uid.'&ul='.$ul.'&parLang='.$parLang.'');
        
        exit();
    }
//     exit(); 
    //$lc = new libcardstudentattend2();
    
    

    $student_array = explode(";",$student_list);;
    $student_size = count($student_array);
    
    $params = array('AcademicYearID'=>Get_Current_Academic_Year_ID(),'RecordDate'=>$TargetDate,'CategoryID'=>$lc->HostelAttendanceGroupCategory);
    if(!$can_take_all_groups || $GroupID!=''){
        $params['GroupID'] = $GroupID;
        //$params['IsGroupStaff'] = true;
        //$params['GroupUserID'] = $_SESSION['UserID'];
    }
    $confirmed_records = $lc->getHostelAttendanceGroupsConfirmedRecords($params);

    //$confirm_record_id = '';
    //if(count($confirmed_records)>0 && $confirmed_records[0]['RecordID']!='') $confirm_record_id = $confirmed_records[0]['RecordID'];
    
    $lc->Start_Trans();
    $timeslots = $_POST[timeslots];
    $result_ary = array();
    for($i=0;$i<$student_size;$i++){
        $data = array();
        $data['UserID'] = $student_array[$i];
        $data['DayNumber'] = $day;
        $data['RecordID'] = $_POST["recordID_".$student_array[$i]];
        if($timeslots =='in'){
            $data['InStatus'] = $_POST["status_".$student_array[$i]];
        }else if($timeslots =='out'){
            $data['OutStatus'] = $_POST["status_".$student_array[$i]];
        }
        if(isset($_POST["arrival_time_".$student_array[$i]])&&$_POST["arrival_time_".$student_array[$i]]!=""){
            $data['InTime'] = $_POST["arrival_time_".$student_array[$i]]!=''? ($TargetDate.' '.$_POST["arrival_time_".$student_array[$i]].':00'):'';
        }
        if(isset($_POST["leave_time_".$student_array[$i]])&&$_POST["leave_time_".$student_array[$i]]!=""){
            $data['OutTime'] = $_POST["leave_time_".$student_array[$i]]!=''? (($today > $TargetDate? $tomorrow:$TargetDate).' '.$_POST["leave_time_".$student_array[$i]].':00'):''; // assume it is next day out
        }
        $data['Remark'] = trim( $use_magic_quotes? stripslashes($_POST["rmk_".$student_array[$i]]) : $_POST["rmk_".$student_array[$i]] );
        $result_ary['upsert_'.$student_array[$i]] = $lc->upsertHostelAttendanceRecord($year,$month,$data);
    }
    
    if(!in_array(false,$result_ary)){
        $_SESSION['STUDENT_ATTENDANCE_HOSTEL_ATTENDANCE_RESULT'] = $Lang['StudentAttendance']['DailyStatusConfirmSuccess'];
        for($i=0;$i<count($confirmed_records);$i++){
            $confirmed_params = array('GroupID'=>$confirmed_records[$i]['GroupID'],'RecordDate'=>$TargetDate);
            if($confirmed_records[$i]['RecordID']!=''){
                $confirmed_params['RecordID'] = $confirmed_records[$i]['RecordID'];
            }
            $lc->upsertHostelAttendanceGroupConfirmRecord($confirmed_params);
            
       }                                                                                                                                                                                                                                            
        $lc->Commit_Trans();
        $msg = 1;
    }else{
        $_SESSION['STUDENT_ATTENDANCE_HOSTEL_ATTENDANCE_RESULT'] = $Lang['StudentAttendance']['DailyStatusConfirmFail'];
        $lc->RollBack_Trans();
   }
  


$lc->log($log_row);

intranet_closedb();

header('Location:attendance_list.php?msg='.$msg.'&date='.$date.'&charactor='.$charactor.'&timeslots='.$timeslots.'&token='.$token.'&uid='.$uid.'&ul='.$ul.'&parLang='.$parLang.'');

?>