<?php

# using: yuen

###################### Change Log Start ########################
#	Date: 	2012-11-30 yuen 
#			parsed phone number to remove unnecessary " " and "-"
#
#	Date:	2012-08-09	YatWoon
#			Improved: Add remark "School will be charged for each SMS message"
#
#	Date:	2010-01-27	YatWoon
#			get "In School Time" value
#
###################### Change Log End ########################

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libsmsv2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();

$linterface = new interface_html();

# class used
$LIDB = new libdb();
$lsms = new libsmsv2();


$Content = $lsms->returnSystemMsg($TemplateCode);


$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = $_REQUEST['this_page_nav'];

$main_content_field = "IF(EnName IS NOT NULL AND EnName !='' AND ChName IS NOT NULL AND ChName !='', IF(IsMain = 1, CONCAT('* ', EnName,' / ',ChName), CONCAT(EnName,' / ',ChName)), IF(EnName IS NOT NULL AND EnName!='',EnName,ChName))";
$main_content_field = "IF($main_content_field='' OR $main_content_field IS NULL,'-',$main_content_field)";
$namefield2 = $lsms->getSMSGuardianUserNameField("");

$sms_ary	= array();
$no_sms_ary = array();
$sms_info 	= "";
for($i=0; $i<sizeOf($user_id); $i++)
{
	$student_id	= $user_id[$i];
	$send_sms = (${"sms_".$student_id} == '') ? 0 : 1;
	
	if($send_sms)
	{
		$lu = new libuser($student_id);
		
		$sql = "
			SELECT 
					$main_content_field,
					Relation,
					Phone, 
					EmPhone,
					IsMain,
					$namefield2
			FROM
					$eclass_db.GUARDIAN_STUDENT
			WHERE 
					UserID = $student_id and 
					isSMS = 1
			ORDER BY 
					IsMain DESC, Relation ASC
		";
		$temp = $LIDB->returnArray($sql);
	
		// guardian info
		for($j=0;$j<sizeof($temp);$j++)
		{
			# phone / emergency Phone 
			$temp[$j][2] = $lsms->parsePhoneNumber($temp[$j][2]);
			$temp[$j][3] = $lsms->parsePhoneNumber($temp[$j][3]);
			$p = $lsms->isValidPhoneNumber($temp[$j][2]) ? $temp[$j][2] : "";
			$e = $lsms->isValidPhoneNumber($temp[$j][3]) ? $temp[$j][3] : "";
			$sms_phone 	= ($p!="") ? $p : $e;
			
			if(!$sms_phone)		
			{	
				$no_sms_ary[$i]['reason'] = $i_SMS_Error_NovalidPhone;
				$mobile 	= ($temp[$j][2]!="") ? $temp[$j][2] : $temp[$j][3];
				$guardianName	= "";
			}
			else
			{
				$no_sms_ary[$i]['reason'] = "";
				$mobile 	= ($p!="") ? $p : $e;
				$guardianName	= $temp[$j][5];
				break;
			}
		}
		
		//Check Guardian
		if(sizeof($temp)==0)	
		{
			$no_sms_ary[$i]['reason'] = $i_SMS_Error_No_Guardian;
			$mobile	= "";
		}
		
		if($no_sms_ary[$i]['reason'])	# in $no_sms_ary
			{
					$no_sms_ary[$i]['name'] 		= $lu->UserName();
					$no_sms_ary[$i]['classname'] 	= $lu->ClassName;
					$no_sms_ary[$i]['classnumber'] 	= $lu->ClassNumber;
					$no_sms_ary[$i]['mobile'] 		= $mobile;
			}
			else
			{
					$sms_ary[$i]['guardian'] 	= $temp[$j][0];
					$sms_ary[$i]['relation'] 	= $temp[$j][1];	
					$sms_ary[$i]['name'] 		= $lu->UserName();
					$sms_ary[$i]['classname'] 	= $lu->ClassName;
					$sms_ary[$i]['classnumber']	= $lu->ClassNumber;
					$sms_ary[$i]['mobile'] 		= $mobile;
					$sms_ary[$i]['guardianName']= $guardianName;
					
					$in_school_time = ${"in_school_time_".$student_id};
					//$sms_info .= $student_id .",".$mobile.",".$guardianName.";";
					$sms_info .= $student_id ."###".$mobile."###".$guardianName."###".$in_school_time.";";
			}
	}
}

############################################################
## no_sms_ary
############################################################


$invalid_table = "<table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\"><tr>";
$invalid_table.= "<td>";
$invalid_table.= "<div class=\"table_board\"><span class=\"sectiontitle_v30\"><font color='red'> {$i_SMS_Warning_Cannot_Send_To_Users}</font></span><br />";
$invalid_table.= "</td></tr></table>";

$table_content = "";
$x = "<table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">\n";
$x .= "<tr class=\"tabletop\">
<td class=\"tabletoplink\" width=\"30%\">$i_UserName</td>
<td class=\"tabletoplink\" width=\"13%\">$i_ClassName</td>
<td class=\"tabletoplink\" width=\"12%\">$i_ClassNumber</td>
<td class=\"tabletoplink\" width=\"20%\">$i_SMS_MobileNumber</td>
<td class=\"tabletoplink\" width=\"35%\">$i_Attendance_Reason</td></tr>\n";

$no_sms_ary_no = 0;
$css = "tablerow1";
foreach($no_sms_ary as $this_ary) {
	if(trim($this_ary['name'])=="")	continue;
	$css = ($css=="tablerow2")?"tablerow1":"tablerow2";
	$x .= "<tr class=\"$css\">";
	$x .= "<td class=\"tabletext\">". $this_ary['name'] ."</td>";
	$x .= "<td class=\"tabletext\">". $this_ary['classname'] ."</td>";
	$x .= "<td class=\"tabletext\">". $this_ary['classnumber'] ."</td>";
	$x .= "<td class=\"tabletext\">". $this_ary['mobile'] ."</td>";
	$x .= "<td class=\"tabletext\">". $this_ary['reason'] ."</td>";
	$x .= "</tr>\n";
	$no_sms_ary_no++;
}
$x .= "</table>\n";
$table_content = $invalid_table.$x;

############################################################
## sms_ary
############################################################

$valid_table = "<table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\"><tr>";
$valid_table.= "<td>";
$valid_table.= "<div class=\"table_board\"><span class=\"sectiontitle_v30\"> {$i_SMS_Notice_Send_To_Users}</span><br />";
$valid_table.= "</td></tr></table>";

$table_content1 = "";

$x = "<table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">\n";
$x .= "<tr class=\"tabletop\">
<td class=\"tabletoplink\" width=\"30%\">$i_UserName</td>
<td class=\"tabletoplink\" width=\"13%\">$i_ClassName</td>
<td class=\"tabletoplink\" width=\"12%\">$i_ClassNumber</td>
<td class=\"tabletoplink\" width=\"20%\">$i_SMS_MobileNumber</td>
<td class=\"tabletoplink\" width=\"35%\">$i_StudentGuardian_GuardianName</td>
</tr>\n";

$sms_ary_no = 0;
$css = "tablerow1";
foreach($sms_ary as $this_ary) {
	if(trim($this_ary['name'])=="")	continue;
	$css = ($css=="tablerow2")?"tablerow1":"tablerow2";
	$x .= "<tr class=$css>";
	$x .= "<td class=\"tabletext\">". $this_ary['name'] ."</td>";
	$x .= "<td class=\"tabletext\">". $this_ary['classname'] ."</td>";
	$x .= "<td class=\"tabletext\">". $this_ary['classnumber'] ."</td>";
	$x .= "<td class=\"tabletext\">". $this_ary['mobile'] ."</td>";
	$x .= "<td class=\"tabletext\">". $this_ary['guardianName'] ."</td>";
	$x .= "</tr>\n";
	$sms_ary_no++;
}
$x .= "</table>\n";
$table_content1 = $valid_table.$x;

$back_button = $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location = '".$this_page."';");
$send_button = $linterface->GET_ACTION_BTN($button_send, "submit", "");


$TAGS_OBJ[] = array($this_page_title, "", 0);
$MODULE_OBJ = $lc->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

$PAGE_NAVIGATION[] = array("$TargetDate ($display_period)");

### Warning
$WarningBox = $linterface->Get_Warning_Message_Box('<font color="red">'.$Lang['General']['Note'].'</font>', $Lang['SMS']['RemarkChargeForSMS'], $others="");

?>
<br />
<form name="form1" method="post" action="confirm_send_sms_update.php" >
<?=$WarningBox?>

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
<? if($sms_ary_no>0) {?>
	<tr>
		<td>
			<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?=$i_SMS_MessageContent?>
					</td>
					<td class="tabletext" width="70%">
						<?=$linterface->GET_TEXTAREA("Content", $Content)?>
						<span class="tabletextremark">
			  <br /><?=$Lang['General']['Caution']?>
			  <li><?=$Lang['SMS']['LimitationArr'][0]['Content']?></li>
			  <li><?=$Lang['SMS']['LimitationArr'][1]['Content']?></li>
			  <li><?=$Lang['SMS']['LimitationArr'][2]['Content']?></li>
			  </span>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr>
			  	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
			  </tr>
				<tr>
				  <td align="center" colspan="2">
						<?= $send_button."&nbsp;".$back_button ?>
					</td>
				</tr>
			</table>
		</td>
	</tr>
<? } ?>
<? if($no_sms_ary_no!=0) {?>
	<tr>
		<td><?=$table_content?></td>
	</tr>
<? } ?>
<? if($sms_ary_no!=0) {?>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td><?=$table_content1?></td>
	</tr>
<? } ?>
<? if($sms_ary_no==0) {?>
	<tr>
		<td>
			<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr>
			  	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
			  </tr>
				<tr>
				  <td align="center" colspan="2">
						<?= $back_button ?>
					</td>
				</tr>
			</table>
		</td>
	</tr>
<? } ?>
</table>
<input type="hidden" name="this_page" value="<?=$this_page?>">
<input type="hidden" name="sms_info" value="<?=$sms_info?>">
<input type="hidden" name="TargetDate" id="TargetDate" value="<?=$TargetDate?>">
<input type="hidden" name="DayType" id="DayType" value="<?=$DayType?>">
<input type="hidden" name="order_by_time" value="<?=$order_by_time?>">
</form>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>