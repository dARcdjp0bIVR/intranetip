<?
// editing by 
############################################ Change Log #################################################
#
# 2010-02-24 by Carlos : Change SQL to select Chinese Class Name or English Class Name depending on Lang
#
#########################################################################################################
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();
$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageAccountResponsibleAdmin_ClassRepresentative";

$TempUserID = $_SESSION['UserID'];

$linterface = new interface_html();

#class used
$LICLASS = new libclass();

$select_class = urldecode($class_name);

$current_academic_yearid = Get_Current_Academic_Year_ID();
#build student
if($select_class<>""){
	$select_student_table = "";

	$name_field = getNameFieldByLang("a.");
	/*
	$sql = "SELECT a.UserID, $name_field, a.ClassNumber,
				IF(b.StudentID IS NOT NULL, CONCAT(\"1\"), CONCAT(\"0\")) 
					FROM INTRANET_USER as a LEFT OUTER JOIN CARD_STUDENT_HELPER_STUDENT as b ON (a.UserID=b.StudentID) WHERE a.RecordType = 2 AND a.RecordStatus IN (0,1,2) AND a.ClassName = '$select_class' ORDER BY a.ClassNumber";
	*/
	$sql = "SELECT a.UserID, $name_field, a.ClassNumber,
				   IF(b.StudentID IS NOT NULL, CONCAT(\"1\"), CONCAT(\"0\")) 
			FROM 
				YEAR AS y
				INNER JOIN YEAR_CLASS yc ON y.YearID = yc.YearID and yc.AcademicYearID = '$current_academic_yearid' 
				INNER JOIN INTRANET_USER as a ON a.ClassName = yc.ClassTitleEN
				LEFT OUTER JOIN CARD_STUDENT_HELPER_STUDENT as b ON (a.UserID=b.StudentID) 
			WHERE 
				a.RecordType = 2 AND a.RecordStatus IN (0,1,2) AND (yc.ClassTitleEN = '$select_class' OR yc.ClassTitleB5 = '$select_class')
			ORDER BY a.ClassNumber ";
	
	$student_list = $LICLASS->returnArray($sql,4);

    $checkall = "<input type=\"checkbox\" onClick=\"(this.checked)?setChecked(1,this.form,'student_id[]'):setChecked(0,this.form,'student_id[]')\">";

	$x = "<table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">\n";
	$x .= "<tr class=\"tabletop\"><td class=\"tabletoplink\">$checkall</td><td class=\"tabletoplink\">$i_UserClassNumber</td><td class=\"tabletoplink\">$i_UserName</td></tr>\n";
	for($i=0; $i<sizeOf($student_list); $i++){
		list($UserID, $name_field, $ClassNumber, $added) = $student_list[$i];
		$css = ($i%2==0)?"tablerow1":"tablerow2";
		$checked = ($added=="1")?"CHECKED":"";

		$x .= "<tr class=\"$css\"><td class=\"tabletext\"><input name=\"student_id[]\" type=\"checkbox\" value=\"$UserID\" $checked></td><td class=\"tabletext\">$ClassNumber</td><td class=\"tabletext\">$name_field</td></tr>\n";
	}
	$x .= "</table>\n";

	$select_student_table = $x;
}

$TAGS_OBJ[] = array($i_SmartCard_Responsible_Admin_Settings_List, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/admin/class/list/", 0);
$TAGS_OBJ[] = array($i_SmartCard_Responsible_Admin_Settings_Manage, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/admin/class/manage/", 1);
		
$MODULE_OBJ = $lc->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

$PAGE_NAVIGATION[] = array($button_edit);

if ($msg == 1) $SysMsg = $linterface->GET_SYS_MSG("add");
if ($msg == 2) $SysMsg = $linterface->GET_SYS_MSG("update");
if ($msg == 3) $SysMsg = $linterface->GET_SYS_MSG("delete");

?>

<script language="javascript">
function checkform(obj){
	/*var element = "student_id[]";
	if(countChecked(obj,element)==0) {
		alert(globalAlertMsg1);
		return false
	} */

    return true;
}
</script>
<br />
<form name="form1" method="post" action="edit_update.php" method="post" onSubmit="return checkform(this);">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	</tr>
	<tr>
		<td>
			<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $i_ClassName ?></td>
					<td class="tabletext" width="70%"><?=$select_class?></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<?=$select_student_table?>
		</td>
	</tr>
	<tr>
		<td>
			<table width="96%" border="0" cellpadding="0" cellspacing="0" align="center">
		    	<tr>
                	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
		    </table>	
		</td>
	</tr>
	<tr>
	    <td align="center" colspan="2">
	    <?
        if (($select_class<>""&&sizeof($student_list)!=0) || $select_type=="1") {
		?>
			<?= $linterface->GET_ACTION_BTN($button_save, "submit", "") ?>
			<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
		<? } ?>
			<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:window.location='index.php'","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
		</td>
	</tr>
</table>
<input name="class_name" type="hidden" value="<?=$select_class?>">
</form>
<br />
<?
$_SESSION['UserID'] = $TempUserID;
$linterface->LAYOUT_STOP();
intranet_closedb();
?>