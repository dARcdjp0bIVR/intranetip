<?
// editing by 
############################################ Change Log #################################################
#
# 2010-02-24 by Carlos : Change SQL to select Chinese Class Name or English Class Name depending on Lang
#
#########################################################################################################
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lc = new libcardstudentattend2();
$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageAccountResponsibleAdmin_ClassRepresentative";

$linterface = new interface_html();

# class used
$LIDB = new libdb();
/*
$sql = "SELECT
            b.ClassName,
            COUNT(c.StudentID) as AdminNumber
        FROM 
        		YEAR as y 
        		inner join 
        		YEAR_CLASS as yc 
        		on y.YearID = yc.YearID and yc.AcademicYearID = '".Get_Current_Academic_Year_ID()."' 
            LEFT OUTER JOIN INTRANET_USER AS b 
            ON (yc.ClassTitleEN=b.ClassName) 
            LEFT OUTER JOIN CARD_STUDENT_HELPER_STUDENT AS c 
            ON (c.StudentID=b.UserID)
        WHERE 
        	b.ClassName IS NOT NULL 
        	and 
        	b.RecordStatus = 1
        GROUP BY b.ClassName
				";
*/
if ($_SESSION['intranet_session_language'] == 'en') {
	$classname_field = "IF(yc.ClassTitleEN IS NULL OR yc.ClassTitleEN = '',yc.ClassTitleB5,yc.ClassTitleEN) ";
}
else {
	$classname_field = "IF(yc.ClassTitleB5 IS NULL OR yc.ClassTitleB5 = '',yc.ClassTitleEN,yc.ClassTitleB5) ";
}
$sql = "SELECT
            $classname_field as ClassName,
            yc.ClassTitleEN as LinkClassName,
            COUNT(c.StudentID) as AdminNumber
        FROM 
        		YEAR as y 
        		inner join 
        		YEAR_CLASS as yc 
        		on y.YearID = yc.YearID and yc.AcademicYearID = '".Get_Current_Academic_Year_ID()."' 
            LEFT OUTER JOIN INTRANET_USER AS b 
            ON (yc.ClassTitleEN=b.ClassName) 
            LEFT OUTER JOIN CARD_STUDENT_HELPER_STUDENT AS c 
            ON (c.StudentID=b.UserID)
        WHERE 
        	b.ClassName IS NOT NULL 
        	and 
        	b.RecordStatus = 1
        GROUP BY ClassName ";

$result = $LIDB->returnArray($sql, 2);
$temp = $lc->getClassListNotTakeAttendance();
for ($i=0; $i<sizeof($temp); $i++)
{
     list($id,$name) = $temp[$i];
     if ($name!="")
     {
         $no_need_array[$name] = $id;
     }
}

$x = "<table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">
		<tr class=\"tabletop\">
		<td width=\"30%\" class=\"tabletoplink\">$i_UserClassName</td>
		<td width=\"70%\" class=\"tabletoplink\">$i_SmartCard_Settings_Admin_Number_Students</td>
		</tr>
		";

for($i=0; $i<sizeOf($result); $i++){
        list($class_name, $link_class_name, $admin_number) = $result[$i];
        $css = ($i%2==0)?"tablerow1":"tablerow2";
        if ($no_need_array[$class_name]=="")
        {
            $link = "<a class=\"tablelink\" href=\"edit.php?class_name=".urlencode($link_class_name)."\">$class_name</a>";
            $x .= "<tr class=\"$css\"><td class=\"tabletext\">$link</td><td class=\"tabletext\">$admin_number</td></tr>\n";
        }
        else
        {
            $x .= "<tr class=\"$css\"><td class=\"tabletext\">$class_name</td><td class=\"tabletext\">$i_StudentAttendance_NoNeedTakeAttendance</td></tr>\n";
        }
}

$x .= "</table>\n";

#$toolbar = $linterface->GET_LNK_NEW("javascript:checkGet(document.form1,'new.php')");

$TAGS_OBJ[] = array($i_SmartCard_Responsible_Admin_Settings_List, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/admin/class/list/", 0);
$TAGS_OBJ[] = array($i_SmartCard_Responsible_Admin_Settings_Manage, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/attendance/admin/class/manage/", 1);
		
$MODULE_OBJ = $lc->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START(urldecode($Msg));

if ($msg == 1) $SysMsg = $linterface->GET_SYS_MSG("add");
if ($msg == 2) $SysMsg = $linterface->GET_SYS_MSG("update");
if ($msg == 3) $SysMsg = $linterface->GET_SYS_MSG("delete");
?>
<br />
<form name="form1" method="get">
<table width="96%" border="0" cellpadding="3" cellspacing="0" align="center">
	<tr>
		<td align="left"></td>
		<td align="right"><?=$SysMsg?></td>
	</tr>
</table>
<?=$x?>
</form>
<br />
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>