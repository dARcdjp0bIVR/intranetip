<?php
// Editing by 
/*
 * 2017-10-26 (Carlos): Created for setting responsible users for taking attendance.
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	intranet_closedb();
	header("Location: /");
	exit();
}

$lc = new libcardstudentattend2();
$CurrentPageArr['StudentAttendance'] = 1;
$CurrentPage = "PageAccountResponsibleAdmin_GroupResponsibleUsers";

$linterface = new interface_html();

$TAGS_OBJ[] = array($Lang['StudentAttendance']['GroupResponsibleUsers'],'', 1);
$MODULE_OBJ = $lc->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();
?>
<?=$linterface->Include_JS_CSS();?>
<br />
<form id="form1" name="form1" method="post" action="">
<div class="content_top_tool">
	<br style="clear:both" />
</div>
<div id="DataTable" class="table_board">
</div>
<br style="clear:both" />
</form>
<script type="text/javascript" language="javascript">
function loadDataTable()
{
	$('#DataTable').html('<?=$linterface->Get_Ajax_Loading_Image()?>');
	$.get(
		'ajax.php?task=getDataTable',
		{
			
		},
		function(returnHtml){
			$('#DataTable').html(returnHtml);
			tb_init('a.thickbox');
		}
	);
}

function loadUIForAddRemoveUsers(GroupID)
{
	$('#TB_ajaxContent').html('<?=$linterface->Get_Ajax_Loading_Image()?>');
	$.post(
		'ajax.php',
		{
			'task':'getAddRemoveUsersUI',
			'GroupID':GroupID 
		},
		function(returnHtml){
			$('#TB_ajaxContent').html(returnHtml);
		}
	);
}

function submitModalForm(formObj)
{
	Block_Thickbox();
	Select_All_Options("GroupUserID[]",true);
	
	$.post(
		'ajax.php',
		$(formObj).serialize(),
		function(returnMsg){
			tb_remove();
			Get_Return_Message(returnMsg);
			loadDataTable();
		}
	);
}

$(document).ready(function(){
	loadDataTable();
});
</script>
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>