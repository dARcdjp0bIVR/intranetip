<?php
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

if(!$_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");

$linterface = new interface_html();
$CurrentPageArr['eAdminHomework'] = 1;
$CurrentPage = "Settings";
$lhomework = new libhomework2007();

$PAGE_NAVIGATION[] = array($Lang['SysMgr']['Homework']['BasicSettings'], "index.php");
$PAGE_NAVIGATION[] = array($Lang['SysMgr']['Homework']['Edit'], "");

# Left menu 
$TAGS_OBJ[] = array($Lang['SysMgr']['Homework']['BasicSettings']);
$MODULE_OBJ = $lhomework->GET_MODULE_OBJ_ARR();

# Start layout
$linterface->LAYOUT_START();
?>

<br />   
<form name="form1" method="get" action="edit_update.php">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?= $linterface->GET_NAVIGATION4($PAGE_NAVIGATION) ?></td>
	<td align='right'><?=$linterface->GET_SYS_MSG($xmsg);?></td>
</tr>

<tr>
	<td colspan="2">
		<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr valign='top'>
			<td width='80%' valign="top" class="formfieldtitle" nowrap><?=$Lang['SysMgr']['Homework']['Disable']?></td>
			<td width="20%" align="left" valign="top">
				<input type="radio" name="disabled" value="1" <?=$lhomework->disabled ? "checked":"" ?> id="disabled1"> <label for="disabled1"><?=$i_general_yes?></label> 
				<input type="radio" name="disabled" value="0" <?=$lhomework->disabled ? "":"checked" ?> id="disabled0"> <label for="disabled0"><?=$i_general_no?></label>
			</td>
		</tr>
		</table>
	</td>
</tr>

<tr>
	<td colspan="2">
		<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr valign='top'>
			<td width='80%' valign="top" class="formfieldtitle" nowrap><?=$Lang['SysMgr']['Homework']['TeacherSearchDisabled']?></td>
			<td width="20%" align="left" valign="top">
				<input type="radio" name="teacherSearchDisabled" value="1" <?=$lhomework->teacherSearchDisabled ? "checked":"" ?> id="teacherSearchDisabled1"> <label for="teacherSearchDisabled1"><?=$i_general_yes?></label> 
				<input type="radio" name="teacherSearchDisabled" value="0" <?=$lhomework->teacherSearchDisabled ? "":"checked" ?> id="teacherSearchDisabled0"> <label for="teacherSearchDisabled0"><?=$i_general_no?></label>
			</td>
		</tr>
		</table>
	</td>
</tr>

<tr>
	<td colspan="2">
		<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr valign='top'>
			<td width='80%' valign="top" class="formfieldtitle" nowrap><?=$Lang['SysMgr']['Homework']['SubjectSearchDisabled']?></td>
			<td width="20%" align="left" valign="top">
				<input type="radio" name="subjectSearchDisabled" value="1" <?=$lhomework->subjectSearchDisabled ? "checked":"" ?> id="subjectSearchDisabled1"> <label for="subjectSearchDisabled1"><?=$i_general_yes?></label> 
				<input type="radio" name="subjectSearchDisabled" value="0" <?=$lhomework->subjectSearchDisabled ? "":"checked" ?> id="subjectSearchDisabled0"> <label for="subjectSearchDisabled0"><?=$i_general_no?></label>
			</td>
		</tr>
		</table>
	</td>
</tr>

<tr>
	<td colspan="2">
		<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr valign='top'>
			<td width='80%' valign="top" class="formfieldtitle" nowrap><?=$Lang['SysMgr']['Homework']['StartFixed']?></td>
			<td width="20%" align="left" valign="top">
				<input type="radio" name="startFixed" value="1" <?=$lhomework->startFixed ? "checked":"" ?> id="startFixed1"> <label for="startFixed1"><?=$i_general_yes?></label> 
				<input type="radio" name="startFixed" value="0" <?=$lhomework->startFixed ? "":"checked" ?> id="startFixed0"> <label for="startFixed0"><?=$i_general_no?></label>
			</td>
		</tr>
		</table>
	</td>
</tr>

<tr>
	<td colspan="2">
		<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr valign='top'>
			<td width='80%' valign="top" class="formfieldtitle" nowrap><?=$Lang['SysMgr']['Homework']['ParentAllowed']?></td>
			<td width="20%" align="left" valign="top">
				<input type="radio" name="parentAllowed" value="1" <?=$lhomework->parentAllowed ? "checked":"" ?> id="parentAllowed1"> <label for="parentAllowed1"><?=$i_general_yes?></label> 
				<input type="radio" name="parentAllowed" value="0" <?=$lhomework->parentAllowed ? "":"checked" ?> id="parentAllowed0"> <label for="parentAllowed0"><?=$i_general_no?></label>
			</td>
		</tr>
		</table>
	</td>
</tr>

<tr>
	<td colspan="2">
		<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr valign='top'>
			<td width='80%' valign="top" class="formfieldtitle" nowrap><?=$Lang['SysMgr']['Homework']['NonTeachingAllowed']?></td>
			<td width="20%" align="left" valign="top">
				<input type="radio" name="nonTeachingAllowed" value="1" <?=$lhomework->nonteachingAllowed ? "checked":"" ?> id="nonTeachingAllowed1"> <label for="nonTeachingAllowed1"><?=$i_general_yes?></label> 
				<input type="radio" name="nonTeachingAllowed" value="0" <?=$lhomework->nonteachingAllowed ? "":"checked" ?> id="nonTeachingAllowed0"> <label for="nonTeachingAllowed0"><?=$i_general_no?></label>
			</td>
		</tr>
		</table>
	</td>
</tr>

<tr>
	<td colspan="2">
		<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr valign='top'>
			<td width='80%' valign="top" class="formfieldtitle" nowrap><?=$Lang['SysMgr']['Homework']['AllowSubLeader']?></td>
			<td width="20%" align="left" valign="top">
				<input type="radio" name="allowSubLeader" value="1" <?=$lhomework->subjectLeaderAllowed ? "checked":"" ?> id="allowSubLeader1"> <label for="allowSubLeader1"><?=$i_general_yes?></label> 
				<input type="radio" name="allowSubLeader" value="0" <?=$lhomework->subjectLeaderAllowed ? "":"checked" ?> id="allowSubLeader0"> <label for="allowSubLeader0"><?=$i_general_no?></label>
			</td>
		</tr>
		</table>
	</td>
</tr>

<tr>
	<td colspan="2">
		<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr valign='top'>
			<td width='80%' valign="top" class="formfieldtitle" nowrap><?=$Lang['SysMgr']['Homework']['AllowExport']?></td>
			<td width="20%" align="left" valign="top">
				<input type="radio" name="allowExport" value="1" <?=$lhomework->exportAllowed ? "checked":"" ?> id="allowExport1"> <label for="allowExport1"><?=$i_general_yes?></label> 
				<input type="radio" name="allowExport" value="0" <?=$lhomework->exportAllowed ? "":"checked" ?> id="allowExport0"> <label for="allowExport0"><?=$i_general_no?></label>
			</td>
		</tr>
		</table>
	</td>
</tr>

<tr>
	<td colspan="2">
		<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr valign='top'>
			<td width='80%' valign="top" class="formfieldtitle" nowrap><?=$Lang['SysMgr']['Homework']['AllowPast']?></td>
			<td width="20%" align="left" valign="top">
				<input type="radio" name="allowPast" value="1" <?=$lhomework->pastInputAllowed ? "checked":"" ?> id="allowPast1"> <label for="allowPast1"><?=$i_general_yes?></label> 
				<input type="radio" name="allowPast" value="0" <?=$lhomework->pastInputAllowed ? "":"checked" ?> id="allowPast0"> <label for="allowPast0"><?=$i_general_no?></label>
			</td>
		</tr>
		</table>
	</td>
</tr>

<tr>
	<td colspan="2">
		<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr valign='top'>
			<td width='80%' valign="top" class="formfieldtitle" nowrap><?=$Lang['SysMgr']['Homework']['DefaltHandinRequired']?></td>
			<td width="20%" align="left" valign="top">
				<input type="radio" name="DeafultHandinRequired" value="1" <?=$lhomework->DeafultHandinRequired ? "checked":"" ?> id="DeafultHandinRequired1"> <label for="DeafultHandinRequired1"><?=$i_general_yes?></label> 
				<input type="radio" name="DeafultHandinRequired" value="0" <?=$lhomework->DeafultHandinRequired ? "":"checked" ?> id="DeafultHandinRequired0"> <label for="DeafultHandinRequired0"><?=$i_general_no?></label>
			</td>
		</tr>
		</table>
	</td>
</tr>

<tr>
	<td colspan="2">
		<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr valign='top'>
			<td width='80%' valign="top" class="formfieldtitle" nowrap><?=$Lang['SysMgr']['Homework']['UseHomeworkCollect']?></td>
			<td width="20%" align="left" valign="top">
				<input type="radio" name="useHomeworkCollect" value="1" <?=$lhomework->useHomeworkCollect ? "checked":"" ?> id="useHomeworkCollect1"> <label for="useHomeworkCollect1"><?=$i_general_yes?></label> 
				<input type="radio" name="useHomeworkCollect" value="0" <?=$lhomework->useHomeworkCollect ? "":"checked" ?> id="useHomeworkCollect0"> <label for="useHomeworkCollect0"><?=$i_general_no?></label>
			</td>
		</tr>
		</table>
	</td>
</tr>


<tr>
	<td colspan="2">        
        <table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
        <tr>
        	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
        </tr>
        <tr>
			<td align="center">
				<?= $linterface->GET_ACTION_BTN($button_update, "submit") ?>
				<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "history.back()","cancelbtn") ?>
			</td>
		</tr>
        </table>                                
	</td>
</tr>

</table>                        
<br />

</form>


<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>