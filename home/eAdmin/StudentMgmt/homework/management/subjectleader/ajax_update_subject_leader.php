<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$lhomework = new libhomework2007();

$gpIDAry = $lhomework->getSubjectGroupByClassID($classID);

$values = "";
$delim = "";
$memberIDs = array();
$result = true;

for($i=0; $i<sizeof($gpIDAry); $i++) {
	$gpID = $gpIDAry[$i];
	if($gpID!="" && $gpID!=0) {
		$gpMember = $lhomework->getSubjectLeaderInfoByGroupID($gpID, $classID);
		for($j=0; $j<sizeof($gpMember); $j++)
			$memberIDs[] = $gpMember[$j][0];
		
		# remove existing subject leader of selected "Subject Group" & "Class"
		if(sizeof($memberIDs)>0) {
			$sql = "DELETE FROM INTRANET_SUBJECT_LEADER WHERE ClassID='$gpID' AND UserID IN (".implode(',', $memberIDs).")";
			$lhomework->db_db_query($sql);	
		}
		
		# Subject ID 
		$sql = "SELECT SubjectID FROM SUBJECT_TERM WHERE SubjectGroupID='$gpID'";
		$temp = $lhomework->returnVector($sql);
		$subjectID = $temp[0];
		
		$dataAry = ${'SubjectGroup_'.$gpID};
		if(isset($dataAry) && sizeof($dataAry)>0) {
			foreach($dataAry as $studentID) {
				if($studentID!="") {
					$values .= $delim."('$studentID', '$gpID', '$subjectID', 1, NOW(), NOW())";
					$delim = ", ";
				}	
			}	
		}
	}
}

if($values !="") {
	$sql = "INSERT INTO INTRANET_SUBJECT_LEADER (UserID, ClassID, SubjectID, RecordStatus, DateInput, DateModified) VALUES $values";
	$result = $lhomework->db_db_query($sql);
}

intranet_closedb();

if($result)
	echo $Lang['General']['ReturnMessage']['UpdateSuccess'];
else
	echo $Lang['General']['ReturnMessage']['UpdateUnsuccess'];

?>