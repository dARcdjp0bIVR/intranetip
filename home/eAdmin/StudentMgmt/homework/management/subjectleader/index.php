<?php
// Modifying : Bill

####### Change log [Start] #######
#
#	Date:	2016-09-20	Bill	[2016-0919-1619-23066]
#			fixed: not set to correct semester after new / import subject leader
#
##################################
		
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");

intranet_auth();
intranet_opendb();

$lhomework = new libhomework2007();

if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || !($_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"] || $_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] && !$lhomework->isViewerGroupMember($UserID)))
{
	if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])
	{
		header("location: ../../settings/index.php");	
		exit;
	}
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}
# Temp Assign memory of this page
ini_set("memory_limit", "150M");

$linterface = new interface_html();

$CurrentPageArr['eAdminHomework'] = 1;

$CurrentPage = "Management_SubjectLeader";
$MODULE_OBJ = $lhomework->GET_MODULE_OBJ_ARR();

$linterface = new interface_html();

$PAGE_TITLE = $Lang['SysMgr']['Homework']['SubjectLeader'];

# tag information
$TAGS_OBJ[] = array($Lang['SysMgr']['Homework']['SubjectLeader'], "index.php", 1); 
$TAGS_OBJ[] = array($Lang['SysMgr']['Homework']['ViewSubjectLeaderByClass'], "classView.php", 0); 

$today = date('Y-m-d');
# Current Year
$yearID = Get_Current_Academic_Year_ID();


# Current Year Term
/*
$currentYearTerm = getAcademicYearAndYearTermByDate($today);
$yearTermID = $currentYearTerm[0];
if($yearTermID=="")
	$yearTermID = 0;
*/

# Select sort field
$sortField = 0;

# Semester Menu
$semAry = array();
$getSemesters = getSemesters($yearID);
$i = 0;
foreach($getSemesters as $id=>$name) {
	$semAry[$i][] = $id;
	$semAry[$i][] = $name;
	$i++;
}

// fixed: not set to correct semester after new / import subject leader 
$yearTermID = $yearTermID!=''? $yearTermID : GetCurrentSemesterID();
$yearTermID = ($semester!='') ? $semester : $yearTermID;

$selectSemester = getSelectByArray($semAry, 'name="semester" id="semester" onChange="reloadForm()"', $yearTermID, 0, 1, "");

# Class Menu
$classes = $lhomework->getAllClassInfo();
$selectClass = getSelectByArray($classes, 'name="classID" id="classID" onChange="document.form1.submit();"', $classID, 0, 0, $i_general_all_classes);

# Subject Menu
# if user is eHomework admin, then he can assign all the subject/subject group's leader
$UserIDTmp = $_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] ? "" : $UserID;
$subject = $lhomework->getTeachingSubjectList($UserIDTmp, $yearID, $yearTermID, $classID);


// $subjectID = ($subjectID=="")? $subject[0][0]:$subjectID;
$subjectID = ($subjectID=="")? -1 :$subjectID;
$display = (sizeof($subject)==0 && !$lhomework->isViewerGroupMember($UserID))? false:true;
$thisUserID = $_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] ? "" : $UserID;

if($display)
{
// 	$selectedSubject = $lhomework->getCurrentTeachingSubjects("name=\"subjectID\" onChange=\"reloadForm()\"", $subject, $subjectID, "", false);
// 	if($subjectID!=""){
// 		$subjectGroups = $lhomework->getTeachingSubjectGroupList($UserIDTmp, $subjectID, $yearID, $yearTermID);
// 		$selectedSubjectGroups = $lhomework->getCurrentTeachingGroups("name=\"subjectGroupID\" onChange=\"reloadForm()\"", $subjectGroups, $subjectGroupID, $Lang['SysMgr']['Homework']['AllSubjectGroups']);
// 	}

	$selectedSubject = $lhomework->getCurrentTeachingSubjects("name=\"subjectID\" onChange=\"document.form1.subjectGroupID.selectedIndex=0; reloadForm()\"", $subject, $subjectID, $Lang['SysMgr']['Homework']['PleaseSelect'], true, -1);
	if($subjectID!="-1"){
		
		$subjectGroups = $lhomework->getTeachingSubjectGroupList($thisUserID,$subjectID, $yearID, $yearTermID, $classID);
	}
	else
	{
		$subjectGroups = $lhomework->getTeachingSubjectGroupList($thisUserID,"", $yearID, $yearTermID, $classID);
	}
	$selectedSubjectGroups = $lhomework->getCurrentTeachingGroups("name=\"subjectGroupID\" onChange=\"reloadForm()\"", $subjectGroups, $subjectGroupID, $Lang['SysMgr']['Homework']['AllSubjectGroups']);

	$filterbar = "$selectSemester $selectClass $selectedSubject $selectedSubjectGroups";

	if($sid !="" && $sid != $subjectID){
		$subjectGroupID="";
	}
	
	# Toolbar: new
	//$toolbar = $linterface->GET_LNK_NEW("add.php?yearID=$yearID&yearTermID=$yearTermID&classID=$classID&subjectID=$subjectID&subjectGroupID=$subjectGroupID", "", "", "", "", 0);
	$y = "yearID=$yearID&yearTermID=$yearTermID&classID=$classID&subjectID=$subjectID&subjectGroupID=$subjectGroupID";
	
	if(!$lhomework->ViewOnly) {
		$toolbar = '	<div class="content_top_tool">
				<div class="Conntent_tool">
										<div class="btn_option">
						<a href="javascript:;" class="new" id="btn_new" onclick="MM_showHideLayers(\'new_option\',\'\',\'show\');document.getElementById(\'btn_new\').className = \'new parent_btn\';"> '.$button_new.'</a>
						<br style="clear:both" />
						<div class="btn_option_layer" id="new_option" onclick="MM_showHideLayers(\'new_option\',\'\',\'hide\');document.getElementById(\'btn_new\').className = \'new\';">
						<a href="add.php?'.$y.'" class="sub_btn">'.$i_StudentAttendance_By.' '.$Lang['SysMgr']['Homework']['SubjectGroup'].'</a>
						<a href="new.php?'.$y.'" class="sub_btn">'.$i_StudentAttendance_By.' '.$i_UserStudentName.'</a>
						</div>	  
					</div>
				</div>
			</div>';
	
		$toolbar .= $linterface->GET_LNK_IMPORT("import.php?yearID=$yearID&yearTermID=$yearTermID&classID=$classID&subjectID=$subjectID&subjectGroupID=$subjectGroupID", "", "", "", "",0);
	}
	$toolbar .= $linterface->GET_LNK_EXPORT("export.php?yearID=$yearID&yearTermID=$yearTermID&classID=$classID&subjectID=$subjectID&subjectGroupID=$subjectGroupID", "", "", "", "",0);
	
	# change page size
	if ($page_size_change == 1)
	{
		setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
		$ck_page_size = $numPerPage;
	}

	if (isset($ck_page_size) && $ck_page_size != "") 
		$page_size = $ck_page_size;

	$pageSizeChangeEnabled = true;

	# Table initialization
	$order = ($order == "") ? 1 : $order;
	$field = ($field == "") ? $sortField : $field;
	$pageNo = ($pageNo == "") ? 1 : $pageNo;

	# Create a new dbtable instance
	$li = new libdbtable2007($field, $order, $pageNo);

	# SQL statement
	$s = addcslashes($s, '_');
	$searchBySubject =($lhomework->subjectSearchDisabled == 0) ? " or IF('$intranet_session_language' = 'en', c.EN_DES, c.CH_DES) like '%$s%'" : "" ;
	$searchBySubjectGroup = "IF('$intranet_session_language' = 'en', d.ClassTitleEN, d.ClassTitleB5) like '%$s%'";
	$searchByClass = " or f.ClassName like '%$s%'";
	$searchByStudentName = " or IF('$intranet_session_language' = 'en', f.EnglishName, f.ChineseName) like '%$s%'";

	$search = "$searchBySubjectGroup $searchByClass $searchByStudentName $searchBySubject";
		
	if(sizeof($subjectGroups)!=0){
		$allGroups = " d.SubjectGroupID IN (";
		for ($i=0; $i < sizeof($subjectGroups); $i++)
		{	
			list($groupID)=$subjectGroups[$i];
			$allGroups .= $groupID.",";
		}
		$allGroups = substr($allGroups,0,strlen($allGroups)-1).")";
		$allGroups .=" AND";
	}

	else{
		$allGroups = "";
	}

	# set conditions
	$name_field = getNameFieldByLang("f.");
	
	$conds = ($subjectGroupID=='')? "$allGroups" : " d.SubjectGroupID = $subjectGroupID AND";
	$conds .= ($s=='')? "": " ($search) AND";

	$fields = "IF('$intranet_session_language'='en', c.EN_DES, c.CH_DES) AS Subject, 
			   IF('$intranet_session_language'='en', d.ClassTitleEN, d.ClassTitleB5) AS SubjectGroup, 
			   f.ClassName, f.ClassNumber, 
			   $name_field AS StudentName, 
			   CONCAT('<input type=checkbox name=LeaderID[] value=', e.LeaderID ,' onClick=\"unset_checkall(this, this.form);\">')";

	if($_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] || $lhomework->isViewerGroupMember($UserID))
	{		
		$dbtables = "
				SUBJECT_TERM AS a
				INNER JOIN ASSESSMENT_SUBJECT AS c ON c.RecordID = a.SubjectID
				INNER JOIN SUBJECT_TERM_CLASS AS d ON d.SubjectGroupID = a.SubjectGroupID
				INNER JOIN INTRANET_SUBJECT_LEADER AS e ON e.ClassID = a.SubjectGroupID
				INNER JOIN INTRANET_USER AS f ON f.UserID = e.UserID
				INNER JOIN ACADEMIC_YEAR_TERM AS g ON g.YearTermID = a.YearTermID 
				INNER JOIN ACADEMIC_YEAR AS h ON h.AcademicYearID = g.AcademicYearID";
				
		$conds .= " h.AcademicYearID = $yearID AND g.YearTermID= $yearTermID AND e.RecordStatus = '1'";		
		
		if($classID!="") {
				
			$dbtables .= " INNER JOIN SUBJECT_TERM_CLASS_USER stcu ON stcu.SubjectGroupID=a.SubjectGroupID
							INNER JOIN YEAR_CLASS_USER ycu ON ycu.UserID=stcu.UserID AND e.UserID=ycu.UserID";
			$conds .= " AND ycu.YearClassID IN ($classID)";
		} 
		
		$groupBy = " GROUP BY a.SubjectGroupID, e.UserID";
	}
	else
	{		   		
		/*	   
		$dbtables = "SUBJECT_TERM AS b 
				INNER JOIN SUBJECT_TERM_CLASS_TEACHER AS a ON b.SubjectGroupID = a.SubjectGroupID
				INNER JOIN ASSESSMENT_SUBJECT AS c ON c.RecordID = b.SubjectID
				INNER JOIN SUBJECT_TERM_CLASS AS d ON d.SubjectGroupID = b.SubjectGroupID
				INNER JOIN INTRANET_SUBJECT_LEADER AS e ON e.ClassID = b.SubjectGroupID
				INNER JOIN INTRANET_USER AS f ON f.UserID = e.UserID
				INNER JOIN ACADEMIC_YEAR_TERM AS g ON g.YearTermID = b.YearTermID 
				INNER JOIN ACADEMIC_YEAR AS h ON h.AcademicYearID = g.AcademicYearID";
		$conds .= " (a.UserID = '$UserID') AND h.AcademicYearID = $yearID AND g.YearTermID= $yearTermID AND e.RecordStatus = '1'";
		*/
		$dbtables = "SUBJECT_TERM AS b 
				LEFT OUTER JOIN SUBJECT_TERM_CLASS_TEACHER AS a ON b.SubjectGroupID = a.SubjectGroupID
				LEFT OUTER JOIN SUBJECT_TERM_CLASS_USER AS k ON k.SubjectGroupID=b.SubjectGroupID
				INNER JOIN YEAR_CLASS_USER AS ycu ON ycu.UserID=k.UserID
				INNER JOIN YEAR_CLASS_TEACHER AS yct ON yct.YearClassID=ycu.YearClassID
				INNER JOIN ASSESSMENT_SUBJECT AS c ON c.RecordID = b.SubjectID
				INNER JOIN SUBJECT_TERM_CLASS AS d ON d.SubjectGroupID = b.SubjectGroupID
				INNER JOIN INTRANET_SUBJECT_LEADER AS e ON e.ClassID = b.SubjectGroupID AND e.UserID=ycu.UserID
				INNER JOIN INTRANET_USER AS f ON f.UserID = e.UserID
				INNER JOIN ACADEMIC_YEAR_TERM AS g ON g.YearTermID = b.YearTermID 
				INNER JOIN ACADEMIC_YEAR AS h ON h.AcademicYearID = g.AcademicYearID
				";
		$conds .= " (a.UserID = '$UserID' OR yct.UserID='$UserID') AND h.AcademicYearID = $yearID AND g.YearTermID= $yearTermID AND e.RecordStatus = '1'";
		
		if($classID!="") {
			$conds .= " AND ycu.YearClassID IN ($classID)";
		}
		$groupBy = " GROUP BY b.SubjectGroupID, e.UserID";
	}
	
	$sql = "SELECT $fields from $dbtables WHERE $conds $groupBy";
	//echo $sql;
	
	$li->field_array = array("Subject", "SubjectGroup", "f.ClassName", "f.ClassNumber", "StudentName", "e.LeaderID"); 
	$li->sql = $sql;
	$li->no_col = sizeof($li->field_array)+1;
	$li->IsColOff = 2;

	# TABLE COLUMN
	$pos = 0;
	$li->column_list .= "<td width='5%'>#</td>\n";
	$li->column_list .= "<td width='20%' class='tabletop tabletoplink'>".$li->column($pos++, $Lang['SysMgr']['Homework']['Subject'])."</td>\n";
	$li->column_list .= "<td width='25%' class='tabletop tabletoplink'>".$li->column($pos++, $Lang['SysMgr']['Homework']['SubjectGroup'])."</td>\n";
	$li->column_list .= "<td width='10%' class='tabletop tabletoplink'>".$li->column($pos++, $Lang['SysMgr']['Homework']['Class'])."</td>\n";
	$li->column_list .= "<td width='15%' class='tabletop tabletoplink'>".$li->column($pos++, $Lang['SysMgr']['Homework']['ClassNumber'])."</td>\n";
	$li->column_list .= "<td width='25%' class='tabletop tabletoplink'>".$li->column($pos++, $i_UserName)."</td>\n";
	$li->column_list .= "<td width='5%' class='tabletop tabletoplink'>".$li->check("LeaderID[]")."</td>\n";
}

$htmlAry['searchBox'] = $linterface->Get_Search_Box_Div('s', $s);
# Start layout
$linterface->LAYOUT_START();
?>

<script language="javascript">
<!--


function goSearch(obj) {

	document.form1.s.value = document.form2.text.value;
	document.form1.pageNo.value = 1;
	document.form1.submit();
	return false;
}

function reloadForm() {
	document.form1.s.value = "";
	document.form1.pageNo.value = 1;
	document.form1.submit();
}
	
function removeCat(obj,element,page){
		var alertConfirmRemove = "<?=$i_Discipline_System_alert_remove_record?>";
        if(countChecked(obj,element)==0)
                alert(globalAlertMsg2);
        else{
                if(confirm(alertConfirmRemove)){
                obj.action=page;
                obj.method="post";
                obj.submit();
                }
        }
}

<? 
if($returnMsg==1)
	echo "Get_Return_Message(\"".$Lang['eHomework']['SubjectLeaderAddedSuccessfully']."\");\n";
?>
//-->
</script>


<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
			<form name="form2" method="post" onSubmit="return goSearch('document.form1')">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td>
							<table border="0" cellspacing="0" cellpadding="2" width="400">
								<tr>
									<td><?=$toolbar ?></td>
								</tr>
							</table>
						</td>
						<?if($display){?>
								
							<td align="right"><?=$htmlAry['searchBox']?>
							<!--<input name="text" id="text" type="text" class="formtextbox" value="<?=stripslashes(stripslashes($s))?>">
								<?=$linterface->GET_BTN($button_find, "button","javascript:goSearch('document.form1')");?>-->
							</td>
						<?}?>
					</tr>
				</table>
			</form>
			<form name="form1" method="POST" action="">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td align="right">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td height="28" align="right" valign="bottom">
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td align="right"><?= $linterface->GET_SYS_MSG($msg) ?>
											</td>
										</tr>
									</table>
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td height="30">
												<?=$filterbar?>
											</td>
											<td align="right" valign="middle" class="thumb_list"><?=$choiceType?></td>
										</tr>
									</table>
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr class="table-action-bar">
											<td>
											</td>
											<td align="right" valign="bottom">
											<?if($display && !$lhomework->ViewOnly){?>
												<table border="0" cellspacing="0" cellpadding="0">
													<tr>
														<td width="21"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_01.gif" width="21" height="23"></td>
														<td background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_02.gif">
															<table border="0" cellspacing="0" cellpadding="2">
																<tr>
																<td nowrap><a href="javascript:removeCat(document.form1,'LeaderID[]','remove_update.php?yearID=<?=$yearID?>&yearTermID=<?=$yearTermID?>&subjectID=<?=$subjectID?>&subjectGroupID=<?=$subjectGroupID?>')" class="tabletool" title="<?= $button_delete ?>"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_delete.gif" width="12" height="12" border="0" align="absmiddle"> <?= $button_delete ?></a></td>
																	<td><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="5"></td>
																</tr>
															</table>
														</td>
														<td width="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_03.gif" width="6" height="23"></td>
													</tr>
												</table>
											<?}?>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>
						<?
						if($display){
							echo $li->display();
						}
						else{
							echo $Lang['SysMgr']['Homework']['SubjectLeaderWarning'];
						}
						?>
					</td>
				</tr>
			</table><br>
			<input type="hidden" name="pageNo" id="pageNo" value="<?php echo $li->pageNo; ?>"/>
			<input type="hidden" name="order" id="order" value="<?php echo $li->order; ?>"/>
			<input type="hidden" name="field" id="field" value="<?php echo $li->field; ?>"/>
			<input type="hidden" name="page_size_change" id="page_size_change" value=""/>
			<input type="hidden" name="numPerPage" id="numPerPage" value="<?=$li->page_size?>"/>
			<input type="hidden" name="s" id="s" value="<?=$s?>"/>
			<input type="hidden" name="sid" id="sid" value="<?=$subjectID?>"/>
			</form>
		</td>
	</tr>
</table>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>