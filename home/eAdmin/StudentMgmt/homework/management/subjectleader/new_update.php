<?php

$PATH_WRT_ROOT = "../../../../../../";
$top_menu_mode = 0;

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || !($_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"] || $_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"]))
{
	if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])
	{
		header("location: ../../settings/index.php");	
		exit;
	}
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}


$lhomework = new libhomework2007();

$CurrentPageArr['eAdminHomework'] = 1;

$values = "";
$updateSQL = array();
$delim = "";

for($i=0; $i<sizeof($subjectGroupID); $i++) {
	$sg = $subjectGroupID[$i];
	$sql = "SELECT LeaderID, RecordStatus FROM INTRANET_SUBJECT_LEADER WHERE ClassID='$sg' AND UserID='$studentID'";	
	$data = $lhomework->returnArray($sql,2);
	if(sizeof($data)==0) {	# student not yet assigned to this subject group 
		$sgAry = $lhomework->RetrieveSubjectbySubjectGroupID($sg);
		list($subjectID, $subjectName) = $sgAry[0];
		$values .= $delim."('$studentID', '$sg', '$subjectID', 1, NOW(), NOW())";
		$delim = ", ";
	} else if($data[0][1]==0) {
		$updateSQL[] = "UPDATE INTRANET_SUBJECT_LEADER SET RecordStatus=1 WHERE ClassID='$sg' AND UserID='$studentID'";	
	}
	
}
if($values!="") {
	$sql = "INSERT INTO INTRANET_SUBJECT_LEADER (UserID, ClassID, SubjectID, RecordStatus, DateInput, DateModified) VALUES $values";
	$return = $lhomework->db_db_query($sql);
}

if(sizeof($updateSQL)>0) {
	foreach($updateSQL as $sql) {
		$lhomework->db_db_query($sql);	
	}
}

intranet_closedb();
header("Location: index.php?returnMsg=1&yearTermID=$yearTermID");
?>