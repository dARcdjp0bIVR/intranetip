<?php
$PATH_WRT_ROOT = "../../../../../../";
$top_menu_mode = 0;

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || !($_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"] || $_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"]))
{
	if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])
	{
		header("location: ../../settings/index.php");	
		exit;
	}
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}


$lhomework = new libhomework2007();
$lu = new libuser($UserID);

$CurrentPageArr['eAdminHomework'] = 1;

$LeaderIDAry = array();

if(is_array($LeaderID))
	$LeaderIDAry = $LeaderID;
else
	$LeaderIDAry[] = $LeaderID;

	
$all_delete = 1;

foreach($LeaderIDAry as $ListLeaderID)
{
	//echo $ListHomeworkID;
	if ($lu->isTeacherStaff())
	{
		if ($lhomework->nonteachingAllowed || $lu->teaching == 1)
		{
			$haveAccess = true;
		}
	}

	if ($haveAccess)
	{
		//$sql = "DELETE FROM INTRANET_SUBJECT_LEADER WHERE LeaderID = $ListLeaderID";
		$sql = "UPDATE INTRANET_SUBJECT_LEADER SET RecordStatus = '0', DateModified = now() WHERE LeaderID = $ListLeaderID";
        $lhomework->db_db_query($sql);
		header ("Location: index.php?");
	}
	else
	{
		$all_delete = 0;
	}
}

$msg = $all_delete ? "delete":"delete_failed";
header("Location: index.php?yearID=$yearID&yearTermID=$yearTermID&subjectID=$subjectID&subjectGroupID=$subjectGroupID&msg=$msg");

intranet_closedb();
?>