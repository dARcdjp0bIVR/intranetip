<?php
// Modifying by : Bill

############ Change Log [Start] ############
#
#	Date:	2016-09-08	Bill	[2016-0907-1759-59235]
#			Fixed: js error due to missing drop down list id
#
############ Change Log [End] ############
		
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || !($_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"] || $_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"]))
{
	if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])
	{
		header("location: ../../settings/index.php");	
		exit;
	}
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}


# Temp assign memory of this page
ini_set("memory_limit", "150M");

$linterface = new interface_html();
$lhomework = new libhomework2007();
$lu = new libuser($UserID);

$CurrentPageArr['eAdminHomework'] = 1;
$CurrentPage = "Management_SubjectLeader";

$MODULE_OBJ = $lhomework->GET_MODULE_OBJ_ARR();

$linterface = new interface_html();

$PAGE_TITLE = $Lang['SysMgr']['Homework']['SubjectLeader'];

# Tag
$TAGS_OBJ[] = array($Lang['SysMgr']['Homework']['SubjectLeader'], "index.php", 1); 
$TAGS_OBJ[] = array($Lang['SysMgr']['Homework']['ViewSubjectLeaderByClass'], "classView.php", 0); 

$PAGE_NAVIGATION[] = array($Lang['SysMgr']['Homework']['SubjectLeader'], "index.php?yearID=$yearID&yearTermID=$yearTermID&subjectID=$subjectID&subjectGroupID=$subjectGroupID");
$PAGE_NAVIGATION[] = array($Lang['SysMgr']['Homework']['AssignSubjectLeader'], "");

$MODULE_OBJ = $lhomework->GET_MODULE_OBJ_ARR();

# Select sort field
$sortField = 0;

# Change Page Size
if ($page_size_change == 1)
{
	setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
	$ck_page_size = $numPerPage;
}
if (isset($ck_page_size) && $ck_page_size != "") 
	$page_size = $ck_page_size;

/*$pageSizeChangeEnabled = true;*/

# Table initialization
$order = ($order == 1) ? 1 : 0;
$field = ($field == "") ? $sortField : $field;
$pageNo = ($pageNo == "") ? 1 : $pageNo;

# Create a new DB Table instance
$ltable = new libdbtable2007($field, $order, $pageNo);
# if user is eHomework admin, then he can assign all the subject/subject group's leadr
$UserIDTmp = $_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] ? "" : $UserID;

# Class Menu
$classes = $lhomework->getAllClassInfo();
$selectClass = getSelectByArray($classes, 'name="classID" id="classID" onChange="this.form.method=\'get\'; this.form.action=\'\'; if(document.getElementById(\'subjectID\')) { document.getElementById(\'subjectID\').selectedIndex=0; } if(document.getElementById(\'subjectGroupID\')) { document.getElementById(\'subjectGroupID\').selectedIndex=0; } this.form.submit();"', $classID, 0, 0, $i_general_all_classes);

$subject = $lhomework->getTeachingSubjectList($UserIDTmp, $yearID, $yearTermID, $classID);
$subjectID = ($subjectID=="")? $subject[0][0]:$subjectID;

// $selectedSubject = $lhomework->getCurrentTeachingSubjects("name=\"subjectID\" onChange=\"this.form.method='get'; this.form.action=''; this.form.submit();\"", $subject, $subjectID, "", false);
$selectedSubject = $lhomework->getCurrentTeachingSubjects("name=\"subjectID\" id=\"subjectID\" onChange=\"this.form.method='get'; this.form.action=''; if(document.getElementById('subjectGroupID')){ document.getElementById('subjectGroupID').selectedIndex=0; } this.form.submit();\"", $subject, $subjectID, $Lang['SysMgr']['Homework']['PleaseSelect'], true);

if($subjectID!=""){
	$subjectGroups = $lhomework->getTeachingSubjectGroupList($UserIDTmp, $subjectID, $yearID, $yearTermID, $classID);
	$selectedSubjectGroup = getSelectByArray($subjectGroups, "name=\"subjectGroupID\" id=\"subjectGroupID\" onChange=\"this.form.method='get'; this.form.action=''; this.form.submit();\"", $subjectGroupID, 0, 0);
}

if($sid!="" && $sid != $subjectID){
	$subjectGroupID="";
}

$filterbar3 = $selectClass;
$filterbar = $selectedSubject;
$filterbar2 = $selectedSubjectGroup;
	
# Start layout
$linterface->LAYOUT_START();

?>

<script language=javascript>


function addSubjectLeader(obj,element,page){
		//var alertConfirmRemove = "<?=$i_Discipline_System_alert_remove_record?>";
        if(countChecked(obj,element)==0)
                alert(globalAlertMsg2);
        else{
			obj.action=page;
			obj.method="post";
			obj.submit();
        }
}

function back(sid, sgid)
{
  window.location="index.php?subjectID="+sid+"&subjectGroupID="+sgid;        
}

</script>

<form name="form1" method="post">
	<table width="100%" order="0" cellspacing="0" cellpadding="5" align="center">
		<tr>
			<td class="navigation">
				<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr><td><?=$linterface->GET_NAVIGATION4($PAGE_NAVIGATION)?></td></tr>
				</table>
			</td>
		</tr>
	</table>

	<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tr>
			<td class="tabletext formfieldtitle" width="30%" valign="top"><?=$Lang['SysMgr']['Homework']['Class']?> <span class="tabletextrequire">*</span></td>
			<td width="567" align="left" valign="top">
				<?=$filterbar3?>
			</td>
		</tr>
		<tr>
			<td class="tabletext formfieldtitle" width="30%" valign="top"><?=$Lang['SysMgr']['Homework']['Subject']?> <span class="tabletextrequire">*</span></td>
			<td width="567" align="left" valign="top">
				<?=$filterbar?>
			</td>
		</tr>
		
		<tr>
			<td class="tabletext formfieldtitle" width="30%" valign="top"><?=$Lang['SysMgr']['Homework']['SubjectGroup']?> <span class="tabletextrequire">*</span></td>
			<td width="567" align="left" valign="top">
				<?=$filterbar2?>
			</td>
		</tr>
	</table>
	<br/>
		<?
		if ($subjectGroupID != "")
		{	

			$fields = "d.ClassName, d.ClassNumber, d.EnglishName, d.ChineseName, f.EN_DES, CONCAT('<input type=checkbox name=RecordID[] value=', d.UserID ,' onClick=\"unset_checkall(this, this.form);\">')";
			if($_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"])
			{
				$dbtables = "
						SUBJECT_TERM_CLASS_USER as c
						INNER JOIN INTRANET_USER AS d ON d.UserID = c.UserID 
						INNER JOIN SUBJECT_TERM AS e ON e.SubjectGroupID = c.SubjectGroupID 
						INNER JOIN ASSESSMENT_SUBJECT AS f ON f.RecordID = e.SubjectID
						INNER JOIN ACADEMIC_YEAR_TERM AS g ON g.YearTermID = e.YearTermID 
						INNER JOIN ACADEMIC_YEAR AS h ON h.AcademicYearID = g.AcademicYearID";
				$conds = "c.SubjectGroupID = $subjectGroupID AND h.AcademicYearID = $yearID AND g.YearTermID = $yearTermID";
				
				if($classID!="") {
					$dbtables .= " INNER JOIN YEAR_CLASS_USER as ycu ON ycu.UserID=c.UserID ";
					$conds .= " AND ycu.YearClassID IN ($classID)";
				}
			}
			else
			{
				$dbtables = "SUBJECT_TERM_CLASS_TEACHER AS a 
						INNER JOIN SUBJECT_TERM_CLASS AS b ON b.SubjectGroupID = a.SubjectGroupID 
						INNER JOIN SUBJECT_TERM_CLASS_USER AS c ON c.SubjectGroupID = a.SubjectGroupID 
						INNER JOIN INTRANET_USER AS d ON d.UserID = c.UserID 
						INNER JOIN SUBJECT_TERM AS e ON e.SubjectGroupID = b.SubjectGroupID 
						INNER JOIN ASSESSMENT_SUBJECT AS f ON f.RecordID = e.SubjectID
						INNER JOIN ACADEMIC_YEAR_TERM AS g ON g.YearTermID = e.YearTermID 
						INNER JOIN ACADEMIC_YEAR AS h ON h.AcademicYearID = g.AcademicYearID";
				$conds = "a.SubjectGroupID = $subjectGroupID AND h.AcademicYearID = $yearID AND g.YearTermID = $yearTermID AND a.UserID = $UserID";
				
				if($classID!="") {
					$dbtables .= " INNER JOIN YEAR_CLASS_USER as ycu ON ycu.UserID=c.UserID ";
					$conds .= " AND ycu.YearClassID IN ($classID)";
				}				
			}

			
			$sql = "SELECT $fields FROM $dbtables WHERE $conds";
			//echo $sql;
			$ltable->field_array = array("d.ClassName", "d.ClassNumber", "d.EnglishName", "d.ChineseName", "f.EN_DES", "d.UserID"); 
			$ltable->sql = $sql;
			$ltable->no_col = sizeof($ltable->field_array)+1;
			$ltable->IsColOff = 2;
			$ltable->fieldorder2 = ", d.ClassName, d.ClassNumber";
			# TABLE COLUMN
			$pos = 0;
			$ltable->column_list .= "<td width='10%'>#</td>\n";
			$ltable->column_list .= "<td width='15%' class='tabletop tabletoplink'>".$ltable->column($pos++, $i_Homework_class)."</td>\n";
			$ltable->column_list .= "<td width='15%' class='tabletop tabletoplink'>".$ltable->column($pos++, $i_ClassNumber)."</td>\n";
			$ltable->column_list .= "<td width='15%' class='tabletop tabletoplink'>".$ltable->column($pos++, $i_UserEnglishName)."</td>\n";
			$ltable->column_list .= "<td width='20%' class='tabletop tabletoplink'>".$ltable->column($pos++, $i_UserChineseName)."</td>\n";
			$ltable->column_list .= "<td width='20%' class='tabletop tabletoplink'>".$ltable->column($pos++, $i_Homework_subject)."</td>\n";
			$ltable->column_list .= "<td width='5%' class='tabletop tabletoplink'>".$ltable->check("RecordID[]")."</td>\n";
		?>
		<table width="98%" border="0" cellpadding="5" cellspacing="0" align="center">
			<tr>
				<td class="tabletext formfieldtitle" width="30%" valign="top" colspan=2>
				<?=$ltable->display()?>
				</td>
			</tr>
		</table>
		<?
		  }
		?>
				
	<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
					<tr><td align="left" class="tabletextremark">&nbsp; &nbsp; &nbsp; <?=$i_general_required_field?></td></tr>
					<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
					<?
						if ($subjectGroupID != "")
						{  
					?>
					<tr>
						<td align="center">
							<?= $linterface->GET_ACTION_BTN($button_submit, "button", "javascript:addSubjectLeader(document.form1,'RecordID[]', 'add_update.php')")?>&nbsp;
							<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:back($subjectID, $subjectGroupID)")?>
						</td>
					</tr>
					<?
						}
					?>
				</table>
			</td>
		</tr>
	</table>
	<input type="hidden" name="pageNo" value="<?php echo $ltable->pageNo; ?>"/>
	<input type="hidden" name="order" value="<?php echo $ltable->order; ?>"/>
	<input type="hidden" name="field" value="<?php echo $ltable->field; ?>"/>
	<input type="hidden" name="page_size_change" value=""/>
	<input type="hidden" name="numPerPage" value="100"/>
	<input type="hidden" name="yearID" value="<?=$yearID?>"/>
	<input type="hidden" name="yearTermID" value="<?=$yearTermID?>"/>
	<input type="hidden" name="sid" value="<?=$subjectID?>"/>
</form>
<?
	$linterface->LAYOUT_STOP();
	intranet_closedb();
?>