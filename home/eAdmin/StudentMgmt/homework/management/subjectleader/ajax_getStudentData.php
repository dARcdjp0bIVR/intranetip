<?php
// modifying : 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || !($_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"] || $_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"]))
{ 
	if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])
	{
		header("location: ../../settings/index.php");	
		exit;
	}
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}


$linterface = new interface_html();
$lhomework = new libhomework2007();

$CurrentPageArr['eAdminHomework'] = 1;

# Get Year and Year Term 
$currentYearTerm = $lhomework->getCurrentAcademicYearAndYearTerm();
$yearID = $currentYearTerm[0]['AcademicYearID'];
$yearTermID = $currentYearTerm[0]['YearTermID'];

if($subjectGroupID == ""){
	$subjectGroups = $lhomework->getTeachingSubjectGroupList($UserID, $subjectID, $yearID, $yearTermID);
	
	if(sizeof($subjectGroups)!=0){
		
		$allGroups = " b.SubjectGroupID IN (";
		for ($i=0; $i < sizeof($subjectGroups); $i++)
		{	
			list($groupID)=$subjectGroups[$i];
			$allGroups .= $groupID.",";
		}
		$allGroups = substr($allGroups,0,strlen($allGroups)-1).")";
		$allGroups .=" AND";
		
	}
	else{
		$allGroups ="";
	}
}

else{
	$allGroups  = "b.SubjectGroupID = $subjectGroupID AND";
}

if(isset($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"]))
	$addCond = " OR 1";

$sql = "SELECT e.CODEID, 
		IF('$intranet_session_language' = 'en', e.EN_DES, e.CH_DES) AS Subject,
		d.ClassCode,
		IF('$intranet_session_language' = 'en', d.ClassTitleEN, d.ClassTitleB5) AS SubjectGroup,
		g.UserID, g.EnglishName, g.ClassName, g.ClassNumber
		FROM SUBJECT_TERM AS a 
		LEFT OUTER JOIN SUBJECT_TERM_CLASS_USER AS b ON b.SubjectGroupID = a.SubjectGroupID
		LEFT OUTER JOIN SUBJECT_TERM_CLASS_TEACHER AS c ON c.SubjectGroupID = a.SubjectGroupID
		INNER JOIN SUBJECT_TERM_CLASS AS d ON d.SubjectGroupID = a.SubjectGroupID
		INNER JOIN ASSESSMENT_SUBJECT AS e ON e.RecordID = a.SubjectID
		INNER JOIN ACADEMIC_YEAR_TERM AS f ON f.YearTermID = a.YearTermID
		INNER JOIN INTRANET_USER AS g ON g.UserID = b.UserID
		LEFT OUTER JOIN YEAR_CLASS_USER ycu ON (ycu.UserID=b.UserID)
		LEFT OUTER JOIN YEAR_CLASS_TEACHER yct ON (yct.YearClassID=ycu.YearClassID)
		WHERE (c.UserID = $UserID OR yct.UserID=$UserID $addCond) AND $allGroups f.AcademicYearID = $yearID AND f.YearTermID = $yearTermID
		GROUP BY d.SubjectGroupID, b.UserID
		ORDER BY Subject, g.ClassName, g.ClassNumber";

$array = $lhomework->returnArray($sql);

$data = "<table width=\"90%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#CCCCCC\" class=\"layer_table_detail\">
			<tr class=\"tabletop\">
				<td>#</td>
				<td>".$Lang['SysMgr']['Homework']['Subject']."</td>
				<td>".$Lang['SysMgr']['Homework']['SubjectGroup']."</td>
				<td>".$Lang['SysMgr']['Homework']['StudentName']."</td>
				<td>".$Lang['SysMgr']['Homework']['Class']."</td>
				<td>".$Lang['SysMgr']['Homework']['ClassNumber']."</td>
			</tr>";
				
				
for($a=0;$a<sizeof($array);$a++)
{
	$subject = intranet_htmlspecialchars($array[$a]['Subject']);
	$subjectGroup = intranet_htmlspecialchars($array[$a]['SubjectGroup']);
	$studentUserID = intranet_htmlspecialchars($array[$a]['UserID']);
	$studentName = intranet_htmlspecialchars($array[$a]['EnglishName']);
	$className = intranet_htmlspecialchars($array[$a]['ClassName']);
	$classNumber = intranet_htmlspecialchars($array[$a]['ClassNumber']);
	
	if($studentUserID!='')
	{
		$data .="<tr class=\"tablerow1\">
					<td>".($a+1)."</td>
					<td>".$subject."</td>
					<td>".$subjectGroup."</td>
					<td>".$studentName."</td>
					<td>".$className."</td>
					<td>".$classNumber."</td>
				</tr>";
	}
}

if(sizeof($array)==0){
	$data .="<tr class=\"tablerow1\" >
				<td colspan=\"6\" align=\"center\">".$Lang['SysMgr']['Homework']['NoRecord']."</td>
			</tr>"; 
}

$data .="</table>";	

echo $data;

intranet_closedb();
?>