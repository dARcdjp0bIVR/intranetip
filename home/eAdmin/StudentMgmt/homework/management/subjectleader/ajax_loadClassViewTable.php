<?php
//using 

/***************************************
 * 
 * Date: 2013-01-22 (Rita)
 * Details: amend "self.location.href='javascript:;';Update_Subject_Leader()" to "Update_Subject_Leader();"
 * 
 ****************************************/
 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$lhomework = new libhomework2007();

$sql = "SELECT 
			SUB.RecordID, ".
			Get_Lang_Selection("SUB.CH_DES", "SUB.EN_DES")." as subject, 
			stc.SubjectGroupID, ".
			Get_Lang_Selection("stc.ClassTitleB5","stc.ClassTitleEN")." as groupname 
		FROM 
			YEAR_CLASS_USER ycu INNER JOIN 
			SUBJECT_TERM_CLASS_USER stcu ON (stcu.UserID=ycu.UserID) INNER JOIN 
			SUBJECT_TERM_CLASS stc ON (stc.SubjectGroupID=stcu.SubjectGroupID) INNER JOIN 
			SUBJECT_TERM st ON (st.SubjectGroupID=stcu.SubjectGroupID) INNER JOIN 
			ASSESSMENT_SUBJECT SUB ON (SUB.RecordID=st.SubjectID) 
		WHERE 
			ycu.YearClassID = '$classID' AND 
			st.YearTermID = ".getCurrentSemesterID()."
		GROUP BY st.SubjectGroupID 
		ORDER BY SUB.DisplayOrder";
$data = $lhomework->returnArray($sql,4);

if(!$lhomework->ViewOnly && sizeof($data)!=0) {
	$x = '<div align="right" class="EditBtn">'.$linterface->GET_SMALL_BTN($button_edit, "button","javascript:EditInfo();","EditBtn").'</div><br>';	
}

# Count number of columns for subject group
$max = 0; 
$count = array();
$subjectIDAry = array();
$gpAry = array();

for($i=0; $i<sizeof($data); $i++) {
	list($subjectID, $subjectName, $groupID, $groupName) = $data[$i];
	if(!isset($subjectIDAry[$subjectID])) {
		$count[$subjectID] = 1;
		$subjectIDAry[$subjectID] = $subjectName;	
	} else {
		$count[$subjectID]++;	
	}
	if(!isset($gpAry[$subjectID][$groupID])) $gpAry[$subjectID][$groupID] = $groupName;
	
	if($max<$count[$subjectID]) $max = $count[$subjectID];
}

# table content 
$x .= '
		<script language="javascript">
		<!--
		function EditInfo()
		{
			$(\'#EditBtn\').attr(\'style\', \'visibility: hidden\'); 
			$(\'.Edit_Hide\').attr(\'style\', \'display: none\');
			$(\'.Edit_Show\').attr(\'style\', \'\');
			
			$(\'#UpdateBtn\').attr(\'style\', \'\');
			$(\'#cancelbtn\').attr(\'style\', \'\');
		}		
		
		function ResetInfo()
		{
			
			$(\'#EditBtn\').attr(\'style\', \'\');
			$(\'.Edit_Hide\').attr(\'style\', \'\');
			$(\'.Edit_Show\').attr(\'style\', \'display: none\');
			
			$(\'#UpdateBtn\').attr(\'style\', \'display: none\');
			$(\'#cancelbtn\').attr(\'style\', \'display: none\');
		}
		//-->
		</script>		
				';
$firstWidth = 20;
$otherWidth = 100 - $firstWidth;
$indiWidth = ($max!=0) ? $otherWidth/$max : $otherWidth;

$x .= '
	<table class="common_table_list">
		<tr>
			<th>'.$Lang['SysMgr']['Homework']['Subject'].'</th><th colspan="'.$max.'">'.$Lang['SysMgr']['Homework']['SubjectGroup'].'</th>
		</tr>';
		foreach($subjectIDAry as $sbjID=>$sbjName) {	
		$x .= '	<tr>
					<td width='.$firstWidth.'%>'.$sbjName.'</td>';
			foreach($gpAry[$sbjID] as $gpID=>$gpName) {		
				$x .= '	<td width='.$indiWidth.'%>'.$gpName.'<br>';
				$y = "";
				$data = $lhomework->getGroupMemberByGroupIDAndClassID($gpID, $classID);
				$leaderInfo = $lhomework->getSubjectLeaderInfoByGroupID($gpID, $classID);
				$leader = $lhomework->getSubjectLeaderByGroupID($gpID, $classID);
				//debug_pr($leader);
				if(sizeof($leader)==0) $leader = "";
				
				$delim = "";
				for($i=0; $i<sizeof($leaderInfo); $i++) {
					$y .= $delim.'- '.$leaderInfo[$i][1];
					$delim = "<br>";
				}
				$selectMenu = getSelectByArray($data,' name="SubjectGroup_'.$gpID.'[]" id="SubjectGroup_'.$gpID.'[]" multiple style="width:150px"', $leader, 0, 0, $i_general_na);
				if(sizeof($leaderInfo)!=0) {
					$x .= '<span class="Edit_Hide">'.$y.'</span>';
					$x .= '<span class="Edit_Show" style="display:none">'.$selectMenu.'</span>';	
				} else {
					$x .= '<span class="tabletextremark Edit_Hide">- '.$i_Discipline_System_Not_Yet_Assigned."</span>";
					$x .= '<span class="Edit_Show" style="display:none">'.$selectMenu.'</span>';	
				}
				$x .= '</td>';
			}
			for($i=0; $i<($max-sizeof($gpAry[$sbjID])); $i++) {	# fill in the missing table cell
				$x .= '<td>&nbsp;</td>';
			}
			$x .= '</tr>';
		}
		if(sizeof($subjectIDAry)==0) {
			$x .= '<tr><td colspan="2" align="center" height="40"><div align="center">'.$i_no_record_exists_msg.'</div></td></tr>';	
		}
$x .='</table>
';

$x .= '
	<div class="edit_bottom_v30">
	<p class="spacer"></p>';
$x .= $linterface->GET_ACTION_BTN($button_update, "button","Update_Subject_Leader();","UpdateBtn", "style='display: none'").'&nbsp;';
$x .=  $linterface->GET_ACTION_BTN($button_cancel, "button", "ResetInfo();","cancelbtn", "style='display: none'");
$x .= '	<p class="spacer"></p>
	</div>	
	';


echo $x;

intranet_closedb();
?>