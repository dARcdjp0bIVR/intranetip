<?php
// Modifying by : Bill

####### Change log [Start] #######
#
#	Date:	2016-09-20	Bill	[2016-0919-1619-23066]
#			- use yearTermID from new.php if isset
#
##################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$lhomework = new libhomework2007();
$linterface = new interface_html();

// [2016-0919-1619-23066] set yearTermID
$yearTermID = $yearTermID? $yearTermID : GetCurrentSemesterID();

# class menu
if($_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"])
	$classes = $lhomework->getAllClassInfo();
else
	$classes = $lhomework->getAllClassesInvolvedByTeacherID(Get_Current_Academic_Year_ID(), $yearTermID, $UserID);

$selectClass = getSelectByArray($classes, 'name="classID" id="classID" onChange="changeSelection();"', $classID, 0, 0, "-- ".$i_alert_pleaseselect."--");

# student name menu
if(isset($classID) && $classID!="") {
	$studentArray = $lhomework->retrieveStudentListByYearClassID($classID, 2);
	$selectedStudent = getSelectByArray($studentArray, ' name="studentID" id="studentID" onChange="changeSelection()"', $studentID, 0, 0, "-- ".$i_alert_pleaseselect."--");
} else {
	$selectedStudent = "<SELECT NAME='studentID' id='studentID'><option>-- $i_alert_pleaseselect --</option></SELECT>";	
}

# 
if(isset($studentID) && $studentID!="") {
	$subjectGroupArray = $lhomework->retrieveSubjectGroupByStudentID($yearTermID, $studentID);
	$selectedSubjectGroup = getSelectByArray($subjectGroupArray, ' name="subjectGroupID[]" id="subjectGroupID[]" multiple', "", 0, 1, "");
	$selectedSubjectGroup .= $linterface->GET_BTN($button_select_all, "button", "SelectAll(document.addSubjectLeader.elements['subjectGroupID[]'])");
	
	
	$btn = $linterface->GET_ACTION_BTN($button_submit, "submit", "")."&nbsp;".
			$linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:self.location.href='index.php'");
} else {
	$selectedSubjectGroup = "<SELECT NAME='subjectGroupID[]' id='subjectGroupID[]' multiple></SELECT>";
}


$filterbar1 = $selectClass;
$filterbar2 = $selectedStudent;
$filterbar3 = $selectedSubjectGroup;

$x = '
<table class="form_table_v30">
';
if($_SESSION['UserType']!=USERTYPE_STUDENT || ($_SESSION['UserType']==USERTYPE_STUDENT && $_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"])) {
$x .= '
<tr>
	<td class="field_title"><span class="tabletextrequire">*</span> '. $i_general_class .'</td>
	<td>'. $filterbar1 .'</td>
</tr>
';
}
$x .= '
<tr>
	<td class="field_title"><span class="tabletextrequire">*</span> '. $i_UserStudentName .'</td>
	<td>'. $filterbar2 .'</td>
</tr>

<tr>
	<td class="field_title"><span class="tabletextrequire">*</span> '. $Lang['SysMgr']['Homework']['SubjectGroup'] .' </td>
	<td>'. $filterbar3 .'</td>
</tr>
</table>
';
$x .= '
<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
	<tr>
		<td>
			<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
				<tr><td align="left" class="tabletextremark">'.$i_general_required_field.'</td></tr>
				<tr>
					<td>
						<div class="edit_bottom_v30">
							'.$btn.'
						</div>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
		';
		
$x .= '<input type="hidden" name="yearTermID" id="yearTermID" value="'.$yearTermID.'">'; 



intranet_closedb();
echo $x;

?>