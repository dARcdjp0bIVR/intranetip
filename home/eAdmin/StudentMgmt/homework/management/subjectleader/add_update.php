<?php
// Using :

######## Change Log [Start] ################
#
#	Date:	2019-05-13 (Bill)
#           - prevent SQL Injection
#
######## Change Log [End] ################

$PATH_WRT_ROOT = "../../../../../../";
$top_menu_mode = 0;

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

### Handle SQL Injection + XSS [START]
$RecordID = IntegerSafe($RecordID);
$subjectGroupID = IntegerSafe($subjectGroupID);
### Handle SQL Injection + XSS [END]

if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || !($_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"] || $_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"]))
{
	if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])
	{
		header("location: ../../settings/index.php");	
		exit;
	}
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$lhomework = new libhomework2007();

$CurrentPageArr['eAdminHomework'] = 1;

$RecordIDAry = array();
if(is_array($RecordID)) {
	$RecordIDAry = $RecordID;
}
else {
	$RecordIDAry[] = $RecordID;
}

$sql = "SELECT SubjectID FROM SUBJECT_TERM WHERE SubjectGroupID = '$subjectGroupID' ";
$result = $lhomework->returnArray($sql);
$subjectID = $result[0]['SubjectID'];

$sql = "SELECT UserID, RecordStatus FROM INTRANET_SUBJECT_LEADER WHERE ClassID = '$subjectGroupID'";
$result = $lhomework->returnArray($sql);

$recordIDArray = array();
$recordStatusArray = array();
if(sizeof($result)!=0){
	for ($a=0; $a<sizeof($result); $a++){
		list ($recordID,$recordStatus) = $result[$a];
		array_push($recordIDArray, $recordID);
		array_push($recordStatusArray, $recordStatus);
	}
}

foreach($RecordIDAry as $recordID)
{
	if(in_array($recordID,$recordIDArray)) {
		$key = array_search($recordID, $recordIDArray);
		
		if($recordStatusArray[$key]=="0"){
			$sql = "UPDATE INTRANET_SUBJECT_LEADER SET RecordStatus = '1', DateModified = now() WHERE UserID = '$recordID' AND ClassID = '$subjectGroupID'";
			$lhomework->db_db_query($sql);
		}
	}
	
	else{
		$sql = "INSERT INTO INTRANET_SUBJECT_LEADER (UserID, ClassID, SubjectID, RecordStatus, DateInput) VALUES ('$recordID', '$subjectGroupID', '$subjectID', '1', now())";
		$lhomework->db_db_query($sql);
	}
}

intranet_closedb();
header("Location: index.php?yearID=$yearID&yearTermID=$yearTermID&subjectID=$subjectID&subjectGroupID=$subjectGroupID");
?>