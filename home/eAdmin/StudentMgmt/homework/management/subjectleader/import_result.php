<?
// modifying by : henry chow

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || !($_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"] || $_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"]))
{
	if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])
	{
		header("location: ../../settings/index.php");	
		exit;
	}
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}


# Temp Assign memory of this page
ini_set("memory_limit", "150M");

$lhomework = new libhomework2007();
$linterface = new interface_html();
$lu = new libuser();

$CurrentPageArr['eAdminHomework'] = 1;

# menu highlight setting
$CurrentPage = "Management_SubjectLeader";

$PAGE_TITLE = $Lang['SysMgr']['Homework']['SubjectLeader'];

# tag information
$TAGS_OBJ[] = array($Lang['SysMgr']['Homework']['SubjectLeader'], "index.php", 1); 
$TAGS_OBJ[] = array($Lang['SysMgr']['Homework']['ViewSubjectLeaderByClass'], "classView.php", 0); 

$MODULE_OBJ = $lhomework->GET_MODULE_OBJ_ARR();

# step information
$STEPS_OBJ[] = array($i_general_select_csv_file, 0);
$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 1);
$STEPS_OBJ[] = array($i_general_imported_result, 0);

# page netvigation
$PAGE_NAVIGATION[] = array($Lang['SysMgr']['Homework']['SubjectLeader'], "index.php?yearID=$yearID&yearTermID=$yearTermID&subjectID=$subjectID&subjectGroupID=$subjectGroupID");
$PAGE_NAVIGATION[] = array($Lang['SysMgr']['Homework']['ImportRecord'], "");


$limport = new libimporttext();
$lo = new libfilesystem();

$name = $_FILES['csvfile']['name'];
$ext = strtoupper($lo->file_ext($name));

if(!($ext == ".CSV" || $ext == ".TXT"))
{
	intranet_closedb();
	header("location: import.php?xmsg=import_failed");
	exit();
}

$data = $limport->GET_IMPORT_TXT($csvfile);
if(is_array($data))
{
	$col_name = array_shift($data);
}

$file_format = array("Subject Group Code", "Class", "Class Number");
$format_wrong = false;

for($i=0; $i<sizeof($file_format); $i++)
{
	if ($col_name[$i]!=$file_format[$i])
	{
		$format_wrong = true;
		break;
	}
}
if($format_wrong)
{
	header("location: import.php?xmsg=wrong_header");
	exit();
}


$tempData = array();

$x = $lhomework->checkSubjectLeaderImportError($data, $yearID, $yearTermID);

$successCount = $lhomework->successCount;
$errorCount = $lhomework->errorCount;

### Show import result summary ###
$result = "<table width='90%' border='0' cellpadding='3' cellspacing='0' align='center'>\n";
$result .= "<tr>\n";
$result .= "<td class='formfieldtitle' width='30%' align='left'>". $Lang['General']['SuccessfulRecord'] ."</td>\n";
$result .= "<td class='tabletext'>". $successCount ."</td>\n";
$result .= "</tr>\n";
$result .= "<tr>\n";
$result .= "<td class='formfieldtitle' width='30%' align='left'>". $Lang['General']['FailureRecord'] ."</td>\n";
$result .= "<td class='tabletext'>". ($errorCount ? "<font color='red'>":"") . $errorCount . ($errorCount ? "</font>":"") ."</td>\n";
$result .= "</tr>\n";
$result .= "</table><br>\n";

if($errorCount!=0)
	$result .= $x;

if($errorCount!=0){
	$import_button = $linterface->GET_ACTION_BTN($button_back, "button", "window.location='import.php?yearID=$yearID&yearTermID=$yearTermID&subjectID=$subjectID&subjectGroupID=$subjectGroupID'");
}

else{	
	for($i=0;$i<sizeof($data);$i++)
	{
		if(!is_array($data[$i]) || trim(implode("",$data[$i]))=="") 
			continue;  // skip empty line	                 
		
		$failed = false;
		$error_occured = 0;
		
		list($subjectGroup, $class, $classNumber) = $data[$i];
		
		//$subjectGroup = intranet_htmlspecialchars($subjectGroup);
		$class = intranet_htmlspecialchars($class);
		$classNumber = intranet_htmlspecialchars($classNumber);
	
		$tempData[$i][0] = $subjectGroup;
		$tempData[$i][1] = $class;
		$tempData[$i][2] = $classNumber;
		$_SESSION['tempData'] = $tempData;
	}
	$import_button = $linterface->GET_ACTION_BTN($button_import, "submit")." &nbsp;".$linterface->GET_ACTION_BTN($button_back, "button", "window.location='import.php?yearID=$yearID&yearTermID=$yearTermID&subjectID=$subjectID&subjectGroupID=$subjectGroupID'");	
}

$linterface->LAYOUT_START();
?>

<br />

<table width="100%" border="0" cellspacing="4" cellpadding="4">
	<tr>
		<td class="navigation">
			<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
			<tr><td><?=$linterface->GET_NAVIGATION4($PAGE_NAVIGATION)?></td></tr>
			</table>
		</td>
	</tr>
</table>

<form name="form1" method="post" action="import_update.php" enctype="multipart/form-data">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
</tr>
<tr>
	<td align="center">
		<?=$result?>
	</td>
</tr>
<tr>
	<td>
		<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr>
			<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
		</tr>
		<tr>
			<td align="center" colspan="2">
			<?=$import_button?>
			</td>
		</tr>
		</table>
	</td>
</tr>
</table>
	<input type="hidden" name="tempData" value="<?=$tempData; ?>"/>
	<input type="hidden" name="yearID" value="<?=$yearID?>"/>
	<input type="hidden" name="yearTermID" value="<?=$yearTermID?>"/>
	<input type="hidden" name="subjectID" value="<?=$subjectID?>"/>
	<input type="hidden" name="subjectGroupID" value="<?=$subjectGroupID?>"/>
</form>
<br />

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>