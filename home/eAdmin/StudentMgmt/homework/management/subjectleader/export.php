<?php
# using: 
$PATH_WRT_ROOT = "../../../../../../";
$CurrentPageArr['eAdminHomework'] = 1;

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$lhomework = new libhomework2007();

if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || !($_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"] || $_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] && !$lhomework->isViewerGroupMember($UserID)))
{
	if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])
	{
		header("location: ../../settings/index.php");	
		exit;
	}
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}


$lexport = new libexporttext();

$filename = "Homework_Student_Leader.csv";

$thisUserID = $_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] ? "" : $UserID;

$ExportArr = array();

# SQL statement
$s = addcslashes($s, '_');
$searchBySubject =($lhomework->subjectSearchDisabled == 0) ? " or IF('$intranet_session_language' = 'en', d.EN_DES, d.CH_DES) like '%$s%'" : "" ;
$searchBySubjectGroup = "IF('$intranet_session_language' = 'en', d.ClassTitleEN, d.ClassTitleB5) like '%$s%'";
$searchByClass = " or f.ClassName like '%$s%'";
$searchByStudentName = " or IF('$intranet_session_language' = 'en', f.EnglishName, f.ChineseName) like '%$s%'";

$search = "$searchBySubjectGroup $searchByClass $searchByStudentName $searchBySubject";
	
if($subjectID!="-1"){
	
	$subjectGroups = $lhomework->getTeachingSubjectGroupList($thisUserID,$subjectID, $yearID, $yearTermID, $classID);
}
else
{
	$subjectGroups = $lhomework->getTeachingSubjectGroupList($thisUserID,"", $yearID, $yearTermID, $classID);
}

if(sizeof($subjectGroups)!=0){
	$allGroups = " a.SubjectGroupID IN (";
	for ($i=0; $i < sizeof($subjectGroups); $i++)
	{	
		list($groupID)=$subjectGroups[$i];
		$allGroups .= $groupID.",";
	}
	$allGroups = substr($allGroups,0,strlen($allGroups)-1).")";
	$allGroups .=" AND";
}

else{
	$allGroups = "";
}

# set conditions
$conds = ($subjectGroupID=='')? "$allGroups" : " a.SubjectGroupID = $subjectGroupID AND";
$conds .= ($s=='')? "": " ($search) AND";

$fields = "IF('$intranet_session_language'='en', c.EN_DES, c.CH_DES) AS Subject, 
		   IF('$intranet_session_language'='en', d.ClassTitleEN, d.ClassTitleB5) AS SubjectGroup, 
		   f.ClassName, f.ClassNumber, 
		   IF('$intranet_session_language'='en', f.EnglishName, f.ChineseName) AS StudentName";

if($_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] || $lhomework->isViewerGroupMember($UserID))
{
	$dbtables = "
			SUBJECT_TERM AS a
			INNER JOIN ASSESSMENT_SUBJECT AS c ON c.RecordID = a.SubjectID
			INNER JOIN SUBJECT_TERM_CLASS AS d ON d.SubjectGroupID = a.SubjectGroupID
			INNER JOIN INTRANET_SUBJECT_LEADER AS e ON e.ClassID = a.SubjectGroupID
			INNER JOIN INTRANET_USER AS f ON f.UserID = e.UserID
			INNER JOIN ACADEMIC_YEAR_TERM AS g ON g.YearTermID = a.YearTermID 
			INNER JOIN ACADEMIC_YEAR AS h ON h.AcademicYearID = g.AcademicYearID";
	$conds .= " h.AcademicYearID = $yearID AND g.YearTermID= $yearTermID AND e.RecordStatus = '1'";
}
else
{		   			   
	$dbtables = "SUBJECT_TERM_CLASS_TEACHER AS a 
			INNER JOIN SUBJECT_TERM AS b ON b.SubjectGroupID = a.SubjectGroupID
			INNER JOIN ASSESSMENT_SUBJECT AS c ON c.RecordID = b.SubjectID
			INNER JOIN SUBJECT_TERM_CLASS AS d ON d.SubjectGroupID = b.SubjectGroupID
			INNER JOIN INTRANET_SUBJECT_LEADER AS e ON e.ClassID = b.SubjectGroupID
			INNER JOIN INTRANET_USER AS f ON f.UserID = e.UserID
			INNER JOIN ACADEMIC_YEAR_TERM AS g ON g.YearTermID = b.YearTermID 
			INNER JOIN ACADEMIC_YEAR AS h ON h.AcademicYearID = g.AcademicYearID";
	$conds .= " a.UserID = $UserID AND h.AcademicYearID = $yearID AND g.YearTermID= $yearTermID AND e.RecordStatus = '1'";
}

$sql = "SELECT $fields from $dbtables WHERE $conds";
$row = $lhomework->returnArray($sql, 5);

// Create data array for export
for($i=0; $i<sizeof($row); $i++){
	$m = 0;
	$ExportArr[$i][$m] = $row[$i][0];		//Subject Name
	$m++;
	$ExportArr[$i][$m] = $row[$i][1];		//Subject Group Name
	$m++;
	$ExportArr[$i][$m] = $row[$i][2];		//Class Name
	$m++;
	$ExportArr[$i][$m] = $row[$i][3];		//Class Number			
	$m++;
	$ExportArr[$i][$m] = $row[$i][4];		//Student Name
	$m++;	
}

if(sizeof($row)==0) {
	$ExportArr[0][0] = $i_no_record_exists_msg;	
}


$exportColumn[] = $Lang['SysMgr']['Homework']['Subject'];
$exportColumn[] = $Lang['SysMgr']['Homework']['SubjectGroup'];
$exportColumn[] = $i_ClassName;
$exportColumn[] = $i_ClassNumber;
$exportColumn[] = $i_UserStudentName;	


$export_content = $lexport->GET_EXPORT_TXT($ExportArr, $exportColumn, "", "\r\n", "", 0, "00");

intranet_closedb();

// Output the file to user browser
$lexport->EXPORT_FILE($filename, $export_content);

?>
