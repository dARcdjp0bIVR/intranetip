<?php
// Modifing by : 

##### Change Log [Start] ######

###### Change Log [End] #######

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
include_once($PATH_WRT_ROOT."includes/libhomework_diary.php");

intranet_auth();
intranet_opendb();


$HomeworkID = integerSafe(standardizeFormPostValue($_POST['HomeworkID']));
$ArticleID = integerSafe(standardizeFormPostValue($_POST['ArticleID']));
$fromPage = standardizeFormPostValue($_POST['fromPage']);
$teacherScore = standardizeFormPostValue($_POST['teacherScore']);
$teacherComment = standardizeFormPostValue($_POST['teacherComment']);

$lhomework = new libhomework2007();
$lhomework_diary = new libhomework_diary();

# check access right
if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || (!$_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"]  && !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] && !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_subject_leader"] && !$lhomework->isViewerGroupMember($UserID)))
{
	if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])
	{
		header("location: ../../settings/index.php");
		exit;
	}

	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$success = $lhomework_diary->saveTeacherFeedback($ArticleID, $teacherScore, $teacherComment);

$returnMsgKey = ($success)? 'UpdateSuccess' : 'UpdateUnsuccess';
header('Location: submission_list.php?HomeworkID='.$HomeworkID.'&fromPage='.$fromPage.'&returnMsgKey='.$returnMsgKey);
?>