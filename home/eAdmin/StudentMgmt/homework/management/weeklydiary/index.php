<?php
// Modifing by : 

##### Change Log [Start] ######
#
#	Date:	2019-05-16 (Bill)
#           prevent SQL Injection + Cross-site Scripting
#
###### Change Log [End] #######

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");

intranet_auth();
intranet_opendb();

$lhomework = new libhomework2007();

# check access right
if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || (!$_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"]  && !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] && !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_subject_leader"] && !$lhomework->isViewerGroupMember($UserID)))
{
	if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])
	{
		header("location: ../../settings/index.php");
		exit;
	}

	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$linterface = new interface_html();

$lclass = new libclass();
$classID = IntegerSafe($classID);

# change page size
if ($page_size_change == 1)
{
	setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
	$ck_page_size = $numPerPage;
}
if (isset($ck_page_size) && $ck_page_size != "") {
	$page_size = $ck_page_size;
}
$pageSizeChangeEnabled = true;

# Table initialization
$order = ($order == "") ? 1 : $order;
$field = ($field == "") ? $sortField : $field;
$pageNo = ($pageNo == "") ? 1 : $pageNo;

# Create a new dbtable instance
$li = new libdbtable2007($field, $order, $pageNo);

# Settings
$s = addcslashes($s, '_');
$searchByTeacher =($lhomework->teacherSearchDisabled == 0) ? "or a.PosterName like '%$s%'" : "" ;

$today = date('Y-m-d');
$subjectID = ($subjectID=="" || $subjectID=="-1")? -1 :$subjectID;

//$today = "2011-02-10";
$yearID = Get_Current_Academic_Year_ID();

# Current Year Term
$currentYearTerm = getAcademicYearAndYearTermByDate($today);
$yearTermID = $currentYearTerm[0];
$yearTermID = $yearTermID ? $yearTermID : 0;

# Filter - Teacher List
if($_SESSION['UserType']!=USERTYPE_STUDENT || ($_SESSION['UserType']==USERTYPE_STUDENT && $_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"])) {
	if(($_SESSION['UserType']==USERTYPE_STAFF && !$_SESSION['isTeaching']) || $_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] || $lhomework->isViewerGroupMember($UserID)) {		# eHomework Admin
		$classes = $lhomework->getAllClassInfo();
	}
	else {		

		$classes = $lhomework->getAllClassesInvolvedByTeacherID(Get_Current_Academic_Year_ID(), GetCurrentSemesterID(), $UserID);
	}
	
	//echo $_SESSION['isTeaching'];
	$selectClass = getSelectByArray($classes, 'name="classID" id="classID" onChange="reloadForm()"', $classID, 0, 0, $i_general_all_classes);
}

# Filter display
$filterbar = "$selectClass";

# SQL statement
	# set conditions
	$date_conds = "AND a.DueDate >= CURDATE()";
	$conds .= ($s=='')? "": "( a.Title like '%$s%' $searchByTeacher) AND";
    $conds .= " a.Type = 1 AND";//type = 1 for weekly diary
	$desc = "CONCAT(' <img src=\"$image_path/homework/$intranet_session_language/hw/icon_alt.gif\" title=\"',a.Description,'\" \>')";
	
    $fields = "IF('$intranet_session_language' = 'en', yc.ClassTitleEN, yc.ClassTitleB5) AS Class,";
	$fields .= " CONCAT(CONCAT(CONCAT(CONCAT('<a class=tablelink href=javascript:viewHmeworkDetail(',a.HomeworkID,')>',a.Title,'</a>'), if (a.StartDate=CURDATE(),' $i_Homework_today','')), IF(a.Description='', '',$desc))),
			   a.StartDate, 
				a.DueDate, 
				concat('<a href=\"javascript:void(0);\" class=\"tablelink\" onclick=\"goSubmissionDetailsPage(', a.HomeworkID, ', 1);\">', IFNULL((Select count(iawa.ArticleID) From INTRANET_APP_WEEKLYDIARY_ARTICLE as iawa Where iawa.HomeworkID = a.HomeworkID Group By iawa.HomeworkID), '0'), '</a> / <a href=\"javascript:void(0);\" class=\"tablelink\" onclick=\"goSubmissionDetailsPage(', a.HomeworkID, ', 0);\">', count(ycu.UserID), '</a>')";
	$fields .= ", a.LastModified";
    
	if($_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] || !$lhomework->OnlyCanEditDeleteOwn || $lhomework->isViewerGroupMember($UserID)) {
		$fields .= ", CONCAT('<input type=checkbox name=HomeworkID[] id=HomeworkID value=', a.HomeworkID ,' onClick=\"unset_checkall(this, this.form);\">')";
	} else if($_SESSION['UserType']==USERTYPE_STAFF) {
		$fields .= ", IF(('".($lhomework->ClassTeacherCanViewHomeworkOnly)."'=0 OR '".($lhomework->ClassTeacherCanViewHomeworkOnly)."'='' OR a.PosterUserID='$UserID' OR a.CreatedBy='$UserID' OR sl.UserID!='' OR stct.SubjectGroupID IS NOT NULL),CONCAT('<input type=checkbox name=HomeworkID[] id=HomeworkID[] value=', a.HomeworkID ,' onClick=\"unset_checkall(this, this.form);\">'),'-')";	
	} else {	# student account (check subject leader)
		$fields .= ", IF((a.PosterUserID = '$UserID' OR a.CreatedBy = '$UserID'),CONCAT('<input type=checkbox name=HomeworkID[] id=HomeworkID value=', a.HomeworkID ,' onClick=\"unset_checkall(this, this.form);\">'),'')";
	}
	
	//$dbtables = "INTRANET_HOMEWORK_WEEKLYDIARY_NEWSPAPERCUTTING as a LEFT OUTER JOIN YEAR_CLASS as yc ON yc.YearClassID = a.ClassID";
	$dbtables = "INTRANET_HOMEWORK_WEEKLYDIARY_NEWSPAPERCUTTING as a
				 Inner JOIN YEAR_CLASS as yc ON yc.YearClassID = a.ClassID
				 Left JOIN YEAR_CLASS_USER as ycu on (yc.YearClassID = ycu.YearClassID) ";
	$conds .= " (a.YearTermID = '$yearTermID' OR a.DueDate>=CURDATE()) AND";
    
	# do not map by user, should allow for same subject group
	$conds .= " a.AcademicYearID = '$yearID' $date_conds";
	if($classID != "") {
		$conds .= " AND yc.YearClassID = '$classID'";
	}
    
	$sql = "SELECT $fields FROM $dbtables WHERE $conds GROUP BY a.HomeworkID";
	
//	debug_pr($sql);
//	die();
//	echo $sql;
	/*
	# Editable List 
	$sqlEditableList = "SELECT a.HomeworkID FROM $dbtables INNER JOIN SUBJECT_TERM_CLASS_TEACHER stct ON (stct.SubjectGroupID=a.ClassGroupID AND stct.UserID='$UserID') WHERE $conds Group BY a.HomeworkID";
	$tempHW = $lhomework->returnVector($sqlEditableList);
	*/
	
	$li->field_array =array("Class","a.Title", "a.StartDate", "a.DueDate");
	$li->sql = $sql;
	//echo $sql;
	
	$li->no_col = sizeof($li->field_array)+3;
		
	$li->field_array[] = "a.LastModified";	
	$li->no_col++;

	$li->IsColOff = "IP25_table";
	$li->count_mode = 1;

	# TABLE COLUMN
	$pos = 0;
	$li->column_list .= "<th width='3%'>#</th>\n";
	$li->column_list .= "<th width='15%' class='tabletop tabletoplink'>".$li->column_IP25($pos++, $Lang['SysMgr']['Homework']['Class'])."</th>\n";
	$li->column_list .= "<th class='tabletop tabletoplink'>".$li->column_IP25($pos++, $Lang['SysMgr']['Homework']['Topic'])."</th>\n";
	$li->column_list .= "<th width='12%' class='tabletop tabletoplink'>".$li->column_IP25($pos++, $Lang['SysMgr']['Homework']['StartDate'])."</th>\n";
	$li->column_list .= "<th width='12%' class='tabletop tabletoplink'>".$li->column_IP25($pos++, $Lang['SysMgr']['Homework']['DueDate'])."</th>\n";
	$li->column_list .= "<th width='10%' class='tabletop tabletopnolink'>".$Lang['SysMgr']['Homework']['Submitted'].' / '.$Lang['General']['Total']."</th>\n";
	$li->column_list .= "<th width='15%' class='tabletop tabletoplink'>".$li->column_IP25($pos++, $Lang['SysMgr']['Homework']['LastModified'])."</th>\n";
	$li->column_list .= "<th width='3%' class='tabletop tabletoplink'>".$li->check("HomeworkID[]")."</th>\n";

if(!$lhomework->ViewOnly || $_SESSION['SSV_PRIVILEGE']['homework']['is_subject_leader'] || !$_SESSION['isTeaching']) {
    # Toolbar: new, import, export
    $toolbar = $linterface->GET_LNK_NEW("add.php?yearID=$yearID&yearTermID=$yearTermID&classID=$classID", "", "", "", "",0);
}

$htmlAry['searchBox'] = $linterface->Get_Search_Box_Div('text', stripslashes(stripslashes($s)));

# Start layout
$CurrentPageArr['eAdminHomework'] = 1;
$CurrentPage = "Management_WeeklyDiary";

$PAGE_TITLE = $Lang['SysMgr']['Homework']['WeeklyDiary'];
$MODULE_OBJ = $lhomework->GET_MODULE_OBJ_ARR();	

$TAGS_OBJ[] = array($Lang['SysMgr']['Homework']['ToDoList'], "index.php?yearID=$yearID&yearTermID=$yearTermID&classID=$classID", 1);
$TAGS_OBJ[] = array($Lang['SysMgr']['Homework']['History'], "history.php?yearID=$yearID&yearTermID=$yearTermID&classID=$classID", 0);

$linterface->LAYOUT_START();
?>

<script language="javascript">
<?
/*
$tempAry = "var tempAry = new Array();\n";

if(!$_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"]) {
	
	for($i=0; $i<sizeof($tempHW); $i++) {
		$tempAry .= "tempAry[$i] = \"".$tempHW[$i]."\";\n";
	}
}
echo $tempAry;
*/
?>

$(document).ready( function() {
	$('input#text').keydown( function(evt) {
		
		if (Check_Pressed_Enter(evt)) {
			// pressed enter
			goSearch();
		
		}
	});
});

function removeCat(obj,element,page){
	var alertConfirmRemove = "<?=$i_Discipline_System_alert_remove_record?>";
    if(countChecked(obj,element)==0)
            alert(globalAlertMsg2);
    else{
            if(confirm(alertConfirmRemove)){
            obj.action=page;
            obj.method="post";
            obj.submit();
            }
    }
}


function reloadForm() {
	document.form1.s.value = "";
	document.form1.pageNo.value = 1;
	document.form1.submit();
}


function goSearch() {
	document.form1.s.value = document.form2.text.value;
	document.form1.pageNo.value = 1;
	document.form1.submit();
}

function goSubmissionDetailsPage(parHwId, parFilter) {
	window.location = 'submission_list.php?HomeworkID=' + parHwId + '&submissionStatus=' + parFilter + '&fromPage=weeklyDiaryIndex';
}


function viewHmeworkDetail(id)
{
	newWindow('./view.php?hid='+id,1);
}


function viewHandinList(id)
{
	newWindow('./handin_list.php?hid='+id,10);
}

function doExport()
{
	if(document.getElementById("exportType1").checked) {
		self.location.href = "<?=$exportLink?>";
	} else {
		if(document.form2.exportDate.value=="") {
			alert("<?=$i_alert_pleasefillin." ".$Lang['eHomework']['ExportDate']?>");
			document.form2.exportDate.focus();
		} else if(!check_date(document.form2.exportDate, "<?=$Lang['General']['InvalidDateFormat']?>!")) { 
			return false;
		} else {
			document.form1.form1ExportDate.value = document.form2.exportDate.value;
			document.form1.action = "export_collection_list.php";
			document.form1.submit();
			document.form1.action = "";
		}
	}
}

function checkEditable(flag) {
	var success = 1;
	/*
	<?if(!$_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"]) {?>
	var hwLen = document.getElementById('HomeworkID[]').length;
	
	for(var i=0; i<hwLen; i++) {
		var thisValue = document.form1.HomeworkID[i].value;
		var thisChk = document.form1.HomeworkID[i].checked;
		
		if(thisChk && jQuery.inArray(thisValue, tempAry)==-1) {
			alert("");
			success = 0;
			return;
		}
	}
	<?}?>
	*/
	if(success) {
		if(flag=='edit')
			checkEdit(document.form1,'HomeworkID[]', 'edit.php');
		else
			removeCat(document.form1,'HomeworkID[]', 'remove_update.php?classID=<?=$classID?>');
	}
}
</script>

<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
			<form name="form2" method="post">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td>
							<table border="0" cellspacing="0" cellpadding="2" width="400">
								<tr>
									<td><?=$toolbar ?></td>
								</tr>
							</table>
						</td>
						<td align="right">
						
						<?=$htmlAry['searchBox']?>
						
					<!--	<input name="text" type="text" class="formtextbox" value="<?=stripslashes(stripslashes($s))?>"> 
						<?=$linterface->GET_BTN($button_find, "button","javascript:goSearch()");?> -->
						</td>
					</tr>
				</table>
			</form>
			<form name="form1" method="get" action="">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td align="right">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td height="28" align="right" valign="bottom">
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td align="right"><?= $linterface->GET_SYS_MSG($msg) ?>
											</td>
										</tr>
									</table>
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td height="30">
												<?=$filterbar?>
											</td>
										</tr>
									</table>
									<div style="clear:both" /></div>
									<div class="table_board">
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td align="right" valign="bottom">
												<? 
												//if ((!$lhomework->ViewOnly && ($_SESSION['UserType']==USERTYPE_STAFF || ($_SESSION['UserType']==USERTYPE_STUDENT && $_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"]))) || $_SESSION['SSV_PRIVILEGE']['homework']['is_subject_leader'] || ($_SESSION['UserType']==USERTYPE_STAFF && !$_SESSION['isTeaching'])) {
												if ($_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] 
													|| !$lhomework->ViewOnly 
													|| $_SESSION['SSV_PRIVILEGE']['homework']['is_subject_leader'] 
													|| ($_SESSION['UserType']==USERTYPE_STAFF && !$_SESSION['isTeaching'])
												) {
												?>
											
													<div class="common_table_tool"> <a title="Edit" class="tool_edit" href="javascript:void(0);" onclick="javascript:checkEditable('edit');" /><?=$button_edit?></a>
													<a title="<?=$button_delete?>" class="tool_delete" href="javascript:void(0);" onclick="javascript:checkEditable('delete')" /><?=$button_delete?></a>
													</div>
															
												<? 
												} else{?>
													<br/>
													<br/>
												<? } ?>
											</td>
										</tr>
									</table>
									</div>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>
						<?
						
						echo $li->display();
						
						/*
						if($display){
							echo $li->display();
						}
						else{
							if($_SESSION["SSV_PRIVILEGE"]["homework"]["is_subject_leader"])
								echo $Lang['SysMgr']['Homework']['HomeworkListWarningSubjectLeader'];
							else
								echo $Lang['SysMgr']['Homework']['HomeworkListWarning'];
						}
						*/
						?>
					</td>

				</tr>
			</table><br>
			<input type="hidden" name="pageNo" name="pageNo" value="<?=$li->pageNo; ?>"/>
			<input type="hidden" name="order" id="order" value="<?=$li->order; ?>"/>
			<input type="hidden" name="field" id="field" value="<?=$li->field; ?>"/>
			<input type="hidden" name="numPerPage" id="numPerPage" value="<?=$li->page_size?>"/>
			<input type="hidden" name="page_size_change" id="page_size_change" value=""/>
			<input type="hidden" name="s" id="s" value="<?=$s?>"/>
			<input type="hidden" name="cid" id="cid" value="<?=$classID?>"/>
			<input type="hidden" name="form1ExportDate" id="form1ExportDate" value=""/>
			<input type="hidden" name="form1ExportType" id="form1ExportType" value="<?=$exportType?>"/>
			
			</form>
		</td>
	</tr>
</table>

<?
	$linterface->LAYOUT_STOP();
	intranet_closedb();
?>