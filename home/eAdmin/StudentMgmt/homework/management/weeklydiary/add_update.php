<?php
// Using :

######## Change Log [Start] ################
#
#	Date:	2019-05-13 (Bill)
#           - prevent SQL Injection
#
######## Change Log [End] ################

$PATH_WRT_ROOT = "../../../../../../";
$top_menu_mode = 0;

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || (!$_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"]  && !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] && !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_subject_leader"] && ($_SESSION['isTeaching'] && $lhomework->ViewOnly)))
{
	if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])
	{
		header("location: ../../settings/index.php");	
		exit;
	}
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$lu = new libuser($UserID);
$lhomework = new libhomework2007();

$CurrentPageArr['eAdminHomework'] = 1;

if ($_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] || $lu->isTeacherStaff() || $_SESSION["SSV_PRIVILEGE"]["homework"]["is_subject_leader"])
{
    if ($_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] || $lhomework->nonteachingAllowed || $lu->teaching == 1 || $lhomework->subjectLeaderAllowed)
    {
        $haveAccess = true;
    }
}

if ($haveAccess)
{
    ### Handle SQL Injection + XSS [START]
    $classID = IntegerSafe($classID);
    $yearID = IntegerSafe($yearID);
    $yearTermID = IntegerSafe($yearTermID);
    ### Handle SQL Injection + XSS [END]
    
	# Check date
	# Start date >= curdate() && Start date <= due date
	$start_stamp = strtotime($startdate);
	$due_stamp = strtotime($duedate);
	$today = mktime(0,0,0,date("m"),date("d"),date("Y"));
	//$cond = ($lhomework->pastInputAllowed) ? compareDate($due_stamp,$start_stamp) < 0 : (compareDate($start_stamp,$today) < 0 || compareDate($due_stamp,$start_stamp)<0);
	
	$success = 1;
	if(!intranet_validateDate($startdate) || !intranet_validateDate($duedate))
	{
	    $success = 0;
	}
	else if ($lhomework->pastInputAllowed)
	{
		if (compareDate($due_stamp,$start_stamp)<0)
		{
			$success = 0;
		}
	}
	else
	{
		if (compareDate($start_stamp,$today) < 0 || compareDate($due_stamp,$start_stamp)<0)
		{
            $success = 0;
		}
	}
	
	if ($success == 1)
	{
		/*
		$sql = "SELECT SubjectID from SUBJECT_TERM WHERE SubjectGroupID = '$subjectGroupID[0]'";
		$result = $lhomework->returnArray($sql);
		$subjectID = $result[0]['SubjectID'];
		*/
		
		$title = intranet_htmlspecialchars($title);
		$description = intranet_htmlspecialchars(trim($description));
		
		$nameField = getNameFieldByLang("");
		$sql = "SELECT $nameField FROM INTRANET_USER WHERE UserID = '$UserID'";
		$result = $lhomework->returnVector($sql);
		$postername = $result[0];
		
		$today_str = date("Y-m-d", $today);
		$AcademicYearID = $yearID;
		$YearTermID = $yearTermID;
		
		if($type==2)
		{
                // by period
				$deadline_date = $startdate;
				while($deadline_date<=$duedate)
				{
					if($deadline_date==$startdate){
						$start_date = $startdate;
					}
					else{
						$start_date = date("Y-m-d",strtotime ("next day",strtotime($deadline_date))); 
					}
					$deadline_date=date("Y-m-d",strtotime ("next $chooseDeadline",strtotime($deadline_date))); 
					
					if($deadline_date<=$duedate&&$start_date<$deadline_date)
					{
						$fieldnames = "ClassID,ClassGroupID, SubjectID, PosterUserID, PosterName, StartDate, DueDate, Title, Description, Type, LastModified, AcademicYearID, YearTermID, CreatedBy";
						$fieldvalues = "'$classID', '', '', '$UserID', '$postername', '$start_date', '$deadline_date','$title', '$description', '1', now(), '$AcademicYearID', '$YearTermID', '$UserID'";
						
						$sql = "INSERT INTO INTRANET_HOMEWORK_WEEKLYDIARY_NEWSPAPERCUTTING ($fieldnames) VALUES ($fieldvalues)";
						$success = $lhomework->db_db_query($sql);
					}
				}
		}
		else
		{
				$fieldnames = "ClassID,ClassGroupID, SubjectID, PosterUserID, PosterName, StartDate, DueDate, Title, Description, Type, LastModified, AcademicYearID, YearTermID, CreatedBy";
				$fieldvalues = "'$classID', '', '', '$UserID', '$postername', '$startdate', '$duedate', '$title', '$description', '1', now(), '$AcademicYearID', '$YearTermID', '$UserID'";
				
				$sql = "INSERT INTO INTRANET_HOMEWORK_WEEKLYDIARY_NEWSPAPERCUTTING ($fieldnames) VALUES ($fieldvalues)";
				$success = $lhomework->db_db_query($sql);
		}
	}
    
	if ($success == 0)
	{
		$msg = "add_failed";
	}
	else
	{
		$msg = "add";
	}
    
	header ("Location: index.php?classID=$classID&msg=$msg");
}
else
{
    header ("Location: index.php");
}

intranet_closedb();
?>