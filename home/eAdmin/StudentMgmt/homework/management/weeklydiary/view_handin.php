<?php
// Modifing by : 

##### Change Log [Start] ######

###### Change Log [End] #######

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
include_once($PATH_WRT_ROOT."includes/libhomework_diary.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");

intranet_auth();
intranet_opendb();

$HomeworkID = integerSafe(standardizeFormPostValue($_POST['HomeworkID']));
$ArticleID = integerSafe(standardizeFormPostValue($_POST['ArticleID']));
$fromPage = standardizeFormPostValue($_POST['fromPage']);

$lhomework = new libhomework2007();
$lhomework_diary = new libhomework_diary();
$linterface = new interface_html();

# check access right
if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || (!$_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"]  && !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] && !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_subject_leader"] && !$lhomework->isViewerGroupMember($UserID)))
{
	if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])
	{
		header("location: ../../settings/index.php");
		exit;  
	}

	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}




# Start layout
$CurrentPageArr['eAdminHomework'] = 1;
if ($fromPage=='weeklyDiaryIndex' || $fromPage=='weeklyDiaryHistory') {
	$CurrentPage = "Management_WeeklyDiary";
	$PAGE_TITLE = $Lang['SysMgr']['Homework']['WeeklyDiary'];
	
	$TAGS_OBJ[] = array($Lang['SysMgr']['Homework']['ToDoList'], "index.php?yearID=$yearID&yearTermID=$yearTermID&classID=$classID", ($fromPage=='weeklyDiaryIndex'));
	$TAGS_OBJ[] = array($Lang['SysMgr']['Homework']['History'], "history.php?yearID=$yearID&yearTermID=$yearTermID&classID=$classID", ($fromPage=='weeklyDiaryHistory'));
}
else if ($fromPage=='newsCutIndex' || $fromPage=='newsCutHistory') {
	$CurrentPage = "Management_NewspaperCutting";
	$PAGE_TITLE = $Lang['SysMgr']['Homework']['NewspaperCutting'];
	
	$TAGS_OBJ[] = array($Lang['SysMgr']['Homework']['ToDoList'], "index.php?yearID=$yearID&yearTermID=$yearTermID&subjectID=$subjectID&subjectGroupID=$sgid&sid=$sid", ($fromPage=='newsCutIndex'));
	$TAGS_OBJ[] = array($Lang['SysMgr']['Homework']['History'], "history.php?yearID=$yearID&yearTermID=$yearTermID&subjectID=$subjectID&subjectGroupID=$sgid&sid=$sid", ($fromPage=='newsCutHistory'));
}
$MODULE_OBJ = $lhomework->GET_MODULE_OBJ_ARR();	
$linterface->LAYOUT_START();

$canEdit = false;
if ($_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] 
	|| !$lhomework->ViewOnly 
	|| $_SESSION['SSV_PRIVILEGE']['homework']['is_subject_leader'] 
	|| ($_SESSION['UserType']==USERTYPE_STAFF && !$_SESSION['isTeaching'])
) {
	$canEdit = true;
}


### navigation
// $navigationAry[] = array($displayLang, $onclickJs);
$navigationAry[] = array($Lang['SysMgr']['Homework']['WeeklyDiary'], 'javascript: goIndex();');
$navigationAry[] = array($Lang['SysMgr']['Homework']['StudentList'], 'javascript: goBack();');
$navigationAry[] = array($Lang['SysMgr']['Homework']['StudentHomework'],);
$htmlAry['navigation'] = $linterface->GET_NAVIGATION_IP25($navigationAry);


// get homework info
$hwInfoAry = $lhomework_diary->getDiaryData($HomeworkID);
$hwTitle = $hwInfoAry[0]['Title'];
$hwDesc = nl2br($hwInfoAry[0]['Description']);
$hwStartDate = $hwInfoAry[0]['StartDate'];
$hwDueDate = $hwInfoAry[0]['DueDate'];

// get student handin info
$handinAry = $lhomework_diary->getDiaryHandinData($HomeworkID, $ArticleID);
$studentId = $handinAry[0]['UserID'];
$handinTitle = $handinAry[0]['Title'];
$handinContent = $handinAry[0]['Content'];
$userObj = new libuser($studentId);
$studentClass = $userObj->ClassName; 
$studentClassNumber = $userObj->ClassNumber;
$studentName = Get_Lang_Selection($userObj->ChineseName, $userObj->EnglishName);
$studentNameDisplay = $studentName.' ('.$studentClass.' - '.$studentClassNumber.')'; 


$x = '';
$x .= '<table class="form_table_v30">'."\r\n";
	// title
	$x .= '<tr>'."\r\n";
		$x .= '<td class="field_title">'.$Lang['SysMgr']['Homework']['Topic'].'</td>'."\r\n";
		$x .= '<td>'."\r\n";
			$x .= $hwTitle;
		$x .= '</td>'."\r\n";
	$x .= '</tr>'."\r\n";
	
	// description
	$x .= '<tr>'."\r\n";
		$x .= '<td class="field_title">'.$Lang['SysMgr']['Homework']['Description'].'</td>'."\r\n";
		$x .= '<td>'."\r\n";
			$x .= $hwDesc;
		$x .= '</td>'."\r\n";
	$x .= '</tr>'."\r\n";
	
	// start and due date
	$x .= '<tr>'."\r\n";
		$x .= '<td class="field_title">'.$Lang['SysMgr']['CycleDay']['PeriodTitle'].'</td>'."\r\n";
		$x .= '<td>'."\r\n";
			$x .= $Lang['General']['From'].' '.$hwStartDate.' '.$Lang['General']['To'].' '.$hwDueDate;
		$x .= '</td>'."\r\n";
	$x .= '</tr>'."\r\n";
	
	// student
	$x .= '<tr>'."\r\n";
		$x .= '<td class="field_title">'.$Lang['Identity']['Student'].'</td>'."\r\n";
		$x .= '<td>'."\r\n";
			$x .= $studentNameDisplay;
		$x .= '</td>'."\r\n";
	$x .= '</tr>'."\r\n";
$x .= '</table>'."\r\n";
$htmlAry['hwInfoTbl'] = $x;


// student submission details
$attachmentAry = $lhomework_diary->getDiaryHandinAttachment($ArticleID);
$numOfAttachment = count($attachmentAry);
$htmlAry['studentSubmissionSection'] = $linterface->GET_NAVIGATION2_IP25($Lang['SysMgr']['Homework']['StudentSubmiitedContent']);
$x = '';
$x .= '<table class="form_table_v30">'."\r\n";
	// title
	$x .= '<tr>'."\r\n";
		$x .= '<td class="field_title">'.$Lang['SysMgr']['Homework']['Topic'].'</td>'."\r\n";
		$x .= '<td>'."\r\n";
			$x .= $handinTitle;
		$x .= '</td>'."\r\n";
	$x .= '</tr>'."\r\n";
	
	// content
	$x .= '<tr>'."\r\n";
		$x .= '<td class="field_title">'.$Lang['SysMgr']['Homework']['Content'].'</td>'."\r\n";
		$x .= '<td>'."\r\n";
			$x .= nl2br($handinContent);
		$x .= '</td>'."\r\n";
	$x .= '</tr>'."\r\n";
	
	// attachment
	if ($numOfAttachment > 0) {
		$x .= '<tr>'."\r\n";
			$x .= '<td class="field_title">'.$Lang['SysMgr']['Homework']['Attachment'].'</td>'."\r\n";
			$x .= '<td>'."\r\n";
				$x .= '<table class="common_table_list_v30 view_table_list_v30">'."\r\n";
					$x .= '<thead>'."\r\n";
						$x .= '<tr>'."\r\n";
							$x .= '<th style="width:30px;">#</th>'."\r\n";
							$x .= '<th>&nbsp;</th>'."\r\n";
						$x .= '</tr>'."\r\n";
					$x .= '</thead>'."\r\n";
					$x .= '<tbody>'."\r\n";
						for ($i=0; $i<$numOfAttachment; $i++) {
							$x .= '<tr>'."\r\n";
								$x .= '<td>'.($i+1).'</td>'."\r\n";
								$x .= '<td>'.$lhomework_diary->getDiaryAttachmentDisplay($attachmentAry[$i]).'</td>'."\r\n";
							$x .= '</tr>'."\r\n";
						}
					$x .= '</tbody>'."\r\n";
				$x .= '</table>'."\r\n";
			$x .= '</td>'."\r\n";
		$x .= '</tr>'."\r\n";
	}
$x .= '</table>'."\r\n";
$htmlAry['studentSubmissionTbl'] = $x;


// teacher feedback
$teacherScore = $handinAry[0]['TeacherScore'];
$teacherComment = $handinAry[0]['TeacherComment'];
$htmlAry['teacherFeedbackSection'] = $linterface->GET_NAVIGATION2_IP25($Lang['SysMgr']['Homework']['TeacherFeedback']);
$x = '';
$x .= '<table class="form_table_v30">'."\r\n";
	// grade or mark
	$x .= '<tr>'."\r\n";
		$x .= '<td class="field_title">'.$Lang['SysMgr']['Homework']['Mark/Grade'].'</td>'."\r\n";
		$x .= '<td>'."\r\n";
			$x .= $linterface->GET_TEXTBOX_NAME('teacherScoreTb', 'teacherScore', $teacherScore, $OtherClass='', $OtherPar=array('maxlength'=>64));
		$x .= '</td>'."\r\n";
	$x .= '</tr>'."\r\n";
	
	// comment
	$x .= '<tr>'."\r\n";
		$x .= '<td class="field_title">'.$Lang['Btn']['Comment'].'</td>'."\r\n";
		$x .= '<td>'."\r\n";
			$x .= $linterface->GET_TEXTAREA('teacherComment', $teacherComment);
		$x .= '</td>'."\r\n";
	$x .= '</tr>'."\r\n";
$x .= '</table>'."\r\n";
$htmlAry['teacherFeedbackTbl'] = $x;


### action buttons
$htmlAry['submitBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Submit'], "button", "goSubmit()", 'submitBtn', $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");
$htmlAry['cancelBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Cancel'], "button", "goBack()", 'cancelBtn', $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");

	
// hidden fields
$x = '';
$x .= $linterface->GET_HIDDEN_INPUT('HomeworkID', 'HomeworkID', $HomeworkID);
$x .= $linterface->GET_HIDDEN_INPUT('ArticleID', 'ArticleID', $ArticleID);
$x .= $linterface->GET_HIDDEN_INPUT('fromPage', 'fromPage', $fromPage);
$htmlAry['hiddenField'] = $x;
?>
<link href="/templates/jquery/urlive/jquery.urlive.css" rel="stylesheet" />
<script language="javascript" src="/templates/jquery/jquery-1.11.1.min.js"></script>
<script language="javascript" src="/templates/jquery/urlive/jquery.urlive.min.js"></script>

<script language="javascript">
$(document).ready( function() {
	$('a.attachmentLink').each( function() {
		var linkId = $(this).attr('id');
		var linkAry = linkId.split('_');
		var attachmentId = linkAry[1];
		 
    	//$('a#attachmentLink_' + attachmentId).urlive({container: 'div#urlLiveDiv_' + attachmentId, imageSize: 'small'});
	});
	
	
});

function goIndex() {
	window.location = 'index.php';
}

function goBack() {
	$('form#form1').attr('action', 'submission_list.php').submit();
}

function goSubmit() {
	var canSubmit = true;
	
	// disable action button to prevent double submission
	$('input.actionBtn').attr('disabled', 'disabled');
	
	if (canSubmit) {
		$('form#form1').attr('action', 'handin_update.php').submit();
	}
	else {
		// enable the action buttons if not allowed to submit
		$('input.actionBtn').attr('disabled', '');
	}
}
</script>
<form name="form1" id="form1" method="POST">
	<?=$htmlAry['navigation']?>
	<p class="spacer"></p>
	
	<div class="table_board">
		<?=$htmlAry['hwInfoTbl']?>
		<br style="clear:both;" />
		
		
		<?=$htmlAry['studentSubmissionSection']?>
		<?=$htmlAry['studentSubmissionTbl']?>
		<br style="clear:both;" />
		
		<?=$htmlAry['teacherFeedbackSection']?>
		<?=$htmlAry['teacherFeedbackTbl']?>
		<br style="clear:both;" />
		
		<div class="edit_bottom_v30">
			<p class="spacer"></p>
			<?=$htmlAry['submitBtn']?>
			<?=$htmlAry['cancelBtn']?>
			<p class="spacer"></p>
		</div>
	</div>
	
	<?=$htmlAry['hiddenField']?>
</form>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>