<?php
// Modifing by : 

########## Change Log
#
###########################

		
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$lhomework = new libhomework2007();

if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || (!$_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"]  && !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] && !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_subject_leader"] && ($_SESSION['isTeaching'] && $lhomework->ViewOnly)))
{
	if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])
	{
		header("location: ../../settings/index.php");	
		exit;
	}
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

# Temp Assign memory of this page
ini_set("memory_limit", "150M");

$linterface = new interface_html();

$lu = new libuser($UserID); 

$CurrentPageArr['eAdminHomework'] = 1;
$CurrentPage = "Management_WeeklyDiary";
$PAGE_TITLE = $Lang['SysMgr']['Homework']['WeeklyDiary'];

# tag information
$TAGS_OBJ[] = array($Lang['SysMgr']['Homework']['ToDoList'], "index.php?yearID=$yearID&yearTermID=$yearTermID&subjectID=$subjectID&subjectGroupID=$sgid&sid=$sid", 1);
//$TAGS_OBJ[] = array($Lang['SysMgr']['Homework']['History'], "history.php?yearID=$yearID&yearTermID=$yearTermID&subjectID=$subjectID&subjectGroupID=$sgid&sid=$sid", 0);

$MODULE_OBJ = $lhomework->GET_MODULE_OBJ_ARR();

$PAGE_NAVIGATION[] = array($Lang['SysMgr']['Homework']['WeeklyDiary'], "index.php?yearID=$yearID&yearTermID=$yearTermID&subjectID=$subjectID&subjectGroupID=$subjectGroupID");
$PAGE_NAVIGATION[] = array($Lang['SysMgr']['Homework']['NewRecord'], "");

# Start layout
$linterface->LAYOUT_START();
?>

<script language=javascript>

function reset_innerHtml()
{
	document.getElementById('div_subjectGroupID_error').innerHTML = "";
 	document.getElementById('div_topic_error').innerHTML = "";
 	document.getElementById('div_DateEnd_err_msg').innerHTML = "";
 	document.getElementById('div_DateStart_err_msg').innerHTML = "";
}
				
function checkform(obj)
{
	reset_innerHtml();
	var error_no=0;
	var focus_field='';
					
	/*
	if(obj.classID.value=="") {
		alert("<?=$i_alert_pleaseselect.$Lang['SysMgr']['Homework']['Class']?>");
		return false;
	}
	*/
	
	if (obj.classID.value=="") 
	{
		document.getElementById('div_subjectGroupID_error').innerHTML = '<font color="red"><?=$Lang['eHomework']['PleaseSelectClass']?></font>';
		error_no++;
		focus_field = "title";
	}
					
	if(obj.title.value=="") 
	{
		document.getElementById('div_topic_error').innerHTML = '<font color="red"><?=$Lang['eHomework']['MissingTitle']?></font>';
		error_no++;
		focus_field = "title";
	}
	
	<? if (!$lhomework->pastInputAllowed) {   ?>  
    	if (compareDate(obj.startdate.value, "<?=date("Y-m-d");?>") < 0) 
    	{
	    	document.getElementById('div_DateStart_err_msg').innerHTML = '<font color="red"><?=$i_Homework_new_startdate_wrong?></font>';
			error_no++;
			if(focus_field=="")	focus_field = "startdate";
	    }
    <? } ?>
    	
	if (compareDate(obj.startdate.value, obj.duedate.value) > 0) 
	{
		document.getElementById('div_DateEnd_err_msg').innerHTML = '<font color="red"><?=$i_Homework_new_duedate_wrong?></font>';
		error_no++;
		if(focus_field=="")	focus_field = "duedate";
	}
					 
    if(error_no>0)
	{
		eval("obj." + focus_field +".focus();");
		return false;
	}
	else
	{
		return Big5FileUploadHandler();
	}
}

function add_upload_field(rows_to_add){
	objTable = document.getElementById("file_list");
	attach_size = parseInt(document.addhomework.attachment_size.value);
	for(i=0;i<rows_to_add;i++){
		attach_size++;
		row = objTable.insertRow(-1);
		cell = row.insertCell(0);
		x = '<input type=file class=file name="filea'+attach_size+'" size=30>'; 
		x+= '<input type=hidden name="hidden_userfile_name'+attach_size+'">';
		cell.innerHTML = x;
	}
	document.addhomework.attachment_size.value = attach_size;
}

function Big5FileUploadHandler() {
	attach_size = parseInt(document.addhomework.attachment_size.value);
	
	for(i=0;i<=attach_size;i++){
		objFile = eval('document.addhomework.filea'+i);
		objUserFile = eval('document.addhomework.hidden_userfile_name'+i);
		if(objFile!=null && objFile.value!='' && objUserFile!=null){
			var Ary = objFile.value.split('\\');
			objUserFile.value = Ary[Ary.length-1];
		}
	}
	return true;
}

function back(yid, sid, sgid)
{
  window.location="index.php?yearID="+yid+"&subjectID="+sid+"&subjectGroupID="+sgid;        
}

function init()
{
	$('#div_content').load(
		'ajax_loadSubjectGroupSelection.php', 
		{
			yearID: <?=$yearID?>,
			yearTermID: <?=$yearTermID?>,
			classID: '<?=$classID?>',
			subjectID: '<?=$subjectID?>',
			subjectGroupID: '<?=$subjectGroupID?>'
			}, 
		function (data){
		}); 
}

function hide_choose_deadline(){
	$('#ChooseDeadline').hide();
}

function show_choose_deadline(){

	$('#ChooseDeadline').show();
}

</script>
<?php
        //if ($classID=="") $classID = 0;
        $today = date('Y-m-d');

        $timestamp =  time();
        $date_time_array =  getdate($timestamp);
        $hours =  $date_time_array[ "hours"];
        $minutes =  $date_time_array["minutes"];
        $seconds =  $date_time_array[ "seconds"];
        $month =  $date_time_array["mon"];
        $day =  $date_time_array["mday"] + 7;
        $year =  $date_time_array["year"];

        // use mktime to recreate the unix timestamp
        $timestamp =  mktime($hours, $minutes, $seconds ,$month, $day, $year);
		$pre_startdate = $today;
        $pre_duedate = date('Y-m-d',$timestamp);
		
		if($lhomework->startFixed)
		{
			$start = "<INPUT TYPE=\"text\" NAME=\"startdate\" SIZE=10 maxlength=10 value='$pre_startdate' readonly>";
		}
		else
		{
			$start = $linterface->GET_DATE_PICKER("startdate",$pre_startdate);
		}
		$default_upload_fields = 2;
?>

<?=$linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION)?>


<form name="addhomework" method="post" ACTION="add_update.php" enctype="multipart/form-data" onSubmit="return checkform(this)">


<div id="div_content">
	<table class="form_table_v30">
	<tr>
		<td><?=$Lang['General']['Loading']?></td>
	</tr>
	</table>
</div>

<table class="form_table_v30">
<tr>
   <td class="field_title"><span class="tabletextrequire">*</span> <?=$Lang['SysMgr']['Homework']['Topic']?></td>
	<td><input type="text" name="title" MAXLENGTH="140" class="textboxtext"><span id="div_topic_error"></span>
	</td>
</tr>

<tr>
	<td class="field_title"><?=$Lang['SysMgr']['Homework']['Description']?></td>
	<td><?=$linterface->GET_TEXTAREA("description","");?></td>
</tr>

<tr>
	<td class="field_title"><span class="tabletextrequire">*</span> <?=$Lang['SysMgr']['Homework']['StartDate']?> </td>
	<td><?=$start?><br><span id="div_DateStart_err_msg"></span></td>
</tr>

<tr>
	<td class="field_title"><span class="tabletextrequire">*</span> <?=$Lang['SysMgr']['Homework']["DueDate"]?> </td>
	<td><?=$linterface->GET_DATE_PICKER("duedate",$pre_duedate)?><br><span id="div_DateEnd_err_msg"></span></td>
</tr>

<tr>
	<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang['SysMgr']['Homework']['WeeklyDiaryType']?></td>
	<td>
	    <label><input type="radio" value="1" id="BookingType1" name="type" onclick="hide_choose_deadline()" checked="checked"><?=$Lang['SysMgr']['Homework']['Single']?> </label>
	    <label><input type="radio" value="2" id="BookingType2" name="type" onclick="show_choose_deadline()"><?=$Lang['SysMgr']['Homework']['Byweek']?></label>

	    <div id="ChooseDeadline" style="display:none">
	    <br />
	    <table class="common_table_list_v30">
	      <tr>
	        <td class="rights_not_select_sub" style="width:30%;"><span class="tabletextrequire">*</span><?=$Lang['SysMgr']['Homework']['Choosethedeadline']?></td>
	        <td>
		        <label><input type="radio" value="Sunday" id="WeekDay0" name="chooseDeadline" ><?=$Lang['SysMgr']['Homework']['Sun']?></label>
				<label><input type="radio" value="Monday" id="WeekDay1" name="chooseDeadline" checked="checked"><?=$Lang['SysMgr']['Homework']['Mon']?></label>
				<label><input type="radio" value="Tuesday" id="WeekDay2" name="chooseDeadline" ><?=$Lang['SysMgr']['Homework']['Tue']?></label>
				<label><input type="radio" value="Wednesday" id="WeekDay3" name="chooseDeadline" ><?=$Lang['SysMgr']['Homework']['Wed']?></label>
				<label><input type="radio" value="Thurday" id="WeekDay4" name="chooseDeadline" ><?=$Lang['SysMgr']['Homework']['Thur']?></label>
				<label><input type="radio" value="Friday" id="WeekDay5" name="chooseDeadline" ><?=$Lang['SysMgr']['Homework']['Fri']?></label>
				<label><input type="radio" value="Saturday" id="WeekDay6" name="chooseDeadline" ><?=$Lang['SysMgr']['Homework']['Sat']?></label>
	        </td>
	      </tr>
	    </table>
	    </div>
	</td>
</tr>
</table>

<?=$linterface->MandatoryField();?>

<div class="edit_bottom_v30">
<p class="spacer"></p>
<?= $linterface->GET_ACTION_BTN($button_submit, "submit")?>&nbsp;
<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "history.back()")?>
<p class="spacer"></p>
</div>

<input type="hidden" name="yearID" value="<?=$yearID?>"/>
<input type="hidden" name="yearTermID" value="<?=$yearTermID?>"/>
</form>

<script language="javascript">
<!--
init();
//-->
</script>

<?
	$linterface->LAYOUT_STOP();
	intranet_closedb();
?>
