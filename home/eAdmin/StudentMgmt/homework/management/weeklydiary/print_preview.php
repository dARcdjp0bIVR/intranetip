<?php
// Modifing by

/********************** Change Log ***********************
#
#************************************************************************
*/
		
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
// 2015-03-11 added
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");

intranet_auth();
intranet_opendb();

$lhomework = new libhomework2007();
// 2015-03-11 added
$scm = new subject_class_mapping();

if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || (!$_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"]  && !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] && !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_subject_leader"] && !$lhomework->isViewerGroupMember($UserID)))
{
	if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])
	{
		header("location: ../../settings/index.php");	
		exit;
	}
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}


$linterface = new interface_html();


$CurrentPageArr['eAdminHomework'] = 1;

$sql = "select HomeworkID,ClassID,StartDate,DueDate,Title,Description,LastModified,PosterName,PosterUserID,CreatedBy from INTRANET_HOMEWORK_WEEKLYDIARY_NEWSPAPERCUTTING where HomeworkID = '$hid'";
$record = $lhomework->returnArray($sql);

list($HomeworkID, $classID, $start, $due, $title, $description, $lastModified, $PosterName, $PostUserID, $CreatedBy)=$record[0]; 
$lu = new libuser($PostUserID);
$PosterName2 = $lu->UserName();

$lu2 = new libuser($CreatedBy);
$CreatedByName = $lu2->UserName();

$className = $lhomework->getClassName($classID);

$description = nl2br($description);
?>


<table width='100%' align='center' class='print_hide' border=0>
	<tr>
		<td align='right'><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></td>
	</tr>
</table>

<!-- Homework Title -->
<table width='100%' align="center" border="0">
	<tr>
        	<td align="center" ><b><?=$Lang['SysMgr']['Homework']['HomeworkList']?></b></td>
	</tr>
</table>        


<table width="50%" border="0" cellpadding="3" cellspacing="0" align="center">
	<tr><td></td><td></td></tr>
	<tr><td align="left" valign="top"><?=$Lang['SysMgr']['Homework']['Class']?> : </td><td align="left" valign="top"><?=$className?></td></tr>
	<tr><td align="left" valign="top"><?=$Lang['SysMgr']['Homework']['Topic']?> : </td><td align="left" valign="top"><?=$title?></td></tr>
	<tr><td align="left" valign="top"><?=$Lang['SysMgr']['Homework']['Description']?> : </td><td align="left" valign="top"><?=($description==""? "--": $description)?></td></tr>
	<tr><td align="left" valign="top"><?=$Lang['SysMgr']['Homework']['StartDate']?> : </td><td align="left" valign="top"><?=$start?></td></tr>
	<tr><td align="left" valign="top"><?=$Lang['SysMgr']['Homework']["DueDate"]?> : </td><td align="left" valign="top"><?=$due?></td></tr>
	<tr><td align="left" valign="top"><?=$Lang['General']['LastModifiedBy']?> : </td><td align="left" valign="top"><?=($PosterName2==""? "--":$PosterName2)?></td></tr>
	<tr><td align="left" valign="top"><?=$Lang['SysMgr']['Homework']['CreatedBy']?> : </td><td align="left" valign="top"><?=($CreatedBy ? $CreatedByName : $PosterName2)?></td></tr>
</table>

<?
include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_footer.php");
intranet_closedb();
?>