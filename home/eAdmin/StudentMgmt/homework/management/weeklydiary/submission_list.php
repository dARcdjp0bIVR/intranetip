<?php
// Modifing by : 

##### Change Log [Start] ######

###### Change Log [End] #######

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
include_once($PATH_WRT_ROOT."includes/libhomework_diary.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");

intranet_auth();
intranet_opendb();

$HomeworkID = isset($_POST['HomeworkID'])? standardizeFormPostValue($_POST['HomeworkID']) : standardizeFormGetValue($_GET['HomeworkID']);
$HomeworkID = integerSafe($HomeworkID);
$submissionStatus = isset($_POST['submissionStatus'])? standardizeFormPostValue($_POST['submissionStatus']) : standardizeFormGetValue($_GET['submissionStatus']);
$submissionStatus = integerSafe($submissionStatus);
$gradingStatus = isset($_POST['gradingStatus'])? standardizeFormPostValue($_POST['gradingStatus']) : standardizeFormGetValue($_GET['gradingStatus']);
$gradingStatus = integerSafe($gradingStatus);
$fromPage = isset($_POST['fromPage'])? standardizeFormPostValue($_POST['fromPage']) : standardizeFormGetValue($_GET['fromPage']);
$returnMsgKey = standardizeFormGetValue($_GET['returnMsgKey']);


### cookies handling to preserve selection
//$arrCookies[] = array("{$moduleName}_{$leftMenu}_{$pageName}_{$variableName}", "$variableName");
// e.g. $arrCookies[] = array("eEnrolment_Mgmt_clubList_field", "field");
$arrCookies = array();
$arrCookies[] = array("eHomework_weeklyDiary_submissionList_submissionStatus", "submissionStatus");
$arrCookies[] = array("eHomework_weeklyDiary_submissionList_gradingStatus", "gradingStatus");
$arrCookies[] = array("eHomework_weeklyDiary_submissionList_fromPage", "fromPage");
if(isset($clearCoo) && $clearCoo == 1) {
	clearCookies($arrCookies);
}
else {
	updateGetCookies($arrCookies);
}



$lhomework = new libhomework2007();
$lhomework_diary = new libhomework_diary();
$fcm = new form_class_manage();
$linterface = new interface_html();

# check access right
if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || (!$_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"]  && !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] && !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_subject_leader"] && !$lhomework->isViewerGroupMember($UserID)))
{
	if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])
	{
		header("location: ../../settings/index.php");
		exit;
	}

	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}




# Start layout
$CurrentPageArr['eAdminHomework'] = 1;

$hwInfoAry = $lhomework_diary->getDiaryData($HomeworkID);
$hwClassId = $hwInfoAry[0]['ClassID'];
$hwSubjectGroupId = $hwInfoAry[0]['ClassGroupID'];
if ($fromPage=='weeklyDiaryIndex' || $fromPage=='weeklyDiaryHistory') {
	$CurrentPage = "Management_WeeklyDiary";
	$PAGE_TITLE = $Lang['SysMgr']['Homework']['WeeklyDiary'];
	
	$TAGS_OBJ[] = array($Lang['SysMgr']['Homework']['ToDoList'], "index.php?yearID=$yearID&yearTermID=$yearTermID&classID=$classID", ($fromPage=='weeklyDiaryIndex'));
	$TAGS_OBJ[] = array($Lang['SysMgr']['Homework']['History'], "history.php?yearID=$yearID&yearTermID=$yearTermID&classID=$classID", ($fromPage=='weeklyDiaryHistory'));
	
	$studentAry = $fcm->Get_Student_By_Class($hwClassId);
}
else if ($fromPage=='newsCutIndex' || $fromPage=='newsCutHistory') {
	$CurrentPage = "Management_NewspaperCutting";
	$PAGE_TITLE = $Lang['SysMgr']['Homework']['NewspaperCutting'];
	
	$TAGS_OBJ[] = array($Lang['SysMgr']['Homework']['ToDoList'], "index.php?yearID=$yearID&yearTermID=$yearTermID&subjectID=$subjectID&subjectGroupID=$sgid&sid=$sid", ($fromPage=='newsCutIndex'));
	$TAGS_OBJ[] = array($Lang['SysMgr']['Homework']['History'], "history.php?yearID=$yearID&yearTermID=$yearTermID&subjectID=$subjectID&subjectGroupID=$sgid&sid=$sid", ($fromPage=='newsCutHistory'));
	
	$sgObj = new subject_term_class($hwSubjectGroupId, $GetTeacherList=false, $GetStudentList=true);
	$studentAry = $sgObj->ClassStudentList;
}
$MODULE_OBJ = $lhomework->GET_MODULE_OBJ_ARR();	
	

$returnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];
$linterface->LAYOUT_START($returnMsg);

$canEdit = false;
if ($_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] 
	|| !$lhomework->ViewOnly 
	|| $_SESSION['SSV_PRIVILEGE']['homework']['is_subject_leader'] 
	|| ($_SESSION['UserType']==USERTYPE_STAFF && !$_SESSION['isTeaching'])
) {
	$canEdit = true;
}


### navigation
// $navigationAry[] = array($displayLang, $onclickJs);
$navigationAry[] = array($Lang['SysMgr']['Homework']['WeeklyDiary'], 'javascript: goBack();');
$navigationAry[] = array($Lang['SysMgr']['Homework']['StudentList']);
$htmlAry['navigation'] = $linterface->GET_NAVIGATION_IP25($navigationAry);


### filtering
$htmlAry['submissionStatusSel'] = $lhomework_diary->getSubmissionStatusSelection('submissionStatusSel', 'submissionStatus', $submissionStatus, 'refreshPage();');
$htmlAry['gradingStatusSel'] = $lhomework_diary->getGradingStatusSelection('gradingStatusSel', 'gradingStatus', $gradingStatus, 'refreshPage();');


// get homework info
$hwTitle = $hwInfoAry[0]['Title'];
$hwDesc = nl2br($hwInfoAry[0]['Description']);
$hwStartDate = $hwInfoAry[0]['StartDate'];
$hwDueDate = $hwInfoAry[0]['DueDate'];
$x .= '<table class="form_table_v30">'."\r\n";
	// title
	$x .= '<tr>'."\r\n";
		$x .= '<td class="field_title">'.$Lang['SysMgr']['Homework']['Topic'].'</td>'."\r\n";
		$x .= '<td>'."\r\n";
			$x .= $hwTitle;
		$x .= '</td>'."\r\n";
	$x .= '</tr>'."\r\n";
	
	// description
	$x .= '<tr>'."\r\n";
		$x .= '<td class="field_title">'.$Lang['SysMgr']['Homework']['Description'].'</td>'."\r\n";
		$x .= '<td>'."\r\n";
			$x .= $hwDesc;
		$x .= '</td>'."\r\n";
	$x .= '</tr>'."\r\n";
	
	// start and due date
	$x .= '<tr>'."\r\n";
		$x .= '<td class="field_title">'.$Lang['SysMgr']['CycleDay']['PeriodTitle'].'</td>'."\r\n";
		$x .= '<td>'."\r\n";
			$x .= $Lang['General']['From'].' '.$hwStartDate.' '.$Lang['General']['To'].' '.$hwDueDate;
		$x .= '</td>'."\r\n";
	$x .= '</tr>'."\r\n";
$x .= '</table>'."\r\n";
$htmlAry['hwInfoTbl'] = $x;


// student list
$numOfStudent = count($studentAry);
$studentHandinAssoAry = BuildMultiKeyAssoc($lhomework_diary->getDiaryHandinData($HomeworkID), 'UserID');
$x = '';
$x .= '<table class="common_table_list">'."\r\n";
	$x .= '<thead>'."\r\n";
		$x .= '<tr>'."\r\n";
			$x .= '<th style="width:30px;">#</th>'."\r\n";
			$x .= '<th style="width:15%;">'.$Lang['SysMgr']['FormClassMapping']['ClassTitle'].'</th>'."\r\n";
			$x .= '<th style="width:10%;">'.$Lang['General']['ClassNumber'].'</th>'."\r\n";
			$x .= '<th style="width:20%;">'.$Lang['SysMgr']['Homework']['StudentName'].'</th>'."\r\n";
			$x .= '<th>'.$Lang['SysMgr']['Homework']['Title'].'</th>'."\r\n";
			$x .= '<th style="width:10%;">'.$Lang['SysMgr']['Homework']['HasGraded'].'</th>'."\r\n";
		$x .= '</tr>'."\r\n";
	$x .= '</thead>'."\r\n";
	$x .= '<tbody>'."\r\n";
		$studentCount = 0;
		for ($i=0; $i<$numOfStudent; $i++) {
			$_studentId = $studentAry[$i]['UserID'];
			$_classNameEn = $studentAry[$i]['ClassTitleEN'];
			$_classNameCh = $studentAry[$i]['ClassTitleB5'];
			$_classNumber = $studentAry[$i]['ClassNumber'];
			$_studentName = $studentAry[$i]['StudentName'];
			
			$_studentHandin = (isset($studentHandinAssoAry[$_studentId]))? true : false;
			if ($submissionStatus == 1 && !$_studentHandin) {
				// filter set to "submitted" but student has not submitted homework => ignore
				continue;
			}
			if ($submissionStatus == 2 && $_studentHandin) {
				// filter set to "not submitted" but student has submitted homework already => ignore
				continue;
			}
			
			$_className = Get_Lang_Selection($_classNameCh, $_classNameEn);
			
			$_handinAry = $studentHandinAssoAry[$_studentId];
			$_handinArticleId = $_handinAry['ArticleID'];
			$_handinTitle = $_handinAry['Title'];
			$_handinHasRead = $_handinAry['TeacherHasRead'];
			
			if ($gradingStatus == 1 && !$_handinHasRead) {
				// filter set to "submitted" but student has not submitted homework => ignore
				continue;
			}
			if ($gradingStatus == 2 && $_handinHasRead) {
				// filter set to "not submitted" but student has submitted homework already => ignore
				continue;
			}
			
			// title display
			if ($_handinArticleId > 0) {
				$_handinTitle = ($_handinTitle=='')? $Lang['General']['EmptySymbol'] : $_handinTitle;
				$_handinTitleDisplay = '<a class="tablelink" href="javascript:void(0);" onclick="viewHandin(\''.$_handinArticleId.'\');">'.$_handinTitle.'</a>';
			}
			 else {
			 	$_handinTitleDisplay = $Lang['General']['EmptySymbol'];
			 }
			 
			 // commented
			 $_handinHasReadDisplay = ($_handinHasRead)? $Lang['General']['Yes'] : $Lang['General']['No']; 
			 
			
			$x .= '<tr>'."\r\n";
				$x .= '<td>'.(++$studentCount).'</td>'."\r\n";
				$x .= '<td>'.$_className.'</td>'."\r\n";
				$x .= '<td>'.$_classNumber.'</td>'."\r\n";
				$x .= '<td>'.$_studentName.'</td>'."\r\n";
				$x .= '<td>'.$_handinTitleDisplay.'</td>'."\r\n";
				$x .= '<td>'.$_handinHasReadDisplay.'</td>'."\r\n";
				
			$x .= '</tr>'."\r\n";
		}
	$x .= '</tbody>'."\r\n";
$x .= '</table>'."\r\n";
$htmlAry['studentTbl'] = $x;


### buttons
$htmlAry['backBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Back'], "button", "goBack()", 'backBtn', $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");


### hidden fields
$x = '';
$x .= $linterface->GET_HIDDEN_INPUT('HomeworkID', 'HomeworkID', $HomeworkID);
$x .= $linterface->GET_HIDDEN_INPUT('ArticleID', 'ArticleID', '');
$x .= $linterface->GET_HIDDEN_INPUT('fromPage', 'fromPage', $fromPage);
$htmlAry['hiddenField'] = $x;
?>
<script language="javascript">
$(document).ready( function() {

});

function refreshPage() {
	$('form#form1').attr('target', '_self').attr('action', 'submission_list.php').submit();
}

function viewHandin(parArticleId) {
	$('input#ArticleID').val(parArticleId);
	$('form#form1').attr('target', '_self').attr('action', 'view_handin.php').submit();
}

function goBack() {
	var fromPage = $('input#fromPage').val();
	
	var targetScript = '';
	if (fromPage == 'weeklyDiaryIndex') {
		targetScript = 'index.php';
	}
	else if (fromPage == 'weeklyDiaryHistory') {
		targetScript = 'history.php';
	}
	else if (fromPage == 'newsCutIndex') {
		targetScript = '../newspapercutting/index.php';
	}
	else if (fromPage == 'newsCutHistory') {
		targetScript = '../newspapercutting/history.php';
	}
	
	window.location = targetScript;
}
</script>
<form name="form1" id="form1" method="POST" action="submission_list.php">
	<?=$htmlAry['navigation']?>
	<p class="spacer"></p>
	
	<div class="table_board">
		<?=$htmlAry['hwInfoTbl']?>
		<br style="clear:both;" />
		
		<div class="table_filter">
			<?=$htmlAry['submissionStatusSel']?>
			<?=$htmlAry['gradingStatusSel']?>
		</div>
		<p class="spacer"></p>
		
		<?=$htmlAry['studentTbl']?>
		<br style="clear:both;" />
		
		<div class="edit_bottom_v30">
			<p class="spacer"></p>
			<?=$htmlAry['backBtn']?>
			<p class="spacer"></p>
		</div>
	</div>
	
	<?=$htmlAry['hiddenField']?>
</form>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>