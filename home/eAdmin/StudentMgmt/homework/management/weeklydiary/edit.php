<?php
// Modifying by: 

########## Change Log
#
###########################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();
 
$lhomework = new libhomework2007();
 
if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || (!$_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"]  && !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] && !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_subject_leader"] && !$lhomework->isViewerGroupMember($UserID)))
{
	if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])
	{
		header("location: ../../settings/index.php");	
		exit;
	}
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

# Temp Assign memory of this page
ini_set("memory_limit", "150M");


$linterface = new interface_html();

$CurrentPageArr['eAdminHomework'] = 1;

# menu highlight setting
$CurrentPage = "Management_WeeklyDiary";

$PAGE_TITLE = $Lang['SysMgr']['Homework']['WeeklyDiary'];

if($history) {
	$highlight1 = 0;
	$highlight2 = 1;
	$url = "history.php";
} else {
	$highlight1 = 1;
	$highlight2 = 0;	
	$url = "index.php";
}

# tag information
$TAGS_OBJ[] = array($Lang['SysMgr']['Homework']['ToDoList'], "index.php?yearID=$yearID&yearTermID=$yearTermID&classID=$classID", $highlight1);
$TAGS_OBJ[] = array($Lang['SysMgr']['Homework']['History'], "history.php?yearID=$yearID&yearTermID=$yearTermID&classID=$classID", $highlight2);

$MODULE_OBJ = $lhomework->GET_MODULE_OBJ_ARR();

# step information
$STEPS_OBJ[] = array($i_general_select_csv_file, 1);
$STEPS_OBJ[] = array('Confirmation', 0);
$STEPS_OBJ[] = array($i_general_imported_result, 0);

# navigation bar
$PAGE_NAVIGATION[] = array($Lang['SysMgr']['Homework']['WeeklyDiary'], "$url?classID=$classID");
$PAGE_NAVIGATION[] = array($Lang['SysMgr']['Homework']['EditRecord'], "");

if(sizeof($HomeworkID)>0)
{
	$HomeworkID = (is_array($HomeworkID)) ? implode(",",$HomeworkID) : $HomeworkID;
}else{
	$HomeworkID = $HomeworkID;
}
$sql = "select HomeworkID,ClassID,StartDate,DueDate,Title,Description,LastModified,PosterName,PosterUserID,CreatedBy from INTRANET_HOMEWORK_WEEKLYDIARY_NEWSPAPERCUTTING where HomeworkID = '$HomeworkID'";
$record = $lhomework->returnArray($sql);
list($HomeworkID, $classID, $start, $due, $title, $description, $lastModified, $PosterName, $posterUserID, $createdBy)=$record[0]; 

# check current user is teaching this subject group / class teacher / subject leader
if(!$_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] && !($_SESSION['UserType']==USERTYPE_STAFF && !$_SESSION['isTeaching'])) {
	// [2015-1015-1207-12071] replaced by access right checking below
	// [2015-1008-1632-45222] not allow access only if user not poster nor creator
	//if($lhomework->ClassTeacherCanViewHomeworkOnly && ($UserID!=$posterUserID || $UserID !=$createdBy)) {
//	if($lhomework->ClassTeacherCanViewHomeworkOnly && $UserID!=$posterUserID && $UserID !=$createdBy) {
//		include_once($PATH_WRT_ROOT."includes/libaccessright.php");
//		$laccessright = new libaccessright();
//		$laccessright->NO_ACCESS_RIGHT_REDIRECT();
//		exit;
//	}
	
	# for checking on subject group teacher
	$sql = "SELECT stct.UserID FROM SUBJECT_TERM_CLASS_TEACHER stct WHERE SubjectGroupID=$subjectGroupID";
	$result1 = $lhomework->returnVector($sql);
	
	# class teacher only can VIEW homework of self-class, therefore cannot edit it if he/she is not the subject group teacher
	// [2015-1012-1659-42207] Checking should apply to staff only
	//if($lhomework->ClassTeacherCanViewHomeworkOnly && !in_array($UserID, $result1)) {
	// [2015-1015-1207-12071] replaced by access right checking below
//	if($_SESSION['UserType']==USERTYPE_STAFF && $_SESSION['isTeaching'] && $lhomework->ClassTeacherCanViewHomeworkOnly && !in_array($UserID, $result1)) {
//		include_once($PATH_WRT_ROOT."includes/libaccessright.php");
//		$laccessright = new libaccessright();
//		$laccessright->NO_ACCESS_RIGHT_REDIRECT();
//		exit;
//	}
	
	# for checking on class teacher
	$sql = "SELECT yct.UserID FROM YEAR_CLASS_TEACHER yct LEFT OUTER JOIN YEAR_CLASS_USER ycu ON (ycu.YearClassID=yct.YearClassID) LEFT OUTER JOIN SUBJECT_TERM_CLASS_USER stcu ON (stcu.UserID=ycu.UserID) LEFT OUTER JOIN SUBJECT_TERM st ON (st.SubjectGroupID=stcu.SubjectGroupID) WHERE st.YearTermID='".getCurrentSemesterID()."' AND st.SubjectGroupID='$subjectGroupID'";
	$result2 = $lhomework->returnVector($sql);

	/* 
	 * [2015-1015-1207-12071] Access Right Checking on Teacher and Subject Leader
	 * - Cond 1
	 * 		a. Not allow class teacher to manage homework of his/her class - True
	 * 		b. Class Teacher
	 * 		c. Not Subject Group Teacher
	 * 			OR
	 * - Cond 2
	 * 		a. Teacher/Subject Leader can only edit/delete self-created homework - True
	 * 		b. Not Poster
	 * 		c. Not Creater
	 */
	if(($lhomework->ClassTeacherCanViewHomeworkOnly && in_array($UserID, $result2) && !in_array($UserID, $result1)) || 
			($lhomework->OnlyCanEditDeleteOwn && $UserID!=$createdBy && $UserID!=$posterUserID)) {
		include_once($PATH_WRT_ROOT."includes/libaccessright.php");
		$laccessright = new libaccessright();
		$laccessright->NO_ACCESS_RIGHT_REDIRECT();
		exit;
	}
	
	$result = (sizeof($result1)!=0 && sizeof($result2)!=0) ? array_merge($result1, $result2) : ((sizeof($result1)==0 && sizeof($result2)==0) ? array() : ((sizeof($result1)==0) ? $result2 : $result1));
	$result = array_unique($result);
	
	# for checking on subject leader
	$sql = "SELECT COUNT(*) FROM INTRANET_SUBJECT_LEADER WHERE ClassID='".$subjectGroupID."' AND UserID='$UserID'";
	$subjectLeader = $lhomework->returnVector($sql);

	if((sizeof($result)==0 || !in_array($UserID, $result)) && !$subjectLeader[0]) {
		include_once($PATH_WRT_ROOT."includes/libaccessright.php");
		$laccessright = new libaccessright();
		$laccessright->NO_ACCESS_RIGHT_REDIRECT();
		exit;		
	}
}


$className = $lhomework->getClassName($classID);

if($AttachmentPath!=""){
	$displayAttachment = $lhomework->displayAttachmentEdit($HomeworkID,"file2delete[]");
}

# default upload field
//$default_upload_fields = 1;

$linterface->LAYOUT_START();
?>

<script language="javascript">

function reset_innerHtml()
{
 	document.getElementById('div_topic_error').innerHTML = "";
 	document.getElementById('div_DateEnd_err_msg').innerHTML = "";
 	document.getElementById('div_DateStart_err_msg').innerHTML = "";
}

function checkform(obj)
{
	reset_innerHtml();
	var error_no=0;
	var focus_field='';
	
	if(obj.title.value=="") 
	{
		document.getElementById('div_topic_error').innerHTML = '<font color="red"><?=$Lang['eHomework']['MissingTitle']?></font>';
		error_no++;
		focus_field = "title";
	}
	
	<? if (!$lhomework->pastInputAllowed) {   ?>  
    	if (compareDate(obj.startdate.value, obj.orginal_startdate.value) < 0) 
    	{
	    	document.getElementById('div_DateStart_err_msg').innerHTML = '<font color="red"><?=str_replace("%%s%%",$start,$Lang['eHomework']['StartDateWarningMsg'])?></font>';
			error_no++;
			if(focus_field=="")	focus_field = "startdate";
	    }
    <? } ?>
    	
	if (compareDate(obj.startdate.value, obj.duedate.value) > 0) 
	{
		document.getElementById('div_DateEnd_err_msg').innerHTML = '<font color="red"><?=$i_Homework_new_duedate_wrong?></font>';
		error_no++;
		if(focus_field=="")	focus_field = "duedate";
	}
		
	/*	
	if (obj.TypeID!=null)
	{
		if (!check_select(obj.TypeID,"<?=$i_Homework_new_homework_type_missing?>",'')) return false;
	}
	*/
			
    if(error_no>0)
	{
		eval("obj." + focus_field +".focus();");
		return false;
	}
	else
	{
		Big5FileUploadHandler();
		
		// create a list of files to be deleted
		objDelFiles = document.getElementsByName('file2delete[]');
		files = obj.deleted_files;
		if(objDelFiles==null)return true;
		x="";
		for(i=0;i<objDelFiles.length;i++){
				if(objDelFiles[i].checked==false){
					x+=objDelFiles[i].value+":";       
				}
		}
		files.value = x.substr(0,x.length-1);
		
		return true;
	}
}

function add_upload_field(rows_to_add){
	objTable = document.getElementById("file_list");
	attach_size = parseInt(document.form1.attachment_size.value);
	for(i=0;i<rows_to_add;i++){
		attach_size++;
		row = objTable.insertRow(-1);
		cell = row.insertCell(0);
		x = '<input type=file class=file name="filea'+attach_size+'" size=30>'; 
		x+= '<input type=hidden name="hidden_userfile_name'+attach_size+'">';
		cell.innerHTML = x;
	}
	document.form1.attachment_size.value = attach_size;
}

function Big5FileUploadHandler() {
	attach_size = parseInt(document.form1.attachment_size.value);
	for(i=0;i<=attach_size;i++){
		objFile = eval('document.form1.filea'+i);
		objUserFile = eval('document.form1.hidden_userfile_name'+i);
		if(objFile!=null && objFile.value!='' && objUserFile!=null){
			var Ary = objFile.value.split('\\');
			objUserFile.value = Ary[Ary.length-1];
		}
	}
	return true;
}

function back(cid)
{
  window.location="<?=$url?>?classID="+cid;        
}

function alertMsg(obj){
	val = obj.checked;
	if(!val){
		if(confirm("<?=$i_Homework_handin_records_will_be_deleted?>"+"\n"+"<?=$i_Homework_handin_continue?>?")){
			obj.checked=val;
		}else{
			obj.checked = !val;
		}
	}
}

</script>

<?=$linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION)?>

<form name="form1" method="POST" action="edit_update.php" enctype="multipart/form-data" onSubmit="return checkform(this)">
<table class="form_table_v30">
		<!-- class -->
		<tr>
			<td class="field_title" valign="top"><?=$Lang['SysMgr']['Homework']['Class']?></td>
			<td>
				<?=$className?>
			</td>
		</tr>
	
		<!-- topic -->
		<tr>
			<td class="field_title" valign="top"><span class="tabletextrequire">*</span> <?=$Lang['SysMgr']['Homework']['Topic']?></td>
			<td>
			  <input type="text" name="title" maxlength=140 value="<?=$title?>" class="textboxtext"/><span id="div_topic_error"></span>
			</td>
		</tr>
		
		<!-- description -->
		<tr>
			<td class="field_title" valign="top"><?=$Lang['SysMgr']['Homework']['Description']?></td>
			<td><?=$linterface->GET_TEXTAREA("description",$description);?></td>
		</tr>
		  
		<tr>
			<td class="field_title" valign="top"><span class="tabletextrequire">*</span> <?=$Lang['SysMgr']['Homework']['StartDate']?></td>
			<td>
				<!--<input type="text" name="startdate" size="10" maxlength="10" value=<?=$start?>><?=$linterface->GET_CALENDAR("form1", "startdate")?>-->
				<?=$linterface->GET_DATE_PICKER("startdate",$start)?><br><span id="div_DateStart_err_msg"></span>
			</td>
		</tr>

		<tr>
			<td class="field_title" valign="top"><span class="tabletextrequire">*</span> <?=$Lang['SysMgr']['Homework']["DueDate"]?></td>
			<td>
			  <!--<input type="text" name="duedate" size="10" maxlength="10" value=<?=$due?>><?=$linterface->GET_CALENDAR("form1", "duedate")?>-->
			  <?=$linterface->GET_DATE_PICKER("duedate",$due)?><br><span id="div_DateEnd_err_msg"></span>
			</td>
		</tr>
	</table>
	
<?=$linterface->MandatoryField();?>

<div class="edit_bottom_v30">
<p class="spacer"></p>
<?= $linterface->GET_ACTION_BTN($button_save, "submit")?>&nbsp;
<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:back($classID)")?>
<p class="spacer"></p>
</div>	
	
	<input type="hidden" name="HomeworkID" value="<?=$HomeworkID?>">
	<input type="hidden" name="classID" value="<?=$classID?>">
	<input type="hidden" name="deleted_files" value=""> 
	<input type="hidden" name="orginal_startdate" value="<?=$start?>"> 
</form>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>