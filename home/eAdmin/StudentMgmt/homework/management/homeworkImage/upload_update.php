<?php
// Editing by 
/*
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

intranet_auth();
intranet_opendb();

$lhomework = new libhomework2007();
$lf = new libfilesystem();
$linterface = new interface_html();

$uploaded_img_path = array();
	
$tmp_folder = $intranet_root.'/file/homeworkImage/tmp/'.$UserID;
$file_arr = $lf->return_folderlist($tmp_folder); 

# check for zip file then unzip it first.
for($i=0;$i<sizeof($file_arr);$i++)
{
	$tmp_arr = explode("/",$file_arr[$i]);
	
	$file = $tmp_arr[sizeof($tmp_arr)-1];
	
	$tmp_file_name_arr = explode(".",$file);
	$array_size = count($tmp_file_name_arr);
	$extension = $tmp_file_name_arr[$array_size-1];
	$upload_file_name =  rtrim($file,".".$extension);
		
	if(strtoupper($extension)=='ZIP'){
		$lf->file_unzip($tmp_folder.'/'.$file, $tmp_folder);
		$lf->lfs_remove($tmp_folder.'/'.$file);
		
		# get the unzipped images
		$file_arr = $lf->return_folderlist($tmp_folder);
	}
	
}	


for($i=0;$i<sizeof($file_arr);$i++)
{
	$tmp_arr = explode("/",$file_arr[$i]);
	
	$file = $tmp_arr[sizeof($tmp_arr)-1];

	$tmp_file_name_arr = explode(".",$file);
	$array_size = count($tmp_file_name_arr);
	$extension = $tmp_file_name_arr[$array_size-1];
	$upload_file_name =  rtrim($file,".".$extension);

	if(strtoupper($extension)!='ZIP'){	
		$handled_obj = checkImageName($tmp_folder.'/'.$file,$upload_file_name,$extension,$date);
		$uploaded_arr[] = $handled_obj;
	}
}

# sort the list, error go first
$error_arr		= array();
$success_arr	= array();
for($a=0;$a<sizeof($uploaded_arr);$a++){
	if(isset($uploaded_arr[$a]['error_msg'])){
		$error_arr[] = $uploaded_arr[$a]; 
	}
	else{
		$success_arr[] = $uploaded_arr[$a];
	}
		
}

function checkImageName($tmp_file_path,$upload_file_name,$extension,$date)
{
	global $lhomework,$intranet_root,$eclass_db,$lf,$UserID,$i_Homework_Error_NoClass;
		
	$sql = "SELECT course_id, course_name FROM $eclass_db.course WHERE roomType=0 ";
			$sql = 'Select
							yc.YearClassID, 
							yc.ClassTitleEN,
							yc.ClassTitleB5
						From
							YEAR_CLASS as yc
						Where 
							yc.AcademicYearID = '.Get_Current_Academic_Year_ID().'
						Order by 
							yc.Sequence, yc.ClassTitleEN
					 ';
	$course_obj = $lhomework->returnArray($sql);

	for($i=0;$i<sizeof($course_obj);$i++){
		$en_course_arr[$course_obj[$i]['ClassTitleEN']] = $course_obj[$i];
		$b5_course_arr[$course_obj[$i]['ClassTitleB5']] = $course_obj[$i];
	}

	$error_msg = '';
	if($en_course_arr[$upload_file_name]!=''){
		$class_obj = $en_course_arr[$upload_file_name];
		$class_name = $upload_file_name;
	}
	elseif($b5_course_arr[$upload_file_name]!=''){
		$class_obj = $b5_course_arr[$upload_file_name];
		$class_name = $upload_file_name;
	}
	else
	{
		$error_msg = $upload_file_name;
		$course_id = "";
	}

	$course_id = $class_obj['YearClassID'];
	if($course_id!=""){
		
		    //resize if need
			$filepath = $tmp_file_path;
			
	        $filetype = strtolower(pathinfo($filepath, PATHINFO_EXTENSION));
	        
	        if($filetype=="jpg"||$filetype=="jpeg"||$filetype=="png"){
	        	
	        	list($width, $height) = getimagesize($filepath);
	      	 
	        	$needResize=false;
	        	 
	        	if($width>1500&&$height<=1500){
	        		$needResize=true;
	        		$new_width = 1500;
	        		$new_height = 1500*$height/$width;
	        	}
	        	else if($width<=1500&&$height>1500){
	        		$needResize=true;
	        		$new_height = 1500;
	        		$new_width = 1500*$width/$height;
	        	}
	        	else if($width>1500&&$height>1500){
	        		$needResize=true;
	        		if($width>=$height){
	        			$new_width = 1500;
	        			$new_height = 1500*$height/$width;
	        		}else{
	        			$new_height = 1500;
	        			$new_width = 1500*$width/$height;
	        		}
	        	}
	
	        	if($needResize){
	        		if($filetype=="jpg"||$filetype=="jpeg"){
	        			 
	        			// Resample
	        			$image_p = imagecreatetruecolor($new_width, $new_height);
	        			$image = imagecreatefromjpeg($filepath);
	        			imagecopyresampled($image_p, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
	        			// Output
	        			imagejpeg($image_p, $filepath);
	        			imagedestroy($image_p);
	        		}
	        		if($filetype=="png"){
	        			 
	        			// Resample
	        			$image_p = imagecreatetruecolor($new_width, $new_height);
	        			$image = imagecreatefrompng($filepath);
	        			imagecopyresampled($image_p, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
	        			// Output
	        			imagepng($image_p, $filepath);
	        			imagedestroy($image_p);
	        			 
	        		}
	        		list($width, $height) = getimagesize($filepath);
	        	}
	        	
	        }
	}
	
	if($error_msg!=''){
		$uploaded_arr = array("error_msg"=>$error_msg,"image_path"=>str_replace($intranet_root.'/file/','',$tmp_file_path));
	}
	else				
		$uploaded_arr = array($class_name,str_replace($intranet_root.'/file/','',$tmp_file_path),$course_id);

	return $uploaded_arr; 
}

$submitBtn = $linterface->GET_ACTION_BTN($button_submit, "submit", "submitHWList()", "submitBtn");
$backBtn = $linterface->GET_ACTION_BTN($button_back, "button", "window.location.href='index.php'", "backBtn");

# navigation
$PAGE_NAVIGATION[] = array($Lang['SysMgr']['Homework']['HomeworkImage'], "index.php");
$PAGE_NAVIGATION[] = array($button_upload, "");
$PageNavigation = $linterface->GET_NAVIGATION($PAGE_NAVIGATION);

$CurrentPageArr['eAdminHomework'] = 1;
$CurrentPage = "Management_HomeworkImage";

$TAGS_OBJ[] = array($Lang['SysMgr']['Homework']['HomeworkImage']);
$MODULE_OBJ = $lhomework->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

?>
<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td>
						<table border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td><?=$PageNavigation ?></td>
							</tr>
						</table>
					</td>
					<td align="right"></td>
				</tr>
			</table>
			<br>
            <form name="form1" action="upload_finish.php" method="post" enctype="multipart/form-data">
                <tr> 
                  <td valign="top" height="100%"> 
                        <div class="upload_result_board">
                    	 <?php if(sizeof($error_arr)>0){?>
                         <h2 class="result_error_title"><?=$Lang['homework_list_image_unverified']?> (<?=sizeof($error_arr)?>)</h2>
                       
                        <div class="result_error">&nbsp;
                         <?php for($i=0; $i<sizeof($error_arr); $i++) {
                        	$disable_next_step = true;
			
							$uploaded_image		= '/file/'.$error_arr[$i]['image_path'];
							$image_width		= "400";
							$error_msg 		  = $error_arr[$i]['error_msg'];
                        	
                        	?>
                        <div style="padding-left: 50px;"><img src="<?=$uploaded_image?>" width="<?=$image_width?>"><br>
                          <p style="padding-left: 170px;"><?=$error_msg?></p></div>
                           <?}?>                                  
                        <br style="clear:both">&nbsp;
                        </div>
                        <?}?>
                        
                        <br style="clear:both">
                        <?php if(sizeof($success_arr)>0){?>
                         <h2 class="result_ok_title"><?=$Lang['homework_list_image_verified']?> (<?=sizeof($success_arr)?>)</h2>
                        <div class="result_ok">&nbsp;
                     	<?php for($i=0; $i<sizeof($success_arr); $i++) { 
		
							$uploaded_image		= '/file/'.$success_arr[$i][1];
							$image_width		= "400px";
							$class_name 		= $success_arr[$i][0];?>
                            <div style="padding-left: 50px;"><img src="<?=$uploaded_image?>" width="<?=$image_width?>" border="0"/><br>
                              <p style="padding-left: 170px;"><?=$class_name?></p><input name=uploaded_img[] type=hidden value="<?=$success_arr[$i][1]?>">
                              <input name=courseID[] type=hidden value="<?=$success_arr[$i][2]?>">	 </div>                               
                          <?}?>
                          <br style="clear:both">&nbsp;
                        </div>
                        <?}?>
                    </div>                                
                  </td>
                </tr>
	<tr>
		<td colspan="2" class="dotline">
			<img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1">
		</td>
	</tr>
	<tr>
		<td colspan="2" align="center">
	        <div align="center">
			    <?if($disable_next_step==false){?>
				<?=$submitBtn?>
				<?}?>
				<img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="1" height="1">
				<?= $backBtn ?>
	        </div>
        </td>
	</tr>
</table>
<input name="import_method" id="import_method" type=hidden value="">
<input name="remove_img" id="remove_img" type=hidden value="">
<input name="date" id="date" type=hidden value="<?=$date?>">            
</form>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT?>templates/jquery/thickbox.js"></script>
<link rel="stylesheet" type="text/css" href="<?=$PATH_WRT_ROOT?>templates/jquery/thickbox.css" media="screen" />
<script>
function backToIndex(){
	document.form1.action="index.php"
	$('#remove_img').val(1);
	$('#import_method').val(3);
	document.form1.submit();
}

function submitHWList(){
	
if(confirm('<?=$Lang['homework_import']['hw_list_submit_confirm']?>'))
	document.form1.submit();
}
</script>
<?		
		
$linterface->LAYOUT_STOP();		
?>