<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libwordtemplates.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$lh = new libhomework();
$lf = new libfilesystem();

if (!$sys_custom['eHomework']['HomeworkImage'])
{
	header("Location: /home/");
}

$pluploadPar['targetDir'] = $intranet_root.'/file/homeworkImage/tmp/'.$UserID;
if(is_dir($pluploadPar['targetDir'])==false)
	$lf->createFolder($pluploadPar['targetDir']);
	
include_once($PATH_WRT_ROOT."includes/plupload.php");

intranet_closedb();
?>