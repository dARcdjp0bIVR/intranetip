<?php
// Modifing by : 

##### Change Log [Start] ######

###### Change Log [End] #######

$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

intranet_auth();
intranet_opendb();

$lhomework = new libhomework2007();
$fcm = new form_class_manage();
$lf = new libfilesystem();

# check access right
if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || (!$_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"]  && !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] && !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_subject_leader"] && !$lhomework->isViewerGroupMember($UserID)))
{
	if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])
	{
		header("location: ../../settings/index.php");
		exit;
	}

	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$linterface = new interface_html();

$lclass = new libclass();

$yearID = Get_Current_Academic_Year_ID();

if(!isset($ToDate) || $ToDate=="")
{
	$ToDate=date('Y-m-d');
}

//empty the tmp user folder
   $lf->folder_remove_recursive($intranet_root.'/file/homeworkImage/tmp/'.$UserID);
# Current Year Term
$currentYearTerm = getAcademicYearAndYearTermByDate($today);
$yearTermID = $currentYearTerm[0];
$yearTermID = $yearTermID ? $yearTermID : 0;

$x .= '<table width=100% cellspacing=0 cellpadding=0><tr><td>';
$x .= '<table class="common_table_list">';
	$x .= '<tr>';
		$x .= '<th style="width:25%;">'.$Lang['AccountMgmt']['Form'].'</th>';
	    $x .= '<th class="sub_row_top" style="width:25%;">'.$Lang['AccountMgmt']['Class'].'</th>';
	    $x .= '<th class="sub_row_top" style="width:25%;">'.$Lang['SysMgr']['Homework']['HomeworkImage'].'</th>';
	    $x .= '<th class="sub_row_top" style="width:25%;"><input type="checkbox" id="checkall" name="checkall" onclick="Set_Checkbox_Value(\'CourseID[]\',this.checked)" /></th>';
    $x .= '</tr>';
	      
// Form list start
$FormList = $fcm->Get_Form_List();
for ($i=0; $i< sizeof($FormList); $i++) {
	
	$ClassList = $fcm->Get_Class_List($yearID,$FormList[$i]['YearID']);
	$RowSpan = (sizeof($ClassList) > 0)? 'rowspan="'.(sizeof($ClassList)+2).'"':'';
	$FirstDisplay = (sizeof($ClassList) > 0)? 'display:none;':'';
	$x .= '<tbody>';
	$x .= '<tr>';
		$x .= '<td width="30%" '.$RowSpan.'>'.$FormList[$i]['YearName'].'</td>';
		$x .= '<td width="30%" style="'.$FirstDisplay.'">&nbsp;</td>';
		$x .= '<td width="40%" style="'.$FirstDisplay.'">&nbsp;</td>';
		$x .= '<td width="25%" style="'.$FirstDisplay.'"></td>';
	$x .= '</tr>';
	for ($j=0; $j< sizeof($ClassList); $j++) {
		$thisClassID = $ClassList[$j]['YearClassID'];
		$sql = "select FilePath from INTRANET_HOMEWORK_IMAGE where CourseID = ".$thisClassID." and Date = '".$ToDate."'";
		$filePath = $lhomework->returnArray($sql);
		$x .= '<tr class="sub_row" id="'.$ClassList[$j]['YearClassID'].'">';
			$x .= '<td>'.Get_Lang_Selection($ClassList[$j]['ClassTitleB5'],$ClassList[$j]['ClassTitleEN']).'</td>';
			if($filePath[0][0]!=""){
				$x .= '<td><img src="'.$filePath[0][0].'" width="400"></td>';
				$x .= '<td><input type="checkbox" id="CourseID[]" name="CourseID[]" value="'.$thisClassID.'" /></td>';				
			}else{
				$x .= '<td></td>';
				$x .= '<td></td>';										
			}
		$x .= '</tr>';
	}
	$x .= '</tbody>';
}
$x .= '</table>';
$x .= '</td></tr></table>';
$toolbar = $linterface->GET_LNK_UPLOAD("upload.php?date=$ToDate");

$datepicker = $linterface->GET_DATE_PICKER($Name="ToDate",$DefaultValue=$ToDate,$OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="reloadPage();",$ID="",$SkipIncludeJS=0, $CanEmptyField=0, $Disable=false, $cssClass="textboxnum");
# Start layout
$CurrentPageArr['eAdminHomework'] = 1;
$CurrentPage = "Management_HomeworkImage";
$PAGE_TITLE = $Lang['SysMgr']['Homework']['HomeworkImage'];
$MODULE_OBJ = $lhomework->GET_MODULE_OBJ_ARR();	
$TAGS_OBJ[] = array($Lang['SysMgr']['Homework']['HomeworkImage']);

$linterface->LAYOUT_START();
	
?>

<script language="javascript">
function reloadPage()
  {
      document.form.action="index.php";
	  document.form.submit();
  }

function deleteImage()
{
	 var r=confirm("<?=$Lang['General']['__Warning']['AreYouSureYouWouldLikeToDeleteThisFile']?>")
     if (r==true)
     {
      document.form.action="delete.php";
	  document.form.submit();
	 }
}
  
</script>

<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
			<form name="form" method="post">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td>
							<table border="0" cellspacing="0" cellpadding="2" width="400">
								<tr>
									<td><?=$toolbar ?></td>
								</tr>
								<tr>
									<td><?=$datepicker ?></td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
				<div class="table_board">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td align="right" valign="bottom">										
								<div class="common_table_tool">
								<a title="<?=$button_delete?>" class="tool_delete" href="javascript:void(0);" onclick="javascript:deleteImage()" /><?=$button_delete?></a>
								</div>
							</td>
						</tr>
					</table>
				</div>
			<?=$x?>
			</form>
		</td>
	</tr>
</table>
<?
	$linterface->LAYOUT_STOP();
	intranet_closedb();
?>