<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

$li = new libdb();
$lo = new libfilesystem();
$lh = new libhomework();
$lf = new libfilesystem();
$linterface = new interface_html();
$lhomework = new libhomework2007();


$result_arr = array();

for($i=0;$i<sizeof($uploaded_img);$i++){
	list($db,$tmp_folder,$user_folder,$img_path) = explode("/",$uploaded_img[$i]);

			
	$img_path_arr= explode(".",$img_path);
	$extension = $img_path_arr[1];

	# move from tmp folder to assignment folder
	$dest_folder = $intranet_root.'/file/homeworkImage/'.$courseID[$i];
			
	//$dest_file = $eclass_filepath.'/files/'.$db.'/assignment/hw_list/'.$img_path;
	
	$dest_file = $dest_folder.'/hw_'.str_replace('-','',$date).'.'.$extension;
	
	$tmp_file = $intranet_root.'/file/homeworkImage/tmp/'.$user_folder.'/'.$img_path;
	
	if(is_dir($dest_folder)==false)
	{
		$lf->folder_new($dest_folder);	
	}
	
	$lf->lfs_move($tmp_file, $dest_file);
	
	
	if($img_path!=''){
		
		# check if it's an image update or insert
		$attachment_path = '/file/homeworkImage/'.$courseID[$i].'/hw_'.str_replace('-','',$date).'.'.$extension;	
		
		$sql = "SELECT count(*) FROM INTRANET_HOMEWORK_IMAGE WHERE CourseID = '".$courseID[$i]."' and Date='".$date."'";
		$count_obj = $li->returnVector($sql);
				
		if($count_obj[0]==0){
			$sql = "INSERT INTO INTRANET_HOMEWORK_IMAGE
					(CourseID,Date,FilePath,LastModified,inputdate)
					values
					('".$courseID[$i]."','".$date."','".$attachment_path."',now(),now())";
		}
		else{
			$sql = "UPDATE INTRANET_HOMEWORK_IMAGE set FilePath='".$attachment_path."',modified = now() WHERE CourseID= '".$courseID[$i]."' and Date='".$date."'";
		}
		
		$result_arr[] = $li->db_db_query($sql);		
	}
}
$backBtn = $linterface->GET_ACTION_BTN($button_back, "button", "window.location.href='index.php?ToDate=$date'", "backBtn");
# navigation
$PAGE_NAVIGATION[] = array($Lang['SysMgr']['Homework']['HomeworkImage'], "index.php");
$PAGE_NAVIGATION[] = array($button_upload, "");
$PageNavigation = $linterface->GET_NAVIGATION($PAGE_NAVIGATION);

$CurrentPageArr['eAdminHomework'] = 1;
$CurrentPage = "Management_HomeworkImage";

$TAGS_OBJ[] = array($Lang['SysMgr']['Homework']['HomeworkImage']);
$MODULE_OBJ = $lhomework->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

intranet_closedb();
?>
<style type="text/css">
</style>
<script type="text/javascript">
</script>
<table width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td>
						<table border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td><?=$PageNavigation ?></td>
							</tr>
						</table>
					</td>
					<td align="right"></td>
				</tr>
			</table>
			<br>
            <form name="form1" action="upload_finish.php" method="post" enctype="multipart/form-data">
                <tr> 
                  <td valign="top" height="100%">                        
                            <h1><?=$Lang['homework_list_uploaded_image_upload_result']?></h1>
                            <div class="upload_result_board" align="center">
                            <?= $homework_import_extra['succeed_num'].": ".sizeof($result_arr) ?>
                            </div>  
                  </td>
                </tr>
	<tr>
		<td colspan="2" class="dotline">
			<img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1">
		</td>
	</tr>
	<tr>
		<td colspan="2" align="center">
	        <div align="center">
				<img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="1" height="1">
				<?= $backBtn ?>
	        </div>
        </td>
	</tr>
</table>

<?		
		
$linterface->LAYOUT_STOP();		
?>
