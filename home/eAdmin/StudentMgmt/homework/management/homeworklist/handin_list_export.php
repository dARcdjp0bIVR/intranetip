<?php
// modifying : 

###################################
#	Date    :	2019-05-08 (Bill) prevent SQL Injection
#   Date    :   2018-05-14 (Anna) When studentStatus is 3, don't export record
#	Date:	:	2017-06-30 (Carlos) $sys_custom['LivingHomeopathy'] - added display columns [Handed In Homework], [Marking] and [Rating].
#	Date	:	2016-08-09 (Bill)	[2016-0519-1005-58207]
#	Detail	:	Fixed: no access right problem
###################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

$lhomework = new libhomework2007();
/******************************/
$handInStatusArr = $lhomework->getAllHandInStatus();
/******************************/

//if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || !($_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"] || $_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] && !$lhomework->isViewerGroupMember($UserID)))
if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || (!($_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"] || $_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"]) && !$lhomework->isViewerGroupMember($UserID)))
{
	if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])
	{
		header("location: ../../settings/index.php");	
		exit;
	}
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$lexport = new libexporttext();

$lu = new libuser($UserID);

$CurrentPageArr['eAdminHomework'] = 1;

/*
if (!$lu->isTeacherStaff())
{
     header("Location: /close.php");
     exit();
}
*/

$hid = IntegerSafe($hid);
$data = $lhomework->handinList($hid);
if($sys_custom['LivingHomeopathy']){
	$handin_documents = $lhomework->getHomeworkHandinListRecords(array('HomeworkID'=>$hid,'StudentIDToRecord'=>1,'GetTeacherName'=>1));
}

for($i=0; $i<sizeof($data); $i++)
{
    list($uid,$studentName,$className,$classNumber,$handinStatus,$handinID,$dateModified,$confirmUserID,$confirmTime,$topic, $due,$SuppRecordStatus,$studentStatus,$LeaveStudent) = $data[$i];

    if (!(empty($handinID) && $studentStatus == '3')) {
        if ($studentStatus == '3') {
            $studentName = strip_tags($studentName);
        }
        
        $m = 0;
        $ExportArr[$i][$m] = $topic;		//Topic
        $m++;
        $ExportArr[$i][$m] = $className;		//Class Name
        $m++;
        $ExportArr[$i][$m] = $classNumber;		//Class Number
        $m++;
        $ExportArr[$i][$m] = $studentName;		//Student Name
        $m++;
        
        // $notSubmitStr = date("Y-m-d") <= $due ? $Lang['SysMgr']['Homework']['NotSubmitted'] : $Lang['SysMgr']['Homework']['ExpiredWithoutSubmission'];
        if (date("Y-m-d") <= $due) {
            $notSubmitStr = $Lang['SysMgr']['Homework']['NotSubmitted'];
        }
        else {
            $notSubmitStr = Get_Lang_Selection($handInStatusArr["-1"]["HandInStatusTitleB5"], $handInStatusArr["-1"]["HandInStatusTitleEN"]);
        }
        
        /*
         if($handinStatus==-1){
         $status = $notSubmitStr;
         }
         
         else if($handinStatus==1){
         $status = $Lang['SysMgr']['Homework']['Submitted'];
         }
         else if($handinStatus==2){
         $status = $Lang['SysMgr']['Homework']['LateSubmitted'];
         }
         else if($handinStatus==3){
         $status = $Lang['SysMgr']['Homework']['Redo'];
         }
         else if($handinStatus==4){
         $status = $Lang['SysMgr']['Homework']['NoNeedSubmit'];
         }
         else if($handinStatus==5){
         $status = $Lang['SysMgr']['Homework']['UnderProcessing'];
         }
         else if($handinStatus==6){
         $status = $Lang['SysMgr']['Homework']['Supplementary'];
         }
         else {
         $status = $Lang['SysMgr']['Homework']['NoNeedSubmit'];
         }
         */
        if (in_array($SuppRecordStatus, array(7,-11)))
        {
            $handinStatus = $SuppRecordStatus;
        }
        if ($handinStatus != -1) {
            $status = Get_Lang_Selection($handInStatusArr[$handinStatus]["HandInStatusTitleB5"], $handInStatusArr[$handinStatus]["HandInStatusTitleEN"]);
        } 
        else {
            $status = $notSubmitStr;
        }
        $ExportArr[$i][$m] = $status;		// Status
        $m++;
        
        if($sys_custom['LivingHomeopathy']){
            if(isset($handin_documents[$uid]) && $handin_documents[$uid]['StudentDocument']!=''){
                $student_document_name = substr($handin_documents[$uid]['StudentDocument'],strrpos($handin_documents[$uid]['StudentDocument'],'/')+1);
                $student_document_display = $student_document_name;
            }
            else{
                $student_document_display = $Lang['eHomework']['NotSubmitted'];
            }
            $teacher_document_display = '';
            if(isset($handin_documents[$uid]) && $handin_documents[$uid]['TeacherDocument']!='')
            {
                $teacher_document_name = substr($handin_documents[$uid]['TeacherDocument'],strrpos($handin_documents[$uid]['TeacherDocument'],'/')+1);
                $teacher_document_display .= $teacher_document_name;
            }
            $score_input_text = '';
            if(isset($handin_documents[$uid]) && $handin_documents[$uid]['TextScore']!=''){
                $score_input_text = $handin_documents[$uid]['TextScore'];
            }
            $score_input_display = '';
            if(isset($handin_documents[$uid]) && $handin_documents[$uid]['StudentDocument']!='')
            {
                $score_input_display.= $score_input_text;
            }
            $ExportArr[$i][$m] = $student_document_display;
            $m++;
            $ExportArr[$i][$m] = $teacher_document_display;
            $m++;
            $ExportArr[$i][$m] = $score_input_display;
            $m++;
        }
    }
}

if(sizeof($data)==0) {
	$ExportArr[0][0] = $i_no_record_exists_msg;	
}

//$filename = "Hand-in_List_".$subject."_".$subjectGroup.".csv";
$filename = "Hand-in_List.csv";

$exportColumn = array($Lang['SysMgr']['Homework']['Topic'], $Lang['SysMgr']['Homework']['Class'], $Lang['SysMgr']['Homework']['ClassNumber'], $Lang['SysMgr']['Homework']['StudentName'], $Lang['SysMgr']['Homework']['Status']);	
if($sys_custom['LivingHomeopathy']){
	$exportColumn = array_merge($exportColumn, array($Lang['eHomework']['HandedInHomework'],$Lang['eHomework']['Mark'],$Lang['eHomework']['Rating']));
}

$export_content = $lexport->GET_EXPORT_TXT($ExportArr, $exportColumn, "", "\r\n", "", 0, "00");

intranet_closedb();

// Output the file to user browser
$lexport->EXPORT_FILE($filename, $export_content);
?>