<?php
// Modifing by yat

$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");

intranet_auth();
intranet_opendb();

# check access right
if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || (!$_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"]  && !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] && !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_subject_leader"]))
{
	if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])
	{
		header("location: ../../settings/index.php");
		exit;
	}

	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$linterface = new interface_html();
$lhomework = new libhomework2007();
$lclass = new libclass();

# change page size
if ($page_size_change == 1)
{
	setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
	$ck_page_size = $numPerPage;
}

if (isset($ck_page_size) && $ck_page_size != "")
	$page_size = $ck_page_size;

$pageSizeChangeEnabled = true;

# Table initialization
$order = ($order == "") ? 1 : $order;
$field = ($field == "") ? $sortField : $field;
$pageNo = ($pageNo == "") ? 1 : $pageNo;

# Create a new dbtable instance
$li = new libdbtable2007($field, $order, $pageNo);

# Settings
$s = addcslashes($s, '_');
$searchByTeacher =($lhomework->teacherSearchDisabled == 0) ? "or a.PosterName like '%$s%'" : "" ;
$searchBySubject =($lhomework->subjectSearchDisabled == 0) ? " or IF('$intranet_session_language' = 'en', b.EN_DES, b.CH_DES) like '%$s%'" : "" ;

$today = date('Y-m-d');
$subjectID = ($subjectID=="")? -1 :$subjectID;

if($subjectGroupID && $subjectID==-1)
{
	$TempSubjectInfo = $lhomework->RetrieveSubjectbySubjectGroupID($subjectGroupID);
	$subjectID = $TempSubjectInfo[0]['SubjectID'];
}
	
$yearID = Get_Current_Academic_Year_ID();

# Current Year Term
$currentYearTerm = getAcademicYearAndYearTermByDate($today);
$yearTermID = $currentYearTerm[0];
$yearTermID = $yearTermID ? $yearTermID : 0;

# Filter - Class
// $select_class = $lclass->getSelectClassID("name=\"ClassID\" onChange=\"document.form1.subjectGroupID.selectedIndex=0; reloadForm()\"",$targetClass,'',$Lang['SysMgr']['FormClassMapping']['AllClass']);
// // if ($targetClass != "")
// //     $select_students = $lclass->getStudentSelectByClass($targetClass,"name=\"targetID\" onChange=\"this.form.targetType.value=1\" ");
// debug_pr($ClassID);

# Filter - Subject 
if($_SESSION['UserType']!=USERTYPE_STUDENT)
{
	$subject = $lhomework->getTeachingSubjectList($UserID, $yearID, $yearTermID);
	$selectedSubject = $lhomework->getCurrentTeachingSubjects("name=\"subjectID\" onChange=\"document.form1.subjectGroupID.selectedIndex=0; reloadForm()\"", $subject, $subjectID, $Lang['SysMgr']['Homework']['AllSubjects'], true, -1);
}
else	# Subject Leader
{
	$subject = $lhomework->getStudyingSubjectList($UserID, 1, $yearID, $yearTermID);
	$selectedSubject = $lhomework->getCurrentStudyingSubjects("name=\"subjectID\" onChange=\"this.form.method='get'; this.form.action=''; this.form.submit();\"", $subject, $subjectID, $Lang['SysMgr']['Homework']['AllSubjects']);
}


if($_SESSION['UserType']!=USERTYPE_STUDENT)
{
	# Filter - Subject Groups
	$subjectGroups = $lhomework->getTeachingSubjectGroupList($UserID,$subjectID, $yearID, $yearTermID);
	$selectedSubjectGroups = $lhomework->getCurrentTeachingGroups("name=\"subjectGroupID\" onChange=\"reloadForm()\"", $subjectGroups, $subjectGroupID, $Lang['SysMgr']['Homework']['AllSubjectGroups']);
}

# Filter display
$filterbar = "$select_class $selectedSubject $selectedSubjectGroups";

$sgid = $subjectGroupID;

if($sid!="" && $sid != $subjectID){
	$subjectGroupID="";
}

# Export link
$exportLink = "export.php?yearID=$yearID&yearTermID=$yearTermID&subjectID=$subjectID&subjectGroupID=$subjectGroupID&picID=$UserID&s=$s&flag=0";

# Toolbar: new, import, export
$toolbar = $linterface->GET_LNK_NEW("add.php?yearID=$yearID&yearTermID=$yearTermID&subjectID=$subjectID&subjectGroupID=$subjectGroupID", "", "", "", "",0)."&nbsp;&nbsp;&nbsp;&nbsp;";
$toolbar .= $linterface->GET_LNK_IMPORT("import.php?yearID=$yearID&yearTermID=$yearTermID&subjectID=$subjectID&subjectGroupID=$subjectGroupID", "", "", "", "",0)."&nbsp;&nbsp;&nbsp;&nbsp;";
if($lhomework->exportAllowed)
	$toolbar .= $linterface->GET_LNK_EXPORT($exportLink, "", "", "", "", 0);

# SQL statement
if(sizeof($subjectGroups)!=0)
{
	$allGroups = " a.ClassGroupID IN (";
	for ($i=0; $i < sizeof($subjectGroups); $i++)
	{
		list($groupID)=$subjectGroups[$i];
		$allGroups .= $groupID.",";
	}
	$allGroups = substr($allGroups,0,strlen($allGroups)-1).")";
	$allGroups .=" AND";
}
else{
	$allGroups ="";
	$subjectGroupID ="";
}

	# set conditions
	$date_conds = "AND a.DueDate >= CURDATE()";
	$conds = ($subjectGroupID=='')? "$allGroups" : " a.ClassGroupID = $subjectGroupID AND";
	$conds .= ($s=='')? "": "(IF('$intranet_session_language' = 'en', c.ClassTitleEN, c.ClassTitleB5) like '%$s%'
								or a.Title like '%$s%'
								$searchByTeacher
								$searchBySubject
							) AND";

	$desc = "CONCAT(' <img src=\"$image_path/homework/$intranet_session_language/hw/icon_alt.gif\" title=\"',a.Description,'\" \>')";
	$attach = "CONCAT(' <img src=\"$image_path/homework/$intranet_session_language/hw/icon_attachment.gif\" title=\"$i_AnnouncementAttachment\" \>')";
	$handin = "CONCAT('<a href=\"javascript:viewHandinList(',a.HomeworkID,')\"><img src=\"$image_path/homework/$intranet_session_language/hw/btn_handin_list.gif\" border=\"0\" /></a>')";

	$fields = "IF('$intranet_session_language' = 'en', b.EN_DES, b.CH_DES) AS Subject,
			   IF('$intranet_session_language' = 'en', c.ClassTitleEN, c.ClassTitleB5) AS SubjectGroup,
			   CONCAT(CONCAT(CONCAT(CONCAT('<a class=tablelink href=javascript:viewHmeworkDetail(',a.HomeworkID,')>',a.Title,'</a>'), if (a.StartDate=CURDATE(),' $i_Homework_today','')), IF(a.Description='', '',$desc)), IF(a.AttachmentPath!='NULL', $attach, '')),
			   a.StartDate, a.DueDate, a.Loading/2, 
			   (if (a.HandinRequired=1, ". $handin . ",'". $i_general_no."')), 
			   ";
	if($lhomework->useHomeworkCollect)
	{
		$fields .= "(if (a.CollectRequired=1,'". $i_general_yes."','". $i_general_no."')), ";
	}			   
	$fields .= " CONCAT('<input type=checkbox name=HomeworkID[] value=', a.HomeworkID ,' onClick=\"unset_checkall(this, this.form);\">')";

	$dbtables = "INTRANET_HOMEWORK as a LEFT OUTER JOIN ASSESSMENT_SUBJECT as b ON b.RecordID = a.SubjectID
				LEFT OUTER JOIN SUBJECT_TERM_CLASS as c ON c.SubjectGroupID = a.ClassGroupID ";

	if($_SESSION['UserType']==USERTYPE_STUDENT)
	{
		$dbtables .= "LEFT OUTER JOIN SUBJECT_TERM_CLASS_USER AS d ON d.SubjectGroupID = a.ClassGroupID";
		$conds .= " d.UserID = $UserID AND a.YearTermID = $yearTermID AND a.AcademicYearID = $yearID $date_conds";
	}
	else
	{
		# do not map by user, should allow for same subject group
		$conds .= " a.YearTermID = $yearTermID AND a.AcademicYearID = $yearID $date_conds";
	}	

	$sql = "SELECT $fields FROM $dbtables WHERE $conds";

	$li->field_array = array("Subject", "SubjectGroup", "a.Title", "a.StartDate", "a.DueDate", "a.Loading", "a.HandinRequired");
	$li->sql = $sql;
	$li->no_col = sizeof($li->field_array)+2;
	if($lhomework->useHomeworkCollect)
		$li->no_col++;
	$li->IsColOff = 2;

	# TABLE COLUMN
	$pos = 0;
	$li->column_list .= "<td width='5%'>#</td>\n";
	$li->column_list .= "<td width='10%' class='tabletop tabletoplink'>".$li->column($pos++, $Lang['SysMgr']['Homework']['Subject'])."</td>\n";
	$li->column_list .= "<td width='20%' class='tabletop tabletoplink'>".$li->column($pos++, $Lang['SysMgr']['Homework']['SubjectGroup'])."</td>\n";
	$li->column_list .= "<td width='20%' class='tabletop tabletoplink'>".$li->column($pos++, $Lang['SysMgr']['Homework']['Topic'])."</td>\n";
	$li->column_list .= "<td width='12%' class='tabletop tabletoplink'>".$li->column($pos++, $Lang['SysMgr']['Homework']['StartDate'])."</td>\n";
	$li->column_list .= "<td width='12%' class='tabletop tabletoplink'>".$li->column($pos++, $Lang['SysMgr']['Homework']['DueDate'])."</td>\n";
	$li->column_list .= "<td width='13%' class='tabletop tabletoplink'>".$li->column($pos++, $Lang['SysMgr']['Homework']['Workload']." (".$Lang['SysMgr']['Homework']['Hours'].")")."</td>\n";
	$li->column_list .= "<td width='12%' class='tabletop tabletoplink'>".$li->column($pos++, $Lang['SysMgr']['Homework']['HandinRequired'])."</td>\n";
	if($lhomework->useHomeworkCollect)
	{
		$li->column_list .= "<td width='12%' class='tabletop tabletoplink' nowrap>".$li->column($pos++, $i_Homework_Collected_By_Class_Teacher)."</td>\n";
	}
	
	$li->column_list .= "<td class='tabletop tabletoplink'>".$li->check("HomeworkID[]")."</td>\n";


# Start layout
$CurrentPageArr['eAdminHomework'] = 1;
$CurrentPage = "Management_HomeworkList";
$PAGE_TITLE = $Lang['SysMgr']['Homework']['HomeworkList'];
$MODULE_OBJ = $lhomework->GET_MODULE_OBJ_ARR();	
$TAGS_OBJ[] = array($Lang['SysMgr']['Homework']['ToDoList'], "index.php?yearID=$yearID&yearTermID=$yearTermID&subjectID=$subjectID&subjectGroupID=$sgid&sid=$sid", 1);
$TAGS_OBJ[] = array($Lang['SysMgr']['Homework']['History'], "history.php?yearID=$yearID&yearTermID=$yearTermID&subjectID=$subjectID&subjectGroupID=$sgid&sid=$sid", 0);
if($_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"])
	$TAGS_OBJ[] = array($Lang['SysMgr']['Homework']['ClearHomework'], "clear_homework.php", 0);
$linterface->LAYOUT_START();
	
?>

<script language="javascript">
	function removeCat(obj,element,page){
		var alertConfirmRemove = "<?=$i_Discipline_System_alert_remove_record?>";
        if(countChecked(obj,element)==0)
                alert(globalAlertMsg2);
        else{
                if(confirm(alertConfirmRemove)){
                obj.action=page;
                obj.method="post";
                obj.submit();
                }
        }
	}


	function reloadForm() {
		document.form1.s.value = "";
		document.form1.pageNo.value = 1;
		document.form1.submit();
	}


	function goSearch() {
		document.form1.s.value = document.form2.text.value;
		document.form1.pageNo.value = 1;
		document.form1.submit();
	}


	function viewHmeworkDetail(id)
	{
		newWindow('./view.php?hid='+id,1);
	}


	function viewHandinList(id)
	{
		newWindow('./handin_list.php?hid='+id,10);
	}

</script>

<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
			<form name="form2" method="post" onSubmit="return false">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td>
							<table border="0" cellspacing="0" cellpadding="2">
								<tr>
									<td><?=$toolbar ?></td>
								</tr>
							</table>
						</td>
						<?if($display){?>
						<td align="right"><input name="text" type="text" class="formtextbox" value="<?=stripslashes(stripslashes($s))?>">
							<?=$linterface->GET_BTN($button_find, "button","javascript:goSearch()");?>
						</td>
						<? }?>
					</tr>
				</table>
			</form>
			<form name="form1" method="get" action="">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td align="right">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td height="28" align="right" valign="bottom">
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td align="right"><?= $linterface->GET_SYS_MSG($msg) ?>
											</td>
										</tr>
									</table>
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td height="30">
												<?=$filterbar?>
											</td>
										</tr>
									</table>
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td>
											</td>
											<td align="right" valign="bottom">
												<? if ($_SESSION['UserType']==USERTYPE_STAFF || $_SESSION["SSV_PRIVILEGE"]["homework"]["is_subject_leader"]) {
														if($display){
												?>
													<table border="0" cellspacing="0" cellpadding="0">
														<tr>
															<td width="21"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_01.gif" width="21" height="23"></td>
															<td background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_02.gif">
																<table border="0" cellspacing="0" cellpadding="2">
																	<tr>
																		<td nowrap><a href="javascript:checkEdit(document.form1,'HomeworkID[]','edit.php')" class="tabletool" title="<?= $button_edit ?>"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_edit.gif" width="12" height="12" border="0" align="absmiddle"> <?=$button_edit ?></a></td>
																		<td><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="5"></td>
																		<td nowrap><a href="javascript:removeCat(document.form1,'HomeworkID[]','remove_update.php?subjectID=<?=$subjectID?>&subjectGroupID=<?=$subjectGroupID?>')" class="tabletool" title="<?= $button_delete ?>"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_delete.gif" width="12" height="12" border="0" align="absmiddle"> <?= $button_delete ?></a></td>
																	</tr>
																</table>
															</td>
															<td width="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_03.gif" width="6" height="23"></td>
														</tr>
													</table>
												<? } } else{?>
													<br/>
													<br/>
												<? } ?>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>
						<?
						
						echo $li->display();
						
						/*
						if($display){
							echo $li->display();
						}
						else{
							if($_SESSION["SSV_PRIVILEGE"]["homework"]["is_subject_leader"])
								echo $Lang['SysMgr']['Homework']['HomeworkListWarningSubjectLeader'];
							else
								echo $Lang['SysMgr']['Homework']['HomeworkListWarning'];
						}
						*/
						?>
					</td>

				</tr>
			</table><br>
			<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>"/>
			<input type="hidden" name="order" value="<?php echo $li->order; ?>"/>
			<input type="hidden" name="field" value="<?php echo $li->field; ?>"/>
			<input type="hidden" name="numPerPage" value="<?=$li->page_size?>"/>
			<input type="hidden" name="page_size_change" value=""/>
			<input type="hidden" name="s" value="<?=$s?>"/>
			<input type="hidden" name="sid" value="<?=$subjectID?>"/>
			</form>
		</td>
	</tr>
</table>
<?
	$linterface->LAYOUT_STOP();
	intranet_closedb();
?>