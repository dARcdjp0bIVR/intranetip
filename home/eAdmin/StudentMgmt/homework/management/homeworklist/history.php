<?php
// Modifing by : 
	
##### Change Log [Start] ######
#	Date:	2019-12-11 (Philips) [2019-1018-1708-35098]
#			Added Export span with period option within 90 days
#			Added HandinRequired in exportlink
#
#	Date:	2019-05-08 (Bill)
#           prevent SQL Injection + Cross-site Scripting
#
#	Date:	2018-03-15 (Bill)	[DM#3389]
#			fixed: cannot search homework if search string contains special characters
#           - use Get_Safe_Sql_Like_Query() in query
#
#	Date:	2015-11-17 (Bill)	[2015-1117-1137-20073]
#			fixed: Viewer Group members should able to select all classes in drop down list 
#
#	Date:	2015-09-02 (Bill)	[2015-0901-1009-21066]
#			fixed: drop down list filtering not work, use data from $_POST
#
#	Date:	2014-09-22 (YatWoon)
#			add flag checking $sys_custom['eHomework']['HideClearHomeworkRecords'] [Case#F68177]
#			Deploy: IPv10.1
#
#	Date:	2014-04-15 (Yuen)
#			only display subject group for particular term		
#
#	Date:	2011-08-02 (Henry Chow)
#			add checking on $lhomework->ClassTeacherCanViewHomeworkOnly
#
#
###### Change Log [End] #######
    
	$PATH_WRT_ROOT = "../../../../../../";
	include_once($PATH_WRT_ROOT."includes/global.php");
	include_once($PATH_WRT_ROOT."includes/libdb.php");
	include_once($PATH_WRT_ROOT."includes/libinterface.php");
	include_once($PATH_WRT_ROOT."includes/libhomework.php");
	include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
	include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
	include_once($PATH_WRT_ROOT."includes/libdbtable.php");
	include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
    
	intranet_auth();
	intranet_opendb();
	
	# Create homework instance
	$lhomework = new libhomework2007();
	
	if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || (!$_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"]  && !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] && !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_subject_leader"] && !$lhomework->isViewerGroupMember($UserID)))
	{
		if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])
		{
			header("location: ../../settings/index.php");
			exit;
		}
		
		include_once($PATH_WRT_ROOT."includes/libaccessright.php");
		$laccessright = new libaccessright();
		$laccessright->NO_ACCESS_RIGHT_REDIRECT();
		exit;
	}
    
	# Temp assign memory of this page
	ini_set("memory_limit", "150M");
    
	# Create interface instance
	$linterface = new interface_html();
    
	# Current Page
	$CurrentPage = "Management_HomeworkList";
	$CurrentPageArr['eAdminHomework'] = 1;
    
	# Page title
	$PAGE_TITLE = $Lang['SysMgr']['Homework']['HomeworkList'];
	$MODULE_OBJ = $lhomework->GET_MODULE_OBJ_ARR();
    
	# Select sort field
	$sortField = 0;
    
	# Change page size
	if ($page_size_change == 1)
	{
		setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
		$ck_page_size = $numPerPage;
	}
	if (isset($ck_page_size) && $ck_page_size != "")
	{
		$page_size = $ck_page_size;
	}
	$pageSizeChangeEnabled = true;
	
	function resetSubjectGroupID($subjectGroupID, $subjectGroups)
	{
		for ($i=0; $i<sizeof($subjectGroups); $i++)
		{
			if ($subjectGroupID==$subjectGroups[$i][0])
			{
				return $subjectGroupID;
			}
		}
		return "";
	}
    
	# Table Setting
	$order = ($order == "") ? 1 : $order;
	$field = ($field == "") ? $sortField : $field;
	$pageNo = ($pageNo == "") ? 1 : $pageNo;
    
	# Create dbtable instance
	$li = new libdbtable2007($field, $order, $pageNo);
	
	// [2015-0901-1009-21066] fixed: drop down list filtering not work, use data from $_GET[] only if $_POST[] is not set
	$yearID = isset($_POST['yearID'])? $_POST['yearID'] : $_GET['yearID'];
	$yearTermID = isset($_POST['yearTermID'])? $_POST['yearTermID'] : $_GET['yearTermID'];
	$subjectID = isset($_POST['subjectID'])? $_POST['subjectID'] : $_GET['subjectID'];
	$subjectGroupID = isset($_POST['subjectGroupID'])? $_POST['subjectGroupID'] : $_GET['subjectGroupID'];
	$sid = isset($_POST['sid'])? $_POST['sid'] : $_GET['sid'];
	
	### Handle SQL Injection + XSS [START]
	$yearID = IntegerSafe($yearID);
	$yearTermID = IntegerSafe($yearTermID);
	$subjectID = IntegerSafe($subjectID);
	$subjectGroupID = IntegerSafe($subjectGroupID);
	$classID = IntegerSafe($classID);
	$sid = IntegerSafe($sid);
	$sgid = IntegerSafe($sgid);
	### Handle SQL Injection + XSS [END]
    
	# Search fields
	//$s = addcslashes($s, '_');
	$s = intranet_htmlspecialchars(trim($s));
	$s = stripslashes($s);
	$searchStringSQL = $lhomework->Get_Safe_Sql_Like_Query($s);
	$searchByTeacher = ($lhomework->teacherSearchDisabled == 0) ? " OR a.PosterName LIKE '%".$searchStringSQL."%'" : "" ;
	$searchBySubject = ($lhomework->subjectSearchDisabled == 0) ? " OR IF('$intranet_session_language' = 'en', b.EN_DES, b.CH_DES) LIKE '%".$searchStringSQL."%'" : "" ;
	
	# Export onclick show span
	
	
	$allowExport = $lhomework->exportAllowed;
	
	# Class Menu
	if($_SESSION['UserType'] != USERTYPE_STUDENT)
	{
		// [2015-1117-1137-20073] Viewer Group member should able to select all classes
		# eHomework Admin
		if(($_SESSION['UserType']==USERTYPE_STAFF && !$_SESSION['isTeaching']) || $_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] || $lhomework->isViewerGroupMember($UserID)) {
			$classes = $lhomework->getAllClassInfo();
		}
		# Class Teacher / Subject Teacher
		else {
			$classes = $lhomework->getAllClassesInvolvedByTeacherID(Get_Current_Academic_Year_ID(), GetCurrentSemesterID(), $UserID);
		}
		$selectClass = getSelectByArray($classes, 'name="classID" id="classID" onChange="document.form1.submit();"', $classID, 0, 0, $i_general_all_classes);
	}
    
	# Teacher Mode with Teaching
	if($_SESSION['UserType']==USERTYPE_STAFF && $_SESSION['isTeaching'])
	{
		# Academic Year
		$academicYear = $lhomework->GetAllAcademicYear();
		$yearID = ($yearID=="") ? Get_Current_Academic_Year_ID() : $yearID;
        
		$selectedYear = $lhomework->getCurrentYear("name=\"yearID\" onChange=\"reloadForm()\"", $academicYear, $yearID, "", false);
		if($yearID != "")
		{
			if($yid != "" && $yid != $yearID) {
				$yearTermID = "";
			}
            
			# Year Term
			$academicYearTerm = $lhomework->getAllAcademicYearTerm($yearID);
			$selectedYearTerm = $lhomework->getCurrentYearTerm("name=\"yearTermID\" onChange=\"reloadForm()\"", $academicYearTerm, $yearTermID, $Lang['SysMgr']['Homework']['AllYearTerms']);
			if($ytid != "" && $ytid != $yearTermID) {
				$subjectID = "";
			}
			
			# Subject Menu
			$subjectID = ($subjectID=="")? -1 : $subjectID;
			$UserIDTmp = $_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] ? "" : $UserID;
			$subject = $lhomework->getTeachingSubjectList($UserIDTmp, $yearID, $yearTermID, $classID);
			$selectedSubject = $lhomework->getCurrentTeachingSubjects("name=\"subjectID\" onChange=\"reloadForm();\"", $subject, $subjectID, $Lang['SysMgr']['Homework']['AllSubjects'], true);		
			if($subjectID != "-1") {
				$subjectGroups = $lhomework->getTeachingSubjectGroupList($UserIDTmp, $subjectID, $yearID, $yearTermID, $classID);
			}
			else {
				$subjectGroups = $lhomework->getTeachingSubjectGroupList($UserIDTmp, "", $yearID, $yearTermID, $classID);
			}
			$subjectGroupID = resetSubjectGroupID($subjectGroupID, $subjectGroups);
			
			$selectedSubject = $lhomework->getCurrentTeachingSubjects("name=\"subjectID\" onChange=\"reloadForm();\"", $subject, $subjectID, $Lang['SysMgr']['Homework']['AllSubjects'], true);
			if ($yearTermID!="" && $yearTermID > 0)
			{
				# only display subject group for particular term		
				$selectedSubjectGroups = $lhomework->getCurrentTeachingGroups("name=\"subjectGroupID\" onChange=\"reloadForm()\"", $subjectGroups, $subjectGroupID, $Lang['SysMgr']['Homework']['AllSubjectGroups']);
			}
			else
			{
				$subjectGroupID = "";
			}
			
			$filterbar = "$selectedYear $selectedYearTerm $selectClass $selectedSubject $selectedSubjectGroups";
		}
		
		# Export link
		$exportLink = "export.php?yearID=$yearID&yearTermID=$yearTermID&classID=$classID&subjectID=$subjectID&subjectGroupID=$subjectGroupID&picID=$UserID&s=$s&flag=1";
		/*
		if($allowExport)
			$toolbar .= $linterface->GET_LNK_EXPORT($exportLink, "", "", "", "", 0);
		*/
		
		# SQL statement
		if(sizeof($subjectGroups) != 0)
		{
			$allGroups = " a.ClassGroupID IN (";
			for ($i=0; $i < sizeof($subjectGroups); $i++)
			{
				list($groupID) = $subjectGroups[$i];
				$allGroups .= $groupID.",";
			}
			$allGroups = substr($allGroups,0,strlen($allGroups)-1).")";
			$allGroups .= " AND";
		}
		else if(sizeof($classes)==0 && sizeof($subject)==0)
		{
			$allGroups = " a.ClassGroupID IN ('') AND ";
			$noRecord = 1;
		}
		else
		{
			$allGroups = "";
			$subjectGroupID = "";
            
			$sbjGps = $lhomework->getSubjectGroupIDByTeacherID($UserID, $yearID);
			if(sizeof($sbjGps) > 0)
			{
				for($i=0; $i<sizeof($sbjGps); $i++) {
					$temp[$i] = $sbjGps[$i][0];
				}
				$allGroups .= " a.ClassGroupID IN (".implode(',',$temp).") AND ";
			}
		}
		
		# Cater with normal teacher without any subject group
		if(sizeof($subjectGroups)==0 && !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"]) {
			$subjectGroupID = -1;
		}
		
		# Set conditions
		$date_conds = "AND a.DueDate < CURDATE()";
		$conds = ($subjectGroupID=='' || $subjectGroupID==-1)? "$allGroups" : " a.ClassGroupID = '$subjectGroupID' AND";
		$conds .= ($s=='')? "" : "(IF('$intranet_session_language' = 'en', c.ClassTitleEN, c.ClassTitleB5) LIKE '%".$searchStringSQL."%'
									OR a.Title LIKE '%".$searchStringSQL."%'
									$searchByTeacher
									$searchBySubject
								) AND";
        
		$desc = "CONCAT(' <img src=\"$image_path/homework/$intranet_session_language/hw/icon_alt.gif\" title=\"',a.Description,'\" \>')";
		$attach = "CONCAT(' <img src=\"$image_path/homework/$intranet_session_language/hw/icon_attachment.gif\" title=\"$i_AnnouncementAttachment\" \>')";
		$handin = "CONCAT('<a href=\"javascript:viewHandinList(',a.HomeworkID,')\"><img src=\"$image_path/homework/$intranet_session_language/hw/btn_handin_list.gif\" border=\"0\" /></a>')";
        
		$fields = "IF('$intranet_session_language' = 'en', b.EN_DES, b.CH_DES) AS Subject,
				   IF('$intranet_session_language' = 'en', c.ClassTitleEN, c.ClassTitleB5) AS SubjectGroup,";
		if($lhomework->useHomeworkType) {
			$fields .= " IF(e.TypeName!='', e.TypeName, '---'), ";	
		}
		$fields .= " CONCAT(CONCAT(CONCAT(CONCAT('<a class=tablelink href=javascript:viewHmeworkDetail(',a.HomeworkID,')>',a.Title,'</a>'), if (a.StartDate=CURDATE(),' $i_Homework_today','')), IF(a.Description='', '',$desc)), IF(a.AttachmentPath!='NULL', $attach, '')),
				   a.StartDate, a.DueDate, a.Loading/2, if (a.HandinRequired=1, $handin,'--')";
		if($lhomework->useHomeworkCollect) {
			$fields .= ", (if (a.CollectRequired=1,'". $i_general_yes."','". $i_general_no."')) ";
		}
		if($_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"]) {
			$fields .= ", CONCAT('<input type=checkbox name=HomeworkID[] value=', a.HomeworkID ,' onClick=\"unset_checkall(this, this.form);\">')";
		}
		else {
			$fields .= ", IF('".($lhomework->ClassTeacherCanViewHomeworkOnly)."'=0 OR '".($lhomework->ClassTeacherCanViewHomeworkOnly)."'='' OR stct.SubjectGroupID IS NOT NULL, CONCAT('<input type=checkbox name=HomeworkID[] value=', a.HomeworkID ,' onClick=\"unset_checkall(this, this.form);\">'),'-')";
		}
		
		$dbtables = "INTRANET_HOMEWORK as a LEFT OUTER JOIN ASSESSMENT_SUBJECT as b ON b.RecordID = a.SubjectID
					LEFT OUTER JOIN SUBJECT_TERM_CLASS as c ON c.SubjectGroupID = a.ClassGroupID";
		if($lhomework->useHomeworkType) {
			$dbtables .= " LEFT OUTER JOIN INTRANET_HOMEWORK_TYPE e ON (e.TypeID=a.TypeID) ";
		}
		
		# Do not map by user, should allow for same subject group
		$conds .= " a.AcademicYearID = '$yearID' $date_conds";
		$conds .= ($yearTermID=='')? "" : " AND a.YearTermID = '$yearTermID'";
		
		if($classID!="")
		{
			$dbtables .= " LEFT OUTER JOIN SUBJECT_TERM_CLASS_USER F ON (F.SubjectGroupID=c.SubjectGroupID)
							LEFT OUTER JOIN YEAR_CLASS_USER ycu ON (ycu.UserID=F.UserID AND ycu.YearClassID = '$classID')";
			$conds .= " AND ycu.YearClassID = '$classID'";
		}
        
		$sql = "SELECT $fields FROM $dbtables LEFT JOIN SUBJECT_TERM_CLASS_TEACHER stct ON (stct.SubjectGroupID=a.ClassGroupID AND stct.UserID = '$UserID') WHERE $conds GROUP BY a.HomeworkID";
		$li->field_array = array("Subject", "SubjectGroup");
		if($lhomework->useHomeworkType)
		{
			$li->field_array[] = "TypeName";	
		}
		$li->field_array = array_merge($li->field_array, array("a.Title", "a.StartDate", "a.DueDate", "a.Loading", "a.HandinRequired"));
		$li->sql = $sql;
		$li->no_col = sizeof($li->field_array)+2;
		if($lhomework->useHomeworkCollect)
		{
			$li->no_col++;
		}
		$li->IsColOff = 2;
        
		# TABLE COLUMN
		$pos = 0;
		$li->column_list .= "<td width='5%'>#</td>\n";
		$li->column_list .= "<td width='10%' class='tabletop tabletoplink'>".$li->column($pos++, $Lang['SysMgr']['Homework']['Subject'])."</td>\n";
		$li->column_list .= "<td width='20%' class='tabletop tabletoplink'>".$li->column($pos++, $Lang['SysMgr']['Homework']['SubjectGroup'])."</td>\n";
		if($lhomework->useHomeworkType)
		{
			$li->column_list .= "<td width='20%' class='tabletop tabletoplink'>".$li->column($pos++, $i_Homework_HomeworkType)."</td>\n";
		}
		$li->column_list .= "<td width='20%' class='tabletop tabletoplink'>".$li->column($pos++, $Lang['SysMgr']['Homework']['Topic'])."</td>\n";
		$li->column_list .= "<td width='12%' class='tabletop tabletoplink'>".$li->column($pos++, $Lang['SysMgr']['Homework']['StartDate'])."</td>\n";
		$li->column_list .= "<td width='12%' class='tabletop tabletoplink'>".$li->column($pos++, $Lang['SysMgr']['Homework']['DueDate'])."</td>\n";
		$li->column_list .= "<td width='13%' class='tabletop tabletoplink'>".$li->column($pos++, $Lang['SysMgr']['Homework']['Workload']." (".$Lang['SysMgr']['Homework']['Hours'].")")."</td>\n";
		$li->column_list .= "<td width='12%' class='tabletop tabletoplink'>".$li->column($pos++, $Lang['SysMgr']['Homework']['HandinRequired'])."</td>\n";
		if($lhomework->useHomeworkCollect)
		{
			$li->column_list .= "<td width='12%' class='tabletop tabletoplink' nowrap>".$li->column($pos++, $i_Homework_Collected_By_Class_Teacher)."</td>\n";
		}
		$li->column_list .= "<td width='12%' class='tabletop tabletopnolink'>".$li->check("HomeworkID[]")."</td>\n";
	}
	# Teacher Mode with Non Teaching
	else if($_SESSION['UserType']==USERTYPE_STAFF && !$_SESSION['isTeaching'])
	{
		# Academic Year
		$academicYear = $lhomework->GetAllAcademicYear();
		$yearID = ($yearID=="")? Get_Current_Academic_Year_ID() : $yearID;
        
		$selectedYear = $lhomework->getCurrentYear("name=\"yearID\" onChange=\"reloadForm()\"", $academicYear, $yearID, "", false);
		if($yearID != "")
		{
			if($yid != "" && $yid != $yearID) {
				$yearTermID = "";
			}
            
			# Year Term
			$academicYearTerm = $lhomework->getAllAcademicYearTerm($yearID);
			$selectedYearTerm = $lhomework->getCurrentYearTerm("name=\"yearTermID\" onChange=\"reloadForm()\"", $academicYearTerm, $yearTermID, $Lang['SysMgr']['Homework']['AllYearTerms']);
			
			if($ytid != "" && $ytid != $yearTermID) {
				$subjectID = "";
			}
            
			# Subject Menu
			$subjectID = ($subjectID=="")? -1 : $subjectID;
			$subject = $lhomework->getTeachingSubjectList("", $yearID, $yearTermID, $classID);
			$selectedSubject = $lhomework->getCurrentTeachingSubjects("name=\"subjectID\" onChange=\"reloadForm()\"", $subject, $subjectID, $Lang['SysMgr']['Homework']['AllSubjects']);
			
			if ($subjectID != "" && $yearTermID != "" && $yearTermID > 0)
			{
				$sgid = $subjectGroupID;
				if($sid != $subjectID) {
					$subjectGroupID = "";
				}
                
				# Subject Group Menu
				$subjectGroups = $lhomework->getTeachingSubjectGroupList("", $subjectID, $yearID, $yearTermID, $classID);
				$subjectGroupID = resetSubjectGroupID($subjectGroupID, $subjectGroups);
				$selectedSubjectGroups = $lhomework->getCurrentTeachingGroups("name=\"subjectGroupID\" onChange=\"reloadForm()\"", $subjectGroups, $subjectGroupID, $Lang['SysMgr']['Homework']['AllSubjectGroups']);
			}
			else
			{
				$subjectGroupID = "";
			}
            
			$filterbar = "$selectedYear $selectedYearTerm $selectClass $selectedSubject $selectedSubjectGroups";
		}
		$exportLink = "export.php?yearID=$yearID&yearTermID=$yearTermID&classID=$classID&subjectID=$subjectID&subjectGroupID=$subjectGroupID&s=$s&flag=1";
		/*
		if($allowExport)
			$toolbar .= $linterface->GET_LNK_EXPORT($exportLink, "", "", "", "", 0);
		*/
		
		# SQL statement
		if(sizeof($subjectGroups) != 0)
		{
			$allGroups = " a.ClassGroupID IN (";
			for ($i=0; $i < sizeof($subjectGroups); $i++)
			{
				list($groupID) = $subjectGroups[$i];
				$allGroups .= $groupID.",";
			}
			$allGroups = substr($allGroups,0,strlen($allGroups)-1).")";
			$allGroups .= " AND";
		}
		else
		{
			$allGroups = "";
			$subjectGroupID = "";
			$sbjGps = $lhomework->getTeachingSubjectGroupList("", $subjectID, $yearID, $yearTermID, $classID);
			if(sizeof($sbjGps) > 0)
			{
				for($i=0; $i<sizeof($sbjGps); $i++) {
					$temp[$i] = $sbjGps[$i][0];
				}
				$allGroups .= " a.ClassGroupID IN (".implode(',',$temp).") AND ";
			}
		}
        
		# SQL statement
		$date_conds = "a.DueDate < CURDATE()";
		$conds = ($subjectGroupID=='' || $subjectGroupID==-1) ? "$allGroups" : " a.ClassGroupID = '$subjectGroupID' AND";
		$conds .= ($s=='')? "" : " (IF('$intranet_session_language' = 'en', c.ClassTitleEN, c.ClassTitleB5) LIKE '%".$searchStringSQL."%'
									OR a.Title LIKE '%".$searchStringSQL."%'
									$searchByTeacher
									$searchBySubject
								) AND";
        
		$desc = "CONCAT(' <img src=\"$image_path/homework/$intranet_session_language/hw/icon_alt.gif\" title=\"',a.Description,'\" \>')";
		$attach = "CONCAT(' <img src=\"$image_path/homework/$intranet_session_language/hw/icon_attachment.gif\" title=\"$i_AnnouncementAttachment\" \>')";
		$handin = "CONCAT('<a href=\"javascript:viewHandinList(',a.HomeworkID,')\"><img src=\"$image_path/homework/$intranet_session_language/hw/btn_handin_list.gif\" border=\"0\" /></a>')";
		
		$fields = "IF('$intranet_session_language' = 'en', b.EN_DES, b.CH_DES) AS Subject,
				   IF('$intranet_session_language' = 'en', c.ClassTitleEN, c.ClassTitleB5) AS SubjectGroup,";
		if($lhomework->useHomeworkType) {
			$fields .= " IF(e.TypeName!='', e.TypeName, '---'), ";
		}
		$fields	.=  "CONCAT(CONCAT(CONCAT(CONCAT('<a class=tablelink href=javascript:viewHmeworkDetail(',a.HomeworkID,')>',a.Title,'</a>'), if (a.StartDate=CURDATE(),' $i_Homework_today','')), IF(a.Description='', '',$desc)), IF(a.AttachmentPath!='NULL', $attach, '')),
				   a.StartDate, a.DueDate, a.Loading/2, (if (a.HandinRequired=1, $handin ,'". $i_general_no."')), CONCAT('<input type=checkbox name=HomeworkID[] value=', a.HomeworkID ,' onClick=\"unset_checkall(this, this.form);\">')";
        
		$dbtables = "INTRANET_HOMEWORK as a LEFT OUTER JOIN ASSESSMENT_SUBJECT as b ON b.RecordID = a.SubjectID
					 LEFT OUTER JOIN SUBJECT_TERM_CLASS as c ON c.SubjectGroupID = a.ClassGroupID";
		if($lhomework->useHomeworkType) {
			$dbtables .= " LEFT OUTER JOIN INTRANET_HOMEWORK_TYPE e ON (e.TypeID=a.TypeID) ";
		}
        
		$conds .= " a.AcademicYearID = '$yearID' AND $date_conds";
		$conds .= ($yearTermID=='')? "" : "AND a.YearTermID = '$yearTermID'";
        
		$sql = "SELECT $fields FROM $dbtables WHERE $conds GROUP BY a.HomeworkID";
// 		$li->field_array = array("Subject", "SubjectGroup", "a.Title", "a.StartDate", "a.DueDate", "a.Loading", "a.HandinRequired");
		$li->field_array = array();
		$li->field_array[] = "Subject";
		$li->field_array[] = "SubjectGroup";
		if($lhomework->useHomeworkType)
		{
			$li->field_array[] = "TypeName";
		}
		$li->field_array[] = "a.Title";
		$li->field_array[] = "a.StartDate";
		$li->field_array[] = "a.DueDate";
		$li->field_array[] = "a.Loading";
		$li->field_array[] = "a.HandinRequired";
		$li->sql = $sql;
		$li->no_col = sizeof($li->field_array)+2;
		$li->IsColOff = 2;
        
		# TABLE COLUMN
		$pos = 0;
		$li->column_list .= "<td width='5%'>#</td>\n";
		$li->column_list .= "<td width='10%' class='tabletop tabletoplink'>".$li->column($pos++, $Lang['SysMgr']['Homework']['Subject'])."</td>\n";
		$li->column_list .= "<td width='20%' class='tabletop tabletoplink'>".$li->column($pos++, $Lang['SysMgr']['Homework']['SubjectGroup'])."</td>\n";
		if($lhomework->useHomeworkType)
		{
			$li->column_list .= "<td width='20%' class='tabletop tabletoplink'>".$li->column($pos++, $i_Homework_HomeworkType)."</td>\n";
		}
		$li->column_list .= "<td width='20%' class='tabletop tabletoplink'>".$li->column($pos++, $Lang['SysMgr']['Homework']['Topic'])."</td>\n";
		$li->column_list .= "<td width='12%' class='tabletop tabletoplink'>".$li->column($pos++, $Lang['SysMgr']['Homework']['StartDate'])."</td>\n";
		$li->column_list .= "<td width='12%' class='tabletop tabletoplink'>".$li->column($pos++, $Lang['SysMgr']['Homework']['DueDate'])."</td>\n";
		$li->column_list .= "<td width='13%' class='tabletop tabletoplink'>".$li->column($pos++, $Lang['SysMgr']['Homework']['Workload']." (".$Lang['SysMgr']['Homework']['Hours'].")")."</td>\n";
		$li->column_list .= "<td width='12%' class='tabletop tabletoplink'>".$li->column($pos++, $Lang['SysMgr']['Homework']['HandinRequired'])."</td>\n";
		$li->column_list .= "<td width='12%' class='tabletop tabletopnolink'>".$li->check("HomeworkID[]")."</td>\n";
	}
	# Subject Leader
	else if($_SESSION['UserType']==USERTYPE_STUDENT)
	{
		# Academic Year
		$academicYear = $lhomework->GetAllAcademicYear();
		$yearID = ($yearID=="")? Get_Current_Academic_Year_ID() : $yearID;
        
		$selectedYear = $lhomework->getCurrentYear("name=\"yearID\" onChange=\"reloadForm()\"", $academicYear, $yearID, "", false);
		if($yearID != "")
		{
			if($yid != "" && $yid != $yearID)
			{
				$yearTermID = "";
			}
            
			# Year Term
			$academicYearTerm = $lhomework->getAllAcademicYearTerm($yearID);
			$selectedYearTerm = $lhomework->getCurrentYearTerm("name=\"yearTermID\" onChange=\"reloadForm()\"", $academicYearTerm, $yearTermID, $Lang['SysMgr']['Homework']['AllYearTerms']);

			if($ytid != "" && $ytid != $yearTermID){
				$subjectID = "";
			}
            
			# Subject Menu
			$subjectID = ($subjectID=="")? -1 : $subjectID;
			$subject = $lhomework->getStudyingSubjectList($UserID, 1, $yearID, $yearTermID);
			$selectedSubject = $lhomework->getCurrentStudyingSubjects("name=\"subjectID\" onChange=\"this.form.method='get'; this.form.action=''; this.form.submit();\"", $subject, $subjectID, $Lang['SysMgr']['Homework']['AllSubjects']);
            
			$filterbar = "$selectedYear $selectedYearTerm $selectedSubject";
		}
		
		# SQL statement
		if(sizeof($subject) != 0)
		{
			$allSubjects = " a.SubjectID IN (";
			for ($i=0; $i < sizeof($subject); $i++)
			{
				list($ID) = $subject[$i];
				$allSubjects .= $ID.",";
			}
			$allSubjects = substr($allSubjects,0,strlen($allSubjects)-1).")";
			$allSubjects .= " AND";
		}
		else
		{
			$allSubjects = "";
		}
		
		# Set conditions
		$date_conds = "AND a.DueDate < CURDATE()";
		$conds = ($subjectID=='-1')? "$allSubjects" : " a.SubjectID = '$subjectID' AND";
		$conds .= ($s=='')? "" : " (IF('$intranet_session_language' = 'en', c.ClassTitleEN, c.ClassTitleB5) LIKE '%".$searchStringSQL."%'
									OR a.Title LIKE '%".$searchStringSQL."%'
									$searchByTeacher
									$searchBySubject
								) AND";
        
		$desc = "CONCAT(' <img src=\"$image_path/homework/$intranet_session_language/hw/icon_alt.gif\" title=\"',a.Description,'\" \>')";
		$attach = "CONCAT(' <img src=\"$image_path/homework/$intranet_session_language/hw/icon_attachment.gif\" title=\"$i_AnnouncementAttachment\" \>')";
		$handin = "CONCAT('<a href=\"javascript:viewHandinList(',a.HomeworkID,')\"><img src=\"$image_path/homework/$intranet_session_language/hw/btn_handin_list.gif\" border=\"0\" /></a>')";
		
		$fields = "IF('$intranet_session_language' = 'en', b.EN_DES, b.CH_DES) AS Subject,
				   IF('$intranet_session_language' = 'en', c.ClassTitleEN, c.ClassTitleB5) AS SubjectGroup,
				   CONCAT(CONCAT(CONCAT(CONCAT('<a class=tablelink href=javascript:viewHmeworkDetail(',a.HomeworkID,')>',a.Title,'</a>'), if (a.StartDate=CURDATE(),' $i_Homework_today','')), IF(a.Description='', '',$desc)), IF(a.AttachmentPath!='NULL', $attach, '')),
				   a.StartDate, a.DueDate, a.Loading/2, (if (a.HandinRequired=1, $handin ,'". $i_general_no."'))";
        
		$dbtables = "INTRANET_HOMEWORK as a LEFT OUTER JOIN ASSESSMENT_SUBJECT as b ON b.RecordID = a.SubjectID
					 LEFT OUTER JOIN SUBJECT_TERM_CLASS as c ON c.SubjectGroupID = a.ClassGroupID
					 LEFT OUTER JOIN SUBJECT_TERM_CLASS_USER AS d ON d.SubjectGroupID = a.ClassGroupID
					 LEFT OUTER JOIN INTRANET_HOMEWORK_HANDIN_LIST AS e ON (e.HomeworkID = a.HomeworkID AND e.StudentID = d.UserID)";
        
		$conds .= " d.UserID = '$UserID' AND a.AcademicYearID = '$yearID' $date_conds";
		$conds .= ($yearTermID=='')? "" : "AND a.YearTermID = '$yearTermID'";
        
		$sql = "SELECT $fields FROM $dbtables WHERE $conds";
		$li->field_array = array("Subject", "SubjectGroup", "a.Title", "a.StartDate", "a.DueDate", "a.Loading", "a.HandinRequired");
		$li->sql = $sql;
		$li->no_col = sizeof($li->field_array)+1;
		$li->IsColOff = 2;
        
		# TABLE COLUMN
		$pos = 0;
		$li->column_list .= "<td width='5%'>#</td>\n";
		$li->column_list .= "<td width='10%' class='tabletop tabletoplink'>".$li->column($pos++, $Lang['SysMgr']['Homework']['Subject'])."</td>\n";
		$li->column_list .= "<td width='20%' class='tabletop tabletoplink'>".$li->column($pos++, $Lang['SysMgr']['Homework']['SubjectGroup'])."</td>\n";
		$li->column_list .= "<td width='20%' class='tabletop tabletoplink'>".$li->column($pos++, $Lang['SysMgr']['Homework']['Topic'])."</td>\n";
		$li->column_list .= "<td width='12%' class='tabletop tabletoplink'>".$li->column($pos++, $Lang['SysMgr']['Homework']['StartDate'])."</td>\n";
		$li->column_list .= "<td width='12%' class='tabletop tabletoplink'>".$li->column($pos++, $Lang['SysMgr']['Homework']['DueDate'])."</td>\n";
		$li->column_list .= "<td width='13%' class='tabletop tabletoplink'>".$li->column($pos++, $Lang['SysMgr']['Homework']['Workload']." (".$Lang['SysMgr']['Homework']['Hours'].")")."</td>\n";
		$li->column_list .= "<td width='12%' class='tabletop tabletoplink'>".$li->column($pos++, $Lang['SysMgr']['Homework']['HandinRequired'])."</td>\n";
        
		$exportLink = "export.php?yearID=$yearID&yearTermID=$yearTermID&classID=$classID&subjectID=$subjectID&subjectGroupID=$subjectGroupID&s=$s&flag=1";
	}
    
	# export link
	$exportLink .= "&noRecord=$noRecord";
	# 20191211 Philips - Only show handin required
	$exportLink .= "&handinRequired=1";
	if($allowExport)
	{
// 		$generalExport = $linterface->GET_LNK_EXPORT($exportLink, $Lang['eHomework']['NormalExport'], "", "", "", 0);
// 		$handinHistoryExport = $linterface->GET_LNK_EXPORT("javascript:showSpan('spanExport')", $Lang['eHomework']['ExportHandinHistory'], "", "", "", 0);
		$generalExport = "<a href=\"$exportLink\"  class=\"sub_btn\"> ".$Lang['eHomework']['NormalExport']."</a>";
		$handinHistoryExport = "<a href=\"javascript:;\" onClick=\"showSpan('spanExport')\" class=\"sub_btn\"> ".$Lang['eHomework']['ExportHandinHistory']."</a>";
		$toolbar = '
				<div class="btn_option"  id="ExportDiv"  >
					<a onclick="js_Clicked_Option_Layer(\'export_option\', \'btn_export\');" id="btn_export" class="export option_layer" href="javascript:void(0);"> '.$Lang['Btn']['Export'].'</a>
					<br style="clear: both;">
					<div onclick="js_Clicked_Option_Layer_Button(\'export_option\', \'btn_export\');" id="export_option" class="btn_option_layer" style="visibility: hidden;">
						'.$generalExport.'
						'.$handinHistoryExport.'
					</div>
				</div>';
	}
	
	/* change to library function
	$TAGS_OBJ[] = array($Lang['SysMgr']['Homework']['ToDoList'], "index.php?subjectID=$subjectID&subjectGroupID=$sgid&sid=$sid", 0);
	$TAGS_OBJ[] = array($Lang['SysMgr']['Homework']['History'], "history.php?yearID=$yearID&yearTermID=$yearTermID&subjectID=$subjectID&subjectGroupID=$sgid&sid=$sid", 1);
	if($_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] && !$sys_custom['eHomework']['HideClearHomeworkRecords'] )
		$TAGS_OBJ[] = array($Lang['SysMgr']['Homework']['ClearHomework'], "clear_homework.php", 0);
	*/
	$query_str = "yearID=" . $yearID . "&yearTermID=" . $yearTermID . "&subjectID=" . $subjectID . "&subjectGroupID=" . $sgid . "&sid=" . $sid;
	$TAGS_OBJ = $lhomework->getHomeworkListTabs(basename(__FILE__), $query_str);
	
	//$htmlAry['searchBox'] = $linterface->Get_Search_Box_Div('text', stripslashes(stripslashes($s)));
	$htmlAry['searchBox'] = '';
	$htmlAry['searchBox'] .= '<div class="Conntent_search">';
	   $htmlAry['searchBox'] .= '<input id="text" name="text" type="text" value="'.$s.'"/>';
	$htmlAry['searchBox'] .= '</div>';
	
	$ssql = "SELECT MAX(a.DueDate) AS LATESTDUE FROM $dbtables WHERE $conds";
	$maxDate = $lhomework->returnVector($ssql);
	if(!empty($maxDate)){
		$endDate = $maxDate[0];
		$startDate = date('Y-m-d', strtotime($endDate .' -90 days')); // Default 90 days before
	}
	
	# Start layout
	$linterface->LAYOUT_START();
?>

<script language="javascript">
    $(document).ready( function()
    {
    	$('input#text').keydown( function(evt) {
    		if (Check_Pressed_Enter(evt)) {
    			// pressed enter
    			goSearch();
    		
    		}
    	});
    });
	
	function removeCat(obj,element,page)
	{
		var alertConfirmRemove = "<?=$i_Discipline_System_alert_remove_record?>";
        if(countChecked(obj,element)==0) {
            alert(globalAlertMsg2);
        }
        else {
            if(confirm(alertConfirmRemove)) {
                obj.action=page;
                obj.method="post";
                obj.submit();
            }
        }
	}
	
	function reloadForm() {
		document.form1.s.value = "";
		document.form1.pageNo.value = 1;
		document.form1.submit();
	}
	
	function goSearch() {
		document.form1.s.value = document.form2.text.value;
		document.form1.pageNo.value = 1;
		document.form1.submit();
	}
	
	function viewHmeworkDetail(id)
	{
	   newWindow('./view.php?hid='+id,1);
	}
	
	function viewHandinList(id)
	{
	   newWindow('./handin_list.php?hid='+id,10);
	}

	function doExport() {
		var startDate = new Date($('#startDate').val());
		var endDate = new Date($('#endDate').val());
		var diffTime = startDate.getTime() - endDate.getTime();
		var diffDay = diffTime / ( 1000 * 60 * 60 * 24); // Divide by milisecond of day
		if( diffDay >= -90 && diffDay <= 90){
			window.open('<?=str_replace('export.php', 'export_handinHistory.php',$exportLink)?>&startDate='+$('#startDate').val()+'&endDate='+$('#endDate').val(), '_blank');
		} else {
			alert('<?=$Lang['eHomework']['ExportPeriodWithin90Days']?>');
		}
	}

	function showSpan(layername) {
		document.getElementById(layername).style.visibility = "visible";
	}

	function hideSpan(layername) {
		document.getElementById(layername).style.visibility = "hidden";
	}
</script>

<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
			<form name="form2" method="post" onSubmit="return false">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td>
						<table border="0" cellspacing="0" cellpadding="2">
							<tr>
								<td class="Conntent_tool">
									<?=$toolbar ?>
									<!-- Export Span -->
									<div class='print_option_layer' id='spanExport' style="width:250px; left:220px">
				                        	<em > - <?=$Lang['General']['ExportOptions']?> -</em>
				                          	<table id='export_period_table' class='form_table'>
			                            		<col class='field_title' />
			                             		<col class='field_c' />
			                             		<tr>
			                             			<td colspan='3'><em > - <?=$Lang['eDiscipline']['Period']?> -</em></td>
			                             		</tr>
			                            		<tr>
				                             		<td width='22%'><?=$i_From?></td>
				                              		<td>:</td>
				                              		<td><?=$linterface->GET_DATE_PICKER("startDate",$startDate)?></td>
				                            	</tr>
				                            	<tr>
				                              		<td><?=$i_To?></td>
				                              		<td>:</td>
				                              		<td><?=$linterface->GET_DATE_PICKER("endDate",$endDate)?></td>
				                            	</tr>
				                          	</table>
				                          	<div class='edit_bottom'>
				                            	<p class='spacer'></p>
				                            	<input type="button" name="button1" id="button1" value="<?=$button_submit?>" onClick="doExport()">
				                            	<input type="button" name="button2" id="button2" value="<?=$button_cancel?>" onClick="hideSpan('spanExport')">
				                            	<p class='spacer'></p>
				                          	</div>
				                        </div>
								</td>
							</tr>
						</table>
					</td>
					<td align="right"> 
						<?= $htmlAry['searchBox']?>
				        <!-- <input name="text" type="text" class="formtextbox" value="<?=stripslashes(stripslashes($s))?>">
						<?=$linterface->GET_BTN($button_find, "button","javascript:goSearch()");?> -->
					</td>
				</tr>
			</table>
			</form>
			
			<form name="form1" method="post" action="">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td align="right">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td height="28" align="right" valign="bottom">
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td align="right"><?= $linterface->GET_SYS_MSG($msg) ?>
											</td>
										</tr>
									</table>
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td height="30">
												<?=$filterbar?>
											</td>
											<td align="right" valign="middle" class="thumb_list"><?=$choiceType?></td>
										</tr>
									</table>
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td>
											</td>
											<td align="right" valign="bottom">
    											<br/>
    											<br/>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr class="table-action-bar">
								<td>
								</td>
								<td align="right" valign="bottom">
									<? 
									if ((!$lhomework->ViewOnly && ($_SESSION['UserType']==USERTYPE_STAFF || $_SESSION["SSV_PRIVILEGE"]["homework"]["is_subject_leader"])) || $_SESSION['SSV_PRIVILEGE']['homework']['is_subject_leader'] || ($_SESSION['UserType']==USERTYPE_STAFF && !$_SESSION['isTeaching']))
									{
									?>
										<table border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td width="21"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_01.gif" width="21" height="23"></td>
												<td background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_02.gif">
													<table border="0" cellspacing="0" cellpadding="2">
														<tr>
															<td nowrap><a href="javascript:checkEdit(document.form1,'HomeworkID[]','edit.php')" class="tabletool" title="<?= $button_edit ?>"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_edit.gif" width="12" height="12" border="0" align="absmiddle"> <?=$button_edit ?></a></td>
															<td><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="5"></td>
															<td nowrap><a href="javascript:removeCat(document.form1,'HomeworkID[]','remove_update.php?subjectID=<?=$subjectID?>&subjectGroupID=<?=$subjectGroupID?>')" class="tabletool" title="<?= $button_delete ?>"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_delete.gif" width="12" height="12" border="0" align="absmiddle"> <?= $button_delete ?></a></td>
														</tr>
													</table>
												</td>
												<td width="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_03.gif" width="6" height="23"></td>
											</tr>
										</table>
									<? 
									}
									else
									{
									?>
										<br/>
										<br/>
									<? } ?>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>
						<?=$li->display()?>
					</td>
				</tr>
			</table><br>
			
			<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>"/>
			<input type="hidden" name="order" value="<?php echo $li->order; ?>"/>
			<input type="hidden" name="field" value="<?php echo $li->field; ?>"/>
			<input type="hidden" name="numPerPage" value="<?=$li->page_size?>"/>
			<input type="hidden" name="page_size_change" value=""/>
			<input type="hidden" name="s" value="<?=$s?>"/>
			<!--input type="hidden" name="cid" value="<?=$childrenID?>"/-->
			<input type="hidden" name="yid" value="<?=$yearID?>"/>
			<input type="hidden" name="ytid" value="<?=$yearTermID?>"/>
			<input type="hidden" name="sid" value="<?=$subjectID?>"/>
			<input type="hidden" name="history" value="1"/>
			</form>
		</td>
	</tr>
</table>

<?
	$linterface->LAYOUT_STOP();
	intranet_closedb();
?>