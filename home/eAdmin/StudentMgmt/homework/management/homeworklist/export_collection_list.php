<?php
# using:
##### Change Log [Start] #####
#
#	Date	:	2020-09-22 (Cameron)
#				- make access right be the same as export.php
#
#	Date	:	2010-12-01 (Henry Chow)
#				change the export format to IP25 standard (with 2 rows of heading)
#
###### Change Log [End] ######

$PATH_WRT_ROOT = "../../../../../../";
$CurrentPageArr['eAdminHomework'] = 1;

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$lhomework = new libhomework2007();

//if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || !($_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"] || $_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] && !$lhomework->isViewerGroupMember($UserID)))
if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || (!$_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"] && !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] && !$lhomework->isViewerGroupMember($UserID)))

{
	if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])
	{
		header("location: ../../settings/index.php");	
		exit;
	}
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}



$lexport = new libexporttext();

$filename = "Homework_collection_list_$form1ExportDate.csv";

$searchByTeacher =($lhomework->teacherSearchDisabled == 0) ? "or h.PosterName like '%$s%'" : "" ;
$searchBySubject =($lhomework->subjectSearchDisabled == 0) ? " or IF('$intranet_session_language' = 'en', SUB.EN_DES, SUB.CH_DES) like '%$s%'" : "" ;

$ExportArr = array();

$yearID = Get_Current_Academic_Year_ID();

$allSemesters = getSemesters($yearID);

$yearTermID = getCurrentSemesterID();

$yearTerm = ($yearTermID=="")? " AND (h.YearTermID=".GetCurrentSemesterID()." OR h.DueDate>=CURDATE())" : "AND (h.YearTermID = '$yearTermID' OR h.DueDate>=CURDATE())";

$conds = " h.AcademicYearID = $yearID AND h.DueDate >= CURDATE() $yearTerm";


if($_SESSION['UserType']==USERTYPE_STUDENT) {	# subject leader

	# subject leader of subject group
	$sql = "SELECT ClassID FROM INTRANET_SUBJECT_LEADER WHERE UserID='$UserID'";
	$sbjGp1 = $lhomework->returnVector($sql);
	
	# student in subject group
	$sql = "SELECT distinct t.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER u LEFT OUTER JOIN SUBJECT_TERM t ON (t.SubjectGroupID=u.SubjectGroupID) WHERE u.UserID='$UserID' AND t.YearTermID=".getCurrentSemesterID();
	$sbjGp2 = $lhomework->returnVector($sql);
	
	$sbjGpID = (sizeof($sbjGp1)>0 && sizeof($sbjGp2)>0) ? array_merge($sbjGp1, $sbjGp2) : ((sizeof($sbjGp1)==0) ? $sbjGp2 : $sbjGp1); 
	
	if(sizeof($sbjGpID)>0) {
		$conds .= " AND h.ClassGroupId IN (".implode(',',$sbjGpID).")";
	}


} else {	# teacher

	if($subjectID != '' && $subjectID!='-1'){

		if ($subjectGroupID != '')
		{
			$conds .= " AND h.ClassGroupID = '$subjectGroupID'";
		}
	
		else{

			if($UserID!='') {
				//echo $subjectID; 
				$subjectGroups = $lhomework->getTeachingSubjectGroupList($UserID, $subjectID, $yearID, "", $classID);
			}
			else {
				$subjectGroups = $lhomework->getTeachingSubjectGroupList("", $subjectID, $yearID, "", $classID);
			}
//debug_pr($subjectGroups);
			if(sizeof($subjectGroups)!=0){

					$allGroups = " AND h.ClassGroupID IN (";
					for ($i=0; $i < sizeof($subjectGroups); $i++)
					{	
						list($groupID)=$subjectGroups[$i];
						$allGroups .= $groupID.",";
					}
					$allGroups = substr($allGroups,0,strlen($allGroups)-1).")";
			} else if($noRecord==1) {
				$allGroups = " AND h.ClassGroupId IN ('') ";	
			} else{
				$allGroups ="";
			}
			
			$conds .= $allGroups;
		}

	} else {
		//if($_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"]) {
		
			$subjectGroups = $lhomework->getTeachingSubjectGroupList($UserID,$subjectID, $yearID, "", $classID);

			if(sizeof($subjectGroups)!=0){
				$delim = "";
				for($i=0; $i<sizeof($subjectGroups); $i++) {
					list($gid, $gname) = $subjectGroups[$i];
					
					$allGroups .= $delim.$gid;
					$delim = ",";
				}
				$conds .= " AND h.ClassGroupID IN ($allGroups)";
			}	
		//}

	}
}



if ($s != '')
{
	$conds .= " AND (
				h.Title like '%$s%' 
				$searchByTeacher 
				$searchBySubject
			)";
}

$conds .= " AND h.DueDate='$form1ExportDate'";

# Select Data from DB
$row = $lhomework->exportHomeworkCollectionList($conds, $classID);
//debug_pr($row); exit;
$HomeworkIDAry = array();
$HomeworkInfo = array();
$StudentIDAry = array();
$StudentInfo = array();

for($i=0; $i<sizeof($row); $i++) {
	list($homeworkID, $classNameNum, $name, $subjectGroupName, $title, $handin, $studentID, $sgID) = $row[$i];
	if(!in_array($homeworkID, $HomeworkIDAry)) {
		$HomeworkIDAry[] = $homeworkID;
		$HomeworkInfo[$homeworkID]['SubjectGroupName'] = $subjectGroupName;
		$HomeworkInfo[$homeworkID]['SubjectGroupID'] = $sgID;
		$HomeworkInfo[$homeworkID]['HomeworkTitle'] = $title;
		$HomeworkInfo[$homeworkID]['HandinRequired'] = $handin;
	}
	if(!in_array($studentID, $StudentIDAry)) {
		$StudentIDAry[] = $studentID;
		$StudentInfo[$studentID]['ClassName'] = $classNameNum;
		$StudentInfo[$studentID]['StudentName'] = $name; 
	} 
}

$homeworkSubmitStatusList = $lhomework->getHomeworkSubmitStatusByHomeworkID($HomeworkIDAry);


if(sizeof($HomeworkInfo)) {
	$total = array();
	$m = 2;
	$r = 0;		# row position
	# 1st cell
	$ExportArr[0][0] = "";
	$ExportArr[0][1] = "";
	$ExportArr[1][0] = $i_general_class;
	$ExportArr[1][1] = $i_UserStudentName;
	
	# 1st row
	for($i=0; $i<sizeof($HomeworkIDAry); $i++) {
		$homeworkid = $HomeworkIDAry[$i];
		$ExportArr[$r][$m] = $HomeworkInfo[$homeworkid]['SubjectGroupName'];	
		$ExportArr[$r+1][$m] = $HomeworkInfo[$homeworkid]['HomeworkTitle'];	
		$m++;
	}
	$r++;
	$r++;
	
	# special homework status ("Late submitted" & "No need submit")
	$homeworkStatus['1'] = $Lang['SysMgr']['Homework']['Submitted'];
	$homeworkStatus['2'] = $Lang['SysMgr']['Homework']['Submitted'];
	$homeworkStatus['4'] = "---";
	
	for($i=0; $i<sizeof($StudentIDAry); $i++) {
		$studentid = $StudentIDAry[$i];
		$c = 0;		# column position
		$ExportArr[$r][$c]	= $StudentInfo[$studentid]['ClassName'];
		$c++;
		$ExportArr[$r][$c]	= $StudentInfo[$studentid]['StudentName']; 
		$c++;
		for($j=0; $j<sizeof($HomeworkIDAry); $j++) {
			$homeworkid = $HomeworkIDAry[$j];
			$groupStudentAry = $lhomework->getStudentListBySubjectGroupID($HomeworkInfo[$homeworkid]['SubjectGroupID']);
			
			$ExportArr[$r][$c] = in_array($homeworkSubmitStatusList[$homeworkid][$studentid],array_keys($homeworkStatus)) ? $homeworkStatus[$homeworkSubmitStatusList[$homeworkid][$studentid]] : (($HomeworkInfo[$homeworkid]['HandinRequired']=="---" && in_array($studentid,$groupStudentAry)) ? "---" : ((sizeof($groupStudentAry)==0 || !in_array($studentid,$groupStudentAry)) ? "xxx" : $HomeworkInfo[$homeworkid]['HandinRequired']));
			
			$total[$homeworkid] += ($ExportArr[$r][$c]==$Lang['SysMgr']['Homework']['NotSubmitted']) ? 1 : 0;
			$c++;	
		}
		$r++;
		
	}
	//debug_pr($total);
	//exit;
	$c = 0;
	$ExportArr[$r][$c] = ""; $c++;
	$ExportArr[$r][$c] = $Lang['eHomework']['NoOfHomeworkCollect']; $c++;
	for($k=0; $k<sizeof($HomeworkIDAry); $k++) {
		$ExportArr[$r][$c] = $total[$HomeworkIDAry[$k]];
		$c++;
	}
}
if(sizeof($row)==0) {
	$ExportArr[0][0] = $i_no_record_exists_msg;	
}
//debug_pr($ExportArr);
/*
$i = 0;	
foreach($Lang['Homework']['ExportColumn']['EN'] as $col) {
	if($i<9) {		
		$exportColumn[0][] = $col;
		$i++;
	}
}

$i = 0;
foreach($Lang['Homework']['ExportColumn']['CH'] as $col) {
	if($i<9) {	
		$exportColumn[1][] = $col;	
		$i++;
	}
}
*/
if($intranet_session_language=="en") 
	$exportColumn[0][0] = $Lang['eHomework']['ExportCollectionList']." ".$Lang['RepairSystem']['on']." ".$form1ExportDate;
else
	$exportColumn[0][0] = $form1ExportDate." ".$Lang['eHomework']['ExportCollectionList'];
	
$exportColumn[1][0] = "";

$export_content .= $lexport->GET_EXPORT_TXT_WITH_REFERENCE($ExportArr, $exportColumn, "\t", "\r\n", "\t", 0, "11");


intranet_closedb();

// Output the file to user browser
$lexport->EXPORT_FILE($filename, $export_content);

?>
