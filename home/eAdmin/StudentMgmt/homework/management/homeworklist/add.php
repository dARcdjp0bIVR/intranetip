<?php
// Modifing by : 

########## Change Log
#
#	Date:	2017-09-27 (Bill)	[2017-0907-1414-16066]
#			Hide * next to Input Field - Class
#			Deploy: IPv10.1
#
#	Date:	2014-09-22 (YatWoon)
#			add flag checking $sys_custom['eHomework']['HideClearHomeworkRecords'] [Case#F68177]
#			Deploy: IPv10.1
#
#	Date:	2013-09-27	YatWoon
#			Enhanced: add checking for default click "Collection Required" [Case#2013-0819-1825-45169] ($sys_custom['eHomework']['CollectionRequiredDefaultOn'])
#
#	Date:	2010-11-19	YatWoon
#			Subject Group select option changes to multiple selection [wish list]
#
#	Date:	2010-10-08 Henry Chow
#			modified js function init(), pass more parameters to pre-load class, subject and subject group
#
#	Date:	2010-09-20 Henry Chow
#			only display the HomeworkType if RecordStatus=1
#
#	Date:	2010-09-16 YatWoon
#			update IP25 layout to standard and set the subject group to 1st group as default
#
###########################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$lhomework = new libhomework2007();

if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || (!$_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"]  && !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] && !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_subject_leader"] && ($_SESSION['isTeaching'] && $lhomework->ViewOnly)))
{
	if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])
	{
		header("location: ../../settings/index.php");	
		exit;
	}
	
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

# Temp assign memory of this page
ini_set("memory_limit", "150M");

$linterface = new interface_html();
$lu = new libuser($UserID); 

$CurrentPageArr['eAdminHomework'] = 1;
$CurrentPage = "Management_HomeworkList";
$PAGE_TITLE = $Lang['SysMgr']['Homework']['HomeworkList'];

# Tag Information
/*
$TAGS_OBJ[] = array($Lang['SysMgr']['Homework']['ToDoList'], "index.php?yearID=$yearID&yearTermID=$yearTermID&subjectID=$subjectID&subjectGroupID=$sgid&sid=$sid", 1);
$TAGS_OBJ[] = array($Lang['SysMgr']['Homework']['History'], "history.php?yearID=$yearID&yearTermID=$yearTermID&subjectID=$subjectID&subjectGroupID=$sgid&sid=$sid", 0);
if($_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] && !$sys_custom['eHomework']['HideClearHomeworkRecords'])
	$TAGS_OBJ[] = array($Lang['SysMgr']['Homework']['ClearHomework'], "clear_homework.php", 0);
*/
$query_str = "yearID=" . $yearID . "&yearTermID=" . $yearTermID . "&subjectID=" . $subjectID . "&subjectGroupID=" . $sgid . "&sid=" . $sid;
$TAGS_OBJ = $lhomework->getHomeworkListTabs(basename(__FILE__), $query_str);

$MODULE_OBJ = $lhomework->GET_MODULE_OBJ_ARR();

$PAGE_NAVIGATION[] = array($Lang['SysMgr']['Homework']['HomeworkList'], "index.php?yearID=$yearID&yearTermID=$yearTermID&subjectID=$subjectID&subjectGroupID=$subjectGroupID");
$PAGE_NAVIGATION[] = array($Lang['SysMgr']['Homework']['NewRecord'], "");

$sql = "SELECT TypeID, TypeName FROM INTRANET_HOMEWORK_TYPE WHERE RecordStatus=1 ORDER BY DisplayOrder";
$homeworkType = $lhomework->returnArray($sql, 2);
$selectHomeworkType = getSelectByArray($homeworkType, "name='typeID' id='typeID'", $typeID, 0, 0);

# Start Layout
$linterface->LAYOUT_START();
?>

<script language=javascript>
function reset_innerHtml()
{
	document.getElementById('div_subjectGroupID_error').innerHTML = "";
 	document.getElementById('div_topic_error').innerHTML = "";
 	document.getElementById('div_DateEnd_err_msg').innerHTML = "";
 	document.getElementById('div_DateStart_err_msg').innerHTML = "";
}

function checkform(obj)
{
	reset_innerHtml();
	var error_no=0;
	var focus_field='';
	
	/*
	if(obj.subjectGroupID.value=="") {
		alert("<?=$i_alert_pleaseselect.$Lang['SysMgr']['Homework']['SubjectGroup']?>");
		return false;
	}
	*/
	
	if (countOption(document.getElementById('subjectGroupID[]')) == 0) 
	{
		document.getElementById('div_subjectGroupID_error').innerHTML = '<font color="red"><?=$Lang['eHomework']['PleaseSelectSubjectGroup']?></font>';
		error_no++;
		focus_field = "title";
	}
	
	if(obj.title.value=="") 
	{
		document.getElementById('div_topic_error').innerHTML = '<font color="red"><?=$Lang['eHomework']['MissingTitle']?></font>';
		error_no++;
		focus_field = "title";
	}
	
	<? if (!$lhomework->pastInputAllowed) {   ?>  
    	if (compareDate(obj.startdate.value, "<?=date("Y-m-d");?>") < 0) 
    	{
	    	document.getElementById('div_DateStart_err_msg').innerHTML = '<font color="red"><?=$i_Homework_new_startdate_wrong?></font>';
			error_no++;
			if(focus_field=="")	focus_field = "startdate";
	    }
    <? } ?>
    
	if (compareDate(obj.startdate.value, obj.duedate.value) > 0) 
	{
		document.getElementById('div_DateEnd_err_msg').innerHTML = '<font color="red"><?=$i_Homework_new_duedate_wrong?></font>';
		error_no++;
		if(focus_field=="")	focus_field = "duedate";
	}
	
    if(error_no>0)
	{
		eval("obj." + focus_field +".focus();");
		return false;
	}
	else
	{
		return Big5FileUploadHandler();
	}
}

function add_upload_field(rows_to_add)
{
	objTable = document.getElementById("file_list");
	attach_size = parseInt(document.addhomework.attachment_size.value);
	for(i=0;i<rows_to_add;i++){
		attach_size++;
		row = objTable.insertRow(-1);
		cell = row.insertCell(0);
		x = '<input type=file class=file name="filea'+attach_size+'" size=30>'; 
		x+= '<input type=hidden name="hidden_userfile_name'+attach_size+'">';
		cell.innerHTML = x;
	}
	document.addhomework.attachment_size.value = attach_size;
}

function Big5FileUploadHandler()
{
	attach_size = parseInt(document.addhomework.attachment_size.value);
	for(i=0;i<=attach_size;i++){
		objFile = eval('document.addhomework.filea'+i);
		objUserFile = eval('document.addhomework.hidden_userfile_name'+i);
		if(objFile!=null && objFile.value!='' && objUserFile!=null){
			var Ary = objFile.value.split('\\');
			objUserFile.value = Ary[Ary.length-1];
		}
	}
	return true;
}

function back(yid, sid, sgid)
{
	window.location="index.php?yearID="+yid+"&subjectID="+sid+"&subjectGroupID="+sgid;
}

function init()
{
	$('#div_content').load(
		'ajax_loadSubjectGroupSelection.php', 
		{
			yearID: <?=$yearID?>,
			yearTermID: <?=$yearTermID?>,
			classID: '<?=$classID?>',
			subjectID: '<?=$subjectID?>',
			subjectGroupID: '<?=$subjectGroupID?>',
			skipMustSelectClass: 1
		},
		function (data){
			// 
		}); 
}

function js_change_class(flag)
{
	if(flag==1)
		document.getElementById('subjectID').selectedIndex = -1;
	//else
	//	document.getElementById('classID').selectedIndex = -1;
	
	$('#div_content').load(
		'ajax_loadSubjectGroupSelection.php', 
		{ 
			yearID: <?=$yearID?>,
			yearTermID: <?=$yearTermID?>,
			<? if(!$_SESSION["SSV_PRIVILEGE"]["homework"]["is_subject_leader"] || ($_SESSION['UserType']==USERTYPE_STUDENT && $_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"])) { ?>
				classID: document.addhomework.classID.value, 
			<? } ?>
			subjectID: document.getElementById('subjectID').value,
			skipMustSelectClass: 1
			//subjectID: document.addhomework.subjectID.value
			//subjectGroupID: document.addhomework.subjectGroupID[].value
		},
		function (data){
			// 
		}); 
}
</script>

<?php
        //if ($classID=="") $classID = 0;
        $today = date('Y-m-d');
        $timestamp = time();
        $date_time_array = getdate($timestamp);
        $hours = $date_time_array[ "hours"];
        $minutes = $date_time_array["minutes"];
        $seconds = $date_time_array[ "seconds"];
        $month = $date_time_array["mon"];
        $day = $date_time_array["mday"] + 7;
        $year = $date_time_array["year"];
		
        // use mktime to recreate the unix timestamp
        $timestamp =  mktime($hours, $minutes, $seconds ,$month, $day, $year);
		$pre_startdate = $today;
        $pre_duedate = date('Y-m-d',$timestamp);
		
		if($lhomework->startFixed)
		{
			$start = "<INPUT TYPE=\"text\" NAME=\"startdate\" SIZE=10 maxlength=10 value='$pre_startdate' readonly>";
		}
		else
		{
			$start = $linterface->GET_DATE_PICKER("startdate",$pre_startdate);
		}
		$default_upload_fields = 2;
?>

<?=$linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION)?>

<form name="addhomework" method="post" ACTION="add_update.php" enctype="multipart/form-data" onSubmit="return checkform(this)">

<div id="div_content">
	<table class="form_table_v30">
	<tr>
		<td><?=$Lang['General']['Loading']?></td>
	</tr>
	</table>
</div>

<table class="form_table_v30">
<? /* ?>
<tr>
	<td class="field_title"><span class="tabletextrequire">*</span> <?=$i_general_class?></td>
	<td><?=$filterbar3?></td>
</tr>

<tr>
	<td class="field_title"><span class="tabletextrequire">*</span> <?=$Lang['SysMgr']['Homework']['Subject']?> </td>
	<td><?=$filterbar?></td>
</tr>

<tr>
	<td class="field_title"><span class="tabletextrequire">*</span> <?=$Lang['SysMgr']['Homework']['SubjectGroup']?> </td>
	<td><?=$filterbar2?></td>
</tr>
<? */ ?>

<? if($lhomework->useHomeworkType) { ?>
<tr>
   	<td class="field_title"><?=$i_Homework_HomeworkType?></td>
	<td><?=$selectHomeworkType?></td>
</tr>
<? } ?>

<tr>
   <td class="field_title"><span class="tabletextrequire">*</span> <?=$Lang['SysMgr']['Homework']['Topic']?></td>
	<td><input type="text" name="title" MAXLENGTH="140" class="textboxtext"><span id="div_topic_error"></span></td>
</tr>

<tr>
	<td class="field_title"><?=$Lang['SysMgr']['Homework']['Description']?></td>
	<td><?=$linterface->GET_TEXTAREA("description","");?></td>
</tr>

<!-- Attachment -->
<tr>
	<td class="field_title"><?=$Lang['SysMgr']['Homework']['Attachment']?></td>
	<td>
		<input type="hidden" name="attachment_size"  value="0">
		<table id="file_list" border=0 cellpadding=0 cellpadding=0>
			<script language="javascript">
				add_upload_field(<?=$default_upload_fields?>);
			</script>
		</table>
		<input type="button" value=" + " onClick="add_upload_field(1)">
	</td>
</tr>
<!-- End of Attachment -->

<tr>
	<td class="field_title"><?=$Lang['SysMgr']['Homework']['Workload']?></td>
	<td><?=$lhomework->getSelectLoading("name=loading")?><?=$Lang['SysMgr']['Homework']['Hours']?></td>
</tr>

<tr>
	<td class="field_title"><span class="tabletextrequire">*</span> <?=$Lang['SysMgr']['Homework']['StartDate']?> </td>
	<td><?=$start?><br><span id="div_DateStart_err_msg"></span></td>
</tr>

<tr>
	<td class="field_title"><span class="tabletextrequire">*</span> <?=$Lang['SysMgr']['Homework']["DueDate"]?> </td>
	<td><?=$linterface->GET_DATE_PICKER("duedate",$pre_duedate)?><br><span id="div_DateEnd_err_msg"></span></td>
</tr>

<tr>	                      	
	<td class="field_title"><?=$Lang['SysMgr']['Homework']['HandinRequired']?></td>
	<td><input type="checkbox" name="handin_required" value=1 <?=($lhomework->DeafultHandinRequired? "checked":"")?>></td>						
</tr> 

<? if($lhomework->useHomeworkCollect)
{
	# check the checkbox default on or not ($sys_custom['eHomework']['CollectionRequiredDefaultOn'])
	$checked = $sys_custom['eHomework']['CollectionRequiredDefaultOn'] ? "checked" : "";
?>
<tr>
	<td class="field_title"><?=$i_Homework_Collect_Required?></td>
	<td><input type="checkbox" name="collect_required" value=1 <?=$checked ?>></td>						
</tr>
<? } ?>
</table>

<?=$linterface->MandatoryField();?>

<div class="edit_bottom_v30">
	<p class="spacer"></p>
	<?= $linterface->GET_ACTION_BTN($button_submit, "submit")?>&nbsp;
	<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "history.back()")?>
	<p class="spacer"></p>
</div>

<input type="hidden" name="yearID" value="<?=$yearID?>"/>
<input type="hidden" name="yearTermID" value="<?=$yearTermID?>"/>
<input type="hidden" name="sid" value="<?=$subjectID?>"/>
</form>

<script language="javascript">
<!--
init();
//-->
</script>

<?
	$linterface->LAYOUT_STOP();
	intranet_closedb();
?>