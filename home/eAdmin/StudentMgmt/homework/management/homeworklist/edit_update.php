<?php
# modifying by : 

########## Change Log
#
#	Date:	2019-05-08 Bill
#           prevent SQL Injection
#
#	Date:	2014-09-16 YatWoon
#			fixed: missing check eHomework role admin (only) can edit the homework [Case#N67113]
#			deploy: IPv10.1
#
#	Date:	2010-09-20 Henry Chow
#			update HomeworkType when submit
#
###########################

$PATH_WRT_ROOT = "../../../../../../";
$top_menu_mode = 0;

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || (!$_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"]  && !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] && !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_subject_leader"]))
{
	if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])
	{
		header("location: ../../settings/index.php");	
		exit;
	}
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}


$title = intranet_htmlspecialchars($title);
$description = intranet_htmlspecialchars(trim($description));

$lu = new libuser($UserID);
$lhomework = new libhomework2007();

$CurrentPageArr['eServiceHomework'] = 1;
if ($lu->isTeacherStaff() || $_SESSION["SSV_PRIVILEGE"]["homework"]["is_subject_leader"])
{
    if ($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"] || $lhomework->nonteachingAllowed || $lu->teaching == 1 || $lhomework->subjectLeaderAllowed)
    {
        $haveAccess = true;
    }
}

if ($haveAccess)
{
    ### Handle SQL Injection + XSS [START]
    $HomeworkID = IntegerSafe($HomeworkID);
    $typeID = IntegerSafe($typeID);
    $subjectID = IntegerSafe($subjectID);
    $subjectGroupID = IntegerSafe($subjectGroupID);
    
    $loading = (float)$loading;
    $handin_required = IntegerSafe($handin_required);
    $collect_required = IntegerSafe($collect_required);
    ### Handle SQL Injection + XSS [END]
    
	# Check date
	# Start date >= curdate() && Start date <= due date
	$start_stamp = strtotime($startdate);
	$due_stamp = strtotime($duedate);
	$record = $lhomework->returnRecord($HomeworkID);
	$input_date = $record[8];
	$input_date_stamp = strtotime($input_date);
	
	$success = 1;
	if(!intranet_validateDate($startdate) || !intranet_validateDate($duedate))
	{
	    $success = 0;
	}
	else if ($lhomework->pastInputAllowed)
	{
		if (compareDate($due_stamp, $start_stamp) < 0)
		{
			$success = 0;
		}
	}
	else
	{
		if (compareDate($start_stamp, $today) < 0 || compareDate($due_stamp, $start_stamp) < 0)
		{
            $success = 0;
		}
	}
			
	if ($success == 1)
	{
		$today = mktime(0,0,0,date("m"),date("d"),date("Y"));
		$today_str = date("Y-m-d",$today);
		
		$handinRequired = $handin_required == ""? 0 : 1;
		
		$fieldnames = "";
		$fieldnames .= "StartDate = '$startdate',";
		$fieldnames .= "DueDate = '$duedate',";
		$fieldnames .= "Loading = '$loading',";
		$fieldnames .= "Title = '$title',";
		$fieldnames .= "Description = '$description'";
		$fieldnames .= ",PosterUserID = '$UserID'";
		$fieldnames .= ",LastModified = now()";
		$fieldnames .= ",TypeID = '$typeID'";
		$fieldnames .= ",HandinRequired = '$handinRequired'";
		$fieldnames .= ",CollectRequired = '$collect_required'";
		$sql = "UPDATE INTRANET_HOMEWORK SET $fieldnames WHERE HomeworkID = '$HomeworkID' ";
		$success = $lhomework->db_db_query($sql);
		
		if($handinRequired==1){
			// $lhomework->createHandinList($HomeworkID,$classID);
		}
		else{
			$lhomework->removeHandinList($HomeworkID, $subjectGroupID);
		}
		   
		# Process Attachment
		$sql = "SELECT AttachmentPath FROM INTRANET_HOMEWORK WHERE HomeworkID = '$HomeworkID'";
		// echo "<p>$sql</p>";
		$temp = $lhomework->returnVector($sql,1);
		if($temp[0]=="") {
            $hasAttachment = false;
            $folder = session_id()."_h";
		}
		else{
			$hasAttachment = true;
			$folder = $temp[0];
		}
		
		$lf = new libfilesystem();
		$path = $file_path."/file/homework/".$folder.$HomeworkID."/";
		
		# Delete Files
		$files2delete = explode(":",$deleted_files);
		if(sizeof($files2delete)>0 && $hasAttachment){
			for ($i=0; $i<sizeof($files2delete); $i++){
			   if (urldecode($files2delete[$i])!=""){
				   $target = $path.urldecode($files2delete[$i]);
				   $lf->lfs_remove($target);
			   }
			}
			
			$size = $lf->folder_size($path);
			if ($size[1]==0){
				  $lf->folder_remove($path);
				  $sql = "UPDATE INTRANET_HOMEWORK SET AttachmentPath = NULL WHERE HomeworkID = '$HomeworkID'";
				  $lhomework->db_db_query($sql);
				  $hasAttachment = false;
			}
		}
		
		# Upload Files
		$update_db= false;
		$attachment_size = $attachment_size==""? 0 : $attachment_size;
		for ($i=1; $i<=$attachment_size; $i++){
		   $key = "filea$i";
		   $loc = ${"filea$i"};
           
		   // Getting file name
		   $file1 = ${"filea$i"."_name"};
		   // $file_hidden = ${"filea$i"."_hidden"};
		   $file_hidden = ${"hidden_userfile_name$i"};
		   $file = ($file_hidden!="") ? $file_hidden : $file1;
		   $des = "$path/".stripslashes($file);
                       
		   if ($loc == "none" || trim($loc) == "")
		   {
		       // do nothing
		   }
		   else
		   {
			  if (strpos($file,"."==0))
			  {
			      // do nothing
			  }
			  else
			  {
					if (!$hasAttachment)
					{
						 $lf->folder_new ($path);
						 $hasAttachment = true;
						 $update_db = true;
					}
					$lf->lfs_copy($loc, $des);
			  }
		   }
		}
		
		# Update attachment in DB
		if ($update_db){
			  $sql = "UPDATE INTRANET_HOMEWORK SET AttachmentPath = '$folder' WHERE HomeworkID = '$HomeworkID'";
			  $lhomework->db_db_query($sql);
		} 
	# end of Process Attachment
	}
	
	if ($success == 0)
	{
		$msg = "delete_failed";
	}
	else
	{
		$msg = "update";
	}
	
	$return_url = 'index.php';
	$url = (($return_url == "")? "/":$return_url);
	header ("Location: index.php?subjectID=$subjectID&subjectGroupID=$subjectGroupID&msg=$msg");
}
else
{
    header ("Location: index.php");
}

intranet_closedb();
?>