<?php
// Modifing by : 

##### Change Log [Start] ######
#
#	Date:	2019-05-08 (Bill)
#           prevent SQL Injection + Cross-site Scripting
#
#   Date:   2018-09-17 (Anna)
#           Added $sys_custom['eHomework_Status_MarkAsNoSubmit_SelectAll'] [Case#F143775]
#
#	Date:	2015-07-16 (Shan)
#			Update serachbox style, add $(document).ready( function()
#
#	Date:	2014-09-22 (YatWoon)
#			add flag checking $sys_custom['eHomework']['HideClearHomeworkRecords'] [Case#F68177]
#			Deploy: IPv10.1
#
#	Date:	2014-09-04 (Bill)
#			add Last Modified
#
#	Date:	2011-08-02 (Henry Chow)
#			add checking on $lhomework->ClassTeacherCanViewHomeworkOnly
#
#	Date:	2011-04-06 (Henry Chow)
#			add checking on $lhomework->exportAllowed while form submission
#
#	Date:	2010-10-08 Henry Chow
#			if student is eHomework admin, then also can see all filters' data
#
#	Date	:	2010-10-08 Henry Chow
#				pass 1 more parameter "classID" to add.php when adding homework
#
###### Change Log [End] #######

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");

intranet_auth();
intranet_opendb();

$lhomework = new libhomework2007();

# check access right
if(
	$_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"]
	|| !$sys_custom['eHomework_Status_Supplementary_WitHandInStatusManage']
	|| (!$_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"]
	&& !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"]
	&& !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_subject_leader"]
	&& !$lhomework->isViewerGroupMember($UserID))
)
{
	if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])
	{
		header("location: ../../settings/index.php");
		exit;
	}

	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$linterface = new interface_html();

/******************************/
$handInStatusArr = $lhomework->getAllHandInStatus();
/******************************/
$needToTransferToDiscipline = false;
$mustBeTransferToDiscipline = false;

$latest_modified_date = $lhomework->checkTransferOrNot();
if ($latest_modified_date !== false) {
    $needToTransferToDiscipline = true;
    if ($lhomework->checkUpdateIsMoreThanOneDay($latest_modified_date))
    {
        $mustBeTransferToDiscipline = true;
    }
}
/******************************/

$lclass = new libclass();
# change page size
if ($page_size_change == 1)
{
	setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
	$ck_page_size = $numPerPage;
}

/**********************************/
function resetSubjectGroupID($subjectGroupID, $subjectGroups)
{
	for ($i=0; $i<sizeof($subjectGroups); $i++)
	{
		if ($subjectGroupID==$subjectGroups[$i][0])
		{
			return $subjectGroupID;
		}
	}
	return "";
}

/**********************************/
if (isset($ck_page_size) && $ck_page_size != "")
	$page_size = $ck_page_size;

	$pageSizeChangeEnabled = true;

	/*
	# Table initialization
	$order = ($order == "") ? 1 : $order;
	$field = ($field == "") ? $sortField : $field;
	$pageNo = ($pageNo == "") ? 1 : $pageNo;
	*/

	# Create a new dbtable instance
	$li = new libdbtable2007($field, $order, $pageNo);

	# Settings
	$s = addcslashes($s, '_');
	if (empty($s)) {
	    $s = empty($_POST["text"]) ? (empty($_GET["text"]) ? '' : $_GET["text"]) : $_POST["text"];
	}
	/*
	$searchByTeacher =($lhomework->teacherSearchDisabled == 0) ? "or a.PosterName like '%$s%'" : "" ;
	$searchBySubject =($lhomework->subjectSearchDisabled == 0) ? " or IF('$intranet_session_language' = 'en', b.EN_DES, b.CH_DES) like '%$s%'" : "" ;
	*/
	$allowExport = $lhomework->exportAllowed;
	
	// [2015-0901-1009-21066] fixed: drop down list filtering not work, use data from $_GET[] only if $_POST[] is not set
	$yearID = isset($_POST['yearID'])? $_POST['yearID'] : $_GET['yearID'];
	$yearTermID = isset($_POST['yearTermID'])? $_POST['yearTermID'] : $_GET['yearTermID'];
	$subjectID = isset($_POST['subjectID'])? $_POST['subjectID'] : $_GET['subjectID'];
	$subjectGroupID = isset($_POST['subjectGroupID'])? $_POST['subjectGroupID'] : $_GET['subjectGroupID'];
	$HandInStatusID = isset($_POST['HandInStatusID'])? $_POST['HandInStatusID'] : $_GET['HandInStatusID'];
	
	### Handle SQL Injection + XSS [START]
	$yearID = IntegerSafe($yearID);
	$yearTermID = IntegerSafe($yearTermID);
	$subjectID = IntegerSafe($subjectID);
	$subjectGroupID = IntegerSafe($subjectGroupID);
	$classID = IntegerSafe($classID);
	$sid = IntegerSafe($sid);
	$sgid = IntegerSafe($sgid);
	$HandInStatusID = IntegerSafe($HandInStatusID);
	### Handle SQL Injection + XSS [END]
	
	/*
	if (isset($_POST["startDate"]) && isset($_POST["endDate"])) {
		$AcademicYearAndYearTerm = getAcademicYearAndYearTermByDate($_POST["startDate"]);
		$searchYearTermID = $AcademicYearAndYearTerm["YearTermID"];
		$reqGetAcademicYear = true;
		$objTerm = new academic_year_term($searchYearTermID, false);
		$termStart = date("Y-m-d", strtotime($objTerm->TermStart));
	
		$yearID = $objTerm->AcademicYearID;
		$yearTermID = $objTerm->YearTermID;
	}
	*/
	# class menu
	if($_SESSION['UserType']!=USERTYPE_STUDENT) {
	    # eHomework Admin
		// [2015-1117-1137-20073] Viewer Group member should able to select all classes
		if(($_SESSION['UserType']==USERTYPE_STAFF && !$_SESSION['isTeaching']) || $_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] || $lhomework->isViewerGroupMember($UserID)) {
		    $classes = $lhomework->getAllClassInfo();
		}
		# Class teacher / subject teacher
		else {
			$classes = $lhomework->getAllClassesInvolvedByTeacherID(Get_Current_Academic_Year_ID(), GetCurrentSemesterID(), $UserID);
		}
		// $selectClass = getSelectByArray($classes, 'name="classID" id="classID" onChange="document.form1.submit();"', $classID, 0, 0, $i_general_all_classes);
		$selectClass = getSelectByArray($classes, 'name="classID" id="classID" onChange="document.form1.submit();"', $classID, 0, 0, $Lang['SysMgr']['Homework']['AllClass']);
	}
    
	if (empty($subjectID)) {
		$sid = isset($_POST['sid'])? $_POST['sid'] : $_GET['sid'];
		// $subjectID = $sid;
	}
	
	# Start layout
	$CurrentPageArr['eAdminHomework'] = 1;
	$CurrentPage = "Management_HomeworkList";
	$PAGE_TITLE = $Lang['SysMgr']['Homework']['HomeworkList'];
	$MODULE_OBJ = $lhomework->GET_MODULE_OBJ_ARR();

	// $query_str = "yearID=" . $yearID . "&yearTermID=" . $yearTermID . "&classID=" . $classID . "&subjectID=" . $subjectID . "&subjectGroupID=" . $sgid . "&sid=" . $sid;
	$query_str = "yearID=" . $yearID . "&yearTermID=" . $yearTermID . "&classID=" . $classID . "&subjectGroupID=" . $sgid . "&sid=" . $sid . "&subjectID=" . $subjectID . "&HandInStatusID=" . $HandInStatusID;
	$TAGS_OBJ = $lhomework->getHomeworkListTabs(basename(__FILE__), $query_str);

	// $htmlAry['searchBox'] = $linterface->Get_Search_Box_Div('text', stripslashes(stripslashes($s)));

	$args = array(
		"attr" => "name=\"HandInStatusID\" onChange=\"reloadForm()\"",
		"dataArr" => $handInStatusArr,
		"dataType" => "Supplementary",
		"selectedID" => $HandInStatusID,
		"firstLabel" => $Lang['SysMgr']['Homework']['AllHandInStatus'],
		"firstVal" => ""
	);
	$selectOption_HandinStatus = $lhomework->getHandleInStatus($args);
	
	if (isset($_POST["cancelRec"]) && !empty($_POST["cancelRec"])) {

		$cancelRecData = $_POST["cancelRec"];
		$cancelInfo = explode("|", $cancelRecData);
		$strSQL = "SELECT CancelViolationStatus, CancelViolationDate FROM INTRANET_HOMEWORK_HANDIN_LIST WHERE RecordID='" . $cancelInfo[0] . "' AND StudentID='" . $cancelInfo[1] . "' AND HomeworkID='" . $cancelInfo[2] . "' AND CancelViolationStatus!='1'";
		$result = $lhomework->returnResultSet($strSQL);
		if (count($result) > 0) {
			$param[0] = array(
				"StudentID" => $cancelInfo[1],
				"HomeworkID" => $cancelInfo[2]
			);
			$strSQL = "UPDATE INTRANET_HOMEWORK_HANDIN_LIST SET CancelViolationStatus='1', CancelViolationDate=NOW(), SuppRecordDate=NOW(), HWViolationDate='' WHERE RecordID='" . $cancelInfo[0] . "' AND StudentID='" . $cancelInfo[1] . "' AND HomeworkID='" . $cancelInfo[2] . "'";
						$lhomework->db_db_query($strSQL);
			
			$log_cancelInfo = array(
					"RecordID" => $cancelInfo[0],
					"StudentID" => $cancelInfo[1],
					"HomeworkID" => $cancelInfo[2],
					"Remark" => "Cancel from Homework",
			);
			$lhomework->AddCancelMisconductLog($log_cancelInfo);
			
			include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
			$disciplinev12 = new libdisciplinev12();
			$disciplinev12->REQUEST_CANCEL_HW_MISCONDUCT_RECORD($param);
		}
		$msg = "update";
		$query_str .= "&startDate=" . $_POST["startDate"] . "&endDate=" . $_POST["endDate"] . "&s=" . $s;
		header("Location: " . basename(__FILE__) . "?" .$query_str . "&updated=1" );
		exit;
	} else if ($_POST["isUpdate"] == "1") {
		/******************************************************/
		/* Update Record
		/******************************************************/
		if (isset($_POST["orghandinRec"]) && count($_POST["orghandinRec"] > 0)) {
			
			/* for Buddhist Wong Wan Tin College */
			if (!$sys_custom['eHomework_Status_Supplementary_WitTwoHandInStatusManage']) {
				$strSQL = "SELECT ihhl.RecordID, ihhl.HomeworkID, ihhl.StudentID, ihhl.RecordStatus, ihhl.SuppRecordStatus, ihhl.SuppRecordDate, ihhl.HWViolationID, ihhl.HWViolationDate, ihhl.DateModified, ihhls.RecordDate as sRecordDate, ihhls.SuppRecordStatus as sSuppRecordStatus";
				$strSQL .= " FROM INTRANET_HOMEWORK_HANDIN_LIST as ihhl";
				$strSQL .= " INNER JOIN INTRANET_USER as IU on (IU.UserID = ihhl.StudentID and IU.RecordStatus != '3') ";
				$strSQL .= " LEFT JOIN INTRANET_HOMEWORK_HANDIN_LIST_SUPP AS ihhls ON (ihhls.HW_RecordID=ihhl.RecordID AND RecordDate='" . $_POST["currDate"] . "')";
				$strSQL .= " WHERE RecordID IN (" . implode(", ", array_keys($_POST["orghandinRec"])) . ")";
			} else {
				$strSQL = "SELECT ihhi.RecordID, ihhi.HomeworkID, ihhi.StudentID, ihhi.RecordStatus, ihhi.SuppRecordStatus, ihhi.SuppRecordDate, ihhi.HWViolationID, ihhi.HWViolationDate, ihhi.DateModified
                            FROM INTRANET_HOMEWORK_HANDIN_LIST as ihhi 
                            INNER JOIN INTRANET_USER as IU on (IU.UserID = ihhi.StudentID and IU.RecordStatus != '3') 
                            WHERE ihhi.RecordID IN (" . implode(", ", array_keys($_POST["orghandinRec"])) . ")";
			}

			$result = $lhomework->returnResultSet($strSQL);
			foreach ($result as $index => $dbhandinRec) {
				$updateRecordID = $dbhandinRec["RecordID"];
				if (isset($_POST["handinRec"][$updateRecordID])) {
					$updateSuppRecordStatus = $_POST["handinRec"][$updateRecordID];
				} else {
					$updateSuppRecordStatus = "-9999";
				}
				if (empty($dbhandinRec["SuppRecordStatus"]) || in_array($dbhandinRec["SuppRecordStatus"], array("0", "-9999"))) {
					$dbhandinRec["SuppRecordStatus"] = "-9999";
				}
				$runUpdate = false;
				if ($sys_custom['eHomework_Status_Supplementary_WitTwoHandInStatusManage'] ) {
					if ($dbhandinRec["SuppRecordStatus"] != $updateSuppRecordStatus) {
						$runUpdate = true;
						if ($updateSuppRecordStatus == "-9999"
							&& $dbhandinRec["SuppRecordStatus"] == "-20")
						{
							$runUpdate = false;
						}
					}
				} else {
					if ($dbhandinRec["sSuppRecordStatus"] != $updateSuppRecordStatus ) {
						$runUpdate = true;
						if ($updateSuppRecordStatus == "-9999"
								&& $dbhandinRec["SuppRecordStatus"] == "-20")
						{
							$runUpdate = false;
						}
					}
				}
				if ($runUpdate) {
					$strSQL = "UPDATE INTRANET_HOMEWORK_HANDIN_LIST ";
					$strSQL .= " SET DateModified=NOW(), SuppRecordDate=NOW(), SuppRecordStatus='" .  $updateSuppRecordStatus . "'";
					$strSQL .= " WHERE RecordID='" . $updateRecordID ."';";

					$lhomework->db_db_query($strSQL);
					$strSQL = "SELECT SuppRecordID FROM INTRANET_HOMEWORK_HANDIN_LIST_SUPP";
					$strSQL .= " WHERE HW_RecordID='" . $updateRecordID . "'";
					$strSQL .= " AND HW_HomeworkID='" . $dbhandinRec["HomeworkID"] . "'";
					$strSQL .= " AND HW_StudentID='" . $dbhandinRec["StudentID"] . "'";
					$strSQL .= " AND RecordDate='" . $_POST["currDate"] . "' ORDER BY RecordDate DESC LIMIT 1";
					$result = $lhomework->returnResultSet($strSQL);
					if (count($result) > 0) {
						$Date_SuppRecordID = $result[0]["SuppRecordID"];
						$strSQL = "UPDATE INTRANET_HOMEWORK_HANDIN_LIST_SUPP SET SuppRecordStatus='" . $updateSuppRecordStatus . "'";
						$strSQL .= " WHERE SuppRecordID='" . $Date_SuppRecordID . "' AND RecordDate='" . $_POST["currDate"] . "'";
						$lhomework->db_db_query($strSQL);
					} else {
						$strSQL = "INSERT INTO INTRANET_HOMEWORK_HANDIN_LIST_SUPP (HW_RecordID, HW_HomeworkID, HW_StudentID, SuppRecordStatus, RecordDate, DateInput, InputBy, LastModified, ModifiedBy)";
						$strSQL .= " VALUES ('" . $updateRecordID . "', '" . $dbhandinRec["HomeworkID"] . "', '" . $dbhandinRec["StudentID"] . "', '" . $updateSuppRecordStatus . "', '" . $_POST["currDate"] . "', NOW(), '" . $_SESSION["UserID"] . "', NOW(), '" . $_SESSION["UserID"] . "') ";
						$lhomework->db_db_query($strSQL);
						$Date_SuppRecordID = $lhomework->db_insert_id();
					}
					if ($Date_SuppRecordID > 0) {
						$insert_param = array(
								"SuppRecordID" => $Date_SuppRecordID,
								"HW_RecordID" => $updateRecordID,
								"HW_HomeworkID" => $dbhandinRec["HomeworkID"],
								"HW_StudentID" => $dbhandinRec["StudentID"],
								"SuppRecordStatus" => $updateSuppRecordStatus,
								"RecordDate" => $_POST["currDate"],
								"InputBy" => $_SESSION["UserID"]
						);
						$strSQL = "INSERT INTO INTRANET_HOMEWORK_HANDIN_LIST_SUPP_REC (" . implode(", ", array_keys($insert_param)) . ", DateInput) VALUES";
						$strSQL .= " ('" . implode("', '", $insert_param) . "', NOW())";
						$lhomework->db_db_query($strSQL);
					}
				}
			}
			$query_str .= "&startDate=" . $_POST["startDate"] . "&endDate=" . $_POST["endDate"] . "&s=" . $s;
			header("Location: " . basename(__FILE__) . "?" .$query_str . "&updated=1" );
			exit;
		}
	} 
	if ($_GET["updated"] == "1") {
		$msg = "update";
	}

	# Teacher Mode with Teaching
	if($_SESSION['UserType']==USERTYPE_STAFF && $_SESSION['isTeaching'])
	{
		# Academic Year
		$academicYear = $lhomework->GetAllAcademicYear();
		$yearID = ($yearID=="")? Get_Current_Academic_Year_ID():$yearID;
		//$yearTerm
		$selectedYear = $lhomework->getCurrentYear("name=\"yearID\" onChange=\"reloadForm()\"", $academicYear, $yearID, "", false);
		if($yearID!=""){
			if($yid!="" && $yid != $yearID){
				$yearTermID="";
			}
			
			# Current Year Term
			$academicYearTerm = $lhomework->getAllAcademicYearTerm($yearID);
			$selectedYearTerm = $lhomework->getCurrentYearTerm("name=\"yearTermID\" onChange=\"reloadForm()\"", $academicYearTerm, $yearTermID, $Lang['SysMgr']['Homework']['AllYearTerms']);
			if($ytid!="" && $ytid != $yearTermID){
				$subjectID="";
			}
			$subjectID = ($subjectID=="")? -1 :$subjectID;
			# Subject Menu
			$UserIDTmp = $_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] ? "" : $UserID;
			$subject = $lhomework->getTeachingSubjectList($UserIDTmp, $yearID, $yearTermID, $classID);
			$selectedSubject = $lhomework->getCurrentTeachingSubjects("name=\"subjectID\" onChange=\"reloadForm();\"", $subject, $subjectID, $Lang['SysMgr']['Homework']['AllSubject'], true);
			if($subjectID != "-1"){
				$subjectGroups = $lhomework->getTeachingSubjectGroupList($UserIDTmp,$subjectID, $yearID, $yearTermID, $classID);
			}
			else
			{
				$subjectGroups = $lhomework->getTeachingSubjectGroupList($UserIDTmp,"", $yearID, $yearTermID, $classID);
			}
			
			$subjectGroupID = resetSubjectGroupID($subjectGroupID, $subjectGroups);
			
			$selectedSubject = $lhomework->getCurrentTeachingSubjects("name=\"subjectID\" onChange=\"reloadForm();\"", $subject, $subjectID, $Lang['SysMgr']['Homework']['AllSubject'], true);
			if ($yearTermID!="" && $yearTermID>0)
			{
				# only display subject group for particular term
				$selectedSubjectGroups = $lhomework->getCurrentTeachingGroups("name=\"subjectGroupID\" onChange=\"reloadForm()\"", $subjectGroups, $subjectGroupID, $Lang['SysMgr']['Homework']['PleaseSelect']);
			} else
			{
				$subjectGroupID = "";
			}
			
			// $filterbar = "$selectedYear $selectedYearTerm $selectClass $selectedSubject";
			$filterbar = "$selectClass $selectedSubject $selectOption_HandinStatus";
		}
		
	}
	# Teacher Mode with Non Teaching
	else if($_SESSION['UserType']==USERTYPE_STAFF && !$_SESSION['isTeaching'])
	{
		# Academic Year
		$academicYear = $lhomework->GetAllAcademicYear();
		$yearID = ($yearID=="")? Get_Current_Academic_Year_ID():$yearID;
		$selectedYear = $lhomework->getCurrentYear("name=\"yearID\" onChange=\"reloadForm()\"", $academicYear, $yearID, "", false);
		if($yearID!=""){
			if($yid!="" && $yid != $yearID){
				$yearTermID="";
			}
		
			# Current Year Term
			$academicYearTerm = $lhomework->getAllAcademicYearTerm($yearID);
			$selectedYearTerm = $lhomework->getCurrentYearTerm("name=\"yearTermID\" onChange=\"reloadForm()\"", $academicYearTerm, $yearTermID, $Lang['SysMgr']['Homework']['AllYearTerms']);
		
			if($ytid!="" && $ytid != $yearTermID){
				$subjectID="";
			}
			$subjectID = ($subjectID=="")? -1 :$subjectID;
		
			# Subject Menu
			$subject = $lhomework->getTeachingSubjectList("", $yearID, $yearTermID, $classID);
		
			$selectedSubject = $lhomework->getCurrentTeachingSubjects("name=\"subjectID\" onChange=\"reloadForm()\"", $subject, $subjectID, $Lang['SysMgr']['Homework']['AllSubjects']);
		
			if ($subjectID!="" && $yearTermID!="" && $yearTermID>0){
		
				$sgid = $subjectGroupID;
				if($sid != $subjectID){
					$subjectGroupID="";
				}
				# Subject Group Menu
				$subjectGroups = $lhomework->getTeachingSubjectGroupList("", $subjectID, $yearID, $yearTermID, $classID);
				$subjectGroupID = resetSubjectGroupID($subjectGroupID, $subjectGroups);
				$selectedSubjectGroups = $lhomework->getCurrentTeachingGroups("name=\"subjectGroupID\" onChange=\"reloadForm()\"", $subjectGroups, $subjectGroupID, $Lang['SysMgr']['Homework']['PleaseSelect']);
			} else
			{
				$subjectGroupID = "";
			}
			// $filterbar = "$selectedYear $selectedYearTerm $selectClass $selectedSubject $selectedSubjectGroups";
			$filterbar = "$selectClass $selectedSubject $selectOption_HandinStatus";
		}
	}
	# Subject Leader
	else if($_SESSION['UserType']==USERTYPE_STUDENT)
	{
		include_once($PATH_WRT_ROOT."includes/libaccessright.php");
		$laccessright = new libaccessright();
		$laccessright->NO_ACCESS_RIGHT_REDIRECT();
		exit;
	}
	
	$order = ($order == "") ? 1 : $order;
	$field = ($field == "") ? $sortField : $field;
	$pageNo = ($pageNo == "") ? 1 : $pageNo;
	
	// $query_str = "yearID=" . $yearID . "&yearTermID=" . $yearTermID . "&classID=" . $classID . "&subjectID=" . $subjectID . "&subjectGroupID=" . $sgid . "&sid=" . $sid;
	
	if (isset($_POST["startDate"])) $startDate = $_POST["startDate"];
	else {
	    
	    if (isset($_GET["startDate"])) $startDate = $_GET["startDate"];
	    else
	    {
    		$TermInfoArr = getCurrentAcademicYearAndYearTerm();
    		$searchYearTermID = $TermInfoArr['YearTermID'];
    		$objTerm = new academic_year_term($searchYearTermID, false);
    		// $startDate = date("Y-m-d", strtotime($objTerm->TermStart));
    		$startDate = date("Y-m-d", strtotime(getStartDateOfAcademicYear($objTerm->AcademicYearID)));
	   }
	}
	
	if (isset($_POST["endDate"])) $endDate = $_POST["endDate"];
	else if (isset($_GET["endDate"])) $endDate= $_GET["endDate"];
	
	else $endDate = date("Y-m-d");
	
	$linterface->LAYOUT_START();
	
	$handin_param = array(
		"yearID" => $yearID,
		"yearTermID" => $yearTermID,
		"classID" => $classID,
		"subjectID" => $subjectID,
		"subjectGroupID" => $sgid,
		"subjectID" => $subjectID,
		"HandInStatusID" => $HandInStatusID,
		"startDate" => $startDate,
		"endDate" => $endDate,
		"searchTxt" => trim($s),
		"order" => $order,
		"field" => $field,
		"callBy" => basename(__FILE__)
	);

	$required = true;
	$handin_studentsArr = $lhomework->getStudentsByParam($handin_param);
	$last_cutoff = $lhomework->getLatestGenerateTime();

	if ($sys_custom["eHomework_Status_Supplementary_WitHandInStatusManage"] && !$sys_custom['eHomework_Status_Supplementary_WitTwoHandInStatusManage']) {
		$eDisRecordsArr = $lhomework->checkDisciplineRec($handin_studentsArr);
	}
	
?>
<script language="javascript">
var eHomeworkAppJS = {
	vars: {
		showWithButton: false,
		showWithCheckbox: true
	},
	listeners: {
		buttonHandler: function(e) {
			var strREL = $(this).attr('rel');
			if (typeof strREL != "undefined") {
				if ($("input." + strREL + ":not(:checked)").length > 0) {
					$("input." + strREL + ":not(:checked)").trigger('click');
				} else {
					$("input." + strREL + "").trigger('click');
				}
				eHomeworkAppJS.func.highlight();
			}
			e.preventDefault();
		},
		buttonCheckboxHandler: function(e) {
			var strREL = $(this).attr('rel');
			if (typeof strREL != "undefined") {
				if ($(this).attr('checked')) {
					$("input." + strREL + ":not(:checked)").trigger('click');
				} else {
					$("input." + strREL + ":checked").trigger('click');
				}
			}
		},
		disableHandler: function(e) {
			e.preventDefault();
		},
		checkboxClickHandler: function(e) {
			var currObj = $(this);
			var row_name = $(this).attr('data-row');
			$('input.' + row_name).not(this).attr('checked', false);
			eHomeworkAppJS.func.highlight();  
		},
		trHoverHandler: function(e) {
			switch (e.type) {
				case "mouseover":
					$(this).css({"background-color":"#efefef"});
					break;
				default:
					$(this).css({"background-color":"#fff"});
					eHomeworkAppJS.func.highlight();
					break;
			}
			e.preventDefault();
		},
		cancelViolationHandler: function(e) {
			if (confirm("<?php echo $Lang['SysMgr']['Homework']['CancelMisconductMsg']; ?>")) {
    			document.form1.cancelRec.value= $(this).attr('rel');
    			document.form1.isUpdate.value='0';
    			document.form1.s.value = "";
    			document.form1.pageNo.value = 1;
    			var url      = window.location.href;     // Returns full URL
    			url = url.replace("&updated=1", "");
    			document.form1.action = url; 
    			document.form1.submit();
			}
			e.preventDefault();
		},
		dateChangeHandler: function() {
			reloadForm();
		}
	},
	func: {
		highlight: function() {
			$('#recTable tbody tr:not(:first-child)').each(function() {
				if ($(this).find('input:checked').eq(0).hasClass('col_1')) {
					$(this).css({"background-color":"#effcff"});
				} else if ($(this).find('input:checked').eq(0).hasClass('col_2')) {
					$(this).css({"background-color":"#e6ffde"});
				} else if ($(this).find('input:checked').eq(0).hasClass('col_3')) {
					$(this).css({"background-color":"#ffe0e1"});
				} else {
					$(this).css({"background-color":"#fff"});
				}
			});
		},
		init: function() {
			// $('#recTable tbody tr:first-child td').css({ "background-color": "#1B91AF" });
			$('#recTable tbody tr td').css({ "padding-top":"4px", "padding-bottom":"4px" });
			$('form[name="form2"]').bind('submit', eHomeworkAppJS.listeners.disableHandler);
			$('input.statusRd').bind('click', eHomeworkAppJS.listeners.checkboxClickHandler);
			$('#recTable tbody tr:not(:first-child)').bind('mouseover mouseleave', eHomeworkAppJS.listeners.trHoverHandler);
			$('#recTable tbody tr:not(:first-child) td').each(function() {
				$(this).css({"border-bottom": "1px solid #afafaf"});
			});
			$('button.cancelViolationBtn').bind('click', eHomeworkAppJS.listeners.cancelViolationHandler);

			if (eHomeworkAppJS.vars.showWithButton) {
				$('#recTable').find('button.checkhdr').bind('click', eHomeworkAppJS.listeners.buttonHandler);
				$('#recTable button.checkhdr').css({"display":"block"}).show();
			} else {
				$('#recTable button.checkhdr').css({"display":"block"}).hide();
			}
			if (eHomeworkAppJS.vars.showWithCheckbox) {
				$('#recTable').find('input.checkhdr').bind('click', eHomeworkAppJS.listeners.buttonCheckboxHandler);
				$('#recTable input[type="checkbox"].checkhdr').show();
			} else {
				$('#recTable input[type="checkbox"].checkhdr').hide();
			}
			eHomeworkAppJS.func.highlight();
		},
		submitRecord: function(formObj) {
			formObj.submit();
		}
	}	
};

$(document).ready( function() {
	$('input#text').keydown( function(evt) {
		if (Check_Pressed_Enter(evt)) {
			evt.preventDefault();
			if ($("input[name='text']").size() > 0) {
				$("input[name='s']").eq(0).val($("input[name='text']").eq(0).val());
			}
			$("input[name='pageNo']").eq(0).val(1);
			// pressed enter
			goSearch();
		}
	});
	eHomeworkAppJS.func.init();
});
	function removeCat(obj,element,page){
		var alertConfirmRemove = "<?=$i_Discipline_System_alert_remove_record?>";
        if(countChecked(obj,element)==0)
                alert(globalAlertMsg2);
        else{
			if(confirm(alertConfirmRemove)){
				obj.action=page;
				obj.method="post";
				obj.submit();
			}
        }
	}

	function reloadForm() {
		document.form1.isUpdate.value='0';
		if ($("input[name='text']").size() > 0) {
			document.form1.s.value = document.form1.text;
			$("input[name='s']").eq(0).val($("input[name='text']").eq(0).val());
		} else {
			document.form1.s.value = '';
			$("input[name='s']").eq(0).val('');
		}
		document.form1.pageNo.value = 1;
		var url      = window.location.href;     // Returns full URL
		url = url.replace("&updated=1", "");
		document.form1.action = url; 
		document.form1.submit();
	}

	function goSearch() {
		// document.form1.s.value = document.form2.text.value;
		if ($("input[name='text']").size() > 0) {
			$("input[name='s']").eq(0).val($("input[name='text']").eq(0).val());
		}
		$("input[name='pageNo']").eq(0).val(1);
		document.form1.pageNo.value = 1;
		document.form1.submit();
	}


	function viewHmeworkDetail(id)
	{
		   newWindow('./view.php?hid='+id,1);
	}


	function viewHandinList(id)
	{
		   // newWindow('./handinmanage_list.php?hid='+id,10);
		newWindowWithSize('./handinmanage_list.php?hid='+id, 0, 1, 960, 600);
	}

	function checkform(formOBJ) {
		if(confirm("<?php echo $i_SmartCard_Confirm_Update_Attend?>")){
			formOBJ.isUpdate.value='1';
			eHomeworkAppJS.func.submitRecord(formOBJ);
		}
	}
	
</script>
	<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
		<tbody>
		<!-- Homework Detail -->
		<tr>
			<td>
				<table width="98%" border="0" cellspacing="0" cellpadding="0">
					<tbody>
					<tr>
						<td height="28" align="right" valign="bottom">
							<form name="form2" method="post">
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tbody>
								<tr>
									<td>
										<table border="0" cellspacing="0" cellpadding="2">
											<tbody><tr>
												<td></td>
											</tr>
										</tbody></table>
									</td>
									<td align="right"> 
										<?= $htmlAry['searchBox']?>
									</td>
								</tr>
								</tbody>
							</table>
							</form>
							<form name="form1" method="post">
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tbody>
							<tr>
								<td height="28" align="right" valign="bottom">
									<table width="100%" border="0" cellspacing="0" cellpadding="4">
										<tr>
											<td align="right"><?= $linterface->GET_SYS_MSG($msg) ?>
											</td>
										</tr>
									</table>
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td height="30">
												<?=$Lang['SysMgr']['Homework']["From"]?>
												<?php echo $linterface->GET_DATE_PICKER("startDate", $startDate, "", "yy-mm-dd", "", "", "", "eHomeworkAppJS.listeners.dateChangeHandler()"); ?>
												<?=$Lang['SysMgr']['Homework']["To"]?>
												<?php echo $linterface->GET_DATE_PICKER("endDate", $endDate, "", "yy-mm-dd", "", "", "", "eHomeworkAppJS.listeners.dateChangeHandler()"); ?>
												<?php echo $filterbar . " " . $selectOpt_homeworkList; ?>
												<?php echo $linterface->Get_Search_Box_Div('text', stripslashes(stripslashes($s))); ?>
											</td>
											<td align="right" valign="middle" class="thumb_list"><?=$choiceType?></td>
										</tr>
									</table>
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td>
											</td>
											<td align="right" valign="bottom">
											<br/>
											<br/>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							</tbody>
							</table>
<?php
						if ($required) {
						    if ($needToTransferToDiscipline) {
						        if ($mustBeTransferToDiscipline)
						        {
						            echo '<div style="text-align:right; padding-bottom:10px;"><span style="color:red;">*** ' . $Lang['SysMgr']['Homework']['RequestTransferMoreThanOneDayRecord'] . " ***</span></div>";
						        } else {
						            echo '<div style="text-align:right; padding-bottom:10px;"><span style="color:red;">***</span> ' . $Lang['SysMgr']['Homework']['RequestTransferRecord'] . "</div>";
						        }
						        
						    }
?>
							<table width="100%" border="0" cellspacing="0" cellpadding="4" id='recTable' align="center">
							<tbody>
							<tr class="tabletop" valign="bottom">
								<td width="5%" class="tablebluetop">#</td>
								<td width="10%" class="tablebluetop"><?php echo $Lang['SysMgr']['Homework']['Class']; ?> / <?php echo $Lang['SysMgr']['Homework']['ClassNumber']; ?></td>
								<td width="10%" class="tablebluetop"><?php echo $Lang['SysMgr']['Homework']['StudentName']; ?></td>
								<td width="10%" class="tablebluetop"><?php echo $Lang['SysMgr']['Homework']['Subject']; ?></td>
								<td width="10%" class="tablebluetop"><?php echo $Lang['SysMgr']['Homework']['Topic']; ?></td>
								<td class="tablebluetop"><?php echo $Lang['SysMgr']['Homework']['Description']; ?></td>
								<td width="10%" class="tablebluetop"><?php echo $Lang['SysMgr']['Homework']['HandinStatus']; ?></td>
								<td width="10%" class="tablebluetop"><?php echo $Lang['SysMgr']['Homework']['DueDate']; ?></td>
<?php
								$tdCount = 8;
								
								if ($sys_custom['eHomework_Status_Supplementary_WitTwoHandInStatusManage']) {
									$showStatus = array( 1 => 7, 2 => -11, 3 => -30);
								} else {
									$showStatus = array( 1 => 7, 2 => -11, 3 => -20);
								}
								foreach ($showStatus as $sskk => $ssvv) {
									if (isset($handInStatusArr[$ssvv])) {
										if ($handInStatusArr[$ssvv]["isDisabled"] != "1") {
											$tdCount++;
											echo '<td width="5%" class="tablebluetop" align="center">' . Get_Lang_Selection($handInStatusArr[$ssvv]["HandInStatusTitleB5"], $handInStatusArr[$ssvv]["HandInStatusTitleEN"]);
											if (!$mustBeTransferToDiscipline) {
    											if(!$lhomework->ViewOnly || $_SESSION['SSV_PRIVILEGE']['homework']['is_subject_leader'] || !$_SESSION['isTeaching']) {
    												// if (!$lhomework->isViewerGroupMember($_SESSION["UserID"]) && !$sys_custom['eHomework_Status_Supplementary_WitTwoHandInStatusManage'] && $ssvv !=' -20') {
    											    if (!$sys_custom['eHomework_Status_Supplementary_WitTwoHandInStatusManage'] && ($ssvv !=' -20' || $sys_custom['eHomework_Status_MarkAsNoSubmit_SelectAll'])) {
    													echo '<br><button style="display:none" class="checkhdr" rel="col_' . $sskk . '" style="padding:4px padding-left:5px; padding-right:5px; margin-top:4px; font-size:0.85em;"><nobr>' . $Lang['Btn']['SelectAll'] . '</nobr></button>';
    													echo '<input type="checkbox" style="display:none" class="checkhdr statusRd row_0 col_' . $sskk . '" data-row="row_0" rel="col_' . $sskk . '" >';
    												}
    											}
											}
											echo '</td>';
										}
									}
								}
								if ($sys_custom['eHomework_Status_Supplementary_WitHandInStatusManage'] && $sys_custom['eHomework_Status_Supplementary_WitTwoHandInStatusManage'] ||
								    $sys_custom['eHomework_allow_cancel_conduct_button']) {
									if(!$lhomework->ViewOnly || $_SESSION['SSV_PRIVILEGE']['homework']['is_subject_leader'] || !$_SESSION['isTeaching']) {
										$tdCount++;
										echo '<td align="center" width="5%" class="tablebluetop">' . $Lang['Btn']['Cancel'] . Get_Lang_Selection($handInStatusArr[-20]["HandInStatusTitleB5"], " '" . $handInStatusArr[-20]["HandInStatusTitleEN"] . "'");
									}
								}
?>
							</tr>
<?php

							if (count($handin_studentsArr) > 0) {
								foreach ($handin_studentsArr as $kk => $vv) {
									if (empty($vv["RecordStatus"])) $vv["RecordStatus"] = "-1";
									$index = $kk + 1;

									echo "<tr>\n";
									echo "	<td class='tabletext'>\n";
									echo $index;
									if(!$lhomework->ViewOnly || $_SESSION['SSV_PRIVILEGE']['homework']['is_subject_leader'] || !$_SESSION['isTeaching']) {
										echo " <input type='hidden' name='orghandinRec[" . $vv["RecordID"] . "]' value='" . $vv["HomeworkID"] . "|" . $vv["StudentID"] . "|" . $vv["RecordStatus"] . "|" . $vv["SuppRecordStatus"] . "'>";
									}
									echo "</td>\n";
									echo "	<td class='tabletext tablerow'>" . $vv["ClassTitle"] . "(" . $vv["ClassNumber"] . ")</td>\n";
									if (empty($vv["StudentName"])) $vv["StudentName"] = $vv["enStudentName"];
									if (empty($vv["StudentName"])) $vv["StudentName"] = $vv["UserLogin"];
									if ($vv["StudentStatus"] == 3) $vv["StudentName"] .= ' <font style="color:red;">*</font>';
									echo "	<td class='tabletext tablerow'>" . $vv["StudentName"] . "</td>\n";
									echo "	<td class='tabletext tablerow'>" . $vv["Subject"] . "</td>\n";
									echo "	<td class='tabletext tablerow'><a href='javascript:viewHmeworkDetail(" . $vv["HomeworkID"] . ");' class='tablelink'>" . $vv["Title"] . "</a></td>\n";
									echo "	<td class='tabletext tablerow'>" . $vv["Description"] . "</td>\n";
									echo "	<td class='tabletext tablerow'>" . Get_Lang_Selection($handInStatusArr[$vv["RecordStatus"]]["HandInStatusTitleB5"], $handInStatusArr[$vv["RecordStatus"]]["HandInStatusTitleEN"]) . "</td>\n";
									echo "	<td class='tabletext tablerow'>" . $vv["DueDate"] . "</td>\n";
									
									foreach ($showStatus as $sskk => $ssvv) {
										if (isset($handInStatusArr[$ssvv])) {
											if ($handInStatusArr[$ssvv]["isDisabled"] != "1") {
												
												if ($sys_custom['eHomework_Status_Supplementary_WitTwoHandInStatusManage']) {
													if ($vv["SuppRecordStatus"] == $ssvv) $strChecked = " checked";
													else $strChecked = "";
												} else {
													if ($vv["SuppRecordStatus"] == $ssvv && !empty($vv["DateSuppRecordStatus"])) {
														$strChecked = " checked";
													} else {
														$strChecked = "";
													}
												}
												echo "	<td class='tabletext tablerow' align='center'>";
												/*
												if ($vv["SuppRecordStatus"] == "-20" && !empty($vv["HWViolationID"]) && !$sys_custom['eHomework_Status_Supplementary_WitTwoHandInStatusManage']) {
													if ($ssvv == "-20") {
														echo "<span style='color:#ff0000;'>" . Get_Lang_Selection($handInStatusArr[$ssvv]["HandInStatusTitleB5"], $handInStatusArr[$ssvv]["HandInStatusTitleEN"]) . "</span>";
													} else if ($sys_custom['eHomework_Status_Supplementary_WitTwoHandInStatusManage'] && $ssvv == "7") {
														echo "<span style='color:#ff0000;'>" . Get_Lang_Selection($handInStatusArr[-20]["HandInStatusTitleB5"], $handInStatusArr[-20]["HandInStatusTitleEN"]) . "</span>";
													} else {
														echo "-";
													}
												} else {
												*/
													if(!$lhomework->ViewOnly || $_SESSION['SSV_PRIVILEGE']['homework']['is_subject_leader'] || !$_SESSION['isTeaching']) {
														if ($ssvv == "-20" &&
														    !in_array($vv["RecordStatus"], array(-1, -12, -13)) &&
														    !$sys_custom['eHomework_Status_Supplementary_WitTwoHandInStatusManage']
													    )
														{
															echo "-";
														} else {
														    if ($ssvv == "-20" && !($vv["CancelViolationDate"] == "0000-00-00 00:00:00" || empty($vv["CancelViolationDate"]))
														        && !empty($vv["HWViolationID"]))
														    {
														        if (isset($eDisRecordsArr[$vv["StudentID"]][$vv["RecordID"]]["DateInput"]))
														        {
    																echo $eDisRecordsArr[$vv["StudentID"]][$vv["RecordID"]]["DateInput"];
														        } else {
														            if ($vv["SuppRecordStatus"] == "-20")
														            {
														                echo $vv["SuppRecordDate"];
														            } else {
														                echo "-";
														            };
														        }
															} else {
															    if ($ssvv == "-20" && isset($eDisRecordsArr[$vv["StudentID"]][$vv["RecordID"]]["DateInput"]))
															    {
															        echo $eDisRecordsArr[$vv["StudentID"]][$vv["RecordID"]]["DateInput"];
															    } else {
    															    if ($vv["StudentStatus"] == 3 || $mustBeTransferToDiscipline) {
    																    echo "<input type='checkbox' disabled value='" . $ssvv. "'" . $strChecked . " name='handinRec[" . $vv["RecordID"] . "]' class='statusRd row_" . $index . " col_" . $sskk . "' data-row='row_" . $index . "' data-col='col_" . $sskk . "'>";
    															    } else {
    															        echo "<input type='checkbox' value='" . $ssvv. "'" . $strChecked . " name='handinRec[" . $vv["RecordID"] . "]' class='statusRd row_" . $index . " col_" . $sskk . "' data-row='row_" . $index . "' data-col='col_" . $sskk . "'>";
    															    }
															    }
															}
														}
													} else {
														if (!empty($vv["SuppRecordStatus"])) {
															$tmp_name = Get_Lang_Selection($handInStatusArr[$vv["SuppRecordStatus"]]["HandInStatusTitleB5"], $handInStatusArr[$vv["SuppRecordStatus"]]["HandInStatusTitleEN"]);
														}
														echo $tmp_name;
													}
												/* } */
												echo "</td>\n";
											}
										}
									}
									if ($sys_custom['eHomework_Status_Supplementary_WitHandInStatusManage'] && $sys_custom['eHomework_Status_Supplementary_WitTwoHandInStatusManage'] ||
									    $sys_custom['eHomework_allow_cancel_conduct_button']) {
										if(!$lhomework->ViewOnly || $_SESSION['SSV_PRIVILEGE']['homework']['is_subject_leader'] || !$_SESSION['isTeaching']) {
										    if (($vv["CancelViolationDate"] == "0000-00-00 00:00:00" || empty($vv["CancelViolationDate"]))
										        && !empty($vv["HWViolationID"]) )
											{
											    if ($vv["StudentStatus"] == 3 || $mustBeTransferToDiscipline) {
											        echo "<td class='tabletext tablerow' align='center'>-</td>";
											    } else {
											        if (isset($eDisRecordsArr[$vv["StudentID"]][$vv["RecordID"]]["DateInput"])) {
											             echo "<td class='tabletext tablerow' align='center'><button class='cancelViolationBtn' rel='" . $vv["RecordID"] . "|" . $vv["StudentID"] . "|" . $vv["HomeworkID"] . "'>" . $Lang['Btn']['Cancel'] . "</button></td>\n";
											        } else {
											            echo "<td class='tabletext tablerow' align='center'>-</td>";
											        }
											    }
											} else {
												if ($vv["CancelViolationStatus"] === "1") {
													echo "<td class='tabletext tablerow' align='center'>" . $vv["CancelViolationDate"] . "</td>\n";
												} else {
													echo "<td class='tabletext tablerow' align='center'>-</td>\n";
												}
											}
										}
									}
									echo "</tr>\n";
								}
							} else {
								echo "<tr>\n";
								echo "<td colspan='" . $tdCount. "' height='80' align='center'>" . $Lang['SysMgr']['Homework']['NoRecord'] . "</td>\n";
								echo "</tr>\n";
							}
?>
							</tbody>
							</table>
							<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>"/>
							<input type="hidden" name="order" value="<?php echo $li->order; ?>"/>
							<input type="hidden" name="field" value="<?php echo $li->field; ?>"/>
							<input type="hidden" name="numPerPage" value="<?=$li->page_size?>"/>
							<input type="hidden" name="page_size_change" value=""/>
							<input type="hidden" name="s" value="<?php echo $s; ?>"/>
							<!--input type="hidden" name="cid" value="<?=$childrenID?>"/-->
							<input type="hidden" name="yid" value="<?=$yearID?>"/>
							<input type="hidden" name="ytid" value="<?=$yearTermID?>"/>
							<input type="hidden" name="sid" value="<?=$subjectID?>"/>
							<input type="hidden" name="history" value="1"/>
							<input type="hidden" name="isUpdate" value="0"/>
							<input type="hidden" name="cancelRec" value=""/>
							<input type="hidden" name="currDate" value="<?php echo date("Y-m-d"); ?>"/>
							</form>
<?php
						}
?>
						</td>
					</tr>
					</tbody>
				</table><br>
				<?php if (count($handin_studentsArr) > 0) {
				    echo $Lang['SysMgr']['FormClassMapping']['DeletedUserLegend'];
                } ?>
			</td>
		</tr>
<?php
	
	if ($required && count($handin_studentsArr) > 0) {
?>
		<!-- Button //-->
		<tr>
			<td>
			
				<?php $totalCount = 1; if($totalCount!=0) { ?>
					<table width="98%" border="0" cellspacing="0" cellpadding="2">
						<tbody>
						<tr>
							<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
						</tr>
						
						<tr>
							<td align="center">
								<?php
								if (!$mustBeTransferToDiscipline)
								{
								    
								    echo $linterface->GET_ACTION_BTN($button_submit, "button", "checkform(document.form1)");
							    }
							    ?>
							</td>
						</tr>
						</tbody>
					</table>
				<? } ?>
			</td>
		</tr>
<?php
	}
?>
		</tbody>
	</table>
<?
	$linterface->LAYOUT_STOP();
	intranet_closedb();
?>