<?php
// Modifying by: 

##### Change Log [Start] #####
#
#	Date:	2014-09-22 (YatWoon)
#			add flag checking $sys_custom['eHomework']['HideClearHomeworkRecords'] [Case#F68177]
#			Deploy: IPv10.1
#
#	Date	:	2010-12-01 (Henry Chow)
#				1) Remove the column of "Subject Code" in Import file
#				2) Add [Ref] column "Subject Group Name"
#
###### Change Log [End] ######

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

intranet_auth();
intranet_opendb();

$lhomework = new libhomework2007();

if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || (!$_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"]  && !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] && !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_subject_leader"] && ($_SESSION['isTeaching'] && $lhomework->ViewOnly)))
{
	if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])
	{
		header("location: ../../settings/index.php");	
		exit;
	}
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}


# Temp Assign memory of this page
ini_set("memory_limit", "150M");

if($yearID=="") $yearID = Get_Current_Academic_Year_ID();
if($yearTermID=="") $yearTermID = getCurrentSemesterID();


$linterface = new interface_html();
$ayt = new academic_year_term($yearTermID);

$CurrentPageArr['eAdminHomework'] = 1;

# menu highlight setting
$CurrentPage = "Management_HomeworkList";

$PAGE_TITLE = $Lang['SysMgr']['Homework']['HomeworkList'];


# tag information 
$TAGS_OBJ[] = array($Lang['SysMgr']['Homework']['ToDoList'], "index.php?yearID=$yearID&yearTermID=$yearTermID&subjectID=$subjectID&subjectGroupID=$sgid&sid=$sid", 1);
$TAGS_OBJ[] = array($Lang['SysMgr']['Homework']['History'], "history.php?yearID=$yearID&yearTermID=$yearTermID&subjectID=$subjectID&subjectGroupID=$sgid&sid=$sid", 0);
if($_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] && !$sys_custom['eHomework']['HideClearHomeworkRecords'])
	$TAGS_OBJ[] = array($Lang['SysMgr']['Homework']['ClearHomework'], "clear_homework.php", 0);

$MODULE_OBJ = $lhomework->GET_MODULE_OBJ_ARR();

# step information
$STEPS_OBJ[] = array($i_general_select_csv_file, 1);
$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 0);
$STEPS_OBJ[] = array($i_general_imported_result, 0);

# page netvigation
$PAGE_NAVIGATION[] = array($Lang['SysMgr']['Homework']['HomeworkList'], "index.php?yearID=$yearID&yearTermID=$yearTermID&subjectID=$subjectID&subjectGroupID=$subjectGroupID");
$PAGE_NAVIGATION[] = array($Lang['SysMgr']['Homework']['ImportRecord'], "");

$ary = getSemesters($yearID);
$yearTermAry = array();
$i = 0 ;
if(sizeof($ary)>0) {
	foreach($ary as $key=>$val) {
		$yearTermAry[$i][] = $key;
		$yearTermAry[$i][] = $val;
		$i++;
	}	
}

$semSelect = getSelectByArray($yearTermAry, 'name="yearTermID" id="yearTermID" onChange="Hide_Window(\'ref_list\')"', $yearTermID, 0,0);

# sample file
if($lhomework->useHomeworkCollect && $lhomework->useHomeworkType)
	$sample_file = "sample_import_homework_collect_hwType_unicode.csv";
else if($lhomework->useHomeworkCollect)
	$sample_file = "sample_import_homework_collect_unicode.csv";
else if($lhomework->useHomeworkType)
	$sample_file = "sample_import_homework_hwType_unicode.csv";
else
	$sample_file = "sample_import_homework_unicode.csv";
	
	
$csvFile = "<a class=\"tablelink\" href=\"get_sample_file.php?file=$sample_file\" target=\"_self\">".$Lang['General']['ClickHereToDownloadSample']."</a>";

# instruction
$format_str .= "<ol>
				<li>$i_Homework_import_instruction1</li>
				<li>$i_Homework_import_instruction2</li>
				<li>$i_Homework_import_instruction3</li>
				</ol>";

# instruction
$csv_format = "";
$delim = "<br>";

for($i=0; $i<sizeof($Lang['SysMgr']['Homework']['ImportHomework']); $i++){
	$csv_format .= $delim.$Lang['SysMgr']['Homework']['Column']." ".($i+1)." : ".$Lang['SysMgr']['Homework']['ImportHomework'][$i];
}


if($lhomework->useHomeworkCollect)
{
	$csv_format .= $delim.$Lang['SysMgr']['Homework']['Column']." ".($i+1)." : ".$Lang['SysMgr']['Homework']['ImportHomework_Col10'];
	$i++;
}
if($lhomework->useHomeworkType) {
	$csv_format .= $delim.$Lang['SysMgr']['Homework']['Column']." ".($i+1)." : ".$Lang['SysMgr']['Homework']['ImportHomework_Col11'];
	$i++;
}

$reference_str = "<div id=\"ref_list\" style='position:absolute; height:150px; z-index:1; visibility: hidden;'></div>";

$reference_str .= "<a class=\"tablelink\" href=javascript:show_ref_list('subjectGroupList','cc_click')>".$Lang['SysMgr']['Homework']['SubjectGroupCode']."</a><span id=\"cc_click\">&nbsp;</span>";
/*
$reference_str .= ",
				<a class=\"tablelink\" href=javascript:show_ref_list('subjectList','sc_click')>".$Lang['SysMgr']['Homework']['SubjectCode']."</a>";
				*/
if($lhomework->useHomeworkType)
	$reference_str .= ", <a class=\"tablelink\" href=javascript:show_type_list('homeworkTypeList','ht_click')>".$i_Homework_HomeworkType."</a> <span id=\"ht_click\">&nbsp;</span>";

$reference_str .= "<span id=\"sc_click\">&nbsp;</span></p>";	

$linterface->LAYOUT_START();
?>
<script language="JavaScript">
var ClickID = '';
var callback_show_ref_list = {
	success: function ( o )
    {
	    var tmp_str = o.responseText;
	    document.getElementById('ref_list').innerHTML = tmp_str;
	    DisplayPosition();
	}
}


function show_ref_list(type,click)
{
	ClickID = click;
	document.getElementById('task').value = type;
	obj = document.form1;
	YAHOO.util.Connect.setForm(obj);
	var path = "ajax_gm.php";
    var connectionObject = YAHOO.util.Connect.asyncRequest("POST", path, callback_show_ref_list);
}

var callback_homework_type_list = {
	success: function ( o )
    {
	    var tmp_str = o.responseText;
	    document.getElementById('ref_list').innerHTML = tmp_str;
	    DisplayPosition();
	}
}

function show_type_list(type,click)
{
	ClickID = click;
	document.getElementById('task').value = type;
	obj = document.form1;
	YAHOO.util.Connect.setForm(obj);
	var path = "ajax_gm.php";
    var connectionObject = YAHOO.util.Connect.asyncRequest("POST", path, callback_homework_type_list);
}

function Hide_Window(pos){
  document.getElementById(pos).style.visibility='hidden';
}

function getPosition(obj, direction)
{
	var objStr = "obj";
	var pos_value = 0;
	while (typeof(eval(objStr))!="undefined" && eval(objStr + ".tagName")!="BODY")
	{
		pos_value += eval(objStr + "." + direction);
		objStr += ".offsetParent";
	}

	return pos_value;
}

function DisplayPosition(){
	
  document.getElementById('ref_list').style.left=getPosition(document.getElementById(ClickID),'offsetLeft') +10;
  document.getElementById('ref_list').style.top=getPosition(document.getElementById(ClickID),'offsetTop');
  document.getElementById('ref_list').style.visibility='visible';
}
</script>
<br />

<table width="100%" border="0" cellspacing="4" cellpadding="4">
	<tr>
		<td class="navigation">
			<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
			<tr><td><?=$linterface->GET_NAVIGATION4($PAGE_NAVIGATION)?></td></tr>
			</table>
		</td>
	</tr>
</table>

<form name="form1" method="post" action="import_result.php" enctype="multipart/form-data">
	<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td align="right" colspan="2"><?=$linterface->GET_SYS_MSG($xmsg, $xmsg2);?></td>
		</tr>
		
		<tr>
			<td>
				<table width="100%" border="0" cellpadding="3" cellspacing="0">
					<tr>
						<td colspan="2"><?=$linterface->GET_STEPS($STEPS_OBJ)?></td>
					</tr>
					
					<tr>
						<td colspan="2"></td>
					</tr>
					
					<tr>
						<td colspan="2">
							<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
							<tr>
								<td class="formfieldtitle" width="30%" align="left"><?=$Lang['SysMgr']['Homework']['AcademicYear']?> </td>
								<td class="tabletext" ><?=$ayt->Get_Academic_Year_Name()?></td>
							</tr>
							
							<tr>
								<td class="formfieldtitle" width="30%" align="left"><?=$Lang['SysMgr']['Homework']['YearTerm']?> </td>
								<td class="tabletext" ><?/*=$ayt->Get_Year_Term_Name()*/?><?=$semSelect?></td>
							</tr>
								
								<tr>
									<td class="formfieldtitle" align="left"><?=$Lang['General']['SourceFile']?>
										<span class="tabletextremark">
											<?=$Lang['General']['CSVFileFormat']?>
										</span>
									</td>
									<td class="tabletext"><input class="file" type="file" name="csvfile" id="csvfile">
									</td>
								</tr>
								
								<tr>
									<td class="formfieldtitle" align="left"><?=$Lang['General']['CSVSample']?></td>
									<td class="tabletext"><?=$csvFile?></td>
								</tr>
								
								<tr>
									<td class="formfieldtitle" align="left"><?=$Lang['SysMgr']['Homework']['Import']['FieldTitle']['DataColumn']?></td>
									<td class="tabletext"><?=$csv_format?></td>
								</tr>
								
								<tr>
									<td class="formfieldtitle" align="left"><?=$Lang['SysMgr']['Homework']['Import']['FieldTitle']['Reference']?></td>
									<td class="tabletext"><?=$reference_str?></td>
								</tr>
								
								<tr>
									<td class="tabletextremark" colspan="2"><?=$Lang['SysMgr']['Homework']['Import']['FieldTitle']['RequiredField']?></td>
								</tr>
								
								<tr>
									<td class="tabletextremark" colspan="2"><?=$Lang['eHomework']['ImportWorkload_Desription']?></td>
								</tr>
								
								<tr>
									<td class="tabletextremark" colspan="2"><?=$Lang['SysMgr']['Homework']['Import']['FieldTitle']['DateFormat']?></td>
								</tr>
							</table>
						</td>
					</tr>
				
					<tr>
						<td colspan="2">
							<table width="95%" border="0" cellpadding="0" cellspacing="0" align="center">
								<tr>
									<td colspan="3" class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1" /></td>
								</tr>
							</table>
						</td>
					</tr>
					
					<tr>
						<td align="center" colspan="2">
							<?= $linterface->GET_ACTION_BTN($button_continue, "submit") ?>&nbsp;
							<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "self.location='index.php?subjectID=$subjectID&subjectGroupID=$subjectGroupID'") ?>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
<input type="hidden" id="task" name="task"/>
<input type="hidden" name="yearID" value="<?=$yearID?>"/>
<!--<input type="hidden" name="yearTermID" value="<?=$yearTermID?>"/>-->
<input type="hidden" name="subjectID" value="<?=$subjectID?>"/>
<input type="hidden" name="subjectGroupID" value="<?=$subjectGroupID?>"/>
</form>
<br />
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
