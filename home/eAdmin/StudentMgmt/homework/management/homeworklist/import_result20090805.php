<?
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"] || !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"]){
	if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])
	{
		header("location: ../../settings/index.php");	
		exit;
	}
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

intranet_auth();
intranet_opendb();

# Temp Assign memory of this page
ini_set("memory_limit", "150M");

$lhomework = new libhomework2007();
$linterface = new interface_html();
$lu = new libuser();

$CurrentPageArr['eAdminHomework'] = 1;

# menu highlight setting
$CurrentPage = "Management_HomeworkList";

$PAGE_TITLE = $Lang['SysMgr']['Homework']['HomeworkList'];

# tag information
$TAGS_OBJ[] = array($Lang['SysMgr']['Homework']['HomeworkList'], ""); 

$MODULE_OBJ = $lhomework->GET_MODULE_OBJ_ARR();

# step information
$STEPS_OBJ[] = array($i_general_select_csv_file, 0);
$STEPS_OBJ[] = array('Confirmation', 1);
$STEPS_OBJ[] = array($i_general_imported_result, 0);

# page netvigation
$PAGE_NAVIGATION[] = array($Lang['SysMgr']['Homework']['HomeworkList'], "index.php?subjectID=$subjectID&subjectGroupID=$subjectGroupID");
$PAGE_NAVIGATION[] = array($Lang['SysMgr']['Homework']['ImportRecord'], "");


$limport = new libimporttext();
$lo = new libfilesystem();
$name = $_FILES['csvfile']['name'];
$ext = strtoupper($lo->file_ext($name));

if($ext != ".CSV")
{
	header("location: import.php?xmsg=import_failed");
	exit();
}

$data = $limport->GET_IMPORT_TXT($csvfile);
if(is_array($data))
{
	$col_name = array_shift($data);
}

$file_format = array("Subject Code", "Subject Group Code", "Topic", "Workload", "Pic", "Start Date", "Due Date", "Description", "Hand-in Required");
$format_wrong = false;

for($i=0; $i<sizeof($file_format); $i++)
{
	if ($col_name[$i]!=$file_format[$i])
	{
		$format_wrong = true;
		break;
	}
}
if($format_wrong)
{
	header("location: import.php?xmsg=wrong_header");
	exit();
}


### List out the import result
$x .= "<table width=\"95%\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\">";
$x .= "<tr>";
$x .= "<td width='4%' class='tablebluetop tabletopnolink'>#</td>";
$x .= "<td width='8%' class='tablebluetop tabletopnolink'>".$Lang['SysMgr']['Homework']['Subject']."</td>";
$x .= "<td width='12%' class='tablebluetop tabletopnolink'>".$Lang['SysMgr']['Homework']['SubjectGroup']."</td>";
$x .= "<td width='18%' class='tablebluetop tabletopnolink'>".$Lang['SysMgr']['Homework']['Topic']."</td>";
$x .= "<td width='10%' class='tablebluetop tabletopnolink'>".$Lang['SysMgr']['Homework']['Teacher']."</td>";
$x .= "<td width='12%' class='tablebluetop tabletopnolink'>".$Lang['SysMgr']['Homework']['StartDate']."</td>";
$x .= "<td width='12%' class='tablebluetop tabletopnolink'>".$Lang['SysMgr']['Homework']['DueDate']."</td>";
$x .= "<td width='8%' class='tablebluetop tabletopnolink'>".$Lang['SysMgr']['Homework']['HandinRequired']."</td>";
$x .= "<td width='24%' class='tablebluetop tabletopnolink'>".$Lang['SysMgr']['Homework']['Error']."</td>";
$x .= "</tr>";


$tempData = array();
$allClassCode = $lhomework->getAllClassCode();
$allSubjectsCodeID = $lhomework->getAllSubjectCodeID();
$teachingGroupSubject = $lhomework->getTeachingGroupSubject($UserID);
	
for($i=0;$i<sizeof($data);$i++)
{
	if(!is_array($data[$i]) || trim(implode("",$data[$i]))=="") 
		continue;  // skip empty line	                 
	
	$failed = false;
	$error_occured = 0;
	
	$x .= $lhomework->checkImportError($allClassCode, $allSubjectsCodeID, $teachingGroupSubject, $data[$i], $i);
	
	list($subject, $subjectGroup, $title,$loading,$pic,$start,$due,$description,$handin) = $data[$i];
	
	$subjectGroup = intranet_htmlspecialchars($subjectGroup);
	$subject = intranet_htmlspecialchars($subject);
	$title = intranet_htmlspecialchars($title);
	$description = intranet_htmlspecialchars($description);
	$handin_required = intranet_htmlspecialchars($handin);
	
	if(strtoupper($handin_required)=='YES')
		$handin_required=1;
	else
		$handin_required=0;
	
	$tempData[$i][0] = $subjectGroup;
	$tempData[$i][1] = $subject;
	$tempData[$i][2] = $start;
	$tempData[$i][3] = $due;
	$tempData[$i][4] = $loading;
	$tempData[$i][5] = $pic;
	$tempData[$i][6] = $title;
	$tempData[$i][7] = $description;
	$tempData[$i][8] = $handin_required;
	$_SESSION['tempData'] = $tempData;
}

$x .= "</table>";

if($lhomework->homeworkListImportError!=0)
{
	$import_button = $linterface->GET_ACTION_BTN($button_back, "button", "window.location='import.php?subjectID=$subjectID&subjectGroupID=$subjectGroupID'");
}
else
{	
	$import_button = $linterface->GET_ACTION_BTN($button_import, "submit")." &nbsp;".$linterface->GET_ACTION_BTN($button_back, "button", "window.location='import.php'");	
}

$linterface->LAYOUT_START();
?>

<br />

<table width="100%" border="0" cellspacing="4" cellpadding="4">
	<tr>
		<td class="navigation">
			<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
			<tr><td><?=$linterface->GET_NAVIGATION4($PAGE_NAVIGATION)?></td></tr>
			</table>
		</td>
	</tr>
</table>

<form name="form1" method="post" action="import_update.php" enctype="multipart/form-data">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
</tr>
<tr>
	<td align="center">
		<?=$x?>
	</td>
</tr>
<tr>
	<td>
		<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr>
			<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
		</tr>
		<tr>
			<td align="center" colspan="2">
			<?=$import_button?>
			</td>
		</tr>
		</table>
	</td>
</tr>
</table>
	<input type="hidden" name="tempData" value="<?php echo $tempData; ?>"/>
	<input type="hidden" name="subjectID" value="<?=$subjectID?>"/>
	<input type="hidden" name="subjectGroupID" value="<?=$subjectGroupID?>"/>
</form>
<br />

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>