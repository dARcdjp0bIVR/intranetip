<?php
# modifying by : henry

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || !($_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"] || $_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"]))
{
	if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])
	{
		header("location: ../../settings/index.php");	
		exit;
	}
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}
//debug_pr($_POST);

$linterface = new interface_html();
$lhomework = new libhomework2007();

$CurrentPageArr['eAdminHomework'] = 1;
$yearID = Get_Current_Academic_Year_ID();
$today = date('Y-m-d');

/*
# Current Year Term
$currentYearTerm = getAcademicYearAndYearTermByDate($today);
$yearTermID = $currentYearTerm[0];
*/
if($yearTermID=="")
	$yearTermID = 0;

if($task=='subjectGroupList')
{
	$output = '<table width="90%" border="0" cellpadding="0" cellspacing="0">';
	$sql = "SELECT a.ClassCode, 
			IF('$intranet_session_language' = 'en', a.ClassTitleEN, a.ClassTitleB5) AS SubjectGroup,
			IF('$intranet_session_language' = 'en', c.EN_DES, c.CH_DES) AS Subject
			FROM SUBJECT_TERM_CLASS AS a 
			INNER JOIN SUBJECT_TERM AS b ON b.SubjectGroupID = a.SubjectGroupID
			INNER JOIN ASSESSMENT_SUBJECT AS c ON c.RecordID = b.SubjectID
			INNER JOIN ACADEMIC_YEAR_TERM AS d ON d.YearTermID = b.YearTermID 
			INNER JOIN ACADEMIC_YEAR AS e ON e.AcademicYearID = d.AcademicYearID
			WHERE e.AcademicYearID = $yearID AND d.YearTermID = $yearTermID
			ORDER BY Subject";
	
    $array = $lhomework->returnArray($sql);
	
	$data = "<table width=\"90%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#CCCCCC\" class=\"layer_table_detail\">";
		$data .="<tr class=\"tabletop\">
					<td>#</td>
					<td>".$Lang['SysMgr']['Homework']['SubjectGroupCode']."</td>
					<td>".$Lang['SysMgr']['Homework']['Subject']."</td>
					<td>".$Lang['SysMgr']['Homework']['SubjectGroup']."</td>
				</tr>";
		
		for($a=0;$a<sizeof($array);$a++)
		{
			$subjectGroupCode = intranet_htmlspecialchars($array[$a]['ClassCode']);
			$subject = intranet_htmlspecialchars($array[$a]['Subject']);
			$subjectGroup = intranet_htmlspecialchars($array[$a]['SubjectGroup']);
			if($subjectGroupCode!='')
			{
				$data .="<tr class=\"tablerow1\">
							<td>".($a+1)."</td>
							<td>".$subjectGroupCode."</td>
							<td>".$subject."</td>
							<td>".$subjectGroup."</td>
						</tr>";
			}
		}
	$data .="</table>";	
}

else if($task=='subjectList')
{	
	$output = '<table width="90%" border="0" cellpadding="0" cellspacing="0">';
	/*	commented by Henry@20100310 since SUBJECTs are not term base
	
	$sql = "SELECT b.CODEID, 
			IF('$intranet_session_language' = 'en', b.EN_DES, b.CH_DES) AS Subject 
			FROM SUBJECT_TERM AS a
			INNER JOIN ASSESSMENT_SUBJECT AS b ON b.RecordID = a.SubjectID
			INNER JOIN ACADEMIC_YEAR_TERM AS c ON c.YearTermID = a.YearTermID 
			INNER JOIN ACADEMIC_YEAR AS d ON d.AcademicYearID = c.AcademicYearID
			WHERE d.AcademicYearID = $yearID AND c.YearTermID = $yearTermID AND b.RecordStatus = 1 AND (CMP_CODEID is NULL OR CMP_CODEID='') 
			GROUP BY b.CODEID ORDER BY b.CODEID";
	*/
	$sql = "SELECT b.CODEID, 
			IF('$intranet_session_language' = 'en', b.EN_DES, b.CH_DES) AS Subject 
			FROM ASSESSMENT_SUBJECT AS b 
			WHERE b.RecordStatus = 1 AND (CMP_CODEID is NULL OR CMP_CODEID='') 
			GROUP BY b.CODEID ORDER BY b.CODEID";

	$array = $lhomework->returnArray($sql);
	
	$data = "<table width=\"90%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#CCCCCC\" class=\"layer_table_detail\">";
		$data .="<tr class=\"tabletop\">
					<td>#</td>
					<td>".$Lang['SysMgr']['Homework']['SubjectCode']."</td>
					<td>".$Lang['SysMgr']['Homework']['Subject']."</td>
				</tr>";
		
		for($a=0;$a<sizeof($array);$a++)
		{
			$SubjectCode = intranet_htmlspecialchars($array[$a]['CODEID']);
			$SubjectName = intranet_htmlspecialchars($array[$a]['Subject']);
			
			if($SubjectCode!='')
			{
				$data .="<tr class=\"tablerow1\">
							<td>".($a+1)."</td>
							<td>".$SubjectCode."</td>
							<td>".$SubjectName."</td>
						</tr>";
			}
		}
	$data .="</table>";	
}

else if($task=='homeworkTypeList')
{	
	$output = '<table width="90%" border="0" cellpadding="0" cellspacing="0">';
	$sql = "SELECT TypeID, TypeName FROM INTRANET_HOMEWORK_TYPE WHERE RecordStatus=1 ORDER BY DisplayOrder";
	$array = $lhomework->returnArray($sql);

	$data = "<table width=\"90%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#CCCCCC\" class=\"layer_table_detail\">";
		$data .="<tr class=\"tabletop\">
					<td>#</td>
					<td>$i_Homework_HomeworkType</td>
					<td>$i_Homework_HomeworkType</td>
				</tr>";
		
		for($a=0;$a<sizeof($array);$a++)
		{
				$data .="<tr class=\"tablerow1\">
							<td>".($a+1)."</td>
							<td>".$array[$a]['TypeID']."</td>
							<td>".intranet_htmlspecialchars($array[$a]['TypeName'])."</td>
						</tr>";			
		}
	$data .="</table>";	
}

$output .= '<tr>
				<td height="19"><table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
					<td width="5" height="19"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_01.gif" width="5" height="19"></td>
					<td height="19" valign="middle" background="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_02.gif">&nbsp;</td>
					<td width="19" height="19"><a href="javascript:Hide_Window(\'ref_list\')"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_close_off.gif" name="pre_close1" width="19" height="19" border="0" id="pre_close1"></a></td>
				  </tr>
				</table></td>
			  </tr>
			  <tr>
				<td><table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
					<td width="5" background="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_04.gif"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_04.gif" width="5" height="19"></td>
					<td align="left" bgcolor="#FFFFF7"><table width="100%" border="0" cellspacing="0" cellpadding="0">
					  
					  <tr>
						<td height="150" align="left" valign="top">
							<div id="list" style="overflow: auto; z-index:1; height: 150px;">
								'.$data.'
							</div>
						</td></tr>
							  <tr>
						    <td><br><table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
						      <tr>
							       <td height="1" class="dotline"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/10x10.gif" width="10" height="1"></td>
						      </tr>
						     </table></td>
						     </tr>
                 </table></td>
					<td width="6" background="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_06.gif"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_06.gif" width="6" height="6"></td>
				  </tr>
				  <tr>
					<td width="5" height="6"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_07.gif" width="5" height="6"></td>
					<td height="6" background="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_08.gif"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_08.gif" width="5" height="6"></td>
					<td width="6" height="6"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_09.gif" width="6" height="6"></td>
				  </tr>
					</table></td>
			  </tr>
			</table>';
echo $output;
?>
