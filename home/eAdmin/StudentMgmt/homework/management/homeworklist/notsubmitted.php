<?php
// Modifing by : 

##### Change Log [Start] ######
#
#	Date:	2019-05-08 (Bill)
#           prevent SQL Injection + Cross-site Scripting
#
#   Date:   2018-05-14 (Anna)
#           when $studentStatus leave school, don't insert into INTRANET_HOMEWORK_HANDIN_LIST
#
#	Date:	2015-07-16 (Shan)
#			Update serachbox style, add $(document).ready( function()
#
#	Date:	2014-09-22 (YatWoon)
#			add flag checking $sys_custom['eHomework']['HideClearHomeworkRecords'] [Case#F68177]
#			Deploy: IPv10.1
#
#	Date:	2014-09-04 (Bill)
#			add Last Modified
#
#	Date:	2011-08-02 (Henry Chow)
#			add checking on $lhomework->ClassTeacherCanViewHomeworkOnly
#
#	Date:	2011-04-06 (Henry Chow)
#			add checking on $lhomework->exportAllowed while form submission
#
#	Date:	2010-10-08 Henry Chow
#			if student is eHomework admin, then also can see all filters' data
#
#	Date	:	2010-10-08 Henry Chow
#				pass 1 more parameter "classID" to add.php when adding homework
#
###### Change Log [End] #######

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");

intranet_auth();
intranet_opendb();

$lhomework = new libhomework2007();

# check access right
if(
	$_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"]
	|| !$sys_custom['eHomework_Status_Supplementary_WitHandInStatusManage']
	|| (!$_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"] 
	&& !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"]
	&& !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_subject_leader"]
	&& !$lhomework->isViewerGroupMember($UserID))
)
{
	if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])
	{
		header("location: ../../settings/index.php");
		exit;
	}

	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$linterface = new interface_html();

/******************************/
$handInStatusArr = $lhomework->getAllHandInStatus();
/******************************/

$lclass = new libclass();
# change page size
if ($page_size_change == 1)
{
	setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
	$ck_page_size = $numPerPage;
}

/**********************************/
function resetSubjectGroupID($subjectGroupID, $subjectGroups)
{
	for ($i=0; $i<sizeof($subjectGroups); $i++)
	{
		if ($subjectGroupID==$subjectGroups[$i][0])
		{
			return $subjectGroupID;
		}
	}
	return "";
}
/**********************************/
if (isset($ck_page_size) && $ck_page_size != "")
	$page_size = $ck_page_size;

	$pageSizeChangeEnabled = true;

	/*
	 # Table initialization
	 $order = ($order == "") ? 1 : $order;
	 $field = ($field == "") ? $sortField : $field;
	 $pageNo = ($pageNo == "") ? 1 : $pageNo;
	 */

	# Create a new dbtable instance
	$li = new libdbtable2007($field, $order, $pageNo);

	# Settings
	$s = addcslashes($s, '_');
	/*
	 $searchByTeacher =($lhomework->teacherSearchDisabled == 0) ? "or a.PosterName like '%$s%'" : "" ;
	 $searchBySubject =($lhomework->subjectSearchDisabled == 0) ? " or IF('$intranet_session_language' = 'en', b.EN_DES, b.CH_DES) like '%$s%'" : "" ;
	 */
	$allowExport = $lhomework->exportAllowed;
    
	// [2015-0901-1009-21066] fixed: drop down list filtering not work, use data from $_GET[] only if $_POST[] is not set
	$yearID = isset($_POST['yearID'])? $_POST['yearID'] : $_GET['yearID'];
	$yearTermID = isset($_POST['yearTermID'])? $_POST['yearTermID'] : $_GET['yearTermID'];
	$subjectID = isset($_POST['subjectID'])? $_POST['subjectID'] : $_GET['subjectID'];
	$subjectGroupID = isset($_POST['subjectGroupID'])? $_POST['subjectGroupID'] : $_GET['subjectGroupID'];
	$sid = isset($_POST['sid'])? $_POST['sid'] : $_GET['sid'];
	
	### Handle SQL Injection + XSS [START]
	$yearID = IntegerSafe($yearID);
	$yearTermID = IntegerSafe($yearTermID);
	$subjectID = IntegerSafe($subjectID);
	$subjectGroupID = IntegerSafe($subjectGroupID);
	$classID = IntegerSafe($classID);
	$sid = IntegerSafe($sid);
	$sgid = IntegerSafe($sgid);
	### Handle SQL Injection + XSS [END]
	
	# class menu
	if($_SESSION['UserType']!=USERTYPE_STUDENT) {
	    # eHomework Admin
		// [2015-1117-1137-20073] Viewer Group member should able to select all classes
		if(($_SESSION['UserType']==USERTYPE_STAFF && !$_SESSION['isTeaching']) || $_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] || $lhomework->isViewerGroupMember($UserID)) {
			$classes = $lhomework->getAllClassInfo();
		}
		# Class teacher / subject teacher
		else {
			$classes = $lhomework->getAllClassesInvolvedByTeacherID(Get_Current_Academic_Year_ID(), GetCurrentSemesterID(), $UserID);
		}
		// $selectClass = getSelectByArray($classes, 'name="classID" id="classID" onChange="document.form1.submit();"', $classID, 0, 0, $i_general_all_classes);
		$selectClass = getSelectByArray($classes, 'name="classID" id="classID" onChange="document.form1.submit();"', $classID, 0, 0, $Lang['SysMgr']['Homework']['AllClass']);
	}
    
	# Start layout
	$CurrentPageArr['eAdminHomework'] = 1;
	$CurrentPage = "Management_HomeworkList";
	$PAGE_TITLE = $Lang['SysMgr']['Homework']['HomeworkList'];
	$MODULE_OBJ = $lhomework->GET_MODULE_OBJ_ARR();

	// $query_str = "yearID=" . $yearID . "&yearTermID=" . $yearTermID . "&classID=" . $classID . "&subjectID=" . $subjectID . "&subjectGroupID=" . $sgid . "&sid=" . $sid;
	$query_str = "yearID=" . $yearID . "&yearTermID=" . $yearTermID . "&classID=" . $classID . "&subjectGroupID=" . $sgid . "&sid=" . $sid;
	$TAGS_OBJ = $lhomework->getHomeworkListTabs(basename(__FILE__), $query_str);

	// $htmlAry['searchBox'] = $linterface->Get_Search_Box_Div('text', stripslashes(stripslashes($s)));

	if (isset($_POST["startDate"])) $startDate = $_POST["startDate"];
	else $startDate = date("Y-m-d");
	
	if (isset($_POST["endDate"])) $endDate = $_POST["endDate"];
	else $endDate = date("Y-m-d");
	
	if (isset($_POST["cancelRec"]) && !empty($_POST["cancelRec"])) {
		/*** Cancel Violation Action ***/
		$cancelRecData = $_POST["cancelRec"];
		$cancelInfo = explode("|", $cancelRecData);
		$strSQL = "SELECT CancelViolationStatus, CancelViolationDate FROM INTRANET_HOMEWORK_HANDIN_LIST WHERE RecordID='" . $cancelInfo[0] . "' AND StudentID='" . $cancelInfo[1] . "' AND HomeworkID='" . $cancelInfo[2] . "'";
		$result = $lhomework->returnResultSet($strSQL);
		if (count($result) > 0) {
			$param[0] = array(
					"StudentID" => $cancelInfo[1],
					"HomeworkID" => $cancelInfo[2]
			);
			$strSQL = "UPDATE INTRANET_HOMEWORK_HANDIN_LIST SET CancelViolationStatus='1', CancelViolationDate=NOW() WHERE RecordID='" . $cancelInfo[0] . "' AND StudentID='" . $cancelInfo[1] . "' AND HomeworkID='" . $cancelInfo[2] . "'";
			$lhomework->db_db_query($strSQL);

			$log_cancelInfo = array(
				"RecordID" => $cancelInfo[0],
				"StudentID" => $cancelInfo[1],
				"HomeworkID" => $cancelInfo[2],
				"Remark" => "Cancel from Homework",
			);
			$lhomework->AddCancelMisconductLog($log_cancelInfo);
			
			include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
			$disciplinev12 = new libdisciplinev12();
			$disciplinev12->REQUEST_CANCEL_HW_MISCONDUCT_RECORD($param);
		}
		/*** Cancel Violation Action ***/
		$msg = "update";

	} else if ($_POST["isUpdate"] == "1") {
		/*** Update HandIn Status ***/
		$strSQL = "SELECT 
                        IHHL.RecordID, IHHL.HomeworkID, IHHL.StudentID, IHHL.RecordStatus, IHHL.SuppRecordStatus, IHHL.SuppRecordDate, IHHL.HWViolationID, IHHL.HWViolationDate, IHHL.DateModified
                    FROM 
                        INTRANET_HOMEWORK_HANDIN_LIST as IHHL
                    INNER JOIN
                        INTRANET_USER as IU on (IU.UserID = IHHL.StudentID and IU.RecordStatus != '3')
                    WHERE
                        IHHL.RecordID IN (" . implode(", ", array_keys($_POST["orghandinRec"])) . ")";
		$result = $lhomework->returnResultSet($strSQL);
		
		foreach ($result as $index => $dbhandinRec) {
			$updateRecordID = $dbhandinRec["RecordID"];
			if (isset($_POST["handinRec"][$updateRecordID]) && !empty($_POST["handinRec"][$updateRecordID])) {
				$updateRecordStatus = $_POST["handinRec"][$updateRecordID];
			} else {
				$updateRecordStatus = 5;
			}
			$runUpdate = false;
		    $strSQL = "UPDATE INTRANET_HOMEWORK_HANDIN_LIST SET DateModified=NOW(), MainStatusRecordDate=NOW(), RecordStatus='" . $updateRecordStatus . "', HWViolationDate=''";

			if ($updateRecordStatus != $dbhandinRec["RecordStatus"]) {
				$runUpdate = true;
			} else {
				switch ($updateRecordStatus) {
					case "-1":
						$runUpdate = true;
						break;
				}
			}
			if ($runUpdate) {
				if ($updateRecordStatus == "-1") {
					$strSQL .= ", SuppRecordStatus='-20', SuppRecordDate=NOW() ";
					// $strSQL .= " WHERE RecordID='" . $updateRecordID . "' AND ( SuppRecordStatus NOT IN (7, -20) OR SuppRecordStatus IS NULL )";
					$strSQL .= " WHERE RecordID='" . $updateRecordID . "' AND RecordStatus != '" . $updateRecordStatus . "'";
				} else {
					$strSQL .= " WHERE RecordID='" . $updateRecordID . "' AND RecordStatus != '" . $updateRecordStatus . "'";
				}
				$lhomework->db_db_query($strSQL);
			}
		}
		// exit;
		unset($dbhandinRec);
		unset($result);
		/*** Update HandIn Status ***/
		$msg = "update";
	}
	
	# Teacher Mode with Teaching
	if($_SESSION['UserType']==USERTYPE_STAFF && $_SESSION['isTeaching'])
	{
		# Academic Year
		$academicYear = $lhomework->GetAllAcademicYear();
		$yearID = ($yearID=="")? Get_Current_Academic_Year_ID():$yearID;
		//$yearTerm
		$selectedYear = $lhomework->getCurrentYear("name=\"yearID\" onChange=\"reloadForm()\"", $academicYear, $yearID, "", false);
		if($yearID!=""){
			if($yid!="" && $yid != $yearID){
				$yearTermID="";
			}
				
			# Current Year Term
			$academicYearTerm = $lhomework->getAllAcademicYearTerm($yearID);
			$selectedYearTerm = $lhomework->getCurrentYearTerm("name=\"yearTermID\" onChange=\"reloadForm()\"", $academicYearTerm, $yearTermID, $Lang['SysMgr']['Homework']['AllYearTerms']);
			if($ytid!="" && $ytid != $yearTermID){
				$subjectID="";
			}
			$subjectID = ($subjectID=="")? -1 :$subjectID;
			# Subject Menu
			$UserIDTmp = $_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] ? "" : $UserID;
			$subject = $lhomework->getTeachingSubjectList($UserIDTmp, $yearID, $yearTermID, $classID);
			$selectedSubject = $lhomework->getCurrentTeachingSubjects("name=\"subjectID\" onChange=\"reloadForm();\"", $subject, $subjectID, $Lang['SysMgr']['Homework']['AllSubject'], true);
			if($subjectID != "-1"){
				$subjectGroups = $lhomework->getTeachingSubjectGroupList($UserIDTmp,$subjectID, $yearID, $yearTermID, $classID);
			}
			else
			{
				$subjectGroups = $lhomework->getTeachingSubjectGroupList($UserIDTmp,"", $yearID, $yearTermID, $classID);
			}
			
			$subjectGroupID = resetSubjectGroupID($subjectGroupID, $subjectGroups);
				
			$selectedSubject = $lhomework->getCurrentTeachingSubjects("name=\"subjectID\" onChange=\"reloadForm();\"", $subject, $subjectID, $Lang['SysMgr']['Homework']['AllSubject'], true);
			if ($yearTermID!="" && $yearTermID>0)
			{
				# only display subject group for particular term
				$selectedSubjectGroups = $lhomework->getCurrentTeachingGroups("name=\"subjectGroupID\" onChange=\"reloadForm()\"", $subjectGroups, $subjectGroupID, $Lang['SysMgr']['Homework']['PleaseSelect']);
			} else
			{
				$subjectGroupID = "";
			}
				
			// $filterbar = "$selectedYear $selectedYearTerm $selectClass $selectedSubject";
			$filterbar = "$selectClass $selectedSubject";
		}
	}
	# Teacher Mode with Non Teaching
	else if($_SESSION['UserType']==USERTYPE_STAFF && !$_SESSION['isTeaching'])
	{
		
		# Academic Year
		$academicYear = $lhomework->GetAllAcademicYear();
		$yearID = ($yearID=="")? Get_Current_Academic_Year_ID():$yearID;
		$selectedYear = $lhomework->getCurrentYear("name=\"yearID\" onChange=\"reloadForm()\"", $academicYear, $yearID, "", false);
		if($yearID!=""){
			if($yid!="" && $yid != $yearID){
				$yearTermID="";
			}

			# Current Year Term
			$academicYearTerm = $lhomework->getAllAcademicYearTerm($yearID);
			$selectedYearTerm = $lhomework->getCurrentYearTerm("name=\"yearTermID\" onChange=\"reloadForm()\"", $academicYearTerm, $yearTermID, $Lang['SysMgr']['Homework']['AllYearTerms']);

			if($ytid!="" && $ytid != $yearTermID){
				$subjectID="";
			}
			$subjectID = ($subjectID=="")? -1 :$subjectID;

			# Subject Menu
			$subject = $lhomework->getTeachingSubjectList("", $yearID, $yearTermID, $classID);

			$selectedSubject = $lhomework->getCurrentTeachingSubjects("name=\"subjectID\" onChange=\"reloadForm()\"", $subject, $subjectID, $Lang['SysMgr']['Homework']['AllSubject']);

			if ($subjectID!="" && $yearTermID!="" && $yearTermID>0){

				$sgid = $subjectGroupID;
				if($sid != $subjectID){
					$subjectGroupID="";
				}

				# Subject Group Menu
				$subjectGroups = $lhomework->getTeachingSubjectGroupList("", $subjectID, $yearID, $yearTermID, $classID);

				$subjectGroupID = resetSubjectGroupID($subjectGroupID, $subjectGroups);
					
					
				$selectedSubjectGroups = $lhomework->getCurrentTeachingGroups("name=\"subjectGroupID\" onChange=\"reloadForm()\"", $subjectGroups, $subjectGroupID, $Lang['SysMgr']['Homework']['PleaseSelect']);
			} else
			{
				$subjectGroupID = "";
			}
			// $filterbar = "$selectedYear $selectedYearTerm $selectClass $selectedSubject $selectedSubjectGroups";
			$filterbar = "$selectClass $selectedSubject";
		}
	}
	# Subject Leader
	else if($_SESSION['UserType']==USERTYPE_STUDENT)
	{
		include_once($PATH_WRT_ROOT."includes/libaccessright.php");
		$laccessright = new libaccessright();
		$laccessright->NO_ACCESS_RIGHT_REDIRECT();
		exit;
	}

	$order = ($order == "") ? 1 : $order;
	$field = ($field == "") ? $sortField : $field;
	$pageNo = ($pageNo == "") ? 1 : $pageNo;

	$query_str = "yearID=" . $yearID . "&yearTermID=" . $yearTermID . "&classID=" . $classID . "&subjectID=" . $subjectID . "&subjectGroupID=" . $sgid . "&sid=" . $sid . "&s=" . $s . "&homeworkID=" . $homeworkID;
	if ($_POST["isUpdate"] == "1" || isset($_POST["cancelRec"]) && !empty($_POST["cancelRec"])) {
	    $query_str .= "&startDate=" . $_POST["startDate"] . "&endDate=" . $_POST["endDate"];
	    header("Location: " . basename(__FILE__) . "?" .$query_str . "&updated=1" );
	    exit;
	}
	/******************************/
	$needToTransferToDiscipline = false;
	$mustBeTransferToDiscipline = false;
	
	$latest_modified_date = $lhomework->checkTransferOrNot();
	if ($latest_modified_date !== false) {
	    $needToTransferToDiscipline = true;
	    if ($lhomework->checkUpdateIsMoreThanOneDay($latest_modified_date))
	    {
	        $mustBeTransferToDiscipline = true;
	    }
	}
	/******************************/
	
	$linterface->LAYOUT_START();
	
	$handin_param = array(
			"yearID" => $yearID,
			"yearTermID" => $yearTermID,
			"classID" => $classID,
			"subjectID" => $subjectID,
			"subjectGroupID" => $sgid,
			"subjectID" => $subjectID,
	        "homeworkID" => (isset($_POST["homeworkID"]) ? $_POST["homeworkID"] : (isset($_GET["homeworkID"]) ? $_GET["homeworkID"] : '')),
			"searchTxt" => $s,
			"order" => $order,
			"field" => $field,
			"startDate" => $startDate,
			"endDate" => $endDate,
			"callBy" => basename(__FILE__)
	);
	if ($handin_param["homeworkID"] > 0) {
		$og_data = $lhomework->handinList($handin_param["homeworkID"]);
		$tmp_sprintf = "INSERT INTO INTRANET_HOMEWORK_HANDIN_LIST (HomeworkID,StudentID,RecordStatus,MainStatusRecordDate,DateInput,DateModified) VALUES ('%d', '%d', '%d', NOW(), NOW(), NOW())";
		if (count($og_data) > 0) {
			$RecordStatus = $sys_custom['eHoemwork_default_status'] ? $sys_custom['eHoemwork_default_status'] : -1;
			foreach ($og_data as $i => $og_dataObj) {
			    list($uid,$studentName,$className,$classNumber,$handinStatus,$handinID,$dateModified,$confirmUserID,$confirmTime,$hw_title, $due, $SuppRecordStatus, $studentStatus,$LeaveStudent) = $og_dataObj;
			    if($handinID=="" && $studentStatus != '3') {
					$strSQL = sprintf($tmp_sprintf, $handin_param["homeworkID"], $uid, $RecordStatus);
					$lhomework->db_db_query($strSQL);
				}
			}
		}
		unset($og_data);
		unset($og_dataObj);
		unset($strSQL);
		unset($RecordStatus);
	}
	
	// $relatedGroups = $lhomework->getSubjectGroupIDByTeacherID($UserID, $handin_param["yearID"]);
	// $relatedGroups = Get_Array_By_Key((array)$relatedGroups, "SubjectGroupID");
	$args = array(
			"attr" => "name=\"homeworkID\" onChange=\"reloadForm()\"",
			"param" => $handin_param,
			"firstLabel" => $Lang['SysMgr']['Homework']['PleaseSelectHomework'],
			"firstVal" => ""
	);
	$selectOpt_homeworkList = $lhomework->getSelectOptHomeworkList($args);
	
	$required = false;
	/* if ((!empty($handin_param["classID"]) && !empty($handin_param["subjectID"]) && !empty($handin_param["homeworkID"])) || !empty($handin_param["searchTxt"])) { */
	if (!empty($handin_param["homeworkID"]) || !empty($handin_param["searchTxt"])) {
		$required = true;
		$handin_studentsArr = $lhomework->getStudentsByParam($handin_param);
	} else {
		$handin_studentsArr = array();
	}
	
?>
<script language="javascript">
var eHomeworkAppJS = {
	vars: {
		showWithButton: false,
		showWithCheckbox: true
	},
	listeners: {
		buttonHandler: function(e) {
			var strREL = $(this).attr('rel');
			if (typeof strREL != "undefined") {
				if ($("input." + strREL + ":not(:checked)").length > 0) {
					$("input." + strREL + ":not(:checked)").trigger('click');
				} else {
					$("input." + strREL + "").trigger('click');
				}
				eHomeworkAppJS.func.highlight();
			}
			e.preventDefault();
		},
		buttonCheckboxHandler: function(e) {
			var strREL = $(this).attr('rel');
			if ($(this).attr('checked')) {
				$("input." + strREL + ":not(:checked)").trigger('click');
			} else {
				$("input." + strREL + ":checked").trigger('click');
			}
		},
		disableHandler: function(e) {
			e.preventDefault();
		},
		dateChangeHandler: function() {
			reloadForm();
		},
		checkboxClickHandler: function(e) {
			var currObj = $(this);
			var row_name = $(this).attr('data-row');
			$('input.' + row_name).not(this).attr('checked', false);
			eHomeworkAppJS.func.highlight();  
		},
		trHoverHandler: function(e) {
			switch (e.type) {
				case "mouseover":
					$(this).css({"background-color":"#efefef"});
					break;
				default:
					$(this).css({"background-color":"#fff"});
					eHomeworkAppJS.func.highlight();
					break;
			}
			e.preventDefault();
		},
		cancelViolationHandler: function(e) {
			if (confirm("<?php echo $Lang['SysMgr']['Homework']['CancelMisconductMsg']; ?>")) {
    			document.form1.cancelRec.value= $(this).attr('rel');
    			document.form1.isUpdate.value='0';
    			document.form1.s.value = "";
    			document.form1.pageNo.value = 1;
    			var url      = window.location.href;     // Returns full URL
    			url = url.replace("&updated=1", "");
    			document.form1.action = url; 
    			document.form1.submit();
			}
			e.preventDefault();
		}
	},
	func: {
		highlight: function() {
			$('#recTable tbody tr:not(:first-child)').each(function() {
				if ($(this).find('input:checked').eq(0).hasClass('col_1')) {
					$(this).css({"background-color":"#effcff"});
				} else if ($(this).find('input:checked').eq(0).hasClass('col_2')) {
					$(this).css({"background-color":"#ffe0e1"});
				} else if ($(this).find('input:checked').eq(0).hasClass('col_4') || $(this).find('input:checked').eq(0).hasClass('col_5')) {
					$(this).css({"background-color":"#e6ffde"});
				} else {
					$(this).css({"background-color":"#fff"});
				}
			});
		},
		init: function() {
			// $('#recTable tbody tr:first-child td').css({ "background-color": "#1B91AF" });
			$('#recTable tbody tr td').css({ "padding-top":"4px", "padding-bottom":"4px" });
			$('form[name="form2"]').bind('submit', eHomeworkAppJS.listeners.disableHandler);
			$('input.statusRd').bind('click', eHomeworkAppJS.listeners.checkboxClickHandler);
			$('#recTable tbody tr:not(:first-child)').bind('mouseover mouseleave', eHomeworkAppJS.listeners.trHoverHandler);
			$('#recTable tbody tr:not(:first-child) td').each(function() {
				$(this).css({"border-bottom": "1px solid #afafaf"});
			});
			if (eHomeworkAppJS.vars.showWithButton) {
				$('#recTable').find('button.checkhdr').bind('click', eHomeworkAppJS.listeners.buttonHandler);
				$('#recTable button.checkhdr').css({"display":"block"}).show();
			} else {
				$('#recTable button.checkhdr').css({"display":"block"}).hide();
			}
			if (eHomeworkAppJS.vars.showWithCheckbox) {
				$('#recTable').find('input.checkhdr').bind('click', eHomeworkAppJS.listeners.buttonCheckboxHandler);
				$('#recTable input[type="checkbox"].checkhdr').show();
			} else {
				$('#recTable input[type="checkbox"].checkhdr').hide();
			}
			
			$('button.cancelViolationBtn').bind('click', eHomeworkAppJS.listeners.cancelViolationHandler);
			eHomeworkAppJS.func.highlight();
		},
		submitRecord: function(formObj) {
			formObj.submit();
		}
	}	
};

$(document).ready( function() {
	$('input#text').keydown( function(evt) {
		if (Check_Pressed_Enter(evt)) {
			if ($("input[name='text']").size() > 0) {
				$("input[name='s']").eq(0).val($("input[name='text']").eq(0).val());
			}
			$("input[name='pageNo']").eq(0).val(1);
			// pressed enter
			goSearch();
		
		}
	});
	eHomeworkAppJS.func.init();
});
	function removeCat(obj,element,page){
		var alertConfirmRemove = "<?=$i_Discipline_System_alert_remove_record?>";
        if(countChecked(obj,element)==0)
                alert(globalAlertMsg2);
        else{
			if(confirm(alertConfirmRemove)){
				obj.action=page;
				obj.method="post";
				obj.submit();
			}
        }
	}

	function reloadForm() {
		document.form1.isUpdate.value='0';
		if ($("input[name='text']").size() > 0) {
			document.form1.s.value = document.form1.text;
			$("input[name='s']").eq(0).val($("input[name='text']").eq(0).val());
		} else {
			document.form1.s.value = '';
			$("input[name='s']").eq(0).val('');
		}
		document.form1.pageNo.value = 1;
		document.form1.submit();
	}


	function goSearch() {
		// document.form1.s.value = document.form2.text.value;
		// $("input[name='s']").eq(0).val($("input[name='text']").eq(0).val());
		// $("input[name='pageNo']").eq(0).val(1);
		// document.form1.pageNo.value = 1;
		if ($("input[name='text']").size() > 0) {
			$("input[name='s']").eq(0).val($("input[name='text']").eq(0).val());
		}
		document.form1.submit();
	}


	function viewHmeworkDetail(id)
	{
		   newWindow('./view.php?hid='+id,1);
	}


	function viewHandinList(id)
	{
		   // newWindow('./handinmanage_list.php?hid='+id,10);
		newWindowWithSize('./handinmanage_list.php?hid='+id, 0, 1, 960, 600);
	}

	function checkform(formOBJ) {
		if(confirm("<?php echo $i_SmartCard_Confirm_Update_Attend?>")){
			formOBJ.isUpdate.value='1';
			eHomeworkAppJS.func.submitRecord(formOBJ);
		}
	}
	
</script>
	<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
		<tbody>
		<!-- Homework Detail -->
		<tr>
			<td>
				<table width="98%" border="0" cellspacing="0" cellpadding="0">
					<tbody>
					<tr>
						<td height="28" align="right" valign="bottom">
							<form name="form2" method="post">
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tbody>
								<tr>
									<td>
										<table border="0" cellspacing="0" cellpadding="2">
											<tbody><tr>
												<td></td>
											</tr>
										</tbody></table>
									</td>
									<td align="right"> 
										<?= $htmlAry['searchBox']?>
									</td>
								</tr>
								</tbody>
							</table>
							</form>
							<form name="form1" method="post">
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tbody>
							<tr>
								<td height="28" align="right" valign="bottom">
									<table width="100%" border="0" cellspacing="0" cellpadding="4">
										<tr>
											<td align="right"><?php echo $linterface->GET_SYS_MSG($msg); ?>
											</td>
										</tr>
									</table>
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td height="30">
												<?=$Lang['SysMgr']['Homework']["From"]?>
												<?php echo $linterface->GET_DATE_PICKER("startDate", $startDate, "", "yy-mm-dd", "", "", "", "eHomeworkAppJS.listeners.dateChangeHandler()"); ?>
												<?=$Lang['SysMgr']['Homework']["To"]?>
												<?php echo $linterface->GET_DATE_PICKER("endDate", $endDate, "", "yy-mm-dd", "", "", "", "eHomeworkAppJS.listeners.dateChangeHandler()"); ?>
												<?php echo $filterbar . " " . $selectOpt_homeworkList; ?>
											</td>
											<td align="right" valign="middle" class="thumb_list"><?=$choiceType?></td>
										</tr>
									</table>
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td>
											</td>
											<td align="right" valign="bottom">
											<br/>
											<br/>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							</tbody>
							</table>
							<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>"/>
							<input type="hidden" name="order" value="<?php echo $li->order; ?>"/>
							<input type="hidden" name="field" value="<?php echo $li->field; ?>"/>
							<input type="hidden" name="numPerPage" value="<?=$li->page_size?>"/>
							<input type="hidden" name="page_size_change" value=""/>
							<input type="hidden" name="s" value="<?=$s?>"/>
							<!--input type="hidden" name="cid" value="<?=$childrenID?>"/-->
							<input type="hidden" name="yid" value="<?=$yearID?>"/>
							<input type="hidden" name="ytid" value="<?=$yearTermID?>"/>
							<input type="hidden" name="sid" value="<?=$subjectID?>"/>
							<input type="hidden" name="history" value="1"/>
							<input type="hidden" name="isUpdate" value="0"/>
							<input type="hidden" name="cancelRec" value=""/>
							<input type="hidden" name="currDate" value="<?php echo date("Y-m-d"); ?>"/>
<?php
						if ($required) {
						    if ($needToTransferToDiscipline) {
						        if ($mustBeTransferToDiscipline)
						        {
						            echo '<div style="text-align:right; padding-bottom:10px;"><span style="color:red;">*** ' . $Lang['SysMgr']['Homework']['RequestTransferMoreThanOneDayRecord'] . " ***</span></div>";
						        } else {
						            echo '<div style="text-align:right; padding-bottom:10px;"><span style="color:red;">***</span> ' . $Lang['SysMgr']['Homework']['RequestTransferRecord'] . "</div>";
						        }
						        
						    }
?>
							<table width="100%" border="0" cellspacing="0" cellpadding="4" id='recTable' align="center">
							<tbody>
							<tr class="tabletop" valign='bottom'>
								<td width="5%" class="tablebluetop">#</td>
								<td width="10%" class="tablebluetop"><?php echo $Lang['SysMgr']['Homework']['Class']; ?> / <?php echo $Lang['SysMgr']['Homework']['ClassNumber']; ?></td>
								<td width="10%" class="tablebluetop"><?php echo $Lang['SysMgr']['Homework']['StudentName']; ?></td>
								<td width="10%" class="tablebluetop"><?php echo $Lang['SysMgr']['Homework']['Subject']; ?></td>
								<td width="10%" class="tablebluetop"><?php echo $Lang['SysMgr']['Homework']['Topic']; ?></td>
								<td class="tablebluetop"><?php echo $Lang['SysMgr']['Homework']['Description']; ?></td>
								<td width="10%" class="tablebluetop"><?php echo $Lang['SysMgr']['Homework']['DueDate']; ?></td>
<?php
								$tdCount = 7;
								$showStatus = array( 1 => 1, 2 => -1, 4 => -12, 5 => -13 );
								foreach ($showStatus as $sskk => $ssvv) {
									if (isset($handInStatusArr[$ssvv])) {
										if ($handInStatusArr[$ssvv]["isDisabled"] != "1") {
											$tdCount++;
											echo '<td align="center" width="5%" class="tablebluetop">' . Get_Lang_Selection($handInStatusArr[$ssvv]["HandInStatusTitleB5"], $handInStatusArr[$ssvv]["HandInStatusTitleEN"]);
											if (!$mustBeTransferToDiscipline) {
    											if(!$lhomework->ViewOnly || $_SESSION['SSV_PRIVILEGE']['homework']['is_subject_leader'] || !$_SESSION['isTeaching']) {
    												// if (!$lhomework->isViewerGroupMember($_SESSION["UserID"])) {
    													echo '<br><button style="display:none" class="checkhdr" rel="col_' . $sskk . '" style="padding:4px padding-left:5px; padding-right:5px; margin-top:4px; font-size:0.85em;"><nobr>' . $Lang['Btn']['SelectAll'] . '</nobr></button>';
    													echo '<input type="checkbox" style="display:none" class="checkhdr statusRd row_0 col_' . $sskk . '" data-row="row_0" rel="col_' . $sskk . '" >';
    												// }
    											}
											}
											echo '</td>';
										}
									}
								}
								if ($sys_custom['eHomework_Status_Supplementary_WitHandInStatusManage'] && 
								    $sys_custom['eHomework_Status_Supplementary_WitTwoHandInStatusManage'] ||
								    $sys_custom['eHomework_allow_cancel_conduct_button']) {
									echo "<td width='5%' class='tablebluetop' align='center'>" . $Lang['SysMgr']['Homework']['LatestUpdate'] . "</td>";
									$tdCount++;

									if(!$lhomework->ViewOnly || $_SESSION['SSV_PRIVILEGE']['homework']['is_subject_leader'] || !$_SESSION['isTeaching']) {
										$tdCount++;
										echo '<td align="center" width="5%" class="tablebluetop">' . $Lang['Btn']['Cancel'] . Get_Lang_Selection($handInStatusArr[-20]["HandInStatusTitleB5"], " '" . $handInStatusArr[-20]["HandInStatusTitleEN"] . "'");
										echo '</td>';
									}
								}
?>
							</tr>
<?php

							if (count($handin_studentsArr) > 0) {
								foreach ($handin_studentsArr as $kk => $vv) {
									if ($sys_custom['eHomework_Status_Supplementary_WitTwoHandInStatusManage']) {
										if (empty($vv["RecordStatus"])) $vv["RecordStatus"] = "1";
									} else {
										if (empty($vv["RecordStatus"])) $vv["RecordStatus"] = "-1";
									}
									/*************************************************************************/
									$allowEdit = true;
									// if (in_array($vv["SuppRecordStatus"], array(7, -20))) {
									if (in_array($vv["SuppRecordStatus"], array(7)) && $vv["RecordStatus"] != 4) {
										$allowEdit = false;
										if ($sys_custom['eHomework_Status_Supplementary_WitTwoHandInStatusManage']) {
											if ($vv["SuppRecordStatus"] == "-20" && empty($vv["HWViolationID"]) ) {
												$allowEdit = true;
											}
										}
									}
									/*************************************************************************/
// 									debug_pr($vv['LeaveStudent']);
									$index = $kk + 1;
									echo "<tr>";
									echo "	<td class='tabletext'>" . $index;
									if(!$lhomework->ViewOnly || $_SESSION['SSV_PRIVILEGE']['homework']['is_subject_leader'] || !$_SESSION['isTeaching']) {
										if ($allowEdit) {
											echo "<input type='hidden' name='orghandinRec[" . $vv["RecordID"] . "]' value='"  . $vv["RecordStatus"] . "'>";
										}
									}
									echo "	</td>";
									echo "	<td class='tabletext tablerow'>" . $vv["ClassTitle"] . " (" . $vv["ClassNumber"] . ")</td>";
									if (empty($vv["StudentName"])) $vv["StudentName"] = $vv["enStudentName"];
									if (empty($vv["StudentName"])) $vv["StudentName"] = $vv["UserLogin"];
									if ($vv["StudentStatus"] == 3) $vv["StudentName"] .= ' <font style="color:red;">*</font>';
									echo "	<td class='tabletext tablerow'>" . $vv["StudentName"] . "</td>";
									echo "	<td class='tabletext tablerow'>" . $vv["Subject"] . "</td>";
									echo "	<td class='tabletext tablerow'><a href='javascript:viewHmeworkDetail(" . $vv["HomeworkID"] . ");' class='tablelink'>" . $vv["Title"] . "</a></td>";
									echo "	<td class='tabletext tablerow'>" . $vv["Description"] . "</td>";
									echo "	<td class='tabletext tablerow'>" . $vv["DueDate"] . "</td>";
									
									foreach ($showStatus as $sskk => $ssvv) {
										if (isset($handInStatusArr[$ssvv])) {
											if ($handInStatusArr[$ssvv]["isDisabled"] != "1") {
												if ($vv["RecordStatus"] == $ssvv) $strChecked = " checked";
												else $strChecked = "";
												
												if (!$allowEdit) {
													echo "  <td class='tabletext tablerow' align='center'>";
													if (trim($strChecked) == "checked") {
														if ($vv["SuppRecordStatus"] == "-20") {
															echo "<span style='color:#ff0000;'>";
														}
														echo Get_Lang_Selection($handInStatusArr[$vv["SuppRecordStatus"]]["HandInStatusTitleB5"], $handInStatusArr[$vv["SuppRecordStatus"]]["HandInStatusTitleEN"]);
														if ($vv["SuppRecordStatus"] == "-20") {
															echo "</span>";
														}
													} else {
														echo "-";
													}
													echo "</td>";
													
												} else {
													$tmp_name = "";
													echo "	<td class='tabletext tablerow' align='center'>";
													if(!$lhomework->ViewOnly || $_SESSION['SSV_PRIVILEGE']['homework']['is_subject_leader'] || !$_SESSION['isTeaching']) {
													    if ($vv["StudentStatus"] == 3 || $mustBeTransferToDiscipline) {
													        $tmp_name = "<input type='checkbox' disabled value='". $ssvv . "'" . $strChecked . " name='handinRec[" . $vv["RecordID"] . "]' class='statusRd row_" . $index . " col_" . $sskk . "' data-row='row_" . $index . "' data-col='col_" . $sskk . "'>";
													    } else {
                                                            $tmp_name = "<input type='checkbox' value='". $ssvv . "'" . $strChecked . " name='handinRec[" . $vv["RecordID"] . "]' class='statusRd row_" . $index . " col_" . $sskk . "' data-row='row_" . $index . "' data-col='col_" . $sskk . "'>";
													    }
													}
													if (empty($tmp_name)) {
														$tmp_name = "-";
														if (!empty($vv["SuppRecordStatus"])) {
															$tmp_name = Get_Lang_Selection($handInStatusArr[$vv["SuppRecordStatus"]]["HandInStatusTitleB5"], $handInStatusArr[$vv["SuppRecordStatus"]]["HandInStatusTitleEN"]);
														}
													}
													echo $tmp_name;
													echo "</td>";
												}
											}
										}
									}
									if ($sys_custom['eHomework_Status_Supplementary_WitHandInStatusManage'] &&
									    $sys_custom['eHomework_Status_Supplementary_WitTwoHandInStatusManage'] ||
									    $sys_custom['eHomework_allow_cancel_conduct_button']) {
										if ($vv["DateModified"] == $vv["DateInput"] && $vv["RecordStatus"] === "1") {
											$confirmDate = "-";
										} else {
										    $confirmDate = $vv["DateModified"];
										    
										    $latest_violation_date = $lhomework->getLatestConfirmationDate();
										    
										    if (strtotime($vv["DateModified"]) > strtotime($latest_violation_date)) {
										        $confirmDate = "<span style='font-style:italic;color:#de6024'>" . str_replace(" ", "<br>", $confirmDate) . "</span>";
										    } 
										}
										echo "<td class='tabletext tablerow' align='center'>" . $confirmDate . "</td>";
										
										if(!$lhomework->ViewOnly || $_SESSION['SSV_PRIVILEGE']['homework']['is_subject_leader'] || !$_SESSION['isTeaching']) {
											if ($vv["RecordStatus"] == "-1" &&
											    ($vv["CancelViolationDate"] == "0000-00-00 00:00:00" || empty($vv["CancelViolationDate"]))
											    && !empty($vv["HWViolationID"]) )
											{
										        if ($vv["StudentStatus"] == 3 || $mustBeTransferToDiscipline) {
												    echo "<td class='tabletext tablerow' align='center'>-</td>";
											    } else {
											        echo "<td class='tabletext tablerow' align='center'><button class='cancelViolationBtn' rel='" . $vv["RecordID"] . "|" . $vv["StudentID"] . "|" . $vv["HomeworkID"] . "'>" . $Lang['Btn']['Cancel'] . "</button></td>";
											    }
											} else {
												if ($vv["CancelViolationStatus"] == 1 && $vv["RecordStatus"] != "1") {
												    echo "<td class='tabletext tablerow' align='center'>" . str_replace(" ", "<br>", $vv["CancelViolationDate"]) . "</td>";
												} else {
													echo "<td class='tabletext tablerow' align='center'>-</td>";
												}	
											}
										}
									}
									echo "</tr>";
								}
							} else {
								echo "<tr>";
								echo "  <td colspan='" . $tdCount . "' height='80' align='center'>" . $Lang['SysMgr']['Homework']['NoRecord'] . "</td>";
								echo "</tr>";
							}
?>
							</tbody>
							</table>
							</form>
<?php
						}
?>
						</td>
					</tr>
					</tbody>
				</table><br>
				<?php
				if (count($handin_studentsArr) > 0) {
					echo $Lang['SysMgr']['FormClassMapping']['DeletedUserLegend'] . "<br>";
                }
                ?>
			</td>
		</tr>
		<!-- Button //-->
		<tr>
			<td>
				<?php
				$totalCount = 1;
				if($totalCount!=0 && $required) {
					if(!$lhomework->ViewOnly || $_SESSION['SSV_PRIVILEGE']['homework']['is_subject_leader'] || !$_SESSION['isTeaching']) {
				?>
					<table width="98%" border="0" cellspacing="0" cellpadding="2">
						<tbody>
						<tr>
							<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
						</tr>
						
						<tr>
							<td align="center">
								<?php
								if (!$mustBeTransferToDiscipline) {
								    echo $linterface->GET_ACTION_BTN($button_submit, "button", "checkform(document.form1)");
								}
								?>
							</td>
						</tr>
						</tbody>
					</table>
				<? } } ?>
			</td>
		</tr>
		</tbody>
	</table>
<?
	$linterface->LAYOUT_STOP();
	intranet_closedb();
?>