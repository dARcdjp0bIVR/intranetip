<?php
// Editing by 
/*
 * 2017-06-27 (Carlos): $sys_custom['LivingHomeopathy'] created.
 */
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/json.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$lhomework = new libhomework2007();
$JSON = new JSON_obj();
$json_ary = array('result'=>0,'content'=>'');

header("Content-Type: application/json;charset=utf-8");

if(!$sys_custom['LivingHomeopathy'] || $_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || (!($_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"] || $_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"]) && !$lhomework->isViewerGroupMember($UserID)))
{
	intranet_closedb();
	echo $JSON->encode($json_ary);
	exit;
}

$is_magic_quotes_active = (function_exists("get_magic_quotes_gpc") && get_magic_quotes_gpc()) || function_exists("magicQuotes_addslashes");

if($_POST['HomeworkID']=='' || $_POST['StudentID']==''){
	intranet_closedb();
	echo $JSON->encode($json_ary);
	exit;
}

$HomeworkID = IntegerSafe($_POST['HomeworkID']);
$StudentID = IntegerSafe($_POST['StudentID']);
if($is_magic_quotes_active){
	$_POST['TextScore'] = stripslashes($_POST['TextScore']);
}
$TextScore = rawurldecode($_POST['TextScore']);

$sql = "UPDATE INTRANET_HOMEWORK_HANDIN_LIST SET TextScore='".$lhomework->Get_Safe_Sql_Query($TextScore)."',TextScoreUpdateTime=NOW(),TextScoreUpdatedBy='".$_SESSION['UserID']."' WHERE HomeworkID='$HomeworkID' AND StudentID='$StudentID' ";
$success = $lhomework->db_db_query($sql);

if($success){
	$handin_records = $lhomework->getHomeworkHandinListRecords(array('HomeworkID'=>$HomeworkID,'StudentID'=>$StudentID,'GetTeacherName'=>1));
	$json_ary['result'] = 1;
	$json_ary['content'] = '('.str_replace('<!--DATETIME-->',$handin_records[0]['TextScoreUpdateTime'],str_replace('<!--NAME-->',$handin_records[0]['TextScoreUpdaterName'],$Lang['eHomework']['RatedRemark'])).')';
}
echo $JSON->encode($json_ary);

intranet_closedb();
?>