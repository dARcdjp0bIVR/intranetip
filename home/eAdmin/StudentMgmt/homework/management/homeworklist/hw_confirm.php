<?php
// Modifing by : yat

##### Change Log [Start] ######
#
#	Date:	2017-01-16 (Frankie)
#			For # CU-2016-2234 TWGHs Wong Fut Nam College
#
###### Change Log [End] #######

$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");

intranet_auth();
intranet_opendb();

$lhomework = new libhomework2007();

# check access right
if(
		$_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"]
		|| !$sys_custom['eHomework_Status_Supplementary_WitHandInStatusManage']
        || !($sys_custom['eHomework_Status_Supplementary_WitTwoHandInStatusManage'] || $sys_custom['eHomework_manual_transfer_to_eDis'])
		|| (!$_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"]
				&& !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"]
				&& !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_subject_leader"]
				&& !$lhomework->isViewerGroupMember($UserID))
		)
{
	if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])
	{
		header("location: ../../settings/index.php");
		exit;
	}

	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}



$linterface = new interface_html();

/******************************/
$handInStatusArr = $lhomework->getAllHandInStatus();
/******************************/

$lclass = new libclass();
# change page size
if ($page_size_change == 1)
{
	setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
	$ck_page_size = $numPerPage;
}

/**********************************/
function resetSubjectGroupID($subjectGroupID, $subjectGroups)
{
	for ($i=0; $i<sizeof($subjectGroups); $i++)
	{
		if ($subjectGroupID==$subjectGroups[$i][0])
		{
			return $subjectGroupID;
		}
	}
	return "";
}
/**********************************/
if (isset($ck_page_size) && $ck_page_size != "")
	$page_size = $ck_page_size;

	$pageSizeChangeEnabled = true;

	/*
	 # Table initialization
	 $order = ($order == "") ? 1 : $order;
	 $field = ($field == "") ? $sortField : $field;
	 $pageNo = ($pageNo == "") ? 1 : $pageNo;
	 */

	# Create a new dbtable instance
	$li = new libdbtable2007($field, $order, $pageNo);

	# Settings
	$s = addcslashes($s, '_');
	
	$allowExport = $lhomework->exportAllowed;

	$yearID = isset($_POST['yearID'])? $_POST['yearID'] : $_GET['yearID'];
	$yearTermID = isset($_POST['yearTermID'])? $_POST['yearTermID'] : $_GET['yearTermID'];
	$subjectID = isset($_POST['subjectID'])? $_POST['subjectID'] : $_GET['subjectID'];
	$subjectGroupID = isset($_POST['subjectGroupID'])? $_POST['subjectGroupID'] : $_GET['subjectGroupID'];
	$sid = isset($_POST['sid'])? $_POST['sid'] : $_GET['sid'];

	# Start layout
	$CurrentPageArr['eAdminHomework'] = 1;
	$CurrentPage = "Management_HomeworkList";
	$PAGE_TITLE = $Lang['SysMgr']['Homework']['HomeworkList'];
	$MODULE_OBJ = $lhomework->GET_MODULE_OBJ_ARR();

	// $query_str = "yearID=" . $yearID . "&yearTermID=" . $yearTermID . "&classID=" . $classID . "&subjectID=" . $subjectID . "&subjectGroupID=" . $sgid . "&sid=" . $sid;
	$query_str = "yearID=" . $yearID . "&yearTermID=" . $yearTermID . "&classID=" . $classID . "&subjectGroupID=" . $sgid . "&sid=" . $sid;
	$TAGS_OBJ = $lhomework->getHomeworkListTabs(basename(__FILE__), $query_str);

	// $htmlAry['searchBox'] = $linterface->Get_Search_Box_Div('text', stripslashes(stripslashes($s)));

	if($_SESSION['UserType']==USERTYPE_STUDENT)
	{
		include_once($PATH_WRT_ROOT."includes/libaccessright.php");
		$laccessright = new libaccessright();
		$laccessright->NO_ACCESS_RIGHT_REDIRECT();
		exit;
	}

	$query_str = "yearID=" . $yearID . "&yearTermID=" . $yearTermID . "&classID=" . $classID . "&subjectID=" . $subjectID . "&subjectGroupID=" . $sgid . "&sid=" . $sid;

	
	if ($_POST["isUpdate"] === "1") {
		
	    # CU-2016-2234 TWGHs Wong Fut Nam College
	    if ($sys_custom['eHomework_Status_Supplementary_WitTwoHandInStatusManage'])
	    {
	    
    		include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
    		
    		$academicYear = $lhomework->GetAllAcademicYear();
    		$yearID = ($yearID=="")? Get_Current_Academic_Year_ID():$yearID;
    		$academicYearTerm = $lhomework->getAllAcademicYearTerm($yearID);
    		
    		$handin_param = array(
    				"yearID" => $yearID,
    				"isCronjob" => TRUE,
    				"sqlType" => "forDataTransfer",
    				"callBy" => basename(__FILE__)
    		);
    		
    		if (!isset($intranet_session_language)) $intranet_session_language = "b5";
    		
    		$funcData = $lhomework->getStudentsByParamForDiscipline($handin_param);
    
    		/************************************************ Batch Update ***************************************/
    		$_violation = $funcData[0];
    		$handleArr = $funcData[1];
    		$discipData = $funcData[2];
    			
    		/**************************/
    		/* For Debug setting */
    		/**************************/
    		$is_debugMode = false;
    		$allowUpdateAndDiscip = true;
    			
    		if ($is_debugMode) {
    			header('Content-type: text/plain; charset=utf-8');
    		}
    		$debugID = 1;
    		/**************************/
    		/* For Debug setting */
    		/**************************/
    		if (count($handleArr) > 0) {
    			if (count($discipData) > 0) {
    				if (!$is_debugMode && $allowUpdateAndDiscip) {
    					/* Pass to eDiscipline */
    					$disciplinev12 = new libdisciplinev12();
    					$disciplinev12->INSERT_HW_MISCONDUCT_DETENTION($discipData);
    				}
    			}
    				
    			$_voliationHTML = "";
    			$_detentionHTML = "";
    			$_supplementHTML = "";
    			$_absHTML = "";
    				
    			foreach ($handleArr as $kk => $formObjs) {
    				if (count($formObjs) > 0) {
    					foreach ($formObjs as $hw_userID => $hw_userInfo) {
    							
    						if (empty($hw_userInfo["info"]["StudentName"])) $studentName = $hw_userInfo["info"]["UserLogin"];
    						else $studentName = $hw_userInfo["info"]["StudentName"];
    							
    						$studenInfo = $hw_userInfo["info"]["StudentID"] . "\t" . $hw_userInfo["info"]["ClassTitle"] . "\t" . $studentName . "";
    							
    						$strSQL = "INSERT INTO INTRANET_HOMEWORK_HANDIN_VIOLATION";
    						$strSQL .= " (HWViolationDate, StudentID, UserLogin, StudentName, ClassNumber, WEBSAMSCode, ClassTitle, ViolationRecord, DateInput)";
    						$strSQL .= " VALUES ";
    						$strSQL .= " ('" . $currDate. "', " . $hw_userID . ", '" . $hw_userInfo["info"]["UserLogin"] . "', '" . $studentName . "', '" . $hw_userInfo["info"]["ClassNumber"] . "', '" . $hw_userInfo["info"]["WEBSAMSCode"] . "', '" . $hw_userInfo["info"]["ClassTitle"] . "', '0', NOW());";
    						if (!$is_debugMode) {
    							if ($allowUpdateAndDiscip) {
    								$lhomework->db_db_query($strSQL);
    								$HWViolationID = $lhomework->db_insert_id();
    							} else {
    								echo $strSQL . "<br>";
    								$HWViolationID = $debugID;
    								$debugID++;
    							}
    						} else {
    							$HWViolationID = $debugID;
    							$debugID++;
    						}
    						if ($HWViolationID > 0) {
    							$pre_strSQL = "INSERT INTO INTRANET_HOMEWORK_HANDIN_VIOLATION_HWLIST ";
    							$pre_strSQL .= " (HWViolationID, StudentID, RecordID, HomeworkID, Title, Description, RecordStatus, DueDate, YearClassID, ClassGroupID, SubjectID, Subject, SuppRecordStatus, SuppRecordDate, RecordType) ";
    							$pre_strSQL .= " VALUES ";
    							if (count($hw_userInfo["_violation"]) > 0) {
    								$_voliationHTML .= $studenInfo . "";
    								$_voliationHTML .= "\n";
    								foreach ($hw_userInfo["_violation"] as $hw_id => $hw_info) {
    									$param = array(
    											$HWViolationID,
    											$hw_userID,
    											$hw_info["RecordID"],
    											$hw_info["HomeworkID"],
    											$hw_info["Title"],
    											$hw_info["Description"],
    											$hw_info["RecordStatus"],
    											$hw_info["DueDate"],
    											$hw_info["YearClassID"],
    											$hw_info["ClassGroupID"],
    											$hw_info["SubjectID"],
    											$hw_info["Subject"],
    											!empty($hw_info["SuppRecordStatus"]) ? $hw_info["SuppRecordStatus"]: -9999,
    											!empty($hw_info["SuppRecordDate"]) ? $hw_info["SuppRecordDate"] : 'NOW()',
    											"VIOLATION"
    									);
    									$strSQL = str_replace("'NOW()'", "NOW()", $pre_strSQL . " ('" . implode("', '", $param) . "');");
    									if (!$is_debugMode) {
    										if ($allowUpdateAndDiscip) {
    											$lhomework->db_db_query($strSQL);
    										} else {
    											echo $strSQL . "<br>";
    										}
    									}
    									$_voliationHTML .= "\t ----- \t" . $hw_info["RecordID"] . "\t" . $hw_info["HomeworkID"] . "\t" . $hw_info["Subject"] . "\t" . $hw_info["DueDate"] . "\t" . $hw_info["Title"] . "\t" . str_replace("\r", " ", str_replace("\n", " ", $hw_info["Description"])) . "\t欠交\t" . $vv["DueDate"] . "\n";
    								}
    								/****************************************************/
    								$strSQL = "UPDATE INTRANET_HOMEWORK_HANDIN_LIST SET HWViolationID='" . $HWViolationID . "', HWViolationDate=NOW() WHERE RecordID in (" . implode(", ", array_keys($hw_userInfo["_violation"])) . ");";
    								if (!$is_debugMode) {
    									if ($allowUpdateAndDiscip) {
    										$lhomework->db_db_query($strSQL);
    									} else {
    										echo $strSQL . "<br>";
    									}
    								}
    								/****************************************************/
    								$strSQL = "UPDATE INTRANET_HOMEWORK_HANDIN_VIOLATION SET ViolationRecord='1' WHERE HWViolationID='" . $HWViolationID . "';";
    								if (!$is_debugMode) {
    									if ($allowUpdateAndDiscip) {
    										$lhomework->db_db_query($strSQL);
    									} else {
    										echo $strSQL . "<br>";
    									}
    								}
    							}
    								
    							if (count($hw_userInfo["_detention"]) > 0) {
    								$_detentionHTML .= $studenInfo . "\n";
    								foreach ($hw_userInfo["_detention"] as $hw_id => $hw_info) {
    									$param = array(
    											$HWViolationID,
    											$hw_userID,
    											$hw_info["RecordID"],
    											$hw_info["HomeworkID"],
    											$hw_info["Title"],
    											$hw_info["Description"],
    											$hw_info["RecordStatus"],
    											$hw_info["DueDate"],
    											$hw_info["YearClassID"],
    											$hw_info["ClassGroupID"],
    											$hw_info["SubjectID"],
    											$hw_info["Subject"],
    											!empty($hw_info["SuppRecordStatus"]) ? $hw_info["SuppRecordStatus"]: -9999,
    											!empty($hw_info["SuppRecordDate"]) ? $hw_info["SuppRecordDate"] : 'NOW()',
    											"DETENTION"
    									);
    									$strSQL = str_replace("'NOW()'", "NOW()", $pre_strSQL . " ('" . implode("', '", $param) . "');");
    									if (!$is_debugMode) {
    										if ($allowUpdateAndDiscip) {
    											$lhomework->db_db_query($strSQL);
    										} else {
    											echo $strSQL . "<br>";
    										}
    									}
    									$_detentionHTML .= "\t ----- \t" . $hw_info["RecordID"] . "\t" . $hw_info["HomeworkID"] . "\t" . $hw_info["Subject"] . "\t" . $hw_info["DueDate"] . "\t" . $hw_info["Title"] . "\t" . str_replace("\r", " ", str_replace("\n", " ", $hw_info["Description"])) . "\t欠交\t" . $vv["DueDate"] . "\n";
    								}
    							}
    								
    							if (count($hw_userInfo["_supplement"]) > 0) {
    								$_supplementHTML .= $studenInfo . "\n";
    								foreach ($hw_userInfo["_supplement"] as $hw_id => $hw_info) {
    									$param = array(
    											$HWViolationID,
    											$hw_userID,
    											$hw_info["RecordID"],
    											$hw_info["HomeworkID"],
    											$hw_info["Title"],
    											$hw_info["Description"],
    											$hw_info["RecordStatus"],
    											$hw_info["DueDate"],
    											$hw_info["YearClassID"],
    											$hw_info["ClassGroupID"],
    											$hw_info["SubjectID"],
    											$hw_info["Subject"],
    											!empty($hw_info["SuppRecordStatus"]) ? $hw_info["SuppRecordStatus"]: -9999,
    											!empty($hw_info["SuppRecordDate"]) ? $hw_info["SuppRecordDate"] : 'NOW()',
    											"SUPPLEMENT"
    									);
    										
    									$_dis_supp[] = array(
    											"StudentID" => $hw_userID,
    											"HomeworkID" => $hw_info["HomeworkID"]
    									);
    										
    									$strSQL = str_replace("'NOW()'", "NOW()", $pre_strSQL . " ('" . implode("', '", $param) . "');");
    									if (!$is_debugMode) {
    										if ($allowUpdateAndDiscip) {
    											$lhomework->db_db_query($strSQL);
    										} else {
    											echo $strSQL . "<br>";
    										}
    									}
    									$_supplementHTML .= "\t ----- \t" . $hw_info["RecordID"] . "\t" . $hw_info["HomeworkID"] . "\t" . $hw_info["Subject"] . "\t" . $hw_info["DueDate"] . "\t" . $hw_info["Title"] . "\t" . str_replace("\r", " ", str_replace("\n", " ", $hw_info["Description"])) . "\t已補交\t" . $vv["DueDate"] . "\n";
    								}
    									
    								if (count($_dis_supp) > 0) {
    									if (!$is_debugMode && $allowUpdateAndDiscip) {
    										/* Pass to eDiscipline */
    										$disciplinev12 = new libdisciplinev12();
    										$disciplinev12->REMOVE_DETENTION_RECORD_AFTER_SUPPLEMENT($_dis_supp);
    									}
    								}
    									
    								/****************************************************/
    								$strSQL = "UPDATE INTRANET_HOMEWORK_HANDIN_LIST SET HWViolationID='" . $HWViolationID . "', HWViolationDate=NOW() WHERE RecordID in (" . implode(", ", array_keys($hw_userInfo["_supplement"])) . ");";
    								if (!$is_debugMode) {
    									if ($allowUpdateAndDiscip) {
    										$lhomework->db_db_query($strSQL);
    									} else {
    										echo $strSQL . "<br>";
    									}
    								}
    								/****************************************************/
    								$strSQL = "UPDATE INTRANET_HOMEWORK_HANDIN_VIOLATION SET ViolationRecord='1' WHERE HWViolationID='" . $HWViolationID . "';";
    								if (!$is_debugMode) {
    									if ($allowUpdateAndDiscip) {
    										$lhomework->db_db_query($strSQL);
    									} else {
    										echo $strSQL . "<br>";
    									}
    								}
    							}
    								
    							if (count($hw_userInfo["_ABS"]) > 0) {
    								$_absHTML .= $studenInfo . "\n";
    								foreach ($hw_userInfo["_ABS"] as $hw_id => $hw_info) {
    									$param = array(
    											$HWViolationID,
    											$hw_userID,
    											$hw_info["RecordID"],
    											$hw_info["HomeworkID"],
    											$hw_info["Title"],
    											$hw_info["Description"],
    											$hw_info["RecordStatus"],
    											$hw_info["DueDate"],
    											$hw_info["YearClassID"],
    											$hw_info["ClassGroupID"],
    											$hw_info["SubjectID"],
    											$hw_info["Subject"],
    											!empty($hw_info["SuppRecordStatus"]) ? $hw_info["SuppRecordStatus"]: -9999,
    											!empty($hw_info["SuppRecordDate"]) ? $hw_info["SuppRecordDate"] : 'NOW()',
    											"ABS"
    									);
    									$strSQL = str_replace("'NOW()'", "NOW()", $pre_strSQL . " ('" . implode("', '", $param) . "');");
    									if (!$is_debugMode) {
    										if ($allowUpdateAndDiscip) {
    											$lhomework->db_db_query($strSQL);
    										} else {
    											echo $strSQL . "<br>";
    										}
    									}
    									$_absHTML .= "\t ----- \t" . $hw_info["RecordID"] . "\t" . $hw_info["HomeworkID"] . "\t" . $hw_info["Subject"] . "\t" . $hw_info["Title"] . "\t" . str_replace("\r", " ", str_replace("\n", " ", $hw_info["Description"])) . "\t缺席\t" . $vv["DueDate"] . "\n";
    								}
    							}
    						}
    					}
    				}
    			}
    			if ($is_debugMode) {
    				if (!empty($_voliationHTML)) echo "\n" . $currDate . "\t'記欠交'記錄\n" . $_voliationHTML;
    				if (!empty($_detentionHTML)) echo "\n" . $currDate . "\t'未補交'功課\n" . $_detentionHTML;
    				if (!empty($_supplementHTML)) echo "\n" . $currDate . "\t'已補交'功課\n" . $_supplementHTML;
    				if (!empty($_absHTML))echo "\n" . $currDate . "\t'當天缺席'或'派功課日缺席'功課\n" . $_absHTML . "\n";
    			}
    		} else {
    			$strSQL = "INSERT INTO INTRANET_HOMEWORK_HANDIN_VIOLATION";
    			$strSQL .= " (HWViolationDate, StudentID, UserLogin, StudentName, ClassNumber, WEBSAMSCode, ClassTitle, ViolationRecord, DateInput)";
    			$strSQL .= " VALUES ";
    			$strSQL .= " ('" . $currDate. "', 0, '--', '--', '0', '--', '--', '0', NOW());";
    			if (!$is_debugMode) {
    				if ($allowUpdateAndDiscip) {
    					$lhomework->db_db_query($strSQL);
    					$HWViolationID = $lhomework->db_insert_id();
    				} else {
    					echo $strSQL . "<br>";
    				}
    			} else {
    				$HWViolationID = $debugID;
    				$debugID++;
    			}
    		}
    		/************************************************ Batch Update ***************************************/
	    } else {
	        $cronjob_path = dirname(dirname(dirname(dirname(dirname(dirname(dirname(__FILE__))))))) . "/schedule_task/homework/detention_record_handle.php";
	        $disable_cronjob_msg = true;
	        require_once($cronjob_path);
	        
	    }
		$msg = "update";
		$PATH_WRT_ROOT = "../../../../../../";
	}
	
	$linterface->LAYOUT_START();

	/* get Lastest Update */
	$LastestConfirmation = $lhomework->getLatestConfirmationDate();

?>
	<script language="javascript" type='text/javascript'>
		function formSubmit() {
			if(confirm(globalAlertMsg32)) {
				document.form1.submit();
			}
		}
	</script>
	<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
		<tbody>
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="4">
					<tr>
						<td align="right"><?php echo $linterface->GET_SYS_MSG($msg); ?>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<!-- Homework Detail -->
		<tr>
			<td>
				<table border="0" cellspacing="0" cellpadding="0" align="center" width="98%">
					<tr>
						<td><br><table border="0" cellspacing="0" cellpadding="4" id='recTable' class="form_table_v30">
						<tr>
							<td class='field_title'><?php echo $Lang['SysMgr']['Homework']['LatestUpdate']; ?> : </td>
							<td><?php echo $LastestConfirmation; ?></td>
						</tr>
						</table>
						<!-- Button //-->
						<table border="0" cellspacing="2" cellpadding="4" width="100%">
						<tr>
							<td colspan="2"><br>
								<form method='post' name="form1" id='form1'>
									<input type='hidden' name='isUpdate' value='1'>
								</form>
								<table border="0" cellspacing="0" cellpadding="2" width="100%">
									<tbody>
									<tr>
										<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
									</tr>
									<tr>
										<td align="center" style="padding-top:10px; text-align:center;">
											<?php echo $linterface->GET_ACTION_BTN($button_submit, "button", "formSubmit()"); ?>
										</td>
									</tr>
									</tbody>
								</table>
							</td>
						</tr>
						</table></td>
					</tr>
				</table>
			</td>
		</tr>
		</tbody>
	</table>
<?
	$linterface->LAYOUT_STOP();
	intranet_closedb();
?>