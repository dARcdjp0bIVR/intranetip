<?php
// Modifing by : 
		
#####################################################
#	Date:	2014-09-22 (YatWoon)
#			add flag checking $sys_custom['eHomework']['HideClearHomeworkRecords'] [Case#F68177]
#			Deploy: IPv10.1
#####################################################
		
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");

intranet_auth();
intranet_opendb();

if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] || $sys_custom['eHomework']['HideClearHomeworkRecords'] )
{
	if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])
	{
		header("location: ../../settings/index.php");	
		exit;
	}
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$linterface = new interface_html();
$lhomework = new libhomework2007();

$CurrentPageArr['eAdminHomework'] = 1;
$CurrentPage = "Management_HomeworkList";

$PAGE_TITLE = $Lang['SysMgr']['Homework']['HomeworkList'];

/* change to library function
# tag information
$TAGS_OBJ[] = array($Lang['SysMgr']['Homework']['ToDoList'], "index.php?yearID=$yearID&yearTermID=$yearTermID&subjectID=$subjectID&subjectGroupID=$sgid&sid=$sid", 0);
$TAGS_OBJ[] = array($Lang['SysMgr']['Homework']['History'], "history.php?yearID=$yearID&yearTermID=$yearTermID&subjectID=$subjectID&subjectGroupID=$sgid&sid=$sid", 0);
$TAGS_OBJ[] = array($Lang['SysMgr']['Homework']['ClearHomework'], "clear_homework.php", 1);
*/
$query_str = "yearID=" . $yearID . "&yearTermID=" . $yearTermID . "&subjectID=" . $subjectID . "&subjectGroupID=" . $sgid . "&sid=" . $sid;
$TAGS_OBJ = $lhomework->getHomeworkListTabs(basename(__FILE__), $query_str);


$MODULE_OBJ = $lhomework->GET_MODULE_OBJ_ARR();

# Start layout
$linterface->LAYOUT_START();
?>

<script language="javascript">
function checkform(obj)
{
	if(obj.delHomeworkRec[1].checked)
	{
		if(!check_text(obj.from, "<?=$i_alert_pleaseselect?> <?=$i_general_startdate?>.")) return false;
		if (!check_date(obj.from,"<?php echo $i_invalid_date; ?>")) return false;
        
		if(!check_text(obj.to, "<?=$i_alert_pleaseselect?> <?=$i_general_enddate?>.")) return false;
		if (!check_date(obj.to,"<?php echo $i_invalid_date; ?>")) return false;
	}
	
	if(!confirm('<?=$i_Homework_admin_clear_all_records_alert?>'))	return false;
}

function setTextEnable(val)
{
	
	var curEnable = document.getElementById('from').disabled;
	
	if(val=='period')
		document.getElementById('delHomeWorkTB').style.display='block';
	else
		document.getElementById('delHomeWorkTB').style.display='none';
	/*
	if(curEnable)
		newEnable = false;
	else
		newEnable = true;
	document.getElementById('from').disabled=newEnable;
	document.getElementById('to').disabled=newEnable;
	*/
}
</script>

<br />
<form name="form1" method="post" ACTION="clear_homework_action.php" onSubmit="return checkform(this)">

	<table width="96%" border="0" cellpadding="5" cellspacing="0">
		<tr>
			<td align="right" colspan="2"><?= $linterface->GET_SYS_MSG($msg, $xmsg) ?></td>
		</tr>		
		<tr>
			<td align="right" colspan="2"><?= $linterface->GET_WARNING_TABLE($Lang['eHomework']['ClearHomeworkWarningMsg']) ?></td>
		</tr>
		
		<tr>
			<td class="formfieldtitle" width="30%" valign="top"><?=$Lang['SysMgr']['Homework']['SelectRecordsCleared']?> <span class="tabletextrequire">*</span></td>
			<td align="left" valign="top">
				<input type="radio" name="delHomeworkRec" value="all" id="delHomeworkRec_all" onclick="setTextEnable(this.value)" checked> <label for="delHomeworkRec_all"><?=$i_Homework_AllRecords?></label>
				<input type="radio" name="delHomeworkRec" value="period" id="delHomeworkRec_period" onclick="setTextEnable(this.value)"> <label for="delHomeworkRec_period"><?=$eEnrollment['Period']?></label>
			</td>
		</tr>
		
		<tr id="delHomeWorkTB" style="display: none">
			<td>&nbsp;</td>
			<td>
				<table bordercolor='#F7F7F9' border="0" cellspacing=1 cellpadding=3>
				<tr>
					<td><?=$i_Homework_startdate." ".$i_From?></td>
					<td><?=$linterface->GET_DATE_PICKER("from",$from)?></td>
				</tr>
				<tr>
					<td><?=$i_To?></td>
					<td><?=$linterface->GET_DATE_PICKER("to",$to)?></td>
				</tr>
				</table>
			</td>
		</tr>

		<tr>
			<td align="left" class="tabletextremark">&nbsp; &nbsp; &nbsp; <?=$i_general_required_field?></td>
		</tr>
		
		<tr>
			<td colspan="2" class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
		</tr>
		<tr>
			<td align="center" colspan="2">
				<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "", "", "", "", "formbutton_alert")?>&nbsp;<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "self.location.href='index.php'","","","","formbutton_v30")?>
			</td>
		</tr>
	</table>
</form>
<?
	$linterface->LAYOUT_STOP();
	intranet_closedb();
?>
