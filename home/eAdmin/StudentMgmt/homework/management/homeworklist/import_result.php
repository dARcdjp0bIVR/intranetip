<?
# using : 

##### Change Log [Start] #####
#
#	Date:	2014-09-22 (YatWoon)
#			add flag checking $sys_custom['eHomework']['HideClearHomeworkRecords'] [Case#F68177]
#			Deploy: IPv10.1
#
#	Date	:	2011-02-15 (Henry Chow)
#				highlight Tag "Homework"
#
#	Date	:	2010-12-01 (Henry Chow)
#				Remove the column of "Subject Code" in Import file
#
###### Change Log [End] ######

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$lhomework = new libhomework2007();

if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || (!$_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"]  && !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] && !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_subject_leader"] && ($_SESSION['isTeaching'] && $lhomework->ViewOnly)))
{
	if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])
	{
		header("location: ../../settings/index.php");	
		exit;
	}
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}


# Temp Assign memory of this page
ini_set("memory_limit", "150M");

$linterface = new interface_html();
$lu = new libuser();

$CurrentPageArr['eAdminHomework'] = 1;

# menu highlight setting
$CurrentPage = "Management_HomeworkList";

$PAGE_TITLE = $Lang['SysMgr']['Homework']['HomeworkList'];

# tag information
$TAGS_OBJ[] = array($Lang['SysMgr']['Homework']['ToDoList'], "index.php?yearID=$yearID&yearTermID=$yearTermID&subjectID=$subjectID&subjectGroupID=$sgid&sid=$sid", 1);
$TAGS_OBJ[] = array($Lang['SysMgr']['Homework']['History'], "history.php?yearID=$yearID&yearTermID=$yearTermID&subjectID=$subjectID&subjectGroupID=$sgid&sid=$sid", 0);
if($_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] && !$sys_custom['eHomework']['HideClearHomeworkRecords'] )
	$TAGS_OBJ[] = array($Lang['SysMgr']['Homework']['ClearHomework'], "clear_homework.php", 0);


$MODULE_OBJ = $lhomework->GET_MODULE_OBJ_ARR();

# step information
$STEPS_OBJ[] = array($i_general_select_csv_file, 0);
$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 1);
$STEPS_OBJ[] = array($i_general_imported_result, 0);

# page netvigation
$PAGE_NAVIGATION[] = array($Lang['SysMgr']['Homework']['HomeworkList'], "index.php?yearID=$yearID&yearTermID=$yearTermID&subjectID=$subjectID&subjectGroupID=$subjectGroupID");
$PAGE_NAVIGATION[] = array($Lang['SysMgr']['Homework']['ImportRecord'], "");


$limport = new libimporttext();
$lo = new libfilesystem();
$name = $_FILES['csvfile']['name'];
$ext = strtoupper($lo->file_ext($name));

if(!($ext == ".CSV" || $ext == ".TXT"))
{
	intranet_closedb();
	header("location: import.php?xmsg=import_failed");
	exit();
}
/*
$data = $limport->GET_IMPORT_TXT($csvfile);
if(is_array($data))
{
	$col_name = array_shift($data);
}

$file_format = array("Subject Code", "Subject Group Code", "Topic", "Workload", "Teacher", "Start Date", "Due Date", "Description", "Hand-in Required");
if($lhomework->useHomeworkCollect)
{
	$file_format[] = "Collection Required";
}
if($lhomework->useHomeworkType)
{
	$file_format[] = "Homework Type";
}
*/
$file_format = array("Subject Group Code", "Topic", "Workload", "Teacher", "Start Date", "Due Date", "Description", "Hand-in Required");
$flagAry = array(1, 0, 1, 1, 1, 1, 1, 1, 1);
if($lhomework->useHomeworkCollect)
{
	$file_format[] = "Collection Required";
	$flagAry[] = 1;
}
if($lhomework->useHomeworkType)
{
	$file_format[] = "Homework Type";
	$flagAry[] = 1;
}

$data = $limport->GET_IMPORT_TXT_WITH_REFERENCE($csvfile, "", "", $file_format, $flagAry);
//debug_pr($data);
$col_name = array_shift($data);
$numOfData = count($data);
//debug_pr($data);
/*
debug_pr($file_format);
debug_pr($col_name);
exit;
*/
$format_wrong = false;

for($i=0; $i<sizeof($file_format); $i++)
{
	if ($col_name[$i]!=$file_format[$i])
	{
		$format_wrong = true;
		break;
	}
}
if($format_wrong)
{
	header("location: import.php?xmsg=wrong_header");
	exit();
}

$yearID = Get_Current_Academic_Year_ID();
//$yearTermID = GetCurrentSemesterID();


$tempData = array();
$x = $lhomework->checkImportError($data, $yearID, $yearTermID);

$successCount = $lhomework->importSuccessCount;
$errorCount = $lhomework->importErrorCount;

### Show import result summary ###
$result = "<table width='90%' border='0' cellpadding='3' cellspacing='0' align='center'>\n";
$result .= "<tr>\n";
$result .= "<td class='formfieldtitle' width='30%' align='left'>". $Lang['General']['SuccessfulRecord'] ."</td>\n";
$result .= "<td class='tabletext'>". $successCount ."</td>\n";
$result .= "</tr>\n";
$result .= "<tr>\n";
$result .= "<td class='formfieldtitle' width='30%' align='left'>". $Lang['General']['FailureRecord'] ."</td>\n";
$result .= "<td class='tabletext'>". ($errorCount ? "<font color='red'>":"") . $errorCount . ($errorCount ? "</font>":"") ."</td>\n";
$result .= "</tr>\n";
$result .= "</table><br>\n";

if($errorCount!=0)
	$result .= $x;

if($errorCount!=0){
	$import_button = $linterface->GET_ACTION_BTN($button_back, "button", "window.location='import.php?yearID=$yearID&yearTermID=$yearTermID&subjectID=$subjectID&subjectGroupID=$subjectGroupID'");
}

else{	
	for($i=0;$i<sizeof($data);$i++)
	{
		if(!is_array($data[$i]) || trim(implode("",$data[$i]))=="") 
			continue;  // skip empty line	                 
		
		$failed = false;
		$error_occured = 0;
		
		if($lhomework->useHomeworkCollect)
			//list($subject, $subjectGroup, $title, $loading, $pic, $start, $due, $description, $handin, $collect, $typeID) = $data[$i];
			list($subjectGroup, $title, $loading, $pic, $start, $due, $description, $handin, $collect, $typeID) = $data[$i];
		else 
			//list($subject, $subjectGroup, $title, $loading, $pic, $start, $due, $description, $handin, $typeID) = $data[$i];
			list($subjectGroup, $title, $loading, $pic, $start, $due, $description, $handin, $typeID) = $data[$i];
			
		$subject = intranet_htmlspecialchars($subject);
		$subjectGroup = intranet_htmlspecialchars($subjectGroup);
		$title = intranet_htmlspecialchars($title);
		$description = intranet_htmlspecialchars($description);
		$handin_required = intranet_htmlspecialchars($handin);
		$collect_required = intranet_htmlspecialchars($collect);
		//$typeID = $typeID;
		
		if(strtoupper($handin_required)=='YES')
			$handin_required=1;
		else
			$handin_required=0;
		
		if(strtoupper($collect_required)=='YES')
			$collect_required=1;
		else
			$collect_required=0;	
		/*
		$tempData[$i][0] = $subject;
		$tempData[$i][1] = $subjectGroup;
		$tempData[$i][2] = $start;
		$tempData[$i][3] = $due;
		$tempData[$i][4] = $loading;
		$tempData[$i][5] = $pic;
		$tempData[$i][6] = $title;
		$tempData[$i][7] = $description;
		$tempData[$i][8] = $handin_required;
		$tempData[$i][9] = $yearID;
		$tempData[$i][10] = $yearTermID;
		
		$tempData[$i][11] = $collect_required;
		$tempData[$i][12] = $typeID;
		*/

		$tempData[$i][0] = $subjectGroup;
		$tempData[$i][1] = $start;
		$tempData[$i][2] = $due;
		$tempData[$i][3] = $loading;
		$tempData[$i][4] = $pic;
		$tempData[$i][5] = $title;
		$tempData[$i][6] = $description;
		$tempData[$i][7] = $handin_required;
		$tempData[$i][8] = $yearID;
		$tempData[$i][9] = $yearTermID;
		
		$tempData[$i][10] = $collect_required;
		$tempData[$i][11] = $typeID;
		$_SESSION['tempData'] = $tempData;
	}
	$import_button = $linterface->GET_ACTION_BTN($button_import, "submit")." &nbsp;".$linterface->GET_ACTION_BTN($button_back, "button", "window.location='import.php?yearID=$yearID&yearTermID=$yearTermID&subjectID=$subjectID&subjectGroupID=$subjectGroupID'");	
}

$linterface->LAYOUT_START();
?>

<br />

<table width="100%" border="0" cellspacing="4" cellpadding="4">
	<tr>
		<td class="navigation">
			<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
			<tr><td><?=$linterface->GET_NAVIGATION4($PAGE_NAVIGATION)?></td></tr>
			</table>
		</td>
	</tr>
</table>

<form name="form1" method="post" action="import_update.php" enctype="multipart/form-data">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?=$linterface->GET_STEPS($STEPS_OBJ)?></td>
</tr>
<tr>
	<td align="center">
		<?=$result?>
	</td>
</tr>
<tr>
	<td>
		<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr>
			<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
		</tr>
		<tr>
			<td align="center" colspan="2">
			<?=$import_button?>
			</td>
		</tr>
		</table>
	</td>
</tr>
</table>
	<input type="hidden" name="yearID" value="<?=$yearID?>"/>
	<input type="hidden" name="yearTermID" value="<?=$yearTermID?>"/>
	<input type="hidden" name="subjectID" value="<?=$subjectID?>"/>
	<input type="hidden" name="subjectGroupID" value="<?=$subjectGroupID?>"/>
</form>
<br />

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>