<?php
// Using :

######## Change Log [Start] ################
#
#	Date:	2019-05-10 (Bill)   [2019-0507-0951-59254]
#           - prevent SQL Injection
#           - apply intranet_auth()
#
######## Change Log [End] ################

$PATH_WRT_ROOT = "../../../../../../../";

//set language
@session_start();
$parLang = $_SESSION['intranet_hardcode_lang'];

//switch (strtolower($parLang)) {
//	case 'en':
//		$intranet_hardcode_lang = 'en';
//		break;
//	default:
//		$intranet_hardcode_lang = 'b5';
//}

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."lang/lang.".$_SESSION['intranet_session_language'].".php");
include_once($PATH_WRT_ROOT."lang/eclassapp_lang.".$_SESSION['intranet_session_language'].".php");
include_once($PATH_WRT_ROOT.'includes/eClassApp/libeClassApp.php');

### Handle SQL Injection + XSS [START]
$uid = IntegerSafe($uid);
$typeID = IntegerSafe($typeID);
$subjectID = IntegerSafe($subjectID);
$subjectGroupID = IntegerSafe($subjectGroupID);
$yearID = IntegerSafe($yearID);
$yearTermID = IntegerSafe($yearTermID);

$loading = (float)$loading;
$handin_required = IntegerSafe($handin_required);
$collect_required = IntegerSafe($collect_required);
### Handle SQL Injection + XSS [END]

$UserID = $uid;
$_SESSION['UserID'] = $uid;

$libeClassApp = new libeClassApp();
$isTokenValid = $libeClassApp->isTokenValid($token, $uid, $ul);
if(!$isTokenValid) {
	echo $i_general_no_access_right	;
	exit;
}

intranet_auth();
intranet_opendb();

$libdb = new libdb();
$lhomework = new libhomework2007();

# Check date
# Start date >= curdate() && Start date <= due date
$start_stamp = date('Y-m-d',strtotime($startdate));
$due_stamp = date('Y-m-d',strtotime($duedate));
//$today = mktime(0,0,0,date("m"),date("d"),date("Y"));
$today = date('Y-m-d');
//$cond = ($lhomework->pastInputAllowed) ? compareDate($due_stamp,$start_stamp) < 0 : (compareDate($start_stamp,$today) < 0 || compareDate($due_stamp,$start_stamp)<0);

$success = 1;
if(!intranet_validateDate($start_stamp) || !intranet_validateDate($due_stamp))
{
    $success = 0;
}
else if ($lhomework->pastInputAllowed)
{
	if (compareDate($due_stamp, $start_stamp) < 0)
	{
		$success = 0;  
	}
}
else
{
	if (compareDate($start_stamp, $today) < 0 || compareDate($due_stamp, $start_stamp) < 0)
	{
        $success = 0;
	}	        
}
if ($success == 1)        
{
	/*
	$sql = "SELECT SubjectID from SUBJECT_TERM WHERE SubjectGroupID = '$subjectGroupID[0]'";
	$result = $lhomework->returnArray($sql);
	$subjectID = $result[0]['SubjectID'];
	*/
	
	$title = intranet_htmlspecialchars($title);
	$description = intranet_htmlspecialchars(trim($description));
	
	$nameField = getNameFieldByLang("");
	$sql = "SELECT $nameField FROM INTRANET_USER WHERE UserID = '$UserID'";
	$result = $lhomework->returnVector($sql);
	$postername = $result[0];
	
	$today_str = date("Y-m-d", $today);
	$handinRequired = $handin_required ==""? 0 : 1;
	$collectRequired = $collect_required  == ""? 0 : 1;
	$AcademicYearID = $yearID;
	$YearTermID = $yearTermID;
	
	$homeworkType = 1;
	foreach($subjectGroupID as $k=>$d)
	{
		$fieldnames = "ClassGroupID, SubjectID, PosterUserID, PosterName, StartDate, DueDate, Loading, Title, Description, RecordType, LastModified, TypeID, HandinRequired, CollectRequired, AcademicYearID, YearTermID, CreatedBy";            
		$fieldvalues = "'$d', '$subjectID', '$UserID', '$postername', '$start_stamp', '$due_stamp', '$loading', '$title', '$description', '$homeworkType', now(), '$typeID', '$handinRequired', '$collectRequired', '$AcademicYearID', '$YearTermID', '$UserID'";            
		
		$sql = "INSERT INTO INTRANET_HOMEWORK ($fieldnames) VALUES ($fieldvalues)";
		$success = $lhomework->db_db_query($sql);
		$HomeworkID = $lhomework->db_insert_id();
        
		# Upload Files
		$attachment = session_id()."_h";
		$homkworkdir = "$file_path/file/homework/";
		$path = "$file_path/file/homework/$attachment$HomeworkID/";
		$lf = new libfilesystem();
        
		$hasAttachment = false;
		$attachment_size = count($_FILES[multiplefile][name]);	
		for ($i=0; $i<$attachment_size; $i++)
		{
			$loc = $_FILES[multiplefile][tmp_name][$i];
			//Getting file name
			$file = $_FILES[multiplefile][name][$i];	
			
			//for ios upload
			$fileexplode = explode(".",$file);
			$filename = $fileexplode[0];
			$fileext = $fileexplode[1];
            if($filename == "image"){
            	$file = "image".($i+1).".".$fileext;
            }
			$des = "$path".stripslashes($file);	
			if ($loc == "none" || trim($loc) == "")
			{
			    // do nothing
			} 
			else
			{
				if (strpos($file,"."==0))
				{
				    // do nothing
				}
				else
				{
					if (!$hasAttachment)
					{
						 # add by Kelvin Ho 2008-11-06  make sure directory 'homework' exists
						 $lf->folder_new ($homkworkdir);
						 $lf->folder_new ($path);
						 $hasAttachment = true;
					}
					$lf->lfs_copy($loc, $des);
				}
			}
		}
        
		# Update attachment in DB
		if ($hasAttachment)
		{
			$sql = "UPDATE INTRANET_HOMEWORK SET AttachmentPath = '$attachment' WHERE HomeworkID = '$HomeworkID'";
			$lhomework->db_db_query($sql);
		}
		
		# End of upload files
	}
}

if ($success == 0)
{
	$msg = "add_failed";
}
else
{
	$msg = "add";
}

intranet_closedb();

header ("Location: new_homework.php?token=$token&uid=$uid&ul=$ul&parLang=$parLang&msg=$msg");
?>