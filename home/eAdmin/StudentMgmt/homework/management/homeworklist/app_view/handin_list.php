<?php
// Using :

######## Change Log [Start] ################
#
#   Date:   2020-10-15 (Bill)   [2020-0910-1206-37073]
#           - not auto create hand-in list records    ($sys_custom['eHomework_Not_Default_Insert_Handlin_List_Records'])
#
#	Date:	2019-05-10 (Bill)   [2019-0507-0951-59254]
#           - prevent SQL Injection
#           - apply intranet_auth()
#           - pass token to ajax page for checking
#
######## Change Log [End] ################

$PATH_WRT_ROOT = "../../../../../../../";
$PATH_WRT_ROOT_NEW = "../../../../../../..";

//switch (strtolower($parLang)) {
//	case 'en':
//		$intranet_hardcode_lang = 'en';
//		break;
//	default:
//		$intranet_hardcode_lang = 'b5';
//}

//set language
@session_start();
include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp_init.php");

$leClassApp_init = new libeClassApp_init();
$parLang = isset($_GET['parLang'])? $_GET['parLang'] : '';
$_SESSION['intranet_hardcode_lang'] = $leClassApp_init->getPageLang($parLang);

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.".$_SESSION['intranet_session_language'].".php");
include_once($PATH_WRT_ROOT."lang/eclassapp_lang.".$_SESSION['intranet_session_language'].".php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT.'includes/eClassApp/libeClassApp.php');

### Handle SQL Injection + XSS [START]
$uid = IntegerSafe($uid);
$hid = IntegerSafe($hid);
### Handle SQL Injection + XSS [END]

$UserID = $uid;
$_SESSION['UserID'] = $uid;

$libeClassApp = new libeClassApp();
$isTokenValid = $libeClassApp->isTokenValid($token, $uid, $ul);
if(!$isTokenValid) {
	echo $i_general_no_access_right	;
	exit;
}

# Generate token for auth checking
$dl_auth_token = '';
if($special_feature['download_attachment_add_auth_checking'])
{
    $dl_auth_token = returnSimpleTokenByDateTime();
}

if(!isset($date) || $date=="")
{
	$date = date('m/d/Y');
}
$date_start = date('Y-m-d',strtotime($date));

intranet_auth();
intranet_opendb();

$libdb = new libdb();
$lu = new libuser();
$linterface = new interface_html();
$lhomework = new libhomework2007();

$record = $lhomework->returnRecord($hid);
list($HomeworkID, $subjectGroupID, $subjectID, $start, $due, $loading, $title, $description, $lastModified, $homeworkType, $PosterName, $TypeID, $AttachmentPath, $handinRequired, $collectRequired) = $record;
$subjectName = $lhomework->getSubjectName($subjectID);
$subjectGroupName = $lhomework->getSubjectGroupName($subjectGroupID);
$description = nl2br($lhomework->convertAllLinks($description,40));
if($AttachmentPath != NULL) {
// 	$displayAttachment = $lhomework->displayAttachment_app($hid);
    $displayAttachment .= $lhomework->displayAttachment_IMG($hid, $dl_auth_token);
}

$display_menu = 'style="display:none"';
if($handinRequired == 1)
{
$display_menu = '';
$notSubmitStr = date("Y-m-d") <= $due ? $Lang['SysMgr']['Homework']['NotSubmitted'] : $Lang['SysMgr']['Homework']['ExpiredWithoutSubmission'];

$data = $lhomework->handinList($HomeworkID, $UserID);

$insertSql = "INSERT INTO INTRANET_HOMEWORK_HANDIN_LIST (HomeworkID,StudentID,RecordStatus,DateInput,DateModified) VALUES";
$values = "";

if (sizeof($data)!=0) $show_table .="<table class='form_table' width='100%'>";
for ($i=0; $i<sizeof($data); $i++){
	list($uid_this,$studentName,$className,$classNumber,$handinStatus,$handinID,$dateModified,$confirmUserID,$confirmTime,$hw_title) = $data[$i];
	$handinStatus = $handinStatus=="" ? ($sys_custom['eHoemwork_default_status'] ? $sys_custom['eHoemwork_default_status'] : -1) : $handinStatus;
// 	switch($handinStatus){
// 		case 1 : $str_status=$Lang['SysMgr']['Homework']['Submitted']; $color="97C856";  break;
// 		case -1: $str_status=$notSubmitStr;$color="#FA0007"; break;
// 		case 2 : $str_status=$Lang['SysMgr']['Homework']['LateSubmitted']; $color="#349ABD";  break;
// 		//case 3 : $str_status=$Lang['SysMgr']['Homework']['Redo']; $css="attendanceabsent"; break;
// 		case 4 : $str_status=$Lang['SysMgr']['Homework']['NoNeedSubmit']; $color="#727272";  break;
// 		case 6 : $str_status=$Lang['SysMgr']['Homework']['Supplementary']; $css="attendanceabsent";  break;
// 		default: $str_status=$Lang['SysMgr']['Homework']['Submitted'];$color="#97C856";
// 	}

	$photo = $lu->GET_OFFICIAL_PHOTO_BY_USER_ID($uid_this);
	$show_table .="<tr><td width='25px' valign='top' style='padding-top:20px'><div class='select_checkbox_div' style='display: none;'><input id='select_checkbox' type='checkbox' data-enhanced='true' name='select_checkbox[]' style='padding-left: 10px;' value='$uid_this'></div></td>"; 
	
	$show_table .="<td width='50px' valign='top'><img src='".$PATH_WRT_ROOT_NEW.$photo[1]."' style='width:40px;height:50px;'></td>";
	$show_table .="<td id='name_$uid_this' style='word-wrap:break-all'>$studentName<br>$className - $classNumber</td>";
	if($handinStatus==1) {
		$show_table .="<td id='statu_$uid_this' style='color:#97C856;text-align:right'>".$Lang['SysMgr']['Homework']['Submitted']."</td>";
	}
	if($handinStatus==-1) {
		$show_table .="<td id='statu_$uid_this' style='color:#FA0007;text-align:right'>".$notSubmitStr."</td>";
	}
	if($handinStatus==2) {
		$show_table .="<td id='statu_$uid_this' style='color:#349ABD;text-align:right'>".$Lang['SysMgr']['Homework']['LateSubmitted']."</td>";
	}
	if($handinStatus==4) {
		$show_table .="<td id='statu_$uid_this' style='color:#727272;text-align:right'>".$Lang['SysMgr']['Homework']['NoNeedSubmit']."</td>";
	}
	if($handinStatus==5) {
		$show_table .="<td id='statu_$uid_this' style='color:#727272;text-align:right'>".$Lang['SysMgr']['Homework']['UnderProcessing']."</td>";
	}
	if($sys_custom['eHomework_Status_Supplementary'] && date("Y-m-d") > $due) {
		if($handinStatus==6) {
			$show_table .="<td id='statu_$uid_this' style='color:#97C856;text-align:right'>".$Lang['SysMgr']['Homework']['Supplementary']."</td>";			
		}
	}
	$show_table .="<td colspan='2' style='text-align:right;display: none;' id='edit_area_$uid_this' >
			         <table style='width:100%;'>
			            <tr align='right'>	
						   <td style='width:50px;border:0px;padding-top: 0px;padding-bottom:0px;' nowrap='nowrap'>
			                  <a href='javascript:turn_status_single(1,$uid_this);' style='color:#97C856'>".$Lang['SysMgr']['Homework']['Submitted']."</a>
			               </td>			              
		                </tr>
		                <tr align='right'>	
			               <td style='width:50px;border:0px;padding-top: 0px;padding-bottom:0px;' nowrap='nowrap'>
			                  <a href='javascript:turn_status_single(-1,$uid_this);' style='color:#FA0007'>".$notSubmitStr."</a>
			               </td>
		                </tr>
		               <tr align='right'>	
			               <td style='width:50px;border:0px;padding-top: 0px;padding-bottom:0px;' nowrap='nowrap'>
			                  <a href='javascript:turn_status_single(2,$uid_this);' style='color:#349ABD'>".$Lang['SysMgr']['Homework']['LateSubmitted']."</a>
			               </td>
		                </tr>
			            <tr align='right'>	
			               <td style='width:50px;border:0px;padding-top: 0px;padding-bottom:0px;' nowrap='nowrap'>
							  <a href='javascript:turn_status_single(4,$uid_this);' style='color:#727272'>".$Lang['SysMgr']['Homework']['NoNeedSubmit']."</a>
			               </td>
		                </tr>
			            <tr align='right'>	
			               <td style='width:50px;border:0px;padding-top: 0px;padding-bottom:0px;' nowrap='nowrap'>
							  <a href='javascript:turn_status_single(5,$uid_this);' style='color:#727272'>".$Lang['SysMgr']['Homework']['UnderProcessing']."</a>
			               </td>
		                </tr>";
						   if($sys_custom['eHomework_Status_Supplementary'] && date("Y-m-d") > $due) {
						       $show_table .="<tr align='right'>	
			                                      <td style='width:50px;border:0px;padding-top: 0px;padding-bottom:0px;' nowrap='nowrap'>
						                              <a href='javascript:turn_status_single(6,$uid_this);' style='color:#97C856'>".$Lang['SysMgr']['Homework']['Supplementary']."</a>
						                      </td></tr>";  
						   }    
    
	$show_table .="</table></td><td width='10px' valign='top' style='padding-top:10px'><a href='javascript:show_edit($uid_this)'><img id='show_edit_icon_$uid_this' src='$image_path/$LAYOUT_SKIN/eClassApp/teacherApp/eHomework/action_right.png' align='absmiddle' style='width:40px; height:40px;'/></a></td>";
	$show_table .="</tr>";
	
	# set values of records in INTRANET_HOMEWORK_HANDIN_LIST
	$RecordStatus = $sys_custom['eHoemwork_default_status'] ? $sys_custom['eHoemwork_default_status'] : -1;
	if($handinID=="") {
		$y = "($hid,$uid_this,$RecordStatus,NOW(),NOW()),";
		$values .= $y;
	}
}
if (sizeof($data)!=0) $show_table .="</table>";

# Insert if records not exists
// [2020-0910-1206-37073] not auto create hand-in list records
if($sys_custom['eHomework_Not_Default_Insert_Handlin_List_Records']){
	// do nothing
}
else if($values!="") {
	$values = substr($values,0,strrpos($values,","));
	$insertSql .= $values;
	$lhomework->db_db_query($insertSql);
}

}

echo $libeClassApp->getAppWebPageInitStart();
?>

<script src="<?=$PATH_WRT_ROOT?>templates/jquery.mobile/jquery.ui.datepicker.js"></script>
<script id="mobile-datepicker" src="<?=$PATH_WRT_ROOT?>templates/jquery.mobile/jquery.mobile.datepicker.js"></script>
<script>
	jQuery(document).ready( function() {
		
	});
	
	function goback(){
		window.history.back()
    }
	
	function refresh(){
        document.form1.action="handin_list.php";
    	document.form1.submit();
	}
	
	function show_hide_description(){
        if($('#description').is(":hidden")){
            $('#description').show();
            $('#show_hide_img').attr("src","<?=$image_path ?>/<?=$LAYOUT_SKIN?>/eClassApp/teacherApp/eHomework/action_up.png");
        }
        else if($('#description').is(":visible")){
            $('#description').hide();
            $('#show_hide_img').attr("src","<?=$image_path ?>/<?=$LAYOUT_SKIN?>/eClassApp/teacherApp/eHomework/action_more.png");
        }
	}
	
	function turn_mutiple_select(){
		 $("#single_select").hide();
	     $("#mutiple_select").show();
	     $(".select_checkbox_div").show();
	}
	
	function turn_single_select(){
		 $("#single_select").show();
	     $("#mutiple_select").hide();
	     $(".select_checkbox_div").hide();
	}
	
	function show_edit(uid){
        if($('#edit_area_'+uid).is(":hidden")){
            $('#edit_area_'+uid).show();
            $('#name_'+uid).hide();
            $('#statu_'+uid).hide();
            $('#show_edit_icon_'+uid).attr("src","<?=$image_path ?>/<?=$LAYOUT_SKIN?>/eClassApp/teacherApp/eHomework/action_left.png");
        }
        else if($('#edit_area_'+uid).is(":visible")){
            $('#edit_area_'+uid).hide();
            $('#name_'+uid).show();
            $('#statu_'+uid).show();
            $('#show_edit_icon_'+uid).attr("src","<?=$image_path ?>/<?=$LAYOUT_SKIN?>/eClassApp/teacherApp/eHomework/action_right.png");
        }
	}
	
	function choose_all(){
		$("input[name='select_checkbox[]']").each(function(){ 
			this.checked = "checked";
		});
    }
	
    function turn_status_single(handin_status,uid){
    	var studentID = uid;
		// Update DB
		$.post(
			"ajax_change_status.php", 
			{ 
				hid:"<?=$hid?>",
				handin_status: handin_status,
		    	studentID: studentID,
		    	token: "<?=$token?>",
		    	uid: "<?=$uid?>",
		    	ul: "<?=$ul?>"
			},
			function(ReturnData)
			{
		       refresh();
			}
		);
    }
    
    function turn_status_multi(handin_status){
    	var studentID = '';
    	$("input[name='select_checkbox[]']").each(function(){ 
		 	var checked_status = this.checked;
		 	if(checked_status){
		 		 if(studentID=='') studentID = this.value;
				 else studentID = studentID+','+this.value;   
		  	}
        });
        
		// Update DB
		$.post(
			"ajax_change_status.php", 
			{ 
				hid:"<?=$hid?>",
				handin_status: handin_status,
		    	studentID: studentID,
		    	token: "<?=$token?>",
		    	uid: "<?=$uid?>",
		    	ul: "<?=$ul?>"
			},
			function(ReturnData)
			{
		       refresh();
			}
		);
    }
</script>

<link rel="stylesheet" href="<?=$PATH_WRT_ROOT?>templates/jquery.mobile/jquery.mobile.datepicker.css">
<style type="text/css">
    #body{
       background:#ffffff;
    }
    
    .form_table tr td{
      vertical-align:center;
/*       line-height:50px;  */
      padding-top:5px; 
      padding-bottom:5px;	
      border-bottom:1px solid #EFEFEF; 
      padding-left:10px; 
      padding-right:2px
    }
    
    a:link { text-decoration: none; color: black;}
    a:active {text-decoration: none; color: black !important;} 
    a:hover {text-decoration: none;color: black !important;}  
    a:visited {text-decoration: none;color: black !important;} 
</style>

</head>
	<body>
		<div data-role="page" id="body">			
            <div data-role="content" style="padding-top: 0px;padding-left: 0px;padding-right: 0px;">
			    <form id="form1" name="form1" method="get" >		   

					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr valign='top'>
							<td bgcolor='#FFFFFF' >
								<table border="0" cellpadding="0" cellspacing="0" style="width:100%;">
									<tr height="30px">
										<td style="vertical-align:middle; font-size:20px;padding-left: 20px;padding-top: 5px;">
											<?=$title?>
										</td>
										<td style="width:200px; text-align:right; vertical-align:bottom; font-size:16px;padding-right: 10px;padding-top: 10px;">
											<img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/icon_date.png" align="absmiddle" style="width:25px; height:25px;" />
											<span style="vertical-align:middle;"><?=$start?></span>
										</td>
									</tr>
									<tr height="30px">
										<td style="vertical-align:middle; font-size:15px;padding-left: 20px;"><?=$subjectName?> - <?=$subjectGroupName?></td>
										<td style="text-align:right; vertical-align:middle; font-size:16px;padding-right: 10px;">
											<img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/icon_deadline.png" align="absmiddle" style="width:25px; height:25px;" />
											<span style="vertical-align:bottom;"><font color="red"><?=$due?></font></span>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
					<div id="description" style="padding-left: 30px;display:none">
				    	<p><?=$description?></p>
				    	<p style="color: gray"><?=$displayAttachment ?></p>
				    	<p style="color: gray"><?=($loading/2)." ".$Lang['SysMgr']['Homework']['Hours'] ?></p>
				    	<p style="color: gray"><?=($handinRequired=="1"? $Lang['eClassApp']['eHomework']['NeedToHand-in']: $Lang['eClassApp']['eHomework']['NoNeedToHand-in'])?></p>
				        <? if($lhomework->useHomeworkCollect){?>  	
				    	<p style="color: gray">
				    	<?=($collectRequired=="1"? $Lang['eClassApp']['eHomework']['NeedToCollectHomework']: $Lang['eClassApp']['eHomework']['NoNeedToCollectHomework'])?>
				    	</p>
				    	<? } ?>
					</div>
					<a href="javascript:show_hide_description()"><img id="show_hide_img" src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/eClassApp/teacherApp/eHomework/action_more.png" align="right" style="width:40px; height:40px;padding-right: 5px;" /></a>
			        <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor='#E7E7E7' <?=$display_menu?>>
					 	<tr valign='top'>			
							<td width="200" align= "right" > 
						        <div id="single_select" align="right" style="font-size:1em;">     
						            <table style="">
						               <tr>
						                 <td>
						                    <a href="javascript:turn_mutiple_select();" ><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/smartcard/icon_change.png" style="width:1.4em; height:1.4em;" /></a>
						                 </td>
						                 <td style="padding-right: 10px;">
						                    <a href="javascript:turn_mutiple_select();" ><?=$Lang['AccountMgmt']['MultiSelect']?></a>
						                 </td>
						               </tr>
						             </table>
						        </div>
						        
								 <div id="mutiple_select" style="font-size:1em;display:none" >
						            <table style="width:100%">
						                <tr>
							                 <td style="padding-left: 10px;" nowrap="nowrap">
							                    <a href="javascript:choose_all();" style="color:black"><?=$Lang['Btn']['SelectAll']?></a>
							                 </td>
							                 <td colspan="3" align="right" style="padding-right: 10px;">
								                 <table>
									                 <tr>
										                 <td >
										                    <a href="javascript:turn_single_select()" ><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/smartcard/icon_change_select.png" style="width:1.4em; height:1.4em;" /></a>
										                 </td>
										                 <td >
										                    <a href="javascript:turn_single_select()" style="color: grey;"><?=$Lang['StudentAttendance']['TurnTo']?></a>
										                 </td>
									                 </tr>
								                 </table>
							                 </td>
						                </tr>  
						                <tr align="right">	
						                     <td></td>			               
							                 <td style="padding-right: 10px" nowrap="nowrap">
							                    <a href="javascript:turn_status_multi(1);" style="color:#97C856"><?=$Lang['SysMgr']['Homework']['Submitted']?></a>
							                 </td>
						                </tr>
						                <tr align="right">		
						                     <td></td>				               
							                 <td style="padding-right: 10px" nowrap="nowrap">
							                    <a href="javascript:turn_status_multi(-1);" style="color:#FA0007"><?=$notSubmitStr?></a>
							                 </td>
						                </tr>
                                        <tr align="right">	
                                             <td></td>		 
							                 <td style="padding-right: 10px" nowrap="nowrap">
							                    <a href="javascript:turn_status_multi(2);" style="color:#349ABD"><?=$Lang['SysMgr']['Homework']['LateSubmitted']?></a>
							                 </td>
						                </tr>
						                <tr align="right">	
                                             <td></td>		 
							                 <td style="padding-right: 10px" nowrap="nowrap">
							                    <a href="javascript:turn_status_multi(4);" style="color:#727272"><?=$Lang['SysMgr']['Homework']['NoNeedSubmit']?></a>
							                 </td>
						                </tr>
						                <tr align="right">	
                                             <td></td>		 
							                 <td style="padding-right: 10px" nowrap="nowrap">
							                    <a href="javascript:turn_status_multi(5);" style="color:#727272"><?=$Lang['SysMgr']['Homework']['UnderProcessing']?></a>
							                 </td>
						                </tr>						                						                
						                <?php if($sys_custom['eHomework_Status_Supplementary'] && date("Y-m-d") > $due) {?>
						                  <tr align="right">	
                                             <td></td>		 
							                 <td style="padding-right: 10px" nowrap="nowrap">
							                    <a href="javascript:turn_status_multi(5);" style="color:#727272"><?=$Lang['SysMgr']['Homework']['UnderProcessing']?></a>
							                 </td>
						                  </tr>
						                <?php }?>     					               
						            </table>			
						          </div>	
			                </td>
						</tr>
					</table>
					
					<?=$show_table ?>
					  <input type="hidden" value=<?=$token?> name="token">
                      <input type="hidden" value=<?=$uid?> name="uid">
                      <input type="hidden" value=<?=$ul?> name="ul">                
                      <input type="hidden" value=<?=$parLang?> name="parLang"> 
                      <input type="hidden" value=<?=$hid?> name="hid"> 
                      
				</form>
				 
            </div>
	       </div>
	</body>
</html>

<?php
intranet_closedb();
?>