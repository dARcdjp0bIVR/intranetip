<?php
// Using :

######## Change Log [Start] ################
#
#   Date:   2020-10-15 (Bill)   [2020-0910-1206-37073]
#           - create hand-in list records if not exist  ($sys_custom['eHomework_Not_Default_Insert_Handlin_List_Records'])
#
#	Date:	2019-05-10 (Bill)   [2019-0507-0951-59254]
#           - prevent SQL Injection
#           - apply intranet_auth(), token checking
#           - added data checking
#
######## Change Log [End] ################

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT.'includes/eClassApp/libeClassApp.php');

intranet_auth();
intranet_opendb();

$libeClassApp = new libeClassApp();
$lhomework = new libhomework2007();

### Handle SQL Injection + XSS [START]
$uid = IntegerSafe($_POST["uid"]);
$hid = IntegerSafe($_POST["hid"]);
$studentID = IntegerSafe($_POST["studentID"]);
$handin_status = IntegerSafe($_POST["handin_status"]);
### Handle SQL Injection + XSS [END]

$isTokenValid = $libeClassApp->isTokenValid($token, $uid, $ul);
if(!$isTokenValid) {
    echo $i_general_no_access_right	;
    exit;
}

// Check if valid data
if($hid == "" || $studentID == "") {
    intranet_closedb();
    die();
}

// [2020-0910-1206-37073] Create hand-in list records if not exist
if($sys_custom['eHomework_Not_Default_Insert_Handlin_List_Records'])
{
    $data = $lhomework->handinList($hid, $uid);

	$insertSql = "INSERT INTO INTRANET_HOMEWORK_HANDIN_LIST (HomeworkID,StudentID,RecordStatus,DateInput,DateModified) VALUES";
	$values = "";

	if (sizeof($data) != 0)
	{
		for ($i=0; $i<sizeof($data); $i++)
		{
			// list($uid_this,$studentName,$className,$classNumber,$handinStatus,$handinID,$dateModified,$confirmUserID,$confirmTime,$hw_title) = $data[$i];
            // $handinStatus = $handinStatus=="" ? ($sys_custom['eHoemwork_default_status'] ? $sys_custom['eHoemwork_default_status'] : -1) : $handinStatus;
            $uid_this = $data[$i]['UserID'];
            $handinID = $data[$i]['RecordID'];

            # set values of records in INTRANET_HOMEWORK_HANDIN_LIST
            $RecordStatus = $sys_custom['eHoemwork_default_status'] ? $sys_custom['eHoemwork_default_status'] : -1;
			if($handinID == "")
			{
				$y = "('$hid','$uid_this','$RecordStatus',NOW(),NOW()),";
				$values .= $y;
			}
		}

		if($values != "") {
			$values = substr($values,0,strrpos($values,","));
			$insertSql .= $values;
			$lhomework->db_db_query($insertSql);
		}
	}
}

$sql  = "UPDATE INTRANET_HOMEWORK_HANDIN_LIST SET RecordStatus=1 WHERE HomeworkID = '$hid' AND StudentID IN ($studentID)";
$sql2 = "UPDATE INTRANET_HOMEWORK_HANDIN_LIST SET RecordStatus=-1 WHERE HomeworkID = '$hid' AND StudentID IN ($studentID)";
$sql3 = "UPDATE INTRANET_HOMEWORK_HANDIN_LIST SET RecordStatus=2 WHERE HomeworkID = '$hid' AND StudentID IN ($studentID)";
//$sql4  = "UPDATE INTRANET_HOMEWORK_HANDIN_LIST SET RecordStatus=3 WHERE HomeworkID=$hid AND StudentID IN($studentID)";
$sql5 = "UPDATE INTRANET_HOMEWORK_HANDIN_LIST SET RecordStatus=4 WHERE HomeworkID = '$hid' AND StudentID IN ($studentID)";
$sql6 = "UPDATE INTRANET_HOMEWORK_HANDIN_LIST SET RecordStatus=5 WHERE HomeworkID = '$hid' AND StudentID IN ($studentID)";
if($sys_custom['eHomework_Status_Supplementary']) {
   $sql7 = "UPDATE INTRANET_HOMEWORK_HANDIN_LIST SET RecordStatus=6 WHERE HomeworkID = '$hid' AND StudentID IN ($studentID)";
}

if($handin_status==1) {
	$lhomework->db_db_query($sql);
}
if($handin_status==-1) {
	$lhomework->db_db_query($sql2);
}
if($handin_status==2) {
	$lhomework->db_db_query($sql3);
}
if($handin_status==4) {
	$lhomework->db_db_query($sql5);
}
if($handin_status==5) {
	$lhomework->db_db_query($sql6);
}
if($sys_custom['eHomework_Status_Supplementary'] && $handin_status==6) {
	$lhomework->db_db_query($sql7);
}

# Update confirm user
$sql_confirm = "UPDATE INTRANET_HOMEWORK SET HandinConfirmUserID = '$UserID', HandinConfirmTime = NOW() WHERE HomeworkID = '$hid'";
$lhomework->db_db_query($sql_confirm);

//echo $hid."/".$handin_status."/".$studentID."/".$sql;

intranet_closedb();
?>