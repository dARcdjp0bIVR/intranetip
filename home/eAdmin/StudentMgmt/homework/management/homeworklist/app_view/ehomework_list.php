<?php
// Using :

######## Change Log [Start] ################
#
#	Date:	2019-05-10 (Bill)   [2019-0507-0951-59254]
#           - prevent SQL Injection
#           - apply intranet_auth()
#
######## Change Log [End] ################

$PATH_WRT_ROOT = "../../../../../../../";
//switch (strtolower($_GET["parLang"])) {
//	case 'en':
//		$intranet_hardcode_lang = 'en';
//		break;
//	default:
//		$intranet_hardcode_lang = 'b5';
//}

// set language
@session_start();

include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp_init.php");
$leClassApp_init = new libeClassApp_init();

$parLang = isset($_GET['parLang'])? $_GET['parLang'] : '';
$_SESSION['intranet_hardcode_lang'] = $leClassApp_init->getPageLang($parLang);

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.".$_SESSION['intranet_session_language'].".php");
include_once($PATH_WRT_ROOT."lang/eclassapp_lang.".$_SESSION['intranet_session_language'].".php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT.'includes/eClassApp/libeClassApp.php');
include_once($PATH_WRT_ROOT.'includes/libuser.php');
include_once($PATH_WRT_ROOT."includes/user_right_target.php");

### Handle SQL Injection + XSS [START]
$uid = IntegerSafe($uid);
$classID = IntegerSafe($classID);
$subjectID = IntegerSafe($subjectID);
$subjectGroupID = IntegerSafe($subjectGroupID);
### Handle SQL Injection + XSS [END]

$UserID = $uid;
$_SESSION['UserID'] = $uid;
$_SESSION['CurrentSchoolYearID'] = '';

$libeClassApp = new libeClassApp();
$isTokenValid = $libeClassApp->isTokenValid($token, $uid, $ul);
if(!$isTokenValid) {
	echo $i_general_no_access_right	;
	exit;
}

if(!isset($date) || $date=="")
{
	$date=date('m/d/Y');
}
$date_start = date('Y-m-d',strtotime($date));

intranet_auth();
intranet_opendb();

$libdb = new libdb();

$linterface = new interface_html();
$lhomework = new libhomework2007();

$UserRightTarget = new user_right_target();
$lu = new libuser($uid);
session_register_intranet('UserType', $lu->RecordType);
session_register_intranet('isTeaching', $lu->teaching);
$_SESSION["SSV_USER_ACCESS"] = $UserRightTarget->Load_User_Right($uid);
if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])
{
	$_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] = true;
}

# Current Year Term
$yearID = Get_Current_Academic_Year_ID();
$currentYearTerm = getAcademicYearAndYearTermByDate($today);
$yearTermID = $currentYearTerm[0];
$yearTermID = $yearTermID? $yearTermID : 0;

# Filter - Teacher List
if($_SESSION['UserType']!=USERTYPE_STUDENT || ($_SESSION['UserType']==USERTYPE_STUDENT && $_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"])) {
	if(($_SESSION['UserType']==USERTYPE_STAFF && !$_SESSION['isTeaching']) || $_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] || $lhomework->isViewerGroupMember($UserID)) {		# eHomework Admin
		$classes = $lhomework->getAllClassInfo();
	}
	else {
		$classes = $lhomework->getAllClassesInvolvedByTeacherID(Get_Current_Academic_Year_ID(), GetCurrentSemesterID(), $UserID);
	}
}

# Filter - Subject
if($_SESSION['UserType']!=USERTYPE_STUDENT || ($_SESSION['UserType']==USERTYPE_STUDENT && $_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"]) || $lhomework->isViewerGroupMember($UserID))
{
	$subject = $lhomework->getTeachingSubjectList($UserID, $yearID, $yearTermID, $classID);
	$selectedSubject = $lhomework->getCurrentTeachingSubjects("name=\"subjectID\" onChange=\"document.form1.subjectGroupID.selectedIndex=0; reloadForm()\"", $subject, $subjectID, $Lang['SysMgr']['Homework']['AllSubjects'], true, -1);
}
else	# Subject Leader
{
	$subject = $lhomework->getStudyingSubjectList($UserID, 1, $yearID, $yearTermID);
}

if($_SESSION['UserType']!=USERTYPE_STUDENT || ($_SESSION['UserType']==USERTYPE_STUDENT && $_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"]) || $lhomework->isViewerGroupMember($UserID))
{
	# Filter - Subject Groups
	$subjectGroups = $lhomework->getTeachingSubjectGroupList($UserID, -1, $yearID, $yearTermID, $classID);
	
}
else
{
    # subject group of subject leader
	$subjectGroups = $lhomework->getStudyingSubjectGroupListByLeader($UserID, $yearID, $yearTermID);
}

# Subject groups of other semesters of selected year
$allSemesters = getSemesters($yearID);
$allSubjectGroupsOfYear = array();
if(sizeof($allSemesters)>0) {
	foreach($allSemesters as $termId=>$termName) {
		$temp = $lhomework->getTeachingSubjectGroupList($UserID, $subjectID, $yearID, $termId, $classID);
		if(sizeof($temp)>0) {
			for($i=0; $i<sizeof($temp); $i++) {
				if(!in_array($temp[$i][0], $allSubjectGroupsOfYear)) {
					$allSubjectGroupsOfYear[] = $temp[$i][0];
				}
			}
		}
	}
}

$sgid = $subjectGroupID;
if($sid!="" && $sid != $subjectID){
	$subjectGroupID = "";
}

# SQL statement
if(sizeof($subjectGroups)!=0)
{
	//echo "###";
	if($sgid!="") {
		$allGroups = " a.ClassGroupID = '$sgid' AND ";
	}
	else {
		if($_SESSION['UserType']==USERTYPE_STAFF) {
			$allGroups = " a.ClassGroupID IN (";
			for ($i=0; $i < sizeof($allSubjectGroupsOfYear); $i++)
			{
				$groupID = $allSubjectGroupsOfYear[$i];
				$allGroups .= $groupID.",";
			}
			$allGroups = substr($allGroups,0,strlen($allGroups)-1).")";
			$allGroups .=" AND";
		}
		else {	# subject leader
			$allGroups = " a.ClassGroupID IN (";
			for ($i=0; $i < sizeof($subjectGroups); $i++)
			{
				$groupID = $subjectGroups[$i][0];
				$allGroups .= $groupID.",";
			}
			$allGroups = substr($allGroups,0,strlen($allGroups)-1).")";
			$allGroups .=" AND";
		}
	}
}
else if(sizeof($classes)==0 && sizeof($subject)==0) {
	$allGroups = " a.ClassGroupID IN ('') AND ";
	$noRecord = 1;
}
else{		# class teacher only (not subject group teacher & class student not in any subject group)
	$allGroups ="";
	$subjectGroupID ="";
	$conds .= " a.ClassGroupID IN ('') AND ";
}

# set conditions
//$date_conds = "AND a.DueDate >= CURDATE()";
$conds .= ($subjectGroupID=='')? "$allGroups" : " a.ClassGroupID = '$subjectGroupID' AND";
$conds .= ($s=='')? "": "(IF('$intranet_session_language' = 'en', c.ClassTitleEN, c.ClassTitleB5) like '%$s%'
or a.Title like '%$s%'
$searchByTeacher
$searchBySubject
) AND";

$desc = "CONCAT(' <img src=\"$image_path/homework/$intranet_session_language/hw/icon_alt.gif\" title=\"',a.Description,'\" \>')";
$attach = "CONCAT(' <img src=\"$image_path/homework/$intranet_session_language/hw/icon_attachment.gif\" title=\"$i_AnnouncementAttachment\" \>')";
$handin = "CONCAT('<a href=\"javascript:viewHandinList(',a.HomeworkID,')\"><img src=\"$image_path/homework/$intranet_session_language/hw/btn_handin_list.gif\" border=\"0\" /></a>')";

$dbtables = "INTRANET_HOMEWORK as a LEFT OUTER JOIN ASSESSMENT_SUBJECT as b ON b.RecordID = a.SubjectID LEFT OUTER JOIN SUBJECT_TERM_CLASS as c ON c.SubjectGroupID = a.ClassGroupID ";
if($lhomework->useHomeworkType) {
	$dbtables .= " LEFT OUTER JOIN INTRANET_HOMEWORK_TYPE e ON (e.TypeID=a.TypeID) ";
}

$conds .= " (a.YearTermID = '$yearTermID' OR a.DueDate>=CURDATE()) AND";

if($_SESSION['UserType']==USERTYPE_STUDENT && $_SESSION["SSV_PRIVILEGE"]["homework"]["is_subject_leader"])
{
    $dbtables .= " LEFT OUTER JOIN SUBJECT_TERM_CLASS_USER AS d ON d.SubjectGroupID = a.ClassGroupID";
	$conds .= " d.UserID = $UserID AND a.AcademicYearID = '$yearID' $date_conds";
	$conds .= ($subjectID!="" && $subjectID!=-1) ? " AND a.SubjectID='$subjectID'" : "";
}
else
{
    # do not map by user, should allow for same subject group
	$conds .= " a.AcademicYearID = '$yearID' $date_conds";
}

if($classID!="") {
    $dbtables .= " LEFT OUTER JOIN SUBJECT_TERM_CLASS_USER F ON (F.SubjectGroupID=c.SubjectGroupID)
                    LEFT OUTER JOIN YEAR_CLASS_USER ycu ON (ycu.UserID=F.UserID AND ycu.YearClassID='$classID')";
    $conds .= " AND ycu.YearClassID='$classID'";
}

$dbtables .= " LEFT OUTER JOIN INTRANET_SUBJECT_LEADER sl ON ((sl.UserID=a.PosterUserID OR sl.UserID=a.CreatedBy) and sl.ClassID=a.ClassGroupID)";
$conds .=" AND a.StartDate='$date_start'";

$sql = "SELECT a.HomeworkID,a.Title,a.StartDate,a.DueDate,a.HandinRequired FROM $dbtables LEFT JOIN SUBJECT_TERM_CLASS_TEACHER stct ON (stct.SubjectGroupID=a.ClassGroupID AND stct.UserID='$UserID') WHERE $conds GROUP BY a.HomeworkID";
$result = $libdb->returnArray($sql);

if(sizeof($result)!=0)$show_list .="<ul id='show_list' data-role='listview' style='margin-top:0px;'>";
for($i=0;$i<sizeof($result);$i++){
	list($homeworkID, $title,$startDate,$dueDate,$handinRequired) = $result[$i];
	$handinCount = 0;
	$totalCount = 0;
	$data = $lhomework->handinList($homeworkID,$uid);
	$notSubmitStr = date("Y-m-d") <= $dueDate ? $Lang['SysMgr']['Homework']['NotSubmitted'] : $Lang['SysMgr']['Homework']['ExpiredWithoutSubmission'];
	
	for ($j=0; $j<sizeof($data); $j++)
	{
		$totalCount++;
	    list($luid,$studentName,$className,$classNumber,$handinStatus,$handinID,$dateModified,$confirmUserID,$confirmTime,$hw_title) = $data[$j];
	    $handinStatus = $handinStatus=="" ? ($sys_custom['eHoemwork_default_status'] ? $sys_custom['eHoemwork_default_status'] : -1) : $handinStatus;
	    switch($handinStatus){
	    	case 1 : $str_status=$Lang['SysMgr']['Homework']['Submitted'];  $handinCount++; break;
	    	case -1: $str_status=$notSubmitStr; break;
	    	case 2 : $str_status=$Lang['SysMgr']['Homework']['LateSubmitted']; $handinCount++; break;
	    	//case 3 : $str_status=$Lang['SysMgr']['Homework']['Redo']; $css="attendanceabsent"; break;
	    	case 4 : $str_status=$Lang['SysMgr']['Homework']['NoNeedSubmit'];  $totalCount--; break;
	    	case 6 : $str_status=$Lang['SysMgr']['Homework']['Supplementary'];  $handinCount++; break;
	    	default: $str_status=$Lang['SysMgr']['Homework']['Submitted'];
	    }
	}
	
    if($handinCount==$totalCount){
    	$count_css = "handinCount_green";
    }
    else{
    	$count_css = "handinCount_red";
    }
    
	$show_list .="<li data-icon='false' style='padding-left: 0px;'>";
	$show_list .="<div class='ui-checkbox' style='margin-top: 0px;margin-bottom: 0px;margin-left: 10px;'><input id='select_homework' type='checkbox' data-enhanced='true' name='select_homework[]' style='margin-top: 15px;margin-left: 15px;' value='$homeworkID'></div>";
	$show_list .="<a href='javascript:go_handin_list($homeworkID)' id='$homeworkID' onclick='' style='background: transparent !important;font-weight:normal!important;' class='ui-btn'><table style='padding-left:45px;' width='100%'><tr><td style='font-size: 18'>$title</td>";
	if($handinRequired=="1"){
		$show_list .="<td rowspan='2' align='right' width='90px' style='padding-right:20px;line-height:50px'><span id='$count_css'>$handinCount</span>/&nbsp;$totalCount</td></tr>";		
	}
	else{
		$show_list .="<td rowspan='2' align='right' width='90px' style='padding-right:30px;line-height:50px'>---</td>";		
	}
	$show_list .="<tr><td style='font-size: 10;color: gray;'>".$Lang['SysMgr']['Homework']['DueDate'].": ".$dueDate."</td></tr></table></a>";
    $show_list .="</li>";	 
}
if(sizeof($result)!=0)$show_list .="</ul>";
echo $libeClassApp->getAppWebPageInitStart();
?>

<script src="<?=$PATH_WRT_ROOT?>templates/jquery.mobile/jquery.ui.datepicker.js"></script>
<script id="mobile-datepicker" src="<?=$PATH_WRT_ROOT?>templates/jquery.mobile/jquery.mobile.datepicker.js"></script>
<script>
	jQuery(document).ready( function() {
		$('#show_list').on('click', 'li', function() {
			var counter = 0;
			$("input[name='select_homework[]']").each(function(){
                var checked_status = this.checked;
                if(checked_status){
                	counter++;
                }
            });
			if(counter==0){
				  $("#delete_button").hide();
				  $("#delete_button_hide").show();
			}
			else{
				  $("#delete_button").show();
				  $("#delete_button_hide").hide();
		    }
		});
	});
	
	function go_handin_list(homeworkID){
	    var div = document.getElementById("div1");
        var input = document.createElement("input");
        input.type = "hidden";
        input.name = "hid";
        input.value = homeworkID;
        div.appendChild(input);
        
		document.form1.action="handin_list.php";
    	document.form1.submit();
	}
	
	function goback(){
		window.history.back()
    }
	
	// handle the datepicker onchange function
	function onSelectedDateOfJqueryMobileDatePicker() {
		refresh();	
	}
	
	function refresh(){
        document.form1.action="ehomework_list.php";
    	document.form1.submit();
	}
	
	function delete_list(){
		var alertConfirmRemove = "<?=$i_Discipline_System_alert_remove_record?>";	
		if(confirm(alertConfirmRemove)){
			var homeworkID = '';
			$("input[name='select_homework[]']").each(function(){ 
		 		var checked_status = this.checked;
			 	if(checked_status){
			 		 if(homeworkID=='') homeworkID = this.value;
					 else  homeworkID = homeworkID+','+this.value;
			  	}
		    });
			// Update DB
			$.post(
				"ajax_list_delete.php", 
				{ 
					HomeworkID: homeworkID,
					token: "<?=$token?>",
			    	uid: "<?=$uid?>",
			    	ul: "<?=$ul?>"
				},
				function(ReturnData)
				{		
					if(ReturnData==1){
                        alert("<?=$Lang['eClassApp']['eHomework']['DeleteSuccess']?>");
				    }
				    else{
				        alert("<?=$Lang['eClassApp']['eHomework']['NoDeleteAccRecord']?>");
					}
					refresh();
				}
			);
		}
    }
</script>

<link rel="stylesheet" href="<?=$PATH_WRT_ROOT?>templates/jquery.mobile/jquery.mobile.datepicker.css">
<style type="text/css">
#body{
   background:#ffffff;
}

.form_table tr td{vertical-align:center; line-height:50px; padding-top:5px; padding-bottom:5px;	border-bottom:2px solid #EFEFEF; padding-left:2px; padding-right:2px}
   #handinCount_red{ 
     background-image: url('/images/2009a/eClassApp/teacherApp/studentPerformance/btn_negative_score.png');
     background-size: 100% 100%;
     height: 50px; 
     width: 50px;
     float:left; 
     line-height: 50px;
     color:red;    
     display:table-cell!important;
	 text-align:center !important;
	 vertical-align:middle!important; 
   }
   
   #handinCount_green{ 
     background-image: url('/images/2009a/eClassApp/teacherApp/studentPerformance/btn_positive_score.png');
     background-size: 100% 100%;
     height: 50px; 
     width: 50px;
     float:left; 
     line-height: 50px;
     color:green;     
     display:table-cell!important;
	 text-align:center !important;
	 vertical-align:middle!important; 
   }

.ui-btn{
	white-space:normal !important;
}
</style>
</head>
	<body>
		<div data-role="page" id="body">			   	     
            <div data-role="content" style="padding-top: 0px;padding-left: 0px;padding-right: 0px;">
			    <form id="form1" name="form1" method="get" >
	                 <table class="form_table_attendence" width="100%" border="0" cellpadding="0" cellspacing="0">
					     <tr>
					         <td class="field_title" style="padding-left: 30px">
					            <label class="label" for="date" ><?=$Lang['StudentAttendance']['RecordDate']?></label>
					         </td>
					         <td style="padding-right: 30px">
					            <input type="text" data-role="date" id="date" name="date" value="<?=$date?>" onchange="refresh();">
					         </td>
					     </tr>
					     <tr>
					        <td colspan="2" align= "right" style="background-color: #E7E7E7;">
					          <div id="delete_button" style="display:none" >
					            <a href="javascript:delete_list()"><img src="/images/2009a/iMail/app_view/icon_trash_60px.png" height="40" width="40" ></a>					          
					          </div>
					          <div id="delete_button_hide" style="padding-bottom: 20px;" >
					            	&nbsp;
					          </div>
					        </td>
					     </tr>
	                  </table>
	                  <div id="div1"></div>
	                  <input type="hidden" value=<?=$token?> name="token">
                      <input type="hidden" value=<?=$uid?> name="uid">
                      <input type="hidden" value=<?=$ul?> name="ul">
                      <input type="hidden" value=<?=$parLang?> name="parLang"> 
	                  <?=$show_list?>
				</form>
            </div>
	    </div>
	</body>
</html>

<?php
intranet_closedb();
?>