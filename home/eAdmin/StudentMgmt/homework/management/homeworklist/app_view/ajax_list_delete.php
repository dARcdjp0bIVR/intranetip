<?php
// Using :

######## Change Log [Start] ################
#
#	Date:	2019-05-10 (Bill)   [2019-0507-0951-59254]
#           - prevent SQL Injection
#           - apply intranet_auth(), token checking
#
######## Change Log [End] ################

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT.'includes/eClassApp/libeClassApp.php');

intranet_auth();
intranet_opendb();

### Handle SQL Injection + XSS [START]
$uid = IntegerSafe($uid);

$HomeworkID_str = $_POST["HomeworkID"];
$HomeworkID_str = IntegerSafe($HomeworkID_str);
### Handle SQL Injection + XSS [END]

$libeClassApp = new libeClassApp();
$isTokenValid = $libeClassApp->isTokenValid($token, $uid, $ul);
if(!$isTokenValid) {
    echo $i_general_no_access_right	;
    exit;
}

$libeClassApp = new libeClassApp();
$lhomework = new libhomework2007();

$lu = new libuser($UserID);

$CurrentPageArr['eAdminHomework'] = 1;
$all_delete = 1;

$HomeworkIDAry = array();
$HomeworkIDAry = explode(",",$HomeworkID_str);
foreach($HomeworkIDAry as $ListHomeworkID)
{
	//echo $ListHomeworkID;
	if ($lu->isTeacherStaff())
	{
		if ($lhomework->nonteachingAllowed || $lu->teaching == 1)
		{
			$haveAccess = true;
		}
	}
	if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])
	{
		$haveAccess = true;
	}
    
	if (!$haveAccess)
	{
		if ($lhomework->subjectLeaderAllowed && $lhomework->isSubjectLeader($UserID))
		{
			$haveAccess = true;
		}
	}

	if ($haveAccess)
	{
		if($lhomework->OnlyCanEditDeleteOwn && !$_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])
		{
			$conds = " AND PosterUserID = '$UserID'";
		}
        
		# prepare string
		$sql = "SELECT c.ClassTitleEN, h.PosterUserID, h.PosterName, h.StartDate, h.DueDate, h.Loading, h.Title, h.HandinRequired, t.TypeName FROM INTRANET_HOMEWORK h LEFT OUTER JOIN SUBJECT_TERM_CLASS c ON (c.SubjectGroupID=h.ClassGroupID) LEFT OUTER JOIN INTRANET_HOMEWORK_TYPE t on (t.TypeID=h.TypeID) WHERE h.HomeworkID = '$ListHomeworkID'";
		$record = $lhomework->returnArray($sql);
		list($subjectGroupName, $posterID, $posterName, $startdate, $duedate, $loading, $title, $handinRequired, $typeName) = $record[0];

		$str = "Start Date : $startdate<br>Due Date : $duedate<br>Homework Title : $title<br>Subject Group : $subjectGroupName";

		# insert into MODULE_RECORD_DELETE_LOG (Deletion Log)
		$sql = "INSERT INTO MODULE_RECORD_DELETE_LOG (Module, Section, RecordDetail, DelTableName, DelRecordID, LogDate, LogBy) VALUES ('".$lhomework->Module."', 'Homework', '$str', 'INTRANET_HOMEWORK', '$ListHomeworkID', NOW(), '$UserID')";
		$lhomework->db_db_query($sql);

		$sql = "DELETE FROM INTRANET_HOMEWORK WHERE HomeworkID = '$ListHomeworkID' $conds";
		$result1 = $lhomework->db_db_query($sql);
		$flag = $lhomework->db_affected_rows();
		//echo $sql;
		if(!$flag)
		$all_delete = 0;
        
		$sql = "DELETE FROM INTRANET_HOMEWORK_HANDIN_LIST WHERE HomeworkID = '$ListHomeworkID'";
		$result2 = $lhomework->db_db_query($sql);
	}
	else
	{
    	$all_delete = 0;
	}
}
echo $all_delete;

intranet_closedb();
?>