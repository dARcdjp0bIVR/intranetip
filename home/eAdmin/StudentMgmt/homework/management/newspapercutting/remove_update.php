<?php
# modifying by : 

########## Change Log [Start] ####################
#
########### Change Log [End] #####################

$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || (!$_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] && !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"] && !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_subject_leader"]))
{
	if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])
	{
		header("location: ../../settings/index.php");	
		exit;
	}
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

intranet_auth();
intranet_opendb();

$lhomework = new libhomework2007();
$lu = new libuser($UserID);

$CurrentPageArr['eAdminHomework'] = 1;

$HomeworkIDAry = array();

if(is_array($HomeworkID))
	$HomeworkIDAry = $HomeworkID;
else
	$HomeworkIDAry[] = $HomeworkID;

	
$all_delete = 1;
	
foreach($HomeworkIDAry as $ListHomeworkID)
{
	//echo $ListHomeworkID;
	if ($lu->isTeacherStaff())
	{
		if ($lhomework->nonteachingAllowed || $lu->teaching == 1)
		{
			$haveAccess = true;
		}
	}
	if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])	
		$haveAccess = true;
		

	if (!$haveAccess)
	{
		  if ($lhomework->subjectLeaderAllowed && $lhomework->isSubjectLeader($UserID))
		  {
			  $haveAccess = true;
		  }
	}

	if ($haveAccess)
	{
		/*
		$record= $lhomework->returnRecord($ListHomeworkID);

		list($HomeworkID, $subjectGroupID, $subjectID, $start, $due, $loading, $title, $description, $lastModified, $homeworkType, $PosterName, $typeID, $AttachmentPath,$handinRequired,$collectRequired, $posterUserID, $createdBy) = $record; 
		if($lhomework->ClassTeacherCanViewHomeworkOnly && ($UserID!=$posterUserID || $UserID !=$createdBy)) {
			$all_delete = 0;
			continue;
		}
		*/

		if($lhomework->OnlyCanEditDeleteOwn && !$_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])
			$conds = " AND PosterUserID = $UserID";
		
		# prepare string
		$sql = "SELECT c.ClassTitleEN, h.PosterUserID, h.PosterName, h.StartDate, h.DueDate, h.Title FROM INTRANET_HOMEWORK_WEEKLYDIARY_NEWSPAPERCUTTING h LEFT OUTER JOIN SUBJECT_TERM_CLASS c ON (c.SubjectGroupID=h.ClassGroupID) WHERE h.HomeworkID=$ListHomeworkID";
		$record = $lhomework->returnArray($sql); 
		list($subjectGroupName, $posterID, $posterName, $startdate, $duedate, $title) = $record[0];
		$str = "Start Date : $startdate<br>Due Date : $duedate<br>Homework Title : $title<br>Subject Group : $subjectGroupName<br>Type : Newspaper Cutting";
		
		# insert into MODULE_RECORD_DELETE_LOG (Deletion Log)
		$sql = "INSERT INTO MODULE_RECORD_DELETE_LOG (Module, Section, RecordDetail, DelTableName, DelRecordID, LogDate, LogBy) VALUES ('".$lhomework->Module."', 'Homework', '$str', 'INTRANET_HOMEWORK_WEEKLYDIARY_NEWSPAPERCUTTING', '$ListHomeworkID', NOW(), '$UserID')";
		$lhomework->db_db_query($sql);
		
		$sql = "DELETE FROM INTRANET_HOMEWORK_WEEKLYDIARY_NEWSPAPERCUTTING WHERE HomeworkID = $ListHomeworkID $conds";
        $result1 = $lhomework->db_db_query($sql);
		$flag = $lhomework->db_affected_rows();
        //echo $sql;
        if(!$flag)
        	$all_delete = 0;
	}
	else
	{
		$all_delete = 0;
	}
} 
//exit;
$url = ($history==1) ? "history.php" : "index.php";

$msg = ($all_delete) ? "delete" : "no_delete_acc_record";
header("Location: $url?subjectID=$subjectID&subjectGroupID=$subjectGroupID&msg=$msg");

intranet_closedb();
?>