<?php
# modifying by : 

########## Change Log
#
###########################

$PATH_WRT_ROOT = "../../../../../../";
$top_menu_mode = 0;

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || (!$_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"]  && !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] && !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_subject_leader"]))
{
	if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])
	{
		header("location: ../../settings/index.php");	
		exit;
	}
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}


$title = intranet_htmlspecialchars($title);
$description = intranet_htmlspecialchars(trim($description));

$lu = new libuser($UserID);
$lhomework = new libhomework2007();
$CurrentPageArr['eServiceHomework'] = 1;
if ($lu->isTeacherStaff() || $_SESSION["SSV_PRIVILEGE"]["homework"]["is_subject_leader"])
{
    if ($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"] || $lhomework->nonteachingAllowed || $lu->teaching == 1 || $lhomework->subjectLeaderAllowed)
    {
        $haveAccess = true;
    }
}

if ($haveAccess)
{
	# Check date
	# Start date >= curdate() && Start date <= due date
	$start_stamp = strtotime($startdate);
	$due_stamp = strtotime($duedate);
	$record = $lhomework->returnRecord($HomeworkID);
	$input_date = $record[8];
	$input_date_stamp = strtotime($input_date);
	
	$success = 1;
	if ($lhomework->pastInputAllowed)
	{
		if (compareDate($due_stamp,$start_stamp)<0)
		{
			$success = 0;  
		}
	}
	else
	{
		if (compareDate($start_stamp,$today) < 0 || compareDate($due_stamp,$start_stamp)<0)
		{
		$success = 0;
		}	        
	}
	if ($success ==1)
	{
		$today = mktime(0,0,0,date("m"),date("d"),date("Y"));
		$today_str = date("Y-m-d",$today);
		$fieldnames = "StartDate = '$startdate',";
		$fieldnames .= "DueDate ='$duedate',";
		$fieldnames .= "Title = '$title',";
		$fieldnames .= "Description = '$description'";
		$fieldnames .= ",PosterUserID = '$UserID'";
		$fieldnames .= ",LastModified = now()";
		$sql = "UPDATE INTRANET_HOMEWORK_WEEKLYDIARY_NEWSPAPERCUTTING SET $fieldnames WHERE HomeworkID = $HomeworkID";
	
		$success = $lhomework->db_db_query($sql);

	}
	
	if ($success == 0)
	{
		$msg = "delete_failed";
	}
	else
	{
		$msg = "update";
	}
	$return_url = 'index.php';
	$url = (($return_url == "")? "/":$return_url);
	header ("Location: index.php?subjectID=$subjectID&subjectGroupID=$subjectGroupID&msg=$msg");
}
else{
    header ("Location: index.php");
}
intranet_closedb();
?>