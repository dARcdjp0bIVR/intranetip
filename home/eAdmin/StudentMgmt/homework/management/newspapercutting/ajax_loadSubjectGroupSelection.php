<?php
// Modifing by : 

########## Change Log
#
#	Date:	2017-09-27 (Bill)	[2017-0907-1414-16066]
#			Hide * next to Input Field - Class
#			Deploy: IPv10.1
#
###########################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$lhomework = new libhomework2007();

# Non-Teaching Staff Mode or is admin
if(((($_SESSION['UserType']==USERTYPE_STAFF) && !$_SESSION['isTeaching']) || $_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"]) || ($_SESSION['UserType']==USERTYPE_STUDENT && $_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"]))
{
	# Class Filter
	$classes = $lhomework->getAllClassInfo();
	//if($classID=="" && ($subjectID=="" || $subjectID=="-1") && $subjectGroupID=="") $classID = $classes[0][0];
	$selectClass = getSelectByArray($classes, 'name="classID" id="classID" onChange="js_change_class(1);"', $classID, 0, 0, $i_general_all_classes);
	
	# All Subjects
	$subject = $lhomework->getTeachingSubjectList("", $yearID, $yearTermID, $classID);
	$subjectID = ($subjectID=="" || $subjectID=="-1")? $subject[0][0] : $subjectID;
	
	# Subject & Subject Group Filter
	$selectedSubject = $lhomework->getCurrentTeachingSubjects("name=\"subjectID\" id=\"subjectID\" onChange=\"js_change_class(2);\"", $subject, $subjectID, $Lang['SysMgr']['Homework']['PleaseSelect'], true);		
	if($subjectID!=""){
		$subjectGroups = $lhomework->getTeachingSubjectGroupList("", $subjectID, $yearID, $yearTermID, $classID);
		if($subjectGroupID=="" && sizeof($subjectGroups)==1) $subjectGroupID = $subjectGroups[0][0];
// 		$selectedSubjectGroup = getSelectByArray($subjectGroups, "name=subjectGroupID onChange=\"js_change_class();\"", $subjectGroupID, 1, 1);
		$selectedSubjectGroup = getSelectByArray($subjectGroups, "name=subjectGroupID[] id=subjectGroupID[] multiple size=10", $subjectGroupID, 1, 1);
	}
	
	if($sid!="" && $sid != $subjectID){
		$subjectGroupID="";
	}
	
	$filterbar3 = $selectClass;
	$filterbar = $selectedSubject;
	$filterbar2 = $selectedSubjectGroup;
}
# Student Mode with Subject Leader Allowed
else if($_SESSION['UserType']==USERTYPE_STUDENT && $_SESSION["SSV_PRIVILEGE"]["homework"]["is_subject_leader"])
{
	# Studying Subject
	$subject = $lhomework->getStudyingSubjectList($UserID, 1, $yearID, $yearTermID);
	$subjectID = ($subjectID=="" || $subjectID=="-1")? $subject[0][0]:$subjectID;
	
	# Subject & Subject Group Filter
	$selectedSubject = $lhomework->getCurrentTeachingSubjects("name=\"subjectID\" id=\"subjectID\" onChange=\"js_change_class(2);\"", $subject, $subjectID, $Lang['SysMgr']['Homework']['PleaseSelect'], true);
	if($subjectID!=""){
		$subjectGroups = $lhomework->getLeaderingSubjectGroupList($UserID, $subjectID, $yearID, $yearTermID);
		if($subjectGroupID=="" && sizeof($subjectGroups)==1) $subjectGroupID = $subjectGroups[0][0];
		$selectedSubjectGroup = getSelectByArray($subjectGroups, "name=subjectGroupID[] id=subjectGroupID[] multiple size=10", $subjectGroupID, 1,1);
	}
	
	if($sid!="" && $sid != $subjectID){
		$subjectGroupID="";
	}
	
	$filterbar = $selectedSubject;
	$filterbar2 = $selectedSubjectGroup;
}
# Teacher Mode
else if($_SESSION['UserType']==USERTYPE_STAFF && $_SESSION['isTeaching'])
{
	# Teaching Subject
	$subject = $lhomework->getTeachingSubjectList($UserID, $yearID, $yearTermID, $classID);
	$subjectID = ($subjectID=="" || $subjectID=="-1")? $subject[0][0] : $subjectID;
	
	// System Properties - Teacher/Subject Leader can only edit/delete self-created homework
	if($lhomework->ClassTeacherCanViewHomeworkOnly) {
		//debug_pr($_POST);
		//$subjectGroups = $lhomework->getSubjectGroupIDByTeacherID($UserID);
		/*
		for($a=0; $a<count($subjectGroups); $a++) {
			
			list($sgid, $sgname) = $subjectGroups[$a];
			$sql = "SELECT SubjectID, ".Get_Lang_Selection("SUB.CH_DES", "SUB.EN_DES")." AS SubjectName FROM ASSESSMENT_SUBJECT SUB INNER JOIN SUBJECT_TERM st ON (st.SubjectID=SUB.RecordID) WHERE st.SubjectGroupID='$sgid'";
			$ary = $lhomework->returnArray($sql);
			
			if(sizeof($ary)>0) {
				$subject[] = array($ary[0]['SubjectID'], $ary[0]['SubjectName']);	
			}	
			
			$sql = "SELECT yc.YearClassID, ".Get_Lang_Selection("yc.ClassTitleB5", "yc.ClassTitleEN")." as classname FROM YEAR_CLASS yc INNER JOIN YEAR_CLASS_USER ycu ON (ycu.YearClassID=yc.YearClassID AND yc.AcademicYearID='".Get_Current_Academic_Year_ID()."') INNER JOIN SUBJECT_TERM_CLASS_USER stcu ON (stcu.UserID=ycu.UserID) INNER JOIN SUBJECT_TERM st ON (st.SubjectGroupID=stcu.SubjectGroupID AND st.YearTermID='".GetCurrentSemesterID()."') INNER JOIN SUBJECT_TERM_CLASS_TEACHER stct ON (stct.SubjectGroupID=st.SubjectGroupID AND stct.UserID='$UserID') GROUP BY yc.YearClassID";
			$classes = $lhomework->returnArray($sql);
			
		}
		*/
		
		# Classes
		$classes = $lhomework->getClassesInvolvedBySubjectGroupTeacherID(Get_Current_Academic_Year_ID(), GetCurrentSemesterID(), $UserID);
		
		# Subject & Subject Group
		$subject = $lhomework->getTeachingSubjectList($UserID, $yearID, $yearTermID, $classID);
		$subjectGroups = $lhomework->getTeachingSubjectGroupList($UserID,$subjectID, $yearID, $yearTermID, $classID,1);
	}
	else {
		# Classes
		$classes = $lhomework->getAllClassesInvolvedByTeacherID(Get_Current_Academic_Year_ID(), GetCurrentSemesterID(), $UserID);
		
		# Subject & Subject Group
		$subject = $lhomework->getTeachingSubjectList($UserID, $yearID, $yearTermID, $classID);
		
		# check is the subjectid value pass from form page is in the subject list (change class)
		$in=0;
		foreach($subject as $k=>$d)
			$in = ($subject[$k][0] == $subjectID) ? 1 : $in;
		
		$subjectGroups = $lhomework->getTeachingSubjectGroupList($UserID,$subjectID, $yearID, $yearTermID, $classID,1);
	}
	
	# Class Filter
	//$selectClass = getSelectByArray($classes, 'name="classID" id="classID" onChange="js_change_class(1);"', $classID, 0, 0, "-- ".$i_alert_pleaseselect."--");
	$selectClass = getSelectByArray($classes, 'name="classID" id="classID" onChange="js_change_class(1);"', $classID, 0, 0, "-- ".$i_general_all_classes."--");
	
	# Subject Filter
	//$subjectID = (!$in)? $subject[0][0]:$subjectID;
	$selectedSubject = $lhomework->getCurrentTeachingSubjects("name=\"subjectID\" id=\"subjectID\" onChange=\"js_change_class(2);\"", $subject, $subjectID, "-- ".$Lang['SysMgr']['Homework']['AllSubjects']." --", false);
	
	# Subject Group Filter
	//echo $subjectID;
	if($subjectGroupID=="" && sizeof($subjectGroups)==1) $subjectGroupID = $subjectGroups[0][0];
	$selectedSubjectGroup = $lhomework->getCurrentTeachingGroups("name=subjectGroupID[] id=subjectGroupID[] multiple size=10", $subjectGroups, $subjectGroupID, "-- ".$i_alert_pleaseselect." ".$Lang['SysMgr']['Homework']['SubjectGroup']." --", false);
	
	$filterbar = $selectedSubject;
	$filterbar2 = $selectedSubjectGroup;
	$filterbar3 = $selectClass;
}

if($filterbar2=="") $filterbar2 = "<select name=\"subjectGroupID[]\" id=\"subjectGroupID[]\" multiple size=10></select>";

$SelectClassPrefix = !$skipMustSelectClass? '<span class="tabletextrequire">*</span>' : "";

$x = '
<table class="form_table_v30">
';
if($_SESSION['UserType']!=USERTYPE_STUDENT || ($_SESSION['UserType']==USERTYPE_STUDENT && $_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"])) {
$x .= '
<tr>
	<td class="field_title">'.$SelectClassPrefix.' '. $i_general_class .'</td>
	<td>'. $filterbar3 .'</td>
</tr>
';
}
$x .= '
<tr>
	<td class="field_title"><span class="tabletextrequire">*</span> '. $Lang['SysMgr']['Homework']['Subject'] .'</td>
	<td>'. $filterbar .'</td>
</tr>

<tr>
	<td class="field_title"><span class="tabletextrequire">*</span> '. $Lang['SysMgr']['Homework']['SubjectGroup'] .' </td>
	<td>'. $filterbar2 .'<span id="div_subjectGroupID_error"></span></td>
</tr>
</table>
';

intranet_closedb();
echo $x;
?>