<?php
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once ($PATH_WRT_ROOT."includes/libuser.php");
include_once ($PATH_WRT_ROOT."includes/libhomework.php");
include_once ($PATH_WRT_ROOT."includes/libhomework2007a.php");

intranet_auth();
intranet_opendb();

$lhomework = new libhomework2007();

if($lhomework->disabled || (!$_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"] && !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] && !$lhomework->isViewerGroupMember($UserID)))
{
	if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])
	{
		header("location: ./settings/index.php");	
		exit;
	}
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}
else if (!$_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"] && !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] && $lhomework->isViewerGroupMember($UserID)){
    header("Location: reports/weeklyhomeworklist/index.php");
}else{
	header("Location: management/homeworklist/index.php");
}

?>