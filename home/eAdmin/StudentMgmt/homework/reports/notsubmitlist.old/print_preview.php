<?php
	// Modifing by
	session_start();
	$targetList = $_SESSION["targetList"];
	$studentList = $_SESSION["studentList"];
	
	$PATH_WRT_ROOT = "../../../../../../";
	$CurrentPageArr['eAdminHomework'] = 1;

	include_once($PATH_WRT_ROOT."includes/global.php");
	include_once($PATH_WRT_ROOT."includes/libdb.php");
	include_once($PATH_WRT_ROOT."includes/libhomework.php");
	include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
	include_once($PATH_WRT_ROOT."includes/libinterface.php");
	include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
	include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");

	intranet_auth();
	intranet_opendb();

	if(!$plugin['eHomework']){
		include_once($PATH_WRT_ROOT."includes/libaccessright.php");
		$laccessright = new libaccessright();
		$laccessright->NO_ACCESS_RIGHT_REDIRECT();
		exit;
	}

	else{
		if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || !($_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"] || $_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"]))
		{
			if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])
			{
				header("location: ../../settings/index.php");	
				exit;
			}
			include_once($PATH_WRT_ROOT."includes/libaccessright.php");
			$laccessright = new libaccessright();
			$laccessright->NO_ACCESS_RIGHT_REDIRECT();
			exit;
		}
	}

	$linterface = new interface_html();
	$lhomework = new libhomework2007();

	$Name = $lhomework->getAcademicYearNameAndYearTermName($yearID, $yearTermID);
	list($yearName, $yearTermName) = $Name[0];
	
	$data = $lhomework->getNotSubmitListPrint($yearID, $yearTermID, $subjectID, $targetID, $targetList, $studentList);

	$x .= "<tr>";
	$x .= "<td valign='middle' class='eHomeworktdborder eHomeworkprinttabletitle'>#</td>";
	$x .= "<td valign='middle' class='eHomeworktdborder eHomeworkprinttabletitle'>".$Lang['SysMgr']['Homework']['Class']."</td>";
	$x .= "<td valign='middle' class='eHomeworktdborder eHomeworkprinttabletitle'>".$Lang['SysMgr']['Homework']['ClassNumber']."</td>";
	$x .= "<td valign='middle' class='eHomeworktdborder eHomeworkprinttabletitle'>".$Lang['SysMgr']['Homework']['Subject']."</td>";
	$x .= "<td valign='middle' class='eHomeworktdborder eHomeworkprinttabletitle'>".$Lang['SysMgr']['Homework']['SubjectGroup']."</td>";
	$x .= "<td valign='middle' class='eHomeworktdborder eHomeworkprinttabletitle'>".$Lang['SysMgr']['Homework']['StudentName']."</td>";
	$x .= "<td valign='middle' class='eHomeworktdborder eHomeworkprinttabletitle'>".$Lang['SysMgr']['Homework']['NotSubmitCount']."</td>";
	$x .= "</tr>";
	if(sizeof($data)!=0){
		for($i=0; $i<sizeof($data); $i++){
			list($className, $calssNumber, $subject, $subjectGroup, $studentName, $recordCount) = $data[$i];
			$x .= "<tr>";
			$x .= "<td class='eHomeworktdborder eHomeworkprinttext'>".($i+1)."</td>";
			$x .= "<td class='eHomeworktdborder eHomeworkprinttext'>".$className."</td>";
			$x .= "<td class='eHomeworktdborder eHomeworkprinttext'>".$calssNumber."</td>";
			$x .= "<td class='eHomeworktdborder eHomeworkprinttext'>".$subject."</td>";
			$x .= "<td class='eHomeworktdborder eHomeworkprinttext'>".$subjectGroup."</td>";
			$x .= "<td class='eHomeworktdborder eHomeworkprinttext'>".$studentName."</td>";
			$x .= "<td class='eHomeworktdborder eHomeworkprinttext'>".$recordCount."</td>";
			$x .= "</tr>";
		}
	}
	
	else{
		$x .="<tr><td colspan=\"7\" align=\"center\">".$Lang['SysMgr']['Homework']['NoRecord']."</td></tr>";
	}
?>

	<table width='100%' align='center' class='print_hide' border=0>
		<tr>
			<td align='right'><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></td>
		</tr>
	</table>

	<!-- Homework Title -->
	<table width='100%' align="center" border="0">
		<tr>
			<td align="center" ><b><?=$Lang['SysMgr']['Homework']['NotSubmitList']?></b></td>
		</tr>
	</table>        

	<br>
	<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tr><td align="left" valign="top" width="100"><?=$Lang['SysMgr']['Homework']['AcademicYear']?> : </td><td align="left" valign="top"><?=$yearName?></td></tr>
		<tr><td align="left" valign="top" width="100"><?=$Lang['SysMgr']['Homework']['YearTerm']?> : </td><td align="left" valign="top"><?=$yearTermName?></td></tr>
	</table>
	<br>

	<table class="eHomeworktableborder" width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
		<?=$x?>
	</table>
<?
	include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_footer.php");
	intranet_closedb();
?>