<?php
// Modifing by
		
$PATH_WRT_ROOT = "../../../../../../";
$CurrentPageArr['eAdminHomework'] = 1;

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");

intranet_auth();
intranet_opendb();

if(!$plugin['eHomework']){
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

else{
	if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"] || !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"]){
		if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])
		{
			header("location: ../../settings/index.php");	
			exit;
		}
		include_once($PATH_WRT_ROOT."includes/libaccessright.php");
		$laccessright = new libaccessright();
		$laccessright->NO_ACCESS_RIGHT_REDIRECT();
		exit;
	}
}

$linterface = new interface_html();
$lhomework = new libhomework2007();

	
$subjectName = $Lang['SysMgr']['Homework']['AllSubjects'];

if ($subjectID != "")
{
	$sql = "SELECT IF('$intranet_session_language' = 'en', EN_DES, CH_DES) FROM ASSESSMENT_SUBJECT WHERE RecordID = $subjectID";
	$sname = $lhomework->returnVector($sql,1);
	$subjectName = $sname[0];
}

$subjectGroupName = $Lang['SysMgr']['Homework']['AllSubjectGroups'];

if ($subjectGroupID != "")
{
	$sql = "SELECT IF('$intranet_session_language' = 'en', ClassTitleEN, ClassTitleB5) FROM SUBJECT_TERM_CLASS WHERE SubjectGroupID = $subjectGroupID";
	$sgname = $lhomework->returnVector($sql,1);
	$subjectGroupName = $sgname[0];
}

$x = $lhomework->printNotSubmitList($subjectID, $subjectGroupID, $yearID, $yearTermID);
?>


<table width='100%' align='center' class='print_hide' border=0>
	<tr>
		<td align='right'><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></td>
	</tr>
</table>

<!-- Homework Title -->
<table width='100%' align="center" border="0">
	<tr>
        	<td align="center" ><b><?=$Lang['SysMgr']['Homework']['NotSubmitList']?></b></td>
	</tr>
</table>        

<br>
<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr><td align="left" valign="top" width="100"><?=$Lang['SysMgr']['Homework']['Subject']?> : </td><td align="left" valign="top"><?=$subjectName?></td></tr>
	<tr><td align="left" valign="top" width="100"><?=$Lang['SysMgr']['Homework']['SubjectGroup']?> : </td><td align="left" valign="top"><?=$subjectGroupName?></td></tr>
</table>
<br>

<table class="eHomeworktableborder" width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<?=$x?>
</table>
<?
include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_footer.php");
intranet_closedb();
?>