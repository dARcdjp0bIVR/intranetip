<?php

// Modifing by
		
$PATH_WRT_ROOT = "../../../../../../";
$CurrentPageArr['eAdminHomework'] = 1;

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if(!$plugin['eHomework']){
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

else{
	if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"] || !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"]){
		if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])
		{
			header("location: ../../settings/index.php");	
			exit;
		}
		include_once($PATH_WRT_ROOT."includes/libaccessright.php");
		$laccessright = new libaccessright();
		$laccessright->NO_ACCESS_RIGHT_REDIRECT();
		exit;
	}
}

$lhomework = new libhomework2007();
$linterface = new interface_html("popup.html");

$MODULE_OBJ['title'] = $Lang['SysMgr']['Homework']['HomeworkList'];

$sql = "SELECT c.ClassName, 
		IF('$intranet_session_language'='en', e.EN_DES, e.CH_DES), 
		IF('$intranet_session_language'='en', a.ClassTitleEN, a.ClassTitleB5),
		IF('$intranet_session_language'='en', c.EnglishName, c.ChineseName)
		from SUBJECT_TERM_CLASS AS a 
		LEFT OUTER JOIN SUBJECT_TERM_CLASS_USER AS b ON b.SubjectGroupID = a.SubjectGroupID 
		LEFT OUTER JOIN INTRANET_USER AS c ON c.UserID = b.UserID 
		LEFT OUTER JOIN SUBJECT_TERM AS d ON d.SubjectGroupID = b.SubjectGroupID 
		LEFT OUTER JOIN ASSESSMENT_SUBJECT AS e ON e.RecordID = d.SubjectID 
		WHERE a.SubjectGroupID = $subjectGroupID AND b.UserID=$studentID";

$record = $lhomework->returnArray($sql,4);
list($className, $subject, $groupTitle, $name) = $record[0];

$x= $lhomework->viewNotSubmitDetail($subjectGroupID, $studentID, $yearID, $yearTermID);

$linterface->LAYOUT_START();
?>
<SCRIPT LANGUAGE=Javascript>
function click_print()
{
	with(document.form1)
    {
        submit();
	}
}

</SCRIPT>


<br />   
<form name="form1" action="detail_print_preview.php" method="post" target = "_blank">
	<table width="100%" border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td colspan="2">
				<table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
					<tr> 
						<td>
							<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
								<!-- class -->
								<tr>
									<td class="tabletext formfieldtitle" width="30%" valign="top"><?=$Lang['SysMgr']['Homework']['Class']?></td>
									<td width="567" align="left" valign="top">
									  <?=$className?>
									</td>
								</tr>
								
								<!-- subject-->
								<tr>
									<td class="tabletext formfieldtitle" width="30%" valign="top"><?=$Lang['SysMgr']['Homework']['Subject']?></td>
									<td width="567" align="left" valign="top">
										<?=$subject?>
									</td>
								</tr>
								
								<!-- subject Group-->
								<tr>
									<td class="tabletext formfieldtitle" width="30%" valign="top"><?=$Lang['SysMgr']['Homework']['SubjectGroup']?></td>
									<td width="567" align="left" valign="top">
										<?=$groupTitle?>
									</td>
								</tr>

								<!-- Name -->
								<tr>
									<td class="tabletext formfieldtitle" width="30%" valign="top"><?=$Lang['SysMgr']['Homework']['StudentName']?></td>
									<td width="567" align="left" valign="top">
									  <?=$name?>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td>
				<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
					<tr>
						<td class="tabletext formfieldtitle" width="30%" valign="top" colspan=2>
							<?=$x?>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">
					<tr>
						<td class="dotline" colspan="2"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
					</tr>
														
					<tr>
						<td align="center" colspan="2">
							<?= $linterface->GET_ACTION_BTN($button_print . ($intranet_session_language=="en"?" ":"") . $button_preview, "button", "click_print()") ?>
							<?= $linterface->GET_ACTION_BTN($button_close, "button", "self.close()","submit3") ?>
						</td>
					</tr>
				</table>										
							
			</td>
		</tr>
	</table>
	<input type="hidden" name="subjectGroupID" value="<?=$subjectGroupID?>">
	<input type="hidden" name="studentID" value="<?=$studentID?>">
	<input type="hidden" name="yearID" value="<?=$yearID?>">
	<input type="hidden" name="yearTermID" value="<?=$yearTermID?>">
</form>

<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>