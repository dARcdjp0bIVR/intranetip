<?php
$PATH_WRT_ROOT = "../../../../../../";
$CurrentPageArr['eAdminHomework'] = 1;

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if(!$plugin['eHomework']){
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

else{
	if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"] || !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"]){
		if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])
		{
			header("location: ../../settings/index.php");	
			exit;
		}
		include_once($PATH_WRT_ROOT."includes/libaccessright.php");
		$laccessright = new libaccessright();
		$laccessright->NO_ACCESS_RIGHT_REDIRECT();
		exit;
	}
}

$lhomework = new libhomework2007();
$lexport = new libexporttext();

$ExportArr = array();
	
$filename = "NotSubmitList.csv";

/*if ($subjectID != "")
{
	//$sql = "SELECT IF('$intranet_session_language' = 'en', EN_DES, CH_DES) FROM ASSESSMENT_SUBJECT WHERE RecordID = $subjectID";
	$sql = "SELECT EN_DES FROM ASSESSMENT_SUBJECT WHERE RecordID = $subjectID";
	$sname = $lhomework->returnVector($sql,1);
	$subjectName = $sname[0];
	$filename = "NotSubmitList_".$subjectName."_AllSubjectGroup.csv";
}

if ($subjectGroupID != "")
{
	$sql = "SELECT ClassTitleEN FROM SUBJECT_TERM_CLASS WHERE SubjectGroupID = $subjectGroupID";
	$sgname = $lhomework->returnVector($sql,1);
	$subjectGroupName = $sgname[0];
	$filename = "NotSubmitList_".$subjectName."_".$subjectGroupName.".csv";
}*/

# Select Data from DB
$row = $lhomework->exportNotSubmitList($subjectID, $subjectGroupID, $yearID, $yearTermID);

// Create data array for export
for($i=0; $i<sizeof($row); $i++){
	$m = 0;
	$ExportArr[$i][$m] = $row[$i][0];		//Class
	$m++;
	$ExportArr[$i][$m] = $row[$i][1];		//Class Number
	$m++;
	$ExportArr[$i][$m] = $row[$i][2];		//Subject Group
	$m++;
	$ExportArr[$i][$m] = $row[$i][3];		//Student Name
	$m++;
	$ExportArr[$i][$m] = $row[$i][4];		//Not Sumbit Count
	$m++;
}

if(sizeof($row)==0) {
	$ExportArr[0][0] = $i_no_record_exists_msg;	
}

$exportColumn = array($Lang['SysMgr']['Homework']['Class'], $Lang['SysMgr']['Homework']['ClassNumber'], $Lang['SysMgr']['Homework']['SubjectGroup'], $Lang['SysMgr']['Homework']['StudentName'], $Lang['SysMgr']['Homework']['NotSubmitCount']);	

$export_content = $lexport->GET_EXPORT_TXT($ExportArr, $exportColumn, "", "\r\n", "", 0, "00");

intranet_closedb();

// Output the file to user browser
$lexport->EXPORT_FILE($filename, $export_content);

?>
