<?php
# Modifing by : 
##### Change Log [Start] #####
#
#   Date    :   2020-09-22 (Cameron)
#               use $lhomework->getReportHomeworkListTabs() to retrieve TAGS_OBJ
#
#   Date    :   2019-09-06 (Tommy)
#               change access permission checking
#
###### Change Log [End] ######	
		
$PATH_WRT_ROOT = "../../../../../../";
$CurrentPageArr['eAdminHomework'] = 1;

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libcycleperiods.php");
include_once($PATH_WRT_ROOT."includes/libcal.php");
include_once($PATH_WRT_ROOT."includes/libcalevent.php");

intranet_auth();
intranet_opendb();

$lhomework = new libhomework2007();

// if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || (!$_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] && !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"] && !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_subject_leader"] && $lhomework->isViewerGroupMember($UserID)))
if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || (!$_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] && !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"] && !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_subject_leader"] && !$lhomework->isViewerGroupMember($UserID)))
{
	if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])
	{
		header("location: ../../settings/index.php");	
		exit;
	}
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}


# Temp Assign memory of this page
ini_set("memory_limit", "150M");

$linterface = new interface_html();


$CurrentPage = "Reports_WeekHomeworkList";
$MODULE_OBJ = $lhomework->GET_MODULE_OBJ_ARR();

$linterface = new interface_html();

$PAGE_TITLE = $Lang['SysMgr']['Homework']['WeeklyHomeworkList'];

# tag information
//$TAGS_OBJ[] = array($Lang['SysMgr']['Homework']['WeeklyHomeworkList'], "index.php", 0);
//$TAGS_OBJ[] = array($Lang['SysMgr']['Homework']['MonthlyHomeworkList'], "monthlyList.php", 1);
$TAGS_OBJ = $lhomework->getReportHomeworkListTabs('ByMonth');

$today = date('Y-m-d');
# Current Year
$yearID = Get_Current_Academic_Year_ID();
# Current Year Term
$currentYearTerm = getAcademicYearAndYearTermByDate($today);
$yearTermID = $currentYearTerm[0];
if($yearTermID=="")
	$yearTermID = 0;
	
# Set TimeStamp
/*
if ($ts == ""){
	$ts = time();
}

$ts = mktime(0,0,0,date('m',$ts),date('d',$ts)-date('w',$ts),date('Y',$ts));
*/
if ($ts == "")
{
    $month = date('m');
    $year = date('Y');
}
else
{
    $month = date('m',$ts);
    $year = date('Y',$ts);
}
$ts = mktime(0,0,0,$month,1,$year);
$month_display = "$month/$year";

$current = time();
$current = mktime(0,0,0,date('m',$current),date('d',$current)-date('w',$current),date('Y',$current));

$prev = mktime(0,0,0,$month-1,1,$year);
$next = mktime(0,0,0,$month+1,1,$year);

$subjectID = ($subjectID=="")? -1 :$subjectID;

if($subjectGroupID && $subjectID==-1)
{
	$TempSubjectInfo = $lhomework->RetrieveSubjectbySubjectGroupID($subjectGroupID);
	$subjectID = $TempSubjectInfo[0]['SubjectID'];
}


//$y = $lh->displayCalendarClass($ts);

# Non-Teaching Staff Mode
if($_SESSION['UserType']==USERTYPE_STAFF && !$_SESSION['isTeaching']){
	$classes = $lhomework->getAllClassInfo();
	$selectClass = getSelectByArray($classes, 'name="classID" id="classID" onChange="document.form1.subjectID.selectedIndex=-1;document.form1.subjectGroupID.selectedIndex=-1;reloadForm('.$ts.')"', $classID, 0, 0, $i_general_all_classes);
	
	$subject = $lhomework->getTeachingSubjectList("", $yearID, $yearTermID, $classID);
	$subjectID = ($subjectID=="")? $subject[0][0]:$subjectID;
	
	$display = (sizeof($subject)==0)? false:true;

	if($display){
		//$selectedSubject = $lhomework->getCurrentTeachingSubjects("name=\"subjectID\" onChange=\"reloadForm($ts)\"", $subject, $subjectID, "", false);		
		  $selectedSubject = $lhomework->getCurrentTeachingSubjects("name=\"subjectID\" onChange=\"reloadForm($ts)\"", $subject, $subjectID, $Lang['SysMgr']['Homework']['AllSubjects'], true, -1);
		
		
		//if($subjectID!=""){
			$subjectGroups = $lhomework->getTeachingSubjectGroupList("", $subjectID, $yearID, $yearTermID, $classID);
			$selectedSubjectGroup = getSelectByArray($subjectGroups, "name=subjectGroupID onChange=\"reloadForm($ts)\"", $subjectGroupID, "", 0, $Lang['SysMgr']['Homework']['AllSubjectGroups']);
			
			if($sid!="" && $sid != $subjectID && $subjectGroupID==""){
				$subjectGroupID="";
			}
			
			$cond = (($subjectID=="" || $subjectID==-1) && $subjectGroupID=='')? "a.AcademicYearID = $yearID AND a.YearTermID = $yearTermID" : (($subjectGroupID!="") ? " a.ClassGroupID = $subjectGroupID AND a.AcademicYearID = $yearID AND a.YearTermID = $yearTermID" : "a.SubjectID = $subjectID AND a.AcademicYearID = $yearID AND a.YearTermID = $yearTermID");
			
		//}
		$y = $lhomework->monthlyHomeworkList($ts, "", $cond,"","",$classID);
		$filterbar = "$selectClass $selectedSubject $selectedSubjectGroup";
	}
}

# Teacher Mode
else if(($_SESSION['UserType']==USERTYPE_STAFF && $_SESSION['isTeaching']) || $lhomework->isViewerGroupMember($UserID)){
	//$classes = $lhomework->getAllClassInfo();
	if($_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] || $lhomework->isViewerGroupMember($UserID))		# eHomework Admin
		$classes = $lhomework->getAllClassInfo();
	else														# Class teacher / subject teacher
		$classes = $lhomework->getAllClassesInvolvedByTeacherID(Get_Current_Academic_Year_ID(), GetCurrentSemesterID(),$UserID);
		
	$selectClass = getSelectByArray($classes, 'name="classID" id="classID" onChange="document.form1.subjectID.selectedIndex=-1;document.form1.subjectGroupID.selectedIndex=-1;reloadForm('.$ts.')"', $classID, 0, 0, $i_general_all_classes);

	$subject = $lhomework->getTeachingSubjectList($UserID, $yearID, $yearTermID, $classID);
	//$subjectID = ($subjectID=="")? $subject[0][0]:$subjectID;

	$display = (sizeof($subject)==0 && sizeof($classes)==0)? false:true;

	if($display){
		$selectedSubject = $lhomework->getCurrentTeachingSubjects("name=\"subjectID\" onChange=\"reloadForm($ts)\"", $subject, $subjectID, $Lang['SysMgr']['Homework']['AllSubjects'], true);
		//if($subjectID!=""){
			$subjectGroups = $lhomework->getTeachingSubjectGroupList($UserID, $subjectID, $yearID, $yearTermID,$classID);
			$selectedSubjectGroup = getSelectByArray($subjectGroups, "name=subjectGroupID onChange=\"reloadForm($ts)\"", $subjectGroupID, "", 0, $Lang['SysMgr']['Homework']['AllSubjectGroups']);
			
			if($sid != $subjectID && $subjectGroupID==""){
				$subjectGroupID="";
			}
			
			$cond = (($subjectID=="" || $subjectID==-1) && $subjectGroupID=='')? "a.AcademicYearID = $yearID AND a.YearTermID = $yearTermID" : (($subjectGroupID!="") ? " a.ClassGroupID = $subjectGroupID AND a.AcademicYearID = $yearID AND a.YearTermID = $yearTermID" : "a.SubjectID = $subjectID AND a.AcademicYearID = $yearID AND a.YearTermID = $yearTermID");
			
		//}
		
		$y = $lhomework->monthlyHomeworkList($ts, "", $cond,"","",$classID);
		$filterbar = "$selectClass $selectedSubject $selectedSubjectGroup";
	}
}

# Student Mode with Subject Leader Allowed
else if($_SESSION['UserType']==USERTYPE_STUDENT && $_SESSION["SSV_PRIVILEGE"]["homework"]["is_subject_leader"]){
	# Select Subjects
	$subject = $lhomework->getStudyingSubjectList($UserID, 1, $yearID, $yearTermID);
	$display = (sizeof($subject)==0)? false:true;

	if($display){
		$selectedSubject = $lhomework->getCurrentStudyingSubjects("name=\"subjectID\" onChange=\"reloadForm($ts)\"", $subject, $subjectID, $Lang['SysMgr']['Homework']['AllSubjects']);
		
		if(sizeof($subject)!=0){
			$allSubjects = " a.SubjectID IN (";
			for ($i=0; $i < sizeof($subject); $i++)
			{	
				list($ID)=$subject[$i];
				$allSubjects .= $ID.",";
			}
			$allSubjects = substr($allSubjects,0,strlen($allSubjects)-1).")";
			$allSubjects .= " AND";
		}
		else{
			$allSubjects ="";
		}
		
		$cond = ($subjectID=='' || $subjectID==-1)? "$allSubjects a.AcademicYearID = $yearID AND a.YearTermID = $yearTermID" : " a.SubjectID = $subjectID AND a.AcademicYearID = $yearID AND a.YearTermID = $yearTermID";
			
		$y = $lhomework->monthlyHomeworkList($ts, "", $cond, 1);
		
		$filterbar = "$selectedSubject";
	}
}

# Print link
$printPreviewLink = "monthly_print_preview.php?yearID=$yearID&yearTermID=$yearTermID&classID=$classID&subjectID=$subjectID&subjectGroupID=$subjectGroupID&ts=$ts&year=$year&month=$month";
# Export link
$exportLink = "monthly_export.php?yearID=$yearID&yearTermID=$yearTermID&classID=$classID&subjectID=$subjectID&subjectGroupID=$subjectGroupID&ts=$ts&year=$year&month=$month";

# Toolbar: print, export
$toolbar = $linterface->GET_LNK_PRINT("javascript:newWindow('$printPreviewLink', 23)", "", "", "", "", 0 );

$allowExport = $lhomework->exportAllowed;
if($allowExport)
	$toolbar .= $linterface->GET_LNK_EXPORT($exportLink, "", "", "", "", 0 );

# Start layout
$linterface->LAYOUT_START();
?>

<script language="javascript">
	function click_previous(prev){
		url = "monthlyList.php?ts=" + prev
		document.form1.action = url;
		document.form1.target = "_self";
		document.form1.submit();
	}
	
	function click_next(next){
		url = "monthlyList.php?ts=" + next
		document.form1.action = url;
		document.form1.target = "_self";
		document.form1.submit();
	}
	
	function click_current(current){
		url = "monthlyList.php?ts=" + current
		document.form1.action = url;
		document.form1.target = "_self";
		document.form1.submit();
	}
	
	function reloadForm(ts) {
		url = "monthlyList.php?ts=" + ts
		document.form1.action = url;
		document.form1.target = "_self";
		document.form1.submit();
	}
	
	function viewHmeworkDetail(id){
		newWindow('../../management/homeworklist/view.php?hid='+id,1);
	}
	
	function viewHandinList(id)
	{
		newWindow('../../management/homeworklist/handin_list.php?hid='+id,10);
	}

</script>
<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
			<form name="form2" method="post" onSubmit="return false">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td>
							<table border="0" cellspacing="0" cellpadding="2" width="300">
								<tr>
									<td>
										<?if($display){
											echo $toolbar;
										}?>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</form>
			<form name="form1" method="post">
				<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
					<tr>
						<td align="right">
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td height="28" align="right" valign="bottom">
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td height="30">
													<?=$filterbar?>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
				<table width="100%" border="0" cellpadding="2" cellspacing="0">
					<tr>
						<td align="right">
							<!--
							<table width="90%"><tr>
								<th align="center">
								<?
									if($month>=10)
										echo $eEnrollment['month'][$month]."/".$year;
									else 
										echo $eEnrollment['month'][substr($month,-1)]."/".$year;
								?>
								</th>
							</tr></table>
							-->
								<?
									if($month>=10)
										$curMonth = $eEnrollment['month'][$month]." ".$year;
									else 
										$curMonth = $eEnrollment['month'][substr($month,-1)]." ".$year;
								
								if($display){		
								?>
									<a href="javascript:click_previous(<?=$prev?>)"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/icalendar/icon_prev_off.gif" width="21" height="21" border="0" align="absmiddle" alt="<?=$Lang['Calendar']['PrevMonth']?>"/></a>
									<?=$curMonth?>
									<a href="javascript:click_next(<?=$next?>)"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/icalendar/icon_next_off.gif" width="21" height="21" border="0" align="absmiddle" alt="<?=$Lang['Calendar']['NextMonth']?>"/></a>
								<? } ?>
						</td>
					</tr>
					<tr>
						<td>
							<?
							if($display){
								echo $y;
							}
							else{
								if($_SESSION["SSV_PRIVILEGE"]["homework"]["is_subject_leader"])
									echo $Lang['SysMgr']['Homework']['MonthlyHomeworkListWarningSubjectLeader'];
								else
									echo $Lang['SysMgr']['Homework']['MonthlyHomeworkListWarning'];
							}
							?>
						</td>
					</tr>
				</table>
				<!--
				<table width="100%" border="0" cellpadding="5" cellspacing="0">
					<?if($display){	
						echo "<tr><td class=\"dotline\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"10\" height=\"1\" /></td></tr>
							<tr><td align=\"center\">".
								$linterface->GET_ACTION_BTN($Lang['Calendar']['PrevMonth'], "button", "javascript:click_previous($prev)")." ".
								$linterface->GET_ACTION_BTN($Lang['SysMgr']['Homework']['currentMonth'], "button", "javascript:click_current($current)")." ".
								$linterface->GET_ACTION_BTN($Lang['Calendar']['NextMonth'], "button", "javascript:click_next($next)")."
							 </td></tr>";
					}
					?>
				</table>
				-->
				<br />
				<input type="hidden" name="yearID" value="<?=$yearID?>"/>
				<input type="hidden" name="yearTermID" value="<?=$yearTermID?>"/>
				<input type="hidden" name="sid" value="<?=$subjectID?>"/>
			</form>
		</td>
	</tr>
</table>
<?
	$linterface->LAYOUT_STOP();
	intranet_closedb();
?>