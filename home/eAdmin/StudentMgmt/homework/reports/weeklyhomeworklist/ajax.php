<?
//Using:
/*
 * 	Log
 *
 * 	Description: output json format data
 *
 * 	2020-10-05 [Cameron] create this file
 */

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
//include_once($PATH_WRT_ROOT."includes/libclass.php");
//include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
include_once($PATH_WRT_ROOT."includes/json.php");

intranet_auth();
intranet_opendb();

//$lclass = new libclass();
$lhomework = new libhomework2007();
if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || (!$sys_custom['eHomework']['DailyHomeworkList']) || (!$_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] && !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"] && !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_subject_leader"] && !$lhomework->isViewerGroupMember($UserID)))
{
    include_once($PATH_WRT_ROOT."includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit;
}

if ($junior_mck) {
    if (phpversion_compare('5.2') != 'ELDER') {
        $characterset = 'utf-8';
    }
    else {
        $characterset = 'big5';
    }
}
else {
    $characterset = 'utf-8';
}

header('Content-Type: text/html; charset='.$characterset);

$ljson = new JSON_obj();

$json['success'] = false;
$ret = array();
$action = $_GET['action'] ? $_GET['action'] : $_POST['action'];
$remove_dummy_chars = ($junior_mck) ? false : true;	// whether to remove new line, carriage return, tab and back slash

if($_SESSION['UserType']==USERTYPE_STAFF && !$_SESSION['isTeaching']){
    $userType = "nonTeaching";
    $userID = '';
}
else if(($_SESSION['UserType']==USERTYPE_STAFF && $_SESSION['isTeaching']) || $lhomework->isViewerGroupMember($UserID)){
    $userType = "teaching";
    $userID = $_SESSION['UserID'];
}
else if($_SESSION['UserType']==USERTYPE_STUDENT && $_SESSION["SSV_PRIVILEGE"]["homework"]["is_subject_leader"]){
    $userType = "subjectLeader";
    $userID = '';
}

switch($action) {

    case 'getSubjectList':
        $classID = IntegerSafe($_POST['classID']);
        $yearD = IntegerSafe($_POST['yearID']);
        $yearTermID = IntegerSafe($_POST['yearTermID']);

        $subject = $lhomework->getTeachingSubjectList($userID, $yearID, $yearTermID, $classID);
        $subjectID='';

        $selectedSubject = $lhomework->getCurrentTeachingSubjects('name="subjectID" id="subjectID" onChange="js_change_subject();"', $subject, $subjectID, $Lang['SysMgr']['Homework']['AllSubjects'], true);
        $ret['subject'] = $selectedSubject;

        $subjectGroups = $lhomework->getTeachingSubjectGroupList($userID, $subjectID, $yearID, $yearTermID, $classID);
        $selectedSubjectGroup = getSelectByArray($subjectGroups, "name=subjectGroupID id=subjectGroupID", $subjectGroupID="", "", 0, $Lang['SysMgr']['Homework']['AllSubjectGroups']);
        $ret['subjectGroup'] = $selectedSubjectGroup;

        $json['success'] = true;
        break;

    case 'getSubjectGroupList':
        $classID = IntegerSafe($_POST['classID']);
        $yearD = IntegerSafe($_POST['yearID']);
        $yearTermID = IntegerSafe($_POST['yearTermID']);
        $subjectID = IntegerSafe($_POST['subjectID']);

        $subjectGroups = $lhomework->getTeachingSubjectGroupList($userID, $subjectID, $yearID, $yearTermID, $classID);
        $selectedSubjectGroup = getSelectByArray($subjectGroups, "name=subjectGroupID id=subjectGroupID", $subjectGroupID="", "", 0, $Lang['SysMgr']['Homework']['AllSubjectGroups']);
        $ret['subjectGroup'] = $selectedSubjectGroup;
        $json['success'] = true;

        break;

    case 'getAcademicYearAndYearTermByDate':
        $filterDate = $_POST['filterDate'];
        $academicYearTermInfo = getAcademicYearAndYearTermByDate($filterDate);
        $ret['AcademicYearID'] = $academicYearTermInfo['AcademicYearID'];
        $ret['YearTermID'] = $academicYearTermInfo['YearTermID'];
        $json['success'] = true;
        break;

    case 'getClassList':
        $classID = '';      // default empty
        $yearID = IntegerSafe($_POST['yearID']);
        $yearTermID = IntegerSafe($_POST['yearTermID']);
        if($_SESSION['UserType']==USERTYPE_STAFF && !$_SESSION['isTeaching']) {
            $classes = $lhomework->getAllClassInfo($yearID);
            $selectClass = getSelectByArray($classes, 'name="classID" id="classID" onChange="js_change_class();"', $classID, 0, 0, $i_general_all_classes);
        }
        else if(($_SESSION['UserType']==USERTYPE_STAFF && $_SESSION['isTeaching']) || $lhomework->isViewerGroupMember($UserID)) {

            if ($_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] || !$_SESSION['isTeaching'] || $lhomework->isViewerGroupMember($UserID)) {        # eHomework Admin
                $classes = $lhomework->getAllClassInfo($yearID);
            } else {                                                       # Class teacher / subject teacher
                $classes = $lhomework->getAllClassesInvolvedByTeacherID($yearID, $yearTermID, $UserID);
            }
            $selectClass = getSelectByArray($classes, 'name="classID" id="classID" onChange="js_change_class();"', $classID, 0, 0, $i_general_all_classes);
        }
        else if($_SESSION['UserType']==USERTYPE_STUDENT && $_SESSION["SSV_PRIVILEGE"]["homework"]["is_subject_leader"]) {
            $selectClass = '';
        }
        $ret['selectClass'] = $selectClass;
        $json['success'] = true;
        break;
}

if ($remove_dummy_chars) {
    foreach ((array)$ret as $k => $v) {
        $ret[$k] = remove_dummy_chars_for_json($v);
    }
}

if (($junior_mck) && (phpversion_compare('5.2') != 'ELDER')) {
    foreach ((array)$ret as $k => $v) {
        $ret[$k] = convert2unicode($v, true, 1);
    }
}

$json = array_merge($json, $ret);
echo $ljson->encode($json);

intranet_closedb();
?>