<?php
// Modifing by : 
		
$PATH_WRT_ROOT = "../../../../../../";
$CurrentPageArr['eAdminHomework'] = 1;

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libcycleperiods.php");
include_once($PATH_WRT_ROOT."includes/libcal.php");
include_once($PATH_WRT_ROOT."includes/libcalevent.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");

intranet_auth();
intranet_opendb();

$lhomework = new libhomework2007();

if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || (!$_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] && !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"] && !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_subject_leader"] && !$lhomework->isViewerGroupMember($UserID)))
{
	if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])
	{
		header("location: ../../settings/index.php");	
		exit;
	}
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$linterface = new interface_html();



if($_SESSION['UserType']==USERTYPE_STAFF && $_SESSION['isTeaching']){

	$className = $eHomework_Homework_List_All_Classes;
	if($classID!="") {
		$sql = "SELECT ".Get_Lang_Selection("ClassTitleB5","ClassTitleEN")." FROM YEAR_CLASS WHERE YearClassID=$classID";
		$clsName = $lhomework->returnVector($sql);
		$className = $clsName[0];
	}
		
	$subjectName = $Lang['SysMgr']['Homework']['AllSubjects'];
	
	if ($subjectID != "")
	{
		$sql = "SELECT IF('$intranet_session_language' = 'en', EN_DES, CH_DES) FROM ASSESSMENT_SUBJECT WHERE RecordID = $subjectID";
		$sname = $lhomework->returnVector($sql,1);
		$subjectName = $sname[0];
	}

	$subjectGroupName = $Lang['SysMgr']['Homework']['AllSubjectGroups'];

	if ($subjectGroupID != "")
	{
		$sql = "SELECT IF('$intranet_session_language' = 'en', ClassTitleEN, ClassTitleB5) FROM SUBJECT_TERM_CLASS WHERE SubjectGroupID = $subjectGroupID";
		$sgname = $lhomework->returnVector($sql,1);
		$subjectGroupName = $sgname[0];
	}

	$cond = (($subjectID=="" || $subjectID==-1) && $subjectGroupID=="") ? "a.AcademicYearID = $yearID AND a.YearTermID = $yearTermID" : (($subjectGroupID!="") ? " a.ClassGroupID = $subjectGroupID AND a.AcademicYearID = $yearID AND a.YearTermID = $yearTermID" : "a.SubjectID = $subjectID AND a.AcademicYearID = $yearID AND a.YearTermID = $yearTermID");
	$x = $lhomework->weeklyHomeworkList($ts, "", $cond, "", 1, $classID);
}


else if($_SESSION['UserType']==USERTYPE_STAFF && !$_SESSION['isTeaching']){
	
	$className = $eHomework_Homework_List_All_Classes;
	if($classID!="") {
		$sql = "SELECT ".Get_Lang_Selection("ClassTitleB5","ClassTitleEN")." FROM YEAR_CLASS WHERE YearClassID=$classID";
		$clsName = $lhomework->returnVector($sql);
		$className = $clsName[0];
	}
	
	$subjectName = $Lang['SysMgr']['Homework']['AllSubjects'];
	
	if ($subjectID != "")
	{
		$sql = "SELECT IF('$intranet_session_language' = 'en', EN_DES, CH_DES) FROM ASSESSMENT_SUBJECT WHERE RecordID = $subjectID";
		$sname = $lhomework->returnVector($sql,1);
		$subjectName = $sname[0];
	}

	$subjectGroupName = $Lang['SysMgr']['Homework']['AllSubjectGroups'];

	if ($subjectGroupID != "")
	{
		$sql = "SELECT IF('$intranet_session_language' = 'en', ClassTitleEN, ClassTitleB5) FROM SUBJECT_TERM_CLASS WHERE SubjectGroupID = $subjectGroupID";
		$sgname = $lhomework->returnVector($sql,1);
		$subjectGroupName = $sgname[0];
	}

	$cond = (($subjectID=="" || $subjectID==-1) && $subjectGroupID=="") ? "a.AcademicYearID = $yearID AND a.YearTermID = $yearTermID" : (($subjectGroupID=="")? "a.SubjectID = $subjectID AND a.AcademicYearID = $yearID AND a.YearTermID = $yearTermID" : " a.ClassGroupID = $subjectGroupID AND a.AcademicYearID = $yearID AND a.YearTermID = $yearTermID");
	$x = $lhomework->weeklyHomeworkList($ts, "", $cond, "", 1, $classID);
}

else if($_SESSION['UserType']==USERTYPE_STUDENT && $_SESSION["SSV_PRIVILEGE"]["homework"]["is_subject_leader"]){
	
	$subjectName = $Lang['SysMgr']['Homework']['AllSubjects'];
	$subjectGroupName = $Lang['SysMgr']['Homework']['AllSubjectGroups'];
	if ($subjectID != "")
	{
		$sql = "SELECT IF('$intranet_session_language' = 'en', a.EN_DES, a.CH_DES) AS SubjectName, 
				IF('$intranet_session_language' = 'en', c.ClassTitleEN, c.ClassTitleB5) AS SubjectGroupName FROM ASSESSMENT_SUBJECT AS a 
				LEFT OUTER JOIN SUBJECT_TERM AS b ON b.SubjectID = a.RecordID 
				LEFT OUTER JOIN SUBJECT_TERM_CLASS AS c on c.SubjectGroupID = b.SubjectGroupID 
				LEFT OUTER JOIN SUBJECT_TERM_CLASS_USER AS d on d.SubjectGroupID = c.SubjectGroupID
				WHERE d.UserID = $UserID AND a.RecordID = $subjectID";
		
		$result = $lhomework->returnArray($sql,2);
		$subjectName = $result[0]['SubjectName'];
		$subjectGroupName = $result[0]['SubjectGroupName'];
	}
	
	
	# Select Subjects
	$subject = $lhomework->getStudyingSubjectList($UserID, 1, $yearID, $yearTermID);
		
	if(sizeof($subject)!=0){
		$allSubjects = " a.SubjectID IN (";
		for ($i=0; $i < sizeof($subject); $i++)
		{	
			list($ID)=$subject[$i];
			$allSubjects .= $ID.",";
		}
		$allSubjects = substr($allSubjects,0,strlen($allSubjects)-1).")";
		$allSubjects .= " AND";
	}
	else{
		$allSubjects ="";
	}
	
	$cond = ($subjectID=="" || $subjectID==-1)? "$allSubjects a.AcademicYearID = $yearID AND a.YearTermID = $yearTermID" : " a.SubjectID = $subjectID AND a.AcademicYearID = $yearID AND a.YearTermID = $yearTermID";
	$x = $lhomework->weeklyHomeworkList($ts, "", $cond, 1, 1);
}
?>


<table width='100%' align='center' class='print_hide' border=0>
	<tr>
		<td align='right'><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></td>
	</tr>
</table>

<!-- Homework Title -->
<table width='100%' align="center" border="0">
	<tr>
        	<td align="center" ><b><?=$Lang['SysMgr']['Homework']['WeeklyHomeworkList']?></b></td>
	</tr>
</table>        

<br>
<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
<? if($_SESSION['UserType']==USERTYPE_STAFF) { ?>
	<tr><td align="left" valign="top" width="100"><?=$i_ClassName?> : </td><td align="left" valign="top"><?=$className?></td></tr>
<? } ?>
	<tr><td align="left" valign="top" width="100"><?=$Lang['SysMgr']['Homework']['Subject']?> : </td><td align="left" valign="top"><?=$subjectName?></td></tr>
	<tr><td align="left" valign="top" width="100"><?=$Lang['SysMgr']['Homework']['SubjectGroup']?> : </td><td align="left" valign="top"><?=$subjectGroupName?></td></tr>
</table>
<br>

<table class="eHomeworktableborder" width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<?=$x?>
</table>
<?
include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_footer.php");
intranet_closedb();
?>