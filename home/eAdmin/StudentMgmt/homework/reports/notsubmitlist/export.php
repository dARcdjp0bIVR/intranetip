<?php
// Modifing by :

##### Change Log [Start] #####
#
#   Date    :   2019-09-06 (Tommy)
#               change access permission checking
#
###### Change Log [End] ######	

	session_start();
	$targetList = $_SESSION["targetList"];
	$studentList = $_SESSION["studentList"];
	
	$PATH_WRT_ROOT = "../../../../../../";
	$CurrentPageArr['eAdminHomework'] = 1;

	include_once($PATH_WRT_ROOT."includes/global.php");
	include_once($PATH_WRT_ROOT."includes/libdb.php");
	include_once($PATH_WRT_ROOT."includes/libexporttext.php");
	include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
	include_once($PATH_WRT_ROOT."includes/libhomework.php");
	include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
	include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

	intranet_auth();
	intranet_opendb();
	
	$lhomework = new libhomework2007();

	if(!$plugin['eHomework']){
		include_once($PATH_WRT_ROOT."includes/libaccessright.php");
		$laccessright = new libaccessright();
		$laccessright->NO_ACCESS_RIGHT_REDIRECT();
		exit;
	}

	else{
// 		if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || !($_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"] || $_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] && !$lhomework->isViewerGroupMember($UserID)))
        if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || (!$_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"] && !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] && !$lhomework->isViewerGroupMember($UserID)))
        {
			if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])
			{
				header("location: ../../settings/index.php");	
				exit;
			}
			include_once($PATH_WRT_ROOT."includes/libaccessright.php");
			$laccessright = new libaccessright();
			$laccessright->NO_ACCESS_RIGHT_REDIRECT();
			exit;
		}
	}

	
	$lexport = new libexporttext();

	if(!isset($status) || $status=="") $status = "-1";
	$statusAry = $lhomework->Get_Handin_Status($returnAssociateArray=1);
	
	$ExportArr = array();
		
	$filename = "HandinStatusList.csv";
	
		# Select Data from DB
	$row = $lhomework->getNotSubmitListPrint($yearID, $yearTermID, $subjectID, $targetID, $targetList, $studentList, $startDate, $endDate, $dateChoice, $times, $range, $order, $group, $DueDateEqualToToday, $status);

	// Create data array for export
	for($i=0; $i<sizeof($row); $i++){
		$m = 0;
		$ExportArr[$i][$m] = $row[$i][0];		//Class
		$m++;
		if($group==1) {
			$ExportArr[$i][$m] = $row[$i][1];	//Class Number
			$m++;
		}
		if($group==1) {
			$ExportArr[$i][$m] = $row[$i][4];	//Student Name
			$m++;			
		}
		$ExportArr[$i][$m] = $row[$i][2];		//Subject
		$m++;
		$ExportArr[$i][$m] = $row[$i][3];		//Subject Group
		$m++;
		$ExportArr[$i][$m] = $row[$i][5];		//Not Sumbit Count
		$m++;
	}

	if(sizeof($row)==0) {
		$ExportArr[0][0] = $i_no_record_exists_msg;	
	}

	$exportColumn[] = $Lang['SysMgr']['Homework']['Class'];
	if($group==1)
		$exportColumn[] = $Lang['SysMgr']['Homework']['ClassNumber'];
	if($group==1)
		$exportColumn[] = $Lang['SysMgr']['Homework']['StudentName'];
	$exportColumn[] = $Lang['SysMgr']['Homework']['Subject'];
	$exportColumn[] = $Lang['SysMgr']['Homework']['SubjectGroup'];
	$exportColumn[] = $statusAry[$status]." ".$Lang['eHomework']['Count'];
	

	$export_content = $lexport->GET_EXPORT_TXT($ExportArr, $exportColumn, "", "\r\n", "", 0, "00");

	intranet_closedb();

	// Output the file to user browser
	$lexport->EXPORT_FILE($filename, $export_content);
?>