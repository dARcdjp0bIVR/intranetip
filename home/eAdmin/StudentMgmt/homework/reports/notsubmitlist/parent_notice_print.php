<?php
// Modifing by :

##### Change Log [Start] #####
#
#   Date    :   2019-09-06 (Tommy)
#               change access permission checking
#
###### Change Log [End] ######	
	
	$PATH_WRT_ROOT = "../../../../../../";
	$CurrentPageArr['eAdminHomework'] = 1;

	include_once($PATH_WRT_ROOT."includes/global.php");
	include_once($PATH_WRT_ROOT."includes/libdb.php");
	include_once($PATH_WRT_ROOT."includes/libuser.php");
	include_once($PATH_WRT_ROOT."includes/libhomework.php");
	include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
	include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
	include_once($PATH_WRT_ROOT."includes/libinterface.php");

	$targetList = $targetListResult;
	$studentList = $studentListResult;
	
	intranet_auth();
	intranet_opendb();
	
	$lhomework = new libhomework2007();

	if(!$plugin['eHomework']){
		include_once($PATH_WRT_ROOT."includes/libaccessright.php");
		$laccessright = new libaccessright();
		$laccessright->NO_ACCESS_RIGHT_REDIRECT();
		exit;
	}

	else{
// 		if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || !($_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"] || $_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] && !$lhomework->isViewerGroupMember($UserID)))
        if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || (!$_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"] && !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] && !$lhomework->isViewerGroupMember($UserID)))
        {
			if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])
			{
				header("location: ../../settings/index.php");	
				exit;
			}
			include_once($PATH_WRT_ROOT."includes/libaccessright.php");
			$laccessright = new libaccessright();
			$laccessright->NO_ACCESS_RIGHT_REDIRECT();
			exit;
		}
	}
	
	$linterface = new interface_html("popup_no_layout.html");

	$dateChoice = "DATE";
	
	# Select Data from DB
	$order = 1;  # must order by student in order to group different subjects
	$times_boundary = $times;
	$times = 1;
	$row = $lhomework->getNotSubmitListPrint_Format2($yearID, $yearTermID, $subjectID, $targetID, $targetList, $studentList, $startDate, $endDate, $dateChoice, $times, $range, $order, $group, $DueDateEqualToToday,$status);


	$previous_user_id = 0;
	$SubjectsInvolved = array();
	
	if ($yearID!="" && $yearTermID!="")
	{
		$Name = $lhomework->getAcademicYearNameAndYearTermName($yearID, $yearTermID);
		list($yearName, $yearTermName) = $Name[0];
	}
	
	$print_count = 0;
	$letters = "";
	
	switch ($status)
	{
		case -1: $ReportTypeTitle = "欠交";
		break;
		
		
		case 2: $ReportTypeTitle = "遲交";
		break;
		
		
		case -3: $ReportTypeTitle = "欠交或遲交";
		break;
	}
	
	$HomeworkCountByType = array();
	$HomeworkList = array();
	for($i=0; $i<sizeof($row); $i++)
	{
		list($ClassName, $ClassNumber, $Subject, $SubjectGroup, $StudentName, $Topic, $Desciprtion, $StartDate, $DueDate, $CountNumber, $StudentID, $Status) = $row[$i];
		//if (($previous_user_id!=$StudentID && $i>0) || $i==sizeof($row)-1)
		$HomeworkList[] = array($DueDate, $Subject, $Topic, $Desciprtion);
		if ($StudentID!=$row[$i+1]["UserID"])
		{
			$print_count ++;
			
			$HomeworkCountByType[$Status] ++;
			
			
			$TotalRecords = (int) $HomeworkCountByType[2] + (int) $HomeworkCountByType[-1];
			if ($TotalRecords<$times_boundary)
			{
				# no need to show (throw the record)
				$HomeworkCountByType = array();
				$HomeworkList = array();
				continue;
			}
			
			
			if ($letters!="")
			{
				$letters .= '<table width="100%" border="0" cellpadding="0" cellspacing="0" class="print_hide" ><tr><td style="border-bottom:dashed orange 2px;">&nbsp;</td></tr></table>';
			}
			
			$Records = "";
			# -1 for no submission; 2 for late
			if ($HomeworkCountByType[-1]!="" && $HomeworkCountByType[-1]>0)
			{
				$Records = "欠交家課 ".$HomeworkCountByType[-1]." 次";
			}
			
			if ($HomeworkCountByType[2]!="" && $HomeworkCountByType[2]>0)
			{
				$Records = (($Records!="") ? "欠交 ".$HomeworkCountByType[-1]." 次及" : "") . "遲交家課 ".$HomeworkCountByType[2]." 次";
			}
			
			$StartMonth = date("n", strtotime($startDate));
			$StartDay = date("j", strtotime($startDate));
			$EndMonth = date("n", strtotime($endDate));
			$EndDay = date("j", strtotime($endDate));
			
			$NowYear = date("Y");
			$NowMonth = date("n");
			$NowDay = date("j");
			
			
			//$letters_main = "<tr><td align='center'><u>東華三院馬振玉紀念中學</u></td></tr>";
			//$letters_main .= "<tr><td align='center'><u>欠交家課通知函</u></td></tr>";
			$letters_main = "<tr><td align='center'>&nbsp;</td></tr>";
			$letters_main .= "<tr><td align='justify'><p>敬啟者　查　貴子弟 {$StudentName}（$ClassName 班 $ClassNumber 號）從 {$StartMonth} 月 {$StartDay} 日至 {$EndMonth} 月 {$EndDay} 日共{$Records}。" .
					"謹請　台端留意並加強督促　貴子弟須準時交家課。煩請簽署回條。</p>" .
					"<p>　　此致</p>" .
					"<p>貴家長</p>" .
					"</td></tr>";
			$letters_main .= "<tr><td align='right'><p>教務委員會　啟</p></td></tr>";
			$letters_main .= "<tr><td>{$NowYear} 年 {$NowMonth} 月 {$NowDay} 日</td></tr>";
			//<p class="page_break" clear="all" />
			
			$letters_reply = "<tr><td align='center'>&nbsp;</td></tr>";
			$letters_reply .= "<tr><td ><p>本人已知悉上述事項，並會督促敝子弟 {$StudentName}（$ClassName 班 $ClassNumber 號）準時交家課。</p>" .
					"<p>&nbsp;</p>" .
					"<p>&nbsp;</p>" .
					"</td></tr>";
			$letters_reply .= "<tr><td height='30' align='right'><p>家長簽署：_______________</p></td></tr>";
			$letters_reply .= "<tr><td height='30' align='right'><p>家長姓名：_______________</p></td></tr>";
			$letters_reply .= "<tr><td>{$NowYear} 年 　　月 　　日</td></tr>";
			
			$page_break00 = ($i<sizeof($row)-1) ? " style='page-break-after:always;' " : "";
			//$page_break = ($i<sizeof($row)-1) ? '<p class="page_break" clear="all" />' : "";
			
			$letters .= "\n\n<br /><table width='680' align='center' height='950' style='page-break-after:always;' border='0'>\n";
			$letters .= "<tr><td height='100%' valign='top'>\n";
				$letters .= "<table width='680' align='center' >\n";
				$letters .= "<tr><td align='center'><u>東華三院馬振玉紀念中學</u></td></tr>\n";
				$letters .= "<tr><td align='center'><u><b>{$ReportTypeTitle}家課通知函</b></u></td></tr>\n";
				$letters .= "<tr><td align='center'>&nbsp;</td></tr>\n";
				$letters .= $letters_main;
				$letters .= "<tr><td height=\"130\">&nbsp;</td></tr>\n";
				$letters .= "<tr><td style='border-bottom:1px dashed #555555;'>&nbsp;</td></tr>\n";
				$letters .= "<tr><td >&nbsp;</td></tr>\n";
				$letters .= "<tr><td align='center'><u>東華三院馬振玉紀念中學</u></td></tr>\n";
				$letters .= "<tr><td align='center'><u><b>回條</b></u></td></tr>\n";
				$letters .= "<tr><td align='center'>&nbsp;</td></tr>\n";
				$letters .= $letters_reply;
				$letters .= "<tr><td height='120'>&nbsp;</td></tr>\n";
				$letters .= "</table> \n";
			$letters .= "</td></tr>\n";
			$letters .= "<tr><td valign='bottom' align='center'><font color='#777777'>1</font></td></tr>\n";
			$letters .= "</table> \n";
			
			# homework details
			$letters .= '<table width="100%" border="0" cellpadding="0" cellspacing="0" class="print_hide" ><tr><td style="border-bottom:dashed orange 2px;">&nbsp;</td></tr></table>';
			
			if (is_array($HomeworkList) && sizeof($HomeworkList)>0)
			{
				$HomeworkList = sortByColumn($HomeworkList, 0, 1);
			}
			
			$letters .= "\n\n<br /><table width='680' align='center' height='950' {$page_break00}  >\n";
			$letters .= "<tr><td height='100%' valign='top'><br />學生{$ReportTypeTitle}家課詳情如下：<br /><br />\n";
				$letters .= "<table width='680' align='center' border='0' cellpadding='5' cellspacing='0'>\n";
				$letters .= "<tr><td align='center' width='100' height='26' style='border-left:1px #666666 solid;border-bottom:1px #666666 solid;border-top:1px #666666 solid;'>日期	</td><td align='center' width='160' style='border-left:1px #666666 solid;border-top:1px #666666 solid;border-bottom:1px #666666 solid;'>科目</td><td align='center' style='border:1px #666666 solid;'>家課內容</td></tr>\n";
				for ($j=0; $j<sizeof($HomeworkList); $j++)
				{
					$hwObj = $HomeworkList[$j];
					$letters .= "<tr><td align='center' height='26' style='border-left:1px #666666 solid;border-bottom:1px #666666 solid;'>".$hwObj[0]."</td><td align='center' style='border-left:1px #666666 solid;border-bottom:1px #666666 solid;'>".$hwObj[1]."</td><td border='1' style='border-left:1px #666666 solid;border-bottom:1px #666666 solid;border-right:1px #666666 solid;'>".(($hwObj[2]=="") ? "--" :$hwObj[2])."</td></tr>\n";
				}
				$letters .= "</table> \n";
			$letters .= "</td></tr>\n";
			$letters .= "<tr><td valign='bottom' align='center'><font color='#777777'>2</font></td></tr>\n";
			$letters .= "</table> \n";
			
			
			$HomeworkCountByType = array();
			$HomeworkList = array();
		} else
		{
			$HomeworkCountByType[$Status] ++;
		}
		//$previous_user_id = $StudentID;
	}



$displayTable = '<table width="680" border="0" cellspacing="0" cellpadding="5" valign="top" align="center">';
if ($letters=="")
{
	$displayTable .= '<tr><td align="center"><br /><br />'.$Lang['General']['NoRecordAtThisMoment'].'</td></tr>';
} else
{
	$displayTable .= '<tr><td align="right" class="print_hide">';
	$displayTable .= $linterface->GET_BTN($button_print, "button","javascript:window.print()");
	$displayTable .= '</td></tr>';
	$displayTable .= '<tr><td style="vertical-align:top;">';
	$displayTable .= $letters;
	$displayTable .= '</td></tr>';
}
$displayTable .= '</table>';

//include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_header.php");

# Start layout
$linterface->LAYOUT_START();
?>

<style type='text/css'>

html, body { margin: 0px; padding: 0px; } 

td { font-size:14px; }

</style>


<?= $displayTable ?>


<?php
intranet_closedb();
///include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_footer.php");

$linterface->LAYOUT_STOP();
?>