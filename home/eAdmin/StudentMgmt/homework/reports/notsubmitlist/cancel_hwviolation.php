<?php
// Modifing by : 
		
##### Change Log [Start] #####
#
#   Date    :   2019-09-06 (Tommy)
#               change access permission checking
#
#	Date	:	2013-12-06 (YatWoon)
#				modified: remove "include due date" and "hide year/term" option
#
#	Date	:	2013-09-23 (Roy Law)
#				add send push message thickbox
#
#	Date	:	2010-03-04 (Henry Chow)
#				assign $yearID if "DATE" range option is selected
#
###### Change Log [End] ######		
		
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");

intranet_auth();
intranet_opendb();

$lhomework = new libhomework2007();

if(!$plugin['eHomework']){
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

else{
	if(
		$_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"]
		|| !$sys_custom['eHomework_Status_Supplementary_WitTwoHandInStatusManage']
		|| (!$_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"]
			&& !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] && !$lhomework->isViewerGroupMember($UserID)
		)
	)
	{
		if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])
		{
			header("location: ../../settings/index.php");	
			exit;
		}
		include_once($PATH_WRT_ROOT."includes/libaccessright.php");
		$laccessright = new libaccessright();
		$laccessright->NO_ACCESS_RIGHT_REDIRECT();
		exit;
	}
}

# Temp Assign memory of this page
ini_set("memory_limit", "150M");

$linterface = new interface_html();

if(!isset($status) || $status=="") $status = "-1";

$CurrentPageArr['eAdminHomework'] = 1;

$CurrentPage = "Reports_CancelViolationRecord";

$MODULE_OBJ = $lhomework->GET_MODULE_OBJ_ARR();

$PAGE_TITLE = $Lang['SysMgr']['Homework']['CancelViolationRecord'];

# tag information
$TAGS_OBJ[] = array($Lang['SysMgr']['Homework']['CancelViolationRecord'], "");

$_SESSION['targetList'] = $targetListResult;
$_SESSION['studentList'] = $studentListResult;

# Date link
$dateChoice ='DATE';		# hard-code, date only,  no year selection

$yearID = Get_Current_Academic_Year_ID();
$handin_param = array(
		"yearID" => $yearID,
		"dataopt" => (isset($_GET["dataopt"])) ? $_GET["dataopt"] : "duedate_range",
		"startDate" => (isset($_GET["startDate"])) ? $_GET["startDate"] : date("Y-m-d"),
		"endDate" => (isset($_GET["endDate"])) ? $_GET["endDate"] : date("Y-m-d"),
		"selectedDate" => (isset($_GET["selectedDate"])) ? $_GET["selectedDate"] : date("Y-m-d"),
		"custOrderBy" => "studentID",
		"cancelOnly" => true,
		"isReport" => true,
		"callBy" => basename(__FILE__)
);

$queryStr = "startDate=" . $handin_param["startDate"] . "&endDate=" . $handin_param["endDate"] . "&dataopt=" . $handin_param["dataopt"] . "&selectedDate=" . $handin_param["selectedDate"] . "";

# Print link
$printPreviewLink = "print_hwsp_preview.php?" . $queryStr . "&hwrp_type=cancel_violation";

# Export link
$exportLink = "export_hwsp.php?" . $queryStr . "&hwrp_type=cancel_violation";

/*
# Export link
$exportLink1 = "export.php?times=$times&range=$range&order=$order&group=$group&$dateLink&subjectID=$subjectID&targetID=$targetID&DueDateEqualToToday=$DueDateEqualToToday&status=$status";
$exportLink2 = "export2.php?times=$times&range=$range&order=$order&group=$group&$dateLink&subjectID=$subjectID&targetID=$targetID&DueDateEqualToToday=$DueDateEqualToToday&status=$status";
*/

# Toolbar: print, export
# Toolbar: print, export
$toolbar = $linterface->GET_LNK_PRINT("javascript:newWindow('$printPreviewLink', 23)", "", "", "", "", 0 );
$toolbar .= $linterface->GET_LNK_PRINT("javascript:newWindow('" . $printPreviewLink . "&hiddenname=1', 23)", $Lang['SysMgr']['Homework']["PrintWithoutSudtentName"], "", "", "", 0 );

$allowExport = $lhomework->exportAllowed;
$allowExport = true;
if($allowExport) {
	$toolbar .= $linterface->GET_LNK_EXPORT($exportLink, "", "", "", "", 0 );
	/*
	 $toolbar .= '<div class="Conntent_tool">
		<div class="btn_option">
		<a href="javascript:;" class="export" id="btn_edit" onclick="MM_showHideLayers(\'edit_option\',\'\',\'show\');">'.$button_export.'</a>
		<br style="clear:both" />
		<div class="btn_option_layer" id="edit_option" onclick="MM_showHideLayers(\'edit_option\',\'\',\'hide\');">
		<a href="'.$exportLink1.'" class="sub_btn">'.$Lang['eHomework']['ExportFormat1'].'</a>
		<a href="'.$exportLink2.'" class="sub_btn">'.$Lang['eHomework']['ExportFormat2'].'</a>
		</div>
		</div>
		</div>';
		*/
}
$linterface->LAYOUT_START();

$handin_studentsArr = $lhomework->getStudentsByParam($handin_param);

$result = '<table width="100%" border="0" align="center" cellpadding="5" cellspacing="0">' . "\n";
$result .= '<thead>';
$result .= '<tr>';
$result .= '	<th width="5%" class="tablebluetop tabletopnolink">#</th>';
$result .= '	<th width="10%" class="tablebluetop tabletopnolink">' . $Lang['SysMgr']['Homework']['Class'] . " / " . $Lang['SysMgr']['Homework']['ClassNumber'] . "</th>\n";
$result .= '	<th width="10%" class="tablebluetop tabletopnolink">' . $Lang['SysMgr']['Homework']['StudentName'] . "</th>\n";
$result .= '	<th width="10%" class="tablebluetop tabletopnolink">' . $Lang['SysMgr']['Homework']['DueDateForViolation'] . "</th>\n";
$result .= '	<th width="10%" class="tablebluetop tabletopnolink">' . $Lang['SysMgr']['Homework']['Subject'] . "</th>\n";
$result .= '	<th width="10%" class="tablebluetop tabletopnolink">' . $Lang['SysMgr']['Homework']['Topic'] . "</th>\n";
$result .= '	<th width="10%" align="center" class="tablebluetop tabletopnolink">' . $Lang['SysMgr']['Homework']['CancelViolationDate'] . "</th>\n";
$result .= '</tr>';
$result .= '</thead>';

if (count($handin_studentsArr) > 0) {
	$i = 1;
	foreach ($handin_studentsArr as $kk => $vv) {
		$studentName = $vv["StudentName"];
		if (empty($studentName)) $studentName = $vv["UserLogin"];
		$result .= '<tbody>';
		if ($i % 2 == 0) {
			$result .= '<tr class="tablerow_total">';
		} else {
			$result .= '<tr>';
		}
		$result .= '	<td>' . $i . "</td>\n";
		$result .= '	<td>' . $vv["ClassTitle"] . " / " . $vv["ClassNumber"] . "</td>\n";
		$result .= '	<td>' . $studentName . "</td>\n";
		$result .= '	<td>' . $vv["DueDate"] . "</td>\n";
		$result .= '	<td>' . $vv["Subject"] . "</td>\n";
		$result .= '	<td>' . $vv['Title'] . "</td>\n";
		$result .= '	<td align="center">' . $vv["CancelViolationDate"] . "</td>\n";
		$result .= '</tr>';
		$result .= '</tbody>';
		$i++;
	}
} else {
	$result .= "<tbody>";
	$result .= "<tr>";
	$result .= "	<td align=center colspan='7'>".$Lang['SysMgr']['Homework']['NoRecord']."</td>";
	$result .= "</tr>";
	$result .= "</tbody>";
}
$result .= '</table>';
?>
<script language="javascript">

var eHomeworkAppJS = {
	vars: {},
	listeners: {
		dateChangeHandler: function() {
			/*
			var strURL = window.location.href;
			var tmp = strURL.split("?selectedDate=");
			var url = tmp[0];
			window.location.href = url + "?selectedDate=" + $('#selectedDate').val() + "&startDate=" + $('#startDate').val() + "&endDate=" + $('#endDate').val() + "&dataopt=" + $('input[name="dataopt"]').val();
			*/
		},
		checkform: function(form) {
			form.submit();
		},
		dueDateHandler: function(e) {
			$('input:radio[value="duedate_range"]').eq(0).trigger('click');
		},
		selectDateHandler: function() {
			$('input:radio[value="confirm_date"]').eq(0).trigger('click');
		}
	},
	func: {
		init: function() {
			$('#startDate').bind('focus', eHomeworkAppJS.listeners.dueDateHandler);
			$('#endDate').bind('focus', eHomeworkAppJS.listeners.dueDateHandler);
			$('#selectedDate').bind('focus', eHomeworkAppJS.listeners.selectDateHandler);
		}
	}	
};

$(document).ready(function () {
	eHomeworkAppJS.func.init();
});

function back(){
	window.location = "index.php";
}

</script>
<form name="form1">
	<table width="100%" border="0" cellpadding="5" cellspacing="5">
		<tr>
			<td>
				<table border="0" cellspacing="0" cellpadding="2" width="300">
					<tr>
						<td><?=$toolbar?></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td>
				<input type='radio' name='dataopt' value='duedate_range' <?php if (empty($_GET["dataopt"]) || $_GET["dataopt"] == "duedate_range") { echo " checked"; } ?>>
				<?php echo $Lang['SysMgr']['Homework']['DueDate']; ?> :
				<?=$Lang['SysMgr']['Homework']["From"]?>
				<?php echo $linterface->GET_DATE_PICKER("startDate", $handin_param["startDate"], "", "yy-mm-dd", "", "", "", "eHomeworkAppJS.listeners.dateChangeHandler()"); ?>
				<?=$Lang['SysMgr']['Homework']["To"]?>
				<?php echo $linterface->GET_DATE_PICKER("endDate", $handin_param["endDate"], "", "yy-mm-dd", "", "", "", "eHomeworkAppJS.listeners.dateChangeHandler()"); ?>
			</td>
		</tr>
		<tr>
			<td>
				<input type='radio' name='dataopt' value='confirm_date' <?php if ($_GET["dataopt"] == "confirm_date") { echo " checked"; } ?>>
				<?php echo $Lang['SysMgr']['Homework']['CancelViolationRecordDate']; ?> :
				<?php echo $linterface->GET_DATE_PICKER("selectedDate", $handin_param["selectedDate"], "", "yy-mm-dd", "", "", "", "eHomeworkAppJS.listeners.dateChangeHandler()"); ?>
			</td>
		</tr>
		<tr>
			<td><?php echo $linterface->GET_ACTION_BTN($button_submit, "button", "eHomeworkAppJS.listeners.checkform(document.form1)"); ?></td>
		</tr>
		<tr>
			<td><?php echo $result; ?></td>
		</tr>
		<tr>
			<td class="dotline">
				<img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1">
			</td>
		</tr>
	</table>
	<input type="hidden" name="targetList[]" value="<?=$targetListResult?>"/>
	<input type="hidden" name="studentList[]" value="<?=$studentListResult?>"/>
</form>

<?=$thickboxContent?>

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>