<?php
# editing by: 
/***************************************************************************
*	modification log:
*	2019-09-06 (Tommy)
*         -  change access permission checking
*
***************************************************************************/

	session_start();
	
	$PATH_WRT_ROOT = "../../../../../../";
	$CurrentPageArr['eAdminHomework'] = 1;

	include_once($PATH_WRT_ROOT."includes/global.php");
	include_once($PATH_WRT_ROOT."includes/libdb.php");
	include_once($PATH_WRT_ROOT."includes/libuser.php");
	include_once($PATH_WRT_ROOT."includes/libsmsv2.php");
	include_once($PATH_WRT_ROOT."includes/libhomework.php");
	include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
	
	$targetList = $_SESSION["targetList"];
	$studentList = $_SESSION["studentList"];
	
	intranet_auth();
	intranet_opendb();

	$libsms = new libsmsv2();
	$lhomework = new libhomework2007();
	$luser = new libuser();

	if(!$plugin['eHomework']){
		include_once($PATH_WRT_ROOT."includes/libaccessright.php");
		$laccessright = new libaccessright();
		$laccessright->NO_ACCESS_RIGHT_REDIRECT();
		exit;
	}

	else{
//		if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || !($_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"] || $_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] && !$lhomework->isViewerGroupMember($UserID)))
        if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || (!$_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"] && !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] && !$lhomework->isViewerGroupMember($UserID)))
        {
			if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])
			{
				header("location: ../../settings/index.php");	
				exit;
			}
			include_once($PATH_WRT_ROOT."includes/libaccessright.php");
			$laccessright = new libaccessright();
			$laccessright->NO_ACCESS_RIGHT_REDIRECT();
			exit;
		}
	}

	
	
	if(!isset($status) || $status=="" || $status != "-1")
	{
		die("This function is to inform parents for their children's records of not handing homework only!");
	}
	$ExportArr = array();
		
	$filename = "HandinStatusList.csv";
	
	# Select Data from DB
	$order = 1;  # must order by student in order to group different subjects
	$row = $lhomework->getNotSubmitListPrint_Format2($yearID, $yearTermID, $subjectID, $targetID, $targetList, $studentList, $startDate, $endDate, $dateChoice, $times, $range, $order, $group, $DueDateEqualToToday,$status);

	$previous_user_id = 0;
	$SubjectsInvolved = array();
	
	if ($yearID!="" && $yearTermID!="")
	{
		$Name = $lhomework->getAcademicYearNameAndYearTermName($yearID, $yearTermID);
		list($yearName, $yearTermName) = $Name[0];
	}
	
	$namefield = ($intranet_default_lang =="b5" || $intranet_default_lang == "gb") ? 'ChineseName' : 'EnglishName';
	
	// $libuser = new libuser($UserID);
	// $notifyuser = $libuser->UserNameLang();
	
	// $asl = new ASLParentApp($asl_parent_app["host"], $asl_parent_app["path"]);
	
	// $SchoolID = isset($asl_parent_app['SchoolID'])? $asl_parent_app['SchoolID'] : $_SERVER['SERVER_NAME'];
	// $MsgNotifyDateTime = date("Y-m-d H:i:s");

	// $NotifyMessageID = -1;

	if (sizeof($row)==0)
	{
		// header("Location: index.php?xmsg=SMSsubmittedfailed");
		// exit();
	}
	
	$error_list=array();
	$valid_list=array();
	for($i=0; $i<sizeof($row); $i++)
	{
		list($ClassName, $ClassNumber, $Subject, $SubjectGroup, $StudentName, $Topic, $Desciprtion, $StartDate, $DueDate, $CountNumber, $StudentID) = $row[$i];
		if ($StudentID!=$row[$i+1]["UserID"])
		{
			# message content
			$SubjectsInvolved[] = "(".$Subject.") ". $Topic;
			$SubjectsText = implode(", ", $SubjectsInvolved);
			$StudentNameApps = $StudentName;
			if ($ClassName!="" && $ClassNumber!="")
			{
				$StudentNameApps .= " ({$ClassName}-{$ClassNumber})";
			}       
			if ($yearName!="" || $yearTermName!="")
			{
				$DateInvolved = $Lang['AppNotifyMessage']['Homework']['In'] . $yearName . " " . $yearTermName;
			} elseif ($startDate==$endDate)
			{
				$DateInvolved = $Lang['AppNotifyMessage']['Homework']['On']. $startDate;
			} else
			{
				$DateInvolved = $Lang['AppNotifyMessage']['Homework']['From'] . $startDate. $Lang['AppNotifyMessage']['Homework']['To'] .$endDate. $Lang['AppNotifyMessage']['Homework']['Period'];
			}
			$MessageCentent = str_replace("[DateInvolved]", $DateInvolved, str_replace("[Subjects]", $SubjectsText, str_replace("[StudentName]", $StudentNameApps, $Lang['AppNotifyMessage']['Homework']['Alert']['sms'])));
			
			$ParentInfoArr = $luser->getParent($StudentID, $sortByClass=0, $filterAppParent=0);
			
			foreach ((array)$ParentInfoArr as $ParentInfo) {
				$ParentID = $ParentInfo['ParentID'];
				$sql = "
						SELECT 
							UserID, 
							TRIM(MobileTelNo) as Mobile,
							$namefield as UserName
						FROM 
							INTRANET_USER 
						WHERE 
							UserID = $ParentID
						ORDER BY 
							ClassName,
							ClassNumber,
							EnglishName
					";
				$users = $libsms->returnArray($sql, 3);

				// list($userid,$mobile) = $users[$i];
				$ParentID = $users[0]['UserID'];
				$mobile = $users[0]['Mobile'];
				$ParentName = $users[0]['UserName'];
				
				if ($ClassNumber==0)
				{
					$ClassNumber = "";
				}
				$mobile = $libsms->parsePhoneNumber($mobile);
				if($libsms->isValidPhoneNumber($mobile)){
					$recipientData[] = array($mobile,$StudentID,addslashes($ParentName),$MessageCentent);
					$valid_list[] = array($ParentID,$ParentName,$mobile,$StudentName,$ClassName,$ClassNumber);
				}else{
					$reason =$i_SMS_Error_NovalidPhone;
					$error_list[] = array($ParentID,$ParentName,$mobile,$StudentName,$ClassName,$ClassNumber,$reason);
				}
			}
			
			$SubjectsInvolved = array();
		} else {
			$SubjectsInvolved[] = "(".$Subject.") ". $Topic;
		}
	}
	
	# send sms
	if(sizeof($recipientData)>0){
		$targetType = 3;
		$picType = 2;
		$adminPIC =$PHP_AUTH_USER;
		$userPIC = $UserID;
		$frModule= "";
		$deliveryTime = $time_send;
		$isIndividualMessage=true;
		$sms_message = ""; #### Use original text - not htmlspecialchars processed
		$returnCode = $libsms->sendSMS($sms_message,$recipientData, $targetType, $picType, $adminPIC, $userPIC,"",$deliveryTime,$isIndividualMessage);
		$isSent = 1;
		$smsTotal = sizeof($recipientData);
	}
	
	# return message
	if($isSent && ($returnCode > 0 && $returnCode==true))
	{
		intranet_closedb();
		echo "<font color='#DD5555'>".str_replace("[smsTotal]", $smsTotal, $Lang['AppNotifyMessage']['Homework']['SMS_send_result']). "</font>";
	} else
	{
		echo "<font color='red'>".$Lang['AppNotifyMessage']['Homework']['send_failed']."</font>";
	}
?>