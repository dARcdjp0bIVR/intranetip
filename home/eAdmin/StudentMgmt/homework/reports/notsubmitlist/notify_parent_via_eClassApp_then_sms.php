<?php
// Modifing by : 

##### Change Log [Start] #####
#
#   Date    :   2019-09-06 (Tommy)
#               change access permission checking
#
###### Change Log [End] ######	
session_start();
$targetList = $_SESSION["targetList"];
$studentList = $_SESSION["studentList"];

$PATH_WRT_ROOT = "../../../../../../";
$CurrentPageArr['eAdminHomework'] = 1;

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libsmsv2.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp.php");

intranet_auth();
intranet_opendb();

$libsms = new libsmsv2();
$lhomework = new libhomework2007();
$luser = new libuser();
$leClassApp = new libeClassApp();

if(!$plugin['eHomework']){
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

else{
// 	if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || !($_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"] || $_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] && !$lhomework->isViewerGroupMember($UserID)))
    if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || (!$_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"] && !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] && !$lhomework->isViewerGroupMember($UserID)))
    {
		if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])
		{
			header("location: ../../settings/index.php");	
			exit;
		}
		include_once($PATH_WRT_ROOT."includes/libaccessright.php");
		$laccessright = new libaccessright();
		$laccessright->NO_ACCESS_RIGHT_REDIRECT();
		exit;
	}
}


if(!isset($status) || $status=="" || $status != "-1")
{
	die("This function is to inform parents for their children's records of not handing homework only!");
}
//$statusAry = $lhomework->Get_Handin_Status($returnAssociateArray=1);



# Select Data from DB
$order = 1;  # must order by student in order to group different subjects
$row = $lhomework->getNotSubmitListPrint_Format2($yearID, $yearTermID, $subjectID, $targetID, $targetList, $studentList, $startDate, $endDate, $dateChoice, $times, $range, $order, $group, $DueDateEqualToToday,$status);
$studentIdAry = Get_Array_By_Key($row, 'UserID');


$previous_user_id = 0;
$SubjectsInvolved = array();

if ($yearID!="" && $yearTermID!="")
{
	$Name = $lhomework->getAcademicYearNameAndYearTermName($yearID, $yearTermID);
	list($yearName, $yearTermName) = $Name[0];
}

$namefield = ($intranet_default_lang =="b5" || $intranet_default_lang == "gb") ? 'ChineseName' : 'EnglishName';
$parentAppFailArr = array();
$nonParentAppParentArr = array();



# get parent app users list
if ($plugin['eClassApp']) {
	$logTable = 'APP_LOGIN_LOG';
}
else {
	$logTable = 'INTRANET_API_REQUEST_LOG';
}
$sql = "select distinct UserID from $logTable";
$parentAppUserArr = $lhomework->returnArray($sql);
$parentAppUserIDArr = array();
foreach ((array)$parentAppUserArr as $parentAppUserInfo) {
	$parentAppUserIDArr[] = $parentAppUserInfo['UserID'];
}

### get parent student mapping
$sql = "SELECT 
				ip.ParentID, ip.StudentID
		from 
				INTRANET_PARENTRELATION AS ip 
				Inner Join INTRANET_USER AS iu ON (ip.ParentID=iu.UserID) 
		WHERE 
				ip.StudentID IN ('".implode("','", (array)$studentIdAry)."') 
				AND iu.RecordStatus = 1";
$relationshipAry = $lhomework->returnResultSet($sql);
$studentParentAssoAry = BuildMultiKeyAssoc($relationshipAry, 'StudentID', array('ParentID'), $SingleValue=1, $BuildNumericArray=1);
$parentStudentAssoAry = BuildMultiKeyAssoc($relationshipAry, 'ParentID', array('StudentID'), $SingleValue=1, $BuildNumericArray=1);
unset($relationshipAry);


$nonParentAppParentIdArr = array();
$messageParentStudentAssoAry = array();
foreach ((array)$parentStudentAssoAry as $_parentId => $_studentIdAry) {
	$_numOfStudent = count((array)$_studentIdAry);
	
	if (in_array($_parentId, (array)$parentAppUserIDArr)) {
		// build push message parent student asso array
		$messageParentStudentAssoAry[$_parentId] = $_studentIdAry;
	}
	else {
		if (!in_array($_parentId, (array)$nonParentAppParentIdArr)) {
			// send on sms to parent even if there are more than 1 student
			$nonParentAppParentArr[] = array('parentID'=>$_parentId, 'studentID'=>$_studentIdAry[0]);
			$nonParentAppParentIdArr[] = $_parentId;
		}
	}
}



### build push message info array
$messageInfoAry = array();
$pushMsgCount = 0;
$MessageTitleAssoAry = array();
$MessageCententAssoAry = array();
for($i=0; $i<sizeof($row); $i++)
{
	list($ClassName, $ClassNumber, $Subject, $SubjectGroup, $StudentName, $Topic, $Desciprtion, $StartDate, $DueDate, $CountNumber, $StudentID) = $row[$i];
	//if (($previous_user_id!=$StudentID && $i>0) || $i==sizeof($row)-1)
	if ($StudentID!=$row[$i+1]["UserID"])
	{			
		$SubjectsInvolved[] = "(".$Subject.") ". $Topic;
		# send the message
		$SubjectsText = implode(", ", $SubjectsInvolved);
		//debug($ClassName, $ClassNumber, $Subject, $SubjectGroup, $StudentName, $SubjectsText, $StudentID);
		$StudentNameApps = $StudentName;
		if ($ClassName!="" && $ClassNumber!="") {
			$StudentNameApps .= " ({$ClassName}-{$ClassNumber})";
		}
		
		if ($yearName!="" || $yearTermName!="") {
			$DateInvolved = $Lang['AppNotifyMessage']['Homework']['In'] . $yearName . " " . $yearTermName;
			
			$MessageCentent[$StudentID] = $Lang['AppNotifyMessage']['Homework']['Alert']['app']['single_day'];
			$MessageCentent[$StudentID] = str_replace("[StartDate]", $DateInvolved, $MessageCentent[$StudentID]);
		} 
		else if ($startDate==$endDate) {
			$DateInvolved = $Lang['AppNotifyMessage']['Homework']['On']. $startDate;
			
			$MessageCentent[$StudentID] = $Lang['AppNotifyMessage']['Homework']['Alert']['app']['single_day'];
			$MessageCentent[$StudentID] = str_replace("[StartDate]", $startDate, $MessageCentent[$StudentID]);
		} 
		else {
			$DateInvolved = $Lang['AppNotifyMessage']['Homework']['From'] . $startDate. $Lang['AppNotifyMessage']['Homework']['To'] .$endDate. $Lang['AppNotifyMessage']['Homework']['Period'];
			
			$MessageCentent[$StudentID] = $Lang['AppNotifyMessage']['Homework']['Alert']['app']['period'];
			$MessageCentent[$StudentID] = str_replace("[StartDate]", $startDate, $MessageCentent[$StudentID]);
			$MessageCentent[$StudentID] = str_replace("[EndDate]", $endDate, $MessageCentent[$StudentID]);
		}
		
		$MessageCentent[$StudentID] = str_replace("[Subjects]", $SubjectsText, $MessageCentent[$StudentID]);
		$MessageCentent[$StudentID] = str_replace("[StudentName]", $StudentNameApps, $MessageCentent[$StudentID]);
		
		$MessageTitle[$StudentID] = $Lang['AppNotifyMessage']['Homework']['Title'];
		
		$SMSMessageContent[$StudentID] = str_replace("[DateInvolved]", $DateInvolved, str_replace("[Subjects]", $SubjectsText, str_replace("[StudentName]", $StudentNameApps, $Lang['AppNotifyMessage']['Homework']['Alert'])));
		
		
		$_parentIdAry = $studentParentAssoAry[$StudentID];
		$_numOfParent = count($_parentIdAry);
		if ($_numOfParent > 0) {
			for ($j=0; $j<$_numOfParent; $j++) {
				$__parentId = $_parentIdAry[$j];
				$messageInfoAry[$pushMsgCount]['relatedUserIdAssoAry'][$__parentId] = array($StudentID);
			}
			
			$messageInfoAry[$pushMsgCount]['messageTitle'] = $MessageTitle[$StudentID];
			$messageInfoAry[$pushMsgCount]['messageContent'] = $MessageCentent[$StudentID];
			$pushMsgCount++;
		}
		
		$SubjectsInvolved = array();
	} else
	{
		//debug($i, $previous_user_id, $StudentID );
		//debug_r( $row[$i]);
		$SubjectsInvolved[] = "(".$Subject.") ". $Topic;
	}
	//$previous_user_id = $StudentID;
}


$notifyMessageId = $leClassApp->sendPushMessage($messageInfoAry, $MessageTitle, 'MULTIPLE MESSAGES');
$statisticsAry = $leClassApp->getPushMessageStatistics($notifyMessageId);
$statisticsAry = $statisticsAry[$notifyMessageId];
$failedParentAssoAry = $statisticsAry['sendFailAssoAry'];
foreach ((array)$failedParentAssoAry as $_parentId => $_studentIdAry) {
	// send on sms to parent even if there are more than 1 student
	$parentAppFailArr[] = array('parentID'=>$_parentId, 'studentID'=>$_studentIdAry[0]);
}



########### Send SMS to non Parent App users OR send SMS to all users when Parent App fail ###########
$smsParentArr = array_merge((array)$nonParentAppParentArr, (array)$parentAppFailArr);
$numOfSmsParent = count($smsParentArr);
$recipientData = array();
for ($i=0; $i<$numOfSmsParent; $i++) {
	$_parentId = $smsParentArr[$i]['parentID'];
	$_studentId = $smsParentArr[$i]['studentID'];
	
	$sql = "
				SELECT 
					UserID, 
					TRIM(MobileTelNo) as Mobile,
					$namefield as UserName
				FROM 
					INTRANET_USER 
				WHERE 
					UserID = $_parentId
				ORDER BY 
					ClassName,
					ClassNumber,
					EnglishName
			";
		$users = $libsms->returnArray($sql, 3);
		
		// list($userid,$mobile) = $users[$i];
		$ParentID = $users[0]['UserID'];
		$mobile = $users[0]['Mobile'];
		$ParentName = $users[0]['UserName'];
		
		// debug_pr($ParentID);
		if ($ClassNumber==0)
		{
			$ClassNumber = "";
		}
		$mobile = $libsms->parsePhoneNumber($mobile);
		
		if($libsms->isValidPhoneNumber($mobile)){
		// debug_pr($mobile);
			$recipientData[] = array($mobile,$StudentID,addslashes($ParentName),$SMSMessageContent[$_studentId]);
			$valid_list[] = array($ParentID,$ParentName,$mobile,$StudentName,$ClassName,$ClassNumber);
		}else{
			$reason =$i_SMS_Error_NovalidPhone;
			$error_list[] = array($ParentID,$ParentName,$mobile,$StudentName,$ClassName,$ClassNumber,$reason);
		}
}



# send sms
if(sizeof($recipientData)>0){
	$targetType = 3;
	$picType = 2;
	$adminPIC =$PHP_AUTH_USER;
	$userPIC = $UserID;
	$frModule= "";
	$deliveryTime = $time_send;
	$isIndividualMessage=true;
	$sms_message = ""; #### Use original text - not htmlspecialchars processed
	$returnCode = $libsms->sendSMS($sms_message,$recipientData, $targetType, $picType, $adminPIC, $userPIC,"",$deliveryTime,$isIndividualMessage);
	$isSent = 1;
	$smsTotal = sizeof($recipientData);
}



intranet_closedb();

// if ($SentTotal>0 && $SentTotal!="")
// {
	// echo "<font color='#DD5555'>".str_replace("[SentTotal]", $SentTotal, $Lang['AppNotifyMessage']['Homework']['send_result']). "</font>";
// } else
// {
	// echo "<font color='red'>".$Lang['AppNotifyMessage']['Homework']['send_failed']."</font>";
// }

//if (($SentTotal>0 && $SentTotal!="") || ($isSent && ($returnCode > 0 && $returnCode==true)))
//{
//	if ($SentTotal == "") {
//		$SentTotal = 0;
//	}
//	if ($smsTotal == "") {
//		$smsTotal = 0;
//	}
//	
//	$returnMsg = str_replace("[smsTotal]", $smsTotal, str_replace("[SentTotal]", $SentTotal, $Lang['AppNotifyMessage']['Homework']['PushMessageThenSMS_send_result']));
//	echo "<font color='#DD5555'>".$returnMsg. "</font>";
//
//} else {
//	echo "<font color='red'>".$Lang['AppNotifyMessage']['Homework']['SMS_send_failed']."</font>";
//}


if (($statisticsAry['numOfSendSuccess']>0 && $statisticsAry['numOfSendSuccess']!="") || ($isSent && $returnCode==true))
{
	if ($statisticsAry['numOfSendSuccess'] == "") {
		$statisticsAry['numOfSendSuccess'] = 0;
	}
	if ($smsTotal == "") {
		$smsTotal = 0;
	}
	
	$returnMsg = str_replace("[smsTotal]", $smsTotal, str_replace("[SentTotal]", $statisticsAry['numOfSendSuccess'], $Lang['AppNotifyMessage']['Homework']['PushMessageThenSMS_send_result']));
	echo "<font color='#DD5555'>".$returnMsg. "</font>";

} else {
	echo "<font color='red'>".$Lang['AppNotifyMessage']['Homework']['SMS_send_failed']."</font>";
}
	
?>