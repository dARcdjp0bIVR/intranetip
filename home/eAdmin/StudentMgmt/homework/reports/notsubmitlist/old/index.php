<?php

// Modifing by
		
$PATH_WRT_ROOT = "../../../../../../";
$CurrentPageArr['eAdminHomework'] = 1;

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

if(!$plugin['eHomework']){
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

else{
	if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"] || !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"]){
		if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])
		{
			header("location: ../../settings/index.php");	
			exit;
		}
		include_once($PATH_WRT_ROOT."includes/libaccessright.php");
		$laccessright = new libaccessright();
		$laccessright->NO_ACCESS_RIGHT_REDIRECT();
		exit;
	}
}

# Temp Assign memory of this page
ini_set("memory_limit", "150M");

$linterface = new interface_html();
$lhomework = new libhomework2007();

$CurrentPage = "Reports_NotSubmitList";
$MODULE_OBJ = $lhomework->GET_MODULE_OBJ_ARR();

$PAGE_TITLE = $Lang['SysMgr']['Homework']['NotSubmitList'];

# tag information
$TAGS_OBJ[] = array($Lang['SysMgr']['Homework']['NotSubmitList'], "");

$today = date('Y-m-d');
$yearID = Get_Current_Academic_Year_ID();
# Current Year Term
$currentYearTerm = getAcademicYearAndYearTermByDate($today);
$yearTermID = $currentYearTerm[0];
if($yearTermID=="")
	$yearTermID = 0;
	
# Subject Menu
$subject = $lhomework->getTeachingSubjectList($UserID, $yearID, $yearTermID);
$subjectID = ($subjectID=="")? $subject[0][0]:$subjectID;
$display = (sizeof($subject)==0)? false:true;

if($display){
	$selectedSubject = $lhomework->getCurrentTeachingSubjects("name=\"subjectID\" onChange=\"this.form.method='get'; this.form.action=''; this.form.submit();\"", $subject, $subjectID, "", false);
	if($subjectID!=""){
		$subjectGroups = $lhomework->getTeachingSubjectGroupList($UserID, $subjectID, $yearID, $yearTermID);
		$selectedSubjectGroup = getSelectByArray($subjectGroups, "name=subjectGroupID onChange=\"this.form.method='get'; this.form.action=''; this.form.submit();\"", $subjectGroupID, "", 0, $Lang['SysMgr']['Homework']['AllSubjectGroups']);
		
		if($sid!="" && $sid != $subjectID){
			$subjectGroupID="";
		}
	}

	$y = $lhomework->notSubmitList($subjectID, $subjectGroupID, $yearID, $yearTermID);
	$filterbar = "$selectedSubject $selectedSubjectGroup";

	# Print link
	$printPreviewLink = "print_preview.php?yearID=$yearID&yearTermID=$yearTermID&subjectID=$subjectID&subjectGroupID=$subjectGroupID";
	# Export link
	$exportLink = "export.php?yearID=$yearID&yearTermID=$yearTermID&subjectID=$subjectID&subjectGroupID=$subjectGroupID";

	# Toolbar: print, export
	$toolbar = $linterface->GET_LNK_PRINT("javascript:newWindow('$printPreviewLink', 23)", "", "", "", "", 0 )."&nbsp;&nbsp;&nbsp;&nbsp;";

	$allowExport = $lhomework->exportAllowed;
	if($allowExport)
		$toolbar .= $linterface->GET_LNK_EXPORT($exportLink, "", "", "", "", 0 );
}

# Start layout
$linterface->LAYOUT_START();
?>
<script language="javascript">
	function viewNotHandinDetail(subjectGroupID, userID, yearID, yearTermID){
       newWindow('./view.php?subjectGroupID='+subjectGroupID+'&studentID='+userID+'&yearID='+yearID+'&yearTermID='+yearTermID,1);
	}
</script>

<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
			<form name="form2" method="post" onSubmit="return false">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td>
							<table border="0" cellspacing="0" cellpadding="2">
								<tr>
									<td><?=$toolbar?></td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</form>
			<form name="form1" method="post">
				<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
					<tr>
						<td align="right">
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td height="28" align="right" valign="bottom">
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td height="30">
													<?=$filterbar?>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
				<table width="100%" border="0" cellpadding="5" cellspacing="0">
					<tr>
						<td>
							<?
							if($display){
								echo $y;
							}
							else{
								echo $Lang['SysMgr']['Homework']['NotSubmitListWarning'];
							}
							?>
						</td>
					</tr>
				</table>
				<?if($display){?>
					<table width="100%" border="0" cellpadding="5" cellspacing="0">
						<tr><td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td></tr>
					</table>
				<?}?>
				<input type="hidden" name="sid" value="<?=$subjectID?>"/>
			</form>
		</td>
	</tr>
</table>
<?
	$linterface->LAYOUT_STOP();
	intranet_closedb();
?>