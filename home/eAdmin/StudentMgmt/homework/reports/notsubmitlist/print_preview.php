<?php
// Modifing by :

##### Change Log [Start] #####
#
#   Date    :   2019-09-06 (Tommy)
#               change access permission checking
#
###### Change Log [End] ######	

	session_start();
	$targetList = $_SESSION["targetList"];
	$studentList = $_SESSION["studentList"];
	
	$PATH_WRT_ROOT = "../../../../../../";
	$CurrentPageArr['eAdminHomework'] = 1;

	include_once($PATH_WRT_ROOT."includes/global.php");
	include_once($PATH_WRT_ROOT."includes/libdb.php");
	include_once($PATH_WRT_ROOT."includes/libhomework.php");
	include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
	include_once($PATH_WRT_ROOT."includes/libinterface.php");
	include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
	include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");

	intranet_auth();
	intranet_opendb();

	$lhomework = new libhomework2007();

	if(!$plugin['eHomework']){
		include_once($PATH_WRT_ROOT."includes/libaccessright.php");
		$laccessright = new libaccessright();
		$laccessright->NO_ACCESS_RIGHT_REDIRECT();
		exit;
	}

	else{
// 		if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || !($_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"] || $_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] && !$lhomework->isViewerGroupMember($UserID)))
        if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || (!$_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"] && !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] && !$lhomework->isViewerGroupMember($UserID)))
        {
			if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])
			{
				header("location: ../../settings/index.php");	
				exit;
			}
			include_once($PATH_WRT_ROOT."includes/libaccessright.php");
			$laccessright = new libaccessright();
			$laccessright->NO_ACCESS_RIGHT_REDIRECT();
			exit;
		}
	}

	$linterface = new interface_html();
	
	if(!isset($status) || $status=="") $status = "-1";

	$statusAry = $lhomework->Get_Handin_Status($returnAssociateArray=1);

	$Name = $lhomework->getAcademicYearNameAndYearTermName($yearID, $yearTermID);
	list($yearName, $yearTermName) = $Name[0];
	if($yearTermID=="") $yearTermName = $Lang['SysMgr']['Homework']['AllYearTerms'];

	$data = $lhomework->getNotSubmitListPrint($yearID, $yearTermID, $subjectID, $targetID, $targetList, $studentList, $startDate, $endDate, $dateChoice, $times, $range, $order, $group, $DueDateEqualToToday, $status);

	$x .= "<tr>";
	$x .= "<td valign='middle' class='eHomeworktdborder eHomeworkprinttabletitle'>#</td>";
	$x .= "<td valign='middle' class='eHomeworktdborder eHomeworkprinttabletitle'>".$Lang['SysMgr']['Homework']['Class']."</td>";
	if($group==1)
		$x .= "<td valign='middle' class='eHomeworktdborder eHomeworkprinttabletitle'>".$Lang['SysMgr']['Homework']['ClassNumber']."</td>";
	if($group==1)
		$x .= "<td valign='middle' class='eHomeworktdborder eHomeworkprinttabletitle'>".$Lang['SysMgr']['Homework']['StudentName']."</td>";
	$x .= "<td valign='middle' class='eHomeworktdborder eHomeworkprinttabletitle'>".$Lang['SysMgr']['Homework']['Subject']."</td>";
	$x .= "<td valign='middle' class='eHomeworktdborder eHomeworkprinttabletitle'>".$Lang['SysMgr']['Homework']['SubjectGroup']."</td>";
	$x .= "<td valign='middle' class='eHomeworktdborder eHomeworkprinttabletitle'>".$statusAry[$status]." ".$Lang['eHomework']['Count']."</td>";
	$x .= "</tr>";
	if(sizeof($data)!=0){
		for($i=0; $i<sizeof($data); $i++){
			list($className, $calssNumber, $subject, $subjectGroup, $studentName, $recordCount) = $data[$i];
			$x .= "<tr>";
			$x .= "<td class='eHomeworktdborder eHomeworkprinttext'>".($i+1)."</td>";
			$x .= "<td class='eHomeworktdborder eHomeworkprinttext'>".$className."</td>";
			if($group==1)
				$x .= "<td class='eHomeworktdborder eHomeworkprinttext'>".$calssNumber."</td>";
			if($group==1)
				$x .= "<td class='eHomeworktdborder eHomeworkprinttext'>".$studentName."</td>";
			$x .= "<td class='eHomeworktdborder eHomeworkprinttext'>".$subject."</td>";
			$x .= "<td class='eHomeworktdborder eHomeworkprinttext'>".$subjectGroup."</td>";
			$x .= "<td class='eHomeworktdborder eHomeworkprinttext'>".$recordCount."</td>";
			$x .= "</tr>";
		}
	}
	
	else{
		$x .="<tr><td colspan=\"7\" align=\"center\">".$Lang['SysMgr']['Homework']['NoRecord']."</td></tr>";
	}
?>

	<table width='100%' align='center' class='print_hide' border=0>
		<tr>
			<td align='right'><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></td>
		</tr>
	</table>

	<!-- Homework Title -->
	<table width='100%' align="center" border="0">
		<tr>
			<td align="center" ><b><?=$Lang['eHomework']['HandinStatusReport']?></b></td>
		</tr>
	</table>        

	<br>
	<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<? if($dateChoice=='YEAR') { ?>
		<tr><td align="left" valign="top" width="100"><?=$Lang['SysMgr']['Homework']['AcademicYear']?> : </td><td align="left" valign="top"><?=$yearName?></td></tr>
		<tr><td align="left" valign="top" width="100"><?=$Lang['SysMgr']['Homework']['YearTerm']?> : </td><td align="left" valign="top"><?=$yearTermName?></td></tr>
	<? } else { ?>
		<tr>
			<td class="tabletext" width="30%" valign="top"><?=$iDiscipline['Period']?></td>
			<td width="70%" align="left" valign="top">
				<?=$startDate." ".$i_To." ".$endDate?>
			</td>
		</tr>
	<? } ?>
	</table>
	<br>

	<table class="eHomeworktableborder" width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
		<?=$x?>
	</table>
<?
	include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_footer.php");
	intranet_closedb();
?>