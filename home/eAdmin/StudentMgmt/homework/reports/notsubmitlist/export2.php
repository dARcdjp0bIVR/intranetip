<?php
// Modifing by :

##### Change Log [Start] #####
#
#   Date    :   2019-09-06 (Tommy)
#               change access permission checking
#
###### Change Log [End] ######	

	session_start();
	$targetList = $_SESSION["targetList"];
	$studentList = $_SESSION["studentList"];
	
	$PATH_WRT_ROOT = "../../../../../../";
	$CurrentPageArr['eAdminHomework'] = 1;

	include_once($PATH_WRT_ROOT."includes/global.php");
	include_once($PATH_WRT_ROOT."includes/libdb.php");
	include_once($PATH_WRT_ROOT."includes/libexporttext.php");
	include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
	include_once($PATH_WRT_ROOT."includes/libhomework.php");
	include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
	include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

	intranet_auth();
	intranet_opendb();
	
	$lhomework = new libhomework2007();

	if(!$plugin['eHomework']){
		include_once($PATH_WRT_ROOT."includes/libaccessright.php");
		$laccessright = new libaccessright();
		$laccessright->NO_ACCESS_RIGHT_REDIRECT();
		exit;
	}

	else{
// 		if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || !($_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"] || $_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] && !$lhomework->isViewerGroupMember($UserID)))
        if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || (!$_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"] && !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] && !$lhomework->isViewerGroupMember($UserID)))
        {
			if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])
			{
				header("location: ../../settings/index.php");	
				exit;
			}
			include_once($PATH_WRT_ROOT."includes/libaccessright.php");
			$laccessright = new libaccessright();
			$laccessright->NO_ACCESS_RIGHT_REDIRECT();
			exit;
		}
	}

	
	$lexport = new libexporttext();

	if(!isset($status) || $status=="") $status = "-1";
	//$statusAry = $lhomework->Get_Handin_Status($returnAssociateArray=1);
	
	$ExportArr = array();
		
	$filename = "HandinStatusList.csv";
	
		# Select Data from DB
	$row = $lhomework->getNotSubmitListPrint_Format2($yearID, $yearTermID, $subjectID, $targetID, $targetList, $studentList, $startDate, $endDate, $dateChoice, $times, $range, $order, $group, $DueDateEqualToToday,$status);

	// Create data array for export
	for($i=0; $i<sizeof($row); $i++){
		$m = 0;
		$ExportArr[$i][$m] = $row[$i][0];		//Class
		$m++;
		if($group==1) {
			$ExportArr[$i][$m] = $row[$i][1];	//Class Number
			$m++;
			$ExportArr[$i][$m] = $row[$i][4];	//Student Name
			$m++;			
		}
		$ExportArr[$i][$m] = $row[$i][2];		//Subject
		$m++;
		$ExportArr[$i][$m] = $row[$i][3];		//Subject Group
		$m++;

		$ExportArr[$i][$m] = $row[$i][5]; $m++;		//Topic
		$ExportArr[$i][$m] = $row[$i][6]; $m++;		//Description
		$ExportArr[$i][$m] = $row[$i][7]; $m++;		//Start Date
		$ExportArr[$i][$m] = $row[$i][8]; $m++;		//Due Date
	}

	if(sizeof($row)==0) {
		$ExportArr[0][0] = $i_no_record_exists_msg;	
	}
/*
	$exportColumn[] = $Lang['SysMgr']['Homework']['Class'];
	if($group==1) {
		$exportColumn[] = $Lang['SysMgr']['Homework']['ClassNumber'];
		$exportColumn[] = $Lang['SysMgr']['Homework']['StudentName'];
	}
	$exportColumn[] = $Lang['SysMgr']['Homework']['Subject'];
	$exportColumn[] = $Lang['SysMgr']['Homework']['SubjectGroup'];
	
	//$exportColumn[] = $Lang['SysMgr']['Homework']['NotSubmitCount'];
	$exportColumn[] = "Topic";
	$exportColumn[] = "Description";
	$exportColumn[] = "Start Date";
	$exportColumn[] = "Due Date";
*/
# define column title (2 dimensions array, 1st row is english, 2nd row is chinese)
$exportColumn[0] = array($Lang['eHomework']['ExportFormat2_EN'][0]);
if($group==1) {
	$exportColumn[0] = array_merge($exportColumn[0],array($Lang['eHomework']['ExportFormat2_EN'][1], $Lang['eHomework']['ExportFormat2_EN'][2]));
}
$exportColumn[0] = array_merge($exportColumn[0],array($Lang['eHomework']['ExportFormat2_EN'][3], $Lang['eHomework']['ExportFormat2_EN'][4], $Lang['eHomework']['ExportFormat2_EN'][5], $Lang['eHomework']['ExportFormat2_EN'][6], $Lang['eHomework']['ExportFormat2_EN'][7], $Lang['eHomework']['ExportFormat2_EN'][8]));


$exportColumn[1] = array($Lang['eHomework']['ExportFormat2_B5'][0]);
if($group==1) {
	$exportColumn[1] = array_merge($exportColumn[1], array($Lang['eHomework']['ExportFormat2_B5'][1], $Lang['eHomework']['ExportFormat2_B5'][2]));
} 
$exportColumn[1] = array_merge($exportColumn[1], array($Lang['eHomework']['ExportFormat2_B5'][3], $Lang['eHomework']['ExportFormat2_B5'][4], $Lang['eHomework']['ExportFormat2_B5'][5], $Lang['eHomework']['ExportFormat2_B5'][6], $Lang['eHomework']['ExportFormat2_B5'][7], $Lang['eHomework']['ExportFormat2_B5'][8]));

	$export_content = $lexport->GET_EXPORT_TXT_WITH_REFERENCE($ExportArr, $exportColumn, "", "\r\n", "", 0, "00");

	intranet_closedb();

	// Output the file to user browser
	$lexport->EXPORT_FILE($filename, $export_content);
?>