<?php
// Modifing by :
/*
 *
 *  2020-10-09 Cameron
 *      - create this file
 */

@SET_TIME_LIMIT(216000);
@ini_set('memory_limit', -1);
ini_set('zend.ze1_compatibility_mode', '0');        // php version <= 5.2 require this
date_default_timezone_set('Asia/Hong_Kong');

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
include_once($PATH_WRT_ROOT."includes/phpxls/Classes/PHPExcel/IOFactory.php");
include_once($PATH_WRT_ROOT."includes/phpxls/Classes/PHPExcel.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$lhomework = new libhomework2007();
if(!$plugin['eHomework']){
    include_once($PATH_WRT_ROOT."includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit;
} elseif($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || (!$sys_custom['eHomework']['CustomizedPrintAndExport']) || (!$_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] && !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"] && !$lhomework->isViewerGroupMember($UserID))) {
    if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])
    {
        header("location: ../../settings/index.php");
        exit;
    }
    include_once($PATH_WRT_ROOT."includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit;
}

## Step 1: get filters
$targetList = IntegerSafe($_POST['targetList']);
$studentList = IntegerSafe($_POST['studentList']);
$times = IntegerSafe($_POST['times']);
$range = IntegerSafe($_POST['range']);
$startDate = $_POST['startDate'];
$endDate = $_POST['endDate'];
$subjectID = IntegerSafe($_POST['subjectID']);
$targetID = IntegerSafe($_POST['targetID']);
$status = IntegerSafe($_POST['status']);
$exportType = IntegerSafe($_POST['exportType']);

$academicYearTermInfo = getAcademicYearAndYearTermByDate($startDate);
$yearID = $academicYearTermInfo['AcademicYearID'];
$yearTermID = $academicYearTermInfo['YearTermID'];

# if YearTermID of start date & end date are different, then homework should be included both semesters
$academicYearTermInfo2 = getAcademicYearAndYearTermByDate($endDate);
$endDateYearTermID = $academicYearTermInfo2['YearTermID'];
if($endDateYearTermID != $yearTermID) {
    $yearTermID = "";
}

$displayStatus = '';
$statusAry = $lhomework->Get_Handin_Status();
foreach($statusAry as $_statusAry) {
    if ($_statusAry[0] == $status) {
        $displayStatus = $_statusAry[1];
        break;
    }
}


## Step 2: get result
$data = $lhomework->getCustomizedStatusReport($yearID, $yearTermID, $subjectID, $targetID, $targetList, $studentList, $startDate, $endDate, $times, $range, $status, $exportType);

$file_name = 'customized_handin_status_report.xls';
$objPHPExcel = new PHPExcel();    // Create new PHPExcel object
// Set properties
$objPHPExcel->getProperties()->setCreator("eClass")
    ->setLastModifiedBy("eClass")
    ->setTitle($Lang['eHomework']['HandinStatusReport'])
    ->setSubject('Customized Handin Status Report')
    ->setDescription('')
    ->setKeywords("eHomework")
    ->setCategory("eHomework");

// Set default font
$objPHPExcel->getDefaultStyle()->getFont()->setName('新細明體');
$objPHPExcel->getDefaultStyle()->getFont()->setSize(12);
$sheetIndex = 0;        // one form/class per sheet

if (count($data['data'])) {

    if ($exportType == 1) {      // by form
        $formAry = $data['form'];
        $homeworkAry = $data['data'];
        foreach((array) $formAry as $formID=>$formName) {
            // Create sheet
            if ($sheetIndex > 0) {
                $objPHPExcel->createSheet();
            }

            // Set active sheet
            $objPHPExcel->setActiveSheetIndex($sheetIndex);
            $activeSheet = $objPHPExcel->getActiveSheet();
            $activeSheet->setTitle($formName);

            $j = 1;     // count number of record for each form
            $rowIndex = 5;      // starting row for details

            // filter
            $activeSheet->setCellValue('A1', "{$Lang['Header']['Menu']['Period']} : {$startDate} {$Lang['General']['To']} {$endDate}");
            $activeSheet->setCellValue('A2', "{$Lang['eHomework']['HandinStatus']} : {$displayStatus}");

            // column header
            $activeSheet->setCellValue('A4','#');
            $activeSheet->setCellValue('B4',$Lang['SysMgr']['Homework']['Class']);
            $activeSheet->setCellValue('C4',$Lang['SysMgr']['Homework']['ClassNumber']);
            $activeSheet->setCellValue('D4',$Lang['SysMgr']['Homework']['StudentName']);
            $activeSheet->setCellValue('E4',$Lang['SysMgr']['Homework']['Subject']);
            $activeSheet->setCellValue('F4',$Lang['SysMgr']['Homework']['SubjectGroup']);
            $activeSheet->setCellValue('G4',$Lang['SysMgr']['Homework']['Title']);
            $activeSheet->setCellValue('H4',$Lang['SysMgr']['Homework']['Content']);
            $activeSheet->setCellValue('I4',$Lang['SysMgr']['Homework']['DueDate']);

            // details
            foreach((array)$homeworkAry[$formID] as $yearClassID=>$_homeworkAry) {
                foreach((array)$_homeworkAry as $handinRecordID=>$__homeworkAry) {
                    $activeSheet->setCellValue('A'.$rowIndex,$j);
                    $activeSheet->setCellValue('B'.$rowIndex,$__homeworkAry['ClassName']);
                    $activeSheet->setCellValue('C'.$rowIndex,$__homeworkAry['ClassNumber']);
                    $activeSheet->setCellValue('D'.$rowIndex,$__homeworkAry['StudentName']);
                    $activeSheet->setCellValue('E'.$rowIndex,$__homeworkAry['Subject']);
                    $activeSheet->setCellValue('F'.$rowIndex,$__homeworkAry['SubjectGroup']);
                    $activeSheet->setCellValue('G'.$rowIndex,($__homeworkAry['Title'] ? $__homeworkAry['Title'] : '--'));
                    $activeSheet->setCellValue('H'.$rowIndex,($__homeworkAry['Description'] ? $__homeworkAry['Description'] : '--'));
                    $activeSheet->setCellValue('I'.$rowIndex,$__homeworkAry['DueDate']);
                    $j++;
                    $rowIndex++;
                }
            }

            $sheetIndex++;
        }
        $objPHPExcel->setActiveSheetIndex(0);       // reset default to first sheet
    }


    else {      // by class
        $classAry = $data['class'];
        $homeworkAry = $data['data'];
        foreach((array) $classAry as $yearClassID=>$className) {
            // Create sheet
            if ($sheetIndex > 0) {
                $objPHPExcel->createSheet();
            }

            // Set active sheet
            $objPHPExcel->setActiveSheetIndex($sheetIndex);
            $activeSheet = $objPHPExcel->getActiveSheet();
            $activeSheet->setTitle($className);

            $j = 1;     // count number of record for each class
            $rowIndex = 5;      // starting row for details

            // filter
            $activeSheet->setCellValue('A1', "{$Lang['Header']['Menu']['Period']} : {$startDate} {$Lang['General']['To']} {$endDate}");
            $activeSheet->setCellValue('A2', "{$Lang['eHomework']['HandinStatus']} : {$displayStatus}");

            // column header
            $activeSheet->setCellValue('A4','#');
            $activeSheet->setCellValue('B4',$Lang['SysMgr']['Homework']['Class']);
            $activeSheet->setCellValue('C4',$Lang['SysMgr']['Homework']['ClassNumber']);
            $activeSheet->setCellValue('D4',$Lang['SysMgr']['Homework']['StudentName']);
            $activeSheet->setCellValue('E4',$Lang['SysMgr']['Homework']['Subject']);
            $activeSheet->setCellValue('F4',$Lang['SysMgr']['Homework']['SubjectGroup']);
            $activeSheet->setCellValue('G4',$Lang['SysMgr']['Homework']['Title']);
            $activeSheet->setCellValue('H4',$Lang['SysMgr']['Homework']['Content']);
            $activeSheet->setCellValue('I4',$Lang['SysMgr']['Homework']['DueDate']);

            foreach((array)$homeworkAry[$yearClassID] as $handinRecordID=>$__homeworkAry) {
                $activeSheet->setCellValue('A'.$rowIndex,$j);
                $activeSheet->setCellValue('B'.$rowIndex,$__homeworkAry['ClassName']);
                $activeSheet->setCellValue('C'.$rowIndex,$__homeworkAry['ClassNumber']);
                $activeSheet->setCellValue('D'.$rowIndex,$__homeworkAry['StudentName']);
                $activeSheet->setCellValue('E'.$rowIndex,$__homeworkAry['Subject']);
                $activeSheet->setCellValue('F'.$rowIndex,$__homeworkAry['SubjectGroup']);
                $activeSheet->setCellValue('G'.$rowIndex,($__homeworkAry['Title'] ? $__homeworkAry['Title'] : '--'));
                $activeSheet->setCellValue('H'.$rowIndex,($__homeworkAry['Description'] ? $__homeworkAry['Description'] : '--'));
                $activeSheet->setCellValue('I'.$rowIndex,$__homeworkAry['DueDate']);
                $j++;
                $rowIndex++;
            }

            $sheetIndex++;
        }
        $objPHPExcel->setActiveSheetIndex(0);       // reset default to first sheet
    }
}
else {
    // Set active sheet
    $objPHPExcel->setActiveSheetIndex($sheetIndex);
    $activeSheet = $objPHPExcel->getActiveSheet();
    $activeSheet->setTitle('No Record');

    // filter
    $activeSheet->setCellValue('A1', "{$Lang['Header']['Menu']['Period']} : {$startDate} {$Lang['General']['To']} {$endDate}");
    $activeSheet->setCellValue('A2', "{$Lang['eHomework']['HandinStatus']} : {$displayStatus}");

    $activeSheet->setCellValue('A4',$Lang['SysMgr']['Homework']['NoRecord']);
}

intranet_closedb();

// output file
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'.$file_name.'"');
header('Cache-Control: max-age=0');

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

$objWriter->save('php://output');

exit;

?>