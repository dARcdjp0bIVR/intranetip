<?php
	// Modifing by : 
			
	/* Change Log [Start]
	 * Date     :   20190906 (Tommy) change access permission checking
	 * Date		:	20131206 (YatWoon)
	 * Detail	:	modified: remove "include due date" and "hide year/term" option
	 * Date		:	20100804 (Henry Chow)
	 * Detail	:	add 1 more option "Time(s)"
	 * 
	 * Change Log [End]
	*/
	$PATH_WRT_ROOT = "../../../../../../";
	$CurrentPageArr['eAdminHomework'] = 1;

	include_once($PATH_WRT_ROOT."includes/global.php");
	include_once($PATH_WRT_ROOT."includes/libdb.php");
	include_once($PATH_WRT_ROOT."includes/libinterface.php");
	include_once($PATH_WRT_ROOT."includes/libhomework.php");
	include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
	include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
	include_once($PATH_WRT_ROOT."includes/libdbtable.php");
	include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

	intranet_auth();
	intranet_opendb();
	
	$lhomework = new libhomework2007();

	if(!$plugin['eHomework']){
		include_once($PATH_WRT_ROOT."includes/libaccessright.php");
		$laccessright = new libaccessright();
		$laccessright->NO_ACCESS_RIGHT_REDIRECT();
		exit;
	}

	else{
		if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || (!$_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"] && !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] && !$lhomework->isViewerGroupMember($UserID)))
		{
			if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])
			{
				header("location: ../../settings/index.php");	
				exit;
			}
			include_once($PATH_WRT_ROOT."includes/libaccessright.php");
			$laccessright = new libaccessright();
			$laccessright->NO_ACCESS_RIGHT_REDIRECT();
			exit;
		}
	}

	# Temp Assign memory of this page
	ini_set("memory_limit", "150M");

	$linterface = new interface_html();
	

	$CurrentPage = "Reports_ParentNotice";
	$MODULE_OBJ = $lhomework->GET_MODULE_OBJ_ARR();

	$PAGE_TITLE = $Lang['SysMgr']['Homework']['ParentNotice'];

	# tag information
	$TAGS_OBJ[] = array($Lang['SysMgr']['Homework']['ParentNotice'], "");

	# Current Year and Year Term
	$currentYearTerm = $lhomework->getCurrentAcademicYearAndYearTerm();
	$yearID = ($yearID=="")? $currentYearTerm[0]['AcademicYearID']:$yearID;
	$yearTermID = ($yearTermID=="")? $currentYearTerm[0]['YearTermID']:$yearTermID;


	# Select Subject
	$subjectID = "";
	$allSubject = $lhomework->getTeachingSubjectList("", $yearID, $yearTermID);
	$selectedSubject = $lhomework->getSlectedList("name=\"subjectID\" id=\"subjectID\" onChange=\"getTarget()\"", $allSubject, $subjectID, $Lang['SysMgr']['Homework']['AllSubjects'], true);

	# Select Target
	//$target[] = array("", $Lang['SysMgr']['Homework']['PleaseSelect']);
	$target[] = array("1", $Lang['SysMgr']['Homework']['Class']);
	$target[] = array("2", $Lang['SysMgr']['Homework']['Student']);
	$target[] = array("3", $Lang['SysMgr']['Homework']['SubjectGroup']);	
	$selectedTarget = $lhomework->getSlectedList("name=\"targetID\" id=\"targetID\" onChange=\"getTargetResult(this.value)\"", $target, $targetID, "", false);

	# Target List
	$selectedTargetList = "<select name=\"targetListResult[]\" id=\"targetListResult\" size=\"5\"></select>";
	$button = $linterface->GET_BTN($button_select_all, "button", "SelectAll(document.getElementById('targetListResult')); return false;", "selectAllBtn");

	# Status Selection
	$statusAry[] = array("-1",$Lang['SysMgr']['Homework']['NotSubmitted']);
	$statusAry[] = array("2",$Lang['SysMgr']['Homework']['LateSubmitted']);
	$statusAry[] = array("-3",$Lang['SysMgr']['Homework']['NotSubmitted']." / ".$Lang['SysMgr']['Homework']['LateSubmitted']);
			
	//$statusAry = $lhomework->Get_Handin_Status();
	$statusSelection = getSelectByArray($statusAry, 'name="status" id="status"', "-1");
	
	$linterface->LAYOUT_START();
?>
<script language="javascript">
	function GetXmlHttpObject(){
		var xmlHttp = null;
		try
		{
			xmlHttp = new XMLHttpRequest();
		}
		catch (e)
		{
			try
			{
				xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
			}
			catch (e)
			{
				xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
			}
		}
		return xmlHttp;
	}
	
	// Get Target
	function getTarget(){
		targetXml = GetXmlHttpObject()
		
		if (targetXml==null)
		{
			alert ("Browser does not support HTTP Request")
			return
		} 

		var url = "";

		url = "ajaxGetTarget.php";
		
		targetXml.onreadystatechange = targetStateChanged
		targetXml.open("GET",url,true)
		targetXml.send(null)
	
	}
	
	function targetStateChanged(){ 
		if (targetXml.readyState==4 || targetXml.readyState=="complete")
		{ 
			document.getElementById("target").innerHTML = targetXml.responseText;
			targetID = document.getElementById("targetID").value;
			getTargetResult(targetID);
		} 
	}
	
	// Get Target Result
	function getTargetResult(targetID){
		<? /* ?>
		yearID = document.getElementById("yearID").value;
		yearTermID = document.getElementById("yearTermID").value;
		<? */ ?>
		yearID = <?=$yearID?>;
		yearTermID = <?=$yearTermID?>;
		subjectID = document.getElementById("subjectID").value;
		targetResultXml = GetXmlHttpObject()
		
		if (targetResultXml==null)
			{
				alert ("Browser does not support HTTP Request")
				return
			} 
			
		url = "ajaxGetTargetResult.php";
		url += "?targetID=" + targetID;
		url += "&yearID=" + yearID;
		url += "&yearTermID=" + yearTermID;
		url += "&subjectID=" + subjectID;
		targetResultXml.onreadystatechange = targetResultStateChanged 
		targetResultXml.open("GET",url,true)
		targetResultXml.send(null)
	} 

	function targetResultStateChanged(){ 
		if (targetResultXml.readyState==4 || targetResultXml.readyState=="complete")
		{ 
			document.getElementById("targetList").innerHTML = targetResultXml.responseText;
		} 
	}

	// Get Student List
	function getStudentList(yearClassID){
		subjectID = document.getElementById("subjectID").value;
		
		studentListXml = GetXmlHttpObject()
		
		if (studentListXml==null)
			{
				alert ("Browser does not support HTTP Request")
				return
			} 
			
		url = "ajaxGetStudentList.php";
		url += "?yearClassID=" + yearClassID;
		url += "&subjectID=" + subjectID;
		
		studentListXml.onreadystatechange = studentListStateChanged 
		studentListXml.open("GET",url,true)
		studentListXml.send(null)
	}
	
	function studentListStateChanged(){ 
		if (studentListXml.readyState==4 || studentListXml.readyState=="complete")
		{ 
			document.getElementById("studentList").innerHTML = studentListXml.responseText;
		} 
	}
	
	// Select All
	function SelectAll(obj){
		for (i=0; i<obj.length; i++){
			obj.options[i].selected = true;
		}
	}
	
	//Generate List
	function generateList(){
		<? /* ?>
		yearID = document.getElementById("yearID").value;
		yearTermID = document.getElementById("yearTermID").value;
		<? */ ?>
		yearID = <?=$yearID?>;
		yearTermID = <?=$yearTermID?>;
		subjectID = document.getElementById("subjectID").value;
		targetID = document.getElementById("targetID").value;
	
	
	if((!check_date(document.getElementById("startDate"),'<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>')) || (!check_date(document.getElementById("endDate"),'<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>')))
	{
		return false;
	}
	if(document.getElementById("startDate").value > document.getElementById("endDate").value) 
	{
		alert("<?=$i_Discipline_System_Reports_Invalid_Date_Compare?>");
		return false;	
	}
	
		if(targetID == ""){
			alert("<?=$i_alert_pleaseselect." ".$Lang['SysMgr']['Homework']['Target']?>");
			return false; 
		}
		
		selectedArray = new Array();
		
		if (targetID!=2){
			var targetList = $("#targetListResult").find("option:selected");
			
			var i;
			var count = 0;
		
			for (i=0; i<targetList.length; i++){		
				selectedArray[count] = targetList.val();
				count++;
			}
			
			if (selectedArray.length == 0){
				alert("<?=$i_alert_pleaseselect." ".$Lang['SysMgr']['Homework']['Target']?>");
				return false; 
			}
			document.getElementsByName("selectedArray").value = selectedArray;
		}
		
		else {
			targetList = document.getElementById("targetListResult")
			
			var studentList = $("#studentListResult").find("option:selected");
			
			var i;
			var count = 0;
			for (i=0; i<studentList.length; i++){
				selectedArray[count] = studentList.val();
				count++;
			}
			
			if (selectedArray.length == 0){
				alert("<?=$i_alert_pleaseselect." ".$Lang['SysMgr']['Homework']['Target']?>");
				return false; 
			}
			document.getElementsByName("classID").value = targetList;
			document.getElementsByName("selectedArray").value = selectedArray;
		}
		
		if($('#status').val()=='') {
			alert("<?=$i_alert_pleaseselect." ".$Lang['eHomework']['HandinStatus']?>");
			return false;	
		}
		
		document.form1.submit();
	}
	
	$().ready( function(){
	
		getTargetResult($('#targetID').val());
	});
	
</script>

<form name="form1" method="post" action="parent_notice_print.php" onSubmit="return false" target="WindowNotice">
	<table width="90%" border="0" cellpadding="5" cellspacing="5">
		<tr>
			<td>
				<table border="0" cellspacing="0" cellpadding="5">
				<? /* ?>
					<tr>
						<td class="tabletext formfieldtitle" width="30%" valign="top" rowspan="2"><?=$Lang['SysMgr']['Homework']['AcademicYear']?></td>
						<td width="567" align="left" valign="top" onClick="document.getElementById('dateChoice1').checked=true">
							<input type="radio" name="dateChoice" id="dateChoice1" value="YEAR" <? if($dateChoice=='YEAR' || $dateChoice=='') echo "checked";?>>
							<span id="year"><?=$selectedYear?></span>
							<span id="yearTerm"><?=$selectedYearTerm?></span>
						</td>
					</tr>
				<? */ ?>
					<tr>
						<td class="tabletext formfieldtitle" width="30%" valign="top"><?=$Lang['SysMgr']['Homework']['DueDate']?></td>
						<td align="left" valign="top">
							<?=$linterface->GET_DATE_PICKER("startDate",$startDate)?><?=$i_To?>
							<?=$linterface->GET_DATE_PICKER("endDate",$endDate)?>
						</td>
					</tr>
					
					<tr>
						<td class="tabletext formfieldtitle" width="30%" valign="top"><?=$Lang['SysMgr']['Homework']['Subject']?></td>
						<td width="567" align="left" valign="top">
							<span id="subject">
								<?=$selectedSubject?>
							</span>
						</td>
					</tr>
					
					<tr>
						<td class="tabletext formfieldtitle" width="30%" valign="top" rowspan="2"><?=$Lang['SysMgr']['Homework']['Target']?> <span class="tabletextrequire">*</span></td>
						<td width="567" align="left" valign="top">
							<span id="target">
								<?=$selectedTarget?>
							</span>
						</td>
					</tr>
					<tr>
						<td width="567" align="left" valign="top">
							<div id="targetList">
								<span id="list">
									<?=$selectedTargetList?>
								</span>
								<br><br>
								<span id="button">
									<?=$button?>
								</span>
							</div>
						</td>
					</tr>
					<? /* ?>
					<tr>
						<td class="tabletext formfieldtitle">&nbsp;</td>
						<td>
							<input type="checkbox" name="DueDateEqualToToday" id="DueDateEqualToToday" value="1">
			<label for="DueDateEqualToToday"><?=$Lang['eHomework']['IncludeDueDate']?></label>
						</td>
					</tr>
					<? */ ?>
					<tr>
						<td class="tabletext formfieldtitle"><?=$Lang['eHomework']['HandinStatus']?></td>
						<td><?=$statusSelection?></td>
					</tr>
					<tr>
						<td class="tabletext formfieldtitle"><?=$Lang['eHomework']['Times']?></td>
						<td>
							<input type="text" name="times" id="times" value="1" maxlength="4" size="4">
							<?=$Lang['eHomework']['OnOrAbove']?>
						</td>
					</tr>
					<!--
					<tr>
						<td class="tabletext formfieldtitle"><?=$i_general_orderby?></td>
						<td>
							<input type="radio" name="order" id="order1" value="1" checked><label for="order1"><?=$i_ClassName.'/'.$i_ClassNumber?></label>
							<input type="radio" name="order" id="order2" value="2"><label for="order2"><?=$i_Discipline_Times?></label>
						</td>
					</tr>
					-->
				</table>
			</td>
		</tr>
		
		<tr>
			<td align="left" class="tabletextremark"><?=$i_general_required_field?></td>
		</tr>
		
		<tr>
			<td class="dotline">
				<img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1">
			</td>
		</tr>
		
		<tr>
			<td align="center">
				<?=$linterface->GET_ACTION_BTN($button_submit, "button", "javascript:generateList()")?>
			</td>
		</tr>
	</table>
	<input type="hidden" name="classID" value=""/>
	<input type="hidden" name="selectedArray" value=""/>
	<input type="hidden" name="order" value="1"/>
</form>
<?
	intranet_closedb();
	$linterface->LAYOUT_STOP();
?>
 