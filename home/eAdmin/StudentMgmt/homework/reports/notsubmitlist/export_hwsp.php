<?php
// Modifing by :

##### Change Log [Start] #####
#
#   Date    :   2019-09-06 (Tommy)
#               change access permission checking
#
###### Change Log [End] ######	

	session_start();
	$targetList = $_SESSION["targetList"];
	$studentList = $_SESSION["studentList"];
	
	$PATH_WRT_ROOT = "../../../../../../";
	$CurrentPageArr['eAdminHomework'] = 1;

	include_once($PATH_WRT_ROOT."includes/global.php");
	include_once($PATH_WRT_ROOT."includes/libdb.php");
	include_once($PATH_WRT_ROOT."includes/libexporttext.php");
	include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
	include_once($PATH_WRT_ROOT."includes/libhomework.php");
	include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
	include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

	intranet_auth();
	intranet_opendb();
	
	$lhomework = new libhomework2007();

	if(!$plugin['eHomework']){
		include_once($PATH_WRT_ROOT."includes/libaccessright.php");
		$laccessright = new libaccessright();
		$laccessright->NO_ACCESS_RIGHT_REDIRECT();
		exit;
	}

	else{
		if(
			$_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"]
			|| !$sys_custom['eHomework_Status_Supplementary_WitTwoHandInStatusManage']
			|| (!$_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"]
				&& !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] && !$lhomework->isViewerGroupMember($UserID)
			)
		)
		{
			if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])
			{
				header("location: ../../settings/index.php");	
				exit;
			}
			include_once($PATH_WRT_ROOT."includes/libaccessright.php");
			$laccessright = new libaccessright();
			$laccessright->NO_ACCESS_RIGHT_REDIRECT();
			exit;
		}
	}

	
	$lexport = new libexporttext();

	if(!isset($status) || $status=="") $status = "-1";
	$statusAry = $lhomework->Get_Handin_Status($returnAssociateArray=1);
	
	$ExportArr = array();
		
	$yearID = Get_Current_Academic_Year_ID();
	$handin_param = array(
			"yearID" => $yearID,
			"dataopt" => (isset($_GET["dataopt"])) ? $_GET["dataopt"] : "duedate_range",
			"startDate" => (isset($_GET["startDate"])) ? $_GET["startDate"] : date("Y-m-d"),
			"endDate" => (isset($_GET["endDate"])) ? $_GET["endDate"] : date("Y-m-d"),
			"selectedDate" => (isset($_GET["selectedDate"])) ? $_GET["selectedDate"] : date("Y-m-d"),
			"custOrderBy" => "studentID",
			"HandInStatusID" => "-1",
			"isReport" => true,
			"callBy" => basename(__FILE__)
	);
	# Select Data from DB
		
	switch ($_GET["hwrp_type"]) {
		case "notsubmit" :
			$filename = "Homework_NotSubmit_" . $handin_param["selectedDate"] . ".csv";
			$print_title = $Lang['SysMgr']['Homework']['InputNotSubmitRecord'];
			$handin_param["HandInStatusID"] = "-1";
			break;
		case "cancel_violation":
			$handin_param["cancelOnly"] = true;
			$cancelViolation = true;
			$filename = "Homework_CancelMarkedNotSubmit_" . $handin_param["selectedDate"] . ".csv";
			$print_title = $Lang['SysMgr']['Homework']['InputCancelViolationRecord'];
		case "hw_violation":
			if (empty($filename)) {
				$filename = "Homework_MarkedNotSubmit_" . $handin_param["selectedDate"] . ".csv";
			}
			if (empty($print_title)) {
				$print_title = $Lang['SysMgr']['Homework']['InputViolationRecord'];
			}
			$handin_param["HandInStatusID"] = "-20";
			$showTotalViolation = true;
			break;
		default:
			include_once($PATH_WRT_ROOT."includes/libaccessright.php");
			$laccessright = new libaccessright();
			$laccessright->NO_ACCESS_RIGHT_REDIRECT();
			exit;
			break;
	}
	$handin_studentsArr = $lhomework->getStudentsByParam($handin_param);
	if (count($handin_studentsArr) > 0) {
		if ($showTotalViolation && !$cancelViolation) {
			foreach ($handin_studentsArr as $kk => $vv) {
				$studentIDMarkAsViolation[$vv["UserID"]] = 0;
			}
			$studentIDMarkAsViolation = $lhomework->getTotalViolationByUserIDs(array_keys($studentIDMarkAsViolation));
		}
		$i = 0;
		foreach ($handin_studentsArr as $kk => $vv) {
			$studentName = $vv["StudentName"];
			$m=0;
			if (empty($studentName)) $studentName = $vv["UserLogin"];
			
			$ExportArr[$i][$m] = $vv["ClassTitle"];
			$m++;
			$ExportArr[$i][$m] = $vv["ClassNumber"];
			$m++;
			if ($_GET["hiddenname"] != "1") {
				$ExportArr[$i][$m] = $studentName;
				$m++;
			}
			$ExportArr[$i][$m] = $vv["DueDate"];
			$m++;
			$ExportArr[$i][$m] = $vv["Subject"];
			$m++;
			$ExportArr[$i][$m] = $vv['Title'];
			$m++;
			if ($cancelViolation) {
				$ExportArr[$i][$m] = $vv["CancelViolationDate"];
				$m++;
			} else if ($showTotalViolation) {
				$ExportArr[$i][$m] = (isset($studentIDMarkAsViolation[$vv['StudentID']]) ? $studentIDMarkAsViolation[$vv['StudentID']] : 0);
				$m++;
			}
			$i++;
		}
	}
	$exportColumn = array();
	$exportColumn[] = $Lang['SysMgr']['Homework']['Class'];
	$exportColumn[] = $Lang['SysMgr']['Homework']['ClassNumber'];
	if ($_GET["hiddenname"] != "1") {
		$exportColumn[] = $Lang['SysMgr']['Homework']['StudentName'];
	}
	$exportColumn[] = $Lang['SysMgr']['Homework']['DueDateForViolation'];
	$exportColumn[] = $Lang['SysMgr']['Homework']['Subject'];
	$exportColumn[] = $Lang['SysMgr']['Homework']['Topic'];
	if ($cancelViolation) {
		$exportColumn[] = $Lang['SysMgr']['Homework']['CancelViolationDate'];
	} else if ($showTotalViolation) {
		$exportColumn[] = $Lang['SysMgr']['Homework']['TotalViolation'];
	}
	$export_content = $lexport->GET_EXPORT_TXT($ExportArr, $exportColumn, "", "\r\n", "", 0, "00");
	intranet_closedb();
	// Output the file to user browser

	$lexport->EXPORT_FILE($filename, $export_content);
?>