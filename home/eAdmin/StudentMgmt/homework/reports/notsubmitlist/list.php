<?php
// Modifying by : 
		
##### Change Log [Start] #####
#
#   Date    :   2020-10-08 (Cameron)
#               add customized print and export [case #U176404]
#
#   Date    :   2019-09-06 (Tommy)
#               change access permission checking
#
#   Date    :   2019-05-07 (Bill)
#               prevent SQL Injection + Cross-site Scripting
#
#	Date	:	2013-12-06 (YatWoon)
#				modified: remove "include due date" and "hide year/term" option
#
#	Date	:	2013-09-23 (Roy Law)
#				add send push message thickbox
#
#	Date	:	2010-03-04 (Henry Chow)
#				assign $yearID if "DATE" range option is selected
#
###### Change Log [End] ######		

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$lhomework = new libhomework2007();

if(!$plugin['eHomework']) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}
else {
// 	if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || !($_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"] || $_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] && !$lhomework->isViewerGroupMember($UserID)))
        if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || (!$_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"] && !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] && !$lhomework->isViewerGroupMember($UserID)))
        {
		if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])
		{
			header("location: ../../settings/index.php");	
			exit;
		}
		include_once($PATH_WRT_ROOT."includes/libaccessright.php");
		$laccessright = new libaccessright();
		$laccessright->NO_ACCESS_RIGHT_REDIRECT();
		exit;
	}
}

# Temp Assign memory of this page
ini_set("memory_limit", "150M");

$linterface = new interface_html();

$CurrentPageArr['eAdminHomework'] = 1;
$CurrentPage = "Reports_NotSubmitList";

$MODULE_OBJ = $lhomework->GET_MODULE_OBJ_ARR();
$PAGE_TITLE = $Lang['SysMgr']['Homework']['NotSubmitList'];

# Tag information
$TAGS_OBJ[] = array($Lang['eHomework']['HandinStatusReport'], "");

### Handle SQL Injection + XSS [START]
if(!intranet_validateDate($startDate)) {
    $startDate = date('Y-m-d');
}
if(!intranet_validateDate($endDate)) {
    $endDate = date('Y-m-d');
}

$subjectID = IntegerSafe($subjectID);
$targetID = IntegerSafe($targetID);
$targetListResult = IntegerSafe($targetListResult);
$studentListResult = IntegerSafe($studentListResult);

if(isset($status)) {
    $status = IntegerSafe($status);
}
$times = IntegerSafe($times);
$range = IntegerSafe($range);
$group = IntegerSafe($group);
$order = IntegerSafe($order);
### Handle SQL Injection + XSS [END]

$_SESSION['targetList'] = $targetListResult;
$_SESSION['studentList'] = $studentListResult;
if(!isset($status) || $status=="") $status = "-1";

# Date link
$dateChoice ='DATE';		# hard-code, date only,  no year selection
if($dateChoice=='YEAR') {
	$dateLink = "dateChoice=YEAR&yearID=$yearID&yearTermID=$yearTermID";
}
else {
	$dateLink = "dateChoice=DATE&startDate=$startDate&endDate=$endDate";
	
	//$currentYearTerm = getAcademicYearAndYearTermByDate($startDate);
	$currentYearTerm = getAcademicYearInfoAndTermInfoByDate($startDate);
	$yearID = $currentYearTerm[0];
	$yearTermID = $currentYearTerm[2];
	
	# if YearTermID of start date & end date are different, then homework should be included both semesters
	$currentYearTerm2 = getAcademicYearInfoAndTermInfoByDate($endDate);
	$endDate_yearTermID = $currentYearTerm2[2];
	if($endDate_yearTermID!=$yearTermID) {
		$yearTermID = "";
	}
}

# Print link
$printPreviewLink = "print_preview.php?times=$times&range=$range&order=$order&group=$group&$dateLink&subjectID=$subjectID&targetID=$targetID&DueDateEqualToToday=$DueDateEqualToToday&status=$status";

# Export link
$exportLink1 = "export.php?times=$times&range=$range&order=$order&group=$group&$dateLink&subjectID=$subjectID&targetID=$targetID&DueDateEqualToToday=$DueDateEqualToToday&status=$status";
$exportLink2 = "export2.php?times=$times&range=$range&order=$order&group=$group&$dateLink&subjectID=$subjectID&targetID=$targetID&DueDateEqualToToday=$DueDateEqualToToday&status=$status";

# Toolbar: print, export
$toolbar = $linterface->GET_LNK_PRINT("javascript:newWindow('$printPreviewLink', 23)", "", "", "", "", 0 );

$allowExport = $lhomework->exportAllowed;
if($allowExport)
{
	//$toolbar .= $linterface->GET_LNK_EXPORT($exportLink, "", "", "", "", 0 );
	$toolbar .= '<div class="Conntent_tool">
		<div class="btn_option">
			<a href="javascript:;" class="export" id="btn_edit" onclick="MM_showHideLayers(\'edit_option\',\'\',\'show\');">'.$button_export.'</a>
			<br style="clear:both" />
			<div class="btn_option_layer" id="edit_option" onclick="MM_showHideLayers(\'edit_option\',\'\',\'hide\');">
			<a href="'.$exportLink1.'" class="sub_btn">'.$Lang['eHomework']['ExportFormat1'].'</a>
			<a href="'.$exportLink2.'" class="sub_btn">'.$Lang['eHomework']['ExportFormat2'].'</a>
			</div>	  
		</div>
	</div>';
}

if ($sys_custom['eHomework']['CustomizedPrintAndExport']) {

    $toolbar .= "<div class='Conntent_tool'><a href='javascript:;' class='print' onclick=\"MM_showHideLayers('print_option_choice','','show');\">  {$Lang['SysMgr']['Homework']['CustomizedPrint']}</a> 
							<div class='print_option_layer' id='print_option_choice' style='top:200px;'>
	                        	<em > - {$Lang['SysMgr']['Homework']['PrintOptions']} -</em>
	                          	<table class='form_table'>
	                            <tr>
	                            	<td width='5%'><input type='radio' name='printType' id='printType1' value='1' " . (($printType == 1 || $printType == "") ? " checked" : "") . " onClick='document.form1.printType.value=1'></td>
	                              	<td><label for='printType1'>{$Lang['SysMgr']['Homework']['ReportByForm']}</label></td>
	                            </tr>
	                            <tr>
	                              	<td><input type='radio' name='printType' id='printType2' value='2'" . (($printType == 2) ? " checked" : "") . " onClick='document.form1.printType.value=2'></td>
	                              	<td><label for='printType2'>{$Lang['SysMgr']['Homework']['ReportByClass']}</label></td>
	                            </tr>
	                          	</table>
	                          	<div class='edit_bottom'>
	                            	<p class='spacer'></p>
	                            	<input type='button' class='formsmallbutton'  onclick=\"doPrint();\"
			 onmouseover=\"this.className='formsmallbuttonon'\" onmouseout=\"this.className='formsmallbutton'\" value='{$Lang['Btn']['Print']}' />
	                            	<input type='button' class='formsmallbutton'
			onmouseover=\"this.className='formsmallbuttonon'\" onmouseout=\"this.className='formsmallbutton'\" value='{$Lang['Btn']['Cancel']}' onclick=\"MM_showHideLayers('print_option_choice','','hide')\"/>
	                           		<p class='spacer'></p>
	                          	</div>
	                        </div>
						</div>";


    $toolbar .= "<div class='Conntent_tool'><a href='javascript:;' class='export' onclick=\"MM_showHideLayers('export_option_choice','','show');\">  {$Lang['SysMgr']['Homework']['CustomizedExport']}</a> 
							<div class='print_option_layer' id='export_option_choice' style='top:200px;'>
	                        	<em > - {$Lang['SysMgr']['Homework']['ExportOptions']} -</em>
	                          	<table class='form_table'>
	                            <tr>
	                            	<td width='5%'><input type='radio' name='exportType' id='exportType1' value='1' " . (($exportType == 1 || $exportType == "") ? " checked" : "") . " onClick='document.form1.exportType.value=1'></td>
	                              	<td><label for='exportType1'>{$Lang['SysMgr']['Homework']['ReportByForm']}</label></td>
	                            </tr>
	                            <tr>
	                              	<td><input type='radio' name='exportType' id='exportType2' value='2'" . (($exportType == 2) ? " checked" : "") . " onClick='document.form1.exportType.value=2'></td>
	                              	<td><label for='exportType2'>{$Lang['SysMgr']['Homework']['ReportByClass']}</label></td>
	                            </tr>
	                          	</table>
	                          	<div class='edit_bottom'>
	                            	<p class='spacer'></p>
	                            	<input type='button' class='formsmallbutton'  onclick=\"doExport();\"
			 onmouseover=\"this.className='formsmallbuttonon'\" onmouseout=\"this.className='formsmallbutton'\" value='$button_export' />
	                            	<input type='button' class='formsmallbutton'
			onmouseover=\"this.className='formsmallbuttonon'\" onmouseout=\"this.className='formsmallbutton'\" value='{$Lang['Btn']['Cancel']}' onclick=\"MM_showHideLayers('export_option_choice','','hide')\"/>
	                           		<p class='spacer'></p>
	                          	</div>
	                        </div>
						</div>";
}

$Name = $lhomework->getAcademicYearNameAndYearTermName($yearID, $yearTermID);
list($yearName, $yearTermName) = $Name[0];
if($yearTermID=="") $yearTermName = $Lang['SysMgr']['Homework']['AllYearTerms'];

$data = $lhomework->getNotSubmitList($yearID, $yearTermID, $subjectID, $targetID, $targetListResult, $studentListResult, $startDate, $endDate, $dateChoice, $times, $range, $group, $order, $DueDateEqualToToday, $status);
// debug_pr($data);

$statusAry = $lhomework->Get_Handin_Status($returnAssociateArray=1);

$result = "<table width='100%' border='0' align='center' cellpadding='5' cellspacing='0'>";
$result .= "<tr>";
$result .= "<td class='tablebluetop tabletopnolink' width='4%'>#</td>";
$result .= "<td class='tablebluetop tabletopnolink' width='10%'>".$Lang['SysMgr']['Homework']['Class']."</td>";
if($group==1) {
	$result .= "<td class='tablebluetop tabletopnolink' width='15%'>".$Lang['SysMgr']['Homework']['ClassNumber']."</td>";
}
if($group==1) {
	$result .= "<td class='tablebluetop tabletopnolink' width='20%'>".$Lang['SysMgr']['Homework']['StudentName']."</td>";
}
$result .= "<td class='tablebluetop tabletopnolink' width='15%'>".$Lang['SysMgr']['Homework']['Subject']."</td>";
$result .= "<td class='tablebluetop tabletopnolink' width='20%'>".$Lang['SysMgr']['Homework']['SubjectGroup']."</td>";
$result .= "<td class='tablebluetop tabletopnolink' width='20%'>".$statusAry[$status]." ".$Lang['eHomework']['Count']."</td>";
$result .= "</tr>";

if(sizeof($data) != 0)
{
	for($i=0; $i<sizeof($data); $i++)
	{
		$css = ($i%2)? "class='tabletext tablerow_total seperator_right'" : "";
		list($className, $calssNumber, $subject, $subjectGroup, $studentName, $recordCount) = $data[$i];
		
		$result .= "<tr $css>";
		$result .= "<td>".($i+1)."</td>";
		$result .= "<td>".$className."</td>";
		if($group==1) {
			$result .= "<td>".$calssNumber."</td>";
		}
		if($group==1) {
			$result .= "<td>".$studentName."</td>";
		}
		$result .= "<td>".$subject."</td>";
		$result .= "<td>".$subjectGroup."</td>";
		$result .= "<td>".$recordCount."</td>";
		$result .= "</tr>";
	}
}
else
{
	$result .= "<tr>";
	$result .= "<td align=\"center\" colspan=\"7\">".$Lang['SysMgr']['Homework']['NoRecord']."</td>";
	$result .= "</tr>";
}

$result .= "</table>";

# Build thickbox
$thickBoxHeight = 150;
$thickBoxWidth = 600;
$pushMessageButton = $linterface->Get_Thickbox_Link($thickBoxHeight, $thickBoxWidth, "", 
										$Lang['AppNotifyMessage']['Homework']['button'], "", 'divPushMessage', $content=$thickboxContent, 'pushMessageButton');

# Thickbox layout
$thickboxContent = '';
$thickboxContent .= '<div id="divPushMessage" style="display:none">';
$thickboxContent .= '<form id="messageMethodForm" action="#">';
$thickboxContent .= '<table class="form_table_v30">';
$thickboxContent .= '<tr>';
$thickboxContent .= '<td class="field_title"><span>'.$Lang['AppNotifyMessage']['Homework']['MessagingMethod'].'</span></td>';
$thickboxContent .= '<td>';
$smsChecked = 1;
if ($plugin['ASLParentApp'] || $plugin['eClassApp']) {
	$smsChecked = 0;
	$thickboxContent .= $linterface->Get_Radio_Button('pushMessage', 'messageMethod', 'pushMessage', $isChecked=1, $Class='', $Lang['AppNotifyMessage']['Homework']['PushMessage'], $Onclick='', $Disabled='');
	$thickboxContent .= '<br/>';
}
if ($plugin['sms']) {
	$thickboxContent .= $linterface->Get_Radio_Button('SMS', 'messageMethod', 'SMS', $isChecked=$smsChecked, $Class='', $Lang['AppNotifyMessage']['Homework']['SMS'], $Onclick='', $Disabled='');
	$thickboxContent .= '<br/>';
}
if (($plugin['ASLParentApp'] || $plugin['eClassApp']) && $plugin['sms']) {
	$thickboxContent .= $linterface->Get_Radio_Button('pushMessageThenSMS', 'messageMethod', 'pushMessageThenSMS', $isChecked=0, $Class='', $Lang['AppNotifyMessage']['Homework']['PushMessageThenSMS'], $Onclick='', $Disabled='');
	$thickboxContent .= '</td>';
}
$thickboxContent .= '</tr>';
$thickboxContent .= '</table>';
$thickboxContent .= '<br/>';
$thickboxContent .= '<div align="center">';
$thickboxContent .= $linterface->GET_ACTION_BTN($Lang['AppNotifyMessage']['Homework']['button'], "button", "js_send_message()");
$thickboxContent .= '</div>';	
$thickboxContent .= '</form>';
$thickboxContent .= '</div>';		

// Date 2016-10-11 Villa
# Thickbox layout
$thickboxContent_student = '';
$thickboxContent_student .= '<div id="divPushMessage_student" style="display:none">';
$thickboxContent_student .= '<form id="messageMethodForm" action="#">';
$thickboxContent_student .= '<table class="form_table_v30">';
$thickboxContent_student .= '<tr>';
$thickboxContent_student .= '<td class="field_title"><span>'.$Lang['AppNotifyMessage']['Homework']['MessagingMethod'].'</span></td>';
$thickboxContent_student .= '<td>';
$smsChecked = 1;
if ($plugin['ASLParentApp'] || $plugin['eClassApp']) {
	$smsChecked = 0;
	$thickboxContent_student .= $linterface->Get_Radio_Button('pushMessage', 'messageMethod', 'pushMessage', $isChecked=1, $Class='', $Lang['AppNotifyMessage']['Homework']['PushMessage'], $Onclick='', $Disabled='');
	$thickboxContent_student .= '<br/>';
}
if ($plugin['sms']) {
	$thickboxContent_student .= $linterface->Get_Radio_Button('SMS', 'messageMethod', 'SMS', $isChecked=$smsChecked, $Class='', $Lang['AppNotifyMessage']['Homework']['SMS'], $Onclick='', $Disabled='');
	$thickboxContent_student .= '<br/>';
}
if (($plugin['ASLParentApp'] || $plugin['eClassApp']) && $plugin['sms']) {
	$thickboxContent_student .= $linterface->Get_Radio_Button('pushMessageThenSMS', 'messageMethod', 'pushMessageThenSMS', $isChecked=0, $Class='', $Lang['AppNotifyMessage']['Homework']['PushMessageThenSMS'], $Onclick='', $Disabled='');
	$thickboxContent_student .= '</td>';
}
$thickboxContent_student .= '</tr>';
$thickboxContent_student .= '</table>';
$thickboxContent_student .= '<br/>';
$thickboxContent_student .= '<div align="center">';
$thickboxContent_student .= $linterface->GET_ACTION_BTN($Lang['AppNotifyMessage']['Homework']['studentButton'], "button", "js_send_message_student()");
$thickboxContent_student .= '</div>';
$thickboxContent_student .= '</form>';
$thickboxContent_student .= '</div>';
$pushMessageButton_student = $linterface->Get_Thickbox_Link($thickBoxHeight, $thickBoxWidth, "",
		'pushMessageButton_student', "", 'divPushMessage_student', $content=$thickboxContent_student, 'pushMessageButton_student');

$linterface->LAYOUT_START();

if ($plugin['eClassApp']) {
	$sendPushMessagePhp = 'notify_parent_via_eClassApp.php';
	$sendPushMessageThenSmsPhp = 'notify_parent_via_eClassApp_then_sms.php';
}
else {
	$sendPushMessagePhp = 'notify_parent_via_app.php';
	$sendPushMessageThenSmsPhp = 'notify_parent_via_app_then_sms.php';
}
?>

<script type="text/javascript" src= "<?=$PATH_WRT_ROOT?>templates/jquery/thickbox.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.blockUI.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.tablednd_0_5.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.cookies.2.2.0.js"></script>

<link rel="stylesheet" href="<?=$PATH_WRT_ROOT?>templates/jquery/thickbox.css" type="text/css" media="screen" />

<script language="javascript">
$(document).ready(function () {
	initThickBox();
});

function back(){
	window.location = "index.php";
}

function viewNotHandinDetail(subjectGroupID, userID, yearID, yearTermID, yearClassID, status){
   newWindow('./view.php?subjectGroupID='+subjectGroupID+'&studentID='+userID+'&yearID='+yearID+'&yearTermID='+yearTermID+'&yearClassID='+yearClassID+'&group=<?=$group?>&startDate=<?=$startDate?>&endDate=<?=$endDate?>&dateChoice=<?=$dateChoice?>&DueDateEqualToToday=<?=$DueDateEqualToToday?>&status='+status,1);
}

function notifyParentByApp(e)
{
	$('#PreviewStatus').html('<?=$linterface->Get_Ajax_Loading_Image()?>');
	$.ajax(
		{
			url: '<?=$sendPushMessagePhp?>?times=<?=$times?>&range=<?=$range?>&order=<?=$order?>&group=<?=$group?>&<?=$dateLink?>&subjectID=<?=$subjectID?>&targetID=<?=$targetID?>&DueDateEqualToToday=<?=$DueDateEqualToToday?>&status=<?=$status?>',
			error: function(xhr) {
				alert('request error');
			},
			success: function(response) {
				$('#PreviewStatus').html(response);
			}
		}
	);
}

function notifyParentBySMS()
{
	$('#PreviewStatus').html('<?=$linterface->Get_Ajax_Loading_Image()?>');
	$.ajax(
		{
			url: 'notify_parent_via_sms.php?times=<?=$times?>&range=<?=$range?>&order=<?=$order?>&group=<?=$group?>&<?=$dateLink?>&subjectID=<?=$subjectID?>&targetID=<?=$targetID?>&DueDateEqualToToday=<?=$DueDateEqualToToday?>&status=<?=$status?>',
			error: function(xhr) {
				alert('request error');
			},
			success: function(response) {
				$('#PreviewStatus').html(response);
			}
		}
	);
}

function notifyParentByAppThenSMS()
{
	$('#PreviewStatus').html('<?=$linterface->Get_Ajax_Loading_Image()?>');
	$.ajax(
		{
			url: '<?=$sendPushMessageThenSmsPhp?>?times=<?=$times?>&range=<?=$range?>&order=<?=$order?>&group=<?=$group?>&<?=$dateLink?>&subjectID=<?=$subjectID?>&targetID=<?=$targetID?>&DueDateEqualToToday=<?=$DueDateEqualToToday?>&status=<?=$status?>',
			error: function(xhr) {
				alert('request error');
			},
			success: function(response) {
				$('#PreviewStatus').html(response);
			}
		}
	);
}

function js_send_message()
{
	var selectedVal = "";
	selectedVal = $('input:radio[name=messageMethod]:checked').val();
	
	if (selectedVal == 'pushMessage') {
		notifyParentByApp();
	} else if (selectedVal == 'SMS') {
		notifyParentBySMS();
	} else if (selectedVal == 'pushMessageThenSMS') {
		notifyParentByAppThenSMS();
	}
	js_Hide_ThickBox();
}

function js_show_thickbox()
{
	$('a#pushMessageButton').click();
}

// Date: 2016-10-11 (villa) send push msg to student
function js_send_message_student()
{
	notifyStudentsByApp();
	js_Hide_ThickBox();
}

function js_show_thickbox_studentApp()
{
	$('a#pushMessageButton_student').click();
}

function notifyStudentsByApp()
{
	$('#PreviewStatus').html('<?=$linterface->Get_Ajax_Loading_Image()?>');
	$.ajax(
		{
			url: 'notify_student_via_app.php?times=<?=$times?>&range=<?=$range?>&order=<?=$order?>&group=<?=$group?>&<?=$dateLink?>&subjectID=<?=$subjectID?>&targetID=<?=$targetID?>&DueDateEqualToToday=<?=$DueDateEqualToToday?>&status=<?=$status?>',
			error: function(xhr) {
				alert('request error');
			},
			success: function(response) {
				$('#PreviewStatus').html(response);
			}
		}
	);
}

function doExport()
{
    var original_action = document.form1.action;
    document.form1.action = "customized_export.php";
    document.form1.method = 'post';
    document.form1.submit();
    document.form1.action = original_action;
}

function doPrint()
{
    var original_action = document.form1.action;
    document.form1.target = '_blank';
    document.form1.action = "customized_print.php";
    document.form1.method = 'post';
    document.form1.submit();
    document.form1.action = original_action;
    document.form1.target = '_self';
}

</script>

<form name="form1" action="list.php">
	<table width="100%" border="0" cellpadding="5" cellspacing="5">
		<tr>
			<td>
				<table border="0" cellspacing="0" cellpadding="2" width="500">
					<tr>
						<td><?=$toolbar?></td>
					</tr>
				</table>
			</td>
		</tr>
		
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="5" cellpadding="5">
				<? if($dateChoice=='YEAR') { ?>
					<tr>
						<td class="tabletext formfieldtitle" width="30%" valign="top"><?=$Lang['SysMgr']['Homework']['AcademicYear']?></td>
						<td width="567" align="left" valign="top">
							<?=$yearName?>
						</td>
					</tr>
					<tr>
						<td class="tabletext formfieldtitle" width="30%" valign="top"><?=$Lang['SysMgr']['Homework']['YearTerm']?></td>
						<td width="567" align="left" valign="top">
							<?=$yearTermName?>
						</td>
					</tr>
				<? } else { ?>
					<tr>
						<td class="tabletext formfieldtitle" width="30%" valign="top"><?=$iDiscipline['Period']?></td>
						<td width="567" align="left" valign="top">
							<?=$startDate." ".$i_To." ".$endDate?>
						</td>
					</tr>
				<? } ?>
				</table>
			</td>
		</tr>
		
		<tr>
			<td>
				<?=$result?>
			</td>
		</tr>
		
		<tr>
			<td class="dotline">
				<img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1">
			</td>
		</tr>
		<tr>
			<td align="center">
				<?=$linterface->GET_ACTION_BTN($button_back, "button", "javascript:back()")?>
				<?php
				if ($status==-1 && sizeof($data)>0 && ($plugin['sms'] || $plugin['ASLParentApp'] || $plugin['eClassApp']))
				{
					//echo $linterface->GET_ACTION_BTN($Lang['AppNotifyMessage']['Homework']['button'], "button", "self.location='notify_parent_via_app.php?times=$times&range=$range&order=$order&group=$group&$dateLink&subjectID=$subjectID&targetID=$targetID&DueDateEqualToToday=$DueDateEqualToToday&status=$status'");
					echo $linterface->GET_ACTION_BTN($Lang['AppNotifyMessage']['Homework']['button'], "button", "js_show_thickbox()");
					echo $pushMessageButton;
					

				}
				if($status==-1 && sizeof($data)>0 && $plugin['eClassStudentApp'])
				{
					//Date: 2016-10-11 Villa
					echo $linterface->GET_ACTION_BTN($Lang['AppNotifyMessage']['Homework']['studentButton'], "button", "js_show_thickbox_studentApp()");
					echo $pushMessageButton_student;
				}
				?>
				<p><span id='PreviewStatus'></span></p>
			</td>
		</tr>
	</table>
	
<?php
    foreach((array) $targetListResult as $v) {
        print '<input type="hidden" name="targetList[]" value="'.$v.'"/>'."\n";
    }
    foreach((array) $studentListResult as $v) {
        print '<input type="hidden" name="studentList[]" value="'.$v.'"/>'."\n";
    }
?>
    <input type="hidden" name="times" value="<?php echo $times;?>"/>
    <input type="hidden" name="range" value="<?php echo $range;?>"/>
    <input type="hidden" name="order" value="<?php echo $order;?>"/>
    <input type="hidden" name="group" value="<?php echo $group;?>"/>
    <input type="hidden" name="dateChoice" value="DATE"/>
    <input type="hidden" name="startDate" value="<?php echo $startDate;?>"/>
    <input type="hidden" name="endDate" value="<?php echo $endDate;?>"/>
    <input type="hidden" name="subjectID" value="<?php echo $subjectID;?>"/>
    <input type="hidden" name="targetID" value="<?php echo $targetID;?>"/>
    <input type="hidden" name="status" value="<?php echo $status;?>"/>

</form>

<?=$thickboxContent?>

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>