<?php
// Modifying by : 

########### Change Log [Start] ###########
#
#   Date    :   2019-09-06 (Tommy)
#               change access permission checking
#
#   Date:   2019-05-07 (Bill)   [2019-0503-1105-06207]
#           fixed cannot send push message
#           updated push message content
#           prevent SQL Injection + Cross-site Scripting
#
#	Date :	2016-12-29 (Bill)	[2016-1223-0935-55206]
#			fixed send duplicated and incorrect push message
#
###########  Change Log [End]  ###########

session_start();
$targetList = $_SESSION["targetList"];
$studentList = $_SESSION["studentList"];

$PATH_WRT_ROOT = "../../../../../../";
$CurrentPageArr['eAdminHomework'] = 1;

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp.php");

intranet_auth();
intranet_opendb();

### Handle SQL Injection + XSS [START]
if(!intranet_validateDate($startDate)) {
    $startDate = date('Y-m-d');
}
if(!intranet_validateDate($endDate)) {
    $endDate = date('Y-m-d');
}

$subjectID = IntegerSafe($subjectID);
$targetID = IntegerSafe($targetID);
$targetList = IntegerSafe($targetList);
$studentList = IntegerSafe($studentList);

$status = IntegerSafe($status);
$times = IntegerSafe($times);
$range = IntegerSafe($range);
$group = IntegerSafe($group);
$order = IntegerSafe($order);
### Handle SQL Injection + XSS [END]

$lhomework = new libhomework2007();
$libeClassApp = new libeClassApp();

if(!$plugin['eHomework']) {
    include_once($PATH_WRT_ROOT."includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit;
}
else {
//    if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || !($_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"] || $_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] && !$lhomework->isViewerGroupMember($UserID)))
      if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || (!$_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"] && !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] && !$lhomework->isViewerGroupMember($UserID)))
      {
        if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])
        {
            header("location: ../../settings/index.php");
            exit;
        }
        include_once($PATH_WRT_ROOT."includes/libaccessright.php");
        $laccessright = new libaccessright();
        $laccessright->NO_ACCESS_RIGHT_REDIRECT();
        exit;
    }
}

if(!isset($status) || $status=="" || $status != "-1")
{
    die("This function is to inform students for their records of not handing homework only!");
}

# Select Data from DB
$order = 1;  # must order by student in order to group different subjects
$row = $lhomework->getNotSubmitListPrint_Format2($yearID, $yearTermID, $subjectID, $targetID, $targetList, $studentList, $startDate, $endDate, $dateChoice, $times, $range, $order, $group, $DueDateEqualToToday, $status);

$previous_user_id = 0;
$SubjectsInvolved = array();
if ($yearID != "" && $yearTermID != "") {
    $Name = $lhomework->getAcademicYearNameAndYearTermName($yearID, $yearTermID);
    list($yearName, $yearTermName) = $Name[0];
}

### Build push message info array
$messageInfoAry = array();
$pushMsgCount = 0;
for($i=0; $i<sizeof($row); $i++)
{
	list($ClassName, $ClassNumber, $Subject, $SubjectGroup, $StudentName, $Topic, $Desciprtion, $StartDate, $DueDate, $CountNumber, $StudentID) = $row[$i];
	
	/* 
	# Set push message target
	$_targetStudentId = $libeClassApp->getDemoSiteUserId($StudentID);
	$messageInfoAry[$pushMsgCount]['relatedUserIdAssoAry'][$StudentID] = array($_targetStudentId);
     */
	
	// [2016-1223-0935-55206]
	// last record of current student
	if ($StudentID != $row[$i+1]["UserID"])
	{
		// Add homework to list
		$SubjectsInvolved[] = "(".$Subject.") ". $Topic;
		
		# Send the message
		$SubjectsText = implode(", ", $SubjectsInvolved);
		$StudentNameApps = $StudentName;
		if ($ClassName!="" && $ClassNumber!="") {
			$StudentNameApps .= " ({$ClassName}-{$ClassNumber})";
		}
		if ($yearName!="" || $yearTermName!="") {
			$DateInvolved = $Lang['AppNotifyMessage']['Homework']['In'] . $yearName . " " . $yearTermName;
			$MessageCentent = $Lang['AppNotifyMessage']['Homework']['Alert']['student_app']['single_day'];
			$MessageCentent = str_replace("[StartDate]", $DateInvolved, $MessageCentent);
		}
		else if ($startDate==$endDate) {
			$DateInvolved = $startDate;
			$MessageCentent = $Lang['AppNotifyMessage']['Homework']['Alert']['student_app']['single_day'];
			$MessageCentent = str_replace("[StartDate]", $startDate, $MessageCentent);
		}
		else {
			$MessageCentent = $Lang['AppNotifyMessage']['Homework']['Alert']['student_app']['period'];
			$MessageCentent = str_replace("[StartDate]", $startDate, $MessageCentent);
			$MessageCentent = str_replace("[EndDate]", $endDate, $MessageCentent);
		}
		$MessageCentent = str_replace("[Subjects]", $SubjectsText, $MessageCentent);
		$MessageCentent = str_replace("[StudentName]", $StudentNameApps, $MessageCentent);
		$MessageTitle = $Lang['AppNotifyMessage']['Homework']['Title'];
		
		# Set push message target
		$_targetStudentId = $libeClassApp->getDemoSiteUserId($StudentID);
		$messageInfoAry[$pushMsgCount]['relatedUserIdAssoAry'][$StudentID] = array($_targetStudentId);
		
		# Set push message content
		$messageInfoAry[$pushMsgCount]['messageTitle'] = $MessageTitle;
		$messageInfoAry[$pushMsgCount]['messageContent'] = $MessageCentent;
		$pushMsgCount++;
		
		// Clear homework list to prevent duplicated homework
		$SubjectsInvolved = array();
	}
	else
	{
		// Add homework to list
		//debug($i, $previous_user_id, $StudentID );
		//debug_r( $row[$i]);
		$SubjectsInvolved[] = "(".$Subject.") ". $Topic;
	}
}

// debug_pr($messageInfoAry);
// die();

// $row = $lhomework->getNotSubmitListPrint_Format2($yearID, $yearTermID, $subjectID, $targetID, $targetList, $studentList, $startDate, $endDate, $dateChoice, $times, $range, $order, $group, $DueDateEqualToToday,$status);
// $studentIdAry = Get_Array_By_Key($row, 'UserID');
// // debug_pr($studentIdAry);
// $individualMessageInfoAry = array();
// foreach ($studentIdAry as $studentId) {
// 	$_targetStudentId = $libeClassApp->getDemoSiteUserId($studentId);
// 	$StudentAssoAry[$studentId] = array($_targetStudentId);
// }
// $individualMessageInfoAry[0]['relatedUserIdAssoAry'] = $StudentAssoAry;
//always delete the old record to override the new one

$notifyMessageId = $libeClassApp->sendPushMessage($messageInfoAry, $MessageTitle, 'MULTIPLE MESSAGES', $isPublic='', $recordStatus=1, $eclassAppConfig['appType']['Student']);
$statisticsAry = $libeClassApp->getPushMessageStatistics($notifyMessageId);
$statisticsAry = $statisticsAry[$notifyMessageId];

intranet_closedb();

if ($notifyMessageId > 0) {
	echo "<font color='#DD5555'>".str_replace("[SentTotal]", $statisticsAry['numOfSendSuccess'], $Lang['AppNotifyMessage']['Homework']['send_result_student']). "</font>";
}
else {
	echo "<font color='red'>".$Lang['AppNotifyMessage']['Homework']['send_failed']."</font>";
}
?>