<?php
// Modifing by 
##### Change Log [Start] #####
#
#   Date    :   2019-09-06 (Tommy)
#               change access permission checking
#
###### Change Log [End] ######	

$PATH_WRT_ROOT = "../../../../../../";
$CurrentPageArr['eAdminHomework'] = 1;

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$lhomework = new libhomework2007();

if(!$plugin['eHomework']){
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

else{
// 	if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || (!$_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"] && !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] && !$lhomework->isViewerGroupMember($UserID)))
    if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || (!$_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"] && !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] && !$lhomework->isViewerGroupMember($UserID)))
    {
		if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])
		{
			header("location: ../../settings/index.php");	
			exit;
		}
		include_once($PATH_WRT_ROOT."includes/libaccessright.php");
		$laccessright = new libaccessright();
		$laccessright->NO_ACCESS_RIGHT_REDIRECT();
		exit;
	}
}

# Temp Assign memory of this page
ini_set("memory_limit", "150M");

$linterface = new interface_html();


if($yearClassID!=""){
	# Select Student
	$name_field = getNameFieldByLang("b.");
	$conds = ($subjectID=="")? "": "AND e.RecordID = ".$subjectID;
	$sql = "SELECT a.UserID, CONCAT(CONCAT(b.ClassName,'-',b.ClassNumber),' ',$name_field)
			FROM YEAR_CLASS_USER AS a
			INNER JOIN INTRANET_USER AS b ON b.UserID = a.UserID
			INNER JOIN SUBJECT_TERM_CLASS_USER AS c ON c.UserID = a.UserID
			INNER JOIN SUBJECT_TERM AS d ON d.SubjectGroupID = c.SubjectGroupID
			INNER JOIN ASSESSMENT_SUBJECT AS e ON e.RecordID = d.SubjectID
			WHERE a.YearClassID = $yearClassID $conds GROUP BY a.UserID
			ORDER BY b.ClassNumber";
	
	$result = $lhomework->returnArray($sql, 2);
	
	if(sizeof($result)!=0)
		$selectedStudentList = $lhomework->getSlectedList("name=\"studentListResult[]\" id=\"studentListResult\" size=\"5\" multiple", $result, "", "", false);
	else
		$selectedStudentList = "<select name=\"studentListResult[]\" id=\"studentListResult\" size=\"5\"></select>";
	$button = $linterface->GET_BTN($button_select_all, "button", "SelectAll(document.getElementById('studentListResult')); return false;", "selectAllBtn");

		
	$output = "	<br>
				<span>
					$selectedStudentList
				</span>
				<br><br>
				<span>
					$button
				</span>";
}

else{
	$selectedStudentList = "<select name=\"studentListResult\" id=\"studentListResult\" size=\"5\"></select>";
	$button = $linterface->GET_BTN($button_select_all, "button", "SelectAll(document.getElementById('studentListResult')); return false;", "selectAllBtn");
	
	$output = "	<br>
				<span>
					$selectedStudentList
				</span>
				<br><br>
				<span>
					$button
				</span>";
}
		
echo $output;

intranet_closedb();

?>