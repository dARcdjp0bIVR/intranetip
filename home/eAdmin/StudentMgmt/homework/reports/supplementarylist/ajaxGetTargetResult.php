<?php
// Modifing by 
##### Change Log [Start] #####
#
#   Date    :   2019-09-06 (Tommy)
#               change access permission checking
#
###### Change Log [End] ######	
		
$PATH_WRT_ROOT = "../../../../../../";
$CurrentPageArr['eAdminHomework'] = 1;

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$lhomework = new libhomework2007();

if(!$plugin['eHomework']){
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

else{
// 	if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || !($_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"] || $_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] && !$lhomework->isViewerGroupMember($UserID)))
    if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || (!$_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"] && !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] && !$lhomework->isViewerGroupMember($UserID)))
    {
		if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])
		{
			header("location: ../../settings/index.php");	
			exit;
		}
		include_once($PATH_WRT_ROOT."includes/libaccessright.php");
		$laccessright = new libaccessright();
		$laccessright->NO_ACCESS_RIGHT_REDIRECT();
		exit;
	}
}

# Temp Assign memory of this page
ini_set("memory_limit", "150M");

$linterface = new interface_html();


# Please Select
if($targetID == ""){
	# Target List
	$selectedTargetList = "<select name=\"targetListResult[]\" id=\"targetListResult\" size=\"5\"></select>";
	$button = $linterface->GET_BTN($button_select_all, "button", "SelectAll(document.getElementById('targetListResult')); return false;", "selectAllBtn");
	
	$output = "<span id=\"list\">
					$selectedTargetList
				</span>
				<br><br>
				<span id=\"button\">
					$button
				</span>";
}

# Class
if($targetID == 1){
	$conds = ($yearTermID=="")? "": "AND a.YearTermID = ".$yearTermID;
	$conds .= ($subjectID=="")? "": " AND d.RecordID = ".$subjectID;
	
	$sql = "SELECT b.YearClassID, 
			IF('$intranet_session_language'= 'en', b.ClassTitleEN, b.ClassTitleB5) AS ClassName
			FROM ACADEMIC_YEAR_TERM AS a
			INNER JOIN YEAR_CLASS AS b ON b.AcademicYearID = a.AcademicYearID
			INNER JOIN YEAR e ON (e.YearID=b.YearID)
			INNER JOIN SUBJECT_TERM AS c ON c.YearTermID = a.YearTermID
			INNER JOIN ASSESSMENT_SUBJECT AS d ON d.RecordID = c.SubjectID
			WHERE a.AcademicYearID = $yearID $conds GROUP BY b.YearClassID 
			ORDER BY e.Sequence, b.Sequence";

	$result = $lhomework->returnArray($sql, 2);
	
	$selectedTargetList = $lhomework->getSlectedList("name=\"targetListResult[]\" id=\"targetListResult\" size=\"5\" multiple", $result, "", "", false);
	$button = $linterface->GET_BTN($button_select_all, "button", "SelectAll(document.getElementById('targetListResult')); return false;", "selectAllBtn");
	
	$output = "<span id=\"list\">
					$selectedTargetList
				</span>
				<br><br>
				<span id=\"button\">
					$button
				</span>";
}

# Student
if($targetID == 2){
	$conds = ($yearTermID=="")? "": "AND a.YearTermID = ".$yearTermID;
	$conds .= ($subjectID=="")? "": " AND d.RecordID = ".$subjectID;
	
	$sql = "SELECT b.YearClassID, 
			IF('$intranet_session_language'= 'en', b.ClassTitleEN, b.ClassTitleB5) AS ClassName
			FROM ACADEMIC_YEAR_TERM AS a
			INNER JOIN YEAR_CLASS AS b ON b.AcademicYearID = a.AcademicYearID
			INNER JOIN YEAR e ON (e.YearID=b.YearID)
			INNER JOIN SUBJECT_TERM AS c ON c.YearTermID = a.YearTermID
			INNER JOIN ASSESSMENT_SUBJECT AS d ON d.RecordID = c.SubjectID
			WHERE a.AcademicYearID = $yearID $conds GROUP BY b.YearClassID
			ORDER BY e.Sequence, b.Sequence";
			
	$result = $lhomework->returnArray($sql, 2);
	
	$selectedTargetList = $lhomework->getSlectedList("name=\"targetListResult[]\" id=\"targetListResult\" onChange=\"getStudentList(this.value)\"", $result, $targetList, $Lang['SysMgr']['Homework']['PleaseSelect'], true);
	
	$selectedStudentList = "<select name=\"studentListResult[]\" id=\"studentListResult[]\" size=\"5\"></select>";
	$button = $linterface->GET_BTN($button_select_all, "button", "SelectAll(document.getElementById('studentListResult')); return false;", "selectAllBtn");

	$output = "<span id=\"list\"> 
					$selectedTargetList
				</span>
				<br>
				<span id=\"studentList\">
					<br>
					<span>
						$selectedStudentList
					</span>
					<br><br>
					<span>
						$button
					</span>
				</span>";
}

# Subject Group
if($targetID == 3){
	$conds = ($yearTermID=="")? "": "AND a.YearTermID = ".$yearTermID;
	$conds .= ($subjectID=="")? "": " AND d.RecordID = ".$subjectID;
	
	$sql = "SELECT b.SubjectGroupID, 
			IF('$intranet_session_language'= 'en', b.ClassTitleEN, b.ClassTitleB5) AS SubjectGroupName
			FROM SUBJECT_TERM AS a
			INNER JOIN SUBJECT_TERM_CLASS AS b ON b.SubjectGroupID = a.SubjectGroupID
			INNER JOIN ACADEMIC_YEAR_TERM AS c ON c.YearTermID = a.YearTermID
			INNER JOIN ASSESSMENT_SUBJECT AS d ON d.RecordID = a.SubjectID
			WHERE c.AcademicYearID = $yearID $conds
			ORDER BY SubjectGroupName";
			
	$result = $lhomework->returnArray($sql, 2);
	
	$selectedTargetList = $lhomework->getSlectedList("name=\"targetListResult[]\" id=\"targetListResult\" size=\"5\" multiple", $result, "", "", false);
	$button = $linterface->GET_BTN($button_select_all, "button", "SelectAll(document.getElementById('targetListResult')); return false;", "selectAllBtn");
	
	$output = "<span id=\"list\">
					$selectedTargetList
				</span>
				<br><br>
				<span id=\"button\">
					$button
				</span>";
}

echo $output;

intranet_closedb();

?>