<?php
// Modifing by 
##### Change Log [Start] #####
#
#   Date    :   2019-09-06 (Tommy)
#               change access permission checking
#
###### Change Log [End] ######	
		
$PATH_WRT_ROOT = "../../../../../../";
$CurrentPageArr['eAdminHomework'] = 1;

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");

intranet_auth();
intranet_opendb();

$lhomework = new libhomework2007();

if(!$plugin['eHomework']){
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

else{
//	if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || !($_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"] || $_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] && !$lhomework->isViewerGroupMember($UserID)))
    if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || (!$_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"] && !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] && !$lhomework->isViewerGroupMember($UserID)))
    {
		if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])
		{
			header("location: ../../settings/index.php");	
			exit;
		}
		include_once($PATH_WRT_ROOT."includes/libaccessright.php");
		$laccessright = new libaccessright();
		$laccessright->NO_ACCESS_RIGHT_REDIRECT();
		exit;
	}
}

$linterface = new interface_html();


$sql = "SELECT c.ClassName, c.ClassNumber,
		IF('$intranet_session_language'='en', e.EN_DES, e.CH_DES), 
		IF('$intranet_session_language'='en', a.ClassTitleEN, a.ClassTitleB5),
		IF('$intranet_session_language'='en', c.EnglishName, c.ChineseName)
		from SUBJECT_TERM_CLASS AS a 
		LEFT OUTER JOIN SUBJECT_TERM_CLASS_USER AS b ON b.SubjectGroupID = a.SubjectGroupID 
		LEFT OUTER JOIN INTRANET_USER AS c ON c.UserID = b.UserID 
		LEFT OUTER JOIN SUBJECT_TERM AS d ON d.SubjectGroupID = b.SubjectGroupID 
		LEFT OUTER JOIN ASSESSMENT_SUBJECT AS e ON e.RecordID = d.SubjectID 
		WHERE a.SubjectGroupID = $subjectGroupID AND b.UserID=$studentID";

$record = $lhomework->returnArray($sql,5);
list($className, $classNumber, $subject, $groupTitle, $name) = $record[0];
$x= $lhomework->printSupplementaryDetail($subjectGroupID, $studentID, $yearID, $yearTermID, $group, $yearClassID,$DueDateEqualToToday);

?>


<table width='100%' align='center' class='print_hide' border=0>
	<tr>
		<td align='right'><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></td>
	</tr>
</table>

<!-- Homework Title -->
<table width='100%' align="center" border="0">
	<tr>
        	<td align="center" ><b><?=$Lang['SysMgr']['Homework']['SupplementaryList']?></b></td>
	</tr>
</table>        

<br>
<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr><td align="left" valign="top" width="200"><?=$Lang['SysMgr']['Homework']['Class']?> : </td><td align="left" valign="top"><?=$className?></td></tr>
	<? if($group==1) { ?>
	<tr><td align="left" valign="top" width="200"><?=$Lang['SysMgr']['Homework']['ClassNumber']?> : </td><td align="left" valign="top"><?=$classNumber?></td></tr>
	<tr><td align="left" valign="top" width="200"><?=$Lang['SysMgr']['Homework']['StudentName']?> : </td><td align="left" valign="top"><?=$name?></td></tr>
	<? } ?>
	<tr><td align="left" valign="top" width="200"><?=$Lang['SysMgr']['Homework']['Subject']?> : </td><td align="left" valign="top"><?=$subject?></td></tr>
	<tr><td align="left" valign="top" width="200"><?=$Lang['SysMgr']['Homework']['SubjectGroup']?> : </td><td align="left" valign="top"><?=$groupTitle?></td></tr>
</table>
<br>
<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
	<?=$x?>
</table>
<?
include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_footer.php");
intranet_closedb();
?>