<?php
// Modifing by 
###### Change Log [Start] ######
#
#   Date    :   2019-09-06 (Tommy)
#               change access permission checking
#
#	Date	:	2014-03-21 Yuen
#				added this stats
#
#
####### Change Log [End] #######

$PATH_WRT_ROOT = "../../../../../../";
$CurrentPageArr['eAdminHomework'] = 1;

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$lhomework = new libhomework2007();

if(!$plugin['eHomework']){
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

else{
//	if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || !($_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"] || $_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] && !$lhomework->isViewerGroupMember($UserID)))
    if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || (!$_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"] && !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] && !$lhomework->isViewerGroupMember($UserID)))
    {
		if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])
		{
			header("location: ../../settings/index.php");	
			exit;
		}
		include_once($PATH_WRT_ROOT."includes/libaccessright.php");
		$laccessright = new libaccessright();
		$laccessright->NO_ACCESS_RIGHT_REDIRECT();
		exit;
	}
}

# Temp Assign memory of this page
ini_set("memory_limit", "150M");

$linterface = new interface_html();


$CurrentPage = "Statistics_StatsByMonth";

# page title
$PAGE_TITLE = $Lang['SysMgr']['Homework']['StatsByMonth'];

# tag information
$TAGS_OBJ[] = array($Lang['SysMgr']['Homework']['StatsByMonth'], "");

$MODULE_OBJ = $lhomework->GET_MODULE_OBJ_ARR();


# Academic Year
$academicYear = $lhomework->GetAllAcademicYear();
$academicYearID = ($academicYearID=="")? Get_Current_Academic_Year_ID():$academicYearID;

$selectedYear = $lhomework->getCurrentYear("name=\"academicYearID\" onChange='this.form.submit()' ", $academicYear, $academicYearID, "", false);



for ($i=9; $i<=12; $i++)
{
	$SchoolMonths[] = array($i, $Lang['General']['month'][$i]);
}
for ($i=1; $i<=6; $i++)
{
	$SchoolMonths[] = array($i, $Lang['General']['month'][$i]);
}
# Form Menu
$MonthNow = ($MonthNow=="") ? (int) date("m") : $MonthNow;
$selectedMonths = $linterface->GET_SELECTION_BOX($SchoolMonths, " id='MonthNow' name='MonthNow' onChange='this.form.submit()' ", "", $MonthNow);

# determine start and end dates of given month
$AcademicYearObj = new academic_year($academicYearID);

$YearStart = date("Y", strtotime($AcademicYearObj->AcademicYearStart));
$YearEnd = date("Y", strtotime($AcademicYearObj->AcademicYearEnd));

if (strtotime($YearStart."-".$MonthNow)<strtotime(date("Y-m", strtotime($AcademicYearObj->AcademicYearStart))))
{
	$datefrom = $YearEnd."-".$MonthNow."-01";
	$dateto = $YearEnd."-".$MonthNow;
} else
{	
	$datefrom = $YearStart."-".$MonthNow."-01";
	$dateto = $YearStart."-".$MonthNow;
}

$dateto .= "-".date("t", strtotime($datefrom));




# Form Menu
$forms = $lhomework->getForms($academicYearID);
//$selectedForms = $lhomework->getCurrentForms("name=\"formID\" onChange=\"reloadForm()\"", $forms, $formID, "");
if ($formID=="")
{
	$formID = $forms[0][0];
}
$selectedForms = $linterface->GET_SELECTION_BOX($forms, "name=\"formID\" onChange='this.form.submit()' ", "", $formID);


$filterbar = "$selectedYear $selectedYearTerm $selectedMonths $selectedForms $selectClass $selectedSubject";
$allowExport = $lhomework->exportAllowed;

# Print link
$printPreviewLink = "month_print.php?academicYearID=$academicYearID&formID=$formID&datefrom=$datefrom&dateto=$dateto&MonthNow=$MonthNow";
# Export link
$exportLink = "month_export.php?academicYearID=$academicYearID&formID=$formID&datefrom=$datefrom&dateto=$dateto&MonthNow=$MonthNow";

# Toolbar: new, import, export
$toolbar = $linterface->GET_LNK_PRINT("javascript:newWindow('$printPreviewLink', 23)", "", "", "", "", 0 );
if($allowExport)
	$toolbar .= $linterface->GET_LNK_EXPORT($exportLink, "", "", "", "", 0);

# Start layout
$linterface->LAYOUT_START();
?>


<form name="form1" method="post" action="month_index.php">
<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td>
							<table border="0" cellspacing="0" cellpadding="2" width="300">
								<tr>
									<td><?=$toolbar ?></td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td>
						<?=$filterbar?>
					</td>
				</tr>
			</table>
			<br>
			
				<?=$lhomework->GenerateMonthStats($academicYearID, $formID, $datefrom, $dateto)?>
		</td>
	</tr>
</table>
</form>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>