<?php
// Modifing by 
###### Change Log [Start] ######
#
#   Date    :   2019-09-06 (Tommy)
#               change access permission checking
#
#	Date	:	2014-03-21 Yuen
#				added this stats
#
#
####### Change Log [End] #######

$PATH_WRT_ROOT = "../../../../../../";
$CurrentPageArr['eAdminHomework'] = 1;

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

$lhomework = new libhomework2007();

if(!$plugin['eHomework']){
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

else{
// 	if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || !($_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"] || $_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] && !$lhomework->isViewerGroupMember($UserID)))
    if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || (!$_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"] && !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] && !$lhomework->isViewerGroupMember($UserID)))
    {
		if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])
		{
			header("location: ../../settings/index.php");	
			exit;
		}
		include_once($PATH_WRT_ROOT."includes/libaccessright.php");
		$laccessright = new libaccessright();
		$laccessright->NO_ACCESS_RIGHT_REDIRECT();
		exit;
	}
}

# Temp Assign memory of this page
ini_set("memory_limit", "150M");


$lexport = new libexporttext();

$ExportArr = array();

# Academic Year
$academicYear = $lhomework->GetAllAcademicYear();
$academicYearID = ($academicYearID=="")? Get_Current_Academic_Year_ID():$academicYearID;



for ($i=9; $i<=12; $i++)
{
	$SchoolMonths[] = array($i, $Lang['General']['month'][$i]);
}
for ($i=1; $i<=6; $i++)
{
	$SchoolMonths[] = array($i, $Lang['General']['month'][$i]);
}
# Form Menu
$MonthNow = ($MonthNow=="") ? (int) date("m") : $MonthNow;

# determine start and end dates of given month
$AcademicYearObj = new academic_year($academicYearID);

$YearStart = date("Y", strtotime($AcademicYearObj->AcademicYearStart));
$YearEnd = date("Y", strtotime($AcademicYearObj->AcademicYearEnd));

if (strtotime($YearStart."-".$MonthNow)<strtotime(date("Y-m", strtotime($AcademicYearObj->AcademicYearStart))))
{
	$datefrom = $YearEnd."-".$MonthNow."-01";
	$dateto = $YearEnd."-".$MonthNow;
} else
{	
	$datefrom = $YearStart."-".$MonthNow."-01";
	$dateto = $YearStart."-".$MonthNow;
}

$dateto .= "-".date("t", strtotime($datefrom));


# Form Menu
$forms = $lhomework->getForms($academicYearID);
if ($formID=="")
{
	$formID = $forms[0][0];
}

$ExportArr = $lhomework->GenerateMonthStats($academicYearID, $formID, $datefrom, $dateto, $IsExport=true);



$exportColumn = array($Lang['SysMgr']['Homework']['Subject'], $Lang['SysMgr']['Homework']['Class']);
$TotalDays = date("t", strtotime($datefrom));
for ($i=1; $i<=$TotalDays; $i++)
{
	$exportColumn[] = $i;
}
$exportColumn[] = $Lang['SysMgr']['Homework']['Total'];
$export_content = $lexport->GET_EXPORT_TXT($ExportArr, $exportColumn, "", "\r\n", "", 0, "00");

intranet_closedb();


$filename = "Homework_Statistics_byMonth_".date("Ym", strtotime($datefrom)).".csv";

// Output the file to user browser
$lexport->EXPORT_FILE($filename, $export_content);

intranet_closedb();
?>