<?php
// modifying : 

###### Change Log [Start] ######
#
#   Date    :   2019-09-06 (Tommy)
#               change access permission checking
#
#	Date    :	2019-05-14 (Bill)
#               - prevent SQL Injection
#
#	Date	:	2010-11-18 YatWoon
#				add "Total WorkLoad" data
#
#	Date	:	2010-09-29 Henry Chow
#				modified exportHomeworkStatistics(), display homework "statistics" depends on $classID
#
####### Change Log [End] #######

$PATH_WRT_ROOT = "../../../../../../";
$CurrentPageArr['eAdminHomework'] = 1;

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

### Handle SQL Injection + XSS [START]
$yearID = IntegerSafe($yearID);
$yearTermID = IntegerSafe($yearTermID);

$formID = IntegerSafe($formID);
$classID = IntegerSafe($classID);
$subjectID = IntegerSafe($subjectID);

if(!intranet_validateDate($from)) {
    $from = date('Y-m-d');
}
if(!intranet_validateDate($to)) {
    $to = date('Y-m-d');
}
### Handle SQL Injection + XSS [END]

$lhomework = new libhomework2007();

if(!$plugin['eHomework']){
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}
else{
//	if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || !($_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"] || $_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] && !$lhomework->isViewerGroupMember($UserID)))
    if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || (!$_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"] && !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] && !$lhomework->isViewerGroupMember($UserID)))
    {
		if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])
		{
			header("location: ../../settings/index.php");	
			exit;
		}
		include_once($PATH_WRT_ROOT."includes/libaccessright.php");
		$laccessright = new libaccessright();
		$laccessright->NO_ACCESS_RIGHT_REDIRECT();
		exit;
	}
}

$lexport = new libexporttext();

$ExportArr = array();

$searchBySubject = ($lhomework->subjectSearchDisabled == 0) ? " or IF('$intranet_session_language' = 'en', f.EN_DES, f.CH_DES) like '%$s%'" : "" ;

$yearTerm = ($yearTermID=="")? "" : "AND d.YearTermID = '$yearTermID'";

$date_conds = " AND date(d.LastModified) >= '$from' AND date(d.LastModified) <= '$to' AND d.AcademicYearID = '$yearID' $yearTerm";
$conds = ($yearID=="")? "" : " h.AcademicYearID = '$yearID'";
$conds .= ($yearTermID=="")? "" : " AND g.YearTermID = '$yearTermID'";

$forms = $lhomework->getForms($yearID);
if(sizeof($forms)!=0){
	$allForms = " AND c.YearID IN (";
	for ($i=0; $i < sizeof($forms); $i++)
	{	
		list($year_id)=$forms[$i];
		$allForms .= $year_id.",";
	}
	$allForms = substr($allForms,0,strlen($allForms)-1).")";
}
else{
	$allForms = "";
}

$conds .= ($formID=="")? $allForms : " AND c.YearID = '$formID' ";

// $yearID = $_GET['yearID'];
if($formID!="")
	$subject = $lhomework->getFormSubjectList($formID, $yearID, $yearTermID, $classID);
else
	$subject = $lhomework->getFormSubjectList("", $yearID, $yearTermID, $classID);

if(sizeof($subject)!=0){
	$allSubjects = " AND f.RecordID IN (";
	for ($i=0; $i < sizeof($subject); $i++)
	{	
		list($subjID)=$subject[$i];
		$allSubjects .= $subjID.",";
	}
	$allSubjects = substr($allSubjects,0,strlen($allSubjects)-1).")";
}
else{
	$allSubjects ="";
}

$conds .= ($subjectID=="")? $allSubjects : " AND f.RecordID = '$subjectID'";

if($s!=""){
	if($conds==""){
		$conds .= "(IF('$intranet_session_language' = 'en', a.ClassTitleEN, a.ClassTitleB5) like '%$s%'
					$searchBySubject
					) ";
	}
	else{
		$conds .= " AND (IF('$intranet_session_language' = 'en', a.ClassTitleEN, a.ClassTitleB5) like '%$s%'
						$searchBySubject
					) ";
	}
}
						
$filename = "Homework_Statistics_".$from."_".$to.".csv";

/*if ($formID != "")
{
	$sql = "SELECT YearName FROM YEAR WHERE YearID = '$formID'";
	$yearName = $lhomework->returnVector($sql,1);
	$filename = "Homework_Statistics_".$yearName[0]."_".$from."_".$to.".csv";
}

if ($subjectID != "")
{
	$sql = "SELECT EN_DES FROM ASSESSMENT_SUBJECT WHERE RecordID = '$subjectID'";
	$subjectName = $lhomework->returnVector($sql,1);
	$filename = "Homework_Statistics_AllForms_".$subjectName[0]."_".$from."_".$to.".csv";
	if($formID != "")
		$filename = "Homework_Statistics_".$yearName[0]."_".$subjectName[0]."_".$from."_".$to.".csv";
}*/

# Select Data from DB
$row = $lhomework->exportHomeworkStatistics($date_conds, $conds, $classID);

// Create data array for export
for($i=0; $i<sizeof($row); $i++){
	$m = 0;
	$ExportArr[$i][$m] = $row[$i][0];		//Subject
	$m++;
	$ExportArr[$i][$m] = $row[$i][1];		//Subject Group
	$m++;
	$ExportArr[$i][$m] = $row[$i][2];		//Homework Count
	$m++;
	$ExportArr[$i][$m] = $row[$i][3];		//Total WorkLoad
	$m++;
}

if(sizeof($row)==0) {
	$ExportArr[0][0] = $i_no_record_exists_msg;	
}

$exportColumn = array($Lang['SysMgr']['Homework']['YearTerm'], $Lang['SysMgr']['Homework']['Subject'], $Lang['SysMgr']['Homework']['SubjectGroup'], $Lang['SysMgr']['Homework']['HomeworkCount'], $Lang['SysMgr']['Homework']["TotalWorkload"]);	
$export_content = $lexport->GET_EXPORT_TXT($ExportArr, $exportColumn, "", "\r\n", "", 0, "00");

intranet_closedb();

// Output the file to user browser
$lexport->EXPORT_FILE($filename, $export_content);

?>