<?php
# using by : 

###############################################
#
#   Date    :   2019-09-06 (Tommy)
#               change access permission checking
#
#	Date:	2019-05-14 (Bill)
#           - prevent SQL Injection
#
###############################################
		
$PATH_WRT_ROOT = "../../../../../../";
$CurrentPageArr['eAdminHomework'] = 1;

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");

intranet_auth();
intranet_opendb();

### Handle SQL Injection + XSS [START]
$yearID = IntegerSafe($yearID);
$yearTermID = IntegerSafe($yearTermID);

$formID = IntegerSafe($formID);
$classID = IntegerSafe($classID);
$subjectID = IntegerSafe($subjectID);

if(!intranet_validateDate($from)) {
    $from = date('Y-m-d');
}
if(!intranet_validateDate($to)) {
    $to = date('Y-m-d');
}
### Handle SQL Injection + XSS [END]

$lhomework = new libhomework2007();

if(!$plugin['eHomework']){
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

else{
// 	if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || !($_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"] || $_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] && !$lhomework->isViewerGroupMember($UserID)))
    if($_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] || (!$_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"] && !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] && !$lhomework->isViewerGroupMember($UserID)))
    {
		if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])
		{
			header("location: ../../settings/index.php");	
			exit;
		}
		include_once($PATH_WRT_ROOT."includes/libaccessright.php");
		$laccessright = new libaccessright();
		$laccessright->NO_ACCESS_RIGHT_REDIRECT();
		exit;
	}
}

$linterface = new interface_html();


$s = addcslashes($s, '_');
$searchBySubject = ($lhomework->subjectSearchDisabled == 0) ? " or IF('$intranet_session_language' = 'en', f.EN_DES, f.CH_DES) like '%$s%'" : "" ;

$yearTerm = ($yearTermID=="")? "" : "AND d.YearTermID = '$yearTermID'";

$date_conds = " AND date(d.LastModified) >= '$from' AND date(d.LastModified) <= '$to' AND d.AcademicYearID = '$yearID' $yearTerm";
$conds = ($yearID=="")? "" : " h.AcademicYearID = '$yearID'";
$conds .= ($yearTermID=="")? "" : " AND g.YearTermID = '$yearTermID'";

$forms = $lhomework->getForms($yearID);
if(sizeof($forms)!=0){
	$allForms = " AND c.YearID IN (";
	for ($i=0; $i < sizeof($forms); $i++)
	{
		list($year_id) = $forms[$i];
		$allForms .= $year_id.",";
	}
	$allForms = substr($allForms,0,strlen($allForms)-1).")";
}
else{
	$allForms ="";
}

$conds .= ($formID=="")? $allForms : " AND c.YearID = '$formID' ";
if($formID != "")
	$subject = $lhomework->getFormSubjectList($formID, $yearID, $yearTermID, $classID);
else
	$subject = $lhomework->getFormSubjectList("", $yearID, $yearTermID, $classID);

if(sizeof($subject)!=0){
	$allSubjects = " AND f.RecordID IN (";
	for ($i=0; $i < sizeof($subject); $i++)
	{	
		list($subjID)=$subject[$i];
		$allSubjects .= $subjID.",";
	}
	$allSubjects = substr($allSubjects,0,strlen($allSubjects)-1).")";
}
else{
	$allSubjects ="";
}

$conds .= ($subjectID=="")? $allSubjects : " AND f.RecordID = '$subjectID'";

if($s!=""){
	if($conds==""){
		$conds .= "(IF('$intranet_session_language' = 'en', a.ClassTitleEN, a.ClassTitleB5) like '%$s%'
					$searchBySubject
					) ";
	}
	else{
		$conds .= " AND (IF('$intranet_session_language' = 'en', a.ClassTitleEN, a.ClassTitleB5) like '%$s%'
						$searchBySubject
					) ";
	}
}

// $yearID = $_GET['yearID'];
if($formID != "")
	$subject = $lhomework->getFormSubjectList($formID, $yearID, $yearTermID, $classID);
else
	$subject = $lhomework->getFormSubjectList("", $yearID, $yearTermID, $classID);

if(sizeof($subject)!=0){
	$allSubjects = " AND f.RecordID IN (";
	for ($i=0; $i < sizeof($subject); $i++)
	{	
		list($subjID)=$subject[$i];
		$allSubjects .= $subjID.",";
	}
	$allSubjects = substr($allSubjects,0,strlen($allSubjects)-1).")";
}
else{
	$allSubjects = "";
	$subjectID = "";
}	
$conds .= ($subjectID=='')? "$allSubjects" : " AND f.RecordID = '$subjectID' ";
	
$yearTerm = $Lang['SysMgr']['Homework']['AllYearTerms'];						
$form = $Lang['SysMgr']['Homework']['AllForms'];
$subject = $Lang['SysMgr']['Homework']['AllSubjects'];
$className = $Lang['SysMgr']['FormClassMapping']['AllClass'];

if ($yearID != "")
{
	$sql = "SELECT IF('$intranet_session_language' = 'en', YearNameEN, YearNameB5) FROM ACADEMIC_YEAR WHERE AcademicYearID = '$yearID'";
	$yearName = $lhomework->returnVector($sql,1);
	$year = $yearName[0];
}

if ($yearTermID != "")
{
	$sql = "SELECT IF('$intranet_session_language' = 'en', YearTermNameEN, YearTermNameB5) FROM ACADEMIC_YEAR_TERM WHERE YearTermID = '$yearTermID'";
	$yearTermName = $lhomework->returnVector($sql,1);
	$yearTerm = $yearTermName[0];
}

if ($formID != "")
{
	$sql = "SELECT YearName FROM YEAR WHERE YearID = '$formID'";
	$formName = $lhomework->returnVector($sql,1);
	$form = $formName[0];
}

if($classID != "") {
	$sql = "SELECT ".Get_Lang_Selection("ClassTitleB5","ClassTitleEN")." FROM YEAR_CLASS WHERE YearClassID = '$classID'";
	$clsName = $lhomework->returnVector($sql);
	$className = $clsName[0];
}


if ($subjectID != "")
{
	$sql = "SELECT IF('$intranet_session_language' = 'en', EN_DES, CH_DES) FROM ASSESSMENT_SUBJECT WHERE RecordID = '$subjectID'";
	$subjectName = $lhomework->returnVector($sql,1);
	$subject = $subjectName[0];
}

# Select Data from DB
$x = $lhomework->printHomeworkStatistics($date_conds, $conds, $classID);
?>

<table width='100%' align='center' class='print_hide' border=0>
	<tr>
		<td align='right'><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></td>
	</tr>
</table>

<!-- Homework Title -->
<table width='100%' align="center" border="0">
	<tr>
    	<td align="center" ><b><?=$Lang['SysMgr']['Homework']['HomeworkStatistics']?></b></td>
	</tr>
</table>        

<br>
<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr><td align="left" valign="top" width="100"><?=$Lang['SysMgr']['Homework']['AcademicYear']?> : </td><td align="left" valign="top"><?=$year?></td></tr>
	<tr><td align="left" valign="top" width="100"><?=$Lang['SysMgr']['Homework']['YearTerm']?> : </td><td align="left" valign="top"><?=$yearTerm?></td></tr>
	<tr><td align="left" valign="top" width="100"><?=$Lang['SysMgr']['Homework']['Form']?> : </td><td align="left" valign="top"><?=$form?></td></tr>
	<tr><td align="left" valign="top" width="100"><?=$i_ClassName?> : </td><td align="left" valign="top"><?=$className?></td></tr>
	<tr><td align="left" valign="top" width="100"><?=$Lang['SysMgr']['Homework']['Subject']?> : </td><td align="left" valign="top"><?=$subject?></td></tr>
	<tr><td align="left" valign="top" width="100"><?=$Lang['SysMgr']['Homework']['From']?> : </td><td align="left" valign="top"><?=$from?></td></tr>
	<tr><td align="left" valign="top" width="100"><?=$Lang['SysMgr']['Homework']['To']?> : </td><td align="left" valign="top"><?=$to?></td></tr>
</table>
<br>
<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
	<?=$x?>
</table>

<?
include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_footer.php");
intranet_closedb();
?>