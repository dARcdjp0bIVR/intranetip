<?php
# using: henry
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");

intranet_auth();
intranet_opendb();

if(!$_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$linterface = new interface_html();
$lhomework = new libhomework2007();

if(is_array($typeID)) $typeID = $typeID[0];


$homeworkType = $lhomework->getHomeworkType($typeID);
list($typeID, $typeName, $displayOrder, $recordType, $recordStatus) = $homeworkType[0];

$CurrentPageArr['eAdminHomework'] = 1;

$CurrentPage = "Settings_HomeworkType";

# Left menu 
$TAGS_OBJ[] = array($Lang['SysMgr']['Homework']['HomeworkType']);

$PAGE_NAVIGATION[] = array($button_new, "");

$MODULE_OBJ = $lhomework->GET_MODULE_OBJ_ARR();

# Start layout
$linterface->LAYOUT_START();
?>

<script language="javascript">
<!--
function checkForm() {
	var obj = document.form1;
	if(obj.typeName.value=="") {
		alert("<?=$i_alert_pleasefillin." ".$i_Homework_HomeworkType?>");
		obj.typeName.select();	
		return false;
	} 
	if(obj.recordStatus[0].checked==false && obj.recordStatus[1].checked==false) {
		alert("<?=$i_alert_pleaseselect." ".$i_general_status?>");	
		obj.record_status[0].focus();
		return false;
	}
}
-->
</script>
<form name="form1" method="post" action="homeworkType_edit_update.php" onSubmit="return checkForm()">
<br />
<table width="96%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td valign="bottom"><?=$linterface->GET_NAVIGATION4($PAGE_NAVIGATION)?></td>
	</tr>
	<tr> 
		<td>
			<table width="96%" border="0" cellspacing="0" cellpadding="0" align="center">
				<tr>
					<td class="tabletext formfieldtitle" width="30%" valign="top"><?=$i_Homework_HomeworkType?> <span class="tabletextrequire">*</span></td>
					<td width="70%"><input type="text" name="typeName" id="typeName" size="40" MAXLENGTH="100" value="<?=$typeName?>"></td>
				</tr>
				<tr>
					<td class="tabletext formfieldtitle" width="30%" valign="top"><?=$i_general_status?> <span class="tabletextrequire">*</span></td>
					<td width="70%"><input type="radio" name="record_status" id="record_status1" value="1" <? if($recordStatus=="1") {echo " checked";} ?>><label for="record_status1"><?=$i_general_active?></label> <input type="radio" name="record_status" id="record_status0" value="0" <? if($recordStatus=="0") {echo " checked";} ?>><label for="record_status0"><?=$i_general_inactive?></label></td>
				</tr>
				<tr>
					<td><?=$toolbar?></td>
					<td align="right"><?=$linterface->GET_SYS_MSG($xmsg)?></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td height="1" class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
	</tr>
	<tr>
		<td align="right"><table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td align="center">
						<span class="dotline">
							<?= $linterface->GET_ACTION_BTN($button_submit, "submit")?> &nbsp;
							<?= $linterface->GET_ACTION_BTN($button_reset, "reset")?> &nbsp;
							<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "self.location.href='homeworkType.php?status=$status'")?>
						</span>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<input type="hidden" name="typeID" id="typeID" value="<?=$typeID?>" />


</form>
<br />
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
