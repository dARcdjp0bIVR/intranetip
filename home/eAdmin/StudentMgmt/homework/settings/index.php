<?php
# using: henry chow

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");

$lhomework = new libhomework2007();

if(!$_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");


$linterface = new interface_html();
$CurrentPageArr['eAdminHomework'] = 1;
$CurrentPage = "Settings_BasicSettings";


# build content table
$table_row = 0;
$table .= "<table width='100%' border='0' cellpadding='5' cellspacing='0'>";
$table .= "<tr>";
$table .= "<td class='tabletop'>". $eDiscipline['SettingName'] ."</td>";
$table .= "<td class='tabletop'>". $eDiscipline['Value'] ."</td>";
$table .= "</tr>";

$tablerow_class = "tablerow". (($table_row++%2 ==1) ? "2":"");
$table .= "<tr class='". $tablerow_class ."'>";
$table .= "<td>". $Lang['SysMgr']['Homework']['Disable'] ."</td>";
$table .= "<td>". ($lhomework->disabled ? $i_general_yes:$i_general_no) ."</td>";
$table .= "</tr>";

$tablerow_class = "tablerow". (($table_row++%2 ==1) ? "2":"");
$table .= "<tr class='". $tablerow_class ."'>";
$table .= "<td>". $Lang['SysMgr']['Homework']['TeacherSearchDisabled'] ."</td>";
$table .= "<td>". ($lhomework->teacherSearchDisabled ? $i_general_yes:$i_general_no) ."</td>";
$table .= "</tr>";

$tablerow_class = "tablerow". (($table_row++%2 ==1) ? "2":"");
$table .= "<tr class='". $tablerow_class ."'>";
$table .= "<td>". $Lang['SysMgr']['Homework']['SubjectSearchDisabled'] ."</td>";
$table .= "<td>". ($lhomework->subjectSearchDisabled ? $i_general_yes:$i_general_no) ."</td>";
$table .= "</tr>";

$tablerow_class = "tablerow". (($table_row++%2 ==1) ? "2":"");
$table .= "<tr class='". $tablerow_class ."'>";
$table .= "<td>". $Lang['SysMgr']['Homework']['StartFixed'] ."</td>";
$table .= "<td>". ($lhomework->startFixed ? $i_general_yes:$i_general_no) ."</td>";
$table .= "</tr>";

$tablerow_class = "tablerow". (($table_row++%2 ==1) ? "2":"");
$table .= "<tr class='". $tablerow_class ."'>";
$table .= "<td>". $Lang['SysMgr']['Homework']['ParentAllowed'] ."</td>";
$table .= "<td>". ($lhomework->parentAllowed ? $i_general_yes:$i_general_no) ."</td>";
$table .= "</tr>";

$tablerow_class = "tablerow". (($table_row++%2 ==1) ? "2":"");
$table .= "<tr class='". $tablerow_class ."'>";
$table .= "<td>". $Lang['SysMgr']['Homework']['NonTeachingAllowed'] ."</td>";
$table .= "<td>". ($lhomework->nonteachingAllowed ? $i_general_yes:$i_general_no) ."</td>";
$table .= "</tr>";

$tablerow_class = "tablerow". (($table_row++%2 ==1) ? "2":"");
$table .= "<tr class='". $tablerow_class ."'>";
$table .= "<td>". $Lang['SysMgr']['Homework']['AllowSubLeader'] ."</td>";
$table .= "<td>". ($lhomework->subjectLeaderAllowed ? $i_general_yes:$i_general_no) ."</td>";
$table .= "</tr>";

$tablerow_class = "tablerow". (($table_row++%2 ==1) ? "2":"");
$table .= "<tr class='". $tablerow_class ."'>";
$table .= "<td>". $Lang['SysMgr']['Homework']['AllowExport'] ."</td>";
$table .= "<td>". ($lhomework->exportAllowed ? $i_general_yes:$i_general_no) ."</td>";
$table .= "</tr>";

$tablerow_class = "tablerow". (($table_row++%2 ==1) ? "2":"");
$table .= "<tr class='". $tablerow_class ."'>";
$table .= "<td>". $Lang['SysMgr']['Homework']['AllowPast'] ."</td>";
$table .= "<td>". ($lhomework->pastInputAllowed ? $i_general_yes:$i_general_no) ."</td>";
$table .= "</tr>";

$tablerow_class = "tablerow". (($table_row++%2 ==1) ? "2":"");
$table .= "<tr class='". $tablerow_class ."'>";
$table .= "<td>". $Lang['SysMgr']['Homework']['DefaltHandinRequired']."</td>";
$table .= "<td>". ($lhomework->DeafultHandinRequired ? $i_general_yes:$i_general_no) ."</td>";
$table .= "</tr>";

$tablerow_class = "tablerow". (($table_row++%2 ==1) ? "2":"");
$table .= "<tr class='". $tablerow_class ."'>";
$table .= "<td>". $Lang['SysMgr']['Homework']['UseHomeworkCollect'] ."</td>";
$table .= "<td>". ($lhomework->useHomeworkCollect ? $i_general_yes:$i_general_no) ."</td>";
$table .= "</tr>";

$tablerow_class = "tablerow". (($table_row++%2 ==1) ? "2":"");
$table .= "<tr class='". $tablerow_class ."'>";
$table .= "<td>". $Lang['SysMgr']['Homework']['UseStartDateToGenerateList'] ."</td>";
$table .= "<td>". ($lhomework->useStartDateToGenerateList ? $i_general_yes:$i_general_no) ."</td>";
$table .= "</tr>";

$tablerow_class = "tablerow". (($table_row++%2 ==1) ? "2":"");
$table .= "<tr class='". $tablerow_class ."'>";
$table .= "<td>". $i_Homework_admin_use_homework_type ."</td>";
$table .= "<td>". ($lhomework->useHomeworkType ? $i_general_yes:$i_general_no) ."</td>";
$table .= "</tr>";

$tablerow_class = "tablerow". (($table_row++%2 ==1) ? "2":"");
$table .= "<tr class='". $tablerow_class ."'>";
$table .= "<td>". $Lang['eHomework']['OnlyCanEditDeleteOwn'] ."</td>";
$table .= "<td>". ($lhomework->OnlyCanEditDeleteOwn ? $i_general_yes:$i_general_no) ."</td>";
$table .= "</tr>";

$tablerow_class = "tablerow". (($table_row++%2 ==1) ? "2":"");
$table .= "<tr class='". $tablerow_class ."'>";
$table .= "<td>". $Lang['eHomework']['ClassTeacherCanViewHomeworkOnly'] ."</td>";
$table .= "<td>". ($lhomework->ClassTeacherCanViewHomeworkOnly ? $i_general_yes:$i_general_no) ."</td>";
$table .= "</tr>";

if ($sys_custom['eHomework_Status_Supplementary_WitHandInStatusManage'] &&
    !($sys_custom['eHomework_Status_Supplementary_WitTwoHandInStatusManage']))
{
	$tablerow_class = "tablerow". (($table_row++%2 ==1) ? "2":"");
	$table .= "<tr class='". $tablerow_class ."'>";
	$table .= "<td>". $Lang['eHomework']['SupplementaryCutOffTime'] ."</td>";
	$table .= "<td>". ($lhomework->SupplementaryCutOffTime ? $lhomework->SupplementaryCutOffTime:"12:00") ."</td>";
	$table .= "</tr>";
}
$table .= "</table>";


# Left menu 
$TAGS_OBJ[] = array($Lang['SysMgr']['Homework']['BasicSettings']);
$MODULE_OBJ = $lhomework->GET_MODULE_OBJ_ARR();

# Start layout
$linterface->LAYOUT_START();
?>

<form name="form1" method="post" action="edit.php">
<br />
<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
<tr>
	<td align="right" valign="bottom"><?=$linterface->GET_SYS_MSG($msg);?></td>
</tr>
<tr> 
	<td>
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td colspan="2"><?=$table?></td>
			</tr>
		</table>
	</td>
</tr>
<tr>
	<td height="1" class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
</tr>
<tr>
	<td align="right"><table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td align="center">
					<span class="dotline">
						<?= $linterface->GET_ACTION_BTN($button_edit, "submit")?>
					</span>
				</td>
			</tr>
		</table>
	</td>
</tr>
</table>


</form>
<br />
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>