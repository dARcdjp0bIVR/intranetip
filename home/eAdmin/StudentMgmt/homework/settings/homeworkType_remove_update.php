<?php
# using: henry
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");

intranet_auth();
intranet_opendb();

if(!$_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$lhomework = new libhomework2007();

if(is_array($typeID)) $typeID = $typeID[0];

$sql = "UPDATE INTRANET_HOMEWORK_TYPE SET RecordStatus='-1', DateModified=NOW() WHERE TypeID=$typeID";
$result = $lhomework->db_db_query($sql);

intranet_closedb();

$flag = ($result) ? "delete" : "delete_failed";

header("Location: homeworkType.php?xmsg=$flag");
?>
