<?
$PATH_WRT_ROOT = "../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");


intranet_auth();
intranet_opendb();

$arrCookies = array();
$arrCookies[] = array("ck_page_size", "numPerPage");
$arrCookies[] = array("ck_page_no", "pageNo");
$arrCookies[] = array("ck_page_order", "order");
//$arrCookies[] = array("ck_page_field", "field");


if(isset($clearCoo) && $clearCoo == 1)
{
	clearCookies($arrCookies);
}
else 
	updateGetCookies($arrCookies);

if ($page_size_change == 1)
{
    $page_size = $ck_page_size;
}
if($order == "") $order = 1;
if($field == "") $field = 0;
$pageSizeChangeEnabled = true;

$name_field = getNameFieldByLang("USR.");
$sql = "SELECT $name_field as UserName, LEFT(m.DateInput,10) as DateInput, CONCAT('<input type=checkbox name=userID[] id=userID[] value=\"',m.UserID,'\">') as checkbox FROM INTRANET_HOMEWORK_VIEWER_GROUP_MEMBER m INNER JOIN INTRANET_USER USR ON (USR.UserID=m.UserID)";

$li = new libdbtable2007($field, $order, $pageNo);

$li->sql = $sql;
$li->field_array = array("UserName", "DateInput");
$li->no_col = sizeof($li->field_array)+2;
$li->IsColOff = "IP25_table";


# TABLE COLUMN
$pos = 0;
$li->column_list .= "<th width='1'>#</th>\n";
$li->column_list .= "<th width='60%' class='tabletop tabletoplink'>".$li->column_IP25($pos++, $i_UserName)."</th>\n";
$li->column_list .= "<th width='40%' class='tabletop tabletoplink'>".$li->column_IP25($pos++, $i_Discipline_System_Access_Right_Added_Date)."</th>\n";
$li->column_list .= "<th width='1' class='tabletop tabletoplink'>".$li->check("userID[]")."</th>\n";

$x = $li->display();

$x .= '
<input type="hidden" name="pageNo" name="pageNo" value="'.$li->pageNo.'"/>
<input type="hidden" name="order" id="order" value="'.$li->order.'"/>
<input type="hidden" name="field" id="field" value="'.$li->field.'"/>
<input type="hidden" name="numPerPage" id="numPerPage" value="'.$li->page_size.'"/>
<!--<input type="hidden" name="page_size_change" id="page_size_change" value=""/>-->
';

echo $x;
intranet_closedb();

?>