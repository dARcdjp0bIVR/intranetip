<?php
# using: henry
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");

intranet_auth();
intranet_opendb();


if(!$_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$lhomework = new libhomework2007();

$maxOrder = $lhomework->getHomeworkTypeMaxOrder();
if($maxOrder=="" || $maxOrder=="0") {
	$maxOrder = "1";	
} else {
	$maxOrder = $maxOrder+1;	
}


$sql = "INSERT INTO INTRANET_HOMEWORK_TYPE (TypeName, DisplayOrder, RecordStatus, DateInput, DateModified) VALUES ('$typeName', '$maxOrder', '$record_status', NOW(), NOW())";
$result = $lhomework->db_db_query($sql);

intranet_closedb();

$flag = ($result) ? "add" : "add_failed";

header("Location: homeworkType.php?xmsg=$flag");
?>
