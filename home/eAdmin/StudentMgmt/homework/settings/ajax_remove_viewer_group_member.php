<?
$PATH_WRT_ROOT = "../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");


intranet_auth();
intranet_opendb();

$lhomework = new libhomework2007();

$result = $lhomework->removeViewerGroupMember($userID);

intranet_closedb();

if(sizeof($result) && in_array(false, $result)) 
	$msg = $Lang['General']['ReturnMessage']['DeleteUnsuccess'];
else 
	$msg = $Lang['General']['ReturnMessage']['DeleteSuccess'];

echo $msg;
?>