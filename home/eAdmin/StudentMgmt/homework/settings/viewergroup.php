<?php
# using: henry chow

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

if(!$_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}
//debug_pr($_POST);
### Cookies handling
# set cookies
/*
$arrCookies = array();
$arrCookies[] = array("ck_page_size", "numPerPage");
$arrCookies[] = array("ck_page_no", "pageNo");
$arrCookies[] = array("ck_page_order", "order");
//$arrCookies[] = array("ck_page_field", "field");


if(isset($clearCoo) && $clearCoo == 1)
{
	clearCookies($arrCookies);
}
else 
	updateGetCookies($arrCookies);
*/


if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
if($order == "") $order = 1;
if($field == "") $field = 0;
$pageSizeChangeEnabled = true;

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;


include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");

$linterface = new interface_html();
$CurrentPageArr['eAdminHomework'] = 1;
$CurrentPage = "Settings_ViewerGroup";
$lhomework = new libhomework2007();

# build content table


# Left menu 
$TAGS_OBJ[] = array($Lang['eHomework']['ViewerGroup']);
$MODULE_OBJ = $lhomework->GET_MODULE_OBJ_ARR();

# Start layout
$linterface->LAYOUT_START();
?>
<br />

<script language="javascript">
function Get_Group_Member_Table() {
	var PostVar = Get_Form_Values(document.getElementById("form1"));
	
	//PostVar = PostVar+"&field="+document.form1.field.value;
	//alert(PostVar);
	Block_Element("divGroupMember");
	$.post('ajax_get_viewer_group_member.php',
			{
				numPerPage:"<?=$numPerPage?>",
				pageNo:"<?=$pageNo?>",
				order:"<?=$order?>",
				field:"<?=$field?>",
				page_size_change:"<?=$page_size_change?>"
			},
					function(data){
						
						if (data == "die") 
							window.top.location = '/';
						else {
							$('div#divGroupMember').html(data);
							//Thick_Box_Init();
							UnBlock_Element("divGroupMember");
						}
					});
		
}


function checkRemove(obj,element,page) {
	var alertConfirmRemove = "<?=$i_Discipline_System_Discipline_Case_Record_Delete_Case_Delete?>";
    if(countChecked(obj,element)==0)
            alert(globalAlertMsg2);
    else{
		if(confirm(alertConfirmRemove)){
			RemoveUser();
		}
    }
	
}

function RemoveUser() {
	var PostVar = Get_Form_Values(document.getElementById("form1"));

	Block_Element("divGroupMember");
	$.post('ajax_remove_viewer_group_member.php',PostVar,
					function(data){
						if (data == "die") 
							window.top.location = '/';
						else {
							Get_Return_Message(data);
							Get_Group_Member_Table();
							UnBlock_Element("divGroupMember");
						}
					});
	
}

function goNew() {
	self.location.href = "viewergroup_member_add.php";	
}
</script>

<form name="form1" id="form1" method="post" action="">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr> 
		<td>
			<div class="Conntent_tool">
				<a href="javascript:;" onClick="goNew()" class="new"><?=$button_new?></a>
				<br style="clear:both" />
			</div>
			<div class="table_board">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td valign="bottom">
							<div class="common_table_tool"> 
								<a title="<?= $Lang['Btn']['Delete'] ?>" class="tool_delete" href="javascript:void(0);" onclick="javascript:checkRemove(document.form1,'userID[]','viewergroup_remove_member.php')" /><?= $Lang['Btn']['Delete'] ?></a>
							</div>
						</td>
					</tr>
				</table>
				<div id="divGroupMember"></div>
			</div>
		</td>
	</tr>
</table>
<!--
<input type="hidden" name="pageNo" name="pageNo" value="<?=$pageNo; ?>"/>
<input type="hidden" name="order" id="order" value="<?=$order; ?>"/>
<input type="hidden" name="field" id="field" value="<?=$field; ?>"/>
<input type="hidden" name="numPerPage" id="numPerPage" value="<?=$page_size?>"/>
-->
<input type="hidden" name="page_size_change" id="page_size_change" value="<?=$page_size_change?>"/>

</form>
<br />

<script language="javascript">
Get_Group_Member_Table();
<? if($msg!="") {
	echo "Get_Return_Message(\"$msg\")";	
}?>	
</script>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>