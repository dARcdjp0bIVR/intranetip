<?php
############################ Function Start ######################################

$IsPrimary6 = ($ReportID == 4);

# function to get Title Table
function GENERATE_TITLE_TABLE()
{
	global $ReportTitle, $image_path, $intranet_root, $PhotoLink;
	global $StudentInfoTable;

	# get school badge
	$SchoolLogo = GET_SCHOOL_BADGE();

	# get school name
	$SchoolName = GET_SCHOOL_NAME();

	$TitleTable = "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>\n";
	//$TitleTable .= "<tr><td width='120' align='center'>&nbsp;</td>";
	if(!empty($ReportTitle) || !empty($SchoolName))
	{
		list($ReportTitle1, $ReportTitle2) = explode("::", $ReportTitle);

		$TitleTable .= "<td valign='top'>";
		$TitleTable .= "<table width='100%' border='0' cellpadding='2' cellspacing='0' align='center'>\n";
		if(!empty($SchoolName))
			$TitleTable .= "<tr><td nowrap='nowrap' class='tc_report_title' align='center'><span style='padding-left: 120px'><strong>".strtoupper($SchoolName)."</strong></span></td></tr>\n";
		if(!empty($ReportTitle1))
			$TitleTable .= "<tr><td nowrap='nowrap' class='tc_report_title' align='center'><span style='padding-left: 120px'><strong>".strtoupper($ReportTitle1)."</strong></span></td></tr>\n";
		if(!empty($ReportTitle2))
			$TitleTable .= "<tr><td nowrap='nowrap' class='tc_report_title' align='center' colspan='2'><span style='padding-left: 120px'><strong>".strtoupper($ReportTitle2)."</strong></span></td></tr>\n";
		$TitleTable .= "</table>\n";
		$TitleTable .= "<br>".$StudentInfoTable;
		$TitleTable .= "</td>";
	}

	//if (is_file($intranet_root.$PhotoLink)) $StudentPhoto = "<img src='$PhotoLink' width='96' height='137'>";
	$TitleTable .= "<td width='120' align='center' valign='top'>
					<table border='0' class='pic'>
		              <tr>
		                <td>&nbsp;</td>
		              </tr>
		            </table>
					</td></tr>";
	//$TitleTable .= "<td width='120' align='center'>&nbsp;</td></tr>";
	$TitleTable .= "</table>";

	return $TitleTable;
}

# function to get Student Info Table
function GENERATE_STUDENT_INFO_TABLE($ParInfoArray)
{
	global $SettingStudentInfo, $StudentInfoTitleArray, $ClassTeacherArray, $today, $LangArray, $IssueDate;
	global $SubjectTitleType;

	list($ClassName, $ClassNumber, $StudentName, $RegNo, $ChiName, $EngName, $DOB, $Gender) = $ParInfoArray;
	
	if(!empty($SettingStudentInfo))
	{
		$count = 0;
		$StudentInfoTable = "<table width='100%' border='0' cellpadding='2' cellspacing='0' align='center'>\n";
				
		if (in_array("Name", $StudentInfoTitleArray)) $TempStudentInfoTitleArray[] = "Name";
		if (in_array("ClassName", $StudentInfoTitleArray)) $TempStudentInfoTitleArray[] = "ClassName";
		if (in_array("Name", $StudentInfoTitleArray)) $TempStudentInfoTitleArray[] = "Name";
		if (in_array("ClassNumber", $StudentInfoTitleArray)) $TempStudentInfoTitleArray[] = "ClassNumber";
		if (in_array("DateOfBirth", $StudentInfoTitleArray)) $TempStudentInfoTitleArray[] = "DateOfBirth";
		if (in_array("Gender", $StudentInfoTitleArray)) $TempStudentInfoTitleArray[] = "Gender";
		if (in_array("StudentNo", $StudentInfoTitleArray)) $TempStudentInfoTitleArray[] = "StudentNo";		
		if (in_array("DateOfIssue", $StudentInfoTitleArray)) $TempStudentInfoTitleArray[] = "DateOfIssue";
		//if (in_array("ClassTeacher", $StudentInfoTitleArray)) $TempStudentInfoTitleArray[] = "ClassTeacher";
				
		for($i=0; $i<sizeof($TempStudentInfoTitleArray); $i++)
		{
			$SettingID = trim($TempStudentInfoTitleArray[$i]);
			if($SettingStudentInfo[$SettingID]==1)
			{
				$Title = $LangArray[$SettingID];
				$Title = str_replace("<br />", " ", $Title);
				$Display = "";
				
				switch($SettingID)
				{
					case "Name":
						$Display = ($count == 0) ? $EngName : "<span style='font-family: 標楷體;'>".$ChiName."</span>";
						if ($SubjectTitleType==1) {				
							$Title = ($count == 0) ? "Name in English" : "Name in Chinese";
						} else {
							$Title = ($count == 0) ? "英文姓名" : "中名姓名";
						}
						break;
					case "ClassName":
						$Display = $ClassName;
						break;
					case "ClassTeacher":
						$Display = (!empty($ClassTeacherArray[$ClassName])) ? implode(", ", $ClassTeacherArray[$ClassName]) : "";
						break;
					case "DateOfIssue":
						$Display = date('d-m-Y', strtotime($IssueDate));
						break;
					case "StudentNo":
						$Display = $RegNo;
						break;
					case "ClassNumber":
						$Display = $ClassNumber;
						break;
					case "DateOfBirth":
						$Display = date('d-m-Y', strtotime($DOB));
						break;
					case "Gender":
						$Display = $Gender;
						break;
				}
				$width = "35%";
				if($count%2==0) {
					$StudentInfoTable .= "<tr>\n";
					$width = "65%";
				}
				if ($SettingID == "DateOfIssue") $StudentInfoTable .= "<td>&nbsp;</td></tr><tr><td>&nbsp;</td>";
				$StudentInfoTable .= "<td class='student_info_tabletext' width='20%' valign='top' >".$Title." : ".$Display."</td>\n";				
				if($count%2==1) {
					$StudentInfoTable .= "</tr>\n";
				}
				
				

				$count++;
			}
		}		
		
		
		/*
		for($i=0; $i<sizeof($StudentInfoTitleArray); $i++)
		{
			$SettingID = trim($StudentInfoTitleArray[$i]);
			if($SettingStudentInfo[$SettingID]==1)
			{
				$Title = $LangArray[$SettingID];
				$Title = str_replace("<br />", " ", $Title);
				$Display = "";
				switch($SettingID)
				{
					case "Name":
						$Display = ($count == 0) ? $EngName : $ChiName;
						if ($_SESSION['intranet_session_language'] == "en") {				
							$Title = ($count == 0) ? "Name in English" : "Name in Chinese";
						} else {
							$Title = ($count == 0) ? "英文姓名" : "中名姓名";
						}
						break;
					case "ClassName":
						$Display = $ClassName;
						break;
					case "ClassTeacher":
						$Display = (!empty($ClassTeacherArray[$ClassName])) ? implode(", ", $ClassTeacherArray[$ClassName]) : "";
						break;
					case "DateOfIssue":
						$Display = date('d-m-Y', strtotime($IssueDate));
						break;
					case "StudentNo":
						$Display = $RegNo;
						break;
					case "ClassNumber":
						$Display = $ClassNumber;
						break;
					case "DateOfBirth":
						$Display = date('d-m-Y', strtotime($DOB));
						break;
					case "Gender":
						$Display = $Gender;
						break;
				}

				if($count%2==0) {
					$StudentInfoTable .= "<tr>\n";
				}
				$StudentInfoTable .= "<td class='student_info_tabletext' width='20%' valign='top' >".$Title." : ".$Display."</td>\n";
				if($count%2==1) {
					$StudentInfoTable .= "</tr>\n";
				}
				
				

				$count++;
			}
		}		
		*/
		
		
		/*
				if($count%3==0) {
					$StudentInfoTable .= "<tr>\n";
				}
				$TempColspan = ($count == 0) ? 2 : 1;
				if ($count == 0) $count++;
				$StudentInfoTable .= "<td class='student_info_tabletext' width='20%' valign='top' colspan='$TempColspan'>".$Title." : ".$Display."</td>\n";
				if($count%3==2) {
					$StudentInfoTable .= "</tr>\n";
				}
				*/
		
		/*
		$StudentInfoTable .= "<tr class='tabletext'>";
		$StudentInfoTable .= "<td>$NameTitle</td>";
		$StudentInfoTable .= "<td>$StudentName</td>";
		$StudentInfoTable .= "<td></td>";
		$StudentInfoTable .= "<td></td>";
		$StudentInfoTable .= "<td>$ClassNameTitle</td>";
		$StudentInfoTable .= "<td>$ClassName</td>";
		$StudentInfoTable .= "</tr>";
		$StudentInfoTable .= "<tr class='tabletext'>";
		$StudentInfoTable .= "<td>$NameTitle</td>";
		$StudentInfoTable .= "<td>$StudentName</td>";
		$StudentInfoTable .= "<td>$DateOfBirthTitle</td>";
		$StudentInfoTable .= "<td>$DateOfBirth</td>";
		$StudentInfoTable .= "<td>$GenderTitle</td>";
		$StudentInfoTable .= "<td>$Gender</td>";
		$StudentInfoTable .= "</tr>";
		$StudentInfoTable .= "<tr class='tabletext'>";
		$StudentInfoTable .= "<td>$StudentNoTitle</td>";
		$StudentInfoTable .= "<td>$StudentNo</td>";
		$StudentInfoTable .= "<td>$ClassNumberTitle</td>";
		$StudentInfoTable .= "<td>$ClassNumber</td>";
		$StudentInfoTable .= "<td>$DateOfIssueTitle</td>";
		$StudentInfoTable .= "<td>$DateOfIssue</td>";
		$StudentInfoTable .= "</tr>";
		*/
		$StudentInfoTable .= "</table>\n";
	}

	return $StudentInfoTable;
}

# Generate a simple cell for holding data
function GENERATE_MISC_CELL($title, $arr, $content="") {
	if ($content == "" && $arr != "") {
		for($i=0; $i<sizeof($arr); $i++) {
			$content .= $arr[$i][0]."<br />";
		}
	}
	return $content;
}

# Generate the ECA data
function GENERATE_ECA_CONTENT($ecaArr) {
	for($i=0; $i<sizeof($ecaArr); $i++) {
		$x .= $ecaArr[$i]["ECA"];
		if (isset($ecaArr[$i]["Grade"])) {
			$x .= " (".$ecaArr[$i]["Grade"].")";
		}
		$x .= "<br />";
	}
	return $x;
}

# function to get MISC Table
function GENERATE_MISC_TABLE($ParClassTeacherComment, $ParStudentID)
{
	global $SettingMiscArray, $MiscTitleArray, $Semester, $LangArray;
	global $StudentAwardArray, $StudentMeritArray, $StudentECAArray, $StudentRemarkArray;
	global $StudentInterSchoolArray, $StudentSchoolServiceArray, $SettingSummaryArray, $StudentMeritArray;

	$ParStudentID = trim($ParStudentID);
	
	if(!empty($SettingMiscArray))
	{
		$IsFirst = 1;
		$count = 0;
		$MiscTable = "<table width='100%' height='100%' border='0' cellpadding='2' cellspacing='0' align='center' class='report_border'>";
		
		if ($SettingSummaryArray["Merit"] || $SettingSummaryArray["Demerit"])
		{
			$MiscTable .= "<tr><td width='100%' align='center' height='2%'>";
			$MiscTable .= "<span class='small_title'>".$LangArray['MeritsAndDemerits']."</span></td></tr>";
			
			$MeritDisplay = ($StudentMeritArray[$ParStudentID][$Semester][0]["Merit"]=="") ? "" : $StudentMeritArray[$ParStudentID][$Semester][0]["Merit"];
			$DemeritDisplay = ($StudentMeritArray[$ParStudentID][$Semester][0]["Demerit"]=="") ? "" : $StudentMeritArray[$ParStudentID][$Semester][0]["Demerit"];
			
			$unit = $LangArray['merits'];
			
			if (($MeritDisplay=="")||($MeritDisplay=="---")) {
				//$MeritDisplay = "---";
			} else {
				$MeritDisplay = $MeritDisplay." ".$LangArray['Merit'];
			}
			if (($DemeritDisplay=="")||($DemeritDisplay=="---")) {
				//$DemeritDisplay = "---";
			} else {
				$DemeritDisplay = $DemeritDisplay." ".$LangArray['Demerit'];
			}
			
			if (($MeritDisplay == "")&&($DemeritDisplay == "")) $MeritDisplay = "---<br>";
			
			$MiscTable .= "<tr><td class='tabletext border_top' valign='top' style='padding-left:10px'>";
			$MiscTable .= $MeritDisplay."<br>";
			$MiscTable .= $DemeritDisplay;
			$MiscTable .= "</td></tr>";
		}
		
		$MiscTable .= "<tr>";
		$MiscTable .= "<td width='100%' align='center' height='2%' class='border_top'>";
		$MiscTable .= "<span class='small_title'>".$LangArray['ECA']."</span></td></tr>";
		$MiscTable .= "<tr><td class='border_top small_text' height='100%' valign='top' style='padding-left:10px'>";
		
		for($i=0; $i<sizeof($MiscTitleArray); $i++)
		{
			$SettingID = trim($MiscTitleArray[$i]);
			if($SettingMiscArray[$SettingID]==1 && $SettingID!="SubjectTeacherComment" && $SettingID!="MeritsAndDemerits"
				&& $SettingID != "Remark"
			)
			{
				$Title = $LangArray[$SettingID];
				//$Display = "";
				switch($SettingID)
				{
					case "ClassTeacherComment":
						$Display["ClassTeacherComment"] = GENERATE_MISC_CELL($Title, nl2br($ParClassTeacherComment));
						break;
					case "Awards":
						$Display["Awards"] = GENERATE_MISC_CELL($Title, $StudentAwardArray[$ParStudentID][$Semester]);
						break;
					case "ECA":
						$Display["ECA"] = GENERATE_MISC_CELL($Title, "", GENERATE_ECA_CONTENT($StudentECAArray[$ParStudentID][$Semester]));
						break;
					case "Remark":
						$Display["Remark"] = GENERATE_MISC_CELL($Title, $StudentRemarkArray[$ParStudentID][$Semester]);
						break;
					case "InterSchoolCompetition":
						$Display["InterSchoolCompetition"] = GENERATE_MISC_CELL($Title, $StudentInterSchoolArray[$ParStudentID][$Semester]);
						break;
					case "SchoolService":
						$Display["SchoolService"] = GENERATE_MISC_CELL($Title, $StudentSchoolServiceArray[$ParStudentID][$Semester]);
						break;
				}
			}
		}
		
		foreach ($Display as $key => $value) {
				$TempMiscTable .= $value;
		}

		//$TempMiscTable = str_replace("<br />", "", $TempMiscTable);
		if ($TempMiscTable == "") $TempMiscTable = "---";
		
		$MiscTable .= $TempMiscTable."</td></tr></table>";
	}

	return $MiscTable;
}

function RETURN_GRADE($ParGrade)
{
	$ReturnGrade = str_replace("-", "<span style='font-size: 12pt'><sup>-</sup></span>", $ParGrade);
	$ReturnGrade = str_replace("+", "<span style='font-size: 10pt'><sup>+</sup></span>", $ReturnGrade);	
	return $ReturnGrade;
}


# function to get REMARK Table
function GENERATE_REMARK_TABLE($ParClassTeacherComment, $ParStudentID)
{
	global $SettingMiscArray, $MiscTitleArray, $Semester, $LangArray;
	global $StudentAwardArray, $StudentMeritArray, $StudentECAArray, $StudentRemarkArray;
	global $StudentInterSchoolArray, $StudentSchoolServiceArray;
	
	$ParStudentID = trim($ParStudentID);
	if(!empty($SettingMiscArray))
	{
		$IsFirst = 1;
		$count = 0;
		$RemarkTable = "<table width='100%' border='0' cellpadding='2' cellspacing='0' align='center'>";
		for($i=0; $i<sizeof($MiscTitleArray); $i++)
		{
			$SettingID = trim($MiscTitleArray[$i]);
			if($SettingMiscArray[$SettingID]==1 && $SettingID!="SubjectTeacherComment" &&
				$SettingID!="MeritsAndDemerits" && $SettingID!="ClassTeacherComment" &&
				$SettingID!="Awards" && $SettingID!="ECA" &&
				$SettingID!="InterSchoolCompetition" && $SettingID!="SchoolService"
				)
			{
				$Title = $LangArray[$SettingID];
				$Display = "";
				switch($SettingID)
				{
					case "Remark":
						$Display = $StudentRemarkArray[$ParStudentID][$Semester][0]["Remark"];
						break;
				}
				$RemarkTable .= "<tr>";
				if($IsFirst==1) {
					$IsFirst = 0;
				}
				else {
					$top_border_style = "class='border_top'";
				}
				$RemarkTable .= "<td width='100%' {$top_border_style}>";
				$RemarkTable .= "<span class='small_title'>".$Title."</span>";
				$RemarkTable .= "<table width='100%' border='0' cellpadding='2' cellspacing='0' align='center'>
								<tr><td class='tabletext' valign='top'>".$Display."</td></tr>
								</table>";
				$RemarkTable .= "</td>";
				$RemarkTable .= "</tr>";

				$count++;
			}
		}
		$RemarkTable .= "</table>";
	}

	if ($RemarkTable == "<table width='100%' border='0' cellpadding='2' cellspacing='0' align='center'></table>") $RemarkTable = "";
	
	return $RemarkTable;
}

# function to get summary display
function RETURN_SUMMARY_DISPLAY($ParInfoArray, $ParSettingID, $ClassHighestAverage, $StudentSummaryArray, $ParColumnSem)
{
	global $StudentMeritArray, $StudentID, $libreportcard, $ClassLevelID;
	global $FailedArray, $DistinctionArray, $PFArray;
	
	$GradeMarkArr = $libreportcard->GET_GRADEMARK_BY_SCHEMEID(6);
	
	list($GT, $AM, $GPA, $OMC, $OMCT, $OMF, $OMFT) = $ParInfoArray;
	
	switch($ParSettingID)
	{
		case "GrandTotal":
				if (($ClassLevelID != 5)&&($ClassLevelID != 6)) {
					$Display = $GT;									
					list($StylePrefix, $StyleSuffix, $IsDistinct, $IsFailed) = $libreportcard->GET_STYLE("S", $AM, $FailedArray[8], $DistinctionArray[8], $PFArray[8]);
					if ($IsFailed) $Display = "(".$Display.")";	
				} else {
					$Display = $GT;
					if ($Semester!="FULL") $Display = "---";
				}
				break;
		case "GPA":
				$Display = $GPA;
				break;
		case "AverageMark":				
				if (($ClassLevelID != 5)&&($ClassLevelID != 6)) {
					$Display = $AM;					
					list($StylePrefix, $StyleSuffix, $IsDistinct, $IsFailed) = $libreportcard->GET_STYLE("S", $AM, $FailedArray[8], $DistinctionArray[8], $PFArray[8]);
					if ($IsFailed) $Display = "(".$Display.")";					
				} else {
					$Display = $libreportcard->GET_CONVERTED_GRADE($GradeMarkArr, $GradeMarkArr[sizeof($GradeMarkArr)-1][1], $AM);
					list($StylePrefix, $StyleSuffix, $IsDistinct, $IsFailed) = $libreportcard->GET_STYLE("G", $Display, $FailedArray[8], $DistinctionArray[8], $PFArray[8]);
					if ($IsFailed) $Display = "(".$Display.")";
				}				
				break;
		case "FormPosition":
				$Display = ($OMF>0) ? $OMF : "---";
				break;
		case "ClassPosition":
				$Display = ($OMC>0) ? $OMC : "---";
				break;
		case "ClassPupilNumber":
				$Display = $OMCT;
				break;
		case "FormPupilNumber":
				$Display = $OMFT;
				break;
		case "ClassHighestAverage":
				$Display = $ClassHighestAverage;
				break;
		########################
		case "Conduct":
				$Display = ($StudentSummaryArray[0]["Conduct"]=="") ? "---" : $StudentSummaryArray[0]["Conduct"];
				break;
		case "Politeness":
				$Display = ($StudentSummaryArray[0]["Politeness"]=="") ? "---" : $StudentSummaryArray[0]["Politeness"];
				break;
		case "Behaviour":
				$Display = ($StudentSummaryArray[0]["Behaviour"]=="") ? "---" : $StudentSummaryArray[0]["Behaviour"];
				break;
		case "Application":
				$Display = ($StudentSummaryArray[0]["Application"]=="") ? "---" : $StudentSummaryArray[0]["Application"];
				break;
		case "Tidiness":
				$Display = ($StudentSummaryArray[0]["Tidiness"]=="") ? "---" : RETURN_GRADE($StudentSummaryArray[0]["Tidiness"]);
				break;
		case "Motivation":
				$Display = ($StudentSummaryArray[0]["Motivation"]=="") ? "---" : RETURN_GRADE($StudentSummaryArray[0]["Motivation"]);
				break;				
		case "SelfConfidence":
				$Display = ($StudentSummaryArray[0]["SelfConfidence"]=="") ? "---" : RETURN_GRADE($StudentSummaryArray[0]["SelfConfidence"]);
				break;
		case "SelfDiscipline":
				$Display = ($StudentSummaryArray[0]["SelfDiscipline"]=="") ? "---" : RETURN_GRADE($StudentSummaryArray[0]["SelfDiscipline"]);
				break;
		case "Courtesy":
				$Display = ($StudentSummaryArray[0]["Courtesy"]=="") ? "---" : RETURN_GRADE($StudentSummaryArray[0]["Courtesy"]);
				break;
		case "Honesty":
				$Display = ($StudentSummaryArray[0]["Honesty"]=="") ? "---" : RETURN_GRADE($StudentSummaryArray[0]["Honesty"]);
				break;
		case "Responsibility":
				$Display = ($StudentSummaryArray[0]["Responsibility"]=="") ? "---" : RETURN_GRADE($StudentSummaryArray[0]["Responsibility"]);
				break;
		case "Cooperation":
				$Display = ($StudentSummaryArray[0]["Cooperation"]=="") ? "---" : RETURN_GRADE($StudentSummaryArray[0]["Cooperation"]);
				break;
		########################
		case "DaysAbsent":
				$Display = ($StudentSummaryArray[0]["DaysAbsent"]=="") ? "---" : $StudentSummaryArray[0]["DaysAbsent"];
				break;
		case "TimesLate":
				$Display = ($StudentSummaryArray[0]["TimesLate"]=="") ? "---" : $StudentSummaryArray[0]["TimesLate"];
				break;
		case "AbsentWOLeave":
				$Display = ($StudentSummaryArray[0]["AbsentWOLeave"]=="") ? "---" : $StudentSummaryArray[0]["AbsentWOLeave"];
				break;
		
		case "Merit":
				$Display = ($StudentMeritArray[$StudentID][$ParColumnSem][0]["Merit"]=="") ? "---" : $StudentMeritArray[$StudentID][$ParColumnSem][0]["Merit"];
				break;
		case "Demerit":
				$Display = ($StudentMeritArray[$StudentID][$ParColumnSem][0]["Demerit"]=="") ? "---" : $StudentMeritArray[$StudentID][$ParColumnSem][0]["Demerit"];
				break;
	}
	return $Display;
}

# function get summary array
function GET_SUMMARY_ARRAY($ParInfoArray, $ParClassHighestAverage, $ParSummaryArray)
{
	global $SummaryTitleArray, $SettingSummaryArray, $ShowFullMark, $Semester, $ColumnArray, $LangArray, $SettingArray;

	$HideOverallResult = $SettingArray["HideOverallResult"];
	$ReturnArray = "";
	
	if(!empty($SettingSummaryArray))
	{
		$count = 0;
		for($k=0; $k<sizeof($SummaryTitleArray); $k++)
		{
			$SettingID = trim($SummaryTitleArray[$k]);
			//if($SettingSummaryArray[$SettingID]==1 && $SettingID!="DaysAbsent" && $SettingID!="TimesLate" && $SettingID!="AbsentWOLeave")
			//if($SettingSummaryArray[$SettingID]==1)
			if ($SettingSummaryArray[$SettingID]==1 &&
			  	$SettingID!="DaysAbsent" && $SettingID!="TimesLate" && $SettingID!="AbsentWOLeave" &&
				$SettingID!="Merit" && $SettingID!="Demerit" && $SettingID!="Conduct" &&
				$SettingID!="Politeness" && $SettingID!="Behaviour" && $SettingID!="Application" &&
				$SettingID!="Tidiness" &&
				
				$SettingID!="Motivation" &&$SettingID!="SelfConfidence" &&$SettingID!="SelfDiscipline" &&
				$SettingID!="Courtesy" &&$SettingID!="Honesty" &&$SettingID!="Responsibility" &&
				$SettingID!="Cooperation"
				)
			{
				$Title = $LangArray[$SettingID];
				$ReturnArray[$count]["Title"] = $Title;
				for($i=0; $i<sizeof($ColumnArray); $i++)
				{
					$column = trim($ColumnArray[$i]["ColumnTitle"]);
										
					if(!empty($ParInfoArray[$column]))
					{
						$Display = RETURN_SUMMARY_DISPLAY($ParInfoArray[$column], $SettingID, $ParClassHighestAverage[$column], $ParSummaryArray[$column], $column);
						$ReturnArray[$count][$column] = $Display;
					}
				}
				if(!($Semester=="FULL" && $HideOverallResult==1))
				{
					$Display = RETURN_SUMMARY_DISPLAY($ParInfoArray[$Semester], $SettingID, $ParClassHighestAverage[$Semester], $ParSummaryArray[$Semester], $Semester);
					$ReturnArray[$count][$Semester] = $Display;
				}
				$count++;
			}
		}
	}
	return $ReturnArray;
}

# function get summary array
function GET_CONDUCT_ARRAY($ParInfoArray, $ParClassHighestAverage, $ParSummaryArray)
{
	global $SummaryTitleArray, $SettingSummaryArray, $ShowFullMark, $Semester, $ColumnArray, $LangArray, $SettingArray;

	$HideOverallResult = $SettingArray["HideOverallResult"];
	$ReturnArray = "";

	if(!empty($SettingSummaryArray))
	{
		$count = 0;
		for($k=0; $k<sizeof($SummaryTitleArray); $k++)
		{
			$SettingID = trim($SummaryTitleArray[$k]);
			//if($SettingSummaryArray[$SettingID]==1 && $SettingID!="DaysAbsent" && $SettingID!="TimesLate" && $SettingID!="AbsentWOLeave")
			//if($SettingSummaryArray[$SettingID]==1)
			if($SettingSummaryArray[$SettingID]==1 &&
				$SettingID!="GrandTotal" && $SettingID!="GPA" && $SettingID!="AverageMark" &&
				$SettingID!="ClassHighestAverage" && $SettingID!="ClassPupilNumber" && 
				$SettingID!="ClassPosition" && $SettingID!="FormPosition" && $SettingID!="FormPupilNumber" && $SettingID!="Merit" && $SettingID!="Demerit"
				)
			{
				$Title = $LangArray[$SettingID];
				$ReturnArray[$count]["Title"] = $Title;
				
				for($i=0; $i<sizeof($ColumnArray); $i++)
				{
					$column = trim($ColumnArray[$i]["ColumnTitle"]);
					
					if(!empty($ParInfoArray[$column]))
					{
						$Display = RETURN_SUMMARY_DISPLAY($ParInfoArray[$column], $SettingID, $ParClassHighestAverage[$column], $ParSummaryArray[$column], $column);
						$ReturnArray[$count][$column] = $Display;
					}
				}
				if(!($Semester=="FULL" && $HideOverallResult==1))
				{
					$Display = RETURN_SUMMARY_DISPLAY($ParInfoArray[$Semester], $SettingID, $ParClassHighestAverage[$Semester], $ParSummaryArray[$Semester], $Semester);
					$ReturnArray[$count][$Semester] = $Display;
				}
				$count++;
			}
		}
	}
	return $ReturnArray;
}

# function to get signature table
function GENERATE_SIGNATURE_TABLE()
{
	global $eReportCard, $SettingArray, $LangArray;
	global $SignatureWidth;

	$SignatureTitleArray = $eReportCard['DisplaySettingsArray']["Signature"];
	$SettingSignatureArray = $SettingArray["Signature"];

	$CellNumber = 0;
	$SignatureRow = "";
	for($i=0; $i<sizeof($SignatureTitleArray); $i++)
	{
		$SettingID = trim($SignatureTitleArray[$i]);
		if($SettingSignatureArray[$SettingID]==1)
		{
			$Title = $LangArray[$SettingID];
			$SignatureRow .= "<tr>";
			$SignatureRow .= "<td class='small_title border_top' valign='top' height='75' width='25%'><span style='width:{$SignatureWidth}px'>".$Title."</span></td>";
			$SignatureRow .= "<td class='small_title border_top border_left'>&nbsp;</td>";
			$SignatureRow .= "</tr>";

			$CellNumber++;
		}
	}
	if($CellNumber>0)
	{
		$SignatureTable = "<table width='100%' height='100%' border='0' cellpadding='2' cellspacing='0' align='center' valign='top'>";
		$SignatureTable .= $SignatureRow;
		$SignatureTable .= "</table>";
	}

	return $SignatureTable;
}

# function to get signature table
function GENERATE_FOOTERREMARKS_TABLE()
{
	
	// class='report_border'
	$ReturnTable = "<table width='100%' border='0' cellpadding='2' cellspacing='0' align='center' valign='top'>";
	$ReturnTable .= "<tr class='tabletext'><td valign='top' width='80'>Remarks";
	$ReturnTable .= "<td>";
	
		$ReturnTable .= "<table width='100%' border='0' cellpadding='2' cellspacing='0' align='center' valign='top'>";
		$ReturnTable .= "<tr class='tabletext'><td width='20'>1.</td><td>Grading System:";
		$ReturnTable .= "</td></tr>";
		$ReturnTable .= "<tr class='tabletext'><td></td><td align='left'>";
	
			$ReturnTable .= "<table border='0' cellpadding='2' cellspacing='0' align='left' valign='top' class='report_border'>";
			$ReturnTable .= "<tr class='tabletext'>
								<td width='80' align='center'>Marks</td>
								<td width='80' align='center' class='border_left'>90 - 100</td>
								<td width='80' align='center' class='border_left'>75 - 89</td>
								<td width='80' align='center' class='border_left'>60 - 74</td>
								<td width='80' align='center' class='border_left'>30 - 59</td>
								<td width='80' align='center' class='border_left'>Below 30</td>
							</tr>";
			$ReturnTable .= "<tr class='tabletext'>
								<td align='center' class='border_top'>Grade</td>
								<td align='center' class='border_left border_top'>A</td>
								<td align='center' class='border_left border_top'>B</td>
								<td align='center' class='border_left border_top'>C</td>
								<td align='center' class='border_left border_top'>D</td>
								<td align='center' class='border_left border_top'>E</td>
							</tr>";
			$ReturnTable .= "</table>";	
	
		$ReturnTable .= "</td></tr>";
		$ReturnTable .= "<tr class='tabletext'><td></td><td>&nbsp;</td></tr>";
		$ReturnTable .= "<tr class='tabletext'><td>2.</td><td>Conduct Assessment: A to D with sub-grades";
		$ReturnTable .= "</td></tr>";
		$ReturnTable .= "</table>";
	
	
	$ReturnTable .= "</td></tr>";
	$ReturnTable .= "</table>";

	return $ReturnTable;
}

function GET_SEMESTER_NUMBER($ParSemester)
{
	global $libreportcard, $SemesterArray;

	$sem_number = "-1";
	if(trim($ParSemester)=="FULL")
	{
		$sem_number = 0;
	} else
	{
		for($i=0; $i<sizeof($SemesterArray); $i++)
		{
			if(trim($ParSemester)==trim($SemesterArray[$i]))
			{
				$sem_number = $i+1;
				break;
			}
		}
	}
	return $sem_number;
}

function CHECK_CSV_FILE_FORMAT($ParHeaderArray, $ParDefaultHeaderArray)
{
	# Check Title Row
	$format_wrong = false;

	for ($i=0; $i<sizeof($ParDefaultHeaderArray); $i++)
	{
		if ($ParHeaderArray[$i]!=$ParDefaultHeaderArray[$i])
		{
			$format_wrong = true;
			break;
		}
	}

	return $format_wrong;
}

function GET_CSV_FILE_CONTENT($ParFile, $ParDefaultHeaderArray, $ReportType="")
{
	global $libreportcard, $lf, $StudentRegNoArray, $SemesterArray;
	
	######### For adding associated array ##########
	global $HeaderSettingMap, $CustomSettingMap;
	################################################
	
	$ClassArr = $libreportcard->GET_CLASSES();
	
	$TargetArray = $SemesterArray;
	$TargetArray[] = "FULL";
	for($i=0; $i<sizeof($TargetArray); $i++)
	{
		$t_semester = trim($TargetArray[$i]);
		$sem_number = GET_SEMESTER_NUMBER($t_semester);
		if($sem_number>=0)
		{
			for ($j = 0; $j < sizeof($ClassArr); $j++)
			{
				$TempClass = str_replace(".", "", str_replace("/", "", $ClassArr[$j]));
				$TargetFile = $ParFile."/".trim($libreportcard->Year)."_".$sem_number."_".$TempClass.".csv";
				if(file_exists($TargetFile))
				{
					$data = $lf->file_read_csv($TargetFile);
					if(!empty($data))
					{
						$header_row = array_shift($data);
						$wrong_format = CHECK_CSV_FILE_FORMAT($header_row, $ParDefaultHeaderArray);
						#########################################
						# Ignore wrong format checking and force
						# assign associated array
						$wrong_format = false;
						#########################################
						if(!$wrong_format)
						{
							for($k=0; $k<sizeof($data); $k++)
							{
								$reg_no = array_shift($data[$k]);
								$reg_no = trim(str_replace("#", "", $reg_no));
								$student_id = trim($StudentRegNoArray[$reg_no]);
								if($student_id!="")
								{
									// add associated array to $data also
									if ($ReportType != "") {
										for($r=0; $r<sizeof($data[$k]); $r++) {
											# assign key name using display wording
											$data[$k][$CustomSettingMap[$ReportType][$header_row[$r+1]]] = $data[$k][$r];
										}
									}
									$ReturnArray[$student_id][$t_semester][] = $data[$k];
								}
							}
						}
					}
				}
			}
		}
	}
	return $ReturnArray;
}

# function to get result table
function GENERATE_RESULT_TABLE($ParSignatureTable, $ParAttendanceMeritTable, $ParStudentResultArray, $ParSubjectTeacherCommentArray)
{
	global $eReportCard, $SubjectArray, $ColumnArray, $ReportCellSetting, $ShowFullMark, $libreportcard;
	global $FailedArray, $DistinctionArray, $PFArray, $SettingArray;
	global $AllSubjectArray, $ShowOverallPosition, $OverallPositionRange, $HideNotEnrolled;
	global $ShowSubjectTeacherComment, $Semester;
	global $SummaryResultArray, $LangArray, $BilingualReportTitle, $ConductResultArray;
	global $LineHeight, $RemarkTable, $MiscTable;
	global $SubjectGradingArray, $libreportcard, $ClassLevelID;
	global $IsPrimary6;

	# Report Type
	# 1 - Display the overall result of Parent Subject AND Component Subject(s)
	# 2 - Display the overall result of Parent Subject ONLY
	# 3 - Display the overall result of Component Subject(s) ONLY
	$ResultDisplayType = $SettingArray["ResultDisplayType"];
	$ResultCalculationType = $SettingArray["ResultCalculationType"];
	$SubjectTitleType = $SettingArray["SubjectTitleType"];
	$ShowColumnPercentage = $SettingArray["ShowColumnPercentage"];
	$MarkTypeDisplay = $SettingArray["MarkTypeDisplay"];
	$HideOverallResult = $SettingArray["HideOverallResult"];

	# get subject title cell
	if($BilingualReportTitle==1 || $SubjectTitleType==0) {
		$SubjectTitleCell = "<td width='50%' class='small_title'>".$LangArray['EngSubject']."</td><td width='50%' class='small_title'>".$LangArray['ChiSubject']."</td>";
	}
	else {
		$SubjectTitleCell = "<td class='small_title'>".$LangArray['Subject']."</td>";
	}

	if ($IsPrimary6) {
		$TempSubjectCell = "<table width='100%' height='100%' border='0' cellpadding='2' cellspacing='0'>
							<tr valign='middle' align='center' height='40'><td width='20'>&nbsp;</td>".$SubjectTitleCell."<td valign='top' width='20' class='small_title'>%</td></tr>
							</table>
							";
		/*
		$TempSubjectCell = "<table width='100%' height='100%' border='0' cellpadding='2' cellspacing='0'>
							<tr valign='top' height='20'><td align='right' class='small_title' colspan='2'>%</td></tr>
							<tr valign='bottom' height='20' align='center'>".$SubjectTitleCell."</tr>
							</table>
							";
		*/
	} else {
		$TempSubjectCell = "<table width='100%' height='100%' border='0' cellpadding='2' cellspacing='0'>
							<tr valign='middle' align='center' height='40'>".$SubjectTitleCell."</tr>
							</table>
							";
	}
	
	$ExtraColumn = 0;
	$ColumnSize = sizeof($ColumnArray);
	$ColumnWidth = round(100/($ColumnSize+3));
	$ResultTable = "<table width='100%' border='0' cellspacing='0' cellpadding='0' class='bg'><tr><td colspan='3' height='5'></td></tr>";
	$ResultTable .= "<tr><td width='67%' class='report_contenttitle'>&nbsp;&nbsp;&nbsp;".$LangArray['AcademicResults'];	
	$ResultTable .= "<td width='3%'></td><td valign='top' class='report_contenttitle'>&nbsp;&nbsp;&nbsp;".$LangArray['ConductAssessment']."</td></tr>";

	$ResultTable .= "<tr><td valign='top'><table width='100%' border='0' cellspacing='0' cellpadding='0' class='report_border'>";
	$ResultTable .= "<tr>
						<td width='26%' class='report_formfieldtitle' height='100%'>
							$TempSubjectCell
						</td>";
	if($ShowFullMark==1)
	{
		$ResultTable .= "<td width='25%' class='border_left small_title report_formfieldtitle' align='center'>".$LangArray['FullMark']."</td>";
		$ExtraColumn++;
	}

	list($op_from, $op_to) = explode(",", $OverallPositionRange);
	for($i=0; $i<sizeof($ColumnArray); $i++)
	{
		list($ReportColumnID, $ColumnTitle, $Weight, $ShowPosition, $PositionRange) = $ColumnArray[$i];
		if($Weight!="")
		{
			$ColumnTitle = ($ShowColumnPercentage==1) ? $ColumnTitle."&nbsp;".$Weight."%" : $ColumnTitle;
			$WeightArray[$ReportColumnID] = $Weight;
		}
		//width='{$ColumnWidth}%'
		$ResultTable .= "<td class='border_left report_formfieldtitle' width='25%'><table border=0 cellpadding=0 cellspacing=0 width='100%'>";
		$ResultTable .= "<tr height='75%'><td align='center' class='small_title' colspan='3'>".$ColumnTitle."</td></tr>";
		if($ShowPosition==1 || $ShowPosition==2)
		{
			$ResultTable .= "<tr>";
			$ResultTable .= "<td align='center' width='50%' class='reportcard_text'>".$LangArray['Mark']."</td>";
			$ResultTable .= "<td width='1'>&nbsp;</td>";
			$ResultTable .= "<td align='center' width='50%' class='reportcard_text'>(".$LangArray['Position'].")</td>";
			$ResultTable .= "</tr>";
		}
		$ResultTable .= "</table></td>";
	}

	if($ColumnSize>1 && $HideOverallResult!=1)
	{
		$ResultTable .= "<td class='border_left report_formfieldtitle' width='{$ColumnWidth}%'><table border=0 cellpadding=0 cellspacing=0 width='100%' align='center'>";
		$ResultTable .= "<tr height='75%'><td align='center' class='small_title' colspan='3'>".$LangArray['OverallResult']."</td></tr>";
		if($ShowOverallPosition==1 || $ShowOverallPosition==2)
		{
			$ResultTable .= "<tr>";
			$ResultTable .=  "<td align='center' width='50%' class='reportcard_text'>".$LangArray['Mark']."</td>";
			$ResultTable .= "<td width='1'>&nbsp;</td>";
			$ResultTable .= "<td align='center' width='50%' class='reportcard_text'>(".$LangArray['Position'].")</td>";
			$ResultTable .= "</tr>";
		}
		$ResultTable .= "</table></td>";
		$ExtraColumn++;
	}

	if($ShowSubjectTeacherComment==1) {
		$ResultTable .= "<td width='10%' class='border_left small_title report_formfieldtitle' align='center'>".$LangArray['SubjectTeacherComment']."</td>";
	}

	$ResultTable .= "</tr>";

	$ColumnSpan = $ColumnSize + $ExtraColumn + 1;
	$IsFirst = 1;
	if(is_array($SubjectArray))
	{
		$count = 0;
		$TempTotalMark = 0;		
		foreach($SubjectArray as $CodeID => $Data)
		{			
			if(is_array($Data))
			{			
				$totalWeight[$StudentID] = 0;
				//unset($totalWeight);
				foreach($Data as $CmpCodeID => $InfoArr)
				{
					list($ReportSubjectID, $SubjectID, $EngSubjectName, $ChiSubjectName, $FullMark, $Scale, $HidePosition) = $InfoArr;
					# Hide not enrolled subject according to the setting
					//if($HideNotEnrolled==1 && (empty($ParStudentResultArray[$SubjectID]["Overall"]) || strcmp($ParStudentResultArray[$SubjectID]["Overall"]["Grade"], "/")==0))
					if ($HideNotEnrolled==1 && !$libreportcard->CHECK_ENROL($ParStudentResultArray[$SubjectID]))
						continue;

					$IsComponent = ($CmpCodeID>0) ? 1 : 0;
					$top_style = ($IsComponent==0 && $IsFirst==0) ? "border_top" : "";
					$IsFirst = 0;
					$Prefix = ($IsComponent==1) ? "&nbsp;&nbsp;&nbsp;&nbsp;" : "";
					
					# get converted grade arr
					$GradeMarkArr = $libreportcard->GET_GRADEMARK_BY_SCHEMEID(6);					
										
					# Highest Grade		
					$MaxGrade = $DistinctionArray[$SubjectID]["G"];
					$FirstRowSetting = $ReportCellSetting[$ColumnArray[0][0]][$ReportSubjectID];
					//$FullMark = ($FirstRowSetting=="G") ? $MaxGrade : $FullMark;
					//$FullMark = (($FirstRowSetting=="G")&&($MaxGrade!="")) ? $MaxGrade : $libreportcard->GET_CONVERTED_GRADE($GradeMarkArr, $GradeMarkArr[sizeof($GradeMarkArr)-1][1], $FullMark);
					
					$CellAlign = "center";					
					if (($IsComponent==1)&&(($ClassLevelID != 5)&&($ClassLevelID != 6))) $CellAlign = "left";
					if (($FirstRowSetting=="G")&&(($ClassLevelID != 5)&&($ClassLevelID != 6))) $CellAlign = "right";
					
					if (($FirstRowSetting=="G")&&($MaxGrade!="")) {
						$FullMark = $MaxGrade;
					} else {
						if (($ClassLevelID == 5)||($ClassLevelID == 6)) {
							$FullMark = $libreportcard->GET_CONVERTED_GRADE($GradeMarkArr, $GradeMarkArr[sizeof($GradeMarkArr)-1][1], $FullMark);
						} else {							
							if ($FirstRowSetting == "G") {
								$FullMark = $libreportcard->GET_CONVERTED_GRADE($GradeMarkArr, $GradeMarkArr[sizeof($GradeMarkArr)-1][1], $FullMark);
							} else {
								$FullMark = $FullMark;
								if (!$IsComponent) $TempTotalMark += $FullMark;
							}
						}						
					} 

					
					# check whether is parent/component subject
					$IsParent = (sizeof($AllSubjectArray[$SubjectID])>1) ? 1 : 0;		
					if (($IsParent==1)&&(($ClassLevelID != 5)&&($ClassLevelID != 6))) $CellAlign = "center";
					$IsComponent = (empty($AllSubjectArray[$SubjectID])) ? 1 : 0;

					
					# get total weight by the component subjects
					if (is_array($AllSubjectArray[$SubjectID])) {
						$TempAllSubjectArray[$StudentID] = $AllSubjectArray[$SubjectID];
						while (list ($key, $val) = each ($TempAllSubjectArray[$StudentID])) {
							if ($key > 0) {
								$totalWeight[$StudentID] += $libreportcard->GET_WEIGHTING($SubjectGradingArray[$key]);
							}							
						}
					}

					$ResultShow = (!($ResultDisplayType==2 && $IsParent==1) && !($IsParent==1 && $ResultCalculationType==1)) ? 1 : 0;
					$OverallShow = (!($ResultDisplayType==2 && $IsParent==1) && !($ResultDisplayType==1 && $IsComponent==1)) ? 1 : 0;

					/*
					if ($IsParent) {
						$ParentWeighting = $libreportcard->GET_WEIGHTING($SubjectGradingArray[$SubjectID]);
					}
					*/
					
					# get subject display cell
					if($BilingualReportTitle==1 || $SubjectTitleType==0) {
						$SubjectCell = "<td>
								<table border='0' cellpadding='2' cellspacing='0' width='100%' height='100%'>
								<tr>
									<td width='50%' class='tabletext'>".$Prefix.$EngSubjectName."</td>
									<td width='50%' class='tabletext'>".$Prefix.$ChiSubjectName."</td>
									";
						if ($CmpCodeID>0) {
							$TempPercentage = $libreportcard->DISPLAY_WEIGHTING($SubjectGradingArray[$SubjectID], $totalWeight[$StudentID], !($CmpCodeID>0));
							if ($TempPercentage == "0%") $TempPercentage = "";
							$TempPercentage = str_replace("%", "", $TempPercentage);
							if (($ClassLevelID == 5)||($ClassLevelID == 6))
							{
								$SubjectCell .= "<td class='tabletext' width='20'>".$TempPercentage."</td>";
							} else {
								$SubjectCell .= "<td class='tabletext' width='20'>&nbsp;</td>";
								if ($FirstRowSetting!="G") $FullMark = $TempPercentage;								
							}
						}
						$SubjectCell .= "</tr>
								</table>
								</td>";
					}
					else {
						$SubjectDisplayName = ($SubjectTitleType==1) ? $EngSubjectName : $ChiSubjectName;
						$SubjectCell = "<td>
										<table border='0' cellpadding='2' cellspacing='0' width='100%' height='100%'>
										<tr>
											<td class='tabletext'>".$Prefix.$SubjectDisplayName."</td>
										";
						if ($CmpCodeID>0) {
							$TempPercentage = $libreportcard->DISPLAY_WEIGHTING($SubjectGradingArray[$SubjectID], $totalWeight[$StudentID], !($CmpCodeID>0));
							if ($TempPercentage == "0%") $TempPercentage = "";
							$TempPercentage = str_replace("%", "", $TempPercentage);
							
							if (($ClassLevelID == 5)||($ClassLevelID == 6))
							{
								$SubjectCell .= "<td class='tabletext' width='20'>".$TempPercentage."</td>";
							} else {
								$SubjectCell .= "<td class='tabletext' width='20'>&nbsp;</td>";
								if ($FirstRowSetting!="G") $FullMark = $TempPercentage;
							}
						}
						$SubjectCell .= "</tr>
										</table>
										</td>";
					}

					$ResultTable .= "<tr>";
					// line height - aki
					$ResultTable .= "<td class='$top_style'>
										<table width='100%' border='0' cellpadding='0' cellspacing='0' height='$LineHeight'><tr>".$SubjectCell."</tr></table>
									</td>";
					$ResultTable .= ($ShowFullMark==1) ? "<td class='tabletext border_left $top_style' align='$CellAlign'>&nbsp;&nbsp;".($ResultShow==1?$FullMark."&nbsp;":"&nbsp;")."&nbsp;</td>" : "";

					for($i=0; $i<sizeof($ColumnArray); $i++)
					{
						list($ReportColumnID, $ColumnTitle, $Weight, $ShowPosition, $PositionRange) = $ColumnArray[$i];
						$CellSetting = $ReportCellSetting[$ReportColumnID][$ReportSubjectID];
						list($p_form, $p_to) = explode(",", $PositionRange);

						$DisplayResult = "";
						if(!$ResultShow)
						{
							$ResultTable .= "<td class='border_left $top_style' align='center'>&nbsp;</td>";
						}
						else
						{
							if($CellSetting!="N/A")
							{
								$StylePrefix = "";
								$StyleSuffix = "";
								if($CellSetting=="S") {			
									$s_raw_mark = $ParStudentResultArray[$SubjectID][$ReportColumnID]["RawMark"];
									$s_weighted_mark = $ParStudentResultArray[$SubjectID][$ReportColumnID]["WeightedMark"];
									$s_grade = $ParStudentResultArray[$SubjectID][$ReportColumnID]["Grade"];

									$o_s_raw_mark = $s_raw_mark;
									
									// temp soln for calculation
									/***********************************************************************/
									if ($CmpCodeID>0)
									{
										$Sql = "
													SELECT
															SchemeID
													FROM
															RC_SUBJECT_GRADING_SCHEME							
													WHERE
															SubjectGradingID = '".$SubjectGradingArray[$SubjectID]."'
												";
										$row = $libreportcard->returnVector($Sql);
										$SchemeID = $row[0];
										
										$Sql = "
													SELECT
															Weighting
													FROM
															RC_GRADING_SCHEME							
													WHERE
															SchemeID = '$SchemeID'
												";
										$row = $libreportcard->returnVector($Sql);
										$temp = $row[0];
										
										if ($totalWeight[$StudentID] > 0) {
											$s_raw_mark = $s_raw_mark * ($temp / $totalWeight[$StudentID]);	
										}
									}
									/***********************************************************************/
									
									
									if($s_raw_mark>=0) {
										if($MarkTypeDisplay==2) {
											$DisplayResult = $s_grade;
											list($StylePrefix, $StyleSuffix, $IsDistinct, $IsFailed) = $libreportcard->GET_STYLE("G", $DisplayResult, $FailedArray[$SubjectID], $DistinctionArray[$SubjectID], $PFArray[$SubjectID]);
										}
										else {
											list($StylePrefix, $StyleSuffix, $IsDistinct, $IsFailed) = $libreportcard->GET_STYLE($CellSetting, $o_s_raw_mark, $FailedArray[$SubjectID], $DistinctionArray[$SubjectID], $PFArray[$SubjectID]);
											$DisplayResult = ($MarkTypeDisplay==1) ? $s_raw_mark : $s_weighted_mark;
											$DisplayResult = round($DisplayResult, 1);
										}
									}
									else
										$DisplayResult = $libreportcard->SpecialMarkArray[trim($s_raw_mark)];
								}
								else {
									$DisplayResult = $ParStudentResultArray[$SubjectID][$ReportColumnID]["Grade"];
									if($DisplayResult!="/" && $DisplayResult!="abs") {
										list($StylePrefix, $StyleSuffix, $IsDistinct, $IsFailed) = $libreportcard->GET_STYLE($CellSetting, $DisplayResult, $FailedArray[$SubjectID], $DistinctionArray[$SubjectID], $PFArray[$SubjectID]);
									}
								}
								//if ($IsFailed) $DisplayResult= "(".$DisplayResult.")";
								if ($DisplayResult=="") $DisplayResult = "---";
								if (($IsFailed)&&($DisplayResult!="---")) $DisplayResult = "(".$DisplayResult.")";								
								$OverallFormPosition = $ParStudentResultArray[$SubjectID]["Overall"]["OMF"];
								if ($DisplayResult!="---") $DisplayResult = $StylePrefix.$DisplayResult.$StyleSuffix;
								$FormPosition = $ParStudentResultArray[$SubjectID][$ReportColumnID]["OMF"];
								$ClassPosition = $ParStudentResultArray[$SubjectID][$ReportColumnID]["OMC"];
							}

							$ResultTable .= "<td class='border_left $top_style' width='10%'><table border=0 cellpadding=0 cellspacing=0 width='100%'>";
							$ResultTable .= "<tr>";
							if($ShowPosition==1 || $ShowPosition==2)
							{
								$ResultTable .= "<td align='$CellAlign' class='tabletext' width='50%'>&nbsp;".($DisplayResult==""?"---":$DisplayResult)."&nbsp;</td>";
								$ResultTable .= "<td width='1'>&nbsp;</td>";

								$PosDisplay = ($ShowPosition==1) ? $ClassPosition : $FormPosition;
								$PosDisplay = ($PosDisplay>0 && ($PosDisplay>=$p_from && $PosDisplay<=$p_to)) ? $PosDisplay : "*";
								$ResultTable .= ($HidePosition==0 && $CellSetting=="S") ? "<td align='center' class='tabletext' width='50%'>(".$PosDisplay.")</td>" : "<td align='center' class='tabletext width='50%''>(*)</td>";
							}
							else
							{
								$ResultTable .= "<td align='$CellAlign' class='tabletext'>&nbsp;&nbsp;".(($DisplayResult=="")?"---":$DisplayResult)."&nbsp;&nbsp;</td>";
							}
							$ResultTable .= "</tr>";
							$ResultTable .= "</table></td>";
						}
					}

					if($ColumnSize>1 && $HideOverallResult!=1)
					{
						if(!$OverallShow)
						{
							$ResultTable .= "<td class='tabletext border_left $top_style' align='center'>&nbsp;</td>";
						}
						else
						{
							$StylePrefix = "";
							$StyleSuffix = "";
							if($CellSetting=="S") {
								$s_overall_mark = $ParStudentResultArray[$SubjectID]["Overall"]["Mark"];
								$s_overall_grade = $ParStudentResultArray[$SubjectID]["Overall"]["Grade"];

								if($s_overall_mark>=0) {
									if($MarkTypeDisplay==2) {
										$OverallResult = $s_overall_grade;
										list($StylePrefix, $StyleSuffix, $IsDistinct, $IsFailed) = $libreportcard->GET_STYLE("G", $OverallResult, $FailedArray[$SubjectID], $DistinctionArray[$SubjectID], $PFArray[$SubjectID]);
									}
									else {
										$OverallResult = $s_overall_mark;
										list($StylePrefix, $StyleSuffix, $IsDistinct, $IsFailed) = $libreportcard->GET_STYLE($CellSetting, $OverallResult, $FailedArray[$SubjectID], $DistinctionArray[$SubjectID], $PFArray[$SubjectID]);
									}
								}
								else
									$OverallResult = $libreportcard->SpecialMarkArray[trim($s_overall_mark)];
							}
							else {
								$OverallResult = $ParStudentResultArray[$SubjectID]["Overall"]["Grade"];
								if($OverallResult!="/" && $OverallResult!="abs") {
									list($StylePrefix, $StyleSuffix, $IsDistinct, $IsFailed) = $libreportcard->GET_STYLE($CellSetting, $OverallResult, $FailedArray[$SubjectID], $DistinctionArray[$SubjectID], $PFArray[$SubjectID]);
								}
							}
							if ($OverallResult=="") $OverallResult = "---";
							if (($IsFailed)&&($OverallResult!="---")) $OverallResult = "(".$OverallResult.")";
							if ($OverallResult!="---") $OverallResult = $StylePrefix.$OverallResult.$StyleSuffix;
							$OverallFormPosition = $ParStudentResultArray[$SubjectID]["Overall"]["OMF"];
							$OverallClassPosition = $ParStudentResultArray[$SubjectID]["Overall"]["OMC"];

							$ResultTable .= "<td class='border_left $top_style' width='10%'><table border=0 cellpadding=0 cellspacing=0 width='100%'>";
							$ResultTable .= "<tr>";
							if($ShowOverallPosition==1 || $ShowOverallPosition==2)
							{								
								$ResultTable .= "<td align='$CellAlign' class='tabletext' width='50%'>".$OverallResult."</td>";
								$ResultTable .= "<td>&nbsp;</td>";

								$OverallPosDisplay = ($ShowOverallPosition==1) ? $OverallClassPosition : $OverallFormPosition;
								$OverallPosDisplay = ($OverallPosDisplay>0 && ($OverallPosDisplay>=$op_from && $OverallPosDisplay<=$op_to)) ? $OverallPosDisplay : "*";
								//aki
								if ($IsFailed) $OverallPosDisplay = "*";
								$ResultTable .= ($HidePosition==0 && $CellSetting=="S") ? "<td align='center' class='tabletext' width='50%'>(".$OverallPosDisplay.")</td>" : "<td align='center' class='tabletext' width='50%'>(*)</td>";
							}
							else
							{
								$ResultTable .= "<td align='$CellAlign' class='tabletext'>".($OverallResult==""?"---":$OverallResult)."</td>";
							}
							$ResultTable .= "</tr>";
							$ResultTable .= "</table></td>";
						}
					}
					if($ShowSubjectTeacherComment==1)
					{
						$Comment = $ParSubjectTeacherCommentArray[$SubjectID];
						$ResultTable .= "<td class='tabletext border_left $top_style' align='center'>".($Comment==""?"&nbsp;":$Comment)."</td>";
					}
					$ResultTable .= "</tr>";
					$count++;
				}
			}
		}
	}
	else
	{
		$ResultTable .= "<tr><td align='center' class='small_text report_formfieldtitle' colspan='".$ColumnSpan."'>".$LangArray['NoRecord']."</td></tr>";
	}

	if(!empty($SummaryResultArray))
	{
		for($k=0; $k<sizeof($SummaryResultArray); $k++)
		{
			$ResultTable .= "<tr>";
			//$top_border_style = ($k==0) ? "border_top" : "";
			$top_border_style = "border_top";			
			
			if (($SummaryResultArray[$k]["Title"] != $LangArray['FormPosition']) && 
				($SummaryResultArray[$k]["Title"] != $LangArray['Position']) && 			
			 	($SummaryResultArray[$k]["Title"] != $LangArray['FormPupilNumber']))
			{
				$ShowTitle = true;
				$colspan = "";
				if ($SummaryResultArray[$k]["Title"] == $LangArray['GrandTotal']) {
					$FullMarkContent = ($TempTotalMark != 0) ? $TempTotalMark : "---";
				}
				if ($SummaryResultArray[$k]["Title"] == $LangArray['AverageMark'])
				{
					$FullMarkContent = ($IsPrimary6)? "A":"100";
				}
			} else {
				$colspan = ($ShowFullMark==1) ? "colspan='2'" : "";
				$ShowTitle = false;				
			}
			
			// line height - aki
			$ResultTable .= "<td class='{$top_border_style}' {$colspan}>
						<table border='0' cellpadding='2' cellspacing='0' width='100%' height='$LineHeight'><tr><td class='small_title'>".$SummaryResultArray[$k]["Title"]."</td></tr></table>
						</td>";
			if ($ShowFullMark && $ShowTitle)
			{
				$ResultTable .= "<td class='tabletext border_left {$top_border_style}' align='center'>".$FullMarkContent."</td>";
			}
			
			//for($i=0; $i<sizeof($ColumnArray); $i++)
			//{
				//$ColumnTitle = trim($ColumnArray[$i]["ColumnTitle"]);
				$ResultDisplay = $SummaryResultArray[$k][$Semester];				
				//aki
				$ResultTable .= "<td class='tabletext border_left {$top_border_style}' align='center'>".($ResultDisplay==""?"---":$ResultDisplay)."</td>";
			//}
			if(($HideOverallResult)&&($ShowFullMark))
			//if(!$HideOverallResult)
				$ResultTable .= "<td class='tabletext border_left {$top_border_style}' align='center'>".($SummaryResultArray[$k][$Semester]==""?"---":$SummaryResultArray[$k][$Semester])."</td>";
			if($ShowSubjectTeacherComment==1) {
				$ResultTable .= "<td class='border_left {$top_border_style}' align='center'>&nbsp;</td>";
			}
			$ResultTable .= "</tr>";
		}
	}
	/*
	if(!empty($ConductResultArray))
	{
		for($k=0; $k<sizeof($ConductResultArray); $k++)
		{
			$ResultTable .= "<tr>";
			$top_border_style = ($k==0) ? "border_top" : "";
			$colspan = ($ShowFullMark==1) ? "colspan='2'" : "";
			// line height - aki
			$ResultTable .= "<td class='{$top_border_style}' {$colspan}>
						<table border='0' cellpadding='2' cellspacing='0' width='100%' height='$LineHeight'><tr><td class='small_title'>".$ConductResultArray[$k]["Title"]."</td></tr></table>
						</td>";
			for($i=0; $i<sizeof($ColumnArray); $i++)
			{
				$ColumnTitle = trim($ColumnArray[$i]["ColumnTitle"]);
				$ResultDisplay = $ConductResultArray[$k][$ColumnTitle];
				//aki
				$ResultTable .= "<td class='tabletext border_left {$top_border_style}' align='center'>".($ResultDisplay==""?"---":$ResultDisplay)."</td>";
			}
			//if(!($Semester=='FULL' && $HideOverallResult==1))
			if($HideOverallResult)
				$ResultTable .= "<td class='tabletext border_left {$top_border_style}' align='center'>".($ConductResultArray[$k][$Semester]==""?"---":$ConductResultArray[$k][$Semester])."</td>";
			if($ShowSubjectTeacherComment==1) {
				$ResultTable .= "<td class='border_left {$top_border_style}' align='center'>&nbsp;</td>";
			}
			$ResultTable .= "</tr>";
		}
	}
	*/
	
	
	$SignatureTitleArray = $LangArray['DisplaySettingsArray']["Signature"];
	$SettingSignatureArray = $SettingArray["Signature"];

	$CellNumber = 0;
	$SignatureRow = "";
	$ShowColumnSpan = $ColumnSpan;
	if (!$ShowFullMark) {
		$ShowColumnSpan--;
	} else {
		$TempColspan = "colspan='2'";
	}
	
	// remark
	if ($RemarkTable != "") {
		$ResultTable .= "<tr>";
		$ResultTable .= "<td $TempColspan class='border_top'>".$RemarkTable."</td>";
		$ResultTable .= "<td class='small_title border_top border_left' colspan='".($ShowColumnSpan)."'>&nbsp;</td>";
		$ResultTable .= "</tr>";	
	}
	
	// signature
	/*
	for($i=0; $i<sizeof($SignatureTitleArray); $i++)
	{
		$SettingID = trim($SignatureTitleArray[$i]);
		if($SettingSignatureArray[$SettingID]==1)
		{
			$Title = $LangArray[$SettingID];
			$ResultTable .= "<tr>";
			$ResultTable .= "<td $TempColspan><table border='0' cellpadding='2' cellspacing='0' width='100%'><tr><td class='report_contenttitle border_top' align='middle' height='40' width='25%'><span style='width:{$SignatureWidth}px'>".$Title."</span></tr></td></table></td>";
			$ResultTable .= "<td class='small_title border_top border_left' colspan='".($ShowColumnSpan)."'>&nbsp;</td>";
			$ResultTable .= "</tr>";

			$CellNumber++;
		}
	}
	*/
	$ResultTable .= "<tr>";
	for($i=0; $i<sizeof($SignatureTitleArray); $i++)
	{
		$SettingID = trim($SignatureTitleArray[$i]);
		if($SettingSignatureArray[$SettingID]==1)
		{
			$Title = $LangArray[$SettingID];
			$TempCss = ($i == 0) ? "":"class='border_left'";
			$ResultTable .= "<td $TempCss><table border='0' cellpadding='2' cellspacing='0' height='36' width='100%'><tr><td class='report_contenttitle border_top' align='middle' width='25%'><span style='width:{$SignatureWidth}px'>".$Title."</span></tr></td></table></td>";
		}
	}
	$ResultTable .= "</tr><tr>";
	for($i=0; $i<sizeof($SignatureTitleArray); $i++)
	{
		$SettingID = trim($SignatureTitleArray[$i]);
		if($SettingSignatureArray[$SettingID]==1)
		{
			$TempCss = ($i == 0) ? "":"border_left";
			$ResultTable .= "<td class='small_title border_top $TempCss' height='60'>&nbsp;</td>";
		}
	}	
	$ResultTable .= "</tr>";
	
	$TempColspan = "";
	
	
	$ResultTable .= "</table>";

	
	$ResultTable .= "</td><td valign='top' width='3%'>";
	$ResultTable .= "</td><td valign='top' height='100%'>";
	$ResultTable .= "<table border='0' cellpadding='0' cellspacing='0' width='100%' height='100%'>";
	
	
	$ResultTable .= "<tr><td height='20%' valign='top'>";
	$ResultTable .= "<table border='0' cellpadding='0' cellspacing='0' width='100%' class='report_border'>";
	
	if(!empty($ConductResultArray))
	{		
		for($k=0; $k<sizeof($ConductResultArray); $k++)
		{
			if (($ConductResultArray[$k]["Title"] == $LangArray['DaysAbsent']) ||
				($ConductResultArray[$k]["Title"] == $LangArray['TimesLate']) ||
				($ConductResultArray[$k]["Title"] == $LangArray['AbsentWOLeave']))
			{
				$LoweronductResultArray[] = $ConductResultArray[$k];
			} else {				
				$UpperConductResultArray[] = $ConductResultArray[$k];
			}			
		}
	}	
	
	if(!empty($UpperConductResultArray))
	{
		for($k=0; $k<sizeof($UpperConductResultArray); $k++)
		{
			$ResultTable .= "<tr>";
			$top_border_style = ($k!=0) ? "border_top" : "";
			$colspan = ($ShowFullMark==1) ? "colspan='2'" : "";
			// line height - aki
			$ResultTable .= "<td class='{$top_border_style}' {$colspan}>
						<table border='0' cellpadding='2' cellspacing='0' width='100%' height='$LineHeight'><tr><td class='small_title'>".$UpperConductResultArray[$k]["Title"]."</td></tr></table>
						</td>";
						
			switch ($UpperConductResultArray[$k]["Title"]) 
			{
				case $eReportCard['Merit']:
					$unit = $eReportCard['merits'];
					break;
				default:
					$unit = "";
					break;
			}		
			
			$ResultDisplay = $UpperConductResultArray[$k][$Semester];
			//aki
			if (($ResultDisplay=="")||($ResultDisplay=="---")) {
				$ResultDisplay = "---";
			} else {
				$ResultDisplay = $ResultDisplay." ".$unit;
			}
			$ResultTable .= "<td class='tabletext border_left {$top_border_style}' align='center' width='28'>&nbsp;</td><td class='tabletext {$top_border_style}' width='20'>".$ResultDisplay."</td><td width='17' class='tabletext {$top_border_style}'>&nbsp;</td>";

			$ResultTable .= "</tr>";
		}
	}
	
	$ResultTable .= "</table></td></tr>";
	
	$ResultTable .= "<tr><td height='20%' valign='top'><br>";
	$ResultTable .= "<table border='0' cellpadding='0' cellspacing='0' width='100%' class='report_border'>";
	
	if(!empty($LoweronductResultArray))
	{
		for($k=0; $k<sizeof($LoweronductResultArray); $k++)
		{
			$ResultTable .= "<tr>";
			$top_border_style = ($k!=0) ? "border_top" : "";
			$colspan = ($ShowFullMark==1) ? "colspan='2'" : "";
			// line height - aki
			$ResultTable .= "<td class='{$top_border_style}' {$colspan}>
						<table border='0' cellpadding='2' cellspacing='0' width='100%' height='$LineHeight'><tr><td class='small_title'>".$LoweronductResultArray[$k]["Title"]."</td></tr></table>
						</td>";
			
			switch ($LoweronductResultArray[$k]["Title"]) 
			{
				case $eReportCard['Merit']:
					$unit = $eReportCard['merits'];
					break;
				default:
					$unit = "";
					break;
			}		
			
			$ResultDisplay = $LoweronductResultArray[$k][$Semester];
			//aki
			if (($ResultDisplay=="")||($ResultDisplay=="---")) {
				$ResultDisplay = "---";
			} else {
				$ResultDisplay = $ResultDisplay." ".$unit;
			}
			$ResultTable .= "<td class='tabletext border_left {$top_border_style}' align='center' width='28'>&nbsp;</td><td class='tabletext {$top_border_style}' width='20'>".$ResultDisplay."</td><td width='17' class='tabletext {$top_border_style}'>&nbsp;</td>";

			$ResultTable .= "</tr>";
		}
	}
	$ResultTable .= "</table></td></tr>";
	
	$ResultTable .= "<tr><td height='1%'>&nbsp;</td></tr>";
	
	$ResultTable .= "<tr><td height='58%' valign='top'>".$MiscTable;
	
	$ResultTable .= "</td></tr></table>";
	
	$ResultTable .= "</td></tr></table>";
	//$MiscTable
	return $ResultTable;
}

# generate attendance table
function GENERATE_ATTENDANCE_AND_MERIT_TABLE($ParSummaryArray, $ParMeritArray)
{
	global $SummaryTitleArray, $SettingSummaryArray, $SettingMiscArray, $ColumnArray, $eReportCard, $SemesterArray, $LangArray;
	global $DaysAbsentTitle, $TimesLateTitle, $AbsentWOLeaveTitle;

	$AttendanceTableShow = ($SettingSummaryArray["DaysAbsent"]==1 || $SettingSummaryArray["TimesLate"] || $SettingSummaryArray["AbsentWOLeave"]);
	$MeritTableShow = ($SettingMiscArray["MeritsAndDemerits"]==1);

	$SemColumnCount = 0;
	for($i=0; $i<sizeof($ColumnArray); $i++)
	{
		$sem_title = trim($ColumnArray[$i]["ColumnTitle"]);
		if($sem_title!="") {
			if(in_array($sem_title, $SemesterArray))
			{
				$SemesterHeaderRow .= ($AttendanceTableShow==1 || $MeritTableShow==1) ? "<td class='reportcard_text border_top border_left' align='center'>".$sem_title."</td>\n" : "";

				$absent_result = $ParSummaryArray[$sem_title][0];
				$DaysAbsentCell .= "<td class='reportcard_text border_top border_left' align='center'>".($absent_result==""?"---":$absent_result)."</td>\n";

				$late_result = $ParSummaryArray[$sem_title][1];
				$LateCell .= "<td class='reportcard_text border_top border_left' align='center'>".($late_result==""?"---":$late_result)."</td>\n";

				$absent_wo_leave_result = $ParSummaryArray[$sem_title][2];
				$AbsentWOLeaveCell .= "<td class='reportcard_text border_top border_left' align='center'>".($absent_wo_leave_result==""?"---":$absent_wo_leave_result)."</td>\n";

				$MeritResult = $ParMeritArray[$sem_title][0];
				$MeritCell .= "<td class='reportcard_text border_top border_left' align='center'>".($MeritResult==""?"---":$MeritResult)."</td>\n";

				$DemeritResult = $ParMeritArray[$sem_title][1];
				$DemeritCell .= "<td class='reportcard_text border_top border_left' align='center'>".($DemeritResult==""?"---":$DemeritResult)."</td>\n";

				$SemColumnCount++;
			}

		}
	}

	if($SemColumnCount>0 && ($AttendanceTableShow==1 || $MeritTableShow==1))
	{
		$ReturnTable = "<table width='100%' valign='bottom' border='0' cellspacing='0' cellpadding='2'>\n";
		if($AttendanceTableShow==1)
		{
			$ReturnTable .= "<tr><td class='reportcard_text border_top'>".$LangArray['Attendance']."</td>\n";
			$ReturnTable .= $SemesterHeaderRow;
		}

		# Rows of Attendance records
		if($SettingSummaryArray["DaysAbsent"]==1)
		{
			$ReturnTable .= "<tr><td class='reportcard_text border_top'>".$DaysAbsentTitle."</td>\n";
			$ReturnTable .= $DaysAbsentCell;
			$ReturnTable .= "</tr>";
		}
		if($SettingSummaryArray["TimesLate"]==1)
		{
			$ReturnTable .= "<tr><td class='reportcard_text border_top'>".$TimesLateTitle."</td>\n";
			$ReturnTable .= $LateCell;
			$ReturnTable .= "</tr>";
		}
		if($SettingSummaryArray["AbsentWOLeave"]==1)
		{
			$ReturnTable .= "<tr><td class='reportcard_text border_top'>".$AbsentWOLeaveTitle."</td>";
			$ReturnTable .= $AbsentWOLeaveCell;
			$ReturnTable .= "</tr>";
		}

		# Row of Merit records
		if($MeritTableShow==1)
		{
			if($AttendanceTableShow==1) {
				$ReturnTable .= "<tr><td class='reportcard_text border_top' colspan='".($SemColumnCount+1)."'>".$LangArray['MeritsAndDemerits']."</td></tr>\n";
			}
			else {
				$ReturnTable .= "<tr><td class='reportcard_text border_top'>".$LangArray['MeritsAndDemerits']."</td>\n";
				$ReturnTable .= $SemesterHeaderRow;
			}

			$ReturnTable .= "<tr><td class='reportcard_text border_top'>".$LangArray['Merit']."</td>\n";
			$ReturnTable .= $MeritCell;
			$ReturnTable .= "</tr>";
			$ReturnTable .= "<tr><td class='reportcard_text border_top'>".$LangArray['Demerit']."</td>\n";
			$ReturnTable .= $DemeritCell;
			$ReturnTable .= "</tr>";
		}
		$ReturnTable .= "</table>";
	}

	return $ReturnTable;
}

################################ Function End #####################################
?>