<?php
############################ Function Start ######################################

$IsPrimary6 = ($ReportID == 5);

$ConvertGradeSchemeID = ($IsPrimary6) ? 3 : 2;

function GENERATE_MARK_TABLE()
{
	global $IsPrimary6;

	if (true) {
		$MarkTable = "<table width='70%' border='0' cellpadding='0' cellspacing='0' align='center'>\n";
		
		$MarkTable .= "<tr class='tabletext'>";
		$MarkTable .= "<td>MARKS</td><td>&nbsp;</td><td width='10'>&nbsp;</td><td>GRADE</td>";
		$MarkTable .= "</tr>";
		
		$MarkTable .= "<tr class='tabletext'>";
		$MarkTable .= "<td>Max.</td><td>100</td><td>&nbsp;</td><td>&nbsp;</td>";
		$MarkTable .= "</tr>";
		
		$MarkTable .= "<tr class='tabletext'>";
		$MarkTable .= "<td>Over</td><td>90</td><td>&nbsp;</td><td align='center'>A</td>";
		$MarkTable .= "</tr>";
		
		$MarkTable .= "<tr class='tabletext'>";
		$MarkTable .= "<td>Over</td><td>80</td><td>&nbsp;</td><td align='center'>B</td>";
		$MarkTable .= "</tr>";
		
		$MarkTable .= "<tr class='tabletext'>";
		$MarkTable .= "<td>Over</td><td>70</td><td>&nbsp;</td><td align='center'>C</td>";
		$MarkTable .= "</tr>";
		
		$MarkTable .= "<tr class='tabletext'>";
		$MarkTable .= "<td>Over</td><td>60</td><td>&nbsp;</td><td align='center'>D</td>";
		$MarkTable .= "</tr>";
		
		$MarkTable .= "<tr class='tabletext'>";
		$MarkTable .= "<td class='border_top'>Under</td><td class='border_top'>60</td><td class='border_top'>&nbsp;</td><td class='border_top' align='center'>E</td>";
		$MarkTable .= "</tr>";
		
		$MarkTable .= "</table>";
	} else {
		$MarkTable = "<table width='90%' border='0' cellpadding='2' cellspacing='0' align='center'>\n";
				
		$MarkTable .= "<tr class='tabletext'>";
		$MarkTable .= "<td colspan='4'>MARKS</td><td width='10'>&nbsp;</td><td>GRADE</td>";
		$MarkTable .= "</tr>";
		
		$MarkTable .= "<tr class='tabletext'>";
		$MarkTable .= "<td colspan='2' nowrap>1st Term</td><td>&nbsp;</td><td colspan='2'>Final</td><td>&nbsp;</td>";
		$MarkTable .= "</tr>";
		
		$MarkTable .= "<tr class='tabletext'>";
		$MarkTable .= "<td>Max.</td><td>50</td><td>&nbsp;</td><td>Max.</td><td>100</td><td>&nbsp;</td>";
		$MarkTable .= "</tr>";
		
		$MarkTable .= "<tr class='tabletext'>";
		$MarkTable .= "<td>Over</td><td>45</td><td>&nbsp;</td><td>Over</td><td>90</td><td align='center'>A</td>";
		$MarkTable .= "</tr>";
		
		$MarkTable .= "<tr class='tabletext'>";
		$MarkTable .= "<td>Over</td><td>40</td><td>&nbsp;</td><td>Over</td><td>80</td><td align='center'>B</td>";
		$MarkTable .= "</tr>";
		
		$MarkTable .= "<tr class='tabletext'>";
		$MarkTable .= "<td>Over</td><td>35</td><td>&nbsp;</td><td>Over</td><td>70</td><td align='center'>C</td>";
		$MarkTable .= "</tr>";
		
		$MarkTable .= "<tr class='tabletext'>";
		$MarkTable .= "<td>Over</td><td>30</td><td>&nbsp;</td><td>Over</td><td>60</td><td align='center'>D</td>";
		$MarkTable .= "</tr>";
		
		
		$MarkTable .= "<tr class='tabletext'>";
		$MarkTable .= "<td class='border_top'>&nbsp;</td><td class='border_top'>25</td><td class='border_top'>&nbsp;</td><td class='border_top'>&nbsp;</td><td class='border_top'>50</td><td class='border_top' align='center'>E</td>";
		$MarkTable .= "</tr>";
		
		$MarkTable .= "<tr class='tabletext'>";
		$MarkTable .= "<td>Under</td><td>25</td><td>&nbsp;</td><td>Under</td><td>50</td><td align='center'>F</td>";
		$MarkTable .= "</tr>";

		$MarkTable .= "</table>";
	}

	return $MarkTable;
}

# function to get Title Table
function GENERATE_TITLE_TABLE()
{
	global $ReportTitle, $image_path, $intranet_root, $lf;
	
	# get school badge
	$SchoolLogo = "<img src='report_image/stClare/badge.gif'>";
		
	# get school name
	$SchoolName = GET_SCHOOL_NAME();	

	$TitleTable = "<table border='0' cellpadding='0' cellspacing='0' align='center'>\n";
	$TitleTable .= "<tr><td align='center' width='110'>".($SchoolLogo==""?"&nbsp;":$SchoolLogo)."</td>";
	//if(!empty($ReportTitle) || !empty($SchoolName))
	if(True)
	{
		list($ReportTitle1, $ReportTitle2) = explode("::", $ReportTitle);

		$TitleTable .= "<td>";
		$TitleTable .= "<table width='100%' border='0' cellpadding='4' cellspacing='0' align='center' style='padding-right: 120px'>\n";
		
		if(!empty($ReportTitle1)) $ReportTitle1 = "<span >$ReportTitle1</span><br>";
		if(!empty($ReportTitle2)) $ReportTitle2 = "<span >$ReportTitle2</span><br>";
		
		$TempTitle = "<span class='report_title'>St. Clare's Primary School</span><br>
					<span class='report_title'>聖嘉勒小學</span><br>
					<span class='small_title'>3-6 Prospect Place, Bonham Road, HK</span><br>					
					<span class='small_title'>香港般咸道光景台3-6號</span><br>
									
					<span class='small_title'>Telephone 電話: 25472751 Fax &nbsp;&nbsp;&nbsp;&nbsp; 傳真: 25594139</span><br>
					<span class='small_title'>Email 電郵: stclareprimary@hotmail.com &nbsp;&nbsp;&nbsp;&nbsp; Webpage URL 網址: http://scps.school.hk</span><br>
					
					<span >ACADEMIC REPORT</span><br>
					<span >學業成績表</span><br>
					$ReportTitle1
					$ReportTitle2
				";
		
		$TitleTable .= "<tr><td nowrap='nowrap' align='center' colspan='2'>".$TempTitle."</td></tr>\n";
		
		$TitleTable .= "</table>\n";
		$TitleTable .= "</td>";
	}
	$TitleTable .= "<td align='center' width='110'>&nbsp;</td></tr>";
	$TitleTable .= "</table>";

	return $TitleTable;
}

# function to get Student Info Table
function GENERATE_STUDENT_INFO_TABLE($ParInfoArray)
{
	global $SettingStudentInfo, $StudentInfoTitleArray, $ClassTeacherArray, $IssueDate, $LangArray;
	global $ChiReportCard, $EngReportCard;
	
	list($ClassName, $ClassNumber, $StudentName, $RegNo, $ChiName, $EngName, $DOB, $Gender) = $ParInfoArray;
	if(!empty($SettingStudentInfo))
	{
		$count = 0;
		$StudentInfoTable = "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>\n";
		for($i=0; $i<sizeof($StudentInfoTitleArray); $i++)
		{
			$SettingID = trim($StudentInfoTitleArray[$i]);
			if($SettingStudentInfo[$SettingID]==1 && $SettingID!="ClassNumber")
			{
				//$Title = $LangArray[$SettingID];
				//$Title = str_replace("<br />", " ", $Title);
				$Title = $EngReportCard[$SettingID]." ".$ChiReportCard[$SettingID];
				$Display = "";
				switch($SettingID)
				{
					case "Name":
						$Display = $StudentName;
						break;
					case "ClassName":
						$Display = ($SettingStudentInfo["ClassNumber"]==1) ? $ClassName." (".$ClassNumber.") " : $ClassName;
						break;
					case "ClassTeacher":
						$Display = (!empty($ClassTeacherArray[$ClassName])) ? implode(", ", $ClassTeacherArray[$ClassName]) : "";
						break;
					case "DateOfIssue":
						$Display = $IssueDate;
						break;
					case "StudentNo":
						$Display = $RegNo;
						break;
					case "Gender":
						$Display = $Gender;
						break;
					case "DateOfBirth":
						$Display = date('Y-m-d', strtotime($DOB));
						break;
				}

				if($count%2==0) {
					$StudentInfoTable .= "<tr>\n";
					$StudentInfoTable .= "<td width=\"65%\" class='tabletext' valign='top'>".$Title." : ".$Display."</td>\n";
				}
				else
				{
					$StudentInfoTable .= "<td class='tabletext' valign='top'>".$Title." : ".$Display."</td>\n";
				}
				//$StudentInfoTable .= "<td class='tabletext' valign='top'>".$Title." : ".$Display."</td>\n";
				
				if($count%2==1) {
					$StudentInfoTable .= "</tr>\n";
				}
				$count++;
			}
		}
		$StudentInfoTable .= "</table>\n";
	}

	return $StudentInfoTable;
}

# Generate a simple cell for holding data
function GENERATE_MISC_CELL($title, $arr, $remarkcss, $content="") {
	$css = $remarkcss ? "" : "class='report_border'";
	
	if ($content == "" && $arr != "") {
		$content = "<ul>";
		for($i=0; $i<sizeof($arr); $i++) {
			$content .= "<li>".$arr[$i][0]."</li>";
		}
		$content .= "</ul>";
	}
	
	$x .= "<td width='50%' $css valign='top'>";
	$x .= "<table width='100%' border='0' cellpadding='2' cellspacing='0' align='center'>";
	$x .= "<tr><td class='small_title'>".$title."</td></tr>";
	$x .= "<tr><td class='small_text' valign='top' height='80'>".$content."</td></tr>";
	$x .= "</table>";
	$x .= "</td>";
	return $x;
}

# Generate the Merit data
function GENERATE_MERIT_CONTENT($meritArr) {
	global $HeaderSettingMap, $EngReportCard, $ChiReportCard;
	$x = "<ul>";
	// a student have 1 row to represent his/her record
	foreach ($meritArr[0] as $key => $value) {
		if ($key !="" && in_array($key, $HeaderSettingMap["merit"])) {
			$x .= "<li>".$EngReportCard[$key]." ".$ChiReportCard[$key].": ".$value."</li>";
		}
	}
	$x .= "</ul>";
	return $x;
}

# Generate the ECA data
function GENERATE_ECA_CONTENT($ecaArr) {
	global $HeaderSettingMap, $EngReportCard, $ChiReportCard;
	$x = "<ul>";
	for($i=0; $i<sizeof($ecaArr); $i++) {
		$x .= "<li>".$ecaArr[$i]["ECA"];
		if (isset($ecaArr[$i]["Grade"])) {
			$x .= " (".$ecaArr[$i]["Grade"].") </li>";
		}
	}
	$x .= "</ul>";
	return $x;
}

# function to get MISC Table
# Changed by Andy Chan:
# 	Divided into two parts,
# 	1st part save the generated cell in an associated array,
# 	2nd part print the Misc Table out
function GENERATE_MISC_TABLE($ParClassTeacherComment, $ParStudentID)
{
	global $SettingMiscArray, $MiscTitleArray, $Semester, $LangArray;
	global $StudentAwardArray, $StudentMeritArray, $StudentECAArray, $StudentRemarkArray;
	global $image_path, $LAYOUT_SKIN;
	global $StudentInterSchoolArray, $StudentSchoolServiceArray;
	global $EngReportCard, $ChiReportCard, $IsPrimary6;
	
	$ParStudentID = trim($ParStudentID);
	
	if(!empty($SettingMiscArray)) {
		for($i=0; $i<sizeof($MiscTitleArray); $i++) {
			$SettingID = trim($MiscTitleArray[$i]);
			if($SettingMiscArray[$SettingID]==1 && $SettingID!="SubjectTeacherComment") {
				$Title = $EngReportCard[$SettingID]." ".$ChiReportCard[$SettingID];
				$Title = str_replace("<br />", " ", $Title);				
				//$Display = "";
				$IsRemark = false;
				switch($SettingID)
				{
					case "ClassTeacherComment":
						$Display["ClassTeacherComment"] = GENERATE_MISC_CELL($Title, $ParClassTeacherComment, $IsRemark && $IsPrimary6);
						break;
					case "Awards":
						$Display["Awards"] = GENERATE_MISC_CELL($Title, $StudentAwardArray[$ParStudentID][$Semester], $IsRemark && $IsPrimary6);
						break;
					case "MeritsAndDemerits":
						$Display["MeritsAndDemerits"] = GENERATE_MISC_CELL($Title, "", $IsRemark && $IsPrimary6, GENERATE_MERIT_CONTENT($StudentMeritArray[$ParStudentID][$Semester]));
						break;
					case "ECA":
						$Display["ECA"] = GENERATE_MISC_CELL($Title, "", $IsRemark && $IsPrimary6, GENERATE_ECA_CONTENT($StudentECAArray[$ParStudentID][$Semester]));
						break;
					case "Remark":
						$IsRemark = true;
						$Display["Remark"] = GENERATE_MISC_CELL($Title, $StudentRemarkArray[$ParStudentID][$Semester], $IsRemark && $IsPrimary6);
						break;
					case "InterSchoolCompetition":
						$Display["InterSchoolCompetition"] = GENERATE_MISC_CELL($Title, $StudentInterSchoolArray[$ParStudentID][$Semester], $IsRemark && $IsPrimary6);
						break;
					case "SchoolService":
						$Display["SchoolService"] = GENERATE_MISC_CELL($Title, $StudentSchoolServiceArray[$ParStudentID][$Semester], $IsRemark && $IsPrimary6);
						break;
				}
			}
		}
		
		$count = 0;
		$IsPrint = 0;
		$MiscTable = "<br /><table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>";
		
		foreach ($Display as $key => $value) {
			$MiscTable .= ($count%2==0) ? "<tr>" : "";
			$MiscTable .= $value;
			if ($IsPrint) {
				$ToPrint = "&nbsp;";
				$TempClass = "";
			} else {
				$MiscTable .= "<td>&nbsp;</td><td class='report_border'><table width='100%' border='0' cellpadding='2' cellspacing='0' align='center'>
							<tr><td class='small_title'>".GENERATE_MARK_TABLE()."</td></tr>
							</table></td></tr>";
				$IsPrint = true;
			}
			$MiscTable .= ($count%2==0) ? "<td>&nbsp;</td><td $TempClass>$ToPrint</td></tr>" : "</tr><tr><td colspan='3'><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width='10' height='5'></td></tr>";
			$count++;
		}
		$MiscTable .= "</table>";
	}

	return $MiscTable;
}

# function to get summary table
function GENERATE_SUMMARY_TABLE($ParInfoArray, $ClassHighestAverage, $ParSummaryArray)
{
	global $SummaryTitleArray, $SettingSummaryArray, $Semester, $LangArray;
	global $LineHeight, $ChiReportCard, $EngReportCard, $ClassID, $libreportcard;
	global $OverallPositionRange;
	
	list($GT, $AM, $GPA, $OMC, $OMCT, $OMF, $OMFT) = $ParInfoArray[$Semester];
	
	$ClassLevelID = $libreportcard->GET_CLASSLEVELID_BY_CLASSID($ClassID);
	
	if(!empty($SettingSummaryArray))
	{
		$count = 0;
		$SummaryTable = "<table width='100%' border='0' cellpadding='2' cellspacing='0' align='center'>";
		for($k=0; $k<sizeof($SummaryTitleArray); $k++)
		{
			$SettingID = trim($SummaryTitleArray[$k]);
			if($SettingSummaryArray[$SettingID]==1)
			{
				//$Title = $LangArray[$SettingID];
				$Title = $EngReportCard[$SettingID]." ".$ChiReportCard[$SettingID];
				switch($SettingID)
				{
					case "GrandTotal":
						$Display = $GT;
						break;
					case "GPA":
						$Display = $GPA;
						break;
					case "AverageMark":
						$Display = $AM;
						break;
					case "FormPosition":						
						list($op_from, $op_to) = explode(",", $OverallPositionRange);
						$Display = $OMF;
						$Display = ($Display>0 && ($Display>=$op_from && $Display<=$op_to)) ? $Display : "--";						
						
						//$Display = ($OMF>0) ? $OMF : "--";
						//if ($OMF > ($OMFT - ($OMFT / 3))) $Display = "--";
						//if (($ClassLevelID==9)&&($OMF > 100)) $Display = "--";
						//if (($ClassLevelID==12)&&($OMF > 90)) $Display = "--";
						// P5-P6 show 2/3人... (全級)						
						break;
					case "ClassPosition":						
						list($op_from, $op_to) = explode(",", $OverallPositionRange);
						$Display = $OMC;
						$Display = ($Display>0 && ($Display>=$op_from && $Display<=$op_to)) ? $Display : "--";
						
						//$Display = ($OMC>0) ? $OMC : "--";
						//if ($OMC > 10) $Display = "--";
						// P1-P4 show 1-10 (全班)
						break;						
						
					case "ClassPupilNumber":
						$Display = $OMCT;
						break;
					case "FormPupilNumber":
						$Display = $OMFT;
						break;
					case "ClassHighestAverage":
						$Display = $ClassHighestAverage[$Semester];
						break;
					
					// should depend on the config file
					case "DaysAbsent":
						$Display = ($ParSummaryArray[$Semester][0]["DaysAbsent"]==""?"--":$ParSummaryArray[$Semester][0]["DaysAbsent"]);
						break;
					case "TimesLate":
						$Display = ($ParSummaryArray[$Semester][0]["TimesLate"]==""?"--":$ParSummaryArray[$Semester][0]["TimesLate"]);
						break;
					case "AbsentWOLeave":
						$Display = ($ParSummaryArray[$Semester][0]["AbsentWOLeave"]==""?"--":$ParSummaryArray[$Semester][0]["AbsentWOLeave"]);
						break;
					case "Conduct":
						$Display = ($ParSummaryArray[$Semester][0]["Conduct"]==""?"--":$ParSummaryArray[$Semester][0]["Conduct"]);
						break;
					case "Politeness":
						$Display = ($ParSummaryArray[$Semester][0]["Politeness"]==""?"--":$ParSummaryArray[$Semester][0]["Politeness"]);
						break;
					case "Behaviour":
						$Display = ($ParSummaryArray[$Semester][0]["Behaviour"]==""?"--":$ParSummaryArray[$Semester][0]["Behaviour"]);
						break;
					case "Application":
						$Display = ($ParSummaryArray[$Semester][0]["Application"]==""?"--":$ParSummaryArray[$Semester][0]["Application"]);
						break;
					case "Tidiness":
						$Display = ($ParSummaryArray[$Semester][0]["Tidiness"]==""?"--":$ParSummaryArray[$Semester][0]["Tidiness"]);
						break;
					case "Motivation":
						$Display = ($ParSummaryArray[$Semester][0]["Motivation"]=="") ? "--" : $ParSummaryArray[$Semester][0]["Motivation"];
						break;				
					case "SelfConfidence":
						$Display = ($ParSummaryArray[$Semester][0]["SelfConfidence"]=="") ? "--" : $ParSummaryArray[$Semester][0]["SelfConfidence"];
						break;
					case "SelfDiscipline":
						$Display = ($ParSummaryArray[$Semester][0]["SelfDiscipline"]=="") ? "--" : $ParSummaryArray[$Semester][0]["SelfDiscipline"];
						break;
					case "Courtesy":
						$Display = ($ParSummaryArray[$Semester][0]["Courtesy"]=="") ? "--" : $ParSummaryArray[$Semester][0]["Courtesy"];
						break;
					case "Honesty":
						$Display = ($ParSummaryArray[$Semester][0]["Honesty"]=="") ? "--" : $ParSummaryArray[$Semester][0]["Honesty"];
						break;
					case "Responsibility":
						$Display = ($ParSummaryArray[$Semester][0]["Responsibility"]=="") ? "--" : $ParSummaryArray[$Semester][0]["Responsibility"];
						break;
					case "Cooperation":
						$Display = ($ParSummaryArray[$Semester][0]["Cooperation"]=="") ? "--" : $ParSummaryArray[$Semester][0]["Cooperation"];
						break;
				}
				if($count%2==0) {
					$SummaryTable .= "<tr>";
				}
				$SummaryTable .= "<td class='small_title' width='30%' valign='top' nowrap='nowrap' height='$LineHeight'>".$Title."</td><td class='small_title' width='5'>:</td><td class='tabletext' width='20%'>".$Display."</td>";
				if($count%2==1) {
					$SummaryTable .= "</tr>";
				}
				$count++;
			}
		}
		$SummaryTable .= "</table>";
	}

	return $SummaryTable;
}

# function to get signature table
function GENERATE_SIGNATURE_TABLE()
{
	global $i_Teaching_ClassTeacher, $eReportCard, $SettingArray, $LangArray, $Footer;
	global $ChiReportCard, $EngReportCard;
	
	$SignatureTitleArray = $eReportCard['DisplaySettingsArray']["Signature"];
	$SettingMiscArray = $SettingArray["Signature"];
	$SignatureCell = "";
	$CellCount = 0;
	
	if($SettingMiscArray['Principal']==1)
	{
		//$Title = $LangArray['Principal'];
		$Title = $EngReportCard['Principal']."<br>".$ChiReportCard['Principal'];
		$SignatureCell .= "<td valign='bottom' align='center'>";
		$SignatureCell .= "<table cellspacing='0' cellpadding='0' border='0'>";
		$SignatureCell .= "<tr><td align='center' class='small_title' height='60' valign='bottom'>____________________</td></tr>";
		$SignatureCell .= "<tr><td align='center' class='small_title' valign='bottom'>".$Title."</td></tr>";
		$SignatureCell .= "</table>";
		$SignatureCell .= "</td>";
		$CellCount++;
	}
	
	if($SettingMiscArray['ClassTeacher']==1)
	{
		//$Title = $LangArray['ClassTeacher'];
		$Title = $EngReportCard['ClassTeacher']."<br>".$ChiReportCard['ClassTeacher'];
		$SignatureCell .= "<td valign='bottom' align='center'>";
		$SignatureCell .= "<table cellspacing='0' cellpadding='0' border='0'>";
		$SignatureCell .= "<tr><td align='center' class='small_title' height='60' valign='bottom'>____________________</td></tr>";
		$SignatureCell .= "<tr><td align='center' class='small_title' valign='bottom'>".$Title."</td></tr>";
		$SignatureCell .= "</table>";
		$SignatureCell .= "</td>";
		$CellCount++;
	}	

	if($SettingMiscArray['SchoolChop']==1)
	{
		//$Title = $LangArray['SchoolChop'];
		$Title = $EngReportCard['SchoolChop']."<br>".$ChiReportCard['SchoolChop'];
		$SignatureCell .= "<td valign='bottom' align='center'>";
		$SignatureCell .= "<table cellspacing='0' cellpadding='0' border='0'>";
		$SignatureCell .= "<tr><td align='center' class='small_title' height='60' valign='bottom'>____________________</td></tr>";
		$SignatureCell .= "<tr><td align='center' class='small_title' valign='bottom'>".$Title."</td></tr>";
		$SignatureCell .= "</table>";
		$SignatureCell .= "</td>";
		$CellCount++;
	}

	
	if($SettingMiscArray['ParentGuardian']==1)
	{
		//$Title = $LangArray['ParentGuardian'];
		$Title = $EngReportCard['ParentGuardian']."<br>".$ChiReportCard['ParentGuardian'];
		$SignatureCell .= "<td valign='bottom' align='center'>";
		$SignatureCell .= "<table cellspacing='0' cellpadding='0' border='0'>";
		$SignatureCell .= "<tr><td align='center' class='small_title' height='60' valign='bottom'>____________________</td></tr>";
		$SignatureCell .= "<tr><td align='center' class='small_title' valign='bottom'>".$Title."</td></tr>";
		$SignatureCell .= "</table>";
		$SignatureCell .= "</td>";
		$CellCount++;
	}
	
	if($CellCount>0)
	{
		$SignatureTable = "<table width='100%' border='0' cellpadding='2' cellspacing='0' align='center'>\n";
		$SignatureTable .= "<tr>\n";
		$SignatureTable .= $SignatureCell;
		$SignatureTable .= "</tr>\n";
		$SignatureTable .= "</table>\n";
	}
	
	if($Footer!="") {
		$SignatureTable .= "<table width='100%' border='0' cellpadding='0' cellspacing='0'><tr><td class='small_text'>".str_replace("\n", "<br />", intranet_undo_htmlspecialchars($Footer))."</td></tr></table>\n";
	}

	return $SignatureTable;
}

function GET_SEMESTER_NUMBER()
{
	global $libreportcard, $SemesterArray;

	$sem_number = "-1";
	if(trim($libreportcard->Semester)=="FULL")
	{
		$sem_number = 0;
	} else
	{
		for($i=0; $i<sizeof($SemesterArray); $i++)
		{
			if(trim($libreportcard->Semester)==trim($SemesterArray[$i]))
			{
				$sem_number = $i+1;
				break;
			}
		}
	}
	return $sem_number;
}

function CHECK_CSV_FILE_FORMAT($ParHeaderArray, $ParDefaultHeaderArray)
{
	# Check Title Row
	$format_wrong = false;				

	for ($i=0; $i<sizeof($ParDefaultHeaderArray); $i++)
	{			
		if ($ParHeaderArray[$i]!=$ParDefaultHeaderArray[$i])
		{
			$format_wrong = true;
			break;
		}
	}

	return $format_wrong;		
}

#######################################################
# New argument added by Andy Chan:
# $ReportType = summary || award || merit || eca || 
#								remark || interschool || schoolservice
# as defined in the default_header.php
#######################################################
function GET_CSV_FILE_CONTENT($ParFile, $ParDefaultHeaderArray, $ReportType="")
{
	global $libreportcard, $lf, $StudentRegNoArray, $SemesterArray;
	
	######### For adding associated array ##########
	global $HeaderSettingMap;
	################################################
	
	$ClassArr = $libreportcard->GET_CLASSES();
	
	$TargetArray = $SemesterArray;
	$TargetArray[] = "FULL";
	for($i=0; $i<sizeof($TargetArray); $i++) {
		$t_semester = trim($TargetArray[$i]);
		$sem_number = GET_SEMESTER_NUMBER($t_semester);
		if($sem_number>=0) {
			for ($j = 0; $j < sizeof($ClassArr); $j++) {
				$TempClass = str_replace(".", "", str_replace("/", "", $ClassArr[$j]));
				$TargetFile = $ParFile."/".trim($libreportcard->Year)."_".$sem_number."_".$TempClass.".csv";
				
				if (file_exists($TargetFile)) {
					$data = $lf->file_read_csv($TargetFile);
					if(!empty($data)) {
						$header_row = array_shift($data);
						$wrong_format = CHECK_CSV_FILE_FORMAT($header_row, $ParDefaultHeaderArray);
						#########################################
						# Ignore wrong format checking and force
						# assign associated array
						$wrong_format = false;
						#########################################
						if(!$wrong_format) {
							for($k=0; $k<sizeof($data); $k++) {
								$reg_no = array_shift($data[$k]);
								$reg_no = trim(str_replace("#", "", $reg_no));
								$student_id = trim($StudentRegNoArray[$reg_no]);
								if($student_id!="") {
									// add associated array to $data also
									if ($ReportType != "") {
										for($r=0; $r<sizeof($data[$k]); $r++) {
											#$data[$k][$HeaderSettingMap[$ReportType][$ParDefaultHeaderArray[$r+1]]] = $data[$k][$r];
											$data[$k][$HeaderSettingMap[$ReportType][$header_row[$r+1]]] = $data[$k][$r];
										}
									}
									$ReturnArray[$student_id][$t_semester][] = $data[$k];
								}
							}
						}
					}
				}
			}
		}
	}
	return $ReturnArray;
}

# function to get result table
function GENERATE_RESULT_TABLE($ParSummaryTable, $ParStudentResultArray, $ParSubjectTeacherCommentArray)
{
	global $eReportCard, $SubjectArray, $ColumnArray, $ReportCellSetting, $ShowFullMark, $libreportcard;
	global $FailedArray, $DistinctionArray, $PFArray, $SettingArray;
	global $AllSubjectArray, $ShowOverallPosition, $OverallPositionRange, $HideNotEnrolled;
	global $ShowSubjectTeacherComment, $LangArray, $BilingualReportTitle;
	global $LineHeight, $SettingArray;
		
	# Report Type
	# 1 - Display the overall result of Parent Subject AND Component Subject(s)
	# 2 - Display the overall result of Parent Subject ONLY
	# 3 - Display the overall result of Component Subject(s) ONLY
	$ResultDisplayType = $SettingArray["ResultDisplayType"];
	$ResultCalculationType = $SettingArray["ResultCalculationType"];
	$SubjectTitleType = $SettingArray["SubjectTitleType"];
	$ShowColumnPercentage = $SettingArray["ShowColumnPercentage"];
	$MarkTypeDisplay = $SettingArray["MarkTypeDisplay"];
	$HideOverallResult = $SettingArray["HideOverallResult"];

	# get subject title cell
	if($BilingualReportTitle==1 || $SubjectTitleType==0) {
		$SubjectTitleCell = "<td width='50%' class='small_title'>".$eReportCard['EngSubject']."</td><td width='50%' class='small_title'>".$eReportCard['ChiSubject']."</td>";
	}
	else {
		$SubjectTitleCell = "<td class='small_title'>".$LangArray['Subject']."</td>";
	}

	$ExtraColumn = 0;
	$ColumnSize = sizeof($ColumnArray);
	$ResultTable = "<table width='100%' border=0 cellspacing='0' cellpadding='2' class='report_border'>";
	$ResultTable .= "<tr>
						<td width='25%' class='report_formfieldtitle'>
							<table width='100%' border='0' cellpadding='0' cellspacing='0'><tr>".$SubjectTitleCell."</tr>
							</table>
						</td>";
	if($ShowFullMark==1)
	{
		$ResultTable .= "<td width='5%' class='border_left small_title report_formfieldtitle' align='center'>".$LangArray['FullMark']."</td>";
		$ExtraColumn++;
	}
	
	list($op_from, $op_to) = explode(",", $OverallPositionRange);
	for($i=0; $i<sizeof($ColumnArray); $i++)
	{
		list($ReportColumnID, $ColumnTitle, $Weight, $ShowPosition, $PositionRange) = $ColumnArray[$i];
		if($Weight!="" && $ShowColumnPercentage==1)
		{
			$ColumnTitle = $ColumnTitle."&nbsp;".$Weight."%";
			$WeightArray[$ReportColumnID] = $Weight;
		}

		$ResultTable .= "<td class='border_left report_formfieldtitle' width='10%'><table border=0 cellpadding=0 cellspacing=0 width='100%'>";
		$ResultTable .= "<tr height='75%'><td align='center' class='small_title' colspan='3'>".$ColumnTitle."</td></tr>";

		$ResultTable .= "</table></td>";
	}

	if($ColumnSize>1 && $HideOverallResult!=1)
	{
		$ResultTable .= "<td class='border_left report_formfieldtitle' width='10%'><table border=0 cellpadding=0 cellspacing=0 width='100%' align='center'>";
		$ResultTable .= "<tr height='75%'><td align='center' class='small_title' colspan='3'>".$LangArray['OverallResult']."</td></tr>";
		$ResultTable .= "</table></td>";
		$ExtraColumn++;
	}
	if($ShowSubjectTeacherComment==1)
	{
		$ResultTable .= "<td width='10%' class='border_left small_title report_formfieldtitle' align='center'>".$LangArray['SubjectTeacherComment']."</td>";
	}
	$ResultTable .= "</tr>";
	
	$ColumnSpan = $ColumnSize + $ExtraColumn + 1;
	$IsFirst = 1;
	if(is_array($SubjectArray))
	{
		$count = 0;
		foreach($SubjectArray as $CodeID => $Data)
		{
			if(is_array($Data))
			{
				foreach($Data as $CmpCodeID => $InfoArr)
				{
					list($ReportSubjectID, $SubjectID, $EngSubjectName, $ChiSubjectName, $FullMark, $Scale, $HidePosition) = $InfoArr;
					
					# Hide not enrolled subject according to the setting
					if($HideNotEnrolled==1 && (empty($ParStudentResultArray[$SubjectID]["Overall"]) || strcmp($ParStudentResultArray[$SubjectID]["Overall"]["Grade"], "/")==0))
						continue;

					$IsComponent = ($CmpCodeID>0) ? 1 : 0;					
					if (($IsComponent)&&($SettingArray['ResultDisplayType'] == 1)) continue;
					
					$top_style = ($IsComponent==0 && $IsFirst==0) ? "border_top" : "";
					$IsFirst = 0;
					$Prefix = ($IsComponent==1) ? "&nbsp;&nbsp;&nbsp;&nbsp;" : "";

					# Highest Grade
					$MaxGrade = $DistinctionArray[$SubjectID]["G"];
					$FirstRowSetting = $ReportCellSetting[$ColumnArray[0][0]][$ReportSubjectID];
					$FullMark = (($FirstRowSetting=="G")&&($MaxGrade!="")) ? $MaxGrade : $FullMark;

					# check whether is parent/component subject
					$IsParent = (sizeof($AllSubjectArray[$SubjectID])>1) ? 1 : 0;
					$IsComponent = (empty($AllSubjectArray[$SubjectID])) ? 1 : 0;

					$ResultShow = (!($ResultDisplayType==2 && $IsParent==1) && !($IsParent==1 && $ResultCalculationType==1)) ? 1 : 0;
					$OverallShow = (!($ResultDisplayType==2 && $IsParent==1) && !($ResultDisplayType==1 && $IsComponent==1)) ? 1 : 0;
					
					# get subject display cell
					if($BilingualReportTitle==1 || $SubjectTitleType==0) {
						$SubjectCell = "<td width='50%' class='small_text'>".$Prefix.$EngSubjectName."</td><td width='50%' class='small_text'>".$Prefix.$ChiSubjectName."</td>";
					}
					else {
						$SubjectDisplayName = ($SubjectTitleType==1) ? $EngSubjectName : $ChiSubjectName;
						$SubjectCell = "<td class='small_text'>".$Prefix.$SubjectDisplayName."</td>";
					}

					$ResultTable .= "<tr>";
					$ResultTable .= "<td class='tabletext $top_style'>
										<table width='100%' border='0' cellpadding='0' cellspacing='0'><tr>".$SubjectCell."</tr></table>
									</td>";
					$ResultTable .= ($ShowFullMark==1) ? "<td class='tabletext border_left $top_style' align='center'>".($ResultShow==1?$FullMark."&nbsp;":"&nbsp;")."</td>" : "";
					
					for($i=0; $i<sizeof($ColumnArray); $i++)
					{
						list($ReportColumnID, $ColumnTitle, $Weight, $ShowPosition, $PositionRange) = $ColumnArray[$i];
						$CellSetting = $ReportCellSetting[$ReportColumnID][$ReportSubjectID];
						$Weight = $WeightArray[$ReportColumnID];
						list($p_form, $p_to) = explode(",", $PositionRange);
						
						$DisplayResult = "";
						if(!$ResultShow)
						{
							$ResultTable .= "<td class='border_left $top_style' align='center'>&nbsp;</td>";
						}
						else 
						{
							if($CellSetting!="N/A")
							{
								$StylePrefix = "";
								$StyleSuffix = "";
								if($CellSetting=="S") {
									$s_raw_mark = $ParStudentResultArray[$SubjectID][$ReportColumnID]["RawMark"];
									$s_weighted_mark = $ParStudentResultArray[$SubjectID][$ReportColumnID]["WeightedMark"];
									$s_grade = $ParStudentResultArray[$SubjectID][$ReportColumnID]["Grade"];

									if($s_raw_mark>=0) {
										if($MarkTypeDisplay==2) {
											$DisplayResult = $s_grade;
											//aki
											list($StylePrefix, $StyleSuffix, $IsDistinct, $IsFailed) = $libreportcard->GET_STYLE("G", $DisplayResult, $FailedArray[$SubjectID], $DistinctionArray[$SubjectID], $PFArray[$SubjectID]);
										}
										else {
											//aki
											list($StylePrefix, $StyleSuffix, $IsDistinct, $IsFailed) = $libreportcard->GET_STYLE($CellSetting, $s_raw_mark, $FailedArray[$SubjectID], $DistinctionArray[$SubjectID], $PFArray[$SubjectID]);
											$DisplayResult = ($MarkTypeDisplay==1) ? $s_raw_mark : $s_weighted_mark;
										}
									}
									else 
										$DisplayResult = $libreportcard->SpecialMarkArray[trim($s_raw_mark)];
								}
								else {
									$DisplayResult = $ParStudentResultArray[$SubjectID][$ReportColumnID]["Grade"];
									if($DisplayResult!="/" && $DisplayResult!="abs") {
										//aki
										list($StylePrefix, $StyleSuffix, $IsDistinct, $IsFailed) = $libreportcard->GET_STYLE($CellSetting, $DisplayResult, $FailedArray[$SubjectID], $DistinctionArray[$SubjectID], $PFArray[$SubjectID]);
									}
								}
								$DisplayResult = $StylePrefix.$DisplayResult.$StyleSuffix;
								$FormPosition = $ParStudentResultArray[$SubjectID][$ReportColumnID]["OMF"];
								$ClassPosition = $ParStudentResultArray[$SubjectID][$ReportColumnID]["OMC"];
							}

							$ResultTable .= "<td class='border_left $top_style' width='10%'><table border=0 cellpadding=0 cellspacing=0 width='100%' height='$LineHeight'>";
							$ResultTable .= "<tr>";
							if($ShowPosition==1 || $ShowPosition==2)
							{
								$ResultTable .= "<td align='center' class='tabletext' width='50%'>".($DisplayResult==""?"--":$DisplayResult)."</td>";
								$ResultTable .= "<td width=5>&nbsp;</td>";

								$PosDisplay = ($ShowPosition==1) ? $ClassPosition : $FormPosition;
								$PosDisplay = ($PosDisplay>0 && ($PosDisplay>=$p_from && $PosDisplay<=$p_to)) ? $PosDisplay : "*";			
								
								$ResultTable .= ($HidePosition==0 && $CellSetting=="S") ? "<td align='center' class='tabletext' width='50%'>(".$PosDisplay.")</td>" : "<td align='center' class='tabletext width='50%'>(*)</td>";
							}
							else
							{
								$ResultTable .= "<td align='center' class='tabletext'>".($DisplayResult==""?"--":$DisplayResult)."</td>";
							}
							$ResultTable .= "</tr>";
							$ResultTable .= "</table></td>";
						}
					}
					
					if($ColumnSize>1 && $HideOverallResult!=1)
					{
						if(!$OverallShow)
						{
							$ResultTable .= "<td class='tabletext border_left $top_style' align='center'>&nbsp;</td>";
						}
						else
						{
							$StylePrefix = "";
							$StyleSuffix = "";
							if($CellSetting=="S") {
								$s_overall_mark = $ParStudentResultArray[$SubjectID]["Overall"]["Mark"];
								$s_overall_grade = $ParStudentResultArray[$SubjectID]["Overall"]["Grade"];
									
								if($s_overall_mark>=0) {
									if($MarkTypeDisplay==2) {
										$OverallResult = $s_overall_grade;										
										// aki
										list($StylePrefix, $StyleSuffix, $IsDistinct, $IsFailed) = $libreportcard->GET_STYLE("G", $OverallResult, $FailedArray[$SubjectID], $DistinctionArray[$SubjectID], $PFArray[$SubjectID]);
									}
									else {
										$OverallResult = $s_overall_mark;										
										// aki
										list($StylePrefix, $StyleSuffix, $IsDistinct, $IsFailed) = $libreportcard->GET_STYLE($CellSetting, $OverallResult, $FailedArray[$SubjectID], $DistinctionArray[$SubjectID], $PFArray[$SubjectID]);
										
									}
								}
								else
									$OverallResult = $libreportcard->SpecialMarkArray[trim($s_overall_mark)];
							}
							else {
								$OverallResult = $ParStudentResultArray[$SubjectID]["Overall"]["Grade"];
								if($OverallResult!="/" && $OverallResult!="abs") {
									// aki
									list($StylePrefix, $StyleSuffix, $IsDistinct, $IsFailed) = $libreportcard->GET_STYLE($CellSetting, $OverallResult, $FailedArray[$SubjectID], $DistinctionArray[$SubjectID], $PFArray[$SubjectID]);
								}
							}
							
							$OverallResult = $StylePrefix.$OverallResult.$StyleSuffix;
							$OverallFormPosition = $ParStudentResultArray[$SubjectID]["Overall"]["OMF"];
							$OverallClassPosition = $ParStudentResultArray[$SubjectID]["Overall"]["OMC"];
							
							$ResultTable .= "<td class='border_left $top_style' width='10%'><table border=0 cellpadding=0 cellspacing=0 width='100%'>";
							$ResultTable .= "<tr>";
							//if($ShowOverallPosition==1 || $ShowOverallPosition==2)
							if(false)
							{
								$ResultTable .= "<td align='center' class='tabletext' width='50%'>".($OverallResult==""?"--":$OverallResult)."</td>";
								$ResultTable .= "<td>&nbsp;</td>";

								$OverallPosDisplay = ($ShowOverallPosition==1) ? $OverallClassPosition : $OverallFormPosition;
								$OverallPosDisplay = ($OverallPosDisplay>0 && ($OverallPosDisplay>=$op_from && $OverallPosDisplay<=$op_to)) ? $OverallPosDisplay : "*";	

								//aki
								if ($IsFailed) $OverallPosDisplay = "*";
								$ResultTable .= ($HidePosition==0 && $CellSetting=="S") ? "<td align='center' class='tabletext' width='50%'>(".$OverallPosDisplay.")</td>" : "<td align='center' class='tabletext' width='50%'>(*)</td>";
							}
							else
							{
								$ResultTable .= "<td align='center' class='tabletext'>".($OverallResult==""?"--":$OverallResult)."</td>";
							}
							$ResultTable .= "</tr>";
							$ResultTable .= "</table></td>";
						}
					}
					if($ShowSubjectTeacherComment==1)
					{
						$Comment = $ParSubjectTeacherCommentArray[$SubjectID];
						$ResultTable .= "<td class='tabletext border_left $top_style' align='center'>".($Comment==""?"&nbsp;":"<li>".$Comment."</li>")."</td>";
					}
					$ResultTable .= "</tr>";
					$count++;
				}
			}
		}
	}
	else
	{
		$ResultTable .= "<tr><td align='center' class='tabletext' colspan='".$ColumnSpan."'>".$eReportCard['NoRecord']."</td></tr>";
	}
	$ResultTable .= "</table>";
	if($ParSummaryTable!="")
	{
		$ResultTable .= "<table width='100%' cellspacing='0' cellpadding='2' class='summary_table'>";
		$ResultTable .= "<tr><td colspan='".$ColumnSpan."' height='100' valign='top'>".$ParSummaryTable."</td></tr>";
		$ResultTable .= "</table>";
	}
	
	return $ResultTable;
}

################################ Function End #####################################
?>