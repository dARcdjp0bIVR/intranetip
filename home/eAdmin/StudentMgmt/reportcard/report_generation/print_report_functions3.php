<?php
############################ Function Start ######################################

# function to get Title Table
function GENERATE_TITLE_TABLE()
{
	global $ReportTitle, $image_path, $intranet_root, $lf;
	
	# get school badge
	$SchoolLogo = GET_SCHOOL_BADGE();
		
	# get school name
	$SchoolName = GET_SCHOOL_NAME();	

	$TitleTable = "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>\n";
	$TitleTable .= "<tr><td width='120' align='center'>".($SchoolLogo==""?"&nbsp;":$SchoolLogo)."</td>";
	if(!empty($SchoolName))
	{
		$TitleTable .= "<td>";
		$TitleTable .= "<table width='100%' border='0' cellpadding='4' cellspacing='0' align='center'>\n";
		if(!empty($SchoolName))
			$TitleTable .= "<tr><td nowrap='nowrap' class='report_title'>".$SchoolName."</td></tr>\n";
		$TitleTable .= "</table>\n";
		$TitleTable .= "</td>";
	}
	$TitleTable .= "<td width='120' align='center'>&nbsp;</td></tr>";
	if(!empty($ReportTitle))
	{
		list($ReportTitle1, $ReportTitle2) = explode("::", $ReportTitle);
		if(!empty($ReportTitle1))
			$TitleTable .= "<tr><td colspan='3' nowrap='nowrap' class='report_contenttitle' align='center'><u>".$ReportTitle1."</u></td></tr>";
		if(!empty($ReportTitle2))
			$TitleTable .= "<tr><td colspan='3' nowrap='nowrap' class='report_contenttitle' align='center'><u>".$ReportTitle2."</u></td></tr>";
	}
	$TitleTable .= "</table>";

	return $TitleTable;
}

# function to get Student Info Table
function GENERATE_STUDENT_INFO_TABLE($ParInfoArray)
{
	global $SettingStudentInfo, $StudentInfoTitleArray, $ClassTeacherArray, $IssueDate, $LangArray;
	
	list($ClassName, $ClassNumber, $StudentName, $RegNo) = $ParInfoArray;
	if(!empty($SettingStudentInfo))
	{
		$count = 0;
		$StudentInfoTable = "<table width='100%' border='0' cellpadding='2' cellspacing='0' align='center'>\n";
		for($i=0; $i<sizeof($StudentInfoTitleArray); $i++)
		{
			$SettingID = trim($StudentInfoTitleArray[$i]);
			if($SettingStudentInfo[$SettingID]==1)
			{
				$Title = $LangArray[$SettingID];
				$Title = str_replace("<br />", " ", $Title);
				$Display = "";
				switch($SettingID)
				{
					case "Name":
						$Display = $StudentName;
						break;
					case "ClassName":
						$Display = $ClassName;
						break;
					case "ClassNumber":
						$Display = $ClassNumber;
						break;
					case "ClassTeacher":
						$Display = (!empty($ClassTeacherArray[$ClassName])) ? implode(", ", $ClassTeacherArray[$ClassName]) : "";
						break;
					case "DateOfIssue":
						$Display = $IssueDate;
						break;
					case "StudentNo":
						$Display = $RegNo;
						break;
				}

				if($count%3==0) {
					$StudentInfoTable .= "<tr>\n";
				}

				if($count%3==1)
					$DisplayAlign =  "align='center'";
				else if($count%3==2)
					$DisplayAlign =  "align='right'";
				else 
					$DisplayAlign = "align='left'";

				$StudentInfoTable .= "<td valign='top' width='33%' {$DisplayAlign}><span class='tabletext'>".$Title."</span> : <span class='tabletext'>".$Display."</span></td>";
				if($count%3==2) {
					$StudentInfoTable .= "</tr>\n";
				}
				$count++;
			}
		}
		$StudentInfoTable .= "</table>\n";
	}

	return $StudentInfoTable;
}

function GENERATE_SUMMARY_MISC_TABLE($ParInfoArray, $ClassHighestAverage, $ParSummaryArray, $ParClassTeacherComment, $ParStudentID)
{
	global $SummaryTitleArray, $MiscTitleArray;
	global $SettingMiscArray, $SettingSummaryArray;
	global $StudentAwardArray, $StudentMeritArray, $StudentECAArray, $StudentRemarkArray;
	global $image_path, $LAYOUT_SKIN, $Semester, $LangArray;
	global $LineHeight;
	global $StudentInterSchoolArray, $StudentSchoolServiceArray;
	
	$ParStudentID = trim($ParStudentID);
	list($GT, $AM, $GPA, $OMC, $OMCT, $OMF, $OMFT) = $ParInfoArray[$Semester];

	if(!empty($SettingSummaryArray) || !empty($SettingMiscArray))
	{
		$count = 0;
		$SummaryMiscTable = "";

		# summary settings
		for($k=0; $k<sizeof($SummaryTitleArray); $k++)
		{
			$SettingID = trim($SummaryTitleArray[$k]);
			if($SettingSummaryArray[$SettingID]==1)
			{
				$Display = "";
				switch($SettingID)
				{
					case "GrandTotal":
						$Display = $GT;
						break;
					case "GPA":
						$Display = $GPA;
						break;
					case "AverageMark":
						$Display = $AM;
						break;
					case "FormPosition":
						$Display = ($OMF>0) ? $OMF : "--";
						break;
					case "ClassPosition":
						$Display = ($OMC>0) ? $OMC : "--";
						break;
					case "ClassPupilNumber":
						$Display = $OMCT;
						break;
					case "ClassHighestAverage":
						$Display = $ClassHighestAverage[$Semester];
						break;
					case "Conduct":
						$Display = ($ParSummaryArray[$Semester][3]==""?"--":$ParSummaryArray[$Semester][3]);
						break;
					case "Politeness":
						$Display = ($ParSummaryArray[$Semester][4]==""?"--":$ParSummaryArray[$Semester][4]);
						break;
					case "Behaviour":
						$Display = ($ParSummaryArray[$Semester][5]==""?"--":$ParSummaryArray[$Semester][5]);
						break;
					case "Application":
						$Display = ($ParSummaryArray[$Semester][6]==""?"--":$ParSummaryArray[$Semester][6]);
						break;
					case "Tidiness":
						$Display = ($ParSummaryArray[$Semester][7]==""?"--":$ParSummaryArray[$Semester][7]);
						break;
					case "Motivation":
						$Display = ($ParSummaryArray[$Semester][8]=="") ? "--" : $ParSummaryArray[$Semester][8];
						break;				
					case "SelfConfidence":
						$Display = ($ParSummaryArray[$Semester][9]=="") ? "--" : $ParSummaryArray[$Semester][9];
						break;
					case "SelfDiscipline":
						$Display = ($ParSummaryArray[$Semester][10]=="") ? "--" : $ParSummaryArray[$Semester][10];
						break;
					case "Courtesy":
						$Display = ($ParSummaryArray[$Semester][11]=="") ? "--" : $ParSummaryArray[$Semester][11];
						break;
					case "Honesty":
						$Display = ($ParSummaryArray[$Semester][12]=="") ? "--" : $ParSummaryArray[$Semester][12];
						break;
					case "Responsibility":
						$Display = ($ParSummaryArray[$Semester][13]=="") ? "--" : $ParSummaryArray[$Semester][13];
						break;
					case "Cooperation":
						$Display = ($ParSummaryArray[$Semester][14]=="") ? "--" : $ParSummaryArray[$Semester][14];
						break;
						
						
					case "DaysAbsent":
						$Display = ($ParSummaryArray[$Semester][0]==""?"--":$ParSummaryArray[$Semester][0]);
						break;
					case "TimesLate":
						$Display = ($ParSummaryArray[$Semester][1]==""?"--":$ParSummaryArray[$Semester][1]);
						break;
					case "AbsentWOLeave":
						$Display = ($ParSummaryArray[$Semester][2]==""?"--":$ParSummaryArray[$Semester][2]);
						break;
				}
				
				$Title = $LangArray[$SettingID];
				if($count%5==0) {
					$SummaryMiscTable .= "<br /><table width='100%' border='0' cellpadding='0' cellspacing='0' align='center' class='report_border'><tr valign='top'>";
					$TableEnd = 0;
					$IsFirst = 1;
				}
				$SummaryMiscTable .= ($IsFirst==0) ? "<td width='20%' class='border_left'>" : "<td width='20%'>";
				$IsFirst = 0;
				$SummaryMiscTable .= "<table width='100%' border='0' cellpaddin='4' cellspacing='0'>
										<tr><td height='$LineHeight' align='center' class='small_title report_formfieldtitle'>".$Title."</td></tr>
										<tr><td height='$LineHeight' align='center' class='small_text'>".($Display==''?'--':$Display)."</td></tr>
									</table>";
				$SummaryMiscTable .= "</td>";
					
				if($count%5==4) {
					$SummaryMiscTable .= "</tr></table>";
					$TableEnd = 1;
					$count = 0;
				}
				else
					$count++;
			}
		}
		
		# misc settings
		for($k=0; $k<sizeof($MiscTitleArray); $k++)
		{
			$SettingID = trim($MiscTitleArray[$k]);
			if($SettingMiscArray[$SettingID]==1)
			{
				$Title = $LangArray[$SettingID];
				if($count%5==0) {
					$SummaryMiscTable .= "<br /><table width='100%' border='0' cellpadding='0' cellspacing='0' align='center' class='report_border'><tr valign='top'>";
					$TableEnd = 0;
					$IsFirst = 1;			
				}

				$Display = "";
				switch($SettingID)
				{
					case "ClassTeacherComment":
						$Display = $ParClassTeacherComment;
						break;
					case "Awards":
						$Display = $StudentAwardArray[$ParStudentID][$Semester];
						break;
					case "MeritsAndDemerits":
						$Display = $StudentMeritArray[$ParStudentID][$Semester];
						break;
					case "ECA":
						$Display = $StudentECAArray[$ParStudentID][$Semester];
						break;				
					case "Remark":
						$Display = $StudentRemarkArray[$ParStudentID][$Semester];
						break;
					case "InterSchoolCompetition":
						$Display = $StudentInterSchoolArray[$ParStudentID][$Semester];
						break;
					case "SchoolService":
						$Display = $StudentSchoolServiceArray[$ParStudentID][$Semester];
						break;
				}

				$SummaryMiscTable .= ($IsFirst==0) ? "<td width='20%' class='border_left'>" : "<td width='20%'>";
				$IsFirst = 0;
				$SummaryMiscTable .= "<table width='100%' border='0' cellpaddin='4' cellspacing='0'>
										<tr><td height='$LineHeight' align='center' class='small_title report_formfieldtitle'>".$Title."</td></tr>
										<tr><td height='$LineHeight' align='center' class='small_text'>".($Display==''?'--':$Display)."</td></tr>
									</table>";
				$SummaryMiscTable .= "</td>";
					
				if($count%5==4) {
					$SummaryMiscTable .= "</tr></table>";
					$TableEnd = 1;
					$count = 0;
				}
				else
					$count++;
			}
		}

		if($TableEnd==0) {
			//$SummaryMiscTable .= ($count>0) ? "<td colspan='".(5-$count)."' class='border_left'>&nbsp;</td>" : "";
			$SummaryMiscTable .= "</tr></table>";
		}
	}
	return $SummaryMiscTable;
}

# function to get signature table
function GENERATE_SIGNATURE_TABLE()
{
	global $i_Teaching_ClassTeacher, $eReportCard, $SettingArray, $LangArray, $Footer;
	
	$SignatureTitleArray = $eReportCard['DisplaySettingsArray']["Signature"];
	$SettingMiscArray = $SettingArray["Signature"];
	$SignatureCell = "";
	$CellCount = 0;
	for($i=0; $i<sizeof($SignatureTitleArray); $i++)
	{	
		$SettingID = trim($SignatureTitleArray[$i]);
		if($SettingMiscArray[$SettingID]==1)
		{
			$Title = $LangArray[$SettingID];
			$SignatureCell .= "<td valign='bottom' align='center'>";
			$SignatureCell .= "<table cellspacing='0' cellpadding='0' border='0'>";
			$SignatureCell .= "<tr><td align='center' class='small_title' height='60' valign='bottom'>____________________</td></tr>";
			$SignatureCell .= "<tr><td align='center' class='small_title' valign='bottom'>".$Title."</td></tr>";
			$SignatureCell .= "</table>";
			$SignatureCell .= "</td>";
			$CellCount++;
		}
	}

	if($CellCount>0)
	{
		$SignatureTable = "<table width='100%' border='0' cellpadding='4' cellspacing='0' align='center'>\n";
		$SignatureTable .= "<tr><td colspan='".$CellCount."' class='small_title'>".$LangArray["Signature"]." : </td></tr>\n";
		$SignatureTable .= "<tr>\n";
		$SignatureTable .= $SignatureCell;
		$SignatureTable .= "</tr>\n";
		$SignatureTable .= "</table>\n";
	}
	
	if($Footer!="") {
		$SignatureTable .= "<table width='100%' border='0' cellpadding='0' cellspacing='0'><tr><td class='small_text'>".str_replace("\n", "<br />", intranet_undo_htmlspecialchars($Footer))."</td></tr></table>\n";
	}
						
	return $SignatureTable;
}


function GET_SEMESTER_NUMBER()
{
	global $libreportcard, $SemesterArray;

	$sem_number = "-1";
	if(trim($libreportcard->Semester)=="FULL")
	{
		$sem_number = 0;
	} else
	{
		for($i=0; $i<sizeof($SemesterArray); $i++)
		{
			if(trim($libreportcard->Semester)==trim($SemesterArray[$i]))
			{
				$sem_number = $i+1;
				break;
			}
		}
	}
	return $sem_number;
}

function CHECK_CSV_FILE_FORMAT($ParHeaderArray, $ParDefaultHeaderArray)
{
	# Check Title Row
	$format_wrong = false;				

	for ($i=0; $i<sizeof($ParDefaultHeaderArray); $i++)
	{			
		if ($ParHeaderArray[$i]!=$ParDefaultHeaderArray[$i])
		{
			$format_wrong = true;
			break;
		}
	}

	return $format_wrong;		
}

function GET_CSV_FILE_CONTENT($ParFile, $ParDefaultHeaderArray)
{
	global $libreportcard, $lf, $StudentRegNoArray, $SemesterArray;
	
	$ClassArr = $libreportcard->GET_CLASSES();
	
	$TargetArray = $SemesterArray;
	$TargetArray[] = "FULL";
	for($i=0; $i<sizeof($TargetArray); $i++)
	{
		$t_semester = trim($TargetArray[$i]);
		$sem_number = GET_SEMESTER_NUMBER($t_semester);
		if($sem_number>=0)
		{
			for ($j = 0; $j < sizeof($ClassArr); $j++)
			{
				$TempClass = str_replace(".", "", str_replace("/", "", $ClassArr[$j]));
				$TargetFile = $ParFile."/".trim($libreportcard->Year)."_".$sem_number."_".$TempClass.".csv";
				if(file_exists($TargetFile))
				{
					$data = $lf->file_read_csv($TargetFile);
					if(!empty($data))
					{
						$header_row = array_shift($data);
						$wrong_format = CHECK_CSV_FILE_FORMAT($header_row, $ParDefaultHeaderArray);
						if(!$wrong_format)
						{
							for($k=0; $k<sizeof($data); $k++)
							{
								$reg_no = array_shift($data[$k]);
								$reg_no = trim(str_replace("#", "", $reg_no));
								$student_id = trim($StudentRegNoArray[$reg_no]);
								if($student_id!="")
								{
									if(sizeof($data[$k])>1) {
										$ReturnArray[$student_id][$t_semester] = $data[$k];
									}
									else {
										$ReturnArray[$student_id][$t_semester] .= $data[$k][0];
									}
								}
							}
						}
					}
				}
			}
		}
	}
	
	return $ReturnArray;
}

# function to get result table
function GENERATE_RESULT_TABLE($ParStudentResultArray, $ParSubjectTeacherCommentArray)
{
	global $eReportCard, $SubjectArray, $ColumnArray, $ReportCellSetting, $ShowFullMark, $libreportcard;
	global $FailedArray, $DistinctionArray, $PFArray, $SettingArray;
	global $AllSubjectArray, $ShowOverallPosition, $OverallPositionRange, $HideNotEnrolled;
	global $ShowSubjectTeacherComment, $LangArray, $BilingualReportTitle;
	global $LineHeight;
	
	# Report Type
	# 1 - Display the overall result of Parent Subject AND Component Subject(s)
	# 2 - Display the overall result of Parent Subject ONLY
	# 3 - Display the overall result of Component Subject(s) ONLY
	$ResultDisplayType = $SettingArray["ResultDisplayType"];
	$ResultCalculationType = $SettingArray["ResultCalculationType"];
	$SubjectTitleType = $SettingArray["SubjectTitleType"];
	$ShowColumnPercentage = $SettingArray["ShowColumnPercentage"];
	$MarkTypeDisplay = $SettingArray["MarkTypeDisplay"];
	$HideOverallResult = $SettingArray["HideOverallResult"];

	# get subject title cell
	if($BilingualReportTitle==1 || $SubjectTitleType==0) {
		$SubjectTitleCell = "<td width='50%' class='small_title'>".$eReportCard['EngSubject']."</td><td width='50%' class='small_title'>".$eReportCard['ChiSubject']."</td>";
	}
	else {
		$SubjectTitleCell = "<td class='small_title'>".$eReportCard['Subject']."</td>";
	}

	$ExtraColumn = 0;
	$ColumnSize = sizeof($ColumnArray);
	$ResultTable = "<table width='100%' border=0 cellspacing='0' cellpadding='4' class='report_border'>";
	$ResultTable .= "<tr>
						<td width='25%' class='report_formfieldtitle'>
							<table width='100%' border='0' cellpadding='0' cellspacing='0'><tr>".$SubjectTitleCell."</tr>
							</table>
						</td>";
	if($ShowFullMark==1)
	{
		$ResultTable .= "<td width='5%' class='border_left small_title report_formfieldtitle' align='center'>".$LangArray['FullMark']."</td>";
		$ExtraColumn++;
	}
	
	list($op_from, $op_to) = explode(",", $OverallPositionRange);
	for($i=0; $i<sizeof($ColumnArray); $i++)
	{
		list($ReportColumnID, $ColumnTitle, $Weight, $ShowPosition, $PositionRange) = $ColumnArray[$i];
		if($Weight!="" && $ShowColumnPercentage==1)
		{
			$ColumnTitle = $ColumnTitle."&nbsp;".$Weight."%";
			$WeightArray[$ReportColumnID] = $Weight;
		}

		$ResultTable .= "<td class='border_left report_formfieldtitle' width='10%'><table border=0 cellpadding=0 cellspacing=0 width='100%'>";
		$ResultTable .= "<tr><td align='center' class='small_title' colspan='3'>".$ColumnTitle."</td></tr>";
		$ResultTable .= "</table></td>";
		if($ShowPosition==1 || $ShowPosition==2)
		{
			$ResultTable .= "<td align='center' class='border_left report_formfieldtitle small_title' width='10%'>".$LangArray['Position']."</td>";
				$ExtraColumn++;
		}
	}

	if($ColumnSize>1 && $HideOverallResult!=1)
	{
		$ResultTable .= "<td class='border_left report_formfieldtitle' width='10%'><table border=0 cellpadding=0 cellspacing=0 width='100%' align='center'>";
		$ResultTable .= "<tr><td align='center' class='small_title' colspan='3'>".$LangArray['OverallResult']."</td></tr>";
		$ResultTable .= "</table></td>";
		$ExtraColumn++;
		if($ShowOverallPosition==1 || $ShowOverallPosition==2)
		{
			$ResultTable .= "<td align='center' class='border_left report_formfieldtitle small_title' width='10%'>".$LangArray['Position']."</td>";
			$ExtraColumn++;
		}
	}
	if($ShowSubjectTeacherComment==1)
	{
		$ResultTable .= "<td width='10%' class='border_left small_title report_formfieldtitle' align='center'>".$LangArray['SubjectTeacherComment']."</td>";
	}
	$ResultTable .= "</tr>";
	
	$ColumnSpan = $ColumnSize + $ExtraColumn + 1;
	$IsFirst = 1;
	if(is_array($SubjectArray))
	{
		$count = 0;
		foreach($SubjectArray as $CodeID => $Data)
		{
			if(is_array($Data))
			{
				foreach($Data as $CmpCodeID => $InfoArr)
				{
					list($ReportSubjectID, $SubjectID, $EngSubjectName, $ChiSubjectName, $FullMark, $Scale, $HidePosition) = $InfoArr;
					
					# Hide not enrolled subject according to the setting
					if($HideNotEnrolled==1 && (empty($ParStudentResultArray[$SubjectID]["Overall"]) || strcmp($ParStudentResultArray[$SubjectID]["Overall"]["Grade"], "/")==0))
						continue;

					$IsComponent = ($CmpCodeID>0) ? 1 : 0;
					$top_style = ($IsFirst==0) ? "border_top" : "";
					$IsFirst = 0;
					$Prefix = ($IsComponent==1) ? "&nbsp;&nbsp;&nbsp;&nbsp; - " : "";

					# Highest Grade
					$MaxGrade = $DistinctionArray[$SubjectID]["G"];
					$FirstRowSetting = $ReportCellSetting[$ColumnArray[0][0]][$ReportSubjectID];
					$FullMark = ($FirstRowSetting=="G") ? $MaxGrade : $FullMark;

					# check whether is parent/component subject
					$IsParent = (sizeof($AllSubjectArray[$SubjectID])>1) ? 1 : 0;
					$IsComponent = (empty($AllSubjectArray[$SubjectID])) ? 1 : 0;

					$ResultShow = (!($ResultDisplayType==2 && $IsParent==1) && !($IsParent==1 && $ResultCalculationType==1)) ? 1 : 0;
					$OverallShow = (!($ResultDisplayType==2 && $IsParent==1) && !($ResultDisplayType==1 && $IsComponent==1)) ? 1 : 0;
					
					# get subject display cell
					if($BilingualReportTitle==1 || $SubjectTitleType==0) {
						$SubjectCell = "<td width='50%' class='small_text'>".$Prefix.$EngSubjectName."</td><td width='50%' class='small_text'>".$Prefix.$ChiSubjectName."</td>";
					}
					else {
						$SubjectDisplayName = ($SubjectTitleType==1) ? $EngSubjectName : $ChiSubjectName;
						$SubjectCell = "<td class='small_text'>".$Prefix.$SubjectDisplayName."</td>";
					}

					$ResultTable .= "<tr>";
					$ResultTable .= "<td class='tabletext $top_style'>
										<table width='100%' border='0' cellpadding='0' cellspacing='0' height='$LineHeight'><tr>".$SubjectCell."</tr></table>
									</td>";
					$ResultTable .= ($ShowFullMark==1) ? "<td class='tabletext border_left $top_style' align='center'>".($ResultShow==1?$FullMark:"&nbsp;")."</td>" : "";
					
					for($i=0; $i<sizeof($ColumnArray); $i++)
					{
						list($ReportColumnID, $ColumnTitle, $Weight, $ShowPosition, $PositionRange) = $ColumnArray[$i];
						$CellSetting = $ReportCellSetting[$ReportColumnID][$ReportSubjectID];
						$Weight = $WeightArray[$ReportColumnID];
						list($p_form, $p_to) = explode(",", $PositionRange);
						
						$DisplayResult = "";
						if(!$ResultShow)
						{
							$ResultTable .= "<td class='border_left $top_style' align='center'>&nbsp;</td>";
						}
						else 
						{
							if($CellSetting!="N/A")
							{
								$StylePrefix = "";
								$StyleSuffix = "";
								if($CellSetting=="S") {
									$s_raw_mark = $ParStudentResultArray[$SubjectID][$ReportColumnID]["RawMark"];
									$s_weighted_mark = $ParStudentResultArray[$SubjectID][$ReportColumnID]["WeightedMark"];
									$s_grade = $ParStudentResultArray[$SubjectID][$ReportColumnID]["Grade"];

									if($s_raw_mark>=0) {
										if($MarkTypeDisplay==2) {
											$DisplayResult = $s_grade;
											list($StylePrefix, $StyleSuffix, $IsDistinct, $IsFailed) = $libreportcard->GET_STYLE("G", $DisplayResult, $FailedArray[$SubjectID], $DistinctionArray[$SubjectID], $PFArray[$SubjectID]);
										}
										else {
											list($StylePrefix, $StyleSuffix, $IsDistinct, $IsFailed) = $libreportcard->GET_STYLE($CellSetting, $s_raw_mark, $FailedArray[$SubjectID], $DistinctionArray[$SubjectID], $PFArray[$SubjectID]);
											$DisplayResult = ($MarkTypeDisplay==1) ? $s_raw_mark : $s_weighted_mark;
										}
									}
									else 
										$DisplayResult = $libreportcard->SpecialMarkArray[trim($s_raw_mark)];
								}
								else {
									$DisplayResult = $ParStudentResultArray[$SubjectID][$ReportColumnID]["Grade"];
									if($DisplayResult!="/" && $DisplayResult!="abs") {
										list($StylePrefix, $StyleSuffix, $IsDistinct, $IsFailed) = $libreportcard->GET_STYLE($CellSetting, $DisplayResult, $FailedArray[$SubjectID], $DistinctionArray[$SubjectID], $PFArray[$SubjectID]);
									}
								}
								$DisplayResult = $StylePrefix.$DisplayResult.$StyleSuffix;
								$FormPosition = $ParStudentResultArray[$SubjectID][$ReportColumnID]["OMF"];
								$ClassPosition = $ParStudentResultArray[$SubjectID][$ReportColumnID]["OMC"];
							}

							$ResultTable .= "<td class='border_left $top_style' width='10%'><table border=0 cellpadding=0 cellspacing=0 width='100%'>";
							$ResultTable .= "<tr>";
							$ResultTable .= "<td align='center' class='tabletext'>".($DisplayResult==""?"--":$DisplayResult)."</td>";
							$ResultTable .= "</tr>";
							$ResultTable .= "</table></td>";							
							if($ShowPosition==1 || $ShowPosition==2)
							{
								$PosDisplay = ($ShowPosition==1) ? $ClassPosition : $FormPosition;
								$PosDisplay = ($PosDisplay>0 && ($PosDisplay>=$p_from && $PosDisplay<=$p_to)) ? $PosDisplay : "*";			
								$ResultTable .= ($HidePosition==0 && $CellSetting=="S") ? "<td align='center' class='border_left tabletext $top_style'>(".$PosDisplay.")</td>" : "<td align='center' class='border_left tabletext $top_style'>(*)</td>";
							}
						}
					}
					
					if($ColumnSize>1 && $HideOverallResult!=1)
					{
						if(!$OverallShow)
						{
							$ResultTable .= "<td class='tabletext border_left $top_style' align='center'>&nbsp;</td>";
						}
						else
						{
							$StylePrefix = "";
							$StyleSuffix = "";
							if($CellSetting=="S") {
								$s_overall_mark = $ParStudentResultArray[$SubjectID]["Overall"]["Mark"];
								$s_overall_grade = $ParStudentResultArray[$SubjectID]["Overall"]["Grade"];
									
								if($s_overall_mark>=0) {
									if($MarkTypeDisplay==2) {
										$OverallResult = $s_overall_grade;
										list($StylePrefix, $StyleSuffix, $IsDistinct, $IsFailed) = $libreportcard->GET_STYLE("G", $OverallResult, $FailedArray[$SubjectID], $DistinctionArray[$SubjectID], $PFArray[$SubjectID]);
									}
									else {
										$OverallResult = $s_overall_mark;
										list($StylePrefix, $StyleSuffix, $IsDistinct, $IsFailed) = $libreportcard->GET_STYLE($CellSetting, $OverallResult, $FailedArray[$SubjectID], $DistinctionArray[$SubjectID], $PFArray[$SubjectID]);
									}
								}
								else
									$OverallResult = $libreportcard->SpecialMarkArray[trim($s_overall_mark)];
							}
							else {
								$OverallResult = $ParStudentResultArray[$SubjectID]["Overall"]["Grade"];
								if($OverallResult!="/" && $OverallResult!="abs") {
									list($StylePrefix, $StyleSuffix, $IsDistinct, $IsFailed) = $libreportcard->GET_STYLE($CellSetting, $OverallResult, $FailedArray[$SubjectID], $DistinctionArray[$SubjectID], $PFArray[$SubjectID]);
								}
							}
							$OverallResult = $StylePrefix.$OverallResult.$StyleSuffix;
							$OverallFormPosition = $ParStudentResultArray[$SubjectID]["Overall"]["OMF"];
							$OverallClassPosition = $ParStudentResultArray[$SubjectID]["Overall"]["OMC"];

							$ResultTable .= "<td class='border_left $top_style' width='10%'><table border=0 cellpadding=0 cellspacing=0 width='100%'>";
							$ResultTable .= "<tr>";
							$ResultTable .= "<td align='center' class='tabletext'>".($OverallResult==""?"--":$OverallResult)."</td>";
							$ResultTable .= "</tr>";
							$ResultTable .= "</table></td>";
							if($ShowOverallPosition==1 || $ShowOverallPosition==2)
							{
								$OverallPosDisplay = ($ShowOverallPosition==1) ? $OverallClassPosition : $OverallFormPosition;
								$OverallPosDisplay = ($OverallPosDisplay>0 && ($OverallPosDisplay>=$op_from && $OverallPosDisplay<=$op_to)) ? $OverallPosDisplay : "*";	
								//aki
								if ($IsFailed) $OverallPosDisplay = "*";
								$ResultTable .= ($HidePosition==0 && $CellSetting=="S") ? "<td class='border_left tabletext $top_style' align='center'>(".$OverallPosDisplay.")</td>" : "<td class='border_left tabletext $top_style' align='center'>(*)</td>";
							}
						}
					}
					if($ShowSubjectTeacherComment==1)
					{
						$Comment = $ParSubjectTeacherCommentArray[$SubjectID];
						$ResultTable .= "<td class='tabletext border_left $top_style' align='center'>".($Comment==""?"&nbsp;":$Comment)."</td>";
					}
					$ResultTable .= "</tr>";
					$count++;
				}
			}
		}
	}
	else
	{
		$ResultTable .= "<tr><td align='center' class='tabletext' colspan='".$ColumnSpan."'>".$eReportCard['NoRecord']."</td></tr>";
	}
	$ResultTable .= "</table>";
	if($ParSummaryTable!="")
	{
		$ResultTable .= "<table width='100%' cellspacing='0' cellpadding='5' class='summary_table'>";
		$ResultTable .= "<tr><td colspan='".$ColumnSpan."' height='100' valign='top'>".$ParSummaryTable."</td></tr>";
		$ResultTable .= "</table>";
	}
	
	return $ResultTable;
}

################################ Function End #####################################
?>