<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
//include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

if ($plugin['ReportCard'])
{
	include_once($PATH_WRT_ROOT."includes/libreportcard.php");
	$libreportcard = new libreportcard();
	$CurrentPage = "PageReportGenerationPositionReport";
	$MODULE_OBJ = $libreportcard->GET_MODULE_OBJ_ARR();
    if ($libreportcard->hasAccessRight())
    {
        $linterface = new interface_html();

		# Get collection period
		$Year = ($libreportcard->Year=="") ? date('Y') : $libreportcard->Year;
				
		# get report list
		$sql = "SELECT
					a.ReportID, 
					REPLACE(a.ReportTitle, '::', '<br />'),
					a.Description,
					date_format(a.LastGenerated, '%Y-%m-%d %H:%i')
				FROM 
					RC_REPORT_TEMPLATE as a
				WHERE 
					a.RecordStatus = 1
				";
		$ReportArray = $libreportcard->returnArray($sql, 4);

		if(!empty($ReportArray))
		{
			$ReportSelect = "<SELECT name='ReportID'>";
			for($i=0; $i<sizeof($ReportArray); $i++)
			{
				$ReportSelect .= "<OPTION value='".$ReportArray[$i][0]."' $Selected>".$ReportArray[$i][1]."</OPTION>";
			}
			$ReportSelect .= "</SELECT>";
		}

		# get semester list
		$semester_data = getSemesters($libreportcard->Year, 0);
		if(!empty($semester_data))
		{
			$SemesterSelect = "<SELECT name='Semester'>";
			for($i=0; $i<sizeof($semester_data); $i++)
			{
				$thisYearTermID = $semester_data[$i]['YearTermID'];
				$thisTermName = $semester_data[$i]['TermName'];
				$Selected = ($libreportcard->Semester==$thisYearTermID) ? "SELECTED='SELECTED'" : "";
				$SemesterSelect .= "<OPTION value='".$thisYearTermID."' $Selected>".$thisTermName."</OPTION>";
			}
			$SemesterSelect .= "<OPTION value='FULL' ".($libreportcard->Semester=="FULL"?"SELECTED='SELECTED'" : "").">".$eReportCard['FullYear']."</OPTION>";
			$SemesterSelect .= "</SELECT>";
		}
		
        # tag information
		$TAGS_OBJ[] = array($eReportCard['PositionReport'], "", 0);
		
		$linterface->LAYOUT_START();
		
		?>
		<script language="javascript">
		function checkform(obj){
			if(!check_positive_nonzero_int (obj.TopX, "<?= $eReportCard['jsNonzeroAlert']?>")) return false;
			return true;
		}
		</script>

		<form name="form1" action="report.php" method="post"onSubmit="return checkform(this);">
		<table width="95%" align="center">
		<tr>
			<td>&nbsp;</td>
			<td align="right"> <?=$linterface->GET_SYS_MSG($Result);?></td>
		</tr>
		</table>
		<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
			<tr><td> </td></tr>
			<tr>
				<td>
					<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
						<tr>
							<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $eReportCard['Report'] ?> </td>
							<td><?=$ReportSelect?></td>
						</tr>
						<tr>
							<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $i_Profile_Semester ?> </td>
							<td><?=$SemesterSelect?></td>
						</tr>
						<tr>
							<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $eReportCard['TopX'] ?> </td>
							<td class="tabletext"><input type="text" class="textboxnum" name="TopX" id="TopX" value="10"> <?= $eReportCard['PositionTitle']?></td>
						</tr>
						<tr>
							<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $eReportCard['OrderBy'] ?> </td>
							<td class="tabletext">
								<input type="radio" name="OrderBy" id="ByTotal" value="total" checked><label for="ByTotal"><?= $eReportCard['ByTotal']?></label>&nbsp;&nbsp;&nbsp;
								<input type="radio" name="OrderBy" id="ByGPA" value="gpa"><label for="ByGPA"><?= $eReportCard['ByGPA']?></label>
							</td>
						</tr>
						<!--
						<tr>
							<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $eReportCard['StartDate']; ?> </td>
							<td><input type="text" class="textboxnum" name="StartDate" maxlength="10" value="<?=$libreportcard->StartDate?>"> <?=$linterface->GET_CALENDAR("form1", "StartDate")?></td>
						</tr>
						<tr>
							<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $eReportCard['EndDate']; ?> </td>
							<td><input type="text" class="textboxnum" name="EndDate" maxlength="10" value="<?=$libreportcard->EndDate?>"> <?=$linterface->GET_CALENDAR("form1", "EndDate")?></td>
						</tr>
						<tr>
							<td colspan=2><input type="checkbox" name="AllowClassTeacherComment" id="AllowClassTeacherComment" value=1 <?=($libreportcard->AllowClassTeacherComment==1?"CHECKED='CHECKED'":"")?> />&nbsp;<span class="tabletext"><label for="AllowClassTeacherComment"><?=$eReportCard['AllowClassTeacherComment']?></label></span></td>
						</tr>
						<tr>
							<td colspan=2><input type="checkbox" name="AllowSubjectTeacherComment" id="AllowSubjectTeacherComment" value=1 <?=($libreportcard->AllowSubjectTeacherComment==1?"CHECKED='CHECKED'":"")?> />&nbsp;<span class="tabletext"><label for="AllowSubjectTeacherComment"><?=$eReportCard['AllowSubjectTeacherComment']?></label></span></td>
						</tr>
						<tr>
							<td colspan=2><input type="checkbox" name="AllowClassTeacherUploadCSV" id="AllowClassTeacherUploadCSV" value=1 <?=($libreportcard->AllowClassTeacherUploadCSV==1?"CHECKED='CHECKED'":"")?> />&nbsp;<span class="tabletext"><label for="AllowClassTeacherUploadCSV"><?=$eReportCard['AllowClassTeacherUploadCSV']?></label></span></td>
						</tr>
						-->
					</table>
				</td>
			</tr>
			<tr>
				<td>
					<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
						<tr>
							<td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
						</tr>
						<tr>
							<td align="center"><?= $linterface->GET_ACTION_BTN($button_submit, "submit", "", "btnSubmit")?>&nbsp;
							<?= $linterface->GET_ACTION_BTN($button_reset, "reset")?>&nbsp;
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		
		</form>
	<?= $linterface->FOCUS_ON_LOAD("form1.StartDate") ?>

	<?
        $linterface->LAYOUT_STOP();
		intranet_closedb();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>
