<?php
$PATH_WRT_ROOT = "../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcard.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

$libreportcard = new libreportcard();
$LibUser = new libuser();

if ($plugin['ReportCard'])
{
	include_once($PATH_WRT_ROOT."includes/libreportcard.php");
	$libreportcard = new libreportcard();
	$CurrentPage = "PageReportGenerationImprovementReport";

	$MODULE_OBJ = $libreportcard->GET_MODULE_OBJ_ARR();
    if ($libreportcard->hasAccessRight($_SESSION['UserID']))
    {
        $linterface = new interface_html();

############################################################################################################

# Assume :
# Array have been sorted
# Key of the element is the StudentID : $Arr[$StudentID] = $Mark
function GET_TOP_X_ARR($ParArr, $ParX) {
	
	$Max = $ParX;
	if ($Max > Sizeof($ParArr)) $Max = Sizeof($ParArr);
	
	$count = 0;
	while (list ($key, $value) = each ($ParArr)) {
		$TempTopXArr[$count]['StudentID'] = $key;
		$TempTopXArr[$count]['Mark'] = $value;
		$count++;
	}
	
	for ($i = 0; $i < sizeof($TempTopXArr); $i++)
	{
		$TopXArr[] = $TempTopXArr[$i];
		if (($TempTopXArr[$i]['Mark'] != $TempTopXArr[$i+1]['Mark']) && ($i >= ($Max-1)))
			break;		
	}
	
	return $TopXArr;	
}


function GEN_TOP_X($ParArr, $ParTitle) {
	
	global $LibUser, $i_UserStudentName;
	
	$TopXStr = "<table width='96%' border='0' cellpadding='2' cellspacing='0'>";
	
	$TopXStr .= "<tr class='tablebluetop tabletoplink'>";
	$TopXStr .= "<td>#</td>";
	$TopXStr .= "<td>".$i_UserStudentName."</td>";
	$TopXStr .= "<td>".$ParTitle."</td>";
	$TopXStr .= "</tr>";
	
	for ($i = 0; $i < sizeof($ParArr); $i++)
	{
		$TopXStr .= "<tr class='tabletext tablebluerow".($i%2+1)."'>";
		$TopXStr .= "<td>".($i+1)."</td>";
		$TopXStr .= "<td>".$LibUser->getNameWithClassNumber($ParArr[$i]['StudentID'])."</td>";
		$TopXStr .= "<td>".$ParArr[$i]['Mark']."</td>";
		$TopXStr .= "</tr>";
	}
	
	$TopXStr .= "</table>";
	
	return $TopXStr;	
}

###############################################################################################################
###############################################################################################################
###############################################################################################################

# get all level
$SelectedFormArr = $libreportcard->GET_REPORT_FORMS_ID($ReportID);

$ColumnArray = $libreportcard->GET_REPORT_TEMPLATE_COLUMN($ReportID);
// process array, get current sem, get pre sem, (both have data)
for ($i = 0; $i < sizeof($ColumnArray); $i++) {
	if (trim($ColumnArray[$i]["ColumnTitle"]) == $Semester) {
		$UseColumnArray[] = $ColumnArray[$i-1]["ColumnTitle"];
		$UseColumnArray[] = $ColumnArray[$i]["ColumnTitle"];
	}
}

# get student of the particular classlevel only
for ($CountLevel = 0; $CountLevel < sizeof($SelectedFormArr); $CountLevel++)
{	
	$CurrentClassLevel = $SelectedFormArr[$CountLevel];
	# no need to use $ReportID
	list($ClassStudentArray, $StudentIDArray) = $libreportcard->GET_STUDENT_BY_CLASSLEVEL($ReportID, $CurrentClassLevel);
	
	if(is_array($ClassStudentArray))
	{
		foreach($ClassStudentArray as $ClassID => $StudentArray)
		{
			if(is_array($StudentArray))
			{
				foreach($StudentArray as $StudentID => $InfoArray)
				{
					$UserInfoArray = $InfoArray["UserInfo"];
					$ResultArray = $InfoArray["Result"];
					if(empty($ResultArray))
						continue;
								
					if(!empty($ResultArray[$UseColumnArray[0]]) && !empty($ResultArray[$UseColumnArray[1]]))
					{
						list($GT0, $AM0, $GPA0, $OMC0, $OMCT0, $OMF0, $OMFT0) = $ResultArray[$UseColumnArray[0]];
						list($GT1, $AM1, $GPA1, $OMC1, $OMCT1, $OMF1, $OMFT1) = $ResultArray[$UseColumnArray[1]];

						// get student class
						$ClassName = $libreportcard->GET_CLASSNAME_BY_USERID($StudentID);		
						// get the difference between 2 data and make an array
						#
						if ($OrderBy == "total") {
							if (($GT1!="") && ($GT0 != "")) {
								$FormArray[$CurrentClassLevel][$StudentID] = $GT1-$GT0;
								$ClassArray[$ClassName][$StudentID] = $GT1-$GT0;
							}
						} else {
							if (($GPA1!="") && ($GPA0 != "")) {
								$FormArray[$CurrentClassLevel][$StudentID] = $GPA1-$GPA0;
								$ClassArray[$ClassName][$StudentID] = $GPA1-$GPA0;
							}
						}
					}
				}
			}
		}
	}
}


if ($OrderBy == "total") {
	$TypeTitle = $eReportCard['TotalImproved'];
} else {
	$TypeTitle = $eReportCard['GPAImproved'];
}

if (is_array($FormArray))
{
	while (list ($ClassLevel, $ClassStudent) = each ($FormArray)) {
		if (is_array($ClassStudent)) {
			arsort($ClassStudent);
			$TopXFormArray[$ClassLevel] = GET_TOP_X_ARR($ClassStudent, $TopX);
			$FormTopX[$ClassLevel] = GEN_TOP_X($TopXFormArray[$ClassLevel], $TypeTitle);
		}
	}
	
	while (list ($ClassLevel, $ResultTable) = each ($FormTopX)) {
		// get lvl name
		$AllFormTopX .= "<span class='tablesubtitle'>$i_ClassLevel: ".$libreportcard->GET_CLASSLEVEL_NAME($ClassLevel)."</span><br/>".$ResultTable."<br/><br/>";
	}
} else {
	$AllFormTopX = "<table width='96%' border='0' cellpadding='2' cellspacing='0'>\n";	
	$AllFormTopX .= "<tr class='tablebluetop tabletoplink'><td>#</td><td>".$i_UserStudentName."</td><td>".$TypeTitle."</td></tr>\n";
	$AllFormTopX .= "<tr><td colspan='3' class='tabletext' align='center'>".$eReportCard['NoRecord']."</td></tr>\n";	
	$AllFormTopX .= "</table>\n";
}


if (is_array($ClassArray))
{
	while (list ($ClassName, $ClassStudent) = each ($ClassArray)) {
		if (is_array($ClassStudent)) {
			arsort($ClassStudent);
			$TopXClassArray[$ClassName] = GET_TOP_X_ARR($ClassStudent, $TopX);
			$ClassTopX[$ClassName] = GEN_TOP_X($TopXClassArray[$ClassName], $TypeTitle);
		}
	}
	
	while (list ($ClassName, $ResultTable) = each ($ClassTopX)) {
		$AllClassTopX .= "<span class='tablesubtitle'>$i_ClassName: $ClassName</span><br/>".$ResultTable."<br/><br/>";
	}
} else {	
	$AllClassTopX = "<table width='96%' border='0' cellpadding='2' cellspacing='0'>\n";	
	$AllClassTopX .= "<tr class='tablebluetop tabletoplink'><td>#</td><td>".$i_UserStudentName."</td><td>".$TypeTitle."</td></tr>\n";
	$AllClassTopX .= "<tr><td colspan='3' class='tabletext' align='center'>".$eReportCard['NoRecord']."</td></tr>\n";	
	$AllClassTopX .= "</table>\n";
}



############################################################################################################

# tag information
$TAGS_OBJ[] = array($eReportCard['ImprovementReport'], "", 0);

$linterface->LAYOUT_START();

?>


		<form name="form1" action="report.php" method="post"onSubmit="return checkform(this);">
		<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
			<tr>
				<td class="tabletext">
				<img src="<?=$image_path?>/2007a/icon_section.gif" width="20" height="20" align="absmiddle">
				<span class="sectiontitle"><?= str_replace("<!--TopX-->", $TopX, $eReportCard['AllFormTopX']); ?></span>			 
				</td>
			</tr>
			<tr>
				<td align="center" class="tabletext">					
					<?= $AllFormTopX; ?>
				</td>
			</tr>
			<tr>
				<td class="tabletext" height="50"></td>
			</tr>
			<tr>
				<td class="tabletext">
				<img src="<?=$image_path?>/2007a/icon_section.gif" width="20" height="20" align="absmiddle">
				<span class="sectiontitle"><?= str_replace("<!--TopX-->", $TopX, $eReportCard['AllClassTopX']); ?></span>			 
				</td>
			</tr>
			<tr>
				<td align="center" class="tabletext">
					<?= $AllClassTopX; ?>
				</td>
			</tr>
			<tr>
				<td>
					<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
						<tr>
							<td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
						</tr>
						<tr>
							<td align="center">
							<?= $linterface->GET_ACTION_BTN($button_back, "button", "self.location='index.php'")?>&nbsp;
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>









<?
        $linterface->LAYOUT_STOP();
		intranet_closedb();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>