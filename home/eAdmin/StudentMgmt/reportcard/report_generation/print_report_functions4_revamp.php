<?php

############################ Function Start ######################################

# function to get Title Table
function GENERATE_TITLE_TABLE()
{
	global $ReportTitle, $image_path, $intranet_root, $PhotoLink, $ReportID;

	# get school badge
	$SchoolLogo = GET_SCHOOL_BADGE();

	# get school name
	$SchoolName = GET_SCHOOL_NAME();

	$Banner = "";
	switch ($ReportID) {
		case 20:
		case 21:
		case 22:
		case 23:
			$Banner = "banner_mid_schname.jpg";
			break;
		default:
			$Banner = "banner_mid.jpg";
			break;
	}

	$TitleTable = "
			        <table width='100%'  border='0' cellpadding='0' cellspacing='0'>
			          <tr>
			          	<td width='10' valign='top'>&nbsp;</td>
			            <td width='120' valign='top'><img src='report_image/EscolaTongNam/image/logo.jpg'></td>
			            <td align='center'><img src='report_image/EscolaTongNam/image/$Banner'>
			            <br><span style='color: #309420; font: bold; font-size: 22px;font-family:標楷體;'>學生成績報告表</span><br><span style='color: #309420; font: bold; font-size: 16px; font-family: arial'>STUDENT REPORT</span>
			            </td>
			            <td width='120' align='right' valign='top'>";
	if (is_file($intranet_root.$PhotoLink)) $TitleTable .= "<img src='$PhotoLink'>";
	$TitleTable .= "</td>
						<td width='10' valign='top'>&nbsp;</td>
			          </tr>
			          <tr><td colspan='5' height='5'></td></tr>
			        </table>
			      ";

	return $TitleTable;
}

# function to get Student Info Table
function GENERATE_STUDENT_INFO_TABLE($ParInfoArray)
{
	global $SettingStudentInfo, $StudentInfoTitleArray, $ClassTeacherArray, $IssueDate, $LangArray, $PhotoLink, $intranet_root;
	global $AcademicYear, $eReportCard, $ChiReportCard, $EngReportCard;

	list($ClassName, $ClassNumber, $StudentName, $RegNo) = $ParInfoArray;

	if(!empty($SettingStudentInfo))
	{
		for($i=0; $i<sizeof($StudentInfoTitleArray); $i++)
		{
			$SettingID = trim($StudentInfoTitleArray[$i]);
			if($SettingStudentInfo[$SettingID]==1)
			{
				//$Title = $LangArray[$SettingID];
				$Title = $ChiReportCard[$SettingID];
				$Title = str_replace("<br />", " ", $Title);
				$Display = "";
				switch($SettingID)
				{
					case "Name":
						$ArrayTitle['Name'] = $Title;
						$ArrayContent['Name'] = $StudentName;
						break;
					case "ClassName":
						$ArrayTitle['ClassName'] = $Title;
						$ArrayContent['ClassName'] = $ClassName;
						break;
					case "ClassNumber":
						$ArrayTitle['ClassNumber'] = $Title;
						$ArrayContent['ClassNumber'] = $ClassNumber;
						break;
					case "ClassTeacher":
						$ArrayTitle['ClassTeacher'] = $Title;
						if (!empty($ClassTeacherArray[$ClassName])) {
							$ArrayContent['ClassTeacher'] = implode(", ", $ClassTeacherArray[$ClassName]);
						} else {
							$ArrayContent['ClassTeacher'] = "";
						}
						break;
					case "DateOfIssue":
						$ArrayTitle['DateOfIssue'] = $Title;
						$ArrayContent['DateOfIssue'] = $IssueDate;
						break;
					case "StudentNo":
						$ArrayTitle['StudentNo'] = $Title;
						$ArrayContent['StudentNo'] = $RegNo;
						break;
				}
			}
		}
	}

	$StudentInfoTable = "
						<table width='100%'  border='0' cellpadding='0' cellspacing='0' class='body_topnaming'>
			              <tr>
			              	<td width='10'>&nbsp;</td>
			                <td nowrap width='40'>".$ArrayTitle['Name']."</td>
			                <td width='10'>";
	if (trim($ArrayTitle['Name']) != "") $StudentInfoTable .= "&nbsp;:&nbsp;";
	$StudentInfoTable .= "</td>
			                <td nowrap width='250'>".$ArrayContent['Name']."</td>
			                <td nowrap width='80'>".$ArrayTitle['ClassNumber']."</td>
			                <td width='10'>";
	if (trim($ArrayTitle['ClassNumber']) != "") $StudentInfoTable .= "&nbsp;:&nbsp;";
	$StudentInfoTable .= "</td>
			                <td>".$ArrayContent['ClassNumber']."</td>
			                <td nowrap width='80'>";
	if (trim($AcademicYear) != "") $StudentInfoTable .= "年度";
	$StudentInfoTable .= "</td>
			                <td width='10'>";
	if (trim($AcademicYear) != "") $StudentInfoTable .= "&nbsp;:&nbsp;";
	$StudentInfoTable .= "</td>
			                <td>".$AcademicYear."</td>
			              </tr>
			              <tr>
			              	<td width='10'>&nbsp;</td>
			                <td nowrap>".$ArrayTitle['ClassName']."</td>
			                <td>";
	if (trim($ArrayTitle['ClassName']) != "") $StudentInfoTable .= "&nbsp;:&nbsp;";
	$StudentInfoTable .= "</td>
			                <td>".$ArrayContent['ClassName']."</td>
			                <td nowrap>".$ArrayTitle['StudentNo']."</td>
			                <td>";
	if (trim($ArrayTitle['StudentNo']) != "") $StudentInfoTable .= "&nbsp;:&nbsp;";
	$StudentInfoTable .= "</td>
			                <td>".$ArrayContent['StudentNo']."</td>
			                <td nowrap>";
	if (trim($ArrayContent['DateOfIssue']) != "") $StudentInfoTable .= $ArrayTitle['DateOfIssue']."</td>
			                <td>";
	if (trim($ArrayContent['DateOfIssue']) != "") $StudentInfoTable .= "&nbsp;:&nbsp;";
	$StudentInfoTable .= "</td>
			                <td>".$ArrayContent['DateOfIssue']."</td>
			              </tr>
			            </table>
			            ";

	return $StudentInfoTable;
}

function GENERATE_MARKS_REMARK_TABLE()
{
	$ReturnStr = "
				<table width='100%' border='0' cellpadding='0' cellspacing='0'>
                  <tr>
                    <td><table width='100%' border='0' cellpadding='0' cellspacing='0' class='remarks'>
                      <tr>
                        <td width='252'>100 - 滿分 Full Mark </td>
                        <td width='510'>60 - 及格 Pass Mark </td>
                      </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td><table width='100%'  border='0' cellpadding='0' cellspacing='0' class='remarks'>
                      <tr>
                        <td width='30'>A+</td>
                        <td width='50' align='center'>100</td>
                        <td width='40'>&nbsp;</td>
                        <td width='30'>B+</td>
                        <td width='50'>85 - 89 </td>
                        <td width='40'>&nbsp;</td>
                        <td width='30'>C+</td>
                        <td width='50'>70 - 74 </td>
                        <td width='40'>&nbsp;</td>
                        <td width='30'>D</td>
                        <td width='100'>不及格 Fail </td>
                        <td>&nbsp;</td>
                      </tr>
                      <tr>
                        <td width='30'>A</td>
                        <td width='50'>95 - 99</td>
                        <td width='40'>&nbsp;</td>
                        <td width='30'>B</td>
                        <td width='50'>80 - 84</td>
                        <td width='40'>&nbsp;</td>
                        <td width='30'>C</td>
                        <td width='50'>65 - 69</td>
                        <td width='40'>&nbsp;</td>
                        <td width='30'>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                      </tr>
                      <tr>
                        <td width='30'>A-</td>
                        <td width='50'>90 - 94 </td>
                        <td width='40'>&nbsp;</td>
                        <td width='30'>B-</td>
                        <td width='50'>75 - 79 </td>
                        <td width='40'>&nbsp;</td>
                        <td width='30'>C-</td>
                        <td width='50'>60 - 64 </td>
                        <td width='40'>&nbsp;</td>
                        <td width='30'>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                      </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td><table width='100%'  border='0' cellpadding='0' cellspacing='0' class='remarks'>
                      <tr>
                        <td>不及格科目以星號“<span class='remarks_big'>*</span>”表示 </td>
                        <td><span class='remarks_big'>*</span> Subject(s) of Failure </td>
                      </tr>
                    </table></td>
                  </tr>
                </table>
                ";
	return $ReturnStr;
}

function GENERATE_MISC_CELL($settingID, $arr, $content="") {
	global $ShowFullMark, $ChiReportCard, $EngReportCard;
	$LineHeight = 20;
	
	if ($content == "" && $arr != "") {
		for($i=0; $i<sizeof($arr); $i++) {
			$content .= $arr[$i][0]."<br />";
		}
	}
	
	$x .= "<tr class='body_content'>";
  $x .= "<td width='1' class='body_content' height='100%'>&nbsp;</td>";
	$x .= "<td width='25%' class='body_content' height='100%' valign='top'>";
	$x .= "<table width='100%' height='100%' border='0' cellpadding='0' cellspacing='0' style='padding-top: 3px; padding-bottom:3px'>";
	$x .= "<tr valign='top' class='body_content'>";
	$x .= "<td width='40%' nowrap>".$ChiReportCard[$settingID]."</td>";
	$x .= "<td width='60%' nowrap>".$EngReportCard[$settingID]."</td>";
	$x .= "</tr></table></td>";
	if ($ShowFullMark)
		$x .= "<td class='body_content'>&nbsp;</td>";
	$x .= "<td class='body_content' colspan='$ColumnSpan' valign='top' style='padding-top: 2px'>".$content."&nbsp;</td>";
	$x .= "</td></tr>";
	
	return $x;
}

# function to get MISC Table
function GENERATE_MISC_TABLE($ParClassTeacherComment, $ParStudentID)
{
	global $SettingMiscArray, $MiscTitleArray, $Semester, $LangArray;
	global $StudentAwardArray, $StudentMeritArray, $StudentECAArray, $StudentRemarkArray;
	global $eReportCard_ReportTitleArray, $LineHeight, $EngReportCard, $ChiReportCard;
	global $ShowFullMark, $ColumnArray, $HidePosition;
	global $StudentInterSchoolArray, $StudentSchoolServiceArray, $commentSemester;
	global $SemStudentRemarkArray, $SemStudentAwardArray;
	
	$ColumnSpan = sizeof($ColumnArray);
	if (!$HidePosition) $ColumnSpan++;

	$ColumnSpan = $ColumnSpan + sizeof($ColumnArray)/2;

	$ParStudentID = trim($ParStudentID);
	if(!empty($SettingMiscArray))
	{
		for($i=0; $i<sizeof($MiscTitleArray); $i++)
		{
			$SettingID = trim($MiscTitleArray[$i]);
			if($SettingMiscArray[$SettingID]==1 && $SettingID!="SubjectTeacherComment" &&
				$SettingID!="MeritsAndDemerits" && $SettingID!="MinorCredit" &&
				$SettingID!="MajorCredit" && $SettingID!="MinorFault" && $SettingID!="MajorFault" &&
				$SettingID!="ECA")
			{
				$Title = $LangArray[$SettingID];
				//$Display = "";
				switch($SettingID)
				{
					case "ClassTeacherComment":
						$Display["ClassTeacherComment"] = GENERATE_MISC_CELL($SettingID, nl2br($ParClassTeacherComment));
						break;
					case "Awards":
						$Display["Awards"] = GENERATE_MISC_CELL($SettingID, $SemStudentAwardArray[$ParStudentID][$commentSemester]);
						break;
					/*
					case "ECA":
						$Display["ECA"] = GENERATE_MISC_CELL($SettingID, "", GENERATE_ECA_CONTENT($StudentECAArray[$ParStudentID][$commentSemester]));
						break;
					*/
					case "Remark":
						$Display["Remark"] = GENERATE_MISC_CELL($SettingID, $SemStudentRemarkArray[$ParStudentID][$commentSemester]);
						break;
					case "InterSchoolCompetition":
						$Display["InterSchoolCompetition"] = GENERATE_MISC_CELL($SettingID, $StudentInterSchoolArray[$ParStudentID][$commentSemester]);
						break;
					case "SchoolService":
						$Display["SchoolService"] = GENERATE_MISC_CELL($SettingID, $StudentSchoolServiceArray[$ParStudentID][$commentSemester]);
				 		break;
				}
			}
		}
		
		$MiscTable = "";
		foreach ($Display as $key => $value) {
			$MiscTable .= $value;
		}
	}

	return $MiscTable;
}

# function to get summary display
function RETURN_SUMMARY_DISPLAY($ParInfoArray, $ParSettingID, $ClassHighestAverage, $StudentSummaryArray, $ParColumnSem)
{
	global $StudentMeritArray, $StudentID;
	global $FailedArray, $DistinctionArray, $PFArray, $libreportcard;
	global $OverallPositionRange, $ReportID;
	list($GT, $AM, $GPA, $OMC, $OMCT, $OMF, $OMFT) = $ParInfoArray;

	if (($ReportID == 18)||($ReportID == 19)) {
		switch($ParColumnSem) {
			case "第一段":
							$ParColumnSem = "上學期第一段";
							break;
			case "第二段":
							$ParColumnSem = "上學期第二段";
							break;
			case "第三段":
							$ParColumnSem = "下學期第一段";
							break;
		}
	}
	
	switch($ParSettingID)
	{
		case "GrandTotal":
				$Display = $GT;
				break;
		case "GPA":
				$Display = $GPA;
				break;
		case "AverageMark":
				$Display = $AM;
				list($StylePrefix, $StyleSuffix, $IsDistinct, $IsFailed) = $libreportcard->GET_STYLE("S", $AM, $FailedArray[6], $DistinctionArray[6], $PFArray[6]);
				//$Display = $libreportcard->GET_STYLE("S", $AM, $FailedArray[$SubjectID], $DistinctionArray[$SubjectID], $PFArray[$SubjectID]);
				if ($IsFailed) $Display = "*".$Display;
				break;
		case "FormPosition":
				list($op_from, $op_to) = explode(",", $OverallPositionRange);
				$Display = $OMF;
				$Display = ($Display>0 && ($Display>=$op_from && $Display<=$op_to)) ? $Display : "----";
				break;
		case "ClassPosition":
				list($op_from, $op_to) = explode(",", $OverallPositionRange);
				$Display = $OMC;
				$Display = ($Display>0 && ($Display>=$op_from && $Display<=$op_to)) ? $Display : "----";
				break;
		case "ClassPupilNumber":
				$Display = $OMCT;
				break;
		case "ClassHighestAverage":
				$Display = $ClassHighestAverage;
				break;
		case "Conduct":
				$Display = ($StudentSummaryArray[0]["Conduct"]=="") ? "----" : $StudentSummaryArray[0]["Conduct"];
				break;
		case "Politeness":
				$Display = ($StudentSummaryArray[0]["Politeness"]=="") ? "----" : $StudentSummaryArray[0]["Politeness"];
				break;
		case "Behaviour":
				$Display = ($StudentSummaryArray[0]["Behaviour"]=="") ? "----" : $StudentSummaryArray[0]["Behaviour"];
				break;
		case "Application":
				$Display = ($StudentSummaryArray[0]["Application"]=="") ? "----" : $StudentSummaryArray[0]["Application"];
				break;
		case "Tidiness":
				$Display = ($StudentSummaryArray[0]["Tidiness"]=="") ? "----" : $StudentSummaryArray[0]["Tidiness"];
				break;
		case "Motivation":
				$Display = ($StudentSummaryArray[0]["Motivation"]=="") ? "----" : $StudentSummaryArray[0]["Motivation"];
				break;
		case "SelfConfidence":
				$Display = ($StudentSummaryArray[0]["SelfConfidence"]=="") ? "----" : $StudentSummaryArray[0]["SelfConfidence"];
				break;
		case "SelfDiscipline":
				$Display = ($StudentSummaryArray[0]["SelfDiscipline"]=="") ? "----" : $StudentSummaryArray[0]["SelfDiscipline"];
				break;
		case "Courtesy":
				$Display = ($StudentSummaryArray[0]["Courtesy"]=="") ? "----" : $StudentSummaryArray[0]["Courtesy"];
				break;
		case "Honesty":
				$Display = ($StudentSummaryArray[0]["Honesty"]=="") ? "----" : $StudentSummaryArray[0]["Honesty"];
				break;
		case "Responsibility":
				$Display = ($StudentSummaryArray[0]["Responsibility"]=="") ? "----" : $StudentSummaryArray[0]["Responsibility"];
				break;
		case "Cooperation":
				$Display = ($StudentSummaryArray[0]["Cooperation"]=="") ? "----" : $StudentSummaryArray[0]["Cooperation"];
				break;

		case "DaysAbsent":
				$Display = ($StudentSummaryArray[0][""]=="DaysAbsent") ? "----" : $StudentSummaryArray[0]["DaysAbsent"];
				break;
		case "TimesLate":
				$Display = ($StudentSummaryArray[0][""]=="TimesLate") ? "----" : $StudentSummaryArray[0]["TimesLate"];
				break;
		case "AbsentWOLeave":
				$Display = ($StudentSummaryArray[0][""]=="AbsentWOLeave") ? "----" : $StudentSummaryArray[0]["AbsentWOLeave"];
				break;
		
		case "Merit":
				$Display = ($StudentMeritArray[$StudentID][$ParColumnSem][0]["Merit"]=="") ? "----" : $StudentMeritArray[$StudentID][$ParColumnSem][0]["Merit"];
				break;
		case "Demerit":
				$Display = ($StudentMeritArray[$StudentID][$ParColumnSem][0]["Demerit"]=="") ? "----" : $StudentMeritArray[$StudentID][$ParColumnSem][0]["Demerit"];
				break;
		case "MinorCredit":
				$Display = ($StudentMeritArray[$StudentID][$ParColumnSem][0]["MinorCredit"]=="") ? "----" : $StudentMeritArray[$StudentID][$ParColumnSem][0]["MinorCredit"];
				break;
		case "MajorCredit":
				$Display = ($StudentMeritArray[$StudentID][$ParColumnSem][0]["MajorCredit"]=="") ? "----" : $StudentMeritArray[$StudentID][$ParColumnSem][0]["MajorCredit"];
				break;
		case "MinorFault":
				$Display = ($StudentMeritArray[$StudentID][$ParColumnSem][0]["MinorFault"]=="") ? "----" : $StudentMeritArray[$StudentID][$ParColumnSem][0]["MinorFault"];
				break;
		case "MajorFault":
				$Display = ($StudentMeritArray[$StudentID][$ParColumnSem][0]["MajorFault"]=="") ? "----" : $StudentMeritArray[$StudentID][$ParColumnSem][0]["MajorFault"];
				break;
	}
	return $Display;
}

# function get summary array
function GET_SUMMARY_ARRAY($ParInfoArray, $ParClassHighestAverage, $ParSummaryArray)
{
	global $SummaryTitleArray, $SettingSummaryArray, $ShowFullMark, $Semester, $ColumnArray, $LangArray, $SettingArray;
	global $EngReportCard, $ChiReportCard, $ReportID;

	$HideOverallResult = $SettingArray["HideOverallResult"];
	$ReturnArray = "";
//hdebug_r($ParSummaryArray);
	if(!empty($SettingSummaryArray))
	{
		$count = 0;
		for($k=0; $k<sizeof($SummaryTitleArray); $k++)
		{
			$SettingID = trim($SummaryTitleArray[$k]);
			//if($SettingSummaryArray[$SettingID]==1 && $SettingID!="DaysAbsent" && $SettingID!="TimesLate" && $SettingID!="AbsentWOLeave")
			if($SettingSummaryArray[$SettingID]==1
				&& $SettingID!="Merit" && $SettingID!="MinorCredit" && $SettingID!="MajorCredit"
				&& $SettingID!="MinorFault" && $SettingID!="MajorFault" && $SettingID!="Demerit"
				)
			{
				$Title = $LangArray[$SettingID];
				$ReturnArray[$count]["Title"] = $Title;
				$ReturnArray[$count]["EngTitle"] = $EngReportCard[$SettingID];
				$ReturnArray[$count]["ChiTitle"] = $ChiReportCard[$SettingID];
				for($i=0; $i<sizeof($ColumnArray); $i++)
				{
					$column = trim($ColumnArray[$i]["ColumnTitle"]);
//hdebug('asdf', $column);
//hdebug_r($ParInfoArray);
					$summary_column = $column;
					if (($ReportID == 18)||($ReportID == 19)) {
						switch($column) {
							case "第一段":
									$summary_column = "上學期第一段";
									break;
							case "第二段":
									$summary_column = "上學期第二段";
									break;
							case "第三段":
									$summary_column = "下學期第一段";
									break;
						}
					}
					//hdebug('bbbb', $column);
				//hdebug_r($ParSummaryArray[$column]);
				if(!empty($ParInfoArray[$column]))
					{
						$Display = RETURN_SUMMARY_DISPLAY($ParInfoArray[$column], $SettingID, $ParClassHighestAverage[$column], $ParSummaryArray[$summary_column], $column);
						$ReturnArray[$count][$column] = $Display;
					}
				}
				if(!($Semester=="FULL" && $HideOverallResult==1))
				{
					$Display = RETURN_SUMMARY_DISPLAY($ParInfoArray[$Semester], $SettingID, $ParClassHighestAverage[$Semester], $ParSummaryArray[$Semester], $Semester);
					$ReturnArray[$count][$Semester] = $Display;
				}
				$count++;
			}
		}
	}
	return $ReturnArray;
}


# function get merit array
function GET_MERIT_ARRAY($ParInfoArray, $ParClassHighestAverage, $ParSummaryArray)
{
	global $SummaryTitleArray, $SettingSummaryArray, $ShowFullMark, $Semester, $ColumnArray, $LangArray, $SettingArray;
	global $EngReportCard, $ChiReportCard, $ReportID;

	$HideOverallResult = $SettingArray["HideOverallResult"];
	$ReturnArray = "";
	
	if(!empty($SettingSummaryArray))
	{
		$count = 0;
		for($k=0; $k<sizeof($SummaryTitleArray); $k++)
		{
			$SettingID = trim($SummaryTitleArray[$k]);
			if($SettingSummaryArray[$SettingID]==1 &&
				($SettingID=="Merit" || $SettingID=="MinorCredit" || $SettingID=="MajorCredit" ||
				 $SettingID=="MinorFault" || $SettingID=="MajorFault" || $SettingID=="Demerit")
				)
			{
				$Title = $LangArray[$SettingID];
				$ReturnArray[$count]["Title"] = $Title;
				$ReturnArray[$count]["ChiTitle"] = $ChiReportCard[$SettingID];
				$ReturnArray[$count]["EngTitle"] = $EngReportCard[$SettingID];
				for($i=0; $i<sizeof($ColumnArray); $i++)
				{
					$column = trim($ColumnArray[$i]["ColumnTitle"]);
					$summary_column = $column;
					if (($ReportID == 18)||($ReportID == 19)) {
						switch($column) {
							case "第一段":
									$summary_column = "上學期第一段";
									break;
							case "第二段":
									$summary_column = "上學期第二段";
									break;
							case "第三段":
									$summary_column = "下學期第一段";
									break;
						}
					}
					//hdebug_r($ParSummaryArray);
					if(!empty($ParInfoArray[$column]))
					{
						$Display = RETURN_SUMMARY_DISPLAY($ParInfoArray[$column], $SettingID, $ParClassHighestAverage[$column], $ParSummaryArray[$summary_column], $column);
						$ReturnArray[$count][$column] = $Display;
					}
				}
				if(!($Semester=="FULL" && $HideOverallResult==1))
				{
					$Display = RETURN_SUMMARY_DISPLAY($ParInfoArray[$Semester], $SettingID, $ParClassHighestAverage[$Semester], $ParSummaryArray[$Semester], $Semester);
					$ReturnArray[$count][$Semester] = $Display;
				}
				$count++;
			}
		}
	}
	
	return $ReturnArray;
}


# function get merit array
function GET_ECA_ARRAY($ParStudentECAArray)
{
	global $LangArray;
	if (sizeof($ParStudentECAArray) > 0) {
		while (list($key, $value) = each($ParStudentECAArray)) {
			$ReturnArray[$key][0]['Title'] = $LangArray['ECA'];
			if (sizeof($value) > 0) {
				while (list($subkey, $subvalue) = each($value)) {
					for($j=0; $j<sizeof($subvalue); $j++) {
						$ReturnArray[$key][$j+1]['Title'] = $subvalue[$j]["ECA"];
						$ReturnArray[$key][$j+1][$subkey] = $subvalue[$j]["Grade"];
					}
				}
			}
		}
	}
	return $ReturnArray;
}

# function to get signature table
function GENERATE_SIGNATURE_TABLE()
{
	global $eReportCard, $SettingArray, $LangArray;


	$SignatureTitleArray = $eReportCard['DisplaySettingsArray']["Signature"];
	$SettingSignatureArray = $SettingArray["Signature"];

	$SignatureTable = "<table width='100%' border='0' cellspacing='0' cellpadding='0'><tr><td class='box_L_R'>";
	$SignatureTable .= "<table width='100%'  border='0' cellpadding='0' cellspacing='0' class='box_B'>
    		          <tr valign='middle' class='remarks_title'>";
	for($k=0; $k<sizeof($SignatureTitleArray); $k++)
	{
		$SettingID = trim($SignatureTitleArray[$k]);
		if($SettingSignatureArray[$SettingID]==1)
		{
			$Title = $LangArray[$SettingID];
			$SignatureTable .= "<td width='80' align='center' class='boxbg_T_B'>".$Title."</td>";
		}
	}

	$SignatureTable .= "<td class='boxbg_T_B'>說 明<br>General Instruction </td></tr>";
	for($k=0; $k<sizeof($SignatureTitleArray); $k++)
	{
		$SettingID = trim($SignatureTitleArray[$k]);
		if($SettingSignatureArray[$SettingID]==1)
		{
			$Title = $LangArray[$SettingID];
			$SignatureTable .= "<td height='80' class='box_R'>&nbsp;</td>";
		}
	}
	$SignatureTable .= "<td height='80' class='remarks_padding'>".GENERATE_MARKS_REMARK_TABLE()."</td></table></td></tr></table>";

	return $SignatureTable;
}

function GET_SEMESTER_NUMBER($ParSemester)
{
	global $libreportcard, $SemesterArray;

	$sem_number = "-1";
	if(trim($ParSemester)=="FULL")
	{
		$sem_number = 0;
	} else
	{
		for($i=0; $i<sizeof($SemesterArray); $i++)
		{
			if(trim($ParSemester)==trim($SemesterArray[$i]))
			{
				$sem_number = $i+1;
				break;
			}
		}
	}
	return $sem_number;
}

function CHECK_CSV_FILE_FORMAT($ParHeaderArray, $ParDefaultHeaderArray)
{
	# Check Title Row
	$format_wrong = false;

	for ($i=0; $i<sizeof($ParDefaultHeaderArray); $i++)
	{
		if ($ParHeaderArray[$i]!=$ParDefaultHeaderArray[$i])
		{
			$format_wrong = true;
			break;
		}
	}

	return $format_wrong;
}

function GET_CSV_FILE_CONTENT($ParFile, $ParDefaultHeaderArray, $ParSem = "", $ReportType="")
{
	global $libreportcard, $lf, $StudentRegNoArray, $SemesterArray, $intranet_root;
	
	######### For adding associated array ##########
	global $HeaderSettingMap, $CustomSettingMap;
	################################################
	
	$ClassArr = $libreportcard->GET_CLASSES();

	if ($ParSem == "")
	{
		$TargetArray = $SemesterArray;
		$TargetArray[] = "FULL";
		for($i=0; $i<sizeof($TargetArray); $i++)
		{
			$t_semester = trim($TargetArray[$i]);
			$sem_number = GET_SEMESTER_NUMBER($t_semester);
			if($sem_number>=0)
			{
				for ($j = 0; $j < sizeof($ClassArr); $j++)
				{
					$TempClass = str_replace(".", "", str_replace("/", "", $ClassArr[$j]));
					$TargetFile = $ParFile."/".trim($libreportcard->Year)."_".$sem_number."_".$TempClass.".csv";
					if(file_exists($TargetFile))
					{
						$data = $lf->file_read_csv($TargetFile);
						if(!empty($data))
						{
							$header_row = array_shift($data);
							$wrong_format = CHECK_CSV_FILE_FORMAT($header_row, $ParDefaultHeaderArray);
							#########################################
							# Ignore wrong format checking and force
							# assign associated array
							$wrong_format = false;
							#########################################
							if(!$wrong_format)
							{
								for($k=0; $k<sizeof($data); $k++)
								{
									$reg_no = array_shift($data[$k]);
									$reg_no = trim(str_replace("#", "", $reg_no));
									$student_id = trim($StudentRegNoArray[$reg_no]);
									if($student_id!="")
									{
										if ($libreportcard->GET_CLASSNAME_BY_USERID($student_id) == $TempClass)
										{
											// add associated array to $data also
											if ($ReportType != "") {
												for($r=0; $r<sizeof($data[$k]); $r++) {
													#$data[$k][$HeaderSettingMap[$ReportType][$header_row[$r+1]]] = $data[$k][$r];
													$data[$k][$CustomSettingMap[$ReportType][$header_row[$r+1]]] = $data[$k][$r];
												}
											}
											$ReturnArray[$student_id][$t_semester][] = $data[$k];
										}
									}
								}
							}
						}
					}
				}
			}
		}
	} else {
		for ($j = 0; $j < sizeof($ClassArr); $j++)
		{
			$TempClass = str_replace(".", "", str_replace("/", "", $ClassArr[$j]));
			$TargetFile = $ParFile."/".trim($libreportcard->Year)."_".$ParSem."_".$TempClass.".csv";			
			
			if(file_exists($TargetFile))
			{
				$data = $lf->file_read_csv($TargetFile);
				if(!empty($data))
				{
					$header_row = array_shift($data);
					$wrong_format = CHECK_CSV_FILE_FORMAT($header_row, $ParDefaultHeaderArray);
					#########################################
					# Ignore wrong format checking and force
					# assign associated array
					$wrong_format = false;
					#########################################
					if(!$wrong_format)
					{
						for($k=0; $k<sizeof($data); $k++)
						{
							$reg_no = array_shift($data[$k]);
							$reg_no = trim(str_replace("#", "", $reg_no));
							$student_id = trim($StudentRegNoArray[$reg_no]);
							if($student_id!="")
							{
								if ($libreportcard->GET_CLASSNAME_BY_USERID($student_id) == $TempClass)
								{
									// add associated array to $data also
									if ($ReportType != "") {
										for($r=0; $r<sizeof($data[$k]); $r++) {
											#$data[$k][$HeaderSettingMap[$ReportType][$header_row[$r+1]]] = $data[$k][$r];
											$data[$k][$CustomSettingMap[$ReportType][$header_row[$r+1]]] = $data[$k][$r];
										}
									}
									$ReturnArray[$student_id][$ParSem][] = $data[$k];
								}
							}
						}
					}
				}
			}
		} 
	}
	return $ReturnArray;
}

# function to get result table
function GENERATE_RESULT_TABLE($ParSignatureTable, $ParAttendanceMeritTable, $ParStudentResultArray, $ParSubjectTeacherCommentArray)
{
	global $eReportCard, $SubjectArray, $ColumnArray, $ReportCellSetting, $ShowFullMark, $libreportcard;
	global $FailedArray, $DistinctionArray, $PFArray, $SettingArray;
	global $AllSubjectArray, $ShowOverallPosition, $OverallPositionRange, $HideNotEnrolled;
	global $ShowSubjectTeacherComment, $Semester, $StudentID;
	global $SummaryResultArray, $LangArray, $BilingualReportTitle, $MeritResultArray, $ECAResultArray;
	global $LineHeight, $ClassTeacherCommentArray, $ChiReportCard, $EngReportCard;
	global $ReportID, $TempGrandTotal;
	##############################
	global $StudentECAArray;
	##############################

	$RoundDP = $libreportcard->GET_DECIMAL_POINT($ReportID);

	if (($ReportID != 18)&&($ReportID != 19)) {
		$SemesterEngArray = array("1st Term<br>1st half", "1st Term<br>2nd half", "Whole Term", "2nd Term<br>1st half", "2nd Term<br>2nd half", "Whole Term");
	} else {
		$SemesterEngArray = array("1st Term", "2nd Term", "3rd Term");
	}

	# Report Type
	# 1 - Display the overall result of Parent Subject AND Component Subject(s)
	# 2 - Display the overall result of Parent Subject ONLY
	# 3 - Display the overall result of Component Subject(s) ONLY
	$ResultDisplayType = $SettingArray["ResultDisplayType"];
	$ResultCalculationType = $SettingArray["ResultCalculationType"];
	$SubjectTitleType = $SettingArray["SubjectTitleType"];
	$ShowColumnPercentage = $SettingArray["ShowColumnPercentage"];
	$MarkTypeDisplay = $SettingArray["MarkTypeDisplay"];
	$HideOverallResult = $SettingArray["HideOverallResult"];

	# get subject title cell
	if($BilingualReportTitle==1 || $SubjectTitleType==0) {
		$SubjectTitleCell = "<td width='50%' class='body_subtitle_small'>".$eReportCard['EngSubject']."</td><td width='50%' class='body_subtitle_small'>".$eReportCard['ChiSubject']."</td>";
	}
	else {
		$SubjectTitleCell = "<td class='body_subtitle_small'>".$eReportCard['Subject']."</td>";
	}

	$ExtraColumn = 0;
	$ColumnSize = sizeof($ColumnArray);

	$ResultTable = "<table width='100%' border='0' cellspacing='0' cellpadding='0'><tr><td class='box_L_R'>";
	$ResultTable .= "<table width='100%' border='0' cellspacing='0' cellpadding='0' class='bg'>";

	//  ----------------------------------------- first row start --------------------------------------------------
	$ResultTable .= "<tr>
						<td width='1%' class='body_title_empty' height='100%'>&nbsp;</td>
						<td class='body_title' height='100%' colspan='1'>
							".$eReportCard['ChiSubject']."
						</td>";
	if($ShowFullMark==1)
	{
		$ResultTable .= "<td class='body_title' align='center'>".$eReportCard['ChiFullMark']."</td>";
		$ExtraColumn++;
	}

	list($op_from, $op_to) = explode(",", $OverallPositionRange);
	for($i=0; $i<sizeof($ColumnArray); $i++)
	{
		//list($ReportColumnID, $ColumnTitle, $Weight, $ShowPosition, $PositionRange) = $ColumnArray[$i];
		list($ReportColumnID, $ColumnTitle, $Weight, $ShowPosition, $PositionRange, $IsColumnSum, $MarkSummaryWeight) = $ColumnArray[$i];
		if($Weight!="")
		{
			$WeightArray[$ReportColumnID] = $Weight;
		}

		$ResultTable .= "<td class='body_title'><table border=0 cellpadding=0 cellspacing=0 width='100%' align='center'>";
		$ResultTable .= "<tr><td align='center' class='body_title_font' colspan='3'>".$ColumnTitle."</td></tr>";
		if($ShowPosition==1 || $ShowPosition==2)
		{
			$ResultTable .= "<tr>";
			$ResultTable .= "<td align='center' width='50%' class='body_subtitle_small'>".$LangArray['Mark']."</td>";
			$ResultTable .= "<td width='1'>&nbsp;</td>";
			$ResultTable .= "<td align='center' width='50%' class='body_subtitle_small'>(".$LangArray['Position'].")</td>";
			$ResultTable .= "</tr>";
		}
		$ResultTable .= "</table></td>";

		/*
		if ((($i % 2) == 1)&&(($ReportID != 18)&&($ReportID != 19))) {
			//$ResultTable .= "<td class='body_title' align='center'>".$ChiReportCard['WholeTerm']."</td>";
		}
		*/
	}

	if($ColumnSize>1 && $HideOverallResult!=1)
	{
		if($ShowPosition==1 || $ShowPosition==2) {
			$Display = $LangArray['OverallResult'];
			$tdClass = "class='body_subtitle_small'";
			$ResultTable .= "<td class='body_title'><table border=0 cellpadding=0 cellspacing=0 width='100%' align='center'>";
			$ResultTable .= "<tr><td align='center' $tdClass colspan='3'>".$Display."</td></tr>";
			$ResultTable .= "</table></td>";
		} else {
			$Display = $ChiReportCard['OverallResult'];
			$tdClass = "";
			$ResultTable .= "<td class='body_title' align='center'>".$Display."</td>";
		}

		$ExtraColumn++;
	}

	$ResultTable .= "<td width='1%' class='body_title_empty'>&nbsp;</td>";

	$ResultTable .= "</tr>";
	//  ----------------------------------------- first row end --------------------------------------------------

	//  ----------------------------------------- second row start --------------------------------------------------
	$ResultTable .= "<tr>
						<td class='body_subtitle' colspan='1'>&nbsp;</td>
						<td class='body_subtitle' colspan='1'>
							<table width='100%' height='100%' border='0' cellpadding='0' cellspacing='0'>
							<tr valign='bottom' class='body_subtitle_big'>".$eReportCard['EngSubject']."</tr>
							</table>
						</td>";
	if($ShowFullMark==1)
	{
		$ResultTable .= "<td class='body_subtitle' align='center'>".$eReportCard['EngFullMark']."</td>";
		$ExtraColumn++;
	}

	list($op_from, $op_to) = explode(",", $OverallPositionRange);
	for($i=0; $i<sizeof($ColumnArray); $i++)
	{
		//list($ReportColumnID, $ColumnTitle, $Weight, $ShowPosition, $PositionRange) = $ColumnArray[$i];
		list($ReportColumnID, $ColumnTitle, $Weight, $ShowPosition, $PositionRange, $IsColumnSum, $MarkSummaryWeight) = $ColumnArray[$i];
		if($Weight!="")
		{
			//$ColumnTitle = ($ShowColumnPercentage==1) ? $ColumnTitle."&nbsp;".$Weight."%" : $ColumnTitle;
			$WeightArray[$ReportColumnID] = $Weight;
		}

		$ResultTable .= "<td class='body_subtitle'><table border=0 cellpadding=0 cellspacing=0 width='100%' align='center'>";
		//$ResultTable .= "<tr><td align='center' class='body_subtitle_small' colspan='3'>".$Weight."%</td></tr>";
		$ResultTable .= "<tr><td align='center' class='body_subtitle_small' colspan='3'>".$SemesterEngArray[$i]."</td></tr>";
		if($ShowPosition==1 || $ShowPosition==2)
		{
			$ResultTable .= "<tr>";
			$ResultTable .= "<td align='center' width='50%' class='body_subtitle_small'>".$LangArray['Mark']."</td>";
			$ResultTable .= "<td width='1'>&nbsp;</td>";
			$ResultTable .= "<td align='center' width='50%' class='body_subtitle_small'>(".$LangArray['Position'].")</td>";
			$ResultTable .= "</tr>";
		}
		$ResultTable .= "</table></td>";

		/*
		if ((($i % 2) == 1)&&(($ReportID != 18)&&($ReportID != 19))) {
			//$ResultTable .= "<td class='body_subtitle' align='center'>".$EngReportCard['WholeTerm']."</td>";
		}
		*/
	}

	if($ColumnSize>1 && $HideOverallResult!=1)
	{

		if(false)
		//$ShowOverallPosition==1 || $ShowOverallPosition==2)
		{
			$ResultTable .= "<td class='body_subtitle'>";
			$ResultTable .= "<table border=0 cellpadding=0 cellspacing=0 width='100%' align='center'><tr>";
			$ResultTable .=  "<td align='center' width='50%' class='body_subtitle_small'>".$LangArray['Mark']."</td>";
			$ResultTable .= "<td width='1'>&nbsp;</td>";
			$ResultTable .= "<td align='center' width='50%' class='body_subtitle_small'>(".$LangArray['Position'].")</td>";
			$ResultTable .= "</tr></table>";
			$ResultTable .= "</td>";
		} else {
			$ResultTable .= "<td class='body_subtitle' align='center'>";
			$ResultTable .= $EngReportCard['OverallResult'];
			$ResultTable .= "</td>";
		}
		$ExtraColumn++;
	}

	$ResultTable .= "<td class='body_subtitle'>&nbsp;</td>";

	$ResultTable .= "</tr>";
	//  ----------------------------------------- second row end --------------------------------------------------


	$ColumnSpan = $ColumnSize + $ExtraColumn + 2;
	$IsFirst = 1;
	$tdClass = "body_content";
	$ResultTable .= "<tr><td align='center' colspan='".($ColumnSpan)."' height='3'></td></tr>";
	if(is_array($SubjectArray))
	{
		$count = 0;
		foreach($SubjectArray as $CodeID => $Data)
		{
			if(is_array($Data))
			{
				foreach($Data as $CmpCodeID => $InfoArr)
				{
					list($ReportSubjectID, $SubjectID, $EngSubjectName, $ChiSubjectName, $FullMark, $Scale, $HidePosition) = $InfoArr;
					# Hide not enrolled subject according to the setting
					//if($HideNotEnrolled==1 && (empty($ParStudentResultArray[$SubjectID]["Overall"]) || strcmp($ParStudentResultArray[$SubjectID]["Overall"]["Grade"], "/")==0))
					if ($HideNotEnrolled==1 && !$libreportcard->CHECK_ENROL($ParStudentResultArray[$SubjectID]))
						continue;

					$IsComponent = ($CmpCodeID>0) ? 1 : 0;
					if ($IsComponent) continue;
					//$top_style = ($IsComponent==0 && $IsFirst==0) ? "border_top" : "";
					$IsFirst = 0;
					$Prefix = ($IsComponent==1) ? "&nbsp;&nbsp;&nbsp;&nbsp;" : "";

					# Highest Grade
					$MaxGrade = $DistinctionArray[$SubjectID]["G"];
					$FirstRowSetting = $ReportCellSetting[$ColumnArray[0][0]][$ReportSubjectID];
					$FullMark = ($FirstRowSetting=="G") ? $MaxGrade : $FullMark;

					# check whether is parent/component subject
					$IsParent = (sizeof($AllSubjectArray[$SubjectID])>1) ? 1 : 0;
					$IsComponent = (empty($AllSubjectArray[$SubjectID])) ? 1 : 0;

					$ResultShow = (!($ResultDisplayType==2 && $IsParent==1) && !($IsParent==1 && $ResultCalculationType==1)) ? 1 : 0;
					$OverallShow = (!($ResultDisplayType==2 && $IsParent==1) && !($ResultDisplayType==1 && $IsComponent==1)) ? 1 : 0;

					# get subject display cell
					if($BilingualReportTitle==1 || $SubjectTitleType==0) {
						$SubjectCell = "<td height='$LineHeight' class='{$tdClass}'>
								<table border='0' cellpadding='0' cellspacing='0' width='100%' height='100%'>
								<tr>
									<td width='40%' class='$tdClass' nowrap>".$Prefix.$ChiSubjectName."</td>
									<td width='60%' class='$tdClass' nowrap>".$Prefix.$EngSubjectName."</td>
								</tr>
								</table>
								</td>";
					}
					else {
						$SubjectDisplayName = ($SubjectTitleType==1) ? $EngSubjectName : $ChiSubjectName;
						$SubjectCell = "<td class='{$tdClass}'>
										<table border='0' cellpadding='0' cellspacing='0' width='100%' height='100%'>
										<tr>
											<td class='$tdClass'>".$Prefix.$SubjectDisplayName."</td>
										</tr>
										</table>
										</td>";
					}

					$ResultTable .= "<tr>";
					// line height - aki
					$ResultTable .= "<td>&nbsp;</td><td class='$top_style'>
										<table width='100%' border='0' cellpadding='0' cellspacing='0'><tr>".$SubjectCell."</tr></table>
									</td>";
					$ResultTable .= ($ShowFullMark==1) ? "<td class='$tdClass' align='center'>".($ResultShow==1?$FullMark:"&nbsp;")."</td>" : "";

					for($i=0; $i<sizeof($ColumnArray); $i++)
					{
						$NotShowWholeYear = false;
						list($ReportColumnID, $ColumnTitle, $Weight, $ShowPosition, $PositionRange, $IsColumnSum, $MarkSummaryWeight) = $ColumnArray[$i];
						$CellSetting = $ReportCellSetting[$ReportColumnID][$ReportSubjectID];
						list($p_from, $p_to) = explode(",", $PositionRange);

						$DisplayResult = "";
						if(!$ResultShow)
						{
							$ResultTable .= "<td class='$top_style' align='center'>&nbsp;</td>";
						}
						else
						{
							if($CellSetting!="N/A")
							{
								$StylePrefix = "";
								$StyleSuffix = "";
								if($CellSetting=="S") {
									$s_raw_mark = $ParStudentResultArray[$SubjectID][$ReportColumnID]["RawMark"];
									$s_weighted_mark = $ParStudentResultArray[$SubjectID][$ReportColumnID]["WeightedMark"];
									$s_grade = $ParStudentResultArray[$SubjectID][$ReportColumnID]["Grade"];

									$idStartTag = "";
									$idEndTag = "";
									
									if ($IsColumnSum)
									{		
										/*	
										$s_raw_mark = $MarkSummary["RawMark"];
										if ($s_raw_mark != 0) $s_raw_mark = number_format($s_raw_mark, $RoundDP);
										$s_weighted_mark = $MarkSummary["WeightedMark"];
										if ($s_weighted_mark != 0) $s_weighted_mark = number_format($s_weighted_mark, $RoundDP);
										$s_grade = $MarkSummary["Grade"];
										*/
										
										$TotalMSWeight = 0;
										
										
										//hdebug_r($RawMarkSummary);
										//hdebug_r($WeightedMarkSummary);
										//hdebug_r($MarkSummaryWeightArr);
										
										while(list($TempSubjectID, $TempColumnArr) = each($MarkSummaryWeightArr)) {
											while(list($TempReportColumnID, $TempArr) = each($TempColumnArr)) {
												for ($innerCount = 0; $innerCount < sizeof($TempArr); $innerCount++) {
													if ($RawMarkSummary[$TempSubjectID][$TempReportColumnID][$innerCount] > 0)
													{
														$TotalMSWeight += $TempArr[$innerCount];														
													}
												}												
											}
										}
										while(list($TempSubjectID, $TempColumnArr) = each($RawMarkSummary)) {
											while(list($TempReportColumnID, $TempArr) = each($TempColumnArr)) {
												for ($innerCount = 0; $innerCount < sizeof($TempArr); $innerCount++) {
													if ($RawMarkSummary[$TempSubjectID][$TempReportColumnID][$innerCount] > 0)
													{
														$TotalRawMark += $RawMarkSummary[$TempSubjectID][$TempReportColumnID][$innerCount] * $MarkSummaryWeightArr[$TempSubjectID][$TempReportColumnID][$innerCount] / $TotalMSWeight;
														$TotalWeightedMark += $WeightedMarkSummary[$TempSubjectID][$TempReportColumnID][$innerCount] * $MarkSummaryWeightArr[$TempSubjectID][$TempReportColumnID][$innerCount] / $TotalMSWeight;														
													}
												}												
											}
										}
										
										
										
										/*
										$RawMarkSummary[$ParentSubjectID][$ReportColumnID][] = $s_raw_mark;
											$WeightedMarkSummary[$ParentSubjectID][$ReportColumnID][] = $s_weighted_mark;
											$MarkSummaryWeightArr[$ParentSubjectID][$ReportColumnID][] = $MarkSummaryWeight;
											*/

										$s_raw_mark = $TotalRawMark;
										if ($s_raw_mark != 0) $s_raw_mark = number_format($s_raw_mark, $RoundDP);
										$s_weighted_mark = $TotalWeightedMark;
										if ($s_weighted_mark != 0) $s_weighted_mark = number_format($s_weighted_mark, $RoundDP);
										$s_grade = $MarkSummary["Grade"];
										
										unset($RawMarkSummary);
										unset($WeightedMarkSummary);
										unset($MarkSummaryWeightArr);
										$TotalRawMark = 0;
										$TotalWeightedMark = 0;
										$TotalMSWeight = 0;										
										
										if (!$IsComponent) {
											$idStartTag = "<div id='ColSum".$SubjectID."_".$ReportColumnID."'>";
											$idEndTag = "</div>";
											// display calculated mark
										} else if ($IsComponent) {
											$TempHorizontalMark[$ParentSubjectID][$ReportColumnID]["RawMark"] += $s_raw_mark;
											$TempHorizontalMark[$ParentSubjectID][$ReportColumnID]["WeightedMark"] += $s_weighted_mark;
											$TempHorizontalMark[$ParentSubjectID][$ReportColumnID]["Grade"] = $s_grade;
											$TempHorizontalMark[$ParentSubjectID][$ReportColumnID]["CellSetting"] = $CellSetting;
											$TempHorizontalMark[$ParentSubjectID][$ReportColumnID]["MarkTypeDisplay"] = $MarkTypeDisplay;
										}
									} else {
										// cal mark
										//if ($libreportcard->CalculationMethodRule)
										//{
											
											/*
											$MarkSummary["RawMark"] += $s_raw_mark * $MarkSummaryWeight / 100;
											$MarkSummary["WeightedMark"] += $s_weighted_mark * $MarkSummaryWeight / 100;
											$MarkSummary["Grade"] = $s_grade;		
											*/
																						
											
											$RawMarkSummary[$SubjectID][$ReportColumnID][] = $s_raw_mark;
											$WeightedMarkSummary[$SubjectID][$ReportColumnID][] = $s_weighted_mark;
											$MarkSummaryWeightArr[$SubjectID][$ReportColumnID][] = $MarkSummaryWeight;
											$MarkSummary["Grade"] = $s_grade;
											
												
										//} else {											
										//}
									}
									
									if($s_raw_mark>=0) {
										if($MarkTypeDisplay==2) {
											$DisplayResult = $s_grade;
											list($StylePrefix, $StyleSuffix, $IsDistinct, $IsFailed) = $libreportcard->GET_STYLE("G", $DisplayResult, $FailedArray[$SubjectID], $DistinctionArray[$SubjectID], $PFArray[$SubjectID]);											
										}
										else {
											list($StylePrefix, $StyleSuffix, $IsDistinct, $IsFailed) = $libreportcard->GET_STYLE($CellSetting, $s_raw_mark, $FailedArray[$SubjectID], $DistinctionArray[$SubjectID], $PFArray[$SubjectID]);
											$DisplayResult = ($MarkTypeDisplay==1) ? $s_raw_mark : $s_weighted_mark;
										}
									}
									else
										$DisplayResult = $libreportcard->SpecialMarkArray[trim($s_raw_mark)];
								}
								else {
									$DisplayResult = $ParStudentResultArray[$SubjectID][$ReportColumnID]["Grade"];
									if($DisplayResult!="/" && $DisplayResult!="abs") {
										list($StylePrefix, $StyleSuffix, $IsDistinct, $IsFailed) = $libreportcard->GET_STYLE($CellSetting, $DisplayResult, $FailedArray[$SubjectID], $DistinctionArray[$SubjectID], $PFArray[$SubjectID]);
									}
								}
								if ($DisplayResult != "")
									$DisplayResult = $idStartTag.$StylePrefix.$DisplayResult.$StyleSuffix.$idEndTag;
								$FormPosition = $ParStudentResultArray[$SubjectID][$ReportColumnID]["OMF"];
								$ClassPosition = $ParStudentResultArray[$SubjectID][$ReportColumnID]["OMC"];
							}

							$ResultTable .= "<td class='$tdClass' align='center'>";
							if($ShowPosition==1 || $ShowPosition==2)
							{
								//NotShowWholeYear
								if ((trim($DisplayResult)=="")||(trim($DisplayResult)=="/")) $NotShowWholeYear = true;
								$ResultTable .= ($DisplayResult==""?"----":$DisplayResult);

								$PosDisplay = ($ShowPosition==1) ? $ClassPosition : $FormPosition;
								$PosDisplay = ($PosDisplay>0 && ($PosDisplay>=$p_from && $PosDisplay<=$p_to)) ? $PosDisplay : "*";
								$ResultTable .= ($HidePosition==0 && $CellSetting=="S") ? "&nbsp;(".$PosDisplay.")" : "(*)";
							}
							else
							{
								//NotShowWholeYear
								if ((trim($DisplayResult)=="")||(trim($DisplayResult)=="/")) $NotShowWholeYear = true;
								$ResultTable .= (($DisplayResult=="")?"----":$DisplayResult);
							}
							$ResultTable .= "</td>";
						}

						if ($IsColumnSum)
						{
							// reset field at the back
							$MarkSummary["RawMark"] = 0;
							$MarkSummary["WeightedMark"] = 0;
							$MarkSummary["Grade"] = "--";
						}
						
						/*
						if ((($i % 2) == 1)&&(($ReportID != 18)&&($ReportID != 19))) {
							//$ResultTable .= "<td align='center' class='{$tdClass}'>----</td>";
						}
						*/
					}

					if($ColumnSize>1 && $HideOverallResult!=1)
					{
						if(!$OverallShow)
						{
							$ResultTable .= "<td class='$top_style' align='center'>&nbsp;</td>";
						}
						else
						{
							$StylePrefix = "";
							$StyleSuffix = "";
							if($CellSetting=="S") {
								$s_overall_mark = $ParStudentResultArray[$SubjectID]["Overall"]["Mark"];
								$s_overall_grade = $ParStudentResultArray[$SubjectID]["Overall"]["Grade"];

								if($s_overall_mark>=0) {
									if($MarkTypeDisplay==2) {
										$OverallResult = $s_overall_grade;
										list($StylePrefix, $StyleSuffix, $IsDistinct, $IsFailed) = $libreportcard->GET_STYLE("G", $OverallResult, $FailedArray[$SubjectID], $DistinctionArray[$SubjectID], $PFArray[$SubjectID]);
									}
									else {
										$OverallResult = $s_overall_mark;
										list($StylePrefix, $StyleSuffix, $IsDistinct, $IsFailed) = $libreportcard->GET_STYLE($CellSetting, $OverallResult, $FailedArray[$SubjectID], $DistinctionArray[$SubjectID], $PFArray[$SubjectID]);
									}
								}
								else
									$OverallResult = $libreportcard->SpecialMarkArray[trim($s_overall_mark)];
							}
							else {
								$OverallResult = $ParStudentResultArray[$SubjectID]["Overall"]["Grade"];
								if($OverallResult!="/" && $OverallResult!="abs") {
									list($StylePrefix, $StyleSuffix, $IsDistinct, $IsFailed) = $libreportcard->GET_STYLE($CellSetting, $OverallResult, $FailedArray[$SubjectID], $DistinctionArray[$SubjectID], $PFArray[$SubjectID]);
								}
							}
							if ($OverallResult != "")
								$OverallResult = $StylePrefix.$OverallResult.$StyleSuffix;
							$OverallFormPosition = $ParStudentResultArray[$SubjectID]["Overall"]["OMF"];
							$OverallClassPosition = $ParStudentResultArray[$SubjectID]["Overall"]["OMC"];

							$ResultTable .= "<td class='$top_style'><table border=0 cellpadding=0 cellspacing=0 width='100%'>";
							$ResultTable .= "<tr>";
							if (false)
							//($ShowOverallPosition==1 || $ShowOverallPosition==2)
							{
								//NotShowWholeYear
								if ($NotShowWholeYear) $OverallResult = "";
								$ResultTable .= "<td align='center' width='50%' class='$tdClass'>".($OverallResult==""?"----":$OverallResult)."</td>";
								$ResultTable .= "<td>&nbsp;</td>";

								$OverallPosDisplay = ($ShowOverallPosition==1) ? $OverallClassPosition : $OverallFormPosition;
								$OverallPosDisplay = ($OverallPosDisplay>0 && ($OverallPosDisplay>=$op_from && $OverallPosDisplay<=$op_to)) ? $OverallPosDisplay : "*";
								//aki
								if ($IsFailed) $OverallPosDisplay = "*";
								$ResultTable .= ($HidePosition==0 && $CellSetting=="S") ? "<td align='center' width='50%' class='$tdClass'>(".$OverallPosDisplay.")</td>" : "<td align='center' class='$tdClass' width='50%'>(*)</td>";
							}
							else
							{
								//NotShowWholeYear
								if ($NotShowWholeYear) $OverallResult = "";
								$ResultTable .= "<td align='center' class='$tdClass'>".($OverallResult==""?"----":$OverallResult)."</td>";
							}
							$ResultTable .= "</tr>";
							$ResultTable .= "</table></td>";
						}
					}
					/*
					if($ShowSubjectTeacherComment==1)
					{
						$Comment = $ParSubjectTeacherCommentArray[$SubjectID];
						$ResultTable .= "<td class='tabletext border_left $top_style' align='center'>".($Comment==""?"&nbsp;":$Comment)."</td>";
					}
					*/
					$ResultTable .= "<td>&nbsp;</td>";
					$ResultTable .= "</tr>";
					$count++;
					$NotShowWholeYear = false;
				}
			}
		}
		if (($libreportcard->CalculationOrder != "Horizontal")&&(is_array($TempHorizontalMark))) {
			$TempJSCode = "\n<script language='JavaScript'>\n";
			while(list($jsSubjectID, $jsSubjectArr) = each($TempHorizontalMark)) {
				while(list($jsReportColumnID, $jsValueArr) = each($jsSubjectArr)) {
					if ($jsValueArr['CellSetting'] == "S") {
						$DisplayValue = ($jsValueArr['MarkTypeDisplay']==1) ? $jsValueArr['RawMark'] : $jsValueArr['WeightedMark'];						
						$DisplayValue = number_format($DisplayValue, 2);
					} else {
						$DisplayValue = $jsValueArr['Grade'];
					}
					
					if($DisplayValue!="/" && $DisplayValue!="abs") {
						list($StylePrefix, $StyleSuffix, $IsDistinct, $IsFailed) = $libreportcard->GET_STYLE($jsValueArr['CellSetting'], $DisplayValue, $FailedArray[$jsSubjectID], $DistinctionArray[$jsSubjectID], $PFArray[$jsSubjectID]);
						if ($IsFailed) $DisplayValue = "*".$DisplayValue;
					}
					if ($DisplayValue == "/") $DisplayValue = "abs";
					//($MarkTypeDisplay==1) ? $s_raw_mark : $s_weighted_mark;
					$TempJSCode .= "\tdocument.getElementById('ColSum".$jsSubjectID."_".$jsReportColumnID."').innerHTML = '".$DisplayValue."'\n";
				}
			}
			$TempJSCode .= "</script>\n";
		}
		
		for ($tempi = $count; $tempi < 15; $tempi++) {
			//$ResultTable .= "<tr><td align='center' colspan='".($ColumnSpan+1+2)."'>&nbsp;</td></tr>";
			$ResultTable .= "<tr><td align='center' colspan='".($ColumnSpan)."'>&nbsp;</td></tr>";
		}
		//$ResultTable .= "<tr><td align='center' colspan='".($ColumnSpan+1+2)."'>&nbsp;</td></tr>";
		$ResultTable .= "<tr><td align='center' colspan='".($ColumnSpan)."'>&nbsp;</td></tr>";
	}
	else
	{
		$ResultTable .= "<tr><td align='center' class='small_text report_formfieldtitle' colspan='".($ColumnSpan)."'>".$eReportCard['NoRecord']."</td></tr>";
	}
	
	if(!empty($ECAResultArray[$StudentID]))
	{
		$ResultTable .= "";
		$ColumnSpan = $ColumnSpan - 2;
		
		for($k=0; $k<sizeof($ECAResultArray[$StudentID]); $k++)
		{
			$NotShowWholeYear = false;
			$ResultTable .= "<tr class='body_content'>";
			$tdClass = "body_content";
			$colspan = ($ShowFullMark==1) ? "colspan='2'" : "";
			// line height - aki
			if ($k == 0) {
				$ResultTable .= "<td>&nbsp;</td>";
				$ResultTable .= "<td colspan='".$ColumnSpan."' class='body_midtitle_line' height='$LineHeight'>課外活動 Extra-curricular Activities</td>";
				$ResultTable .= "<td>&nbsp;</td>";
				$ResultTable .= "</tr>";
			} else {
				$ResultTable .= "<td>&nbsp;</td>";
				$ResultTable .= "<td class='{$tdClass}' {$colspan} height='$LineHeight'>".$ECAResultArray[$StudentID][$k]["Title"]."</td>";
				for($i=0; $i<sizeof($ColumnArray); $i++)
				{
					$ColumnTitle = trim($ColumnArray[$i]["ColumnTitle"]);
					$ResultDisplay = $ECAResultArray[$StudentID][$k][$ColumnTitle];

					if (($ResultDisplay == "")||($ResultDisplay == "----")) {
						while (list($ColumnTitleName, $value) = each($ECAResultArray[$StudentID][$k])) {

							if ((preg_match("/".$ColumnTitleName."/", $ColumnTitle))||(preg_match("/".$ColumnTitle."/", $ColumnTitleName)))
							{
								$ResultDisplay = str_replace("<br>", "", $ECAResultArray[$StudentID][$k][$ColumnTitleName]);
							}
						}
					}

					//NotShowWholeYear
					if ($ResultDisplay=="") $NotShowWholeYear = true;
					$ResultTable .= "<td class='{$tdClass}' align='center'>".($ResultDisplay==""?"----":$ResultDisplay)."</td>";
				}
				if($ColumnSize>1 && $HideOverallResult!=1) {
				//if($Semester=='FULL' && $HideOverallResult==1) {
				//if($HideOverallResult==0) {
					if ($NotShowWholeYear) $ECAResultArray[$StudentID][$k][$Semester] = "----";
					$ResultTable .= "<td class='{$tdClass}' align='center'>".($ECAResultArray[$StudentID][$k][$Semester]==""?"----":$ECAResultArray[$StudentID][$k][$Semester])."</td>";
				}
				$ResultTable .= "<td>&nbsp;</td>";
			}
			$ResultTable .= "</tr>";
		}
		$TempCountVal = 4;
		if ($ReportID == 14) $TempCountVal = 2;
		for ($tempi = sizeof($ECAResultArray[$StudentID]); $tempi < $TempCountVal; $tempi++) {
			$ResultTable .= "<tr>";
			$ResultTable .= "<td>&nbsp;</td>";
			$ResultTable .= "<td class='{$tdClass}' {$colspan} height='$LineHeight'>&nbsp;</td>";
			for($i=0; $i<sizeof($ColumnArray); $i++)
			{
				$ResultTable .= "<td class='{$tdClass}' align='center'>&nbsp;</td>";
			}
			if($Semester=='FULL' && $HideOverallResult==1)
			//if($HideOverallResult==0)
				$ResultTable .= "<td class='{$tdClass}' align='center'>&nbsp;</td>";
			$ResultTable .= "<td>&nbsp;</td>";
			$ResultTable .= "</tr>";
		}

		$ResultTable .= "<tr>";
		$ResultTable .= "<td>&nbsp;</td>";
		$ResultTable .= "<td class='{$tdClass}' {$colspan} height='$LineHeight'>&nbsp;</td>";
		for($i=0; $i<sizeof($ColumnArray); $i++)
		{
			$ResultTable .= "<td class='{$tdClass}' align='center'>&nbsp;</td>";
		}
		if($Semester=='FULL' && $HideOverallResult==1)
		//if($HideOverallResult==0)
			$ResultTable .= "<td class='{$tdClass}' align='center'>&nbsp;</td>";
		$ResultTable .= "<td>&nbsp;</td>";
		$ResultTable .= "</tr>";
	}
	
	if(!empty($SummaryResultArray))
	{
		$TotalTempMarkWeight = 0;
		for($k=0; $k<sizeof($SummaryResultArray); $k++)
		{
			$NotShowWholeYear = false;
			$ResultTable .= "<tr class='body_content'>";

			$tdClass = "body_content";
			$colspan = ($ShowFullMark==1) ? "colspan='2'" : "";

			$ResultTable .= "<td class='body_content'>&nbsp;</td>";
			$ResultTable .= "<td class='$tdClass' height='$LineHeight'>";
			$ResultTable .= "<table border='0' cellspacing='0' cellpadding='0' width='100%'><tr><td class='$tdClass' width='40%' nowrap>";
			$ResultTable .= $SummaryResultArray[$k]["ChiTitle"];
			$ResultTable .= "</td><td class='$tdClass' width='60%' nowrap>";
			$ResultTable .= $SummaryResultArray[$k]["EngTitle"];
			$ResultTable .= "</td></tr></table></td>";
			if ($ShowFullMark) $ResultTable .= "<td class='body_content'>&nbsp;</td>";

			// line height - aki
			for($i=0; $i<sizeof($ColumnArray); $i++)
			{
				$ColumnTitle = trim($ColumnArray[$i]["ColumnTitle"]);
				$ResultDisplay = str_replace("<br>", "", $SummaryResultArray[$k][$ColumnTitle]);

				if (($ResultDisplay == "")||($ResultDisplay == "----")) {
					while (list($ColumnTitleName, $value) = each($SummaryResultArray[$k])) {

						if ((preg_match("/".$ColumnTitleName."/", $ColumnTitle))||(preg_match("/".$ColumnTitle."/", $ColumnTitleName)))
						{
							$ResultDisplay = str_replace("<br>", "", $SummaryResultArray[$k][$ColumnTitleName]);
						}
					}
				}
				
				$ReportColumnID = $ColumnArray[$i]["ReportColumnID"];
				if ($ColumnArray[$i]["IsColumnSum"]) {
					if ($SummaryResultArray[$k]['EngTitle'] == "Position in Class") {
						$ResultDisplay = "<div id='Rank".$ReportColumnID."_".$StudentID."'>".$ResultDisplay."</div>";
					} else if ($SummaryResultArray[$k]['EngTitle'] == "Total Marks")
					{
						
						if (!isset($TempGrandTotal[$ReportColumnID][$StudentID]))
						{							
							while(list($TempReportColumnID, $TempStudentArr) = each($TempMarkSummaryWeightArr)) {
								while(list($TempStudentID, $TempMarkSummaryWeight) = each($TempStudentArr)) {
									if ($TempResultDisplayArr[$TempReportColumnID][$TempStudentID] > 0) $TotalTempMarkWeight += $TempMarkSummaryWeight;
								}
							}							
							
							if ($TotalTempMarkWeight > 0)
								while(list($TempReportColumnID, $TempStudentArr) = each($TempResultDisplayArr)) {
									while(list($TempStudentID, $TempResultDisplay) = each($TempStudentArr)) {
										$TempMark += $TempResultDisplay * $TempMarkSummaryWeightArr[$TempReportColumnID][$TempStudentID] / $TotalTempMarkWeight;
									}
								}

							$TempGrandTotal[$ReportColumnID][$StudentID] = $TempMark;
						}
						
						$ResultDisplay = $TempMark;
						list($StylePrefix, $StyleSuffix, $IsDistinct, $IsFailed) = $libreportcard->GET_STYLE("S", $ResultDisplay, $FailedArray[6], $DistinctionArray[6], $PFArray[6]);
						if ($IsFailed) $ResultDisplay = "*".$ResultDisplay;
						$TempMark = 0;
						unset($TempResultDisplayArr);
						unset($TempMarkSummaryWeightArr);
						$TotalTempMarkWeight = 0;
					} else if ($SummaryResultArray[$k]['EngTitle'] == "Average Marks")
					{						
						if (!isset($TempAverage[$ReportColumnID][$StudentID]))
						{							
							while(list($TempReportColumnID, $TempStudentArr) = each($TempMarkSummaryWeightAverageArr)) {
								while(list($TempStudentID, $TempMarkSummaryWeight) = each($TempStudentArr)) {
									if ($TempResultDisplayAverageArr[$TempReportColumnID][$TempStudentID] > 0) $TotalTempMarkAverageWeight += $TempMarkSummaryWeight;
								}
							}							
							//if ($StudentID == 4048) hdebug($TotalTempMarkAverageWeight);
							if ($TotalTempMarkAverageWeight > 0)
								while(list($TempReportColumnID, $TempStudentArr) = each($TempResultDisplayAverageArr)) {
									while(list($TempStudentID, $TempResultDisplay) = each($TempStudentArr)) {
										$TempMark += $TempResultDisplay * $TempMarkSummaryWeightAverageArr[$TempReportColumnID][$TempStudentID] / $TotalTempMarkAverageWeight;
									}
								}

							$TempAverage[$ReportColumnID][$StudentID] = $TempMark;
						}
						$ResultDisplay = $TempMark;
						list($StylePrefix, $StyleSuffix, $IsDistinct, $IsFailed) = $libreportcard->GET_STYLE("S", $ResultDisplay, $FailedArray[6], $DistinctionArray[6], $PFArray[6]);
						if ($IsFailed) $ResultDisplay = "*".$ResultDisplay;
						$TempMark = 0;
						unset($TempResultDisplayAverageArr);
						unset($TempMarkSummaryWeightAverageArr);
						$TotalTempMarkAverageWeight = 0;
					} else {
						$ResultDisplay = $TempMark;
						$TempMark = "";
					}
				} else {					
					//$TempMark += $ResultDisplay * $ColumnArray[$i]["MarkSummaryWeight"] / 100;
					if (($SummaryResultArray[$k]['EngTitle'] == "No. of Students in Class") || 						
						($SummaryResultArray[$k]['EngTitle'] == "Conduct Grade")
						)
					{
						$TempMark = $ResultDisplay;
					} else if (($SummaryResultArray[$k]['EngTitle'] == "No. of Periods Absent") ||
								($SummaryResultArray[$k]['EngTitle'] == "No. of Times Late")
								)
					{
						$TempMark += $ResultDisplay;
					} else if ($SummaryResultArray[$k]['EngTitle'] == "Total Marks")
					{
						//$TempMark += trim($ResultDisplay, '*') * $ColumnArray[$i]["MarkSummaryWeight"] / 100;		
						$NotDisplay[$ReportColumnID] = (trim($ResultDisplay, '*') == 0) ? true : false;
													
						if (!isset($TempResultDisplayArr[$ReportColumnID][$StudentID]))
							$TempResultDisplayArr[$ReportColumnID][$StudentID] = trim($ResultDisplay, '*');
						if (!isset($TempMarkSummaryWeightArr[$ReportColumnID][$StudentID]))
							$TempMarkSummaryWeightArr[$ReportColumnID][$StudentID] = $ColumnArray[$i]["MarkSummaryWeight"];

					} else if ($SummaryResultArray[$k]['EngTitle'] == "Average Marks")
					{	
						
						if (!isset($TempResultDisplayAverageArr[$ReportColumnID][$StudentID]))
							$TempResultDisplayAverageArr[$ReportColumnID][$StudentID] = trim($ResultDisplay, '*');
						if (!isset($TempMarkSummaryWeightAverageArr[$ReportColumnID][$StudentID]))
							$TempMarkSummaryWeightAverageArr[$ReportColumnID][$StudentID] = $ColumnArray[$i]["MarkSummaryWeight"];

					} else {
						$TempMark += $ResultDisplay;
					}
				}
				
				if ($i > 2) $ResultDisplay = "";
				if ($NotDisplay[$ReportColumnID]) $ResultDisplay = "";		

				if ((($ReportID == 18)||($ReportID == 19))&&
					($SummaryResultArray[$k]['EngTitle'] == "Position in Class"))
				{
					$ResultDisplay = "<div id='Rank".$ReportColumnID."_".$StudentID."'>".$ResultDisplay."</div>";
				}
				
				if (($i > 1)&&(($ReportID == 18)||($ReportID == 19))) $ResultDisplay = "";
				
				//NotShowWholeYear
				if ($ResultDisplay=="") $NotShowWholeYear = true;
				$ResultTable .= "<td align='center' class='$tdClass'>".($ResultDisplay==""?"----":$ResultDisplay)."</td>";
				/*
				if ((($i % 2) == 1)&&(($ReportID != 18)&&($ReportID != 19))) {
					//$ResultTable .= "<td align='center' class='{$tdClass}'>----</td>";
				}
				*/
			}
			//if($Semester=='FULL' && $HideOverallResult==1) {
			if($HideOverallResult==0) {
				if ($NotShowWholeYear) $SummaryResultArray[$k][$Semester] = "";
				$ResultTable .= "<td align='center' class='$tdClass'>".($SummaryResultArray[$k][$Semester]==""?"----":$SummaryResultArray[$k][$Semester])."</td>";
			}

			$ResultTable .= "<td>&nbsp;</td>";
			$ResultTable .= "</tr>";
		}

		/*
		$ResultTable .= "<tr class='body_content'>";

		$tdClass = "body_content";
		$colspan = ($ShowFullMark==1) ? "colspan='2'" : "";
		$ResultTable .= "<td>&nbsp;</td>";
		$ResultTable .= "<td class='$tdClass' {$colspan} height='$LineHeight'>&nbsp;</td>";

		// line height - aki
		for($i=0; $i<sizeof($ColumnArray); $i++)
		{
			$ResultTable .= "<td align='right' class='$tdClass'>&nbsp;</td>";
			if ((($i % 2) == 1)&&(($ReportID != 18)&&($ReportID != 19))) {
				$ResultTable .= "<td>&nbsp;</td>";
			}
		}
		//if($Semester=='FULL' && $HideOverallResult==1)
		if($HideOverallResult==0)
			$ResultTable .= "<td align='right' class='$tdClass'>&nbsp;</td>";

		$ResultTable .= "<td>&nbsp;</td>";
		$ResultTable .= "</tr>";
		*/
	}
	
	//$TempMark = 0;
	if(!empty($MeritResultArray))
	{
		for($k=0; $k<sizeof($MeritResultArray); $k++)
		{
			$NotShowWholeYear = false;
			$ResultTable .= "<tr class='body_content'>";
			if ($k==0) {
				$tdClass = "boxbg_T";
			} else if ($k == sizeof($MeritResultArray)-1) {
				$tdClass = "boxbg_B";
			} else {
				$tdClass = "boxbg";
			}
			$colspan = ($ShowFullMark==1) ? "colspan='2'" : "";

			$ResultTable .= "<td class='$tdClass'>&nbsp;</td>";
			$ResultTable .= "<td class='$tdClass' height='$LineHeight'>";
			$ResultTable .= "<table border='0' cellpadding='0' cellspacing='0' width='100%'><tr><td class='body_content' width='40%' nowrap>";
			$ResultTable .= $MeritResultArray[$k]["ChiTitle"]."</td><td class='body_content' width='60%' nowrap>";
			$ResultTable .= $MeritResultArray[$k]["EngTitle"]."</td></tr></table>";
			$ResultTable .= "</td>";
			if ($ShowFullMark) $ResultTable .= "<td class='$tdClass'>&nbsp;</td>";

			// line height - aki
			for($i=0; $i<sizeof($ColumnArray); $i++)
			{
				$ColumnTitle = trim($ColumnArray[$i]["ColumnTitle"]);
				$ResultDisplay = $MeritResultArray[$k][$ColumnTitle];
				
				if (($ResultDisplay == "")||($ResultDisplay == "----")) {
					while (list($ColumnTitleName, $value) = each($MeritResultArray[$k])) {
						if ((preg_match("/".$ColumnTitleName."/", $ColumnTitle))||(preg_match("/".$ColumnTitle."/", $ColumnTitleName)))
						{
							$ResultDisplay = str_replace("<br>", "", $MeritResultArray[$k][$ColumnTitleName]);
						}
					}
				}
				
				$ReportColumnID = $ColumnArray[$i]["ReportColumnID"];
				if ($ColumnArray[$i]["IsColumnSum"]) {
					//debug_r($ColumnTitle);
					if (!isset($TempGrandTotal[$ReportColumnID][$StudentID]))
						$TempGrandTotal[$ReportColumnID][$StudentID] = $TempMark;
					$ResultDisplay = $TempMark;
					$TempMark = 0;
				} else {
					$TempMark += $ResultDisplay;
				}
				
				if ($i > 2) $ResultDisplay = "";
				if (($i > 1)&&(($ReportID == 18)||($ReportID == 19))) $ResultDisplay = "";
				//NotShowWholeYear
				if ($ResultDisplay=="") $NotShowWholeYear = true;
				
				$ResultTable .= "<td align='center' class='$tdClass'>".($ResultDisplay==""?"----":$ResultDisplay)."</td>";
			}
			//if($Semester=='FULL' && $HideOverallResult==1) {
			if($HideOverallResult==0) {
				if ($NotShowWholeYear) $MeritResultArray[$k][$Semester] = "";
				$ResultTable .= "<td align='center' class='$tdClass'>".($MeritResultArray[$k][$Semester]==""?"----":$MeritResultArray[$k][$Semester])."</td>";
			}

			$ResultTable .= "<td class='$tdClass'>&nbsp;</td>";
			$ResultTable .= "</tr>";
		}
	}

	$ResultTable .= GENERATE_MISC_TABLE($ClassTeacherCommentArray[$StudentID], $StudentID);

	$ResultTable .= "</table></td></tr></table>".$TempJSCode;

	return $ResultTable;
}

# generate attendance table
function GENERATE_ATTENDANCE_AND_MERIT_TABLE($ParSummaryArray, $ParMeritArray)
{
	global $SummaryTitleArray, $SettingSummaryArray, $SettingMiscArray, $ColumnArray, $eReportCard, $SemesterArray, $LangArray;
	global $DaysAbsentTitle, $TimesLateTitle, $AbsentWOLeaveTitle;

	$AttendanceTableShow = ($SettingSummaryArray["DaysAbsent"]==1 || $SettingSummaryArray["TimesLate"] || $SettingSummaryArray["AbsentWOLeave"]);
	$MeritTableShow = ($SettingMiscArray["MeritsAndDemerits"]==1);

	$SemColumnCount = 0;
	for($i=0; $i<sizeof($ColumnArray); $i++)
	{
		$sem_title = trim($ColumnArray[$i]["ColumnTitle"]);
		if($sem_title!="") {
			if(in_array($sem_title, $SemesterArray))
			{
				$SemesterHeaderRow .= ($AttendanceTableShow==1 || $MeritTableShow==1) ? "<td class='body_subtitle_small border_top border_left' align='center'>".$sem_title."</td>\n" : "";

				$absent_result = $ParSummaryArray[$sem_title][0];
				$DaysAbsentCell .= "<td class='body_subtitle_small border_top border_left' align='center'>".($absent_result==""?"----":$absent_result)."</td>\n";

				$late_result = $ParSummaryArray[$sem_title][1];
				$LateCell .= "<td class='body_subtitle_small border_top border_left' align='center'>".($late_result==""?"----":$late_result)."</td>\n";

				$absent_wo_leave_result = $ParSummaryArray[$sem_title][2];
				$AbsentWOLeaveCell .= "<td class='body_subtitle_small border_top border_left' align='center'>".($absent_wo_leave_result==""?"----":$absent_wo_leave_result)."</td>\n";

				$MeritResult = $ParMeritArray[$sem_title][0];
				$MeritCell .= "<td class='body_subtitle_small border_top border_left' align='center'>".($MeritResult==""?"----":$MeritResult)."</td>\n";

				$DemeritResult = $ParMeritArray[$sem_title][1];
				$DemeritCell .= "<td class='body_subtitle_small border_top border_left' align='center'>".($DemeritResult==""?"----":$DemeritResult)."</td>\n";

				$SemColumnCount++;
			}

		}
	}

	if($SemColumnCount>0 && ($AttendanceTableShow==1 || $MeritTableShow==1))
	{
		$ReturnTable = "<table width='100%' valign='bottom' border='0' cellspacing='0' cellpadding='0'>\n";
		if($AttendanceTableShow==1)
		{
			$ReturnTable .= "<tr><td class='body_subtitle_small border_top'>".$LangArray['Attendance']."</td>\n";
			$ReturnTable .= $SemesterHeaderRow;
		}

		# Rows of Attendance records
		if($SettingSummaryArray["DaysAbsent"]==1)
		{
			$ReturnTable .= "<tr><td class='body_subtitle_small border_top'>".$DaysAbsentTitle."</td>\n";
			$ReturnTable .= $DaysAbsentCell;
			$ReturnTable .= "</tr>";
		}
		if($SettingSummaryArray["TimesLate"]==1)
		{
			$ReturnTable .= "<tr><td class='body_subtitle_small border_top'>".$TimesLateTitle."</td>\n";
			$ReturnTable .= $LateCell;
			$ReturnTable .= "</tr>";
		}
		if($SettingSummaryArray["AbsentWOLeave"]==1)
		{
			$ReturnTable .= "<tr><td class='body_subtitle_small border_top'>".$AbsentWOLeaveTitle."</td>";
			$ReturnTable .= $AbsentWOLeaveCell;
			$ReturnTable .= "</tr>";
		}

		# Row of Merit records
		if($MeritTableShow==1)
		{
			if($AttendanceTableShow==1) {
				$ReturnTable .= "<tr><td class='body_subtitle_small border_top' colspan='".($SemColumnCount+1)."'>".$LangArray['MeritsAndDemerits']."</td></tr>\n";
			}
			else {
				$ReturnTable .= "<tr><td class='body_subtitle_small border_top'>".$LangArray['MeritsAndDemerits']."</td>\n";
				$ReturnTable .= $SemesterHeaderRow;
			}

			$ReturnTable .= "<tr><td class='body_subtitle_small border_top'>".$LangArray['Merit']."</td>\n";
			$ReturnTable .= $MeritCell;
			$ReturnTable .= "</tr>";
			$ReturnTable .= "<tr><td class='body_subtitle_small border_top'>".$LangArray['Demerit']."</td>\n";
			$ReturnTable .= $DemeritCell;
			$ReturnTable .= "</tr>";
		}
		$ReturnTable .= "</table>";
	}

	return $ReturnTable;
}

################################ Function End #####################################

function CAL_POSITION() {

	global $SummaryResultArray, $TempGrandTotalArr, $ColumnArray, $StudentID, $ReportID;
	//hdebug_r($SummaryResultArray);
	if(!empty($SummaryResultArray))
	{
		for($k=0; $k<sizeof($SummaryResultArray); $k++)
		{
			for($i=0; $i<sizeof($ColumnArray); $i++)
			{
				$ColumnTitle = trim($ColumnArray[$i]["ColumnTitle"]);
				$ResultDisplay = str_replace("<br>", "", $SummaryResultArray[$k][$ColumnTitle]);

				if (($ResultDisplay == "")||($ResultDisplay == "----")) {
					while (list($ColumnTitleName, $value) = each($SummaryResultArray[$k])) {

						if ((preg_match("/".$ColumnTitleName."/", $ColumnTitle))||(preg_match("/".$ColumnTitle."/", $ColumnTitleName)))
						{
							$ResultDisplay = str_replace("<br>", "", $SummaryResultArray[$k][$ColumnTitleName]);							
						}
					}
				}
				
				$ReportColumnID = $ColumnArray[$i]["ReportColumnID"];
				if ($ColumnArray[$i]["IsColumnSum"]) {
					if ($SummaryResultArray[$k]['EngTitle'] == "Average Marks")
					{
						if (!isset($TempGrandTotalArr[$ReportColumnID][$StudentID]))
							$TempGrandTotalArr[$ReportColumnID][$StudentID] = $TempMark;
						$TempMark = 0;
					}
				} else {					
					if ($SummaryResultArray[$k]['EngTitle'] == "Average Marks")
					{						
						if (($ReportID == 18)||($ReportID == 19))
						{
							if (!isset($TempGrandTotalArr[$ReportColumnID][$StudentID]))
								$TempGrandTotalArr[$ReportColumnID][$StudentID] = trim($ResultDisplay, '*');
							$TempMark = 0;
						} else {
							$TempMark += trim($ResultDisplay, '*') * $ColumnArray[$i]["MarkSummaryWeight"] / 100;	
						}
					}					
				}
			}
		}
	}	
}



?>