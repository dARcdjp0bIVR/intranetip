<?php
$PATH_WRT_ROOT = "../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_header.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

intranet_auth();
intranet_opendb();

echo "<script>if(typeof(opener)!='undefined') { opener.close(); }</script>";

if ($plugin['ReportCard'])
{
	include_once($PATH_WRT_ROOT."includes/libreportcard.php");
	include_once($PATH_WRT_ROOT."home/eAdmin/StudentMgmt/reportcard/report_generation/print_report_functions2.php");

	$libreportcard = new libreportcard();
	$libformclass = new year_class();
	$CurrentPage = "ReportGeneration";
	if ($libreportcard->hasAccessRight())
    {
	    $linterface = new interface_html();
#######################################################################################################
		
		# get current Year and Semester
		$Year = trim($libreportcard->Year);
		$Semester = trim($libreportcard->Semester);
		$SemesterName = trim($libreportcard->SemesterName);

		# Get Report Info
		$ReportInfo = $libreportcard->GET_REPORT_TEMPLATE($ReportID);
		
		list($ReportTitle, $ReportType, $Description, $ShowFullMark, $Settings, $ReportStatus, $ShowOverallPosition, $OverallPositionRange, $HideNotEnrolled, $Footer, $NoHeader, $LineHeight, $SignatureWidth) = $ReportInfo;
		$SettingArray = unserialize($Settings);
		$BilingualReportTitle = $SettingArray["BilingualReportTitle"];
		
		$ReportTemplateSemester = $ReportType;
		$TermObj = new academic_year_term($ReportTemplateSemester,$GetAcademicYearInfo=false);
		$ReportTemplateSemesterName = $TermObj->YearTermNameEN;
		# Get term number - To determine display Overall Result
		$currentReportTermNumber = $TermObj->Get_Term_Number();
		
		# define the name of the variable array by checking the bilingual setting
		if($BilingualReportTitle==1)
		{
			$LangArray = $eReportCardBilingual;

			# prepare titles
			$DaysAbsentTitle = $LangArray["DaysAbsent"];
			$pos = strpos($DaysAbsentTitle, " ");
			$DaysAbsentTitle = substr_replace($DaysAbsentTitle, "<br />", $pos, 1);

			$TimesLateTitle = $LangArray["TimesLate"];
			$pos = strpos($TimesLateTitle, " ");
			$TimesLateTitle = substr_replace($TimesLateTitle, "<br />", $pos, 1);

			$AbsentWOLeaveTitle = $LangArray["AbsentWOLeave"];
			$pos = strpos($AbsentWOLeaveTitle, " ");
			$AbsentWOLeaveTitle = substr_replace($AbsentWOLeaveTitle, "<br />", $pos, 1);
		}
		else {
			$LangArray = $eReportCard;

			# prepare titles
			$DaysAbsentTitle = $LangArray["DaysAbsent"];
			$TimesLateTitle = $LangArray["TimesLate"];
			$AbsentWOLeaveTitle = $LangArray["AbsentWOLeave"];
		}

		# Get Class Level(s) of the report
		if($ClassLevelID!="")
			$SelectedFormArr = array($ClassLevelID);
		else
			$SelectedFormArr = $libreportcard->GET_REPORT_TEMPLATE_FORM($ReportID);

		# Get Class Student(s)
		$LevelList = implode(",", $SelectedFormArr);

		$TargetStudentList = (is_array($TargetStudentID)) ? implode(",", $TargetStudentID) : "";

		list($ClassStudentArray, $StudentIDArray) = $libreportcard->GET_STUDENT_BY_CLASSLEVEL($ReportID, $LevelList, $ClassID, $TargetStudentList);
		
		# Get Class Teacher Comment
		$ClassTeacherCommentArray = $libreportcard->GET_REPORT_CLASS_TEACHER_COMMENT($LevelList, $ClassID, $TargetStudentList);
		
		# Get Subject Teacher Comment
		$SubjectTeacherCommentArray = $libreportcard->GET_REPORT_SUBJECT_TEACHER_COMMENT($LevelList, $ClassID, $TargetStudentList);

		# Get Class Teacher(s)
		$ClassTeacherArray = $libreportcard->GET_CLASSTEACHER_BY_CLASSLEVEL($LevelList);

		$StudentInfoTitleArray = $eReportCard['DisplaySettingsArray']["StudentInfo"];
		$SettingStudentInfo = $SettingArray["StudentInfo"];

		$MiscTitleArray = $eReportCard['DisplaySettingsArray']["Misc"];
		$SettingMiscArray = $SettingArray["Misc"];
		$ShowSubjectTeacherComment = $SettingMiscArray["SubjectTeacherComment"];

		$SummaryTitleArray = $eReportCard['DisplaySettingsArray']["Summary"];
		
		### add merit to display detail (aki)
		$SettingSummaryArray = $SettingArray["Summary"];
		if ($SettingMiscArray["MeritsAndDemerits"]==1) {
			$SettingSummaryArray['Merit'] = 1;
			$SettingSummaryArray['Demerit'] = 1;
			$SettingSummaryArray['Offence'] = 1;

			$SummaryTitleArray[] = "Merit";
			$SummaryTitleArray[] = "Demerit";
			$SummaryTitleArray[] = "Offence";
		}

		# Get Report Subject and Column
		$SubjectArray = $libreportcard->GET_REPORT_TEMPLATE_SUBJECT($ReportID);
		$ColumnArray = $libreportcard->GET_REPORT_TEMPLATE_COLUMN($ReportID);

		# Get Report Cell Setting
		$ReportCellSetting = $libreportcard->GET_REPORT_CELL($ReportID);

		# Get All subject array
		$AllSubjectArray = $libreportcard->GET_ALL_SUBJECTS();

		# Get Report Marksheet result
		$SubjectIDArray = $libreportcard->GET_REPORT_SELECTED_SUBJECT_ID($ReportID);
		if(is_array($SubjectIDArray) && is_array($StudentIDArray))
		{
			$SubjectIDList = implode(",", $SubjectIDArray);
			$StudentIDList = implode(",", $StudentIDArray);
			$ReportMarksheetArray = $libreportcard->GET_REPORT_MARKSHEET_SCORES($SubjectIDList, $StudentIDList);
		}


		# Get Grademark scheme of subjects
		$SubjectGradingArray = $libreportcard->GET_REPORT_SUBJECT_GRADING($ReportID, $SelectedFormArr);

		if(is_array($SubjectGradingArray))
		{
			$SubjectGradingList = implode(",", $SubjectGradingArray);
			list($FailedArray, $DistinctionArray, $PFArray) = $libreportcard->GET_REPORT_GRADEMARK($SubjectGradingList);
		}

		# Get Highest Average in Class
		$HighestAverageArray = $libreportcard->GET_CLASS_HIGHEST_AVERAGE($LevelList);

		# Get Subject and Subject Code Mapping Array
		$SubjectCodeMappingArr = GET_SUBJECT_SUBJECTCODE_MAPPING();
		
		# Get Semesters List
		$allSemester = getSemesters();
		
		# Page Break Style
		$page_break_here = "<p class='breakhere'></p>\n";

		$lf = new libfilesystem();
		$SemesterArray = $libreportcard->GET_SEMESTER_ARRAY();
		
		$SemColumnExist = 0;
		for($i=0; $i<sizeof($ColumnArray); $i++)
		{
			$ColumnTitle = trim($ColumnArray[$i]["ColumnTitle"]);
			if(in_array($ColumnTitle, $SemesterArray))
			{
				//$ColumnSemesterArray[] = $ColumnTitle;
				$SemColumnExist = 1;
				break;
			}
		}

		$StudentRegNoArray = $libreportcard->GET_STUDENT_REGNO_ARRAY($LevelList, $ClassID, $TargetStudentList);
		#####################################################################
		# get summary data from csv file

		//$SummaryHeader = array("REGNO","ABSENT","LATE","ABSENT_WO_LEAVE","CONDUCT","POLITENESS","BEHAVIOUR","APPLICATION","TIDINESS");
		$SummaryHeader = array("REGNO","CONDUCT","TIDINESS","APPLICATION","DAYS_ABSENT","TIMES_LATE","DAYS_ABSENT_WO_REASON");
		$TargetSummaryFile = $intranet_root."/file/reportcard/summary";
		$StudentSummaryArray = GET_CSV_FILE_CONTENT($TargetSummaryFile, $SummaryHeader);
		
		
//		debug_pr($StudentSummaryArray);
		#####################################################################

		#####################################################################
		# get award data from csv file

		//$AwardHeader = array("REGNO", "MERITS", "ATTENDANCE", "COMPETITIONS", "PERFORMANCES", "SERVICES");
// 		$AwardHeader = array("REGNO", "ATTENDANCE", "COMPETITIONS", "PERFORMANCES", "SERVICES");
		$AwardHeader = array("REGNO", "COMPETITIONS", "PERFORMANCES", "SERVICES");
		$TargetAwardFile = $intranet_root."/file/reportcard/award";
		$StudentAwardArray = GET_CSV_FILE_CONTENT($TargetAwardFile, $AwardHeader);
		#####################################################################

		#####################################################################
		# get merit data from csv file

		$MeritHeader = ($ReportCardTemplate==2) ? array("REGNO", "MERIT", "DEMERIT") : array("REGNO", "TITLE");
		$TargetMeritFile = $intranet_root."/file/reportcard/merit";
		$StudentMeritArray = GET_CSV_FILE_CONTENT($TargetMeritFile, $MeritHeader);
		//hdebug_r($StudentMeritArray);
		#####################################################################

		#####################################################################
		# get eca data from csv file

		$ECAHeader = array("REGNO", "TITLE");
		$TargetECAFile = $intranet_root."/file/reportcard/eca";
		$StudentECAArray = GET_CSV_FILE_CONTENT($TargetECAFile, $ECAHeader);
		#####################################################################

		#####################################################################
		# get remark data from csv file

		//$RemarkHeader = array("REGNO", "DEMERITS", "FORGETFULNESS", "TARDINESS", "MISBEHAVIOUR");
		$RemarkHeader = array("REGNO", "FORGETFULNESS", "MISBEHAVIOUR");
		$TargetRemarkFile = $intranet_root."/file/reportcard/remark";
		$StudentRemarkArray = GET_CSV_FILE_CONTENT($TargetRemarkFile, $RemarkHeader);
		#####################################################################

		#####################################################################
		# get inter school competition data from csv file

		$InterSchoolHeader = array("REGNO", "TITLE");
		$TargetInterSchoolFile = $intranet_root."/file/reportcard/interschool";
		$StudentInterSchoolArray = GET_CSV_FILE_CONTENT($TargetInterSchoolFile, $InterSchoolHeader);
		#####################################################################

		#####################################################################
		# get school service data from csv file

		$SchoolServiceHeader = array("REGNO", "TITLE");
		$TargetSchoolServiceFile = $intranet_root."/file/reportcard/schoolservice";
		$StudentSchoolServiceArray = GET_CSV_FILE_CONTENT($TargetSchoolServiceFile, $SchoolServiceHeader);
		#####################################################################

		# get issue date
		$IssueDate = $libreportcard->GET_ISSUE_DATE($ReportID);

#######################################################################################################
		# Generate Report
		# Generate Title Table
		if ($NoHeader == -1) {
			$TitleTable = GENERATE_TITLE_TABLE($ReportTitle);
		} else {
// 			$TitleTable = "<center><font face='times' size='5'><b>Santa Rosa de Lima English Secondary School
// 			<br />Macau</b>
// 			<br /><font size='4'>ACADEMIC REPORT</font>
// 			</font></center>";
			$TitleTable = "<center><font face='times' size='5'><b>Colegio de Santa Rosa de Lima
			<br />English Secondary</b>
			<br /><font size='4'>ACADEMIC REPORT</font>
			</font></center>";
			for ($i = 0; $i < $NoHeader; $i++) {
				$TitleTable .= "<br />";
			}
		}
		if(is_array($ClassStudentArray))
		{
			foreach($ClassStudentArray as $ClassID => $StudentArray)
			{
				# Get Related Subject Unit Array
				$libformclass->Get_Year_Class_Info($ClassID);
				$libformclass->Get_Year_Info();
				$SubjectUnitArr = Get_CLASS_LEVEL_SUBJECT_UNIT($libformclass->WEBSAMSCode, $ClassID);
				
				if(is_array($StudentArray))
				{
					foreach($StudentArray as $StudentID => $InfoArray)
					{	
						
						# Get Student Attendance and Tardiness Array
						$StudentSummaryCalculateArray = GET_REPORT_ATTENDANCE_AND_TARDINESS($StudentSummaryArray[$StudentID]);
						
						$UserInfoArray = $InfoArray["UserInfo"];

						$ResultArray = $InfoArray["Result"];
						if(empty($ResultArray))
							continue;

						################################################################
						# Generate Signature Table
						$SignatureTable = GENERATE_SIGNATURE_TABLE();

						# Generate Student Info Table
						$StudentInfoTable = GENERATE_STUDENT_INFO_TABLE($UserInfoArray);
						
						# Generate Summary Table
						$SummaryResultArray = GET_SUMMARY_ARRAY($ResultArray, $HighestAverageArray[$ClassID], $StudentSummaryArray[$StudentID], $ColumnArray);
						$ConductResultArray = GET_CONDUCT_ARRAY($ResultArray, $HighestAverageArray[$ClassID], $StudentSummaryArray[$StudentID], $ColumnArray, $StudentAwardArray[$StudentID], $StudentRemarkArray[$StudentID]);
												
						# Generate Attendance and Merit Table
						$AttendanceMeritTable = ($SemColumnExist==1) ? GENERATE_ATTENDANCE_AND_MERIT_TABLE($StudentSummaryArray[$StudentID], $StudentMeritArray[$StudentID]) : "";
						
						# Generate Details Table
						$ResultTable = GENERATE_RESULT_TABLE($SignatureTable, $AttendanceMeritTable, $ReportMarksheetArray[$StudentID], $SubjectTeacherCommentArray[$StudentID]);

						# Generate Misc Table
						$MiscTable = GENERATE_MISC_TABLE($ClassTeacherCommentArray[$StudentID], $StudentID, $libformclass->WEBSAMSCode);

						################################################################

						$rx .= "<table width='726px;' height='100%' border=0 cellpadding=4 cellspacing=1 align='center' style='page-break-after:always'>\n";
						$rx .= "<tr><td valign='top'>\n";
						$rx .= $TitleTable."\n";
						$rx .= $StudentInfoTable."\n";
						$rx .= $ResultTable."\n";
						$rx .= $MiscTable."\n";
						$rx .= "</td></tr>\n";
						$rx .= ($Footer!="") ? "<tr><td valign='bottom' class='small_text'>".str_replace("\n", "<br />", intranet_undo_htmlspecialchars($Footer))."</td></tr>\n" : "";
						$rx .= "</table>\n";
					}
				}
			}
		}
##########################################################################################

echo '<link rel="stylesheet" href="'.$PATH_WRT_ROOT.'file/reportcard/print.css" type="text/css" />';
echo '
<style type="text/css">
.border_top_bold {
	border-top-width: 2px;
	border-top-style: solid;
	border-top-color: #000000;
}
</style>
';
?>


<table border="0" cellpadding="4" width="100%" cellspacing="0" class="print_hide">
<tr><td align="right"><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></span></td></tr>
</table>
<?=$rx?>
<table border="0" cellpadding="4" width="100%" cellspacing="0" class="print_hide">
<tr><td align="right"><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></span></td></tr>
</table>

<?
	include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_footer.php");
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>