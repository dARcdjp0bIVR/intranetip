<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcard.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
//include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

$libreportcard = new libreportcard();
$LibUser = new libuser();

if ($plugin['ReportCard'])
{
	include_once($PATH_WRT_ROOT."includes/libreportcard.php");
	$libreportcard = new libreportcard();
	$CurrentPage = "PageReportGenerationStatistic";

	$MODULE_OBJ = $libreportcard->GET_MODULE_OBJ_ARR();
    if ($libreportcard->hasAccessRight($_SESSION['UserID']))
    {
        $linterface = new interface_html();

############################################################################################################

# get all level
//$SelectedFormArr = $libreportcard->GET_REPORT_FORMS_ID($ReportID);
$SelectedFormArr[] = $ClassLevelID;

# Get Report Marksheet result
$SubjectIDArray = $libreportcard->GET_REPORT_SELECTED_SUBJECT_ID($ReportID);
if (is_array($SubjectIDArray)) $SubjectIDList = implode(",", $SubjectIDArray);

// get ReportColumnID
$ColumnArray = $libreportcard->GET_REPORT_TEMPLATE_COLUMN($ReportID);
$ReportColumnID = -1;
for ($i = 0; $i < sizeof($ColumnArray); $i++) {
	if ($Semester == $ColumnArray[$i]['ColumnTitle']) {
		$ReportColumnID = $ColumnArray[$i]['ReportColumnID'];
	}
}

$SchemeIDArr = $libreportcard->GET_REPORT_SCHEME_ID($ReportID);
$SchemeGradeArr = $libreportcard->GET_SCHEME_GRADE($SchemeIDArr[$SubjectID]);
$FullMark = $libreportcard->GET_SCHEME_FULLMARK($SchemeIDArr[$SubjectID]);

# get student of the particular classlevel only
unset($GradeArray);
unset($MarkArray);
unset($TotalArray);
unset($MeanArray);
unset($HeadCountArray);
for ($CountLevel = 0; $CountLevel < sizeof($SelectedFormArr); $CountLevel++)
{	
	$CurrentClassLevel = $SelectedFormArr[$CountLevel];
	# no need to use $ReportID
	list($ClassStudentArray, $StudentIDArray) = $libreportcard->GET_STUDENT_BY_CLASSLEVEL($ReportID, $CurrentClassLevel);
	
	if(is_array($StudentIDArray))
	{		
		$StudentIDList = implode(",", $StudentIDArray);
		$ReportMarksheetArray = $libreportcard->GET_REPORT_MARKSHEET_SCORES($SubjectIDList, $StudentIDList);
	}
	// get subject, get grading scheme
	if (is_array($ReportMarksheetArray)) {
		
		$Range = $FullMark / 10;
		
		$HighestMarkRange = ($FullMark-$Range).'-'.$FullMark;
		
		
		/*
		$Base = 0;
		for ($TempCount = 0; $TempCount < 10; $TempCount++)
		{
			$TempIndex = "$Base-";
			$TempIndex .= (($Base+$Range) != $FullMark) ? ($Base+$Range-1) : ($Base+$Range);
			$MarkArray[$CurrentClassLevel][$TempIndex] = 0;
			$Base += $Range;
		}
		*/
		
		$Top = $FullMark;
		for ($TempCount = 0; $TempCount < 10; $TempCount++)
		{			
			$TempIndex = $Top-$Range;
			if ($TempCount != 0) $TempIndex++;
			$MarkRange[$TempCount][0] = $TempIndex;
			$TempIndex .= "-$Top";
			$MarkRange[$TempCount][1] = $Top;
			$MarkArray[$CurrentClassLevel][$TempIndex] = 0;
			$MarkIndex[$TempCount] = $TempIndex;
			$Top -= $Range;
			if ($TempCount == 0) $Top -= 1;			
		}
				
		for ($i = 0; $i < sizeof($SchemeGradeArr); $i++)		
		{
			$GradeArray[$CurrentClassLevel][$SchemeGradeArr[$i]] = 0;
		}
		
		while (list ($StudentID, $StudentResultArray) = each ($ReportMarksheetArray)) {
			if ($ReportColumnID == -1) {
				if ($StudentResultArray[$SubjectID]["Overall"]["Grade"] != "")
				{
					$GradeArray[$CurrentClassLevel][$StudentResultArray[$SubjectID]["Overall"]["Grade"]]++;
				}
				if ($StudentResultArray[$SubjectID]["Overall"]["Mark"] != "")
				{					
					$Mark = $StudentResultArray[$SubjectID]["Overall"]["Mark"];

					if ($Mark >= $MarkRange[0][0]) {
						$MarkArray[$CurrentClassLevel][$MarkIndex[0]]++;
					} else if (($Mark >= $MarkRange[1][0]) && ($Mark <= $MarkRange[1][1])) {
						$MarkArray[$CurrentClassLevel][$MarkIndex[1]]++;
					} else if (($Mark >= $MarkRange[2][0]) && ($Mark <= $MarkRange[2][1])) {
						$MarkArray[$CurrentClassLevel][$MarkIndex[2]]++;
					} else if (($Mark >= $MarkRange[3][0]) && ($Mark <= $MarkRange[3][1])) {
						$MarkArray[$CurrentClassLevel][$MarkIndex[3]]++;
					} else if (($Mark >= $MarkRange[4][0]) && ($Mark <= $MarkRange[4][1])) {
						$MarkArray[$CurrentClassLevel][$MarkIndex[4]]++;
					} else if (($Mark >= $MarkRange[5][0]) && ($Mark <= $MarkRange[5][1])) {
						$MarkArray[$CurrentClassLevel][$MarkIndex[5]]++;
					} else if (($Mark >= $MarkRange[6][0]) && ($Mark <= $MarkRange[6][1])) {
						$MarkArray[$CurrentClassLevel][$MarkIndex[6]]++;
					} else if (($Mark >= $MarkRange[7][0]) && ($Mark <= $MarkRange[7][1])) {
						$MarkArray[$CurrentClassLevel][$MarkIndex[7]]++;
					} else if (($Mark >= $MarkRange[8][0]) && ($Mark <= $MarkRange[8][1])) {
						$MarkArray[$CurrentClassLevel][$MarkIndex[8]]++;
					} else {
						$MarkArray[$CurrentClassLevel][$MarkIndex[9]]++;
					}
					/*
					if ($Mark > 89) {
						$MarkArray[$CurrentClassLevel][$HighestMarkRange]++;
					} else if (($Mark > 79) && ($Mark < 90)) {
						$MarkArray[$CurrentClassLevel]['80-89']++;
					} else if (($Mark > 69) && ($Mark < 80)) {
						$MarkArray[$CurrentClassLevel]['70-79']++;
					} else if (($Mark > 59) && ($Mark < 70)) {
						$MarkArray[$CurrentClassLevel]['60-69']++;
					} else if (($Mark > 49) && ($Mark < 60)) {
						$MarkArray[$CurrentClassLevel]['50-59']++;
					} else if (($Mark > 39) && ($Mark < 50)) {
						$MarkArray[$CurrentClassLevel]['40-49']++;
					} else if (($Mark > 29) && ($Mark < 40)) {
						$MarkArray[$CurrentClassLevel]['30-39']++;
					} else if (($Mark > 19) && ($Mark < 30)) {
						$MarkArray[$CurrentClassLevel]['20-29']++;
					} else if (($Mark > 9) && ($Mark < 20)) {
						$MarkArray[$CurrentClassLevel]['10-19']++;
					} else {
						$MarkArray[$CurrentClassLevel]['0 - 9']++;
					}
					*/
					
					$TotalArray[$libreportcard->GET_CLASSNAME_BY_USERID($StudentID)] += $Mark;
					$HeadCountArray[$libreportcard->GET_CLASSNAME_BY_USERID($StudentID)]++;
				}
			} else {
				if ($StudentResultArray[$SubjectID][$ReportColumnID]['Grade'] != "")
				{
					$GradeArray[$CurrentClassLevel][$StudentResultArray[$SubjectID][$ReportColumnID]['Grade']]++;
				}
				if ($StudentResultArray[$SubjectID][$ReportColumnID]['RawMark'] != "")
				{
					$Mark = $StudentResultArray[$SubjectID][$ReportColumnID]['RawMark'];
					/*
					if ($Mark > 89) {
						$MarkArray[$CurrentClassLevel][$HighestMarkRange]++;
					} else if (($Mark > 79) && ($Mark < 90)) {
						$MarkArray[$CurrentClassLevel]['80-89']++;
					} else if (($Mark > 69) && ($Mark < 80)) {
						$MarkArray[$CurrentClassLevel]['70-79']++;
					} else if (($Mark > 59) && ($Mark < 70)) {
						$MarkArray[$CurrentClassLevel]['60-69']++;
					} else if (($Mark > 49) && ($Mark < 60)) {
						$MarkArray[$CurrentClassLevel]['50-59']++;
					} else if (($Mark > 39) && ($Mark < 50)) {
						$MarkArray[$CurrentClassLevel]['40-49']++;
					} else if (($Mark > 29) && ($Mark < 40)) {
						$MarkArray[$CurrentClassLevel]['30-39']++;
					} else if (($Mark > 19) && ($Mark < 30)) {
						$MarkArray[$CurrentClassLevel]['20-29']++;
					} else if (($Mark > 9) && ($Mark < 20)) {
						$MarkArray[$CurrentClassLevel]['10-19']++;
					} else {
						$MarkArray[$CurrentClassLevel]['0 - 9']++;
					}
					*/
					
					if ($Mark >= $MarkRange[0][0]) {
						$MarkArray[$CurrentClassLevel][$MarkIndex[0]]++;
					} else if (($Mark >= $MarkRange[1][0]) && ($Mark <= $MarkRange[1][1])) {
						$MarkArray[$CurrentClassLevel][$MarkIndex[1]]++;
					} else if (($Mark >= $MarkRange[2][0]) && ($Mark <= $MarkRange[2][1])) {
						$MarkArray[$CurrentClassLevel][$MarkIndex[2]]++;
					} else if (($Mark >= $MarkRange[3][0]) && ($Mark <= $MarkRange[3][1])) {
						$MarkArray[$CurrentClassLevel][$MarkIndex[3]]++;
					} else if (($Mark >= $MarkRange[4][0]) && ($Mark <= $MarkRange[4][1])) {
						$MarkArray[$CurrentClassLevel][$MarkIndex[4]]++;
					} else if (($Mark >= $MarkRange[5][0]) && ($Mark <= $MarkRange[5][1])) {
						$MarkArray[$CurrentClassLevel][$MarkIndex[5]]++;
					} else if (($Mark >= $MarkRange[6][0]) && ($Mark <= $MarkRange[6][1])) {
						$MarkArray[$CurrentClassLevel][$MarkIndex[6]]++;
					} else if (($Mark >= $MarkRange[7][0]) && ($Mark <= $MarkRange[7][1])) {
						$MarkArray[$CurrentClassLevel][$MarkIndex[7]]++;
					} else if (($Mark >= $MarkRange[8][0]) && ($Mark <= $MarkRange[8][1])) {
						$MarkArray[$CurrentClassLevel][$MarkIndex[8]]++;
					} else {
						$MarkArray[$CurrentClassLevel][$MarkIndex[9]]++;
					}
										
					$TotalArray[$libreportcard->GET_CLASSNAME_BY_USERID($StudentID)] += $Mark;
					$HeadCountArray[$libreportcard->GET_CLASSNAME_BY_USERID($StudentID)]++;
				}
			}
		}
	}

	if (sizeof($TotalArray) > 0)
	{
		while (list ($ClassName, $Value) = each ($TotalArray))
		{
			$MeanArray[$ClassName] = round($TotalArray[$ClassName] / $HeadCountArray[$ClassName]);
		}
	}
}


$ConfArr['IsHorizontal'] = 1;
$ConfArr['ChartY'] = $eReportCard['headcount'];
$ConfArr['MaxWidth'] = 500;		

if (is_array($GradeArray))
{
	$ConfArr['ChartX'] = $eReportCard['Grade'];
	while (list ($ClassLevel, $FormGrade) = each ($GradeArray)) {
		unset($DataArr);
		if (is_array($FormGrade)) {			
			while (list ($key, $value) = each ($FormGrade)) {
				$DataArr[] = array($key, $value);
			}
			$FormGradeChartArr[$ClassLevel] = $linterface->GEN_CHART($ConfArr, $DataArr);
		}
	}
	
	$FormGradeStr = "<span class='tablesubtitle'>".$eReportCard['GradeStat']."</span><br/>";
	while (list ($ClassLevel, $FormGradeChart) = each ($FormGradeChartArr)) {
		$FormGradeStr .= $FormGradeChart."<br/><br/>";
	}
}


if (is_array($MarkArray))
{
	$ConfArr['ChartX'] = $eReportCard['MarkRange'];
	while (list ($ClassLevel, $FormMark) = each ($MarkArray)) {
		unset($DataArr);
		if (is_array($FormMark)) {			
			while (list ($key, $value) = each ($FormMark)) {
				$DataArr[] = array($key, $value);
			}
			$FormMarkChartArr[$ClassLevel] = $linterface->GEN_CHART($ConfArr, $DataArr);
		}
	}

	$FormMarkStr = "<span class='tablesubtitle'>".$eReportCard['MarkStat']."</span><br/>";
	while (list ($ClassLevel, $FormMarkChart) = each ($FormMarkChartArr)) {
		$FormMarkStr .= $FormMarkChart."<br/><br/>";
	}
}

if (is_array($MeanArray))
{
	$ConfArr['ChartX'] = $eReportCard['ClassName'];
	unset($DataArr);
	while (list ($key, $value) = each ($MeanArray)) {
		$DataArr[] = array($key, $value);
	}
	$MeanChart = $linterface->GEN_CHART($ConfArr, $DataArr);

	$MeanMarkStr = "<span class='tablesubtitle'>".$eReportCard['MeanStat']."</span><br/>";
	$MeanMarkStr .= $MeanChart."<br/><br/>";
}

############################################################################################################

# tag information
$TAGS_OBJ[] = array($eReportCard['Statistic'], "", 0);

$linterface->LAYOUT_START();

$TermObj = new academic_year_term($Semester,$GetAcademicYearInfo=false);
$SemesterName = $TermObj->YearTermNameEN;

?>


		<form name="form1" action="report.php" method="post"onSubmit="return checkform(this);">
		<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">

			<tr>
				<td class="tabletext" align="center">
				<span class="sectiontitle">
				<?
					print $libreportcard->GET_REPORT_TITLE($ReportID)." ";
					//print $Semester.$eReportCard['Semester']."<br/>";
					print $SemesterName."<br/>";
					print $libreportcard->GET_CLASSLEVEL_NAME($ClassLevelID)." ";
					print $libreportcard->GET_SUBJECT_NAME($SubjectID).$eReportCard['ResultStat'];

				?>
				</span>			 
				</td>
			</tr>
			<tr>
				<td align="center" class="tabletext">					
					<?= $FormGradeStr; ?>
				</td>
			</tr>
			<tr>
				<td class="tabletext" height="50"></td>
			</tr>
			<tr>
				<td align="center" class="tabletext">
					<?= $FormMarkStr; ?>
				</td>
			</tr>
			<tr>
				<td class="tabletext" height="50"></td>
			</tr>
			<tr>
				<td align="center" class="tabletext">
					<?= $MeanMarkStr; ?>
				</td>
			</tr>		
			<tr>
				<td>
					<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
						<tr>
							<td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
						</tr>
						<tr>
							<td align="center">
							<?= $linterface->GET_ACTION_BTN($button_back, "button", "self.location='index.php'")?>&nbsp;
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>









<?
        $linterface->LAYOUT_STOP();
		intranet_closedb();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>