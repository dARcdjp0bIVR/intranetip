<?php
$PATH_WRT_ROOT = "../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libreportcard.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
intranet_auth();
intranet_opendb();

############################# FUNCTION ###########################
function GET_RANKING($ParMark, $ParPrevMark, $ParRank, $ParCumRanking)
{
	global $RankingType;

	//settype($ParMark, "float");
	//settype($ParPrevMark, "float");
	$ParMark = round($ParMark, 4);
	$ParPrevMark = round($ParPrevMark, 4);
	$ParCumRanking++;

	if($ParPrevMark=="-1")
	{
		$ParRank = 1;
		$ParCumRanking = 1;
		$DisplayRanking = 1;
	} elseif ($ParMark!=$ParPrevMark)
	{
		if($RankingType==1)
		{
			$DisplayRanking = $ParCumRanking;
			$ParRank = $ParCumRanking;
		} else
		{
			$ParRank++;
			$DisplayRanking = $ParRank;
		}
	} else
	{
		// same mark
		$DisplayRanking = $ParRank;
	}


	return array($ParRank, $ParCumRanking, $DisplayRanking);
}

function UPDATE_SUBJECT_OVERALL_RANKING($ParStudentID, $ParSubjectID, $ParClassRanking, $ParFormRanking, $ParConvertedGrade="")
{
	global $li, $Year, $Semester;

	$GradeUpdate = ($ParConvertedGrade!="") ? ", Grade = '".$ParConvertedGrade."' " : "";

	$sql = "UPDATE RC_MARKSHEET_SCORE SET OrderMeritClass='$ParClassRanking', OrderMeritForm='$ParFormRanking' {$GradeUpdate}, DateModified = now() WHERE IsOverall = 1 AND StudentID = '$ParStudentID' AND SubjectID = '$ParSubjectID' AND Year = '$Year' AND Semester = '$Semester'";
	$li->db_db_query($sql);
}

function UPDATE_SUBJECT_COLUMN_RANKING($ParSubjectID, $ParStudentID, $ParReportColumnID, $ParClassRanking, $ParFormRanking, $ParConvertedGrade="")
{
	global $li, $Year, $Semester;

	$GradeUpdate = ($ParConvertedGrade!="") ? ", Grade = '".$ParConvertedGrade."' " : "";

	$sql = "UPDATE RC_MARKSHEET_SCORE SET OrderMeritClass='$ParClassRanking', OrderMeritForm='$ParFormRanking' {$GradeUpdate}, DateModified = now() WHERE IsOverall = 0 AND StudentID = '$ParStudentID' AND SubjectID = '$ParSubjectID' AND ReportColumnID = '$ParReportColumnID' AND Year = '$Year' AND Semester = '$Semester'";
	$li->db_db_query($sql);
}

function GET_RESULT_SUMMARY($ParResultArray)
{
	//calmethod
	global $ClassSubjectWeightArray, $MarkArray, $GradeArray, $PassFailArray;
	global $libreportcard, $ReportID, $SemesterArray;
	global $CalculationMethodRule;

	foreach($ParResultArray as $ClassLevelID => $ClassArray)
	{
		foreach($ClassArray as $ClassID => $StudentArray)
		{
			foreach($StudentArray as $StudentID => $SubjectArray)
			{

				$StudentIDArray[] = $StudentID;
				$OverallWeight = 0;
				$GPOverallWeight = 0;

				// GT: vertical calculation
				foreach($SubjectArray as $SubjectID => $MarkGrade)
				{
					list($Mark, $Grade, $IsComponent) = $MarkGrade;

					list($Weighting, $SchemeID) = $ClassSubjectWeightArray[$ClassID][$SubjectID];
					if ($Weighting<0 || $Weighting=="")
					{
						$Weighting = 1;
					}
					if($Mark!="NULL" && $Mark!="")
					{
						if($Mark>=0)
						{
							$LevelSubjectSummaryArray[$SubjectID][$ClassLevelID][$StudentID] = $Mark;
							$ClassSubjectSummaryArray[$SubjectID][$ClassID][$StudentID] = $Mark;

							if($IsComponent==0)
							{

								//$ClassSubjectWeightArray[$ClassID][$SubjectID][1] <- SchemeID
								$SubjectFullMark = $libreportcard->GET_SCHEME_FULLMARK($ClassSubjectWeightArray[$ClassID][$SubjectID][1]);

								if ($CalculationMethodRule == 1)
								{
									// assume subject full mark base: 100
									if ($SubjectFullMark > 0)
									{
										$PercentageMark = $Mark / $SubjectFullMark * 100;

										# Calculate Grand Total
										$OverallWeight += $Weighting;
										//$SummaryArray[$StudentID]["GT"] = $SummaryArray[$StudentID]["GT"] + ($Weighting*$Mark) + 0;
										//$SummaryArray[$StudentID]["GT"] += $Mark*$Weighting;
										//$SummaryArray[$StudentID]["GT"] += $PercentageMark * $Weighting / $OverallWeight;
										$SummaryArray[$StudentID]["GT"] += $PercentageMark * $Weighting;
										//if ($StudentID == 1793) debug($PercentageMark, $Weighting, $OverallWeight, $SummaryArray[$StudentID]["GT"], $SubjectID);
										//asdf
									}
								} else {
									$OverallWeight += $SubjectFullMark * $Weighting;
									$SummaryArray[$StudentID]["GT"] += $Mark * $Weighting;
									//if ($StudentID == 1358) debug("OverallWeight: $OverallWeight, GT :".$SummaryArray[$StudentID]["GT"]);
								}

								# Calculate GPA
								$GP_MarkArray = $MarkArray[trim($SchemeID)];
								if(!empty($GP_MarkArray))
								{
									foreach($GP_MarkArray as $LowerLimit => $GP)
									{
										if($Mark>=$LowerLimit)
										{
											$GPOverallWeight = $GPOverallWeight + $Weighting;
											$SummaryArray[$StudentID]["GP"] = $SummaryArray[$StudentID]["GP"] + ($Weighting*$GP) + 0;
											break;
										}
									}
								}
							}
						}
					}
					else if(strcmp($Grade, "/")!=0 && $IsComponent==0)
					{
						# Calculate GPA
						if(!(is_array($PassFailArray[$SchemeID]) && in_array($Grade, $PassFailArray[$SchemeID])))
						{
							$GP_GradeArray = $GradeArray[$SchemeID];
							if(!empty($GP_GradeArray))
							{
								$GP = ($GP_GradeArray[$Grade]!="") ? $GP_GradeArray[$Grade] : 0;

								$GPOverallWeight = $GPOverallWeight + $Weighting;
								$SummaryArray[$StudentID]["GP"] = $SummaryArray[$StudentID]["GP"] + ($Weighting*$GP) + 0;
							}
						}
					}
				}

				###############################################################################################
				#### NEED TO DEBUG START ######################################################################
				###############################################################################################
				/*
				if (($libreportcard->CalculationOrder == "Horizontal")&&($libreportcard->Semester == "FULL")) {
					// calculate horizontally
					// weighting
					$Sql = "
								SELECT
										GrandTotal, Semester
								FROM
										RC_REPORT_RESULT
								WHERE
										StudentID = '".$StudentID."'
									AND ReportID = '".$ReportID."'
									AND Year = '".$libreportcard->Year."'
									AND NOT (Semester = 'FULL')
							";
					$row = $libreportcard->returnArray($Sql, 2);

					for ($tempcount = 0; $tempcount < sizeof($row); $tempcount++)
					{
						$TempGrandTotal[$row[$tempcount][1]] = $row[$tempcount][0];
					}
					######### Get semester weight #####################################################
					$Sql = "
							SELECT
									distinct(ColumnTitle), Weight, IsColumnSum
							FROM
									RC_REPORT_TEMPLATE_COLUMN
							WHERE
									ReportID = '$ReportID'
							ORDER BY
									ReportColumnID
							";
					$row = $libreportcard->returnArray($Sql, 3);

					$TempSemesterCount = 0;
					for ($tempcount = 0; $tempcount < sizeof($row); $tempcount++)
					{
						if (!$row[$tempcount][2]) {
							//$TempWeight[$SemesterArray[$TempSemesterCount]] += $row[$tempcount][1];
							$TempWeight[$row[$tempcount][0]] = $row[$tempcount][1];
						} else {
							//$TempSemesterCount++;
						}
					}
					######### Get semester weight #####################################################

					######### Calculate grand total #####################################################
					$TempCalculatedGT = 0;
					if (is_array($TempGrandTotal)) {
						foreach($TempGrandTotal as $TempSemester => $TempGrandMark)
						{
							$TempCalculatedGT = $TempGrandMark * $TempWeight[$TempSemester] / 100;
						}
					}
					######### Calculate grand total #####################################################
					$SummaryArray[$StudentID]["GT"] = $TempCalculatedGT;
				}
				*/
				###############################################################################################
				#### NEED TO DEBUG END ########################################################################
				###############################################################################################

				$SummaryArray[$StudentID]["GPA"] = ($GPOverallWeight>0) ? $SummaryArray[$StudentID]["GP"]/$GPOverallWeight : "0";
				$SummaryArray[$StudentID]["AM"] = ($OverallWeight>0) ? $SummaryArray[$StudentID]["GT"]/$OverallWeight : "0";
				if ($CalculationMethodRule == 0) {
					$SummaryArray[$StudentID]["AM"] = ($OverallWeight>0) ? $SummaryArray[$StudentID]["GT"]*100/$OverallWeight : "0";
				}

				###############################################################################################
				#### NEED TO DEBUG START ######################################################################
				###############################################################################################
				/*
				if (($libreportcard->CalculationOrder == "Horizontal")&&($libreportcard->Semester == "FULL")) {
					// calculate horizontally
					// weighting
					$Sql = "
								SELECT
										AverageMark, Semester
								FROM
										RC_REPORT_RESULT
								WHERE
										StudentID = '".$StudentID."'
									AND ReportID = '".$ReportID."'
									AND Year = '".$libreportcard->Year."'
									AND NOT (Semester = 'FULL')
							";
					$row = $libreportcard->returnVector($Sql, 3);
					for ($tempcount = 0; $tempcount < sizeof($row); $tempcount++)
					{
						$TempAM[$row[$tempcount][1]] = $row[$tempcount][0];
					}

					######### Calculate Average Mark #####################################################
					$TempCalculatedAM = 0;
					if (is_array($TempAM)) {
						foreach($TempAM as $TempSemester => $TempAverageMark)
						{
							$TempCalculatedAM = $TempAverageMark * $TempWeight[$TempSemester] / 100;
						}
					}
					######### Calculate Average Mark #####################################################
					$SummaryArray[$StudentID]["AM"] = $TempCalculatedAM;
				}
				*/
				###############################################################################################
				#### NEED TO DEBUG END ########################################################################
				###############################################################################################

				$SummaryArray[$StudentID]["ClassID"] = $ClassID;
				if($SummaryArray[$StudentID]["AM"]>0)
				{
					//$LevelSummaryArray[$ClassLevelID][$StudentID] = $SummaryArray[$StudentID]["GT"];
					//$ClassSummaryArray[$ClassID][$StudentID] = $SummaryArray[$StudentID]["GT"];
					$LevelSummaryArray[$ClassLevelID][$StudentID] = $SummaryArray[$StudentID]["AM"];
					$ClassSummaryArray[$ClassID][$StudentID] = $SummaryArray[$StudentID]["AM"];
				}
			}
		}
	}


	if(is_array($ClassSummaryArray))
	{
		# Calculate class rankings
		foreach($ClassSummaryArray as $CID => $ScoreArray)
		{
			if(array_sum($ScoreArray)!=0)
			{
				arsort($ScoreArray);
				$prev_mark = "-1";
				foreach($ScoreArray as $SID => $mark)
				{
					list($ranking, $cum_ranking, $final_ranking) = GET_RANKING($mark, $prev_mark, $ranking, $cum_ranking);
					$prev_mark = $mark;
					$SummaryArray[$SID]["OMC"] = $final_ranking;
				}
			}
		}

		# Calculate level rankings
		foreach($LevelSummaryArray as $LID => $ScoreArray)
		{
			if(array_sum($ScoreArray)!=0)
			{
				arsort($ScoreArray);
				$prev_mark = "-1";
				foreach($ScoreArray as $SID => $mark)
				{
					list($ranking, $cum_ranking, $final_ranking) = GET_RANKING($mark, $prev_mark, $ranking, $cum_ranking);
					$prev_mark = $mark;
					$SummaryArray[$SID]["OMF"] = $ranking;
				}
			}
		}
	}

	return array($StudentIDArray, $SummaryArray, $LevelSubjectSummaryArray, $ClassSubjectSummaryArray);
}

function UPDATE_SUMMARY_RESULT($ParSummaryArray, $ParStudentIDArray, $ParSemester="")
{
	global $li, $ReportID, $Year, $Semester, $ClassStudentCountArray, $FormStudentCountArray, $StudentClassArray;
	global $libreportcard;

	# remove record
	$StudentList = implode(",", $ParStudentIDArray);
	$TargetSemester = ($ParSemester=="") ? $Semester : $ParSemester;
	$sql = "DELETE FROM RC_REPORT_RESULT WHERE StudentID IN ($StudentList) AND ReportID = '".$ReportID."' AND Year = '".$Year."' AND Semester = '".$TargetSemester."'";
	$li->db_db_query($sql);

	foreach($ParSummaryArray as $StudentID => $FinalResultArray)
	{
		$ClassID = trim($FinalResultArray["ClassID"]);
		$ClassPupilNo = $ClassStudentCountArray[$ClassID];
		$TempClassLevelID = $libreportcard->GET_CLASSLEVELID_BY_CLASSID($ClassID);
		$FormPupilNo = $FormStudentCountArray[$TempClassLevelID];

		$GrandTotal = $FinalResultArray["GT"];
		$AverageMark = $FinalResultArray["AM"];
		$GPA = $FinalResultArray["GPA"];
		$OrderMeritClass = $FinalResultArray["OMC"];
		$OrderMeritForm = $FinalResultArray["OMF"];

		list($ClassName, $ClassNumber) = $StudentClassArray[$StudentID];
		//print "$Semester $StudentID $GrandTotal $AverageMark<br>";
//print $Semester."<br>";
		$sql = "INSERT INTO RC_REPORT_RESULT (StudentID, ReportID, GrandTotal, AverageMark, GPA, OrderMeritClass, OrderMeritClassTotal, OrderMeritForm, OrderMeritFormTotal, Year, Semester, ClassName, ClassNumber, DateInput, DateModified) VALUES ('".$StudentID."', '".$ReportID."', '".$GrandTotal."', '".$AverageMark."', '".$GPA."', '".$OrderMeritClass."', '".$ClassPupilNo."', '".$OrderMeritForm."', '".$FormPupilNo."', '".$Year."', '".$TargetSemester."', '".$ClassName."', '".$ClassNumber."', now(), now())";
		$success = $li->db_db_query($sql);
	}

	return $success;
}

########################################################################

$li = new libdb();
$libreportcard = new libreportcard();
$CalculationMethodRule = $libreportcard->CalculationMethodRule;
if ($ReportCardTemplate == "7") $CalculationMethodRule = 0;

# Get ranking setting
# 0 - duplicated AM have same ranking, the next ranking will be continuous
# 1 - duplicated AM have same ranking, the next ranking will not be continuous (ranking increased even AM duplicated)
$sql = "SELECT DisplaySettings FROM RC_REPORT_TEMPLATE WHERE ReportID = '$ReportID'";
$row = $li->returnVector($sql);
$SettingArray = unserialize($row[0]);
$RankingType = $SettingArray["RankingType"];


$Year = $libreportcard->Year;
$Semester = $libreportcard->Semester;

# Get the total no. of pupils in Class/Form
$ClassStudentCountArray = $libreportcard->GET_CLASS_STUDENT_COUNT();
$FormStudentCountArray = $libreportcard->GET_FORM_STUDENT_COUNT();

# get subject grading array
$ReportSchemeIDArray = $libreportcard->GET_REPORT_SCHEME_ID($ReportID);

# GAP = (Grade Point*Credits Attempted)/Total Credits
list($MarkArray, $GradeArray, $PassFailArray) = $libreportcard->GET_ALL_GRADEMARK();

######################################################################################
################ Retrieve weighted mark and calcualte subject column ranking #########
$sql = "SELECT
			d.StudentID,
			b.ClassLevelID,
			b.ClassID,
			d.SubjectID,
			d.ReportColumnID,
			d.IsWeighted,
			d.Mark
		FROM
			RC_REPORT_TEMPLATE_FORM as a
			LEFT JOIN INTRANET_CLASS as b ON a.ClassLevelID = b.ClassLevelID
			LEFT JOIN INTRANET_USER as c ON b.ClassName = c.ClassName
			LEFT JOIN RC_MARKSHEET_SCORE as d ON c.UserID = d.StudentID
		WHERE
			a.ReportID = '$ReportID'
			AND c.RecordType = 2
			AND c.RecordStatus = 1
			AND d.IsOverall = 0
			AND d.Mark IS NOT NULL
			AND d.Mark >= 0
			AND d.Year = '$Year'
			AND d.Semester = '$Semester'
		ORDER BY
			b.ClassID,
			b.ClassName
		";
$row = $li->returnArray($sql, 7);

$AllGradeMarkArray = array();
for($i=0; $i<sizeof($row); $i++)
{
	list($student_id, $class_level_id, $class_id, $subject_id, $report_column_id, $is_weighted, $mark) = $row[$i];

	if($is_weighted==1)
	{
		$ClassColumnSubjectSummaryArray[$report_column_id][$subject_id][$class_id][$student_id] = $mark;
		$LevelColumnSubjectSummaryArray[$report_column_id][$subject_id][$class_level_id][$student_id] = $mark;
	}
	else
		$SubjectColumnResultArray[$report_column_id][$subject_id][$student_id] = $mark;

	if(empty($AllGradeMarkArray[$subject_id]))
	{
		$scheme_id = $ReportSchemeIDArray[$subject_id];
		$AllGradeMarkArray[$subject_id] = $libreportcard->GET_GRADEMARK_BY_SCHEMEID($scheme_id);
	}
}


# Calculate class ranking of student in corresponding subject and column
if(is_array($ClassColumnSubjectSummaryArray))
{
	foreach($ClassColumnSubjectSummaryArray as $ReportColumnID => $subject_array)
	{
		if(is_array($subject_array))
		{
			foreach($subject_array as $SubjectID => $r_class_array)
			{
				if(is_array($r_class_array))
				{
					# Get
					foreach($r_class_array as $r_class_id => $r_mark_array)
					{
						if(is_array($r_mark_array))
						{
							arsort($r_mark_array);
							$prev_mark = "-1";
							foreach($r_mark_array as $student_id => $mark)
							{
								list($ranking, $cum_ranking, $final_ranking) = GET_RANKING($mark, $prev_mark, $ranking, $cum_ranking);
								$prev_mark = $mark;

								$ClassRankingArray[$SubjectID][$student_id][$ReportColumnID] = $final_ranking;
							}
						}
					}
				}
			}
		}
	}
}

# Calculate class ranking of student in corresponding subject and column
if(is_array($LevelColumnSubjectSummaryArray))
{
	foreach($LevelColumnSubjectSummaryArray as $ReportColumnID => $subject_array)
	{
		if(is_array($subject_array))
		{
			foreach($subject_array as $SubjectID => $r_level_array)
			{
				$grade_mark_array = $AllGradeMarkArray[$SubjectID];
				$LowestGrade = $grade_mark_array[sizeof($grade_mark_array)-1][1];
				if(is_array($r_level_array))
				{
					foreach($r_level_array as $r_level_id => $r_mark_array)
					{
						if(is_array($r_mark_array))
						{
							arsort($r_mark_array);
							$prev_mark = "-1";
							foreach($r_mark_array as $student_id => $mark)
							{
								$raw_mark = $SubjectColumnResultArray[$ReportColumnID][$SubjectID][$student_id];
								$ConvertedGrade = $libreportcard->GET_CONVERTED_GRADE($grade_mark_array, $LowestGrade, $raw_mark);

								list($ranking, $cum_ranking, $final_ranking) = GET_RANKING($mark, $prev_mark, $ranking, $cum_ranking);
								$prev_mark = $mark;

								UPDATE_SUBJECT_COLUMN_RANKING($SubjectID, $student_id, $ReportColumnID, $ClassRankingArray[$SubjectID][$student_id][$ReportColumnID], $final_ranking, $ConvertedGrade);
							}
						}
					}
				}
			}
		}
	}
}

################################################
# Retrieve Semester Column in Full Report that have no result
# ????
if ($Semester=="FULL" || $ReportCardTemplate=="4")
{
	$SemesterArray = $libreportcard->GET_SEMESTER_ARRAY();
	$SemesterList = "'".implode("','", $SemesterArray)."'";
	$sql = "SELECT DISTINCT
				a.ReportColumnID,
				a.ColumnTitle
			FROM
				RC_REPORT_TEMPLATE_COLUMN as a
				LEFT JOIN RC_REPORT_TEMPLATE_CELL as b ON a.ReportColumnID = b.ReportColumnID
			WHERE
				a.ReportID = '".$ReportID."'
				AND a.ColumnTitle IN ({$SemesterList})
			";
	$row_semester = $libreportcard->returnArray($sql, 2);

	if ($ReportCardTemplate=="4" && sizeof($row_semester)<=0)
	{
		$sql = "SELECT DISTINCT
					a.ReportColumnID,
					a.ColumnTitle
				FROM
					RC_REPORT_TEMPLATE_COLUMN as a
					LEFT JOIN RC_REPORT_TEMPLATE_CELL as b ON a.ReportColumnID = b.ReportColumnID
				WHERE
					a.ReportID = '".$ReportID."'
				";
		$row_semester = $libreportcard->returnArray($sql, 2);
		$SpecialForTungNam = true;
	}
	if ($ReportCardTemplate=="4") $SpecialForTungNam = true;

	for($k=0; $k<sizeof($row_semester); $k++)
	{
		list($t_rcid, $t_semester) = $row_semester[$k];
		$IsExist = $libreportcard->CHECK_RESULT_EXIST($Year, $t_semester);
		# for ROSA, allow all semesters
		if ($ReportCardTemplate=="2")
		{
			$IsExist = true;
		}

		if ($IsExist || $SpecialForTungNam)
		{
			$sql = "SELECT
						a.StudentID,
						b.ClassName,
						b.ClassNumber,
						a.SubjectID,
						a.Mark,
						a.Grade,
						a.TeacherID,
						a.OrderMeritClass,
						a.OrderMeritForm,
						c.ClassID,
						c.ClassLevelID,
						if(d.CMP_CODEID IS NULL, 0, 1) as IsComponent
					FROM
						RC_MARKSHEET_SCORE as a
						LEFT JOIN INTRANET_USER as b ON a.StudentID = b.UserID
						LEFT JOIN INTRANET_CLASS as c ON b.ClassName = c.ClassName
						LEFT JOIN {$eclass_db}.ASSESSMENT_SUBJECT as d ON a.SubjectID = d.RecordID
					WHERE
						a.ReportColumnID = '".$t_rcid."'
						AND b.RecordType = 2 
						AND b.RecordStatus = 1 
						AND a.Year = '".$Year."'
						AND a.Semester = '".$Semester."'
						AND a.IsWeighted = 0
					ORDER BY
						c.ClassLevelID,
						c.ClassID
					";
					// ??  AND a.Semester = '".$Semester."' removed in where
			$row = $libreportcard->returnArray($sql, 12);

			for($i=0; $i<sizeof($row); $i++)
			{
				list($r_student_id, $r_class_name, $r_class_number, $r_subject_id, $r_mark, $r_grade, $r_teacher_id, $r_rank_class, $r_rank_form, $r_class_id, $r_classlevel_id, $r_IsComponent) = $row[$i];

				if($r_mark>0)
				{
					# Get grade mark array
					$grade_mark_array = $AllGradeMarkArray[$r_subject_id];
					if(!empty($grade_mark_array))
					{
						$LowestGrade = $grade_mark_array[sizeof($grade_mark_array)-1][1];
						for($m=0; $m<sizeof($grade_mark_array); $m++)
						{
							list($m_score, $m_grade) = $grade_mark_array[$m];
							if($r_mark>=$m_score)
							{
								$r_grade = $m_grade;
								break;
							}
						}
						if($r_grade=="")
							$r_grade = $LowestGrade;
					}
				}
				else if($r_grade=="")
					$r_grade = "/";

				# remove existing records first
				$sql = "DELETE FROM RC_MARKSHEET_SCORE WHERE StudentID = '".$r_student_id."' AND SubjectID = '".$r_subject_id."' AND Year = '".$Year."' AND Semester = '".$t_semester."' AND IsOverall = 1";
				$libreportcard->db_db_query($sql);

				$sql = "INSERT INTO RC_MARKSHEET_SCORE(StudentID, ClassName, ClassNumber, SubjectID, Mark, Grade, IsOverall, TeacherID, Year, Semester, OrderMeritClass, OrderMeritForm, DateInput, DateModified) VALUES('".$r_student_id."', '".$r_class_name."', '".$r_class_number."', '".$r_subject_id."', '".$r_mark."', '".$r_grade."', 1, '".$r_teacher_id."', '".$Year."', '".$t_semester."', '".$r_rank_class."', '".$r_rank_form."', now(), now())";
				$libreportcard->db_db_query($sql);

				//$r_mark>0 &&
				if($r_IsComponent==0)
				{
					$NoResultSemesterArray[$t_semester][$r_classlevel_id][$r_class_id][$r_student_id][$r_subject_id] = array($r_mark, $r_grade, $r_IsComponent);

					$StudentClassArray[$r_student_id] = array($r_class_name, $r_class_number);
				}
			}
		}
	}
}
################################################

######################################################################################

$SemesterArray = $libreportcard->GET_SEMESTER_ARRAY();

$TempSql = "
			SELECT
					SubjectID
			FROM
					RC_REPORT_TEMPLATE_SUBJECT
			WHERE
					ReportID = '$ReportID'
		";
$TempRow = $li->returnVector($TempSql);
$TempSubjectIDStr = implode($TempRow, ", ");

# Retrieve Student and their overall result
$sql = "SELECT
			b.ClassLevelID,
			b.ClassID,
			c.UserID,
			c.ClassName,
			c.ClassNumber,
			d.SubjectID,
			if(d.Mark IS NULL, 'NULL', d.Mark),
			if(d.Grade IS NULL, 'NULL', d.Grade),
			if(e.CMP_CODEID IS NULL, 0, 1) as IsComponent
		FROM
			RC_REPORT_TEMPLATE_FORM as a
			LEFT JOIN INTRANET_CLASS as b ON a.ClassLevelID = b.ClassLevelID
			LEFT JOIN INTRANET_USER as c ON b.ClassName = c.ClassName
			LEFT JOIN RC_MARKSHEET_SCORE as d ON c.UserID = d.StudentID
			LEFT JOIN {$eclass_db}.ASSESSMENT_SUBJECT as e ON d.SubjectID = e.RecordID
		WHERE
			a.ReportID = '$ReportID'
			AND c.RecordType = 2
			AND c.RecordStatus = 1
			AND d.IsOverall = 1
			AND d.Year = '$Year'
			AND d.Semester = '$Semester'
			AND d.SubjectID IN ($TempSubjectIDStr)
		ORDER BY
			b.ClassID,
			b.ClassName,
			c.UserID
		";
$row = $li->returnArray($sql, 9);

$AllSubjectArray = array();
for($i=0; $i<sizeof($row); $i++)
{
	list($class_level_id, $class_id, $student_id, $class_name, $class_number, $subject_id, $mark, $grade, $IsComponent) = $row[$i];
	//if ($student_id == 2022) debug($mark, $grade, $IsComponent);
	//asdf
	$ResultArray[$class_level_id][$class_id][$student_id][$subject_id] = array($mark, $grade, $IsComponent);
	$ClassSubjectArray[$class_id][$subject_id] = 1;

	if(empty($StudentClassArray[$student_id]))
		$StudentClassArray[$student_id] = array($class_name, $class_number);

	if(!in_array($subject_id, $AllSubjectArray))
		$AllSubjectArray[] = $subject_id;
}

# Get weighting by class and subject
if(is_array($ClassSubjectArray))
{
	foreach($ClassSubjectArray as $ClassID => $SubjectArray)
	{
		if(is_array($SubjectArray))
		{
			$TempArray = array_keys($SubjectArray);
			$SubjectList = implode(",", $TempArray);
		}
		$ClassSubjectWeightArray[$ClassID] = $libreportcard->GET_WEIGHTING_BY_SUBJECT_CLASS($SubjectList, $ClassID);
	}
}

# save the overall result of semester that have no result before
//debug_r($NoResultSemesterArray);
if(is_array($NoResultSemesterArray))
{
	foreach($NoResultSemesterArray as $NoResultSemester => $NoResultArray)
	{
		list($NoResultStudentIDArray, $NoResultSummaryArray) = GET_RESULT_SUMMARY($NoResultArray);
		UPDATE_SUMMARY_RESULT($NoResultSummaryArray, $NoResultStudentIDArray, $NoResultSemester);
	}
}
//debug_r($ResultArray);
if(is_array($ResultArray))
{
	list($StudentIDArray, $SummaryArray, $LevelSubjectSummaryArray, $ClassSubjectSummaryArray) = GET_RESULT_SUMMARY($ResultArray);

	if(is_array($SummaryArray))
	{
		$success = UPDATE_SUMMARY_RESULT($SummaryArray, $StudentIDArray);

		# Update last generate in the RC_REPORT_TEMPLATE
		$sql = "UPDATE RC_REPORT_TEMPLATE SET LastGenerated = now() WHERE ReportID = '$ReportID'";
		$li->db_db_query($sql);
	}

	####################################################################################################
	################################ Calculate the overall result position of subjects ################

	for($i=0; $i<sizeof($AllSubjectArray); $i++)
	{
		$s_subject = $AllSubjectArray[$i];
		$s_class_array = $ClassSubjectSummaryArray[$s_subject];
		$s_form_array = $LevelSubjectSummaryArray[$s_subject];

		$grade_mark_array = $AllGradeMarkArray[$s_subject];
		$LowestGrade = $grade_mark_array[sizeof($grade_mark_array)-1][1];

		# Calculate class rankings
		if(is_array($s_class_array))
		{
			foreach($s_class_array as $s_class_id => $s_class_mark_array)
			{
				if(is_array($s_class_mark_array))
				{
					arsort($s_class_mark_array);
					$prev_mark = "-1";
					foreach($s_class_mark_array as $s_student_id => $mark)
					{
						list($ranking, $cum_ranking, $final_ranking) = GET_RANKING($mark, $prev_mark, $ranking, $cum_ranking);
						$prev_mark = $mark;

						$ClassOverallRankingArray[$s_subject][$s_student_id] = $final_ranking;
					}
				}
			}
		}

		# Calculate level rankings
		if(is_array($s_form_array))
		{
			foreach($s_form_array as $s_level_id => $s_level_mark_array)
			{
				if(is_array($s_level_mark_array))
				{
					arsort($s_level_mark_array);
					$prev_mark = "-1";
					foreach($s_level_mark_array as $s_student_id => $mark)
					{
						$ConvertedGrade = $libreportcard->GET_CONVERTED_GRADE($grade_mark_array, $LowestGrade, $mark);

						list($ranking, $cum_ranking, $final_ranking) = GET_RANKING($mark, $prev_mark, $ranking, $cum_ranking);
						$prev_mark = $mark;

						UPDATE_SUBJECT_OVERALL_RANKING($s_student_id, $s_subject, $ClassOverallRankingArray[$s_subject][$s_student_id], $final_ranking, $ConvertedGrade);
					}
				}
			}
		}
	}
}

intranet_closedb();
$Result = ($success==1) ? "report_generate" : "report_generate_failed";
header("Location: index.php?Result=$Result");
?>