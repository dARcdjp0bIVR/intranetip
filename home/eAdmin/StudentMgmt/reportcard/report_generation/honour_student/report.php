<?php
$PATH_WRT_ROOT = "../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcard.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

$libreportcard = new libreportcard();
$LibUser = new libuser();
$lf = new libfilesystem();

if ($plugin['ReportCard'])
{
	include_once($PATH_WRT_ROOT."includes/libreportcard.php");
	$libreportcard = new libreportcard();
	$CurrentPage = "PageReportGenerationHonourStudent";

	$MODULE_OBJ = $libreportcard->GET_MODULE_OBJ_ARR();
    if ($libreportcard->hasAccessRight($_SESSION['UserID']))
    {
        $linterface = new interface_html();

############################################################################################################
############################################################################################################
############################################################################################################

function GET_CSV_FILE_CONTENT($ParFile, $ParDefaultHeaderArray)
{
	global $libreportcard, $lf, $StudentRegNoArray, $Semester;
	
	$ClassArr = $libreportcard->GET_CLASSES();
	$TargetArray[] = $Semester;
	for($i=0; $i<sizeof($TargetArray); $i++)
	{
		$t_semester = trim($TargetArray[$i]);
		$sem_number = GET_SEMESTER_NUMBER($t_semester);
		if($sem_number>=0)
		{
			for ($j = 0; $j < sizeof($ClassArr); $j++)
			{
				$TargetFile = $ParFile."/".trim($libreportcard->Year)."_".$sem_number."_".$ClassArr[$j].".csv";
				if(file_exists($TargetFile))
				{
					$data = $lf->file_read_csv($TargetFile);
					if(!empty($data))
					{
						$header_row = array_shift($data);
						$wrong_format = CHECK_CSV_FILE_FORMAT($header_row, $ParDefaultHeaderArray);
						if(!$wrong_format)
						{
							for($k=0; $k<sizeof($data); $k++)
							{
								$reg_no = array_shift($data[$k]);								
								$reg_no = trim(str_replace("#", "", $reg_no));	
								$student_id = trim($StudentRegNoArray[$reg_no]);
								if($student_id!="")
								{
									if(sizeof($data[$k])>1) {
										$ReturnArray[$student_id][$t_semester] = $data[$k];
									}
									else {
										$ReturnArray[$student_id][$t_semester] .= "<li>".$data[$k][0]."</li>";
									}
								}
							}
						}
					}
				}
			}
		}
	}

	return $ReturnArray;
}

function GET_SEMESTER_NUMBER($ParSemester)
{
	global $libreportcard, $SemesterArray;

	$sem_number = "-1";
	if(trim($ParSemester)=="FULL")
	{
		$sem_number = 0;
	} else
	{
		for($i=0; $i<sizeof($SemesterArray); $i++)
		{
			if(trim($ParSemester)==trim($SemesterArray[$i]))
			{
				$sem_number = $i+1;
				break;
			}
		}
	}
	return $sem_number;
}

function CHECK_CSV_FILE_FORMAT($ParHeaderArray, $ParDefaultHeaderArray)
{
	# Check Title Row
	$format_wrong = false;

	for ($i=0; $i<sizeof($ParDefaultHeaderArray); $i++)
	{
		if ($ParHeaderArray[$i]!=$ParDefaultHeaderArray[$i])
		{
			$format_wrong = true;
			break;
		}
	}

	return $format_wrong;
}

############################################################################################################
############################################################################################################
############################################################################################################

unset($QualifiedStudentList);
unset($QualifiedStudentListSWAP);

# get all level
$SelectedFormArr = $libreportcard->GET_REPORT_FORMS_ID($ReportID);

# get student reg no
$ClassLevels = implode(",", $SelectedFormArr);
$StudentRegNoArray = $libreportcard->GET_STUDENT_REGNO_ARRAY($ClassLevels);

# get student of the particular classlevel only
# Check the qualification of Academic Result
for ($CountLevel = 0; $CountLevel < sizeof($SelectedFormArr); $CountLevel++)
{	
	$CurrentClassLevel = $SelectedFormArr[$CountLevel];
	
	# no need to use $ReportID
	list($ClassStudentArray, $StudentIDArray) = $libreportcard->GET_STUDENT_BY_CLASSLEVEL($ReportID, $CurrentClassLevel);
	
	if(is_array($ClassStudentArray))
	{
		foreach($ClassStudentArray as $ClassID => $StudentArray)
		{
			if(is_array($StudentArray))
			{
				foreach($StudentArray as $StudentID => $InfoArray)
				{
					$UserInfoArray = $InfoArray["UserInfo"];
					$ResultArray = $InfoArray["Result"];
					if(empty($ResultArray))
						continue;
								
					if(!empty($ResultArray[$Semester]))
					{
						list($GT, $AM, $GPA, $OMC, $OMCT, $OMF, $OMFT) = $ResultArray[$Semester];

						if ($JudgeBy == "total") {
							if ($GT > $ResultOver) {
								$QualifiedStudentList[] = $StudentID;
							}
						} else if ($JudgeBy == "gpa") {
							if ($GPA > $ResultOver) {
								$QualifiedStudentList[] = $StudentID;
							}
						} else {
							if ($AM > $ResultOver) {
								$QualifiedStudentList[] = $StudentID;
							}
						}
					}
				}
			}
		}
	}
}

$SemesterArray = $libreportcard->GET_SEMESTER_ARRAY();

# Number of Absent Less than 
# Number of Late Less than
if (sizeof($QualifiedStudentList) > 0) {
	
	$QualifiedStudentListSWAP = $QualifiedStudentList;
	unset($QualifiedStudentList);
	
	#####################################################################
	# get summary data from csv file
	
	$SummaryHeader = array("REGNO", "ABSENT", "LATE", "ABSENT_WO_LEAVE", "CONDUCT");
	$TargetSummaryFile = $intranet_root."/file/reportcard/summary";
	$StudentSummaryArray = GET_CSV_FILE_CONTENT($TargetSummaryFile, $SummaryHeader);
	#####################################################################

	if (sizeof($StudentSummaryArray) > 0)
	{
		while (list ($StudentID, $SemesterRecord) = each ($StudentSummaryArray)) {
			# $SemesterRecord[$Semester][0] : absent
			# $SemesterRecord[$Semester][1] : late
			# $SemesterRecord[$Semester][3] : conduct
			if (($SemesterRecord[$Semester][0] < $AbsentSelect)
				&& ($SemesterRecord[$Semester][1] < $LateSelect)
				&& (in_array($StudentID, $QualifiedStudentListSWAP)))
			{
				for ($countGrade = 0; $countGrade < sizeof($Grade); $countGrade++) {
					if ($Grade[$countGrade] == $SemesterRecord[$Semester][3])
						$QualifiedStudentList[] = $StudentID;
				}
			}
		}
	}
}

# Number of Merit More than 
# Number of Demerit Less than 
if (sizeof($QualifiedStudentList) > 0) {
	
	$QualifiedStudentListSWAP = $QualifiedStudentList;
	unset($QualifiedStudentList);
	
	#####################################################################
	# get merit data from csv file

	$MeritHeader = ($ReportCardTemplate==2) ? array("REGNO", "MERIT", "DEMERIT") : array("REGNO", "TITLE","MINORCREDIT","MAJORCREDIT","MINORFAULT","MAJORFAULT");
	$TargetMeritFile = $intranet_root."/file/reportcard/merit";
	$StudentMeritArray = GET_CSV_FILE_CONTENT($TargetMeritFile, $MeritHeader);
	#####################################################################
	//debug_r($StudentMeritArray);
	if (sizeof($StudentMeritArray) > 0)
	{
		while (list ($StudentID, $MeritRecord) = each ($StudentMeritArray)) {
			# $SemesterRecord[$Semester][0] : merit
			# $SemesterRecord[$Semester][1] : demerit
			if (($MeritRecord[$Semester][0] >= $MeritSelect)
				&& ($MeritRecord[$Semester][1] < $DemeritSelect)
				&& (in_array($StudentID, $QualifiedStudentListSWAP)))
			{
				$QualifiedStudentList[] = $StudentID;
			}
		}	
	}
}
	

// output student list
$HonourStudentList = "<table width='60%' border='0' cellpadding='2' cellspacing='0'>";
	
$HonourStudentList .= "<tr class='tablebluetop tabletoplink'>";
$HonourStudentList .= "<td>".$i_UserStudentName."</td>";
$HonourStudentList .= "</tr>";

for ($i = 0; $i < sizeof($QualifiedStudentList); $i++)
{
	$HonourStudentList .= "<tr class='tabletext tablebluerow".($i%2+1)."'>";
	$HonourStudentList .= "<td>".$LibUser->getNameWithClassNumber($QualifiedStudentList[$i])."</td>";
	$HonourStudentList .= "</tr>";
}

if (sizeof($QualifiedStudentList) == 0)
{
	$HonourStudentList .= "<tr class='tabletext tablebluerow".($i%2+1)."'>";
	$HonourStudentList .= "<td>".$eReportCard['NoRecord']."</td>";
	$HonourStudentList .= "</tr>";
}
	
$HonourStudentList .= "</table>";



############################################################################################################

# tag information
$TAGS_OBJ[] = array($eReportCard['HonourStudent'], "", 0);

$linterface->LAYOUT_START();

?>


		<form name="form1" action="report.php" method="post"onSubmit="return checkform(this);">
		<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
			<tr>
				<td align="center" class="tabletext">					
					<br><?= $HonourStudentList?>
				</td>
			</tr>
			<tr>
				<td class="tabletext" height="50"></td>
			</tr>
			<tr>
				<td>
					<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
						<tr>
							<td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
						</tr>
						<tr>
							<td align="center">
							<?= $linterface->GET_ACTION_BTN($button_back, "button", "self.location='index.php'")?>&nbsp;
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>




<?
        $linterface->LAYOUT_STOP();
		intranet_closedb();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>