<?php
$PATH_WRT_ROOT = "../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcard.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
//include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

if ($plugin['ReportCard'])
{
	$lreportcard = new libreportcard();
	$CurrentPage = "ReportGeneration";
	$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
    if ($lreportcard->hasAccessRight())
    {
        $linterface = new interface_html();
############################################################################################################
		# Get Reports
		$sql = "SELECT
					a.ReportID,
					REPLACE(a.ReportTitle, '::', '<br />'),
					a.Description,
					date_format(a.LastGenerated, '%Y-%m-%d %H:%i'),
					IssueDate
				FROM
					RC_REPORT_TEMPLATE as a
				WHERE
					a.RecordStatus = 1
				ORDER BY
					a.ReportTitle
				";
		$ReportArray = $lreportcard->returnArray($sql, 4);
		
		//$PreviewFileName = (file_exists($PATH_WRT_ROOT."home/admin/reportcard/report_builder/preview".$ReportCardTemplate.".php")) ? "preview".$ReportCardTemplate.".php" : "preview.php";
		$PreviewFileName = (file_exists($PATH_WRT_ROOT."home/eAdmin/StudentMgmt/reportcard/report_builder/preview".$ReportCardTemplate.".php")) ? "preview".$ReportCardTemplate.".php" : "preview.php";
		if(!empty($ReportArray))
		{
			$rx = "";
			for($i=0; $i<sizeof($ReportArray); $i++)
			{
				$css = ($i%2==0) ? 1 : 2;
				list($ReportID, $ReportTitle, $Description, $LastGenerated, $IssueDate) = $ReportArray[$i];

				# Check Result Exist
				$ResultCount = $lreportcard->GET_REPORT_RESULT_COUNT($ReportID);
				$PrintBtn = ($ResultCount>0) ? $linterface->GET_SMALL_BTN($button_print, "button", "javascript:jGO_PRINT_MENU(".$ReportID.")") : "--";

				$rx .= "<tr class='tablegreenrow".$css."'>";
				$rx .= "<td class='tabletext'>".$ReportTitle."</td>";
				$rx .= "<td class='tabletext'><a class='tablelink' href=\"javascript:newWindow('../report_builder/".$PreviewFileName."?ReportID=$ReportID', 10)\" >
				<img src=\"$image_path/$LAYOUT_SKIN/icon_view.gif\" border=0 align='absmiddle'>$button_preview</a></td>";
				$rx .= "<td class='tabletext'>".$Description."&nbsp;</td>";
				$rx .= "<td class='tabletext'>".$linterface->GET_SMALL_BTN($eReportCard['Generate'], "button", "javascript:self.location.href='generate.php?ReportID=$ReportID'")."</td>";
				if ($IssueDate == "") $IssueDate = date('Y-m-d');
				$rx .= "<td class='tabletext'><input type='text' class='textboxtext' name='IssueDate{$ReportID}' id='IssueDate{$ReportID}' value='$IssueDate' maxlength='10'><br>".$linterface->GET_SMALL_BTN($eReportCard['SetIssueDate'], "button", "javascript:if (check_date(document.getElementById('IssueDate$ReportID'), '".$eReportCard['DateInvalid']."')) self.location.href='issuedate_update.php?ReportID=$ReportID&IssueDate='+document.getElementById('IssueDate$ReportID').value")."</td>";
				$rx .= "<td class='tabletext'>".$PrintBtn."</td>";
				$rx .= "<td class='tabletext'>".($LastGenerated==""?"--":$LastGenerated)."</td>";
				$rx .= "</tr>";
			}
		}
		else {
			$rx .= "<tr><td class='tabletext' colspan='6' align='center'>".$i_no_record_exists_msg."</td></tr>";
		}
############################################################################################################

# tag information
$TAGS_OBJ[] = array($eReportCard['ReportGeneration'], "", 0);

$linterface->LAYOUT_START();

?>
<br/>
<script language="javascript">
<!--
function jGO_PRINT_MENU(jReportID)
{
	newWindow('print_menu.php?ReportID='+jReportID, 15);
}
//-->
</script>

<form name="form1" method="get">
<table width="90%" border="0" cellpadding="3" cellspacing="0">
<tr><td align="right" colspan="6"><?= $linterface->GET_SYS_MSG($Result); ?></td></tr>
<?=$RemoveAllBtn?>
<tr class='tablegreentop'>
<td class="tableTitle" width="20%"><span class="tabletoplink"><?=$eReportCard['Report']?></span></td>
<td class="tableTitle" width="15%"><span class="tabletoplink">&nbsp;</span></td>
<td class="tableTitle" width="25%"><span class="tabletoplink"><?=$i_general_description?></span></td>
<td class="tableTitle"><span class="tabletoplink">&nbsp;</span></td>
<td class="tableTitle"><span class="tabletoplink">&nbsp;</span></td>
<td class="tableTitle"><span class="tabletoplink">&nbsp;</span></td>
<td class="tableTitle" width="19%"><span class="tabletoplink"><?=$eReportCard['LastGenerated']?></span></td>
</tr>
<?=$rx?>
</table>

<input type="hidden" name="ReportID" />
</form>
<?

        $linterface->LAYOUT_STOP();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>