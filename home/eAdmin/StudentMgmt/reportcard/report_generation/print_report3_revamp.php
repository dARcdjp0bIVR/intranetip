<?php
$PATH_WRT_ROOT = "../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_header.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

echo "<script>if(typeof(opener)!='undefined') { opener.close(); }</script>";

if ($plugin['ReportCard'])
{
	include_once($PATH_WRT_ROOT."includes/libreportcard.php");
	include_once($PATH_WRT_ROOT."home/admin/reportcard/report_generation/print_report_functions3_revamp.php");

	$libreportcard = new libreportcard();	
	$CurrentPage = "ReportGeneration";
	if ($libreportcard->hasAccessRight())
    {
	    $linterface = new interface_html();
#######################################################################################################

		# get current Year and Semester
		$Year = $libreportcard->Year;
		$Semester = $libreportcard->Semester;

		# Get Report Info
		$ReportInfo = $libreportcard->GET_REPORT_TEMPLATE($ReportID);

		list($ReportTitle, $ReportType, $Description, $ShowFullMark, $Settings, $ReportStatus, $ShowOverallPosition, $OverallPositionRange, $HideNotEnrolled, $Footer, $NoHeader, $LineHeight, $SignatureWidth) = $ReportInfo;
		$SettingArray = unserialize($Settings);
		$BilingualReportTitle = $SettingArray["BilingualReportTitle"];

		# define the name of the variable array by checking the bilingual setting
		$LangArray = ($BilingualReportTitle==1) ? $eReportCardBilingual : $eReportCard;

		# Get Class Level(s) of the report
		if($ClassLevelID!="")
			$SelectedFormArr = array($ClassLevelID);
		else
			$SelectedFormArr = $libreportcard->GET_REPORT_TEMPLATE_FORM($ReportID);

		# Get Class Student(s)
		$LevelList = implode(",", $SelectedFormArr);
		
		$TargetStudentList = (is_array($TargetStudentID)) ? implode(",", $TargetStudentID) : "";

		list($ClassStudentArray, $StudentIDArray) = $libreportcard->GET_STUDENT_BY_CLASSLEVEL($ReportID, $LevelList, $ClassID, $TargetStudentList);

		# Get Class Teacher Comment
		$ClassTeacherCommentArray = $libreportcard->GET_REPORT_CLASS_TEACHER_COMMENT($LevelList, $ClassID, $TargetStudentList);
		
		# Get Subject Teacher Comment
		$SubjectTeacherCommentArray = $libreportcard->GET_REPORT_SUBJECT_TEACHER_COMMENT($LevelList, $ClassID, $TargetStudentList);

		# Get Class Teacher(s)
		$ClassTeacherArray = $libreportcard->GET_CLASSTEACHER_BY_CLASSLEVEL($LevelList);
		
		################################## !!!Important!!! ##################################
		# Defined in /lang/reportcard_lang.en.php
		# To disable (not displaying) any part in "Misc", remove the array item
		$eReportCard['DisplaySettingsArray']["Misc"] = array("ClassTeacherComment", "SubjectTeacherComment", "Awards", "MeritsAndDemerits", "Remark");
		#####################################################################################
		
		$StudentInfoTitleArray = $eReportCard['DisplaySettingsArray']["StudentInfo"];
		$SettingStudentInfo = $SettingArray["StudentInfo"];

		$MiscTitleArray = $eReportCard['DisplaySettingsArray']["Misc"];
		$SettingMiscArray = $SettingArray["Misc"];
		$ShowSubjectTeacherComment = $SettingMiscArray["SubjectTeacherComment"];

		$SummaryTitleArray = $eReportCard['DisplaySettingsArray']["Summary"];
		$SettingSummaryArray = $SettingArray["Summary"];
		
		################# New Appraoch Start ################
		include_once("../default_header.php");
		
		for($i=0; $i<sizeof($DefaultHeaderArray["summary"]); $i++) {
			$tmpSettingTitle = $HeaderSettingMap["summary"][$DefaultHeaderArray["summary"][$i]];
			$SettingSummaryArray[$tmpSettingTitle] = 1;
		}
		
		if (isset($DefaultHeaderArray["award"]))
			$SettingMiscArray["Awards"] = 1;
		if (isset($DefaultHeaderArray["merit"]))
			$SettingMiscArray["MeritsAndDemerits"] = 1;
		if (isset($DefaultHeaderArray["eca"]))
			$SettingMiscArray["ECA"] = 1;
		if (isset($DefaultHeaderArray["remark"]))
			$SettingMiscArray["Remark"] = 1;
		if (isset($DefaultHeaderArray["interschool"]))
			$SettingMiscArray["InterSchoolCompetition"] = 1;
		if (isset($DefaultHeaderArray["schoolservice"]))
			$SettingMiscArray["SchoolService"] = 1;
    ################## New Appraoch End #################
		
		# Get Report Subject and Column
		$SubjectArray = $libreportcard->GET_REPORT_TEMPLATE_SUBJECT($ReportID);
		$ColumnArray = $libreportcard->GET_REPORT_TEMPLATE_COLUMN($ReportID);
		
		# Get Report Cell Setting
		$ReportCellSetting = $libreportcard->GET_REPORT_CELL($ReportID);
		
		# Get All subject array
		$AllSubjectArray = $libreportcard->GET_ALL_SUBJECTS();

		# Get Report Marksheet result
		$SubjectIDArray = $libreportcard->GET_REPORT_SELECTED_SUBJECT_ID($ReportID);
		if(is_array($SubjectIDArray) && is_array($StudentIDArray))
		{
			$SubjectIDList = implode(",", $SubjectIDArray);
			$StudentIDList = implode(",", $StudentIDArray);			
			$ReportMarksheetArray = $libreportcard->GET_REPORT_MARKSHEET_SCORES($SubjectIDList, $StudentIDList);
		}
		
		# Get Grademark scheme of subjects
		$SubjectGradingArray = $libreportcard->GET_REPORT_SUBJECT_GRADING($ReportID, $SelectedFormArr);
		if(is_array($SubjectGradingArray))
		{
			$SubjectGradingList = implode(",", $SubjectGradingArray);
			list($FailedArray, $DistinctionArray, $PFArray) = $libreportcard->GET_REPORT_GRADEMARK($SubjectGradingList);
		}

		# Get Highest Average in Class
		$HighestAverageArray = $libreportcard->GET_CLASS_HIGHEST_AVERAGE($LevelList);
		
		# Page Break Style
		$page_break_here = "<p class='breakhere'></p>\n";
		
		$lf = new libfilesystem();
		$SemesterArray = $libreportcard->GET_SEMESTER_ARRAY();
		$StudentRegNoArray = $libreportcard->GET_STUDENT_REGNO_ARRAY($LevelList, $ClassID, $TargetStudentList);
		
		#####################################################################
		# get summary data from csv file
		
		//$SummaryHeader = array("REGNO","ABSENT","LATE","ABSENT_WO_LEAVE","CONDUCT","POLITENESS","BEHAVIOUR","APPLICATION","TIDINESS");
		$SummaryHeader = $DefaultHeaderArray["summary"];
		$TargetSummaryFile = $intranet_root."/file/reportcard/summary";
		$StudentSummaryArray = GET_CSV_FILE_CONTENT($TargetSummaryFile, $SummaryHeader, "summary");
		
		#####################################################################

		#####################################################################
		# get award data from csv file

		//$AwardHeader = array("REGNO", "TITLE");
		$AwardHeader = $DefaultHeaderArray["award"];
		$TargetAwardFile = $intranet_root."/file/reportcard/award";
		$StudentAwardArray = GET_CSV_FILE_CONTENT($TargetAwardFile, $AwardHeader);
		#####################################################################

		#####################################################################
		# get merit data from csv file

		//$MeritHeader = ($ReportCardTemplate==2) ? array("REGNO", "MERIT", "DEMERIT") : array("REGNO", "TITLE");
		$MeritHeader = $DefaultHeaderArray["merit"];
		$TargetMeritFile = $intranet_root."/file/reportcard/merit";
		$StudentMeritArray = GET_CSV_FILE_CONTENT($TargetMeritFile, $MeritHeader, "merit");
		#####################################################################

		#####################################################################
		# get eca data from csv file

		//$ECAHeader = array("REGNO", "TITLE");
		$ECAHeader = $DefaultHeaderArray["eca"];
		$TargetECAFile = $intranet_root."/file/reportcard/eca";
		$StudentECAArray = GET_CSV_FILE_CONTENT($TargetECAFile, $ECAHeader, "eca");
		#####################################################################

		#####################################################################
		# get remark data from csv file

		//$RemarkHeader = array("REGNO", "REMARK");
		$RemarkHeader = $DefaultHeaderArray["remark"];
		$TargetRemarkFile = $intranet_root."/file/reportcard/remark";
		$StudentRemarkArray = GET_CSV_FILE_CONTENT($TargetRemarkFile, $RemarkHeader, "remark");
		#####################################################################

		#####################################################################
		# get inter school competition data from csv file

		//$InterSchoolHeader = array("REGNO", "TITLE");
		$InterSchoolHeader = $DefaultHeaderArray["interschool"];
		$TargetInterSchoolFile = $intranet_root."/file/reportcard/interschool";
		$StudentInterSchoolArray = GET_CSV_FILE_CONTENT($TargetInterSchoolFile, $InterSchoolHeader, "interschool");
		#####################################################################

		#####################################################################
		# get school service data from csv file

		//$SchoolServiceHeader = array("REGNO", "TITLE");
		$SchoolServiceHeader = $DefaultHeaderArray["schoolservice"];
		$TargetSchoolServiceFile = $intranet_root."/file/reportcard/schoolservice";
		$StudentSchoolServiceArray = GET_CSV_FILE_CONTENT($TargetSchoolServiceFile, $SchoolServiceHeader, "schoolservice");
		#####################################################################
		
		# get issue date
		$IssueDate = $libreportcard->GET_ISSUE_DATE($ReportID);

#######################################################################################################
		# Generate Report
		# Generate Title Table
		if ($NoHeader == -1) {
			$TitleTable = GENERATE_TITLE_TABLE($ReportTitle);
		} else {
			$TitleTable = "";
			for ($i = 0; $i < $NoHeader; $i++) {
				$TitleTable .= "<br/>";
			}
		}
		$rx = "";
		if(is_array($ClassStudentArray))
		{
			foreach($ClassStudentArray as $ClassID => $StudentArray)
			{
				if(is_array($StudentArray))
				{
					foreach($StudentArray as $StudentID => $InfoArray)
					{
						$UserInfoArray = $InfoArray["UserInfo"];
						$ResultArray = $InfoArray["Result"];				
						if(empty($ResultArray))
							continue;

						################################################################
						# Generate Student Info Table
						$StudentInfoTable = GENERATE_STUDENT_INFO_TABLE($UserInfoArray);
						
						# Generate Details Table
						$ResultTable = GENERATE_RESULT_TABLE($ReportMarksheetArray[$StudentID], $SubjectTeacherCommentArray[$StudentID]);

						# Generate Summary & Misc Table
						$SummaryMiscTable = GENERATE_SUMMARY_MISC_TABLE($ResultArray, $HighestAverageArray[$ClassID], $StudentSummaryArray[$StudentID], $ClassTeacherCommentArray[$StudentID], $StudentID);

						# Generate Signature Table
						$SignatureTable = GENERATE_SIGNATURE_TABLE();

						################################################################
												
						$rx .= "<table width='95%' height='100%' border=0 cellpadding=4 cellspacing=1 align='center' style='page-break-after:always'>\n";
						$rx .= "<tr><td valign='top'>\n";
						$rx .= $TitleTable."\n";
						$rx .= $StudentInfoTable."\n";
						$rx .= $ResultTable."\n";
						$rx .= $SummaryMiscTable."\n";
						$rx .= "</td></tr>\n";
						$rx .= ($SignatureTable!="") ? "<tr><td valign='bottom'>".$SignatureTable."</td></tr>\n" : "";
						$rx .= "</table>\n";
					}
				}
			}
		}
##########################################################################################

?>
<table border="0" cellpadding="4" width="100%" cellspacing="0" class="print_hide">
<tr><td align="right"><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></span></td></tr>
</table>
<?=$rx?>
<table border="0" cellpadding="4" width="100%" cellspacing="0" class="print_hide">
<tr><td align="right"><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></span></td></tr>
</table>

<?
	include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_footer.php");
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>