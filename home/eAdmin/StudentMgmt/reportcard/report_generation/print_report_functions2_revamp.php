<?php
############################ Function Start ######################################

# function to get Title Table
function GENERATE_TITLE_TABLE()
{
	global $ReportTitle, $image_path, $intranet_root;

	# get school badge
	$SchoolLogo = GET_SCHOOL_BADGE();

	# get school name
	$SchoolName = GET_SCHOOL_NAME();

	$TitleTable = "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>\n";
	$TitleTable .= "<tr><td width='120' align='center'>".($SchoolLogo==""?"&nbsp;":$SchoolLogo)."</td>";
	if(!empty($ReportTitle) || !empty($SchoolName))
	{
		list($ReportTitle1, $ReportTitle2) = explode("::", $ReportTitle);

		$TitleTable .= "<td>";
		$TitleTable .= "<table width='100%' border='0' cellpadding='1' cellspacing='0' align='center'>\n";
		if(!empty($SchoolName))
			$TitleTable .= "<tr><td nowrap='nowrap' class='report_title' align='center'>".$SchoolName."</td></tr>\n";
		if(!empty($ReportTitle1))
			$TitleTable .= "<tr><td nowrap='nowrap' class='report_title' align='center'>".$ReportTitle1."</td></tr>\n";
		if(!empty($ReportTitle2))
			$TitleTable .= "<tr><td nowrap='nowrap' class='report_title' align='center' colspan='2'>".$ReportTitle2."</td></tr>\n";
		$TitleTable .= "</table>\n";
		$TitleTable .= "</td>";
	}
	$TitleTable .= "<td width='120' align='center'>&nbsp;</td></tr>";
	$TitleTable .= "</table>";

	return $TitleTable;
}

# function to get Student Info Table
function GENERATE_STUDENT_INFO_TABLE($ParInfoArray)
{
	global $SettingStudentInfo, $StudentInfoTitleArray, $ClassTeacherArray, $today, $LangArray, $IssueDate;
	global $libreportcard;

	list($ClassName, $ClassNumber, $StudentName, $RegNo) = $ParInfoArray;
	if(!empty($SettingStudentInfo))
	{
		$count = 0;
		$StudentInfoTable = "<table width='100%' border='0' cellpadding='1' cellspacing='0' align='center'>\n";
		for($i=0; $i<sizeof($StudentInfoTitleArray); $i++)
		{
			$SettingID = trim($StudentInfoTitleArray[$i]);
			if($SettingStudentInfo[$SettingID]==1 && $SettingID!="ClassNumber")
			{
				$Title = $LangArray[$SettingID];
				$Title = str_replace("<br />", " ", $Title);
				$Display = "";
				switch($SettingID)
				{
					case "Name":
						$Display = $StudentName;
						break;
					case "ClassName":
						$Display = ($SettingStudentInfo["ClassNumber"]==1) ? $ClassName." (".$ClassNumber.") " : $ClassName;
						break;
					case "ClassTeacher":
						$Display = (!empty($ClassTeacherArray[$ClassName])) ? implode(", ", $ClassTeacherArray[$ClassName]) : "";
						break;
					case "DateOfIssue":
						$Display = $IssueDate;
						if ($Display == "") $Display = date('Y-m-d');
						break;
					case "StudentNo":
						$Display = $RegNo;
						break;
					case "AcademicYear":
						$Display = $libreportcard->GET_MARKSHEET_COLLECTION_YEAR();
						break;
				}

				if($count%2==0) {
					$StudentInfoTable .= "<tr>\n";
				}
				$StudentInfoTable .= "<td class='tabletext' width='20%' valign='top'>".$Title." : ".$Display."</td>\n";
				if($count%2==1) {
					$StudentInfoTable .= "</tr>\n";
				}
				$count++;
			}
		}
		$StudentInfoTable .= "</table>\n";
	}

	return $StudentInfoTable;
}

# Generate a simple cell for holding data
function GENERATE_MISC_CELL($title, $arr, $content="") {
	if ($content == "" && $arr != "") {
		$content = "<ul>";
		for($i=0; $i<sizeof($arr); $i++) {
			$content .= "<li>".$arr[$i][0]."</li>";
		}
		$content .= "</ul>";
	}
	
	$x = "<span class='small_title'>".$title."</span>";
	$x .= "<table width='100%' border='0' cellpadding='1' cellspacing='0' align='center'>";
	$x .= "<tr><td class='small_text' valign='top' height='60'>".$content."</td></tr>";
	$x .= "</table>";
	return $x;
}

# Generate the ECA data
function GENERATE_ECA_CONTENT($ecaArr) {
	global $HeaderSettingMap, $EngReportCard, $ChiReportCard;
	$x = "<ul>";
	for($i=0; $i<sizeof($ecaArr); $i++) {
		$x .= "<li>".$ecaArr[$i]["ECA"];
		if (isset($ecaArr[$i]["Grade"])) {
			$x .= " (".$ecaArr[$i]["Grade"].") </li>";
		}
	}
	$x .= "</ul>";
	return $x;
}

# function to get MISC Table
function GENERATE_MISC_TABLE($ParClassTeacherComment, $ParStudentID)
{
	global $SettingMiscArray, $MiscTitleArray, $Semester, $LangArray;
	global $StudentAwardArray, $StudentMeritArray, $StudentECAArray, $StudentRemarkArray;
	global $StudentInterSchoolArray, $StudentSchoolServiceArray;
	$ParStudentID = trim($ParStudentID);
	
	if(!empty($SettingMiscArray))
	{
		for($i=0; $i<sizeof($MiscTitleArray); $i++)
		{
			$SettingID = trim($MiscTitleArray[$i]);
			// Remark: MeritsAndDemerits displayed in summary
			if($SettingMiscArray[$SettingID]==1 && $SettingID!="SubjectTeacherComment" && $SettingID!="MeritsAndDemerits")
			{
				$Title = $LangArray[$SettingID];
				switch($SettingID)
				{
					case "ClassTeacherComment":
						$Display["ClassTeacherComment"] = GENERATE_MISC_CELL($Title, nl2br($ParClassTeacherComment));
						break;
					case "Awards":
						$Display["Awards"] = GENERATE_MISC_CELL($Title, $StudentAwardArray[$ParStudentID][$Semester]);
						break;
					case "ECA":
						$Display["ECA"] = GENERATE_MISC_CELL($Title, "", GENERATE_ECA_CONTENT($StudentECAArray[$ParStudentID][$Semester]));
						break;
					case "Remark":
						$Display["Remark"] = GENERATE_MISC_CELL($Title, $StudentRemarkArray[$ParStudentID][$Semester]);
						break;
					case "InterSchoolCompetition":
						$Display["InterSchoolCompetition"] = GENERATE_MISC_CELL($Title, $StudentInterSchoolArray[$ParStudentID][$Semester]);
						break;
					case "SchoolService":
						$Display["SchoolService"] = GENERATE_MISC_CELL($Title, $StudentSchoolServiceArray[$ParStudentID][$Semester]);
						break;
				}
			}
		}
		$IsFirst = 1;
		$MiscTable = "<table width='100%' border='0' cellpadding='1' cellspacing='0' align='center' class='report_border'>";
		
		foreach ($Display as $key => $value) {
			$MiscTable .= "<tr>";
			if($IsFirst==1) {
				$IsFirst = 0;
			} else {
				$top_border_style = "class='border_top'";
			}
			$MiscTable .= "<td width='100%' {$top_border_style}>";
			$MiscTable .= $value;
			$MiscTable .= "</td>";
			$MiscTable .= "</tr>";
		}
		$MiscTable .= "</table>";
	}

	return $MiscTable;
}

# function to get summary display
function RETURN_SUMMARY_DISPLAY($ParInfoArray, $ParSettingID, $ClassHighestAverage, $StudentSummaryArray, $ParColumnSem)
{
	global $StudentMeritArray, $StudentID;
	global $FailedArray, $DistinctionArray, $PFArray, $libreportcard;
	list($GT, $AM, $GPA, $OMC, $OMCT, $OMF, $OMFT) = $ParInfoArray;
	
	switch($ParSettingID)
	{
		case "GrandTotal":				
				$Display = ($GT > 0) ? number_format($GT, 2) : "--";
				break;
		case "GPA":
				$Display = $GPA;
				break;
		case "AverageMark":
				$Display = number_format($AM, 2);
				if ($ParColumnSem!="FULL" || $libreportcard->HighlightOverall)
				{
					list($StylePrefix, $StyleSuffix, $IsDistinct, $IsFailed) = $libreportcard->GET_STYLE("S", $AM, $FailedArray[6], $DistinctionArray[6], $PFArray[6]);
					$Display = $StylePrefix.$Display.$StyleSuffix;
				}
				if (!($GT > 0)) $Display = "--";
				break;
		case "FormPosition":
				$Display = (($OMF>0)&&($GT > 0)) ? $OMF : "--";
				break;
		case "ClassPosition":
				$Display = ($OMC>0) ? $OMC : "--";
				break;
		case "ClassPupilNumber":
				$Display = $OMCT;
				if (!($GT > 0)) $Display = "--";
				break;
		case "FormPupilNumber":
				$Display = $OMFT;
				break;
		case "ClassHighestAverage":
				$Display = $ClassHighestAverage;
				break;
		case "Conduct":
				$Display = ($StudentSummaryArray[0]["Conduct"]=="") ? "--" : $StudentSummaryArray[0]["Conduct"];
				break;
		case "Politeness":
				$Display = ($StudentSummaryArray[0]["Politeness"]=="") ? "--" : $StudentSummaryArray[0]["Politeness"];
				break;
		case "Behaviour":
				$Display = ($StudentSummaryArray[0]["Behaviour"]=="") ? "--" : $StudentSummaryArray[0]["Behaviour"];
				break;
		case "Application":
				$Display = ($StudentSummaryArray[0]["Application"]=="") ? "--" : $StudentSummaryArray[0]["Application"];
				break;
		case "Tidiness":
				$Display = ($StudentSummaryArray[0]["Tidiness"]=="") ? "--" : $StudentSummaryArray[0]["Tidiness"];
				break;
		case "Motivation":
				$Display = ($StudentSummaryArray[0]["Motivation"]=="") ? "--" : $StudentSummaryArray[0]["Motivation"];
				break;
		case "SelfConfidence":
				$Display = ($StudentSummaryArray[0]["SelfConfidence"]=="") ? "--" : $StudentSummaryArray[0]["SelfConfidence"];
				break;
		case "SelfDiscipline":
				$Display = ($StudentSummaryArray[0]["SelfDiscipline"]=="") ? "--" : $StudentSummaryArray[0]["SelfDiscipline"];
				break;
		case "Courtesy":
				$Display = ($StudentSummaryArray[0]["Courtesy"]=="") ? "--" : $StudentSummaryArray[0]["Courtesy"];
				break;
		case "Honesty":
				$Display = ($StudentSummaryArray[0]["Honesty"]=="") ? "--" : $StudentSummaryArray[0]["Honesty"];
				break;
		case "Responsibility":
				$Display = ($StudentSummaryArray[0]["Responsibility"]=="") ? "--" : $StudentSummaryArray[0]["Responsibility"];
				break;
		case "Cooperation":
				$Display = ($StudentSummaryArray[0]["Cooperation"]=="") ? "--" : $StudentSummaryArray[0]["Cooperation"];
				break;

		case "DaysAbsent":
				$Display = ($StudentSummaryArray[0]["DaysAbsent"]=="") ? "--" : $StudentSummaryArray[0]["DaysAbsent"];
				break;
		case "TimesLate":
				$Display = ($StudentSummaryArray[0]["TimesLate"]=="") ? "--" : $StudentSummaryArray[0]["TimesLate"];
				break;
		case "AbsentWOLeave":
				$Display = ($StudentSummaryArray[0]["AbsentWOLeave"]=="") ? "--" : $StudentSummaryArray[0]["AbsentWOLeave"];
				break;
		case "Merit":
				$Display = ($StudentMeritArray[$StudentID][$ParColumnSem][0]["Merit"]=="") ? "--" : $StudentMeritArray[$StudentID][$ParColumnSem][0]["Merit"];
				break;
		case "Demerit":
				$Display = ($StudentMeritArray[$StudentID][$ParColumnSem][0]["Demerit"]=="") ? "--" : $StudentMeritArray[$StudentID][$ParColumnSem][0]["Demerit"];
				break;
	}
	return $Display;
}

# function get summary array
function GET_SUMMARY_ARRAY($ParInfoArray, $ParClassHighestAverage, $ParSummaryArray)
{
	global $SummaryTitleArray, $SettingSummaryArray, $ShowFullMark, $Semester, $ColumnArray, $LangArray, $SettingArray;
	global $libreportcard, $Year;

	$HideOverallResult = $SettingArray["HideOverallResult"];
	$ReturnArray = "";
	
	if(!empty($SettingSummaryArray))
	{
		$count = 0;
		for($k=0; $k<sizeof($SummaryTitleArray); $k++)
		{
			$SettingID = trim($SummaryTitleArray[$k]);
			if ($SettingSummaryArray[$SettingID]==1 &&
			  	$SettingID!="DaysAbsent" && $SettingID!="TimesLate" && $SettingID!="AbsentWOLeave" &&
				$SettingID!="Merit" && $SettingID!="Demerit" && $SettingID!="Conduct" &&
				$SettingID!="Politeness" && $SettingID!="Behaviour" && $SettingID!="Application" &&
				$SettingID!="Tidiness" &&

				$SettingID!="Motivation" &&$SettingID!="SelfConfidence" &&$SettingID!="SelfDiscipline" &&
				$SettingID!="Courtesy" &&$SettingID!="Honesty" &&$SettingID!="Responsibility" &&
				$SettingID!="Cooperation"
				)
			{
				$Title = $LangArray[$SettingID];
				$ReturnArray[$count]["Title"] = $Title;
				for($i=0; $i<sizeof($ColumnArray); $i++)
				{
					$column = trim($ColumnArray[$i]["ColumnTitle"]);
					if(!empty($ParInfoArray[$column]))
					{
						if (!isset($SemFlag[$column]))
						{
							$SemFlag[$column] = $libreportcard->CHECK_RESULT_EXIST($Year, $column);
							if ($Semester=="FULL") $SemFlag[$column] = true;
						}
						$Display = ($SemFlag[$column]) ? RETURN_SUMMARY_DISPLAY($ParInfoArray[$column], $SettingID, $ParClassHighestAverage[$column], $ParSummaryArray[$column], $column) : 0;
						$ReturnArray[$count][$column] = $Display;
					}
				}
				if(!($Semester=="FULL" && $HideOverallResult==1))
				{
					$Display = RETURN_SUMMARY_DISPLAY($ParInfoArray[$Semester], $SettingID, $ParClassHighestAverage[$Semester], $ParSummaryArray[$Semester], $Semester);
					$ReturnArray[$count][$Semester] = $Display;
				}
				$count++;
			}
		}
	}
	return $ReturnArray;
}

# function get summary array
function GET_CONDUCT_ARRAY($ParInfoArray, $ParClassHighestAverage, $ParSummaryArray)
{
	global $SummaryTitleArray, $SettingSummaryArray, $ShowFullMark, $Semester, $ColumnArray, $LangArray, $SettingArray;
	
	$HideOverallResult = $SettingArray["HideOverallResult"];
	$ReturnArray = "";
//	debug_r($ColumnArray);
	if(!empty($SettingSummaryArray))
	{
		$count = 0;
		for($k=0; $k<sizeof($SummaryTitleArray); $k++)
		{
			$SettingID = trim($SummaryTitleArray[$k]);
			if($SettingSummaryArray[$SettingID]==1 &&
				$SettingID!="GrandTotal" && $SettingID!="GPA" && $SettingID!="AverageMark" &&
				$SettingID!="ClassHighestAverage" && $SettingID!="ClassPupilNumber" &&
				$SettingID!="ClassPosition" && $SettingID!="FormPosition" && $SettingID!="FormPupilNumber"
				)
			{
				$Title = $LangArray[$SettingID];
				$ReturnArray[$count]["Title"] = $Title;
				for($i=0; $i<sizeof($ColumnArray); $i++)
				{
					$column = trim($ColumnArray[$i]["ColumnTitle"]);
					if(!empty($ParInfoArray[$column]))
					{
						$Display = RETURN_SUMMARY_DISPLAY($ParInfoArray[$column], $SettingID, $ParClassHighestAverage[$column], $ParSummaryArray[$column], $column);
						$ReturnArray[$count][$column] = $Display;
					}
				}
				if(!($Semester=="FULL" && $HideOverallResult==1))
				{
					$Display = RETURN_SUMMARY_DISPLAY($ParInfoArray[$Semester], $SettingID, $ParClassHighestAverage[$Semester], $ParSummaryArray[$Semester], $Semester);
					$ReturnArray[$count][$Semester] = $Display;
				}
				$count++;
			}
		}
	}
	
	return $ReturnArray;
}

# function to get signature table
function GENERATE_SIGNATURE_TABLE()
{
	global $eReportCard, $SettingArray, $LangArray;
	global $SignatureWidth;

	$SignatureTitleArray = $eReportCard['DisplaySettingsArray']["Signature"];
	$SettingSignatureArray = $SettingArray["Signature"];

	$CellNumber = 0;
	$SignatureRow = "";

	for($i=0; $i<sizeof($SignatureTitleArray); $i++)
	{
		$SettingID = trim($SignatureTitleArray[$i]);
		if($SettingSignatureArray[$SettingID]==1)
		{
			$Title = $LangArray[$SettingID];
			if ($Title=="Class Teacher")
			{
				$Title .= "'s Signature";
			}
			$SignatureRow .= "<tr>";
			$SignatureRow .= "<td class='small_title report_formfieldtitle' valign='top' height='75'><span style='width:{$SignatureWidth}px'>".$Title."</span></td>";
			$SignatureRow .= "</tr>";

			$CellNumber++;
		}
	}
	if($CellNumber>0)
	{
		$SignatureTable = "<table width='100%' height='100%' border='0' cellpadding='2' cellspacing='0' align='center' valign='top'>";
		$SignatureTable .= $SignatureRow;
		$SignatureTable .= "</table>";
	}

	return $SignatureTable;
}

function GET_SEMESTER_NUMBER($ParSemester)
{
	global $libreportcard, $SemesterArray;

	$sem_number = "-1";
	if(trim($ParSemester)=="FULL")
	{
		$sem_number = 0;
	} else
	{
		for($i=0; $i<sizeof($SemesterArray); $i++)
		{
			if(trim($ParSemester)==trim($SemesterArray[$i]))
			{
				$sem_number = $i+1;
				break;
			}
		}
	}
	return $sem_number;
}

function CHECK_CSV_FILE_FORMAT($ParHeaderArray, $ParDefaultHeaderArray)
{
	# Check Title Row
	$format_wrong = false;

	for ($i=0; $i<sizeof($ParDefaultHeaderArray); $i++)
	{
		if ($ParHeaderArray[$i]!=$ParDefaultHeaderArray[$i])
		{
			$format_wrong = true;
			break;
		}
	}

	return $format_wrong;
}

function GET_CSV_FILE_CONTENT($ParFile, $ParDefaultHeaderArray, $ReportType="")
{
	global $libreportcard, $lf, $StudentRegNoArray, $SemesterArray;
	
	######### For adding associated array ##########
	global $HeaderSettingMap, $CustomSettingMap;
	################################################
	
	$ClassArr = $libreportcard->GET_CLASSES();

	$TargetArray = $SemesterArray;
	$TargetArray[] = "FULL";
	for($i=0; $i<sizeof($TargetArray); $i++)
	{
		$t_semester = trim($TargetArray[$i]);
		$sem_number = GET_SEMESTER_NUMBER($t_semester);
		if($sem_number>=0)
		{
			for ($j = 0; $j < sizeof($ClassArr); $j++)
			{
				$TempClass = str_replace(".", "", str_replace("/", "", $ClassArr[$j]));
				$TargetFile = $ParFile."/".trim($libreportcard->Year)."_".$sem_number."_".$TempClass.".csv";
				if(file_exists($TargetFile))
				{
					$data = $lf->file_read_csv($TargetFile);
					if(!empty($data))
					{
						$header_row = array_shift($data);
						$wrong_format = CHECK_CSV_FILE_FORMAT($header_row, $ParDefaultHeaderArray);
						#########################################
						# Ignore wrong format checking and force
						# assign associated array
						$wrong_format = false;
						#########################################
						if(!$wrong_format)
						{
							for($k=0; $k<sizeof($data); $k++)
							{
								$reg_no = array_shift($data[$k]);
								$reg_no = trim(str_replace("#", "", $reg_no));
								$student_id = trim($StudentRegNoArray[$reg_no]);
								if($student_id!="")
								{
									// add associated array to $data also
									if ($ReportType != "") {
										for($r=0; $r<sizeof($data[$k]); $r++) {
											# assign key name using display wording
											$data[$k][$CustomSettingMap[$ReportType][$header_row[$r+1]]] = $data[$k][$r];
										}
									}
									$ReturnArray[$student_id][$t_semester][] = $data[$k];
								}
							}
						}
					}
				}
			}
		}
	}
	return $ReturnArray;
}



# function to get result table
function GENERATE_RESULT_TABLE($ParSignatureTable, $ParAttendanceMeritTable, $ParStudentResultArray, $ParSubjectTeacherCommentArray)
{
	global $eReportCard, $SubjectArray, $ColumnArray, $ReportCellSetting, $ShowFullMark, $libreportcard;
	global $FailedArray, $DistinctionArray, $PFArray, $SettingArray;
	global $AllSubjectArray, $ShowOverallPosition, $OverallPositionRange, $HideNotEnrolled;
	global $ShowSubjectTeacherComment, $Semester;
	global $SummaryResultArray, $LangArray, $BilingualReportTitle, $ConductResultArray;
	global $LineHeight;

	# Report Type
	# 1 - Display the overall result of Parent Subject AND Component Subject(s)
	# 2 - Display the overall result of Parent Subject ONLY
	# 3 - Display the overall result of Component Subject(s) ONLY
	$ResultDisplayType = $SettingArray["ResultDisplayType"];
	$ResultCalculationType = $SettingArray["ResultCalculationType"];
	$SubjectTitleType = $SettingArray["SubjectTitleType"];
	$ShowColumnPercentage = $SettingArray["ShowColumnPercentage"];
	$MarkTypeDisplay = $SettingArray["MarkTypeDisplay"];
	$HideOverallResult = $SettingArray["HideOverallResult"];

	# get subject title cell
	if($BilingualReportTitle==1 || $SubjectTitleType==0) {
		$SubjectTitleCell = "<td width='50%' class='small_title'>".$eReportCard['EngSubject']."</td><td width='50%' class='small_title'>".$eReportCard['ChiSubject']."</td>";
	}
	else {
		$SubjectTitleCell = "<td class='small_title'>".$eReportCard['Subject']."</td>";
	}

	$ExtraColumn = 0;
	$ColumnSize = sizeof($ColumnArray);
	$ColumnWidth = round(100/($ColumnSize+3));
	$ResultTable = "<table width='100%' border='0' cellspacing='0' cellpadding='0' class='report_border'>";

	$ResultTable .= "<tr>
						<td width='25%' class='report_formfieldtitle' height='100%'>
							<table width='100%' height='100%' border='0' cellpadding='0' cellspacing='0'>
							<tr valign='top'><td align='right' class='small_title' colspan='2'>".$LangArray['Marks']."</td></tr>
							<tr valign='bottom'>".$SubjectTitleCell."</tr>
							</table>
						</td>";

	/*
	$ResultTable .= "<tr>
						<td width='25%' class='report_formfieldtitle' height='100%'>
							<table width='100%' height='100%' border='0' cellpadding='2' cellspacing='0'>
							<tr>".$SubjectTitleCell."</tr>
							</table>
						</td>";
						*/
	if($ShowFullMark==1)
	{
		$ResultTable .= "<td width='5%' class='border_left small_title report_formfieldtitle' align='center'>".$LangArray['FullMark']."</td>";
		$ExtraColumn++;
	}

	list($op_from, $op_to) = explode(",", $OverallPositionRange);
	for($i=0; $i<sizeof($ColumnArray); $i++)
	{
		list($ReportColumnID, $ColumnTitle, $Weight, $ShowPosition, $PositionRange) = $ColumnArray[$i];
		if($Weight!="")
		{
			$ColumnTitle = ($ShowColumnPercentage==1) ? $ColumnTitle."&nbsp;".$Weight."%" : $ColumnTitle;
			$WeightArray[$ReportColumnID] = $Weight;
		}

		$ResultTable .= "<td class='border_left report_formfieldtitle' width='{$ColumnWidth}%'><table border=0 cellpadding=0 cellspacing=0 width='100%'>";
		$ResultTable .= "<tr height='75%'><td align='center' class='small_title' colspan='3'>".$ColumnTitle."</td></tr>";
		if($ShowPosition==1 || $ShowPosition==2)
		{
			$ResultTable .= "<tr>";
			$ResultTable .= "<td align='center' width='50%' class='reportcard_text'>".$LangArray['Mark']."</td>";
			$ResultTable .= "<td width='1'>&nbsp;</td>";
			$ResultTable .= "<td align='center' width='50%' class='reportcard_text'>(".$LangArray['Position'].")</td>";
			$ResultTable .= "</tr>";
		}
		$ResultTable .= "</table></td>";
	}

	if($ColumnSize>1 && $HideOverallResult!=1)
	{
		$ResultTable .= "<td class='border_left report_formfieldtitle' width='{$ColumnWidth}%'><table border=0 cellpadding=0 cellspacing=0 width='100%' align='center'>";
		$ResultTable .= "<tr height='75%'><td align='center' class='small_title' colspan='3'>".$LangArray['OverallResult']."</td></tr>";
		if($ShowOverallPosition==1 || $ShowOverallPosition==2)
		{
			$ResultTable .= "<tr>";
			$ResultTable .=  "<td align='center' width='50%' class='reportcard_text'>".$LangArray['Mark']."</td>";
			$ResultTable .= "<td width='1'>&nbsp;</td>";
			$ResultTable .= "<td align='center' width='50%' class='reportcard_text'>(".$LangArray['Position'].")</td>";
			$ResultTable .= "</tr>";
		}
		$ResultTable .= "</table></td>";
		$ExtraColumn++;
	}

	if($ShowSubjectTeacherComment==1) {
		$ResultTable .= "<td width='10%' class='border_left small_title report_formfieldtitle' align='center'>".$LangArray['SubjectTeacherComment']."</td>";
	}

	if ($ParSignatureTable!="" || $ParAttendanceMeritTable!="")
	{
			$ResultTable .= "<td height='100%' rowspan='50' class='border_left' width='2'><img src='/images/spacer.gif' width='2' height='1' border='0' /></td>";
			$ResultTable .= "<td width='15%' height='100%' rowspan='50' class='border_left'>";
			$ResultTable .= "<table border='0' height='100%' width='100%' cellpadding='0' cellspacing='0'>";
			$ResultTable .= ($ParSignatureTable!="") ? "<tr><td valign='top'>".$ParSignatureTable."</td></tr>" : "";
			//$ResultTable .= ($ParAttendanceMeritTable!="") ? "<tr><td valign='bottom'>".$ParAttendanceMeritTable."</td></tr>" : "";
			$ResultTable .= "</table>";
			$ResultTable .= "</td>";
	}
	$ResultTable .= "</tr>";

	$ColumnSpan = $ColumnSize + $ExtraColumn + 1;
	$IsFirst = 1;
	if(is_array($SubjectArray))
	{
		$count = 0;
		foreach($SubjectArray as $CodeID => $Data)
		{
			if(is_array($Data))
			{
				foreach($Data as $CmpCodeID => $InfoArr)
				{
					list($ReportSubjectID, $SubjectID, $EngSubjectName, $ChiSubjectName, $FullMark, $Scale, $HidePosition) = $InfoArr;
					# Hide not enrolled subject according to the setting
					//if($HideNotEnrolled==1 && (empty($ParStudentResultArray[$SubjectID]["Overall"]) || strcmp($ParStudentResultArray[$SubjectID]["Overall"]["Grade"], "/")==0))
					if ($HideNotEnrolled==1 && !$libreportcard->CHECK_ENROL($ParStudentResultArray[$SubjectID]))
					{
						continue;
					}
					//debug($SubjectID);
					//debug_r($ParStudentResultArray[$SubjectID]);

					$IsComponent = ($CmpCodeID>0) ? 1 : 0;
					$top_style = ($IsComponent==0 && $IsFirst==0) ? "border_top" : "";
					$IsFirst = 0;
					$Prefix = ($IsComponent==1) ? "&nbsp;&nbsp;&nbsp;&nbsp;" : "";

					# Highest Grade
					$MaxGrade = $DistinctionArray[$SubjectID]["G"];
					$FirstRowSetting = $ReportCellSetting[$ColumnArray[0][0]][$ReportSubjectID];
					$FullMark = ($FirstRowSetting=="G") ? $MaxGrade : $FullMark;

					# check whether is parent/component subject
					$IsParent = (sizeof($AllSubjectArray[$SubjectID])>1) ? 1 : 0;
					$IsComponent = (empty($AllSubjectArray[$SubjectID])) ? 1 : 0;

					$ResultShow = (!($ResultDisplayType==2 && $IsParent==1) && !($IsParent==1 && $ResultCalculationType==1)) ? 1 : 0;
					$OverallShow = (!($ResultDisplayType==2 && $IsParent==1) && !($ResultDisplayType==1 && $IsComponent==1)) ? 1 : 0;

					# get subject display cell
					if($BilingualReportTitle==1 || $SubjectTitleType==0) {
						$SubjectCell = "<td>
								<table border='0' cellpadding='1' cellspacing='0' width='100%' height='100%'>
								<tr>
									<td width='50%' class='small_text'>".$Prefix.$EngSubjectName."</td>
									<td width='50%' class='small_text'>".$Prefix.$ChiSubjectName."</td>
								</tr>
								</table>
								</td>";
					}
					else {
						$SubjectDisplayName = ($SubjectTitleType==1) ? $EngSubjectName : $ChiSubjectName;
						$SubjectCell = "<td>
										<table border='0' cellpadding='1' cellspacing='0' width='100%' height='100%'>
										<tr>
											<td class='small_text'>".$Prefix.$SubjectDisplayName."</td>
										</tr>
										</table>
										</td>";
					}

					$ResultTable .= "<tr>";
					// line height - aki
					$ResultTable .= "<td class='$top_style'>
										<table width='100%' border='0' cellpadding='0' cellspacing='0' height='$LineHeight'><tr>".$SubjectCell."</tr></table>
									</td>";
					$ResultTable .= ($ShowFullMark==1) ? "<td class='tabletext border_left $top_style' align='center'>".($ResultShow==1?$FullMark:"&nbsp;")."</td>" : "";

					for($i=0; $i<sizeof($ColumnArray); $i++)
					{
						list($ReportColumnID, $ColumnTitle, $Weight, $ShowPosition, $PositionRange) = $ColumnArray[$i];
						$CellSetting = $ReportCellSetting[$ReportColumnID][$ReportSubjectID];
						list($p_form, $p_to) = explode(",", $PositionRange);

						$DisplayResult = "";
						if(!$ResultShow)
						{
							$ResultTable .= "<td class='border_left $top_style' align='center'>&nbsp;</td>";
						}
						else
						{
							if($CellSetting!="N/A")
							{
								$StylePrefix = "";
								$StyleSuffix = "";
								if($CellSetting=="S")
								{
									$s_raw_mark = $ParStudentResultArray[$SubjectID][$ReportColumnID]["RawMark"];
									$s_weighted_mark = $ParStudentResultArray[$SubjectID][$ReportColumnID]["WeightedMark"];
									$s_grade = $ParStudentResultArray[$SubjectID][$ReportColumnID]["Grade"];

									if($s_raw_mark>=0)
									{
										if($MarkTypeDisplay==2)
										{
											$DisplayResult = $s_grade;
											list($StylePrefix, $StyleSuffix, $IsDistinct, $IsFailed) = $libreportcard->GET_STYLE("G", $DisplayResult, $FailedArray[$SubjectID], $DistinctionArray[$SubjectID], $PFArray[$SubjectID]);
										} else
										{
											list($StylePrefix, $StyleSuffix, $IsDistinct, $IsFailed) = $libreportcard->GET_STYLE($CellSetting, $s_raw_mark, $FailedArray[$SubjectID], $DistinctionArray[$SubjectID], $PFArray[$SubjectID]);
											$DisplayResult = ($MarkTypeDisplay==1) ? $s_raw_mark : $s_weighted_mark;
										}
									}
									else
										$DisplayResult = $libreportcard->SpecialMarkArray[trim($s_raw_mark)];
								} else
								{
									$DisplayResult = $ParStudentResultArray[$SubjectID][$ReportColumnID]["Grade"];
									if($DisplayResult!="/" && $DisplayResult!="abs")
									{
										list($StylePrefix, $StyleSuffix, $IsDistinct, $IsFailed) = $libreportcard->GET_STYLE($CellSetting, $DisplayResult, $FailedArray[$SubjectID], $DistinctionArray[$SubjectID], $PFArray[$SubjectID]);
									}
								}
								$DisplayResult = $StylePrefix.$DisplayResult.$StyleSuffix;
								$FormPosition = $ParStudentResultArray[$SubjectID][$ReportColumnID]["OMF"];
								$ClassPosition = $ParStudentResultArray[$SubjectID][$ReportColumnID]["OMC"];
							}

							$ResultTable .= "<td class='border_left $top_style' width='10%'><table border=0 cellpadding=0 cellspacing=0 width='100%'>";
							$ResultTable .= "<tr>";
							if($ShowPosition==1 || $ShowPosition==2)
							{
								$ResultTable .= "<td align='center' class='tabletext' width='50%'>".($DisplayResult==""?"--":$DisplayResult)."</td>";
								$ResultTable .= "<td width='1'>&nbsp;</td>";

								$PosDisplay = ($ShowPosition==1) ? $ClassPosition : $FormPosition;
								$PosDisplay = ($PosDisplay>0 && ($PosDisplay>=$p_from && $PosDisplay<=$p_to)) ? $PosDisplay : "*";
								$ResultTable .= ($HidePosition==0 && $CellSetting=="S") ? "<td align='center' class='tabletext' width='50%'>(".$PosDisplay.")</td>" : "<td align='center' class='tabletext width='50%''>(*)</td>";
							}
							else
							{
								$ResultTable .= "<td align='center' class='tabletext'>".(($DisplayResult=="")?"--":$DisplayResult)."</td>";
							}
							$ResultTable .= "</tr>";
							$ResultTable .= "</table></td>";
						}
					}

					if($ColumnSize>1 && $HideOverallResult!=1)
					{
						if(!$OverallShow)
						{
							$ResultTable .= "<td class='tabletext border_left $top_style' align='center'>&nbsp;</td>";
						}
						else
						{
							$StylePrefix = "";
							$StyleSuffix = "";
							$IsDistinct = "";
							$IsFailed = "";
							if($CellSetting=="S")
							{
								$s_overall_mark = $ParStudentResultArray[$SubjectID]["Overall"]["Mark"];
								$s_overall_grade = $ParStudentResultArray[$SubjectID]["Overall"]["Grade"];
								
								

								if($s_overall_mark>=0)
								{
									if($MarkTypeDisplay==2)
									{
										$OverallResult = $s_overall_grade;
										if ($libreportcard->HighlightOverall)
										{
											list($StylePrefix, $StyleSuffix, $IsDistinct, $IsFailed) = $libreportcard->GET_STYLE("G", $OverallResult, $FailedArray[$SubjectID], $DistinctionArray[$SubjectID], $PFArray[$SubjectID]);
										}
									} else
									{
										$OverallResult = $s_overall_mark;
										if ($libreportcard->HighlightOverall)
										{
											list($StylePrefix, $StyleSuffix, $IsDistinct, $IsFailed) = $libreportcard->GET_STYLE($CellSetting, $OverallResult, $FailedArray[$SubjectID], $DistinctionArray[$SubjectID], $PFArray[$SubjectID]);
										}
									}
								} else
								{
									$OverallResult = $libreportcard->SpecialMarkArray[trim($s_overall_mark)];
								}
							} else
							{
								$OverallResult = $ParStudentResultArray[$SubjectID]["Overall"]["Grade"];
								
								if($OverallResult!="/" && $OverallResult!="abs")
								{
									if ($libreportcard->HighlightOverall)
									{
										list($StylePrefix, $StyleSuffix, $IsDistinct, $IsFailed) = $libreportcard->GET_STYLE($CellSetting, $OverallResult, $FailedArray[$SubjectID], $DistinctionArray[$SubjectID], $PFArray[$SubjectID]);
									}
								}
							}
							if ($OverallResult=="/")
							{
								$OverallResult = "--";
							}
							$OverallResult = $StylePrefix.$OverallResult.$StyleSuffix;
							$OverallFormPosition = $ParStudentResultArray[$SubjectID]["Overall"]["OMF"];
							$OverallClassPosition = $ParStudentResultArray[$SubjectID]["Overall"]["OMC"];

							$ResultTable .= "<td class='border_left $top_style' width='10%'><table border=0 cellpadding=0 cellspacing=0 width='100%'>";
							$ResultTable .= "<tr>";
							if($ShowOverallPosition==1 || $ShowOverallPosition==2)
							{
								$ResultTable .= "<td align='center' class='tabletext' width='50%'>".($OverallResult==""?"--":$OverallResult)."</td>";
								$ResultTable .= "<td>&nbsp;</td>";

								$OverallPosDisplay = ($ShowOverallPosition==1) ? $OverallClassPosition : $OverallFormPosition;
								$OverallPosDisplay = ($OverallPosDisplay>0 && ($OverallPosDisplay>=$op_from && $OverallPosDisplay<=$op_to)) ? $OverallPosDisplay : "*";
								//aki
								if ($IsFailed) $OverallPosDisplay = "*";
								$ResultTable .= ($HidePosition==0 && $CellSetting=="S") ? "<td align='center' class='tabletext' width='50%'>(".$OverallPosDisplay.")</td>" : "<td align='center' class='tabletext' width='50%'>(*)</td>";
							}
							else
							{
								$ResultTable .= "<td align='center' class='tabletext'>".($OverallResult==""?"--":$OverallResult)."</td>";
							}
							$ResultTable .= "</tr>";
							$ResultTable .= "</table></td>";
						}
					}
					if($ShowSubjectTeacherComment==1)
					{
						$Comment = $ParSubjectTeacherCommentArray[$SubjectID];
						$ResultTable .= "<td class='tabletext border_left $top_style' align='center'>".($Comment==""?"&nbsp;":$Comment)."</td>";
					}
					$ResultTable .= "</tr>";
					$count++;
				}
			}
		}
	}
	else
	{
		$ResultTable .= "<tr><td align='center' class='small_text report_formfieldtitle' colspan='".$ColumnSpan."'>".$eReportCard['NoRecord']."</td></tr>";
	}
//hdebug_r($SummaryResultArray);
//hdebug_r(getSemesters());
//hdebug(getCurrentSemester());

	if(!empty($SummaryResultArray))
	{
		for($k=0; $k<sizeof($SummaryResultArray); $k++)
		{
			$ResultTable .= "<tr>";
			$top_border_style = ($k==0) ? "border_top" : "";
			$colspan = ($ShowFullMark==1) ? "colspan='2'" : "";
			// line height - aki
			$ResultTable .= "<td class='{$top_border_style}' {$colspan}>
						<table border='0' cellpadding='1' cellspacing='0' width='100%' height='$LineHeight'><tr><td class='small_title'>".$SummaryResultArray[$k]["Title"]."</td></tr></table>
						</td>";
			for($i=0; $i<sizeof($ColumnArray); $i++)
			{
				$ColumnTitle = trim($ColumnArray[$i]["ColumnTitle"]);
				$ResultDisplay = $SummaryResultArray[$k][$ColumnTitle];
				//aki
				$ResultTable .= "<td class='tabletext border_left {$top_border_style}' align='center'>".($ResultDisplay==""?"--":$ResultDisplay)."</td>";
			}
			//if(!($Semester=='FULL' && $HideOverallResult==1))
			if ($Semester=='FULL' || $HideOverallResult!=1)
				$ResultTable .= "<td class='tabletext border_left {$top_border_style}' align='center'>".($SummaryResultArray[$k][$Semester]==""?"--":$SummaryResultArray[$k][$Semester])."</td>";
			if($ShowSubjectTeacherComment==1) {
				$ResultTable .= "<td class='border_left {$top_border_style}' align='center'>&nbsp;</td>";
			}
			$ResultTable .= "</tr>";
		}
	}
	
	if(!empty($ConductResultArray))
	{
		for($k=0; $k<sizeof($ConductResultArray); $k++)
		{
			$ResultTable .= "<tr>";
			$top_border_style = ($k==0) ? "border_top" : "";
			$colspan = ($ShowFullMark==1) ? "colspan='2'" : "";
			// line height - aki
			$ResultTable .= "<td class='{$top_border_style}' {$colspan}>
						<table border='0' cellpadding='1' cellspacing='0' width='100%' height='$LineHeight'><tr><td class='small_title'>".$ConductResultArray[$k]["Title"]."</td></tr></table>
						</td>";
			for($i=0; $i<sizeof($ColumnArray); $i++)
			{
				$ColumnTitle = trim($ColumnArray[$i]["ColumnTitle"]);
				$ResultDisplay = $ConductResultArray[$k][$ColumnTitle];
				//aki
				$ResultTable .= "<td class='tabletext border_left {$top_border_style}' align='center'>".($ResultDisplay==""?"--":$ResultDisplay)."</td>";
			}
			//if(!($Semester=='FULL' && $HideOverallResult==1))
			if ($Semester=='FULL' || $HideOverallResult!=1)
				$ResultTable .= "<td class='tabletext border_left {$top_border_style}' align='center'>".($ConductResultArray[$k][$Semester]==""?"--":$ConductResultArray[$k][$Semester])."</td>";
			if($ShowSubjectTeacherComment==1) {
				$ResultTable .= "<td class='border_left {$top_border_style}' align='center'>&nbsp;</td>";
			}
			$ResultTable .= "</tr>";
		}
	}

	$ResultTable .= "</table>";

	return $ResultTable;
}

# generate attendance table
function GENERATE_ATTENDANCE_AND_MERIT_TABLE($ParSummaryArray, $ParMeritArray)
{
	global $SummaryTitleArray, $SettingSummaryArray, $SettingMiscArray, $ColumnArray, $eReportCard, $SemesterArray, $LangArray;
	global $DaysAbsentTitle, $TimesLateTitle, $AbsentWOLeaveTitle;
	
	$AttendanceTableShow = ($SettingSummaryArray["DaysAbsent"]==1 || $SettingSummaryArray["TimesLate"] || $SettingSummaryArray["AbsentWOLeave"]);
	$MeritTableShow = ($SettingMiscArray["MeritsAndDemerits"]==1);

	$SemColumnCount = 0;
	for($i=0; $i<sizeof($ColumnArray); $i++)
	{
		$sem_title = trim($ColumnArray[$i]["ColumnTitle"]);
		if($sem_title!="") {
			if(in_array($sem_title, $SemesterArray))
			{
				$SemesterHeaderRow .= ($AttendanceTableShow==1 || $MeritTableShow==1) ? "<td class='reportcard_text border_top border_left' align='center'>".$sem_title."</td>\n" : "";

				$absent_result = $ParSummaryArray[$sem_title][0]["DaysAbsent"];
				$DaysAbsentCell .= "<td class='reportcard_text border_top border_left' align='center'>".($absent_result==""?"--":$absent_result)."</td>\n";
				
				$late_result = $ParSummaryArray[$sem_title][0]["TimesLate"];
				$LateCell .= "<td class='reportcard_text border_top border_left' align='center'>".($late_result==""?"--":$late_result)."</td>\n";

				$absent_wo_leave_result = $ParSummaryArray[$sem_title][0]["AbsentWOLeave"];
				$AbsentWOLeaveCell .= "<td class='reportcard_text border_top border_left' align='center'>".($absent_wo_leave_result==""?"--":$absent_wo_leave_result)."</td>\n";

				$MeritResult = $ParMeritArray[$sem_title][0]["Merit"];
				$MeritCell .= "<td class='reportcard_text border_top border_left' align='center'>".($MeritResult==""?"--":$MeritResult)."</td>\n";

				$DemeritResult = $ParMeritArray[$sem_title][0]["Demerit"];
				$DemeritCell .= "<td class='reportcard_text border_top border_left' align='center'>".($DemeritResult==""?"--":$DemeritResult)."</td>\n";

				$SemColumnCount++;
			}
		}
	}

	if($SemColumnCount>0 && ($AttendanceTableShow==1 || $MeritTableShow==1))
	{
		$ReturnTable = "<table width='100%' valign='bottom' border='0' cellspacing='0' cellpadding='1'>\n";
		if($AttendanceTableShow==1)
		{
			$ReturnTable .= "<tr><td class='reportcard_text border_top'>".$LangArray['Attendance']."</td>\n";
			$ReturnTable .= $SemesterHeaderRow;
		}
		
		# Rows of Attendance records
		if($SettingSummaryArray["DaysAbsent"]==1)
		{
			$ReturnTable .= "<tr><td class='reportcard_text border_top'>".$DaysAbsentTitle."</td>\n";
			$ReturnTable .= $DaysAbsentCell;
			$ReturnTable .= "</tr>";
		}
		if($SettingSummaryArray["TimesLate"]==1)
		{
			$ReturnTable .= "<tr><td class='reportcard_text border_top'>".$TimesLateTitle."</td>\n";
			$ReturnTable .= $LateCell;
			$ReturnTable .= "</tr>";
		}
		if($SettingSummaryArray["AbsentWOLeave"]==1)
		{
			$ReturnTable .= "<tr><td class='reportcard_text border_top'>".$AbsentWOLeaveTitle."</td>";
			$ReturnTable .= $AbsentWOLeaveCell;
			$ReturnTable .= "</tr>";
		}

		# Row of Merit records
		if($MeritTableShow==1)
		{
			if($AttendanceTableShow==1) {
				$ReturnTable .= "<tr><td class='reportcard_text border_top' colspan='".($SemColumnCount+1)."'>".$LangArray['MeritsAndDemerits']."</td></tr>\n";
			}
			else {
				$ReturnTable .= "<tr><td class='reportcard_text border_top'>".$LangArray['MeritsAndDemerits']."</td>\n";
				$ReturnTable .= $SemesterHeaderRow;
			}

			$ReturnTable .= "<tr><td class='reportcard_text border_top'>".$LangArray['Merit']."</td>\n";
			$ReturnTable .= $MeritCell;
			$ReturnTable .= "</tr>";
			$ReturnTable .= "<tr><td class='reportcard_text border_top'>".$LangArray['Demerit']."</td>\n";
			$ReturnTable .= $DemeritCell;
			$ReturnTable .= "</tr>";
		}
		$ReturnTable .= "</table>";
	}

	return $ReturnTable;
}

################################ Function End #####################################
?>