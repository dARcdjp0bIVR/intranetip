<?php
echo "<script>if(typeof(opener)!='undefined') { opener.close(); }</script>";
$PATH_WRT_ROOT = "../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_header.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if ($plugin['ReportCard'])
{
	include_once($PATH_WRT_ROOT."includes/libreportcard.php");
	include_once($PATH_WRT_ROOT."home/admin/reportcard/report_generation/print_report_functions5.php");
	
	$libreportcard = new libreportcard();	
	$CurrentPage = "ReportGeneration";
	if ($libreportcard->hasAccessRight())
    {
	    $linterface = new interface_html();
#######################################################################################################
		
		# get current Year and Semester
		$Year = trim($libreportcard->Year);
		$Semester = trim($libreportcard->Semester);

		# Get Report Info
		$ReportInfo = $libreportcard->GET_REPORT_TEMPLATE($ReportID);

		list($ReportTitle, $ReportType, $Description, $ShowFullMark, $Settings, $ReportStatus, $ShowOverallPosition, $OverallPositionRange, $HideNotEnrolled, $Footer, $NoHeader, $LineHeight, $SignatureWidth) = $ReportInfo;
		$SettingArray = unserialize($Settings);
		$BilingualReportTitle = $SettingArray["BilingualReportTitle"];
		$SubjectTitleType = $SettingArray["SubjectTitleType"];

		# define the name of the variable array by checking the bilingual setting
		if($BilingualReportTitle==1)
		{
			$LangArray = $eReportCardBilingual;

			# prepare titles
			$DaysAbsentTitle = $LangArray["DaysAbsent"];
			$pos = strpos($DaysAbsentTitle, " ");
			$DaysAbsentTitle = substr_replace($DaysAbsentTitle, "<br />", $pos, 1);

			$TimesLateTitle = $LangArray["TimesLate"];
			$pos = strpos($TimesLateTitle, " ");
			$TimesLateTitle = substr_replace($TimesLateTitle, "<br />", $pos, 1);

			$AbsentWOLeaveTitle = $LangArray["AbsentWOLeave"];
			$pos = strpos($AbsentWOLeaveTitle, " ");
			$AbsentWOLeaveTitle = substr_replace($AbsentWOLeaveTitle, "<br />", $pos, 1);
		}
		else {
			$LangArray = ($SubjectTitleType==1) ? $EngReportCard : $ChiReportCard;
		
			# prepare titles
			$DaysAbsentTitle = $LangArray["DaysAbsent"];
			$TimesLateTitle = $LangArray["TimesLate"];
			$AbsentWOLeaveTitle = $LangArray["AbsentWOLeave"];
		}
		
		# Get Class Level(s) of the report
		if($ClassLevelID!="")
			$SelectedFormArr = array($ClassLevelID);
		else
			$SelectedFormArr = $libreportcard->GET_REPORT_TEMPLATE_FORM($ReportID);

		# Get Class Student(s)
		$LevelList = implode(",", $SelectedFormArr);
		
		$TargetStudentList = (is_array($TargetStudentID)) ? implode(",", $TargetStudentID) : "";

		list($ClassStudentArray, $StudentIDArray) = $libreportcard->GET_STUDENT_BY_CLASSLEVEL($ReportID, $LevelList, $ClassID, $TargetStudentList);

		# Get Class Teacher Comment
		$ClassTeacherCommentArray = $libreportcard->GET_REPORT_CLASS_TEACHER_COMMENT($LevelList, $ClassID, $TargetStudentList);
		
		# Get Subject Teacher Comment
		$SubjectTeacherCommentArray = $libreportcard->GET_REPORT_SUBJECT_TEACHER_COMMENT($LevelList, $ClassID, $TargetStudentList);

		# Get Class Teacher(s)
		$ClassTeacherArray = $libreportcard->GET_CLASSTEACHER_BY_CLASSLEVEL($LevelList);
		
		//$StudentInfoTitleArray = $eReportCard['DisplaySettingsArray']["StudentInfo"];
		$StudentInfoTitleArray = array("Name", "ClassName", "Name", "DateOfBirth", "Gender", "StudentNo", "ClassNumber", "DateOfIssue");
		
		$SettingStudentInfo = $SettingArray["StudentInfo"];

		//$MiscTitleArray = $eReportCard['DisplaySettingsArray']["Misc"];
		$MiscTitleArray = array("ClassTeacherComment", "SubjectTeacherComment", "Awards", "MeritsAndDemerits", "MinorCredit", "MajorCredit", "MinorFault", "MajorFault", "Remark", "InterSchoolCompetition", "SchoolService", "ECA");
		$SettingMiscArray = $SettingArray["Misc"];
		$ShowSubjectTeacherComment = $SettingMiscArray["SubjectTeacherComment"];

		//$SummaryTitleArray = $eReportCard['DisplaySettingsArray']["Summary"];
		$SummaryTitleArray = array("GrandTotal", "GPA", "AverageMark", "FormPosition", "ClassPosition", "ClassPupilNumber", "FormPupilNumber", "ClassHighestAverage", "Conduct", "Politeness", "Behaviour", "Application", "Motivation", "SelfConfidence", "SelfDiscipline", "Courtesy", "Honesty", "Responsibility", "Cooperation", "Tidiness", "DaysAbsent", "TimesLate", "AbsentWOLeave");

		### add merit to display detail (aki)
		$SettingSummaryArray = $SettingArray["Summary"];		
		if ($SettingMiscArray["MeritsAndDemerits"]==1) {
			$SettingSummaryArray['Merit'] = 1;
			$SettingSummaryArray['Demerit'] = 1;
			
			$SummaryTitleArray[] = "Merit";
			$SummaryTitleArray[] = "Demerit";			
		}
		
		# Get Report Subject and Column
		$SubjectArray = $libreportcard->GET_REPORT_TEMPLATE_SUBJECT($ReportID);
		$ColumnArray = $libreportcard->GET_REPORT_TEMPLATE_COLUMN($ReportID);
		
		# Get Report Cell Setting
		$ReportCellSetting = $libreportcard->GET_REPORT_CELL($ReportID);
		
		# Get All subject array
		$AllSubjectArray = $libreportcard->GET_ALL_SUBJECTS();

		# Get Report Marksheet result
		$SubjectIDArray = $libreportcard->GET_REPORT_SELECTED_SUBJECT_ID($ReportID);
		if(is_array($SubjectIDArray) && is_array($StudentIDArray))
		{
			$SubjectIDList = implode(",", $SubjectIDArray);
			$StudentIDList = implode(",", $StudentIDArray);
			$ReportMarksheetArray = $libreportcard->GET_REPORT_MARKSHEET_SCORES($SubjectIDList, $StudentIDList);
		}
		
		# Get Grademark scheme of subjects
		$SubjectGradingArray = $libreportcard->GET_REPORT_SUBJECT_GRADING($ReportID, $SelectedFormArr);
		if(is_array($SubjectGradingArray))
		{
			$SubjectGradingList = implode(",", $SubjectGradingArray);
			list($FailedArray, $DistinctionArray, $PFArray) = $libreportcard->GET_REPORT_GRADEMARK($SubjectGradingList);
		}

		# Get Highest Average in Class
		$HighestAverageArray = $libreportcard->GET_CLASS_HIGHEST_AVERAGE($LevelList);
		
		# Page Break Style
		$page_break_here = "<p class='breakhere'></p>\n";
		
		$lf = new libfilesystem();
		$SemesterArray = $libreportcard->GET_SEMESTER_ARRAY();
		$SemColumnExist = 0;
		for($i=0; $i<sizeof($ColumnArray); $i++)
		{
			$ColumnTitle = trim($ColumnArray[$i]["ColumnTitle"]);
			if(in_array($ColumnTitle, $SemesterArray))
			{
				//$ColumnSemesterArray[] = $ColumnTitle;
				$SemColumnExist = 1;
				break;
			}
		}

		$StudentRegNoArray = $libreportcard->GET_STUDENT_REGNO_ARRAY($LevelList, $ClassID, $TargetStudentList);
		#####################################################################
		# get summary data from csv file
		
		$SummaryHeader = array("REGNO","ABSENT","LATE","ABSENT_WO_LEAVE","TIDINESS","MOTIVATION","SELF_CONFIDENCE","SELF_DISCIPLINE", "COURTESY", "HONESTY", "RESPONSIBILITY", "COOPERATION");
		$TargetSummaryFile = $intranet_root."/file/reportcard/summary";
		$StudentSummaryArray = GET_CSV_FILE_CONTENT($TargetSummaryFile, $SummaryHeader);
		#####################################################################
			
		#####################################################################
		# get award data from csv file
		
		$AwardHeader = array("REGNO", "TITLE");
		$TargetAwardFile = $intranet_root."/file/reportcard/award";
		$StudentAwardArray = GET_CSV_FILE_CONTENT($TargetAwardFile, $AwardHeader);
		#####################################################################

		#####################################################################
		# get merit data from csv file
		
		//$MeritHeader = ($ReportCardTemplate==2) ? array("REGNO", "MERIT", "DEMERIT") : array("REGNO", "TITLE");
		$MeritHeader = array("REGNO", "MERIT", "DEMERIT");
		$TargetMeritFile = $intranet_root."/file/reportcard/merit";
		$StudentMeritArray = GET_CSV_FILE_CONTENT($TargetMeritFile, $MeritHeader);		
		#####################################################################
		
		#####################################################################
		# get eca data from csv file
		
		$ECAHeader = array("REGNO", "TITLE");
		$TargetECAFile = $intranet_root."/file/reportcard/eca";
		$StudentECAArray = GET_CSV_FILE_CONTENT($TargetECAFile, $ECAHeader);
		#####################################################################

		#####################################################################
		# get remark data from csv file
		
		$RemarkHeader = array("REGNO", "REMARK");
		$TargetRemarkFile = $intranet_root."/file/reportcard/remark";
		$StudentRemarkArray = GET_CSV_FILE_CONTENT($TargetRemarkFile, $RemarkHeader);
		#####################################################################
		
		#####################################################################
		# get inter school competition data from csv file
		
		$InterSchoolHeader = array("REGNO", "TITLE");
		$TargetInterSchoolFile = $intranet_root."/file/reportcard/interschool";
		$StudentInterSchoolArray = GET_CSV_FILE_CONTENT($TargetInterSchoolFile, $InterSchoolHeader);
		#####################################################################
		
		#####################################################################
		# get school service data from csv file
		
		$SchoolServiceHeader = array("REGNO", "TITLE");
		$TargetSchoolServiceFile = $intranet_root."/file/reportcard/schoolservice";
		$StudentSchoolServiceArray = GET_CSV_FILE_CONTENT($TargetSchoolServiceFile, $SchoolServiceHeader);
		#####################################################################

		# get issue date
		$IssueDate = $libreportcard->GET_ISSUE_DATE($ReportID);
		
#######################################################################################################
		
		if(is_array($ClassStudentArray))
		{
			foreach($ClassStudentArray as $ClassID => $StudentArray)
			{
				if(is_array($StudentArray))
				{
					foreach($StudentArray as $StudentID => $InfoArray)
					{
						$UserInfoArray = $InfoArray["UserInfo"];

						$ResultArray = $InfoArray["Result"];
						if(empty($ResultArray))
							continue;
						
						$PhotoLink = $libreportcard->GET_USER_PHOTO($StudentID);
						
						# Generate Student Info Table
						$StudentInfoTable = GENERATE_STUDENT_INFO_TABLE($UserInfoArray);
						
						# Generate Report
						# Generate Title Table		
						if ($NoHeader == -1) {
							$TitleTable = GENERATE_TITLE_TABLE($ReportTitle);
						} else {
							$TitleTable = "";
						for ($i = 0; $i < $NoHeader; $i++) {
							$TitleTable .= "<br/>";
						}
					}
					
					################################################################
					# Generate Signature Table
					$SignatureTable = GENERATE_SIGNATURE_TABLE();
											
					# Generate Summary Table
					//debug_r($ResultArray);
					//hdebug_r($ResultArray);
					$SummaryResultArray = GET_SUMMARY_ARRAY($ResultArray, $HighestAverageArray[$ClassID], $StudentSummaryArray[$StudentID], $ColumnArray);
					//hdebug_r($SummaryResultArray);
					$ConductResultArray = GET_CONDUCT_ARRAY($ResultArray, $HighestAverageArray[$ClassID], $StudentSummaryArray[$StudentID], $ColumnArray);
					
					# Generate Attendance and Merit Table
					$AttendanceMeritTable = ($SemColumnExist==1) ? GENERATE_ATTENDANCE_AND_MERIT_TABLE($StudentSummaryArray[$StudentID], $StudentMeritArray[$StudentID]) : "";
					
					# Generate remark table
					$RemarkTable = GENERATE_REMARK_TABLE($ClassTeacherCommentArray[$StudentID], $StudentID);
					# Generate Misc Table
					$MiscTable = GENERATE_MISC_TABLE($ClassTeacherCommentArray[$StudentID], $StudentID);
					
					# Generate Details Table
					$ResultTable = GENERATE_RESULT_TABLE($SignatureTable, $AttendanceMeritTable, $ReportMarksheetArray[$StudentID], $SubjectTeacherCommentArray[$StudentID]);

					# Generate FooterRemarks Table
					$FooterRemarksTable = GENERATE_FOOTERREMARKS_TABLE();						

					################################################################
					
					$rx .= "<table width='95%' height='100%' border='0' cellpadding='2' cellspacing='1' align='center' style='page-break-after:always'>\n";
					$rx .= "<tr><td valign='top'>\n";
					$rx .= $TitleTable."\n";
					//$rx .= $StudentInfoTable."\n";
					$rx .= $ResultTable."\n";
					$rx .= "<div style='margin: 10px 0 0 0; padding:0 0 0 0; line-height:0px'>&nbsp;</div>".$FooterRemarksTable."\n";
					$rx .= "</td></tr>\n";
					$rx .= ($Footer!="") ? "<tr><td valign='bottom' class='small_text'>".str_replace("\n", "<br />", intranet_undo_htmlspecialchars($Footer))."</td></tr>\n" : "";
					$rx .= "</table>\n";
				}
			}
		}
	}
##########################################################################################

?>
<link href="report_image/tackching/css/contentstyle<?=$logo?>.css" rel="stylesheet" type="text/css">
<table border="0" cellpadding="4" width="100%" cellspacing="0" class="print_hide">
<tr><td align="right"><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></span></td></tr>
</table>
<?=$rx?>
<table border="0" cellpadding="4" width="100%" cellspacing="0" class="print_hide">
<tr><td align="right"><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></span></td></tr>
</table>

<?
	include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_footer.php");
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>
