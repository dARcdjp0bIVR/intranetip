<?php
$PATH_WRT_ROOT = "../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_header.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

echo "<script>if(typeof(opener)!='undefined') { opener.close(); }</script>";

if ($plugin['ReportCard'])
{
	include_once($PATH_WRT_ROOT."includes/libreportcard.php");
	include_once($PATH_WRT_ROOT."home/admin/reportcard/report_generation/print_report_functions4_revamp.php");
	
	$libreportcard = new libreportcard();	
	$CurrentPage = "ReportGeneration";
	if ($libreportcard->hasAccessRight())
    {
	    $linterface = new interface_html();
#######################################################################################################
		
		# get current Year and Semester
		$Year = trim($libreportcard->Year);
		$Semester = trim($libreportcard->Semester);

		# Get Report Info
		$ReportInfo = $libreportcard->GET_REPORT_TEMPLATE($ReportID);

		list($ReportTitle, $ReportType, $Description, $ShowFullMark, $Settings, $ReportStatus, $ShowOverallPosition, $OverallPositionRange, $HideNotEnrolled, $Footer, $NoHeader, $LineHeight, $SignatureWidth) = $ReportInfo;
		$SettingArray = unserialize($Settings);
		$BilingualReportTitle = $SettingArray["BilingualReportTitle"];

		# define the name of the variable array by checking the bilingual setting
		if($BilingualReportTitle==1)
		{
			$LangArray = $eReportCardBilingual;

			# prepare titles
			$DaysAbsentTitle = $LangArray["DaysAbsent"];
			$pos = strpos($DaysAbsentTitle, " ");
			$DaysAbsentTitle = substr_replace($DaysAbsentTitle, "<br />", $pos, 1);

			$TimesLateTitle = $LangArray["TimesLate"];
			$pos = strpos($TimesLateTitle, " ");
			$TimesLateTitle = substr_replace($TimesLateTitle, "<br />", $pos, 1);

			$AbsentWOLeaveTitle = $LangArray["AbsentWOLeave"];
			$pos = strpos($AbsentWOLeaveTitle, " ");
			$AbsentWOLeaveTitle = substr_replace($AbsentWOLeaveTitle, "<br />", $pos, 1);
		}
		else {
			$LangArray = $eReportCard;
		
			# prepare titles
			$DaysAbsentTitle = $LangArray["DaysAbsent"];
			$TimesLateTitle = $LangArray["TimesLate"];
			$AbsentWOLeaveTitle = $LangArray["AbsentWOLeave"];
		}
		
		# Get Class Level(s) of the report
		if($ClassLevelID!="")
			$SelectedFormArr = array($ClassLevelID);
		else
			$SelectedFormArr = $libreportcard->GET_REPORT_TEMPLATE_FORM($ReportID);

		# Get Class Student(s)
		$LevelList = implode(",", $SelectedFormArr);
		
		$TargetStudentList = (is_array($TargetStudentID)) ? implode(",", $TargetStudentID) : "";

		list($ClassStudentArray, $StudentIDArray) = $libreportcard->GET_STUDENT_BY_CLASSLEVEL($ReportID, $LevelList, $ClassID, $TargetStudentList);
		
		#### Calculate Position ####
		list($AllClassStudentArray, $AllStudentIDArray) = $libreportcard->GET_STUDENT_BY_CLASSLEVEL($ReportID, $LevelList, $ClassID, "");

		# Get Class Teacher Comment
		$ClassTeacherCommentArray = $libreportcard->GET_REPORT_CLASS_TEACHER_COMMENT($LevelList, $ClassID, $TargetStudentList);
		
		# Get Subject Teacher Comment
		$SubjectTeacherCommentArray = $libreportcard->GET_REPORT_SUBJECT_TEACHER_COMMENT($LevelList, $ClassID, $TargetStudentList);

		# Get Class Teacher(s)
		$ClassTeacherArray = $libreportcard->GET_CLASSTEACHER_BY_CLASSLEVEL($LevelList);
		
		################################## !!!Important!!! ##################################
		# Defined in /lang/reportcard_lang.en.php also
		# To disable (not displaying) any part in "Misc", remove the array item
		$eReportCard['DisplaySettingsArray']["Misc"] = array("ClassTeacherComment", "SubjectTeacherComment", "Awards", "MeritsAndDemerits");
		#####################################################################################
		
		$StudentInfoTitleArray = $eReportCard['DisplaySettingsArray']["StudentInfo"];
		$SettingStudentInfo = $SettingArray["StudentInfo"];

		$MiscTitleArray = $eReportCard['DisplaySettingsArray']["Misc"];
		$SettingMiscArray = $SettingArray["Misc"];
		$ShowSubjectTeacherComment = $SettingMiscArray["SubjectTeacherComment"];

		$SummaryTitleArray = $eReportCard['DisplaySettingsArray']["Summary"];
		$SettingSummaryArray = $SettingArray["Summary"];
		
		################# New Appraoch Start ################
		include_once("../default_header_revamp.php");
		
		for($i=0; $i<sizeof($DefaultHeaderArray["summary"]); $i++) {
			$tmpSettingTitle = $HeaderSettingMap["summary"][$DefaultHeaderArray["summary"][$i]];
			$SettingSummaryArray[$tmpSettingTitle] = 1;
		}
		
		if (isset($DefaultHeaderArray["award"]))
			$SettingMiscArray["Awards"] = 1;
		if (isset($DefaultHeaderArray["merit"]))
			$SettingMiscArray["MeritsAndDemerits"] = 1;
		if (isset($DefaultHeaderArray["eca"]))
			$SettingMiscArray["ECA"] = 1;
		if (isset($DefaultHeaderArray["remark"]))
			$SettingMiscArray["Remark"] = 1;
		if (isset($DefaultHeaderArray["interschool"]))
			$SettingMiscArray["InterSchoolCompetition"] = 1;
		if (isset($DefaultHeaderArray["schoolservice"]))
			$SettingMiscArray["SchoolService"] = 1;
    ################## New Appraoch End #################
		
		### add merit to display detail (aki)
		if ($SettingMiscArray["MeritsAndDemerits"]==1) {
			$SettingSummaryArray['Merit'] = 1;
			$SummaryTitleArray[] = "Merit";		
		}
		if ($SettingMiscArray["MeritsAndDemerits"]==1) {
			$SettingSummaryArray['Demerit'] = 1;			
			$SummaryTitleArray[] = "Demerit";					
		}
		if ($SettingMiscArray["MinorCredit"]==1) {
			$SettingSummaryArray['MinorCredit'] = 1;		
			$SummaryTitleArray[] = "MinorCredit";			
		}
		if ($SettingMiscArray["MajorCredit"]==1) {
			$SettingSummaryArray['MajorCredit'] = 1;		
			$SummaryTitleArray[] = "MajorCredit";			
		}
		if ($SettingMiscArray["MinorFault"]==1) {
			$SettingSummaryArray['MinorFault'] = 1;		
			$SummaryTitleArray[] = "MinorFault";			
		}
		if ($SettingMiscArray["MajorFault"]==1) {
			$SettingSummaryArray['MajorFault'] = 1;		
			$SummaryTitleArray[] = "MajorFault";			
		}
		
		
		# Get Report Subject and Column
		$SubjectArray = $libreportcard->GET_REPORT_TEMPLATE_SUBJECT($ReportID);
		$ColumnArray = $libreportcard->GET_REPORT_TEMPLATE_COLUMN($ReportID);
		
		# Get Report Cell Setting
		$ReportCellSetting = $libreportcard->GET_REPORT_CELL($ReportID);
		
		# Get All subject array
		$AllSubjectArray = $libreportcard->GET_ALL_SUBJECTS();

		# Get Report Marksheet result
		$SubjectIDArray = $libreportcard->GET_REPORT_SELECTED_SUBJECT_ID($ReportID);
		if(is_array($SubjectIDArray) && is_array($StudentIDArray))
		{
			$SubjectIDList = implode(",", $SubjectIDArray);
			$StudentIDList = implode(",", $StudentIDArray);
			$ReportMarksheetArray = $libreportcard->GET_REPORT_MARKSHEET_SCORES($SubjectIDList, $StudentIDList);
		}
		
		# Get Grademark scheme of subjects
		$SubjectGradingArray = $libreportcard->GET_REPORT_SUBJECT_GRADING($ReportID, $SelectedFormArr);
		if(is_array($SubjectGradingArray))
		{
			$SubjectGradingList = implode(",", $SubjectGradingArray);
			list($FailedArray, $DistinctionArray, $PFArray) = $libreportcard->GET_REPORT_GRADEMARK($SubjectGradingList);
		}

		# Get Highest Average in Class
		$HighestAverageArray = $libreportcard->GET_CLASS_HIGHEST_AVERAGE($LevelList);
		
		# Page Break Style
		$page_break_here = "<p class='breakhere'></p>\n";
		
		$lf = new libfilesystem();
		$SemesterArray = $libreportcard->GET_SEMESTER_ARRAY();
		$SemColumnExist = 0;
		for($i=0; $i<sizeof($ColumnArray); $i++)
		{
			$ColumnTitle = trim($ColumnArray[$i]["ColumnTitle"]);
			if(in_array($ColumnTitle, $SemesterArray))
			{
				//$ColumnSemesterArray[] = $ColumnTitle;
				$SemColumnExist = 1;
				break;
			}
		}

		$StudentRegNoArray = $libreportcard->GET_STUDENT_REGNO_ARRAY($LevelList, $ClassID, $TargetStudentList);
		#####################################################################
		# get summary data from csv file
		
		$SummaryHeader = $DefaultHeaderArray["summary"];
		$TargetSummaryFile = $intranet_root."/file/reportcard/summary";
		$StudentSummaryArray = GET_CSV_FILE_CONTENT($TargetSummaryFile, $SummaryHeader, "", "summary");
		#####################################################################
		
		#####################################################################
		# get award data from csv file
		
		$AwardHeader = $DefaultHeaderArray["award"];
		$TargetAwardFile = $intranet_root."/file/reportcard/award";
		$StudentAwardArray = GET_CSV_FILE_CONTENT($TargetAwardFile, $AwardHeader, "", "award");
		
		$SemStudentAwardArray = GET_CSV_FILE_CONTENT($TargetAwardFile, $AwardHeader, $commentSemester, "awards");
		#####################################################################

		#####################################################################
		# get merit data from csv file
		
		$MeritHeader = $DefaultHeaderArray["merit"];
		$TargetMeritFile = $intranet_root."/file/reportcard/merit";
		$StudentMeritArray = GET_CSV_FILE_CONTENT($TargetMeritFile, $MeritHeader, "", "merit");
		#####################################################################
		
		#####################################################################
		# get eca data from csv file
		
		$ECAHeader = $DefaultHeaderArray["eca"];
		$TargetECAFile = $intranet_root."/file/reportcard/eca";
		$StudentECAArray = GET_CSV_FILE_CONTENT($TargetECAFile, $ECAHeader, "", "eca");
		$ECAResultArray = GET_ECA_ARRAY($StudentECAArray);		
		#####################################################################

		#####################################################################
		# get remark data from csv file
		
		$RemarkHeader = $DefaultHeaderArray["remark"];
		$TargetRemarkFile = $intranet_root."/file/reportcard/remark";
		$StudentRemarkArray = GET_CSV_FILE_CONTENT($TargetRemarkFile, $RemarkHeader, "", "remark");
		
		$SemStudentRemarkArray = GET_CSV_FILE_CONTENT($TargetRemarkFile, $RemarkHeader, $commentSemester, "remark");
		#####################################################################

		#####################################################################
		# get inter school competition data from csv file
		
		$InterSchoolHeader = $DefaultHeaderArray["interschool"];
		$TargetInterSchoolFile = $intranet_root."/file/reportcard/interschool";
		$StudentInterSchoolArray = GET_CSV_FILE_CONTENT($TargetInterSchoolFile, $InterSchoolHeader, "", "interschool");
		#####################################################################
		
		#####################################################################
		# get school service data from csv file
		
		$SchoolServiceHeader = $DefaultHeaderArray["schoolservice"];
		$TargetSchoolServiceFile = $intranet_root."/file/reportcard/schoolservice";
		$StudentSchoolServiceArray = GET_CSV_FILE_CONTENT($TargetSchoolServiceFile, $SchoolServiceHeader, "", "schoolservice");
		#####################################################################
		
		# get issue date
		$IssueDate = $libreportcard->GET_ISSUE_DATE($ReportID);
		
#######################################################################################################
		//array to calculate column summmary "rank"
		//$TempGrandTotal = array();
		
		if(is_array($ClassStudentArray))
		{
			foreach($ClassStudentArray as $ClassID => $StudentArray)
			{
				if(is_array($StudentArray))
				{
					foreach($StudentArray as $StudentID => $InfoArray)
					{
						$UserInfoArray = $InfoArray["UserInfo"];
						
						$ResultArray = $InfoArray["Result"];
						if(empty($ResultArray))
							continue;
								
						//hdebug_r($ResultArray);
											
						$PhotoLink = $libreportcard->GET_USER_PHOTO($StudentID);
						$AcademicYear = $libreportcard->GET_ACADEMIC_YEAR($ReportID);
						
						# Generate Report
						# Generate Title Table		
						if ($NoHeader == -1) {
							$TitleTable = GENERATE_TITLE_TABLE($ReportTitle);
						} else {
							$TitleTable = "";
							for ($i = 0; $i < $NoHeader; $i++) {
								$TitleTable .= "<br/>";
							}
						}
						
						################################################################
						# Generate Signature Table
						$SignatureTable = GENERATE_SIGNATURE_TABLE();

						# Generate Student Info Table
						$StudentInfoTable = GENERATE_STUDENT_INFO_TABLE($UserInfoArray);
						
						# Generate Summary Table
						$MeritResultArray = GET_MERIT_ARRAY($ResultArray, $HighestAverageArray[$ClassID], $StudentSummaryArray[$StudentID], $ColumnArray);
						$SummaryResultArray = GET_SUMMARY_ARRAY($ResultArray, $HighestAverageArray[$ClassID], $StudentSummaryArray[$StudentID], $ColumnArray);
						
						# Generate Attendance and Merit Table
						$AttendanceMeritTable = ($SemColumnExist==1) ? GENERATE_ATTENDANCE_AND_MERIT_TABLE($StudentSummaryArray[$StudentID], $StudentMeritArray[$StudentID]) : "";
						
						# Generate Details Table
						$ResultTable = GENERATE_RESULT_TABLE($SignatureTable, $AttendanceMeritTable, $ReportMarksheetArray[$StudentID], $SubjectTeacherCommentArray[$StudentID]);

						# Generate Misc Table
						//$MiscTable = GENERATE_MISC_TABLE($ClassTeacherCommentArray[$StudentID], $StudentID);

						################################################################
						
						$rx .= "<table width='100%' height='100%' border=0 cellpadding=1 cellspacing=1 align='center' style='page-break-after:always'>\n";
						$rx .= "<tr><td valign='top'>\n";
						$rx .= $TitleTable."\n";
						$rx .= $StudentInfoTable."\n";
						$rx .= $ResultTable."\n";						
						//$rx .= $MiscTable."\n";
						$rx .= $SignatureTable."\n";
						$rx .= "</td></tr>\n";
						$rx .= ($Footer!="") ? "<tr><td valign='bottom' class='small_text'>".str_replace("\n", "<br />", intranet_undo_htmlspecialchars($Footer))."</td></tr>\n" : "";
						$rx .= "</table>\n";
						$rx .= "<table border='0' cellpadding='0' width='100%' cellspacing='0' class='print_hide'>";
						$rx .= "<tr><td height='20'></td></tr></table>";
					}
					/*
					list($op_from, $op_to) = explode(",", $OverallPositionRange);
					
					if (is_array($TempGrandTotal))
					{
						$rx .= "\n<script language='JavaScript'>\n";
						while(list($ReportColumnID, $TempStudentMark) = each($TempGrandTotal)) {
							arsort($TempStudentMark);
							$RankCount = 1;
							if (is_array($TempStudentMark))
							while(list($TempStudentID, $TempMark) = each($TempStudentMark)) {
								$TempRank[$ReportColumnID][$TempStudentID] = $RankCount++;
								if (!($TempRank[$ReportColumnID][$TempStudentID]>=$op_from && $TempRank[$ReportColumnID][$TempStudentID]<=$op_to))
									$TempRank[$ReportColumnID][$TempStudentID] = "----";
								$IDField = "Rank".$ReportColumnID."_".$TempStudentID;
								$rx .= "\t if (document.getElementById('$IDField') != null) document.getElementById('$IDField').innerHTML = '".$TempRank[$ReportColumnID][$TempStudentID]."'\n";
							}
						}
						$rx .= "</script>\n";
					}
					*/
				}
			}
		}
		
		#### Calculate Position ####
		$TempGrandTotalArr = array();
		//if (($ReportID != 18)&&($ReportID != 19))
		if (true)
		{
			if(is_array($AllClassStudentArray))
			{
				foreach($AllClassStudentArray as $ClassID => $StudentArray)
				{
					if(is_array($StudentArray))
					{
						foreach($StudentArray as $StudentID => $InfoArray)
						{						
							$ResultArray = $InfoArray["Result"];
							if(empty($ResultArray))
								continue;
	
							$SummaryResultArray = GET_SUMMARY_ARRAY($ResultArray, $HighestAverageArray[$ClassID], $StudentSummaryArray[$StudentID], $ColumnArray);
							CAL_POSITION();
						}
	
						list($op_from, $op_to) = explode(",", $OverallPositionRange);
						if (is_array($TempGrandTotalArr))
						{
							$rx .= "\n<script language='JavaScript'>\n";
							while(list($ReportColumnID, $TempStudentMark) = each($TempGrandTotalArr)) {
								arsort($TempStudentMark);
								$IsFirst = true;
								$PreviousMark = 999;
								$RankCount = 0;
								if (is_array($TempStudentMark))
								while(list($TempStudentID, $TempMark) = each($TempStudentMark)) {
									if ($PreviousMark == $TempMark) {
										$TempRank[$ReportColumnID][$TempStudentID] = $RankCount;
									} else {
										$TempRank[$ReportColumnID][$TempStudentID] = ++$RankCount;
									}
									if (!($TempRank[$ReportColumnID][$TempStudentID]>=$op_from && $TempRank[$ReportColumnID][$TempStudentID]<=$op_to))
										$TempRank[$ReportColumnID][$TempStudentID] = "----";
									$IDField = "Rank".$ReportColumnID."_".$TempStudentID;
									$rx .= "\t if (document.getElementById('$IDField') != null) document.getElementById('$IDField').innerHTML = '".$TempRank[$ReportColumnID][$TempStudentID]."'\n";
									$PreviousMark = $TempMark;
								}
							}
							$rx .= "</script>\n";
						}
					}
				}
			}
		}
		
##########################################################################################

?>
<link href="report_image/EscolaTongNam/css/contentstyle.css" rel="stylesheet" type="text/css">
<table border="0" cellpadding="0" width="100%" cellspacing="0" class="print_hide">
<tr><td align="right"><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></span></td></tr>
</table>
<?=$rx?>
<table border="0" cellpadding="0" width="100%" cellspacing="0" class="print_hide">
<tr><td align="right"><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></span></td></tr>
</table>

<?
	include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_footer.php");
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>