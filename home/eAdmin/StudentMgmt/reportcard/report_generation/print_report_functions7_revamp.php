<?php
############################ Function Start ######################################

# function to get Title Table
function GENERATE_TITLE_TABLE()
{
	global $ReportTitle, $image_path, $intranet_root, $lf;

	# get school badge
	//$SchoolLogo = GET_SCHOOL_BADGE();

	//$imgfile = get_file_content($intranet_root."/file/schoolbadge.txt");
	//$SchoolLogo = ($imgfile != "") ? "<img src=\"/file/{$imgfile}\">\n" : "";

	$SchoolLogo = "<img src=\"report_image/fatho/logo.jpg\">\n";

	# get school name
	$SchoolName = GET_SCHOOL_NAME();

	$TitleTable = "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>\n";
	$TitleTable .= "<tr><td width='120' align='center'>".($SchoolLogo==""?"&nbsp;":$SchoolLogo)."</td>";
	if(!empty($ReportTitle) || !empty($SchoolName))
	{
		list($ReportTitle1, $ReportTitle2) = explode("::", $ReportTitle);

		$TitleTable .= "<td>";
		$TitleTable .= "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>\n";
		//if(!empty($SchoolName))
		$TitleTable .= "<tr><td nowrap='nowrap' class='report_title' align='center'>Buddhist Fat Ho Memorial College</td></tr>\n";
		$TitleTable .= "<tr><td nowrap='nowrap' class='report_title' align='center'>佛教筏可紀念中學</td></tr>\n";

		if(!empty($ReportTitle1))
			$TitleTable .= "<tr><td nowrap='nowrap' class='report_title' align='center'>".($ReportTitle1)."</td></tr>\n";
		if(!empty($ReportTitle2))
			$TitleTable .= "<tr><td nowrap='nowrap' class='report_title' align='center' colspan='2'>".($ReportTitle2)."</td></tr>\n";
		$TitleTable .= "</table>\n";
		$TitleTable .= "</td>";
	}
	//$TitleTable .= "<td width='120' align='center'>&nbsp;</td></tr>";
	$TitleTable .= "</table>";

	return $TitleTable;
}

# function to get Student Info Table
function GENERATE_STUDENT_INFO_TABLE($ParInfoArray)
{
	global $SettingStudentInfo, $StudentInfoTitleArray, $ClassTeacherArray, $IssueDate, $LangArray;
	global $EngReportCard, $ChiReportCard, $StudentRemarkArray, $StudentID, $Semester, $AcademicYear;

	list($ClassName, $ClassNumber, $StudentName, $RegNo, $ChineseName, $EnglishName, $DOB, $Gender) = $ParInfoArray;

	if(!empty($SettingStudentInfo))
	{
		$count = 0;
		$StudentInfoTable = "<table width='100%' border='0' cellpadding='1' cellspacing='0' align='center'>\n";
		for($i=0; $i<sizeof($StudentInfoTitleArray); $i++)
		{
			$SettingID = trim($StudentInfoTitleArray[$i]);
			if($SettingStudentInfo[$SettingID]==1 && $SettingID!="ClassNumber" && $SettingID!="DateOfIssue")
			{
				//$Title = $LangArray[$SettingID];
				$Title = $EngReportCard[$SettingID]." ".$ChiReportCard[$SettingID];
				//$Title = str_replace("<br />", " ", $Title);
				$Display = "";
				switch($SettingID)
				{
					case "Name":
						$Display = $StudentName;
						break;
					case "ClassName":
						$Display = ($SettingStudentInfo["ClassNumber"]==1 && $ClassNumber!="") ? $ClassName." (".$ClassNumber.") " : $ClassName;
						break;
					case "ClassTeacher":
						$Display = (!empty($ClassTeacherArray[$ClassName])) ? implode(", ", $ClassTeacherArray[$ClassName]) : "";
						break;
					case "DateOfIssue":
						$Display = $IssueDate;
						break;
					case "StudentNo":
						$Display = $RegNo;
						break;
					case "DateOfBirth":
						$Display = date('d/m/Y', strtotime($DOB));
						break;
					case "AcademicYear":
						$Display = $AcademicYear;
						break;
					case "Gender":
						$Display = $Gender;
						break;
				}
				//if (($count % 4) == 0) $StudentInfoTable .= "<tr>";
				//$StudentInfoTable .= "<td class='tabletext' valign='top'>".$Title." : ".$Display."</td>\n";
				/*
				if ($SettingID == "ClassName")
				{
					$StudentInfoTable .= "<td class='tabletext' valign='top'>HKID No. 身份証號碼 : ".$StudentRemarkArray[$StudentID]['FULL'][0]['Remark']."</td>\n";
					$count++;
				}
				*/
				//if (($count % 4) == 3) $StudentInfoTable .= "</tr>";
				//$count++;
			}
		}
		
		$StudentInfoTable .= "<tr>";
		
		$StudentInfoTable .= "<td class='tabletext' valign='top' colspan='3'>";
		if($SettingStudentInfo['Name']) {
			$StudentInfoTable .= $EngReportCard['Name']." ".$ChiReportCard['Name']." : ".$StudentName;
		}
		$StudentInfoTable .= "&nbsp;</td>\n";		
		$StudentInfoTable .= "</tr>";
		
		$StudentInfoTable .= "<tr>";
		$StudentInfoTable .= "<td class='tabletext' valign='top'>";
		if($SettingStudentInfo['Gender']) {
			$StudentInfoTable .= $EngReportCard['Gender']." ".$ChiReportCard['Gender']." : ".$Gender;
		}
		$StudentInfoTable .= "&nbsp;</td>\n";
		
		$StudentInfoTable .= "<td class='tabletext' valign='top' width='25%'>";
		if($SettingStudentInfo['ClassName']) {
			$Display = ($SettingStudentInfo["ClassNumber"]==1 && $ClassNumber!="") ? $ClassName." (".$ClassNumber.") " : $ClassName;
			$StudentInfoTable .= $EngReportCard['ClassName']." ".$ChiReportCard['ClassName']." : ".$Display;
		}
		$StudentInfoTable .= "&nbsp;</td>\n";
		
		$StudentInfoTable .= "<td class='tabletext' valign='top' width='25%'>";
		if($SettingStudentInfo['StudentNo']) {
			$StudentInfoTable .= $EngReportCard['StudentNo']." ".$ChiReportCard['StudentNo']." : ".$RegNo;
		}
		$StudentInfoTable .= "&nbsp;</td>\n";
		
		/*
		$StudentInfoTable .= "<td class='tabletext' valign='top'>";
		if($SettingStudentInfo['ClassTeacher']) {
			$Display = (!empty($ClassTeacherArray[$ClassName])) ? implode(", ", $ClassTeacherArray[$ClassName]) : "";
			$StudentInfoTable .= $EngReportCard['ClassTeacher']." ".$ChiReportCard['ClassTeacher']." : ".$Display;
		}
		$StudentInfoTable .= "&nbsp;</td>\n";
		
		$StudentInfoTable .= "<td class='tabletext' valign='top'>";
		if($SettingStudentInfo['DateOfBirth']) {
			$StudentInfoTable .= $EngReportCard['DateOfBirth']." ".$ChiReportCard['DateOfBirth']." : ".date('d/m/Y', strtotime($DOB));
		}
		$StudentInfoTable .= "&nbsp;</td>\n";
		
		$StudentInfoTable .= "<td class='tabletext' valign='top'>";
		if($SettingStudentInfo['AcademicYear']) {
			$StudentInfoTable .= $EngReportCard['AcademicYear']." ".$ChiReportCard['AcademicYear']." : ".$AcademicYear;
		}
		$StudentInfoTable .= "&nbsp;</td>\n";
		*/
		$StudentInfoTable .= "</tr>";
	}

	return $StudentInfoTable;
}

# Generate a simple cell for holding data
function GENERATE_MISC_CELL($title, $arr, $content="") {
	if ($content == "" && $arr != "") {
		$content = "<ul>";
		for($i=0; $i<sizeof($arr); $i++) {
			$content .= "<li>".$arr[$i][0]."</li>";
		}
		$content .= "</ul>";
	}
	
	$x .= "<td width='50%' class='report_border' valign='top'>";
	$x .= "<table width='100%' border='0' cellpadding='2' cellspacing='0' align='center'>";
	$x .= "<tr><td class='small_title'>".$title."</td></tr>";
	$x .= "<tr><td class='small_text' valign='top' height='80'>".$content."</td></tr>";
	$x .= "</table>";
	$x .= "</td>";
	return $x;
}

# Generate the Merit data
function GENERATE_MERIT_CONTENT($meritArr) {
	global $HeaderSettingMap, $EngReportCard, $ChiReportCard;
	
	$TempCount = 0;
	$x = "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center' valign='top'>";
	// a student have 1 row to represent his/her record
	foreach ($meritArr[0] as $key => $value) {
		if ($key !="" && in_array($key, $HeaderSettingMap["merit"])) {
			//$x .= $EngReportCard[$key]." ".$ChiReportCard[$key].": ".$value."<br>";
			
			if ($TempCount%3==0) $x .= "<tr>";
			$CountTitle = $EngReportCard[$key]." ".$ChiReportCard[$key];
			$DisplayContent = ($value == "") ? "--" : $value;
			$x .= "<td class='tabletext'>".$CountTitle.": ".$DisplayContent."</td>";
			if ($TempCount%3==2) $x .= "</tr>";
			$TempCount++;
		}
	}	
	$x .= "</table>";
	
	return $x;
}

# Generate the ECA data
function GENERATE_ECA_CONTENT($ecaArr) {
	global $HeaderSettingMap, $EngReportCard, $ChiReportCard;
	$x = "<ul>";
	for($i=0; $i<sizeof($ecaArr); $i++) {
		$x .= "<li>".$ecaArr[$i]["ECA"];
		if (isset($ecaArr[$i]["Grade"])) {
			$x .= " (".$ecaArr[$i]["Grade"].") </li>";
		}
	}
	$x .= "</ul>";
	return $x;
}

# Generate the ECA data
function GENERATE_NO_LI_CONTENT($ParArr, $ParTitle) {
	global $HeaderSettingMap, $EngReportCard, $ChiReportCard;
	
	$x = "";
	for($i=0; $i<sizeof($ParArr); $i++) {
		$x .= $ParArr[$i][$ParTitle];
		if (($i+1) < sizeof($ParArr)) $x .= "<br>";
	}
	
	return $x;
}

# function to get MISC Table
function GENERATE_MISC_TABLE($ParClassTeacherComment, $ParStudentID)
{
	global $SettingMiscArray, $MiscTitleArray, $Semester, $LangArray;
	global $StudentAwardArray, $StudentMeritArray, $StudentECAArray, $StudentRemarkArray;
	global $image_path, $LAYOUT_SKIN;
	global $StudentInterSchoolArray, $StudentSchoolServiceArray, $Semester;
	global $EngReportCard, $ChiReportCard;

	$ParStudentID = trim($ParStudentID);
	if(!empty($SettingMiscArray))
	{
		$count = 0;
		$MiscTable = "<br /><table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'><tr>";

		unset($MiscTitleArray);
		//if ($Semester == "FULL")
		if (true)
		{
			if($SettingMiscArray['MeritsAndDemerits']) $MiscTitleArray[] = "MeritsAndDemerits";
			if($SettingMiscArray['Remark']) $MiscTitleArray[] = "Remark";			
			if($SettingMiscArray['ClassTeacherComment']) $MiscTitleArray[] = "ClassTeacherComment";
			if($SettingMiscArray['ECA']) $MiscTitleArray[] = "ECA";
			
			$RemarkContent = "參閱個人其他學習經歷概覽";
			$RemarkContent .= "<br>Refer to Student Portfolio";
			/*
			$RemarkContent = "*方格中前者為考生所考獲分數，括號中為滿分分數<br>";
			$RemarkContent .= "<table border='0' cellpadding='0' cellspacing='0'>";
			$RemarkContent .= "<tr class='tabletext'><td>#Reading (English) 英文閱讀</td><td width='20'>&nbsp;</td><td>DS-Distinction (優良)</td></tr>";
			$RemarkContent .= "<tr class='tabletext'><td>Reading (Chinese) 中文閱讀</td><td>&nbsp;</td><td>PS-Pass (及格)</td></tr>";
			$RemarkContent .= "<tr class='tabletext'><td>Online Learning</td><td>&nbsp;</td><td>NI-Need Improvement (有待改善)</td></tr>";
			$RemarkContent .= "</table>";
			*/

			/*
			for($i=0; $i<sizeof($MiscTitleArray); $i++)
			{
				$SettingID = trim($MiscTitleArray[$i]);
				if($SettingMiscArray[$SettingID]==1 && $SettingID!="SubjectTeacherComment")
				{
					//$Title = $LangArray[$SettingID];
					//$Title = str_replace("<br />", " ", $Title);
					$Title = $EngReportCard[$SettingID]." ".$ChiReportCard[$SettingID];
					$Display = "";
					switch($SettingID)
					{
						case "ClassTeacherComment":
							$Display = "<li>".$ParClassTeacherComment."</li>";
							//$Display = $ParClassTeacherComment;
							break;
						case "Awards":
							$Display = GENERATE_MISC_CELL($Title, $StudentAwardArray[$ParStudentID][$Semester]);
							break;
						case "MeritsAndDemerits":
							$Display = $StudentMeritArray[$ParStudentID][$Semester];
							break;
						case "ECA":
							//$Display = $StudentECAArray[$ParStudentID][$Semester];
							for ($Tempi = 0; $Tempi < sizeof($StudentECAArray[$ParStudentID][$Semester]); $Tempi+=2)
							{
								$Display .= $StudentECAArray[$ParStudentID][$Semester][$Tempi]."<br>";
							}
							break;
						case "Remark":
							$Display = $StudentRemarkArray[$ParStudentID][$Semester];
							break;
						case "InterSchoolCompetition":
							$Display = $StudentInterSchoolArray[$ParStudentID][$Semester];
							break;
						case "SchoolService":
							$Display = $StudentSchoolServiceArray[$ParStudentID][$Semester];
							break;
					}
					$count++;
				}
			}
			*/
			
			/*
			$MiscTable .= "<tr><td width='50%' class='report_border' colspan='3'>";
			$MiscTable .= "<table width='100%' border='0' cellpadding='1' cellspacing='0' align='center'>
							<tr><td class='small_title'>".$EngReportCard['Awards']." ".$ChiReportCard['Awards']."</td></tr>
							<tr><td class='small_text' valign='top' height='40'>".GENERATE_NO_LI_CONTENT($StudentAwardArray[$ParStudentID][$Semester])."</td></tr>
							</table>";
			$MiscTable .= "</td></tr>";
			*/
			
			for($i=0; $i<sizeof($MiscTitleArray); $i++)
			{
				$SettingID = trim($MiscTitleArray[$i]);
				if($SettingMiscArray[$SettingID]==1 && $SettingID!="SubjectTeacherComment")
				{
					$Title = $EngReportCard[$SettingID]." ".$ChiReportCard[$SettingID];
					switch ($SettingID) {
						case "MeritsAndDemerits":
								$Display = GENERATE_MERIT_CONTENT($StudentMeritArray[$ParStudentID][$Semester]);
								break;
						case "Remark":
								$Display = GENERATE_NO_LI_CONTENT($StudentRemarkArray[$ParStudentID][$Semester], "Remark");
								break;
						case "ClassTeacherComment":
								$Display = $ParClassTeacherComment;
								break;
						case "ECA":
								$Display = "";
								for ($Tempi = 0; $Tempi < sizeof($StudentECAArray[$ParStudentID][$Semester]); $Tempi++)
								{
									$Display .= $StudentECAArray[$ParStudentID][$Semester][$Tempi]['ECA']."<br>";
								}
								break;
					}
				}
				if($i%2==0) {
					$MiscTable .= "<tr>";
				}
				
				$MiscTable .= "<td width='49%' class='report_border' valign='top'>";
				$MiscTable .= "<table width='100%' border='0' cellpadding='1' cellspacing='0' align='center'>
								<tr><td class='small_title'>".$Title."</td></tr>
								<tr><td class='small_text' valign='top' height='40'>".$Display."</td></tr>
								</table>";
				$MiscTable .= "</td>";
			
				if($i%2==0) {
					$MiscTable .= "<td width='1%'>&nbsp;</td>";
				}
				if($i%2==1) {
					$MiscTable .= "</tr>";
					$MiscTable .= "<tr><td colspan='3' height='13'></td></tr>";
				}
			}
		
			
			/*
			$MiscTable .= "<tr>";
			$MiscTable .= "<td width='49%' class='report_border' valign='top'>";
			$MiscTable .= "<table width='100%' border='0' cellpadding='1' cellspacing='0' align='center'>
							<tr><td class='small_title'>".$EngReportCard['Awards']." ".$ChiReportCard['Awards']."</td></tr>
							<tr><td class='small_text' valign='top' height='40'>".GENERATE_MERIT_CONTENT($StudentMeritArray[$ParStudentID][$Semester])."</td></tr>
							</table>";
			$MiscTable .= "</td>";
			$MiscTable .= "<td width='1%'>&nbsp;";
			$MiscTable .= "</td>";
			$MiscTable .= "<td width='49%' class='report_border' valign='top'>";
			$MiscTable .= "<table width='100%' border='0' cellpadding='1' cellspacing='0' align='center'>
							<tr><td class='small_title'>".$EngReportCard['Remark']." ".$ChiReportCard['Remark']."</td></tr>
							<tr><td class='small_text' valign='top' height='40'>".GENERATE_NO_LI_CONTENT($StudentRemarkArray[$ParStudentID][$Semester], "Remark")."</td></tr>
							</table>";
			$MiscTable .= "</td>";
			$MiscTable .= "</tr>";
			
			
			$MiscTable .= "<tr><td colspan='3' height='13'></td></tr>";
			
			$MiscTable .= "<tr>";
			$MiscTable .= "<td width='49%' class='report_border' valign='top'>";
			$MiscTable .= "<table width='100%' border='0' cellpadding='1' cellspacing='0' align='center'>
							<tr><td class='small_title'>".$EngReportCard['ClassTeacherComment']." ".$ChiReportCard['ClassTeacherComment']."</td></tr>
							<tr><td class='small_text' valign='top' height='40'>".$ParClassTeacherComment."</td></tr>
							</table>";
			$MiscTable .= "</td>";
			$MiscTable .= "<td width='1%'>&nbsp;</td>";						
			$MiscTable .= "<td width='49%' class='report_border'>";
			$Display = "";
			for ($Tempi = 0; $Tempi < sizeof($StudentECAArray[$ParStudentID][$Semester]); $Tempi++)
			{
				$Display .= $StudentECAArray[$ParStudentID][$Semester][$Tempi]['ECA']."<br>";
			}
			$MiscTable .= "<table width='100%' border='0' cellpadding='1' cellspacing='0' align='center'>
							<tr><td class='small_title'>".$EngReportCard['ECA']." ".$ChiReportCard['ECA']."</td></tr>
							<tr><td class='small_text' valign='top' height='40'>".$Display."</td></tr>
							</table>";
			//$RemarkContent
			$MiscTable .= "</td>";

			$MiscTable .= "</tr>";
			*/
			$MiscTable .= "</table>";
			

		}


	}

	return $MiscTable;
}


function GET_COLUMN_FULLMARK($FullMark, $ReportColumnID, $ColumnWeight, $ColumnSummaryWeight){

	if (($FullMark+0)<=0)
	{
		$rx = $FullMark;
	} elseif ($ColumnSummaryWeight[$ReportColumnID]>0)
	{
		$rx = ($ColumnSummaryWeight[$ReportColumnID]*$FullMark/100);
	} elseif ($ColumnWeight[$ReportColumnID]>0)
	{
		$rx = ($ColumnWeight[$ReportColumnID]*$FullMark/100);
	} else
	{
		$rx = $FullMark;
	}

	return $rx;
}

# function to get summary table
function GENERATE_SUMMARY_TABLE($ParInfoArray, $ClassHighestAverage, $ParSummaryArray)
{
	global $SummaryTitleArray, $SettingSummaryArray, $Semester, $LangArray;
	global $LineHeight;

	list($GT, $AM, $GPA, $OMC, $OMCT, $OMF, $OMFT) = $ParInfoArray[$Semester];

	if(!empty($SettingSummaryArray))
	{
		$count = 0;
		$SummaryTable = "<table width='100%' border='1' cellpadding='1' cellspacing='0' align='center'>";
		for($k=0; $k<sizeof($SummaryTitleArray); $k++)
		{
			$SettingID = trim($SummaryTitleArray[$k]);
			if($SettingSummaryArray[$SettingID]==1)
			{
				$Title = $LangArray[$SettingID];
				switch($SettingID)
				{
					case "GrandTotal":
						$Display = $GT;
						break;
					case "GPA":
						$Display = $GPA;
						break;
					case "AverageMark":
						$Display = $AM;
						break;
					case "FormPosition":
						$Display = ($OMF>0) ? $OMF : "--";
						break;
					case "ClassPosition":
						$Display = ($OMC>0) ? $OMC : "--";
						break;
					case "ClassPupilNumber":
						$Display = $OMCT;
						break;
					case "ClassHighestAverage":
						$Display = $ClassHighestAverage[$Semester];
						break;
					case "Conduct":
						$Display = ($ParSummaryArray[$Semester][0]["Conduct"]==""?"--":$ParSummaryArray[$Semester][0]["Conduct"]);
						break;
					case "Politeness":
						$Display = ($ParSummaryArray[$Semester][0]["Politeness"]==""?"--":$ParSummaryArray[$Semester][0]["Politeness"]);
						break;
					case "Behaviour":
						$Display = ($ParSummaryArray[$Semester][0]["Behaviour"]==""?"--":$ParSummaryArray[$Semester][0]["Behaviour"]);
						break;
					case "Application":
						$Display = ($ParSummaryArray[$Semester][0]["Application"]==""?"--":$ParSummaryArray[$Semester][0]["Application"]);
						break;
					case "Tidiness":
						$Display = ($ParSummaryArray[$Semester][0]["Tidiness"]==""?"--":$ParSummaryArray[$Semester][0]["Tidiness"]);
						break;
					case "Motivation":
						$Display = ($ParSummaryArray[$Semester][0]["Motivation"]=="") ? "--" : $ParSummaryArray[$Semester][0]["Motivation"];
						break;
					case "SelfConfidence":
						$Display = ($ParSummaryArray[$Semester][0]["SelfConfidence"]=="") ? "--" : $ParSummaryArray[$Semester][0]["SelfConfidence"];
						break;
					case "SelfDiscipline":
						$Display = ($ParSummaryArray[$Semester][0]["SelfDiscipline"]=="") ? "--" : $ParSummaryArray[$Semester][0]["SelfDiscipline"];
						break;
					case "Courtesy":
						$Display = ($ParSummaryArray[$Semester][0]["Courtesy"]=="") ? "--" : $ParSummaryArray[$Semester][0]["Courtesy"];
						break;
					case "Honesty":
						$Display = ($ParSummaryArray[$Semester][0]["Honesty"]=="") ? "--" : $ParSummaryArray[$Semester][0]["Honesty"];
						break;
					case "Responsibility":
						$Display = ($ParSummaryArray[$Semester][0]["Responsibility"]=="") ? "--" : $ParSummaryArray[$Semester][0]["Responsibility"];
						break;
					case "Cooperation":
						$Display = ($ParSummaryArray[$Semester][0]["Cooperation"]=="") ? "--" : $ParSummaryArray[$Semester][0]["Cooperation"];
						break;

					case "DaysAbsent":
						//$Display = ($ParSummaryArray[$Semester][0]==""?"--":$ParSummaryArray[$Semester][0]);
						$Display = ($ParSummaryArray[$Semester][0]["DaysAbsent"]==""?"--":$ParSummaryArray[$Semester][0]["DaysAbsent"]);
						break;
					case "TimesLate":
						$Display = ($ParSummaryArray[$Semester][0]["TimesLate"]==""?"--":$ParSummaryArray[$Semester][0]["TimesLate"]);
						break;
					case "AbsentWOLeave":
						$Display = ($ParSummaryArray[$Semester][0]["AbsentWOLeave"]==""?"--":$ParSummaryArray[$Semester][0]["AbsentWOLeave"]);
						break;
				}
				if($count%2==0) {
					$SummaryTable .= "<tr>";
				}
				$SummaryTable .= "<td class='small_title' width='30%' valign='top' nowrap='nowrap' height='$LineHeight'>".$Title."</td><td class='small_title' width='5'>:</td><td class='tabletext' width='20%'>".$Display."</td>";
				if($count%2==1) {
					$SummaryTable .= "</tr>";
				}
				$count++;
			}
		}
		$SummaryTable .= "</table>";
	}

	return $SummaryTable;
}

# function to get signature table
function GENERATE_SIGNATURE_TABLE()
{
	global $i_Teaching_ClassTeacher, $eReportCard, $SettingArray, $LangArray, $Footer;
	global $ChiReportCard, $EngReportCard, $IssueDate, $SettingStudentInfo;

	$SignatureTitleArray = $eReportCard['DisplaySettingsArray']["Signature"];
	$SettingMiscArray = $SettingArray["Signature"];
	$SignatureCell = "";
	$CellCount = 0;
	for($i=0; $i<sizeof($SignatureTitleArray); $i++)
	{
		$SettingID = trim($SignatureTitleArray[$i]);
		if($SettingMiscArray[$SettingID]==1)
		{
			$Title = strtoupper($EngReportCard[$SettingID])."<br>".$ChiReportCard[$SettingID];
			$SignatureCell .= "<td valign='bottom' align='center'>";
			$SignatureCell .= "<table cellspacing='0' cellpadding='0' border='0'>";
			$SignatureCell .= "<tr><td align='center' class='tabletext' height='50' valign='bottom'>____________________</td></tr>";
			$SignatureCell .= "<tr><td align='center' class='small_title' valign='bottom'>".$Title."</td></tr>";
			$SignatureCell .= "</table>";
			$SignatureCell .= "</td>";
			$CellCount++;
		}
	}
	//$IssueDate
	//strlen($IssueDate);
	if ($SettingStudentInfo['DateOfIssue']==1) 
	{
		for ($TempCount = 0; $TempCount < (20 - strlen($IssueDate))/2; $TempCount++)
		{
			$TempPrefix .= "_";
		}
		$Title = strtoupper($EngReportCard['DateOfIssue'])."<br>".$ChiReportCard['DateOfIssue'];
		$SignatureCell .= "<td valign='bottom' align='center'>";
		$SignatureCell .= "<table cellspacing='0' cellpadding='0' border='0'>";
		$SignatureCell .= "<tr><td align='center' class='tabletext' height='50' valign='bottom'>$TempPrefix<span style='text-decoration: underline'>".$IssueDate."</span>$TempPrefix</td></tr>";
		$SignatureCell .= "<tr><td align='center' class='small_title' valign='bottom'>".$Title."</td></tr>";
		$SignatureCell .= "</table>";
		$SignatureCell .= "</td>";
	}

	if($CellCount>0)
	{
		$SignatureTable = "<table width='100%' border='0' cellpadding='1' cellspacing='0' align='center'>\n";
		$SignatureTable .= "<tr>\n";
		$SignatureTable .= $SignatureCell;
		$SignatureTable .= "</tr>\n";
		$SignatureTable .= "</table>\n";
	}

	if($Footer!="") {
		$SignatureTable .= "<table width='100%' border='0' cellpadding='0' cellspacing='0'><tr><td class='small_text'>".str_replace("\n", "<br />", intranet_undo_htmlspecialchars($Footer))."</td></tr></table>\n";
	}

	return $SignatureTable;
}

function GET_SEMESTER_NUMBER()
{
	global $libreportcard, $SemesterArray;

	$sem_number = "-1";
	if(trim($libreportcard->Semester)=="FULL")
	{
		$sem_number = 0;
	} else
	{
		for($i=0; $i<sizeof($SemesterArray); $i++)
		{
			if(trim($libreportcard->Semester)==trim($SemesterArray[$i]))
			{
				$sem_number = $i+1;
				break;
			}
		}
	}
	return $sem_number;
}

function CHECK_CSV_FILE_FORMAT($ParHeaderArray, $ParDefaultHeaderArray)
{
	# Check Title Row
	$format_wrong = false;

	for ($i=0; $i<sizeof($ParDefaultHeaderArray); $i++)
	{
		if ($ParHeaderArray[$i]!=$ParDefaultHeaderArray[$i])
		{
			$format_wrong = true;
			break;
		}
	}

	return $format_wrong;
}

function GET_CSV_FILE_CONTENT($ParFile, $ParDefaultHeaderArray, $ReportType="")
{
	global $libreportcard, $lf, $StudentRegNoArray, $SemesterArray;
	
	######### For adding associated array ##########
	global $HeaderSettingMap, $CustomSettingMap;
	################################################
	
	$ClassArr = $libreportcard->GET_CLASSES();

	$TargetArray = $SemesterArray;
	$TargetArray[] = "FULL";
	for($i=0; $i<sizeof($TargetArray); $i++)
	{
		$t_semester = trim($TargetArray[$i]);
		$sem_number = GET_SEMESTER_NUMBER($t_semester);
		if($sem_number>=0)
		{
			for ($j = 0; $j < sizeof($ClassArr); $j++)
			{
				$TempClass = str_replace(".", "", str_replace("/", "", $ClassArr[$j]));
				$TargetFile = $ParFile."/".trim($libreportcard->Year)."_".$sem_number."_".$TempClass.".csv";
				if(file_exists($TargetFile))
				{
					$data = $lf->file_read_csv($TargetFile);
					if(!empty($data))
					{
						$header_row = array_shift($data);
						$wrong_format = CHECK_CSV_FILE_FORMAT($header_row, $ParDefaultHeaderArray);
						#########################################
						# Ignore wrong format checking and force
						# assign associated array
						$wrong_format = false;
						#########################################
						if(!$wrong_format)
						{
							for($k=0; $k<sizeof($data); $k++)
							{
								$reg_no = array_shift($data[$k]);
								$reg_no = trim(str_replace("#", "", $reg_no));
								$student_id = trim($StudentRegNoArray[$reg_no]);
								if($student_id!="")
								{
									// add associated array to $data also
									if ($ReportType != "") {
										for($r=0; $r<sizeof($data[$k]); $r++) {
											# assign key name using display wording
											$data[$k][$CustomSettingMap[$ReportType][$header_row[$r+1]]] = $data[$k][$r];
										}
									}
									$ReturnArray[$student_id][$t_semester][] = $data[$k];
								}
							}
						}
					}
				}
			}
		}
	}
	return $ReturnArray;
}

# function to get result table
function GENERATE_RESULT_TABLE($ParSummaryTable, $ParStudentResultArray, $ParSubjectTeacherCommentArray)
{
	global $eReportCard, $SubjectArray, $ColumnArray, $ReportCellSetting, $ShowFullMark, $libreportcard;
	global $FailedArray, $DistinctionArray, $PFArray, $SettingArray;
	global $AllSubjectArray, $ShowOverallPosition, $OverallPositionRange, $HideNotEnrolled;
	global $ShowSubjectTeacherComment, $LangArray, $BilingualReportTitle;
	global $LineHeight;
	global $SettingSummaryArray, $SummaryResultArray, $SemesterArray, $Semester, $StudentID;

	# Report Type
	# 1 - Display the overall result of Parent Subject AND Component Subject(s)
	# 2 - Display the overall result of Parent Subject ONLY
	# 3 - Display the overall result of Component Subject(s) ONLY
	$ResultDisplayType = $SettingArray["ResultDisplayType"];
	$ResultCalculationType = $SettingArray["ResultCalculationType"];
	$SubjectTitleType = $SettingArray["SubjectTitleType"];
	$ShowColumnPercentage = $SettingArray["ShowColumnPercentage"];
	$MarkTypeDisplay = $SettingArray["MarkTypeDisplay"];
	$HideOverallResult = $SettingArray["HideOverallResult"];

	# get subject title cell
	if($BilingualReportTitle==1 || $SubjectTitleType==0) {
		$SubjectTitleCell = "<td width='50%' class='small_title' align='left'>".$eReportCard['EngSubject']."</td><td width='50%' class='small_title' align='left'>".$eReportCard['ChiSubject']."</td>";
	}
	else {
		$SubjectTitleCell = "<td class='small_title' align='left'>".$eReportCard['Subject']."</td>";
	}

	$ExtraColumn = 0;
	//debug_r($ColumnArray);
	$ColumnSize = sizeof($ColumnArray);
	$ResultTable = "<table width='100%' border=0 cellspacing='0' cellpadding='1' class='report_border'>";


	################################################################
	$ResultTable .= "<tr>
						<td width='25%'>&nbsp;
						</td>";

	$TempCount = 0;
	for($i=0; $i<sizeof($ColumnArray); $i++)
	{
		$TempCss = ($i%3==0) ? "border_left" : "";
		if ($TempCss != "")
		{
			//$TempTitle = ($TempCount == 0) ? "First Term" : "Second Term";
			$TempTitle = "&nbsp;";
			$TempCount++;
		} else {
			$TempTitle = "&nbsp";
		}
		$ResultTable .= "<td class='small_title $TempCss' width='10%'>".$TempTitle;
		$ResultTable .= "</td>";
	}

	if($ColumnSize>1 && $HideOverallResult!=1)
	{
		$ResultTable .= "<td class='border_left small_title' width='10%' align='center'>Total Result</td>";
	}
	$ResultTable .= "</tr>";
	################################################################





























	$ResultTable .= "<tr>
						<td width='25%' class='report_formfieldtitle'>
							<table width='100%' border='0' cellpadding='0' cellspacing='0'><tr>".$SubjectTitleCell."</tr>
							</table>
						</td>";
	/*
	if($ShowFullMark==1)
	{
		$ResultTable .= "<td width='5%' class='border_left small_title report_formfieldtitle' align='center'>".$LangArray['FullMark']."</td>";
		$ExtraColumn++;
	}
	*/

	list($op_from, $op_to) = explode(",", $OverallPositionRange);
	for($i=0; $i<sizeof($ColumnArray); $i++)
	{
		//list($ReportColumnID, $ColumnTitle, $Weight, $ShowPosition, $PositionRange) = $ColumnArray[$i];
		list($ReportColumnID, $ColumnTitle, $Weight, $ShowPosition, $PositionRange, $IsColumnSum, $MarkSummaryWeight) = $ColumnArray[$i];

		if($Weight!="" && $ShowColumnPercentage==1)
		{
			$ColumnTitle = $ColumnTitle."&nbsp;".$Weight."%";
			$WeightArray[$ReportColumnID] = $Weight;
		}
		$TempCss = ($i%3==0) ? "border_left" : "";
		if ($i%3==2) $ColumnTitle = "Total";
		$ResultTable .= "<td class='$TempCss report_formfieldtitle' width='10%'><table border=0 cellpadding=0 cellspacing=0 width='100%'>";
		$ResultTable .= "<tr height='75%'><td align='center' class='small_title' colspan='3'>".$ColumnTitle."</td></tr>";
		if($ShowPosition==1 || $ShowPosition==2)
		{
			$ResultTable .= "<tr>";
			$ResultTable .= "<td align='center' width='50%' class='reportcard_text'>".$LangArray['Mark']."</td>";
			$ResultTable .= "<td width=5>&nbsp;</td>";
			$ResultTable .= "<td align='center' width='50%' class='reportcard_text'>(".$LangArray['Position'].")</td>";
			$ResultTable .= "</tr>";
		}

		$ColumnWeight[$ReportColumnID] = $Weight;
		$ColumnSummaryWeight[$ReportColumnID] = $MarkSummaryWeight;

		$ResultTable .= "</table></td>";
	}

	if($ColumnSize>1 && $HideOverallResult!=1)
	{
		$ResultTable .= "<td class='border_left report_formfieldtitle' width='10%'><table border=0 cellpadding=0 cellspacing=0 width='100%' align='center'>";
		//$ResultTable .= "<tr height='75%'><td align='center' class='small_title' colspan='3'>".$LangArray['OverallResult']."</td></tr>";
		$ResultTable .= "<tr height='75%'><td align='center' class='small_title' colspan='3'>總成績</td></tr>";
		if($ShowOverallPosition==1 || $ShowOverallPosition==2)
		{
			$ResultTable .= "<tr>";
			$ResultTable .=  "<td align='center' width='50%' class='reportcard_text'>".$LangArray['Mark']."</td>";
			$ResultTable .= "<td width=5>&nbsp;</td>";
			$ResultTable .= "<td align='center' width='50%' class='reportcard_text'>(".$LangArray['Position'].")</td>";
			$ResultTable .= "</tr>";
		}
		$ResultTable .= "</table></td>";
		$ExtraColumn++;
	}
	if($ShowSubjectTeacherComment==1)
	{
		$ResultTable .= "<td width='10%' class='border_left small_title report_formfieldtitle' align='center'>".$LangArray['SubjectTeacherComment']."</td>";
	}
	$ResultTable .= "</tr>";

	$ColumnSpan = $ColumnSize + $ExtraColumn + 1;
	$IsFirst = 1;
	if(is_array($SubjectArray))
	{
		$count = 0;
		foreach($SubjectArray as $CodeID => $Data)
		{
			if(is_array($Data))
			{
				foreach($Data as $CmpCodeID => $InfoArr)
				{
					list($ReportSubjectID, $SubjectID, $EngSubjectName, $ChiSubjectName, $FullMark, $Scale, $HidePosition) = $InfoArr;

					# Hide not enrolled subject according to the setting
					if($HideNotEnrolled==1 && (empty($ParStudentResultArray[$SubjectID]["Overall"]) || strcmp($ParStudentResultArray[$SubjectID]["Overall"]["Grade"], "/")==0))
						continue;

					$IsComponent = ($CmpCodeID>0) ? 1 : 0;
					$top_style = ($IsComponent==0 && $IsFirst==0) ? "border_top" : "";
					$IsFirst = 0;
					$Prefix = ($IsComponent==1) ? "&nbsp;&nbsp;&nbsp;&nbsp;" : "";

					# Highest Grade
					$MaxGrade = $DistinctionArray[$SubjectID]["G"];
					$FirstRowSetting = $ReportCellSetting[$ColumnArray[0][0]][$ReportSubjectID];
					$FullMark = (($FirstRowSetting=="G")&&($MaxGrade!="")) ? $MaxGrade : $FullMark;

					# check whether is parent/component subject
					$IsParent = (sizeof($AllSubjectArray[$SubjectID])>1) ? 1 : 0;
					$IsComponent = (empty($AllSubjectArray[$SubjectID])) ? 1 : 0;

					if (!$IsComponent) $ParentSubjectID = $SubjectID;

					$ResultShow = (!($ResultDisplayType==2 && $IsParent==1) && !($IsParent==1 && $ResultCalculationType==1)) ? 1 : 0;
					$OverallShow = (!($ResultDisplayType==2 && $IsParent==1) && !($ResultDisplayType==1 && $IsComponent==1)) ? 1 : 0;

					# get subject display cell
					if($BilingualReportTitle==1 || $SubjectTitleType==0) {
						$SubjectCell = "<td width='60%' class='small_text'>".$Prefix.$EngSubjectName."</td><td width='40%' class='small_text'>".$Prefix.$ChiSubjectName."</td>";
					}
					else {
						$SubjectDisplayName = ($SubjectTitleType==1) ? $EngSubjectName : $ChiSubjectName;
						$SubjectCell = "<td class='small_text'>".$Prefix.$SubjectDisplayName."</td>";
					}

					$ResultTable .= "<tr>";
					$ResultTable .= "<td class='tabletext $top_style'>
										<table width='100%' border='0' cellpadding='0' cellspacing='0'><tr>".$SubjectCell."</tr></table>
									</td>";
					//$ResultTable .= ($ShowFullMark==1) ? "<td class='tabletext border_left $top_style' align='center'>".($ResultShow==1?$FullMark."&nbsp;":"&nbsp;")."</td>" : "";

					// fatho
					$TempOverallResult = 0;
					for($i=0; $i<sizeof($ColumnArray); $i++)
					{
						list($ReportColumnID, $ColumnTitle, $Weight, $ShowPosition, $PositionRange, $IsColumnSum, $MarkSummaryWeight) = $ColumnArray[$i];

						$CellSetting = $ReportCellSetting[$ReportColumnID][$ReportSubjectID];
						$Weight = $WeightArray[$ReportColumnID];
						list($p_form, $p_to) = explode(",", $PositionRange);

						$DisplayResult = "";
						if(!$ResultShow)
						{
							$ResultTable .= "<td class='border_left $top_style' align='center'>&nbsp;</td>";
						}
						else
						{
							if($CellSetting!="N/A")
							{
								$StylePrefix = "";
								$StyleSuffix = "";
								if($CellSetting=="S") {
									$s_raw_mark = $ParStudentResultArray[$SubjectID][$ReportColumnID]["RawMark"];
									$s_weighted_mark = $ParStudentResultArray[$SubjectID][$ReportColumnID]["WeightedMark"];
									$s_grade = $ParStudentResultArray[$SubjectID][$ReportColumnID]["Grade"];

									$idStartTag = "";
									$idEndTag = "";

									if ($IsColumnSum)
									{
										$s_raw_mark = $MarkSummary["RawMark"];
										$s_weighted_mark = $MarkSummary["WeightedMark"];
										$s_grade = $MarkSummary["Grade"];


										/*
										if ($s_raw_mark > 0) $ParentMarkSummary[$ParentSubjectID][$ReportColumnID][$StudentID]["RawMark"] += $s_raw_mark;
										if ($s_weighted_mark > 0) $ParentMarkSummary[$ParentSubjectID][$ReportColumnID][$StudentID]["WeightedMark"] += $s_weighted_mark;
										$ParentMarkSummary[$ParentSubjectID][$ReportColumnID][$StudentID]["Grade"] = $s_grade;
										$ParentMarkSummary[$ParentSubjectID][$ReportColumnID][$StudentID]["CellSetting"] = $CellSetting;
										$ParentMarkSummary[$ParentSubjectID][$ReportColumnID][$StudentID]["MarkTypeDisplay"] = $MarkTypeDisplay;
										*/


										if (!$IsComponent) {
											$idStartTag = "<div id='ColSum".$SubjectID."_".$ReportColumnID."_".$StudentID."'>";
											$idEndTag = "</div>";
											//hdebug('asdf', $s_raw_mark, $s_weighted_mark, $s_grade);
											// display calculated mark
										} else if ($IsComponent) {
											if ($s_raw_mark > 0) $TempHorizontalMark[$ParentSubjectID][$ReportColumnID][$StudentID]["RawMark"] += $s_raw_mark;
											if ($s_weighted_mark > 0) $TempHorizontalMark[$ParentSubjectID][$ReportColumnID][$StudentID]["WeightedMark"] += $s_weighted_mark;
											$TempHorizontalMark[$ParentSubjectID][$ReportColumnID][$StudentID]["Grade"] = $s_grade;
											$TempHorizontalMark[$ParentSubjectID][$ReportColumnID][$StudentID]["CellSetting"] = $CellSetting;
											$TempHorizontalMark[$ParentSubjectID][$ReportColumnID][$StudentID]["MarkTypeDisplay"] = $MarkTypeDisplay;
										}
									} else {
										//$MarkSummary["RawMark"] += $s_raw_mark * $MarkSummaryWeight / 100;
										//$MarkSummary["WeightedMark"] += $s_weighted_mark * $MarkSummaryWeight / 100;
										//$MarkSummary["Grade"] = $s_grade;


										if (!$IsComponent) {
											$idStartTag = "<div id='SubTotalMark".$SubjectID."_".$ReportColumnID."_".$StudentID."'>";
											$idEndTag = "</div>";
											// display calculated mark
										} else if ($IsComponent) {
											$MarkSummary["RawMark"] += $s_raw_mark * $MarkSummaryWeight / 100;
											$MarkSummary["WeightedMark"] += $s_weighted_mark * $MarkSummaryWeight / 100;
											$MarkSummary["Grade"] = $s_grade;

											if ($s_raw_mark > 0) $TempTotalMark[$ParentSubjectID][$ReportColumnID][$StudentID]["RawMark"] += $s_raw_mark;
											if ($s_weighted_mark > 0) $TempTotalMark[$ParentSubjectID][$ReportColumnID][$StudentID]["WeightedMark"] += $s_weighted_mark;
											$TempTotalMark[$ParentSubjectID][$ReportColumnID][$StudentID]["Grade"] = $s_grade;
											$TempTotalMark[$ParentSubjectID][$ReportColumnID][$StudentID]["CellSetting"] = $CellSetting;
											$TempTotalMark[$ParentSubjectID][$ReportColumnID][$StudentID]["MarkTypeDisplay"] = $MarkTypeDisplay;
										}
									}

									if($s_raw_mark>=0) {
										if($MarkTypeDisplay==2) {
											$DisplayResult = $s_grade;
											//aki
											list($StylePrefix, $StyleSuffix, $IsDistinct, $IsFailed) = $libreportcard->GET_STYLE("G", $DisplayResult, $FailedArray[$SubjectID], $DistinctionArray[$SubjectID], $PFArray[$SubjectID]);
										}
										else {
											//aki											
											list($StylePrefix, $StyleSuffix, $IsDistinct, $IsFailed) = $libreportcard->GET_STYLE($CellSetting, $s_raw_mark, $FailedArray[$SubjectID], $DistinctionArray[$SubjectID], $PFArray[$SubjectID]);
											$DisplayResult = ($MarkTypeDisplay==1) ? $s_raw_mark : $s_weighted_mark;
											$DisplayResult = number_format($DisplayResult, 2);
											//hdebug($s_raw_mark, $DisplayResult, $IsFailed, $SubjectID);
											//hdebug_r($PFArray[$SubjectID]);
										}
									}
									else
										$DisplayResult = $libreportcard->SpecialMarkArray[trim($s_raw_mark)];
								}
								else {
									$DisplayResult = $ParStudentResultArray[$SubjectID][$ReportColumnID]["Grade"];
									if($DisplayResult!="/" && $DisplayResult!="abs") {
										//aki
										list($StylePrefix, $StyleSuffix, $IsDistinct, $IsFailed) = $libreportcard->GET_STYLE($CellSetting, $DisplayResult, $FailedArray[$SubjectID], $DistinctionArray[$SubjectID], $PFArray[$SubjectID]);
									}
								}
								
								if ($DisplayResult < 0) {
									$StylePrefix = "";
									$StyleSuffix = "";
								}
								if (($DisplayResult == "/")||($DisplayResult == -2)) {
									$DisplayResult = "--";
								}
								
								$DisplayResult = $idStartTag.$StylePrefix.$DisplayResult.$StyleSuffix.$idEndTag;
								$FormPosition = $ParStudentResultArray[$SubjectID][$ReportColumnID]["OMF"];
								$ClassPosition = $ParStudentResultArray[$SubjectID][$ReportColumnID]["OMC"];
							}
							
							// fatho
							if ($DisplayResult > 0) $TempOverallResult += $DisplayResult;
							

							$ResultTable .= "<td class='border_left $top_style' width='10%'><table border=0 cellpadding=0 cellspacing=0 width='100%' height='$LineHeight'>";
							$ResultTable .= "<tr>";
							if ($CellSetting == "G") {
								
								$Sql = "
											SELECT
													SchemeID
											FROM
													RC_SUBJECT_GRADING_SCHEME
											WHERE
													SubjectGradingID = '$SubjectGradingArray[$SubjectID]'
										";
								$row = $libreportcard->returnVector($Sql);
								$SchemeID = $row[0];
								
								$GradeMarkArr = $libreportcard->GET_GRADEMARK_BY_SCHEMEID($SchemeID);
							}
							
							
							if($ShowPosition==1 || $ShowPosition==2)
							{
								$ResultTable .= "<td align='center' class='tabletext' width='50%'>";
								$ResultTable .= "<table width='90%' cellspacing='0' cellpadding='0' align='right'><tr><td width='50%' class='tabletext' align='center'>";
								$ResultTable .= ($DisplayResult==""?"--":$DisplayResult);
								$ResultTable .= "</td><td width='50%' class='tabletext' align='center'>";
								if ($CellSetting == "G") $FullMark = $GradeMarkArr[0][1];
								$ResultTable .= (($ShowFullMark) ? " (".$FullMark.")":"")."</td></tr></table>";
								$ResultTable .= "<td width=5>&nbsp;</td>";

								$PosDisplay = ($ShowPosition==1) ? $ClassPosition : $FormPosition;
								$PosDisplay = ($PosDisplay>0 && ($PosDisplay>=$p_from && $PosDisplay<=$p_to)) ? $PosDisplay : "*";

								$ResultTable .= ($HidePosition==0 && $CellSetting=="S") ? "<td align='center' class='tabletext' width='50%'>(".$PosDisplay.")</td>" : "<td align='center' class='tabletext width='50%'>(*)</td>";
							}
							else
							{
								$ResultTable .= "<td align='center' class='tabletext'>";
								//($DisplayResult==""?"--":$DisplayResult).(($ShowFullMark) ? " (".$FullMark.")":"")
								$ResultTable .= "<table width='90%' cellspacing='0' cellpadding='0' align='right'><tr><td width='50%' class='tabletext' align='center'>";
								$ResultTable .= ($DisplayResult==""?"--":$DisplayResult);
								$ResultTable .= "</td><td width='50%' class='tabletext' align='center'>";
								$FullMarkShow = GET_COLUMN_FULLMARK($FullMark, $ReportColumnID, $ColumnWeight, $ColumnSummaryWeight);
								if ($CellSetting == "G") $FullMarkShow = $GradeMarkArr[0][1];
								$ResultTable .= (($ShowFullMark) ? " (".$FullMarkShow.")":"")."</td></tr></table>";
								$ResultTable .= "</td>";
							}
							$ResultTable .= "</tr>";
							$ResultTable .= "</table></td>";
						}
						if ($IsColumnSum)
						{
							// reset field at the back
							$MarkSummary["RawMark"] = 0;
							$MarkSummary["WeightedMark"] = 0;
							$MarkSummary["Grade"] = "--";
						}
					}

					if($ColumnSize>1 && $HideOverallResult!=1)
					{
						if(!$OverallShow)
						{
							$ResultTable .= "<td class='tabletext border_left $top_style' align='center'>&nbsp;</td>";
						}
						else
						{
							$StylePrefix = "";
							$StyleSuffix = "";
							if($CellSetting=="S") {
								$s_overall_mark = $ParStudentResultArray[$SubjectID]["Overall"]["Mark"];
								$s_overall_grade = $ParStudentResultArray[$SubjectID]["Overall"]["Grade"];

								if($s_overall_mark>=0) {
									if($MarkTypeDisplay==2) {
										$OverallResult = $s_overall_grade;
										// fatho
										//$OverallResult = $TempOverallResult;
										// aki
										list($StylePrefix, $StyleSuffix, $IsDistinct, $IsFailed) = $libreportcard->GET_STYLE("G", $OverallResult, $FailedArray[$SubjectID], $DistinctionArray[$SubjectID], $PFArray[$SubjectID]);
									}
									else {
										$OverallResult = $s_overall_mark;
										// fatho
										//$OverallResult = $TempOverallResult;
										// aki
										list($StylePrefix, $StyleSuffix, $IsDistinct, $IsFailed) = $libreportcard->GET_STYLE($CellSetting, $OverallResult, $FailedArray[$SubjectID], $DistinctionArray[$SubjectID], $PFArray[$SubjectID]);
										$OverallResult = number_format($s_overall_mark, 2);
									}
								}
								else
									$OverallResult = $libreportcard->SpecialMarkArray[trim($s_overall_mark)];
							}
							else {
								$OverallResult = $ParStudentResultArray[$SubjectID]["Overall"]["Grade"];
								// fatho
								//$OverallResult = $TempOverallResult;
								if($OverallResult!="/" && $OverallResult!="abs") {
									// aki
									list($StylePrefix, $StyleSuffix, $IsDistinct, $IsFailed) = $libreportcard->GET_STYLE($CellSetting, $OverallResult, $FailedArray[$SubjectID], $DistinctionArray[$SubjectID], $PFArray[$SubjectID]);
								}
							}
							if ($OverallResult == "/") $OverallResult = "--";
							$OverallResult = $StylePrefix.$OverallResult.$StyleSuffix;
							$OverallFormPosition = $ParStudentResultArray[$SubjectID]["Overall"]["OMF"];
							$OverallClassPosition = $ParStudentResultArray[$SubjectID]["Overall"]["OMC"];

							$ResultTable .= "<td class='border_left $top_style' width='10%'><table border=0 cellpadding=0 cellspacing=0 width='100%'>";
							$ResultTable .= "<tr>";
							if($ShowOverallPosition==1 || $ShowOverallPosition==2)
							{
								$ResultTable .= "<td align='center' class='tabletext' width='50%'>".($OverallResult==""?"--":$OverallResult)."</td>";
								$ResultTable .= "<td>&nbsp;</td>";

								$OverallPosDisplay = ($ShowOverallPosition==1) ? $OverallClassPosition : $OverallFormPosition;
								$OverallPosDisplay = ($OverallPosDisplay>0 && ($OverallPosDisplay>=$op_from && $OverallPosDisplay<=$op_to)) ? $OverallPosDisplay : "*";

								//aki
								if ($IsFailed) $OverallPosDisplay = "*";
								$ResultTable .= ($HidePosition==0 && $CellSetting=="S") ? "<td align='center' class='tabletext' width='50%'>(".$OverallPosDisplay.")</td>" : "<td align='center' class='tabletext' width='50%'>(*)</td>";
							}
							else
							{
								$ResultTable .= "<td align='center' class='tabletext'>";
								//($OverallResult==""?"--":$OverallResult)
								$ResultTable .= "<table width='90%' cellspacing='0' cellpadding='0' align='right'><tr><td width='50%' class='tabletext' align='center'>";
								$ResultTable .= ($OverallResult==""?"--":$OverallResult);
								$ResultTable .= "</td><td width='50%' class='tabletext' align='center'>";
								if ($CellSetting == "G") $FullMark = $GradeMarkArr[0][1];
								$ResultTable .= (($ShowFullMark) ? " (".$FullMark.")":"")."</td></tr></table>";
								$ResultTable .= "</td>";
							}
							$ResultTable .= "</tr>";
							$ResultTable .= "</table></td>";
						}
					}
					if($ShowSubjectTeacherComment==1)
					{
						$Comment = $ParSubjectTeacherCommentArray[$SubjectID];
						$ResultTable .= "<td class='tabletext border_left $top_style' align='center'>".($Comment==""?"&nbsp;":"<li>".$Comment."</li>")."</td>";
					}
					$ResultTable .= "</tr>";
					$count++;
				}
			}
		}

		if (($libreportcard->CalculationOrder != "Horizontal")&&(is_array($TempHorizontalMark))) {
			$TempJSCode = "\n<script language='JavaScript'>\n";
			while(list($jsSubjectID, $jsSubjectArr) = each($TempHorizontalMark)) {
				while(list($jsReportColumnID, $jsStudentArr) = each($jsSubjectArr)) {
					while(list($jsStudentID, $jsValueArr) = each($jsStudentArr)) {
						if ($jsValueArr['CellSetting'] == "S") {
							$DisplayValue = ($jsValueArr['MarkTypeDisplay']==1) ? $jsValueArr['RawMark'] : $jsValueArr['WeightedMark'];
							$DisplayValue = number_format($DisplayValue, 2);
						} else {
							$DisplayValue = $jsValueArr['Grade'];
						}

						if($DisplayValue!="/" && $DisplayValue!="abs") {
							list($StylePrefix, $StyleSuffix, $IsDistinct, $IsFailed) = $libreportcard->GET_STYLE($jsValueArr['CellSetting'], $DisplayValue, $FailedArray[$jsSubjectID], $DistinctionArray[$jsSubjectID], $PFArray[$jsSubjectID]);
						}
						if ($DisplayValue == "/") $DisplayValue = "--";
						//($MarkTypeDisplay==1) ? $s_raw_mark : $s_weighted_mark;
						$FieldName = "ColSum".$jsSubjectID."_".$jsReportColumnID."_".$jsStudentID;
						$TempJSCode .= "\t if (document.getElementById('".$FieldName."') != null) document.getElementById('".$FieldName."').innerHTML = '".$DisplayValue."'\n";
					}
				}
			}
			$TempJSCode .= "</script>\n";
		}

		##### replace the parent subject mark by sum up the component subjects ###################################
		/*
		if (is_array($TempTotalMark)) {
			$TempJSCode = "\n<script language='JavaScript'>\n";
			while(list($jsSubjectID, $jsSubjectArr) = each($TempTotalMark)) {
				while(list($jsReportColumnID, $jsStudentArr) = each($jsSubjectArr)) {
					while(list($jsStudentID, $jsValueArr) = each($jsStudentArr)) {
						if ($jsValueArr['CellSetting'] == "S") {
							$DisplayValue = ($jsValueArr['MarkTypeDisplay']==1) ? $jsValueArr['RawMark'] : $jsValueArr['WeightedMark'];
							$DisplayValue = number_format($DisplayValue, 2);
						} else {
							$DisplayValue = $jsValueArr['Grade'];
						}

						if($DisplayValue!="/" && $DisplayValue!="abs") {
							list($StylePrefix, $StyleSuffix, $IsDistinct, $IsFailed) = $libreportcard->GET_STYLE($jsValueArr['CellSetting'], $DisplayValue, $FailedArray[$jsSubjectID], $DistinctionArray[$jsSubjectID], $PFArray[$jsSubjectID]);
						}
						if ($DisplayValue == "/") $DisplayValue = "--";
						//($MarkTypeDisplay==1) ? $s_raw_mark : $s_weighted_mark;
						$FieldName = "SubTotalMark".$jsSubjectID."_".$jsReportColumnID."_".$jsStudentID;
						$TempJSCode .= "\t if (document.getElementById('".$FieldName."') != null) document.getElementById('".$FieldName."').innerHTML = '".$DisplayValue."'\n";
					}
				}
			}
			$TempJSCode .= "</script>\n";
		}
		*/
		##### replace the parent subject mark by sum up the component subjects ###################################

		/*
		if (is_array($ParentMarkSummary)) {
			$TempJSCode = "\n<script language='JavaScript'>\n";
			while(list($jsSubjectID, $jsSubjectArr) = each($ParentMarkSummary)) {
				while(list($jsReportColumnID, $jsStudentArr) = each($jsSubjectArr)) {
					while(list($jsStudentID, $jsValueArr) = each($jsStudentArr)) {
						if ($jsValueArr['CellSetting'] == "S") {
							$DisplayValue = ($jsValueArr['MarkTypeDisplay']==1) ? $jsValueArr['RawMark'] : $jsValueArr['WeightedMark'];
							$DisplayValue = number_format($DisplayValue, 2);
						} else {
							$DisplayValue = $jsValueArr['Grade'];
						}

						if($DisplayValue!="/" && $DisplayValue!="abs") {
							list($StylePrefix, $StyleSuffix, $IsDistinct, $IsFailed) = $libreportcard->GET_STYLE($jsValueArr['CellSetting'], $DisplayValue, $FailedArray[$jsSubjectID], $DistinctionArray[$jsSubjectID], $PFArray[$jsSubjectID]);
						}
						if ($DisplayValue == "/") $DisplayValue = "abs";
						//($MarkTypeDisplay==1) ? $s_raw_mark : $s_weighted_mark;
						$FieldName = "ColSum".$jsSubjectID."_".$jsReportColumnID."_".$jsStudentID;
						$TempJSCode .= "\t if (document.getElementById('".$FieldName."') != null) document.getElementById('".$FieldName."').innerHTML = '".$DisplayValue."'\n";
					}
				}
			}
			$TempJSCode .= "</script>\n";
		}
		*/


	}
	else
	{
		$ResultTable .= "<tr><td align='center' class='tabletext' colspan='".$ColumnSpan."'>".$eReportCard['NoRecord']."</td></tr>";
	}

	/*
	if($ParSummaryTable!="")
	{
		$ResultTable .= "<table width='100%' cellspacing='0' cellpadding='5' class='summary_table'>";
		$ResultTable .= "<tr><td colspan='".$ColumnSpan."' height='100' valign='top'>".$ParSummaryTable."</td></tr>";
		$ResultTable .= "</table>";
	}
	*/
	//asdf
	//hdebug_r($SummaryResultArray);
/*
Array
(
    [0] => First Term
    [1] => Second Term
)
*/
	//hdebug_r($ColumnArray);
	//hdebug($Semester);
	if(!empty($SummaryResultArray))
	{
		for($k=0; $k<sizeof($SummaryResultArray); $k++)
		{
			$ResultTable .= "<tr>";
			$top_border_style = ($k==0) ? "border_top" : "";
			//$colspan = ($ShowFullMark==1) ? "colspan='2'" : "";
			// line height - aki
			$ResultTable .= "<td class='{$top_border_style}' {$colspan}>
						<table border='0' cellpadding='1' cellspacing='0' width='100%' height='$LineHeight'><tr><td class='small_text'>".$SummaryResultArray[$k]["Title"]."</td></tr></table>
						</td>";

			for($i=0; $i<sizeof($ColumnArray); $i++)
			{
				$ColumnTitle = trim($ColumnArray[$i]["ColumnTitle"]);
				$ResultDisplay = $SummaryResultArray[$k][$ColumnTitle];

				if (($ColumnArray[$k]["IsColumnSum"])&&($Semester=="FULL")) {
					$ResultDisplayFieldName = $SemesterArray[$k];
					$ResultDisplay = $SummaryResultArray[$k][$ResultDisplayFieldName];
				}

				//aki
				$ResultTable .= "<td class='tabletext border_left {$top_border_style}' align='center'>".($ResultDisplay==""?"--":$ResultDisplay)."</td>";
			}

			$ResultDisplayFieldName = "";
			for ($checkSemester = 0; $checkSemester < sizeof($SemesterArray); $checkSemester++) {
				if ($SemesterArray[$checkSemester] == $Semester) {
					$ResultDisplayFieldName = $SemesterArray[$checkSemester];
				}
			}

			if(!$HideOverallResult)
				$ResultTable .= "<td class='tabletext border_left {$top_border_style}' align='center'>".($SummaryResultArray[$k][$ResultDisplayFieldName]==""?"--":$SummaryResultArray[$k][$ResultDisplayFieldName])."</td>";
			if($ShowSubjectTeacherComment==1) {
				$ResultTable .= "<td class='border_left {$top_border_style}' align='center'>&nbsp;</td>";
			}
			$ResultTable .= "</tr>";
		}
	}

	$ResultTable .= "</table>".$TempJSCode;

	return $ResultTable;
}

# function get summary array
function GET_SUMMARY_ARRAY($ParInfoArray, $ParClassHighestAverage, $ParSummaryArray)
{
	global $SummaryTitleArray, $SettingSummaryArray, $ShowFullMark, $Semester, $ColumnArray, $LangArray, $SettingArray;
	global $libreportcard, $Year, $ChiReportCard, $EngReportCard;

	$HideOverallResult = $SettingArray["HideOverallResult"];
	$ReturnArray = "";

	if ($SettingSummaryArray['AverageMark']) $TempSettingSummaryArray['AverageMark'] = 1;
	if ($SettingSummaryArray['ClassPosition']) $TempSettingSummaryArray['ClassPosition'] = 1;
	if ($SettingSummaryArray['FormPosition']) $TempSettingSummaryArray['FormPosition'] = 1;
	if ($SettingSummaryArray['DaysAbsent']) $TempSettingSummaryArray['DaysAbsent'] = 1;
	if ($SettingSummaryArray['TimesLate']) $TempSettingSummaryArray['TimesLate'] = 1;
	if ($SettingSummaryArray['Conduct']) $TempSettingSummaryArray['Conduct'] = 1;

	$SettingSummaryArray = $TempSettingSummaryArray;

	if(!empty($SettingSummaryArray))
	{
		$count = 0;
		foreach($SettingSummaryArray as $SettingID => $Data)
		{
			if ($Data==1)
			{
				$Title = "<table border='0' cellspacing='0' cellpadding='0' width='100%'><tr>";
				$Title .= "<td width='60%' class='small_text'>".$EngReportCard[$SettingID]."</td><td width='40%' class='small_text'>".$ChiReportCard[$SettingID]."</td>";
				$Title .= "</tr></table>";
				$ReturnArray[$count]["Title"] = $Title;
				for($i=0; $i<sizeof($ColumnArray); $i++)
				{
					$column = trim($ColumnArray[$i]["ColumnTitle"]);
					if(!empty($ParInfoArray[$column]))
					{
						if (!isset($SemFlag[$column]))
						{
							$SemFlag[$column] = $libreportcard->CHECK_RESULT_EXIST($Year, $column);
						}
						$Display = ($SemFlag[$column]) ? RETURN_SUMMARY_DISPLAY($ParInfoArray[$column], $SettingID, $ParClassHighestAverage[$column], $ParSummaryArray[$column], $column) : 0;
						$ReturnArray[$count][$column] = $Display;
					}
				}
				if(!($Semester=="FULL" && $HideOverallResult==1))
				{
					$Display = RETURN_SUMMARY_DISPLAY($ParInfoArray[$Semester], $SettingID, $ParClassHighestAverage[$Semester], $ParSummaryArray[$Semester], $Semester);
					$ReturnArray[$count][$Semester] = $Display;
				}
				$count++;
			}
		}
	}

	return $ReturnArray;
}

# function to get summary display
function RETURN_SUMMARY_DISPLAY($ParInfoArray, $ParSettingID, $ClassHighestAverage, $StudentSummaryArray, $ParColumnSem)
{
	global $StudentMeritArray, $StudentID;
	global $FailedArray, $DistinctionArray, $PFArray, $libreportcard;
	list($GT, $AM, $GPA, $OMC, $OMCT, $OMF, $OMFT) = $ParInfoArray;

	switch($ParSettingID)
	{
		case "GrandTotal":
				$Display = $GT;
				break;
		case "GPA":
				$Display = $GPA;
				break;
		case "AverageMark":
				$Display = number_format($AM, 2);
				if ($ParColumnSem!="FULL" || $libreportcard->HighlightOverall)
				{
					list($StylePrefix, $StyleSuffix, $IsDistinct, $IsFailed) = $libreportcard->GET_STYLE("S", $AM, $FailedArray[6], $DistinctionArray[6], $PFArray[6]);
					$Display = $StylePrefix.$Display.$StyleSuffix;
				}
				break;
		case "FormPosition":
				$Display = ($OMF>0) ? $OMF : "--";
				break;
		case "ClassPosition":
				$Display = ($OMC>0) ? $OMC : "--";
				break;
		case "ClassPupilNumber":
				$Display = $OMCT;
				break;
		case "FormPupilNumber":
				$Display = $OMFT;
				break;
		case "ClassHighestAverage":
				$Display = $ClassHighestAverage;
				break;
		case "Conduct":
				$Display = ($StudentSummaryArray[0]["Conduct"]=="") ? "--" : $StudentSummaryArray[0]["Conduct"];
				break;
		case "Politeness":
				$Display = ($StudentSummaryArray[0]["Politeness"]=="") ? "--" : $StudentSummaryArray[0]["Politeness"];
				break;
		case "Behaviour":
				$Display = ($StudentSummaryArray[0]["Behaviour"]=="") ? "--" : $StudentSummaryArray[0]["Behaviour"];
				break;
		case "Application":
				$Display = ($StudentSummaryArray[0]["Application"]=="") ? "--" : $StudentSummaryArray[0]["Application"];
				break;
		case "Tidiness":
				$Display = ($StudentSummaryArray[0]["Tidiness"]=="") ? "--" : $StudentSummaryArray[0]["Tidiness"];
				break;

		case "Motivation":
				$Display = ($StudentSummaryArray[0]["Motivation"]=="") ? "--" : $StudentSummaryArray[0]["Motivation"];
				break;
		case "SelfConfidence":
				$Display = ($StudentSummaryArray[0]["SelfConfidence"]=="") ? "--" : $StudentSummaryArray[0]["SelfConfidence"];
				break;
		case "SelfDiscipline":
				$Display = ($StudentSummaryArray[0]["SelfDiscipline"]=="") ? "--" : $StudentSummaryArray[0]["SelfDiscipline"];
				break;
		case "Courtesy":
				$Display = ($StudentSummaryArray[0]["Courtesy"]=="") ? "--" : $StudentSummaryArray[0]["Courtesy"];
				break;
		case "Honesty":
				$Display = ($StudentSummaryArray[0]["Honesty"]=="") ? "--" : $StudentSummaryArray[0]["Honesty"];
				break;
		case "Responsibility":
				$Display = ($StudentSummaryArray[0]["Responsibility"]=="") ? "--" : $StudentSummaryArray[0]["Responsibility"];
				break;
		case "Cooperation":
				$Display = ($StudentSummaryArray[0]["Cooperation"]=="") ? "--" : $StudentSummaryArray[0]["Cooperation"];
				break;


		case "DaysAbsent":
		//debug_r($StudentSummaryArray);
				$Display = ($StudentSummaryArray[0]["DaysAbsent"]=="") ? "--" : $StudentSummaryArray[0]["DaysAbsent"];
				break;
		case "TimesLate":
				$Display = ($StudentSummaryArray[0]["TimesLate"]=="") ? "--" : $StudentSummaryArray[0]["TimesLate"];
				break;
		case "AbsentWOLeave":
				$Display = ($StudentSummaryArray[0]["AbsentWOLeave"]=="") ? "--" : $StudentSummaryArray[0]["AbsentWOLeave"];
				break;
		case "Merit":
				$Display = ($StudentMeritArray[$StudentID][$ParColumnSem][0]["Merit"]=="") ? "--" : $StudentMeritArray[$StudentID][$ParColumnSem][0]["Merit"];
				break;
		case "Demerit":
				$Display = ($StudentMeritArray[$StudentID][$ParColumnSem][0]["Demerit"]=="") ? "--" : $StudentMeritArray[$StudentID][$ParColumnSem][0]["Demerit"];
				break;
	}

	return $Display;
}

################################ Function End #####################################
?>
