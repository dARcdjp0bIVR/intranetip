<?php
/*
 * Subject Unit Setting Array - defined in Get_CLASS_LEVEL_SUBJECT_UNIT()
 * Need to update /report_builder/preview2.php
 */
############################ Function Start ######################################

$eRC_custom['HideOverallSubjectPosition'] = true;

# function to get Title Table
function GENERATE_TITLE_TABLE()
{
	global $ReportTitle, $image_path, $intranet_root;

	# get school badge
	$SchoolLogo = GET_SCHOOL_BADGE();

	# get school name
	$SchoolName = GET_SCHOOL_NAME();
	// 		$SchoolName = "Santa Rosa de Lima English Secondary School Macau"; // 2019-12-27 Philips - Use hardcode school name

	/*
	$TitleTable = "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>\n";
	$TitleTable .= "<tr>";
	$TitleTable .= "<td width='45%'><table  width='100%' border='0' cellpadding='0' cellspacing='0'>";
	if(!empty($ReportTitle)){
		list($ReportTitle1, $ReportTitle2) = explode("::", $ReportTitle);
	}
	if(!empty($SchoolName))
		$TitleTable .= "<tr><td class='report_title' align='center'>".$SchoolName."</td></tr>";
	if(!empty($ReportTitle1))
		$TitleTable .= "<tr><td class='report_title' align='center'>".$ReportTitle1."</td></tr>";
	if(!empty($ReportTitle2))
		$TitleTable .= "<tr><td class='report_title' align='center'>".$ReportTitle2."</td></tr>";
	$TitleTable .= "</table></td>";
	$TitleTable .= "<td width='10%' align='center'>".$SchoolLogo."</td>";
	$TitleTable .= "<td width='45%'>&nbsp;</td>";
	$TitleTable .= "</tr>";
	$TitleTable .= "</table>";
	*/

	$TitleTable = "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>\n";
	$TitleTable .= "<tr><td width='120' align='center'>".($SchoolLogo==""?"&nbsp;":$SchoolLogo)."</td>";
	if(!empty($ReportTitle) || !empty($SchoolName))
	{
		list($ReportTitle1, $ReportTitle2) = explode("::", $ReportTitle);

		$TitleTable .= "<td>";
		$TitleTable .= "<table width='100%' border='0' cellpadding='1' cellspacing='0' align='center'>\n";
		if(!empty($SchoolName))
			$TitleTable .= "<tr><td nowrap='nowrap' class='report_title' align='center'>".$SchoolName."</td></tr>\n";
		if(!empty($ReportTitle1))
			$TitleTable .= "<tr><td nowrap='nowrap' class='report_title' align='center'>".$ReportTitle1."</td></tr>\n";
		if(!empty($ReportTitle2))
			$TitleTable .= "<tr><td nowrap='nowrap' class='report_title' align='center' colspan='2'>".$ReportTitle2."</td></tr>\n";
		$TitleTable .= "</table>\n";
		$TitleTable .= "</td>";
	}
	$TitleTable .= "<td width='120' align='center'>&nbsp;</td></tr>";
	$TitleTable .= "</table>";

	return $TitleTable;
}

# function to get Student Info Table
function GENERATE_STUDENT_INFO_TABLE($ParInfoArray)
{
	global $SettingStudentInfo, $StudentInfoTitleArray, $ClassTeacherArray, $today, $LangArray, $IssueDate, $ReportInfo;
	global $libreportcard;
	
	//2012-0719-1026-32096 point 3
	//list($ClassName, $ClassNumber, $StudentName, $RegNo) = $ParInfoArray;
	list($ClassName, $ClassNumber, $StudentName, $RegNo, $chi_name, $eng_name) = $ParInfoArray;
	if(!empty($SettingStudentInfo))
	{
		$count = 0;
		$StudentInfoTable = "<table width='100%' border='0' cellpadding='1' cellspacing='0' align='center'>\n";
		for($i=0; $i<sizeof($StudentInfoTitleArray); $i++)
		{
			$SettingID = trim($StudentInfoTitleArray[$i]);
			if($SettingStudentInfo[$SettingID]==1 && $SettingID!="ClassNumber")
			{
				$Title = $LangArray[$SettingID];
				$Title = str_replace("<br />", " ", $Title);
				$Display = "";
				switch($SettingID)
				{
					case "Name":
						//2012-0719-1026-32096 point 3
						//$Display = $StudentName;
						$Display = $eng_name;
						break;
					case "ClassName":
						$Display = ($SettingStudentInfo["ClassNumber"]==1) ? $ClassName." (".$ClassNumber.") " : $ClassName;
						break;
					case "ClassTeacher":
						$Display = (!empty($ClassTeacherArray[$ClassName])) ? implode(", ", $ClassTeacherArray[$ClassName]) : "";
						break;
					case "DateOfIssue":
						$Display = $IssueDate;
						if ($Display == "") $Display = date('Y-m-d');
						break;
					case "StudentNo":
						$Display = $RegNo;
						break;
					case "AcademicYear":
						//$Display = $libreportcard->GET_MARKSHEET_COLLECTION_YEAR();
						
						global $PATH_WRT_ROOT;
						include_once($PATH_WRT_ROOT.'includes/form_class_manage.php');
						$AcademicYearObj = new academic_year($ReportInfo['AcademicYear']);
						$Display = $AcademicYearObj->YearNameEN;
						
						break;
				}

				if($count%2==0) {
					$StudentInfoTable .= "<tr>\n";
				}
				$StudentInfoTable .= "<td class='tabletext' width='20%' valign='top'>".$Title." : ".$Display."</td>\n";
				if($count%2==1) {
					$StudentInfoTable .= "</tr>\n";
				}
				$count++;
			}
		}
		$StudentInfoTable .= "</table>\n";
	}

	return $StudentInfoTable;
}

# function to get MISC Table
function GENERATE_MISC_TABLE($ParClassTeacherComment, $ParStudentID, $ParClassWebSAMSCode)
{
	global $SettingMiscArray, $MiscTitleArray, $Semester, $SemesterName, $LangArray;
	global $StudentAwardArray, $StudentMeritArray, $StudentECAArray, $StudentRemarkArray;
	global $StudentInterSchoolArray, $StudentSchoolServiceArray;
	global $ReportTemplateSemesterName;

	$targetReportTemplateSemesterName = ($ReportTemplateSemesterName=='')? 'FULL' : $ReportTemplateSemesterName;
	
	//2014-0509-1103-37071
	$targetMiscTitleArray = array('ECA', 'ClassTeacherComment');
	
	$ParStudentID = trim($ParStudentID);
	if(!empty($SettingMiscArray))
	{
		$IsFirst = 1;
		$count = 0;
		$ECARowContent = "";
		$MiscTable = "<table width='100%' border='0' cellpadding='2' cellspacing='0' align='center' class='report_border'>";
		for($i=0; $i<sizeof($targetMiscTitleArray); $i++)
		{
			$SettingID = trim($targetMiscTitleArray[$i]);
			
			// 2012-0921-1641-51140
//			if($SettingMiscArray[$SettingID]==1 && $SettingID!="SubjectTeacherComment" && $SettingID!="MeritsAndDemerits")
//			{
//				$Title = $LangArray[$SettingID];
//				
//				
//				$Display = "";
				switch($SettingID)
				{
//					case "ClassTeacherComment":
//						$Display = nl2br($ParClassTeacherComment);
//						break;
//					case "Awards":
//						$Display = $StudentAwardArray[$ParStudentID][$ReportTemplateSemesterName];
//						break;
					case "ECA":
						//2014-0509-1103-37071
						//$Display = $StudentECAArray[$ParStudentID][$targetReportTemplateSemesterName];
						# ECA - Full Year only
						$Display = $StudentECAArray[$ParStudentID]['FULL'];
//						$Display = "";
//						if(count($StudentECAArray[$ParStudentID]) > 0){
//							$Display = "<br>";
//							# Display ECA of each term
//							foreach($StudentECAArray[$ParStudentID] as $termname => $ECAdetails){
//								$Display .= (isset($termname) && isset($ECAdetails))? (str_replace("FULL", "Full Year", $termname)).": ".$ECAdetails : "";
//							}
//						}
						break;
//					case "Remark":
//						$Display = $StudentRemarkArray[$ParStudentID][$ReportTemplateSemesterName];
//						break;
//					case "InterSchoolCompetition":
//						$Display = $StudentInterSchoolArray[$ParStudentID][$ReportTemplateSemesterName];
//						break;
//					case "SchoolService":
//						$Display = $StudentSchoolServiceArray[$ParStudentID][$ReportTemplateSemesterName];
//						break;
				}
				if (is_array($Display))
					$Display = implode('<br />', $Display);
				
				//2014-0509-1103-37071
//				if ($SettingID != 'ClassTeacherComment') {
				if ($SettingID != 'ClassTeacherComment' && $SettingID != 'ECA') {
					// show class teacher comment only
					continue;
				}
				$Title = $LangArray[$SettingID];
				if ($SettingID == 'ECA') {
					$Title = 'ECA';
				}
				$classTeacherCommentAry = explode("\n", $ParClassTeacherComment);
				$numOfComment = count($classTeacherCommentAry);
				
				if ($SettingID == 'ClassTeacherComment') {
					$MiscTable .= "<tr>";
					if($IsFirst==1) {
						$IsFirst = 0;
					}
					else {
						$top_border_style = "class='border_top'";
					}
					
					//2014-0509-1103-37071
					//$MiscTable .= "<td class='small_title' valign='top'>".$Title."</td></tr>";
					$MiscTable .= "<td class='small_title' valign='top'>".$Title."</td></tr>";
					# Add ECA below "Comments"
					$MiscTable .= $ECARowContent;
					for ($j=0; $j<$numOfComment; $j++) {
						$_comment = trim($classTeacherCommentAry[$j]);
						
						if ($_comment == '') {
							continue;
						}
						
						$_top_border_style = "border_top";
						$MiscTable .= "<tr><td class='small_text ".$_top_border_style."' valign='top'>".$_comment."</td></tr>";
					}
				}
				else if($SettingID == 'ECA'){
					//F81303 - F6 學生沒有記錄，無需要顯示ECA
					$showEca = true;
					if ($ParClassWebSAMSCode == "S6" && $Display == '') {
						$showEca = false;
					}
					
					if ($showEca) {
						$ECARowContent = "<tr><td class='border_top' valign='top'><span class='small_text'>".$Title.": ".$Display."</span></td></tr>";
					}
				}
				else{
					$MiscTable .= "<tr>";
					if($IsFirst==1) {
						$IsFirst = 0;
					}
					else {
						$top_border_style = "class='border_top'";
					}
//					$MiscTable .= "<td class='small_title' valign='top'>".$Title."</td></tr>";
//					$MiscTable .= "<tr><td class='small_text ".$top_border_style."' valign='top'>".$Display."</td></tr>";
					$MiscTable .= "<td valign='top'><span class='small_title'>".$Title.": </span><span class='small_text'>".$Display."</span></td></tr>";
				}

				$count++;
//			}
		}
		$MiscTable .= "</table>";
	}

	return $MiscTable;
}

# Function: Get summary display
# $previousSummaryArray: Contain required summary data for Attendance, Tardiness and TardinessOffence
function RETURN_SUMMARY_DISPLAY($ParInfoArray, $ParSettingID, $ClassHighestAverage, $StudentSummaryArray, $ParColumnSem)
{
	global $StudentMeritArray, $StudentID, $ReportID, $StudentAwardArray, $StudentRemarkArray, $StudentSummaryCalculateArray;
	global $FailedArray, $DistinctionArray, $PFArray, $libreportcard;
	list($GT, $AM, $GPA, $OMC, $OMCT, $OMF, $OMFT) = $ParInfoArray;
	
	
	switch($ParSettingID)
	{
		case "GrandTotal":				
				$Display = ($GT > 0) ? number_format($GT, 2) : "--";
				break;
		case "GPA":
				$Display = $GPA;
				break;
		case "AverageMark":
				$Display = number_format($AM, 2);
				if ($ParColumnSem!="FULL" || $libreportcard->HighlightOverall)
				{
					list($StylePrefix, $StyleSuffix, $IsDistinct, $IsFailed) = $libreportcard->GET_STYLE("S", $AM, $FailedArray[6], $DistinctionArray[6], $PFArray[6]);
					$Display = $StylePrefix.$Display.$StyleSuffix;
				}
				if (!($GT > 0)) $Display = "--";
				break;
		case "FormPosition":
				$TemplateSettingsArr = $libreportcard->GET_REPORT_TEMPLATE($ReportID);
				$ShowPosition = $TemplateSettingsArr['ShowPosition'];
				
				### Ivan 20091126 Check position display range
				if ($ShowPosition == 2)
				{
					$PositionRange = $TemplateSettingsArr['PositionRange'];
					$PositionRangeArr = explode(',', $PositionRange);
					$PositionLowerLimit = $PositionRangeArr[0];
					$PositionUpperLimit = $PositionRangeArr[1];
				
					if (($OMF>0) && ($GT > 0))
					{
						if ($OMF >= $PositionLowerLimit && $OMF <= $PositionUpperLimit)
							$Display = $OMF;
						else
							$Display = "--";
					}
					else
					{
						$Display = "--";
					}
					
				}
				else
				{
					$Display = (($OMF>0)&&($GT > 0)) ? $OMF : "--";
				}
				
				break;
		case "ClassPosition":
				$TemplateSettingsArr = $libreportcard->GET_REPORT_TEMPLATE($ReportID);
				$ShowPosition = $TemplateSettingsArr['ShowPosition'];
				
				### Ivan 20091126 Check position display range
				if ($ShowPosition == 1)
				{
					$PositionRange = $TemplateSettingsArr['PositionRange'];
					$PositionRangeArr = explode(',', $PositionRange);
					$PositionLowerLimit = $PositionRangeArr[0];
					$PositionUpperLimit = $PositionRangeArr[1];
					
					if ($OMC > 0)
					{
						if ($OMC >= $PositionLowerLimit && $OMC <= $PositionUpperLimit)
							$Display = $OMC;
						else
							$Display = "--";
					}
					else
					{
						$Display = "--";
					}
				}
				else
				{
					$Display = ($OMC>0) ? $OMC : "--";
				}
				
				break;
		case "ClassPupilNumber":
				$Display = $OMCT;
				if (!($GT > 0)) $Display = "--";
				break;
		case "FormPupilNumber":
				$Display = $OMFT;
				break;
		case "ClassHighestAverage":
				$Display = $ClassHighestAverage;
				break;
		case "PFRemarks":
				$Display = GET_REPORT_REMARKS($ParColumnSem);
				break;
				
		case "Conduct":
				$Display = ($StudentSummaryArray[0]=="") ? "--" : $StudentSummaryArray[0];
				break;
		case "Tidiness":
				$Display = ($StudentSummaryArray[1]=="") ? "--" : $StudentSummaryArray[1];
				break;
		case "Application":
				$Display = ($StudentSummaryArray[2]=="") ? "--" : $StudentSummaryArray[2];
				break;
		case "DaysAbsent":
				$Display = ($StudentSummaryArray[3]=="") ? "--" : $StudentSummaryArray[3];
				break;
		case "TimesLate":
//				$Display = ($StudentSummaryArray[4]=="") ? "--" : $StudentSummaryArray[4];
				$TimeLateContent = $StudentSummaryCalculateArray[$ParColumnSem]['TimesLate'];
				$Display = ($TimeLateContent == "" || $TimeLateContent === 0) ? "--" : $TimeLateContent;
				break;
		case "AbsentWOReason":
				$Display = ($StudentSummaryArray[5]=="") ? "--" : $StudentSummaryArray[5];
				break;
		
		
		case "Merits":
				$Display = '--';
				break;
		case "Attendance":
//				# Determine - Gain Attendance or not (DaysAbsent, TimesLate, AbsentWOReason => All Empty?)
//				$DisplayAttendance = is_array($previousSummaryArray) && ($StudentSummaryArray[3]=="" && $StudentSummaryArray[4]=="" && $StudentSummaryArray[5]=="" 
//										&& $previousSummaryArray[3]=="" && $previousSummaryArray[4]=="" && $previousSummaryArray[5]=="");
//				$Display = $DisplayAttendance? "1" : "--";
				$attendanceContent = $StudentSummaryCalculateArray[$ParColumnSem]['Attendance'];
				$Display = ($attendanceContent == "" || $attendanceContent === 0) ? "--" : $attendanceContent;
				break;
		case "Competitions":
				$Display = ($StudentAwardArray[$StudentID][$ParColumnSem][0]=="") ? "--" : $StudentAwardArray[$StudentID][$ParColumnSem][0];
				break;
		case "Performances":
				$Display = ($StudentAwardArray[$StudentID][$ParColumnSem][1]=="") ? "--" : $StudentAwardArray[$StudentID][$ParColumnSem][1];
				break;
		case "Services":
				$Display = ($StudentAwardArray[$StudentID][$ParColumnSem][2]=="") ? "--" : $StudentAwardArray[$StudentID][$ParColumnSem][2];
				break;
				
				
		case "Demerits":
				$Display = '--';
				break;
		case "Tardiness":
//				# Commented as Demerits - Tardiness now depends on Time Late only 
//				//$Display = ($StudentRemarkArray[$StudentID][$ParColumnSem][1]=="") ? "--" : $StudentRemarkArray[$StudentID][$ParColumnSem][1];
//				# Default Display
//				$Display = "--";
//			 	# Summary Array - contain all Times Late records
//				if(is_array($previousSummaryArray) && isset($previousSummaryArray['TimesLate'])){
//					# Total Number of Times Late up to current term
//					$currentTotalLateNum = array_sum(array_slice($previousSummaryArray['TimesLate'], 0, $previousSummaryArray['CurrentTermNumber']));
//					# Calculate total Offence - Tardines Number
//					$totalOffanceNumber = intval($currentTotalLateNum / 9);
//					# Calculate current Demerits - Tardiness Number
//					$Display = intval($currentTotalLateNum / 3) - $previousSummaryArray['Tardiness'] - (is_numeric($totalOffanceNumber)? $totalOffanceNumber : 0);
//					$Display = is_numeric($Display)? $Display : "--";
//				}
				$DemeritsContent = $StudentSummaryCalculateArray[$ParColumnSem]['Tardiness'];
				$Display = ($DemeritsContent == "" || $DemeritsContent === 0) ? "--" : $DemeritsContent;
				break;
		case "Forgetfulness":
				$Display = ($StudentRemarkArray[$StudentID][$ParColumnSem][0]=="") ? "--" : $StudentRemarkArray[$StudentID][$ParColumnSem][0];
				break;
		case "Misbehaviour":
				$Display = ($StudentRemarkArray[$StudentID][$ParColumnSem][1]=="") ? "--" : $StudentRemarkArray[$StudentID][$ParColumnSem][1];
				break;
				
				
		case "Offence":
				$Display = "--";
				break;
		case "TardinessOffence":
//				# Default Display
//				$Display = "--";
//				# Summary Array - contain all Times Late records
//				if(is_array($previousSummaryArray) && isset($previousSummaryArray['TimesLate'])){
//					# Total Number of Times Late up to current term
//					$currentTotalLateNum = array_sum(array_slice($previousSummaryArray['TimesLate'], 0, $previousSummaryArray['CurrentTermNumber']));
//					# Calculate current Offence - Tardiness Number
//					$Display = intval($currentTotalLateNum / 9) - $previousSummaryArray['TardinessOffence'];
//					$Display = is_numeric($Display)? $Display : "--";
//				}
				$OffenceContent = $StudentSummaryCalculateArray[$ParColumnSem]['TardinessOffence'];
				$Display = ($OffenceContent == "" || $OffenceContent === 0) ? "--" : $OffenceContent;
				break;
	}
	
	return $Display;
}

# Function: Get summary array
function GET_SUMMARY_ARRAY($ParInfoArray, $ParClassHighestAverage, $ParSummaryArray)
{
	global $SummaryTitleArray, $SettingSummaryArray, $ShowFullMark, $Semester, $ColumnArray, $LangArray, $SettingArray;
	global $ReportType, $currentReportTermNumber;
	global $libreportcard, $Year;

	$HideOverallResult = $SettingArray["HideOverallResult"];
	$ReturnArray = "";
	
	// remove no of pupils of future semesters
	/*
	if (sizeof($ParInfoArray)>0 && is_array($ParInfoArray))
	{
		foreach($ParInfoArray as $SemIndex => $DataArr)
		{
			$count = 0;
			for ($i=0; $i<sizeof($DataArr); $i++)
			{
				if ($DataArr[$i]!=0)
				{
					$count ++;
				}
			}
			debug_r($DataArr);
			debug($count, sizeof($DataArr));
			if ($count==1)
			{
				for ($i=0; $i<sizeof($DataArr); $i++)
				{
					$ParInfoArray[$SemIndex][$i] = 0;
				}
			}
		}
	}
	*/

//	debug_pr($SettingSummaryArray);
	if(!empty($SettingSummaryArray))
	{
		$count = 0;
		for($k=0; $k<sizeof($SummaryTitleArray); $k++)
		{
			$SettingID = trim($SummaryTitleArray[$k]);
			//if($SettingSummaryArray[$SettingID]==1 && $SettingID!="DaysAbsent" && $SettingID!="TimesLate" && $SettingID!="AbsentWOLeave")
			//if($SettingSummaryArray[$SettingID]==1)
			if ($SettingSummaryArray[$SettingID]==1 &&
			  	$SettingID!="DaysAbsent" && $SettingID!="TimesLate" && $SettingID!="AbsentWOLeave" && $SettingID!="AbsentWOReason" &&
				$SettingID!="Merit" && $SettingID!="Demerit" && $SettingID!="Conduct" &&
				$SettingID!="Politeness" && $SettingID!="Behaviour" && $SettingID!="Application" &&
				$SettingID!="Tidiness" &&

				$SettingID!="Motivation" &&$SettingID!="SelfConfidence" &&$SettingID!="SelfDiscipline" &&
				$SettingID!="Courtesy" &&$SettingID!="Honesty" &&$SettingID!="Responsibility" &&
				$SettingID!="Cooperation" &&
				
				$SettingID!="Merits" &&$SettingID!="Attendance" &&$SettingID!="Competitions" &&
				$SettingID!="Performances" &&$SettingID!="Services" &&
				
				$SettingID!="Demerits" &&$SettingID!="Forgetfulness" &&$SettingID!="Tardiness" &&
				$SettingID!="Misbehaviour" &&
				
				$SettingID!="Offence" && $SettingID!="TardinessOffence"
				)
			{
				$Title = $LangArray[$SettingID];
				$ReturnArray[$count]["Title"] = $Title;
				//debug_r($ParInfoArray);
				//debug_r($ColumnArray);
				for($i=0; $i<sizeof($ColumnArray); $i++)
				{
					$column = trim($ColumnArray[$i]["ColumnTitle"]);
					$column = $libreportcard->Get_Semester_By_SemesterName($column);
					//debug("column: ".$column);
					//debug("count: ".$count);					
					//debug_r($SemFlag);
					
					if(!empty($ParInfoArray[$column]))
					{
						//debug_r($column);
						if (!isset($SemFlag[$column]))
						{
							$SemFlag[$column] = $libreportcard->CHECK_RESULT_EXIST($Year, $column);
							if ($Semester=="FULL" || $Semester==0) $SemFlag[$column] = true;
						}
						$Display = ($SemFlag[$column]) ? RETURN_SUMMARY_DISPLAY($ParInfoArray[$column], $SettingID, $ParClassHighestAverage[$column], $ParSummaryArray[$column], $column) : 0;
						$ReturnArray[$count][$column] = $Display;
					}
				}
				if(!($Semester=="FULL" && $HideOverallResult==1))
				{

					$Display = RETURN_SUMMARY_DISPLAY($ParInfoArray[$Semester], $SettingID, $ParClassHighestAverage[$Semester], $ParSummaryArray[$Semester], $Semester);
					# Not show overall result when is not full year report
					//F81303 - 4 terms to 3 terms
					//$Display = ($currentReportTermNumber != 4 && $ReportType != "F")? "--" : $Display;
					$Display = ($currentReportTermNumber != 3 && $ReportType != "F")? "--" : $Display;
					$ReturnArray[$count][$Semester] = $Display;
				}
				
				$count++;
			}
		}
	}
	
	

	return $ReturnArray;
}

# Function: Get conduct array
function GET_CONDUCT_ARRAY($ParInfoArray, $ParClassHighestAverage, $ParSummaryArray, $ParColumnArray, $ParAwardArray, $ParRemarkAry)
{
	global $SummaryTitleArray, $SettingSummaryArray, $ShowFullMark, $Semester, $ColumnArray, $LangArray, $SettingArray;
	global $ReportType, $currentReportTermNumber;
	
	
	$HideOverallResult = $SettingArray["HideOverallResult"];
	$ReturnArray = "";
	$totalAttendance = 0;
//	$tardinessCountArr = array('CurrentTermNumber' => 0, 'TimesLate' => array(), 'Tardiness' => 0, 'TardinessOffence' => 0);
	$tardinessCountArr = array();
//	debug_r($ColumnArray);
	
	if(!empty($SettingSummaryArray))
	{
		$count = 0;
		# Row - Settings Type
		for($k=0; $k<sizeof($SummaryTitleArray); $k++)
		{
			$SettingID = trim($SummaryTitleArray[$k]);
			
			//if($SettingSummaryArray[$SettingID]==1 && $SettingID!="DaysAbsent" && $SettingID!="TimesLate" && $SettingID!="AbsentWOLeave")
			//if($SettingSummaryArray[$SettingID]==1)
			if($SettingSummaryArray[$SettingID]==1 &&
				$SettingID!="GrandTotal" && $SettingID!="GPA" && $SettingID!="AverageMark" &&
				$SettingID!="ClassHighestAverage" && $SettingID!="ClassPupilNumber" &&
				$SettingID!="ClassPosition" && $SettingID!="FormPosition" && $SettingID!="FormPupilNumber" && 
				$SettingID!="PFRemarks"
				)
			{
				
				$Title = $LangArray[$SettingID];
				$ReturnArray[$count]["Title"] = $Title;
				
				# Column - Term
				for($i=0; $i<sizeof($ColumnArray); $i++)
				{
					$column = trim($ColumnArray[$i]["ColumnTitle"]);

//					debug_pr("column: ".$column);
					//debug_pr("count: ".$count);
					//debug_pr("title: ".$Title);
					//if(!empty($ParInfoArray[$column]))
//					if(!empty($ParSummaryArray[$column]))
//					{
//						$Display = RETURN_SUMMARY_DISPLAY($ParInfoArray[$column], $SettingID, $ParClassHighestAverage[$column], $ParSummaryArray[$column], $column);
//						$ReturnArray[$count][$column] = $Display;
//					}
					
					if(!empty($ParSummaryArray[$column]))
					{
						# Default Display
						$Display = RETURN_SUMMARY_DISPLAY($ParInfoArray[$column], $SettingID, $ParClassHighestAverage[$column], $ParSummaryArray[$column], $column);
						
//						# For Attendance only
//						if ($SettingID == "Attendance"){
//							# Summary array of previous term (for even terms only)
//							$previousAttendanceSummaryArr = ($i > 0 && $i % 2)? (array)$ParSummaryArray[trim($ColumnArray[$i-1]["ColumnTitle"])] : null;
//							# Pass $previousSummaryArr to Calculate Attendance Number
//							$Display = RETURN_SUMMARY_DISPLAY($ParInfoArray[$column], $SettingID, $ParClassHighestAverage[$column], $ParSummaryArray[$column], $column, $previousAttendanceSummaryArr);
//							$totalAttendance += is_numeric($Display)? $Display : 0;
//						}
//						
//						# For TimeLate & Tardiness only
//						if(isset($tardinessCountArr[$SettingID])){
//						if($SettingID == "TimesLate" || $SettingID == "Tardiness" || $SettingID == "TardinessOffence"){
//							$tardinessCountArr['CurrentTermNumber'] = ($i + 1);
//							# Obtain Display and array that store tardiness result
//							list($Display, $tardinessCountArr) = RETURN_SUMMARY_DISPLAY($ParInfoArray[$column], $SettingID, $ParClassHighestAverage[$column], $ParSummaryArray[$column], $column, $tardinessCountArr);
//							# Store TimeLate number of each terms
//							if($SettingID == 'TimesLate'){
//								$tardinessCountArr[$SettingID][$i] = is_numeric($Display)? $Display : 0;
//								$tardinessCountArr[$i][$SettingID] = is_numeric($Display)? $Display : 0;
//								$tardinessCountArr['Overall'][$SettingID] += is_numeric($Display)? $Display : 0;
//							}
//							# Add to Total number of Tardiness
//							else {
//								$tardinessCountArr[$SettingID] += is_numeric($Display)? $Display : 0;
//								$tardinessCountArr[$i][$SettingID] = is_numeric($Display)? $Display : 0;
//								$tardinessCountArr['Overall'][$SettingID] += is_numeric($Display)? $Display : 0;
//							}
//						}
						$ReturnArray[$count][$column] = $Display;
					}
				}
				
				# Overall Result
				if(!($Semester=="FULL" && $HideOverallResult==1))
				{
//					# For Attendance only - Display Total Attendance Number
//					if ($SettingID == "Attendance"){
//						$Display = $totalAttendance;
//					} 
//					# For Tardiness only - Display Total Tardiness Number
//					else if($SettingID == "Tardiness" || $SettingID == "TardinessOffence") {
//						$Display = $tardinessCountArr['Overall'][$SettingID];
//					}
//					else {
						//$Display = RETURN_SUMMARY_DISPLAY($ParInfoArray[$Semester], $SettingID, $ParClassHighestAverage[$Semester], $ParSummaryArray[$Semester], $Semester);
						$Display = RETURN_SUMMARY_DISPLAY($ParInfoArray[$Semester], $SettingID, $ParClassHighestAverage[$Semester], $ParSummaryArray[$Semester], $Semester);
//					}
					
					//F81303 - 4 terms to 3 terms
					//$Display = ($currentReportTermNumber != 4 && $ReportType != "F")? '--' : $Display;
					$Display = ($currentReportTermNumber != 3 && $ReportType != "F")? '--' : $Display;
					$ReturnArray[$count][$Semester] = $Display;
				}
				$count++;
			}
		}
	}
//	debug_pr($ReturnArray);
	return $ReturnArray;
}

# function to get signature table
function GENERATE_SIGNATURE_TABLE()
{
	global $eReportCard, $SettingArray, $LangArray;
	global $SignatureWidth;

	$SignatureTitleArray = $eReportCard['DisplaySettingsArray']["Signature"];
	$SettingSignatureArray = $SettingArray["Signature"];

	$CellNumber = 0;
	$SignatureRow = "";

	for($i=0; $i<sizeof($SignatureTitleArray); $i++)
	{	
		$bottom_border = $i == (sizeof($SignatureTitleArray) - 1)? " " : "report_formfieldtitle";
		$SettingID = trim($SignatureTitleArray[$i]);
		if($SettingSignatureArray[$SettingID]==1)
		{
			$Title = $LangArray[$SettingID];
			if ($Title=="Class Teacher")
			{
				$Title .= "'s Signature";
			}
			$SignatureRow .= "<tr>";
			//$SignatureRow .= "<td class='small_title ".$bottom_border."' valign='top' height='75'><span style='width:{$SignatureWidth}px'>".$Title."</span></td>";
			$SignatureRow .= "<td class='small_title report_formfieldtitle' valign='top' height='150'><span style='width:{$SignatureWidth}px'>".$Title."</span></td>";
			$SignatureRow .= "</tr>";

			$CellNumber++;
		}
	}
	
	if($CellNumber>0)
	{
		$SignatureRow .= "<tr>";
		$SignatureRow .= "<td class='small_title' valign='top' height='5'>";
			$SignatureRow .= "<span style='width:{$SignatureWidth}px'>";
				//2017-0110-1200-20236
//				$SignatureRow .= "*Maximum = 100 <br>";
//				$SignatureRow .= "<span style='visibility: hidden;'>*</span>Pass = 50 <br>";
//				$SignatureRow .= "<span style='visibility: hidden;'>*</span>Fail : Below 50";
				$SignatureRow .= "*Maximum : 100 <br>";
				$SignatureRow .= "<span style='visibility: hidden;'>*</span>Pass : 50 or above<br>";
				$SignatureRow .= "<span style='visibility: hidden;'>*</span>Fail : Below 50";
			$SignatureRow .= "</span>";
		$SignatureRow .= "</td>";
		$SignatureRow .= "</tr>";
			
		$SignatureTable = "<table width='100%' height='100%' border='0' cellpadding='2' cellspacing='0' align='center' valign='top' style='min-height:100%;'>";
		$SignatureTable .= $SignatureRow;
		$SignatureTable .= "</table>";
	}

	return $SignatureTable;
}

function GET_SEMESTER_NUMBER($ParSemester)
{
	global $libreportcard, $SemesterArray;

	$sem_number = "-1";
	if(trim($ParSemester)=="FULL" || $ParSemester === 0 || $ParSemester === '0')
	{
		$sem_number = 0;
	} 
	else
	{
		/*
		for($i=0; $i<sizeof($SemesterArray); $i++)
		{
			if(trim($ParSemester)==trim($SemesterArray[$i]))
			{
				$sem_number = $i+1;
				break;
			}
		}
		*/
				
		$sql = "Select
						YearTermID
				From
						ACADEMIC_YEAR_TERM
				Where
						AcademicYearID = '".$libreportcard->Year."'
						And
						(
							YearTermNameEN Like '%".$libreportcard->Get_Safe_Sql_Like_Query($ParSemester)."%'
							Or
							YearTermNameB5 Like '%".$libreportcard->Get_Safe_Sql_Like_Query($ParSemester)."%'
						)
				";
		$ResultArr = $libreportcard->returnArray($sql);
		
		if (count($ResultArr) > 0)
			$sem_number = $ResultArr[0]['YearTermID'];
		
	}
	return $sem_number;
}

function CHECK_CSV_FILE_FORMAT($ParHeaderArray, $ParDefaultHeaderArray)
{
	# Check Title Row
	$format_wrong = false;

	for ($i=0; $i<sizeof($ParDefaultHeaderArray); $i++)
	{
		if ($ParHeaderArray[$i]!=$ParDefaultHeaderArray[$i])
		{
			$format_wrong = true;
			break;
		}
	}

	return $format_wrong;
}

function GET_CSV_FILE_CONTENT($ParFile, $ParDefaultHeaderArray, $debug=0)
{
	global $PATH_WRT_ROOT, $libreportcard, $lf, $StudentRegNoArray, $SemesterArray;
	
	include_once($PATH_WRT_ROOT."includes/libimporttext.php");
	$limport = new libimporttext();
	
	$ClassArr = $libreportcard->GET_CLASSES();
	
	$TargetArray = $SemesterArray;
	$TargetArray[] = "FULL";
	for($i=0; $i<sizeof($TargetArray); $i++)
	{
		$t_semester = trim($TargetArray[$i]);
		$sem_number = GET_SEMESTER_NUMBER($t_semester);
		if($sem_number>=0)
		{
			for ($j=0; $j<sizeof($ClassArr); $j++)
			{
				$TempClass = str_replace(".", "", str_replace("/", "", $ClassArr[$j]));
				$TargetFile = $ParFile."/".trim($libreportcard->YearName)."_".$sem_number."_".$TempClass.".csv";
				
				if(file_exists($TargetFile))
				{
					//$data = $lf->file_read_csv($TargetFile);
					$data = $limport->GET_IMPORT_TXT($TargetFile, 0, '<br />');

					if(!empty($data))
					{
						$header_row = array_shift($data);
						$wrong_format = CHECK_CSV_FILE_FORMAT($header_row, $ParDefaultHeaderArray);
						if(!$wrong_format)
						{
							for($k=0; $k<sizeof($data); $k++)
							{
								$reg_no = array_shift($data[$k]);
								$reg_no = trim(str_replace("#", "", $reg_no));
								$student_id = trim($StudentRegNoArray[$reg_no]);
								
								if($student_id!="")
								{
									if(sizeof($data[$k])>1) {
										//$ReturnArray[$student_id][$sem_number] = $data[$k];
										$ReturnArray[$student_id][$t_semester] = $data[$k];
									} else if(sizeof($data[$k])== 1) {
										//$ReturnArray[$student_id][$sem_number] .= $data[$k][0]."<br/>";
										$ReturnArray[$student_id][$t_semester] .= $data[$k][0]."<br/>";
									}
								}
							}
						}
					}
				}
			}
		}
	}
	//hdebug_r($SemesterArray);
	//h _r($ReturnArray);
	
	return $ReturnArray;
}



# Function: Get result table
function GENERATE_RESULT_TABLE($ParSignatureTable, $ParAttendanceMeritTable, $ParStudentResultArray, $ParSubjectTeacherCommentArray)
{
	global $eReportCard, $SubjectArray, $SubjectUnitArr, $SubjectCodeMappingArr, $ColumnArray, $ReportCellSetting, $ShowFullMark, $libreportcard;
	global $FailedArray, $DistinctionArray, $PFArray, $SettingArray;
	global $AllSubjectArray, $ShowOverallPosition, $OverallPositionRange, $HideNotEnrolled;
	global $ShowSubjectTeacherComment, $Semester, $ReportType, $currentReportTermNumber;
	global $SummaryResultArray, $LangArray, $BilingualReportTitle, $ConductResultArray;
	global $LineHeight;
	global $eRC_custom;
	
	# Report Type
	# 1 - Display the overall result of Parent Subject AND Component Subject(s)
	# 2 - Display the overall result of Parent Subject ONLY
	# 3 - Display the overall result of Component Subject(s) ONLY
	$ResultDisplayType = $SettingArray["ResultDisplayType"];
	$ResultCalculationType = $SettingArray["ResultCalculationType"];
	$SubjectTitleType = $SettingArray["SubjectTitleType"];
	$ShowColumnPercentage = $SettingArray["ShowColumnPercentage"];
	$MarkTypeDisplay = $SettingArray["MarkTypeDisplay"];
	$HideOverallResult = $SettingArray["HideOverallResult"];

	# get subject title cell
	if($BilingualReportTitle==1 || $SubjectTitleType==0) {
		$SubjectTitleCell = "<td width='50%' class='small_title'>".$eReportCard['EngSubject']."</td><td width='50%' class='small_title'>".$eReportCard['ChiSubject']."</td>";
	}
	else {
		$SubjectTitleCell = "<td class='small_title'>".$eReportCard['Subject']."</td>";
	}

	$ExtraColumn = 0;
	$ColumnSize = sizeof($ColumnArray);
	//$ColumnWidth = round(100/($ColumnSize+3));
	// 2014-0606-1609-55194
	//$subjectColWidth = 23;
	//$subjectColWidth = 35;
	$subjectUnitColWidth = 6;
	//$signatureColWidth = 25;
	$signatureColWidth = 22;
	
	$ColumnWidth = 11;
	$totalMarkColWidth = $ColumnWidth * ($ColumnSize+1);
	
	//$subjectColWidth = 100 - $signatureColWidth - $subjectUnitColWidth - $totalMarkColWidth;		
	//$totalMarkColWidth = 100 - $subjectColWidth - $signatureColWidth - $subjectUnitColWidth;			// 100 - (subject col width) - (signature col width)
	//$ColumnWidth = ceil($totalMarkColWidth/ ($ColumnSize+1));	// +1 means the overall result column
	
	$ResultTable = "<table width='100%' border='0' cellspacing='0' cellpadding='0' class='report_border' style='min-height:500px;'>";

	$ResultTable .= "<tr>
						<td class='report_formfieldtitle' height='100%'>
							<table width='100%' height='100%' border='0' cellpadding='0' cellspacing='0'>
							<tr valign='top'><td align='right' class='small_title' colspan='2'>&nbsp;</td></tr>
							<tr valign='bottom'>".$SubjectTitleCell."</tr>
							</table>
						</td>";

	/*
	$ResultTable .= "<tr>
						<td width='25%' class='report_formfieldtitle' height='100%'>
							<table width='100%' height='100%' border='0' cellpadding='2' cellspacing='0'>
							<tr>".$SubjectTitleCell."</tr>
							</table>
						</td>";
						*/
	
	# Commented - Replace Full Marks column by Subject Unit
//	if($ShowFullMark==1)
//	{
//		$ResultTable .= "<td width='5%' class='border_left small_title report_formfieldtitle' align='center'>".$LangArray['FullMark']."</td>";
//		$ExtraColumn++;
//	}		
	# Suject Unit Column
	$ResultTable .= "<td width='".$subjectUnitColWidth."%' class='border_left small_title report_formfieldtitle'>
						<table width='100%' height='100%' border='0' cellpadding='0' cellspacing='0'>
						<tr valign='top'><td align='right' class='small_title'>".$LangArray['Marks']."</td></tr>
						<tr valign='bottom'><td align='left' class='small_title'>".$LangArray['SubjectUnit']."</td></tr>
					</table></td>";
	$ExtraColumn++;

	list($op_from, $op_to) = explode(",", $OverallPositionRange);
	for($i=0; $i<sizeof($ColumnArray); $i++)
	{
		list($ReportColumnID, $ColumnTitle, $Weight, $ShowPosition, $PositionRange) = $ColumnArray[$i];
		if($Weight!="")
		{
			$ColumnTitle = ($ShowColumnPercentage==1) ? $ColumnTitle."&nbsp;".$Weight."%" : $ColumnTitle;
			$WeightArray[$ReportColumnID] = $Weight;
		}
		
//		$ColumnTitle = str_replace(' Term', '<br>Term', $ColumnTitle); 

		$ResultTable .= "<td class='border_left report_formfieldtitle' width='{$ColumnWidth}%'><table border=0 cellpadding=0 cellspacing=0 width='100%'>";
		$ResultTable .= "<tr height='75%'><td align='center' class='small_title' colspan='3' width='100%' style=''>".$ColumnTitle."</td></tr>";
		if($ShowPosition==1 || $ShowPosition==2)
		{
			$ResultTable .= "<tr>";
			$ResultTable .= "<td align='center' width='50%' class='reportcard_text'>".$LangArray['Mark']."</td>";
			$ResultTable .= "<td width='1'>&nbsp;</td>";
			$ResultTable .= "<td align='center' width='50%' class='reportcard_text'>(".$LangArray['Position'].")</td>";
			$ResultTable .= "</tr>";
		}
		$ResultTable .= "</table></td>";
	}

	if($ColumnSize>1 && $HideOverallResult!=1)
	{
//		$overallResultLang = "Overall<br>Result";
		$ResultTable .= "<td class='border_left report_formfieldtitle' width='{$ColumnWidth}%'><table border=0 cellpadding=0 cellspacing=0 width='100%' align='center'>";
		$ResultTable .= "<tr height='75%'><td align='center' class='small_title' colspan='3'>".$LangArray['OverallResult']."</td></tr>";
//		$ResultTable .= "<tr height='75%'><td align='center' class='small_title' colspan='3'>".$overallResultLang."</td></tr>";
		if(($ShowOverallPosition==1 || $ShowOverallPosition==2) && !$eRC_custom['HideOverallSubjectPosition'])
		{
			$ResultTable .= "<tr>";
			$ResultTable .=  "<td align='center' width='50%' class='reportcard_text'>".$LangArray['Mark']."</td>";
			$ResultTable .= "<td width='1'>&nbsp;</td>";
			$ResultTable .= "<td align='center' width='50%' class='reportcard_text'>(".$LangArray['Position'].")</td>";
			$ResultTable .= "</tr>";
		}
		$ResultTable .= "</table></td>";
		$ExtraColumn++;
	}

	if($ShowSubjectTeacherComment==1) {
		$ResultTable .= "<td width='10%' class='border_left small_title report_formfieldtitle' align='center'>".$LangArray['SubjectTeacherComment']."</td>";
	}

	if ($ParSignatureTable!="" || $ParAttendanceMeritTable!="")
	{
			$ResultTable .= "<td height='100%' rowspan='50' class='border_left' width='2'><img src='/images/spacer.gif' width='2' height='1' border='0' /></td>";
			$ResultTable .= "<td width='".$signatureColWidth."%' height='100%' rowspan='50' class='border_left' valign='top'>";
			$ResultTable .= "<table border='0' height='100%' width='100%' cellpadding='0' cellspacing='0'>";
			$ResultTable .= ($ParSignatureTable!="") ? "<tr><td valign='top'>".$ParSignatureTable."</td></tr>" : "";
			//$ResultTable .= ($ParAttendanceMeritTable!="") ? "<tr><td valign='bottom'>".$ParAttendanceMeritTable."</td></tr>" : "";
			$ResultTable .= "</table>";
			$ResultTable .= "</td>";
	}
	$ResultTable .= "</tr>";

	$ColumnSpan = $ColumnSize + $ExtraColumn + 1;
	$IsFirst = 1;
	//$HasAbsent = false;		// Hide GrandTotal and GrandAverage if there are any absent score in any subjects
	$HasAbsentArr = array();
	$HasFailArr = array();
	
	# Subject Table
	if(is_array($SubjectArray))
	{
		$count = 0;
		foreach($SubjectArray as $CodeID => $Data)
		{
			if(is_array($Data))
			{
				foreach($Data as $CmpCodeID => $InfoArr)
				{
					list($ReportSubjectID, $SubjectID, $EngSubjectName, $ChiSubjectName, $FullMark, $Scale, $HidePosition) = $InfoArr;
					# Hide not enrolled subject according to the setting
					//if($HideNotEnrolled==1 && (empty($ParStudentResultArray[$SubjectID]["Overall"]) || strcmp($ParStudentResultArray[$SubjectID]["Overall"]["Grade"], "/")==0))
					if ($HideNotEnrolled==1 && !$libreportcard->CHECK_ENROL($ParStudentResultArray[$SubjectID]))
					{
						continue;
					}
//					debug_pr($SubjectArray[$CodeID]);
//					debug_pr($ParStudentResultArray[$SubjectID]);

					//$IsComponent = ($CmpCodeID>0) ? 1 : 0;
					$IsComponent = ($CmpCodeID != '') ? 1 : 0;
					$top_style = ($IsComponent==0 && $IsFirst==0) ? "border_top" : "";
					$IsFirst = 0;
					$Prefix = ($IsComponent==1) ? "&nbsp;&nbsp;&nbsp;&nbsp;" : "";
					
					# Highest Grade
					$MaxGrade = $DistinctionArray[$SubjectID]["G"];
					$FirstRowSetting = $ReportCellSetting[$ColumnArray[0][0]][$ReportSubjectID];
					$FullMark = ($FirstRowSetting=="G") ? $MaxGrade : $FullMark;

					# check whether is parent/component subject
					$IsParent = (sizeof($AllSubjectArray[$SubjectID])>1) ? 1 : 0;
					$IsComponent = (empty($AllSubjectArray[$SubjectID])) ? 1 : 0;

					$ResultShow = (!($ResultDisplayType==2 && $IsParent==1) && !($IsParent==1 && $ResultCalculationType==1)) ? 1 : 0;
					$OverallShow = (!($ResultDisplayType==2 && $IsParent==1) && !($ResultDisplayType==1 && $IsComponent==1)) ? 1 : 0;

					# get subject display cell
					if($BilingualReportTitle==1 || $SubjectTitleType==0) {
//						$SubjectCell = "<td>
//								<table border='0' cellpadding='1' cellspacing='0' width='100%' height='100%'>
//								<tr>
//									<td width='50%' class='small_text'>".$Prefix.$EngSubjectName."</td>
//									<td width='50%' class='small_text'>".$Prefix.$ChiSubjectName."</td>
//								</tr>
//								</table>
//								</td>";
						$SubjectCell = "<td>
								<table border='0' cellpadding='1' cellspacing='0' width='100%' height='100%'>
								<tr>
									<td width='50%' class='tabletext'>".$Prefix.$EngSubjectName."</td>
									<td width='50%' class='tabletext'>".$Prefix.$ChiSubjectName."</td>
								</tr>
								</table>
								</td>";
					}
					else {
						$SubjectDisplayName = ($SubjectTitleType==1) ? $EngSubjectName : $ChiSubjectName;
//						$SubjectCell = "<td>
//										<table border='0' cellpadding='1' cellspacing='0' width='100%' height='100%'>
//										<tr>
//											<td class='small_text'>".$Prefix.$SubjectDisplayName."</td>
//										</tr>
//										</table>
//										</td>";
						$SubjectCell = "<td>
										<table border='0' cellpadding='1' cellspacing='0' width='100%' height='100%'>
										<tr>
											<td class='tabletext'>".$Prefix.$SubjectDisplayName."</td>
										</tr>
										</table>
										</td>";
					}

					$ResultTable .= "<tr>";
					// line height - aki
					$ResultTable .= "<td class='$top_style'>
										<table width='100%' border='0' cellpadding='0' cellspacing='0' height='$LineHeight'><tr>".$SubjectCell."</tr></table>
									</td>";
//					$ResultTable .= ($ShowFullMark==1) ? "<td class='tabletext border_left $top_style' align='center'>".($ResultShow==1?$FullMark:"&nbsp;")."</td>" : "";
					
					# Display Subject Unit
//					$CurrentSubjectUnit = $SubjectUnitArr['subjectUnitAry'][$unitCount]['unit'];
//					$CurrentSubjectUnit = is_numeric($CurrentSubjectUnit)? $CurrentSubjectUnit : "&nbsp;";
					
					$currentSubjectCode = trim($SubjectCodeMappingArr[$SubjectID]);
					list($CurrentSubjectUnit, $RelatedSubjectCodeAry) = GET_SUBJECT_UNIT($currentSubjectCode, $SubjectUnitArr);
					
					# Subject with Combined Unit
//					if(count($SubjectUnitArr['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry']) > 1){
					if(count($RelatedSubjectCodeAry) > 1){
						$firstSubjectCode = trim($RelatedSubjectCodeAry[0]);
						# Display Combined Subject Unit - First subject
						if($currentSubjectCode == $firstSubjectCode){
							$ResultTable .= "<td class='tabletext border_left $top_style' align='center' rowspan=2>".$CurrentSubjectUnit."</td>";
						}
					} 
					# Normal Subject
					else {
						$ResultTable .= "<td class='tabletext border_left $top_style' align='center'>".$CurrentSubjectUnit."</td>";
					}
					
					for($i=0; $i<sizeof($ColumnArray); $i++)
					{
						list($ReportColumnID, $ColumnTitle, $Weight, $ShowPosition, $PositionRange) = $ColumnArray[$i];
						$CellSetting = $ReportCellSetting[$ReportColumnID][$ReportSubjectID];
						list($p_form, $p_to) = explode(",", $PositionRange);

						$DisplayResult = "";
						$IsFailed = false;
						
						if(!$ResultShow)
						{
							$ResultTable .= "<td class='border_left $top_style' align='center'>&nbsp;</td>";
						}
						else
						{
							if($CellSetting!="N/A")
							{
								$StylePrefix = "";
								$StyleSuffix = "";
								if($CellSetting=="S")
								{
									$s_raw_mark = $ParStudentResultArray[$SubjectID][$ReportColumnID]["RawMark"];
									$s_weighted_mark = $ParStudentResultArray[$SubjectID][$ReportColumnID]["WeightedMark"];
									$s_grade = $ParStudentResultArray[$SubjectID][$ReportColumnID]["Grade"];
									
									if ($s_grade == 'abs'){
										//$HasAbsent = true;
										$HasAbsentArr[$ReportColumnID] = true;
									}

									if($s_raw_mark>=0)
									{
										if($MarkTypeDisplay==2)
										{
											$DisplayResult = $s_grade;
											list($StylePrefix, $StyleSuffix, $IsDistinct, $IsFailed) = $libreportcard->GET_STYLE("G", $DisplayResult, $FailedArray[$SubjectID], $DistinctionArray[$SubjectID], $PFArray[$SubjectID]);
										} else
										{
											list($StylePrefix, $StyleSuffix, $IsDistinct, $IsFailed) = $libreportcard->GET_STYLE($CellSetting, $s_raw_mark, $FailedArray[$SubjectID], $DistinctionArray[$SubjectID], $PFArray[$SubjectID]);
											$DisplayResult = ($MarkTypeDisplay==1) ? $s_raw_mark : $s_weighted_mark;
										}
									}
									else
										$DisplayResult = $libreportcard->SpecialMarkArray[trim($s_raw_mark)];
								} else
								{
									$DisplayResult = $ParStudentResultArray[$SubjectID][$ReportColumnID]["Grade"];
									if($DisplayResult!="/" && $DisplayResult!="abs")
									{
										list($StylePrefix, $StyleSuffix, $IsDistinct, $IsFailed) = $libreportcard->GET_STYLE($CellSetting, $DisplayResult, $FailedArray[$SubjectID], $DistinctionArray[$SubjectID], $PFArray[$SubjectID]);
									} 
									else if($DisplayResult == "abs") 
									{
										$HasAbsentArr[$ReportColumnID] = true;
									}
								}
								
								if ($IsFailed) {
									$DisplayResult = $DisplayResult.'*';
									$HasFailArr[$ReportColumnID] = true;
								}
								$DisplayResult = $StylePrefix.$DisplayResult.$StyleSuffix;
								
								$FormPosition = $ParStudentResultArray[$SubjectID][$ReportColumnID]["OMF"];
								$ClassPosition = $ParStudentResultArray[$SubjectID][$ReportColumnID]["OMC"];
							}

							$ResultTable .= "<td class='border_left $top_style' width='10%'><table border=0 cellpadding=0 cellspacing=0 width='100%'>";
							$ResultTable .= "<tr>";
							if($ShowPosition==1 || $ShowPosition==2)
							{
								$ResultTable .= "<td align='center' class='tabletext' width='50%'>".($DisplayResult==""?"--":$DisplayResult)."</td>";
								$ResultTable .= "<td width='1'>&nbsp;</td>";

								$PosDisplay = ($ShowPosition==1) ? $ClassPosition : $FormPosition;
								$PosDisplay = ($PosDisplay>0 && ($PosDisplay>=$p_from && $PosDisplay<=$p_to)) ? $PosDisplay : "*";
								$ResultTable .= ($HidePosition==0 && $CellSetting=="S") ? "<td align='center' class='tabletext' width='50%'>(".$PosDisplay.")</td>" : "<td align='center' class='tabletext width='50%''>(*)</td>";
							}
							else
							{
								$ResultTable .= "<td align='center' class='tabletext'>".(($DisplayResult=="")?"--":$DisplayResult)."</td>";
							}
							$ResultTable .= "</tr>";
							$ResultTable .= "</table></td>";
						}
					}

					if($ColumnSize>1 && $HideOverallResult!=1)
					{
						if(!$OverallShow)
						{
							$ResultTable .= "<td class='tabletext border_left $top_style' align='center'>&nbsp;</td>";
						}
						//F81303 - 4 terms to 3 terms
						//else if ($currentReportTermNumber != 4 && $ReportType != "F"){
						else if ($currentReportTermNumber != 3 && $ReportType != "F"){
							$ResultTable .= "<td class='tabletext border_left $top_style' align='center'>--</td>";
						}
						else
						{
							$StylePrefix = "";
							$StyleSuffix = "";
							$IsDistinct = "";
							$IsFailed = "";
							if($CellSetting=="S")
							{
								$s_overall_mark = $ParStudentResultArray[$SubjectID]["Overall"]["Mark"];
								$s_overall_grade = $ParStudentResultArray[$SubjectID]["Overall"]["Grade"];
								
								

								if($s_overall_mark>=0)
								{
									# Commented if loop: $libreportcard->HighlightOverall - To display Highlight
									if($MarkTypeDisplay==2)
									{
										$OverallResult = $s_overall_grade;
//										if ($libreportcard->HighlightOverall)
//										{
//											list($StylePrefix, $StyleSuffix, $IsDistinct, $IsFailed) = $libreportcard->GET_STYLE("G", $OverallResult, $FailedArray[$SubjectID], $DistinctionArray[$SubjectID], $PFArray[$SubjectID]);
//										}
										list($StylePrefix, $StyleSuffix, $IsDistinct, $IsFailed) = $libreportcard->GET_STYLE("G", $OverallResult, $FailedArray[$SubjectID], $DistinctionArray[$SubjectID], $PFArray[$SubjectID]);
									} else
									{
										$OverallResult = $s_overall_mark;
//										if ($libreportcard->HighlightOverall)
//										{
//											list($StylePrefix, $StyleSuffix, $IsDistinct, $IsFailed) = $libreportcard->GET_STYLE($CellSetting, $OverallResult, $FailedArray[$SubjectID], $DistinctionArray[$SubjectID], $PFArray[$SubjectID]);
//										}
										list($StylePrefix, $StyleSuffix, $IsDistinct, $IsFailed) = $libreportcard->GET_STYLE($CellSetting, $OverallResult, $FailedArray[$SubjectID], $DistinctionArray[$SubjectID], $PFArray[$SubjectID]);
									}
								} else
								{
									$OverallResult = $libreportcard->SpecialMarkArray[trim($s_overall_mark)];
								}
							} else
							{
								$OverallResult = $ParStudentResultArray[$SubjectID]["Overall"]["Grade"];
								
								if($OverallResult!="/" && $OverallResult!="abs")
								{
									# Commented if loop: $libreportcard->HighlightOverall - To display Highlight
//									if ($libreportcard->HighlightOverall)
//									{
//										list($StylePrefix, $StyleSuffix, $IsDistinct, $IsFailed) = $libreportcard->GET_STYLE($CellSetting, $OverallResult, $FailedArray[$SubjectID], $DistinctionArray[$SubjectID], $PFArray[$SubjectID]);
//									}
									list($StylePrefix, $StyleSuffix, $IsDistinct, $IsFailed) = $libreportcard->GET_STYLE($CellSetting, $OverallResult, $FailedArray[$SubjectID], $DistinctionArray[$SubjectID], $PFArray[$SubjectID]);
								}
							}
							if ($OverallResult=="/")
							{
								$OverallResult = "--";
							}
							$OverallResult = $StylePrefix.$OverallResult.$StyleSuffix;
							$OverallFormPosition = $ParStudentResultArray[$SubjectID]["Overall"]["OMF"];
							$OverallClassPosition = $ParStudentResultArray[$SubjectID]["Overall"]["OMC"];
							
							if ($IsFailed) {
								$OverallResult .= $StylePrefix.'*'.$StyleSuffix;
								$HasFailArr['Overall'] = true;
							}
								
							$ResultTable .= "<td class='border_left $top_style' width='10%'><table border=0 cellpadding=0 cellspacing=0 width='100%'>";
							$ResultTable .= "<tr>";
							if(($ShowOverallPosition==1 || $ShowOverallPosition==2) && !$eRC_custom['HideOverallSubjectPosition'])
							{
								$ResultTable .= "<td align='center' class='tabletext' width='50%'>".($OverallResult==""?"--":$OverallResult)."</td>";
								$ResultTable .= "<td>&nbsp;</td>";

								$OverallPosDisplay = ($ShowOverallPosition==1) ? $OverallClassPosition : $OverallFormPosition;
								$OverallPosDisplay = ($OverallPosDisplay>0 && ($OverallPosDisplay>=$op_from && $OverallPosDisplay<=$op_to)) ? $OverallPosDisplay : "*";
								//aki
								if ($IsFailed) $OverallPosDisplay = "*";
								$ResultTable .= ($HidePosition==0 && $CellSetting=="S") ? "<td align='center' class='tabletext' width='50%'>(".$OverallPosDisplay.")</td>" : "<td align='center' class='tabletext' width='50%'>(*)</td>";
							}
							else
							{
								$ResultTable .= "<td align='center' class='tabletext'>".($OverallResult==""?"--":$OverallResult)."</td>";
							}
							$ResultTable .= "</tr>";
							$ResultTable .= "</table></td>";
						}
					}
					if($ShowSubjectTeacherComment==1)
					{
						$Comment = $ParSubjectTeacherCommentArray[$SubjectID];
						$ResultTable .= "<td class='tabletext border_left $top_style' align='center'>".($Comment==""?"&nbsp;":$Comment)."</td>";
					}
					$ResultTable .= "</tr>";
					$count++;
				}
			}
		}
	}
	else
	{
		$ResultTable .= "<tr><td align='center' class='small_text report_formfieldtitle' colspan='".$ColumnSpan."'>".$eReportCard['NoRecord']."</td></tr>";
	}
//hdebug_r($SummaryResultArray);
//hdebug_r(getSemesters());
//hdebug(getCurrentSemester());

	# Summary Result Table
	if(!empty($SummaryResultArray))
	{
		for($k=0; $k<sizeof($SummaryResultArray); $k++)
		{	
			
			$ResultTable .= "<tr>";
			//2012-0921-1641-51140
			//$top_border_style = ($k==0) ? "border_top" : "";
			
//			$top_border_style = "border_top";
			$top_border_style = $SummaryResultArray[$k]["Title"] == 'Grand Total'? 'border_top_bold' : 'border_top';
//			$colspan = ($ShowFullMark==1) ? "colspan='2'" : "";
			$colspan = "colspan='2'";
			// line height - aki
			//2012-0921-1641-51140
//			$ResultTable .= "<td class='{$top_border_style}' {$colspan}>
//						<table border='0' cellpadding='1' cellspacing='0' width='100%' height='$LineHeight'><tr><td class='small_title'>".$SummaryResultArray[$k]["Title"]."</td></tr></table>
//						</td>";
			$ResultTable .= "<td class='{$top_border_style}' {$colspan}>
						<table border='0' cellpadding='1' cellspacing='0' width='100%' height='$LineHeight'><tr><td class='tabletext'>".$SummaryResultArray[$k]["Title"]."</td></tr></table>
						</td>";
						
			for($i=0; $i<sizeof($ColumnArray); $i++)
			{
				$thisReportColumnID = $ColumnArray[$i]["ReportColumnID"];
				$ColumnTitle = trim($ColumnArray[$i]["ColumnTitle"]);
				$thisSemesterID = $libreportcard->Get_Semester_By_SemesterName($ColumnTitle);
				$ResultDisplay = $SummaryResultArray[$k][$thisSemesterID];		
				
				# Hide GrandTotal,  Grand Average or Position in Class if there are absent score or fail in any subjects
				if ($SummaryResultArray[$k]["Title"] == 'Grand Total' || $SummaryResultArray[$k]["Title"] == 'Average Mark' || 
					$SummaryResultArray[$k]["Title"] == 'Position in Class')
				{
					//if ($HasAbsent || $ResultDisplay=="")
					# Absent: Hide 3 content
					# Fail: Hide Position in Class only
					if ($HasAbsentArr[$thisReportColumnID] === true || ($HasFailArr[$thisReportColumnID] === true && $SummaryResultArray[$k]["Title"] == 'Position in Class') || 
						$ResultDisplay==""){
						$thisDisplay = '--';
					}
					else
						$thisDisplay = $ResultDisplay;
				}
				else
				{
					if ($ResultDisplay=="")
						$thisDisplay = '--';
					else
						$thisDisplay = $ResultDisplay;
				}
				
				//aki
				$ResultTable .= "<td class='tabletext border_left {$top_border_style}' align='center'>".$thisDisplay."</td>";
			}
			
			### Overall Grand Result
			//if($Semester=='FULL' && $HideOverallResult!=1)
			if(!(($Semester=='FULL' || $Semester==0) && $HideOverallResult==1))
			{
				# Hide GrandTotal,  Grand Average or Position in Class if there are absent or fail in any subjects
				if ($SummaryResultArray[$k]["Title"] == 'Grand Total' || $SummaryResultArray[$k]["Title"] == 'Average Mark' || 
					$SummaryResultArray[$k]["Title"] == 'Position in Class')
				{
					//if ($HasAbsent || $SummaryResultArray[$k][$Semester]=="")
					# Absent: Hide when any term contain "abs"
					# Fail: Hide when any subject fail in overall result
					if (in_array(true, $HasAbsentArr) || ($HasFailArr['Overall'] && $SummaryResultArray[$k]["Title"] == 'Position in Class') || 
						$SummaryResultArray[$k][$Semester]=="")
						$thisDisplay = '--';
					else
						$thisDisplay = $SummaryResultArray[$k][$Semester];
				}
				else
				{
					if ($SummaryResultArray[$k][$Semester]=="")
						$thisDisplay = '--';
					else
						$thisDisplay = $SummaryResultArray[$k][$Semester];
				}
				
				//F81303 - 4 terms to 3 terms
				//$thisDisplay = ($currentReportTermNumber != 4 && $ReportType != "F")? "--" : $thisDisplay;
				$thisDisplay = ($currentReportTermNumber != 3 && $ReportType != "F")? "--" : $thisDisplay;
				$ResultTable .= "<td class='tabletext border_left {$top_border_style}' align='center'>".$thisDisplay."</td>";
			}
			
			if($ShowSubjectTeacherComment==1) {
				$ResultTable .= "<td class='border_left {$top_border_style}' align='center'>&nbsp;</td>";
			}
			$ResultTable .= "</tr>";
		}
	}

	# Conduct Result Table
	if(!empty($ConductResultArray))
	{
		for($k=0; $k<sizeof($ConductResultArray); $k++)
		{
			$ResultTable .= "<tr>";
			//2012-0921-1641-51140
			//$top_border_style = ($k==0) ? "border_top" : "";
			$_title = $ConductResultArray[$k]['Title'];
			$top_border_style = ($_title=='Conduct' || $_title=='Merits' || $_title=='Demerits' || $_title=='Offence') ? "border_top_bold" : "border_top";
			
			if ($_title=='Merits' || $_title=='Demerits' || $_title=='Offence') {
//				$colspan = ($ShowFullMark==1) ? "colspan='".(sizeof($ColumnArray) + 3)."'" : "colspan='".(sizeof($ColumnArray) + 2)."'";
				$colspan = "colspan='".(sizeof($ColumnArray) + 3)."'";
				$stylePrefix = '<b>';
				$styleSuffix = '</b>';
			}
			else {
				$colspan = ($ShowFullMark==1) ? "colspan='2'" : "";
				$colspan = "colspan='2'";
				$stylePrefix = '';
				$styleSuffix = '';
			}
			
			// line height - aki
			//2012-0921-1641-51140
//			$ResultTable .= "<td class='{$top_border_style}' {$colspan}>
//						<table border='0' cellpadding='1' cellspacing='0' width='100%' height='$LineHeight'><tr><td class='small_title'>".$ConductResultArray[$k]["Title"]."</td></tr></table>
//						</td>";
			$ResultTable .= "<td class='{$top_border_style}' {$colspan}>
						<table border='0' cellpadding='1' cellspacing='0' width='100%' height='$LineHeight'><tr><td class='tabletext'>".$stylePrefix.$ConductResultArray[$k]["Title"].$styleSuffix."</td></tr></table>
						</td>";
						
			if ($_title=='Merits' || $_title=='Demerits' || $_title=='Offence') {
				// do nth
			}
			else {
				for($i=0; $i<sizeof($ColumnArray); $i++)
				{
					$ColumnTitle = trim($ColumnArray[$i]["ColumnTitle"]);
					$ResultDisplay = $ConductResultArray[$k][$ColumnTitle];
					//aki
					$ResultTable .= "<td class='tabletext border_left {$top_border_style}' align='center'>".($ResultDisplay==""?"--":$ResultDisplay)."</td>";
				}
				if(!($Semester=='FULL' && $HideOverallResult==1))
					$ResultTable .= "<td class='tabletext border_left {$top_border_style}' align='center'>".($ConductResultArray[$k][$Semester]==""?"--":$ConductResultArray[$k][$Semester])."</td>";
				if($ShowSubjectTeacherComment==1) {
					$ResultTable .= "<td class='border_left {$top_border_style}' align='center'>&nbsp;</td>";
				}
			}
			
			$ResultTable .= "</tr>";
		}
	}

	$ResultTable .= "</table>";

	return $ResultTable;
}

# generate attendance table
function GENERATE_ATTENDANCE_AND_MERIT_TABLE($ParSummaryArray, $ParMeritArray)
{
	global $SummaryTitleArray, $SettingSummaryArray, $SettingMiscArray, $ColumnArray, $eReportCard, $SemesterArray, $LangArray;
	global $DaysAbsentTitle, $TimesLateTitle, $AbsentWOLeaveTitle;
	
	$AttendanceTableShow = ($SettingSummaryArray["DaysAbsent"]==1 || $SettingSummaryArray["TimesLate"] || $SettingSummaryArray["AbsentWOLeave"]);
	$MeritTableShow = ($SettingMiscArray["MeritsAndDemerits"]==1);

	$SemColumnCount = 0;
	for($i=0; $i<sizeof($ColumnArray); $i++)
	{
		$sem_title = trim($ColumnArray[$i]["ColumnTitle"]);
		if($sem_title!="") {
			if(in_array($sem_title, $SemesterArray))
			{
				$SemesterHeaderRow .= ($AttendanceTableShow==1 || $MeritTableShow==1) ? "<td class='reportcard_text border_top border_left' align='center'>".$sem_title."</td>\n" : "";

				$absent_result = $ParSummaryArray[$sem_title][0];
				$DaysAbsentCell .= "<td class='reportcard_text border_top border_left' align='center'>".($absent_result==""?"--":$absent_result)."</td>\n";

				$late_result = $ParSummaryArray[$sem_title][1];
				$LateCell .= "<td class='reportcard_text border_top border_left' align='center'>".($late_result==""?"--":$late_result)."</td>\n";

				$absent_wo_leave_result = $ParSummaryArray[$sem_title][2];
				$AbsentWOLeaveCell .= "<td class='reportcard_text border_top border_left' align='center'>".($absent_wo_leave_result==""?"--":$absent_wo_leave_result)."</td>\n";

				$MeritResult = $ParMeritArray[$sem_title][0];
				$MeritCell .= "<td class='reportcard_text border_top border_left' align='center'>".($MeritResult==""?"--":$MeritResult)."</td>\n";

				$DemeritResult = $ParMeritArray[$sem_title][1];
				$DemeritCell .= "<td class='reportcard_text border_top border_left' align='center'>".($DemeritResult==""?"--":$DemeritResult)."</td>\n";

				$SemColumnCount++;
			}

		}
	}

	if($SemColumnCount>0 && ($AttendanceTableShow==1 || $MeritTableShow==1))
	{
		$ReturnTable = "<table width='100%' valign='bottom' border='0' cellspacing='0' cellpadding='1'>\n";
		if($AttendanceTableShow==1)
		{
			$ReturnTable .= "<tr><td class='reportcard_text border_top'>".$LangArray['Attendance']."</td>\n";
			$ReturnTable .= $SemesterHeaderRow;
		}

		# Rows of Attendance records
		if($SettingSummaryArray["DaysAbsent"]==1)
		{
			$ReturnTable .= "<tr><td class='reportcard_text border_top'>".$DaysAbsentTitle."</td>\n";
			$ReturnTable .= $DaysAbsentCell;
			$ReturnTable .= "</tr>";
		}
		if($SettingSummaryArray["TimesLate"]==1)
		{
			$ReturnTable .= "<tr><td class='reportcard_text border_top'>".$TimesLateTitle."</td>\n";
			$ReturnTable .= $LateCell;
			$ReturnTable .= "</tr>";
		}
		if($SettingSummaryArray["AbsentWOLeave"]==1)
		{
			$ReturnTable .= "<tr><td class='reportcard_text border_top'>".$AbsentWOLeaveTitle."</td>";
			$ReturnTable .= $AbsentWOLeaveCell;
			$ReturnTable .= "</tr>";
		}

		# Row of Merit records
		if($MeritTableShow==1)
		{
			if($AttendanceTableShow==1) {
				$ReturnTable .= "<tr><td class='reportcard_text border_top' colspan='".($SemColumnCount+1)."'>".$LangArray['MeritsAndDemerits']."</td></tr>\n";
			}
			else {
				$ReturnTable .= "<tr><td class='reportcard_text border_top'>".$LangArray['MeritsAndDemerits']."</td>\n";
				$ReturnTable .= $SemesterHeaderRow;
			}

			$ReturnTable .= "<tr><td class='reportcard_text border_top'>".$LangArray['Merit']."</td>\n";
			$ReturnTable .= $MeritCell;
			$ReturnTable .= "</tr>";
			$ReturnTable .= "<tr><td class='reportcard_text border_top'>".$LangArray['Demerit']."</td>\n";
			$ReturnTable .= $DemeritCell;
			$ReturnTable .= "</tr>";
		}
		
		$ReturnTable .= "</table>";
	}
	return $ReturnTable;
}

# Santa Rosa - return Hard Code Subject Unit Array
function Get_CLASS_LEVEL_SUBJECT_UNIT($ParClassLevelReg, $ClassID){
	global $PATH_WRT_ROOT;
	include_once($PATH_WRT_ROOT."includes/libclass.php");
	
	$unitCount = 0;
	$unitAry = array();
	$levelReg = trim($ParClassLevelReg);
	
	# Check Arts / Science
	if($ParClassLevelReg == "S4" || $ParClassLevelReg == "S5" || $ParClassLevelReg == "S6"){
		$liclass = new libclass();
		$ClassName = $liclass->getClassName($ClassID);
		if(strpos($ClassName, 'Science') !== false){
			$Elective = "Sci";
		} else {
			$Elective = "Arts";
		}
	}
	
	# Primary
	# P1
	if($levelReg == 'P1'){
		$unitAry['borderPassUnit'] = 2;
		$unitAry['failUnit'] = 3;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('117');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 4;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('118');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 2;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('130');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 2;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('115');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('165');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('140');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('111');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('137');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('135');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('132');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('119');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('114');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('151');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		
		// [2017-1013-1811-15235] added Drama
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('148');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('164');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 0;	
	} 
	# P2
	else if($levelReg == 'P2'){
		$unitAry['borderPassUnit'] = 2;
		$unitAry['failUnit'] = 3;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('117');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 4;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('118');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 2;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('130');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 2;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('115');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('165');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('140');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('111');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('137');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('135');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('132');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('119');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('114');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('151');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		
		// [2017-1013-1811-15235] added Drama
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('148');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('164');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 0;
	} 
	# P3
	else if($levelReg == 'P3'){
		$unitAry['borderPassUnit'] = 2;
		$unitAry['failUnit'] = 3;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('117');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 4;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('118');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 2;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('130');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 2;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('115');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('165');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('140');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('132');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('119');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('135');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('137');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('111');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('114');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('151');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('164');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 0;	
	}	
	# P4
	else if($levelReg == 'P4'){
		$unitAry['borderPassUnit'] = 3;
		$unitAry['failUnit'] = 4;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('117');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 4;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('165');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('115');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('118');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 2;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('130');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 2;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('140');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('132');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('119');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('135');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('137');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('111');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('151');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('164');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 0;
	}
	# P5
	else if($levelReg == 'P5'){
		$unitAry['borderPassUnit'] = 3;
		$unitAry['failUnit'] = 4;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('117');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 4;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('116');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('115');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('118');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 2;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('122');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('130');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 2;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('140');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('132');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('119');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('135');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('137');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('111');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('151');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('164');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 0;	
	} 
	# P6
	else if($levelReg == 'P6'){
		$unitAry['borderPassUnit'] = 3;
		$unitAry['failUnit'] = 4;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('117');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 4;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('116');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('115');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('118');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 2;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('121');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('122');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('130');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 2;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('127');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;		
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('132');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('119');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('135');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('137');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('111');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('151');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('164');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 0;		
	} 
	# Secondary
	# F1
	else if($levelReg == 'S1'){
		$unitAry['borderPassUnit'] = 3;
		$unitAry['failUnit'] = 4;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('117');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 4;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('116');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('115');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('118', '157');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 2;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('121');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('122');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;	
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('130');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 2;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('127');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('132');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('119');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('111');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('135');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('137');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('151');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('164');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 0;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('176');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 0;
		
	}
	# F2
	else if($levelReg == 'S2'){
		$unitAry['borderPassUnit'] = 3;
		$unitAry['failUnit'] = 4;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('117');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 4;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('116');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('115');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('118', '157');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 2;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('124');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;	
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('121');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('122');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('130');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 2;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('127');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;	
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('132');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('119');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('111');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('135');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('137');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('151');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('164');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 0;
	}
	# F3
	else if($levelReg == 'S3'){
		$unitAry['borderPassUnit'] = 3;
		$unitAry['failUnit'] = 4;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('117');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 4;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('116');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('115');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('118', '157');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 2;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('124');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;		
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('163', '126');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('121');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('122');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('130');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 2;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('132');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('119');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('111');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('135');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('137');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('151');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
	}
	# F4
	else if($levelReg == 'S4'){
		$unitAry['borderPassUnit'] = 3;
		$unitAry['failUnit'] = 4;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('117');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 4;
		$unitCount++;
		if ($Elective == "Arts"){
			$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('166');
			//$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1.5;
			$unitAry['subjectUnitAry'][$unitCount]['unit'] = 2;
			$unitCount++;
		}
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('116');
		//$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1.5;
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 2;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('115');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		if ($Elective == "Arts"){
			$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('160');
			//$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1.5;
			$unitAry['subjectUnitAry'][$unitCount]['unit'] = 2;
			$unitCount++;
		}
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('118', '157');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 2;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('124');
		//$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1.5;
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 2;
		$unitCount++;
		if ($Elective == "Sci"){	
			$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('160');
			//$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1.5;
			$unitAry['subjectUnitAry'][$unitCount]['unit'] = 2;
			$unitCount++;
		}
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('122');
		//$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1.5;
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 2;
		$unitCount++;
		if ($Elective == "Arts"){
			$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('159');
			$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1.5;
			$unitCount++;
		}
		if ($Elective == "Sci"){
			$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('129');
			//$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1.5;
			$unitAry['subjectUnitAry'][$unitCount]['unit'] = 2;
			$unitCount++;
			$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('126');
			//$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1.5;
			$unitAry['subjectUnitAry'][$unitCount]['unit'] = 2;
			$unitCount++;
			$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('128');
			//$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1.5;
			$unitAry['subjectUnitAry'][$unitCount]['unit'] = 2;
			$unitCount++;
		}
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('130');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 2;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('132');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('158');
		//$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1.5;
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 2;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('162');
		//$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1.5;
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 2;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('111');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('135');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		
		// F81303 - 為兩個新增學科加上相關的 "Units"
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('171');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('172');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
	}
	# F5
	else if($levelReg == 'S5'){
		$unitAry['borderPassUnit'] = 3;
		$unitAry['failUnit'] = 4;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('117');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 4;
		$unitCount++;
		if ($Elective == "Arts"){
			$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('166');
			//$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1.5;
			$unitAry['subjectUnitAry'][$unitCount]['unit'] = 2;
			$unitCount++;
			$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('116');
			//$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1.5;
			$unitAry['subjectUnitAry'][$unitCount]['unit'] = 2;
			$unitCount++;
			$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('161');
			$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1.5;
			$unitCount++;
		}
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('115');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		if ($Elective == "Sci"){	
			$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('126');
			//$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1.5;
			$unitAry['subjectUnitAry'][$unitCount]['unit'] = 2;
			$unitCount++;
			$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('128');
			//$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1.5;
			$unitAry['subjectUnitAry'][$unitCount]['unit'] = 2;
			$unitCount++;
			$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('129');
			//$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1.5;
			$unitAry['subjectUnitAry'][$unitCount]['unit'] = 2;
			$unitCount++;
			$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('160');
			//$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1.5;
			$unitAry['subjectUnitAry'][$unitCount]['unit'] = 2;
			$unitCount++;
		}
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('124');
		//$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1.5;
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 2;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('122');
		//$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1.5;
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 2;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('159');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		if ($Elective == "Arts"){
			$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('160');
			//$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1.5;
			$unitAry['subjectUnitAry'][$unitCount]['unit'] = 2;
			$unitCount++;
		}
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('130');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 2;
		$unitCount++;
		
		// [2016-1214-1048-47235] removed Chinese History, cancelled Chinese and Chinese History grouping
//		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('118', '157');
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('118');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 2;
		$unitCount++;
		
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('132');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('158');
		//$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1.5;
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 2;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('162');
		//$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1.5;
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 2;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('111');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('135');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		
		// [2016-1214-1048-47235] added Music and Integrated Science
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('137');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('172');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
	}
	# F6
	else if($levelReg == 'S6'){
		$unitAry['borderPassUnit'] = 3;
		$unitAry['failUnit'] = 4;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('117');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 4;
		$unitCount++;
		if ($Elective == "Arts"){
			$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('116');
			$unitAry['subjectUnitAry'][$unitCount]['unit'] = 2; // 2019-12-30 (Philips) [2019-1212-1809-07235] - 1.5 to 2
			$unitCount++;
			
			// [2016-1214-1048-47235] removed Business English, replaced by Business Studies
			//$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('161');
			$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('166');
			$unitAry['subjectUnitAry'][$unitCount]['unit'] = 2; // 2019-12-30 (Philips) [2019-1212-1809-07235] - 1.5 to 2
			$unitCount++;
		}
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('115');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('118');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 2;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('124');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 2; // 2019-12-30 (Philips) [2019-1212-1809-07235] - 1.5 to 2
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('122');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1.5;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('159');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1.5;
		$unitCount++;
		
		// [2017-0103-1001-49207] added Accounting for F.6 Science
		//if ($Elective == "Arts"){
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('160');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 2; // 2019-12-30 (Philips) [2019-1212-1809-07235] - 1.5 to 2
		$unitCount++;
		//}
		
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('130');
		//$unitAry['subjectUnitAry'][$unitCount]['unit'] = 2;
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1.5;
		$unitCount++;
		
		// [2017-1013-1811-15235] added Additional Mathematics
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('175');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 0.5;
		$unitCount++;
		
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('132');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('158');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1.5;
		$unitCount++;
		if ($Elective == "Sci"){	
			$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('129');
			$unitAry['subjectUnitAry'][$unitCount]['unit'] = 2; // 2019-12-30 (Philips) [2019-1212-1809-07235] - 1.5 to 2
			$unitCount++;
			$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('128');
			$unitAry['subjectUnitAry'][$unitCount]['unit'] = 2; // 2019-12-30 (Philips) [2019-1212-1809-07235] - 1.5 to 2
			$unitCount++;
			$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('126');
			$unitAry['subjectUnitAry'][$unitCount]['unit'] = 2; // 2019-12-30 (Philips) [2019-1212-1809-07235] - 1.5 to 2
			$unitCount++;
		}
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('162');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1.5;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('111');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('135');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('177');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1; // 2019-12-30 (Philips) [2019-1212-1809-07235] - Add Geo Literacy
// 		$unitCount++;
//		if ($Elective = "Arts"){
//			$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('127');
//			$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1.5;
//			$unitCount++;
//		}
	}
	# S7 (146 - Testing)
	else if($levelReg == '146-S7'){
		$unitAry['borderPassUnit'] = 3;
		$unitAry['failUnit'] = 4;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('165');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 4;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('80', '203');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 2;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('201');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('280');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 2;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('606');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('coc');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1.5;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('104');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('901');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('401');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 0;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('101', '102');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
	}
	# dummy 2 (149 - Testing)
	else if($levelReg == '149-S1'){
		$unitAry['borderPassUnit'] = 3;
		$unitAry['failUnit'] = 4;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('P008');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 4;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('S02', '052');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 2;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('21B');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 2;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('099');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('0009');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('053');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('054');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('061', '063');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1.5;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('333');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('036');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('082');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 0;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('009');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 1;
		$unitCount++;
		$unitAry['subjectUnitAry'][$unitCount]['relatedSubjectCodeAry'] = array('124');
		$unitAry['subjectUnitAry'][$unitCount]['unit'] = 0;
		$unitCount++;
	}
	
	
	$returnAry = $unitAry;
	unset($unitAry);
	return $returnAry;
}

# Return remarks of each term
function GET_REPORT_REMARKS($currentSem){
	
	global $StudentID, $SettingArray, $libreportcard;
	global $ColumnArray, $ReportCellSetting, $FailedArray, $DistinctionArray, $PFArray;
	global $SubjectArray, $ReportMarksheetArray, $SubjectUnitArr, $SubjectCodeMappingArr, $allSemester;	
	$Display = "--";
	$MarkTypeDisplay = $SettingArray["MarkTypeDisplay"];

	$failUnit = 0;
	# Handle Overall result column
	$overall = ($currentSem === '0')? 1 : 0;
	
	# Marksheet Result
	$currentResult = $ReportMarksheetArray[$StudentID];
	# Subject Unit Array
	$currentSubjectArr = $SubjectUnitArr['subjectUnitAry'];
	# Get $ReportColumnID
	$targetColumnArr = BuildMultiKeyAssoc($ColumnArray, 'ColumnTitle');
	list($ReportColumnID, $ColumnTitle, $Weight, $ShowPosition, $PositionRange) = !$overall? $targetColumnArr[$allSemester[$currentSem]] : end($targetColumnArr);
	$targetColumnID = $ReportColumnID;
	# Handle Overall result column
	$ReportColumnID = !$overall? $ReportColumnID : 'Overall';
	$MarkName = !$overall? "RawMark" : "Mark";

	if(count($currentSubjectArr) > 0){
		# All Subjects of a form
		foreach ($currentSubjectArr as $currentSubject){
			
//			$failComponent = 0;
//			$invalidComponent = 0;
			$componentTotalMark = 0;
			$validComponent = 0;
			$componentSubjuctNum = count($currentSubject['relatedSubjectCodeAry']);
			
			# Single and Component Subject
			for($i = 0; $i < $componentSubjuctNum; $i++){
				
				$IsFailed = false;
				$currentSubjectRegCode = $currentSubject['relatedSubjectCodeAry'][$i];
				
				$s_grade = "";
				$s_raw_mark = "";
				$DisplayResult = "";
				
				# Get $CellSetting
				list($ReportSubjectID, $SubjectID, $EngSubjectName, $ChiSubjectName, $FullMark, $Scale, $HidePosition) = $SubjectArray[$currentSubjectRegCode][0];
				$CellSetting = $ReportCellSetting[$targetColumnID][$ReportSubjectID];
				
				# Ensure CellSetting is valid before checking fail
				if($CellSetting!="N/A" && $CellSetting!=null){
					if($CellSetting=="S")
					{
						$s_raw_mark = $currentResult[$SubjectID][$ReportColumnID][$MarkName];
						$s_grade = $currentResult[$SubjectID][$ReportColumnID]["Grade"];
						
						//2014-0915-1841-09164: (Internal) Follow-up by sienasze on 2014-11-24 10:12
						//if($s_raw_mark>=0)			
						if($s_raw_mark>=0 && $s_raw_mark!='')
						{
							# "Failed" if Chinese (not F1 to F5) or Maths < 30 marks
							# 146 - Testing
//							if((($currentSubject['relatedSubjectCodeAry'][$i] == '80' && $componentSubjuctNum === 1) || $currentSubject['relatedSubjectCodeAry'][$i] == '280') 
							# 149 - Testing
//							if((($currentSubject['relatedSubjectCodeAry'][$i] == 'S02' && $componentSubjuctNum === 1) || $currentSubject['relatedSubjectCodeAry'][$i] == '21B') 
							# Client Site
							if((($currentSubject['relatedSubjectCodeAry'][$i] == '118' && $componentSubjuctNum === 1) || $currentSubject['relatedSubjectCodeAry'][$i] == '130')
								&& $s_raw_mark < 30){
								$failUnit += $SubjectUnitArr['failUnit'];
								break;
							}
							
							if($MarkTypeDisplay==2)
							{	
								$DisplayResult = $currentResult[$SubjectID][$ReportColumnID]["Grade"];
								list($StylePrefix, $StyleSuffix, $IsDistinct, $IsFailed) = $libreportcard->GET_STYLE("G", $DisplayResult, $FailedArray[$SubjectID], $DistinctionArray[$SubjectID], $PFArray[$SubjectID]);
							} else
							{
								list($StylePrefix, $StyleSuffix, $IsDistinct, $IsFailed) = $libreportcard->GET_STYLE($CellSetting, $s_raw_mark, $FailedArray[$SubjectID], $DistinctionArray[$SubjectID], $PFArray[$SubjectID]);
								
							}
						}
					}
					else
					{
						$DisplayResult = trim($currentResult[$SubjectID][$ReportColumnID]["Grade"]);
						if($DisplayResult!="/" && $DisplayResult!="abs")
						{
							list($StylePrefix, $StyleSuffix, $IsDistinct, $IsFailed) = $libreportcard->GET_STYLE($CellSetting, $DisplayResult, $FailedArray[$SubjectID], $DistinctionArray[$SubjectID], $PFArray[$SubjectID]);
						}
					}
					# Fail - Single Subject (Add subject unit)
					if ($IsFailed && $componentSubjuctNum == 1) {
						$failUnit += $currentSubject['unit'];
					}
					# Component Subject
					else if($componentSubjuctNum > 1){
//						# Fail (Add fail count)
//						if ($IsFailed){
//							$failComponent++;
//						}
//						# "/" or "abs" (neglect and add fail count)
//						else if($s_grade == "/" || $s_grade == "abs" || $DisplayResult == "/" || $DisplayResult == "abs"){
//							$failComponent++;
//							$invalidComponent++;
//						}
						# Add marks for calculating average
						if($s_grade != "/" && $s_grade != "abs"){
							$componentTotalMark += $s_raw_mark;
							$validComponent++;
						}
					}
				}
				
				# All component subjects - Fail
//				if($failComponent == $componentSubjuctNum && $failComponent != $invalidComponent){
//					$failUnit += $currentSubject['unit'];
//				}
				# Fail if average of all components < 50 - Enter checking when last component
				if($componentSubjuctNum > 1 && $i == ($componentSubjuctNum - 1) && $validComponent > 0){
					$average = $componentTotalMark / $validComponent;
					if($average < 50){
						$failUnit += $currentSubject['unit'];
					}
					# "Failed" if combined Chinese (F1 to F5) < 30 marks
					# 146 - Testing
//					if($currentSubject['relatedSubjectCodeAry'][0] == '80' && $average < 30){
					# 149 - Testing
//					if($currentSubject['relatedSubjectCodeAry'][0] == 'S02' && $average < 30){
					# Client Site
					if($currentSubject['relatedSubjectCodeAry'][0] == '118' && $average < 30){
						$failUnit += $SubjectUnitArr['failUnit'];
						break;
					}
				}
			}	// end loop combined subject
			
			# "Failed"
			if($failUnit >= $SubjectUnitArr['failUnit']){
				break;
			}
		}	// end loop subject

		# Return remarks
		if($failUnit >= $SubjectUnitArr['failUnit']){
			$Display =  'Fail';
		} else if ($failUnit < $SubjectUnitArr['borderPassUnit']){
			$Display = 'Pass';
		} else {
			$Display = 'Borderline Pass';
		}
	}
	return $Display;
}

# Get report Time Late and Tardiness
function GET_REPORT_ATTENDANCE_AND_TARDINESS($ParStudentSummaryArray){
	global $ColumnArray;
	$summaryDataArr = array();
	$summaryDataArr[0]['Attendance'] = 0;
	$summaryDataArr[0]['TimesLate'] = 0;
	$summaryDataArr[0]['Tardiness'] = 0;
	$summaryDataArr[0]['TardinessOffence'] = 0;
	
	# For each term
	for($i=0; $i<sizeof($ColumnArray); $i++)
	{
		$column = trim($ColumnArray[$i]["ColumnTitle"]);
		$targetSummary = $ParStudentSummaryArray[$column];
				
		if(!isset($targetSummary)){
			continue;
		}
		
		# Determine - Gain Attendance or not (DaysAbsent, TimesLate, AbsentWOReason => All Empty?)
		// F81303 - Attendance 數值改為每學段顯示一次
//		if($i > 0){
			$prevColumn = trim($ColumnArray[$i-1]["ColumnTitle"]);
			$prevSummary = $ParStudentSummaryArray[$prevColumn];
			// F81303 - Attendance 數值改為每學段顯示一次
//	 		$attendanceGain = ($i % 2) && ($targetSummary[3]=="" && $targetSummary[4]=="" && $targetSummary[5]=="" && $prevSummary[3]=="" && 
//	 							$prevSummary[4]=="" && $prevSummary[5]=="");
	 		$attendanceGain = ($targetSummary[3]=="" && $targetSummary[4]=="" && $targetSummary[5]=="");
//		}
		$summaryDataArr[$column]['Attendance'] = $attendanceGain? 1 : 0;
		$summaryDataArr[0]['Attendance'] += $attendanceGain? 1 : 0;
		
		# Time Late
		$currentTimeLate = ($targetSummary[4]=="") ? 0 : $targetSummary[4];
		$summaryDataArr[$column]['TimesLate'] = $currentTimeLate;
		$summaryDataArr[0]['TimesLate'] += $currentTimeLate;
		
		$totalTimeLate = $summaryDataArr[0]['TimesLate'];
		
		# Offence - Tardiness
		$currentOffence = intval($totalTimeLate / 9) - $summaryDataArr[0]['TardinessOffence'];
		$summaryDataArr[$column]['TardinessOffence'] = $currentOffence;
		$summaryDataArr[0]['TardinessOffence'] += $currentOffence;
		
		$totalOffence = $summaryDataArr[0]['TardinessOffence'];
		
		# Demerit - Tardiness
		$currentDemerit = intval($totalTimeLate / 3) - $summaryDataArr[0]['Tardiness'] - $totalOffence;
		$summaryDataArr[$column]['Tardiness'] = $currentDemerit;
		$summaryDataArr[0]['Tardiness'] += $currentDemerit;
	
	}
	$summaryDataArr[0]['Attendance'] = 0;
	$summaryDataArr[0]['TimesLate'] = 0;
	$summaryDataArr[0]['Tardiness'] = 0;
	$summaryDataArr[0]['TardinessOffence'] = 0;
	
	return $summaryDataArr;
	
}

# Map Subject Code and Subject ID
function GET_SUBJECT_SUBJECTCODE_MAPPING(){
	
	global $PATH_WRT_ROOT;
	include_once($PATH_WRT_ROOT."includes/libdb.php");
	
	$li = new libdb();
	$sql = "SELECT 
				CODEID,
				RecordID
			FROM
				ASSESSMENT_SUBJECT 
			WHERE
				EN_DES IS NOT NULL
				AND RecordStatus = 1
				AND (CMP_CODEID IS NULL || CMP_CODEID = '')
			ORDER BY
				DisplayOrder
			";
	$SubjectArr = $li->returnArray($sql, 2);
	
	$ReturnArr = array();
	if (sizeof($SubjectArr) > 0) {
		for($i=0; $i<sizeof($SubjectArr); $i++) {
			$ReturnArr[$SubjectArr[$i]["RecordID"]] = $SubjectArr[$i]["CODEID"];
		}
	}
	return $ReturnArr;
}

function GET_SUBJECT_UNIT($subjectCode, $SubjectUnitArr) {
	$subjectUnit = 0;
	$relatedSubjectCodeAry = array();
	$numOfUnit = count($SubjectUnitArr['subjectUnitAry']);
	
	for ($i=0; $i<$numOfUnit; $i++) {
		$_relatedSubjectCodeAry = $SubjectUnitArr['subjectUnitAry'][$i]['relatedSubjectCodeAry'];
		$_unit = $SubjectUnitArr['subjectUnitAry'][$i]['unit'];
		
		if (in_array($subjectCode, (array)$_relatedSubjectCodeAry)) {
			$subjectUnit = $_unit;
			$relatedSubjectCodeAry = $_relatedSubjectCodeAry;
			break;
		}
	}
	
	return array($subjectUnit, $relatedSubjectCodeAry);
}


################################ Function End #####################################
?>