<?php
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
intranet_auth();
intranet_opendb();

$li = new libdb();

$list = (is_array($SchemeID) && sizeof($SchemeID)!=0) ? implode(",",$SchemeID) : $SchemeID;
if ($list != "")
{
	# Step 1: Remove record(s) from RC_GRADING_SCHEME
	$sql = "DELETE FROM RC_GRADING_SCHEME WHERE SchemeID IN ($list)";
	$success = $li->db_db_query($sql);
	
	if($success==1)
	{
		# Step 2: Remove record(s) from RC_GRADING_SCHEME_GRADEMARK
		$sql = "DELETE FROM RC_GRADING_SCHEME_GRADEMARK WHERE SchemeID IN ($list)";
		$li->db_db_query($sql);

		# Step 3: Remove record(s) in RC_SUBJECT_GRADING_SCHEME and RC_SUBJECT_GRADING_SCHEME_FORM
		// Retrieve target SubjectGradingID
		$sql = "SELECT SubjectGradingID FROM RC_SUBJECT_GRADING_SCHEME WHERE SchemeID IN ($list)";
		$SubjectGradingArr = $li->returnVector($sql);
		if(!empty($SubjectGradingArr))
		{
			// Remove record(s) in RC_SUBJECT_GRADING_SCHEME_FORM
			$SubjectGradingList = implode(",", $SubjectGradingArr);
			$sql = "DELETE FROM RC_SUBJECT_GRADING_SCHEME_FORM WHERE SubjectGradingID IN ($SubjectGradingList)";
			$li->db_db_query($sql);
		}
		// Remove record(s) in RC_SUBJECT_GRADING_SCHEME
		$sql = "DELETE FROM RC_SUBJECT_GRADING_SCHEME WHERE SchemeID IN ($list)";
		$li->db_db_query($sql);
	}
}
intranet_closedb();

$Result = ($success==1) ? "delete" : "delete_failed";
header("Location: index.php?Result=$Result");
?>