<?php
$PATH_WRT_ROOT = "../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

if ($plugin['ReportCard'])
{
	include_once($PATH_WRT_ROOT."includes/libreportcard.php");
	$lreportcard = new libreportcard();
	$CurrentPage = "SchemeSettings_GradingSchemes";
	$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
    if ($lreportcard->hasAccessRight())
    {
        $linterface = new interface_html();

		# tag information
		$TAGS_OBJ[] = array($eReportCard['SchemeSettings_GradingSchemes'], "", 0);

		# page navigation (leave the array empty if no need)
		$PAGE_NAVIGATION[] = array($button_new, "");

		$linterface->LAYOUT_START();

?>
<script language="javascript">
$(document).ready(function () {
	//$('tr#PassRow').hide();
	//$('tr#FailRow').hide();
});

function checkform(obj){
	if(!check_text(obj.SchemeTitle, "<?php echo $i_alert_pleasefillin.$i_general_title; ?>.")) return false;
	if(obj.grading_honor.checked)
	{	
		if(!check_text(obj.FullMark, "<?php echo $i_alert_pleasefillin.$eReportCard['FullMark']; ?>.")) return false;
	}
	else
	{
		if(!check_text(obj.Pass, "<?php echo $i_alert_pleasefillin.$eReportCard['Pass']; ?>.")) return false;
		if(!check_text(obj.Fail, "<?php echo $i_alert_pleasefillin.$eReportCard['Fail']; ?>.")) return false;
	}
	if(!check_text(obj.Weighting, "<?php echo $i_alert_pleasefillin.$eReportCard['WeightingGrandTotal']; ?>.")) return false;
   
     return true;
}

// reference
//http://www.mredkj.com/tutorials/tablebasics3.html 

function jADD_ROW(jTableID, jPrefix)
{
  var jTableBody = document.getElementById(jTableID).tBodies[0];
  var jNewRow = jTableBody.insertRow(-1);
  var jNewCell0 = jNewRow.insertCell(0);
  jNewCell0.innerHTML = "<input class='textboxnum' maxlength='10' type='text' name='"+jPrefix+"LowerLimit[]' id='PassLowerLimit[]' />";
  var jNewCell1 = jNewRow.insertCell(1);
  jNewCell1.innerHTML = "<input class='textboxnum' maxlength='10' type='text' name='"+jPrefix+"Grade[]' id='PassGrade[]' />";
  var jNewCell2 = jNewRow.insertCell(2);
  jNewCell2.innerHTML = "<input class='textboxnum' maxlength='10' type='text' name='"+jPrefix+"GradePoint[]' id='PassGradePoint[]' /></td>";
}

function jCHANGE_TABLE(type)
{
	if(type=="PF")
	{
		displayTable('FullMarkRow', 'none');
		displayTable('HonorPassRow', 'none');
		displayTable('HonorFailRow', 'none');
		displayTable('HonorDistRow', 'none');

		displayTable('PassRow', '');
		displayTable('FailRow', '');
		//displayTable('DistinctRow', '');		
	}
	else 
	{
		displayTable('FullMarkRow', '');
		displayTable('HonorPassRow', '');
		displayTable('HonorFailRow', '');
		displayTable('HonorDistRow', '');

		displayTable('PassRow', 'none');
		displayTable('FailRow', 'none');
		//displayTable('DistinctRow', 'none');
	}
}
</script>


<form name="form1" action="new_update.php" method="post" onSubmit="return checkform(this);">
<br />
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
</tr>
<tr>
	<td>
		<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
			<tr><td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $i_general_title; ?> <span class="tabletextrequire">*</span></td><td width="75%"><input class="textboxtext" type="text" name="SchemeTitle" maxlength="255" /></td></tr>
			<tr>
				<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $eReportCard['GradingType']; ?> <span class="tabletextrequire">*</span></td>
				<td width="75%" valign="top">
					<table width='50%'>
						<tr>
							<td class="tabletext"><input type="radio" name="SchemeType" value="H" id="grading_honor" onClick="if(this.checked){jCHANGE_TABLE('H')}" checked="checked" /><label for="grading_honor"><?=$eReportCard['HonorBased']?></label></td>
							<td class="tabletext"><input type="radio" name="SchemeType" value="PF" id="grading_passfail" onClick="if(this.checked){jCHANGE_TABLE('PF')}" /><label for="grading_passfail"><?=$eReportCard['PassFailBased']?></label></td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $i_general_description; ?></td>
				<td width="75%"><?= $linterface->GET_TEXTAREA("Description", "", 40) ?></td>
			</tr>
			<tr id="PassRow" style="display:none">
				<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
					<?= $eReportCard['Pass']; ?> <span class="tabletextrequire">*</span>
				</td>
				<td width="75%">
					<input class="textboxnum" type="text" name="Pass" maxlength="10" value='P' />
				</td>
			</tr>
			<tr id="FailRow" style="display:none">
				<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $eReportCard['Fail']; ?> <span class="tabletextrequire">*</span></td>
				<td width="75%"><input class="textboxnum" type="text" name="Fail" maxlength="10" value='F' /></td>
			</tr>
			<tr id="FullMarkRow">
				<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $eReportCard['FullMark']; ?> <span class="tabletextrequire">*</span></td>
				<td width="75%"><input class="textboxnum" type="text" name="FullMark" value="100" maxlength="10" /></td>
			</tr>
			<tr id="HonorPassRow">
				<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $eReportCard['Pass']; ?></td><td width="75%" valign="top">
				<table id="tblPass">
				<tr>
					<td class="tabletext"><?=$eReportCard['LowerLimit']?></td>
					<td class="tabletext"><?=$eReportCard['Grade']?></td>
					<td class="tabletext"><?=$eReportCard['GradePoint']?></td>
				</tr>
				<?php echo $lreportcard->GENERATE_GRADING_FORM_CELL(1, "Pass"); ?>
				</table>
				<span class="tabletext"><a href="javascript:jADD_ROW('tblPass', 'Pass');" class="tablelink">[<?=$eReportCard['AddMore']?>]</a></span>
			</td></tr>
			<tr id="HonorFailRow"><td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $eReportCard['Fail']; ?></td><td width="75%" valign="top">
				<table id="tblFail">
				<tr>
					<td class="tabletext"><label for="FailLowerLimit"><?=$eReportCard['LowerLimit']?></label></td>
					<td class="tabletext"><label for="FailGrade"><?=$eReportCard['Grade']?></label></td>
					<td class="tabletext"><label for="FailGradePoint"><?=$eReportCard['GradePoint']?></label></td>
				</tr>
				<?php echo $lreportcard->GENERATE_GRADING_FORM_CELL(1, "Fail"); ?>
				</table>
				<span class="tabletext"><a href="javascript:jADD_ROW('tblFail', 'Fail');" class="tablelink">[<?=$eReportCard['AddMore']?>]</a></span>
			</td></tr>
			<tr id="HonorDistRow"><td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $eReportCard['Distinction']; ?></td><td width="75%" valign="top">
				<table>
				<tr>
					<td class="tabletext"><label for="DistLowerLimit"><?=$eReportCard['LowerLimit']?></label></td>
					<td class="tabletext"><label for="DistGrade"><?=$eReportCard['Grade']?></label></td>
					<td class="tabletext"><label for="DistGradePoint"><?=$eReportCard['GradePoint']?></label></td>
				</tr>
				<tr>
					<td class="tabletext"><input class="textboxnum" maxlength="10" type="text" name="DistLowerLimit" id="DistLowerLimit" /></td>
					<td class="tabletext"><input class="textboxnum" maxlength="10" type="text" name="DistGrade" id="DistGrade" /></td>
					<td class="tabletext"><input class="textboxnum" maxlength="10" type="text" name="DistGradePoint" id="DistGradePoint" /></td>
				</tr>
				</table>
			</td></tr>
			<tr><td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $eReportCard['WeightingGrandTotal']; ?> <span class="tabletextrequire">*</span></td><td width="75%"><input class="textboxnum" type="text" name="Weighting" value="1" maxlength="10" /></td></tr>
		</table>
	</td>
</tr>
<tr>
	<td><table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tr><td align="left" class="tabletextremark">&nbsp; &nbsp; &nbsp; <?=$i_general_required_field?></td></tr>
		<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
		<tr>
			<td align="center"><?= $linterface->GET_ACTION_BTN($button_submit, "submit", "", "btnSubmit")?>&nbsp;
			<?= $linterface->GET_ACTION_BTN($button_reset, "reset")?>&nbsp;
			<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:history.back()")?>
			</td>
		</tr>
	</table>
	</td>
</tr>
</table>


<p></p>
</form>
<?= $linterface->FOCUS_ON_LOAD("form1.SchemeTitle") ?>

<?
        $linterface->LAYOUT_STOP();
		intranet_closedb();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>
