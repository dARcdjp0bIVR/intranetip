<?php
$PATH_WRT_ROOT = "../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
intranet_auth();
intranet_opendb();

$li = new libdb();

$SchemeTitle = intranet_htmlspecialchars($_POST['SchemeTitle']);
$Description = intranet_htmlspecialchars($_POST['Description']);

$fields = "SchemeTitle, Description, SchemeType";
$fields .= ($SchemeType=="H") ? ", FullMark" : ", Pass, Fail";
$fields .= ", Weighting, DateInput, DateModified";
$values = "'$SchemeTitle', '$Description', '$SchemeType'";
$values .= ($SchemeType=="H") ? ", '$FullMark'" : ", '$Pass', '$Fail'";
$values .= ", '$Weighting' , now() , now()";

$sql = "INSERT INTO RC_GRADING_SCHEME ($fields) VALUES ($values)";
$success = $li->db_db_query($sql);
$SchemeID = $li->db_insert_id();

if(!empty($PassLowerLimit))
{
	for($i=0; $i<sizeof($PassLowerLimit); $i++)
	{
		$p_lower_limit = $PassLowerLimit[$i];
		if($p_lower_limit!="")
		{
			$p_grade = $PassGrade[$i];
			$p_grade_point = $PassGradePoint[$i];
			$sql = "INSERT INTO RC_GRADING_SCHEME_GRADEMARK (SchemeID, Nature, LowerLimit, Grade, GradePoint, DateInput, DateModified)
				   VALUES ('$SchemeID', 'P', '$p_lower_limit', '$p_grade', '$p_grade_point' , now() , now())";
			$li->db_db_query($sql);
		}
	}
}
if(!empty($FailLowerLimit))
{
	for($i=0; $i<sizeof($FailLowerLimit); $i++)
	{
		$f_lower_limit = $FailLowerLimit[$i];
		if($f_lower_limit!="")
		{
			$f_grade = $FailGrade[$i];
			$f_grade_point = $FailGradePoint[$i];
			$sql = "INSERT INTO RC_GRADING_SCHEME_GRADEMARK (SchemeID, Nature, LowerLimit, Grade, GradePoint, DateInput, DateModified)
				   VALUES ('$SchemeID', 'F', '$f_lower_limit', '$f_grade', '$f_grade_point' , now() , now())";
			$li->db_db_query($sql);
		}
	}
}
if($DistLowerLimit!="")
{
	$sql = "INSERT INTO RC_GRADING_SCHEME_GRADEMARK (SchemeID, Nature, LowerLimit, Grade, GradePoint, DateInput, DateModified)
               VALUES ('$SchemeID', 'D', '$DistLowerLimit', '$DistGrade', '$DistGradePoint' , now() , now())";
	$li->db_db_query($sql);
}

intranet_closedb();

$Result = ($success==1) ? "add" : "add_failed";

header("Location: index.php?Result=$Result");
?>