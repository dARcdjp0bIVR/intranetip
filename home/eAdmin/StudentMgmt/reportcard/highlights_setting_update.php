<?php
$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

$file_content = "";

// Format : bold|italic|underline|color:$color
$failed_style = array();
if ($failed_bold == 1) array_push($failed_style, 'bold');
if ($failed_italic == 1) array_push($failed_style, 'italic');
if ($failed_underline == 1) array_push($failed_style, 'underline');
if ($failed_color == 1)
{
	array_push($failed_style, 'color:'.$FailColor);
}
$file_content .= implode('|', $failed_style)."\n";

$distinction_style = array();
if ($distinction_bold == 1) array_push($distinction_style, 'bold');
if ($distinction_italic == 1) array_push($distinction_style, 'italic');
if ($distinction_underline == 1) array_push($distinction_style, 'underline');
if ($distinction_color == 1)
{
	array_push($distinction_style, 'color:'.$DistinctionColor);
}
$file_content .= implode('|', $distinction_style)."\n";

# Write to file
$li = new libfilesystem();
$location = $intranet_root."/file/reportcard";
$li->folder_new($location);
$file = $location."/highlights_setting.txt";

$success = $li->file_write($file_content, $file);

$Result = ($success==1) ? "update" : "update_failed";
header("Location: highlights_setting.php?Result=$Result");
?>