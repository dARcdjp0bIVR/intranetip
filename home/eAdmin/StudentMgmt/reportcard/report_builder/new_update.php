<?php
$PATH_WRT_ROOT = "../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
intranet_auth();
intranet_opendb();

$li = new libdb();

$ReportType = ($YearTermID == 0)? 'F' : $YearTermID;

$SerializedSettings = serialize($Settings);
$ReportTitle = $ReportTitle1."::".$ReportTitle2;
$Description = intranet_htmlspecialchars($Description);
$Footer = intranet_htmlspecialchars($Footer);
$ReportTitle = intranet_htmlspecialchars($ReportTitle);

$ShowPosition = ($ShowPosClassColumn==="") ? 0 : $ShowPosition;
$PositionRange = ($ShowPosition==1 || $ShowPosition==2) ? $PositionFrom.",".$PositionTo : "";

if ($NoHeader == 1) $NoHeader = $NoHeaderBr;
if ($LineHeight < 20) $LineHeight = 20;
if ($SignatureWidth < 120) $SignatureWidth = 120;

# Step 1: Insert record to RC_SUBJECT_GRADING_SCHEME
$sql = "INSERT INTO RC_REPORT_TEMPLATE
		(ReportTitle, ReportType, Description, Footer, ShowFullMark, DisplaySettings, RecordStatus, ShowPosition, PositionRange, HideNotEnrolled, NoHeader, LineHeight, SignatureWidth, AcademicYear, DecimalPoint, DateInput, DateModified)
		Values
		('$ReportTitle', '$ReportType', '$Description', '$Footer', '$ShowFullMark', '$SerializedSettings', '$RecordStatus', '$ShowPosition', '$PositionRange', '$HideNotEnrolled', '$NoHeader', '$LineHeight', '$SignatureWidth', '$AcademicYearID', '$DecimalPoint', now(), now()) ";
$li->db_db_query($sql);
$ReportID = $li->db_insert_id();

for($i=0; $i<sizeof($Form); $i++)
{
	$ClassLevelID = $Form[$i];

	# Step 2: Insert reocrd in RC_SUBJECT_GRADING_SCHEME_FORM
	$sql = "INSERT INTO RC_REPORT_TEMPLATE_FORM (ReportID, ClassLevelID, DateInput, DateModified) VALUES ('$ReportID', '$ClassLevelID', now(), now())";
	$li->db_db_query($sql);
}

intranet_closedb();
header("Location:report_subject.php?ReportID=$ReportID&ReportType=$ReportType");
?>

