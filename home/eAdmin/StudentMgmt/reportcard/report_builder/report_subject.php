<?php
$PATH_WRT_ROOT = "../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

if ($plugin['ReportCard'])
{
	include_once($PATH_WRT_ROOT."includes/libreportcard.php");
	$lreportcard = new libreportcard();
	$CurrentPage = "ReportBuilder_Template";
	$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
    if ($lreportcard->hasAccessRight())
    {
        $linterface = new interface_html();

###############################################################
		$SelectedFormArr = $lreportcard->GET_REPORT_TEMPLATE_FORM($ReportID);
		$SubjectGradingArr = $lreportcard->GET_REPORT_SUBJECT_GRADING($ReportID, $SelectedFormArr);
		$SubjectArray = $lreportcard->GET_REPORT_TEMPLATE_SUBJECT($ReportID, $SubjectGradingArr);
		$ColumnArray = $lreportcard->GET_REPORT_TEMPLATE_COLUMN($ReportID);
		$ReportCellSetting = $lreportcard->GET_REPORT_CELL($ReportID);
				
		# subject title display
		$SubjectTitleCell = "<td width='50%' class='tabletopnolink'>".$eReportCard['EngSubject']."</td><td width='50%' class='tabletopnolink'>".$eReportCard['ChiSubject']."</td>";

		# Generate Details Table
		$DetailsTable = "<table width='100%' cellspacing='0' cellpadding='5'>";
		$DetailsTable .= "<tr>
							<td width='30%' class='tablegreentop'>
								<table width='100%' border='0' cellpadding='5' cellspacing='0'><tr>".$SubjectTitleCell."</tr></table>
							</td>";
		$DetailsTable .= "<td width='10%' class='tabletopnolink tablegreentop' align='center'>".$eReportCard['FullMark']."</td>";

		for($i=0; $i<sizeof($ColumnArray); $i++)
		{
			list($ReportColumnID, $ColumnTitle, $Weight, $ShowPosition, $PositionRange, $IsColumnSum, $MarkSummaryWeight) = $ColumnArray[$i];
			if(($Weight!="")&&(!$IsColumnSum))
			{
				$ColumnTitle = $ColumnTitle."<br />".$Weight."%";
			}
			$DetailsTable .= "<td width='10%' align='center' class='tabletopnolink tablegreentop'>".$ColumnTitle."</td>";
		}
		$DetailsTable .= "<td width='10%' class='tabletopnolink tablegreentop' align='center'>".$eReportCard['OverallResult']."</td>";
		$DetailsTable .= "</tr>";
		
		$colnum = (!empty($ColumnArray)) ? sizeof($ColumnArray) + 4 : 4;
		if(is_array($SubjectArray))
		{
			$count = 0;
			foreach($SubjectArray as $CodeID => $Data)
			{
				if(is_array($Data))
				{
					foreach($Data as $CmpCodeID => $InfoArr)
					{
						$IsComponent = ($CmpCodeID>0) ? 1 : 0;
						$td_style = ($count%2==0) ? "tablegreenrow2" : "tablegreenrow1";
						$Prefix = ($IsComponent==1) ? "&nbsp;&nbsp;&nbsp;&nbsp;" : "";
						list($ReportSubjectID, $SubjectID, $EngSubjectName, $ChiSubjectName, $FullMark, $Scale) = $InfoArr;
						
						$DetailsTable .= "<tr>";
						
						// Subject Display
						$SubjectCell = "<td width='50%' class='tabletext'>".$Prefix.$EngSubjectName."</td><td width='50%' class='tabletext'>".$Prefix.$ChiSubjectName."</td>";
						
						$DetailsTable .= "<td class='tabletext $td_style'>
											<table width='100%' border='0' cellpadding='5' cellspacing='0'><tr>".$SubjectCell."</tr></table>
										</td>";
						$DetailsTable .= "<td class='tabletext $td_style' align='center'>".$FullMark."</td>";
						for($i=0; $i<sizeof($ColumnArray); $i++)
						{
							list($ReportColumnID, $ColumnTitle) = $ColumnArray[$i];
							$Setting = $ReportCellSetting[$ReportColumnID][$ReportSubjectID];
							
							$ScaleSelected = ($Setting=="" || $Setting==$Scale) ? "SELECTED='SELECTED'" : "";
							$NullSelected = ($Setting=="N/A") ? "SELECTED='SELECTED'" : "";

							$ScaleSelect = "<SELECT name='Scale_".$ReportColumnID."_".$ReportSubjectID."'>";
							$ScaleSelect .= "<OPTION value='".$Scale."' ".$ScaleSelected.">".$Scale."</OPTION>";
							$ScaleSelect .= "<OPTION value='N/A' ".$NullSelected.">N/A</OPTION>";
							$ScaleSelect .= "</SELECT>";
							$DetailsTable .= "<td class='tabletext $td_style' align='center'>".$ScaleSelect."</td>";
						}
						$DetailsTable .= "<td class='tabletext $td_style' align='center'>".$Scale."</td>";
						$DetailsTable .= "</tr>";

						$count++;
					}
				}
			}
		}
		else
		{
			$DetailsTable .= "<tr><td align='center' class='tabletext tablegreenrow2' colspan='".$colnum."'>".$i_no_record_exists_msg."</td></tr>";
		}
		$DetailsTable .= "</table>";

################################################################
# tag information
$TAGS_OBJ[] = array($eReportCard['ReportBuilder_Template'], "", 0);

$STEPS_OBJ[] = array($eReportCard['SetBasicInfo'], 0);
$STEPS_OBJ[] = array($eReportCard['SetTableDetails'], 1);

$linterface->LAYOUT_START();
?>
<script language="javascript">
function jREMOVE_SUBJECT(jSubjectID, jIsComponent)
{
	var alertConfirmRemove = (jSubjectID=="ALL") ? "<?=$eReportCard['RemoveAllItemAlert']?>" : "<?=$eReportCard['RemoveItemAlert']?>";

	if(confirm(alertConfirmRemove)){
		self.location.href='report_subject_remove.php?ReportID=<?=$ReportID?>&SubjectID=' + jSubjectID + '&IsComponent=' + jIsComponent;
	}
}
</script>

<form name="form1" action="report_subject_update.php" method="post">
<br />
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
</tr>
<tr>
	<td>
		<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tr><td align='right' colspan="2"><?= $linterface->GET_SYS_MSG($Result); ?></td></tr>
		<!--<tr><td valign="top" nowrap="nowrap" class="tabletext"><?= $eReportCard['TableDetails']; ?></td><td width="75%">&nbsp;</td></tr>-->
		<tr>
			<td colspan="2" align="right">
				<?=$linterface->GET_SMALL_BTN($eReportCard['SubjectManage'], "button", "javascript:newWindow('subject_manage.php?ReportID=$ReportID', 14)")?>
				&nbsp;
				<?=$linterface->GET_SMALL_BTN($eReportCard['ColumnManage'], "button", "javascript:newWindow('column_manage.php?ReportID=$ReportID&ReportType=$ReportType',14)")?>
			</td>
		</tr>
		<tr><td valign="top" nowrap="nowrap" colspan="2"><?=$DetailsTable?></td></tr>
		</table>
	</td>
</tr>
<tr>
	<td><table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
		<tr>
			<td align="center"><?= $linterface->GET_ACTION_BTN($button_submit, "submit", "", "btnSubmit")?>&nbsp;
			<?= $linterface->GET_ACTION_BTN($button_reset, "reset")?>&nbsp;
			<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:self.location.href='index.php'")?>
			</td>
		</tr>
	</table>
	</td>
</tr>
</table>

<input type="hidden" name="ReportID" value="<?=$ReportID?>">
<p></p>
</form>

<?

        $linterface->LAYOUT_STOP();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>
