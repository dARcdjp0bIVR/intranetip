<?php
$PATH_WRT_ROOT = "../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libreportcard.php");
intranet_auth();
intranet_opendb();

$li = new libdb();
$lreportcard = new libreportcard();

$ReportColumnIDList = (is_array($ReportColumnID)) ? implode(",", $ReportColumnID) : $ReportColumnID;
if($ReportColumnIDList!="")
{
	# Reomve record(s) in RC_REPORT_TEMPLATE_COLUMN
	$sql = "DELETE FROM RC_REPORT_TEMPLATE_COLUMN WHERE ReportColumnID IN (".$ReportColumnIDList.")";
	$success = $li->db_db_query($sql);
		
	if($success)
	{
		# Reomve record(s) in RC_REPORT_TEMPLATE_CELL
		$sql = "DELETE FROM RC_REPORT_TEMPLATE_CELL WHERE ReportColumnID IN (".$ReportColumnIDList.")";
		$li->db_db_query($sql);

		# Remove records in RC_MARKSHEET_SCORE
		$sql = "DELETE FROM RC_MARKSHEET_SCORE WHERE ReportColumnID IN (".$ReportColumnIDList.")";
		$li->db_db_query($sql);
		
		# Remove records in RC_SUB_MARKSHEET_COLUMN and RC_SUB_MARKSHEET_SCORE
		$sql = "DELETE FROM RC_SUB_MARKSHEET_COLUMN WHERE ReportColumnID IN (".$ReportColumnIDList.")";
		$li->db_db_query($sql);

		$sql = "DELETE FROM RC_SUB_MARKSHEET_SCORE WHERE ReportColumnID IN (".$ReportColumnIDList.")";
		$li->db_db_query($sql);
	}
}

$Result = ($success==1) ? "delete" : "delete_failed";
intranet_closedb();
header("Location:column_manage.php?Result=$Result&ReportID=$ReportID");
?>
