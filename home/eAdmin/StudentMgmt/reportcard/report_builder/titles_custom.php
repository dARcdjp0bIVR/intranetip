<?php
$PATH_WRT_ROOT = "../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

if ($plugin['ReportCard'])
{
	include_once($PATH_WRT_ROOT."includes/libreportcard.php");
	
	$lreportcard = new libreportcard();
	$CurrentPage = "ReportBuilder_TitlesCustomization";
	$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
    if ($lreportcard->hasAccessRight())
    {
        $linterface = new interface_html();
        $lf = new libfilesystem();
		
		# custom lang paths
		$EngCustomFile = $intranet_root."/file/reportcard/lang.en.php";
		$EngFileContent = $lf->file_read($EngCustomFile);
		$EngFileContent = str_replace("<?php", "", $EngFileContent);
		$EngFileContent = str_replace("?>", "", $EngFileContent);

		$ChiCustomFile = $intranet_root."/file/reportcard/lang.b5.php";
		$ChiFileContent = $lf->file_read($ChiCustomFile);
		$ChiFileContent = str_replace("<?php", "", $ChiFileContent);
		$ChiFileContent = str_replace("?>", "", $ChiFileContent);

		########################English########################		
		$EngCustomTitleArray = explode("\n", $EngFileContent); 
		
		for($i=0; $i<sizeof($EngCustomTitleArray); $i++)
		{	
			$EngCustomTitleArrayList = $EngCustomTitleArray[$i];
			$PrefixVaraibleName = substr($EngCustomTitleArrayList, 1, 20);
			if($PrefixVaraibleName!="eReportCardBilingual" && !empty($EngCustomTitleArrayList))
			{
				$t_s_pos = strpos($EngCustomTitleArrayList, "['");
				$t_e_pos = strpos($EngCustomTitleArrayList, "']");
				$title_id = trim(substr($EngCustomTitleArrayList, $t_s_pos+2, $t_e_pos-$t_s_pos-2));
				if($title_id!="")
				{
					$v_s_pos = strpos($EngCustomTitleArrayList, "\"");
					$v_e_pos = strrpos($EngCustomTitleArrayList, "\"");
					$title_value = trim(substr($EngCustomTitleArrayList, $v_s_pos+1, $v_e_pos-$v_s_pos-1));
					$EngArray[$title_id] = stripslashes($title_value);
				}
			}
		}
		#########################English End#######################	
		
		########################Chinese Start########################	
		$ChiCustomTitleArray = explode("\n", $ChiFileContent);
		
		for($i=0; $i<sizeof($ChiCustomTitleArray); $i++)
		{	
			$ChiCustomTitleArrayList = $ChiCustomTitleArray[$i];
			$PrefixVaraibleName = substr($ChiCustomTitleArrayList, 1, 20);
			if($PrefixVaraibleName!="eReportCardBilingual" && !empty($ChiCustomTitleArrayList))
			{		
				$t_s_pos = strpos($ChiCustomTitleArrayList, "['");
				$t_e_pos = strpos($ChiCustomTitleArrayList, "']");
				$title_id = trim(substr($ChiCustomTitleArrayList, $t_s_pos+2, $t_e_pos-$t_s_pos-2));

				if($title_id!="")
				{
					$v_s_pos = strpos($ChiCustomTitleArrayList, "\"");
					$v_e_pos = strrpos($ChiCustomTitleArrayList, "\"");
					$title_value = trim(substr($ChiCustomTitleArrayList, $v_s_pos+1, $v_e_pos-$v_s_pos-1));
					$ChiArray[$title_id] = stripslashes($title_value);
				}
			}
		}

		########################Chinese End########################
        
		# Get title array
		$ChiTitleArray = $eReportCard_ReportTitleArray["b5"];
		$EngTitleArray = $eReportCard_ReportTitleArray["en"];
		
		#Read lang file						
		$count = 0;
		$rx = "";
		foreach($EngTitleArray as $ID => $EngTitle)
		{
			$css = ($count%2?"2":"");
			$ChiTitle = $ChiTitleArray[$ID];

			$rx .= "<tr class='tablegreenrow".$css."'>";
			$rx .= "<td class='tabletext'>".++$count."</td>";
			$rx .= "<td class='tabletext' nowrap='nowrap'>".$EngTitle."</td>";
			$rx .= "<td class='tabletext' nowrap='nowrap'>".$ChiTitle."</td>"; 
		
			$rx .= "<td class='tabletext'><input class='textboxtext' type='text' name='{$ID}_eng' maxlength='255' value='".intranet_htmlspecialchars($EngArray[$ID])."' /></td>";
			$rx .= "<td class='tabletext'><input class='textboxtext' type='text' name='{$ID}_chi' maxlength='255' value='".intranet_htmlspecialchars($ChiArray[$ID])."' /></td>";
			$rx .= "</tr>";
		}
		
    # tag information
		$TAGS_OBJ[] = array($eReportCard['ReportBuilder_TitlesCustomization'], "", 0);
		
		//echo "<div id='top'></div>";
		$linterface->LAYOUT_START();		
?>
<br/>
<form name="form1" method="POST" action="titles_custom_update.php">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="4">
<tr><td align="right"><?= $linterface->GET_SYS_MSG($Result); ?></td></tr>
<tr>
	<td>
		<table width="90%" border="0" cellpadding="4" cellspacing="0" align="center">
		<tr>
			<td align='right' class='tabletext' nowrap='nowrap' colspan="5"><a class='tablegreenlink' href='#bottom'><?=$eReportCard['GoToBottom']?></a></td>
		</tr>
		<tr class='tablegreentop'>
			<td class="tableTitle tabletoplink" width='1'>#</td>
			<td class="tableTitle tabletoplink" nowrap='nowrap'><?=$eReportCard['DefaultTitle']." (".$eReportCard['Eng'].")"?></td>
			<td class="tableTitle tabletoplink" nowrap='nowrap'><?=$eReportCard['DefaultTitle']." (".$eReportCard['Chi'].")"?></td>
			<td class="tableTitle tabletoplink" nowrap='nowrap'><?=$eReportCard['CustomTitle']." (".$eReportCard['Eng'].")"?></td>
			<td class="tableTitle tabletoplink" nowrap='nowrap'><?=$eReportCard['CustomTitle']." (".$eReportCard['Chi'].")"?></td>
		</tr>
		<?=$rx?>
		<tr>
			<td align='right' class='tabletext' nowrap='nowrap' colspan="5"><a class='tablegreenlink' href='#top'><?=$eReportCard['GoToTop']?></a></td>
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td>
	<table width="95%" border="0" cellpadding="4" cellspacing="0" align="center">
	<tr>
		<td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
	</tr>
	<tr>
		<td align="center" id="bottom">
		<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "", "btnSubmit")?>&nbsp;
		<?= $linterface->GET_ACTION_BTN($button_reset, "reset")?>&nbsp;
		</td>
	</tr>
	</table>
	</td>
</tr>
</table>
<br/>

</form>
	<?
        $linterface->LAYOUT_STOP();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>
