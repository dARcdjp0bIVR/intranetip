<?php
$PATH_WRT_ROOT = "../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
intranet_auth();
intranet_opendb();

$li = new libdb();
$ReportIDList = (is_array($ReportID)) ? implode(",", $ReportID) : $ReportID;

# Update record status in RC_REPORT_TEMPLATE
$sql = "UPDATE RC_REPORT_TEMPLATE SET RecordStatus = '$RecordStatus' WHERE ReportID IN ($ReportIDList)";
$success = $li->db_db_query($sql);

$Result = ($success==1) ? "update" : "update_failed";
intranet_closedb();
header("Location:index.php?Result=$Result");
?>

