<?php
$PATH_WRT_ROOT = "../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libreportcard.php");
intranet_auth();
intranet_opendb();

$li = new libdb();
$lreportcard = new libreportcard();

if($ReportSubjectID!="")
{
	########################### FUNCTION START #####################################
	function GET_DISPLAY_ORDER($TempOrderArray)
	{
		global $RSOrderArr, $LastOrder, $ParentSubjectID;
		
		$DisplayOrder = -1;

		$AfterOrder = array_shift($TempOrderArray);
		list($RSID, $SID, $ParentSID) = $RSOrderArr[$AfterOrder];
		if($ParentSubjectID>0 && $ParentSID==$ParentSubjectID)
		{
			$InsertSameParent = 1;				
			$DisplayOrder = array_shift($TempOrderArray);
			return array($DisplayOrder, $InsertSameParent);
		}
		
		while($AfterOrder<$LastOrder)
		{
			$AfterOrder = array_shift($TempOrderArray);
			list($RSID, $SID, $ParentSID) = $RSOrderArr[$AfterOrder];

			if($ParentSID==$SID)
			{
				$DisplayOrder = $AfterOrder;
				return array($DisplayOrder, 0);
				break;
			}
		}

		return array($DisplayOrder, 0);
	}
	########################### FUNCTION END #####################################

	# UPDATE the posistion option of the subject
	/*
	$ShowPosClassColumn = ($ShowPosClassColumn==="") ? 0 : $ShowPosClassColumn;
	$ShowPosFormColumn = ($ShowPosFormColumn==="") ? 0 : $ShowPosFormColumn;
	$ClassPosRange = ($ShowPosClassColumn==1) ? $ClassPosFrom.",".$ClassPosTo : "";
	$FormPosRange = ($ShowPosFormColumn==1) ? $FormPosFrom.",".$FormPosTo : "";

	$FieldValue = "ShowPosClassColumn='$ShowPosClassColumn', ";
	$FieldValue .= "ShowPosFormColumn='$ShowPosFormColumn', ";
	$FieldValue .= "ClassPosRange='$ClassPosRange', ";
	$FieldValue .= "FormPosRange='$FormPosRange', ";
	$FieldValue .= "DateModified=now()";

	*/
	$HidePosition = ($HidePosition==="") ? 0 : $HidePosition;
	$FieldValue = "HidePosition='$HidePosition', ";
	$FieldValue .= "DateModified=now()";			

	$sql = "UPDATE 
				RC_REPORT_TEMPLATE_SUBJECT 
			SET 
				$FieldValue
			WHERE 
				SubjectID = '$SubjectID' 
				AND ReportID = '$ReportID'
			";
	$success = $li->db_db_query($sql);

	################################# STEP 1: Check the type of the Subject ##############################
	# Retreive related subjects of the target subjects and all selected subjects
	$SubjectArray = $lreportcard->GET_RELATED_SUBJECT($SubjectID);
	$SelectedSubjectIDArray = $lreportcard->GET_REPORT_SELECTED_SUBJECT_ID($ReportID);

	# Check whether the selected subject is a component subject
	$ParentSubjectID = 0;
	if($SubjectArray[0]!=$SubjectID || sizeof($SubjectArray)>1)
	{
		$ParentSubjectID = $SubjectArray[0];
	}
	
	################################# STEP 2: Set the Display Order #####################################
	# GET Last Display Order
	$sql = "SELECT MAX(DisplayOrder) FROM RC_REPORT_TEMPLATE_SUBJECT WHERE ReportID = '$ReportID';";
	$row = $li->returnVector($sql);
	$LastOrder = $row[0];

	# Get DisplayOrder List
	$sql = "SELECT 
					a.ReportSubjectID,
					a.SubjectID,
					c.RecordID,
					a.DisplayOrder
				FROM 
					RC_REPORT_TEMPLATE_SUBJECT as a
					LEFT JOIN ASSESSMENT_SUBJECT as b ON a.SubjectID = b.RecordID
					LEFT JOIN ASSESSMENT_SUBJECT as c ON b.CODEID = c.CODEID AND (c.CMP_CODEID IS NULL || c.CMP_CODEID = '')
				WHERE 
					a.ReportID = '$ReportID' 
					And
					b.RecordStatus = 1
					And
					c.RecordStatus = 1
				ORDER BY 
					a.DisplayOrder
				";
	$row = $li->returnArray($sql, 4);

	if($InsertAfterID=="" && $row[0][0]==$ReportSubjectID)
	{
		$NoOrderChange = 1;
	}
	else
	{
		for($i=0; $i<sizeof($row); $i++)
		{
			list($RSID, $SID, $ParentSID, $Order) = $row[$i];
			if($InsertAfterID==$RSID)
			{
				if($row[$i+1][0]==$ReportSubjectID)
				{
					$NoOrderChange = 1;
				}
				else
				{
					$AfterOrder = $Order;
					$Reorder = 1;
				}
			}
			$RSOrderArr[$Order] = array($RSID, $SID, $ParentSID);
			
			# Array for handling component subject
			if($ParentSubjectID>0 && $ParentSID==$ParentSubjectID)
			{
				$SArr[$Order] = $SID;
			}
		}
	}
	
	# prevent inserting parent subject to it's component subject
	if($SubjectID==$RSOrderArr[$AfterOrder][2])
	{
		$NoOrderChange = 1;
	}
	
	# GO only there is order change
	if($NoOrderChange!=1)
	{
  		if(is_array($RSOrderArr))
		{
  			$OrderArr = array_keys($RSOrderArr);
		}
		
		$Reorder = 1;
		if($InsertAfterID!="" && is_array($OrderArr))
		{
			$TempOrderArray = array_slice($OrderArr, array_search($AfterOrder, $OrderArr));

			list($DisplayOrder, $InsertSameParent) = GET_DISPLAY_ORDER($TempOrderArray);
			
			if($DisplayOrder<0)	// The new order is in the end
			{
				$DisplayOrder = $LastOrder+1;
				$Reorder = 0;
			}
		}
		else	// The new order is in the beginning
		{					
			$DisplayOrder = 1;
		}
		
		# update the subject array if component subject selected.
		if($ParentSubjectID>0)
		{
			unset($SubjectArray);
			if($InsertSameParent==1)
			{
				$SubjectArray[] = $SubjectID;
				$OldSubjectOrderArray[] = $OldDisplayOrder;
			}
			else
			{
				foreach($SArr as $s_order => $s_id)
				{
					$SubjectArray[] = $s_id;
					$OldSubjectOrderArray[] = $s_order;
				}
			}
		}
		else
		{
			$OldSubjectOrderArray[] = $OldDisplayOrder;
		}
		
		# IF DisplayOrder get is same as the old one, no udpate need!
		if($DisplayOrder!=$OldSubjectOrderArray[0])
		{
			######################### STEP 3: Update Record ####################################
			# update record into RC_REPORT_TEMPLATE_SUBJECT
			$FinalDisplayOrder = $DisplayOrder;
			for($i=0; $i<sizeof($SubjectArray); $i++)
			{
				$sid = $SubjectArray[$i];
				$old_order = $OldSubjectOrderArray[$i];

				$sql = "UPDATE RC_REPORT_TEMPLATE_SUBJECT SET DisplayOrder='$FinalDisplayOrder', DateModified=now() WHERE SubjectID = '$sid' AND ReportID = '$ReportID'";
				$success = $li->db_db_query($sql);

				unset($RSOrderArr[$old_order]);
				$FinalDisplayOrder++;
			}
			
			######################### STEP 4: Reordering ####################################
			if(is_array($RSOrderArr))
			{
				unset($OrderArr);
				$OrderArr = array_keys($RSOrderArr);
			}
			if($Reorder!=0 && is_array($OrderArr))
			{
				$NewOrder = $FinalDisplayOrder;
				$StartPos = ($DisplayOrder==1) ? 0 : array_search($DisplayOrder, $OrderArr);
				$SortingOrderArray = array_slice($OrderArr, $StartPos);
				
				for($i=0; $i<sizeof($SortingOrderArray); $i++)
				{
					$Order = $SortingOrderArray[$i];
					$rsid = $RSOrderArr[$Order][0];
			
					$sql = "UPDATE RC_REPORT_TEMPLATE_SUBJECT SET DisplayOrder = '".$NewOrder."' WHERE ReportSubjectID = '".$rsid."'";
					$li->db_db_query($sql);
					$NewOrder++;
				}
			}
			######################################################################################
		}
	}
}

intranet_closedb();

$Result = ($success==1) ? "update" : "update_failed";
header("Location:subject_manage.php?Result=$Result&ReportID=$ReportID");
?>
