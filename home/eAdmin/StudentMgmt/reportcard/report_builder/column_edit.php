<?php
$PATH_WRT_ROOT = "../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
//include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
intranet_auth();
intranet_opendb();

if ($plugin['ReportCard'])
{
	include_once($PATH_WRT_ROOT."includes/libreportcard.php");
	$lreportcard = new libreportcard();
	$CurrentPage = "ReportBuilder_Template";
	$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
	$MODULE_OBJ['title'] = $eReportCard['ColumnManage'];

	if ($lreportcard->hasAccessRight())
    {
         $linterface = new interface_html("popup.html");

################################################################
		
		$ReportColumnID = (is_array($ReportColumnID)) ? $ReportColumnID[0] : $ReportColumnID;
		$InfoArr = $lreportcard->GET_REPORT_COLUMN_DETAILS($ReportColumnID);
		list($ColumnTitle, $Weight, $DisplayOrder, $ShowPosition, $PositionRange, $MarkSummaryWeight, $IsColumnSum, $NotForInput) = $InfoArr;

		$WeightTableStyle = ($IsColumnSum) ? "none" : "";
		
		# Build ColumnTable
		$ColumnTable = "<table width='100%' border='0' cellpadding='5' cellspacing='0'>";
		//$semester_data = getSemesters();
		$ReportInfoArr = $lreportcard->GET_REPORT_TEMPLATE($ReportID);
		$ReportAcademicYearID = $ReportInfoArr['AcademicYear'];
		$AcademicYearObj = new academic_year($ReportAcademicYearID);
		$TermInfoArr = $AcademicYearObj->Get_Term_List($returnAsso=0, $ParLang='en');
		$numOfTerm = count($TermInfoArr);
		
		for($i=0; $i<$numOfTerm; $i++)
		{
			$thisYearTermID = $TermInfoArr[$i]['YearTermID'];
			$thisYearTermName = $TermInfoArr[$i]['TermName'];
			
			if($ColumnTitle == $thisYearTermName)
			{
				$SemesterChecked = "CHECKED='CHECKED'";
				$IsChecked = 1;
			}
			else
				$SemesterChecked = "";

			$ColumnTable .= "<tr><td class='tabletext' nowrap='nowrap'><input type='radio' name='ColumnTitle' value='".intranet_htmlspecialchars($thisYearTermName)."' id='title_".$thisYearTermID."' $SemesterChecked onClick='displayTable(\"WeightTable\", \"\")'/><label for='title_".$thisYearTermID."'>".$thisYearTermName."</label></td></tr>";

			if($thisYearTermID==$ReportType)
				break;
		}
		if($IsChecked!=1)
		{
			switch($ColumnTitle)
			{
				case $eReportCard['CommonTest']:
					$CommentTestChecked = "CHECKED='CHECKED'";
					break;
				case $eReportCard['FinalExam']:
					$FinalExamChecked = "CHECKED='CHECKED'";
					break;
				case $eReportCard['CourseMark']:
					$CourseMarkChecked = "CHECKED='CHECKED'";
					break;
				default:
					$CustomChecked = "CHECKED='CHECKED'";
					$CustomTitle = $ColumnTitle;
					break;
			}
		}
		
		if ($IsColumnSum) {
			$CustomChecked = "";
			$CustomTitle = "";
			$WeightCustomChecked = "CHECKED='CHECKED'";
			$WeightCustomTitle = $ColumnTitle;
		}
		
		$ColumnTable .= "<tr><td class='tabletext' nowrap='nowrap' width='30%'><input type='radio' name='ColumnTitle' value='".$eReportCard['CommonTest']."' id='title_".$eReportCard['CommonTest']."' $CommentTestChecked onClick='displayTable(\"WeightTable\", \"\")'/><label for='title_".$eReportCard['CommonTest']."'>".$eReportCard['CommonTest']."</label></td></tr>";
		$ColumnTable .= "<tr><td class='tabletext' nowrap='nowrap'><input type='radio' name='ColumnTitle' value='".$eReportCard['FinalExam']."' id='title_".$eReportCard['FinalExam']."' $FinalExamChecked onClick='displayTable(\"WeightTable\", \"\")'/><label for='title_".$eReportCard['FinalExam']."'>".$eReportCard['FinalExam']."</label></td></tr>";
		$ColumnTable .= "<tr><td class='tabletext' nowrap='nowrap'><input type='radio' name='ColumnTitle' value='".$eReportCard['CourseMark']."' id='title_".$eReportCard['CourseMark']."' $CourseMarkChecked onClick='displayTable(\"WeightTable\", \"\")'/><label for='title_".$eReportCard['CourseMark']."'>".$eReportCard['CourseMark']."</label></td></tr>";
		$ColumnTable .= "<tr><td class='tabletext' nowrap='nowrap'><input type='radio' name='ColumnTitle' value='' id='title_".$eReportCard['Custom']."' $CustomChecked onClick='displayTable(\"WeightTable\", \"\")'/><label for='title_".$eReportCard['Custom']."'>".$eReportCard['Custom']."&nbsp;<input type='text' name='CustomTitle' class='tabletext' value='".$CustomTitle."' /></label></td></tr>";
		$ColumnTable .= "<tr><td class='tabletext' nowrap='nowrap'><input type='radio' name='ColumnTitle' value='SYS_SUM_COLUMN' id='title_SYS_SUM_COLUMN' $WeightCustomChecked onClick='displayTable(\"WeightTable\", \"none\")'/><label for='title_SYS_SUM_COLUMN'>".$eReportCard['MarkSummary']."&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type='text' name='WeightCustomTitle' class='tabletext' value='".$WeightCustomTitle."' onFocus='jCHECK_WEIGHTCUSTOM(); displayTable(\"WeightTable\", \"none\")'/></label></td></tr>";
		$ColumnTable .= "</table>";
		
		# Build Insert Seletion
		$InsertSelect = $lreportcard->GET_REPORT_COLUMN_INSERT_SELECTION($ReportID, $ReportColumnID);
		
		# Build Position Settings Table
		if($PositionRange!="")
		{
			$TmpArr = explode(",", $PositionRange);
			$PositionFrom = $TmpArr[0];
			$PositionTo = $TmpArr[1];
		}
		
		$RangeTableStyle = ($ShowPosition==1 || $ShowPosition==2) ? "" : "style='display: none'";
		$RangeTable = "<table id='RangeTable' $RangeTableStyle><tr>
							<td colspan=3 nowrap='nowrap' class='tabletext'>".$i_From."&nbsp;<input type='text' name='PositionFrom' class='textboxnum' value='".$PositionFrom."'/>&nbsp;".$i_To."&nbsp;<input type='text' name='PositionTo' class='textboxnum' value='".$PositionTo."'/></td>
						</tr></table>";

		$PositionTable = "<table>";
		$PositionTable .= "<tr>
							<td class='tabletext'><input type='radio' name='ShowPosition' id='HidePosition' value='0' onClick='displayTable(\"RangeTable\", \"none\")' ".($ShowPosition==0?"CHECKED='CHECKED'":"")." /><label for='HidePosition'>".$eReportCard['HidePosition']."</label></td>
						</tr>
						<tr>
							<td class='tabletext'><input type='radio' name='ShowPosition' id='ShowClassPosition' value='1' onClick='displayTable(\"RangeTable\", \"\")' ".($ShowPosition==1?"CHECKED='CHECKED'":"")." /><label for='ShowClassPosition'>".$eReportCard['ShowClassPosition']."</label></td>
						</tr>
						<tr>
							<td class='tabletext'><input type='radio' name='ShowPosition' id='ShowFormPosition' value='2' onClick='displayTable(\"RangeTable\", \"\")' ".($ShowPosition==2?"CHECKED='CHECKED'":"")." /><label for='ShowFormPosition'>".$eReportCard['ShowFormPosition']."</label></td>
						</tr>";
		$PositionTable .= "</table>".$RangeTable;
################################################################
		
		# page navigation (leave the array empty if no need)
		$PAGE_NAVIGATION[] = array($eReportCard['ColumnManage'], "column_manage.php?ReportID=$ReportID");
		$PAGE_NAVIGATION[] = array($button_edit, "");

		$linterface->LAYOUT_START();
?>
<script language="javascript">
function checkform(obj){
	
	for (var i = 0; i < obj.ColumnTitle.length; i++) {
		if(obj.ColumnTitle[i].checked)
		{
			if (obj.ColumnTitle[i].value=='' && obj.CustomTitle.value=='')
			{
				alert("<?=$i_alert_pleasefillin.$eReportCard['ColumnTitle'];?>");
				obj.CustomTitle.focus();
				return false;
			}
			break;
		}
	}
	
	for (var i = 0; i < obj.ShowPosition.length; i++) {
		if(obj.ShowPosition[i].checked)
		{
			if ((obj.ShowPosition[i].value==1 || obj.ShowPosition[i].value==2) && (obj.PositionFrom.value=='' || obj.PositionTo.value==''))
			{
				alert("<?=$i_alert_pleasefillin.$eReportCard['PositionRange'];?>");
				obj.PositionFrom.focus();
				return false;
			}
			break;
		}
	}
	
	return true;
}

function jCHECK_WEIGHTCUSTOM(){
	obj = document.form1;
	for (var i = 0; i < obj.ColumnTitle.length; i++) {
		if (obj.ColumnTitle[i].value=='SYS_SUM_COLUMN')
		{
			obj.ColumnTitle[i].checked=true;
			break;
		}
	}
}
</script>
<form name="form1" action="column_edit_update.php" method="post" onSubmit="return checkform(this);">
<br />
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
</tr>
<tr>
	<td>

	<table width="90%" border="0" cellpadding="5" cellspacing="1">
	<tr>
		<td>
			<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
			<tr><td valign="top" nowrap="nowrap" class="formfieldtitle tabletext" width='30%'><?=$eReportCard['ColumnTitle']?></td><td class="tabletext" width="75%"><?=$ColumnTable?></td></tr>
			<tr><td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$eReportCard['InsertAfter']?></td><td class="tabletext" width="75%"><?=$InsertSelect?></td></tr>
			</table>
			<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center" id='WeightTable' style="display: <?=$WeightTableStyle?>">
			<tr><td valign="top" nowrap="nowrap" class="formfieldtitle tabletext" width='30%'><?=$eReportCard['WeightTotalSummary']?></td><td width="75%"><input type='text' name='Weight' class="textboxnum"  value="<?= $Weight?>"/>&nbsp;<span class="textboxnum">%</span></td></tr>
			<tr><td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$eReportCard['WeightMarkSummary']?></td><td width="75%"><input type='text' name='MarkSummaryWeight' class="textboxnum" value="<?= $MarkSummaryWeight?>" />&nbsp;<span class="textboxnum">%</span></td></tr>
			</table>
			<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
			<tr>
				<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext" width='30%'><?=$eReportCard['ShowPosition']?></td>
				<td width="75%"><?=$PositionTable?></td>
			</tr>
			</table>
			<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
			<tr>
				<td valign="top" nowrap="nowrap" class="tabletext" width='30%'></td>
				<td width="75%" class="tabletext">&nbsp;<input type="checkbox" name="NotAllowToInput" id="NotAllowToInput" value="1" <?if ($NotForInput) print "checked"?>>&nbsp;<label for="NotAllowToInput"><?= $eReportCard['NotAllowToInput'] ?></label>	</td>
			</tr>
			</table>
		</td>
	</tr>
	</table>

</td>
</tr>
<tr>
	<td><table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
		<tr>
			<td align="center"><?= $linterface->GET_ACTION_BTN($button_submit, "submit", "", "btnSubmit")?>&nbsp;
			<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "")?>
			<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:self.location.href='column_manage.php?ReportID=$ReportID&ReportType=$ReportType'")?>
			</td>
		</tr>
	</table>
	</td>
</tr>
</table>
<input type="hidden" name="ReportID" value="<?=$ReportID?>" />
<input type="hidden" name="ReportColumnID" value="<?=$ReportColumnID?>" />
<input type="hidden" name="OldDisplayOrder" value="<?=$DisplayOrder?>" />
<input type="hidden" name="ReportType" value="<?=$ReportType?>" />

<p></p>
</form>
<?

        $linterface->LAYOUT_STOP();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>