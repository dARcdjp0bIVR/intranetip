<?php
$PATH_WRT_ROOT = "../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
//include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

if ($plugin['ReportCard'])
{
	include_once($PATH_WRT_ROOT."includes/libreportcard.php");
	$lreportcard = new libreportcard();
	$CurrentPage = "ReportBuilder_Template";
	$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
    if ($lreportcard->hasAccessRight())
    {
        $linterface = new interface_html();

###############################################################
		$ReportID = (is_array($ReportID)) ? $ReportID[0] : $ReportID;
		$BasicInfo = $lreportcard->GET_REPORT_TEMPLATE($ReportID);

		list($ReportTitle, $ReportType, $Description, $ShowFullMark, $DisplaySettings, $RecordStatus, $ShowPosition, $PositionRange, $HideNotEnrolled, $Footer, $NoHeader, $LineHeight, $SignatureWidth, $AcademicYear, $DecimalPoint) = $BasicInfo;
		list($ReportTitle1, $ReportTitle2) = explode("::", $ReportTitle);
		if ($LineHeight == "") $LineHeight = 20;
		if ($SignatureWidth == "") $SignatureWidth = 120;

		$SelectedFormArr = $lreportcard->GET_REPORT_TEMPLATE_FORM($ReportID);
		
		# Generate Report Type Fields
		$TypeTable = "<table width='100%'><tr>";
		$TypeTable .= "<td class='tabletext' width='20%'><INPUT type='radio' name='ReportType' id='type_F' value='F' ".($ReportType=="F"?"CHECKED='CHECKED'":"")."/><label for='type_F'>".$eReportCard['FullYear']."</label></td>";
		$semester_data = getSemesters();
		for($i=0; $i<sizeof($semester_data); $i++)
		{
			$TmpArr = explode("::", $semester_data[$i]);
			$Semester = $TmpArr[0];
			$Checked = ($Semester==$ReportType) ? "CHECKED='CHECKED'" : "";
			$TypeTable .= "<td class='tabletext' width='20%'><INPUT type='radio' name='ReportType' id='type_".$Semester."' value='".$Semester."' $Checked /><label for='type_".$Semester."'>".$Semester."</label></td>";
		}
		$TypeTable .= "</tr></table>";
		
		# Generate Settings Table
		$SettingResultArr = unserialize($DisplaySettings);
		$SettingsTable = "<table width='100%'>";
		foreach($eReportCard['DisplaySettingsArray'] as $SettingType => $SettingArr)
		{
			$SettingsTable .= "<tr><td class='tabletext'>".$eReportCard[$SettingType]."</td></tr>";
			for($k=0; $k<sizeof($SettingArr); $k++)
			{
				$SettingID = $SettingArr[$k];
				$SettingName = $eReportCard[trim($SettingID)];
				$Checked = (!empty($SettingResultArr[$SettingType]) && $SettingResultArr[$SettingType][$SettingID]==1) ? "CHECKED='CHECKED'" : "";
				$SettingsTable .= "<tr><td class='tabletext'><input type='checkbox' name=\"Settings[$SettingType][$SettingID]\" id='".$SettingType."_".$SettingID."' value='1' $Checked /><label for='".$SettingType."_".$SettingID."'>".$SettingName."</label></td></tr>";
			}
			$SettingsTable .= "<tr><td>&nbsp;</td></tr>";
		}
		$SettingsTable .= "</table>";
		
		# Generate Form Table
		# Biuld Form Selection Table
		$FormArray = $lreportcard->GET_FORMS();
		$FormTable = "<table width='100%' border='0' cellpadding='4' cellspacing='0' align='center' id='FormTable'>";
		for($i=0; $i<sizeof($FormArray); $i++)
		{
			list($FormID, $FormName) = $FormArray[$i];
			$Checked = (is_array($SelectedFormArr) && in_array($FormID, $SelectedFormArr)) ? "CHECKED='CHECKED'" : "";

			$FormTable .= ($i%4==0) ? "<tr>" : "";
			$FormTable .= "<td class='tabletext'><input type='checkbox' name='Form[]' value='".$FormID."' id='form_".$FormID."' $Checked /><label for='form_".$FormID."'>".$FormName."</label></td>";
			if($i%4==3)
			{
				$FormTable .= "</tr>";
			}
			else if(($i+1)==sizeof($FormArray))
			{
				$FormTable .= "<td colspan='".(3-($i%4))."'>&nbsp;</td></tr>";
			}
		}
		$FormTable .= "</table>";
		
		# Generate Ranking Type Table
		$RankingType = $SettingResultArr["RankingType"];
		$RankingTypeTable = "<table width='100%' border='0' cellpadding='4' cellspacing='0'>";
		$RankingTypeTable .= "<tr>
								<td class='tabletext'><input type='radio' name=\"Settings[RankingType]\" id='RankingType1' value='0' ".($RankingType==0?"CHECKED='CHECKED'":'')." /><label for='RankingType1'>".$eReportCard['RankingType1']."</label></td>
							 </tr>
							 <tr>
								<td class='tabletext'><input type='radio' name=\"Settings[RankingType]\" id='RankingType2' value='1' ".($RankingType==1?"CHECKED='CHECKED'":'')." /><label for='RankingType2'>".$eReportCard['RankingType2']."</label></td>
							</tr>";
		$RankingTypeTable .= "</table>";
		
		# Generate Bilingual Report Title Table
		$BilingualReportTitle = $SettingResultArr["BilingualReportTitle"];
		$BilingualReportTitleTable = "<table width='50%' border='0' cellpadding='4' cellspacing='0'>";
		$BilingualReportTitleTable .= "<td class='tabletext'><input type='radio' name='Settings[BilingualReportTitle]' value='1' id='bilingual_yes' ".($BilingualReportTitle==1?"CHECKED='CHECKED'":"")." onClick=\"displayTable('SubjectTitleDisplayRow','none');\" /><label for='bilingual_yes'>".$i_general_yes."</label></td>";
		$BilingualReportTitleTable .= "<td class='tabletext'><input type='radio' name='Settings[BilingualReportTitle]' value='0' id='bilingual_no' ".($BilingualReportTitle==0?"CHECKED='CHECKED'":"")." onClick=\"displayTable('SubjectTitleDisplayRow','block');\" /><label for='bilingual_no'>".$i_general_no."</label></td>";
		$BilingualReportTitleTable .= "</table>";

		# Generate Subject Title Table
		$SubjectTableDisplayStyle = ($BilingualReportTitle==1) ? "style='display: none;'" : "style='display: block;'"; 
		$SubjectTitleType = $SettingResultArr["SubjectTitleType"];
		$SubjectTitleTable = "<table width='100%' border='0' cellpadding='4' cellspacing='0'>";
		$SubjectTitleTable .= "<tr>
							<td class='tabletext'><input type='radio' name=\"Settings[SubjectTitleType]\" id='SubjectTitleType0' value='0' ".($SubjectTitleType==0?"CHECKED='CHECKED'":'')." /><label for='SubjectTitleType0'>".$eReportCard['SubjectTitleDisplayBoth']."</label></td>
						</tr>
						<tr>
							<td class='tabletext'><input type='radio' name=\"Settings[SubjectTitleType]\" id='SubjectTitleType1' value='1' ".($SubjectTitleType==1?"CHECKED='CHECKED'":'')." /><label for='SubjectTitleType1'>".$eReportCard['SubjectTitleDisplayEng']."</label></td>
						</tr>
						<tr>
							<td class='tabletext'><input type='radio' name=\"Settings[SubjectTitleType]\" id='SubjectTitleType2' value='2' ".($SubjectTitleType==2?"CHECKED='CHECKED'":'')." /><label for='SubjectTitleType2'>".$eReportCard['SubjectTitleDisplayChi']."</label></td>
						</tr>";
		$SubjectTitleTable .= "</table>";

		# Generate Parent Subject Result Calculation  Table
		$ResultCalculationType = $SettingResultArr["ResultCalculationType"];
		$ParentSubjectCalTable = "<table width='100%' border='0' cellpadding='4' cellspacing='0'>";
		$ParentSubjectCalTable .= "<tr><td class='tabletext'><input type='radio' name=\"Settings[ResultCalculationType]\" id='ResultCalType1' value='0' ".($ResultCalculationType==0?"CHECKED='CHECKED'":'')." /><label for='ResultCalType1'>".$eReportCard['ParentSubjectCalMethod1']."</label></td></tr>";
		$ParentSubjectCalTable .= "<tr><td class='tabletext'><input type='radio' name=\"Settings[ResultCalculationType]\" id='ResultCalType2' value='1' ".($ResultCalculationType==1?"CHECKED='CHECKED'":'')." /><label for='ResultCalType2'>".$eReportCard['ParentSubjectCalMethod2']."</label></td></tr>";
		$ParentSubjectCalTable .= "</table>";

		# Generate Result Display Type Table
		$ResultDisplayType = $SettingResultArr["ResultDisplayType"];
		$ResultDisplayTable = "<table width='100%' border='0' cellpadding='4' cellspacing='0'>";
		$ResultDisplayTable .= "<tr><td class='tabletext'><input type='radio' name=\"Settings[ResultDisplayType]\" id='DisplayType1' value='0' ".($ResultDisplayType==0?"CHECKED='CHECKED'":'')." /><label for='DisplayType1'>".$eReportCard['ResultDisplayType1']."</label></td></tr>";
		$ResultDisplayTable .= "<tr><td class='tabletext'><input type='radio' name=\"Settings[ResultDisplayType]\" id='DisplayType2' value='1' ".($ResultDisplayType==1?"CHECKED='CHECKED'":'')." /><label for='DisplayType2'>".$eReportCard['ResultDisplayType2']."</label></td></tr>";
		$ResultDisplayTable .= "<tr><td class='tabletext'><input type='radio' name=\"Settings[ResultDisplayType]\" id='DisplayType3' value='2' ".($ResultDisplayType==2?"CHECKED='CHECKED'":'')." /><label for='DisplayType3'>".$eReportCard['ResultDisplayType3']."</label></td></tr>";
		$ResultDisplayTable .= "</table>";
		

		# Status Table
		$StatusTable = "<table width='50%' border='0' cellpadding='4' cellspacing='0'><tr>";
		$StatusTable .= "<td class='tabletext'><input type='radio' name='RecordStatus' value='1' id='status_private' ".($RecordStatus==1?"CHECKED='CHECKED'":"")." /><label for='status_private'>".$eReportCard['Public']."</a></td>";
		$StatusTable .= "<td class='tabletext'><input type='radio' name='RecordStatus' value='0' id='status_public' ".($RecordStatus==0?"CHECKED='CHECKED'":"")." /><label for='status_public'>".$eReportCard['Private']."</a></td>";
		$StatusTable .= "</tr></table>";
		
		# Full Mark Table
		$FullMarkTable = "<table width='50%' border='0' cellpadding='4' cellspacing='0'><tr>";
		$FullMarkTable .= "<td class='tabletext'><input type='radio' name='ShowFullMark' value='1' id='show_yes' ".($ShowFullMark==1?"CHECKED='CHECKED'":"")." /><label for='show_yes'>".$i_general_yes."</label></td>";
		$FullMarkTable .= "<td class='tabletext'><input type='radio' name='ShowFullMark' value='0' id='show_no' ".($ShowFullMark==0?"CHECKED='CHECKED'":"")." /><label for='show_no'>".$i_general_no."</label></td>";
		$FullMarkTable .= "</tr></table>";

		# Column Percentage Table
		$ShowColumnPercentage = $SettingResultArr["ShowColumnPercentage"];
		$ColumnPercentageTable = "<table width='50%' border='0' cellpadding='4' cellspacing='0'><tr>";
		$ColumnPercentageTable .= "<td class='tabletext'><input type='radio' name='Settings[ShowColumnPercentage]' value='1' id='show_percentage_yes' ".($ShowColumnPercentage==1?"CHECKED='CHECKED'":"")." /><label for='show_percentage_yes'>".$i_general_yes."</label></td>";
		$ColumnPercentageTable .= "<td class='tabletext'><input type='radio' name='Settings[ShowColumnPercentage]' value='0' id='show_percentage_no' ".($ShowColumnPercentage==0?"CHECKED='CHECKED'":"")." /><label for='show_percentage_no'>".$i_general_no."</label></td>";
		$ColumnPercentageTable .= "</tr></table>";
		
		# Build Insert Seletion
		$InsertSelect = $lreportcard->GET_REPORT_COLUMN_INSERT_SELECTION($ReportID, $ReportColumnID);
		
		# Hide Overall Result Table
		$HideOverallResult = $SettingResultArr["HideOverallResult"];
		$HideOverallResultTable = "<table width='50%' border='0' cellpadding='4' cellspacing='0'><tr>";
		$HideOverallResultTable .= "<td class='tabletext'><input type='radio' name='Settings[HideOverallResult]' value='1' id='hide_overall_yes' onFocus=\"displayTable('OverallPositionRow', 'none')\" ".($HideOverallResult==1?"CHECKED='CHECKED'":"")." /><label for='hide_overall_yes'>".$i_general_yes."</label></td>";
		$HideOverallResultTable .= "<td class='tabletext'><input type='radio' name='Settings[HideOverallResult]' value='0' id='hide_overall_no' onFocus=\"displayTable('OverallPositionRow', 'block')\" ".($HideOverallResult==0?"CHECKED='CHECKED'":"")." /><label for='hide_overall_no'>".$i_general_no."</label></td>";
		$HideOverallResultTable .= "</tr></table>";

		$PositionRowStyle = ($HideOverallResult==1) ? "style='display: none'" : "style='display: block'";

		# Build Position Settings Table
		if($PositionRange!="")
		{
			$TmpArr = explode(",", $PositionRange);
			$PositionFrom = $TmpArr[0];
			$PositionTo = $TmpArr[1];
		}
		
		$RangeTableStyle = ($ShowPosition==1 || $ShowPosition==2) ? "style='display: block'" : "style='display: none'";
		$RangeTable = "<table id='RangeTable' $RangeTableStyle><tr>
							<td colspan=3 nowrap='nowrap' class='tabletext'>".$i_From."&nbsp;<input type='text' name='PositionFrom' class='textboxnum' value='".$PositionFrom."'/>&nbsp;".$i_To."&nbsp;<input type='text' name='PositionTo' class='textboxnum' value='".$PositionTo."'/></td>
						</tr></table>";

		$PositionTable = "<table>";
		$PositionTable .= "<tr>
							<td class='tabletext'><input type='radio' name='ShowPosition' id='HidePosition' value='0' onClick='displayTable(\"RangeTable\", \"none\")' ".($ShowPosition==0?"CHECKED='CHECKED'":"")." /><label for='HidePosition'>".$eReportCard['HidePosition']."</label></td>
						</tr>
						<tr>
							<td class='tabletext'><input type='radio' name='ShowPosition' id='ShowClassPosition' value='1' onClick='displayTable(\"RangeTable\", \"block\")' ".($ShowPosition==1?"CHECKED='CHECKED'":"")." /><label for='ShowClassPosition'>".$eReportCard['ShowClassPosition']."</label></td>
						</tr>
						<tr>
							<td class='tabletext'><input type='radio' name='ShowPosition' id='ShowFormPosition' value='2' onClick='displayTable(\"RangeTable\", \"block\")' ".($ShowPosition==2?"CHECKED='CHECKED'":"")." /><label for='ShowFormPosition'>".$eReportCard['ShowFormPosition']."</label></td>
						</tr>";
		$PositionTable .= "</table>".$RangeTable;

		# Hide Not Enrolled Subject Table
		$NotEnrolledTable = "<table width='50%' border='0' cellpadding='4' cellspacing='0'><tr>";
		$NotEnrolledTable .= "<td class='tabletext'><input type='radio' name='HideNotEnrolled' value='1' id='hide_yes' ".($HideNotEnrolled==1?"CHECKED='CHECKED'":"")." /><label for='hide_yes'>".$i_general_yes."</label></td>";
		$NotEnrolledTable .= "<td class='tabletext'><input type='radio' name='HideNotEnrolled' value='0' id='hide_no' ".($HideNotEnrolled==0?"CHECKED='CHECKED'":"")." /><label for='hide_no'>".$i_general_no."</label></td>";
		$NotEnrolledTable .= "</tr></table>";
		
		# Mark Type Table
		$MarkTypeDisplay = $SettingResultArr["MarkTypeDisplay"];
		$MarkTypeDisplayTable = "<table width='100%' border='0' cellpadding='4' cellspacing='0'>";
		$MarkTypeDisplayTable .= "<tr>
							<td class='tabletext'><input type='radio' name=\"Settings[MarkTypeDisplay]\" id='MarkTypeDisplay0' value='0' ".($MarkTypeDisplay==0?"CHECKED='CHECKED'":'')." /><label for='MarkTypeDisplay0'>".$eReportCard['WeightedMarks']."</label></td>
						</tr>
						<tr>
							<td class='tabletext'><input type='radio' name=\"Settings[MarkTypeDisplay]\" id='MarkTypeDisplay1' value='1' ".($MarkTypeDisplay==1?"CHECKED='CHECKED'":'')." /><label for='MarkTypeDisplay1'>".$eReportCard['RawMarks']."</label></td>
						</tr>
						<tr>
							<td class='tabletext'><input type='radio' name=\"Settings[MarkTypeDisplay]\" id='MarkTypeDisplay2' value='2' ".($MarkTypeDisplay==2?"CHECKED='CHECKED'":'')." /><label for='MarkTypeDisplay2'>".$eReportCard['ConvertedGrade']."</label></td>
						</tr>";
		$MarkTypeDisplayTable .= "</table>";
		
		### Academic Year Selection
		$AcademicYearSelection = getSelectAcademicYear('AcademicYearID', $tag='onchange="js_Changed_Academic_Year_Selection(this.value);"', $noFirst=1, $noPastYear=0, $AcademicYear);

		$jsYearTermID = ($ReportType == 'F')? 0 : $ReportType;
		
################################################################
# tag information
$TAGS_OBJ[] = array($eReportCard['ReportBuilder_Template'], "", 0);

$STEPS_OBJ[] = array($eReportCard['SetBasicInfo'], 1);
$STEPS_OBJ[] = array($eReportCard['SetTableDetails'], 0);

$linterface->LAYOUT_START();

?>
<script language="javascript">
$(document).ready(function () {
	js_Reload_Term_Selection($('select#AcademicYearID').val(), '<?=$jsYearTermID?>');
});

function js_Changed_Academic_Year_Selection(jsAcademicYearID)
{
	js_Reload_Term_Selection(jsAcademicYearID);
}

function js_Reload_Term_Selection(jsAcademicYearID, jsYearTermID)
{
	$('div#TermSelectionDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"../ajax_reload.php", 
		{ 
			Action: 'Term_Selection',
			SelectionID: 'YearTermID',
			AcademicYearID: jsAcademicYearID,
			YearTermID: jsYearTermID,
			NoFirst: 1,
			WithWholeYear: 1
		},
		function(ReturnData)
		{
			
		}
	);
}

function checkform(obj){
	if(obj.ReportTitle1.value=="" && obj.ReportTitle2.value=="")
	{
		alert("<?php echo $i_alert_pleasefillin.$i_general_title; ?>.");
		obj.ReportTitle1.focus();
		return false;
	}
	
	if (!check_positive_nonzero_int(obj.LineHeight, "<?=$eReportCard['JSLineHeight']?>")) 
	{
		return false;
	}
	<? if ($ReportCardTemplate == 2) { ?>
	if (!check_positive_nonzero_int(obj.SignatureWidth, "<?=$eReportCard['JSSignatureWidth']?>")) 
	{
		return false;
	}
	<? } ?>
	
	if(countChecked(obj, "Form[]")==0)
	{
		alert("<?=$i_alert_pleaseselect.$eReportCard['Form']?>");
		return false;
	}

	for (var i = 0; i < obj.ShowPosition.length; i++) {
		if(obj.ShowPosition[i].checked)
		{
			if ((obj.ShowPosition[i].value==1 || obj.ShowPosition[i].value==2) && (obj.PositionFrom.value=='' || obj.PositionTo.value==''))
			{
				alert("<?=$i_alert_pleasefillin.$eReportCard['PositionRange'];?>");
				obj.PositionFrom.focus();
				return false;
			}
			break;
		}
	}

	return true;
}
</script>


<form name="form1" action="edit_update.php" method="post" onSubmit="return checkform(this);">
<br />
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?= $linterface->GET_STEPS($STEPS_OBJ) ?>
	</td>
</tr>
<tr>
	<td>
		<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tr><td valign="top" class="tabletext"><?= $i_general_title; ?> <span class="tabletextrequire">*</span></td><td width="70%"><input class="textboxtext" type="text" name="ReportTitle1" maxlength="255" value="<?=$ReportTitle1?>" /></td></tr>
		<tr><td class="formfieldtitle tabletext">&nbsp;</td><td width="70%"><input class="textboxtext" type="text" name="ReportTitle2" maxlength="255" value="<?=$ReportTitle2?>" /></td></tr>		
		<tr><td valign="top" class="formfieldtitle tabletext"><?= $eReportCard['AcademicYear'] ?></td><td width="70%"><?=$AcademicYearSelection?></td></tr>		
		
		<tr>
			<td valign="top" class="formfieldtitle tabletext"><?= $eReportCard['ReportType']; ?> <span class="tabletextrequire">*</span></td>
			<td width="70%" valign="top"><div id="TermSelectionDiv"></div></td>
		</tr>
		<tr><td valign="top" class="formfieldtitle tabletext"><?= $i_general_description; ?></td><td width="70%">
		<?= $linterface->GET_TEXTAREA("Description", $Description, 40) ?>
		</td></tr>
		<tr><td valign="top" class="formfieldtitle tabletext"><?= $eReportCard['Header'] ?></td><td width="70%" class="tabletext">
		<input type="radio" name="NoHeader" id="NotNoHeader" value="-1" <? if ($NoHeader == -1) print "checked"?> > <label for="NotNoHeader"><?= $eReportCard['NotNoHeader']?></label><br>
		<input type="radio" name="NoHeader" id="IsNoHeader" value="1" <? if ($NoHeader > -1) print "checked"?>> <label for="IsNoHeader"><?= $eReportCard['IsNoHeader']?>
		<?= $eReportCard['NoHeaderBr']?></label><input type="text" class="textboxnum" id="NoHeaderBr" name="NoHeaderBr" value="<? ($NoHeader == -1) ? print 0 : print $NoHeader; ?>">
		</td></tr>
		<tr><td valign="top" class="formfieldtitle tabletext"><?= $eReportCard['Footer']; ?></td><td width="70%">
		<?= $linterface->GET_TEXTAREA("Footer", $Footer, 40) ?>
		</td></tr>
		
		<? if ($ReportCardTemplate == 6) { ?>
		<tr>
			<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $eReportCard['SchoolReopenDay']; ?> </td>
			<td width="70%" class="tabletext"><input class="textboxnum" type="text" name="Settings[SchoolReopenDay]" maxlength="10" value="<?= $SettingResultArr['SchoolReopenDay'] ?>" /> <span class='tabletextremark'>(YYYY-MM-DD)</span></td>
		</tr>
		<? } ?>
		
		<tr><td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $eReportCard['LineHeight']; ?> <span class="tabletextrequire">*</span></td><td width="70%" class="tabletext"><input class="textboxnum" type="text" name="LineHeight" maxlength="2" value="<?=$LineHeight?>" /> px</td></tr>
		<? if ($ReportCardTemplate == 2) { ?>
		<tr><td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $eReportCard['SignatureWidth']; ?> <span class="tabletextrequire">*</span></td><td width="70%" class="tabletext"><input class="textboxnum" type="text" name="SignatureWidth" maxlength="3" value="<?=$SignatureWidth?>" /> px</td></tr>
		<? } ?>
		<tr><td valign="top" class="formfieldtitle tabletext"><?= $eReportCard['Form']; ?><span class='tabletextrequire'>*</span></td><td width="70%"><?=$FormTable?></td></tr>
		<tr><td valign="top" class="formfieldtitle tabletext"><?= $i_general_status; ?> <span class="tabletextrequire">*</span></td><td width="70%"><?=$StatusTable?></td></tr>
		<tr><td valign="top" class="formfieldtitle tabletext"><?=$eReportCard['ShowFullMark']?> <span class="tabletextrequire">*</span></td><td width="70%"><?=$FullMarkTable?></td></tr>
		<tr><td valign="top" class="formfieldtitle tabletext"><?=$eReportCard['ShowColumnPercentage']?> <span class="tabletextrequire">*</span></td><td width="70%"><?=$ColumnPercentageTable?></td></tr>
		<tr><td valign="top" class="formfieldtitle tabletext"><?=$eReportCard['HideNotEnrolledSubject']?> <span class="tabletextrequire">*</span></td><td width="70%"><?=$NotEnrolledTable?></td></tr>
		<tr><td valign="top" class="formfieldtitle tabletext"><?= $i_Profile_settings_display; ?></td><td  width="70%"><?=$SettingsTable?></td></tr>
		<tr><td valign="top" class="formfieldtitle tabletext"><?= $eReportCard['MarkTypeDisplay'] ?> <span class="tabletextrequire">*</span></td><td width="70%"><?=$MarkTypeDisplayTable?></td></tr>

		<!-- Decimal Point -->
		<tr><td valign="top" class="formfieldtitle tabletext"><?= $eReportCard['DecimalPoint'] ?> <span class="tabletextrequire">*</span></td><td width="70%"><input type='text' name='DecimalPoint' class='textboxnum' value='<?= $DecimalPoint?>'/></td></tr>		
		
		<tr><td valign="top" class="formfieldtitle tabletext"><?= $eReportCard['BilingualReportTitle'] ?> <span class="tabletextrequire">*</span></td><td width="70%"><?=$BilingualReportTitleTable?></td></tr>
		<tr id="SubjectTitleDisplayRow" <?=$SubjectTableDisplayStyle?>><td valign="top" class="formfieldtitle tabletext"><?= $eReportCard['SubjectTitleDisplay'] ?> <span class="tabletextrequire">*</span></td><td width="70%"><?=$SubjectTitleTable?></td></tr>

		<tr><td valign="top" class="formfieldtitle tabletext"><?= $eReportCard['RankingType'] ?> <span class="tabletextrequire">*</span></td><td width="70%"><?=$RankingTypeTable?></td></tr>
		<tr>
			<td valign="top" class="formfieldtitle tabletext"><?=$eReportCard['HideOverallResult']?> <span class="tabletextrequire">*</span></td>
			<td width="70%"><?=$HideOverallResultTable?></td>
		</tr>
		<tr id="OverallPositionRow" <?=$PositionRowStyle?>>
			<td valign="top" class="formfieldtitle tabletext"><?=$eReportCard['ShowOverallPosition']?> <span class="tabletextrequire">*</span></td>
			<td width="70%"><?=$PositionTable?></td>
		</tr>
		<tr><td colspan=2 class="tablesubtitle2"><?=$eReportCard['ComponentSubjectSettingMsg']?></td></tr>
		<!--
		<tr><td valign="top" class="formfieldtitle tabletext"><?= $eReportCard['ResultCalType'] ?></td><td width="70%"><?=$ParentSubjectCalTable?></td></tr>
		-->
		<tr><td valign="top" class="formfieldtitle tabletext"><?= $eReportCard['ResultDisplayType'] ?></td><td width="70%"><?=$ResultDisplayTable?></td></tr>
		</table>
		<?= "<input type='hidden' name=\"Settings[ResultCalculationType]\" id='ResultCalType1' value='0' />" //raw mark ?>
	</td>
</tr>
<tr>
	<td><table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tr><td align="left" class="tabletextremark">&nbsp; &nbsp; &nbsp; <?=$i_general_required_field?></td></tr>
		<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
		<tr>
			<td align="center">
			<?= $linterface->GET_ACTION_BTN($button_continue, "submit", "")?>&nbsp;
			<?= $linterface->GET_ACTION_BTN($button_reset, "reset")?>
			<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:history.back()")?>
			</td>
		</tr>
	</table>
	</td>
</tr>
</table>

<input type="hidden" name="ReportID" value="<?=$ReportID?>" />
<p></p>
</form>
<?= $linterface->FOCUS_ON_LOAD("form1.ReportTitle1") ?>

<?

        $linterface->LAYOUT_STOP();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>
