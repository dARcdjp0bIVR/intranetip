<?php
$PATH_WRT_ROOT = "../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libreportcard.php");
intranet_auth();
intranet_opendb();

$li = new libdb();
$lreportcard = new libreportcard();

############################ FUNCTION ##################################

function REMOVE_REPORT_SUBJECT($ParTargetList)
{
	global $li;

	$sql = "DELETE FROM RC_REPORT_TEMPLATE_SUBJECT WHERE ReportSubjectID IN ($ParTargetList)";
	$success = $li->db_db_query($sql);
	
	if($success==1)
	{
		$sql = "DELETE FROM RC_REPORT_TEMPLATE_CELL WHERE ReportSubjectID IN ($ParTargetList)";
		$success = $li->db_db_query($sql);
	}

	return $success;
}

########################################################################

# Get SubjectID List
if(!empty($ReportSubjectID))
{
	for($i=0; $i<sizeof($ReportSubjectID); $i++)
	{
		$rs_id = $ReportSubjectID[$i];
		$sql = "SELECT SubjectID FROM RC_REPORT_TEMPLATE_SUBJECT WHERE ReportSubjectID = '$rs_id'";
		$row = $li->returnVector($sql);
		$SubjectID = $row[0];
		
		# Get Component Subject(s) if any
		$RelatedSubjectArray = $lreportcard->GET_RELATED_SUBJECT($SubjectID);
		if(sizeof($RelatedSubjectArray)>1)
		{
			if($RelatedSubjectArray[0]!=$SubjectID)
			{
				$RelatedSubjectArray = array($SubjectID);
			}
			$RelatedList = implode(",", $RelatedSubjectArray);

			# Get related ReportSubjectID(s)
			$sql = "SELECT ReportSubjectID FROM RC_REPORT_TEMPLATE_SUBJECT WHERE ReportID = '$ReportID' AND SubjectID IN ($RelatedList)";
			$row = $li->returnVector($sql);
			$TargetList = implode(",", $row);
		}
		else
		{
			$TargetList = $rs_id;
		}
		# Remove record(s);
		$success = REMOVE_REPORT_SUBJECT($TargetList);
	}
}

$Result = ($success) ? "delete" : "delete_failed";
intranet_closedb();
header("Location:subject_manage.php?ReportID=$ReportID&Result=$Result");
?>

