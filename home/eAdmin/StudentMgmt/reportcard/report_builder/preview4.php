<?php
$PATH_WRT_ROOT = "../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

############################ Functions Start #############################

/*
* Generate signature table
*/
function GENERATE_SIGNATURE_TABLE()
{
	global $SignatureTitleArray, $SettingSignatureArray, $LangArray;

	$SignatureTable = "<table width='100%' border='0' cellpadding='4' cellspacing='0' align='center' class='report_border'>";
	$SignatureTable .= "<tr>";
	for($k=0; $k<sizeof($SignatureTitleArray); $k++)
	{
		$SettingID = trim($SignatureTitleArray[$k]);
		if($SettingSignatureArray[$SettingID]==1)
		{
			$Title = $LangArray[$SettingID];
			$SignatureTable .= "<td valign='bottom' align='center'>";
			$SignatureTable .= "<table cellspacing='0' cellpadding='0' border='0'>";
			$SignatureTable .= "<tr><td align='center' class='small_title' height='60' valign='bottom'>____________________</td></tr>";
			$SignatureTable .= "<tr><td align='center' class='small_title' valign='bottom'>".$Title."</td></tr>";
			$SignatureTable .= "</table>";
			$SignatureTable .= "</td>";
		}
	}

		$SignatureTable .= "</tr>";
		$SignatureTable .= "</table>";

	return "<tr><td>".$SignatureTable."</td></tr>";
}

/*
* Generate attendance table
*/
function GENERATE_ATTENDANCE_AND_MERIT_TABLE()
{
	global $SummaryTitleArray, $SettingSummaryArray, $SettingMiscArray, $ColumnArray, $LangArray, $SemesterArray;
	global $DaysAbsentTitle, $TimesLateTitle, $AbsentWOLeaveTitle;

	$AttendanceTableShow = ($SettingSummaryArray["DaysAbsent"]==1 || $SettingSummaryArray["TimesLate"] || $SettingSummaryArray["AbsentWOLeave"]);
	$MeritTableShow = ($SettingMiscArray["MeritsAndDemerits"]==1);

	$SemColumnCount = 0;
	for($i=0; $i<sizeof($ColumnArray); $i++)
	{
		$sem_title = trim($ColumnArray[$i]["ColumnTitle"]);
		if ($sem_title!="")
		{
			if(in_array($sem_title, $SemesterArray))
			{
				$SemesterHeaderRow .= ($AttendanceTableShow==1 || $MeritTableShow==1) ? "<td class='reportcard_text border_top border_left' align='center'>".$sem_title."</td>\n" : "";

				$DaysAbsentCell .= "<td class='reportcard_text border_top border_left' align='center'>#</td>\n";
				$LateCell .= "<td class='reportcard_text border_top border_left' align='center'>#</td>\n";
				$AbsentWOLeaveCell .= "<td class='reportcard_text border_top border_left' align='center'>#</td>\n";
				$MeritCell .= "<td class='reportcard_text border_top border_left' align='center'>#</td>\n";
				$DemeritCell .= "<td class='reportcard_text border_top border_left' align='center'>#</td>\n";

				$SemColumnCount++;
			}
		}
	}

	if($SemColumnCount>0 && ($AttendanceTableShow==1 || $MeritTableShow==1))
	{
		$ReturnTable = "<table width='100%' valign='bottom' border='0' cellspacing='0' cellpadding='2'>\n";
		if($AttendanceTableShow==1)
		{
			$ReturnTable .= "<tr><td class='reportcard_text border_top'>".$LangArray['Attendance']."</td>\n";
			$ReturnTable .= $SemesterHeaderRow;
		}

		# Rows of Attendance records
		if($SettingSummaryArray["DaysAbsent"]==1)
		{
			$ReturnTable .= "<tr><td class='reportcard_text border_top'>".$DaysAbsentTitle."</td>\n";
			$ReturnTable .= $DaysAbsentCell;
			$ReturnTable .= "</tr>";
		}
		if($SettingSummaryArray["TimesLate"]==1)
		{
			$ReturnTable .= "<tr><td class='reportcard_text border_top'>".$TimesLateTitle."</td>\n";
			$ReturnTable .= $LateCell;
			$ReturnTable .= "</tr>";
		}
		if($SettingSummaryArray["AbsentWOLeave"]==1)
		{
			$ReturnTable .= "<tr><td class='reportcard_text border_top'>".$AbsentWOLeaveTitle."</td>";
			$ReturnTable .= $AbsentWOLeaveCell;
			$ReturnTable .= "</tr>";
		}
		# Row of Merit records
		if($MeritTableShow==1)
		{
			if($AttendanceTableShow==1) {
				$ReturnTable .= "<tr><td class='reportcard_text border_top' colspan='".($SemColumnCount+1)."'>".$LangArray['MeritsAndDemerits']."</td></tr>\n";
			}
			else {
				$ReturnTable .= "<tr><td class='reportcard_text border_top'>".$LangArray['MeritsAndDemerits']."</td>\n";
				$ReturnTable .= $SemesterHeaderRow;
			}

			$ReturnTable .= "<tr><td class='reportcard_text border_top'>".$LangArray['Merit']."</td>\n";
			$ReturnTable .= $MeritCell;
			$ReturnTable .= "</tr>";
			$ReturnTable .= "<tr><td class='reportcard_text border_top'>".$LangArray['Demerit']."</td>\n";
			$ReturnTable .= $DemeritCell;
			$ReturnTable .= "</tr>";
		}
		$ReturnTable .= "</table>";
	}

	return $ReturnTable;
}


/*
* Get summary array
*/
function GET_SUMMARY_ARRAY()
{
	global $SummaryTitleArray, $SettingSummaryArray, $ShowFullMark, $Semester, $ColumnArray, $LangArray, $HideOverallResult;

	$ReturnArray = "";

	if(!empty($SettingSummaryArray))
	{
		$count = 0;
		for($k=0; $k<sizeof($SummaryTitleArray); $k++)
		{
			$SettingID = trim($SummaryTitleArray[$k]);
			//if($SettingSummaryArray[$SettingID]==1 && $SettingID!="DaysAbsent" && $SettingID!="TimesLate" && $SettingID!="AbsentWOLeave")
			if($SettingSummaryArray[$SettingID]==1)
			{
				$Title = $LangArray[$SettingID];
				$ReturnArray[$count]["Title"] = $Title;
				switch ($SettingID) {
					case "GrandTotal":
					case "GPA":
					case "AverageMark":
					case "ClassHighestAverage":
						$Display = "S";
						break;
					case "ClassPupilNumber":
					case "ClassPosition":
					case "FormPosition":
					
					//case "Attendance":
					case "DaysAbsent":
					case "TimesLate":
					case "AbsentWOLeave":
					//case "MeritsAndDemerits":
					case "Merit":
					case "MinorCredit":
					case "MajorCredit":
					case "Demerit":
					case "MinorFault":
					case "MajorFault":
						$Display = "#";
						break;
					case "ECA":
					case "Conduct":
					case "Politeness":
					case "Behaviour":
					case "Application":
					case "Tidiness":
						$Display = "G";
						break;
				}
				
				for($i=0; $i<sizeof($ColumnArray); $i++)
				{
					$column = trim($ColumnArray[$i]["ColumnTitle"]);
					$ReturnArray[$count][$column] = $Display;
				}
				if(!($Semester=="FULL" && $HideOverallResult==1))
					$ReturnArray[$count][$Semester] = $Display;

				$count++;
			}
		}
	}
	return $ReturnArray;
}


/*
* Get summary array
*/
function GET_MERIT_ARRAY()
{
	global $MeritTitleArray, $MeritSummaryArray, $ShowFullMark, $Semester, $ColumnArray, $LangArray, $HideOverallResult;

	$ReturnArray = "";

	if(!empty($MeritSummaryArray))
	{
		$count = 0;
		for($k=0; $k<sizeof($MeritTitleArray); $k++)
		{
			$SettingID = trim($MeritTitleArray[$k]);
			//if($SettingSummaryArray[$SettingID]==1 && $SettingID!="DaysAbsent" && $SettingID!="TimesLate" && $SettingID!="AbsentWOLeave")
			if($MeritSummaryArray[$SettingID]==1)
			{
				$Title = $LangArray[$SettingID];
				$ReturnArray[$count]["Title"] = $Title;
				switch ($SettingID) {
					case "GrandTotal":
					case "GPA":
					case "AverageMark":
					case "ClassHighestAverage":
						$Display = "S";
						break;
					case "ClassPupilNumber":
					case "ClassPosition":
					case "FormPosition":
					
					//case "Attendance":
					case "DaysAbsent":
					case "TimesLate":
					case "AbsentWOLeave":
					//case "MeritsAndDemerits":
					case "Merit":
					case "MinorCredit":
					case "MajorCredit":
					case "Demerit":
					case "MinorFault":
					case "MajorFault":
						$Display = "#";
						break;
					case "ECA":
					case "Conduct":
					case "Politeness":
					case "Behaviour":
					case "Application":
					case "Tidiness":
						$Display = "G";
						break;
				}
				
				for($i=0; $i<sizeof($ColumnArray); $i++)
				{
					$column = trim($ColumnArray[$i]["ColumnTitle"]);
					$ReturnArray[$count][$column] = $Display;
				}
				if(!($Semester=="FULL" && $HideOverallResult==1))
					$ReturnArray[$count][$Semester] = $Display;

				$count++;
			}
		}
	}
	return $ReturnArray;
}


/*
* generate MISC Table
*/
function GENERATE_MISC_TABLE()
{
	global $SettingMiscArray, $MiscTitleArray, $Semester, $LangArray;

	if(!empty($SettingMiscArray))
	{
		$IsFirst = 1;
		$MiscTable = "<table width='100%' border='0' cellpadding='2' cellspacing='0' align='center' class='report_border'>";
		for($k=0; $k<sizeof($MiscTitleArray); $k++)
		{
			$SettingID = trim($MiscTitleArray[$k]);
			if($SettingMiscArray[$SettingID]==1 && $SettingID!="SubjectTeacherComment" &&
				$SettingID!="MeritsAndDemerits" && $SettingID!="MinorCredit" && 
				$SettingID!="MajorCredit" && $SettingID!="MinorFault" && $SettingID!="MajorFault"
				 && $SettingID!="ECA"
				)
			{
				$Title = $LangArray[$SettingID];
				$Display = "";
				$MiscTable .= "<tr>";
				if(($IsFirst==1)) {
					$IsFirst = 0;
				}
				else {
					$top_border_style = "class='border_top'";
				}
				$MiscTable .= "<td width='100%' {$top_border_style}>";
				$MiscTable .= "<span class='small_title'>".$Title."</span>";
				$MiscTable .= "<table width='100%' border='0' cellpadding='2' cellspacing='0' align='center'>
								<tr><td class='small_text' valign='top' height='60'>&nbsp;</td></tr>
								</table>";
				$MiscTable .= "</td>";
				$MiscTable .= "</tr>";
			}
		}
		$MiscTable .= "</table>";
	}

	return $MiscTable;
}

############################ Functions End ################################
if ($plugin['ReportCard'])
{
	include_once($PATH_WRT_ROOT."includes/libreportcard.php");

	$libreportcard = new libreportcard();
	$CurrentPage = "ReportGeneration";
	$MODULE_OBJ = $libreportcard->GET_MODULE_OBJ_ARR();
	$MODULE_OBJ['title'] = $button_preview;

	if ($libreportcard->hasAccessRight())
    {
		$linterface = new interface_html("popup.html");
#######################################################################################################
		$SelectedFormArr = $libreportcard->GET_REPORT_TEMPLATE_FORM($ReportID);
		$SubjectGradingArray = $libreportcard->GET_REPORT_SUBJECT_GRADING($ReportID, $SelectedFormArr);
		$SubjectGradingIDArray = (is_array($SubjectGradingArray)) ? array_values($SubjectGradingArray) : "";

		# get current Year and Semester
		$Year = trim($libreportcard->Year);
		$Semester = trim($libreportcard->Semester);

		# Get Semester Array
		$SemesterArray = $libreportcard->GET_SEMESTER_ARRAY();

		# Get Column Array of the report
		$ColumnArray = $libreportcard->GET_REPORT_TEMPLATE_COLUMN($ReportID);

		if(is_array($SubjectGradingArray))
		{
			$SubjectGradingList = implode(",", $SubjectGradingArray);
			list($FailedArray, $DistinctionArray, $PFArray) = $libreportcard->GET_REPORT_GRADEMARK($SubjectGradingList);
		}

		$SubjectArr = $libreportcard->GET_REPORT_TEMPLATE_SUBJECT($ReportID, $SubjectGradingIDArray);
		$ColumnArr = $libreportcard->GET_REPORT_TEMPLATE_COLUMN($ReportID);

		$ColumnSize = sizeof($ColumnArr);
		$ReportCellSetting = $libreportcard->GET_REPORT_CELL($ReportID);

		# Get All subject array
		$AllSubjectArray = $libreportcard->GET_ALL_SUBJECTS();

		# Get Report Info
		$ReportInfo = $libreportcard->GET_REPORT_TEMPLATE($ReportID);
		list($ReportTitle, $ReportType, $Description, $ShowFullMark, $Settings, $ReportStatus, $ShowOverallPosition, $OverallPositionRange, $HideNotEnrolled, $Footer, $NoHeader) = $ReportInfo;

		$SettingArray = unserialize($Settings);
		$ResultDisplayType = $SettingArray["ResultDisplayType"];
		$ShowColumnPercentage = $SettingArray["ShowColumnPercentage"];
		$MarkTypeDisplay = $SettingArray["MarkTypeDisplay"];
		$BilingualReportTitle = $SettingArray["BilingualReportTitle"];
		$HideOverallResult = $SettingArray["HideOverallResult"];

		# define the name of the variable array by checking the bilingual setting
		if($BilingualReportTitle==1)
		{
			$LangArray = $eReportCardBilingual;

			# prepare titles
			$DaysAbsentTitle = $LangArray["DaysAbsent"];
			$pos = strpos($DaysAbsentTitle, " ");
			$DaysAbsentTitle = substr_replace($DaysAbsentTitle, "<br />", $pos, 1);

			$TimesLateTitle = $LangArray["TimesLate"];
			$pos = strpos($TimesLateTitle, " ");
			$TimesLateTitle = substr_replace($TimesLateTitle, "<br />", $pos, 1);

			$AbsentWOLeaveTitle = $LangArray["AbsentWOLeave"];
			$pos = strpos($AbsentWOLeaveTitle, " ");
			$AbsentWOLeaveTitle = substr_replace($AbsentWOLeaveTitle, "<br />", $pos, 1);
		}
		else {
			$LangArray = $eReportCard;

			# prepare titles
			$DaysAbsentTitle = $LangArray["DaysAbsent"];
			$TimesLateTitle = $LangArray["TimesLate"];
			$AbsentWOLeaveTitle = $LangArray["AbsentWOLeave"];
		}

		# get school badge
		$SchoolLogo = GET_SCHOOL_BADGE();

		# get school name
		$SchoolName = GET_SCHOOL_NAME();

		$TempLogo = ($SchoolLogo=="") ? "&nbsp;" : $SchoolLogo;
		if ($NoHeader != -1) $TempLogo = "&nbsp;";

		$TitleTable = "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>\n";
		$TitleTable .= "<tr><td width='120' align='center'>".$TempLogo."</td>";
		if(!empty($ReportTitle) || !empty($SchoolName))
		{
			list($ReportTitle1, $ReportTitle2) = explode("::", $ReportTitle);
			$TitleTable .= "<td>";
			if ($NoHeader == -1) {
				$TitleTable .= "<table width='100%' border='0' cellpadding='2' cellspacing='0' align='center'>\n";
				if(!empty($SchoolName))
					$TitleTable .= "<tr><td nowrap='nowrap' class='report_title' align='center'>".$SchoolName."</td></tr>\n";
				if(!empty($ReportTitle1))
					$TitleTable .= "<tr><td nowrap='nowrap' class='report_title' align='center'>".$ReportTitle1."</td></tr>\n";
				if(!empty($ReportTitle2))
					$TitleTable .= "<tr><td nowrap='nowrap' class='report_title' align='center' colspan='2'>".$ReportTitle2."</td></tr>\n";
				$TitleTable .= "</table>\n";
			} else {
				for ($i = 0; $i < $NoHeader; $i++) {
					$TitleTable .= "<br/>";
				}
			}
			$TitleTable .= "</td>";
		}
		$TitleTable .= "<td width='120' align='center'>&nbsp;</td></tr>";
		$TitleTable .= "</table>";

		################################################################
		# Generate Student Info Table
		$StudentTitleArray = $eReportCard['DisplaySettingsArray']["StudentInfo"];
		$SettingStudentInfo = $SettingArray["StudentInfo"];
		if(!empty($SettingStudentInfo))
		{
			$count = 0;
			
			### reconstruct array
			for($i=0; $i<sizeof($StudentTitleArray); $i++) {
				$SettingID = trim($StudentTitleArray[$i]);
				if($SettingID!="StudentNo") {
					$TempStudentTitleArray[] = $StudentTitleArray[$i];
				} else {
					$Tempi = $i;
				}
			}
			$TempStudentTitleArray[] = $StudentTitleArray[$Tempi];
			$StudentTitleArray = $TempStudentTitleArray;
			### reconstruct array
			
			$StudentInfoTable = "<table width='100%' border='0' cellpadding='2' cellspacing='0' align='center'><tr>";			

			for($i=0; $i<sizeof($StudentTitleArray); $i++)
			{
				$SettingID = trim($StudentTitleArray[$i]);
				if($SettingStudentInfo[$SettingID]==1 && $SettingID!="ClassTeacher")
				{
					$Title = $LangArray[$SettingID];
					$Title = str_replace("<br />", " ", $Title);
					if($count%2==0) {
						//$StudentInfoTable .= "<tr>";
					}
					$StudentInfoTable .= "<td class='tabletext' width='20%' valign='top'>".$Title." : XXX</td>";
					if (($i+1) != sizeof($StudentTitleArray)) $StudentInfoTable .= "<td width='10'>&nbsp;</td>";
					if($count%2==1) {
						//$StudentInfoTable .= "</tr>";
					}
					$count++;
				}
			}
			$StudentInfoTable .= "</tr></table>";
		}

		$StudentInfoRow = (!empty($StudentInfoTable)) ? "<tr><td>".$StudentInfoTable."</td></tr>" : "";
		################################################################
		# Generate Summary Table
		$SummaryTitleArray = $eReportCard['DisplaySettingsArray']["Summary"];
		$SettingSummaryArray = $SettingArray["Summary"];
		
		################################################################
		# Generate Misc Table
		$MiscTitleArray = $eReportCard['DisplaySettingsArray']["Misc"];
		$SettingMiscArray = $SettingArray["Misc"];
		
		### add merit to display detail (aki)
		$SettingSummaryArray = $SettingArray["Summary"];		
		if ($SettingMiscArray["MeritsAndDemerits"]==1) {
			$MeritSummaryArray['Merit'] = 1;
			$MeritTitleArray[] = "Merit";		
		}
		if ($SettingMiscArray["MinorCredit"]==1) {
			$MeritSummaryArray['MinorCredit'] = 1;		
			$MeritTitleArray[] = "MinorCredit";			
		}
		if ($SettingMiscArray["MajorCredit"]==1) {
			$MeritSummaryArray['MajorCredit'] = 1;		
			$MeritTitleArray[] = "MajorCredit";			
		}
		if ($SettingMiscArray["MinorFault"]==1) {
			$MeritSummaryArray['MinorFault'] = 1;		
			$MeritTitleArray[] = "MinorFault";			
		}
		if ($SettingMiscArray["MajorFault"]==1) {
			$MeritSummaryArray['MajorFault'] = 1;		
			$MeritTitleArray[] = "MajorFault";			
		}
		if ($SettingMiscArray["MeritsAndDemerits"]==1) {
			$MeritSummaryArray['Demerit'] = 1;			
			$MeritTitleArray[] = "Demerit";					
		}
		
		$SummaryResultArray = GET_SUMMARY_ARRAY();
		$MeritResultArray = GET_MERIT_ARRAY();
		
		$ShowSubjectTeacherComment = $SettingMiscArray["SubjectTeacherComment"];
		$MiscTable = GENERATE_MISC_TABLE();
		$MiscTableRow = (!empty($MiscTable)) ? "<tr><td>".$MiscTable."</td></tr>" : "";

		################################################################
		# Generate Signature Table
		$SignatureTitleArray = $eReportCard['DisplaySettingsArray']["Signature"];
		$SettingSignatureArray = $SettingArray["Signature"];
		$SignatureTable = GENERATE_SIGNATURE_TABLE();

		# Generate Attendance and Merit Table
		$AttendanceMeritTable = GENERATE_ATTENDANCE_AND_MERIT_TABLE();

		################################################################
		# Generate Details Table
		$SubjectTitleType = $SettingArray["SubjectTitleType"];
		if($BilingualReportTitle==1 || $SubjectTitleType==0)
		{
			$SubjectTitleCell = "<td width='50%' class='small_title'>".$eReportCard['EngSubject']."</td><td width='50%' class='small_title'>".$eReportCard['ChiSubject']."</td>";
		}
		else
		{
			$SubjectTitleCell = "<td class='small_title'>".$eReportCard['Subject']."</td>";
		}

		$ExtraColumn = 0;
		$DetailsTable = "<table width='100%' border='0' cellspacing='0' cellpadding='0' class='report_border'>";
		$DetailsTable .= "<tr>
							<td width='30%' class='report_formfieldtitle'>
								<table width='100%' border='0' cellpadding='2' cellspacing='0'><tr>".$SubjectTitleCell."</tr></table>
							</td>";
		if($ShowFullMark==1)
		{
			$DetailsTable .= "<td width='5%' class='report_formfieldtitle border_left small_title' align='center' >".$LangArray['FullMark']."</td>";
			$ExtraColumn++;
		}

		for($i=0; $i<sizeof($ColumnArr); $i++)
		{
			list($ReportColumnID, $ColumnTitle, $Weight, $ShowPosition, $PositionRange) = $ColumnArr[$i];
			if($Weight!="" && $ShowColumnPercentage==1)
			{
				$ColumnTitle = $ColumnTitle."&nbsp;".$Weight."%";
			}
			$DetailsTable .= "<td class='border_left report_formfieldtitle' width='15%'><table border=0 cellpadding=0 cellspacing=0 width='100%'>";
			$DetailsTable .= "<tr height='75%'><td align='center' class='small_title' colspan='3'>".$ColumnTitle."</td></tr>";
			if($ShowPosition==1 || $ShowPosition==2)
			{
				$DetailsTable .= "<tr>";
				$DetailsTable .=  "<td align='center' width='50%' class='reportcard_text'>".$LangArray['Mark']."</td>";
				$DetailsTable .= "<td>&nbsp;</td>";
				$DetailsTable .= "<td align='center' width='50%' class='reportcard_text'>(".$LangArray['Position'].")</td>";
				$DetailsTable .= "</tr>";
			}
			$DetailsTable .= "</table></td>";
		}
		if($ColumnSize>1 && $HideOverallResult!=1)
		{
			// width='10%'
			$DetailsTable .= "<td class='border_left report_formfieldtitle' width='15%'><table border=0 cellpadding=0 cellspacing=0 width='100%'>";
			$DetailsTable .= "<tr height='75%'><td align='center' class='small_title' colspan='3'>".$LangArray['OverallResult']."</td></tr>";
			if($ShowOverallPosition==1 || $ShowOverallPosition==2)
			{
				$DetailsTable .= "<tr>";
				$DetailsTable .=  "<td align='center' width='50%' class='reportcard_text'>".$LangArray['Mark']."</td>";
				$DetailsTable .= "<td>&nbsp;</td>";
				$DetailsTable .= "<td align='center' width='50%' class='reportcard_text'>(".$LangArray['Position'].")</td>";
				$DetailsTable .= "</tr>";
			}
			$DetailsTable .= "</table></td>";
			$ExtraColumn++;
		}
		
		/*
		if($ShowSubjectTeacherComment==1) {
			$DetailsTable .= "<td class='report_formfieldtitle border_left small_title' align='center'>".$LangArray['SubjectTeacherComment']."</td>";
		}
		*/
		/*
		if ($SignatureTable!="" || $AttendanceMeritTable!="")
		{
			$DetailsTable .= "<td rowspan='50' class='border_left' width='2'><img src='/images/spacer.gif' width='2' height='1' border='0' /></td>";
			$DetailsTable .= "<td height='100%' rowspan='50' align='right' class='border_left'>";
			$DetailsTable .= "<table border='0' height='100%' width='100%' cellpadding='0' cellspacing='0'>";
			//$DetailsTable .= ($SignatureTable!="") ? "<tr><td valign='top' width='100%' height='100%'>".$SignatureTable."</td></tr>" : "";
			//$DetailsTable .= ($AttendanceMeritTable!="") ? "<tr><td valign='bottom' width='100%' height='100%'>".$AttendanceMeritTable."</td></tr>" : "";
			$DetailsTable .= "</table>";
			$DetailsTable .= "</td>";
		}
		*/
		$DetailsTable .= "</tr>";

		$ColumnSpan = $ColumnSize + $ExtraColumn + 1;
		if(is_array($SubjectArr))
		{
			$count = 0;
			$IsFirst = 1;
			foreach($SubjectArr as $CodeID => $Data)
			{
				if(is_array($Data))
				{
					foreach($Data as $CmpCodeID => $InfoArr)
					{
						$IsComponent = ($CmpCodeID>0) ? 1 : 0;
						$top_style = ($IsComponent==0 && $IsFirst==0) ? "border_top" : "";
						$IsFirst = 0;

						$Prefix = ($IsComponent==1) ? "&nbsp;&nbsp;&nbsp;&nbsp;" : "";
						list($ReportSubjectID, $SubjectID, $EngSubjectName, $ChiSubjectName, $FullMark, $Scale, $HidePosition) = $InfoArr;

						# Highest Grade
						$MaxGrade = $DistinctionArray[$SubjectID]["G"];
						$FirstRowSetting = $ReportCellSetting[$ColumnArr[0][0]][$ReportSubjectID];
						$FullMark = ($FirstRowSetting=="G") ? $MaxGrade : $FullMark;

						# check whether is parent/component subject
						$IsParent = (sizeof($AllSubjectArray[$SubjectID])>1) ? 1 : 0;
						$IsComponent = (empty($AllSubjectArray[$SubjectID])) ? 1 : 0;

						$ResultShow = (!($ResultDisplayType==2 && $IsParent==1)) ? 1 : 0;
						$OverallShow = (!($ResultDisplayType==2 && $IsParent==1) && !($ResultDisplayType==1 && $IsComponent==1)) ? 1 : 0;

						if($BilingualReportTitle==1 || $SubjectTitleType==0)
						{
							$SubjectCell = "<td width='50%' class='small_text'>".$Prefix.$EngSubjectName."</td><td width='50%' class='small_text'>".$Prefix.$ChiSubjectName."</td>";
						}
						else
						{
							$SubjectDisplayName = ($SubjectTitleType==1) ? $EngSubjectName : $ChiSubjectName;
							$SubjectCell = "<td class='small_text'>".$Prefix.$SubjectDisplayName."</td>";
						}
						$DetailsTable .= "<tr>";
						$DetailsTable .= "<td class='tabletext $top_style'>
											<table width='100%' border='0' cellpadding='2' cellspacing='0'><tr>".$SubjectCell."</tr></table>
										</td>";
						$DetailsTable .= ($ShowFullMark==1) ? "<td class='border_left tabletext $top_style' align='center'>".($ResultShow==1?$FullMark:"&nbsp;")."</td>" : "";
						$ValidCount = 0;
						for($i=0; $i<sizeof($ColumnArr); $i++)
						{
							list($ReportColumnID, $ColumnTitle, $Weight, $ShowPosition, $PositionRange) = $ColumnArr[$i];
							$Setting = $ReportCellSetting[$ReportColumnID][$ReportSubjectID];

							if($Setting!="N/A")
								$ValidCount++;
							else
								$Setting = "--";

							$Setting = ($MarkTypeDisplay==2 && $Setting=="S") ? "G" : $Setting;
							$DetailsTable .= "<td class='border_left $top_style'><table border=0 cellpadding=0 cellspacing=0 width='100%'>";
							$DetailsTable .= "<tr>";
							if(!$ResultShow) {
								$DetailsTable .= "<td align='center' class='tabletext'>&nbsp;</td>";
							}
							else
							{
								if($ShowPosition==1 || $ShowPosition==2) {
									$DetailsTable .= "<td align='center' class='tabletext' width='50%'>".$Setting."</td>";
									$DetailsTable .= "<td>&nbsp;</td>";
									$DetailsTable .= ($HidePosition==0 && $Setting=="S") ? "<td align='center' class='tabletext' width='50%'>(#)</td>" : "<td align='center' class='tabletext' width='50%'>(*)</td>";
								}
								else {
									$DetailsTable .= "<td align='center' class='tabletext'>".$Setting."</td>";
								}
							}
							$DetailsTable .= "</tr>";
							$DetailsTable .= "</table></td>";
						}

						if($ColumnSize>1 && $HideOverallResult!=1)
						{
							$Scale = ($MarkTypeDisplay==2 && $Scale=="S") ? "G" : $Scale;
							$DetailsTable .= "<td class='border_left $top_style'><table border=0 cellpadding=0 cellspacing=0 width='100%'>";
							$DetailsTable .= "<tr>";
							if(!$OverallShow)
							{
								$DetailsTable .= "<td class='tabletext' align='center'>&nbsp;</td>";
							}
							else
							{
								if($ShowOverallPosition==1 || $ShowOverallPosition==2)
								{
									$DetailsTable .= "<td align='center' class='tabletext' width='50%'>".($ValidCount>0?$Scale:'--')."</td>";
									$DetailsTable .= "<td>&nbsp;</td>";
									$DetailsTable .= ($HidePosition==0 && $Setting=="S") ? "<td align='center' class='tabletext' width='50%'>(#)</td>" : "<td align='center' class='tabletext' width='50%'>(*)</td>";
								}
								else
								{
									$DetailsTable .= "<td align='center' class='tabletext'>".($ValidCount>0?$Scale:'--')."</td>";
								}
							}
							$DetailsTable .= "</tr>";
							$DetailsTable .= "</table></td>";
						}
						/*
						if($ShowSubjectTeacherComment==1)
						{
							$DetailsTable .= "<td class='tabletext border_left $top_style'>&nbsp;</td>";
						}
						*/
						$DetailsTable .= "</tr>";

						$count++;
					}
				}
			}
		}
		else
		{
			$DetailsTable .= "<tr><td align='center' class='tabletext' colspan='".$ColumnSpan."'>&nbsp;</td></tr>";
		}


		$colspan = ($ShowFullMark==1) ? "colspan='2'" : "";
		$DetailsTable .= "<tr>";
		$DetailsTable .= "<td class='border_top' {$colspan}>
					<table border='0' cellpadding='2' cellspacing='0' width='100%'><tr><td class='small_title'>".$LangArray['ECA']."</td></tr></table>
					</td>";
		for($i=0; $i<sizeof($ColumnArray); $i++)
		{
			$ColumnTitle = trim($ColumnArray[$i]["ColumnTitle"]);
			$DetailsTable .= "<td class='tabletext border_left border_top' align='center'>G</td>";
		}
		if(!($Semester=='FULL' && $HideOverallResult==1))
			$DetailsTable .= "<td class='tabletext border_left border_top' align='center'>##G</td>";
		$DetailsTable .= "</tr>";

		
		
		if(!empty($SummaryResultArray))
		{
			for($k=0; $k<sizeof($SummaryResultArray); $k++)
			{
				$top_border_style = ($k==0) ? "border_top" : "";
				$colspan = ($ShowFullMark==1) ? "colspan='2'" : "";
				$DetailsTable .= "<tr>";
				$DetailsTable .= "<td class='{$top_border_style}' {$colspan}>
							<table border='0' cellpadding='2' cellspacing='0' width='100%'><tr><td class='small_title'>".$SummaryResultArray[$k]["Title"]."</td></tr></table>
							</td>";
				for($i=0; $i<sizeof($ColumnArray); $i++)
				{
					$ColumnTitle = trim($ColumnArray[$i]["ColumnTitle"]);
					$DetailsTable .= "<td class='tabletext border_left {$top_border_style}' align='center'>".$SummaryResultArray[$k][$ColumnTitle]."</td>";
				}
				if(!($Semester=='FULL' && $HideOverallResult==1))
					$DetailsTable .= "<td class='tabletext border_left {$top_border_style}' align='center'>##".$SummaryResultArray[$k][$Semester]."</td>";
				/*
				if($ShowSubjectTeacherComment==1) {
					$DetailsTable .= "<td class='tabletext border_left {$top_border_style}' align='center'>&nbsp;</td>";
				}
				*/
				$DetailsTable .= "</tr>";
			}
		}
		
		if(!empty($MeritResultArray))
		{
			for($k=0; $k<sizeof($MeritResultArray); $k++)
			{
				$top_border_style = ($k==0) ? "border_top" : "";
				$colspan = ($ShowFullMark==1) ? "colspan='2'" : "";
				$DetailsTable .= "<tr>";
				$DetailsTable .= "<td class='{$top_border_style}' {$colspan}>
							<table border='0' cellpadding='2' cellspacing='0' width='100%'><tr><td class='small_title'>".$MeritResultArray[$k]["Title"]."</td></tr></table>
							</td>";
				for($i=0; $i<sizeof($ColumnArray); $i++)
				{
					$ColumnTitle = trim($ColumnArray[$i]["ColumnTitle"]);
					$DetailsTable .= "<td class='tabletext border_left {$top_border_style}' align='center'>".$MeritResultArray[$k][$ColumnTitle]."</td>";
				}
				if(!($Semester=='FULL' && $HideOverallResult==1))
					$DetailsTable .= "<td class='tabletext border_left {$top_border_style}' align='center'>##".$MeritResultArray[$k][$Semester]."</td>";
				/*
				if($ShowSubjectTeacherComment==1) {
					$DetailsTable .= "<td class='tabletext border_left {$top_border_style}' align='center'>&nbsp;</td>";
				}
				*/
				$DetailsTable .= "</tr>";
			}
		}
		
		$DetailsTable .= "</table>";
		//debug($DetailsTable);

		if($Footer!="") {
			$FooterRow = "<tr><td class='small_text'>".str_replace("\n", "<br />", intranet_undo_htmlspecialchars($Footer))."</td></tr>\n";
		}
##########################################################################################

$linterface->LAYOUT_START();
?>
<link href="<?=$PATH_WRT_ROOT?>templates/<?=$LAYOUT_SKIN?>/css/print.css" rel="stylesheet" type="text/css" />

<table border="0" cellpadding="0" cellspacing="0" align="center" width="90%">
<tr>
<td align="center">
	<table width="95%" border="0" cellpadding="2" cellspacing="1">
	<tr><td><?= $TitleTable?></td><tr>
	<?=$StudentInfoRow?>
	<tr><td align="center" width="100%"><?=$DetailsTable?></td><tr>
	<?=$MiscTableRow?>
	<?=$SignatureTable?>
	<?=$FooterRow?>
	</table>
</td>
</tr>

<tr><td>
<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
<tr>
	<td align="center"><?= $linterface->GET_ACTION_BTN($button_close, "button", "javascript:self.close()")?>
	</td>
</tr>
</table>
<br/>
</td></tr>

</table>
<?
        $linterface->LAYOUT_STOP();
		intranet_closedb();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>