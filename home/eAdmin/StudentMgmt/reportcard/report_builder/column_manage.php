<?php

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
# preserve table view
if ($ck_report_column_page_number!=$pageNo && $pageNo!="")
{
	setcookie("ck_report_column_page_number", $pageNo, 0, "", "", 0);
	$ck_report_column_page_number = $pageNo;
} else if (!isset($pageNo) && $ck_report_column_page_number!="")
{
	$pageNo = $ck_report_column_page_number;
}

if ($ck_report_column_page_order!=$order && $order!="")
{
	setcookie("ck_report_column_page_order", $order, 0, "", "", 0);
	$ck_report_column_page_order = $order;
} else if (!isset($order) && $ck_report_column_page_order!="")
{
	$order = $ck_report_column_page_order;
}

if ($ck_report_column_page_field!=$field && $field!="")
{
	setcookie("ck_report_column_page_field", $field, 0, "", "", 0);
	$ck_report_column_page_field = $field;
} else if (!isset($field) && $ck_report_column_page_field!="")
{
	$field = $ck_report_column_page_field;
}

$PATH_WRT_ROOT = "../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcard.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
//include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();


if ($plugin['ReportCard'])
{
	include_once($PATH_WRT_ROOT."includes/libreportcard.php");
	$lreportcard = new libreportcard();
	$CurrentPage = "ReportBuilder_Template";
	$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
	$MODULE_OBJ['title'] = $eReportCard['ColumnManage'];

	if ($lreportcard->hasAccessRight())
    {
         $linterface = new interface_html("popup.html");

############################################################################################################

# TABLE SQL
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

if (!isset($field)) $field = 1;
if (!isset($order)) $order = 1;
$order = ($order == 1) ? 1 : 0;

$sql  = "SELECT
			CONCAT('<a class=\'tablelink\' href=\'column_edit.php?ReportID=$ReportID&ReportType=$ReportType&ReportColumnID=', ReportColumnID,'\'>', ColumnTitle, '</a>'),
			if (IsColumnSum, '--', CONCAT(Weight, '%')),
			if (IsColumnSum, '--', CONCAT(MarkSummaryWeight, '%')),
			DisplayOrder,
			DateModified,
			CONCAT('<input type=checkbox name=ReportColumnID[] value=', ReportColumnID,'>')
         FROM
            RC_REPORT_TEMPLATE_COLUMN
		 WHERE 
			ReportID = '".$ReportID."'
         ";
$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("ColumnTitle", "Weight", "MarkSummaryWeight", "DisplayOrder", "DateModified");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;
$li->title = "";
$li->column_array = array(0,0,0,0,0);
$li->wrap_array = array(0,0,0,0,0);
$li->IsColOff = 2;

# TABLE COLUMN
$pos = 0;
$li->column_list .= "<td width='1' class='tabletoplink'>#</td>\n";
$li->column_list .= "<td width='20%' >".$li->column($pos++, $i_general_title)."</td>\n";
$li->column_list .= "<td width='20%' >".$li->column($pos++, $eReportCard['WeightTotalSummary'])."</td>\n";
$li->column_list .= "<td width='20%' >".$li->column($pos++, $eReportCard['WeightMarkSummary'])."</td>\n";
$li->column_list .= "<td width='20%' >".$li->column($pos++, $i_general_DisplayOrder)."</td>\n";
$li->column_list .= "<td width='20%'>".$li->column($pos++, $i_LastModified)."</td>\n";
$li->column_list .= "<td width='1'>".$li->check("ReportColumnID[]")."</td>\n";

$toolbar = $linterface->GET_LNK_NEW("javascript:checkGet(document.form1,'column_new.php')", '', '', '', '', 0);

############################################################################################################

$linterface->LAYOUT_START();
?>
<script language="javascript">
function jREMOVE_COLUMN(obj,element,page){
		var alertConfirmRemove = "<?=$eReportCard['RemoveItemAlert']?>";
        if(countChecked(obj,element)==0)
			alert(globalAlertMsg2);
        else{
                if(confirm(alertConfirmRemove)){
                obj.action=page;
                obj.method="POST";
                obj.submit();
                }
        }
}
</script>
<br />
<form name="form1" method="get">
<table width="96%" border="0" cellpadding="0" cellspacing="0" align="center">
<tr><td><?= $toolbar ?></td><td align="right"><?= $linterface->GET_SYS_MSG($Result); ?></td></tr>
<tr><td>&nbsp;</td><td align="right">
<table border="0" cellspacing="0" cellpadding="0">
<tr>
  <td width="21"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_01.gif" width="21" height="23"></td>
  <td background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_02.gif"><table border="0" cellspacing="0" cellpadding="2">
      <tr>
        <td nowrap="nowrap"><a href="javascript:checkEdit(document.form1,'ReportColumnID[]','column_edit.php')" class="tabletool"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_edit.gif" width="12" height="12" border="0" align="absmiddle">
          <?=$button_edit?></a></td>
        <td><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="5"></td>
        <td nowrap="nowrap"><a href="javascript:jREMOVE_COLUMN(document.form1,'ReportColumnID[]','column_remove.php')" class="tabletool"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_delete.gif" width="12" height="12" border="0" align="absmiddle">
          <?=$button_remove?></a></td>
      </tr>
    </table></td>
  <td width="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_03.gif" width="6" height="23"></td>
</tr>
</table>
<tr><td colspan="2"><?= $li->display() ?></td></tr>
<tr><td colspan="2">&nbsp;</td></tr>
<tr>
	<td align="center" colspan="2">
	<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tr><td align="center"><?= $linterface->GET_ACTION_BTN($button_close, "button", "javascript:self.close()")?></td></tr>
	</table>
	</td>
</tr>
</table>

<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
<input type="hidden" name="order" value="<?php echo $li->order; ?>" />
<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
<input type="hidden" name="page_size_change" value="" />
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />
<input type="hidden" name="ReportID" value="<?=$ReportID?>" />
<input type="hidden" name="ReportType" value="<?=$ReportType?>" />

</form>
<?
		if($Result!="")
		{
			echo "<script language='javascript'>opener.location.reload();</script>";
		}

        $linterface->LAYOUT_STOP();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>