<?php
$PATH_WRT_ROOT = "../../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

$lf = new libfilesystem();

if ($plugin['ReportCard'])
{
	include_once($PATH_WRT_ROOT."includes/libreportcard.php");
	$libreportcard = new libreportcard();
	if($ck_ReportCard_UserType=="ADMIN")
	{
		$CurrentPage = "MarksheetRevision";
		$BackPage = "admin_index.php?SubjectID=$SubjectID&ClassLevelID=$ClassLevelID";
		$TagName = $eReportCard['MarksheetRevision'];
	}
	else
	{
		$CurrentPage = "MarksheetSubmission";
		$BackPage = "index.php";
		$TagName = $eReportCard['MarksheetSubmission'];
	}
	$MODULE_OBJ = $libreportcard->GET_MODULE_OBJ_ARR();
	$MODULE_OBJ['title'] = $eReportCard['ScoreList'];

    if ($libreportcard->hasAccessRight())
    {
        $linterface = new interface_html("popup.html");

###############################################################

#############################################################################################
#############################################################################################

function GET_SEMESTER_NUMBER($ParSemester)
{
	global $libreportcard, $SemesterArray;

	$sem_number = "-1";
	if(trim($ParSemester)=="FULL")
	{
		$sem_number = 0;
	} else
	{
		for($i=0; $i<sizeof($SemesterArray); $i++)
		{
			if(trim($ParSemester)==trim($SemesterArray[$i]))
			{
				$sem_number = $i+1;
				break;
			}
		}
	}
	return $sem_number;
}

function CHECK_CSV_FILE_FORMAT($ParHeaderArray, $ParDefaultHeaderArray)
{
	# Check Title Row
	$format_wrong = false;

	for ($i=0; $i<sizeof($ParDefaultHeaderArray); $i++)
	{
		if ($ParHeaderArray[$i]!=$ParDefaultHeaderArray[$i])
		{
			$format_wrong = true;
			break;
		}
	}

	return $format_wrong;
}

function GET_CSV_FILE_CONTENT($ParFile, $ParDefaultHeaderArray)
{
	global $libreportcard, $lf, $StudentRegNoArray, $SemesterArray;

	$ClassArr = $libreportcard->GET_CLASSES();
	
	$TargetArray = $SemesterArray;
	$TargetArray[] = "FULL";
	for($i=0; $i<sizeof($TargetArray); $i++)
	{
		$t_semester = trim($TargetArray[$i]);
		$sem_number = GET_SEMESTER_NUMBER($t_semester);
		if($sem_number>=0)
		{
			for ($j = 0; $j < sizeof($ClassArr); $j++)
			{
				$TargetFile = $ParFile."/".trim($libreportcard->Year)."_".$sem_number."_".str_replace("/", "", $ClassArr[$j]).".csv";
				if(file_exists($TargetFile))
				{
					$data = $lf->file_read_csv($TargetFile);
					if(!empty($data))
					{
						$header_row = array_shift($data);
						$wrong_format = CHECK_CSV_FILE_FORMAT($header_row, $ParDefaultHeaderArray);
						if(!$wrong_format)
						{
							for($k=0; $k<sizeof($data); $k++)
							{
								$reg_no = array_shift($data[$k]);
								$reg_no = trim(str_replace("#", "", $reg_no));
								$student_id = trim($StudentRegNoArray[$reg_no]);
								print $student_id."<br>";
								if($student_id!="")
								{
									if(sizeof($data[$k])>1) {
										$ReturnArray[$student_id][$t_semester] = $data[$k];
									}
									else {
										$ReturnArray[$student_id][$t_semester] .= "<li>".$data[$k][0]."</li>";
									}
								}
							}
						}
					}
				}
			}
		}
	}

	return $ReturnArray;
}


		if ($UploadType == 0) {
			
			# get summary data from csv file			
			$SummaryHeader = array("REGNO", "ABSENT", "LATE", "ABSENT_WO_LEAVE", "CONDUCT");
			$TargetSummaryFile = $intranet_root."/file/reportcard/summary";
			$CSVDataArray = GET_CSV_FILE_CONTENT($TargetSummaryFile, $SummaryHeader);
			
		} else if ($UploadType == 1) {
			
			# get award data from csv file			
			$AwardHeader = array("REGNO", "TITLE");
			$TargetAwardFile = $intranet_root."/file/reportcard/award";
			$CSVDataArray = GET_CSV_FILE_CONTENT($TargetAwardFile, $AwardHeader);
			
		} else if ($UploadType == 2) {
			
			# get merit data from csv file			
			$MeritHeader = ($ReportCardTemplate==2) ? array("REGNO", "MERIT", "DEMERIT") : array("REGNO", "TITLE");
			$TargetMeritFile = $intranet_root."/file/reportcard/merit";
			$CSVDataArray = GET_CSV_FILE_CONTENT($TargetMeritFile, $MeritHeader);
			
		} else if ($UploadType == 3) {
			
			# get eca data from csv file			
			$ECAHeader = array("REGNO", "TITLE");
			$TargetECAFile = $intranet_root."/file/reportcard/eca";
			$CSVDataArray = GET_CSV_FILE_CONTENT($TargetECAFile, $ECAHeader);
			
		} else if ($UploadType == 4) {
			
			# get remark data from csv file			
			$RemarkHeader = array("REGNO", "REMARK");
			$TargetRemarkFile = $intranet_root."/file/reportcard/remark";
			$CSVDataArray = GET_CSV_FILE_CONTENT($TargetRemarkFile, $RemarkHeader);
			
		} else if ($UploadType == 5) {
			
			# get inter school competition data from csv file			
			$InterSchoolHeader = array("REGNO", "TITLE");
			$TargetInterSchoolFile = $intranet_root."/file/reportcard/interschool";
			$CSVDataArray = GET_CSV_FILE_CONTENT($TargetInterSchoolFile, $InterSchoolHeader);
			
		} else if ($UploadType == 6) {
			
			# get school service data from csv file
			$SchoolServiceHeader = array("REGNO", "TITLE");
			$TargetSchoolServiceFile = $intranet_root."/file/reportcard/schoolservice";
			$CSVDataArray = GET_CSV_FILE_CONTENT($TargetSchoolServiceFile, $SchoolServiceHeader);
			
		}
debug($CSVDataArray);
		$csvSelection = "<select id='UploadType' name='UploadType' onchange='document.form1.submit();'>";
		$csvSelection .= "<option value='0' ".(($UploadType == 0) ? "selected":"").">".$eReportCard['SummaryInfoUpload']."</option>";
		$csvSelection .= "<option value='1' ".(($UploadType == 1) ? "selected":"").">".$eReportCard['AwardRecordUpload']."</option>";
		$csvSelection .= "<option value='2' ".(($UploadType == 2) ? "selected":"").">".$eReportCard['MeritRecordUpload']."</option>";
		$csvSelection .= "<option value='3' ".(($UploadType == 3) ? "selected":"").">".$eReportCard['ECARecordUpload']."</option>";
		$csvSelection .= "<option value='4' ".(($UploadType == 4) ? "selected":"").">".$eReportCard['RemarkUpload']."</option>";
		$csvSelection .= "<option value='5' ".(($UploadType == 5) ? "selected":"").">".$eReportCard['InterSchoolCompetitionUpload']."</option>";
		$csvSelection .= "<option value='6' ".(($UploadType == 6) ? "selected":"").">".$eReportCard['SchoolServiceUpload']."</option>";
		$csvSelection .= "</select>";

		
		

//debug_r($CSVDataArray);
		
		
		
		
		


#############################################################################################
#############################################################################################





		
		if($ck_ReportCard_UserType=="ADMIN")
		{
			$FormArray = $libreportcard->GET_ALL_REPORT_FORMS();
			if($ClassLevelID=="")
				$ClassLevelID = $FormArray[0][0];

			$FormSelection = getSelectByArray($FormArray, "name='ClassLevelID' onChange=\"document.form1.form_change.value=1;jFilterChange()\"", $ClassLevelID, 1, 1, "");

			$ClassArray = $libreportcard->GET_CLASSES_BY_FORM($ClassLevelID);
			$ClassSelection = getSelectByArray($ClassArray, "name='ClassID' onChange=\"jFilterChange()\"", $ClassID, 1, 1, "");
			
			if($ClassID=="" || $form_change==1)
				$ClassID = $ClassArray[0][0];

			$FormRow = "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">".$eReportCard['Form']."</td><td class=\"tabletext\" width=\"70%\">".$FormSelection."</td></tr>";
		}
		else
			$HiddenClassIDRow = "<input type=\"hidden\" name=\"ClassID\" value=\"".$ClassID."\" />";
		
		# get class object
		$ClassObj = $libreportcard->GET_CLASS_OBJ($ClassID);
		list($ClassID, $ClassLevelID, $ClassName) = $ClassObj;
		
		$ClassDisplay = ($ck_ReportCard_UserType=="ADMIN") ? $ClassSelection : $ClassName;

		# Get ReportID Class
		$ReportID = $libreportcard->GET_REPORTID_BY_CLASS($ClassID);
		
		#########################################################################################################
		# Get Column Array
		$ColumnArray = $libreportcard->GET_REPORT_TEMPLATE_COLUMN($ReportID);
		/*
            [ReportColumnID] => 19
            [ColumnTitle] => �W
		*/
		# Get Column Selection
		$ColumnArraySel = "<select id='ReportColumnID' name='ReportColumnID' onchange='document.form1.action=\"score_list.php\"; document.form1.submit();'>";
		$ColumnArraySel .= "<option value=''>".$eReportCard['OverallResult']."</option>";
		for ($temp = 0; $temp < sizeof($ColumnArray); $temp++) {
			$selected = ($ReportColumnID == $ColumnArray[$temp][0]) ? "selected" : "";
			$ColumnArraySel .= "<option value='".$ColumnArray[$temp][0]."' $selected>".$ColumnArray[$temp][1]."</option>";
		}
		$ColumnArraySel .= "</select>";
		
		# Get IsWeighted Selection
		$IsWeightedSel = "<select id='IsWeighted' name='IsWeighted' onchange='document.form1.action=\"score_list.php\"; document.form1.submit();'>";		
		$IsWeightedSel .= "<option value='0'".($IsWeighted?"":"selected").">".$eReportCard['RawMarks']."</option>";
		$IsWeightedSel .= "<option value='1'".($IsWeighted?"selected":"").">".$eReportCard['WeightedMarks']."</option>";
		$IsWeightedSel .= "</select>";
		#########################################################################################################
		
		# Get Results
		if ($ReportColumnID != "") {
			$ResultArray = $libreportcard->GET_STUDENT_SUBJECT_COLUMN_RESULT($ClassID, $ReportColumnID, $IsWeighted);
		} else {
			$ResultArray = $libreportcard->GET_STUDENT_SUBJECT_RESULT($ClassID);
		}
				
		# Get Student of Class
		$StudentArray = $libreportcard->GET_STUDENT_BY_CLASS($ClassID);		
		
		# Get Report Subject 
		$SubjectArray = $libreportcard->GET_REPORT_SUBJECTS($ReportID);
		
		# Get Report Display Settings
		$ReportInfo = $libreportcard->GET_REPORT_TEMPLATE($ReportID);
		$Settings = $ReportInfo[4];
		$SettingArray = unserialize($Settings);
		$MarkTypeDisplay = $SettingArray["MarkTypeDisplay"];
		
		$TableContent = "<table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">";
		if(is_array($SubjectArray))
		{
			$HeaderRow = "<tr class='tablebluetop'>";
			$HeaderRow .= "<td>&nbsp;</td>";
			foreach($SubjectArray as $subject_id => $subject_name)
			{
				$HeaderRow .= "<td class=\"tabletoplink\">".$subject_name."</td>";
				$SubjectIDArray[] = $subject_id;
			}
			$HeaderRow .= "</tr>";
		}
		
		# Get Grademark scheme of subjects
		$SubjectIDList = (is_array($SubjectIDArray)) ? implode(",", $SubjectIDArray) : "";
		list($FailedArray, $DistinctionArray, $PFArray, $SubjectScaleArray) = $libreportcard->GET_SUBJECT_GRADEMARK($ClassID, $SubjectIDList);
		
		$colsize = sizeof($SubjectArray) + 1;
		$GoToBottomRow = "<tr><td align='right' colspan='".$colsize."'><a class='tablegreenlink' href='#bottom'>".$eReportCard['GoToBottom']."</a></td></tr>";
		$GoToTopRow = "<tr><td align='right' colspan='".$colsize."'><a class='tablegreenlink' href='#top'>".$eReportCard['GoToTop']."</a></td></tr>";
		
		$TableContent .= $GoToBottomRow;
		$TableContent .= $HeaderRow;
		
		for($i=0; $i<sizeof($StudentArray); $i++)
		{
			$css = ($i%2?"2":"");
			list($r_student_id, $class_number, $student_name) = $StudentArray[$i];

			$TableContent .= "<tr class='tablebluerow".$css."'>";
			$TableContent .= "<td nowrap='nowrap'><span class=\"tabletext\">&lt;".$class_number."&gt; ".$student_name."</span></td>";
			for($k=0; $k<sizeof($SubjectIDArray); $k++)
			{
				$r_subject_id = $SubjectIDArray[$k];
				$SubjectSetting = $SubjectScaleArray[$r_subject_id];
				
				$StylePrefix = "";
				$StyleSuffix = "";
				if($SubjectSetting=="S")
				{
					$s_mark = $ResultArray[$r_student_id][$r_subject_id]["Mark"];
					$s_grade = $ResultArray[$r_student_id][$r_subject_id]["Grade"];
					if($s_mark>=0) {
						$Result = ($MarkTypeDisplay==2) ? $s_grade : $s_mark;
						$TempSetting = ($MarkTypeDisplay==2) ? "G" : $SubjectSetting;
						list($StylePrefix, $StyleSuffix) = $libreportcard->GET_STYLE($TempSetting, $Result, $FailedArray[$r_subject_id], $DistinctionArray[$r_subject_id], $PFArray[$r_subject_id]);
					}
					else
						$Result = $libreportcard->SpecialMarkArray[trim($s_mark)];
				} 
				else {
					$Result = $ResultArray[$r_student_id][$r_subject_id]["Grade"];
					if($Result!="/" && $Result!="abs") {
						list($StylePrefix, $StyleSuffix) = $libreportcard->GET_STYLE($SubjectSetting, $Result, $FailedArray[$r_subject_id], $DistinctionArray[$r_subject_id], $PFArray[$r_subject_id]);
					}
				}
				$Result = $StylePrefix.$Result.$StyleSuffix;
				$TableContent .= "<td nowrap='nowrap'><span class=\"tabletext\">".($Result==""?"--":$Result)."</span></td>";
			}
			$TableContent .= "</tr>";
		}
		$TableContent .= $GoToTopRow;
		$TableContent .= "</table>";
################################################################

$linterface->LAYOUT_START();
?>
<style type='text/css' media='print'>
.print_hide {display:none;}
</style>
<SCRIPT LANGUAGE="Javascript">
function jGO_PRINT_PAGE()
{
	obj = document.form1;
	obj.IsPrint.value = 1;
	obj.action = "score_list_print.php";
	obj.target = "_blank";
	obj.submit();
	obj.action = "score_list.php";
	obj.target = "_self";
}

function jFilterChange()
{
	obj = document.form1;
	obj.action = "score_list.php";
	obj.target = "_self";
	obj.submit();
}
</SCRIPT>

<br />
<form name="form1" action="" method="GET">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td>
		<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
			<?=$FormRow?>
			<tr><td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $eReportCard['ClassName']; ?></td><td class="tabletext" width="70%"><?=$ClassDisplay?></td></tr>
			<tr><td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $eReportCard['Column']; ?></td><td class="tabletext" width="70%"><?=$csvSelection?></td></tr>			
			<tr><td class="dotline" colspan=2><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
		</table>
	</td>
</tr>
<tr>
	<td>
		<?=$TableContent?>
	</td>
</tr>
<tr>
	<td><table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
	<tr>
		<td align="center"" id="bottom">
			<?= $linterface->GET_ACTION_BTN($button_close, "button", "self.close();")?>&nbsp;
			<?= $linterface->GET_ACTION_BTN($eReportCard['PrintPage'], "button", "jGO_PRINT_PAGE()")?>
			<?= $linterface->GET_ACTION_BTN($button_export, "button", "self.location.href='score_list_export.php?ClassID=$ClassID'")?>
		</td>
	</tr>
	</table>
	</td>
</tr>
</table>
<?=$HiddenClassIDRow?>
<input type="hidden" name="IsPrint" />
<input type="hidden" name="form_change" />

</form>
<?
        $linterface->LAYOUT_STOP();
		intranet_closedb();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>
