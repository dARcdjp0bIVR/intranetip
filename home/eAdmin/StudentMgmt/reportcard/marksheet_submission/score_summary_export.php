<?php
$PATH_WRT_ROOT = "../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libreportcard.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once("../default_header.php");

intranet_auth();
intranet_opendb();

$libreportcard = new libreportcard();
$lexport = new libexporttext();

$IsWeighted = 0;
$ClassArray = $libreportcard->GET_CLASSES_BY_FORM($ClassLevelID);

foreach($ClassArray as $ClassObj)
{
	$ClassID = $ClassObj['ClassID'];
	if ($ReportColumnID != "") {
		$ResultArray[$ClassID] = $libreportcard->GET_STUDENT_SUBJECT_COLUMN_RESULT($ClassID, $ReportColumnID, $IsWeighted, $SubjectID);
	} else {
		$ResultArray[$ClassID] = $libreportcard->GET_STUDENT_SUBJECT_RESULT($ClassID, $SubjectID);
	}

	# Get Student of Class
	$StudentArray[$ClassID] = $libreportcard->GET_STUDENT_BY_CLASS($ClassID);
}

# Get Report Display Settings
$ReportInfo = $libreportcard->GET_REPORT_TEMPLATE($ReportID);
$Settings = $ReportInfo[4];
$SettingArray = unserialize($Settings);
$MarkTypeDisplay = $SettingArray["MarkTypeDisplay"];

$OutstandingMark = 90;
$PassingMark = 50;
if(is_array($ResultArray))
{
	foreach($ResultArray as $ClassID => $Students)
	{
	  if (!isset($PassingMarkArr[$SubjectID][$ClassID]))
  			{
	  			list($GradingSchemeID, $DefaultSetting, $FullMark) = $libreportcard->GET_GRADING_SCHEME_BY_SUBJECT_CLASS($SubjectID, $ClassID);
	  			list($MainInfo, $GradeMarkArr) = $libreportcard->GET_SCHEME_INFO($GradingSchemeID);
	  			$PassingMarkArr[$SubjectID][$ClassID] = $GradeMarkArr['P'][sizeof($GradeMarkArr['P'])-1][0];
			  	$PassingMark = $PassingMarkArr[$SubjectID][$ClassID];
	  			$OutstandingMark = $GradeMarkArr['D'][0];
			}

  		  //$MarkClass[0][$ClassID] = count($Students); // Number of students
  		  $MarkClass[0][$ClassID] = 0;
	  $MarkClass[1][$ClassID] = 0; // Below 10
	  $MarkClass[2][$ClassID] = 0; // Below 20
	  $MarkClass[3][$ClassID] = 0; // Below 30
	  $MarkClass[4][$ClassID] = 0; // Below 40
	  $MarkClass[5][$ClassID] = 0; // Below 50
	  $MarkClass[6][$ClassID] = 0; // Below 60
	  $MarkClass[7][$ClassID] = 0; // Below 70
	  $MarkClass[8][$ClassID] = 0; // Below 80
	  $MarkClass[9][$ClassID] = 0; // Below 90
	  $MarkClass[10][$ClassID] = 0; // Over 90
	  $MarkClass[11][$ClassID] = -999; // Highest mark
	  $MarkClass[12][$ClassID] = 999; // Lowest mark
	  $MarkClass[13][$ClassID] = 0; // Number of outstandings
	  $MarkClass[14][$ClassID] = 0; // Outstanding rate
	  $MarkClass[15][$ClassID] = 0; // Number of passes
	  $MarkClass[16][$ClassID] = 0; // Passing rate
	  $MarkClass[17][$ClassID] = 0; // Number of fails
	  $MarkClass[18][$ClassID] = 0; // Failure rate
	  $MarkClass[19][$ClassID] = ""; // Mean

	  $DummyTotal = 0;
	  $tmp_mark = array();

    if(is_array($Students))
    {
      foreach($Students as $StudentID => $Subject)
      {
        foreach($Subject as $SubjectID => $Mark)
        {
        	if ($Mark['Mark']<0)
            	{
            		continue;
            	}

            	$MarkClass[0][$ClassID] ++;

          if($Mark['Mark'] < 10)
            $MarkClass[1][$ClassID]++;
          else if($Mark['Mark'] < 20)
            $MarkClass[2][$ClassID]++;
          else if($Mark['Mark'] < 30)
            $MarkClass[3][$ClassID]++;
          else if($Mark['Mark'] < 40)
            $MarkClass[4][$ClassID]++;
          else if($Mark['Mark'] < 50)
            $MarkClass[5][$ClassID]++;
          else if($Mark['Mark'] < 60)
            $MarkClass[6][$ClassID]++;
          else if($Mark['Mark'] < 70)
            $MarkClass[7][$ClassID]++;
          else if($Mark['Mark'] < 80)
            $MarkClass[8][$ClassID]++;
          else if($Mark['Mark'] < 90)
            $MarkClass[9][$ClassID]++;
          else
            $MarkClass[10][$ClassID]++;

          if($MarkClass[11][$ClassID] < $Mark['Mark'])
            $MarkClass[11][$ClassID] = $Mark['Mark'];
          if($MarkClass[12][$ClassID] > $Mark['Mark'])
            $MarkClass[12][$ClassID] = $Mark['Mark'];

          if($Mark['Mark'] >= $OutstandingMark)
            $MarkClass[13][$ClassID]++;
          if($Mark['Mark'] >= $PassingMark)
            $MarkClass[15][$ClassID]++;
          else
            $MarkClass[17][$ClassID]++;

          $tmp_mark[] = $Mark['Mark'];

          $DummyTotal += $Mark['Mark'];
        }
      }
    }

    if($MarkClass[11][$ClassID] == -999)
      $MarkClass[11][$ClassID] = "--";
    if($MarkClass[12][$ClassID] == 999)
      $MarkClass[12][$ClassID] = "--";
    if($MarkClass[0][$ClassID] != 0)
    {
      $MarkClass[14][$ClassID] = number_format(($MarkClass[13][$ClassID]/$MarkClass[0][$ClassID]*100), 2, '.', '')."%";
      $MarkClass[16][$ClassID] = number_format(($MarkClass[15][$ClassID]/$MarkClass[0][$ClassID]*100), 2, '.', '')."%";
      $MarkClass[18][$ClassID] = number_format(($MarkClass[17][$ClassID]/$MarkClass[0][$ClassID]*100), 2, '.', '')."%";
      $MarkClass[19][$ClassID] = round(($DummyTotal/$MarkClass[0][$ClassID]), 2);
      $MarkClass[20][$ClassID] = round($libreportcard->getStandardDeviation($tmp_mark),2);
    }
    if(strcmp($MarkClass[19][$ClassID],"") == 0)
      $MarkClass[19][$ClassID] = "--";
    if(strcmp($MarkClass[20][$ClassID],"") == 0)
      $MarkClass[20][$ClassID] = "--";
    else if($MarkClass[20][$ClassID] == -1)
      $MarkClass[20][$ClassID] = "Error in calculation";
	}
}

if(is_array($ClassArray))
{
  $exportColumn[] = "";
	foreach($ClassArray as $ClassObj)
	{
		$exportColumn[] = $ClassObj['ClassName'];
		$ClassIDArray[] = $ClassObj['ClassID'];
	}
}


for($i=0; $i<count($MarkClass); $i++)
{
	list($r_student_id, $class_number, $student_name) = $StudentArray[$i];

	$row[] = $eReportCard['ScoreSummaryTitle'][$i];
  foreach($ClassArray as $ClassObj)
    $row[] = $MarkClass[$i][$ClassObj['ClassID']];
	$rows[] = $row;
	unset($row);
}

$ClassLevelName = $libreportcard->GET_CLASSLEVEL_NAME($ClassLevelID);
$ClassLevelName = str_replace(".", "", $ClassLevelName);
intranet_closedb();

// Output the file to user browser
$filename = str_replace("&nbsp;", "", "Score_Summary_".$ClassLevelName.".csv");

$export_content = $lexport->GET_EXPORT_TXT($rows, $exportColumn);
$lexport->EXPORT_FILE($filename, $export_content);

//output2browser($Content, $filename);

?>
