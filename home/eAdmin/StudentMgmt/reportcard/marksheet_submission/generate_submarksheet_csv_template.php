<?php
$PATH_WRT_ROOT = "../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libreportcard.php");
//include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

$libreportcard = new libreportcard();

# Get Subject Name
$SubjectName = $libreportcard->GET_SUBJECT_NAME($SubjectID);
$SubjectName = str_replace("&nbsp;", "_", $SubjectName);

$ReportColumnInfo = $libreportcard->GET_REPORT_COLUMN_DETAILS($ReportColumnID);
$ColumnName = $ReportColumnInfo[0];

# Get Column Array
$ColumnArray = $libreportcard->GET_ALL_SUB_MARKSHEET_COLUMN($ReportColumnID, $SubjectID);
$ColumnSize = sizeof($ColumnArray);

# Get and Student Array
$StudentArray = $libreportcard->GET_STUDENT_BY_CLASS($ClassID, $TargetStudentIDList);
$StudentSize = sizeof($StudentArray);

$exportColumn = array();
$exportColumn[] = 'ClassName';
$exportColumn[] = 'ClassNumber';

//$Content .= "ClassName,ClassNumber";
for($i=0; $i<$ColumnSize; $i++)
{
	list($ColumnID, $ColumnTitle, $Weight) = $ColumnArray[$i];
	//$Content .= ",".$ColumnTitle;
	$exportColumn[] = $ColumnTitle;
}
$Content .= "\n";

$ExportArr = array();
$Counter = 0;
for($j=0; $j<sizeof($StudentArray); $j++)
{
	$Class = $StudentArray[$j]['ClassTitleEn'];
	$ClassNumber = $StudentArray[$j]['ClassNumber'];
	if(!empty($Class))
	{
		//list($ClassName, $ClassNumber) = explode("-", $Class);
		//$Content .= trim($ClassName).",".trim($ClassNumber)."\n";
		(array)$ExportArr[$Counter][] = $Class;
		(array)$ExportArr[$Counter][] = $ClassNumber;
		
		$Counter++;
	}
}

intranet_closedb();

// Output the file to user browser
$filename = "SubMarksheet_Template_".trim($SubjectName)."_".trim($ClassName)."_".trim($ColumnName).".csv";
//output2browser($Content, $filename);

$lexport = new libexporttext();
$export_content = $lexport->GET_EXPORT_TXT($ExportArr, $exportColumn);
$lexport->EXPORT_FILE($filename, $export_content);
?>