<?php
$PATH_WRT_ROOT = "../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libreportcard.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
intranet_auth();

$limport = new libimporttext();

$param = "SubjectID=$SubjectID&ClassID=$ClassID&ReportColumnID=$ReportColumnID&TargetStudentIDList=$TargetStudentIDList";

# uploaded file information
$filepath = $userfile;
$filename = $userfile_name;

if($filepath=="none" || $filepath == "" || !is_uploaded_file($filepath) || !$limport->CHECK_FILE_EXT($filename))
{
	header("Location: import.php?$param");
}
else
{
	intranet_opendb();

	$li = new libdb();
	$lo = new libfilesystem();
	$libreportcard = new libreportcard();
	
	/*
	$ext = strtoupper($lo->file_ext($filename));
	if($ext == ".CSV")
	{
		$data = $lo->file_read_csv($filepath);
		$HeaderRow = array_shift($data);	# drop the title bar
	}
	*/
	
	$data = $limport->GET_IMPORT_TXT($filepath);
	$HeaderRow = array_shift($data);		# drop the title bar
	
	# submission Year & Semester
	$Year = $libreportcard->Year;
	$Semester = $libreportcard->Semester;

	$ColumnTitle = array("ClassName", "ClassNumber");
	$ColumnArray = $libreportcard->GET_ALL_SUB_MARKSHEET_COLUMN($ReportColumnID, $SubjectID);
	$ColumnSize = sizeof($ColumnArray);
	for($i=0; $i<$ColumnSize; $i++)
	{
		$ColumnTitle[] = $ColumnArray[$i][1];
	}

	for($j=0; $j<sizeof($HeaderRow); $j++)
	{
		if($HeaderRow[$j]!=$ColumnTitle[$j])
		{
			$FormatWrong = 1;
			break;
		}
	}
	
	if($FormatWong!=1 && $ColumnSize>0)
	{
		for($i=0; $i<sizeof($data); $i++)
		{
			$data_obj = $data[$i];
			$cname = trim(array_shift($data_obj));
			$cnum = trim(array_shift($data_obj));
			$sql = "SELECT UserID FROM INTRANET_USER WHERE ClassName = '$cname' AND (ClassNumber = '$cnum' OR CONCAT('0',ClassNumber) = '$cnum' or ClassNumber = '0".$cnum."')";
			$row = $li->returnVector($sql);
			$StudentID = $row[0];

			if(!empty($StudentID) && !empty($data_obj))
			{
				$TotalPercent = 0;
				$WeightedMarkArray = array();
				
				# Remove all reocrds of student in the subject
				$sql = "DELETE FROM RC_SUB_MARKSHEET_SCORE WHERE ReportColumnID = '$ReportColumnID' AND SubjectID = '$SubjectID' AND StudentID = '$StudentID'";
				$li->db_db_query($sql);

				$TotalMark = 0;
				$TotalWeight = 0;
				for($j=0; $j<$ColumnSize; $j++)
				{
					list($ColumnID, $ColumnTitle, $Weight) = $ColumnArray[$j];

					$value = $data_obj[$j];
					if(strtolower($value)=="abs")
						$value = strtolower($value);

					# "-1" stand for "abs"; "-2" stand for "/"
					if($value=="abs")
						$value = "-1";
					else if(strcmp($value, "/")==0 || $value==="")
						$value = "-2";
					else
						$TotalMark = $TotalMark + ($value*$Weight/100);
					
					if($value!="-2")
						$TotalWeight = $TotalWeight + $Weight;

					# Insert Result
					$sql = "INSERT INTO RC_SUB_MARKSHEET_SCORE (ColumnID, SubjectID, ReportColumnID, StudentID, Mark, TeacherID, Year, Semester, DateInput, DateModified) VALUES ('$ColumnID', '$SubjectID', '$ReportColumnID', '$StudentID', '$value', '$UserID', '$Year', '$Semester', now(), now())";
					$success = $li->db_db_query($sql);
				}
				
				# Insert overall result
				$OverallResult = ($TotalWeight>0) ? ($TotalMark*100/$TotalWeight) : "-2";
				
				$sql = "INSERT INTO RC_SUB_MARKSHEET_SCORE (SubjectID, ReportColumnID, StudentID, Mark, TeacherID, Year, Semester, IsOverall, DateInput, DateModified) VALUES ('$SubjectID', '$ReportColumnID', '$StudentID', '$OverallResult', '$UserID', '$Year', '$Semester', 1, now(), now())";
				$success = $libreportcard->db_db_query($sql);
				
				# Insert overall weighted result
				$OverallWeightedResult = ($OverallResult>=0) ? ($OverallResult*$ColumnWeight/100) : "-2";
				$sql = "INSERT INTO RC_SUB_MARKSHEET_SCORE (SubjectID, ReportColumnID, StudentID, Mark, TeacherID, Year, Semester, IsOverallWeighted, DateInput, DateModified) VALUES ('$SubjectID', '$ReportColumnID', '$StudentID', '$OverallWeightedResult', '$UserID', '$Year', '$Semester', 1, now(), now())";
				$success = $libreportcard->db_db_query($sql);
			}				
		}
	}
}

$Result = ($success==1) ? "import_success" : "import_failed";
intranet_closedb();

header("Location:sub_marksheet.php?$param&Result=$Result");
?>

