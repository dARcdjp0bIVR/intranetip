<?php
$PATH_WRT_ROOT = "../../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

$Disabled = ($ViewOnly) ? 1 : 0;
if ($MarkType == 0) $Disabled = 0;
if ($ToCalculate) $RawCalculate = 1;

if ($plugin['ReportCard'])
{
	include_once($PATH_WRT_ROOT."includes/libreportcard.php");
	$libreportcard = new libreportcard();


	# get class object
	$ClassObj = $libreportcard->GET_CLASS_OBJ($ClassID);
	list($ClassID, $ClassLevelID, $ClassName) = $ClassObj;

	$TempIP = $SubjectID;
	if ($ParentSubjectID != "") $TempIP = $ParentSubjectID;
	$NotificationArray = $libreportcard->GET_CLASS_NOTIFICATION($TempIP);
	//$IsMSConfirmed = ($NotificationArray[$ClassID]==1);
	$IsMSSubmissionPeriod = $libreportcard->CHECK_MARKSHEET_SUBMISSION_PERIOD();

	if($ck_ReportCard_UserType=="ADMIN")
	{
		$ReturnSubjectID = ($ParentSubjectID!="") ? $ParentSubjectID : $SubjectID;
		$CurrentPage = "MarksheetRevision";
		$BackPage = "admin_index.php?SubjectID=$ReturnSubjectID&ClassLevelID=$ClassLevelID";
		$TagName = $eReportCard['MarksheetRevision'];
	}
	else
	{
		$CurrentPage = "MarksheetSubmission";
		$BackPage = "index.php";
		$TagName = $eReportCard['MarksheetSubmission'];
	}
	$MODULE_OBJ = $libreportcard->GET_MODULE_OBJ_ARR();
    if ($libreportcard->hasAccessRight())
    {
        $linterface = new interface_html();
###############################################################
		//StartTimer();

		# Get Subject Name
		$SubjectName = $libreportcard->GET_SUBJECT_NAME($SubjectID);

		# Retrievie report display setting
		$ReportSettings = $libreportcard->GET_REPORT_TEMPLATE_SETTINGS($ReportID);
		$ReportSettingArray = unserialize($ReportSettings);
		$ResultCalculationType = $ReportSettingArray["ResultCalculationType"];

		if(!empty($ReportID))
		{
			$param = "SubjectID=$SubjectID&ClassID=$ClassID&ReportID=$ReportID&HaveComponent=$HaveComponent&IsParent=$IsParent&ParentSubjectID=$ParentSubjectID&TargetStudentIDList=$TargetStudentIDList";

			$MarkType = ($MarkType=="") ? 0 : $MarkType;
			$InputMode = ($InputMode=="") ? 0 : $InputMode;

			# Get GradingSchemeID
			list($GradingSchemeID, $DefaultSetting, $FullMark) = $libreportcard->GET_GRADING_SCHEME_BY_SUBJECT_CLASS($SubjectID, $ClassID);
			if($FullMark!="")
			{
				$FullMarkRow = "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">".$eReportCard['FullMark']."</td><td class=\"tabletext\" width=\"70%\" nowrap=\"nowrap\">".$FullMark."</td></tr>";
			}

			# Check if no cell is available in the parent subject
			$CellNumber = $libreportcard->CHECK_REPORT_CELL_NULL($ReportID, $SubjectID);
			if($IsParent==1 && $DefaultSetting=="S" && ($CellNumber==0 || $ResultCalculationType==1))
			{
				$NoCellParent = 1;
				$MarkType = 1;
			}

			if($DefaultSetting!="S")
			{
				# Get the result array
				$ResultSelectonArray = ($DefaultSetting=="G") ? $libreportcard->GET_SCHEME_GRADE($GradingSchemeID) :  $libreportcard->GET_SCHEME_PASS_FAIL($GradingSchemeID);
			}
			else if($MarkType==1)
			{
				# Get the result array for grade
				$ResultSelectonArray = $libreportcard->GET_SCHEME_GRADE($GradingSchemeID);

				$GradeMarkArray = $libreportcard->GET_GRADEMARK_BY_SCHEMEID($GradingSchemeID);
				$LowestGrade = $GradeMarkArray[sizeof($GradeMarkArray)-1][1];
			}
			$ResultSelectionList = (!empty($ResultSelectonArray)) ? implode(",", $ResultSelectonArray) : "";

			# Get ReportSubjectName
			$ReportSubject = $libreportcard->GET_REPORT_SUBJECT_ID($ReportID, $SubjectID);
			$ReportSubjectID = $ReportSubject[0];

			# Get Column Array and Student Array
			if(!($IsParent==1 && $ResultCalculationType==1))
			{
				$ColumnArray = $libreportcard->GET_REPORT_TEMPLATE_COLUMN($ReportID);
				$ColumnSize = sizeof($ColumnArray);
			}
			$StudentArray = $libreportcard->GET_STUDENT_BY_CLASS($ClassID, $TargetStudentIDList);
			$StudentSize = sizeof($StudentArray);

			if(!empty($TargetStudentIDList))
			{
				$StudentIDList = $TargetStudentIDList;
			}
			else
			{
				for($i=0; $i<$StudentSize; $i++)
				{
					$StudentIDArray[] = $StudentArray[$i][0];
				}
				if(!empty($StudentIDArray))
					$StudentIDList = implode(",", $StudentIDArray);
			}

			$MarksheetTable = "<table width='100%' border='0' cellspacing='0' cellpadding='5'>";
			$MarksheetTable .= "<tr>
								<td width='15%' class='tabletopnolink tablegreentop'>".$i_ClassNumber."</td>
								<td class='tabletopnolink tablegreentop'>".$i_identity_student."</td>";

			for($i=0; $i<$ColumnSize; $i++)
			{
				list($ReportColumnID, $ColumnTitle, $Weight, $ShowPosition, $PositionRange, $IsColumnSum, $MarkSummaryWeight, $NotForInput) = $ColumnArray[$i];
				// ReportColumnID, ColumnTitle, Weight, ShowPosition, PositionRange, IsColumnSum, MarkSummaryWeight
				$ReportColumnWeightArray[$ReportColumnID] = $Weight;
				$ReportColumnTitleArray[] = trim($ColumnTitle);

				$ColumnTitle = (($Weight!="")&&(!$IsColumnSum)) ? $ColumnTitle."<br />".$Weight."%" : $ColumnTitle;
				$ColumnTitle .= ($DefaultSetting=="S" && $MarkType==0) ? "<br />".$linterface->GET_SMALL_BTN($eReportCard['SubMS'], "button", "javascript:newWindow('sub_marksheet.php?SubjectID=$SubjectID&ClassID=$ClassID&ReportColumnID=$ReportColumnID&TargetStudentIDList=$TargetStudentIDList&ParentSubjectID=$ParentSubjectID&ReportID=$ReportID', 10)") : "";
				$MarksheetTable .= "<td width='12%' align='center' class='tabletopnolink tablegreentop'>".$ColumnTitle."</td>";
				$ReportColumnIDArray[] = $ReportColumnID;
			}
			if(!empty($ReportColumnIDArray))
				$ReportColumnIDList = implode(",", $ReportColumnIDArray);

			# Get Report Cell Setting
			$ReportCell = $libreportcard->GET_REPORT_CELL_BY_SUBJECT_COLUMN($ReportSubjectID, $ReportColumnIDList);
			//debug_r($ReportCell);
			if(!($DefaultSetting=="S" && $MarkType==0))
			{
				if(!($DefaultSetting!="S" && $ColumnSize==1))
				{
					$MarksheetTable .= "<td width='12%' align='center' class='tabletopnolink tablegreentop'>".$eReportCard['OverallResult']."</td>";
				}
			}
			$MarksheetTable .= "</tr>";

			##########################################################################################
			# Generate Marksheet Table
			//$ReportType = $libreportcard->GET_REPORT_TYPE($ReportID);

			# Check whehter previous result can be retrieved
			$AllowRetrievePrevious = 0;
			$SemesterArray = $libreportcard->GET_SEMESTER_ARRAY();
			$FirstSemester = $SemesterArray[0];
			if(($libreportcard->Semester=="FULL" || $libreportcard->Semester!=$FirstSemester) && $MarkType==0)
			{
				for($i=0; $i<sizeof($ReportColumnTitleArray); $i++)
				{
					$tmp_title = $ReportColumnTitleArray[$i];
					if(in_array($tmp_title, $SemesterArray))
					{
						$AllowRetrievePrevious = 1;
						break;
					}
				}
			}
			/////////////////
			if($ComponentCalculate==1)	# Calculate score from component subjects
			{
				$MarksheetResult = $libreportcard->GET_MARKSHEET_SCORE_FROM_COMPONENT($SubjectID, $ReportID, $ClassID, $StudentIDList);
			}
			else if($CalParentOverall==1)	# Calculate overall score of parent subject from component subjects
			{
				if($ResultCalculationType==1)
				{
					$OverallResultArray = $libreportcard->GET_OVERALL_RESULT_FROM_COMPONENT_2($SubjectID, $ReportID, $ClassID, $StudentIDList);
				}
				else
				{
					# ****** $ResultCalculationType should be 1 now, not in use ******
					#$CalculatedMarksheetResult = $libreportcard->GET_MARKSHEET_SCORE_FROM_COMPONENT($SubjectID, $ReportID, $ClassID, $StudentIDList);
					#$OverallResultArray = $libreportcard->GET_OVERALL_RESULT_FROM_COMPONENT_1($CalculatedMarksheetResult, $ReportColumnWeightArray);
				}
			}
			else	# get marksheet scores
			{
				$MarksheetResult = $libreportcard->GET_MARKSHEET_SCORE($ReportID, $SubjectID, $StudentIDList, $DefaultSetting, $MarkType, $AllowRetrievePrevious, $GetAllPrevious);
			}
			
			if($MarkType==1)
			{
				if($NoCellParent==1)
				{
					$CalComponentMarkBtn = $linterface->GET_SMALL_BTN($eReportCard['CalOverallComponentResult'], "button", "javascript:self.location.href='marksheet_edit2.php?$param&CalParentOverall=1'");
				}
				else
				{
					$RawMarksheetResult = $libreportcard->GET_MARKSHEET_SCORE($ReportID, $SubjectID, $StudentIDList, $DefaultSetting, 0, $AllowRetrievePrevious);
					//$CalRawMarkBtn = $linterface->GET_SMALL_BTN($eReportCard['CalWeightedMark'], "button", "javascript:self.location.href='marksheet_edit2.php?$param&MarkType=1&RawCalculate=1'");
					
					if(empty($MarksheetResult))
						$RawCalculate = 1;
				}
			}
			else if($MarkType!=2 && $IsParent==1 && $DefaultSetting=="S")	# Generate button for calculate mark from component subject(s)
			{
				$CalComponentMarkBtn = $linterface->GET_SMALL_BTN($eReportCard['CalComponentMark'], "button", "javascript:self.location.href='marksheet_edit2.php?$param&ComponentCalculate=1'");
			}

			if($AllowRetrievePrevious==1)
			{
				$RetrievePreviousResultBtn = $linterface->GET_SMALL_BTN($eReportCard['GetPreviousSemesterResult'], "button", "javascript:self.location.href='marksheet_edit2.php?$param&GetAllPrevious=1'");
			}

			for($j=0; $j<$StudentSize; $j++)
			{
				list($StudentID, $WebSAMSRegNo, $Class, $StudentName) = $StudentArray[$j];
				$td_style = ($j%2==1) ? "tablegreenrow2" : "tablegreenrow1";

				$MarksheetTable .= "<tr>";
				$MarksheetTable .= "<td class='tabletext $td_style' nowrap='nowrap'>".$Class."</td>";
				$MarksheetTable .= "<td class='tabletext $td_style' nowrap='nowrap'>".$StudentName."</td>";

				$MarkArray = array();
				$TotalPercent = 0;
				$OverallMark = 0;

				$TextColumnCount = 0;

				for($i=0; $i<$ColumnSize; $i++)
				{
					//list($RCID, $ColumnTitle, $Weight) = $ColumnArray[$i];
					list($RCID, $ColumnTitle, $Weight, $ShowPosition, $PositionRange, $IsColumnSum, $MarkSummaryWeight, $NotForInput) = $ColumnArray[$i];
					$CellSetting = $ReportCell[$RCID];

					$FieldName = "mark_".$StudentID."_".$RCID;
					$FieldID = "mark[$j][$TextColumnCount]";
					$InputField = "";

					//if($CellSetting=="N/A")
					if(false)
					{
						$InputField = "<span class='textboxtext'>N/A</span>";
						$InputField .= "<input type='hidden' name='$FieldName' id='$FieldID' value='N/A' />";
						$MarksheetTable .= "<td align='center' class='tabletext $td_style'>".$InputField."</td>";
					} else
					{
						if($MarkType==2)
						{
							$MarksheetTable .= "<td align='center' class='tabletext $td_style'>".($MarksheetResult[$StudentID][$RCID]==""?"--":$MarksheetResult[$StudentID][$RCID])."</td>";
						}
						else
						{
							if($RawCalculate==1) {
								$RawMark = $RawMarksheetResult[$StudentID][$RCID];
								$MarkLong = ($RawMark>=0) ? $RawMark*$Weight/100 : $RawMark;
							}
							else {
								$MarkLong = $MarksheetResult[$StudentID][$RCID];
							}

							if ($CellSetting=="G" || $CellSetting=="PF")
							{
								$Mark = $MarkLong;
								if ($Disabled) {
									$InputField = $Mark."<input type='hidden' name='$FieldName' id='$FieldID' value='".$Mark."'/>";
								} else {
									//$InputField = ($InputMode==1) ? "<input class='textboxnum' maxlength='6' type='text' name='$FieldName' id='$FieldID' value='".$Mark."' onPaste=\"pasteTable('$j', '$i');\" onblur=\"if (!setStart) { this.value=startContent; setStart = 1}\" />" : $libreportcard->GET_MARKSHEET_RESULT_SELECTION($ResultSelectonArray, $FieldName, $Mark);
									$InputField = ($InputMode==1) ? "<input class='textboxnum' maxlength='6' type='text' name='$FieldName' id='$FieldID' value='".$Mark."' onPaste=\"pasteTable('$j', '$TextColumnCount');\" onblur=\"if (!setStart) { this.value=startContent; setStart = 1}\" />" : $libreportcard->GET_MARKSHEET_RESULT_SELECTION($ResultSelectonArray, $FieldName, $Mark);
								}
							}
							else if($CellSetting=="S")
							{
								$Mark = ($MarkLong>0) ? round($MarkLong, 2) : $MarkLong;
								if($Mark!="-2" && !($ReportCardTemplate==2 && $Mark=="-1"))
								{
									$TotalPercent = $TotalPercent + $Weight;
									if($Mark>=0) {
										$MarkArray[] = $MarkLong;
									}
								}
								if($Mark<0) {
									if ($Mark == -1)
									{
										$Mark = ($libreportcard->SpecialMarkArray[-1]!="") ? $libreportcard->SpecialMarkArray[-1] : $libreportcard->SpecialMarkArray["-1"];
									}
									if ($Mark == -2)
									{
										$Mark = ($libreportcard->SpecialMarkArray[-2]!="") ? $libreportcard->SpecialMarkArray[-2] : $libreportcard->SpecialMarkArray["-2"];
									}

									$MarkLong = $Mark;
								}
								if ($Disabled) {
									$InputField = $Mark."<input type='hidden' name='{$FieldName}' id='{$FieldID}' value='".$Mark."'/>";
								} else {
									//$InputField = "<input class='textboxnum' maxlength='6' type='text' name='{$FieldName}' id='{$FieldID}' value='".$Mark."' onPaste=\"pasteTable('$j', '$i');\" onblur=\"if (!setStart) { this.value=startContent; setStart = 1}\" />";
									$InputField = "<input class='textboxnum' maxlength='6' type='text' name='{$FieldName}' id='{$FieldID}' value='".$Mark."' onPaste=\"pasteTable('$j', '$TextColumnCount');\" onblur=\"if (!setStart) { this.value=startContent; setStart = 1}\" />";

								}

								# save the mark before round up
								$LongFieldName = "mark_long_".$StudentID."_".$RCID;
								$InputField .= "<input type='hidden' name='{$LongFieldName}' value='".$MarkLong."' >";
							}

							if ($NotForInput) {

								$Mark = ($MarkLong>0) ? round($MarkLong, 2) : $MarkLong;
								if($Mark!="-2" && !($ReportCardTemplate==2 && $Mark=="-1"))
								{
									$TotalPercent = $TotalPercent + $Weight;
									if($Mark>=0) {
										$MarkArray[] = $MarkLong;
									}
								}
								if($Mark<0) {
									/*
									if ($Mark == -1) $Mark = $libreportcard->SpecialMarkArray["-1"];
									if ($Mark == -2) $Mark = $libreportcard->SpecialMarkArray["-2"];
									*/
									if ($Mark == -1)
									{
										$Mark = ($libreportcard->SpecialMarkArray[-1]!="") ? $libreportcard->SpecialMarkArray[-1] : $libreportcard->SpecialMarkArray["-1"];
									}
									if ($Mark == -2)
									{
										$Mark = ($libreportcard->SpecialMarkArray[-2]!="") ? $libreportcard->SpecialMarkArray[-2] : $libreportcard->SpecialMarkArray["-2"];
									}
									$MarkLong = $Mark;
								}
								$TempDisplay = $Mark;
								if ($Mark == "") $TempDisplay = "N/A";
								$InputField = $TempDisplay."<input type='hidden' name='{$FieldName}' id='{$FieldID}' value='".$Mark."'/>";

								# save the mark before round up
								$LongFieldName = "mark_long_".$StudentID."_".$RCID;
								$InputField .= "<input type='hidden' name='{$LongFieldName}' value='".$MarkLong."' >";

							}

							if ($IsColumnSum){
								if(trim($CellSetting)=="S")
								{
									$InputField = "<span class='textboxtext'>N/A</span>";
								} else {
									$Mark = $MarkLong;
									if ($Disabled) {
										$InputField = $Mark."<input type='hidden' name='$FieldName' id='$FieldID' value='".$Mark."'/>";
									} else {
										//$InputField = ($InputMode==1) ? "<input class='textboxnum' maxlength='6' type='text' name='$FieldName' id='$FieldID' value='".$Mark."' onPaste=\"pasteTable('$j', '$i');\" onblur=\"if (!setStart) { this.value=startContent; setStart = 1}\" />" : $libreportcard->GET_MARKSHEET_RESULT_SELECTION($ResultSelectonArray, $FieldName, $Mark);
										$InputField = ($InputMode==1) ? "<input class='textboxnum' maxlength='6' type='text' name='$FieldName' id='$FieldID' value='".$Mark."' onPaste=\"pasteTable('$j', '$TextColumnCount');\" onblur=\"if (!setStart) { this.value=startContent; setStart = 1}\" />" : $libreportcard->GET_MARKSHEET_RESULT_SELECTION($ResultSelectonArray, $FieldName, $Mark);
									}
								}
							}

							$MarksheetTable .= "<td align='center' class='tabletext $td_style'>".$InputField."</td>";
						}
					}
					if (!$IsColumnSum) $TextColumnCount++;
				}

				$OverallFieldName = "overall_".$StudentID;
				$OverallFieldID = "mark[$j][$i]";
				
				if($MarkType>0)
				{
					$i++;
					if($MarkType==2)
					{
						$MarksheetTable .= "<td align='center' class='tabletext $td_style'>".($MarksheetResult[$StudentID]["Overall"]==""?"--":$MarksheetResult[$StudentID]["Overall"])."</td>";
					}
					else
					{
						if($RawCalculate==1 || $CalParentOverall==1)
						{
							if($RawCalculate==1) {
								if(!empty($MarkArray)) {
									//$OverallMarkLong = array_sum($MarkArray)*100/$TotalPercent;
									//$OverallMarkLong = array_sum($MarkArray); // DR down-right calculation
									//$OverallMark = ($OverallMarkLong>0) ? round($OverallMarkLong, 2) : $OverallMarkLong;

									if (($libreportcard->CalculationOrder == "Horizontal")||($libreportcard->CalculationOrder == "")||(($ParentSubjectID == "")||($ParentSubjectID != $SubjectID))) {
										$OverallMarkLong = array_sum($MarkArray); // DR down-right calculation
										if ($ReportCardTemplate==2 && $TotalPercent>0 && $TotalPercent!="")
										{
											//$OverallMarkLong = $OverallMarkLong * (100/$TotalPercent);
										}
										$OverallMark = ($OverallMarkLong>0) ? round($OverallMarkLong, 2) : $OverallMarkLong;
									} else {
										// get overall mark from db and add it up
										if ($IsParent) {
											$OverallMark = $libreportcard->PARENT_GET_OVERALL_FROM_COMPONENT($SubjectID, $ReportID, $ClassID, $StudentID, $ParentSubjectID);
										} else {
											$OverallMark = $libreportcard->GET_OVERALL_FROM_COMPONENT($SubjectID, $ReportID, $ClassID, $StudentID, $ParentSubjectID);
										}
									}
									
									/*
									if ($libreportcard->CalculationOrder == "Horizontal") {
										$OverallMarkLong = array_sum($MarkArray); // DR down-right calculation
										$OverallMark = ($OverallMarkLong>0) ? round($OverallMarkLong, 2) : $OverallMarkLong;
									} else {
										// get overall mark from db and add it up

									}
									*/
								}
								else {
									$OverallMarkLong = "/";
									$OverallMark = $OverallMarkLong;
								}
							}
							else {
								// get from databases, no need to calculate
								$OverallMarkLong = $OverallResultArray[$StudentID];
								$OverallMark = ($OverallMarkLong>0) ? round($OverallMarkLong, 2) : $OverallMarkLong;
							}
						}
						else
						{
							// get from databases, no need to calculate
							$OverallMarkLong  = $MarksheetResult[$StudentID]["Overall"];
							$OverallMark  = ($OverallMarkLong>0) ? round($OverallMarkLong, 2) : $OverallMarkLong;
							//hdebug('hereeee', $OverallMark);
						}

						if($OverallMark<0) {
							// no need to calculate
							$OverallMark  = $libreportcard->SpecialMarkArray[trim($OverallMark)];
							$OverallMarkLong = $OverallMark;
						}
						
						$MarksheetTable .= "<td align='center' class='tabletext $td_style'>";
						if ($Disabled) {
							$MarksheetTable .= $OverallMark."<input type='hidden' name='$OverallFieldName' id='$OverallFieldID' value='".$OverallMark."'/>";
						} else {
							//$MarksheetTable .= "<input class='textboxnum' maxlength='6' type='text' name='$OverallFieldName' id='$OverallFieldID' value='".$OverallMark."' onPaste=\"pasteTable('$j', '$i');\" onblur=\"if (!setStart) { this.value=startContent; setStart = 1}\" />";
							$MarksheetTable .= "<input class='textboxnum' maxlength='6' type='text' name='$OverallFieldName' id='$OverallFieldID' value='".$OverallMark."' onPaste=\"pasteTable('$j', '$TextColumnCount');\" onblur=\"if (!setStart) { this.value=startContent; setStart = 1}\" />";
						}
						$MarksheetTable .= "</td>";

						# mark before round up
						$OverallLongFieldName = "overall_long_".$StudentID;
						$MarksheetTable .= "<input type='hidden' name='{$OverallLongFieldName}' value='".$OverallMarkLong."' >";
					}
				}
				else if($DefaultSetting!="S" && $ColumnSize>1)
				{
					$OverallMark  = $MarksheetResult[$StudentID]["Overall"];

					if ($Disabled) {
						$InputField = $OverallMark."<input type='hidden' name='$OverallFieldName' id='$OverallFieldID' value='".$OverallMark."' />";
					} else {
						//$InputField = ($InputMode==1) ? "<input class='textboxnum' maxlength='6' type='text' name='$OverallFieldName' id='$OverallFieldID' value='".$OverallMark."' onPaste=\"pasteTable('$j', '$i');\" onblur=\"if (!setStart) { this.value=startContent; setStart = 1}\" />" : $libreportcard->GET_MARKSHEET_RESULT_SELECTION($ResultSelectonArray, $OverallFieldName, $OverallMark, 1);
						$InputField = ($InputMode==1) ? "<input class='textboxnum' maxlength='6' type='text' name='$OverallFieldName' id='$OverallFieldID' value='".$OverallMark."' onPaste=\"pasteTable('$j', '$TextColumnCount');\" onblur=\"if (!setStart) { this.value=startContent; setStart = 1}\" />" : $libreportcard->GET_MARKSHEET_RESULT_SELECTION($ResultSelectonArray, $OverallFieldName, $OverallMark, 1);
					}
//asdf
					$MarksheetTable .= "<td align='center' class='tabletext $td_style'>".$InputField."</td>";
				}
				$MarksheetTable .= "</tr>";
			}
			$MarksheetTable .= "</table>";

			if(($MarkType==0)&&(!($IsMSSubmissionPeriod &&($ck_ReportCard_UserType!="ADMIN"))))
			{
				$ImportButton = $linterface->GET_SMALL_BTN($button_import, "button", "javascript:self.location.href='import.php?$param&MarkType=$MarkType&DefaultSetting=$DefaultSetting'")."&nbsp;";
			}
			$MarkRemindRow = "<tr><td align='left' class='tabletextremark' colspan=2>".$eReportCard['MarkRemind']."</td></tr>";

			$x = $MarkRemindRow;
			$x .= "<tr><td colspan='1' align='left'>".$ImportButton;
			$x .= $linterface->GET_SMALL_BTN($button_export, 'button', "javascript:self.location.href='export_update.php?$param&MarkType=$MarkType'");

			if(!empty($CalRawMarkBtn))
				$x .= "&nbsp;".$CalRawMarkBtn;
			if((!empty($CalComponentMarkBtn)))
				$x .= "&nbsp;".$CalComponentMarkBtn;
			if(!empty($RetrievePreviousResultBtn))
				$x .= "&nbsp;".$RetrievePreviousResultBtn;

			$x .= "</td>";
			$x .= "</tr>";
			$x .= "<tr><td align='right' nowrap='nowrap'><a class='tablegreenlink' href='#bottom'>".$eReportCard['GoToBottom']."</a></td></tr>";
			$x .= "<tr><td valign='top' nowrap='nowrap' colspan='2'>".$MarksheetTable."</td></tr>
					<tr><td align='right' colspan='2' nowrap='nowrap'><a class='tablegreenlink' href='#top'>".$eReportCard['GoToTop']."</a></td></tr>";

			$x .= $MarkRemindRow;

			$ButtonRow = "<tr><td align='center' id='bottom'>";
			if(($MarkType==0)&&(!(!$IsMSSubmissionPeriod &&($ck_ReportCard_UserType!="ADMIN"))))
			{
				//$ButtonRow .= $linterface->GET_ACTION_BTN($button_submit, "submit", "javascript:document.getElementById('ToIndex').value=1", "btnSubmit");
				$ButtonRow .= $linterface->GET_ACTION_BTN($button_submit, "button", "javascript:jCHANGE_MARK_TYPE(0)", "btnSubmit");
				$ButtonRow .= "&nbsp;";
				$ButtonRow .= $linterface->GET_ACTION_BTN($button_reset, "button", "javascript:jRESET_FORM()");
				$ButtonRow .= "&nbsp;";
			}
			$ButtonRow .= ($HaveComponent) ? $linterface->GET_ACTION_BTN($button_back, "button", "javascript:self.location.href='marksheet_edit1.php?$param'") : $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:self.location.href='$BackPage'");
			$ButtonRow .= "</td></tr>";
		}
		else
		{
			$x = "<tr><td align='center' nowrap='nowrap' colspan='2' class='tabletext'>".$eReportCard['NoReportBuiltMsg']."</td></tr>";
			$ButtonRow = "<tr><td align='center' id='bottom'>";
			$ButtonRow .= $linterface->GET_ACTION_BTN($button_back, "button", "javascript:self.location.href='$BackPage'");
			$ButtonRow .= $BackButton."</td></tr>";
		}
################################################################
# tag information
if($DefaultSetting=="S")
{
	if($NoCellParent==1)
	{
		$TAGS_OBJ[] = array($TagName, "javascript:jCHANGE_MARK_TYPE(1)", 1);
	}
	else
	{
		${"link".$MarkType} = 1;
		if ($IsMSSubmissionPeriod &&($ck_ReportCard_UserType!="ADMIN")) {
			$TAGS_OBJ[] = array($eReportCard['RawMarks'], "", $link0);
		} else {
			$TAGS_OBJ[] = array($eReportCard['RawMarks'], "javascript:jCHANGE_MARK_TYPE(0)", $link0);
			$TAGS_OBJ[] = array($eReportCard['WeightedMarks'], "javascript:jCHANGE_MARK_TYPE(1)", $link1);
			$TAGS_OBJ[] = array($eReportCard['ConvertedGrade'], "javascript:jCHANGE_MARK_TYPE(2)", $link2);
		}
	}
}
else
{
	${"link".$InputMode} = 1;
	$TAGS_OBJ[] = array($eReportCard['SelectionListMode'], "javascript:jCHANGE_INPUT_MODE(0)", $link0);
	$TAGS_OBJ[] = array($eReportCard['TextInputMode'], "javascript:jCHANGE_INPUT_MODE(1)", $link1);
}

if($HaveComponent)
{
	$STEPS_OBJ[] = array($eReportCard['SelectSubject'], 0);
	$STEPS_OBJ[] = array($eReportCard['SubmitMarksheet'], 1);
	$StepRow .= "<tr><td>".$linterface->GET_STEPS($STEPS_OBJ)."</td></tr>";
}

$TotalColumnSize = ($MarkType==1) ? $ColumnSize+1 : $ColumnSize;

$linterface->LAYOUT_START();
?>
<script language="javascript">
var rowx = 0, coly = 0;		//init
var xno = "<?=$TotalColumnSize?>";yno = "<?=$StudentSize?>";		// set table size

var startContent = "";
var setStart = 1;

function setxy(jParX, jParY){
	rowx = jParX;
	coly = jParY;
	document.getElementById('mark['+ jParX +']['+ jParY +']').blur();
}

function paste_clipboard() {

	var temp1 = null;
	var text1 = null;
	var s = "";

	// you must use a 'text' field
	// (a 'hidden' text field will not show anything)

	temp1 = document.getElementById('text1').createTextRange();
	temp1.execCommand( 'Paste' );

	s = document.getElementById('text1').value; // clipboard is pasted to a string here
	setStart = 0;

	document.getElementById('mark['+ (rowx) +']['+ (coly) +']').value = "";

	var row_array = s.split("\n");
	var row_num = 0;

	while (row_num < row_array.length)
	{
		var col_array = row_array[row_num].split("\t");
		var col_num = 0;
		var xPos=0, yPos=0;
		while (col_num < col_array.length)
		{
			if ((col_num == 0) && (row_num == 0)) startContent = col_array[0];
			if ((row_array[row_num] != "")&&(col_array[col_num] != ""))
			{
				var temp = document.getElementById('mark['+ (row_num + rowx) +']['+ (col_num + coly) +']');
				xPos = parseInt(row_num) + parseInt(rowx);
				yPos = parseInt(col_num) + parseInt(coly);
				if (document.getElementById('mark['+ xPos +']['+ yPos +']') != null)
				{
					document.getElementById('mark['+ xPos +']['+ yPos +']').value = col_array[col_num];
				}
			}
			col_num+=1;
		}
		row_num+=1;
	}
}

function pasteTable(jPari, jParj) {
	setxy(jPari, jParj);
	paste_clipboard();
}

function jCHANGE_MARK_TYPE(jNewType)
{
	var obj = document.form1;

	if(jNewType==1) obj.ViewOnly.value = 1;
	if(jNewType > 0) obj.ToCalculate.value = 1;

	if(obj.MarkType.value > 0)
	{
		obj.MarkType.value = jNewType;
		obj.NewMarkType.value = jNewType;
		obj.action = "marksheet_edit2.php";
		obj.submit();
	}
	else
	{
		obj.NewMarkType.value = jNewType;
		obj.MarkTypeChanged.value = 1;
		if(confirm("<?=$eReportCard['SubmitMarksheetConfirm']?>"))
		{
			if(checkform(obj))
			{
				obj.action = "marksheet_edit_update.php";
				obj.submit();
			}
		}
	}
}

function jCHANGE_INPUT_MODE(jInputType)
{
	obj = document.form1;

	obj.InputMode.value = jInputType;
	if(confirm("<?=$eReportCard['ChangeInputModeConfirm']?>"))
	{
		obj.action = "marksheet_edit2.php";
		obj.submit();
	}
}

function jRESET_FORM()
{
	obj = document.form1;
	obj.action = "marksheet_edit2.php";
	obj.submit();
}

function checkform(obj){

   	var jDefaultSetting = obj.DefaultSetting.value;
	var jInputMode = obj.InputMode.value;

	if(jDefaultSetting=="S" || (jDefaultSetting!="S" && jInputMode==1))
	{
		var jStudentList = obj.StudentIDList.value;
		var jStudentIDArray = jStudentList.split(",");

		var jReportColumnList = obj.ReportColumnIDList.value;
		var jReportColumnArray = jReportColumnList.split(",");
		var jValidResult = false;

		if(jDefaultSetting!="S") {
			var jResultSelectionList = obj.ResultSelectionList.value;
			var jResultSelectionArray = jResultSelectionList.split(",");
		}
		else {
			var jFullMark = obj.FullMark.value;
		}

		for(var i=0; i<jStudentIDArray.length; i++)
		{
			jSID = jStudentIDArray[i];
			for(var j=0; j<jReportColumnArray.length; j++)
			{
				jRCID = jReportColumnArray[j];
				if (eval("document.getElementById('obj.mark_"+jSID+"_"+jRCID+"')") != null) {
					jValue = eval("obj.mark_"+jSID+"_"+jRCID+".value");

					if(jValue!="" && jValue!="/" && jValue!="abs")
					{
						//alert(jValue);
						if(jDefaultSetting=="S")
						{
							jValidResult = jIS_NUMERIC(jValue);
							if(jValidResult==true) {
								jValidResult = (jValue>=0 && (jFullMark-jValue>=0)) ? true : false;
							}
						}
						else
							jValidResult = tempjIN_ARRAY(jResultSelectionArray, jValue);

						if(jValidResult==false)
						{
							alert("<?=$eReportCard['InvalidInputMsg']?>");
							eval("obj.mark_"+jSID+"_"+jRCID+".focus()");
							return false;
						}
					}
				}
			}
		}
	}

	obj.ViewOnly.value = 1;
	obj.ToCalculate.value = 1;
	if (obj.NewMarkType.value == 0) {
		obj.NewMarkType.value = 1;
		obj.BackToZero.value = 1;
	}
	//obj.NewMarkType.value = 1;
	obj.MarkTypeChanged.value = 1;

	return true;
}

</script>

<form name="form1" action="marksheet_edit_update.php" method="post" onSubmit="return checkform(this)">
<br />
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td id="top"><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
</tr>
<?=$StepRow?>
<tr>
	<td>
		<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tr><td align="right" colspan="2"><?= $linterface->GET_SYS_MSG($Result)?></td></tr>
		<tr>
			<td colspan="2">
			<table width='100%' border='0' cellpadding="3" cellspacing="1">
			<tr><td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $eReportCard['ClassName']; ?></td><td class="tabletext" width="70%" nowrap="nowrap"><?=$ClassName?></td></tr>
			<tr><td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=($intranet_session_language=="en"?$eReportCard['EngSubject']:$eReportCard['ChiSubject']); ?></td><td class="tabletext" width="70%" nowrap="nowrap"><?=$SubjectName?></td></tr>
			<?=$FullMarkRow?>
			</table>
			</td>
		</tr>
		<tr><td class="dotline" colspan="2"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width=10 height=1></td></tr>
		<?=$x?>
		</table>
	</td>
</tr>
<tr>
	<td><table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
	<?=$ButtonRow?>
	</table>
	</td>
</tr>
</table>

<textarea style="height: 1px; width: 1px; visibility:hidden;" id="text1" name="text1"></textarea>

<input type="hidden" name="ReportID" value="<?=$ReportID?>" />
<input type="hidden" name="SubjectID" value="<?=$SubjectID?>" />
<input type="hidden" name="ClassID" value="<?=$ClassID?>" />
<input type="hidden" name="ClassLevelID" value="<?=$ClassLevelID?>" />
<input type="hidden" name="StudentIDList" value="<?=$StudentIDList?>" />
<input type="hidden" name="TargetStudentIDList" value="<?=$TargetStudentIDList?>" />
<input type="hidden" name="ReportColumnIDList" value="<?=$ReportColumnIDList?>" />
<input type="hidden" name="DefaultSetting" value="<?=$DefaultSetting?>" >
<input type="hidden" name="HaveComponent" value="<?=$HaveComponent?>" />
<input type="hidden" name="IsParent" value="<?=$IsParent?>" />
<input type="hidden" name="ParentSubjectID" value="<?=$ParentSubjectID?>" />
<input type="hidden" name="GradingSchemeID" value="<?=$GradingSchemeID?>" />
<input type="hidden" name="MarkType" value="<?=$MarkType?>" />
<input type="hidden" name="ResultSelectionList" value="<?=$ResultSelectionList?>" />
<input type="hidden" name="FullMark" value="<?=$FullMark?>" />
<input type="hidden" name="InputMode" value="<?=$InputMode?>" />
<input type="hidden" name="MarkTypeChanged" value="<?=$MarkTypeChanged?>"/>
<input type="hidden" name="NewMarkType" value="<?=$NewMarkType?>" />
<input type="hidden" name="ViewOnly" />
<input type="hidden" name="RawCalculate" />
<input type="hidden" name="ToCalculate" />
<input type="hidden" name="ToIndex" id="ToIndex" />
<input type="hidden" name="BackToZero" value="<?=$BackToZero?>"/>
<input type="hidden" name="ResultCalculationType" value="<?=$ResultCalculationType?>" />

<? if (($ToCalculate)&&($MarkType!=2)) { ?>
<script language='javascript'>
	document.form1.ViewOnly.value = 1;
	document.form1.MarkTypeChanged.value = 1;
	document.form1.ToCalculate.value = 0;
	if (document.form1.BackToZero.value == 1) {
		document.form1.BackToZero.value = 0;
		document.form1.NewMarkType.value = 0;
	}
	document.form1.action = "marksheet_edit_update.php";
	document.form1.submit();
</script>
<? } ?>

<?php
if ($AlertToSave)
{
	echo "<script language='javascript'>\n alert(\"Please check the weighted marks and submit to save.\"); \n</script>";
}
?>

<p></p>
</form>
<?
	//echo(StopTimer());

		$linterface->LAYOUT_STOP();
		intranet_closedb();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>