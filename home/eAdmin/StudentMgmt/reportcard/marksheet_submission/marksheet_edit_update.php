<?php
$PATH_WRT_ROOT = "../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libreportcard.php");
intranet_auth();
intranet_opendb();

$li = new libdb();
$libreportcard = new libreportcard();


# Get student with ClassName and ClassNumber
$StudentClassArray = $libreportcard->GET_CLASS_BY_USERID($StudentIDList);

$ColumnArray = $libreportcard->GET_REPORT_TEMPLATE_COLUMN($ReportID);
$ColumnSize = sizeof($ColumnArray);


if(!empty($StudentClassArray) && !empty($ColumnArray))
{
	$Year = $libreportcard->Year;
	$Semester = $libreportcard->Semester;

	if ($MarkTypeChanged==1) # Save previous marksheet result (raw / weighted)
	{
		if($MarkType==1) {
			$IsWeighted = 1;
		}
		else if($MarkType==0) {
			$IsWeighted = 0;
		}
	}
	else
	{
		if($DefaultSetting=="S")
		{
			if($MarkType==1) {
				$IsWeighted = 1;
			}
			else {
				$IsWeighted = 0;
				# retrieve saved weighted marks
				/* disabled by Yuen
				$WeightedResult = $libreportcard->GET_MARKSHEET_SCORE($ReportID, $SubjectID, $StudentIDList, $DefaultSetting, 1);
				$SaveWeightedMark = (empty($WeightedResult)) ? 1 : 0;
				*/
			}
		}
	}
	

//debug("$DefaultSetting, $IsWeighted, $SaveWeightedMark");
	# get grade mark
	$GradeMarkArray = $libreportcard->GET_GRADEMARK_BY_SCHEMEID($GradingSchemeID);
	
	$LastIndex = sizeof($GradeMarkArray)-1;
	$LowestGrade = $GradeMarkArray[$LastIndex][1];

	foreach($StudentClassArray as $SID => $ClassInfo)
	{
		list($ClassName, $ClassNumber) = $ClassInfo;

		if(!($ResultCalculationType==1 && $IsParent==1))
		{
			$TotalPercent = 0;
			$WeightedMarkArray = array();
			$TotalMarkArray = array();
			$TotalWeightArray = array();
			for($j=0; $j<sizeof($ColumnArray); $j++)
			{
				# added on 4 Dec 2008 by Ivan
				# if it is the second round of calculation and the scale input is "Grade", stop the overall mark calculation
				# otherwise, the grade input by user will be converted to zero
				if (!$BackToZero && $DefaultSetting=="G")
				{
					continue;
				}
				
				$RCID = $ColumnArray[$j][0];
				if ($DefaultSetting=="S")
				{
					$long_value = ${"mark_long_".$SID."_".$RCID};
					$short_value = ${"mark_".$SID."_".$RCID};
					$value = ($long_value==$short_value || ($long_value>0 && $short_value>0 && round($long_value, 1)==$short_value)) ? $long_value : $short_value;
				}
				else {
					$value = ${"mark_".$SID."_".$RCID};
				}

				if(strtolower($value)=="abs") {
					$value = strtolower($value);
				}
				
				# Remove Reocrds
				$sql = "DELETE FROM RC_MARKSHEET_SCORE WHERE StudentID = '$SID' AND SubjectID = '$SubjectID' AND ReportColumnID = '$RCID' AND IsWeighted='$IsWeighted' AND IsOverall = 0 AND Year = '$Year' AND Semester = '$Semester'";
				$li->db_db_query($sql);

				if($value!="N/A")
				{
					$ConvertedGrade = "";
					if($DefaultSetting=="S")
					{
						# "-1" stand for "abs"; "-2" stand for "/"
						if($value=="abs") {
							$value = "-1";
						}
						else if(strcmp($value, "/")==0 || $value==="") {
							$value = "-2";
						}
						$ConvertedGrade = $libreportcard->GET_CONVERTED_GRADE($GradeMarkArray, $LowestGrade, $value);
					}
					else
						$value = ($value==="" || empty($value)) ? "/" : $value;

					$field = "StudentID, SubjectID, ReportColumnID";
					$field .= ($DefaultSetting=="S") ? ", Mark, Grade" : ", Grade";
					$field .= ", TeacherID, IsWeighted, Year, Semester, ClassName, ClassNumber, DateInput, DateModified";
					$value_field = "'$SID', '$SubjectID', '$RCID'";
					$value_field .= ($DefaultSetting=="S") ? ", '$value', '$ConvertedGrade'" : ", '$value'";

					$value_field .= ", '$UserID', '$IsWeighted', '$Year', '$Semester', '".$ClassName."', '".$ClassNumber."', now(), now()";
					$sql = "INSERT INTO RC_MARKSHEET_SCORE({$field}) VALUES({$value_field})";
					
					$success = $li->db_db_query($sql);

					$Weight = $ColumnArray[$j][2];
					$WeightedMark = ($value>=0) ? ($value*$Weight/100) : $value;
					
					if($WeightedMark!="-2" && $WeightedMark!="-1")
					{
						$TotalPercent = $TotalPercent + $Weight;
						if($WeightedMark>=0)
							$WeightedMarkArray[] = $WeightedMark;
						$TotalMarkArray[] = $value;
						$TotalWeightArray[] = $Weight;
					}

					if($SaveWeightedMark==1)
					{
						$sql = "INSERT INTO RC_MARKSHEET_SCORE(StudentID, SubjectID, ReportColumnID, Mark, TeacherID, IsWeighted, Year, Semester, ClassName, ClassNumber, DateInput, DateModified) VALUES('$SID', '$SubjectID', '$RCID', '$WeightedMark', '$UserID', '1', '$Year', '$Semester', '".$ClassName."', '".$ClassNumber."', now(), now())";
						$li->db_db_query($sql);
					}


					/*
					if($SaveWeightedMark==1)
					{
						$Weight = $ColumnArray[$j][2];
						$WeightedMark = ($value>=0) ? ($value*$Weight/100) : $value;
						if($WeightedMark!="-2")
						{
							$TotalPercent = $TotalPercent + $Weight;
							if($WeightedMark>=0)
								$WeightedMarkArray[] = $WeightedMark;
						}
						$sql = "INSERT INTO RC_MARKSHEET_SCORE(StudentID, SubjectID, ReportColumnID, Mark, TeacherID, IsWeighted, Year, Semester, ClassName, ClassNumber, DateInput, DateModified) VALUES('$SID', '$SubjectID', '$RCID', '$WeightedMark', '$UserID', '1', '$Year', '$Semester', '".$ClassName."', '".$ClassNumber."', now(), now())";
						$li->db_db_query($sql);
					}
					*/

				}
			}
		}
		
		

		# If there are more than one column, Overall Result needed!
		# Save the overall result
		//if ($DefaultSetting=="S" || $IsWeighted==1 || $SaveWeightedMark==1)
		if ($DefaultSetting=="S" || $DefaultSetting=="G" || $IsWeighted==1 || $SaveWeightedMark==1)
		{
			
			# As only one column exist, save as overall mark
			if($DefaultSetting=="S" && $ColumnSize==1 && !($IsParent==1 && $ResultCalculationType==1)) {
				$OverallResult = $value;
			}
			else if($SaveWeightedMark==1) {
				$OverallResult = (!empty($WeightedMarkArray)) ? (array_sum($WeightedMarkArray)*100/$TotalPercent) : "-2";
			}
			else {
				if($DefaultSetting=="S") {

					$long_overall_value = ${"overall_long_".$SID};
					$short_overall_value = ${"overall_".$SID};

					$OverallResult = ($long_overall_value==$short_overall_value || round($long_overall_value, 1)==$short_overall_value) ? $long_overall_value : $short_overall_value;
					
					
					/*
					$TempMark = 0;
					for ($CalWeightedMark = 0; $CalWeightedMark < sizeof($WeightedMarkArray); $CalWeightedMark++) {
						$TempMark += $WeightedMarkArray[$CalWeightedMark];
					}
					$OverallResult = $TempMark;
					*/
					/*
					$TempMark = 0;
					if (sizeof($TotalMarkArray) == 1) {
						$OverallResult = $TotalMarkArray[0];
					} else {
						for ($CalWeightedMark = 0; $CalWeightedMark < sizeof($TotalMarkArray); $CalWeightedMark++) {
							$TempMark += $TotalMarkArray[$CalWeightedMark] * $TotalWeightArray[$CalWeightedMark] / 100;
						}
						$OverallResult = $TempMark;
					}
					*/

				}
				else {
					$OverallGrade = ${"overall_".$SID};
					$OverallResult = ${"overall_".$SID};
				}
			}
			
			
			if ($OverallResult!="" && $OverallResult>=0)
			{
				# added on 4 Dec 2008 by Ivan
				# if it is the second round of calculation and the scale input is "Grade", stop the overall mark calculation
				# otherwise, the grade input by user will be converted to zero
				if (!$BackToZero && $DefaultSetting=="G")
				{
					continue;
				}
				
				# Remove Overall Reocrds
				if($IsParent==1 && $ResultCalculationType==1)
				{
					$sql = "DELETE FROM RC_MARKSHEET_SCORE WHERE StudentID = '$SID' AND SubjectID = '$SubjectID' AND Year = '$Year' AND Semester = '$Semester'";
				}
				else
				{
					$sql = "DELETE FROM RC_MARKSHEET_SCORE WHERE StudentID = '$SID' AND SubjectID = '$SubjectID' AND IsOverall=1 AND Year = '$Year' AND Semester = '$Semester'";
				}
				$li->db_db_query($sql);

				
	/* temporarily disabled as this case seems unnecessary
				if ($IsWeighted==1 || $SaveWeightedMark==1)
				{
					$OverallResult = ($OverallResult==="" || empty($OverallResult)) ? "/" : $OverallResult;
					$ResultField = ($DefaultSetting=="S") ? "Mark" : "Grade";
					$sql = "INSERT INTO RC_MARKSHEET_SCORE(StudentID, SubjectID, $ResultField, TeacherID, IsOverall, Year, Semester, ClassName, ClassNumber, DateInput, DateModified) VALUES('$SID', '$SubjectID', '$OverallResult', '$UserID', '1', '$Year', '$Semester', '".$ClassName."', '".$ClassNumber."', now(), now())";
				} else
				{
				}
	*/
				
//				$OverallResult = (strcmp($OverallResult, "/")==0 || $OverallResult==="") ? "-2" : $OverallResult;
				# Overall result: "/" or ""
				if(strcmp($OverallResult, "/") == 0 || $OverallResult === ""){
					$OverallResult = "-2";
				} 
				# Calculate overall result
				else if($OverallResult >= 0 && $SaveWeightedMark !== 1){
					$OverallResult = ($OverallResult * 100 / $TotalPercent);
				}

				if($DefaultSetting=="S") $OverallGrade = $libreportcard->GET_CONVERTED_GRADE($GradeMarkArray, $LowestGrade, $OverallResult);
				$sql = "INSERT INTO RC_MARKSHEET_SCORE(StudentID, SubjectID, Mark, Grade, TeacherID, IsOverall, Year, Semester, ClassName, ClassNumber, DateInput, DateModified) VALUES ('$SID', '$SubjectID', '$OverallResult', '$OverallGrade', '$UserID', '1', '$Year', '$Semester', '".$ClassName."', '".$ClassNumber."', now(), now())";
				//print "asdf";
				$success = $li->db_db_query($sql);
			}
			else if ($OverallResult == '')
			{
				# Remove Overall Reocrds
				if($IsParent==1 && $ResultCalculationType==1)
				{
					$sql = "DELETE FROM RC_MARKSHEET_SCORE WHERE StudentID = '$SID' AND SubjectID = '$SubjectID' AND Year = '$Year' AND Semester = '$Semester'";
				}
				else
				{
					$sql = "DELETE FROM RC_MARKSHEET_SCORE WHERE StudentID = '$SID' AND SubjectID = '$SubjectID' AND IsOverall=1 AND Year = '$Year' AND Semester = '$Semester'";
				}
				$li->db_db_query($sql);
			}
		}
	} 
}

$Result = ($success==1) ? "update" : "update_failed";


/*
if($MarkTypeChanged==1)
{
	$param = "SubjectID=$SubjectID&ClassID=$ClassID&ReportID=$ReportID&MarkType=$NewMarkType&HaveComponent=$HaveComponent&IsParent=$IsParent&ParentSubjectID=$ParentSubjectID&TargetStudentIDList=$TargetStudentIDList";

	# proceed to calculate from RAW mark only if no weighted mark calculated before
	$sql = "SELECT count(*) from RC_MARKSHEET_SCORE WHERE SubjectID='$SubjectID' AND isweighted=1 AND Year='$Year' AND semester='$Semester' AND ClassName='$ClassName' ";
	$result = $li->returnVector($sql);
	if ($result[0]<=0)
	{
		$param .= "&RawCalculate=1";
	}
	intranet_closedb();
	header("Location:marksheet_edit2.php?$param&Result=$Result");
}
else
{

	$ReturnSubjectID = ($ParentSubjectID!="") ? $ParentSubjectID : $SubjectID;
	$BackPage = ($ck_ReportCard_UserType=="ADMIN") ? "admin_index.php?SubjectID=$ReturnSubjectID&ClassLevelID=$ClassLevelID&Result=$Result" : "index.php?Result=$Result";
intranet_closedb();
	header("Location:$BackPage");
}
*/

# proceed to calculate from RAW mark only if no weighted mark calculated before
$sql = "SELECT count(*) from RC_MARKSHEET_SCORE WHERE SubjectID='$SubjectID' AND isweighted=1 AND Year='$Year' AND semester='$Semester' AND ClassName='$ClassName' ";
$result = $li->returnVector($sql);

intranet_closedb();
if(($MarkTypeChanged==1 || (($result[0]<=0)&&($DefaultSetting == "S")))&&(!$ToIndex))
{
	if ($result[0]<=0 && $DefaultSetting != "G")
	{
		$NewMarkType = 1;
		$param .= "RawCalculate=1&AlertToSave=1&";
	}
	else if ($DefaultSetting == "G")
	{
		$Result = "update";
	}
	$param .= "SubjectID=$SubjectID&ClassID=$ClassID&ReportID=$ReportID&MarkType=$NewMarkType&HaveComponent=$HaveComponent&IsParent=$IsParent&ParentSubjectID=$ParentSubjectID&TargetStudentIDList=$TargetStudentIDList";
	$param .= "&MarkTypeChanged=$MarkTypeChanged&BackToZero=$BackToZero";
	
	if ($ViewOnly) $param .= "&ViewOnly=1&MarkTypeChanged=$MarkTypeChanged";	
	if ($ToCalculate) $param .= "&ToCalculate=1&NewMarkType=$NewMarkType";	
	
	header("Location:marksheet_edit2.php?$param&Result=$Result");
}
else
{
	$ReturnSubjectID = ($ParentSubjectID!="") ? $ParentSubjectID : $SubjectID;
	$BackPage = ($ck_ReportCard_UserType=="ADMIN") ? "admin_index.php?SubjectID=$ReturnSubjectID&ClassLevelID=$ClassLevelID&Result=$Result" : "index.php?Result=$Result";
	header("Location:$BackPage");
}
?>