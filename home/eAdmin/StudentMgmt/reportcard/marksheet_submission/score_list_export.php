<?php
$PATH_WRT_ROOT = "../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libreportcard.php");
//include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once("../default_header.php");


intranet_auth();
intranet_opendb();

$libreportcard = new libreportcard();
$limport = new libimporttext();
$lexport = new libexporttext();

global $eReportCard;
	
# Add Tidiness - Offence to header for export 
$DefaultHeaderArray[4][2][] = "TARDINESS_OFFENCE";
$HeaderLangMap[4]["TARDINESS_OFFENCE"] = $eReportCard['TardinessOffence']."(".$eReportCard['Offence'].")";

# get class object
$ClassObj = $libreportcard->GET_CLASS_OBJ($ClassID);
list($ClassID, $ClassLevelID, $ClassName) = $ClassObj;

# Get Results
if ($ReportColumnID != "") {
	$ResultArray = $libreportcard->GET_STUDENT_SUBJECT_COLUMN_RESULT($ClassID, $ReportColumnID, $IsWeighted);
} else {
	$ResultArray = $libreportcard->GET_STUDENT_SUBJECT_RESULT($ClassID);
}
//$ResultArray = $libreportcard->GET_STUDENT_SUBJECT_RESULT($ClassID);
		
# Get Student of Class
$StudentArray = $libreportcard->GET_STUDENT_BY_CLASS($ClassID);

# Get RegNo map
$StudentRegNoMap = $libreportcard->GET_STUDENT_REGNO_ARRAY($ClassLevelID, $ClassID);

# Get ReportID Class
$ReportID = $libreportcard->GET_REPORTID_BY_CLASS($ClassID);
		
# Get Report Subject 
$SubjectArray = $libreportcard->GET_REPORT_SUBJECTS($ReportID);


$ColumnArray = $libreportcard->GET_REPORT_TEMPLATE_COLUMN($ReportID);
for ($temp = 0; $temp < sizeof($ColumnArray); $temp++) {
  if($ReportColumnID == $ColumnArray[$temp][0])
  {
    $SelectedColumn = $ColumnArray[$temp];
    break;
  }
}

### 20091126 Ivan
### Scan the score list -> if the subject score of all student is empty => hide the subject
$DisplaySubjectIDAssoArr = array();
for($i=0; $i<sizeof($StudentArray); $i++)
{
    list($r_student_id, $RegNo, $class_number, $student_name) = $StudentArray[$i];
    
    if (is_array($SubjectArray) && count($SubjectArray) > 0)
    {
	    foreach($SubjectArray as $r_subject_id => $subject_name)
		{
	    	$s_mark = $ResultArray[$r_student_id][$r_subject_id]["Mark"];
			$s_grade = $ResultArray[$r_student_id][$r_subject_id]["Grade"];
			
			if ($s_mark != '' || $s_grade != '')
				$DisplaySubjectIDAssoArr[$r_subject_id] = true;
    	}
    }
}

		
# Get Report Display Settings
$ReportInfo = $libreportcard->GET_REPORT_TEMPLATE($ReportID);
$Settings = $ReportInfo[4];
$SettingArray = unserialize($Settings);
$MarkTypeDisplay = $SettingArray["MarkTypeDisplay"];

### aki
### GET GT, AM START ##################################################################################
# Get Class Level(s) of the report
if($ClassLevelID!="")
	$SelectedFormArr = array($ClassLevelID);
else
	$SelectedFormArr = $libreportcard->GET_REPORT_TEMPLATE_FORM($ReportID);

# Get Class Student(s)
$LevelList = implode(",", $SelectedFormArr);
list($ClassStudentArray, $StudentIDArray) = $libreportcard->GET_STUDENT_BY_CLASSLEVEL($ReportID, $LevelList, $ClassID);    
### GET GT, AM END ####################################################################################
### aki
$thisYearTermID = $libreportcard->Get_Semester_By_SemesterName($SelectedColumn['ColumnTitle']);
if ($SelectedColumn['ColumnTitle'] == '' || $thisYearTermID == '')
	$thisYearTermID = 0;
	
if ($ReportColumnID=="") {
	$SummaryColumnTitle = $libreportcard->Semester;
} else {
	//$SummaryColumnTitle = $SelectedColumn['ColumnTitle'];
	$SummaryColumnTitle = $thisYearTermID;
}
$CmpArray = $libreportcard->GET_REPORT_SELECTED_SUBJECTS($ReportID);			
for ($CmpCount = 0; $CmpCount < sizeof($CmpArray); $CmpCount++) {
	$IsCmpArray[$CmpArray[$CmpCount][3]] = $CmpArray[$CmpCount][1];
}

##############################################	Get Attendance and Tardiness	##############################################
		
include_once($PATH_WRT_ROOT."home/eAdmin/StudentMgmt/reportcard/report_generation/print_report_functions2.php");
$lf = new libfilesystem();
$StudentRegNoArray = $StudentRegNoMap;
$SemesterArray = $libreportcard->GET_SEMESTER_ARRAY();
		
# Get Summary Array from CSV file
$SummaryHeader = array("REGNO","CONDUCT","TIDINESS","APPLICATION","DAYS_ABSENT","TIMES_LATE","DAYS_ABSENT_WO_REASON","POLITENESS","BEHAVIOUR");
$TargetSummaryFile = $intranet_root."/file/reportcard/summary";
$StudentSummaryArray = GET_CSV_FILE_CONTENT($TargetSummaryFile, $SummaryHeader);

##############################################	Get Attendance and Tardiness	##############################################

$exportColumn[] = "";
if(is_array($SubjectArray))
{
	foreach($SubjectArray as $subject_id => $subject_name)
	{
		### 20091126 Ivan
		### if the subject score of all student is empty => hide the subject
		if (!$DisplaySubjectIDAssoArr[$subject_id])
			continue;
			
		$exportColumn[] = str_replace("<br />", " - ", $subject_name);
		$SubjectIDArray[] = $subject_id;
	}
}

$SubjectIDList = (is_array($SubjectIDArray)) ? implode(",", $SubjectIDArray) : "";
list($FailedArray, $DistinctionArray, $PFArray, $SubjectScaleArray) = $libreportcard->GET_SUBJECT_GRADEMARK($ClassID, $SubjectIDList);

$exportColumn[] = $eReportCard['ByTotal'];
$exportColumn[] = $eReportCard['AverageMark'];
$exportColumn[] = $eReportCard['ClassPosition'];
$exportColumn[] = $eReportCard['FormPosition'];
$exportColumn[] = $eReportCard['SubjectTaken'];

for($k=0; $k<sizeof($ReportTypeArray); $k++)
{
  //$TargetFile = $intranet_root."/file/reportcard/".$ReportTypeArray[$k]."/".$libreportcard->Year."_".$ReportPeriod."_".str_replace("/", "", $ClassName).".csv";
  $TargetFile = $intranet_root."/file/reportcard/".$ReportTypeArray[$k]."/".$libreportcard->YearName."_".$thisYearTermID."_".str_replace("/", "", $ClassName).".csv";

  if(file_exists($TargetFile))
  {
    $stu = 0;
    //$handle = fopen($TargetFile, "r");
    //$CSVHeader = fgetcsv($handle, 1000, ",");
    
    $data = $limport->GET_IMPORT_TXT($TargetFile, 0, '<br />');
    $CSVHeader = array_shift($data);
    
    //while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
    for ($i=0; $i<count($data); $i++) {
      for($j=0; $j<count($CSVHeader); $j++)
      {
        $StudentRegNo = str_replace("#", "", $data[$i][0]);
        $Student_ID = $StudentRegNoMap[$StudentRegNo];
        if($Student_ID != "")
          $StudentExRecord[$Student_ID][$ReportTypeArray[$k]][$CSVHeader[$j]] = $data[$i][$j];
      }
      $stu++;
    }
//    fclose($handle);
  }
  
  if(!in_array($k, $ExcludeInReport))
  {
	  if (is_array($DefaultHeaderArray[$k][$ReportCardTemplate])) {
		  foreach($DefaultHeaderArray[$k][$ReportCardTemplate] as $key=>$value){
		    if(strcmp($value, "REGNO") != 0)
	        $exportColumn[] = $HeaderLangMap[$k][$value];
	      }
      }
  }
}


for($i=0; $i<sizeof($StudentArray); $i++)
{
  $TotalMark = 0;
  $AvgMark = 0;
  $SubjectTaken = 0;

	list($r_student_id, $RegNo, $class_number, $student_name) = $StudentArray[$i];
	list($GT, $AM, $GPA, $OMC, $OMCT, $OMF, $OMFT) = $ClassStudentArray[$ClassID][$r_student_id]["Result"][$SummaryColumnTitle];
	
	$row[] = "<".trim($class_number)."> ".trim($student_name);
	for($k=0; $k<sizeof($SubjectIDArray); $k++)
	{
		$r_subject_id = $SubjectIDArray[$k];
		$SubjectSetting = $SubjectScaleArray[$r_subject_id];
		$s_mark = "";
		
		### 20091126 Ivan
		### if the subject score of all student is empty => hide the subject
		if (!$DisplaySubjectIDAssoArr[$r_subject_id])
			continue;
		
		if($SubjectSetting=="S")
		{
			$s_mark = $ResultArray[$r_student_id][$r_subject_id]["Mark"];
			$s_grade = $ResultArray[$r_student_id][$r_subject_id]["Grade"];
			if ((!$IsCmpArray[$r_subject_id])&&($s_mark>=-1)) $SubjectTaken++;
			if($s_mark>=0) {
				$Result = ($MarkTypeDisplay==2) ? $s_grade : $s_mark;
				
				if($s_mark != "")
				{
					$TotalMark += $s_mark;
					//if (!$IsCmpArray[$r_subject_id]) $SubjectTaken++;
				}
			}
			else
				$Result = $libreportcard->SpecialMarkArray[trim($s_mark)];
		} 
		else {
			$Result = $ResultArray[$r_student_id][$r_subject_id]["Grade"];
		}
		$Result = ($Result=="") ? "--" : trim($Result);
		$row[] = $Result;
	}
	
	if($SubjectTaken > 0)
	{
		$TotalMark = number_format($TotalMark, 2, '.', '');
		$AvgMark = number_format(($TotalMark/$SubjectTaken), 2, '.', '');
	}
	
	$row[] = $GT;
	$row[] = $AM;
	$row[] = $OMC;
	$row[] = $OMF;
	$row[] = $SubjectTaken;
	
	# Get calculated attendance and tardiness
	$Summary_Details_Ary = GET_REPORT_ATTENDANCE_AND_TARDINESS($StudentSummaryArray[$r_student_id]);
	$Summary_Details_Ary = $Summary_Details_Ary[$SelectedColumn['ColumnTitle']];

	for($k=0; $k<sizeof($ReportTypeArray); $k++)
	{
	  if(!in_array($k, $ExcludeInReport))
	  {
		  $Header = $DefaultHeaderArray[$k][$ReportCardTemplate];
		  $Content = $StudentExRecord[$r_student_id][$ReportTypeArray[$k]];
		  
		  # Display calculated value
    	  if(in_array('ATTENDANCE', $Header)){
		  	$Content['ATTENDANCE'] = $Summary_Details_Ary['Attendance']? $Summary_Details_Ary['Attendance'] : "";
		  }
		  if(in_array('TIMES_LATE', $Header)){
			$Content['TIMES_LATE'] = $Summary_Details_Ary['TimesLate']? $Summary_Details_Ary['TimesLate'] : "";
		  }
		  if(in_array('TARDINESS', $Header)){
			$Content['TARDINESS'] = $Summary_Details_Ary['Tardiness']? $Summary_Details_Ary['Tardiness'] : "";
			$Content['TARDINESS_OFFENCE'] = $Summary_Details_Ary['TardinessOffence']? $Summary_Details_Ary['TardinessOffence'] : "";
		  }
		  
		  if(is_array($Content))
		  {
			if (is_array($Header))
		    foreach($Header as $idx => $fieldname)
		    {
		      if($fieldname != "REGNO")
		        $row[] = $Content[$fieldname];
	      }
		  }
		  else
		  {
		    for($a=1; $a<count($Header); $a++)
            	$row[] = "--";
		  }
		}
	}
	
	$rows[] = $row;
	unset($row);
}

intranet_closedb();

// Output the file to user browser
$filename = str_replace("&nbsp;", "", "Score_List_".trim($ClassName).".csv");

$export_content = $lexport->GET_EXPORT_TXT($rows, $exportColumn);
$lexport->EXPORT_FILE($filename, $export_content);

//output2browser($Content, $filename);

?>
