<?php
$PATH_WRT_ROOT = "../../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
//include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

if ($plugin['ReportCard'])
{
	include_once($PATH_WRT_ROOT."includes/libreportcard.php");
	$libreportcard = new libreportcard();
	
	# get class object
	$ClassObj = $libreportcard->GET_CLASS_OBJ($ClassID);
	list($ClassID, $ClassLevelID, $ClassName) = $ClassObj;

	if($ck_ReportCard_UserType=="ADMIN")
	{
		$CurrentPage = "MarksheetRevision";
		$BackPage = "admin_index.php?SubjectID=$SubjectID&ClassLevelID=$ClassLevelID";
		$TagName = $eReportCard['MarksheetRevision'];
	}
	else
	{
		$CurrentPage = "MarksheetSubmission";
		$BackPage = "index.php";
		$TagName = $eReportCard['MarksheetSubmission'];
	}
	$MODULE_OBJ = $libreportcard->GET_MODULE_OBJ_ARR();
    if ($libreportcard->hasAccessRight())
    {
        $linterface = new interface_html();

###############################################################

		# Get ReportID
		$ReportID = $libreportcard->GET_REPORTID_BY_CLASS($ClassID);

		# Get component subject(s) if any
		$CmpSubjectArray = $libreportcard->GET_REPORT_CMP_SUBJECT($SubjectID, $ReportID);
		if(empty($CmpSubjectArray))
		{
			$param = "SubjectID=$SubjectID&ClassID=$ClassID&ClassLevelID=$ClassLevelID&ReportID=$ReportID&TargetStudentIDList=$TargetStudentIDList";
			header("Location: marksheet_edit2.php?$param");
		}
		else
		{
			$SubjectSelect = "<SELECT name='SubjectID'>";

			$ParentSubjectID = ($ParentSubjectID=="") ? $SubjectID : $ParentSubjectID;
			$ParentSubjectName = $libreportcard->GET_SUBJECT_NAME($ParentSubjectID);
			$SubjectSelect .= "<OPTION value='".$ParentSubjectID."'>".$ParentSubjectName."</OPTION>";

			for($i=0; $i<sizeof($CmpSubjectArray); $i++)
			{
				list($sid, $sname) = $CmpSubjectArray[$i];
				$CellNumber = $libreportcard->CHECK_REPORT_CELL_NULL($ReportID, $sid);
				$SubjectSelect .= ($CellNumber>0) ? "<OPTION value='".$sid."' ".($i==0?"SELECTED='SELECTED'":"").">&nbsp;&nbsp;&nbsp;&nbsp;".$sname."</OPTION>" : "";
			}
			$SubjectSelect .= "</SELECT>";
		}

################################################################

# tag information
$TAGS_OBJ[] = array($TagName, "", 0);

$STEPS_OBJ[] = array($eReportCard['SelectSubject'], 1);
$STEPS_OBJ[] = array($eReportCard['SubmitMarksheet'], 0);

$linterface->LAYOUT_START();
?>
<script language="javascript">
function jCHECK_PARENT()
{
	var obj = document.form1;
	if(obj.SubjectID.value=="<?=$ParentSubjectID?>")
		obj.IsParent.value = 1;
}
</script>
<form name="form1" action="marksheet_edit2.php" method="get">
<br />
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
</tr>
<tr>
	<td>
		<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tr><td align='right' colspan="2"><?= $linterface->GET_SYS_MSG($Result); ?></td></tr>
		<tr><td valign="top" nowrap="nowrap" colspan="2">
			<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
			<tr><td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $eReportCard['ClassName']; ?></td><td class="textboxnum tablerow2" width="75%"><?=$ClassName?></td></tr>
			<tr><td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=($intranet_session_language=="en"?$eReportCard['EngSubject']:$eReportCard['ChiSubject']); ?></td><td width="75%"><?=$SubjectSelect?></td></tr>
			</table>
		</td></tr>
		</table>
	</td>
</tr>
<tr>
	<td><table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
	<tr>
		<td align="center"" id="bottom"><?= $linterface->GET_ACTION_BTN($button_continue, "submit", "onSubmit:jCHECK_PARENT()", "btnSubmit")?>&nbsp;
		<?= $linterface->GET_ACTION_BTN($button_reset, "reset")?>&nbsp;
		<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:self.location.href='$BackPage'")?>
		</td>
	</tr>
	</table>
	</td>
</tr>
</table>

<input type="hidden" name="ParentSubjectID" value="<?=$ParentSubjectID?>">
<input type="hidden" name="ReportID" value="<?=$ReportID?>">
<input type="hidden" name="ClassID" value="<?=$ClassID?>">
<input type="hidden" name="ClassLevelID" value="<?=$ClassLevelID?>">
<input type="hidden" name="TargetStudentIDList" value="<?=$TargetStudentIDList?>">
<input type="hidden" name="IsParent">
<input type="hidden" name="HaveComponent" value="1">
<p></p>
</form>
<?=$linterface->FOCUS_ON_LOAD("form1.SubjectID")?>
<?
        $linterface->LAYOUT_STOP();
		intranet_closedb();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>
