<?php
$PATH_WRT_ROOT = "../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libreportcard.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

$libreportcard = new libreportcard();
$lexport = new libexporttext();

# Get SubjectName
$SubjectName = $libreportcard->GET_SUBJECT_NAME($SubjectID);
$SubjectName = str_replace("&nbsp;", "_", $SubjectName);

$ReportColumnInfo = $libreportcard->GET_REPORT_COLUMN_DETAILS($ReportColumnID);
$ColumnName = $ReportColumnInfo[0];

# Get Column Array
$ColumnArray = $libreportcard->GET_ALL_SUB_MARKSHEET_COLUMN($ReportColumnID, $SubjectID);
$ColumnSize = sizeof($ColumnArray);

# Get and Student Array
$StudentArray = $libreportcard->GET_STUDENT_BY_CLASS($ClassID, $TargetStudentIDList);
$StudentSize = sizeof($StudentArray);

if(!empty($TargetStudentIDList))
{
	$StudentIDList = $TargetStudentIDList;
}
else
{
	for($i=0; $i<$StudentSize; $i++)
	{
		$StudentIDArray[] = $StudentArray[$i][0];
	}
	if(!empty($StudentIDArray))
		$StudentIDList = implode(",", $StudentIDArray);
}

$Content = "ClassName,ClassNumber";
$exportColumn[] = "ClassName";
$exportColumn[] = "ClassNumber";
list($GradingSchemeID, $DefaultSetting) = $libreportcard->GET_GRADING_SCHEME_BY_SUBJECT_CLASS($SubjectID, $ClassID);
for($i=0; $i<$ColumnSize; $i++)
{
	list($ColumnID, $ColumnTitle, $Weight) = $ColumnArray[$i];
	$Content .= ",".$ColumnTitle;
	$exportColumn[] = $ColumnTitle;
}

if($ColumnSize>0)
{
	$Content .= ",".$eReportCard['OverallResult'];
	$Content .= ",".$eReportCard['WeightedOverallResult']."\n";
	$exportColumn[] = $eReportCard['OverallResult'];
	$exportColumn[] = $eReportCard['WeightedOverallResult'];
}
else
	$Content .= "\n";

# Get Marksheet Result
$SubMarksheetResult = $libreportcard->GET_SUB_MARKSHEET_SCORE($ReportColumnID, $SubjectID);

for($j=0; $j<sizeof($StudentArray); $j++)
{
	list($StudentID, $Class, $StudentName) = $StudentArray[$j];

	if(!empty($StudentID))
	{
		list($ClassName, $ClassNumber) = explode("-", $Class);
		$Content .= trim($ClassName).",".trim($ClassNumber);
		$row[] = trim($ClassName);
		$row[] = trim($ClassNumber);
		if(!empty($SubMarksheetResult))
		{
			for($k=0; $k<sizeof($ColumnArray); $k++)
			{
				list($ColumnID, $ColumnTitle, $Weight) = $ColumnArray[$k];

				$Mark = $SubMarksheetResult[$StudentID][$ColumnID];
				$Mark = round($Mark, 1);
				if($Mark<0)
					$Mark = $libreportcard->SpecialMarkArray[trim($Mark)];
				
				$Content .= ",".$Mark;
				$row[] = $Mark;
			}
			
			if($ColumnSize>0)
			{
				$OverallMark = $SubMarksheetResult[$StudentID]["Overall"];
				$OverallMark = round($OverallMark, 1);
				$OverallMark = ($OverallMark<0) ? $libreportcard->SpecialMarkArray[trim($OverallMark)] : $OverallMark;
				$Content .= ",".$OverallMark;
				$row[] = $OverallMark;

				$OverallWeightedMark = $SubMarksheetResult[$StudentID]["WeightedOverall"];
				$OverallWeightedMark = round($OverallWeightedMark, 1);
				$OverallWeightedMark = ($OverallWeightedMark<0) ? $libreportcard->SpecialMarkArray[trim($OverallWeightedMark)] : $OverallWeightedMark;
				$Content .= ",".$OverallWeightedMark;
				$row[] = $OverallWeightedMark;
			}
		}
		$Content .= "\n";
		$rows[] = $row;
		unset($row);
	}
}

intranet_closedb();

// Output the file to user browser
$filename = str_replace("&nbsp;", "", "SubMarksheet_".trim(str_replace("-", "_", $SubjectName))."_".trim($ClassName)."_".trim($ColumnName).".csv");

$export_content = $lexport->GET_EXPORT_TXT($rows, $exportColumn);
$lexport->EXPORT_FILE($filename, $export_content);


//output2browser($Content, $filename);

?>
