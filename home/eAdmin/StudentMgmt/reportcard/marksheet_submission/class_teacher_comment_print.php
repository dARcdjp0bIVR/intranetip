<?php
$PATH_WRT_ROOT = "../../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libwordtemplates.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");
if ($plugin['ReportCard'])
{
	include_once($PATH_WRT_ROOT."includes/libreportcard.php");
	$libreportcard = new libreportcard();
	$MODULE_OBJ = $libreportcard->GET_MODULE_OBJ_ARR();

	
	##### Word Template Setting #####################################################
	$lf = new libwordtemplates(1);
	$base_dir = "$intranet_root/file/templates/";
	if (!is_dir($base_dir))
	{
	     $lf->folder_new($base_dir);
	}
	
	$file_array = $lf->file_array;
	$word_array = $lf->word_array;
	$data = get_file_content($base_dir.$file_array[0]); // 0 - class teacher
	$CommentArr = explode("\n", $data);
	
	function GEN_COMMENT_SEL($ParStuID) {
		global $CommentArr, $button_select;
		
		$ReturnStr = "<select onchange='document.getElementById(\"Comment_".$ParStuID."\").innerText+=\" \"+this.value'>\n";		
		$ReturnStr .= "<option value=''>-- {$button_select} --</option>";
		for ($i = 0; $i < sizeof($CommentArr); $i++) {
			$ReturnStr .= "<option value='".str_replace("'", "&#39;", str_replace("\"", "&quot;", $CommentArr[$i]))."'>".$CommentArr[$i]."</option>\n";
		}
		$ReturnStr .= "<select>\n";
		return $ReturnStr;
	}
	##### Word Template Setting #####################################################
	
	if ($libreportcard->hasAccessRight())
    {
        $linterface = new interface_html();

		if($ck_ReportCard_UserType=="ADMIN")
		{
			$FormArray = $libreportcard->GET_ALL_REPORT_FORMS();
			if($ClassLevelID=="")
				$ClassLevelID = $FormArray[0][0];

			$FormSelection = getSelectByArray($FormArray, "name='ClassLevelID' onChange=\"document.form1.form_change.value=1;jFilterChange()\"", $ClassLevelID, 1, 1, "");

			$ClassArray = $libreportcard->GET_CLASSES_BY_FORM($ClassLevelID);
			$ClassSelection = getSelectByArray($ClassArray, "name='ClassID' onChange=\"jFilterChange()\"", $ClassID, 1, 1, "");
			
			if($ClassID=="" || $form_change==1)
				$ClassID = $ClassArray[0][0];

			$FormRow = "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">".$eReportCard['Form']."</td><td class=\"tabletext\" width=\"70%\">".$FormSelection."</td></tr>";
		}
		else
			$HiddenClassIDRow = "<input type=\"hidden\" name=\"ClassID\" value=\"".$ClassID."\" />";

		# get class object
		$ClassObj = $libreportcard->GET_CLASS_OBJ($ClassID);
		list($ClassID, $ClassLevelID, $ClassName) = $ClassObj;
		
		$ClassDisplay = ($ck_ReportCard_UserType=="ADMIN") ? $ClassSelection : $ClassName;
###############################################################
		
		# Get Results
		$CommentArray = $libreportcard->GET_TEACHER_COMMENT($ClassID);
		
		# Get Student of Class
		$StudentArray = $libreportcard->GET_STUDENT_BY_CLASS($ClassID);

		$TableContent = "<table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\" class=\"eSporttableborder\">";
		$HeaderRow = "<tr>";
		$HeaderRow .= "<td class=\"eSporttdborder  eSportprinttext\">".$eReportCard['ClassNumber']."</td>";
		$HeaderRow .= "<td class=\"eSporttdborder  eSportprinttext\">".$i_identity_student."</td>";
		$HeaderRow .= "<td class=\"eSporttdborder  eSportprinttext\">".$eReportCard['Comment']."</td>";
		$HeaderRow .= "</tr>";
		
		# Get Grademark scheme of subjects
		$SubjectIDList = (is_array($SubjectIDArray)) ? implode(",", $SubjectIDArray) : "";
		list($FailedArray, $DistinctionArray, $PFArray, $SubjectScaleArray) = $libreportcard->GET_SUBJECT_GRADEMARK($ClassID, $SubjectIDList);

		$TableContent .= $HeaderRow;
		for($i=0; $i<sizeof($StudentArray); $i++)
		{
			$css = ($i%2?"2":"");
			list($r_student_id, $class_number, $student_name) = $StudentArray[$i];
			$r_comment = $CommentArray[$r_student_id];

			$TableContent .= "<tr>";
			$TableContent .= "<td nowrap='nowrap' valign='top' class=\"eSporttdborder eSportprinttext\">".$class_number."&nbsp;</td>";
			$TableContent .= "<td nowrap='nowrap' valign='top' class=\"eSporttdborder eSportprinttext\">".$student_name."&nbsp;</td>";
			$TableContent .= "<td valign='top' class=\"eSporttdborder eSportprinttext\">".nl2br($r_comment)." &nbsp;</td>";
			$TableContent .= "</tr>";
		}
		$TableContent .= "</table>";
		
################################################################
?>
<style type='text/css' media='print'>
.print_hide {display:none;}
</style>

<br />
<form name="form1" action="class_teacher_comment_update.php" method="POST">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="4">
<tr>
	<td>
		<?=$TableContent?>
	</td>
</tr>

</table>
<?=$HiddenClassIDRow?>
<input type="hidden" name="form_change" />
</form>
<?
		intranet_closedb();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>
