<?php
$PATH_WRT_ROOT = "../../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

if ($plugin['ReportCard'])
{
	include_once($PATH_WRT_ROOT."includes/libreportcard.php");
  include_once("../default_header.php");

	$libreportcard = new libreportcard();
	$lf = new libfilesystem();
	if($ck_ReportCard_UserType=="ADMIN")
	{
		$CurrentPage = "MarksheetRevision";
		$BackPage = "admin_index.php?SubjectID=$SubjectID&ClassLevelID=$ClassLevelID";
		$TagName = $eReportCard['MarksheetRevision'];
	}
	else
	{
		$CurrentPage = "MarksheetSubmission";
		$BackPage = "index.php";
		$TagName = $eReportCard['MarksheetSubmission'];
	}
	$MODULE_OBJ = $libreportcard->GET_MODULE_OBJ_ARR();
	$MODULE_OBJ['title'] = $eReportCard['ScoreSummary'];

    if ($libreportcard->hasAccessRight())
    {
        $linterface = new interface_html("popup.html");

###############################################################

		if($ck_ReportCard_UserType=="ADMIN")
		{
		  $IsWeighted = 0;

			$FormArray = $libreportcard->GET_ALL_REPORT_FORMS();
			if($ClassLevelID=="")
				$ClassLevelID = $FormArray[0][0];

			$FormSelection = getSelectByArray($FormArray, "name='ClassLevelID' onChange=\"document.form1.form_change.value=1;jFilterChange()\"", $ClassLevelID, 1, 1, "");
      		$FormRow = "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">".$eReportCard['Form']."</td><td class=\"tabletext\" width=\"70%\">".$FormSelection."</td></tr>";

			$ClassArray = $libreportcard->GET_CLASSES_BY_FORM($ClassLevelID, $libreportcard->Year);
			
  		# Get All Subjects
  		$SubjectArray = $libreportcard->GET_SUBJECT_BY_FORM($ClassLevelID);
		
  		if(!empty($SubjectArray))
  		{
  			if($SubjectID=="" || $form_change==1)
  				$SubjectID = $SubjectArray[0][0];

  			$SubjectName = $libreportcard->GET_SUBJECT_NAME($SubjectID);

  			$SubjectSelection = getSelectByArray($SubjectArray, "name='SubjectID' onChange='document.form1.submit()'", $SubjectID, 1, 1, "");
  			$SubjectRow .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">".$eReportCard['Subject']."</td><td class=\"tabletext\" width=\"70%\">".$SubjectSelection."</td></tr>";
  		}
		}
		else
			$HiddenClassIDRow = "<input type=\"hidden\" name=\"ClassID\" value=\"".$ClassID."\" />";

		# Get ReportID Class
		$ReportID = $libreportcard->GET_REPORTID_BY_CLASSLEVEL($ClassLevelID);

		#########################################################################################################
		# Get Column Array
		$ColumnArray = $libreportcard->GET_REPORT_TEMPLATE_COLUMN($ReportID);
		/*
            [ReportColumnID] => 19
            [ColumnTitle] => �W
		*/
		# Get Column Selection
		$ColumnArraySel = "<select id='ReportColumnID' name='ReportColumnID' onchange='document.form1.action=\"score_summary.php\"; document.form1.submit();'>";
		$ColumnArraySel .= "<option value=''>".$eReportCard['OverallResult']."</option>";
		for ($temp = 0; $temp < sizeof($ColumnArray); $temp++) {
			$selected = ($ReportColumnID == $ColumnArray[$temp][0]) ? "selected" : "";
			$ColumnArraySel .= "<option value='".$ColumnArray[$temp][0]."' $selected>".$ColumnArray[$temp][1]."</option>";
		}
		$ColumnArraySel .= "</select>";

		# Get IsWeighted Selection
		$IsWeightedSel = "<select id='IsWeighted' name='IsWeighted' onchange='document.form1.action=\"score_summary.php\"; document.form1.submit();'>";
		$IsWeightedSel .= "<option value='0'".($IsWeighted?"":"selected").">".$eReportCard['RawMarks']."</option>";
		$IsWeightedSel .= "<option value='1'".($IsWeighted?"selected":"").">".$eReportCard['WeightedMarks']."</option>";
		$IsWeightedSel .= "</select>";
		#########################################################################################################

		# Get Results
		foreach($ClassArray as $ClassObj)
		{
	  		$ClassID = $ClassObj['ClassID'];
	  		if ($ReportColumnID != "") {
	  			$ResultArray[$ClassID] = $libreportcard->GET_STUDENT_SUBJECT_COLUMN_RESULT($ClassID, $ReportColumnID, $IsWeighted, $SubjectID);
	  		} else {
	  			$ResultArray[$ClassID] = $libreportcard->GET_STUDENT_SUBJECT_RESULT($ClassID, $SubjectID);
	  		}
	
	  		# Get Student of Class
	  		$StudentArray[$ClassID] = $libreportcard->GET_STUDENT_BY_CLASS($ClassID);
	  	}

	list($GradingSchemeID, $DefaultSetting, $FullMark) = $libreportcard->GET_GRADING_SCHEME_BY_SUBJECT_CLASS($SubjectID, $ClassID);

  	$OutstandingMark = 10;
  	$PassingMark = 50;
  	
  	if(is_array($ResultArray))
  	{
  		foreach($ResultArray as $ClassID => $Students)
  		{
  			if (!isset($PassingMarkArr[$SubjectID][$ClassID]))
  			{
	  			list($GradingSchemeID, $DefaultSetting, $FullMark) = $libreportcard->GET_GRADING_SCHEME_BY_SUBJECT_CLASS($SubjectID, $ClassID);
	  			list($MainInfo, $GradeMarkArr) = $libreportcard->GET_SCHEME_INFO($GradingSchemeID);
	  			$PassingMarkArr[$SubjectID][$ClassID] = $GradeMarkArr['P'][sizeof($GradeMarkArr['P'])-1][0];
			  	$PassingMark = $PassingMarkArr[$SubjectID][$ClassID];
	  			$OutstandingMark = $GradeMarkArr['D'][0];
			}

		# count the student if assessed
  		  //$MarkClass[0][$ClassID] = count($Students); // Number of students
  		  $MarkClass[0][$ClassID] = 0;
  		  $MarkClass[1][$ClassID] = 0; // Below 10
  		  $MarkClass[2][$ClassID] = 0; // Below 20
  		  $MarkClass[3][$ClassID] = 0; // Below 30
  		  $MarkClass[4][$ClassID] = 0; // Below 40
  		  $MarkClass[5][$ClassID] = 0; // Below 50
  		  $MarkClass[6][$ClassID] = 0; // Below 60
  		  $MarkClass[7][$ClassID] = 0; // Below 70
  		  $MarkClass[8][$ClassID] = 0; // Below 80
  		  $MarkClass[9][$ClassID] = 0; // Below 90
  		  $MarkClass[10][$ClassID] = 0; // Over 90
  		  $MarkClass[11][$ClassID] = -999; // Highest mark
  		  $MarkClass[12][$ClassID] = 999; // Lowest mark
  		  $MarkClass[13][$ClassID] = 0; // Number of outstandings
  		  $MarkClass[14][$ClassID] = 0; // Outstanding rate
  		  $MarkClass[15][$ClassID] = 0; // Number of passes
  		  $MarkClass[16][$ClassID] = 0; // Passing rate
  		  $MarkClass[17][$ClassID] = 0; // Number of fails
  		  $MarkClass[18][$ClassID] = 0; // Failure rate
  		  $MarkClass[19][$ClassID] = ""; // Mean

  		  $DummyTotal = 0;
  		  $tmp_mark = array();

        if(is_array($Students))
        {
          foreach($Students as $StudentID => $Subject)
          {
            foreach($Subject as $SubjectID => $Mark)
            {
            	if ($Mark['Mark']<0)
            	{
            		continue;
            	}

            	$MarkClass[0][$ClassID] ++;
              if($Mark['Mark'] < 10)
                $MarkClass[1][$ClassID]++;
              else if($Mark['Mark'] < 20)
                $MarkClass[2][$ClassID]++;
              else if($Mark['Mark'] < 30)
                $MarkClass[3][$ClassID]++;
              else if($Mark['Mark'] < 40)
                $MarkClass[4][$ClassID]++;
              else if($Mark['Mark'] < 50)
                $MarkClass[5][$ClassID]++;
              else if($Mark['Mark'] < 60)
                $MarkClass[6][$ClassID]++;
              else if($Mark['Mark'] < 70)
                $MarkClass[7][$ClassID]++;
              else if($Mark['Mark'] < 80)
                $MarkClass[8][$ClassID]++;
              else if($Mark['Mark'] < 90)
                $MarkClass[9][$ClassID]++;
              else
                $MarkClass[10][$ClassID]++;

              if($MarkClass[11][$ClassID] < $Mark['Mark'])
                $MarkClass[11][$ClassID] = $Mark['Mark'];
              if($MarkClass[12][$ClassID] > $Mark['Mark'] && $Mark['Mark']>=0)
                $MarkClass[12][$ClassID] = $Mark['Mark'];

              if($Mark['Mark'] >= $OutstandingMark)
                $MarkClass[13][$ClassID]++;
              if($Mark['Mark'] >= $PassingMark)
                $MarkClass[15][$ClassID]++;
              else
                $MarkClass[17][$ClassID]++;

              $tmp_mark[] = $Mark['Mark'];

              $DummyTotal += $Mark['Mark'];
            }
          }
        }

        if($MarkClass[11][$ClassID] == -999)
          $MarkClass[11][$ClassID] = "--";
        if($MarkClass[12][$ClassID] == 999)
          $MarkClass[12][$ClassID] = "--";
        if($MarkClass[0][$ClassID] != 0)
        {
          $MarkClass[14][$ClassID] = number_format(($MarkClass[13][$ClassID]/$MarkClass[0][$ClassID]*100), 2, '.', '')."%";
          $MarkClass[16][$ClassID] = number_format(($MarkClass[15][$ClassID]/$MarkClass[0][$ClassID]*100), 2, '.', '')."%";
          $MarkClass[18][$ClassID] = number_format(($MarkClass[17][$ClassID]/$MarkClass[0][$ClassID]*100), 2, '.', '')."%";
          $MarkClass[19][$ClassID] = round(($DummyTotal/$MarkClass[0][$ClassID]), 2);
          $MarkClass[20][$ClassID] = round($libreportcard->getStandardDeviation($tmp_mark),2);
        }
        if(strcmp($MarkClass[19][$ClassID],"") == 0)
          $MarkClass[19][$ClassID] = "--";
        if(strcmp($MarkClass[20][$ClassID],"") == 0)
          $MarkClass[20][$ClassID] = "--";
        else if($MarkClass[20][$ClassID] == -1)
          $MarkClass[20][$ClassID] = "Error in calculation";
  		}
  	}

		# Get Report Display Settings
		$ReportInfo = $libreportcard->GET_REPORT_TEMPLATE($ReportID);
		$Settings = $ReportInfo[4];
		$SettingArray = unserialize($Settings);
		$MarkTypeDisplay = $SettingArray["MarkTypeDisplay"];

		$TableContent = "<table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">";
		if(is_array($ClassArray))
		{
			$HeaderRow = "<tr class='tablebluetop'>";
			$HeaderRow .= "<td>&nbsp;</td>";
			foreach($ClassArray as $ClassObj)
			{
				$HeaderRow .= "<td class=\"tabletoplink\">".$ClassObj['ClassName']."</td>";
				$ClassIDArray[] = $ClassObj['ClassID'];
			}
			$HeaderRow .= "</tr>";
		}

		# Get Grademark scheme of subjects
//		$SubjectIDList = (is_array($SubjectIDArray)) ? implode(",", $SubjectIDArray) : "";
//		list($FailedArray, $DistinctionArray, $PFArray, $SubjectScaleArray) = $libreportcard->GET_SUBJECT_GRADEMARK($ClassID, $SubjectIDList);

		$colsize = sizeof($ClassArray) + 1;
		$GoToBottomRow = "<tr><td align='right' colspan='".$colsize."'><a class='tablegreenlink' href='#bottom'>".$eReportCard['GoToBottom']."</a></td></tr>";
		$GoToTopRow = "<tr><td align='right' colspan='".$colsize."'><a class='tablegreenlink' href='#top'>".$eReportCard['GoToTop']."</a></td></tr>";

		$TableContent .= $GoToBottomRow;
		$TableContent .= $HeaderRow;

    for($i=0; $i<count($MarkClass); $i++)
    {
      $css = ($i%2?"2":"");
      $TableContent .= "<tr class='tablebluerow".$css."'>";
      $TableContent .= "<td nowrap='nowrap'><span class=\"tabletext\">".$eReportCard['ScoreSummaryTitle'][$i]."</span></td>";
      foreach($ClassArray as $ClassObj)
      {
        $TableContent .= "<td nowrap='nowrap'><span class=\"tabletext\">".$MarkClass[$i][$ClassObj['ClassID']]."</span></td>";
      }
      $TableContent .= "</tr>";
    }

		$TableContent .= $GoToTopRow;
		$TableContent .= "</table>";
################################################################

$linterface->LAYOUT_START();
?>
<style type='text/css' media='print'>
.print_hide {display:none;}
</style>
<SCRIPT LANGUAGE="Javascript">
function jGO_PRINT_PAGE()
{
	obj = document.form1;
	obj.IsPrint.value = 1;
	obj.action = "score_summary_print.php";
	obj.target = "_blank";
	obj.submit();
	obj.action = "score_summary.php";
	obj.target = "_self";
}

function jFilterChange()
{
	obj = document.form1;
	obj.action = "score_summary.php";
	obj.target = "_self";
	obj.submit();
}
</SCRIPT>

<br />
<form name="form1" action="" method="GET">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td>
		<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
			<?=$FormRow?>
			<?=$SubjectRow?>
			<tr><td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $eReportCard['Column']; ?></td><td class="tabletext" width="70%"><?=$ColumnArraySel?></td></tr>
			<tr><td class="dotline" colspan=2><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
		</table>
	</td>
</tr>
<tr>
	<td>
		<?=$TableContent?>
	</td>
</tr>
<tr>
	<td><table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
	<tr>
		<td align="center"" id="bottom">
			<?= $linterface->GET_ACTION_BTN($button_close, "button", "self.close();")?>&nbsp;
			<?= $linterface->GET_ACTION_BTN($eReportCard['PrintPage'], "button", "jGO_PRINT_PAGE()")?>
			<?= $linterface->GET_ACTION_BTN($button_export, "button", "self.location.href='score_summary_export.php?ClassLevelID=$ClassLevelID&SubjectID=$SubjectID&ReportColumnID=$ReportColumnID'")?>
		</td>
	</tr>
	</table>
	</td>
</tr>
</table>
<?=$HiddenClassIDRow?>
<input type="hidden" name="IsPrint" />
<input type="hidden" name="form_change" />

</form>
<?
        $linterface->LAYOUT_STOP();
		intranet_closedb();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>
