<?php
$PATH_WRT_ROOT = "../../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

if ($plugin['ReportCard'])
{
	include_once($PATH_WRT_ROOT."includes/libreportcard.php");
	$lreportcard = new libreportcard();
	$CurrentPage = "MarksheetSubmission";
	$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();

	if ($lreportcard->hasAccessRight())
    {
         $linterface = new interface_html();

################################################################
		
		# Get Subject Name
		$SubjectName = $lreportcard->GET_SUBJECT_NAME($SubjectID);
		$SubMarksheetExist = $lreportcard->CHECK_SUB_MARKSHEET_EXIST($SubjectID, $ClassID);
		if($SubMarksheetExist==1)
		{
			$SubMarksheetRow = "<tr>
									<td><input type='checkbox' name='ShowSubMarksheet' id='ShowSubMarksheet' value=1 CHECKED='CHECKED' />&nbsp;<span class='tabletext'><label for='ShowSubMarksheet'>".$eReportCard['ShowSubMarksheet']."</label></span></td>
									</tr>
								";
		}
################################################################
	
# tag information
$TAGS_OBJ[] = array($eReportCard['MarksheetSubmission'], "", 0);

# page navigation (leave the array empty if no need)
$PAGE_NAVIGATION[] = array($eReportCard['ConfirmMarksheet'], "");

$linterface->LAYOUT_START();
?>
<script language="javascript">
<!--
function checkform(obj){
	if(!check_text(obj.Deadline, "<?php echo $i_alert_pleasefillin.$eReportCard['Deadline']; ?>.")) return false;
	if(!check_date(obj.Deadline, "<?php echo $eReportCard['InvalidDateFormatAlert']; ?>")) return false;
	return true;
}
//-->
</script>
<form name="form1" action="notify_update.php" method="post" onSubmit="return checkform(this);">
<br />
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
</tr>
<tr>
	<td>

	<table width="90%" border="0" cellpadding="5" cellspacing="1" align="center">
	<tr>
		<td colspan="2">
			<table width='100%' border='0' cellpadding="3" cellspacing="1">
			<tr><td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $eReportCard['Class']; ?></td><td class="tabletext tablerow2" width="85%" nowrap="nowrap"><?=$ClassName?></td></tr>
			<tr><td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $eReportCard['Subject']; ?></td><td class="tabletext tablerow2" width="85%" nowrap="nowrap"><?=$SubjectName?></td></tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table width="100%" border="0" cellpadding="3" cellspacing="0" align="center">
			<tr>
			<td><input type="checkbox" name="ShowMainMarksheet" id="ShowMainMarksheet" value=1 CHECKED='CHECKED' />&nbsp;<span class="tabletext"><label for="ShowMainMarksheet"><?=$eReportCard['ShowMainMarksheet']?></label></span></td>
			</tr>
			<?=$SubMarksheetRow?>
			</table>
		</td>
	</tr>
	</table>
</td>
</tr>
<tr>
	<td><table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
		<tr>
			<td align="center"><?= $linterface->GET_ACTION_BTN($button_submit, "submit", "", "btnSubmit")?>&nbsp;
			<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "")?>
			<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:history.back()")?>
			</td>
		</tr>
	</table>
	</td>
</tr>
</table>
<input type="hidden" name="SubjectID" value="<?=$SubjectID?>" />
<input type="hidden" name="ClassID" value="<?=$ClassID?>" />
<p></p>
</form>
<?= $linterface->FOCUS_ON_LOAD("form1.Deadline") ?>

<?
        $linterface->LAYOUT_STOP();
		intranet_closedb();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>
