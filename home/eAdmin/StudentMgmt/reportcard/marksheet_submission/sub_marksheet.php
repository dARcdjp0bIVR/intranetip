<?php
$PATH_WRT_ROOT = "../../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

if ($plugin['ReportCard'])
{
	include_once($PATH_WRT_ROOT."includes/libreportcard.php");
	$libreportcard = new libreportcard();
	$CurrentPage = "MarksheetSubmission";
	$MODULE_OBJ = $libreportcard->GET_MODULE_OBJ_ARR();
	$MODULE_OBJ['title'] = $eReportCard['SubMarksheet'];

	$TempIP = $SubjectID;
	if ($ParentSubjectID != "") $TempIP = $ParentSubjectID;
	$NotificationArray = $libreportcard->GET_CLASS_NOTIFICATION($TempIP);
	//$IsMSConfirmed = ($NotificationArray[$ClassID]==1);
	$IsMSSubmissionPeriod = $libreportcard->CHECK_MARKSHEET_SUBMISSION_PERIOD();
		
    if ($libreportcard->hasAccessRight())
    {
        $linterface = new interface_html("popup.html");

		# get class object
		$ClassObj = $libreportcard->GET_CLASS_OBJ($ClassID);
		list($ClassID, $ClassLevelID, $ClassName) = $ClassObj;
###############################################################
		//StartTimer();

		# Get Subject Name
		$SubjectName = $libreportcard->GET_SUBJECT_NAME($SubjectID);
		$ReportColumnInfo = $libreportcard->GET_REPORT_COLUMN_DETAILS($ReportColumnID);
		$ColumnName = $ReportColumnInfo[0];
		$ColumnWeight = $ReportColumnInfo[1];
		$NotForInput = $ReportColumnInfo[7];
		$Disabled = ($NotForInput) ? 1 : 0;
			
		
		# Get ReportSubjectName
		/*
		$ReportSubject = $libreportcard->GET_REPORT_SUBJECT_ID($ReportID, $SubjectID);
		$ReportSubjectID = $ReportSubject[0];
		$ReportCell = $libreportcard->GET_REPORT_CELL_BY_SUBJECT_COLUMN($ReportSubjectID, $ReportColumnID);
		$Disabled = ($ReportCell[$ReportColumnID] == "N/A") ? 1 : 0;
		*/
		
		if(!empty($ReportColumnID))
		{
			$param = "SubjectID=$SubjectID&ClassID=$ClassID&ReportColumnID=$ReportColumnID&TargetStudentIDList=$TargetStudentIDList";
			
			# Get Column Array
			$ColumnArray = $libreportcard->GET_ALL_SUB_MARKSHEET_COLUMN($ReportColumnID, $SubjectID);
			$ColumnSize = sizeof($ColumnArray);

			# Get and Student Array
			$StudentArray = $libreportcard->GET_STUDENT_BY_CLASS($ClassID, $TargetStudentIDList);
			$StudentSize = sizeof($StudentArray);

			if(!empty($TargetStudentIDList)) {
				$StudentIDList = $TargetStudentIDList;
			}
			else {
				for($i=0; $i<$StudentSize; $i++)
				{
					$StudentIDArray[] = $StudentArray[$i][0];
				}
				if(!empty($StudentIDArray))
					$StudentIDList = implode(",", $StudentIDArray);
			}
		
			$MarksheetTable = "<table width='100%' border='0' cellspacing='0' cellpadding='5'>";
			$MarksheetTable .= "<tr>
								<td width='15%' class='tabletopnolink tablegreentop'>".$i_ClassNumber."</td>
								<td class='tabletopnolink tablegreentop'>".$i_identity_student."</td>";
			
			for($i=0; $i<$ColumnSize; $i++)
			{
				list($ColumnID, $ColumnTitle, $Weight) = $ColumnArray[$i];
					
				$ColumnTitle = ($Weight!="") ? $ColumnTitle."<br />".$Weight."%" : $ColumnTitle;
				if ($IsMSSubmissionPeriod) {
					$MarksheetTable .= "<td width='12%' align='center' class='tabletopnolink tablegreentop'>";
					if (!$Disabled) {
						$MarksheetTable .= "<a class='tablelink' href=\"javascript:jDELETE_COLUMN('$ColumnID')\" >
											<img src=\"$image_path/$LAYOUT_SKIN/icon_delete.gif\" border=0 align='absmiddle' title='".$eReportCard['RemoveColumn']."'></a>&nbsp;<a class='tablelink' href=\"javascript:self.location.href='ms_column_new.php?$param&ColumnID=$ColumnID'\" >
											<img src=\"$image_path/$LAYOUT_SKIN/icon_edit.gif\" border=0 align='absmiddle' title='".$eReportCard['EditColumn']."'></a><br />";
					}
					$MarksheetTable .= $ColumnTitle."</td>";
				}
			}
			if($ColumnSize>0)
			{
				$MarksheetTable .= "<td width='12%' align='center' class='tabletopnolink tablegreentop'>".$eReportCard['OverallResult']."</td>";
				$MarksheetTable .= "<td width='12%' align='center' class='tabletopnolink tablegreentop'>".$eReportCard['WeightedOverallResult']."</td>";
			}
			$MarksheetTable .= "</tr>";

			##########################################################################################
			# Generate Sub-Marksheet Table
			$SubMarksheetResult = $libreportcard->GET_SUB_MARKSHEET_SCORE($ReportColumnID, $SubjectID);
			for($j=0; $j<$StudentSize; $j++)
			{
				//list($StudentID, $Class, $StudentName) = $StudentArray[$j];
				$td_style = ($j%2==1) ? "tablegreenrow2" : "tablegreenrow1";
				
				$StudentID = $StudentArray[$j]['UserID'];
				$Class = Get_Lang_Selection($StudentArray[$j]['ClassTitleCh'], $StudentArray[$j]['ClassTitleEn']);
				$StudentName = $StudentArray[$j]['StudentName'];
				
				$MarksheetTable .= "<tr>";
				$MarksheetTable .= "<td class='tabletext $td_style' nowrap='nowrap'>".$Class."</td>";
				$MarksheetTable .= "<td class='tabletext $td_style' nowrap='nowrap'>".$StudentName."</td>";
				
				if($ColumnSize>0)
				{
					$MarkArray = array();
					$ColumnIDArray = array();
					$TotalPercent = 0;
					for($i=0; $i<$ColumnSize; $i++)
					{
						list($ColumnID, $ColumnTitle, $Weight) = $ColumnArray[$i];
						
						$ColumnID = trim($ColumnID);
						if(!in_array($ColumnID, $ColumnIDArray))
								$ColumnIDArray[] = trim($ColumnID);

						$MarkLong = $SubMarksheetResult[$StudentID][$ColumnID];
						$Mark = ($MarkLong!="") ? round($MarkLong, 1) : $Mark;
						
						$LongFieldName = "mark_long_".$StudentID."_".$ColumnID;
						$FieldName = "mark_".$StudentID."_".$ColumnID;
						$FieldID = "mark[$j][$i]";
						if($Mark!="-2")
						{
							$TotalPercent = $TotalPercent + $Weight;
							if($Mark>=0)						
								$MarkArray[] = $MarkLong;
						}
						if($Mark<0) {
							$Mark = $libreportcard->SpecialMarkArray[trim($Mark)];
							$MarkLong = $Mark;
						}

						if ($Disabled) {
							$InputField = $Mark."<input type='hidden' name='$FieldName' id='$FieldID' value='".$Mark."'/>";
						} else {
							$InputField = "<input class='textboxnum' maxlength='5' type='text' name='$FieldName' id='$FieldID' value='".$Mark."' onPaste=\"pasteTable('$j', '$i');\" onblur=\"if (!setStart) { this.value=startContent; setStart = 1}\" />";
						}						
						$InputField .= "<input type='hidden' name='{$LongFieldName}' value='".$MarkLong."' >";

						$MarksheetTable .= "<td align='center' class='tabletext $td_style'>".$InputField."</td>";
					}
					$ColumnIDList = implode(",", $ColumnIDArray);

					$OverallMarkLong = $SubMarksheetResult[$StudentID]["Overall"];
					$OverallMark = ($OverallMarkLong!="") ? round($OverallMarkLong, 1) : $OverallMark;
					$OverallLongFieldName = "overall_long_".$StudentID;
					$OverallFieldName = "overall_".$StudentID;
					$OverallFieldID = "mark[$j][$i]";
					$OverallMark = ($OverallMark<0) ? $libreportcard->SpecialMarkArray[trim($OverallMark)] : $OverallMark;
						
					$MarksheetTable .= "<td align='center' class='tabletext $td_style'>";
					if ($Disabled) {
						$MarksheetTable .= $OverallMark."<input type='hidden' name='$OverallFieldName' id='$OverallFieldID' value='".$OverallMark."' />";
					} else {
						$MarksheetTable .= "<input class='textboxnum' maxlength='5' type='text' name='$OverallFieldName' id='$OverallFieldID' value='".$OverallMark."' onPaste=\"pasteTable('$j', '$i');\" onblur=\"if (!setStart) { this.value=startContent; setStart = 1}\" />";
					}					
					$MarksheetTable .= "</td>";
					$MarksheetTable .= "<input type='hidden' name='{$OverallLongFieldName}' value='".$OverallMarkLong."' >";

					$i++;
					$OverallWeightedMarkLong = $SubMarksheetResult[$StudentID]["WeightedOverall"];
					$OverallWeightedMark = ($OverallWeightedMarkLong!="")? round($OverallWeightedMarkLong, 1) : $OverallWeightedMark;

					$OverallWeightedLongFieldName = "overall_weighted_long_".$StudentID;
					$OverallWeightedFieldName = "overall_weighted_".$StudentID;
					$OverallWeightedFieldID = "mark[$j][$i]";
					$OverallWeightedMark = ($OverallWeightedMark<0) ? $libreportcard->SpecialMarkArray[trim($OverallWeightedMark)] : $OverallWeightedMark;
					$MarksheetTable .= "<td align='center' class='tabletext $td_style'>";
					if ($Disabled) {
						$MarksheetTable .= $OverallWeightedMark."<input type='hidden' name='$OverallWeightedFieldName' id='$OverallWeightedFieldID' value='".$OverallWeightedMark."' />";
					} else {
						$MarksheetTable .= "<input class='textboxnum' maxlength='5' type='text' name='$OverallWeightedFieldName' id='$OverallWeightedFieldID' value='".$OverallWeightedMark."' onPaste=\"pasteTable('$j', '$i');\" onblur=\"if (!setStart) { this.value=startContent; setStart = 1}\" />";
					}					
					$MarksheetTable .= "</td>";
					$MarksheetTable .= "<input type='hidden' name='{$OverallWeightedLongFieldName}' value='".$OverallWeightedMarkLong."' >";
				}
				$MarksheetTable .= "</tr>";
			}
			$MarksheetTable .= "</table>";

			$ImportButton = $linterface->GET_SMALL_BTN($button_import, "button", "javascript:self.location.href='sub_marksheet_import.php?$param'");
			$ExportButton = "&nbsp;".$linterface->GET_SMALL_BTN($button_export, 'button', "javascript:self.location.href='sub_marksheet_export_update.php?$param'");
			
			$MarkRemindRow = "<tr><td align='left' class='tabletextremark' colspan=2>".$eReportCard['MarkRemind']."</td></tr>";

			$colsize = ($ColumnSize>0) ? $ColumnSize+4 : 2;
			$x = $MarkRemindRow;
			$x .= "<tr><td class='tabletext' colspan='$colsize' align='right'><a class='tablelink' href='#bottom'>".$eReportCard['GoToBottom']."</a></td></tr>";
			$x .= "<tr><td colspan='1' align='left'>";
			if($ColumnSize>0)
			{
				if (!$Disabled) $x .= $ImportButton;
				$x .= $ExportButton;
				if (!$Disabled) $x .= "&nbsp;".$linterface->GET_SMALL_BTN($eReportCard['CalculateOverallResult'], 'button', "javascript:jSUBMIT_FORM(0)");
			}
			else
				$x .= "&nbsp;";
			$x .= "</td>";
			$x .= "<td align='right' nowrap='nowrap'>";
			if (($IsMSSubmissionPeriod)&&(!$Disabled))
				$x .= "<a class='tablelink' href=\"javascript:self.location.href='ms_column_new.php?$param'\" ><img src=\"$image_path/$LAYOUT_SKIN/icon_new.gif\" border=0 align='absmiddle' title='".$eReportCard['AddColumn']."'>".$eReportCard['AddColumn']."</a>";
			$x .= "</td></tr>";	
			$x .= "<tr><td valign='top' nowrap='nowrap' colspan='2'>".$MarksheetTable."</td></tr>
					<tr><td align='right' colspan='2' nowrap='nowrap'><a class='tablelink' href='#top'>".$eReportCard['GoToTop']."</a></td></tr>";
			$x .= $MarkRemindRow;

			$ButtonRow = "<tr><td align='center' id='bottom'>";
			if ($Disabled) {
				$ButtonRow .= $linterface->GET_ACTION_BTN($button_close, "button", "javascript:self.close()");
			} else {
				if (!(!$IsMSSubmissionPeriod &&($ck_ReportCard_UserType!="ADMIN"))) {
					$ButtonRow .= $linterface->GET_ACTION_BTN($button_save, "button", "javascript:jSUBMIT_FORM(1)");
					$ButtonRow .= "&nbsp;";		
					$ButtonRow .= $linterface->GET_ACTION_BTN($eReportCard['SaveAndUpdateMainMS'], "button", "javascript:jSUBMIT_FORM(2)");
					$ButtonRow .= "&nbsp;";
					$ButtonRow .= $linterface->GET_ACTION_BTN($button_reset, "reset");
					$ButtonRow .= "&nbsp;";
				}
				$ButtonRow .= ($HaveComponent) ? $linterface->GET_ACTION_BTN($button_back, "button", "javascript:self.location.href='marksheet_edit1.php?$param&ParentSubjectID=$ParentSubjectID'") : $linterface->GET_ACTION_BTN($button_close, "button", "javascript:self.close()");
			}
			$ButtonRow .= "</td></tr>";
		}

################################################################

$linterface->LAYOUT_START();
?>
<script language="javascript">
var rowx = 0, coly = 0;		//init
var xno = "<?=$ColumnSize?>";yno = "<?=$StudentSize?>";		// set table size

var startContent = "";
var setStart = 1;

function setxy(jParX, jParY){
	rowx = jParX;
	coly = jParY;
	document.getElementById('mark['+ jParX +']['+ jParY +']').blur();
}

function paste_clipboard() {
		
	var temp1 = null;
	var text1 = null;
	var s = "";
			
	// you must use a 'text' field
	// (a 'hidden' text field will not show anything)
		
	temp1 = document.getElementById('text1').createTextRange();
	temp1.execCommand( 'Paste' );
		
	s = document.getElementById('text1').value; // clipboard is pasted to a string here	
	setStart = 0;
		
	document.getElementById('mark['+ (rowx) +']['+ (coly) +']').value = "";

	var row_array = s.split("\n");
	var row_num = 0;

	while (row_num < row_array.length)
	{		
		var col_array = row_array[row_num].split("\t");
		var col_num = 0;
		var xPos=0, yPos=0;
		while (col_num < col_array.length)
		{	
			if ((col_num == 0) && (row_num == 0)) startContent = col_array[0];
			if ((row_array[row_num] != "")&&(col_array[col_num] != ""))
			{
				var temp = document.getElementById('mark['+ (row_num + rowx) +']['+ (col_num + coly) +']');
				xPos = parseInt(row_num) + parseInt(rowx);
				yPos = parseInt(col_num) + parseInt(coly);
				if (document.getElementById('mark['+ xPos +']['+ yPos +']') != null)
				{
					document.getElementById('mark['+ xPos +']['+ yPos +']').value = col_array[col_num];			
				}
			}
			col_num+=1;
		}		
		row_num+=1;
	}	
}

function pasteTable(jPari, jParj) {
	setxy(jPari, jParj);
	paste_clipboard();
}

function jDELETE_COLUMN(jColumnID)
{
	obj = document.form1;
	if(confirm("<?=$eReportCard['RemoveItemAlert']?>"))
	{
		self.location.href = "<?=$PATH_WRT_ROOT?>home/eAdmin/StudentMgmt/reportcard/marksheet_submission/ms_column_remove.php?<?=$param?>&ColumnSize=<?=$ColumnSize?>&ColumnID=" + jColumnID;
	}
}

function jSUBMIT_FORM(jSubmitType)
{
	obj = document.form1;
	var jCheckResult = checkform(obj);
	if(jCheckResult==true)
	{
		obj.SubmitType.value = jSubmitType;
		obj.action = "<?=$PATH_WRT_ROOT?>home/eAdmin/StudentMgmt/reportcard/marksheet_submission/sub_marksheet_update.php";
		obj.submit();
	}
}

function jRESET_FORM()
{
	obj = document.form1;
	obj.action = "<?=$PATH_WRT_ROOT?>home/eAdmin/StudentMgmt/reportcard/marksheet_submission/sub_marksheet.php";
	obj.submit();
}

function checkform(obj)
{
	var jStudentList = obj.StudentIDList.value;
	var jStudentIDArray = jStudentList.split(",");

	var jColumnList = obj.ColumnIDList.value;
	var jColumnArray = jColumnList.split(",");
	var jValidResult = false;
	
	for(var i=0; i<jStudentIDArray.length; i++)
	{
		jSID = jStudentIDArray[i];
		for(var j=0; j<jColumnArray.length; j++)
		{
			jCID = jColumnArray[j];
			if (document.getElementById('mark_'+jSID+'_'+jCID) != null) {
				jValue = eval("obj.mark_"+jSID+"_"+jCID+".value");
				if(jValue!="" && jValue!="/" && jValue!="abs")
				{
					jValidResult = jIS_NUMERIC(jValue);
					if(jValidResult==false)
					{
						alert("<?=$eReportCard['InvalidInputMsg']?>");
						eval("obj.mark_"+jSID+"_"+jCID+".focus()");
						return false;
					}
				}	
			}		
		}
	}

	return true;
}

</script>

<form name="form1" action="" method="post">
<br />
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td id="top"><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
</tr>
<tr>
	<td>
		<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tr><td align="right" colspan="2"><?= $linterface->GET_SYS_MSG($Result)?></td></tr>
		<tr>
			<td colspan="2">
			<table width='100%' border='0' cellpadding="3" cellspacing="1">
			<tr><td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $eReportCard['ClassName']; ?></td><td class="tabletext" width="70%" nowrap="nowrap"><?=$ClassName?></td></tr>
			<tr><td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=($intranet_session_language=="en"?$eReportCard['EngSubject']:$eReportCard['ChiSubject']); ?></td><td class="tabletext" width="70%" nowrap="nowrap"><?=$SubjectName?></td></tr>
			<tr><td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $eReportCard['Column']; ?></td><td class="tabletext" width="70%" nowrap="nowrap"><?=$ColumnName?></td></tr>
			<tr><td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $eReportCard['Weight']; ?></td><td class="tabletext" width="70%" nowrap="nowrap"><?=$ColumnWeight?>%</td></tr>
			</table>
			</td>
		</tr>
		<tr><td class="dotline" colspan="2"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width=10 height=1></td></tr>
		<?=$x?>
		</table>
	</td>
</tr>
<tr>
	<td><table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
	<?=$ButtonRow?>
	</table>
	</td>
</tr>
</table>

<textarea style="height: 1px; width: 1px; visibility: hidden;" id="text1" name="text1"></textarea>
<input type="hidden" name="StudentIDList" value="<?=$StudentIDList?>" />
<input type="hidden" name="TargetStudentIDList" value="<?=$TargetStudentIDList?>" />
<input type="hidden" name="ReportColumnID" value="<?=$ReportColumnID?>" />
<input type="hidden" name="SubjectID" value="<?=$SubjectID?>" />
<input type="hidden" name="ClassID" value="<?=$ClassID?>" />
<input type="hidden" name="ColumnWeight" value="<?=$ColumnWeight?>" />
<input type="hidden" name="ColumnIDList" value="<?=$ColumnIDList?>" />
<input type="hidden" name="SubmitType" />

<p></p>
</form>
<?
	//echo(StopTimer());

		$linterface->LAYOUT_STOP();
		intranet_closedb();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>
