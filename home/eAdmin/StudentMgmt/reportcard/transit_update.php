<?php
$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
intranet_auth();
intranet_opendb();

$li = new libdb();

# Hide all the report templates
$sql = "UPDATE RC_REPORT_TEMPLATE SET RecordStatus = 0";
$success = $li->db_db_query($sql);

# remove marksheet collection period data
$sql = "DELETE FROM RC_MARKSHEET_COLLECTION";
$success = $li->db_db_query($sql);

# remove result verification period data
$sql = "DELETE FROM RC_MARKSHEET_VERIFICATION";
$success = $li->db_db_query($sql);

# remove teacher notification
$sql = "DELETE FROM RC_MARKSHEET_NOTIFICATION";
$success = $li->db_db_query($sql);

intranet_closedb();

$Result = ($success==1) ? "data_transit" : "data_transit_failed";
header("Location:transit.php?Result=$Result");
?>

