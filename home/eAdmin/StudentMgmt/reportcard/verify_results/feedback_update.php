<?php
$PATH_WRT_ROOT = "../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libreportcard.php");
intranet_auth();
intranet_opendb();

$li = new libdb();

# Insert record to RC_MARKSHEET_FEEDBACK
if(!empty($SubjectID))
{
	# Get Submission Year and Semester
	$libreportcard = new libreportcard();

	$Feedback = intranet_htmlspecialchars($Feedback);

	# Get student ClassName and ClassNumber
	$StudentClassArray = $libreportcard->GET_CLASS_BY_USERID($UserID);
	list($ClassName, $ClassNumber) = $StudentClassArray[$UserID];

	$sql = "INSERT INTO RC_MARKSHEET_FEEDBACK (SubjectID, StudentID, IsComment, Comment, Year, Semester, ClassName, ClassNumber, DateInput, DateModified) VALUES ('$SubjectID', '$UserID', 1, '$Feedback', '".$libreportcard->Year."', '".$libreportcard->Semester."', '".$CLassName."', '".$ClassNumber."', now(), now())";
	$success = $li->db_db_query($sql);
}

intranet_closedb();
$Result = ($success==1) ? "update" : "update_failed";
header("Location: index.php?Result=$Result");
?>
