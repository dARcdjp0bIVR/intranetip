<?php
$PATH_WRT_ROOT = "../../../../../";
$PageRight = "STUDENT";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcard.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable{$LAYOUT_SKIN}.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

if ($plugin['ReportCard'])
{
	include_once($PATH_WRT_ROOT."includes/libreportcard.php");
	$libreportcard = new libreportcard();
	$CurrentPage = "VerifyAssessmentResult";

	$MODULE_OBJ = $libreportcard->GET_MODULE_OBJ_ARR();
    if ($libreportcard->hasAccessRight($_SESSION['UserID']))
    {
        $linterface = new interface_html();

############################################################################################################

		# check whether now is in marksheet submission period
		$VerificationPeriod = $libreportcard->GET_MARKSHEET_VERIFICATION();
		list($StartDate, $EndDate) = $VerificationPeriod[0];
		$ValidPeriod = ($StartDate=="" || $EndDate=="") ? 0 : validatePeriod($StartDate, $EndDate);
		
		if($ValidPeriod)
		{
			$ReminderRow = "<tr><td class='tabletext' height='40'>".$eReportCard['VerifyResultRemind']."</td></tr>";
			
			$PeriodTable = "<tr><td>";
			$PeriodTable .= "<table border='0' width='100%' cellpadding='5' cellspacing='0'>";
			$PeriodTable .= "<tr><td class=\"tabletext\" nowrap='nowrap' colspan=3>".$eReportCard['ResultVerificationPeriod'].":</td></tr>";
			$PeriodTable .= "<tr><td class=\"tabletext formfieldtitle\" nowrap='nowrap'>".$eReportCard['StartDate']."</td><td class=\"tabletext\" nowrap='nowrap' width='70%'>".$StartDate."</td></tr>";
			$PeriodTable .= "<tr><td class=\"tabletext formfieldtitle\" nowrap='nowrap'>".$eReportCard['EndDate']."</td><td class=\"tabletext\" nowrap='nowrap' width='70%'>".$EndDate."</td></tr>";
			$PeriodTable .= "</table>";
			$PeriodTable .= "</td></tr>";
			$PeriodTable .= "<tr><td class=\"dotline\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/10x10.gif\" width=10 height=1></td></tr>";

			# Get Student Subjects
			$Content = "<tr><td align='center'>";
			$Content .= "<table width='100%' border='0' cellspacing='4' cellpadding='2'>";
			$ClassReport = $libreportcard->GET_STUDENT_CLASS_REPORT_ID();
			list($ClassID, $ReportID) = $ClassReport[0];
			
			# Get Report Cell Setting
			$SubjectScaleArray = $libreportcard->GET_SUBJECT_GRADINGS_RECORDS($ReportID);

			# Get mark type settings
			$ReportInfo = $libreportcard->GET_REPORT_TEMPLATE($ReportID);
			$Settings = $ReportInfo[4];
			$SettingArray = unserialize($Settings);
			$MarkTypeDisplay = $SettingArray['MarkTypeDisplay'];

			$ReportSubjectArray = $libreportcard->GET_STUDENT_REPORT_SUBJECT($ClassID, $ReportID);
			
			for($i=0; $i<sizeof($ReportSubjectArray); $i++)
			{
				list($SubjectID, $SubjectName) = $ReportSubjectArray[$i];
				
				# Get Student Feedback and Status
				list($IsConfirmed, $Feedback) = $libreportcard->GET_STUDENT_FEEDBACK_STATUS($SubjectID);
			
				$Content .= "<tr height='80' width='55%'>";
				$Content .= "<td class='tablegreenrow2' valign='top'>
						<table border=0 cellpadding=5 cellspacing=0 width='100%'>
						<tr><td class='tabletext' valign='top'>".$SubjectName."</td>
						";
				
				$SubjectScale = $SubjectScaleArray[trim($SubjectID)][1];
				$OverallResultArray = $libreportcard->GET_OVERALL_RESULT($SubjectID);
				if(!empty($OverallResultArray))
				{
					$OverallResultMark = $OverallResultArray["Mark"];
					$OverallResultGrade = $OverallResultArray["Grade"];
					if($SubjectScale=="S") {
						if($OverallResultMark<0)
							$OverallResult = $libreportcard->SpecialMarkArray[trim($OverallResultMark)];
						else
							$OverallResult = ($MarkTypeDisplay==2) ? $OverallResultGrade : $OverallResultMark;
					}
					else {
						$OverallResult = $OverallResultGrade;
					}

					$DetailsTableID = "tableDetails".$i;
					$CloseBtn = $linterface->GET_BTN($eReportCard['Close'], "button", "javascript:jDISPLAY_TABLE('$DetailsTableID', 'none');");
					
					$DetailsTable = $libreportcard->GET_RESULT_DETAILS_TABLE($ClassID, $SubjectID, $ReportID, $DetailsTableID, $CloseBtn, $MarkTypeDisplay, $OverallResult, $SubjectScale);

					$Content .= "<td class='tabletext' valign='top' align='right'><a class='tablegreenlink' href=\"javascript:jDISPLAY_TABLE('$DetailsTableID', 'block');\"><b>".$OverallResult."</b></a></td>";  
				}
				$Content .= "</tr></table>";
				$Content .= $DetailsTable;

				$Content .= "</td>";
				$Content .= "<td class='tablegreenrow tablerow2' valign='top' width='45%'>";
				$Content .= "<table border=0 width='100%' cellpadding=5 cellspacing=0>";
				
				if($IsConfirmed==0)
				{
					if(!empty($Feedback))
					{
						$ButtonRow = "<tr><td colspan=2 class='tabletext'><font color='red'>".$Feedback."</font></td></tr>";
					}
					else if(!empty($OverallResult))
					{	
						$FeedbackTableID = "tableFeedback".$i;
						$SubmitBtn = $linterface->GET_BTN($button_submit, "submit", "", "btnSubmit");
						$CancelBtn = $linterface->GET_BTN($button_cancel, "button", "javascript:jDISPLAY_TABLE('$FeedbackTableID', 'none');");
						$FeedbackTable = $libreportcard->GENERATE_FEEDBACK_TABLE($SubjectID, $FeedbackTableID, $SubmitBtn, $CancelBtn);
								
						$ButtonRow = "<tr><td align='center'>".$linterface->GET_BTN($eReportCard['Appeal'], "button", "javascript:jDISPLAY_TABLE('$FeedbackTableID', 'block');")."</td>";
						$ButtonRow .= "<td align='center'>".$linterface->GET_BTN($button_confirm, "button", "javascript:jCONFIRM($SubjectID);")."</td></tr>";
					}
					else
					{
						$ButtonRow = "<tr><td class='tabletext'>&nbsp;</td></tr>";
					}
				}
				else
				{
					$ButtonRow = "<tr><td align='center' class='tabletext'><font color='blue'>".$eReportCard['ConfirmedMsg']."</font></td></tr>";
				}
				
				$Content .= "<tr><td colspan=2>&nbsp;</td></tr>";
				$Content .= $ButtonRow;

				$Content .= "</table>";
				$Content .= $FeedbackTable;
				$Content .= "</td>";
				$Content .= "</tr>";
			}
			$Content .= "</table>";
			$Content .= "</td></tr>";
		}
		else
		{
			$Content = "<tr><td align=\"center\" class='tabletext'>".$eReportCard['ResultVerificationNotInPeriodMsg']."</td></tr>";
		}

############################################################################################################

# tag information
$TAGS_OBJ[] = array($eReportCard['VerifyAssessmentResult'], "", 0);

$linterface->LAYOUT_START();


?>
<SCRIPT LANGUAGE="Javascript">
function jDISPLAY_TABLE(jTableID, jStyle){
	
	var jCurrentStyle = document.getElementById(jTableID).style.display;
	var jNewStyle = "none";
	if (typeof(jStyle)!="undefined")
	{
		jNewStyle = jStyle;
	} else
	{
		jNewStyle = (jCurrentStyle=="none") ? "block" : "none";
	}

	document.getElementById(jTableID).style.display = jStyle;

	return;
}

function jCONFIRM(jSubjectID)
{
	if(confirm("<?=$eReportCard['ConfirmResultAlert']?>"))
	{
		self.location.href = "confirm_update.php?SubjectID="+jSubjectID;
	}
}

function jSUBMIT_FEEDBACK(obj)
{
	if(!check_text(obj.Feedback, "<?php echo $i_alert_pleasefillin.$eReportCard['Feedback']; ?>.")) return false;

	return true;
}
</SCRIPT>
<table width="96%" border="0" cellpadding="5" cellspacing="0">
<tr>
	<td><br />
		<table width="100%" border="0" cellpadding="5" cellspacing="0">
		<tr><td align="right"><?= $linterface->GET_SYS_MSG($Result)?></td></tr>
		<?=$ReminderRow?>
		<?=$PeriodTable?>
		<?=$Content?>
		</table>
	</td>
</tr>
</table>
<?
        $linterface->LAYOUT_STOP();
		intranet_closedb();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>