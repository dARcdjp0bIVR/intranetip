<?php

$ReportTypeArray[0] = "summary";
$ReportTypeArray[1] = "award";
$ReportTypeArray[2] = "merit";
$ReportTypeArray[3] = "eca";
$ReportTypeArray[4] = "remark";
$ReportTypeArray[5] = "interschool";
$ReportTypeArray[6] = "schoolservice";

//**************************************************
// New approach change on 25/1/2008 by Andy Chan
// Use config CSV files to load the required fields
// instead of using a hard-coded array
//**************************************************
for($i=0; $i<sizeof($ReportTypeArray); $i++) {
	$fileName = $PATH_WRT_ROOT."home/admin/reportcard/info_upload/".$ReportTypeArray[$i]."_config.csv";
	if (file_exists($fileName)) {
		// libdb.php must be included before libimporttext.php & libexporttext.php
		include_once($PATH_WRT_ROOT."includes/libimporttext.php");
		include_once($PATH_WRT_ROOT."includes/libexporttext.php");
		$limport = new libimporttext();
		$lexport = new libexporttext();
		
		$data = $limport->GET_IMPORT_TXT($fileName);
		$header = array_shift($data);
		// reset arrays
		$fieldName = array();
		$fieldType = array();
		$fieldLength = array();
		$sample1 = array();
		$sample2 = array();
		$fieldNameMap = array();
		$customNameMap = array();
		if (sizeof($data) > 0) {
			$customNameMap["REG_NO"] = "REG_NO";
			for($j=0; $j<sizeof($data); $j++) {
				$fieldName[] = $data[$j][0];
				$fieldType[] = $data[$j][1];
				$fieldLength[] = $data[$j][2];
				$sample1[] = $data[$j][3];
				$sample2[] = $data[$j][4];
				$fieldNameMap[$data[$j][0]] = $data[$j][5];
				$customNameMap[$eReportCard[$data[$j][5]]] = $data[$j][5];
			}
			$DefaultHeaderArray[$ReportTypeArray[$i]] = $fieldName;
			$DefaultHeaderType[$ReportTypeArray[$i]] = $fieldType;
			$DefaultHeaderLength[$ReportTypeArray[$i]] = $fieldLength;
			$DefaultSampleData1[$ReportTypeArray[$i]] = $sample1;
			$DefaultSampleData2[$ReportTypeArray[$i]] = $sample2;
			$HeaderSettingMap[$ReportTypeArray[$i]] = $fieldNameMap;
			
			$CustomSettingMap[$ReportTypeArray[$i]] = $customNameMap;
		}
	}
}

//*******************************************
// New approach coding end
//*******************************************

/*
##############################################################
Modified on 4/2/2008 by Andy Chan
Following code have been disabled, it should only affect pages
related to file uploading.
##############################################################
// Summary Header
$DefaultHeaderArray[0][1] = array("REGNO", "ABSENT", "LATE", "ABSENT_WO_LEAVE", "CONDUCT");
$DefaultHeaderArray[0][2] = array("REGNO","ABSENT","LATE","ABSENT_WO_LEAVE","CONDUCT","POLITENESS","BEHAVIOUR","APPLICATION","TIDINESS");
$DefaultHeaderArray[0][3] = array("REGNO", "ABSENT", "LATE", "ABSENT_WO_LEAVE", "CONDUCT");
$DefaultHeaderArray[0][4] = array("REGNO", "ABSENT", "LATE", "ABSENT_WO_LEAVE", "CONDUCT");
$DefaultHeaderArray[0][5] = array("REGNO","ABSENT","LATE","ABSENT_WO_LEAVE","CONDUCT","POLITENESS","BEHAVIOUR","APPLICATION","TIDINESS","MOTIVATION","SELF_CONFIDENCE","SELF_DISCIPLINE", "COURTESY", "HONESTY", "RESPONSIBILITY", "COOPERATION");
$DefaultHeaderArray[0][6] = array("REGNO", "ABSENT", "LATE", "ABSENT_WO_LEAVE", "CONDUCT");
$DefaultHeaderArray[0][7] = array("REGNO", "ABSENT", "LATE", "ABSENT_WO_LEAVE", "CONDUCT");

$HeaderLangMap[0]["ABSENT"] = $eReportCard['DaysAbsent'];
$HeaderLangMap[0]["LATE"] = $eReportCard['TimesLate'];
$HeaderLangMap[0]["ABSENT_WO_LEAVE"] = $eReportCard['AbsentWOLeave'];
$HeaderLangMap[0]["CONDUCT"] = $eReportCard['Conduct'];
$HeaderLangMap[0]["POLITENESS"] = $eReportCard['Politeness'];
$HeaderLangMap[0]["BEHAVIOUR"] = $eReportCard['Behaviour'];
$HeaderLangMap[0]["APPLICATION"] = $eReportCard['Application'];
$HeaderLangMap[0]["TIDINESS"] = $eReportCard['Tidiness'];
$HeaderLangMap[0]["MOTIVATION"] = $eReportCard['Motivation'];
$HeaderLangMap[0]["SELF_CONFIDENCE"] = $eReportCard['SelfConfidence'];
$HeaderLangMap[0]["SELF_DISCIPLINE"] = $eReportCard['SelfDiscipline'];
$HeaderLangMap[0]["COURTESY"] = $eReportCard['Courtesy'];
$HeaderLangMap[0]["HONESTY"] = $eReportCard['Honesty'];
$HeaderLangMap[0]["RESPONSIBILITY"] = $eReportCard['Responsibility'];
$HeaderLangMap[0]["COOPERATION"] = $eReportCard['Cooperation'];

// Award Header
$DefaultHeaderArray[1][1] = array("REGNO", "TITLE");
$DefaultHeaderArray[1][2] = array("REGNO", "TITLE");
$DefaultHeaderArray[1][3] = array("REGNO", "TITLE");
$DefaultHeaderArray[1][4] = array("REGNO", "TITLE");
$DefaultHeaderArray[1][5] = array("REGNO", "TITLE");
$DefaultHeaderArray[1][6] = array("REGNO", "TITLE");
$DefaultHeaderArray[1][7] = array("REGNO", "TITLE");

$HeaderLangMap[1]["TITLE"] = $eReportCard['Awards'];

// Merit Header
$DefaultHeaderArray[2][1] = array("REGNO", "TITLE");
$DefaultHeaderArray[2][2] = array("REGNO", "MERIT", "DEMERIT");
$DefaultHeaderArray[2][3] = array("REGNO", "TITLE");
$DefaultHeaderArray[2][4] = array("REGNO", "MERIT", "DEMERIT", "MINORCREDIT", "MAJORCREDIT", "MINORFAULT", "MAJORFAULT");
$DefaultHeaderArray[2][5] = array("REGNO", "MERIT", "DEMERIT", "MINORCREDIT", "MAJORCREDIT", "MINORFAULT", "MAJORFAULT");
$DefaultHeaderArray[2][6] = array("REGNO", "TITLE");
$DefaultHeaderArray[2][7] = array("REGNO", "MERIT", "DEMERIT", "MINORCREDIT", "MAJORCREDIT", "MINORFAULT", "MAJORFAULT");

$HeaderLangMap[2]["TITLE"] = $eReportCard['MeritsAndDemerits'];
$HeaderLangMap[2]["MERIT"] = $eReportCard['Merit'];
$HeaderLangMap[2]["DEMERIT"] = $eReportCard['Demerit'];
$HeaderLangMap[2]["MINORCREDIT"] = $eReportCard['MinorCredit'];
$HeaderLangMap[2]["MAJORCREDIT"] = $eReportCard['MajorCredit'];
$HeaderLangMap[2]["MINORFAULT"] = $eReportCard['MinorFault'];
$HeaderLangMap[2]["MAJORFAULT"] = $eReportCard['MajorFault'];

// ECA Header
$DefaultHeaderArray[3][1] = array("REGNO", "TITLE");
$DefaultHeaderArray[3][2] = array("REGNO", "TITLE");
$DefaultHeaderArray[3][3] = array("REGNO", "TITLE");
$DefaultHeaderArray[3][4] = array("REGNO", "TITLE", "GRADE");
$DefaultHeaderArray[3][5] = array("REGNO", "TITLE");
$DefaultHeaderArray[3][6] = array("REGNO", "TITLE");
$DefaultHeaderArray[3][7] = array("REGNO", "TITLE", "GRADE");

$HeaderLangMap[3]["TITLE"] = $eReportCard['ECA'];
$HeaderLangMap[3]["GRADE"] = $eReportCard['Grade'];

// Remark Header
$DefaultHeaderArray[4][1] = array("REGNO", "REMARK");
$DefaultHeaderArray[4][2] = array("REGNO", "REMARK");
$DefaultHeaderArray[4][3] = array("REGNO", "REMARK");
$DefaultHeaderArray[4][4] = array("REGNO", "REMARK");
$DefaultHeaderArray[4][5] = array("REGNO", "REMARK");
$DefaultHeaderArray[4][6] = array("REGNO", "REMARK");
$DefaultHeaderArray[4][7] = array("REGNO", "REMARK");

$HeaderLangMap[4]["REMARK"] = $eReportCard['Remark'];

// Inter-school Header
$DefaultHeaderArray[5][1] = array("REGNO", "TITLE");
$DefaultHeaderArray[5][2] = array("REGNO", "TITLE");
$DefaultHeaderArray[5][3] = array("REGNO", "TITLE");
$DefaultHeaderArray[5][4] = array("REGNO", "TITLE");
$DefaultHeaderArray[5][5] = array("REGNO", "TITLE");
$DefaultHeaderArray[5][6] = array("REGNO", "TITLE");
$DefaultHeaderArray[5][7] = array("REGNO", "TITLE");

$HeaderLangMap[5]["TITLE"] = $eReportCard['InterSchoolCompetition'];

// School Service Header
$DefaultHeaderArray[6][1] = array("REGNO", "TITLE");
$DefaultHeaderArray[6][2] = array("REGNO", "TITLE");
$DefaultHeaderArray[6][3] = array("REGNO", "TITLE");
$DefaultHeaderArray[6][4] = array("REGNO", "TITLE");
$DefaultHeaderArray[6][5] = array("REGNO", "TITLE");
$DefaultHeaderArray[6][6] = array("REGNO", "TITLE");
$DefaultHeaderArray[6][7] = array("REGNO", "TITLE");

$HeaderLangMap[6]["TITLE"] = $eReportCard['SchoolService'];
*/
$ExcludeInReport = array(1, 5, 6);

?>
