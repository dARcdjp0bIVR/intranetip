<?php
$PATH_WRT_ROOT = "../../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
//include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

########################################################################
########################## Function Start ##############################
function GET_CSV_FILE($ParFolderPath)
{
	if (file_exists($ParFolderPath))
	{
		$handle = opendir($ParFolderPath);
		while (($file = readdir($handle))!==false)
		{
			if($file!="." && $file!="..")
			{
				$SplitArr = explode(".", $file);
				$ext = substr(basename($file), (strpos(basename($file), ".")+1));
				if($ext=="csv")
				{
					$FileArray[] = $file;
				}
			}
		}
		if (is_array($FileArray) && sizeof($FileArray)>0)
		{
			sort($FileArray);
		}
		closedir($handle);
	}

	return $FileArray;
}

function GET_CONTENT_TABLE($ParFileArray)
{
	global $PATH_WRT_ROOT, $LAYOUT_SKIN;
	global $eReportCard, $button_upload, $button_remove, $button_remove_all;
	global $UploadType, $linterface, $lreportcard, $image_path, $TargetURLPath, $i_no_record_exists_msg;
	global $i_Profile_Year, $i_Profile_Semester, $i_ClassName, $i_general_all_classes, $i_ReportCard_All_Year;
	global $Year, $Semester, $ClassList;
	global $ck_ReportCard_UserType, $TeachingClassName;
	global $intranet_root;

	$FileArraySize = sizeof($ParFileArray);
	$upload_icon = $linterface->GET_LNK_UPLOAD("javascript:doUpload('".$UploadType."')");
	$remove_all_icon = (!empty($ParFileArray)) ? "&nbsp;&nbsp;".$linterface->GET_LNK_REMOVEALL("javascript:doRemove(".$UploadType.", 'ALL')") : "";
	if ($ck_ReportCard_UserType!="ADMIN") $remove_all_icon = "";

	$year_data = $lreportcard->GET_YEAR();
	if($Year == "")
   		$Year = 0;
	$YearObj = new academic_year($Year);
	$YearName = $YearObj->YearNameEN;
	
	if(!empty($year_data))
	{
		$YearSelect = "<SELECT name='Year' onChange=\"doFilter()\">";
		$YearSelect .= "<OPTION value='0' ".($Year==0?"SELECTED='SELECTED'" : "").">".$i_ReportCard_All_Year."</OPTION>";
		for($i=0; $i<sizeof($year_data); $i++)
		{
			$Selected = ($year_data[$i]['Year'] == $Year) ? "SELECTED='SELECTED'" : "";
			$YearSelect .= "<OPTION value='".$year_data[$i]['Year']."' $Selected>".$year_data[$i]['YearName']."</OPTION>";
		}
		$YearSelect .= "</SELECT>";
	}

	$semester_data = getSemesters($Year, 0);
	if($Semester == "")
    	$Semester = 0;

	if(!empty($semester_data))
	{
		$SemesterSelect = "<SELECT name='Semester' onChange=\"doFilter()\">";
		$SemesterSelect .= "<OPTION value='0' ".($Semester==0?"SELECTED='SELECTED'" : "").">".$eReportCard['FullYear']."</OPTION>";
		for($i=0; $i<sizeof($semester_data); $i++)
		{
			$thisYearTermID = $semester_data[$i]['YearTermID'];
			$thisTermName = $semester_data[$i]['TermName'];
			
			$Selected = ($thisYearTermID==$Semester) ? "SELECTED='SELECTED'" : "";
			$SemesterSelect .= "<OPTION value='".$thisYearTermID."' $Selected>".$thisTermName."</OPTION>";
		}
		$SemesterSelect .= "</SELECT>";
	}

	$lclass = new libclass();
	if ($ck_ReportCard_UserType=="ADMIN")
	{
		$class = $lclass->getClassList($lreportcard->Year);
	} else
	{
		/*
		$sql1 = "SELECT ic.ClassID, ic.ClassName, ic.ClassLevelID
				FROM INTRANET_CLASS AS ic, INTRANET_CLASSTEACHER AS ict
				WHERE ic.RecordStatus = 1 AND ict.ClassID=ic.ClassID AND ict.UserID='$UserID' ORDER BY ic.ClassName";
		$class = $lclass->returnArray($sql1,3);
		*/
		$ClassNameField = Get_Lang_Selection('yc.ClassTitleB5', 'yc.ClassTitleEN');
		$sql1 = "SELECT
					yc.YearClassID as ClassID,
					$ClassNameField as ClassName,
					yc.YearID as ClassLevelID,
					yc.ClassTitleEN
				FROM
					YEAR_CLASS_TEACHER as yct
					INNER JOIN
					YEAR_CLASS as yc
					ON (yct.YearClassID = yc.YearClassID AND yc.AcademicYearID = '".$lreportcard->Year."' )
				WHERE
					yct.UserID = '".$_SESSION['UserID']."'
				";
		$class = $lreportcard->returnArray($sql1);
		
		//debug_r($class_arr1);
		$ClassList = $class[0]['ClassTitleEN'];
	}
	
	if($ClassList == "")
    	$ClassList = 0;

	if(!empty($class))
	{
		$ClassSelect = "<SELECT name='ClassList' onChange=\"doFilter()\">";
		if (sizeof($class)>1)
		{
			$ClassSelect .= "<OPTION value='0' ".($ClassList==0?"SELECTED='SELECTED'" : "").">".$i_general_all_classes."</OPTION>";
		}
		for($i=0; $i<sizeof($class); $i++)
		{
		  $Selected = strcmp($class[$i]['ClassName'], $ClassList) == 0? "SELECTED='SELECTED'" : "";
			$ClassSelect .= "<OPTION value='".$class[$i]['ClassName']."' ".$Selected.">".$class[$i]['ClassName']."</OPTION>";

//			DEBUG($class[$i]['ClassName']." ".$ClassList);
		}
		$ClassSelect .= "</SELECT>";
	}
//	DIE();
//	debug($ClassList);

	$ReturnValue = "<table width=\"90%\" border=\"0\" cellpadding=\"4\" cellspacing=\"0\">";
	$ReturnValue .= "<tr><td class='tabletext' colspan='3' align='left'>".$upload_icon.$remove_all_icon."</td></tr>";
	$ReturnValue .= "<tr class='tablegreentop'>";
	$ReturnValue .= "<td class=\"tableTitle tabletoplink\" width='1' align='center'>#</td>";
	$ReturnValue .= "<td class=\"tableTitle tabletoplink\" width='100'>".$eReportCard['File']."</td>";
	$ReturnValue .= "<td class=\"tableTitle tabletoplink\" width='25%'>".$i_Profile_Year." ".$YearSelect."</td>";
	$ReturnValue .= "<td class=\"tableTitle tabletoplink\" width='25%'>".$i_Profile_Semester." ".$SemesterSelect."</td>";
	$ReturnValue .= "<td class=\"tableTitle tabletoplink\" width='25%'>".$i_ClassName." ".$ClassSelect."</td>";
	$ReturnValue .= "<td class=\"tableTitle tabletoplink\" width='1' align='center'>&nbsp;</td>";
	$ReturnValue .= "</tr>";
	$DisplayCount = 0;

	if(!empty($ParFileArray))
	{
		for($i=0; $i<$FileArraySize; $i++)
		{
			$css = ($i%2?"2":"");
			$file = $ParFileArray[$i];

			$semester_data = getSemesters();
			//$semester_select = substr($file, strpos($file,"_")+1 , 1);
			$TempArr = explode('_', $file);
			$semester_select = $TempArr[1];
			$t_semester = $semester_select==0? $eReportCard['FullYear'] : $semester_data[$semester_select];

      if($Year == 0 || substr($file,0,strpos($file,"_")) == $YearName)
      {
        if($Semester == 0 || $semester_select == $Semester)
        {
          if($ClassList == 0 || strcmp(substr($file,strrpos($file,"_")+1,strrpos($file,".")-strrpos($file,"_")-1), $ClassList)== 0)
          {
      			//if (($ck_ReportCard_UserType!="ADMIN")&&(!preg_match("/".addslashes($TeachingClassName)."/", $file)))
      			if (($ck_ReportCard_UserType!="ADMIN")&&(!preg_match("/".str_replace('/', '', $TeachingClassName)."/", $file)))
      				continue;
      				
      			$fileName = $file;
      			$downloadFileName = str_replace('_unicode', '', $fileName);
      			$thisHref = GET_CSV($downloadFileName, $intranet_root.'/'.$TargetURLPath."/", 0);

      			$ReturnValue .= "<tr class='tablegreenrow".$css."'>";
      			$ReturnValue .= "<td class='tabletext' align='center' width='1'>".($i+1)."</td>";
      			$ReturnValue .= "<td class='tabletext'><a href='".$thisHref."' target='_blank' class='tablelink'><img src='".$PATH_WRT_ROOT."/images/{$LAYOUT_SKIN}/icon_files/xls.gif' border='0' hspace='5' align='absmiddle' /></a></td>";
      			$ReturnValue .= "<td class='tabletext'>".substr($file,0,strpos($file,"_"))."</td>";
      			$ReturnValue .= "<td class='tabletext'>".$t_semester."</td>";
      			$ReturnValue .= "<td class='tabletext'>".substr($file,strrpos($file,"_")+1,strrpos($file,".")-strrpos($file,"_")-1)."</td>";
      			$ReturnValue .= "<td class='tabletext' align='center' width='1'><a href=\"javascript:doRemove('".$UploadType."', '".$file."')\"><img src=\"".$PATH_WRT_ROOT."/images/{$LAYOUT_SKIN}/icon_delete.gif\" border=\"0\" align=\"absmiddle\" title='".$button_remove."'></a></td>";
      			$ReturnValue .= "</tr>";
      			$DisplayCount++;
      		}
  			}
  		}
		}
	}
	else {
		$ReturnValue .= "<tr><td class='tabletext' colspan='3' align='center'>".$i_no_record_exists_msg."</td></tr>";
		$DisplayCount++;
	}
	if ($DisplayCount == 0)
		$ReturnValue .= "<tr><td class='tabletext' colspan='3' align='center'>".$i_no_record_exists_msg."</td></tr>";
	$ReturnValue .= "</table>";

	return $ReturnValue;
}

########################## Function End ##############################
########################################################################

if ($plugin['ReportCard'])
{
	include_once($PATH_WRT_ROOT."includes/libreportcard.php");
	$lreportcard = new libreportcard();
	$TeachingClassName = $lreportcard->GET_TEACHING_CLASS();
	$CurrentPage = "InfoUpload";
	$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
    if ($lreportcard->hasAccessRight())
    {
        $linterface = new interface_html();

		##############################################################
		######################## Contents ############################
		$li = new libfilesystem();

		$TargetFolderPath = $intranet_root."/file/reportcard/";
		$TargetURLPath = "/file/reportcard/";
		if($UploadType=="")
			$UploadType = 0;

		switch ($UploadType) {
			case 0:
				$TargetFolderPath = $TargetFolderPath."summary";
				$TargetURLPath = $TargetURLPath."summary";
				break;
			case 1:
				$TargetFolderPath = $TargetFolderPath."award";
				$TargetURLPath = $TargetURLPath."award";
				break;
			case 2:
				$TargetFolderPath = $TargetFolderPath."merit";
				$TargetURLPath = $TargetURLPath."merit";
				break;
			case 3:
				$TargetFolderPath = $TargetFolderPath."eca";
				$TargetURLPath = $TargetURLPath."eca";
				break;
			case 4:
				$TargetFolderPath = $TargetFolderPath."remark";
				$TargetURLPath = $TargetURLPath."remark";
				break;
			case 5:
				$TargetFolderPath = $TargetFolderPath."interschool";
				$TargetURLPath = $TargetURLPath."interschool";
				break;
			case 6:
				$TargetFolderPath = $TargetFolderPath."schoolservice";
				$TargetURLPath = $TargetURLPath."schoolservice";
				break;
		}
		$TargetFileArray = GET_CSV_FILE($TargetFolderPath);
		$DisplayTable = GET_CONTENT_TABLE($TargetFileArray);

		##############################################################
		${"link".$UploadType} = 1;

		# tag information
		$TAGS_OBJ[] = array($eReportCard['SummaryInfoUpload'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/reportcard/info_upload/?UploadType=0", $link0);
		$TAGS_OBJ[] = array($eReportCard['AwardRecordUpload'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/reportcard/info_upload/?UploadType=1", $link1);
		if ($ReportCardTemplate!="2")
		{
			$TAGS_OBJ[] = array($eReportCard['MeritRecordUpload'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/reportcard/info_upload/?UploadType=2", $link2);
		}
		$TAGS_OBJ[] = array($eReportCard['ECARecordUpload'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/reportcard/info_upload/?UploadType=3", $link3);
		$TAGS_OBJ[] = array($eReportCard['RemarkUpload'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/reportcard/info_upload/?UploadType=4", $link4);
		if ($ReportCardTemplate!="2")
		{
			$TAGS_OBJ[] = array($eReportCard['InterSchoolCompetitionUpload'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/reportcard/info_upload/?UploadType=5", $link5);
			$TAGS_OBJ[] = array($eReportCard['SchoolServiceUpload'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/reportcard/info_upload/?UploadType=6", $link6);
		}

		$linterface->LAYOUT_START();

		?>

<script language="javascript">
function doUpload(jUploadType){
	document.form1.UploadType.value = jUploadType;
	document.form1.action = "file_upload.php";
	document.form1.submit();
	//newWindow("file_upload.php?UploadType="+jUploadType, 11);
}

function doRemove(jUploadType, jFileName){
	var jConfirmMsg = (jFileName=='ALL') ? "<?=$eReportCard['FileRemoveAllConfirm']?>" : "<?=$eReportCard['FileRemoveConfirm']?>";
	if(confirm(jConfirmMsg))
	{
		document.form1.UploadType.value = jUploadType;
		document.form1.FileName.value = jFileName;
		document.form1.action = "file_remove_update.php";
		document.form1.submit();
	}
}

function doFilter()
{
	document.form1.action = "index.php";
	document.form1.submit();
}

</script>


		<br />
		<form name="form1" method="post">
		<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="4">
		<tr><td align="right" colspan="<?=$col_size?>"><?= $linterface->GET_SYS_MSG($Result)?></td></tr>
			<tr>
				<td align='center'>
				<?=$DisplayTable?>
				</td>
			</tr>
		</table>
		<input type="hidden" name="UploadType" />
		<input type="hidden" name="FileName" />
		</form>
	<?

        $linterface->LAYOUT_STOP();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>
