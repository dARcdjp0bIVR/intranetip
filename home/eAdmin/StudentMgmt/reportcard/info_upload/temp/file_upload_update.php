<?php
$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libreportcard.php");

$fs = new libfilesystem();
intranet_opendb();
$lreportcard = new libreportcard();

$TeachingClassName = $lreportcard->GET_TEACHING_CLASS();

//if (!(($ck_ReportCard_UserType!="ADMIN")&&(!preg_match("/".str_replace('/', '', $TeachingClassName)."/", $file)))) {
if (
	(($ck_ReportCard_UserType=="TEACHER")&&(preg_match("/".str_replace('/', '', $TeachingClassName)."/", $userfile_name)))
	|| ($ck_ReportCard_UserType=="ADMIN"))
	{
	if(!empty($userfile))
	{
		switch ($UploadType) {
				case 0:
					$folder_prefix = $intranet_root."/file/reportcard/summary";
					$file_format = array("REGNO","ABSENT","LATE","CONDUCT");
					break;
				case 1:
					$folder_prefix = $intranet_root."/file/reportcard/award";
					$file_format = array("REGNO","TITLE");
					break;
				case 2:
					$folder_prefix = $intranet_root."/file/reportcard/merit";
					$file_format = array("REGNO","TITLE");
					break;
				case 3:
					$folder_prefix = $intranet_root."/file/reportcard/eca";
					$file_format = array("REGNO","TITLE");
					break;
				case 4:
					$folder_prefix = $intranet_root."/file/reportcard/remark";
					$file_format = array("REGNO","REMARK");
					break;
				case 5:
					$folder_prefix = $intranet_root."/file/reportcard/interschool";
					$file_format = array("REGNO","TITLE");
					break;
				case 6:
					$folder_prefix = $intranet_root."/file/reportcard/schoolservice";
					$file_format = array("REGNO","TITLE");
					break;
		}
		$fs->folder_new($folder_prefix);	
	
		$loc = $userfile;
		$filename = $userfile_name;
		$success = 0;
		if($filename!="")
		{			
			$ext = $fs->file_ext($filename);
			if($ext==".csv")
			{
				if ($loc!="none" && file_exists($loc))
				{
					if(strpos($filename, ".")!=0)
					{
						$success = $fs->file_copy($loc, stripslashes($folder_prefix."/".$filename));
					}
				}
			}
		}
	}
	
	$Result = ($success==1) ? "upload" : "upload_failed";
	
} else {
	$Result = "upload_failed";
}

header("Location: index.php?Result=$Result&UploadType=$UploadType");
?>