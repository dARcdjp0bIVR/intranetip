<?php
$PATH_WRT_ROOT = "../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

if ($plugin['ReportCard'])
{
	include_once($PATH_WRT_ROOT."includes/libreportcard.php");
	$lreportcard = new libreportcard();
	$CurrentPage = "InfoUpload";
	$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
	//$MODULE_OBJ['title'] = $RecordTitle;

	switch ($UploadType) {
		case 0:
			$RecordTitle = $eReportCard['SummaryInfoUpload'];
			$SampleCSV = "summary_sample.csv";
			break;
		case 1:
			$RecordTitle = $eReportCard['AwardRecordUpload'];
			$SampleCSV = "award_sample.csv";
			break;
		case 2:
			$RecordTitle = $eReportCard['MeritRecordUpload'];
			$SampleCSV = "merit_sample.csv";
			if ($ReportCardTemplate == 2) $SampleCSV = "merit_sample".$ReportCardTemplate.".csv";
			if ($ReportCardTemplate >= 4) $SampleCSV = "merit_sample2.csv";
			break;
		case 3:
			$RecordTitle = $eReportCard['ECARecordUpload'];
			$SampleCSV = "eca_sample.csv";
			if ($ReportCardTemplate == 4) $SampleCSV = "eca_sample".$ReportCardTemplate.".csv";
			break;
		case 4:
			$RecordTitle = $eReportCard['RemarkUpload'];
			$SampleCSV = "remark_sample.csv";
			break;
		case 5:
			$RecordTitle = $eReportCard['InterSchoolCompetitionUpload'];
			$SampleCSV = "interschool_sample.csv";
			break;
		case 6:
			$RecordTitle = $eReportCard['SchoolServiceUpload'];
			$SampleCSV = "schoolservice_sample.csv";
			break;
	}
	
	if ($lreportcard->hasAccessRight())
    {
         $linterface = new interface_html();

################################################################
		
		# generate table
		$UploadTableRow = "";
		$UploadTableRow = "<tr>
							  <td height=\"10\" colspan=\"2\"><span class='tabletextremark'>".$eReportCard['FileUploadRemind']."</span></td>
							</tr>";
		$UploadTableRow .= "<tr>
									<td valgin='top' class='tabletext formfieldtitle' width='30%'>".$eReportCard['File']."</td>
									<td valgin='top'><input class=\"textboxtext\" type='file' name='userfile' size=\"25\"  maxlength=\"255\">
									";
		
		if($g_encoding_unicode)
		{
		$UploadTableRow .= "<span class='tabletextremark'>$i_import_utf_type</span>";
		}
		
		$UploadTableRow .= "</tr>";
		$UploadTableRow .= "<tr><td nowrap='nowrap' colspan='2'><a target='_blank' href='".$SampleCSV."' class='tablelink'><img 	src='".$PATH_WRT_ROOT."/images/2007a/icon_files/xls.gif' border='0' align='absmiddle' hspace='3'>".$eReportCard['DownloadCSVTemplate']."</td></tr>";

		$ButtonHTML = $linterface->GET_ACTION_BTN($button_submit, "submit", "", "btnSubmit");
		$ButtonHTML .= "&nbsp;".$linterface->GET_ACTION_BTN($button_reset, "reset", "");
		$ButtonHTML .= "&nbsp;".$linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:history.back()");

################################################################
		${"link".$UploadType} = 1;

		# tag information
		$TAGS_OBJ[] = array($eReportCard['SummaryInfoUpload'], $PATH_WRT_ROOT."home/admin/reportcard/info_upload/?UploadType=0", $link0);
		$TAGS_OBJ[] = array($eReportCard['AwardRecordUpload'], $PATH_WRT_ROOT."home/admin/reportcard/info_upload/?UploadType=1", $link1);
		$TAGS_OBJ[] = array($eReportCard['MeritRecordUpload'], $PATH_WRT_ROOT."home/admin/reportcard/info_upload/?UploadType=2", $link2);
		$TAGS_OBJ[] = array($eReportCard['ECARecordUpload'], $PATH_WRT_ROOT."home/admin/reportcard/info_upload/?UploadType=3", $link3);
		$TAGS_OBJ[] = array($eReportCard['RemarkUpload'], $PATH_WRT_ROOT."home/admin/reportcard/info_upload/?UploadType=4", $link4);
		$TAGS_OBJ[] = array($eReportCard['InterSchoolCompetitionUpload'], $PATH_WRT_ROOT."home/admin/reportcard/info_upload/?UploadType=5", $link5);
		$TAGS_OBJ[] = array($eReportCard['SchoolServiceUpload'], $PATH_WRT_ROOT."home/admin/reportcard/info_upload/?UploadType=6", $link6);

		# page navigation (leave the array empty if no need)
		$PAGE_NAVIGATION[] = array($button_upload, "");

		$linterface->LAYOUT_START();
?>
<?=$js_reload?>
<form name="form1" enctype="multipart/form-data" action="file_upload_update.php" method="post">
<br />
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
</tr>
<tr>
	<td>
		<table width="90%" border="0" cellpadding="4" cellspacing="1" align="center">
			<?=$UploadTableRow?>
		</table>
	</td>
</tr>
<tr>
	<td><table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
		<tr>
			<td align="center"><?=$ButtonHTML?></td>
		</tr>
	</table>
	</td>
</tr>
</table>
<input type="hidden" name="UploadType" value="<?=$UploadType?>" />
</form>
<?

        $linterface->LAYOUT_STOP();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>
