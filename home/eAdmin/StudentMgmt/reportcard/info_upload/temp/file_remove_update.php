<?php
$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

#######################################################
function removeFiles($ParAll, $ParFileName="")
{
	global $fs, $intranet_root, $RecordType, $admin_root_path, $UploadType;
	
	$TargetFolderPath = $intranet_root."/file/reportcard/";
	switch ($UploadType) {
			case 0:
				$TargetFolderPath = $TargetFolderPath."summary";
				break;
			case 1:
				$TargetFolderPath = $TargetFolderPath."award";
				break;
			case 2:
				$TargetFolderPath = $TargetFolderPath."merit";
				break;
			case 3:
				$TargetFolderPath = $TargetFolderPath."eca";
				break;
			case 4:
				$TargetFolderPath = $TargetFolderPath."remark";
				break;
			case 5:
				$TargetFolderPath = $TargetFolderPath."interschool";
				break;
			case 6:
				$TargetFolderPath = $TargetFolderPath."schoolservice";
				break;
	}

	if (file_exists($TargetFolderPath))
	{
		$handle = opendir($TargetFolderPath);
		if($ParAll==1)
		{
			while (($file = readdir($handle))!==false) 
			{
				if($file!="." && $file!="..")
				{
					$filepath = $TargetFolderPath."/".$file;
					$success = $fs->file_remove($filepath);
				}
			}
		}
		else
		{
			$filepath = $TargetFolderPath."/".$ParFileName;
			if(file_exists($filepath)) {
				$success = $fs->file_remove($filepath);
			}
		}
	}

	return $success;
}

#######################################################

$fs = new libfilesystem();

if($FileName=="ALL") {
	$success = removeFiles(1);
}
else {
	$success = removeFiles(0, $FileName);
}

$Result = ($success==1) ? "delete" : "delete_failed";
header("Location: index.php?Result=$Result&UploadType=$UploadType");
?>