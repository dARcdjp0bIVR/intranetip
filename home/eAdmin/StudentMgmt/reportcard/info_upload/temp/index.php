<?php
$PATH_WRT_ROOT = "../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

########################################################################
########################## Function Start ##############################
function GET_CSV_FILE($ParFolderPath)
{
	if (file_exists($ParFolderPath))
	{
		$handle = opendir($ParFolderPath);
		while (($file = readdir($handle))!==false)
		{
			if($file!="." && $file!="..")
			{
				$SplitArr = explode(".", $file);
				$ext = substr(basename($file), (strpos(basename($file), ".")+1));
				if($ext=="csv")
				{
					$FileArray[] = $file;
				}
			}
		}
		if (is_array($FileArray) && sizeof($FileArray)>0)
		{
			sort($FileArray);
		}
		closedir($handle);
	}

	return $FileArray;
}

function GET_CONTENT_TABLE($ParFileArray)
{
	global $eReportCard, $button_upload, $button_remove, $button_remove_all;
	global $UploadType, $linterface, $image_path, $TargetURLPath, $i_no_record_exists_msg;
	global $ck_ReportCard_UserType, $TeachingClassName;

	$FileArraySize = sizeof($ParFileArray);
	$upload_icon = $linterface->GET_LNK_UPLOAD("javascript:doUpload('".$UploadType."')");
	$remove_all_icon = (!empty($ParFileArray)) ? "&nbsp;&nbsp;".$linterface->GET_LNK_REMOVEALL("javascript:doRemove(".$UploadType.", 'ALL')") : "";
	if ($ck_ReportCard_UserType!="ADMIN") $remove_all_icon = "";

	$ReturnValue = "<table width=\"90%\" border=\"0\" cellpadding=\"4\" cellspacing=\"0\">";
	$ReturnValue .= "<tr><td class='tabletext' colspan='3' align='left'>".$upload_icon.$remove_all_icon."</td></tr>";
	$ReturnValue .= "<tr class='tablegreentop'>";
	$ReturnValue .= "<td class=\"tableTitle tabletoplink\" width='1' align='center'>#</td>";
	$ReturnValue .= "<td class=\"tableTitle tabletoplink\" width='100%'>".$eReportCard['File']."</td>";
	$ReturnValue .= "<td class=\"tableTitle tabletoplink\" width='1' align='center'>&nbsp;</td>";
	$ReturnValue .= "</tr>";
	$DisplayCount = 0;

	if(!empty($ParFileArray))
	{
		for($i=0; $i<$FileArraySize; $i++)
		{
			$css = ($i%2?"2":"");
			$file = $ParFileArray[$i];

			//if (($ck_ReportCard_UserType!="ADMIN")&&(!preg_match("/".addslashes($TeachingClassName)."/", $file)))
			if (($ck_ReportCard_UserType!="ADMIN")&&(!preg_match("/".str_replace('/', '', $TeachingClassName)."/", $file)))
				continue;

			$ReturnValue .= "<tr class='tablegreenrow".$css."'>";
			$ReturnValue .= "<td class='tabletext' align='center' width='1'>".($i+1)."</td>";
			$ReturnValue .= "<td class='tabletext'><a href='".$TargetURLPath."/".$file."' target='_blank' class='tablelink'><img src='".$PATH_WRT_ROOT."/images/2007a/icon_files/xls.gif' border='0' hspace='5' align='absmiddle' />".$file."</a></td>";
			$ReturnValue .= "<td class='tabletext' align='center' width='1'><a href=\"javascript:doRemove('".$UploadType."', '".$file."')\"><img src=\"".$PATH_WRT_ROOT."/images/2007a/icon_delete.gif\" border=\"0\" align=\"absmiddle\" title='".$button_remove."'></a></td>";
			$ReturnValue .= "</tr>";
			$DisplayCount++;
		}
	}
	else {
		$ReturnValue .= "<tr><td class='tabletext' colspan='3' align='center'>".$i_no_record_exists_msg."</td></tr>";
		$DisplayCount++;
	}
	if ($DisplayCount == 0)
		$ReturnValue .= "<tr><td class='tabletext' colspan='3' align='center'>".$i_no_record_exists_msg."</td></tr>";
	$ReturnValue .= "</table>";

	return $ReturnValue;
}

########################## Function End ##############################
########################################################################

if ($plugin['ReportCard'])
{
	include_once($PATH_WRT_ROOT."includes/libreportcard.php");
	$lreportcard = new libreportcard();
	$TeachingClassName = $lreportcard->GET_TEACHING_CLASS();
	$CurrentPage = "InfoUpload";
	$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
    if ($lreportcard->hasAccessRight())
    {
        $linterface = new interface_html();

		##############################################################
		######################## Contents ############################
		$li = new libfilesystem();

		$TargetFolderPath = $intranet_root."/file/reportcard/";
		$TargetURLPath = "/file/reportcard/";
		if($UploadType=="")
			$UploadType = 0;

		switch ($UploadType) {
			case 0:
				$TargetFolderPath = $TargetFolderPath."summary";
				$TargetURLPath = $TargetURLPath."summary";
				break;
			case 1:
				$TargetFolderPath = $TargetFolderPath."award";
				$TargetURLPath = $TargetURLPath."award";
				break;
			case 2:
				$TargetFolderPath = $TargetFolderPath."merit";
				$TargetURLPath = $TargetURLPath."merit";
				break;
			case 3:
				$TargetFolderPath = $TargetFolderPath."eca";
				$TargetURLPath = $TargetURLPath."eca";
				break;
			case 4:
				$TargetFolderPath = $TargetFolderPath."remark";
				$TargetURLPath = $TargetURLPath."remark";
				break;
			case 5:
				$TargetFolderPath = $TargetFolderPath."interschool";
				$TargetURLPath = $TargetURLPath."interschool";
				break;
			case 6:
				$TargetFolderPath = $TargetFolderPath."schoolservice";
				$TargetURLPath = $TargetURLPath."schoolservice";
				break;
		}
		$TargetFileArray = GET_CSV_FILE($TargetFolderPath);
		$DisplayTable = GET_CONTENT_TABLE($TargetFileArray);

		##############################################################
		${"link".$UploadType} = 1;

		# tag information
		$TAGS_OBJ[] = array($eReportCard['SummaryInfoUpload'], $PATH_WRT_ROOT."home/admin/reportcard/info_upload/?UploadType=0", $link0);
		$TAGS_OBJ[] = array($eReportCard['AwardRecordUpload'], $PATH_WRT_ROOT."home/admin/reportcard/info_upload/?UploadType=1", $link1);
		$TAGS_OBJ[] = array($eReportCard['MeritRecordUpload'], $PATH_WRT_ROOT."home/admin/reportcard/info_upload/?UploadType=2", $link2);
		$TAGS_OBJ[] = array($eReportCard['ECARecordUpload'], $PATH_WRT_ROOT."home/admin/reportcard/info_upload/?UploadType=3", $link3);
		$TAGS_OBJ[] = array($eReportCard['RemarkUpload'], $PATH_WRT_ROOT."home/admin/reportcard/info_upload/?UploadType=4", $link4);
		$TAGS_OBJ[] = array($eReportCard['InterSchoolCompetitionUpload'], $PATH_WRT_ROOT."home/admin/reportcard/info_upload/?UploadType=5", $link5);
		$TAGS_OBJ[] = array($eReportCard['SchoolServiceUpload'], $PATH_WRT_ROOT."home/admin/reportcard/info_upload/?UploadType=6", $link6);

		$linterface->LAYOUT_START();

		?>

<script language="javascript">
function doUpload(jUploadType){
	document.form1.UploadType.value = jUploadType;
	document.form1.action = "file_upload.php";
	document.form1.submit();
	//newWindow("file_upload.php?UploadType="+jUploadType, 11);
}

function doRemove(jUploadType, jFileName){
	var jConfirmMsg = (jFileName=='ALL') ? "<?=$eReportCard['FileRemoveAllConfirm']?>" : "<?=$eReportCard['FileRemoveConfirm']?>";
	if(confirm(jConfirmMsg))
	{
		document.form1.UploadType.value = jUploadType;
		document.form1.FileName.value = jFileName;
		document.form1.action = "file_remove_update.php";
		document.form1.submit();
	}
}
</script>
		<br />
		<form name="form1" method="post">
		<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="4">
		<tr><td align="right" colspan="<?=$col_size?>"><?= $linterface->GET_SYS_MSG($Result)?></td></tr>
			<tr>
				<td align='center'>
				<?=$DisplayTable?>
				</td>
			</tr>
		</table>
		<input type="hidden" name="UploadType" />
		<input type="hidden" name="FileName" />
		</form>
	<?

        $linterface->LAYOUT_STOP();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>
