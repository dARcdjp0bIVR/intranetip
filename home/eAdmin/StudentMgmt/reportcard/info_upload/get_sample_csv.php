<?php
$PATH_WRT_ROOT = "../../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if ($plugin['ReportCard']) {
	include_once($PATH_WRT_ROOT."includes/libreportcard.php");
	$lreportcard = new libreportcard();
	
	if ($lreportcard->hasAccessRight()) {
		include_once("../default_header_revamp.php");
		
		$fieldName = $DefaultHeaderArray[$ReportTypeArray[$UploadType]];
		$sampleData[0] = $DefaultSampleData1[$ReportTypeArray[$UploadType]];
		$sampleData[1] = $DefaultSampleData2[$ReportTypeArray[$UploadType]];
		
		$filename = $ReportTypeArray[$UploadType]."_sample.csv";
		
		// Change header wording to display wording (or user-customized)
		for($i=0; $i<sizeof($fieldName); $i++) {
			$displayName = trim($eReportCard[$HeaderSettingMap[$ReportTypeArray[$UploadType]][$fieldName[$i]]]);
			if ($displayName != "")
				$fieldName[$i] = $displayName;
		}
		
		$lexport = new libexporttext();
		$exportContent = $lexport->GET_EXPORT_TXT($sampleData, $fieldName, "", "\r\n", "", 0, "11");
		
		intranet_closedb();
		$lexport->EXPORT_FILE($filename, $exportContent);
	} else {
?>
You have no priviledge to access this page.
<?
	}
} else {
?>
You have no priviledge to access this page.
<?
}
?>
