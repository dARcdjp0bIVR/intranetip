<?php
$PATH_WRT_ROOT = "../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libreportcard.php");

$fs = new libfilesystem();
intranet_opendb();
$lreportcard = new libreportcard();

$TeachingClassName = $lreportcard->GET_TEACHING_CLASS();

//if (!(($ck_ReportCard_UserType!="ADMIN")&&(!preg_match("/".str_replace('/', '', $TeachingClassName)."/", $file)))) {
$FileClassName = str_replace('.', '', str_replace('/', '', $TeachingClassName));
$URL = "index.php";


if (
	(($ck_ReportCard_UserType=="TEACHER")&&(preg_match("/".$FileClassName."/", $userfile_name)))
	|| ($ck_ReportCard_UserType=="ADMIN"))
	{
	if(!empty($userfile))
	{
		switch ($UploadType) {
				case 0:
					$folder_prefix = $intranet_root."/file/reportcard/summary";
					break;
				case 1:
					$folder_prefix = $intranet_root."/file/reportcard/award";
					break;
				case 2:
					$folder_prefix = $intranet_root."/file/reportcard/merit";
					break;
				case 3:
					$folder_prefix = $intranet_root."/file/reportcard/eca";
					break;
				case 4:
					$folder_prefix = $intranet_root."/file/reportcard/remark";
					break;
				case 5:
					$folder_prefix = $intranet_root."/file/reportcard/interschool";
					break;
				case 6:
					$folder_prefix = $intranet_root."/file/reportcard/schoolservice";
					break;
		}
		$fs->folder_new($folder_prefix);	
	
		$loc = $userfile;
		$filename = $userfile_name;
		$success = 0;
		
		include_once("../default_header_revamp.php");
		
		if($filename!="")
		{
			$ext = $fs->file_ext($filename);
			if($ext==".csv")
			{
				if ($loc!="none" && file_exists($loc))
				{
      		$data = $fs->file_read_csv($loc);
      		
      		/*
          $x = $UploadType;
          if($ReportCardTemplate == "")
            $y = 1;
          else
            $y = $ReportCardTemplate; 
          */
          
          if(!empty($data)) {
            $header_row = array_shift($data);
            
            // Use display wording (or user-customized) instead of header wording
            $displayFieldName = $DefaultHeaderArray[$ReportTypeArray[$UploadType]];
						for($i=0; $i<sizeof($displayFieldName); $i++) {
							$displayName = trim($eReportCard[$HeaderSettingMap[$ReportTypeArray[$UploadType]][$displayFieldName[$i]]]);
							if ($displayName != "")
								$displayFieldName[$i] = $displayName;
						}
						
            $wrong_format = $fs->CHECK_CSV_FILE_FORMAT($header_row, $displayFieldName);
            //$wrong_format = $fs->CHECK_CSV_FILE_FORMAT($header_row, $DefaultHeaderArray[$ReportTypeArray[$UploadType]]);
            //$wrong_format = $fs->CHECK_CSV_FILE_FORMAT($header_row, $DefaultHeaderArray[$x][$y]);
            
            // Validate the data according to the setting of config file
            // Empty fields will be accepted
            $wrong_row = array();
            for($i=0; $i<sizeof($data); $i++) {
            	for($j=0; $j<sizeof($data[$i]); $j++) {
            		if ($data[$i][$j] != "") {
            			$DataLength = $DefaultHeaderLength[$ReportTypeArray[$UploadType]][$j];
            			if ($DataLength != "" && strlen($data[$i][$j]) != $DataLength) {
	            			$wrong_row[] = $i+1;
	            			continue;
	            		}
	            		$DataType = $DefaultHeaderType[$ReportTypeArray[$UploadType]][$j];
	            		if (strtoupper($DataType) == "NUM" && !is_numeric($data[$i][$j])) {
	            			$wrong_row[] = $i+1;
	            			continue;
	            		}
	            		$gradeRegEx = '/^[a-zA-Z][+|-]?$/';
	            		if (strtoupper($DataType) == "GRADE" && !preg_match($gradeRegEx, $data[$i][$j])) {
	            			$wrong_row[] = $i+1;
	            			continue;
	            		}
	            	}
            	}
            }
          }
          
          if($wrong_format || sizeof($wrong_row)>0) {
            $success = 0;
            $URL = "file_upload_revamp.php";
          } else if(strpos($filename, ".")!=0) {
					  $Class = str_replace(".", "", str_replace("/", "", $Class));
					  $filename = $Year."_".$Semester."_".$Class.substr($filename, strpos($filename, "."));
						$success = $fs->file_copy($loc, stripslashes($folder_prefix."/".$filename));
					}
				}
			}
		}
	}
	
	if ($success==1) {
		$Result = "upload";
	} else {
		if ($wrong_format) {
			$Result = "wrong_csv_header";
		} else {
			$Result = "upload_failed";
		}
	}

} else {
	$Result = "upload_failed";
}

if (sizeof($wrong_row) > 0) {
	$wrong_row = implode("+", $wrong_row);
	$URL .= "?WrongRow=$wrong_row&Result=$Result&UploadType=$UploadType";
} else {
	$URL .= "?Result=$Result&UploadType=$UploadType";
}

header("Location: $URL");
?>
