<?php
$PATH_WRT_ROOT = "../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
intranet_opendb();

$li = new libdb();

# Remove the old record
$sql = "DELETE FROM RC_MARKSHEET_VERIFICATION";
$li->db_db_query($sql);

# Insert the new record
$sql = "INSERT INTO RC_MARKSHEET_VERIFICATION (StartDate, EndDate, DateInput, DateModified) VALUES ('$StartDate', '$EndDate', now(), now())";
$success = $li->db_db_query($sql);

intranet_closedb();
$Result = ($success==1) ? "update" : "update_fail";
header("Location: verification_settings.php?Result=$Result&DuplicatedList=$DuplicatedList");
?>