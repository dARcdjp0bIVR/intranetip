<?php
$PATH_WRT_ROOT = "../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
intranet_opendb();

$li = new libdb();

# Remove the old record
$sql = "DELETE FROM RC_MARKSHEET_COLLECTION";
$li->db_db_query($sql);

# Insert the new record
$sql = "INSERT INTO RC_MARKSHEET_COLLECTION (StartDate, EndDate, Year, Semester, AllowClassTeacherComment, AllowSubjectTeacherComment, AllowClassTeacherUploadCSV, DateInput, DateModified) VALUES ('$StartDate', '$EndDate', '$AcademicYearID', '$YearTermID', '$AllowClassTeacherComment', '$AllowSubjectTeacherComment', '$AllowClassTeacherUploadCSV', now(), now())";
$success = $li->db_db_query($sql);

intranet_closedb();
$Result = ($success==1) ? "update" : "update_fail";
header("Location: settings.php?Result=$Result&DuplicatedList=$DuplicatedList");
?>