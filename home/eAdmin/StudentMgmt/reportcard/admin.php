<?php
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
intranet_opendb();

$IntranetReportCardType = ($_SESSION['intranet_reportcard_admin']==1 && $top_menu_mode==1) ? "ADMIN" : $_SESSION['intranet_reportcard_usertype'];

header("Location: index.php?IntranetReportCardType=$IntranetReportCardType");
?>