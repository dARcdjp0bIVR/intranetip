<?php
$PATH_WRT_ROOT = "../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

if ($plugin['ReportCard'])
{
	include_once($PATH_WRT_ROOT."includes/libreportcard.php");
	$lreportcard = new libreportcard();
	$CurrentPage = "TransitData";
	$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
    if ($lreportcard->hasAccessRight())
    {
		$linterface = new interface_html();
		
###############################################################################

###############################################################################

        # tag information
		$TAGS_OBJ[] = array($eReportCard['TransitData'], "", 0);
		
		$linterface->LAYOUT_START();
		
		?>
		<script language="javascript">
		function checkform(obj){
			if(confirm("<?=$eReportCard['TransitDataMsg']?>")) {
					return true;
			}
			return false;
		}
		</script>

		<form name="form1" action="transit_update.php" method="post"onSubmit="return checkform(this);">
		<table width="95%" align="center">
		<tr>
			<td>&nbsp;</td>
			<td align="right"> <?=$linterface->GET_SYS_MSG($Result);?></td>
		</tr>
		</table>
		<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
			<tr>
				<td>
					<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
						<tr>
							<td class="tabletext" height='35'><?= $eReportCard['TransitDataRemind']; ?></td>
						</tr>
					</table>
				</td>
			</tr>
			<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
			<tr>
				<td align="center"><?= $linterface->GET_ACTION_BTN($eReportCard['Transit'], "submit", "", "btnSubmit")?>
				</td>
			</tr>
		</table>
		
		</form>
	<?
        $linterface->LAYOUT_STOP();
		intranet_closedb();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>
