<?php
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
intranet_opendb();

unset($ck_ReportCard_UserType);
unset($IntranetReportCardType);

if (!isset($ck_ReportCard_UserType) && !isset($IntranetReportCardType))
{
	if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eReportCard1.0"]==1)
	{
		$IntranetReportCardType = "ADMIN";
	}
	else
	{
		//include_once ("$intranet_root/includes/libuser.php");
		//$header_lu = new libuser($_SESSION['UserID']);
		//$IntranetReportCardType = ($header_lu->isTeacherStaff()) ? "TEACHER" : "STUDENT";
		$IntranetReportCardType = ($_SESSION['UserType']==USERTYPE_STAFF && $_SESSION['isTeaching']==1) ? "TEACHER" : "STUDENT";
	}
}

//debug_pr('$ck_ReportCard_UserType = '.$ck_ReportCard_UserType);
//debug_pr('$IntranetReportCardType = '.$IntranetReportCardType);

if (isset($IntranetReportCardType))
{
	# TYPES: ADMIN, TEACHER, STUDENT
	setcookie("ck_ReportCard_UserType", $IntranetReportCardType, 0, "", "", 0);
	$ck_ReportCard_UserType = $IntranetReportCardType;
}

if($ck_ReportCard_UserType=="TEACHER") {
	header("Location: marksheet_submission/index.php");
}
else if($ck_ReportCard_UserType=="STUDENT") {
	header("Location: verify_results/index.php");
}
else {
	header("Location: grading_schemes/index.php");
}
?>