<?php
$PATH_WRT_ROOT = "../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

if ($plugin['ReportCard'])
{

	include_once($PATH_WRT_ROOT."includes/libreportcard.php");
	$lreportcard = new libreportcard();
	$CurrentPage = "SchemeSettings_SubjectGradings";
	$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
	$MODULE_OBJ['title'] = $eReportCard['GradingSchemeDetails'];

	if ($lreportcard->hasAccessRight())
    {
         $linterface = new interface_html("popup.html");
################################################################
list($MainInfo, $GradeMarkArr) = $lreportcard->GET_SCHEME_INFO($SchemeID);
list($SchemeTitle, $Description, $SchemeType, $FullMark, $Pass, $Fail, $Weighting) = $MainInfo;
if($SchemeType=="PF")
{
	$FormTable = "<tr><td valign='top' nowrap='nowrap' class='formfieldtitle tabletext'>".$eReportCard['Pass']."</td><td class='tabletext' width='75%'>".$Pass."</td></tr>";
	$FormTable .= "<tr><td valign='top' nowrap='nowrap' class='formfieldtitle tabletext'>".$eReportCard['Fail']."</td><td class='tabletext' width='75%'>".$Fail."</td></tr>";
}
else
{
	
	$FormTable = "<tr><td valign='top' nowrap='nowrap' class='formfieldtitle tabletext'>".$eReportCard['FullMark']."</td><td class='tabletext' width='75%'>".$FullMark."</td></tr>";

	$FormTable .= "<tr><td valign='top' nowrap='nowrap' class='formfieldtitle tabletext'>".$eReportCard['Pass']."</td><td width='75%'  valign='top'>
			<table id='tblPass'>
			<tr>
				<td class='tabletext'>".$eReportCard['LowerLimit']."</td>
				<td class='tabletext'>".$eReportCard['Grade']."</td>
				<td class='tabletext'>".$eReportCard['GradePoint']."</td>
			</tr>
			".($lreportcard->GENERATE_GRADING_FORM_CELL(0, 'Pass', $GradeMarkArr['P']))."
			</table>
		</td></tr>";

	$FormTable .= "<tr><td valign='top' nowrap='nowrap' class='formfieldtitle tabletext'>".$eReportCard['Fail']."</td><td width='75%' valign='top'>
			<table id='tblFail'>
			<tr>
				<td class='tabletext'>".$eReportCard['LowerLimit']."</td>
				<td class='tabletext'>".$eReportCard['Grade']."</td>
				<td class='tabletext'>".$eReportCard['GradePoint']."</td>
			</tr>
			".($lreportcard->GENERATE_GRADING_FORM_CELL(0, 'Fail', $GradeMarkArr['F']))."
			</table>
		</td></tr>";

	$FormTable .= "<tr><td valign='top' nowrap='nowrap' class='formfieldtitle tabletext'>".$eReportCard['Distinction']."</td><td width='75%' valign='top'>
			<table>
			<tr>
				<td class='tabletext'><label for='DistLowerLimit'>".$eReportCard['LowerLimit']."</label></td>
				<td class='tabletext'><label for='DistGrade'>".$eReportCard['Grade']."</td>
				<td class='tabletext'><label for='DistGradePoint'>".$eReportCard['GradePoint']."</td>
			</tr>
			<tr>
				<td class='formtextbox' width='100'><span class='tabletext'>".$GradeMarkArr['D'][0]."</span></td>
				<td class='formtextbox' width='100'><span class='tabletext'>".$GradeMarkArr['D'][1]."</span></td>
				<td class='formtextbox' width='100'><span class='tabletext'>".$GradeMarkArr['D'][2]."</span></td>
			</tr>
			</table>
		</td></tr>";
}
################################################################

$linterface->LAYOUT_START();
?>
<table border="0" cellpadding="0" cellspacing="0" align="center" width="90%">
<tr>
<td align="center">
<table width="95%" border="0" cellpadding="5" cellspacing="1">
<tr>
	<td>
		<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tr><td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $i_general_title; ?></td><td class="tabletext" width="75%"><?=$SchemeTitle?></td></tr>
		<tr><td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $eReportCard['GradingType']; ?></td><td class="textboxtext" width="75%"><?=($SchemeType=="H"?$eReportCard['HonorBased']:$eReportCard['PassFailBased'])?></td></tr>
		<tr><td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $i_general_description; ?></td><td class="tabletext" width="75%"><?=nl2br($Description)?></td></tr>
		<?=$FormTable?>
		<tr><td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $eReportCard['WeightingGrandTotal']; ?></td><td class="tabletext" width="75%"><?=$Weighting?></td></tr>
		</table>
	</td>
</tr>
</td>
</tr>
</table>

<tr><td>

<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
<tr>
	<td align="center"><?= $linterface->GET_ACTION_BTN($button_close, "button", "javascript:self.close()")?>
	</td>
</tr>
</table>
<br/>
</td></tr>

</table>
<?

        $linterface->LAYOUT_STOP();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>