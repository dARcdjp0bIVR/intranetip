<?php
$PATH_WRT_ROOT = "../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

if ($plugin['ReportCard'])
{
	include_once($PATH_WRT_ROOT."includes/libreportcard.php");
	$lreportcard = new libreportcard();
	$CurrentPage = "SchemeSettings_SubjectGradings";
	$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();

	if ($lreportcard->hasAccessRight())
    {
         $linterface = new interface_html();
################################################################
		
		# Subject Selection
		$SubjectSelect = $lreportcard->GET_SUBJECT_SELECTION();
		
		# Scheme Selection
		$SchemeArr = $lreportcard->GET_ALL_SCHEMES();
		$SchemeSelect = getSelectByArray($SchemeArr, "name='Scheme' onChange='jCHANGE_SCHEME(this.value)'", $Scheme, 0, 1);
		
		$TempArr = explode("_", $SchemeArr[0][0]);
		if($TempArr[1]=="H")
		{
			$ScoreChecked = "CHECKED='CHECKED'";
			$PFDisabled = "DISABLED='DISABLED'";
		}
		else
		{
			$ScoreDisabled = "DISABLED='DISABLED'";
			$GradeDisabled = "DISABLED='DISABLED'";
			$PFChecked = "CHECKED='CHECKED'";
		}
		# Scale radio box 
		$ScaleSelect = "<table width='100%' border='0' cellpadding='0' cellspacing='0'><tr>";
		$ScaleSelect .= "<td><span class=\"tabletext\"><input type=\"radio\" name=\"Scale\" id='Scale_S' value=\"S\" $ScoreChecked $ScoreDisabled /><label for='Scale_S'>".$eReportCard['ScoreScale']."</label></span></td>";
		$ScaleSelect .= "<td><span class=\"tabletext\"><input type=\"radio\" name=\"Scale\" id='Scale_G' value=\"G\" $GradeDisabled /><label for='Scale_G'>".$eReportCard['GradeScale']."</label></span></td>";
		$ScaleSelect .= "<td><span class=\"tabletext\"><input type=\"radio\" name=\"Scale\" id='Scale_PF' value=\"PF\" $PFChecked $PFDisabled /><label for='Scale_PF'>".$eReportCard['PassFail']."</label></span></td>";
		$ScaleSelect .= "</tr></table>";
		
		# Biuld Form Selection Table
		$FormArray = $lreportcard->GET_FORMS();
		$FormTable = "<table width='100%' border='0' cellpadding='5' cellspacing='0' align='center' id='FormTable'>";
		for($i=0; $i<sizeof($FormArray); $i++)
		{
			list($FormID, $FormName) = $FormArray[$i];
			$FormTable .= ($i%4==0) ? "<tr>" : "";
			$FormTable .= "<td class='tabletext'><input type='checkbox' name='Form[]' value='".$FormID."' id='form_".$FormID."' /><label for='form_".$FormID."'>".$FormName."</label></td>";
			if($i%4==3)
			{
				$FormTable .= "</tr>";
			}
			else if(($i+1)==sizeof($FormArray))
			{
				$FormTable .= "<td colspan='".(3-($i%4))."'>&nbsp;</td></tr>";
			}
		}
		$FormTable .= "</table>";
		
################################################################
		# tag information
		$TAGS_OBJ[] = array($eReportCard['DefaultSubjectGradings'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/reportcard/subject_gradings/?GradingType=0", 0);
		$TAGS_OBJ[] = array($eReportCard['SpecialSubjectGradings'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/reportcard/subject_gradings/?GradingType=1", 1);

		# page navigation (leave the array empty if no need)
		$PAGE_NAVIGATION[] = array($eReportCard['AddNewRecord'], "");

		$linterface->LAYOUT_START();
?>
<script language="javascript">
<!--
function checkform(obj){
   if(countChecked(obj, "Form[]")==0)
	{
		alert("<?=$i_alert_pleaseselect.$eReportCard['Form']?>");
		return false;
	}
    return true;
}

function jCHANGE_SCHEME(jScheme){

	var jSchemeArr = jScheme.split("_");
	var jSchemeType = jSchemeArr[1];

	var jScalseScore = document.getElementById("Scale_S");
	var jScalseGrade = document.getElementById("Scale_G");
	var jScalsePF = document.getElementById("Scale_PF");
	
	if(jSchemeType=="PF")
	{
		jScalsePF.disabled = false;
		jScalsePF.checked = true;
		
		jScalseScore.checked = false;
		jScalseScore.disabled = true;

		jScalseGrade.checked = false;
		jScalseGrade.disabled = true;
	}
	else
	{
		jScalsePF.checked = false;
		jScalsePF.disabled = true;

		jScalseScore.checked = true;
		jScalseScore.disabled = false;

		jScalseGrade.checked = false;
		jScalseGrade.disabled = false;
	}
}

//-->
</script>

<form name="form1" action="special_update.php" method="post" onSubmit="return checkform(this);">
<br />
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
</tr>
<tr>
	<td align="center">
	<table width="90%" border="0" cellpadding="5" cellspacing="0">
	<tr>
		<td>
			<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
			<tr><td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_Homework_subject?> <span class="tabletextrequire">*</span></td><td class="tabletext" width="75%"><?=$SubjectSelect?></td></tr>
			<tr><td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$eReportCard['GradingScheme']?> <span class="tabletextrequire">*</span></td><td class="tabletext" width="75%"><?=$SchemeSelect?></td></tr>
			<tr><td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$eReportCard['Scale']?> <span class="tabletextrequire">*</span></td><td class="tabletext" width="75%"><?=$ScaleSelect?></td></tr>
			<tr><td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$eReportCard['Form']?> <span class="tabletextrequire">*</span></td><td class="tabletext" width="75%"><?=$FormTable?></td></tr>
			</table>
		</td>
	</tr>
	</table>

</td>
</tr>
<tr>
	<td><table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr><td align="left" class="tabletextremark">&nbsp; &nbsp; &nbsp; <?=$i_general_required_field?></td></tr>
		<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
		<tr>
			<td align="center"><?= $linterface->GET_ACTION_BTN($button_submit, "submit", "", "btnSubmit")?>&nbsp;
			<?= $linterface->GET_ACTION_BTN($button_reset, "button", "self.location.reload()")?>
			<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:history.back()")?>
			</td>
		</tr>
	</table>
	</td>
</tr>
</table>

<p></p>
</form>
<?= $linterface->FOCUS_ON_LOAD("form1.SubjectID") ?>
<?

        $linterface->LAYOUT_STOP();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>