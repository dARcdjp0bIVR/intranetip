<?php
$PATH_WRT_ROOT = "../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libreportcard.php");
intranet_auth();
intranet_opendb();

$li = new libdb();
$lreportcard = new libreportcard();

if($SubjectGradingID!="")
{
	if(!empty($Form))
	{
		# Step 1: Remove old record in RC_SUBJECT_GRADING_SCHEME_FORM
		$sql = "DELETE FROM RC_SUBJECT_GRADING_SCHEME_FORM WHERE SubjectGradingID = '$SubjectGradingID'";
		$li->db_db_query($sql);
		
		$lreportcard->INSERT_SUBJECT_GRADING_FORM($SubjectGradingID, $Form);
	}
}
intranet_closedb();
?>
<script language='javascript'>
opener.location.reload();
self.close();
</script>
