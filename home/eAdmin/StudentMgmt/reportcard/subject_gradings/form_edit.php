<?php
$PATH_WRT_ROOT = "../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

if ($plugin['ReportCard'])
{
	include_once($PATH_WRT_ROOT."includes/libreportcard.php");
	$lreportcard = new libreportcard();
	$CurrentPage = "SchemeSettings_SubjectGradings";
	$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
	$MODULE_OBJ['title'] = $eReportCard['SelectForms'];

	if ($lreportcard->hasAccessRight())
    {
         $linterface = new interface_html("popup.html");

################################################################
		
		# Get subject name
		$SubjectName = $lreportcard->GET_SUBJECT_NAME($SubjectID);

		# Get subject grading form(s)
		$FormInfo = $lreportcard->GET_SUBJECT_GRADING_FORM(0, $SubjectGradingID);

		# Biuld Form Selection Table
		$FormArray = $lreportcard->GET_FORMS();
		$FormTable = "<table width='100%' border='0' cellpadding='5' cellspacing='0' align='center' id='FormTable'>";
		for($i=0; $i<sizeof($FormArray); $i++)
		{
			list($FormID, $FormName) = $FormArray[$i];

			$Checked = (in_array($FormID, $FormInfo)) ? "CHECKED='CHECKED'" : "";
			$FormTable .= ($i%4==0) ? "<tr>" : "";
			$FormTable .= "<td class='tabletext'><input type='checkbox' name='Form[]' value='".$FormID."' id='form_".$FormID."' $Checked /><label for='form_".$FormID."'>".$FormName."</label></td>";
			if($i%4==3)
			{
				$FormTable .= "</tr>";
			}
			else if(($i+1)==sizeof($FormArray))
			{
				$FormTable .= "<td colspan='".(3-($i%4))."'>&nbsp;</td></tr>";
			}
		}
		$FormTable .= "</table>";
################################################################

		$linterface->LAYOUT_START();
?>
<script language="javascript">
<!--
function checkform(obj){
   if(countChecked(obj, "Form[]")==0)
	{
		alert("<?=$i_alert_pleaseselect.$eReportCard['Form']?>");
		return false;
	}
    return true;
}
//-->
</script>
<form name="form1" action="form_update.php" method="post" onSubmit="return checkform(this);">
<br />
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
</tr>
<tr>
	<td>

	<table width="90%" border="0" cellpadding="5" cellspacing="1">
	<tr>
		<td>
			<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
			<tr><td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_Homework_subject?></td><td class="tabletext" width="75%"><?=$SubjectName?></td></tr>
			<tr><td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$eReportCard['Form']?> <span class="tabletextrequire">*</span></td><td class="tabletext" width="75%"><?=$FormTable?></td></tr>
			</table>
		</td>
	</tr>
	</table>

</td>
</tr>
<tr>
	<td><table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr><td align="left" class="tabletextremark">&nbsp; &nbsp; &nbsp; <?=$i_general_required_field?></td></tr>
		<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
		<tr>
			<td align="center"><?= $linterface->GET_ACTION_BTN($button_submit, "submit", "", "btnSubmit")?>&nbsp;
			<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "")?>
			<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:self.close()")?>
			</td>
		</tr>
	</table>
	</td>
</tr>
</table>
<input type="hidden" name="SubjectGradingID" value="<?=$SubjectGradingID?>" />
<p></p>
</form>
<?

        $linterface->LAYOUT_STOP();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>