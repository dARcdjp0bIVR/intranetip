<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclassdiary.php");
include_once($PATH_WRT_ROOT."includes/libclassdiary_ui.php");

intranet_auth();
intranet_opendb();

$libclassdiary_ui = new libclassdiary_ui();
$libclassdiary = $libclassdiary_ui->Get_libclassdiary();
if(!$plugin['ClassDiary'] || !$libclassdiary->IS_ADMIN_USER())
{
	intranet_closedb();
	exit;
}

$task = $_REQUEST['task'];
switch($task)
{
	case "Get_Report_Statistics_Table":
		echo $libclassdiary_ui->Get_Report_Statistics_Table($_REQUEST['StartDate'],$_REQUEST['EndDate'],$_REQUEST['ClassID'], $_REQUEST['ClassName'],$_REQUEST['RecordType'], $_REQUEST['ShowDates']==1);
	break;
}

intranet_closedb();
?>