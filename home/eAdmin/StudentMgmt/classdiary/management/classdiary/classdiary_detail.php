<?php
// editing by 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclassdiary.php");
include_once($PATH_WRT_ROOT."includes/libclassdiary_ui.php");

intranet_auth();
intranet_opendb();

$libclassdiary_ui = new libclassdiary_ui();
$libclassdiary = $libclassdiary_ui->Get_libclassdiary();
if(!$plugin['ClassDiary'] || !$libclassdiary->IS_ADMIN_USER())
{
	header("Location:/");
	intranet_closedb();
	exit;
}

$CurrentPageArr['ClassDiary'] = 1;
$CurrentPage['ClassDiary'] = 1;
$TAGS_OBJ[] = array($Lang['ClassDiary']['ClassDiary'],"",0);

$MODULE_OBJ = $libclassdiary_ui->GET_MODULE_OBJ_ARR();
$libclassdiary_ui->LAYOUT_START(urldecode($_REQUEST['Msg']));

echo $libclassdiary_ui->Get_Class_Diary_View_Form($_REQUEST['DiaryID']);

$libclassdiary_ui->LAYOUT_STOP();
intranet_closedb();
?>
<script type="text/javaScript" language="JavaScript">
function DeleteClassDiary(DiaryID)
{
	if(confirm(jsLang['ConfirmDeleteClassDiary'])){
		window.location.href = "delete_classdiary.php?DiaryID="+DiaryID;
	}
}

</script>