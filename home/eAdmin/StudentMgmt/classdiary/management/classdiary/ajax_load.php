<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclassdiary.php");
include_once($PATH_WRT_ROOT."includes/libclassdiary_ui.php");

intranet_auth();
intranet_opendb();

$libclassdiary_ui = new libclassdiary_ui();
$libclassdiary = $libclassdiary_ui->Get_libclassdiary();
if(!$plugin['ClassDiary'] || !$libclassdiary->IS_ADMIN_USER())
{
	intranet_closedb();
	exit;
}

$task = $_REQUEST['task'];
switch($task)
{
	case "Get_Class_Diary_Table":
		$FromDate = $_REQUEST['FromDate'];
		$ToDate = $_REQUEST['ToDate'];
		
		echo $libclassdiary_ui->Get_Class_Diary_Table($FromDate,$ToDate);
	break;
	
	case "Get_Student_Selection_Table":
		echo $libclassdiary_ui->Get_Student_Selection_Table($ClassID,$FieldID,array(),explode(",",$StudentID));
	break;
	
	case "Get_Student_Names":
		$libclassdiary = $libclassdiary_ui->Get_libclassdiary();
		$ClassID = $_REQUEST['ClassID'];
		$StudentID = $_REQUEST['StudentID'];
		if(count($StudentID)==0){
			$StudentID = array(-1);
		}
		$student_array = $libclassdiary->Get_Students_By_ClassID($ClassID,$StudentID);
		echo $libclassdiary_ui->Get_Student_Names_Block($student_array);
	break;
	
	case "Get_Class_Subject_Selection":
		$ClassID = $_REQUEST['ClassID'];
		$ID_Name = $_REQUEST['ID_Name'];
		
		echo $libclassdiary_ui->Get_Class_Subject_Selection($ClassID,$ID_Name,"","",0,$Lang['General']['PleaseSelect']);
	break;
}

intranet_closedb();
?>