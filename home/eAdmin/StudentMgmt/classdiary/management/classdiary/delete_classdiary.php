<?php
// editing by 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclassdiary.php");
include_once($PATH_WRT_ROOT."includes/libclassdiary_ui.php");

intranet_auth();
intranet_opendb();

$libclassdiary_ui = new libclassdiary_ui();
$libclassdiary = $libclassdiary_ui->Get_libclassdiary();
if(!$plugin['ClassDiary'] || !$libclassdiary->IS_ADMIN_USER())
{
	header("Location:/");
	intranet_closedb();
	exit;
}

$success = $libclassdiary->Delete_Class_Diary($_REQUEST['DiaryID']);
if($success){
	$Msg = $Lang['General']['ReturnMessage']['DeleteSuccess'];
}else{
	$Msg = $Lang['General']['ReturnMessage']['DeleteUnsuccess'];
}

intranet_closedb();
header("Location: index.php?Msg=".rawurlencode($Msg));
?>