<?php
# using: 

########## Change Log [Start] #############
#
#   Date:   2019-05-14 Cameron
#           - fix error: should cast 2nd parameter to array when calling in_array
#
#	Date:	2014-08-21	Bill
#			Add HomeTelNo in default mode 
#			
#	Date:	2014-07-17	Bill
#			Add flag $plugin['eEnrollment'] to print data with column CardID - eEnrolment
#
########## Change Log [End] #############


$PATH_WRT_ROOT = "../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/libuserpersonalsettings.php");

intranet_opendb();

$lexport = new libexporttext();
$laccount = new libaccountmgmt();
$libps = new libuserpersonalsettings();

//if (!isset($default) || $default != 1)
{
   
     # check if there parent / guardian column is selected
     # set prefix if yes
     
     if (!isset($default) || $default != 1) {
		$FieldArray = $Fields;
	} else {
		$FieldArray = array("UserLogin", "UserPassword", "UserEmail", "EnglishName", "ChineseName", "Title", "Gender", "HomeTelNo", "MobileTelNo");
		if((isset($plugin['attendancestudent']) && $plugin['attendancestudent']) ||(isset($plugin['payment'])&& $plugin['payment'])|| (isset($plugin['eEnrollment'])&& $plugin['eEnrollment']))
		{
			$FieldArray = array_merge($FieldArray, array("CardID"));
		}
		
		$FieldArray = array_merge($FieldArray,array("DateOfBirth","WebSAMSRegNo"));
		
         if($special_feature['ava_hkid'])
         {
			
			$FieldArray = array_merge($FieldArray, array("HKID"));
         }
         
         if($special_feature['ava_strn'])
         {
			
			$FieldArray = array_merge($FieldArray, array("STRN"));
         }
         
         $ExtraInfoCatInfo = $laccount->Get_User_Extra_Info_Category();
		 $totalCategory = count($ExtraInfoCatInfo);
		
		for($i=0; $i<$totalCategory; $i++) {
			$catname = Get_Lang_Selection($ExtraInfoCatInfo[$i]['CategoryNameCh'], $ExtraInfoCatInfo[$i]['CategoryNameEn']);
			$FieldArray[] = $ExtraInfoCatInfo[$i]['CategoryCode'];	
		}
		
		$BlissFieldAry = $Lang['AccountMgmt']['BlissWisdomFieldAry'];
		for($a=0; $a<count($BlissFieldAry); $a++) {
			$FieldArray[] = $BlissFieldAry[$a];
		}	
	}
	
    $conds = "RecordType = ".USERTYPE_STUDENT." AND ";
    

	if($targetClass!="0") {
		include_once($PATH_WRT_ROOT."includes/libclass.php");
		$lclass = new libclass();
		$studentAry = $lclass->storeStudent(0,$targetClass);
		
		if(sizeof($studentAry)>0) {
			$conds .= " UserID IN (".implode(',', $studentAry).") AND ";
		}
	}		

     

	 # student
	 
	 	if($userList && $notInList) {	// all students
			// no addition condition	
		} else {
			
			$sql = "SELECT UserID FROM YEAR_CLASS_USER ycu LEFT OUTER JOIN YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID) WHERE yc.AcademicYearID=".Get_Current_Academic_Year_ID();
		 		$temp = $laccount->returnVector($sql);
		 		
			if($userList) {		// only export user list (with classes)
		 		if(sizeof($temp)>0) $conds .= " UserID IN (".implode(',',$temp).") AND ";
			} 
			else if($notInList) {	// only export user list (without classes)
				if(sizeof($temp)>0) $conds .= " UserID NOT IN (".implode(',',$temp).") AND ";
			} else {
				// export nothing	
			}
		}
	 
	 
	 if($recordstatus!="")
	 	$conds .= " RecordStatus='$recordstatus' AND ";
	 

	
	 
     $finalConds = "WHERE $conds (UserLogin like '%$keyword%' OR UserEmail like '%$keyword%' OR ChineseName like '%$keyword%' OR EnglishName like '%$keyword%' OR ClassName like '%$keyword%' OR WebSamsRegNo like '%$keyword%')";

     $url = "/file/export/eclass-user-".session_id()."-".time().".csv";
	 
	$ExtraInfoCatInfo = $laccount->Get_User_Extra_Info_Category();
	//debug_pr($ExtraInfoCatInfo);
	$totalCategory = count($ExtraInfoCatInfo);
	for($a=0; $a<$totalCategory; $a++) {
		$catCode[] = $ExtraInfoCatInfo[$a]['CategoryCode'];	
		$thisCatName = Get_Lang_Selection($ExtraInfoCatInfo[$a]['CategoryNameCh'], $ExtraInfoCatInfo[$a]['CategoryNameEn']);
		//echo $thisCatName.'/';
		$catInfo[$ExtraInfoCatInfo[$a]['CategoryCode']] = $ExtraInfoCatInfo[$a]['CategoryID'];
	}
	
	$BlissFieldAry = $Lang['AccountMgmt']['BlissWisdomFieldAry'];
	
	
	 $delim = "";
	 foreach($FieldArray as $_field) {
	 	if(!in_array($_field, (array)$catCode) && !in_array($_field, $BlissFieldAry)) {
				$fieldStr .= $delim.$_field;
				$delim = ", ";
	 	}
	 }
	 $sql = "SELECT $fieldStr, UserID FROM INTRANET_USER $finalConds ORDER BY ClassName, ClassNumber";
	 $export_content = $laccount->returnArray($sql);
	 
?>
	<table border="1" cellspacing="0" cellpadding="0">
	
	<?
	
	# table header
	echo '<tr>';
	for($i=0; $i<count($FieldArray); $i++) {
		echo '<td>'.$FieldArray[$i].'</td>'; 	
	}
	echo '</tr>';
	
	$BlissFieldAry = $Lang['AccountMgmt']['BlissWisdomFieldAry'];
	//debug_pr($BlissFieldAry);
	# table content
	for($i=0; $i<count($export_content); $i++) {
		$colNo = (count($export_content[$i])/2)-1;
		echo '<tr>
					';
		for($j=0; $j<$colNo; $j++) {
			echo '<td>'.$export_content[$i][$j].'</td>';	
		}
		
		 foreach($FieldArray as $_field) {
		 	
			if(in_array($_field, (array)$catCode)) {
				$itemAry = $laccount->Get_User_Extra_Info_Item('', $catInfo[$_field]);
				
				$getItemName = Get_Lang_Selection("i.ItemNameCh", "i.ItemNameEn");
				$sql = "SELECT CONCAT($getItemName, IF((m.OtherInfo IS NOT NULL AND m.OtherInfo!=''),CONCAT('::',m.OtherInfo),'')) as ItemName FROM INTRANET_USER_EXTRA_INFO_MAPPING m INNER JOIN INTRANET_USER_EXTRA_INFO_ITEM i ON (i.ItemID=m.ItemID) WHERE m.UserID='".$export_content[$i]['UserID']."' AND i.CategoryID='".$catInfo[$_field]."'";
				$itemAry = $laccount->returnVector($sql);
				echo '<td>'.((count($itemAry)>0) ? implode(',',$itemAry) : "&nbsp;").'</td>';
				
			}
			if(in_array($_field, $BlissFieldAry)) {
				$ResultAry = $libps->Get_Setting($export_content[$i]['UserID'], $BlissFieldAry);
				
				if($_field=="HowToKnowBlissWisdom") {
					// To display HowToKnowBlissWisdom
					$HowToAry = explode('::', $ResultAry['HowToKnowBlissWisdom']);
					$HowToID = $HowToAry[0];
					$HowToOthers = $HowToAry[1] ? '::'.$HowToAry[1] : "";
					
					echo '<td>'.(isset($Lang['AccountMgmt']['KnowAboutBlissWisdomOptions'][$HowToID]) ? $Lang['AccountMgmt']['KnowAboutBlissWisdomOptions'][$HowToID].$HowToOthers : "&nbsp;").'</td>';	
				} else {
					echo '<td>'.(($ResultAry[$_field]!="0000-00-00" && $ResultAry[$_field]!="") ? $ResultAry[$_field] : "&nbsp;").'</td>';
				}	
			} 
		 }
		 
		echo ' 			
			</tr>';	
	}
	 ?>

	</table>
<?	

}

intranet_closedb();
?>
