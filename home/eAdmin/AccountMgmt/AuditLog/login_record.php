<?php 
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libcurdlog.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
intranet_opendb();
$linterface = new interface_html();
$log = new libcurdlog();

$CurrentPageArr['AuditLog'] = 1;
$CurrentPage = "Mgmt_Account";

// Search Parameters
$field = $_POST["field"];
$order = isset($_POST["order"])?$_POST["order"]:1;
$pageNo = $_POST["pageNo"];
$page_size_change = $_POST["page_size_change"];	//	To refresh the page when change the $numPerPage, (without it cannot change the page size)
$numPerPage = !empty($_POST["numPerPage"])?$_POST["numPerPage"]:$page_size;	//	The parameter $page_size seems to be set in global.php, pls use $numPerPage to avoid modified the $page_size

// other parameters
$keyword = trim($_POST["keyword"]);	//	This is the string for searching
$classId = isset($_POST["YearClassID"])?$_POST["YearClassID"]:'all';
$status = isset($_POST["status"])?$_POST["status"]:'all';
if($module == "") $module = "login";
$now = time();
$today = date('Y-m-d',$now);
$ts_weekstart = mktime(0,0,0,date('m',$now),date('d',$now)-date('w',$now),date('Y',$now));
$ts_weekend = mktime(0,0,-1,date('m',$ts_weekstart),date('d',$ts_weekstart)+7,date('Y',$ts_weekstart));
$ts_monthstart = mktime(0,0,0,date('m',$now),1,date('Y',$now));
$ts_monthend = mktime(0,0,-1,date('m',$now)+1,1,date('Y',$now));

$weekstart = date('Y-m-d',$ts_weekstart);
$weekend = date('Y-m-d',$ts_weekend);
$monthstart = date('Y-m-d',$ts_monthstart);
$monthend = date('Y-m-d',$ts_monthend);
$yearstart = date('Y-m-d',getStartOfAcademicYear($now));
$yearend = date('Y-m-d',getEndOfAcademicYear($now));

if ($start == "" || $end == "")
{
	$start = $today;
	$end = $today;
}

$datetype += 0;
$selected[$datetype] = "SELECTED";

$classSelect = "<SELECT name=datetype onChange=\"changeDateType(this.form)\">\n";
$classSelect .= "<OPTION value=0 ".$selected[0].">$i_Profile_Today</OPTION>\n";
$classSelect .= "<OPTION value=1 ".$selected[1].">$i_Profile_ThisWeek</OPTION>\n";
$classSelect .= "<OPTION value=2 ".$selected[2].">$i_Profile_ThisMonth</OPTION>\n";
$classSelect .= "<OPTION value=3 ".$selected[3].">$i_Profile_ThisAcademicYear</OPTION>\n";
$classSelect .= "</SELECT> \n";

unset($selected);
$selected[$identity] = "SELECTED";

$identitySelect = "<SELECT name=identity onChange=\"changeDateType(this.form)\">\n";
$identitySelect .= "<OPTION value=0 ".$selected[0].">$button_select_all</OPTION>\n";
$identitySelect .= "<OPTION value=1 ".$selected[1].">$i_identity_teachstaff</OPTION>\n";
$identitySelect .= "<OPTION value=2 ".$selected[2].">$i_identity_student</OPTION>\n";
$identitySelect .= "</SELECT> \n";

$functionbar = "<table border=0>\n";
$functionbar .= "<tr><td nowrap>$i_Profile_SelectSemester:</td><td>$classSelect</td></tr>";
$functionbar .= "<tr><td></td><td colspan=2>$i_Profile_From <input type=text name=start size=10 value='$start' onblur=\"this.form.submit()\">  ";
$functionbar .= "$i_Profile_To <input type=text name=end size=10 value='$end' onblur=\"this.form.submit()\"> <span class=extraInfo>(yyyy-mm-dd) </span>";
$functionbar .= "</td></tr>";
$functionbar .= "<tr>";
$functionbar .= "<td>$i_identity:</td>";
$functionbar .= "<td>".$identitySelect."</td>";
$functionbar .= "</tr>";

$functionbar .= "</table>\n";

$toolbar ="<div class='Conntent_tool'><a href='javascript:goExport()' class='export'>".$button_export."</a></div>";

$TAGS_OBJ[] = array($Lang['AuditLog']['UserMgmt'],"/home/eAdmin/AccountMgmt/AuditLog/?module=user",$module=="user");
$TAGS_OBJ[] = array($Lang['AuditLog']['GroupMgmt'],"/home/eAdmin/AccountMgmt/AuditLog/?module=group",$module=="group");
$TAGS_OBJ[] = array($Lang['AuditLog']['LoginRecord'],"/home/eAdmin/AccountMgmt/AuditLog/login_record.php",$module=="login");

$MODULE_OBJ['title'] = $Lang['Header']['Menu']['AuditLog'];

# online help button
$onlineHelpBtn = gen_online_help_btn_and_layer('user','student');
//$TAGS_OBJ_RIGHT[] = array($onlineHelpBtn);

$li = new libdbtable2007($field, $order, $pageNo);

$username_field = getNameFieldWithLoginClassNumberByLang("b.");
if($identity)
{
	$identity_sql = " and b.RecordType=$identity ";	
}
if($keyword)
{
	$keyword_sql = " and $username_field LIKE '%".$keyword."%' ";
}
$sql  = "SELECT
			$username_field, 
			a.StartTime, 
			a.DateModified
			,a.ClientHost
			,UNIX_TIMESTAMP(a.DateModified) - UNIX_TIMESTAMP(a.StartTime) as duration
		FROM 
			INTRANET_LOGIN_SESSION as a 
			LEFT OUTER JOIN INTRANET_USER as b ON (a.UserID = b.UserID)
		where 
			a.StartTime>='$start 00:00:00' and a.DateModified<='$end 23:59:59'
			 $identity_sql
			 $keyword_sql
";
$li->field_array = array("$username_field","a.StartTime","a.DateModified","a.ClientHost", "duration");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+1;
$li->IsColOff = 2;
		
// TABLE COLUMN
$li->column_list .= "<th class='num_check'>#</th>\n";
$li->column_list .= "<td width=20% class=tableTitle>".$li->column(0, "$i_UserName ($i_UserLogin)")."</td>\n";
$li->column_list .= "<td width=20% class=tableTitle>".$li->column(1, $i_UsageStartTime)."</td>\n";
$li->column_list .= "<td width=20% class=tableTitle>".$li->column(2, $i_UsageEndTime)."</td>\n";
$li->column_list .= "<td width=20% class=tableTitle>".$li->column(3, $i_UsageHost)."</td>\n";
$li->column_list .= "<td width=20% class=tableTitle>".$li->column(4, $i_UsageDuration)."</td>\n";
$li->column_array = array(0,0,0,0,9);
$li->wrap_array = array(0,0,0,0,0);
$li->page_size = $numPerPage;

$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$xmsg]);
?>
<script type="text/javascript">

function changeDateType(obj){
	switch (obj.datetype.value){
	  	case '0':
			  obj.start.value = '<?=$today?>';
			  obj.end.value = '<?=$today?>';
		break;
		case '1':
			  obj.start.value = '<?=$weekstart?>';
			  obj.end.value = '<?=$weekend?>';
		break;
		case '2':
			  obj.start.value = '<?=$monthstart?>';
			  obj.end.value = '<?=$monthend?>';
		break;
		case '3':
			  obj.start.value = '<?=$yearstart?>';
			  obj.end.value = '<?=$yearend?>';
		break;
	}
	obj.submit();
}

function goExport(){
	document.form1.action = "export.php?module=login";
	document.form1.submit();
	document.form1.action = "";
}

$(document).ready(function(){
	$('#keyword').keypress(function(e) {
	    if(e.which == 13) {
	    	document.form1.submit();
	    }
	});
});

</script>
<form name="form1" method="post">
	<?=$toolbar?>
	<div class="content_top_tool">
		<?=$linterface->Get_Search_Box_Div("keyword", $keyword)?>
		<?=$functionbar?>	
	<br style="clear:both" />
	</div>
	<div class="table_board">
		<?= $li->display()?>				
	<input type="hidden" name="StudentInfo" value="<?=$StudentInfo?>" />
	<input type="hidden" name="FieldChanged" />
	<input type="hidden" name="RecordType" value=<?=$RecordType?> />
	<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
	
	<input type="hidden" name="order" value="<?php echo $li->order; ?>" />
	<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
	<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />
	<input type="hidden" name="displayBy" value="<?=$displayBy?>" />
	<input type="hidden" name="page_size_change" />
	<input type="hidden" name="miscParameter" />
	<input type="hidden" name="ReportItemID" id="ReportItemID" value="">
</form>
</body>
<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>