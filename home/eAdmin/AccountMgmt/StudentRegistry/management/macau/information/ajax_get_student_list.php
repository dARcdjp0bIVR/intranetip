<?php
//editing by 
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccessright.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry_ui.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$laccessright = new libaccessright();

if(!$plugin['AccountMgmt_StudentRegistry'] || (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-View") && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-Manage"))) {
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$lsr = new libstudentregistry();
$lsrUI = new libstudentregistry_ui();

$currentYearID = Get_Current_Academic_Year_ID();

$searchText = trim(urldecode(stripslashes($_REQUEST['searchText'])));

$li = new libdbtable2007($field, $order, $pageNo);

# conditions
$conds = " AND yc.YearClassID='$targetClass'";

if($registryStatus!="")
	$conds .= " AND USR.RecordStatus='$registryStatus'";
else 
	$conds .= " AND USR.RecordStatus IN (0,1,2)";

if($searchText!="")
	$conds .= " AND (USR.EnglishName LIKE '%$searchText%' OR USR.ChineseName LIKE '%$searchText%' OR USR.WebSAMSRegNo LIKE '%$searchText%' OR srs.CODE LIKE '%$searchText%')";
	
$sql = "SELECT ".
			Get_Lang_Selection('yc.ClassTitleB5','yc.ClassTitleEN')." as clsName,
			ycu.ClassNumber as clsNo,
			USR.EnglishName as engname,
			USR.ChineseName as chiname,
			USR.WebSAMSRegNo as websams,
			IFNULL(srs.CODE,'---') as DSEJ,
			'<img src=$image_path/$LAYOUT_SKIN/icon_learning_portfolio.gif>' as ipf,
			IFNULL(srs.DateModified,'---') as lastUpdated,
			'".$Lang['StudentRegistry']['Approved']."(".$Lang['StudentRegistry']['ApprovedBySystem'].")' as status,
			CONCAT('<input type=\'checkbox\' name=\'userID[]\' id=\'userID[]\' value=\'',USR.UserID,'\'>') as checkbox,
			USR.UserID
		FROM
			INTRANET_USER USR LEFT OUTER JOIN
			YEAR_CLASS_USER ycu ON (ycu.UserID=USR.UserID) LEFT OUTER JOIN
			YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID) LEFT OUTER JOIN
			STUDENT_REGISTRY_STUDENT srs ON (srs.UserID=USR.UserID)
		WHERE
			USR.RecordType=2 AND
			yc.AcademicYearID='".$currentYearID."'
			$conds
		";

echo $sql;
$li->sql = $sql;
$li->field_array = array("clsName", "concat(TRIM(SUBSTRING_INDEX(clsNo, '-', 1)), IF(INSTR(clsNo, '-') = 0, NULL, LPAD(TRIM(SUBSTRING_INDEX(clsNo, '-', -1)), 10, '0')))", "engname", "chiname", "websams", "DSEJ", "ipf", "lastUpdated", "status");
$li->no_col = sizeof($li->field_array)+2;
$li->IsColOff = "IP25_table";


$pos = 0;
$li->column_list .= "<th width='1'>#</th>\n";
$li->column_list .= "<th width='10%' >".$li->column($pos++, $Lang['StudentRegistry']['Class'])."</th>\n";
$li->column_list .= "<th width='5%' >".$li->column($pos++, $Lang['StudentRegistry']['ClassNumber'])."</th>\n";
$li->column_list .= "<th width='18%' >".$li->column($pos++, $Lang['StudentRegistry']['EnglishName'])."</th>\n";
$li->column_list .= "<th width='18%' >".$li->column($pos++, $Lang['StudentRegistry']['ChineseName'])."</th>\n";
$li->column_list .= "<th width='12%' >".$li->column($pos++, $Lang['StudentRegistry']['WebSAMSRegNo'])."</th>\n";
$li->column_list .= "<th width='12%' >".$li->column($pos++, $Lang['StudentRegistry']['DSEJ_Number'])."</th>\n";
$li->column_list .= "<th width='5%' >".$ip20TopMenu['iPortfolio']."</th>\n"; $pos++;
$li->column_list .= "<th width='10%' >".$li->column($pos++, $Lang['StudentRegistry']['LastUpdated'])."</th>\n";
$li->column_list .= "<th width='10%' >".$li->column($pos++, $Lang['StudentRegistry']['ApproveStatus'])."</th>\n";
$li->column_list .= "<th width='1'>".$li->check("userID[]")."</td>\n";

//echo $li->display();

intranet_closedb();
?>