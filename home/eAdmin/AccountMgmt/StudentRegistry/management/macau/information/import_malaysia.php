<?php
// Modifying by: 

############# Change Log [Start] ################
#
#
############# Change Log [End] ################

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libaccessright.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry.php");

intranet_auth();
intranet_opendb();

$laccessright = new libaccessright();
$lsr = new libstudentregistry();

if(!$plugin['AccountMgmt_StudentRegistry'] || (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-View") && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-Manage"))) {
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

# display date format remark
//$mandatory_remark = array(3,4,5,6,8,9,10,11,12,13,14,15,16,17,18,20,21,24,25,27,28,29,30,31,32,34,35,38,39,40,42,43,46,47,50,52,53,54,55,56,57,60,61,62,63,64,65,66,67);
$mandatory_remark = array();
$date_remark = array(5,8);


for($i=0; $i<sizeof($Lang['StudentRegistry']['Import_Malaysia']); $i++) {
	$displayRemark = in_array($i, $mandatory_remark) ? $Lang['StudentRegistry']['MandatoryRemark'] : "";
	$displayRemark .= in_array($i, $date_remark) ? $Lang['StudentRegistry']['DateFormatRemark'] : "";
	$displayColumn .= $Lang['SysMgr']['Homework']['Column']." ".($i+1)." : ".$displayRemark.$Lang['StudentRegistry']['Import_Malaysia'][$i]."<br>";	
}
/*
$reference_str = "<div id=\"ref_list\" style='position:absolute; height:150px; z-index:1; visibility: hidden;'></div>";
$reference_str .= "<a class=\"tablelink\" href=javascript:show_ref_list('sex','sc_click')>".$Lang['StudentRegistry']['Import_Macau'][9]."</a>,<span id=\"sc_click\">&nbsp;</span>
				<a class=\"tablelink\" href=javascript:show_ref_list('birthplace','bc_click')>".$Lang['StudentRegistry']['Import_Macau'][11]."</a>,<span id=\"bc_click\">&nbsp;</span>
				<a class=\"tablelink\" href=javascript:show_ref_list('id_type','ic_click')>".$Lang['StudentRegistry']['Import_Macau'][12]."</a>,<span id=\"ic_click\">&nbsp;</span>
				<a class=\"tablelink\" href=javascript:show_ref_list('id_place','tc_click')>".$Lang['StudentRegistry']['Import_Macau'][14]."</a>,<span id=\"tc_click\">&nbsp;</span>
				<a class=\"tablelink\" href=javascript:show_ref_list('s6_type','s6_click')>".$Lang['StudentRegistry']['Import_Macau'][17]."</a>,<span id=\"s6_click\">&nbsp;</span>
				<a class=\"tablelink\" href=javascript:show_ref_list('nation','nc_click')>".$Lang['StudentRegistry']['Import_Macau'][20]."</a>,<span id=\"nc_click\">&nbsp;</span>
				<a class=\"tablelink\" href=javascript:show_ref_list('religion','rc_click')>".$Lang['StudentRegistry']['Import_Macau'][22]."</a>,<span id=\"rc_click\">&nbsp;</span>
				<a class=\"tablelink\" href=javascript:show_ref_list('area','a_click')>".$Lang['StudentRegistry']['Import_Macau'][25]."</a>,<span id=\"a_click\">&nbsp;</span>
				<a class=\"tablelink\" href=javascript:show_ref_list('area2','a2_click')>".$Lang['StudentRegistry']['Import_Macau'][27]." / ".$Lang['StudentRegistry']['Import_Macau'][53]." / ".$Lang['StudentRegistry']['Import_Macau'][65]."</a>,<span id=\"a2_click\">&nbsp;</span>
				<a class=\"tablelink\" href=javascript:show_ref_list('marital','mc_click')>".$Lang['StudentRegistry']['Import_Macau'][33]." / ".$Lang['StudentRegistry']['Import_Macau'][41]."</a>,<span id=\"mc_click\">&nbsp;</span>
				<a class=\"tablelink\" href=javascript:show_ref_list('guardian','g_click')>".$Lang['StudentRegistry']['Import_Macau'][50]." / ".$Lang['StudentRegistry']['Import_Macau'][62]."</a><span id=\"g_click\">&nbsp;</span>
				";
*/

$linterface = new interface_html();

### Title / Menu
$CurrentPageArr['StudentRegistry'] = 1;
$CurrentPage = "Mgmt_RegistryInfo";

$TAGS_OBJ[] = array($Lang['Menu']['AccountMgmt']['StudentRegistryInfo'], "index.php");

$MODULE_OBJ['title'] = $Lang['Header']['Menu']['StudentRegistry'];

$MODULE_OBJ = $lsr->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START(urldecode($_REQUEST['ReturnMessage'])); 

$PAGE_NAVIGATION[] = array($Lang['StudentRegistry']['ClassList'], "index.php?AcademicYearID=$AcademicYearID");
$PAGE_NAVIGATION[] = array($Lang['StudentRegistry']['StudentList'], "class.php?AcademicYearID=$AcademicYearID");

# step information
$STEPS_OBJ[] = array($i_general_select_csv_file, 1);
$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 0);
$STEPS_OBJ[] = array($i_general_imported_result, 0);

# form class csv sample
$csvFile = "<a class='tablelink' href='". GET_CSV("sample_student_registry_malaysia.csv") ."'>". $Lang['General']['ClickHereToDownloadSample'] ."</a>";



?>
<script language="JavaScript">
var ClickID = '';
var callback_show_ref_list = {
	success: function ( o )
    {
	    var tmp_str = o.responseText;
	    document.getElementById('ref_list').innerHTML = tmp_str;
	    DisplayPosition();
	}
}


function show_ref_list(type, click)
{
	ClickID = click;
	document.getElementById('task').value = type;
	obj = document.form1;
	YAHOO.util.Connect.setForm(obj);
	var path = "ajax_import_macau_ref.php";
    var connectionObject = YAHOO.util.Connect.asyncRequest("POST", path, callback_show_ref_list);
}


function Hide_Window(pos){
  document.getElementById(pos).style.visibility='hidden';
}

function getPosition(obj, direction)
{
	var objStr = "obj";
	var pos_value = 0;
	while (typeof(eval(objStr))!="undefined" && eval(objStr + ".tagName")!="BODY")
	{
		pos_value += eval(objStr + "." + direction);
		objStr += ".offsetParent";
	}

	return pos_value;
}

function DisplayPosition(){
	
  document.getElementById('ref_list').style.left=getPosition(document.getElementById(ClickID),'offsetLeft') +10;
  document.getElementById('ref_list').style.top=getPosition(document.getElementById(ClickID),'offsetTop');
  document.getElementById('ref_list').style.visibility='visible';
}
</script>

<br />
<form name="form1" method="post" action="import_macau_confirm.php" enctype="multipart/form-data">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td><?=$linterface->GET_NAVIGATION($PAGE_NAVIGATION)?></td>
		<td align="right"><?=$linterface->GET_SYS_MSG($xmsg, $xmsg2);?></td>
	</tr>
	
	<tr>
		<td colspan="2"><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
	</tr>
	
	
	<tr>
		<td colspan="2">
			<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
				<!--
				<tr>
					<td class="formfieldtitle" width="30%" align="left"><?=$Lang['SysMgr']['FormClassMapping']['SchoolYear']?>: </td>
					<td class="tabletext" ><?=GetCurrentAcademicYear()?></td>
				</tr>
				-->
				<tr>
					<td class="formfieldtitle" align="left"><?=$Lang['General']['SourceFile']?> <span class="tabletextremark"><?=$Lang['General']['CSVFileFormat']?></span></td>
					<td class="tabletext"><input class="file" type="file" name="csvfile" id="csvfile"></td>
				</tr>
				<tr>
					<td class="formfieldtitle" align="left"><?=$Lang['General']['CSVSample']?> </td>
					<td class="tabletext"><?=$csvFile?></td>
				</tr>
				<tr>
					<td class="formfieldtitle" align="left"><?=$Lang['SysMgr']['Homework']['Import']['FieldTitle']['DataColumn']?> </td>
					<td class="tabletext"><?=$displayColumn?></td>
				</tr>
				<tr>
					<td class="formfieldtitle" align="left"><?=$Lang['SysMgr']['Homework']['Import']['FieldTitle']['Reference']?></td>
					<td class="tabletext"><?=$reference_str?></td>
				</tr>
				<tr>
					<td class="tabletextremark" colspan="2">
						<?=$Lang['SysMgr']['Homework']['Import']['FieldTitle']['RequiredField']?>
						<br>
						<?=$Lang['SysMgr']['Homework']['Import']['FieldTitle']['DateFormat']?>
					</td>
				</tr>
				
				<!--
				<tr>
					<td class="tabletext" align="left" colspan="2"><?=$iDiscipline['Import_Source_File_Note']?></td>
				</tr>
				<tr>
					<td class="tabletext" colspan="2"><?=$format_str?></td>
				</tr>
				-->
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<table width="95%" border="0" cellpadding="0" cellspacing="0" align="center">
				<tr>
					<td colspan="3" class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td align="center" colspan="2">
			<?= $linterface->GET_ACTION_BTN($button_continue, "submit") ?>&nbsp; 
			<?=$linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='index.php?AcademicYearID=$AcademicYearID'")?>
		</td>
	</tr>
</table>
<input type="hidden" name="AcademicYearID" id="AcademicYearID value="<?=$AcademicYearID?>">
<input type="hidden" name="task" id="task" value="<?=$AcademicYearID?>">
<input type="hidden" name="ClickID" id="ClickID" value="<?=$AcademicYearID?>">
</form>
<br />
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
