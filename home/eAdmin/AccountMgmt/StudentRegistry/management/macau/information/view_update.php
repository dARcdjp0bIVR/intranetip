<?php
#using: 

#########################
#
#   Date:   2019-07-18 Cameron
#           fix: don't call updateCustomValue if there's no custom column [case #B165321]
#
#   Date:   2019-06-26  Philips
#           add Custom Column
#
#########################
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccessright.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry.php");

intranet_auth();
intranet_opendb();

$laccessright = new libaccessright();

if(!$plugin['AccountMgmt_StudentRegistry'] || (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-View") && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-Manage"))) {
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$lsr = new libstudentregistry();

$dataAry = array();

$dataAry['STUD_ID'] = $StudentID;
$dataAry['CODE'] = $DSEJ_Number;
$dataAry['S_CODE'] = $SchoolCode;

$pgAry = array();
$pgAry['NAME_C'] = $ec_NAME_C;
$pgAry['NAME_E'] = $ec_NAME_E;
$pgAry['TEL'] = $ec_HOME_TEL;
$pgAry['MOBILE'] = $ec_MOBILE_TEL;

$lsr->updateStudentRegistry_Simple_Macau($dataAry, $userID, $pgAry);

$userAry['ChineseName'] = $_POST['ChineseName'];
$userAry['EnglishName'] = $_POST['EnglishName'];
$userAry['Gender'] = $Gender;
$userAry['HomeTelNo'] = $HomePhone;
$lsr->updateStudentInfo($userAry, $userID);

# update eClass
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libeclass40.php");
$lu = new libuser($userID);
$le = new libeclass();
$le->eClass40UserUpdateInfo($lu->UserEmail, $userAry);

$custCol = $_POST['custCol'];
if (count($custCol)) {
    $custValues = array();
    foreach ((array)$custCol as $code => $colId) {
        $custValues[] = array(
            "ColumnID" => $colId,
            "value" => intranet_htmlspecialchars(trim($_POST[$code]))
        );
    }
    $lsr->updateCustomValue($userID, $custValues);
}

intranet_closedb();

header("Location: view.php?xmsg=1&AcademicYearID=$AcademicYearID&userID=$userID");
?>