<?php
// using : 

###########################################
#
#	Date:	2019-04-29 Philips
#           Create file
#
###########################################

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libaccessright.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
intranet_auth();
intranet_opendb();

$laccessright = new libaccessright();
if(!$plugin['AccountMgmt_StudentRegistry'] || (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-View") && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-Manage")) && !$_SESSION['ParentFillOnlineReg']) {
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$lexport = new libexporttext();

$linterface 	= new interface_html();
$lsr = new libstudentregistry();
$filename = "student_reg_sample.csv";

$exportColumn = array();
$exportColumn[0] = $Lang['StudentRegistry']['Macau_CSV'][0];
$exportColumn[1] = $Lang['StudentRegistry']['Macau_CSV'][1];

// Custom Column
$custCols = $lsr->getCustomColumn();
// $textLang = Get_Lang_Selection('DisplayText_ch', 'DisplayText_en');
foreach($custCols as $ccol){    
    array_push($exportColumn[0], $ccol['Code']);
    array_push($exportColumn[1], $ccol['DisplayText_ch']);
}

// empty data
$data = array();
for($i=0;$i<count($exportColumn[0]);$i++){
    $data[$i] = '';
}

$file_content .= $lexport->GET_EXPORT_TXT($data, $exportColumn);
$lexport->EXPORT_FILE($filename, $file_content);
?>