<?
// Modifying by:

############# Change Log [Start] ################
#
#	Date:	2020-03-16 Philips [2019-1115-1452-49066]
#			Add sync Guardian Info to Student Account 
#
#   Date:   2019-07-18 Cameron
#           fix: don't call updateCustomValue if there's no custom column [case #B165321]
#
############# Change Log [End] ################


$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libaccessright.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry.php");

intranet_opendb();

# Check access right
if(!$plugin['AccountMgmt_StudentRegistry'] || (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-View") && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-Manage"))) {
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$lsr = new libstudentregistry();
$linterface = new interface_html();

$thisAry = array();

$thisAry['NAME_C'] 	= intranet_htmlspecialchars(stripslashes(trim($_POST['ChineseName'])));
$thisAry['NAME_E'] 	= intranet_htmlspecialchars(stripslashes(trim($_POST['EnglishName'])));
$thisAry['SEX'] 	= intranet_htmlspecialchars(trim($Gender));
$thisAry['B_DATE'] 	= intranet_htmlspecialchars(trim($BirthDate));
$thisAry['TEL'] 	= intranet_htmlspecialchars(trim($HomePhone));

$thisAry['UserID'] 		= $userID;
$thisAry['STUD_ID'] 	= intranet_htmlspecialchars(trim($StudentID));
$thisAry['CODE'] 		= intranet_htmlspecialchars(trim($DSEJ_Number));
$thisAry['ENTRY_DATE']	= intranet_htmlspecialchars(trim($EntryDate));
$thisAry['S_CODE'] 		= intranet_htmlspecialchars(trim($SchoolCode));
$thisAry['B_PLACE'] 	= intranet_htmlspecialchars(trim($BirthPlace));
$thisAry['ID_TYPE'] 	= intranet_htmlspecialchars(trim($IdType));
$thisAry['ID_NO'] 		= intranet_htmlspecialchars(trim($IdCardNumber));
$thisAry['I_PLACE'] 	= intranet_htmlspecialchars(trim($IdPlace));
$thisAry['I_DATE'] 		= intranet_htmlspecialchars(trim($IdIssueDate));
$thisAry['V_DATE'] 		= intranet_htmlspecialchars(trim($IdValidDate));
$thisAry['S6_TYPE'] 	= intranet_htmlspecialchars(trim($StayType));
$thisAry['S6_IDATE'] 	= intranet_htmlspecialchars(trim($StayIssueDate));
$thisAry['S6_VDATE'] 	= intranet_htmlspecialchars(trim($StayValidDate));
$thisAry['NATION'] 		= intranet_htmlspecialchars(trim($Nationality));
$thisAry['ORIGIN'] 		= intranet_htmlspecialchars(stripslashes(trim($Province)));
$thisAry['RELIGION'] 	= intranet_htmlspecialchars(stripslashes(trim($Religion)));
$thisAry['R_AREA'] 		= intranet_htmlspecialchars(trim($ResArea));
$thisAry['RA_DESC'] 	= intranet_htmlspecialchars(stripslashes(trim($DistrictName)));
$thisAry['AREA'] 		= intranet_htmlspecialchars(trim($AddArea));
$thisAry['ROAD'] 		= intranet_htmlspecialchars(stripslashes(trim($Street)));
$thisAry['ADDRESS'] 	= intranet_htmlspecialchars(stripslashes(trim($AddDetail)));
$thisAry['EMAIL']		= intranet_htmlspecialchars(trim($Email));
if($tagName!="") {
	$tagIDAry = returnModuleTagIDByTagName($tagName, $lsr->Module);
	$thisAry['TAGID'] = implode(',', $tagIDAry);
} else {
	$thisAry['TAGID'] = "";
}

$thisAry['FATHER_C'] 	= intranet_htmlspecialchars(stripslashes(trim($FatherChiName)));
$thisAry['FATHER_E'] 	= intranet_htmlspecialchars(stripslashes(trim($FatherEngName)));
$thisAry['F_PROF'] 		= intranet_htmlspecialchars(stripslashes(trim($FatherJobName)));
$thisAry['F_MARTIAL'] 	= intranet_htmlspecialchars(trim($FatherMarry));
$thisAry['F_TEL'] 		= intranet_htmlspecialchars(trim($FatherHomePhone));
$thisAry['F_MOBILE'] 	= intranet_htmlspecialchars(trim($FatherCellPhone));
$thisAry['F_OFFICE'] 	= intranet_htmlspecialchars(trim($FatherOfficePhone));
$thisAry['F_EMAIL'] 	= intranet_htmlspecialchars(trim($FatherEmail));

$thisAry['MOTHER_C'] 	= intranet_htmlspecialchars(stripslashes(trim($MotherChiName)));
$thisAry['MOTHER_E'] 	= intranet_htmlspecialchars(stripslashes(trim($MotherEngName)));
$thisAry['M_PROF'] 		= intranet_htmlspecialchars(stripslashes(trim($MotherJobName)));
$thisAry['M_MARTIAL'] 	= intranet_htmlspecialchars(trim($MotherMarry));
$thisAry['M_TEL'] 		= intranet_htmlspecialchars(trim($MotherHomePhone));
$thisAry['M_MOBILE'] 	= intranet_htmlspecialchars(trim($MotherCellPhone));
$thisAry['M_OFFICE'] 	= intranet_htmlspecialchars(trim($MotherOfficePhone));
$thisAry['M_EMAIL'] 	= intranet_htmlspecialchars(trim($MotherEmail));

if($GuardRelation=="F") { 			# copy from father
	$thisAry['GUARDIAN_C'] 	= $thisAry['FATHER_C'];
	$thisAry['GUARDIAN_E'] 	= $thisAry['FATHER_E'];
	$thisAry['G_SEX'] 		= "M";
	$thisAry['G_PROF'] 		= $thisAry['F_PROF'];
	$thisAry['GUARD'] 		= "F";
	$thisAry['G_REL_OTHERS']= "";
	$thisAry['LIVE_SAME'] 	= "1";
	$thisAry['G_AREA'] 		= $thisAry['AREA'];
	$thisAry['G_ROAD'] 		= $thisAry['ROAD'];
	$thisAry['G_ADDRESS'] 	= $thisAry['ADDRESS'];
	$thisAry['G_TEL'] 		= $thisAry['F_TEL'];
	$thisAry['G_MOBILE'] 	= $thisAry['F_MOBILE'];
	$thisAry['G_OFFICE'] 	= $thisAry['F_OFFICE'];
	$thisAry['G_EMAIL'] 	= $thisAry['F_EMAIL'];
} else if($GuardRelation=="M") {	# copy from father
	$thisAry['GUARDIAN_C'] 	= $thisAry['MOTHER_C'];
	$thisAry['GUARDIAN_E'] 	= $thisAry['MOTHER_E'];
	$thisAry['G_SEX'] 		= "F";
	$thisAry['G_PROF'] 		= $thisAry['M_PROF'];
	$thisAry['GUARD'] 		= "M";
	$thisAry['G_REL_OTHERS']= "";
	$thisAry['LIVE_SAME'] 	= "1";
	$thisAry['G_AREA'] 		= $thisAry['AREA'];
	$thisAry['G_ROAD'] 		= $thisAry['ROAD'];
	$thisAry['G_ADDRESS']	= $thisAry['ADDRESS'];
	$thisAry['G_TEL'] 		= $thisAry['M_TEL'];
	$thisAry['G_MOBILE'] 	= $thisAry['M_MOBILE'];
	$thisAry['G_OFFICE'] 	= $thisAry['M_OFFICE'];
	$thisAry['G_EMAIL'] 	= $thisAry['M_EMAIL'];
} else {							# self input info
	$thisAry['GUARDIAN_C'] 	= intranet_htmlspecialchars(trim($GuardChiName));
	$thisAry['GUARDIAN_E'] 	= intranet_htmlspecialchars(trim($GuardEngName));
	$thisAry['G_SEX'] 		= intranet_htmlspecialchars(trim($GuardGender));
	$thisAry['G_PROF'] 		= intranet_htmlspecialchars(trim($GuardJobName));
	$thisAry['GUARD'] 		= intranet_htmlspecialchars(trim($GuardRelation));
	$thisAry['G_REL_OTHERS']= intranet_htmlspecialchars(trim($GuardRelationOther));
	$thisAry['LIVE_SAME'] 	= intranet_htmlspecialchars(trim($GuardStay));
	$thisAry['G_AREA'] 		= intranet_htmlspecialchars(trim($GuardArea));
	$thisAry['G_ROAD'] 		= intranet_htmlspecialchars(trim($GuardStreet));
	$thisAry['G_ADDRESS'] 	= intranet_htmlspecialchars(trim($GuardAddDetail));
	$thisAry['G_TEL'] 		= intranet_htmlspecialchars(trim($GuardHomePhone));
	$thisAry['G_MOBILE'] 	= intranet_htmlspecialchars(trim($GuardCellPhone));
	$thisAry['G_OFFICE'] 	= intranet_htmlspecialchars(trim($GuardOfficePhone));
	$thisAry['G_EMAIL'] 	= intranet_htmlspecialchars(trim($GuardEmail));
}

if($EmergencyRelation=="F") {
	$thisAry['EC_NAME_C'] 	= $thisAry['FATHER_C'];
	$thisAry['EC_NAME_E'] 	= $thisAry['FATHER_E'];
	$thisAry['EC_REL'] 		= intranet_htmlspecialchars(trim($EmergencyRelation));
	$thisAry['EC_TEL'] 		= $thisAry['F_TEL'];
	$thisAry['EC_MOBILE'] 	= $thisAry['F_MOBILE'];
	$thisAry['EC_OFFICE']	= $thisAry['F_OFFICE'];
	$thisAry['EC_AREA'] 	= $thisAry['AREA'];
	$thisAry['EC_ROAD'] 	= $thisAry['ROAD'];
	$thisAry['EC_ADDRESS'] 	= $thisAry['ADDRESS'];
} else if($EmergencyRelation=="M") {
	$thisAry['EC_NAME_C'] 	= $thisAry['MOTHER_C'];
	$thisAry['EC_NAME_E'] 	= $thisAry['MOTHER_E'];
	$thisAry['EC_REL'] 		= intranet_htmlspecialchars(trim($EmergencyRelation));
	$thisAry['EC_TEL'] 		= $thisAry['M_TEL'];
	$thisAry['EC_MOBILE'] 	= $thisAry['M_MOBILE'];
	$thisAry['EC_OFFICE']	= $thisAry['M_OFFICE'];
	$thisAry['EC_AREA'] 	= $thisAry['AREA'];
	$thisAry['EC_ROAD'] 	= $thisAry['ROAD'];
	$thisAry['EC_ADDRESS'] 	= $thisAry['ADDRESS'];
} else {
	$thisAry['EC_NAME_C'] 	= intranet_htmlspecialchars(trim($EmergencyChiName));
	$thisAry['EC_NAME_E']	= intranet_htmlspecialchars(trim($EmergencyEngName));
	$thisAry['EC_REL'] 		= intranet_htmlspecialchars(trim($EmergencyRelation));
	$thisAry['EC_TEL'] 		= intranet_htmlspecialchars(trim($EmergencyHomePhone));
	$thisAry['EC_MOBILE'] 	= intranet_htmlspecialchars(trim($EmergencyCellPhone));
	$thisAry['EC_OFFICE'] 	= intranet_htmlspecialchars(trim($EmergencyOfficePhone));
	$thisAry['EC_AREA'] 	= intranet_htmlspecialchars(trim($EmergencyArea));
	$thisAry['EC_ROAD'] 	= intranet_htmlspecialchars(trim($EmergencyStreet));
	$thisAry['EC_ADDRESS']	= intranet_htmlspecialchars(trim($EmergencyAddDetail));	
}


	# update student info in STUDENT_REGISTRY_STUDENT
	$dataAry = array(
					"STUD_ID"=>$thisAry['STUD_ID'],
					"CODE"=>$thisAry['CODE'],
					"ADMISSION_DATE"=>$thisAry['ENTRY_DATE'],
					"S_CODE"=>$thisAry['S_CODE'],
					"B_PLACE"=>$thisAry['B_PLACE'],
					"ID_TYPE"=>$thisAry['ID_TYPE'],
					"ID_NO"=>$thisAry['ID_NO'],
					"I_PLACE"=>$thisAry['I_PLACE'],
					"I_DATE"=>$thisAry['I_DATE'],
					"V_DATE"=>$thisAry['V_DATE'],
					"S6_TYPE"=>$thisAry['S6_TYPE'],
					"S6_IDATE"=>$thisAry['S6_IDATE'],
					"S6_VDATE"=>$thisAry['S6_VDATE'],
					"NATION"=>$thisAry['NATION'],
					"ORIGIN"=>$thisAry['ORIGIN'],
					"RELIGION"=>$thisAry['RELIGION'],
					"R_AREA"=>$thisAry['R_AREA'],
					"RA_DESC"=>$thisAry['RA_DESC'],
					"AREA"=>$thisAry['AREA'],
					"ROAD"=>$thisAry['ROAD'],
					"ADDRESS"=>$thisAry['ADDRESS'],
					"EMAIL"=>$thisAry['EMAIL'],
					"TAGID"=>$thisAry['TAGID']
				);
	$lsr->updateStudentRegistry_Simple_Macau($dataAry, $userID);
				
	# update student info in INTRANET_USER
	$studentAry = array(
					"ChineseName"=>$thisAry['NAME_C'],
					"EnglishName"=>$thisAry['NAME_E'],
					"Gender"=>$thisAry['SEX'],
					"DateOfBirth"=>$thisAry['B_DATE'],
					"HomeTelNo"=>$thisAry['TEL']
				);
	$lsr->updateStudentInfo($studentAry, $userID);
	
	# update eClass
	include_once($PATH_WRT_ROOT."includes/libuser.php");
	include_once($PATH_WRT_ROOT."includes/libeclass40.php");
	$lu = new libuser($userID);
	$le = new libeclass();
	$le->eClass40UserUpdateInfo($lu->UserEmail, $studentAry);
	
	
	# update father info in STUDENT_REGISTRY_PG
	$fatherAry = array(
					"NAME_C"=>$thisAry['FATHER_C'],
					"NAME_E"=>$thisAry['FATHER_E'],
					"PROF"=>$thisAry['F_PROF'],
					"MARITAL_STATUS"=>$thisAry['F_MARTIAL'],
					"TEL"=>$thisAry['F_TEL'],
					"MOBILE"=>$thisAry['F_MOBILE'],
					"OFFICE_TEL"=>$thisAry['F_OFFICE'],
					"EMAIL"=>$thisAry['F_EMAIL']					
				);
	$lsr->removeStudentRegistry_PG($userID, "F");			
	$lsr->updateStudentRegistry_PG($fatherAry, "F", $userID);
	
	# update mother info in STUDENT_REGISTRY_PG
	$motherAry = array(
					"NAME_C"=>$thisAry['MOTHER_C'],
					"NAME_E"=>$thisAry['MOTHER_E'],
					"PROF"=>$thisAry['M_PROF'],
					"MARITAL_STATUS"=>$thisAry['M_MARTIAL'],
					"TEL"=>$thisAry['M_TEL'],
					"MOBILE"=>$thisAry['M_MOBILE'],
					"OFFICE_TEL"=>$thisAry['M_OFFICE'],
					"EMAIL"=>$thisAry['M_EMAIL']					
				);
	$lsr->removeStudentRegistry_PG($userID, "M");			
	$lsr->updateStudentRegistry_PG($motherAry, "M", $userID);
	
	# update guardian info in STUDENT_REGISTRY_PG
	if($thisAry['GUARD']=="F") {
		$guardianAry = $fatherAry;
		$guardianAry['G_GENDER'] = "M";
	} else if($thisAry['GUARD']=="M") {
		$guardianAry = $motherAry;
		$guardianAry['G_GENDER'] = "F";
	} else {
		$guardianAry = array(
						"NAME_C"=>$thisAry['GUARDIAN_C'],
						"NAME_E"=>$thisAry['GUARDIAN_E'],
						"PROF"=>$thisAry['G_PROF'],
						"TEL"=>$thisAry['M_TEL'],
						"MOBILE"=>$thisAry['M_MOBILE'],
						"OFFICE_TEL"=>$thisAry['M_OFFICE'],
						"EMAIL"=>$thisAry['M_EMAIL']					
					);
		$guardianAry['G_GENDER'] = $thisAry['G_SEX'];
		$guardianAry['G_RELATION'] = $thisAry['G_REL_OTHERS'];
		$guardianAry['LIVE_SAME'] = $thisAry['LIVE_SAME'];
		$guardianAry['G_AREA'] = $thisAry['G_AREA'];
		$guardianAry['G_ROAD'] = $thisAry['G_ROAD'];
		$guardianAry['G_ADDRESS'] = $thisAry['G_ADDRESS'];
		$guardianAry['TEL'] = $thisAry['G_TEL'];
		$guardianAry['MOBILE'] = $thisAry['G_MOBILE'];
		$guardianAry['OFFICE_TEL'] = $thisAry['G_OFFICE'];
		$guardianAry['EMAIL'] = $thisAry['G_EMAIL'];
	}
	$guardianAry['PG_TYPE'] = "G";
	$guardianAry['StudentID'] = $userID;
	$guardianAry['GUARD'] = $thisAry['GUARD'];
	$lsr->removeStudentRegistry_PG($userID, "G");
	$lsr->insertStudentRegistry_PG($guardianAry);
	
	# 2020-03-16 (Philips) Sync Guardian to Student Account
	if($sys_custom['AccountMgmt']['cdsj_5_macau']['syncAccountAndRegistry']){
		$lsr->syncToAccountMainGuardian($userID, $guardianAry);
	}
	
	# update emergency contact info in STUDENT_REGISTRY_PG
	if($EmergencyRelation=="F") {
		$ecAry = $fatherAry;
		$ecAry['GUARD'] = "F";
	} else if($EmergencyRelation=="M") {
		$ecAry = $motherAry;
		$ecAry['GUARD'] = "M";
	} else {
		$ecAry = array(
						"NAME_C"=>$thisAry['EC_NAME_C'],
						"NAME_E"=>$thisAry['EC_NAME_E'],
						"TEL"=>$thisAry['EC_TEL'],
						"MOBILE"=>$thisAry['EC_MOBILE'],
						"OFFICE_TEL"=>$thisAry['G_OFFICE'],
					);
		$ecAry['GUARD'] = "O";
		$ecAry['G_AREA'] = $thisAry['EC_AREA'];
		$ecAry['G_ROAD'] = $thisAry['EC_ROAD'];
		$ecAry['G_ADDRESS'] = $thisAry['EC_ADDRESS'];
		$ecAry['TEL'] = $thisAry['EC_TEL'];
		$ecAry['OFFICE_TEL'] = $thisAry['EC_OFFICE'];
	}
	$ecAry['PG_TYPE'] = "E";
	$ecAry['StudentID'] = $userID;
	$lsr->removeStudentRegistry_PG($userID, "E");
	$lsr->insertStudentRegistry_PG($ecAry);

	# update custom column values
	$custCol = $_POST['custCol'];
	if (count($custCol)) {
		$custValues = array();
		foreach ((array)$custCol as $code => $colId) {
			$custValues[] = array(
				"ColumnID" => $colId,
				"value" => intranet_htmlspecialchars(trim($_POST[$code]))
			);
		}
		$lsr->updateCustomValue($userID, $custValues);
	}

intranet_closedb();

header("Location: view_adv.php?xmsg=1&AcademicYearID=$AcademicYearID&userID=$userID");

?>
