<?php
#using :

############# Change Log [Start]
#
#   Date:   2020-08-24 (Cameron)
#           Cancel customize flag for export xml [case #X193351]
#
#   Date:   2020-08-14 (Cameron)
#           add export xml method 2 and enable method 3 and 5
#
#	Date:   2010-11-23 (Henry Chow)
#			add "TAG" filter 
#
#	Date:   2010-07-27 Thomas
#			update the layout of Edit Button to IP25 standard
#			Add javascript function - 'checkEdit' to the Edit button
#
############# Change Log [End]

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccessright.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/json.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");

intranet_auth();
intranet_opendb();

$laccessright = new libaccessright();

if(!$plugin['AccountMgmt_StudentRegistry'] || (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-View") && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-Manage"))) {
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$lsr = new libstudentregistry();
$linterface = new interface_html();
$lpf = new libportfolio();
//$has_lp_access_right = (strstr($ck_user_rights, ":web:") && $lpf->HAS_RIGHT("learning_sharing")) ? 1 : 0;

$searchText = stripslashes(htmlspecialchars($searchText));

//$currentYearID = Get_Current_Academic_Year_ID();
//$currentYear = GetCurrentAcademicYear();


# class menu
//$classMenu = $lsr->getSelectClass("name='targetClass' id='targetClass' onChange='document.form1.submit()'", $targetClass, $i_Discipline_System_Award_Punishment_All_Classes);
$classMenu = $lsr->getSelectClassWithWholeForm("name=\"targetClass\" id=\"targetClass\" onChange=\"document.form1.submit()\"", $targetClass, $i_Discipline_System_Award_Punishment_All_Classes, $i_general_please_select, $AcademicYearID);
$studentAry = $lsr->storeStudent('0', $targetClass, $AcademicYearID);

# Button
$editBtn = $linterface->GET_LNK_EDIT("javascript:checkEdit(document.form1,'userID[]','view.php')", "", "", "", "", 0);

# registry status 
$registryStatusMenu = $lsr->getRegistryStatusMenu($registryStatus, ' onChange="document.form1.submit()"');

# record status
//$recordStatusMenu = $lsr->getRecordStatusMenu($recordstatus, ' onChange="document.form1.submit()"');

# tag selection
$tagAry = returnModuleAvailableTag($lsr->Module, $assocAry=1);
$temp = array();
$i = 0;
foreach($tagAry as $id=>$name) {
	$temp[$i][] = $id;
	$temp[$i][] = $name;
	$i++;
}
$tagSelection = getSelectByArray($temp, ' name="tagID" id="tagID" onChange="document.form1.submit()"', $tagID, 0, 0, $Lang['StudentRegistry']['AllTags']);

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

if($order == "") $order = 1;
if($field == "") $field = 0;
$pageSizeChangeEnabled = true;

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;

$li = new libdbtable2007($field, $order, $pageNo);

$years = GetAllAcademicYearInfo();
for($i=0; $i<sizeof($years); $i++) {
	list($yearid, $yearNameEN, $yearNameB5) = $years[$i];
	$yearAry[$i][] = $yearid;
	$yearAry[$i][] = Get_Lang_Selection($yearNameB5, $yearNameEN);	
}
$yearSelectionMenu = getSelectByArray($yearAry, 'name="AcademicYearID" id="AcademicYearID" onChange="changeYear()"', $AcademicYearID);


# conditions
$conds = "";

if($targetClass!="") {
	if(sizeof($studentAry)>0)
		$conds .= " AND ycu.UserID IN (".implode(',', $studentAry).")";
	else {
		if(is_numeric($targetClass)) 
			$conds .= " AND y.YearID=".$targetClass;	
		else
			$conds .= " AND yc.YearClassID=".str_replace("::","",$targetClass);	  
	}		
}

if($registryStatus!="")
	$conds .= " AND USR.RecordStatus='$registryStatus'";
/*
else 
	$conds .= " AND USR.RecordStatus IN (0,1,2)";
*/

if($tagID!='')
	$conds .= " AND CONCAT(',',srs.TAGID,',') LIKE '%,$tagID,%'";

if($searchText!="") {
	$conds .= " AND (
					".Get_Lang_Selection('yc.ClassTitleB5','yc.ClassTitleEN')." LIKE '%$searchText%' OR 
					USR.EnglishName LIKE '%$searchText%' OR 
					USR.ChineseName LIKE '%$searchText%' OR 
					srs.STUD_ID LIKE '%$searchText%' OR 
					srs.CODE LIKE '%$searchText%' OR
					srs.NAME_P LIKE '%$searchText%' OR
					srs.BIRTH_CERT_NO LIKE '%$searchText%' OR 
					srs.ID_NO LIKE '%$searchText%' OR 
					srs.ROAD LIKE '%$searchText%' OR
					srs.ADDRESS LIKE '%$searchText%' OR
					srs.EMAIL LIKE '%$searchText%' OR
					srs.RA_DESC LIKE '%$searchText%' OR
					USR.HomeTelNo LIKE '%$searchText%'
				)";
}
$name_field = getNameFieldByLang("USR.");
	
$sql = "SELECT ".
			Get_Lang_Selection('yc.ClassTitleB5','yc.ClassTitleEN')." as clsName,
			ycu.ClassNumber as clsNo,
			IF(USR.EnglishName='','---',IFNULL(USR.EnglishName,'---')) as engname,
			IF(USR.ChineseName='','---',IFNULL(USR.ChineseName,'---')) as chiname,
			IF(srs.STUD_ID='','---',IFNULL(srs.STUD_ID,'---')) as stud_id,
			IF(srs.CODE='','---',IFNULL(srs.CODE,'---')) as DSEJ,";
if($_SESSION["SSV_PRIVILEGE"]["eclass"]["Access2Portfolio"])
	$sql .= "IF(ps.RecordID IS NOT NULL AND ps.IsSuspend = 0, CONCAT('<a href=/home/portfolio/profile/student_info_teacher.php?StudentID=',USR.UserID,'><img src=$image_path/$LAYOUT_SKIN/icon_learning_portfolio.gif border=0></a>'), '---'), ";
else 
	$sql .= "USR.UserID, ";
$sql .= "
			IFNULL(srs.DateModified,'---') as lastUpdated,
			'---' as status,
			CONCAT('<input type=\'checkbox\' name=\'userID[]\' id=\'userID[]\' value=\'',USR.UserID,'\'>') as checkbox,
			USR.UserID,
			srs.ModifyBy,
			yc.AcademicYearID
		FROM
			INTRANET_USER USR LEFT OUTER JOIN
			YEAR_CLASS_USER ycu ON (ycu.UserID=USR.UserID) LEFT OUTER JOIN
			YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID) LEFT OUTER JOIN
			YEAR y ON (y.YearID=yc.YearID) LEFT OUTER JOIN
			STUDENT_REGISTRY_STUDENT srs ON (srs.UserID=USR.UserID) LEFT JOIN 
			{$eclass_db}.PORTFOLIO_STUDENT AS ps ON USR.UserID = ps.UserID
		WHERE
			USR.RecordType=2 AND
			yc.AcademicYearID='$AcademicYearID'
			$conds
		";

$li->sql = $sql;
$li->field_array = array("clsName", "clsNo", "engname", "chiname", "stud_id", "DSEJ");
//if($_SESSION["SSV_PRIVILEGE"]["eclass"]["Access2Portfolio"])
	$li->field_array[] = "ipf";
$li->field_array[] = "lastUpdated";
$li->field_array[] = "status";
$li->no_col = sizeof($li->field_array)+2;
$li->fieldorder2 = ", y.Sequence, yc.Sequence, ycu.ClassNumber";
$li->IsColOff = "StudentRegistry_StudentList";

$pos = 0;
$li->column_list .= "<th width='1'>#</th>\n";
$li->column_list .= "<th width='10%' >".$li->column_IP25($pos++, $Lang['StudentRegistry']['Class'])."</th>\n";
$li->column_list .= "<th width='12%' >".$li->column_IP25($pos++, $Lang['StudentRegistry']['ClassNumber'])."</th>\n";
$li->column_list .= "<th width='15%' >".$li->column_IP25($pos++, $Lang['StudentRegistry']['EnglishName'])."</th>\n";
$li->column_list .= "<th width='15%' >".$li->column_IP25($pos++, $Lang['StudentRegistry']['ChineseName'])."</th>\n";
$li->column_list .= "<th width='10%' >".$li->column_IP25($pos++, $Lang['StudentRegistry']['Import_Macau'][3])."</th>\n";
$li->column_list .= "<th width='12%' >".$li->column_IP25($pos++, $Lang['StudentRegistry']['DSEJ_Number'])."</th>\n";
if($_SESSION["SSV_PRIVILEGE"]["eclass"]["Access2Portfolio"]) {
	$li->column_list .= "<th width='6%' >".$ip20TopMenu['iPortfolio']."</th>\n"; $pos++;
} else 
	$pos++;
$li->column_list .= "<th width='10%' >".$li->column_IP25($pos++, $Lang['StudentRegistry']['LastUpdated'])."</th>\n";
$li->column_list .= "<th width='10%' >".$Lang['StudentRegistry']['ApproveStatus']."</th>\n"; $pos++;
$li->column_list .= "<th width='1'>".$li->check("userID[]")."</td>\n";

$CurrentPageArr['StudentRegistry'] = 1;
$CurrentPage = "Mgmt_RegistryInfo";

$TAGS_OBJ[] = array($Lang['Menu']['AccountMgmt']['StudentRegistryInfo'], "index.php");

$MODULE_OBJ['title'] = $Lang['Header']['Menu']['StudentRegistry'];

$MODULE_OBJ = $lsr->GET_MODULE_OBJ_ARR();

# navigation bar
$PAGE_NAVIGATION[] = array($Lang['StudentRegistry']['ClassList'], "index.php?AcademicYearID=".$AcademicYearID);
$PAGE_NAVIGATION[] = array($Lang['StudentRegistry']['StudentList'], "");

$linterface->LAYOUT_START();

?>
<style type="text/css">
html .jqueryslidemenu{height: 1%;} /*Holly Hack for IE7 and below*/
</style>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/<?=$LAYOUT_SKIN?>/js/form_class_management.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/thickbox.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.blockUI.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.autocomplete.js"></script>

<link rel="stylesheet" href="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.autocomplete.css" type="text/css" />
<link rel="stylesheet" href="<?=$PATH_WRT_ROOT?>templates/jquery/thickbox.css" type="text/css" media="screen" />

<script language="javascript">
function displaySpan(layerName) {
	if(document.getElementById('exportFlag').value==0) {
		document.getElementById(layerName).style.visibility = 'visible';
		document.getElementById('exportFlag').value = 1;
	} else {
		document.getElementById(layerName).style.visibility = 'hidden';
		document.getElementById('exportFlag').value = 0;
	}
}

function Check_Go_Search(evt)
{
	var key = evt.which || evt.charCode || evt.keyCode;
	
	if (key == 13) // enter
		document.form1.submit();
	else
		return false;
}

function displayExport() {
	displaySpan('export_option');	
}

function goExport(flag) {
	var str = "AcademicYearID=<?=$AcademicYearID?>&searchText="+document.getElementById('searchText').value+"&targetClass="+document.getElementById('targetClass').value+"&registryStatus="+document.getElementById('registryStatus').value;
	if(flag=="CSV") {
		self.location.href = "export_student_macau_csv.php?"+str;	
	// } else if(flag=="XML") {
	// 	self.location.href = "export_student_macau_xml.php?"+str;
	} else if(flag=="XML2") {
        self.location.href = "export_student_macau_xml2.php?"+str;
    } else if(flag=="XML3And5") {
        self.location.href = "export_student_macau_xml3and5.php?"+str;
    }

}	

function goImport() {
	self.location.href = "import_macau.php?AcademicYearID=<?=$AcademicYearID?>&targetClass=<?=$targetClass?>";	
}

function changeYear() {
	document.getElementById('searchText').value = '';
	document.getElementById('targetClass').selectedIndex = 0;
	document.getElementById('registryStatus').selectedIndex = 0;
	document.form1.submit();
}

</script>
<form name="form1" method="post" action="">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr> 
		<td>
			<div class="navigation">
    			<?=$linterface->GET_NAVIGATION($PAGE_NAVIGATION)?>
			</div>
			<div class="Conntent_tool">
				<a href="javascript:;" onClick="goImport()" class="import"><?=$button_import?></a>
				<div class="btn_option">
					<a href="javascript:;" class="export" id="btn_export" onclick="displayExport()"><?=$button_export?></a>
<!--					<a href="javascript:;" class="export" id="btn_export" onclick="goExport('CSV')">--><?//=$button_export?><!--</a>-->
					<br style="clear:both" />
					<div class="btn_option_layer" id="export_option" onClick="displaySpan('export_option')">
						<a href="javascript:;" onClick="goExport('CSV')" class="sub_btn"><?=$Lang['StudentRegistry']['CSV']?></a>
<!--						<a href="javascript:;" onClick="goExport('XML')" class="sub_btn">--><?//=$Lang['StudentRegistry']['XML']?><!--</a>-->
                        <a href="javascript:;" onClick="goExport('XML2')" class="sub_btn"><?=$Lang['StudentRegistry']['XML2']?></a>
                        <a href="javascript:;" onClick="goExport('XML3And5')" class="sub_btn"><?=$Lang['StudentRegistry']['XML']?></a>
					</div>
				</div>
				<!--<a href="javascript:;" class="print"><?=$button_print?></a>-->
				</div>
				<div class="Conntent_search">
					<input name="searchText" id="searchText" type="text" value="<?=$searchText?>" onKeyUp="Check_Go_Search(event)"/>
				</div>
				<br style="clear:both" />
			</div>
			<p class="spacer"></p>
			<div class="table_board">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr class="table-action-bar">
						<td valign="bottom">
							<div class="table_filter"><?=$yearSelectionMenu?> &nbsp;</div>
							<div class="table_filter">
								<?=$classMenu?>
								<?=$registryStatusMenu?>
								<?=$recordStatusMenu?>
								<?=$tagSelection?>
							</div>
							<br  style="clear:both"/>
						</td>
						<td valign="bottom"><div class="common_table_tool"> <a title="<?= $Lang['Btn']['Edit'] ?>" class="tool_edit" href="javascript:void(0);" onclick="javascript:document.form1.EditRecord.value=1;checkEdit(document.form1,'userID[]','view.php')" /><?= $Lang['Btn']['Edit'] ?></div></td>
					</tr>
				</table>
				<!--<div id="ClassListLayer">-->
				<?=$li->display()?>
				<!--</div>-->
			</div>
			<p>&nbsp;</p>
		</td>
	</tr>
</table>

<input type="hidden" name="EditRecord" id="EditRecord" value="0">
<input type="hidden" name="exportFlag" id="exportFlag" value="0">
<input type="hidden" name="pageNo" id="pageNo" value="<?=$li->pageNo; ?>" />
<input type="hidden" name="order" id="order" value="<?=$li->order; ?>" />
<input type="hidden" name="field" id="field" value="<?=$li->field; ?>" />
<input type="hidden" name="page_size_change" id="page_size_change" value="" />
<input type="hidden" name="numPerPage" id="numPerPage" value="<?=$li->page_size?>" />
<!--<input type="hidden" name="targetClass" id="targetClass" value="<?=$targetClass?>">-->
</form>
<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>