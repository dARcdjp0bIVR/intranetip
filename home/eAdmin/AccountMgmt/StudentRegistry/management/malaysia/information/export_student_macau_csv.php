<?php
# using: 
$PATH_WRT_ROOT = "../../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/libaccessright.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$laccessright = new libaccessright();

if(!$plugin['AccountMgmt_StudentRegistry'] || (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-View") && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-Manage"))) {
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}
 
# Temp Assign memory of this page
ini_set("memory_limit", "150M"); 

$lsr = new libstudentregistry();
$lexport = new libexporttext();

$ExportArr = array();

//$AcademicYearID = Get_Current_Academic_Year_ID();

# condition
$conds = "";

if($searchText != "")
	$conds .= " AND (".Get_Lang_Selection('yc.ClassTitleB5','yc.ClassTitleEN')." LIKE '%$searchText%' OR USR.EnglishName LIKE '%$searchText%' OR USR.ChineseName LIKE '%$searchText%' OR USR.WebSAMSRegNo LIKE '%$searchText%' OR srs.CODE LIKE '%$searchText%')";

if($targetClass!="") {
	$studentAry = $lsr->storeStudent('0', $targetClass);
	$conds .= " AND ycu.UserID IN (".implode(',', $studentAry).")";
}

if($registryStatus!="")
	$conds .= " AND USR.RecordStatus=$registryStatus";
else 
	$conds .= " AND USR.RecordStatus IN (0,1,2)";


$filename = "student_registry_class.csv";	


# SQL Statement
$sql = "SELECT 
			srs.STUD_ID,
			srs.CODE,
			USR.ChineseName,
			USR.EnglishName,
			USR.Gender,
			IF(USR.DateOfBirth='0000-00-00 00:00:00','',LEFT(USR.DateOfBirth,10)),
			srs.B_PLACE,
			srs.ID_TYPE,
			srs.ID_NO,
			srs.I_PLACE,
			srs.I_DATE,
			srs.V_DATE,
			srs.S6_TYPE,
			srs.S6_IDATE,
			srs.S6_VDATE,
			srs.NATION,
			srs.ORIGIN,
			IFNULL(USR.HomeTelNo, (IFNULL(MobileTelNo, '---'))) TEL,
			srs.R_AREA,
			srs.RA_DESC,
			srs.AREA,
			srs.ROAD,
			srs.ADDRESS,
			srs.S_CODE,
			y.YearName as GRADE,
			".Get_Lang_Selection('yc.ClassTitleB5','yc.ClassTitleEN')." as CLASS,
			ycu.ClassNumber,
			USR.UserID
		FROM
			INTRANET_USER USR LEFT OUTER JOIN 
			STUDENT_REGISTRY_STUDENT srs ON (srs.UserID=USR.UserID) LEFT OUTER JOIN
			YEAR_CLASS_USER ycu ON (ycu.UserID=USR.UserID) LEFT OUTER JOIN
			YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID) LEFT OUTER JOIN 
			YEAR y ON (y.YearID=yc.YearID)
		WHERE 
			yc.AcademicYearID='$AcademicYearID' AND
			USR.RecordType=2 
			$conds 
		ORDER BY 
			y.Sequence, yc.Sequence, ycu.ClassNumber
		";	
		
$row = $lsr->returnArray($sql,28);



# Create data array for export
for($i=0; $i<sizeof($row); $i++){
	list($STUD_ID, $CODE, $NAME_C, $NAME_P, $SEX, $B_DATE, $B_PLACE, $ID_TYPE, $ID_NO, $I_PLACE, $I_DATE, $V_DATE, $S6_TYPE, $S6_IDATE, $S6_VDATE, $NATION, $ORIGIN, $TEL, $R_AREA, $RA_DESC, $AREA, $ROAD, $ADDRESS, $s_CODE, $GRADE, $CLASS, $C_NO, $userid) = $row[$i];
	$ExportArr[$i][0] = $STUD_ID;		//student id
	$ExportArr[$i][1] = $CODE;			//code
	$ExportArr[$i][2] = $NAME_C;  		//name in chinese
	$ExportArr[$i][3] = $NAME_P;  		//name in p
	$ExportArr[$i][4] = $SEX;  			//sex
	$ExportArr[$i][5] = $B_DATE;  		//date of birth
	$ExportArr[$i][6] = $B_PLACE;  		//place of birth
	$ExportArr[$i][7] = $ID_TYPE;  		//id type
	$ExportArr[$i][8] = $ID_NO;  		//id number
	$ExportArr[$i][9] = $I_PLACE;  		//issue place
	$ExportArr[$i][10] = $I_DATE;  		//issue date
	$ExportArr[$i][11] = $V_DATE;  		//valid date
	$ExportArr[$i][12] = $S6_TYPE;  		
	$ExportArr[$i][13] = $S6_IDATE;  		
	$ExportArr[$i][14] = $S6_VDATE;  		
	$ExportArr[$i][15] = $NATION;  		//nation
	$ExportArr[$i][16] = $ORIGIN;  		//origin
	$ExportArr[$i][17] = $TEL;  		//telephone
	$ExportArr[$i][18] = $R_AREA;  		
	$ExportArr[$i][19] = $RA_DESC;  		
	$ExportArr[$i][20] = $AREA;  		//area
	$ExportArr[$i][21] = $ROAD;  		//road
	$ExportArr[$i][22] = $ADDRESS;  	//address
	$ExportArr[$i][35] = $S_CODE;  		
	$ExportArr[$i][36] = $GRADE;  		//form
	$ExportArr[$i][37] = $CLASS;  		//class name
	$ExportArr[$i][38] = $C_NO;  		//class number
	
	$father = $lsr->getParentGuardianInfo($userid, 'F');
	$mother = $lsr->getParentGuardianInfo($userid, 'M');
	$guardian = $lsr->getParentGuardianInfo($userid, 'G');
	$emergency = $lsr->getParentGuardianInfo($userid, 'E');
	
	$ExportArr[$i][23] = ($father['NAME_C']=="") ? $father['NAME_E'] : $father['NAME_C'];				// father name
	$ExportArr[$i][24] = ($mother['NAME_C']=="") ? $mother['NAME_E'] : $mother['NAME_C'];				// mother name
	$ExportArr[$i][25] = $father['PROF'];																// father professional
	$ExportArr[$i][26] = $mother['PROF'];																// mother professional
	$ExportArr[$i][27] = $Lang['StudentRegistry']['Relation'][$guardian['GUARD']];						// guardian relationship
	$ExportArr[$i][28] = $guardian['LIVE_SAME'];														// live with guardian

	$ExportArr[$i][29] = ($emergency['NAME_C']=="") ? $emergency['NAME_E'] : $emergency['NAME_C'];  	//emergency contact
	$ExportArr[$i][30] = ($emergency['GUARD']=="O") ? $emergency['G_RELATION'] : $Lang['StudentRegistry']['Relation'][$emergency['GUARD']]; 	//emergency relationship
	$ExportArr[$i][31] = ($emergency['TEL']=="") ? $emergency['MOBILE'] : $emergency['TEL'];  			//emergency telephone
	$ExportArr[$i][32] = $emergency['G_AREA'];  														//emergency area
	$ExportArr[$i][33] = $emergency['G_ROAD'];  														//emergency road
	$ExportArr[$i][34] = $emergency['G_ADDRESS']; 											 			//emergency address
	
	$ExportArr[$i][39] = ($guardian['NAME_C']=="") ? $guardian['NAME_E'] : $guardian['NAME_C'];			// guardian name
	$ExportArr[$i][40] = $guardian['G_RELATION'];			// guardian relationship
	$ExportArr[$i][41] = $guardian['PROF'];					// guardian professional
	$ExportArr[$i][42] = $guardian['G_AREA'];				// guardian area
	$ExportArr[$i][43] = $guardian['G_ROAD'];				// guardian road
	$ExportArr[$i][44] = $guardian['G_ADDRESS'];			// guardian address
	$ExportArr[$i][45] = $guardian['TEL'];					// guardian telephone
	$ExportArr[$i][46] = $guardian['MOBILE'];				// guardian mobile
		
}


# define column title
for($m=0; $m<2; $m++) {
	for($k=0; $k<sizeof($Lang['StudentRegistry']['CSV_field'][$m]); $k++) {
		$exportColumn[$m][$k] = $Lang['StudentRegistry']['CSV_field'][$m][$k];
	}
}

	
$export_content .= $lexport->GET_EXPORT_TXT_WITH_REFERENCE($ExportArr, $exportColumn, "\t", "\r\n", "\t", 0, "11");

intranet_closedb();

# Output the file to user browser
$lexport->EXPORT_FILE($filename, $export_content);

?>
