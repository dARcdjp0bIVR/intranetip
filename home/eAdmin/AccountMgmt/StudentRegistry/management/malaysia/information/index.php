<?php
#using : henry chow

############# Change Log [Start]
#
#	Date:   2010-07-27 Thomas
#			update the layout of Class List($tableContent)
#
############# Change Log [End]

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccessright.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry_ui.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$laccessright = new libaccessright();

if(!$plugin['AccountMgmt_StudentRegistry'] || (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-View") && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-ViewBasic") && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-ViewAdv") && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-Manage") && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-ManageBasic") && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-ManageAdv"))) {
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}


$lsr = new libstudentregistry();
$lsrUI = new libstudentregistry_ui();
$linterface = new interface_html();

$searchText = stripslashes(htmlspecialchars($searchText));

# table content
$totalStudent = 0;
$totalwaitApproval = 0;
$formNameAry = array();
$yearAry = array();

if(!isset($AcademicYearID) || $AcademicYearID=="")
	$AcademicYearID = Get_Current_Academic_Year_ID();

$years = GetAllAcademicYearInfo();
for($i=0; $i<sizeof($years); $i++) {
	list($yearid, $yearNameEN, $yearNameB5) = $years[$i];
	$yearAry[$i][] = $yearid;
	$yearAry[$i][] = Get_Lang_Selection($yearNameB5, $yearNameEN);	
}
$yearSelectionMenu = getSelectByArray($yearAry, 'name="AcademicYearID" id="AcademicYearID" onChange="document.form1.submit()"', $AcademicYearID, 0, 1);

$result = $lsr->displayClassInfoInStudentRegistry($AcademicYearID);

$formNameAry[] = $result[0][4];
$NoOfRecordinForm = 0;
$temptableContent = "";

for($i=0; $i<sizeof($result); $i++) {
	if(!in_array($result[$i][4], $formNameAry))
	{
		$tableContent .= "<tbody>
							<tr>
								<td style=\"background-color:#FFFFFF\" rowspan=\"".($NoOfRecordinForm+2)."\">".$result[$i-1][4]."</td>
								<td style=\"display:none\">&nbsp;</td>
								<td style=\"display:none\">&nbsp;</td>
								<td style=\"display:none\">&nbsp;</td>
							</tr>".$temptableContent."</tbody>";
		$NoOfRecordinForm = 0;
		$temptableContent = "";
		$formNameAry[] = $result[$i][4];
	}

	$temptableContent .= "<tr class=\"sub_row\">
							<td>".$result[$i][1]."</td>
		     			  	<td><a href=\"class.php?AcademicYearID=$AcademicYearID&targetClass=::".$result[$i][0]."\">".$result[$i][2]."</a></td>
		      			  	<td>0</td>
						  </tr>";
						  
	$NoOfRecordinForm ++;
	$totalStudent += $result[$i][2];
	$totalWaitApproval += 0;

	if($i == (sizeof($result) - 1))
	{
		$tableContent .= "<tbody>
							<tr>
								<td style=\"background-color:#FFFFFF\" rowspan=\"".($NoOfRecordinForm+2)."\">".$result[$i][4]."</td>
								<td style=\"display:none\">&nbsp;</td>
								<td style=\"display:none\">&nbsp;</td>
								<td style=\"display:none\">&nbsp;</td>
							</tr>".$temptableContent."</tbody>";
	}
}
/*
for($i=0; $i<sizeof($result); $i++) {
	$tableContent .= '	<tr class="sub_row">
							<td style="background-color:#FFFFFF">'.((!in_array($result[$i][4], $formNameAry)) ? $result[$i][4]:"&nbsp;").'</td>
							<td>'.$result[$i][1].'</td>
							<td><a href="class.php?targetClass=::'.$result[$i][0].'">'.$result[$i][2].'</a></td>
							<td>0</td>
						</tr>
	';
	$totalStudent += $result[$i][2];
	$totalWaitApproval += 0;
	if(!in_array($result[$i][4], $formNameAry))	$formNameAry[] = $result[$i][4];
}
*/

$currentYear = getCurrentAcademicYear();
$CurrentPageArr['StudentRegistry'] = 1;
$CurrentPage = "Mgmt_RegistryInfo";

$TAGS_OBJ[] = array($Lang['Menu']['AccountMgmt']['StudentRegistryInfo'], "index.php");

$MODULE_OBJ['title'] = $Lang['Header']['Menu']['StudentRegistry'];

$MODULE_OBJ = $lsr->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();
echo $lsrUI->Include_JS_CSS();

?>
<script language="javascript">
function displaySpan(layerName) {
	if(document.getElementById('exportFlag').value==0) {
		document.getElementById(layerName).style.visibility = 'visible';
		document.getElementById('exportFlag').value = 1;
	} else {
		document.getElementById(layerName).style.visibility = 'hidden';
		document.getElementById('exportFlag').value = 0;
	}
}

function goExport() {
	displaySpan('export_option');	
}

function reloadForm() {

	//document.form1.pageNo.value = 1;
	document.form1.submit();	
}

{
function Check_Go_Search(evt)
{
	var key = evt.which || evt.charCode || evt.keyCode;
	
	if (key == 13) { // enter 
		//Get_Class_List();
		document.form1.submit();
	}
	else
		return false;
}
}

{
function Get_Class_List()
{
	var PostVar = {
			searchText: encodeURIComponent($('Input#searchText').val()),
			AcademicYearID: encodeURIComponent($('#AcademicYearID').val())
			}

	Block_Element("ClassListLayer");
	$.post('ajax_get_class_list.php',PostVar,
					function(data){
						//alert(data);
						if (data == "die") 
							window.top.location = '/';
						else {
							$('div#ClassListLayer').html(data);
							Thick_Box_Init();
							UnBlock_Element("ClassListLayer");
						}
					});
}
	
}

function exportCSV() {
	self.location.href = 'export_csv.php?searchText='+document.getElementById('searchText').value;
}

// thick box function 
{
//on page load call tb_init
function Thick_Box_Init()
{   
	tb_init('a.thickbox, area.thickbox, input.thickbox');//pass where to apply thickbox
}
}
</script>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr> 
		<td class="main_content">
<form name="form1" action="class.php">
			<div class="content_top_tool">
				<div class="Conntent_tool">
				</div>
					<div class="Conntent_search">
						<input name="searchText" id="searchText" type="text" value="<?=$searchText?>" onkeyup="Check_Go_Search(event);"/>
					</div>
					<br style="clear:both" />
				</div>
			</div>
			
<!--<form name="form1">-->
			
			<div class="table_board">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td valign="bottom">
						<div class="table_filter">
						<?=$Lang['StudentRegistry']['AcademicYear']?> : <?=$yearSelectionMenu?>
						</div>
						<br  style="clear:both"/>
					</td>
					<td valign="bottom">&nbsp;</td>
				</tr>
			</table>
			<div id="ClassListLayer">
			<table class="common_table_list">
				<thead>
					<tr>
						<th><?=$Lang['AccountMgmt']['Form']?></th>
						<th><?=$Lang['StudentRegistry']['Class']?></th>
						<th><?=$Lang['StudentRegistry']['NoOfStudents']?></th>
						<th><a href="#"><?=$Lang['StudentRegistry']['WaitForApproval']?></a></th>
					</tr>
					<?=$tableContent?>
					<tr  class="total_row">
						<th>&nbsp;</th>
						<th><?=$Lang['StudentRegistry']['Total']?></th>
						<td><?=$totalStudent?></td>
						<td><?=$totalWaitApproval?></td>
					</tr>
				</thead>
			</table>
			</div>
<input type="hidden" name="exportFlag" id="exportFlag" value="0">
</form>
		</td>
	</tr>
</table>

</body>
<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>