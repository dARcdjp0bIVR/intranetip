<?
// Modifying by: henry chow

############# Change Log [Start] ################
#
#
############# Change Log [End] ################


$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libaccessright.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry.php");

intranet_auth();
intranet_opendb();

$laccessright = new libaccessright();

# Check access right
if(!$plugin['AccountMgmt_StudentRegistry'] || (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-View") && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-ViewAdv") && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-Manage") && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-ManageAdv"))) {
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$lsr = new libstudentregistry();
$linterface = new interface_html();

$thisAry = array();

$thisAry['NAME_C'] 				= intranet_htmlspecialchars(stripslashes(trim($ChineseName)));
$thisAry['NAME_E'] 				= intranet_htmlspecialchars(stripslashes(trim($EnglishName)));
$thisAry['SEX'] 				= intranet_htmlspecialchars(trim($Gender));
$thisAry['B_DATE'] 				= intranet_htmlspecialchars(trim($BirthDate));
$thisAry['TEL'] 				= intranet_htmlspecialchars(trim($HomePhone));

$thisAry['UserID']				= $userID;
$thisAry['STUD_ID']				= intranet_htmlspecialchars(trim($StudentID));
$thisAry['B_PLACE']				= intranet_htmlspecialchars(trim($BirthPlace));
$thisAry['B_PLACE_OTHERS']		= $thisAry['B_PLACE']=="-9" ? intranet_htmlspecialchars(trim($bplace_others)) : "";
$thisAry['NATION']				= intranet_htmlspecialchars(trim($Nationality));
$thisAry['NATION_OTHERS']		= $thisAry['NATION']=="-9" ? intranet_htmlspecialchars(trim($nation_others)) : "";
$thisAry['ANCESTRAL_HOME']		= intranet_htmlspecialchars(stripslashes(trim($AncestralHome)));
$thisAry['RACE']				= intranet_htmlspecialchars(stripslashes(trim($Race)));
$thisAry['RACE_OTHERS']			= $thisAry['RACE']=="-9" ? intranet_htmlspecialchars(stripslashes(trim($race_others))) : "";
$thisAry['ID_NO']				= intranet_htmlspecialchars(trim($IdCardNumber));
$thisAry['BIRTH_CERT_NO']		= intranet_htmlspecialchars(trim($BirthCertNo));
$thisAry['RELIGION']			= intranet_htmlspecialchars(trim($Religion));
$thisAry['RELIGION_OTHERS']		= $thisAry['RELIGION']=="-9" ? intranet_htmlspecialchars(trim($religion_others)) : "";
$thisAry['FAMILY_LANG']			= intranet_htmlspecialchars(trim($FamilyLang));
$thisAry['FAMILY_LANG_OTHERS']	= $thisAry['FAMILY_LANG']=="-9" ? intranet_htmlspecialchars(trim($familyLang_others)) : "";
$thisAry['PAYMENT_TYPE']		= intranet_htmlspecialchars(trim($PayType));
$thisAry['EMAIL'] 		  		= intranet_htmlspecialchars(stripslashes(trim($Email)));
$thisAry['LODGING'] 	  		= intranet_htmlspecialchars(trim($HouseState));
$thisAry['LODGING_OTHERS']		= $thisAry['LODGING']=="-9" ? intranet_htmlspecialchars(trim($lodging_others)) : "";
$thisAry['ADDRESS1'] 	  		= intranet_htmlspecialchars(stripslashes(trim($Address1)));
$thisAry['ADDRESS2'] 	  		= intranet_htmlspecialchars(stripslashes(trim($Address2)));
$thisAry['ADDRESS3']  	  		= intranet_htmlspecialchars(stripslashes(trim($Address3)));
$thisAry['ADDRESS4']  	  		= intranet_htmlspecialchars(stripslashes(trim($Address4)));
$thisAry['ADDRESS5']  	  		= intranet_htmlspecialchars(stripslashes(trim($Address5)));
$thisAry['ADDRESS6'] 	  		= intranet_htmlspecialchars(stripslashes(trim($Address6)));
$thisAry['ENTRY_DATE'] 	  		= intranet_htmlspecialchars(trim($EntryDate));
$thisAry['SIBLINGS']	  		= intranet_htmlspecialchars(trim($SiblingsStr));
$thisAry['PRI_SCHOOL']    		= intranet_htmlspecialchars(stripslashes(trim($PriSchool)));

$thisAry['FATHER_C'] 	   		= intranet_htmlspecialchars(stripslashes(trim($FatherChiName)));
$thisAry['FATHER_E'] 	   		= intranet_htmlspecialchars(stripslashes(trim($FatherEngName)));
$thisAry['F_PROF'] 	 	   		= intranet_htmlspecialchars(stripslashes(trim($FatherJobName)));
$thisAry['F_JOB_NATURE']   		= intranet_htmlspecialchars(stripslashes(trim($FatherJobNature)));
$thisAry['F_JOB_NATURE_OTHERS'] = $thisAry['F_JOB_NATURE']=="-9" ? intranet_htmlspecialchars(stripslashes(trim($f_jobNature_others))) : "";
$thisAry['F_JOB_TITLE']    		= intranet_htmlspecialchars(stripslashes(trim($FatherJobTitle)));
$thisAry['F_JOB_TITLE_OTHERS'] 	= $thisAry['F_JOB_TITLE']=="-9" ? intranet_htmlspecialchars(stripslashes(trim($f_jobTitle_others))) : "";
$thisAry['F_MARITAL'] 	   		= intranet_htmlspecialchars(trim($FatherMarry));
$thisAry['F_EDU_LEVEL']    		= intranet_htmlspecialchars(stripslashes(trim($FatherEduLevel)));
$thisAry['F_EDU_LEVEL_OTHERS']  = $thisAry['F_EDU_LEVEL']=="-9" ? intranet_htmlspecialchars(stripslashes(trim($f_eduLevel_others))) : "";
$thisAry['F_TEL'] 		   		= intranet_htmlspecialchars(trim($FatherHomePhone));
$thisAry['F_MOBILE']	   		= intranet_htmlspecialchars(trim($FatherCellPhone));
$thisAry['F_OFFICE'] 	   		= intranet_htmlspecialchars(trim($FatherOfficePhone));
$thisAry['F_POSTAL_ADDR1'] 		= intranet_htmlspecialchars(stripslashes(trim($FatherAddr1)));
$thisAry['F_POSTAL_ADDR2'] 		= intranet_htmlspecialchars(stripslashes(trim($FatherAddr2)));
$thisAry['F_POSTAL_ADDR3'] 		= intranet_htmlspecialchars(stripslashes(trim($FatherAddr3)));
$thisAry['F_POSTAL_ADDR4'] 		= intranet_htmlspecialchars(stripslashes(trim($FatherAddr4)));
$thisAry['F_POSTAL_ADDR5'] 		= intranet_htmlspecialchars(stripslashes(trim($FatherAddr5)));
$thisAry['F_POSTAL_ADDR6'] 		= intranet_htmlspecialchars(stripslashes(trim($FatherAddr6)));
$thisAry['F_POST_CODE']    		= intranet_htmlspecialchars(stripslashes(trim($FatherPostCode)));
$thisAry['F_EMAIL'] 	   		= intranet_htmlspecialchars(trim($FatherEmail));

$thisAry['MOTHER_C'] 	   		= intranet_htmlspecialchars(stripslashes(trim($MotherChiName)));
$thisAry['MOTHER_E'] 	   		= intranet_htmlspecialchars(stripslashes(trim($MotherEngName)));
$thisAry['M_PROF'] 	 	   		= intranet_htmlspecialchars(stripslashes(trim($MotherJobName)));
$thisAry['M_JOB_NATURE']   		= intranet_htmlspecialchars(stripslashes(trim($MotherJobNature)));
$thisAry['M_JOB_NATURE_OTHERS'] = $thisAry['M_JOB_NATURE']=="-9" ? intranet_htmlspecialchars(stripslashes(trim($m_jobNature_others))) : "";
$thisAry['M_JOB_TITLE']    		= intranet_htmlspecialchars(stripslashes(trim($MotherJobTitle)));
$thisAry['M_JOB_TITLE_OTHERS'] 	= $thisAry['M_JOB_TITLE']=="-9" ? intranet_htmlspecialchars(stripslashes(trim($m_jobTitle_others))) : "";
$thisAry['M_MARITAL'] 	   		= intranet_htmlspecialchars(trim($MotherMarry));
$thisAry['M_EDU_LEVEL']    		= intranet_htmlspecialchars(stripslashes(trim($MotherEduLevel)));
$thisAry['M_EDU_LEVEL_OTHERS']  = $thisAry['M_EDU_LEVEL']=="-9" ? intranet_htmlspecialchars(stripslashes(trim($m_eduLevel_others))) : "";
$thisAry['M_TEL'] 		   		= intranet_htmlspecialchars(trim($MotherHomePhone));
$thisAry['M_MOBILE']	   		= intranet_htmlspecialchars(trim($MotherCellPhone));
$thisAry['M_OFFICE'] 	   		= intranet_htmlspecialchars(trim($MotherOfficePhone));
$thisAry['M_POSTAL_ADDR1'] 		= intranet_htmlspecialchars(stripslashes(trim($MotherAddr1)));
$thisAry['M_POSTAL_ADDR2']		= intranet_htmlspecialchars(stripslashes(trim($MotherAddr2)));
$thisAry['M_POSTAL_ADDR3']		= intranet_htmlspecialchars(stripslashes(trim($MotherAddr3)));
$thisAry['M_POSTAL_ADDR4'] 		= intranet_htmlspecialchars(stripslashes(trim($MotherAddr4)));
$thisAry['M_POSTAL_ADDR5'] 		= intranet_htmlspecialchars(stripslashes(trim($MotherAddr5)));
$thisAry['M_POSTAL_ADDR6'] 		= intranet_htmlspecialchars(stripslashes(trim($MotherAddr6)));
$thisAry['M_POST_CODE']    		= intranet_htmlspecialchars(stripslashes(trim($MotherPostCode)));
$thisAry['M_EMAIL'] 	   		= intranet_htmlspecialchars(trim($MotherEmail));

if($GuardRelation=="F") { 			# copy from father
	$thisAry['GUARD'] 		   		= "F";
	$thisAry['GUARDIAN_C'] 	   		= $thisAry['FATHER_C'];
	$thisAry['GUARDIAN_E'] 	   		= $thisAry['FATHER_E'];
	$thisAry['G_SEX'] 		   		= "M";
	$thisAry['G_REL_OTHERS']   		= "";
	$thisAry['LIVE_SAME'] 	   		= "1";
	$thisAry['G_PROF'] 		   		= $thisAry['F_PROF'];
	$thisAry['G_JOB_NATURE']   		= $thisAry['F_JOB_NATURE'];
	$thisAry['G_JOB_NATURE_OTHERS'] = $thisAry['F_JOB_NATURE_OTHERS'];
	$thisAry['G_JOB_TITLE']    		= $thisAry['F_JOB_TITLE'];
	$thisAry['G_JOB_TITLE_OTHERS']  = $thisAry['F_JOB_TITLE_OTHERS'];
	$thisAry['G_MARITAL']	   		= $thisAry['F_MARITAL'];
	$thisAry['G_EDU_LEVEL']	   		= $thisAry['F_EDU_LEVEL'];
	$thisAry['G_EDU_LEVEL_OTHERS']	= $thisAry['F_EDU_LEVEL_OTHERS'];
	$thisAry['G_TEL']	   	   		= $thisAry['F_TEL'];
	$thisAry['G_MOBILE'] 	   		= $thisAry['F_MOBILE'];
	$thisAry['G_OFFICE'] 	   		= $thisAry['F_OFFICE'];
	$thisAry['G_POSTAL_ADDR1'] 		= $thisAry['F_POSTAL_ADDR1'];
	$thisAry['G_POSTAL_ADDR2'] 		= $thisAry['F_POSTAL_ADDR2'];
	$thisAry['G_POSTAL_ADDR3'] 		= $thisAry['F_POSTAL_ADDR3'];
	$thisAry['G_POSTAL_ADDR4'] 		= $thisAry['F_POSTAL_ADDR4'];
	$thisAry['G_POSTAL_ADDR5'] 		= $thisAry['F_POSTAL_ADDR5'];
	$thisAry['G_POSTAL_ADDR6'] 		= $thisAry['F_POSTAL_ADDR6'];
	$thisAry['G_POST_CODE']    		= $thisAry['F_POST_CODE'];
	$thisAry['G_EMAIL'] 	   		= $thisAry['F_EMAIL'];
} 
else if($GuardRelation=="M") {	# copy from father
	$thisAry['GUARD'] 		   		= "M";
	$thisAry['GUARDIAN_C'] 	   		= $thisAry['MOTHER_C'];
	$thisAry['GUARDIAN_E'] 	   		= $thisAry['MOTHER_E'];
	$thisAry['G_SEX'] 		   		= "F";
	$thisAry['G_REL_OTHERS']   		= "";
	$thisAry['LIVE_SAME'] 	   		= "1";
	$thisAry['G_PROF'] 		   		= $thisAry['M_PROF'];
	$thisAry['G_JOB_NATURE']   		= $thisAry['M_JOB_NATURE'];
	$thisAry['G_JOB_NATURE_OTHERS'] = $thisAry['M_JOB_NATURE_OTHERS'];
	$thisAry['G_JOB_TITLE']    		= $thisAry['M_JOB_TITLE'];
	$thisAry['G_JOB_TITLE_OTHERS']  = $thisAry['M_JOB_TITLE_OTHERS'];
	$thisAry['G_MARITAL']	  		= $thisAry['M_MARITAL'];
	$thisAry['G_EDU_LEVEL']	   		= $thisAry['M_EDU_LEVEL'];
	$thisAry['G_EDU_LEVEL_OTHERS']	= $thisAry['M_EDU_LEVEL_OTHERS'];
	$thisAry['G_TEL']	   	   		= $thisAry['M_TEL'];
	$thisAry['G_MOBILE'] 	   		= $thisAry['M_MOBILE'];
	$thisAry['G_OFFICE'] 	   		= $thisAry['M_OFFICE'];
	$thisAry['G_POSTAL_ADDR1'] 		= $thisAry['M_POSTAL_ADDR1'];
	$thisAry['G_POSTAL_ADDR2'] 		= $thisAry['M_POSTAL_ADDR2'];
	$thisAry['G_POSTAL_ADDR3'] 		= $thisAry['M_POSTAL_ADDR3'];
	$thisAry['G_POSTAL_ADDR4'] 		= $thisAry['M_POSTAL_ADDR4'];
	$thisAry['G_POSTAL_ADDR5'] 		= $thisAry['M_POSTAL_ADDR5'];
	$thisAry['G_POSTAL_ADDR6'] 		= $thisAry['M_POSTAL_ADDR6'];
	$thisAry['G_POST_CODE']    		= $thisAry['M_POST_CODE'];
	$thisAry['G_EMAIL'] 	   		= $thisAry['M_EMAIL'];
}
else {							# self input info
	$thisAry['GUARD']		   		= intranet_htmlspecialchars(trim($GuardRelation));
	$thisAry['GUARDIAN_C']	   		= intranet_htmlspecialchars(trim($GuardChiName));
	$thisAry['GUARDIAN_E']	   		= intranet_htmlspecialchars(trim($GuardEngName));
	$thisAry['G_SEX']		   		= intranet_htmlspecialchars(trim($GuardGender));
	$thisAry['G_REL_OTHERS']   		= intranet_htmlspecialchars(trim($GuardRelationOther));
	$thisAry['LIVE_SAME']	   		= intranet_htmlspecialchars(trim($GuardStay));	
	$thisAry['G_PROF']		   		= intranet_htmlspecialchars(trim($GuardJobName));
	$thisAry['G_JOB_NATURE']   		= intranet_htmlspecialchars(trim($GuardJobNature));
	$thisAry['G_JOB_NATURE_OTHERS'] = $thisAry['G_JOB_NATURE']=="-9" ? intranet_htmlspecialchars(trim($g_jobNature_others)) : "";
	$thisAry['G_JOB_TITLE']	   		= intranet_htmlspecialchars(trim($GuardJobTitle));
	$thisAry['G_JOB_TITLE_OTHERS']	= $thisAry['G_JOB_TITLE']=="-9" ? intranet_htmlspecialchars(trim($g_jobTitle_others)) : "";
	$thisAry['G_MARITAL']	   		= intranet_htmlspecialchars(trim($GuardMarry));
	$thisAry['G_EDU_LEVEL']	   		= intranet_htmlspecialchars(trim($GuardEduLevel));
	$thisAry['G_EDU_LEVEL_OTHERS']	= $thisAry['G_EDU_LEVEL']=="-9" ? intranet_htmlspecialchars(trim($g_eduLevel_others)) : "";
	$thisAry['G_TEL']		   		= intranet_htmlspecialchars(trim($GuardHomePhone));
	$thisAry['G_MOBILE']	   		= intranet_htmlspecialchars(trim($GuardCellPhone));
	$thisAry['G_OFFICE']	   		= intranet_htmlspecialchars(trim($GuardOfficePhone));
	$thisAry['G_POSTAL_ADDR1'] 		= intranet_htmlspecialchars(trim($GuardAddr1));
	$thisAry['G_POSTAL_ADDR2'] 		= intranet_htmlspecialchars(trim($GuardAddr2));
	$thisAry['G_POSTAL_ADDR3'] 		= intranet_htmlspecialchars(trim($GuardAddr3));
	$thisAry['G_POSTAL_ADDR4'] 		= intranet_htmlspecialchars(trim($GuardAddr4));
	$thisAry['G_POSTAL_ADDR5'] 		= intranet_htmlspecialchars(trim($GuardAddr5));
	$thisAry['G_POSTAL_ADDR6'] 		= intranet_htmlspecialchars(trim($GuardAddr6));
	$thisAry['G_POST_CODE']    		= intranet_htmlspecialchars(stripslashes(trim($GuardPostCode)));	
	$thisAry['G_EMAIL'] 	   		= intranet_htmlspecialchars(trim($GuardEmail));
}

$error = $lsr->checkData_Mal('edit', $thisAry);

if(sizeof($error)>0) {			# error on the data
	//debug_pr($_POST);
	//debug_pr($error);
	echo "<body onload='document.form1.submit();'>\n<form name='form1' method='POST' action='view_adv.php'>";
	
	echo "
		<input type='hidden' name='AcademicYearID' id='AcademicYearID' value='$AcademicYearID'>
		
		<input type='hidden' name='NAME_C' id='NAME_C' value='".$thisAry['NAME_C']."'>
		<input type='hidden' name='NAME_E' id='NAME_E' value='".$thisAry['NAME_E']."'>
		<input type='hidden' name='SEX' id='SEX' value='$Gender'>
		<input type='hidden' name='B_DATE' id='B_DATE' value='$BirthDate'>
		<input type='hidden' name='TEL' id='TEL' value='$HomePhone'>
	
		<input type='hidden' name='userID' id='userID' value='$userID'>	
		<input type='hidden' name='STUD_ID' id='STUD_ID' value='$StudentID'>
		<input type='hidden' name='B_PLACE' id='B_PLACE' value='$BirthPlace'>
		<input type='hidden' name='B_PLACE_OTHERS' id='B_PLACE_OTHERS' value='$bplace_others'>
		<input type='hidden' name='NATION' id='NATION' value='$Nationality'>
		<input type='hidden' name='NATION_OTHERS' id='NATION_OTHERS' value='$nation_others'>
		<input type='hidden' name='ANCESTRAL_HOME' id='ANCESTRAL_HOME' value='$AncestralHome'>
		<input type='hidden' name='RACE' id='RACE' value='$Race'>
		<input type='hidden' name='RACE_OTHERS' id='RACE_OTHERS' value='$race_others'>
		<input type='hidden' name='ID_NO' id='ID_NO' value='$IdCardNumber'>
		<input type='hidden' name='BIRTH_CERT_NO' id='BIRTH_CERT_NO' value='$BirthCertNo'>
		<input type='hidden' name='RELIGION' id='RELIGION' value='$Religion'>
		<input type='hidden' name='RELIGION_OTHERS' id='RELIGION_OTHERS' value='$religion_others'>
		<input type='hidden' name='FAMILY_LANG' id='FAMILY_LANG' value='$FamilyLang'>
		<input type='hidden' name='FAMILY_LANG_OTHERS' id='FAMILY_LANG_OTHERS' value='$familyLang_others'>
		<input type='hidden' name='PAYMENT_TYPE' id='PAYMENT_TYPE' value='$PayType'>
		<input type='hidden' name='EMAIL' id='EMAIL' value='$Email'>
		<input type='hidden' name='LODGING' id='LODGING' value='$HouseState'>
		<input type='hidden' name='LODGING_OTHERS' id='LODGING_OTHERS' value='$lodging_others'>
		<input type='hidden' name='ADDRESS1' id='ADDRESS1' value='$Address1'>
		<input type='hidden' name='ADDRESS2' id='ADDRESS2' value='$Address2'>
		<input type='hidden' name='ADDRESS3' id='ADDRESS3' value='$Address3'>
		<input type='hidden' name='ADDRESS4' id='ADDRESS4' value='$Address4'>
		<input type='hidden' name='ADDRESS5' id='ADDRESS5' value='$Address5'>
		<input type='hidden' name='ADDRESS6' id='ADDRESS6' value='$Address6'>
		<input type='hidden' name='ENTRY_DATE' id='ENTRY_DATE' value='$EntryDate'>
		<input type='hidden' name='PRI_SCHOOL' id='PRI_SCHOOL' value='$PriSchool'>
				
		<input type='hidden' name='FATHER_C' id='FATHER_C' value='$FatherChiName'>
		<input type='hidden' name='FATHER_E' id='FATHER_E' value='$FatherEngName'>
		<input type='hidden' name='F_PROF' id='F_PROF' value='$FatherJobName'>
		<input type='hidden' name='F_JOB_NATURE' id='F_JOB_NATURE' value='$FatherJobNature'>
		<input type='hidden' name='F_JOB_NATURE_OTHERS' id='F_JOB_NATURE_OTHERS' value='$f_jobNature_others'>
		<input type='hidden' name='F_JOB_TITLE' id='F_JOB_TITLE' value='$FatherJobTitle'>
		<input type='hidden' name='F_JOB_TITLE_OTHERS' id='F_JOB_TITLE_OTHERS' value='$f_jobTitle_others'>
		<input type='hidden' name='F_MARTIAL' id='F_MARTIAL' value='$FatherMarry'>
		<input type='hidden' name='F_EDU_LEVEL' id='F_EDU_LEVEL' value='$FatherEduLevel'>
		<input type='hidden' name='F_EDU_LEVEL_OTHERS' id='F_EDU_LEVEL_OTHERS' value='$f_eduLevel_others'>
		<input type='hidden' name='F_TEL' id='F_TEL' value='$FatherHomePhone'>
		<input type='hidden' name='F_MOBILE' id='F_MOBILE' value='$FatherCellPhone'>
		<input type='hidden' name='F_OFFICE' id='F_OFFICE' value='$FatherOfficePhone'>
		<input type='hidden' name='F_POSTAL_ADDR1' id='F_POSTAL_ADDR1' value='$FatherAddr1'>
		<input type='hidden' name='F_POSTAL_ADDR2' id='F_POSTAL_ADDR2' value='$FatherAddr2'>
		<input type='hidden' name='F_POSTAL_ADDR3' id='F_POSTAL_ADDR3' value='$FatherAddr3'>
		<input type='hidden' name='F_POSTAL_ADDR4' id='F_POSTAL_ADDR4' value='$FatherAddr4'>
		<input type='hidden' name='F_POSTAL_ADDR5' id='F_POSTAL_ADDR5' value='$FatherAddr5'>
		<input type='hidden' name='F_POSTAL_ADDR6' id='F_POSTAL_ADDR6' value='$FatherAddr6'>
		<input type='hidden' name='F_EMAIL' id='F_EMAIL' value='$FatherEmail'>
	
		<input type='hidden' name='MOTHER_C' id='MOTHER_C' value='$MotherChiName'>
		<input type='hidden' name='MOTHER_E' id='MOTHER_E' value='$MotherEngName'>
		<input type='hidden' name='M_PROF' id='M_PROF' value='$MotherJobName'>
		<input type='hidden' name='M_JOB_NATURE' id='M_JOB_NATURE' value='$MotherJobNature'>
		<input type='hidden' name='M_JOB_NATURE_OTHERS' id='M_JOB_NATURE_OTHERS' value='$m_jobNature_others'>
		<input type='hidden' name='M_JOB_TITLE' id='M_JOB_TITLE' value='$MotherJobTitle'>
		<input type='hidden' name='M_JOB_TITLE_OTHERS' id='M_JOB_TITLE_OTHERS' value='$m_jobTitle_others'>
		<input type='hidden' name='M_MARTIAL' id='M_MARTIAL' value='$MotherMarry'>
		<input type='hidden' name='M_EDU_LEVEL' id='M_EDU_LEVEL' value='$MotherEduLevel'>
		<input type='hidden' name='M_EDU_LEVEL_OTHERS' id='M_EDU_LEVEL_OTHERS' value='$_eduLevel_others'>
		<input type='hidden' name='M_TEL' id='M_TEL' value='$MotherHomePhone'>
		<input type='hidden' name='M_MOBILE' id='M_MOBILE' value='$MotherCellPhone'>
		<input type='hidden' name='M_OFFICE' id='M_OFFICE' value='$MotherOfficePhone'>
		<input type='hidden' name='M_POSTAL_ADDR1' id='M_POSTAL_ADDR1' value='$MotherAddr1'>
		<input type='hidden' name='M_POSTAL_ADDR2' id='M_POSTAL_ADDR2' value='$MotherAddr2'>
		<input type='hidden' name='M_POSTAL_ADDR3' id='M_POSTAL_ADDR3' value='$MotherAddr3'>
		<input type='hidden' name='M_POSTAL_ADDR4' id='M_POSTAL_ADDR4' value='$MotherAddr4'>
		<input type='hidden' name='M_POSTAL_ADDR5' id='M_POSTAL_ADDR5' value='$MotherAddr5'>
		<input type='hidden' name='M_POSTAL_ADDR6' id='M_POSTAL_ADDR6' value='$MotherAddr6'>
		<input type='hidden' name='M_EMAIL' id='M_EMAIL' value='$MotherEmail'>
	
		<input type='hidden' name='GUARD' id='GUARD' value='$GuardRelation'>
		<input type='hidden' name='GUARDIAN_C' id='GUARDIAN_C' value='$GuardChiName'>
		<input type='hidden' name='GUARDIAN_E' id='GUARDIAN_E' value='$GuardEngName'>
		<input type='hidden' name='G_SEX' id='G_SEX' value='$GuardGender'>
		<input type='hidden' name='G_REL_OTHERS' id='G_REL_OTHERS' value='$GuardRelationOther'>
		<input type='hidden' name='LIVE_SAME' id='LIVE_SAME' value='$GuardStay'>
		<input type='hidden' name='G_PROF' id='G_PROF' value='$GuardJobName'>
		<input type='hidden' name='G_JOB_NATURE' id='G_JOB_NATURE' value='$GuardJobNature'>
		<input type='hidden' name='G_JOB_NATURE_OTHERS' id='G_JOB_NATURE_OTHERS' value='$g_jobNature_others'>
		<input type='hidden' name='G_JOB_TITLE' id='G_JOB_TITLE' value='$GuardJobTitle'>
		<input type='hidden' name='G_JOB_TITLE_OTHERS' id='G_JOB_TITLE_OTHERS' value='$g_jobTitle_others'>
		<input type='hidden' name='G_EDU_LEVEL' id='G_EDU_LEVEL' value='$GuardEduLevel'>
		<input type='hidden' name='G_EDU_LEVEL_OTHERS' id='G_EDU_LEVEL_OTHERS' value='$g_eduLevel_others'>
		<input type='hidden' name='G_TEL' id='G_TEL' value='$GuardHomePhone'>
		<input type='hidden' name='G_MOBILE' id='G_MOBILE' value='$GuardCellPhone'>
		<input type='hidden' name='G_OFFICE' id='G_OFFICE' value='$GuardOfficePhone'>
		<input type='hidden' name='G_POSTAL_ADDR1' id='G_POSTAL_ADDR1' value='$GuardAddr1'>
		<input type='hidden' name='G_POSTAL_ADDR2' id='G_POSTAL_ADDR2' value='$GuardAddr2'>
		<input type='hidden' name='G_POSTAL_ADDR3' id='G_POSTAL_ADDR3' value='$GuardAddr3'>
		<input type='hidden' name='G_POSTAL_ADDR4' id='G_POSTAL_ADDR4' value='$GuardAddr4'>
		<input type='hidden' name='G_POSTAL_ADDR5' id='G_POSTAL_ADDR5' value='$GuardAddr5'>
		<input type='hidden' name='G_POSTAL_ADDR6' id='G_POSTAL_ADDR6' value='$GuardAddr6'>
		<input type='hidden' name='G_EMAIL' id='G_EMAIL' value='$GuardEmail'>
		";
		foreach($error as $key=>$val) {
			echo "<input type='hidden' name='error[]' id='error[]' value='$val'>\n";	
		}
		echo "
			<script language='javascript'>
				document.form1.submit();
			</script>
			</body>
		";
	exit;	
} else {						# data ok

	# update student info in STUDENT_REGISTRY_STUDENT
	$dataAry = array(
					"STUD_ID"			=>$thisAry['STUD_ID'],
					"B_PLACE"			=>$thisAry['B_PLACE'],
					"B_PLACE_OTHERS"	=>$thisAry['B_PLACE_OTHERS'],
					"NATION"			=>$thisAry['NATION'],
					"NATION_OTHERS"		=>$thisAry['NATION_OTHERS'],
					"ANCESTRAL_HOME"	=>$thisAry['ANCESTRAL_HOME'],
					"RACE"				=>$thisAry['RACE'],
					"RACE_OTHERS"		=>$thisAry['RACE_OTHERS'],
					"ID_NO"				=>$thisAry['ID_NO'],
					"BIRTH_CERT_NO"		=>$thisAry['BIRTH_CERT_NO'],
					"RELIGION"			=>$thisAry['RELIGION'],
					"RELIGION_OTHERS"	=>$thisAry['RELIGION_OTHERS'],
					"FAMILY_LANG"		=>$thisAry['FAMILY_LANG'],
					"FAMILY_LANG_OTHERS"=>$thisAry['FAMILY_LANG_OTHERS'],
					"PAYMENT_TYPE"		=>$thisAry['PAYMENT_TYPE'],
					"EMAIL"				=>$thisAry['EMAIL'],
					"LODGING"			=>$thisAry['LODGING'],
					"LODGING_OTHERS"	=>$thisAry['LODGING_OTHERS'],
					"ADDRESS1"			=>$thisAry['ADDRESS1'],
					"ADDRESS2"			=>$thisAry['ADDRESS2'],
					"ADDRESS3"			=>$thisAry['ADDRESS3'],
					"ADDRESS4"			=>$thisAry['ADDRESS4'],
					"ADDRESS5"			=>$thisAry['ADDRESS5'],
					"ADDRESS6"			=>$thisAry['ADDRESS6'],
					"ADMISSION_DATE"	=>$thisAry['ENTRY_DATE'],
					"BRO_SIS_IN_SCHOOL"	=>$thisAry['SIBLINGS'],
					"PRI_SCHOOL"		=>$thisAry['PRI_SCHOOL']
				);
	$lsr->updateStudentRegistry_Simple_Mal($dataAry, $userID);
				
	# update student info in INTRANET_USER
	$studentAry = array(
					"ChineseName"	=>$thisAry['NAME_C'],
					"EnglishName"	=>$thisAry['NAME_E'],
					"Gender"		=>$thisAry['SEX'],
					"DateOfBirth"	=>$thisAry['B_DATE'],
					"HomeTelNo"		=>$thisAry['TEL']
				);
	$lsr->updateStudentInfo($studentAry, $userID);

	# update eClass
	include_once($PATH_WRT_ROOT."includes/libuser.php");
	include_once($PATH_WRT_ROOT."includes/libeclass40.php");
	$lu = new libuser($userID);
	$le = new libeclass();
	$le->eClass40UserUpdateInfo($lu->UserEmail, $studentAry);
	
	# update father info in STUDENT_REGISTRY_PG
	$fatherAry = array(
					"GUARD"			 	=>"F",
					"NAME_C"		 	=>$thisAry['FATHER_C'],
					"NAME_E"		 	=>$thisAry['FATHER_E'],
					"G_GENDER"		 	=>"M",
					"PROF"			 	=>$thisAry['F_PROF'],
					"JOB_NATURE"	 	=>$thisAry['F_JOB_NATURE'],
					"JOB_NATURE_OTHERS" =>$thisAry['F_JOB_NATURE_OTHERS'],
					"JOB_TITLE"		 	=>$thisAry['F_JOB_TITLE'],
					"JOB_TITLE_OTHERS" =>$thisAry['F_JOB_TITLE_OTHERS'],
					"MARITAL_STATUS" 	=>$thisAry['F_MARITAL'],
					"EDU_LEVEL"		 	=>$thisAry['F_EDU_LEVEL'],
					"EDU_LEVEL_OTHERS"	=>$thisAry['F_EDU_LEVEL_OTHERS'],
					"TEL"			 	=>$thisAry['F_TEL'],
					"MOBILE"		 	=>$thisAry['F_MOBILE'],
					"OFFICE_TEL"	 	=>$thisAry['F_OFFICE'],
					"POSTAL_ADDR1"	 	=>$thisAry['F_POSTAL_ADDR1'],
					"POSTAL_ADDR2"	 	=>$thisAry['F_POSTAL_ADDR2'],
					"POSTAL_ADDR3"	 	=>$thisAry['F_POSTAL_ADDR3'],
					"POSTAL_ADDR4"	 	=>$thisAry['F_POSTAL_ADDR4'],
					"POSTAL_ADDR5"	 	=>$thisAry['F_POSTAL_ADDR5'],
					"POSTAL_ADDR6"	 	=>$thisAry['F_POSTAL_ADDR6'],
					"POST_CODE"		 	=>$thisAry['F_POST_CODE'],
					"EMAIL"			 	=>$thisAry['F_EMAIL']					
				);
	$lsr->removeStudentRegistry_PG($userID, "F");			
	$lsr->updateStudentRegistry_PG($fatherAry, "F", $userID);
	
	# update mother info in STUDENT_REGISTRY_PG
	$motherAry = array(
					"GUARD"			 	=>"M",
					"NAME_C"		 	=>$thisAry['MOTHER_C'],
					"NAME_E"		 	=>$thisAry['MOTHER_E'],
					"G_GENDER"		 	=>"F",
					"PROF"			 	=>$thisAry['M_PROF'],
					"JOB_NATURE"	 	=>$thisAry['M_JOB_NATURE'],
					"JOB_NATURE_OTHERS" =>$thisAry['M_JOB_NATURE_OTHERS'],
					"JOB_TITLE"		 	=>$thisAry['M_JOB_TITLE'],
					"JOB_TITLE_OTHERS"  =>$thisAry['M_JOB_TITLE_OTHERS'],
					"MARITAL_STATUS" 	=>$thisAry['M_MARITAL'],
					"EDU_LEVEL"		 	=>$thisAry['M_EDU_LEVEL'],
					"EDU_LEVEL_OTHERS"	=>$thisAry['M_EDU_LEVEL_OTHERS'],
					"TEL"			 	=>$thisAry['M_TEL'],
					"MOBILE"		 	=>$thisAry['M_MOBILE'],
					"OFFICE_TEL"	 	=>$thisAry['M_OFFICE'],
					"POSTAL_ADDR1"	 	=>$thisAry['M_POSTAL_ADDR1'],
					"POSTAL_ADDR2"	 	=>$thisAry['M_POSTAL_ADDR2'],
					"POSTAL_ADDR3"	 	=>$thisAry['M_POSTAL_ADDR3'],
					"POSTAL_ADDR4"	 	=>$thisAry['M_POSTAL_ADDR4'],
					"POSTAL_ADDR5"	 	=>$thisAry['M_POSTAL_ADDR5'],
					"POSTAL_ADDR6"	 	=>$thisAry['M_POSTAL_ADDR6'],
					"POST_CODE"		 	=>$thisAry['M_POST_CODE'],
					"EMAIL"			 	=>$thisAry['M_EMAIL']					
				);
	$lsr->removeStudentRegistry_PG($userID, "M");			
	$lsr->updateStudentRegistry_PG($motherAry, "M", $userID);

	# update guardian info in STUDENT_REGISTRY_PG
	$guardianAry = array(
					"GUARD"			 	=>$thisAry['GUARD'],
					"NAME_C"		 	=>$thisAry['GUARDIAN_C'],
					"NAME_E"		 	=>$thisAry['GUARDIAN_E'],
					"G_GENDER"		 	=>$thisAry['G_SEX'],
					"G_RELATION"	 	=>$thisAry['G_REL_OTHERS'],
					"LIVE_SAME"		 	=>$thisAry['LIVE_SAME'],
					"PROF"			 	=>$thisAry['G_PROF'],
					"JOB_NATURE"	 	=>$thisAry['G_JOB_NATURE'],
					"JOB_NATURE_OTHERS"	=>$thisAry['G_JOB_NATURE_OTHERS'],
					"JOB_TITLE"		 	=>$thisAry['G_JOB_TITLE'],
					"JOB_TITLE_OTHERS"	=>$thisAry['G_JOB_TITLE_OTHERS'],
					"MARITAL_STATUS" 	=>"",
					"EDU_LEVEL"		 	=>$thisAry['G_EDU_LEVEL'],
					"EDU_LEVEL_OTHERS"	=>$thisAry['G_EDU_LEVEL_OTHERS'],
					"TEL"			 	=>$thisAry['G_TEL'],
					"MOBILE"		 	=>$thisAry['G_MOBILE'],
					"OFFICE_TEL"	 	=>$thisAry['G_OFFICE'],
					"POSTAL_ADDR1"	 	=>$thisAry['G_POSTAL_ADDR1'],
					"POSTAL_ADDR2"	 	=>$thisAry['G_POSTAL_ADDR2'],
					"POSTAL_ADDR3"	 	=>$thisAry['G_POSTAL_ADDR3'],
					"POSTAL_ADDR4"	 	=>$thisAry['G_POSTAL_ADDR4'],
					"POSTAL_ADDR5"	 	=>$thisAry['G_POSTAL_ADDR5'],
					"POSTAL_ADDR6"	 	=>$thisAry['G_POSTAL_ADDR6'],
					"POST_CODE"			=>$thisAry['G_POST_CODE'],
					"EMAIL"				=>$thisAry['G_EMAIL']					
				);
	$lsr->removeStudentRegistry_PG($userID, "G");			
	$lsr->updateStudentRegistry_PG($guardianAry, "G", $userID);

/*
	# update guardian info in STUDENT_REGISTRY_PG
	if($thisAry['GUARD']=="F") {
		$guardianAry = $fatherAry;
		$guardianAry['G_GENDER'] 	= "M";
		$guardianAry['G_REL_OTHERS']= "";
		$guardianAry['Live_Same'] 	= "1";
	} else if($thisAry['GUARD']=="M") {
		$guardianAry = $motherAry;
		$guardianAry['G_GENDER'] 	= "F";
		$guardianAry['G_REL_OTHERS']= "";
		$guardianAry['Live_Same'] 	= "1";
	} else {
		$guardianAry = array(
						"NAME_C"=>$thisAry['GUARDIAN_C'],
						"NAME_E"=>$thisAry['GUARDIAN_E'],
						"PROF"=>$thisAry['G_PROF'],
						"TEL"=>$thisAry['M_TEL'],
						"MOBILE"=>$thisAry['M_MOBILE'],
						"OFFICE_TEL"=>$thisAry['M_OFFICE'],
						"EMAIL"=>$thisAry['M_EMAIL']					
					);
		$guardianAry['G_GENDER'] = $thisAry['G_SEX'];
		$guardianAry['G_RELATION'] = $thisAry['G_REL_OTHERS'];
		$guardianAry['LIVE_SAME'] = $thisAry['LIVE_SAME'];
		$guardianAry['G_AREA'] = $thisAry['G_AREA'];
		$guardianAry['G_ROAD'] = $thisAry['G_ROAD'];
		$guardianAry['G_ADDRESS'] = $thisAry['G_ADDRESS'];
		$guardianAry['TEL'] = $thisAry['G_TEL'];
		$guardianAry['MOBILE'] = $thisAry['G_MOBILE'];
		$guardianAry['OFFICE_TEL'] = $thisAry['G_OFFICE'];
		$guardianAry['EMAIL'] = $thisAry['G_EMAIL'];
	}
	$guardianAry['PG_TYPE'] = "G";
	$guardianAry['StudentID'] = $userID;
	$guardianAry['GUARD'] = $thisAry['GUARD'];
	$lsr->removeStudentRegistry_PG($userID, "G");
	$lsr->insertStudentRegistry_PG($guardianAry);
	*/
}

intranet_closedb();

header("Location: view_adv.php?xmsg=1&AcademicYearID=$AcademicYearID&userID=$userID");

?>
