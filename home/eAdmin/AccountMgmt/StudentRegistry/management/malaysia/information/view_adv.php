<?php
#using: henry chow

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccessright.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry_ui.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$laccessright = new libaccessright();

if(!$_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] && (!$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-View") && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-ViewAdv")) && ($laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-View") || $laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-ViewBasic"))) {
	header("Location: view.php?AcademicYearID=$AcademicYearID&userID=$userID");
	exit;
}

if(!$plugin['AccountMgmt_StudentRegistry'] || (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-ViewAdv") && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-ManageAdv"))) {
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$lsr = new libstudentregistry();
$lsrUI = new libstudentregistry_ui();
$linterface = new interface_html();

#Student's Info
$result = $lsr -> RETRIEVE_STUDENT_INFO_ADV_MAL($userID);
$Father = $lsr -> RETRIEVE_GUARDIAN_INFO_ADV_MAL($userID, 'F');
$Mother = $lsr -> RETRIEVE_GUARDIAN_INFO_ADV_MAL($userID, 'M');
$Guard 	= $lsr -> RETRIEVE_GUARDIAN_INFO_ADV_MAL($userID, 'G');

//echo $Father[0]['PROF'] !== '' ? "":"yeah";

#Gender related info
$gender_ary      = $lsr -> GENDER_ARY();
$gender_val      = $result[0]['Gender']? $lsr -> RETRIEVE_DATA_ARY($gender_ary, $result[0]['Gender']) : "--";
$gender_edit_ary = $lsr -> RETRIEVE_DATA_ARY($gender_ary);
$gender_RBL_html = $lsr -> GENERATE_RADIO_BTN_LIST($gender_edit_ary, "Gender", $gender_val);

#Nationality related info
$nation_ary      = $lsr -> MAL_NATION_DATA_ARY();
$nation_val      = $result[0]['NATION']? $lsr -> RETRIEVE_DATA_ARY($nation_ary, $result[0]['NATION']) : "--";
$nation_edit_ary = $lsr -> RETRIEVE_DATA_ARY($nation_ary);
$nation_DDL_html = $lsr -> GENERATE_DROP_DWON_LIST($nation_edit_ary, "Nationality", $nation_val, ' onChange="if(this.value==\'-9\') showSpan(\'spanNation\'); else hideSpan(\'spanNation\');"');
if($result[0]['NATION']=="-9" && $result[0]['NATION_OTHERS']!="") $nation_val .= " (".$result[0]['NATION_OTHERS'].")";

#Race related info
$race_ary 	   = $lsr -> MAL_RACE_DATA_ARY();
$race_val      = $result[0]['RACE']? $lsr -> RETRIEVE_DATA_ARY($race_ary, $result[0]['RACE']) : "--";
$race_edit_ary = $lsr -> RETRIEVE_DATA_ARY($race_ary);
$race_DDL_html = $lsr -> GENERATE_DROP_DWON_LIST($race_edit_ary, "Race", $race_val, ' onChange="if(this.value==\'-9\') showSpan(\'spanRace\'); else hideSpan(\'spanRace\');"');
if($result[0]['RACE']=="-9" && $result[0]['RACE_OTHERS']!="") $race_val .= " (".$result[0]['RACE_OTHERS'].")";

#religion related info
$religion_ary      = $lsr -> MAL_RELIGION_DATA_ARY();
$religion_val      = $result[0]['RELIGION']? $lsr -> RETRIEVE_DATA_ARY($religion_ary, $result[0]['RELIGION']) : "--";
$religion_edit_ary = $lsr -> RETRIEVE_DATA_ARY($religion_ary);
$religion_DDL_html = $lsr -> GENERATE_DROP_DWON_LIST($religion_edit_ary, "Religion", $religion_val, ' onChange="if(this.value==\'-9\') showSpan(\'spanReligion\'); else hideSpan(\'spanReligion\');"');
if($result[0]['RELIGION']=="-9" && $result[0]['RELIGION_OTHERS']!="") $religion_val .= " (".$result[0]['RELIGION_OTHERS'].")";

#Family Language related info
$familyLang_ary      = $lsr -> MAL_FAMILY_LANG_ARY();
$familyLang_val      = $result[0]['FAMILY_LANG']? $lsr -> RETRIEVE_DATA_ARY($familyLang_ary, $result[0]['FAMILY_LANG']) : "--";
$familyLang_edit_ary = $lsr -> RETRIEVE_DATA_ARY($familyLang_ary);
$familyLang_DDL_html = $lsr -> GENERATE_DROP_DWON_LIST($familyLang_edit_ary, "FamilyLang", $familyLang_val, ' onChange="if(this.value==\'-9\') showSpan(\'spanFamilyLang\'); else hideSpan(\'spanFamilyLang\');"');
if($result[0]['FAMILY_LANG']=="-9" && $result[0]['FAMILY_LANG_OTHERS']!="") $familyLang_val .= " (".$result[0]['FAMILY_LANG_OTHERS'].")";

#Family Language related info
$payType_ary      = $lsr -> MAL_PAYMENT_TYPE_ARY();
$payType_val      = $result[0]['PAYMENT_TYPE']? $lsr -> RETRIEVE_DATA_ARY($payType_ary, $result[0]['PAYMENT_TYPE']) : "--";
$payType_edit_ary = $lsr -> RETRIEVE_DATA_ARY($payType_ary);
$payType_DDL_html = $lsr -> GENERATE_DROP_DWON_LIST($payType_edit_ary, "PayType", $payType_val);

#BirthPlace related info
$BPlace_ary      = $lsr -> MAL_B_PLACE_DATA_ARY();
$BPlace_val      = $result[0]['B_PLACE']? $lsr -> RETRIEVE_DATA_ARY($BPlace_ary, $result[0]['B_PLACE']) : "--";
$BPlace_edit_ary = $lsr -> RETRIEVE_DATA_ARY($BPlace_ary);
$BPlace_DDL_html = $lsr -> GENERATE_DROP_DWON_LIST($BPlace_edit_ary, "BirthPlace", $BPlace_val, ' onChange="if(this.value==\'-9\') showSpan(\'spanBPlace\'); else hideSpan(\'spanBPlace\');"');
if($result[0]['B_PLACE']=="-9" && $result[0]['B_PLACE_OTHERS']!="") $BPlace_val .= " (".$result[0]['B_PLACE_OTHERS'].")";

#Housing State related info
$HouseState_ary      = $lsr -> MAL_LODGING_ARY();
$HouseState_val      = $result[0]['LODGING']? $lsr -> RETRIEVE_DATA_ARY($HouseState_ary, $result[0]['LODGING']) : "--";
$HouseState_edit_ary = $lsr -> RETRIEVE_DATA_ARY($HouseState_ary);
$HouseState_DDL_html = $lsr -> GENERATE_DROP_DWON_LIST($HouseState_edit_ary, "HouseState", $HouseState_val, ' onChange="if(this.value==\'-9\') showSpan(\'spanLodging\'); else hideSpan(\'spanLodging\');"');
if($result[0]['LODGING']=="-9" && $result[0]['LODGING_OTHERS']!="") $HouseState_val .= " (".$result[0]['LODGING_OTHERS'].")";

# Registry Status
$registry_status = $lsr -> RETRIEVE_STUDENT_REGISTRY_STATUS($userID);
switch($registry_status) {
	case 1: $ownRegistryStatus = $Lang['StudentRegistry']['RegistryStatus_Left'];
			break;
	case 2: $ownRegistryStatus = $Lang['StudentRegistry']['RegistryStatus_Suspend'];
			break;
	case 3: $ownRegistryStatus = $Lang['StudentRegistry']['RegistryStatus_Resume'];
			break;
	default: $ownRegistryStatus = $Lang['StudentRegistry']['RegistryStatus_Approved'];	
}

//debug_pr($result[0]['Gender']);
//debug_pr($gender_DDL_html);

#Common Array (For Father, Mother, Guardian)
	$Marry_ary 			= $lsr -> MAL_MARITAL_STATUS_DATA_ARY();
	$Marry_edit_ary 	= $lsr -> RETRIEVE_DATA_ARY($Marry_ary);
	$JobNature_ary 		= $lsr -> MAL_JOB_NATURE_DATA_ARY();
	$JobNature_edit_ary = $lsr -> RETRIEVE_DATA_ARY($JobNature_ary);
	$JobTitle_ary 		= $lsr -> MAL_JOB_TITLE_DATA_ARY();
	$JobTitle_edit_ary 	= $lsr -> RETRIEVE_DATA_ARY($JobTitle_ary);
	$EduLevel_ary		= $lsr -> MAL_EDU_LEVEL_DATA_ARY();
	$EduLevel_edit_ary	= $lsr -> RETRIEVE_DATA_ARY($EduLevel_ary);
	
#Father's Info
	//$Father = $lsr -> RETRIEVE_GUARDIAN_INFO_ADV_MACAU($userID, 'F');
	#Father's Marital Status Together info
	$FatherMarry_val 	  = $Father[0]['MARITAL_STATUS'] !== null? $lsr -> RETRIEVE_DATA_ARY($Marry_ary, $Father[0]['MARITAL_STATUS']) : "--";
	$FatherMarry_DDL_html = $lsr -> GENERATE_DROP_DWON_LIST($Marry_edit_ary, "FatherMarry", $FatherMarry_val);
	
	#Father's Job Nature info
	$FatherJobNature_val = $Father[0]['JOB_NATURE'] !== null? $lsr -> RETRIEVE_DATA_ARY($JobNature_ary, $Father[0]['JOB_NATURE']) : "--";
	$FatherJobNature_DDL_html = $lsr -> GENERATE_DROP_DWON_LIST($JobNature_edit_ary, "FatherJobNature", $FatherJobNature_val, ' onChange="if(this.value==\'-9\') showSpan(\'span_F_JobNature\'); else hideSpan(\'span_F_JobNature\');"');
	if($Father[0]['JOB_NATURE']=="-9" && $Father[0]['JOB_NATURE_OTHERS']!="") $FatherJobNature_val .= " (".$Father[0]['JOB_NATURE_OTHERS'].")";
	
	#Father's Job Title info
	$FatherJobTitle_val = $Father[0]['JOB_TITLE'] !== null? $lsr -> RETRIEVE_DATA_ARY($JobTitle_ary, $Father[0]['JOB_TITLE']) : "--";
	$FatherJobTitle_DDL_html = $lsr -> GENERATE_DROP_DWON_LIST($JobTitle_edit_ary, "FatherJobTitle", $FatherJobTitle_val, ' onChange="if(this.value==\'-9\') showSpan(\'span_F_JobTitle\'); else hideSpan(\'span_F_JobTitle\');"');
	if($Father[0]['JOB_TITLE']=="-9" && $Father[0]['JOB_TITLE_OTHERS']!="") $FatherJobTitle_val .= " (".$Father[0]['JOB_TITLE_OTHERS'].")";
	
	#Father's Education Level info
	$FatherEduLevel_val = $Father[0]['EDU_LEVEL'] !== null? $lsr -> RETRIEVE_DATA_ARY($EduLevel_ary, $Father[0]['EDU_LEVEL']) : "--";
	$FatherEduLevel_DDL_html = $lsr -> GENERATE_DROP_DWON_LIST($EduLevel_edit_ary, "FatherEduLevel", $FatherEduLevel_val, ' onChange="if(this.value==\'-9\') showSpan(\'span_F_EduLevel\'); else hideSpan(\'span_F_EduLevel\');"');	
	if($Father[0]['EDU_LEVEL']=="-9" && $Father[0]['EDU_LEVEL_OTHERS']!="") $FatherEduLevel_val .= " (".$Father[0]['EDU_LEVEL_OTHERS'].")";
	
#Mother's Info
	//$Mother = $lsr -> RETRIEVE_GUARDIAN_INFO_ADV_MACAU($userID, 'M');
	#Mother's Marital Status info
	$MotherMarry_val 	  = $Mother[0]['MARITAL_STATUS'] !== null? $lsr -> RETRIEVE_DATA_ARY($Marry_ary, $Mother[0]['MARITAL_STATUS']) : "--";
	$MotherMarry_DDL_html = $lsr -> GENERATE_DROP_DWON_LIST($Marry_edit_ary, "MotherMarry", $MotherMarry_val);
	
	#Mother's Job Nature info
	$MotherJobNature_val = $Mother[0]['JOB_NATURE'] !== null? $lsr -> RETRIEVE_DATA_ARY($JobNature_ary, $Mother[0]['JOB_NATURE']) : "--";
	$MotherJobNature_DDL_html = $lsr -> GENERATE_DROP_DWON_LIST($JobNature_edit_ary, "MotherJobNature", $MotherJobNature_val, ' onChange="if(this.value==\'-9\') showSpan(\'span_M_JobNature\'); else hideSpan(\'span_M_JobNature\');"');
	if($Mother[0]['JOB_NATURE']=="-9" && $Mother[0]['JOB_NATURE_OTHERS']!="") $MotherJobNature_val .= " (".$Mother[0]['JOB_NATURE_OTHERS'].")";
	
	#Mother's Job Title info
	$MotherJobTitle_val = $Mother[0]['JOB_TITLE'] !== null? $lsr -> RETRIEVE_DATA_ARY($JobTitle_ary, $Mother[0]['JOB_TITLE']) : "--";
	$MotherJobTitle_DDL_html = $lsr -> GENERATE_DROP_DWON_LIST($JobTitle_edit_ary, "MotherJobTitle", $MotherJobTitle_val, ' onChange="if(this.value==\'-9\') showSpan(\'span_M_JobTitle\'); else hideSpan(\'span_M_JobTitle\');"');
	if($Mother[0]['JOB_TITLE']=="-9" && $Mother[0]['JOB_TITLE_OTHERS']!="") $MotherJobTitle_val .= " (".$Mother[0]['JOB_TITLE_OTHERS'].")";
	
	#Mother's Education Level info
	$MotherEduLevel_val = $Mother[0]['EDU_LEVEL'] !== null? $lsr -> RETRIEVE_DATA_ARY($EduLevel_ary, $Mother[0]['EDU_LEVEL']) : "--";
	$MotherEduLevel_DDL_html = $lsr -> GENERATE_DROP_DWON_LIST($EduLevel_edit_ary, "MotherEduLevel", $MotherEduLevel_val, ' onChange="if(this.value==\'-9\') showSpan(\'span_M_EduLevel\'); else hideSpan(\'span_M_EduLevel\');"');	
	if($Mother[0]['EDU_LEVEL']=="-9" && $Mother[0]['EDU_LEVEL_OTHERS']!="") $MotherEduLevel_val .= " (".$Mother[0]['EDU_LEVEL_OTHERS'].")";
	
#Guardian's Info
	//$Guard = $lsr -> RETRIEVE_GUARDIAN_INFO_ADV_MACAU($userID, 'G');
	#Guardian's Gender info
	$GuardGender_val	  = ($Guard[0]['GUARD'] == 'O' && $Guard[0]['G_GENDER'])? $lsr -> RETRIEVE_DATA_ARY($gender_ary, $Guard[0]['G_GENDER']) : "--";
	$GuardGender_RBL_html = $lsr -> GENERATE_RADIO_BTN_LIST($gender_edit_ary, "GuardGender", $GuardGender_val);

	#Guardian's Job Nature info
	$GuardJobNature_val = $Guard[0]['JOB_NATURE'] !== null? $lsr -> RETRIEVE_DATA_ARY($JobNature_ary, $Guard[0]['JOB_NATURE']) : "--";
	$GuardJobNature_DDL_html = $lsr -> GENERATE_DROP_DWON_LIST($JobNature_edit_ary, "GuardJobNature", $GuardJobNature_val, ' onChange="if(this.value==\'-9\') showSpan(\'span_G_JobNature\'); else hideSpan(\'span_G_JobNature\');"');
	if($Guard[0]['JOB_NATURE']=="-9" && $Guard[0]['JOB_NATURE_OTHERS']!="") $GuardJobNature_val .= " (".$Guard[0]['JOB_NATURE_OTHERS'].")";
	
	#Guardian's Job Title info
	$GuardJobTitle_val = $Guard[0]['JOB_TITLE'] !== null? $lsr -> RETRIEVE_DATA_ARY($JobTitle_ary, $Guard[0]['JOB_TITLE']) : "--";
	$GuardJobTitle_DDL_html = $lsr -> GENERATE_DROP_DWON_LIST($JobTitle_edit_ary, "GuardJobTitle", $GuardJobTitle_val, ' onChange="if(this.value==\'-9\') showSpan(\'span_G_JobTitle\'); else hideSpan(\'span_G_JobTitle\');"');
	if($Guard[0]['JOB_TITLE']=="-9" && $Guard[0]['JOB_TITLE_OTHERS']!="") $GuardJobTitle_val .= " (".$Guard[0]['JOB_TITLE_OTHERS'].")";
	
	#Guardian's Education Level info
	$GuardEduLevel_val = $Guard[0]['EDU_LEVEL'] !== null? $lsr -> RETRIEVE_DATA_ARY($EduLevel_ary, $Guard[0]['EDU_LEVEL']) : "--";
	$GuardEduLevel_DDL_html = $lsr -> GENERATE_DROP_DWON_LIST($EduLevel_edit_ary, "GuardEduLevel", $GuardEduLevel_val, ' onChange="if(this.value==\'-9\') showSpan(\'span_G_EduLevel\'); else hideSpan(\'span_G_EduLevel\');"');	
	if($Guard[0]['EDU_LEVEL']=="-9" && $Guard[0]['EDU_LEVEL_OTHERS']!="") $GuardEduLevel_val .= " (".$Guard[0]['EDU_LEVEL_OTHERS'].")";
	
	#Guardian's Relation with Student info
	$GuardRelation_ary 		= $lsr -> MAL_GUARD_DATA_ARY();
	$GuardRelation_val		= $Guard[0]['GUARD']? $lsr -> RETRIEVE_DATA_ARY($GuardRelation_ary, $Guard[0]['GUARD']) : $lsr -> RETRIEVE_DATA_ARY($GuardRelation_ary, 'O');
	$GuardRelation_edit_ary = $lsr -> RETRIEVE_DATA_ARY($GuardRelation_ary);
	$GuardRelation_RBL_html = $lsr -> GENERATE_RADIO_BTN_LIST($GuardRelation_edit_ary, "GuardRelation", $GuardRelation_val);

	#Guardian's Stay with Student info
	$GuardStay_ary 		= $lsr -> MAL_LIVE_SAME_ARY();
	$GuardStay_val 		= ($Guard[0]['GUARD'] == 'O' && $Guard[0]['LIVE_SAME']!="")? $lsr -> RETRIEVE_DATA_ARY($GuardStay_ary, $Guard[0]['LIVE_SAME']) : "--";
	$GuardStay_edit_ary = $lsr -> RETRIEVE_DATA_ARY($GuardStay_ary);
	$GuardStay_RBL_html = $lsr -> GENERATE_RADIO_BTN_LIST($GuardStay_edit_ary, "GuardStay", $GuardStay_val);

//debug_pr($FatherLive_val);
//debug_pr($FatherMarry_ary);
//debug_pr($Father[0]['MARITAL_STATUS']);

#Elder Brothers / Sister in School
if($result[0]['BRO_SIS_IN_SCHOOL'])
{
	$Siblings_UserID_ary = explode(",", $result[0]['BRO_SIS_IN_SCHOOL']);
	$Siblings_html = "";
	$Siblings_option_html = "";
	for($i=0;$i<sizeof($Siblings_UserID_ary);$i++)
	{
		$Siblings_Info = $lsr->getStudentInfo_by_UserID($Siblings_UserID_ary[$i], $AcademicYearID);
		
		if(!$Siblings_Info)
			continue;
		else
		{
			$text = '<a alt="#TB_inline?height=450&width=750&inlineId=FakeLayer" onclick="displayLinkedAccount('.$Siblings_Info['UserID'].');" class="thickbox" />'.(($Siblings_Info['YearOfLeft']!="") ? "(".$Siblings_Info['YearOfLeft'].")" : $Siblings_Info['ClassName']."-".$Siblings_Info['ClassNumber'])." ".$Siblings_Info['StudentName'].'</a>';
			$Siblings_html .= "<tr><td><em>".($i+1).".</em>$text</td></tr>";
			$Siblings_option_html .= "<option value=\"".$Siblings_Info['UserID']."\">".(($Siblings_Info['YearOfLeft']!="") ? "(".$Siblings_Info['YearOfLeft'].")" : $Siblings_Info['ClassName']."-".$Siblings_Info['ClassNumber'])." ".$Siblings_Info['StudentName']."</option>";
		}
	}
	if($Siblings_html == "")
		$Siblings_html = "<tr><td>---</td></tr>";
}
else
	$Siblings_html = "<tr><td>---</td></tr>";

#Past Enrollment Information
$Past_Enroll = $lsr -> RETRIEVE_STUDENT_PAST_ENROLLMENT_RECORD($userID);

if($Past_Enroll)
{
	$Past_Enroll_html = "";
	for($i=0;$i<sizeof($Past_Enroll);$i++)
	{
		$Past_Enroll_html .= "<tr>
        						<td><em>(".$Lang['StudentRegistry']['AcademicYear'].")</em></td>
								<td><span class=\"row_content\">".$Past_Enroll[$i]['YearName']."</span></td>
        						<td><em>(".$Lang['StudentRegistry']['Grade'].")</em></td>
								<td><span class=\"row_content\">".$Past_Enroll[$i]['Grade']."</span></td>								
								<td><em>(".$Lang['StudentRegistry']['Class'].")</em></td>
								<td><span class=\"row_content\">".$Past_Enroll[$i]['Class']."</span></td>
								<td><em>(".$Lang['StudentRegistry']['InClassNumber'].")</em></td>
								<td><span class=\"row_content\">".$Past_Enroll[$i]['ClassNumber']."</span></td>
                        	 </tr>";
	}
}
else
{
	$Past_Enroll_html = "<tr><td>---</td></tr>";
}

#Modified By
$ModifiedBy = $result[0]['ModifyBy']? $lsr -> RETRIEVE_MODIFIED_BY_INFO($result[0]['ModifyBy']) : "";

if(sizeof($error)>0) {
	$err_msg = "<div align='left'>\n<ul>";
	foreach($error as $key=>$val) {
		$err_msg .= "<font color='red'><li>$val</li></font>";
	}
    $err_msg .= "</ul>\n</div>";
} else {
	$err_msg = "<p class=\"spacer\"></p>";	
}
$CurrentPageArr['StudentRegistry'] = 1;
$CurrentPage = "Mgmt_RegistryInfo";

$TAGS_OBJ[] = array($Lang['Menu']['AccountMgmt']['StudentRegistryInfo'], "index.php");

$MODULE_OBJ['title'] = $Lang['Header']['Menu']['StudentRegistry'];

$MODULE_OBJ = $lsr->GET_MODULE_OBJ_ARR();

# navigation bar
# need to modified here!
$PAGE_NAVIGATION[] = array($Lang['StudentRegistry']['ClassList'], "index.php?AcademicYearID=$AcademicYearID");
$PAGE_NAVIGATION[] = array($result[0]['ClassName']." ".$Lang['StudentRegistry']['StudentList'], "class.php?AcademicYearID=$AcademicYearID&targetClass=::".$result[0]['YearClassID']);
$PAGE_NAVIGATION[] = array(($intranet_session_language == "en"? $result[0]['EnglishName'] : $result[0]['ChineseName']), "");

#Content Top Tool Button
$Content_Top_BTN[] = array($Lang['StudentRegistry']['AdvInfo'], "view_adv.php?AcademicYearID=$AcademicYearID&userID=$userID", "", true);
if($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] || $laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-ViewBasic"))
	$Content_Top_BTN[] = array($Lang['StudentRegistry']['BasicInfo'], "view.php?AcademicYearID=$AcademicYearID&userID=$userID", "", false);

if($xmsg==1) $msg = $Lang['General']['ReturnMessage']['UpdateSuccess'];
$linterface->LAYOUT_START(urldecode($msg));

echo $lsrUI->Include_JS_CSS();
?>
<script language="javascript">
	function EditInfo()
	{
		$('#LastModified').attr('style', 'display: none');
		$('#EditBtn').attr('style', 'visibility: hidden');
		
		$('.tabletextrequire').attr('style', '');
		$('#SaveBtn').attr('style', '');
		$('#CancelBtn').attr('style', '');
		$('#FormReminder').attr('style', '');
		
		$(':text').attr('style', '');
		$(':text').attr('readonly', '');
		$(':text[value="--"]').val('');
		
		$('.GuardRelationInfo').attr('style', '');
		$('#GuardNoInfo').attr('style', 'display: none');
		changeRelation('G');
		
		$('.Edit_Hide').attr('style', 'display: none');
		$('.Edit_Show').attr('style', '');
		<? if($result[0]['B_PLACE']=="-9") {?>
		$('#spanBPlace').attr('style', '');
		<?}?>
		<? if($result[0]['NATION']=="-9") {?>
		$('#spanNation').attr('style', '');
		<?}?>
		<? if($result[0]['RACE']=="-9") {?>
		$('#spanRace').attr('style', '');
		<?}?>
		<? if($result[0]['RELIGION']=="-9") {?>
		$('#spanReligion').attr('style', '');
		<?}?>
		<? if($result[0]['FAMILY_LANG']=="-9") {?>
		$('#spanFamilyLang').attr('style', '');
		<?}?>
		<? if($result[0]['LODGING']=="-9") {?>
		$('#spanLodging').attr('style', '');
		<?}?>
		<? if($Father[0]['JOB_NATURE']=="-9") {?>
		$('#span_F_JobNature').attr('style', '');
		<?}?>		
		<? if($Father[0]['JOB_TITLE']=="-9") {?>
		$('#span_F_JobTitle').attr('style', '');
		<?}?>	
		<? if($Father[0]['EDU_LEVEL']=="-9") {?>
		$('#span_F_EduLevel').attr('style', '');
		<?}?>	
		<? if($Mother[0]['JOB_NATURE']=="-9") {?>
		$('#span_M_JobNature').attr('style', '');
		<?}?>		
		<? if($Mother[0]['JOB_TITLE']=="-9") {?>
		$('#span_M_JobTitle').attr('style', '');
		<?}?>	
		<? if($Mother[0]['EDU_LEVEL']=="-9") {?>
		$('#span_M_EduLevel').attr('style', '');
		<?}?>	
		<? if($Guard[0]['JOB_NATURE']=="-9") {?>
		$('#span_G_JobNature').attr('style', '');
		<?}?>		
		<? if($Guard[0]['JOB_TITLE']=="-9") {?>
		$('#span_G_JobTitle').attr('style', '');
		<?}?>	
		<? if($Guard[0]['EDU_LEVEL']=="-9") {?>
		$('#span_G_EduLevel').attr('style', '');
		<?}?>	
		//$('.EmergencyRelationInfo').attr('style', '');
		//$('#EmergencyNoInfo').attr('style', 'display: none');
		//changeRelation('E');
	}
	
	function ResetInfo()
	{
		document.form1.reset();
		$('#LastModified').attr('style', '');
		$('#EditBtn').attr('style', '');
		
		$('.tabletextrequire').attr('style', 'display: none');
		$('#SaveBtn').attr('style', 'display: none');
		$('#CancelBtn').attr('style', 'display: none');
		$('#FormReminder').attr('style', 'display: none');
		
		$(':text').attr('style', 'border: 0px');
		$(':text').attr('readonly', 'readonly');
		$(':text[value=""]').val('--');
		
		$('.GuardRelationInfo').attr('style', '<?= $Guard? "" : "Display:none"?>');
		$('#GuardNoInfo').attr('style', '<?= $Guard? "Display:none" : ""?>');
		$('.GuardInfo').attr('style', '<?= $Guard[0]['GUARD'] == 'O'? "" : "Display:none"?>');
		$('#spanBPlace').attr('style', 'display: none');
		$('#spanNation').attr('style', 'display: none');
		$('#spanRace').attr('style', 'display: none');
		$('#spanReligion').attr('style', 'display: none');
		$('#spanFamilyLang').attr('style', 'display: none');
		$('#spanLodging').attr('style', 'display: none');
		$('#span_F_JobNature').attr('style', 'display: none');
		$('#span_F_JobTitle').attr('style', 'display: none');
		$('#span_F_EduLevel').attr('style', 'display: none');
		$('#span_M_JobNature').attr('style', 'display: none');
		$('#span_M_JobTitle').attr('style', 'display: none');
		$('#span_M_EduLevel').attr('style', 'display: none');
		$('#span_G_JobNature').attr('style', 'display: none');
		$('#span_G_JobTitle').attr('style', 'display: none');
		$('#span_G_EduLevel').attr('style', 'display: none');

		$('#ErrorLog').html('');
		
		$('.Edit_Hide').attr('style', '');
		$('.Edit_Show').attr('style', 'display: none');
		
		//$('.EmergencyRelationInfo').attr('style', '<?= $Emergency? "" : "Display:none"?>');
		//$('#EmergencyNoInfo').attr('style', '<?= $Emergency? "Display:none" : ""?>');
		//$('.EmergencyInfo').attr('style', '<?= $Emergency[0]['GUARD'] == 'O'? "" : "Display:none"?>');
	}
	
	function Hide_Table(table_name, hide_btn_name, show_btn_name)
	{
		$('#'+ table_name).slideUp();
		$('#'+ hide_btn_name).attr('style', 'display:none');
		$('#'+ show_btn_name).attr('style', '');
	}
	
	function Show_Table(table_name, hide_btn_name, show_btn_name)
	{
		$('#'+ table_name).slideDown();
		$('#'+ hide_btn_name).attr('style', 'display:none');
		$('#'+ show_btn_name).attr('style', '');
	}

/*	
	function changeStayType()
	{
		var select_type = $('[name="StayType"]').val();
		if(select_type == '1')
			$('.Stay_V_Date').attr('style', 'display:none');
		else
			$('.Stay_V_Date').attr('style', '');
	}
*/
	
	function changeRelation(type)
	{
		if(type == 'G')
		{
			var Relation_RBL = "GuardRelation";
			var TableClass = "GuardInfo";
		}
		else if(type == 'E')
		{
			var Relation_RBL = "EmergencyRelation";
			var TableClass = "EmergencyInfo";
		}
				
		var RelationInfo = $('[name="'+Relation_RBL+'"]:checked').val();
		if(RelationInfo == 'O')
			$('.'+TableClass).attr('style', '');
		else
			$('.'+TableClass).attr('style', 'display:none');
	}
	
	function Search_Siblings(obj)
	{

		$.post(
				'ajax_search_siblings.php',
				{
					AcademicYearID	: obj.AcademicYearID.value,
					StudentNo		: obj.SearchSiblings.value
				},
				function(data){
					//alert(data);
					if(data=="")
					{
						$('#SearchResult').html('<option value=""><?=$Lang['StudentRegistry']['NoRecord']?></option>');
					}
					else
					{
						$('#SearchResult').html(data);
					}
				});
	}
	
	function show_arg()
	{
		//var abc = $('[name="form1"]').serialize();
		//alert(abc);
		document.form1.action = "view_adv_update.php";
		document.form1.submit();
	}
	
	function checksubmit(obj)
	{
		var Gender = obj.Gender[0].checked? "M":"F";
		var GuardGender = obj.GuardGender[0].checked? "M":"F";
		var GuardRelation = obj.GuardRelation[0].checked? "F" : (obj.GuardRelation[1].checked? "M" : "O");
		var GuardStay = obj.GuardStay[0].checked? 1:0;
		
		var SiblingsLength = obj.elements['Siblings'].length;
		var SiblingsStr = "";
		for(i=0;i<SiblingsLength;i++)
		{
			SiblingsStr = SiblingsStr + obj.elements['Siblings'][i].value;
			if(i!=SiblingsLength-1)
				SiblingsStr = SiblingsStr + ',';
		}
		
		$.post(
				'ajax_view_check.php',
				{
					Mode 			  : 1,
					StudentID 		  : obj.StudentID.value,
					ChineseName 	  : obj.ChineseName.value,
					EnglishName 	  : obj.EnglishName.value,
					Gender 			  : Gender,
					BirthDate		  : obj.BirthDate.value,
					BirthPlace		  : obj.BirthPlace.value,
					Nationality 	  : obj.Nationality.value,
					AncestralHome	  : obj.AncestralHome.value,
					Race			  : obj.Race.value,
					IdCardNumber	  : obj.IdCardNumber.value,
					BirthCertNo		  : obj.BirthCertNo.value,
					Religion		  : obj.Religion.value,
					FamilyLang		  : obj.FamilyLang.value,
					PayType			  : obj.PayType.value,
					HomePhone 		  : obj.HomePhone.value,
					Email			  : obj.Email.value,
					HouseState		  : obj.HouseState.value,
					Address1		  : obj.Address1.value,
					Address2		  : obj.Address2.value,
					Address3		  : obj.Address3.value,
					Address4		  : obj.Address4.value,
					Address5		  : obj.Address5.value,
					Address6		  : obj.Address6.value,
					EntryDate		  : obj.EntryDate.value,
					SiblingsStr		  : SiblingsStr,
					PriSchool		  : obj.PriSchool.value,
					FatherChiName  	  : obj.FatherChiName.value,
					FatherEngName  	  : obj.FatherEngName.value,
					FatherJobName	  : obj.FatherJobName.value,
					FatherJobNature	  : obj.FatherJobNature.value,
					FatherJobTitle	  : obj.FatherJobTitle.value,
					FatherMarry		  : obj.FatherMarry.value,
					FatherEduLevel	  : obj.FatherEduLevel.value,
					FatherHomePhone   : obj.FatherHomePhone.value,
					FatherCellPhone   : obj.FatherCellPhone.value,
					FatherOfficePhone : obj.FatherOfficePhone.value,
					FatherAddr1		  : obj.FatherAddr1.value,
					FatherAddr2		  : obj.FatherAddr2.value,
					FatherAddr3		  : obj.FatherAddr3.value,
					FatherAddr4		  : obj.FatherAddr4.value,
					FatherAddr5		  : obj.FatherAddr5.value,
					FatherAddr6		  : obj.FatherAddr6.value,
					FatherPostCode	  : obj.FatherPostCode.value,
					FatherEmail		  : obj.FatherEmail.value,
					MotherChiName  	  : obj.MotherChiName.value,
					MotherEngName  	  : obj.MotherEngName.value,
					MotherJobName	  : obj.MotherJobName.value,
					MotherJobNature	  : obj.MotherJobNature.value,
					MotherJobTitle	  : obj.MotherJobTitle.value,
					MotherMarry		  : obj.MotherMarry.value,
					MotherEduLevel	  : obj.MotherEduLevel.value,
					MotherHomePhone   : obj.MotherHomePhone.value,
					MotherCellPhone   : obj.MotherCellPhone.value,
					MotherOfficePhone : obj.MotherOfficePhone.value,
					MotherAddr1		  : obj.MotherAddr1.value,
					MotherAddr2		  : obj.MotherAddr2.value,
					MotherAddr3		  : obj.MotherAddr3.value,
					MotherAddr4		  : obj.MotherAddr4.value,
					MotherAddr5		  : obj.MotherAddr5.value,
					MotherAddr6		  : obj.MotherAddr6.value,
					MotherPostCode	  : obj.MotherPostCode.value,
					MotherEmail		  : obj.MotherEmail.value,
					GuardRelation	  : GuardRelation,
					GuardChiName  	  : obj.GuardChiName.value,
					GuardEngName  	  : obj.GuardEngName.value,
					GuardGender		  : GuardGender,
					GuardRelationOther: obj.GuardRelationOther.value,
					GuardStay		  : GuardStay,
					GuardJobName	  : obj.GuardJobName.value,
					GuardJobNature	  : obj.GuardJobNature.value,
					GuardJobTitle	  : obj.GuardJobTitle.value,
					GuardEduLevel	  : obj.GuardEduLevel.value,
					GuardHomePhone    : obj.GuardHomePhone.value,
					GuardCellPhone    : obj.GuardCellPhone.value,
					GuardOfficePhone  : obj.GuardOfficePhone.value,
					GuardAddr1		  : obj.GuardAddr1.value,
					GuardAddr2		  : obj.GuardAddr2.value,
					GuardAddr3		  : obj.GuardAddr3.value,
					GuardAddr4		  : obj.GuardAddr4.value,
					GuardAddr5		  : obj.GuardAddr5.value,
					GuardAddr6		  : obj.GuardAddr6.value,
					GuardPostCode	  : obj.GuardPostCode.value,
					GuardEmail		  : obj.GuardEmail.value
				},
				function(data){
					if(data=="")
					{
						obj.SiblingsStr.value = SiblingsStr;
						obj.action = "view_adv_update.php";
						obj.submit();
					}
					else
					{
						$('#ErrorLog').html(data);
					}
				});
	}
	

	$(document).ready(function()
	{
		<? if(sizeof($error)==0) { ?>
			$(':text[value=""]').val('--');
		<? } ?>
		
		$('[name="GuardRelation"]').click(function(){
    		changeRelation('G');
		});
		
		//$('[name="EmergencyRelation"]').click(function(){
    	//	changeRelation('E');
		//});
	});
	
function showSpan(layerName) {
	document.getElementById(layerName).style.display = 'inline';
	//$('#spanNation').attr('style', '');
}

function hideSpan(layerName) {
	document.getElementById(layerName).style.display = 'none';	
	//$('#spanNation').attr('style', 'none');
}

function displayLinkedAccount(id) {
		$.post(
		"ajax_get_view_adv_layer.php", 
		{ 
			RecordType: 'Layer',
			AcademicYearID: <?=$AcademicYearID?>,
			userID : id 
		},
		function(ReturnData)
		{
			$('#TB_ajaxContent').html(ReturnData);			
		}
	);
}
	
</script>
<form name="form1" method="POST" action="">
<table width="95%" border="0" cellspacing="0" cellpadding="0">
	<tr> 
		<td class="main_content">
			<div class="navigation">
				<?=$linterface->GET_NAVIGATION($PAGE_NAVIGATION)?>
				<p class="spacer"></p>
			</div>
			<p class="spacer"></p>
			<div class="content_top_tool">
				<?= $linterface->GET_CONTENT_TOP_BTN($Content_Top_BTN)?>
			</div>
			<div class="table_board">
        		<div class="table_row_tool row_content_tool">
                	<?=$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-Manage") || $laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-ManageAdv") || $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"]? $linterface->GET_SMALL_BTN($Lang['StudentRegistry']['Edit'], "button", "javascript:EditInfo();", "EditBtn") : "&nbsp;" ?>
                </div>
                
        		<?=$err_msg?>				
                <div class="form_sub_title_v30">
                	<em>- <span class="field_title"><?= $Lang['StudentRegistry']['StudInfo'] ?></span> -</em>
                	<a id="Stud_Hide_Btn" href="javascript:void(0)" onclick="javascript:Hide_Table('StudentInfo_Table', 'Stud_Hide_Btn', 'Stud_Show_Btn');"><?= $Lang['StudentRegistry']['Hide'] ?></a>
                    <a id="Stud_Show_Btn" href="javascript:void(0)" onclick="javascript:Show_Table('StudentInfo_Table', 'Stud_Show_Btn', 'Stud_Hide_Btn');" style="display:none"><?= $Lang['StudentRegistry']['Show'] ?></a>
                    <p class="spacer"></p>
                </div>
                <table id="StudentInfo_Table" class="form_table_v30">
                	<col class="field_title_short">
                    <col class="field_content_short">
                    <col class="field_title_short">
                    <col class="field_content_short">
                    <col class="field_title_short">
                    <col class="field_content_short">
					<tbody>
						<tr>
                        	<td class="field_title_short"><span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['Mal_StudentNo'] ?></td>
                        	<td colspan="5"><input class="textboxnum" type="text" name="StudentID" value="<?= $result[0]['STUD_ID']?>" readonly="readonly" style="border: 0px"/></td>
						</tr>
                        <tr>
                            <td class="field_title_short"><span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['ChineseName'] ?></td>
                            <td><input class="textboxtext" type="text" name="ChineseName" value="<?= $result[0]['ChineseName']?>" readonly="readonly" style="border: 0px"/></td>
                            <td class="field_title_short"><span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['EnglishName'] ?></td>
                            <td><input class="textboxtext" type="text" name="EnglishName" value="<?= $result[0]['EnglishName']?>" readonly="readonly" style="border: 0px"/></td>
                        	<td>&nbsp;</td>
                        	<td>&nbsp;</td>
                        </tr>
						<tr>
                            <td class="field_title_short"><span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['Gender'] ?></td>
                            <td><span class="Edit_Hide"><?= $gender_val ?></span><?= $gender_RBL_html ?></td>
                            <td class="field_title_short"><span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['BirthDate']?></span></td>
                        	<td><input class="textboxnum" type="text" name="BirthDate" value="<?=$result[0]['DateOfBirth']? date("Y-m-d", strtotime($result[0]['DateOfBirth'])) : ""?>" readonly="readonly" style="border: 0px"/><span class="Edit_Show" style="display: none"> (yyyy-mm-dd) </span></td>
                            <td class="field_title_short"><span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['BirthPlace']?></span></td>
                            <td><span class="Edit_Hide"><?= $BPlace_val ?></span><?= $BPlace_DDL_html ?><div id="spanBPlace" style='display:none' class='form_field_sub_content'><table><tr><td><em><?=$i_staffAttendance_PleaseSpecify?></em></td><td><input type="text" name="bplace_others" id="bplace_others" value="<?=$result[0]['B_PLACE_OTHERS']?>"></td></tr></table></div></td>
                        </tr>
                        <tr>
                            <td class="field_title_short"><span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['Nationality']?></td>
                            <td><span class="Edit_Hide"><?= $nation_val ?></span><?= $nation_DDL_html ?><div id="spanNation" style='display:none' class='form_field_sub_content'><table><tr><td><em><?=$i_staffAttendance_PleaseSpecify?></em></td><td><input type="text" name="nation_others" id="nation_others" value="<?=$result[0]['NATION_OTHERS']?>"></td></tr></table></div></td>                      
                        	<td class="field_title_short"><span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['AncestalHome']?></td>
                        	<td><input class="textboxtext" type="text" name="AncestralHome" value="<?= $result[0]['ANCESTRAL_HOME']?>" readonly="readonly" style="border: 0px"/></td>
                        	<td class="field_title_short"><span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['Race']?></td>
                        	<td><span class="Edit_Hide"><?= $race_val ?></span><?= $race_DDL_html ?><div id="spanRace" style='display:none' class='form_field_sub_content'><table><tr><td><em><?=$i_staffAttendance_PleaseSpecify?></em></td><td><input type="text" name="race_others" id="race_others" value="<?=$result[0]['RACE_OTHERS']?>"></td></tr></table></div></td>
                        </tr>
                        <tr>
                        	<td class="field_title_short"><span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['IdCard'].$Lang['StudentRegistry']['CardNumber']?></td>
                        	<td><input class="textboxnum" type="text" name="IdCardNumber" value="<?= $result[0]['ID_NO']?>" readonly="readonly" style="border: 0px"/></td>                 
                        	<td class="field_title_short"><span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['BirthCertNo']?></td>
                        	<td colspan="3"><input class="textboxnum" type="text" name="BirthCertNo" value="<?= $result[0]['BIRTH_CERT_NO']?>" readonly="readonly" style="border: 0px"/></td>
                        </tr>
                        <tr>
                            <td class="field_title_short"><span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['Religion']?></td>
                            <td><span class="Edit_Hide"><?= $religion_val ?></span><?= $religion_DDL_html ?><div id="spanReligion" style='display:none' class='form_field_sub_content'><table><tr><td><em><?=$i_staffAttendance_PleaseSpecify?></em></td><td><input type="text" name="religion_others" id="religion_others" value="<?=$result[0]['RELIGION_OTHERS']?>"></td></tr></table></div></td>
                            <td class="field_title_short"><span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['FamilyLang']?></td>
                            <td><span class="Edit_Hide"><?= $familyLang_val ?></span><?= $familyLang_DDL_html ?><div id="spanFamilyLang" style='display:none' class='form_field_sub_content'><table><tr><td><em><?=$i_staffAttendance_PleaseSpecify?></em></td><td><input type="text" name="familyLang_others" id="familyLang_others" value="<?=$result[0]['FAMILY_LANG_OTHERS']?>"></td></tr></table></div></td>                       
                            <td class="field_title_short"><span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['PayType']?></td>
                            <td><span class="Edit_Hide"><?= $payType_val ?></span><?= $payType_DDL_html ?></td>
                        </tr>
                        <tr>
                            <td class="field_title_short"><?= $Lang['StudentRegistry']['HomePhone'] ?></td>
                            <td><input class="textboxnum" type="text" name="HomePhone" value="<?= $result[0]['HomeTelNo']?>" readonly="readonly" style="border: 0px"/></td>
                            <td class="field_title_short"><span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['Email'] ?></td>
                            <td><input class="textboxtext" type="text" name="Email" value="<?= $result[0]['EMAIL']?>" readonly="readonly" style="border: 0px"/></td>
                        	<td colspan="2"></td>
                        </tr>
                        <tr>
                            <td class="field_title_short"><span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['HousingState']?></td>
                            <td colspan="5"><span class="Edit_Hide"><?= $HouseState_val ?></span><?= $HouseState_DDL_html ?><div id="spanLodging" style='display:none' class='form_field_sub_content'><table><tr><td><em><?=$i_staffAttendance_PleaseSpecify?></em></td><td><input type="text" name="lodging_others" id="lodging_others" value="<?=$result[0]['LODGING_OTHERS']?>"></td></tr></table></div></td>
                        </tr>
                        <tr>
                            <td class="field_title_short"><?= $Lang['StudentRegistry']['HomeAddress'] ?></td>
                        	<td colspan="5">
                        		<div class="form_field_sub_content">
                        			<table>
                        				<tr>
                  	  	    				<td><em>(<?= $Lang['StudentRegistry']['Door'] ?>)</em></td>
                  		      				<td><span class="row_content"><input class="textboxnum" type="text" name="Address1" value="<?= $result[0]['ADDRESS1']?>" readonly="readonly" style="border: 0px"/></span></td>
                  	  	    				<td><em>(<?= $Lang['StudentRegistry']['Road'] ?>)</em></td>
                  		      				<td><span class="row_content"><input class="textbox_name" type="text" name="Address2" value="<?= $result[0]['ADDRESS2']?>" readonly="readonly" style="border: 0px"/></span></td>
                     		       		</tr>
                     		       	</table>
                     		       	<table>
                     		       		<tr>
                     		       			<td><em>(<?= $Lang['StudentRegistry']['Garden'] ?>)</em></td>
                        	    			<td><span class="row_content"><input class="textbox_name" type="text" name="Address3" value="<?= $result[0]['ADDRESS3']?>" readonly="readonly" style="border: 0px"/></span></td>
                         	    		</tr>
                         	    	</table>
                         	    	<table>
                         	    		<tr>    
                         	    		    <td><em>(<?= $Lang['StudentRegistry']['Town'] ?>)</em></td>
                        	    			<td><span class="row_content"><input class="textbox_name" type="text" name="Address4" value="<?= $result[0]['ADDRESS4']?>" readonly="readonly" style="border: 0px"/></span></td>
                        	    			<td><em>(<?= $Lang['StudentRegistry']['State'] ?>)</em></td>
                        	    			<td><span class="row_content"><input class="textbox_name" type="text" name="Address5" value="<?= $result[0]['ADDRESS5']?>" readonly="readonly" style="border: 0px"/></span></td>
                         	    		</tr>
                        	    	</table>
                        	    	<table>
                     		       		<tr>
                     		       			<td><em>(<?= $Lang['StudentRegistry']['Postal'] ?>)</em></td>
                        	    			<td><span class="row_content"><input class="textboxnum" type="text" name="Address6" value="<?= $result[0]['ADDRESS6']?>" readonly="readonly" style="border: 0px"/></span></td>
                         	    		</tr>
                         	    	</table>
                        		</div>
                        	</td>
                        </tr>
                        <tr>
                        	<td class="field_title_short"><?= $Lang['StudentRegistry']['StudyAt'] ?></td>
                        	<td colspan="5">
                        		<div class="form_field_sub_content">
                     		       	<table>
                     		       		<tr>
                     		       			<td><em>(<?= $Lang['StudentRegistry']['Grade'] ?>)</em></td>
                     		       			<td><span class="row_content"><?= $result[0]['YearName']?></span></td>
                     		       			<td><em>(<?= $Lang['StudentRegistry']['Class'] ?>)</em></td>
                     		       			<td><span class="row_content"><?= $result[0]['ClassName']?></span></td>
                     		       			<td><em>(<?= $Lang['StudentRegistry']['InClassNumber'] ?>)</em></td>
                        	    			<td><span class="row_content"><?= $result[0]['ClassNumber']?></span></td>
                        	    		</tr>
                        	    	</table>
                        	    	<table>
                        	    			<td><em>(<span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['EntryDate'] ?>)</em></td>
                  		      				<td colspan="7"><span class="row_content"><input class="textboxnum" type="text" name="EntryDate" value="<?=$result[0]['ADMISSION_DATE']? date("Y-m-d", strtotime($result[0]['ADMISSION_DATE'])) : ""?>" readonly="readonly" style="border: 0px"/><span class="Edit_Show" style="display: none"> (yyyy-mm-dd) </span></span></td>
                        	    	</table>
								</div>
                            </td>
                        </tr>
                        <tr>
                        	<td class="field_title_short"><?= $Lang['StudentRegistry']['PastEnroll'] ?></td>
                        	<td colspan="5">
                        		<div class="form_field_sub_content">
                        			<table> 
										<?= $Past_Enroll_html ?>
                        			</table>
                        		</div>
                        	</td>
                        </tr>
                        <tr>
                            <td class="field_title_short"><span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['PastPrimarySch'] ?></td>
                            <td colspan="5"><input class="textbox_name" type="text" name="PriSchool" value="<?= $result[0]['PRI_SCHOOL']?>" readonly="readonly" style="border: 0px"/></td>
                        </tr>
                        <tr>
                        	<td class="field_title_short"><?=$Lang['StudentRegistry']['Siblings']?></td>
                        	<td colspan="5">
                        		<table class="Edit_Hide form_field_sub_content">
                        			<?=$Siblings_html?>
                        		</table>
                        		<div class="Edit_Show" style="display:none">
                        			<table cellspacing="0" cellpadding="0" border="0" width="100%">
                        				<col width="40%">
                        				<col width="20%">
                        				<col width="40%">
                        				<tr>
                        					<td colspan="3" style="text-align:right">
                        						<?=$Lang['StudentRegistry']['SearchSiblings']?> : 
                        					    <input class="textboxnum" type="text" name="SearchSiblings" value=""/>
                        						<?= $linterface->GET_SMALL_BTN($Lang['StudentRegistry']['Search'], "button", "javascript:Search_Siblings(this.form)", "SearchBtn") ?>
                        					</td>
                        				</tr>
                        				<tr>
                        					<td>
                        						<select size="4" style="width:100%" id="Siblings" name="Siblings[]" multiple>
                        							<?=$Siblings_option_html?>
                        						</select>
                        						<input type="hidden" id="SiblingsStr" name="SiblingsStr" value="">
                        					</td>
                        					<td valign="center" style="text-align:center">
                        						<?= $linterface->GET_SMALL_BTN("<< ".$Lang['StudentRegistry']['Add'], "button", "javascript:checkOptionTransfer(this.form.elements['SearchResult[]'],this.form.elements['Siblings[]']);return false;", "SearchAddBtn") ?>
                        						<br><br>
                        						<?= $linterface->GET_SMALL_BTN($Lang['StudentRegistry']['Remove']." >>", "button", "javascript:checkOptionTransfer(this.form.elements['Siblings[]'],this.form.elements['SearchResult[]']);return false;", "SearchRemoveBtn") ?>                        						
                        					</td>
                        					<td>
                        						<select size="4" style="width:100%" id="SearchResult" name="SearchResult[]" multiple>
                        							<option value=""><?=$Lang['StudentRegistry']['NoRecord']?></option>
                        						</select>
                        					</td>
                        				</tr>
                        			</table> 
                        		</div>
                        	</td>
                        </tr>
                        <tr>
                        	<td class="field_title_short"><?= $Lang['StudentRegistry']['RegistryStatus'] ?></td>
                        	<td colspan="5">
                        		<?=$ownRegistryStatus?>
                        	</td>
                        </tr>
                        <tr>
							<td class="field_title_short" style="visibility: hidden">&nbsp;</td>
							<td style="visibility: hidden">&nbsp;</td>
							<td class="field_title_short" style="visibility: hidden">&nbsp;</td>
							<td style="visibility: hidden">&nbsp;</td>
							<td class="field_title_short" style="visibility: hidden">&nbsp;</td>
							<td style="visibility: hidden">&nbsp;</td>
						</tr>
					</tbody>
					<col class="field_title_short">
                    <col class="field_c">
				</table>
    			<div class="form_sub_title_v30">
    				<em>- <span class="field_title"><?= $Lang['StudentRegistry']['FatherInfo'] ?></span> -</em>
					<a id="Fath_Hide_Btn" href="javascript:void(0)" onclick="javascript:Hide_Table('FatherInfo_Table', 'Fath_Hide_Btn', 'Fath_Show_Btn');"><?= $Lang['StudentRegistry']['Hide'] ?></a>
                    <a id="Fath_Show_Btn" href="javascript:void(0)" onclick="javascript:Show_Table('FatherInfo_Table', 'Fath_Show_Btn', 'Fath_Hide_Btn');" style="display:none"><?= $Lang['StudentRegistry']['Show'] ?></a>
					<p class="spacer"></p>
				</div>
        		<table id="FatherInfo_Table" class="form_table_v30">
                	<col class="field_title_short">
                	<col class="field_content_short">
                	<col class="field_title_short">
                	<col class="field_content_short">
                	<col class="field_title_short">
                	<col class="field_content_short">
					<tbody>
						<tr>
                            <td class="field_title_short"><span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['ChineseName'] ?></td>
                            <td><input class="textboxtext" type="text" name="FatherChiName" value="<?= $Father[0]['NAME_C']?>" readonly="readonly" style="border: 0px"/></td>
                            <td class="field_title_short"><span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['EnglishName'] ?></td>
                            <td><input class="textboxtext" type="text" name="FatherEngName" value="<?= $Father[0]['NAME_E']?>" readonly="readonly" style="border: 0px"/></td>
							<td colspan="2">&nbsp;</td>
                        </tr>
                        <tr>
                        	<td class="field_title_short"><?= $Lang['StudentRegistry']['Job'] ?></td>
                        	<td><input class="textboxtext" type="text" name="FatherJobName" value="<?= $Father[0]['PROF']?>" readonly="readonly" style="border: 0px"/></td>
                        	<td class="field_title_short"><span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['JobNature'] ?></td>
                        	<td><span class="row_content Edit_Hide"><?= $FatherJobNature_val ?></span><?= $FatherJobNature_DDL_html ?><div id="span_F_JobNature" style='display:none' class='form_field_sub_content'><table><tr><td><em><?=$i_staffAttendance_PleaseSpecify?></em></td><td><input type="text" name="f_jobNature_others" id="f_jobNature_others" value="<?=$Father[0]['JOB_NATURE_OTHERS']?>"></td></tr></table></div></td>
                        	<td class="field_title_short"><span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['JobTitle'] ?></td>
                        	<td><span class="row_content Edit_Hide"><?= $FatherJobTitle_val ?></span><?= $FatherJobTitle_DDL_html ?><div id="span_F_JobTitle" style='display:none' class='form_field_sub_content'><table><tr><td><em><?=$i_staffAttendance_PleaseSpecify?></em></td><td><input type="text" name="f_jobTitle_others" id="f_jobTitle_others" value="<?=$Father[0]['JOB_TITLE_OTHERS']?>"></td></tr></table></div></td>
						</tr>
						<tr>
						    <td class="field_title_short"><span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['MaritalStatus'] ?></td>
                         	<td><span class="row_content Edit_Hide"><?= $FatherMarry_val ?></span><?= $FatherMarry_DDL_html ?></td>
                         	<td class="field_title_short"><span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['EducationLevel'] ?></td>
                        	<td><span class="row_content Edit_Hide"><?= $FatherEduLevel_val ?></span><?= $FatherEduLevel_DDL_html ?><div id="span_F_EduLevel" style='display:none' class='form_field_sub_content'><table><tr><td><em><?=$i_staffAttendance_PleaseSpecify?></em></td><td><input type="text" name="f_eduLevel_others" id="f_eduLevel_others" value="<?=$Father[0]['EDU_LEVEL_OTHERS']?>"></td></tr></table></div></td>
							<td colspan="2">&nbsp;</td>
						</tr>
                        <tr>
                        	<td class="field_title_short"><span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['HomePhone'] ?></td>
                        	<td><input class="textboxnum" type="text" name="FatherHomePhone" value="<?= $Father[0]['TEL']?>" readonly="readonly" style="border: 0px"/></td>
                        	<td class="field_title_short"><span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['CellPhone'] ?></td>
                         	<td><input class="textboxnum" type="text" name="FatherCellPhone" value="<?= $Father[0]['MOBILE']?>" readonly="readonly" style="border: 0px"/></td>
                        	<td class="field_title_short"><span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['OfficeNumber'] ?></td>
                         	<td><input class="textboxnum" type="text" name="FatherOfficePhone" value="<?= $Father[0]['OFFICE_TEL']?>" readonly="readonly" style="border: 0px"/></td>						
						</tr>
						<tr>
                            <td class="field_title_short"><span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['ContactAddress'] ?></td>
                        	<td colspan="5">
                        		<div class="form_field_sub_content">
                        			<table>
                        				<tr>
                  	  	    				<td><em>(<?= $Lang['StudentRegistry']['Door'] ?>)</em></td>
                  		      				<td><span class="row_content"><input class="textboxnum" type="text" name="FatherAddr1" value="<?= $Father[0]['POSTAL_ADDR1']?>" readonly="readonly" style="border: 0px"/></span></td>
                  	  	    				<td><em>(<?= $Lang['StudentRegistry']['Road'] ?>)</em></td>
                  		      				<td><span class="row_content"><input class="textbox_name" type="text" name="FatherAddr2" value="<?= $Father[0]['POSTAL_ADDR2']?>" readonly="readonly" style="border: 0px"/></span></td>
                     		       		</tr>
                     		       	</table>
                     		       	<table>
                     		       		<tr>
                     		       			<td><em>(<?= $Lang['StudentRegistry']['Garden'] ?>)</em></td>
                        	    			<td><span class="row_content"><input class="textbox_name" type="text" name="FatherAddr3" value="<?= $Father[0]['POSTAL_ADDR3']?>" readonly="readonly" style="border: 0px"/></span></td>
                         	    		</tr>
                         	    	</table>
                         	    	<table>
                         	    		<tr>    
                         	    		    <td><em>(<?= $Lang['StudentRegistry']['Town'] ?>)</em></td>
                        	    			<td><span class="row_content"><input class="textbox_name" type="text" name="FatherAddr4" value="<?= $Father[0]['POSTAL_ADDR4']?>" readonly="readonly" style="border: 0px"/></span></td>
                        	    			<td><em>(<?= $Lang['StudentRegistry']['State'] ?>)</em></td>
                        	    			<td><span class="row_content"><input class="textbox_name" type="text" name="FatherAddr5" value="<?= $Father[0]['POSTAL_ADDR5']?>" readonly="readonly" style="border: 0px"/></span></td>
                         	    		</tr>
                        	    	</table>
                        	    	<table>
                     		       		<tr>
                     		       			<td><em>(<?= $Lang['StudentRegistry']['Postal'] ?>)</em></td>
                        	    			<td><span class="row_content"><input class="textboxnum" type="text" name="FatherAddr6" value="<?= $Father[0]['POSTAL_ADDR6']?>" readonly="readonly" style="border: 0px"/></span></td>
                         	    		</tr>
                         	    	</table>
                        		</div>
                        	</td>
                        </tr>
						<tr>
							<td class="field_title_short"><span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['Postal'] ?></td>
							<td><input class="textboxtext" type="text" name="FatherPostCode" value="<?= $Father[0]['POST_CODE']?>" readonly="readonly" style="border: 0px"/></td>
							<td class="field_title_short"><?= $Lang['StudentRegistry']['Email'] ?></td>
							<td><input class="textboxtext" type="text" name="FatherEmail" value="<?= $Father[0]['EMAIL']?>" readonly="readonly" style="border: 0px"/></td>
							<td colspan="2">&nbsp;</td>
						</tr>
						<tr>
							<td class="field_title_short" style="visibility: hidden">&nbsp;</td>
							<td style="visibility: hidden">&nbsp;</td>
							<td class="field_title_short" style="visibility: hidden">&nbsp;</td>
							<td style="visibility: hidden">&nbsp;</td>
							<td class="field_title_short" style="visibility: hidden">&nbsp;</td>
							<td style="visibility: hidden">&nbsp;</td>
						</tr>
					</tbody>
					<col class="field_title_short">
                    <col class="field_c">
				</table>
    			<div class="form_sub_title_v30">
    				<em>- <span class="field_title"><?= $Lang['StudentRegistry']['MotherInfo'] ?></span> -</em>
					<a id="Moth_Hide_Btn" href="javascript:void(0)" onclick="javascript:Hide_Table('MotherInfo_Table', 'Moth_Hide_Btn', 'Moth_Show_Btn');"><?= $Lang['StudentRegistry']['Hide'] ?></a>
                    <a id="Moth_Show_Btn" href="javascript:void(0)" onclick="javascript:Show_Table('MotherInfo_Table', 'Moth_Show_Btn', 'Moth_Hide_Btn');" style="display:none"><?= $Lang['StudentRegistry']['Show'] ?></a>
					<p class="spacer"></p>
				</div>
        		<table id="MotherInfo_Table" class="form_table_v30">
                	<col class="field_title_short">
                	<col class="field_content_short">
                	<col class="field_title_short">
                	<col class="field_content_short">
                	<col class="field_title_short">
                	<col class="field_content_short">
					<tbody>
						<tr>
                            <td class="field_title_short"><span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['ChineseName'] ?></td>
                            <td><input class="textboxtext" type="text" name="MotherChiName" value="<?= $Mother[0]['NAME_C']?>" readonly="readonly" style="border: 0px"/></td>
                            <td class="field_title_short"><span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['EnglishName'] ?></td>
                            <td><input class="textboxtext" type="text" name="MotherEngName" value="<?= $Mother[0]['NAME_E']?>" readonly="readonly" style="border: 0px"/></td>
							<td colspan="2">&nbsp;</td>
                        </tr>
                        <tr>
                        	<td class="field_title_short"><?= $Lang['StudentRegistry']['Job'] ?></td>
                        	<td><input class="textboxtext" type="text" name="MotherJobName" value="<?= $Mother[0]['PROF']?>" readonly="readonly" style="border: 0px"/></td>
                        	<td class="field_title_short"><span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['JobNature'] ?></td>
                        	<td><span class="row_content Edit_Hide"><?= $MotherJobNature_val ?></span><?= $MotherJobNature_DDL_html ?><div id="span_M_JobNature" style='display:none' class='form_field_sub_content'><table><tr><td><em><?=$i_staffAttendance_PleaseSpecify?></em></td><td><input type="text" name="m_jobNature_others" id="m_jobNature_others" value="<?=$Mother[0]['JOB_NATURE_OTHERS']?>"></td></tr></table></div></td>
                        	<td class="field_title_short"><span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['JobTitle'] ?></td>
                        	<td><span class="row_content Edit_Hide"><?= $MotherJobTitle_val ?></span><?= $MotherJobTitle_DDL_html ?><div id="span_M_JobTitle" style='display:none' class='form_field_sub_content'><table><tr><td><em><?=$i_staffAttendance_PleaseSpecify?></em></td><td><input type="text" name="m_jobTitle_others" id="m_jobTitle_others" value="<?=$Mother[0]['JOB_TITLE_OTHERS']?>"></td></tr></table></div></td>
						</tr>
						<tr>
						    <td class="field_title_short"><span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['MaritalStatus'] ?></td>
                         	<td><span class="row_content Edit_Hide"><?= $MotherMarry_val ?></span><?= $MotherMarry_DDL_html ?></td>
                         	<td class="field_title_short"><span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['EducationLevel'] ?></td>
                        	<td><span class="row_content Edit_Hide"><?= $MotherEduLevel_val ?></span><?= $MotherEduLevel_DDL_html ?><div id="span_M_EduLevel" style='display:none' class='form_field_sub_content'><table><tr><td><em><?=$i_staffAttendance_PleaseSpecify?></em></td><td><input type="text" name="m_eduLevel_others" id="m_eduLevel_others" value="<?=$Mother[0]['EDU_LEVEL_OTHERS']?>"></td></tr></table></div></td>
							<td colspan="2">&nbsp;</td>
						</tr>
                        <tr>
                        	<td class="field_title_short"><span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['HomePhone'] ?></td>
                        	<td><input class="textboxnum" type="text" name="MotherHomePhone" value="<?= $Mother[0]['TEL']?>" readonly="readonly" style="border: 0px"/></td>
                        	<td class="field_title_short"><span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['CellPhone'] ?></td>
                         	<td><input class="textboxnum" type="text" name="MotherCellPhone" value="<?= $Mother[0]['MOBILE']?>" readonly="readonly" style="border: 0px"/></td>
                        	<td class="field_title_short"><span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['OfficeNumber'] ?></td>
                         	<td><input class="textboxnum" type="text" name="MotherOfficePhone" value="<?= $Mother[0]['OFFICE_TEL']?>" readonly="readonly" style="border: 0px"/></td>						
						</tr>
						<tr>
                            <td class="field_title_short"><span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['ContactAddress'] ?></td>
                        	<td colspan="5">
                        		<div class="form_field_sub_content">
                        			<table>
                        				<tr>
                  	  	    				<td><em>(<?= $Lang['StudentRegistry']['Door'] ?>)</em></td>
                  		      				<td><span class="row_content"><input class="textboxnum" type="text" name="MotherAddr1" value="<?= $Mother[0]['POSTAL_ADDR1']?>" readonly="readonly" style="border: 0px"/></span></td>
                  	  	    				<td><em>(<?= $Lang['StudentRegistry']['Road'] ?>)</em></td>
                  		      				<td><span class="row_content"><input class="textbox_name" type="text" name="MotherAddr2" value="<?= $Mother[0]['POSTAL_ADDR2']?>" readonly="readonly" style="border: 0px"/></span></td>
                     		       		</tr>
                     		       	</table>
                     		       	<table>
                     		       		<tr>
                     		       			<td><em>(<?= $Lang['StudentRegistry']['Garden'] ?>)</em></td>
                        	    			<td><span class="row_content"><input class="textbox_name" type="text" name="MotherAddr3" value="<?= $Mother[0]['POSTAL_ADDR3']?>" readonly="readonly" style="border: 0px"/></span></td>
                         	    		</tr>
                         	    	</table>
                         	    	<table>
                         	    		<tr>    
                         	    		    <td><em>(<?= $Lang['StudentRegistry']['Town'] ?>)</em></td>
                        	    			<td><span class="row_content"><input class="textbox_name" type="text" name="MotherAddr4" value="<?= $Mother[0]['POSTAL_ADDR4']?>" readonly="readonly" style="border: 0px"/></span></td>
                        	    			<td><em>(<?= $Lang['StudentRegistry']['State'] ?>)</em></td>
                        	    			<td><span class="row_content"><input class="textbox_name" type="text" name="MotherAddr5" value="<?= $Mother[0]['POSTAL_ADDR5']?>" readonly="readonly" style="border: 0px"/></span></td>
                         	    		</tr>
                        	    	</table>
                        	    	<table>
                     		       		<tr>
                     		       			<td><em>(<?= $Lang['StudentRegistry']['Postal'] ?>)</em></td>
                        	    			<td><span class="row_content"><input class="textboxnum" type="text" name="MotherAddr6" value="<?= $Mother[0]['POSTAL_ADDR6']?>" readonly="readonly" style="border: 0px"/></span></td>
                         	    		</tr>
                         	    	</table>
                        		</div>
                        	</td>
                        </tr>
						<tr>
							<td class="field_title_short"><span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['Postal'] ?></td>
							<td><input class="textboxtext" type="text" name="MotherPostCode" value="<?= $Mother[0]['POST_CODE']?>" readonly="readonly" style="border: 0px"/></td>
							<td class="field_title_short"><?= $Lang['StudentRegistry']['Email'] ?></td>
							<td><input class="textboxtext" type="text" name="MotherEmail" value="<?= $Mother[0]['EMAIL']?>" readonly="readonly" style="border: 0px"/></td>
							<td colspan="2">&nbsp;</td>
						</tr>
						<tr>
							<td class="field_title_short" style="visibility: hidden">&nbsp;</td>
							<td style="visibility: hidden">&nbsp;</td>
							<td class="field_title_short" style="visibility: hidden">&nbsp;</td>
							<td style="visibility: hidden">&nbsp;</td>
							<td class="field_title_short" style="visibility: hidden">&nbsp;</td>
							<td style="visibility: hidden">&nbsp;</td>
						</tr>
					</tbody>
					<col class="field_title_short">
                    <col class="field_c">
				</table>
				<div class="form_sub_title_v30">
    				<em>- <span class="field_title"><?= $Lang['StudentRegistry']['GuardianInfo'] ?></span> -</em>
					<a id="Guard_Hide_Btn" href="javascript:void(0)" onclick="javascript:Hide_Table('GuardianInfo_Table', 'Guard_Hide_Btn', 'Guard_Show_Btn');"><?= $Lang['StudentRegistry']['Hide'] ?></a>
                    <a id="Guard_Show_Btn" href="javascript:void(0)" onclick="javascript:Show_Table('GuardianInfo_Table', 'Guard_Show_Btn', 'Guard_Hide_Btn');" style="display:none"><?= $Lang['StudentRegistry']['Show'] ?></a>					
					<p class="spacer"></p>
				</div>
        		<table id="GuardianInfo_Table" class="form_table_v30">
                	<col class="field_title_short">
                	<col class="field_content_short">
                	<col class="field_title_short">
                	<col class="field_content_short">
                	<col class="field_title_short">
                	<col class="field_content_short">
					<tbody>
						<tr id="GuardNoInfo" <?= $Guard? "style=\"Display:none\"" : ""?>>
							<td colspan="6"><?=$Lang['StudentRegistry']['NoInformation']?></td>
						</tr>
						<tr class="GuardRelationInfo" <?= $Guard? "" : "style=\"Display:none\""?>>
							<td class="field_title_short"><span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['Relationship'] ?></td>
							<td><span class="row_content Edit_Hide"><?= $GuardRelation_val ?></span><?= $GuardRelation_RBL_html ?></td>
							<td class="GuardInfo field_title_short" <?= $Guard[0]['GUARD'] == 'O'? "" : "style=\"Display:none\""?>><span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['RelationOther'] ?></td>
                        	<td class="GuardInfo" colspan="3"><input class="textboxtext" type="text" name="GuardRelationOther" value="<?=$Guard[0]['G_RELATION']?>" readonly="readonly" style="border: 0px"/></td>
						</tr>
						<tr class="GuardInfo" <?= $Guard[0]['GUARD'] == 'O'? "" : "style=\"Display:none\""?>>
                            <td class="field_title_short"><span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['ChineseName'] ?></td>
                            <td><input class="textboxtext" type="text" name="GuardChiName" value="<?=$Guard[0]['NAME_C']?>" readonly="readonly" style="border: 0px"/></td>
                            <td class="field_title_short"><span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['EnglishName'] ?></td>
                            <td><input class="textboxtext" type="text" name="GuardEngName" value="<?=$Guard[0]['NAME_E']?>" readonly="readonly" style="border: 0px"/></td>
                            <td colspan="2">&nbsp;</td>
                        </tr>
                        <tr class="GuardInfo" <?= $Guard[0]['GUARD'] == 'O'? "" : "style=\"Display:none\""?>>
                        	<td class="field_title_short"><span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['Gender'] ?></td>
                        	<td><span class="row_content Edit_Hide"><?= $GuardGender_val ?></span><?= $GuardGender_RBL_html ?></td>
                        	<td class="field_title_short"><?= $Lang['StudentRegistry']['StayTogether'] ?></td>
                         	<td colspan="3"><span class="row_content Edit_Hide"><?= $GuardStay_val ?></span><?= $GuardStay_RBL_html ?></td>
                        </tr>
                        <!--
                        <tr class="GuardInfo" <?= $Guard[0]['GUARD'] == 'O'? "" : "style=\"Display:none\""?>>
                        	<td class="field_title_short"><span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['RelationOther'] ?></td>
                        	<td><input class="textboxtext" type="text" name="GuardRelationOther" value="<?=$Guard[0]['G_RELATION']?>" readonly="readonly" style="border: 0px"/></td>                   	
                        	<td class="field_title_short"><?= $Lang['StudentRegistry']['StayTogether'] ?></td>
                         	<td><span class="row_content Edit_Hide"><?= $GuardStay_val ?></span><?= $GuardStay_RBL_html ?></td>
                        </td>
                        -->
                        <tr class="GuardInfo" <?= $Guard[0]['GUARD'] == 'O'? "" : "style=\"Display:none\""?>>
                        	<td class="field_title_short"><?= $Lang['StudentRegistry']['Job'] ?></td>
                        	<td><input class="textboxtext" type="text" name="GuardJobName" value="<?=$Guard[0]['PROF']?>" readonly="readonly" style="border: 0px"/></td>
                        	<td class="field_title_short"><span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['JobNature'] ?></td>
                        	<td><span class="row_content Edit_Hide"><?= $GuardJobNature_val ?></span><?= $GuardJobNature_DDL_html ?><div id="span_G_JobNature" style='display:none' class='form_field_sub_content'><table><tr><td><em><?=$i_staffAttendance_PleaseSpecify?></em></td><td><input type="text" name="g_jobNature_others" id="g_jobNature_others" value="<?=$Guard[0]['JOB_NATURE_OTHERS']?>"></td></tr></table></div></td>
                        	<td class="field_title_short"><span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['JobTitle'] ?></td>
                        	<td><span class="row_content Edit_Hide"><?= $GuardJobTitle_val ?></span><?= $GuardJobTitle_DDL_html ?><div id="span_G_JobTitle" style='display:none' class='form_field_sub_content'><table><tr><td><em><?=$i_staffAttendance_PleaseSpecify?></em></td><td><input type="text" name="g_jobTitle_others" id="g_jobTitle_others" value="<?=$Guard[0]['JOB_TITLE_OTHERS']?>"></td></tr></table></div></td>
 						</tr>
 						<tr class="GuardInfo" <?= $Guard[0]['GUARD'] == 'O'? "" : "style=\"Display:none\""?>>
                         	<td class="field_title_short"><span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['EducationLevel'] ?></td>
                        	<td><span class="row_content Edit_Hide"><?= $GuardEduLevel_val ?></span><?= $GuardEduLevel_DDL_html ?><div id="span_G_EduLevel" style='display:none' class='form_field_sub_content'><table><tr><td><em><?=$i_staffAttendance_PleaseSpecify?></em></td><td><input type="text" name="g_eduLevel_others" id="g_eduLevel_others" value="<?=$Guard[0]['EDU_LEVEL_OTHERS']?>"></td></tr></table></div></td>
							<td colspan="4">&nbsp;</td>
 						</tr>
                        <tr class="GuardInfo" <?= $Guard[0]['GUARD'] == 'O'? "" : "style=\"Display:none\""?>>
                        	<td class="field_title_short"><span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['HomePhone'] ?></td>
                        	<td><input class="textboxnum" type="text" name="GuardHomePhone" value="<?=$Guard[0]['TEL']?>" readonly="readonly" style="border: 0px"/></td>
                        	<td class="field_title_short"><span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['CellPhone'] ?></td>
                         	<td><input class="textboxnum" type="text" name="GuardCellPhone" value="<?=$Guard[0]['MOBILE']?>" readonly="readonly" style="border: 0px"/></td>
						    <td class="field_title_short"><span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['OfficeNumber'] ?></td>
                         	<td><input class="textboxnum" type="text" name="GuardOfficePhone" value="<?=$Guard[0]['OFFICE_TEL']?>" readonly="readonly" style="border: 0px"/></td>
						</tr>
						<tr class="GuardInfo" <?= $Guard[0]['GUARD'] == 'O'? "" : "style=\"Display:none\""?>>
                            <td class="field_title_short"><span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['ContactAddress'] ?></td>
                        	<td colspan="5">
                        		<div class="form_field_sub_content">
                        			<table>
                        				<tr>
                  	  	    				<td><em>(<?= $Lang['StudentRegistry']['Door'] ?>)</em></td>
                  		      				<td><span class="row_content"><input class="textboxnum" type="text" name="GuardAddr1" value="<?= $Guard[0]['POSTAL_ADDR1']?>" readonly="readonly" style="border: 0px"/></span></td>
                  	  	    				<td><em>(<?= $Lang['StudentRegistry']['Road'] ?>)</em></td>
                  		      				<td><span class="row_content"><input class="textbox_name" type="text" name="GuardAddr2" value="<?= $Guard[0]['POSTAL_ADDR2']?>" readonly="readonly" style="border: 0px"/></span></td>
                     		       		</tr>
                     		       	</table>
                     		       	<table>
                     		       		<tr>
                     		       			<td><em>(<?= $Lang['StudentRegistry']['Garden'] ?>)</em></td>
                        	    			<td><span class="row_content"><input class="textbox_name" type="text" name="GuardAddr3" value="<?= $Guard[0]['POSTAL_ADDR3']?>" readonly="readonly" style="border: 0px"/></span></td>
                         	    		</tr>
                         	    	</table>
                         	    	<table>
                         	    		<tr>    
                         	    		    <td><em>(<?= $Lang['StudentRegistry']['Town'] ?>)</em></td>
                        	    			<td><span class="row_content"><input class="textbox_name" type="text" name="GuardAddr4" value="<?= $Guard[0]['POSTAL_ADDR4']?>" readonly="readonly" style="border: 0px"/></span></td>
                        	    			<td><em>(<?= $Lang['StudentRegistry']['State'] ?>)</em></td>
                        	    			<td><span class="row_content"><input class="textbox_name" type="text" name="GuardAddr5" value="<?= $Guard[0]['POSTAL_ADDR5']?>" readonly="readonly" style="border: 0px"/></span></td>
                         	    		</tr>
                        	    	</table>
                        	    	<table>
                     		       		<tr>
                     		       			<td><em>(<?= $Lang['StudentRegistry']['Postal'] ?>)</em></td>
                        	    			<td><span class="row_content"><input class="textboxnum" type="text" name="GuardAddr6" value="<?= $Guard[0]['POSTAL_ADDR6']?>" readonly="readonly" style="border: 0px"/></span></td>
                         	    		</tr>
                         	    	</table>
                        		</div>
                        	</td>
                        </tr>
						<tr class="GuardInfo" <?= $Guard[0]['GUARD'] == 'O'? "" : "style=\"Display:none\""?>>
							<td class="field_title_short"><span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['Postal'] ?></td>
							<td><input class="textboxtext" type="text" name="GuardPostCode" value="<?= $Guard[0]['POST_CODE']?>" readonly="readonly" style="border: 0px"/></td>
							<td class="field_title_short"><?= $Lang['StudentRegistry']['Email'] ?></td>
							<td><input class="textboxtext" type="text" name="GuardEmail" value="<?=$Guard[0]['EMAIL']?>" readonly="readonly" style="border: 0px"/></td>
							<td colspan="4">&nbsp;</td>
						</tr>
						<tr>
							<td class="field_title_short" style="visibility: hidden">&nbsp;</td>
							<td style="visibility: hidden">&nbsp;</td>
							<td class="field_title_short" style="visibility: hidden">&nbsp;</td>
							<td style="visibility: hidden">&nbsp;</td>
							<td class="field_title_short" style="visibility: hidden">&nbsp;</td>
							<td style="visibility: hidden">&nbsp;</td>
						</tr>
					</tbody>
					<col class="field_title_short">
                    <col class="field_c">
				</table>
				<span id="FormReminder" class="tabletextremark" style="display: none"><?=$i_general_required_field?></span>
				<div id="ErrorLog"></div>
				<div class="edit_bottom">
					<span id="LastModified" class="row_content tabletextremark"><?= $Lang['StudentRegistry']['LastUpdated'] ?> : <?= $result[0]['DateModified']? $result[0]['DateModified']."(".$ModifiedBy.")" : "--"?></span>
                    <p class="spacer"></p>
					<!--
      			    <input id="SaveBtn" type="button" value="<?= $Lang['StudentRegistry']['Save'] ?>" class="formbutton" name="submit" style="display: none" onclick="javascript:show_arg()">
                    <input id="CancelBtn" type="button" value="<?= $Lang['StudentRegistry']['Cancel'] ?>" onclick="window.location.reload(true)" class="formbutton" name="submit2" style="display: none">
					-->
					<?=$linterface->GET_ACTION_BTN($button_submit, "button", "checksubmit(this.form)", "SaveBtn")?>&nbsp;
					<?=$linterface->GET_ACTION_BTN($button_cancel, "button", "ResetInfo()", "CancelBtn")?>
				</div>
					<p class="spacer"></p>
			</div>
		</td>
	</tr>
</table>
<input type="hidden" name="userID" id="userID" value="<?=$userID?>">
<input type="hidden" name="AcademicYearID" id="AcademicYearID" value="<?=$AcademicYearID?>">
</form>
<script language="javascript">
	document.getElementById('SaveBtn').style.visibility = "hidden";
	document.getElementById('CancelBtn').style.visibility = "hidden";
	<?
	if(sizeof($error)>0)
		echo "EditInfo();";
		
	?>
</script>
<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>