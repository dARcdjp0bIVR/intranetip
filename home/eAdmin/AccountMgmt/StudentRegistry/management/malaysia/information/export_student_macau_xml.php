<?php
# using: 
$PATH_WRT_ROOT = "../../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccessright.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$laccessright = new libaccessright();

if(!$plugin['AccountMgmt_StudentRegistry'] || (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-View") && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-Manage"))) {
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}
 
# Temp Assign memory of this page
ini_set("memory_limit", "150M"); 

$lsr = new libstudentregistry();

$ExportArr = array();

//$AcademicYearID = Get_Current_Academic_Year_ID();

# condition
$conds = "";

if($searchText != "")
	$conds .= " AND (".Get_Lang_Selection('yc.ClassTitleB5','yc.ClassTitleEN')." LIKE '%$searchText%' OR USR.EnglishName LIKE '%$searchText%' OR USR.ChineseName LIKE '%$searchText%' OR USR.WebSAMSRegNo LIKE '%$searchText%' OR srs.CODE LIKE '%$searchText%')";

if($targetClass!="") {
	$studentAry = $lsr->storeStudent('0', $targetClass);
	$conds .= " AND ycu.UserID IN (".implode(',', $studentAry).")";
}

if($registryStatus!="")
	$conds .= " AND USR.RecordStatus=$registryStatus";
else 
	$conds .= " AND USR.RecordStatus IN (0,1,2)";


$filename = "student_registry_class.xml";	


# SQL Statement
$sql = "SELECT 
			srs.STUD_ID,
			srs.CODE,
			USR.ChineseName,
			USR.EnglishName,
			USR.Gender,
			IF(USR.DateOfBirth='0000-00-00 00:00:00','',LEFT(USR.DateOfBirth,10)),
			srs.B_PLACE,
			srs.ID_TYPE,
			srs.ID_NO,
			srs.I_PLACE,
			srs.I_DATE,
			srs.V_DATE,
			srs.S6_TYPE,
			srs.S6_IDATE,
			srs.S6_VDATE,
			srs.NATION,
			srs.ORIGIN,
			IFNULL(USR.HomeTelNo, (IFNULL(MobileTelNo, '---'))) TEL,
			srs.R_AREA,
			srs.RA_DESC,
			srs.AREA,
			srs.ROAD,
			srs.ADDRESS,
			srs.S_CODE,
			y.YearName as GRADE,
			".Get_Lang_Selection('yc.ClassTitleB5','yc.ClassTitleEN')." as CLASS,
			ycu.ClassNumber,
			USR.UserID
		FROM
			INTRANET_USER USR LEFT OUTER JOIN 
			STUDENT_REGISTRY_STUDENT srs ON (srs.UserID=USR.UserID) LEFT OUTER JOIN
			YEAR_CLASS_USER ycu ON (ycu.UserID=USR.UserID) LEFT OUTER JOIN
			YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID) LEFT OUTER JOIN 
			YEAR y ON (y.YearID=yc.YearID)
		WHERE 
			yc.AcademicYearID='$AcademicYearID' AND
			USR.RecordType=2 
			$conds 
		ORDER BY 
			y.Sequence, yc.Sequence, ycu.ClassNumber
		";	
		
$row = $lsr->returnArray($sql,28);

$data = "<?xml version=\"1.0\" encoding=\"BIG5\"?>\r\n";
$data .= "<!DOCTYPE SRA SYSTEM \"http://app.dsej.gov.mo/prog/edu/sra/sra3.dtd\">\r\n";
$data .= "\r\n<SRA>\r\n";

# Create data array for export
for($i=0; $i<sizeof($row); $i++){
	list($STUD_ID, $CODE, $NAME_C, $NAME_P, $SEX, $B_DATE, $B_PLACE, $ID_TYPE, $ID_NO, $I_PLACE, $I_DATE, $V_DATE, $S6_TYPE, $S6_IDATE, $S6_VDATE, $NATION, $ORIGIN, $TEL, $R_AREA, $RA_DESC, $AREA, $ROAD, $ADDRESS, $s_CODE, $GRADE, $CLASS, $C_NO, $userid) = $row[$i];
	
	$father = $lsr->getParentGuardianInfo($userid, 'F');
	$mother = $lsr->getParentGuardianInfo($userid, 'M');
	$guardian = $lsr->getParentGuardianInfo($userid, 'G');
	$emergency = $lsr->getParentGuardianInfo($userid, 'E');
			
	$data .= "<STUDENT>\r\n";
	$data .= "<STUD_ID>".iconv("UTF-8","big5",$STUD_ID)."</STUD_ID>\r\n";		//student id
	$data .= "<CODE>".iconv("UTF-8","big5",$CODE)."</CODE>\r\n";			//code
	$data .= "<NAME_C>".iconv("UTF-8","big5",$NAME_C)."</NAME_C>\r\n";  		//name in chinese
	$data .= "<NAME_P>".iconv("UTF-8","big5",$NAME_P)."</NAME_P>\r\n";  		//name in p
	$data .= "<SEX>".iconv("UTF-8","big5",$SEX)."</SEX>\r\n";  			//sex
	$data .= "<B_DATE>".iconv("UTF-8","big5",$B_DATE)."</B_DATE>\r\n";  		//date of birth
	$data .= "<B_PLACE>".iconv("UTF-8","big5",$B_PLACE)."</B_PLACE>\r\n";  		//place of birth
	$data .= "<ID_TYPE>".iconv("UTF-8","big5",$ID_TYPE)."</ID_TYPE>\r\n";  		//id type
	$data .= "<ID_NO>".iconv("UTF-8","big5",$ID_NO)."</ID_NO>\r\n";  		//id number
	$data .= "<I_PLACE>".iconv("UTF-8","big5",$I_PLACE)."</I_PLACE>\r\n";  		//issue place
	$data .= "<I_DATE>".iconv("UTF-8","big5",$I_DATE)."</I_DATE>\r\n";  		//issue date
	$data .= "<V_DATE>".iconv("UTF-8","big5",$V_DATE)."</V_DATE>\r\n";  		//valid date
	$data .= "<S6_TYPE>".iconv("UTF-8","big5",$S6_TYPE)."</S6_TYPE>\r\n";  		
	$data .= "<S6_IDATE>".iconv("UTF-8","big5",$S6_IDATE)."</S6_IDATE>\r\n";  		
	$data .= "<S6_VDATE>".iconv("UTF-8","big5",$S6_VDATE)."</S6_VDATE>\r\n";  		
	$data .= "<NATION>".iconv("UTF-8","big5",$NATION)."</NATION>\r\n";  		//nation
	$data .= "<ORIGIN>".iconv("UTF-8","big5",$ORIGIN)."</ORIGIN>\r\n";  		//origin
	$data .= "<TEL>".iconv("UTF-8","big5",$TEL)."</TEL>\r\n";  		//telephone
	$data .= "<R_AREA>".iconv("UTF-8","big5",$R_AREA)."</R_AREA>\r\n";  		
	$data .= "<RA_DESC>".iconv("UTF-8","big5",$RA_DESC)."</RA_DESC>\r\n";  		
	$data .= "<AREA>".iconv("UTF-8","big5",$AREA)."</AREA>\r\n";  		//area
	$data .= "<ROAD>".iconv("UTF-8","big5",$ROAD)."</ROAD>\r\n";  		//road
	$data .= "<ADDRESS>".iconv("UTF-8","big5",$ADDRESS)."</ADDRESS>\r\n";  	//address
	
	$data .= "<FATHER>".(($father['NAME_C']=="") ? iconv("UTF-8","big5",$father['NAME_E']) : iconv("UTF-8","big5",$father['NAME_C']))."</FATHER>\r\n";				// father name
	$data .= "<MOTHER>".(($mother['NAME_C']=="") ? iconv("UTF-8","big5",$mother['NAME_E']) : iconv("UTF-8","big5",$mother['NAME_C']))."</MOTHER>\r\n";				// mother name
	$data .= "<F_PROF>".iconv("UTF-8","big5",$father['PROF'])."</F_PROF>\r\n";																// father professional
	$data .= "<M_PROF>".iconv("UTF-8","big5",$mother['PROF'])."</M_PROF>\r\n";																// mother professional
	$data .= "<GUARD>".$Lang['StudentRegistry']['Relation'][$guardian['GUARD']]."</GUARD>\r\n";						// guardian relationship
	$data .= "<LIVE_SAME>".iconv("UTF-8","big5",$guardian['LIVE_SAME'])."</LIVE_SAME>\r\n";														// live with guardian

	$data .= "<EC_NAME>".(($emergency['NAME_C']=="") ? iconv("UTF-8","big5",$emergency['NAME_E']) : iconv("UTF-8","big5",$emergency['NAME_C']))."</EC_NAME>\r\n";  	//emergency contact
	$data .= "<EC_REL>".(($emergency['GUARD']=="O") ? iconv("UTF-8","big5",$emergency['G_RELATION']) : $Lang['StudentRegistry']['Relation'][$emergency['GUARD']])."</EC_REL>\r\n"; 	//emergency relationship
	$data .= "<EC_TEL>".(($emergency['TEL']=="") ? iconv("UTF-8","big5",$emergency['MOBILE']) : iconv("UTF-8","big5",$emergency['TEL']))."</EC_TEL>\r\n";  			//emergency telephone
	$data .= "<EC_AREA>".iconv("UTF-8","big5",$emergency['G_AREA'])."</EC_AREA>\r\n";  														//emergency area
	$data .= "<EC_ROAD>".iconv("UTF-8","big5",$emergency['G_ROAD'])."</EC_ROAD>\r\n";  														//emergency road
	$data .= "<EC_ADDRESS>".iconv("UTF-8","big5",$emergency['G_ADDRESS'])."</EC_ADDRESS>\r\n"; 											 			//emergency address
	
	$data .= "<S_CODE>".iconv("UTF-8","big5",$S_CODE)."</S_CODE>\r\n";  		
	$data .= "<GRADE>".iconv("UTF-8","big5",$GRADE)."</GRADE>\r\n";  		//form
	$data .= "<CLASS>".iconv("UTF-8","big5",$CLASS)."</CLASS>\r\n";  		//class name
	$data .= "<C_NO>".iconv("UTF-8","big5",$C_NO)."</C_NO>\r\n";  		//class number
	
	
	$data .= "<G_NAME>".(($guardian['NAME_C']=="") ? iconv("UTF-8","big5",$guardian['NAME_E']) : iconv("UTF-8","big5",$guardian['NAME_C']))."</G_NAME>\r\n";			// guardian name
	$data .= "<G_RELATION>".iconv("UTF-8","big5",$guardian['G_RELATION'])."</G_RELATION>\r\n";			// guardian relationship
	$data .= "<G_PROFESSION>".iconv("UTF-8","big5",$guardian['PROF'])."</G_PROFESSION>\r\n";					// guardian professional
	$data .= "<G_AREA>".iconv("UTF-8","big5",$guardian['G_AREA'])."</G_AREA>\r\n";				// guardian area
	$data .= "<G_ROAD>".iconv("UTF-8","big5",$guardian['G_ROAD'])."</G_ROAD>\r\n";				// guardian road
	$data .= "<G_ADDRESS>".iconv("UTF-8","big5",$guardian['G_ADDRESS'])."</G_ADDRESS>\r\n";			// guardian address
	$data .= "<G_TEL>".iconv("UTF-8","big5",$guardian['TEL'])."</G_TEL>\r\n";					// guardian telephone
	$data .= "<GUARDMOBILE>".iconv("UTF-8","big5",$guardian['MOBILE'])."</GUARDMOBILE>\r\n";				// guardian mobile
	$data .= "</STUDENT>\r\n";
}
$data .= "</SRA>";


intranet_closedb();


header('Pragma: public');
header('Expires: 0');
header('Cache-Control: must-revalidate, post-check=0, pre-check=0');

header('Content-type: application/octet-stream');
header('Content-Length: '.strlen($data));

header('Content-Disposition: attachment; filename="'.$filename.'";');	
header("ContentType:text/xml; pragma:no-cache; Cache-Control:no-cache; charset=BIG5; ");
?>


<?=$data?>