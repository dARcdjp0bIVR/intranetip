<?php
# using:
/*
 *  2019-05-13 Cameron
 *      - fix potential sql injection problem by enclosing var with apostrophe
 */
$PATH_WRT_ROOT = "../../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/libaccessright.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$laccessright = new libaccessright();

if(!$plugin['AccountMgmt_StudentRegistry'] || (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-View") && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-ViewBasic") && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-ViewAdv") && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-Manage") && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-ManageBasic") && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-ManageAdv"))) {
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}
 
# Temp Assign memory of this page
ini_set("memory_limit", "150M"); 

$lsr = new libstudentregistry();
$lexport = new libexporttext();

$ExportArr = array();

//$AcademicYearID = Get_Current_Academic_Year_ID();

# condition
$conds = "";

if($searchText != "")
	$conds .= " AND (".Get_Lang_Selection('yc.ClassTitleB5','yc.ClassTitleEN')." LIKE '%$searchText%' OR USR.EnglishName LIKE '%$searchText%' OR USR.ChineseName LIKE '%$searchText%')";

if($targetClass!="") {
	$studentAry = $lsr->storeStudent('0', $targetClass);
	$conds .= " AND ycu.UserID IN (".implode(',', $studentAry).")";
}

if($registryStatus!="")
	$conds .= " AND USR.RecordStatus='$registryStatus'";
else 
	$conds .= " AND USR.RecordStatus IN (0,1,2)";


$filename = "student_registry_class.csv";	

# SQL Statement
$sql = "SELECT
			".Get_Lang_Selection('yc.ClassTitleB5','yc.ClassTitleEN')." as CLASS,
			ycu.ClassNumber,
			USR.UserLogin,
			srs.STUD_ID,
			USR.ChineseName,
			USR.EnglishName,
			USR.Gender,
			IF(USR.DateOfBirth='0000-00-00 00:00:00','',LEFT(USR.DateOfBirth,10)),
			srs.B_PLACE,
			srs.NATION,
			srs.ANCESTRAL_HOME,
			srs.RACE,
			srs.ID_NO,
			srs.BIRTH_CERT_NO,
			srs.RELIGION,
			srs.FAMILY_LANG,
			srs.PAYMENT_TYPE,
			USR.HomeTelNo,
			srs.EMAIL,
			srs.LODGING,
			srs.ADDRESS1,
			srs.ADDRESS2,
			srs.ADDRESS3,
			srs.ADDRESS4,
			srs.ADDRESS5,
			srs.ADDRESS6,
			IF(srs.ADMISSION_DATE='0000-00-00 00:00:00','',LEFT(srs.ADMISSION_DATE,10)),
			srs.BRO_SIS_IN_SCHOOL,
			srs.PRI_SCHOOL,
			USR.UserID
		FROM
			INTRANET_USER USR LEFT OUTER JOIN 
			STUDENT_REGISTRY_STUDENT srs ON (srs.UserID=USR.UserID) LEFT OUTER JOIN
			YEAR_CLASS_USER ycu ON (ycu.UserID=USR.UserID) LEFT OUTER JOIN
			YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID)
		WHERE 
			yc.AcademicYearID='$AcademicYearID' AND
			USR.RecordType=2 
			$conds 
		ORDER BY 
			yc.Sequence, ycu.ClassNumber
		";	

$row = $lsr->returnArray($sql,29);

# Create data array for export
for($i=0; $i<sizeof($row); $i++){
	//list($STUD_ID, $CODE, $NAME_C, $NAME_P, $SEX, $B_DATE, $B_PLACE, $ID_TYPE, $ID_NO, $I_PLACE, $I_DATE, $V_DATE, $S6_TYPE, $S6_IDATE, $S6_VDATE, $NATION, $ORIGIN, $TEL, $R_AREA, $RA_DESC, $AREA, $ROAD, $ADDRESS, $s_CODE, $GRADE, $CLASS, $C_NO, $userid) = $row[$i];
	list($CLASS, $C_NO, $UserLogin, $STUD_ID, $NAME_C, $NAME_E, $SEX, $B_DATE, $B_PLACE, $NATION, $ANCESTRAL_HOME, $RACE, $ID_NO, $BIRTH_CERT_NO, $RELIGION, $FAMILY_LANG, $PAYMENT_TYPE, $TEL, $EMAIL, $LODGING, $ADDRESS1, $ADDRESS2, $ADDRESS3, $ADDRESS4, $ADDRESS5, $ADDRESS6, $ADMISSION_DATE, $SIBLINGS, $PRI_SCHOOL, $userid) = $row[$i];
	
	#Transfer Siblings' UserID to STUD_ID
	$Siblings_STUD_ID_str = "";
	if($SIBLINGS)
	{
		$Siblings_UserID_ary  = explode(',', $SIBLINGS);
		$Siblings_STUD_ID_ary = array();
		for($j=0;$j<sizeof($Siblings_UserID_ary);$j++)
		{
			$sql = "SELECT STUD_ID FROM STUDENT_REGISTRY_STUDENT WHERE UserID = '".$Siblings_UserID_ary[$j]."'";
			$result = $lsr->returnVector($sql);
			if(!in_array($result[0], $Siblings_STUD_ID_ary))
				$Siblings_STUD_ID_ary[] = $result[0];
		}
		if(sizeof($Siblings_STUD_ID_ary)!=0)
		{
			for($k=0;$k<sizeof($Siblings_STUD_ID_ary);$k++)
			{
				$Siblings_STUD_ID_str .= $Siblings_STUD_ID_ary[$k];
				if($k!=sizeof($Siblings_STUD_ID_ary)-1)
					$Siblings_STUD_ID_str .= ",";
			}
		}
	}
	
	$ExportArr[$i][0]  = $CLASS;		//student id
	$ExportArr[$i][1]  = $C_NO;			//code
	$ExportArr[$i][2]  = $UserLogin;  		//name in chinese
	$ExportArr[$i][3]  = $STUD_ID;  		//name in p
	$ExportArr[$i][4]  = $NAME_C;  			//sex
	$ExportArr[$i][5]  = $NAME_E;  		//date of birth
	$ExportArr[$i][6]  = $SEX;  		//place of birth
	$ExportArr[$i][7]  = $B_DATE;  		//id type
	$ExportArr[$i][8]  = $B_PLACE;  		//id number
	$ExportArr[$i][9]  = $NATION;  		//issue place
	$ExportArr[$i][10] = $ANCESTRAL_HOME;  		//issue date
	$ExportArr[$i][11] = $RACE;  		//valid date
	$ExportArr[$i][12] = $ID_NO;  		
	$ExportArr[$i][13] = $BIRTH_CERT_NO;  		
	$ExportArr[$i][14] = $RELIGION;  		
	$ExportArr[$i][15] = $FAMILY_LANG;  		//nation
	$ExportArr[$i][16] = $PAYMENT_TYPE;  		//origin
	$ExportArr[$i][17] = $TEL;  		//telephone
	$ExportArr[$i][18] = $EMAIL;
	$ExportArr[$i][19] = $LODGING;  		
	$ExportArr[$i][20] = $ADDRESS1;  		
	$ExportArr[$i][21] = $ADDRESS2; 		//area
	$ExportArr[$i][22] = $ADDRESS3;  		//road
	$ExportArr[$i][23] = $ADDRESS4;   	//address
	$ExportArr[$i][24] = $ADDRESS5; 		
	$ExportArr[$i][25] = $ADDRESS6; 		//form
	$ExportArr[$i][26] = $ADMISSION_DATE;  		//class name
	$ExportArr[$i][27] = $Siblings_STUD_ID_str;
	$ExportArr[$i][28] = $PRI_SCHOOL;  		//class number
	
	$father = $lsr->getParentGuardianInfo($userid, 'F');
	//debug_pr($father);
	$ExportArr[$i][29] = $father['NAME_C'];
	$ExportArr[$i][30] = $father['NAME_E'];
	$ExportArr[$i][31] = $father['PROF'];
	$ExportArr[$i][32] = $father['JOB_NATURE'];
	$ExportArr[$i][33] = $father['JOB_TITLE'];
	$ExportArr[$i][34] = $father['MARITAL_STATUS'];
	$ExportArr[$i][35] = $father['EDU_LEVEL'];
	$ExportArr[$i][36] = $father['TEL'];
	$ExportArr[$i][37] = $father['MOBILE'];
	$ExportArr[$i][38] = $father['OFFICE_TEL'];
	$ExportArr[$i][39] = $father['POSTAL_ADDR1'];
	$ExportArr[$i][40] = $father['POSTAL_ADDR2'];
	$ExportArr[$i][41] = $father['POSTAL_ADDR3'];
	$ExportArr[$i][42] = $father['POSTAL_ADDR4'];
	$ExportArr[$i][43] = $father['POSTAL_ADDR5'];
	$ExportArr[$i][44] = $father['POSTAL_ADDR6'];
	$ExportArr[$i][45] = $father['POST_CODE'];
	$ExportArr[$i][46] = $father['EMAIL'];
 	
 	$mother = $lsr->getParentGuardianInfo($userid, 'M');

	$ExportArr[$i][47] = $mother['NAME_C'];
	$ExportArr[$i][48] = $mother['NAME_E'];
	$ExportArr[$i][49] = $mother['PROF'];
	$ExportArr[$i][50] = $mother['JOB_NATURE'];
	$ExportArr[$i][51] = $mother['JOB_TITLE'];
	$ExportArr[$i][52] = $mother['MARITAL_STATUS'];
	$ExportArr[$i][53] = $mother['EDU_LEVEL'];
	$ExportArr[$i][54] = $mother['TEL'];
	$ExportArr[$i][55] = $mother['MOBILE'];
	$ExportArr[$i][56] = $mother['OFFICE_TEL'];
	$ExportArr[$i][57] = $mother['POSTAL_ADDR1'];
	$ExportArr[$i][58] = $mother['POSTAL_ADDR2'];
	$ExportArr[$i][59] = $mother['POSTAL_ADDR3'];
	$ExportArr[$i][60] = $mother['POSTAL_ADDR4'];
	$ExportArr[$i][61] = $mother['POSTAL_ADDR5'];
	$ExportArr[$i][62] = $mother['POSTAL_ADDR6'];
	$ExportArr[$i][63] = $mother['POST_CODE'];
	$ExportArr[$i][64] = $mother['EMAIL'];
	
	$guardian = $lsr->getParentGuardianInfo($userid, 'G');
	
	$ExportArr[$i][65] = $guardian['NAME_C'];
	$ExportArr[$i][66] = $guardian['NAME_E'];
	$ExportArr[$i][67] = $guardian['G_GENDER'];
	$ExportArr[$i][68] = $guardian['GUARD'];
	$ExportArr[$i][69] = $guardian['G_RELATION'];
	$ExportArr[$i][70] = $guardian['LIVE_SAME'];
	$ExportArr[$i][71] = $guardian['PROF'];
	$ExportArr[$i][72] = $guardian['JOB_NATURE'];
	$ExportArr[$i][73] = $guardian['JOB_TITLE'];
	$ExportArr[$i][74] = $guardian['EDU_LEVEL'];
	$ExportArr[$i][75] = $guardian['TEL'];
	$ExportArr[$i][76] = $guardian['MOBILE'];
	$ExportArr[$i][77] = $guardian['OFFICE_TEL'];
	$ExportArr[$i][78] = $guardian['POSTAL_ADDR1'];
	$ExportArr[$i][79] = $guardian['POSTAL_ADDR2'];
	$ExportArr[$i][80] = $guardian['POSTAL_ADDR3'];
	$ExportArr[$i][81] = $guardian['POSTAL_ADDR4'];
	$ExportArr[$i][82] = $guardian['POSTAL_ADDR5'];
	$ExportArr[$i][83] = $guardian['POSTAL_ADDR6'];
	$ExportArr[$i][84] = $guardian['POST_CODE'];
	$ExportArr[$i][85] = $guardian['EMAIL'];
}

//debug_pr($ExportArr);

# define column title
$exportColumn = array();
$exportColumn[0] = $Lang['StudentRegistry']['Malaysia_CSV'][0];
$exportColumn[1] = $Lang['StudentRegistry']['Malaysia_CSV'][1];

$export_content .= $lexport->GET_EXPORT_TXT_WITH_REFERENCE($ExportArr, $exportColumn, "\t", "\r\n", "\t", 0, "11");

intranet_closedb();

# Output the file to user browser
$lexport->EXPORT_FILE($filename, $export_content);

?>
