<?php
# modifying by :

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libaccessright.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry.php");

intranet_auth();
intranet_opendb();

$laccessright = new libaccessright();

if(!$plugin['AccountMgmt_StudentRegistry'] || (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-View") && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-Manage"))) {
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$linterface = new interface_html();
$lsr = new libstudentregistry();

	
if($task=='sex')
{
	$data = "<table width=\"90%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#CCCCCC\" class=\"layer_table_detail\">";
		$data .="<tr class=\"tabletop\">
					<td>#</td>
					<td>".$Lang['StudentRegistry']['Import_Malaysia'][6]."</td>
				</tr>";
		$result = $lsr->GENDER_ARY();
		foreach($result as $val=>$arr) {
			$data .="<tr class=\"tablerow1\">
						<td>$val</td>
						<td>".Get_Lang_Selection($arr[1],$arr[0])."</td>
					</tr>
					";
		}
	$data .="</table>";	
} 
else if($task=='birthplace')
{
	$data = "<table width=\"90%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#CCCCCC\" class=\"layer_table_detail\">";
		$data .="<tr class=\"tabletop\">
					<td>#</td>
					<td>".$Lang['StudentRegistry']['Import_Malaysia'][8]."</td>
				</tr>";
		$result = $lsr->MAL_B_PLACE_DATA_ARY();
		foreach($result as $val=>$arr) {
			$data .="<tr class=\"tablerow1\">
						<td>$val</td>
						<td>".Get_Lang_Selection($arr[1],$arr[0])."</td>
					</tr>
					";
		}
	$data .="</table>";	
} 

else if($task=='nation')
{
	$data = "<table width=\"90%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#CCCCCC\" class=\"layer_table_detail\">";
		$data .="<tr class=\"tabletop\">
					<td>#</td>
					<td>".$Lang['StudentRegistry']['Import_Malaysia'][9]."</td>
				</tr>";
		$result = $lsr->MAL_NATION_DATA_ARY();
		foreach($result as $val=>$arr) {
			$data .="<tr class=\"tablerow1\">
						<td>$val</td>
						<td>".Get_Lang_Selection($arr[1],$arr[0])."</td>
					</tr>
					";
		}
	$data .="</table>";	
} 
else if($task=='religion')
{
	$data = "<table width=\"90%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#CCCCCC\" class=\"layer_table_detail\">";
		$data .="<tr class=\"tabletop\">
					<td>#</td>
					<td>".$Lang['StudentRegistry']['Import_Malaysia'][14]."</td>
				</tr>";
		$result = $lsr->MAL_RELIGION_DATA_ARY();
		foreach($result as $val=>$arr) {
			$data .="<tr class=\"tablerow1\">
						<td>$val</td>
						<td>".Get_Lang_Selection($arr[1],$arr[0])."</td>
					</tr>
					";
		}
	$data .="</table>";	
}

else if($task=='familylang')
{
	$data = "<table width=\"90%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#CCCCCC\" class=\"layer_table_detail\">";
		$data .="<tr class=\"tabletop\">
					<td>#</td>
					<td>".$Lang['StudentRegistry']['Import_Malaysia'][15]."</td>
				</tr>";
		$result = $lsr->MAL_FAMILY_LANG_ARY();
		foreach($result as $val=>$arr) {
			$data .="<tr class=\"tablerow1\">
						<td>$val</td>
						<td>".Get_Lang_Selection($arr[1],$arr[0])."</td>
					</tr>
					";
		}
	$data .="</table>";	
}

else if($task=='paytype')
{
	$data = "<table width=\"90%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#CCCCCC\" class=\"layer_table_detail\">";
		$data .="<tr class=\"tabletop\">
					<td>#</td>
					<td>".$Lang['StudentRegistry']['Import_Malaysia'][16]."</td>
				</tr>";
		$result = $lsr->MAL_PAYMENT_TYPE_ARY();
		foreach($result as $val=>$arr) {
			$data .="<tr class=\"tablerow1\">
						<td>$val</td>
						<td>".Get_Lang_Selection($arr[1],$arr[0])."</td>
					</tr>
					";
		}
	$data .="</table>";	
}

else if($task=='house')
{
	$data = "<table width=\"90%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#CCCCCC\" class=\"layer_table_detail\">";
		$data .="<tr class=\"tabletop\">
					<td>#</td>
					<td>".$Lang['StudentRegistry']['Import_Malaysia'][19]."</td>
				</tr>";
		$result = $lsr->MAL_LODGING_ARY();
		foreach($result as $val=>$arr) {
			$data .="<tr class=\"tablerow1\">
						<td>$val</td>
						<td>".Get_Lang_Selection($arr[1],$arr[0])."</td>
					</tr>
					";
		}
	$data .="</table>";	
}

else if($task=='marital')
{
	$data = "<table width=\"90%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#CCCCCC\" class=\"layer_table_detail\">";
		$data .="<tr class=\"tabletop\">
					<td>#</td>
					<td>".$Lang['StudentRegistry']['MaritalStatus']."</td>
				</tr>";
		$result = $lsr->MAL_MARITAL_STATUS_DATA_ARY();
		foreach($result as $val=>$arr) {
			$data .="<tr class=\"tablerow1\">
						<td>$val</td>
						<td>".Get_Lang_Selection($arr[1],$arr[0])."</td>
					</tr>
					";
		}
	$data .="</table>";	
}

else if($task=='guardrelation')
{
	$data = "<table width=\"90%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#CCCCCC\" class=\"layer_table_detail\">";
		$data .="<tr class=\"tabletop\">
					<td>#</td>
					<td>".$Lang['StudentRegistry']['Import_Malaysia'][68]."</td>
				</tr>";
		$result = $lsr->MAL_GUARD_DATA_ARY();
		foreach($result as $val=>$arr) {
			$data .="<tr class=\"tablerow1\">
						<td>$val</td>
						<td>".Get_Lang_Selection($arr[1],$arr[0])."</td>
					</tr>
					";
		}
	$data .="</table>";	
}

else if($task=='staytogether')
{
	$data = "<table width=\"90%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#CCCCCC\" class=\"layer_table_detail\">";
		$data .="<tr class=\"tabletop\">
					<td>#</td>
					<td>".$Lang['StudentRegistry']['Import_Malaysia'][70]."</td>
				</tr>";
		$result = $lsr->MAL_LIVE_SAME_ARY();
		foreach($result as $val=>$arr) {
			$data .="<tr class=\"tablerow1\">
						<td>$val</td>
						<td>".Get_Lang_Selection($arr[1],$arr[0])."</td>
					</tr>
					";
		}
	$data .="</table>";	
}

else if($task=='race')
{
	$data = "<table width=\"90%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#CCCCCC\" class=\"layer_table_detail\">";
		$data .="<tr class=\"tabletop\">
					<td>#</td>
					<td>".$Lang['StudentRegistry']['Import_Malaysia'][11]."</td>
				</tr>";
		$result = $lsr->MAL_RACE_DATA_ARY();
		foreach($result as $val=>$arr) {
			$data .="<tr class=\"tablerow1\">
						<td>$val</td>
						<td>".Get_Lang_Selection($arr[1],$arr[0])."</td>
					</tr>
					";
		}
	$data .="</table>";	
}

else if($task=='jobnature')
{
	$data = "<table width=\"90%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#CCCCCC\" class=\"layer_table_detail\">";
		$data .="<tr class=\"tabletop\">
					<td>#</td>
					<td>".$Lang['StudentRegistry']['JobNature']."</td>
				</tr>";
		$result = $lsr->MAL_JOB_NATURE_DATA_ARY();
		foreach($result as $val=>$arr) {
			$data .="<tr class=\"tablerow1\">
						<td>$val</td>
						<td>".Get_Lang_Selection($arr[1],$arr[0])."</td>
					</tr>
					";
		}
	$data .="</table>";	
}

else if($task=='jobtitle')
{
	$data = "<table width=\"90%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#CCCCCC\" class=\"layer_table_detail\">";
		$data .="<tr class=\"tabletop\">
					<td>#</td>
					<td>".$Lang['StudentRegistry']['JobTitle']."</td>
				</tr>";
		$result = $lsr->MAL_JOB_TITLE_DATA_ARY();
		foreach($result as $val=>$arr) {
			$data .="<tr class=\"tablerow1\">
						<td>$val</td>
						<td>".Get_Lang_Selection($arr[1],$arr[0])."</td>
					</tr>
					";
		}
	$data .="</table>";	
}

else if($task=='edulevel')
{
	$data = "<table width=\"90%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#CCCCCC\" class=\"layer_table_detail\">";
		$data .="<tr class=\"tabletop\">
					<td>#</td>
					<td>".$Lang['StudentRegistry']['EducationLevel']."</td>
				</tr>";
		$result = $lsr->MAL_EDU_LEVEL_DATA_ARY();
		foreach($result as $val=>$arr) {
			$data .="<tr class=\"tablerow1\">
						<td>$val</td>
						<td>".Get_Lang_Selection($arr[1],$arr[0])."</td>
					</tr>
					";
		}
	$data .="</table>";	
}

$output = '<table width="90%" border="0" cellpadding="0" cellspacing="0">
			  <tr>
				<td height="19"><table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
					<td width="5" height="19"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_01.gif" width="5" height="19"></td>
					<td height="19" valign="middle" background="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_02.gif">&nbsp;</td>
					<td width="19" height="19"><a href="javascript:Hide_Window(\'ref_list\')"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_close_off.gif" name="pre_close1" width="19" height="19" border="0" id="pre_close1"></a></td>
				  </tr>
				</table></td>
			  </tr>
			  <tr>
				<td><table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
					<td width="5" background="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_04.gif"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_04.gif" width="5" height="19"></td>
					<td align="left" bgcolor="#FFFFF7"><table width="100%" border="0" cellspacing="0" cellpadding="0">
					  
					  <tr>
						<td height="150" align="left" valign="top">
							<div id="list" style="overflow: auto; z-index:1; height: 150px;">
								'.$data.'
							</div>
						</td></tr>
							  <tr>
						    <td><br><table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
						      <tr>
							       <td height="1" class="dotline"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/10x10.gif" width="10" height="1"></td>
						      </tr>
						     </table></td>
						     </tr>
                 </table></td>
					<td width="6" background="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_06.gif"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_06.gif" width="6" height="6"></td>
				  </tr>
				  <tr>
					<td width="5" height="6"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_07.gif" width="5" height="6"></td>
					<td height="6" background="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_08.gif"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_08.gif" width="5" height="6"></td>
					<td width="6" height="6"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_09.gif" width="6" height="6"></td>
				  </tr>
					</table></td>
			  </tr>
			</table>';
echo $output;
?>
