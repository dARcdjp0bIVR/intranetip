<?
// Modifying by: 

############# Change Log [Start] ################
#
#   Date:   2019-05-13 Cameron
#           - fix potential sql injection problem by enclosing var with apostrophe
#
############# Change Log [End] ################


$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libaccessright.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry.php");

intranet_opendb();

# Check access right
if(!$plugin['AccountMgmt_StudentRegistry'] || (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-View") && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-ViewBasic") && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-ViewAdv") && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-Manage") && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-ManageBasic") && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-ManageAdv"))) {
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$lsr = new libstudentregistry();
$li = new libdb();

$limport = new libimporttext();
$lo = new libfilesystem();
$name = $_FILES['csvfile']['name'];
$ext = strtoupper($lo->file_ext($name));

if(!($ext == ".CSV" || $ext == ".TXT"))
{
	intranet_closedb();
	header("location: import_malaysia.php?xmsg=import_failed");
	exit();
}

//$file_format = array("CLASS", "C_NO", "UserLogin", "STUD_ID", "CODE", "NAME_C", "NAME_E", "ENTRY_DATE", "S_CODE", "SEX", "B_DATE", "B_PLACE", "ID_TYPE", "ID_NO", "I_PLACE", "I_DATE", "V_DATE", "S6_TYPE", "S6_IDATE", "S6_VDATE", "NATION", "ORIGIN", "RELIGION", "EMAIL", "TEL", "R_AREA", "RA_DESC", "AREA", "ROAD", "ADDRESS", "FATHER_C", "FATHER_E", "F_PROF", "F_MARTIAL", "F_TEL", "F_MOBILE", "F_OFFICE", "F_EMAIL", "MOTHER_C", "MOTHER_E", "M_PROF", "M_MARTIAL", "M_TEL", "M_MOBILE", "M_OFFICE", "M_EMAIL", "GUARDIAN_C", "GUARDIA_E", "G_SEX", "G_PROF", "GUARD", "G_REL_OTHERS", "LIVE_SAME", "G_AREA", "G_ROAD", "G_ADDRESS", "G_TEL", "G_MOBILE", "G_OFFICE", "G_EMAIL", "EC_NAME_C", "EC_NAME_E", "EC_NAME_P", "EC_TEL", "EC_MOBILE", "EC_AREA", "EC_ROAD", "EC_ADDRESS");
$file_format = array("CLASS", "C_NO", "UserLogin", "STUD_ID", "NAME_C", "NAME_E", "SEX", "B_DATE", "B_PLACE", "NATION", "ANCESTRAL_HOME", "RACE", "ID_NO", "BIRHT_CERT_NO", "RELIGION", "FAMILY_LANG", "PAYMENT_TYPE", "TEL", "EMAIL", "LODGING", "ADDRESS1", "ADDRESS2", "ADDRESS3", "ADDRESS4", "ADDRESS5", "ADDRESS6", "ENTRY_DATE", "SIBLINGS", "PRI_SCHOOL", "FATHER_C", "FATHER_E", "F_PROF", "F_JOB_NATURE", "F_JOB_TITLE", "F_MARITAL", "F_EDU_LEVEL", "F_TEL", "F_MOBILE", "F_OFFICE", "F_POSTAL_ADDR1", "F_POSTAL_ADDR2", "F_POSTAL_ADDR3", "F_POSTAL_ADDR4", "F_POSTAL_ADDR5", "F_POSTAL_ADDR6", "F_POSTAL_CODE", "F_EMAIL", "MOTHER_C", "MOTHER_E", "M_PROF", "M_JOB_NATURE", "M_JOB_TITLE", "M_MARITAL", "M_EDU_LEVEL", "M_TEL", "M_MOBILE", "M_OFFICE", "M_POSTAL_ADDR1", "M_POSTAL_ADDR2", "M_POSTAL_ADDR3", "M_POSTAL_ADDR4", "M_POSTAL_ADDR5", "M_POSTAL_ADDR6", "M_POSTAL_CODE", "M_EMAIL", "GUARD_C", "GUARD_E", "G_SEX", "GUARD", "G_REL_OTHERS", "LIVE_SAME", "G_PROF", "G_JOB_NATURE", "G_JOB_TITLE", "G_EDU_LEVEL", "G_TEL", "G_MOBILE", "G_OFFICE", "G_POSTAL_ADDR1", "G_POSTAL_ADDR2", "G_POSTAL_ADDR3", "G_POSTAL_ADDR4", "G_POSTAL_ADDR5", "G_POSTAL_ADDR6", "G_POSTAL_CODE", "G_EMAIL");
$flagAry = array(1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1);

$data = $limport->GET_IMPORT_TXT_WITH_REFERENCE($csvfile,"","",$file_format,$flagAry);

$col_name = array_shift($data);

$PAGE_NAVIGATION[] = array($Lang['StudentRegistry']['ClassList'], "index.php?AcademicYearID=".$AcademicYearID);
$PAGE_NAVIGATION[] = array($Lang['StudentRegistry']['StudentList'], "class.php?AcademicYearID=".$AcademicYearID."&targetClass=".$targetClass);

# step information
$STEPS_OBJ[] = array($i_general_select_csv_file, 0);
$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 1);
$STEPS_OBJ[] = array($i_general_imported_result, 0);

$format_wrong = false;
for($i=0; $i<sizeof($file_format); $i++)
{
	if ($col_name[$i]!=$file_format[$i])
	{
		$format_wrong = true;
		break;
	}
}


$numOfData = count($data);
if($format_wrong || $numOfData==0)
{
	$returnMsg = ($format_wrong)? 'import_header_failed' : 'import_no_record';
	intranet_closedb();
	header("location: import_malaysia.php?xmsg=".$returnMsg);
	exit();
}

# Title / Menu
$linterface = new interface_html();

$CurrentPageArr['StudentRegistry'] = 1;
$CurrentPage = "Mgmt_RegistryInfo";

$MODULE_OBJ['title'] = $Lang['Header']['Menu']['StudentRegistry'];
$TAGS_OBJ[] = array($Lang['Menu']['AccountMgmt']['StudentRegistryInfo'], "index.php");

$linterface->LAYOUT_START(urldecode($_REQUEST['ReturnMessage'])); 

# create temp table if temp table not exists
$sql = "CREATE TABLE IF NOT EXISTS TEMP_STUDENT_REGISTRY_MALAYSIA_IMPORT
	(
		TempID int(11) NOT NULL auto_increment,
		StudentID int(11),
		CLASS varchar(255),
		C_NO int(8),
		UserLogin varchar(20),
		STUD_ID varchar(20),
		NAME_C varchar(100),
		NAME_E varchar(100),
		SEX char(2),
		B_DATE date,
		B_PLACE int(8),
		NATION varchar(2),
		ANCESTRAL_HOME varchar(20),
		RACE varchar(20),
		ID_NO varchar(20),
		BIRTH_CERT_NO varchar(10),
		RELIGION int(8),
		FAMILY_LANG int(8),
		PAYMENT_TYPE int(8),
		TEL varchar(20),
		EMAIL varchar(255),
		LODGING int(8),
		ADDRESS1 varchar(255),
		ADDRESS2 varchar(255),
		ADDRESS3 varchar(255),
		ADDRESS4 varchar(255),
		ADDRESS5 varchar(255),
		ADDRESS6 varchar(255),
		ENTRY_DATE date,
		SIBLINGS varchar(255),
		PRI_SCHOOL varchar(255),
		FATHER_C varchar(255),
		FATHER_E varchar(255),
		F_PROF varchar(255),
		F_JOB_NATURE varchar(255),
		F_JOB_TITLE varchar(255),
		F_MARITAL int(8),
		F_EDU_LEVEL varchar(255),
		F_TEL varchar(20),
		F_MOBILE varchar(20),
		F_OFFICE varchar(20),
		F_POSTAL_ADDR1 varchar(255),
		F_POSTAL_ADDR2 varchar(255),
		F_POSTAL_ADDR3 varchar(255),
		F_POSTAL_ADDR4 varchar(255),
		F_POSTAL_ADDR5 varchar(255),
		F_POSTAL_ADDR6 varchar(255),
		F_POSTAL_CODE varchar(255),
		F_EMAIL varchar(255),
		MOTHER_C varchar(255),
		MOTHER_E varchar(255),
		M_PROF varchar(255),
		M_JOB_NATURE varchar(255),
		M_JOB_TITLE varchar(255),
		M_MARITAL int(8),
		M_EDU_LEVEL varchar(255),
		M_TEL varchar(20),
		M_MOBILE varchar(20),
		M_OFFICE varchar(20),
		M_POSTAL_ADDR1 varchar(255),
		M_POSTAL_ADDR2 varchar(255),
		M_POSTAL_ADDR3 varchar(255),
		M_POSTAL_ADDR4 varchar(255),
		M_POSTAL_ADDR5 varchar(255),
		M_POSTAL_ADDR6 varchar(255),
		M_POSTAL_CODE varchar(255),
		M_EMAIL varchar(255),
		GUARD_C varchar(255),
		GUARD_E varchar(255),
		G_SEX char(2),
		GUARD char(1),
		G_REL_OTHERS varchar(255),
		LIVE_SAME tinyint(1),
		G_PROF varchar(255),
		G_JOB_NATURE varchar(255),
		G_JOB_TITLE varchar(255),
		G_EDU_LEVEL varchar(255),
		G_TEL varchar(40),
		G_MOBILE varchar(40),
		G_OFFICE varchar(40),
		G_POSTAL_ADDR1 varchar(255),
		G_POSTAL_ADDR2 varchar(255),
		G_POSTAL_ADDR3 varchar(255),
		G_POSTAL_ADDR4 varchar(255),
		G_POSTAL_ADDR5 varchar(255),
		G_POSTAL_ADDR6 varchar(255),
		G_POSTAL_CODE varchar(255),
		G_EMAIL varchar(255),
		DateInput datetime,
		InputBy int(11),
		PRIMARY KEY (TempID),
		KEY InputBy (InputBy)

	) ENGINE=InnoDB DEFAULT CHARSET=utf8";
$lsr->db_db_query($sql) or die(mysql_error());

# delete the temp data in temp table 
$sql = "delete from TEMP_STUDENT_REGISTRY_MALAYSIA_IMPORT where InputBy=$UserID";

$lsr->db_db_query($sql) or die(mysql_error());

$errorCount = 0;
$successCount = 0;
$error_result = array();
$i=0;


foreach($data as $key => $data)
{
	$i++;
	$thisAry = array();
	$error_msg = array();
	
	#Covert StudentNo to UserID in Siblings
	$SIBLINGS_STUD_ID_ARY = explode(",", trim($data[27]));
	$SIBLINGS_UserID_Str = "";
	for($i=0;$i<sizeof($SIBLINGS_STUD_ID_ARY);$i++)
	{
		$sql = "SELECT UserID FROM STUDENT_REGISTRY_STUDENT WHERE STUD_ID = '".$SIBLINGS_STUD_ID_ARY[$i]."'";
		$result_UserID = $lsr->returnVector($sql);
		for($j=0;$j<sizeof($result_UserID);$j++)
		{
			$SIBLINGS_UserID_Str .= $result_UserID[$j];
			if($j!=sizeof($result_UserID)-1)
				$SIBLINGS_UserID_Str .= ",";
		}
		if($i!=sizeof($SIBLINGS_STUD_ID_ARY)-1)
			$SIBLINGS_UserID_Str .= ",";
	}

	$thisAry['CLASS'] 			= trim($data[0]);
	$thisAry['C_NO'] 			= trim($data[1]);
	$thisAry['UserLogin'] 		= trim($data[2]);
	$thisAry['STUD_ID'] 		= trim($data[3]);
	$thisAry['NAME_C'] 			= trim($data[4]);
	$thisAry['NAME_E'] 			= trim($data[5]);
	$thisAry['SEX'] 			= trim($data[6]);
	$thisAry['B_DATE'] 			= trim($data[7]);
	$thisAry['B_PLACE'] 		= trim($data[8]);
	$thisAry['NATION'] 			= trim($data[9]);
	$thisAry['ANCESTRAL_HOME'] 	= trim($data[10]);
	$thisAry['RACE'] 			= trim($data[11]);
	$thisAry['ID_NO'] 			= trim($data[12]);
	$thisAry['BIRTH_CERT_NO'] 	= trim($data[13]);
	$thisAry['RELIGION'] 		= trim($data[14]);
	$thisAry['FAMILY_LANG'] 	= trim($data[15]);
	$thisAry['PAYMENT_TYPE'] 	= trim($data[16]);
	$thisAry['TEL'] 			= trim($data[17]);
	$thisAry['EMAIL'] 			= trim($data[18]);
	$thisAry['LODGING'] 		= trim($data[19]);
	$thisAry['ADDRESS1'] 		= trim($data[20]);
	$thisAry['ADDRESS2'] 		= trim($data[21]);
	$thisAry['ADDRESS3'] 		= trim($data[22]);
	$thisAry['ADDRESS4'] 		= trim($data[23]);
	$thisAry['ADDRESS5'] 		= trim($data[24]);
	$thisAry['ADDRESS6'] 		= trim($data[25]);
	$thisAry['ENTRY_DATE'] 		= trim($data[26]);
	$thisAry['SIBLINGS'] 		= $SIBLINGS_UserID_Str;
	$thisAry['PRI_SCHOOL'] 		= trim($data[28]);
	$thisAry['FATHER_C'] 		= trim($data[29]);
	$thisAry['FATHER_E'] 		= trim($data[30]);
	$thisAry['F_PROF'] 			= trim($data[31]);
	$thisAry['F_JOB_NATURE'] 	= trim($data[32]);
	$thisAry['F_JOB_TITLE'] 	= trim($data[33]);
	$thisAry['F_MARITAL'] 		= trim($data[34]);
	$thisAry['F_EDU_LEVEL'] 	= trim($data[35]);
	$thisAry['F_TEL'] 			= trim($data[36]);
	$thisAry['F_MOBILE'] 		= trim($data[37]);
	$thisAry['F_OFFICE'] 		= trim($data[38]);
	$thisAry['F_POSTAL_ADDR1']	= trim($data[39]);
	$thisAry['F_POSTAL_ADDR2'] 	= trim($data[40]);
	$thisAry['F_POSTAL_ADDR3'] 	= trim($data[41]);
	$thisAry['F_POSTAL_ADDR4'] 	= trim($data[42]);
	$thisAry['F_POSTAL_ADDR5'] 	= trim($data[43]);
	$thisAry['F_POSTAL_ADDR6'] 	= trim($data[44]);
	$thisAry['F_POST_CODE'] 	= trim($data[45]);
	$thisAry['F_EMAIL']			= trim($data[46]);
	$thisAry['MOTHER_C'] 		= trim($data[47]);
	$thisAry['MOTHER_E'] 		= trim($data[48]);
	$thisAry['M_PROF'] 			= trim($data[49]);
	$thisAry['M_JOB_NATURE'] 	= trim($data[50]);
	$thisAry['M_JOB_TITLE'] 	= trim($data[51]);
	$thisAry['M_MARITAL'] 		= trim($data[52]);
	$thisAry['M_EDU_LEVEL'] 	= trim($data[53]);
	$thisAry['M_TEL'] 			= trim($data[54]);
	$thisAry['M_MOBILE'] 		= trim($data[55]);
	$thisAry['M_OFFICE'] 		= trim($data[56]);
	$thisAry['M_POSTAL_ADDR1'] 	= trim($data[57]);
	$thisAry['M_POSTAL_ADDR2']	= trim($data[58]);
	$thisAry['M_POSTAL_ADDR3'] 	= trim($data[59]);
	$thisAry['M_POSTAL_ADDR4'] 	= trim($data[60]);
	$thisAry['M_POSTAL_ADDR5']	= trim($data[61]);
	$thisAry['M_POSTAL_ADDR6'] 	= trim($data[62]);
	$thisAry['M_POST_CODE'] 	= trim($data[63]);
	$thisAry['M_EMAIL'] 		= trim($data[64]);
	$thisAry['GUARDIAN_C']		= trim($data[65]);
	$thisAry['GUARDIAN_E'] 		= trim($data[66]);
	$thisAry['G_SEX'] 			= trim($data[67]);
	$thisAry['GUARD'] 			= trim($data[68]);
	$thisAry['G_REL_OTHERS']	= trim($data[69]);
	$thisAry['LIVE_SAME'] 		= trim($data[70]);
	$thisAry['G_PROF'] 			= trim($data[71]);
	$thisAry['G_JOB_NATURE'] 	= trim($data[72]);
	$thisAry['G_JOB_TITLE'] 	= trim($data[73]);
	$thisAry['G_EDU_LEVEL'] 	= trim($data[74]);
	$thisAry['G_TEL'] 			= trim($data[75]);
	$thisAry['G_MOBILE'] 		= trim($data[76]);
	$thisAry['G_OFFICE'] 		= trim($data[77]);
	$thisAry['G_POSTAL_ADDR1'] 	= trim($data[78]);
	$thisAry['G_POSTAL_ADDR2'] 	= trim($data[79]);
	$thisAry['G_POSTAL_ADDR3'] 	= trim($data[80]);
	$thisAry['G_POSTAL_ADDR4'] 	= trim($data[81]);
	$thisAry['G_POSTAL_ADDR5'] 	= trim($data[82]);
	$thisAry['G_POSTAL_ADDR6'] 	= trim($data[83]);
	$thisAry['G_POST_CODE'] 	= trim($data[84]);
	$thisAry['G_EMAIL'] 		= trim($data[85]);

	### store csv data to temp data
	$error_msg = $lsr->checkData_Mal('import', $thisAry);
		
	$s_id = $lsr->retrieveStudentIDByClassOrLogin($thisAry['CLASS'], $thisAry['C_NO'], $thisAry['UserLogin']);
	
	if(empty($error_msg))
	{
		$successCount++;
		
		$sql = "INSERT INTO TEMP_STUDENT_REGISTRY_MALAYSIA_IMPORT 
				(
				 StudentID,
				 CLASS,
				 C_NO,
				 UserLogin,
				 STUD_ID,
				 NAME_C,
				 NAME_E,
				 SEX,
				 B_DATE,
				 B_PLACE,
				 NATION, 
				 ANCESTRAL_HOME,
				 RACE,
				 ID_NO,
				 BIRTH_CERT_NO,
				 RELIGION,
				 FAMILY_LANG,
			 	 PAYMENT_TYPE,
				 TEL,
				 EMAIL,
			 	 LODGING,
			     ADDRESS1,
				 ADDRESS2,
				 ADDRESS3,
				 ADDRESS4,
				 ADDRESS5,
				 ADDRESS6,
				 ENTRY_DATE,
				 SIBLINGS,
				 PRI_SCHOOL,
				 FATHER_C,
				 FATHER_E,
				 F_PROF,
				 F_JOB_NATURE,
				 F_JOB_TITLE,
				 F_MARITAL,
				 F_EDU_LEVEL,
				 F_TEL,
				 F_MOBILE,
				 F_OFFICE,
				 F_POSTAL_ADDR1,
				 F_POSTAL_ADDR2,
				 F_POSTAL_ADDR3,
				 F_POSTAL_ADDR4,
				 F_POSTAL_ADDR5,
				 F_POSTAL_ADDR6,
				 F_POSTAL_CODE,
				 F_EMAIL,
				 MOTHER_C,
				 MOTHER_E,
				 M_PROF,
				 M_JOB_NATURE,
				 M_JOB_TITLE,
				 M_MARITAL,
				 M_EDU_LEVEL,
				 M_TEL,
				 M_MOBILE,
				 M_OFFICE,
				 M_POSTAL_ADDR1,
				 M_POSTAL_ADDR2,
				 M_POSTAL_ADDR3,
				 M_POSTAL_ADDR4,
				 M_POSTAL_ADDR5,
				 M_POSTAL_ADDR6,
				 M_POSTAL_CODE,
				 M_EMAIL,
				 GUARD_C,
				 GUARD_E,
				 G_SEX,
				 GUARD,
				 G_REL_OTHERS,
				 LIVE_SAME,
				 G_PROF,
				 G_JOB_NATURE,
				 G_JOB_TITLE,
				 G_EDU_LEVEL,
				 G_TEL,
				 G_MOBILE,
				 G_OFFICE,
				 G_POSTAL_ADDR1,
				 G_POSTAL_ADDR2,
				 G_POSTAL_ADDR3,
				 G_POSTAL_ADDR4,
				 G_POSTAL_ADDR5,
				 G_POSTAL_ADDR6,
				 G_POSTAL_CODE,
				 G_EMAIL,
				 DateInput,
				 InputBy
				) 
				VALUES
				(
				 $s_id,
				 '".intranet_htmlspecialchars($thisAry['CLASS'])."',
				 '".intranet_htmlspecialchars($thisAry['C_NO'])."',
				 '".$thisAry['UserLogin']."',
				 '".$thisAry['STUD_ID']."',
				 '".intranet_htmlspecialchars($thisAry['NAME_C'])."',
				 '".intranet_htmlspecialchars($thisAry['NAME_E'])."',
				 '".$thisAry['SEX']."',
				 '".$thisAry['B_DATE']."',
				 '".intranet_htmlspecialchars($thisAry['B_PLACE'])."', 
				 '".intranet_htmlspecialchars($thisAry['NATION'])."',
				 '".intranet_htmlspecialchars($thisAry['ANCESTRAL_HOME'])."',
				 '".intranet_htmlspecialchars($thisAry['RACE'])."',
				 '".intranet_htmlspecialchars($thisAry['ID_NO'])."',
				 '".intranet_htmlspecialchars($thisAry['BIRTH_CERT_NO'])."',
				 '".intranet_htmlspecialchars($thisAry['RELIGION'])."',
				 '".intranet_htmlspecialchars($thisAry['FAMILY_LANG'])."',
				 '".intranet_htmlspecialchars($thisAry['PAYMENT_TYPE'])."',
				 '".$thisAry['TEL']."',
				 '".$thisAry['EMAIL']."',
				 '".intranet_htmlspecialchars($thisAry['LODGING'])."',
				 '".intranet_htmlspecialchars($thisAry['ADDRESS1'])."',
				 '".intranet_htmlspecialchars($thisAry['ADDRESS2'])."',
				 '".intranet_htmlspecialchars($thisAry['ADDRESS3'])."',
				 '".intranet_htmlspecialchars($thisAry['ADDRESS4'])."',
				 '".intranet_htmlspecialchars($thisAry['ADDRESS5'])."',
				 '".intranet_htmlspecialchars($thisAry['ADDRESS6'])."',
				 '".$thisAry['ENTRY_DATE']."',
				 '".intranet_htmlspecialchars($thisAry['SIBLINGS'])."',
				 '".intranet_htmlspecialchars($thisAry['PRI_SCHOOL'])."',
				 '".intranet_htmlspecialchars($thisAry['FATHER_C'])."',
				 '".intranet_htmlspecialchars($thisAry['FATHER_E'])."',
				 '".intranet_htmlspecialchars($thisAry['F_PROF'])."',
				 '".intranet_htmlspecialchars($thisAry['F_JOB_NATURE'])."',
				 '".intranet_htmlspecialchars($thisAry['F_JOB_TITLE'])."',
				 '".$thisAry['F_MARITAL']."',
				 '".intranet_htmlspecialchars($thisAry['F_EDU_LEVEL'])."',
				 '".$thisAry['F_TEL']."',
				 '".$thisAry['F_MOBILE']."',
				 '".$thisAry['F_OFFICE']."',
				 '".intranet_htmlspecialchars($thisAry['F_POSTAL_ADDR1'])."',
				 '".intranet_htmlspecialchars($thisAry['F_POSTAL_ADDR2'])."',
				 '".intranet_htmlspecialchars($thisAry['F_POSTAL_ADDR3'])."',
				 '".intranet_htmlspecialchars($thisAry['F_POSTAL_ADDR4'])."',
				 '".intranet_htmlspecialchars($thisAry['F_POSTAL_ADDR5'])."',
				 '".intranet_htmlspecialchars($thisAry['F_POSTAL_ADDR6'])."',
				 '".intranet_htmlspecialchars($thisAry['F_POST_CODE'])."',
				 '".$thisAry['F_EMAIL']."',
				 '".intranet_htmlspecialchars($thisAry['MOTHER_C'])."',
				 '".intranet_htmlspecialchars($thisAry['MOTHER_E'])."',
				 '".intranet_htmlspecialchars($thisAry['M_PROF'])."',
				 '".intranet_htmlspecialchars($thisAry['M_JOB_NATURE'])."',
				 '".intranet_htmlspecialchars($thisAry['M_JOB_TITLE'])."',
				 '".$thisAry['M_MARITAL']."',
				 '".intranet_htmlspecialchars($thisAry['M_EDU_LEVEL'])."',
				 '".$thisAry['M_TEL']."',
				 '".$thisAry['M_MOBILE']."',
				 '".$thisAry['M_OFFICE']."',
				 '".intranet_htmlspecialchars($thisAry['M_POSTAL_ADDR1'])."',
				 '".intranet_htmlspecialchars($thisAry['M_POSTAL_ADDR2'])."',
				 '".intranet_htmlspecialchars($thisAry['M_POSTAL_ADDR3'])."',
				 '".intranet_htmlspecialchars($thisAry['M_POSTAL_ADDR4'])."',
				 '".intranet_htmlspecialchars($thisAry['M_POSTAL_ADDR5'])."',
				 '".intranet_htmlspecialchars($thisAry['M_POSTAL_ADDR6'])."',
				 '".intranet_htmlspecialchars($thisAry['M_POST_CODE'])."',
				 '".$thisAry['M_EMAIL']."',
				 '".intranet_htmlspecialchars($thisAry['GUARDIAN_C'])."',
				 '".intranet_htmlspecialchars($thisAry['GUARDIAN_E'])."',
				 '".$thisAry['G_SEX']."',
				 '".$thisAry['GUARD']."',
				 '".intranet_htmlspecialchars($thisAry['G_REL_OTHERS'])."',
				 '".$thisAry['LIVE_SAME']."',
				 '".intranet_htmlspecialchars($thisAry['G_PROF'])."',
				 '".intranet_htmlspecialchars($thisAry['G_JOB_NATURE'])."',
				 '".intranet_htmlspecialchars($thisAry['G_JOB_TITLE'])."',
				 '".intranet_htmlspecialchars($thisAry['G_EDU_LEVEL'])."',
				 '".$thisAry['G_TEL']."',
				 '".$thisAry['G_MOBILE']."',
				 '".$thisAry['G_OFFICE']."',
				 '".intranet_htmlspecialchars($thisAry['G_POSTAL_ADDR1'])."',
				 '".intranet_htmlspecialchars($thisAry['G_POSTAL_ADDR2'])."',
				 '".intranet_htmlspecialchars($thisAry['G_POSTAL_ADDR3'])."',
				 '".intranet_htmlspecialchars($thisAry['G_POSTAL_ADDR4'])."',
				 '".intranet_htmlspecialchars($thisAry['G_POSTAL_ADDR5'])."',
				 '".intranet_htmlspecialchars($thisAry['G_POSTAL_ADDR6'])."',
				 '".intranet_htmlspecialchars($thisAry['G_POST_CODE'])."',
				 '".$thisAry['G_EMAIL']."',
				 NOW(),
				 $UserID
				) 
				";
		$lsr->db_db_query($sql) or die(mysql_error());
	}
	else
	{
		$error_result[$TempID] = $error_msg;
		$error_TempID_str .=  $TempID . ",";
		$errorCount++;
		
		$displayContent = "<ul>";
		foreach($error_msg as $err) {
			$displayContent .= "<li>".$err."</li>";
		}
		$displayContent .= "</ul>";
		
		$stdInfo = $lsr->getUserInfoByID($s_id);
		$name = ((!isset($error_msg['UserInfo'])) ? Get_Lang_Selection($stdInfo['ChineseName'], $stdInfo['EnglishName']) : ("<font color='red'>[".(($thisAry['CLASS']!="" && $thisAry['C_NO']!="")? ($thisAry['CLASS']."-".$thisAry['C_NO']) : $thisAry['UserLogin'])."]</font>"));
																					
		$msg .= "<tr style='vertical-align:top'>";
		$msg .= "<td class=\"tablebluerow".(($i+1)%2+1)."\" width=\"10\">". $i ."</td>";
		$msg .= "<td class=\"tablebluerow".(($i+1)%2+1)."\">". $name ."</td>";
		$msg .= "<td class=\"tablebluerow".(($i+1)%2+1)."\"><font color='red'>". $displayContent ."</font></td>";
		$msg .= "</tr>";
		
	}
	
}

### List out the import result
$x .= "<table width=\"90%\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\">";
/*
$x .= "<tr>";
$x .= "<td class='formfieldtitle' width='30%' align='left'>". $Lang['SysMgr']['FormClassMapping']['SchoolYear'] ."</td>";
$x .= "<td class='tabletext'>". GetCurrentAcademicYear() ."</td>";
$x .= "</tr>";
*/
$x .= "<tr>";
$x .= "<td class='formfieldtitle' width='30%' align='left'>". $Lang['General']['SuccessfulRecord'] ."</td>";
$x .= "<td class='tabletext'>". $successCount ."</td>";
$x .= "</tr>";

$x .= "<tr>";
$x .= "<td class='formfieldtitle' width='30%' align='left'>". $Lang['General']['FailureRecord'] ."</td>";
$x .= "<td class='tabletext'>". ($errorCount ? "<font color='red'>":"") . $errorCount . ($errorCount ? "</font>":"") ."</td>";
$x .= "</tr>";

$x .= "</table><br>";

if($error_result)
{
	$x .= "<table width=\"90%\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\">";
	
	$x .= "<tr>";
	$x .= "<td class=\"tablebluetop tabletopnolink\" width=\"10\">Row#</td>";
	$x .= "<td class=\"tablebluetop tabletopnolink\">". $i_UserStudentName ."</td>";
	$x .= "<td class=\"tablebluetop tabletopnolink\">". $Lang['General']['Remark'] ."</td>";
	$x .= "</tr>";
	$x .= $msg;
	$x .= "</table>";
}


if($errorCount>0)
{
	$import_button = $linterface->GET_ACTION_BTN($button_back, "button", "window.location='import_malaysia.php?AcademicYearID=".$AcademicYearID."&targetClass=".$targetClass."'");
}
else
{
	$import_button = $linterface->GET_ACTION_BTN($button_import, "submit");
	$import_button .= " &nbsp;";
	$import_button .= $linterface->GET_ACTION_BTN($button_back, "button", "window.location='import_malaysia.php?AcademicYearID=".$AcademicYearID."&targetClass=".$targetClass."'");
}
?>

<br />
<form name="form1" method="post" action="import_malaysia_update.php" enctype="multipart/form-data">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?=$linterface->GET_NAVIGATION($PAGE_NAVIGATION)?></td>
</tr>
<tr>
	<td><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
</tr>
<tr>
	<td align="center"><?=$x?></td>
</tr>
		
<tr>
	<td>
		<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr>
			<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
		</tr>
		<tr>
			<td align="center" colspan="2">
			<?=$import_button?>
			</td>
		</tr>
		</table>
	</td>
</tr>
</table>
<input type="hidden" name="AcademicYearID" value="<?=$AcademicYearID?>">
<input type="hidden" name="targetClass" value="<?=$targetClass?>">
</form>
<br />

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>
