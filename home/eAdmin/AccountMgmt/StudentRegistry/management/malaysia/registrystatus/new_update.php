<?php
// modifying by : 
 
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccessright.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry.php");

intranet_auth();
intranet_opendb();

$laccessright = new libaccessright();

if(!$plugin['AccountMgmt_StudentRegistry'] || (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryStatus-Manage"))) {
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}
if(sizeof($userID)==0) {
	header("Location: index.php");
	exit;
}

$lsr = new libstudentregistry();

$dataAry = array();
$dataAry['ApplyDate'] = $applyDate;
if($newstatus!=STUDENT_REGISTRY_RECORDSTATUS_RESUME) {
	$dataAry['Reason'] = $reason;
	if($reason==0)
		$dataAry['ReasonOthers'] = $otherReason;
}

$dataAry['RecordStatus'] = $newstatus;
$dataAry['IsCurrent'] = 1;

$lsr->insertStudentRegistryStatus($dataAry, $userID);
$RecordID = $lsr->db_insert_id();

if($printNotice==1) {
	include_once($PATH_WRT_ROOT."includes/libnotice.php");
	include_once($PATH_WRT_ROOT."includes/libucc.php");
	$lnotice = new libnotice();
	$lc = new libucc();
		
	$template_info = $lsr->RETRIEVE_NOTICE_TEMPLATE_INFO($TemplateID);
	
	if(sizeof($template_info) && isset($TemplateID) && $TemplateID!="") {
		if($reason=="0")
			$reason = $otherReason;
		else {
			$reason = $Lang['StudentRegistry']['ReasonAry'][$reason];
		}
		
		$template_data = $lsr->TEMPLATE_VARIABLE_CONVERSION($userID, $newstatus, $TextAdditionalInfo, $reason, $applyDate);
		//debug_pr($template_data);
		$UserName = $lsr->getUserNameByID($userID);
	
		$Module = $lsr->Module;
		$NoticeNumber = time();
		$TemplateID = $TemplateID;
		$Variable = $template_data;
		$IssueUserID = $UserID;
		$IssueUserName = $UserName[0];
		$TargetRecipientType = "U";
		$RecipientID = array($userID);
		$RecordType = NOTICE_TO_SOME_STUDENTS;
			
		//	debug_pr($_POST); exit;
		$ReturnArr = $lc->setNoticeParameter($Module, $NoticeNumber, $TemplateID, $Variable, $IssueUserID, $IssueUserName, $TargetRecipientType, $RecipientID, $RecordType);
		//debug_pr($ReturnArr);
		$NoticeID = $lc->sendNotice();
	
		if($NoticeID != -1)
		{
			# update Conduct Record
			$sql = "UPDATE STUDENT_REGISTRY_STATUS SET NoticeID=$NoticeID WHERE RecordID=$RecordID";
			$lsr->db_db_query($sql);
		}
		
	}
}

intranet_closedb();


$msg = ($RecordID!=-1) ? $Lang['General']['ReturnMessage']['AddSuccess'] : $Lang['General']['ReturnMessage']['AddUnsuccess'];

if($skipPrintNotice==0 && $RecordID!=-1)
	header("Location: new_print_notice.php?NoticeID=$NoticeID&newstatus=$newstatus&xmsg=$msg");
else 
	header("Location: index.php?xmsg=$msg");
	
?>
