<?php
# using: 
$PATH_WRT_ROOT = "../../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/libaccessright.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$laccessright = new libaccessright();

if(!$plugin['AccountMgmt_StudentRegistry'] || (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-View") && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-Manage"))) {
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}
 
# Temp Assign memory of this page
ini_set("memory_limit", "150M"); 

$lsr = new libstudentregistry();
$lexport = new libexporttext();

$ExportArr = array();

# condition
$conds = "";

if($text != "")
	$conds .= " AND yc.ClassTitleEN LIKE '%$text%' OR yc.ClassTitleB5 LIKE '%$text%' OR USR.EnglishName LIKE '%$text%' OR USR.ChineseName LIKE '%$text%' OR USR.WebSAMSRegNo LIKE '%$text%' OR srs.CODE LIKE '%$text%')";

$filename = "student_registry_status_class_malaysia.csv";	


# SQL Statement
		$sql = "SELECT 
					yc.YearClassID, 
					".Get_Lang_Selection("yc.ClassTitleB5","yc.ClassTitleEN").", 
					COUNT(ycu.UserID) as studentCount, 
					y.YearName
				FROM 
					YEAR_CLASS yc LEFT OUTER JOIN 
					YEAR_CLASS_USER ycu ON (ycu.YearClassID=yc.YearClassID) INNER JOIN
					YEAR y ON (y.YearID=yc.YearID) LEFT OUTER JOIN
					INTRANET_USER USR ON (USR.UserID=ycu.UserID)
				WHERE 
					yc.AcademicYearID=$AcademicYearID AND USR.RecordStatus IN (0,1,2) AND (yc.ClassTitleEN LIKE '%$text%' OR yc.ClassTitleB5 LIKE '%$text%')
				GROUP BY 
					yc.YearClassID
				ORDER BY 
					y.Sequence, yc.Sequence
				";
	
$row = $lsr->returnArray($sql,4);

$inForm = array();
$totalNormal = 0;
$totalLeft = 0;
$totalSuspend = 0;
$totalResume = 0;
$sumTotal = 0;

# Create data array for export
for($i=0; $i<sizeof($row); $i++){
	list($yearClassID, $className, $studentCount, $yearName) = $row[$i];
	
	$studentAry = $lsr->storeStudent(0, '::'.$yearClassID);
	$conds = "";
	if(sizeof($studentAry)>0) {
		$studentIDList = implode(',', $studentAry);
		$conds = " AND UserID IN ($studentIDList)";
	}
			
	$sql = "SELECT RecordStatus, COUNT(DISTINCT UserID) FROM STUDENT_REGISTRY_STATUS WHERE IsCurrent=1 $conds GROUP BY RecordStatus";
	$temp = $lsr->returnArray($sql,2);
	$ary = array();
	for($k=0; $k<sizeof($temp); $k++) {
		list($status, $count) = $temp[$k];
		$ary[$status] = $count;
	}
	
	$normal = $studentCount-$ary[STUDENT_REGISTRY_RECORDSTATUS_LEFT]-$ary[STUDENT_REGISTRY_RECORDSTATUS_SUSPEND]-$ary[STUDENT_REGISTRY_RECORDSTATUS_RESUME];
	
	if(in_array($yearName, $inForm)) {
		$displayYearName = "";
	} else {
		$displayYearName = $yearName;
		$inForm[] = $yearName;
	}
	
	$ExportArr[$i][0] = $displayYearName;		//Year Name
	$ExportArr[$i][1] = $className;			//Class Name
	$ExportArr[$i][2] = $normal;  		//total no. of normal student
	$ExportArr[$i][3] = (isset($ary[STUDENT_REGISTRY_RECORDSTATUS_LEFT]) ? $ary[STUDENT_REGISTRY_RECORDSTATUS_LEFT] : 0);  		//total no. of LEFT student
	$ExportArr[$i][4] = (isset($ary[STUDENT_REGISTRY_RECORDSTATUS_SUSPEND]) ? $ary[STUDENT_REGISTRY_RECORDSTATUS_SUSPEND] : 0);  			//total no. of SUSPEND student
	$ExportArr[$i][5] = (isset($ary[STUDENT_REGISTRY_RECORDSTATUS_RESUME]) ? $ary[STUDENT_REGISTRY_RECORDSTATUS_RESUME] : 0);  		//total no. of RESUME student
	$ExportArr[$i][6] = $studentCount;  		//total students of class
	
	$totalNormal += $normal;
	$totalLeft += $ary[STUDENT_REGISTRY_RECORDSTATUS_LEFT];
	$totalSuspend += $ary[STUDENT_REGISTRY_RECORDSTATUS_SUSPEND];
	$totalResume += $ary[STUDENT_REGISTRY_RECORDSTATUS_RESUME];
	$sumTotal += $studentCount;
	
}

$ExportArr[$i][] = "";
$ExportArr[$i][] = $Lang['StudentRegistry']['Total'];
$ExportArr[$i][] = $totalNormal;
$ExportArr[$i][] = $totalLeft;
$ExportArr[$i][] = $totalSuspend;
$ExportArr[$i][] = $totalResume;
$ExportArr[$i][] = $sumTotal;


for($m=0; $m<2; $m++) {
	for($k=0; $k<sizeof($Lang['StudentRegistry']['Malaysia_Class_CSV_field'][$m]); $k++) {
		$exportColumn[$m][$k] = $Lang['StudentRegistry']['Malaysia_Class_CSV_field'][$m][$k];
	}
}
	
$export_content .= $lexport->GET_EXPORT_TXT_WITH_REFERENCE($ExportArr, $exportColumn, "\t", "\r\n", "\t", 0, "11");

intranet_closedb();

# Output the file to user browser
$lexport->EXPORT_FILE($filename, $export_content);

?>
