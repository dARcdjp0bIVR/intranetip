<?php
//editing by 
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccessright.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry.php");

intranet_auth();
intranet_opendb();

$laccessright = new libaccessright();

if(!$plugin['AccountMgmt_StudentRegistry'] || (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-View") && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-Manage"))) {
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$lsr = new libstudentregistry();

$sql = "SELECT ApplyDate FROM STUDENT_REGISTRY_STATUS WHERE UserID='$userID' AND IsCurrent=1";
$currentDate = $lsr->returnVector($sql);
//echo $currentDate[0].'/'.$applyDate;
if($currentDate[0]>$applyDate) {
	echo $Lang['StudentRegistry']['NewStatusApplyDateError'];
}

intranet_closedb();
?>