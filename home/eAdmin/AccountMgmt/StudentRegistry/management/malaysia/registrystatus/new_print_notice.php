<?php
#using : 

############# Change Log [Start]
#
#	Date:   2010-07-27 Thomas
#			update the layout of Edit Button to IP25 standard
#			Add javascript function - 'checkEdit' to the Edit button
#
############# Change Log [End]

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccessright.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry_ui.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");

intranet_auth();
intranet_opendb();

$laccessright = new libaccessright();

if(!$plugin['AccountMgmt_StudentRegistry'] || (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryStatus-View") && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryStatus-Manage"))) {
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$lsr = new libstudentregistry();
$lnotice = new libnotice();
$linterface = new interface_html();

$CurrentPageArr['StudentRegistry'] = 1;
$CurrentPage = "Mgmt_RegistryStatus";

$TAGS_OBJ[] = array($Lang['StudentRegistry']['StudentRegistryStatus'],);

$MODULE_OBJ['title'] = $Lang['Header']['Menu']['StudentRegistry'];

$MODULE_OBJ = $lsr->GET_MODULE_OBJ_ARR();

$lnotice->returnRecord($NoticeID);

# step information
switch($newstatus) {
	case 1 : $step2 = $Lang['StudentRegistry']['InputLeftInfo']; $title = $Lang['StudentRegistry']['LeftNotice']; break;
	case 2 : $step2 = $Lang['StudentRegistry']['InputSuspendInfo']; $title = $Lang['StudentRegistry']['SuspendNotice']; break;
	case 3 : $step2 = $Lang['StudentRegistry']['InputResumeInfo']; $title = $Lang['StudentRegistry']['ResumeNotice']; break;
	default : $newstatus=STUDENT_REGISTRY_RECORDSTATUS_LEFT; $step2 = $Lang['StudentRegistry']['InputLeftInfo']; $title = $Lang['StudentRegistry']['LeftNotice']; break;
}
$STEPS_OBJ[] = array($Lang['StudentRegistry']['SelectStudent'], 0);
$STEPS_OBJ[] = array($step2, 0);
$STEPS_OBJ[] = array($Lang['StudentRegistry']['PrintNotice'], 1);

# navigation bar
$PAGE_NAVIGATION[] = array($Lang['StudentRegistry']['ClassList'], "index.php?AcademicYearID=".$AcademicYearID);
$PAGE_NAVIGATION[] = array($navigation, "");

$linterface->LAYOUT_START($xmsg);

?>

<script language="javascript">
function goPrint() {
	newWindow("notice_print_preview.php?NoticeID=<?=$NoticeID?>&newstatus=<?=$newstatus?>",1);
}
</script>

<table width="98%" border="0" cellspacing="0" cellpadding="0">
	<tr> 
		<td class="main_content">
			<div class="navigation">
    			<?=$linterface->GET_NAVIGATION($PAGE_NAVIGATION)?>
			</div>
			 <br style="clear:both" />
			<div class="table_board">
<form name="searchForm" method="post" onSubmit="return submitSearch()">			
				<div>      
                        <?= $linterface->GET_STEPS_IP25($STEPS_OBJ) ?>
                        <br style="clear:both" />
                </div>
                <br>
						<div class="report_template_paper"> 
                          
                          <!-- Header -->
                          <div class="template_part" >
                            <p class="spacer"></p> 
                             <div class="template_part_content">
                         	 	<center><h2><?=$title?></h2></center>
                             </div>
                          </div>
                          <br />
                          <!-- Content -->
                          <div class="template_part" >
                            <p class="spacer"></p> 
                       	    <div class="template_part_content">
                         	    <div  class="added_content">
                         	    <?=$lnotice->Description?>
                         	    <p align="center"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eOffice/print_scissors.gif"></p>
                         	    <?=$lnotice->ReplySlipContent?>
                         	    </div>
                         	  </div>
                         	  <p class="spacer"></p>
                         	  <p align="center">
                         	  	<?=$linterface->GET_ACTION_BTN($button_preview, "button", "goPrint()")?>&nbsp;
                         	  	<?=$linterface->GET_ACTION_BTN($button_finish, "button", "self.location.href='index.php'")?>
                         	  </p>
                        </div>
			</div>
			<p>&nbsp;</p>
		</td>
	</tr>
</table>
<input type="hidden" name="newstatus" id="newstatus" value="<?=$newstatus?>">
</form>
<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>