<?php
//editing by 
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libaccessright.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry.php");

intranet_auth();
intranet_opendb();

$laccessright = new libaccessright();

if(!$plugin['AccountMgmt_StudentRegistry'] || (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryStatus-View") && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryStatus-Manage"))) {
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$lsr = new libstudentregistry();
$linterface = new interface_html();

$conds = "";

if($yearid!="")
	$conds .= " AND yc.AcademicYearID='$yearid'";
	
if($stdno!="")
	$conds .= " AND srs.STUD_ID='$stdno'";

if($stdname!="") {
	//$stdname = iconv("big5","UTF-8",$stdname);
	$stdname = urldecode($stdname);
	$conds .= " AND (USR.ChineseName LIKE '%$stdname%' OR USR.EnglishName LIKE '%$stdname%')";
}

if($newstatus==STUDENT_REGISTRY_RECORDSTATUS_LEFT) {
	$conds .= " AND (st.RecordStatus!='$newstatus' OR st.RecordStatus IS NULL)";
} else if($newstatus==STUDENT_REGISTRY_RECORDSTATUS_SUSPEND) {
	$conds .= " AND (st.RecordStatus='".STUDENT_REGISTRY_RECORDSTATUS_RESUME."' OR st.RecordStatus IS NULL)";
} else if($newstatus==STUDENT_REGISTRY_RECORDSTATUS_RESUME) {
	$conds .= " AND st.RecordStatus=".STUDENT_REGISTRY_RECORDSTATUS_SUSPEND;
} 

$sql = "SELECT 
			IFNULL(USR.EnglishName, '---'), 
			IFNULL(USR.ChineseName, '---'),
			srs.STUD_ID,
			IFNULL(st.RecordStatus,'".$Lang['StudentRegistry']['RegistryStatus_Approved']."') as status,
			IF(st.Reason IS NULL OR st.Reason='','---',IF(st.Reason!=0,
				CASE st.Reason
					WHEN 1 THEN '".$Lang['StudentRegistry']['ReasonAry'][1]."'
					WHEN 2 THEN '".$Lang['StudentRegistry']['ReasonAry'][2]."'
					WHEN 3 THEN '".$Lang['StudentRegistry']['ReasonAry'][3]."'
					WHEN 4 THEN '".$Lang['StudentRegistry']['ReasonAry'][4]."'
					WHEN 5 THEN '".$Lang['StudentRegistry']['ReasonAry'][5]."'
					WHEN 6 THEN '".$Lang['StudentRegistry']['ReasonAry'][6]."'
					WHEN 7 THEN '".$Lang['StudentRegistry']['ReasonAry'][7]."'
					WHEN 8 THEN '".$Lang['StudentRegistry']['ReasonAry'][8]."'
					WHEN 9 THEN '".$Lang['StudentRegistry']['ReasonAry'][9]."'
					WHEN 10 THEN '".$Lang['StudentRegistry']['ReasonAry'][10]."'
					ELSE '---' END 
			, st.ReasonOthers)) as reason,
			IFNULL(ApplyDate,'---') as applyDate,
			IFNULL(NoticeID,'---') as noticeID,
			IFNULL(ApplyDate,'---') as applyForm,
			IFNULL(LEFT(st.DateModified,10),'---') as lastUpdated,
			CONCAT('<input type=\'checkbox\' name=\'userID[]\' id=\'userID[]\' value=\'',USR.UserID,'\'>') as checkbox,
			USR.UserID,
			st.DateModified,
			st.ModifyBy
		FROM
			INTRANET_USER USR LEFT OUTER JOIN
			YEAR_CLASS_USER ycu ON (ycu.UserID=USR.UserID) LEFT OUTER JOIN
			YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID) LEFT OUTER JOIN
			YEAR y ON (y.YearID=yc.YearID) LEFT OUTER JOIN
			STUDENT_REGISTRY_STUDENT srs ON (srs.UserID=USR.UserID) LEFT OUTER JOIN 
			STUDENT_REGISTRY_STATUS st ON (st.UserID=USR.UserID AND st.IsCurrent=1)
		WHERE
			USR.RecordType=2 $conds	
		GROUP BY 
			USR.UserID	 
		";
		
$result = $lsr->returnArray($sql,9);
$returnStr = '
<table class="common_table_list">
    <thead>
      <tr>
        <th class="num_check">#</th>
        <th> '.$i_UserEnglishName.'</th>
        <th> '.$i_UserChineseName.'</th>
		<th> '.$Lang['StudentRegistry']['StudentNo'].'</th>
        <th>'.$Lang['StudentRegistry']['RegistryStatus'].'</a></th>
        <th>'.$Lang['StudentRegistry']['Reason'].'</th>
        <th>'.$Lang['StudentRegistry']['ApplyDate'].'</th>
        <th><span class="field_title_short">'.$Lang['StudentRegistry']['Notice'].'</span></th>
        <th>'.$Lang['StudentRegistry']['ApplyYear'].'</th>
        <th>'.$Lang['StudentRegistry']['ApplyForm'].'</th>
        <th>'.$Lang['StudentRegistry']['LastUpdated'].'</a></th>
        <th class="num_check">&nbsp;</th>
      </tr>';
   
for($i=0; $i<sizeof($result); $i++) {
	list($engname, $chiname, $stud_id, $status, $reason, $applyDate, $noticeID, $applyForm, $lastUpdated, $checkbox, $userid, $dateModified, $modifyBy) = $result[$i];
	if($lastUpdated!='---') {
		$userinfo = $lsr->getUserInfoByID($modifyBy);
		$name = " (".Get_Lang_Selection($userinfo['ChineseName'], $userinfo['EnglishName']).")";
	} else {
		$name = "";
	}
	switch($status) {
		case 1 : $css = "row_resign"; break;
		case 2 : $css = "row_temp_resign"; break;
		case 3 : $css = "row_signback"; break;
		default : $css = ""; break;
	}
	if($applyDate!="---") {
		$yearInfo = getAcademicYearInfoAndTermInfoByDate($applyDate);
		$applyYear = $yearInfo[1];
		$applyYearID = $yearInfo[0];
		$applyForm = $lsr->getYearNameByYearClassID($applyYearID, $userid);
		
	} else {
		$applyYear = "---";
		$applyForm = "---";
	}
	
	
	$returnStr .= '      
      <tr class="'.$css.'">
        <td>'.($i+1).'</td>
        <td><a href="../information/view.php?userID='.$userid.'">'.$engname.'</a></td>
        <td><a href="../information/view.php?userID='.$userid.'">'.$chiname.'</a></td>
		<td>'.($stud_id!=""?$stud_id:"---").'</td>
        <td>'.(is_numeric($status) ? $lsr->getRegistryStatusByStatusID($status) : $status).'</td>
        <td>'.$reason.'</td>
        <td>'.$applyDate.'</td>
        <td>'.(($noticeID!="---") ? "<a href=javascript:newWindow('/home/eService/notice/sign.php?NoticeID=".$noticeID."',1);>[".$Lang['StudentRegistry']['View']."]</a>" : "---").'</td>
        <td>'.$applyYear.'</td>
        <td>'.$applyForm.'</td>
        <td>'.$lastUpdated.$name.'</td>
        <td><input type="checkbox" name="userID[]" id="userID" value="'.$userid.'"/></td>
      </tr>';
}     

if(sizeof($result)==0) {
	$returnStr .= '<tr><td colspan="11" align="center" height="40">'.$Lang['StudentRegistry']['NoStudentRecord'].'</td></tr>';
}

$returnStr .= '
    </thead>
    <tbody>
    </tbody>
  </table>
				<br>
				<div class="edit_bottom">
   			    	<p class="spacer"></p>
        			    '.$linterface->GET_ACTION_BTN($button_continue, "button", "checkForm()").'&nbsp; 
       			      '.$linterface->GET_ACTION_BTN($button_cancel, "button", "").' 
                </div>';

intranet_closedb();
echo $returnStr;
?>