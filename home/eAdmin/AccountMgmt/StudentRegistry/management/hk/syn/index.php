<?php
#using :

############# Change Log [Start]
/*
 *  2020-04-06 Cameron
 *      - add $returnMsg to show synchronization result
 *      
 *  2019-11-19 Cameron
 *      - add scope of all / form / class / specific student
 *
 *  2019-10-29 Cameron
 *      - create this file
 */
############# Change Log [End]

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libaccessright.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry_ui.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$laccessright = new libaccessright();
if(!$plugin['AccountMgmt_StudentRegistry'] || (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] || !$sys_custom['StudentRegistry']['Kentville'])) {
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$lsr = new libstudentregistry();
$linterface = new interface_html();


$lclass = new libclass();

$currentAcademicYearID = Get_Current_Academic_Year_ID();
$formSelection = $lclass->getSelectLevel("name='FormName' id='FormName' ",$selected="",$ValueIsID=false, $firstOptionName='', $currentAcademicYearID);
$classSelection = $lclass->getSelectClass("name='ClassName' id='ClassName' ");
$studentSelection = getSelectByArray(array(),"name='StudentID' id='StudentID'");



$CurrentPageArr['StudentRegistry'] = 1;
$CurrentPage = "Mgmt_RegistrySynchronization";

$TAGS_OBJ[] = array($Lang['StudentRegistry']['Synchronization'], "index.php");

$MODULE_OBJ['title'] = $Lang['Header']['Menu']['StudentRegistry'];

$MODULE_OBJ = $lsr->GET_MODULE_OBJ_ARR();

$returnMsg = $Lang['StudentRegistry']['SynchronizationResult'][$returnMsgKey];
$linterface->LAYOUT_START($returnMsg);

?>
<script language="javascript">

    function checkForm(obj)
    {
        var scope = $("input:radio[name='Scope']:checked").val();
        if (scope == '2' && !check_text(obj.FormName,'<?php echo $Lang['General']['JS_warning']['SelectAForm'];?>')) {
            return false;
        }
        else if(scope == '3' && !check_text(obj.ClassName,'<?php echo $Lang['General']['JS_warning']['SelectClass'];?>')) {
            return false;
        }
        else if(scope == '4' && !check_text(obj.StudentID,'<?php echo $Lang['General']['JS_warning']['SelectStudent'];?>')) {
            return false;
        }
        return true;
    }

    $(document).ready(function(){

        var isLoading = true;
        var loadingImg = '<?php echo $linterface->Get_Ajax_Loading_Image(); ?>';

        $("input:radio[name='Scope']").change(function(){
           var scope = $("input:radio[name='Scope']:checked").val();
           switch(scope){
               case '1':
                   $('#SpanForm').css('display','none');
                   $('#SpanClass').css('display','none');
                   $('#SpanStudent').css('display','none');
                   break;
               case '2':
                   $('#SpanForm').css('display','');
                   $('#SpanClass').css('display','none');
                   $('#SpanStudent').css('display','none');
                   break;
               case '3':
                   $('#SpanForm').css('display','none');
                   $('#SpanClass').css('display','');
                   $('#SpanStudent').css('display','none');
                   break;
               case '4':
                   $('#SpanForm').css('display','none');
                   $('#SpanClass').css('display','');
                   $('#SpanStudent').css('display','');
                   break;
           }
        });

        $('#ClassName').change(function(){
            if ($("input:radio[name='Scope']:checked").val() == "4") {
                isLoading = true;
                $('#StudentNameSpan').html(loadingImg);

                $.ajax({
                    dataType: "json",
                    type: "POST",
                    url: 'ajax.php',
                    data: {
                        'action': 'getStudentNameByClass',
                        'ClassName': $('#ClassName').val()
                    },
                    success: update_student_list,
                    error: show_ajax_error
                });
            }
        });

        $('#btnSubmit').click(function(e) {
            e.preventDefault();
            if (checkForm(document.form1)) {
                $('#form1').submit();
            }
        });

    });

    function update_student_list(ajaxReturn) {
        if (ajaxReturn != null && ajaxReturn.success){
            $('#StudentNameSpan').html(ajaxReturn.html);
            isLoading = false;
        }
    }

    function show_ajax_error() {
        alert('<?php echo $Lang['General']['AjaxError'];?>');
    }

</script>

<form name="form1" id="form1" method="post" action="sync_registry_update.php">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="form_table_v30">
                    <tr valign="top">
                        <td width="20%" valign="top" nowrap="nowrap" class="field_title_short"><?php echo $Lang['StudentRegistry']['SynchronizeScope'];?></td>
                        <td class="tabletext">
                            <input type="radio" name="Scope" id="ScopeAll" value="1"><label for="ScopeAll"><?php echo $Lang['Btn']['All'];?></label><br>
                            <input type="radio" name="Scope" id="ScopeForm" value="2"><label for="ScopeForm"><?php echo $Lang['General']['Form'];?></label><br>
<!--                            <input type="radio" name="Scope" id="ScopeClass" value="3"><label for="ScopeClass">--><?php //echo $Lang['Header']['Menu']['Class'];?><!--</label><br>-->
                            <input type="radio" name="Scope" id="ScopeStudent" value="4" checked><label for="ScopeStudent"><?php echo $Lang['StudentRegistry']['SpecificStudent'];?></label>
                        </td>
                        <td class="tabletext">
                            <span id="SpanForm" style="display:none;"><?php echo $Lang['General']['Form'];?>&nbsp;<?php echo $formSelection;?></span>&nbsp;
                            <span id="SpanClass" style="display:'';"><?php echo $Lang['Header']['Menu']['Class'];?>&nbsp;<?php echo $classSelection;?></span>&nbsp;
                            <span id="SpanStudent" style="display:'';"><?php echo $Lang['General']['StudentName'];?>&nbsp;<span id="StudentNameSpan"><?php echo $studentSelection;?></span></span>
                        </td>
                    </tr>

                    <tr>
                        <td align="right" colspan="3">
                            <div class="edit_bottom">
                                <?php echo $linterface->GET_ACTION_BTN($button_submit, "button","","btnSubmit", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");?>&nbsp;
                            </div>
                        </td>
                    </tr>
                </table>

            </td>
        </tr>
    </table>
</form>

</body>
<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>