<?php
/*
 *  Using:
 *
 *  Should open in utf-8
 *
 *  Purpose: sync student registry info to intranet user account and their guardians
 *
 *  2020-04-06 Cameron
 *      - pass $returnMsgKey to index.php instead of showing result here
 *      - return SyncSuccess if there's no record to sync
 *      
 *  2019-11-19 Cameron
 *      - support sync all students / by form / by specific student
 *
 *  2019-10-23 Cameron
 *      - create this file, test for one student first
 */

$enableDebug = false;
//$enableDebug = true;

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccessright.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry_kv.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry_ui.php");
include_once($PATH_WRT_ROOT."includes/libeclass40.php");

intranet_auth();
intranet_opendb();

$laccessright = new libaccessright();

if (!$plugin['AccountMgmt_StudentRegistry'] || !$sys_custom['StudentRegistry']['Kentville'] || !$_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] ) {
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit;
}

$ldb = new libdb();
$lsr = new libstudentregistry_kv();

###################################################################################################

$studentIDAry = array();
$currentAcademicYearID = Get_Current_Academic_Year_ID();
$extraJoin = '';

$scope = IntegerSafe($_POST['Scope']);

switch ($scope) {
    case 1:     // all students
        $condition = '';
        break;
    case 2:     // by form
        $formName = standardizeFormPostValue($_POST['FormName']);
        $condition = " AND y.YearName='".$formName."'";
        $extraJoin = "INNER JOIN YEAR y ON y.YearID=yc.YearID ";
        break;
    case 3:     // by class
        $className = standardizeFormPostValue($_POST['ClassName']);
        $condition = " AND yc.ClassTitleEN='".$className."'";
        break;
    case 4:     // by specific student
        $studentID = IntegerSafe($_POST['StudentID']);
        $studentIDAry[] = $studentID;
        break;
}

// get students of current year
if ($scope != 4) {
    $sql = "SELECT u.UserID, 
                u.EnglishName, 
                u.ChineseName,
                yc.ClassTitleEN,
                ycu.ClassNumber
        FROM 
                INTRANET_USER u
                INNER JOIN YEAR_CLASS_USER ycu on ycu.UserID=u.UserID
                INNER JOIN YEAR_CLASS yc ON yc.YearClassID=ycu.YearClassID
                INNER JOIN STUDENT_REGISTRY_STUDENT s ON s.UserID=u.UserID
                {$extraJoin}
        WHERE
                yc.AcademicYearID='" . $currentAcademicYearID . "'
                AND u.RecordStatus=1
                {$condition}
                ORDER BY yc.ClassTitleEN, ycu.ClassNumber";
    $studentAry = $ldb->returnResultSet($sql);
    $studentIDAry = Get_Array_By_Key($studentAry, 'UserID');
    if ($enableDebug) {
        echo "studentAry <br>";
        echo "$sql <br>";
        debug_pr($studentAry);
        debug_pr($studentIDAry);
    }
}


if (count($studentIDAry)){
    $result = $lsr->syncStudentRegistryToAccount($studentIDAry);

    if ($result) {
        $returnMsgKey = 'SyncSuccess';
    }
    else {
        $returnMsgKey = 'SyncFail';
    }
}
else {
    $returnMsgKey = 'SyncSuccess';
}

intranet_closedb();

header('location: index.php?returnMsgKey='. $returnMsgKey);
?>
