<?
/*
 * Log
 *
 * Description: output json format data
 *
 *  2019-08-29 [Cameron] disable checking duplicate email as there could be the same parent for multiple children
 *
 *  2019-05-09 [Cameron] retrieve preset code list for import
 *  
 *  2019-03-13 [Cameron] create this file
 */

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccessright.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry_kv.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry_ui.php");
include_once($PATH_WRT_ROOT."includes/json.php");


$laccessright = new libaccessright();

if (!$plugin['AccountMgmt_StudentRegistry'] || !$sys_custom['StudentRegistry']['Kentville'] || (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] && !$_SESSION['ParentFillOnlineReg'])) {
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit;
}


intranet_auth();
intranet_opendb();

$characterset = 'utf-8';

header('Content-Type: text/html; charset=' . $characterset);

if($_SESSION['ParentFillOnlineReg']) {
    $linterface = new libstudentregistry_ui("popup.html");
}
else {
    $linterface = new libstudentregistry_ui();
}

$ljson = new JSON_obj();

$json['success'] = false;
$x = '';
$action = $_GET['action'] ? $_GET['action'] : $_POST['action'];
$remove_dummy_chars = ($junior_mck) ? false : true; // whether to remove new line, carriage return, tab and back slash

switch ($action) {
    
//     case 'addBSRow':
//         $rowNr = IntegerSafe(trim($_POST['rowNr']));
//         $bsRow = $linterface->addSiblingRow($rowNr);
//         $x = $bsRow;
//         $json['success'] = true;
//         break;
        
    case 'checkFormBeforeSave':
        $studentID = IntegerSafe(trim($_POST['studentID']));
        $id_no = trim($_POST['id_no']);
        $guardian_email1= trim($_POST['guardian_email1']);
        $guardian_email2= trim($_POST['guardian_email2']);

        $lsr = new libstudentregistry_kv();
        if (!empty($id_no)) {
            $isDuplicateCertificateNo = $lsr->isDuplicateCertificateNo($studentID, $id_no);
            if ($isDuplicateCertificateNo) {
                $x .= "<li>".$Lang['StudentRegistry']['StudInfo']." - ". $Lang['StudentRegistry']['ID_DocumentNo'] . " - " . $Lang['StudentRegistry']['alreadyExist'] ."</li>";
            }
        }

        // 2019-08-29: disable this checking as there could be the same parent for multiple children
/*
        for ($i=1; $i<=2; $i++) {
            $guardian_email = ${"guardian_email$i"};
            if (!empty($guardian_email)) {
                $isDuplicateEmail = $lsr->isDuplicateEmail($studentID, $guardian_email);
                if ($isDuplicateEmail) {
                    $x .= "<li>".$Lang['StudentRegistry']['StudInfo']." - ". $Lang['StudentRegistry']['EMailAddress'] . " - " . $guardian_email. " - ". $Lang['StudentRegistry']['alreadyExist'] ."</li>";
                }
            }
        }
*/
        $json['success'] = true;
        
        break;
        
    case 'getPlaceOfBirth':
        $x = $linterface->getPresetCodeList('PLACEOFBIRTH');
        $json['success'] = true;
        break;
        
    case 'getNationality':
        $x = $linterface->getPresetCodeList('NATIONALITY');
        $json['success'] = true;
        break;

    case 'getEthnicity':
        $x = $linterface->getPresetCodeList('ETHNICITY');
        $json['success'] = true;
        break;

    case 'getHomeLang':
        $x = $linterface->getPresetCodeList('FAMILY_LANG');
        $json['success'] = true;
        break;
        
    case 'getDistrict':
        $x = $linterface->getPresetCodeList('SUBDISTRICT');
        $json['success'] = true;
        break;
        
    case 'getSiblingRelationship':
        $x = $linterface->getPresetCodeList('SIBLINGS');
        $json['success'] = true;
        break;
        
    case 'getSchoolLevel':
        $x = $linterface->getPresetCodeList('SCHOOLLEVEL');
        $json['success'] = true;
        break;
        
    case 'getRelationship':
        $x = $linterface->getPresetCodeList('RELATIONSHIP');
        $json['success'] = true;
        break;
}

if ($remove_dummy_chars) {
    $x = remove_dummy_chars_for_json($x);
}

$json['html'] = $x;

echo $ljson->encode($json);

intranet_closedb();
?>