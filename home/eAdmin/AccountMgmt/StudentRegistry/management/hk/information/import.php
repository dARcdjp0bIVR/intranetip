<?php
// using : 

###########################################
#
#	Date:	2013-06-27
#
###########################################

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libaccessright.php");
intranet_auth();
intranet_opendb();

$laccessright = new libaccessright();
if(!$plugin['AccountMgmt_StudentRegistry'] || (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-View") && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-Manage")) && !$_SESSION['ParentFillOnlineReg']) {
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$linterface 	= new interface_html();
$lsr = new libstudentregistry();
//$csvFile = "<a class='tablelink' href='". GET_CSV("student_reg_sample.csv") ."'>". $Lang['General']['ClickHereToDownloadSample'] ."</a>";
$csvFile = "<a class='tablelink' href='import_template.php'>". $Lang['General']['ClickHereToDownloadSample'] ."</a>";
$CurrentPageArr['StudentRegistry'] = 1;
$CurrentPage = "Mgmt_RegistryInfo";
$TAGS_OBJ[] = array($Lang['Menu']['AccountMgmt']['StudentRegistryInfo'], "index.php");
$MODULE_OBJ['title'] = $Lang['Header']['Menu']['StudentRegistry'];
$MODULE_OBJ = $lsr->GET_MODULE_OBJ_ARR();

$STEPS_OBJ[] = array($i_general_select_csv_file, 1);
$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 0);
$STEPS_OBJ[] = array($i_general_imported_result, 0);

# navigation bar
$linterface->LAYOUT_START();

$title_field = $intranet_session_language=="en" ? "NameEng" : "NameChi";
# nationality_table
$sql = "select Code, $title_field from PRESET_CODE_OPTION where CodeType='NATIONALITY' and RecordStatus=1 order by DisplayOrder";
$result = $lsr->returnArray($sql);
$this_row = "";
foreach($result as $k=>$d)
{
	list($code, $title) = $d;
	$this_row .= "<tr><td bgcolor='FFFFFF'>". $code."</td><td bgcolor='FFFFFF'>". $title."</td></tr>";
}
$nationality_table = "<table id='nationality_table' border='0' cellpadding=2 cellspacing=1 bgcolor='000000' style='display:none'><tr><td bgcolor='FFFFFF'><b>". $Lang['AccountMgmt']['UserExtraInfoCategoryCode'] ."</b></td><td bgcolor='FFFFFF'><b>". $Lang['StudentRegistry']['Nationality'] ."</b></td></tr>". $this_row ."</table>";

# ethnicity_table
$sql = "select Code, $title_field from PRESET_CODE_OPTION where CodeType='ETHNICITY' and RecordStatus=1 order by DisplayOrder";
$result = $lsr->returnArray($sql);
$this_row = "";
foreach($result as $k=>$d)
{
	list($code, $title) = $d;
	$this_row .= "<tr><td bgcolor='FFFFFF'>". $code."</td><td bgcolor='FFFFFF'>". $title."</td></tr>";
}
$ethnicity_table = "<table id='ethnicity_table' border='0' cellpadding=2 cellspacing=1 bgcolor='000000' style='display:none'><tr><td bgcolor='FFFFFF'><b>". $Lang['AccountMgmt']['UserExtraInfoCategoryCode'] ."</b></td><td bgcolor='FFFFFF'><b>". $Lang['StudentRegistry']['Race'] ."</b></td></tr>". $this_row ."</table>";

# id_doc_type_table
$sql = "select Code, $title_field from PRESET_CODE_OPTION where CodeType='IDENT_DOC_TYPE' and RecordStatus=1 order by DisplayOrder";
$result = $lsr->returnArray($sql);
$this_row = "";
foreach($result as $k=>$d)
{
	list($code, $title) = $d;
	$this_row .= "<tr><td bgcolor='FFFFFF'>". $code."</td><td bgcolor='FFFFFF'>". $title."</td></tr>";
}
$id_doc_type_table = "<table id='id_doc_type_table' border='0' cellpadding=2 cellspacing=1 bgcolor='000000' style='display:none'><tr><td bgcolor='FFFFFF'><b>". $Lang['AccountMgmt']['UserExtraInfoCategoryCode'] ."</b></td><td bgcolor='FFFFFF'><b>". $Lang['StudentRegistry']['ID_DocumentType'] ."</b></td></tr>". $this_row ."</table>";

# home_lang_table
$sql = "select Code, $title_field from PRESET_CODE_OPTION where CodeType='FAMILY_LANG' and RecordStatus=1 order by DisplayOrder";
$result = $lsr->returnArray($sql);
$this_row = "";
foreach($result as $k=>$d)
{
	list($code, $title) = $d;
	$this_row .= "<tr><td bgcolor='FFFFFF'>". $code."</td><td bgcolor='FFFFFF'>". $title."</td></tr>";
}
$home_lang_table = "<table id='home_lang_table' border='0' cellpadding=2 cellspacing=1 bgcolor='000000' style='display:none'><tr><td bgcolor='FFFFFF'><b>". $Lang['AccountMgmt']['UserExtraInfoCategoryCode'] ."</b></td><td bgcolor='FFFFFF'><b>". $Lang['StudentRegistry']['HomeLang'] ."</b></td></tr>". $this_row ."</table>";

# religion_table
$sql = "select Code, $title_field from PRESET_CODE_OPTION where CodeType='RELIGION' and RecordStatus=1 order by DisplayOrder";
$result = $lsr->returnArray($sql);
$this_row = "";
foreach($result as $k=>$d)
{
	list($code, $title) = $d;
	$this_row .= "<tr><td bgcolor='FFFFFF'>". $code."</td><td bgcolor='FFFFFF'>". $title."</td></tr>";
}
$religion_table = "<table id='religion_table' border='0' cellpadding=2 cellspacing=1 bgcolor='000000' style='display:none'><tr><td bgcolor='FFFFFF'><b>". $Lang['AccountMgmt']['UserExtraInfoCategoryCode'] ."</b></td><td bgcolor='FFFFFF'><b>". $Lang['StudentRegistry']['Religion'] ."</b></td></tr>". $this_row ."</table>";

# district_table
$sql = "select Code, $title_field from PRESET_CODE_OPTION where CodeType='DISTRICT' and RecordStatus=1 order by DisplayOrder";
$result = $lsr->returnArray($sql);
$this_row = "";
foreach($result as $k=>$d)
{
	list($code, $title) = $d;
	$this_row .= "<tr><td bgcolor='FFFFFF'>". $code."</td><td bgcolor='FFFFFF'>". $title."</td></tr>";
}
$district_table = "<table id='district_table' border='0' cellpadding=2 cellspacing=1 bgcolor='000000' style='display:none'><tr><td bgcolor='FFFFFF'><b>". $Lang['AccountMgmt']['UserExtraInfoCategoryCode'] ."</b></td><td bgcolor='FFFFFF'><b>". $Lang['StudentRegistry']['AddressField']['District'] ."</b></td></tr>". $this_row ."</table>";

# area_table
$this_row = "";
for($i=0;$i<sizeof($Lang['StudentRegistry']['AreaOptions']);$i++)
{
	$this_row .= "<tr><td bgcolor='FFFFFF'>". ($i+1) ."</td><td bgcolor='FFFFFF'>". $Lang['StudentRegistry']['AreaOptions'][$i]."</td></tr>";
}
$area_table = "<table id='area_table' border='0' cellpadding=2 cellspacing=1 bgcolor='000000' style='display:none'><tr><td bgcolor='FFFFFF'><b>". $Lang['AccountMgmt']['UserExtraInfoCategoryCode'] ."</b></td><td bgcolor='FFFFFF'><b>". $Lang['StudentRegistry']['AddressField']['Area'] ."</b></td></tr>". $this_row ."</table>";

# GuardianStudentRelationship_table
$this_row = "";
$this_row .= "<tr><td bgcolor='FFFFFF'>1</td><td bgcolor='FFFFFF'>". $Lang['StudentRegistry']['Father'] ."</td></tr>";
$this_row .= "<tr><td bgcolor='FFFFFF'>2</td><td bgcolor='FFFFFF'>". $Lang['StudentRegistry']['Mother'] ."</td></tr>";
$this_row .= "<tr><td bgcolor='FFFFFF'>0</td><td bgcolor='FFFFFF'>". $Lang['General']['Others'] ."</td></tr>";
$GuardianStudentRelationship_table = "<table id='GuardianStudentRelationship_table' border='0' cellpadding=2 cellspacing=1 bgcolor='000000' style='display:none'><tr><td bgcolor='FFFFFF'><b>". $Lang['AccountMgmt']['UserExtraInfoCategoryCode'] ."</b></td><td bgcolor='FFFFFF'><b>". $Lang['StudentRegistry']['GuardianStudentRelationship'] ."</b></td></tr>". $this_row ."</table>";

# UserTitle_table
$this_row = "";
$this_row .= "<tr><td bgcolor='FFFFFF'>1</td><td bgcolor='FFFFFF'>". $i_title_mr ."</td></tr>";
$this_row .= "<tr><td bgcolor='FFFFFF'>2</td><td bgcolor='FFFFFF'>". $i_title_mrs ."</td></tr>";
$this_row .= "<tr><td bgcolor='FFFFFF'>3</td><td bgcolor='FFFFFF'>". $i_title_ms ."</td></tr>";
$UserTitle_table = "<table id='UserTitle_table' border='0' cellpadding=2 cellspacing=1 bgcolor='000000' style='display:none'><tr><td bgcolor='FFFFFF'><b>". $Lang['AccountMgmt']['UserExtraInfoCategoryCode'] ."</b></td><td bgcolor='FFFFFF'><b>". $i_UserTitle ."</b></td></tr>". $this_row ."</table>";

?>

<script language="javascript">
<!--
function click_code(t)
{
	var s = eval("document.getElementById('"+ t +"').style.display;");
	if(s=="none")
		eval("document.getElementById('"+ t +"').style.display='';");
	else
		eval("document.getElementById('"+ t +"').style.display='none';");
}
//-->
</script>
<div id="ToolMenu2" style="position:absolute; width=0px; height=0px; visibility:hidden; z-index:999999"></div>

<form method="POST" name="frm1" id="frm1" action="import_check.php" enctype="multipart/form-data" onsubmit="return CheckForm()">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td colspan="2" align='right'><?=$linterface->GET_SYS_MSG("",$xmsg);?></td>
</tr>
<tr>
	<td colspan="2"><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
</tr>
<tr>
	<td colspan="2"><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
</tr>
<tr>
	<td colspan="2">
        <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
            <tr> 
            	<td><br />
					<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">			
						<tr>
							<td class="formfieldtitle" align="left"><?=$Lang['General']['SourceFile']?> <span class="tabletextremark"><?=$Lang['General']['CSVFileFormat']?></span></td>
							<td class="tabletext"><input class="file" type="file" id="userfile" name="userfile"></td>
						</tr>
						<tr>
							<td class="formfieldtitle" align="left"><?=$Lang['General']['CSVSample']?> </td>
							<td class="tabletext"><?=$csvFile?></td>
						</tr>
						<tr>
							<td class="formfieldtitle" align="left"><?=$Lang['StudentRegistry']['CodeFormat']?></td>
							<td class="tabletext">
								STU_NationalityCode: <a href="javascript:click_code('nationality_table');"><?=$Lang['StudentRegistry']['Nationality']?></a><br>
								<?=$nationality_table?>
								
								STU_EthnicityCode: <a href="javascript:click_code('ethnicity_table');"><?=$Lang['StudentRegistry']['Race']?></a><br>
								<?=$ethnicity_table?>
								
								STU_IDTypeCode: <a href="javascript:click_code('id_doc_type_table');"><?=$Lang['StudentRegistry']['ID_DocumentType']?></a><br>
								<?=$id_doc_type_table?>
								
								STU_HomeLangCode: <a href="javascript:click_code('home_lang_table');"><?=$Lang['StudentRegistry']['HomeLang']?></a><br>
								<?=$home_lang_table?>
								
								STU_ReligionCode, GRD_ReligionCode1, GRD_ReligionCode2: <a href="javascript:click_code('religion_table');"><?=$Lang['StudentRegistry']['Religion']?></a><br>
								<?=$religion_table?>
								
								STU_AddressDistrictCode, GRD_AddressDistrictCode1, GRD_AddressDistrictCode2: <a href="javascript:click_code('district_table');"><?=$Lang['StudentRegistry']['AddressField']['District']?></a><br>
								<?=$district_table?>
								
								STU_AddressAreaCode, GRD_AddressAreaCode1, GRD_AddressAreaCode2: <a href="javascript:click_code('area_table');"><?=$Lang['StudentRegistry']['AddressField']['Area']?></a><br>
								<?=$area_table?>
								
								GRD_TypeCode1, GRD_TypeCode2: <a href="javascript:click_code('GuardianStudentRelationship_table');"><?=$Lang['StudentRegistry']['GuardianStudentRelationship']?></a><br>
								<?=$GuardianStudentRelationship_table?>
							
								OtherConact_TitleCode: <a href="javascript:click_code('UserTitle_table');"><?=$i_UserTitle?></a><br>
								<?=$UserTitle_table?>
								
							</td>
						</tr>
						
					</table>
				</td>
	        </tr>
	    </table>
	</td>
</tr>
<tr>
	<td colspan="2">        
		<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
			<tr>
                <td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
            </tr>
            <tr>
				<td align="center">
					<?= $linterface->GET_ACTION_BTN($button_import, "submit") ?>&nbsp;
					<?= $linterface->GET_ACTION_BTN($button_back, "button", "window.location='index.php'");?>
				</td>
			</tr>
        </table>                                
	</td>
</tr>
</table>  
</form>
<?
 $linterface->LAYOUT_STOP();
?>

