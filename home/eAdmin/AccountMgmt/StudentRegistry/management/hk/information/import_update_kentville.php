<?php 
/*
 *  Using:
 *  
 *  2019-04-11 Cameron
 *      - create this file
 */

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccessright.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry_kv.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libeclass40.php");

intranet_auth();
intranet_opendb();

$laccessright = new libaccessright();

if (!$plugin['AccountMgmt_StudentRegistry'] || !$sys_custom['StudentRegistry']['Kentville'] || !$_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"]) {
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$lsr = new libstudentregistry_kv();
$limport = new libimporttext();
$lo = new libfilesystem();

//$csvFile = $intranet_root."/file/student_registry/initial_import_data.csv";
$name = $_FILES['upload_file']['name'];
$ext = strtoupper($lo->file_ext($name));

if(!($ext == ".CSV" || $ext == ".TXT"))
{
    intranet_closedb();
    header("location: import_kentville.php?retMsg=0");
    exit();
}
$data = $limport->GET_IMPORT_TXT($upload_file);

//if (is_file($csvFile)) {
//    $data = $limport->GET_IMPORT_TXT($csvFile);
//}

if(is_array($data)) {
    $csv_header = array_shift($data);
}

$resultAry = array();
$nrUpdate = 0;

foreach ((array)$data as $_row=>$_col) {
    $userAry = array();
    $sql = "SELECT UserID, UserEmail FROM INTRANET_USER WHERE UserLogin='".$_col[2]."'";
    $rs = $lsr->returnResultSet($sql);
    if (count($rs)) {
        $nrUpdate++;
        $studentID = $rs[0]['UserID'];
        $studentEmail = $rs[0]['UserEmail'];
        
        # INTRANET_USER
        if (!empty($_col[7])) {
            $placeOfBirth = $lsr->getPresetCodeEnglishName('PLACEOFBIRTH',$_col[7]);
            if ($placeOfBirth) {
                $placeOfBirthCode = $_col[7];
                $placeOfBirthOthers = '';
            }
            else {
                $placeOfBirthCode = '999';
                $placeOfBirthOthers = $_col[7];
                $placeOfBirth = $_col[7];
            }
        }
        else {
            $placeOfBirth = '';
            $placeOfBirthOthers = '';
        }
        
        $userAry['LastName'] = $_col[3];
        $userAry['FirstName'] = $_col[4];
        $userAry['ChineseName'] = $_col[5];
        $userAry['DateOfBirth'] = convertDateTimeToStandardFormat($_col[6],false);
        $userAry['PlaceOfBirth'] = $placeOfBirth;
        $userAry['HKID'] = $_col[8];
        $userAry['Gender'] = $_col[9];
        $userAry['HomeTelNo'] = $_col[22];
        $userAry['EnglishName'] = $userAry['LastName'] . ' ' . $userAry['FirstName'];
        $userAry['ModifyBy'] = $_SESSION['UserID'];
        
        $condAry = array();
        $condAry['UserID'] = $studentID;
        $resultAry['UpdateIntranet'] = $lsr->update2Table('INTRANET_USER', $userAry, $condAry);
//error_log("\n\n condAry-->".print_r($condAry,true)."<---- ".date("Y-m-d H:i:s")." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__."\n", 3, "/tmp/ccc.txt");
        
        
        # STUDENT_REGISTRY_STUDENT
        
        if($lsr->returnPresetCodeName("NATIONALITY", $_col[10])) {
            $nationCode = $_col[10];
            $nationOthers = '';
        }
        else {
            if ($_col[10]) {
                $nationCode = '999';
                $nationOthers = $_col[10];
            }
            else {
                $nationCode = '';
                $nationOthers = '';
            }
        }

        if($lsr->returnPresetCodeName("ETHNICITY", $_col[11])) {
            $ethnicityCode = $_col[11];
            $ethnicityOthers = '';
        }
        else {
            if ($_col[11]) {
                $ethnicityCode = '999';
                $ethnicityOthers = $_col[11];
            }
            else {
                $ethnicityCode = '';
                $ethnicityOthers = '';
            }
        }

        if($lsr->returnPresetCodeName("FAMILY_LANG", $_col[12])) {
            $familyLangCode = $_col[12];
            $familyLangOthers = '';
        }
        else {
            if ($_col[12]) {
                $familyLangCode = '999';
                $familyLangOthers = $_col[12];
            }
            else {
                $familyLangCode = '';
                $familyLangOthers = '';
            }
        }
        
        if($lsr->returnPresetCodeName("SUBDISTRICT", $_col[19])) {
            $districtCode = $_col[19];
            $districtOthers = '';
        }
        else {
            if ($_col[19]) {
                $districtCode = '999';
                $districtOthers = $_col[19];
            }
            else {
                $districtCode = '';
                $districtOthers = '';
            }
        }
        
        $stuAry = array();
        $stuAry['ID_NO'] = $_col[8];
        $stuAry['B_PLACE_CODE'] = $placeOfBirthCode;
        $stuAry['B_PLACE_OTHERS'] = $placeOfBirthOthers;
        $stuAry['NATION_CODE'] = $nationCode;
        $stuAry['NATION_OTHERS'] = $nationOthers;
        $stuAry['ETHNICITY_CODE'] = $ethnicityCode;
        $stuAry['RACE_OTHERS'] = $ethnicityOthers;
        $stuAry['FAMILY_LANG'] = $familyLangCode;
        $stuAry['FAMILY_LANG_OTHERS'] = $familyLangOthers;
        $stuAry['ADDRESS_ROOM_EN'] = $_col[13];
        $stuAry['ADDRESS_FLOOR_EN'] = $_col[14];
        $stuAry['ADDRESS_BLK_EN'] = $_col[15];
        $stuAry['ADDRESS_BLD_EN'] = $_col[16];
        $stuAry['ADDRESS_EST_EN'] = $_col[17];
        $stuAry['ADDRESS_STR_EN'] = $_col[18];
        $stuAry['DISTRICT_CODE'] = $districtCode;
        $stuAry['ADDRESS_DISTRICT_EN'] = $districtOthers;
        $stuAry['ADDRESS_AREA'] = $lsr->getAreaVal($_col[20]);
        $stuAry['ADDRESS_STR_CH'] = $_col[21];
        $stuAry['NANNY_BUS'] = $_col[23];
        $stuAry['LAST_SCHOOL_EN'] = $_col[62];
        $stuAry['ModifyBy'] = $_SESSION['UserID'];
        
        $isStudentRegistryExist = $lsr->isStudentRegistryExist($studentID);
        if ($isStudentRegistryExist) {
            $condAry = array();
            $condAry['UserID'] = $studentID;
            $resultAry['UpdateStudentRegistry'] = $lsr->update2Table('STUDENT_REGISTRY_STUDENT', $stuAry, $condAry);
        }
        else {
            $stuAry['UserID'] = $studentID;
            $stuAry['InputBy'] = $_SESSION['UserID'];
            $stuAry['DateInput'] = 'now()';
            $resultAry['InsertStudentRegistry'] = $lsr->insert2Table('STUDENT_REGISTRY_STUDENT', $stuAry);
        }
//error_log("\n\n stuAry-->".print_r($stuAry,true)."<---- ".date("Y-m-d H:i:s")." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__."\n", 3, "/tmp/ccc.txt");
        
        
        # STUDENT_REGISTRY_BROSIS
        
        $maxSiblings = 3;
        $resultAry['RemoveSiblings'] = $lsr->removeSiblings($studentID);
        
        $startIdx = 63;
        for($i=0;$i<$maxSiblings;$i++)
        {
            $j = $startIdx+($i*8);
            
            $bsAry = array();
            $_bsRelationship = $_col[$j];
            $_bsDateOfBirth = $_col[$j+1];
            $_bsName = $_col[$j+2];
            $_bsIsSchoolStudentOrAlumni= (strtoupper($_col[$j+3]) == 'Y') ? '1' : '0';
            $_bsSchoolClass = $_col[$j+4];
            $_bsGraduateYear = $_col[$j+5];
            $_bsCurSchool = $_col[$j+6];
            
            if($lsr->returnPresetCodeName("SCHOOLLEVEL", $_col[$j+7])) {
                $_bsCurLevel = $_col[$j+7];
                $_bsCurLevelOthers = '';
            }
            else {
                if ($_col[$j+7]) {
                    $_bsCurLevel = '999';
                    $_bsCurLevelOthers = $_col[$j+7];
                }
                else {
                    $_bsCurLevel = '';
                    $_bsCurLevelOthers = '';
                }
            }
            
            if ($_bsRelationship || $_bsName || $_bsDateOfBirth || $_bsSchoolClass || $_bsGraduateYear || $_bsCurSchool || $_bsCurLevel) {
                $bsAry['StudentID'] = $studentID;
                $bsAry['Relationship'] = $_bsRelationship;
                $bsAry['DateOfBirth'] = $_bsDateOfBirth;
                $bsAry['StudentName'] = $_bsName;
                $bsAry['IsSchoolStudentOrAlumni'] = $_bsIsSchoolStudentOrAlumni;
                if ($_bsIsSchoolStudentOrAlumni) {
                    $bsAry['SchoolClass'] = $_bsSchoolClass;
                    $bsAry['GraduateYear'] = $_bsGraduateYear;
                }
                else {
                    $bsAry['SchoolClass'] = '';
                    $bsAry['GraduateYear'] = '';
                }
                $bsAry['CurSchool'] = $_bsCurSchool;
                $bsAry['CurLevel'] = $_bsCurLevel;
                $bsAry['CurLevelOthers'] = $_bsCurLevelOthers;
                
                $resultAry['InsertBrotherNSister_'.$i] = $lsr->insert2Table('STUDENT_REGISTRY_BROSIS', $bsAry, $condition = array(), $run = true, $insertIgnore = false, $updateDateModified = false);
//error_log("\n\n bsAry-->".print_r($bsAry,true)."<---- ".date("Y-m-d H:i:s")." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__."\n", 3, "/tmp/ccc.txt");
                
            }
        }       // end loop for siblings
        
        
        # STUDENT_REGISTRY_PG
        
        $startIdx = 24;
        for($i=0;$i<2;$i++)
        {
            $j = $startIdx+($i*19);
            $g = $i + 1;
            
            $gAry = array();
            $gAry['SEQUENCE'] = $g;
            $gAry['PG_TYPE'] = ($i==0) ? 'F' : 'M';
            $gAry['FIRST_NAME_E'] = $_col[$j+1];
            $gAry['LAST_NAME_E'] = $_col[$j];
            $gAry['NAME_E'] = $gAry['LAST_NAME_E'] . ' ' . $gAry['FIRST_NAME_E'];
            $gAry['NAME_C'] = $_col[$j+2];
            $gAry['BUSINESS_NAME'] = $_col[$j+3];
            $gAry['EMAIL'] = $_col[$j+4];
            $gAry['JOB_TITLE'] = $_col[$j+13];
            $gAry['JOB_NATURE'] = $_col[$j+14];
            $gAry['IS_ALUMNI'] = ($_col[$j+15] == 'Y') ? 1 : 0;
            $gAry['GRADUATE_YEAR'] = $_col[$j+16];
            $gAry['TEL'] = $_col[$j+17];;
            $gAry['MOBILE'] = $_col[$j+18];
            $gAry['ADDRESS_ROOM_EN'] = $_col[$j+5];
            $gAry['ADDRESS_FLOOR_EN'] = $_col[$j+6];
            $gAry['ADDRESS_BLK_EN'] = $_col[$j+7];
            $gAry['ADDRESS_BLD_EN'] = $_col[$j+8];
            $gAry['ADDRESS_EST_EN'] = $_col[$j+9];
            $gAry['ADDRESS_STR_EN'] = $_col[$j+10];
            
            if($lsr->returnPresetCodeName("SUBDISTRICT", $_col[$j+11])) {
                $districtCode = $_col[$j+11];
                $districtOthers = '';
            }
            else {
                if ($_col[$j+11]) {
                    $districtCode = '999';
                    $districtOthers = $_col[$j+11];
                }
                else {
                    $districtCode = '';
                    $districtOthers = '';
                }
            }
            
            $gAry['DISTRICT_CODE'] = $districtCode;
            $gAry['ADDRESS_DISTRICT_EN'] = $districtOthers;
            $gAry['ADDRESS_AREA'] = $lsr->getAreaVal($_col[$j+12]);
            $gAry['ModifyBy'] = $_SESSION['UserID'];
            
            $isParentGuardianExist = $lsr->isParentGuardianExist($studentID, $g);
            if ($isParentGuardianExist) {
                $condAry = array();
                $condAry['StudentID'] = $studentID;
                $condAry['SEQUENCE'] = $g;
                $resultAry['UpdateParentGuardian_'.$g] = $lsr->update2Table('STUDENT_REGISTRY_PG', $gAry, $condAry);
            }
            else {
                $gAry['StudentID'] = $studentID;
                $gAry['InputBy'] = $_SESSION['UserID'];
                $gAry['DateInput'] = 'now()';
                $resultAry['InsertParentGuardian_'.$g] = $lsr->insert2Table('STUDENT_REGISTRY_PG', $gAry);
            }
//error_log("\n\n gAry-->".print_r($gAry,true)."<---- ".date("Y-m-d H:i:s")." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__."\n", 3, "/tmp/ccc.txt");
            
        }
        
        
        # STUDENT_REGISTRY_EMERGENCY_CONTACT
        $startIdx = 87;
        for($i=0;$i<2;$i++)
        {
            $j = $startIdx+($i*5);
            $c = $i + 1;
            
            $eContactAry= array();
            $eContactAry['Sequence'] = $c;
            $eContactAry['Contact3_EglishFirstName'] = $_col[$j+1];
            $eContactAry['Contact3_EglishLastName'] = $_col[$j];
            $eContactAry['Contact3_EnglishName'] = $eContactAry['Contact3_EglishLastName'] . ' ' . $eContactAry['Contact3_EglishFirstName'];
            $eContactAry['Contact3_ChineseName'] = $_col[$j+2];
            
            if($lsr->returnPresetCodeName("RELATIONSHIP", $_col[$j+3])) {
                $relationshipCode = $_col[$j+3];
                $relationshipOthers = '';
            }
            else {
                if ($_col[$j+3]) {
                    $relationshipCode = '999';
                    $relationshipOthers = $_col[$j+3];
                }
                else {
                    $relationshipCode = '';
                    $relationshipOthers = '';
                }
            }
            
            $eContactAry['Contact3_RelationshipCode'] = $relationshipCode;
            $eContactAry['Contact3_Relationship'] = $relationshipOthers;
            $eContactAry['Contact3_Mobile'] = $_col[$j+4];
            $eContactAry['ModifyBy'] = $_SESSION['UserID'];
            
            $isEmergencyContactExist = $lsr->isEmergencyContactExist($studentID, $c);
            if ($isEmergencyContactExist) {
                $condAry = array();
                $condAry['StudentID'] = $studentID;
                $condAry['Sequence'] = $c;
                $resultAry['UpdateEmergencyContact_'.$c] = $lsr->update2Table('STUDENT_REGISTRY_EMERGENCY_CONTACT', $eContactAry, $condAry);
            }
            else {
                $eContactAry['StudentID'] = $studentID;
                $eContactAry['InputBy'] = $_SESSION['UserID'];
                $eContactAry['DateInput'] = 'now()';
                $resultAry['InsertEmergencyContact_'.$c] = $lsr->insert2Table('STUDENT_REGISTRY_EMERGENCY_CONTACT', $eContactAry);
            }
            
//error_log("\n\n eContactAry-->".print_r($eContactAry,true)."<---- ".date("Y-m-d H:i:s")." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__."\n", 3, "/tmp/ccc.txt");
            
        }
     
        
        # update eClass40 data
        $dataAry = array();
        $dataAry['EnglishName'] = $userAry['EnglishName'];
        $dataAry['ChineseName'] = $userAry['ChineseName'];
        $dataAry['Gender'] = $userAry['Gender'];
        $dataAry['DateOfBirth'] = $userAry['DateOfBirth'];
        $dataAry['HomeTelNo'] = $userAry['HomeTelNo'];
        
        $le = new libeclass();
        $le->eClass40UserUpdateInfo($studentEmail, $dataAry, $studentEmail);
        
//error_log("\n\n dataAry-->".print_r($dataAry,true)."<---- ".date("Y-m-d H:i:s")." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__."\n", 3, "/tmp/ccc.txt");
        
    }
}

header("Location: import_done.php?no=".$nrUpdate);
//echo "done<br>";

intranet_closedb();

?>