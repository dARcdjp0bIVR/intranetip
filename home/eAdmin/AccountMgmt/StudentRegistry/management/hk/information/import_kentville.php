<?php
// Editing by 

/*
 *  Using:
 *
 *  2019-05-09 Cameron
 *      - create this file
 */

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccessright.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry_kv.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry_ui.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");

intranet_auth();
intranet_opendb();

$laccessright = new libaccessright();

if (!$plugin['AccountMgmt_StudentRegistry'] || !$sys_custom['StudentRegistry']['Kentville'] || !$_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"]) {
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit;
}

$lsr = new libstudentregistry_kv();
$limport = new libimporttext();
$linterface = new libstudentregistry_ui();


$retMsg = IntegerSafe($_GET['retMsg']);
if ($retMsg) {
    $returnMsg = $Lang['General']['ReturnMessage']['ImportSuccess'];  
}
else if ($retMsg != '') {
    $returnMsg = $Lang['General']['ReturnMessage']['ImportUnsuccess'];
}

$csvFile = "<a class='tablelink' href='import_template.php'>". $Lang['General']['ClickHereToDownloadSample'] ."</a>";
$CurrentPageArr['StudentRegistry'] = 1;
$CurrentPage = "Mgmt_RegistryInfo";
$TAGS_OBJ[] = array($Lang['Menu']['AccountMgmt']['StudentRegistryInfo'], "index.php");
$MODULE_OBJ['title'] = $Lang['Header']['Menu']['StudentRegistry'];
$MODULE_OBJ = $lsr->GET_MODULE_OBJ_ARR();


$linterface->LAYOUT_START($returnMsg);

$PAGE_NAVIGATION = array();
$PAGE_NAVIGATION[] = array($Lang['eSchoolBus']['SettingsArr']['StudentsArr']['MenuTitle'],'index.php?');
$PAGE_NAVIGATION[] = array($Lang['Btn']['Import'],'');

# step information
$STEPS_OBJ[] = array($i_general_select_csv_file, 1);
//$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 0);
$STEPS_OBJ[] = array($i_general_imported_result, 0);

?>
<script>
$(document).ready(function(){
	$('#submitBtn').click(function(){
		if ($('#upload_file').val() == '') {
			alert("<?php echo $Lang['General']['PleaseSelectFiles'];?>");
		}
		else {
			$('#form1').submit();
		}
	});
});
</script>
<form name="form1" id="form1" method="POST" action="import_update_kentville.php" enctype="multipart/form-data">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td><?=$linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION)?></td>
	</tr>
	<tr>
		<td>
			&nbsp;
		</td>
	</tr>
	<tr>
		<td><?=$linterface->GET_STEPS($STEPS_OBJ)?></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
</table>
<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
			<?=$Lang['Btn']['Select']?>
		</td>
		<td width="70%" class="tabletext">
			<input type="file" class="file" name="upload_file" id="upload_file"><br />
		</td>
	</tr>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle" align="left"><?=$Lang['General']['CSVSample']?> </td>
		<td width="70%" class="tabletext"><a class="tablelink" href="import_template_kentville.php" target="_blank"><?=$Lang['General']['ClickHereToDownloadSample']?></a></td>
	</tr>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle" align="left"><?=$Lang['SysMgr']['Homework']['Import']['FieldTitle']['DataColumn']?></td>
		<td width="70%" class="tabletext">
	<?php
	$x = '';
	
	for($i=0,$iMax=count($Lang['StudentRegistry']['ImportAry']);$i<$iMax;$i++){
		$x .= $Lang['General']['ImportArr']['Column'].'&nbsp;'.($i+1).'&nbsp;:&nbsp;';
		$x .= $Lang['StudentRegistry']['ImportAry'][$i][0].' - '.$Lang['StudentRegistry']['ImportAry'][$i][1];
		if($Lang['StudentRegistry']['ImportAry'][$i][2] !=''){
			$x .= ' [<a class="tablelink" href="javascript:void(0);" onclick="displayInfoLayer(this,\'InfoLayer\',\''.$Lang['StudentRegistry']['ImportAry'][$i][2].'\');">'.$Lang['Btn']['View'].'</a>]';
		}
		$x .= '<br />';
	}
	
	echo $x;
	?>	
		</td>
	</tr>
	<tr><td colspan="2"><?=$linterface->MandatoryField()?></td></tr>
</table>
<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
    	<td class="dotline"><img src="<?=$PATH_WRT_ROOT?>images/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1" /></td>
    </tr>
</table>
<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
	    <td align="center" colspan="2">
			<?=$linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "button", "", "submitBtn", " class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"")?>
			<?=$linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "javascript:window.location.href='index.php'","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"")?>
		</td>
	</tr>
</table>
<div id="InfoLayer" class="selectbox_layer selectbox_layer_show_info" style="visibility:visible;display:none;">
	<div style="float:right">
		<a href="javascript:void(0);" onclick="$('#InfoLayer').toggle('fast');">
			<img border="0" src="/images/<?=$LAYOUT_SKIN?>/ecomm/btn_mini_off.gif">
		</a>
	</div>
	<p class="spacer"></p>
	<div class="content" style="min-width:256px;max-height:512px;overflow-y:auto;"></div>
</div>
</form>
<br />
<script type="text/javascript" language="JavaScript">
function displayInfoLayer(btnElement,layerId,task)
{
	var btnObj = $(btnElement);
	var layerObj = $('#'+layerId);
	var posX = btnObj.offset().left;
	var posY = btnObj.offset().top + btnObj.height();
	
	if(layerObj.is(':visible') && task == document.last_task){
		layerObj.toggle('fast');
		document.last_task = task;
		return;
	}
	document.last_task = task;

	$.ajax({
		dataType: "json",
		type: "POST",
		url: 'ajax.php',
		data : {
			action: task,
			},		  
		success: function(ajaxReturn){
			if (ajaxReturn != null && ajaxReturn.success){
				if (ajaxReturn.html != '') {
					
					layerObj.css({'position':'absolute','top':posY+'px','left':posX+'px'});
					layerObj.show('fast');
					$('#'+layerId+' .content').html(ajaxReturn.html);
				}
			}
		},
		error: show_ajax_error
	});
}

function show_ajax_error() 
{
	alert('<?=$Lang['General']['AjaxError']?>');
}

</script>
<?php
$linterface->LAYOUT_STOP();
?>