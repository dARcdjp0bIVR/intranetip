<?php
#using:

############# Change Log
#
#	Date:	2020-03-12 Philips
#			- Added Section for custom column
#
#   Date:   2019-08-15 Cameron
#           - pass parameter $AcademicYearID to RETRIEVE_STUDENT_INFO_BASIC_HK() for displaying last submit by
#
#   Date:   2019-04-29 Philips
#           added Custom Column
#
#   Date:   2019-03-11 Cameron
#           redirect to view_kentville.php if $sys_custom['StudentRegistry']['Kentville'] is set
#
#	Date:	2016-09-26 Cameron
#			add hkid validation function
#	
#	Date:	2015-07-22  Cameron
#			fix bug on navigation bar linking: when ClassName is 0 and YearClassID is empty, don't provide hyperlink  		
#
#	Date:	2013-08-16	YatWoon
#			Add "gardian address same as student" logic
#
#################


$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccessright.php");
// include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
// include_once($PATH_WRT_ROOT."includes/libdbtable.php");
// include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");


intranet_auth();
intranet_opendb();

$studentID = (is_array($studentID)? $studentID[0]:$studentID);
$laccessright = new libaccessright();

if	(	!$plugin['AccountMgmt_StudentRegistry'] || 
		(!$_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-View") && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-Manage")) &&
		!$_SESSION['ParentFillOnlineReg']
	) 
{
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

if ($sys_custom['StudentRegistry']['Kentville']) {
    include_once($PATH_WRT_ROOT."home/eAdmin/AccountMgmt/StudentRegistry/management/hk/information/view_kentville.php");
    exit();
}

$lsr = new libstudentregistry();
if($_SESSION['ParentFillOnlineReg'])
	$linterface = new interface_html("popup.html");
else
	$linterface = new interface_html();

$result = $lsr -> RETRIEVE_STUDENT_INFO_BASIC_HK($studentID, $StudentID_str='', $AcademicYearID);

$li = new libuser($studentID);
$lfcm = new form_class_manage();

# Chinse Name
if($_SESSION['ParentFillOnlineReg'])
{
	$chinese_name_data = $result[0]['ChineseName'];	
}
else
{
	$chinese_name_data = "<input type='text' name='chinese_name' value='". $result[0]['ChineseName'] ."'>";
}

# English Name
if($_SESSION['ParentFillOnlineReg'])
{
	$english_name_data = $result[0]['EnglishName'];	
}
else
{
	$english_name_data = "<input type='text' name='english_name' value='". $result[0]['EnglishName'] ."'>";
}


#Gender related info
$gender_ary      = $lsr -> GENDER_ARY();
$gender_val      = $result[0]['Gender']? $lsr -> RETRIEVE_DATA_ARY($gender_ary, $result[0]['Gender']) : "--";
$gender_edit_ary = $lsr -> RETRIEVE_DATA_ARY($gender_ary);
$gender_RBL_html = $lsr -> GENERATE_RADIO_BTN_LIST($gender_edit_ary, "gender", $gender_val);

# ID Document Type
$ID_DATA = $lsr->returnPresetCodeName("IDENT_DOC_TYPE", $result[0]['ID_TYPE']);
$ID_DATA_OPTIONS = $lsr->displayPresetCodeSelection("IDENT_DOC_TYPE", "passport_type", $result[0]['ID_TYPE'] ? $result[0]['ID_TYPE'] : "04");
	 
# Ethnicity
$Ethnicity_data = $lsr->returnPresetCodeName("ETHNICITY", $result[0]['ETHNICITY_CODE']);
$Ethnicity_data_options = $lsr->displayPresetCodeSelection("ETHNICITY", "ethnicity", $result[0]['ETHNICITY_CODE']);

# Language Spoken at Home
$HomeLang_data = $lsr->returnPresetCodeName("FAMILY_LANG", $result[0]['FAMILY_LANG']);
$HomeLang_data_options = $lsr->displayPresetCodeSelection("FAMILY_LANG", "family_lang", $result[0]['FAMILY_LANG']);

# Religion
$Religion_data = $lsr->returnPresetCodeName("RELIGION", $result[0]['RELIGION']);
$Religion_data_options = $lsr->displayPresetCodeSelection("RELIGION", "religion", $result[0]['RELIGION']);
	
# Nationality
$student_nationality_data = $lsr->returnPresetCodeName("NATIONALITY", $result[0]['NATION_CODE']);
$InputByTextfield = $lsr->isNationailityInputByText();
if($InputByTextfield)
{
	$student_nationality_options = "<input type='text' name='nationality' value='". $result[0]['Nationality']."'/>";
}
else
{
	$student_nationality_options = $lsr->displayPresetCodeSelection("NATIONALITY", "nation_code", $result[0]['NATION_CODE']);
}

# District 
$student_district_data = $lsr->returnPresetCodeName("DISTRICT", $result[0]['DISTRICT_CODE']);
$student_district_options = $lsr->displayPresetCodeSelection("DISTRICT", "student_district_code", $result[0]['DISTRICT_CODE']);

# Address Area
for($i=0;$i<sizeof($Lang['StudentRegistry']['AreaOptions']);$i++)
{
	$AddressAreaOptions .= "<input type='radio' name='address_area' value='". ($i+1) ."' id='AddressArea". ($i+1) ."' ". ($result[0]['ADDRESS_AREA']==($i+1) ? "checked": "")."> <label for='AddressArea". ($i+1) ."'>". $Lang['StudentRegistry']['AreaOptions'][$i] ."</label> ";
}

/*
# - Last Class Level & Class No. (For current students only) -
$LastAcademicYear_data = $lsr->RETRIEVE_LAST_CLASSINFO_HK($studentID);
$last_academic_year = $lfcm->Get_Academic_Year_List('', $OrderBySequence, $excludeYearIDArr="", $noPastYear="", $pastAndCurrentYearOnly=1, $excludeCurrentYear=1);
for($i=0;$i<6;$i++)
{
	if($last_academic_year[$i]['AcademicYearID']=="") break;
	
	$LastAcademicYear_table_year .= "<td style='text-align:center;'>". $last_academic_year[$i]['YearNameEN'] ."</td>";
	$LastAcademicYear_table_classname .= "<td><input type='text' name='ClassName_". $last_academic_year[$i]['AcademicYearID'] ."' value='". $LastAcademicYear_data[$last_academic_year[$i]['AcademicYearID']]['ClassName'] ."'></td>";
	$LastAcademicYear_table_classnumber .= "<td><input type='text' name='ClassNumber_". $last_academic_year[$i]['AcademicYearID'] ."' value='". $LastAcademicYear_data[$last_academic_year[$i]['AcademicYearID']]['ClassNumber'] ."'></td>";
	
	if($LastAcademicYear_data[$last_academic_year[$i]['AcademicYearID']])
	{
		$LastAcademicYear_data_table_year .= "<td style='text-align:center;'>". $last_academic_year[$i]['YearNameEN'] ."</td>";
		$LastAcademicYear_data_table_classname .= "<td style='text-align:center;'>". $LastAcademicYear_data[$last_academic_year[$i]['AcademicYearID']]['ClassName']  ."</td>";
		$LastAcademicYear_data_table_classnumber .= "<td style='text-align:center;'>". $LastAcademicYear_data[$last_academic_year[$i]['AcademicYearID']]['ClassNumber']  ."</td>";
	}
}	
$LastAcademicYear_table = "<table class='inside_form_table' width='100%'><tr><td></td>";
$LastAcademicYear_table .= $LastAcademicYear_table_year;
$LastAcademicYear_table .= "</tr><tr><td>". $i_ClassName ."</td>";
$LastAcademicYear_table .= $LastAcademicYear_table_classname;
$LastAcademicYear_table .= "</tr><tr><td>". $i_ClassNumber ."</td>";
$LastAcademicYear_table .= $LastAcademicYear_table_classnumber;
$LastAcademicYear_table .= "</tr></table>";

if(!empty($LastAcademicYear_data))
{
	$LastAcademicYear_data_table = "<table class='inside_form_table' width='100%'><tr><td></td>";
	$LastAcademicYear_data_table .= $LastAcademicYear_data_table_year;
	$LastAcademicYear_data_table .= "</tr><tr><td>". $i_ClassName ."</td>";
	$LastAcademicYear_data_table .= $LastAcademicYear_data_table_classname;
	$LastAcademicYear_data_table .= "</tr><tr><td>". $i_ClassNumber ."</td>";
	$LastAcademicYear_data_table .= $LastAcademicYear_data_table_classnumber;
	$LastAcademicYear_data_table .= "</tr></table>";
}
else
{
	$LastAcademicYear_data_table = $Lang['General']['NoRecordAtThisMoment'];
}
*/

# Brothers & sisters currently studying in school
$BroSis_data = $lsr->RETRIEVE_BRO_SIS_HK($studentID);
$withBS = empty($BroSis_data) ? "0" : "1";

//$BroSis_table = $Lang['StudentRegistry']['BrotherSisterStudyingInSchool'] ."? <input type='radio' value='1' name='withBS' onClick='enable_BSTable();' ". ($withBS?"checked":"")."> ". $i_general_yes ." <input type='radio' value='0' name='withBS' ". ($withBS?"":"checked")." onClick='enable_BSTable();'> ". $i_general_no ." <br>";
$BroSis_table = $Lang['StudentRegistry']['BrotherSisterStudyingInSchool'] ."? ";
$BroSis_table .= $linterface->Get_Radio_Button("withBS1", "withBS", "1", $withBS, "", $Lang['General']['Yes2'], "enable_BSTable();");
$BroSis_table .= $linterface->Get_Radio_Button("withBS0", "withBS", "0", !$withBS, "", $Lang['General']['No2'], "enable_BSTable();");

$BroSis_table .= "<table id='BSTable' class='inside_form_table' width='100%' style='display:none'><tr><td>". $i_UserLogin ."</td><td>". $Lang['AccountMgmt']['StudentName'] ."</td><td>". $Lang['StudentRegistry']['Relationship'] ."</td></tr>";
for($i=0;$i<3;$i++)
{
	$BroSis_table .= "<tr>";
	$BroSis_table .= "<td><input type='text' name='BS_UserLogin_". $i ."' value='". $BroSis_data[$i]['BS_UserLogin'] ."'></td>";
	$BroSis_table .= "<td><input type='text' name='BS_Name_". $i ."' value='". $BroSis_data[$i]['StudentName'] ."'></td>";
	$BroSis_table .= "<td><input type='text' name='BS_Relationship_". $i ."' value='". $BroSis_data[$i]['Relationship'] ."'></td>";
	$BroSis_table .= "</tr>";
	
	# Not Ready for this Section
	/*
	foreach($custCols as $ccol){
        if($ccol['Section'] == 'BrotherSisterStudyingInSchool'){
        	$displayText = $ccol["DisplayText_".Get_Lang_Selection('ch','en')];
        	$val = $ccol['Value'];
        	$code = $ccol[Code];
        	$colID = $ccol['ColumnID'];
        	$BroSis_table .= <<< EOD_HTML
                        	<tr>
                        		<td class="field_title_short">{$displayText}</td>
                        		<td>
                        			<span class="Edit_Hide">{$ccol['Value']}</span>
                        			<span class="Edit_Show" style="display:none"><input type="text" class="textboxtext" name="{$code}" value="{$val}"/></span>
                        			<input type="hidden" name="custCol[{$code}]" value="{$colID}" />
                        		</td>
                        	</tr>
EOD_HTML;
    	}
    }
    */
}
$BroSis_table .= "</table>";
if(!empty($BroSis_data))
{
	$BroSis_data_table = "<table class='inside_form_table' width='100%'><tr><td>". $i_UserLogin ."</td><td>". $Lang['AccountMgmt']['StudentName'] ."</td><td>". $Lang['StudentRegistry']['Relationship'] ."</td></tr>";
	if(!empty($BroSis_data))
	{
		foreach($BroSis_data as $k=>$d)
		{
			$BroSis_data_table .= "<tr>";
			$BroSis_data_table .= "<td>". $d['BS_UserLogin'] ."</td>";
			$BroSis_data_table .= "<td>". $d['StudentName'] ."</td>";
			$BroSis_data_table .= "<td>". $d['Relationship'] ."</td>";
			$BroSis_data_table .= "</tr>";	
		}
	}
	$BroSis_data_table .= "</table>";
}
else
{
	$BroSis_data_table = $Lang['General']['NoRecordAtThisMoment'];
}

# Guardian
$guardian_selection = array();
for($g=1;$g<=2;$g++)
{
	$g_result = $lsr -> RETRIEVE_GUARDIAN_INFO_HK($studentID, $g);

	$gType[$g] = $g_result['PG_TYPE']==1 ? $Lang['StudentRegistry']['Father'] : ($g_result['PG_TYPE']==2 ? $Lang['StudentRegistry']['Mother'] : $g_result['PG_TYPE_OTHERS']);
	$guardian_type[$g] = "	<input type='radio' name='guardian_type". $g."' id='guardian_type". $g."_1' value='1' ". ($g_result['PG_TYPE']==1?"checked":"") ."> <label for='guardian_type". $g."_1'>" . $Lang['StudentRegistry']['Father'] . "</label>  
							<input type='radio' name='guardian_type". $g."' id='guardian_type". $g."_2' value='2' ". ($g_result['PG_TYPE']==2?"checked":"") ."> <label for='guardian_type". $g."_2'>" . $Lang['StudentRegistry']['Mother'] . "</label> 
							<br><input type='radio' name='guardian_type". $g."' id='guardian_type". $g."_0' value='0' ". ($g_result['PG_TYPE']==0?"checked":"") ."> <label for='guardian_type". $g."_0'>" . $Lang['StudentRegistry']['Other'] . "</label> (". $Lang['General']['PlsSpecify'].": <input type='text' name='guardian_type_others". $g ."' value='". $g_result['PG_TYPE_OTHERS'] ."'/>)";
							
	$gEmail[$g] = $g_result['EMAIL'];
	$guardian_email[$g] = "<input type='text' name='guardian_email". $g."' value='". $gEmail[$g] ."'/>";
	
	$gEnglishName[$g] = $g_result['NAME_E'];
	$guardian_english_name[$g] = "<input type='text' name='guardian_english_name". $g."' value='". $gEnglishName[$g] ."'/>";
	
	$gChineseName[$g] = $g_result['NAME_C'];
	$guardian_chinese_name[$g] = "<input type='text' name='guardian_chinese_name". $g."' value='". $gChineseName[$g] ."'/>";
	
	/*
	$gHKID[$g] = $g_result['HKID'];
	$guardian_hkid[$g] = "<input type='text' name='guardian_hkid". $g."' value='". $g_result['HKID'] ."'/>";
	*/
	
	$gOccupation[$g] = $g_result['JOB_TITLE_OTHERS'];
	$guardian_occupation[$g] = "<input type='text' name='guardian_occupation". $g."' value='". $g_result['JOB_TITLE_OTHERS'] ."'/>";
	
	$gTel[$g] = $g_result['TEL'];
	$guardian_tel[$g] = "<input type='text' name='guardian_tel". $g."' value='". $g_result['TEL'] ."'/>";
	
	$gMobile[$g] = $g_result['MOBILE'];
	$guardian_mobile[$g] = "<input type='text' name='guardian_mobile". $g."' id='guardian_mobile". $g."' value='". $g_result['MOBILE'] ."' maxlength='8'/>";
	
	# guardian Religion
	$gReligion[$g] = $lsr->returnPresetCodeName("RELIGION", $g_result['RELIGION']);
	$guardian_religion[$g] .= $lsr->displayPresetCodeSelection("RELIGION", "guardian_religion". $g, $g_result['RELIGION']);
	
	/*
	$gReligion[$g] = $g_result['RELIGION']>0 ? $Lang['StudentRegistry']['ReligionOptions'][$g_result['RELIGION']-1] : $g_result['RELIGION_OTHERS'];
	for($i=0;$i<sizeof($Lang['StudentRegistry']['ReligionOptions']);$i++)
	{
		$guardian_religion[$g] .= "<input type='radio' name='guardian_religion". $g."' value='". ($i+1) ."' id='guardian_religion".$g."_". ($i+1) ."' ". ($g_result['RELIGION']==($i+1)?"checked":"") ."> <label for='guardian_religion".$g."_". ($i+1) ."'>". $Lang['StudentRegistry']['ReligionOptions'][$i] ."</label> ";
	}
	$guardian_religion[$g] .= "<br><input type='radio' name='guardian_religion". $g."' value='0' id='guardian_religion".$g."_0' ". ($g_result['RELIGION']==0?"checked":"") ."> <label for='guardian_religion".$g."_0'>". $Lang['General']['Others'] ."</label> (". $Lang['General']['PlsSpecify'].": <input type='text' name='guardian_religion".$g."_others' value='". $g_result['RELIGION_OTHERS'] ."'/>)";
	*/
	
	$gChurch[$g] = $g_result['CHURCH'];
	$guardian_church[$g] = "<input type='text' name='guardian_church". $g."' value='". $g_result['CHURCH'] ."'/>";
	
	$gAddressRoomEn[$g] = $g_result['ADDRESS_ROOM_EN'];
	$gAddressFloorEn[$g] = $g_result['ADDRESS_FLOOR_EN'];
	$gAddressBlkEn[$g] = $g_result['ADDRESS_BLK_EN'];
	$gAddressBldEn[$g] = $g_result['ADDRESS_BLD_EN'];
	$gAddressEstEn[$g] = $g_result['ADDRESS_EST_EN'];
	$gAddressStrEn[$g] = $g_result['ADDRESS_STR_EN'];
	//$gAddressDistrictEn[$g] = $g_result['ADDRESS_DISTRICT_EN'];
	$gAddressDistrictEn[$g] = $lsr->returnPresetCodeName("DISTRICT", $g_result['DISTRICT_CODE']);
	$guardian_district[$g] = $lsr->displayPresetCodeSelection("DISTRICT", "guardian_district".$g, $g_result['DISTRICT_CODE']);


	$gAddressArea[$g] = $Lang['StudentRegistry']['AreaOptions'][$g_result['ADDRESS_AREA']-1];
	for($i=0;$i<sizeof($Lang['StudentRegistry']['AreaOptions']);$i++)
	{
		$guardian_address_area[$g] .= "<input type='radio' name='guardian_address_area". $g."' value='". ($i+1) ."' id='guardian_address_area".$g."_". ($i+1) ."' ". ($g_result['ADDRESS_AREA']==($i+1)?"checked":"") ."> <label for='guardian_address_area".$g."_". ($i+1) ."'>". $Lang['StudentRegistry']['AreaOptions'][$i] ."</label> ";
	}
	
	# address with data?
	$gAddressData[$g] = 0;
	if($gAddressRoomEn[$g] || $gAddressFloorEn[$g] || $gAddressBlkEn[$g] || $gAddressBldEn[$g] || $gAddressEstEn[$g] || $gAddressStrEn[$g])
	{
		$gAddressData[$g] = 1;
	}
}



# Contact Persion in case of emergency
$contact_result = $lsr -> RETRIEVE_EMERGENCY_CONTACT_INFO_HK($studentID);
if($contact_result['Contact1'] > 0)	
	$contact1_data = $contact_result['Contact1']==1? $Lang['StudentRegistry']['Father'] : $Lang['StudentRegistry']['Mother'];
else							# others
	$contact1_data = $contact_result['Contact1_Others'];
if($contact_result['Contact2'] > 0)	
	$contact2_data = $contact_result['Contact2']==1? $Lang['StudentRegistry']['Father'] : $Lang['StudentRegistry']['Mother'];
else							# others
	$contact2_data = $contact_result['Contact2_Others'];
$contact3_title_data = $contact_result['Contact3_Title']==1 ? $i_title_mr :($contact_result['Contact3_Title']==2 ? $i_title_mrs : $i_title_ms);

	

#Past Enrollment Information
// $Past_Enroll = $lsr -> RETRIEVE_STUDENT_PAST_ENROLLMENT_RECORD($studentID);

/*
#Emergency Contact Info
$ecInfo = $lsr->getParentGuardianInfo($studentID, "E");
$ec_CH_Name = intranet_htmlspecialchars($ecInfo['NAME_C']);
$ec_EN_Name = intranet_htmlspecialchars($ecInfo['NAME_E']);
$CH_Name_HTML = "<input type=\"text\" name=\"ec_NAME_C\" id=\"ec_NAME_C\" VALUE=\"$ec_CH_Name\" readonly=\"readonly\" style=\"border: 0px\">";
$EN_Name_HTML = "<input type=\"text\" name=\"ec_NAME_E\" id=\"ec_NAME_E\" VALUE=\"$ec_EN_Name\" readonly=\"readonly\" style=\"border: 0px\">";  
$ec_HOME_TEL = "<input type=\"text\" name=\"ec_HOME_TEL\" id=\"ec_HOME_TEL\" VALUE=\"".$ecInfo['TEL']."\" readonly=\"readonly\" style=\"border: 0px\">";
$ec_MOBILE_TEL = "<input type=\"text\" name=\"ec_MOBILE_TEL\" id=\"ec_MOBILE_TEL\" VALUE=\"".$ecInfo['MOBILE']."\" readonly=\"readonly\" style=\"border: 0px\">";
*/

// # Registry Status
// switch($result[0]['RecordStatus']) {
// 	case 0: $ownRegistryStatus = $Lang['StudentRegistry']['StatusSuspended'];
// 			break;
// 	case 1: $ownRegistryStatus = $Lang['StudentRegistry']['StatusApproved'];
// 			break;
// 	case 2: $ownRegistryStatus = $Lang['StudentRegistry']['StatusSuspended'];
// 			break;
// 	case 3: $ownRegistryStatus = $Lang['StudentRegistry']['StatusLeft'];
// 			break;
// 	default: $ownRegistryStatus = "---";	
// }

/*
if($Past_Enroll)
{
	$Past_Enroll_html = "";
	for($i=0;$i<sizeof($Past_Enroll);$i++)
	{
		$Past_Enroll_html .= "<tr>
        						<td><em>(".$Lang['StudentRegistry']['AcademicYear'].")</em></td>
								<td><span class=\"row_content\">".$Past_Enroll[$i]['YearName']."</span></td>
        						<td><em>(".$Lang['StudentRegistry']['Grade'].")</em></td>
								<td><span class=\"row_content\">".$Past_Enroll[$i]['Grade']."</span></td>								
								<td><em>(".$Lang['StudentRegistry']['Class'].")</em></td>
								<td><span class=\"row_content\">".$Past_Enroll[$i]['Class']."</span></td>
								<td><em>(".$Lang['StudentRegistry']['InClassNumber'].")</em></td>
								<td><span class=\"row_content\">".$Past_Enroll[$i]['ClassNumber']."</span></td>
                        	 </tr>";
	}
}
*/

#Modified By
$ModifiedBy = $result[0]['ModifyBy']? $lsr -> RETRIEVE_MODIFIED_BY_INFO($result[0]['ModifyBy']) : "";

$CurrentPageArr['StudentRegistry'] = 1;
$CurrentPage = "Mgmt_RegistryInfo";
$TAGS_OBJ[] = array($Lang['Menu']['AccountMgmt']['StudentRegistryInfo'], "index.php");
$MODULE_OBJ['title'] = $Lang['Header']['Menu']['StudentRegistry'];
$MODULE_OBJ = $lsr->GET_MODULE_OBJ_ARR();

if(!$_SESSION['ParentFillOnlineReg'])
{
	# navigation bar 
	$PAGE_NAVIGATION[] = array($Lang['StudentRegistry']['ClassList'], "index.php?AcademicYearID=".$AcademicYearID);
	$PAGE_NAVIGATION[] = array(($result[0]['ClassName'] ? $result[0]['ClassName'] : '')." ".$Lang['StudentRegistry']['StudentList'], ($result[0]['YearClassID']) ? "class.php?AcademicYearID=$AcademicYearID&targetClass=::".$result[0]['YearClassID'] : '');
	$PAGE_NAVIGATION[] = array(($intranet_session_language == "en"? $result[0]['EnglishName'] : $result[0]['ChineseName']), "");
}

#Save and Cancel Button
$SaveBtn   = $linterface->GET_ACTION_BTN($button_save, "button", "javascript:checksubmit(this.form)", "SaveBtn", "style=\"display: none\"");
$SaveSubmitBtn   = $linterface->GET_ACTION_BTN($Lang['Button']['SaveSubmit'], "button", "javascript:document.form1.confirm_flag.value=1; checksubmit(this.form)", "SaveSubmitBtn", "style=\"display: none\"");
$CancelBtn = $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:ResetInfo()", "CancelBtn", "style=\"display: none\"");

########## Official Photo
$photo_link = "/images/myaccount_personalinfo/samplephoto.gif";
if ($li->PhotoLink !="")
{
    if (is_file($intranet_root.$li->PhotoLink))
    {
        $photo_link = $li->PhotoLink;
    }
}
########## Personal Photo
$photo_personal_link = "/images/myaccount_personalinfo/samplephoto.gif";
if ($li->PersonalPhotoLink !="")
{
    if (is_file($intranet_root.$li->PersonalPhotoLink))
    {
        $photo_personal_link = $li->PersonalPhotoLink;
   }
}

########## Custom Column
$custCols = $lsr->getCustomColumnValueByUserID($studentID);
$cCols = $lsr->getCustomColumn();
if(sizeof($custCols) < sizeof($cCols)){
    $tempCols = array();
    foreach($custCols as $ccol){
        $tempCols[$ccol['Code']] = $ccol;
    }
    foreach($cCols as $k => $ccol){
        if($tempCols[$ccol['Code']])
            $cCols[$k] = $tempCols[$ccol['Code']];
    }
    $custCols = $cCols;
}
if($xmsg==1) $msg = $Lang['General']['ReturnMessage']['UpdateSuccess'];
$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$xmsg]);
?>

<script language="javascript">
<!--
	function EditInfo()
	{
		$('#LastModified').attr('style', 'display: none');
		$('#EditBtn').attr('style', 'visibility: hidden');
		
		$('.Edit_Hide').attr('style', 'display: none');
		$('.Edit_Show').attr('style', '');
		
		$('.tabletextrequire').attr('style', '');
		$('#SaveBtn').attr('style', '');
		$('#SaveSubmitBtn').attr('style', '');
		$('#CancelBtn').attr('style', '');
		$('#FormReminder').attr('style', '');
		
		$(':text').attr('style', '');
		$(':text').attr('readonly', '');
// 		$(':text[value="--"]').val('');

		enable_BSTable();
	}
	
	function ResetInfo()
	{
		document.form1.reset();
		$('#LastModified').attr('style', '');
		$('#EditBtn').attr('style', '');
		
		$('.Edit_Hide').attr('style', '');
		$('.Edit_Show').attr('style', 'display: none');
		
		$('.tabletextrequire').attr('style', 'display: none');
		$('#SaveBtn').attr('style', 'display: none');
		$('#SaveSubmitBtn').attr('style', 'display: none');
		$('#CancelBtn').attr('style', 'display: none');
		$('#FormReminder').attr('style', 'display: none');
		
		$(':text').attr('style', 'border: 0px');
		$(':text').attr('readonly', 'readonly');
// 		$(':text[value=""]').val('--');
		
		$('#ErrorLog').html('');
	}

	function checksubmit(obj)
	{
		<? if(!$_SESSION['ParentFillOnlineReg']) { ?> 
		// English name, Chinese Name
		if(!check_text(obj.english_name,"<?=$i_alert_pleasefillin." ".$Lang['StudentRegistry']['EnglishName']?>"))	return false;
		if(!check_text(obj.chinese_name,"<?=$i_alert_pleasefillin." ".$Lang['StudentRegistry']['ChineseName']?>"))	return false;
		<? } ?>
		
		<? 
		# for parent fill-in checking only 
		if($_SESSION['ParentFillOnlineReg']) { ?> 
		// compulsory field: English first name, last name
		if(!check_text(obj.first_name,"<?=$i_alert_pleasefillin." ".$i_UserFirstName?>"))	return false;
		if(!check_text(obj.last_name,"<?=$i_alert_pleasefillin." ".$i_UserLastName?>"))	return false;
		 
		// compulsory field: gender
		if(!countChecked(obj, "gender"))
		{
			alert("<?=$i_alert_pleaseselect?> <?=$Lang['StudentRegistry']['Gender']?>");
			obj.gender[0].focus();
			return false;
		}
		
		// compulsory field: DOB 
		if(!check_text(obj.date_of_birth,"<?=$i_alert_pleasefillin." ".$Lang['StudentRegistry']['BirthDate']?>"))	return false;
		
		// compulsory field: Identification Document No. 
		if(!check_text(obj.id_no,"<?=$i_alert_pleasefillin." ".$Lang['StudentRegistry']['ID_DocumentNo']?>"))	return false;
		
		// compulsory field: Home Phone No.
		if(!check_text(obj.home_tel_no,"<?=$i_alert_pleasefillin." ".$Lang['StudentRegistry']['HomePhoneNo']?>"))	return false;

		// compulsory field: Mobile Phone No.
		if(!check_text(obj.mobile_no,"<?=$i_alert_pleasefillin." ".$Lang['StudentRegistry']['MobilePhoneNo']?>"))	return false;
		
		// compulsory field: 1st Guardian > Relationship with Student
		if(!countChecked(obj, "guardian_type1"))
		{
			alert("<?=$i_alert_pleaseselect?> <?=$Lang['StudentRegistry']['GuardianStudentRelationship']?>");
			obj.guardian_type1[0].focus();
			return false;
		}
		if(obj.guardian_type1[2].checked)
		{
			if(!check_text(obj.guardian_type_others1,"<?=$i_alert_pleasefillin." ".$Lang['StudentRegistry']['GuardianStudentRelationship']?>"))	return false;
		}
		
		// compulsory field: 1st Guardian > Email
		if(!check_text(obj.guardian_email1,"<?=$i_alert_pleasefillin." ".$i_UserEmail?>"))	return false;
		
		// compulsory field: 1st Guardian > English Name
		if(!check_text(obj.guardian_english_name1,"<?=$i_alert_pleasefillin." ".$Lang['StudentRegistry']['EnglishName']?>"))	return false;
		
		// compulsory field: 1st Guardian > Day time Contact Phone No.
		if(!check_text(obj.guardian_tel1,"<?=$i_alert_pleasefillin." ".$Lang['StudentRegistry']['GuardianDayPhone']?>"))	return false;

		// compulsory field: 1st Guardian > Mobile Phone No.
		if(!check_text(obj.guardian_mobile1,"<?=$i_alert_pleasefillin." ".$Lang['StudentRegistry']['GuardianMobile']?>"))	return false;
		<? } ?>
		
		// Chinese Commercial Code
		for(var i=1;i<=4;i++)
		{
			var this_comm = eval("obj.chn_commercial_code"+ i +".value;");
			if(Trim(this_comm))
			{
				if(!is_int(this_comm) || this_comm.length!=4)
				{
					alert("<?=$Lang['General']['JS_warning']['InvalidChineseCommercialCode']?>");
					eval("obj.chn_commercial_code"+ i +".focus();");
					return false;
				}
			}
		}
		
		// Date of Birth
		if(Trim(obj.date_of_birth.value))
		{	
			if(!check_date(obj.date_of_birth,"<?=$Lang['General']['InvalidDateFormat']?>"))	return false;
		}
		
		// home tel
		if(Trim(obj.home_tel_no.value)) 
		{
			if(!is_int(obj.home_tel_no.value) || obj.home_tel_no.value.length!=8)
			{
				alert("<?=$Lang['General']['JS_warning']['InvalidHomeTel']?>");
				obj.home_tel_no.focus();
				return false;
			}
		}
		
		// student & guardian, other guardian > Mobile Phone No.
		if(Trim(obj.mobile_no.value))
		{
			if(!is_int(obj.mobile_no.value) || obj.mobile_no.value.length!=8)
			{
				alert("<?=$Lang['General']['JS_warning']['InvalidMobile']?>");
				obj.mobile_no.focus();
				return false;
			}
		}
		
		for(var i=1;i<=2;i++)
		{
			var this_gmobile = eval("obj.guardian_mobile"+ i +".value;");
			if(Trim(this_gmobile))
			{
				if(!is_int(this_gmobile) || this_gmobile.length!=8)
				{
					alert("<?=$Lang['General']['JS_warning']['InvalidMobile']?>");
					eval("obj.guardian_mobile"+ i +".focus();");
					return false;
				}
			}
		}

		if(Trim(obj.emergency_contact3_mobile.value))
		{
			if(!is_int(obj.emergency_contact3_mobile.value) || obj.emergency_contact3_mobile.value.length!=8)
			{
				alert("<?=$Lang['General']['JS_warning']['InvalidMobile']?>");
				obj.emergency_contact3_mobile.focus();
				return false;
			}
		}
		
		if (($(":input[name='passport_type']").val() =='04') && ($.trim($(":input[name='id_no']").val()) !='') && !check_hkid($(":input[name='id_no']").val())) {
			alert("<?=$Lang['StudentRegistry']['InputValidHKID']?>");
			obj.id_no.focus();
			return false;
		}
		
		$.post(
				'ajax_view_check.php',
				{
					studentID 	: obj.studentID.value,
					student_email : obj.student_email.value,
					passport_type: obj.passport_type.value,
					id_no: obj.id_no.value,
					guardian_email1: obj.guardian_email1.value,
					guardian_email2: obj.guardian_email2.value
				},
				function(data){
					if(data=="")
					{
						// clear bro_sis data if option is no 
						if(document.form1.withBS[1].checked)
						{
							for(this_i = 0; this_i<3; this_i++)
							{
								eval("document.form1.BS_UserLogin_"+ this_i +".value='';");
								eval("document.form1.BS_Name_"+ this_i +".value='';");
								eval("document.form1.BS_Relationship_"+ this_i +".value='';");
							}
						}
						
						obj.submit();
					}
					else
					{
						$('#ErrorLog').html(data);
					}
				});
	}

	
<? if($_SESSION['ParentFillOnlineReg'] && $xmsg) {?>
if(window.opener!=null)
	window.opener.location.reload();
<? } ?>	
	
function display_gAddress(g)
{
	cb = eval("document.form1.gSameAsStudent"+ g +".checked;");
	if(cb)
		eval("$('#gAddressDiv"+ g +"').attr('style', 'display: none');");
	else
		eval("$('#gAddressDiv"+ g +"').attr('style', '');");
}

function enable_BSTable()
{
	//var withBS = document.form1.withBS.value;
	//alert(document.form1.withBS[0].checked);
	//alert(document.form1.withBS[1].checked);
	
	if(document.form1.withBS[0].checked)
	{
	//alert("yes");
		$('#BSTable').attr('style', '');
		//$('#FormReminder').attr('style', '');
	}
	else
	{
	//alert("no");
		$('#BSTable').attr('style', 'display:none');
	}
}

/*
	hkid format:  A123456(7)
	A1234567
	AB123456(7)
	AB1234567
*/
function check_hkid(hkid) {
	hkid = $.trim(hkid);
	hkid = hkid.replace(/\s/g, '');
	hkid = hkid.toUpperCase();
	$(":input[name='id_no']").val(hkid);
	
	re = /^([A-Z]{1,2})((\d){6})\({0,1}([A0-9]{1})\){0,1}$/g;
	var ra = re.exec(hkid);

	if (ra != null) {
		var p1 = ra[1];
		var p2 = ra[2];
		var p3 = ra[4];
		var check_sum = 0;
		if (p1.length == 2) {
			check_sum = (p1.charCodeAt(0)-55) * 9 + (p1.charCodeAt(1)-55) * 8;
		}
		else if (p1.length == 1){
			check_sum = 324 + (p1.charCodeAt(0)-55) * 8;
		}

		check_sum += parseInt(p2.charAt(0)) * 7 + parseInt(p2.charAt(1)) * 6 + parseInt(p2.charAt(2)) * 5 + parseInt(p2.charAt(3)) * 4 + parseInt(p2.charAt(4)) * 3 + parseInt(p2.charAt(5)) * 2;
		var check_digit = 11 - (check_sum % 11);
		if (check_digit == '11') {
			check_digit = 0;
		}
		else if (check_digit == '10') {
			check_digit = 'A';
		}
		if (check_digit == p3 ) {
			return true;
		}
		else {
			return false;
		}
	}
	else {
		return false;
	}
}
//-->
</script>

<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
<form name="form1" method="POST" action="view_update.php">
<table width="95%" border="0" cellspacing="0" cellpadding="0">
	<tr> 
		<td class="main_content">
			<div class="navigation">
				<?=$linterface->GET_NAVIGATION($PAGE_NAVIGATION)?>
				<p class="spacer"></p>
			</div>
			<div class="table_board">
        		<div class="table_row_tool row_content_tool">
                	<?=($laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-Manage") || $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] || $_SESSION['ParentFillOnlineReg']) ? $linterface->GET_SMALL_BTN($Lang['StudentRegistry']['Edit'], "button", "javascript:EditInfo();", "EditBtn") : "&nbsp;" ?>
                </div>
                <p class="spacer"></p>
                
                <? ########################################################### ?>
				<? #  Student Information                                    # ?>
				<? ########################################################### ?>
                <div class="form_sub_title_v30">
                	<em>- <span class="field_title"><?= $Lang['StudentRegistry']['StudInfo'] ?></span> -</em>
                    <p class="spacer"></p>
                </div>
                <table class="form_table_v30">
                	<col class="field_title_short">
                    <col class="field_content_short">
                    <col class="field_title_short">
                    <col class="field_content_short">
                    <col class="field_title_short">
                    <col class="field_content_short">
					<tbody>
                        <tr>
                            <td class="field_title_short"><?= $Lang['StudentRegistry']['EnglishName'] ?></td>
                            <td colspan="3">
                    			<div class="form_field_sub_content">
                     		       	<table class="inside_form_table">
                     		       		<tr>
                     		       			<td><em>(<?=!$_SESSION['ParentFillOnlineReg'] ? '<span class="tabletextrequire" style="display: none">*</span>' : ''?><?= $i_UserDisplayName ?>)</em></td>
                     		       			<td>
                     		       				<span class="Edit_Hide"><?=$result[0]['EnglishName']?></span>
                     		       				<span class="Edit_Show" style="display:none"><?=$english_name_data?></span>
                     		       			</td>
                 		       			</tr>
                 		       			<tr>
                     		       			<td><em>(<?=$_SESSION['ParentFillOnlineReg'] ? '<span class="tabletextrequire" style="display: none">*</span>' : ''?><?= $i_UserFirstName ?>)</em></td>
                     		       			<td>
                     		       				<span class="Edit_Hide"><?= $result[0]['FirstName'] ? $result[0]['FirstName'] : "--"?></span>
                     		       				<span class="Edit_Show" style="display:none"><input type="text" name="first_name" value="<?= $result[0]['FirstName']?>"/></span>
                     		       			</td>
                     		       			
                     		       			<td><em>(<?=$_SESSION['ParentFillOnlineReg'] ? '<span class="tabletextrequire" style="display: none">*</span>' : ''?><?= $i_UserLastName ?>)</em></td>
                     		       			<td>
                     		       				<span class="Edit_Hide"><?= $result[0]['LastName'] ? $result[0]['LastName'] : "--"?></span>
                     		       				<span class="Edit_Show" style="display:none"><input type="text" name="last_name" value="<?= $result[0]['LastName']?>"/></span>
                     		       			</td>
                        	    		</tr>
                        	    	</table>
								</div>
								</td>
                        	
							<td rowspan="10">
								<? if(!$_SESSION['ParentFillOnlineReg']) { ?> 
	                            	<?=$Lang['Personal']['OfficialPhoto']?><br /><img src="<?=$photo_link?>" width="100" height="130"><br>
	                            <? } ?>
	                            <? if(!$_SESSION['ParentFillOnlineReg']) { ?> 
	                        		<?=$Lang['Personal']['PersonalPhoto']?><br /><img src="<?=$photo_personal_link?>" width="100" height="130">
	                        	<? } ?>
                        	</td>
                        </tr>
                        
                        <tr>
                        	<td class="field_title_short"><?= $Lang['StudentRegistry']['ChineseName'] ?></td>
							<td colspan="3">
                    			<div class="form_field_sub_content">
                     		       	<table class="inside_form_table">
                     		       		<tr>
                     		       			<td><em>(<?=!$_SESSION['ParentFillOnlineReg'] ? '<span class="tabletextrequire" style="display: none">*</span>' : ''?><?= $i_UserDisplayName ?>)</em></td>
                     		       			<td>
                     		       				<span class="Edit_Hide"><?=$result[0]['ChineseName']?></span>
                     		       				<span class="Edit_Show" style="display:none"><?=$chinese_name_data?></span>
                     		       			</td>
                     		       			
                 		       			</tr>
                 		       			<tr>
                     		       			<td><em>(<?= $i_UserFirstName ?>)</em></td>
                     		       			<td>
                     		       				<span class="Edit_Hide"><?= $result[0]['CFirstName'] ? $result[0]['CFirstName'] : "--"?></span>
                     		       				<span class="Edit_Show" style="display:none"><input type="text" name="cfirst_name" value="<?= $result[0]['CFirstName']?>"/></span>
                     		       			</td>
                     		       			
                     		       			<td><em>(<?= $i_UserLastName ?>)</em></td>
                     		       			<td>
                     		       				<span class="Edit_Hide"><?= $result[0]['CLastName'] ? $result[0]['CLastName'] : "--"?></span>
                     		       				<span class="Edit_Show" style="display:none"><input type="text" name="clast_name" value="<?= $result[0]['CLastName']?>"/></span>
                     		       			</td>
                        	    		</tr>
                        	    	</table>
								</div>
								</td>
								
                        </tr>
                        
						<tr>
							<td class="field_title_short"><?= $Lang['StudentRegistry']['ChineseCommercialCode'] ?></td>
							<td colspan="3">
								<span class="Edit_Hide"><?= $result[0]['CHN_COMMERCIAL_CODE1'] ? $result[0]['CHN_COMMERCIAL_CODE1']. " " . $result[0]['CHN_COMMERCIAL_CODE2']. " " . $result[0]['CHN_COMMERCIAL_CODE3']. " " . $result[0]['CHN_COMMERCIAL_CODE4']. " " : "--"?></span>
								<span class="Edit_Show" style="display:none">
									<input type="text" name="chn_commercial_code1" class="textboxnum" maxlength="4" value="<?= $result[0]['CHN_COMMERCIAL_CODE1']?>"/>
									<input type="text" name="chn_commercial_code2" class="textboxnum" maxlength="4" value="<?= $result[0]['CHN_COMMERCIAL_CODE2']?>"/>
									<input type="text" name="chn_commercial_code3" class="textboxnum" maxlength="4" value="<?= $result[0]['CHN_COMMERCIAL_CODE3']?>"/>
									<input type="text" name="chn_commercial_code4" class="textboxnum" maxlength="4" value="<?= $result[0]['CHN_COMMERCIAL_CODE4']?>"/>
								</span>
                            </td>
                        </tr>
                        
                        <tr>
                            <td class="field_title_short"><?=$_SESSION['ParentFillOnlineReg'] ? '<span class="tabletextrequire" style="display: none">*</span>' : ''?><?= $Lang['StudentRegistry']['Gender'] ?></td>
                            <td>
                            	<span class="Edit_Hide"><?= $gender_val ?></span>
                            	<span class="Edit_Show" style="display:none"><?= $gender_RBL_html ?></span>
                            </td>
                            <td class="field_title_short"><?=$_SESSION['ParentFillOnlineReg'] ? '<span class="tabletextrequire" style="display: none">*</span>' : ''?><?= $Lang['StudentRegistry']['BirthDate'] ?></td>
                            <td>
                            	<span class="Edit_Hide"><?= $result[0]['DOB']=="0000-00-00" ? "" : $result[0]['DOB'] ?></span>
                            	<span class="Edit_Show" style="display:none">
                            	<input type="text" name="date_of_birth" value="<?= $result[0]['DOB']=="0000-00-00" ? "" : $result[0]['DOB']  ?>"/> <span class="tabletextremark">(YYYY-MM-DD)</span>
                            	<?// echo $linterface->GET_DATE_PICKER("date_of_birth", $result[0]['DOB']) ?>
                            	</span>
                            </td>
                        </tr>
                        
                        <? /* ?>
                        <tr>
							<td class="field_title_short"><?= $Lang['StudentRegistry']['BirthDate'] ?></td>
                            <td>
                            	<span class="Edit_Hide"><?= $result[0]['DOB']=="0000-00-00" ? "" : $result[0]['DOB'] ?></span>
                            	<span class="Edit_Show" style="display:none"><? echo $linterface->GET_DATE_PICKER("date_of_birth", $result[0]['DOB']) ?></span>
                            </td>
                            <td class="field_title_short"><?= $Lang['StudentRegistry']['Province'] ?></td>
                            <td>
                            	<span class="Edit_Hide"><?= $result[0]['ORIGIN']?></span>
                            	<span class="Edit_Show" style="display:none"><input type="text" name="origin" value="<?= $result[0]['ORIGIN'] ?>"/></span>
                            </td>
                        </tr>
                        <? */ ?>
                        
                        <tr>
							
                            <td class="field_title_short"><?=$_SESSION['ParentFillOnlineReg'] ? '<span class="tabletextrequire" style="display: none">*</span>' : ''?><?= $profile_nationality ?></td>
                            <td>
                            	<span class="Edit_Hide"><?= $student_nationality_data ?></span>
                            	<span class="Edit_Show" style="display:none"><?=$student_nationality_options?></span>
                            </td>
                            <td class="field_title_short"><?= $Lang['StudentRegistry']['Race'] ?></td>
                            <td>
                            	<span class="Edit_Hide"><?= $Ethnicity_data?></span>
                            	<span class="Edit_Show" style="display:none"><?=$Ethnicity_data_options?></span>
                            </td>
                            
                       <tr>     
                            <td class="field_title_short"><?=$_SESSION['ParentFillOnlineReg'] ? '<span class="tabletextrequire" style="display: none">*</span>' : ''?><?= $Lang['StudentRegistry']['ID_DocumentType'] ?></td>
                            <td>
                            	<span class="Edit_Hide"><?= $ID_DATA?></span>
                            	<span class="Edit_Show" style="display:none"><?=$ID_DATA_OPTIONS?></span>
                            </td>
                            <td class="field_title_short"><?=$_SESSION['ParentFillOnlineReg'] ? '<span class="tabletextrequire" style="display: none">*</span>' : ''?><?= $Lang['StudentRegistry']['ID_DocumentNo'] ?></td>
                            <td>
                            	<span class="Edit_Hide"><?= $result[0]['ID_NO']?></span>
                            	<span class="Edit_Show" style="display:none"><input type="text" name="id_no" value="<?= $result[0]['ID_NO']?>"/></span>
                            </td>
                        </tr>
                        
                        <tr>
							<td class="field_title_short"><?= $Lang['StudentRegistry']['HomeLang'] ?></td>
                            <td>
                            	<span class="Edit_Hide"><?= $HomeLang_data?></span>
                            	<span class="Edit_Show" style="display:none"><?= $HomeLang_data_options?></span>
                            </td>
                            <td class="field_title_short"><?= $Lang['StudentRegistry']['Religion'] ?></td>
                            <td>
                            	<span class="Edit_Hide"><?= $Religion_data?></span>
                            	<span class="Edit_Show" style="display:none"><?= $Religion_data_options?></span>
                            </td>
                        </tr>
                        
                        <tr>
                            <td class="field_title_short"><?= $Lang['StudentRegistry']['Church'] ?></td>
                            <td colspan="4">
                            	<span class="Edit_Hide"><?= $result[0]['CHURCH'] ?></span>
                            	<span class="Edit_Show" style="display:none"><input type="text" class="textboxtext" name="church" value="<?= $result[0]['CHURCH']?>"/></span>
                            </td>
                        </tr>
                        
                        <tr>
							<td class="field_title_short"><?=$_SESSION['ParentFillOnlineReg'] ? '<span class="tabletextrequire" style="display: none">*</span>' : ''?><?= $Lang['StudentRegistry']['HomePhoneNo'] ?></td>
                            <td>
                            	<span class="Edit_Hide"><?= $result[0]['HomeTelNo']?></span>
                            	<span class="Edit_Show" style="display:none"><input type="text" name="home_tel_no" value="<?= $result[0]['HomeTelNo']?>" maxlength="8"/></span>
                            </td>
                            <td class="field_title_short"><?=$_SESSION['ParentFillOnlineReg'] ? '<span class="tabletextrequire" style="display: none">*</span>' : ''?><?= $Lang['StudentRegistry']['MobilePhoneNo'] ?></td>
                            <td>
                            	<span class="Edit_Hide"><?= $result[0]['MobileTelNo'] ?></span> 
                            	<span class="Edit_Show" style="display:none"><input type="text" name="mobile_no" value="<?= $result[0]['MobileTelNo']?>" maxlength="8" /></span>
                            </td>
                        </tr>
                        
                        <tr>
							<td class="field_title_short"><span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['EMailAddress'] ?></td>
                            <td colspan="4">
                            	<span class="Edit_Hide"><?= $result[0]['UserEmail'] ?></span>
                            	<span class="Edit_Show" style="display:none"><input type="text" class="textboxtext" name="student_email" value="<?= $lsr->isStudentRegRecordExists($studentID) ? $result[0]['UserEmail'] : "" ?>"/></span>
                            </td>
                        </tr>
                        
                        <tr>
							<td class="field_title_short"><?= $Lang['StudentRegistry']['Address'] ?></td>
                            <td colspan="3">
                            			<div class="form_field_sub_content">
		                     		       	<table class="inside_form_table">
		                     		       		<tr>
		                     		       			<td><em>(<?= $Lang['StudentRegistry']['AddressField']['Flat'] ?>)</em></td>
		                     		       			<td>
		                     		       				<span class="Edit_Hide"><?= $result[0]['ADDRESS_ROOM_EN']?></span>
		                     		       				<span class="Edit_Show" style="display:none"><input type="text" class="textboxnum" name="address_room_en" value="<?= $result[0]['ADDRESS_ROOM_EN']?>"/></span>
		                     		       			</td>
		                     		       			<td><em>(<?= $Lang['StudentRegistry']['AddressField']['Floor'] ?>)</em></td>
		                     		       			<td>
		                     		       				<span class="Edit_Hide"><?= $result[0]['ADDRESS_FLOOR_EN']?></span>
		                     		       				<span class="Edit_Show" style="display:none"><input type="text" class="textboxnum" name="address_floor_en" value="<?= $result[0]['ADDRESS_FLOOR_EN']?>"/></span>
		                     		       			</td>
		                     		       			<td><em>(<?= $Lang['StudentRegistry']['AddressField']['Block'] ?>)</em></td>
		                        	    			<td>
		                        	    				<span class="Edit_Hide"><?= $result[0]['ADDRESS_BLK_EN']?></span>
		                        	    				<span class="Edit_Show" style="display:none"><input type="text" class="textboxnum" name="address_blk_en" value="<?= $result[0]['ADDRESS_BLK_EN']?>"/></span>
		                        	    			</td>
		                        	    		</tr>
		                        	    		<tr>
		                     		       			<td><em>(<?= $Lang['StudentRegistry']['AddressField']['Building'] ?>)</em></td>
		                     		       			<td colspan="5">
		                     		       				<span class="Edit_Hide"><?= $result[0]['ADDRESS_BLD_EN']?></span>
		                     		       				<span class="Edit_Show" style="display:none"><input type="text" class="textboxtext" name="address_bld_en" value="<?= $result[0]['ADDRESS_BLD_EN']?>"/></span>
		                     		       			</td>
		                        	    		</tr>
		                        	    		<tr>
		                     		       			<td><em>(<?= $Lang['StudentRegistry']['AddressField']['Estate'] ?>)</em></td>
		                     		       			<td colspan="5">
		                     		       				<span class="Edit_Hide"><?= $result[0]['ADDRESS_EST_EN']?></span>
		                     		       				<span class="Edit_Show" style="display:none"><input type="text" class="textboxtext" name="address_est_en" value="<?= $result[0]['ADDRESS_EST_EN']?>"/></span>
		                     		       			</td>
		                        	    		</tr>
		                        	    		<tr>
		                     		       			<td><em>(<?= $Lang['StudentRegistry']['AddressField']['Street'] ?>)</em></td>
		                     		       			<td colspan="5">
		                     		       				<span class="Edit_Hide"><?= $result[0]['ADDRESS_STR_EN']?></span>
		                     		       				<span class="Edit_Show" style="display:none"><input type="text" class="textboxtext" name="address_str_en" value="<?= $result[0]['ADDRESS_STR_EN']?>"/></span>
		                     		       			</td>
		                        	    		</tr>
		                        	    		<tr>
		                     		       			<td><em>(<?= $Lang['StudentRegistry']['AddressField']['District'] ?>)</em></td>
		                     		       			<td colspan="5">
		                     		       				<span class="Edit_Hide"><?= $student_district_data?></span>
		                     		       				<span class="Edit_Show" style="display:none"><?=$student_district_options?></span>
		                     		       			</td>
		                        	    		</tr>
		                        	    		<tr>
		                     		       			<td><em>(<?= $Lang['StudentRegistry']['AddressField']['Area'] ?>)</em></td>
		                     		       			<td colspan="5">
		                     		       				<span class="Edit_Hide"><?= $Lang['StudentRegistry']['AreaOptions'][$result[0]['ADDRESS_AREA']-1]?></span>
		                     		       				<span class="Edit_Show" style="display:none"><?=$AddressAreaOptions?></span>
		                     		       			</td>
		                        	    		</tr>
		                        	    	</table>
										</div>
										</td>
                        </tr>
                        <?php foreach($custCols as $ccol){
                        	if($ccol['Section'] == 'StudInfo'){?>                        
                        	<tr>
                        		<td class="field_title_short"><?php echo $ccol["DisplayText_".Get_Lang_Selection('ch','en')]?></td>
                        		<td colspan="4">
                        			<span class="Edit_Hide"><?php echo $ccol['Value']?></span>
                        			<span class="Edit_Show" style="display:none"><input type="text" class="textboxtext" name="<?php echo $ccol[Code]?>" value="<?php echo $ccol['Value']?>"/></span>
                        			<input type="hidden" name="custCol[<?php echo $ccol['Code']?>]" value="<?php echo $ccol['ColumnID']?>" />
                        		</td>
                        	</tr>
                        <?php }
                        }?>
                       
					</tbody>
					<col class="field_title_short">
                    <col class="field_c">
				</table>
				
				
				<? ########################################################### ?>
				<? #  Last School 	                                         # ?>
				<? ########################################################### ?>
				<div class="form_sub_title_v30">
    				<em>- <span class="field_title"><?=$Lang['StudentRegistry']['LastSchool']?></span> -</em>
					<p class="spacer"></p>
				</div>
				<table class="form_table_v30">
					<tbody>
                        <tr>
                            <td class="field_title_short"><?= $Lang['General']['EnglishName2'] ?></td>
                            <td>
                            	<span class="Edit_Hide"><?= $result[0]['LAST_SCHOOL_EN']?></span>
                            	<span class="Edit_Show" style="display:none"><input type="text" class="textboxtext" name="last_school_en" value="<?= $result[0]['LAST_SCHOOL_EN']?>"/></span>
                            	</td>
                        </tr>
                        <tr>
							<td class="field_title_short"><?= $Lang['General']['ChineseName2'] ?></td>
                            <td>
                            	<span class="Edit_Hide"><?= $result[0]['LAST_SCHOOL_CH']?></span>
                            	<span class="Edit_Show" style="display:none"><input type="text" class="textboxtext" name="last_school_ch" value="<?= $result[0]['LAST_SCHOOL_CH']?>"/></span>
                            </td>
                        </tr>
                        <?php foreach($custCols as $ccol){
                        	if($ccol['Section'] == 'LastSchool'){?>                        
                        	<tr>
                        		<td class="field_title_short"><?php echo $ccol["DisplayText_".Get_Lang_Selection('ch','en')]?></td>
                        		<td>
                        			<span class="Edit_Hide"><?php echo $ccol['Value']?></span>
                        			<span class="Edit_Show" style="display:none"><input type="text" class="textboxtext" name="<?php echo $ccol[Code]?>" value="<?php echo $ccol['Value']?>"/></span>
                        			<input type="hidden" name="custCol[<?php echo $ccol['Code']?>]" value="<?php echo $ccol['ColumnID']?>" />
                        		</td>
                        	</tr>
                        <?php }
                        }?>
				</tbody>
				</table>
				
				<? /* ?>
				<? ########################################################### ?>
				<? #  Class Level & Class No.                                # ?>
				<? ########################################################### ?>
				<div class="form_sub_title_v30">
    				<em>- <span class="field_title"><?=$Lang['StudentRegistry']['LastClassLevelClassNo']?></span> -</em>
					<p class="spacer"></p>
				</div>
				<table class="form_table_v30">
					<tbody>
                        <tr>
                            <td>
                            	<span class="Edit_Hide"><?=$LastAcademicYear_data_table?></span>
                            	<span class="Edit_Show" style="display:none;"><?=$LastAcademicYear_table?></span>
                            </td>
                        </tr>
				</tbody>
				</table>
				<? */ ?>
				
				<? ########################################################### ?>
				<? #  Brothers & Sistes currently studying                   # ?>
				<? ########################################################### ?>
				<div class="form_sub_title_v30">
    				<em>- <span class="field_title"><?=$Lang['StudentRegistry']['BrotherSisterStudyingInSchool']?></span> -</em>
					<p class="spacer"></p>
				</div>
				<table class="form_table_v30">
					<tbody>
                        <tr>
                            <td>
                            	<span class="Edit_Hide"><?=$BroSis_data_table?></span>
                            	<span class="Edit_Show" style="display:none;">
								
								<?=$BroSis_table?>
								</span>
                            </td>
                        </tr>
				</tbody>
				</table>
				
				<? ########################################################### ?>
				<? #  Guardian                                               # ?>
				<? ########################################################### ?>
				<? for($g=1;$g<=2;$g++)  { ?>
				<div class="form_sub_title_v30">
    				<em>- <span class="field_title"><?= ($g==1) ?$Lang['StudentRegistry']['1st'] :$Lang['StudentRegistry']['2nd'] ?><?= $Lang['Identity']['Guardian'] ?></span> -</em>
					<p class="spacer"></p>
				</div>
				<table class="form_table_v30">
					<tbody>
                        <tr>
							<td class="field_title_short"><?=$_SESSION['ParentFillOnlineReg'] && $g==1 ? '<span class="tabletextrequire" style="display: none">*</span>' : ''?><?= $Lang['StudentRegistry']['GuardianStudentRelationship'] ?></td>
                            <td>
                            	<span class="Edit_Hide"><?=$gType[$g]?></span>
                            	<span class="Edit_Show" style="display:none"><?=$guardian_type[$g]?></span>
                            </td>
                            <td class="field_title_short"><?=$_SESSION['ParentFillOnlineReg'] && $g==1 ? '<span class="tabletextrequire" style="display: none">*</span>' : ''?><?= $i_UserEmail ?></td>
                            <td>
                            	<span class="Edit_Hide"><?= $gEmail[$g] ?></span>
                            	<span class="Edit_Show" style="display:none"><?=$guardian_email[$g]?></span>
                            </td>
                        </tr>
						<tr>
                         	<td class="field_title_short"><?=$_SESSION['ParentFillOnlineReg'] && $g==1 ? '<span class="tabletextrequire" style="display: none">*</span>' : ''?><?= $Lang['StudentRegistry']['EnglishName'] ?></td>
                            <td>
                           		<span class="Edit_Hide"><?= $gEnglishName[$g] ?></span>
                            	<span class="Edit_Show" style="display:none"><?=$guardian_english_name[$g]?></span>
                            </td>
                            <td class="field_title_short"><?= $Lang['StudentRegistry']['ChineseName'] ?></td>
                            <td>
                            	<span class="Edit_Hide"><?= $gChineseName[$g] ?></span>
                            	<span class="Edit_Show" style="display:none"><?=$guardian_chinese_name[$g]?></span>
                            </td>
                        </tr>
                        <tr>
                        <? /* ?>
                         	<td class="field_title_short"><?= $i_HKID ?></td>
                            <td>
                            	<span class="Edit_Hide"><?= $gHKID[$g] ?></span>
                            	<span class="Edit_Show" style="display:none"><?=$guardian_hkid[$g]?></span>	
                           	</td>
						<? */ ?>
                            <td class="field_title_short"><?= $i_StudentGuardian_Occupation ?></td>
                            <td colspan="3">
                            	<span class="Edit_Hide"><?= $gOccupation[$g] ?></span>
                            	<span class="Edit_Show" style="display:none"><?=$guardian_occupation[$g]?></span>	
                           	</td>
                        </tr>
                         <tr>
                        	<td class="field_title_short"><?=$_SESSION['ParentFillOnlineReg'] && $g==1 ? '<span class="tabletextrequire" style="display: none">*</span>' : ''?><span class="field_title"><?= $Lang['StudentRegistry']['GuardianDayPhone'] ?></span></td>
                        	<td>
                            	<span class="Edit_Hide"><?= $gTel[$g] ?></span>
                            	<span class="Edit_Show" style="display:none"><?=$guardian_tel[$g]?></span>	
                           	</td>
                        	<td class="field_title_short"><?=$_SESSION['ParentFillOnlineReg'] && $g==1 ? '<span class="tabletextrequire" style="display: none">*</span>' : ''?><span class="field_title"><?= $Lang['StudentRegistry']['GuardianMobile'] ?></span></td>
                         	<td>
                            	<span class="Edit_Hide"><?= $gMobile[$g] ?></span>
                            	<span class="Edit_Show" style="display:none"><?=$guardian_mobile[$g]?></span>	
                           	</td>
						</tr>
						<tr>
							<td class="field_title_short"><?= $Lang['StudentRegistry']['Religion'] ?></td>
                            <td>
                            	<span class="Edit_Hide"><?= $gReligion[$g] ?></span>
                            	<span class="Edit_Show" style="display:none"><?=$guardian_religion[$g]?></span>	
                           	</td>
                            <td class="field_title_short"><?=$Lang['StudentRegistry']['PreviousYearChurch']?></td>
                            <td>
                            	<span class="Edit_Hide"><?= $gChurch[$g] ?></span>
                            	<span class="Edit_Show" style="display:none"><?=$guardian_church[$g]?></span>	
                           	</td>
                        </tr>
                        
                        <tr>
							<td class="field_title_short"><?= $Lang['StudentRegistry']['Address'] ?></td>
                            <td colspan="3">
                            	<? /* ?> <span class="Edit_Show" style="display:none"><b>(<?=$Lang['StudentRegistry']['NoNeedCompleteSameAsStudent']?>)</b></span> <? */ ?>
                            	<span class="Edit_Show" style="display:none"><input type="checkbox" value="1" <?=!$gAddressData[$g]? "checked":""?> name="gSameAsStudent<?=$g?>" id="gSameAsStudent<?=$g?>" onClick="display_gAddress(<?=$g?>);"> <label for="gSameAsStudent<?=$g?>"><?=$Lang['StudentRegistry']['AddressSameAsStudent']?></label></span>
                    			<div class="form_field_sub_content" style="<?=!$gAddressData[$g]? "display:none":""?>" id="gAddressDiv<?=$g?>">
                     		       	<table class="inside_form_table">
                     		       		<tr>
                     		       			<td><em>(<?= $Lang['StudentRegistry']['AddressField']['Flat'] ?>)</em></td>
                     		       			<td>
                     		       				<span class="Edit_Hide"><?= $gAddressRoomEn[$g] ?></span>
                     		       				<span class="Edit_Show" style="display:none"><input type="text" class="textboxnum" name="address_room_en<?=$g?>" value="<?= $gAddressRoomEn[$g] ?>"/></span>
                     		       			</td>
                     		       			<td><em>(<?= $Lang['StudentRegistry']['AddressField']['Floor'] ?>)</em></td>
                     		       			<td>
                     		       				<span class="Edit_Hide"><?= $gAddressFloorEn[$g] ?></span>
                     		       				<span class="Edit_Show" style="display:none"><input type="text" class="textboxnum" name="address_floor_en<?=$g?>" value="<?= $gAddressFloorEn[$g] ?>"/></span>
                     		       			</td>
                     		       			<td><em>(<?= $Lang['StudentRegistry']['AddressField']['Block'] ?>)</em></td>
                        	    			<td>
                        	    				<span class="Edit_Hide"><?= $gAddressBlkEn[$g] ?></span>
                        	    				<span class="Edit_Show" style="display:none"><input type="text" class="textboxnum" name="address_blk_en<?=$g?>" value="<?= $gAddressBlkEn[$g] ?>"/></span>
                        	    			</td>
                        	    		</tr>
                        	    		<tr>
                     		       			<td><em>(<?= $Lang['StudentRegistry']['AddressField']['Building'] ?>)</em></td>
                     		       			<td colspan="5">
                     		       				<span class="Edit_Hide"><?= $gAddressBldEn[$g] ?></span>
                     		       				<span class="Edit_Show" style="display:none"><input type="text" class="textboxtext" name="address_bld_en<?=$g?>" value="<?= $gAddressBldEn[$g] ?>"/></span>
                     		       			</td>
                        	    		</tr>
                        	    		<tr>
                     		       			<td><em>(<?= $Lang['StudentRegistry']['AddressField']['Estate'] ?>)</em></td>
                     		       			<td colspan="5">
                     		       				<span class="Edit_Hide"><?= $gAddressEstEn[$g] ?></span>
                     		       				<span class="Edit_Show" style="display:none"><input type="text" class="textboxtext" name="address_est_en<?=$g?>" value="<?= $gAddressEstEn[$g] ?>"/></span>
                     		       			</td>
                        	    		</tr>
                        	    		<tr>
                     		       			<td><em>(<?= $Lang['StudentRegistry']['AddressField']['Street'] ?>)</em></td>
                     		       			<td colspan="5">
                     		       				<span class="Edit_Hide"><?= $gAddressStrEn[$g] ?></span>
                     		       				<span class="Edit_Show" style="display:none"><input type="text" class="textboxtext" name="address_str_en<?=$g?>" value="<?= $gAddressStrEn[$g] ?>"/></span>
                     		       			</td>
                        	    		</tr>
                        	    		<tr>
                     		       			<td><em>(<?= $Lang['StudentRegistry']['AddressField']['District'] ?>)</em></td>
                     		       			<td colspan="5">
                     		       				<span class="Edit_Hide"><?= $gAddressDistrictEn[$g] ?></span>
                     		       				<span class="Edit_Show" style="display:none"><?=$guardian_district[$g]?></span>
                     		       			</td>
                        	    		</tr>
                        	    		<tr>
                     		       			<td><em>(<?= $Lang['StudentRegistry']['AddressField']['Area'] ?>)</em></td>
                     		       			<td colspan="5">
                     		       				<span class="Edit_Hide"><?= $gAddressArea[$g] ?></span>
                     		       				<span class="Edit_Show" style="display:none"><?=$guardian_address_area[$g]?></span>
                     		       			</td>
                        	    		</tr>
                        	    	</table>
								</div>
							</td>
                        </tr>
                        <?php foreach($custCols as $ccol){
                        	if($ccol['Section'] == 'Guardian'.$g){?>                        
                        	<tr>
                        		<td class="field_title_short"><?php echo $ccol["DisplayText_".Get_Lang_Selection('ch','en')]?></td>
                        		<td colspan="5">
                        			<span class="Edit_Hide"><?php echo $ccol['Value']?></span>
                        			<span class="Edit_Show" style="display:none"><input type="text" class="textboxtext" name="<?php echo $ccol[Code]?>" value="<?php echo $ccol['Value']?>"/></span>
                        			<input type="hidden" name="custCol[<?php echo $ccol['Code']?>]" value="<?php echo $ccol['ColumnID']?>" />
                        		</td>
                        	</tr>
                        <?php }
                        }?>
					</tbody>
				</table>
				<? } ?>
				
				<? ########################################################### ?>
				<? #  Contact Persion in case of emergency                   # ?>
				<? ########################################################### ?>
				<div class="form_sub_title_v30">
    				<em>- <span class="field_title"><?= $Lang['StudentRegistry']['OtherContactPerson'] ?></span> -</em>
					<p class="spacer"></p>
				</div>
				<table class="form_table_v30">
					<col class="field_title_short">
                    <col class="field_content_short">
                    <col class="field_title_short">
                    <col class="field_content_short">
					<tbody>
					<? /* ?>
						<tr>
                        	<td class="field_title_short"><?= $Lang['StudentRegistry']['1st'] ?></td>
                        	<td>
                        		<span class="Edit_Hide"><?= $contact1_data ?></span>
								<span class="Edit_Show" style="display:none">
									<input type='radio' name='emergency_contact1' id='emergency_contact1_1' value='1' <?=$contact_result['Contact1']==1 ? "checked":""?>> <label for='emergency_contact1_1'><?=$Lang['StudentRegistry']['Father']?></label>  
									<input type='radio' name='emergency_contact1' id='emergency_contact1_2' value='2' <?=$contact_result['Contact1']==2 ? "checked":""?>> <label for='emergency_contact1_2'><?=$Lang['StudentRegistry']['Mother']?></label> 
									<input type='radio' name='emergency_contact1' id='emergency_contact1_0' value='0' <?=$contact_result['Contact1']==0 ? "checked":""?>> <label for='emergency_contact1_0'><?=$Lang['StudentRegistry']['Other']?></label> (<?=$Lang['General']['PlsSpecify']?>: <input type='text' name='emergency_contact1_others' value='<?=$contact_result['Contact1_Others']?>'/>)
								</span>
                        	</td>
                        </tr>
                        <tr>
                        	<td class="field_title_short"><?= $Lang['StudentRegistry']['2nd'] ?></td>
                        	<td>
                        		<span class="Edit_Hide"><?= $contact2_data ?></span>
                        		<span class="Edit_Show" style="display:none">
									<input type='radio' name='emergency_contact2' id='emergency_contact2_1' value='1' <?=$contact_result['Contact2']==1 ? "checked":""?>> <label for='emergency_contact2_1'><?=$Lang['StudentRegistry']['Father']?></label>  
									<input type='radio' name='emergency_contact2' id='emergency_contact2_2' value='2' <?=$contact_result['Contact2']==2 ? "checked":""?>> <label for='emergency_contact2_2'><?=$Lang['StudentRegistry']['Mother']?></label> 
									<input type='radio' name='emergency_contact2' id='emergency_contact2_0' value='0' <?=$contact_result['Contact2']==0 ? "checked":""?>> <label for='emergency_contact2_0'><?=$Lang['StudentRegistry']['Other']?></label> (<?=$Lang['General']['PlsSpecify']?>: <input type='text' name='emergency_contact2_others' value='<?=$contact_result['Contact2_Others']?>'/>)
								</span>
                        	</td>
                        </tr>
                        <? */ ?>
                        <tr>
							<td class="field_title_short"><?= $i_UserTitle ?></td>
                            <td>
                            	<span class="Edit_Hide"><?= $contact3_title_data ?></span>
                            	<span class="Edit_Show" style="display:none">
     		       					<input type="radio" name="emergency_contact3_title" id="emergency_contact3_title1" value="1" <?=$contact_result['Contact3_Title']==1 ? "checked":""?>/> <label for="emergency_contact3_title1"><?=$i_title_mr?></label> 
     		       					<input type="radio" name="emergency_contact3_title" id="emergency_contact3_title2" value="2" <?=$contact_result['Contact3_Title']==2 ? "checked":""?>/> <label for="emergency_contact3_title2"><?=$i_title_mrs?></label> 
     		       					<input type="radio" name="emergency_contact3_title" id="emergency_contact3_title3" value="3" <?=$contact_result['Contact3_Title']==3 ? "checked":""?>/> <label for="emergency_contact3_title3"><?=$i_title_ms?></label> 
     		       				</span>
                           	</td>
                           	<td class="field_title_short"><?= $Lang['StudentRegistry']['Relationship'] ?></td>
                            <td>
                            	<span class="Edit_Hide"><?= $contact_result['Contact3_Relationship'] ?></span>
                            	<span class="Edit_Show" style="display:none"><input type="text" name="emergency_contact3_relationship" value="<?= $contact_result['Contact3_Relationship']?>"/></span>
                           	</td>
                        </tr>
                        
                        <tr>
							<td class="field_title_short"><?= $Lang['General']['EnglishName'] ?></td>
                            <td>
                            	<span class="Edit_Hide"><?= $contact_result['Contact3_EnglishName'] ?></span>
                            	<span class="Edit_Show" style="display:none"><input type="text" name="emergency_contact3_english_name" value="<?= $contact_result['Contact3_EnglishName']?>"/></span>
                           	</td>
                            <td class="field_title_short"><?= $Lang['General']['ChineseName'] ?></td>
                            <td>
                            	<span class="Edit_Hide"><?= $contact_result['Contact3_ChineseName'] ?></span>
                            	<span class="Edit_Show" style="display:none"><input type="text" name="emergency_contact3_chinese_name" value="<?= $contact_result['Contact3_ChineseName']?>"/></span>
                           	</td>
                        </tr>
                        
                        <tr>
                            <td class="field_title_short"><?= $Lang['StudentRegistry']['GuardianDayPhone'] ?></td>
                            <td>
                            	<span class="Edit_Hide"><?= $contact_result['Contact3_Phone'] ?></span>
                            	<span class="Edit_Show" style="display:none"><input type="text" name="emergency_contact3_phone" value="<?= $contact_result['Contact3_Phone']?>"/></span>
                           	</td>
                           	<td class="field_title_short"><?= $Lang['StudentRegistry']['Mobile'] ?></td>
                            <td colspan="3">
                            	<span class="Edit_Hide"><?= $contact_result['Contact3_Mobile'] ?></span>
                            	<span class="Edit_Show" style="display:none"><input type="text" name="emergency_contact3_mobile" value="<?= $contact_result['Contact3_Mobile']?>" maxlength="8"/></span>
                           	</td>
                        </tr>
                        <?php foreach($custCols as $ccol){
                        	if($ccol['Section'] == 'OtherContactPerson'){?>                        
                        	<tr>
                        		<td class="field_title_short"><?php echo $ccol["DisplayText_".Get_Lang_Selection('ch','en')]?></td>
                        		<td colspan="5">
                        			<span class="Edit_Hide"><?php echo $ccol['Value']?></span>
                        			<span class="Edit_Show" style="display:none"><input type="text" class="textboxtext" name="<?php echo $ccol[Code]?>" value="<?php echo $ccol['Value']?>"/></span>
                        			<input type="hidden" name="custCol[<?php echo $ccol['Code']?>]" value="<?php echo $ccol['ColumnID']?>" />
                        		</td>
                        	</tr>
                        <?php }
                        }?>
                  	</tbody>
				</table>
				<div class="form_sub_title_v30">
    				<em>- <span class="field_title"><?= $Lang['StudentRegistry']['Settings']['CustomCol'] ?></span> -</em>
					<p class="spacer"></p>
				</div>
				<table class="form_table_v30">
                	<tbody>
                        <?php foreach($custCols as $ccol){
                        	if($ccol['Section'] == ''){?>                        
                        	<tr>
                        		<td class="field_title_short"><?php echo $ccol["DisplayText_".Get_Lang_Selection('ch','en')]?></td>
                        		<td>
                        			<span class="Edit_Hide"><?php echo $ccol['Value']?></span>
                        			<span class="Edit_Show" style="display:none"><input type="text" class="textboxtext" name="<?php echo $ccol[Code]?>" value="<?php echo $ccol['Value']?>"/></span>
                        			<input type="hidden" name="custCol[<?php echo $ccol['Code']?>]" value="<?php echo $ccol['ColumnID']?>" />
                        		</td>
                        	</tr>
                        <?php }
                        }?>
                	</tbody>
            	</table>
				
				<span id="FormReminder" class="tabletextremark" style="display: none"><?=$i_general_required_field?></span>
				
				<div id="ErrorLog"></div>
				
				<div class="edit_bottom">
					<span id="LastModified" class="row_content tabletextremark" style="text-align:left">
					<?= $Lang['StudentRegistry']['LastUpdated'] ?> : <?= $result[0]['DateModified']? $result[0]['DateModified']." (".$ModifiedBy.")" : "--"?><br>
					<?= $Lang['StudentRegistry']['LastSubmitted'] ?> : <?= $result[0]['DATA_CONFIRM_DATE']? $result[0]['DATA_CONFIRM_DATE']." (".$result[0]['SubmitByName'].")" : "--"?>
					</span>
					
					<? if($_SESSION['ParentFillOnlineReg']) {?>
					<br><br><br><span class="row_content tabletextremark" style="text-align:left"><?=$Lang['StudentRegistry']['PrivayRemark']?></span>
					<br><br>
					<? } ?>
                    <p class="spacer"></p>
                    <?= $SaveBtn ?>
                    <?= $SaveSubmitBtn ?>
                    <?= $CancelBtn ?>
                </div>
			</div>
		</td>
	</tr>
	
</table>
<br>

<input type="hidden" name="studentID" id="studentID" value="<?=$studentID?>">
<input type="hidden" name="confirm_flag" id="confirm_flag" value="0">
<input type="hidden" name="AcademicYearID" id="AcademicYearID" value="<?=$AcademicYearID?>">
</form>
<script language="javascript">
	//document.getElementById('SaveBtn').style.visibility = "hidden";
	//document.getElementById('CancelBtn').style.visibility = "hidden";
	<? if($EditRecord) {?>
	$('#EditBtn').click();	
	<? }?>
</script>
<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>