<?php

//using : 

###########################################
/*
 * 2019-05-09 Cameron
 *  - handle backPage for kentville
 */
###########################################

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$lsr = new libstudentregistry();
$BackBtn = $linterface->GET_ACTION_BTN($button_back, "button", "window.location='$url'","back"," class='formbutton'  onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"");

### Title ###
$CurrentPageArr['StudentRegistry'] = 1;
$CurrentPage = "Mgmt_RegistryInfo";
$TAGS_OBJ[] = array($Lang['Menu']['AccountMgmt']['StudentRegistryInfo'], "index.php");
$MODULE_OBJ['title'] = $Lang['Header']['Menu']['StudentRegistry'];
$MODULE_OBJ = $lsr->GET_MODULE_OBJ_ARR();
$STEPS_OBJ[] = array($i_general_select_csv_file, 0);
if (!$sys_custom['StudentRegistry']['Kentville']) {
    $STEPS_OBJ[] = array($iDiscipline['Confirmation'], 0);
}
$STEPS_OBJ[] = array($i_general_imported_result, 1);
$linterface->LAYOUT_START();

$backPage = $sys_custom['StudentRegistry']['Kentville'] ? "import_kentville.php" : "import.php"; 
?>

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
	<tr>
		<td colspan="2"><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
	</tr>
	<tr>
		<td clospan="2" align="center">
		<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr>
			<td class='tabletext' align='center'><?=IntegerSafe($no)?> <?=$Lang['SysMgr']['FormClassMapping']['RecordsImportedSuccessfully']?></td>
		</tr>

		<tr>
			<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
		</tr>
		<tr>
			<td align="center" colspan="2">
			<?= $linterface->GET_ACTION_BTN($button_back, "button", "window.location='".$backPage."'");?>
			</td>
		</tr>
		</table>
		</td>
	</tr>
</table>


<?
$linterface->LAYOUT_STOP();
?>