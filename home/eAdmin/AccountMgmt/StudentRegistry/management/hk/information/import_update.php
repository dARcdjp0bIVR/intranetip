<?php
#using: 

#################################
#
#   Date:   2019-04-29  Philips
#           add Custom Column
#
#   Date:   2018-07-04  Cameron
#           fix: also update EnglishName and ChineseName if they are not null in import file [ip2.5.9.7.1]
#
#	Date:	2014-05-05	YatWoon
#			add BroSisName
#
#	Date:	2013-06-26	YatWoon
#			
#################################

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccessright.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry.php");
include_once($PATH_WRT_ROOT."includes/libuserpersonalsettings.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libeclass40.php");
intranet_auth();
intranet_opendb();

$laccessright = new libaccessright();
$lps = new libuserpersonalsettings();

if(!$plugin['AccountMgmt_StudentRegistry'] || (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-View") && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-Manage")) && !$_SESSION['ParentFillOnlineReg']) {
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$lsr = new libstudentregistry();
$lfcm = new form_class_manage();
	
$sql = "select * from TEMP_STUDENT_REG_IMPORT where InputBy=$UserID";
$result = $lsr->returnArray($sql);
$total_no = sizeof($result);

foreach($result as $k=>$d)
{
	$studentID = $d['StudentID'];
	
	# retrieve original email
	$sql = "select UserEmail from INTRANET_USER where UserID=$studentID";
	$result = $lsr->returnVector($sql);
	$ori_UserEmail = $result[0];
	
	# INTRANET_USER
	$userAry = array();
	/*
	if(!$_SESSION['ParentFillOnlineReg'])
	{
		$userAry['EnglishName'] = $english_name;
		$userAry['ChineseName'] = $chinese_name;
	}
	*/
	if ($d['STU_EngName']) {
	    $userAry['EnglishName'] = $d['STU_EngName'];
	}
	if ($d['STU_ChiName']) {
	    $userAry['ChineseName'] = $d['STU_ChiName'];
	}
	$userAry['DateOfBirth'] = $d['STU_DOB'];
	$userAry['Gender'] = $d['STU_Gender'];
	$userAry['HKID'] = $d['STU_IDTypeCode']=="04" ? $d['STU_IDNo'] : "";
	$userAry['HomeTelNo'] = $d['STU_HomePhone'];
	$userAry['MobileTelNo'] = $d['STU_Mobile'];
	$userAry['UserEmail'] = $d['STU_Email'];
	$student_email = $d['STU_Email'];
	$userAry['FirstName'] = $d['STU_EngFirstName'];
	$userAry['LastName'] = $d['STU_EngLastName'];
	$userAry['CFirstName'] = $d['STU_ChiFirstName'];
	$userAry['CLastName'] = $d['STU_ChiLastName'];
	$lsr->updateStudentInfo($userAry, $studentID);	
	
	
	# INTRANET_USER_PERSONAL_SETTINGS
	$psAry['Nationality'] = $lsr->returnPresetCodeName("NATIONALITY", $d['STU_NationalityCode']);
	$psAry['PassportNo'] = $d['STU_IDTypeCode']=="02" ? $d['STU_IDNo'] : "";
	/////debug_pr($psAry);
	$lps->Save_Setting($studentID, $psAry);
	
	# STUDENT_REGISTRY_STUDENT
	$stuAry['CHN_COMMERCIAL_CODE1'] = $d['STU_ChiCommercialCode1'];
	$stuAry['CHN_COMMERCIAL_CODE2'] = $d['STU_ChiCommercialCode2'];
	$stuAry['CHN_COMMERCIAL_CODE3'] = $d['STU_ChiCommercialCode3'];
	$stuAry['CHN_COMMERCIAL_CODE4'] = $d['STU_ChiCommercialCode4'];
// 	$stuAry['ORIGIN'] = $origin;
	$stuAry['ID_TYPE'] = $d['STU_IDTypeCode'];
	$stuAry['ID_NO'] = $d['STU_IDNo'];
	$stuAry['ETHNICITY_CODE'] = $d['STU_EthnicityCode'];
	$stuAry['FAMILY_LANG'] = $d['STU_HomeLangCode'];
// 	$stuAry['FAMILY_LANG_OTHERS'] = $family_lang==0 ? $family_lang_others : "";
	$stuAry['RELIGION'] = $d['STU_ReligionCode'];
// 	$stuAry['RELIGION_OTHERS'] = $religion==0 ? $religion_others : "";
	$stuAry['CHURCH'] = $d['STU_Church'];
	$stuAry['ADDRESS_ROOM_EN'] = $d['STU_AddressFlat'];
	$stuAry['ADDRESS_FLOOR_EN'] = $d['STU_AddressFloor'];
	$stuAry['ADDRESS_BLK_EN'] = $d['STU_AddressBlock'];
	$stuAry['ADDRESS_BLD_EN'] = $d['STU_AddressBuilding'];
	$stuAry['ADDRESS_EST_EN'] = $d['STU_AddressEstate'];
	$stuAry['ADDRESS_STR_EN'] = $d['STU_AddressStreet'];
	$stuAry['DISTRICT_CODE'] = $d['STU_AddressDistrictCode'];
	$stuAry['ADDRESS_AREA'] = $d['STU_AddressAreaCode'];
	$stuAry['LAST_SCHOOL_EN'] = $d['STU_LastSchoolEngName'];
	$stuAry['LAST_SCHOOL_CH'] = $d['STU_LastSchoolChiName'];
	$stuAry['NATION_CODE'] = $d['STU_NationalityCode'];
	/////debug_pr($stuAry);
	$lsr->updateStudentRegistry_HK($stuAry, $studentID);
	
	# STUDENT_REGISTRY_BROSIS
	$bsAry = array();
	for($i=0;$i<3;$i++)
	{
		if($d['STU_BroSisLoginID'.$i])
		{
			$bsAry[$i]['BS_UserLogin'] = $d['STU_BroSisLoginID'.$i];
			$bsAry[$i]['BS_Name'] = $d['STU_BroSisName'.$i];
			$bsAry[$i]['Relationship'] = $d['STU_BroSisRelationship'.$i];
		}
	}
	$lsr->updateStudentBroSisInfo_HK($studentID,$bsAry);
	
	# STUDENT_REGISTRY_PG
	for($g=1;$g<=2;$g++)
	{
		$gAry['SEQUENCE'] = $g;
		$gAry['PG_TYPE'] = $d['GRD_TypeCode'.$g];
		$gAry['PG_TYPE_OTHERS'] = $d['GRD_TypeCode'.$g]==0 ? $d['GRD_TypeOther'.$g] : "";
		$gAry['EMAIL'] = $d['GRD_Email'.$g];
		$gAry['NAME_E'] = $d['GRD_EngName'.$g];
		$gAry['NAME_C'] = $d['GRD_ChiName'.$g];
		$gAry['JOB_TITLE_OTHERS'] = $d['GRD_Occupation'.$g];
		$gAry['TEL'] = $d['GRD_DayPhone'.$g];
		$gAry['MOBILE'] = $d['GRD_Mobile'.$g];
		$gAry['RELIGION'] = $d['GRD_ReligionCode'.$g];
// 		$gAry['RELIGION_OTHERS'] = ${"guardian_religion".$g}==0 ? ${"guardian_religion".$g."_others"} : "";
		$gAry['CHURCH'] = $d['GRD_Church'.$g];
		$gAry['ADDRESS_ROOM_EN'] = $d['GRD_AddressFlat'.$g];
		$gAry['ADDRESS_FLOOR_EN'] = $d['GRD_AddressFloor'.$g];
		$gAry['ADDRESS_BLK_EN'] = $d['GRD_AddressBlock'.$g];
		$gAry['ADDRESS_BLD_EN'] = $d['GRD_AddressBuilding'.$g];
		$gAry['ADDRESS_EST_EN'] = $d['GRD_AddressEstate'.$g];
		$gAry['ADDRESS_STR_EN'] = $d['GRD_AddressStreet'.$g];
		$gAry['DISTRICT_CODE'] = $d['GRD_AddressDistrictCode'.$g];
		$gAry['ADDRESS_AREA'] = $d['GRD_AddressAreaCode'.$g];
		/////debug_pr($gAry);
		$lsr->updateStudentRegistry_HK(array(), $studentID, $gAry);
	}
	
	# STUDENT_REGISTRY_EMERGENCY_CONTACT
	$eContactAry['Contact3_Title'] = $d['OtherConact_TitleCode'];
	$eContactAry['Contact3_EnglishName'] = $d['OtherConact_EngName'];
	$eContactAry['Contact3_ChineseName'] = $d['OtherConact_ChiName'];
	$eContactAry['Contact3_Relationship'] = $d['OtherConact_Relationship'];
	$eContactAry['Contact3_Phone'] = $d['OtherConact_DayPhone'];
	$eContactAry['Contact3_Mobile'] = $d['OtherConact_Mobile'];
	/////debug_pr($eContactAry);
	$lsr->updateStudentRegistry_HK(array(), $studentID, array(), $eContactAry);
	
	# update eClass40 data
	$dataAry = array();
// 	if(!$_SESSION['ParentFillOnlineReg'])
// 	{
// 		$dataAry['EnglishName'] = $english_name;
// 		$dataAry['ChineseName'] = $chinese_name;
// 	}
	$dataAry['Gender'] = $d['STU_Gender'];
	$dataAry['DateOfBirth'] = $d['STU_DOB'];
	$dataAry['HomeTelNo'] = $d['STU_HomePhone'];
	/////debug_pr($dataAry);
	$le = new libeclass();
	$le->eClass40UserUpdateInfo($ori_UserEmail, $dataAry, $student_email);
	
	$custCols = $lsr->getCustomColumn();
	$sql = "SELECT * FROM TEMP_STUDENT_REG_IMPORT_CUST WHERE RecordID = '$d[RecordID]'";
	$custRecord = $lsr->returnArray($sql);
	if(sizeof($custRecord)>0){
	    $custRecord = $custRecord[0];
    	$custValues = array();
    	foreach($custCols as $col){
    	    $custValues[] = array(
    	        "ColumnID" => $col['ColumnID'],
    	        "value" => $custRecord[$col['Code']]
    	    );
    	}
//     	debug_pr($custValues);die();
    	$lsr->updateCustomValue($studentID, $custValues);
	}
}

# remove data in temp table
$sql = "delete from TEMP_STUDENT_REG_IMPORT where InputBy=$UserID";
$lsr->db_db_query($sql);
$sql = "delete from TEMP_STUDENT_REG_IMPORT_CUST where InputBy=$UserID";
$lsr->db_db_query($sql);


intranet_closedb();
header("Location: import_done.php?no=".$total_no);
?>