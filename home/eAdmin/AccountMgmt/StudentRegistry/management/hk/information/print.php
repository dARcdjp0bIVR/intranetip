<?php
#using: 

## Note: it retrieves record based on filter condition but not checked records
#
############# Change Log
#
#   Date:   2020-09-17 Cameron
#           - fix Guardian Religion  field so that it's consistent with view [case #B197079]
#
#   Date:   2019-08-15 Cameron
#           - change checking submit status in STUDENT_REGISTRY_SUBMITTED [case #Y164458]
#
#	Date:	2017-06-09 Cameron
#			- add filter: IncludeLeft student (alumni & left student, all in INTRANET_USER) or not
#
#	Date:	2015-07-10 Cameron
#			- add filter of LastUpdatedDate
#			- add "submit status" filter
#			- disable $HidePhoto as this is a temp solution and uat didn't implement this  					
#		
#	Date:		2013-07-09	(yuen)
#			 temporarily hide the photos using hardcoded $HidePhoto
#
#################


//$HidePhoto = true;


$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccessright.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_header.php");

intranet_auth();
intranet_opendb();


$studentID = (is_array($studentID)? $studentID[0]:$studentID);
$laccessright = new libaccessright();

if	(	!$plugin['AccountMgmt_StudentRegistry'] || 
		(!$_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-View") && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-Manage")) &&
		!$_SESSION['ParentFillOnlineReg']
	) 
{
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$lsr = new libstudentregistry();
$linterface = new interface_html("");
$lfcm = new form_class_manage();

?>

<table width="98%" border="0" cellspacing="0" cellpadding="5" valign="top">
	<tr>
		<td align="right" class='print_hide'>
			<?=$linterface->GET_BTN($button_print, "button","javascript:window.print()")?>
		</td>
	</tr>
</table>

<?
# retrieve student ids
$conds = "";

if($targetClass!="") {
	if(is_numeric($targetClass)) 
		$conds .= " AND y.YearID=".$targetClass;	
	else
		$conds .= " AND yc.YearClassID=".str_replace("::","",$targetClass);	  
}

if($searchText!="") {
	$conds .= " AND (
					".Get_Lang_Selection('yc.ClassTitleB5','yc.ClassTitleEN')." LIKE '%$searchText%' OR 
					USR.EnglishName LIKE '%$searchText%' OR 
					USR.ChineseName LIKE '%$searchText%' OR 
					srs.ID_NO LIKE '%$searchText%' OR 
					srs.EMAIL LIKE '%$searchText%' OR
					USR.HomeTelNo LIKE '%$searchText%'
				)";
}

if($submit_status==1)
{
	$conds .= " and srs2.DATA_CONFIRM_DATE is not null";
}

if($submit_status==-1)
{
	$conds .= " and srs2.DATA_CONFIRM_DATE is null";
}

if ($IncludeEverUpdatedOnly){
	$conds .= " and srs.DateModified IS NOT NULL";
}
if ($StartDate != '') {
	if ($IncludeEverUpdatedOnly){
		$conds .= " and srs.DateModified >='".$StartDate." 00:00:00'";
	}
	else {
		$conds .= " and (srs.DateModified >='".$StartDate." 00:00:00' or srs.DateModified IS NULL)";
	}
}
if ($EndDate != '') {
	if ($IncludeEverUpdatedOnly){
		$conds .= " and srs.DateModified <='".$EndDate." 23:59:59'";
	}
	else {
		$conds .= " and (srs.DateModified <='".$EndDate." 23:59:59' or srs.DateModified IS NULL)";
	}
}

$name_field = getNameFieldByLang("USR.");

$cond2 = ($IncludeLeft == '1') ? " AND USR.RecordType IN(2,4) AND USR.RecordStatus IN (0,1,2,3)" : " AND USR.RecordType=2 AND USR.RecordStatus IN (0,1,2)";
	
$sql = "SELECT 
			USR.UserID 
		FROM
			INTRANET_USER USR LEFT OUTER JOIN
			YEAR_CLASS_USER ycu ON (ycu.UserID=USR.UserID) LEFT OUTER JOIN
			YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID) LEFT OUTER JOIN
			YEAR y ON (y.YearID=yc.YearID) LEFT OUTER JOIN
			STUDENT_REGISTRY_STUDENT srs ON (srs.UserID=USR.UserID) LEFT JOIN 
			{$eclass_db}.PORTFOLIO_STUDENT AS ps ON USR.UserID = ps.UserID LEFT JOIN
			STUDENT_REGISTRY_SUBMITTED srs2 ON (srs2.UserID=USR.UserID AND srs2.AcademicYearID='$AcademicYearID')
		WHERE
			yc.AcademicYearID='$AcademicYearID'
			{$cond2}
			$conds
		ORDER BY
			USR.ClassName, USR.ClassNumber, USR.Englishname	
		";
$StudentIDs_tmp = $lsr->returnVector($sql);

for($s=0;$s<sizeof($StudentIDs_tmp);$s++)
{
	$studentID = $StudentIDs_tmp[$s];
	$result = $lsr -> RETRIEVE_STUDENT_INFO_BASIC_HK($studentID);
	$li = new libuser($studentID);
	
	# Chinse Name
	$chinese_name_data = $result[0]['ChineseName'];	
	
	# English Name
	$english_name_data = $result[0]['EnglishName'];	
	
	#Gender related info
	$gender_ary      = $lsr -> GENDER_ARY();
	$gender_val      = $result[0]['Gender']? $lsr -> RETRIEVE_DATA_ARY($gender_ary, $result[0]['Gender']) : "--";
	
	# ID Document Type
	$ID_DATA = $lsr->returnPresetCodeName("IDENT_DOC_TYPE", $result[0]['ID_TYPE']);
		 
	# Ethnicity
	$Ethnicity_data = $lsr->returnPresetCodeName("ETHNICITY", $result[0]['ETHNICITY_CODE']);
	
	# Language Spoken at Home
	$HomeLang_data = $lsr->returnPresetCodeName("FAMILY_LANG", $result[0]['FAMILY_LANG']);
	
	# Religion
	$Religion_data = $lsr->returnPresetCodeName("RELIGION", $result[0]['RELIGION']);
		
	# Nationality
	$student_nationality_data = $lsr->returnPresetCodeName("NATIONALITY", $result[0]['NATION_CODE']);
	
	# District 
	$student_district_data = $lsr->returnPresetCodeName("DISTRICT", $result[0]['DISTRICT_CODE']);
	
	# Brothers & sisters currently studying in school
	$BroSis_data = $lsr->RETRIEVE_BRO_SIS_HK($studentID);
	$BroSis_table = "<table class='inside_form_table' width='100%'><tr><td>". $i_UserLogin ."</td><td>". $Lang['StudentRegistry']['Relationship'] ."</td></tr>";
	for($i=0;$i<3;$i++)
	{
		$BroSis_table .= "<tr>";
		$BroSis_table .= "<td><input type='text' name='BS_UserLogin_". $i ."' value='". $BroSis_data[$i]['BS_UserLogin'] ."'></td>";
		$BroSis_table .= "<td><input type='text' name='BS_Relationship_". $i ."' value='". $BroSis_data[$i]['Relationship'] ."'></td>";
		$BroSis_table .= "</tr>";
	}
	$BroSis_table .= "</table>";
	if(!empty($BroSis_data))
	{
		$BroSis_data_table = "<table class='inside_form_table' width='100%'><tr><td>". $i_UserLogin ."</td><td>". $Lang['StudentRegistry']['Relationship'] ."</td></tr>";
		if(!empty($BroSis_data))
		{
			foreach($BroSis_data as $k=>$d)
			{
				$BroSis_data_table .= "<tr>";
				$BroSis_data_table .= "<td>". $d['BS_UserLogin'] ."</td>";
				$BroSis_data_table .= "<td>". $d['Relationship'] ."</td>";
				$BroSis_data_table .= "</tr>";	
			}
		}
		$BroSis_data_table .= "</table>";
	}
	else
	{
		$BroSis_data_table = $Lang['General']['NoRecordAtThisMoment'];
	}
	
	# Guardian
	$guardian_selection = array();
	for($g=1;$g<=2;$g++)
	{
		$g_result = $lsr -> RETRIEVE_GUARDIAN_INFO_HK($studentID, $g);
	
		$gType[$g] = $g_result['PG_TYPE']==1 ? $Lang['StudentRegistry']['Father'] : ($g_result['PG_TYPE']==2 ? $Lang['StudentRegistry']['Mother'] : $g_result['PG_TYPE_OTHERS']);
		$gEmail[$g] = $g_result['EMAIL'];
		$gEnglishName[$g] = $g_result['NAME_E'];
		$gChineseName[$g] = $g_result['NAME_C'];
		$gOccupation[$g] = $g_result['JOB_TITLE_OTHERS'];
		$gTel[$g] = $g_result['TEL'];
		$gMobile[$g] = $g_result['MOBILE'];
//		$gReligion[$g] = $g_result['RELIGION']>0 ? $Lang['StudentRegistry']['ReligionOptions'][$g_result['RELIGION']-1] : $g_result['RELIGION_OTHERS'];
        $gReligion[$g] = $lsr->returnPresetCodeName("RELIGION", $g_result['RELIGION']);
		$gChurch[$g] = $g_result['CHURCH'];
		$gAddressRoomEn[$g] = $g_result['ADDRESS_ROOM_EN'];
		$gAddressFloorEn[$g] = $g_result['ADDRESS_FLOOR_EN'];
		$gAddressBlkEn[$g] = $g_result['ADDRESS_BLK_EN'];
		$gAddressBldEn[$g] = $g_result['ADDRESS_BLD_EN'];
		$gAddressEstEn[$g] = $g_result['ADDRESS_EST_EN'];
		$gAddressStrEn[$g] = $g_result['ADDRESS_STR_EN'];
		$gAddressDistrictEn[$g] = $lsr->returnPresetCodeName("DISTRICT", $g_result['DISTRICT_CODE']);
		$gAddressArea[$g] = $Lang['StudentRegistry']['AreaOptions'][$g_result['ADDRESS_AREA']-1];
	}
	
	# Contact Persion in case of emergency
	$contact_result = $lsr -> RETRIEVE_EMERGENCY_CONTACT_INFO_HK($studentID);
	if($contact_result['Contact1'] > 0)	
		$contact1_data = $contact_result['Contact1']==1? $Lang['StudentRegistry']['Father'] : $Lang['StudentRegistry']['Mother'];
	else							# others
		$contact1_data = $contact_result['Contact1_Others'];
	if($contact_result['Contact2'] > 0)	
		$contact2_data = $contact_result['Contact2']==1? $Lang['StudentRegistry']['Father'] : $Lang['StudentRegistry']['Mother'];
	else							# others
		$contact2_data = $contact_result['Contact2_Others'];
	$contact3_title_data = $contact_result['Contact3_Title']==1 ? $i_title_mr :($contact_result['Contact3_Title']==2 ? $i_title_mrs : $i_title_ms);
	
	########## Official Photo
	$photo_link = "/images/myaccount_personalinfo/samplephoto.gif";
	if ($li->PhotoLink !="")
	{
	    if (is_file($intranet_root.$li->PhotoLink))
	    {
	        $photo_link = $li->PhotoLink;
	    }
	}
	########## Personal Photo
	$photo_personal_link = "/images/myaccount_personalinfo/samplephoto.gif";
	if ($li->PersonalPhotoLink !="")
	{
	    if (is_file($intranet_root.$li->PersonalPhotoLink))
	    {
	        $photo_personal_link = $li->PersonalPhotoLink;
	   }
	}
	?>
	
	<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr> 
			<td class="main_content">
				<div class="table_board">
	                <p class="spacer"></p>
	                
	                <? ########################################################### ?>
					<? #  Student Information                                    # ?>
					<? ########################################################### ?>
	                <div class="form_sub_title_v30">
	                	<em>- <span class="field_title"><?= $Lang['StudentRegistry']['StudInfo'] ?></span> -</em>
	                    <p class="spacer"></p>
	                </div>
	                <table class="form_table_v30">
	                	<col class="field_title_short">
	                    <col class="field_content_short">
	                    <col class="field_title_short">
	                    <col class="field_content_short">
	                    <col class="field_title_short">
	                    <col class="field_content_short">
						<tbody>
	                        <tr>
	                            <td class="field_title_short"><?= $Lang['StudentRegistry']['EnglishName'] ?></td>
	                            <td colspan="3">
	                    			<div class="form_field_sub_content">
	                     		       	<table class="inside_form_table">
	                     		       		<tr>
	                     		       			<td><em>(<?= $i_UserDisplayName ?>)</em></td>
	                     		       			<td><?=$result[0]['EnglishName']?></td>
	                 		       			</tr>
	                 		       			<tr>
	                     		       			<td><em>(<?= $i_UserFirstName ?>)</em></td>
	                     		       			<td><?= $result[0]['FirstName'] ? $result[0]['FirstName'] : "--"?></td>
	                     		       			<td><em>(<?= $i_UserLastName ?>)</em></td>
	                     		       			<td><?= $result[0]['LastName'] ? $result[0]['LastName'] : "--"?></td>
	                        	    		</tr>
	                        	    	</table>
									</div>
									</td>
	                        	
								<td rowspan="10">
									<? if($li->PhotoLink !="" && $li->PhotoLink!="/images/myaccount_personalinfo/samplephoto.gif")	{?>
										<?=$Lang['Personal']['OfficialPhoto']?><br /><img src="<?=$photo_link?>" width="100" height="130"><br>
									<? } ?>
									<? //if (!$HidePhoto &&  trim($li->PersonalPhotoLink) !="") {
										if (trim($li->PersonalPhotoLink) !="") { ?>
		                        		<?=$Lang['Personal']['PersonalPhoto']?><br /><img src="<?=$photo_personal_link?>" width="100" height="130">
		                        	<? } ?>
	                        	</td>
	                        </tr>
	                        
	                        <tr>
	                        	<td class="field_title_short"><?= $Lang['StudentRegistry']['ChineseName'] ?></td>
								<td colspan="3">
	                    			<div class="form_field_sub_content">
	                     		       	<table class="inside_form_table">
	                     		       		<tr>
	                     		       			<td><em>(<?= $i_UserDisplayName ?>)</em></td>
	                     		       			<td><?=$result[0]['ChineseName']?></td>
	                 		       			</tr>
	                 		       			<tr>
	                     		       			<td><em>(<?= $i_UserFirstName ?>)</em></td>
	                     		       			<td><?= $result[0]['CFirstName'] ? $result[0]['CFirstName'] : "--"?></td>
	                     		       			
	                     		       			<td><em>(<?= $i_UserLastName ?>)</em></td>
	                     		       			<td><?= $result[0]['CLastName'] ? $result[0]['CLastName'] : "--"?></td>
	                        	    		</tr>
	                        	    	</table>
									</div>
									</td>
	                        </tr>
	                        
							<tr>
								<td class="field_title_short"><?= $Lang['StudentRegistry']['ChineseCommercialCode'] ?></td>
								<td colspan="3"><?= $result[0]['CHN_COMMERCIAL_CODE1'] ? $result[0]['CHN_COMMERCIAL_CODE1']. " " . $result[0]['CHN_COMMERCIAL_CODE2']. " " . $result[0]['CHN_COMMERCIAL_CODE3']. " " . $result[0]['CHN_COMMERCIAL_CODE4']. " " : "--"?></td>
	                        </tr>
	                        
	                        <tr>
	                            <td class="field_title_short"><?= $Lang['StudentRegistry']['Gender'] ?></td>
	                            <td><?= $gender_val ?></td>
	                            <td class="field_title_short"><?= $Lang['StudentRegistry']['BirthDate'] ?></td>
	                            <td><?= $result[0]['DOB']=="0000-00-00" ? "" : $result[0]['DOB'] ?></td>
	                        </tr>
	                        
	                        <tr>
	                            <td class="field_title_short"><?= $profile_nationality ?></td>
	                            <td><?= $student_nationality_data ?></td>
	                            <td class="field_title_short"><?= $Lang['StudentRegistry']['Race'] ?></td>
	                            <td><?= $Ethnicity_data?></td>
	                            
	                       <tr>     
	                            <td class="field_title_short"><?= $Lang['StudentRegistry']['ID_DocumentType'] ?></td>
	                            <td><?= $ID_DATA?></td>
	                            <td class="field_title_short"><?= $Lang['StudentRegistry']['ID_DocumentNo'] ?></td>
	                            <td><?= $result[0]['ID_NO']?></td>
	                        </tr>
	                        
	                        <tr>
								<td class="field_title_short"><?= $Lang['StudentRegistry']['HomeLang'] ?></td>
	                            <td><?= $HomeLang_data?></td>
	                            <td class="field_title_short"><?= $Lang['StudentRegistry']['Religion'] ?></td>
	                            <td><?= $Religion_data?></td>
	                        </tr>
	                        
	                        <tr>
	                            <td class="field_title_short"><?= $Lang['StudentRegistry']['Church'] ?></td>
	                            <td colspan="4"><?= $result[0]['CHURCH'] ?></td>
	                        </tr>
	                        
	                        <tr>
								<td class="field_title_short"><?= $Lang['StudentRegistry']['HomePhoneNo'] ?></td>
	                            <td><?= $result[0]['HomeTelNo']?></td>
	                            <td class="field_title_short"><?= $Lang['StudentRegistry']['MobilePhoneNo'] ?></td>
	                            <td><?= $result[0]['MobileTelNo'] ?></td>
	                        </tr>
	                        
	                        <tr>
								<td class="field_title_short"><span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['EMailAddress'] ?></td>
	                            <td colspan="4"><?= $result[0]['UserEmail'] ?></td>
	                        </tr>
	                        
	                        <tr>
								<td class="field_title_short"><?= $Lang['StudentRegistry']['Address'] ?></td>
	                            <td colspan="3">
	                            			<div class="form_field_sub_content">
			                     		       	<table class="inside_form_table">
			                     		       		<tr>
			                     		       			<td><em>(<?= $Lang['StudentRegistry']['AddressField']['Flat'] ?>)</em></td>
			                     		       			<td><?= $result[0]['ADDRESS_ROOM_EN']?></td>
			                     		       			<td><em>(<?= $Lang['StudentRegistry']['AddressField']['Floor'] ?>)</em></td>
			                     		       			<td><?= $result[0]['ADDRESS_FLOOR_EN']?></td>
			                     		       			<td><em>(<?= $Lang['StudentRegistry']['AddressField']['Block'] ?>)</em></td>
			                        	    			<td><?= $result[0]['ADDRESS_BLK_EN']?></td>
			                        	    		</tr>
			                        	    		<tr>
			                     		       			<td><em>(<?= $Lang['StudentRegistry']['AddressField']['Building'] ?>)</em></td>
			                     		       			<td colspan="5"><?= $result[0]['ADDRESS_BLD_EN']?></td>
			                        	    		</tr>
			                        	    		<tr>
			                     		       			<td><em>(<?= $Lang['StudentRegistry']['AddressField']['Estate'] ?>)</em></td>
			                     		       			<td colspan="5"><?= $result[0]['ADDRESS_EST_EN']?></td>
			                        	    		</tr>
			                        	    		<tr>
			                     		       			<td><em>(<?= $Lang['StudentRegistry']['AddressField']['Street'] ?>)</em></td>
			                     		       			<td colspan="5"><?= $result[0]['ADDRESS_STR_EN']?></td>
			                        	    		</tr>
			                        	    		<tr>
			                     		       			<td><em>(<?= $Lang['StudentRegistry']['AddressField']['District'] ?>)</em></td>
			                     		       			<td colspan="5"><?= $student_district_data?></td>
			                        	    		</tr>
			                        	    		<tr>
			                     		       			<td><em>(<?= $Lang['StudentRegistry']['AddressField']['Area'] ?>)</em></td>
			                     		       			<td colspan="5"><?= $Lang['StudentRegistry']['AreaOptions'][$result[0]['ADDRESS_AREA']-1]?></td>
			                        	    		</tr>
			                        	    	</table>
											</div>
											</td>
	                        </tr>
	                       
						</tbody>
						<col class="field_title_short">
	                    <col class="field_c">
					</table>
					
					
					<? ########################################################### ?>
					<? #  Last School 	                                         # ?>
					<? ########################################################### ?>
					<div class="form_sub_title_v30">
	    				<em>- <span class="field_title"><?=$Lang['StudentRegistry']['LastSchool']?></span> -</em>
						<p class="spacer"></p>
					</div>
					<table class="form_table_v30">
						<tbody>
	                        <tr>
	                            <td class="field_title_short"><?= $Lang['General']['EnglishName2'] ?></td>
	                            <td><?= $result[0]['LAST_SCHOOL_EN']?></td>
	                        </tr>
	                        <tr>
								<td class="field_title_short"><?= $Lang['General']['ChineseName2'] ?></td>
	                            <td><?= $result[0]['LAST_SCHOOL_CH']?></td>
	                        </tr>
					</tbody>
					</table>
					
					<? ########################################################### ?>
					<? #  Brothers & Sistes currently studying                   # ?>
					<? ########################################################### ?>
					<div class="form_sub_title_v30">
	    				<em>- <span class="field_title"><?=$Lang['StudentRegistry']['BrotherSisterStudyingInSchool']?></span> -</em>
						<p class="spacer"></p>
					</div>
					<table class="form_table_v30">
						<tbody>
	                        <tr>
	                            <td><?=$BroSis_data_table?></td>
	                        </tr>
					</tbody>
					</table>
					
					<? ########################################################### ?>
					<? #  Guardian                                               # ?>
					<? ########################################################### ?>
					<? for($g=1;$g<=2;$g++)  { ?>
					<div class="form_sub_title_v30">
	    				<em>- <span class="field_title"><?= ($g==1) ?$Lang['StudentRegistry']['1st'] :$Lang['StudentRegistry']['2nd'] ?><?= $Lang['Identity']['Guardian'] ?></span> -</em>
						<p class="spacer"></p>
					</div>
					<table class="form_table_v30">
						<tbody>
	                        <tr>
								<td class="field_title_short"><?= $Lang['StudentRegistry']['GuardianStudentRelationship'] ?></td>
	                            <td><?=$gType[$g]?></td>
	                            <td class="field_title_short"><?= $i_UserEmail ?></td>
	                            <td><?= $gEmail[$g] ?></td>
	                        </tr>
							<tr>
	                         	<td class="field_title_short"><?= $Lang['StudentRegistry']['EnglishName'] ?></td>
	                            <td><?= $gEnglishName[$g] ?></td>
	                            <td class="field_title_short"><?= $Lang['StudentRegistry']['ChineseName'] ?></td>
	                            <td><?= $gChineseName[$g] ?></td>
	                        </tr>
	                        <tr>
	                            <td class="field_title_short"><?= $i_StudentGuardian_Occupation ?></td>
	                            <td colspan="3"><?= $gOccupation[$g] ?></td>
	                        </tr>
	                         <tr>
	                        	<td class="field_title_short"><span class="field_title"><?= $Lang['StudentRegistry']['GuardianDayPhone'] ?></span></td>
	                        	<td><?= $gTel[$g] ?></td>
	                        	<td class="field_title_short"><span class="field_title"><?= $Lang['StudentRegistry']['GuardianMobile'] ?></span></td>
	                         	<td><?= $gMobile[$g] ?></td>
							</tr>
							<tr>
								<td class="field_title_short"><?= $Lang['StudentRegistry']['Religion'] ?></td>
	                            <td><?= $gReligion[$g] ?></td>
	                            <td class="field_title_short"><?= $Lang['StudentRegistry']['Church'] ?></td>
	                            <td><?= $gChurch[$g] ?></td>
	                        </tr>
	                        
	                        <tr>
								<td class="field_title_short"><?= $Lang['StudentRegistry']['Address'] ?></td>
	                            <td colspan="3">
	                            	<span class="Edit_Show" style="display:none"><b>(<?=$Lang['StudentRegistry']['NoNeedCompleteSameAsStudent']?>)</b></span>
	                    			<div class="form_field_sub_content">
	                     		       	<table class="inside_form_table">
	                     		       		<tr>
	                     		       			<td><em>(<?= $Lang['StudentRegistry']['AddressField']['Flat'] ?>)</em></td>
	                     		       			<td><?= $gAddressRoomEn[$g] ?></td>
	                     		       			<td><em>(<?= $Lang['StudentRegistry']['AddressField']['Floor'] ?>)</em></td>
	                     		       			<td><?= $gAddressFloorEn[$g] ?></td>
	                     		       			<td><em>(<?= $Lang['StudentRegistry']['AddressField']['Block'] ?>)</em></td>
	                        	    			<td><?= $gAddressBlkEn[$g] ?></td>
	                        	    		</tr>
	                        	    		<tr>
	                     		       			<td><em>(<?= $Lang['StudentRegistry']['AddressField']['Building'] ?>)</em></td>
	                     		       			<td colspan="5"><?= $gAddressBldEn[$g] ?></td>
	                        	    		</tr>
	                        	    		<tr>
	                     		       			<td><em>(<?= $Lang['StudentRegistry']['AddressField']['Estate'] ?>)</em></td>
	                     		       			<td colspan="5"><?= $gAddressEstEn[$g] ?></td>
	                        	    		</tr>
	                        	    		<tr>
	                     		       			<td><em>(<?= $Lang['StudentRegistry']['AddressField']['Street'] ?>)</em></td>
	                     		       			<td colspan="5"><?= $gAddressStrEn[$g] ?></td>
	                        	    		</tr>
	                        	    		<tr>
	                     		       			<td><em>(<?= $Lang['StudentRegistry']['AddressField']['District'] ?>)</em></td>
	                     		       			<td colspan="5"><?= $gAddressDistrictEn[$g] ?></td>
	                        	    		</tr>
	                        	    		<tr>
	                     		       			<td><em>(<?= $Lang['StudentRegistry']['AddressField']['Area'] ?>)</em></td>
	                     		       			<td colspan="5"><?= $gAddressArea[$g] ?></td>
	                        	    		</tr>
	                        	    	</table>
									</div>
								</td>
	                        </tr>
						</tbody>
					</table>
					<? } ?>
					
					<? ########################################################### ?>
					<? #  Contact Persion in case of emergency                   # ?>
					<? ########################################################### ?>
					<div class="form_sub_title_v30">
	    				<em>- <span class="field_title"><?= $Lang['StudentRegistry']['OtherContactPerson'] ?></span> -</em>
						<p class="spacer"></p>
					</div>
					<table class="form_table_v30">
						<col class="field_title_short">
	                    <col class="field_content_short">
	                    <col class="field_title_short">
	                    <col class="field_content_short">
						<tbody>
	                        <tr>
								<td class="field_title_short"><?= $i_UserTitle ?></td>
	                            <td><?= $contact3_title_data ?></td>
	                           	<td class="field_title_short"><?= $Lang['StudentRegistry']['Relationship'] ?></td>
	                            <td><?= $contact_result['Contact3_Relationship'] ?></td>
	                        </tr>
	                        
	                        <tr>
								<td class="field_title_short"><?= $Lang['General']['EnglishName'] ?></td>
	                            <td><?= $contact_result['Contact3_EnglishName'] ?></td>
	                            <td class="field_title_short"><?= $Lang['General']['ChineseName'] ?></td>
	                            <td><?= $contact_result['Contact3_ChineseName'] ?></td>
	                        </tr>
	                        
	                        <tr>
	                            <td class="field_title_short"><?= $Lang['StudentRegistry']['GuardianDayPhone'] ?></td>
	                            <td><?= $contact_result['Contact3_Phone'] ?></td>
	                           	<td class="field_title_short"><?= $Lang['StudentRegistry']['Mobile'] ?></td>
	                            <td colspan="3"><?= $contact_result['Contact3_Mobile'] ?></td>
	                        </tr>
	                  	</tbody>
					</table>
				</div>
			</td>
		</tr>
	</table>
	<? if($s<sizeof($StudentIDs_tmp)-1) {?> <P class="breakhere"></P><? } ?>
<? } ?>
<?php
intranet_closedb();
// $linterface->LAYOUT_STOP();
?>