<?
# using: 

## !!! use utf-8 to edit
## Note: it retrieves record based on filter condition but not checked records
#
#######################################
#
#   Date:   2019-08-15 Cameron
#           - change checking submit status in STUDENT_REGISTRY_SUBMITTED [case #Y164458]
#
#	Date:	2017-06-09 Cameron
#			- add filter: IncludeLeft student (alumni & left student, all in INTRANET_USER) or not
#
# 	Date:	2015-12-18 Omas
#			 replace split by explode - for php 5.4
#
#	Date:	2015-07-24 Cameron
#			- keep filter RecordType=2
#			- set document type and identity document number to empty
#		
#	Date:	2015-07-22 Cameron
#			- modify export WebSAMS format data to suit WebSAMS
#
#	Date:	2015-07-10 Cameron
#			- add filter of LastUpdatedDate
#			- add "submit status" filter			
#
#	Date:	2014-05-02	YatWoon
#			add BroSisName
#
#	Date:	2013-08-27	YatWoon
#			add export for websams
#
#	Date:	2013-06-25	YatWoon
#			add ClassName, ClassNumber, WebSAMSNo
#######################################
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
// include_once($PATH_WRT_ROOT."includes/libdbdump.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

intranet_opendb();

$lsr = new libstudentregistry();
$lexport = new libexporttext();

##############################################################
# build header [Start]
##############################################################
$export_header = array();
# Student Information
$type = $type ? $type : 1;
switch ($type) {
	case 1:
		array_push($export_header, "ClassName", "ClassNumber", "WebSAMSRegNo", "STU_EngName", "STU_EngFirstName", "STU_EngLastName", "STU_ChiName", "STU_ChiFirstName", 
					"STU_ChiLastName", "STU_ChiCommercialCode1", "STU_ChiCommercialCode2", "STU_ChiCommercialCode3", "STU_ChiCommercialCode4",
					"STU_Gender", "STU_DOB", "STU_Nationality", "STU_Ethnicity", "STU_IDType", "STU_IDNo", "STU_HomeLang",
					"STU_Religion", "STU_Church", "STU_HomePhone", "STU_Mobile", "STU_Email",
					"STU_AddressFlat", "STU_AddressFloor", "STU_AddressBlock", "STU_AddressBuilding", "STU_AddressEstate",
					"STU_AddressStreet", "STU_AddressDistrict", "STU_AddressArea");
		break;
	case 2:
		array_push($export_header, "ClassName", "ClassNumber", "WebSAMSRegNo", "STU_EngName", "STU_EngFirstName", "STU_EngLastName", "STU_ChiName", "STU_ChiFirstName", 
				"STU_ChiLastName", "STU_ChiCommercialCode1", "STU_ChiCommercialCode2", "STU_ChiCommercialCode3", "STU_ChiCommercialCode4",
				"STU_Gender", "STU_DOB", "STU_NationalityCode", "STU_EthnicityCode", "STU_IDTypeCode", "STU_IDNo", "STU_HomeLangCode",
				"STU_ReligionCode", "STU_Church", "STU_HomePhone", "STU_Mobile", "STU_Email",
				"STU_AddressFlat", "STU_AddressFloor", "STU_AddressBlock", "STU_AddressBuilding", "STU_AddressEstate",
				"STU_AddressStreet", "STU_AddressDistrictCode", "STU_AddressAreaCode");
		break;
	case 3:	// export for WebSAMS		
		array_push($export_header, "Registration No", "School Year", "Class Level", "Class Code", "Class No", "STRN", "English Name", "Chinese Name", 
				"Sex Code", "Date of Birth", "Home District Council Code", "Ethnicity Code", "Spoken Language at Home Code", "Other Name", "CCC",
				"HKIC No", "HK Birth Cert No", "Place of Birth Code", "Religion Code", "Nationality Code", "Origin", "Ident. Doc Type Code",  
				"Ident. Doc No", "Non-Chinese Speaking", "Date from Mainland", "Email", "Phone No", "Student Mobile Phone No", "Flat No (Eng)", 
				"Floor No (Eng)", "Block No (Eng)", "Flat No (Chi)", "Floor No (Chi)", "Block No (Chi)", "Building Name (Eng)", "Building Name (Chi)", 
				"Village/Estate (Eng)", "Village/Estate (Chi)", "Street (Eng)", "Street (Chi)", "District (Eng)", "District (Chi)", "Area Code",
				"Defined Fields:Church", "Defined Fields:Eng Name of other Parent", "Defined Fields:Chi Name of other Parent", "Defined Fields:HKID Number",
				"Defined Fields:Occupation", "Defined Fields:Mobile", "Defined Fields:email address", 
				"Defined Fields:Religion", "Defined Fields:Other Contact Person Chi Name", "Defined Fields:Other Contact Mobile");
		break;
	default:
		break;
}

if ($type != 3) {
	# Last School
	array_push($export_header, "STU_LastSchoolEngName", "STU_LastSchoolChiName");
	
	# Brothers & sisters currently studying in school
	array_push($export_header, "STU_BroSisLoginID1", "STU_BroSisName1", "STU_BroSisRelationship1", "STU_BroSisLoginID2", "STU_BroSisName2", "STU_BroSisRelationship2", "STU_BroSisLoginID3", "STU_BroSisName3", "STU_BroSisRelationship3");
	
	# Guardian 
	for($i=1;$i<=2;$i++)
	{
		array_push($export_header, "GRD_TypeCode".$i,"GRD_TypeOther".$i, "GRD_Email".$i, "GRD_EngName".$i, "GRD_ChiName".$i, "GRD_Occupation".$i,
		"GRD_DayPhone".$i, "GRD_Mobile".$i, "GRD_ReligionCode".$i, "GRD_Church".$i,
		"GRD_AddressFlat".$i, "GRD_AddressFloor".$i, "GRD_AddressBlock".$i, "GRD_AddressBuilding".$i, "GRD_AddressEstate".$i,
		"GRD_AddressStreet".$i, "GRD_AddressDistrictCode".$i, "GRD_AddressAreaCode".$i);
	}
	
	# Other Contact Person
	if($type==1)
	{
		array_push($export_header, "OtherConact_Title", "OtherConact_Relationship", "OtherConact_EngName", "OtherConact_ChiName",
				"OtherConact_DayPhone", "OtherConact_Mobile");
	}
	elseif($type==2)
	{
		array_push($export_header, "OtherConact_TitleCode", "OtherConact_Relationship", "OtherConact_EngName", "OtherConact_ChiName",
				"OtherConact_DayPhone", "OtherConact_Mobile");
	}
	
	
	// Custom Column
	$custCols = $lsr->getCustomColumn();
	foreach($custCols as $ccol){
	    array_push($export_header, $ccol['Code']);
	}
}

##############################################################
# build header [End]
##############################################################

# retrieve student ids
$conds = "";

if($targetClass!="") {
	if(is_numeric($targetClass)) 
		$conds .= " AND y.YearID=".$targetClass;	
	else
		$conds .= " AND yc.YearClassID=".str_replace("::","",$targetClass);	  
}

if($searchText!="") {
	$conds .= " AND (
					".Get_Lang_Selection('yc.ClassTitleB5','yc.ClassTitleEN')." LIKE '%$searchText%' OR 
					USR.EnglishName LIKE '%$searchText%' OR 
					USR.ChineseName LIKE '%$searchText%' OR 
					srs.ID_NO LIKE '%$searchText%' OR 
					srs.EMAIL LIKE '%$searchText%' OR
					USR.HomeTelNo LIKE '%$searchText%'
				)";
}

if($submit_status==1)
{
	$conds .= " and srs2.DATA_CONFIRM_DATE is not null";
}

if($submit_status==-1)
{
	$conds .= " and srs2.DATA_CONFIRM_DATE is null";
}

if ($IncludeEverUpdatedOnly){
	$conds .= " and srs.DateModified IS NOT NULL";
}
if ($StartDate != '') {
	if ($IncludeEverUpdatedOnly){
		$conds .= " and srs.DateModified >='".$StartDate." 00:00:00'";
	}
	else {
		$conds .= " and (srs.DateModified >='".$StartDate." 00:00:00' or srs.DateModified IS NULL)";
	}
}
if ($EndDate != '') {
	if ($IncludeEverUpdatedOnly){
		$conds .= " and srs.DateModified <='".$EndDate." 23:59:59'";
	}
	else {
		$conds .= " and (srs.DateModified <='".$EndDate." 23:59:59' or srs.DateModified IS NULL)";
	}
}

$name_field = getNameFieldByLang("USR.");
	
$cond2 = ($IncludeLeft == '1') ? " AND USR.RecordType IN(2,4) AND USR.RecordStatus IN (0,1,2,3)" : " AND USR.RecordType=2 AND USR.RecordStatus IN (0,1,2)";	
		
$sql = "SELECT 
			USR.UserID 
		FROM
			INTRANET_USER USR LEFT OUTER JOIN
			YEAR_CLASS_USER ycu ON (ycu.UserID=USR.UserID) LEFT OUTER JOIN
			YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID) LEFT OUTER JOIN
			YEAR y ON (y.YearID=yc.YearID) LEFT OUTER JOIN
			STUDENT_REGISTRY_STUDENT srs ON (srs.UserID=USR.UserID) LEFT JOIN 
			{$eclass_db}.PORTFOLIO_STUDENT AS ps ON USR.UserID = ps.UserID LEFT JOIN
			STUDENT_REGISTRY_SUBMITTED srs2 ON (srs2.UserID=USR.UserID AND srs2.AcademicYearID='$AcademicYearID')
		WHERE
			yc.AcademicYearID='$AcademicYearID'
			{$cond2}
			$conds
		ORDER BY
			USR.ClassName, USR.ClassNumber
		";
$StudentIDs_tmp = $lsr->returnVector($sql);

$academicYear = '';
if ($AcademicYearID) {
	$ay = new academic_year($AcademicYearID);
	$engYearName = $ay->YearNameEN;
	$yStr = explode('-',$engYearName);
	$year = '';
	if (count($yStr) > 0) {
		$year = $yStr[0];
		$academicYear = $year;	
	}
}


if(!empty($StudentIDs_tmp))
{
	$student_id_str = implode(",",$StudentIDs_tmp);

	##############################################################
	# retrieve data [Start]
	##############################################################
	$student_info = $lsr->RETRIEVE_STUDENT_INFO_BASIC_HK("",$student_id_str);
	$student_bs_data = $lsr->RETRIEVE_BRO_SIS_HK("",$student_id_str);
	for($g=1;$g<=2;$g++)
		$guardian_data[$g] = $lsr->RETRIEVE_GUARDIAN_INFO_HK("",$g,$student_id_str);
	$contact_data = $lsr->RETRIEVE_EMERGENCY_CONTACT_INFO_HK("",$student_id_str);
	##############################################################
	# retrieve data [End]
	##############################################################
	
	$ExportArr = array();
	
	foreach($StudentIDs_tmp as $k=>$this_StudentID)
	{
		$data = $student_info[$this_StudentID];
		$bs_data = $student_bs_data[$this_StudentID];
		$c_data = $contact_data[$this_StudentID];
		
		##############################################################
		# build export data [Start]
		##############################################################
		$export_data = array();
		
		# Student Information
		$student_DOB = $data['DOB'] == "0000-00-00" ? "" : $data['DOB'];
		
		# display different data
		if($type==1)	# data format
		{
			$student_nationality_data = $lsr->returnPresetCodeName("NATIONALITY", $data['NATION_CODE']);
			$Ethnicity_data = $lsr->returnPresetCodeName("ETHNICITY", $data['ETHNICITY_CODE']);
			$ID_DATA = $lsr->returnPresetCodeName("IDENT_DOC_TYPE", $data['ID_TYPE']);
			$HomeLang_data = $lsr->returnPresetCodeName("FAMILY_LANG", $data['FAMILY_LANG']);
			$Religion_data = $lsr->returnPresetCodeName("RELIGION", $data['RELIGION']);
			$student_district_data = $lsr->returnPresetCodeName("DISTRICT", $data['DISTRICT_CODE']);
			$Address_Area = $Lang['StudentRegistry']['AreaOptions'][$data['ADDRESS_AREA']-1];
			
		} elseif ($type==2 || $type==3)	# for import / websams
		{
			if($type==3)
			{
				$student_district_data = $lsr->returnPresetCodeName("DISTRICT", $data['DISTRICT_CODE']);
				list($dob_y, $dob_m, $dob_d) = explode("-", $student_DOB);
				if($dob_d)
					$student_DOB = $dob_d ."/". $dob_m ."/". $dob_y;
			}
			else {
				$student_district_data = $data['DISTRICT_CODE'];	
			}
			$student_nationality_data = $data['NATION_CODE'];
			$Ethnicity_data = $data['ETHNICITY_CODE'];
			$ID_DATA = $data['ID_TYPE'];
			$HomeLang_data = $data['FAMILY_LANG'];
			$Religion_data = $data['RELIGION'];			
			$Address_Area = $data['ADDRESS_AREA'];
		} 
		
		if (($type==1) || ($type==2)) {
			array_push($export_data, $data['ClassName'],$data['ClassNumber'],$data['WebSAMSRegNo'],$data['EnglishName'], $data['FirstName'], $data['LastName'], $data['ChineseName'], $data['CFirstName'], $data['CLastName'],
						$data['CHN_COMMERCIAL_CODE1'], $data['CHN_COMMERCIAL_CODE2'], $data['CHN_COMMERCIAL_CODE3'], $data['CHN_COMMERCIAL_CODE4'], 
						$data['Gender'], $student_DOB, $student_nationality_data, $Ethnicity_data, $ID_DATA, $data['ID_NO'],
						$HomeLang_data, $Religion_data, $data['CHURCH'], $data['HomeTelNo'], $data['MobileTelNo'],
						$data['UserEmail'], $data['ADDRESS_ROOM_EN'], $data['ADDRESS_FLOOR_EN'], $data['ADDRESS_BLK_EN'], $data['ADDRESS_BLD_EN'], 
						$data['ADDRESS_EST_EN'], $data['ADDRESS_STR_EN'], $student_district_data, $Address_Area
						);
		/*
		if($type==1)
		{
			array_push($export_data, $data['ClassName'],$data['ClassNumber'],$data['WebSAMSRegNo'],$data['EnglishName'], $data['FirstName'], $data['LastName'], $data['ChineseName'], $data['CFirstName'], $data['CLastName'],
					$data['CHN_COMMERCIAL_CODE1'], $data['CHN_COMMERCIAL_CODE2'], $data['CHN_COMMERCIAL_CODE3'], $data['CHN_COMMERCIAL_CODE4'], 
					$data['Gender'], $student_DOB, $student_nationality_data, $Ethnicity_data, $ID_DATA, $data['ID_NO'],
					
					$HomeLang_data, $Religion_data, $data['CHURCH'], $data['HomeTelNo'], $data['MobileTelNo'],
					$data['UserEmail'], $data['ADDRESS_ROOM_EN'], $data['ADDRESS_FLOOR_EN'], $data['ADDRESS_BLK_EN'], $data['ADDRESS_BLD_EN'], 
					$data['ADDRESS_EST_EN'], $data['ADDRESS_STR_EN'], $student_district_data, $Address_Area
					);
		} elseif ($type==2)
		{
			array_push($export_data, $data['ClassName'],$data['ClassNumber'],$data['WebSAMSRegNo'],$data['EnglishName'], $data['FirstName'], $data['LastName'], $data['ChineseName'], $data['CFirstName'], $data['CLastName'],
					$data['CHN_COMMERCIAL_CODE1'], $data['CHN_COMMERCIAL_CODE2'], $data['CHN_COMMERCIAL_CODE3'], $data['CHN_COMMERCIAL_CODE4'], 
					$data['Gender'], $student_DOB, $data['NATION_CODE'], $data['ETHNICITY_CODE'], $data['ID_TYPE'], $data['ID_NO'],
					
					$data['FAMILY_LANG'], $data['RELIGION'], $data['CHURCH'], $data['HomeTelNo'], $data['MobileTelNo'],
					$data['UserEmail'], $data['ADDRESS_ROOM_EN'], $data['ADDRESS_FLOOR_EN'], $data['ADDRESS_BLK_EN'], $data['ADDRESS_BLD_EN'], 
					$data['ADDRESS_EST_EN'], $data['ADDRESS_STR_EN'], $data['DISTRICT_CODE'], $data['ADDRESS_AREA']
					);
		}
		*/
		
					 
			# Last School
			array_push($export_data, $data['LAST_SCHOOL_EN'], $data['LAST_SCHOOL_CH']);
				
			# Brothers & sisters currently studying in school
			for($bs=0;$bs<=2;$bs++)
	 			array_push($export_data, $bs_data[$bs]['BS_UserLogin'], $bs_data[$bs]['StudentName'], $bs_data[$bs]['Relationship']);
			
			# Guardian 
			for($g=1;$g<=2;$g++)
			{
				$this_g = $guardian_data[$g][$this_StudentID];
				$this_g_type = $this_g['PG_TYPE']==1 ? $Lang['StudentRegistry']['Father'] : ($this_g['PG_TYPE']==2 ? $Lang['StudentRegistry']['Mother'] : $this_g['PG_TYPE_OTHERS']);
				//$this_g_religion = $this_g['RELIGION']>0 ? $Lang['StudentRegistry']['ReligionOptions'][$this_g['RELIGION']-1] : $this_g['RELIGION_OTHERS'];
				$this_g_religion = $lsr->returnPresetCodeName("RELIGION", $this_g['RELIGION']);
				$this_g_district_data = $lsr->returnPresetCodeName("DISTRICT", $this_g['DISTRICT_CODE']);
				$this_g_Address_Area = $Lang['StudentRegistry']['AreaOptions'][$this_g['ADDRESS_AREA']-1];
				
				$this_g_type_other = $this_g['PG_TYPE']==0 ? $this_g['PG_TYPE_OTHERS'] : "";
				
				if($type==1)
				{
					array_push($export_data, $this_g_type,$this_g_type_other,$this_g['EMAIL'],$this_g['NAME_E'],$this_g['NAME_C'],
							$this_g['JOB_TITLE_OTHERS'],$this_g['TEL'],$this_g['MOBILE'],$this_g_religion,
							$this_g['CHURCH'],$this_g['ADDRESS_ROOM_EN'],$this_g['ADDRESS_FLOOR_EN'],
							$this_g['ADDRESS_BLK_EN'],$this_g['ADDRESS_BLD_EN'],$this_g['ADDRESS_EST_EN'],
							$this_g['ADDRESS_STR_EN'],$this_g_district_data,$this_g_Address_Area
							);
				} elseif ($type==2)
				{
					array_push($export_data, $this_g['PG_TYPE'],$this_g_type_other,$this_g['EMAIL'],$this_g['NAME_E'],$this_g['NAME_C'],
							$this_g['JOB_TITLE_OTHERS'],$this_g['TEL'],$this_g['MOBILE'],$this_g['RELIGION'],
							$this_g['CHURCH'],$this_g['ADDRESS_ROOM_EN'],$this_g['ADDRESS_FLOOR_EN'],
							$this_g['ADDRESS_BLK_EN'],$this_g['ADDRESS_BLD_EN'],$this_g['ADDRESS_EST_EN'],
							$this_g['ADDRESS_STR_EN'],$this_g['DISTRICT_CODE'],$this_g['ADDRESS_AREA']
							);
				}
			}
	
			# Other Contact Person
			$contact3_title_data = "";
			if($type==1)
			{
				if(trim($c_data['Contact3_Title']))
					$contact3_title_data = $c_data['Contact3_Title']==1 ? $i_title_mr :($c_data['Contact3_Title']==2 ? $i_title_mrs : $i_title_ms);
				else
					$contact3_title_data = "";
			} elseif ($type==2)
			{
				$contact3_title_data = trim($c_data['Contact3_Title'])==0 ? "" : trim($c_data['Contact3_Title']); 
			}
			
			array_push($export_data, $contact3_title_data, $c_data['Contact3_Relationship'], 
					$c_data['Contact3_EnglishName'], $c_data['Contact3_ChineseName'], 
					$c_data['Contact3_Phone'], $c_data['Contact3_Mobile']
			    );
			
			// Custom Column
			
			$custValues = $lsr->getCustomColumnValueByUserID($this_StudentID);
			$tempValues = array();
			foreach($custCols as $ccol){
			    $tempValues[$ccol['Code']] = '';
			}
			foreach($custValues as $cval){
			    $tempValues[$cval['Code']] = $cval['Value'];
			}
			foreach($tempValues as $tval){
			    array_push($export_data, $tval);
			}
		}
		else {		// export for WebSAMS
			$id_no = trim($data['ID_NO']);
			$id_no = str_replace("(","",$id_no);
			$id_no = str_replace(")","",$id_no);
			$id_no = str_replace("（","",$id_no);
			$id_no = str_replace("）","",$id_no);
			
			array_push($export_data, 
						str_replace("#","",$data['WebSAMSRegNo']),
						$academicYear, 
						$data['WEBSAMSCode'],
						$data['ClassName'],
						$data['ClassNumber'],
						$id_no,
						$data['EnglishName'],
						$data['ChineseName'], 
						$data['Gender'],
						$student_DOB,
						$data['DISTRICT_CODE'],
						$Ethnicity_data,
						$HomeLang_data,
						'',
						$data['CHN_COMMERCIAL_CODE1'].$data['CHN_COMMERCIAL_CODE2'].$data['CHN_COMMERCIAL_CODE3'].$data['CHN_COMMERCIAL_CODE4'],
						$id_no,
						$data['BIRTH_CERT_NO'],
						'',
						$Religion_data,
						$student_nationality_data,
						$data['ORIGIN'],
						'',		// set document type to empty 
						'',		// set identity document number to empty 
						'',
						'',
						$data['UserEmail'],
						$data['HomeTelNo'], 
						$data['MobileTelNo'],
						$data['ADDRESS_ROOM_EN'], 
						$data['ADDRESS_FLOOR_EN'], 
						$data['ADDRESS_BLK_EN'],
						'',
						'',
						'', 
						$data['ADDRESS_BLD_EN'],
						'',
						$data['ADDRESS_EST_EN'],
						'', 
						$data['ADDRESS_STR_EN'], 
						'',
						$student_district_data,
						'',
						$Address_Area,
						$data['CHURCH'], 
						$c_data['Contact3_EnglishName'], 
						$c_data['Contact3_ChineseName'],
						'',
						'', 
						$c_data['Contact3_Mobile'],
						'',
						'', 
						'',
						''
						);
		}
					
		##############################################################
		# build export data [End]
		##############################################################
		
		$ExportArr[] = $export_data;
	}
}

$filename = $type==1 ? "student_reg.csv" : "student_reg_import.csv";
//$export_content = $lexport->GET_EXPORT_TXT($ExportArr, $export_header, "\t", "\r\n", "\t", 0, "11");
$export_content .= $lexport->GET_EXPORT_TXT($ExportArr, $export_header);
$lexport->EXPORT_FILE($filename, $export_content);

intranet_closedb();
?>