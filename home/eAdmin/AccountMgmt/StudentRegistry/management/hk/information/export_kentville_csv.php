<?
/*
 *  Using:
 *  
 *  !!! use utf-8 to edit
 *  Note: it retrieves record based on filter condition but not checked records
 *  
 *  2019-03-25 Cameron
 *      - create this file
 */

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccessright.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry_kv.php");

intranet_opendb();

$laccessright = new libaccessright();
if (!$plugin['AccountMgmt_StudentRegistry'] || !$sys_custom['StudentRegistry']['Kentville'] || !$_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"]) {
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit;
}

$lsr = new libstudentregistry_kv();
$lexport = new libexporttext();

##############################################################
# build header [Start]
##############################################################
$export_header = array();
$type = $_GET['type'] ? IntegerSafe($_GET['type']): 4;
switch($type) {
    case 4:
        # Student Information
        array_push($export_header, "ClassName", "ClassNumber", "StudentID", "STU_EngLastName", "STU_EngFirstName", "STU_ChiName",
        "STU_DateOfBirth", "STU_PlaceOfBirth", "STU_BirthCertificateNo", "STU_Gender", "STU_Nationality", "STU_Ethnicity", "STU_HomeLang",
        "STU_AddressFlat", "STU_AddressFloor", "STU_AddressBlock", "STU_AddressBuilding", "STU_AddressEstate",
        "STU_AddressStreet", "STU_AddressDistrict", "STU_AddressArea", "STU_AddressChi", "STU_HomePhone", "STU_SchoolBus", "STU_NannyBus");
        
        # Guardian - father
        array_push($export_header, "FA_EngLastName", "FA_EngFirstName", "FA_ChiName", "FA_BusinessName", "FA_Email",
            "FA_AddressFlat", "FA_AddressFloor", "FA_AddressBlock", "FA_AddressBuilding", "FA_AddressEstate",
            "FA_AddressStreet", "FA_AddressDistrict", "FA_AddressArea", "FA_Title", "FA_Occupation", "FA_IsAlumni", "FA_GraduateYear", "FA_OfficePhone", "FA_Mobile");
        
        # Guardian - mother
        array_push($export_header, "MO_EngLastName", "MO_EngFirstName", "MO_ChiName", "MO_BusinessName", "MO_Email",
            "MO_AddressFlat", "MO_AddressFloor", "MO_AddressBlock", "MO_AddressBuilding", "MO_AddressEstate",
            "MO_AddressStreet", "MO_AddressDistrict", "MO_AddressArea", "MO_Title", "MO_Occupation", "MO_IsAlumni", "MO_GraduateYear", "MO_OfficePhone", "MO_Mobile");
        
        # Last School
        array_push($export_header, "STU_LastSchoolEngName");
        
        # Siblings
        for ($i=1;$i<=3;$i++) {
            array_push($export_header, "STU_SiblingsRelationship_".$i, "STU_SiblingsAge_".$i, "STU_SiblingsName_".$i, "STU_SiblingsIsSchoolPupilOrAlumni_".$i,
                "STU_SiblingsKVClass_".$i, "STU_SiblingsGraduateYear_".$i, "STU_SiblingsCurSchool_".$i, "STU_SiblingsCurLevel_".$i);
        }
        
        # Emergency Contact Person
        for ($i=1;$i<=2;$i++) {
            array_push($export_header, "EmgContact_EngLastName_".$i, "EmgContact_EngFirstName_".$i, "EmgContact_ChiName_".$i,
                "EmgContact_Relationship_".$i, "EmgContact_Telephone_".$i);
        }
        
        break;
        
    case 5:
        # Student Information
        array_push($export_header, "ClassName", "ClassNumber", "StudentID", "STU_EngLastName", "STU_EngFirstName", "STU_ChiName",
        "STU_DateOfBirth", "STU_PlaceOfBirthCode", "STU_BirthCertificateNo", "STU_Gender", "STU_NationalityCode", "STU_EthnicityCode", "STU_HomeLangCode",
        "STU_AddressFlat", "STU_AddressFloor", "STU_AddressBlock", "STU_AddressBuilding", "STU_AddressEstate",
        "STU_AddressStreet", "STU_AddressDistrictCode", "STU_AddressAreaCode", "STU_AddressChi", "STU_HomePhone", "STU_NannyBus");
        
        # Guardian - father
        array_push($export_header, "FA_EngLastName", "FA_EngFirstName", "FA_ChiName", "FA_BusinessName", "FA_Email",
            "FA_AddressFlat", "FA_AddressFloor", "FA_AddressBlock", "FA_AddressBuilding", "FA_AddressEstate",
            "FA_AddressStreet", "FA_AddressDistrictCode", "FA_AddressAreaCode", "FA_Title", "FA_Occupation", "FA_IsAlumni", "FA_GraduateYear", "FA_OfficePhone", "FA_Mobile");
        
        # Guardian - mother
        array_push($export_header, "MO_EngLastName", "MO_EngFirstName", "MO_ChiName", "MO_BusinessName", "MO_Email",
            "MO_AddressFlat", "MO_AddressFloor", "MO_AddressBlock", "MO_AddressBuilding", "MO_AddressEstate",
            "MO_AddressStreet", "MO_AddressDistrictCode", "MO_AddressAreaCode", "MO_Title", "MO_Occupation", "MO_IsAlumni", "MO_GraduateYear", "MO_OfficePhone", "MO_Mobile");
        
        # Last School
        array_push($export_header, "STU_LastSchoolEngName");
        
        # Siblings
        for ($i=1;$i<=3;$i++) {
            array_push($export_header, "STU_SiblingsRelationshipCode_".$i, "STU_SiblingsDateOfBirth_".$i, "STU_SiblingsName_".$i, "STU_SiblingsIsSchoolPupilOrAlumni_".$i,
                "STU_SiblingsKVClass_".$i, "STU_SiblingsGraduateYear_".$i, "STU_SiblingsCurSchool_".$i, "STU_SiblingsCurLevelCode_".$i);
        }
        
        # Emergency Contact Person
        for ($i=1;$i<=2;$i++) {
            array_push($export_header, "EmgContact_EngLastName_".$i, "EmgContact_EngFirstName_".$i, "EmgContact_ChiName_".$i,
                "EmgContact_RelationshipCode_".$i, "EmgContact_Telephone_".$i);
        }
        
        break;
}

##############################################################
# build header [End]
##############################################################

$studentIDAry = $lsr->getSearchStudentID();

if (count($studentIDAry)) {

	##############################################################
	# retrieve data [Start]
	##############################################################
	
    $student_info = $lsr->getStudentInfoBasic($studentIDAry, IntegerSafe($_POST['AcademicYearID']));
    
	for($g=1;$g<=2;$g++) {
	    $guardian_data[$g] = $lsr->getGuardianInfo($studentIDAry,$g);
	}
	
	$student_bs_data = $lsr->getBroterAndSister($studentIDAry);
	
	$contact_data = $lsr->getEmergencyContactInfo($studentIDAry);
	
	##############################################################
	# retrieve data [End]
	##############################################################
	
	$ExportArr = array();
	
	foreach((array)$studentIDAry as $_studentID)
	{
		$data = $student_info[$_studentID];
		$bs_data = $student_bs_data[$_studentID];
		$c_data = $contact_data[$_studentID];
		
		##############################################################
		# build export data [Start]
		##############################################################
		$export_data = array();
		
		# Student Information
		$student_DOB = $data['DOB'] == "0000-00-00" ? "" : $data['DOB'];
		if ($type == 4) {
    		$gender = $lsr->getGenderDisplay($data['Gender']);
    		
    		$placeOfBirth = $lsr->returnPresetCodeName("PLACEOFBIRTH", $data['B_PLACE_CODE']);
    		if ($data['B_PLACE_CODE'] == '999') {
    		    $placeOfBirth = $data['B_PLACE_OTHERS'];
    		}
    		
    		$nationality = $lsr->returnPresetCodeName("NATIONALITY", $data['NATION_CODE']);
    		if ($data['NATION_CODE'] == '999') {
    		    $nationality = $data['NATION_OTHERS'];
    		}
    		
    		$ethnicity = $lsr->returnPresetCodeName("ETHNICITY", $data['ETHNICITY_CODE']);
    		if ($data['ETHNICITY_CODE'] == '999') {
    		    $ethnicity = $data['RACE_OTHERS'];
    		}
    		
    		$homeLang = $lsr->returnPresetCodeName("FAMILY_LANG", $data['FAMILY_LANG']);
    		if ($data['FAMILY_LANG'] == '999') {
    		    $homeLang = $data['FAMILY_LANG_OTHERS'];
    		}
    		
    		$districtName = $lsr->getPresetCodeEnglishName("DISTRICT", $data['DISTRICT_CODE']);
    		if ($data['DISTRICT_CODE'] == '999') {
    		    $districtName = $data['ADDRESS_DISTRICT_EN'];
    		}
    		
    		$addressArea = $Lang['StudentRegistry']['AreaOptions'][$data['ADDRESS_AREA']-1];
		}
		else {
		    $gender = $data['Gender'];
		    
		    $placeOfBirth = $data['B_PLACE_CODE'];
		    if ($data['B_PLACE_CODE'] == '999') {
		        $placeOfBirth = $data['B_PLACE_OTHERS'];
		    }
		    
		    $nationality = $data['NATION_CODE'];
		    if ($data['NATION_CODE'] == '999') {
		        $nationality = $data['NATION_OTHERS'];
		    }
		    
		    $ethnicity = $data['ETHNICITY_CODE'];
		    if ($data['ETHNICITY_CODE'] == '999') {
		        $ethnicity = $data['RACE_OTHERS'];
		    }
		    
		    $homeLang = $data['FAMILY_LANG'];
		    if ($data['FAMILY_LANG'] == '999') {
		        $homeLang = $data['FAMILY_LANG_OTHERS'];
		    }
		    
		    $districtName = $data['DISTRICT_CODE'];
		    if ($data['DISTRICT_CODE'] == '999') {
		        $districtName = $data['ADDRESS_DISTRICT_EN'];
		    }
		    
		    $addressArea= $lsr->getAreaCode($data['ADDRESS_AREA']);
		}
		
		array_push($export_data, $data['ClassName'],$data['ClassNumber'],$data['UserLogin'],$data['LastName'],$data['FirstName'],$data['ChineseName'],
		    $student_DOB,$placeOfBirth,$data['ID_NO'],$gender,$nationality, $ethnicity,  
		    $homeLang,$data['ADDRESS_ROOM_EN'], $data['ADDRESS_FLOOR_EN'], $data['ADDRESS_BLK_EN'], $data['ADDRESS_BLD_EN'], 
		    $data['ADDRESS_EST_EN'], $data['ADDRESS_STR_EN'], $districtName, $addressArea, $data['ADDRESS_STR_CH'], $data['HomeTelNo']
		);
		if ($type == 4) {
		    array_push($export_data,$data['SCHOOL_BUS']);
		}
		array_push($export_data, $data['NANNY_BUS']);
		
		# Guardian 
		for($g=1;$g<=2;$g++) {
			$this_g = $guardian_data[$g][$_studentID];
			
			if ($type == 4) {
			    $this_g_district_data = $lsr->returnPresetCodeName("SUBDISTRICT", $this_g['DISTRICT_CODE']);
			    $this_g_Address_Area = $Lang['StudentRegistry']['AreaOptions'][$this_g['ADDRESS_AREA']-1];
			    $this_g_is_alumni = $this_g['IS_ALUMNI'] ? $Lang['General']['Yes'] : $Lang['General']['No'];
			}
			else {
			    $this_g_district_data = $this_g['DISTRICT_CODE'];
			    $this_g_Address_Area = $lsr->getAreaCode($this_g['ADDRESS_AREA']);
			    $this_g_is_alumni = $this_g['IS_ALUMNI'] ? 'Y' : 'N';
			}
			if ($this_g['DISTRICT_CODE'] == '999') {
			    $this_g_district_data = $this_g['ADDRESS_DISTRICT_EN'];
			}
			
			array_push($export_data, $this_g['LAST_NAME_E'], $this_g['FIRST_NAME_E'], $this_g['NAME_C'],
			    $this_g['BUSINESS_NAME'], $this_g['EMAIL'], $this_g['ADDRESS_ROOM_EN'], $this_g['ADDRESS_FLOOR_EN'],
				$this_g['ADDRESS_BLK_EN'], $this_g['ADDRESS_BLD_EN'], $this_g['ADDRESS_EST_EN'],
			    $this_g['ADDRESS_STR_EN'], $this_g_district_data, $this_g_Address_Area, 
			    $this_g['JOB_TITLE'], $this_g['JOB_NATURE'], $this_g_is_alumni, $this_g['GRADUATE_YEAR'], $this_g['TEL'], $this_g['MOBILE']
    		);
		}
		
		# Last School
		array_push($export_data, $data['LAST_SCHOOL_EN']);
		
		# Siblings
		$nrSiblings = count($bs_data);
		foreach((array)$bs_data as $_bs_data) {
		    if ($type == 4) {
    		    $_relationship = $lsr->returnPresetCodeName("SIBLINGS", $_bs_data['Relationship']);
    		    $ageInfo = $_bs_data['Age'];
    		    $_isSchoolStudentOrAlumni = $_bs_data['IsSchoolStudentOrAlumni'] ? $Lang['General']['Yes'] : $Lang['General']['No'];
    		    $_curLevel = $lsr->returnPresetCodeName("SCHOOLLEVEL", $_bs_data['CurLevel']);
		    }
		    else {
		        $_relationship = $_bs_data['Relationship'];
		        $ageInfo = $_bs_data['DateOfBirth'];
		        $_isSchoolStudentOrAlumni = $_bs_data['IsSchoolStudentOrAlumni'] ? 'Y' : 'N';
		        $_curLevel = $_bs_data['CurLevel'];
		    }
		    if ($_bs_data['CurLevel'] == '999') {
		        $_curLevel = $_bs_data['CurLevelOthers'];
		    }
		    $_graduateYear = $_bs_data['GraduateYear'] ? $_bs_data['GraduateYear'] : '';
		    array_push($export_data, $_relationship, $ageInfo, $_bs_data['StudentName'], $_isSchoolStudentOrAlumni, $_bs_data['SchoolClass'], 
		        $_graduateYear, $_bs_data['CurSchool'], $_curLevel
		    );
		}
		
		for($i=$nrSiblings;$i<3;$i++) {
            for($j=0;$j<8;$j++){
                array_push($export_data, '');
            }
		}

		# Emergency Contact Person
		foreach((array)$c_data as $_c_data) {
		    if ($type == 4) {
		        $_relationship = $lsr->returnPresetCodeName("RELATIONSHIP",  $_c_data['Contact3_RelationshipCode']);
		    }
		    else {
		        $_relationship = $_c_data['Contact3_RelationshipCode'];
		    }
		    if ($_c_data['Contact3_RelationshipCode'] == '999') {
		        $_relationship = $_c_data['Contact3_Relationship'];
		    }
		    
		    array_push($export_data, $_c_data['Contact3_EglishLastName'], $_c_data['Contact3_EglishFirstName'],
		        $_c_data['Contact3_ChineseName'], $_relationship,
		        $_c_data['Contact3_Mobile']
		    );
		}
					
		##############################################################
		# build export data [End]
		##############################################################
		
		$ExportArr[] = $export_data;
	}
}

$filename = "student_data_record.csv";
//$export_content = $lexport->GET_EXPORT_TXT($ExportArr, $export_header, "\t", "\r\n", "\t", 0, "11");
$export_content .= $lexport->GET_EXPORT_TXT($ExportArr, $export_header);
$lexport->EXPORT_FILE($filename, $export_content);

intranet_closedb();
?>