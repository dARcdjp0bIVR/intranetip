<?php
// using : 

###########################################
#
#	Date:	2019-04-29 Philips
#           Create file
#
###########################################

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libaccessright.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
intranet_auth();
intranet_opendb();

$laccessright = new libaccessright();
if(!$plugin['AccountMgmt_StudentRegistry'] || (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-View") && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-Manage")) && !$_SESSION['ParentFillOnlineReg']) {
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$lexport = new libexporttext();

$linterface 	= new interface_html();
$lsr = new libstudentregistry();
$filename = "student_reg_sample.csv";

$file_format = array();
# Student Information
array_push($file_format, "ClassName", "ClassNumber", "WebSAMSRegNo", "STU_EngName", "STU_EngFirstName", "STU_EngLastName", "STU_ChiName", "STU_ChiFirstName",
    "STU_ChiLastName", "STU_ChiCommercialCode1", "STU_ChiCommercialCode2", "STU_ChiCommercialCode3", "STU_ChiCommercialCode4",
    "STU_Gender", "STU_DOB", "STU_NationalityCode", "STU_EthnicityCode", "STU_IDTypeCode", "STU_IDNo", "STU_HomeLangCode",
    "STU_ReligionCode", "STU_Church", "STU_HomePhone", "STU_Mobile", "STU_Email",
    "STU_AddressFlat", "STU_AddressFloor", "STU_AddressBlock", "STU_AddressBuilding", "STU_AddressEstate",
    "STU_AddressStreet", "STU_AddressDistrictCode", "STU_AddressAreaCode");
# Last School
array_push($file_format, "STU_LastSchoolEngName", "STU_LastSchoolChiName");

# Brothers & sisters currently studying in school
array_push($file_format, "STU_BroSisLoginID1", "STU_BroSisName1", "STU_BroSisRelationship1", "STU_BroSisLoginID2", "STU_BroSisName2", "STU_BroSisRelationship2", "STU_BroSisLoginID3", "STU_BroSisName3", "STU_BroSisRelationship3");

# Guardian
for($i=1;$i<=2;$i++)
{
    array_push($file_format, "GRD_TypeCode".$i, "GRD_TypeOther".$i, "GRD_Email".$i, "GRD_EngName".$i, "GRD_ChiName".$i, "GRD_Occupation".$i,
        "GRD_DayPhone".$i, "GRD_Mobile".$i, "GRD_ReligionCode".$i, "GRD_Church".$i,
        "GRD_AddressFlat".$i, "GRD_AddressFloor".$i, "GRD_AddressBlock".$i, "GRD_AddressBuilding".$i, "GRD_AddressEstate".$i,
        "GRD_AddressStreet".$i, "GRD_AddressDistrictCode".$i, "GRD_AddressAreaCode".$i);
}

# Other Contact Person
array_push($file_format, "OtherConact_TitleCode", "OtherConact_Relationship", "OtherConact_EngName", "OtherConact_ChiName",
    "OtherConact_DayPhone", "OtherConact_Mobile");

// Custom Column
$custCols = $lsr->getCustomColumn();
foreach($custCols as $ccol){
    array_push($file_format, $ccol['Code']);
}

// empty data
$data = array();
for($i=0;$i<count($file_format);$i++){
    $data[$i] = '';
}

$file_content .= $lexport->GET_EXPORT_TXT($data, $file_format);
$lexport->EXPORT_FILE($filename, $file_content);
?>