<?
#using:  
/*
 *  2019-06-20 Cameron
 *      - show error message in red color [case #B162946]
 *      - fix potential sql injection by applying IntegerSafe to studentID      
 */

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccessright.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry.php");

intranet_auth();
intranet_opendb();

$laccessright = new libaccessright();

if(!$plugin['AccountMgmt_StudentRegistry'] || (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-View") && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-Manage")) && !$_SESSION['ParentFillOnlineReg']) 
{
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$lsr = new libstudentregistry();

$thisAry = array();

/*
$thisAry['NAME_C'] 	= intranet_htmlspecialchars(stripslashes(trim($ChineseName)));


$error = array();
$error = $lsr->checkData_Macau('edit', $thisAry, $Mode);
$err_msg = "";
*/

/*
if(sizeof($error['StudentInfo'])>0) {
	$err_msg .= "<font color='red'><ul>";
	foreach($error['StudentInfo'] as $key=>$val) {
		$err_msg .= "<li>".$Lang['StudentRegistry']['StudInfo']." - $val</li>";
	}
   	$err_msg .= "</ul></font>";
}

if(sizeof($error['FatherInfo'])>0) {
	$err_msg .= "<font color='red'><ul>";
	foreach($error['FatherInfo'] as $key=>$val) {
		$err_msg .= "<li>".$Lang['StudentRegistry']['FatherInfo']." - $val</li>";
	}
   	$err_msg .= "</ul></font>";
}

if(sizeof($error['MotherInfo'])>0) {
	$err_msg .= "<font color='red'><ul>";
	foreach($error['MotherInfo'] as $key=>$val) {
		$err_msg .= "<li>".$Lang['StudentRegistry']['MotherInfo']." - $val</li>";
	}
   	$err_msg .= "</ul></font>";
}

if(sizeof($error['GuardianInfo'])>0) {
	$err_msg .= "<font color='red'><ul>";
	foreach($error['GuardianInfo'] as $key=>$val) {
		$err_msg .= "<li>".$Lang['StudentRegistry']['GuardianInfo']." - $val</li>";
	}
   	$err_msg .= "</ul></font>";
}
*/

########################################
# Compulsory fields:
#	Student	-	English Name, Chinese Name, UserEmail
########################################
/*
##### checked with javascript
if(trim($english_name)=="")
	$err_msg .= "<li>".$Lang['StudentRegistry']['StudInfo']." - ". $Lang['StudentRegistry']['EnglishName'] . " - " . $Lang['StudentRegistry']['IsMissing'] ."</li>";
if(trim($chinese_name)=="")
	$err_msg .= "<li>".$Lang['StudentRegistry']['StudInfo']." - ". $Lang['StudentRegistry']['ChineseName'] . " - " . $Lang['StudentRegistry']['IsMissing'] ."</li>";
if(trim($student_email)=="")
	$err_msg .= "<li>".$Lang['StudentRegistry']['StudInfo']." - ". $Lang['StudentRegistry']['EMailAddress'] . " - " . $Lang['StudentRegistry']['IsMissing'] ."</li>";
*/
	
########################################
# Unique
#	Student	- HKID, PassportNo, DocumentIDNo, UserEmail
#	Guardian - Email 
########################################
##### Student
$studentID = IntegerSafe($studentID);
if($passport_type=="04" && trim($id_no)!="")
{
	$sql = "select count(UserID) from INTRANET_USER where HKID='$id_no' and UserID!='$studentID'";
	$result = $lsr->returnVector($sql);
	
	$sql2 = "select count(StudentID) from STUDENT_REGISTRY_PG where HKID='$id_no' and StudentID!='$studentID'";
	$result2 = $lsr->returnVector($sql2);
	
	if($result[0]>0 || $result2[0]>0)
		$err_msg .= "<li style='color:red'>".$Lang['StudentRegistry']['StudInfo']." - ". $Lang['StudentRegistry']['ID_DocumentNo'] . " - " . $Lang['StudentRegistry']['alreadyExist'] ."</li>";
}

if(trim($student_email)!="")
{
	$sql = "select count(UserID) from INTRANET_USER where UserEmail='$student_email' and UserID!='$studentID'";
	$result = $lsr->returnVector($sql);
	
	$sql2 = "select count(StudentID) from STUDENT_REGISTRY_PG where EMAIL='$student_email' and StudentID!='$studentID'";
	$result2 = $lsr->returnVector($sql2);
	
	if($result[0]>0 || $result2[0]>0)
		$err_msg .= "<li style='color:red'>".$Lang['StudentRegistry']['StudInfo']." - ". $Lang['StudentRegistry']['EMailAddress'] . " - " . $Lang['StudentRegistry']['alreadyExist'] ."</li>";
}

if($passport_type=="02" && trim($id_no)!="")
{
	$sql = "select count(UserID) from INTRANET_USER_PERSONAL_SETTINGS where PassportNo='$id_no' and UserID!='$studentID'";
	$result = $lsr->returnVector($sql);
	if($result[0]>0)
		$err_msg .= "<li style='color:red'>".$Lang['StudentRegistry']['StudInfo']." - ". $Lang['StudentRegistry']['ID_DocumentNo'] . " - " . $Lang['StudentRegistry']['alreadyExist'] ."</li>";
}

if($passport_type==2 && $id_no!="")
{
	$sql = "select count(UserID) from STUDENT_REGISTRY_STUDENT where ID_NO='$id_no' and UserID!='$studentID'";
	$result = $lsr->returnVector($sql);
	if($result[0]>0)
		$err_msg .= "<li style='color:red'>".$Lang['StudentRegistry']['StudInfo']." - ". $Lang['StudentRegistry']['ID_DocumentType'] . " - " . $Lang['StudentRegistry']['alreadyExist'] ."</li>";
}

##### Guardian
/*
for($g=1;$g<=2;$g++)
{
	$this_g_email = ${"guardian_email".$g};
	if(trim($this_g_email)!="")
	{
		$sql = "select count(UserID) from INTRANET_USER where UserEmail='$this_g_email' and UserID!=$studentID";
		$result = $lsr->returnVector($sql);
		
		$sql2 = "select count(StudentID) from STUDENT_REGISTRY_PG where EMAIL='$this_g_email' and StudentID!=$studentID";
		$result2 = $lsr->returnVector($sql2);
		
		if($result[0]>0 || $result2[0]>0)
			$err_msg .= "<li>".$Lang['StudentRegistry']['StudInfo']." - ". $Lang['StudentRegistry']['EMailAddress'] . " - " . $Lang['StudentRegistry']['alreadyExist'] ."</li>";
	}
	
	
}
exit;
*/

########################################
# Check format
#	Student	-	UserEmail
#	Guardian - Email  xxxxxx
########################################
if(!intranet_validateEmail($student_email))
	$err_msg .= "<li style='color:red'>".$Lang['StudentRegistry']['StudInfo']." - ". $Lang['StudentRegistry']['EMailAddress'] . " - " . $Lang['StudentRegistry']['IsInvalid'] ."</li>";



if(sizeof($error)>0) {
	$err_msg .= "<font color='red'><ul>";
	foreach($error as $key=>$val) {
		$err_msg .= "<li>$val</li>";
	}
   	$err_msg .= "</ul></font>";
}

echo $err_msg;

intranet_closedb();
?>