<?
/*
 * 	Log
 *  
 *  2019-05-15 Cameron
 *      - fix export from admin (all student of the filter criterias but not selected students) 
 *      
 *  2019-05-09 Cameron
 *      - use studentID as export file name
 *      
 * 	2019-03-22 Cameron
 * 		- create this file
 */
 
ini_set('memory_limit',-1);	// unlimit
set_time_limit(0);			// unlimit

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccessright.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry_kv.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry_ui.php");
require_once($PATH_WRT_ROOT."includes/mpdf/mpdf.php");

intranet_auth();
intranet_opendb();

$laccessright = new libaccessright();

if (!$plugin['AccountMgmt_StudentRegistry'] || !$sys_custom['StudentRegistry']['Kentville'] || (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] && !$_SESSION['ParentFillOnlineReg'])) {
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit;
}

$lsr = new libstudentregistry_kv();

if ($_GET['studentID']) {   // from Parent or one student from Admin
    $studentID = IntegerSafe($studentID);
    $studentIDAry = array($studentID);
//     $studentInfo = $lsr->getStudentKeyInfo($studentID);
//     $filename = $reportTitle . '_'. $studentInfo['EnglishName'];
    $filename = $studentID;
}
else {
    $studentIDAry = $lsr->getSearchStudentID();
    if (count($studentIDAry) == 1) {
        $filename = $studentIDAry[0];
    }
    else {
        $filename = 'student_data_record';
    }
}

if($_SESSION['ParentFillOnlineReg']) {
    $isMyChild = $lsr->verifyParentChild($studentID, $_SESSION['UserID']);
    if (!$isMyChild) {
        $laccessright->NO_ACCESS_RIGHT_REDIRECT();
        exit;
    }
}


$printDate = date("d/m/Y");
// $reportTitle = $Lang['StudentRegistry']['StudentDataRecord'].' ('.getCurrentAcademicYear('en').')';
// $filename = $reportTitle;

$nrStudent = count($studentIDAry);
$lui = new libstudentregistry_ui();

//start creating PDF
//new mPDF ($mode,$format,$default_font_size,$default_font,$margin_left,$margin_right,$margin_top,$margin_bottom,$margin_header,$margin_footer,$orientation)
// 	$margin_left = 18; //mm
// 	$margin_right = 18;
// 	$margin_top = 14;
// 	$margin_bottom = 20;
// 	$margin_header = 12;
// 	$margin_footer = 12;
//	$pdf = new mPDF('','A4',0,'',$margin_left,$margin_right,$margin_top,$margin_bottom,$margin_header,$margin_footer);
	$pdf = new mPDF('','A4');
	
	// Chinese use mingliu 
//	$pdf->backupSubsFont = array('mingliu');
	$pdf->useSubstitutions = false;
	$pdf->setAutoTopMargin = 'stretch';
	
	$evenHeaderAry['html'] = '';
	$evenHeaderAry['h'] = 0;
	
	// header footer style
	$style = $lui->getPdfStyleSheet();
	

	// Gen PDF here
	for($y = 0; $y<$nrStudent; $y++){
        $_studentID = $studentIDAry[$y];
        
        $content = $lui->getPdfStudentDataRecord($_studentID);
        
        $reportHeader = $lui->getPdfStudentDataRecordHeader($_studentID, $reportTitle);
        $pdf->SetHTMLHeader($reportHeader);
		
		if($y == 0){
			$pdf->WriteHTML($style);		
		}
		
		if($y != 0 ){
		    $pdf->AddPage("","", 1);	// reset page number
		    $pdf->SetHTMLHeader($reportHeader);
		}
        
		$pdf->pagenumPrefix = $Lang['StudentRegistry']['ExportPdf']['PageNumSuffix'];
		$pdf->nbpgPrefix = $Lang['StudentRegistry']['ExportPdf']['TotPageNumPrefix'];
		$pdf->nbpgSuffix = $Lang['StudentRegistry']['ExportPdf']['TotPageNumSuffix'];
		$pdf->defaultfooterline=0;
		$pdf->PAGE_NUMBER_SPECIAL = '2';
		
		$pdf->SetFooter('<div style="text-align: center;">{PAGENO}{nbpg}</div>');
		
		// write into PDF
		$pdf->WriteHTML($content);
		
	}
	
	$pdf->Output($filename.'.pdf', 'D');       // download, not directly open in browser

?>