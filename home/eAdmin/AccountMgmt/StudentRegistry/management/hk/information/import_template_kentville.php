<?php
// Editing by

/*
 *  Using:
 *
 *  2019-05-09 Cameron
 *      - create this file
 */

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccessright.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

$laccessright = new libaccessright();

if (!$plugin['AccountMgmt_StudentRegistry'] || !$sys_custom['StudentRegistry']['Kentville'] || !$_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"]) {
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit;
}

$lexport = new libexporttext();

$filename = "student_data_record_sample.csv";

$file_format = array(
    'ClassName',
    'ClassNumber',
    'StudentID',
    'STU_EngLastName',
    'STU_EngFirstName',
    'STU_ChiName',
    'STU_DateOfBirth',
    'STU_PlaceOfBirthCode',
    'STU_BirthCertificateNo',
    'STU_Gender',
    'STU_NationalityCode',
    'STU_EthnicityCode',
    'STU_HomeLangCode',
    'STU_AddressFlat',
    'STU_AddressFloor',
    'STU_AddressBlock',
    'STU_AddressBuilding',
    'STU_AddressEstate',
    'STU_AddressStreet',
    'STU_AddressDistrictCode',
    'STU_AddressAreaCode',
    'STU_AddressChi',
    'STU_HomePhone',
    'STU_NannyBus',
    'FA_EngLastName',
    'FA_EngFirstName',
    'FA_ChiName',
    'FA_BusinessName',
    'FA_Email',
    'FA_AddressFlat',
    'FA_AddressFloor',
    'FA_AddressBlock',
    'FA_AddressBuilding',
    'FA_AddressEstate',
    'FA_AddressStreet',
    'FA_AddressDistrictCode',
    'FA_AddressAreaCode',
    'FA_Title',
    'FA_Occupation',
    'FA_IsAlumni',
    'FA_GraduateYear',
    'FA_OfficePhone',
    'FA_Mobile',
    'MO_EngLastName',
    'MO_EngFirstName',
    'MO_ChiName',
    'MO_BusinessName',
    'MO_Email',
    'MO_AddressFlat',
    'MO_AddressFloor',
    'MO_AddressBlock',
    'MO_AddressBuilding',
    'MO_AddressEstate',
    'MO_AddressStreet',
    'MO_AddressDistrictCode',
    'MO_AddressAreaCode',
    'MO_Title',
    'MO_Occupation',
    'MO_IsAlumni',
    'MO_GraduateYear',
    'MO_OfficePhone',
    'MO_Mobile',
    'STU_LastSchoolEngName',
    'STU_SiblingsRelationshipCode_1',
    'STU_SiblingsDateOfBirth_1',
    'STU_SiblingsName_1',
    'STU_SiblingsIsSchoolPupilOrAlumni_1',
    'STU_SiblingsKVClass_1',
    'STU_SiblingsGraduateYear_1',
    'STU_SiblingsCurSchool_1',
    'STU_SiblingsCurLevelCode_1',
    'STU_SiblingsRelationshipCode_2',
    'STU_SiblingsDateOfBirth_2',
    'STU_SiblingsName_2',
    'STU_SiblingsIsSchoolPupilOrAlumni_2',
    'STU_SiblingsKVClass_2',
    'STU_SiblingsGraduateYear_2',
    'STU_SiblingsCurSchool_2',
    'STU_SiblingsCurLevelCode_2',
    'STU_SiblingsRelationshipCode_3',
    'STU_SiblingsDateOfBirth_3',
    'STU_SiblingsName_3',
    'STU_SiblingsIsSchoolPupilOrAlumni_3',
    'STU_SiblingsKVClass_3',
    'STU_SiblingsGraduateYear_3',
    'STU_SiblingsCurSchool_3',
    'STU_SiblingsCurLevelCode_3',
    'EmgContact_EngLastName_1',
    'EmgContact_EngFirstName_1',
    'EmgContact_ChiName_1',
    'EmgContact_RelationshipCode_1',
    'EmgContact_Telephone_1',
    'EmgContact_EngLastName_2',
    'EmgContact_EngFirstName_2',
    'EmgContact_ChiName_2',
    'EmgContact_RelationshipCode_2',
    'EmgContact_Telephone_2'
);

// empty data
$data = array();
$data[] = array(
    "1A",
    "2",
    "s1011a0123",
    "Ma",
    "Sealing",
    "馬 小 玲",
    "2016-01-25",
    "FRA",
    "A190321(A)",
    "F",
    "BCI",
    "ZWH",
    "ENG",
    "D",
    "20/F.",
    "I",
    "The Belcher's",
    "89 Pok Fu Lam Road,",
    "Pok Fu Lam,",
    "A05",
    "HK",
    "香港中西區薄扶林薄扶林道89號寶翠園第一期20樓D室",
    "20190321",
    "娥姐 / 6019 0321",
    "Ma",
    "Kelmen",
    "馬家明",
    "China Coco Dev. (Holding) Far East Ltd.",
    "kelmen_ma@chinacocodev.com",
    "A",
    "28/F.",
    "C",
    "China Coco Stand Centre",
    "",
    "28 Queen's Road Central,",
    "C05",
    "HK",
    "Manager Director",
    "商界領袖",
    "Y",
    "1988",
    "25190321",
    "61190321",
    "Chu",
    "Mandy",
    "朱嫚迪",
    "American Express",
    "mandychu@american_express.com",
    "RM 2",
    "16/F.",
    "B",
    "Treasure Building",
    "KowloonBay CBD",
    "12 Wai Yip Street",
    "J02",
    "KLN",
    "Sales Executive",
    "Merchandiser",
    "Y",
    "1990",
    "23190321",
    "81190321",
    "Kowloon Tong Kindergarten",
    "YS",
    "2017-10-13",
    "Cindy Ma",
    "N",
    "",
    "",
    "Since Kindergarten",
    "PRN",
    "EB",
    "2015-08-21",
    "Kelvin Ma",
    "Y",
    "3KM",
    "",
    "",
    "",
    "ES",
    "2012-01-30",
    "Angela Ma",
    "Y",
    "K3B",
    "2017",
    "Tong Sam Pri.School",
    "Sunshine Kindergarten",
    "Chan",
    "Wendy",
    "陳婉琪",
    "12",
    "91190321",
    "Amber",
    "Sarah",
    "沙拉",
    "菲傭姐姐",
    "51190321"
);

// for($i=0;$i<count($file_format);$i++){
//     $data[$i] = '';
// }

$file_content .= $lexport->GET_EXPORT_TXT($data, $file_format, $Delimiter="", $LineBreak="\r\n", $ColumnDefDelimiter="", $DataSize=0, $Quoted="11");

$lexport->EXPORT_FILE($filename, $file_content);
?>