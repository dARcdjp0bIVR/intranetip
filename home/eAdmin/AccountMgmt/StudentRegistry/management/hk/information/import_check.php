<?php
# using: 

###########################################
#
#   Date:   2019-04-29  Philips
#           add Custom Column    
#
#	Date:	2014-05-05	YatWoon
#			add BroSisName
#
#	Date:	2013-06-25
#
###########################################

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libftp.php");
include_once($PATH_WRT_ROOT."includes/libimport.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libaccessright.php");

intranet_auth();
intranet_opendb();

$laccessright = new libaccessright();
if(!$plugin['AccountMgmt_StudentRegistry'] || (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-View") && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-Manage")) && !$_SESSION['ParentFillOnlineReg']) {
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$lsr = new libstudentregistry();
$linterface 	= new interface_html();
$limport = new libimporttext();
$lo = new libfilesystem();
$li = new libimport();
$lu = new libuser();

### CSV Checking
$name = $_FILES['userfile']['name'];
$ext = strtoupper($lo->file_ext($name));

if(!($ext == ".CSV" || $ext == ".TXT"))
{
	intranet_closedb();
	header("location: import.php?xmsg=import_failed");
	exit();
}
$data = $limport->GET_IMPORT_TXT($userfile);
$csv_header = array_shift($data);


# check csv header
##############################################################
# build header [Start]
##############################################################
$file_format = array();
# Student Information
array_push($file_format, "ClassName", "ClassNumber", "WebSAMSRegNo", "STU_EngName", "STU_EngFirstName", "STU_EngLastName", "STU_ChiName", "STU_ChiFirstName", 
				"STU_ChiLastName", "STU_ChiCommercialCode1", "STU_ChiCommercialCode2", "STU_ChiCommercialCode3", "STU_ChiCommercialCode4",
				"STU_Gender", "STU_DOB", "STU_NationalityCode", "STU_EthnicityCode", "STU_IDTypeCode", "STU_IDNo", "STU_HomeLangCode",
				"STU_ReligionCode", "STU_Church", "STU_HomePhone", "STU_Mobile", "STU_Email",
				"STU_AddressFlat", "STU_AddressFloor", "STU_AddressBlock", "STU_AddressBuilding", "STU_AddressEstate",
				"STU_AddressStreet", "STU_AddressDistrictCode", "STU_AddressAreaCode");
# Last School
array_push($file_format, "STU_LastSchoolEngName", "STU_LastSchoolChiName");

# Brothers & sisters currently studying in school
array_push($file_format, "STU_BroSisLoginID1", "STU_BroSisName1", "STU_BroSisRelationship1", "STU_BroSisLoginID2", "STU_BroSisName2", "STU_BroSisRelationship2", "STU_BroSisLoginID3", "STU_BroSisName3", "STU_BroSisRelationship3");

# Guardian 
for($i=1;$i<=2;$i++)
{
	array_push($file_format, "GRD_TypeCode".$i, "GRD_TypeOther".$i, "GRD_Email".$i, "GRD_EngName".$i, "GRD_ChiName".$i, "GRD_Occupation".$i,
	"GRD_DayPhone".$i, "GRD_Mobile".$i, "GRD_ReligionCode".$i, "GRD_Church".$i,
	"GRD_AddressFlat".$i, "GRD_AddressFloor".$i, "GRD_AddressBlock".$i, "GRD_AddressBuilding".$i, "GRD_AddressEstate".$i,
	"GRD_AddressStreet".$i, "GRD_AddressDistrictCode".$i, "GRD_AddressAreaCode".$i);
}

# Other Contact Person
array_push($file_format, "OtherConact_TitleCode", "OtherConact_Relationship", "OtherConact_EngName", "OtherConact_ChiName",
			"OtherConact_DayPhone", "OtherConact_Mobile");

$ori_file_format = $file_format;
$custCols = $lsr->getCustomColumn();
foreach($custCols as $ccol){
    array_push($file_format, $ccol['Code']);
}
##############################################################
# build header [End]
##############################################################
$format_wrong = false;
for($i=0; $i<sizeof($file_format); $i++)
{
	if ($csv_header[$i]!=$file_format[$i])
	{
		$format_wrong = true;
		break;
	}
}
if($format_wrong)
{
	intranet_closedb();
	header("location:  import.php?xmsg=import_header_failed");
	exit();
}

if(empty($data))
{
	intranet_closedb();
	header("location: import.php?xmsg=import_failed");
	exit();
}
// debug_pr($data);

# print for data checking
// for($i=0;$i<sizeof($data);$i++)
// {
// 	for($j=0;$j<sizeof($csv_header);$j++)
// 		echo $j . " [". $csv_header[$j] ."] => ". $data[$i][$j] ."<br>";
// }


##### TEMP table
# store to temp table
$sql = "CREATE TABLE IF NOT EXISTS TEMP_STUDENT_REG_IMPORT
(
 RecordID int(11) NOT NULL auto_increment,
 StudentID int(8) default NULL,
 ";
 foreach($ori_file_format as $k=>$field)	$sql .= $field . " varchar(200) default NULL, ";
 $sql .= "
 InputBy int(8) default NULL,
 DateInput datetime default NULL,
 PRIMARY KEY (RecordID)
)ENGINE=InnoDB DEFAULT CHARSET=utf8";
$lsr->db_db_query($sql);

# add extra columns in TEMP_STUDENT_REG_IMPORT
$sql = "alter table TEMP_STUDENT_REG_IMPORT 
			ADD COLUMN STU_BroSisName1 varchar(50) default NULL AFTER STU_BroSisLoginID1, 
			ADD COLUMN STU_BroSisName2 varchar(50) default NULL AFTER STU_BroSisLoginID2, 
			ADD COLUMN STU_BroSisName3 varchar(50) default NULL AFTER STU_BroSisLoginID3";
$lsr->db_db_query($sql);

# add custom columns in TEMP_STUDENT_REG_IMPORT
if(sizeof($custCols) > 0){
    
    $sql = "CREATE TABLE IF NOT EXISTS TEMP_STUDENT_REG_IMPORT_CUST
    (
     RecordID int(11) NOT NULL,
     ";
     foreach($custCols as $ccol)	$sql .= $ccol[Code] . " varchar(200) default NULL, ";
        $sql .= "
     InputBy int(8) default NULL,
     DateInput datetime default NULL,
     PRIMARY KEY (RecordID)
    )ENGINE=InnoDB DEFAULT CHARSET=utf8";
    $lsr->db_db_query($sql);
    
    $sql = "alter table TEMP_STUDENT_REG_IMPORT_CUST ";
    foreach($custCols as $ccol){
        $exsql = $sql . "ADD COLUMN $ccol[Code] varchar(200) default NULL";
        $lsr->db_db_query($exsql);
    }
//     debug_pr($sql);
//     $sql = substr($sql, 0, -1);
//     $lsr->db_db_query($sql);
}

# remove own old records
$sql = "delete from TEMP_STUDENT_REG_IMPORT where InputBy=$UserID";
$lsr->db_db_query($sql);
		
##### check data [start]
$error_msg = array();
foreach($data as $row=>$d)
{
	# Retrieve UserID
	$thisUserID = "";
	$thisClassName = $d[0];
	$thisClassNumber = $d[1];	
	$thisWebSAMSRegNo = $d[2];
	if($thisClassName!="" && $thisClassNumber!="")
	{
		$thisUserID = $lu->returnUserID($thisClassName, $thisClassNumber);
	}
	if($thisUserID=="" && $thisWebSAMSRegNo!="")
	{
		$thisUserID = $lu->getUserIDByWebSamsRegNo($thisWebSAMSRegNo);
	}
	if($thisUserID=="")
	{
		$error_msg[$row]['reason'][] = $i_Profile_StudentNotFound;
	}
	else
	{
		/* 
		# complusory fields
		$except_complusory = array(0,1,2,12,27,29,30,33,34,35,36,37,38,39,40,42,51,52,53,54,55,56,58,60,69,70,71,72,73,74,76);
		$with_comp = 0;
		for($i=0;$i<sizeof($d);$i++)
		{
			if(in_array($i,$except_complusory))	continue;
			if(trim($d[$i])=="")	
			{	
				//debug_pr($i);
				$with_comp = 1;
			}
		}
		if($with_comp)	
		{
			$error_msg[$row]['reason'][] = $Lang['SysMgr']['FormClassMapping']['MissingData'];
		}
		*/
		
		# Email is a must!
		if(!trim($d[24]))
		{
			$error_msg[$row]['reason'][] = $Lang['SysMgr']['FormClassMapping']['MissingData'];
		}
		
		# DOB format
		if(!empty($d[14]))
		{
			$d[14] = getDefaultDateFormat($d[14]);
		}
		
		# Nationality
		if(!$lsr->returnPresetCodeName("NATIONALITY", $d[15]))
		{
			//$error_msg[$row]['reason'][] = $profile_nationality ." " . $Lang['eEnrolment']['Code'] . $Lang['General']['_Invalid'];
			$d[15] = "999";
		}
		
		# Ethnicity
		if(!$lsr->returnPresetCodeName("ETHNICITY", $d[16]))
		{
			//$error_msg[$row]['reason'][] = $Lang['StudentRegistry']['Race'] ." " . $Lang['eEnrolment']['Code'] . $Lang['General']['_Invalid'];
			$d[16] = "999";
		}

		# ID Document Type
		if(!$lsr->returnPresetCodeName("IDENT_DOC_TYPE", $d[17]))
		{
			//$error_msg[$row]['reason'][] = $Lang['StudentRegistry']['ID_DocumentType'] ." " . $Lang['eEnrolment']['Code'] . $Lang['General']['_Invalid'];
			$d[17] = "09";
		}
		
		# Language Spoken at Home
		if(!$lsr->returnPresetCodeName("FAMILY_LANG", $d[19]))
		{
			//$error_msg[$row]['reason'][] = $Lang['StudentRegistry']['HomeLang'] ." " . $Lang['eEnrolment']['Code'] . $Lang['General']['_Invalid'];
			$d[19] = "999";
		}
		
		# Religion
		if(!$lsr->returnPresetCodeName("RELIGION", $d[20]))
		{
			//$error_msg[$row]['reason'][] = $Lang['StudentRegistry']['Religion'] ." " . $Lang['eEnrolment']['Code'] . $Lang['General']['_Invalid'];
			$d[20] = "9";
		}

		# District
		if(!$lsr->returnPresetCodeName("DISTRICT", $d[31]))
		{
			//$error_msg[$row]['reason'][] = $Lang['StudentRegistry']['AddressField']['District'] ." " . $Lang['eEnrolment']['Code'] . $Lang['General']['_Invalid'];
			$d[31] = "X";
		}

		# Area Code
		/*
		if($d[32]<0 || $d[32]>sizeof($Lang['StudentRegistry']['AreaOptions']))
		{
			$error_msg[$row]['reason'][] = $Lang['StudentRegistry']['AddressField']['Area'] ." " . $Lang['eEnrolment']['Code'] . $Lang['General']['_Invalid'];
		}
		*/
		
		# guardian Religion
		if(!$lsr->returnPresetCodeName("RELIGION", $d[52]))
		{
			//$error_msg[$row]['reason'][] = $Lang['Identity']['Guardian'] . $Lang['StudentRegistry']['Religion'] ." " . $Lang['eEnrolment']['Code'] . $Lang['General']['_Invalid'];
			$d[52] = "9";
		}
		if(!$lsr->returnPresetCodeName("RELIGION", $d[70]))
		{
			//$error_msg[$row]['reason'][] = $Lang['Identity']['Guardian'] . $Lang['StudentRegistry']['Religion'] ." " . $Lang['eEnrolment']['Code'] . $Lang['General']['_Invalid'];
			$d[70] = "9";
		}
		/*
		if(!$lsr->returnPresetCodeName("RELIGION", $d[49]) || !$lsr->returnPresetCodeName("RELIGION", $d[67]))
		{
			//$error_msg[$row]['reason'][] = $Lang['Identity']['Guardian'] . $Lang['StudentRegistry']['Religion'] ." " . $Lang['eEnrolment']['Code'] . $Lang['General']['_Invalid'];
			$d[49] = "9";
		}
		*/
		
		# guardian District
		if(!$lsr->returnPresetCodeName("DISTRICT", $d[60]))
		{
			//$error_msg[$row]['reason'][] = $Lang['Identity']['Guardian'] . $Lang['StudentRegistry']['AddressField']['District'] ." " . $Lang['eEnrolment']['Code'] . $Lang['General']['_Invalid'];
			$d[60] = "X";
		}
		if(!$lsr->returnPresetCodeName("DISTRICT", $d[78]))
		{
			//$error_msg[$row]['reason'][] = $Lang['Identity']['Guardian'] . $Lang['StudentRegistry']['AddressField']['District'] ." " . $Lang['eEnrolment']['Code'] . $Lang['General']['_Invalid'];
			$d[78] = "X";
		}
		/*
		if(!$lsr->returnPresetCodeName("DISTRICT", $d[57]) || !$lsr->returnPresetCodeName("DISTRICT", $d[75]))
		{
			$error_msg[$row]['reason'][] = $Lang['Identity']['Guardian'] . $Lang['StudentRegistry']['AddressField']['District'] ." " . $Lang['eEnrolment']['Code'] . $Lang['General']['_Invalid'];
		}
		*/

		# guardian Area
		/*
		if(($d[58]<0 || $d[58]>sizeof($Lang['StudentRegistry']['AreaOptions'])) || ($d[76]<0 || $d[76]>sizeof($Lang['StudentRegistry']['AreaOptions'])))
		{
			$error_msg[$row]['reason'][] = $Lang['Identity']['Guardian'] . $Lang['StudentRegistry']['AddressField']['Area'] ." " . $Lang['eEnrolment']['Code'] . $Lang['General']['_Invalid'];
		}
		*/
		
		# guardian type code
		if(!($d[44]==1 || $d[44]==2))
		{
			//$error_msg[$row]['reason'][] = $Lang['Identity']['Guardian'] . $Lang['StudentRegistry']['Religion'] ." " . $Lang['eEnrolment']['Code'] . $Lang['General']['_Invalid'];
			$d[44] = "3";
		}
		if(!($d[62]==1 || $d[62]==2))
		{
			//$error_msg[$row]['reason'][] = $Lang['Identity']['Guardian'] . $Lang['StudentRegistry']['Religion'] ." " . $Lang['eEnrolment']['Code'] . $Lang['General']['_Invalid'];
			$d[62] = "3";
		}
		
	
		
		
		# set classname, classnumber and websams data for error array
		if(!empty($error_msg[$row]['reason']))
		{
			$error_msg[$row]['ClassName'] = $thisClassName;
			$error_msg[$row]['ClassNumber'] = $thisClassNumber;
			$error_msg[$row]['WebSAMSRegNo'] = $thisWebSAMSRegNo;
			$error_msg[$row]['EnglishName'] = $d[3];
			$error_msg[$row]['ChineseName'] = $d[6];
		}		
	}
	
	# save to temp table
	$sql = "insert into TEMP_STUDENT_REG_IMPORT (StudentID, ";
	foreach($ori_file_format as $k=>$field)	$sql .= $field. ",";
	$sql .= "InputBy, DateInput) values (".$thisUserID.",";
	//for($i=0;$i<sizeof($d);$i++)		$sql .= "'". addslashes($d[$i]) . "',";
	for($i=0;$i<sizeof($d) - sizeof($custCols);$i++)		$sql .= "'". addslashes($d[$i]) . "',";
	$sql .= $UserID . ", now())";
	$lsr->db_db_query($sql) or die(mysql_error());
	
	$rID = $lsr->db_insert_id();
	
	# save to temp table
	$sql = "insert into TEMP_STUDENT_REG_IMPORT_CUST (RecordID,";
	foreach($custCols as $ccol)	$sql .= $ccol[Code] . ",";
	$sql .= "InputBy, DateInput) values ('$rID',";
	//for($i=0;$i<sizeof($d);$i++)		$sql .= "'". addslashes($d[$i]) . "',";
	for($i=0;$i<sizeof($custCols);$i++)		$sql .= "'". addslashes($d[sizeof($ori_file_format) + $i]) . "',";
	$sql .= $UserID . ", now())";
	$lsr->db_db_query($sql) or die(mysql_error());
}
##### check data [end]

# pass header checking / start import
foreach((array)$error_msg as $row => $errorInfo)
{
	#Build Table Content
	$rowcss = " class='".($ctr++%2==0? "tablebluerow2":"tablebluerow1")."' ";
	$Confirmtable .= "	<tr $rowcss>";
	$Confirmtable .= "		<td class='tabletext' valign='top'>".($row+1)."</td>";
	$Confirmtable .= "		<td class='tabletext' valign='top'>".$errorInfo['ClassName']."</td>";
	$Confirmtable .= "		<td class='tabletext' valign='top'>".$errorInfo['ClassNumber']."</td>";
	$Confirmtable .= "		<td class='tabletext' valign='top'>".$errorInfo['WebSAMSRegNo']."</td>";
	$Confirmtable .= "		<td class='tabletext' valign='top'>".$errorInfo['EnglishName']."</td>";
	$Confirmtable .= "		<td class='tabletext' valign='top'>".$errorInfo['ChineseName']."</td>";
	$Confirmtable .= "		<td class='tabletext' valign='top'>".implode("<br>",$errorInfo['reason'])."</td>";
	$Confirmtable .= "	</tr>";
}

$BackBtn = $linterface->GET_ACTION_BTN($button_back, "button", "goBack()");
$NextBtn = $linterface->GET_ACTION_BTN($button_import, "submit");

$Btn = empty($error_msg)?$NextBtn."&nbsp;":"";
$Btn .= $BackBtn;

$CurrentPageArr['StudentRegistry'] = 1;
$CurrentPage = "Mgmt_RegistryInfo";
$TAGS_OBJ[] = array($Lang['Menu']['AccountMgmt']['StudentRegistryInfo'], "index.php");
$MODULE_OBJ['title'] = $Lang['Header']['Menu']['StudentRegistry'];
$MODULE_OBJ = $lsr->GET_MODULE_OBJ_ARR();
$STEPS_OBJ[] = array($i_general_select_csv_file, 0);
$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 1);
$STEPS_OBJ[] = array($i_general_imported_result, 0);

$linterface->LAYOUT_START();
?>
<script language="javascript">
function goBack() {
	document.frm1.action = "import.php";
	document.frm1.submit();
}
</script>
<form name="frm1" method="POST" action="import_update.php">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
	<tr>
		<td colspan="2" align='right'><?=$linterface->GET_SYS_MSG("",$xmsg);?></td>
	</tr>
	<tr>
		<td colspan="2"><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
	</tr>
	<tr>
		<td clospan="2" align="center">
			<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
			<tr>
				<td colspan="5">
				<table width="30%" border="0">
					<tr>
						<td class='formfieldtitle'><?=$Lang['General']['SuccessfulRecord']?></td>
						<td class='tabletext'><?=count($data)-count($error_msg)?></td>
					</tr>
					<tr>
						<td class='formfieldtitle'><?=$Lang['General']['FailureRecord']?></td>
						<td class='tabletext <?=count($error_msg)?"red":""?>'><?=count($error_msg)?></td>
					</tr>
				</table>
				</td>
			</tr>
			<tr><td colspan="2">&nbsp;</td></tr>
			<?
				if(!empty($error_msg)){
			?>
			<tr>
				<td class='tablebluetop tabletopnolink' width="1%">Row#</td>
				<td class='tablebluetop tabletopnolink'><?=$i_ClassName?></td>
				<td class='tablebluetop tabletopnolink'><?=$i_ClassNumber?></td>
				<td class='tablebluetop tabletopnolink'><?=$ec_iPortfolio['WebSAMSRegNo']?></td>
				<td class='tablebluetop tabletopnolink'><?=$Lang['General']['EnglishName']?></td>
				<td class='tablebluetop tabletopnolink'><?=$Lang['General']['ChineseName']?></td>
				<td class='tablebluetop tabletopnolink' width="50%"><?=$Lang['General']['Remark']?></td>
			</tr>
			<?=$Confirmtable?>
			<?}?>
			</table>
		</td>
	</tr>
	<tr>
		<td>
		<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
            <tr>
            	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
            </tr>
            <tr>
				<td align="center">
				<?=$Btn?>
				</td>
			</tr>
        </table> 
		</td>
	</tr>
</table>
</form>
<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>
