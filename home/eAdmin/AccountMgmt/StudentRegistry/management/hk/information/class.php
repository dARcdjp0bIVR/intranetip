<?php
#using :

############# Change Log [Start]
#
#   Date:   2019-08-15 Cameron
#           - change confirm submitted to based on academic year [case #Y164458]
#
#   Date:   2019-05-15 Cameron
#           - hide import button for Kentville
#           - fix: don't apply IntegerSafe to targetClass as it contains string
#
#   Date:   2019-05-09 Cameron
#           - add import button for Kentville
#           - fix security issue
#
#   Date:   2019-04-03 Cameron
#           - add checking of DATA_CONFIRM_DATE against CurrentStudentStartDate and CurrentStudentEndDate to determine parent has submitted or not for Kentville
#
#	Date:	2019-03-25 Cameron
#			- customize function for Kentville student data records [Y140316]
#           - add label id to checkbox for easy selection
#
#	Date:	2017-06-07 Cameron
#			- add filter: IncludeLeft student (alumni & left student, all in INTRANET_USER) or not
#
#	Date:	2015-07-24 Cameron
#			- filter out alumni (RecordType=4) and graduated student (RecordStatus=3)
#
#	Date:	2015-07-22 Cameron
#			- fix bug on change academic year, there's no RegistryStatus, use submit_status instead
#			- get value of AcademicYearID and targetClass from $_POST rather than $_GET
#
#	Date:	2015-07-10 Cameron
#			add filter of LastUpdatedDate			
#		
#	Date:	2013-08-27	YatWoon
#			add export for websams
#
#	Date:	2013-08-16	YatWoon
#			add "submit status" filter (for UCCKE)
#
#	Date:	2013-06-25	YatWoon
#			add import link
#	Date:	2013-06-04	YatWoon
#			add Print link
#
############# Change Log [End]

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccessright.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/json.php");
// include_once($PATH_WRT_ROOT."includes/libportfolio.php");

intranet_auth();
intranet_opendb();

$laccessright = new libaccessright();

if(!$plugin['AccountMgmt_StudentRegistry'] || (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-View") && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-Manage"))) {
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$lsr = new libstudentregistry();
$linterface = new interface_html();
// $lpf = new libportfolio();

$searchText = stripslashes(htmlspecialchars(cleanHtmlJavascript($searchText)));

$AcademicYearID = isset($_POST['AcademicYearID']) ? $_POST['AcademicYearID'] : $_GET['AcademicYearID'];
$AcademicYearID = IntegerSafe($AcademicYearID);
$targetClass = isset($_POST['targetClass']) ? $_POST['targetClass'] : $_GET['targetClass'];
 
# class menu
$classMenu = $lsr->getSelectClassWithWholeForm("name=\"targetClass\" id=\"targetClass\" onChange=\"document.form1.submit()\"", $targetClass, $i_Discipline_System_Award_Punishment_All_Classes, $i_general_please_select, $AcademicYearID);
$studentAry = $lsr->storeStudent('0', $targetClass, $AcademicYearID, $IncludeLeft);

# Button
$editBtn = $linterface->GET_LNK_EDIT("javascript:checkEdit(document.form1,'studentID[]','view.php')", "", "", "", "", 0);

/*
# tag selection
$tagAry = returnModuleAvailableTag($lsr->Module, $assocAry=1);
$temp = array();
$i = 0;
foreach($tagAry as $id=>$name) {
	$temp[$i][] = $id;
	$temp[$i][] = $name;
	$i++;
}
$tagSelection = getSelectByArray($temp, ' name="tagID" id="tagID" onChange="document.form1.submit()"', $tagID, 0, 0, $Lang['StudentRegistry']['AllTags']);
*/

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

if($order == "") $order = 1;
if($field == "") $field = 0;
$pageSizeChangeEnabled = true;

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;

$li = new libdbtable2007($field, $order, $pageNo);
if($sys_custom['OnlineRegistry']) 
{
	# submit status filter
	$submit_status_filter = "<select name='submit_status' id='submit_status' onChange='document.form1.submit();'>";
	$submit_status_filter .= "<option value='0'>". $Lang['StudentRegistry']['Submitted'] ." / " . $Lang['StudentRegistry']['NonSubmitted'] ."</option>";
	$submit_status_filter .= "<option value='1' ". ($submit_status==1 ? "selected":"") .">". $Lang['StudentRegistry']['Submitted'] ."</option>";
	$submit_status_filter .= "<option value='-1' ". ($submit_status==-1 ? "selected":"") .">". $Lang['StudentRegistry']['NonSubmitted'] ."</option>";
	$submit_status_filter .= "</select>";
}

# year filter
$years = GetAllAcademicYearInfo();
for($i=0; $i<sizeof($years); $i++) {
	list($yearid, $yearNameEN, $yearNameB5) = $years[$i];
	$yearAry[$i][] = $yearid;
	$yearAry[$i][] = Get_Lang_Selection($yearNameB5, $yearNameEN);	
}
$yearSelectionMenu = getSelectByArray($yearAry, 'name="AcademicYearID" id="AcademicYearID" onChange="changeYear()"', $AcademicYearID);

# last updated date filter

$EndDate = isset($EndDate) && $EndDate!='' ? $EndDate : '';		// default empty date
$StartDate = isset($StartDate) && $StartDate != '' ? $StartDate :'';
$StartDateInput = $linterface->GET_DATE_PICKER("StartDate",$StartDate,$OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="",$ID="",$SkipIncludeJS=0, $CanEmptyField=1, $Disable=false, $cssClass="textboxnum");
$EndDateInput = $linterface->GET_DATE_PICKER("EndDate",$EndDate,$OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="",$ID="",$SkipIncludeJS=0, $CanEmptyField=1, $Disable=false, $cssClass="textboxnum");
if (isset($IncludeEverUpdatedOnly) && ($IncludeEverUpdatedOnly !='')) {
	$includeEverUpdatedOnly = 'checked';
} 
else {
	$includeEverUpdatedOnly = '';
}

# Search bar
$searchbar .= '<div class="selectbox_group selectbox_group_filter" style="margin-top:0px;">';
	$searchbar .= '<a href="javascript:void(0);" style="height: 17px;" onclick="if(document.getElementById(\'search_option\').style.visibility==\'hidden\'){MM_showHideLayers(\'search_option\',\'\',\'show\');}else{MM_showHideLayers(\'search_option\',\'\',\'hide\');}">'.$Lang['StudentRegistry']['LastUpdatedDate'].'</a>';
$searchbar .= '</div>';
$searchbar .= '<p class="spacer"></p>';
$searchbar .= '<div id="search_option" class="selectbox_layer selectbox_group_layer" style="width:540px;visibility:hidden;">';
$searchbar .= '<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td>
					<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
					<tr>
						<td valign="top" colspan="2">
							<input type="checkbox" name="IncludeEverUpdatedOnly" id="IncludeEverUpdatedOnly" value="checked" '.$includeEverUpdatedOnly.'/><label for="IncludeEverUpdatedOnly">'.$Lang['StudentRegistry']['IncludeEverUpdatedOnly'].'</label>
                        </td>			
					</tr>
					<tr>
						<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">'.$Lang['StudentRegistry']['LastUpdatedDate'].'</td>
						<td valign="top" class="tabletext">
						'.$Lang['General']['From'].'&nbsp;'.$StartDateInput.$Lang['General']['To'].'&nbsp;'.$EndDateInput.'
						</td>
					</tr>
					</table>
					</td>
				</tr>
				</table>';


	$searchbar .= '<p class="spacer"></p>';
	$searchbar .= '<div class="edit_bottom">';
	$searchbar .= '<input type="submit" onmouseout="this.className=\'formsmallbutton\'" onmouseover="this.className=\'formsmallbuttonon\'" value="'.$Lang['Btn']['Search'].'" onclick="MM_showHideLayers(\'search_option\',\'\',\'hide\');" class="formsmallbutton ">&nbsp;';
	$searchbar .= '<input type="button" onmouseout="this.className=\'formsmallbutton\'" onmouseover="this.className=\'formsmallbuttonon\'" value="'.$Lang['Btn']['Cancel'].'" onclick="MM_showHideLayers(\'search_option\',\'\',\'hide\');" class="formsmallbutton ">';
	$searchbar .= '</div>';
$searchbar .= '</div>';

#incude left student ?
$includLeft = '<div><input type="checkbox" name="IncludeLeft" id="IncludeLeft" value="1"'.($IncludeLeft?" checked":"").'><label for="IncludeLeft">'.$Lang['StudentRegistry']['IncludeLeftStudent'].'</label></div>';

# conditions
$conds = "";

if($targetClass!="") {
	if(sizeof($studentAry)>0)
		$conds .= " AND ycu.UserID IN (".implode(',', $studentAry).")";
	else {
		if(is_numeric($targetClass)) 
			$conds .= " AND y.YearID=".$targetClass;	
		else
			$conds .= " AND yc.YearClassID=".str_replace("::","",$targetClass);	  
	}		
}

//if($registryStatus!="")
//	$conds .= " AND USR.RecordStatus='$registryStatus'";
/*
else 
	$conds .= " AND USR.RecordStatus IN (0,1,2)";
*/

if($tagID!='')
	$conds .= " AND CONCAT(',',srs.TAGID,',') LIKE '%,$tagID,%'";

if($searchText!="") {
	$conds .= " AND (
					".Get_Lang_Selection('yc.ClassTitleB5','yc.ClassTitleEN')." LIKE '%$searchText%' OR 
					USR.EnglishName LIKE '%$searchText%' OR 
					USR.ChineseName LIKE '%$searchText%' OR 
					srs.ID_NO LIKE '%$searchText%' OR 
					srs.EMAIL LIKE '%$searchText%' OR
					USR.HomeTelNo LIKE '%$searchText%'
				)";
}

if($submit_status==1)
{
	$conds .= " and srs2.DATA_CONFIRM_DATE is not null";
}

if($submit_status==-1)
{
    $conds .= " and srs2.DATA_CONFIRM_DATE is null";
}

if ($IncludeEverUpdatedOnly){
	$conds .= " and srs.DateModified IS NOT NULL";
}
if ($StartDate != '') {
	if ($IncludeEverUpdatedOnly){
		$conds .= " and srs.DateModified >='".$StartDate." 00:00:00'";
	}
	else {
		$conds .= " and (srs.DateModified >='".$StartDate." 00:00:00' or srs.DateModified IS NULL)";
	}
}
if ($EndDate != '') {
	if ($IncludeEverUpdatedOnly){
		$conds .= " and srs.DateModified <='".$EndDate." 23:59:59'";
	}
	else {
		$conds .= " and (srs.DateModified <='".$EndDate." 23:59:59' or srs.DateModified IS NULL)";
	}
}

$recordStatusCond = ($IncludeLeft == '1') ? ",3" : "";

//$name_field = getNameFieldByLang("USR.");

if ($IncludeLeft == '1') {	
	$sql = "SELECT ".
				Get_Lang_Selection('yc.ClassTitleB5','yc.ClassTitleEN')." as clsName,
				ycu.ClassNumber as clsNo,
				CASE
					WHEN USR.RecordStatus=3 OR USR.RecordType=4 THEN CONCAT('<font style=\"color:red;\">*</font>',IF(USR.EnglishName='','---',IFNULL(USR.EnglishName,'---')))
					ELSE IF(USR.EnglishName='','---',IFNULL(USR.EnglishName,'---'))
				END as engname,
				CASE
					WHEN USR.RecordStatus=3 OR USR.RecordType=4 THEN CONCAT('<font style=\"color:red;\">*</font>',IF(USR.ChineseName='','---',IFNULL(USR.ChineseName,'---')))
					ELSE IF(USR.ChineseName='','---',IFNULL(USR.ChineseName,'---'))
				END as chiname,
				IFNULL(srs.DateModified,'---') as lastUpdated,
				CONCAT('<input type=\'checkbox\' name=\'studentID[]\' id=\'studentID[]\' value=\'',USR.UserID,'\'>') as checkbox,
				USR.UserID,
				srs.ModifyBy,
				yc.AcademicYearID
			FROM
				INTRANET_USER USR LEFT OUTER JOIN
				YEAR_CLASS_USER ycu ON (ycu.UserID=USR.UserID) LEFT OUTER JOIN
				YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID) LEFT OUTER JOIN
				YEAR y ON (y.YearID=yc.YearID) LEFT OUTER JOIN
				STUDENT_REGISTRY_STUDENT srs ON (srs.UserID=USR.UserID) LEFT JOIN 
				{$eclass_db}.PORTFOLIO_STUDENT AS ps ON USR.UserID = ps.UserID LEFT JOIN
				STUDENT_REGISTRY_SUBMITTED srs2 ON (srs2.UserID=USR.UserID AND srs2.AcademicYearID='$AcademicYearID')
			WHERE
				USR.RecordType IN (2,4) AND
				USR.RecordStatus IN (0,1,2,3) AND 
				yc.AcademicYearID='$AcademicYearID'
				$conds
			";
}
else {
	$sql = "SELECT ".
				Get_Lang_Selection('yc.ClassTitleB5','yc.ClassTitleEN')." as clsName,
				ycu.ClassNumber as clsNo,
				IF(USR.EnglishName='','---',IFNULL(USR.EnglishName,'---')) as engname,
				IF(USR.ChineseName='','---',IFNULL(USR.ChineseName,'---')) as chiname,
				IFNULL(srs.DateModified,'---') as lastUpdated,
				CONCAT('<input type=\'checkbox\' name=\'studentID[]\' id=\'studentID[]\' value=\'',USR.UserID,'\'>') as checkbox,
				USR.UserID,
				srs.ModifyBy,
				yc.AcademicYearID
			FROM
				INTRANET_USER USR LEFT OUTER JOIN
				YEAR_CLASS_USER ycu ON (ycu.UserID=USR.UserID) LEFT OUTER JOIN
				YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID) LEFT OUTER JOIN
				YEAR y ON (y.YearID=yc.YearID) LEFT OUTER JOIN
				STUDENT_REGISTRY_STUDENT srs ON (srs.UserID=USR.UserID) LEFT JOIN 
				{$eclass_db}.PORTFOLIO_STUDENT AS ps ON USR.UserID = ps.UserID LEFT JOIN
				STUDENT_REGISTRY_SUBMITTED srs2 ON (srs2.UserID=USR.UserID AND srs2.AcademicYearID='$AcademicYearID')
			WHERE
				USR.RecordType=2 AND
				USR.RecordStatus IN (0,1,2) AND 
				yc.AcademicYearID='$AcademicYearID'
				$conds
			";
}
//debug_r( $sql);
$li->sql = $sql;
$li->field_array = array("clsName", "clsNo", "engname", "chiname");
$li->field_array[] = "lastUpdated";
$li->no_col = sizeof($li->field_array)+2;
$li->fieldorder2 = ", y.Sequence, yc.Sequence, ycu.ClassNumber";
$li->IsColOff = "StudentRegistry_StudentList_HK";

$pos = 0;
$li->column_list .= "<th width='1'>#</th>\n";
$li->column_list .= "<th width='10%' >".$li->column_IP25($pos++, $Lang['StudentRegistry']['Class'])."</th>\n";
$li->column_list .= "<th width='10%' >".$li->column_IP25($pos++, $Lang['StudentRegistry']['ClassNumber'])."</th>\n";
$li->column_list .= "<th width='25%' >".$li->column_IP25($pos++, $Lang['StudentRegistry']['EnglishName'])."</th>\n";
$li->column_list .= "<th width='25%' >".$li->column_IP25($pos++, $Lang['StudentRegistry']['ChineseName'])."</th>\n";
$li->column_list .= "<th width='20%' >".$li->column_IP25($pos++, $Lang['StudentRegistry']['LastUpdated'])."</th>\n";
$li->column_list .= "<th width='1'>".$li->check("studentID[]")."</td>\n";

$CurrentPageArr['StudentRegistry'] = 1;
$CurrentPage = "Mgmt_RegistryInfo";

$TAGS_OBJ[] = array($Lang['Menu']['AccountMgmt']['StudentRegistryInfo'], "index.php");
$MODULE_OBJ['title'] = $Lang['Header']['Menu']['StudentRegistry'];
$MODULE_OBJ = $lsr->GET_MODULE_OBJ_ARR();

# navigation bar
$PAGE_NAVIGATION[] = array($Lang['StudentRegistry']['ClassList'], "index.php?AcademicYearID=".$AcademicYearID);
$PAGE_NAVIGATION[] = array($Lang['StudentRegistry']['StudentList'], "");

$linterface->LAYOUT_START();
?>
<style type="text/css">
html .jqueryslidemenu{height: 1%;} /*Holly Hack for IE7 and below*/
</style>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/<?=$LAYOUT_SKIN?>/js/form_class_management.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/thickbox.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.blockUI.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.autocomplete.js"></script>

<link rel="stylesheet" href="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.autocomplete.css" type="text/css" />
<link rel="stylesheet" href="<?=$PATH_WRT_ROOT?>templates/jquery/thickbox.css" type="text/css" media="screen" />

<script language="javascript">
function displaySpan(layerName) {
	if(document.getElementById('exportFlag').value==0) {
		document.getElementById(layerName).style.visibility = 'visible';
		document.getElementById('exportFlag').value = 1;
	} else {
		document.getElementById(layerName).style.visibility = 'hidden';
		document.getElementById('exportFlag').value = 0;
	}
}

function Check_Go_Search(evt)
{
	var key = evt.which || evt.charCode || evt.keyCode;
	
	if (key == 13) // enter
		document.form1.submit();
	else
		return false;
}

function goExport(type)
{
	if (type == 4 || type == 5) {
		eval("document.form1.action='export_kentville_csv.php?type=" + type + "';");
	}
	else if (type == 6) {
		document.form1.action='export_kentville_pdf.php';
	}
	else {
		eval("document.form1.action='export.php?type=" + type + "';");
	}
	document.form1.submit();	
	document.form1.action="";
}

function changeYear() {
	document.getElementById('searchText').value = '';
	document.getElementById('targetClass').selectedIndex = 0;
	document.getElementById('submit_status').selectedIndex = 0;
	document.form1.submit();
}

function goPrint()
{
	document.form1.target="_blank";
	document.form1.action="print.php";
	document.form1.submit();
	document.form1.target="";	
	document.form1.action="";
}

function checkDate() {
	var from_date_obj = document.getElementById('StartDate');
	var to_date_obj = document.getElementById('EndDate');
	var is_valid = true;
	
	if (from_date_obj.value != '') {
		if(!check_date_without_return_msg(from_date_obj)){
			is_valid = false;
		}
	}
	if (to_date_obj.value != '') {
		if(!check_date_without_return_msg(to_date_obj)){
			is_valid = false;
		}
	}
		
	if ((is_valid) && (from_date_obj.value != '') && (to_date_obj.value != '')) {
		if(from_date_obj.value > to_date_obj.value){
			is_valid = false;
			alert('<?=$Lang['General']['JS_warning']['InvalidDateRange']?>');
		}
	}
	
	return is_valid;	
}

$(document).ready(function(){
	$('#IncludeLeft').click(function(){
		document.form1.submit();
	});
});

</script>
<form name="form1" method="post" action="" onsubmit="return checkDate()">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr> 
		<td>
			<div class="navigation">
    			<?=$linterface->GET_NAVIGATION($PAGE_NAVIGATION)?>
			</div>
			<div class="Conntent_tool">
		<?php if ($sys_custom['StudentRegistry']['Kentville']):?>
				<!--<a href="import_kentville.php" class="import"> <?=$button_import?></a>-->				
				<a href="javascript: goExport(4);" class="export"> <?=$Lang['StudentRegistry']['BtnExportCsv']?></a>
				<a href="javascript: goExport(5);" class="export"> <?=$Lang['StudentRegistry']['ExportForImport']?></a>
				<a href="javascript: goExport(6);" class="export"> <?=$Lang['StudentRegistry']['BtnExportPdf']?></a>
		<?php else:?>			
				<a href="javascript: goPrint();" class="print"> <?=$button_print?></a>
				<a href="import.php" class="import"> <?=$button_import?></a>
				<a href="javascript: goExport(1);" class="export"> <?=$button_export?></a>
				<a href="javascript: goExport(2);" class="export"> <?=$Lang['StudentRegistry']['ExportForImport']?></a>
				<a href="javascript: goExport(3);" class="export"> <?=$Lang['StudentRegistry']['ExportForWebSAMS']?></a>
		<?php endif;?>				
			</div>
			
			<div class="Conntent_search">
				<input name="searchText" id="searchText" type="text" value="<?=$searchText?>" onKeyUp="Check_Go_Search(event)"/>
			</div>
			<div class="table_board">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr class="table-action-bar">
						<td valign="bottom">
							<div class="table_filter"><?=$yearSelectionMenu?> &nbsp;</div>
							<div class="table_filter"><?=$classMenu?> &nbsp;</div>
							<div class="table_filter"><?=$submit_status_filter?>&nbsp;</div>
							<div class="table_filter"><?=$searchbar?></div>
							<div class="table_filter"><?=$includLeft?></div>							
							<br  style="clear:both"/>
						</td>
						<td valign="bottom"><div class="common_table_tool"> <a title="<?= $Lang['Btn']['Edit'] ?>" class="tool_edit" href="javascript:void(0);" onclick="javascript:document.form1.EditRecord.value=1;checkEdit(document.form1,'studentID[]','view.php')" /><?= $Lang['Btn']['Edit'] ?></div></td>
					</tr>
				</table>

				<!--<div id="ClassListLayer">-->
				<?=$li->display()?>
				<!--</div>-->
			</div>
			<div><?=$Lang['StudentRegistry']['IncludeLeftStudentRemark']?></div>
			<p>&nbsp;</p>
		</td>
	</tr>
</table>

<input type="hidden" name="EditRecord" id="EditRecord" value="0">
<input type="hidden" name="exportFlag" id="exportFlag" value="0">
<input type="hidden" name="pageNo" id="pageNo" value="<?=$li->pageNo; ?>" />
<input type="hidden" name="order" id="order" value="<?=$li->order; ?>" />
<input type="hidden" name="field" id="field" value="<?=$li->field; ?>" />
<input type="hidden" name="page_size_change" id="page_size_change" value="" />
<input type="hidden" name="numPerPage" id="numPerPage" value="<?=$li->page_size?>" />
<!--<input type="hidden" name="targetClass" id="targetClass" value="<?=$targetClass?>">-->
</form>
<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>