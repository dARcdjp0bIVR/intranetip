<?
#using: yat

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccessright.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry.php");

intranet_auth();
intranet_opendb();

$laccessright = new libaccessright();

if(!$plugin['AccountMgmt_StudentRegistry'] || (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-View") && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-Manage")) && !$_SESSION['ParentFillOnlineReg']) 
{
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$lsr = new libstudentregistry();

$thisAry = array();

/*
$thisAry['NAME_C'] 	= intranet_htmlspecialchars(stripslashes(trim($ChineseName)));
$thisAry['NAME_E'] 	= intranet_htmlspecialchars(stripslashes(trim($EnglishName)));
$thisAry['SEX'] 	= intranet_htmlspecialchars(trim($Gender));
$thisAry['B_DATE'] 	= intranet_htmlspecialchars(trim($BirthDate));
$thisAry['TEL'] 	= intranet_htmlspecialchars(trim($HomePhone));

$thisAry['UserID'] 		= $userID;
$thisAry['STUD_ID'] 	= intranet_htmlspecialchars(trim($StudentID));
$thisAry['CODE'] 		= intranet_htmlspecialchars(trim($DSEJ_Number));
$thisAry['ENTRY_DATE']	= intranet_htmlspecialchars(trim($EntryDate));
$thisAry['S_CODE'] 		= intranet_htmlspecialchars(trim($SchoolCode));
$thisAry['B_PLACE'] 	= intranet_htmlspecialchars(trim($BirthPlace));
$thisAry['ID_TYPE'] 	= intranet_htmlspecialchars(trim($IdType));
$thisAry['ID_NO'] 		= intranet_htmlspecialchars(trim($IdCardNumber));
$thisAry['I_PLACE'] 	= intranet_htmlspecialchars(trim($IdPlace));
$thisAry['I_DATE'] 		= intranet_htmlspecialchars(trim($IdIssueDate));
$thisAry['V_DATE'] 		= intranet_htmlspecialchars(trim($IdValidDate));
$thisAry['S6_TYPE'] 	= intranet_htmlspecialchars(trim($StayType));
$thisAry['S6_IDATE'] 	= intranet_htmlspecialchars(trim($StayIssueDate));
$thisAry['S6_VDATE'] 	= intranet_htmlspecialchars(trim($StayValidDate));
$thisAry['NATION'] 		= intranet_htmlspecialchars(trim($Nationality));
$thisAry['ORIGIN'] 		= intranet_htmlspecialchars(stripslashes(trim($Province)));
$thisAry['RELIGION'] 	= intranet_htmlspecialchars(stripslashes(trim($Religion)));
$thisAry['R_AREA'] 		= intranet_htmlspecialchars(trim($ResArea));
$thisAry['RA_DESC'] 	= intranet_htmlspecialchars(stripslashes(trim($DistrictName)));
$thisAry['AREA'] 		= intranet_htmlspecialchars(trim($AddArea));
$thisAry['ROAD'] 		= intranet_htmlspecialchars(stripslashes(trim($Street)));
$thisAry['ADDRESS'] 	= intranet_htmlspecialchars(stripslashes(trim($AddDetail)));
$thisAry['EMAIL'] 		= intranet_htmlspecialchars(trim($Email));

$thisAry['FATHER_C'] 	= intranet_htmlspecialchars(stripslashes(trim($FatherChiName)));
$thisAry['FATHER_E'] 	= intranet_htmlspecialchars(stripslashes(trim($FatherEngName)));
$thisAry['F_PROF'] 		= intranet_htmlspecialchars(stripslashes(trim($FatherJobName)));
$thisAry['F_MARTIAL'] 	= intranet_htmlspecialchars(trim($FatherMarry));
$thisAry['F_TEL'] 		= intranet_htmlspecialchars(trim($FatherHomePhone));
$thisAry['F_MOBILE'] 	= intranet_htmlspecialchars(trim($FatherCellPhone));
$thisAry['F_OFFICE'] 	= intranet_htmlspecialchars(trim($FatherOfficePhone));
$thisAry['F_EMAIL'] 	= intranet_htmlspecialchars(trim($FatherEmail));

$thisAry['MOTHER_C'] 	= intranet_htmlspecialchars(stripslashes(trim($MotherChiName)));
$thisAry['MOTHER_E'] 	= intranet_htmlspecialchars(stripslashes(trim($MotherEngName)));
$thisAry['M_PROF'] 		= intranet_htmlspecialchars(stripslashes(trim($MotherJobName)));
$thisAry['M_MARTIAL'] 	= intranet_htmlspecialchars(trim($MotherMarry));
$thisAry['M_TEL'] 		= intranet_htmlspecialchars(trim($MotherHomePhone));
$thisAry['M_MOBILE'] 	= intranet_htmlspecialchars(trim($MotherCellPhone));
$thisAry['M_OFFICE'] 	= intranet_htmlspecialchars(trim($MotherOfficePhone));
$thisAry['M_EMAIL'] 	= intranet_htmlspecialchars(trim($MotherEmail));

if($GuardRelation=="F") { 			# copy from father
	$thisAry['GUARDIAN_C'] 	= $thisAry['FATHER_C'];
	$thisAry['GUARDIAN_E'] 	= $thisAry['FATHER_E'];
	$thisAry['G_SEX'] 		= "M";
	$thisAry['G_PROF'] 		= $thisAry['F_PROF'];
	$thisAry['GUARD'] 		= "F";
	$thisAry['G_REL_OTHERS']= "";
	$thisAry['LIVE_SAME'] 	= "1";
	$thisAry['G_AREA'] 		= $thisAry['AREA'];
	$thisAry['G_ROAD'] 		= $thisAry['ROAD'];
	$thisAry['G_ADDRESS'] 	= $thisAry['ADDRESS'];
	$thisAry['G_TEL'] 		= $thisAry['F_TEL'];
	$thisAry['G_MOBILE'] 	= $thisAry['F_MOBILE'];
	$thisAry['G_OFFICE'] 	= $thisAry['F_OFFICE'];
	$thisAry['G_EMAIL'] 	= $thisAry['F_EMAIL'];
} else if($GuardRelation=="M") {	# copy from father
	$thisAry['GUARDIAN_C'] 	= $thisAry['MOTHER_C'];
	$thisAry['GUARDIAN_E'] 	= $thisAry['MOTHER_E'];
	$thisAry['G_SEX'] 		= "F";
	$thisAry['G_PROF'] 		= $thisAry['M_PROF'];
	$thisAry['GUARD'] 		= "M";
	$thisAry['G_REL_OTHERS']= "";
	$thisAry['LIVE_SAME'] 	= "1";
	$thisAry['G_AREA'] 		= $thisAry['AREA'];
	$thisAry['G_ROAD'] 		= $thisAry['ROAD'];
	$thisAry['G_ADDRESS']	= $thisAry['ADDRESS'];
	$thisAry['G_TEL'] 		= $thisAry['M_TEL'];
	$thisAry['G_MOBILE'] 	= $thisAry['M_MOBILE'];
	$thisAry['G_OFFICE'] 	= $thisAry['M_OFFICE'];
	$thisAry['G_EMAIL'] 	= $thisAry['M_EMAIL'];
} else {							# self input info
	$thisAry['GUARDIAN_C'] 	= intranet_htmlspecialchars(trim($GuardChiName));
	$thisAry['GUARDIAN_E'] 	= intranet_htmlspecialchars(trim($GuardEngName));
	$thisAry['G_SEX'] 		= intranet_htmlspecialchars(trim($GuardGender));
	$thisAry['G_PROF'] 		= intranet_htmlspecialchars(trim($GuardJobName));
	$thisAry['GUARD'] 		= intranet_htmlspecialchars(trim($GuardRelation));
	$thisAry['G_REL_OTHERS']= intranet_htmlspecialchars(trim($GuardRelationOther));
	$thisAry['LIVE_SAME'] 	= intranet_htmlspecialchars(trim($GuardStay));
	$thisAry['G_AREA'] 		= intranet_htmlspecialchars(trim($GuardArea));
	$thisAry['G_ROAD'] 		= intranet_htmlspecialchars(trim($GuardStreet));
	$thisAry['G_ADDRESS'] 	= intranet_htmlspecialchars(trim($GuardAddDetail));
	$thisAry['G_TEL'] 		= intranet_htmlspecialchars(trim($GuardHomePhone));
	$thisAry['G_MOBILE'] 	= intranet_htmlspecialchars(trim($GuardCellPhone));
	$thisAry['G_OFFICE'] 	= intranet_htmlspecialchars(trim($GuardOfficePhone));
	$thisAry['G_EMAIL'] 	= intranet_htmlspecialchars(trim($GuardEmail));
}

if($EmergencyRelation=="F") {
	$thisAry['EC_NAME_C'] 	= $thisAry['FATHER_C'];
	$thisAry['EC_NAME_E'] 	= $thisAry['FATHER_E'];
	$thisAry['EC_REL'] 		= intranet_htmlspecialchars(trim($EmergencyRelation));
	$thisAry['EC_TEL'] 		= $thisAry['F_TEL'];
	$thisAry['EC_MOBILE'] 	= $thisAry['F_MOBILE'];
	$thisAry['EC_AREA'] 	= $thisAry['AREA'];
	$thisAry['EC_ROAD'] 	= $thisAry['ROAD'];
	$thisAry['EC_ADDRESS'] 	= $thisAry['ADDRESS'];
} else if($EmergencyRelation=="M") {
	$thisAry['EC_NAME_C'] 	= $thisAry['MOTHER_C'];
	$thisAry['EC_NAME_E'] 	= $thisAry['MOTHER_E'];
	$thisAry['EC_REL'] 		= intranet_htmlspecialchars(trim($EmergencyRelation));
	$thisAry['EC_TEL'] 		= $thisAry['M_TEL'];
	$thisAry['EC_MOBILE'] 	= $thisAry['M_MOBILE'];
	$thisAry['EC_AREA'] 	= $thisAry['AREA'];
	$thisAry['EC_ROAD'] 	= $thisAry['ROAD'];
	$thisAry['EC_ADDRESS'] 	= $thisAry['ADDRESS'];
} else {
	$thisAry['EC_NAME_C'] 	= intranet_htmlspecialchars(trim($EmergencyChiName));
	$thisAry['EC_NAME_E']	= intranet_htmlspecialchars(trim($EmergencyEngName));
	$thisAry['EC_REL'] 		= intranet_htmlspecialchars(trim($EmergencyRelation));
	$thisAry['EC_TEL'] 		= intranet_htmlspecialchars(trim($EmergencyHomePhone));
	$thisAry['EC_MOBILE'] 	= intranet_htmlspecialchars(trim($EmergencyCellPhone));
	$thisAry['EC_AREA'] 	= intranet_htmlspecialchars(trim($EmergencyArea));
	$thisAry['EC_ROAD'] 	= intranet_htmlspecialchars(trim($EmergencyStreet));
	$thisAry['EC_ADDRESS']	= intranet_htmlspecialchars(trim($EmergencyAddDetail));	
}


$error = array();
$error = $lsr->checkData_Macau('edit', $thisAry, $Mode);
$err_msg = "";
*/

/*
if(sizeof($error['StudentInfo'])>0) {
	$err_msg .= "<font color='red'><ul>";
	foreach($error['StudentInfo'] as $key=>$val) {
		$err_msg .= "<li>".$Lang['StudentRegistry']['StudInfo']." - $val</li>";
	}
   	$err_msg .= "</ul></font>";
}

if(sizeof($error['FatherInfo'])>0) {
	$err_msg .= "<font color='red'><ul>";
	foreach($error['FatherInfo'] as $key=>$val) {
		$err_msg .= "<li>".$Lang['StudentRegistry']['FatherInfo']." - $val</li>";
	}
   	$err_msg .= "</ul></font>";
}

if(sizeof($error['MotherInfo'])>0) {
	$err_msg .= "<font color='red'><ul>";
	foreach($error['MotherInfo'] as $key=>$val) {
		$err_msg .= "<li>".$Lang['StudentRegistry']['MotherInfo']." - $val</li>";
	}
   	$err_msg .= "</ul></font>";
}

if(sizeof($error['GuardianInfo'])>0) {
	$err_msg .= "<font color='red'><ul>";
	foreach($error['GuardianInfo'] as $key=>$val) {
		$err_msg .= "<li>".$Lang['StudentRegistry']['GuardianInfo']." - $val</li>";
	}
   	$err_msg .= "</ul></font>";
}
*/

########################################
# Compulsory fields:
#	Student	-	English Name, Chinese Name, UserEmail
########################################
if(trim($english_name)=="")
	$err_msg .= "<li>".$Lang['StudentRegistry']['StudInfo']." - ". $Lang['StudentRegistry']['EnglishName'] . " - " . $Lang['StudentRegistry']['IsMissing'] ."</li>";
if(trim($chinese_name)=="")
	$err_msg .= "<li>".$Lang['StudentRegistry']['StudInfo']." - ". $Lang['StudentRegistry']['ChineseName'] . " - " . $Lang['StudentRegistry']['IsMissing'] ."</li>";
if(trim($student_email)=="")
	$err_msg .= "<li>".$Lang['StudentRegistry']['StudInfo']." - ". $Lang['StudentRegistry']['EMailAddress'] . " - " . $Lang['StudentRegistry']['IsMissing'] ."</li>";

	
########################################
# Unique
#	Student	-	HKID, PassportNo, DocumentIDNo, UserEmail
#	Guardian - HKID, Email xxxxxx
########################################
if(trim($hkid)!="")
{
	$sql = "select count(UserID) from INTRANET_USER where HKID='$hkid' and UserID!=$studentID";
	$result = $lsr->returnVector($sql);
	if($result[0]>0)
		$err_msg .= "<li>".$Lang['StudentRegistry']['StudInfo']." - ". $i_HKID . " - " . $Lang['StudentRegistry']['alreadyExist'] ."</li>";
}
if(trim($student_email)!="")
{
	$sql = "select count(UserID) from INTRANET_USER where UserEmail='$student_email' and UserID!=$studentID";
	$result = $lsr->returnVector($sql);
	if($result[0]>0)
		$err_msg .= "<li>".$Lang['StudentRegistry']['StudInfo']." - ". $Lang['StudentRegistry']['EMailAddress'] . " - " . $Lang['StudentRegistry']['alreadyExist'] ."</li>";
}

	
########################################
# Check format
#	Student	-	UserEmail
#	Guardian - Email  xxxxxx
########################################
if(!intranet_validateEmail($student_email))
	$err_msg .= "<li>".$Lang['StudentRegistry']['StudInfo']." - ". $Lang['StudentRegistry']['EMailAddress'] . " - " . $Lang['StudentRegistry']['IsInvalid'] ."</li>";



echo $err_msg;
exit;

if(sizeof($error)>0) {
	$err_msg .= "<font color='red'><ul>";
	foreach($error as $key=>$val) {
		$err_msg .= "<li>$val</li>";
	}
   	$err_msg .= "</ul></font>";
}

echo $err_msg;

intranet_closedb();
?>