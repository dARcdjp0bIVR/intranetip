<?php
#using: yat

############# Change Log
#
#
#################


$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccessright.php");
// include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
// include_once($PATH_WRT_ROOT."includes/libdbtable.php");
// include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");


intranet_auth();
intranet_opendb();

$studentID = (is_array($studentID)? $studentID[0]:$studentID);
$laccessright = new libaccessright();

if	(	!$plugin['AccountMgmt_StudentRegistry'] || 
		(!$_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-View") && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-Manage")) &&
		!$_SESSION['ParentFillOnlineReg']
	) 
{
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$lsr = new libstudentregistry();
if($_SESSION['ParentFillOnlineReg'])
	$linterface = new interface_html("popup.html");
else
	$linterface = new interface_html();

$result = $lsr -> RETRIEVE_STUDENT_INFO_BASIC_HK($studentID);
$li = new libuser($studentID);
$lfcm = new form_class_manage();

#Gender related info
$gender_ary      = $lsr -> GENDER_ARY();
$gender_val      = $result[0]['Gender']? $lsr -> RETRIEVE_DATA_ARY($gender_ary, $result[0]['Gender']) : "--";
$gender_edit_ary = $lsr -> RETRIEVE_DATA_ARY($gender_ary);
$gender_RBL_html = $lsr -> GENERATE_RADIO_BTN_LIST($gender_edit_ary, "gender", $gender_val);

# ID Document Type
if($result[0]['ID_TYPE']==1)	# passport
	$ID_DATA = $Lang['StudentRegistry']['Passport'] . " (". $result[0]['PassportNo'] .")";
else							# others
	$ID_DATA = $i_general_others . " (". $result[0]['ID_NO'] .")";
$ID_DATA_OPTIONS = "<input type='radio' name='passport_type' value='1' id='passporttype1' ". ($result[0]['ID_TYPE']==1 ? "checked": "")."> <label for='passporttype1'>".$Lang['StudentRegistry']['Passport'] . "</label> (". $Lang['StudentRegistry']['Number'].": <input type='text' name='passport_no' value='". $result[0]['PassportNo'] ."'/>)<br>";
$ID_DATA_OPTIONS .= "<input type='radio' name='passport_type' value='2' id='passporttype2' ". ($result[0]['ID_TYPE']==2 ? "checked": "")."> <label for='passporttype2'>". $Lang['General']['Others'] . "</label> (". $Lang['General']['PlsSpecify'].": <input type='text' name='id_no' value='". $result[0]['ID_NO'] ."'/>)";
	 
# Ethnicity
if($result[0]['ETHNICITY_TYPE']==1)	# Chinese
	$Ethnicity_data = $Lang['StudentRegistry']['Ethnicity_Chinese'];
else							# others
	$Ethnicity_data = $result[0]['ETHNICITY_OTHERS'];
$Ethnicity_data_options = "<input type='radio' name='ethnicity' value='1' id='ethnicity1' ". ($result[0]['ETHNICITY_TYPE']==1 ? "checked": "")."> <label for='ethnicity1'>". $Lang['StudentRegistry']['Ethnicity_Chinese'] ."</label> ";
$Ethnicity_data_options .= "<input type='radio' name='ethnicity' value='2' id='ethnicity2' ". ($result[0]['ETHNICITY_TYPE']==2 ? "checked": "")."> <label for='ethnicity2'>". $Lang['General']['Others'] ."</label> (". $Lang['General']['PlsSpecify'].": <input type='text' name='ethnicity_others' value='". $result[0]['ETHNICITY_OTHERS'] ."'/>)";

# Language Spoken at Home
if($result[0]['FAMILY_LANG'] > 0)	
	$HomeLang_data = $Lang['StudentRegistry']['HomeLangOptions'][$result[0]['FAMILY_LANG']-1];
else							# others
	$HomeLang_data = $result[0]['FAMILY_LANG_OTHERS'];
for($i=0;$i<sizeof($Lang['StudentRegistry']['HomeLangOptions']);$i++)
{
	$HomeLang_data_options .= "<input type='radio' name='family_lang' value='". ($i+1) ."' id='FamilyLang". ($i+1) ."' ". ($result[0]['FAMILY_LANG']==($i+1) ? "checked": "")."> <label for='FamilyLang". ($i+1) ."'>". $Lang['StudentRegistry']['HomeLangOptions'][$i] ."</label> ";
}
$HomeLang_data_options .= "<input type='radio' name='family_lang' value='0' id='FamilyLang0' ". ($result[0]['FAMILY_LANG']==0 ? "checked": "")."> <label for='FamilyLang0'>". $Lang['General']['Others'] ."</label> (". $Lang['General']['PlsSpecify'].": <input type='text' name='family_lang_others' value='". $result[0]['FAMILY_LANG_OTHERS'] ."'/>)";

# Religion
if($result[0]['RELIGION'] > 0)	
	$Religion_data = $Lang['StudentRegistry']['ReligionOptions'][$result[0]['RELIGION']-1];
else							# others
	$Religion_data = $result[0]['RELIGION_OTHERS'];
for($i=0;$i<sizeof($Lang['StudentRegistry']['ReligionOptions']);$i++)
{
	$Religion_data_options .= "<input type='radio' name='religion' value='". ($i+1) ."' id='Religion". ($i+1) ."' ". ($result[0]['RELIGION']==($i+1) ? "checked": "")."> <label for='Religion". ($i+1) ."'>". $Lang['StudentRegistry']['ReligionOptions'][$i] ."</label> ";
}
$Religion_data_options .= "<input type='radio' name='religion' value='0' id='Religion0' ". ($result[0]['RELIGION']==0 ? "checked": "")."> <label for='Religion0'>". $Lang['General']['Others'] ."</label> (". $Lang['General']['PlsSpecify'].": <input type='text' name='religion_others' value='". $result[0]['RELIGION_OTHERS'] ."'/>)";
	
# English Address
for($i=0;$i<sizeof($Lang['StudentRegistry']['AreaOptions']);$i++)
{
	$AddressAreaOptions .= "<input type='radio' name='address_area' value='". ($i+1) ."' id='AddressArea". ($i+1) ."' ". ($result[0]['ADDRESS_AREA']==($i+1) ? "checked": "")."> <label for='AddressArea". ($i+1) ."'>". $Lang['StudentRegistry']['AreaOptions'][$i] ."</label> ";
}

# - Last Class Level & Class No. (For current students only) -
$LastAcademicYear_data = $lsr->RETRIEVE_LAST_CLASSINFO_HK($studentID);
$last_academic_year = $lfcm->Get_Academic_Year_List('', $OrderBySequence, $excludeYearIDArr="", $noPastYear="", $pastAndCurrentYearOnly=1, $excludeCurrentYear=1);
for($i=0;$i<6;$i++)
{
	if($last_academic_year[$i]['AcademicYearID']=="") break;
	
	$LastAcademicYear_table_year .= "<td style='text-align:center;'>". $last_academic_year[$i]['YearNameEN'] ."</td>";
	$LastAcademicYear_table_classname .= "<td><input type='text' name='ClassName_". $last_academic_year[$i]['AcademicYearID'] ."' value='". $LastAcademicYear_data[$last_academic_year[$i]['AcademicYearID']]['ClassName'] ."'></td>";
	$LastAcademicYear_table_classnumber .= "<td><input type='text' name='ClassNumber_". $last_academic_year[$i]['AcademicYearID'] ."' value='". $LastAcademicYear_data[$last_academic_year[$i]['AcademicYearID']]['ClassNumber'] ."'></td>";
	
	if($LastAcademicYear_data[$last_academic_year[$i]['AcademicYearID']])
	{
		$LastAcademicYear_data_table_year .= "<td style='text-align:center;'>". $last_academic_year[$i]['YearNameEN'] ."</td>";
		$LastAcademicYear_data_table_classname .= "<td style='text-align:center;'>". $LastAcademicYear_data[$last_academic_year[$i]['AcademicYearID']]['ClassName']  ."</td>";
		$LastAcademicYear_data_table_classnumber .= "<td style='text-align:center;'>". $LastAcademicYear_data[$last_academic_year[$i]['AcademicYearID']]['ClassNumber']  ."</td>";
	}
}	
$LastAcademicYear_table = "<table class='inside_form_table' width='100%'><tr><td></td>";
$LastAcademicYear_table .= $LastAcademicYear_table_year;
$LastAcademicYear_table .= "</tr><tr><td>". $i_ClassName ."</td>";
$LastAcademicYear_table .= $LastAcademicYear_table_classname;
$LastAcademicYear_table .= "</tr><tr><td>". $i_ClassNumber ."</td>";
$LastAcademicYear_table .= $LastAcademicYear_table_classnumber;
$LastAcademicYear_table .= "</tr></table>";

if(!empty($LastAcademicYear_data))
{
	$LastAcademicYear_data_table = "<table class='inside_form_table' width='100%'><tr><td></td>";
	$LastAcademicYear_data_table .= $LastAcademicYear_data_table_year;
	$LastAcademicYear_data_table .= "</tr><tr><td>". $i_ClassName ."</td>";
	$LastAcademicYear_data_table .= $LastAcademicYear_data_table_classname;
	$LastAcademicYear_data_table .= "</tr><tr><td>". $i_ClassNumber ."</td>";
	$LastAcademicYear_data_table .= $LastAcademicYear_data_table_classnumber;
	$LastAcademicYear_data_table .= "</tr></table>";
}
else
{
	$LastAcademicYear_data_table = $Lang['General']['NoRecordAtThisMoment'];
}

# Brothers & sisters currently studying in school
$BroSis_data = $lsr->RETRIEVE_BRO_SIS_HK($studentID);
$BroSis_table = "<table class='inside_form_table' width='100%'><tr><td>". $Lang['SysMgr']['SchoolNews']['Name'] ."</td><td>". $i_ClassName ."</td><td>". $i_ClassNumber ."</td><td>". $Lang['StudentRegistry']['Relationship'] ."</td></tr>";
for($i=0;$i<3;$i++)
{
	$BroSis_table .= "<tr>";
	$BroSis_table .= "<td><input type='text' name='BS_Name_". $i ."' value='". $BroSis_data[$i]['BS_Name'] ."'></td>";
	$BroSis_table .= "<td><input type='text' name='BS_ClassName_". $i ."' value='". $BroSis_data[$i]['ClassName'] ."'></td>";
	$BroSis_table .= "<td><input type='text' name='BS_ClassNumber_". $i ."' value='". $BroSis_data[$i]['ClassNumber'] ."'></td>";
	$BroSis_table .= "<td><input type='text' name='BS_Relationship_". $i ."' value='". $BroSis_data[$i]['Relationship'] ."'></td>";
	$BroSis_table .= "</tr>";
}
$BroSis_table .= "</table>";
if(!empty($BroSis_data))
{
	$BroSis_data_table = "<table class='inside_form_table' width='100%'><tr><td>". $Lang['SysMgr']['SchoolNews']['Name'] ."</td><td>". $i_ClassName ."</td><td>". $i_ClassNumber ."</td><td>". $Lang['StudentRegistry']['Relationship'] ."</td></tr>";
	if(!empty($BroSis_data))
	{
		foreach($BroSis_data as $k=>$d)
		{
			$BroSis_data_table .= "<tr>";
			$BroSis_data_table .= "<td>". $d['BS_Name'] ."</td>";
			$BroSis_data_table .= "<td>". $d['ClassName'] ."</td>";
			$BroSis_data_table .= "<td>". $d['ClassNumber'] ."</td>";
			$BroSis_data_table .= "<td>". $d['Relationship'] ."</td>";
			$BroSis_data_table .= "</tr>";	
		}
	}
	$BroSis_data_table .= "</table>";
}
else
{
	$BroSis_data_table = $Lang['General']['NoRecordAtThisMoment'];
}

# Guardian
$guardian_selection = array();
for($g=1;$g<=2;$g++)
{
	$g_result = $lsr -> RETRIEVE_GUARDIAN_INFO_HK($studentID, $g);

	$gType[$g] = $g_result['PG_TYPE']==1 ? $Lang['StudentRegistry']['Father'] : ($g_result['PG_TYPE']==2 ? $Lang['StudentRegistry']['Mother'] : $g_result['PG_TYPE_OTHERS']);
	$guardian_type[$g] = "	<input type='radio' name='guardian_type". $g."' id='guardian_type". $g."_1' value='1' ". ($g_result['PG_TYPE']==1?"checked":"") ."> <label for='guardian_type". $g."_1'>" . $Lang['StudentRegistry']['Father'] . "</label>  
							<input type='radio' name='guardian_type". $g."' id='guardian_type". $g."_2' value='2' ". ($g_result['PG_TYPE']==2?"checked":"") ."> <label for='guardian_type". $g."_2'>" . $Lang['StudentRegistry']['Mother'] . "</label> 
							<br><input type='radio' name='guardian_type". $g."' id='guardian_type". $g."_0' value='0' ". ($g_result['PG_TYPE']==0?"checked":"") ."> <label for='guardian_type". $g."_0'>" . $Lang['StudentRegistry']['Other'] . "</label> (". $Lang['General']['PlsSpecify'].": <input type='text' name='guardian_type_others". $g ."' value='". $g_result['PG_TYPE_OTHERS'] ."'/>)";
							
	$gEmail[$g] = $g_result['EMAIL'];
	$guardian_email[$g] = "<input type='text' name='guardian_email". $g."' value='". $gEmail[$g] ."'/>";
	
	$gEnglishName[$g] = $g_result['NAME_E'];
	$guardian_english_name[$g] = "<input type='text' name='guardian_english_name". $g."' value='". $gEnglishName[$g] ."'/>";
	
	$gChineseName[$g] = $g_result['NAME_C'];
	$guardian_chinese_name[$g] = "<input type='text' name='guardian_chinese_name". $g."' value='". $gChineseName[$g] ."'/>";
	
	$gHKID[$g] = $g_result['HKID'];
	$guardian_hkid[$g] = "<input type='text' name='guardian_hkid". $g."' value='". $g_result['HKID'] ."'/>";
	
	$gOccupation[$g] = $g_result['JOB_TITLE_OTHERS'];
	$guardian_occupation[$g] = "<input type='text' name='guardian_occupation". $g."' value='". $g_result['JOB_TITLE_OTHERS'] ."'/>";
	
	$gTel[$g] = $g_result['TEL'];
	$guardian_tel[$g] = "<input type='text' name='guardian_tel". $g."' value='". $g_result['TEL'] ."'/>";
	
	$gMobile[$g] = $g_result['MOBILE'];
	$guardian_mobile[$g] = "<input type='text' name='guardian_mobile". $g."' value='". $g_result['MOBILE'] ."'/>";
	
	$gReligion[$g] = $g_result['RELIGION']>0 ? $Lang['StudentRegistry']['ReligionOptions'][$g_result['RELIGION']-1] : $g_result['RELIGION_OTHERS'];
	for($i=0;$i<sizeof($Lang['StudentRegistry']['ReligionOptions']);$i++)
	{
		$guardian_religion[$g] .= "<input type='radio' name='guardian_religion". $g."' value='". ($i+1) ."' id='guardian_religion".$g."_". ($i+1) ."' ". ($g_result['RELIGION']==($i+1)?"checked":"") ."> <label for='guardian_religion".$g."_". ($i+1) ."'>". $Lang['StudentRegistry']['ReligionOptions'][$i] ."</label> ";
	}
	$guardian_religion[$g] .= "<br><input type='radio' name='guardian_religion". $g."' value='0' id='guardian_religion".$g."_0' ". ($g_result['RELIGION']==0?"checked":"") ."> <label for='guardian_religion".$g."_0'>". $Lang['General']['Others'] ."</label> (". $Lang['General']['PlsSpecify'].": <input type='text' name='guardian_religion".$g."_others' value='". $g_result['RELIGION_OTHERS'] ."'/>)";

	$gChurch[$g] = $g_result['CHURCH'];
	$guardian_church[$g] = "<input type='text' name='guardian_church". $g."' value='". $g_result['CHURCH'] ."'/>";
	
	$gAddressRoomEn[$g] = $g_result['ADDRESS_ROOM_EN'];
	$gAddressFloorEn[$g] = $g_result['ADDRESS_FLOOR_EN'];
	$gAddressBlkEn[$g] = $g_result['ADDRESS_BLK_EN'];
	$gAddressBldEn[$g] = $g_result['ADDRESS_BLD_EN'];
	$gAddressEstEn[$g] = $g_result['ADDRESS_EST_EN'];
	$gAddressStrEn[$g] = $g_result['ADDRESS_STR_EN'];
	$gAddressDistrictEn[$g] = $g_result['ADDRESS_DISTRICT_EN'];
	$gAddressArea[$g] = $Lang['StudentRegistry']['AreaOptions'][$g_result['ADDRESS_AREA']-1];
	for($i=0;$i<sizeof($Lang['StudentRegistry']['AreaOptions']);$i++)
	{
		$guardian_address_area[$g] .= "<input type='radio' name='guardian_address_area". $g."' value='". ($i+1) ."' id='guardian_address_area".$g."_". ($i+1) ."' ". ($g_result['ADDRESS_AREA']==($i+1)?"checked":"") ."> <label for='guardian_address_area".$g."_". ($i+1) ."'>". $Lang['StudentRegistry']['AreaOptions'][$i] ."</label> ";
	}
}

# Contact Persion in case of emergency
$contact_result = $lsr -> RETRIEVE_EMERGENCY_CONTACT_INFO_HK($studentID);
if($contact_result['Contact1'] > 0)	
	$contact1_data = $contact_result['Contact1']==1? $Lang['StudentRegistry']['Father'] : $Lang['StudentRegistry']['Mother'];
else							# others
	$contact1_data = $contact_result['Contact1_Others'];
$contact2_data = $contact_result['Contact2']==1? $Lang['StudentRegistry']['Father'] : $Lang['StudentRegistry']['Mother'];
$contact3_title_data = $contact_result['Contact3_Title']==1 ? $i_title_mr :($contact_result['Contact1_Others']==2 ? $i_title_mrs : $i_title_ms);

	

#Past Enrollment Information
// $Past_Enroll = $lsr -> RETRIEVE_STUDENT_PAST_ENROLLMENT_RECORD($studentID);

/*
#Emergency Contact Info
$ecInfo = $lsr->getParentGuardianInfo($studentID, "E");
$ec_CH_Name = intranet_htmlspecialchars($ecInfo['NAME_C']);
$ec_EN_Name = intranet_htmlspecialchars($ecInfo['NAME_E']);
$CH_Name_HTML = "<input type=\"text\" name=\"ec_NAME_C\" id=\"ec_NAME_C\" VALUE=\"$ec_CH_Name\" readonly=\"readonly\" style=\"border: 0px\">";
$EN_Name_HTML = "<input type=\"text\" name=\"ec_NAME_E\" id=\"ec_NAME_E\" VALUE=\"$ec_EN_Name\" readonly=\"readonly\" style=\"border: 0px\">";  
$ec_HOME_TEL = "<input type=\"text\" name=\"ec_HOME_TEL\" id=\"ec_HOME_TEL\" VALUE=\"".$ecInfo['TEL']."\" readonly=\"readonly\" style=\"border: 0px\">";
$ec_MOBILE_TEL = "<input type=\"text\" name=\"ec_MOBILE_TEL\" id=\"ec_MOBILE_TEL\" VALUE=\"".$ecInfo['MOBILE']."\" readonly=\"readonly\" style=\"border: 0px\">";
*/

// # Registry Status
// switch($result[0]['RecordStatus']) {
// 	case 0: $ownRegistryStatus = $Lang['StudentRegistry']['StatusSuspended'];
// 			break;
// 	case 1: $ownRegistryStatus = $Lang['StudentRegistry']['StatusApproved'];
// 			break;
// 	case 2: $ownRegistryStatus = $Lang['StudentRegistry']['StatusSuspended'];
// 			break;
// 	case 3: $ownRegistryStatus = $Lang['StudentRegistry']['StatusLeft'];
// 			break;
// 	default: $ownRegistryStatus = "---";	
// }

/*
if($Past_Enroll)
{
	$Past_Enroll_html = "";
	for($i=0;$i<sizeof($Past_Enroll);$i++)
	{
		$Past_Enroll_html .= "<tr>
        						<td><em>(".$Lang['StudentRegistry']['AcademicYear'].")</em></td>
								<td><span class=\"row_content\">".$Past_Enroll[$i]['YearName']."</span></td>
        						<td><em>(".$Lang['StudentRegistry']['Grade'].")</em></td>
								<td><span class=\"row_content\">".$Past_Enroll[$i]['Grade']."</span></td>								
								<td><em>(".$Lang['StudentRegistry']['Class'].")</em></td>
								<td><span class=\"row_content\">".$Past_Enroll[$i]['Class']."</span></td>
								<td><em>(".$Lang['StudentRegistry']['InClassNumber'].")</em></td>
								<td><span class=\"row_content\">".$Past_Enroll[$i]['ClassNumber']."</span></td>
                        	 </tr>";
	}
}
*/

#Modified By
$ModifiedBy = $result[0]['ModifyBy']? $lsr -> RETRIEVE_MODIFIED_BY_INFO($result[0]['ModifyBy']) : "";

$CurrentPageArr['StudentRegistry'] = 1;
$CurrentPage = "Mgmt_RegistryInfo";
$TAGS_OBJ[] = array($Lang['Menu']['AccountMgmt']['StudentRegistryInfo'], "index.php");
$MODULE_OBJ['title'] = $Lang['Header']['Menu']['StudentRegistry'];
$MODULE_OBJ = $lsr->GET_MODULE_OBJ_ARR();

if(!$_SESSION['ParentFillOnlineReg'])
{
	# navigation bar 
	$PAGE_NAVIGATION[] = array($Lang['StudentRegistry']['ClassList'], "index.php?AcademicYearID=".$AcademicYearID);
	$PAGE_NAVIGATION[] = array($result[0]['ClassName']." ".$Lang['StudentRegistry']['StudentList'], "class.php?AcademicYearID=$AcademicYearID&targetClass=::".$result[0]['YearClassID']);
	$PAGE_NAVIGATION[] = array(($intranet_session_language == "en"? $result[0]['EnglishName'] : $result[0]['ChineseName']), "");
}

#Save and Cancel Button
$SaveBtn   = $linterface->GET_ACTION_BTN($button_save, "button", "javascript:checksubmit(this.form)", "SaveBtn", "style=\"display: none\"");
$CancelBtn = $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:ResetInfo()", "CancelBtn", "style=\"display: none\"");

########## Official Photo
$photo_link = "/images/myaccount_personalinfo/samplephoto.gif";
if ($li->PhotoLink !="")
{
    if (is_file($intranet_root.$li->PhotoLink))
    {
        $photo_link = $li->PhotoLink;
    }
}
########## Personal Photo
$photo_personal_link = "/images/myaccount_personalinfo/samplephoto.gif";
if ($li->PersonalPhotoLink !="")
{
    if (is_file($intranet_root.$li->PersonalPhotoLink))
    {
        $photo_personal_link = $li->PersonalPhotoLink;
   }
}

if($xmsg==1) $msg = $Lang['General']['ReturnMessage']['UpdateSuccess'];
$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$xmsg]);
?>

<script language="javascript">
	function EditInfo()
	{
		$('#LastModified').attr('style', 'display: none');
		$('#EditBtn').attr('style', 'visibility: hidden');
		
		$('.Edit_Hide').attr('style', 'display: none');
		$('.Edit_Show').attr('style', '');
		
		$('.tabletextrequire').attr('style', '');
		$('#SaveBtn').attr('style', '');
		$('#CancelBtn').attr('style', '');
		$('#FormReminder').attr('style', '');
		
		$(':text').attr('style', '');
		$(':text').attr('readonly', '');
// 		$(':text[value="--"]').val('');
	}
	
	function ResetInfo()
	{
		document.form1.reset();
		$('#LastModified').attr('style', '');
		$('#EditBtn').attr('style', '');
		
		$('.Edit_Hide').attr('style', '');
		$('.Edit_Show').attr('style', 'display: none');
		
		$('.tabletextrequire').attr('style', 'display: none');
		$('#SaveBtn').attr('style', 'display: none');
		$('#CancelBtn').attr('style', 'display: none');
		$('#FormReminder').attr('style', 'display: none');
		
		$(':text').attr('style', 'border: 0px');
		$(':text').attr('readonly', 'readonly');
// 		$(':text[value=""]').val('--');
		
		$('#ErrorLog').html('');
	}

	function checksubmit(obj)
	{
		$.post(
				'ajax_view_check.php',
				{
					studentID 	: obj.studentID.value,
					chinese_name : obj.chinese_name.value,
					english_name : obj.english_name.value,
					student_email : obj.student_email.value,
					hkid : obj.hkid.value,
					student_email : obj.student_email.value
				},
				function(data){
					if(data=="")
					{
						obj.submit();
					}
					else
					{
						$('#ErrorLog').html(data);
					}
				});
	}

	$(document).ready(function()
	{
// 		$(':text[value=""]').val('--');
	});
	
<? if($_SESSION['ParentFillOnlineReg'] && $xmsg) {?>
if(window.opener!=null)
	window.opener.location.reload();
<? } ?>	
	
</script>

<form name="form1" method="POST" action="view_update.php">
<table width="95%" border="0" cellspacing="0" cellpadding="0">
	<tr> 
		<td class="main_content">
			<div class="navigation">
				<?=$linterface->GET_NAVIGATION($PAGE_NAVIGATION)?>
				<p class="spacer"></p>
			</div>
			<div class="table_board">
        		<div class="table_row_tool row_content_tool">
                	<?=($laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-Manage") || $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] || $_SESSION['ParentFillOnlineReg']) ? $linterface->GET_SMALL_BTN($Lang['StudentRegistry']['Edit'], "button", "javascript:EditInfo();", "EditBtn") : "&nbsp;" ?>
                </div>
                <p class="spacer"></p>
                
                <? ########################################################### ?>
				<? #  Student Information                                    # ?>
				<? ########################################################### ?>
                <div class="form_sub_title_v30">
                	<em>- <span class="field_title"><?= $Lang['StudentRegistry']['StudInfo'] ?></span> -</em>
                    <p class="spacer"></p>
                </div>
                <table class="form_table_v30">
                	<col class="field_title_short">
                    <col class="field_content_short">
                    <col class="field_title_short">
                    <col class="field_content_short">
                    <col class="field_title_short">
                    <col class="field_content_short">
					<tbody>
                        <tr>
                            <td class="field_title_short"><span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['EnglishName'] ?></td>
                            <td>
                            	<span class="Edit_Hide"><?= $result[0]['EnglishName'] ? $result[0]['EnglishName'] : "--"?></span>
								<span class="Edit_Show" style="display:none"><input type="text" name="english_name" value="<?= $result[0]['EnglishName']?>"/></span>
                            </td>
                        	<td class="field_title_short"><span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['ChineseName'] ?></td>
                            <td>
                            	<span class="Edit_Hide"><?= $result[0]['ChineseName'] ? $result[0]['ChineseName'] : "--"?></span>
								<span class="Edit_Show" style="display:none"><input type="text" name="chinese_name" value="<?= $result[0]['ChineseName']?>"/></span>
							<td rowspan="10">
								<? if(is_file($intranet_root.$li->PhotoLink)) { ?> 
	                            	<?=$Lang['Personal']['OfficialPhoto']?><br /><img src="<?=$photo_link?>" width="100" height="130"><br>
	                            <? } ?>
	                            <? if(is_file($intranet_root.$li->PersonalPhotoLink)) { ?> 
	                        		<?=$Lang['Personal']['PersonalPhoto']?><br /><img src="<?=$photo_personal_link?>" width="100" height="130">
	                        	<? } ?>
                        	</td>
                        </tr>
						<tr>
							<td class="field_title_short"><?= $Lang['StudentRegistry']['ChineseCommercialCode'] ?></td>
							<td>
								<span class="Edit_Hide"><?= $result[0]['CHN_COMMERCIAL_CODE'] ? $result[0]['CHN_COMMERCIAL_CODE'] : "--"?></span>
								<span class="Edit_Show" style="display:none"><input type="text" name="chn_commercial_code" value="<?= $result[0]['CHN_COMMERCIAL_CODE']?>"/></span>
                            </td>
                            <td class="field_title_short"><?= $Lang['StudentRegistry']['Gender'] ?></td>
                            <td>
                            	<span class="Edit_Hide"><?= $gender_val ?></span>
                            	<span class="Edit_Show" style="display:none"><?= $gender_RBL_html ?></span>
                            </td>
                        </tr>
                        <tr>
							<td class="field_title_short"><?= $Lang['StudentRegistry']['BirthDate'] ?></td>
                            <td>
                            	<span class="Edit_Hide"><?= $result[0]['DOB']=="0000-00-00" ? "" : $result[0]['DOB'] ?></span>
                            	<span class="Edit_Show" style="display:none"><? echo $linterface->GET_DATE_PICKER("date_of_birth", $result[0]['DOB']) ?></span>
                            </td>
                            <td class="field_title_short"><?= $Lang['StudentRegistry']['Province'] ?></td>
                            <td>
                            	<span class="Edit_Hide"><?= $result[0]['ORIGIN']?></span>
                            	<span class="Edit_Show" style="display:none"><input type="text" name="origin" value="<?= $result[0]['ORIGIN'] ?>"/></span>
                            </td>
                        </tr>
                        
                        <tr>
							<td class="field_title_short"><?= $i_HKID ?></td>
                            <td>
                            	<span class="Edit_Hide"><?= $result[0]['HKID'] ?></span>
                            	<span class="Edit_Show" style="display:none"><input type="text" name="hkid" value="<?= $result[0]['HKID']?>"/></span>
                            </td>
                            <td class="field_title_short"><?= $profile_nationality ?></td>
                            <td>
                            	<span class="Edit_Hide"><?= $result[0]['Nationality'] ?></span>
                            	<span class="Edit_Show" style="display:none"><input type="text" name="nationality" value="<?= $result[0]['Nationality']?>"/></span>
                            </td>
                            
                       <tr>     
                            <td class="field_title_short"><?= $Lang['StudentRegistry']['ID_DocumentType'] ?></td>
                            <td colspan="3">
                            	<span class="Edit_Hide"><?= $ID_DATA?></span>
                            	<span class="Edit_Show" style="display:none"><?=$ID_DATA_OPTIONS?></span>
                            </td>
                        </tr>
                        <tr>
                            <td class="field_title_short"><?= $Lang['StudentRegistry']['Race'] ?></td>
                            <td colspan="3">
                            	<span class="Edit_Hide"><?= $Ethnicity_data?></span>
                            	<span class="Edit_Show" style="display:none"><?=$Ethnicity_data_options?></span>
                            </td>
                        </tr>
                        
                        <tr>
							<td class="field_title_short"><?= $Lang['StudentRegistry']['HomeLang'] ?></td>
                            <td colspan="3">
                            	<span class="Edit_Hide"><?= $HomeLang_data?></span>
                            	<span class="Edit_Show" style="display:none"><?= $HomeLang_data_options?></span>
                            	
                            </td>
                        </tr>
                        
                        <tr>
							<td class="field_title_short"><?= $Lang['StudentRegistry']['Religion'] ?></td>
                            <td colspan="3">
                            	<span class="Edit_Hide"><?= $Religion_data?></span>
                            	<span class="Edit_Show" style="display:none"><?= $Religion_data_options?></span>
                            </td>
                        </tr>
                        
                        <tr>
                            <td class="field_title_short"><?= $Lang['StudentRegistry']['Church'] ?></td>
                            <td colspan="4">
                            	<span class="Edit_Hide"><?= $result[0]['CHURCH'] ?></span>
                            	<span class="Edit_Show" style="display:none"><input type="text" class="textboxtext" name="church" value="<?= $result[0]['CHURCH']?>"/></span>
                            </td>
                        </tr>
                        
                        <tr>
							<td class="field_title_short"><?= $Lang['StudentRegistry']['HomePhoneNo'] ?></td>
                            <td>
                            	<span class="Edit_Hide"><?= $result[0]['HomeTelNo']?></span>
                            	<span class="Edit_Show" style="display:none"><input type="text" name="home_tel_no" value="<?= $result[0]['HomeTelNo']?>"/></span>
                            </td>
                            <td class="field_title_short"><?= $Lang['StudentRegistry']['MobilePhoneNo'] ?></td>
                            <td>
                            	<span class="Edit_Hide"><?= $result[0]['MobileTelNo'] ?></span> 
                            	<span class="Edit_Show" style="display:none"><input type="text" name="mobile_no" value="<?= $result[0]['MobileTelNo']?>"/></span>
                            </td>
                        </tr>
                        
                        <tr>
							<td class="field_title_short"><span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['EMailAddress'] ?></td>
                            <td colspan="4">
                            	<span class="Edit_Hide"><?= $result[0]['UserEmail'] ?></span>
                            	<span class="Edit_Show" style="display:none"><input type="text" class="textboxtext" name="student_email" value="<?= $result[0]['UserEmail']?>"/></span>
                            </td>
                        </tr>
                        
                        <tr>
							<td class="field_title_short"><?= $Lang['StudentRegistry']['Address'] ?></td>
                            <td colspan="3">
                            			<div class="form_field_sub_content">
		                     		       	<table class="inside_form_table">
		                     		       		<tr>
		                     		       			<td><em>(<?= $Lang['StudentRegistry']['AddressField']['Flat'] ?>)</em></td>
		                     		       			<td>
		                     		       				<span class="Edit_Hide"><?= $result[0]['ADDRESS_ROOM_EN']?></span>
		                     		       				<span class="Edit_Show" style="display:none"><input type="text" class="textboxnum" name="address_room_en" value="<?= $result[0]['ADDRESS_ROOM_EN']?>"/></span>
		                     		       			</td>
		                     		       			<td><em>(<?= $Lang['StudentRegistry']['AddressField']['Floor'] ?>)</em></td>
		                     		       			<td>
		                     		       				<span class="Edit_Hide"><?= $result[0]['ADDRESS_FLOOR_EN']?></span>
		                     		       				<span class="Edit_Show" style="display:none"><input type="text" class="textboxnum" name="address_floor_en" value="<?= $result[0]['ADDRESS_FLOOR_EN']?>"/></span>
		                     		       			</td>
		                     		       			<td><em>(<?= $Lang['StudentRegistry']['AddressField']['Block'] ?>)</em></td>
		                        	    			<td>
		                        	    				<span class="Edit_Hide"><?= $result[0]['ADDRESS_BLK_EN']?></span>
		                        	    				<span class="Edit_Show" style="display:none"><input type="text" class="textboxnum" name="address_blk_en" value="<?= $result[0]['ADDRESS_BLK_EN']?>"/></span>
		                        	    			</td>
		                        	    		</tr>
		                        	    		<tr>
		                     		       			<td><em>(<?= $Lang['StudentRegistry']['AddressField']['Building'] ?>)</em></td>
		                     		       			<td colspan="5">
		                     		       				<span class="Edit_Hide"><?= $result[0]['ADDRESS_BLD_EN']?></span>
		                     		       				<span class="Edit_Show" style="display:none"><input type="text" class="textboxtext" name="address_bld_en" value="<?= $result[0]['ADDRESS_BLD_EN']?>"/></span>
		                     		       			</td>
		                        	    		</tr>
		                        	    		<tr>
		                     		       			<td><em>(<?= $Lang['StudentRegistry']['AddressField']['Estate'] ?>)</em></td>
		                     		       			<td colspan="5">
		                     		       				<span class="Edit_Hide"><?= $result[0]['ADDRESS_EST_EN']?></span>
		                     		       				<span class="Edit_Show" style="display:none"><input type="text" class="textboxtext" name="address_est_en" value="<?= $result[0]['ADDRESS_EST_EN']?>"/></span>
		                     		       			</td>
		                        	    		</tr>
		                        	    		<tr>
		                     		       			<td><em>(<?= $Lang['StudentRegistry']['AddressField']['Street'] ?>)</em></td>
		                     		       			<td colspan="5">
		                     		       				<span class="Edit_Hide"><?= $result[0]['ADDRESS_STR_EN']?></span>
		                     		       				<span class="Edit_Show" style="display:none"><input type="text" class="textboxtext" name="address_str_en" value="<?= $result[0]['ADDRESS_STR_EN']?>"/></span>
		                     		       			</td>
		                        	    		</tr>
		                        	    		<tr>
		                     		       			<td><em>(<?= $Lang['StudentRegistry']['AddressField']['District'] ?>)</em></td>
		                     		       			<td colspan="5">
		                     		       				<span class="Edit_Hide"><?= $result[0]['ADDRESS_DISTRICT_EN']?></span>
		                     		       				<span class="Edit_Show" style="display:none"><input type="text" class="textboxtext" name="address_district_en" value="<?= $result[0]['ADDRESS_DISTRICT_EN']?>"/></span>
		                     		       			</td>
		                        	    		</tr>
		                        	    		<tr>
		                     		       			<td><em>(<?= $Lang['StudentRegistry']['AddressField']['Area'] ?>)</em></td>
		                     		       			<td colspan="5">
		                     		       				<span class="Edit_Hide"><?= $Lang['StudentRegistry']['AreaOptions'][$result[0]['ADDRESS_AREA']-1]?></span>
		                     		       				<span class="Edit_Show" style="display:none"><?=$AddressAreaOptions?></span>
		                     		       			</td>
		                        	    		</tr>
		                        	    	</table>
										</div>
										</td>
                        </tr>
                       
					</tbody>
					<col class="field_title_short">
                    <col class="field_c">
				</table>
				
				
				<? ########################################################### ?>
				<? #  Last School 	                                         # ?>
				<? ########################################################### ?>
				<div class="form_sub_title_v30">
    				<em>- <span class="field_title"><?=$Lang['StudentRegistry']['LastSchool']?></span> -</em>
					<p class="spacer"></p>
				</div>
				<table class="form_table_v30">
					<tbody>
                        <tr>
                            <td class="field_title_short"><?= $Lang['General']['EnglishName2'] ?></td>
                            <td>
                            	<span class="Edit_Hide"><?= $result[0]['LAST_SCHOOL_EN']?></span>
                            	<span class="Edit_Show" style="display:none"><input type="text" class="textboxtext" name="last_school_en" value="<?= $result[0]['LAST_SCHOOL_EN']?>"/></span>
                            	</td>
                        </tr>
                        <tr>
							<td class="field_title_short"><?= $Lang['General']['ChineseName2'] ?></td>
                            <td>
                            	<span class="Edit_Hide"><?= $result[0]['LAST_SCHOOL_CH']?></span>
                            	<span class="Edit_Show" style="display:none"><input type="text" class="textboxtext" name="last_school_ch" value="<?= $result[0]['LAST_SCHOOL_CH']?>"/></span>
                            </td>
                        </tr>
				</tbody>
				</table>
				
				<? ########################################################### ?>
				<? #  Class Level & Class No.                                # ?>
				<? ########################################################### ?>
				<div class="form_sub_title_v30">
    				<em>- <span class="field_title"><?=$Lang['StudentRegistry']['LastClassLevelClassNo']?></span> -</em>
					<p class="spacer"></p>
				</div>
				<table class="form_table_v30">
					<tbody>
                        <tr>
                            <td>
                            	<span class="Edit_Hide"><?=$LastAcademicYear_data_table?></span>
                            	<span class="Edit_Show" style="display:none;"><?=$LastAcademicYear_table?></span>
                            </td>
                        </tr>
				</tbody>
				</table>
				
				<? ########################################################### ?>
				<? #  Brothers & Sistes currently studying                   # ?>
				<? ########################################################### ?>
				<div class="form_sub_title_v30">
    				<em>- <span class="field_title"><?=$Lang['StudentRegistry']['BrotherSisterStudyingInSchool']?></span> -</em>
					<p class="spacer"></p>
				</div>
				<table class="form_table_v30">
					<tbody>
                        <tr>
                            <td>
                            	<span class="Edit_Hide"><?=$BroSis_data_table?></span>
                            	<span class="Edit_Show" style="display:none;"><?=$BroSis_table?></span>
                            </td>
                        </tr>
				</tbody>
				</table>
				
				<? ########################################################### ?>
				<? #  Guardian                                               # ?>
				<? ########################################################### ?>
				<? for($g=1;$g<=2;$g++)  { ?>
				<div class="form_sub_title_v30">
    				<em>- <span class="field_title"><?= ($g==1) ?$Lang['StudentRegistry']['1st'] :$Lang['StudentRegistry']['2nd'] ?><?= $Lang['Identity']['Guardian'] ?></span> -</em>
					<p class="spacer"></p>
				</div>
				<table class="form_table_v30">
					<tbody>
                        <tr>
							<td class="field_title_short"><?= $Lang['StudentRegistry']['GuardianStudentRelationship'] ?></td>
                            <td>
                            	<span class="Edit_Hide"><?=$gType[$g]?></span>
                            	<span class="Edit_Show" style="display:none"><?=$guardian_type[$g]?></span>
                            </td>
                            <td class="field_title_short"><?= $Lang['StudentRegistry']['EMailAddress'] ?></td>
                            <td>
                            	<span class="Edit_Hide"><?= $gEmail[$g] ?></span>
                            	<span class="Edit_Show" style="display:none"><?=$guardian_email[$g]?></span>
                            </td>
                        </tr>
						<tr>
                         	<td class="field_title_short"><?= $Lang['StudentRegistry']['EnglishName'] ?></td>
                            <td>
                           		<span class="Edit_Hide"><?= $gEnglishName[$g] ?></span>
                            	<span class="Edit_Show" style="display:none"><?=$guardian_english_name[$g]?></span>
                            </td>
                            <td class="field_title_short"><?= $Lang['StudentRegistry']['ChineseName'] ?></td>
                            <td>
                            	<span class="Edit_Hide"><?= $gChineseName[$g] ?></span>
                            	<span class="Edit_Show" style="display:none"><?=$guardian_chinese_name[$g]?></span>
                            </td>
                        </tr>
                        <tr>
                         	<td class="field_title_short"><?= $i_HKID ?></td>
                            <td>
                            	<span class="Edit_Hide"><?= $gHKID[$g] ?></span>
                            	<span class="Edit_Show" style="display:none"><?=$guardian_hkid[$g]?></span>	
                           	</td>
                            <td class="field_title_short"><?= $i_StudentGuardian_Occupation ?></td>
                            <td>
                            	<span class="Edit_Hide"><?= $gOccupation[$g] ?></span>
                            	<span class="Edit_Show" style="display:none"><?=$guardian_occupation[$g]?></span>	
                           	</td>
                        </tr>
                         <tr>
                        	<td class="field_title_short"><span class="field_title"><?= $Lang['StudentRegistry']['GuardianDayPhone'] ?></span></td>
                        	<td>
                            	<span class="Edit_Hide"><?= $gTel[$g] ?></span>
                            	<span class="Edit_Show" style="display:none"><?=$guardian_tel[$g]?></span>	
                           	</td>
                        	<td class="field_title_short"><span class="field_title"><?= $Lang['StudentRegistry']['GuardianMobile'] ?></span></td>
                         	<td>
                            	<span class="Edit_Hide"><?= $gMobile[$g] ?></span>
                            	<span class="Edit_Show" style="display:none"><?=$guardian_mobile[$g]?></span>	
                           	</td>
						</tr>
						<tr>
							<td class="field_title_short"><?= $Lang['StudentRegistry']['Religion'] ?></td>
                            <td>
                            	<span class="Edit_Hide"><?= $gReligion[$g] ?></span>
                            	<span class="Edit_Show" style="display:none"><?=$guardian_religion[$g]?></span>	
                           	</td>
                            <td class="field_title_short"><?= $Lang['StudentRegistry']['Church'] ?></td>
                            <td>
                            	<span class="Edit_Hide"><?= $gChurch[$g] ?></span>
                            	<span class="Edit_Show" style="display:none"><?=$guardian_church[$g]?></span>	
                           	</td>
                        </tr>
                        
                        <tr>
							<td class="field_title_short"><?= $Lang['StudentRegistry']['Address'] ?></td>
                            <td colspan="3">
                            	<span class="Edit_Show" style="display:none"><b>(<?=$Lang['StudentRegistry']['NoNeedCompleteSameAsStudent']?>)</b></span>
                    			<div class="form_field_sub_content">
                     		       	<table class="inside_form_table">
                     		       		<tr>
                     		       			<td><em>(<?= $Lang['StudentRegistry']['AddressField']['Flat'] ?>)</em></td>
                     		       			<td>
                     		       				<span class="Edit_Hide"><?= $gAddressRoomEn[$g] ?></span>
                     		       				<span class="Edit_Show" style="display:none"><input type="text" class="textboxnum" name="address_room_en<?=$g?>" value="<?= $gAddressRoomEn[$g] ?>"/></span>
                     		       			</td>
                     		       			<td><em>(<?= $Lang['StudentRegistry']['AddressField']['Floor'] ?>)</em></td>
                     		       			<td>
                     		       				<span class="Edit_Hide"><?= $gAddressFloorEn[$g] ?></span>
                     		       				<span class="Edit_Show" style="display:none"><input type="text" class="textboxnum" name="address_floor_en<?=$g?>" value="<?= $gAddressFloorEn[$g] ?>"/></span>
                     		       			</td>
                     		       			<td><em>(<?= $Lang['StudentRegistry']['AddressField']['Block'] ?>)</em></td>
                        	    			<td>
                        	    				<span class="Edit_Hide"><?= $gAddressBlkEn[$g] ?></span>
                        	    				<span class="Edit_Show" style="display:none"><input type="text" class="textboxnum" name="address_blk_en<?=$g?>" value="<?= $gAddressBlkEn[$g] ?>"/></span>
                        	    			</td>
                        	    		</tr>
                        	    		<tr>
                     		       			<td><em>(<?= $Lang['StudentRegistry']['AddressField']['Building'] ?>)</em></td>
                     		       			<td colspan="5">
                     		       				<span class="Edit_Hide"><?= $gAddressBldEn[$g] ?></span>
                     		       				<span class="Edit_Show" style="display:none"><input type="text" class="textboxtext" name="address_bld_en<?=$g?>" value="<?= $gAddressBldEn[$g] ?>"/></span>
                     		       			</td>
                        	    		</tr>
                        	    		<tr>
                     		       			<td><em>(<?= $Lang['StudentRegistry']['AddressField']['Estate'] ?>)</em></td>
                     		       			<td colspan="5">
                     		       				<span class="Edit_Hide"><?= $gAddressEstEn[$g] ?></span>
                     		       				<span class="Edit_Show" style="display:none"><input type="text" class="textboxtext" name="address_est_en<?=$g?>" value="<?= $gAddressEstEn[$g] ?>"/></span>
                     		       			</td>
                        	    		</tr>
                        	    		<tr>
                     		       			<td><em>(<?= $Lang['StudentRegistry']['AddressField']['Street'] ?>)</em></td>
                     		       			<td colspan="5">
                     		       				<span class="Edit_Hide"><?= $gAddressStrEn[$g] ?></span>
                     		       				<span class="Edit_Show" style="display:none"><input type="text" class="textboxtext" name="address_str_en<?=$g?>" value="<?= $gAddressStrEn[$g] ?>"/></span>
                     		       			</td>
                        	    		</tr>
                        	    		<tr>
                     		       			<td><em>(<?= $Lang['StudentRegistry']['AddressField']['District'] ?>)</em></td>
                     		       			<td colspan="5">
                     		       				<span class="Edit_Hide"><?= $gAddressDistrictEn[$g] ?></span>
                     		       				<span class="Edit_Show" style="display:none"><input type="text" class="textboxtext" name="address_district_en<?=$g?>" value="<?= $gAddressDistrictEn[$g] ?>"/></span>
                     		       			</td>
                        	    		</tr>
                        	    		<tr>
                     		       			<td><em>(<?= $Lang['StudentRegistry']['AddressField']['Area'] ?>)</em></td>
                     		       			<td colspan="5">
                     		       				<span class="Edit_Hide"><?= $gAddressArea[$g] ?></span>
                     		       				<span class="Edit_Show" style="display:none"><?=$guardian_address_area[$g]?></span>
                     		       			</td>
                        	    		</tr>
                        	    	</table>
								</div>
							</td>
                        </tr>
					</tbody>
				</table>
				<? } ?>
				
				<? ########################################################### ?>
				<? #  Contact Persion in case of emergency                   # ?>
				<? ########################################################### ?>
				<div class="form_sub_title_v30">
    				<em>- <span class="field_title"><?= $Lang['StudentRegistry']['ContactPersonInCaseOfEmergency'] ?></span> -</em>
					<p class="spacer"></p>
				</div>
				<table class="form_table_v30">
					<tbody>
						<tr>
                        	<td class="field_title_short"><?= $Lang['StudentRegistry']['1st'] ?></td>
                        	<td>
                        		<span class="Edit_Hide"><?= $contact1_data ?></span>
								<span class="Edit_Show" style="display:none">
									<input type='radio' name='emergency_contact1' id='emergency_contact1_1' value='1' <?=$contact_result['Contact1']==1 ? "checked":""?>> <label for='emergency_contact1_1'><?=$Lang['StudentRegistry']['Father']?></label>  
									<input type='radio' name='emergency_contact1' id='emergency_contact1_2' value='2' <?=$contact_result['Contact1']==2 ? "checked":""?>> <label for='emergency_contact1_2'><?=$Lang['StudentRegistry']['Mother']?></label> 
									<input type='radio' name='emergency_contact1' id='emergency_contact1_0' value='0' <?=$contact_result['Contact1']==0 ? "checked":""?>> <label for='emergency_contact1_0'><?=$Lang['StudentRegistry']['Other']?></label> (<?=$Lang['General']['PlsSpecify']?>: <input type='text' name='emergency_contact1_others' value='<?=$contact_result['Contact1_Others']?>'/>)
								</span>
                        	</td>
                        </tr>
                        <tr>
                        	<td class="field_title_short"><?= $Lang['StudentRegistry']['2nd'] ?></td>
                        	<td>
                        		<span class="Edit_Hide"><?= $contact2_data ?></span>
								<span class="Edit_Show" style="display:none">
									<input type='radio' name='emergency_contact2' id='emergency_contact2_1' value='1' <?=$contact_result['Contact2']==1 ? "checked":""?>> <label for='emergency_contact2_1'><?=$Lang['StudentRegistry']['Father']?></label>  
									<input type='radio' name='emergency_contact2' id='emergency_contact2_2' value='2' <?=$contact_result['Contact2']==2 ? "checked":""?>> <label for='emergency_contact2_2'><?=$Lang['StudentRegistry']['Mother']?></label> 
								</span>
                        	</td>
                        </tr>
                        <tr>
                        	<td class="field_title_short"><?= $Lang['StudentRegistry']['3rd'] ?></td>
                        	<td>
                        		<div class="form_field_sub_content">
                     		       	<table class="inside_form_table">
                     		       		<tr>
                     		       			<td><em><?= $i_UserTitle ?></em></td>
                     		       			<td>
                     		       				<span class="Edit_Hide"><?= $contact3_title_data ?></span>
                     		       				<span class="Edit_Show" style="display:none">
                     		       					<input type="radio" name="emergency_contact3_title" id="emergency_contact3_title1" value="1" <?=$contact_result['Contact3_Title']==1 ? "checked":""?>/> <label for="emergency_contact3_title1"><?=$i_title_mr?></label> 
                     		       					<input type="radio" name="emergency_contact3_title" id="emergency_contact3_title2" value="2" <?=$contact_result['Contact3_Title']==2 ? "checked":""?>/> <label for="emergency_contact3_title2"><?=$i_title_mrs?></label> 
                     		       					<input type="radio" name="emergency_contact3_title" id="emergency_contact3_title3" value="3" <?=$contact_result['Contact3_Title']==3 ? "checked":""?>/> <label for="emergency_contact3_title3"><?=$i_title_ms?></label> 
                     		       				</span>
                     		       			</td>
                     		       			<td><em><?= $Lang['General']['EnglishName'] ?></em></td>
                     		       			<td>
				                            	<span class="Edit_Hide"><?= $contact_result['Contact3_EnglishName'] ?></span>
												<span class="Edit_Show" style="display:none"><input type="text" name="emergency_contact3_english_name" value="<?= $contact_result['Contact3_EnglishName']?>"/></span>
				                            </td>
                     		       			<td><em><?= $Lang['General']['ChineseName'] ?></em></td>
                        	    			<td>
				                            	<span class="Edit_Hide"><?= $contact_result['Contact3_ChineseName'] ?></span>
												<span class="Edit_Show" style="display:none"><input type="text" name="emergency_contact3_chinese_name" value="<?= $contact_result['Contact3_ChineseName']?>"/></span>
				                            </td>
                        	    		</tr>
                        	    		<tr>
                     		       			<td><em><?= $Lang['StudentRegistry']['Relationship'] ?></em></td>
                     		       			<td>
                     		       				<span class="Edit_Hide"><?= $contact_result['Contact3_Relationship'] ?></span>
                     		       				<span class="Edit_Show" style="display:none"><input type="text" name="emergency_contact3_relationship" value="<?= $contact_result['Contact3_Relationship'] ?>"/></span>
                     		       			</td>
                     		       			<td><em><?= $Lang['StudentRegistry']['GuardianDayPhone'] ?></em></td>
                     		       			<td>
				                            	<span class="Edit_Hide"><?= $contact_result['Contact3_Phone'] ?></span>
												<span class="Edit_Show" style="display:none"><input type="text" name="emergency_contact3_phone" value="<?= $contact_result['Contact3_Phone'] ?>"/></span>
				                            </td>
                     		       			<td><em><?= $Lang['StudentRegistry']['Mobile'] ?></em></td>
                        	    			<td>
				                            	<span class="Edit_Hide"><?= $contact_result['Contact3_Mobile'] ?></span>
												<span class="Edit_Show" style="display:none"><input type="text" name="emergency_contact3_mobile" value="<?= $contact_result['Contact3_Mobile'] ?>"/></span>
				                            </td>
                        	    		</tr>
                        	    	</table>
								</div>
                        	</td>
                        </tr>
                  	</tbody>
				</table>
				
				<span id="FormReminder" class="tabletextremark" style="display: none"><?=$i_general_required_field?></span>
				<div id="ErrorLog"></div>
				<div class="edit_bottom">
					<span id="LastModified" class="row_content tabletextremark"><?= $Lang['StudentRegistry']['LastUpdated'] ?> : <?= $result[0]['DateModified']? $result[0]['DateModified']." (".$ModifiedBy.")" : "--"?></span>
                    <p class="spacer"></p>
                    <?= $SaveBtn ?>
                    <?= $CancelBtn ?>
                </div>
			</div>
		</td>
	</tr>
</table>
<input type="hidden" name="studentID" id="studentID" value="<?=$studentID?>">
<input type="hidden" name="AcademicYearID" id="AcademicYearID" value="<?=$AcademicYearID?>">
</form>
<script language="javascript">
	document.getElementById('SaveBtn').style.visibility = "hidden";
	document.getElementById('CancelBtn').style.visibility = "hidden";
	<? if($EditRecord) {?>
	$('#EditBtn').click();	
	<? }?>
</script>
<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>