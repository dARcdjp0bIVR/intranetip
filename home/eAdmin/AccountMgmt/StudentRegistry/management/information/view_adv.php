<?php
#using: henry chow

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccessright.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$laccessright = new libaccessright();

if(!$plugin['AccountMgmt_StudentRegistry'] || (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-View") && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-Manage"))) {
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$lsr = new libstudentregistry();
$linterface = new interface_html();

#Student's Info
if(sizeof($error)>0) {
	//hdebug_pr($_POST);
	$stdInfo = $lsr->getStudentClassInfo($userID);
	$result[0]['YearName'] = $stdInfo['YearName'];
	$result[0]['ClassName'] = $stdInfo['className'];
	$result[0]['ClassNumber'] = $stdInfo['ClassNumber'];
	$result[0]['YearClassID'] = $stdInfo['YearClassID'];
	$result[0]['STUD_ID'] = $STUD_ID;
	$result[0]['CODE'] = $CODE;
	$result[0]['ChineseName'] = stripslashes($NAME_C);
	$result[0]['EnglishName'] = stripslashes($NAME_E);
	$result[0]['Gender'] = $SEX;
	$result[0]['DateOfBirth'] = $B_DATE;
	$result[0]['B_PLACE'] = $B_PLACE;
	$result[0]['HomeTelNo'] = $TEL;
	$result[0]['ADMISSION_DATE'] = $ENTRY_DATE;
	$result[0]['S_CODE'] = $S_CODE;
	$result[0]['ID_TYPE'] = $ID_TYPE;
	$result[0]['ID_NO'] = $ID_NO;
	$result[0]['I_PLACE'] = $I_PLACE;
	$result[0]['I_DATE'] = $I_DATE;
	$result[0]['V_DATE'] = $V_DATE;
	$result[0]['S6_TYPE'] = $S6_TYPE;
	$result[0]['S6_IDATE'] = $S6_IDATE;
	$result[0]['S6_VDATE'] = $S6_VDATE;
	$result[0]['NATION'] = $NATION;
	$result[0]['ORIGIN'] = stripslashes($ORIGIN);
	$result[0]['RELIGION'] = $RELIGION;
	$result[0]['R_AREA'] = $R_AREA;
	$result[0]['RA_DESC'] = stripslashes($RA_DESC);
	$result[0]['AREA'] = $AREA;
	$result[0]['ROAD'] = stripslashes($ROAD);
	$result[0]['ADDRESS'] = stripslashes($ADDRESS);
	
	$Father[0]['NAME_C'] = stripslashes($FATHER_C);
	$Father[0]['NAME_E'] = stripslashes($FATHER_E);
	$Father[0]['PROF'] = stripslashes($F_PROF);
	$Father[0]['MARITAL_STATUS'] = $F_MARTIAL;
	$Father[0]['TEL'] = $F_TEL;
	$Father[0]['MOBILE'] = $F_MOBILE;
	$Father[0]['OFFICE_TEL'] = $F_OFFICE;
	$Father[0]['EMAIL'] = $F_EMAIL;
	
	$Mother[0]['NAME_C'] = stripslashes($MOTHER_C);
	$Mother[0]['NAME_E'] = stripslashes($MOTHER_E);
	$Mother[0]['PROF'] = stripslashes($M_PROF);
	$Mother[0]['MARITAL_STATUS'] = $M_MARTIAL;
	$Mother[0]['TEL'] = $M_TEL;
	$Mother[0]['MOBILE'] = $M_MOBILE;
	$Mother[0]['OFFICE_TEL'] = $M_OFFICE;
	$Mother[0]['EMAIL'] = $M_EMAIL;
	
	$Guard[0]['NAME_C'] = stripslashes($GUARDIAN_C);
	$Guard[0]['NAME_E'] = stripslashes($GUARDIAN_E);
	$Guard[0]['SEX'] = $G_SEX;
	$Guard[0]['PROF'] = stripslashes($G_PROF);
	$Guard[0]['GUARD'] = $GUARD;
	$Guard[0]['G_RELATION'] = stripslashes($G_REL_OTHERS);
	$Guard[0]['LIVE_SAME'] = $LIVE_SAME;
	$Guard[0]['G_AREA'] = $G_AREA;
	$Guard[0]['G_ROAD'] = stripslashes($G_ROAD);
	$Guard[0]['G_ADDRESS'] = stripslashes($G_ADDRESS);
	$Guard[0]['TEL'] = $G_TEL;
	$Guard[0]['MOBILE'] = $G_MOBILE;
	$Guard[0]['OFFICE_TEL'] = $G_OFFICE;
	$Guard[0]['EMAIL'] = $G_EMAIL;
	
	$Emergency[0]['NAME_C'] = stripslashes($EC_NAME_C);
	$Emergency[0]['NAME_E'] = stripslashes($EC_NAME_E);
	$Emergency[0]['GUARD'] = $EC_REL;
	$Emergency[0]['TEL'] = $EC_TEL;
	$Emergency[0]['MOBILE'] = $EC_MOBILE;
	$Emergency[0]['G_AREA'] = $EC_AREA;
	$Emergency[0]['G_ROAD'] = stripslashes($EC_ROAD);
	$Emergency[0]['G_ADDRESS'] = stripslashes($EC_ADDRESS);
} else {
	$result = $lsr -> RETRIEVE_STUDENT_INFO_ADV_MACAU($userID);
	$Father = $lsr -> RETRIEVE_GUARDIAN_INFO_ADV_MACAU($userID, 'F');
	$Mother = $lsr -> RETRIEVE_GUARDIAN_INFO_ADV_MACAU($userID, 'M');
	$Guard = $lsr -> RETRIEVE_GUARDIAN_INFO_ADV_MACAU($userID, 'G');
	$Emergency = $lsr -> RETRIEVE_GUARDIAN_INFO_ADV_MACAU($userID, 'E');
}

#Gender related info
$gender_ary      = $lsr -> GENDER_ARY();
$gender_val      = $result[0]['Gender']? $lsr -> RETRIEVE_DATA_ARY($gender_ary, $result[0]['Gender']) : "--";
$gender_edit_ary = $lsr -> RETRIEVE_DATA_ARY($gender_ary);
$gender_RBL_html = $lsr -> GENERATE_RADIO_BTN_LIST($gender_edit_ary, "Gender", $gender_val);

#Nationality related info
$nation_ary      = $lsr -> MACAU_NATION_DATA_ARY();
$nation_val      = $result[0]['NATION']? $lsr -> RETRIEVE_DATA_ARY($nation_ary, $result[0]['NATION']) : "--";
$nation_edit_ary = $lsr -> RETRIEVE_DATA_ARY($nation_ary);
$nation_DDL_html = $lsr -> GENERATE_DROP_DWON_LIST($nation_edit_ary, "Nationality", $nation_val);

#religion related info
$religion_ary      = $lsr -> MACAU_RELIGION_DATA_ARY();
$religion_val      = $result[0]['RELIGION']? $lsr -> RETRIEVE_DATA_ARY($religion_ary, $result[0]['RELIGION']) : "--";
$religion_edit_ary = $lsr -> RETRIEVE_DATA_ARY($religion_ary);
$religion_DDL_html = $lsr -> GENERATE_DROP_DWON_LIST($religion_edit_ary, "Religion", $religion_val);

#BirthPlace related info
$BPlace_ary      = $lsr -> MACAU_B_PLACE_DATA_ARY();
$BPlace_val      = $result[0]['B_PLACE']? $lsr -> RETRIEVE_DATA_ARY($BPlace_ary, $result[0]['B_PLACE']) : "--";
$BPlace_edit_ary = $lsr -> RETRIEVE_DATA_ARY($BPlace_ary);
$BPlace_DDL_html = $lsr -> GENERATE_DROP_DWON_LIST($BPlace_edit_ary, "BirthPlace", $BPlace_val);

#School code related info
$schoolcode_ary  	 = $lsr -> MACAU_S_CODE_DATA_ARY();
$schoolcode_val	 	 = $result[0]['S_CODE']? $lsr -> RETRIEVE_DATA_ARY($schoolcode_ary, $result[0]['S_CODE']) : "--";
$schoolcode_edit_ary = $lsr -> RETRIEVE_DATA_ARY($schoolcode_ary);
$schoolcode_DDL_html = $lsr -> GENERATE_DROP_DWON_LIST($schoolcode_edit_ary, "SchoolCode", $schoolcode_val);

#ID Card related info
$IdType_ary 	 = $lsr -> MACAU_ID_TYPE_DATA_ARY();
$IdType_val 	 = $result[0]['ID_TYPE']? $lsr -> RETRIEVE_DATA_ARY($IdType_ary, $result[0]['ID_TYPE']) : "--";
$IdType_edit_ary = $lsr -> RETRIEVE_DATA_ARY($IdType_ary);
$IdType_DDL_html = $lsr -> GENERATE_DROP_DWON_LIST($IdType_edit_ary, "IdType", $IdType_val);

$IdPlace_ary 	  = $lsr -> MACAU_ID_PLACE_DATA_ARY();
$IdPlace_val 	  = $result[0]['I_PLACE']? $lsr -> RETRIEVE_DATA_ARY($IdPlace_ary, $result[0]['I_PLACE']) : "--";
$IdPlace_edit_ary = $lsr -> RETRIEVE_DATA_ARY($IdPlace_ary);
$IdPlace_DDL_html = $lsr -> GENERATE_DROP_DWON_LIST($IdPlace_edit_ary, "IdPlace", $IdPlace_val);

#Stay Permission related info
$StayType_ary 	   = $lsr -> MACAU_S6_TYPE_DATA_ARY();
$StayType_val 	   = $result[0]['S6_TYPE']? $lsr -> RETRIEVE_DATA_ARY($StayType_ary, $result[0]['S6_TYPE']) : "--";
$StayType_edit_ary = $lsr -> RETRIEVE_DATA_ARY($StayType_ary);
$StayType_onchange = "onchange=\"javascript:changeStayType()\"";
$StayType_DDL_html = $lsr -> GENERATE_DROP_DWON_LIST($StayType_edit_ary, "StayType", $StayType_val, $StayType_onchange);

#Residential related info
$ResArea_ary 	  = $lsr -> MACAU_R_AREA_DATA_ARY();
$ResArea_val 	  = $result[0]['R_AREA']? $lsr -> RETRIEVE_DATA_ARY($ResArea_ary, $result[0]['R_AREA']) : "--";
$ResArea_edit_ary = $lsr -> RETRIEVE_DATA_ARY($ResArea_ary);
$ResArea_DDL_html = $lsr -> GENERATE_DROP_DWON_LIST($ResArea_edit_ary, "ResArea", $ResArea_val);

#Address related info
$AddArea_ary 	  = $lsr -> MACAU_AREA_DATA_ARY();
$AddArea_val 	  = $result[0]['AREA']? $lsr -> RETRIEVE_DATA_ARY($AddArea_ary, $result[0]['AREA']) : "--";
$AddArea_edit_ary = $lsr -> RETRIEVE_DATA_ARY($AddArea_ary);
$AddArea_DDL_html = $lsr -> GENERATE_DROP_DWON_LIST($AddArea_edit_ary, "AddArea", $AddArea_val);

# Registry Status
switch($result[0]['RecordStatus']) {
	case 0: $ownRegistryStatus = $Lang['StudentRegistry']['StatusSuspended'];
			break;
	case 1: $ownRegistryStatus = $Lang['StudentRegistry']['StatusApproved'];
			break;
	case 2: $ownRegistryStatus = $Lang['StudentRegistry']['StatusSuspended'];
			break;
	case 3: $ownRegistryStatus = $Lang['StudentRegistry']['StatusLeft'];
			break;
	default: $ownRegistryStatus = "---";	
}

//debug_pr($result[0]['Gender']);
//debug_pr($gender_DDL_html);

#Common Array (For Father, Mother, Guardian, Emergency Contact Person)
$Marry_ary = $lsr -> MACAU_MARITAL_STATUS_DATA_ARY();
$Marry_edit_ary = $lsr -> RETRIEVE_DATA_ARY($Marry_ary);
$Relation_ary = $lsr -> MACAU_GUARD_DATA_ARY();
$Relation_edit_ary = $lsr -> RETRIEVE_DATA_ARY($Relation_ary);

#Father's Info
	//$Father = $lsr -> RETRIEVE_GUARDIAN_INFO_ADV_MACAU($userID, 'F');
	#Father's Marital Status Together info
	$FatherMarry_val 	  = $Father[0]['MARITAL_STATUS']? $lsr -> RETRIEVE_DATA_ARY($Marry_ary, $Father[0]['MARITAL_STATUS']) : "--";
	$FatherMarry_DDL_html = $lsr -> GENERATE_DROP_DWON_LIST($Marry_edit_ary, "FatherMarry", $FatherMarry_val);

#Mother's Info
	//$Mother = $lsr -> RETRIEVE_GUARDIAN_INFO_ADV_MACAU($userID, 'M');
	#Mother's Marital Status info
	$MotherMarry_val 	  = $Mother[0]['MARITAL_STATUS']? $lsr -> RETRIEVE_DATA_ARY($Marry_ary, $Mother[0]['MARITAL_STATUS']) : "--";
	$MotherMarry_DDL_html = $lsr -> GENERATE_DROP_DWON_LIST($Marry_edit_ary, "MotherMarry", $MotherMarry_val);

#Guardian's Info
	//$Guard = $lsr -> RETRIEVE_GUARDIAN_INFO_ADV_MACAU($userID, 'G');
	#Guardian's Gender info
	$GuardGender_val	  = ($Guard[0]['GUARD'] == 'O' && $Guard[0]['G_GENDER'])? $lsr -> RETRIEVE_DATA_ARY($gender_ary, $Guard[0]['G_GENDER']) : "--";
	$GuardGender_RBL_html = $lsr -> GENERATE_RADIO_BTN_LIST($gender_edit_ary, "GuardGender", $GuardGender_val);

	#Guardian's Marital Status info
	$GuardMarry_val 	 = ($Guard[0]['GUARD'] == 'O' && $Guard[0]['MARITAL_STATUS'])? $lsr -> RETRIEVE_DATA_ARY($Marry_ary, $Guard[0]['MARITAL_STATUS']) : "--";
	$GuardMarry_DDL_html = $lsr -> GENERATE_DROP_DWON_LIST($Marry_edit_ary, "GuardMarry", $GuardMarry_val);
	
	#Guardian's Relation with Student info
	$GuardRelation_val		= $Guard[0]['GUARD']? $lsr -> RETRIEVE_DATA_ARY($Relation_ary, $Guard[0]['GUARD']) : $lsr -> RETRIEVE_DATA_ARY($Relation_ary, 'O');
	$GuardRelation_RBL_html = $lsr -> GENERATE_RADIO_BTN_LIST($Relation_edit_ary, "GuardRelation", $GuardRelation_val);

	#Guardian's Stay with Student info
	$GuardStay_ary 		= $lsr -> MACAU_LIVE_SAME_ARY();
	$GuardStay_val 		= ($Guard[0]['GUARD'] == 'O' && $Guard[0]['LIVE_SAME'])? $lsr -> RETRIEVE_DATA_ARY($GuardStay_ary, $Guard[0]['LIVE_SAME']) : "--";
	$GuardStay_edit_ary = $lsr -> RETRIEVE_DATA_ARY($GuardStay_ary);
	$GuardStay_RBL_html = $lsr -> GENERATE_RADIO_BTN_LIST($GuardStay_edit_ary, "GuardStay", $GuardStay_val);

	#Guardian's Address related info
	$GuardArea_val 	    = ($Guard[0]['GUARD'] == 'O' && $Guard[0]['G_AREA'])? $lsr -> RETRIEVE_DATA_ARY($AddArea_ary, $Guard[0]['G_AREA']) : "--";
	$GuardArea_DDL_html = $lsr -> GENERATE_DROP_DWON_LIST($AddArea_edit_ary, "GuardArea", $GuardArea_val);

#Emergency Contact Person's Info
	//$Emergency = $lsr -> RETRIEVE_GUARDIAN_INFO_ADV_MACAU($userID, 'E');
	#Emergency Relation with Student info
	$EmergencyRelation_val		= $Emergency[0]['GUARD']? $lsr -> RETRIEVE_DATA_ARY($Relation_ary, $Emergency[0]['GUARD']) : $lsr -> RETRIEVE_DATA_ARY($Relation_ary, 'O');
	$EmergencyRelation_RBL_html = $lsr -> GENERATE_RADIO_BTN_LIST($Relation_edit_ary, "EmergencyRelation", $EmergencyRelation_val);

	#Emergency Address related info
	$EmergencyArea_val 	    = $Emergency[0]['G_AREA']? $lsr -> RETRIEVE_DATA_ARY($AddArea_ary, $Emergency[0]['G_AREA']) : "--";
	$EmergencyArea_DDL_html = $lsr -> GENERATE_DROP_DWON_LIST($AddArea_edit_ary, "EmergencyArea", $EmergencyArea_val);

//debug_pr($FatherLive_val);
//debug_pr($FatherMarry_ary);
//debug_pr($Father[0]['MARITAL_STATUS']);

#Past Enrollment Information
$Past_Enroll = $lsr -> RETRIEVE_STUDENT_PAST_ENROLLMENT_RECORD($userID);

if($Past_Enroll)
{
	$Past_Enroll_html = "";
	for($i=0;$i<sizeof($Past_Enroll);$i++)
	{
		$Past_Enroll_html .= "<tr>
        						<td><em>(".$Lang['StudentRegistry']['AcademicYear'].")</em></td>
								<td><span class=\"row_content\">".$Past_Enroll[$i]['YearName']."</span></td>
        						<td><em>(".$Lang['StudentRegistry']['Grade'].")</em></td>
								<td><span class=\"row_content\">".$Past_Enroll[$i]['Grade']."</span></td>								
								<td><em>(".$Lang['StudentRegistry']['Class'].")</em></td>
								<td><span class=\"row_content\">".$Past_Enroll[$i]['Class']."</span></td>
								<td><em>(".$Lang['StudentRegistry']['InClassNumber'].")</em></td>
								<td><span class=\"row_content\">".$Past_Enroll[$i]['ClassNumber']."</span></td>
                        	 </tr>";
	}
}

#Modified By
$ModifiedBy = $result[0]['ModifyBy']? $lsr -> RETRIEVE_MODIFIED_BY_INFO($result[0]['ModifyBy']) : "";

if(sizeof($error)>0) {
	$err_msg = "<div align='left'>\n<ul>";
	foreach($error as $key=>$val) {
		$err_msg .= "<font color='red'><li>$val</li></font>";
	}
    $err_msg .= "</ul>\n</div>";
} else {
	$err_msg = "<p class=\"spacer\"></p>";	
}
$CurrentPageArr['StudentRegistry'] = 1;
$CurrentPage = "Mgmt_RegistryInfo";

$TAGS_OBJ[] = array($Lang['Menu']['AccountMgmt']['StudentRegistryInfo'], "index.php");

$MODULE_OBJ['title'] = $Lang['Header']['Menu']['StudentRegistry'];

$MODULE_OBJ = $lsr->GET_MODULE_OBJ_ARR();

# navigation bar
# need to modified here!
$PAGE_NAVIGATION[] = array($Lang['StudentRegistry']['ClassList'], "index.php?AcademicYearID=$AcademicYearID");
$PAGE_NAVIGATION[] = array($result[0]['ClassName']." ".$Lang['StudentRegistry']['StudentList'], "class.php?AcademicYearID=$AcademicYearID&targetClass=::".$result[0]['YearClassID']);
$PAGE_NAVIGATION[] = array(($intranet_session_language == "en"? $result[0]['EnglishName'] : $result[0]['ChineseName']), "");

#Content Top Tool Button
$Content_Top_BTN[] = array($Lang['StudentRegistry']['AdvInfo'], "view_adv.php?AcademicYearID=AcademicYearID=$AcademicYearID&userID=$userID", "", true);
$Content_Top_BTN[] = array($Lang['StudentRegistry']['BasicInfo'], "view.php?AcademicYearID=$AcademicYearID&view.php?userID=$userID", "", false);

if($xmsg==1) $msg = $Lang['General']['ReturnMessage']['UpdateSuccess'];
$linterface->LAYOUT_START(urldecode($msg));

?>
<script language="javascript">
	function EditInfo()
	{
		$('#LastModified').attr('style', 'display: none');
		$('#EditBtn').attr('style', 'visibility: hidden');
		
		$('.Edit_Hide').attr('style', 'display: none');
		$('.Edit_Show').attr('style', '');
		
		$('.tabletextrequire').attr('style', '');
		$('#SaveBtn').attr('style', '');
		$('#CancelBtn').attr('style', '');
		$('#FormReminder').attr('style', '');
		
		$(':text').attr('style', '');
		$(':text').attr('readonly', '');
		$(':text[value="--"]').val('');
		
		$('.GuardRelationInfo').attr('style', '');
		$('#GuardNoInfo').attr('style', 'display: none');
		changeRelation('G');

		$('.EmergencyRelationInfo').attr('style', '');
		$('#EmergencyNoInfo').attr('style', 'display: none');
		changeRelation('E');
	}
	
	function ResetInfo()
	{
		document.form1.reset();
		$('#LastModified').attr('style', '');
		$('#EditBtn').attr('style', '');
		
		$('.Edit_Hide').attr('style', '');
		$('.Edit_Show').attr('style', 'display: none');
		
		$('.tabletextrequire').attr('style', 'display: none');
		$('#SaveBtn').attr('style', 'display: none');
		$('#CancelBtn').attr('style', 'display: none');
		$('#FormReminder').attr('style', 'display: none');
		
		$(':text').attr('style', 'border: 0px');
		$(':text').attr('readonly', 'readonly');
		$(':text[value=""]').val('--');
		
		$('.GuardRelationInfo').attr('style', '<?= $Guard? "" : "Display:none"?>');
		$('#GuardNoInfo').attr('style', '<?= $Guard? "Display:none" : ""?>');
		$('.GuardInfo').attr('style', '<?= $Guard[0]['GUARD'] == 'O'? "" : "Display:none"?>');

		$('.EmergencyRelationInfo').attr('style', '<?= $Emergency? "" : "Display:none"?>');
		$('#EmergencyNoInfo').attr('style', '<?= $Emergency? "Display:none" : ""?>');
		$('.EmergencyInfo').attr('style', '<?= $Emergency[0]['GUARD'] == 'O'? "" : "Display:none"?>');
	}
	
	function Hide_Table(table_name, hide_btn_name, show_btn_name)
	{
		$('#'+ table_name).slideUp();
		$('#'+ hide_btn_name).attr('style', 'display:none');
		$('#'+ show_btn_name).attr('style', '');
	}
	
	function Show_Table(table_name, hide_btn_name, show_btn_name)
	{
		$('#'+ table_name).slideDown();
		$('#'+ hide_btn_name).attr('style', 'display:none');
		$('#'+ show_btn_name).attr('style', '');
	}
	
	function changeStayType()
	{
		var select_type = $('[name="StayType"]').val();
		if(select_type == '1')
			$('.Stay_V_Date').attr('style', 'display:none');
		else
			$('.Stay_V_Date').attr('style', '');
	}
	
	function changeRelation(type)
	{
		if(type == 'G')
		{
			var Relation_RBL = "GuardRelation";
			var TableClass = "GuardInfo";
		}
		else if(type == 'E')
		{
			var Relation_RBL = "EmergencyRelation";
			var TableClass = "EmergencyInfo";
		}
				
		var RelationInfo = $('[name="'+Relation_RBL+'"]:checked').val();
		if(RelationInfo == 'O')
			$('.'+TableClass).attr('style', '');
		else
			$('.'+TableClass).attr('style', 'display:none');
	}
	
	function show_arg()
	{
		//var abc = $('[name="form1"]').serialize();
		//alert(abc);
		document.form1.action = "view_adv_update.php";
		document.form1.submit();
	}
	

	$(document).ready(function()
	{
		<? if(sizeof($error)==0) { ?>
			$(':text[value=""]').val('--');
		<? } ?>
		
		$('[name="GuardRelation"]').click(function(){
    		changeRelation('G');
		});
		
		$('[name="EmergencyRelation"]').click(function(){
    		changeRelation('E');
		});
	});
	
	
</script>
<form name="form1" method="POST" action="">
<table width="95%" border="0" cellspacing="0" cellpadding="0">
	<tr> 
		<td class="main_content">
			<div class="navigation">
				<?=$linterface->GET_NAVIGATION($PAGE_NAVIGATION)?>
				<p class="spacer"></p>
			</div>
			<p class="spacer"></p>
			<div class="content_top_tool">
				<?= $linterface->GET_CONTENT_TOP_BTN($Content_Top_BTN)?>
			</div>
			<div class="table_board">
        		<div class="table_row_tool row_content_tool">
                	<?= $linterface->GET_SMALL_BTN($Lang['StudentRegistry']['Edit'], "button", "javascript:EditInfo();", "EditBtn") ?>
                </div>
                
        		<?=$err_msg?>				
                <div class="form_sub_title">
                	<em>- <span class="field_title"><?= $Lang['StudentRegistry']['StudInfo'] ?></span> -</em>
                	<a id="Stud_Hide_Btn" href="javascript:void(0)" onclick="javascript:Hide_Table('StudentInfo_Table', 'Stud_Hide_Btn', 'Stud_Show_Btn');"><?= $Lang['StudentRegistry']['Hide'] ?></a>
                    <a id="Stud_Show_Btn" href="javascript:void(0)" onclick="javascript:Show_Table('StudentInfo_Table', 'Stud_Show_Btn', 'Stud_Hide_Btn');" style="display:none"><?= $Lang['StudentRegistry']['Show'] ?></a>
                    <p class="spacer"></p>
                </div>
                <table id="StudentInfo_Table" class="form_table">
                	<col class="field_title_short">
                    <col class="field_content_short">
                    <col class="field_title_short">
                    <col class="field_content_short">
                    <col class="field_title_short">
                    <col class="field_content_short">
					<tbody>
						<tr>
                        	<td class="field_title_short"><span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['StudentID'] ?></td>
                        	<td><input class="textboxnum" type="text" name="StudentID" value="<?= $result[0]['STUD_ID']?>" readonly="readonly" style="border: 0px"/></td>
                        	<td class="field_title_short"><span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['DSEJ_Number'] ?></td>
                        	<td><input class="textboxnum" type="text" name="DSEJ_Number" value="<?= $result[0]['CODE']?>" readonly="readonly" style="border: 0px"/></td>
                        	<td>&nbsp;</td>
                        	<td>&nbsp;</td>
						</tr>
                        <tr>
                            <td class="field_title_short"><span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['ChineseName'] ?></td>
                            <td><input class="textboxtext" type="text" name="ChineseName" value="<?= $result[0]['ChineseName']?>" readonly="readonly" style="border: 0px"/></td>
                            <td class="field_title_short"><span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['ForeignName'] ?></td>
                            <td><input class="textboxtext" type="text" name="EnglishName" value="<?= $result[0]['EnglishName']?>" readonly="readonly" style="border: 0px"/></td>
                        	<td>&nbsp;</td>
                        	<td>&nbsp;</td>
                        </tr>
						<tr>
                            <td class="field_title_short"><span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['Gender'] ?></td>
                            <td><span class="Edit_Hide"><?= $gender_val ?></span><?= $gender_RBL_html ?></td>
                            <td class="field_title_short"><span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['BirthDate']?></span></td>
                        	<td><input class="textboxnum" type="text" name="BirthDate" value="<?= date("Y-m-d", strtotime($result[0]['DateOfBirth']))?>" readonly="readonly" style="border: 0px"/><span class="Edit_Show" style="display: none"> (yyyy-mm-dd) </span></td>
                            <td class="field_title_short"><span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['BirthPlace']?></span></td>
                            <td><span class="Edit_Hide"><?= $BPlace_val ?></span><?= $BPlace_DDL_html ?></td>
                        </tr>
                        <tr>
                            <td class="field_title_short"><span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['Nationality']?></td>
                            <td><span class="Edit_Hide"><?= $nation_val ?></span><?= $nation_DDL_html ?></td>                      
                        	<td class="field_title_short"><span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['Province']?></td>
                        	<td><input class="textboxnum" type="text" name="Province" value="<?= $result[0]['ORIGIN']?>" readonly="readonly" style="border: 0px"/></td>
                            <td class="field_title_short"><?= $Lang['StudentRegistry']['Religion']?></td>
                            <td><span class="Edit_Hide"><?= $religion_val ?></span><?= $religion_DDL_html ?></td>
                        </tr>
                        <tr>
                        	<td class="field_title_short"><?= $Lang['StudentRegistry']['StudyAt'] ?></td>
                        	<td colspan="5">
                        		<div class="form_field_sub_content">
                        			<table>
                        				<tr>
                  	  	    				<td><em>(<span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['SchoolCode'] ?>)</em></td>
                  		      				<td><span class="row_content"><span class="Edit_Hide"><?= $schoolcode_val ?></span><?= $schoolcode_DDL_html ?></span></td>
                     		       		</tr>
                     		       	</table>
                     		       	<table>
                     		       		<tr>
                     		       			<td><em>(<?= $Lang['StudentRegistry']['Grade'] ?>)</em></td>
                     		       			<td><span class="row_content"><?= $result[0]['YearName']?></span></td>
                     		       			<td><em>(<?= $Lang['StudentRegistry']['Class'] ?>)</em></td>
                     		       			<td><span class="row_content"><?= $result[0]['ClassName']?></span></td>
                     		       			<td><em>(<?= $Lang['StudentRegistry']['InClassNumber'] ?>)</em></td>
                        	    			<td><span class="row_content"><?= $result[0]['ClassNumber']?></span></td>
                        	    		</tr>
                        	    	</table>
                        	    	<table>
                        	    			<td><em>(<?= $Lang['StudentRegistry']['EntryDate'] ?>)</em></td>
                  		      				<td colspan="7"><span class="row_content"><input class="textboxnum" type="text" name="EntryDate" value="<?= date("Y-m-d", strtotime($result[0]['ADMISSION_DATE']))?>" readonly="readonly" style="border: 0px"/><span class="Edit_Show" style="display: none"> (yyyy-mm-dd) </span></span></td>
                        	    	</table>
								</div>
                            </td>
                        </tr>
                        <tr>
                        	<td class="field_title_short"><span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['IdCard'] ?></td>
                        	<td colspan="5">
                        		<div class="form_field_sub_content">
                        			<table>
                        				<tr>
                  	  	    				<td><em>(<?= $Lang['StudentRegistry']['CardType'] ?>)</em></td>
                  		      				<td><span class="row_content"><span class="Edit_Hide"><?= $IdType_val ?></span><?= $IdType_DDL_html ?></span></td>
                     		       			<td><em>(<?= $Lang['StudentRegistry']['CardNumber'] ?>)</em></td>
                     		       			<td><input class="row_content textboxnum" type="text" name="IdCardNumber" value="<?= $result[0]['ID_NO']?>" readonly="readonly" style="border: 0px"/></td>
                     		       			<td><em>(<?= $Lang['StudentRegistry']['CardIssuePlace'] ?>)</em></td>
                     		       			<td><span class="row_content Edit_Hide"><?= $IdPlace_val ?></span><?= $IdPlace_DDL_html ?></td>
                     		       		</tr>
                     		       	</table>
                     		       	<table>
                     		       		<tr>
                     		       			<td><em>(<?= $Lang['StudentRegistry']['CardIssueDate'] ?>)</em></td>
                        	    			<td><span class="row_content"><input class="textboxnum" type="text" name="IdIssueDate" value="<?= date("Y-m-d", strtotime($result[0]['I_DATE']))?>" readonly="readonly" style="border: 0px"/><span class="Edit_Show" style="display: none"> (yyyy-mm-dd) </span></span></td>
                        	    			<td><em>(<?= $Lang['StudentRegistry']['CardValidDate'] ?>)</em></td>
                        	    			<td><span class="row_content"><input class="textboxnum" type="text" name="IdValidDate" value="<?= date("Y-m-d", strtotime($result[0]['V_DATE']))?>" readonly="readonly" style="border: 0px"/><span class="Edit_Show" style="display: none"> (yyyy-mm-dd) </span></span></td>
                        	    		</tr>
                        	    	</table>
								</div>
							</td>
                        </tr>
                        <tr>
                        	<td class="field_title_short"><span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['StayPermission'] ?></td>
                        	<td colspan="5">
                        		<div class="form_field_sub_content">
                        			<table>
                        				<tr>
                  	  	    				<td><em>(<?= $Lang['StudentRegistry']['CardType'] ?>)</em></td>
                  		      				<td><span class="row_content"><span class="Edit_Hide"><?= $StayType_val ?></span><?= $StayType_DDL_html ?></span></td>
                     		       			<td><em>(<?= $Lang['StudentRegistry']['CardIssueDate'] ?>)</em></td>
                        	    			<td><span class="row_content"><input class="textboxnum" type="text" name="StayIssueDate" value="<?= date("Y-m-d", strtotime($result[0]['S6_IDATE']))?>" readonly="readonly" style="border: 0px"/><span class="Edit_Show" style="display: none"> (yyyy-mm-dd) </span></span></td>
                        	    			<td class="Stay_V_Date" style="<?= ($result[0]['S6_TYPE'] == '1' || $result[0]['S6_TYPE']=='')? "display: none" : "" ?>"><em>(<?= $Lang['StudentRegistry']['CardValidDate'] ?>)</em></td>
                        	    			<td class="Stay_V_Date" style="<?= ($result[0]['S6_TYPE'] == '1' || $result[0]['S6_TYPE']=='')? "display: none" : "" ?>"><span class="row_content"><input class="textboxnum" type="text" name="StayValidDate" value="<?= date("Y-m-d", strtotime($result[0]['S6_VDATE']))?>" readonly="readonly" style="border: 0px"/><span class="Edit_Show" style="display: none"> (yyyy-mm-dd) </span></span></td>
                        	    		</tr>
                        	    	</table>
								</div>
							</td>
                        </tr>
						<tr>
                            <td class="field_title_short"><span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['HomePhone'] ?></td>
                            <td colspan="5"><input class="textboxnum" type="text" name="HomePhone" value="<?= $result[0]['HomeTelNo']?>" readonly="readonly" style="border: 0px"/></td>
                        </tr>
                        <tr>
                            <td class="field_title_short"><?= $Lang['StudentRegistry']['NightRes'] ?></td>
                        	<td colspan="5">
                        		<div class="form_field_sub_content">
                        			<table>
                        				<tr>
                  	  	    				<td><em>(<span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['Area'] ?>)</em></td>
                  		      				<td><span class="row_content Edit_Hide"><?= $ResArea_val ?></span><?= $ResArea_DDL_html ?></td>
                     		       			<td><em>(<?= $Lang['StudentRegistry']['DistrictName'] ?>)</em></td>
                        	    			<td><input class="textbox_name" type="text" name="DistrictName" value="<?= $result[0]['RA_DESC']?>" readonly="readonly" style="border: 0px"/></td>
                         	    		</tr>
                        	    	</table>
                        		</div>
                        	</td>
                        </tr>
                        <tr>
                            <td class="field_title_short"><span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['Address'] ?></td>
                        	<td colspan="5">
                        		<div class="form_field_sub_content">
                        			<table>
                        				<tr>
                  	  	    				<td><em>(<?= $Lang['StudentRegistry']['HomeDistrict'] ?>)</em></td>
                  		      				<td><span class="row_content Edit_Hide"><?= $AddArea_val ?></span><?= $AddArea_DDL_html ?></td>
                     		       		</tr>
                     		       	</table>
                     		       	<table>
                     		       		<tr>
                     		       			<td><em>(<?= $Lang['StudentRegistry']['Street'] ?>)</em></td>
                        	    			<td><span class="row_content"><input class="textbox_name" type="text" name="Street" value="<?= $result[0]['ROAD']?>" readonly="readonly" style="border: 0px"/></span></td>
                         	    		</tr>
                         	    	</table>
                         	    	<table>
                         	    		<tr>    
                         	    		    <td><em>(<?= $Lang['StudentRegistry']['AddDetail'] ?>)</em></td>
                        	    			<td><span class="row_content"><input class="textbox_name" type="text" name="AddDetail" value="<?= $result[0]['ADDRESS']?>" readonly="readonly" style="border: 0px"/></span></td>
                         	    		</tr>
                        	    	</table>
                        		</div>
                        	</td>
                        </tr>
                        <tr>
                        	<td class="field_title_short"><?= $Lang['StudentRegistry']['PastEnroll'] ?></td>
                        	<td colspan="5">
                        		<div class="form_field_sub_content">
                        			<table> 
										<?= $Past_Enroll_html ?>
                        			</table>
                        		</div>
                        	</td>
                        </tr>
                        <tr>
                        	<td class="field_title_short"><?= $Lang['StudentRegistry']['RegistryStatus'] ?></td>
                        	<td colspan="5">
                        		<?=$ownRegistryStatus?>
                        	</td>
                        </tr>
                        <tr>
							<td class="field_title_short" style="visibility: hidden">&nbsp;</td>
							<td style="visibility: hidden">&nbsp;</td>
							<td class="field_title_short" style="visibility: hidden">&nbsp;</td>
							<td style="visibility: hidden">&nbsp;</td>
							<td class="field_title_short" style="visibility: hidden">&nbsp;</td>
							<td style="visibility: hidden">&nbsp;</td>
						</tr>
					</tbody>
					<col class="field_title_short">
                    <col class="field_c">
				</table>
    			<div class="form_sub_title">
    				<em>- <span class="field_title"><?= $Lang['StudentRegistry']['FatherInfo'] ?></span> -</em>
					<a id="Fath_Hide_Btn" href="javascript:void(0)" onclick="javascript:Hide_Table('FatherInfo_Table', 'Fath_Hide_Btn', 'Fath_Show_Btn');"><?= $Lang['StudentRegistry']['Hide'] ?></a>
                    <a id="Fath_Show_Btn" href="javascript:void(0)" onclick="javascript:Show_Table('FatherInfo_Table', 'Fath_Show_Btn', 'Fath_Hide_Btn');" style="display:none"><?= $Lang['StudentRegistry']['Show'] ?></a>
					<p class="spacer"></p>
				</div>
        		<table id="FatherInfo_Table" class="form_table">
                	<col class="field_title_short">
                	<col class="field_content_short">
                	<col class="field_title_short">
                	<col class="field_content_short">
                	<col class="field_title_short">
                	<col class="field_content_short">
					<tbody>
						<tr>
                            <td class="field_title_short"><span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['ChineseName'] ?></td>
                            <td><input class="textboxtext" type="text" name="FatherChiName" value="<?= $Father[0]['NAME_C']?>" readonly="readonly" style="border: 0px"/></td>
                            <td class="field_title_short"><span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['ForeignName'] ?></td>
                            <td><input class="textboxtext" type="text" name="FatherEngName" value="<?= $Father[0]['NAME_E']?>" readonly="readonly" style="border: 0px"/></td>
							<td>&nbsp;</td>
                        	<td>&nbsp;</td>
                        </tr>
                        <tr>
                        	<td class="field_title_short"><span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['Job'] ?></td>
                        	<td><input class="textboxtext" type="text" name="FatherJobName" value="<?= $Father[0]['PROF']?>" readonly="readonly" style="border: 0px"/></td>
                        	<td class="field_title_short"><?= $Lang['StudentRegistry']['MaritalStatus'] ?></td>
                         	<td colspan="3"><span class="row_content Edit_Hide"><?= $FatherMarry_val ?></span><?= $FatherMarry_DDL_html ?></td>
						</tr>
                        <tr>
                        	<td class="field_title_short"><span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['HomePhone'] ?></td>
                        	<td><input class="textboxnum" type="text" name="FatherHomePhone" value="<?= $Father[0]['TEL']?>" readonly="readonly" style="border: 0px"/></td>
                        	<td class="field_title_short"><span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['CellPhone'] ?></td>
                         	<td><input class="textboxnum" type="text" name="FatherCellPhone" value="<?= $Father[0]['MOBILE']?>" readonly="readonly" style="border: 0px"/></td>
                        	<td class="field_title_short"><?= $Lang['StudentRegistry']['OfficeNumber'] ?></td>
                         	<td><input class="textboxnum" type="text" name="FatherOfficePhone" value="<?= $Father[0]['OFFICE_TEL']?>" readonly="readonly" style="border: 0px"/></td>						
						</tr>
						<tr>
							<td class="field_title_short"><?= $Lang['StudentRegistry']['Email'] ?></td>
							<td><input class="textboxtext" type="text" name="FatherEmail" value="<?= $Father[0]['EMAIL']?>" readonly="readonly" style="border: 0px"/></td>
							<td colspan="4">&nbsp;</td>
						</tr>
						<tr>
							<td class="field_title_short" style="visibility: hidden">&nbsp;</td>
							<td style="visibility: hidden">&nbsp;</td>
							<td class="field_title_short" style="visibility: hidden">&nbsp;</td>
							<td style="visibility: hidden">&nbsp;</td>
							<td class="field_title_short" style="visibility: hidden">&nbsp;</td>
							<td style="visibility: hidden">&nbsp;</td>
						</tr>
					</tbody>
					<col class="field_title_short">
                    <col class="field_c">
				</table>
    			<div class="form_sub_title">
    				<em>- <span class="field_title"><?= $Lang['StudentRegistry']['MotherInfo'] ?></span> -</em>
					<a id="Moth_Hide_Btn" href="javascript:void(0)" onclick="javascript:Hide_Table('MotherInfo_Table', 'Moth_Hide_Btn', 'Moth_Show_Btn');"><?= $Lang['StudentRegistry']['Hide'] ?></a>
                    <a id="Moth_Show_Btn" href="javascript:void(0)" onclick="javascript:Show_Table('MotherInfo_Table', 'Moth_Show_Btn', 'Moth_Hide_Btn');" style="display:none"><?= $Lang['StudentRegistry']['Show'] ?></a>
					<p class="spacer"></p>
				</div>
        		<table id="MotherInfo_Table" class="form_table">
                	<col class="field_title_short">
                	<col class="field_content_short">
                	<col class="field_title_short">
                	<col class="field_content_short">
                	<col class="field_title_short">
                	<col class="field_content_short">
					<tbody>
						<tr>
                            <td class="field_title_short"><span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['ChineseName'] ?></td>
                            <td><input class="textboxtext" type="text" name="MotherChiName" value="<?= $Mother[0]['NAME_C']?>" readonly="readonly" style="border: 0px"/></td>
                            <td class="field_title_short"><span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['ForeignName'] ?></td>
                            <td><input class="textboxtext" type="text" name="MotherEngName" value="<?= $Mother[0]['NAME_E']?>" readonly="readonly" style="border: 0px"/></td>
							<td>&nbsp;</td>
                        	<td>&nbsp;</td>
                        </tr>
                        <tr>
                        	<td class="field_title_short"><span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['Job'] ?></td>
                        	<td><input class="textboxtext" type="text" name="MotherJobName" value="<?= $Mother[0]['PROF']?>" readonly="readonly" style="border: 0px"/></td>
                        	<td class="field_title_short"><?= $Lang['StudentRegistry']['MaritalStatus'] ?></td>
                         	<td colspan="3"><span class="row_content Edit_Hide"><?= $MotherMarry_val ?></span><?= $MotherMarry_DDL_html ?></td>
						</tr>
                        <tr>
                        	<td class="field_title_short"><span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['HomePhone'] ?></td>
                        	<td><input class="textboxnum" type="text" name="MotherHomePhone" value="<?= $Mother[0]['TEL']?>" readonly="readonly" style="border: 0px"/></td>
                        	<td class="field_title_short"><span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['CellPhone'] ?></td>
                         	<td><input class="textboxnum" type="text" name="MotherCellPhone" value="<?= $Mother[0]['MOBILE']?>" readonly="readonly" style="border: 0px"/></td>
						    <td class="field_title_short"><?= $Lang['StudentRegistry']['OfficeNumber'] ?></td>
                         	<td><input class="textboxnum" type="text" name="MotherOfficePhone" value="<?= $Mother[0]['OFFICE_TEL']?>" readonly="readonly" style="border: 0px"/></td>
						</tr>
						<tr>
							<td class="field_title_short"><?= $Lang['StudentRegistry']['Email'] ?></td>
							<td><input class="textboxtext" type="text" name="MotherEmail" value="<?= $Mother[0]['EMAIL']?>" readonly="readonly" style="border: 0px"/></td>
							<td colspan="4">&nbsp;</td>
						</tr>
						<tr>
							<td class="field_title_short" style="visibility: hidden">&nbsp;</td>
							<td style="visibility: hidden">&nbsp;</td>
							<td class="field_title_short" style="visibility: hidden">&nbsp;</td>
							<td style="visibility: hidden">&nbsp;</td>
							<td class="field_title_short" style="visibility: hidden">&nbsp;</td>
							<td style="visibility: hidden">&nbsp;</td>
						</tr>
					</tbody>
					<col class="field_title_short">
                    <col class="field_c">
				</table>
				<div class="form_sub_title">
    				<em>- <span class="field_title"><?= $Lang['StudentRegistry']['GuardianInfo'] ?></span> -</em>
					<a id="Guard_Hide_Btn" href="javascript:void(0)" onclick="javascript:Hide_Table('GuardianInfo_Table', 'Guard_Hide_Btn', 'Guard_Show_Btn');"><?= $Lang['StudentRegistry']['Hide'] ?></a>
                    <a id="Guard_Show_Btn" href="javascript:void(0)" onclick="javascript:Show_Table('GuardianInfo_Table', 'Guard_Show_Btn', 'Guard_Hide_Btn');" style="display:none"><?= $Lang['StudentRegistry']['Show'] ?></a>					
					<p class="spacer"></p>
				</div>
        		<table id="GuardianInfo_Table"class="form_table">
                	<col class="field_title_short">
                	<col class="field_content_short">
                	<col class="field_title_short">
                	<col class="field_content_short">
                	<col class="field_title_short">
                	<col class="field_content_short">
					<tbody>
						<tr id="GuardNoInfo" <?= $Guard? "style=\"Display:none\"" : ""?>>
							<td colspan="6">No information</td>
						</tr>
						<tr class="GuardRelationInfo" <?= $Guard? "" : "style=\"Display:none\""?>>
							<td class="field_title_short"><span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['ByWho'] ?></td>
							<td colspan="5">
								<span class="row_content Edit_Hide"><?= $GuardRelation_val ?></span><?= $GuardRelation_RBL_html ?>
							</td>
						</tr>
						<tr class="GuardInfo" <?= $Guard[0]['GUARD'] == 'O'? "" : "style=\"Display:none\""?>>
                            <td class="field_title_short"><span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['ChineseName'] ?></td>
                            <td><input class="textboxtext" type="text" name="GuardChiName" value="<?=$Guard[0]['NAME_C']?>" readonly="readonly" style="border: 0px"/></td>
                            <td class="field_title_short"><span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['ForeignName'] ?></td>
                            <td><input class="textboxtext" type="text" name="GuardEngName" value="<?=$Guard[0]['NAME_E']?>" readonly="readonly" style="border: 0px"/></td>
                            <td>&nbsp;</td>
                        	<td>&nbsp;</td>
                        </tr>
                        <tr class="GuardInfo" <?= $Guard[0]['GUARD'] == 'O'? "" : "style=\"Display:none\""?>>
                        	<td class="field_title_short"><?= $Lang['StudentRegistry']['Gender'] ?></td>
                        	<td colspan="5"><span class="row_content Edit_Hide"><?= $GuardGender_val ?></span><?= $GuardGender_RBL_html ?></td>
                        </tr>
                        <tr class="GuardInfo" <?= $Guard[0]['GUARD'] == 'O'? "" : "style=\"Display:none\""?>>
                        	<td class="field_title_short"><?= $Lang['StudentRegistry']['Relationship'] ?></td>
                        	<td><span class="row_content"><?= $Lang['StudentRegistry']['Other'] ?></span></td>
                        	<td class="field_title_short"><span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['RelationOther'] ?></td>
                        	<td><input class="textboxtext" type="text" name="GuardRelationOther" value="<?=$Guard[0]['G_RELATION']?>" readonly="readonly" style="border: 0px"/></td>                   	
                        	<td class="field_title_short"><span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['StayTogether'] ?></td>
                         	<td><span class="row_content Edit_Hide"><?= $GuardStay_val ?></span><?= $GuardStay_RBL_html ?></td>
                        </td>
                        <tr class="GuardInfo" <?= $Guard[0]['GUARD'] == 'O'? "" : "style=\"Display:none\""?>>
                        	<td class="field_title_short"><span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['Job'] ?></td>
                        	<td><input class="textboxtext" type="text" name="GuardJobName" value="<?=$Guard[0]['PROF']?>" readonly="readonly" style="border: 0px"/></td>
 							<td class="field_title_short"><?= $Lang['StudentRegistry']['MaritalStatus'] ?></td>
                         	<td colspan="3"><span class="row_content Edit_Hide"><?= $GuardMarry_val ?></span><?= $GuardMarry_DDL_html ?></td>
 						</tr>
                        <tr class="GuardInfo" <?= $Guard[0]['GUARD'] == 'O'? "" : "style=\"Display:none\""?>>
                        	<td class="field_title_short"><span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['HomePhone'] ?></td>
                        	<td><input class="textboxnum" type="text" name="GuardHomePhone" value="<?=$Guard[0]['TEL']?>" readonly="readonly" style="border: 0px"/></td>
                        	<td class="field_title_short"><span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['CellPhone'] ?></td>
                         	<td><input class="textboxnum" type="text" name="GuardCellPhone" value="<?=$Guard[0]['MOBILE']?>" readonly="readonly" style="border: 0px"/></td>
						    <td class="field_title_short"><?= $Lang['StudentRegistry']['OfficeNumber'] ?></td>
                         	<td><input class="textboxnum" type="text" name="GuardOfficePhone" value="<?=$Guard[0]['OFFICE_TEL']?>" readonly="readonly" style="border: 0px"/></td>
						</tr>
						<tr class="GuardInfo" <?= $Guard[0]['GUARD'] == 'O'? "" : "style=\"Display:none\""?>>
							<td class="field_title_short"><span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['Address'] ?></td>
							<td colspan="5">
                        		<div class="form_field_sub_content">
                        			<table>
                        				<tr>
                  	  	    				<td><em>(<?= $Lang['StudentRegistry']['HomeDistrict'] ?>)</em></td>
                  		      				<td><span class="row_content Edit_Hide"><?= $GuardArea_val ?></span><?= $GuardArea_DDL_html ?></td>
                     		       		</tr>
                     		       	</table>
                     		       	<table>
                     		       		<tr>
                     		       			<td><em>(<?= $Lang['StudentRegistry']['Street'] ?>)</em></td>
                        	    			<td><span class="row_content"><input class="textbox_name" type="text" name="GuardStreet" value="<?=$Guard[0]['G_ROAD']?>" readonly="readonly" style="border: 0px"/></span></td>
                         	    		</tr>
                         	    	</table>
                         	    	<table>
                         	    		<tr>    
                         	    		    <td><em>(<?= $Lang['StudentRegistry']['AddDetail'] ?>)</em></td>
                        	    			<td><span class="row_content"><input class="textbox_name" type="text" name="GuardAddDetail" value="<?=$Guard[0]['G_ADDRESS']?>" readonly="readonly" style="border: 0px"/></span></td>
                         	    		</tr>
                        	    	</table>
                        		</div>
                        	</td>
						</tr>
						<tr class="GuardInfo" <?= $Guard[0]['GUARD'] == 'O'? "" : "style=\"Display:none\""?>>
							<td class="field_title_short"><?= $Lang['StudentRegistry']['Email'] ?></td>
							<td><input class="textboxtext" type="text" name="GuardEmail" value="<?=$Guard[0]['EMAIL']?>" readonly="readonly" style="border: 0px"/></td>
							<td colspan="4">&nbsp;</td>
						</tr>
						<tr>
							<td class="field_title_short" style="visibility: hidden">&nbsp;</td>
							<td style="visibility: hidden">&nbsp;</td>
							<td class="field_title_short" style="visibility: hidden">&nbsp;</td>
							<td style="visibility: hidden">&nbsp;</td>
							<td class="field_title_short" style="visibility: hidden">&nbsp;</td>
							<td style="visibility: hidden">&nbsp;</td>
						</tr>
					</tbody>
					<col class="field_title_short">
                    <col class="field_c">
				</table>
        		<div class="form_sub_title">
    				<em>- <span class="field_title"><?= $Lang['StudentRegistry']['EmergencyContactInfo'] ?></span> -</em>
					<a id="Emergency_Hide_Btn" href="javascript:void(0)" onclick="javascript:Hide_Table('EmergencyInfo_Table', 'Emergency_Hide_Btn', 'Emergency_Show_Btn');"><?= $Lang['StudentRegistry']['Hide'] ?></a>
                    <a id="Emergency_Show_Btn" href="javascript:void(0)" onclick="javascript:Show_Table('EmergencyInfo_Table', 'Emergency_Show_Btn', 'Emergency_Hide_Btn');" style="display:none"><?= $Lang['StudentRegistry']['Show'] ?></a>					
					<p class="spacer"></p>
				</div>
        		<table id="EmergencyInfo_Table"class="form_table">
                	<col class="field_title_short">
                	<col class="field_content_short">
                	<col class="field_title_short">
                	<col class="field_content_short">
                	<col class="field_title_short">
                	<col class="field_content_short">
					<tbody>
						<tr id="EmergencyNoInfo" <?= $Emergency? "style=\"Display:none\"" : ""?>>
							<td colspan="6">No information</td>
						</tr>
						<tr class="EmergencyRelationInfo" <?= $Emergency? "" : "style=\"Display:none\""?>>
							<td class="field_title_short"><span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['ByWho'] ?></td>
							<td colspan="5">
								<span class="row_content Edit_Hide"><?= $EmergencyRelation_val ?></span><?= $EmergencyRelation_RBL_html ?>
							</td>
						</tr>
                        <tr class="EmergencyInfo" <?= $Emergency[0]['GUARD'] == 'O'? "" : "style=\"Display:none\""?>>
                            <td class="field_title_short"><span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['ChineseName'] ?></td>
                            <td><input class="textboxtext" type="text" name="EmergencyChiName" value="<?=$Emergency[0]['NAME_C']?>" readonly="readonly" style="border: 0px"/></td>
                            <td class="field_title_short"><span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['ForeignName'] ?></td>
                            <td><input class="textboxtext" type="text" name="EmergencyEngName" value="<?=$Emergency[0]['NAME_E']?>" readonly="readonly" style="border: 0px"/></td>
                            <td>&nbsp;</td>
                        	<td>&nbsp;</td>
                        </tr>
                        <tr class="EmergencyInfo" <?= $Emergency[0]['GUARD'] == 'O'? "" : "style=\"Display:none\""?>>
                        	<td class="field_title_short"><span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['HomePhone'] ?></td>
                        	<td><input class="textboxnum" type="text" name="EmergencyHomePhone" value="<?=$Emergency[0]['TEL']?>" readonly="readonly" style="border: 0px"/></td>
                        	<td class="field_title_short"><span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['CellPhone'] ?></td>
                         	<td><input class="textboxnum" type="text" name="EmergencyCellPhone" value="<?=$Emergency[0]['MOBILE']?>" readonly="readonly" style="border: 0px"/></td>
						    <td class="field_title_short"><?= $Lang['StudentRegistry']['OfficeNumber'] ?></td>
                         	<td><input class="textboxnum" type="text" name="EmergencyOfficePhone" value="<?=$Emergency[0]['OFFICE_TEL']?>" readonly="readonly" style="border: 0px"/></td>
						</tr>
						<tr class="EmergencyInfo" <?= $Emergency[0]['GUARD'] == 'O'? "" : "style=\"Display:none\""?>>
							<td class="field_title_short"><span class="tabletextrequire" style="display: none">*</span><?= $Lang['StudentRegistry']['Address'] ?></td>
							<td colspan="5">
                        		<div class="form_field_sub_content">
                        			<table>
                        				<tr>
                  	  	    				<td><em>(<?= $Lang['StudentRegistry']['HomeDistrict'] ?>)</em></td>
                  		      				<td><span class="row_content Edit_Hide"><?= $EmergencyArea_val ?></span><?= $EmergencyArea_DDL_html ?></td>
                     		       		</tr>
                     		       	</table>
                     		       	<table>
                     		       		<tr>
                     		       			<td><em>(<?= $Lang['StudentRegistry']['Street'] ?>)</em></td>
                        	    			<td><span class="row_content"><input class="textbox_name" type="text" name="EmergencyStreet" value="<?=$Emergency[0]['G_ROAD']?>" readonly="readonly" style="border: 0px"/></span></td>
                         	    		</tr>
                         	    	</table>
                         	    	<table>
                         	    		<tr>    
                         	    		    <td><em>(<?= $Lang['StudentRegistry']['AddDetail'] ?>)</em></td>
                        	    			<td><span class="row_content"><input class="textbox_name" type="text" name="EmergencyAddDetail" value="<?=$Emergency[0]['G_ADDRESS']?>" readonly="readonly" style="border: 0px"/></span></td>
                         	    		</tr>
                        	    	</table>
                        		</div>
                        	</td>
						</tr>
						<tr>
							<td class="field_title_short" style="visibility: hidden">&nbsp;</td>
							<td style="visibility: hidden">&nbsp;</td>
							<td class="field_title_short" style="visibility: hidden">&nbsp;</td>
							<td style="visibility: hidden">&nbsp;</td>
							<td class="field_title_short" style="visibility: hidden">&nbsp;</td>
							<td style="visibility: hidden">&nbsp;</td>
						</tr>
					</tbody>
					<col class="field_title_short">
                    <col class="field_c">
				</table>
				<span id="FormReminder" class="tabletextremark" style="display: none"><?=$i_general_required_field?></span>
				<div class="edit_bottom">
					<span id="LastModified" class="row_content tabletextremark"><?= $Lang['StudentRegistry']['LastUpdated'] ?> : <?= $result[0]['DateModified']? $result[0]['DateModified']."(".$ModifiedBy.")" : "--"?></span>
                    <p class="spacer"></p>
					<!--
      			    <input id="SaveBtn" type="button" value="<?= $Lang['StudentRegistry']['Save'] ?>" class="formbutton" name="submit" style="display: none" onclick="javascript:show_arg()">
                    <input id="CancelBtn" type="button" value="<?= $Lang['StudentRegistry']['Cancel'] ?>" onclick="window.location.reload(true)" class="formbutton" name="submit2" style="display: none">
					-->
					<?=$linterface->GET_ACTION_BTN($button_submit, "submit", "show_arg()", "SaveBtn")?>&nbsp;
					<?=$linterface->GET_ACTION_BTN($button_cancel, "button", "ResetInfo()", "CancelBtn")?>
				</div>
					<p class="spacer"></p>
			</div>
		</td>
	</tr>
</table>
<input type="hidden" name="userID" id="userID" value="<?=$userID?>">
<input type="hidden" name="AcademicYearID" id="AcademicYearID" value="<?=$AcademicYearID?>">
</form>
<script language="javascript">
	document.getElementById('SaveBtn').style.visibility = "hidden";
	document.getElementById('CancelBtn').style.visibility = "hidden";
	<?
	if(sizeof($error)>0)
		echo "EditInfo();";
		
	?>
</script>
<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>