<?
// Modifying by: henry chow

############# Change Log [Start] ################
#
# Date:	2010-07-07 (Henry Chow)
#		- add GET_IMPORT_TXT_WITH_REFERENCE(), Apply new standard of import
#
# Date:	2009-12-15 YatWoon
#		- Add Access right checking 
#
############# Change Log [End] ################


$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libaccessright.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry.php");

intranet_auth();
intranet_opendb();

# Check access right
if(!$plugin['AccountMgmt_StudentRegistry'] || (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-View") && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-Manage"))) {
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$lsr = new libstudentregistry();
$li = new libdb();
$linterface = new interface_html();

$CurrentPageArr['StudentRegistry'] = 1;
$CurrentPage = "Mgmt_RegistryInfo";


$fail = 0;
$success = 0;

# delete the temp data in temp table 
$sql = "SELECT * FROM TEMP_STUDENT_REGISTRY_MACAU_IMPORT WHERE InputBy=$UserID";
$data = $lsr->returnArray($sql);

if(sizeof($data)==0) {
	intranet_closedb();
	header("Location: import_macau.php?xmsg=import_no_record");	
	exit();
}

$MODULE_OBJ['title'] = $Lang['Header']['Menu']['StudentRegistry'];
$TAGS_OBJ[] = array($Lang['Menu']['AccountMgmt']['StudentRegistryInfo'], "index.php");

$PAGE_NAVIGATION[] = array($Lang['StudentRegistry']['ClassList'], "index.php?AcademicYearID=$AcademicYearID");
$PAGE_NAVIGATION[] = array($Lang['StudentRegistry']['StudentList'], "class.php?AcademicYearID=$AcademicYearID");

# step information
$STEPS_OBJ[] = array($i_general_select_csv_file, 0);
$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 0);
$STEPS_OBJ[] = array($i_general_imported_result, 1);

//debug_pr($data);
$linterface->LAYOUT_START(); 

for($i=0; $i<sizeof($data); $i++)
{
	$result = array();
	
	list($rec_id, $StudentID, $CLASS, $C_NO, $UserLogin, $STUD_ID, $CODE, $NAME_C, $NAME_E, $ENTRY_DATE, $S_CODE, $SEX, $B_DATE, $B_PLACE, $ID_TYPE, $ID_NO, $I_PLACE, $I_DATE, $V_DATE, $S6_TYPE, $S6_IDATE, $S6_VDATE, $NATION, $ORIGIN, $RELIGION, $EMAIL, $TEL, $R_AREA, $RA_DESC, $AREA, $ROAD, $ADDRESS, $FATHER_C, $FATHER_E, $F_PROF, $F_MARTIAL, $F_TEL, $F_MOBILE, $F_OFFICE, $F_EMAIL, $MOTHER_C, $MOTHER_E, $M_PROF, $M_MARTIAL, $M_TEL, $M_MOBILE, $M_OFFICE, $M_EMAIL, $GUARDIAN_C, $GUARDIAN_E, $G_SEX, $G_PROF, $GUARD, $G_REL_OTHERS, $LIVE_SAME, $G_AREA, $G_ROAD, $G_ADDRESS, $G_TEL, $GUARDMOBILE, $G_OFFICE, $G_EMAIL, $EC_NAME_C, $EC_NAME_E, $EC_REL, $EC_TEL, $EC_MOBILE, $EC_AREA, $EC_ROAD, $EC_ADDRESS) = $data[$i];
	
	# update INTRANET_USER
	$sql = "UPDATE INTRANET_USER SET ChineseName='$NAME_C', EnglishName='$NAME_E', Gender='$SEX', DateOfBirth='$B_DATE', HomeTelNo='$TEL' WHERE UserID='$StudentID'";
	$result[] = $lsr->db_db_query($sql);
//echo $sql;
	# insert into STUDENT_REGISTRY_STUDENT
	$sql = "INSERT INTO STUDENT_REGISTRY_STUDENT 
			(UserID, STUD_ID, CODE, ADMISSION_DATE, S_CODE, B_PLACE, ID_NO, ID_TYPE, I_PLACE, I_DATE, V_DATE, S6_TYPE, S6_IDATE, S6_VDATE, NATION, ORIGIN, RELIGION, R_AREA, RA_DESC, AREA, ROAD, ADDRESS, EMAIL, DateInput, InputBy, DateModified, ModifyBy)
			VALUES
			('$StudentID','$STUD_ID','$CODE','$ENTRY_DATE','$S_CODE','$B_PLACE','$ID_NO','$ID_TYPE','$I_PLACE','$I_DATE','$V_DATE','$S6_TYPE','$S6_IDATE','$S6_VDATE','$NATION','$ORIGIN','$RELIGION','$R_AREA','$RA_DESC','$AREA','$ROAD','$ADDRESS','$EMAIL',NOW(), $UserID, NOW(), $UserID)
			";
	$result[] = $lsr->db_db_query($sql);
//echo $sql.'<br>';
	# insert into STUDENT_REGISTRY_PG - FATHER
	$sql = "INSERT INTO STUDENT_REGISTRY_PG
			(StudentID, PG_TYPE, NAME_C, NAME_E, G_GENDER, GUARD, PROF, MARITAL_STATUS, TEL, MOBILE, OFFICE_TEL, LIVE_SAME, G_AREA, G_ROAD, G_ADDRESS, EMAIL, DateInput, InputBy, DateModified, ModifyBy)
			VALUES
			('$StudentID', 'F', '$FATHER_C', '$FATHER_E', 'M', 'F', '$F_PROF', '$F_MARTIAL', '$F_TEL', '$F_MOBILE', '$F_OFFICE', NULL, '$AREA', '$ROAD', '$ADDRESS', '$F_EMAIL', NOW(), $UserID, NOW(), $UserID)
			";
	$result[] = $lsr->db_db_query($sql);
//echo $sql.'<br>';	
	# insert into STUDENT_REGISTRY_PG - MOTHER
	$sql = "INSERT INTO STUDENT_REGISTRY_PG
			(StudentID, PG_TYPE, NAME_C, NAME_E, G_GENDER, GUARD, PROF, MARITAL_STATUS, TEL, MOBILE, OFFICE_TEL, LIVE_SAME, G_AREA, G_ROAD, G_ADDRESS, EMAIL, DateInput, InputBy, DateModified, ModifyBy)
			VALUES
			('$StudentID', 'M', '$MOTHER_C', '$MOTHER_E', 'F', 'M', '$M_PROF', '$M_MARTIAL', '$M_TEL', '$M_MOBILE', '$M_OFFICE', NULL, '$AREA', '$ROAD', '$ADDRESS', '$M_EMAIL', NOW(), $UserID, NOW(), $UserID)
			";
	$result[] = $lsr->db_db_query($sql);
//echo $sql.'<br>';	
	# insert into STUDENT_REGISTRY_PG - GUARDIAN
	$sql = "INSERT INTO STUDENT_REGISTRY_PG
			(StudentID, PG_TYPE, NAME_C, NAME_E, G_GENDER, GUARD, G_RELATION, PROF, TEL, MOBILE, OFFICE_TEL, LIVE_SAME, G_AREA, G_ROAD, G_ADDRESS, EMAIL, DateInput, InputBy, DateModified, ModifyBy)
			VALUES
			('$StudentID', 'G', '$GUARDIAN_C', '$GUARDIAN_E', '$G_SEX', '$GUARD', '$G_REL_OTHERS', '$G_PROF', '$G_TEL', '$GUARDMOBILE', '$G_OFFICE', '$LIVE_SAME', '$G_AREA', '$G_ROAD', '$G_ADDRESS', '$G_EMAIL', NOW(), $UserID, NOW(), $UserID)
			";
	$result[] = $lsr->db_db_query($sql);
//echo $sql.'<br>';	
	# insert into STUDENT_REGISTRY_PG - EMERGENCY CONTACT
	$sql = "INSERT INTO STUDENT_REGISTRY_PG
			(StudentID, PG_TYPE, NAME_C, NAME_E, GUARD, TEL, MOBILE, G_AREA, G_ROAD, G_ADDRESS, DateInput, InputBy, DateModified, ModifyBy)
			VALUES
			('$StudentID', 'E', '$EC_NAME_C', '$EC_NAME_E', '$EC_REL', '$EC_TEL', '$EC_MOBILE', '$EC_AREA', '$EC_ROAD', '$EC_ADDRESS', NOW(), $UserID, NOW(), $UserID)
			";
	$result[] = $lsr->db_db_query($sql);
//echo $sql.'<br>';	
	if(in_array(false, $result)) {
		$fail++;	
	} else {
		$sql = "DELETE FROM TEMP_STUDENT_REGISTRY_MACAU_IMPORT WHERE StudentID=$StudentID AND InputBy=$UserID";
		$lsr->db_db_query($sql);
		$success++;	
	}
	//debug_pr($result);
}

### List out the import result
$x .= "<table width=\"90%\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\">";
/*
$x .= "<tr>";
$x .= "<td class='formfieldtitle' width='30%' align='left'>". $Lang['SysMgr']['FormClassMapping']['SchoolYear'] ."</td>";
$x .= "<td class='tabletext'>". GetCurrentAcademicYear() ."</td>";
$x .= "</tr>";
*/
$x .= "<tr>";
$x .= "<td class='formfieldtitle' width='30%' align='left'>". $Lang['General']['SuccessfulRecord'] ."</td>";
$x .= "<td class='tabletext'>". $success ."</td>";
$x .= "</tr>";

$x .= "<tr>";
$x .= "<td class='formfieldtitle' width='30%' align='left'>". $Lang['General']['FailureRecord'] ."</td>";
$x .= "<td class='tabletext'>". ($fail ? "<font color='red'>":"") . $fail . ($fail ? "</font>":"") ."</td>";
$x .= "</tr>";

$x .= "</table><br>";


$import_button = $linterface->GET_ACTION_BTN($button_finish, "button", "window.location='class.php?AcademicYearID=$AcademicYearID'")."&nbsp;".$linterface->GET_ACTION_BTN($iDiscipline['ImportOtherRecords'], "button", "window.location='import_macau.php?AcademicYearID=$AcademicYearID'");

?>

<br />
<form name="form1" method="post" action="">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?=$linterface->GET_NAVIGATION($PAGE_NAVIGATION)?></td>
</tr>
<tr>
	<td><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
</tr>
<tr>
	<td align="center"><?=$x?></td>
</tr>
		
<tr>
	<td>
		<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr>
			<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
		</tr>
		<tr>
			<td align="center" colspan="2">
			<?=$import_button?>
			</td>
		</tr>
		</table>
	</td>
</tr>
</table>
<input type="hidden" name="AcademicYearID" value="<?=$AcademicYearID?>">
</form>
<br />

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>
