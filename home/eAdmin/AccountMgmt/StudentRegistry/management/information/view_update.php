<?php
#using: henry chow

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccessright.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry.php");

intranet_auth();
intranet_opendb();

$laccessright = new libaccessright();

if(!$plugin['AccountMgmt_StudentRegistry'] || (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-View") && !$laccessright->CHECK_ACCESS("StudentRegistry-MGMT-RegistryInformation-Manage"))) {
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$lsr = new libstudentregistry();

$dataAry = array();

$dataAry['STUD_ID'] = $StudentID;
$dataAry['CODE'] = $DSEJ_Number;
$dataAry['S_CODE'] = $SchoolCode;
$lsr->updateStudentRegistry_Simple_Macau($dataAry, $userID);

$userAry['ChineseName'] = $ChineseName;
$userAry['EnglishName'] = $EnglishName;
$userAry['Gender'] = $Gender;
$userAry['HomeTelNo'] = $HomePhone;
$lsr->updateStudentInfo($userAry, $userID);

intranet_closedb();

header("Location: view.php?xmsg=1&AcademicYearID=$AcademicYearID&userID=$userID");
?>