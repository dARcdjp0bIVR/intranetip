<?php
#using: Thomas

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccessright.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_header.php");

intranet_auth();
intranet_opendb();

$laccessright = new libaccessright();

if(!$plugin['AccountMgmt_StudentRegistry'] || (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] && !$laccessright->CHECK_ACCESS("StudentRegistry-REPORTS-RegistryInformation-View"))) {
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$lsr = new libstudentregistry();
$linterface = new interface_html();
$lclass = new libclass();

# Get Selected Student Info
$StudBasic = isset($StudBasic)? $StudBasic : 0;
$StudAdv = isset($StudAdv)? $StudAdv : 0;
$S_Info = (!$StudBasic && !$StudAdv)? 0 : (($StudBasic)? 1 : 2);

# Display selected class information in table head
$AcademicYearName = $lsr->getAcademicYearNameByYearID($AcademicYearID);
$ClassName = "";
$ClassTeacher = "";
for($i=0;$i<sizeof($ClassList);$i++)
{
	if($ClassName!="")
		$ClassName .= ", ";
	$ClassName .= $lclass->getClassNameByLang($ClassList[$i]);
	
	$Temp_ClassTeacher = $lsr->getClassTeacherByYearClassID($ClassList[$i]);
	for($j=0;$j<sizeof($Temp_ClassTeacher);$j++)
	{
		if($ClassTeacher!="")
			$ClassTeacher .= ", ";
		$ClassTeacher .= $Temp_ClassTeacher[$j][1];
	}
}
$table_head = "<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"100%\">
					<tr>
						<td valign=\"bottom\">
							<div class=\"table_filter\">".
								$Lang['StudentRegistry']['AcademicYear']." : $AcademicYearName ". 
								$Lang['StudentRegistry']['Class']." : $ClassName
							</div> 
						</td>
						<td valign=\"bottom\">
							<div class=\"Conntent_search\">
								".$Lang['SysMgr']['FormClassMapping']['ClassTeacher']." : $ClassTeacher
							</div>
						</td>
					</tr>
   			   </table>";

//debug_pr($S_Info);
?>

<table width="98%" border="0" cellspacing="0" cellpadding="5" valign="top">
	<tr>
		<td align="right" class='print_hide'>
			<?=$linterface->GET_BTN($button_print, "button","javascript:window.print()")?>
		</td>
	</tr>
	<tr>
		<td>
			<div class="page_title_print">
				<?=GET_SCHOOL_NAME()?>
			</div>
			<div class="table_board">
				<?=$table_head?>
			</div>
			<div class="table_board">
				<?=$lsr->getStudRegInfoReport_StudentList($StudentList, $S_Info, $FatherInfo, $MotherInfo, $GuardInfo, $AcademicYearID, true)?>
			</div>
		</td>
	</tr>
</table>

<?php
intranet_closedb();
include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_footer.php");
?>