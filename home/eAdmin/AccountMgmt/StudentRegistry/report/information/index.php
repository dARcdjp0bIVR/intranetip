<?php
#using: Thomas

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccessright.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");

intranet_auth();
intranet_opendb();

$laccessright = new libaccessright();

if(!$plugin['AccountMgmt_StudentRegistry'] || (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] && !$laccessright->CHECK_ACCESS("StudentRegistry-REPORTS-RegistryInformation-View"))) {
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$lsr = new libstudentregistry();
$linterface = new interface_html();
$lclass = new libclass();

# Get Submit_flag
$submit_flag = isset($submit_flag)? $submit_flag : 0;

# Get Report Type
$report_type = isset($report_type)? $report_type : 1;

# Get Selected Student Info
$StudBasic = isset($StudBasic)? $StudBasic : 0;
$StudAdv = isset($StudAdv)? $StudAdv : 0;
$S_Info = (!$StudBasic && !$StudAdv)? 0 : (($StudBasic)? 1 : 2);

# Get Academic Year Drop Down List
$AcademicYearID = isset($AcademicYearID)? $AcademicYearID : Get_Current_Academic_Year_ID();
$years = GetAllAcademicYearInfo();
for($i=0; $i<sizeof($years); $i++) {
	list($yearid, $yearNameEN, $yearNameB5) = $years[$i];
	$yearAry[$i][] = $yearid;
	$yearAry[$i][] = Get_Lang_Selection($yearNameB5, $yearNameEN);	
}
$yearSelectionMenu = getSelectByArray($yearAry, 'name="AcademicYearID" id="AcademicYearID" onchange="javascript:ChangeReportOption(this.form, 3)"', $AcademicYearID, 0, 1);

# Display selected class information in table head
$AcademicYearName = $lsr->getAcademicYearNameByYearID($AcademicYearID);
$ClassName = "";
$ClassTeacher = "";
for($i=0;$i<sizeof($ClassList);$i++)
{
	if($ClassName!="")
		$ClassName .= ", ";
	$ClassName .= $lclass->getClassNameByLang($ClassList[$i]);
	
	$Temp_ClassTeacher = $lsr->getClassTeacherByYearClassID($ClassList[$i]);
	for($j=0;$j<sizeof($Temp_ClassTeacher);$j++)
	{
		if($ClassTeacher!="")
			$ClassTeacher .= ", ";
		$ClassTeacher .= $Temp_ClassTeacher[$j][1];
	}
}
$table_head = "<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"100%\">
					<tr>
						<td class=\"table_filter\">".
							$Lang['StudentRegistry']['AcademicYear']." : $AcademicYearName ". 
							$Lang['StudentRegistry']['Class']." : $ClassName 
						</td>
						<td class=\"Conntent_search\">".$Lang['SysMgr']['FormClassMapping']['ClassTeacher']." : $ClassTeacher</td>
					</tr>
   			   </table>";

/*
# Get Class List
$ClassList = isset($ClassList)? $ClassList : array();
$ClassListArr = $lsr->displayClassInfoInStudentRegistry($AcademicYearID);

if($_POST["submit_flag"] != "YES")
	$ClassList[0] = $ClassListArr[0][0];

$ClassOptionHTML = "";
for($i=0;$i<sizeof($ClassListArr);$i++)
{
	$ClassOptionHTML .= "<option value=\"".$ClassListArr[$i][0]."\"";
	if (in_array($ClassListArr[$i][0], $ClassList))
		$ClassOptionHTML .= " selected";
	$ClassOptionHTML .= ">".$ClassListArr[$i][1]."</option>\n";
}

# Get Student List
$StudentList = isset($StudentList)? $StudentList : array();
$StudentOptionHTML = "";

for($i=0;$i<sizeof($ClassList);$i++)
{
	$StudentListArr = $lsr->displayStudentInfoInStudentRegistry($ClassList[$i]);
	for($j=0;$j<sizeof($StudentListArr);$j++)
	{
		$StudentOptionHTML .= "<option value=\"".$StudentListArr[$j][0]."\"";
		if(in_array($StudentListArr[$j][0], $StudentList))
			$StudentOptionHTML .= "selected";
		$StudentOptionHTML .= ">".$StudentListArr[$j][1]."-".$StudentListArr[$j][2]." ".$StudentListArr[$j][3]."</option>\n";
	}

}*/

#Class List Array to String
$ClassListStr = "";
for($i=0;$i<sizeof($ClassList);$i++)
{
	if($ClassListStr!="")
		$ClassListStr .= ",";
	$ClassListStr .= strval($ClassList[$i]);
}

#Student List Array to String
$StudentListStr = "";
for($i=0;$i<sizeof($StudentList);$i++)
{
	if($StudentListStr!="")
		$StudentListStr .= ",";
	$StudentListStr .= strval($StudentList[$i]);
}

$CurrentPageArr['StudentRegistry'] = 1;
$CurrentPage = "Report_RegistryInfo";

$TAGS_OBJ[] = array($Lang['Menu']['AccountMgmt']['StudentRegistryInfo'], "index.php");

$MODULE_OBJ['title'] = $Lang['Header']['Menu']['StudentRegistry'];

$MODULE_OBJ = $lsr->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();
?>

<script language="javascript">
function hideOption()
{
	$('#spanHideOption').attr('style', 'display:none');
	$('#spanShowOption').attr('style', '');
	$('#report_form').attr('style', 'display:none');
	$('#td_option').addClass('report_show_option');
}

function showOption()
{
	$('#spanHideOption').attr('style', '');
	$('#spanShowOption').attr('style', 'display:none');
	$('#report_form').attr('style', '');
	$('#td_option').removeClass();
}

function hide_year_class_stud()
{
	$('.blank_hide').attr('style', 'display:none');
}

function show_year_class_stud()
{
	$('.blank_hide').attr('style', '');	
}

function ChangeReportOption(obj, mode)
{
	if(mode == 4)
	{
		var ClassListLength = obj.elements['ClassList'].length;
		var ClassListStr = "";
		for(i=0;i<ClassListLength;i++)
		{
			if(obj.elements['ClassList'][i].selected)
			{
				if(ClassListStr!="")
					ClassListStr = ClassListStr + ",";
				ClassListStr = ClassListStr + obj.elements['ClassList'][i].value;
			}
		}
		
		var StudentListLength = obj.elements['StudentList'].length;
		var StudentListStr = "";
		for(i=0;i<StudentListLength;i++)
		{
			if(obj.elements['StudentList'][i].selected)
			{
				if(StudentListStr!="")
					StudentListStr = StudentListStr + ",";
				StudentListStr = StudentListStr + obj.elements['StudentList'][i].value;
			}
		}
	}
	else if(mode == 2)
	{
		ClassListStr = '<?=$ClassListStr?>';
		StudentListStr = '<?=$StudentListStr?>';
	}
		
	//alert('Class: '+ClassListStr+' | Student: '+StudentListStr);
	
	var AcademicYearID = $('#AcademicYearID').val();

	$.post(
			'ajax_change_report_options.php',
		   	{
				AcademicYearID 	: AcademicYearID,
				ClassListStr	: ClassListStr,
				StudentListStr	: StudentListStr,
				mode		   	: mode
		 	},
		 	function(data){
		 		var DataArr = data.split("|=|")
		 		if(mode!=4)
		 		{
		 			if(DataArr[0]=="")
			 		{
		 				$('#ClassList').html('<option value="">No Class</option>');
	 					$('#ClassList').attr('disabled', 'disabled');
	 					$('#ClassCtrlRemind').attr('style', 'display:none');
						$('#ClassSelectAll').attr('style', 'display:none');
			 		}
			 		else
			 		{
		 				$('#ClassList').html(DataArr[0]);
		 				$('#ClassList').attr('disabled', '');
		 				$('#ClassCtrlRemind').attr('style', '');
			 			$('#ClassSelectAll').attr('style', '');
			 		}
		 		}
		 		if(DataArr[1]=="")
		 		{
		 			$('#StudentList').html('<option value="">No Student</option>');
		 			$('#StudentList').attr('disabled', 'disabled');
		 			$('#StudentCtrlRemind').attr('style', 'display:none');
		 			$('#StudentSelectAll').attr('style', 'display:none');
		 		}
		 		else
		 		{
		 			$('#StudentList').html(DataArr[1]);
		 			$('#StudentList').attr('disabled', '');
		 			$('#StudentCtrlRemind').attr('style', '');
		 			$('#StudentSelectAll').attr('style', '');
		 		}
		 	});
}

function checksubmit(obj, mode)
{
	if(!obj.report_type[0].checked && !obj.report_type[1].checked && !obj.report_type[2].checked)
	{
		alert('<?=$i_alert_pleaseselect.$Lang['StudentRegistry']['ReportType']?>');
		return false;
	}
	if(obj.AcademicYearID.value == '' && !obj.report_type[2].checked)
	{
		alert('<?=$i_alert_pleaseselect.$Lang['StudentRegistry']['AcademicYear']?>');
		return false;
	}
	if(obj.ClassList.value == '' && !obj.report_type[2].checked)
	{
		alert('<?=$i_alert_pleaseselect.$Lang['StudentRegistry']['Class']?>');
		return false;
	}
	if(obj.StudentList.value == '' && !obj.report_type[2].checked)
	{
		alert('<?=$i_alert_pleaseselect.$Lang['StudentRegistry']['Student']?>');
		return false;
	}
	if(!obj.StudBasic.checked && !obj.StudAdv.checked && !obj.FatherInfo.checked && !obj.MotherInfo.checked && !obj.GuardInfo.checked)
	{
		alert('<?=$i_alert_pleaseselect.$Lang['StudentRegistry']['InfoShown']?>');
		return false;
	}
		
	obj.submit_flag.value = 1;
	
	if(mode==0) //Go to generate report
	{
		if(obj.report_type[0].checked)
		{
			obj.target = "";
			obj.action = "index.php";
		}
		else if(obj.report_type[1].checked)
		{
			obj.target = "_blank";
			obj.action = "personal_report.php";
		}
		else if(obj.report_type[2].checked)
		{
			obj.target = "_blank";
			obj.action = "personal_report.php";
		}
	}
	else if(mode==1) //Go to Print
	{
		obj.target = "_blank";
		obj.action = "index_print.php"
	}
	else if(mode==2) //Go to export csv
	{
		obj.target = "_blank"
		obj.action = "index_export.php";
	}
	
	//alert($('[name="form1"]').serialize());
	obj.submit();
}

$(document).ready(function(){
	if(<?=$submit_flag?>)
		ChangeReportOption(document.form1, 2);
	else
		ChangeReportOption(document.form1, 1);
});
</script>
<form name="form1" method="POST" action="">
	<table cellspacing="0" cellpadding="3" border="0" align="center" width="99%">
		<tr class="report_show_option" <?=$_POST["submit_flag"] == 1? "" : "style=\"display:none\""?>>
			<td id="td_option" class="report_show_option">
				<span id="spanHideOption" <?=$_POST["submit_flag"] == 1? "style=\"display:none\"" : ""?>>
					<a href="javascript:hideOption();" class="contenttool">
						<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_hide_option.gif" width="20" height="20" border="0" align="absmiddle"><?=$Lang['StudentRegistry']['HideReportOption']?>
					</a>
				</span>
				<span id="spanShowOption">
					<a href="javascript:showOption();" class="contenttool">
						<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_show_option.gif" width="20" height="20" border="0" align="absmiddle"><?=$Lang['StudentRegistry']['ShowReportOption']?>
					</a>
				</span>
			</td>
		</tr>
	</table>
	<table cellspacing="0" cellpadding="0" border="0" width="99%">
		<tr>
			<td class="main_content">
				<span id="report_form" <?=$_POST["submit_flag"] == 1? "style=\"display:none\"" : ""?>>
					<div class="report_option report_option_title" <?=$_POST["submit_flag"] == 1? "style=\"display:none\"" : ""?>>
						- <?= $Lang['StudentRegistry']['Options'] ?> -
					</div>
					<div class="table_board">
						<table class="form_table_v30">
							<tbody>
								<tr>
									<td class="field_title"><?= $Lang['StudentRegistry']['ReportType'] ?></td>
									<td>
										<input type="radio" id="report_type1" name="report_type" value="1" <?= $report_type == 1? "checked=\"checked\"" : ""?> onclick="javascript:show_year_class_stud()"><label for="report_type1"><?=$Lang['StudentRegistry']['EntireClassList']?></label>
										&nbsp;
										<input type="radio" id="report_type2" name="report_type" value="2" <?= $report_type == 2? "checked=\"checked\"" : ""?> onclick="javascript:show_year_class_stud()"><label for="report_type2"><?=$Lang['StudentRegistry']['PersonalReport']?></label>
										&nbsp;
										<input type="radio" id="report_type3" name="report_type" value="3" <?= $report_type == 3? "checked=\"checked\"" : ""?> onclick="javascript:hide_year_class_stud()"><label for="report_type3"><?=$Lang['StudentRegistry']['BlankForm']?></label>
									</td>
								</tr>
							</tbody>
							<col class="field_title">
        					<col class="field_c">
							<tbody>
								<tr class="blank_hide" <?= $report_type == 3? "style=\"display:none\"" : ""?>>
									<td class="field_title"><?=$Lang['StudentRegistry']['AcademicYear']?></td>
									<td><?=$yearSelectionMenu?></td>
								</tr>
								<tr class="blank_hide" <?= $report_type == 3? "style=\"display:none\"" : ""?>>
									<td class="field_title"><?=$Lang['StudentRegistry']['Class']?></td>
									<td>
										<select size="5" id="ClassList" name="ClassList[]" onchange="javascript:ChangeReportOption(this.form, 4)" multiple>
											<?=$ClassOptionHTML?>
										</select>
										<?= $linterface->GET_SMALL_BTN($button_select_all, "button", "javascript:checkOptionAll(this.form.ClassList); ChangeReportOption(this.form, 4);", "ClassSelectAll")?>
										<br>
										<span id="ClassCtrlRemind" class="tabletextremark">(<?=$Lang['StudentRegistry']['CtrlSelectAll']?>)</span>
									</td>
								</tr>
								<tr class="blank_hide" <?= $report_type == 3? "style=\"display:none\"" : ""?>>
									<td class="field_title"><?=$Lang['StudentRegistry']['Student']?></td>
									<td>
										<select size="5" id="StudentList" name="StudentList[]" multiple>
											<?=$StudentOptionHTML?>
										</select>
										<?= $linterface->GET_SMALL_BTN($button_select_all, "button", "javascript:checkOptionAll(this.form.StudentList)", "StudentSelectAll")?>
										<br>
										<span id="StudentCtrlRemind" class="tabletextremark">(<?=$Lang['StudentRegistry']['CtrlSelectAll']?>)</span>
									</td>
								</tr>
								<tr>
									<td class="field_title"><?=$Lang['StudentRegistry']['InfoShown']?></td>
									<td>
										<input type="checkbox" id="StudBasic" name="StudBasic" value="1" onclick="this.form.StudAdv.checked = 0" <?= $StudBasic == 1? "checked=\"checked\"" : ""?>><label for="StudBasic"><?=$Lang['StudentRegistry']['StudentBasicInfo_s']?></label>&nbsp;
										<input type="checkbox" id="StudAdv" name="StudAdv" value="1"  onclick="this.form.StudBasic.checked = 0" <?= $StudAdv == 1? "checked=\"checked\"" : ""?>><label for="StudAdv"><?=$Lang['StudentRegistry']['StudentAdvInfo_s']?></label>&nbsp;
										<input type="checkbox" id="FatherInfo" name="FatherInfo" value="1" <?= $FatherInfo == 1? "checked=\"checked\"" : ""?>><label for="FatherInfo"><?=$Lang['StudentRegistry']['FatherInfo_s']?></label>&nbsp;
										<input type="checkbox" id="MotherInfo" name="MotherInfo" value="1" <?= $MotherInfo == 1? "checked=\"checked\"" : ""?>><label for="MotherInfo"><?=$Lang['StudentRegistry']['MotherInfo_s']?></label>&nbsp;
										<input type="checkbox" id="GuardInfo" name="GuardInfo" value="1" <?= $GuardInfo == 1? "checked=\"checked\"" : ""?>><label for="GuardInfo"><?=$Lang['StudentRegistry']['GuardianInfo_s']?></label>
									</td>
								</tr>
							</tbody>
						</table>
						<p class="spacer"></p>
					</div>
					<div class="edit_bottom">
						<p class="spacer"></p>
						<?=$linterface->GET_ACTION_BTN($Lang['StudentRegistry']['GenerateReport'], "button", "checksubmit(this.form, 0)", "GenerateBtn")?>
						<p class="spacer"></p>
					</div>
				</span>
				<div class="table_board">
					<?=$submit_flag? $table_head : ""?>
				</div>
				<div class="table_board" style="width: 1050px; overflow: auto;">
					<?=$submit_flag? $lsr->getStudRegInfoReport_StudentList($StudentList, $S_Info, $FatherInfo, $MotherInfo, $GuardInfo, $AcademicYearID, false) : ""?>
				</div>
				<br style="clear: both">
				<div class="edit_bottom" <?=$submit_flag? "" : "style=\"display:none\""?>>
					<p class="spacer"></p>
					<?=$linterface->GET_ACTION_BTN($button_print, "button", "checksubmit(this.form, 1)", "PrintBtn")?>
					<?=$linterface->GET_ACTION_BTN($button_export, "button", "checksubmit(this.form, 2)", "ExportBtn")?>
					<p class="spacer"></p>
				</div>
			</td>
		</tr>
	</table>
	<input type="hidden" id="submit_flag" name="submit_flag" value="">
</form>

<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>