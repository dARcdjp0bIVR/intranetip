<?php 
# using: 

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libaccessright.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry.php");

intranet_auth();
intranet_opendb();

$laccessright = new libaccessright();

if(!$plugin['AccountMgmt_StudentRegistry'] || (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] && !$laccessright->CHECK_ACCESS("StudentRegistry-REPORTS-RegistryStatus-View"))) {
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$linterface = new interface_html();
$lsr = new libstudentregistry();

if(!isset($AcademicYearID) || $AcademicYearID=="")
	$AcademicYearID = Get_Current_Academic_Year_ID();

$sql = "SELECT AcademicYearID, ".Get_Lang_Selection('YearNameB5', 'YearNameEN')." as yearName FROM ACADEMIC_YEAR ORDER BY Sequence";
$yearInfo = $lsr->returnArray($sql);
$yearAry = array();
for($i=0; $i<sizeof($yearInfo); $i++) {
	$yearAry[$i][] = $yearInfo[$i][0];
	$yearAry[$i][] = $yearInfo[$i][1];
}
$yearSelection = getSelectByArray($yearAry, ' name="AcademicYearID" id="AcademicYearID"', $AcademicYearID, 0, 1, "");

if($flag == 1) {
	$optionString = "<td id=\"tdOption\" class=\"tabletextremark\"><span id=\"spanShowOption\"><a href=\"javascript:showOption();\" class=\"contenttool\"><img src=\"$image_path/$LAYOUT_SKIN/icon_show_option.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\">$i_Discipline_Show_Statistics_Option</a></span>";
	$optionString .= "<span id=\"spanHideOption\" style=\"display:none\"><a href=\"javascript:hideOption();\" class=\"contenttool\"><img src=\"$image_path/$LAYOUT_SKIN/icon_hide_option.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\">$i_Discipline_Hide_Statistics_Option</a></span></td>";
	$initialString = "<script language=\"javascript\">hideOption();</script>\n";

	$print = 0;
	$dateAry = array("startDate"=>$startDate, "endDate"=>$endDate);
	$tableContent = $lsr->getRegistryStatusReport($AcademicYearID, $Type, $list, $statistics, $print, $dateSelect, $dateAry);

} else {
	// do nothing
}

switch($Type) {
	case STUDENT_REGISTRY_RECORDSTATUS_LEFT : $typeText = $Lang['StudentRegistry']['RegistryStatus_Left']; break;
	case STUDENT_REGISTRY_RECORDSTATUS_SUSPEND : $typeText = $Lang['StudentRegistry']['RegistryStatus_Suspend']; break;
	case STUDENT_REGISTRY_RECORDSTATUS_RESUME : $typeText = $Lang['StudentRegistry']['RegistryStatus_Resume']; break;
	default : $typeText = $Lang['StudentRegistry']['RegistryStatus_Left']; break;
}

# Top menu highlight setting
$CurrentPage = "Report_RegistryStatus";

$TAGS_OBJ[] = array($Lang['StudentRegistry']['StudentRegistryStatus']);

$CurrentPageArr['StudentRegistry'] = 1;

# Left menu 
$MODULE_OBJ = $lsr->GET_MODULE_OBJ_ARR();

# Start layout
$linterface->LAYOUT_START();


?>
<script language="javascript">
function loadText(flag) {
	if(flag==3) {
		listText = "<?=$Lang['StudentRegistry']['ListOfResume']?>";
		statisticsText = "<?=$Lang['StudentRegistry']['StatisticsOfResume']?>";
	}
	else if(flag==2) {
		listText = "<?=$Lang['StudentRegistry']['ListOfSuspend']?>";
		statisticsText = "<?=$Lang['StudentRegistry']['StatisticsAndReasonOfSuspend']?>";
	}
	else {
		listText = "<?=$Lang['StudentRegistry']['ListOfLeft']?>";
		statisticsText = "<?=$Lang['StudentRegistry']['StatisticsAndReasonOfLeft']?>";
	}
	document.getElementById('spanList').innerHTML = listText;
	document.getElementById('spanStatistics').innerHTML = statisticsText;
}

function showSpan(span) {
	//alert("show:"+span);
	document.getElementById(span).style.display = "inline";
}

function hideSpan(span) {
	//alert("hide:"+span);
	document.getElementById(span).style.display = "none";
}

function showOption(){
	hideSpan('spanShowOption');
	showSpan('spanHideOption');
	showSpan('spanOptionContent');
	document.getElementById('tdOption').className = '';
}

function hideOption(){
	hideSpan('spanOptionContent');
	hideSpan('spanHideOption');
	showSpan('spanShowOption');
	document.getElementById('tdOption').className = 'report_show_option';
}



function checkForm() {
	if(document.getElementById('Type1').checked==false && document.getElementById('Type2').checked==false && document.getElementById('Type3').checked==false) {
		alert("<?=$i_alert_pleaseselect." ".$Lang['StudentRegistry']['ReportType']?>");
		document.getElementById('Type1').focus();
		return false;
	}
	else if(document.getElementById('list').checked==false && document.getElementById('statistics').checked==false) {
		alert("<?=$i_alert_pleaseselect." ".$Lang['StudentRegistry']['ReportType']?>");
		document.getElementById('list').focus();
		return false;
	}
	else if(document.getElementById('dateSelect1').checked==true && (document.getElementById('AcademicYearID').value=="" || document.getElementById('AcademicYearID').value==0)) {
		alert("<?=$i_alert_pleaseselect." ".$Lang['General']['SchoolYear']?>");
		document.getElementById('AcademicYearID').focus();
		return false;
	}
	else if(document.getElementById('dateSelect2').checked==true && (document.getElementById('startDate').value == "" || document.getElementById('endDate').value == "")) {
		alert("<?=$i_alert_pleaseselect?><?=$iDiscipline['Period']?>");	
		return false;
	}
	else if(document.getElementById('dateSelect2').checked==true && ((!check_date(document.form1.startDate,'<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>')) || (!check_date(document.form1.endDate,'<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>'))))
	{
		return false;
	}
	else if(document.getElementById('dateSelect2').checked==true && (document.getElementById('startDate').value > document.getElementById('endDate').value)) {
		alert("<?=$i_Discipline_System_Reports_Invalid_Date_Compare?>");
		return false;	
	}
	else {
		//document.form1.action = "index.php";
		//document.form1.submit();
		return true;
	}
	
}

function goGenerate() {
	if(checkForm()) {
		document.form1.action = "index.php";
		document.form1.target = "_self";
		document.form1.submit();
	}
}

function goPrint() {
	if(checkForm()) {
		document.form1.action = "print.php";
		document.form1.target = "_blank";
		document.form1.submit();
	}
}
</script>
<form name="form1" method="post" action="">
<table width="99%" border="0" cellspacing="0" cellpadding="3" align="center">
	<tr align="left" <? if($flag==1) echo "class=\"report_show_option\""; ?>>
		<?=$optionString?>
	</tr>
</table>
<table width="99%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td class="main_content">
			<span id="spanOptionContent">
			<? if($flag!=1) { ?>
			<div class="report_option report_option_title">- <?=$Lang['StudentRegistry']['Options']?> - </div>
			<? } ?>
			<div class="table_board">
				<table class="form_table_v30">
					<tr>
						<td rowspan="2" class="field_title"><?=$Lang['StudentRegistry']['ReportType']?></td>
						<td><input type="radio" value="<?=STUDENT_REGISTRY_RECORDSTATUS_LEFT?>" name="Type" id="Type1" onClick="loadText(<?=STUDENT_REGISTRY_RECORDSTATUS_LEFT?>)" <? if($Type==STUDENT_REGISTRY_RECORDSTATUS_LEFT || !isset($Type) || $Type=="") echo "checked"; ?>/>
						<label for="Type1"><?=$Lang['StudentRegistry']['RegistryStatus_Left']?></label>
						<input type="radio" value="<?=STUDENT_REGISTRY_RECORDSTATUS_SUSPEND?>" name="Type" id="Type2" onClick="loadText(<?=STUDENT_REGISTRY_RECORDSTATUS_SUSPEND?>)" <? if($Type==STUDENT_REGISTRY_RECORDSTATUS_SUSPEND) echo "checked"; ?>/>
						<label for="Type2"><?=$Lang['StudentRegistry']['RegistryStatus_Suspend']?></label>
						<input type="radio" value="<?=STUDENT_REGISTRY_RECORDSTATUS_RESUME?>" name="Type" id="Type3" onClick="loadText(<?=STUDENT_REGISTRY_RECORDSTATUS_RESUME?>)" <? if($Type==STUDENT_REGISTRY_RECORDSTATUS_RESUME) echo "checked"; ?>/> 
						<label for="Type3"><?=$Lang['StudentRegistry']['RegistryStatus_Resume']?></label></td>
					</tr>
					<tr>
						<td><input type="checkbox" name="list" id="list"  value="1" <? if($list==1) echo "checked"; ?>/>
						<label for="list"><span id="spanList"></span></label>
						<br />
						<input type="checkbox" name="statistics" id="statistics" value="1"  <? if($statistics==1) echo "checked"; ?>/>
						<label for="statistics"><span id="spanStatistics"></span></label></td>
					</tr>
					<col class="field_title" />
					<col  class="field_c" />
					<tr>
						<td class="field_title"><?=$Lang['General']['SchoolYear']?><span class="tabletextrequire">*</span></td>
						<td>
							<table>
								<tr><td><input type="radio" name="dateSelect" id="dateSelect1" value="YEAR" <? if(!isset($dateSelect) || $dateSelect=="YEAR") echo "checked"; ?>></td><td onClick="document.form1.dateSelect1.checked=true"><?=$yearSelection?></td><tr>
								<tr><td><input type="radio" name="dateSelect" id="dateSelect2" value="DATE" <? if(isset($dateSelect) && $dateSelect=="DATE") echo "checked"; ?>></td><td onClick="document.form1.dateSelect2.checked=true" onFocus="document.form1.dateSelect2.checked=true"><?=$linterface->GET_DATE_PICKER("startDate",$startDate)." $i_To".$linterface->GET_DATE_PICKER("endDate",$endDate)?></td><tr>
							</table>
						</td>
					</tr>
				</table>
				<span class="tabletextremark"><span class="tabletextrequire">*</span><?=$Lang['StudentRegistry']['SelectDateWithinSameAcademicYear']?></span>
				<p class="spacer"></p>
			</div>
			<div class="edit_bottom">
				<p class="spacer"></p>
				<?= $linterface->GET_ACTION_BTN($Lang['StudentRegistry']['GenerateReport'], "button", "goGenerate()")?>
				<p class="spacer"></p>
			</div>
			</span>
			<? if($flag==1) { ?>
			<div class="table_filter"> <?=$Lang['General']['SchoolYear']?> : <?=$lsr->getAcademicYearNameByYearID($AcademicYearID)?>  <?=$Lang['StudentRegistry']['ReportType']?> : <?=$typeText?></div>
			<p class="spacer"></p>
			<? } ?>
			<?=$tableContent?>
			<?if($flag==1) { ?>
			<div class="edit_bottom">
				<p class="spacer"></p>
				<?=$linterface->GET_ACTION_BTN($button_print, "button", "goPrint()")?>
				<p class="spacer"></p>
			</div>			
			<? } ?>
		</td>
	</tr>
</table>
<?=$initialString?>
<input type="hidden" name="flag" id="flag" value="1" />
</form>
<script language="javascript">
<?
if($flag==1)
	echo "loadText(".$Type.");";
else 
	echo "loadText(".STUDENT_REGISTRY_RECORDSTATUS_LEFT.");";
?>

</script>
<?php

$linterface->LAYOUT_STOP();
intranet_closedb();

?>