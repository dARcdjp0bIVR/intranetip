<?php 
# using: 

################# Change Log [Start] ############
#
#	2017-07-13 [Cameron]
#		- Fix bug: correct column title for $Lang['StudentRegistry']['ParentEMailAddress']
#
#	2015-09-14 [Cameron]
#		- Fix bug on not limit number of records in $sql_sr 
#
################# Change Log [End] ############

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");

if(!$sys_custom['OnlineRegistry'] || !$plugin['AccountMgmt_StudentRegistry'] || (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"])) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$CurrentPageArr['StudentRegistry'] = 1;
$CurrentPage = "Settings_OnlineReg";
$lsr = new libstudentregistry();
$lgroup = new libgroup();

# Left menu 
$TAGS_OBJ[] = array($Lang['StudentRegistry']['Settings']['OnlineReg']);
$MODULE_OBJ = $lsr->GET_MODULE_OBJ_ARR();


$NewStudentStartDate = $lsr->NewStudentStartDate;
$NewStudentEndDate = $lsr->NewStudentEndDate;
$new_period = $NewStudentStartDate ? $NewStudentStartDate . " " . $eEnrollment['to'] . " " . $NewStudentEndDate : $Lang['General']['EmptySymbol'];
$GroupID = $lsr->GroupID;
$group_selection = $lgroup->displayGroupSelection("name='GroupID'", $GroupID);
$GroupTitleInfo = $lgroup->returnGroup($GroupID);
$GroupTitle = ($intranet_session_language=="en" ? $GroupTitleInfo[0]['Title'] : $GroupTitleInfo[0]['TitleChinese']);


# retrieve all parent ID, phone

$sql_p = "select iu.UserID as ParentID, iu.MobileTelNo, iu.UserEmail, iu.ChineseName, iu.EnglishName from INTRANET_USER as iu, INTRANET_USERGROUP AS iug WHERE iug.GroupID='$GroupID' AND iug.UserID=iu.UserID order by iu.UserID";
$rows_p = $lgroup->returnResultSet($sql_p);
//debug_r($rows_p);
for ($i=0; $i<sizeof($rows_p); $i++)
{
	$ParentContacts[$rows_p[$i]["ParentID"]] = $rows_p[$i];
}

# STUDENT_REGISTRY_PG 
$sql_pr = "select ipr.StudentID, ipr.ParentID from INTRANET_PARENTRELATION as ipr, INTRANET_USERGROUP AS iug WHERE iug.GroupID='$GroupID' AND iug.UserID=ipr.ParentID order by ipr.ParentID";
$rows_s = $lgroup->returnArray($sql_pr);
//debug_r($rows_s);
$Std2ParentArr = build_assoc_array($rows_s);
//debug_r($Std2ParentArr);
for ($i=0; $i<sizeof($rows_s); $i++)
{
	$std_id_arr[] = $rows_s[$i]["StudentID"];
}
$std_ids = implode(", ", $std_id_arr);

$sql_sr = "Select TEL, MOBILE, EMAIL, StudentID, NAME_E, NAME_C, PG_TYPE From STUDENT_REGISTRY_PG where SEQUENCE=1 and StudentID in ($std_ids) order by DateModified desc";
$rows_sr = $lgroup->returnResultSet($sql_sr);
//debug_r($rows_sr);

$NumOfDiff = 0;
for ($i=0; $i<sizeof($rows_sr); $i++)
{
	$row_obj = $rows_sr[$i];
	$curr_student_id = $row_obj["StudentID"];
	$curr_parent_id = $Std2ParentArr[$curr_student_id];
	$parent_account_obj = $ParentContacts[$curr_parent_id];
	
	$IsDifferent = false;
	$sql_updates = "";
	
	$EmailDiff = "";
	$MobileDiff = "";

	if (trim(strtoupper($parent_account_obj["UserEmail"]))!=trim(strtoupper($row_obj["EMAIL"])) && trim($row_obj["EMAIL"])!="")
	{
		//debug("different email: ". $parent_account_obj["UserEmail"] . " vs ".$row_obj["EMAIL"]);
		$IsDifferent = true;
		$sql_updates .= "UserEmail='".addslashes(trim($row_obj["EMAIL"]))."'";
		$EmailDiff = "orange";
	}
	
	if (trim($parent_account_obj["MobileTelNo"])!=trim($row_obj["MOBILE"]) && trim($row_obj["MOBILE"])!="")
	{
		//debug("different mobile: ". $parent_account_obj["MobileTelNo"] . " vs ".$row_obj["MOBILE"]);
		$IsDifferent = true;
		$sql_updates .= ($sql_updates!="" ? ", " : "") . " MobileTelNo='".trim($row_obj["MOBILE"])."'";
		$MobileDiff = "orange";
	}
	
	if ($IsDifferent)
	{
		$NumOfDiff ++;
		if ($IsUpdateNow)
		{
			# perform update!!!
			$sql_update = "UPDATE INTRANET_USER SET {$sql_updates} WHERE UserID='{$curr_parent_id}'";
			 $lgroup->db_db_query($sql_update);
			//debug($sql_update);
		} else
		{
			# show the difference
			$gType = $row_obj["PG_TYPE"]==1 ? $Lang['StudentRegistry']['Father'] : ($row_obj["PG_TYPE"]==2 ? $Lang['StudentRegistry']['Mother'] : $g_result['PG_TYPE_OTHERS']);
			$table_rows .= "<tr><td>{$NumOfDiff}</td><td>".$parent_account_obj["ChineseName"]."<br />".$parent_account_obj["EnglishName"]."</td><td>".$parent_account_obj["MobileTelNo"]."</td><td>".$parent_account_obj["UserEmail"]."</td>" .
					"<td>".$gType."</td><td>".$row_obj["NAME_C"]."<br />".$row_obj["NAME_E"]."</td><td><font color='{$MobileDiff}'>".$row_obj["MOBILE"]."</font></td><td><font color='{$EmailDiff}'>".$row_obj["EMAIL"]."</font></td></tr>";
		}
	}
}

if (!$IsUpdateNow)
{
	
	# Start layout
	$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$xmsg]);
?>
	

<script language="javascript">
<!--	

function SynNow()
{
	self.location = "syn_contacts.php?IsUpdateNow=1";
}

function CancelNow()
{
	self.location = "index.php";
}
//-->
</script>	
<form name="form1" method="get" action="edit_update.php">

<div class="table_board"> 
	
	<p><b> <?=$Lang['StudentRegistry']['Menu']['Syn2GuardianIntro']?> </b></p>
	<table class='common_table_list_v30 '>
		<thead>
		<tr class='tabletop'>
		
			<th rowspan="2" width="1%">#</td>
			<th colspan="3" align="center"><?=$Lang['Header']['Menu']['ParentAccount']?></td>
			<th colspan="4" align="center"><?=$Lang['Menu']['AccountMgmt']['StudentRegistryInfo']?></td>
		</tr>
		<tr>
			<th width="15%"><?=$Lang['AccountMgmt']['UserName']?></td>
			<th width="13%" ><?=$Lang['StudentRegistry']['MobilePhoneNo']?></td>
			<th width="15%" ><?=$Lang['StudentRegistry']['ParentEMailAddress']?></td>
			<th width="8%" ><?=$Lang['StudentRegistry']['GuardianStudentRelationship']?></td>
			<th width="15%" ><?=$Lang['AccountMgmt']['UserName']?></td>
			<th width="13%"><?=$Lang['StudentRegistry']['MobilePhoneNo']?></td>
			<th width="15%" ><?=$Lang['StudentRegistry']['ParentEMailAddress']?></td>
		</tr>
		</thead>
		<?= $table_rows ?>
	</table>
	
	<div class="edit_bottom_v30">
		<p class="spacer"></p>
		<?= $linterface->GET_ACTION_BTN($Lang['StudentRegistry']['Menu']['Syn2GuardianConfirm'], "button","SynNow()","UpdateBtn", "") ?>
		<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "CancelNow();","cancelbtn", "") ?>
		<p class="spacer"></p>
	</div>
</div>

</form>

<?php

}


intranet_closedb();

if (!$IsUpdateNow)
{
	$linterface->LAYOUT_STOP();
} else
{
	header("location: index.php?xmsg=UpdateSuccess");
} 
?>
