<?php
# using: 

################# Change Log [Start] ############
#
#   2019-07-08 [Cameron]
#       - hide CurrentStudentText2Display for Kentville [case #Y164476]
#
#   2019-03-25 [Cameron]
#       - update settings for current student only when $sys_custom['StudentRegistry']['Kentville'] is set
#
################# Change Log [End] ############

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

if(!$sys_custom['OnlineRegistry'] || !$plugin['AccountMgmt_StudentRegistry'] || (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"])) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
// include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry.php");

intranet_auth();
intranet_opendb();

$lsr = new libstudentregistry();
$lgeneralsettings = new libgeneralsettings();

$data = array();
if (!$sys_custom['StudentRegistry']['Kentville']) {
    $data['NewStudentStartDate'] = $NewStudentStartDate;
    $data['NewStudentEndDate'] = $NewStudentEndDate;
    $data['NewStudentText2Display'] = $NewStudentText2Display;
    $data['GroupID'] = $GroupID;
}
$data['CurrentStudentStartDate'] = $CurrentStudentStartDate;
$data['CurrentStudentEndDate'] = $CurrentStudentEndDate;
if (!$sys_custom['StudentRegistry']['Kentville']) {
    $data['CurrentStudentText2Display'] = $CurrentStudentText2Display;
}
if(!empty($YearID))
	$data['YearIDStr'] = "," . implode(",",$YearID) .",";

# store in DB
$lgeneralsettings->Save_General_Setting($lsr->Module, $data);

# set $this->
if (!$sys_custom['StudentRegistry']['Kentville']) {
    $lsr->NewStudentStartDate = $NewStudentStartDate;			
    $lsr->NewStudentEndDate = $NewStudentEndDate;		
    $lsr->GroupID = $GroupID;
}
$lsr->CurrentStudentStartDate = $CurrentStudentStartDate;		
$lsr->CurrentStudentEndDate = $CurrentStudentEndDate;		
$lsr->YearIDStr = $data['YearIDStr'];		

# update session
foreach($data as $name=>$val)
	$_SESSION["SSV_PRIVILEGE"]["StudentRegistry"][$name] = $val;

intranet_closedb();
header("Location: index.php?xmsg=UpdateSuccess");
?>