<?php 
# using: 

################# Change Log [Start] ############
#
#   2019-07-08 [Cameron]
#       - hide "Message to display after login" for parent for Kentville [case #Y164476]
#
#   2019-03-25 [Cameron]
#       - show setting for current student's parent only if $sys_custom['StudentRegistry']['Kentville'] is true [Y140316]
#
#	2017-07-13 [Cameron]
#		- Fix bug: correct field title for $Lang['StudentRegistry']['Settings']['Text2Display4Current']
#
#	2017-06-08 [Cameron]
#		- Fix bug: should trim email for comparison so that they are consistent with syn_contacts.php
#
#	2015-09-14 [Cameron]
#		- Fix bug on not limit number of records in $sql_sr 
#
#	2015-09-14 [Cameron] 
#		- Add filter "iug.GroupID>0" in retrieving ParentID and STUDENT REGISTRY so that it won't show 
#		"Synchronize Contact (mobile # and email)" button for empty GroupID 
#
################# Change Log [End] ############

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");

if(!$sys_custom['OnlineRegistry'] || !$plugin['AccountMgmt_StudentRegistry'] || (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"])) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$CurrentPageArr['StudentRegistry'] = 1;
$CurrentPage = "Settings_OnlineReg";
$lsr = new libstudentregistry();
$lgroup = new libgroup();

# Left menu 
$TAGS_OBJ[] = array($Lang['StudentRegistry']['Settings']['OnlineReg']);
$MODULE_OBJ = $lsr->GET_MODULE_OBJ_ARR();

# Start layout
$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$xmsg]);

$NewStudentStartDate = $lsr->NewStudentStartDate;
$NewStudentEndDate = $lsr->NewStudentEndDate;
$new_period = $NewStudentStartDate ? $NewStudentStartDate . " " . $eEnrollment['to'] . " " . $NewStudentEndDate : $Lang['General']['EmptySymbol'];
$GroupID = $lsr->GroupID;

$group_selection = $lgroup->displayGroupSelection("name='GroupID'", $GroupID);

$GroupTitleInfo = $lgroup->returnGroup($GroupID);

$GroupTitle = ($intranet_session_language=="en" ? $GroupTitleInfo[0]['Title'] : $GroupTitleInfo[0]['TitleChinese']);

$NewStudentText2Display = $lsr->NewStudentText2Display;


# retrieve all parent ID, phone
// !!! 2015-09-11 Note: the UserID get from following query can be Teacher, Student and Parent, suppose user select a group where group members are parent
$sql_p = "select iu.UserID as ParentID, iu.MobileTelNo, iu.UserEmail from INTRANET_USER as iu, INTRANET_USERGROUP AS iug WHERE iug.GroupID='$GroupID' AND iug.GroupID>0 AND iug.UserID=iu.UserID order by iu.UserID";
$rows_p = $lgroup->returnResultSet($sql_p);

//debug_r($rows_p);
for ($i=0; $i<sizeof($rows_p); $i++)
{
	$ParentContacts[$rows_p[$i]["ParentID"]] = $rows_p[$i];
}
//debug_r($ParentContacts);

# STUDENT_REGISTRY_PG 
$sql_pr = "select ipr.StudentID, ipr.ParentID from INTRANET_PARENTRELATION as ipr, INTRANET_USERGROUP AS iug WHERE iug.GroupID='$GroupID' AND iug.GroupID>0 AND iug.UserID=ipr.ParentID order by ipr.ParentID";
$rows_s = $lgroup->returnArray($sql_pr);


//debug_r($rows_s);
/* $Std2ParentArr = array($key => $value)
 * $key: 	StudentID
 * $value: 	ParentID
 */
$Std2ParentArr = build_assoc_array($rows_s);

//debug_r($Std2ParentArr);
for ($i=0; $i<sizeof($rows_s); $i++)
{
	$std_id_arr[] = $rows_s[$i]["StudentID"];
}

if (sizeof($std_id_arr)>0)
{
	$std_ids = implode(", ", $std_id_arr);
	
	$sql_sr = "Select TEL, MOBILE, EMAIL, StudentID From STUDENT_REGISTRY_PG where SEQUENCE=1 and StudentID in ($std_ids) order by DateModified desc";
	$rows_sr = $lgroup->returnResultSet($sql_sr);
}
//debug_r($rows_sr);

$NumOfDiff = 0;
for ($i=0; $i<sizeof($rows_sr); $i++)
{
	$row_obj = $rows_sr[$i];
	$curr_student_id = $row_obj["StudentID"];
	$curr_parent_id = $Std2ParentArr[$curr_student_id];
	$parent_account_obj = $ParentContacts[$curr_parent_id];
	
	$IsDifferent = false;
	
	if (trim(strtoupper($parent_account_obj["UserEmail"]))!=trim(strtoupper($row_obj["EMAIL"])) && trim($row_obj["EMAIL"])!="")
	{
		//debug("different email: ". $parent_account_obj["UserEmail"] . " vs ".$row_obj["EMAIL"]);
		$IsDifferent = true;
	}
	
	if (trim($parent_account_obj["MobileTelNo"])!=trim($row_obj["MOBILE"]) && trim($row_obj["MOBILE"])!="")
	{
		//debug("different mobile: ". $parent_account_obj["MobileTelNo"] . " vs ".$row_obj["MOBILE"]);
		$IsDifferent = true;
	}
	
	if ($IsDifferent)
	{
		$NumOfDiff ++;
	}
}

if ($NumOfDiff>0)
{
	$SynContact = "<br /><font color=\"red\">{$NumOfDiff} ".$Lang['StudentRegistry']['Menu']['Syn2Guardian_difference']."</font><br />".$linterface->GET_SMALL_BTN($Lang['StudentRegistry']['Menu']['Syn2Guardian'], "button","javascript:SynContacts();","EditBtn")." ".$Lang['StudentRegistry']['Menu']['Syn2Guardian_remark']." <!--<font color='grey'>(Last synchronized at 2015-05-26 09:50:01)--></font>";
}

include_once($PATH_WRT_ROOT.'templates/html_editor/fckeditor.php');
$oFCKeditor = new FCKeditor("NewStudentText2Display",600,180);
$oFCKeditor->Value =  $NewStudentText2Display;
$NewStudentText2DisplayEdit = $oFCKeditor->Create2();



$CurrentStudentText2Display = $lsr->CurrentStudentText2Display;

$oFCKeditor = new FCKeditor("CurrentStudentText2Display",600,180);
$oFCKeditor->Value =  $CurrentStudentText2Display;
$CurrentStudentText2DisplayEdit = $oFCKeditor->Create2();


$CurrentStudentStartDate = $lsr->CurrentStudentStartDate;
$CurrentStudentEndDate = $lsr->CurrentStudentEndDate;
$current_period = $CurrentStudentStartDate ? $CurrentStudentStartDate . " " . $eEnrollment['to'] . " " . $CurrentStudentEndDate : $Lang['General']['EmptySymbol'];
$selected_YearID = explode(",",$lsr->YearIDStr);

if(sizeof($selected_YearID)>0)
{
	$lyear = new Year();
	foreach($selected_YearID as $k=>$d)
	{
		if(trim($d))
		{
			$lyear->YearID = $d;
			$lyear->Get_Year_Info();
			$YearDisplay .= $lyear->YearName . "<br>";
		}
	}	
}

$libFCM = new form_class_manage();
$formInfo = $libFCM->Get_Form_List();
$year_selection = "";
foreach($formInfo as $k=>$d)
{
	$selected = in_array($d['YearID'], $selected_YearID) ? "checked" : "";
	$year_selection .= "<input name='YearID[]' type='checkbox' id='". $d['YearID'] ."' value='". $d['YearID'] ."' $selected> <label for='". $d['YearID'] ."'>" . $d['YearName'] ."</label><br>";
}
?>

<script language="javascript">
<!--
function EditInfo()
{
	$('#EditBtn').attr('style', 'visibility: hidden'); 
	$('.Edit_Hide').attr('style', 'display: none');
	$('.Edit_Show').attr('style', '');
	
	$('#UpdateBtn').attr('style', '');
	$('#cancelbtn').attr('style', '');
}		

function ResetInfo()
{
	document.form1.reset();
	$('#EditBtn').attr('style', '');
	$('.Edit_Hide').attr('style', '');
	$('.Edit_Show').attr('style', 'display: none');
	
	$('#UpdateBtn').attr('style', 'display: none');
	$('#cancelbtn').attr('style', 'display: none');
}

function SynContacts()
{
	self.location = "syn_contacts.php";
}
//-->
</script>
		
<form name="form1" method="get" action="edit_update.php">

<div class="table_board">
	<div class="table_row_tool row_content_tool">
		<?= $linterface->GET_SMALL_BTN($button_edit, "button","javascript:EditInfo();","EditBtn")?>
	</div>
	
	<? ########################## ?>
	<? # New Student        # ?>
	<? ########################## ?>
<?php if (!$sys_custom['StudentRegistry']['Kentville']):?>	
	<p class="spacer"></p>
	<div class="form_sub_title_v30"><em>- <span class="field_title"> <?=$Lang['StudentRegistry']['Settings']['NewStudent']?> </span>-</em>
		<p class="spacer"></p>
	</div>

	<table class="form_table_v30">
			<tr valign='top'>
				<td class="field_title" nowrap><?=$Lang['StudentRegistry']['Settings']['ReleasePeriod']?></td>
				<td> 
				<span class="Edit_Hide"><?=$new_period?></span>
				<span class="Edit_Show" style="display:none">
					<?= $linterface->GET_DATE_PICKER("NewStudentStartDate",$NewStudentStartDate) ?>
					&nbsp;<?= $eEnrollment['to']?>&nbsp;
					<?= $linterface->GET_DATE_PICKER("NewStudentEndDate",$NewStudentEndDate) ?>
				</span></td>
			</tr>
			
			<tr valign='top'>
				<td class="field_title" nowrap><?=$Lang['Header']['Menu']['Group']?></td>
				<td>
				<span class="Edit_Hide"><?=$GroupTitle ? $GroupTitle : $i_general_NotSet ?><?= $SynContact ?></span>
				<span class="Edit_Show" style="display:none"><?=$group_selection?></span></td>
			</tr> 
			
			<tr valign='top'>
				<td class="field_title" nowrap><?=$Lang['StudentRegistry']['Settings']['Text2Display']?></td>
				<td>
				<span class="Edit_Hide"><table width="600" border="0"><tr><td><?=$NewStudentText2Display ?></td></tr></table></span>
				<span class="Edit_Show" style="display:none"><?=$NewStudentText2DisplayEdit?></span></td>
			</tr> 
	</table>                        
	
	<? ########################## ?>
	<? # Current Student        # ?>
	<? ########################## ?>
	<p class="spacer"></p>
	<div class="form_sub_title_v30"><em>- <span class="field_title"> <?=$Lang['StudentRegistry']['Settings']['CurrentStudent']?> </span>-</em>
		<p class="spacer"></p>
	</div>
<?php endif;?>

	<table class="form_table_v30">
			<tr valign='top'>
				<td class="field_title" nowrap><?=$Lang['StudentRegistry']['Settings']['ReleasePeriod']?></td>
				<td> 
				<span class="Edit_Hide"><?=$current_period?></span>
				<span class="Edit_Show" style="display:none">
					<?= $linterface->GET_DATE_PICKER("CurrentStudentStartDate",$CurrentStudentStartDate) ?>
					&nbsp;<?= $eEnrollment['to']?>&nbsp;
					<?= $linterface->GET_DATE_PICKER("CurrentStudentEndDate",$CurrentStudentEndDate) ?>
				</span></td>
			</tr>
			
			<tr valign='top'>
				<td class="field_title" nowrap><?=$Lang['General']['Form']?></td>
				<td>
				<span class="Edit_Hide"><?=$YearDisplay ? $YearDisplay : $i_general_NotSet ?></span>
				<span class="Edit_Show" style="display:none"><?=$year_selection?></span></td>
			</tr>
<?php if (!$sys_custom['StudentRegistry']['Kentville']):?>
			<tr valign='top'>
				<td class="field_title" nowrap><?=$Lang['StudentRegistry']['Settings']['Text2Display4Current']?></td>
				<td>
				<span class="Edit_Hide"><table width="600" border="0"><tr><td><?=$CurrentStudentText2Display ?></td></tr></table></span>
				<span class="Edit_Show" style="display:none"><?=$CurrentStudentText2DisplayEdit?></span></td>
			</tr>
<?php endif;?>
	</table>     
	
	<div class="edit_bottom_v30">
		<p class="spacer"></p>
		<?= $linterface->GET_ACTION_BTN($button_update, "submit","","UpdateBtn", "style='display: none'") ?>
		<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "ResetInfo();","cancelbtn", "style='display: none'") ?>
		<p class="spacer"></p>
	</div>
</div>

</form>




<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
