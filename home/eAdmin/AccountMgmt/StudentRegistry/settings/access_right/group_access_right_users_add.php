<?php
// editing by : henry chow

############# Change Log [Start]
#
#	Date:   2010-07-27 Thomas
#			Change the wording of the warning for blank group name
#			Modify Javascript Function:			
#			- Show_Edit_Icon
#			- Hide_Edit_Icon
#
############# Change Log [End]

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libaccessright.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry_ui.php");

intranet_auth();
intranet_opendb();

$laccessright = new libaccessright();


if(!$plugin['AccountMgmt_StudentRegistry'] || (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] && !$laccessright->CHECK_ACCESS("StudentRegistry-SETTINGS-AccessRight-Access"))) {
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

if(!$_REQUEST['GroupID'] || $_REQUEST['GroupID'] == '')
{
	header("Location: group_access_right_detail.php");
	intranet_closedb();
	exit();
}

$linterface = new interface_html();
$lsr = new libstudentregistry();
$lsrUI = new libstudentregistry_ui();

$stdInGroup = $lsr->memberInMgmtGroup($GroupID);

$name_field = getNameFieldByLang("USR.");
$typeList = "1";		# 1-teacher only	

if(sizeof($stdInGroup)>0)
	$conds = " AND USR.UserID NOT IN (".implode(',', $stdInGroup).")";

$sql1 = "SELECT USR.UserID, $name_field as name, ycu.ClassNumber, UserLogin FROM INTRANET_USER USR LEFT OUTER JOIN YEAR_CLASS_USER ycu ON (ycu.UserID=USR.UserID) WHERE USR.RecordType IN ($typeList) $conds GROUP BY USR.UserID ORDER BY UserLogin";
$result1 = $lsr->returnArray($sql1,5);

for ($j = 0; $j < sizeof($result1); $j++) 
{
	list($this_userid, $this_stu_name, $this_class_number, $this_userlogin) = $result1[$j];
	$data_ary[] = array($this_userid, $this_classname, $this_class_number, $this_stu_name, $this_userlogin);
}

if(!empty($data_ary)) 
{
	#define yui array (Search by input format )
	for($i=0;$i<sizeof($data_ary);$i++)
	{
		list($this_userid, $this_classname, $this_class_number, $this_stu_name, $this_userlogin) = $data_ary[$i];
	
		$temp_str = $this_classname . $this_class_number. " ". $this_stu_name;
		$temp_str2 = $this_stu_name;
		
		$liArr .= "[\"". addslashes(intranet_undo_htmlspecialchars($temp_str)) ."\", \"". addslashes(intranet_undo_htmlspecialchars($temp_str2)) ."\", \"". $this_userid ."\"]";
		($i == (sizeof($result)-1)) ? $liArr .= "" : $liArr .= ",\n";
	}
	
	foreach ($data_ary as $key => $row) 
	{
		$field1[$key] = $row[0];	//user id
		$field2[$key] = $row[1];	//class name
		$field3[$key] = $row[2];	//class number
		$field4[$key] = $row[3];	//stu name
		$field5[$key] = $row[4];	//login id
	}
	array_multisort($field5, SORT_ASC, $data_ary);
	
	#define yui array (Search by login id )
	for($i=0;$i<sizeof($data_ary);$i++)
	{
		list($this_userid, $this_classname, $this_class_number, $this_stu_name, $this_userlogin) = $data_ary[$i];
		$temp_str2 = $this_stu_name;
		
		$liArr2 .= "[\"". $this_userlogin ."\", \"". addslashes(intranet_undo_htmlspecialchars($temp_str2)) ."\", \"". $this_userid ."\"]";
		($i == (sizeof($result)-1)) ? $liArr2 .= "" : $liArr2 .= ",\n";
	}
	### end for searching
}
$student_selected = $linterface->GET_SELECTION_BOX($array_student, "name='student[]' id='student[]' class='select_studentlist' size='15' multiple='multiple'", "");

$button_remove_html = $linterface->GET_BTN($button_remove, "button", "javascript:checkOptionRemove(document.getElementById('student[]'))");

$CurrentPageArr['StudentRegistry'] = 1;
$CurrentPage = "Setting_RegistryAccessRight";

$ResultArr = $lsr->Get_Group_Info($GroupID);

$PAGE_NAVIGATION[] = array($i_Discipline_System_Group_Right_Navigation_Group_List, "index.php");
$PAGE_NAVIGATION[] = array($ResultArr[0]['GroupTitle'], "group_access_right_users.php?GroupID=$GroupID");
$PAGE_NAVIGATION[] = array($i_Discipline_System_Group_Right_Navigation_New_Member, "");

$TAGS_OBJ[] = array($Lang['AccountMgmt']['Settings']['AccessRights'], "", 0);

$MODULE_OBJ = $lsr->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START(urldecode($_REQUEST['Msg']));

//echo $lsrUI->Get_Group_Access_Right_Users_Index($_REQUEST['GroupID']);
?>
<style type="text/css">
    #statesmod {position:relative;}
    #statesautocomplete {position:relative;width:22em;margin-bottom:1em;}/* set width of widget here*/
    #statesautocomplete {z-index:9000} /* for IE z-index of absolute divs inside relative divs issue */
    #statesinput {_position:absolute;width:100%;height:1.4em;z-index:0;} /* abs for ie quirks */
    #statescontainer, #statescontainerCC, #statescontainerBCC {position:absolute;top:0.3em;width:100%}
    #statescontainer .yui-ac-content, #statescontainerCC .yui-ac-content, #statescontainerBCC .yui-ac-content {position:absolute;width:100%;border:1px solid #404040;background:#eeeeee;overflow:hidden;z-index:9050;}
    #statescontainer .yui-ac-shadow, #statescontainerCC .yui-ac-shadow, #statescontainerBCC .yui-ac-shadow {position:absolute;margin:.3em;width:100%;background:#a0a0a0;z-index:9049;}
    #statescontainer ul, #statescontainerCC ul, #statescontainerBCC ul {padding:5px 0;width:100%;}
    #statescontainer li, #statescontainerCC li, #statescontainerBCC li {padding:0 5px;cursor:default;white-space:nowrap;}
    #statescontainer li.yui-ac-highlight, #statescontainerCC li.yui-ac-highlight, #statescontainerBCC li.yui-ac-highlight {background:#bbbbbb;}
    #statescontainer li.yui-ac-prehighlight, #statescontainerCC li.yui-ac-prehighlight, #statescontainerBCC li.yui-ac-prehighlight {background:#FFFFFF;}
    
    
	#statesmod div,dl,dt,dd,ul,ol,li,h1,h2,h3,h4,h5,h6,pre,form,fieldset,input,textarea,p,blockquote{margin:0;padding:0;}
	#statesmod table{border-collapse:collapse;border-spacing:0;}
	#statesmod fieldset,img{border:0;}
	#statesmod address,caption,cite,code,dfn,em,strong,th,var{font-style:normal;font-weight:normal;}
	#statesmod ol,ul {list-style:none;}
	#statesmod caption,th {text-align:left;}
	#statesmod h1,h2,h3,h4,h5,h6{font-size:100%;font-weight:normal;}
	#statesmod q:before,q:after{content:'';}
	#statesmod abbr,acronym {border:0;}
	#statesmod {font:13px arial,helvetica,clean,sans-serif;*font-size:small;*font:x-small;}
</style>
<!-- In-memory JS array begins-->
<!-- Libary begins -->
<script type="text/javascript" src="<?= $PATH_WRT_ROOT?>templates/yui/autocomplete_files/yahoo.js"></script>
<script type="text/javascript" src="<?= $PATH_WRT_ROOT?>templates/yui/autocomplete_files/dom.js"></script>
<script type="text/javascript" src="<?= $PATH_WRT_ROOT?>templates/yui/autocomplete_files/event-debug.js"></script>
<script type="text/javascript" src="<?= $PATH_WRT_ROOT?>templates/yui/autocomplete_files/animation.js"></script>
<script type="text/javascript" src="<?= $PATH_WRT_ROOT?>templates/yui/autocomplete_files/autocomplete-debug-ip20.js"></script>
<!-- Library ends -->
<script type="text/javascript">

var statesArray = [
    <?= $liArr?>
];

var loginidArray = [
    <?= $liArr2?>
];

var delimArray = [
    ";"
];

function checkForm()
{
    obj = document.form1;
    
        if(obj.elements["student[]"].length != 0) {
	        for(i=0; i<obj.elements["student[]"].length; i++) {
		     	obj.elements["student[]"].options[i].selected = true;   
	        }
        	return true;
		}
        else
        {
                alert('<?=$i_Discipline_System_alert_PleaseSelectMember?>');
                return false;
        }
}
</script>
<!-- In-memory JS array ends-->
<form name="form1" method="POST" onsubmit="return checkForm();" action="group_access_right_users_add_update.php">
<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td class="navigation"><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
				</tr>
			</table>	
			<br style="clear: both;"/>
              <div class="shadetabs">
                <ul>
                  <li><a href="group_access_right_detail.php?GroupID=<?=$GroupID?>"><?=$Lang['StaffAttendance']['AccessRight']?></a></li>
                  <li class="selected"><a href="group_access_right_users.php?GroupID=<?=$GroupID?>"><?=$Lang['StudentRegistry']['UserList']?></a></li>
                </ul>
              </div>
								
			<table width="88%" border="0" cellpadding="5" cellspacing="0">
				<tr>
					<td>
						<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
							<tr valign="top">
								<td width="40%"><span class="tabletext"><?=$i_Discipline_System_Group_Right_Navigation_Choose_Member ?>  
								:</span></td>
								<td width="10" nowrap="nowrap"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="10"></td>
								<td width="60%" nowrap="nowrap"><span class="tabletext"><?=$i_Discipline_System_Group_Right_Navigation_Selected_Member2 ?> 
								:</span></td>
							</tr>
							<tr>
								<td valign="top" class="tablerow2">
									<table width="100%" border="0" cellpadding="5" cellspacing="0">
										<tr>
											<td class="tabletext"><?=$eDiscipline["SelectTeacherStaff"]?> </td>
										</tr>
										<tr>
										<td class="tabletext"><?= $linterface->GET_BTN($button_select, "button", "javascript:newWindow('/home/eAdmin/ResourcesMgmt/RepairSystem/settings/choose/index.php?fieldname=student[]&GroupID=$GroupID', 9)")?></td>
											</tr>
										<tr>
											<td class="tabletext"><i><?=$i_general_or?></i></td>
										</tr>
										<tr>
											<td class="tabletext"><?=$i_general_search_by_loginid?><br \>
												<div id="statesautocomplete">
													<input type="text" class="tabletext" name="search2" id="search2">
													<div id="statescontainerCC" style="left:142px; top:0px;">
														<div style="display: none; width: 199px; height: 0px;" class="yui-ac-content">
															<div style="display: none;" class="yui-ac-hd"></div>
															<div class="yui-ac-bd"></div>
															<div style="display: none;" class="yui-ac-ft"></div>
														</div>
														<div style="width: 0pt; height: 0pt;" class="yui-ac-shadow"></div>
													</div>
												</div>

											</td>
										</tr>
									</table>
								</td>
								<td valign="top" nowrap="nowrap">&nbsp;</td>
								<td valign="top" nowrap="nowrap">
									<table width="100%" border="0" cellpadding="3" cellspacing="0">
									<tr>
										<td><?=$student_selected ?>
										</td>
									</tr>
									<tr>
										<td align="right"><?=$button_remove_html?></td>
									</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td height="1" class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
				</tr>
			</table>
		<br>
		</td>
	</tr>
	<tr>
		<td align="center">
			<input type="hidden" name="flag" value="0" />
			<input type="hidden" name="GroupID" value="<?=$GroupID ?>" />
			<?= $linterface->GET_ACTION_BTN($button_submit, "submit") ?>&nbsp;
			<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:self.location.href='group_access_right_users.php?GroupID=$GroupID'") ?>
		</td>
	</tr>
</table>

</form>
<script type="text/javascript">
YAHOO.example.ACJSArray = function() {
    var oACDS, oACDS2, oAutoComp, oAutoComp2;
    return {
        init: function() {

            // Instantiate first JS Array DataSource
            /*
            oACDS = new YAHOO.widget.DS_JSArray(statesArray);

            // Instantiate first AutoComplete            
            oAutoComp = new YAHOO.widget.AutoComplete('search1','student[]', 'statescontainer', oACDS);
            oAutoComp.queryDelay = 0;
            oAutoComp.prehighlightClassName = "yui-ac-prehighlight";
            oAutoComp.useShadow = true;
            oAutoComp.minQueryLength = 0;
            */
            oACDS2 = new YAHOO.widget.DS_JSArray(loginidArray);
            oAutoComp2 = new YAHOO.widget.AutoComplete('search2','student[]', 'statescontainerCC', oACDS2);
            oAutoComp2.queryDelay = 0;
            oAutoComp2.prehighlightClassName = "yui-ac-prehighlight";
            oAutoComp2.useShadow = true;
            oAutoComp2.minQueryLength = 0;
        },

        validateForm: function() {
            // Validate form inputs here
            return false;
        }
    };
}();

YAHOO.util.Event.addListener(this,'load',YAHOO.example.ACJSArray.init);
</script>

<script type="text/javascript" src="<?= $PATH_WRT_ROOT?>templates/yui/autocomplete_files/dpSyntaxHighlighter.js"></script>
<script type="text/javascript">
dp.SyntaxHighlighter.HighlightAll('code');
</script>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
