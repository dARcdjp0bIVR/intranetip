<?php
// editing by : 

############# Change Log [Start]
#
#	Date:   2010-08-05 (Henry Chow)
#	Detail:	add access right of "Student Registry Status" 
#
#	Date:   2010-07-27 Thomas
#			Change the wording of the warning for blank group name
#			Modify Javascript Function:			
#			- Show_Edit_Icon
#			- Hide_Edit_Icon
#
############# Change Log [End]

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libaccessright.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry_ui.php");

intranet_auth();
intranet_opendb();

$laccessright = new libaccessright();

if(!$plugin['AccountMgmt_StudentRegistry'] || (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] && !$laccessright->CHECK_ACCESS("StudentRegistry-SETTINGS-AccessRight-Access"))) {
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}


$linterface = new interface_html();
$lsr = new libstudentregistry();
$lsrUI = new libstudentregistry_ui();

$CurrentPageArr['StudentRegistry'] = 1;
$CurrentPage = "Setting_RegistryAccessRight";

$TAGS_OBJ[] = array($Lang['AccountMgmt']['Settings']['AccessRights'], "", 0);
$MODULE_OBJ = $lsr->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START(urldecode($_REQUEST['Msg']));

echo $lsrUI->Get_Group_Access_Right_Settings_Index($_REQUEST['GroupID']);

$linterface->LAYOUT_STOP();
intranet_closedb();
?>
<script type="text/javascript" language="javascript">
function goSubmit() {
	if(document.getElementById('GroupTitle').value=="") {
		setTimeout("submitAction();", 500);	
	} else {
		document.GroupRightForm.submit();		
	}
	
}

function submitAction() {
	document.GroupRightForm.submit();	
}

function Check_Group(Obj)
{
	if (Obj.checked) 
		StyleClass = 'rights_selected';
	else
		StyleClass = 'rights_not_select';
	$('input.'+Obj.id+'-Check').attr('checked',Obj.checked);
	$('td.'+Obj.id+'-Cell').attr('class',Obj.id+'-Cell '+StyleClass);
}

function Check_Single(Obj)
{
	if(Obj != null)
	{
		if(Obj.checked)
		{ 
			$('Input#'+Obj.id).parent().removeClass('rights_not_select');
			$('Input#'+Obj.id).parent().addClass('rights_selected');
		}else
		{
			$('Input#'+Obj.id).parent().removeClass('rights_selected');
			$('Input#'+Obj.id).parent().addClass("rights_not_select");
		}
	}
}

function Show_Edit_Icon(IconID)
{
	$('#'+IconID).attr('class', '');
	//LayerObj = eval(LayerObj);
	//LayerObj.style.backgroundImage = "url(<?=$PATH_WRT_ROOT?>images/<?=$LAYOUT_SKIN?>/icon_edit_b.gif)";
	//LayerObj.style.backgroundPosition = "center right";
	//LayerObj.style.backgroundRepeat = "no-repeat";
	//LayerObj.style.border = "1px solid #FF0000";
}

function Hide_Edit_Icon(IconID)
{
	$('#'+IconID).attr('class', 'edit_dim');
	//LayerObj = eval(LayerObj);
	//LayerObj.style.backgroundImage = "";
	//LayerObj.style.backgroundPosition = "";
	//LayerObj.style.backgroundRepeat = "";
	//LayerObj.style.border = "";
}

function Check_Group_Title(GroupTitle,GroupID)
{
	var GroupID = GroupID || "";
	var PostVar = {
			"GroupID": GroupID,
			"GroupTitle": encodeURIComponent(GroupTitle)
			}
	var ElementObj = $('div#GroupTitleWarningLayer');
	
	if (Trim(GroupTitle) != "") {
		$.post('ajax_check_group_title.php',PostVar,
				function(data){
					if (data == "die"){ 
						window.top.location = '/';
					}
					else if (data == "1") {
						ElementObj.html('');
						ElementObj.hide();
						if (document.getElementById('GroupTitleWarningRow')) 
							document.getElementById('GroupTitleWarningRow').style.display = 'none';
					}
					else {
						ElementObj.html('<?=$Lang['StudentRegistry']['GroupNameDuplicateWarning']?>');
						ElementObj.show('fast');
						if (document.getElementById('GroupTitleWarningRow')) 
							document.getElementById('GroupTitleWarningRow').style.display = '';
					}
				}
				)
				;
	}
	else if(Trim(GroupTitle) == ""){
		ElementObj.html('<?=$Lang['StudentRegistry']['GroupNameDuplicateWarning']?>');
		ElementObj.show('fast');
		
		if (document.getElementById('GroupTitleWarningRow')) 
			document.getElementById('GroupTitleWarningRow').style.display = '';
	}
	
}

// jEditable function 
function Init_JEdit_Input(objDom)
{
	var WarningLayer = "div#GroupTitleWarningLayer";
	$(objDom).editable
	( 
      function(value, settings)
      {
    	var ElementObj = $(this);
    	
    	if (Trim($(WarningLayer).html()) == "" && ElementObj[0].revert != value)
    	{
    		if($('Input#GroupID').val() == '')
	    	{
	    		ElementObj.html(value);
	    		$('span#GroupTitleNavLayer').html(value);
				$('Input#GroupTitle').val(value);
	    	}
	    	else
	    	{
	    		var PostVar = {
	    			"GroupID":$('Input#GroupID').val(),
	    			"GroupTitle":encodeURIComponent(value)
	    		};
		    				     
		    	$.post('ajax_rename_group.php',PostVar,
		    		function (data) {
		    			Get_Return_Message(Trim(data));
		    			ElementObj.html(value);
						$('span#GroupTitleNavLayer').html(value);
						$('Input#GroupTitle').val(value);
		    		});
	    	}
		}
		else {
			ElementObj[0].reset();
		}
	  }, 
	  {
	    tooltip   : "<?=$Lang['SysMgr']['FormClassMapping']['ClickToEdit']?>",
	    event : "click",
	    onblur : "submit",
	    type : "text",     
	    style  : "display: inline",
	    height: "20px",
	    maxlength: 100,
	    onreset: function() {
	    	$(WarningLayer).html('');
	    	$(WarningLayer).hide();
	  	}
  	  }
  	);
  
  $(objDom).keyup
  (
  	 function()
  	 {
		var GroupTitle = Trim($('form input').val());
		var GroupID = $('Input#GroupID').val();
		Check_Group_Title(GroupTitle,GroupID);
		
	 }
  );
  
}

function Init_JEdit_Input2(objDom)
{
	//$(objDom).attr('style', '');
	
	$(objDom).editable
	(
      function(value, settings)
      {
    	var ElementObj = $(this);
    	ElementObj.html(value);
		$('Input#GroupDescription').val(value);
	
		if($('Input#GroupID').val() != '' && ElementObj[0].revert != value)
		{
			var PostVar = {
	    			"GroupID":$('Input#GroupID').val(),
	    			"GroupDescription":encodeURIComponent(value)
	    		};
		    				     
		    	$.post('ajax_set_group_description.php',PostVar,
		    		function (data) {
		    			Get_Return_Message(Trim(data));
		    		});
		}
	  }, 
	  {
	    tooltip   : "<?=$Lang['SysMgr']['FormClassMapping']['ClickToEdit']?>",
	    event : "click",
	    onblur : "submit",
	    type : "text",     
	    style  : "display: inline",
	    height: "20px",
	    maxlength: 100,
	    onreset: function() {
	    	
	  	}
  	  }
  	);
}

function jsClickEdit()
{
	$('span#EditableGroupTitle').click();
}

function jsClickEdit2()
{
	$('span#EditableGroupDescription').click();
}

// thick box function 
{
//on page load call tb_init
function Thick_Box_Init()
{   
	tb_init('a.thickbox, area.thickbox, input.thickbox');//pass where to apply thickbox
}
}

$(document).ready(
	function()
	{
		Check_Single('MGMT_RegistryInformation_View');
		Check_Single('MGMT_RegistryInformation_Manage');
		
		Check_Single('SETTINGS_AccessRight_Access');
		
		Init_JEdit_Input('span.jEditInput');
		Init_JEdit_Input2('span.jEditInput2');
		Thick_Box_Init();
	}
);



</script>
