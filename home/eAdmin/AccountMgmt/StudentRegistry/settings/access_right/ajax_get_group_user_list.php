<?php
//editing by 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccessright.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry_ui.php");

intranet_auth();
intranet_opendb();
$linterface = new interface_html();
$laccessright = new libaccessright();

if(!$plugin['AccountMgmt_StudentRegistry'] || (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] && !$laccessright->CHECK_ACCESS("StudentRegistry-SETTINGS-AccessRight-Access"))) {
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}


$lsr = new libstudentregistry();
$lsrUI = new libstudentregistry_ui();

$GroupID = $_REQUEST['GroupID'];
$Keyword = trim(urldecode(stripslashes($_REQUEST['Keyword'])));
//echo '<br style="clear: both;"/><div class="Conntent_tool">';
echo $lsrUI->Get_Group_Access_Right_Users_List($GroupID, $Keyword);
//echo '</div>';
intranet_closedb();
?>