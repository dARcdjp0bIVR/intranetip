<?php
// editing by : henry chow

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libaccessright.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry_ui.php");

intranet_auth();
intranet_opendb();

$lsr = new libstudentregistry();

$result = $lsr->Add_Member($GroupID, $student);

intranet_closedb();

$msg = ($result) ? "add" : "add_failed";

header("Location: group_access_right_users.php?xmsg=$msg&GroupID=".$GroupID);
?>
