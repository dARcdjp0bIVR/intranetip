<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libaccessright.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry_ui.php");

intranet_auth();
intranet_opendb();

$laccessright = new libaccessright();

if(!$plugin['AccountMgmt_StudentRegistry'] || (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] && !$laccessright->CHECK_ACCESS("StudentRegistry-SETTINGS-AccessRight-Access"))) {
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}


$linterface = new interface_html();
$lsr = new libstudentregistry();
$lsrUI = new libstudentregistry_ui();

$CurrentPageArr['StudentRegistry'] = 1;
$CurrentPage = "Setting_RegistryAccessRight";

$TAGS_OBJ[] = array($Lang['AccountMgmt']['Settings']['AccessRights'], "index.php");

$MODULE_OBJ['title'] = $Lang['Header']['Menu']['StudentRegistry'];

$MODULE_OBJ = $lsr->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START(urldecode($_REQUEST['Msg']));

echo $lsrUI->Get_Function_Access_Right_Settings_Index();

echo $lsrUI->Include_JS_CSS();

$linterface->LAYOUT_STOP();
intranet_closedb();
?>
<script language="javascript">
{
function Check_Go_Search(evt)
{
	var key = evt.which || evt.charCode || evt.keyCode;
	
	if (key == 13) // enter
		Get_Function_Group_List();
	else
		return false;
}
}

// ajax function
{
function Get_Function_Group_List()
{
	var PostVar = {
			Keyword: encodeURIComponent($('Input#Keyword').val())
			}

	Block_Element("FunctionGroupListLayer");
	$.post('ajax_get_function_group_list.php',PostVar,
					function(data){
						if (data == "die") 
							window.top.location = '/';
						else {
							$('div#FunctionGroupListLayer').html(data);
							Thick_Box_Init();
							UnBlock_Element("FunctionGroupListLayer");
						}
					});
}

function Delete_Group(GroupID)
{
	if (confirm('<?=$Lang['StaffAttendance']['AccessRightDeleteGroupWarning']?>')) {
		var PostVar = {
			"GroupID":GroupID
		}
		
		$.post('ajax_delete_group.php',PostVar,
			function(data) {
				if (data == "die") 
					window.top.location = '/';
				else {
					Get_Return_Message(data);
					Get_Function_Group_List();
					Scroll_To_Top();
				}
			});
	}
}
}

{
//on page load call tb_init
function Thick_Box_Init()
{   
	tb_init('a.thickbox, area.thickbox, input.thickbox');//pass where to apply thickbox
}
}
</script>