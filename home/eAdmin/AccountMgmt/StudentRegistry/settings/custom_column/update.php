<?php 

# using: Philips

################# Change Log [Start] ############
#
#	2020-03-12 [Philips]
#		- added Section Column
#
#   2019-04-24 [Philips]
#       - create file
#
################# Change Log [End] ##############

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");

if(!$plugin['AccountMgmt_StudentRegistry'] || (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"])) {
    include_once($PATH_WRT_ROOT."includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit;
}

include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");

intranet_auth();
intranet_opendb();

$lrs = new libstudentregistry();

$columnArr = $_POST['column'];
$columnID = $_POST['ColumnID'];
$displayText_ch = intranet_htmlspecialchars($_POST['DisplayText_ch']);
$displayText_en = intranet_htmlspecialchars($_POST['DisplayText_en']);
$code = intranet_htmlspecialchars($_POST['Code']);
$section = $_POST['Section'];
$isDelete = $_GET['delete'];
$action = $_POST['action'];

if($isDelete == 1){
    $result = $lrs->deleteCustomColumn($columnArr);
        if($result)
            $msg = "DeleteSuccess";
} else if($action == 'order'){
    $column = $_POST['column'];
    $result = $lrs->orderCustomColumn($column);
        if($result)
            $msg = "UpdateSuccess";
} else {
    if($columnID != ''){
        $updateArr = array(
            "ColumnID" => $columnID,
            "DisplayText_ch" => $displayText_ch,
            "DisplayText_en" => $displayText_en,
            "Code" => $code,
        	"Section" => $section
        );
        $result = $lrs->updateCustomColumn($updateArr);
        if($result)
            $msg = "EditSuccess";
    } else {
        $insertArr = array(
            "DisplayText_ch" => $displayText_ch,
            "DisplayText_en" => $displayText_en,
            "Code" => $code,
        	"Section" => $section
        );
        $result = $lrs->addCustomColumn($insertArr);
        if($result)
            $msg = "AddSuccess";
    }
}

header("Location: index.php?xmsg=$msg");

intranet_closedb();
?>