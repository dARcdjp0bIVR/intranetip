<?php 

# using: Philips

################# Change Log [Start] ############
#
#	2020-03-12 [Philips]
#		- added Section Column
#
#   2019-04-24 [Philips]
#       - create file
#
################# Change Log [End] ##############

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");

if(!$plugin['AccountMgmt_StudentRegistry'] || (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"])) {
    include_once($PATH_WRT_ROOT."includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit;
}

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$CurrentPageArr['CustomColumn'] = 1;
$CurrentPage = "Settings_CustomCol";
$lrs = new libstudentregistry();

# Left menu
$TAGS_OBJ[] = array($Lang['StudentRegistry']['Settings']['CustomCol']);
$MODULE_OBJ = $lrs->GET_MODULE_OBJ_ARR();

// Edit
if(isset($column)){
    $ColumnID = $column[0];
}

$headerArr = array("Code", "DisplayText_ch", "DisplayText_en", "Section");

if($plugin['StudentRegistry_HongKong']){
	$sectionArr = array(
			array('StudInfo', $Lang['StudentRegistry']['StudInfo']),
			array('LastSchool', $Lang['StudentRegistry']['LastSchool']),
	// 		array('BrotherSisterStudyingInSchool', $Lang['StudentRegistry']['BrotherSisterStudyingInSchool']),
			array('Guardian1', $Lang['StudentRegistry']['1st'] . $Lang['Identity']['Guardian']),
			array('Guardian2', $Lang['StudentRegistry']['2nd'] . $Lang['Identity']['Guardian']),
			array('OtherContactPerson', $Lang['StudentRegistry']['OtherContactPerson'])
	);
}else if($plugin['StudentRegistry_Macau']){
	$sectionArr = array(
			array('StudInfo', $Lang['StudentRegistry']['StudInfo']),
			array('FatherInfo', $Lang['StudentRegistry']['FatherInfo']),
			array('MotherInfo', $Lang['StudentRegistry']['MotherInfo']),
			array('GuardianInfo', $Lang['StudentRegistry']['GuardianInfo']),
			array('EmergencyContactInfo', $Lang['StudentRegistry']['EmergencyContactInfo']),
			array('ContactPersonInfo', $Lang['StudentRegistry']['ContactPersonInfo'])
	);
}
$PAGE_NAVIGATION = array();
$PAGE_NAVIGATION[] = array($button_new . ' ' .$Lang['StudentRegistry']['Settings']['CustomCol'], '');
$btn_submit = $linterface->GET_ACTION_BTN($button_submit, "button", "checkForm()");
$btn_cancel = $linterface->GET_ACTION_BTN($button_cancel, "button", "self.location='index.php'");

$x = "<form method='POST' name='form1' action='update.php' >";

// Get Column values if edit
if($ColumnID){
    $column = $lrs->getCustomColumn($ColumnID);
    $column = $column[0];
} else {
    $column = array();
}

$x .= '<div class="form_table">';
    $x .= '<table class="form_table_v30">';
    foreach($headerArr as $harr){
        $x .= '<tr>';
            $x .= '<td class="field_title"><span class="tabletextrequire">*</span>'. $Lang['StudentRegistry']['Settings']['Custom'][$harr] .'</td>';
        	$x .= '<td>';
        	   	if($harr != 'Section'){
        	   		$x .= '<input type="text" class="textbox_name" name="'.$harr.'" value="'.$column[$harr].'" required />';
        	   	} else {
        	   		$x .= getSelectByArray($sectionArr, 'name="'.$harr.'"', $column[$harr], $all=0, $noFirst=0, $Lang['StudentRegistry']['Settings']['CustomColNotSelected']);
        	   	}
    	       	$x .= "&nbsp;<span id='alert_$harr' style='color: #FF0000'></span>";
    	   $x .= '</td>';
        $x .= '</tr>';
    }
    $x .= '</table>';
$x .= '</div>';

// Button
$x .= '<div class="edit_bottom_v30">';
$x .= '<p class="spacer"></p>';
$x .= $btn_submit;
$x .= "&nbsp;";
$x .= $btn_cancel;
$x .= '</div>';

// hidden values
$x .= "<input type='hidden' name='ColumnID' value='$column[ColumnID]' />";

$x .= "</form>";
$linterface->LAYOUT_START();
?>
<?php echo $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION);?>
<?php echo $x?>
<script type="text/javascript">
$(document).ready(function(){
	$('#alert_Code').hide();
});
function checkForm(){
	if(document.form1.Code.value.match('[!@#$%^&*(),.?":{}|<>\\s\\t]')){
		$('#alert_Code').text('<?php echo $Lang['StudentRegistry']['NoSpecialCharacter']?>');
		$('#alert_Code').show();
		return false;
	} else {
		$('#alert_Code').hide();
	}
// 	if(document.form1.DisplayText_ch.value.match('[!@#$%^&*(),.?":{}|<>\\s\\t]')){
//		$('#alert_DisplayText_ch').text('<?php echo $Lang['StudentRegistry']['NoSpecialCharacter']?>');
// 		$('#alert_DisplayText_ch').show();
// 		return false;
// 	} else {
// 		$('#alert_DisplayText_ch').hide();
// 	}
// 	if(document.form1.DisplayText_en.value.match('[!@#$%^&*(),.?":{}|<>\\s\\t]')){
//		$('#alert_DisplayText_en').text('<?php echo $Lang['StudentRegistry']['NoSpecialCharacter']?>');
// 		$('#alert_DisplayText_en').show();
// 		return false;
// 	} else {
// 		$('#alert_DisplayText_en').hide();
// 	}
	var data = {
			"ColumnID" : document.form1.ColumnID.value,
			"Code" : document.form1.Code.value
		};
	$.ajax({
		url: 'ajax_checkCode.php',
		method: 'POST',
		data: data,
		success: function(result){
			if(result){
				$('#alert_Code').hide();
				document.form1.submit();
			} else {
				$('#alert_Code').text(data.Code + " <?php echo $Lang['General']['JS_warning']['CodeIsInUse']?>");
				$('#alert_Code').show();
			}
		}
	});
}
</script>
<?php 
$linterface->LAYOUT_STOP();
intranet_closedb();
?>