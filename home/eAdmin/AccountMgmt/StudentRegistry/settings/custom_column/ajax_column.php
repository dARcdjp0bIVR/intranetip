<?php 

# using: Philips

################# Change Log [Start] ############
#
#   2019-04-24 [Philips]
#       - create file
#
################# Change Log [End] ##############

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");

if(!$sys_custom['OnlineRegistry'] || !$plugin['AccountMgmt_StudentRegistry'] || (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"])) {
    include_once($PATH_WRT_ROOT."includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit;
}

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$lrs = new libstudentregistry();

$headerArr = array("DisplayText_ch", "DisplayText_en", "Code");
$PAGE_NAVIGATION = array();
$PAGE_NAVIGATION[] = array($button_new . ' ' .$Lang['StudentRegistry']['Settings']['CustomCol'], '');
$btn_submit = $linterface->GET_ACTION_BTN($button_submit, "submit");

$x = "<form method='POST' action='update.php'>";

// Get Column values if edit
if($_POST['ColumnID']){
    $column = $lrs->getCustomColumn($_POST['ColumnID']);
} else {
    $column = array();
}

$x .= '<div class="form_table">';
    $x .= '<table class="form_table_v30">';
    foreach($headerArr as $harr){
        $x .= '<tr>';
            $x .= '<td class="field_title"><span class="tabletextrequire">*</span>'. $Lang['StudentRegistry']['Settings']['Custom'][$harr] .'</td>';
        	$x .= '<td><input type="text" name="'.$harr.'" value="'.$column[$harr].'" required /></td>';
        $x .= '</tr>';
    }
    $x .= '</table>';
$x .= '</div>';

// Button
$x .= '<div class="edit_bottom_v30">';
$x .= '<p class="spacer"></p>';
$x .= $btn_submit;
$x .= '</div>';

// hidden values
$x .= "<input type='hidden' name='ColumnID' value='$column[ColumnID]' />";

$x .= "</form>";
?>
<?php echo $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION);?>
<?php echo $x?>
<?php intranet_closedb();?>