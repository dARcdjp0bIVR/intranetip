<?php 
# using: Philips

################# Change Log [Start] ############
#
#	2020-03-12 [Philips]
#		- added Section Column
#
#   2019-04-24 [Philips]
#       - create file
#
################# Change Log [End] ##############
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");

if(!$plugin['AccountMgmt_StudentRegistry'] || (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"])) {
    include_once($PATH_WRT_ROOT."includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit;
}

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");

intranet_auth();
intranet_opendb();

global $i_no_record_exists_msg;

$linterface = new interface_html();
$lsr = new libstudentregistry();

$AddBtn = $linterface->GET_LNK_ADD("setting.php", $button_new ,"","","",0);
$MoveBtn = $linterface->GET_LNK_MOVE("#", $Lang['SysMgr']['Location']['Reorder']['Room']);

$custCols = $lsr->getCustomColumn();
$headerArr = array("DisplayOrder", "Code","DisplayText_ch", "DisplayText_en", 'Section');

// Table tool
$toolsBar = "<div class='common_table_tool'>";
$toolsBar .= "<a href='javascript:checkEdit(document.form1, \"column[]\", \"setting.php\")' class='tool_edit' >$button_edit</a>";
$toolsBar .= "<a href='javascript:checkRemove(document.form1, \"column[]\", \"update.php?delete=1\")' class='tool_delete' >$button_delete</a>";
$toolsBar .= "</div>";


// Table Init
$table = "<table class='common_table_list_v30'>";
// Table Header
$table .= "<thead>";
$table .= "<tr>";
foreach($headerArr as $harr){
    $width = ($harr != 'DisplayOrder') ? '25%' : '1';
    $table .= "<th width='$width'>" . ( ($harr != 'DisplayOrder') ? $Lang['StudentRegistry']['Settings']['Custom'][$harr] : '#') . "</th>";
}
$table .= "<th width='1'></th>";
$table .= "<th width='1'></th>";
$table .= "</tr>";
$table .= "</thead>";

// Table Content
$table .= "<tbody id='sortable'>";
foreach($custCols as $cols){
    $table .= "<tr val_id='$cols[ColumnID]'>";
    foreach($headerArr as $harr){
    	if($harr != 'Section'){
        	$table .= "<td>" . $cols[$harr] . "</td>";
    	} else {
    		if($cols[$harr] == ''){
    			$displaySection = $Lang['StudentRegistry']['Settings']['CustomColNotSelected'];
    		} else if(is_int(strpos($cols[$harr],'Guardian'))){
    			if($plugin['StudentRegistry_HongKong']){
	    			if($cols[$harr] == 'Guardian1'){
	    				$displaySection = $Lang['StudentRegistry']['1st'] . $Lang['Identity']['Guardian'];
	    			} else if($cols[$harr] == 'Guardian2'){
	    				$displaySection = $Lang['StudentRegistry']['2nd'] . $Lang['Identity']['Guardian'];
	    			}
    			} else if($plugin['StudentRegistry_Macau']){
    				$displaySection = $Lang['StudentRegistry']['GuardianInfo'];
    			}
    		} else {
    			$displaySection = $Lang['StudentRegistry'][$cols[$harr]];
    		}
    		$table .= "<td>" . $displaySection . '</td>';
    	}
    }
    $table .= "<td class='Dragable'>" . $MoveBtn . "</td>";
    $table .= "<td><input type='checkbox' name='column[]' value='$cols[ColumnID]' /></td>";
    $table .= "</tr>";
}
if(count($custCols) == 0){
    $table .= "<tr><td style='text-align: center' colspan='" . (sizeof($headerArr) + 1) . "'>$i_no_record_exists_msg</td></tr>";
}
$table .= "</tbody>";

$table .= "</table>";

?>
<div class="content_top_tool">
		<?=$AddBtn?>
	<br style="clear:both" />
</div>
<?php echo $toolsBar?>
<form name="form1" id="form1" method="POST">
	<?php echo $table?>
</form>

<?php intranet_closedb();?>