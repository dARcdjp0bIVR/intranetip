<?php 
# using: Philips

################# Change Log [Start] ############
#
#   2019-04-24 [Philips]
#       - create file
#
################# Change Log [End] ##############
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");

if(!$plugin['AccountMgmt_StudentRegistry'] || (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"])) {
    include_once($PATH_WRT_ROOT."includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit;
}

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");

intranet_auth();
intranet_opendb();

global $i_no_record_exists_msg;

$linterface = new interface_html();
$CurrentPageArr['CustomColumn'] = 1;
$CurrentPage = "Settings_CustomCol";
$lsr = new libstudentregistry();

# Left menu
$TAGS_OBJ[] = array($Lang['StudentRegistry']['Settings']['CustomCol']);
$MODULE_OBJ = $lsr->GET_MODULE_OBJ_ARR();

# JS CSS
$jsCSS = $linterface->Include_JS_CSS();

# Start layout
$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$xmsg]);
?>

<?php echo $jsCSS?>
<div id="table_container">
</div>
<form name="form2" id="form2" method="POST">
</form>
<script type="text/javascript">
	$(document).ready(function(){
		reload_Table();
	});
	function reload_Table(){
		$.ajax({
			url: 'ajax_reload_table.php',
			method: 'GET',
			success: function(data){
				$('#table_container').empty().append(data);
				init_Drop_Table();
				$('#form2').empty();
				$('#form2').append($('<input type="hidden" name="action" value="order" />'));
			}
		});
	}
	function init_Drop_Table(){
		var target_tBodiesIndex = 0;
		var JQueryObj = $(".common_table_list_v30");
		JQueryObj.tableDnD({
			onDrop: function(table, DroppedRow) {
				// Get the order string
				let rows = $(table).find("tbody")[0].children;
				$.each(rows, function(i, v){
					$('#form2').append($('<input type="hidden" name="column['+(i+1)+']" value="'+$(v).attr('val_id')+'" />'));
				});
				//$('#form2').submit();
				$.post('update.php', $('#form2').serialize(), function(result) { if(result) reload_Table(); } );
			},
			onDragStart: function(table, DraggedRow) {
				//$('#debugArea').html("Started dragging row "+row.id);
			},
			dragHandle: "Dragable", 
			onDragClass: "move_selected"
		});
	}
	function checkEdit(obj,element,page){
        if(countChecked(obj,element)==1) {
	        	var id = document.getElementsByName(element);
	        	var len = id.length;
	        	var targetIndex;
	        	for( i=0 ; i<len ; i++) {
	                if (id[i].checked)
	                {
	                	targetIndex=i;
	                	break;
                	}
		        }
                obj.action=page;
                obj.submit();
        } else {
                alert(globalAlertMsg1);
        }
	}
</script>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
