<?php
// Modifying by:

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry.php");
include_once($PATH_WRT_ROOT."includes/libaccessright.php");

intranet_auth();
intranet_opendb();

$laccessright = new libaccessright();

if(!$plugin['AccountMgmt_StudentRegistry'] || (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] && !$laccessright->CHECK_ACCESS("StudentRegistry-SETTINGS-eNoticeTemplate-Access"))) {
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$lsr = new libstudentregistry();


$Title = htmlspecialchars(trim($Title));
$Content = htmlspecialchars(trim($Content));
$Subject = htmlspecialchars(trim($Subject));
$qStr = htmlspecialchars(trim($qStr));
$cStr = htmlspecialchars(trim($cStr));

$TempVar = $lsr->TemplateVariable($CategoryID);
$Content = str_replace("http://".$_SERVER['HTTP_HOST'],'',$Content);
$cStr = str_replace("http://".$_SERVER['HTTP_HOST'],'',$cStr);


$sql = "UPDATE INTRANET_NOTICE_MODULE_TEMPLATE 
		set
		CategoryID = '$CategoryID',
		Title = '$Title',
		Content = '$Content',
		Subject = '$Subject',
		ReplySlip = '$qStr',
		ReplySlipContent = '$cStr',
		RecordStatus = '$RecordStatus',
		DateModified = now()
		WHERE TemplateID = '$TemplateID'
		";

if($lsr->db_db_query($sql))
{
	$SysMsg = "update";
}
else
{
	$SysMsg = "update_failed";
}	
		
$ReturnPage = "index.php?xmsg=$SysMsg";


intranet_closedb();
header("Location: $ReturnPage");
?>
