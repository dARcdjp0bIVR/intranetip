<?php
// Modifying by:

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry.php");
include_once($PATH_WRT_ROOT."includes/libaccessright.php");

intranet_auth();
intranet_opendb();

$laccessright = new libaccessright();

if(!$plugin['AccountMgmt_StudentRegistry'] || (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] && !$laccessright->CHECK_ACCESS("StudentRegistry-SETTINGS-eNoticeTemplate-Access"))) {
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$lsr = new libstudentregistry();
$IDs = implode(",",$TemplateID);
$sql = "DELETE FROM INTRANET_NOTICE_MODULE_TEMPLATE WHERE TemplateID in ($IDs) ";

if($lsr->db_db_query($sql))
{
	$SysMsg = $Lang['StudentAttendance']['eNotice']['TemplateDeleteSuccess'];
}
else
{
	$SysMsg = $Lang['StudentAttendance']['eNotice']['TemplateDeleteFail'];
}	
		
$ReturnPage = "index.php?Msg=$SysMsg";


intranet_closedb();
header("Location: $ReturnPage");
?>
