<?php
// Modifying by:

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry.php");
include_once($PATH_WRT_ROOT."includes/libaccessright.php");

intranet_auth();
intranet_opendb();

$laccessright = new libaccessright();

if(!$plugin['AccountMgmt_StudentRegistry'] || (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] && !$laccessright->CHECK_ACCESS("StudentRegistry-SETTINGS-eNoticeTemplate-Access"))) {
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}
$lsr = new libstudentregistry();
# Check access right


$qStr = htmlspecialchars(trim($qStr));
$Title = htmlspecialchars(trim($Title));
$Content = htmlspecialchars(trim($Content));
$Subject = htmlspecialchars(trim($Subject));
$TempVar = $lsr->TemplateVariable($CategoryID);
$Content = str_replace("http://".$_SERVER['HTTP_HOST'],'',$Content);

$cStr = htmlspecialchars(trim($cStr));
$cStr = str_replace("http://".$_SERVER['HTTP_HOST'],'',$cStr);

$fields = "Module,CategoryID,Title,Content,Subject,RecordStatus,ReplySlip,DateInput,DateModified,ReplySlipContent";
$InsertValue = "'".$lsr->Module."', '$CategoryID','$Title','$Content','$Subject','$RecordStatus','$qStr',now(),now(), '$cStr'";
$sql = "INSERT INTO INTRANET_NOTICE_MODULE_TEMPLATE ($fields) VALUES ($InsertValue)";

if($lsr->db_db_query($sql))
{
	$SysMsg = "add";
}
else
{
	$SysMsg = "add_failed";
}	
		
$ReturnPage = "index.php?xmsg=$SysMsg";


intranet_closedb();
header("Location: $ReturnPage");
?>
