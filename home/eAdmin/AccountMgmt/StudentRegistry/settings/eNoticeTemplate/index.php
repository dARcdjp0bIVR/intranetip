<?php 
# using: 

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libaccessright.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry.php");

intranet_auth();
intranet_opendb();

$laccessright = new libaccessright();

if(!$plugin['AccountMgmt_StudentRegistry'] || (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] && !$laccessright->CHECK_ACCESS("StudentRegistry-SETTINGS-eNoticeTemplate-Access"))) {
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

# preserve table view
if ($ck_right_page_number!=$pageNo && $pageNo!="")
{
	setcookie("ck_right_page_number", $pageNo, 0, "", "", 0);
	$ck_right_page_number = $pageNo;
} else if (!isset($pageNo) && $ck_right_page_number!="")
{
	$pageNo = $ck_right_page_number;
}

if ($ck_right_page_order!=$order && $order!="")
{
	setcookie("ck_right_page_order", $order, 0, "", "", 0);
	$ck_right_page_order = $order;
} else if (!isset($order) && $ck_right_page_order!="")
{
	$order = $ck_right_page_order;
}

if ($ck_right_page_field!=$field && $field!="")
{
	setcookie("ck_right_page_field", $field, 0, "", "", 0);
	$ck_right_page_field = $field;
} else if (!isset($field) && $ck_right_page_field!="")
{
	$field = $ck_right_page_field;
}

$linterface = new interface_html();
$lsr = new libstudentregistry();

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$order = ($order == 1) ? 1 : 0;
$field = ($field == "") ? 0 : $field;

$conds = "";
if(isset($CategoryID) && $CategoryID!="")
	$conds .= " AND CategoryID='$CategoryID'";
	
if(isset($RecordStatus) && $RecordStatus!="")
	$conds .= " AND RecordStatus='$RecordStatus'";	

$sql  = "SELECT
               CONCAT('<a class=\'tablelink\' href=\'edit.php?TemplateID=', TemplateID, '\'>', Title, '</a>') as Title,
               IF(CategoryID='0',CONCAT('--'),
               CASE (CategoryID)
				WHEN '1' THEN '".$Lang['StudentRegistry']['RegistryStatus_Left']."'
				WHEN '2' THEN '".$Lang['StudentRegistry']['RegistryStatus_Suspend']."'
				WHEN '3' THEN '".$Lang['StudentRegistry']['RegistryStatus_Resume']."'
				ELSE '---' END) as CategoryID,
               IF(RecordStatus=1,CONCAT('$i_Discipline_System_Discipline_Template_Published'),CONCAT('$i_Discipline_System_Discipline_Template_Draft')) as Status,
               CONCAT('<input type=\'checkbox\' name=\'TemplateID[]\' value=\'', TemplateID,'\'>')
                FROM INTRANET_NOTICE_MODULE_TEMPLATE
                WHERE Module='".$lsr->Module."' $conds ";

$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("Title", "CategoryID", "Status");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;
$li->column_array = array(0,0,0);
$li->wrap_array = array(0,0,0);
$li->IsColOff = "IP25_table";

$pos = 0;
$li->column_list .= "<th width='1' class='tabletoplink'>#</th>\n";
$li->column_list .= "<th width='40%' >".$li->column($pos++, $i_Discipline_System_Discipline_Template_Title)."</th>\n";
$li->column_list .= "<th width='30%'>".$li->column($pos++, $i_Discipline_System_Discipline_Situation)."</th>\n";
$li->column_list .= "<th width='30%'>".$li->column($pos++, $i_Discipline_System_Discipline_Status)."</th>\n";
$li->column_list .= "<th width='1'>".$li->check("TemplateID[]")."</th>\n";


$toolbar = $linterface->GET_LNK_NEW("new.php",$button_new,"","","",0);

$catTmpArr = $lsr->TemplateCategory();
if($CategoryID=='')
{
	//$CategoryID	= $catTmpArr[0][0];
}

if(is_array($catTmpArr))
{
	foreach($catTmpArr as $Key=>$Value)
	{
		$catArr[] = array($Key,$Value);
	}
}
$situationSelection = getSelectByArray($catTmpArr, ' name="CategoryID" id="CategoryID" onChange="document.form1.submit()"', $CategoryID, "", 0, $Lang['StudentRegistry']['AllRegistryStatus']);

$statusArr[0] = array("",$i_Discipline_System_Discipline_Template_All_Status);
$statusArr[1] = array("0",$i_Discipline_System_Discipline_Template_Draft);
$statusArr[2] = array("1",$i_Discipline_System_Discipline_Template_Published);

$statusSelection = $linterface->GET_SELECTION_BOX($statusArr, 'id="RecordStatus", name="RecordStatus" onChange="document.form1.submit()"', "", $RecordStatus);


# Top menu highlight setting
$CurrentPage = "Setting_RegistryNoticeTemplate";

$TAGS_OBJ[] = array($Lang['StudentRegistry']['eNoticeTemplate']);

$CurrentPageArr['StudentRegistry'] = 1;

# Left menu 
$MODULE_OBJ = $lsr->GET_MODULE_OBJ_ARR();

# Start layout
$linterface->LAYOUT_START();

?>
<script language="javascript">
function removeCat(obj,element,page){
		var alertConfirmRemove = "<?=$i_Usage_RemoveConfirm?>";
        if(countChecked(obj,element)==0)
            alert(globalAlertMsg2);
        else{
            if(confirm(alertConfirmRemove)){
            	obj.action=page;
            	obj.method="post";
            	obj.submit();
            }
        }
}
</script>
<form name="form1" method="post" action="">
<table width="98%" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td class="main_content">
			<div class="content_top_tool">
				<div class="Conntent_tool">
					<a href="new.php" class="new"> <?=$button_new?></a>
				</div>                    

				<div class="table_board">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr class="table-action-bar">
							<td>				
								<div class="table_filter">
								<?=$situationSelection?> <?=$statusSelection?>
								</div>
							</td>
							<td valign="bottom">
								<div class="common_table_tool">
									<a href="javascript:checkEdit(document.form1,'TemplateID[]','edit.php')" class="edit"><?=$button_edit?></a>
									<a href="javascript:removeCat(document.form1,'TemplateID[]','remove_update.php')" class="tool_delete"><?=$button_remove?></a>
								</div>
							</td>
						</tr>
					</table>
					<?=$li->display();?>
				
				</div>
			</div>
			<br />
			<p>&nbsp;</p>
		</td>
	</tr>
</table>
<br>
<input type="hidden" name="pageNo" id="pageNo" value="<?= $li->pageNo; ?>" />
<input type="hidden" name="order" id="order" value="<?= $li->order; ?>" />
<input type="hidden" name="field" id="field" value="<?= $li->field; ?>" />
<input type="hidden" name="page_size_change" id="page_size_change" value="" />
<input type="hidden" name="numPerPage" id="numPerPage" value="<?=$li->page_size?>" />
</form>
<?php

$linterface->LAYOUT_STOP();
intranet_closedb();

?>