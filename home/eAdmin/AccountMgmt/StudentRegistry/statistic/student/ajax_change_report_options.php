<?php
#using: Thomas

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccessright.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");

intranet_auth();
intranet_opendb();

$laccessright = new libaccessright();

if(!$plugin['AccountMgmt_StudentRegistry'] || (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] && !$laccessright->CHECK_ACCESS("StudentRegistry-STATS-Student-View"))) {
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$lsr = new libstudentregistry();
$lclass = new libclass();

/*
# Get Class List
$SelectedClassList = explode(",", $ClassList);
$ClassListArr = $lsr->displayClassInfoInStudentRegistry($AcademicYearID);

if($submit_flag != 1)
	$SelectedClassList[0] = $ClassListArr[0][0];

$ClassOptionHTML = "";
for($i=0;$i<sizeof($ClassListArr);$i++)
{
	$ClassOptionHTML .= "<option value=\"".$ClassListArr[$i][0]."\"";
	if (in_array($ClassListArr[$i][0], $SelectedClassList))
		$ClassOptionHTML .= " selected";
	$ClassOptionHTML .= ">".$ClassListArr[$i][1]."</option>\n";
}
*/

$SelectedClassList 	 = array();
$SelectedStudentList = array();

# Get Class List
if($mode != 4)
{
	$ClassListArr = $lsr->displayClassInfoInStudentRegistry($AcademicYearID);

	if($mode!=2)
		$SelectedClassList[0] = $ClassListArr[0][0];
	else
		$SelectedClassList = explode(",", $ClassListStr);
	
	$ClassOptionHTML = "";
	for($i=0;$i<sizeof($ClassListArr);$i++)
	{
		$ClassOptionHTML .= "<option value=\"".$ClassListArr[$i][0]."\"";
		if (in_array($ClassListArr[$i][0], $SelectedClassList))
			$ClassOptionHTML .= " selected";
		$ClassOptionHTML .= ">".$ClassListArr[$i][1]."</option>\n";
	}
}

# Get Student List
if($mode==2 || $mode==4)
	$SelectedStudentList = explode(",", $StudentListStr);
if($mode==4)
	$SelectedClassList = explode(",", $ClassListStr);
	
$StudentOptionHTML = "";
for($i=0;$i<sizeof($SelectedClassList);$i++)
{
	$StudentListArr = $lsr->displayStudentInfoInStudentRegistry($SelectedClassList[$i]);
	for($j=0;$j<sizeof($StudentListArr);$j++)
	{
		$StudentOptionHTML .= "<option value=\"".$StudentListArr[$j][0]."\"";
		if(in_array($StudentListArr[$j][0], $SelectedStudentList))
			$StudentOptionHTML .= "selected";
		$StudentOptionHTML .= ">".$StudentListArr[$j][1]."-".$StudentListArr[$j][2]." ".$StudentListArr[$j][3]."</option>\n";
	}

}

//debug_pr($SelectedClassList);
echo $ClassOptionHTML."|=|".$StudentOptionHTML;

intranet_closedb();
?>