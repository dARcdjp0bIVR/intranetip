<?php
#using: 

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccessright.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_header.php");

intranet_auth();
intranet_opendb();

$laccessright = new libaccessright();
$linterface = new interface_html();

if(!$plugin['AccountMgmt_StudentRegistry'] || (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] && !$laccessright->CHECK_ACCESS("StudentRegistry-STATS-Student-View"))) {
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$lsr = new libstudentregistry();

$print = 1;

$tableContent = $lsr->getStudentRegistryStatistics($type, $AcademicYearID, $StudentList, $group1, $group2, $subtotal, $total, $print);

?>

<table width="98%" border="0" cellspacing="0" cellpadding="5" valign="top">
	<tr>
		<td align="right" class='print_hide'>
			<?=$linterface->GET_BTN($button_print, "button","javascript:window.print()")?>
		</td>
	</tr>	
	<tr>
		<td align="center">
			<h4><?=GET_SCHOOL_NAME()?></h4>
		</td>
	</tr>	
	<tr>
		<td align="center">
			<?=$tableContent?>
		</td>
	</tr>
</table>

<p>&nbsp;</p>
<?

intranet_closedb();

include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_footer.php");

?>