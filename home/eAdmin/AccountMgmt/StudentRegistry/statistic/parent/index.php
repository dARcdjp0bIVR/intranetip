<?php
#using:

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccessright.php");
include_once($PATH_WRT_ROOT."includes/libstudentregistry.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$laccessright = new libaccessright();

if(!$plugin['AccountMgmt_StudentRegistry'] || (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] && !$laccessright->CHECK_ACCESS("StudentRegistry-STATS-Parent-View"))) {
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$lsr = new libstudentregistry();
$linterface = new interface_html();

# Get Academic Year Drop Down List
$AcademicYearID = isset($AcademicYearID)? $AcademicYearID : Get_Current_Academic_Year_ID();
$years = GetAllAcademicYearInfo();
for($i=0; $i<sizeof($years); $i++) {
	list($yearid, $yearNameEN, $yearNameB5) = $years[$i];
	$yearAry[$i][] = $yearid;
	$yearAry[$i][] = Get_Lang_Selection($yearNameB5, $yearNameEN);	
}
//$yearSelectionMenu = getSelectByArray($yearAry, 'name="AcademicYearID" id="AcademicYearID"', $AcademicYearID, 0, 0, $Lang['SysMgr']['AcademicYear']['FieldTitle']['SelectAllAcademicYear']);
$yearSelectionMenu = getSelectByArray($yearAry, 'name="AcademicYearID" id="AcademicYearID"', $AcademicYearID, 0, 1);

if($_POST["submit_flag"] == "YES")
{
	$profession = isset($profession)? $profession : 0;
	$title = isset($title)? $title : 0;
	$father = isset($father)? $father : 0;
	$mother = isset($mother)? $mother : 0;
	$guardian = isset($guardian)? $guardian : 0;
	$year_cond  = $AcademicYearID == ""? "WHERE yc.AcademicYearID IS NOT NULL" : "WHERE yc.AcademicYearID = $AcademicYearID";
	$parent_cond .= $father? ($parent_cond? " OR srpg.PG_TYPE = 'F'" : "srpg.PG_TYPE = 'F'") : "";
	$parent_cond .= $mother? ($parent_cond? " OR srpg.PG_TYPE = 'M'" : "srpg.PG_TYPE = 'M'") : "";
	$parent_cond .= $guardian? ($parent_cond? " OR (srpg.PG_TYPE = 'G' AND srpg.GUARD = 'O')" : "srpg.PG_TYPE = 'G'") : "";
	
	if($profession)
	{
		$sql = "SELECT
					IF(a.JOB_NATURE = '', '--', a.JOB_NATURE) AS profession,
					COUNT(*) AS total,
					ROUND(100*(COUNT(*)/b.total), 2) AS percent
				FROM
					(SELECT
						srpg.StudentID,
						IF(srpg.JOB_NATURE IS NULL, '', srpg.JOB_NATURE) AS JOB_NATURE
					FROM
						STUDENT_REGISTRY_PG AS srpg".
					($parent_cond? " WHERE $parent_cond" : "").") AS a LEFT JOIN
					YEAR_CLASS_USER AS ycu ON a.StudentID = ycu.UserID LEFT JOIN
					YEAR_CLASS AS yc ON ycu.YearClassID = yc.YearClassID, 
					(SELECT
						COUNT(*) AS total
					 FROM
						STUDENT_REGISTRY_PG AS srpg LEFT JOIN
						YEAR_CLASS_USER AS ycu ON srpg.StudentID = ycu.UserID LEFT JOIN
						YEAR_CLASS AS yc ON ycu.YearClassID = yc.YearClassID
					 $year_cond".($parent_cond? " AND ($parent_cond)" : "").") AS b
				$year_cond
				GROUP BY
					a.JOB_NATURE
				";
		$result = $lsr->returnArray($sql);
		//debug_pr($sql);
		
		$PROF_report = "<div ".($profession&&$title? "class=\"report_half_v30\"":"").">
							<div class=\"table_board\">"
								.$linterface->GET_NAVIGATION2($Lang['StudentRegistry']['ParentJobNatureStat'])."
								<br>
								<table class=\"common_table_list\">
									<thead>
										<tr>
											<th>".$Lang['StudentRegistry']['JobNatureGroup']."</th>
											<th>".$Lang['StudentRegistry']['NoOfParent']."</th>
											<th>%</th>
										</tr>
									</thead>
									<tbody>";
		$no_of_parent = 0;
		$JOB_NATURE_ary = $lsr -> MAL_JOB_NATURE_DATA_ARY();
		for($i=0;$i<sizeof($result);$i++)
		{
			$JOB_NATURE_val = $lsr -> RETRIEVE_DATA_ARY($JOB_NATURE_ary, $result[$i]['profession']);
			$JOB_NATURE_val	= $JOB_NATURE_val == ''? '--' : $JOB_NATURE_val;
			$PROF_report .= "<tr>
								<td>".$JOB_NATURE_val."</td>
								<td>".$result[$i]['total']."</td>
								<td>".$result[$i]['percent']."</td>
							 <tr>";
			$no_of_parent += $result[$i]['total'];
		}
		$PROF_report .= "<tr class=\"total_row\">
							<td>".$Lang['StudentRegistry']['Total']."</td>
							<td>$no_of_parent</td>
							<td>&nbsp;</td>
						 </tr>
						 </tbody></table></div></div>";
	}
	
	if($title)
	{
		$sql = "SELECT
					IF(a.JOB_TITLE = '', '--', a.JOB_TITLE) AS title,
					COUNT(*) AS total,
					ROUND(100*(COUNT(*)/b.total), 2) AS percent
				FROM
					(SELECT
						srpg.StudentID,
						IF(srpg.JOB_TITLE IS NULL, '', srpg.JOB_TITLE) AS JOB_TITLE
					FROM
						STUDENT_REGISTRY_PG AS srpg".
					($parent_cond? " WHERE $parent_cond" : "").") AS a LEFT JOIN
					YEAR_CLASS_USER AS ycu ON a.StudentID = ycu.UserID LEFT JOIN
					YEAR_CLASS AS yc ON ycu.YearClassID = yc.YearClassID, 
					(SELECT
						COUNT(*) AS total
					 FROM
						STUDENT_REGISTRY_PG AS srpg LEFT JOIN
						YEAR_CLASS_USER AS ycu ON srpg.StudentID = ycu.UserID LEFT JOIN
						YEAR_CLASS AS yc ON ycu.YearClassID = yc.YearClassID
					 $year_cond".($parent_cond? " AND ($parent_cond)" : "").") AS b
				$year_cond
				GROUP BY
					a.JOB_TITLE
				";
		$result = $lsr->returnArray($sql);
		//debug_pr($sql);
		
		$JOB_TITLE_report = "<div ".($profession&&$title? "class=\"report_half_v30\"":"").">
							<div class=\"table_board\">"
								.$linterface->GET_NAVIGATION2($Lang['StudentRegistry']['ParentJobTitleStat'])."
								<br>
								<table class=\"common_table_list\">
									<thead>
										<tr>
											<th>".$Lang['StudentRegistry']['JobTitleGroup']."</th>
											<th>".$Lang['StudentRegistry']['NoOfParent']."</th>
											<th>%</th>
										</tr>
									</thead>
									<tbody>";
		$no_of_parent = 0;
		$JOB_TITLE_ary = $lsr -> MAL_JOB_TITLE_DATA_ARY();
		for($i=0;$i<sizeof($result);$i++)
		{
			$JOB_TITLE_val 	   = $lsr -> RETRIEVE_DATA_ARY($JOB_TITLE_ary, $result[$i]['title']);
			$JOB_TITLE_val	   = $JOB_TITLE_val == ''? '--' : $JOB_TITLE_val;
			$JOB_TITLE_report .= "<tr>
								<td>".$JOB_TITLE_val."</td>
								<td>".$result[$i]['total']."</td>
								<td>".$result[$i]['percent']."</td>
							 <tr>";
			$no_of_parent += $result[$i]['total'];
		}
		$JOB_TITLE_report .= "<tr class=\"total_row\">
							<td>".$Lang['StudentRegistry']['Total']."</td>
							<td>$no_of_parent</td>
							<td>&nbsp;</td>
						 </tr>
						 </tbody></table></div></div>";
	}
}

$CurrentPageArr['StudentRegistry'] = 1;
$CurrentPage = "Stat_Parent";

$TAGS_OBJ[] = array($Lang['Menu']['AccountMgmt']['Parent'], "index.php");

$MODULE_OBJ['title'] = $Lang['Header']['Menu']['StudentRegistry'];

$MODULE_OBJ = $lsr->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();
?>

<script language="javascript">
function hideOption()
{
	$('#spanHideOption').attr('style', 'display:none');
	$('#spanShowOption').attr('style', '');
	$('#report_form').attr('style', 'display:none');
	$('#td_option').addClass('report_show_option');
}

function showOption()
{
	$('#spanHideOption').attr('style', '');
	$('#spanShowOption').attr('style', 'display:none');
	$('#report_form').attr('style', '');
	$('#td_option').removeClass();
}

function checksubmit(obj, print)
{
	if(!obj.profession.checked && !obj.title.checked)
	{
		alert('<?=$i_alert_pleaseselect.$Lang['StudentRegistry']['ReportType']?>');
		return false;
	}
	
	if(obj.AcademicYearID.value == '')
	{
		alert('<?=$i_alert_pleaseselect.$Lang['StudentRegistry']['AcademicYear']?>');
		return false;
	}
	
	if(!obj.father.checked && !obj.mother.checked && !obj.guardian.checked)
	{
		alert('<?=$i_alert_pleaseselect.$Lang['StudentRegistry']['ParentType']?>');
		return false;
	}	
	
	obj.submit_flag.value = "YES";
	
	if(print)
	{
		obj.target = "_blank";
		obj.action = "index_print.php"
	}
	else
	{
		obj.target = "";
		obj.action = "index.php";
	}
	obj.submit();
}
</script>
<form name="form1" method="POST" action="">
	<table cellspacing="0" cellpadding="3" border="0" align="center" width="99%">
		<tr class="report_show_option" <?=$_POST["submit_flag"] == "YES"? "" : "style=\"display:none\""?>>
			<td id="td_option" class="report_show_option">
				<span id="spanHideOption" <?=$_POST["submit_flag"] == "YES"? "style=\"display:none\"" : ""?>>
					<a href="javascript:hideOption();" class="contenttool">
						<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_hide_option.gif" width="20" height="20" border="0" align="absmiddle"><?=$Lang['StudentRegistry']['HideStatOption']?>
					</a>
				</span>
				<span id="spanShowOption">
					<a href="javascript:showOption();" class="contenttool">
						<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_show_option.gif" width="20" height="20" border="0" align="absmiddle"><?=$Lang['StudentRegistry']['ShowStatOption']?>
					</a>
				</span>
			</td>
		</tr>
	</table>
	<table cellspacing="0" cellpadding="0" border="0" width="99%">
		<tr>
			<td class="main_content">
				<span id="report_form" <?=$_POST["submit_flag"] == "YES"? "style=\"display:none\"" : ""?>>
					<div class="report_option report_option_title" <?=$_POST["submit_flag"] == "YES"? "style=\"display:none\"" : ""?>>
						- <?= $Lang['StudentRegistry']['Options'] ?> -
					</div>
					<div class="table_board">
						<table class="form_table_v30">
							<tbody>
								<tr>
									<td class="field_title"><?= $Lang['StudentRegistry']['ReportType'] ?></td>
									<td>
										<input type="checkbox" id="profession" name="profession" value="1" <?= $profession? "checked=\"checked\"" : ""?>><label for="profession"><?=$Lang['StudentRegistry']['ProfessionStat']?></label>
										<br>
										<input type="checkbox" id="title" name="title" value="1" <?= $title? "checked=\"checked\"" : ""?>><label for="title"><?=$Lang['StudentRegistry']['TitleStat']?></label>
									</td>
								</tr>
							</tbody>
							<col class="field_title">
        					<col class="field_c">
							<tbody>
								<tr>
									<td class="field_title"><?=$Lang['StudentRegistry']['AcademicYear']?></td>
									<td><?=$yearSelectionMenu?></td>
								</tr>
							</tbody>
							<tbody>
								<tr>
									<td class="field_title"><?=$Lang['StudentRegistry']['ParentType']?></td>
									<td>
										<input type="checkbox" id="father" name="father" value="1" <?= $father? "checked=\"checked\"" : ""?>><label for="father"><?=$Lang['StudentRegistry']['Father']?></label>
										<input type="checkbox" id="mother" name="mother" value="1" <?= $mother? "checked=\"checked\"" : ""?>><label for="mother"><?=$Lang['StudentRegistry']['Mother']?></label>
										<input type="checkbox" id="guardian" name="guardian" value="1" <?= $guardian? "checked=\"checked\"" : ""?>><label for="guardian"><?=$Lang['StudentRegistry']['Guardian']?></label>
									</td>
								</tr>
							</tbody>
						</table>
						<p class="spacer"></p>
					</div>
					<div class="edit_bottom">
						<p class="spacer"></p>
						<?=$linterface->GET_ACTION_BTN($Lang['StudentRegistry']['GenerateReport'], "button", "checksubmit(this.form, false)", "GenerateBtn")?>
						<p class="spacer"></p>
					</div>
				</span>
				<div class="table_board">
					<table cellspacing="0" cellpadding="3" border="0" align="center" width="100%">
						<tr>
							<td>
								<div class="table_filter" <?=$_POST["submit_flag"] == "YES"? "" : "style=\"display:none\""?>>
									<?=$Lang['StudentRegistry']['AcademicYear']." : "?><?=$AcademicYearID == ""? $Lang['SysMgr']['AcademicYear']['FieldTitle']['SelectAllAcademicYear'] : $lsr->getAcademicYearNameByYearID($AcademicYearID)?> 
								</div>
								<br style="clear:both">
							</td>
						</tr>
						<tr>
							<td>
								<?=$PROF_report?>
								<?=$JOB_TITLE_report?>
							</td>
						</tr>
						<tr>
							<td>
								<p class="spacer"></p>
								<div class="edit_bottom" <?=$_POST["submit_flag"] == "YES"? "" : "style=\"display:none\""?>>
									<p class="spacer"></p>
									<?=$linterface->GET_ACTION_BTN($button_print, "button", "checksubmit(this.form, true)", "PrintBtn")?>
									<p class="spacer"></p>
								</div>
							</td>
						</tr>
					</table>
				</div>
			</td>
		</tr>
	</table>
	<input type="hidden" id="submit_flag" name="submit_flag" value="">
</form>

<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>