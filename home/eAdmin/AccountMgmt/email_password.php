<?php
# using: 

########## Change Log [Start] ############
#	
#	Date:	2013-09-09	Ivan [2013-0906-1154-01177]
#			updated the password field to reset password path
#
#	Date:	2013-02-08	YatWoon
#			use "user_id[]" instead of userID[]
#
########## Change Log [End] ############


@SET_TIME_LIMIT(600);
$PATH_WRT_ROOT = "../../../";
include($PATH_WRT_ROOT."includes/global.php");
include($PATH_WRT_ROOT."includes/libdb.php");
include($PATH_WRT_ROOT."includes/libuser.php");
include($PATH_WRT_ROOT."includes/libauth.php");
include($PATH_WRT_ROOT."includes/libemail.php");
include($PATH_WRT_ROOT."includes/libaccountmgmt.php");
include($PATH_WRT_ROOT."includes/libsendmail.php");
// include($PATH_WRT_ROOT."lang/email.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");
intranet_opendb();

if(!($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"])) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$li = new libdb();
$lauth = new libauth();
$laccount = new libaccountmgmt();

if(sizeof($_POST))
	$userList = implode(',', $user_id);

$sql = "SELECT 
			UserID, UserLogin, UserPassword, UserEmail, EnglishName, Title 
		FROM 
			INTRANET_USER
		WHERE
			UserID IN ($userList)
		";
$row = $li->returnArray($sql, 6);

for($i=0; $i<sizeof($row); $i++){
     $uid = $row[$i][0];
     $UserLogin = $row[$i][1];
     $UserPassword = $row[$i][2];
     $UserEmail = $row[$i][3];
     $EnglishName = $row[$i][4];
     
     # Call sendmail function
	 list($welcomeMailSubject,$mailBody) = $laccount->returnEmailNotificationData_SendPassword($UserEmail, $UserLogin, $UserPassword, "", $EnglishName);
	 
	
	 // 2013-0906-1154-01177	
//     $mailTo = $UserEmail;
//     $lu = new libsendmail();
//     $webmaster = get_webmaster();
//     $headers = "From: $webmaster\r\n";
//     $lu->SendMail( $mailTo, $mailSubject, $mailBody,"$headers");

	# Hash function
    if ($intranet_authentication_method == "HASH" )
    {
		# Call sendmail function
		$luser = new libuser();
		$Key = $lauth->CreateResetPasswordKey($UserLogin);
		list($mailSubject,$mailBody) = $luser->returnEmailNotificationData_HashedPw($UserEmail, $UserLogin, $EnglishName, $Key);
	    $mailTo = $UserEmail;
	    $webmaster = get_webmaster();
	    $lu = new libsendmail();
	
	    $result = $lu->send_email($mailTo, $welcomeMailSubject, $mailBody,"");
    }
	else
	{
	    # Call sendmail function
		$luser = new libuser();
		list($mailSubject,$mailBody) = $luser->returnEmailNotificationData($UserEmail, $UserLogin, $UserPassword, $EnglishName);
	    $mailTo = $UserEmail;
	    $webmaster = get_webmaster();
	    $lu = new libsendmail();
	
	    $result = $lu->send_email($mailTo, $welcomeMailSubject, $mailBody,"");
	     
	    # Send ACK Email
		list($ackMailSubject,$ackMailBody) = $luser->returnEmailNotificationData_ACK($UserEmail, $UserLogin, $UserPassword, $EnglishName);
		
		$result2 = $lu->send_email($webmaster, $ackMailSubject,$ackMailBody,"");
	}
}


intranet_closedb();
$comeFrom = $comeFrom ? $comeFrom : "index.php";
header("Location: $comeFrom?xmsg=EmailSent");
?>