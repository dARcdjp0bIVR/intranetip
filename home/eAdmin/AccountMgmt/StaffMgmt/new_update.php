<?php
# using: Henry

############# Change Log
#	Date:	2020-07-14 Philips
#			Added position, contractType, contractStartDate, contractEndDate for KIS CEES
#
#	Date:	2019-06-24 Carlos
#			Empty UserPassword field.
#
#   Date:   2019-04-30 Cameron
#           fix cross site scripting by applying cleanHtmlJavascript() to hidden variables
#
#	Date:	2019-01-21 (Carlos) [ip.2.5.10.2.1]: $sys_custom['SupplementarySmartCard'] checked duplication of CardID2, CardID3 and CardID4.
#
#   Date:   2018-08-30 (Pun) [ip.2.5.9.10.1]
#           Modified syncUserFromEClassToGoogle(), added $chiname
#
#
#   Date:   2018-08-14 (Cameron) [ip.2.5.9.10.1]: assign random password for HKPF if it's empty
#
#   Date:   2018-08-07 (Cameron) [ip.2.5.9.10.1]: add Grade and Duty for HKPF
#
#	Date:	2018-01-03 (Carlos) [ip.2.5.9.1.1]: Improved password checking.
#
#	Date:	2017-06-12 Pun [ip.2.5.8.4.1]
#           Added $sys_custom['iPortfolio_auto_add_teacher_to_group'] for auto add teacher to iPortfolio teacher group
#
#   Date:   2016-11-29 (HenryHM) [ip.2.5.8.1.1]
#           $ssoservice["Google"]["Valid"] - add GSuite Logic
#
#	Date:	2016-10-04 (Carlos) [ip.2.5.8.1.1] 
#			$sys_custom['iMail']['UserDisplayOrder'] - added input field DisplayOrder
#
#	Date:	2016-09-28 (Ivan) [ip.2.5.7.10.1]
#			Added teaching = 'S' supply teacher logic
#
#	Date:	2016-05-18 (Carlos)
#			$sys_custom['DisableEmailAfterCreatedNewAccount'] - disable sending notification email after created new user account. 
#
#	Date:	2015-12-30 (Carlos)
#			$plugin['radius_server'] - added Wi-Fi access option.
#
#	Date:	2015-05-26 (Carlos)
#			added libaccountmgmt.php suspendEmailFunctions($targetUserId, $resume=false) to resume or suspend mail server auto forward and auto reply functions. 
#
#	Date:	2015-05-05 (Omas)
# 			improved - insert MyCalendar
# 
#	Date:	2015-03-23 (Bill)
#			fixed - cannot add staff to usergroup if usergroup without default group role
#
#	Date:	2014-12-02 (Bill)
#			add email, smart card id and staff code to $error_msg if duplicate
#
#	Date:	2014-10-16 (Ryan) - Create account into PMS for SIS Flag : $sys_custom['SISPMSRegistry']
#
#	Date:	2014-09-19 (Bill)	
#			Add RoleID to INTRANET_USERGROUP to display default role in Group
# 
#	Date:	2014-09-03 (Carlos) - iMail plus: Block receive/send internet mails when [iMail Settings > Default Usage Rights and Quota > Internal mail only] is on
#
#	Date:	2014-05-29 (Carlos) - only send notification email if recipient email is valid
#
#	Date:	2013-12-12 (Carlos)
#			$sys_custom['iMailPlus']['EmailAliasName'] - user can input its own iMail plus user name
#
#	Date:	2013-03-14	Carlos
#			add shared group calendars to user
#
#	Date:	2012-08-30	YatWoon
#			add barcode
#
#	Date:	2012-08-03	YatWoon
#			remove the flag checking for websams staff code
#
#	Date:	2012-08-01	Ivan
#			added logic to syn user info to library system 
#
#	Date:	2011-09-30  Carlos
#			store two-hashed/encrypted password
#
#	Date:	2011-03-28	YatWoon
#			change email notification subject & content data 
#
#	Date:	2010-08-18	YatWoon
#			Add "Remark" field
#
#################
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libauth.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libregistry.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."includes/libemail.php");
include_once($PATH_WRT_ROOT."lang/email.php");
include_once($PATH_WRT_ROOT."includes/libftp.php");
include_once($PATH_WRT_ROOT."includes/libldap.php");
include_once($PATH_WRT_ROOT."includes/imap_gamma.php");
include_once($PATH_WRT_ROOT."includes/libeclass40.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/icalendar.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");

intranet_auth();
intranet_opendb();


if(!($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Staff"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"])) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

if(sizeof($_POST)==0) {
	header("Location: new.php");	
	exit;
}

### start handle sql injection and cross site scripting
$TeachingType = cleanHtmlJavascript($TeachingType);
$recordstatus = cleanHtmlJavascript($recordstatus);
$userlogin = cleanHtmlJavascript($userlogin);
$pwd = cleanHtmlJavascript($pwd);
$teaching = cleanHtmlJavascript($teaching);
$email = cleanHtmlJavascript($email);
$status = IntegerSafe($status);
$smartcardid = cleanHtmlJavascript($smartcardid);
$staffCode = cleanHtmlJavascript($staffCode);
$barcode = cleanHtmlJavascript($barcode);
$grade = cleanHtmlJavascript($grade);
$duty = cleanHtmlJavascript($duty);
$engname = cleanHtmlJavascript($engname);
$chiname = cleanHtmlJavascript($chiname);
$nickname = cleanHtmlJavascript($nickname);
$gender = cleanHtmlJavascript($gender);
$engTitle = cleanHtmlJavascript($engTitle);
$chiTitle = cleanHtmlJavascript($chiTitle);
$address = cleanHtmlJavascript($address);
$country = cleanHtmlJavascript($country);
$homePhone = cleanHtmlJavascript($homePhone);
$officePhone = cleanHtmlJavascript($officePhone);
$mobilePhone = cleanHtmlJavascript($mobilePhone);
$faxPhone = cleanHtmlJavascript($faxPhone);
$open_webmail = cleanHtmlJavascript($open_webmail);
$open_file = cleanHtmlJavascript($open_file);
$errorList = cleanHtmlJavascript($errorList);
$remark = cleanHtmlJavascript($remark);
$position = cleanHtmlJavascript($position);
$contractType = cleanHtmlJavascript($contractType);
$contractStartDate = cleanHtmlJavascript($contractStartDate);
$contractEndDate = cleanHtmlJavascript($contractEndDate);
if($sys_custom['iMailPlus']['EmailAliasName']){
    $ImapUserLogin = cleanHtmlJavascript($ImapUserLogin);
    $UseImapUserLogin = cleanHtmlJavascript($UseImapUserLogin);
}
if($sys_custom['iMail']['UserDisplayOrder']){
    $DisplayOrder = cleanHtmlJavascript($DisplayOrder);
}
if($ssoservice["Google"]["Valid"]){
	$googleUserLogin = cleanHtmlJavascript($googleUserLogin);
}
### end handle sql injection and cross site scripting

$engname = intranet_htmlspecialchars(stripslashes($engname));
$chiname = intranet_htmlspecialchars(stripslashes($chiname));

$lauth = new libauth();
$laccount = new libaccountmgmt();
$li = new libregistry();
$lwebmail = new libwebmail();
$le = new libeclass();
$lu = new libuser();
$lcalendar = new icalendar();

$thisUserType = USERTYPE_STAFF;
$SettingArr = $lu->RetrieveUserInfoSettingInArrary(array('EnablePasswordPolicy_'.$thisUserType));
$PasswordLength = $SettingArr['EnablePasswordPolicy_'.$thisUserType];
if ($PasswordLength<6) $PasswordLength = 6;

$CurrentPageArr['StaffMgmt'] = 1;
//$CurrentPage = "User List";

$TAGS_OBJ[] = array($Lang['Group']['UserList']);
$MODULE_OBJ['title'] = $Lang['Header']['Menu']['StaffAccount'];

# navigation bar
$PAGE_NAVIGATION[] = array($Lang['AccountMgmt']['UserList'], "javascript:goURL('index.php')");
$PAGE_NAVIGATION[] = array($Lang['AccountMgmt']['NewUser'], "");

# step information
$STEPS_OBJ[] = array("Input Staff Details", 0);
$STEPS_OBJ[] = array("Select Related Group and role ", 1);

## check User Login duplication ###
if($userlogin != ""){
	$sql = "SELECT UserID FROM INTRANET_USER WHERE UserLogin='$userlogin'";
	$temp = $laccount->returnVector($sql);
	if(sizeof($temp)!=0){
		$error_msg[] = $userlogin;
	}
}

if ($sys_custom['project']['HKPF'] && trim($pwd) == '') {
    $pwd = intranet_random_passwd($length=16, $enableUpperCase=true);   // assign random password
}

if($sys_custom['UseStrongPassword']){
	$check_password_result = $lauth->CheckPasswordCriteria($pwd,$userlogin,$PasswordLength);
	if(!in_array(1,$check_password_result)){
		$error_msg[] = $pwd;
	}
}

## check Email duplication ###
if($email != ""){
	$sql = "SELECT UserID FROM INTRANET_USER WHERE UserEmail='$email'";
	$temp = $laccount->returnVector($sql);
	if(sizeof($temp)!=0){
		$error_msg[] = $email;
	}
}

## check cardid duplication ###
if($smartcardid!=""){
	$sql="SELECT UserID FROM INTRANET_USER WHERE CardID='$smartcardid' ";
	if($sys_custom['SupplementarySmartCard']){
		$sql.=" OR CardID2='$smartcardid' OR CardID3='$smartcardid' OR CardID4='$smartcardid'";
	}
	$temp = $li->returnVector($sql);
	if(sizeof($temp)!=0){
		$error_msg[] = $smartcardid;
	}
}

## check Staff Code duplication ###
if($staffCode!=""){
	$sql="SELECT UserID FROM INTRANET_USER WHERE StaffCode='$staffCode'";
	$temp = $li->returnVector($sql);
	if(sizeof($temp)!=0){
		$error_msg[] = $staffCode;
	}
}

## check Barcode duplication ###
if($barcode != ""){
	$sql = "SELECT UserID FROM INTRANET_USER WHERE Barcode='$barcode'";
	$temp = $laccount->returnVector($sql);
	if(sizeof($temp)!=0){
		$error_msg[] = $barcode;
	}
}

## check iMail plus type in UserEmail ##
if($sys_custom['iMailPlus']['EmailAliasName'] && $UseImapUserLogin==1 && $ImapUserLogin!='' && $ImapUserLogin != $userlogin){
	$sql = "SELECT UserID FROM INTRANET_USER WHERE UserLogin='$ImapUserLogin'";
	$temp = $li->returnVector($sql);
	$IMap = new imap_gamma(1);
	if(!intranet_validateEmail($ImapUserLogin."@".$SYS_CONFIG['Mail']['UserNameSubfix']) || sizeof($temp)!=0 || in_array($ImapUserLogin, (array)$system_reserved_account) || $IMap->is_user_exist($ImapUserLogin."@".$SYS_CONFIG['Mail']['UserNameSubfix'])){
		$error_msg[] = $ImapUserLogin."@".$SYS_CONFIG['Mail']['UserNameSubfix'];
	}
}

if(sizeof($error_msg)>0) {
	$action = "new.php";
	$errorList = implode(',', $error_msg);	
}



if(sizeof($error_msg)>0) { 
?>

	<body onload="document.form1.submit()">
	<form name="form1" method="post" action="new.php">
		<input type="hidden" name="TeachingType" id="TeachingType" value="<?=$TeachingType?>">
		<input type="hidden" name="recordstatus" id="recordstatus" value="<?=$recordstatus?>">
		<input type="hidden" name="userlogin" id="userlogin" value="<?=$userlogin?>">
		<input type="hidden" name="pwd" id="pwd" value="<?=$pwd?>">
		<input type="hidden" name="teaching" id="teaching" value="<?=$teaching?>">
		<input type="hidden" name="email" id="email" value="<?=$email?>">
		<input type="hidden" name="status" id="status" value="<?=$status?>">
		<input type="hidden" name="smartcardid" id="smartcardid" value="<?=$smartcardid?>">
		<input type="hidden" name="staffCode" id="staffCode" value="<?=$staffCode?>">
		<input type="hidden" name="barcode" id="barcode" value="<?=$barcode?>">
		<input type="hidden" name="grade" id="grade" value="<?=$grade?>">
		<input type="hidden" name="duty" id="duty" value="<?=$duty?>">
		<input type="hidden" name="engname" id="engname" value="<?=$engname?>">
		<input type="hidden" name="chiname" id="chiname" value="<?=$chiname?>">
		<input type="hidden" name="nickname" id="nickname" value="<?=$nickname?>">
		<input type="hidden" name="gender" id="gender" value="<?=$gender?>">
		<input type="hidden" name="engTitle" id="engTitle" value="<?=$engTitle?>">
		<input type="hidden" name="chiTitle" id="chiTitle" value="<?=$chiTitle?>">
		<input type="hidden" name="address" id="address" value="<?=$address?>">
		<input type="hidden" name="country" id="country" value="<?=$country?>">
		<input type="hidden" name="homePhone" id="homePhone" value="<?=$homePhone?>">
		<input type="hidden" name="officePhone" id="officePhone" value="<?=$officePhone?>">
		<input type="hidden" name="mobilePhone" id="mobilePhone" value="<?=$mobilePhone?>">
		<input type="hidden" name="faxPhone" id="faxPhone" value="<?=$faxPhone?>">
		<input type="hidden" name="open_webmail" id="open_webmail" value="<?=$open_webmail?>">
		<input type="hidden" name="open_file" id="open_file" value="<?=$open_file?>">
		<input type="hidden" name="error" id="error" value="<?=$errorList?>">
		<input type="hidden" name="remark" id="remark" value="<?=$remark?>">
		<input type="hidden" name="position" id="position" value="<?=$position?>" />
		<input type="hidden" name="contractType" id="contractType" value="<?=$contractType?>" />
		<input type="hidden" name="contractStartDate" id="contractStartDate" value="<?=$contractStartDate?>" />
		<input type="hidden" name="contractEndDate" id="contractEndDate" value="<?=$contractEndDate?>" />
	<?php if($sys_custom['iMailPlus']['EmailAliasName']){ ?>	
		<input type="hidden" name="ImapUserLogin" id="ImapUserLogin" value="<?=$ImapUserLogin?>" />
		<input type="hidden" name="UseImapUserLogin" id="UseImapUserLogin" value="<?=$UseImapUserLogin?>" />
	<?php } ?>
	<?php if($sys_custom['iMail']['UserDisplayOrder']){ ?>
		<input type="hidden" name="DisplayOrder" id="DisplayOrder" value="<?=$DisplayOrder?>" />
	<?php } ?>
	<?php if(!$ssoservice["Google"]["Valid"]){ ?>
		<input type="hidden" name="googleUserLogin" id="googleUserLogin" value="<?=$googleUserLogin?>" />
	<?php } ?>
	</form>
	<script language="javascript">
		document.form1.submit();
	</script>
	</body>
<? 
	exit;

	} else { 
		
	$domain_name = ($intranet_email_generation_domain==""? $_SERVER["SERVER_NAME"]:$intranet_email_generation_domain);
	$target_email = ($email=="" ? $userlogin . "@".$domain_name : $email);
	
	if($li->UserNewAdd($userlogin, $target_email, trim($engname), trim($chiname), $status)==1) {
		
		# create & update info in INTRANET_USER
		
		$uid = $li->db_insert_id();
		$UserPassword = $pwd;
		if ($intranet_authentication_method=="HASH") {
		    $fieldname .= "UserPassword = NULL,HashedPass = MD5('$userlogin$UserPassword$intranet_password_salt'),";
		} else {
		    $fieldname .= "UserPassword = NULL, ";
		}
		$fieldname .= "NickName = '".intranet_htmlspecialchars(trim($nickname))."', ";
		//$fieldname .= "Title = '".intranet_htmlspecialchars(trim($Title))."', ";
		$fieldname .= "Gender = '$gender', ";
		$fieldname .= "HomeTelNo = '".intranet_htmlspecialchars(trim($homePhone))."', ";
		$fieldname .= "OfficeTelNo = '".intranet_htmlspecialchars(trim($officePhone))."', ";
		$fieldname .= "MobileTelNo = '".intranet_htmlspecialchars(trim($mobilePhone))."', ";
		$fieldname .= "FaxNo = '".intranet_htmlspecialchars(trim($faxPhone))."', ";
		$fieldname .= "Address = '".intranet_htmlspecialchars(trim($address))."', ";
		$fieldname .= "Country = '".intranet_htmlspecialchars(trim($country))."', ";
		$fieldname .= "Remark = '".intranet_htmlspecialchars(trim($remark))."', ";
		$fieldname .= "Grade = '".intranet_htmlspecialchars(trim($grade))."', ";
		$fieldname .= "Duty = '".intranet_htmlspecialchars(trim($duty))."', ";
		$fieldname .= "RecordType = '".TYPE_TEACHER."'";
		/*
		$fieldname .= "FaxNo = '$FaxNo', ";
		$fieldname .= "ICQNo = '$ICQNo', ";
		$fieldname .= "Info = '$Info', ";
		$fieldname .= "Remark = '$Remark', ";
		$fieldname .= "DateOfBirth = '$DateOfBirth', ";
		$fieldname .= "ClassNumber = '$ClassNumber' ";
		$fieldname .= ",YearOfLeft = '$YearOfLeft'";
		*/	

		# GSuite
		
		//google api
//		error_reporting(E_ALL);
//		ini_set('display_errors', 1);
		if($ssoservice["Google"]["Valid"]){
			include_once($PATH_WRT_ROOT."includes/google_api/libgoogleapi.php");
			include_once($PATH_WRT_ROOT."includes/sso/libSSO_db.php");
			include_once($PATH_WRT_ROOT."includes/sso/libGoogleSSO.php");
			$libGoogleSSO = new libGoogleSSO();
			
			//prepare password
			$password_for_google_account_creation = $pwd;
			
			$gmail_input_array = array();
			$array_config_index = $libGoogleSSO->getAllGoogleAPIConfigIndex();
			foreach((array)$array_config_index as $config_index){
				$gmail_array_index = 'gmail_' . $config_index;
				$gmail_input_array[$gmail_array_index] = $$gmail_array_index;
			}
			
			$google_have_error=false;
			$array_error_message = $libGoogleSSO->syncUserFromEClassToGoogle($uid, $userlogin, $status, $engname, $password_for_google_account_creation, $gmail_input_array, $chiname);

			foreach((array)$array_error_message as $config_index => $error_message){
		
				if($error_message==''){
					if($google_account_exists_during_creation){
						$_SESSION["SSO"]["Google"]["Message"]['Config'][$config_index] = "ACCOUNT_CREATED";
					}else{
						$_SESSION["SSO"]["Google"]["Message"]['Config'][$config_index] = "NO_MESSAGE";
					}
				}else{
					$google_have_error = true;
					$_SESSION["SSO"]["Google"]["Message"]['Config'][$config_index] = $error_message;
				}
			}
		}
	
		if ((isset($module_version['StaffAttendance'])&&$module_version['StaffAttendance']!="" ) || (isset($plugin['payment'])&& $plugin['payment'])) {
			$fieldname .= ",CardID = '$smartcardid'";
		}
		$fieldname .= ", StaffCode = '$staffCode'";
		$fieldname .= ", Barcode = '$barcode'";
		$TitleEnglish = intranet_htmlspecialchars(trim($engTitle));
		$TitleChinese = intranet_htmlspecialchars(trim($chiTitle));
		$fieldname .= ",TitleEnglish = '$TitleEnglish'";
		$fieldname .= ",TitleChinese = '$TitleChinese'";
		
		if ($teaching == 1) {   # Teaching staff -> Teacher group
		    $target_idgroup = 1;
		    $fieldname .= ",Teaching = ".TEACHING;
		}
		else if ($teaching == 'S') {
			$fieldname .= ",Teaching = 'S'";
		}
		else {                   # Non-teaching staff -> Admin staff group
		    $target_idgroup = 3;
		    $fieldname .= ",Teaching = ".NONTEACHING;
		}
		if($sys_custom['iMail']['UserDisplayOrder']){
			$fieldname .= ",DisplayOrder='$DisplayOrder' ";
		}
		$fieldname .= ", DateModified = now()";
		$sql = "UPDATE INTRANET_USER SET $fieldname WHERE UserID = '$uid'";
		$li->db_db_query($sql);
		
		# Log down two-way hashed/encrypted password
		$lauth->UpdateEncryptedPassword($uid, $UserPassword);
		
		if ($teaching == 'S') {
			// don't add to identity group for supply teacher at this moment
		}
		else {
			# insert into INTRANET_USERGROUP (assign to teacher group / admin staff group)
			$gpID = ($teaching==1) ? 1 : 3;
			$lgroup = new libgroup($gpID);
			$defaultRoleID = $lgroup->returnGroupDefaultRole($gpID);
			$sql = "INSERT INTO INTRANET_USERGROUP (GroupID, UserID, RoleID, DateInput, DateModified) VALUES ('$gpID', '$uid', '$defaultRoleID', now(), now())";
			$li->db_db_query($sql);
		}
		
				
		# Assign to Group
		for($i=0; $i<sizeof($GroupID); $i++){
			if($GroupID[$i] != "") {
				$lgroup = new libgroup($GroupID[$i]);
				$defaultRoleID = $lgroup->returnGroupDefaultRole($GroupID[$i]);
				$sql = "INSERT INTO INTRANET_USERGROUP (GroupID, UserID, RoleID, DateInput, DateModified) VALUES ('".$GroupID[$i]."', '$uid', '$defaultRoleID', now(), now())";
				$li->db_db_query($sql);
			}
		}
	
		if($ssoservice["Google"]["Valid"] && $ssoservice["Google"]['mode']['user_and_group']){
			include_once($PATH_WRT_ROOT."includes/google_api/libgoogleapi.php");
			include_once($PATH_WRT_ROOT."includes/sso/libSSO_db.php");
			include_once($PATH_WRT_ROOT."includes/sso/libGoogleSSO.php");
			$libGoogleSSO = new libGoogleSSO();

			$libGoogleSSO->syncGroupForUserFromEClassToGoogle($laccount, $userlogin, $gmail_input_array, $GroupID);
		}
		
		# Assign to Role
		$identityType = ($teaching==0) ? "NonTeaching" : "Teaching";
		for($i=0; $i<sizeof($RoleID); $i++){
			if($RoleID[$i] != "" && $RoleID[$i]!=0) {
				$sql = "INSERT INTO ROLE_MEMBER (RoleID, UserID, DateInput, InputBy, DateModified, ModifyBy, IdentityType) VALUES ('".$RoleID[$i]."', '$uid', now(), '$UserID', now(), '$UserID', '$identityType')";
				$li->db_db_query($sql);
			}
		}
		
		$msg = 1;

		# insert MyCalendar
		$lcalendar->insertMyCalendar($uid);
		
		# insert group calendar
		//if (!empty($GroupID)){
			$cal_sql = "
				insert into CALENDAR_CALENDAR_VIEWER 
				(CalID, UserID, GroupID, GroupType, Access, Color, Visible)
				select g.CalID, '$uid', g.GroupID, 'E', 'R', '2f75e9', '1' 
				from INTRANET_GROUP as g inner join INTRANET_USERGROUP as u 
				on g.GroupID = u.GroupID and u.UserID = '$uid'
			"; //echo $cal_sql.'<br><br>';
			$li->db_db_query($cal_sql);
		//}
		# insert calendar viewer to calendars that have shared to the user's groups
		$cal_group_sql = "SELECT g.GroupID FROM INTRANET_GROUP as g 
							INNER JOIN INTRANET_USERGROUP as u ON u.GroupID=g.GroupID 
							WHERE u.UserID='$uid'";
		$cal_group_ary = $li->returnVector($cal_group_sql);
		for($i=0;$i<sizeof($cal_group_ary);$i++) {
			$lcalendar->addCalendarViewerToGroup($cal_group_ary[$i],$uid);
		}
		
		#### Special handling
		$li->UpdateRole_UserGroup();
		
		$acl_field = 0;
		
		# iFolder
		if ($open_file && in_array(TYPE_TEACHER, $personalfile_identity_allowed) )
		{
		    if ($personalfile_type=='FTP')
		    {
		        $lftp = new libftp();
		        if ($lftp->isAccountManageable)
		        {
		            $file_failed = ($lftp->open_account($userlogin,$UserPassword)? 0:1);
		            $file_content = get_file_content($intranet_root."/file/account_file_quota.txt");
		            if ($file_content == "")
		            {
		                $userquota = array(10,10,10);
		            }
		            else
		            {
		                $userquota = explode("\n", $file_content);
		            }
		            $target_quota = $userquota[TYPE_TEACHER-1];
		            $lftp->setTotalQuota($userlogin, $target_quota, "iFolder");
		        }
		    }
		    $acl_field += 2;
		}
		
		# gamma mail
		if($plugin['imail_gamma'])
		{
			if($EmailStatus=='enable')
			{
				$IMap = new imap_gamma(1);
				if($sys_custom['iMailPlus']['EmailAliasName'] && $UseImapUserLogin==1){
					$ImapUserLogin = trim($ImapUserLogin);
					$IMapEmail = $ImapUserLogin."@".$SYS_CONFIG['Mail']['UserNameSubfix'];
					if(!$IMap->is_user_exist($IMapEmail)){
						if($IMap->open_account($IMapEmail,$pwd))
						{
							$laccount->setIMapUserEmail($uid,$IMapEmail);
							$IMap->SetTotalQuota($IMapEmail,$Quota,$uid);
							if($ImapUserLogin != trim($userlogin)){
								$sql = "UPDATE INTRANET_USER SET ImapUserLogin='".$ImapUserLogin."' WHERE UserID='$uid'";
								$li->db_db_query($sql);
							}
							// Block receive/send internet mails when [iMail Settings > Default Usage Rights and Quota > Internal mail only] is on
							$internal_only = $IMap->internal_mail_only; // index 0 is staff, 1 is student, 2 is parent, 3 is alumni
							if($internal_only[USERTYPE_STAFF-1]){ // shift index by one
								$IMap->addGroupBlockExternal(array($IMapEmail));
								//$sql = "UPDATE INTRANET_SYSTEM_ACCESS SET ACL = ACL - 1 WHERE UserID = '$uid' AND ACL IN (1,3)";
						    	//$li->db_db_query($sql);
						    	$acl_field -= 1; // offset the following $acl_field += 1
							}
						}
					}
				}else{
					$IMapEmail = trim($userlogin)."@".$SYS_CONFIG['Mail']['UserNameSubfix'];
					if($IMap->open_account($IMapEmail,$pwd))
					{
						$laccount->setIMapUserEmail($uid,$IMapEmail);
						$IMap->SetTotalQuota($IMapEmail,$Quota,$uid);
						
						// Block receive/send internet mails when [iMail Settings > Default Usage Rights and Quota > Internal mail only] is on
						$internal_only = $IMap->internal_mail_only; // index 0 is staff, 1 is student, 2 is parent, 3 is alumni
						if($internal_only[USERTYPE_STAFF-1]){ // shift index by one
							$IMap->addGroupBlockExternal(array($IMapEmail));
							//$sql = "UPDATE INTRANET_SYSTEM_ACCESS SET ACL = ACL - 1 WHERE UserID = '$uid' AND ACL IN (1,3)";
					    	//$li->db_db_query($sql);
					    	$acl_field -= 1; // offset the following $acl_field += 1
						}
					}
				}
			}
			$acl_field += 1;
		}
		else
		{
			# Webmail
			if ($open_webmail && in_array(TYPE_TEACHER, $webmail_identity_allowed))
			{
				
				if ($lwebmail->has_webmail) {
					$lwebmail->open_account($userlogin, $UserPassword);
					$acl_field += 1;
					$file_content = get_file_content($intranet_root."/file/account_mail_quota.txt");
					if ($file_content == "") {
						$userquota = array(10,10,10);
					}
					else {
						$userquota = explode("\n", $file_content);
					}
					$target_quota = $userquota[TYPE_TEACHER-1];
					$lwebmail->setTotalQuota($userlogin, $target_quota, "iMail");
				}
			}
			
			# iMail Quota
			if ($special_feature['imail'])
			{
			    # Get Default Quota
			    $file_content = get_file_content($intranet_root."/file/campusmail_set.txt");
			    if ($file_content == "")
			    {
			        $quota = 10;
			    }
			    else
			    {
			        $userquota = explode("\n", $file_content);
		            $quotaline = $userquota[TYPE_TEACHER-1];
			        $temp = explode(":",$quotaline);
			        $quota = $temp[1];
			    }
			    $sql = "INSERT INTO INTRANET_CAMPUSMAIL_USERQUOTA (UserID, Quota) VALUES ('$uid', '$quota')";
			    $li->db_db_query($sql);
			    
			    $sql = "INSERT INTO INTRANET_CAMPUSMAIL_USED_STORAGE (UserID, QuotaUsed) VALUES ('$uid', 0)";
			    $li->db_db_query($sql);
			}
		}
		
		$sql = "INSERT INTO INTRANET_SYSTEM_ACCESS (UserID, ACL) VALUES ($uid, $acl_field)";
		$li->db_db_query($sql);
		
		# Call sendmail function (Email notification of registration)
		list($mailSubject,$mailBody) = $laccount->returnEmailNotificationData($email, $userlogin, $UserPassword, stripslashes($engname), stripslashes($chiname));
		//$mailTo = $uid;
//		$lwebmail->sendModuleMail((array)$mailTo,$mailSubject,$mailBody);
		include_once($PATH_WRT_ROOT."includes/libsendmail.php");
		$lsendmail = new libsendmail();
		$webmaster = get_webmaster();
		$headers = "From: $webmaster\r\n";
		if(!$sys_custom['DisableEmailAfterCreatedNewAccount'] && $email != "" && intranet_validateEmail($email,true)){
			$lsendmail->SendMail($email, $mailSubject, $mailBody,"$headers");
		}
				
		# LDAP
		if ($intranet_authentication_method=='LDAP')
		{
		    $lldap = new libldap();
		    if ($lldap->isAccountControlNeeded()) {
		        $lldap->connect();
		        $lldap->openAccount($userlogin, $UserPassword);
		    }
		}
		
		# Payment
		if($plugin['payment']) {
		    $sql = "INSERT INTO PAYMENT_ACCOUNT (StudentID, Balance) VALUES ('$uid',0)";
		    $li->db_db_query($sql);
		}
		
		
		# SchoolNet integration
		if ($plugin['SchoolNet'])
		{
		    include_once($PATH_WRT_ROOT."includes/libschoolnet.php");
		    $lschoolnet = new libschoolnet();
	        # Param: array in ($userlogin, $password, $DOB, $gender, $cname, $ename, $tel, $mobile, $address, $email, $teaching)
	        $sql = "SELECT UserLogin, UserPassword, DATE_FORMAT(DateOfBirth,'%Y-%m-%d'), Gender, Chinesename, EnglishName, HomeTelNo, MobileTelNo, Address, UserEmail, Teaching FROM INTRANET_USER WHERE UserID = '$uid'";
	        $data = $li->returnArray($sql, 11);
	        $lschoolnet->addStaffUser($data);
		}	
		
		# Stem Learning Scheme Cust
		if($plugin['iPortfolio'] && $sys_custom['iPortfolio_auto_add_teacher_to_group']){
		    include_once($PATH_WRT_ROOT."includes/libeclass40.php");
            
            $sql = "Select course_id From {$eclass_db}.course Where RoomType = 4";
            $rs = $li->returnVector($sql);
            $course_id = $rs[0];
            
            $lo = new libeclass($course_id);
            $lo->setRoomType($lo->getEClassRoomType($course_id));
            $lo->eClassUserAddFullInfoByUserID($uid,'T');
		}

		if($plugin['SDAS_module']['KISMode']){
			$cols = "UserID, InfoField, InfoValue, DateInput, InputBy";
			$vals = "";
			$vals .= "('$uid','Position','$position', NOW(), '{$_SESSION['UserID']}'),";
			$vals .= "('$uid','ContractType','$contractType', NOW(), '{$_SESSION['UserID']}'),";
			$vals .= "('$uid','ContractStartDate','$contractStartDate', NOW(), '{$_SESSION['UserID']}'),";
			$vals .= "('$uid','ContractEndDate','$contractEndDate', NOW(), '{$_SESSION['UserID']}'),";
			$sql=" INSERT INTO INTRANET_USER_ADDITIONAL_INFO ($cols)";
			$lu->db_db_query($sql);
		}
		
		$flag = "AddSuccess";
	} else {
		$flag = "AddUnsuccess";	
	}

	$uid =IntegerSafe($uid);
	
	$le->addTitleToCourseUser($target_email,$TitleEnglish,$TitleChinese);
	$successAry['synUserDataToModules'] = $lu->synUserDataToModules($uid);
	
	$laccount->suspendEmailFunctions($uid, $status == 1);
	
	// SIS Create account to PMS 
	if($sys_custom['SISPMSRegistry']){
		include_once($PATH_WRT_ROOT."includes/cust/libregistrySIS_PMS.php");
		$liPMS = new libregistryPMS();
		$Par = array(
					 $pwd,$intranet_password_salt,$nickname
					,$Title,$gender,$homePhone,$faxPhone
					,$MobileTelNo,$FaxNo,$Address,$country,$Info
					,$remark,$TeachingType,$engTitle,$chiTitle
					,$TitleEnglish,$TitleChinese,$uid,$identity,$teaching
					);
		$rs = $liPMS->CreateUser($userlogin, $target_email, $engname, $chiname, $status,$Par);
		$flag = $rs ? 'AddSuccess' : 'AddUnsuccess';
	}
	
	if($plugin['radius_server']){
		$laccount->disableUserInRadiusServer(array($uid), !$enable_wifi_access);
	}
	
	intranet_closedb();
	if($ssoservice["Google"]["Valid"]){
		if($google_have_error){
			header("Location: edit.php?uid=$uid");
			exit();
		}
	}
	
	header("Location: index.php?xmsg=$flag");
}
?>