<?php
// Editing by 
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libauth.php");
//include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
//include_once($PATH_WRT_ROOT."includes/libgrouping.php");
//include_once($PATH_WRT_ROOT."includes/librole.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/DHL/libdhl.php");

intranet_auth();
intranet_opendb();

$add_update_key = isset($_POST['user_id']) && $_POST['user_id']>0 ? 'Update' : 'Add';

if(!$sys_custom['DHL'] || !($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Staff"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"])) {
	$_SESSION['DHL_USERMGMT_RETURN_MSG'] =$Lang['General']['ReturnMessage'][$add_update_key.'Unsuccess'];
	intranet_closedb();
	header("Location:index.php");
	exit;
}

//$laccount = new libaccountmgmt();
$lauth = new libauth();
$lu = new libuser();
$libdhl = new libdhl();


$data = array('CheckUserLogin'=>1,'UserLogin'=>$_POST['UserLogin']);
if(isset($_POST['user_id'])){
	$data['ExcludeUserID'] = $_POST['user_id'];
}
$check_userlogin_records = $libdhl->getUserRecords($data);

if(trim($_POST['UserEmail']) != '')
{
	$data = array('CheckUserEmail'=>1,'UserEmail'=>$_POST['UserEmail']);
	if(isset($_POST['user_id'])){
		$data['ExcludeUserID'] = $_POST['user_id'];
	}
	$check_useremail_records = $libdhl->getUserRecords($data);
}

if(count($check_userlogin_records)>0 || count($check_useremail_records)>0){
	$_SESSION['DHL_USERMGMT_RETURN_MSG'] = $Lang['General']['ReturnMessage'][$add_update_key.'Unsuccess'];
	intranet_closedb();
	header("Location:index.php");
	exit;
}


//$libdhl->Start_Trans();

$success_or_newRecordId = $libdhl->upsertUserRecord($_POST);
//debug_pr($success_or_newRecordId);
$user_id = isset($_POST['user_id']) && $_POST['user_id']>0 ? $_POST['user_id'] : $success_or_newRecordId;

if($user_id != '' && $user_id > 0)
{
	$department_id = trim($_POST['DepartmentID']);
	
	$libdhl->deleteDepartmentUserRecord('',$user_id);
	$libdhl->insertDepartmentUserRecord($department_id,array($user_id));
	
	# Log down two-way hashed/encrypted password
	if(trim($_POST['UserPassword'])!='')
	{
		$lauth->UpdateEncryptedPassword($user_id, trim($_POST['UserPassword']));
	}
	
	$lu->synUserDataToModules($user_id);
}

//if($success_or_newRecordId){
//	$libdhl->Commit_Trans();
//}else{
//	$libdhl->RollBack_Trans();
//}

$_SESSION['DHL_USERMGMT_RETURN_MSG'] = $success_or_newRecordId? $Lang['General']['ReturnMessage'][$add_update_key.'Success'] : $Lang['General']['ReturnMessage'][$add_update_key.'Unsuccess'];

intranet_closedb();
header("Location:index.php");
exit;
?>