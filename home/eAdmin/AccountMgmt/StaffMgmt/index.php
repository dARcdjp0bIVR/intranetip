<?php
// modifying :   

############ Change Log Start ###############
#
#   Date:   2020-05-11 Cameron
#           don't allow to delete and prompt alert message when delete teacher if eGuidance module is enabled and the teacher is in SEN case confidential viewer list or pic list
#
#	Date:	2019-03-12 Carlos
#			$sys_custom['iMail']['ExportForwardingEmail'] - Added export forwarding emails for Webmail.
#
#   Date:   2018-11-12 Cameron
#           consolidate Amway, Oaks, CEM to use the same customized flag control ($sys_custom['project']['CourseTraining']['IsEnable'])
#
#   Date:   2018-08-15 Henry
#           fixed sql injection
#
#   Date:   2018-08-14 Cameron
#           hide "Email", "Send Reset Password Email" button, hide Class column
#
#	Date:	2017-12-22 Cameron
#			hide all button except new for Oaks
#
#	Date:	2016-09-28 (Ivan) [ip.2.5.7.10.1]
#			Added teaching = 'S' supply teacher logic
#
#	Date:	2016-08-09 Carlos
#			$sys_custom['BIBA_AccountMgmtCust'] - display House group.
#
#	Date:	2015-12-30 Carlos
#			$plugin['radius_server'] - added table tool buttons for enable/disable Wi-Fi access and export Wi-Fi access status list.
#
#	Date:	2015-12-15 Cameron
#			hide all button except new for Amway
#
#	Date:   2014-11-21 Omas
#			Fix : Improved SQL can be sort by classname correctly
#
#	Date:   2014-10-23 Ryan
#			SIS Hide Import
#
#	Date:	2014-03-11	Carlos
#			Added [Import Forwarding Emails] & [Export Forwarding Emails] buttons for Lassel College
#
#	Date:	2013-02-08	YatWoon
#			use "user_id[]" instead of userID[]
#
#	Date:	2011-01-27 Yatwoon
#			can also search with SmartCardID
# 
#	Date:	2011-01-10	YatWoon
#			- IP25 UI
#			- Add "Active", "Suspend" tool options
#			- set cookies
#
#	Date:	2010-06-21	YatWoon
#			add online help button
#
############ Change Log End ###############

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

if($sys_custom['DHL']){
	include("./dhl_users.php");
	exit;
}

# set cookies
$arrCookies[] = array("ck_page_size", "numPerPage");
$arrCookies[] = array("ck_page_no", "pageNo");
$arrCookies[] = array("ck_page_order", "order");
$arrCookies[] = array("ck_page_field", "field");

$arrCookies[] = array("ck_staff_TeachingType", "TeachingType");
$arrCookies[] = array("ck_staff_recordstatus", "recordstatus");
$arrCookies[] = array("ck_staff_keyword", "keyword");
if(isset($clearCoo) && $clearCoo == 1)
{
	clearCookies($arrCookies);
} 
else 
{
	updateGetCookies($arrCookies);
}

intranet_auth();
intranet_opendb();


if(!($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Staff"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"])) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$laccount = new libaccountmgmt();
$linterface = new interface_html();

$CurrentPageArr['StaffMgmt'] = 1;
$CurrentPage = "Mgmt_Account";

$TAGS_OBJ[] = array($Lang['Group']['UserList']);
$MODULE_OBJ['title'] = $Lang['Header']['Menu']['StaffAccount'];

# online help button
//$onlineHelpBtn = gen_online_help_btn_and_layer('user','staff');
//$TAGS_OBJ_RIGHT[] = array($onlineHelpBtn);


$toolbar .= $linterface->GET_LNK_NEW("javascript:newUser()",$button_new,"","","",0);

// hide all button except new for Amway
if (!$sys_custom['project']['CourseTraining']['IsEnable']) {
	//disable import function on SIS
	if(!$sys_custom['SISPMSRegistry']){
		$toolbar .= $linterface->GET_LNK_IMPORT("javascript:goImport()",$button_import,"","","",0);
	}
	$toolbar .= $linterface->GET_LNK_EXPORT("javascript:goExport()",$button_export,"","","",0);
	if (!$sys_custom['project']['HKPF']) {
	   $toolbar .= $linterface->GET_LNK_EMAIL("javascript:checkPost(document.form1,'email.php')",$button_email,"","","",0);
	}
	if($plugin['imail_gamma'] && $sys_custom['iMailPlus']['BatchSetForwardingEmail']){
		$toolbar .= $linterface->GET_LNK_IMPORT("../import/import_forwarding_email.php?TabID=".USERTYPE_STAFF,$Lang['AccountMgmt']['ImportForwardingEmails'],"","","",0);
		$toolbar .= $linterface->GET_LNK_EXPORT("../export_forwarding_email.php?TabID=".USERTYPE_STAFF,$Lang['AccountMgmt']['ExportForwardingEmails'],"","","",0);
	}
	if($plugin['webmail'] && $sys_custom['iMail']['ExportForwardingEmail']){
		$toolbar .= $linterface->GET_LNK_EXPORT("../export_forwarding_email.php?TabID=".USERTYPE_STAFF,$Lang['AccountMgmt']['ExportForwardingEmails'],"","","",0);
	}
	if($plugin['radius_server']){
		$toolbar .= $linterface->GET_LNK_EXPORT("../export_wifi_status.php?TabID=".USERTYPE_STAFF,$Lang['AccountMgmt']['ExportWiFiAccessStatus'],"","","",0);
	}
}

# Staff Type 
$staffTypeMenu = "<select name=\"TeachingType\" id=\"TeachingType\" onChange=\"reloadForm()\">";
$staffTypeMenu .= "<option value=''".((!isset($TeachingType) || $TeachingType=="") ? " selected" : "").">{$Lang['StaffAttendance']['AllStaff']}</option>";
$staffTypeMenu .= "<option value='1'".(($TeachingType=='1') ? " selected" : "").">{$Lang['Identity']['TeachingStaff']}</option>";
$staffTypeMenu .= "<option value='0'".(($TeachingType=='0') ? " selected" : "").">{$Lang['Identity']['NonTeachingStaff']}</option>";
if ($plugin['SLRS']) {
	$staffTypeMenu .= "<option value='S'".(($TeachingType=='S') ? " selected" : "").">{$Lang['Identity']['SupplyTeacher']}</option>";
}
$staffTypeMenu .= "</select>";

# Record Status 
$recordStatusMenu = "<select name=\"recordstatus\" id=\"recordstatus\" onChange=\"reloadForm()\">";
$recordStatusMenu .= "<option value=''".((!isset($recordstatus) || $recordstatus=="") ? " selected" : "").">$i_Discipline_Detention_All_Status</option>";
$recordStatusMenu .= "<option value='1'".(($recordstatus=='1') ? " selected" : "").">{$Lang['Status']['Active']}</option>";
$recordStatusMenu .= "<option value='0'".(($recordstatus=='0') ? " selected" : "").">{$Lang['Status']['Suspended']}</option>";
$recordStatusMenu .= "</select>";


if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
if($order == "") $order = 1;
if($field == "") $field = 0;
$pageSizeChangeEnabled = true;

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;

$li = new libdbtable2007($field, $order, $pageNo);

# Teaching type
if($TeachingType==1) {
	$conds = " AND Teaching=1";
}
else if($TeachingType=='S') {
	$conds = " AND Teaching = 'S'";
}
else if(isset($TeachingType) && $TeachingType!='') {
	$conds = " AND ((Teaching!=".TEACHING." AND Teaching != 'S') OR Teaching IS NULL)";
}

# record status
if(isset($recordstatus) && $recordstatus==1)
	$conds .= " AND iu.RecordStatus=".STATUS_APPROVED;
else if(isset($recordstatus) && $recordstatus!=1 && $recordstatus!="")
	$conds .= " AND iu.RecordStatus=".STATUS_SUSPENDED;
if($keyword != "")	
	$conds .= " AND (iu.UserLogin like '%$keyword%' OR
				iu.UserEmail like '%$keyword%' OR
				iu.EnglishName like '%$keyword%' OR
				iu.ChineseName like '%$keyword%' OR
				yc.ClassTitleB5 like '%$keyword%' OR
				yc.ClassTitleEN like '%$keyword%' OR
				iu.WebSamsRegNo like '%$keyword%' OR
				iu.CardID like '%$keyword%'
				)";
// CONCAT('<a href=javascript:; title=\'',IFNULL(IF(EnglishName='','---',EnglishName),'---'),'\' onclick=goEdit(',UserID,')>',IFNULL(IF(EnglishName='','---',EnglishName),'---'),'</a>') as EnglishName,	
//$sql = "SELECT
//			IFNULL(IF(EnglishName='', '---', EnglishName),'---') as EnglishName,
//			IFNULL(IF(ChineseName='', '---', ChineseName),'---') as ChineseName,
//			CASE Teaching
//				WHEN 1 THEN '".$Lang['Identity']['TeachingStaff']."'
//				WHEN 0 THEN '".$Lang['Identity']['NonTeachingStaff']."'
//				ELSE '---' END as teaching,
//			UserID,
//			UserLogin,
//			IFNULL(LastUsed, '---') as LastUsed,
//			CASE RecordStatus
//				WHEN 0 THEN '". $Lang['Status']['Suspended']."'
//				WHEN 1 THEN '". $Lang['Status']['Active'] ."'
//				ELSE '$i_status_suspended' END as recordStatus,
//			CONCAT('<input type=\'checkbox\' name=\'user_id[]\' id=\'user_id[]\' value=', UserID ,'>') as checkbox
//		FROM 
//			INTRANET_USER
//		WHERE
//			RecordType = ".TYPE_TEACHER."
//			$conds ";


$yearClassIdAry = array();
$AcademicYearID = Get_Current_Academic_Year_ID();
$sql = "SELECT YearClassID FROM YEAR_CLASS WHERE AcademicYearID = '".$AcademicYearID."'";
$yearClassIdAry = $laccount->ReturnVector($sql);
$ClassName = Get_Lang_Selection('yc.ClassTitleB5','yc.ClassTitleEN');

$houseGroupNameField = Get_Lang_Selection('g.TitleChinese','g.Title');

$sql = "SELECT
			IFNULL(IF(iu.EnglishName='', '---', iu.EnglishName),'---') as EnglishName,
			IFNULL(IF(iu.ChineseName='', '---', iu.ChineseName),'---') as ChineseName,
			IF (iu.Teaching = 1, '".$Lang['Identity']['TeachingStaff']."',
				IF (iu.Teaching = 'S', '".$Lang['Identity']['SupplyTeacher']."', 
					IF (iu.Teaching = 0 || iu.Teaching is null || iu.Teaching = '', '".$Lang['Identity']['NonTeachingStaff']."', '---')
				)
			) as teaching,";
if (!$sys_custom['project']['HKPF']) {
	$sql .= " IFNULL( GROUP_CONCAT(DISTINCT $ClassName SEPARATOR ', ') , '---') as ClassName,";
}
if($sys_custom['BIBA_AccountMgmtCust']){
	$sql .= " GROUP_CONCAT(DISTINCT $houseGroupNameField SEPARATOR ', ') as House, ";
}
$sql .= " iu.UserLogin,
			IFNULL(iu.LastUsed, '---') as LastUsed,
			CASE iu.RecordStatus
				WHEN 0 THEN '". $Lang['Status']['Suspended']."'
				WHEN 1 THEN '". $Lang['Status']['Active'] ."'
				ELSE '$i_status_suspended' END as recordStatus,
			CONCAT('<input type=\'checkbox\' name=\'user_id[]\' id=\'user_id[]\' value=', iu.UserID ,'>') as checkbox,
			GROUP_CONCAT(yc.ClassTitleEN SEPARATOR ', ') as ClassNameSorting,
			iu.UserID as UserID
		FROM 
			INTRANET_USER as iu
			LEFT OUTER JOIN YEAR_CLASS_TEACHER as yct ON iu.UserID = yct.UserID AND yct.YearClassID IN ('".implode("','",$yearClassIdAry)."')
			LEFT OUTER JOIN YEAR_CLASS as yc ON yct.YearClassID = yc.YearClassID ";
if($sys_custom['BIBA_AccountMgmtCust']){
	$sql .= " LEFT JOIN INTRANET_USERGROUP as ug ON ug.UserID=iu.UserID 
			  LEFT JOIN INTRANET_GROUP as g ON g.GroupID=ug.GroupID AND g.RecordType=4 AND g.AcademicYearID='$AcademicYearID' ";
}
$sql .= " WHERE
			iu.RecordType = ".TYPE_TEACHER."
			$conds 
		GROUP BY
			iu.UserID
		";


$result = $laccount->returnArray($sql,9);			
 
$li->sql = $sql;
$field_array = array("EnglishName", "ChineseName", "teaching");
if (!$sys_custom['project']['HKPF']) {
    $field_array[] = "ClassNameSorting";
}
$li->field_array = $field_array;

if($sys_custom['BIBA_AccountMgmtCust']){
	$li->field_array = array_merge($li->field_array, array("House"));
}
$li->field_array = array_merge($li->field_array, array("UserLogin", "LastUsed", "recordStatus"));
$li->no_col = sizeof($li->field_array)+2;
$li->IsColOff = "UserMgmtStaffAccount";

$pos = 0;
$li->column_list .= "<th class='num_check'>#</th>\n";
$li->column_list .= "<th width='15%' >".$li->column($pos++, $i_UserEnglishName)."</th>\n";
$li->column_list .= "<th width='15%' >".$li->column($pos++, $i_UserChineseName)."</th>\n";
$li->column_list .= "<th width='10%' >".$li->column($pos++, $i_eClass_Identity)."</th>\n";
if (!$sys_custom['project']['HKPF']) {
    $li->column_list .= "<th width='20%' >".$li->column($pos++, $i_general_class)."</th>\n";
}
if($sys_custom['BIBA_AccountMgmtCust']){
	$li->column_list .= "<th width='5%' >".$li->column($pos++, $Lang['AccountMgmt']['House'])."</th>\n";
}
$li->column_list .= "<th width='15%' >".$li->column($pos++, $i_UserLogin)."</th>\n";
$li->column_list .= "<th width='15%' >".$li->column($pos++, $i_frontpage_eclass_lastlogin)."</th>\n";
$li->column_list .= "<th width='10%' >".$li->column($pos++, $i_UserRecordStatus)."</th>\n";
$li->column_list .= "<th width='1'>".$li->check("user_id[]")."</th>\n";

//debug_pr($li->built_sql());

$MODULE_OBJ = $laccount->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$xmsg]);

?>
<script language="javascript">
function reloadForm() {
	document.form1.action = "index.php";
	document.form1.submit();
}

function goEdit(id) {
	document.form1.action = "edit.php";
	document.getElementById('uid').value = id;
	document.form1.submit();
}

function newUser() {
	document.form1.action = "new.php";
	document.form1.submit();
}

function goExport() {
	document.form1.action = "../export.php?TabID=<?=TYPE_TEACHER?>";
	document.form1.submit();
}
<?if(!$sys_custom['SISPMSRegistry']):?>
function goImport() {
	window.location.href ="../import/import.php?TabID=<?=TYPE_TEACHER?>";
}
<?endif?>
function changeFormAction(obj,element,url,type) {
	if(countChecked(obj,element)==0)
		alert(globalAlertMsg2);
	else {
		if(confirm(globalAlertMsgSendPassword)) {
			document.form1.action = url;
			document.form1.submit();
		}
	}
}

function checkRemoveStaff(obj,element,page,confirmMsg){
    if(countChecked(obj,element)==0) {
        alert(globalAlertMsg2);
    }
    else{
<?php if ($plugin['eGuidance']): ?>
        $.ajax({
            dataType: "json",
            type: "POST",
            url: 'ajax.php?action=checkSENCaseConfidentialViewerOrPIC',
            data : $('#form1').serialize(),
            success: function(ajaxReturn){
            	if (ajaxReturn != null && ajaxReturn.success){
    				if (ajaxReturn.isStaffSENCaseConfidentialViewerOrPIC) {
    					alert("<?php echo $Lang['AccountMgmt']['DeleteStaffAcctWarning']['ChangeSENCasePIC'];?>");
    				}
    				else {
    			        if (confirmMsg == null || confirmMsg == '') {
    			        	confirmMsg = globalAlertMsg3;
    			        }
    			        	
    			        if(confirm(confirmMsg)){	            
    			            obj.action=page;                
    			            obj.method="POST";
    			            obj.submit();				             
    			        }
    				}
            	}
            },
            error: show_ajax_error
        });

<?php else:?>
        if (confirmMsg == null || confirmMsg == '') {
        	confirmMsg = globalAlertMsg3;
        }
        	
        if(confirm(confirmMsg)){	            
            obj.action=page;                
            obj.method="POST";
            obj.submit();				             
        }
<?php endif;?>        
        
    }
}

function show_ajax_error() {
	alert('<?=$Lang['General']['AjaxError']?>');
}

</script>
<form name="form1" id="form1" method="post" action="">

<div class="content_top_tool">
	<?=$toolbar?>
	<?=$linterface->Get_Search_Box_Div("keyword", $keyword)?>
<br style="clear:both" />
</div>

<div class="table_board">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr class="table-action-bar">
	<td valign="bottom">
		<?=$staffTypeMenu?> <?=$recordStatusMenu?>
	</td>
	<td valign="bottom">
		<div class="common_table_tool">
<?php if (!$sys_custom['project']['HKPF']):?>		
		<a href="javascript:changeFormAction(document.form1,'user_id[]','../email_password.php')" class="tool_email"><?=$Lang['AccountMgmt']['SendResetPasswordEmail']?></a>
<?php endif;?>		
		<? if($plugin['radius_server']){ ?>
        <a href="javascript:checkEditMultiple(document.form1,'user_id[]','../enable_wifi_access.php')" class="tool_approve"><?=$Lang['AccountMgmt']['EnableWifiAccess']?></a>
        <a href="javascript:checkEditMultiple(document.form1,'user_id[]','../disable_wifi_access.php')" class="tool_reject"><?=$Lang['AccountMgmt']['DisableWifiAccess']?></a>
        <? } ?>
		<? if($recordstatus!=1) {?>
		<a href="javascript:checkActivate(document.form1,'user_id[]','../approve.php')" class="tool_approve"><?=$Lang['Status']['Activate']?></a>
		<? } ?>
		<? if($recordstatus=="" || $recordstatus==1) {?>
		<a href="javascript:checkSuspend(document.form1,'user_id[]','../suspend.php')" class="tool_reject"><?=$Lang['Status']['Suspend']?></a>
		<? } ?>		
		<a href="javascript:checkRemoveStaff(document.form1,'user_id[]','remove.php','<?=$Lang['AccountMgmt']['DeleteUserWarning']?>')" class="tool_delete"><?=$button_delete?></a>
		</div>
	</td>
</tr>
</table>

<?= $li->display()?>
	
<input type="hidden" name="uid" id="uid" value="" />
<input type="hidden" name="pageNo" id="pageNo" value="<?=$li->pageNo; ?>" />
<input type="hidden" name="order" id="order" value="<?=$li->order; ?>" />
<input type="hidden" name="field" id="field" value="<?=$li->field; ?>" />
<input type="hidden" name="page_size_change" id="page_size_change" value="" />
<input type="hidden" name="numPerPage" id="numPerPage" value="<?=$li->page_size?>" />
<input type="hidden" name="comeFrom" value="/home/eAdmin/AccountMgmt/StaffMgmt/index.php">
<input type="hidden" name="clearCoo" id="clearCoo" value="" />
</form>
</body>

<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>