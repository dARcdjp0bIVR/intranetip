<?php
$PATH_WRT_ROOT = "../../../../";
include($PATH_WRT_ROOT."includes/global.php");
include($PATH_WRT_ROOT."includes/libdb.php");
include($PATH_WRT_ROOT."includes/libgrouping.php");
include($PATH_WRT_ROOT."includes/libaccountmgmt.php");
include($PATH_WRT_ROOT."includes/libinterface.php");
include($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

if(!($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Staff"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"])) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

if(sizeof($_POST)==0) {
	header("Location : index.php");	
	exit;
}

intranet_opendb();

$lo = new libgrouping();
$linterface = new interface_html();
$laccount = new libaccountmgmt();

$CurrentPageArr['StaffMgmt'] = 1;
$CurrentPage = "Mgmt_Account";

$TAGS_OBJ[] = array($Lang['Group']['UserList']);
$MODULE_OBJ['title'] = $Lang['Header']['Menu']['StaffAccount'];

if($TeachingType==1)
	$conds = " AND Teaching=$TeachingType";
else if(isset($TeachingType) && $TeachingType!='')
	$conds = " AND (Teaching!=".TEACHING." OR Teaching IS NULL)";

# record status
if(isset($recordstatus) && $recordstatus==1)
	$conds .= " AND RecordStatus=".STATUS_APPROVED;
else if(isset($recordstatus) && $recordstatus!=1 && $recordstatus!="")
	$conds .= " AND RecordStatus=".STATUS_SUSPENDED;
if($keyword != "")	
	$conds .= " AND (UserLogin like '%$keyword%' OR
				UserEmail like '%$keyword%' OR
				EnglishName like '%$keyword%' OR
				ChineseName like '%$keyword%' OR
				ClassName like '%$keyword%' OR
				WebSamsRegNo like '%$keyword%'
				)";
				

$result = $laccount->getUserNameArrayByConds(TYPE_TEACHER, $conds);
for($i=0; $i<sizeof($result); $i++) {
	$emailList .= "<option value='{$result[$i][0]}' selected>{$result[$i][1]}</option>";
}
				
$MODULE_OBJ = $laccount->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();

?>

<script language="javascript">
function checkform(obj){
        if(!check_text(obj.subject, "<?=$i_alert_pleasefillin.$i_email_subject; ?>.")) return false;
        if(!check_text(obj.message, "<?=$i_alert_pleasefillin.$i_email_message; ?>.")) return false;
        return (confirm("<?=$i_email_sendemail; ?>?")) ? true : false;
}

function goBack() {
	document.form1.action = "index.php";
	document.form1.submit();
}
</script>

<form name="form1" action="email_update.php" method="post" onSubmit="return checkform(this);">
<table width="560" border="0" cellpadding="0" cellspacing="0" align="center">
	<tr>
		<td>
		<blockquote>
			<table width="400" border="0" cellpadding="2" cellspacing="1">
				<tr><td align="right"><?=$i_email_to; ?>:</td><td><select name="UserEmail[]" size="5" multiple><?/*=$lo->displayUserEmailOptionByConds(TYPE_TEACHER, $conds); */?><?=$emailList?></select></td></tr>
				<tr><td align="right"><?=$i_email_subject; ?>:</td><td><input class="text" type="text" name="subject" size="35"></td></tr>
				<tr><td>&nbsp;</td><td><textarea name="message" cols="55" rows="15" wrap="virtual"></textarea></td></tr>
			</table>
		</blockquote>
		</td>
	</tr>
</table>
	
<table width="560" border="0" cellpadding="0" cellspacing="0" align="center">
	<tr><td height="22" style="vertical-align:bottom"><hr size="1"></td></tr>
	<tr>
		<td align="right">
			<?= $linterface->GET_ACTION_BTN($button_send, "submit")?>
			<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "")?>
			<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "goBack()")?>
		</td>
	</tr>
</table>

<input type="hidden" name="TeachingType" id="TeachingType" value="<?=$TeachingType?>">
<input type="hidden" name="recordstatus" id="recordstatus" value="<?=$recordstatus?>">
<input type="hidden" name="pageNo" id="pageNo" value="<?=$pageNo?>">
<input type="hidden" name="order" id="order" value="<?=$order?>">
<input type="hidden" name="field" id="field" value="<?=$field?>">
<input type="hidden" name="keyword" id="keyword" value="<?=$keyword?>">
</form>

<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>