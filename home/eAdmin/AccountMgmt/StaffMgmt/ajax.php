<?
//Using: 
/*
 * 	Log
 * 
 * 	Description: output json format data
 *
 * 	2020-05-11 [Cameron] create this file
 */

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/json.php");

intranet_auth();
intranet_opendb();

if(!($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Staff"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"])) {
    include_once($PATH_WRT_ROOT."includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit;
}
if ($junior_mck) {
    if (phpversion_compare('5.2') != 'ELDER') {
        $characterset = 'utf-8';
    }
    else {
        $characterset = 'big5';
    }
}
else {
    $characterset = 'utf-8';
}

header('Content-Type: text/html; charset='.$characterset);

$ljson = new JSON_obj();

$json['success'] = false;
$x = '';
$action = $_GET['action'] ? $_GET['action'] : $_POST['action'];
$remove_dummy_chars = ($junior_mck) ? false : true;	// whether to remove new line, carriage return, tab and back slash

switch($action) {
    
    case 'checkSENCaseConfidentialViewerOrPIC':
        include_once ($PATH_WRT_ROOT . "includes/eGuidance/libguidance.php");
        $libguidance = new libguidance();
        
        $staffIDAry = IntegerSafe($_POST['user_id']);
        $json['isStaffSENCaseConfidentialViewerOrPIC'] = $libguidance->isStaffSENCaseConfidentialViewerOrPIC($staffIDAry);
        $json['success'] = true;
        break;

}

if ($remove_dummy_chars) {
	$x = remove_dummy_chars_for_json($x);
}

if (($junior_mck) && (phpversion_compare('5.2') != 'ELDER')) {
    $x = convert2unicode($x,true,1);
}

$json['html'] = $x;
echo $ljson->encode($json);


intranet_closedb();
?>