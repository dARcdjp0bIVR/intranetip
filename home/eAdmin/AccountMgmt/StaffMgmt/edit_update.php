<?php
# using: Henry

############# Change Log
#	Date:	2020-07-14 Philips
#			Added position, contractType, contractStartDate, contractEndDate for KIS CEES
#
#   Date:   2019-11-19 Paul
#           Fixed: updated Google SSO not to sync data on is_readonly case
#
#	Date:	2019-08-04 (Chris)
#			- Updated SSO implementation.
#	Date:	2019-06-24 (Carlos)
#			- Empty UserPassword field.
#   Date:   2019-04-30 (Cameron)
#           - fix cross site scripting by applying cleanHtmlJavascript() to variables
#           - fix potential sql injection problem by enclosed var with apostrophe
#
#	Date:	2019-01-21 (Carlos) [ip.2.5.10.2.1]: $sys_custom['SupplementarySmartCard'] checked duplication of CardID2, CardID3 and CardID4.
#
#   Date:   2018-10-24 (Chris) [ip.2.5.9.10.1]: modified variables
#
#   Date:   2018-08-30 (Pun) [ip.2.5.9.10.1]: Modified syncUserFromEClassToGoogle(), added $chiname
#
#   Date:   2018-08-15 (Henry) [ip.2.5.9.10.1]: fixed sql injection
#
#   Date:   2018-08-09 (Henry) [ip.2.5.9.10.1]: fixed sql injection
#
#   Date:   2018-08-07 (Cameron) [ip.2.5.9.10.1]: add Grade and Duty for HKPF
#
#	Date:	2018-01-03 (Carlos) [ip.2.5.9.1.1]: Improved password checking.
#
#	Date:	2016-11-21 (HenryHM) [ip.2.5.8.1.1]
#			$ssoservice["Google"]["Valid"] - add GSuite Logic
#
#	Date:	2016-10-04 (Carlos) [ip.2.5.8.1.1]
#			$sys_custom['iMail']['UserDisplayOrder'] - added input field DisplayOrder
#
#	Date:	2016-09-28 (Ivan) [ip.2.5.7.10.1]
#			Added teaching = 'S' supply teacher logic
#
#	Date:	2015-12-30 (Carlos)
#			$plugin['radius_server'] - added Wi-Fi access option.
#
#	Date:	2015-12-24 (Yuen)
#			no need to remove user from student and parent groups (ID 2 & 4)
#
#	Date:	2015-08-14 (Omas)
#			Add Edit Log
#
#	Date:	2015-05-26 (Carlos)
#			added libaccountmgmt.php suspendEmailFunctions($targetUserId, $resume=false) to resume or suspend mail server auto forward and auto reply functions.
#
#	Date:	2015-03-23 (Bill)
#			fixed - cannot add staff to usergroup if usergroup without default group role
#
#	Date:	2014-10-16 (Ryan) - Update account into PMS for SIS Flag : $sys_custom['SISPMSRegistry']
#
#	Date:	2014-09-19 (Bill)
#			Add RoleID to INTRANET_USERGROUP to display default role in Group
#
#	Date: 	2014-09-03 (Carlos) - iMail plus: Block receive/send internet mails when [iMail Settings > Default Usage Rights and Quota > Internal mail only] is on
#
#	Date:	2014-05-29 (Carlos) - only send notification email if recipient email is valid
#
#	2014-05-07 (Carlos): Added suspend/unsuspend iMail plus account api call
#
#	Date:	2013-12-12 (Carlos)
#			$sys_custom['iMailPlus']['EmailAliasName'] - user can input its own iMail plus user name
#
#	Date:	2013-03-14	Carlos
#			remove shared group calendars for user and add shared group calendars to user
#
#	Date:	2012-10-03	Carlos
#			fix failed to enable iMail plus account when password method is HASHED
#
#	Date:	2012-08-30	YatWoon
#			add barcode
#
#	Date:	2012-08-03	YatWoon
#			remove the flag checking for websams staff code
#
#	Date:	2012-08-01	Ivan
#			added logic to syn user info to library system
#
#	Date:	2012-04-02 	Jason
#			fix the problem of the failure of updating user_email from ip to eclass
#
#	Date:	2012-03-14	YatWoon
#			allow admin update user's personal email [Case#2012-0313-1603-23071]
#
#	Date:	2012-02-07	YatWoon
#			add: add hashed password
#
#	Date:	2011-12-13	YatWoon
#			fixed: Case#2011-1213-1402-29073, the problem is occurs after the v11.1 critical fix
#
#	Date:	2011-09-30  Carlos
#			update two-way hashed/encrypted password if password is modified
#
#	Date:	2011-03-30	Henry Chow
#			include library "libldap.php"
#
#	Date:	2011-03-28	YatWoon
#			change email notification subject & content data
#
#	2011-03-07	(Henry Chow)
#		Password not display in edit page (related case : #2011-0304-1154-52067)
#		if password is empty => keep unchange ; if password is entered => change
#
#	Date: 	2010-12-30 (Jason)
#			change the update eclass info function by eClass40UserUpdateInfo()
#
#	Date:	2010-08-18	YatWoon
#			Add "Remark" field
#
#################

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libauth.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libemail.php");
// include_once($PATH_WRT_ROOT."lang/email.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."includes/libftp.php");
include_once($PATH_WRT_ROOT."includes/imap_gamma.php");
include_once($PATH_WRT_ROOT."includes/libeclass40.php");
include_once($PATH_WRT_ROOT."includes/libldap.php");
include_once($PATH_WRT_ROOT."includes/icalendar.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."includes/libregistry.php");

intranet_auth();
intranet_opendb();

if(!($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Staff"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"])) {
    include_once($PATH_WRT_ROOT."includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit;
}

### start handle sql injection and cross site scripting
$uid = IntegerSafe($uid);
$TeachingType = cleanHtmlJavascript($TeachingType);
$recordstatus = cleanHtmlJavascript($recordstatus);
$keyword = cleanHtmlJavascript($keyword);
$pwd = cleanHtmlJavascript($pwd);
$teaching = cleanHtmlJavascript($teaching);
$status = IntegerSafe($status);
$smartcardid = cleanHtmlJavascript($smartcardid);
$staffCode = cleanHtmlJavascript($staffCode);
$barcode = cleanHtmlJavascript($barcode);
$grade = cleanHtmlJavascript($grade);
$duty = cleanHtmlJavascript($duty);
$engname = cleanHtmlJavascript($engname);
$chiname = cleanHtmlJavascript($chiname);
$nickname = cleanHtmlJavascript($nickname);
$gender = cleanHtmlJavascript($gender);
$engTitle = cleanHtmlJavascript($engTitle);
$chiTitle = cleanHtmlJavascript($chiTitle);
$address = cleanHtmlJavascript($address);
$country = cleanHtmlJavascript($country);
$homePhone = cleanHtmlJavascript($homePhone);
$officePhone = cleanHtmlJavascript($officePhone);
$mobilePhone = cleanHtmlJavascript($mobilePhone);
$faxPhone = cleanHtmlJavascript($faxPhone);
$errorList = cleanHtmlJavascript($errorList);
$remark = cleanHtmlJavascript($remark);
$position = cleanHtmlJavascript($position);
$contractType = cleanHtmlJavascript($contractType);
$contractStartDate = cleanHtmlJavascript($contractStartDate);
$contractEndDate = cleanHtmlJavascript($contractEndDate);
$UserEmail = cleanHtmlJavascript($UserEmail);
$EmailStatus = cleanHtmlJavascript($EmailStatus);
if($sys_custom['iMailPlus']['EmailAliasName']){
    $OldImapUserEmail = cleanHtmlJavascript($OldImapUserEmail);
    $ImapUserLogin = cleanHtmlJavascript($ImapUserLogin);
    $UseImapUserLogin = cleanHtmlJavascript($UseImapUserLogin);
}
if($plugin['radius_server']){
    $enable_wifi_access = IntegerSafe($enable_wifi_access);
}
if($sys_custom['iMail']['UserDisplayOrder']){
    $DisplayOrder = cleanHtmlJavascript($DisplayOrder);
}
if($ssoservice["Google"]["Valid"]){
	$googleuserlogin = cleanHtmlJavascript($googleuserlogin);
}
### end handle sql injection and cross site scripting

$lauth = new libauth();
$laccount = new libaccountmgmt();
$lu = new libuser($uid);
$le = new libeclass();
$lcalendar = new icalendar();

# Log
if($sys_custom['AccountMgmt']['LogEditImportUser']){
    include_once($PATH_WRT_ROOT."includes/libupdatelog.php");
    $liblog = new libupdatelog();
    $liblog->INSERT_UPDATE_LOG('AccountMgmt', 'StaffMgmt', 'target='.$uid);
    unset($liblog);
}

$SettingArr = $lu->RetrieveUserInfoSettingInArrary(array('EnablePasswordPolicy_'.$lu->RecordType));
$PasswordLength = $SettingArr['EnablePasswordPolicy_'.$lu->RecordType];
if ($PasswordLength<6) $PasswordLength = 6;

$dataAry = array();

$userInfo = $laccount->getUserInfoByID($uid);

if($sys_custom['UseStrongPassword']){
    if(trim($pwd) != ''){
        $check_password_result = $lauth->CheckPasswordCriteria($pwd,$lu->UserLogin,$PasswordLength);
        if(!in_array(1,$check_password_result)){
            $error_msg[] = $pwd;
        }
    }
}

# check email
$UserEmail = intranet_htmlspecialchars(trim($UserEmail));

if ($UserEmail != "" && $userInfo['UserEmail'] != $UserEmail)
{
    if ($lu->isEmailExists($UserEmail) || $le->isEmailExistInSystem($UserEmail))
    {
        header("Location: edit.php?uid=$uid&xmsg=EmailUsed");
        exit;
    }
}

## check Smart Card ID duplication ###
if($smartcardid!=""){
    $sql = "SELECT UserID FROM INTRANET_USER WHERE (CardID='$smartcardid'".($sys_custom['SupplementarySmartCard']?" OR CardID2='$smartcardid' OR CardID3='$smartcardid' OR CardID4='$smartcardid'":"").") AND UserID!='$uid'";
    $temp = $laccount->returnVector($sql);
    if(sizeof($temp)!=0){
        $error_msg[] = $smartcardid;
    }
}

## check Staff Code duplication ###
if($staffCode!=""){
    $sql = "SELECT UserID FROM INTRANET_USER WHERE StaffCode='$staffCode' AND UserID!='$uid'";
    $temp = $laccount->returnVector($sql);
    if(sizeof($temp)!=0){
        $error_msg[] = $staffCode;
    }
}

## check Barcode duplication ###
if($barcode != ""){
    $sql = "SELECT UserID FROM INTRANET_USER WHERE Barcode='$barcode' AND UserID!='$uid'";
    $temp = $laccount->returnVector($sql);
    if(sizeof($temp)!=0){
        $error_msg[] = $barcode;
    }
}

## check iMail plus type in UserEmail ##
if($sys_custom['iMailPlus']['EmailAliasName'] && $UseImapUserLogin==1 && $ImapUserLogin!='' && $ImapUserLogin != $userInfo['UserLogin']){
    $sql = "SELECT UserID FROM INTRANET_USER WHERE UserLogin='$ImapUserLogin'";
    $temp = $laccount->returnVector($sql);
    $IMap = new imap_gamma(1);
    if(!intranet_validateEmail($ImapUserLogin."@".$SYS_CONFIG['Mail']['UserNameSubfix']) || sizeof($temp)!=0 || in_array($ImapUserLogin, (array)$system_reserved_account) ||
        ($OldImapUserEmail != $ImapUserLogin."@".$SYS_CONFIG['Mail']['UserNameSubfix'] && $IMap->is_user_exist($ImapUserLogin."@".$SYS_CONFIG['Mail']['UserNameSubfix']))){
            $error_msg[] = $ImapUserLogin."@".$SYS_CONFIG['Mail']['UserNameSubfix'];
    }
}

if(sizeof($error_msg)>0) {
    $errorList = implode(',', $error_msg);
}

if(sizeof($error_msg)>0) {
    ?>

	<body onload="document.form1.submit()">
	<form name="form1" method="post" action="edit.php">
		<input type="hidden" name="TeachingType" id="TeachingType" value="<?=$TeachingType?>">
		<input type="hidden" name="recordstatus" id="recordstatus" value="<?=$recordstatus?>">
		<input type="hidden" name="keyword" id="keyword" value="<?=$keyword?>">
		<input type="hidden" name="pwd" id="pwd" value="<?=$pwd?>">
		<input type="hidden" name="teaching" id="teaching" value="<?=$teaching?>">
		<input type="hidden" name="status" id="status" value="<?=$status?>">
		<input type="hidden" name="smartcardid" id="smartcardid" value="<?=$smartcardid?>">
		<input type="hidden" name="staffCode" id="staffCode" value="<?=$staffCode?>">
		<input type="hidden" name="barcode" id="barcode" value="<?=$barcode?>">
		<input type="hidden" name="grade" id="grade" value="<?=$grade?>">
		<input type="hidden" name="duty" id="duty" value="<?=$duty?>">
		<input type="hidden" name="engname" id="engname" value="<?=$engname?>">
		<input type="hidden" name="chiname" id="chiname" value="<?=$chiname?>">
		<input type="hidden" name="nickname" id="nickname" value="<?=$nickname?>">
		<input type="hidden" name="gender" id="gender" value="<?=$gender?>">
		<input type="hidden" name="engTitle" id="engTitle" value="<?=$engTitle?>">
		<input type="hidden" name="chiTitle" id="chiTitle" value="<?=$chiTitle?>">
		<input type="hidden" name="address" id="address" value="<?=$address?>">
		<input type="hidden" name="country" id="country" value="<?=$country?>">
		<input type="hidden" name="homePhone" id="homePhone" value="<?=$homePhone?>">
		<input type="hidden" name="officePhone" id="officePhone" value="<?=$officePhone?>">
		<input type="hidden" name="mobilePhone" id="mobilePhone" value="<?=$mobilePhone?>">
		<input type="hidden" name="faxPhone" id="faxPhone" value="<?=$faxPhone?>">
		<input type="hidden" name="error" id="error" value="<?=$errorList?>">
		<input type="hidden" name="remark" id="remark" value="<?=$remark?>">
		<input type="hidden" name="position" id="position" value="<?=$position?>" />
		<input type="hidden" name="contractType" id="contractType" value="<?=$contractType?>" />
		<input type="hidden" name="contractStartDate" id="contractStartDate" value="<?=$contractStartDate?>" />
		<input type="hidden" name="contractEndDate" id="contractEndDate" value="<?=$contractEndDate?>" />
		<input type="hidden" name="uid" id="uid" value="<?=$uid?>">
		<input type="hidden" name="UserEmail" id="UserEmail" value="<?=$UserEmail?>">
	<?php if($sys_custom['iMailPlus']['EmailAliasName']){ ?>
		<input type="hidden" name="OldImapUserEmail" id="OldImapUserEmail" value="<?=$OldImapUserEmail?>" />
		<input type="hidden" name="ImapUserLogin" id="ImapUserLogin" value="<?=$ImapUserLogin?>" />
		<input type="hidden" name="UseImapUserLogin" id="UseImapUserLogin" value="<?=$UseImapUserLogin?>" />
	<?php } ?>
		<?php if($plugin['radius_server']){ ?>
		<input type="hidden" name="enable_wifi_access" id="enable_wifi_access" value="<?=$enable_wifi_access?>" />
		<?php } ?>
	<?php if($sys_custom['iMail']['UserDisplayOrder']){ ?>
		<input type="hidden" name="DisplayOrder" id="DisplayOrder" value="<?=$DisplayOrder?>" />
	<?php } ?>
	<?php if(!$ssoservice["Google"]["Valid"]){ ?>
		<input type="hidden" name="googleUserLogin" id="googleUserLogin" value="<?=$googleUserLogin?>" />
	<?php } ?>
	</form>
	<script language="javascript">
		document.form1.submit();
	</script>
	</body>
<? 
	exit;
}


# retrieve original email
//$sql = "select UserEmail from INTRANET_USER where UserID=$uid";
//$result = $lu->returnVector($sql);
$ori_UserEmail = $lu->UserEmail;
$new_UserEmail = $UserEmail;

if ($intranet_authentication_method=="HASH" && $pwd!='') {
	$modifyPassword = 1;
}
else {
	$modifyPassword = ($pwd!="" && $pwd!=$original_pass) ? 1 : 0;
}	

if($modifyPassword) {
	if ($intranet_authentication_method=="HASH") {
		//$dataAry['UserPassword'] = "HashedPass = MD5('$userlogin$UserPassword$intranet_password_salt'),";
		$dataAry['UserPassword'] = "";
		//$dataAry['HashedPass'] = "md5('$userlogin$UserPassword$intranet_password_salt')";
		$dataAry['HashedPass'] = "md5('".$lu->UserLogin.$pwd.$intranet_password_salt."')";
	} else {
		$dataAry['UserPassword'] = "";
	}
}

if(!$modifyPassword){
	if($intranet_authentication_method=="HASH"){
		$UserPassword = $lauth->GetUserDecryptedPassword($uid);
	}else{
		$UserPassword = $userInfo['UserPassword'];
	}
}else{
	$UserPassword = $pwd;
}
		
$dataAry['Teaching'] = $teaching;
$dataAry['UserEmail'] = intranet_htmlspecialchars(stripslashes(trim($UserEmail)));
$dataAry['RecordStatus'] = $status;
$dataAry['CardID'] = $smartcardid;
$dataAry['StaffCode'] = $staffCode;
$dataAry['Barcode'] = $barcode;
$dataAry['EnglishName'] = intranet_htmlspecialchars(stripslashes(trim($engname)));
$dataAry['ChineseName'] = intranet_htmlspecialchars(stripslashes(trim($chiname)));
$dataAry['NickName'] = intranet_htmlspecialchars(trim($nickname));
$dataAry['Gender'] = $gender;
$dataAry['TitleEnglish'] = intranet_htmlspecialchars(trim($engTitle));
$dataAry['TitleChinese'] = intranet_htmlspecialchars(trim($chiTitle));
$dataAry['Address'] = intranet_htmlspecialchars(trim($address));
$dataAry['Country'] = $country;
$dataAry['HomeTelNo'] = intranet_htmlspecialchars(trim($homePhone));
$dataAry['OfficeTelNo'] = intranet_htmlspecialchars(trim($officePhone));
$dataAry['MobileTelNo'] = intranet_htmlspecialchars(trim($mobilePhone));
$dataAry['FaxNo'] = intranet_htmlspecialchars(trim($faxPhone));
$dataAry['Remark'] = intranet_htmlspecialchars(trim($remark));
$dataAry['Grade'] = intranet_htmlspecialchars(trim($grade));
$dataAry['Duty'] = intranet_htmlspecialchars(trim($duty));
if($sys_custom['iMail']['UserDisplayOrder']){
	$dataAry['DisplayOrder'] = $DisplayOrder;
}
if(!$ssoservice["Google"]["Valid"]){
	$dataAry['GoogleUserLogin'] = cleanHtmlJavascript($googleUserLogin);
}

$success = $laccount->updateUserInfo($uid, $dataAry);

if($modifyPassword && $success){
	$lauth->UpdateEncryptedPassword($uid, trim($pwd));
}

if ($teaching == 1) {   # Teaching staff -> Teacher group
	$other_idgroup = 3;
    $target_idgroup = 1;
} 
else {                   # Non-teaching staff -> Admin staff group
	$other_idgroup = 1;
    $target_idgroup = 3;
}
/*
$sql = "DELETE FROM INTRANET_USERGROUP WHERE UserID='$uid' AND GroupID IN (1,3)";
$lu->db_db_query($sql);

$sql = "INSERT INTO INTRANET_USERGROUP (GroupID, UserID, DateInput, DateModified) VALUES ($target_idgroup, $uid, now(), now())";
$lu->db_db_query($sql);
*/
$sql = "SELECT GroupID FROM INTRANET_USERGROUP WHERE UserID='$uid'";
$existing_groups = $lu->returnVector($sql);

if ($teaching == 'S') {
	// remove from teaching and non-teaching groups
	$sql = "DELETE FROM INTRANET_USERGROUP WHERE UserID='$uid' AND GroupID in (1, 3)";
	$lu->db_db_query($sql);
}
else {
	$sql = "SELECT COUNT(*) FROM INTRANET_USERGROUP WHERE UserID='$uid' AND GroupID='$target_idgroup'";
	$count = $lu->returnVector($sql);
	
	if($count[0]==0) {
		
		$sql = "DELETE FROM INTRANET_USERGROUP WHERE UserID='$uid' AND GroupID='$other_idgroup'";
		$lu->db_db_query($sql);
		
		$lgroup = new libgroup($target_idgroup);
		$defaultRoleID = $lgroup->returnGroupDefaultRole($target_idgroup);
		
		$sql = "INSERT INTO INTRANET_USERGROUP (GroupID, UserID, RoleID, DateInput, DateModified) VALUES ('$target_idgroup', '$uid', '$defaultRoleID', now(), now())";
		$lu->db_db_query($sql);
	
	}
}


$userlogin = $lu->UserLogin;

if($plugin["imail_gamma"])
{
	$IMap = new imap_gamma(1);
	$IMapEmail = trim($userlogin)."@".$SYS_CONFIG['Mail']['UserNameSubfix'];
	
	if($EmailStatus=="enable")
	{
		if($sys_custom['iMailPlus']['EmailAliasName']){
			$imap_userlogin_email = trim($userlogin)."@".$SYS_CONFIG['Mail']['UserNameSubfix'];
			if($UseImapUserLogin == 1) {
				$ImapUserLogin = trim($ImapUserLogin);
				$IMapEmail = $ImapUserLogin."@".$SYS_CONFIG['Mail']['UserNameSubfix'];
				if($ImapUserLogin == $userlogin) {
					$sql = "UPDATE INTRANET_USER SET ImapUserLogin=NULL WHERE UserID='$uid'";
					$lu->db_db_query($sql);
				}else{
					$sql = "UPDATE INTRANET_USER SET ImapUserLogin='$ImapUserLogin' WHERE UserID='$uid'";
					$lu->db_db_query($sql);
				}
			}else if($OldImapUserEmail != ""){
				$IMapEmail = $OldImapUserEmail;
			}
			if($IMapEmail != $OldImapUserEmail){
				//if(!$IMap->is_user_exist($IMapEmail)) {
					$changeAccountName_result = $IMap->changeAccountName($OldImapUserEmail,$IMapEmail);
					$IMap->Change_Preference_Mailbox($OldImapUserEmail,$IMapEmail);
				//}
			}
		}
		
		if($IMap->is_user_exist($IMapEmail))
		{
			$account_exist = true;
			if($modifyPassword)
				$IMap->change_password($IMapEmail, $UserPassword);
		}
		else
		{
			$igmma_password = $UserPassword;
			
			$account_exist = $IMap->open_account($IMapEmail, $igmma_password);
			// Block receive/send internet mails when [iMail Settings > Default Usage Rights and Quota > Internal mail only] is on
			$internal_only = $IMap->internal_mail_only; // index 0 is staff, 1 is student, 2 is parent, 3 is alumni
			if($internal_only[USERTYPE_STAFF-1]){ // shift index by one
				$IMap->addGroupBlockExternal(array($IMapEmail));
				$sql = "UPDATE INTRANET_SYSTEM_ACCESS SET ACL = ACL - 1 WHERE UserID = '$uid' AND ACL IN (1,3)";
		    	$lu->db_db_query($sql);
			}
			
		}
		if($account_exist)
		{
			$laccount->setIMapUserEmail($uid,$IMapEmail);
			$result["setQuota"] = $IMap->SetTotalQuota($IMapEmail,$Quota, $uid);
			$IMap->setUnsuspendUser($IMapEmail,"iMail",$UserPassword); // resume iMail plus account
		}
	}
	else
	{
		# disable gamma mail
		$laccount->setIMapUserEmail($uid,"");
		
		$IMap->setSuspendUser($IMapEmail,"iMail"); // suspend iMail plus account
	}
}

	
$lwebmail = new libwebmail();
if(!$plugin["imail_gamma"])
{
	# update webmail password
	if ($modifyPassword && $lwebmail->has_webmail)
		$lwebmail->change_password($userlogin, $UserPassword, "iMail");
}
	
# FTP management
if ($plugin['personalfile'])
{
	$lftp = new libftp();
	if ($modifyPassword && $lftp->isFTP)
	{
		$lftp->changePassword($userlogin, $UserPassword, "iFolder");
	}
}
    
if ($intranet_authentication_method=='LDAP')
{
	$lldap = new libldap();
	if ($modifyPassword && $lldap->isPasswordChangeNeeded())
	{
		$lldap->connect();
		$lldap->changePassword($userlogin, $UserPassword);
	}
}

# send email
//	$le = new libsendmail();
//	$le->send_email ($lu->UserEmail, reset_password_title(), reset_password_body($lu->Title, $lu->EnglishName) );
if($modifyPassword)
{
	list($mailSubject,$mailBody) = $laccount->returnEmailNotificationData("", "", "", stripslashes($engname), stripslashes($chiname), 1);
// 	$lwebmail->sendModuleMail((array)$uid,reset_password_title(), reset_password_body($lu->Title, $lu->EnglishName));
// 	$lwebmail->sendModuleMail((array)$uid,$mailSubject,$mailBody);
		include_once($PATH_WRT_ROOT."includes/libsendmail.php");
		$lsendmail = new libsendmail();
		$webmaster = get_webmaster();
		$headers = "From: $webmaster\r\n";
		if(!$sys_custom['DisableEmailAfterChangedAccountPassword'] && $UserEmail !="" && intranet_validateEmail($UserEmail,true)){
			$lsendmail->SendMail($UserEmail, $mailSubject, $mailBody,"$headers");
		}
} 

# SchoolNet
if ($plugin['SchoolNet']) {
	include_once($PATH_WRT_ROOT."includes/libschoolnet.php");
	$lschoolnet = new libschoolnet();
	
	$sql = "SELECT UserLogin, UserPassword,ClassName, ClassNumber, DATE_FORMAT(DateOfBirth,'%Y-%m-%d'), Gender, Chinesename, EnglishName, HomeTelNo, MobileTelNo, Address, UserEmail FROM INTRANET_USER WHERE UserID = '$uid'";
	$data = $lu->returnArray($sql,12);
	if($modifyPassword)
		$data[0][1] = $UserPassword;
	$lschoolnet->addStudentUser($data);
}

# GSuite
//error_reporting(E_ALL);
//ini_set('display_errors', 1);
if($ssoservice["Google"]["Valid"]){
	//prepare password
	include_once($PATH_WRT_ROOT."includes/libpwm.php");
	$libpwm = new libpwm();
	
	$password_for_google_account_creation='';
	$rows_password = $libpwm->getData(array($uid));
	foreach((array)$rows_password as $key=>$row_password){
		$password_for_google_account_creation = $row_password;
	}
	
	//google api
	include_once($PATH_WRT_ROOT."includes/google_api/libgoogleapi.php");
	include_once($PATH_WRT_ROOT."includes/sso/libSSO_db.php");
	include_once($PATH_WRT_ROOT."includes/sso/libGoogleSSO.php");
	$libGoogleSSO = new libGoogleSSO();
			
	$gmail_input_array = array();
	$array_config_index = $libGoogleSSO->getAllGoogleAPIConfigIndex();
	foreach((array)$array_config_index as $config_index){
		$gmail_array_index = 'gmail_' . $config_index;
		$gmail_input_array[$gmail_array_index] = $$gmail_array_index;
	}
	
	$google_have_error=false;
	if ($ssoservice["Google"]['service_directory']['user']['is_readonly'] == false) {
    	$array_error_message = $libGoogleSSO->syncUserFromEClassToGoogle($uid, $userlogin, $status, $engname, $password_for_google_account_creation, $gmail_input_array, $chiname);
    	
    	foreach((array)$array_error_message as $config_index => $error_message){
    		if($error_message==''){
    			if($google_account_exists_during_creation){
    				$_SESSION["SSO"]["Google"]["Message"]['Config'][$config_index] = "ACCOUNT_CREATED";
    			}else{
    				$_SESSION["SSO"]["Google"]["Message"]['Config'][$config_index] = "NO_MESSAGE";
    			}
    		}else{
    			$google_have_error = true;
    			$_SESSION["SSO"]["Google"]["Message"]['Config'][$config_index] = $error_message;
    		}
    	}
	}
	$user_groups = $libGoogleSSO->enabledUserForGoogle($laccount, array($uid));
	if (count($user_groups['activeUsers'])>0) {
	    $year_id = Get_Current_Academic_Year_ID();
	    $sql_query_one = "SELECT GroupID FROM INTRANET_GROUP WHERE (AcademicYearID = ".$year_id." OR AcademicYearID is NULL)";
	    $all_group_ids = $laccount->returnVector($sql_query_one);
	    if (count($all_group_ids)>0) {
	        $sql_query = "SELECT GroupID FROM INTRANET_USERGROUP WHERE UserID='".$uid."' AND GroupID IN (".implode(',', $all_group_ids).")";
	        $group_ids = $laccount->returnVector($sql_query);
	        if ($ssoservice["Google"]['service_directory']['group']['is_readonly'] == false) {
	           $libGoogleSSO->syncGroupForUserFromEClassToGoogle($laccount, $userlogin, $gmail_input_array, $group_ids);
	        }
	    }
	}
}

/*
 		if ($teaching == 1) {   # Teaching staff -> Teacher group
		    $target_idgroup = 1;
		    $fieldname .= ",Teaching = ".TEACHING;
		} else {                   # Non-teaching staff -> Admin staff group
		    $target_idgroup = 3;
		    $fieldname .= ",Teaching = ".NONTEACHING;
		}
*/
if($skipStep2 != 1) {
	# Assign to Group
	# Hide ECA Groups if eEnrolment is updated to use Year-based Term-based Club
	include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
	$libenroll = new libclubsenrol();
	$cond_ECA_GroupID = "";
	if ($plugin['eEnrollment'] && $libenroll->isUsingYearTermBased)
	{
		$sql = "SELECT GroupID FROM INTRANET_GROUP WHERE RecordType = '5'";
		$ECA_GroupIDArr = $libenroll->returnVector($sql);
		
		$cond_ECA_GroupID = '';
		if (is_array($ECA_GroupIDArr) && count($ECA_GroupIDArr) > 0)
		{
			$ECA_GroupIDList = implode(',', $ECA_GroupIDArr);
			$cond_ECA_GroupID = " AND GroupID NOT IN (".$ECA_GroupIDList.") ";
		}
	}
		
	# remove existing group
	$gpID = ($teaching==1) ? 1 : 3;
		
	if($ssoservice["Google"]["Valid"]){
		$sql = "SELECT GroupID FROM INTRANET_USERGROUP WHERE UserID='$uid' AND GroupID!=$gpID AND GroupID NOT IN (1,2,3,4)";
		$rows_group_original = $laccount->returnArray($sql);
		
		$array_original_group_id = array();
		foreach((array)$rows_group_original as $row_group_original){
			array_push($array_original_group_id,$row_group_original['GroupID']);
		}
	}
	$sql = "DELETE FROM INTRANET_USERGROUP WHERE UserID='$uid' AND GroupID!=$gpID AND GroupID NOT IN (1,2,3,4)";
	$sql .= $cond_ECA_GroupID;
	$laccount->db_db_query($sql);
	
	# add group member
	for($i=0; $i<sizeof($GroupID); $i++){
		if($GroupID[$i] != "") {
			$lgroup = new libgroup($GroupID[$i]);
			$defaultRoleID = $lgroup->returnGroupDefaultRole($GroupID[$i]);
			$sql = "INSERT INTO INTRANET_USERGROUP (GroupID, UserID, RoleID, DateInput, DateModified) VALUES ('".$GroupID[$i]."', '$uid', '$defaultRoleID', now(), now())";
			$laccount->db_db_query($sql);
		}
	}
	
	# Assign to Role
	# remove existing role
	$sql = "DELETE FROM ROLE_MEMBER WHERE UserID='$uid'";
	$laccount->db_db_query($sql);

	# add role
	$identityType = ($teaching==0) ? "NonTeaching" : "Teaching";
	for($i=0; $i<sizeof($RoleID); $i++){
		if($RoleID[$i] != "" && $RoleID[$i]!=0) {
			$RoleID[$i] = IntegerSafe($RoleID[$i]);
			$sql = "INSERT INTO ROLE_MEMBER (RoleID, UserID, DateInput, InputBy, DateModified, ModifyBy, IdentityType) VALUES ('".$RoleID[$i]."', '$uid', now(), '$UserID', now(), '$UserID', '$identityType')";
			$laccount->db_db_query($sql);
		}
	}
	
	if($ssoservice["Google"]["Valid"] && $ssoservice["Google"]['mode']['user_and_group']){
		include_once($PATH_WRT_ROOT."includes/google_api/libgoogleapi.php");
		include_once($PATH_WRT_ROOT."includes/sso/libSSO_db.php");
		include_once($PATH_WRT_ROOT."includes/sso/libGoogleSSO.php");
		$libGoogleSSO = new libGoogleSSO();
		if ($ssoservice["Google"]["Valid"] && $ssoservice["Google"]['mode']['user_and_group'] && $ssoservice["Google"]['service_directory']['group']['is_readonly'] == false ) {
		    $libGoogleSSO->syncGroupForUserFromEClassToGoogle($laccount, $userlogin, $gmail_input_array, $GroupID, $array_original_group_id);
		}
	}
}

#### Special handling
//$lireg = new libregistry();
$laccount->UpdateRole_UserGroup();

# icalendar
	//$GroupID = empty($GroupID)?Array():$GroupID;
	
	$sql = "select GroupID from INTRANET_USERGROUP where UserID = '$uid' ";
	$all_groups = $lu->returnVector($sql);
	
	$current_group_to_skip = implode(",",$all_groups);
	$current_group_to_skip .= ((trim($current_group_to_skip)=="") ? "" : "," ) . "2, 4";	#2 and 4 for student and parent groups respectively
	if (trim($current_group_to_skip)=="")
	{
		$current_group_to_skip = 0;
	}
	
	# no need to remove user from student and parent groups
	$sql = "delete from CALENDAR_CALENDAR_VIEWER where 
		GroupID not in ({$current_group_to_skip}) and GroupType = 'E' and UserID = '$uid'";
	$lu->db_db_query($sql);
	
	$sql = "select GroupID from CALENDAR_CALENDAR_VIEWER where GroupType = 'E' and UserID = '$uid'";
	$existing = $lu->returnVector($sql);
	$existing = empty($existing)? array():$existing;
	
	
	//$newGroup = array_diff($GroupID,$existing);
	//if (!empty($existing)){
		$sql = "insert into CALENDAR_CALENDAR_VIEWER 
				(CalID, UserID, GroupID, GroupType, Access, Color, Visible)
				select g.CalID, '$uid', g.GroupID, 'E', 'R', '2f75e9', '1' 
				from INTRANET_GROUP as g inner join INTRANET_USERGROUP as u 
				on g.GroupID = u.GroupID and u.UserID = '$uid'
				where g.GroupID not in ('".implode("','",$existing)."')";
		$lu->db_db_query($sql);
	//}
	
	# remove shared group calendars 
	$cal_remove_groups = array_values(array_diff($existing_groups,$all_groups));
	for($i=0;$i<sizeof($cal_remove_groups);$i++) {
		$lcalendar->removeCalendarViewerFromGroup($cal_remove_groups[$i],$uid);
	}
	# insert calendar viewer to calendars that have shared to the user's groups
	for($i=0;$i<sizeof($all_groups);$i++) {
		$lcalendar->addCalendarViewerToGroup($all_groups[$i],$uid);
	}
	
# no longer use this function
//$le->addTitleToCourseUser($lu->UserEmail,$dataAry['TitleEnglish'],$dataAry['TitleChinese'],$dataAry['EnglishName'],$dataAry['ChineseName'],false,$dataAry['HomeTelNo'],$dataAry['FaxNo'],$dataAry['Address']);

$le->eClass40UserUpdateInfo($ori_UserEmail, $dataAry, $new_UserEmail);
// $le->eClass40UserUpdateInfo($ori_UserEmail,$dataAry);


$successAry['synUserDataToModules'] = $lu->synUserDataToModules($uid);

$laccount->suspendEmailFunctions($uid, $status == 1);

// SIS Create account to PMS 
if($sys_custom['SISPMSRegistry']){
		include_once($PATH_WRT_ROOT."includes/cust/libregistrySIS_PMS.php");
		$liPMS = new libregistryPMS();
		$Par = array(
					 $engname,$intranet_password_salt,$UserPassword,$chiname,$nickname
					,$Title,$gender,$homePhone,$officePhone
					,$mobilePhone,$FaxNo,$address
					,$remark,$status,TYPE_TEACHER,$engTitle,$chiTitle
					,$TitleEnglish,$TitleChinese,$uid
					);
		$success = $liPMS->UpdateUser($userInfo,$modifyPassword,$Par);
}	

if($plugin['radius_server']){
	$laccount->disableUserInRadiusServer(array($uid), !$enable_wifi_access);
}

if($plugin['SDAS_module']['KISMode']){
	// Delete Exist Additional Info
	$conds = "UserID = '$uid' AND InfoField IN ('Position','ContractType','ContractStartDate','ContractEndDate')";
	$sql = "DELETE FROM INTRANET_USER_ADDITIONAL_INFO WHERE $conds";
	$lu->db_db_query($sql);
	
	$cols = "UserID, InfoField, InfoValue, DateInput, InputBy, DateModified, ModifiedBy";
	$vals = "";
	$vals .= "('$uid','Position','$position', NOW(), '{$_SESSION['UserID']}', NOW(), '{$_SESSION['UserID']}'),";
	$vals .= "('$uid','ContractType','$contractType', NOW(), '{$_SESSION['UserID']}', NOW(), '{$_SESSION['UserID']}'),";
	$vals .= "('$uid','ContractStartDate','$contractStartDate', NOW(), '{$_SESSION['UserID']}', NOW(), '{$_SESSION['UserID']}'),";
	$vals .= "('$uid','ContractEndDate','$contractEndDate', NOW(), '{$_SESSION['UserID']}', NOW(), '{$_SESSION['UserID']}')";
	$sql=" INSERT INTO INTRANET_USER_ADDITIONAL_INFO ($cols) VALUES $vals";
	$lu->db_db_query($sql);
}

if($success) 
	$xmsg = "UpdateSuccess";
else 
	$xmsg = "UpdateUnsuccess";
	

intranet_closedb();
// header("Location: index.php?targetClass=$targetClass&recordstatus=$recordstatus&keyword=$keyword&xmsg=$flag");
if($ssoservice["Google"]["Valid"]){
	if($google_have_error){
		header("Location: edit.php?uid=$uid");
		exit();
	}
}
header("Location: index.php?xmsg=$xmsg");	
?>