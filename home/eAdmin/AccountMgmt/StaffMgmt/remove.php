<?php
// Modifng by : 
/*
 * Modification:
 * Date: 2019-08-08 Chris
 *  - Updated SSO implementation
 * Date: 2019-05-13 Henry
 *  - Security fix: SQL without quote
 * Date: 2014-10-16 Ryan
 *  - delete account From PMS for SIS Flag : $sys_custom['SISPMSRegistry']
 * Date: 2013-02-08	YatWoon
 *  - use "user_id[]" instead of userID[] 
 * Date: 2012-08-01 (Ivan)
 * 	- added logic to syn user info to library system
 */

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."includes/libform.php");
include_once($PATH_WRT_ROOT."includes/libftp.php");
include_once($PATH_WRT_ROOT."includes/libldap.php");
include_once($PATH_WRT_ROOT."includes/libstudentpromotion.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");

if(!($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Staff"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"])) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

if(sizeof($_POST)==0) {
	header("Location: index.php");	
	exit;
}

intranet_opendb();
//$userID = array(1760,1761,1965,2025);
$list = implode(",", $user_id);

$isKIS = $_SESSION["platform"]=="KIS" && $sys_custom['ePayment']['KIS_NoBalance'];

$lu = new libuser();

#### Check any account has non-zero balance
$sql="SELECT COUNT(*) FROM PAYMENT_ACCOUNT AS a LEFT OUTER JOIN PAYMENT_PAYMENT_ITEMSTUDENT AS b ON (a.StudentID = b.StudentID) WHERE a.StudentID IN ('".implode("','", $user_id)."') AND (a.Balance >0 OR b.RecordStatus=0)";
$temp = $lu->returnVector($sql);

if (!$isKIS && $temp[0]>0)
{
    # Stop this action
    include_once($PATH_WRT_ROOT."includes/libaccount.php");
    include_once($PATH_WRT_ROOT."includes/libinterface.php");
    include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
    
    $linterface = new interface_html();
    
	$CurrentPageArr['StaffMgmt'] = 1;
	//$CurrentPage = "User List";
	
	$TAGS_OBJ[] = array($Lang['Group']['UserList']);
	$MODULE_OBJ['title'] = $Lang['Header']['Menu']['StaffAccount'];
	
	# navigation bar
	$PAGE_NAVIGATION[] = array($Lang['AccountMgmt']['UserList'], "javascript:goBack()");
	$PAGE_NAVIGATION[] = array($Lang['AccountMgmt']['DeleteUser'], "");

	$linterface->LAYOUT_START();
	
    $langfield = getNameFieldByLang("b.");
    $sql = "SELECT 
    			b.UserLogin, 
    			$langfield, 
    			b.ClassName, 
    			b.ClassNumber, 
    			IF(a.Balance>0,CONCAT('<font color=red>$', FORMAT(a.Balance, 1),'</font>'),CONCAT('$', FORMAT(a.Balance, 1))) AS Amount,
    			c.PaymentID,
    			d.Name,
    			CONCAT('$',FORMAT(c.Amount,1))
			FROM PAYMENT_ACCOUNT as a LEFT OUTER JOIN INTRANET_USER as b ON a.StudentID = b.UserID LEFT OUTER JOIN 
				PAYMENT_PAYMENT_ITEMSTUDENT as c ON (a.StudentID = c.StudentID) LEFT OUTER JOIN 
				PAYMENT_PAYMENT_ITEM as d ON (c.ItemID = d.ItemID)
			WHERE 
				a.StudentID IN ('".implode("','", $user_id)."') AND 
				(a.Balance >0 OR c.RecordStatus=0) 
			ORDER BY 
				a.StudentID";
             
    $result = $lu->returnArray($sql,8);
    
    $result2 = array();
    for ($i=0; $i<sizeof($result); $i++)
    {
         list($t_userLogin,$t_name, $t_className, $t_classNum, $t_balance, $payment_id, $item_name, $unpaid_amount) = $result[$i];
	     $result2[$t_userLogin]['name'] = $t_name;
	     $result2[$t_userLogin]['class'] = $t_className;
	     $result2[$t_userLogin]['classnumber'] = $t_classNum;
	     $result2[$t_userLogin]['balance'] = $t_balance;
	     if($payment_id != ""){
	     	$result2[$t_userLogin]['items'][] = $item_name." (".$unpaid_amount.")";
	     }
    }
    
    $content = "";

    $j=0;
    foreach($result2 as $user_login => $values){
	    $name = $values['name'];
	    $class = $values['class'];
	    $classnumber = $values['classnumber'];
	    $balance = $values['balance'];
	    
	    if(is_array($values['items'])){
		    $items = "<table border=0>";
		  	for($x=0; $x<sizeof($values['items']); $x++){
	    		$items .= "<tr><td><font color=red>-</font></td><td><font color=red>".$values['items'][$x]."</font></td></tr>";
	    	}
	    	$items .= "</table>";
		}
	    
	    $j++;
	    $css =$css = ($j%2 ? "1":"2");
        $content .= "<tr class=tablerow$css><td>$user_login</td><td>$name</td><td>$class&nbsp;</td><td>$classnumber</td><td>$balance</td><td>$items&nbsp;</td></tr>\n";

	}
	
    ?>
    <form name="form1" method="post" action="index.php">
    <table align="left" width="95%" border="0" cellpadding="4" cellspacing="0"><tr><td height="40"><?=$linterface->GET_NAVIGATION($PAGE_NAVIGATION)?></td></tr></table>
    <p>&nbsp;</p><p>&nbsp;</p>
		<span class="extraInfo"><span class="subTitle"><?=$i_UserRemoveStop_PaymentBalanceNonZero?></span></span>
			<p>
			<table width="90%" border="0" cellpadding="4" cellspacing="0">
				<tr class="tableTitle" bgcolor="#CCCCCC"><td><?=$i_UserLogin?></td><td><?=$i_UserName?></td><td><?=$i_UserClassName?></td><td><?=$i_UserClassNumber?></td><td><?=$i_Payment_Field_Balance?></td><td><?="$i_Payment_Field_PaymentItem ($i_Payment_Field_Amount)"?></td></tr>
				<?=$content?>
			</table>
		<br><br>
		<?= $linterface->GET_ACTION_BTN($button_back, "button", "goBack()")?>
	<input type="hidden" name="TeachingType" id="TeachingType" value="<?=$TeachingType?>">
	<input type="hidden" name="recordstatus" id="recordstatus" value="<?=$recordstatus?>">
	<input type="hidden" name="keyword" id="keyword" value="<?=$keyword?>">
	</form>
	<script language="javascript">
	<!--
		function goBack() {
			document.form1.submit();	
		}
	-->
	</script>

    <?
    intranet_closedb();
    $linterface->LAYOUT_STOP();

    exit();

}

# SchoolNet integration
if ($plugin['SchoolNet'])
{
    $sql = "SELECT UserLogin FROM INTRANET_USER WHERE UserID IN ('".implode("','", $user_id)."')";
    $schnet_target = $lu->returnVector($sql);
    include_once($PATH_WRT_ROOT."includes/libschoolnet.php");
    $lschoolnet = new libschoolnet();
    $lschoolnet->removeUser($schnet_target);
}

$lsp = new libstudentpromotion();
$sql = "SELECT UserID FROM INTRANET_USER WHERE RecordType=".TYPE_TEACHER." AND UserID IN ('".implode("','", $user_id)."')";
$others = $lu->returnVector($sql);

if (sizeof($others)!=0)
{
    $otherList = implode(",",$others);
    $lsp->archiveIntranetUser($otherList);
}

if($ssoservice["Google"]["Valid"]){
    include_once($PATH_WRT_ROOT."includes/google_api/libgoogleapi.php");
    include_once($PATH_WRT_ROOT."includes/sso/libSSO_db.php");
    include_once($PATH_WRT_ROOT."includes/sso/libGoogleSSO.php");
    $libGoogleSSO = new libGoogleSSO();
    $user_list = $libGoogleSSO->enabledUserForGoogle($lu, $user_id);
    if (count($user_list['activeUsers'])>0 && count($user_list['suspendedUsers'])>0) {
        $users = array_merge($user_list['activeUsers'],$user_list['suspendedUsers']);
    }elseif (count($user_list['activeUsers'])>0) {
        $users = $user_list['activeUsers'];
    }elseif (count($user_list['suspendedUsers'])>0) {
        $users = $user_list['suspendedUsers'];
    }else {
        $users = array();
    }
    $userGroupIDArr = array();
    foreach ($users as $value) {
        $sql2 = 'SELECT `UserLogin` FROM `INTRANET_USER` WHERE `UserID`=\'' . $value. '\';';
        $userlogin = current($lu->returnVector($sql2));
        $sql = "SELECT GroupID FROM INTRANET_USERGROUP WHERE UserID = '".$value."'";
        $userGroupIDArr[$userlogin] = $lu->returnVector($sql);
    }
}	
$lu->prepareUserRemoval($list);
$lu->removeUsers($list);

# delete calendar viewer
$sql = "delete from CALENDAR_CALENDAR_VIEWER where UserID in ('".implode("','", $user_id)."')";
$lu->db_db_query($sql);


$successAry['synUserDataToModules'] = $lu->synUserDataToModules($user_id);

# SIS Cust 
if($sys_custom['SISPMSRegistry']){
	include_once($PATH_WRT_ROOT."includes/cust/libregistrySIS_PMS.php");
	$liPMS = new libregistryPMS();
	$liPMS->RemoveUser($list);
}


# GSuite
//error_reporting(E_ALL);
//ini_set('display_errors', 1);
if($ssoservice["Google"]["Valid"]){
	include_once($PATH_WRT_ROOT."includes/google_api/libgoogleapi.php");
	include_once($PATH_WRT_ROOT."includes/sso/libSSO_db.php");
	include_once($PATH_WRT_ROOT."includes/sso/libGoogleSSO.php");
	$libGoogleSSO = new libGoogleSSO();
	
	$libGoogleSSO->removeUserFromEClassToGoogle($user_id);
	foreach ($userGroupIDArr as $key => $value) {
	    foreach ($value as $GroupID) {
	        $libGoogleSSO->removeGroupMemberFromEClassToGoogle($key, $GroupID);
	    }
	}
}	

//exit;
intranet_closedb();
header("Location: index.php?xmsg=DeleteSuccess");
?>
