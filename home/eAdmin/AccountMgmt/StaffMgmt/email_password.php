<?php
# using: 

########## Change Log [Start] ############
#
#
########## Change Log [End] ############

@SET_TIME_LIMIT(600);
$PATH_WRT_ROOT = "../../../../";
include($PATH_WRT_ROOT."includes/global.php");
include($PATH_WRT_ROOT."includes/libdb.php");
include($PATH_WRT_ROOT."includes/libemail.php");
include($PATH_WRT_ROOT."includes/libaccountmgmt.php");
include($PATH_WRT_ROOT."includes/libsendmail.php");
include($PATH_WRT_ROOT."lang/email.php");

intranet_opendb();

$li = new libdb();

if(!$_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"]) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

if(sizeof($_POST))
	$userList = implode(',', $userID);

$conds = " AND RecordType = ".TYPE_TEACHER;

$sql = "SELECT 
			UserID, UserLogin, UserPassword, UserEmail, EnglishName, Title 
		FROM 
			INTRANET_USER
		WHERE
			UserID IN ($userList)
		";
$row = $li->returnArray($sql, 6);

for($i=0; $i<sizeof($row); $i++){
     $uid = $row[$i][0];
     $UserLogin = $row[$i][1];
     $UserPassword = $row[$i][2];
     $UserEmail = $row[$i][3];
     $EnglishName = $row[$i][4];
     $title = $row[$i][5];
     
     switch ($title)
     {
	     case 0:	$title_name = $i_title_mr;		break;
	     case 1:	$title_name = $i_title_miss;	break;
	     case 2:	$title_name = $i_title_mrs;		break;
	     case 3:	$title_name = $i_title_ms;		break;
	     case 4:	$title_name = $i_title_dr;		break;
	     case 5:	$title_name = $i_title_prof;	break;
	     default:	$title_name = ""; 				break;
     }
     
     # Call sendmail function
     $mailSubject = registration_title();
     $mailBody = registration_body($UserEmail, $UserLogin, $UserPassword, $title_name, $EnglishName);
     $mailTo = $UserEmail;
     $lu = new libsendmail();
     $headers = "From: $webmaster\r\n";
     $lu->SendMail( $mailTo, $mailSubject, $mailBody,"$headers");
}

intranet_closedb();

header("Location: index.php?TeachingType=$TeachingType&recordstatus=$recordstatus&keyword=$keyword&xmsg=email");
?>