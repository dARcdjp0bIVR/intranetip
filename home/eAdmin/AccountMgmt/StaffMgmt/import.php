<?php
// Modifying by:

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");

intranet_auth();
intranet_opendb();

if(!$_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"]) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$laccount = new libaccountmgmt();

$linterface = new interface_html();

$CurrentPageArr['StaffMgmt'] = 1;
//$CurrentPage = "User List";

$TAGS_OBJ[] = array($Lang['Group']['UserList']);
$MODULE_OBJ['title'] = $Lang['Header']['Menu']['StaffAccount'];

# step information
$STEPS_OBJ[] = array($i_general_select_csv_file, 1);
$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 0);
$STEPS_OBJ[] = array($i_general_imported_result, 0);


$sample_file = "staff_account_sample_unicode.csv";
$csvFile = "<a class=\"tablelink\" href=\"get_sample_file.php?file=$sample_file\" target=\"_self\">".$Lang['General']['ClickHereToDownloadSample']."</a>";

$csv_format = "";
$delim = "<br>";
for($i=0; $i<sizeof($Lang['AccountMgmt']['StaffImport']); $i++){
	if($i!=0) $csv_format .= $delim;
	$csv_format .= $Lang['SysMgr']['Homework']['Column']." ".($i+1)." : ".$Lang['AccountMgmt']['StaffImport'][$i];
}
/*
$reference_str = "<div id=\"ref_list\" style='position:absolute; height:100px; z-index:1; visibility: none;'></div>";
$reference_str .= "<a class=\"tablelink\" href=javascript:show_ref_list('identity','id_click')>".$i_identity."</a><span id=\"id_click\">&nbsp;</span>,
				<a class=\"tablelink\" href=javascript:show_ref_list('title','ti_click')>".$i_UserTitle."</a><span id=\"ti_click\">&nbsp;</span>,
				<a class=\"tablelink\" href=javascript:show_ref_list('country','co_click')>".$i_UserCountry."</a><span id=\"co_click\">&nbsp;</span>,
				<a class=\"tablelink\" href=javascript:show_ref_list('group','gp_click')>".$i_StaffAttendance_GroupID."</a><span id=\"gp_click\">&nbsp;</span>,
				<a class=\"tablelink\" href=javascript:show_ref_list('role','ro_click')>".$Lang['AccountMgmt']['RoleID']."</a><span id=\"ro_click\">&nbsp;</span>
				</p>";	
*/

$linterface->LAYOUT_START();
?>
<script language="JavaScript">
var ClickID = '';
var callback_show_ref_list = {
	success: function ( o )
    {
	    var tmp_str = o.responseText;
	    document.getElementById('ref_list').innerHTML = tmp_str;
	    DisplayPosition();
	}
}


function show_ref_list(type,click)
{
	ClickID = click;
	document.getElementById('task').value = type;
	obj = document.form1;
	YAHOO.util.Connect.setForm(obj);
	var path = "ajax_ref.php";
    var connectionObject = YAHOO.util.Connect.asyncRequest("POST", path, callback_show_ref_list);
}

var callback_list = {
	success: function ( o )
    {
	    var tmp_str = o.responseText;
	    document.getElementById('ref_list').innerHTML = tmp_str;
	    DisplayPosition();
	}
}

function show_type_list(type,click)
{
	ClickID = click;
	document.getElementById('task').value = type;
	obj = document.form1;
	YAHOO.util.Connect.setForm(obj);
	var path = "ajax_ref.php";
    var connectionObject = YAHOO.util.Connect.asyncRequest("POST", path, callback_list);
}

function Hide_Window(pos){
  document.getElementById(pos).style.visibility='hidden';
}

function getPosition(obj, direction)
{
	var objStr = "obj";
	var pos_value = 0;
	while (typeof(eval(objStr))!="undefined" && eval(objStr + ".tagName")!="BODY")
	{
		pos_value += eval(objStr + "." + direction);
		objStr += ".offsetParent";
	}

	return pos_value;
}

function DisplayPosition(){
	
  document.getElementById('ref_list').style.left = getPosition(document.getElementById(ClickID),'offsetLeft') +10;
  document.getElementById('ref_list').style.top = getPosition(document.getElementById(ClickID),'offsetTop');
  document.getElementById('ref_list').style.visibility = 'visible';
}

function goBack() {
	document.form1.action = "index.php";
	document.form1.submit();
}
</script>
<br />
<form name="form1" method="post" action="import_result.php" enctype="multipart/form-data">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="right" colspan="2"><?=$linterface->GET_SYS_MSG($xmsg, $xmsg2);?></td>
	</tr>
	<tr>
		<td><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
	</tr>
	<tr>
		<td colspan="2">
			<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr>
					<td class="formfieldtitle" align="left" width="30%"><?=$iDiscipline['Import_Source_File']?>: </td>
					<td class="tabletext" width="70%">
						<input class="file" type="file" name="csvfile">
					</td>
				</tr>
				<tr>
					<td class="formfieldtitle" align="left"><?=$Lang['General']['CSVSample']?></td>
					<td class="tabletext"><?=$csvFile?></td>
				</tr>
				<tr>
					<td class="formfieldtitle" align="left"><?=$Lang['SysMgr']['Homework']['Import']['FieldTitle']['DataColumn']?></td>
					<td class="tabletext"><?=$csv_format?></td>
				</tr>
				<!--
				<tr>
					<td class="formfieldtitle" align="left" height="30"><?=$Lang['SysMgr']['Homework']['Import']['FieldTitle']['Reference']?></td>
					<td class="tabletext"><?=$reference_str?></td>
				</tr>
				-->
				<tr>
					<td class="tabletextremark" colspan="2"><?=$Lang['SysMgr']['Homework']['Import']['FieldTitle']['RequiredField']?></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<table width="95%" border="0" cellpadding="0" cellspacing="0" align="center">
				<tr>
					<td colspan="3" class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td align="center" colspan="2">
			<?= $linterface->GET_ACTION_BTN($button_continue, "submit") ?>&nbsp;
			<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "goBack()") ?>
		</td>
	</tr>
</table>
<input type="hidden" id="task" name="task"/>
<input type="hidden" id="TeachingType" name="TeachingType" value="<?=$TeachingType?>"/>
<input type="hidden" id="recordstatus" name="recordstatus" value="<?=$recordstatus?>"/>
<input type="hidden" id="keyword" name="keyword" value="<?=$keyword?>"/>
</form>
<br />
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
