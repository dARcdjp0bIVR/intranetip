<?php
//
/**
 * Log
 * - 2010-06-08 (Sandy)
 *   update db after deleteing physical file.
 */
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

intranet_auth();
intranet_opendb();

$laccount = new libaccountmgmt();
$lfs = new libfilesystem();
$li = new libdb();

foreach((array) $UserLogin as $thisUserLogin)
{
	$img_path = $PATH_WRT_ROOT."/file/signature/$thisUserLogin.jpg";
	## Unlink file
	$is_succ = $lfs->file_remove($img_path);
	
	if($is_succ == true || $is_succ == 1)
		$SuccUserLoginArr[] = $thisUserLogin;

}

$result = $laccount->deleteSignatureLink($SuccUserLoginArr);

$msg = $result?"DeleteSuccess":"DeleteUnSuccess";
header("location: photo_list.php?msg=$msg");
?>