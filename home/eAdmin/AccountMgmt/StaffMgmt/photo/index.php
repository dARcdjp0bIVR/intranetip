<?php
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if(!($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Staff"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"])) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}
 
$laccount = new libaccountmgmt();
$linterface = new interface_html();

# upload btn
$toolbar .= $linterface->GET_LNK_UPLOAD("upload.php");

# get photo mgmt table
$photoMgmtTable = $laccount->getPhotoMgmtIndexTable();

# page info
$CurrentPageArr['StudentMgmt'] = 1;
$CurrentPage = "Mgmt_Photo";

$PAGE_NAVIGATION[] = array($Lang['AccountMgmt']['ManagePhoto'], "");
$PageNavigation = $linterface->GET_NAVIGATION($PAGE_NAVIGATION);

$TAGS_OBJ[] = array($Lang['Personal']['OfficialPhoto']);
$MODULE_OBJ = $laccount->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();

?>
<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
			<form name="form2" method="post" onSubmit="return false">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td>
							<?=$PageNavigation?>
							<br><br>
							<table border="0" cellspacing="0" cellpadding="2">
								<tr>
									<td><?=$toolbar?></td>
								</tr>
							</table>
						</td>
						<td align="right"></td>
					</tr>
				</table>
			</form>
			<form name="form1" method="get" action="">
			<?=$photoMgmtTable?>
			<br><br>
			</form>
		</td>
	</tr>
</table>

<?			
		
$linterface->LAYOUT_STOP();		
?>