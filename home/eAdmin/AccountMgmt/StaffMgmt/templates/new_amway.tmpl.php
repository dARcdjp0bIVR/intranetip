<?php
	// Using:
?>	
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr><td height="40"><?=$linterface->GET_NAVIGATION($PAGE_NAVIGATION)?></td></tr>
    <tr><td height="40"><?=$linterface->GET_STEPS($STEPS_OBJ)?></td></tr>
    <tr><td class="board_menu_closed">
		  
		  <table width="99%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td class="main_content">
                
                      <!-- ******************************************* -->
                      <div class="table_board">

						<table class="form_table" width="90%" cellpadding="4" cellspacing="0" border="0">
						  <tr>
						    <td colspan="2"><em class="form_sep_title"> - <?=$Lang['AccountMgmt']['SystemInfo']?> -</em></td>
						    </tr>
						 
						  <tr>
						    <td class="formfieldtitle" width="20%"><span class="tabletextrequire">*</span><?=$i_UserLogin?></td>
						    <td ><input name="userlogin" type="text" id="userlogin" class="textboxnum" value="<?=$userlogin?>" maxlength="20" onKeyUp="updateIMapEmail(this);"/>
						    		<?/* if(sizeof($errorAry)>0 && in_array($userlogin, $errorAry)) echo "<font color='#FF0000'>".$i_UserLogin.$Lang['AccountMgmt']['UsedByOtherUser']."</font>"; */?>
						    		<span id="spanChecking"><? if(sizeof($errorAry)>0 && in_array($userlogin, $errorAry)) echo " <font color='#FF0000'>".$i_UserLogin.$Lang['AccountMgmt']['UsedByOtherUser']."</font>"; ?><input type="hidden" name="available" id="available" value="1"></span>
						    		<span id="spanCheckingImage" style="display:none;"><?=$linterface->Get_Ajax_Loading_Image()?></span>
						    </td>
						  </tr>
						  <tr>
						    <td class="formfieldtitle" rowspan="3"><span class="tabletextrequire">*</span><?=$i_UserPassword?></td>
						    <td><input name="pwd" type="password" id="pwd" class="textboxnum" value="<?=$pwd?>" maxlength="20"/></td>
						  </tr>
						  <tr>
						    <td><input name="pwd2" type="password" id="pwd2" class="textboxnum" value="<?=$pwd?>" maxlength="20"/> 
						      (<?=$Lang['AccountMgmt']['Retype']?>)        </td>
						  </tr>
						  <tr>
							<td class="tabletextremark"><?=$Lang['AccountMgmt']['PasswordRemark']?></td>
						  </tr>
						  <tr>
						    <td class="formfieldtitle"><!--<span class="tabletextrequire">*</span>--><?=$Lang['Gamma']['UserEmail']?></td>
						    <td>
						    	<input name="email" type="text" id="email" class="textbox_name" value="<?=$email?>" maxlength="100"/>
						    	<? if(sizeof($errorAry)>0 && in_array($email, $errorAry)) echo "<font color='#FF0000'>".$i_UserEmail.$Lang['AccountMgmt']['UsedByOtherUser']."</font>"; ?>
						    	<span class="tabletextremark"><?=$Lang['AccountMgmt']['EmailFieldReminder']?></span>
						    </td>
						  </tr>
  							<?if($plugin['imail_gamma']) {
									include_once($PATH_WRT_ROOT."includes/imap_gamma.php");
									$IMap = new imap_gamma($skipLogin = 1);
									if($IMap->CheckIdentityPermission(TYPE_TEACHER) )
									{	
										if($sys_custom['iMailPlus']['EmailAliasName'])
										{
											$btnsImapUserLogin = '<input name="ImapUserLogin" type="text" id="ImapUserLogin" class="textbox_name" value="'.$ImapUserLogin.'" maxlength="100" '.($UseImapUserLogin==1?'':'style="display:none;"').' /><span id="ImapUserLoginDomain" '.($UseImapUserLogin==1?'':'style="display:none;"').'>@'.$SYS_CONFIG['Mail']['UserNameSubfix'].'</span>';
											$btnsImapUserLogin.= '&nbsp;'.$linterface->GET_SMALL_BTN($Lang['Btn']['Edit'], "button", 'ToggleImapUserLogin(true);', "BtnEditImapUserLogin", ($UseImapUserLogin==1?'style="display:none;"':''), "", "");
											$btnsImapUserLogin.= '&nbsp;'.$linterface->GET_SMALL_BTN($Lang['Btn']['Cancel'], "button", 'ToggleImapUserLogin(false);', "BtnCancelImapUserLogin", ($UseImapUserLogin==1?'':'style="display:none;"'), "", "");
											$btnsImapUserLogin.= '<input type="hidden" id="UseImapUserLogin" name="UseImapUserLogin" value="'.($UseImapUserLogin==1?'1':'0').'" />';
											if(sizeof($errorAry)>0 && in_array($ImapUserLogin."@".$SYS_CONFIG['Mail']['UserNameSubfix'], $errorAry)){
												if(!intranet_validateEmail($ImapUserLogin."@".$SYS_CONFIG['Mail']['UserNameSubfix'])){
													$btnsImapUserLogin .= "<br /><font color='#FF0000'>".$Lang['AccountMgmt']['ErrorMsg']['InvalidEmail'].": ".$ImapUserLogin."@".$SYS_CONFIG['Mail']['UserNameSubfix']."</font>";
												}else if(in_array($ImapUserLogin, (array)$system_reserved_account)){
													$btnsImapUserLogin .= "<br /><font color='#FF0000'>".$Lang['AccountMgmt']['ErrorMsg']['EmailUsed'].": ".$ImapUserLogin."@".$SYS_CONFIG['Mail']['UserNameSubfix']."</font>";
												}else{
													$btnsImapUserLogin .= "<br /><font color='#FF0000'>".$ImapUserLogin."@".$SYS_CONFIG['Mail']['UserNameSubfix'].$Lang['AccountMgmt']['UsedByOtherUser']."</font>";
												}
											}
										}
										?>
										<!-- imail gamma -->	
										<tr>
											<td class="formfieldtitle"><span class="tabletextrequire">*</span><?=$i_UserEmail?></td>
											<td>
												<input name="EmailStatus" type="radio" id="EmailDisable" value="disable" /><label for="EmailDisable"><?=$Lang['General']['Disabled']?></label><br>
												<input name="EmailStatus" type="radio" id="EmailEnable" value="enable" CHECKED/><label for="EmailEnable"><?=$Lang['General']['Enabled']?></label>,&nbsp;<span id="IMapUserEmail" <?=$sys_custom['iMailPlus']['EmailAliasName'] && $UseImapUserLogin==1?'style="display:none"':''?>>@<?=$SYS_CONFIG['Mail']['UserNameSubfix']?></span><?=$sys_custom['iMailPlus']['EmailAliasName']?$btnsImapUserLogin:''?><br>
												<?="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$Lang['Gamma']['Quota']?>:<input name="Quota" type="text" id="Quota" class="textboxnum" maxlength=5 value="<?=$EmailQuota?>"/>MB</div>
											</td>
										</tr>
										<?
									}
								}?>
								<!-- imail -->
						  <tr>
						    <td class="formfieldtitle"><span class="tabletextrequire">*</span><?=$i_general_status?></td>
						    <td>
						    	<input type="radio" name="status" id="status1" value="1" <? if($status=="" || $status=="1") echo "checked";?>><label for="status1"><?=$Lang['Status']['Activate']?></label>
						    	<input type="radio" name="status" id="status0" value="0" <? if($status=="0") echo "checked";?>><label for="status0"><?=$Lang['Status']['Suspend']?></label>
						    </td>
						  </tr>
						  
						  <tr>
						    <td colspan="2"><em class="form_sep_title"> - <?=$Lang['AccountMgmt']['BasicInfo']?> -</em></td>
						    </tr>
						  <tr>
						    <td class="formfieldtitle" rowspan="2"><?=$i_general_name?></td>
						    <td class="sub_row_content"><span class="tabletextrequire">*</span>(<?=$ip20_lang_eng?>)
						      <input name="engname" type="text" id="engname" class="textbox_name" value="<?=$engname?>" maxlength="250"/>
						    </td>
						  </tr>
						  <tr>
						    <td><span class="sub_row_content">(<?=$Lang['AccountMgmt']['Chi']?>)</span>
						      <input name="chiname" type="text" id="chiname" class="textbox_name" value="<?=$chiname?>" maxlength="50"/>
						   </td>
						  </tr>
						  <tr>
						    <td><span class="tabletextrequire">*</span><?=$i_UserGender?></td>
						    <td><input type="radio" name="gender" id="genderM" value="M"  <? if($gender=="M" || $gender=="") echo "checked"; ?>/>
						      <label for="genderM"><?=$i_gender_male?></label>
						      <input type="radio" name="gender" id="genderF" value="F" <? if($gender=="F") echo "checked"; ?>/>
						      <label for="genderF"><?=$i_gender_female?></label></td>
						  </tr>
						  <tr>
						    <td class="formfieldtitle"><?=$i_UserAddress?></td>
						    <td><?=$linterface->GET_TEXTAREA("address", $address);?>
						      <br />
						      <?=$countrySelection?></td>
						  </tr>
						  <tr>
						    <td class="formfieldtitle" rowspan="4"><?=$Lang['AccountMgmt']['Tel']?></td>
						    <td><span class="sub_row_content">(<?=$Lang['AccountMgmt']['Home']?>)</span>
						        <input name="homePhone" type="text" id="homePhone" class="textboxnum" value="<?=$homePhone?>" maxlength="20"/></td>
						  </tr>
						  <tr>
						    <td><span class="sub_row_content">(<?=$Lang['AccountMgmt']['Office']?>)</span>
						        <input name="officePhone" type="text" id="officePhone" class="textboxnum" value="<?=$officePhone?>" maxlength="20"/></td>
						  </tr>
						  <tr>
						    <td><span class="sub_row_content">(<?=$Lang['AccountMgmt']['Mobile']?>)</span>
						        <input name="mobilePhone" type="text" id="mobilePhone" class="textboxnum" value="<?=$mobilePhone?>" maxlength="20" /><? if (isset($plugin['sms']) && $plugin['sms']){ echo $i_UserMobileSMSNotes; }?></td>
						  </tr>
						  <tr>
						    <td><span class="sub_row_content">(<?=$Lang['AccountMgmt']['Fax']?>)</span>
						        <input name="faxPhone" type="text" id="faxPhone" class="textboxnum" value="<?=$faxPhone?>" maxlength="20" /></td>
						  </tr>
						  <!-- Additional Info //-->
								<tr>
									<td colspan="2"><em class="form_sep_title"> - <?=$Lang['AccountMgmt']['AdditionInfo']?> -</em></td>
								</tr>
								<tr>
								<td class="formfieldtitle"><?=$Lang['General']['Remark']?></td>
									<td>
										<?=$linterface->GET_TEXTAREA("remark", $remark);?>
									</td>
								</tr>
								
						  <tr>
						    <td colspan="2"><em class="form_sep_title">- <?=$Lang['AccountMgmt']['InternetUsage']?> -</em></td>
						  </tr>
						  <?if(!$plugin['imail_gamma']) {?>
						    	<? if(isset($plugin['webmail']) && $plugin['webmail'] && in_array(TYPE_TEACHER, $webmail_identity_allowed)) { ?>
								  <tr>
								    <td class="formfieldtitle"><?=$ip20TopMenu['iMail']?></td>
								    <td><input type="checkbox" value="1" name="open_webmail" id="open_webmail" <?if(!isset($open_webmail) || $open_webmail==1) echo "checked";?>/>
								      <label for="open_webmail"><?=$i_Mail_AllowSendReceiveExternalMail?></label></td>
								  </tr>
						  		<? } ?>
						  <? } ?>
						  <? if(isset($plugin['personalfile']) && $plugin['personalfile'] && in_array(TYPE_TEACHER,$personalfile_identity_allowed)) { ?>
							  <tr>
							    <td class="formfieldtitle"><?=$ip20TopMenu['iFolder']?></td>
							    <td><input type="checkbox" value="1" name="open_file" id="open_file" <?if(!isset($open_file) || $open_file==1) echo "checked";?>/>
							      <label for="open_file"><?=$i_Files_OpenAccount?></label></td>
							  </tr>
						  <? } ?>
						</table>
						<p class="spacer"></p>
                      </div>
                      <div class="edit_bottom">
                          <p class="spacer"></p>
                          <?= $linterface->GET_ACTION_BTN($button_continue, "button", "goSubmit('new2.php')")?>
                          <?= $linterface->GET_ACTION_BTN($button_finish, "button", "goSubmit('new_update.php')")?>
                          <?= $linterface->GET_ACTION_BTN($button_cancel, "button", "goURL('index.php')")?>
							<p class="spacer"></p>
                    </div></td>
                </tr>
              </table>
    </td></tr>
</table>