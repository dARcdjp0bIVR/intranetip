<?php
// Using:
/*
 * 2020-11-04 (Philips) default $contractStartDate empty
 * 2020-07-13 (Philips) Added position, contractType, contractStartDate,contractEndDate for CEES KIS
 * 2019-05-13 (Henry) Security fix: SQL without quote
 * 2018-08-21 (Cameron) [ip.2.5.9.10.1]: remove * for password field as it's not mandatory, hide unnecessary fields for HKPF
 * 2018-08-07 (Cameron) [ip.2.5.9.10.1]: add Grade and Duty for HKPF 
 * 2018-05-15 (Isaac):  Added Last Modified username and time.
 * 2018-01-03 (Carlos): [ip.2.5.9.1.1]: Apply new password criteria remark.
 * 2016-11-21 (HenryHM) [ip.2.5.8.1.1]: $ssoservice["Google"]["Valid"] - add GSuite Logic
 * 2016-10-04 (Carlos) [ip.2.5.8.1.1]: $sys_custom['iMail']['UserDisplayOrder'] - added input field DisplayOrder
 * 2016-09-28 (Ivan) [ip.2.5.7.10.1]: Added teaching = 'S' supply teacher logic
 * 2016-06-10 (Cara)  : add creator info
 * 2015-12-30 (Carlos): $plugin['radius_server'] - added Wi-Fi access option.
 */
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr><td height="40"><?=$linterface->GET_NAVIGATION($PAGE_NAVIGATION)?></td></tr>
    <tr><td class="board_menu_closed">
		  
		  <table width="99%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td class="main_content">
                
                      <!-- ******************************************* -->
                      <div class="table_board">

						<table class="form_table" border="0">
						  <tr>
						    <td colspan="3"><em class="form_sep_title"> - <?=$Lang['AccountMgmt']['SystemInfo']?> -</em></td>
						    </tr>
						 
						  <tr>
						    <td class="formfieldtitle" width="20%"><span class="tabletextrequire">*</span><?=$i_UserLogin?></td>
						    <td width="75%"><?=$userInfo['UserLogin']?></td>
						    <td rowspan="7"><img src="<?=$photo_personal_link?>" width="100" height="130" title="<?=$Lang['Personal']['PersonalPhoto']?>" /></td>
						  </tr>
						  <tr>
						    <td class="formfieldtitle" rowspan="3"><?=$i_UserPassword?></td>
						    <td>
						    	<input name="pwd" type="password" id="pwd" class="textboxnum" value="<?=$pwd?>"/>
						    	<? if(sizeof($errorAry)>0 && in_array($pwd, $errorAry)) echo "<font color='#FF0000'>".$Lang["Login"]["password_err"]."</font>"; ?>
						    </td>
						  </tr>
						  <tr>
						    <td><input name="pwd2" type="password" id="pwd2" class="textboxnum" value="<?=$pwd?>"/> 
						      (<?=$Lang['AccountMgmt']['Retype']?>)        </td>
						  </tr>
						  <tr>
							<td class="tabletextremark"><?=$sys_custom['UseStrongPassword']?str_replace("<!--PASSWORD_LENGTH-->",$PasswordLength,implode("<br />\n",$Lang['AccountMgmt']['PasswordRequirements'])):$Lang['AccountMgmt']['PasswordRemark']?></td>
						  </tr>
						  <tr>
						    <td class="formfieldtitle"><span class="tabletextrequire">*</span><?=$i_identity?></td>
						    <td ><input type="radio" value="1" name="teaching" id="teaching1" <? if($teaching==1) echo "checked";?>/>
						      <label for="teaching1"><?=$Lang['Identity']['TeachingStaff']?></label>
						        <input type="radio" value="0" name="teaching" id="teaching0" <? if($teaching==0 || $teaching=="") echo "checked";?>/>
						        <label for="teaching0"><?=$Lang['Identity']['NonTeachingStaff']?></label>
						        <?php if ($plugin['SLRS']) { ?>
							        <input type="radio" value="S" name="teaching" id="teachingS" <? if($teaching=='S') echo "checked"; ?>/>
							        <label for="teachingS"><?=$Lang['Identity']['SupplyTeacher']?></label>
						        <?php } ?>
						        <br />
						        <span class="tabletextremark"><?=$i_teachingDifference?></span></td>
						  </tr>
  							<?if($plugin['imail_gamma']) {
									include_once($PATH_WRT_ROOT."includes/imap_gamma.php");
									$IMap = new imap_gamma($skipLogin = 1);
									if($IMap->CheckIdentityPermission(TYPE_TEACHER) )
									{	
										if($sys_custom['iMailPlus']['EmailAliasName'])
										{
											$btnsImapUserLogin = '<input name="ImapUserLogin" type="text" id="ImapUserLogin" class="textbox_name" value="'.(isset($ImapUserLogin)?$ImapUserLogin:$CurrentImapUserLogin).'" maxlength="100" '.($UseImapUserLogin==1?'':'style="display:none;"').' /><span id="ImapUserLoginDomain" '.($UseImapUserLogin==1?'':'style="display:none;"').'>@'.$SYS_CONFIG['Mail']['UserNameSubfix'].'</span>';
											$btnsImapUserLogin.= '&nbsp;'.$linterface->GET_SMALL_BTN($Lang['Btn']['Edit'], "button", 'ToggleImapUserLogin(true);', "BtnEditImapUserLogin", ($UseImapUserLogin==1?'style="display:none;"':''), "", "");
											$btnsImapUserLogin.= '&nbsp;'.$linterface->GET_SMALL_BTN($Lang['Btn']['Cancel'], "button", 'ToggleImapUserLogin(false);', "BtnCancelImapUserLogin", ($UseImapUserLogin==1?'':'style="display:none;"'), "", "");
											$btnsImapUserLogin.= '<input type="hidden" id="UseImapUserLogin" name="UseImapUserLogin" value="'.($UseImapUserLogin==1?'1':'0').'" />';
											$btnsImapUserLogin.= '<input type="hidden" id="OldImapUserEmail" name="OldImapUserEmail" value="'.(isset($OldImapUserEmail)?$OldImapUserEmail:$IMapEmail).'" />';
											if(sizeof($errorAry)>0 && in_array($ImapUserLogin."@".$SYS_CONFIG['Mail']['UserNameSubfix'], $errorAry)){
												if(!intranet_validateEmail($ImapUserLogin."@".$SYS_CONFIG['Mail']['UserNameSubfix'])){
													$btnsImapUserLogin .= "<br /><font color='#FF0000'>".$Lang['AccountMgmt']['ErrorMsg']['InvalidEmail'].": ".$ImapUserLogin."@".$SYS_CONFIG['Mail']['UserNameSubfix']."</font>";
												}else if(in_array($ImapUserLogin, (array)$system_reserved_account)){
													$btnsImapUserLogin .= "<br /><font color='#FF0000'>".$Lang['AccountMgmt']['ErrorMsg']['EmailUsed'].": ".$ImapUserLogin."@".$SYS_CONFIG['Mail']['UserNameSubfix']."</font>";
												}else{
													$btnsImapUserLogin .= "<br /><font color='#FF0000'>".$ImapUserLogin."@".$SYS_CONFIG['Mail']['UserNameSubfix'].$Lang['AccountMgmt']['UsedByOtherUser']."</font>";
												}
											}
										}
										?>
										<!-- imail gamma -->	
										<tr>
											<td class="formfieldtitle"><span class="tabletextrequire">*</span><?=$i_UserEmail?></td>
											<td>
												<input name="EmailStatus" type="radio" id="EmailDisable" value="disable" <?=$disabled?>/><label for="EmailDisable"><?=$Lang['General']['Disabled']?></label><br>
												<input name="EmailStatus" type="radio" id="EmailEnable" value="enable" <?=$enabled?>/><label for="EmailEnable"><?=$Lang['General']['Enabled']?></label> - <span id="IMapUserEmail" <?=$sys_custom['iMailPlus']['EmailAliasName'] && $UseImapUserLogin==1?'style="display:none"':''?>><?=$IMapEmail?></span><?=$sys_custom['iMailPlus']['EmailAliasName']?$btnsImapUserLogin:""?><br>
												<?="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$Lang['Gamma']['Quota']?>: <input name="Quota" type="text" id="Quota" class="textboxnum" maxlength=5 value="<?=$EmailQuota?>"/>MB</div>
											</td>
										</tr>
										<?
									}
								}?>
								
							<?php if(!$ssoservice["Google"]["Valid"]){ ?>
							  <tr>
							    <td class="formfieldtitle"><?php echo $Lang['Gamma']['UserGmail']; ?></td>
							    <td>
							    	<div id="div_gmail">
							    	<?php $is_choose_gmail = false; ?>
							    	<?php foreach((array)$google_apis as $google_api){ ?>
    									<?php $config_index = $google_api->getConfigIndex(); ?>
    									<?php $gmail_variable = 'gmail_'.$config_index; ?>
    									<?php
    									if($$gmail_variable=='1'){
    										$is_choose_gmail = true;
    									}
    									?>
						    		<?php } ?>
							    	<input type="checkbox" class="is_display_options" <?php echo $is_choose_gmail?'checked':''?> />
							    	<div class="options" style="display:<?php echo $is_choose_gmail?'block':'none'?>;">
							    	<?php foreach((array)$google_apis as $google_api){ ?>
    									<?php $config_index = $google_api->getConfigIndex(); ?>
    									<?php $gmail_variable = 'gmail_'.$config_index; ?>
    									<input type="radio" name="r_gmail" id="r_gmail_<?php echo $config_index; ?>" <?php echo ($$gmail_variable == '1')? "checked" : "" ?> <?php echo ($total_quota[$config_index]!=='NO_QUOTA' && $remaining_quota[$config_index] <= 0 && $account_exists[$config_index]===false)? "disabled" : "" ?> />
						    			<input type="checkbox" value="1" name="gmail_<?php echo $config_index; ?>" id="gmail_<?php echo $config_index; ?>" <?php echo ($$gmail_variable == '1')? "checked" : "" ?> style="display:none;" />
						    			<label for="r_gmail_<?php echo $config_index; ?>" id="gmail_<?php echo $config_index; ?>_label"><?php echo $userInfo['UserLogin'];?>@<?php echo $google_api->getDomain();?> <?php if($total_quota[$config_index]!=='NO_QUOTA'){?><?php echo $total_quota[$config_index] - $remaining_quota[$config_index]; ?>/<?php echo $total_quota[$config_index]; ?>)<?php } ?><?php echo $account_exists[$config_index]?' <span style="color:rgb(0,97,0);background-color:rgb(198,239,206);">(G Suite '.Get_Lang_Selection('帳戶已經存在', 'account exists').')</span>':'' ?></label>
						    			<?php
						    			$message = '';
					    				switch($_SESSION["SSO"]["Google"]["Message"]['Config'][$google_api->getConfigIndex()]){
					    					case "ACCOUNT_CREATED":
					    						$message = $Lang['Gamma']['UserGmailAccountExists'];
					    						$_SESSION["SSO"]["Google"]["Message"]['Config'][$google_api->getConfigIndex()]='NO_MESSAGE';
					    						break;
					    					case "NO_MESSAGE":
					    						break;
					    					default:
					    						$message = $_SESSION["SSO"]["Google"]["Message"]['Config'][$google_api->getConfigIndex()];
					    						$_SESSION["SSO"]["Google"]["Message"]['Config'][$google_api->getConfigIndex()]='NO_MESSAGE';
					    						break;
					    				}
						    			?>
						    			<span style="background-color:rgb(255,199,206);color:rgb(156,0,6);"><?php echo ($message==''?'':'&nbsp;&nbsp;&nbsp;').$message.($message==''?'':'&nbsp;&nbsp;&nbsp;'); ?></span>
						    			<br/>
						    		<?php } ?>
						    		</div>
						    		</div>
								</td>
							  </tr>
							  <tr id="tr_googleUserLogin" style="display:none;">
							    <td class="formfieldtitle"><?=$Lang['General']['GoogleUserLogin']?></td>
							    <td>
							    	<input name="googleUserLogin" type="text" id="googleUserLogin" class="textbox_name" value="<?=$googleUserLogin?>" maxlength="100"/>
							    </td>
							  </tr>
							  <script>
							  	var obj_is_display_options = $('div#div_gmail .is_display_options');
								var obj_checkboxes = $('div#div_gmail input[type="checkbox"]').not(obj_is_display_options);
								var obj_google_radio_buttons = $('div#div_gmail input[type="radio"]');
								
								var google_radio_button_initially_checked = [];
								obj_google_radio_buttons.each(function(){
									var google_radio_button_id = $(this).attr('id');
									google_radio_button_initially_checked[google_radio_button_id] = $(this).is(':checked');
								});
								
							  	obj_is_display_options.bind('change',function(){
									var is_checked = $(this).is(":checked");
									obj_checkboxes.attr('checked',false);
									obj_google_radio_buttons.attr('checked',false);
									if(is_checked){
										obj_google_radio_buttons.each(function(){
											var google_radio_button_id = $(this).attr('id');
											if(google_radio_button_initially_checked[google_radio_button_id]){
												$(this).attr('checked',true);
											}
										});
										$('div#div_gmail .options').css({'display':'block'});
										$('#tr_googleUserLogin').show();
									}else{
										$('div#div_gmail .options').css({'display':'none'});
										$('#tr_googleUserLogin').hide();
									}
							  	});
								if(obj_checkboxes.length == 1){
									obj_is_display_options.css({'display':'none'});
									$('div#div_gmail .options').css({'display':'block'});
									$('#tr_googleUserLogin').show();
									
									obj_checkboxes.each(function(){
										var id = $(this).attr('id');
										$(this).css({'display':'inline'});
										obj_google_radio_buttons.css('display','none');
										$('#'+id+'_label').css({'display':($(this).is(":checked") == true?'inline':'none')});
									});
									obj_checkboxes.bind('click',function(){
										var id = $(this).attr('id');
										$('#'+id+'_label').css({'display':($(this).is(":checked") == true?'inline':'none')});
									});
								}
								obj_checkboxes.bind('change',function(){
									var is_checked = $(this).is(":checked");
									if(is_checked){
										obj_checkboxes.each(function(){
											$(this).attr('checked',false);
										});
										$(this).attr('checked',true);
									}
								});
								obj_google_radio_buttons.bind('change',function(){
									var id = $(this).attr('id');
									var checkbox_id = id.substring(2);
									$('div#div_gmail #'+checkbox_id).attr('checked',true);
									$('div#div_gmail #'+checkbox_id).trigger('change');
								});
							  </script>
							<?php }	?>
								
						  <tr>
						    <td class="formfieldtitle"><span class="tabletextrequire">*</span><?=$Lang['Gamma']['UserEmail']?></td>
						    <td><input class="textbox_name" type="text" name="UserEmail" size="30" maxlength="50" value="<?=$email?>">
<?php if (!$sys_custom['project']['HKPF']):?>						    
								<br /><span class="tabletextremark"><?=$Lang['AccountMgmt']['EmailFieldReminder']?></span></td>
<?php endif;?>								
						  </tr>
						  <tr>
						    <td class="formfieldtitle"><span class="tabletextrequire">*</span><?=$i_general_status?></td>
						    <td>
						    	<input type="radio" name="status" id="status1" value="1" <? if($status=="1") echo "checked";?>><label for="status1"><?=$Lang['Status']['Activate']?></label>
						    	<input type="radio" name="status" id="status0" value="0" <? if($status=="0" || $status=="") echo "checked";?>><label for="status0"><?=$Lang['Status']['Suspend']?></label>
						    </td>
						  </tr>
						  <? if((isset($module_version['StaffAttendance'])&&$module_version['StaffAttendance']!="" ) || (isset($plugin['payment'])&& $plugin['payment'])) { ?>
						  <tr>
						    <td class="formfieldtitle"><?=$i_SmartCard_CardID?><? if(sizeof($errorAry)>0 && in_array($smartcardid, $errorAry)) echo "<font color='#FF0000'>".$i_SmartCard_CardID.$Lang['AccountMgmt']['UsedByOtherUser']."</font>"; ?></td>
						    <td><input name="smartcardid" type="text" id="smartcardid" class="textboxnum" value="<?=$smartcardid?>"/></td>
						  </tr>
						  <? } ?>
						  
<?php if (!$sys_custom['project']['HKPF']):?>						  
						  <tr<?=$isKIS?' style="display:none;"':''?>>
						    <td class="formfieldtitle"><?=$Lang['AccountMgmt']['StaffCode']?></td>
						    <td><input name="staffCode" type="text" id="staffCode" class="textboxnum" value="<?=$staffCode?>"/><span id="spanChecking"><? if(sizeof($errorAry)>0 && in_array($staffCode, $errorAry)) echo " <font color='#FF0000'>".$Lang['AccountMgmt']['StaffCode'].$Lang['AccountMgmt']['UsedByOtherUser']."</font>"; ?></td>
						  </tr>
						 
						  <tr>
						    <td class="formfieldtitle"><?=$Lang['AccountMgmt']['Barcode']?></td>
						    <td><input name="barcode" type="text" id="barcode" class="textboxnum" value="<?=$barcode?>"/><span id="spanChecking"><? if(sizeof($errorAry)>0 && in_array($barcode, $errorAry)) echo " <font color='#FF0000'>".$Lang['AccountMgmt']['Barcode'].$Lang['AccountMgmt']['UsedByOtherUser']."</font>"; ?></td>
						  </tr>
<?php endif;?>
						  
<?php if ($sys_custom['project']['HKPF']):?>
						  <tr>
								<td class="formfieldtitle"><?=$Lang['AccountMgmt']['Grade']?></td>
								<td><input name="grade" type="text" id="grade" class="textbox_name" value="<?=$grade?>"/></td>
						  </tr>

						  <tr>
								<td class="formfieldtitle"><?=$Lang['AccountMgmt']['Duty']?></td>
								<td><input name="duty" type="text" id="duty" class="textbox_name" value="<?=$duty?>" /></td>
						  </tr>
<?php endif;?>							
						  
						  <tr>
						    <td colspan="3"><em class="form_sep_title"> - <?=$Lang['AccountMgmt']['BasicInfo']?> -</em></td>
						    </tr>
						  <tr>
						    <td class="formfieldtitle" rowspan="2"><?=$i_general_name?></td>
						    <td colspan="2"><span class="sub_row_content"><span class="tabletextrequire">*</span>(<?=$ip20_lang_eng?>)</span>
								<input name="engname" type="text" id="engname" class="textbox_name" value="<?=$engname?>" />
						    </td>
						  </tr>
						  <tr>
						    <td colspan="2"><span class="sub_row_content">(<?=$Lang['AccountMgmt']['Chi']?>)</span>
								<input name="chiname" type="text" id="chiname" class="textbox_name" value="<?=$chiname?>" />
						    </td>
						  </tr>
						  
<?php if (!$sys_custom['project']['HKPF']):?>						  
						  <tr>
						    <td class="formfieldtitle"><?=$i_UserNickName?></td>
						    <td colspan="2"><input name="nickname" type="text" id="nickname" class="textbox_name" value="<?=$nickname?>"/></td>
						  </tr>
						  <tr>
						    <td><span class="tabletextrequire">*</span><?=$i_UserGender?></td>
						    <td colspan="2"><input type="radio" name="gender" id="genderM" value="M" <? if($gender=="M") echo "checked"; ?>/>
						      <label for="genderM"><?=$i_gender_male?></label>
						      <input type="radio" name="gender" id="genderF" value="F"  <? if($gender=="F") echo "checked"; ?>/>
						      <label for="genderF"><?=$i_gender_female?></label></td>
						  </tr>
						  <tr>
						    <td class="formfieldtitle" rowspan="2"><?=$Lang['AccountMgmt']['UserDisplayTitle']?></td>
						    <td colspan="2"><span class="sub_row_content">(<?=$ip20_lang_eng?>)</span>
						      <input name="engTitle" type="text" id="engTitle" class="textbox_name" value="<?=$engTitle?>"/> <?=$select_eng_title?>
						       </td>
						  </tr>
						  <tr>
						    <td colspan="2"><span class="sub_row_content">(<?=$Lang['AccountMgmt']['Chi']?>)</span>
						      <input name="chiTitle" type="text" id="chiTitle" class="textbox_name" value="<?=$chiTitle?>"/> <?=$select_chi_title?>
						       </td>
						  </tr>
						  <tr>
						    <td class="formfieldtitle"><?=$i_UserAddress?></td>
						    <td colspan="2"><?=$linterface->GET_TEXTAREA("address", $address);?>
						      <br />
						      <?=$countrySelection?></td>
						  </tr>
						  <tr>
						    <td class="formfieldtitle" rowspan="<?=$sys_custom['eAdmin']['AcctMgmt']['UseFaxForBaptism']?'3':'4'?>"><?=$Lang['AccountMgmt']['Tel']?></td>
						    <td colspan="2"><span class="sub_row_content">(<?=$Lang['AccountMgmt']['Home']?>)</span>
						        <input name="homePhone" type="text" id="homePhone" class="textboxnum" value="<?=$homePhone?>"/></td>
						  </tr>
						  <tr>
						    <td colspan="2"><span class="sub_row_content">(<?=$Lang['AccountMgmt']['Office']?>)</span>
						        <input name="officePhone" type="text" id="officePhone" class="textboxnum" value="<?=$officePhone?>"/></td>
						  </tr>
						  <tr>
						    <td colspan="2"><span class="sub_row_content">(<?=$Lang['AccountMgmt']['Mobile']?>)</span>
						        <input name="mobilePhone" type="text" id="mobilePhone" class="textboxnum" value="<?=$mobilePhone?>"/></td>
						  </tr>
<?php
	if ($sys_custom['eAdmin']['AcctMgmt']['UseFaxForBaptism']) {
?>
						<tr>
							<td class="formfieldtitle"><?=$Lang['AccountMgmt']['Fax']?></td>
							<td><input name="faxPhone" type="text" id="faxPhone" class="textboxnum" value="<?=$faxPhone?>" maxlength="10"/>(<?=$Lang['AccountMgmt']['YYYYMMDD']?>)</td>
						</tr>
<?php
	}
	else {
?>
						  <tr>
						    <td colspan="2"><span class="sub_row_content">(<?=$Lang['AccountMgmt']['Fax']?>)</span>
						        <input name="faxPhone" type="text" id="faxPhone" class="textboxnum" value="<?=$faxPhone?>"/></td>
						  </tr>
<?php		
	}	
?>								
						  <?php if($sys_custom['iMail']['UserDisplayOrder']){ ?>	
								<tr>
								<td class="formfieldtitle"><?=$Lang['AccountMgmt']['DisplayPriority']?></td>
									<td colspan="2">
										<?=$linterface->GET_TEXTBOX_NUMBER("DisplayOrder", "DisplayOrder", $DisplayOrder, '', array('onchange'=>'restrictInteger(this);'));?><br /><span class="tabletextremark">(<?=$Lang['AccountMgmt']['DisplayPriorityRemark']?>)</span>
									</td>
								</tr>
						  <?php } ?>
<?php endif; // not HKPF?>						  
						  <!-- Additional Info //-->
								<tr>
									<td colspan="3"><em class="form_sep_title"> - <?=$Lang['AccountMgmt']['AdditionInfo']?> -</em></td>
								</tr>
								<tr>
								<td class="formfieldtitle"><?=$Lang['General']['Remark']?></td>
									<td colspan="2">
										<?=$linterface->GET_TEXTAREA("remark", $remark);?>
									</td>
								</tr>
<?php if($plugin['SDAS_module']['KISMode']):?>
<?php include($PATH_WRT_ROOT."/home/cees_kis/lang/".$intranet_session_language.".php");?>
								<td class="formfieldtitle"><?=$Lang['CEES']['MonthlyReport']['JSLang']['Position']?></td>
									<td>
										<?=getSelectByAssoArray($Lang['CEES']['MonthlyReport']['JSLang']['PositionName'], " name='position' id='form_position' ", $position);?>
									</td>
								</tr>
								<td class="formfieldtitle"><?=$Lang['CEES']['MonthlyReport']['JSLang']['ContractType']?></td>
									<td>
										<?=getSelectByAssoArray(array('1' => $Lang['CEES']['MonthlyReport']['JSLang']['Permanent'], '4' => $Lang['CEES']['MonthlyReport']['JSLang']['Contract']), " name='contractType' id='form_contractType' ", $contractType);?>
									</td>
								</tr>
								<td class="formfieldtitle"><?=$Lang['CEES']['MonthlyReport']['JSLang']['EffectiveDate']?></td>
									<td>
										<?=$linterface->GET_DATE_PICKER("contractStartDate", $contractStartDate,"","yy-mm-dd","","","","","",0, $CanEmptyField=1);?>
									</td>
								</tr>
								<td class="formfieldtitle"><?=$Lang['CEES']['MonthlyReport']['JSLang']['TerminationDate']?></td>
									<td>
										<?=$linterface->GET_DATE_PICKER("contractEndDate", $contractEndDate,"","yy-mm-dd","","","","","",0, $CanEmptyField=1);?>
									</td>
								</tr>
<?php endif;?>
<?php if (!$sys_custom['project']['HKPF']):?>								
						  <!-- Internet Usage //-->
						  <tr>
						    <td colspan="3"><em class="form_sep_title">- <?=$Lang['AccountMgmt']['InternetUsage']?> -</em></td>
						  </tr>
<?php endif;?>						  
						  <?
						  	$sql = "SELECT REVERSE(BIN(ACL)) FROM INTRANET_SYSTEM_ACCESS WHERE UserID = '".$uid."'";
						  	$result = $laccount->returnVector($sql);
							$can_access_iMail = substr($result[0],0,1);
							$can_access_iFolder = substr($result[0],1,1);
						  ?>
						  <?if(!$plugin['imail_gamma']) {?>
						  	<? if(isset($plugin['webmail']) && $plugin['webmail'] && in_array(TYPE_TEACHER, $webmail_identity_allowed)) { ?>
							  <tr>
							    <td class="formfieldtitle"><?=$ip20TopMenu['iMail']?></td>
							    <td colspan="2"><input type="checkbox" value="1" name="open_webmail" disabled <? echo ($can_access_iMail)? "checked" : "" ?> />
							      <?=$i_Mail_AllowSendReceiveExternalMail?></td>
							  </tr>
						  	<?}?>
						  <?}?>
						  <? if(isset($plugin['personalfile']) && $plugin['personalfile'] && in_array(TYPE_TEACHER,$personalfile_identity_allowed)) { ?>
							  <tr>
							    <td class="formfieldtitle"><?=$ip20TopMenu['iFolder']?></td>
							    <td colspan="2"><input type="checkbox" value="1" name="open_file" disabled <? echo ($can_access_iFolder)? "checked" : "" ?> />
							      <?=$i_Files_OpenAccount?></td>
							  </tr>
						  <? } ?>
						  
						  <?php if($plugin['radius_server']){ 
						  	$enable_wifi_access = !$laccount->isUserDisabledInRadiusServer($userlogin);
						  ?>
						  <tr>
						  	<td colspan="3"><em class="form_sep_title">- <?=$Lang['AccountMgmt']['WifiUsage']?> -</em></td>
						  </tr>
						  <tr>
						  	<td class="formfieldtitle"><?=$Lang['AccountMgmt']['EnableWifiAccess']?></td>
						  	<td><input type="checkbox" name="enable_wifi_access" value="1" <?=$enable_wifi_access?'checked="checked"':''?> /></td>
						  </tr>
						  <?php } ?>
						  
							<tr>
							<td colspan="3"><em class="form_sep_title"> -  <?=$Lang['AccountMgmt']['CreatedAndModifiedRecords']?> -</em></td>
							</tr>
							<?php  if($nameDisplay){?>
							<tr>
							<td class="formfieldtitle"><?=$Lang['AccountMgmt']['CreateUserName']?></td>
							<td colspan="2"><?=$nameDisplay?></td>
							</tr>
							<tr>
							<td class="formfieldtitle"><?=$Lang['AccountMgmt']['CreateUserDate']?></td>
							<td colspan="2"><?=$DateInput?></td>
							</tr>
							<? } ?>
							<tr>
							<td class="formfieldtitle"><?=$Lang['AccountMgmt']['LastEditUserName']?></td>
							<td colspan="2"><?=$ModifiernameDisplay?></td>
							</tr>
							<tr>
							<td class="formfieldtitle"><?=$Lang['AccountMgmt']['LastEditUserDate']?></td>
							<td colspan="2"><?=$DateModified?></td>
							</tr>

						  
						  
						</table>
						<p class="spacer"></p>
                      </div>
                      <div class="edit_bottom">
                          <p class="spacer"></p>
<?php if (!$sys_custom['project']['HKPF']):?>                          
                          <?= $linterface->GET_ACTION_BTN($button_continue, "button", "goSubmit('edit2.php')")?>
<?php endif;?>                          
                          <?= $linterface->GET_ACTION_BTN($button_finish, "button", "document.form1.skipStep2.value=1;goSubmit('edit_update.php')")?>
                          <?= $linterface->GET_ACTION_BTN($button_cancel, "button", "goBack()")?>
							<p class="spacer"></p>
                    </div></td>
                </tr>
              </table>
    </td></tr>
</table>
