<?php
// Using :

###########################################
#
#   Date:   2019-11-05 Ray - add CALENDAR_CALENDAR_VIEWER group create
#	Date:	2019-08-04 Chris - Added in Google SSO to sync default identity groups.
#	Date:	2018-07-24 Carlos - Enlarge the script timeout from 10 minutes to one hour.
#	Date:	2017-06-12 Pun [ip.2.5.8.4.1]
#           Added $sys_custom['iPortfolio_auto_add_teacher_to_group'] for auto add teacher to iPortfolio teacher group
#
#	Date:	2017-06-06 Pun [ip.2.5.8.4.1]
#           Added $sys_custom['iPortfolio_auto_active_student'] for auto active iPortfolio account
#
#	Date:	2016-08-09 Carlos
#			$sys_custom['BIBA_AccountMgmtCust'] - added [HouseholdRegister] and [House] for student. 
#												- added [House] for staff.
#
#	Date:	2016-05-09 Carlos
#			$sys_custom['ImportStudentAccountActionType'] - added action_type for student user type.
#
#	Date:	2015-08-14 (Omas)
#			Add Import Log
#
#	Date:	2015-05-05 (Omas)
# 			improved - insert MyCalendar
#
#	Date:	2013-09-20	Ivan
#			Fixed: $ImportData changed from intranet_undo_htmlspecialchars to urldecode
#
#	Date:	2012-08-01	Ivan
#			added logic to syn user info to library system
#
#	Date:	2011-11-02	YatWoon
#			add alumni 
#
###########################################

@SET_TIME_LIMIT(3600);
ini_set("memory_limit", "1024M");

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");
include_once($PATH_WRT_ROOT."includes/libimport.php");
include_once($PATH_WRT_ROOT."includes/libftp.php");
// include_once($PATH_WRT_ROOT."includes/libeclass.php");
include_once($PATH_WRT_ROOT."includes/libeclass40.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/icalendar.php");

intranet_auth();
intranet_opendb();

$laccount = new libaccountmgmt();
$li = new libimport();
$lu = new libuser();
$lcalendar = new icalendar();
$linterface = new interface_html();

//$data = unserialize(intranet_undo_htmlspecialchars(stripslashes($ImportData)));
$data = unserialize(urldecode(stripslashes($ImportData)));

# Log
if($sys_custom['AccountMgmt']['LogEditImportUser'])
{
	include_once($PATH_WRT_ROOT."includes/libupdatelog.php");
	$liblog = new libupdatelog();

	$filepath = $_SERVER['DOCUMENT_ROOT'].'/file/logfiles/accountMgmt';
	$filename = $liblog->LOG_IMPORT_FILE($data,$filepath);
	$liblog->INSERT_UPDATE_LOG('AccountMgmt', 'Import', $filepath.'/'.$filename);

	unset($liblog);
}

if($TabID != TYPE_ALUMNI)
{
    if (is_array($data))
    {
    	$format = 1; # 1 means CSV
        $li->open_webmail = ($open_webmail==1);
        $li->open_file_account= ($open_file==1);
        $li->teaching = $teaching;
        if(/*$sys_custom['ImportStudentAccountActionType'] && */ $TabID == USERTYPE_STUDENT){
			if(!in_array($action_type,array(1,2))) $action_type == 1;
			$li->action_type = $action_type;
		}
        $li->set_format($format);
        $li->process_data2($data);
        
//         if($ssoservice["Google"]["Valid"]){
//             include_once($PATH_WRT_ROOT."includes/google_api/libgoogleapi.php");
//             include_once($PATH_WRT_ROOT."includes/sso/libSSO_db.php");
//             include_once($PATH_WRT_ROOT."includes/sso/libGoogleSSO.php");
//             $libGoogleSSO = new libGoogleSSO();
//
//             $gmail_input_array = array();
//             $array_config_index = $libGoogleSSO->getAllGoogleAPIConfigIndex();
//             foreach((array)$array_config_index as $config_index){
//                 $gmail_array_index = 'gmail_' . $config_index;
//                 $gmail_input_array[$gmail_array_index] = $$gmail_array_index;
//             }
//
//             foreach ($data as $value) {
//                 $userlogin = $value[0];
//                 $sql = "SELECT UserID FROM INTRANET_USER WHERE UserLogin='".$userlogin."'";
//                 $userId = $laccount->returnVector($sql);
//                 $uid = $userId[0];
//                 $user_groups = $libGoogleSSO->enabledUserForGoogle($laccount, array($uid));
//                 if (count($user_groups['activeUsers'])>0) {
//                     $year_id = Get_Current_Academic_Year_ID();
//                     $sql_query_one = "SELECT GroupID FROM INTRANET_GROUP WHERE (AcademicYearID = ".$year_id." OR AcademicYearID is NULL)";
//                     $all_group_ids = $laccount->returnVector($sql_query_one);
//                     if (count($all_group_ids)>0) {
//                         $sql_query = "SELECT GroupID FROM INTRANET_USERGROUP WHERE UserID='".$uid."' AND GroupID IN (".implode(',', $all_group_ids).")";
//                         $group_ids = $laccount->returnVector($sql_query);
//                         $libGoogleSSO->syncGroupForUserFromEClassToGoogle($laccount, $userlogin, $gmail_input_array, $group_ids);
//                     }
//                 }
//             }
//         }

        # SchoolNet integration
        if ($plugin['SchoolNet'])
        {
            include_once($PATH_WRT_ROOT."includes/libschoolnet.php");
            $lschoolnet = new libschoolnet();

            if (sizeof($li->new_account_userids) != 0)
            {
                $userlist = implode(",",$li->new_account_userids);
                if ($TabID == 1)
                {
                    $sql = "SELECT UserLogin, UserPassword, DATE_FORMAT(DateOfBirth,'%Y-%m-%d'), Gender, Chinesename, EnglishName, HomeTelNo, MobileTelNo, Address, UserEmail, Teaching FROM INTRANET_USER WHERE UserID = $UserID";
                    $tmpdata = $li->returnArray($sql,11);
                    $lschoolnet->addStaffUser($tmpdata);
                }
                else if ($TabID == 2)
                {
                     $sql = "SELECT UserLogin, UserPassword,ClassName, ClassNumber, DATE_FORMAT(DateOfBirth,'%Y-%m-%d'), Gender, Chinesename, EnglishName, HomeTelNo, MobileTelNo, Address, UserEmail FROM INTRANET_USER WHERE UserID = $UserID";
                     $tmpdata = $li->returnArray($sql,12);
                     $lschoolnet->addStudentUser($tmpdata);
                }
                else if ($TabID == 3)
                {
                    // nothing
                }
            }
        }
         
        $error_msg = $li->getErrorMsgArr();
    }
}
else // Alumni
{
	if (is_array($data))
    {
    	$format = 1; # 1 means CSV
        $li->open_webmail = ($open_webmail==1);
        // $li->open_file_account= ($open_file==1);
        // $li->teaching = $teaching;
        $li->set_format($format);
        $li->process_data2($data);

        $error_msg = $li->getErrorMsgArr();
    }
}

for($i=0; $i<count($data); $i++)
{
	if(empty($li->reason[$i])) {
		$import_success++;
    }
}

$successAry['synUserDataToModules'] = $lu->synUserDataToModules($li->new_account_userids);

## Add My Calendar to Users
$newAccountArr = $li->new_account_userids;
$numNewAccount = count($newAccountArr);
for($i=0; $i<$numNewAccount; $i++){
	$lcalendar->insertMyCalendar($newAccountArr[$i]);
}

# iCalendar
foreach($newAccountArr as $userId)
{
	$sql = "SELECT GroupID FROM INTRANET_USERGROUP WHERE UserID = '$userId' ";
	$all_groups = $lu->returnVector($sql);

	$sql = "DELETE FROM CALENDAR_CALENDAR_VIEWER 
            WHERE GroupID NOT IN ('".implode("','", $all_groups)."') AND GroupType = 'E' AND UserID = '$userId'";
	$lu->db_db_query($sql);

	$sql = "SELECT GroupID FROM CALENDAR_CALENDAR_VIEWER WHERE GroupType = 'E' AND UserID = '$userId'";
	$existing = $lu->returnVector($sql);

	$existing = empty($existing) ? array() : $existing;

	$sql = "INSERT INTO CALENDAR_CALENDAR_VIEWER 
				(CalID, UserID, GroupID, GroupType, Access, Color, Visible)
                SELECT
                    g.CalID, '$userId', g.GroupID, 'E', 'R', '2f75e9', '1' 
                FROM INTRANET_GROUP as g 
                    INNER JOIN INTRANET_USERGROUP as u ON g.GroupID = u.GroupID AND u.UserID = '$userId'
				WHERE g.GroupID NOT IN ('".implode("','", $existing)."')";
	$lu->db_db_query($sql);

	$cal_group_sql = "SELECT g.GroupID FROM INTRANET_GROUP as g 
                          INNER JOIN INTRANET_USERGROUP as u ON u.GroupID = g.GroupID 
                      WHERE u.UserID = '$userId'";
	$cal_group_ary = $li->returnVector($cal_group_sql);
	for($i=0; $i<sizeof($cal_group_ary); $i++) {
		$lcalendar->addCalendarViewerToGroup($cal_group_ary[$i],$userId);
	}
}

if($TabID == TYPE_STUDENT && $plugin['iPortfolio'] && $sys_custom['iPortfolio_auto_active_student'])
{
    $newAccountSql = implode("','", $newAccountArr);
    $sql = "UPDATE INTRANET_USER SET WebSAMSRegNo = CONCAT('#', UserLogin) WHERE UserID IN ('{$newAccountSql}') AND (WebSAMSRegNo IS NULL OR WebSAMSRegNo = '')";
    $li->db_db_query($sql);

    include_once($PATH_WRT_ROOT.'includes/libpf-sturec.php');
    #Siuwan 20130828 Set $ck_course_id before init $lpf_sturec

    $sql = "SELECT course_id FROM $eclass_db.course WHERE RoomType = 4";
    $ary = $li->returnVector($sql);
    $ck_course_id = $ary[0];
    $lpf_sturec = new libpf_sturec();
    
    foreach($newAccountArr as $userId){
    	$lpf_sturec->LOAD_KEYS_FROM_SETTING();
        $lpf_sturec->DO_ACTIVATE_STUDENT($userId);
    }
}

if($TabID == TYPE_TEACHER && $plugin['iPortfolio'] && $sys_custom['iPortfolio_auto_add_teacher_to_group'])
{
    $sql = "SELECT course_id FROM {$eclass_db}.course WHERE RoomType = 4";
    $rs = $li->returnVector($sql);
    $course_id = $rs[0];
    
    $lo = new libeclass($course_id);
    $lo->setRoomType($lo->getEClassRoomType($course_id));
    foreach($newAccountArr as $userId){
        $lo->eClassUserAddFullInfoByUserID($userId,'T');
    }
}

## gen title / back_btn_url 
switch ($TabID)
{
        case TYPE_TEACHER: 
        	$Title = $Lang['AccountMgmt']['ImportTeacherAccount'];

        	$CurrentPageArr['StaffMgmt'] = 1;
            $CurrentPageArr['StaffMgmt'] = 1;
        	$MODULE_OBJ['title'] = $Lang['Header']['Menu']['StaffAccount'];

        	$url = "../StaffMgmt/index.php";
        	break;

        case TYPE_STUDENT:
        	$Title = $Lang['AccountMgmt']['ImportStudentAccount'];

        	$CurrentPageArr['StudentMgmt'] = 1; 
        	$CurrentPage = "Mgmt_Account";
        	$MODULE_OBJ = $laccount->GET_MODULE_OBJ_ARR();

        	$url = "../StudentMgmt/index.php";
        	break;

        case TYPE_PARENT:
        	$Title = $Lang['AccountMgmt']['ImportParentAccount'];

        	$CurrentPageArr['ParentMgmt'] = 1; 
        	$MODULE_OBJ['title'] = $Lang['Header']['Menu']['ParentAccount'];

			$url = "../ParentMgmt/index.php";
        	break;

        case TYPE_ALUMNI: 
        	$Title = $Lang['AccountMgmt']['ImportAlumniAccount'];

        	$CurrentPageArr['StudentMgmt'] = 1;
        	$MODULE_OBJ['title'] = $Lang['Header']['Menu']['AlumniAccount'];

        	$url = "../AlumniMgmt/index.php";
        	break;
}
$BackBtn = $linterface->GET_ACTION_BTN($button_back, "button", "window.location='$url'","back"," class='formbutton'  onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"");

### Title ###
$PAGE_NAVIGATION[] = array($Title,"");

$TAGS_OBJ[] = array($Lang['Group']['UserList'], "", 0);    

$STEPS_OBJ[] = array($i_general_select_csv_file, 0);
$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 0);
$STEPS_OBJ[] = array($i_general_imported_result, 1);

$linterface->LAYOUT_START();
?>

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
	<tr>
		<td colspan="2"><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	</tr>
	<tr>
		<td colspan="2"><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
	</tr>
	<tr>
		<td clospan="2" align="center">
		<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr>
			<td class='tabletext' align='center'><?=$import_success?> <?=$Lang['SysMgr']['FormClassMapping']['RecordsImportedSuccessfully']?></td>
		</tr>

		<tr>
			<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
		</tr>
		<tr>
			<td align="center" colspan="2">
			<?=$BackBtn ?>
			</td>
		</tr>
		</table>
		</td>
	</tr>
</table>

<?
$linterface->LAYOUT_STOP();
?>