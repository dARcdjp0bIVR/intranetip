<?php
// Editing by 
/*
 * 2014-03-11 (Carlos): Created for Lassel College
 */
@SET_TIME_LIMIT(3600);
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libimport.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if(!$plugin['imail_gamma'] || !$sys_custom['iMailPlus']['BatchSetForwardingEmail'] || !isset($TabID) || !in_array($TabID,array(USERTYPE_STAFF,USERTYPE_STUDENT,USERTYPE_PARENT,USERTYPE_ALUMNI))){
	No_Access_Right_Pop_Up();
}
if(!($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"] || 
	($TabID==USERTYPE_STAFF && $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Staff"]) || 
	($TabID==USERTYPE_STUDENT && $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Student"]) || 
	($TabID==USERTYPE_PARENT &&  $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Parent"]) || 
	$_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"])) 
{
	No_Access_Right_Pop_Up();
}

$laccount = new libaccountmgmt();

$linterface 	= new interface_html();
$limport = new libimporttext();
$lo = new libfilesystem();

### CSV Checking
$name = $_FILES['userfile']['name'];
$ext = strtoupper($lo->file_ext($name));

if(!($ext == ".CSV" || $ext == ".TXT"))
{
	intranet_closedb();
	header("location: import_forwarding_email.php?xmsg=import_failed&TabID=$TabID");
	exit();
}
$data = $limport->GET_IMPORT_TXT($userfile);

$csv_header = array_shift($data);

$file_format = array("Email","English Name","Chinese Name","Forwarding Emails","Keep Copy");

# check csv header
$format_wrong = false;

for($i=0; $i<sizeof($file_format); $i++)
{
	if ($csv_header[$i]!=$file_format[$i])
	{
		$format_wrong = true;
		break;
	}
}

if($format_wrong)
{
	intranet_closedb();
	header("location:  import_forwarding_email.php?xmsg=import_header_failed&TabID=$TabID");
	exit();
}
if(empty($data))
{
	intranet_closedb();
	header("location: import_forwarding_email.php?xmsg=import_failed&TabID=$TabID");
	exit();	
}

$sql = "DROP TABLE TEMP_IMPORT_MAIL_FORWARD_EMAIL";
$laccount->db_db_query($sql);

$sql = "CREATE TABLE IF NOT EXISTS TEMP_IMPORT_MAIL_FORWARD_EMAIL (
			Email varchar(255) NOT NULL, 
			ForwardingEmails text DEFAULT NULL,
			KeepCopy varchar(1) DEFAULT '1',
			PRIMARY KEY (Email) 
		)ENGINE=InnoDB DEFAULT CHARSET=utf8";
$laccount->db_db_query($sql);

$data_count = count($data);

$sql = "SELECT 
			ImapUserEmail 
		FROM INTRANET_USER 
		WHERE RecordStatus='1' AND RecordType='$TabID' AND ImapUserEmail IS NOT NULL AND ImapUserEmail <> ''";
$emails = $laccount->returnVector($sql);

$error_msg = array();
$insert_sql = "INSERT INTO TEMP_IMPORT_MAIL_FORWARD_EMAIL (Email, ForwardingEmails, KeepCopy) VALUES ";
//$values = '';
//$delim = "";
$insertAry = array();
for($i=0;$i<$data_count;$i++){
	$row = $i+1;
	list($t_email, $t_eng_name, $t_chi_name, $t_forward_emails, $t_keepcopy) =  $data[$i];
	
	$error_reason = array();
	$t_email = trim($t_email);
	$t_forward_emails = trim($t_forward_emails);
	$t_keepcopy = trim($t_keepcopy);
	$t_keepcopy = $t_keepcopy == '' || $t_keepcopy=='1'? '1' : '0'; 
	
	if(!intranet_validateEmail($t_email)){
		$error_reason[] = $Lang['AccountMgmt']['EmailForwardingErrors']['InvalidEmail'] .": " . $t_email;
	}
	if(!in_array($t_email,$emails)){
		$error_reason[] = $Lang['AccountMgmt']['EmailForwardingErrors']['UserEmailNotFound'] . ": " . $t_email;
	}
	
	if($t_forward_emails != ''){
		$t_forwards_ary = explode(",", $t_forward_emails);
		for($j=0;$j<count($t_forwards_ary);$j++) {
			$t_forwards_ary[$j] = trim($t_forwards_ary[$j]);
			
			if(!intranet_validateEmail($t_forwards_ary[$j])){
				$error_reason[] = $Lang['AccountMgmt']['EmailForwardingErrors']['InvalidForwardingEmail'] .": " . $t_forwards_ary[$j];
			}
			if($t_forwards_ary[$j] == $t_email){
				$error_reason[] = $Lang['AccountMgmt']['EmailForwardingErrors']['DoNotForwardToSelfEmail'] .": ". $t_forwards_ary[$j];
			}
		}
		$t_forward_emails = implode(",",$t_forwards_ary);
	}
	if(count($error_reason)>0){
		$error_msg[$row] = array($t_email, $t_eng_name, $t_chi_name, $t_forward_emails, $t_keepcopy, $error_reason);
	}else{
		//$values .= $delim."('$t_email','$t_forward_emails','$t_keepcopy')";
		//$delim = ",";
		
		$insertAry[] = "('$t_email','$t_forward_emails','$t_keepcopy')";
	}
}


foreach($error_msg as $key => $errorInfo)
{
	#Build Table Content
	$rowcss = " class='".($ctr++%2==0? "tablebluerow2":"tablebluerow1")."' ";
	
	$Confirmtable .= "	<tr $rowcss>";
	$Confirmtable .= "		<td class='tabletext' valign='top'>".($key+1)."</td>";
	$Confirmtable .= "		<td class='tabletext' valign='top'>".$errorInfo[0]."</td>";
	$Confirmtable .= "		<td class='tabletext' valign='top'>".$errorInfo[1]."</td>";
	$Confirmtable .= "		<td class='tabletext' valign='top'>".$errorInfo[2]."</td>";
	$Confirmtable .= "		<td class='tabletext' valign='top'>".(str_replace(",","<br />",$errorInfo[3]))."</td>";
	$Confirmtable .= "		<td class='tabletext' valign='top'>".($errorInfo[4]=='1'?$Lang['General']['Yes']:$Lang['General']['No'])."</td>";
	$Confirmtable .= "		<td class='tabletext'>";
	foreach($errorInfo[5] as $thisReason)
		$Confirmtable .= $thisReason."<br>";
	$Confirmtable .= "		</td>";
	$Confirmtable .= "	</tr>";
}

if(count($error_msg)==0 && count($insertAry)>0){
	$chunks = array_chunk($insertAry,1000);
	for($k=0;$k<count($chunks);$k++) {
		$values = implode(",",$chunks[$k]);
		$sql = $insert_sql.$values;
		$laccount->db_db_query($sql);
	}
	//$insert_sql .= $values;
	//debug_r($insert_sql);
	//$laccount->db_db_query($insert_sql);
}

### Title ###
switch ($TabID)
{
        case TYPE_TEACHER: 
        	$Title = $Lang['AccountMgmt']['ImportStaffForwardingEmails'] ;
        	$CurrentPageArr['StaffMgmt'] = 1;
        	$MODULE_OBJ['title'] = $Lang['Header']['Menu']['StaffAccount'];
        	$CurrentPage = "Mgmt_Account";
        	$back_url = "../StaffMgmt/index.php";
        	break;
        case TYPE_STUDENT:
        	$Title = $Lang['AccountMgmt']['ImportStudentForwardingEmails'] ;
        	$CurrentPageArr['StudentMgmt'] = 1; 
        	$CurrentPage = "Mgmt_Account";
        	//$MODULE_OBJ = $laccount->GET_MODULE_OBJ_ARR();
        	$back_url = "../StudentMgmt/index.php"; 
        	break;
        case TYPE_PARENT:
        	$Title = $Lang['AccountMgmt']['ImportParentForwardingEmails'];
        	$CurrentPageArr['ParentMgmt'] = 1; 
        	$MODULE_OBJ['title'] = $Lang['Header']['Menu']['ParentAccount'];
        	$back_url = "../ParentMgmt/index.php";
        	$CurrentPage = "Mgmt_Account";
        	break;
        case TYPE_ALUMNI: 
        	$Title = $Lang['AccountMgmt']['ImportAlumniForwardingEmails'];
        	$CurrentPageArr['AlumniMgmt'] = 1;
        	$CurrentPage = "Mgmt_Account";
        	$MODULE_OBJ['title'] = $Lang['Header']['Menu']['AlumniAccount'];
        	$back_url = "../AlumniMgmt/index.php"; 
			break;
        default:  
        	break;
}

$BackBtn = $linterface->GET_ACTION_BTN($button_back, "button", "goBack()","back"," class='formbutton'  onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"");
$NextBtn = $linterface->GET_ACTION_BTN($button_import, "submit", "","submit2"," class='formbutton'  onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"");

$Btn = empty($error_msg)?$NextBtn."&nbsp;":"";
$Btn .= $BackBtn;


$PAGE_NAVIGATION[] = array($Title,"");

$TAGS_OBJ[] = array($Lang['AccountMgmt']['UserEmailForwarding'], "", 0);
      
$STEPS_OBJ[] = array($i_general_select_csv_file, 0);
$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 1);
$STEPS_OBJ[] = array($i_general_imported_result, 0);

$MODULE_OBJ = $laccount->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();

?>
<script type="text/javascript" language="javascript">
function goBack() {
	document.form1.action = "import_forwarding_email.php";
	document.form1.submit();
}
</script>
<form id="form1" name="form1" method="POST" action="import_forwarding_email_update.php">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
	<tr>
		<td colspan="2" align='right'><?=$linterface->GET_SYS_MSG("",$xmsg);?></td>
	</tr>
	<tr>
		<td colspan="2"><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	</tr>
	<tr>
		<td colspan="2"><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
	</tr>
	<tr>
		<td clospan="2" align="center">
			<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
			<tr>
				<td colspan="5">
				<table width="30%">
					<tr>
						<td class='formfieldtitle'><?=$Lang['General']['SuccessfulRecord']?></td>
						<td class='tabletext'><?=count($data)-count($error_msg)?></td>
					</tr>
					<tr>
						<td class='formfieldtitle'><?=$Lang['General']['FailureRecord']?></td>
						<td class='tabletext <?=count($error_msg)?"red":""?>'><?=count($error_msg)?></td>
					</tr>
				</table>
				</td>
			</tr>
			<tr><td colspan="2">&nbsp;</td></tr>
			<?if(!empty($error_msg)){?>
			<tr>
				<td class='tablebluetop tabletopnolink' width="1%">Row#</td>
				<td class='tablebluetop tabletopnolink' width="10%"><?=$i_UserEmail?></td>
				<td class='tablebluetop tabletopnolink' width="10%"><?=$Lang['General']['EnglishName']?></td>
				<td class='tablebluetop tabletopnolink' width="10%"><?=$Lang['General']['ChineseName']?></td>
				<td class='tablebluetop tabletopnolink' width="10%"><?=$Lang['AccountMgmt']['ForwardingEmails']?></td>
				<td class='tablebluetop tabletopnolink' width="9%"><?=$Lang['AccountMgmt']['KeepCopy']?></td>
				<td class='tablebluetop tabletopnolink' width="50%"><?=$Lang['General']['Remark']?></td>
			</tr>
			<?=$Confirmtable?>
			<?}?>
			</table>
		</td>
	</tr>
	<tr>
		<td>
		<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
            <tr>
            	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
            </tr>
            <tr>
				<td align="center">
				<?=$Btn?>
				</td>
			</tr>
        </table> 
		</td>
	</tr>
</table>
<input type="hidden" value="<?=$TabID?>" id="TabID" name="TabID">
</form>
<?
$linterface->LAYOUT_STOP();
?>