<?php
// Editing by

// Please use UTF-8 萬國碼 
/*
 * 2020-02-26 (Bill)    Added Graduation Date column to sample file    [2020-0220-1623-28098]
 * 2018-11-09 (Anna)    Added $plugin['StudentDataAnalysisSystem_Style'] == "tungwah" cust
 * 2018-08-21 (Cameron) revised HKPF fields, left necessary fields only
 * 2018-08-08 (Cameron) $sys_custom['project']['HKPF'] - add Grade and Duty field for staff
 * 2016-08-09 (Carlos): $sys_custom['BIBA_AccountMgmtCust'] - added HouseholdRegister and House import fields for student. 
 * 															- added House import field for staff.
 * 2016-01-13 (Carlos): $sys_custom['StudentAccountAdditionalFields'] - added five fields [NonChineseSpeaking], [SpecialEducationNeeds], [PrimarySchool], [University] and [Programme]. 	
 * 2015-03-19 (Cameron): use fax field for recording date of baptism if $sys_custom['eAdmin']['AcctMgmt']['UseFaxForBaptism'] = true
 * 								(applys to student and teacher only)								
 * 2014-12-10 (Bill)   : $sys_custom['SupplementarySmartCard'] - Added SmartCardID4 for student user type
 * 2014-08-21 (Bill)   : Add Home Tel column
 * 2014-07-17 (Bill)   : Add flag $plugin['eEnrollment'] to get simple file with CardID for eEnrolment
 * 2014-04-08 (Carlos) : if $plugin['medical'], add [StatyOverNight] for student type
 * 2013-12-12 (Carlos) : $sys_custom['iMailPlus']['EmailAliasName'] - Added field EmailUserLogin for teacher user type
 * 2013-11-13 (Carlos) : $sys_custom['SupplementarySmartCard'] - Added SmartCardID2 and SmartCardID3 for student user type
 * 2013-08-13 (Carlos) : KIS - hide staff code, WEBSAMS number and JUPAS application number
 * 2013-08-07 (Carlos) : For dynamically get csv sample for import
 */

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if(!isset($TabID) || $TabID == "") {
	header("location: /");
	exit();
}

if($sys_custom['StudentMgmt']['BlissWisdom'] && $TabID == USERTYPE_STUDENT) {
	header("Location: import_bliss_wisdom.php?TabID=$TabID");	
}

### Smartcard
$hasSmartCard = ($plugin['attendancestudent'] || $plugin['payment'] || $plugin['Lunchbox'] || $plugin['eEnrollment']);
$hasTeacherSmartCard = ($module_version['StaffAttendance'] || $plugin['payment'] );

$lexport = new libexporttext();

$isKIS = $_SESSION["platform"] == "KIS";

### download csv / page title
$other_fields = "";
$faxVal = array();
if ($sys_custom['eAdmin']['AcctMgmt']['UseFaxForBaptism'])
{
	if (($TabID == TYPE_STUDENT) || ($TabID == TYPE_TEACHER)) {
		$other_fields .= "_baptism";
	}

	$faxCol = 'DateOfBaptism';	
	$faxVal[] = '2015-02-28';
	$faxVal[] = '2015-03-02';
	$faxVal[] = '2015-03-18';
	$faxVal[] = '';	
}
else
{
	$faxCol = 'Fax';
	for ($i=0; $i<4; $i++) {
		$faxVal[] = '';
	}
}

if($TabID == TYPE_STUDENT && $hasSmartCard) {
	$other_fields .= "_card";
}
if($special_feature['ava_hkid']) {
	$other_fields .= "_hkid";
}
if($special_feature['ava_strn'] && ($TabID == TYPE_STUDENT || $TabID == TYPE_ALUMNI)) {
	$other_fields .= "_strn";
}
$other_fields .= "_unicode";

$rows = array();

switch ($TabID)
{
    case TYPE_TEACHER: 
    	$sample_file = $hasTeacherSmartCard?"eclass_staff_card$other_fields.csv":"eclass_staff$other_fields.csv"; 
    	break;
    case TYPE_STUDENT:
    	$sample_file = "eclass$other_fields.csv";
    	break;
    case TYPE_PARENT:
    	$sample_file = "eclass_parent$other_fields.csv";
    	break;
    case TYPE_ALUMNI: 
		$sample_file = "eclass_alumni.csv";
		break;
    default: 
    	$sample_file = "eclass$other_fields.csv"; 
    	break;
}

if ($TabID == TYPE_PARENT)	# Parent
{
	//$x = "UserLogin,Password,UserEmail,EnglishName,ChineseName,Gender,Mobile,Fax,Barcode,Remarks";
    $exportColumn = array("UserLogin","Password","UserEmail","EnglishName","ChineseName","Gender","Mobile","Fax","Barcode","Remarks");
    $rows = array();
    $rows[0] = array("parent1","a","","parent1","","M","","","111111","123");
    $rows[1] = array("parent2","a","","parent2","陳小麗","F","95687412","","","p2");
    $rows[2] = array("parent3","a","","parent3","李志文","M","98565444","","","p3");	
    $rows[3] = array("parent4","a","","parent4","何釗仁","M","95566321","","","p4");
    $rows[4] = array("parent5","a","","parent5","郭美鳳","F","98756412","","","p5");
    
    if($special_feature['ava_hkid'])
    {
		//$x .= ",HKID";
		$exportColumn = array_merge($exportColumn, array("HKID"));
		$rows[0][] = "A123566(0)";
		$rows[1][] = "B123456(0)";
		$rows[2][] = "C123456(7)";
		$rows[3][] = "";
		$rows[4][] = "";
    }

	//$x .= ",StudentLogin1,StudentEngName1,StudentLogin2,StudentEngName2,StudentLogin3,StudentEngName3";
	$exportColumn = array_merge($exportColumn, array("StudentLogin1","StudentEngName1","StudentLogin2","StudentEngName2","StudentLogin3","StudentEngName3"));
	$rows[0] = array_merge($rows[0], array("henry_s","Henry Student 1","","","",""));
	$rows[1] = array_merge($rows[1], array("henry_s2","Henry Student 2","","","",""));
	$rows[2] = array_merge($rows[2], array("henry_s0","Henry Student 0","","","",""));
	$rows[3] = array_merge($rows[3], array("","","","","",""));
	$rows[4] = array_merge($rows[4], array("","","","","",""));
}
else if ($TabID == TYPE_TEACHER)		# Teacher
{
    $rows = array();
    //$x = "UserLogin,Password,UserEmail,EnglishName,ChineseName,NickName,Gender,Mobile,Fax,Barcode,Remarks,TitleEnglish,TitleChinese,WebSAMSStaffCode";
    
    if ($sys_custom['project']['HKPF'])
    {
        $exportColumn = array("UserLogin","Password","UserEmail","EnglishName","ChineseName","Remarks","Grade","Duty");
        $rows[0] = array("peter","a","peter@xyz.com","Peter Wong","王小明","","Junior","Patrol");
        $rows[1] = array("john","aaa","johnlee@bbb.com","John Lee","李志文","","","");
        $rows[2] = array("superman","b","superman@ccc.com","Ho Chiu Yan","何釗仁","精通越南話","Inspector","Lead a team");
        $rows[3] = array("amy","bb","amykwok@amykwok.com","Amy Kwok","郭美鳳","法律專長","","跟進檢控案件");
    }
    else
    {
     	$exportColumn = array("UserLogin","Password","UserEmail","EnglishName","ChineseName","NickName","Gender","Mobile","$faxCol","Barcode","Remarks","TitleEnglish","TitleChinese");
     	if(!$isKIS) {
     		$exportColumn = array_merge($exportColumn,array("WebSAMSStaffCode"));
     	}

     	$rows[0] = array("peter","a","peter@xyz.com","Peter Wong","王小明","PeterW","M","91203544",$faxVal[0],"2222","1","Principal","校長");
     	$rows[1] = array("john","aaa","johnlee@bbb.com","John Lee","李志文","John","M","98565444",$faxVal[1],"2","","","");
     	$rows[2] = array("superman","b","superman@ccc.com","Ho Chiu Yan","何釗仁","YanYan","M","95566321",$faxVal[2],"3","","","");
     	$rows[3] = array("amy","bb","amykwok@amykwok.com","Amy Kwok","郭美鳳","AMKwok","F","98756412",$faxVal[3],"4","","","");
    
     	if(!$isKIS) {
     		$rows[0] = array_merge($rows[0], array(""));
     		$rows[1] = array_merge($rows[1], array("s101"));
     		$rows[2] = array_merge($rows[2], array(""));
     		$rows[3] = array_merge($rows[3], array("8976543"));
     	}
    }
    
 	if((isset($module_version['StaffAttendance']) && $module_version['StaffAttendance'] != "") || (isset($plugin['payment']) && $plugin['payment']))
	{
		//$x .= ",CardID";
		$exportColumn = array_merge($exportColumn, array("CardID"));
		$rows[0][] = "11122334";
		$rows[1][] = "11345";
		$rows[2][] = "111111";
		$rows[3][] = "";
	}
	if($special_feature['ava_hkid'] && !$sys_custom['project']['HKPF'])
 	{
		//$x .= ",HKID";
		$exportColumn = array_merge($exportColumn, array("HKID"));
		$rows[0][] = "A122456(7)";
		$rows[1][] = "B123356(7)";
		$rows[2][] = "C124456(7)";
		$rows[3][] = "D123446(7)";
    }
    
    if($ssoservice["Google"]["Valid"])
    {
		$exportColumn = array_merge($exportColumn, array("EnableGSuite"));
		$rows[0][] = "Y";
		$rows[1][] = "N";
		$rows[2][] = "Y";
		$rows[3][] = "N";
		
		$exportColumn = array_merge($exportColumn, array("GoogleUserLogin"));
		$rows[0][] = "peter";
		$rows[1][] = "johnlee";
		$rows[2][] = "superman";
		$rows[3][] = "amykwok";
	}
    
    if($sys_custom['BIBA_AccountMgmtCust'])
    {
    	$exportColumn = array_merge($exportColumn, array("House"));
    	$rows[0][] = "紅社";
		$rows[1][] = "藍社";
		$rows[2][] = "";
		$rows[3][] = "";
    }
    
    if($plugin['imail_gamma'] && $sys_custom['iMailPlus']['EmailAliasName'])
    {
    	$exportColumn = array_merge($exportColumn, array("EmailUserLogin"));
    	$rows[0][] = "peter_email";
		$rows[1][] = "johnlee";
		$rows[2][] = "";
		$rows[3][] = "amy_kwok";
    }

}
else if ($TabID == TYPE_STUDENT)		# student
{
    //$x = "UserLogin,Password,UserEmail,EnglishName,ChineseName,NickName,Gender,Mobile,Fax,Barcode,Remarks,DOB,WebSAMSRegNo,HKJApplNo";
 	$exportColumn = array("UserLogin","Password","UserEmail","EnglishName","ChineseName","NickName","Gender","Home Tel","Mobile","$faxCol","Barcode","Remarks","DOB");
 	if(!$isKIS) {
 		$exportColumn = array_merge($exportColumn,array("WebSAMSRegNo","HKJApplNo"));
 	}
 	$exportColumn = array_merge($exportColumn, array("Address"));

	$rows = array();
	$rows[0] = array("peteru","a","peter@zxyz.com","Peter Wong","王小明","PeterW","M","39133000","91203544",$faxVal[0],"","","1988-01-23");
	$rows[1] = array("maryu","aa","marychan@yahoo.com","Mary Chan","陳小麗","Mary","F","39133000","95687412",$faxVal[1],"","","1990-01-24");
	$rows[2] = array("supermanu","b","superman@hotmail.com","Ho Chiu Yan","何釗仁","YanYan","M","39133000","95566321",$faxVal[2],"","","1991-10-09");
	$rows[3] = array("amyu","bb","amykwok@amy-kwok.com","Amy Kwok","郭美鳳","AMKwok","F","39133000","98756412",$faxVal[3],"","","1993-10-22	");

	if(!$isKIS) {
		$rows[0] = array_merge($rows[0], array("#1100186","40000401"));
		$rows[1] = array_merge($rows[1], array("#1012342","40000402"));
		$rows[2] = array_merge($rows[2], array("#9322107","40000403"));
		$rows[3] = array_merge($rows[3], array("#321456","40000404"));
	}

	$rows[0] = array_merge($rows[0], array("Flat 25, 12/F, Acacia Building, 150 Kennedy Road, WAN CHAI, HONG KONG"));
	$rows[1] = array_merge($rows[1], array(""));
	$rows[2] = array_merge($rows[2], array(""));
	$rows[3] = array_merge($rows[3], array(""));

	if((isset($plugin['attendancestudent']) && $plugin['attendancestudent']) || (isset($plugin['payment']) && $plugin['payment']) || (isset($plugin['eEnrollment']) && $plugin['eEnrollment']))
	{
		//$x .= ",CardID";
		$exportColumn = array_merge($exportColumn, array("CardID"));
		if($sys_custom['SupplementarySmartCard']){
			$exportColumn = array_merge($exportColumn, array("CardID2","CardID3","CardID4"));
		}

		$rows[0][] = "0000287792";
		if($sys_custom['SupplementarySmartCard']){
			$rows[0][] = "0000287793";
			$rows[0][] = "0000287794";
			$rows[0][] = "0000287795";
		}
		$rows[1][] = "0000025394";
		if($sys_custom['SupplementarySmartCard']){
			$rows[1][] = "";
			$rows[1][] = "";
			$rows[1][] = "";
		}
		$rows[2][] = "0000289038";
		if($sys_custom['SupplementarySmartCard']){
			$rows[2][] = "0000289039";
			$rows[2][] = "";
			$rows[2][] = "";
		}
		$rows[3][] = "0000853912";
		if($sys_custom['SupplementarySmartCard']){
			$rows[3][] = "";
			$rows[3][] = "";
			$rows[3][] = "";
		}
	}

 	if($special_feature['ava_hkid'])
 	{
		//$x .= ",HKID";
		$exportColumn = array_merge($exportColumn, array("HKID"));
		$rows[0][] = "A103456(7)";
		$rows[1][] = "B1723456(7)";
		$rows[2][] = "C1234586(7)";
		$rows[3][] = "D123056(7)";
 	}
 
 	if($special_feature['ava_strn'])
 	{
		//$x .= ",STRN";
		$exportColumn = array_merge($exportColumn, array("STRN"));
		$rows[0][] = "65498765";
		$rows[1][] = "54312159";
		$rows[2][] = "89120540";
		$rows[3][] = "50456480";
 	}
    
    if($ssoservice["Google"]["Valid"] && $ssoservice["Google"]['mode']['student']){
		$exportColumn = array_merge($exportColumn, array("EnableGSuite"));
		$rows[0][] = "Y";
		$rows[1][] = "N";
		$rows[2][] = "Y";
		$rows[3][] = "N";
	}
 	
 	 if($sys_custom['BIBA_AccountMgmtCust'])
 	 {
 		$exportColumn = array_merge($exportColumn, array("HouseholdRegister", "House"));
 		$rows[0][] = "廣東";
		$rows[1][] = "北京";
		$rows[2][] = "福建";
		$rows[3][] = "湖南";
 		$rows[0][] = "紅社";
		$rows[1][] = "藍社";
		$rows[2][] = "黃社";
		$rows[3][] = "綠社";
 	}
 	
 	if($sys_custom['StudentAccountAdditionalFields'])
 	{
 		$exportColumn = array_merge($exportColumn, array("Non-Chinese Speaking"));
 		$rows[0][] = "N";
		$rows[1][] = "Y";
		$rows[2][] = "N";
		$rows[3][] = "N";
 		
 		$exportColumn = array_merge($exportColumn, array("Special Education Needs"));
 		$rows[0][] = "Y";
		$rows[1][] = "N";
		$rows[2][] = "Y";
		$rows[3][] = "N";
		
		$exportColumn = array_merge($exportColumn, array("Primary School"));
 		$rows[0][] = "Hong Kong Primary School";
		$rows[1][] = "Kowloon Primary School";
		$rows[2][] = "NT Primary School";
		$rows[3][] = "New Education Primary School";
		
		$exportColumn = array_merge($exportColumn, array("University/Institution"));
 		$rows[0][] = "The University of Hong Kong";
		$rows[1][] = "The Chinese University of Hong Kong";
		$rows[2][] = "Vocational Training Council";
		$rows[3][] = "Hong Kong Institute of Vocational Education";
		
		$exportColumn = array_merge($exportColumn, array("Programme"));
 		$rows[0][] = "";
		$rows[1][] = "";
		$rows[2][] = "VTC";
		$rows[3][] = "IVE";
 	}
 	
 	//$x .="Nationality, PlaceOfBirth, AdmissionDate";
 	if($plugin['medical'])
 	{
 		$exportColumn = array_merge($exportColumn, array("StayOverNight"));
 		$rows[0][] = "Y";
		$rows[1][] = "";
		$rows[2][] = "";
		$rows[3][] = "";
 	}

 	// [2020-0220-1623-28098]
 	$exportColumn = array_merge($exportColumn, array("Nationality", "PlaceOfBirth", "AdmissionDate", "GraduationDate"));
 	$rows[0] = array_merge($rows[0], array("Chinese","Hong Kong","1992-09-01","2011-07-15"));
 	$rows[1] = array_merge($rows[1], array("","","",""));
 	$rows[2] = array_merge($rows[2], array("","","",""));
 	$rows[3] = array_merge($rows[3], array("","","",""));
 	
 	if($plugin['SDAS_module']['KISMode']){
 		$exportColumn = array_merge($exportColumn, array("KISClassType"));
 		$rows[0][] = "1";
 		$rows[1][] = "3";
 		$rows[2][] = "4";
 		$rows[3][] = "5";
 	}
 	if($plugin['StudentDataAnalysisSystem_Style'] == "tungwah")
 	{
 	    $exportColumn = array_merge($exportColumn, array("PrimarySchoolCode"));
 	    $rows[0][] = "123123";
 	    $rows[1][] = "111111";
 	    $rows[2][] = "";
 	    $rows[3][] = "";
 	}
}
else	# alumni
{
    //$x = "UserLogin,Password,UserEmail,EnglishName,ChineseName,Nickname,Gender,Mobile,Fax,Barcode,Remarks,ClassName,ClassNumber,YearOfLeft";
 	$exportColumn = array("UserLogin","Password","UserEmail","EnglishName","ChineseName","Nickname","Gender","Mobile","Fax","Barcode","Remarks","ClassName","ClassNumber","YearOfLeft");
 	$rows = array();
 	$rows[0] = array("peter","ab","peter@xyz123.com","Peter Wong","王小明","Peter","M","91203544","211111111","","Remarks fields","4A","1","2004");
 	/*
 	if($special_feature['ava_strn']){
 		$exportColumn = array_merge($exportColumn, array("STRN"));
 		$rows[0][] = "";
 	}
 	*/
}

$export_content = $lexport->GET_EXPORT_TXT($rows, $exportColumn);	
$filename = $sample_file;
$lexport->EXPORT_FILE($filename, $export_content);

intranet_closedb();
?>