<?php
// using : 

/*
 *  2020-03-31 Cameron
 *      - don't show $i_SmartCard_CardID for standard LMS
 *      
 *  2018-11-12 Cameron
 *      - consolidate Amway, Oaks, CEM to use the same customized flag control ($sys_custom['project']['CourseTraining']['IsEnable'])
 *  
 * 	2017-12-22 Cameron
 * 		- configure for Oaks refer to Amways
 * 
 * 	2017-10-26 Cameron
 * 		- create this file
 */

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
intranet_auth();
intranet_opendb();

if(!isset($TabID) || $TabID=="") {
	header("location: /");
	exit();
}

if (!$sys_custom['project']['CourseTraining']['IsEnable']) {
	header("location: /");
	exit();
}

$linterface = new interface_html();
$laccount = new libaccountmgmt();

### smartcard

### download csv / page title

$compulsoryField = array(1,1,0,1,0,1,0,0,0,0,0);
if ($sys_custom['project']['CourseTraining']['StandardLMS']) {
    $other_fields = "_standard_lms";
}
else if ($sys_custom['project']['CourseTraining']['Oaks']) {
    $other_fields = "_oaks";
    $compulsoryField[] = 0;     // $i_SmartCard_CardID
}
else {
    $other_fields = "_amway";
    $compulsoryField[] = 0;     // $i_SmartCard_CardID
}

$csv_format = "";
$delim = "";

$Title = $Lang['AccountMgmt']['ImportStudentAccount'];
$CurrentPageArr['StudentMgmt'] = 1; 
$CurrentPage = "Mgmt_Account";
$MODULE_OBJ = $laccount->GET_MODULE_OBJ_ARR();
$back_url = "../StudentMgmt/index.php"; 
$sample_file = GET_CSV("eclass$other_fields.csv");
$referenceAry = $Lang['AccountMgmt']['StudentImportFields']; 
//if($ssoservice["Google"]["Valid"] && $ssoservice["Google"]['mode']['student']){
//	foreach((array)$Lang['AccountMgmt']['GoogleImportFields'] as $google_field){
//		array_push($referenceAry, $google_field);
//		array_push($compulsoryField, 0);
//	}
//}

$col_i = 0;
for($i=0; $i<sizeof($referenceAry); $i++){
	
	$csv_format .= $delim.$Lang['SysMgr']['Homework']['Column']." ".($col_i+1)." : ";
	$csv_format .= ($compulsoryField[$i]) ? "<font color=red>*</font>" : "";
	$csv_format .= $referenceAry[$i];
	if($i==2) {
		$csv_format .= " <span class=\"tabletextremark\">".$Lang['AccountMgmt']['EmailFieldReminder']."</span>";
	}
	$delim = "<br>";
	$col_i++;
}

$csvFile = "<a class='tablelink' href='". $sample_file ."'>". $Lang['General']['ClickHereToDownloadSample'] ."</a>";
$BackBtn = $linterface->GET_ACTION_BTN($button_back, "button", "window.location='$back_url'","back_btn"," class='formbutton' "); 


### Import Remarks
$ImportRemarkArr = $Lang['AccountMgmt']['ImportRemarks'];

$ImportRemark = '<table cellpadding=0 cellspacing=0 border=0>';	
for($i=0; $i < count($ImportRemarkArr); $i++)
	$ImportRemark .= '<tr><td valign=top width=20px class="tabletext">'.($i+1).'. </td><td>'.$ImportRemarkArr[$i].'</td></tr>';	 
$ImportRemark .= '</table>';

### Title ###
$PAGE_NAVIGATION[] = array($Title,"");

$TAGS_OBJ[] = array($Lang['Group']['UserList'], "", 0);    

$STEPS_OBJ[] = array($i_general_select_csv_file, 1);
$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 0);
$STEPS_OBJ[] = array($i_general_imported_result, 0);

$MODULE_OBJ = $laccount->GET_MODULE_OBJ_ARR();


$linterface->LAYOUT_START();
?>

<form method="POST" name="frm1" id="frm1" action="import_check_amway.php" enctype="multipart/form-data" onsubmit="return CheckForm()">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td colspan="2" align='right'><?=$linterface->GET_SYS_MSG("",$xmsg);?></td>
</tr>
<tr>
	<td colspan="2"><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
</tr>
<tr>
	<td colspan="2"><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
</tr>
<tr>
	<td colspan="2">
        <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
            <tr> 
            	<td><br />
					<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">			
						<tr>
							<td class="formfieldtitle" align="left"><?=$Lang['General']['SourceFile']?> <span class="tabletextremark"><?=$Lang['General']['CSVFileFormat']?></span></td>
							<td class="tabletext"><input class="file" type="file"  id = "userfile" name="userfile"></td>
						</tr>
					<?php
					if($TabID == USERTYPE_STUDENT){
					?>
						<tr>
							<td class="formfieldtitle" align="left"><?=$Lang['AccountMgmt']['ActionType']?></td>
							<td class="tabletext">
								<?=$linterface->Get_Radio_Button("action_type1", "action_type", "1", 1, "", $Lang['AccountMgmt']['ActionType.Add']).'&nbsp;'?>
								<?=$linterface->Get_Radio_Button("action_type2", "action_type", "2", 0, "", $Lang['AccountMgmt']['ActionType.Update'])?>
							</td>
						</tr>
					<?php
					} 
					?>
						<tr>
							<td class="formfieldtitle" align="left"><?=$Lang['AccountMgmt']['Remarks']?> </td>
							<td class="tabletext"><?=$ImportRemark?></td>
						</tr>
						<tr>
							<td class="formfieldtitle" align="left"><?=$Lang['General']['CSVSample']?> </td>
							<td class="tabletext"><?=$csvFile?></td>
						</tr>
						<tr>
							<td class="formfieldtitle" align="left"><?=$Lang['SysMgr']['Homework']['Import']['FieldTitle']['DataColumn']?></td>
							<td class="tabletext"><?=$csv_format?></td>
						</tr>
					</table>
					<span class="tabletextremark"><?=$i_general_required_field?></span>
				</td>
	        </tr>
	    </table>
	</td>
</tr>
<tr>
	<td colspan="2">        
		<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
			<tr>
                <td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
            </tr>
            <tr>
				<td align="center">
					<?= $linterface->GET_ACTION_BTN($button_import, "submit", "","submit2"," class='formbutton'  onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
					&nbsp;
					<?= $BackBtn ?>				
				</td>
			</tr>
        </table>                                
	</td>
</tr>
</table>  
<input name="TabID" value="<?=$TabID?>" type="hidden">
</form>
<br><br>
<script>
function CheckForm()
{
	var filename = $("#userfile").val()
	var fileext = filename.substring(filename.lastIndexOf(".")).toLowerCase()
	if(fileext!=".csv" && fileext!=".txt")
	{	
		alert("<?=$Lang['AccountMgmt']['WarnMsg']['PleaseSelectCSVorTXT']?>");
		return false;
	}
	return true;	
}
</script>
<?
 $linterface->LAYOUT_STOP();
?>

