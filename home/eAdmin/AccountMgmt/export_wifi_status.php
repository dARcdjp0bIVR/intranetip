<?php
// Editing by 
/*
 * 2015-12-30 (Carlos): $plugin['radius_server'] - export Wi-Fi access status user list.
 */
$PATH_WRT_ROOT = "../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_opendb();

if(!$plugin['radius_server'] || !isset($TabID) || !in_array($TabID,array(USERTYPE_STAFF,USERTYPE_STUDENT,USERTYPE_PARENT,USERTYPE_ALUMNI))){
	No_Access_Right_Pop_Up();
}
if(!($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"] || 
	($TabID==USERTYPE_STAFF && $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Staff"]) || 
	($TabID==USERTYPE_STUDENT && $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Student"]) || 
	($TabID==USERTYPE_PARENT &&  $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Parent"]) || 
	$_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"])) 
{
	No_Access_Right_Pop_Up();
}

$lexport = new libexporttext();
$laccount = new libaccountmgmt();

$order_by_field = Get_Lang_Selection("u.ChineseName","u.EnglishName");
if($TabID==USERTYPE_STAFF){
	$eng_name_field = "u.EnglishName";
	$chi_name_field = "u.ChineseName";
}else if($TabID==USERTYPE_STUDENT){
	$eng_name_field = "CONCAT(u.EnglishName,IF(TRIM(u.ClassName)<>'',CONCAT('(',u.ClassName,'#',u.ClassNumber,')'),''))";
	$chi_name_field = "CONCAT(u.ChineseName,IF(TRIM(u.ClassName)<>'',CONCAT('(',u.ClassName,'#',u.ClassNumber,')'),''))";
	$order_by_field = "u.ClassName,u.ClassNumber+0,".Get_Lang_Selection("u.ChineseName","u.EnglishName");
}else if($TabID==USERTYPE_PARENT){
	$eng_name_field = "CONCAT(u.EnglishName,IF(u2.UserID IS NOT NULL,CONCAT('(',u2.EnglishName,' - ',u2.ClassName,'#',u2.ClassNumber,')'),''))";
	$chi_name_field = "CONCAT(u.ChineseName,IF(u2.UserID IS NOT NULL,CONCAT('(',u2.ChineseName,' - ',u2.ClassName,'#',u2.ClassNumber,')'),''))";
	$student_user_table = "LEFT JOIN INTRANET_PARENTRELATION as r ON r.ParentID=u.UserID 
							LEFT JOIN INTRANET_USER as u2 ON u2.UserID=r.StudentID ";
}else if($TabID==USERTYPE_ALUMNI){
	$eng_name_field = "CONCAT(u.EnglishName,IF(TRIM(u.YearOfLeft)<>'',CONCAT('(',u.YearOfLeft,')'),''))";
	$chi_name_field = "CONCAT(u.ChineseName,IF(TRIM(u.YearOfLeft)<>'',CONCAT('(',u.YearOfLeft,')'),''))";
	$order_by_field = "u.YearOfLeft,".Get_Lang_Selection("u.ChineseName","u.EnglishName");
}

$sql = "SELECT 
			u.UserLogin as '0', 
			$eng_name_field as '1',
			$chi_name_field as '2',
			'".$Lang['General']['Yes']."' as '3' 
		FROM INTRANET_USER as u 
		$student_user_table 
		WHERE u.RecordType='$TabID' 
		ORDER BY ".$order_by_field;
$records = $laccount->returnResultSet($sql);
$record_count = count($records);

$userlogin_ary = Get_Array_By_Key($records, '0');
$disabled_userlogins = $laccount->isUserDisabledInRadiusServer($userlogin_ary);
for($i=0;$i<$record_count;$i++){
	if($disabled_userlogins[$records[$i]['0']]){
		$records[$i]['3'] = $Lang['General']['No'];
	}
}

$headers = array("Login ID","English Name","Chinese Name","Enable Wi-Fi Access");
$rows = array();
$filename = "wifi_usage.csv";

$export_content = $lexport->GET_EXPORT_TXT($records, $headers);
$lexport->EXPORT_FILE($filename, $export_content);

intranet_closedb();
?>