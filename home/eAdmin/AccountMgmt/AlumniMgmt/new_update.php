<?php
# using: 

############# Change Log
#	Date:	2019-06-24 Carlos
#			Empty UserPassword field.
#
#	Date:	2019-05-13 (Henry) Security fix: SQL without quote
#
#	Date:	2018-01-03 (Carlos) [ip.2.5.9.1.1]: Improved password checking.
#
#	Date:	2016-05-18 (Carlos)
#			$sys_custom['DisableEmailAfterCreatedNewAccount'] - disable sending notification email after created new user account.
#
#	Date:	2015-12-30 (Carlos)
#			$plugin['radius_server'] - added Wi-Fi access option.
#
#	Date:	2015-05-26 (Carlos)
#			added libaccountmgmt.php suspendEmailFunctions($targetUserId, $resume=false) to resume or suspend mail server auto forward and auto reply functions.
#
#	Date:	2014-09-03 (Carlos) - iMail plus: Block receive/send internet mails when [iMail Settings > Default Usage Rights and Quota > Internal mail only] is on
#
#	Date:	2014-05-29 (Carlos) - only send notification email if recipient email is valid
#
#	Date:	2013-03-15	Carlos
#			add shared group calendars to user
#
#	Date:	2012-08-30	YatWoon
#			add barcode
#
#	Date:	2012-08-01	Ivan
#			added logic to syn user info to library system
#
#################
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libauth.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libregistry.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."includes/libemail.php");
include_once($PATH_WRT_ROOT."lang/email.php");
include_once($PATH_WRT_ROOT."includes/libftp.php");
include_once($PATH_WRT_ROOT."includes/libldap.php");
include_once($PATH_WRT_ROOT."includes/imap_gamma.php");
include_once($PATH_WRT_ROOT."includes/libeclass40.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/icalendar.php");
intranet_auth();
intranet_opendb();


if(!($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"])) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

if(sizeof($_POST)==0) {
	header("Location: new.php");	
	exit;
}
$engname = intranet_htmlspecialchars(stripslashes($engname));
$chiname = intranet_htmlspecialchars(stripslashes($chiname));

$lauth = new libauth();
$laccount = new libaccountmgmt();
$li = new libregistry();
$lwebmail = new libwebmail();
$le = new libeclass();
$lu = new libuser();
$lcalendar = new icalendar();

$SettingArr = $lu->RetrieveUserInfoSettingInArrary(array('EnablePasswordPolicy_'.USERTYPE_ALUMNI));
$PasswordLength = $SettingArr['EnablePasswordPolicy_'.USERTYPE_ALUMNI];
if ($PasswordLength<6) $PasswordLength = 6;

// $CurrentPageArr['StaffMgmt'] = 1;
//$CurrentPage = "User List";

// $TAGS_OBJ[] = array($Lang['Group']['UserList']);
// $MODULE_OBJ['title'] = $Lang['Header']['Menu']['StaffAccount'];

// # navigation bar
// $PAGE_NAVIGATION[] = array($Lang['AccountMgmt']['UserList'], "javascript:goURL('index.php')");
// $PAGE_NAVIGATION[] = array($Lang['AccountMgmt']['NewUser'], "");

## check User Login duplication ###
if($userlogin != ""){
	$sql = "SELECT UserID FROM INTRANET_USER WHERE UserLogin='$userlogin'";
	$temp = $laccount->returnVector($sql);
	if(sizeof($temp)!=0){
		$error_msg[] = $userlogin;
	}
}

if($sys_custom['UseStrongPassword']){
	$check_password_result = $lauth->CheckPasswordCriteria($pwd,$userlogin,$PasswordLength);
	if(!in_array(1,$check_password_result)){
		$error_msg[] = $pwd;
	}
}

## check cardid duplication ###
if($CardID!=""){
	$sql="SELECT UserID FROM INTRANET_USER WHERE CardID='$CardID'";
	$temp = $li->returnVector($sql);
	if(sizeof($temp)!=0){
		$error_msg ="Smart Card ID has been used by other user.";
	}
}

if(sizeof($error_msg)>0) {
	$action = "new.php";
	$errorList = implode(',', $error_msg);	
}



if(sizeof($error_msg)>0) { 
?>

	<body onload="document.form1.submit()">
	<form name="form1" method="post" action="new.php">
		<input type="hidden" name="recordstatus" id="recordstatus" value="<?=$recordstatus?>">
		<input type="hidden" name="userlogin" id="userlogin" value="<?=$userlogin?>">
		<input type="hidden" name="pwd" id="pwd" value="<?=$pwd?>">
		<input type="hidden" name="email" id="email" value="<?=$email?>">
		<input type="hidden" name="status" id="status" value="<?=$status?>">
		<input type="hidden" name="barcode" id="barcode" value="<?=$barcode?>">
		<input type="hidden" name="engname" id="engname" value="<?=$engname?>">
		<input type="hidden" name="chiname" id="chiname" value="<?=$chiname?>">
		<input type="hidden" name="nickname" id="nickname" value="<?=$nickname?>">
		<input type="hidden" name="gender" id="gender" value="<?=$gender?>">
		<input type="hidden" name="engTitle" id="engTitle" value="<?=$engTitle?>">
		<input type="hidden" name="chiTitle" id="chiTitle" value="<?=$chiTitle?>">
		<input type="hidden" name="address" id="address" value="<?=$address?>">
		<input type="hidden" name="country" id="country" value="<?=$country?>">
		<input type="hidden" name="homePhone" id="homePhone" value="<?=$homePhone?>">
		<input type="hidden" name="officePhone" id="officePhone" value="<?=$officePhone?>">
		<input type="hidden" name="mobilePhone" id="mobilePhone" value="<?=$mobilePhone?>">
		<input type="hidden" name="faxPhone" id="faxPhone" value="<?=$faxPhone?>">
		<input type="hidden" name="open_webmail" id="open_webmail" value="<?=$open_webmail?>">
		<input type="hidden" name="error" id="error" value="<?=$errorList?>">
		<input type="hidden" name="remark" id="remark" value="<?=$remark?>">
		<input type="hidden" name="classname" id="classname" value="<?=$classname?>">
		<input type="hidden" name="classnumber" id="classnumber" value="<?=$classnumber?>">
		<input type="hidden" name="yearofleft" id="yearofleft" value="<?=$yearofleft?>">
		<?php if($plugin['radius_server']){ ?>
		<input type="hidden" name="enable_wifi_access" id="enable_wifi_access" value="<?=$enable_wifi_access?>" />
		<?php } ?>
	</form>
	<script language="javascript">
		document.form1.submit();
	</script>
	</body>
<? } else { 
		
	$domain_name = ($intranet_email_generation_domain==""? $_SERVER["SERVER_NAME"]:$intranet_email_generation_domain);
	$target_email = ($email=="" ? $userlogin . "@".$domain_name : $email);
	
	if($li->UserNewAdd($userlogin, $target_email, trim($engname), trim($chiname), $status)==1) {
		
		# create & update info in INTRANET_USER
		
		$uid = $li->db_insert_id();
		$UserPassword = $pwd;
		if ($intranet_authentication_method=="HASH") {
		    $fieldname .= "UserPassword = NULL,HashedPass = MD5('$userlogin$UserPassword$intranet_password_salt'),";
		} else {
		    $fieldname .= "UserPassword = NULL, ";
		}
		$fieldname .= "NickName = '".intranet_htmlspecialchars(trim($nickname))."', ";
		//$fieldname .= "Title = '".intranet_htmlspecialchars(trim($Title))."', ";
		$fieldname .= "Gender = '$gender', ";
		$fieldname .= "HomeTelNo = '".intranet_htmlspecialchars(trim($homePhone))."', ";
		$fieldname .= "OfficeTelNo = '".intranet_htmlspecialchars(trim($officePhone))."', ";
		$fieldname .= "MobileTelNo = '".intranet_htmlspecialchars(trim($mobilePhone))."', ";
		$fieldname .= "FaxNo = '".intranet_htmlspecialchars(trim($faxPhone))."', ";
		$fieldname .= "Address = '".intranet_htmlspecialchars(trim($address))."', ";
		$fieldname .= "Country = '".intranet_htmlspecialchars(trim($country))."', ";
		$fieldname .= "Remark = '".intranet_htmlspecialchars(trim($remark))."', ";
		$fieldname .= "ClassName = '".intranet_htmlspecialchars(trim($classname))."', ";
		$fieldname .= "ClassNumber = '".intranet_htmlspecialchars(trim($classnumber))."', ";
		$fieldname .= "YearOfLeft = '".intranet_htmlspecialchars(trim($yearofleft))."', ";
		$fieldname .= "Barcode = '".intranet_htmlspecialchars(trim($barcode))."', ";
		$fieldname .= "RecordType = '".USERTYPE_ALUMNI."'";
		/*
		$fieldname .= "FaxNo = '$FaxNo', ";
		$fieldname .= "ICQNo = '$ICQNo', ";
		$fieldname .= "Info = '$Info', ";
		$fieldname .= "Remark = '$Remark', ";
		$fieldname .= "DateOfBirth = '$DateOfBirth', ";
		$fieldname .= "ClassNumber = '$ClassNumber' ";
		$fieldname .= ",YearOfLeft = '$YearOfLeft'";
		*/
	
		$TitleEnglish = intranet_htmlspecialchars(trim($engTitle));
		$TitleChinese = intranet_htmlspecialchars(trim($chiTitle));
		$fieldname .= ",TitleEnglish = '$TitleEnglish'";
		$fieldname .= ",TitleChinese = '$TitleChinese'";
		
		$fieldname .= ", DateModified = now()";
		$sql = "UPDATE INTRANET_USER SET $fieldname WHERE UserID = $uid";
		$li->db_db_query($sql);
		
		# insert into INTRANET_USERGROUP (assign to alumni group)
		$sql = "INSERT INTO INTRANET_USERGROUP (GroupID, UserID, DateInput, DateModified) VALUES ($alumni_GroupID, $uid, now(), now())";
		$li->db_db_query($sql);
				
		
		# add to corresponding alumni group according to YearOfLeft
		$lu->AddOrUpdateAlumniYearGroup($uid, trim($yearofleft));
		
		$msg = 1;

		
		# insert group calendar
		//if (!empty($GroupID)){
			$cal_sql = "
				insert into CALENDAR_CALENDAR_VIEWER 
				(CalID, UserID, GroupID, GroupType, Access, Color, Visible)
				select g.CalID, '$uid', g.GroupID, 'E', 'R', '2f75e9', '1' 
				from INTRANET_GROUP as g inner join INTRANET_USERGROUP as u 
				on g.GroupID = u.GroupID and u.UserID = $uid
			"; //echo $cal_sql.'<br><br>';
			$li->db_db_query($cal_sql);
		//}
		# insert calendar viewer to calendars that have shared to the user's groups
		$cal_group_sql = "SELECT g.GroupID FROM INTRANET_GROUP as g 
							INNER JOIN INTRANET_USERGROUP as u ON u.GroupID=g.GroupID 
							WHERE u.UserID='$uid'";
		$cal_group_ary = $li->returnVector($cal_group_sql);
		for($i=0;$i<sizeof($cal_group_ary);$i++) {
			$lcalendar->addCalendarViewerToGroup($cal_group_ary[$i],$uid);
		}
		
// 		#### Special handling
// 		$li->UpdateRole_UserGroup();
		
		$acl_field = 0;
		/*
		# iFolder
		if ($open_file && in_array(TYPE_TEACHER, $personalfile_identity_allowed) )
		{
		    if ($personalfile_type=='FTP')
		    {
		        $lftp = new libftp();
		        if ($lftp->isAccountManageable)
		        {
		            $file_failed = ($lftp->open_account($userlogin,$UserPassword)? 0:1);
		            $file_content = get_file_content($intranet_root."/file/account_file_quota.txt");
		            if ($file_content == "")
		            {
		                $userquota = array(10,10,10);
		            }
		            else
		            {
		                $userquota = explode("\n", $file_content);
		            }
		            $target_quota = $userquota[TYPE_TEACHER-1];
		            $lftp->setTotalQuota($userlogin, $target_quota, "iFolder");
		        }
		    }
		    $acl_field += 2;
		}
		*/
		
		# gamma mail
		if($plugin['imail_gamma'])
		{
			if($EmailStatus=='enable')
			{
				$IMapEmail = trim($userlogin)."@".$SYS_CONFIG['Mail']['UserNameSubfix'];
				$IMap = new imap_gamma(1);
				if($IMap->open_account($IMapEmail,$pwd))
				{
					$laccount->setIMapUserEmail($uid,$IMapEmail);
					$IMap->SetTotalQuota($IMapEmail,$Quota,$uid);
					
					// Block receive/send internet mails when [iMail Settings > Default Usage Rights and Quota > Internal mail only] is on
					$internal_only = $IMap->internal_mail_only; // index 0 is staff, 1 is student, 2 is parent, 3 is alumni
					if($internal_only[USERTYPE_ALUMNI-1]){ // shift index by one
						$IMap->addGroupBlockExternal(array($IMapEmail));
						//$sql = "UPDATE INTRANET_SYSTEM_ACCESS SET ACL = ACL - 1 WHERE UserID = '$uid' AND ACL IN (1,3)";
				    	//$li->db_db_query($sql);
				    	$acl_field -= 1; // offset the following $acl_field += 1
					}
				}
			}
			$acl_field += 1;
		}
		else
		{
			# Webmail
			if ($open_webmail && in_array(TYPE_TEACHER, $webmail_identity_allowed))
			{
				
				if ($lwebmail->has_webmail) {
					$lwebmail->open_account($userlogin, $UserPassword);
					$acl_field += 1;
					$file_content = get_file_content($intranet_root."/file/account_mail_quota.txt");
					if ($file_content == "") {
						$userquota = array(10,10,10);
					}
					else {
						$userquota = explode("\n", $file_content);
					}
					$target_quota = $userquota[TYPE_TEACHER-1];
					$lwebmail->setTotalQuota($userlogin, $target_quota, "iMail");
				}
			}
			
			# iMail Quota
			if ($special_feature['imail'])
			{
			    # Get Default Quota
			    $file_content = get_file_content($intranet_root."/file/campusmail_set.txt");
			    if ($file_content == "")
			    {
			        $quota = 10;
			    }
			    else
			    {
			        $userquota = explode("\n", $file_content);
		            $quotaline = $userquota[TYPE_TEACHER-1];
			        $temp = explode(":",$quotaline);
			        $quota = $temp[1];
			    }
			    $sql = "INSERT INTO INTRANET_CAMPUSMAIL_USERQUOTA (UserID, Quota) VALUES ('$uid', '$quota')";
			    $li->db_db_query($sql);
			    
			    $sql = "INSERT INTO INTRANET_CAMPUSMAIL_USED_STORAGE (UserID, QuotaUsed) VALUES ('$uid', 0)";
			    $li->db_db_query($sql);
			}
		}
		
		$sql = "INSERT INTO INTRANET_SYSTEM_ACCESS (UserID, ACL) VALUES ($uid, $acl_field)";
		$li->db_db_query($sql);
		
		$laccount->suspendEmailFunctions($uid, $status == 1);
		
		# Call sendmail function (Email notification of registration)
		list($mailSubject,$mailBody) = $laccount->returnEmailNotificationData($email, $userlogin, $UserPassword, stripslashes($engname), stripslashes($chiname));
		include_once($PATH_WRT_ROOT."includes/libsendmail.php");
		$lsendmail = new libsendmail();
		$webmaster = get_webmaster();
		$headers = "From: $webmaster\r\n";
		if(!$sys_custom['DisableEmailAfterCreatedNewAccount'] && $email != "" && intranet_validateEmail($email,true)){
			$lsendmail->SendMail($email, $mailSubject, $mailBody,"$headers");
		}
				
		# LDAP
		if ($intranet_authentication_method=='LDAP')
		{
		    $lldap = new libldap();
		    if ($lldap->isAccountControlNeeded()) {
		        $lldap->connect();
		        $lldap->openAccount($userlogin, $UserPassword);
		    }
		}
		
		/*
		# Payment
		if($plugin['payment']) {
		    $sql = "INSERT INTO PAYMENT_ACCOUNT (StudentID, Balance) VALUES ('$uid',0)";
		    $li->db_db_query($sql);
		}
		*/
		
		# SchoolNet integration
		if ($plugin['SchoolNet'])
		{
		    include_once($PATH_WRT_ROOT."includes/libschoolnet.php");
		    $lschoolnet = new libschoolnet();
	        # Param: array in ($userlogin, $password, $DOB, $gender, $cname, $ename, $tel, $mobile, $address, $email, $teaching)
	        $sql = "SELECT UserLogin, UserPassword, DATE_FORMAT(DateOfBirth,'%Y-%m-%d'), Gender, Chinesename, EnglishName, HomeTelNo, MobileTelNo, Address, UserEmail, Teaching FROM INTRANET_USER WHERE UserID = '".$uid."'";
	        $data = $li->returnArray($sql, 11);
	        $lschoolnet->addStaffUser($data);
		}
		
		$successAry['synUserDataToModules'] = $lu->synUserDataToModules($uid);
		
		if($plugin['radius_server']){
			$laccount->disableUserInRadiusServer(array($uid), !$enable_wifi_access);
		}
		
		$flag = "AddSuccess";
	} else {
		$flag = "AddUnsuccess";	
	}

// 	$le->addTitleToCourseUser($target_email,$TitleEnglish,$TitleChinese);
	intranet_closedb();
	
	header("Location: index.php?xmsg=$flag");
}
?>