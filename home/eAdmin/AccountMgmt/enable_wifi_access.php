<?php

/*
 * 2015-12-30 (Carlos): Created. 
 */
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");

intranet_auth();
intranet_opendb();

if(!($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"]) || !$plugin['radius_server']) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

if(sizeof($user_id)==0) {
	header("Location : index.php");	
	exit;
}

$laccount = new libaccountmgmt();
$success = $laccount->disableUserInRadiusServer($user_id, $____disable=false);

intranet_closedb();

if($success)
	$xmsg = "UpdateSuccess";
else 
	$xmsg = "UpdateUnsuccess";
	
$comeFrom = $comeFrom ? $comeFrom : "index.php";	

header("Location: $comeFrom?&xmsg=$xmsg");
?>