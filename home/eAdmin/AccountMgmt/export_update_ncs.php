<?php
// using:
/**
 * 2018-04-13 Pun
 * - File created
 */
$PATH_WRT_ROOT = "../../../";

include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "includes/libdbdump.php");
include_once ($PATH_WRT_ROOT . "includes/libfilesystem.php");

include_once ($PATH_WRT_ROOT . "includes/libexporttext.php");
include_once ($PATH_WRT_ROOT . "includes/libgroup.php");

intranet_opendb();

$isKIS = $_SESSION["platform"] == "KIS";

$lexport = new libexporttext();
$li = new libdb();
if ($default == 1) // Export default format
{
    if ($TabID == "") {
        $tab_conds = "";
    } else {
        $tab_conds = " U.RecordType = $TabID AND ";
    }
    if ($recordstatus != "")
        $tab_conds .= " U.RecordStatus='$recordstatus' AND ";
    
    if ($TeachingType != "")
        $tab_conds .= ($TeachingType == 1 || $TeachingType == 'S') ? " U.Teaching='$TeachingType' AND " : " (U.Teaching='0' OR U.Teaching IS NULL) AND ";
    
    if ($targetClass != "0" && $targetClass != '') {
        include_once ($PATH_WRT_ROOT . "includes/libclass.php");
        $lclass = new libclass();
        $studentAry = $lclass->storeStudent(0, $targetClass);
        
        if (sizeof($studentAry) > 0) {
            if ($TabID == 3)
                $studentConds .= " b.StudentID IN (" . implode(',', $studentAry) . ") AND ";
            else
                $tab_conds .= " U.UserID IN (" . implode(',', $studentAry) . ") AND ";
        } else {
            if ($TabID == 3)
                $studentConds .= " b.StudentID IN (-1) AND ";
            else
                $tab_conds .= " U.UserID IN (-1) AND ";
        }
    }
    
    // student
    if ($TabID == 2) {
        /*
         * if(!isset($notInList) || $notInList=="") { # only export User List
         * $li2 = new libdb();
         * $sql = "SELECT UserID FROM YEAR_CLASS_USER ycu LEFT OUTER JOIN YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID) WHERE yc.AcademicYearID=".Get_Current_Academic_Year_ID();
         * $temp = $li2->returnVector($sql);
         * if(sizeof($temp)>0) $tab_conds .= " UserID IN (".implode(',',$temp).") AND ";
         * }
         */
        if ($userList && $notInList) { // all students
                                           // no addition condition
        } else {
            
            $sql = "SELECT UserID FROM YEAR_CLASS_USER ycu LEFT OUTER JOIN YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID) WHERE yc.AcademicYearID=" . Get_Current_Academic_Year_ID();
            $temp = $li->returnVector($sql);
            
            if ($userList) { // only export user list (with classes)
                if (sizeof($temp) > 0)
                    $tab_conds .= " U.UserID IN (" . implode(',', $temp) . ") AND ";
            } else 
                if ($notInList) { // only export user list (without classes)
                    if (sizeof($temp) > 0)
                        $tab_conds .= " U.UserID NOT IN (" . implode(',', $temp) . ") AND ";
                } else {
                    // export nothing
                }
        }
    }
    
    // house filtering
    $libgroup = new libgroup();
    $houseArr = $libgroup->returnAllGroupIDAndName("", "", 4, Get_Current_Academic_Year_ID());
    
    if (isset($houseID)) {
        if ($houseID > 0) {
            $sql = "SELECT UserID FROM INTRANET_USERGROUP WHERE GroupID = '" . $houseID . "'";
            $houseStudentIdAry = Get_Array_By_Key($lexport->returnResultSet($sql), 'UserID');
            
            $conds_houseUser = " U.UserID IN (" . implode(',', $houseStudentIdAry) . ") AND ";
        } elseif ($houseID == - 1) {
            // do nothing
        } elseif ($houseID == - 2 || $houseID == - 3) {
            // $houseID==-2 => "all house" option => with house students only
            // $houseID==-3 => "without house" option => without house students only
            // Get All House ID in current ac yr
            $houseGroupIDArr = Get_Array_By_Key($houseArr, 'GroupID');
            $houseStudentIdAry = array();
            for ($i = 0; $i < count($houseArr); $i ++) {
                // Create new obj for each house
                $groupObj = new libgroup($houseGroupIDArr[$i]);
                $houseStudentIdAry = array_values(array_unique(array_merge($houseStudentIdAry, Get_Array_By_Key($groupObj->Get_Group_User(USERTYPE_STUDENT), 'UserID'))));
            }
            if ($houseID == - 2) {
                $conds_houseUser = " U.UserID IN (" . implode(',', $houseStudentIdAry) . ") AND ";
            } elseif ($houseID == - 3) {
                $conds_houseUser = " U.UserID NOT IN (" . implode(',', $houseStudentIdAry) . ") AND ";
            }
        }
    }
    $conds = "WHERE $tab_conds $conds_houseUser (U.UserLogin like '%$keyword%' OR U.UserEmail like '%$keyword%' OR U.ChineseName like '%$keyword%' OR U.EnglishName like '%$keyword%' OR U.ClassName like '%$keyword%' OR U.WebSamsRegNo like '%$keyword%')";
    
    if ($TabID == 1) { // Teacher
        $col_count = 14;
        $sql = "SELECT U.UserID, U.UserLogin, '', U.UserEmail, U.EnglishName, U.ChineseName, U.NickName,
             U.Gender, U.MobileTelNo, U.FaxNo, U.Barcode, U.Remark, U.TitleEnglish, U.TitleChinese ";
        if (! $isKIS) {
            $col_count ++;
            $sql .= ", U.StaffCode ";
        }
        if ((isset($module_version['StaffAttendance']) && $module_version['StaffAttendance'] != "") || (isset($plugin['payment']) && $plugin['payment'])) {
            $col_count ++;
            $sql .= ", U.CardID ";
        }
        if ($special_feature['ava_hkid']) {
            $col_count ++;
            $sql .= ", U.HKID ";
        }
        if ($sys_custom['BIBA_AccountMgmtCust']) {
            $houseGroupNameField = Get_Lang_Selection('G.TitleChinese', 'G.Title');
            $sql .= ",GROUP_CONCAT(DISTINCT $houseGroupNameField SEPARATOR ', ') as House ";
            $join_tables = " LEFT JOIN INTRANET_USERGROUP as UG ON UG.UserID=U.UserID 
			  				 LEFT JOIN INTRANET_GROUP as G ON G.GroupID=UG.GroupID AND G.RecordType=4 AND G.AcademicYearID='" . Get_Current_Academic_Year_ID() . "' ";
        }
        if ($plugin['imail_gamma'] && $sys_custom['iMailPlus']['EmailAliasName']) {
            $col_count ++;
            $sql .= ",U.ImapUserLogin ";
        }
        $sql .= " FROM INTRANET_USER as U $join_tables $conds GROUP BY U.UserID ORDER BY U.EnglishName";
        $row = $li->returnArray($sql);
    } elseif ($TabID == 2) { // student
        
        $col_count = 28;
        
        $sql = "SELECT 
            U.UserID, 
            U.UserLogin, 
            '', 
            U.UserEmail, 
            U.EnglishName, 
            U.ChineseName, 
            U.NickName, 
            U.Gender, 
            U.HomeTelNo, 
            U.MobileTelNo, 
            U.FaxNo, 
            U.Barcode, 
            U.Remark, 
            DATE_FORMAT(U.DateOfBirth, '%Y-%m-%d'), 
            IF(TRIM(U.WebSAMSRegNo)!='' AND LEFT(TRIM(U.WebSAMSRegNo),1)!='#',CONCAT('#',TRIM(U.WebSAMSRegNo)),TRIM(WebSAMSRegNo)),
            if (U.HKJApplNo = 0,'',U.HKJApplNo),
            U.Address,
            U.CardID, 
            U.HKID, 
            U.STRN, 
            P.Nationality, 
            P.PlaceOfBirth, 
            P.AdmissionDate,
            NCS.StaffInCharge,
            NCS.DurationInHK,
            NCS.LanguagePriority,
            IF(NCS.IsNCS, 'Y', 'N'),
            NCS.NGO
        FROM 
            INTRANET_USER U 
        LEFT JOIN 
            INTRANET_USER_PERSONAL_SETTINGS P 
        ON 
            U.UserID = P.UserID 
        LEFT JOIN
            INTRANET_USER_NCS_SETTINGS NCS
        ON
            U.UserID = NCS.UserID 
            $conds 
        GROUP BY 
            U.UserID ORDER BY U.ClassName, U.ClassNumber, U.EnglishName";
        $row = $li->returnArray($sql);

        for ($i = 0; $i < sizeof($row); $i ++) {
            $LangArr = array();
            
        	$re = '/([^,]+):([^,]+)/';
        	preg_match_all($re, $row[$i][25], $priorityOrder, PREG_SET_ORDER, 0);
        	foreach($priorityOrder as $order){
        	    $langName = trim($order[1]);
        	    
        	    if(trim($order[2])){
        	        if(isset($Lang['AccountMgmt']['LanguageCode'][$langName])){
        	            $LangArr[] = $Lang['AccountMgmt']['LanguageCode'][$langName];
        	        }else{
        	            $LangArr[] = trim($order[2]);
        	        }
        	    }
        	}
        	
            $row[$i][25] = implode(',', $LangArr);
        }
        
        $exportColumn = array(
            "UserLogin",
            "Password",
            "UserEmail",
            "EnglishName",
            "ChineseName",
            "NickName",
            "Gender",
            "Home Tel",
            "Mobile",
            "Fax",
            "Barcode",
            "Remarks",
            "DOB",
            "WebSAMSRegNo",
            "HKJApplNo",
            "Address",
            "CardID",
            "HKID",
            "STRN",
            "Nationality",
            "PlaceOfBirth",
            "AdmissionDate",
            "StaffInCharge",
            "DurationInHK",
            "Language",
            "IsNCS",
            "NGO"
        );
    } else { // alumni
        $col_count = 15;
        $sql = "SELECT 
         	UserID, UserLogin, '', UserEmail, EnglishName, ChineseName, Nickname,
             Gender, MobileTelNo, FaxNo, Barcode, Remark, 
             ClassName,
             ClassNumber,
             YearOfLeft
             FROM 
             	INTRANET_USER 
             Where
             	RecordType = 4 
             ORDER BY EnglishName";
        $row = $li->returnArray($sql);
    }
    
    // debug_pr($sql);
    if ($TabID == 1) { // teacher
        $exportColumn = array(
            "UserLogin",
            "Password",
            "UserEmail",
            "EnglishName",
            "ChineseName",
            "NickName",
            "Gender",
            "Mobile",
            "Fax",
            "Barcode",
            "Remarks",
            "TitleEnglish",
            "TitleChinese",
            "WebSAMSStaffCode"
        );
        if ((isset($module_version['StaffAttendance']) && $module_version['StaffAttendance'] != "") || (isset($plugin['payment']) && $plugin['payment'])) {
            $x .= ",CardID";
            $exportColumn = array_merge($exportColumn, array(
                "CardID"
            ));
        }
        if ($special_feature['ava_hkid']) {
            $x .= ",HKID";
            $exportColumn = array_merge($exportColumn, array(
                "HKID"
            ));
        }
        if ($sys_custom['BIBA_AccountMgmtCust']) {
            $exportColumn = array_merge($exportColumn, array(
                "House"
            ));
        }
        if ($plugin['imail_gamma'] && $sys_custom['iMailPlus']['EmailAliasName']) {
            $x .= ",EmailUserLogin";
            $exportColumn = array_merge($exportColumn, array(
                "EmailUserLogin"
            ));
        }
    }
    
    for ($i = 0; $i < sizeof($row); $i ++) {
        $row_length = $col_count;
        
        $utf_row = array();
        for ($j = 1; $j < $row_length; $j ++) {
            $utf_row[] = intranet_undo_htmlspecialchars($row[$i][$j]);
        }
        
        $utf_rows[] = $utf_row;
    }
    
    
    $export_content = $lexport->GET_EXPORT_TXT($utf_rows, $exportColumn);
}

$filename = "eclass-user.csv";
/*
 * header("Pragma: public");
 * header("Expires: 0"); // set expiration time
 * header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
 *
 * header("Content-type: application/octet-stream");
 * header("Content-Length: ".strlen($export_content));
 * header("Content-Disposition: attachment; filename=\"".$filename."\"");
 * echo $export_content;
 */
// output2browser($export_content, $filename);
$lexport->EXPORT_FILE($filename, $export_content);
intranet_closedb();
// header("Location: $url");