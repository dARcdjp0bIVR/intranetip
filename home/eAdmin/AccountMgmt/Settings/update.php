<?php
# using: 

################# Change Log [Start] ############
#
#	Date	:	2015-09-16	(Bill)	[2015-0916-0938-00066]
#	update 'CanUpdatePhoto_'.$thisUserType using value of $officialphoto
#
#	Date	:	2014-10-24	(Omas)
#	1)	merge StaffMgmt, StudentMgmt, ParentMgmt, AlumniMgmt/settings/update.php to this file
#	2)	Add a new setting ChangeEmailAlert_USERTYPE
#
################# Change Log [End] ############


$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

if(!($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Staff"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"])) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");

intranet_auth();
intranet_opendb();

$laccount = new libaccountmgmt();
$lgeneralsettings = new libgeneralsettings();

$data = array();

$thisUserType = $RecordType;

##### Personal Info
$data['CanUpdateNickName_'.$thisUserType] = $nickname ? $nickname : 0;
if($RecordType == USERTYPE_STUDENT){
	// [2015-0916-0938-00066] get value from $officialphoto
	$data['CanUpdatePhoto_'.$thisUserType] = $officialphoto ? $officialphoto : 0;
}
$data['CanUpdatePersonalPhoto_'.$thisUserType] = $photo ? $photo : 0;
$data['CanUpdateGender_'.$thisUserType] = $gender ? $gender : 0;
$data['CanUpdateDOB_'.$thisUserType] = $dob ? $dob : 0;

##### Contact Info
$data['CanUpdateHomeTel_'.$thisUserType] = $hometel ? $hometel : 0;
$data['CanUpdateOfficeTel_'.$thisUserType] = $officetel ? $officetel : 0;
$data['CanUpdateMobile_'.$thisUserType] = $mobile ? $mobile : 0;
$data['CanUpdateFax_'.$thisUserType] = $fax ? $fax : 0;
$data['CanUpdateAddress_'.$thisUserType] = $address ? $address : 0;
$data['CanUpdateCountry_'.$thisUserType] = $country ? $country : 0;
$data['CanUpdateURL_'.$thisUserType] = $url ? $url : 0;
$data['CanUpdateEmail_'.$thisUserType] = $email ? $email : 0;
$data['ChangeEmailAlert_'.$thisUserType] = $ChangeEmailAlert ? $ChangeEmailAlert : 0;

##### Message
$data['CanUpdateMessage_'.$thisUserType] = $CanUpdateMessage;

##### Display    Parent & Alumni do not need this part
if($RecordType == USERTYPE_STAFF || $RecordType == USERTYPE_STUDENT){
$data['DisplayDOB_'.$thisUserType] = $display_dob ? $display_dob : 0;
$data['DisplayHomeTel_'.$thisUserType] = $display_hometel ? $display_hometel : 0;
$data['DisplayFax_'.$thisUserType] = $display_fax ? $display_fax : 0;
$data['DisplayAddress_'.$thisUserType] = $display_address ? $display_address : 0;
$data['DisplayCountry_'.$thisUserType] = $display_country ? $display_country : 0;
$data['DisplayEmail_'.$thisUserType] = $display_email ? $display_email : 0;
}

if($plugin["platform"] == "KIS") 
{
	##### Login Password
	$data['CanUpdatePassword_'.$thisUserType] = $CanUpdatePassword;
	
	##### Enable Password Policy
	$data['EnablePasswordPolicy_'.$thisUserType] = $EnablePasswordPolicy;
}

# store in DB
$lgeneralsettings->Save_General_Setting("UserInfoSettings", $data);

intranet_closedb();
header("Location: index.php?RecordType=$RecordType&xmsg=".$Lang['eNotice']['SettingsUpdateSuccess']);
?>