<?php
// modifying : 

##### Change Log [Start] #####
#	Date:	2014-05-29 (Carlos) - only send notification email if recipient email is valid
#
#	2014-05-07 (Carlos): Added suspend/unsuspend iMail plus account api call
#
#	Date:	2014-04-30	Carlos
#			update eclass info function by eClass40UserUpdateInfo()
#
#	Date:	2013-12-09	Ivan
#			fixed to syn parent info to eLib+
#
#	Date:	2013-03-15	Carlos
#			remove shared group calendars for user and add shared group calendars to user
#
#	Date:	2012-10-03	Carlos
#			fix failed to enable iMail plus account when password method is HASHED
#
#	Date:	2012-08-30	YatWoon
#			added barcode
#
#	Date:	2012-08-01	Ivan
#			added logic to syn user info to library system
#
#	Date:	2012-04-03 (Henry Chow)
#			allow to input HKID if flag is ON
#
#	Date:	2012-03-14	YatWoon
#			allow admin update user's personal email [Case#2012-0313-1603-23071]
#
#	Date:	2012-02-07	YatWoon
#			fixed: failed to update hashed password due to mysql syntax error
#
#	Date:	2011-09-30  Carlos
#			update two-way hashed/encrypted password if password is modified
#
#	Date :  2011-06-28  Fai
#			exit the program if $error_msg > 0
#
#	Date:	2011-04-04	YatWoon
#			change email notification subject & content data 
#
#	2011-03-07	(Henry Chow)
#		Password not display in edit page (related case : #2011-0304-1154-52067)
#		if password is empty => keep unchange ; if password is entered => change 
#
#	Date:	2010-11-19	(Henry Chow)
#			add selection of "Group" & "Role"
#
###### Change Log [End] ######
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libauth.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libregistry.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."includes/libemail.php");
include_once($PATH_WRT_ROOT."lang/email.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libftp.php");
include_once($PATH_WRT_ROOT."includes/libldap.php");
include_once($PATH_WRT_ROOT."includes/imap_gamma.php");
include_once($PATH_WRT_ROOT."includes/icalendar.php");
include_once($PATH_WRT_ROOT."includes/libeclass40.php");

intranet_auth();
intranet_opendb();

if(!$sys_custom['SISUserManagement']){
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

if(!($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Parent"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"])) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

if(sizeof($_POST)==0) {
	header("Location: index.php");	
	exit;
}

$lauth = new libauth();
$laccount = new libaccountmgmt();
$lu = new libuser($uid);
$lwebmail = new libwebmail();
$lc = new libeclass();
$lcalendar = new icalendar();

$engname = intranet_htmlspecialchars(stripslashes($engname));
$chiname = intranet_htmlspecialchars(stripslashes($chiname));
/*
## check Email duplication ###
if($email != ""){
	$sql = "SELECT UserID FROM INTRANET_USER WHERE UserEmail='$email' AND UserID != $uid";
	$temp = $laccount->returnVector($sql);
	if(sizeof($temp)!=0){
		$error_msg[] = $email;
	}
}
*/

$userInfo = $laccount->getUserInfoByID($uid);

# check email
$UserEmail = intranet_htmlspecialchars(trim($UserEmail));

if ($UserEmail != "" && $userInfo['UserEmail'] != $UserEmail)
{
    if ($lu->isEmailExists($UserEmail) || $lc->isEmailExistInSystem($UserEmail))
    {
        header("Location: edit.php?uid=$uid&xmsg=EmailUsed");
        exit;
    }
}

$ori_UserEmail = $lu->UserEmail;
$new_UserEmail = $UserEmail;

if(sizeof($error_msg)>0) {
	$errorList = implode(',', $error_msg);	
}

if(sizeof($error_msg)>0) { 
?>

	<body onload="document.form1.submit()">
	<form name="form1" method="post" action="edit.php">
		<input type="hidden" name="comeFrom" id="comeFrom" value="<?=$comeFrom?>">
		<input type="hidden" name="keyword" id="keyword" value="<?=$keyword?>">
		
		<input type="hidden" name="uid" id="uid" value="<?=$uid?>">
	<?/*
		<input type="hidden" name="targetClass" id="targetClass" value="<?=$targetClass?>">
		<input type="hidden" name="targetClass" id="targetClass" value="<?=$targetClass?>">
		<input type="hidden" name="recordstatus" id="recordstatus" value="<?=$recordstatus?>">
		<input type="hidden" name="recordstatus" id="recordstatus" value="<?=$recordstatus?>">
		<input type="hidden" name="keyword" id="keyword" value="<?=$keyword?>">
		<input type="hidden" name="UserEmail" id="UserEmail" value="<?=$UserEmail?>"> 
		<input type="hidden" name="status" id="status" value="<?=$status?>">*/?>
		<input type="hidden" name="pwd" id="pwd" value="<?=$pwd?>"> 
		<input type="hidden" name="engname" id="engname" value="<?=$engname?>">
		<input type="hidden" name="chiname" id="chiname" value="<?=$chiname?>">
<?/*		<input type="hidden" name="gender" id="gender" value="<?=$gender?>">
		<input type="hidden" name="address" id="address" value="<?=$address?>">
		<input type="hidden" name="country" id="country" value="<?=$country?>">
		<input type="hidden" name="homePhone" id="homePhone" value="<?=$homePhone?>">
		<input type="hidden" name="officePhone" id="officePhone" value="<?=$officePhone?>">
		<input type="hidden" name="mobilePhone" id="mobilePhone" value="<?=$mobilePhone?>">
		<input type="hidden" name="faxPhone" id="faxPhone" value="<?=$faxPhone?>">
		<input type="hidden" name="hkid" id="hkid" value="<?=$hkid?>">
		<input type="hidden" name="barcode" id="barcode" value="<?=$barcode?>">
		<input type="hidden" name="open_webmail" id="open_webmail" value="<?=$open_webmail?>">
		<input type="hidden" name="open_file" id="open_file" value="<?=$open_file?>">*/?>
		<input type="hidden" name="error" id="error" value="<?=$errorList?>">
	</form>
	<script language="javascript">
		document.form1.submit();
	</script>
	</body>
<? 
	exit();
	
	} else { 

	$li = new libregistry();
	
	$userlogin = $userInfo['UserLogin'];
	$email = $userInfo['UserEmail'];
	$original_password = $userInfo['UserPassword'];
	
	if ($intranet_authentication_method=="HASH" && $pwd!='') {
		$modifyPassword = 1;
	}
	else {
		$modifyPassword = ($pwd!="" && $pwd!=$original_password) ? 1 : 0;
	}	


		# create & update info in INTRANET_USER
		$UserPassword = trim($pwd);
//		$CardID = trim($smartcardid);
//		$STRN = trim($strn);
//		$WebSAMSRegNo = trim($WebSAMSRegNo);
//		$NickName = intranet_htmlspecialchars(trim($nickname));
//		$DateOfBirth = trim($dob);
//		$Gender = trim($gender);
//// 		$HKID = trim($hkid);
//// 		$Barcode = trim($barcode);
//		$Address = intranet_htmlspecialchars(trim($address));
//		$Country = trim($country);
//		$HomeTelNo = intranet_htmlspecialchars(trim($homePhone));
//		$OfficeTelNo = intranet_htmlspecialchars(trim($officePhone));
//		$MobileTelNo = intranet_htmlspecialchars(trim($mobilePhone));
//		$FaxNo = intranet_htmlspecialchars(trim($faxPhone));
//		$HKID = intranet_htmlspecialchars(trim($hkid));
//		$Barcode = intranet_htmlspecialchars(trim($barcode));
//		$remark = intranet_htmlspecialchars(trim($remark));
//		
		$dataAry = array();
		
		if($modifyPassword) {
			if ($intranet_authentication_method=="HASH") {
				//$dataAry['UserPassword'] = "HashedPass = MD5('$userlogin$UserPassword$intranet_password_salt'),";
				$dataAry['UserPassword'] = "";
				$dataAry['HashedPass'] = "md5('$userlogin$UserPassword$intranet_password_salt')";
			} else {
				$dataAry['UserPassword'] = trim($pwd);
			}
		}
		
		if(!$modifyPassword){
			if($intranet_authentication_method=="HASH"){
				$UserPassword = $lauth->GetUserDecryptedPassword($uid);
			}else{
				$UserPassword = $userInfo['UserPassword'];
			}
		}
		
//		$dataAry['UserEmail'] = intranet_htmlspecialchars(trim($UserEmail));
		$dataAry['EnglishName'] = trim($engname);
		$dataAry['ChineseName'] = trim($chiname);
//		$dataAry['RecordStatus'] = $status;
//		$dataAry['Gender'] = $Gender;
//		$dataAry['Address'] = $Address;
//		$dataAry['Country'] = $Country;
//		$dataAry['HomeTelNo'] = $HomeTelNo;
//		$dataAry['OfficeTelNo'] = $OfficeTelNo;
//		$dataAry['MobileTelNo'] = $MobileTelNo;
//		$dataAry['FaxNo'] = $FaxNo;
//		$dataAry['HKID'] = $HKID;
//		$dataAry['Barcode'] = $Barcode;
//		$dataAry['Remark'] = $remark;
		
		$sql = "Update INTRANET_USER SET EnglishName='".$dataAry['EnglishName']."',ChineseName='".$dataAry['ChineseName']."',HashedPass =".$dataAry['HashedPass'].", UserPassword = '".$dataAry['UserPassword']."'  where UserID = '{$uid}'";
		$success = $lu->db_db_query($sql);
//		$success = $laccount->updateUserInfo($uid, $dataAry);
		if($modifyPassword && $success){
			$lauth->UpdateEncryptedPassword($uid, $UserPassword);
		}
		# send email
		if($modifyPassword && $success)
		{
			list($mailSubject,$mailBody) = $laccount->returnEmailNotificationData("", "", "", stripslashes($engname), stripslashes($chiname), 1);
			//$lwebmail->sendModuleMail((array)$uid,$mailSubject,$mailBody);
			include_once($PATH_WRT_ROOT."includes/libsendmail.php");
			$lsendmail = new libsendmail();
			$webmaster = get_webmaster();
			$headers = "From: $webmaster\r\n";
			if($UserEmail !="" && intranet_validateEmail($UserEmail,true)){
				$lsendmail->SendMail($UserEmail, $mailSubject, $mailBody,"$headers");
			}
		} 
//		
//		$sql = "SELECT GroupID FROM INTRANET_USERGROUP WHERE UserID='$uid'";
//		$existing_groups = $lu->returnVector($sql);
//		
//	if($plugin["imail_gamma"])
//	{
//		$IMap = new imap_gamma();
//		$IMapEmail = trim($userlogin)."@".$SYS_CONFIG['Mail']['UserNameSubfix'];
//		if($EmailStatus=="enable")
//		{
//			if($IMap->is_user_exist($IMapEmail))
//			{
//				$account_exist = true;
//				if($modifyPassword)
//					$IMap->change_password($IMapEmail, $UserPassword);
//			}
//			else
//			{
//				$igamma_pwd = $UserPassword;
//				$account_exist = $IMap->open_account($IMapEmail,$igamma_pwd);
//			}
//			
//			if($account_exist)
//			{
//				$laccount->setIMapUserEmail($uid,$IMapEmail);
//				
//				$result["setQuota"] = $IMap->SetTotalQuota($IMapEmail,$Quota, $uid);
//				
//				$IMap->setUnsuspendUser($IMapEmail,"iMail",$UserPassword); // resume iMail plus account
//			}
//			
//		}
//		else
//		{
//			# disable gamma mail
//			$laccount->setIMapUserEmail($uid,"");
//			
//			$IMap->setSuspendUser($IMapEmail,"iMail"); // suspend iMail plus account
//		}
//	}
//	
//	
//	if(!$plugin["imail_gamma"])
//	{
//		# update webmail password
//		if ($modifyPassword && $lwebmail->has_webmail)
//			$lwebmail->change_password($userlogin, $UserPassword, "iMail");
//	}
//	
//	# FTP management
//	if ($plugin['personalfile'])
//	{
//		$lftp = new libftp();
//		if ($modifyPassword && $lftp->isFTP)
//		{
//			$lftp->changePassword($userlogin, $UserPassword, "iFolder");
//		}
//	}
//        
//	if ($intranet_authentication_method=='LDAP')
//	{
//		$lldap = new libldap();
//		if ($modifyPassword && $lldap->isPasswordChangeNeeded())
//		{
//			$lldap->connect();
//			$lldap->changePassword($userlogin, $UserPassword);
//		}
//	}
//	
//    # SchoolNet
//	if ($plugin['SchoolNet']) {
//		include_once($PATH_WRT_ROOT."includes/libschoolnet.php");
//		$lschoolnet = new libschoolnet();
//		
//		$sql = "SELECT UserLogin, UserPassword,ClassName, ClassNumber, DATE_FORMAT(DateOfBirth,'%Y-%m-%d'), Gender, Chinesename, EnglishName, HomeTelNo, MobileTelNo, Address, UserEmail FROM INTRANET_USER WHERE UserID = $uid";
//		$data = $li->returnArray($sql,12);
//		if($modifyPassword)
//			$data[0][1] = $UserPassword;
//		$lschoolnet->addStudentUser($data);
//	}
//	
//	if($skipStep2!=1 || (isset($newids) && $newids=="")) {
//				
//		# delete Parent relationship
//		$sql = "DELETE FROM INTRANET_PARENTRELATION WHERE ParentID=".$uid;
//		$laccount->db_db_query($sql);
//				
//		# modify Group
//		# Hide ECA Groups if eEnrolment is updated to use Year-based Term-based Club
//		include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
//		$libenroll = new libclubsenrol();
//		$cond_ECA_GroupID = "";
//		if ($plugin['eEnrollment'] && $libenroll->isUsingYearTermBased)
//		{
//			$sql = "SELECT GroupID FROM INTRANET_GROUP WHERE RecordType = '5'";
//			$ECA_GroupIDArr = $li->returnVector($sql);
//			
//			$cond_ECA_GroupID = '';
//			if (is_array($ECA_GroupIDArr) && count($ECA_GroupIDArr) > 0)
//			{
//				$ECA_GroupIDList = implode(',', $ECA_GroupIDArr);
//				$cond_ECA_GroupID = " AND GroupID NOT IN (".$ECA_GroupIDList.") ";
//			}
//		}
//		
//		$gpID = 4;
//		$sql = "DELETE FROM INTRANET_USERGROUP WHERE UserID=$uid AND GroupID!=$gpID AND GroupID NOT IN (1,2,3,4)";
//		$sql .= $cond_ECA_GroupID;
//		$laccount->db_db_query($sql);
//		# add group member
//		for($i=0; $i<sizeof($GroupID); $i++){
//			if($GroupID[$i] != "") {
//				$sql = "INSERT INTO INTRANET_USERGROUP (GroupID, UserID, DateInput, DateModified) VALUES (".$GroupID[$i].", $uid, now(), now())";
//				$laccount->db_db_query($sql);
//			}
//		}
//
//		# Assign to Role
//		# remove existing role
//		$sql = "DELETE FROM ROLE_MEMBER WHERE UserID=$uid";
//		$laccount->db_db_query($sql);
//	
//		# add role
//		$identityType = "Parent";
//		for($i=0; $i<sizeof($RoleID); $i++){
//			if($RoleID[$i] != "" && $RoleID[$i]!=0) {
//				$sql = "INSERT INTO ROLE_MEMBER (RoleID, UserID, DateInput, InputBy, DateModified, ModifyBy, IdentityType) VALUES ('".$RoleID[$i]."', '$uid', now(), '$UserID', now(), '$UserID', '$identityType')";
//				$laccount->db_db_query($sql);
//			}
//		}
//		
//		if(isset($newids) && $newids!="") {
//			
//			$ids = explode(",",$newids);
//			for($i=0;$i<sizeof($ids);$i++){
//				$id = $ids[$i];			
//				$sid = ${'uid_'.$id};	// student id 
//				$ch_name = $chiname;	
//				$en_name = $engname;
//				$emphone = ${'emphone_new_'.$id};
//				
//				$linkedGuardianID = explode(',', ${'linkedGuardianID_'.$id});
//				//debug_pr($linkedGuardianID);
//				$add_area = '';
//				$add_road = '';
//				$add_address = '';
//				$address = ${'address_new_'.$id};
//				$relation= ${'relation_new_'.$id};
//				$occupation = ${'occupation_new_'.$id};
//				$IsLiveTogether = (int)${'liveTogether_new_'.$id};
//				
//				$new_str ="new_$id";
//				
//				$IsMain = ${'mainGuardianFlag_'.$id}=='0' ? 1 : 0;
//				$IsSMS = ${'smsFlag_'.$id}=='0' ? 1 : 0;
//				$IsEmergencyContact = ${'emergencyContactFlag_'.$id}=='0' ? 1 : 0;
//				
//				# re-build Parent relationship
//				$sql = "INSERT INTO INTRANET_PARENTRELATION SET ParentID=$uid, StudentID=$sid";
//				$laccount->db_db_query($sql);
//				
//				# modify guardians
//				$sql = "UPDATE $eclass_db.GUARDIAN_STUDENT SET IsMain=0 WHERE UserID=$sid";
//				$laccount->db_db_query($sql);	
//				$sql = "UPDATE $eclass_db.GUARDIAN_STUDENT SET IsSMS=0 WHERE UserID=$sid";	
//				$laccount->db_db_query($sql);
//				$sql = "UPDATE $eclass_db.GUARDIAN_STUDENT_EXT_1 SET IsEmergencyContact=0 WHERE UserID=$sid";	
//				$laccount->db_db_query($sql);
//						
//				if(${'guardianCheck_'.$id}=="on") {
//					
//					$sql = "INSERT INTO $eclass_db.GUARDIAN_STUDENT (
//							UserID,ChName,EnName,
//							Phone,EmPhone,Address,Relation,
//							IsMain,IsSMS,InputDate,ModifiedDate
//						) VALUES (
//							'$sid','$ch_name','$en_name',
//							'$phone','$emphone','$address','$relation',
//							'$IsMain', '$IsSMS',NOW(), NOW()
//						) ";
//					$li->db_db_query($sql);
//					//echo $sql.'<br>';
//					$record_id = mysql_insert_id();
//					
//					//insert the extend record
//					$sql = "INSERT INTO $eclass_db.GUARDIAN_STUDENT_EXT_1 (
//							UserID,RecordID, IsLiveTogether, IsEmergencyContact, Occupation,
//							AreaCode, Road, Address,
//							InputDate,ModifiedDate
//						) VALUES (
//							'$sid','$record_id', '$IsLiveTogether', '$IsEmergencyContact', '$occupation',
//							'$add_area','$add_road', '$add_address', 
//							NOW(), NOW()
//						)";
//					//echo $sql.'<br>';
//					$li->db_db_query($sql);
//				}
//				
//				# update Main Guardian, SMS Receiver, Emergency Contact
//				if($IsMain!=1) {
//					$sql = "UPDATE $eclass_db.GUARDIAN_STUDENT SET IsMain=1 WHERE RecordID=".$linkedGuardianID[${'mainGuardianFlag_'.$id}-1];
//					$laccount->db_db_query($sql);
//					//echo $sql.'<br>';
//				}
//				if($IsSMS!=1) {
//					$sql = "UPDATE $eclass_db.GUARDIAN_STUDENT SET IsSMS=1 WHERE RecordID=".$linkedGuardianID[${'smsFlag_'.$id}-1];
//					$laccount->db_db_query($sql);
//					//echo $sql.'<br>';
//				}
//				if($IsEmergencyContact!=1) {
//					$sql = "UPDATE $eclass_db.GUARDIAN_STUDENT_EXT_1 SET IsEmergencyContact=1 WHERE RecordID=".$linkedGuardianID[${'emergencyContactFlag_'.$id}-1];
//					$laccount->db_db_query($sql);
//					//echo $sql.'<br>';
//				}
//			}
//		
//		}
//	}
//	
//	
//		# icalendar
//		$sql = "select GroupID from INTRANET_USERGROUP where UserID = '$uid' ";
//		$all_groups = $lu->returnVector($sql);
//		
//		$sql = "delete from CALENDAR_CALENDAR_VIEWER where 
//			GroupID not in ('".implode("','",$all_groups)."') and GroupType = 'E' and UserID = $uid";
//		$lu->db_db_query($sql);
//		
//		$sql = "select GroupID from CALENDAR_CALENDAR_VIEWER where GroupType = 'E' and UserID = $uid";
//		$existing = $lu->returnVector($sql);
//		
//		$existing = empty($existing)? array():$existing;	
//	
//		//$newGroup = array_diff($GroupID,$existing);
//		//if (!empty($existing)){
//		$sql = "insert into CALENDAR_CALENDAR_VIEWER 
//				(CalID, UserID, GroupID, GroupType, Access, Color, Visible)
//				select g.CalID, '$uid', g.GroupID, 'E', 'R', '2f75e9', '1' 
//				from INTRANET_GROUP as g inner join INTRANET_USERGROUP as u 
//				on g.GroupID = u.GroupID and u.UserID = $uid
//				where g.GroupID not in ('".implode("','",$existing)."')";
//		$lu->db_db_query($sql);
//	
//		# remove shared group calendars 
//		$cal_remove_groups = array_values(array_diff($existing_groups,$all_groups));
//		for($i=0;$i<sizeof($cal_remove_groups);$i++) {
//			$lcalendar->removeCalendarViewerFromGroup($cal_remove_groups[$i],$uid);
//		}
//		# insert calendar viewer to calendars that have shared to the user's groups
//		for($i=0;$i<sizeof($all_groups);$i++) {
//			$lcalendar->addCalendarViewerToGroup($all_groups[$i],$uid);
//		}
//	
//// 	$le = new libeclass();
////	$lc->addTitleToCourseUser($UserEmail,$dataAry['TitleEnglish'],$dataAry['TitleChinese'],$dataAry['EnglishName'],$dataAry['ChineseName'],false,$dataAry['HomeTelNo'],$dataAry['FaxNo'],$dataAry['Address']);
//	
//	$lc->eClass40UserUpdateInfo($ori_UserEmail, $dataAry, $new_UserEmail);
//	
//	$successAry['synUserDataToModules'] = $lu->synUserDataToModules($uid);
	
	intranet_closedb();
	
	$flag = ($success) ? "UpdateSuccess" : "UpdateUnsuccess";
	
	if(substr($comeFrom,0,2)=="./")
	$comeFrom = ".".$comeFrom;
	$comeFrom = $comeFrom ? $comeFrom : "index.php";

	header("Location: $comeFrom?xmsg=$flag");
}
?>