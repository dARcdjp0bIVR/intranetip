<?php
// Editing by 

##### Change Log [Start] #####
#   Date:   2019-04-25 (Isaac):
#           modified displayStudentID() to using $.post for supporting PowerClass EmulateIE9
#
#	Date:	2018-01-03 (Carlos) [ip.2.5.9.1.1]: Improved password checking.
#	Date:	2015-12-30 (Carlos) - $plugin['radius_server'] - added Wi-Fi access option.
#
#	Date:	2015-05-22 (Bill)
#			added eNotice content to "- Related to Student -" section
#			To support assign notice function
#			- added js function setAllCheckBox(), showOption(), hideOption(), ajaxLoadAddNoticeContent(), js_update_notice_list(), Refresh_Notice_Content_Table() 
#			- modified js function callback_displayInfo()
#			fixed: duplicated "- Related to Student -" content after refresh using FireFox
#
#	Date:	2014-09-19 (Carlos)
#			added js isValueInCsvString(csv_string,value) for checking value position in comma separated string
#
#	2012-08-30 (YatWoon): add barcode
#
#	Date:	2012-04-03 (Henry Chow)
#			allow to input HKID if flag is ON
#
#	Date:	2010-11-19	(Henry Chow)
#			add selection of "Group" & "Role"
#
###### Change Log [End] ######

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libgrouping.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/librole.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libauth.php");

intranet_auth();
intranet_opendb();

if(!($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Parent"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"])) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

if(sizeof($_POST)==0) {
	header("Location: new.php");	
	exit;
}

$laccount = new libaccountmgmt();
$linterface = new interface_html();
$lclass = new libclass();
$lo = new libgrouping();
$lrole = new librole();

$lu = new libuser();
$lauth = new libauth();

$SettingArr = $lu->RetrieveUserInfoSettingInArrary(array('EnablePasswordPolicy_'.USERTYPE_PARENT));
$PasswordLength = $SettingArr['EnablePasswordPolicy_'.USERTYPE_PARENT];
if ($PasswordLength<6) $PasswordLength = 6;

$engname = intranet_htmlspecialchars(stripslashes($engname));
$chiname = intranet_htmlspecialchars(stripslashes($chiname));

## check User Login duplication ###
if($userlogin != ""){
	$sql = "SELECT UserID FROM INTRANET_USER WHERE UserLogin='$userlogin'";
	$temp = $laccount->returnVector($sql);
	if(sizeof($temp)!=0){
		$error_msg[] = $userlogin;
	}
}

if($sys_custom['UseStrongPassword']){
	$check_password_result = $lauth->CheckPasswordCriteria($pwd,$userlogin,$PasswordLength);
	if(!in_array(1,$check_password_result)){
		$error_msg[] = $pwd;
	}
}

## check Email duplication ###
if($email != ""){
	$sql = "SELECT UserID FROM INTRANET_USER WHERE UserEmail='$email'";
	$temp = $laccount->returnVector($sql);
	if(sizeof($temp)!=0){
		$error_msg[] = $email;
	}
}

## check Barcode duplication ###
if($barcode != ""){
	$sql = "SELECT UserID FROM INTRANET_USER WHERE Barcode='$barcode'";
	$temp = $laccount->returnVector($sql);
	if(sizeof($temp)!=0){
		$error_msg[] = $barcode;
	}
}

## check HKID duplication ###
if($hkid != ""){
	$sql = "SELECT UserID FROM INTRANET_USER WHERE HKID='$hkid'";
	$temp = $laccount->returnVector($sql);
	if(sizeof($temp)!=0){
		$error_msg[] = $hkid;
	}
}

if(sizeof($error_msg)>0) {
	$action = "new.php";
	$errorList = implode(',', $error_msg);	
}


if(sizeof($error_msg)>0) { 
?>

	<body onload="document.form1.submit()">
	<form id="form1" name="form1" method="post" action="new.php">
		<input type="hidden" name="comeFrom" id="comeFrom" value="<?=$comeFrom?>">
		<input type="hidden" name="targetClass" id="targetClass" value="<?=$targetClass?>">
		<input type="hidden" name="recordstatus" id="recordstatus" value="<?=$recordstatus?>">
		<input type="hidden" name="keyword" id="keyword" value="<?=$keyword?>">
		<input type="hidden" name="userlogin" id="userlogin" value="<?=$userlogin?>">
		<input type="hidden" name="pwd" id="pwd" value="<?=$pwd?>">
		<input type="hidden" name="email" id="email" value="<?=$email?>">
		<input type="hidden" name="status" id="status" value="<?=$status?>">
		<input type="hidden" name="engname" id="engname" value="<?=$engname?>">
		<input type="hidden" name="chiname" id="chiname" value="<?=$chiname?>">
		<input type="hidden" name="gender" id="gender" value="<?=$gender?>">
		<input type="hidden" name="address" id="address" value="<?=$address?>">
		<input type="hidden" name="country" id="country" value="<?=$country?>">
		<input type="hidden" name="homePhone" id="homePhone" value="<?=$homePhone?>">
		<input type="hidden" name="officePhone" id="officePhone" value="<?=$officePhone?>">
		<input type="hidden" name="mobilePhone" id="mobilePhone" value="<?=$mobilePhone?>">
		<input type="hidden" name="faxPhone" id="faxPhone" value="<?=$faxPhone?>">
		<input type="hidden" name="hkid" id="hkid" value="<?=$hkid?>">
		<input type="hidden" name="barcode" id="barcode" value="<?=$barcode?>">
		<input type="hidden" name="open_webmail" id="open_webmail" value="<?=$open_webmail?>">
		<input type="hidden" name="open_file" id="open_file" value="<?=$open_file?>">
		<input type="hidden" name="error" id="error" value="<?=$errorList?>">
		<?php if($plugin['radius_server']){ ?>
		<input type="hidden" name="enable_wifi_access" id="enable_wifi_access" value="<?=$enable_wifi_access?>" />
		<?php } ?>
	</form>
	<script language="javascript">
		document.form1.submit();
	</script>
	</body>
<? 
	exit;

	} else { 

	$countrySelection = "<select name='country' id='country'><option value='' ".(($userInfo['Country']=="") ? "selected" : "").">-- $i_alert_pleaseselect $i_UserCountry --</option>";
	for($i=0; $i<sizeof($Lang['Country']); $i++) {
		$countrySelection .= "<option value=\"".$Lang['Country'][$i]."\" ".(($Lang['Country'][$i]==$country)?"selected" : "").">".$Lang['Country'][$i]."</option>";	
	}
	$countrySelection .= "</select>";
	
	    
	$CurrentPageArr['ParentMgmt'] = 1;
	$CurrentPage = "Mgmt_Account";
	
if($comeFrom=="")
	$comeFrom = "index.php";

	if($comeFrom=="noChildInSchool.php") {
		$TAGS_OBJ[] = array($Lang['AccountMgmt']['UserList'],"index.php",0);
		$TAGS_OBJ[] = array($Lang['AccountMgmt']['ParentWithoutChildInClass'],"noChildInSchool.php",1);	
	} else {
		$TAGS_OBJ[] = array($Lang['AccountMgmt']['UserList'],"index.php",1);
		$TAGS_OBJ[] = array($Lang['AccountMgmt']['ParentWithoutChildInClass'],"noChildInSchool.php",0);		
	}
	$MODULE_OBJ['title'] = $Lang['Header']['Menu']['ParentAccount'];
	
	# navigation bar
	$PAGE_NAVIGATION[] = array($Lang['AccountMgmt']['UserList'], "javascript:goURL('$comeFrom')");
	$PAGE_NAVIGATION[] = array($Lang['AccountMgmt']['NewUser'], "");
	
	# step information
	$STEPS_OBJ[] = array($Lang['AccountMgmt']['InputParentDetails'], 0);
	$STEPS_OBJ[] = array($Lang['AccountMgmt']['SelectCorrespondingStudents'], 1);
	
	if(isset($error) && $error!="")
		$errorAry = explode(',', $error);
	
	// class menu default option "-- Select student --"
	//$classMenu = $lclass->getSelectClass(' name="className" id="className" onChange="displayStudentID(\'studentID\', \'spanStudentID\')"', $classID, 0, "", Get_Current_Academic_Year_ID());
	$classMenu = $lclass->getSelectClass(' name="className" id="className" onChange="displayStudentID(\'studentID\', \'spanStudentID\')"', $classID, 0, "-- ".$Lang['StudentRegistry']['SelectStudent']." --", Get_Current_Academic_Year_ID());	
	
	$MODULE_OBJ = $laccount->GET_MODULE_OBJ_ARR();
	
	$linterface->LAYOUT_START();
	
	// include JS CSS for thickbox 
	echo $linterface->Include_JS_CSS();
	
	?>
	<script language="javascript">
	function goSubmit() {
		var IDAry = new Array();
		IDAry = document.form1.newids.value.split(',');
		if(document.form1.newids.value != "") {
			for(i=0; i<IDAry.length; i++) {
				if(eval("document.form1.linkedGuardianID_"+IDAry[i]+".value")!="") {
					if(eval("document.form1.guardianCheck_"+IDAry[i]+".checked")==false && eval("document.form1.mainGuardianFlag_"+IDAry[i]+".value")==0) {
						alert(".<?=$i_alert_pleaseselect." ".$Lang['AccountMgmt']['MainGuardian']?>");
						return false;
					}
					if(eval("document.form1.guardianCheck_"+IDAry[i]+".checked")==false && eval("document.form1.smsFlag_"+IDAry[i]+".value")==0) {
						alert(".<?=$i_alert_pleaseselect." ".$Lang['AccountMgmt']['ReceiveSMS']?>");
						return false;
					}
					if(eval("document.form1.guardianCheck_"+IDAry[i]+".checked")==false && eval("document.form1.emergencyContactFlag_"+IDAry[i]+".value")==0) {
						alert(".<?=$i_alert_pleaseselect." ".$Lang['AccountMgmt']['EmergencyContact']?>");
						return false;
					}
				}
				if(eval("document.form1.linkedGuardianID_"+IDAry[i]+".value")!="" || eval("document.form1.guardianCheck_"+IDAry[i]+".checked")==true) {
					if(eval("document.form1.mainGuardianFlag_"+IDAry[i]+".value")=="") {
						alert("<?=$i_alert_pleaseselect." ".$Lang['AccountMgmt']['MainGuardian']?>");	
						return false;
					}
					if(eval("document.form1.smsFlag_"+IDAry[i]+".value")=="") {
						alert("<?=$i_alert_pleaseselect." ".$Lang['AccountMgmt']['ReceiveSMS']?>");	
						return false;
					}
					if(eval("document.form1.emergencyContactFlag_"+IDAry[i]+".value")=="") {
						alert("<?=$i_alert_pleaseselect." ".$Lang['AccountMgmt']['EmergencyContact']?>");	
						return false;
					}
					if(eval("document.form1.guardianCheck_"+IDAry[i]+".checked")==true) {
						if(eval("document.form1.relation_new_"+IDAry[i]+".value")=="") {
							alert("<?=$i_alert_pleaseselect." ".$ec_iPortfolio['relation']?>");	
							eval("document.form1.relation_new_"+IDAry[i]).focus();
							return false;
						}
						if(eval("document.form1.emphone_new_"+IDAry[i]+".value")=="") {
							alert("<?=$i_alert_pleaseselect." ".$i_StudentGuardian_EMPhone?>");	
							eval("document.form1.emphone_new_"+IDAry[i]).focus();
							return false;
						}
					}
				}
			}
		}
		temp = ","+document.getElementById('studentIDList').value+",";
		//if(document.getElementById('studentIDList').value.indexOf(document.getElementById('studentID').value)==-1) {
		if(isValueInCsvString(document.getElementById('studentIDList').value,document.getElementById('studentID').value)==-1){
			if(!confirm("<?=$Lang['AccountMgmt']['StudentNotYetLinkToParent']?>")) {
				return false;
			}
		} 
		
		var groupLength = document.form1.elements['GroupID'].length;
		var roleLength = document.form1.elements['RoleID'].length;

		
		for(i=0; i<groupLength; i++) {
			document.form1.elements['GroupID'][i].selected = true;	
		}
		
		for(i=0; i<roleLength; i++) {
			document.form1.elements['RoleID'][i].selected = true;	
		}

	}
	
	function goFinish() {
		document.form1.action = "new_update.php";
		document.form1.submit();	
	}
	
	function goURL(urlLink) {
		document.form1.action = urlLink;
		document.form1.submit();
	}
	</script>
	<script language="JavaScript">
	var ClickID = '';
	var callback_displayInfo = {
		success: function ( o )
	    {
		    var tmp_str = o.responseText;
		    document.getElementById('spanDisplay').innerHTML = tmp_str;
		    DisplayPosition();
		    // init thickbox for assigning eNotice
			initThickBox();
		}
	}
	

	function displayInfo(type, click) {
		ClickID = click;
		$("#task").val(type);
		var PostVar = Get_Form_Values(document.getElementById("form1"));
		
		$.post(
		"ajax_displayInfo.php", PostVar,
			function(ReturnData)
			{
				//alert(ReturnData);
				$('#spanDisplay').html(ReturnData);
				// init for thickbox display
				initThickBox()
			}
		);	
	}
	
	
// 	function displayInfo(type, click)
// 	{	
// 		ClickID = click;
// 		document.getElementById('task').value = type;
		
// 		obj = document.form1;
// 		YAHOO.util.Connect.setForm(obj);
// 		var path = "ajax_displayInfo.php";
// 	    var connectionObject = YAHOO.util.Connect.asyncRequest("POST", path, callback_displayInfo);
// 	}
	
	var callback_displayStudentID = {
		success: function ( o )
	    {
		    var tmp_str = o.responseText;
		    document.getElementById('spanStudentID').innerHTML = tmp_str;
		    DisplayPosition();
		}
	}

	function displayStudentID(type, click)
	{
		ClickID = click;
		$("#task").val(type);
		var PostVar = Get_Form_Values(document.getElementById("form1"));
			
		$.post(
		"ajax_displayInfo.php", PostVar,
			function(ReturnData)
			{
				//alert(ReturnData);
				$('#spanStudentID').html(ReturnData);
				DisplayPosition();
			}
		);	
	}
	
// 	function displayStudentID(type, click)
// 	{
// 		ClickID = click;
// 		document.getElementById('task').value = type;
// 		obj = document.form1;
// 		YAHOO.util.Connect.setForm(obj);
// 		var path = "ajax_displayInfo.php";
// 	    var connectionObject = YAHOO.util.Connect.asyncRequest("POST", path, callback_displayStudentID);
// 	}
	
	
	function getPosition(obj, direction)
	{
		var objStr = "obj";
		var pos_value = 0;
		while (typeof(eval(objStr))!="undefined" && eval(objStr + ".tagName")!="BODY")
		{
			pos_value += eval(objStr + "." + direction);
			objStr += ".offsetParent";
		}
	
		return pos_value;
	}
	
	function DisplayPosition(){
		
	  document.getElementById('spanDisplay').style.left=getPosition(document.getElementById(ClickID),'offsetLeft') +10;
	  document.getElementById('spanDisplay').style.top=getPosition(document.getElementById(ClickID),'offsetTop');
	  document.getElementById('spanDisplay').style.visibility='visible';
	}
	
	function addStudent() {
		if(document.getElementById('className').value=="" || document.getElementById('studentID').value=="") {
			alert("<?=$i_alert_pleaseselect." ".$i_general_class." ".$i_general_or." ".$i_identity_student?>");
		} else {
			duplicateStudent = 0;
			temp = ","+document.form1.studentIDList.value+",";
			

			//if(document.getElementById('studentIDList').value.indexOf(document.getElementById('studentID').value)!=-1){
			if(isValueInCsvString(document.getElementById('studentIDList').value,document.getElementById('studentID').value)!=-1){
				duplicateStudent = 1;
			}
			if(duplicateStudent==1) {
				alert("<?=$Lang['AccountMgmt']['DuplicateStudentToParent']?>");
			} else {
				document.form1.tsize.value = parseInt(document.form1.tsize.value) + 1;
				document.form1.newids.value += document.form1.newids.value!="" ? ","+document.form1.tsize.value : document.form1.tsize.value;
				document.form1.studentIDList.value += document.form1.studentIDList.value!="" ? ","+document.form1.studentID.value : document.form1.studentID.value;
				
				document.getElementById('ID').value = "";
				displayInfo('getStudent','spanDisplay');
								
				document.form1.className.options.selectedIndex = 0;
				displayStudentID('studentID', 'spanStudentID');			
			}
		}
	}
	
	function removeStudent(rid) 
	{
		
		var newids = document.getElementById('newids').value;
		if(newids==rid) newids="";
		newids = newids.replace(","+rid,"");
		newids = newids.replace(rid+",","");
		document.form1.newids.value = newids;
		
		if(document.form1.newids.value=="") {
			document.form1.tsize.value = 0;
			document.form1.studentIDList.value = "";
		}
		
		//alert(document.form1.hiddenOutput[].length);
		document.getElementById('ID').value = rid;
		displayInfo('getStudent', 'spanDisplay');
		
		
		/*
		var objTable = document.getElementById("studentTable");
		tableRows = objTable.rows;
		
		for(i=0;i<tableRows.length;i++)
			if(tableRows[i].id==rid){
				objTable.deleteRow(i);
				break;
			}
		*/
		
		document.getElementById('c').value -= 1;
		
	}
	
	function changeOptionByGuardian(emt, emtRef, total, rid, curPos) {
		if(emt=='mainGuardian') {
			for(i=0; i<=total; i++) {
				if(document.getElementById(emtRef+"_"+i) != undefined)
					document.getElementById(emtRef+"_"+i).className = "main_guardian";
			}
			document.getElementById(emtRef+"_"+curPos).className = (document.getElementById('mainGuardianFlag_'+rid).value==curPos) ? "main_guardian" : "main_guardian_selected";
			document.getElementById('mainGuardianFlag_'+rid).value = (document.getElementById('mainGuardianFlag_'+rid).value!=curPos || document.getElementById('mainGuardianFlag_'+rid).value=="") ? curPos : "";
		}
		if(emt=='sms') {
			for(i=0; i<=total; i++) {
				if(document.getElementById(emtRef+"_"+i) != undefined)
					document.getElementById(emtRef+"_"+i).className = "send_sms_to";
			}
			document.getElementById(emtRef+"_"+curPos).className = (document.getElementById('smsFlag_'+rid).value==curPos) ? "send_sms_to" : "send_sms_to_selected";
			document.getElementById('smsFlag_'+rid).value = (document.getElementById('smsFlag_'+rid).value!=curPos || document.getElementById('smsFlag_'+rid).value=="") ? curPos : "";
		}
		if(emt=='emergencyContact') {
			for(i=0; i<=total; i++) {
				if(document.getElementById(emtRef+"_"+i) != undefined)
					document.getElementById(emtRef+"_"+i).className = "emergency_contact";
			}
			document.getElementById(emtRef+"_"+curPos).className = (document.getElementById('emergencyContactFlag_'+rid).value==curPos) ? "emergency_contact" : "emergency_contact_selected";
			document.getElementById('emergencyContactFlag_'+rid).value = (document.getElementById('emergencyContactFlag_'+rid).value!=curPos || document.getElementById('emergencyContactFlag_'+rid).value=="") ? curPos : "";
		}
	}
	
	function isValueInCsvString(csv_string,value)
	{
		if(value == "") return 0;
		var csv_ary = csv_string.split(',');
		for(var i=0;i<csv_ary.length;i++)
		{
			if(value == csv_ary[i]){
				return 0;
			}
		}
		return -1;
	}
	
	function setAllCheckBox(val, name, element_name){
		var obj = $("#" + name + " input[name='TargetNotice[]']");
        var len=obj.length;
        var i=0;
        for(i=0; i<len; i++) {
        	if (obj[i].name==element_name && !obj[i].disabled)
            obj[i].checked=val;
        }
	}
	
	function showOption(index){
		document.getElementById('Notice_table_hide_'+index).style.display="none";
		document.getElementById('Notice_table_'+index).style.display="inline";
	}
	
	function hideOption(index){
		document.getElementById('Notice_table_'+index).style.display="none";
		document.getElementById('Notice_table_hide_'+index).style.display="inline";
	}
	
	function ajaxLoadAddNoticeContent(student_id, st_count){
	  	var url = 'ajax_displayInfo.php';
	  	var postContent = 'task=ajaxLoadAddNotice&StudentID='+student_id+'&st_count='+st_count;
		
		AddNoticeContentAjax = GetXmlHttpObject();   
	  	if (AddNoticeContentAjax == null)
	  	{
	    	alert (errAjax);
	    	return;
	  	}
	  	
		AddNoticeContentAjax.onreadystatechange = function() {
			if (AddNoticeContentAjax.readyState == 4) {
				document.getElementById('TB_ajaxContent').innerHTML = AddNoticeContentAjax.responseText;
			}
		};
	  	AddNoticeContentAjax.open("POST", url, true);
		AddNoticeContentAjax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		AddNoticeContentAjax.send(postContent);
	}
	
	function Refresh_Notice_Content_Table(student_id, st_count){
		Block_Element("notice_content_"+st_count);
		
		var url = 'ajax_displayInfo.php';
	  	var postContent = 'task=refreshNotice&StudentID='+student_id+'&st_count='+st_count;
		
		RefreshNoticeContentAjax = GetXmlHttpObject();
	  	if (RefreshNoticeContentAjax == null)
	  	{
	    	alert (errAjax);
	    	return;
	  	}
	  	
	  	RefreshNoticeContentAjax.onreadystatechange = function() {
			if (RefreshNoticeContentAjax.readyState == 4) {
				document.getElementById('notice_content_'+st_count).innerHTML = RefreshNoticeContentAjax.responseText;
				UnBlock_Element("notice_content_"+st_count);
			}
		};
	  	RefreshNoticeContentAjax.open("POST", url, true);
		RefreshNoticeContentAjax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		RefreshNoticeContentAjax.send(postContent);
	}
	
	function js_update_notice_list(student_id, st_count){
		if(countChecked(document.getElementById('LoadingForm'), 'TargetNotice[]') == 0){
			document.getElementById('submit_btn_tb').disabled  = false; 
			document.getElementById('submit_btn_tb').className  = "formbutton_v30 print_hide"; 
			
			alert('<?=$Lang['eNotice']['PleaseSelectOneNotice']?>');
		} else {
			$.post(
				"ajax_update_enotice.php", 
				$("#LoadingForm").serialize(),
				function(ReturnData)
				{
					if (ReturnData == 1){
						// do nothing
					}
					else {
						alert('<?=$Lang['eNotice']['FailedToSetNotice']?>');
					}
					$("#cancelBtn_tb").click();
					Refresh_Notice_Content_Table(student_id, st_count);
				}
			);
		}
	}
	
	</script>
	<form id="form1" name="form1" method="post" action="new_update.php" onSubmit="return goSubmit()">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	    <tr><td height="40"><?=$linterface->GET_NAVIGATION($PAGE_NAVIGATION)?></td></tr>
	    <tr><td height="40"><?=$linterface->GET_STEPS($STEPS_OBJ)?></td></tr>
	    <tr><td class="board_menu_closed">
			  
			  <table width="99%" border="0" cellspacing="0" cellpadding="0">
	                <tr> 
	                  <td class="main_content">
	                      <div class="table_board">
							<table class="form_table">
							  <tr>
							    <td colspan="2"><em class="form_sep_title"> - <?=$Lang['AccountMgmt']['ClassGroupInfo']?> -</em></td>
							  </tr>
							  <tr>
							    <td class="formfieldtitle" width="20%"><?=$i_adminmenu_group?></td>
							    <td >
								    <table cellpadding="0" cellspacing="0" border="0">
										<tr>
											<td><?php echo $lo->displayUserGroups(); ?></td>
										</tr>
								    </table>
							    </td>
							  </tr>
							  <tr>
							    <td class="formfieldtitle" width="20%"><?=$Lang['Header']['Menu']['Role']?></td>
							    <td >
								    <table cellpadding="0" cellspacing="0" border="0">
										<tr>
											<td><?= $lrole->displayRoleSelection(); ?></td>
										</tr>
								    </table>
							    </td>
							  </tr>
							</table>
							<p class="spacer"></p>
	                      	</div>
							<div class="table_board">
							<table class="form_table">
								<tr>
									<td colspan="2"><em class="form_sep_title"> - <?=$Lang['AccountMgmt']['RelatedToStudent']?> -</em></td>
								</tr>
								<!-- Older version
								<tr>
									<td class="field_title"><?=$i_identity_student?></td>
									<td >
										<div id="spanDisplay" class="sub_info_box"><table class='common_table_list' id='studentTable'></table></div>
										<span><?=$classMenu?></span>
										<span id="spanStudentID"><select name="studentID" id="studentID"><option>-- <?=$button_select?> --</option></select></span>
										<a href="javascript:addStudent()" class="thickbox">[ + ]</a>
									</td>
								</tr>
								-->
								<tr>
									<td>
										<div id="spanDisplay" class="sub_info_box">
											<table class='common_table_list' id='studentTable'></table>
										</div>
										<span><?=$classMenu?></span>
										<span id="spanStudentID"><input type="hidden" name="studentID" id="studentID" value=""><select name="studentIDDefault" id="studentIDDefault"><option>-- <?=$button_select?> --</option></select></span>
										<a href="javascript:addStudent(0)">[ + ]</a>
									</td>
								</tr>
							</table>
							<p class="spacer"></p>
							</div>
							<div class="edit_bottom">
	                          <p class="spacer"></p>
	                          <?= $linterface->GET_ACTION_BTN($button_submit, "submit")?>
	                          <?= $linterface->GET_ACTION_BTN($button_back, "button", "goURL('new.php')")?>
	                          <?= $linterface->GET_ACTION_BTN($button_cancel, "button", "goURL('$comeFrom')")?>
							  <p class="spacer"></p>
	                    	</div>
	                    </td>
	                </tr>
	              </table>
			</td>
		</tr>
	</table>
	<input type="hidden" name="comeFrom" id="comeFrom" value="<?=$comeFrom?>">
	<input type="hidden" name="task" id="task" value="">
	<input type="hidden" name="newids" id="newids" value="">
	<input type="hidden" name="studentIDList" id="studentIDList" value="">
	<input type="hidden" name="tsize" id="tsize" value="0">
	<input type="hidden" name="ClickID" id="ClickID" value="">
	<input type="hidden" name="ID" id="ID" value="">
	<input type="hidden" name="uid" id="uid" value="<?=$uid?>">
	<input type="hidden" name="targetClass" id="targetClass" value="<?=$targetClass?>">
	<input type="hidden" name="recordstatus" id="recordstatus" value="<?=$recordstatus?>">
	<input type="hidden" name="keyword" id="keyword" value="<?=$keyword?>">
	
	<input type="hidden" name="userlogin" id="userlogin" value="<?=$userlogin?>">
	<input type="hidden" name="pwd" id="pwd" value="<?=$pwd?>">
	<input type="hidden" name="email" id="email" value="<?=$email?>">
	<input type="hidden" name="status" id="status" value="<?=$status?>">
	<input type="hidden" name="chiname" id="chiname" value="<?=$chiname?>">
	<input type="hidden" name="engname" id="engname" value="<?=$engname?>">
	<input type="hidden" name="gender" id="gender" value="<?=$gender?>">
	<input type="hidden" name="address" id="address" value="<?=$address?>">
	<input type="hidden" name="country" id="country" value="<?=$country?>">
	<input type="hidden" name="homePhone" id="homePhone" value="<?=$homePhone?>">
	<input type="hidden" name="officePhone" id="officePhone" value="<?=$officePhone?>">
	<input type="hidden" name="mobilePhone" id="mobilePhone" value="<?=$mobilePhone?>">
	<input type="hidden" name="faxPhone" id="faxPhone" value="<?=$faxPhone?>">
	<input type="hidden" name="hkid" id="hkid" value="<?=$hkid?>">
	<input type="hidden" name="barcode" id="barcode" value="<?=$barcode?>">
	<input type="hidden" name="open_webmail" id="open_webmail" value="<?=$open_webmail?>">
	<input type="hidden" name="remark" id="remark" value="<?=$remark?>">
	<input type="hidden" name="Quota" id="Quota" value="<?=$Quota?>">
	<input type="hidden" name="EmailStatus" id="EmailStatus" value="<?=$EmailStatus?>">
	<?php if($plugin['radius_server']){ ?>
		<input type="hidden" name="enable_wifi_access" id="enable_wifi_access" value="<?=$enable_wifi_access?>" />
	<?php } ?>
	</form>
	
	<script language="javascript">
		document.form1.newids.value = "";
	</script>
<?php
}
intranet_closedb();
$linterface->LAYOUT_STOP();
?>