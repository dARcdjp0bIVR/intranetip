<?php
# modifying by : 

##### Change Log [Start] #####
#
#	Date:	2015-05-22 (Bill)
#			create file
#
###### Change Log [End] ######

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if(!($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Parent"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"])) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$laccount = new libaccountmgmt();
$lnotice = new libnotice();

$values = "";
$delimiter = "";
$notice_count = count($TargetNotice);
if($notice_count > 0){
	for($i=0; $i<$notice_count; $i++){
		$values .= "$delimiter ('".$TargetNotice[$i]."', '$TargetStudentID', '".addslashes($TargetStudentName)."', 0, now(), now())";
		$delimiter = ",";
	}
	
	$sql = "INSERT IGNORE INTO INTRANET_NOTICE_REPLY (NoticeID,StudentID,StudentName,RecordStatus,DateInput,DateModified)
				VALUES $values";
	$success = $lnotice->db_db_query($sql);
}

echo ($success > 0);

?>