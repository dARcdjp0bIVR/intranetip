<?php
# using: 

############ Change Log Start ###############
#	Date:	2020-09-08 Henry
#			Bug fix for fail to search parent if input Chinese name [Case#P188827]
#
#	Date:	2019-03-12 Carlos
#			$sys_custom['iMail']['ExportForwardingEmail'] - Added export forwarding emails for Webmail.
#
#   Date:   2018-12-17 Isaac
#           added $save_keyword to prevent sql injection 
#           added FaxNo, HomeTelNo, OfficeTelNo and MobileTelNo to the keyword search
#
#	Date:	2015-12-30 Carlos
#			$plugin['radius_server'] - added table tool buttons for enable/disable Wi-Fi access and export Wi-Fi access status list.
#
#	Date:	2014-11-11	Omas
#			Modified SQL Can sort by Student Name and improve performance 
#
#	Date:	2014-03-11	Carlos
#			Added [Import Forwarding Emails] & [Export Forwarding Emails] buttons for Lassel College
#
#	Date:	2013-02-08	YatWoon
#			use "user_id[]" instead of userID[]
#
#	Date:	2011-01-10	YatWoon
#			- set cookies
#
#	Date:	2011-01-06	YatWoon
#			- IP25 UI
#			- Add "Active", "Suspend" tool options
#
#	Date:	2010-06-21	YatWoon
#			add online help button
#
############ Change Log End ###############

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

# set cookies
$arrCookies[] = array("ck_page_size", "numPerPage");
$arrCookies[] = array("ck_page_no", "pageNo");
$arrCookies[] = array("ck_page_order", "order");
$arrCookies[] = array("ck_page_field", "field");

$arrCookies[] = array("ck_parent_targetClass", "targetClass");
$arrCookies[] = array("ck_parent_recordstatus", "recordstatus");
$arrCookies[] = array("ck_parent_keyword", "keyword");
if(isset($clearCoo) && $clearCoo == 1)
{
	clearCookies($arrCookies);
} 
else 
{
	updateGetCookies($arrCookies);
}

intranet_auth();
intranet_opendb();

if(!($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Parent"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"])) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$laccount = new libaccountmgmt();
$linterface = new interface_html();
$lclass = new libclass();

$CurrentPageArr['ParentMgmt'] = 1;
$CurrentPage = "Mgmt_Account";

$TAGS_OBJ[] = array($Lang['AccountMgmt']['UserList'],"index.php?clearCoo=1",1);
$TAGS_OBJ[] = array($Lang['AccountMgmt']['ParentWithoutChildInClass'],"noChildInSchool.php?clearCoo=1",0);
$MODULE_OBJ['title'] = $Lang['Header']['Menu']['ParentAccount'];

# online help button
//$onlineHelpBtn = gen_online_help_btn_and_layer('user','parent');
//$TAGS_OBJ_RIGHT[] = array($onlineHelpBtn);

$toolbar .= $linterface->GET_LNK_NEW("javascript:newUser()",$button_new,"","","",0);
$toolbar .= $linterface->GET_LNK_IMPORT("javascript:goImport()",$button_import,"","","",0);
$toolbar .= $linterface->GET_LNK_EXPORT("javascript:goExport()",$button_export,"","","",0);
$toolbar .= $linterface->GET_LNK_EMAIL("javascript:checkPost(document.form1,'email.php')",$button_email,"","","",0);
if($plugin['imail_gamma'] && $sys_custom['iMailPlus']['BatchSetForwardingEmail']){
	$toolbar .= $linterface->GET_LNK_IMPORT("../import/import_forwarding_email.php?TabID=".USERTYPE_PARENT,$Lang['AccountMgmt']['ImportForwardingEmails'],"","","",0);
	$toolbar .= $linterface->GET_LNK_EXPORT("../export_forwarding_email.php?TabID=".USERTYPE_PARENT,$Lang['AccountMgmt']['ExportForwardingEmails'],"","","",0);
}
if($plugin['webmail'] && $sys_custom['iMail']['ExportForwardingEmail']){
	$toolbar .= $linterface->GET_LNK_EXPORT("../export_forwarding_email.php?TabID=".USERTYPE_PARENT,$Lang['AccountMgmt']['ExportForwardingEmails'],"","","",0);
}
if($plugin['radius_server']){
	$toolbar .= $linterface->GET_LNK_EXPORT("../export_wifi_status.php?TabID=".USERTYPE_PARENT,$Lang['AccountMgmt']['ExportWiFiAccessStatus'],"","","",0);
}

# Record Status 
$recordStatusMenu = "<select name=\"recordstatus\" id=\"recordstatus\" onChange=\"reloadForm()\">";
$recordStatusMenu .= "<option value=''".((!isset($recordstatus) || $recordstatus=="") ? " selected" : "").">$i_Discipline_Detention_All_Status</option>";
$recordStatusMenu .= "<option value='1'".(($recordstatus=='1') ? " selected" : "").">{$Lang['Status']['Active']}</option>";
$recordStatusMenu .= "<option value='0'".(($recordstatus=='0') ? " selected" : "").">{$Lang['Status']['Suspended']}</option>";
$recordStatusMenu .= "</select>";

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
if($order == "") $order = 1;
if($field == "") $field = 0;
$pageSizeChangeEnabled = true;

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;

$li = new libdbtable2007($field, $order, $pageNo);

$targetClassMenu = $lclass->getSelectClassWithWholeForm("name=\"targetClass\" onChange=\"reloadForm()\"", $targetClass, $Lang['AccountMgmt']['AllClassesParent'], "");

$AcademicYearID = Get_Current_Academic_Year_ID();

if($targetClass != "") {
	if($targetClass=='0') {					# all classes
		# do nothing	
	} else if(is_numeric($targetClass)) {	# All students in specific form
		$conds .= " AND y.YearID=$targetClass";
	} else {								# All students in specific class
		$conds .= " AND yc.YearClassID=".substr($targetClass, 2);
	}
}

# record status
if(isset($recordstatus) && $recordstatus==1)
	$conds .= " AND USR.RecordStatus=".STATUS_APPROVED;
else if(isset($recordstatus) && $recordstatus!=1 && $recordstatus!="")
	$conds .= " AND USR.RecordStatus=".STATUS_SUSPENDED;

if($keyword != ""){	
    $safe_keyword = htmlentities($li->Get_Safe_Sql_Like_Query($keyword),ENT_QUOTES);
    $conds .= " AND (USR.UserLogin like '%$safe_keyword%' OR
                USR.UserEmail like '%$safe_keyword%' OR
                USR.EnglishName like '%$safe_keyword%' OR
                USR.ChineseName like '%".$li->Get_Safe_Sql_Like_Query($keyword)."%' OR
                USR.ClassName like '%$safe_keyword%' OR
                USR.WebSamsRegNo like '%$safe_keyword%' OR
                USR.HomeTelNo like '%$safe_keyword%' OR
                USR.OfficeTelNo like '%$safe_keyword%' OR
                USR.FaxNo like '%$safe_keyword%' OR
                USR.MobileTelNo like '%$safe_keyword%'
				)";
}
	
//$sql = "SELECT
//			IFNULL(IF(USR.EnglishName='', '---', USR.EnglishName),'---') as EnglishName,
//			IFNULL(IF(USR.ChineseName='', '---', USR.ChineseName),'---') as ChineseName,
//			USR.UserID as studentName,
//			USR.UserID as phone,
//			USR.UserLogin,
//			IFNULL(USR.LastUsed, '---') as LastUsed,
//			CASE USR.RecordStatus
//				WHEN 0 THEN '". $Lang['Status']['Suspended'] ."'
//				WHEN 1 THEN '". $Lang['Status']['Active'] ."'
//				ELSE '$i_status_suspended' END as recordStatus,
//			CONCAT('<input type=\'checkbox\' name=\'user_id[]\' id=\'user_id[]\' value=', USR.UserID ,'>') as checkbox,
//			USR.UserID
//		FROM 
//			INTRANET_USER USR INNER JOIN
//			INTRANET_PARENTRELATION pr ON (pr.ParentID=USR.UserID) LEFT OUTER JOIN
//			YEAR_CLASS_USER ycu ON (ycu.UserID=pr.StudentID) LEFT OUTER JOIN
//			YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID AND yc.AcademicYearID=$AcademicYearID) LEFT OUTER JOIN
//			YEAR y ON (y.YearID=yc.YearID) 
//		WHERE
//			USR.RecordType = ".TYPE_PARENT." and USR.RecordStatus IN (0,1,2)		
//			
//			$conds 
//		GROUP BY
//			USR.UserID
//			";

$sortingField = Get_Lang_Selection('IF(stu.ChineseName=\'\' OR stu.ChineseName is null, stu.EnglishName, stu.ChineseName)', 'IF(stu.EnglishName=\'\' OR stu.EnglishName is null, stu.ChineseName, stu.EnglishName)');
$classNameField = Get_Lang_Selection('yc.ClassTitleB5','yc.ClassTitleEN');
if($sys_custom['SISUserManagement']){
	$editPage = 'edit_sis.php';	
}
else {
	$editPage = 'edit.php';
}

$sql = "SELECT YearClassID FROM YEAR_CLASS Where AcademicYearID IN ($AcademicYearID)";
$currentYearClassIdAry = $li->ReturnVector($sql);
$currentYearClassId = implode("','" ,$currentYearClassIdAry);

$sql = "SELECT
			IFNULL(IF(USR.EnglishName='', '---', USR.EnglishName),'---') as EnglishName,
			IFNULL(IF(USR.ChineseName='', '---', USR.ChineseName),'---') as ChineseName,
			/*start of link*/
			Group_Concat(
				Distinct CONCAT( 	'<a href=\"javascript:goURL(\'../StudentMgmt/".$editPage."\',' , stu.UserID , ');\">', 
									$sortingField,
					            	IF ($classNameField is null, 
										'',
										CONCAT(	' (' , 
												IF(".$classNameField." is null, '', ".$classNameField."),
												IF(ycu.ClassNumber is null, '', CONCAT('-',ycu.ClassNumber)),
												')'
										)
									),									 
									'</a>'
								) 
				Separator '<br>' 
			) as studentNameLink,
			
			/*end of link
			/* USR.UserID as phone, */
			CONCAT_WS(' / ', IF(USR.HomeTelNo='', null, USR.HomeTelNo), IF(USR.OfficeTelNo='', null, USR.OfficeTelNo), IF(USR.MobileTelNo='', null, USR.MobileTelNo)) as phone,
			USR.UserLogin,
			IFNULL(USR.LastUsed, '---') as LastUsed,
			CASE USR.RecordStatus
				WHEN 0 THEN '". $Lang['Status']['Suspended'] ."'
				WHEN 1 THEN '". $Lang['Status']['Active'] ."'
				ELSE '$i_status_suspended' END as recordStatus,
			CONCAT('<input type=\'checkbox\' name=\'user_id[]\' id=\'user_id[]\' value=', USR.UserID ,'>') as checkbox,
			USR.UserID,
			group_concat(distinct stu.EnglishName SEPARATOR '<br/>') as studentName
		FROM 
			INTRANET_USER USR INNER JOIN
			INTRANET_PARENTRELATION pr ON (pr.ParentID=USR.UserID) LEFT OUTER JOIN
			YEAR_CLASS_USER ycu ON (ycu.UserID=pr.StudentID AND ycu.YearClassID in ('".$currentYearClassId."')) LEFT OUTER JOIN
			YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID AND yc.AcademicYearID='$AcademicYearID') LEFT OUTER JOIN
			YEAR y ON (y.YearID=yc.YearID)
			LEFT OUTER JOIN INTRANET_USER stu ON (pr.StudentID = stu.UserID)
		WHERE
			USR.RecordType = '".TYPE_PARENT."' and USR.RecordStatus IN (0,1,2)
			$conds 
		GROUP BY
			USR.UserID
			";

$li->sql = $sql;
$li->field_array = array("EnglishName", "ChineseName", "studentName", "UserLogin", "LastUsed", "recordStatus");
$li->no_col = sizeof($li->field_array)+3;
$li->IsColOff = "UserMgmtParentAccount";
$pos = 0;
$li->column_list .= "<th class='num_check'>#</th>\n";
$li->column_list .= "<th width='15%' >".$li->column($pos++, $i_UserEnglishName)."</th>\n";
$li->column_list .= "<th width='15%' >".$li->column($pos++, $i_UserChineseName)."</th>\n";
$li->column_list .= "<th width='25%' >".$li->column($pos++, $i_identity_student)."</th>\n";
$li->column_list .= "<th width='10%' >".$Lang['AccountMgmt']['Tel']."</th>\n";
$li->column_list .= "<th width='15%' >".$li->column($pos++, $i_UserLogin)."</th>\n";
$li->column_list .= "<th width='10%' >".$li->column($pos++, $i_frontpage_eclass_lastlogin)."</th>\n";
$li->column_list .= "<th width='10%' >".$li->column($pos++, $i_UserRecordStatus)."</th>\n";
$li->column_list .= "<th width='1'>".$li->check("user_id[]")."</th>\n";

$MODULE_OBJ = $laccount->GET_MODULE_OBJ_ARR();



$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$xmsg]);

?>
<script language="javascript">
function reloadForm() {
	document.form1.action = "index.php";
	document.form1.submit();
}

function goEdit(id) {
	document.form1.action = "edit.php";
	document.getElementById('uid').value = id;
	document.form1.submit();
}

function newUser() {
	document.form1.action = "new.php";
	document.form1.submit();
}

function goExport() {
	document.form1.action = "../export.php?TabID=<?=TYPE_PARENT?>";
	document.form1.submit();
}

function goImport() {
	window.location.href ="../import/import.php?TabID=<?=TYPE_PARENT?>";
}

function changeFormAction(obj,element,url,type) {
	if(countChecked(obj,element)==0)
		alert(globalAlertMsg2);
	else {
		if(confirm(globalAlertMsgSendPassword)) {
			document.form1.action = url;
			document.form1.submit();
		}
	}
}

function goURL(thisURL, uid) {
	document.form1.action = thisURL;
	document.form1.uid.value = uid;
	document.form1.submit();
}
</script>

<form name="form1" method="post" action="">

<div class="content_top_tool">
	<?=$toolbar?>
	<?=$linterface->Get_Search_Box_Div("keyword", $keyword)?>
<br style="clear:both" />
</div>

<div class="table_board">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr class="table-action-bar">
	<td valign="bottom">
		<?=$targetClassMenu?> <?=$recordStatusMenu?>
	</td>
	<td valign="bottom">
		<div class="common_table_tool">
		<a href="javascript:changeFormAction(document.form1,'user_id[]','../email_password.php')" class="tool_email"><?=$Lang['AccountMgmt']['SendResetPasswordEmail']?></a>
		<? if($plugin['radius_server']){ ?>
        <a href="javascript:checkEditMultiple(document.form1,'user_id[]','../enable_wifi_access.php')" class="tool_approve"><?=$Lang['AccountMgmt']['EnableWifiAccess']?></a>
        <a href="javascript:checkEditMultiple(document.form1,'user_id[]','../disable_wifi_access.php')" class="tool_reject"><?=$Lang['AccountMgmt']['DisableWifiAccess']?></a>
        <? } ?>
		<? if($recordstatus!=1) {?>
		<a href="javascript:checkActivate(document.form1,'user_id[]','../approve.php')" class="tool_approve"><?=$Lang['Status']['Activate']?></a>
		<? } ?>
		<? if($recordstatus=="" || $recordstatus==1) {?>
		<a href="javascript:checkSuspend(document.form1,'user_id[]','../suspend.php')" class="tool_reject"><?=$Lang['Status']['Suspend']?></a>
		<? } ?>
		<a href="javascript:checkRemove(document.form1,'user_id[]','remove.php','<?=$Lang['AccountMgmt']['DeleteUserWarning']?>')" class="tool_delete"><?=$button_delete?></a>
		</div>
	</td>
</tr>
</table>

<?= $li->display()?>

</div>


<input type="hidden" name="uid" id="uid" value="" />
<input type="hidden" name="pageNo" id="pageNo" value="<?=$li->pageNo; ?>" />
<input type="hidden" name="order" id="order" value="<?=$li->order; ?>" />
<input type="hidden" name="field" id="field" value="<?=$li->field; ?>" />
<input type="hidden" name="page_size_change" id="page_size_change" value="" />
<input type="hidden" name="numPerPage" id="numPerPage" value="<?=$li->page_size?>" />
<input type="hidden" name="comeFrom" value="/home/eAdmin/AccountMgmt/ParentMgmt/index.php">
<input type="hidden" name="clearCoo" id="clearCoo" value="" />

</form>
</body>

<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>