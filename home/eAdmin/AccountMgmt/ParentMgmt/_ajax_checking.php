<?php
# modifying by : henry

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");


intranet_auth();
intranet_opendb();

if(!($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"])) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}


$laccount = new libaccountmgmt();

if($task=='userlogin')
{
	if($userlogin!="") {
		$sql = "SELECT UserID FROM INTRANET_USER WHERE UserLogin='$userlogin'";
		$result = $laccount->returnVector($sql);
		
		if($result[0]!=0) {
			$output = "<font color='#FF0000'>\"$userlogin\" ".$Lang['AccountMgmt']['IsNotAvailable']."</font>";	
			$output .= "<input type=\"hidden\" name=\"available\" id=\"available\" value=\"0\">";
		} else {
			$output = "<font color='#008F37'>\"$userlogin\" ".$Lang['AccountMgmt']['IsAvailable']."</font>";
			$output .= "<input type=\"hidden\" name=\"available\" id=\"available\" value=\"1\">";	
		}
	}
	
}


echo $output;
?>
