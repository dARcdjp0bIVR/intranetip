<?php
# modifying by : 

##### Change Log [Start] #####
#
#	Date:	2015-05-22 (Bill)
#			modified task "getStudent", added task "refreshNotice", ajaxLoadAddNotice to support assign notice function
#
###### Change Log [End] ######

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");


intranet_auth();
intranet_opendb();

if(!($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Parent"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"])) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}


$laccount = new libaccountmgmt();
$lnotice = new libnotice();
$ldiscipline = new libdisciplinev12();
$linterface = new interface_html();


if($task=='getStudent')
{
	$c = 0;
	$i = 0;
	
	if($newids != "") {
		$newIDAry = explode(',', $newids);
		$studentAry = explode(',', $studentIDList);
		
		for($i=0; $i<sizeof($newIDAry); $i++) {
			$isMain = "";
			$isSMS = "";
			$IsEmergencyContact = "";
			$studentData = $laccount->getUserInfoByID($studentAry[$newIDAry[$i]-1]);
			$middleOutput[$c] .= "
					<tr class='sub_row' id='".($newIDAry[$i])."'>";

			// Student Info table
			$middleOutput[$c] .= "		
						<td class='num_check' width='1'>".($i+1).".</td>
						<td width='6%'>".Get_Lang_Selection($studentData['ChineseName'], $studentData['EnglishName'])."</td>
						<td width='5%'>".($studentData['ClassName'].(($studentData['ClassNumber']!="")?"-".$studentData['ClassNumber'] : ""))."</td>
						<td width='15%'>
							<table class=' form_table inside_form_table'>";
			
			$sql = "SELECT ".Get_Lang_Selection("a.ChName","a.EnName")." as Name, a.isMain, a.isSMS, b.IsEmergencyContact, a.RecordID FROM $eclass_db.GUARDIAN_STUDENT a LEFT OUTER JOIN $eclass_db.GUARDIAN_STUDENT_EXT_1 b ON (a.RecordID=b.ExtendRecordID) WHERE a.UserID=".$studentAry[$newIDAry[$i]-1];
			$result = $laccount->returnArray($sql, 4);
			
			$linkedGuardianID = "";
			# Guardian Info (display when editing parent)
			for($j=0; $j<sizeof($result); $j++) {
			
				$guardianData = $result[$j];
				
				if($guardianData['isMain']==1) 
					$isMain = (isset(${"mainGuardianFlag_".$newIDAry[$i]})) ? (${"mainGuardianFlag_".$newIDAry[$i]}) : $j+1;
				else if($isMain=="")
					$isMain = ${"mainGuardianFlag_".$newIDAry[$i]};
				
					
				if($guardianData['isSMS']==1) 
					$isSMS = (isset(${"smsFlag_".$newIDAry[$i]})) ? (${"smsFlag_".$newIDAry[$i]}) : $j+1;
				else if($isSMS=="")
					$isSMS = ${"smsFlag_".$newIDAry[$i]};
					
				if($guardianData['IsEmergencyContact']==1) 
					$IsEmergencyContact = (isset(${"emergencyContactFlag_".$newIDAry[$i]})) ? (${"emergencyContactFlag_".$newIDAry[$i]}) : $j+1;
				else if($IsEmergencyContact=="")
					$IsEmergencyContact = ${"emergencyContactFlag_".$newIDAry[$i]};
				
				$middleOutput[$c] .= "
								<tr>
									<td>".$guardianData['Name']."</td>
									<td><div class='guardian_option' id='spanOptions_{$studentAry[$newIDAry[$i]-1]}_{$newIDAry[$i]}'> 
										<a href=\"javascript:changeOptionByGuardian('mainGuardian','main_guardian_".$studentAry[$newIDAry[$i]-1]."_".$newIDAry[$i]."','".sizeof($result)."','{$newIDAry[$i]}', '".($j+1)."')\" class='main_guardian".(($isMain==$j+1)?"_selected":"")."' id='main_guardian_{$studentAry[$newIDAry[$i]-1]}_{$newIDAry[$i]}_".($j+1)."' title='".$Lang['AccountMgmt']['MainGuardian']."'>&nbsp;</a> 
										<a href=\"javascript:changeOptionByGuardian('sms','sms_".$studentAry[$newIDAry[$i]-1]."_".$newIDAry[$i]."','".sizeof($result)."','{$newIDAry[$i]}', '".($j+1)."')\" class='send_sms_to".(($isSMS==$j+1)?"_selected":"")."' id='sms_{$studentAry[$newIDAry[$i]-1]}_{$newIDAry[$i]}_".($j+1)."' title='".$Lang['AccountMgmt']['ReceiveSMS']."'>&nbsp;</a> 
										<a href=\"javascript:changeOptionByGuardian('emergencyContact','emergencyContact_".$studentAry[$newIDAry[$i]-1]."_".$newIDAry[$i]."','".sizeof($result)."','{$newIDAry[$i]}', '".($j+1)."')\" class='emergency_contact".(($IsEmergencyContact==$j+1)?"_selected":"")."' id='emergencyContact_{$studentAry[$newIDAry[$i]-1]}_{$newIDAry[$i]}_".($j+1)."'  title='".$Lang['AccountMgmt']['EmergencyContact']."'>&nbsp;</a> 
										</div>
										<br />
									</td>
								</tr>";
								
				$linkedGuardianID .= ($linkedGuardianID=="") ? $guardianData['RecordID'] : ",".$guardianData['RecordID'];
				
			}
			
			if(sizeof($result)==0) {
				$isMain = 0;
				$isSMS = 0;
				$IsEmergencyContact = 0;
			}
			$middleOutput[$c] .= "<input type='hidden' name='mainGuardianFlag_{$newIDAry[$i]}' id='mainGuardianFlag_{$newIDAry[$i]}' value='{$isMain}'>";
			$middleOutput[$c] .= "<input type='hidden' name='smsFlag_{$newIDAry[$i]}' id='smsFlag_{$newIDAry[$i]}' value='{$isSMS}'>";
			$middleOutput[$c] .= "<input type='hidden' name='emergencyContactFlag_{$newIDAry[$i]}' id='emergencyContactFlag_{$newIDAry[$i]}' value='$IsEmergencyContact'>";
			$middleOutput[$c] .= "<input type='hidden' name='linkedGuardianID_{$newIDAry[$i]}' id='linkedGuardianID_{$newIDAry[$i]}' value='$linkedGuardianID'>";
			$middleOutput[$c] .= "
							</table>              
						</td>";
			
			// Guardian Info table
			$middleOutput[$c] .= "			
						<td width='26%'>
							<input name='guardianCheck_".($newIDAry[$i])."' type='checkbox' id='guardianCheck_".($newIDAry[$i])."' ".((${"guardianCheck_".$newIDAry[$i]}=="on" || sizeof($result)==0)?" checked" : "")." onClick=\"if(this.checked==true) {document.getElementById('spanNewGuardian".($newIDAry[$i])."').style.display = 'inline';} else {document.getElementById('spanNewGuardian".($newIDAry[$i])."').style.display = 'none';} \"/>
							<div id='spanNewGuardian".($newIDAry[$i])."' style='display:".((${"guardianCheck_".$newIDAry[$i]}=="on" || sizeof($result)==0)?" inline" : "none")."'>
							<fieldset class='form_sub_option_form' style='margin-left: 5px;'>
							<table class='form_table inside_form_table'>
								<tr>
									<td colspan='2' style='padding: 0px 3px;'><em class='form_sep_title' style='margin-top:0px'> - ".$i_Discipline_Details."-</em></td>
								</tr>
								<tr>
									<td class='field_title' style='width: 40%'><span class='tabletextrequire'>*</span>".$ec_iPortfolio['relation']."</td>
									<td>
										<select name='relation_new_".($newIDAry[$i])."' id='relation_new_".($newIDAry[$i])."'>
											<option value=''>- ".$i_alert_pleaseselect.' '.$i_general_Type." -</option>
											<option value ='01'".((${"relation_new_".$newIDAry[$i]}=="01")?" selected" : "").">".$ec_guardian['01']."</option>
											<option value ='02'".((${"relation_new_".$newIDAry[$i]}=="02")?" selected" : "").">".$ec_guardian['02']."</option>
											<option value ='03'".((${"relation_new_".$newIDAry[$i]}=="03")?" selected" : "").">".$ec_guardian['03']."</option>
											<option value ='04'".((${"relation_new_".$newIDAry[$i]}=="04")?" selected" : "").">".$ec_guardian['04']."</option>
											<option value ='05'".((${"relation_new_".$newIDAry[$i]}=="05")?" selected" : "").">".$ec_guardian['05']."</option>
											<option value ='06'".((${"relation_new_".$newIDAry[$i]}=="06")?" selected" : "").">".$ec_guardian['06']."</option>
											<option value ='07'".((${"relation_new_".$newIDAry[$i]}=="07")?" selected" : "").">".$ec_guardian['07']."</option>
											<option value ='08'".((${"relation_new_".$newIDAry[$i]}=="08")?" selected" : "").">".$ec_guardian['08']."</option>
										</select>
									</td>
								</tr>
								<tr>
									<td class='field_title' style='width: 40%'><span class='tabletextrequire'>*</span>".$i_StudentGuardian_EMPhone."</td>
									<td><input name='emphone_new_".($newIDAry[$i])."' type='text' id='emphone_new_".($newIDAry[$i])."' class='textboxnum' value='".((${"emphone_new_".$newIDAry[$i]}!="")?${"emphone_new_".$newIDAry[$i]} : "")."'/>
									<br /></td>
								</tr>
								<tr>
									<td class='field_title' style='width: 40%'>".$i_StudentGuardian_Occupation." </td>
									<td><input name='occupation_new_".($newIDAry[$i])."' type='text' id='occupation_new_".($newIDAry[$i])."' value='".((${"occupation_new_".$newIDAry[$i]}!="")?${"occupation_new_".$newIDAry[$i]} : "")."'/><br /></td>
								</tr>
								<tr>
									<td class='field_title' style='width: 40%'>".$i_Discipline_Statistics_Options."</td>
									<td>
										<div class='guardian_option'>
										<a href=\"javascript:changeOptionByGuardian('mainGuardian','main_guardian_".$studentAry[$newIDAry[$i]-1]."_".$newIDAry[$i]."','".sizeof($result)."','{$newIDAry[$i]}', '0')\" class='".((($isMain)=='0' || sizeof($result)==0)?"main_guardian_selected":"main_guardian")."' id='main_guardian_{$studentAry[$newIDAry[$i]-1]}_{$newIDAry[$i]}_0' title='".$Lang['AccountMgmt']['MainGuardian']."'>&nbsp;</a> 
										<a href=\"javascript:changeOptionByGuardian('sms','sms_".$studentAry[$newIDAry[$i]-1]."_".$newIDAry[$i]."','".sizeof($result)."','{$newIDAry[$i]}', '0')\" id='sms_{$studentAry[$newIDAry[$i]-1]}_{$newIDAry[$i]}_0' class='".((($isSMS)=='0' || sizeof($result)==0)?"send_sms_to_selected":"send_sms_to")."' title='".$Lang['AccountMgmt']['ReceiveSMS']."'>&nbsp;</a> 
										<a href=\"javascript:changeOptionByGuardian('emergencyContact','emergencyContact_".$studentAry[$newIDAry[$i]-1]."_".$newIDAry[$i]."','".sizeof($result)."','{$newIDAry[$i]}', '0')\" id='emergencyContact_{$studentAry[$newIDAry[$i]-1]}_{$newIDAry[$i]}_0' class='".((($IsEmergencyContact)=='0' || sizeof($result)==0)?"emergency_contact_selected":"emergency_contact")."'  title='".$Lang['AccountMgmt']['EmergencyContact']."'>&nbsp;</a> 
										</div><br />
									</td>
								</tr>
							</table>
							<input type='hidden' name='uid_".$newIDAry[$i]."' id='uid_".$newIDAry[$i]."' value='".$studentAry[$newIDAry[$i]-1]."'>
							<!--<a href='#'>[ ".$Lang['AccountMgmt']['CopyToAll']." ]</a>-->
							</fieldset>
						</td>";
			
			# eNotice Info
			$current_id = ($newIDAry[$i]-1);
			$academicYearID = Get_Current_Academic_Year_ID();		
			list($yearStartDate, $yearEndDate) = getPeriodOfAcademicYear($academicYearID);
			
			# related eNotice in current academic year
			$sql = "SELECT 
						a.NoticeID, a.Title, NoticeNumber as NoticeCode, a.TargetType
                    FROM INTRANET_NOTICE_REPLY as c
                        INNER JOIN INTRANET_NOTICE as a ON a.NoticeID = c.NoticeID
                    WHERE c.StudentID = '".$studentAry[$current_id]."' AND a.RecordStatus = 1 AND a.Module IS NULL
					 	AND a.DateStart >= '$yearStartDate' AND a.DateEnd <= '$yearEndDate'
					GROUP BY a.NoticeID
					ORDER BY a.NoticeID";
			$notice_result = BuildMultiKeyAssoc($laccount->returnArray($sql, 4), array("TargetType", "NoticeID"));
			
			# Parent-signed Notice
			$parent_notice = $notice_result['P'];
			$pnotice_content = "";
			$pnotice_count = 1;
			if(count($parent_notice) > 0){
				foreach($parent_notice as $current_notice){
					$pnotice_content .= "<tr><td>$pnotice_count</td><td>".$current_notice['NoticeCode']."</td><td>".$current_notice['Title']."</td></tr>\n";
					$pnotice_count++;
				}
			} else {
				$pnotice_content .= "<tr><td colspan='3' style='text-align:center'>".$Lang['General']['NoRecordFound']."</td></tr>\n";
			}
			
			# Student-signed Notice
			$student_notice = $notice_result['S'];
			$snotice_content = "";
			$snotice_count = 1;
			if(count($student_notice) > 0){
				foreach($student_notice as $current_notice){
					$snotice_content .= "<tr><td>$snotice_count</td><td>".$current_notice['NoticeCode']."</td><td>".$current_notice['Title']."</td></tr>\n";
					$snotice_count++;
				}
			} else {
				$snotice_content .= "<tr><td colspan='3' style='text-align:center'>".$Lang['General']['NoRecordFound']."</td></tr>\n";
			}
			
			// Notice Info table 
			$middleOutput[$c] .= "
							<td width='46%'>
								<div class='table_row_tool row_content_tool'>".
									$linterface->Get_Thickbox_Link(515, 775, "add_dim", $Lang['eNotice']['AssignNoticeToStudent'], "ajaxLoadAddNoticeContent('".$studentAry[$current_id]."', '$current_id');", "FakeLayer_$current_id", "", "thickbox_$current_id")."
								</div>
								<div id='notice_content_$current_id' width='100%'>
								<table width='100%'>
									<tr><td style='border-right:0px'>
										<fieldset id=\"Notice_table_P$current_id\" class=\"form_sub_option_form\" style=\"margin-left:5px; display:none;\" width='100%'>".
											$linterface->GET_NAVIGATION2($Lang['eNotice']['SchoolNotice'])."
											<span id=\"spanHidePNoticeOption_$current_id\">
												<a href=\"javascript:hideOption('P$current_id');\" class=\"contenttool\">
													<img src=\"$image_path/$LAYOUT_SKIN/icon_hide_option.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\">
												</a>
											</span>
											<table>
												<tr>
													<td colspan='3' style='padding: 0px 3px;'></td>
												</tr>
												<tr>
													<td width='1'>#</td>
													<td width='40%'>$i_Notice_NoticeNumber</td>
													<td width='60%'>$i_Notice_Title</td>
												</tr>
												$pnotice_content
											</table>
										</fieldset>
										<fieldset id=\"Notice_table_hide_P$current_id\" class=\"form_sub_option_form\" style=\"margin-left:5px; display:inline;\" width=\"100%\">".
											$linterface->GET_NAVIGATION2($Lang['eNotice']['SchoolNotice'])."
											<span id=\"spanShowPNoticeOption_$current_id\">
												<a href=\"javascript:showOption('P$current_id');\" class=\"contenttool\">
													<img src=\"$image_path/$LAYOUT_SKIN/icon_show_option.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\">
												</a>
											</span>
											<table width='100%'>
												<tr><td colspan='3' style=\"border:0px\">".$Lang['eNotice']['NoticeTotalNumber'].": ".($pnotice_count-1)."</td></tr>
											</table>
										</fieldset>
									</td></tr>
									<tr><td style='border-right:0px; border-bottom:0px'>
										<fieldset id=\"Notice_table_S$current_id\" class=\"form_sub_option_form\" style=\"margin-left:5px; display:none;\" width='100%'>".
											$linterface->GET_NAVIGATION2($Lang['eNotice']['SchoolStudentNotice'])."
											<span id=\"spanHideSNoticeOption_$current_id\">
												<a href=\"javascript:hideOption('S$current_id');\" class=\"contenttool\">
													<img src=\"$image_path/$LAYOUT_SKIN/icon_hide_option.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\">
												</a>
											</span>
											<table width='100%'>			
												<tr>	
													<td colspan='3' style='padding: 0px 3px;'></td>
												</tr>	
												<tr>
													<td>#</td>
													<td width='40%'>$i_Notice_NoticeNumber</td>
													<td width='60%'>$i_Notice_Title</td>
												</tr>
												$snotice_content
											</table>
										</fieldset>
										<fieldset id=\"Notice_table_hide_S$current_id\" class=\"form_sub_option_form\" style=\"margin-left:5px; display:inline;\" width=\"100%\">".
											$linterface->GET_NAVIGATION2($Lang['eNotice']['SchoolStudentNotice'])."
											<span id=\"spanShowSNoticeOption_$current_id\">
												<a href=\"javascript:showOption('S$current_id');\" class=\"contenttool\">
													<img src=\"$image_path/$LAYOUT_SKIN/icon_show_option.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\">
												</a>
											</span>
											<table width='100%'>
												<tr><td colspan='3' style=\"border:0px\">".$Lang['eNotice']['NoticeTotalNumber'].": ".($snotice_count-1)."</td></tr>
											</table>
										</fieldset>
									</td></tr>
								</table>
								</div>
							</td>";
							
			// Remove icon 
			$middleOutput[$c] .= "
							<td width='2%'><div class='table_row_tool row_content_tool'><a href='javascript:removeStudent(".($newIDAry[$i]).")' class='delete_dim' title='".$button_delete."'></a></div></td>";
							
			$middleOutput[$c] .= "
						</tr>";
				//echo ${mainGuardianFlag_}.$newIDAry[$i].'/'.${smsFlag_}.$newIDAry[$i].'/'.${emergencyContactFlag_}.$newIDAry[$i].'<br>';
			$c++;
		}
	} else 
		$middleOutput = array();
	
	# generate $output tables
	$output = "<table class='common_table_list' id='studentTable'>";
	$output .= "<tr>
					<th class='num_check'>&nbsp;</th>
					<th>".$i_general_name."</th>
					<th>".$i_general_class."</th>
					<th>".$Lang['Identity']['Guardian']."</th>
					<th>".$Lang['AccountMgmt']['AddAsStudentGuardian']."</th>
					<th>".$Lang['eNotice']['NoticeInCurrentYear']."</th>
					<th><div class='table_row_tool row_content_tool'></div></th>
				</tr>";
		
	for($i=0; $i<sizeof($middleOutput); $i++) {
		$output .= $middleOutput[$i];
		//$output .= "<input type='hidden' name='hiddenOutput[$i]' id='hiddenOutput[$i]' value='".urlencode($middleOutput[$i])."'>";
	}
	$output .= "</table>";
		
	$output .= "<input type='hidden' name='c' id='c' value='$c'>";
	//$output .= "<input type='hidden' name='max' id='max' value='$max'>";
}

else if($task == 'studentID') {
	
	$academicYearID = Get_Current_Academic_Year_ID();
	$clsID = $ldiscipline->getClassIDByClassName($className, $academicYearID);
	$clsIDAry[0] = $clsID;

	$temp = $ldiscipline->getStudentListByClassID($clsIDAry, 2);
	
	$output = getSelectByArray($temp, ' name="studentID" id="studentID"', $studentID, 0, 0, "");	
	
}

else if($task=='refreshNotice')
{
	$academicYearID = Get_Current_Academic_Year_ID();
	list($yearStartDate, $yearEndDate) = getPeriodOfAcademicYear($academicYearID);
				
	# eNotice in current academic year
	$sql = "SELECT 
				a.NoticeID, a.Title, a.NoticeNumber as NoticeCode, a.TargetType
            FROM INTRANET_NOTICE_REPLY as c
                INNER JOIN INTRANET_NOTICE as a ON a.NoticeID = c.NoticeID
            WHERE c.StudentID = '$StudentID' AND a.RecordStatus = 1 AND a.Module IS NULL
			 	AND a.DateStart >= '$yearStartDate' AND a.DateEnd <= '$yearEndDate'
			GROUP BY a.NoticeID
			ORDER BY a.NoticeID";
	$notice_result = BuildMultiKeyAssoc($laccount->returnArray($sql, 4), array("TargetType", "NoticeID"));
	
	# Parent-signed Notice
	$parent_notice = $notice_result['P'];
	$pnotice_content = "";
	$pnotice_count = 1;
	if(count($parent_notice) > 0){
		foreach($parent_notice as $current_notice){
			$pnotice_content .= "<tr><td>$pnotice_count</td><td>".$current_notice['NoticeCode']."</td><td>".$current_notice['Title']."</td></tr>"."\r\n";
			$pnotice_count++;
		}
	} else {
		$pnotice_content .= "<tr><td colspan='3' style='text-align:center'>".$Lang['General']['NoRecordFound']."</td></tr>";
	}
	
	# Student-signed Notice
	$student_notice = $notice_result['S'];
	$snotice_content = "";
	$snotice_count = 1;
	if(count($student_notice) > 0){
		foreach($student_notice as $current_notice){
			$snotice_content .= "<tr><td>$snotice_count</td><td>".$current_notice['NoticeCode']."</td><td>".$current_notice['Title']."</td></tr>"."\r\n";
			$snotice_count++;
		}
	} else {
		$snotice_content .= "<tr><td colspan='3' style='text-align:center'>".$Lang['General']['NoRecordFound']."</td></tr>"."\r\n";
	}
				
	$output = "	<table width='100%'><tr><td style='border-right:0px'>
					<fieldset id=\"Notice_table_P$st_count\" class=\"form_sub_option_form\" style=\"margin-left:5px; display:none;\" width='100%'>".
						$linterface->GET_NAVIGATION2($Lang['eNotice']['SchoolNotice'])."
						<span id=\"spanHidePNoticeOption_$st_count\">
							<a href=\"javascript:hideOption('P$st_count');\" class=\"contenttool\">
								<img src=\"$image_path/$LAYOUT_SKIN/icon_hide_option.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\">
							</a>
						</span>
						<table>
							<tr>
								<td colspan='3' style='padding: 0px 3px;'></td>
							</tr>							
							<tr>
								<td width='1'>#</td>
								<td width='40%'>$i_Notice_NoticeNumber</td>
								<td width='60%'>$i_Notice_Title</td>
							</tr>
							$pnotice_content
						</table>
					</fieldset>
					<fieldset id=\"Notice_table_hide_P$st_count\" class=\"form_sub_option_form\" style=\"margin-left:5px; display:inline;\" width=\"100%\">".
						$linterface->GET_NAVIGATION2($Lang['eNotice']['SchoolNotice'])."
						<span id=\"spanShowPNoticeOption_$st_count\">
							<a href=\"javascript:showOption('P$st_count');\" class=\"contenttool\">
								<img src=\"$image_path/$LAYOUT_SKIN/icon_show_option.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\">
							</a>
						</span>
						<table width='100%'>
							<tr><td colspan='3' style=\"border:0px\">".$Lang['eNotice']['NoticeTotalNumber'].": ".($pnotice_count-1)."</td></tr>
						</table>
					</fieldset>
				</td></tr>
				<tr><td style='border-right:0px; border-bottom:0px'>
					<fieldset id=\"Notice_table_S$st_count\" class=\"form_sub_option_form\" style=\"margin-left:5px; display:none;\" width='100%'>".
						$linterface->GET_NAVIGATION2($Lang['eNotice']['SchoolStudentNotice'])."
						<span id=\"spanHideSNoticeOption_$st_count\">
							<a href=\"javascript:hideOption('S$st_count');\" class=\"contenttool\">
								<img src=\"$image_path/$LAYOUT_SKIN/icon_hide_option.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\">
							</a>
						</span>
						<table width='100%'>			
							<tr>	
								<td colspan='3' style='padding: 0px 3px;'></td>
							</tr>	
							<tr>
								<td>#</td>
								<td width='40%'>$i_Notice_NoticeNumber</td>
								<td width='60%'>$i_Notice_Title</td>
							</tr>
							$snotice_content
						</table>
					</fieldset>
					<fieldset id=\"Notice_table_hide_S$st_count\" class=\"form_sub_option_form\" style=\"margin-left:5px; display:inline;\" width=\"100%\">".
						$linterface->GET_NAVIGATION2($Lang['eNotice']['SchoolStudentNotice'])."
						<span id=\"spanShowSNoticeOption_$st_count\">
							<a href=\"javascript:showOption('S$st_count');\" class=\"contenttool\">
								<img src=\"$image_path/$LAYOUT_SKIN/icon_show_option.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\">
							</a>
						</span>
						<table width='100%'>
							<tr><td colspan='3' style=\"border:0px\">".$Lang['eNotice']['NoticeTotalNumber'].": ".($snotice_count-1)."</td></tr>
						</table>
					</fieldset>
				</td></tr></table>";
}

else if($task == 'ajaxLoadAddNotice') {
	
	$academicYearID = Get_Current_Academic_Year_ID();
	list($yearStartDate, $yearEndDate) = getPeriodOfAcademicYear($academicYearID);
	
	$studentid = $_POST["StudentID"];
	$sql = "SELECT ".getNameFieldByLang("", $displayLang="en")." AS Name FROM INTRANET_USER WHERE UserID = '$studentid'";
	$studentName = $lnotice->returnVector($sql);
	$studentName = $studentName[0];
	
	# get assigned Notice ID
	$sql = "SELECT a.NoticeID FROM INTRANET_NOTICE_REPLY as c INNER JOIN INTRANET_NOTICE as a ON a.NoticeID = c.NoticeID
            WHERE c.StudentID = '$studentid' AND a.RecordStatus = 1 AND a.Module IS NULL AND a.DateStart >= '$yearStartDate' AND a.DateEnd <= '$yearEndDate'
			GROUP BY a.NoticeID ORDER BY a.NoticeID";
	$existing_notice_result = $lnotice->returnVector($sql);
	
	# get not assigned Notice ID
	$sql = "SELECT NoticeID, Title, IF(IsModule=1,'--',NoticeNumber) as NoticeCode, TargetType
            FROM INTRANET_NOTICE
            WHERE RecordStatus = 1 AND Module IS NULL AND DateStart >= '$yearStartDate' AND DateEnd <= '$yearEndDate' AND NoticeID NOT IN ('".implode("','", $existing_notice_result)."')
			GROUP BY NoticeID ORDER BY NoticeID";
	$notice_result = BuildMultiKeyAssoc($lnotice->returnArray($sql, 4), array("TargetType", "NoticeID"));
	
	$output = "<div class=\"edit_pop_board_write\">";
	$output .= "<form id=\"LoadingForm\" action=\"#\">";
	$output .= "<br/>";
		
	$p_count = 1;
	$output .= $linterface->GET_NAVIGATION2($Lang['eNotice']['SchoolNotice']);
	$output .= "<table id=\"LoadingPForm\" class=\"common_table_list\">";
	$output .= "<tr>";
		$output .= "<th width='1'><span>#</span></th>";
		$output .= "<th width='40%'><span>".$i_Notice_NoticeNumber."</span></th>";
		$output .= "<th width='60%'><span>".$i_Notice_Title."</span></th>";
		$output .= "<th width='1'><input id=\"AllPNotice\" type=\"checkbox\" name=\"AllPNotice\" value=\"\" onclick=\"(this.checked)?setAllCheckBox(1,'LoadingPForm','TargetNotice[]'):setAllCheckBox(0,'LoadingPForm','TargetNotice[]')\"></th>";
	$output .= "</tr>";
	
	if(count($notice_result['P']) > 0){
		foreach($notice_result['P'] as $current_notice){
			$output .= "<tr>";
				$output .= "<td><span>$p_count</span></td>";
				$output .= "<td><span>".$current_notice["NoticeCode"]."</span></td>";
				$output .= "<td><span>".$current_notice["Title"]."</span></td>";
				$output .= "<td><input type=\"checkbox\" name=\"TargetNotice[]\" value=\"".$current_notice["NoticeID"]."\"></td>";
			$output .= "</tr>";
			$p_count++;
		}
	} else {
		$output .= "<tr><td colspan='4' align='center'>".$Lang['General']['NoRecordFound']."</td></tr>\n";
	}
	$output .= "</table>";
	
	$output .= "<br/>";
	
	$s_count = 1;
	$output .= $linterface->GET_NAVIGATION2($Lang['eNotice']['SchoolStudentNotice']);
	$output .= "<table id=\"LoadingSForm\" class=\"common_table_list\">";
	$output .= "<tr>";
		$output .= "<th width='1'><span>#</span></th>";
		$output .= "<th width='40%'><span>".$i_Notice_NoticeNumber."</span></th>";
		$output .= "<th width='60%'><span>".$i_Notice_Title."</span></th>";
		$output .= "<th width='1'><input id=\"AllSNotice\" type=\"checkbox\" name=\"AllSNotice\" value=\"\" onclick=\"(this.checked)?setAllCheckBox(1,'LoadingSForm','TargetNotice[]'):setAllCheckBox(0,'LoadingSForm','TargetNotice[]')\"></th>";
	$output .= "</tr>";
	if(count($notice_result['S']) > 0){
		foreach($notice_result['S'] as $current_notice){
			$output .= "<tr>";
				$output .= "<td><span>$s_count</span></td>";
				$output .= "<td><span>".$current_notice["NoticeCode"]."</span></td>";
				$output .= "<td><span>".$current_notice["Title"]."</span></td>";
				$output .= "<td><input type=\"checkbox\" name=\"TargetNotice[]\" value=\"".$current_notice["NoticeID"]."\"></td>";
			$output .= "</tr>";
			$s_count++;
		}
	} else {
		$output .= "<tr><td colspan='4' align='center'>".$Lang['General']['NoRecordFound']."</td></tr>\n";
	}
	$output .= "</table>";
	
	$output .= "<input type=\"hidden\" name=\"TargetStudentID\" value=\"$studentid\">";
	$output .= "<input type=\"hidden\" name=\"TargetStudentName\" value=\"$studentName\">";
	$output .= "<input type=\"hidden\" name=\"AcademicYearIDs\" value=\"$academicYearID\">";
	
	$output .= "</form>";
	$output .= "</div>";
	
	$output .= "<br/>";
	$output .= "<div class=\"edit_bottom\" align=\"center\">";
	$output .= $linterface->GET_ACTION_BTN($button_submit, "button", "this.disabled=true; this.className='formbutton_disable_v30 print_hide'; js_update_notice_list('$studentid','$st_count')","submit_btn_tb");
	$output .= "&nbsp;";
	$output .= $linterface->GET_ACTION_BTN($button_cancel, "button", "js_Hide_ThickBox();", "cancelBtn_tb");
	$output .= "</div>";
}

echo $output;
?>
