<?php
# using: 

### 		!!!!! Note: Required to deploy with libuser.php if upload this file to client BEFORE ip.2.5.9.1.1	!!!!!	

#################### Change Log ###
#   Date:   2019-05-13 Henry
#			Security fix: SQL without quote
#   Date:   2018-08-31 Pun [ip.2.5.9.7.1#CD1]
#           Hide display debug message
#	Date:	2018-07-20 Carlos - Fixed the missing get minimum password length security setting.
#   Date:   2018-05-16 Isaac
#           Added Last Modified username and time.
#
#	Date:	2018-01-03 (Carlos) [ip.2.5.9.1.1]: Improved password checking.
#	Date:	2017-10-19 Ivan [ip.2.5.9.1.1]
#			Changed default personal photo path to call function by libuser.php
#
#   Date:	2016-06-10 Anna
#   add creator's information below the webpage

#	2015-12-30 (Carlos): $plugin['radius_server'] - added Wi-Fi access option.
#	2014-11-03 (Bill) : Added checking to prevent displaying data of linked students
#	2012-08-30 (YatWoon): added barcode
#	2012-06-08 (Carlos): added password remark
#
#	Date:	2012-04-03 (Henry Chow)
#			allow to input HKID if flag is ON
#
#	Date:	2012-03-14	YatWoon
#			allow admin update user's personal email [Case#2012-0313-1603-23071]
#
#	Date :	2011-12-20	(Yuen)
#			Display personal  photos
#
#	2011-03-07	(Henry Chow)
#		Password not display in edit page (related case : #2011-0304-1154-52067)
#		if password is empty => keep unchange ; if password is entered => change 
#
#	2011-01-31	YatWoon
#		Gender field not a MUST for parent (requested by UCCKE with case #2011-0131-1621-41073)
#
####################################

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");


intranet_auth();
intranet_opendb();

if(!($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Parent"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"])) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

if($comeFrom=="")
	$comeFrom = "index.php";


//if(sizeof($_POST)==0) 
if(empty($uid))
{
	header("Location: $comeFrom");
	exit;
}
/*
$_SERVER['HTTP_COOKIE'];
debug_pr($HTTP_COOKIE_VARS['ck_accountmgmt_teachingType']);
*/
if($uid=="") {
	header("Location: index.php?targetClass=$targetClass&keyword=$keyword");
	exit;	
}

$laccount = new libaccountmgmt();
$linterface = new interface_html();

$userInfo = $laccount->getUserInfoByID($uid);

# Not from student account
if($fromParent!=1) {
	if($userlogin=="") $userlogin = $userInfo['UserLogin'];
	//if($pwd=="") $pwd = $userInfo['UserPassword'];
	if($email=="") $email = isset($UserEmail)? $UserEmail : $userInfo['UserEmail'];
	if($status=="") $status = $userInfo['RecordStatus'];
	if($engname=="") $engname = $userInfo['EnglishName']; $engname = stripslashes($engname);
	if($chiname=="") $chiname = $userInfo['ChineseName']; $chiname = stripslashes($chiname);
	if($gender=="") $gender = $userInfo['Gender'];
	if($address=="") $address = $userInfo['Address'];
	if($country=="") $country = $userInfo['Country'];
	if($homePhone=="") $homePhone = $userInfo['HomeTelNo'];
	if($officePhone=="") $officePhone = $userInfo['OfficeTelNo'];
	if($mobilePhone=="") $mobilePhone = $userInfo['MobileTelNo'];
	if($faxPhone=="") $faxPhone = $userInfo['FaxNo'];
	if($remark=="") $remark = $userInfo['Remark'];
	$original_pass = $userInfo['UserPassword'];
	if($hkid=="") $hkid = $userInfo['HKID'];
	if($barcode=="") $barcode = $userInfo['Barcode'];
} 
# From student account - Ensure all data from DB
else {
	$userlogin = $userInfo['UserLogin'];
	//if($pwd=="") $pwd = $userInfo['UserPassword'];
	$email = $userInfo['UserEmail'];
	$status = $userInfo['RecordStatus'];
	$engname = $userInfo['EnglishName']; $engname = stripslashes($engname);
	$chiname = $userInfo['ChineseName']; $chiname = stripslashes($chiname);
	$gender = $userInfo['Gender'];
	$address = $userInfo['Address'];
	$country = $userInfo['Country'];
	$homePhone = $userInfo['HomeTelNo'];
	$officePhone = $userInfo['OfficeTelNo'];
	$mobilePhone = $userInfo['MobileTelNo'];
	$faxPhone = $userInfo['FaxNo'];
	$remark = $userInfo['Remark'];
	$original_pass = $userInfo['UserPassword'];
	$hkid = $userInfo['HKID'];
	$barcode = $userInfo['Barcode'];
}



$li = new libuser($uid);
$inputBy = $li->InputBy;
$DateInput = $li->DateInput;
$ModifyBy = $li->ModifyBy;
$DateModified = $li->DateModified;

$userObj = new libuser($inputBy);
if(empty($userObj->UserID)){
    $CreatorUserinfoAry= $userObj->ReturnArchiveStudentInfo($inputBy);
    $CreatorEnglishName = $CreatorUserinfoAry['EnglishName'];
    $CreatorChineseName = $CreatorUserinfoAry['ChineseName'];
}else{
    $CreatorEnglishName = $userObj->EnglishName;
    $CreatorChineseName = $userObj->ChineseName;
}
if($CreatorChineseName){
    $nameDisplay = Get_Lang_Selection($CreatorChineseName, $CreatorEnglishName);
} else{
    $nameDisplay=$CreatorEnglishName;
}

$ModifyUserObj = new libuser($ModifyBy);
if(empty($ModifyUserObj->UserID)){
    $ModifyUserinfoAry= $ModifyUserObj->ReturnArchiveStudentInfo($ModifyBy);
//     debug_pr($ModifyUserinfoAry);
    $ModifierEnglishName = $ModifyUserinfoAry['EnglishName'];
    $ModifierChineseName = $ModifyUserinfoAry['ChineseName'];
}else{
    $ModifierEnglishName = $ModifyUserObj->EnglishName;
    $ModifierChineseName = $ModifyUserObj->ChineseName;
}
if($ModifierChineseName){
    $ModifiernameDisplay = Get_Lang_Selection($ModifierChineseName, $ModifierEnglishName);
} else{
    $ModifiernameDisplay=$ModifierEnglishName;
}

$SettingArr = $li->RetrieveUserInfoSettingInArrary(array('EnablePasswordPolicy_'.USERTYPE_PARENT));
$PasswordLength = $SettingArr['EnablePasswordPolicy_'.USERTYPE_PARENT];
if ($PasswordLength<6) $PasswordLength = 6;


# imail gamma
if($plugin['imail_gamma'])
{
	if(!$IMapEmail = $laccount->getIMapUserEmail($uid))
	{
		$userInfo = $laccount->getUserInfoByID($uid);
		$IMapEmail = $userInfo['UserLogin']."@".$SYS_CONFIG['Mail']['UserNameSubfix'];
		$disabled = "checked";
	}	
	else
	{
		$enabled = "checked";
	}
	$DefaultQuota = $laccount->getEmailDefaultQuota(TYPE_PARENT);
	$EmailQuota = $laccount->getUserImailGammaQuota($uid);
	$EmailQuota = $EmailQuota?$EmailQuota:$DefaultQuota[1];
}
/*
$countrySelection = "<select name='country' id='country'><option value='' ".(($userInfo['Country']=="") ? "selected" : "").">-- $i_alert_pleaseselect $i_UserCountry --</option>";
for($i=0; $i<sizeof($Lang['Country']); $i++) {
	$countrySelection .= "<option value=\"".$Lang['Country'][$i]."\" ".(($Lang['Country'][$i]==$country)?"selected" : "").">".$Lang['Country'][$i]."</option>";	
}
$countrySelection .= "</select>";
*/
$countrySelection = "<input type='text' name='country' id='country' value='$country'>";
    
$CurrentPageArr['ParentMgmt'] = 1;
$CurrentPage = "Mgmt_Account";

if($comeFrom=="/home/eAdmin/AccountMgmt/ParentMgmt/noChildInSchool.php") {
	$TAGS_OBJ[] = array($Lang['AccountMgmt']['UserList'],"index.php",0);
	$TAGS_OBJ[] = array($Lang['AccountMgmt']['ParentWithoutChildInClass'],"noChildInSchool.php",1);	
} else {
	$TAGS_OBJ[] = array($Lang['AccountMgmt']['UserList'],"index.php",1);
	$TAGS_OBJ[] = array($Lang['AccountMgmt']['ParentWithoutChildInClass'],"noChildInSchool.php",0);		
}
$MODULE_OBJ['title'] = $Lang['Header']['Menu']['ParentAccount'];

# navigation bar
$PAGE_NAVIGATION[] = array($Lang['AccountMgmt']['UserList'], "javascript:goURL('$comeFrom','')");
$PAGE_NAVIGATION[] = array($Lang['AccountMgmt']['EditUser'], "");

# step information
$STEPS_OBJ[] = array($Lang['AccountMgmt']['InputParentDetails'], 1);
$STEPS_OBJ[] = array($Lang['AccountMgmt']['SelectCorrespondingStudents'], 0);

if(isset($error) && $error!="")
	$errorAry = explode(',', $error);

$linkedStudentList = $laccount->getStudentListByParentID($uid);


$li = new libuser($uid);

########## Personal Photo
//$photo_personal_link = "/images/myaccount_personalinfo/samplephoto.gif";
$photo_personal_link = $userObj->returnDefaultPersonalPhotoPath();
if ($li->PersonalPhotoLink !="")
{
    if (is_file($intranet_root.$li->PersonalPhotoLink))
    {
        $photo_personal_link = $li->PersonalPhotoLink;
   }
}

$MODULE_OBJ = $laccount->GET_MODULE_OBJ_ARR();

//$linterface->LAYOUT_START();
$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$xmsg]);

?>
<script language="javascript">
function goSubmit(urlLink) {
	var password_warnings = {};
<?php
	foreach($Lang['AccountMgmt']['PasswordCheckingWarnings'] as $key => $val){
		echo "	password_warnings['".$key."'] = '".addslashes($val)."';\n";
	}
?>		
	var obj = document.form1;
			
	//if (!check_text(obj.pwd,"<?=$i_alert_pleasefillin." ".$i_UserPassword?>")) return false;
	//if (!check_text(obj.pwd2,"<?=$i_alert_pleasefillin." ".$i_frontpage_myinfo_password_retype?>")) return false;
	if (obj.pwd.value!="" && (obj.pwd.value != obj.pwd2.value))
	{
		alert ("<?=$i_frontpage_myinfo_password_mismatch?>");
		obj.pwd.select();
		return false;
	}
<?php if($sys_custom['UseStrongPassword']){ ?>	
	if(obj.pwd.value != ''){
		var check_password_result = CheckPasswordCriteria(obj.pwd.value,'<?=$li->UserLogin?>',<?=$PasswordLength?>);
		if(check_password_result.indexOf(1) == -1){
			var password_warning_msg = '';
			for(var i=0;i<check_password_result.length;i++){
				password_warning_msg += password_warnings[check_password_result[i]] + "\n";
			}
			alert(password_warning_msg);
			obj.pwd.focus();
			return false;
		}
	}
<?php } ?>	
	// check email
	if(!check_text(obj.UserEmail, "<?php echo $i_alert_pleasefillin.$Lang['Gamma']['UserEmail']; ?>."))  return false;
	if(!validateEmail(obj.UserEmail, "<?php echo $i_invalid_email; ?>.")) return false;
	
	if(obj.status[0].checked==false && obj.status[1].checked==false) {
		alert("<?=$i_alert_pleaseselect." ".$i_general_status?>");
		return false;
	}
	if(obj.engname.value=="") {
		alert("<?=$i_alert_pleasefillin." ".$ec_student_word['name_english']?>");	
		return false;		
	}
	
	/*
	if(obj.gender[0].checked==false && obj.gender[1].checked==false) {
		alert("<?=$i_alert_pleaseselect." ".$i_UserGender?>");
		return false;
	}
	*/
	
	obj.action = urlLink;
	obj.submit();
}

function goFinish() {
	document.form1.action = "new_update.php";
	document.form1.submit();	
}

function goURL(urlLink, id) {
	if(id!=undefined) {
		document.form1.uid.value = id;
		document.form1.fromParent.value = 1;
	}
	document.form1.action = urlLink;
	document.form1.submit();
}
</script>

<form name="form1" method="post" action="">
<table width="100%" border="0" cellspacing="0" cellpadding="0">

    <tr><td height="40"><?=$linterface->GET_NAVIGATION($PAGE_NAVIGATION)?></td></tr>
    <tr><td height="40"><?=$linterface->GET_STEPS($STEPS_OBJ)?></td></tr>
    <tr><td class="board_menu_closed">
		  
		  <table width="99%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td class="main_content">
                
                      <!-- ******************************************* -->
						<div class="table_board">
						<table class="form_table" width="90%" cellpadding="4" cellspacing="0" border="0">
							<tr>
								<td colspan="3"><em class="form_sep_title"> - <?=$Lang['AccountMgmt']['SystemInfo']?> -</em></td>
							</tr>
							<tr>
								<td class="formfieldtitle" width="20%"><span class="tabletextrequire">*</span><?=$i_UserLogin?></td>
								<td width="75%"><?=$userlogin?></td>
								<td rowspan="5"><img src="<?=$photo_personal_link?>" width="100" height="130" title="<?=$Lang['Personal']['PersonalPhoto']?>" /></td>
							</tr>
							<tr>
								<td rowspan="3" class="formfieldtitle"><span class="tabletextrequire">*</span><?=$i_UserPassword?></td>
								<td>
									<input name="pwd" type="password" id="pwd" class="textboxnum" value="<?=$pwd?>"/>
									<?php
									if(isset($_SESSION['ParentAccountUpdatePasswordError'])){
										echo "<font color='#FF0000'>".$_SESSION['ParentAccountUpdatePasswordError']."</font>";
										unset($_SESSION['ParentAccountUpdatePasswordError']);
									}
									?>
								</td>
							</tr>
							<tr>
								<td><input name="pwd2" type="password" id="pwd2" class="textboxnum" value="<?=$pwd?>"/> (<?=$Lang['AccountMgmt']['Retype']?>) </td>
							</tr>
							<tr>
								<td class="tabletextremark"><?=$sys_custom['UseStrongPassword']?str_replace("<!--PASSWORD_LENGTH-->",$PasswordLength,implode("<br />\n",$Lang['AccountMgmt']['PasswordRequirements'])):$Lang['AccountMgmt']['PasswordRemark']?></td>
							</tr>
							
							<?if($plugin['imail_gamma']) {
									include_once($PATH_WRT_ROOT."includes/imap_gamma.php");
									$IMap = new imap_gamma($skipLogin = 1);
									if($IMap->CheckIdentityPermission(TYPE_PARENT) )
									{	
										?>
										<!-- imail gamma -->	
										<tr>
											<td class="formfieldtitle"><span class="tabletextrequire">*</span><?=$i_UserEmail?></td>
											<td>
												<input name="EmailStatus" type="radio" id="EmailDisable" value="disable" <?=$disabled?>/><label for="EmailDisable"><?=$Lang['General']['Disabled']?></label><br>
												<input name="EmailStatus" type="radio" id="EmailEnable" value="enable" <?=$enabled?>/><label for="EmailEnable"><?=$Lang['General']['Enabled']?></label> - <span id="IMapUserEmail"><?=$IMapEmail?></span><br>
												<?="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$Lang['Gamma']['Quota']?>: <input name="Quota" type="text" id="Quota" class="textboxnum" maxlength=5 value="<?=$EmailQuota?>"/>MB</div>
											</td>
										</tr>
										<?
									}
								}?>
							<!-- imail -->	
							<tr>
								<td class="formfieldtitle"><span class="tabletextrequire">*</span><?=$Lang['Gamma']['UserEmail']?></td>
								<td>
									<input class="textbox_name" type="text" name="UserEmail" size="30" maxlength="50" value="<?=$email?>">
									<br /><span class="tabletextremark"><?=$Lang['AccountMgmt']['EmailFieldReminder']?></span>
								</td>
							</tr>							
						  <tr>
						    <td class="formfieldtitle"><span class="tabletextrequire">*</span><?=$i_general_status?></td>
						    <td>
						    	<input type="radio" name="status" id="status1" value="1" <? if($status=="" || $status=="1") echo "checked";?>><label for="status1"><?=$Lang['Status']['Activate']?></label>
						    	<input type="radio" name="status" id="status0" value="0" <? if($status=="0") echo "checked";?>><label for="status0"><?=$Lang['Status']['Suspend']?></label>
						    </td>
						  </tr>
						  
						  <tr>
								<td class="formfieldtitle"><?=$Lang['AccountMgmt']['Barcode']?></td>
								<td colspan="2">
									<input name="barcode" type="text" id="barcode" class="textboxnum" value="<?=$barcode?>"/>
									<? if(sizeof($errorAry)>0 && in_array($barcode, $errorAry)) echo "<font color='#FF0000'>".$Lang['AccountMgmt']['Barcode'].$Lang['AccountMgmt']['UsedByOtherUser']."</font>"; ?>
								</td>
							</tr>
							
							
							
							<tr>
								<td colspan="3"><em class="form_sep_title"> - <?=$Lang['AccountMgmt']['BasicInfo']?> -</em></td>
							</tr>
							<tr>
								<td rowspan="2" class="formfieldtitle"><?=$i_general_name?></td>
								<td colspan="2" ><span class="sub_row_content"><span class="tabletextrequire">*</span>(<?=$ip20_lang_eng?>)</span>
								<input name="engname" type="text" id="engname" class="textbox_name" value="<?=$engname?>"/></td>
							</tr>
							<tr>
								<td colspan="2"><span class="sub_row_content">(<?=$Lang['AccountMgmt']['Chi']?>)</span> 
								<input name="chiname" type="text" id="chiname" class="textbox_name" value="<?=$chiname?>"/>
								</td>
							</tr>
							<tr>
								<td class="formfieldtitle"><?=$i_UserGender?></td>
								<td colspan="2"><input type="radio" name="gender" id="genderM" value="M"  <? if($gender=="M") echo "checked"; ?>/>
						      <label for="genderM"><?=$i_gender_male?></label>
						      <input type="radio" name="gender" id="genderF" value="F" <? if($gender=="F") echo "checked"; ?>/>
						      <label for="genderF"><?=$i_gender_female?></label></td>
							</tr>
							<? if($special_feature['ava_hkid']) { ?>
							<tr>
								<td class="formfieldtitle"><?=$i_HKID?></td>
								<td colspan="2">
									<input name="hkid" type="text" id="hkid" class="textboxnum" value="<?=$hkid?>"/>
									<? if(sizeof($errorAry)>0 && in_array($hkid, $errorAry)) echo "<font color='#FF0000'>".$i_HKID.$Lang['AccountMgmt']['UsedByOtherUser']."</font>"; ?>
								</td>
							</tr>
							<? } ?>
							
							<tr>
								<td class="formfieldtitle"><?=$i_UserAddress?></td>
								<td colspan="2"><?=$linterface->GET_TEXTAREA("address", $address);?>
						      <br />
						      <?=$countrySelection?></td>
							</tr>
						  <tr>
						    <td class="formfieldtitle" rowspan="4"><?=$Lang['AccountMgmt']['Tel']?></td>
						    <td colspan="2"><span class="sub_row_content">(<?=$Lang['AccountMgmt']['Home']?>)</span>
						        <input name="homePhone" type="text" id="homePhone" class="textboxnum" value="<?=$homePhone?>" maxlength="20"/></td>
						  </tr>
						  <tr>
						    <td colspan="2"><span class="sub_row_content">(<?=$Lang['AccountMgmt']['Office']?>)</span>
						        <input name="officePhone" type="text" id="officePhone" class="textboxnum" value="<?=$officePhone?>" maxlength="20"/></td>
						  </tr>
						  <tr>
						    <td colspan="2"><span class="sub_row_content">(<?=$Lang['AccountMgmt']['Mobile']?>)</span>
						        <input name="mobilePhone" type="text" id="mobilePhone" class="textboxnum" value="<?=$mobilePhone?>" maxlength="20" /><span class="tabletextremark"><? if (isset($plugin['sms']) && $plugin['sms']){ echo $i_UserMobileSMSNotes; }?></span></td>
						  </tr>
						  <tr>
						    <td colspan="2"><span class="sub_row_content">(<?=$Lang['AccountMgmt']['Fax']?>)</span>
						        <input name="faxPhone" type="text" id="faxPhone" class="textboxnum" value="<?=$faxPhone?>" maxlength="20" /></td>
						  </tr>
							
						  <!-- Additional Info //-->
								<tr>
									<td colspan="3"><em class="form_sep_title"> - <?=$Lang['AccountMgmt']['AdditionInfo']?> -</em></td>
								</tr>
								<tr>
								<td class="formfieldtitle"><?=$Lang['General']['Remark']?></td>
									<td colspan="2">
										<?=$linterface->GET_TEXTAREA("remark", $remark);?>
									</td>
								</tr>
							
				  	<?if(!$plugin['imail_gamma']) {?>
				  		<? if(isset($plugin['webmail']) && $plugin['webmail'] && in_array(USERTYPE_PARENT, $webmail_identity_allowed)) { ?>
							<!-- Internet Usage //-->
							<tr>
						  		<td colspan="3"><em class="form_sep_title">- <?=$Lang['AccountMgmt']['InternetUsage']?> -</em></td>
						  	</tr>
						  	<?
								$sql = "SELECT REVERSE(BIN(ACL)) FROM INTRANET_SYSTEM_ACCESS WHERE UserID = '".$uid."'";
						  		$result = $laccount->returnVector($sql);
								$can_access_iMail = substr($result[0],0,1);
								$can_access_iFolder = substr($result[0],1,1);
						  	?>
									  <tr>
									    <td class="formfieldtitle"><?=$ip20TopMenu['iMail']?></td>
									    <td colspan="2"><input type="checkbox" value="1" name="open_webmail" disabled <? echo ($can_access_iMail)? "checked" : "" ?> />
									      <label for="open_webmail"><?=$i_Mail_AllowSendReceiveExternalMail?></label></td>
									  </tr>
						<?}?>
					<?}?>
							
						  <?php if($plugin['radius_server']){ 
						  	$enable_wifi_access = !$laccount->isUserDisabledInRadiusServer($userlogin);
						  ?>
						  <tr>
						  	<td colspan="3"><em class="form_sep_title">- <?=$Lang['AccountMgmt']['WifiUsage']?> -</em></td>
						  </tr>
						  <tr>
						  	<td class="formfieldtitle"><?=$Lang['AccountMgmt']['EnableWifiAccess']?></td>
						  	<td><input type="checkbox" name="enable_wifi_access" value="1" <?=$enable_wifi_access?'checked="checked"':''?> /></td>
						  </tr>
						  <?php } ?>
								
							<? if($linkedStudentList!="") { ?>
							<tr>
								<td colspan="3"><em class="form_sep_title"> -  <?=$i_UserParentLink?> -</em></td>
							</tr>
							<tr>
								<td class="formfieldtitle"><?=$i_UserStudentName?></td>
								<td colspan="2" class="formfieldtitle"><?=$linkedStudentList?></td>
							</tr>
							<? } ?>
														  
							<tr>
							<td colspan="3"><em class="form_sep_title"> -  <?=$Lang['AccountMgmt']['CreatedAndModifiedRecords']?> -</em></td>
							</tr>
							<?php  if($nameDisplay){?>
							<tr>
							<td class="formfieldtitle"><?=$Lang['AccountMgmt']['CreateUserName']?></td>
							<td colspan="2"><?=$nameDisplay?></td>
							</tr>
							<tr>
							<td class="formfieldtitle"><?=$Lang['AccountMgmt']['CreateUserDate']?></td>
							<td colspan="2"><?=$DateInput?></td>
							</tr>
							<? } ?>
							<tr>
							<td class="formfieldtitle"><?=$Lang['AccountMgmt']['LastEditUserName']?></td>
							<td colspan="2"><?=$ModifiernameDisplay?></td>
							</tr>
							<tr>
							<td class="formfieldtitle"><?=$Lang['AccountMgmt']['LastEditUserDate']?></td>
							<td colspan="2"><?=$DateModified?></td>
							</tr>					
														
						</table>
						<p class="spacer"></p>
						</div>
						<div class="edit_bottom">
                          <p class="spacer"></p>
                          <?= $linterface->GET_ACTION_BTN($button_continue	, "button", "goSubmit('edit2.php')")?>
                          <?= $linterface->GET_ACTION_BTN($button_finish, "button", "document.form1.skipStep2.value=1;goSubmit('edit_update.php')")?>
                          <?= $linterface->GET_ACTION_BTN($button_cancel, "button", "goURL('$comeFrom','')")?>
							<p class="spacer"></p>
                    </div></td>
                </tr>
              </table>
    </td></tr>
</table>
<input type="hidden" name="comeFrom" id="comeFrom" value="<?=$comeFrom?>">
<input type="hidden" name="fromParent" id="fromParent" value="">
<input type="hidden" name="task" id="task" value="">
<input type="hidden" name="ClickID" id="ClickID" value="">
<input type="hidden" name="uid" id="uid" value="<?=$uid?>">
<input type="hidden" name="original_pass" id="original_pass" value="<?=$original_pass?>">
<input type="hidden" name="skipStep2" id="skipStep2" value="">
<input type="hidden" name="targetClass" id="targetClass" value="<?=$targetClass?>">
<input type="hidden" name="recordstatus" id="recordstatus" value="<?=$recordstatus?>">
<input type="hidden" name="keyword" id="keyword" value="<?=$keyword?>">
</form>
<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>