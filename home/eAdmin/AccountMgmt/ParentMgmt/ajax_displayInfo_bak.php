<?php
# modifying by : henry

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();


$laccount = new libaccountmgmt();
$ldiscipline = new libdisciplinev12();


if($task=='getStudent')
{
	$c = 0;
	$i = 0;
	//debug_pr($hiddenOutput);
	if(sizeof($hiddenOutput)>0) {
		for($i=0; $i<=$max; $i++) {

			if(!isset($ID) || $ID != ($i+1)) {
				if($hiddenOutput[$i]) {
					$middleOutput[$c] = urldecode($hiddenOutput[$i]);
					$c++;
				}
			}
		}
	} else 
		$middleOutput = array();
		
		
	/*
	# display linked student when editing parent
	$name_field = getNameFieldByLang();
	$sql = "SELECT UserID, $name_field, ClassName, ClassNumber FROM INTRANET_PARENTRELATION WHERE ParentID=$parentID";
	$result = $laccount->returnArray($sql, 4);
	$stdInfo = array();
	
	for($i=0; $i<sizeof($result); $i++) {
		list($userid, $name, $clsName, $clsNo) = $result[$i];	
		$stdInfo[$userid]['Name'] = $name;
		$stdInfo[$userid]['ClassNameNumber'] = $clsName.'-'.$clsNo;
	}	
				
	$i = 0;
	foreach($stdInfo as $key=>$ary[]) {
		$middleOutput[$c] .= "			
					<tr class='sub_row' id='".($i+1)."'>
						<td class='num_check' width='1'>".($i+1).".</td>
						<td><a href='#' width='15%'>".$ary['Name']."</a></td>
						<td width='15%'>".$ary['ClassNameNumber']."</td>
						<td width='20%'>
							<table class=' form_table inside_form_table'>
								<tr>
									<td>&nbsp;</td>
									<td><div class='guardian_option'> <a href='#' class='main_guardian_selected' title='".$Lang['AccountMgmt']['MainGuardian']."'>&nbsp;</a></div>
										<br />
									</td>
								</tr>
								<tr>
									<td>Chan Ka Ka</td>
									<td><div class='guardian_option'><a href='#' class='send_sms_to_selected' title='".$Lang['AccountMgmt']['ReceiveSMS']."'>&nbsp;</a> <a href='#' class='emergency_contact_selected'  title='".$Lang['AccountMgmt']['EmergencyContact']."'>&nbsp;</a> </div><br />										
									</td>
								</tr>
							</table>              
						</td>
						<td width='50%'>
							<input name='guardianCheck_".($i+1)."' type='checkbox' id='guardianCheck_".($i+1)."' checked='checked' onClick=\"if(this.checked==true) {document.getElementById('spanNewGuardian".($i+1)."').style.display = 'inline';} else {document.getElementById('spanNewGuardian".($i+1)."').style.display = 'none';} \"/>
							<div id='spanNewGuardian".($i+1)."' style='display:inline'>
							<fieldset class='form_sub_option_form'>
							<table class='form_table inside_form_table'>
								<tr>
									<td colspan='2'><em class='form_sep_title'> - ".$i_Discipline_Details."-</em></td>
								</tr>
								<tr>
									<td class='field_title'><span class='tabletextrequire'>*</span>".$ec_iPortfolio['relation']."</td>
									<td>
										<select name='relation_new_1' id='relation_new_1'>
											<option selected>- ".$i_alert_pleaseselect.' '.$i_general_Type." -</option>
											<option value ='01'>".$ec_guardian['01']."</option>
											<option value ='02'>".$ec_guardian['02']."</option>
											<option value ='03'>".$ec_guardian['03']."</option>
											<option value ='04'>".$ec_guardian['04']."</option>
											<option value ='05'>".$ec_guardian['05']."</option>
											<option value ='06'>".$ec_guardian['06']."</option>
											<option value ='07'>".$ec_guardian['07']."</option>
											<option value ='08'>".$ec_guardian['08']."</option>
										</select>
									</td>
								</tr>
								<tr>
									<td class='field_title'><span class='tabletextrequire'>*</span>".$i_StudentGuardian_EMPhone."</td>
									<td><input name='emphone_new_' type='text' id='emphone_new_' class='textboxnum'/>
									<br /></td>
								</tr>
								<tr>
									<td class='field_title'>".$i_StudentGuardian_Occupation." </td>
									<td><input name='occupation_new_' type='text' id='occupation_new_'/><br /></td>
								</tr>
								<tr>
									<td class='field_title'>".$i_Discipline_Statistics_Options."</td>
									<td><div class='guardian_option'> <a href='#' class='main_guardian' title='".$Lang['AccountMgmt']['MainGuardian']."'>&nbsp;</a> <a href='#' class='send_sms_to' title='".$Lang['AccountMgmt']['ReceiveSMS']."'>&nbsp;</a> <a href='#' class='emergency_contact'  title='".$Lang['AccountMgmt']['EmergencyContact']."'>&nbsp;</a> </div><br /></td>
								</tr>
							</table>
							<a href='#'>[ ".$Lang['AccountMgmt']['CopyToAll']." ]</a>
							</fieldset>
						</td>
						<td><div class='table_row_tool row_content_tool'><a href='removeStudent(".($i+1).")' class='delete_dim' title='".$button_delete."'></a></div></td>
					</tr>";
		$i++;
		$c++;
	}
	*/
	if($ID=="") {
		$studentData = $laccount->getUserInfoByID($studentID);
		$middleOutput[$c] .= "
					<tr class='sub_row' id='".($max+1)."'>
						<td class='num_check' width='1'>".($max+1).".</td>
						<td><a href='#' width='15%'>".Get_Lang_Selection($studentData['ChineseName'], $studentData['EnglishName'])."</a></td>
						<td width='15%'>".($studentData['ClassName'].(($studentData['ClassNumber']!="")?"-".$studentData['ClassNumber'] : ""))."</td>
						<td width='20%'>
							<table class=' form_table inside_form_table'>
								<tr>
									<td>Chan Ka Yan</td>
									<td><div class='guardian_option'> <a href='#' class='main_guardian_selected' title='".$Lang['AccountMgmt']['MainGuardian']."'>&nbsp;</a></div>
										<br />
									</td>
								</tr>
								<tr>
									<td>Chan Ka Ka</td>
									<td><div class='guardian_option'><a href='#' class='send_sms_to_selected' title='".$Lang['AccountMgmt']['ReceiveSMS']."'>&nbsp;</a> <a href='#' class='emergency_contact_selected'  title='".$Lang['AccountMgmt']['EmergencyContact']."'>&nbsp;</a> </div><br />										
									</td>
								</tr>
							</table>              
						</td>
						<td width='50%'>
							<input name='guardianCheck_".($max+1)."' type='checkbox' id='guardianCheck_".($max+1)."' checked='checked' onClick=\"if(this.checked==true) {document.getElementById('spanNewGuardian".($max+1)."').style.display = 'inline';} else {document.getElementById('spanNewGuardian".($max+1)."').style.display = 'none';} \"/>
							<div id='spanNewGuardian".($max+1)."' style='display:inline'>
							<fieldset class='form_sub_option_form'>
							<table class='form_table inside_form_table'>
								<tr>
									<td colspan='2'><em class='form_sep_title'> - ".$i_Discipline_Details."-</em></td>
								</tr>
								<tr>
									<td class='field_title'><span class='tabletextrequire'>*</span>".$ec_iPortfolio['relation']."</td>
									<td>
										<select name='relation_new_".($max+1)."' id='relation_new_".($max+1)."'>
											<option selected>- ".$i_alert_pleaseselect.' '.$i_general_Type." -</option>
											<option value ='01'>".$ec_guardian['01']."</option>
											<option value ='02'>".$ec_guardian['02']."</option>
											<option value ='03'>".$ec_guardian['03']."</option>
											<option value ='04'>".$ec_guardian['04']."</option>
											<option value ='05'>".$ec_guardian['05']."</option>
											<option value ='06'>".$ec_guardian['06']."</option>
											<option value ='07'>".$ec_guardian['07']."</option>
											<option value ='08'>".$ec_guardian['08']."</option>
										</select>
									</td>
								</tr>
								<tr>
									<td class='field_title'><span class='tabletextrequire'>*</span>".$i_StudentGuardian_EMPhone."</td>
									<td><input name='emphone_new_".($max+1)."' type='text' id='emphone_new_".($max+1)."' class='textboxnum'/>
									<br /></td>
								</tr>
								<tr>
									<td class='field_title'>".$i_StudentGuardian_Occupation." </td>
									<td><input name='occupation_new_".($max+1)."' type='text' id='occupation_new_".($max+1)."'/><br /></td>
								</tr>
								<tr>
									<td class='field_title'>".$i_Discipline_Statistics_Options."</td>
									<td><div class='guardian_option'> <a href='#' class='main_guardian' title='".$Lang['AccountMgmt']['MainGuardian']."'>&nbsp;</a> <a href='#' class='send_sms_to' title='".$Lang['AccountMgmt']['ReceiveSMS']."'>&nbsp;</a> <a href='#' class='emergency_contact'  title='".$Lang['AccountMgmt']['EmergencyContact']."'>&nbsp;</a> </div><br /></td>
								</tr>
							</table>
							<a href='#'>[ ".$Lang['AccountMgmt']['CopyToAll']." ]</a>
							</fieldset>
						</td>
						<td><div class='table_row_tool row_content_tool'><a href='javascript:removeStudent(".($max+1).")' class='delete_dim' title='".$button_delete."'></a></div></td>
					</tr>";
		$c++;
		$max++;
	}
	$max = ($max=="" || $max<$c) ? $c : $max;
	
	# generate $output tables
	$output = "<table class='common_table_list' id='studentTable'>";
	$output .= "<tr>
					<th class='num_check'>&nbsp;</th>
					<th>".$i_general_name."</th>
					<th>".$i_general_class."</th>
					<th>".$Lang['Identity']['Guardian']."</th>
					<th>".$Lang['AccountMgmt']['AddAsStudentGuardian']."</th>
					<th><div class='table_row_tool row_content_tool'></div></th>
				</tr>";
		
	for($i=0; $i<sizeof($middleOutput); $i++) {
		$output .= $middleOutput[$i];
		$output .= "<input type='hidden' name='hiddenOutput[$i]' id='hiddenOutput[$i]' value='".urlencode($middleOutput[$i])."'>";
	}
	$output .= "</table>";
		
	$output .= "<input type='hidden' name='c' id='c' value='$c'>";
	$output .= "<input type='hidden' name='max' id='max' value='$max'>";
	
}

else if($task == 'studentID') {
	
	$academicYearID = Get_Current_Academic_Year_ID();
	$clsID = $ldiscipline->getClassIDByClassName($className, $academicYearID);
	$clsIDAry[0] = $clsID;

	$temp = $ldiscipline->getStudentListByClassID($clsIDAry, 2);
	
	$output = getSelectByArray($temp, ' name="studentID" id="studentID"', $studentID, 0, 0, "");	
	
}


echo $output;
?>
