<?php
// Modifng by : 
/*
 * Modification:
 * Date: 2019-05-13 (Henry)
 * 	- Security fix: SQL without quote
 * Date: 2012-08-01 (Ivan)
 * 	- added logic to syn user info to library system
 */
 
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libftp.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libform.php");
include_once($PATH_WRT_ROOT."includes/libftp.php");
include_once($PATH_WRT_ROOT."includes/libldap.php");
include_once($PATH_WRT_ROOT."includes/libstudentpromotion.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");

if(!($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Parent"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"])) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

if(sizeof($_POST)==0) {
	header("Location: index.php");	
	exit;
}

intranet_auth();
intranet_opendb();

if($comeFrom=="")
	$comeFrom = "index.php";

$lu = new libuser();
$laccount = new libaccountmgmt();

$isKIS = $_SESSION["platform"]=="KIS" && $sys_custom['ePayment']['KIS_NoBalance'];

# delete all parents
if($all==1) {
	$sql = "SELECT UserID FROM INTRANET_USER WHERE RecordType=".TYPE_PARENT." AND RecordStatus IN (0,1,2) AND UserID NOT IN (
				SELECT
					USR.UserID
				FROM 
					INTRANET_USER USR INNER JOIN
					INTRANET_PARENTRELATION pr ON (pr.ParentID=USR.UserID)
				WHERE
					USR.RecordType = ".TYPE_PARENT." and USR.RecordStatus IN (0,1,2)		
				GROUP BY 
					USR.UserID
			)
			";				
	$user_id = $laccount->returnVector($sql);	
}

if (sizeof($user_id)!=0)
{
	$list = implode(",", $user_id);
    
	#### Check any account has non-zero balance
	$sql="SELECT COUNT(*) FROM PAYMENT_ACCOUNT AS a LEFT OUTER JOIN PAYMENT_PAYMENT_ITEMSTUDENT AS b ON (a.StudentID = b.StudentID) WHERE a.StudentID IN ('".implode("','", $user_id)."') AND (a.Balance >0 OR b.RecordStatus=0)";
	
	$temp = $lu->returnVector($sql);
	
	if (!$isKIS && $temp[0]>0)
	{
	    # Stop this action
	    include_once($PATH_WRT_ROOT."includes/libaccount.php");
	    include_once($PATH_WRT_ROOT."includes/libinterface.php");
	    include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
	    
	    $linterface = new interface_html();
	    
		$CurrentPageArr['StaffMgmt'] = 1;
		//$CurrentPage = "User List";
		
		$TAGS_OBJ[] = array($Lang['AccountMgmt']['UserList']);
		$MODULE_OBJ['title'] = $Lang['Header']['Menu']['StaffAccount'];
		
		# navigation bar
		$PAGE_NAVIGATION[] = array($Lang['AccountMgmt']['UserList'], "javascript:goBack()");
		$PAGE_NAVIGATION[] = array($Lang['AccountMgmt']['DeleteUser'], "");
	
		$linterface->LAYOUT_START();
	
	    $langfield = getNameFieldByLang("b.");
	    $sql = "SELECT 
	    			b.UserLogin, 
	    			$langfield, 
	    			b.ClassName, 
	    			b.ClassNumber, 
	    			IF(a.Balance>0.01,CONCAT('<font color=red>$', FORMAT(a.Balance, 1),'</font>'),CONCAT('$', FORMAT(a.Balance, 1))) AS Amount,
	    			c.PaymentID,
	    			d.Name,
	    			CONCAT('$',FORMAT(c.Amount,1))
				FROM PAYMENT_ACCOUNT as a LEFT OUTER JOIN INTRANET_USER as b ON a.StudentID = b.UserID LEFT OUTER JOIN 
					PAYMENT_PAYMENT_ITEMSTUDENT as c ON (a.StudentID = c.StudentID) LEFT OUTER JOIN 
					PAYMENT_PAYMENT_ITEM as d ON (c.ItemID = d.ItemID)
				WHERE 
					a.StudentID IN ('".implode("','", $user_id)."') AND 
					(a.Balance >0 OR c.RecordStatus=0) 
				ORDER BY 
					a.StudentID";
	             
	    $result = $lu->returnArray($sql,8);
	    
	    $result2 = array();
	    for ($i=0; $i<sizeof($result); $i++)
	    {
	         list($t_userLogin,$t_name, $t_className, $t_classNum, $t_balance, $payment_id, $item_name, $unpaid_amount) = $result[$i];
		     $result2[$t_userLogin]['name'] = $t_name;
		     $result2[$t_userLogin]['class'] = $t_className;
		     $result2[$t_userLogin]['classnumber'] = $t_classNum;
		     $result2[$t_userLogin]['balance'] = $t_balance;
		     if($payment_id != ""){
		     	$result2[$t_userLogin]['items'][] = $item_name." (".$unpaid_amount.")";
		     }
	    }
	    
	    $content = "";
	
	    $j=0;
	    foreach($result2 as $user_login => $values){
		    $name = $values['name'];
		    $class = $values['class'];
		    $classnumber = $values['classnumber'];
		    $balance = $values['balance'];
		    
		    if(is_array($values['items'])){
			    $items = "<table border=0>";
			  	for($x=0; $x<sizeof($values['items']); $x++){
		    		$items .= "<tr><td><font color=red>-</font></td><td><font color=red>".$values['items'][$x]."</font></td></tr>";
		    	}
		    	$items .= "</table>";
			}
		    
		    $j++;
		    $css =$css = ($j%2 ? "1":"2");
	        $content .= "<tr class=tablerow$css><td>$user_login</td><td>$name</td><td>$class&nbsp;</td><td>$classnumber</td><td>$balance</td><td>$items&nbsp;</td></tr>\n";
	
		}
	
	    ?>
	    <form name="form1" method="post" action="<?=$comeFrom?>">
	    <table align="left" width="95%" border="0" cellpadding="4" cellspacing="0"><tr><td height="40"><?=$linterface->GET_NAVIGATION($PAGE_NAVIGATION)?></td></tr></table>
	    <p>&nbsp;</p><p>&nbsp;</p>
			<span class="extraInfo"><span class="subTitle"><?=$i_UserRemoveStop_PaymentBalanceNonZero?></span></span>
				<p>
				<table width="90%" border="0" cellpadding="4" cellspacing="0">
					<tr class="tableTitle" bgcolor="#CCCCCC"><td><?=$i_UserLogin?></td><td><?=$i_UserName?></td><td><?=$i_UserClassName?></td><td><?=$i_UserClassNumber?></td><td><?=$i_Payment_Field_Balance?></td><td><?="$i_Payment_Field_PaymentItem ($i_Payment_Field_Amount)"?></td></tr>
					<?=$content?>
				</table>
			<br><br>
			<?= $linterface->GET_ACTION_BTN($button_back, "button", "goBack()")?>
		<input type="hidden" name="comeFrom" id="comeFrom" value="<?=$comeFrom?>">
		<input type="hidden" name="TeachingType" id="TeachingType" value="<?=$TeachingType?>">
		<input type="hidden" name="recordstatus" id="recordstatus" value="<?=$recordstatus?>">
		<input type="hidden" name="keyword" id="keyword" value="<?=$keyword?>">
		</form>
		<script language="javascript">
		<!--
			function goBack() {
				document.form1.submit();	
			}
		-->
		</script>
	
	    <?
	    intranet_closedb();
	    $linterface->LAYOUT_STOP();
	
	    exit();
	
	} else {
		
		# SchoolNet integration
		if ($plugin['SchoolNet'])
		{
		    $sql = "SELECT UserLogin FROM INTRANET_USER WHERE UserID IN ('".implode("','", $user_id)."')";
		    $schnet_target = $lu->returnVector($sql);
		    include_once("../../includes/libschoolnet.php");
		    $lschoolnet = new libschoolnet();
		    $lschoolnet->removeUser($schnet_target);
		}
	
		$lu->prepareUserRemoval($list);
		$lsp = new libstudentpromotion();
		$sql = "SELECT UserID FROM INTRANET_USER WHERE RecordType = ".TYPE_PARENT." AND UserID IN ('".implode("','", $user_id)."')";
		$students = $lu->returnVector($sql);
		
		if (sizeof($students)!=0)
		{
		    $otherList = implode(",",$students);
		    $lsp->archiveIntranetUser($otherList);
		
		}
		
		$lu->prepareUserRemoval($list);
		$lu->removeUsers($list);
		
		# delete calendar viewer
		$sql = "delete from CALENDAR_CALENDAR_VIEWER where UserID in ('".implode("','", $user_id)."')";
		$lu->db_db_query($sql);
		
		$successAry['synUserDataToModules'] = $lu->synUserDataToModules($user_id);

		intranet_closedb();
		header("Location: $comeFrom?xmsg=DeleteSuccess");
		
	}
}
?>
