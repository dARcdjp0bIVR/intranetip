<?php
# modifying by : 

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if(!($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Staff"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"])) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$laccount = new libaccountmgmt();


$linterface = new interface_html();

$CurrentPageArr['eAdminRepairSystem'] = 1;

if($task=='identity')
{	
	$data = "<table width=\"95%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#CCCCCC\" class=\"layer_table_detail\">";
		$data .="<tr class=\"tabletop\">
					<td>".$i_identity."</td>
				</tr>
				<tr class=\"tablerow1\">
					<td>Teaching</td>
				</tr>
				<tr class=\"tablerow1\">
					<td>NonTeaching</td>
				</tr>
						";
	$data .="</table>";	

}

else if($task=='title')
{	
	$data = "<table width=\"95%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#CCCCCC\" class=\"layer_table_detail\">";
		$data .="<tr class=\"tabletop\">
					<td>".$i_UserTitle."</td>
				</tr>
				<tr class=\"tablerow1\">
					<td>Mr.</td>
				</tr>
				<tr class=\"tablerow1\">
					<td>Miss</td>
				</tr>
				<tr class=\"tablerow1\">
					<td>Mrs.</td>
				</tr>
				<tr class=\"tablerow1\">
					<td>Ms.</td>
				</tr>
				<tr class=\"tablerow1\">
					<td>Dr.</td>
				</tr>
				<tr class=\"tablerow1\">
					<td>Prof.</td>
				</tr>
						";
	$data .="</table>";	

}

else if($task=='country')
{	
	$data = "<table width=\"95%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#CCCCCC\" class=\"layer_table_detail\">";
		$data .= "<tr class=\"tabletop\">
					<td>".$i_UserCountry."</td>
				</tr>";
		for($i=0; $i<sizeof($Lang['Country']); $i++) {
			$data .= "<tr class=\"tablerow1\">
							<td>".$Lang['Country'][$i]."</td>
						</tr>";
		}
	$data .= "</table>";	
}

else if($task=='group')
{	
	$sql = "SELECT 
				a.GroupID, a.Title, b.GroupID, c.CategoryName, a.RecordType
			FROM 
				INTRANET_GROUP AS a LEFT OUTER JOIN 
				INTRANET_USERGROUP AS b ON (a.GroupID = b.GroupID) LEFT OUTER JOIN 
				INTRANET_GROUP_CATEGORY as c ON (a.RecordType = c.GroupCategoryID)
			WHERE 
				a.RecordType >= 1 AND 
				(a.AcademicYearID = ".Get_Current_Academic_Year_ID()." or a.AcademicYearID is NULL) 
			GROUP BY
				a.GroupID
			ORDER BY 
				c.GroupCategoryID, a.Title
			";
	$row = $laccount->returnArray($sql,5);
	
	$data = "<table width=\"95%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#CCCCCC\" class=\"layer_table_detail\">";
		$data .= "<tr class=\"tabletop\">
					<td width='30%'>".$i_StaffAttendance_GroupID."</td>
					<td width='70%'>".$i_GroupName."</td>
				</tr>";
		for($i=0; $i<sizeof($row); $i++) {
			$data .= "<tr class=\"tablerow1\">
							<td>".$row[$i][0]."</td>
							<td>(".$row[$i][3].") ".$row[$i][1]."</td>
						</tr>";
		}
	$data .= "</table>";	
}

else if($task=='role')
{	
	$sql = "SELECT RoleID, RoleName FROM ROLE ORDER BY RoleName";
	$row = $laccount->returnArray($sql,2);
	
	$data = "<table width=\"95%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#CCCCCC\" class=\"layer_table_detail\">";
		$data .= "<tr class=\"tabletop\">
					<td width='35%'>".$Lang['AccountMgmt']['RoleID']."</td>
					<td width='65%'>".$Lang['Header']['Menu']['Role']."</td>
				</tr>";
		for($i=0; $i<sizeof($row); $i++) {
			$data .= "<tr class=\"tablerow1\">
							<td>".$row[$i][0]."</td>
							<td>".$row[$i][1]."</td>
						</tr>";
		}
	$data .= "</table>";	
}

$output = '<table width="50%" border="0" cellpadding="0" cellspacing="0">
			  <tr>
				<td height="19"><table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
					<td width="5" height="19"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_01.gif" width="5" height="19"></td>
					<td height="19" valign="middle" background="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_02.gif">&nbsp;</td>
					<td width="19" height="19"><a href="javascript:Hide_Window(\'ref_list\')"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_close_off.gif" name="pre_close1" width="19" height="19" border="0" id="pre_close1"></a></td>
				  </tr>
				</table></td>
			  </tr>
			  <tr>
				<td><table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
					<td width="5" background="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_04.gif"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_04.gif" width="5" height="19"></td>
					<td align="left" bgcolor="#FFFFF7"><table width="100%" border="0" cellspacing="0" cellpadding="0">
					  
					  <tr>
						<td height="150" align="left" valign="top">
							<div id="list" style="overflow: auto; z-index:1; height: 150px;">
								'.$data.'
							</div>
						</td></tr>
							  <tr>
						    <td><br><table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
						      <tr>
							       <td height="1" class="dotline"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/10x10.gif" width="10" height="1"></td>
						      </tr>
						     </table></td>
						     </tr>
                 </table></td>
					<td width="6" background="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_06.gif"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_06.gif" width="6" height="6"></td>
				  </tr>
				  <tr>
					<td width="5" height="6"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_07.gif" width="5" height="6"></td>
					<td height="6" background="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_08.gif"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_08.gif" width="5" height="6"></td>
					<td width="6" height="6"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_09.gif" width="6" height="6"></td>
				  </tr>
					</table></td>
			  </tr>
			</table>';
echo $output;
?>
