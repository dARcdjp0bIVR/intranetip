<?php
// Modifng by : 
/*
 * Modification:
 * Date: 2014-09-12 (YatWoon): use "user_id[]" instead of userID[]
 * Date: 2012-09-11 (Carlos): include missing libldap.php
 * Date: 2012-08-01 (Ivan)
 * 	- added logic to syn user info to library system
 */
 
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."includes/libftp.php");
include_once($PATH_WRT_ROOT."includes/libldap.php");

if(!($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"])) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

if(sizeof($_POST)==0) {
	header("Location: index.php");	
	exit;
}

intranet_opendb();
$list = implode(",", $user_id);

$lu = new libuser();
$lu->prepareUserRemoval($list);
$lu->removeUsers($list);

# delete calendar viewer
$sql = "delete from CALENDAR_CALENDAR_VIEWER where UserID in ($list)";
$lu->db_db_query($sql);

$successAry['synUserDataToModules'] = $lu->synUserDataToModules($user_id);

intranet_closedb();
header("Location: index.php?xmsg=DeleteSuccess");
?>
