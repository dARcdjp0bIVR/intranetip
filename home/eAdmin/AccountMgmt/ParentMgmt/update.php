<?php
# using: yat

################# Change Log [Start] ############
# 
#
################# Change Log [End] ############

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

if(!($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Parent"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"])) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");

intranet_auth();
intranet_opendb();

$laccount = new libaccountmgmt();
$lgeneralsettings = new libgeneralsettings();

$data = array();
$thisUserType = USERTYPE_PARENT;

##### Personal Info
$data['CanUpdateNickName_'.$thisUserType] = $nickname ? $nickname : 0;
$data['CanUpdatePersonalPhoto_'.$thisUserType] = $photo ? $photo : 0;
$data['CanUpdateGender_'.$thisUserType] = $gender ? $gender : 0;
$data['CanUpdateDOB_'.$thisUserType] = $dob ? $dob : 0;

##### Contact Info
$data['CanUpdateHomeTel_'.$thisUserType] = $hometel ? $hometel : 0;
$data['CanUpdateOfficeTel_'.$thisUserType] = $officetel ? $officetel : 0;
$data['CanUpdateMobile_'.$thisUserType] = $mobile ? $mobile : 0;
$data['CanUpdateFax_'.$thisUserType] = $fax ? $fax : 0;
$data['CanUpdateAddress_'.$thisUserType] = $address ? $address : 0;
$data['CanUpdateCountry_'.$thisUserType] = $country ? $country : 0;
$data['CanUpdateURL_'.$thisUserType] = $url ? $url : 0;
$data['CanUpdateEmail_'.$thisUserType] = $email ? $email : 0;

##### Message
$data['CanUpdateMessage_'.$thisUserType] = $CanUpdateMessage;

##### Login Password
$data['CanUpdatePassword_'.$thisUserType] = $CanUpdatePassword;

# store in DB
$lgeneralsettings->Save_General_Setting("UserInfoSettings", $data);

intranet_closedb();
header("Location: index.php?xmsg=".$Lang['eNotice']['SettingsUpdateSuccess']);
?>