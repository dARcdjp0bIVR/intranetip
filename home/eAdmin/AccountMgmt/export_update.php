<?php
# using:

########## Change Log [Start] #############
#
#   Date:   2020-02-26 Bill     [2020-0220-1623-28098]
#           support Graduation Date export in standard format
#
#   Date:   2019-05-06 Cameron
#           Customize export fields for Oaks
#
#   Date:   2018-11-20 Anna
#           Added tungwah sdas cust - PrimarySchoolCode
#
#   Date:   2018-09-04 Pun
#           Added $ssoservice["Google"]["Valid"]
#
#   Date:   2018-08-21 Cameron
#           revised columns for HKPF
#
#   Date:   2018-08-10 Cameron
#           add Grade and Duty for HKPF
#
#	Date:	2017-07-14	Icarus
#			fixed the export function of parent, school staff and student
#
#	Date:	2017-06-08 Icarus
#			added house filtering.
#
#	Date:	2016-08-09 Carlos
#			$sys_custom['BIBA_AccountMgmtCust'] - added [HouseholdRegister] and [House] fields for student. 
#												- added [House] field for staff.
#
#	Date:	2016-01-13 Carlos
#			$sys_custom['StudentAccountAdditionalFields'] - added five fields [NonChineseSpeaking], [SpecialEducationNeeds], [PrimarySchool], [University] and [Programme].
#
#	Date:	2015-03-20	Cameron
#			change fax field to date of baptism if $sys_custom['eAdmin']['AcctMgmt']['UseFaxForBaptism'] = true for teacher and student
# 									
#	Date:	2014-12-10	Bill
#			$sys_custom['SupplementarySmartCard'] - Add SmartCardID4 for student user type
#
#	Date:	2014-11-17	Bill
#			Add Guardian Info
#
#	Date:	2014-08-21	Bill
#			Add Home Tel column to default mode
#
#	Date:	2014-07-17	Bill
#			Add flag $plugin['eEnrollment'] for export file with CardID - eEnrolment
#
#	Date:	2014-04-08 (Carlos)
#			if $plugin['medical'], add [StatyOverNight] for default csv for student type			
#
#	Date:	2013-12-12 (Carlos) 
#			iMail plus - $sys_custom['iMailPlus']['EmailAliasName'] Added field [EmailUserLogin] for staff user to input its own iMail plus account name
#
#	Date:	2013-11-13	Carlos
#			$sys_custom['SupplementarySmartCard'] - Add SmartCardID2 and SmartCardID3 for student user type
#
#	Date:	2013-08-13  Carlos
#			hide staff code, WEBSAMS number and JUPAS application number for KIS			
#
#	Date:	2013-03-22	Rita
#			amend DOB format - remove time
#
#	Date:	2013-01-04	Bill Mak
#			fixed: incorrect col_count for student, unable to display AdmissionDate for default format [Case#2013-0103-1644-17071]
#
#	Date:	2012-12-27	YatWoon
#			fixed: incorrect col_count for partent, so cannot display HKID data for default format [Case#2012-1221-1459-47158]
#
#	Date:	2012-09-27	YatWoon
#			add Barcode
#
#	Date:	2012-08-03	Bill Mak
#			add Nationality, PlaceOfBirth, AdmissionDate Group, Role for Export
#
#	Date:	2011-11-03	YatWoon
#			add alumni
#
#	Date :	2011-06-23	(Fai)
#			ADD Application for JUPAS for student account $HKJApplNo
#
#	Date:	2011-01-13	YatWoon
#			improved: add "StudentInfo" for parent export
#
#	Date:	2010-05-20	YatWoon
#			Fixed: cannot export parent's HKID with "Export Default Format"
#
#	Date:	2010-04-19 YatWoon
#			for "LaiShan" project, add parent/guardian name to student export csv
#
#	Date:	2010-04-16 YatWoon
#			Export staff csv for import: missing check CardID should be include or not
#
########## Change Log [End] #############

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbdump.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");

intranet_opendb();

$isKIS = $_SESSION["platform"] == "KIS";

$lexport = new libexporttext();
$li = new libdbdump();

if (!isset($default) || $default != 1)
{
     if(!isset($Fields)) {
         $Fields = array();
     }

     # check if there parent / guardian column is selected
     # set prefix if yes
     if(in_array("Parent", $Fields) || in_array("Guardian", $Fields) || in_array("GuardianInfo", $Fields) || in_array("StudentInfo", $Fields) || in_array("Group", $Fields) || in_array("Role", $Fields))
     {
	     # handle special column
	     foreach($Fields as $k=>$d)
	     {
		  	if($d == "Parent")		{	$WithParent = 1;		unset($Fields[$k]);	}
		  	if($d == "Guardian")	{	$WithGuardian = 1;		unset($Fields[$k]);	}
		  	if($d == "GuardianInfo"){	$WithGuardianInfo = 1;	unset($Fields[$k]);	}
		  	if($d == "StudentInfo")	{	$WithStudentInfo = 1;	unset($Fields[$k]);	}
		  	if($d == "Group")		{	$WithGroup = 1;			unset($Fields[$k]);	}
		  	if($d == "Role")		{	$WithRole = 1;			unset($Fields[$k]);	}
	     }
     }
     
	 $li->setFieldArray($Fields);
	 $li->setTable("INTRANET_USER");

     if ($TabID == "")
     {
         $conds = "";
     }
     else
     {
         $conds = "RecordType = $TabID AND ";
     }

	 if($recordstatus != "")
     {
	 	$conds .= "RecordStatus = '$recordstatus' AND ";
     }

	 if($TeachingType != "")
	 {
        if ($TeachingType == 1 || $TeachingType == 'S') {
            $conds .= "Teaching = '$TeachingType' AND ";
        }
        else {
            $conds .= "(Teaching = '0' OR Teaching IS NULL) AND ";
        }
	 }

	 if($targetClass != "0" && $targetClass != '')
	 {
		include_once($PATH_WRT_ROOT."includes/libclass.php");
		$lclass = new libclass();
		$studentAry = $lclass->storeStudent(0,$targetClass);
		
		if(sizeof($studentAry) > 0)
		{
			if($TabID == 3) {
				$studentConds .= " b.StudentID IN (".implode(',', $studentAry).") AND ";
            }
			else {
				$conds .= " UserID IN (".implode(',', $studentAry).") AND ";
            }
		}
		else
        {
			if($TabID == 3) {
				$studentConds .= " b.StudentID IN (-1) AND ";
            }
			else {
				$conds .= " UserID IN (-1) AND ";
            }
		}
	 }
	
	# house filtering
	$libgroup = new libgroup();
	$houseArr = $libgroup->returnAllGroupIDAndName("","",4,Get_Current_Academic_Year_ID());
	
	if (isset($houseID))
	{
		if ($houseID > 0){
			$sql = "SELECT UserID FROM INTRANET_USERGROUP WHERE GroupID = '".$houseID."'";
			$houseStudentIdAry = Get_Array_By_Key($lexport->returnResultSet($sql), 'UserID');
		
			if($TabID == 3) {
				$studentConds .= " b.StudentID IN (".implode(',', $houseStudentIdAry).") AND ";
            }
			else {
				$conds .= " UserID IN (".implode(',', $houseStudentIdAry).") AND ";
            }
		}
		else if ($houseID == -1) {
			// do nothing
		}
		else if($houseID == -2 || $houseID == -3) {
			// $houseID==-2 => "all house" option => with house students only
			// $houseID==-3 => "without house" option => without house students only
			// Get All House ID in current ac yr
			$houseGroupIDArr = Get_Array_By_Key($houseArr, 'GroupID');
			$houseStudentIdAry = array();
			for($i=0; $i<count($houseArr); $i++) {
				// Create new obj for each house
				$groupObj = new libgroup($houseGroupIDArr[$i]);
				$houseStudentIdAry = array_values(array_unique(array_merge($houseStudentIdAry,Get_Array_By_Key($groupObj->Get_Group_User(USERTYPE_STUDENT), 'UserID'))));
			}

			if($houseID == -2){
				$conds .= " UserID IN (".implode(',',$houseStudentIdAry).") AND ";
			}
			else if($houseID == -3){
				$conds .= " UserID NOT IN (".implode(',',$houseStudentIdAry).") AND ";
			}
		}		
	}

     # parent
     if($TabID == 3)
     {
	     $li2 = new libdb();
	     
		 if(!isset($notInList) || $notInList == "") 	# only export User List
         {
		     $sqlParents = "SELECT DISTINCT UserID FROM INTRANET_USER as a, INTRANET_PARENTRELATION as b WHERE $studentConds b.ParentID IS NOT NULL AND b.StudentID IS NOT NULL AND a.UserID = b.ParentID";
		     $r = $li2->returnVector($sqlParents,1);
	    	 $parentIDs = implode(",",$r);
	
//			 if(sizeof($r)>0)
//			 	$conds .= " UserID IN (".implode(',', $r).") AND ";
             $conds .= " UserID IN (".(sizeof($r) > 0 ? implode(',', $r) : '-1').") AND ";
		 }
		 else
         {
     		# parent not in list
     		$sql = "SELECT UserID FROM INTRANET_USER WHERE RecordType = 3 AND UserID NOT IN (SELECT DISTINCT ParentID FROM INTRANET_PARENTRELATION)";
     		$result = $li->returnVector($sql);

			# class filter
     		include_once($PATH_WRT_ROOT."includes/libclass.php");
     		$lclass = new libclass();
     		
     		if(isset($targetClass) && $targetClass != "0") {
	     		$studentIDAry = $lclass->storeStudent('0',$targetClass,Get_Current_Academic_Year_ID());
	     		$addCond = " WHERE StudentID IN (".implode(',',$studentIDAry).")";
     		}
     		
     		//if(sizeof($studentIDAry)>0) {
     			$sql = "SELECT ParentID FROM INTRANET_PARENTRELATION $addCond";
     			$temp = $li->returnVector($sql);

     			if(sizeof($temp) > 0) {
     				$result = array_merge($result,$temp);
                }
     		//}

     		$conds .= " UserID IN (".implode(',',$result).") AND ";
     	}
	 }

	 # student
	 if($TabID == 2)
	 {
	 	/*
	 	if(!isset($notInList) || $notInList=="") {	# only export User List
		 	$sql = "SELECT UserID FROM YEAR_CLASS_USER ycu LEFT OUTER JOIN YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID) WHERE yc.AcademicYearID=".Get_Current_Academic_Year_ID();
		 	$temp = $li->returnVector($sql);
		 	if(sizeof($temp)>0) $conds .= " UserID IN (".implode(',',$temp).") AND ";
		}
		*/
		if($userList && $notInList) {	// all students
			// no addition condition	
		}
		else {
			$sql = "SELECT UserID FROM YEAR_CLASS_USER ycu LEFT OUTER JOIN YEAR_CLASS yc ON (yc.YearClassID = ycu.YearClassID) WHERE yc.AcademicYearID = ".Get_Current_Academic_Year_ID();
            $temp = $li->returnVector($sql);
		 		
			if($userList) {		// only export user list (with classes)
		 		if(sizeof($temp) > 0) {
		 		    $conds .= " UserID IN (".implode(',',$temp).") AND ";
                }
			} 
			else if($notInList) {	// only export user list (without classes)
				if(sizeof($temp) > 0) {
				    $conds .= " UserID NOT IN (".implode(',',$temp).") AND ";
                }
			}
			else {
				// export nothing	
			}
		}
	 }
     $li->setFilterSQL("WHERE $conds (UserLogin LIKE '%$keyword%' OR UserEmail LIKE '%$keyword%' OR ChineseName LIKE '%$keyword%' OR EnglishName LIKE '%$keyword%' OR ClassName LIKE '%$keyword%' OR WebSamsRegNo LIKE '%$keyword%')");

	$url = "/file/export/eclass-user-".session_id()."-".time().".csv";
    #$lo = new libfilesystem();
    $export_content = $li->getDumpDataWithSeparator_utf($WithParent, $WithGuardian, 0, $WithStudentInfo, $TabID, $WithGroup, $WithRole, $WithGuardianInfo);

	# change "StaffCode" to "WebSAMSStaffCode"
	$export_content = str_replace("StaffCode", "WebSAMSStaffCode", $export_content);
	
	# teacher & student use fax field for date of baptism
	if ($sys_custom['eAdmin']['AcctMgmt']['UseFaxForBaptism'] && (($TabID == 1) || ($TabID == 2))) {
		$export_content = str_replace("FaxNo", "DateOfBaptism", $export_content);
	}
	#$lo->file_write($li->getDumpDataWithSeparator(), $intranet_root.$url);
}
else # Export default format
{
     if ($TabID == "")
     {
         $tab_conds = "";
     }
     else
     {
         $tab_conds = " U.RecordType = $TabID AND ";
     }

	 if($recordstatus != "")
     {
	 	$tab_conds .= " U.RecordStatus = '$recordstatus' AND ";
     }

	 if($TeachingType != "")
     {
	 	$tab_conds .= ($TeachingType == 1 || $TeachingType == 'S') ? " U.Teaching = '$TeachingType' AND " : " (U.Teaching = '0' OR U.Teaching IS NULL) AND ";
     }

	 if($targetClass != "0" && $targetClass != '')
	 {
		include_once($PATH_WRT_ROOT."includes/libclass.php");
		$lclass = new libclass();
		$studentAry = $lclass->storeStudent(0,$targetClass);

		if(sizeof($studentAry) > 0)
		{
			if($TabID == 3) {
				$studentConds .= " b.StudentID IN (".implode(',', $studentAry).") AND ";
            }
			else {
				$tab_conds .= " U.UserID IN (".implode(',', $studentAry).") AND ";
            }
		}
		else
        {
			if($TabID == 3) {
				$studentConds .= " b.StudentID IN (-1) AND ";
            }
			else {
				$tab_conds .= " U.UserID IN (-1) AND ";
            }
		}
	 }
	
     if($TabID == 3)	// parent
     {
      	if(!isset($notInList) || $notInList == "") 	# only export User List
        {
            $li2 = new libdb();
            $sqlParents = "SELECT DISTINCT a.UserID FROM INTRANET_USER as a, INTRANET_PARENTRELATION as b WHERE $studentConds b.ParentID IS NOT NULL AND b.StudentID IS NOT NULL AND a.UserID = b.ParentID";
            $r = $li2->returnVector($sqlParents,1);

            $parentIDs = implode(",",$r);
            //if(sizeof($r)>0)
                $tab_conds .= " U.UserID IN (".(sizeof($r) > 0 ? implode(',', $r) : '-1').") AND ";
      	}
      	else
        {
     		# parent not in list
     		$sql = "SELECT UserID FROM INTRANET_USER WHERE RecordType = 3 AND UserID NOT IN (SELECT DISTINCT ParentID FROM INTRANET_PARENTRELATION)";
     		$result = $li->returnVector($sql);

			# class filter
     		include_once($PATH_WRT_ROOT."includes/libclass.php");
     		$lclass = new libclass();
     		
     		if(isset($targetClass) && $targetClass != "0") {
	     		$studentIDAry = $lclass->storeStudent('0',$targetClass,Get_Current_Academic_Year_ID());
	     		$addCond = " WHERE StudentID IN (".implode(',',$studentIDAry).")";
     		}
     		
     		//if(sizeof($studentIDAry)>0) {
     			$sql = "SELECT ParentID FROM INTRANET_PARENTRELATION $addCond";
     			$temp = $li->returnVector($sql);

     			if(sizeof($temp) > 0) {
     				$result = array_merge($result,$temp);
                }
     		//}

     		$conds .= " U.UserID IN (".implode(',',$result).") AND ";
     	}
	 }

	 # student
	 if($TabID == 2)
	 {
	 	/*
	 	if(!isset($notInList) || $notInList=="") {	# only export User List
	 		$li2 = new libdb();
		 	$sql = "SELECT UserID FROM YEAR_CLASS_USER ycu LEFT OUTER JOIN YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID) WHERE yc.AcademicYearID=".Get_Current_Academic_Year_ID();
		 	$temp = $li2->returnVector($sql);
		 	if(sizeof($temp)>0) $tab_conds .= " UserID IN (".implode(',',$temp).") AND ";
		}
		*/
		if($userList && $notInList) {	// all students
			// no addition condition	
		}
		else {
			$sql = "SELECT UserID FROM YEAR_CLASS_USER ycu LEFT OUTER JOIN YEAR_CLASS yc ON (yc.YearClassID = ycu.YearClassID) WHERE yc.AcademicYearID = ".Get_Current_Academic_Year_ID();
            $temp = $li->returnVector($sql);
		 		
			if($userList) {		// only export user list (with classes)
		 		if(sizeof($temp) > 0) {
		 		    $tab_conds .= " U.UserID IN (".implode(',',$temp).") AND ";
                }
			} 
			else if($notInList) {	// only export user list (without classes)
				if(sizeof($temp) > 0) {
				    $tab_conds .= " U.UserID NOT IN (".implode(',',$temp).") AND ";
                }
			}
			else {
				// export nothing	
			}
		}
	 }

    # house filtering
	$libgroup = new libgroup();
	$houseArr = $libgroup->returnAllGroupIDAndName("","",4,Get_Current_Academic_Year_ID());
	
	if (isset($houseID))
	{
		if ($houseID > 0) {
			$sql = "SELECT UserID FROM INTRANET_USERGROUP WHERE GroupID = '".$houseID."'";
			$houseStudentIdAry = Get_Array_By_Key($lexport->returnResultSet($sql), 'UserID');
		
			$conds_houseUser = " U.UserID IN (".implode(',', $houseStudentIdAry).") AND ";			
		}
		else if ($houseID == -1) {
			// do nothing
		}
		else if($houseID == -2 || $houseID == -3) {
			// $houseID==-2 => "all house" option => with house students only
			// $houseID==-3 => "without house" option => without house students only
			// Get All House ID in current ac yr
			$houseGroupIDArr = Get_Array_By_Key($houseArr, 'GroupID');
			$houseStudentIdAry = array();
			for($i=0; $i<count($houseArr); $i++){
				// Create new obj for each house
				$groupObj = new libgroup($houseGroupIDArr[$i]);
				$houseStudentIdAry = array_values(array_unique(array_merge($houseStudentIdAry,Get_Array_By_Key($groupObj->Get_Group_User(USERTYPE_STUDENT), 'UserID'))));
			}

			if($houseID == -2){
				$conds_houseUser = " U.UserID IN (".implode(',',$houseStudentIdAry).") AND ";
			}
			else if($houseID == -3){
				$conds_houseUser = " U.UserID NOT IN (".implode(',',$houseStudentIdAry).") AND ";
			}
		}		
	}
    $conds = " WHERE $tab_conds $conds_houseUser (U.UserLogin LIKE '%$keyword%' OR U.UserEmail LIKE '%$keyword%' OR U.ChineseName LIKE '%$keyword%' OR U.EnglishName LIKE '%$keyword%' OR U.ClassName LIKE '%$keyword%' OR U.WebSamsRegNo LIKE '%$keyword%')";

    if ($TabID == 1)	# Teacher
    {
         if ($sys_custom['project']['HKPF']) {
             $col_count = 9;
             $sql = "SELECT U.UserID, U.UserLogin, '', U.UserEmail, U.EnglishName, U.ChineseName, U.Remark, U.Grade, U.Duty";
         }
         else {
            $col_count = 14;
            
     //    if($special_feature['ava_hkid']){
     //    	$col_count++;
     //    }
     //    if((isset($module_version['StaffAttendance'])&&$module_version['StaffAttendance']!="" ) || (isset($plugin['payment'])&& $plugin['payment'])){
     //    	$col_count++;
     //    }
            $sql = "SELECT U.UserID, U.UserLogin, '', U.UserEmail, U.EnglishName, U.ChineseName, U.NickName, 
                            U.Gender, U.MobileTelNo, U.FaxNo, U.Barcode, U.Remark, U.TitleEnglish, U.TitleChinese ";
            if(!$isKIS) {
             	$col_count++;
             	$sql .= ", U.StaffCode ";
            }
         }

        if((isset($module_version['StaffAttendance']) && $module_version['StaffAttendance'] != "") || (isset($plugin['payment']) && $plugin['payment']))
        {
        	$col_count++;
        	$sql .= ", U.CardID ";
        }

        if($special_feature['ava_hkid'] && !$sys_custom['project']['HKPF'])
        {
			$col_count++;
			$sql .= ", U.HKID ";
		}

		if($sys_custom['BIBA_AccountMgmtCust'])
		{	
			$col_count++;
			$houseGroupNameField = Get_Lang_Selection('G.TitleChinese','G.Title');
			$sql .= ",GROUP_CONCAT(DISTINCT $houseGroupNameField SEPARATOR ', ') as House ";
			$join_tables = " LEFT JOIN INTRANET_USERGROUP as UG ON UG.UserID = U.UserID 
			  				 LEFT JOIN INTRANET_GROUP as G ON G.GroupID=UG.GroupID AND G.RecordType = 4 AND G.AcademicYearID = '".Get_Current_Academic_Year_ID()."' ";
		}

		if($plugin['imail_gamma'] && $sys_custom['iMailPlus']['EmailAliasName'])
		{
			$col_count++;
			$sql .= ",U.ImapUserLogin ";
		}

		if($ssoservice["Google"]["Valid"])
		{
			$col_count++;
			$col_count++;
		    $sql .= ", IF(ISA.SSOAccountStatus <> 'n' AND ISA.SSOAccountStatus <> 'x' AND ISA.SSOAccountStatus IS NOT NULL, 'Y', 'N') AS EnableGSuite, U.GoogleUserLogin ";
		    $join_tables .= " LEFT JOIN INTRANET_SSO_ACCOUNT ISA ON U.UserID = ISA.UserID";
		}
		$sql .= " FROM INTRANET_USER as U $join_tables $conds GROUP BY U.UserID ORDER BY U.EnglishName";
     }
     else if ($TabID == 2)	# student
     {
         if ($sys_custom['project']['CourseTraining']['Oaks'])
         {
             $col_count = 13;
             $sql = "SELECT 
                            U.UserID, U.UserLogin, '', U.UserEmail, U.EnglishName, U.ChineseName, U.Gender,
                            U.HomeTelNo, U.MobileTelNo, U.FaxNo, U.Remark, U.Address, U.CardID
                     FROM
                            INTRANET_USER U 
                     $conds 
                     GROUP BY U.UserID 
                     ORDER BY U.ClassName, U.ClassNumber, U.EnglishName";
         }
         else
         {
             // [2020-0220-1623-28098]
             //$col_count = 18;
             $col_count = 19;

             //$conds = str_replace('UserID', 'U.userID', $conds);
             if($special_feature['ava_hkid'])	$col_count++;
             if($special_feature['ava_strn'])	$col_count++;
             if($sys_custom['StudentAccountAdditionalFields']) {
             	$col_count += 5;	
             }

             if((isset($plugin['attendancestudent']) && $plugin['attendancestudent']) || (isset($plugin['payment']) && $plugin['payment']) || (isset($plugin['eEnrollment']) && $plugin['eEnrollment']))
             {
                 $col_count += 1;  
             	 if($sys_custom['SupplementarySmartCard']){
             		$col_count += 3;
             	 }
             }
             
             $sql = "SELECT U.UserID, U.UserLogin, '', U.UserEmail, U.EnglishName, U.ChineseName, U.NickName, U.Gender, 
                            U.HomeTelNo, U.MobileTelNo, U.FaxNo, U.Barcode, U.Remark, DATE_FORMAT(U.DateOfBirth, '%Y-%m-%d') ";
             if(!$isKIS) {
             	$col_count += 2;    
                $sql .= ", IF(TRIM(U.WebSAMSRegNo)!='' AND LEFT(TRIM(U.WebSAMSRegNo),1)!='#',CONCAT('#',TRIM(U.WebSAMSRegNo)),TRIM(WebSAMSRegNo)) ";
    			$sql .= ", if (U.HKJApplNo = 0,'',U.HKJApplNo) ";
             }
             $sql .= ",U.Address ";

             if((isset($plugin['attendancestudent']) && $plugin['attendancestudent']) ||(isset($plugin['payment']) && $plugin['payment']) || (isset($plugin['eEnrollment']) && $plugin['eEnrollment']))
             {
                $sql .= ", U.CardID ";
                if($sys_custom['SupplementarySmartCard']){
                    $sql .= ", U.CardID2, U.CardID3, U.CardID4 ";
                }
             }

             if($special_feature['ava_hkid'])
             {
                $sql .= ", U.HKID ";
             }

             if($special_feature['ava_strn'])
             {
                $sql .= ", U.STRN ";
             }

             if($ssoservice["Google"]["Valid"])
             {
                $col_count++;
                $sql .= ", IF(ISA.SSOAccountStatus <> 'n' AND ISA.SSOAccountStatus <> 'x' AND ISA.SSOAccountStatus IS NOT NULL, 'Y', 'N') AS EnableGSuite";
                $join_tables .= " LEFT JOIN INTRANET_SSO_ACCOUNT ISA ON U.UserID=ISA.UserID";
             }

             if($sys_custom['BIBA_AccountMgmtCust'])
             {
                $col_count += 2;
                $sql .= ",U.HouseholdRegister ";
                $houseGroupNameField = Get_Lang_Selection('G.TitleChinese','G.Title');
                $sql .= ",GROUP_CONCAT(DISTINCT $houseGroupNameField SEPARATOR ', ') as House ";
                $join_tables = " LEFT JOIN INTRANET_USERGROUP as UG ON UG.UserID = U.UserID 
                                 LEFT JOIN INTRANET_GROUP as G ON G.GroupID = UG.GroupID AND G.RecordType = 4 AND G.AcademicYearID = '".Get_Current_Academic_Year_ID()."' ";
             }

             if($sys_custom['StudentAccountAdditionalFields'])
             {
                $sql .= ",U.NonChineseSpeaking, U.SpecialEducationNeeds, U.PrimarySchool, U.University, U.Programme ";
             }

             if($plugin['medical'])
             {
                $col_count += 1;
                $sql .= ",IF(P.stayOverNight IS NOT NULL AND P.stayOverNight='1','Y','') as StayOverNight ";
             }

             // [2020-0220-1623-28098]
             //$sql .= ", P.Nationality, P.PlaceOfBirth, P.AdmissionDate";
             $sql .= ", P.Nationality, P.PlaceOfBirth, P.AdmissionDate, P.GraduationDate";
             
             if($plugin['SDAS_module']['KISMode'])
             {
             	$col_count += 1;
             	$sql .= ", P.KISClassType";
             }
             
             if($plugin['StudentDataAnalysisSystem_Style'] == "tungwah")
             {
                 $col_count += 1;
                 $sql .= ", U.PrimarySchoolCode";
             }
    
             $sql .= " FROM INTRANET_USER U LEFT JOIN INTRANET_USER_PERSONAL_SETTINGS P on U.UserID = P.UserID $join_tables $conds GROUP BY U.UserID ORDER BY U.ClassName, U.ClassNumber, U.EnglishName";
         }         
     }
     else if ($TabID == 3)	# parent
     {
         $col_count = 11;
         if($special_feature['ava_hkid'])	$col_count++;
         
         $sql = "SELECT U.UserID, U.UserLogin, '', U.UserEmail, U.EnglishName, U.ChineseName,
                        U.Gender, U.MobileTelNo, U.FaxNo, U.Barcode, U.Remark";
         if($special_feature['ava_hkid']) {
             $sql .= ", U.HKID ";
         }
		 $sql .= " FROM INTRANET_USER as U $conds ORDER BY U.EnglishName";
     }
     else # alumni
     {
         $col_count = 15;
         /*
         $sql = "SELECT 
         	a.UserID, a.UserLogin, '', a.UserEmail, a.EnglishName, a.ChineseName, a.Nickname,
             a.Gender, a.MobileTelNo, a.FaxNo, a.Remark, 
             if(a.ClassName, a.ClassName, b.ClassName),
             if(a.ClassNumber, a.ClassNumber, b.ClassNumber),
             if(a.YearOfLeft, a.YearOfLeft, b.YearOfLeft)
             FROM 
             	INTRANET_USER as a
             	left join INTRANET_ARCHIVE_USER as b on (b.UserID=a.UserID)
             Where
             	a.RecordType = 4 
             ORDER BY a.EnglishName";
             */
			$sql = "SELECT 
                         UserID, UserLogin, '', UserEmail, EnglishName, ChineseName, Nickname,
                         Gender, MobileTelNo, FaxNo, Barcode, Remark, 
                         ClassName,
                         ClassNumber,
                         YearOfLeft
                    FROM 
                        INTRANET_USER 
                    Where
                        RecordType = 4 
                    ORDER BY EnglishName";
     }

     $li = new libdb();
     $row = $li->returnArray($sql,$col_count);
     //debug_pr($sql);debug_pr($row);

     if ($TabID == 3)	# parent
     {
         # Try to find children relation
         $sql = " SELECT
                        b.ParentID, c.UserLogin, c.EnglishName
                  FROM INTRANET_PARENTRELATION as b
                  LEFT OUTER JOIN INTRANET_USER as a ON b.ParentID = a.UserID
                  LEFT OUTER JOIN INTRANET_USER as c ON b.StudentID = c.UserID AND c.RecordType = 2
                  WHERE a.UserID IS NOT NULL AND c.UserID IS NOT NULL
                  ORDER BY a.UserID";
         $raw_relations = $li->returnArray($sql,3);

         $relations = array();
         $i = 0;
         $prev_id = 0;
         #print_r($raw_relations);
         while ($i < sizeof($raw_relations))
         {
                $temp = array();
                list($id,$login,$eng) = $raw_relations[$i];
                $prev_id = $id;
                while ($prev_id == $id)
                {
                       $temp[] = array($login,$eng);
                       $i++;
                       list($id,$login,$eng) = $raw_relations[$i];
                }
                $relations[$prev_id] = $temp;
         }
         #print_r($relations);

         $x = "UserLogin,Password,UserEmail,EnglishName,ChineseName,Gender,Mobile,Fax,Barcode,Remarks";
         $exportColumn = array("UserLogin","Password","UserEmail","EnglishName","ChineseName","Gender","Mobile","Fax","Barcode","Remarks");
         
         if($special_feature['ava_hkid'])
         {
			$x .= ",HKID";
			$exportColumn = array_merge($exportColumn, array("HKID"));
         }
		 $x .= ",StudentLogin1,StudentEngName1,StudentLogin2,StudentEngName2,StudentLogin3,StudentEngName3";
		 $exportColumn = array_merge($exportColumn, array("StudentLogin1","StudentEngName1","StudentLogin2","StudentEngName2","StudentLogin3","StudentEngName3"));
     }
     else if ($TabID == 1)		# teacher
     {
         if ($sys_custom['project']['HKPF']){
             $x = "UserLogin,Password,UserEmail,EnglishName,ChineseName,Remarks,Grade,Duty";
             $exportColumn = array("UserLogin","Password","UserEmail","EnglishName","ChineseName","Remarks","Grade","Duty");
         }
         else {
             $x = "UserLogin,Password,UserEmail,EnglishName,ChineseName,NickName,Gender,Mobile,Fax,Barcode,Remarks,TitleEnglish,TitleChinese";
             if(!$isKIS) {
             	$x .= ",WebSAMSStaffCode";
             }

             $colFax = ($sys_custom['eAdmin']['AcctMgmt']['UseFaxForBaptism']) ? "DateOfBaptism" : "Fax";
             $exportColumn = array("UserLogin","Password","UserEmail","EnglishName","ChineseName","NickName","Gender","Mobile","$colFax","Barcode","Remarks","TitleEnglish","TitleChinese");
             if(!$isKIS) {
             	$exportColumn = array_merge($exportColumn, array("WebSAMSStaffCode"));
             }
         }

         if((isset($module_version['StaffAttendance']) && $module_version['StaffAttendance'] != "" ) || (isset($plugin['payment']) && $plugin['payment']))
		 {
			$x .= ",CardID";
			$exportColumn = array_merge($exportColumn, array("CardID"));
		 }

		 if($special_feature['ava_hkid'] && !$sys_custom['project']['HKPF'])
         {
			$x .= ",HKID";
			$exportColumn = array_merge($exportColumn, array("HKID"));
         }

         if($sys_custom['BIBA_AccountMgmtCust'])
         {
         	$exportColumn = array_merge($exportColumn, array("House"));
         }

         if($plugin['imail_gamma'] && $sys_custom['iMailPlus']['EmailAliasName'])
         {
         	$x .= ",EmailUserLogin";
         	$exportColumn = array_merge($exportColumn, array("EmailUserLogin"));
         }

         if($ssoservice["Google"]["Valid"])
         {
            $x .= ",EnableGSuite,GoogleUserLogin";
         	$exportColumn = array_merge($exportColumn, array("EnableGSuite","GoogleUserLogin"));
         }
     }
     else if ($TabID == 2)		# student
     {
         if ($sys_custom['project']['CourseTraining']['Oaks']) {
             $exportColumn = array("UserLogin","Password","UserEmail","EnglishName","ChineseName","Gender","Home Tel","Mobile","Fax","Remarks","Address","CardID");
         }
         else {
             $x = "UserLogin,Password,UserEmail,EnglishName,ChineseName,NickName,Gender,Home Tel,Mobile,Fax,Barcode,Remarks,DOB";
             if(!$isKIS) {
             	$x .= ",WebSAMSRegNo,HKJApplNo";
             }

         	 $colFax = ($sys_custom['eAdmin']['AcctMgmt']['UseFaxForBaptism']) ? "DateOfBaptism" : "Fax";
         	 // $exportColumn is used to export to default format
             $exportColumn = array("UserLogin","Password","UserEmail","EnglishName","ChineseName","NickName","Gender","Home Tel","Mobile","$colFax","Barcode","Remarks","DOB");
    		 if(!$isKIS) {
    		 	$exportColumn = array_merge($exportColumn, array("WebSAMSRegNo","HKJApplNo"));
    		 }

    		 $x .= ",Address";
    		 $exportColumn = array_merge($exportColumn,array("Address"));

             if((isset($plugin['attendancestudent']) && $plugin['attendancestudent']) || (isset($plugin['payment']) && $plugin['payment']) || (isset($plugin['eEnrollment']) && $plugin['eEnrollment']))
    		 {
    			$x .= ",CardID";
    			$exportColumn = array_merge($exportColumn, array("CardID"));
    			if($sys_custom['SupplementarySmartCard']){
    				$x .= ",CardID2,CardID3,CardID4 ";
    				$exportColumn = array_merge($exportColumn, array("CardID2","CardID3","CardID4"));
    			}
    		 }

             if($special_feature['ava_hkid'])
             {
    			$x .= ",HKID";
    			$exportColumn = array_merge($exportColumn, array("HKID"));
             }
             
             if($special_feature['ava_strn'])
             {
    			$x .= ",STRN";
    			$exportColumn = array_merge($exportColumn, array("STRN"));
             }
    		 
             if($ssoservice["Google"]["Valid"])
             {
                $x .= ",EnableGSuite";
             	$exportColumn = array_merge($exportColumn, array("EnableGSuite"));
             }

             if($sys_custom['BIBA_AccountMgmtCust'])
             {
             	$exportColumn = array_merge($exportColumn, array("HouseholdRegister","House"));
             }

             if($sys_custom['StudentAccountAdditionalFields'])
             {
             	$x .= ",Non-Chinese Speaking,Special Education Needs,Primary School,University/Institution,Programme ";
             	$exportColumn = array_merge($exportColumn, array("Non-Chinese Speaking"));
             	$exportColumn = array_merge($exportColumn, array("Special Education Needs"));
             	$exportColumn = array_merge($exportColumn, array("Primary School"));
             	$exportColumn = array_merge($exportColumn, array("University/Institution"));
             	$exportColumn = array_merge($exportColumn, array("Programme"));
             }
             
             if($plugin['medical'])
             {
            	$x .= ",StayOverNight ";
            	$exportColumn = array_merge($exportColumn, array("StayOverNight"));
             }

             // [2020-0220-1623-28098]
             //$x .= ",Nationality, PlaceOfBirth, AdmissionDate";
             //$exportColumn = array_merge($exportColumn, array("Nationality", "PlaceOfBirth", "AdmissionDate"));
             $x .= ",Nationality, PlaceOfBirth, AdmissionDate, GraduationDate";
             $exportColumn = array_merge($exportColumn, array("Nationality", "PlaceOfBirth", "AdmissionDate", "GraduationDate"));
    		 
             if($plugin['SDAS_module']['KISMode']){
             	$x .= ",KISClassType";
             	$exportColumn = array_merge($exportColumn, array("KISClassType"));
             }
             
    		 if($plugin['StudentDataAnalysisSystem_Style'] == "tungwah"){ 
    		     $x .= ",PrimarySchoolCode";
    		     $exportColumn = array_merge($exportColumn, array("PrimarySchoolCode"));     
    		 }
         }
     }
     else	# alumni
     {
         $x = "UserLogin,Password,UserEmail,EnglishName,ChineseName,Nickname,Gender,Mobile,Fax,Barcode,Remarks,ClassName,ClassNumber,YearOfLeft";
         $exportColumn = array("UserLogin","Password","UserEmail","EnglishName","ChineseName","Nickname","Gender","Mobile","Fax","Barcode","Remarks","ClassName","ClassNumber","YearOfLeft");
         
         /*
         if($special_feature['ava_hkid'])
         {
			$x .= ",HKID";
			$exportColumn = array_merge($exportColumn, array("HKID"));
         }
         */
     }
     $x .= "\n";

    /*
     for($i=0; $i<sizeof($row); $i++)
     {
         $delim = "";
         $row_length = ($TabID==3? 11: $col_count);
         for($j=1; $j<$row_length; $j++)
         {
             $row[$i][$j] = "\"".intranet_undo_htmlspecialchars($row[$i][$j])."\"";
             $x .= $delim.$row[$i][$j];
             $delim = ",";
         }
         #$x .= implode(",", $row[$i]);
         if ($TabID == 3)
         {
             $child_data = $relations[$row[$i][0]];
             for ($k=0; $k<sizeof($child_data); $k++)
             {
                  list($login, $eng) = $child_data[$k];
                  $x .= ",\"".intranet_undo_htmlspecialchars($login)."\",\"".intranet_undo_htmlspecialchars($eng)."\"";
             }

         }
         $x .= "\n";
    }
    */

     for($i=0; $i<sizeof($row); $i++)
     {
         $delim = "";
         //$row_length = ($TabID==3? 11: $col_count);
         $row_length = $col_count; 
         $child_data = $relations[$row[$i][0]];
         
         if($TabID != 3)
         {
            for($j=1; $j<$row_length; $j++)
            {
                 $utf_row[] = intranet_undo_htmlspecialchars($row[$i][$j]);
                 $row[$i][$j] = "\"".intranet_undo_htmlspecialchars($row[$i][$j])."\"";
                 $x .= $delim.$row[$i][$j];
                 $delim = ",";
            }
            #$x .= implode(",", $row[$i]);
            $utf_rows[] = $utf_row;

            unset($utf_row);
            $x .="\n";
	     }
	     # parent
		 else
         {
			 	$child_data = $relations[$row[$i][0]];
			 	
			 	# Linked
			 	if($parentLink == 1)
			 	{
				 	if(sizeof($child_data) > 0)
				 	{
				      	for($j=1; $j<$row_length; $j++){
					      	 $utf_row[] = intranet_undo_htmlspecialchars($row[$i][$j]);
				             $row[$i][$j] = "\"".intranet_undo_htmlspecialchars($row[$i][$j])."\"";
				             $x .= $delim.$row[$i][$j];				             
				             $delim = ",";
	      				}

	      		        for ($k=0; $k<sizeof($child_data); $k++){
			                 list($login, $eng) = $child_data[$k];
			                 $x .= ",\"".intranet_undo_htmlspecialchars($login)."\",\"".intranet_undo_htmlspecialchars($eng)."\"";
			                 $utf_row[] = intranet_undo_htmlspecialchars($login);
			                 $utf_row[] = intranet_undo_htmlspecialchars($eng);
	            		}
// 	            		$utf_rows[] = $utf_row;
// 	            		unset($utf_row);
// 	            		$x .="\n";	 	
					}
				}
				# No Linked
			 	else if($parentLink == 2)
			 	{
				 	if(sizeof($child_data) <= 0)
				 	{
				      	for($j=1; $j<$row_length; $j++){
					      	 $utf_row[] = intranet_undo_htmlspecialchars($row[$i][$j]);
				             $row[$i][$j] = "\"".intranet_undo_htmlspecialchars($row[$i][$j])."\"";
				             $x .= $delim.$row[$i][$j];				             
				             $delim = ",";
	      				}
// 	      				$x .="\n";
// 	      				$utf_rows[] = $utf_row;
// 	            		unset($utf_row);
					}
				}
			 	# ALL 
			 	else if($parentLink == "")
			 	{
				      	for($j=1; $j<$row_length; $j++){
					      	 $utf_row[] = intranet_undo_htmlspecialchars($row[$i][$j]);
				             $row[$i][$j] = "\"".intranet_undo_htmlspecialchars($row[$i][$j])."\"";
				             $x .= $delim.$row[$i][$j];				             
				             $delim = ",";
	      				}

	      		        for ($k=0; $k<sizeof($child_data); $k++){
			                 list($login, $eng) = $child_data[$k];
			                 $x .= ",\"".intranet_undo_htmlspecialchars($login)."\",\"".intranet_undo_htmlspecialchars($eng)."\"";
			                 $utf_row[] = intranet_undo_htmlspecialchars($login);
			                 $utf_row[] = intranet_undo_htmlspecialchars($eng);
	            		}
				}

				/*
				if($special_feature['ava_hkid'])
		         {
			         # add skip empty cell
			         for($k=sizeof($child_data); $k<3;$k++)	
			         {
				         $x .= ",\" \",\" \"";
			         	$utf_row[] = " ";
			         	$utf_row[] = " ";
			         }
			         
			         $x .= ",\"".intranet_undo_htmlspecialchars($row[$i]['HKID'])."\"";
			         $utf_row[] = intranet_undo_htmlspecialchars($row[$i]['HKID']);
		         }
		         */
		         $utf_rows[] = $utf_row;
		         unset($utf_row);	
			     $x .="\n";
	     }
     }

     #$url = "/file/export/eclass-user-".session_id()."-".time().".csv";
     #$lo = new libfilesystem();
     #$lo->file_write($x, $intranet_root.$url);
     $export_content = $lexport->GET_EXPORT_TXT($utf_rows, $exportColumn);
}

// Output the file to user browser
#$filename = "eclass-user-".session_id()."-".time().".csv";
$filename = "eclass-user.csv";

/*
header("Pragma: public");
header("Expires: 0"); // set expiration time
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");

header("Content-type: application/octet-stream");
header("Content-Length: ".strlen($export_content));
header("Content-Disposition: attachment; filename=\"".$filename."\"");
echo $export_content;
*/
//output2browser($export_content, $filename);

$lexport->EXPORT_FILE($filename, $export_content);
intranet_closedb();
#header("Location: $url");
?>