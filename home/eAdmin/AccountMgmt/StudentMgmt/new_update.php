<?php
# using:  

############# Change Log
#	Date:	2020-09-21 Philips
#			Added KISClassType for $plugin['SDAS_module']['KISMode']
#
#	Date:	2020-03-16 Philips
#			Added syncToStudentRegistry for cdsj_5_macau (customization)
#
#	Date:	2019-06-24 Carlos
#			Empty UserPassword field.
#
#   Date:   2019-05-13 [Henry]
# 			Security fix: SQL without quote
#
#   Date:   2019-04-30 Cameron
#           fix cross site scripting by applying cleanHtmlJavascript() to hidden variables
#
#   Date:   2018-11-12 Paul
#           Handled NCS Lang Priority error. Bypassing the inputfield creation if no Lang chosen.
#
#   Date:   2018-09-19 Anna
#           Added tungwah sdas cust - PrimarySchoolCode
#
#   Date:   2018-09-03 (Bill)   [2017-1207-0956-53277]
#           added Graduation Date
#
#   Date:   2018-08-30 (Pun) [ip.2.5.9.10.1]
#           Modified syncUserFromEClassToGoogle(), added $chiname
#
#	Date:	2018-01-03 (Carlos) [ip.2.5.9.1.1] - Improved password checking.
#
#   Date:   2017-06-06 (Pun) [ip.2.5.8.7.1]
#           added $sys_custom['iPortfolio_auto_active_student'] for auto active iPortfolio account
#
#   Date:   2017-01-13 (HenryHM) [ip.2.5.8.1.1]
#           $ssoservice["Google"]["Valid"] - add GSuite Logic
#
#	Date:	2016-08-09 (Carlos)
#			$sys_custom['BIBA_AccountMgmtCust'] - added new text field HouseholdRegister.
#
#	Date:	2016-05-18 (Carlos)
#			$sys_custom['DisableEmailAfterCreatedNewAccount'] - disable sending notification email after created new user account. 
#
#	Date:	2016-01-13 (Carlos)
#			$sys_custom['StudentAccountAdditionalFields'] - added five fields [NonChineseSpeaking], [SpecialEducationNeeds], [PrimarySchool], [University] and [Programme].
#
#	Date:	2015-12-30 (Carlos)
#			$plugin['radius_server'] - added Wi-Fi access option.
#
#	Date:	2015-05-26 (Carlos)
#			added libaccountmgmt.php suspendEmailFunctions($targetUserId, $resume=false) to resume or suspend mail server auto forward and auto reply functions.
#
#	Date:	2015-05-05 (Omas)
# 			improved - insert MyCalendar
#
#	Date:	2015-03-23 (Bill)
#			fixed - cannot add student to usergroup if usergroup without default group role
# 									
#	Date:	2014-12-10 (Bill)
#			$sys_custom['SupplementarySmartCard'] - Add SmartCardID4
#
#	Date:	2014-09-22 (Bill)
#			Add RoleID to INTRANET_USERGROUP to display default role in Group
#
#	Date:	2014-09-03 (Carlos) - iMail plus: Block receive/send internet mails when [iMail Settings > Default Usage Rights and Quota > Internal mail only] is on
#
#	Date:	2014-05-29 (Carlos) - only send notification email if recipient email is valid
#
#	Date:	2013-12-12	Fai
#			Add a cust attribute "stayOverNight"
#
#	2013-11-13 (Carlos): $sys_custom['SupplementarySmartCard'] - Add SmartCardID2 and SmartCardID3
#
#	Date:	2013-10-30	Carlos
#			KIS - set ePayment account balance to 9999999
#
#	Date:	2013-03-15	Carlos
#			add shared group calendars to user
#
#	Date:	2012-08-30	YatWoon
#			add barcode
#
#	Date:	2012-08-01	Ivan
#			added logic to syn user info to library system
#
# 	Date:	2011-09-30  (Carlos)
#			Store two-way hashed/encrypted password
#
#	Date :	2011-06-23	(Fai)
#			ADD Application for JUPAS for student account $HKJApplNo
#
#	Date:	2011-03-28	YatWoon
#			change email notification subject & content data 
#
#	Date:	2010-11-19	(Henry Chow)
#			add selection of "Group" & "Role"
#
#	Date:	2010-11-05	Marcus
#			Add User Extra Info (Shek Wu eRC Rubrics Cust)
#
#	Date:	2010-08-18	YatWoon
#			Add "Remark" field
#
#################

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libauth.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libregistry.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."includes/libemail.php");
// include_once($PATH_WRT_ROOT."lang/email.php");
include_once($PATH_WRT_ROOT."includes/libftp.php");
include_once($PATH_WRT_ROOT."includes/libldap.php");
include_once($PATH_WRT_ROOT."includes/imap_gamma.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/icalendar.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");

intranet_auth();
intranet_opendb();

if(!($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Student"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"])) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}


if(sizeof($_POST)==0) {
	header("Location: new.php");	
	exit;
}

$magic_quotes_enabled = function_exists("get_magic_quotes_gpc") && get_magic_quotes_gpc();

### start handle sql injection and cross site scripting
$status = IntegerSafe($status);
$comeFrom = cleanCrossSiteScriptingCode($comeFrom);

$targetClass = cleanHtmlJavascript($targetClass);
$recordstatus = cleanHtmlJavascript($recordstatus);
$keyword = cleanHtmlJavascript($keyword);
$userlogin = cleanHtmlJavascript($userlogin);
$pwd = cleanHtmlJavascript($pwd);
$email = cleanHtmlJavascript($email);           // not available in edit
$smartcardid=cleanHtmlJavascript($smartcardid);
if($sys_custom['SupplementarySmartCard']){
    $smartcardid2 = cleanHtmlJavascript($smartcardid2);
    $smartcardid3 = cleanHtmlJavascript($smartcardid3);
    $smartcardid4 = cleanHtmlJavascript($smartcardid4);
}
if($plugin['medical']){
    $stayOverNight = IntegerSafe($stayOverNight);
}
if($plugin['SDAS_module']['KISMode']){
	$KISClassType= IntegerSafe($KISClassType);
}

$strn = cleanHtmlJavascript($strn);
$WebSAMSRegNo = cleanHtmlJavascript($WebSAMSRegNo);
$HKJApplNo = cleanHtmlJavascript($HKJApplNo);
$barcode = cleanHtmlJavascript($barcode);
$engname = cleanHtmlJavascript($engname);
$chiname = cleanHtmlJavascript($chiname);
$nickname = cleanHtmlJavascript($nickname);
$dob = cleanHtmlJavascript($dob);
$gender = cleanHtmlJavascript($gender);
$hkid = cleanHtmlJavascript($hkid);
$address = cleanHtmlJavascript($address);
$country = cleanHtmlJavascript($country);
$homePhone = cleanHtmlJavascript($homePhone);
$mobilePhone = cleanHtmlJavascript($mobilePhone);
$faxPhone = cleanHtmlJavascript($faxPhone);
$open_webmail = cleanHtmlJavascript($open_webmail);
$open_file = cleanHtmlJavascript($open_file);
$remark = cleanHtmlJavascript($remark);

if($plugin['StudentDataAnalysisSystem_Style'] == "tungwah"){
    $PrimarySchoolCode = cleanHtmlJavascript($PrimarySchoolCode);
}

if($sys_custom['StudentAccountAdditionalFields']){
    $NonChineseSpeaking = cleanHtmlJavascript($NonChineseSpeaking);
    $SpecialEducationNeeds = cleanHtmlJavascript($SpecialEducationNeeds);
    $PrimarySchool = cleanHtmlJavascript($PrimarySchool);
    $University = cleanHtmlJavascript($University);
    $Programme = cleanHtmlJavascript($Programme);
}
if($sys_custom['BIBA_AccountMgmtCust']){
    $HouseholdRegister = cleanHtmlJavascript($HouseholdRegister);
}
$Nationality = cleanHtmlJavascript($Nationality);
$PlaceOfBirth = cleanHtmlJavascript($PlaceOfBirth);
$AdmissionDate = cleanHtmlJavascript($AdmissionDate);
$GraduationDate = cleanHtmlJavascript($GraduationDate);
if($plugin['radius_server']){
    $enable_wifi_access = IntegerSafe($enable_wifi_access);
}
//$errorList = cleanHtmlJavascript($errorList);
if(isset($_SESSION['ncs_role'])&&$_SESSION['ncs_role']!=""){
    $staffInCharge = cleanHtmlJavascript($staffInCharge);
    $hkDuration = cleanHtmlJavascript($hkDuration);
    $b5Priority = cleanHtmlJavascript($b5Priority);
    $enPriority = cleanHtmlJavascript($enPriority);
    $hinduPriority = cleanHtmlJavascript($hinduPriority);
    $OthersPriority = cleanHtmlJavascript($OthersPriority);                 // not availabe in edit
    $OthersPriority_details=cleanHtmlJavascript($OthersPriority_details);   // not availabe in edit
    $isNCS = cleanHtmlJavascript($isNCS);
    $NGO = cleanHtmlJavascript($NGO);
}
### end handle sql injection and cross site scripting


$lauth = new libauth();
$laccount = new libaccountmgmt();
$li = new libregistry();
$lwebmail = new libwebmail();
$luser = new libuser();
$lcalendar = new icalendar();

$thisUserType = USERTYPE_STUDENT;
$SettingNameArr[] = 'CanUpdatePassword_'.$thisUserType;
$SettingNameArr[] = 'EnablePasswordPolicy_'.$thisUserType;
$SettingNameArr[] = 'RequirePasswordPeriod_'.$thisUserType;
$SettingArr = $luser->RetrieveUserInfoSettingInArrary($SettingNameArr);
$PasswordLength = $SettingArr['EnablePasswordPolicy_'.$thisUserType];
if ($PasswordLength<6) $PasswordLength = 6;


$engname = intranet_htmlspecialchars(stripslashes($engname));
$chiname = intranet_htmlspecialchars(stripslashes($chiname));

## check User Login duplication ###
if($userlogin != ""){
	$sql = "SELECT UserID FROM INTRANET_USER WHERE UserLogin='$userlogin'";
	$temp = $laccount->returnVector($sql);
	if(sizeof($temp)!=0){
		$error_msg[] = $userlogin;
	}
}

if($sys_custom['UseStrongPassword']){
	$check_password_result = $lauth->CheckPasswordCriteria($pwd,$userlogin,$PasswordLength);
	if(!in_array(1,$check_password_result)){
		$error_msg[] = $pwd;
	}
}

## check Smart Card ID duplication ###
if($smartcardid!=""){
	$sql = "SELECT UserID FROM INTRANET_USER WHERE CardID='$smartcardid'";
	if($sys_custom['SupplementarySmartCard']){
		$sql.=" OR CardID2='$smartcardid' OR CardID3='$smartcardid' OR CardID4='$smartcardid'";
	}
	$temp = $laccount->returnVector($sql);
	if(sizeof($temp)!=0){
		$error_msg[] = $smartcardid;
	}
}
if($sys_custom['SupplementarySmartCard'] && $smartcardid2!=""){
	$sql = "SELECT UserID FROM INTRANET_USER WHERE CardID='$smartcardid2' OR CardID2='$smartcardid2' OR CardID3='$smartcardid2' OR CardID4='$smartcardid2'";
	$temp = $laccount->returnVector($sql);
	if(sizeof($temp)!=0){
		$error_msg[] = $smartcardid2;
	}
}
if($sys_custom['SupplementarySmartCard'] && $smartcardid3!=""){
	$sql = "SELECT UserID FROM INTRANET_USER WHERE CardID='$smartcardid3' OR CardID2='$smartcardid3' OR CardID3='$smartcardid3' OR CardID4='$smartcardid3'";
	$temp = $laccount->returnVector($sql);
	if(sizeof($temp)!=0){
		$error_msg[] = $smartcardid3;
	}
}
if($sys_custom['SupplementarySmartCard'] && $smartcardid4!=""){
	$sql = "SELECT UserID FROM INTRANET_USER WHERE CardID='$smartcardid4' OR CardID2='$smartcardid4' OR CardID3='$smartcardid4' OR CardID4='$smartcardid4'";
	$temp = $laccount->returnVector($sql);
	if(sizeof($temp)!=0){
		$error_msg[] = $smartcardid4;
	}
}

## check STRN duplication ###
if($strn!=""){
	$sql = "SELECT UserID FROM INTRANET_USER WHERE STRN='$strn'";
	$temp = $laccount->returnVector($sql);
	if(sizeof($temp)!=0){
		$error_msg[] = $strn;
	}
}

## check WebSAMSRegNo duplication ###
if($WebSAMSRegNo!=""){
	if(substr(trim($WebSAMSRegNo),0,1)!="#")
		$WebSAMSRegNo = "#".$WebSAMSRegNo;
		
	$sql = "SELECT UserID FROM INTRANET_USER WHERE WebSAMSRegNo='$WebSAMSRegNo' AND RecordType=".TYPE_STUDENT;
	$temp = $laccount->returnVector($sql);
	
	if(sizeof($temp)!=0){
		$error_msg[] = $WebSAMSRegNo;
	}
}

## check JUPAS APPLICATION NO duplication ###
if($HKJApplNo!="" && $HKJApplNo != 0){		
	$sql = "SELECT UserID FROM INTRANET_USER WHERE HKJApplNo='$HKJApplNo' AND RecordType=".TYPE_STUDENT;
	$temp = $laccount->returnVector($sql);
	if(sizeof($temp)!=0){
		$error_msg[] = $HKJApplNo;
	}
}

## check HKID duplication ###
if($hkid!=""){
	$sql = "SELECT UserID FROM INTRANET_USER WHERE HKID='$hkid'";
	$temp = $laccount->returnVector($sql);
	if(sizeof($temp)!=0){
		$error_msg[] = $hkid;
	}
}

## check Barcode duplication ###
if($barcode != ""){
	$sql = "SELECT UserID FROM INTRANET_USER WHERE Barcode='$barcode'";
	$temp = $laccount->returnVector($sql);
	if(sizeof($temp)!=0){
		$error_msg[] = $barcode;
	}
}

if(sizeof($error_msg)>0) {
	$action = "new.php";
	$errorList = implode(',', $error_msg);	
}


if(sizeof($error_msg)>0) {
//     debug_pr($PrimarySchoolCode);
//     die();
?>

	<body onload="document.form1.submit()">
	<form name="form1" method="post" action="new.php">
		<input type="hidden" name="targetClass" id="targetClass" value="<?=$targetClass?>">
		<input type="hidden" name="recordstatus" id="recordstatus" value="<?=$recordstatus?>">
		<input type="hidden" name="keyword" id="keyword" value="<?=$keyword?>">
		<input type="hidden" name="userlogin" id="userlogin" value="<?=$userlogin?>">
		<input type="hidden" name="pwd" id="pwd" value="<?=$pwd?>">
		<input type="hidden" name="email" id="email" value="<?=$email?>">
		<input type="hidden" name="status" id="status" value="<?=$status?>">
		<input type="hidden" name="smartcardid" id="smartcardid" value="<?=$smartcardid?>">
	<?php if($sys_custom['SupplementarySmartCard']){ ?>
		<input type="hidden" name="smartcardid2" id="smartcardid2" value="<?=$smartcardid2?>">
		<input type="hidden" name="smartcardid3" id="smartcardid3" value="<?=$smartcardid3?>">
		<input type="hidden" name="smartcardid4" id="smartcardid4" value="<?=$smartcardid4?>">
	<?php } ?>
		<?php if($plugin['medical']){ ?>	
				<input type="hidden" name="stayOverNight" id="stayOverNight" value="<?=$stayOverNight?>">
	<?php } ?>
		<input type="hidden" name="strn" id="strn" value="<?=$strn?>">
		<input type="hidden" name="WebSAMSRegNo" id="WebSAMSRegNo" value="<?=$WebSAMSRegNo?>">
		<input type="hidden" name="HKJApplNo" id="WebSAMSRegNo" value="<?=$HKJApplNo?>">
		<input type="hidden" name="barcode" id="barcode" value="<?=$barcode?>">
		<input type="hidden" name="engname" id="engname" value="<?=$engname?>">
		<input type="hidden" name="chiname" id="chiname" value="<?=$chiname?>">
		<input type="hidden" name="nickname" id="nickname" value="<?=$nickname?>">
		<input type="hidden" name="dob" id="dob" value="<?=$dob?>">
		<input type="hidden" name="gender" id="gender" value="<?=$gender?>">
		<input type="hidden" name="hkid" id="hkid" value="<?=$hkid?>">
		<input type="hidden" name="address" id="address" value="<?=$address?>">
		<input type="hidden" name="country" id="country" value="<?=$country?>">
		<input type="hidden" name="homePhone" id="homePhone" value="<?=$homePhone?>">
		<input type="hidden" name="mobilePhone" id="mobilePhone" value="<?=$mobilePhone?>">
		<input type="hidden" name="faxPhone" id="faxPhone" value="<?=$faxPhone?>">
		<input type="hidden" name="open_webmail" id="open_webmail" value="<?=$open_webmail?>">
		<input type="hidden" name="open_file" id="open_file" value="<?=$open_file?>">
		<input type="hidden" name="error" id="error" value="<?=$errorList?>">
		<input type="hidden" name="remark" id="remark" value="<?=$remark?>">
	<?php if($sys_custom['StudentAccountAdditionalFields']){ 
		if($magic_quotes_enabled){
			$PrimarySchool = stripslashes($PrimarySchool);
			$University = stripslashes($University);
			$Programme = stripslashes($Programme);
		}
	?>	
		<input type="hidden" name="NonChineseSpeaking" id="NonChineseSpeaking" value="<?=$NonChineseSpeaking?>" />
		<input type="hidden" name="SpecialEducationNeeds" id="SpecialEducationNeeds" value="<?=$SpecialEducationNeeds?>" />
		<input type="hidden" name="PrimarySchool" id="PrimarySchool" value="<?=intranet_htmlspecialchars($PrimarySchool)?>" />
		<input type="hidden" name="University" id="University" value="<?=intranet_htmlspecialchars($University)?>" />
		<input type="hidden" name="Programme" id="Programme" value="<?=intranet_htmlspecialchars($Programme)?>" />
	<?php } ?>
	
	<?php if($plugin['StudentDataAnalysisSystem_Style'] == "tungwah"){ ?>
	    <input type="hidden" name="PrimarySchoolCode" id="PrimarySchoolCode" value="<?=$PrimarySchoolCode?>" /> 
	<? }?>
	
	
	<?php if($sys_custom['BIBA_AccountMgmtCust']){ ?>
		<input type="hidden" name="HouseholdRegister" id="HouseholdRegister" value="<?=intranet_htmlspecialchars($HouseholdRegister)?>" />
	<?php } ?>
		<input type="hidden" name="Nationality" id="Nationality" value="<?=$Nationality?>">
		<input type="hidden" name="PlaceOfBirth" id="PlaceOfBirth" value="<?=$PlaceOfBirth?>">
		<input type="hidden" name="AdmissionDate" id="AdmissionDate" value="<?=$AdmissionDate?>">
		<input type="hidden" name="GraduationDate" id="GraduationDate" value="<?=$GraduationDate?>">
		<?php if($plugin['radius_server']){ ?>
		<input type="hidden" name="enable_wifi_access" id="enable_wifi_access" value="<?=$enable_wifi_access?>" />
		<?php } ?>
		<?php if(isset($_SESSION['ncs_role'])&&$_SESSION['ncs_role']!=""){?>
    		<input type="hidden" name="staffInCharge" id="staffInCharge" value="<?=$staffInCharge?>" />
    		<input type="hidden" name="hkDuration" id="hkDuration" value="<?=$hkDuration?>" />
    		<input type="hidden" name="b5Priority" id="b5Priority" value="<?=$b5Priority?>" />
    		<input type="hidden" name="enPriority" id="enPriority" value="<?=$enPriority?>" />
    		<input type="hidden" name="hinduPriority" id="hinduPriority" value="<?=$hinduPriority?>" />
    		<input type="hidden" name="OthersPriority" id="OthersPriority" value="<?=$OthersPriority?>" />
    		<?php 
    		if(!empty($LangPriority)){
	    	    foreach($LangPriority as $priority){
	    	        echo '<input type="hidden" name="LangPriority[]" value="'.$priority.'">';
	    	    }
    		} 
    	    ?>
    		<input type="hidden" name="OthersPriority_details" id="OthersPriority_details" value="<?=$OthersPriority_details?>" />
    		<input type="hidden" name="isNCS" id="isNCS" value="<?=$isNCS?>" />
    		<input type="hidden" name="NGO" id="NGO" value="<?=$NGO?>" />
		<?php }?>
		<?php if($plugin['SDAS_module']['KISMode']){?>
			<input type="hidden" name="KISClassType" id="KISClassType" value=<?=$KISClassType?> />
		<?php }?>
	</form>
	<script language="javascript">
		document.form1.submit();
	</script>
	</body>
<? 
	exit;
	
} else {
	
	$domain_name = ($intranet_email_generation_domain==""? $_SERVER["SERVER_NAME"]:$intranet_email_generation_domain);
	$target_email = ($email=="" ? $userlogin . "@".$domain_name : $email);
	
	if($li->UserNewAdd($userlogin, $target_email, trim($engname), trim($chiname), $status)==1) {
		# create & update info in INTRANET_USER
		
		$uid = $li->db_insert_id();
		$UserPassword = $pwd;
		$CardID = trim($smartcardid);
		if($sys_custom['SupplementarySmartCard']){
			$CardID2 = trim($smartcardid2);
			$CardID3 = trim($smartcardid3);
			$CardID4 = trim($smartcardid4);
		}
		if($plugin['medical']){
			$stayOverNight = trim($stayOverNight);
		}
		if($plugin['SDAS_module']['KISMode']){
			$KISClassType = trim($KISClassType);
		}
		$STRN = trim($strn);
		$WebSAMSRegNo = trim($WebSAMSRegNo);
		$NickName = intranet_htmlspecialchars(trim($nickname));
		$DateOfBirth = trim($dob);
		$Gender = trim($gender);
		$HKID = trim($hkid);
		$Address = intranet_htmlspecialchars(trim($address));
		$Country = trim($country);
		$HomeTelNo = intranet_htmlspecialchars(trim($homePhone));
		$MobileTelNo = intranet_htmlspecialchars(trim($mobilePhone));
		$FaxNo = intranet_htmlspecialchars(trim($faxPhone));
		$Remark = intranet_htmlspecialchars(trim($remark));
		$Nationality_ = intranet_htmlspecialchars(trim($Nationality));
		$Place_OfBirth = intranet_htmlspecialchars(trim($PlaceOfBirth));
		$Admission_Date = intranet_htmlspecialchars(trim($AdmissionDate));
		$Graduation_Date = intranet_htmlspecialchars(trim($GraduationDate));
		
		if ($intranet_authentication_method=="HASH") {
		    $fieldname .= "UserPassword = NULL,HashedPass = MD5('$userlogin$UserPassword$intranet_password_salt'),";
		} else {
		    $fieldname .= "UserPassword = NULL, ";
		}
        $fieldname .= "NickName = '$NickName', ";
        $fieldname .= "Title = '$Title', ";
        $fieldname .= "Teaching = '$Teaching', ";
        $fieldname .= "Gender = '$Gender', ";
        $fieldname .= "DateOfBirth = '$DateOfBirth', ";
        $fieldname .= "HomeTelNo = '$HomeTelNo', ";
        $fieldname .= "MobileTelNo = '$MobileTelNo', ";
        $fieldname .= "FaxNo = '$FaxNo', ";
        $fieldname .= "Address = '$Address', ";
        $fieldname .= "Country = '$Country' ";
        $fieldname .= ",CardID = '$CardID'";
        if($sys_custom['SupplementarySmartCard']){
        	$fieldname .= ",CardID2 = '$CardID2'";
        	$fieldname .= ",CardID3 = '$CardID3'";
        	$fieldname .= ",CardID4 = '$CardID4'";
        }
        $fieldname .= ",WebSAMSRegNo = '$WebSAMSRegNo'";
        $fieldname .= ",HKJApplNo = '$HKJApplNo'";
        $fieldname .= ",Barcode = '$barcode'";
        if($special_feature['ava_hkid']) {
	        $fieldname .= ",HKID = '$HKID'";
        }
        
        if($special_feature['ava_strn']) {
	        $fieldname .= ",STRN = '$STRN'";
        }
        
		$fieldname .= ",RecordType = '".TYPE_STUDENT."'";
	
		if ((isset($module_version['StaffAttendance'])&&$module_version['StaffAttendance']!="" ) || (isset($plugin['payment'])&& $plugin['payment'])) {
			$fieldname .= ",CardID = '$smartcardid'";
		}
		$fieldname .= ", DateModified = now()";
		 $fieldname .= ", Remark = '$Remark' ";
		
		if($sys_custom['StudentAccountAdditionalFields']){
			$PrimarySchool = trim($PrimarySchool);
			if(!$magic_quotes_enabled){
				$PrimarySchool = addslashes($PrimarySchool);
			}
			$University = trim($University);
			if(!$magic_quotes_enabled){
				$University = addslashes($University);
			}
			$Programme = trim($Programme);
			if(!$magic_quotes_enabled){
				$Programme = addslashes($Programme);
			}
			
			$fieldname .= ",NonChineseSpeaking='$NonChineseSpeaking' ";
			$fieldname .= ",SpecialEducationNeeds='$SpecialEducationNeeds' ";
			$fieldname .= ",PrimarySchool='".$PrimarySchool."' ";
			$fieldname .= ",University='".$University."' ";
			$fieldname .= ",Programme='".$Programme."' ";
		}
		
		
	    if($plugin['StudentDataAnalysisSystem_Style'] == "tungwah"){ 	      
            $fieldname .= ",PrimarySchoolCode ='".$PrimarySchoolCode."' ";        
	    }
	    
		if($sys_custom['BIBA_AccountMgmtCust']){
			$HouseholdRegister = trim($HouseholdRegister);
			if(!$magic_quotes_enabled){
				$HouseholdRegister = addslashes($HouseholdRegister);
			}
			$fieldname .= ",HouseholdRegister='".$HouseholdRegister."' ";
		}
		
		$sql = "UPDATE INTRANET_USER SET $fieldname WHERE UserID = '$uid'";

		//echo $sql;
		$li->db_db_query($sql);
		
		# Log down two-way hashed/encrypted password
		$lauth->UpdateEncryptedPassword($uid, $UserPassword);
		
		

		# GSuite
		
		//google api
//		error_reporting(E_ALL);
//		ini_set('display_errors', 1);
		if($ssoservice["Google"]["Valid"] && $ssoservice["Google"]['mode']['student']){
			include_once($PATH_WRT_ROOT."includes/google_api/libgoogleapi.php");
			include_once($PATH_WRT_ROOT."includes/sso/libSSO_db.php");
			include_once($PATH_WRT_ROOT."includes/sso/libGoogleSSO.php");
			$libGoogleSSO = new libGoogleSSO();
			
			//prepare password
			$password_for_google_account_creation = $pwd;
			
			$gmail_input_array = array();
			$array_config_index = $libGoogleSSO->getAllGoogleAPIConfigIndex();
			foreach((array)$array_config_index as $config_index){
				$gmail_array_index = 'gmail_' . $config_index;
				$gmail_input_array[$gmail_array_index] = $$gmail_array_index;
			}
			
			$google_have_error=false;
			$array_error_message = $libGoogleSSO->syncUserFromEClassToGoogle($uid, $userlogin, $status, $engname, $password_for_google_account_creation, $gmail_input_array, $chiname);

			foreach((array)$array_error_message as $config_index => $error_message){
		
				if($error_message==''){
					if($google_account_exists_during_creation){
						$_SESSION["SSO"]["Google"]["Message"]['Config'][$config_index] = "ACCOUNT_CREATED";
					}else{
						$_SESSION["SSO"]["Google"]["Message"]['Config'][$config_index] = "NO_MESSAGE";
					}
				}else{
					$google_have_error = true;
					$_SESSION["SSO"]["Google"]["Message"]['Config'][$config_index] = $error_message;
				}
			}
		}
		
		# insert into INTRANET_USERGROUP (student = 2)
		$gpID = 2;
		$lgroup = new libgroup($gpID);
		$defaultRoleID = $lgroup->returnGroupDefaultRole($gpID);
		$sql = "INSERT INTO INTRANET_USERGROUP (GroupID, UserID, RoleID, DateInput, DateModified) VALUES ('$gpID', '$uid', '$defaultRoleID', now(), now())";

		$li->db_db_query($sql);
					
		$msg = 1;
		
		# insert MyCalendar
		$lcalendar->insertMyCalendar($uid);
		
		# insert group calendar
		$cal_sql = "
				insert into CALENDAR_CALENDAR_VIEWER 
				(CalID, UserID, GroupID, GroupType, Access, Color, Visible)
				select g.CalID, '$uid', g.GroupID, 'E', 'R', '2f75e9', '1' 
				from INTRANET_GROUP as g inner join INTRANET_USERGROUP as u 
				on g.GroupID = u.GroupID and u.UserID = '$uid'
			"; 
			//echo $cal_sql.'<br><br>';
			$li->db_db_query($cal_sql);
		
		# insert calendar viewer to calendars that have shared to the user's groups
		$cal_group_sql = "SELECT g.GroupID FROM INTRANET_GROUP as g 
							INNER JOIN INTRANET_USERGROUP as u ON u.GroupID=g.GroupID 
							WHERE u.UserID='$uid'";
		$cal_group_ary = $li->returnVector($cal_group_sql);
		for($i=0;$i<sizeof($cal_group_ary);$i++) {
			$lcalendar->addCalendarViewerToGroup($cal_group_ary[$i],$uid);
		}
			
		#### Special handling
		$li->UpdateRole_UserGroup();

		# modify Group
		$gpID = 2;
		//$sql = "DELETE FROM INTRANET_USERGROUP WHERE UserID=$uid AND GroupID!=$gpID";
		//$laccount->db_db_query($sql);
		# add group member
		for($i=0; $i<sizeof($GroupID); $i++){
			if($GroupID[$i] != "") {
				$lgroup = new libgroup($GroupID[$i]);
				$defaultRoleID = $lgroup->returnGroupDefaultRole($GroupID[$i]);
				$sql = "INSERT INTO INTRANET_USERGROUP (GroupID, UserID, RoleID, DateInput, DateModified) VALUES ('".$GroupID[$i]."', '$uid', '$defaultRoleID', now(), now())";
				$laccount->db_db_query($sql);
			}
		}
	
		if($ssoservice["Google"]["Valid"]  && $ssoservice["Google"]['mode']['student'] && $ssoservice["Google"]['mode']['user_and_group']){
			include_once($PATH_WRT_ROOT."includes/google_api/libgoogleapi.php");
			include_once($PATH_WRT_ROOT."includes/sso/libSSO_db.php");
			include_once($PATH_WRT_ROOT."includes/sso/libGoogleSSO.php");
			$libGoogleSSO = new libGoogleSSO();

			$libGoogleSSO->syncGroupForUserFromEClassToGoogle($laccount, $userlogin, $gmail_input_array, $GroupID);
		}
		
		# Assign to Role
		# remove existing role
		//$sql = "DELETE FROM ROLE_MEMBER WHERE UserID=$uid";
		//$laccount->db_db_query($sql);
	
		# add role
		$identityType = "Student";
		for($i=0; $i<sizeof($RoleID); $i++){
			if($RoleID[$i] != "" && $RoleID[$i]!=0) {
				$sql = "INSERT INTO ROLE_MEMBER (RoleID, UserID, DateInput, InputBy, DateModified, ModifyBy, IdentityType) VALUES ('".$RoleID[$i]."', '$uid', now(), '$UserID', now(), '$UserID', '$identityType')";
				$laccount->db_db_query($sql);
			}
		}
		
		#### Special handling
		//$lireg = new libregistry();
		$li->UpdateRole_UserGroup();
		
		$acl_field = 0;
		
		# iFolder
		if ($open_file && in_array(TYPE_STUDENT, $personalfile_identity_allowed) )
		{
		    if ($personalfile_type=='FTP')
		    {
		        $lftp = new libftp();
		        if ($lftp->isAccountManageable)
		        {
		            $file_failed = ($lftp->open_account($userlogin,$UserPassword)? 0:1);
		            $file_content = get_file_content($intranet_root."/file/account_file_quota.txt");
		            if ($file_content == "")
		            {
		                $userquota = array(10,10,10);
		            }
		            else
		            {
		                $userquota = explode("\n", $file_content);
		            }
		            $target_quota = $userquota[TYPE_STUDENT-1];
		            $lftp->setTotalQuota($userlogin, $target_quota, "iFolder");
		        }
		    }
		    $acl_field += 2;
		}
		
		# gamma mail
		if($plugin['imail_gamma'])
		{
			if($EmailStatus=='enable')
			{
				$IMapEmail = trim($userlogin)."@".$SYS_CONFIG['Mail']['UserNameSubfix'];
				$IMap = new imap_gamma(1);
				if($IMap->open_account($IMapEmail,$pwd))
				{
					$laccount->setIMapUserEmail($uid,$IMapEmail);
					$IMap->SetTotalQuota($IMapEmail,$Quota,$uid);
					
					// Block receive/send internet mails when [iMail Settings > Default Usage Rights and Quota > Internal mail only] is on
					$internal_only = $IMap->internal_mail_only; // index 0 is staff, 1 is student, 2 is parent, 3 is alumni
					if($internal_only[USERTYPE_STUDENT-1]){ // shift index by one
						$IMap->addGroupBlockExternal(array($IMapEmail));
						//$sql = "UPDATE INTRANET_SYSTEM_ACCESS SET ACL = ACL - 1 WHERE UserID = '$uid' AND ACL IN (1,3)";
				    	//$li->db_db_query($sql);
				    	$acl_field -= 1; // offset the following $acl_field += 1
					}
				}
			}
			$acl_field += 1;
		}
		else
		{
		
			# Webmail
			if ($open_webmail && in_array(TYPE_STUDENT, $webmail_identity_allowed))
			{
				
				if ($lwebmail->has_webmail) {
					$lwebmail->open_account($userlogin, $UserPassword);
					$acl_field += 1;
					$file_content = get_file_content($intranet_root."/file/account_mail_quota.txt");
					if ($file_content == "") {
						$userquota = array(10,10,10);
					}
					else {
						$userquota = explode("\n", $file_content);
					}
					$target_quota = $userquota[TYPE_STUDENT-1];
					$lwebmail->setTotalQuota($userlogin, $target_quota, "iMail");
				}
			}
						
			# iMail Quota
			if ($special_feature['imail'])
			{
			    # Get Default Quota
			    $file_content = get_file_content($intranet_root."/file/campusmail_set.txt");
			    if ($file_content == "")
			    {
			        $quota = 10;
			    }
			    else
			    {
			        $userquota = explode("\n", $file_content);
		            $quotaline = $userquota[TYPE_STUDENT-1];
			        $temp = explode(":",$quotaline);
			        $quota = $temp[1];
			    }
			    $sql = "INSERT INTO INTRANET_CAMPUSMAIL_USERQUOTA (UserID, Quota) VALUES ('$uid', '$quota')";
			    $li->db_db_query($sql);
			    
			    $sql = "INSERT INTO INTRANET_CAMPUSMAIL_USED_STORAGE (UserID, QuotaUsed) VALUES ('$uid', 0)";
			    $li->db_db_query($sql);
			}
		}
		
		$sql = "INSERT INTO INTRANET_SYSTEM_ACCESS (UserID, ACL) VALUES ($uid, $acl_field)";
		$li->db_db_query($sql);
		
		$laccount->suspendEmailFunctions($uid, $status == 1);
		
		# Call sendmail function (Email notification of registration)
// 		$mailSubject = registration_title();
// 		$mailBody = registration_body_new($email, $userlogin, $UserPassword, stripslashes($engname), stripslashes($chiname));
		list($mailSubject,$mailBody) = $laccount->returnEmailNotificationData($email, $userlogin, $UserPassword, stripslashes($engname), stripslashes($chiname));
// 		$mailTo = $uid;
// 		$lwebmail->sendModuleMail((array)$mailTo,$mailSubject,$mailBody);
		include_once($PATH_WRT_ROOT."includes/libsendmail.php");
		$lsendmail = new libsendmail();
		$webmaster = get_webmaster();
		$headers = "From: $webmaster\r\n";
		if(!$sys_custom['DisableEmailAfterCreatedNewAccount'] && $email != "" && intranet_validateEmail($email,true)){
			$lsendmail->SendMail($email, $mailSubject, $mailBody,"$headers");
		}
		# LDAP
		if ($intranet_authentication_method=='LDAP')
		{
		    $lldap = new libldap();
		    if ($lldap->isAccountControlNeeded()) {
		        $lldap->connect();
		        $lldap->openAccount($userlogin, $UserPassword);
		    }
		}
		
		# Payment
		if($plugin['payment']) {
			$default_balance = $_SESSION["platform"]=="KIS" && $sys_custom['ePayment']['KIS_NoBalance'] ? "9999999" : "0";
		    $sql = "INSERT INTO PAYMENT_ACCOUNT (StudentID, Balance) VALUES ('$uid',$default_balance)";
		    $li->db_db_query($sql);
		}
		
		# Activate IPF for KIS students
		if(
		    $plugin['iPortfolio']&&
		    (
    		    $_SESSION['platform']=='KIS' ||
    		    $sys_custom['iPortfolio_auto_active_student']
		    )
        ) {
			include_once($PATH_WRT_ROOT.'includes/libpf-sturec.php');
			#Siuwan 20130828 Set $ck_course_id before init $lpf_sturec
			$sql = "Select course_id From $eclass_db.course Where RoomType = 4";
			$ary = $li->returnVector($sql);
			$ck_course_id = $ary[0];
			$lpf_sturec = new libpf_sturec();
		    $sql = "Update INTRANET_USER Set WebSAMSRegNo = concat('#', UserLogin) Where UserID ='$uid' AND (WebSAMSRegNo IS NULL OR WebSAMSRegNo = '')";
			$li->db_db_query($sql);
			$lpf_sturec->LOAD_KEYS_FROM_SETTING();
			$lpf_sturec->DO_ACTIVATE_STUDENT($uid);
		}		
		
		# SchoolNet integration
		if ($plugin['SchoolNet'])
		{
		    include_once($PATH_WRT_ROOT."includes/libschoolnet.php");
		    $lschoolnet = new libschoolnet();
	        # Param: array in ($userlogin, $password, $DOB, $gender, $cname, $ename, $tel, $mobile, $address, $email, $teaching)
	        $sql = "SELECT UserLogin, UserPassword, DATE_FORMAT(DateOfBirth,'%Y-%m-%d'), Gender, Chinesename, EnglishName, HomeTelNo, MobileTelNo, Address, UserEmail, Teaching FROM INTRANET_USER WHERE UserID = '".$uid."'";
	        $data = $li->returnArray($sql, 11);
	        $lschoolnet->addStaffUser($data);
		}	
		
		if($sys_custom['UserExtraInformation'] )
		{
			if(!isset($ExtraInfo2)) {
				$ExtraInfoCatArr = $laccount->Get_User_Extra_Info_Category("", " AND eic.RecordType=2");
				$noOfSingleSelectItem = count($ExtraInfoCatArr);
				if($noOfSingleSelectItem>0) {
					for($i=0; $i<$noOfSingleSelectItem; $i++) {
						if(${'ExtraInfo2_'.$ExtraInfoCatArr[$i]['CategoryID']}[0] != 0) {
							$ExtraInfo2[] .= ${'ExtraInfo2_'.$ExtraInfoCatArr[$i]['CategoryID']}[0];
						}
					}
				}
			}
				
				
			if(count($ExtraInfo)==0)
				$ExtraInfo = array_merge((array)$ExtraInfo1,(array)$ExtraInfo2);
			
			$ExtraInfoItemArr = $laccount->Get_User_Extra_Info_Item();
			$ExtraInfoOthersArr = BuildMultiKeyAssoc($ExtraInfoItemArr, "ItemID", "ItemCode", 1);
			if(count($ExtraInfoOthersArr)>0) {
				foreach($ExtraInfoOthersArr as $item_id=>$item_code) {
					if(${'ExtraOthers_'.$item_id}!='') 
						$ExtraOthers[$item_id] = ${'ExtraOthers_'.$item_id};
				}
			}
			
			if(count($ExtraInfo)>0)
				$laccount->Insert_User_Extra_Info_Mapping($uid,$ExtraInfo,$ExtraOthers);
		}
		
		if($sys_custom['StudentMgmt']['BlissWisdom']) {
			include_once($PATH_WRT_ROOT."includes/libuserpersonalsettings.php");
			$libps = new libuserpersonalsettings();
			
			if(is_numeric($HowToKnowBlissWisdom)) {
				if($HowToKnowBlissWisdom==1) {
					$HowToKnowBlissWisdom .= "::".$FriendName;	
				} else if($HowToKnowBlissWisdom==6) {
					$HowToKnowBlissWisdom.= "::".$HowToKnowOthersField;	
				}
			}
			
			$FieldAry = array('Occupation'=>$Occupation, 'Company'=>$Company, 'PassportNo'=>$PassportNo, 'Passport_ValidDate'=>$Passport_ValidDate, 'HowToKnowBlissWisdom'=>$HowToKnowBlissWisdom);
			
			$libps->Save_Setting($uid, $FieldAry);
			
		}
		
		#Set Personal Information
		$dataAry_PersonalSetting = array();
		$dataAry_PersonalSetting['Nationality'] = $Nationality_;
		$dataAry_PersonalSetting['PlaceOfBirth'] = $Place_OfBirth;
		$dataAry_PersonalSetting['AdmissionDate'] = $Admission_Date;
		$dataAry_PersonalSetting['GraduationDate'] = $Graduation_Date;
		if($plugin['medical']){
			$dataAry_PersonalSetting['stayOverNight'] = $stayOverNight;
		}
		if($plugin['SDAS_module']['KISMode']){
			$dataAry_PersonalSetting['KISClassType'] = $KISClassType;
		}

		$success_ps = $laccount->updateUserPersonalSetting($uid, $dataAry_PersonalSetting);

		#Set Personal Information
		if(isset($_SESSION['ncs_role'])&&$_SESSION['ncs_role']!=""){
			$dataAry_NCSSettings = array();
			$dataAry_NCSSettings['StaffInCharge'] = $staffInCharge;
			$dataAry_NCSSettings['DurationInHK'] = $hkDuration;
			if(!empty($LangPriority)){
				$dataAry_NCSSettings['LanguagePriority']=implode(", ",$LangPriority);
			}
    		if($OthersPriority){
    		    $dataAry_NCSSettings['LanguagePriority'] .= ', Others: ';
    		    $dataAry_NCSSettings['LanguagePriority'] .= trim($OthersPriority_details);
    		    $dataAry_NCSSettings['LanguagePriority'] = trim($dataAry_NCSSettings['LanguagePriority'],', ');
    		}
			$dataAry_NCSSettings['IsNCS'] = $isNCS;
			$dataAry_NCSSettings['NGO'] = $NGO;
		
			$success_ncs = $laccount->updateUserNCSSetting($uid, $dataAry_NCSSettings);
		}
		
		# Create guardians
		
		$ids = explode(",",$newids);
		
		for($i=0;$i<sizeof($ids);$i++){
			$id = $ids[$i];
			if($id!="") {
				$ch_name = ${'chname_new_'.$id};
				$en_name = ${'enname_new_'.$id};
				$phone   = ${'phone_new_'.$id};
				$emphone = ${'emphone_new_'.$id};
				
				$add_area = '';
				$add_road = '';
				$add_address = '';
				$address = ${'address_new_'.$id};
				
				$relation= ${'relation_new_'.$id};
				
				$new_str ="new_$id";
				/*
				$IsMain = (int)(is_array($main) && in_array($new_str,$main));
				$IsSMS = (int) ($plugin['sms'] && is_array($sms) && in_array($new_str,$sms));
				$IsEmergencyContact = (int)(is_array($ec) && in_array($new_str,$ec));
				*/
				$IsMain = ($mainGuardianFlag==$id) ? 1 : 0;
				$IsSMS = ($smsFlag==$id) ? 1 : 0;
				$IsEmergencyContact = ($emergencyContactFlag==$id) ? 1 : 0;
				
				
				
				$occupation = ${'occupation_new_'.$id};
				
				$IsLiveTogether = (int)${'liveTogether_new_'.$id};
				
				
				$sql = "INSERT INTO $eclass_db.GUARDIAN_STUDENT (
						UserID,ChName,EnName,
						Phone,EmPhone,Address,Relation,
						IsMain,IsSMS,InputDate,ModifiedDate
					) VALUES (
						'$uid','$ch_name','$en_name',
						'$phone','$emphone','$address','$relation',
						'$IsMain', '$IsSMS',NOW(), NOW()
					) ";
				$li->db_db_query($sql);
				$record_id = mysql_insert_id();
				
				//insert the extend record
				$sql = "INSERT INTO $eclass_db.GUARDIAN_STUDENT_EXT_1 (
						UserID,RecordID, IsLiveTogether, IsEmergencyContact, Occupation,
						AreaCode, Road, Address,
						InputDate,ModifiedDate
					) VALUES (
						'$uid','$record_id', '$IsLiveTogether', '$IsEmergencyContact', '$occupation',
						'$add_area','$add_road', '$add_address', 
						NOW(), NOW()
					)";
				$li->db_db_query($sql);
				
				# 2020-03-16 (Philips) - Sync to Student Registry
				if($sys_custom['AccountMgmt']['cdsj_5_macau']['syncAccountAndRegistry'] && $IsMain){
					$guardianAry = array();
					$guardianAry['StudentID'] = $uid;
					$guardianAry['PG_TYPE'] = 'G';
					$guardianAry['GUARD'] = 'O';
					$guardianAry['G_GENDER'] = 'M';
					$guardianAry['NAME_C'] = $ch_name;
					$guardianAry['NAME_E'] = $en_name;
					$guardianAry['G_RELATION'] = $ec_guardian[$relation];
					$guardianAry['TEL'] = $phone;
					$guardianAry['MOBILE'] = $emphone;
					$guardianAry['PROF'] = $occupation;
					$guardianAry['LIVE_SAME'] = $IsLiveTogether;
					$nl = strpos($address, "\n");
					if(is_int($nl)){
						$road = substr($address,0, $nl);
						$gAddress = substr($address, $nl);
					} else {
						$road = $address;
						$gAddress = '';
					}
					$guardianAry['G_ROAD'] = $road;
					$guardianAry['G_ADDRESS'] = $gAddress;
					$laccount->syncToStudentRegistry_GuardianInfo($uid, $guardianAry);
				}
			
			}
			$flag = "AddSuccess";
		}
		
		$successAry['synUserDataToModules'] = $luser->synUserDataToModules($uid);
		
		if($plugin['radius_server']){
			$laccount->disableUserInRadiusServer(array($uid), !$enable_wifi_access);
		}
		
	} else {
		$flag = "AddUnsuccess";	
	}

	intranet_closedb();
	if($ssoservice["Google"]["Valid"] && $ssoservice["Google"]['mode']['student']){
		if($google_have_error){
			header("Location: edit.php?uid=$uid");
			exit();
		}
	}

	header("Location: $comeFrom?xmsg=$flag");
}
?>