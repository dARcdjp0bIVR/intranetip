<?php
# modifying by :   

###
### 		!!!!! Note: Please also upload templates/new.tmpl.php when update this file since 2015-12-17 		!!!!! 
###

############# Change Log
#   Date:	2019-08-08 Paul
#   		handle Google SSO quota checking by try / catch
#
#   Date:   2019-04-30 Cameron
#           fix cross site scripting by applying cleanCrossSiteScriptingCode() and cleanHtmlJavascript() to variables
#
#   Date:   2018-12-31 Isaac
#           Added flag $sys_custom['AccountMgmt']['StudentDOBRequired'] to force DOB field compulsory
#
#   Date:   2018-11-12 Cameron
#           consolidate Amway, Oaks, CEM to use the same customized flag control ($sys_custom['project']['CourseTraining']['IsEnable'])
#
#   Date:   2018-09-19 Anna
#           Added tungwah sdas cust - PrimarySchoolCode
#
#   Date:   2018-09-03 (Bill)   [2017-1207-0956-53277]
#           added Graduation Date
#
#	Date:	2018-01-03 Carlos
#			Improved password checking.
#
#	Date:	2017-12-22 Cameron
#			configure for Oaks refer to Amway
#
#	Date:	2017-01-13 Cameron
#			$ssoservice["Google"]["Valid"] - add GSuite Logic
#
#	Date:	2015-12-16 Cameron
#			move layout to template folder to customize layout [Amway #P89445]
#
#	Date:	2015-03-17 Cameron
#			use fax field for recording date of baptism if $sys_custom['eAdmin']['AcctMgmt']['UseFaxForBaptism'] = true  
#
#	Date:	2015-01-12	Omas 	
#			new js file for delay checking loginID templates/2009a/js/account_mgmt_checking.js
#								
#	Date:	2014-12-10	Bill
#			$sys_custom['SupplementarySmartCard'] - Add SmartCardID4
#
#	Date:	2014-07-17	Bill
#			Add flag $plugin['eEnrollment'] to display CardID textbox for eEnrolment
#
#	Date:	2013-12-12	Fai
#			Add a cust attribute "stayOverNight"
#
#	2013-11-13 (Carlos):  $sys_custom['SupplementarySmartCard'] - Add SmartCardID2 and SmartCardID3
#	2013-08-13 (Carlos): hide WEBSAMS number and JUPAS Application number for KIS
#
#	Date:	2012-08-30	YatWoon
#			add barcode
#
#	2012-06-08 (Carlos): added password remark
#
#	Date :	2011-06-23	(Fai)
#			ADD Application for JUPAS for student account $HKJApplNo
#
#	Date:	2010-11-05	Marcus
#			Add User Extra Info (Shek Wu eRC Rubrics Cust)
#
#	Date:	2010-08-18	YatWoon
#			Add "Remark" field
#
#################

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");


intranet_auth();
intranet_opendb();

if(!($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Student"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"])) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$isKIS = $_SESSION["platform"]=="KIS";
$laccount = new libaccountmgmt();
$linterface = new interface_html();

$lu = new libuser();
$thisUserType = USERTYPE_STUDENT;
$SettingNameArr[] = 'CanUpdatePassword_'.$thisUserType;
$SettingNameArr[] = 'EnablePasswordPolicy_'.$thisUserType;
$SettingNameArr[] = 'RequirePasswordPeriod_'.$thisUserType;
$SettingArr = $lu->RetrieveUserInfoSettingInArrary($SettingNameArr);
$PasswordLength = $SettingArr['EnablePasswordPolicy_'.$thisUserType];
if ($PasswordLength<6 && $PasswordLength>0) $PasswordLength = 6;
if($sys_custom['UseStrongPassword'] && ($PasswordLength == '' || $PasswordLength<6)){
	$PasswordLength = 6;
}

/*
$countrySelection = "<select name='country' id='country'><option value='' ".(($userInfo['Country']=="") ? "selected" : "").">-- $i_alert_pleaseselect $i_UserCountry --</option>";
for($i=0; $i<sizeof($Lang['Country']); $i++) {
	$countrySelection .= "<option value=\"".$Lang['Country'][$i]."\" ".(($Lang['Country'][$i]==$country)?"selected" : "").">".$Lang['Country'][$i]."</option>";	
}
$countrySelection .= "</select>";
*/
$countrySelection = "<input type='text' name='country' id='country' value='$country'>";

$CurrentPageArr['StudentMgmt'] = 1;
$CurrentPage = "Mgmt_Account";

if($plugin['imail_gamma'])
{
	$DefaultQuota = $laccount->getEmailDefaultQuota(TYPE_STUDENT);
	$EmailQuota = $DefaultQuota[1];
}

if($ssoservice["Google"]["Valid"] && $ssoservice["Google"]['mode']['student']){
	include_once($PATH_WRT_ROOT."includes/google_api/libgoogleapi.php");
	include_once($PATH_WRT_ROOT."includes/sso/libSSO_db.php");
	include_once($PATH_WRT_ROOT."includes/sso/libGoogleSSO.php");
	
	$google_apis = libGoogleAPI::getAllGoogleAPIs($ssoservice);
	$lib_google_sso = new libGoogleSSO();
	foreach((array)$google_apis as $google_api){
		$config_index = $google_api->getConfigIndex();
		try{
			$remaining_quota[$config_index] = $lib_google_sso->getRemainingQuota($google_api);
		}catch(Exception $e){
			$remaining_quota[$config_index] = false;
		}
		$total_quota[$config_index] = $ssoservice["Google"]['api'][$config_index]['quota'];
	}
}

if(!isset($comeFrom) || $comeFrom=="")
	$comeFrom = "index.php";

$comeFrom = cleanCrossSiteScriptingCode($comeFrom);
	
# For eRC Rubrics Project (Shek Wu)
if($sys_custom['UserExtraInformation']) {
	$ExtraInfoItemArr = $laccount->Get_User_Extra_Info_Item();
	$ExtraInfoOthersArr = BuildMultiKeyAssoc($ExtraInfoItemArr, "ItemID", "ItemCode", 1);
	if(count($ExtraInfoOthersArr)>0) {
		foreach($ExtraInfoOthersArr as $item_id=>$item_code) {
			if(${'ExtraOthers_'.$item_id}!='') 
				$ExtraOthers[$item_id] = ${'ExtraOthers_'.$item_id};
		}
	}
	
	if($sys_custom['StudentMgmt']['BlissWisdom']) {
		if($HowToKnowBlissWisdom==1) {
			$HowToKnowBlissWisdom .= "::".$FriendName;	
		} else if($HowToKnowBlissWisdom==6) {
			$HowToKnowBlissWisdom.= "::".$HowToKnowOthersField;	
		}
		
		$FieldAry = array('Occupation'=>$Occupation, 'Company'=>$Company, 'PassportNo'=>$PassportNo, 'Passport_ValidDate'=>$Passport_ValidDate, 'HowToKnowBlissWisdom'=>$HowToKnowBlissWisdom);
	}
	
	$UserExtraInfoOption = $laccount->Get_User_Extra_Info_UI('',$ExtraInfo, $ExtraOthers, $FieldAry);
}


if($comeFrom=="notInClass.php") {
	$TAGS_OBJ[] = array($Lang['AccountMgmt']['UserList'],"notInClass.php",0);
	$TAGS_OBJ[] = array($Lang['AccountMgmt']['StudentNotInClass'],"notInClass.php",1);
} else {
	$TAGS_OBJ[] = array($Lang['AccountMgmt']['UserList'],"index.php",1);
	$TAGS_OBJ[] = array($Lang['AccountMgmt']['StudentNotInClass'],"notInClass.php",0);
}
if(isset($_SESSION['ncs_role'])&&$_SESSION['ncs_role']!=""){
	$re = '/([^,]+):([^,]+)/';
	$_LangPriority = (array)$LangPriority;
	foreach($_LangPriority as $langPrio){
    	preg_match_all($re, $langPrio, $priorityOrder, PREG_SET_ORDER, 0);
    	foreach($priorityOrder as $order){
    	    $LangPriority[ trim($order[1]) ] = trim($order[2]);
    	}
	}
}
$MODULE_OBJ['title'] = $Lang['Header']['Menu']['StudentAccount'];

# navigation bar
$PAGE_NAVIGATION[] = array($Lang['AccountMgmt']['UserList'], "javascript:goURL('$comeFrom')");
$PAGE_NAVIGATION[] = array($Lang['AccountMgmt']['NewUser'], "");

# step information
$STEPS_OBJ[] = array($Lang['AccountMgmt']['InputStudentDetails'], 1);
if (!$sys_custom['project']['CourseTraining']['IsEnable']) {
	$STEPS_OBJ[] = array($Lang['AccountMgmt']['InputGuardianDetail'], 0);
}
if(isset($error) && $error!="")
	$errorAry = explode(',', $error);

$MODULE_OBJ = $laccount->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();

if($plugin['StudentDataAnalysisSystem_Style'] == "tungwah"){
    include_once($PATH_WRT_ROOT."includes/cees/libcees.php");
    $lcees = new libcees();
    $SchoolInfoAry= $lcees->getAllSchoolInfoFromCentralServer('P');
    $i = 0;
    $SchoolNameAry[0][] = '000000';
    $SchoolNameAry[0][] = $Lang['General']['NotApplicable'] ;
    foreach((array)$SchoolInfoAry as $SchoolInfo){   
        $i++; 
        $SchoolNameAry[$i][] = $SchoolInfo['SchoolCode'];
        $SchoolNameAry[$i][] = $SchoolInfo['NameEn'];              
    }
}


# check student license quota
$license_student_left = 9999;
if (isset($account_license_student) && $account_license_student>0)
{
	$lclass = new libclass();
	$license_student_used = $lclass->getLicenseUsed();
	$license_student_left = $account_license_student - $license_student_used;
}

if ($license_student_left<=0)
{
	# no quota left
	echo "<table width=560  border=0 cellpadding=0 cellspacing=0 align='center'>
			<tr><td align='center'><br /><br />
			{$i_license_sys_msg_none}
			</td></tr>
		</table>";
	$linterface->LAYOUT_STOP();
	
	die();
}
?>
<script src="<?=$PATH_WRT_ROOT.'/templates/2009a/js/account_mgmt_checking.js'?>" type="text/javascript"></script>
<script language="javascript">

function goSubmit(urlLink) {
	var password_warnings = {};
<?php
	foreach($Lang['AccountMgmt']['PasswordCheckingWarnings'] as $key => $val){
		echo "	password_warnings['".$key."'] = '".addslashes($val)."';\n";
	}
?>	
	var obj = document.form1;
	show_checking('userlogin','spanChecking');
	
	if(obj.userlogin.value=="") {
		alert("<?=$i_alert_pleasefillin." ".$i_UserLogin?>");	
		return false;
	}
	
	if (!check_text(obj.pwd,"<?=$i_alert_pleasefillin." ".$i_UserPassword?>")) return false;
	if (!check_text(obj.pwd2,"<?=$i_alert_pleasefillin." ".$i_frontpage_myinfo_password_retype?>")) return false;
	
	if (obj.pwd.value != obj.pwd2.value)
	{
		alert ("<?=$i_frontpage_myinfo_password_mismatch?>");
		obj.pwd.select();
		return false;
	}
<?php if($sys_custom['UseStrongPassword']){ ?>	
	var check_password_result = CheckPasswordCriteria(obj.pwd.value,obj.userlogin.value,<?=$PasswordLength?>);
	if(check_password_result.indexOf(1) == -1){
		var password_warning_msg = '';
		for(var i=0;i<check_password_result.length;i++){
			password_warning_msg += password_warnings[check_password_result[i]] + "\n";
		}
		alert(password_warning_msg);
		obj.pwd.focus();
		return false;
	}
<?php } ?>	
	if (obj.email) {
		if( obj.email.value != "")
		{
			if(!validateEmail(obj.email, "<?=$i_invalid_email?>")) return false;
		}
	//	else
	//	{
	//		alert("<?=$i_alert_pleasefillin." ".$Lang['Gamma']['UserEmail']?>");
	//		return false;
	//	}
			
	}
	
	if(obj.status[0].checked==false && obj.status[1].checked==false) {
		alert("<?=$i_alert_pleaseselect." ".$i_general_status?>");
		return false;
	}
	<?php if($sys_custom['SupplementarySmartCard']){ ?>
	if(obj.smartcardid && obj.smartcardid2 && obj.smartcardid3 && obj.smartcardid4){
		var smartcardid_ary = [];
		var smartcardid_word = [];
		smartcardid_ary.push(obj.smartcardid.value.Trim());
		smartcardid_ary.push(obj.smartcardid2.value.Trim());
		smartcardid_ary.push(obj.smartcardid3.value.Trim());
		smartcardid_ary.push(obj.smartcardid4.value.Trim());
		smartcardid_word.push('<?=$Lang['AccountMgmt']['SmartCardID']?>');
		smartcardid_word.push('<?=$Lang['AccountMgmt']['SmartCardID2']?>');
		smartcardid_word.push('<?=$Lang['AccountMgmt']['SmartCardID3']?>');
		smartcardid_word.push('<?=$Lang['AccountMgmt']['SmartCardID4']?>');
		
		for(var i=0;i<smartcardid_ary.length;i++){
			for(var j=0;j<smartcardid_ary.length;j++){
				if(i != j){
					if(smartcardid_ary[i]!='' && smartcardid_ary[j]!='' && smartcardid_ary[i] == smartcardid_ary[j]){
						alert(smartcardid_word[j] + "<?=$Lang['AccountMgmt']['IsDuplicatedInput']?>");
						return false;
					}
				}
			}
		}
	}
	<?php } ?>
	if(obj.engname.value=="") {
		alert("<?=$i_alert_pleasefillin." ".$ec_student_word['name_english']?>");	
		return false;		
	}
	<?php if (!$sys_custom['project']['CourseTraining']['IsEnable']): ?>	
	if(obj.dob.value!="") {
		if(!check_date(obj.dob,'<?=$iDiscipline['Invalid_Date_Format']?>')) {
			return false;
		}
	}
	if(obj.AdmissionDate.value!="") {
		if(!check_date(obj.AdmissionDate,'<?=$iDiscipline['Invalid_Date_Format']?>')) {
			return false;
		}
	}
	if(obj.GraduationDate.value!="") {
		if(!check_date(obj.GraduationDate,'<?=$iDiscipline['Invalid_Date_Format']?>')) {
			return false;
		}
	}
<?php endif; ?>
	
	if(obj.gender[0].checked==false && obj.gender[1].checked==false) {
		alert("<?=$i_alert_pleaseselect." ".$i_UserGender?>");
		return false;
	}
	
	if(obj.available.value==0) {
		//alert("<?=$i_UserLogin.$Lang['AccountMgmt']['UsedByOtherUser']?>");
		alert("<?=$i_UserLogin.$Lang['StudentRegistry']['IsInvalid']?>");
		return false;	
	}
	if(obj.available.value==2) {
		alert("<?=$i_UserLogin.$Lang['AccountMgmt']['StartedWithA2Z']?>");
		return false;	
	}
	
<?php
	if ($sys_custom['eAdmin']['AcctMgmt']['UseFaxForBaptism']) {
?>
	if(obj.faxPhone.value!="") {
		if(!check_date(obj.faxPhone,'<?=$iDiscipline['Invalid_Date_Format']?>')) {
			return false;
		}
	}
<?php
	}
?>	
<?php
if($sys_custom['AccountMgmt']['StudentDOBRequired']){
    ?>
	if(obj.dob.value=="" || obj.dob.value==null) {
		alert("<?=$i_alert_pleasefillin." ".$i_UserDateOfBirth?>");	
		return false;
	}
    <?php
}
?>
	obj.action = urlLink;
	obj.submit();
}

function goFinish() {
	document.form1.action = "new_update.php";
	document.form1.submit();	
}

function goURL(urlLink) {
	document.form1.action = urlLink;
	document.form1.submit();
}
</script>
<script language="JavaScript">

var ClickID = '';
var callback_checking = {
	success: function ( o )
    {
	    var tmp_str = o.responseText;
	    document.getElementById('spanChecking').innerHTML = tmp_str;
	    DisplayPosition();
	    $('#spanCheckingImage').hide();
	}
}


function show_checking(type, click)
{
	$('#spanCheckingImage').show();
	ClickID = click;
	document.getElementById('task').value = type;
	obj = document.form1;
	YAHOO.util.Connect.setForm(obj);
	var path = "../ajax_checking.php";
    var connectionObject = YAHOO.util.Connect.asyncRequest("POST", path, callback_checking);
}


function getPosition(obj, direction)
{
	var objStr = "obj";
	var pos_value = 0;
	while (typeof(eval(objStr))!="undefined" && eval(objStr + ".tagName")!="BODY")
	{
		pos_value += eval(objStr + "." + direction);
		objStr += ".offsetParent";
	}

	return pos_value;
}

function DisplayPosition(){
	
  document.getElementById('spanChecking').style.left=getPosition(document.getElementById(ClickID),'offsetLeft') +10;
  document.getElementById('spanChecking').style.top=getPosition(document.getElementById(ClickID),'offsetTop');
  document.getElementById('spanChecking').style.visibility='visible';
}

function updateIMapEmail(obj)
{
	$("span#IMapUserEmail").html(obj.value.Trim()+"@<?=$SYS_CONFIG['Mail']['UserNameSubfix']?>");
}



</script>
<form name="form1" method="post" action="">
<?php
    if ($sys_custom['project']['CourseTraining']['IsEnable']) {
		include("templates/new_amway.tmpl.php");
	}
	else {
		include("templates/new.tmpl.php");
	}
?>
<input type="hidden" name="task" id="task" value="">
<input type="hidden" name="comeFrom" id="comeFrom" value="<?=$comeFrom?>">
<input type="hidden" name="ClickID" id="ClickID" value="">
<input type="hidden" name="targetClass" id="targetClass" value="<?=cleanHtmlJavascript($targetClass)?>">
<input type="hidden" name="recordstatus" id="recordstatus" value="<?=cleanHtmlJavascript($recordstatus)?>">
<input type="hidden" name="keyword" id="keyword" value="<?=cleanHtmlJavascript($keyword)?>">
</form>


	
</script>
<!--script language="javascript" src="multicomplete.js"></script-->
<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>