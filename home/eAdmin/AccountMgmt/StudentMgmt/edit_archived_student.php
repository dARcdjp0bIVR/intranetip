<?php
// Edit by 
/*
 * 2019-10-31 (Henry) : Bug fix for the warning msg of foreach($SchoolInfoAry as $SchoolInfo)
 * 2018-05-16 (Isaac) : Added Archiver username and time.
 * 2017-08-24 ((Simon): Remove the photo height.
 * 2016-06-28 (Carlos): Added. 
 */
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libgrouping.php");
include_once($PATH_WRT_ROOT."includes/librole.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($eclass_filepath."/src/includes/php/lib-portfolio.php");

intranet_auth();
intranet_opendb();

if(isset($_SESSION['FLASH_VAR_ARCHIVED_STUDENT_ERROR']))
{
	$errors = $_SESSION['FLASH_VAR_ARCHIVED_STUDENT_ERROR'];
	unset($_SESSION['FLASH_VAR_ARCHIVED_STUDENT_ERROR']);
}

if(!($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Student"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"])) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

if(!isset($user_id))
{
	intranet_closedb();
	header("Location: archived_student.php");	
	exit;
}

$isKIS = $_SESSION["platform"]=="KIS";

$laccount = new libaccountmgmt();
$linterface = new interface_html();

$uid = IntegerSafe( is_array($user_id)? $user_id[0] : $user_id );
$records = $laccount->returnResultSet("SELECT * FROM INTRANET_ARCHIVE_USER WHERE UserID='$uid'");
$userInfo = $records[0];

if($userInfo['UserID'] != $uid){
	intranet_closedb();
	header("Location: archived_student.php");	
	exit;
}
//debug_pr($errors);
$errorAry = isset($errors) && count($errors)>0? $errors : array();

$userlogin = $userInfo['UserLogin'];
$email = $userInfo['UserEmail'];
//$status = $userInfo['RecordStatus'];
$smartcardid = $userInfo['CardID'];
if($sys_custom['SupplementarySmartCard']){
	$smartcardid2 = $userInfo['CardID2'];
	$smartcardid3 = $userInfo['CardID3'];
	$smartcardid4 = $userInfo['CardID4'];
}
$strn = isset($strn)? $strn : $userInfo['STRN'];
$WebSAMSRegNo = isset($WebSAMSRegNo)? $WebSAMSRegNo : $userInfo['WebSAMSRegNo'];
$HKJApplNo = isset($HKJApplNo)? $HKJApplNo : $userInfo['HKJApplNo'];
$barcode = $userInfo['Barcode'];
$engname = $userInfo['EnglishName'];
$chiname = $userInfo['ChineseName'];
$nickname = $userInfo['NickName'];
$dob = isset($dob)? $dob : substr($userInfo['DateOfBirth'],0,10);
if($dob=="0000-00-00") $dob = "";
$gender = $userInfo['Gender'];
$hkid = isset($hkid)? $hkid : $userInfo['HKID'];
$address = $userInfo['Address'];
$country = $userInfo['Country'];
$homePhone = $userInfo['HomeTelNo'];
$mobilePhone = $userInfo['MobileTelNo'];
$faxPhone = $userInfo['FaxNo'];
//$original_pass = $userInfo['UserPassword'];
$remark = $userInfo['Remark'];
if($sys_custom['StudentAccountAdditionalFields']){
	$NonChineseSpeaking = $userInfo['NonChineseSpeaking'];
	$SpecialEducationNeeds = $userInfo['SpecialEducationNeeds'];
	$PrimarySchool = $userInfo['PrimarySchool'];
	$University = $userInfo['University'];
	$Programme = $userInfo['Programme'];
}
$Nationality = $userPersonalSetting['Nationality'];
$PlaceOfBirth = $userPersonalSetting['PlaceOfBirth'];
$AdmissionDate = $userPersonalSetting['AdmissionDate'];
if($AdmissionDate=="0000-00-00") $AdmissionDate = "";
if($plugin['medical']){
	if($stayOverNight=="") $stayOverNight = $userPersonalSetting['stayOverNight'];
	$stayOverNightIsChecked = ' checked ';
	if($stayOverNight == '' || $stayOverNight == 0){
		$stayOverNightIsChecked = '';
	}
}

########## Official Photo
$photo_link = "/images/myaccount_personalinfo/samplephoto.gif";
if ($userInfo['PhotoLink'] !="")
{
    if (is_file($intranet_root.$userInfo['PhotoLink']))
    {
        $photo_link = $userInfo['PhotoLink'];
    }
}
########## Personal Photo
$photo_personal_link = "/images/myaccount_personalinfo/samplephoto.gif";
if ($userInfo['PersonalPhotoLink'] !="")
{
    if (is_file($intranet_root.$userInfo['PersonalPhotoLink']))
    {
        $photo_personal_link = $userInfo['PersonalPhotoLink'];
   }
}



$CurrentPageArr['StudentMgmt'] = 1;
$CurrentPage = "Mgmt_Account";

$TAGS_OBJ[] = array($Lang['AccountMgmt']['UserList'],"index.php?clearCoo=1",0);
$TAGS_OBJ[] = array($Lang['AccountMgmt']['StudentNotInClass'],"notInClass.php?clearCoo=1",0);
$TAGS_OBJ[] = array($Lang['AccountMgmt']['ArchivedStudent'],"",1);

# navigation bar
$PAGE_NAVIGATION[] = array($Lang['AccountMgmt']['UserList'], "archived_student.php");
$PAGE_NAVIGATION[] = array($Lang['AccountMgmt']['EditUser'], "");

$MODULE_OBJ['title'] = $Lang['Header']['Menu']['StudentAccount'];

$MODULE_OBJ = $laccount->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$xmsg]);

$userObj = new libuser($uid);
$ArchiverUserinfoAry = $userObj->ReturnArchiveUserCreateModifyInfo($uid);
$ArchiverUserObj = new libuser($ArchiverUserinfoAry['ArchivedBy']);
$DateArchive = $ArchiverUserinfoAry['DateArchive'];
if(empty($ArchiverUserObj->UserID)){
    $ArchiverUserinfoAry = $ArchiverUserObj->ReturnArchiveStudentInfo($ArchiverUserinfoAry['ArchivedBy']);
    $ArchiverEnglishName = $ArchiverUserinfoAry['EnglishName'];
    $ArchiverChineseName = $ArchiverUserinfoAry['ChineseName'];
}else{
$ArchiverEnglishName = $ArchiverUserObj->EnglishName;
$ArchiverChineseName = $ArchiverUserObj->ChineseName;
}
if($intranet_session_language=='b5' && $ArchiverChineseName){
    $nameDisplay = $ArchiverChineseName;
} else{
    $nameDisplay = $ArchiverEnglishName;
}



if($plugin['StudentDataAnalysisSystem_Style'] == "tungwah"){
    include_once($PATH_WRT_ROOT."includes/cees/libcees.php");
    include_once($PATH_WRT_ROOT."includes/libpf-exam.php");
    include_once($PATH_WRT_ROOT."lang/cust/analysis_system_lang.$intranet_session_language.php");
    
    include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
    $lay = new academic_year();
   
    
    $lcees = new libcees();
    $lpf = new libpf_exam();
    $SchoolInfoAry= $lcees->getAllSchoolInfoFromCentralServer('P');
    $i = 0;
    $SchoolNameAry[0][] = '000000';
    $SchoolNameAry[0][] = $Lang['General']['NotApplicable'] ;
    
    if($SchoolInfoAry){
	    foreach($SchoolInfoAry as $SchoolInfo){
	        $i++;
	        $SchoolNameAry[$i][] = $SchoolInfo['SchoolCode'];
	        $SchoolNameAry[$i][] = $SchoolInfo['NameEn'];
	        
	    }
    }
    
    $UniversityInfoAry = $Lang['SDAS']['Jupas']['InstitutionName'];
    $i = 0;
    $UniversityNameAry[0][] = 'XXX';
    $UniversityNameAry[0][] = $Lang['General']['NotApplicable'] ;
    foreach($UniversityInfoAry as $UniversityCode => $UniversityName){
        $i++;
        $UniversityNameAry[$i][] = $UniversityCode;
        $UniversityNameAry[$i][] = $UniversityName;
    }
//     $GraduationYear = getAcademicYearInfoAndTermInfoByDate($GraduationDate);
//     $GraduationAcademicYearID = $GraduationYear[0];
    
    
    $JUPASRecordAry = $lpf->getStudentJupasOfferByStudentID($uid);
    $academicYearID =  $JUPASRecordAry[0]['AcademicYearID'];
    $JUPASCode =  $JUPASRecordAry[0]['JupasCode'];
    $UniversityName = $JUPASRecordAry[0]['Institution'];
    
    $ProgrammeFullName = $JUPASRecordAry[0]['ProgrammeFullName'];
    
    
    $StudentExitInfo = $lpf->Get_User_ExitTo_Info($uid);
    $FinalChoice = $StudentExitInfo['FinalChoice'];
    $Remark = $StudentExitInfo['Remark'];
    if($FinalChoice =='2'){
        $Remark1 = $Remark;
    }
    else if($FinalChoice =='3'){
        $Remark2 = $Remark;
    }
    
    
    $graduateAcademicYearID = $academicYearID == '' ? Get_Current_Academic_Year_ID():$academicYearID;
    
    $academic_year_arr = $lay->Get_All_Year_List();
    $ay_selection_html = getSelectByArray($academic_year_arr, "name='academicYearID' id='academicYearID' ", $graduateAcademicYearID, 0, 1, "", 2);
    
}
?>


<?php if($plugin['StudentDataAnalysisSystem_Style'] == "tungwah"){?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js" type="text/javascript" charset="utf-8"></script>
<script src="/templates/jquery/jquery.inputselect.js" type="text/javascript" charset="utf-8"></script>
<link href="/templates/jquery/jquery.inputselect.css" rel="stylesheet" type="text/css">

<!-- <script src="./TextExt.js" type="text/javascript" charset="utf-8"></script> -->

<script src="/templates/jquery/jquery.autocomplete.js" type="text/javascript" charset="utf-8"></script>
<link href="/templates/jquery/jquery.autocomplete.css" rel="stylesheet" type="text/css">
<script language="javascript">

function changeJUPASProgramme(){
	var programme_title= $( "#UniversityProgramme" ).val();

	$('.inputselect').each(function(){
		var this_id = $(this).attr('id');
		var school_id = $("#UniversityCode option:selected" ).val();
		if($(this).length > 0){
			$(this).autocomplete(
		      "ajax_get_JUPAS_program.php",
		      {
// 		  			delay:3,
		  			minChars:1,
		  			matchContains:1,
		  			extraParams: {'field':this_id,'school_id':school_id,'programme_title':programme_title},
		  			autoFill:true,
		  			overflow_y: 'auto',
		  			overflow_x: 'hidden',
		  			maxHeight: '200px'		  			
		  	   }
		    )	   
		}
	});
		
}

</script>
<?php }?>

<script type="text/javascript" language="javascript">
function goSubmit()
{
	document.form1.action = "edit_archived_student_update.php";
	document.form1.submit();
}

function goBack()
{
	document.form1.action = "archived_student.php";
	document.form1.submit();
}
</script>
<form id="form1" name="form1" method="post" action="edit_archived_student_update.php">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr><td height="40"><?=$linterface->GET_NAVIGATION($PAGE_NAVIGATION)?></td></tr>
    <tr>
    	<td class="board_menu_closed">
    		<table width="99%" border="0" cellspacing="0" cellpadding="0">
				<tr> 
					<td class="main_content">
						<div class="table_board">
							<table class="form_table" width="90%" cellpadding="4" cellspacing="0" border="0">
								<tr>
									<td colspan="3"><em class="form_sep_title"> - <?=$eDiscipline["BasicInfo"]?> -</em></td>
								</tr>
								<tr>
									<td class="formfieldtitle" width="25%"><?=$i_UserLogin?></td>
									<td width="70%">
										<?=$userlogin?>
									</td>
									<td rowspan="8"><?=$Lang['Personal']['OfficialPhoto']?><br /><img src="<?=$photo_link?>" width="100" />
                        				<p><?=$Lang['Personal']['PersonalPhoto']?><br /><img src="<?=$photo_personal_link?>" width="100" /></p>
									</td>
								</tr>
								
								<tr>
									<td class="formfieldtitle"><?=$Lang['Gamma']['UserEmail']?></td>
									<td ><?=Get_String_Display( $userInfo['UserEmail'] )?>
									<br /><span class="tabletextremark"><?=$Lang['AccountMgmt']['EmailFieldReminder']?></span></td>
								</tr>
								
								<? if((isset($plugin['attendancestudent']) && $plugin['attendancestudent']) ||(isset($plugin['payment'])&& $plugin['payment']) ||(isset($plugin['eEnrollment'])&& $plugin['eEnrollment'])) { ?>
								<tr>
									<td class="formfieldtitle"><?=$i_SmartCard_CardID?></td>
									<td >
										<?=Get_String_Display($smartcardid)?>
									</td>
								</tr>
								<?php if($sys_custom['SupplementarySmartCard']){ ?>
								<tr>
									<td class="formfieldtitle"><?=$Lang['AccountMgmt']['SmartCardID2']?></td>
									<td>
										<?=Get_String_Display($smartcardid2)?>
									</td>
								</tr>
								<tr>
									<td class="formfieldtitle"><?=$Lang['AccountMgmt']['SmartCardID3']?></td>
									<td>
										<?=Get_String_Display($smartcardid3)?>
										
									</td>
								</tr>
								<tr>
									<td class="formfieldtitle"><?=$Lang['AccountMgmt']['SmartCardID4']?></td>
									<td>
										<?=Get_String_Display($smartcardid4)?>
										
									</td>
								</tr>
								<?php } ?>
								<? } ?>
								<? if($special_feature['ava_strn']) { ?>
								<tr>
									<td class="formfieldtitle"><?=$i_STRN?></td>
									<td >
										<input name="strn" type="text" id="strn" class="textboxnum" value="<?=$strn?>"/>
										<? if(sizeof($errorAry)>0 && in_array($strn, $errorAry)) echo "<font color='#FF0000'>".$i_STRN.$Lang['AccountMgmt']['UsedByOtherUser']."</font>"; ?>
									</td>
								</tr>
								<? } ?>
								<tr<?=$isKIS?' style="display:none;"':''?>>
									<td class="formfieldtitle"><?=$i_WebSAMS_Registration_No?></td>
									<td >
										<input name="WebSAMSRegNo" type="text" id="WebSAMSRegNo" class="textboxnum" value="<?=$WebSAMSRegNo?>" <?=($iportfolio_activated?"DISABLED=TRUE":"")?> /><br /><span class="tabletextremark"><?=$i_WebSAMSRegNo_Format_Notice?></span>
										<? if(sizeof($errorAry)>0 && in_array($WebSAMSRegNo, $errorAry)) echo "<font color='#FF0000'>".$i_WebSAMS_Registration_No.$Lang['AccountMgmt']['UsedByOtherUser']."</font>"; ?>
									</td>
								</tr>
								<tr<?=$isKIS?' style="display:none;"':''?>>
									<td class="formfieldtitle"><?=$Lang['AccountMgmt']['HKJApplNo']?></td>
									<td >
										<input name="HKJApplNo" type="text" id="HKJApplNo" class="textboxnum" value="<?=$HKJApplNo?>"/> 
										<? if(sizeof($errorAry)>0 && in_array($HKJApplNo, $errorAry)) echo "<font color='#FF0000'>".$Lang['AccountMgmt']['HKJApplNo'].$Lang['AccountMgmt']['UsedByOtherUser']."</font>"; ?>
									</td>
								</tr>
								
								<tr>
									<td class="formfieldtitle"><?=$Lang['AccountMgmt']['Barcode']?></td>
									<td >
										<?=Get_String_Display($barcode)?>
								
									</td>
								</tr>

								<tr>
									<td colspan="3"><em class="form_sep_title"> - <?=$i_UserProfilePersonal?> -</em></td>
								</tr>
								<tr>
								<td class="formfieldtitle" rowspan="2"> <?=$i_general_name?></td>
									<td class="sub_row_content" colspan="2">
										(<?=$ip20_lang_eng?>)
										<?=Get_String_Display($engname)?>
										
									</td>
								</tr>
								<tr>
									<td colspan="2">(<?=$Lang['AccountMgmt']['Chi']?>)</span> 
										<?=Get_String_Display($chiname)?>
									</td>
								</tr>
								<tr>
									<td class="formfieldtitle"><?=$i_UserNickName?></td>
									<td colspan="2"><?=Get_String_Display($nickname)?></td>
								</tr>
								<tr>
									<td class="formfieldtitle"><?=$profile_dob?></td>
									<td colspan="2">
										<input name="dob" type="text" id="dob" value="<?=$dob?>" maxlength="10"/>(<?=$Lang['AccountMgmt']['YYYYMMDD']?>)
										<? if(sizeof($errorAry)>0 && in_array($dob, $errorAry)) echo "<font color='#FF0000'>".$Lang['General']['InvalidDateFormat']."</font>"; ?>
									</td>
								</tr>
								<tr>
								<td class="formfieldtitle"><?=$i_UserGender?></td>
									<td colspan="2">
										<?=$gender=="M" ? $i_gender_male : ($gender=="F"? $i_gender_female : "")?>
									</td>
								</tr>
								<? if($special_feature['ava_hkid']) { ?>
								<tr>
									<td class="formfieldtitle"><?=$i_HKID?></td>
									<td colspan="2">
										<input name="hkid" type="text" id="hkid" class="textboxnum" value="<?=$hkid?>"/>
										<? if(sizeof($errorAry)>0 && in_array($hkid, $errorAry)) echo "<font color='#FF0000'>".$i_HKID.$Lang['AccountMgmt']['UsedByOtherUser']."</font>"; ?>
									</td>
								</tr>
								<? } ?>
								<tr>
								<td class="formfieldtitle"><?=$i_UserAddress?></td>
									<td colspan="2">
										<?=Get_String_Display($address)?>
										<br />
										<?=$countrySelection?>
									</td>
								</tr>
								<tr>
									<td class="formfieldtitle" rowspan="<?=$sys_custom['eAdmin']['AcctMgmt']['UseFaxForBaptism']?'2':'3'?>"><?=$Lang['AccountMgmt']['Tel']?></td>
									<td colspan="2"><span class="sub_row_content">(<?=$Lang['AccountMgmt']['Home']?>)</span>
									<?=Get_String_Display($homePhone)?></td>
								</tr>
								<tr>
									<td colspan="2"><span class="sub_row_content">(<?=$Lang['AccountMgmt']['Mobile']?>)</span>
									<?=Get_String_Display($mobilePhone)?></td>
								</tr>
<?php
	if ($sys_custom['eAdmin']['AcctMgmt']['UseFaxForBaptism']) {
?>
								<tr>
									<td class="formfieldtitle"><?=$Lang['AccountMgmt']['Fax']?></td>
									<td colspan="2"><?=Get_String_Display($faxPhone)?></td>
								</tr>
<?php
	}
	else {
?>
								<tr>
									<td colspan="2"><span class="sub_row_content">(<?=$Lang['AccountMgmt']['Fax']?>)</span>
									<?=Get_String_Display($faxPhone)?></td>
								</tr>
<?php		
	}	
?>								
								
								<!-- Nationality, Birth Place, Admission Date //-->
								<tr>
									<td class="formfieldtitle"><?=$Lang['AccountMgmt']['Nationality']?></td>
									<td colspan="2"><?=Get_String_Display($Nationality)?></td>
								</tr>
								<tr>
									<td class="formfieldtitle"><?=$Lang['AccountMgmt']['PlaceOfBirth']?></td>
									<td colspan="2"><?=Get_String_Display($PlaceOfBirth)?></td>
								</tr>
								<tr>
									<td class="formfieldtitle"><?=$Lang['AccountMgmt']['AdmissionDate']?></td>
									<td colspan="2"><?=Get_String_Display($AdmissionDate)?></td>
								</tr>

								<?php if($plugin['medical']){ ?>
									<tr>
									<td class="formfieldtitle"><?=$Lang["medical"]["stayOverNight"]?></td>
									<td colspan="2">
										<?=$stayOverNight=='1'?$Lang['General']['Yes']:$Lang['General']['No']?>
									</td>
									</tr>	

								<?php	}?>
								<!-- Additional Info //-->
								<tr>
									<td colspan="3"><em class="form_sep_title"> - <?=$Lang['AccountMgmt']['AdditionInfo']?> -</em></td>
								</tr>
								<tr>
								<td class="formfieldtitle"><?=$Lang['General']['Remark']?></td>
									<td colspan="2">
										<?=Get_String_Display($remark)?>
									</td>
								</tr>
								
								<?php if($plugin['StudentDataAnalysisSystem_Style'] == "tungwah"){  ?>	
								<!-- Exit Info //-->								
								<tr>							    
									<td class="formfieldtitle"><?=$Lang['SDAS']['ExitTo']['LeftYear']?></td>
									<td>
										<? echo $ay_selection_html;?>									
									</td>
								</tr>
								 
								<tr>							    
									<td class="formfieldtitle"><?=$Lang['SDAS']['Jupas']['Result']?></td>
									<td>
										<?php echo $Lang['SDAS']['Jupas']['University'].'/'.$Lang['SDAS']['Jupas']['Institution'];?><br>
										<?=$linterface->GET_SELECTION_BOX($UniversityNameAry,"id='UniversityCode' name='UniversityCode' onchange='javascript:changeJUPASProgramme();'","",$UniversityName);?>
										<br>
										<?=$Lang['SDAS']['Jupas']['ProgrammeName']?>
										<br>
											<input id="UniversityProgramme" name="UniversityProgramme" type="text" class="textboxtext inputselect" maxlength="64"  size="40" value="<?php echo $ProgrammeFullName;?>"/>
										<br>
										<?=$Lang['SDAS']['Jupas']['JUPASNo']?>
										<br>
											<input id="JUPASCode" name="JUPASCode" type="text" class="textboxtext inputselect" maxlength="64"  size="40" value="<?php echo $JUPASCode;?>" onclick="javascript:changeJUPASProgramme();" />										
										<span class="tabletextremark">Please strictly adopt the JUPAS notations</span>
									</td>
								</tr>
								
								<tr>							    
									<td class="formfieldtitle"><?=$Lang['SDAS']['ExitTo']['FinalChoice']?></td>
									<td>
										<input name="FinalChoice" type="radio" id="JUPAS" value="1" <? if($FinalChoice=="1") echo "checked";?> /><label for="JUPAS"><?php echo $Lang['SDAS']['ExitTo']['JUPAS'];?></label><br>
										<input name="FinalChoice" type="radio" id="OtherInstitution"  value="2" <? if($FinalChoice=="2") echo "checked";?>/><label for="OtherInstitution"><?php echo $Lang['SDAS']['ExitTo']['OtherInstitution'];?></label> <input type="text" name="Remark1" id="Remark1" value="<?=$Remark1?>"></input><br>
										<input name="FinalChoice" type="radio" id="Employment" value="3" <? if($FinalChoice=="3") echo "checked";?> /><label for="Employment"><?=$Lang['SDAS']['ExitTo']['Employment']?></label> <input type="text" name="Remark2" id="Remark2" value="<?=$Remark2?>"></input><br>
									
									</td>
								</tr>
								
								
								<?php }?>
								
								
								
								<?php if($sys_custom['StudentAccountAdditionalFields']){ ?>
								<tr>
									<td class="formfieldtitle"><?=$Lang['AccountMgmt']['NonChineseSpeaking']?></td>
									<td><?= $NonChineseSpeaking=="Y"? $Lang['General']['Yes'] : $Lang['General']['No']?></td>
								</tr>
								<tr>
									<td class="formfieldtitle"><?=$Lang['AccountMgmt']['SpecialEducationNeeds']?></td>
									<td><?=$SpecialEducationNeeds=="Y"? $Lang['General']['Yes'] : $Lang['General']['No']?></td>
								</tr>
								<tr>
									<td class="formfieldtitle"><?=$Lang['AccountMgmt']['PrimarySchool']?></td>
									<td><?=Get_String_Display($PrimarySchool)?></td>
								</tr>
								<tr>
									<td class="formfieldtitle"><?=$Lang['AccountMgmt']['UniversityInstitution']?></td>
									<td><?=Get_String_Display($University)?></td>
								</tr>
								<tr>
									<td class="formfieldtitle"><?=$Lang['AccountMgmt']['Programme']?></td>
									<td><?=Get_String_Display($Programme)?></td>
								</tr>
								<?php } ?>
								<?php  if($nameDisplay){?>
								 <tr>
    							<td colspan="3"><em class="form_sep_title"> -  <?=$Lang['AccountMgmt']['ArchivedRecords']?> -</em></td>
    						</tr>
    							<tr>
    							<td class="formfieldtitle"><?=$Lang['AccountMgmt']['ArchivedBy']?></td>
    							<td colspan="2"><?=$nameDisplay?></td>
    							</tr>
    							<tr>
    							<td class="formfieldtitle"><?=$Lang['AccountMgmt']['DateOfArchive']?></td>
    							<td colspan="2"><?=$DateArchive?></td>
    							</tr>
    							<? } ?>
								
							</table>
						</div>
						<p class="spacer"></p>
	                        <div class="edit_bottom">									
								<?= $linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "button", "goSubmit();")?>
								<?= $linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "goBack();")?>
	                        </div>			
					</td>
				</tr>
			</table>
    	</td>
    </tr>
</table>
<input type="hidden" name="user_id" id="user_id" value="<?=$uid?>">
<input type="hidden" name="pageNo" id="pageNo" value="<?=$li->pageNo; ?>" />
<input type="hidden" name="order" id="order" value="<?=$li->order; ?>" />
<input type="hidden" name="field" id="field" value="<?=$li->field; ?>" />
<input type="hidden" name="page_size_change" id="page_size_change" value="" />
<input type="hidden" name="numPerPage" id="numPerPage" value="<?=$li->page_size?>" />
<input type="hidden" name="keyword" id="keyword" value="<?=trim(stripslashes(rawurldecode($keyword)))?>" />
</form>
<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>