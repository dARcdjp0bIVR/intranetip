<?php 
# using: henry chow

################# Change Log [Start] ############
#
################# Change Log [End] ############

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$laccount = new libaccountmgmt();


if(!($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Student"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"])) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

# set cookies
$arrCookies[] = array("ck_page_size", "numPerPage");
$arrCookies[] = array("ck_page_no", "pageNo");
$arrCookies[] = array("ck_page_order", "order");
$arrCookies[] = array("ck_page_field", "field");

$arrCookies[] = array("ck_parent_targetClass", "targetClass");
$arrCookies[] = array("ck_parent_recordstatus", "recordstatus");
$arrCookies[] = array("ck_parent_keyword", "keyword");
if(isset($clearCoo) && $clearCoo == 1)
{
	clearCookies($arrCookies);
} 
else 
{
	updateGetCookies($arrCookies);
}

$CurrentPageArr['StudentMgmt'] = 1;
$CurrentPage = "Settings_OfficalPhotoMgmt";

# Left menu 
$TAGS_OBJ[] = array($Lang['AccountMgmt']['OfficalPhotoMgmt']);
$MODULE_OBJ = $laccount->GET_MODULE_OBJ_ARR();

$PAGE_NAVIGATION[] = array($Lang['AccountMgmt']['UserList'], "javascript:goURL('index.php')");

# Start layout
$linterface->LAYOUT_START($xmsg);

?>

<script language="javascript">
<!--
function goURL(url) {
	document.form1.action = "index.php";
	document.form1.submit();
}


//-->
</script>
		
<form name="form1" id="form1" method="POST" onSubmit="return false;">

<div class="navigation">
	<img height="15" width="15" align="absmiddle" src="<?=$PATH_WRT_ROOT?>images/<?=$LAYOUT_SKIN?>/nav_arrow.gif"/><?=$Lang['AccountMgmt']['UserList']?>
</div>
<div class="content_top_tool">
	<div class="Conntent_tool"><a title="<?=$Lang['StaffAttendance']['AddUser']?>" href="add.php"><?=$Lang['StaffAttendance']['AddUser']?></a></div>
	<div class="Conntent_search" id="SearchInputLayer">
	    <input type="text" name="Keyword" id="Keyword" onkeyup="Check_Go_Search(event);" />
	</div>
	<br style="clear: both;"/>
	
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tbody>
			<tr>
		    	<td valign="bottom"><div class="common_table_tool"> <a title="<?=$Lang['Btn']['Delete']?>" class="tool_delete" href="javascript:void(0);" onclick="Remove_Group_User();" /><?=$Lang['Btn']['Delete']?></a></div></td>
		  	</tr>
		</tbody>
		<tr>
			<td>
				<div class="table_board" id="UserListLayer"></div>
			</td>
		</tr>
	</table>	
	
</div>

</form>

<script language="Javascript">
{
<?
if(isset($msg))
	echo "Get_Return_Message('$msg');";
?>	
	
	
function Check_Go_Search(evt)
{
	var key = evt.which || evt.charCode || evt.keyCode;
	
	if (key == 13) // enter
		Get_Group_User_List();
	else
		return false;
}
}

// ajax function
{
function Get_Group_User_List()
{

	var PostVar = {
			"Keyword": encodeURIComponent($('Input#Keyword').val())
			}
		
	Block_Element("UserListLayer");
	$.post('ajax_get_user_list.php',PostVar,
	
	function(data){
		if (data == "die") 
			window.top.location = '/';
		else {
			$('div#UserListLayer').html(data);
			//Thick_Box_Init();
			UnBlock_Element("UserListLayer");
		}
	});
}

function Remove_Group_User()
{
	
	if(countChecked(document.form1, 'userID[]')==0) {
		alert(globalAlertMsg2);
	} else {
	
		var PostString = Get_Form_Values(document.getElementById("form1"));
		var PostVar = PostString;
	
		Block_Element("UserListLayer");
		$.post('ajax_remove_user.php',PostVar,
			function(data){
				//alert(data);
				if (data == "die") 
					window.top.location = '/';
				else {
					Get_Return_Message(data);
					Get_Group_User_List();
				}
			});
	}
}
}

Get_Group_User_List();
</script>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
