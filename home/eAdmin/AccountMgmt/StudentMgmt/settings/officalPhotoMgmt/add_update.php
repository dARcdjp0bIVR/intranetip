<?

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");


intranet_auth();
intranet_opendb();

$laccount = new libaccountmgmt();

if(sizeof($staff)>0) {
	foreach($staff as $id) {
		if($id!="")
			$userID[] = !is_numeric($id) ? substr($id,1) : $id;
	}

	$result = $laccount->addOfficalPhotoMgmtGroupMember($userID);
}

intranet_closedb();

$msg = $result ? $Lang['StaffAttendance']['AddMemberSuccess'] : $Lang['StaffAttendance']['AddMemberFail'];

header("Location: index.php?msg=$msg");
?>