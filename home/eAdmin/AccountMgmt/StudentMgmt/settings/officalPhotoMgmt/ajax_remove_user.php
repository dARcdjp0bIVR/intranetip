<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");


intranet_auth();
intranet_opendb();

$laccount = new libaccountmgmt();

if(sizeof($userID)>0)
	$result = $laccount->removeOfficalPhotoMgmtGroupMember($userID);

intranet_closedb();

if($result)
	echo $Lang['StaffAttendance']['DeleteMemberSuccess'];
else
	echo $Lang['StaffAttendance']['DeleteMemberFail'];
?>