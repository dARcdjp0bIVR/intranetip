<?php
# using: 

########################################
#
########################################
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");


intranet_auth();
intranet_opendb();

$laccount = new libaccountmgmt();

if(!($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Student"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"])) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}


$linterface = new interface_html();
/*
if($GroupID=="")
	header("Location: index.php");
*/
$CurrentPageArr['StudentMgmt'] = 1;
$CurrentPage = "Settings_OfficalPhotoMgmt";

$TAGS_OBJ[] = array($Lang['AccountMgmt']['OfficalPhotoMgmt']);

$MODULE_OBJ = $laccount->GET_MODULE_OBJ_ARR();


$student_selected = $linterface->GET_SELECTION_BOX($studentArray, "name='staff[]' id='staff[]' class='select_studentlist' size='15' multiple='multiple'", "");
$button_remove_html = $linterface->GET_BTN($button_remove, "button", "javascript:checkOptionRemove(document.getElementById('staff[]'))");

$linterface->LAYOUT_START();

$select_target_field = $Lang['CommonChoose']['SelectTargetField']['TeacherStaff'];
$typeList = "1,4";		# 1-teacher/staff only	


### start for searching ###
#get required data
$data_ary = array();

$name_field = getNameFieldByLang("USR.");

$sql1 = "SELECT USR.UserID, $name_field as name, ycu.ClassNumber, UserLogin FROM INTRANET_USER USR LEFT OUTER JOIN YEAR_CLASS_USER ycu ON (ycu.UserID=USR.UserID) WHERE USR.RecordType IN ($typeList) and USR.RecordStatus=1 $conds GROUP BY USR.UserID ORDER BY UserLogin";
$result1 = $laccount->returnArray($sql1,5);

for ($j = 0; $j < sizeof($result1); $j++) 
{
	list($this_userid, $this_stu_name, $this_class_number, $this_userlogin) = $result1[$j];
	$data_ary[] = array($this_userid, $this_classname, $this_class_number, $this_stu_name, $this_userlogin);
}

if(!empty($data_ary)) 
{
	#define yui array (Search by input format )
	for($i=0;$i<sizeof($data_ary);$i++)
	{
		list($this_userid, $this_classname, $this_class_number, $this_stu_name, $this_userlogin) = $data_ary[$i];
	
		$temp_str = $this_classname . $this_class_number. " ". $this_stu_name;

		$temp_str2 = $this_stu_name;
		
		$liArr .= "[\"". addslashes(intranet_undo_htmlspecialchars($temp_str)) ."\", \"". addslashes(intranet_undo_htmlspecialchars($temp_str2)) ."\", \"". $this_userid ."\"]";
		($i == (sizeof($result)-1)) ? $liArr .= "" : $liArr .= ",\n";
	}
	
	foreach ($data_ary as $key => $row) 
	{
		$field1[$key] = $row[0];	//user id
		$field2[$key] = $row[1];	//class name
		$field3[$key] = $row[2];	//class number
		$field4[$key] = $row[3];	//stu name
		$field5[$key] = $row[4];	//login id
	}
	array_multisort($field5, SORT_ASC, $data_ary);
	
	#define yui array (Search by login id )
	for($i=0;$i<sizeof($data_ary);$i++)
	{
		list($this_userid, $this_classname, $this_class_number, $this_stu_name, $this_userlogin) = $data_ary[$i];

		$temp_str2 = $this_stu_name;
		
		$liArr2 .= "[\"". $this_userlogin ."\", \"". addslashes(intranet_undo_htmlspecialchars($temp_str2)) ."\", \"". $this_userid ."\"]";
		($i == (sizeof($result)-1)) ? $liArr2 .= "" : $liArr2 .= ",\n";
	}
	### end for searching
}

$PAGE_NAVIGATION[] = array($i_Discipline_System_Group_Right_Navigation_Group_List, "index.php");
$PAGE_NAVIGATION[] = array($groupTitle, "member.php?GroupID=$GroupID");
$PAGE_NAVIGATION[] = array($i_Discipline_System_Group_Right_Navigation_New_Member, "");



?>


<link type="text/css" rel="stylesheet" href="<?= $PATH_WRT_ROOT?>templates/yui/autocomplete_files/dpSyntaxHighlighter.css">

<style type="text/css">
    #statesmod {position:relative;}
    #statesautocomplete {position:relative;width:22em;margin-bottom:1em;}/* set width of widget here*/
    #statesautocomplete {z-index:9000} /* for IE z-index of absolute divs inside relative divs issue */
    #statesinput {_position:absolute;width:100%;height:1.4em;z-index:0;} /* abs for ie quirks */
    #statescontainer, #statescontainerCC, #statescontainerBCC {position:absolute;top:0.3em;width:100%}
    #statescontainer .yui-ac-content, #statescontainerCC .yui-ac-content, #statescontainerBCC .yui-ac-content {position:absolute;width:100%;border:1px solid #404040;background:#eeeeee;overflow:hidden;z-index:9050;}
    #statescontainer .yui-ac-shadow, #statescontainerCC .yui-ac-shadow, #statescontainerBCC .yui-ac-shadow {position:absolute;margin:.3em;width:100%;background:#a0a0a0;z-index:9049;}
    #statescontainer ul, #statescontainerCC ul, #statescontainerBCC ul {padding:5px 0;width:100%;}
    #statescontainer li, #statescontainerCC li, #statescontainerBCC li {padding:0 5px;cursor:default;white-space:nowrap;}
    #statescontainer li.yui-ac-highlight, #statescontainerCC li.yui-ac-highlight, #statescontainerBCC li.yui-ac-highlight {background:#bbbbbb;}
    #statescontainer li.yui-ac-prehighlight, #statescontainerCC li.yui-ac-prehighlight, #statescontainerBCC li.yui-ac-prehighlight {background:#FFFFFF;}
    
    
	#statesmod div,dl,dt,dd,ul,ol,li,h1,h2,h3,h4,h5,h6,pre,form,fieldset,input,textarea,p,blockquote{margin:0;padding:0;}
	#statesmod table{border-collapse:collapse;border-spacing:0;}
	#statesmod fieldset,img{border:0;}
	#statesmod address,caption,cite,code,dfn,em,strong,th,var{font-style:normal;font-weight:normal;}
	#statesmod ol,ul {list-style:none;}
	#statesmod caption,th {text-align:left;}
	#statesmod h1,h2,h3,h4,h5,h6{font-size:100%;font-weight:normal;}
	#statesmod q:before,q:after{content:'';}
	#statesmod abbr,acronym {border:0;}
	#statesmod {font:13px arial,helvetica,clean,sans-serif;*font-size:small;*font:x-small;}
</style>



<script language="javascript">

function checkForm()
{
    obj = document.form1;
    var staff_length = obj.elements['staff[]'].length;
    
    if(staff_length==0) {
    	alert("<?=$Lang['General']['PleaseSelectMembers']?>");
    	return false;
    } else {
		checkOptionAll(obj.elements["staff[]"]);
		
    }
    
}

function checkCR(evt) {

	var evt  = (evt) ? evt : ((event) ? event : null);
	var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
	if ((evt.keyCode == 13) && (node.type=="text")) {return false;}	

}

document.onkeypress = checkCR;
</SCRIPT>

<form name="form1" method="POST" action="add_update.php" onsubmit="return checkForm();">
<div class="navigation">
	<img height="15" width="15" align="absmiddle" src="<?=$PATH_WRT_ROOT?>images/<?=$LAYOUT_SKIN?>/nav_arrow.gif"/><a href="index.php"><?=$Lang['AccountMgmt']['UserList']?></a>
	<img height="15" width="15" align="absmiddle" src="<?=$PATH_WRT_ROOT?>images/<?=$LAYOUT_SKIN?>/nav_arrow.gif"/><?=$Lang['StaffAttendance']['AddUser']?>
	
</div>

<div class="this_content">
<table class="inside_form_table">
<tr valign="top">
	<td width="40%"><span class="tabletext"><?=$i_Discipline_System_Group_Right_Navigation_Choose_Member ?>:</span></td>
	<td width="10" nowrap="nowrap"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="10"></td>
	<td width="60%" nowrap="nowrap"><span class="tabletext"><?=$i_Discipline_System_Group_Right_Navigation_Selected_Member2 ?>:</span></td>
</tr>
<tr>
	<td valign="top" class="tablerow2">
		<table width="100%" border="0" cellpadding="5" cellspacing="0">
			<tr>
				<td class="tabletext"><?=$select_target_field?> </td>
			</tr>
			<tr>
			<td class="tabletext">
			<?//= $linterface->GET_BTN($button_select." temp", "button", "javascript:newWindow('../choose/index.php?fieldname=student[]&GroupID=$GroupID', 9)")?>
			<?= $linterface->GET_BTN($button_select, "button", "javascript:newWindow('/home/common_choose/index.php?fieldname=staff[]&page_title=SelectMembers&permitted_type=$typeList', 16)")?>
			</td>
				</tr>
			<tr>
				<td class="tabletext"><i><?=$i_general_or?></i></td>
			</tr>
			<tr>
				<td class="tabletext"><?=$i_general_search_by_loginid?><br \>
					<div id="statesautocomplete">
						<input type="text" class="tabletext" name="search2" id="search2">
						<div id="statescontainerCC" style="left:142px; top:0px;">
							<div style="display: none; width: 199px; height: 0px;" class="yui-ac-content">
								<div style="display: none;" class="yui-ac-hd"></div>
								<div class="yui-ac-bd"></div>
								<div style="display: none;" class="yui-ac-ft"></div>
							</div>
							<div style="width: 0pt; height: 0pt;" class="yui-ac-shadow"></div>
						</div>
					</div>

				</td>
			</tr>
		</table>
	</td>
	<td valign="top" nowrap="nowrap">&nbsp;</td>
	<td valign="top" nowrap="nowrap">
		<table width="100%" border="0" cellpadding="3" cellspacing="0">
		<tr>
			<td><?=$student_selected ?><?=$button_remove_html?>
			</td>
		</tr>
		</table>
	</td>
</tr>
</table>

<div class="edit_bottom_v30">
<p class="spacer"></p>
<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "") ?>&nbsp;
<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:self.location.href='index.php'") ?>
<p class="spacer"></p>
</div>

</div>


</form>

    <?
		print $linterface->FOCUS_ON_LOAD("form1.search2");
        $linterface->LAYOUT_STOP();
?>


<!-- Libary begins -->
<script type="text/javascript" src="<?= $PATH_WRT_ROOT?>templates/yui/autocomplete_files/yahoo.js"></script>
<script type="text/javascript" src="<?= $PATH_WRT_ROOT?>templates/yui/autocomplete_files/dom.js"></script>
<script type="text/javascript" src="<?= $PATH_WRT_ROOT?>templates/yui/autocomplete_files/event-debug.js"></script>
<script type="text/javascript" src="<?= $PATH_WRT_ROOT?>templates/yui/autocomplete_files/animation.js"></script>
<script type="text/javascript" src="<?= $PATH_WRT_ROOT?>templates/yui/autocomplete_files/autocomplete-debug-ip20.js"></script>
<!-- Library ends -->

<!-- In-memory JS array begins-->
<script type="text/javascript">

var statesArray = [
    <?= $liArr?>
];

var loginidArray = [
    <?= $liArr2?>
];

var delimArray = [
    ";"
];
</script>
<!-- In-memory JS array ends-->


<script type="text/javascript">
YAHOO.example.ACJSArray = function() {
    var oACDS, oACDS2, oAutoComp, oAutoComp2;
    return {
        init: function() {

            // Instantiate first JS Array DataSource
            /*
            oACDS = new YAHOO.widget.DS_JSArray(statesArray);

            // Instantiate first AutoComplete            
            oAutoComp = new YAHOO.widget.AutoComplete('search1','staff[]', 'statescontainer', oACDS);
            oAutoComp.queryDelay = 0;
            oAutoComp.prehighlightClassName = "yui-ac-prehighlight";
            oAutoComp.useShadow = true;
            oAutoComp.minQueryLength = 0;
            */
            oACDS2 = new YAHOO.widget.DS_JSArray(loginidArray);
            oAutoComp2 = new YAHOO.widget.AutoComplete('search2','staff[]', 'statescontainerCC', oACDS2);
            oAutoComp2.queryDelay = 0;
            oAutoComp2.prehighlightClassName = "yui-ac-prehighlight";
            oAutoComp2.useShadow = true;
            oAutoComp2.minQueryLength = 0;
        },

        validateForm: function() {
            // Validate form inputs here
            return false;
        }
    };
}();

YAHOO.util.Event.addListener(this,'load',YAHOO.example.ACJSArray.init);
</script>

<script type="text/javascript" src="<?= $PATH_WRT_ROOT?>templates/yui/autocomplete_files/dpSyntaxHighlighter.js"></script>
<script type="text/javascript">
dp.SyntaxHighlighter.HighlightAll('code');
</script>