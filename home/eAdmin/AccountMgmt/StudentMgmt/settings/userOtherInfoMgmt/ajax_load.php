<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt_ui.php");

intranet_auth();
intranet_opendb();

$laccount = new libaccountmgmt();
$laccount_ui = new libaccountmgmt_ui();

switch($Action)
{
	case "ReloadItemTable":
		echo $laccount_ui->Get_User_Extra_Info_Setting_Table();
	break;
	
	case "ReloadCategoryLayer":
		echo $laccount_ui->Get_User_Extra_Info_Category_Setting_Layer();
	break;
	
}

intranet_closedb();

?>