<?php 
# using: henry chow

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt_ui.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$laccount = new libaccountmgmt();
$laccount_ui = new libaccountmgmt_ui();


if(!($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Student"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"])) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$CurrentPageArr['StudentMgmt'] = 1;
$CurrentPage = "Settings_UserOtherInfoMgmt";

# Left menu 
$TAGS_OBJ[] = array($Lang['AccountMgmt']['UserExtraInfoMgmt']);
$MODULE_OBJ = $laccount->GET_MODULE_OBJ_ARR();

# Start layout
$linterface->LAYOUT_START($xmsg);

echo $laccount_ui->Include_JS_CSS();
echo $laccount_ui->Get_User_Extra_Info_Setting_UI();


?>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.jeditable.js"></script>
<script language="Javascript">
var AddingItem = 0;
var EditCategoryID = '';


function js_Reload_Setting_Table()
{
	Block_Element("ExtraInfoTableDiv");
	$.post(
		"ajax_load.php",
			{
				Action:"ReloadItemTable"
			},
			function(ReturnData)
			{
				$("#ExtraInfoTableDiv").html(ReturnData);
				js_Init_DND_Table();
				initThickBox();
				js_Init_Editable();
				UnBlock_Element("ExtraInfoTableDiv");
			}
	)
}

function js_Add_Item(CatID)
{
	
	if(AddingItem==0)
	{
		var html = '<?=$laccount_ui->Get_User_Extra_Info_Setting_Table_Add_Row()?>';
		
		$("tbody.Cat_"+CatID).find(".addItemRow").before(html)
		$("tbody.Cat_"+CatID).find(".CatName").attr("rowspan",$("tbody.Cat_"+CatID+">tr").length)
		AddingItem = 1;
		EditCategoryID = CatID;
	}
}

function js_Cancel_Item(CatID)
{
	$(".tmp_new_row").remove();
	$("tbody.Cat_"+CatID).find(".CatName").attr("rowspan",$("tbody.Cat_"+EditCategoryID+">tr").length)
	AddingItem = 0;
}

function js_Delete_Item(ItemID)
{
	if(!confirm("<?=$Lang['AccountMgmt']['jsConfirm']['DeleteUserExtraInfoItem']?>"))
		return false;
	
	$.post(
			"ajax_update.php",
			{
				Action:"DeleteItem",
				ItemID:ItemID
			},
			function(ReturnData)
			{
				Get_Return_Message(ReturnData);
				if(ReturnData.substring(0,1)==1)
				{
					js_Reload_Setting_Table();
				}
			}
		)
}

function js_Check_Item()
{
	var ItemNameEn = $("#ItemNameEn").val();
	var ItemNameCh = $("#ItemNameCh").val();
	
	FormValid = true;
	
	$("#WarnItemNameEn").hide();
	if(ItemNameEn.Trim()=='')
	{
		FormValid= false;
		$("#WarnItemNameEn").show();
	}
	
	$("#WarnItemNameCh").hide();
	if(ItemNameCh.Trim()=='')
	{
		FormValid= false;
		$("#WarnItemNameCh").show();
	}
		
	if(FormValid)
	{
		$.post(
			"ajax_update.php",
			{
				Action:"AddItem",
				CategoryID:EditCategoryID,
				ItemNameEn:ItemNameEn,
				ItemNameCh:ItemNameCh
			},
			function(ReturnData)
			{
				Get_Return_Message(ReturnData);
				if(ReturnData.substring(0,1)==1)
				{
					js_Reload_Setting_Table();
					AddingItem = 0;
					EditCategoryID = '';
				}
			}
		)
	}
}

function js_Reload_Category_Layer(Msg)
{
	$.post(
		"ajax_load.php",
		{
			Action:"ReloadCategoryLayer"
		},
		function(ReturnData)
		{
			$("div#TB_ajaxContent").html(ReturnData);
//			js_Init_DND_Table();
//			js_Init_Category_Editable();

			if(Msg)
				Get_Return_Message(Msg);
		}
	)
}

function js_Update_Category_Multi_Select(CategoryID, obj)
{
	oriStatus = $(obj).attr("className").Trim();
	if($(obj).attr("className").Trim()=='inactive_item') // not multi select
	{
		var RecordType = 1; // enable multi select 
		newStatus = "active_item"
	}
	else
	{
		var RecordType = 2;// disable multi select
		newStatus = "inactive_item"
	}
		 
	$.post(
		"ajax_update.php",
		{
			Action:"UpdateCategoryMultiSelect",
			CategoryID:CategoryID,
			RecordType:RecordType
		},
		function(ReturnData){
			Get_Return_Message(ReturnData);
			if(ReturnData.substring(0,1)==1)
			{
				$(obj).attr("className",newStatus)
			}
		}
	);
	
	
}

var AddingCategory = 0;
function js_Add_Category(CatID)
{
	if(AddingCategory==0)
	{
		var html = '<?=$laccount_ui->Get_User_Extra_Info_Category_Edit_Layer_Add_Row()?>';
		
		$("table#CategorySettingTable").find(".addItemRow").before(html)
		$("table#CategorySettingTable").find(".CatName").attr("rowspan",$("tbody.Cat_"+CatID+">tr").length)
		AddingCategory = 1;
	}
}

function js_Delete_Category(CategoryID)
{
	$.post(
		"ajax_update.php",
		{
			Action:"DeleteCategory",
			CategoryID:CategoryID
		},
		function(ReturnData){
//			Get_Return_Message(ReturnData);
			if(ReturnData.substring(0,1)==1)
				js_Reload_Category_Layer(ReturnData)
		}
	);
	
	
}

function js_Cancel_Category()
{
	$("table#CategorySettingTable").find(".tmp_new_row").remove();
	AddingCategory = 0;
}

function js_Check_Category()
{
	var CategoryNameEn = $("input#CategoryNameEn").val();
	var CategoryNameCh = $("input#CategoryNameCh").val();
	var CategoryCode = $("input#CategoryCode").val();
	var RecordType = $("input#MultiSelect").attr("checked")==1?1:2; 
	
	FormValid = true;
	
	$(".WarnMsg").hide();
	$(".Mandatory").each(function(){
		console.log($(this).val());
		if($(this).val().Trim()=='')
		{
			FormValid= false;
			$("#WarnEmpty"+$(this).attr("id")).show();
		}
	})
	
	if(CategoryCode.Trim()!='')
	{
		if(!valid_code(CategoryCode))	
		{
			$("#WarnInvalidCode").show();
		}
	}
	

	if(FormValid)
	{
		$.post(
			"ajax_update.php",
			{
				Action:"AddCategory",
				CategoryCode:CategoryCode,
				CategoryNameEn:CategoryNameEn,
				CategoryNameCh:CategoryNameCh,
				RecordType:RecordType
			},
			function(ReturnData)
			{
				if(ReturnData.substring(0,1)==1)
				{
					js_Reload_Category_Layer(ReturnData);
					AddingCategory = 0;
				}
			}
		)
	}
}


$().ready(function(){
	js_Reload_Setting_Table();
	
})

function js_Init_DND_Table() {
		
	var JQueryObj = $("table#ExtraInfoTable");
	var DraggingRowIdx = 0; 
	
	JQueryObj.tableDnD({
		onDrop: function(table, DroppedRow) {
			if($(table).find("tr").index(DroppedRow)!= DraggingRowIdx) // update db only if order changed
			{
				var CategoryID = $(DroppedRow).parent().attr("CategoryID");
				
				js_Update_Order(CategoryID);
			}	
			
		},
		onDragStart: function(table, DraggedTD) {
			DraggingRowIdx = $(table).find("tr").index($(DraggedTD).parent());
		},
		dragHandle: "Draggable", 
		onDragClass: "move_selected"
	});
	
}

function js_Update_Order(CategoryID)
{
	var jsSubmitString = $("tbody.Cat_"+CategoryID).find("input.OrderItemID").serialize();
	
	jsSubmitString += '&Action=UpdateItemOrder&CategoryID='+CategoryID;

	$.ajax({  
		type: "POST",  
		url: "ajax_update.php",
		data: jsSubmitString,  
		success: function(ReturnData) {
			Get_Return_Message(ReturnData);
			Scroll_To_Top();
			if(ReturnData.substring(0,1) != 1)
				js_Reload_Setting_Table();
				
		} 
	});
	
}

function js_Init_Editable()
{
	$('.edit').editable(
		function (jsInputValue, settings)
		{	
			var jsDivID = $(this).attr("id");
			var idArr = jsDivID.split('_');
			var jsField = idArr[0];
			var jsItemID = idArr[1];
			
			ElementObj = $(this)
			if(jsInputValue.Trim()=='')
			{
				ElementObj[0].reset();
			}
			else
			{
				ElementObj.html('<?=$linterface->Get_Ajax_Loading_Image()?>');
				js_Update_Item_Info(jsItemID,jsField,jsInputValue,ElementObj);
			}
		}
		, {
        indicator 	:  '<img src="/images/2009a/indicator.gif">',
		tooltip   	: "<?=$Lang['SysMgr']['Location']['ClickToEdit']?>",
		event 		: "click",
		onblur 		: "submit",
		type 		: "text",
		style  		: "display:inline",
		maxlength  	: "255",
		height		: "20px",
		width		: "75px",
		select		: true
     });
}

function js_Update_Item_Info(ItemID, Field, Value, EditObj)
{
	$.post(
			"ajax_update.php",
			{
				Action:"UpdateItem",
				ItemID:ItemID,
				Field:Field,
				Value:Value
			},
			function(ReturnData)
			{
				Get_Return_Message(ReturnData);
				if(ReturnData.substring(0,1)==1)
				{
					EditObj.html(Value)
				}
				else
				{
					EditObj[0].reset();
				}
			}
		)
}

function Show_Edit_Background(Obj) {
	Obj.style.backgroundImage = "url(<?=$PATH_WRT_ROOT?>/images/<?=$LAYOUT_SKIN?>/icon_edit_b.gif)";
	Obj.style.backgroundPosition = "center right";
	Obj.style.backgroundRepeat = "no-repeat";
}

function Hide_Edit_Background(Obj) {
	Obj.style.backgroundImage = "";
	Obj.style.backgroundPosition = "";
	Obj.style.backgroundRepeat = "";
}

</script>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
