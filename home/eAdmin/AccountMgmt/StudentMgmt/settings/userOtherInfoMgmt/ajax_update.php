<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$laccount = new libaccountmgmt();

switch($Action)
{
	case "AddItem":
		$ItemNameEn = trim(stripslashes($_REQUEST['ItemNameEn']));
		$ItemNameCh = trim(stripslashes($_REQUEST['ItemNameCh']));
		$CategoryID = $_REQUEST['CategoryID'];
		
		$Success = $laccount->Insert_User_Extra_Info_Item($CategoryID,$ItemNameEn,$ItemNameCh);
		if($Success)
			echo $Lang['General']['ReturnMessage']['AddSuccess'];
		else
			echo $Lang['General']['ReturnMessage']['AddUnsuccess'];
	break;
	
	case "UpdateItemOrder":
		$CategoryID = $_REQUEST['CategoryID'];
		$ItemIDArr = $_REQUEST['OrderItemIDArr'];
		$Success = $laccount->Update_User_Extra_Info_Display_Order($CategoryID,$ItemIDArr);
		if($Success)
			echo $Lang['General']['ReturnMessage']['UpdateSuccess'];
		else
			echo $Lang['General']['ReturnMessage']['UpdateUnsuccess'];
	break;
	
	case "DeleteItem":
		$ItemID = $_REQUEST['ItemID'];
		$Success = $laccount->Remove_User_Extra_Info_Item($ItemID);
		if($Success)
			echo $Lang['General']['ReturnMessage']['DeleteSuccess'];
		else
			echo $Lang['General']['ReturnMessage']['DeleteUnsuccess'];
	break;
	
	case "UpdateItem":
		
    	$Value = trim(stripslashes($_REQUEST['Value']));
    	$Field = trim(stripslashes($_REQUEST['Field']));
    	
    	$UpdateArr[$Field] = $Value;
    	$Success = $laccount->Update_User_Extra_Info_Item($ItemID,$UpdateArr);
    	
		if($Success)
			echo $Lang['General']['ReturnMessage']['UpdateSuccess'];
		else
			echo $Lang['General']['ReturnMessage']['UpdateUnsuccess'];
		
	break;
	
	case "AddCategory":
		$CategoryNameEn = trim(stripslashes($_REQUEST['CategoryNameEn']));
		$CategoryNameCh = trim(stripslashes($_REQUEST['CategoryNameCh']));
		$CategoryCode = $_REQUEST['CategoryCode'];
		$RecordType = $_REQUEST['RecordType'];
		
		if($laccount->Is_Category_Code_Valid($CategoryCode))
		{
			$Success = $laccount->Insert_User_Extra_Info_Category($CategoryCode,$CategoryNameEn,$CategoryNameCh,$RecordType);
			if($Success)
				echo $Lang['General']['ReturnMessage']['AddSuccess'];
			else
				echo $Lang['General']['ReturnMessage']['AddUnuccess'];
		}
		else
			echo $Lang['AccountMgmt']['ReturnMessage']['CodeNotAvailable'];
		
	break;
	
	case "UpdateCategoryMultiSelect":
	
		$CategoryID = $_REQUEST['CategoryID'];
		$UpdateArr['RecordType'] = $_REQUEST['RecordType'];
		$Success = $laccount->Update_User_Extra_Info_Category($CategoryID,$UpdateArr);
		
		if($Success)
			echo $Lang['General']['ReturnMessage']['UpdateSuccess'];
		else
			echo $Lang['General']['ReturnMessage']['UpdateUnsuccess'];
	break;
	
	case "DeleteCategory":
		$CategoryID = $_REQUEST['CategoryID'];
		$UpdateArr['RecordStatus'] = 0;
		$Success = $laccount->Update_User_Extra_Info_Category($CategoryID,$UpdateArr);
		
		if($Success)
			echo $Lang['General']['ReturnMessage']['DeleteSuccess'];
		else
			echo $Lang['General']['ReturnMessage']['DeleteUnsuccess'];
	break;
}
?>