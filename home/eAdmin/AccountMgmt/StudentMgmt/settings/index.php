<?php 
# using: yuen

################# Change Log [Start] ############
#	Date:	2014-08-29	YatWoon
#			Improved: enable "update password" for KIS version [Case#S66387]
#			Deploy: IPv10.1
#
#	Date:	2014-03-03	Yuen
#			relocated "Enable password policy" School Settings -> System Security
#
#	Date:	2011-12-14	YatWoon
#			add "Enable password policy"
#
################# Change Log [End] ############

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

if(!($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Student"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"])) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$laccount = new libaccountmgmt();
$lgeneralsettings = new libgeneralsettings();

$CurrentPageArr['StudentMgmt'] = 1;
$CurrentPage = "Page_Settings";

# Left menu 
$TAGS_OBJ[] = array($i_general_BasicSettings);
$MODULE_OBJ = $laccount->GET_MODULE_OBJ_ARR();

# Start layout
$linterface->LAYOUT_START($xmsg);

$data = $lgeneralsettings->Get_General_Setting("UserInfoSettings");
$thisUserType = USERTYPE_STUDENT;

##### Contact Info
$personal_info .= $data['CanUpdateDOB_'.$thisUserType] ? "- " . $i_UserDateOfBirth . "<br>" : "";
$personal_info .= $data['CanUpdateGender_'.$thisUserType] ? "- " . $i_UserGender . "<br>" : "";
$personal_info .= $data['CanUpdateNickName_'.$thisUserType] ? "- " . $i_UserNickName . "<br>" : "";
$personal_info .= $data['CanUpdatePhoto_'.$thisUserType] ? "- " . $Lang['Personal']['OfficialPhoto'] . "<br>" : "";
$personal_info .= $data['CanUpdatePersonalPhoto_'.$thisUserType] ? "- " . $Lang['Personal']['PersonalPhoto'] . "<br>" : "";
$personal_info = $personal_info ? $personal_info : "---";

##### Contact Info
$contact_info .= $data['CanUpdateAddress_'.$thisUserType] ? "- " . $i_UserAddress . "<br>" : "";
$contact_info .= $data['CanUpdateCountry_'.$thisUserType] ? "- " . $i_UserCountry . "<br>" : "";
$contact_info .= $data['CanUpdateEmail_'.$thisUserType] ? "- " . $i_UserEmail . "<br>" : "";
$contact_info .= $data['CanUpdateFax_'.$thisUserType] ? "- " . $i_UserFaxNo . "<br>" : "";
$contact_info .= $data['CanUpdateHomeTel_'.$thisUserType] ? "- " . $i_UserHomeTelNo . "<br>" : "";
$contact_info .= $data['CanUpdateURL_'.$thisUserType] ? "- " . $i_UserURL . "<br>" : "";
$contact_info .= $data['CanUpdateMobile_'.$thisUserType] ? "- " . $i_UserMobileTelNo . "<br>" : "";
$contact_info .= $data['CanUpdateOfficeTel_'.$thisUserType] ? "- " . $i_UserOfficeTelNo . "<br>" : "";
$contact_info = $contact_info ? $contact_info : "---";

##### Display
$display_info .= $data['DisplayAddress_'.$thisUserType] ? "- " . $i_UserAddress . "<br>" : "";
$display_info .= $data['DisplayCountry_'.$thisUserType] ? "- " . $i_UserCountry . "<br>" : "";
$display_info .= $data['DisplayDOB_'.$thisUserType] ? "- " . $i_UserDateOfBirth . "<br>" : "";
$display_info .= $data['DisplayEmail_'.$thisUserType] ? "- " . $i_UserEmail . "<br>" : "";
$display_info .= $data['DisplayFax_'.$thisUserType] ? "- " . $i_UserFaxNo . "<br>" : "";
$display_info .= $data['DisplayHomeTel_'.$thisUserType] ? "- " . $i_UserHomeTelNo . "<br>" : "";
$display_info = $display_info ? $display_info : "---";


?>

<script language="javascript">
<!--
function EditInfo()
{
	$('#EditBtn').attr('style', 'visibility: hidden'); 
	$('.Edit_Hide').attr('style', 'display: none');
	$('.Edit_Show').attr('style', '');
	
	$('#UpdateBtn').attr('style', '');
	$('#cancelbtn').attr('style', '');
	
	click_password();
}		

function ResetInfo()
{
	document.form1.reset();
	$('#EditBtn').attr('style', '');
	$('.Edit_Hide').attr('style', '');
	$('.Edit_Show').attr('style', 'display: none');
	
	$('#UpdateBtn').attr('style', 'display: none');
	$('#cancelbtn').attr('style', 'display: none');
	
	$('.Edit_Show_EnablePasswordPolicy').attr('style', 'display: none');
}

function check_form()
{
	<? if(isset($plugin['sms']) && $plugin['sms']) { ?>
	if(document.form1.mobile.checked)
	{
		if(confirm("<?=$Lang['AccountMgmt']['UpdateMobileWarning']?>"))
		{
			document.form1.submit();
		}
	}
	else
		document.form1.submit();
	<? } else { ?>
		document.form1.submit();
	<? } ?>
}

function click_password()
{
	var update_pass = document.form1.CanUpdatePassword[0].checked;
	if(update_pass==true)
	{
		$('.Edit_Show_EnablePasswordPolicy').attr('style', '');
	}
	else
	{
		$('.Edit_Show_EnablePasswordPolicy').attr('style', 'display: none');
	}
		
}
//-->
</script>
		
<form name="form1" method="get" action="update.php">

<div class="table_board">
	<div class="table_row_tool row_content_tool">
		<?= $linterface->GET_SMALL_BTN($button_edit, "button","javascript:EditInfo();","EditBtn")?>
	</div>
	<p class="spacer"></p>
	<div class="form_sub_title_v30"><em>- <span class="field_title"> <?=$i_adminmenu_sc_user_info_settings?> </span>-</em>
		<p class="spacer"></p> 
	</div>

	<table class="form_table_v30">
	
	<!-- Personal Info //-->
	<tr valign='top'>
		<td class="field_title" nowrap><?=$Lang['AccountMgmt']['UserCanUpdate']?><?=$Lang['AccountMgmt']['PersonalInfo']?></td>
		<td> 
		<span class="Edit_Hide"><?=$personal_info?></span>
		<span class="Edit_Show" style="display:none">
		<input type="checkbox" name="dob" value="1" id="dob" <?=($data['CanUpdateDOB_'.$thisUserType]?"checked":"")?>> <label for="dob"><?=$i_UserDateOfBirth?></label> <br>
		<input type="checkbox" name="gender" value="1" id="gender" <?=($data['CanUpdateGender_'.$thisUserType]?"checked":"")?>> <label for="gender"><?=$i_UserGender?></label> <br>
		<input type="checkbox" name="nickname" value="1" id="nickname" <?=($data['CanUpdateNickName_'.$thisUserType]?"checked":"")?>> <label for="nickname"><?=$i_UserNickName?></label> <br>
		<input type="checkbox" name="photo" value="1" id="photo" <?=($data['CanUpdatePhoto_'.$thisUserType]?"checked":"")?>> <label for="photo"><?=$Lang['Personal']['OfficialPhoto']?></label> <br>
		<input type="checkbox" name="personalphoto" value="1" id="personalphoto" <?=($data['CanUpdatePersonalPhoto_'.$thisUserType]?"checked":"")?>> <label for="personalphoto"><?=$Lang['Personal']['PersonalPhoto']?></label> <br>
		</span></td>
	</tr>
	
	<!-- Contact Info //-->
	<tr valign='top'>
		<td class="field_title" nowrap><?=$Lang['AccountMgmt']['UserCanUpdate']?><?=$Lang['AccountMgmt']['ContactInfo']?></td>
		<td> 
		<span class="Edit_Hide"><?=$contact_info?></span>
		<span class="Edit_Show" style="display:none">
		<input type="checkbox" name="address" value="1" id="address" <?=($data['CanUpdateAddress_'.$thisUserType]?"checked":"")?>> <label for="address"><?=$i_UserAddress?></label> <br>
		<input type="checkbox" name="country" value="1" id="country" <?=($data['CanUpdateCountry_'.$thisUserType]?"checked":"")?>> <label for="country"><?=$i_UserCountry?></label> <br>
		<input type="checkbox" name="email" value="1" id="email" <?=($data['CanUpdateEmail_'.$thisUserType]?"checked":"")?>> <label for="email"><?=$i_UserEmail?></label> <span class="tabletextremark"><?=$Lang['AccountMgmt']['EmailFieldReminder']?></span><br>
		<input type="checkbox" name="fax" value="1" id="fax" <?=($data['CanUpdateFax_'.$thisUserType]?"checked":"")?>> <label for="fax"><?=$i_UserFaxNo?></label> <br>
		<input type="checkbox" name="hometel" value="1" id="hometel" <?=($data['CanUpdateHomeTel_'.$thisUserType]?"checked":"")?>> <label for="hometel"><?=$i_UserHomeTelNo?></label> <br>
		<input type="checkbox" name="url" value="1" id="url" <?=($data['CanUpdateURL_'.$thisUserType]?"checked":"")?>> <label for="url"><?=$i_UserURL?></label> <br>
		<input type="checkbox" name="mobile" value="1" id="mobile" <?=($data['CanUpdateMobile_'.$thisUserType]?"checked":"")?>> <label for="mobile"><?=$i_UserMobileTelNo?></label> <span class="tabletextremark"><?=$Lang['AccountMgmt']['MobileUsage']?></span><br>
		<input type="checkbox" name="officetel" value="1" id="officetel" <?=($data['CanUpdateOfficeTel_'.$thisUserType]?"checked":"")?>> <label for="officetel"><?=$i_UserOfficeTelNo?></label> <br>
		</span></td>
	</tr>
	
	<!-- Message //-->
	<tr valign='top'>
		<td class="field_title" nowrap><?=$Lang['AccountMgmt']['UserCanUpdate']?><?=$i_UserProfileMessage?></td>
		<td> 
		<span class="Edit_Hide"><?=$data['CanUpdateMessage_'.$thisUserType] ? $i_general_yes:$i_general_no?></span>
		<span class="Edit_Show" style="display:none">
		<input type="radio" name="CanUpdateMessage" value="1" <?=$data['CanUpdateMessage_'.$thisUserType] ? "checked":"" ?> id="CanUpdateMessage1"> <label for="CanUpdateMessage1"><?=$i_general_yes?></label> 
		<input type="radio" name="CanUpdateMessage" value="0" <?=$data['CanUpdateMessage_'.$thisUserType] ? "":"checked" ?> id="CanUpdateMessage0"> <label for="CanUpdateMessage0"><?=$i_general_no?></label>
		</span>
		</td>
	</tr>
	
	<!-- Display //-->
	<tr valign='top'>
		<td class="field_title" nowrap><?=$Lang['AccountMgmt']['display']?></td>
		<td> 
		<span class="Edit_Hide"><?=$display_info?></span>
		<span class="Edit_Show" style="display:none">
		<input type="checkbox" name="display_address" value="1" id="display_address" <?=($data['DisplayAddress_'.$thisUserType]?"checked":"")?>> <label for="display_address"><?=$i_UserAddress?></label> <br>
		<input type="checkbox" name="display_country" value="1" id="display_country" <?=($data['DisplayCountry_'.$thisUserType]?"checked":"")?>> <label for="display_country"><?=$i_UserCountry?></label> <br>
		<input type="checkbox" name="display_dob" value="1" id="display_dob" <?=($data['DisplayDOB_'.$thisUserType]?"checked":"")?>> <label for="display_dob"><?=$i_UserDateOfBirth?></label> <br>
		<input type="checkbox" name="display_email" value="1" id="display_email" <?=($data['DisplayEmail_'.$thisUserType]?"checked":"")?>> <label for="display_email"><?=$i_UserEmail?></label> <br>
		<input type="checkbox" name="display_fax" value="1" id="display_fax" <?=($data['DisplayFax_'.$thisUserType]?"checked":"")?>> <label for="display_fax"><?=$i_UserFaxNo?></label> <br>
		<input type="checkbox" name="display_hometel" value="1" id="display_hometel" <?=($data['DisplayHomeTel_'.$thisUserType]?"checked":"")?>> <label for="display_hometel"><?=$i_UserHomeTelNo?></label> <br>
		</span></td>
	</tr>
	
	<!-- Login Password //-->
	<? if($plugin["platform"] == "KIS") {?>
	<tr valign='top'>
		<td class="field_title" nowrap><?=$Lang['AccountMgmt']['UserCanUpdate']?><?=$i_UserProfileLogin?></td>
		<td> 
		<span class="Edit_Hide"><?=$data['CanUpdatePassword_'.$thisUserType] ? $i_general_yes . ($data['EnablePasswordPolicy_'.$thisUserType]? "<br>" . $Lang['AccountMgmt']['EnablePasswordPolicy']:"") :$i_general_no?></span>
		<span class="Edit_Show" style="display:none">
		<input type="radio" name="CanUpdatePassword" value="1" <?=$data['CanUpdatePassword_'.$thisUserType] ? "checked":"" ?> id="CanUpdatePassword1" onClick="click_password()"> <label for="CanUpdatePassword1"><?=$i_general_yes?></label> 
		<input type="radio" name="CanUpdatePassword" value="0" <?=$data['CanUpdatePassword_'.$thisUserType] ? "":"checked" ?> id="CanUpdatePassword0" onClick="click_password()"> <label for="CanUpdatePassword0"><?=$i_general_no?></label>
		</span>
		
		<span class="Edit_Show_EnablePasswordPolicy" style="display:none">
		<br>
		<input type="checkbox" name="EnablePasswordPolicy" value="1" <?=$data['EnablePasswordPolicy_'.$thisUserType] ? "checked":"" ?> id="EnablePasswordPolicy1"> <label for="EnablePasswordPolicy1"><?=$Lang['AccountMgmt']['EnablePasswordPolicy']?></label> 
		</span>
		</td>
	</tr>
	<? } ?>
	
	</table>                        
	
	<div class="edit_bottom_v30">
		<p class="spacer"></p>
		<?= $linterface->GET_ACTION_BTN($button_update, "button","check_form();","UpdateBtn", "style='display: none'") ?>
		<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "ResetInfo();","cancelbtn", "style='display: none'") ?>
		<p class="spacer"></p>
	</div>
</div>

</form>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
