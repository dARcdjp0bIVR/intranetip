<?php
//using: Simon
/*
* *******************************************
*  	2017-11-14 Simon [K130888 - 保良局香港道教聯合會圓玄小學]
* 	- bulk download or one class zip student photo
*
* *******************************************
*/
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");
include_once($PATH_WRT_ROOT.'includes/libfilesystem.php');
		
intranet_auth();
intranet_opendb();

### Temp Assign memory of this page
ini_set("memory_limit", "150M"); 

$laccount = new libaccountmgmt();
$libfs = new libfilesystem();

$thisClassID = $_GET['YearClassID'];

if($thisClassID == "ALL"){
	// Form list start
// 	include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
	$fcm = new form_class_manage();
	$AcademicYearID = Get_Current_Academic_Year_ID();
	$FormList = $fcm->Get_Form_List();
	
	$_TempFolder = $intranet_root."/file/temp/class_photo";
	### create folder to contain the bulk photo folder
	$_TempBulkPhotoFolder = $_TempFolder.'/bulk';
	if (!file_exists($_TempBulkPhotoFolder)) {
		$SuccessArr['CreateTempBulkFolder'] = $libfs->folder_new($_TempBulkPhotoFolder);
	}
	
	for ($i=0; $i< sizeof($FormList); $i++) {
		$ClassList = $fcm->Get_Class_List($AcademicYearID,$FormList[$i]['YearID']);
		for ($j=0; $j< sizeof($ClassList); $j++) {
			$_thisClassID = $ClassList[$j]['YearClassID'];
			$_StudentPhotoInfoAry = $laccount->getStudentListWithPhoto($_thisClassID);

			if(!empty($_StudentPhotoInfoAry) && $_StudentPhotoInfoAry != null){
				
				$_thisClassName = $_StudentPhotoInfoAry[0]['ClassTitleEN'];
				
				### Prepare folder
				
				$_TempPhotoFolder = $_TempFolder.'/'.$_thisClassName;
				
				foreach($_StudentPhotoInfoAry as $_StudentPhoto){

					### Create temp folder
					if (!file_exists($_TempPhotoFolder) && !empty($_StudentPhoto['PhotoLink'])) {
						$SuccessArr['CreateTempPDFFolder'] = $libfs->folder_new($_TempPhotoFolder);
					}
			
					if( !empty($_StudentPhoto['PhotoLink']) && $_StudentPhoto['PhotoLink'] != null ){
						$_thisFilePath = $_StudentPhoto['PhotoLink'];
					}

					$_onlyfilename = end( explode( "/", $_thisFilePath ) );

					if(!empty($_onlyfilename)){
						
						$SuccessArr['FolderContentCopy'] = $libfs->file_copy($intranet_root . $_thisFilePath, $_TempPhotoFolder . '/' . $_onlyfilename);
					}
					
				}
				
				### Temp Photo Folder copy to bulk folder
				$SuccessArr['FolderCopy'] = $libfs->folder_copy($_TempPhotoFolder, $_TempBulkPhotoFolder);

				$SuccessArr['DeleteTempPhotoFiles'] = $libfs->folder_remove_recursive($_TempPhotoFolder);
				
			}
		}
	}
// 	die();
// 	chdir("$_TempFolder");
// 	exec('zip -r bulk2.zip bulk');
// 	unlink($intranet_root."/file/temp/class_photo/bulk.zip");
	
// 	unlink($intranet_root."/file/temp/class_photo/S8B.zip");
// 	die();

	$fileName = 'bulk';
	### Download path
	$downloadZipFile = $fileName. '.zip';

	### Generated zip file
	# 1st param: download file name
	# 2nd param: download zip file path
	# 3rd param: download folder location
	$SuccessArr['ZipBulkPhotoFiles'] = $libfs->file_zip($fileName, $downloadZipFile, $_TempFolder);
	
	### Remove bulk folder
	$SuccessArr['DeleteBulkPhotoFolder'] = $libfs->folder_remove_recursive($_TempBulkPhotoFolder);

	output2browser(get_file_content($_TempBulkPhotoFolder . '.zip'), $downloadZipFile);
	
	die();
	
}else if($thisClassID == ""){
	$_TempFolder = $intranet_root."/file/temp/class_photo";
	
	### create folder to contain the bulk photo folder
	$_TempBulkPhotoFolder = $_TempFolder.'/archive';
	
	$_StudentPhotoInfoAry = $laccount->getArchiveStudentListWithPhoto();
	
	if(!empty($_StudentPhotoInfoAry) && $_StudentPhotoInfoAry != null){
		$_thisClassName = $_StudentPhotoInfoAry[0]['ClassTitleEN'];
		
		### Prepare folder
		$_TempPhotoFolder = $_TempFolder.'/'.$_thisClassName;
		
		foreach($_StudentPhotoInfoAry as $_StudentPhoto){
			### Create temp folder
			if (!file_exists($_TempPhotoFolder) && !empty($_StudentPhoto['PhotoLink'])) {
				$SuccessArr['CreateTempPDFFolder'] = $libfs->folder_new($_TempPhotoFolder);
			}
			
			if( !empty($_StudentPhoto['PhotoLink']) && $_StudentPhoto['PhotoLink'] != null ){
				$_thisFilePath = $_StudentPhoto['PhotoLink'];
			}
			
			$_onlyfilename = end( explode( "/", $_thisFilePath ) );
			
			if(!empty($_onlyfilename)){
				if(file_exists($intranet_root . $_thisFilePath)){
					$SuccessArr['FolderContentCopy'] = $libfs->file_copy($intranet_root . $_thisFilePath, $_TempPhotoFolder . '/' . $_onlyfilename);
				}
			}
		}
		
		### Temp Photo Folder copy to bulk folder
		$SuccessArr['FolderCopy'] = $libfs->folder_copy($_TempPhotoFolder, $_TempBulkPhotoFolder);
		
		$SuccessArr['DeleteTempPhotoFiles'] = $libfs->folder_remove_recursive($_TempPhotoFolder);
	}
// 	die();
	$fileName = 'archive';
	### Download path
	$downloadZipFile = $fileName. '.zip';
	
	### Generated zip file
	# 1st param: download file name
	# 2nd param: download zip file path
	# 3rd param: download folder location
	$SuccessArr['ZipBulkPhotoFiles'] = $libfs->file_zip($fileName, $downloadZipFile, $_TempFolder);
	
	### Remove bulk folder
	$SuccessArr['DeleteBulkPhotoFolder'] = $libfs->folder_remove_recursive($_TempBulkPhotoFolder);
	
	output2browser(get_file_content($_TempBulkPhotoFolder . '.zip'), $downloadZipFile);
	
	die();
	
}else{
	$StudentPhotoInfoAry = $laccount->getStudentListWithPhoto($thisClassID);
	if(!empty($StudentPhotoInfoAry)){
		foreach($StudentPhotoInfoAry as $StudentPhoto){
			if(empty($StudentPhoto['PhotoLink'])){
				echo "<script type='text/javascript'>alert('No such information. Please select the class again.');";
				echo "window.location='photo_list.php?YearClassID=".$thisClassID."';";
				echo "</script>";
			}
		}
	}else{
		echo "<script type='text/javascript'>alert('No such information. Please select the class again.');";
		echo "window.location='photo_list.php?YearClassID=".$thisClassID."';";
		echo "</script>";
	}

	$thisClassName = $StudentPhotoInfoAry[0]['ClassTitleEN'];
	
	### Prepare folder
	$TempFolder = $intranet_root."/file/temp/class_photo";
	
	$TempPhotoFolder = $TempFolder.'/'.$thisClassName;
	
	### Create temp folder
	if (!file_exists($TempPhotoFolder)) {
		$SuccessArr['CreateTempPhotoFolder'] = $libfs->folder_new($TempPhotoFolder);
	}
	
	foreach($StudentPhotoInfoAry as $StudentPhoto){
// 		debug_pr($StudentPhoto['PhotoLink']);
		if(!empty($StudentPhoto['PhotoLink'])){
			$thisFilePath = $StudentPhoto['PhotoLink'];
		}
		
		$onlyfilename = end( explode( "/", $thisFilePath ) );
		
		$SuccessArr['FolderContentCopy'] = $libfs->file_copy($intranet_root . $thisFilePath, $TempPhotoFolder . '/' . $onlyfilename);
		
	}
// 	die();
	$downloadFileName = $thisClassName;
	
	$ZipTempPhotoFolder = $thisClassName . '.zip';
	
	### Download path
	$downloadZipPath = $TempFolder . '/' . $ZipTempPhotoFolder;
	
	### Remove the last generated zip file
	$SuccessArr['DeleteLastGeneratedZipFile'] = $libfs->file_remove($TempFolder.'/'.$ZipTempPhotoFolder);
	
	### Generated zip file
	# 1st param: download file name
	# 2nd param: download zip file path
	# 3rd param: download folder location
	$SuccessArr['ZipPhotoFiles'] = $libfs->file_zip($downloadFileName, $downloadZipPath, $TempFolder);
	
	### Remove the folder
	$SuccessArr['DeleteTempPhotoFiles'] = $libfs->folder_remove_recursive($TempPhotoFolder);
	
	### Output the zip file to browser and let the user download it.
	output2browser(get_file_content($TempFolder.'/'.$ZipTempPhotoFolder), $ZipTempPhotoFolder);
	
	die();
}

?>
