<?php 
// using: 
/**********************************************
 *  Date:	2017-11-27 (Isaac)
 * Details: seperate the export button
 *
 *
 *	Date:	2017-11-15 (Simon) [K130888 - 保良局香港道教聯合會圓玄小學]
 * Details: Add download student photo
 *
 *  Date:	2015-08-17 (Omas)
 * Details:	Add Tab for no class student
 * 
 *  Date:	2013-02-21 (Rita)
 * Details:	add export student without photo btn
 **********************************************/
 
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$laccount = new libaccountmgmt();

$officalPhotoMgmtMember = $laccount->getOfficalPhotoMgmtMemberID();

if(!($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Student"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || in_array($UserID, $officalPhotoMgmtMember))) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}
 

$linterface = new interface_html();

# upload btn
$toolbar .= $linterface->GET_LNK_UPLOAD("upload.php");
$toolbar .= $linterface->GET_LNK_EXPORT("export_student_without_photo.php", $Lang['AccountMgmt']['StudentWithoutPhotoList'], '', '', '', 0);

# Add download student photo
//$subBtnAry = array();
// $subBtnAry[] = array('export_student_without_photo.php', $Lang['AccountMgmt']['StudentWithoutPhotoList']);

// $subBtnAry[] = array('download_student_photo.php?YearClassID=ALL', $Lang['AccountMgmt']['StudentWithPhotoList']);
// $btnAry[] = array('export', 'javascript: void(0);', '', $subBtnAry);
// $toolbar .= $linterface->Get_Content_Tool_By_Array_v30($btnAry);
$toolbar .= $linterface->GET_LNK_EXPORT('download_student_photo.php?YearClassID=ALL', $Lang['AccountMgmt']['StudentWithPhotoList'],'', '', '', 0);

# get photo mgmt table
$photoMgmtTable = $laccount->getPhotoMgmtIndexTable();

# page info
$CurrentPageArr['StudentMgmt'] = 1;
$CurrentPage = "Mgmt_Photo";

# Tag
$curTab = 'withclass';
$TAGS_OBJ[] = array($Lang['General']['StudentWithClass'], 'index.php', $curTab=='withclass');
$TAGS_OBJ[] = array($Lang['General']['StudentWithoutClass'], 'photo_list.php', $curTab=='withoutclass');
$MODULE_OBJ = $laccount->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();

?>


</script>
<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
			<form name="form2" method="post" onSubmit="return false">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td>
							<table border="0" cellspacing="0" cellpadding="2">
								<tr>
									<td><?=$toolbar?></td>
								</tr>
							</table>
						</td>
						<td align="right"></td>
					</tr>
				</table>
			</form>
			<form name="form1" method="get" action="">
			<?=$photoMgmtTable?>
			<br><br>
			</form>
		</td>
	</tr>
</table>

<?			
intranet_closedb();
$linterface->LAYOUT_STOP();		
?>