<?php

# using: 
##################################### Change Log [Start] #####################################################
# 
#
#2017-11-21 Simon: Add print student photo btn [E131184 - 優才(楊殷有娣)書院]
#
# 2017-11-15 Simon: Add export student photo btn [K130888 - 保良局香港道教聯合會圓玄小學]
#
# 2015-08-17 Omas:	Add support to show student without class
#
# 
###################################### Change Log [End] ######################################################

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");

intranet_auth();
intranet_opendb();

$laccount = new libaccountmgmt();

$officalPhotoMgmtMember = $laccount->getOfficalPhotoMgmtMemberID();


if(!($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Student"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || in_array($UserID,$officalPhotoMgmtMember))) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$linterface = new interface_html();

$lclass = new libclass(); 

# upload btn
$toolbar .= $linterface->GET_LNK_UPLOAD("upload.php?YearClassID=$YearClassID");

#check lang
// if($_SESSION['intranet_session_language'] == 'b5'){
// 	$styleType = "style=width:50px;";
// }else{
// 	$styleType = "style=width:110px;";
// }

# Add export student photo btn
if($sys_custom['AccountMgmt']['StudentPhoto']['ExportAllStudentPhoto']){
	//$toolbar .= $linterface->GET_LNK_EXPORT("export_student_with_photo.php?YearClassID=$YearClassID", $Lang['AccountMgmt']['StudentWithPhotoList'], "", "$styleType", '', 0);
	$toolbar .= $linterface->GET_LNK_EXPORT("download_student_photo.php?YearClassID=$YearClassID", $Lang['AccountMgmt']['StudentWithPhotoList'], "", "$styleType", '', 0);;
}

# print student photo btn
$toolbar .= $linterface->GET_LNK_PRINT("#", "", "printStudentPhoto($YearClassID)", "", "", 0);


# Class Selection
if($YearClassID==''){
	$filterbar = '';
}
else{
	$filterbar = $lclass->getSelectClassID(' id="YearClassID" name="YearClassID" onchange="this.form.submit();"',$YearClassID);
}

# Searchbox for no class student only
if (isset($_POST['keyword'])) {
	$keyword = standardizeFormPostValue($_POST['keyword']);
}
if($YearClassID == ''){
	$htmlAry['searchBox'] = $linterface->Get_Search_Box_Div('keyword', stripslashes($keyword));
}	

#delete btn
$deleteBtn = '<a href="javascript:checkAlert(document.form1,\'UserLogin[]\',\'remove_update.php\',\''.$Lang['AccountMgmt']['ConfirmMsg']['RemoveSelectedPhoto'].'\')" class="tabletool"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_delete.gif" width="12" height="12" border="0" align="absmiddle">'.$button_delete.'</a>';

# Get Student Photo Display Table 
if($YearClassID == ''){
	$StudentPhotoListTable = $laccount->getStudentPhotoDisplayTable($YearClassID,$keyword);
}
else{
	$StudentPhotoListTable = $laccount->getStudentPhotoDisplayTable($YearClassID);
}

$CurrentPageArr['StudentMgmt'] = 1;
$CurrentPage = "Mgmt_Photo";

$PAGE_NAVIGATION[] = array($Lang['AccountMgmt']['ManagePhoto'], "index.php");
if($YearClassID==''){
	$PAGE_NAVIGATION[] = array($Lang['General']['StudentWithoutClass'], "");
	$curTab = 'withoutclass';
}
else{
	$PAGE_NAVIGATION[] = array($lclass->getClassNameByLang($YearClassID), "");
	$curTab = 'withclass';
}

# Tag
$TAGS_OBJ[] = array($Lang['General']['StudentWithClass'], 'index.php', $curTab=='withclass');
$TAGS_OBJ[] = array($Lang['General']['StudentWithoutClass'], 'photo_list.php', $curTab=='withoutclass');
$MODULE_OBJ = $laccount->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();
?>
<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
			<form name="form2" method="post" onSubmit="return false" id="form2">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td>
							<?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?>
							<br><br>
							<table width="100%" border="0" cellspacing="0" cellpadding="2">
								<tr>
									<td><?=$toolbar ?></td>
								</tr>
							</table>
						</td>
						<td align="right"></td>
					</tr>
				</table>
				<input type="hidden" id="YearClassID" name="YearClassID" />
			</form>
			<form name="form1" method="get" action="">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td align="right">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td height="28" align="right" valign="bottom">
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td align="right"><?= $linterface->GET_SYS_MSG($msg) ?></td>
											<td align="right"><?=$htmlAry['searchBox']?></td>
										</tr>
									</table>									
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr class="table-action-bar">
											<td><?=$filterbar?></td>
											<td align="right" valign="bottom">
												<table border="0" cellspacing="0" cellpadding="0">
													<tr>
														<td width="21"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_01.gif" width="21" height="23"></td>
														<td background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_02.gif">
															<table border="0" cellspacing="0" cellpadding="2">
																<tr>
																	<td nowrap><?=$deleteBtn?></td>
																</tr>
															</table>
														</td>
														<td width="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_03.gif" width="6" height="23"></td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>
						<?=$StudentPhotoListTable?>
					</td>
				</tr>
			</table><br>
			</form>
		</td>
	</tr>
</table>
<script>
function check_click(obj)
{
	if(obj.checked)
	{
		if($("[name='UserLogin[]']:not(:checked)").length==0)
			$("#CheckAllPhoto").attr("checked","checked");
	}
	else
	{
		$("#CheckAllPhoto").attr("checked","");
	}
}

function CheckAll(objname)
{
	if($("#CheckAllPhoto").attr("checked"))
	{
		$("[name='"+objname+"']").attr("checked","checked")
	}
	else
	{
		$("[name='"+objname+"']").attr("checked","")
	}
}
function printStudentPhoto(yearClassId)
{
	document.form1.action = "print_student_record.php?YearClassID=" + yearClassId;
	document.form1.target = "_blank";
	
	document.form1.submit();
	document.form1.action = "";
	document.form1.target = "";
}
</script>
<?		
		
$linterface->LAYOUT_STOP();		
?>