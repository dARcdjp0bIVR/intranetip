<?php
// Modifng by : 

###############################################
#
#   Date:   2019-01-23 Anna
#           set larger time limit [#152375]
#
#	Date:	2012-08-01	Ivan
#			added logic to syn user info to library system
#
###############################################

set_time_limit(600);

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."includes/libform.php");
include_once($PATH_WRT_ROOT."includes/libftp.php");
include_once($PATH_WRT_ROOT."includes/libldap.php");
include_once($PATH_WRT_ROOT."includes/libstudentpromotion.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");

intranet_auth();
intranet_opendb();


if(!($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Student"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"])) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$lu = new libuser();

# SchoolNet integration
if ($plugin['SchoolNet'])
{
    $sql = "SELECT UserLogin FROM INTRANET_USER WHERE UserID IN ($list)";
    $schnet_target = $lu->returnVector($sql);
    include_once("../../includes/libschoolnet.php");
    $lschoolnet = new libschoolnet();
    $lschoolnet->removeUser($schnet_target);
}

if($ssoservice["Google"]["Valid"]){
    include_once($PATH_WRT_ROOT."includes/google_api/libgoogleapi.php");
    include_once($PATH_WRT_ROOT."includes/sso/libSSO_db.php");
    include_once($PATH_WRT_ROOT."includes/sso/libGoogleSSO.php");
    $libGoogleSSO = new libGoogleSSO();
    $aggregated_list = explode(',', $list);
    $user_list = $libGoogleSSO->enabledUserForGoogle($lu, $aggregated_list);
    if (count($user_list['activeUsers'])>0 && count($user_list['suspendedUsers'])>0) {
        $users = array_merge($user_list['activeUsers'],$user_list['suspendedUsers']);
    }elseif (count($user_list['activeUsers'])>0) {
        $users = $user_list['activeUsers'];
    }elseif (count($user_list['suspendedUsers'])>0) {
        $users = $user_list['suspendedUsers'];
    }else {
        $users = array();
    }
    $userGroupIDArr = array();
    foreach ($users as $value) {
        $sql2 = 'SELECT `UserLogin` FROM `INTRANET_USER` WHERE `UserID`=\'' . $value. '\';';
        $userlogin = current($lu->returnVector($sql2));
        $sql = "SELECT GroupID FROM INTRANET_USERGROUP WHERE UserID = '".$value."'";
        $userGroupIDArr[$userlogin] = $lu->returnVector($sql);
    }
}

$lu->prepareUserRemoval($list);
$lsp = new libstudentpromotion();

$sql = "SELECT UserID FROM INTRANET_USER WHERE RecordType=".TYPE_STUDENT." AND UserID IN ($list)";
$others = $lu->returnVector($sql);

if (sizeof($others)!=0)
{
    $otherList = implode(",",$others);
    $lsp->addClassHistory($otherList);
    if($ssoservice["Google"]["Valid"]){
        include_once($PATH_WRT_ROOT."includes/google_api/libgoogleapi.php");
        include_once($PATH_WRT_ROOT."includes/sso/libSSO_db.php");
        include_once($PATH_WRT_ROOT."includes/sso/libGoogleSSO.php");
        $libGoogleSSO = new libGoogleSSO();
        $libGoogleSSO->removeUserFromEClassToGoogle($users);
        foreach ($userGroupIDArr as $key => $value) {
            foreach ($value as $GroupID) {
                $libGoogleSSO->removeGroupMemberFromEClassToGoogle($key, $GroupID);
            }
        }
    }	
    $lsp->archiveStudentsInfo($otherList, $YearOfLeft);
    if ($special_feature['alumni'] && $alumni_system_type==1)
    {
        $lu->setToAlumni($otherList, $YearOfLeft);
    }
    else
    {
        $lu->removeUsers($otherList);
		# delete calendar viewer
		$sql = "delete from CALENDAR_CALENDAR_VIEWER where UserID in ($list)";
		$lu->db_db_query($sql);
    }
    $successAry['synUserDataToModules'] = $lu->synUserDataToModules(explode(',', $list));
}

intranet_closedb();
header("Location: index.php?xmsg=DeleteSuccess");
?>