<?php
# modifying by :

/** Change Log
 *
 * 2018-04-16 (Danny ) : Create this page to select year to export xls for alumni
 *
 */

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$linterface 	= new interface_html();

$laccount = new libaccountmgmt();

### Title ###

$Title = $Lang['AccountMgmt']['ExportAlumniAccount'];
$Title = $Lang['StudentMgmt']['ExportForAlumni'];
$CurrentPageArr['StudentMgmt'] = 1;
$CurrentPage = "Mgmt_Account";
$MODULE_OBJ['title'] = $Lang['Header']['Menu']['AlumniAccount'];
$back_url = "archived_student.php?clearCoo=1"; 


# BackBtn
$BackBtn = $linterface->GET_ACTION_BTN($button_back, "button", "goBack()","back"," class='formbutton'  onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"");
$goBackURL = $back_url;


$sql = "
		SELECT 
			DISTINCT(YEAROFLEFT)
		FROM
			INTRANET_ARCHIVE_USER
		WHERE 
			YEAROFLEFT IS NOT NULL
		ORDER BY 
			YEAROFLEFT DESC
		";

$result = $laccount->returnArray($sql);

### prepare option array
$option= array();

$firstYear = "";

foreach ($result as $_result) {
	if ($firstYear == "") {
		$firstYear = $_result['YEAROFLEFT'];
	}
	$option[$_result['YEAROFLEFT']] = array($_result['YEAROFLEFT'],$_result['YEAROFLEFT']) ;
}

$option["null_year"] = array("null_year", $Lang['StudentMgmt']['EmptyYearRecords']);

# get selectbox / checkbox
$default_selected = array($firstYear);
if($sys_custom['UserExpoertWithCheckbox']) 
{
	foreach($option as $id => $thisoption)
	{
		list($val,$label) = $thisoption;
		$checked = in_array($val,$default_selected)?"checked":""; 
		$selectoption .= '<input type="checkbox" name="Fields[]" value="'.$val.'" id="'.$id.'" '.$checked.'> <label for="'.$id.'">'.$label.'</label> <br>';
	}
}
else
	$selectoption = getSelectByArray(array_values($option)," name='Fields[] ' id='Fields' multiple size=10 style='width:400px;'",$default_selected ,0,1);



$PAGE_NAVIGATION[] = array($Title,"");

// $TAGS_OBJ[] = array($Lang['Group']['UserList'], "", 0);

$TAGS_OBJ[] = array($Lang['AccountMgmt']['UserList'],"index.php?clearCoo=1",0);
$TAGS_OBJ[] = array($Lang['AccountMgmt']['StudentNotInClass'],"notInClass.php?clearCoo=1",0);
$TAGS_OBJ[] = array($Lang['AccountMgmt']['ArchivedStudent'],"archived_student.php?clearCoo=1",1);

$MODULE_OBJ = $laccount->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();

?>

<form method="POST" name="form1" id="frm1" action="export_archived_student_for_alumni.php" onsubmit="return CheckForm()">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td colspan="2" align='right'><?=$linterface->GET_SYS_MSG("",$xmsg);?></td>
</tr>
<tr>
	<td colspan="2"><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
</tr>
<tr>
	<td colspan="2">
        <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
            <tr> 
            	<td><br />
					<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">		
						<?=$ycc_cust?>
						<tr>
							<td class="formfieldtitle" align="left"  width="30%"><?=$Lang['AccountMgmt']['ExportFormat']?></td>
							<td class="tabletext">
								<input type="radio" value="0" name="ExportFormat" checked id="SpecificColumn" onclick="disableSelection()"><label for="SpecificColumn"><?=$Lang['StudentMgmt']['ExportSpecificYear']?></label>
								<input type="radio" value="1" name="ExportFormat" id="DefaultFormat" onclick="disableSelection()"><label for="DefaultFormat"><?=$Lang['StudentMgmt']['ExportAll']?></label>
							</td>
						</tr>
						<tr>
							<td class="formfieldtitle" align="left"  width="30%"><?=$Lang['StudentMgmt']['ExportSpecificYear']?></td>
							<td class="tabletext">
								<?php echo $selectoption; ?>
							</td>
						</tr>
					</table>
				</td>
	        </tr>

			<tr>
				<td colspan="2" class="dotline">
					<img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1">
				</td>
			</tr>
			<tr>
				<td colspan="2"><br>
					<span class="tabletextremark">
						<span class="tabletextrequire">
							*
						</span>
						<?=$Lang['StudentMgmt']['MayNotImportToAlumniDirectly']?>
					</span>
				</td>
			</tr>
			<tr>
				<td colspan="2" align="center">
					<br>
					<?=$linterface->GET_ACTION_BTN($button_export, "button", "checksubmit()","export2"," class=\'formbutton\'")?>
					<img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="1" height="1" />
					<?=$BackBtn?>
				</td>
			</tr>	        
	    </table>
	</td>
</tr>
</table>  
<input type=hidden name=filter value=1>
</form>
<br><br>
<script>
function checksubmit(){ 
	var pass = 1;
	
	if(pass) {
		var printdefault = $("input[name='ExportFormat']:checked").val()==1;
		
		if(printdefault)
		{
			$("#default").val(1);

			<?php 
			if(isset($_SESSION['ncs_role']) && $_SESSION['ncs_role']!=""){
			    echo "$('#frm1').attr('action', 'export_archived_student_for_alumni.php')";
			}
			?>			
			document.form1.submit()
		}
		else
		{
			
			if($("select>option:selected").length>0 || $("input:checkbox:checked").length>0)
			{
				$("#default").val(0);
				$('#frm1').attr('action', 'export_archived_student_for_alumni.php')
		    	document.form1.submit()
			}
			else
				alert(globalAlertMsg18);
	
		}
	} else {
		alert(globalAlertMsg18);
	}
}

function goBack() {
	document.form1.action = "<?=$goBackURL?>";
	document.form1.submit();	
}

function disableSelection()
{
	var val = $("input[name='ExportFormat']:checked").val();
	if(val==1)
	{
		$("select#Fields").attr("disabled","disabled");
	}
	else
	{
		$("select#Fields").attr("disabled","");
	}	
}

$(function(){
	disableSelection();	
});
</script>
<?
 $linterface->LAYOUT_STOP();
?>

