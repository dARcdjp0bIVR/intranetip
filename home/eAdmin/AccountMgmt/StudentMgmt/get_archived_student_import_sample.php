<?php
// Editing by Carlos
// Please use UTF-8 萬國碼 
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$isKIS = $_SESSION["platform"]=="KIS";

$lexport = new libexporttext();

$exportColumn = array("UserLogin");
$rows = array();

$rows[0][] = "s100001";

#Class Name
$exportColumn = array_merge($exportColumn, array("ClassName"));
$rows[0][] = "6S";
#Class Number
$exportColumn = array_merge($exportColumn, array("ClassNumber"));
$rows[0][] = "99";

if($special_feature['ava_strn'])
{
	$exportColumn = array_merge($exportColumn, array("STRN"));
	$rows[0][] = "65498765";
}

if(!$isKIS) {
	$exportColumn = array_merge($exportColumn,array("WebSAMSRegNo","HKJApplNo"));
	$rows[0] = array_merge($rows[0], array("#1100186","40000401"));
}

$exportColumn = array_merge($exportColumn, array("DOB"));
$rows[0][] = "1993-01-20";

if($special_feature['ava_hkid'])
{
	$exportColumn = array_merge($exportColumn, array("HKID"));
	$rows[0][] = "A103456(7)";
}
#Year of Left
$exportColumn = array_merge($exportColumn, array("YearOfLeft"));
$rows[0][] = "2016";

$export_content = $lexport->GET_EXPORT_TXT($rows, $exportColumn);	
$filename = "eclass_archived_student_sample.csv";
$lexport->EXPORT_FILE($filename, $export_content);

intranet_closedb();
?>