<?php 
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

if(!($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Student"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $canAccessOfficalPhotoSetting)) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$words = ($_SESSION['intranet_session_language']=='b5')?array('ClassTitleB5','ChineseName'):array('ClassTitleEN','EnglishName');

$db = new libdb();
$sql = 'Select CONCAT(yc.'.$words[0].',"(",ycu.ClassNumber,")") AS ClassNo, u.'.$words[1].' as UserName,u.UserLogin, u.UserID,y.YearName,u.NickName,u.EnglishName  From YEAR_CLASS_USER ycu 
INNER JOIN INTRANET_USER u 
ON ycu.UserID = u.UserID AND ycu.YearClassID="'.$YearClassID.'"
INNER JOIN YEAR_CLASS yc ON ycu.YearClassID=yc.YearClassID
INNER JOIN YEAR y ON yc.YearID=y.YearID';
$stdArr = $db->returnArray($sql);

intranet_closedb();
?>
<html>
<head>
	<title>Print QR code</title>
	<script src="/templates/jquery/jquery-1.3.2.min.js"></script>
	<script src="/templates/polyu_ncs/js/jquery.qrcode.min.js"></script>
	<script>
		$(document).ready(
			function(){
				window.print();
			}	
		);
	</script>
	<style>
	table {
	    font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
	    border-collapse: collapse;
	    width: 100%;
	    page-break-inside:auto; 
	}
	
	table td{
	    border: 1px solid #ddd;
	    padding: 20px;
	    border-top:double;
	    border-bottom:double;
	}
	
	table tr{
		background-color: #ffffff;
		text-align:center;
		page-break-inside:avoid; 
		page-break-after:auto;
		font-size: 16px;
	}
		
	@media print {
	   .print_hide {
	     display:none;
	   }
	}
	
	</style>
	
	<link href="/templates/2009a/css/content_25.css" rel="stylesheet" type="text/css">
	<link href="/templates/2009a/css/content_30.css" rel="stylesheet" type="text/css">
	<link href="/templates/2009a/css/ereportcard.css" rel="stylesheet" type="text/css">
</head>
<body>
	<table>
		<?php
		$cnt = 0;
		foreach($stdArr as $std){
			if($cnt % 2 == 0){
				echo "<tr>";
			}
			$qrText = ($std['NickName']!="")?"{\"id\":\"".$std['UserLogin']."\",\"name\":\"".$std['NickName']."\"}":"{\"id\":\"".$std['UserLogin']."\",\"name\":\"".$std['EnglishName']."\"}";
			echo "	<td id='user_photo_qr_".$std['UserID']."' style='border-left:double;'></td>";
			echo "<script>
											$('#user_photo_qr_".$std['UserID']."').qrcode({
												width	: 125, 
												height	: 125,
												correctLevel : 0,
												text	: '".$qrText."'
											});
										</script>";
			echo "	<td style='border-right: double'>".$std['YearName']."<br><br>".$std['ClassNo']."<br><br>".$std['UserName']."</td>";
			if($cnt % 2 == 1){
				echo "</tr>";
			}
			$cnt++;
		}	
		?>
	</table>
	<p class="spacer print_hide"></p>
	<div class="edit_bottom">									
		<input type="button" onclick="window.print();" class="formbutton_v30 print_hide " value="<?=$Lang['Btn']['Print']?>">
		<input type="button" onclick="window.close();" class="formbutton_v30 print_hide " value="<?=$Lang['Btn']['Close']?>">
	</div>
	
</body>
</html>


