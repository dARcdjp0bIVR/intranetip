<?php
# using: fai

############# Change Log
#	Date:	2013-12-12	Fai
#			Add a cust attribute "stayOverNight" with $plugin['medical']
#
#	2013-11-13 (Carlos): $sys_custom['SupplementarySmartCard'] - Add SmartCardID2 and SmartCardID3
#	2013-08-13 (Carlos): hide WEBSAMS number and JUPAS Application number for KIS
#
#	Date:   2012-10-29 	Rita	
#			add Parent List
#
#	Date:	2012-08-30	YatWoon
#			add barcode
#
#	Date :	2012-08-03 Bill Mak
#			Add Fields Nationality, PlaceOfBirth, AdmissionDate
#
#	2012-06-08 (Carlos): added password remark
#
#	Date:	2012-03-14	YatWoon
#			allow admin update user's personal email [Case#2012-0313-1603-23071]
#
#	Date :	2011-12-19	(Yuen)
#			Display student photos
#
#	Date :	2011-06-23	(Fai)
#			ADD Application for JUPAS for student account $HKJApplNo
#
#	2011-03-07	(Henry Chow)
#		Password not display in edit page (related case : #2011-0304-1154-52067)
#		if password is empty => keep unchange ; if password is entered => change 
#
#	Date:	2010-11-19 (Henry Chow)
#			revised "Group"/"Role" display from function getGroupInfoByUserID() & getRoleInfoByUserID()
#
#	Date:	2010-11-05	Marcus
#			Add User Extra Info (Shek Wu eRC Rubrics Cust)
#
#	Date:	2010-08-18	YatWoon
#			Add "Remark" field
#
#################

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libgrouping.php");
include_once($PATH_WRT_ROOT."includes/librole.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($eclass_filepath."/src/includes/php/lib-portfolio.php");


intranet_auth();
intranet_opendb();

if(!$sys_custom['SISUserManagement']){
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

if(!($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Student"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"])) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

//if(sizeof($_POST)==0) 
if(empty($uid))
{
	header("Location: index.php");	
	exit;
}

$isKIS = $_SESSION["platform"]=="KIS";

$laccount = new libaccountmgmt();
$linterface = new interface_html();
$lo = new libgrouping();
$lrole = new librole();

$iportfolio_activated = false;

if($plugin['iPortfolio']){
	$lportfolio = new portfolio();
	$data = $lportfolio->returnAllActivatedStudent();

		for($i=0;$i<sizeof($data);$i++){
			if($uid==$data[$i][0]){
				$iportfolio_activated = true;
				break;
			}
		}

}
//$iportfolio_activated = false;

$userInfo = $laccount->getUserInfoByID($uid);
$userPersonalSetting = $laccount->getUserPersonalSettingByID($uid);

# 1) eRC Rubrics Project (Shek Wu)
# 2) Student Mgmt additional info [2012-0201-1440-47098 : Bliss and Wisdom Charitable Foundation]
if($sys_custom['UserExtraInformation']) {
	$ExtraInfoItemArr = $laccount->Get_User_Extra_Info_Item();
	$ExtraInfoOthersArr = BuildMultiKeyAssoc($ExtraInfoItemArr, "ItemID", "ItemCode", 1);
	if(count($ExtraInfoOthersArr)>0) {
		foreach($ExtraInfoOthersArr as $item_id=>$item_code) {
			if(${'ExtraOthers_'.$item_id}!='') 
				$ExtraOthers[$item_id] = ${'ExtraOthers_'.$item_id};
		}
	}
			
	if($sys_custom['StudentMgmt']['BlissWisdom']) {
		if($HowToKnowBlissWisdom==1) {
			$HowToKnowBlissWisdom .= "::".$FriendName;	
		} else if($HowToKnowBlissWisdom==6) {
			$HowToKnowBlissWisdom.= "::".$HowToKnowOthersField;	
		}
		
		$FieldAry = array('Occupation'=>$Occupation, 'Company'=>$Company, 'PassportNo'=>$PassportNo, 'Passport_ValidDate'=>$Passport_ValidDate, 'HowToKnowBlissWisdom'=>$HowToKnowBlissWisdom);
	}
			
	$UserExtraInfoOption = $laccount->Get_User_Extra_Info_UI($uid,$ExtraInfo,$ExtraOthers,$FieldAry);
}
	
if($fromParent!=1) {
	if($userlogin=="") $userlogin = $userInfo['UserLogin'];
	//if($pwd=="") $pwd = $userInfo['UserPassword'];
	if($email=="") $email = $userInfo['UserEmail'];
	if($status=="") $status = $userInfo['RecordStatus'];
	if($smartcardid=="") $smartcardid = $userInfo['CardID'];
	if($sys_custom['SupplementarySmartCard']){
		if($smartcardid2=="") $smartcardid2 = $userInfo['CardID2'];
		if($smartcardid3=="") $smartcardid3 = $userInfo['CardID3'];
	}
	if($strn=="") $strn = $userInfo['STRN'];
	if($WebSAMSRegNo=="") $WebSAMSRegNo = $userInfo['WebSAMSRegNo'];
	if($HKJApplNo=="") $HKJApplNo = $userInfo['HKJApplNo'];
	if($barcode=="") $barcode = $userInfo['Barcode'];
	if($engname=="") $engname = $userInfo['EnglishName']; $engname = stripslashes($engname);
	if($chiname=="") $chiname = $userInfo['ChineseName']; $chiname = stripslashes($chiname);
	if($nickname=="") $nickname = $userInfo['NickName'];
	if($dob=="") $dob = substr($userInfo['DateOfBirth'],0,10);
	if($dob=="0000-00-00") $dob = "";
	if($gender=="") $gender = $userInfo['Gender'];
	if($hkid=="") $hkid = $userInfo['HKID'];
	if($address=="") $address = $userInfo['Address'];
	if($country=="") $country = $userInfo['Country'];
	if($homePhone=="") $homePhone = $userInfo['HomeTelNo'];
	if($mobilePhone=="") $mobilePhone = $userInfo['MobileTelNo'];
	if($faxPhone=="") $faxPhone = $userInfo['FaxNo'];
	$original_pass = $userInfo['UserPassword'];
	if($remark=="") $remark = $userInfo['Remark'];
	if($Nationality=="") $Nationality = $userPersonalSetting['Nationality'];
	if($PlaceOfBirth=="") $PlaceOfBirth = $userPersonalSetting['PlaceOfBirth'];
	if($AdmissionDate=="") $AdmissionDate = $userPersonalSetting['AdmissionDate'];
	if($AdmissionDate=="0000-00-00") $AdmissionDate = "";
	if($plugin['medical']){
		if($stayOverNight=="") $stayOverNight = $userPersonalSetting['stayOverNight'];
		$stayOverNightIsChecked = ' checked ';
		if($stayOverNight == '' || $stayOverNight == 0){
			$stayOverNightIsChecked = '';
		}
	}
} else {
	$userlogin = $userInfo['UserLogin'];
	//$pwd = $userInfo['UserPassword'];
	$email = $userInfo['UserEmail'];
	$status = $userInfo['RecordStatus'];
	$smartcardid = $userInfo['CardID'];
	if($sys_custom['SupplementarySmartCard']){
		$smartcardid2 = $userInfo['CardID2'];
		$smartcardid3 = $userInfo['CardID3'];
	}
	$strn = $userInfo['STRN'];
	$WebSAMSRegNo = $userInfo['WebSAMSRegNo'];
	$HKJApplNo = $userInfo['HKJApplNo'];
	$barcode = $userInfo['Barcode'];
	$engname = $userInfo['EnglishName'];
	$chiname = $userInfo['ChineseName'];
	$nickname = $userInfo['NickName'];
	$dob = substr($userInfo['DateOfBirth'],0,10);
	if($dob=="0000-00-00") $dob = "";
	$gender = $userInfo['Gender'];
	$hkid = $userInfo['HKID'];
	$address = $userInfo['Address'];
	$country = $userInfo['Country'];
	$homePhone = $userInfo['HomeTelNo'];
	$mobilePhone = $userInfo['MobileTelNo'];
	$faxPhone = $userInfo['FaxNo'];
	$original_pass = $userInfo['UserPassword'];
	$remark = $userInfo['Remark'];
	$Nationality = $userPersonalSetting['Nationality'];
	$PlaceOfBirth = $userPersonalSetting['PlaceOfBirth'];
	$AdmissionDate = $userPersonalSetting['AdmissionDate'];
	if($AdmissionDate=="0000-00-00") $AdmissionDate = "";
	if($plugin['medical']){
		if($stayOverNight=="") $stayOverNight = $userPersonalSetting['stayOverNight'];
		$stayOverNightIsChecked = ' checked ';
		if($stayOverNight == '' || $stayOverNight == 0){
			$stayOverNightIsChecked = '';
		}
	}
}

//IF the $HKJApplNo is 0 , set it to empty
$HKJApplNo = ($HKJApplNo == 0)?'':$HKJApplNo;

# imail gamma
if($plugin['imail_gamma'])
{
	if(!$IMapEmail = $laccount->getIMapUserEmail($uid))
	{
		$userInfo = $laccount->getUserInfoByID($uid);
		$IMapEmail = $userInfo['UserLogin']."@".$SYS_CONFIG['Mail']['UserNameSubfix'];
		$disabled = "checked";
	}	
	else
	{
		$enabled = "checked";
	}
	$DefaultQuota = $laccount->getEmailDefaultQuota(TYPE_STUDENT);
	$EmailQuota = $laccount->getUserImailGammaQuota($uid);
	$EmailQuota = $EmailQuota?$EmailQuota:$DefaultQuota[1];
}

# - class & group info -
# class
$classInfo = $laccount->getClassNameClassNoByUserID($uid);
$classNameNo = ($classInfo['ClassName']) ? ($classInfo['ClassName'].($classInfo['ClassNumber']?"-".$classInfo['ClassNumber']:"")) : "---";
# group
/*
$groupInfo = $laccount->getGroupNameByStudentID($uid, Get_Current_Academic_Year_ID());
$groupList = implode(', ', $groupInfo);
if($groupList=="") $groupList = "---";
*/
$gp = $lo->getGroupInfoByUserID($uid, " AND u.GroupID!=2 AND g.AcademicYearID='".Get_Current_Academic_Year_ID()."'");

if(sizeof($gp)>0) {
	$delim = "";
	for($i=0; $i<sizeof($gp); $i++) {
		$groupList .= $delim."(".$gp[$i][0].") ".$gp[$i][1]."";
		$delim = ", ";
	}	
} else {
	$groupList = "---";
}


# role
/*
$roleInfo = $laccount->getGroupRoleByStudentID($uid, Get_Current_Academic_Year_ID());
if($roleInfo=="") $roleInfo = "---";
*/
$role = $lrole->getRoleInfoByUserID($uid);
if(sizeof($role)>0) {
	$delim = "";
	for($i=0; $i<sizeof($role); $i++) {
		$roleInfo .= $delim.$role[$i][0];
		$delim = ", ";
	}
} else {
	$roleInfo = "---";
}




/*
$countrySelection = "<select name='country' id='country'><option value='' ".(($userInfo['Country']=="") ? "selected" : "").">-- $i_alert_pleaseselect $i_UserCountry --</option>";
for($i=0; $i<sizeof($Lang['Country']); $i++) {
	$countrySelection .= "<option value=\"".$Lang['Country'][$i]."\" ".(($Lang['Country'][$i]==$country)?"selected" : "").">".$Lang['Country'][$i]."</option>";	
}
$countrySelection .= "</select>";
*/
$countrySelection = "<input type='text' name='country' id='country' value='$country'>";

$CurrentPageArr['StudentMgmt'] = 1;
$CurrentPage = "Mgmt_Account";

if(!isset($comeFrom) || $comeFrom=="")
	$comeFrom = "index.php";

$TAGS_OBJ[] = array($Lang['AccountMgmt']['UserList'],"index_sis.php?clearCoo=1",1);
$MODULE_OBJ['title'] = $Lang['Header']['Menu']['StudentAccount'];

# navigation bar
$PAGE_NAVIGATION[] = array($Lang['AccountMgmt']['UserList'], "javascript:goURL('$comeFrom')");
$PAGE_NAVIGATION[] = array($Lang['AccountMgmt']['EditUser'], "");

# step information
$STEPS_OBJ[] = array($Lang['AccountMgmt']['InputStudentDetails'], 1);
$STEPS_OBJ[] = array($Lang['AccountMgmt']['InputGuardianDetail'], 0);

if(isset($error) && $error!="")
	$errorAry = explode(',', $error);

$MODULE_OBJ = $laccount->GET_MODULE_OBJ_ARR();


# parent
//$parentList = implode(', ', $parentInfo);
//if($parentList=="") $parentList = "---";
$parentList = $laccount->getParentListByStudentID($uid);


$li = new libuser($uid);

########## Official Photo
$photo_link = "/images/myaccount_personalinfo/samplephoto.gif";
if ($li->PhotoLink !="")
{
    if (is_file($intranet_root.$li->PhotoLink))
    {
        $photo_link = $li->PhotoLink;
    }
}
########## Personal Photo
$photo_personal_link = "/images/myaccount_personalinfo/samplephoto.gif";
if ($li->PersonalPhotoLink !="")
{
    if (is_file($intranet_root.$li->PersonalPhotoLink))
    {
        $photo_personal_link = $li->PersonalPhotoLink;
   }
}

	
$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$xmsg]);

?>
<script language="javascript">
function goSubmit(urlLink) {
	var obj = document.form1;	
	//if (!check_text(obj.pwd,"<?=$i_alert_pleasefillin," ".$i_UserPassword?>")) return false;
	//if (!check_text(obj.pwd2,"<?=$i_alert_pleasefillin." ".$i_frontpage_myinfo_password_retype?>")) return false;
	if (obj.pwd.value!="" && (obj.pwd.value != obj.pwd2.value))
	{
		alert ("<?=$i_frontpage_myinfo_password_mismatch?>");
		obj.pwd.select();
		return false;
	}
	
	obj.action = urlLink;
	obj.submit();
}

function goFinish() {
	document.form1.action = "new_update.php";
	document.form1.submit();	
}

function goURL(urlLink, id) {
	if(id!=undefined) {
		document.form1.uid.value = id;
		document.form1.fromParent.value = 1;
	}
	document.form1.action = urlLink;
	document.form1.submit();
}

//function goURL(urlLink) {
//	document.form1.action = urlLink;
//	document.form1.submit();
//}
</script>
<form name="form1" method="post" action="">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr><td height="40"><?=$linterface->GET_NAVIGATION($PAGE_NAVIGATION)?></td></tr>
    <tr>
    	<td class="board_menu_closed">
			<table width="99%" border="0" cellspacing="0" cellpadding="0">
				<tr> 
					<td class="main_content">
						<div class="table_board">
							<table class="form_table" width="90%" cellpadding="4" cellspacing="0" border="0">
								<tr>
									<td colspan="3"><em class="form_sep_title"> - <?=$eDiscipline["BasicInfo"]?> -</em></td>
								</tr>
								<tr>
									<td class="formfieldtitle" width="25%"><span class="tabletextrequire">*</span><?=$i_UserLogin?></td>
									<td width="70%">
										<?=$userlogin?>
									</td>
									<td rowspan="8"><?=$Lang['Personal']['OfficialPhoto']?><br /><img src="<?=$photo_link?>" width="100" height="130" />
                        				<p><?=$Lang['Personal']['PersonalPhoto']?><br /><img src="<?=$photo_personal_link?>" width="100" height="130" /></p>
									</td>
								</tr>
								<tr>
									<td class="formfieldtitle" rowspan="3"><span class="tabletextrequire">*</span><?=$i_UserPassword?></td>
									<td><input name="pwd" type="password" id="pwd" class="textboxnum" value="<?=$pwd?>"/></td>
								</tr>
								<tr>
									<td ><input name="pwd2" type="password" id="pwd2" class="textboxnum" value="<?=$pwd?>"/> 
									(<?=$Lang['AccountMgmt']['Retype']?>)        </td>
								</tr>
								<tr>
									<td class="tabletextremark"><?=$Lang['AccountMgmt']['PasswordRemark']?></td>
								</tr>

							</table>
						</div>
						<p class="spacer"></p>
	                        <div class="edit_bottom">									
								<?= $linterface->GET_ACTION_BTN($button_finish, "button", "document.form1.skipStep2.value=1;goSubmit('edit_update.php')")?>
								<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "goURL('$comeFrom')")?>
	                        </div>			
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>

<input type="hidden" name="status" id="status" value="<?=$status?>">
<input type="hidden" name="strn" id="strn" value="<?=$strn?>">
<input type="hidden" name="WebSAMSRegNo" id="WebSAMSRegNo" value="<?=$WebSAMSRegNo?>">
<input type="hidden" name="HKJApplNo" id="HKJApplNo" value="<?=$HKJApplNo?>">
<input type="hidden" name="barcode" id="barcode" value="<?=$barcode?>">
<input type="hidden" name="engname" id="engname" value="<?=intranet_htmlspecialchars(stripslashes($engname))?>">
<input type="hidden" name="chiname" id="chiname" value="<?=intranet_htmlspecialchars(stripslashes($chiname))?>">
<input type="hidden" name="nickname" id="nickname" value="<?=$nickname?>">
<input type="hidden" name="dob" id="dob" value="<?=$dob?>">
<input type="hidden" name="gender" id="gender" value="<?=$gender?>">
<input type="hidden" name="hkid" id="hkid" value="<?=$hkid?>">
<input type="hidden" name="address" id="address" value="<?=$address?>">
<input type="hidden" name="country" id="country" value="<?=$country?>">
<input type="hidden" name="homePhone" id="homePhone" value="<?=$homePhone?>">
<input type="hidden" name="mobilePhone" id="mobilePhone" value="<?=$mobilePhone?>">
<input type="hidden" name="faxPhone" id="faxPhone" value="<?=$faxPhone?>">
<input type="hidden" name="remark" id="remark" value="<?=$remark?>">
<input type="hidden" name="Nationality" id="Nationality" value="<?=$Nationality?>">
<input type="hidden" name="PlaceOfBirth" id="PlaceOfBirth" value="<?=$PlaceOfBirth?>">
<input type="hidden" name="AdmissionDate" id="AdmissionDate" value="<?=$AdmissionDate?>">

<input type="hidden" name="uid" id="uid" value="<?=$uid?>">
<input type="hidden" name="fromParent" id="fromParent" value="">
<input type="hidden" name="comeFrom" id="comeFrom" value="<?=$comeFrom?>">
<input type="hidden" name="skipStep2" id="skipStep2" value="">
<input type="hidden" name="targetClass" id="targetClass" value="<?=$targetClass?>">
<input type="hidden" name="recordstatus" id="recordstatus" value="<?=$recordstatus?>">
<input type="hidden" name="original_pass" id="original_pass" value="<?=$original_pass?>">
<input type="hidden" name="keyword" id="keyword" value="<?=$keyword?>">
</form>
<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>