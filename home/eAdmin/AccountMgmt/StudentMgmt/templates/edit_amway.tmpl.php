<?php
	// using:
/*
 *  2020-03-31 Cameron
 *      - hide smartcardid field for Standard LMS
 *      
 *  2019-05-13 Cameron
 *      - security fix  to prevent sql injection by adding apostrophe 
 */	
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr><td height="40"><?=$linterface->GET_NAVIGATION($PAGE_NAVIGATION)?></td></tr>
    <tr><td height="40"><?=$linterface->GET_STEPS($STEPS_OBJ)?></td></tr>
    <tr>
    	<td class="board_menu_closed">
			<table width="99%" border="0" cellspacing="0" cellpadding="0">
				<tr> 
					<td class="main_content">
						<div class="table_board">
							<table class="form_table" width="90%" cellpadding="4" cellspacing="0" border="0">
								<tr>
									<td colspan="3"><em class="form_sep_title"> - <?=$eDiscipline["BasicInfo"]?> -</em></td>
								</tr>
								<tr>
									<td class="formfieldtitle" width="25%"><span class="tabletextrequire">*</span><?=$i_UserLogin?></td>
									<td width="70%">
										<?=$userlogin?>
									</td>
									<td rowspan="8">
                        				<?=$Lang['Personal']['PersonalPhoto']?><br /><img src="<?=$photo_personal_link?>" width="100" height="130" />
                        				<?=$personal_photo_delete?>
									</td>
								</tr>
								<tr>
									<td class="formfieldtitle" rowspan="3"><span class="tabletextrequire">*</span><?=$i_UserPassword?></td>
									<td><input name="pwd" type="password" id="pwd" class="textboxnum" value="<?=$pwd?>"/></td>
								</tr>
								<tr>
									<td ><input name="pwd2" type="password" id="pwd2" class="textboxnum" value="<?=$pwd?>"/> 
									(<?=$Lang['AccountMgmt']['Retype']?>)        </td>
								</tr>
								<tr>
									<td class="tabletextremark"><?=$Lang['AccountMgmt']['PasswordRemark']?></td>
								</tr>
								<?if($plugin['imail_gamma']) {
									include_once($PATH_WRT_ROOT."includes/imap_gamma.php");
									$IMap = new imap_gamma($skipLogin = 1);
									if($IMap->CheckIdentityPermission(TYPE_STUDENT) )
									{	
										?>
										<!-- imail gamma -->	
										<tr>
											<td class="formfieldtitle"><span class="tabletextrequire">*</span><?=$i_UserEmail?></td>
											<td>
												<input name="EmailStatus" type="radio" id="EmailDisable" value="disable" <?=$disabled?>/><label for="EmailDisable"><?=$Lang['General']['Disabled']?></label><br>
												<input name="EmailStatus" type="radio" id="EmailEnable" value="enable" <?=$enabled?>/><label for="EmailEnable"><?=$Lang['General']['Enabled']?></label> - <span id="IMapUserEmail"><?=$IMapEmail?></span><br>
												<?="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$Lang['Gamma']['Quota']?>: <input name="Quota" type="text" id="Quota" class="textboxnum" maxlength=5 value="<?=$EmailQuota?>"/>MB</div>
											</td>
										</tr>
										<?
									}
								}?>
								<!-- imail -->
								<tr>
									<td class="formfieldtitle"><?=$Lang['Gamma']['UserEmail']?></td>
									<td ><input class="textbox_name" type="text" name="UserEmail" size="30" maxlength="50" value="<?=$userInfo['UserEmail']?>">
									<br /><span class="tabletextremark"><?=$Lang['AccountMgmt']['EmailFieldReminder']?></span></td>
								</tr>
								<tr>
									<td class="formfieldtitle"><span class="tabletextrequire">*</span><?=$i_general_status?></td>
									<td >
										<input type="radio" name="status" id="status1" value="1" <? if($status=="1") echo "checked";?>><label for="status1"><?=$Lang['Status']['Activate']?></label>
										<input type="radio" name="status" id="status0" value="0" <? if($status=="0" || $status=="2") echo "checked";?>><label for="status0"><?=$Lang['Status']['Suspend']?></label>
										<input type="radio" name="status" id="status3" value="3" <? if($status=="3") echo "checked";?>><label for="status3"><?=$Lang['Status']['Left']?></label>
									</td>
								</tr>
<?php if (!$sys_custom['project']['CourseTraining']['StandardLMS']):?>								
								<tr>
									<td class="formfieldtitle"><?=$i_SmartCard_CardID?></td>
									<td >
										<input name="smartcardid" type="text" id="smartcardid" class="textboxnum" value="<?=$smartcardid?>"/>
										<? if(sizeof($errorAry)>0 && in_array($smartcardid, $errorAry)) echo "<font color='#FF0000'>".$i_SmartCard_CardID.$Lang['AccountMgmt']['UsedByOtherUser']."</font>"; ?>
									</td>
								</tr>
<?php endif;?>								
								<tr>
									<td colspan="3"><em class="form_sep_title"> - <?=$i_UserProfilePersonal?> -</em></td>
								</tr>
								<tr>
								<td class="formfieldtitle" rowspan="2"> <?=$i_general_name?></td>
									<td class="sub_row_content" colspan="2">
										<span class="tabletextrequire">*</span>(<?=$ip20_lang_eng?>)
										<input name="engname" type="text" id="engname" class="textbox_name" value="<?=$engname?>"/>
										
									</td>
								</tr>
								<tr>
									<td colspan="2"><span class="sub_row_content">(<?=$Lang['AccountMgmt']['Chi']?>)</span> 
									<input name="chiname" type="text" id="chiname" class="textbox_name" value="<?=$chiname?>"/>
									 </td>
								</tr>
								<tr>
								<td class="formfieldtitle"><?=$i_UserGender?><span class="tabletextrequire">*</span></td>
									<td colspan="2">
										<input type="radio" name="gender" id="genderM" value="M"  <? if($gender=="M") echo "checked"; ?>/>
										<label for="genderM"><?=$i_gender_male?></label>
										<input type="radio" name="gender" id="genderF" value="F" <? if($gender=="F") echo "checked"; ?>/>
										<label for="genderF"><?=$i_gender_female?></label>
									</td>
								</tr>
								<tr>
								<td class="formfieldtitle"><?=$i_UserAddress?></td>
									<td colspan="2">
										<?=$linterface->GET_TEXTAREA("address", $address);?>
										<br />
										<?=$countrySelection?>
									</td>
								</tr>
								<tr>
									<td class="formfieldtitle" rowspan="<?=$sys_custom['eAdmin']['AcctMgmt']['UseFaxForBaptism']?'2':'3'?>"><?=$Lang['AccountMgmt']['Tel']?></td>
									<td colspan="2"><span class="sub_row_content">(<?=$Lang['AccountMgmt']['Home']?>)</span>
									<input name="homePhone" type="text" id="homePhone" class="textboxnum" value="<?=$homePhone?>"/></td>
								</tr>
								<tr>
									<td colspan="2"><span class="sub_row_content">(<?=$Lang['AccountMgmt']['Mobile']?>)</span>
									<input name="mobilePhone" type="text" id="mobilePhone" class="textboxnum" value="<?=$mobilePhone?>"/></td>
								</tr>
								<tr>
									<td colspan="2"><span class="sub_row_content">(<?=$Lang['AccountMgmt']['Fax']?>)</span>
									<input name="faxPhone" type="text" id="faxPhone" class="textboxnum" value="<?=$faxPhone?>"/></td>
								</tr>
								
								<!-- Additional Info //-->
								<tr>
									<td colspan="3"><em class="form_sep_title"> - <?=$Lang['AccountMgmt']['AdditionInfo']?> -</em></td>
								</tr>
								<tr>
								<td class="formfieldtitle"><?=$Lang['General']['Remark']?></td>
									<td colspan="2">
										<?=$linterface->GET_TEXTAREA("remark", $remark);?>
									</td>
								</tr>
								
								<!-- Internet Usage //-->
								<tr>
									<td colspan="3"><em class="form_sep_title">- <?=$Lang['AccountMgmt']['InternetUsage']?> -</em></td>
								</tr>
								<?
								$sql = "SELECT REVERSE(BIN(ACL)) FROM INTRANET_SYSTEM_ACCESS WHERE UserID = '$uid'";
						  		$result = $laccount->returnVector($sql);
								$can_access_iMail = substr($result[0],0,1);
								$can_access_iFolder = substr($result[0],1,1);
						  		?>
								  <?if(!$plugin['imail_gamma']) {?>
								  	<? if(isset($plugin['webmail']) && $plugin['webmail'] && in_array(USERTYPE_STUDENT, $webmail_identity_allowed)) { ?>
									  <tr>
									    <td class="formfieldtitle"><?=$ip20TopMenu['iMail']?></td>
									    <td colspan="2"><input type="checkbox" value="1" name="open_webmail" disabled <? echo ($can_access_iMail)? "checked" : "" ?> />
									      <?=$i_Mail_AllowSendReceiveExternalMail?></td>
									  </tr>
								  	<?}?>
								  <?}?>
								  <? if(isset($plugin['personalfile']) && $plugin['personalfile'] && in_array(USERTYPE_STUDENT,$personalfile_identity_allowed)) { ?>
									  <tr>
									    <td class="formfieldtitle"><?=$ip20TopMenu['iFolder']?></td>
									    <td colspan="2"><input type="checkbox" value="1" name="open_file" disabled <? echo ($can_access_iFolder)? "checked" : "" ?> />
									      <?=$i_Files_OpenAccount?></td>
									  </tr>
								  <? } ?>
								
								<?=$UserExtraInfoOption?>
								<tr>
									<td colspan="3"><em class="form_sep_title"> - <?=$Lang['AccountMgmt']['ClassGroupInfo']?> -</em></td>
								</tr>
								<tr>
									<td class="formfieldtitle"><?=$i_general_class?></td>
									<td colspan="2"><?=$classNameNo?></td>
								</tr>
								<tr>
									<td class="formfieldtitle"><?=$i_adminmenu_group?></td>
									<td colspan="2"><?=$groupList?></td>
								</tr>
								<tr>
									<td class="formfieldtitle"><?=$Lang['Header']['Menu']['Role']?></td>
									<td colspan="2"><?=$roleInfo?></td>
								</tr>
							</table>
						</div>
						<p class="spacer"></p>
	                        <div class="edit_bottom">									
								<?= $linterface->GET_ACTION_BTN($button_finish, "button", "document.form1.skipStep2.value=1;goSubmit('edit_update.php')")?>
								<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "goURL('$comeFrom')")?>
	                        </div>			
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
