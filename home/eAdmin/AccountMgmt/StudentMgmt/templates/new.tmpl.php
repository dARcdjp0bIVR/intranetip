<?php
// using: 
/*
 * 2018-12-31 (Isaac):Added $sys_custom['AccountMgmt']['StudentDOBRequired'] 
 * 2018-09-19 (Anna): Added PrimarySchoolCode
 * 2018-09-03 (Bill): added Graduation Date
 * 2018-01-03 (Carlos): [ip.2.5.9.1.1]: Apply new password criteria remark.
 * 2017-01-13 (HenryHM) [ip.2.5.8.1.1]: $ssoservice["Google"]["Valid"] - add GSuite Logic
 * 2016-08-09 (Carlos): $sys_custom['BIBA_AccountMgmtCust'] - added new text field HouseholdRegister.
 * 2016-01-13 (Carlos): $sys_custom['StudentAccountAdditionalFields'] - added five fields [NonChineseSpeaking], [SpecialEducationNeeds], [PrimarySchool], [University] and [Programme].
 * 2015-12-30 (Carlos): $plugin['radius_server'] - added Wi-Fi access option.
 */
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr><td height="40"><?=$linterface->GET_NAVIGATION($PAGE_NAVIGATION)?></td></tr>
    <tr><td height="40"><?=$linterface->GET_STEPS($STEPS_OBJ)?></td></tr>
    <tr>
    	<td class="board_menu_closed">
			<table width="99%" border="0" cellspacing="0" cellpadding="0">
				<tr> 
					<td class="main_content">
						<div class="table_board">
							<table class="form_table" width="90%" cellpadding="4" cellspacing="0" border="0">
								<tr>
									<td colspan="2"><em class="form_sep_title"> - <?=$eDiscipline["BasicInfo"]?> -</em></td>
								</tr>
								<tr>
									<td class="formfieldtitle" width="30%"><span class="tabletextrequire">*</span><?=$i_UserLogin?></td>
									<td >
										<input name="userlogin" type="text" id="userlogin" class="textboxnum" value="<?=$userlogin?>" maxlength="20" onKeyUp=" updateIMapEmail(this);"/>
										<?/* if(sizeof($errorAry)>0 && in_array($userlogin, $errorAry)) echo "<font color='#FF0000'>".$i_UserLogin.$Lang['AccountMgmt']['UsedByOtherUser']."</font>"; */?>
										<span id="spanChecking"><? if(sizeof($errorAry)>0 && in_array($userlogin, $errorAry)) echo " <font color='#FF0000'>".$i_UserLogin.$Lang['AccountMgmt']['UsedByOtherUser']."</font>"; ?><input type="hidden" name="available" id="available" value="1"></span>
										<span id="spanCheckingImage" style="display:none;"><?=$linterface->Get_Ajax_Loading_Image()?></span>
									</td>
								</tr>
								<tr>
									<td class="formfieldtitle" rowspan="3"><span class="tabletextrequire">*</span><?=$i_UserPassword?></td>
									<td>
										<input name="pwd" type="password" id="pwd" class="textboxnum" value="<?=$pwd?>"/>
										<? if(sizeof($errorAry)>0 && in_array($pwd, $errorAry)) echo "<font color='#FF0000'>".$Lang["Login"]["password_err"]."</font>"; ?>
									</td>
								</tr>
								<tr>
									<td><input name="pwd2" type="password" id="pwd2" class="textboxnum" value="<?=$pwd?>"/> 
									(<?=$Lang['AccountMgmt']['Retype']?>)        </td>
								</tr>
								<tr>
									<td class="tabletextremark"><?=$sys_custom['UseStrongPassword']?str_replace("<!--PASSWORD_LENGTH-->",$PasswordLength,implode("<br />\n",$Lang['AccountMgmt']['PasswordRequirements'])):$Lang['AccountMgmt']['PasswordRemark']?></td>
								</tr>
								<?if($plugin['imail_gamma']) {
									include_once($PATH_WRT_ROOT."includes/imap_gamma.php");
									$IMap = new imap_gamma($skipLogin = 1);
									if($IMap->CheckIdentityPermission(TYPE_STUDENT) )
									{	
										?>
										<!-- imail gamma -->	
										<tr>
											<td class="formfieldtitle"><span class="tabletextrequire">*</span><?=$i_UserEmail?></td>
											<td>
												<input name="EmailStatus" type="radio" id="EmailDisable" value="disable" /><label for="EmailDisable"><?=$Lang['General']['Disabled']?></label><br>
												<input name="EmailStatus" type="radio" id="EmailEnable" value="enable" CHECKED/><label for="EmailEnable"><?=$Lang['General']['Enabled']?></label>,<span id="IMapUserEmail">@<?=$SYS_CONFIG['Mail']['UserNameSubfix']?></span><br>
												<?="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$Lang['Gamma']['Quota']?>:<input name="Quota" type="text" id="Quota" class="textboxnum" maxlength=5 value="<?=$EmailQuota?>"/>MB</div>
											</td>
										</tr>
										<?
									}
								}?>
								<!-- imail -->
								<tr>
									<td class="formfieldtitle"><!--<span class="tabletextrequire">*</span>--><?=$Lang['Gamma']['UserEmail']?></td>
									<td><input name="email" type="text" id="email" class="textbox_name" value="<?=$email?>"/> <span class="tabletextremark"><?=$Lang['AccountMgmt']['EmailFieldReminder']?></span></td>
								</tr>
								
							<?php if($ssoservice["Google"]["Valid"] && $ssoservice["Google"]['mode']['student']){ ?>
							  <tr>
							    <td class="formfieldtitle"><?php echo $Lang['Gamma']['UserGmail']; ?></td>
							    <td>
							    	<div id="div_gmail">
							    	<input type="checkbox" class="is_display_options" />
							    	<div class="options" style="display:none;">
							    	<?php foreach((array)$google_apis as $google_api){ ?>
    									<?php $config_index = $google_api->getConfigIndex(); ?>
    									<?php $gmail_variable = 'gmail_'.$config_index; ?>
    									<input type="radio" name="r_gmail" id="r_gmail_<?php echo $config_index; ?>" <?php echo ($$gmail_variable == '1')? "checked" : "" ?> <?php echo ($total_quota[$config_index]!=='NO_QUOTA' && $remaining_quota[$config_index] <= 0)? "disabled" : "" ?> />
						    			<input type="checkbox" value="1" name="gmail_<?php echo $config_index; ?>" id="gmail_<?php echo $config_index; ?>" <?php echo ($$gmail_variable == '1')? "checked" : "" ?> style="display:none;" />
						    			<label for="r_gmail_<?php echo $config_index; ?>" id="gmail_<?php echo $config_index; ?>_label"><span class="GSuiteUserLogin"></span>@<?php echo $google_api->getDomain();?> <?php if($total_quota[$config_index]!=='NO_QUOTA'){ ?>(<?php echo $total_quota[$config_index] - $remaining_quota[$config_index]; ?>/<?php echo $total_quota[$config_index]; ?>)<?php } ?></label>
						    			<?php
						    			$message = '';
					    				switch($_SESSION["SSO"]["Google"]["Message"]['Config'][$google_api->getConfigIndex()]){
					    					case "ACCOUNT_CREATED":
					    						$message = $Lang['Gamma']['UserGmailAccountExists'];
					    						$_SESSION["SSO"]["Google"]["Message"]['Config'][$google_api->getConfigIndex()]='NO_MESSAGE';
					    						break;
					    					case "NO_MESSAGE":
					    						break;
					    					default:
					    						$message = $_SESSION["SSO"]["Google"]["Message"]['Config'][$google_api->getConfigIndex()];
					    						$_SESSION["SSO"]["Google"]["Message"]['Config'][$google_api->getConfigIndex()]='NO_MESSAGE';
					    						break;
					    				}
						    			?>
						    			<span style="background-color:rgb(255,199,206);color:rgb(156,0,6);"><?php echo ($message==''?'':'&nbsp;&nbsp;&nbsp;').$message.($message==''?'':'&nbsp;&nbsp;&nbsp;'); ?></span>
						    			<br/>
						    		<?php } ?>
						    		</div>
						    		</div>
								</td>
							  </tr>
							  <script>
							  	var obj_is_display_options = $('div#div_gmail .is_display_options');
								var obj_checkboxes = $('div#div_gmail input[type="checkbox"]').not(obj_is_display_options);
								var obj_google_radio_buttons = $('div#div_gmail input[type="radio"]');
								
							  	obj_is_display_options.bind('change',function(){
									var is_checked = $(this).is(":checked");
									obj_checkboxes.attr('checked',false);
									obj_google_radio_buttons.attr('checked',false);
									if(is_checked){
										$('div#div_gmail .options').css({'display':'block'});
									}else{
										$('div#div_gmail .options').css({'display':'none'});
									}
							  	});
								if(obj_checkboxes.length == 1){
									obj_is_display_options.css({'display':'none'});
									$('div#div_gmail .options').css({'display':'block'});
									
									obj_checkboxes.each(function(){
										var id = $(this).attr('id');
										$(this).css({'display':'inline'});
										obj_google_radio_buttons.css('display','none');
										$('#'+id+'_label').css({'display':($(this).is(":checked") == true?'inline':'none')});
									});
									obj_checkboxes.bind('click',function(){
										var id = $(this).attr('id');
										$('#'+id+'_label').css({'display':($(this).is(":checked") == true?'inline':'none')});
									});
								}
								obj_checkboxes.bind('change',function(){
									var is_checked = $(this).is(":checked");
									if(is_checked){
										obj_checkboxes.each(function(){
											$(this).attr('checked',false);
										});
										$(this).attr('checked',true);
									}
								});
								obj_google_radio_buttons.bind('change',function(){
									var id = $(this).attr('id');
									var checkbox_id = id.substring(2);
									$('div#div_gmail #'+checkbox_id).attr('checked',true);
									$('div#div_gmail #'+checkbox_id).trigger('change');
								});
							  </script>
							<?php }	?>
								<tr>
									<td class="formfieldtitle"><span class="tabletextrequire">*</span><?=$i_general_status?></td>
									<td>
										<input type="radio" name="status" id="status1" value="1" <? if($status=="" || $status=="1") echo "checked";?>><label for="status1"><?=$Lang['Status']['Activate']?></label>
										<input type="radio" name="status" id="status0" value="0" <? if($status=="0") echo "checked";?>><label for="status0"><?=$Lang['Status']['Suspend']?></label>
									</td>
								</tr>
								<? if((isset($plugin['attendancestudent']) && $plugin['attendancestudent']) ||(isset($plugin['payment'])&& $plugin['payment']) ||(isset($plugin['eEnrollment'])&& $plugin['eEnrollment'])) { ?>
								<tr>
									<td class="formfieldtitle"><?=$i_SmartCard_CardID?></td>
									<td>
										<input name="smartcardid" type="text" id="smartcardid" class="textboxnum" value="<?=$smartcardid?>"/>
										<? if(sizeof($errorAry)>0 && in_array($smartcardid, $errorAry)) echo "<font color='#FF0000'>".$i_SmartCard_CardID.$Lang['AccountMgmt']['UsedByOtherUser']."</font>"; ?>
									</td>
								</tr>
								<?php if($sys_custom['SupplementarySmartCard']){ ?>
								<tr>
									<td class="formfieldtitle"><?=$Lang['AccountMgmt']['SmartCardID2']?></td>
									<td>
										<input name="smartcardid2" type="text" id="smartcardid2" class="textboxnum" value="<?=$smartcardid2?>"/>
										<? if(sizeof($errorAry)>0 && in_array($smartcardid2, $errorAry)) echo "<font color='#FF0000'>".$i_SmartCard_CardID.$Lang['AccountMgmt']['UsedByOtherUser']."</font>"; ?>
									</td>
								</tr>
								<tr>
									<td class="formfieldtitle"><?=$Lang['AccountMgmt']['SmartCardID3']?></td>
									<td>
										<input name="smartcardid3" type="text" id="smartcardid3" class="textboxnum" value="<?=$smartcardid3?>"/>
										<? if(sizeof($errorAry)>0 && in_array($smartcardid3, $errorAry)) echo "<font color='#FF0000'>".$i_SmartCard_CardID.$Lang['AccountMgmt']['UsedByOtherUser']."</font>"; ?>
									</td>
								</tr>
								<tr>
									<td class="formfieldtitle"><?=$Lang['AccountMgmt']['SmartCardID4']?></td>
									<td>
										<input name="smartcardid4" type="text" id="smartcardid4" class="textboxnum" value="<?=$smartcardid4?>"/>
										<? if(sizeof($errorAry)>0 && in_array($smartcardid4, $errorAry)) echo "<font color='#FF0000'>".$i_SmartCard_CardID.$Lang['AccountMgmt']['UsedByOtherUser']."</font>"; ?>
									</td>
								</tr>
								<?php } ?>
								<? } ?>
								<? if($special_feature['ava_strn']) { ?>
								<tr>
									<td class="formfieldtitle"><?=$i_STRN?></td>
									<td>
										<input name="strn" type="text" id="strn" class="textboxnum" value="<?=$strn?>"/>
										<? if(sizeof($errorAry)>0 && in_array($strn, $errorAry)) echo "<font color='#FF0000'>".$i_STRN.$Lang['AccountMgmt']['UsedByOtherUser']."</font>"; ?>
									</td>
								</tr>
								<? } ?>
								<tr<?=$isKIS?' style="display:none;"':''?>>
									<td class="formfieldtitle"><?=$i_WebSAMS_Registration_No?></td>
									<td>
										<input name="WebSAMSRegNo" type="text" id="WebSAMSRegNo" class="textboxnum" value="<?=$WebSAMSRegNo?>"/> <?=$i_WebSAMSRegNo_Format_Notice?>
										<? if(sizeof($errorAry)>0 && in_array($WebSAMSRegNo, $errorAry)) echo "<font color='#FF0000'>".$i_WebSAMS_Registration_No.$Lang['AccountMgmt']['UsedByOtherUser']."</font>"; ?>
									</td>
								</tr>
								<tr<?=$isKIS?' style="display:none;"':''?>>
									<td class="formfieldtitle"><?=$Lang['AccountMgmt']['HKJApplNo']?></td>
									<td>
										<input name="HKJApplNo" type="text" id="HKJApplNo" class="textboxnum" value="<?=$HKJApplNo?>"/> 
										<? if(sizeof($errorAry)>0 && in_array($HKJApplNo, $errorAry)) echo "<font color='#FF0000'> ".$Lang['AccountMgmt']['HKJApplNo'].$Lang['AccountMgmt']['UsedByOtherUser']."</font>"; ?>
									</td>
								</tr>

								<tr>
									<td class="formfieldtitle"><?=$Lang['AccountMgmt']['Barcode']?></td>
									<td>
										<input name="barcode" type="text" id="barcode" class="textboxnum" value="<?=$barcode?>"/> 
										<? if(sizeof($errorAry)>0 && in_array($barcode, $errorAry)) echo "<font color='#FF0000'> ".$Lang['AccountMgmt']['Barcode'].$Lang['AccountMgmt']['UsedByOtherUser']."</font>"; ?>
									</td>
								</tr>
								
								<tr>
									<td colspan="2"><em class="form_sep_title"> - <?=$i_UserProfilePersonal?> -</em></td>
								</tr>
								<tr>
								<td class="formfieldtitle" rowspan="2"> <?=$i_general_name?></td>
									<td class="sub_row_content">
										<span class="tabletextrequire">*</span>(<?=$ip20_lang_eng?>)
										<input name="engname" type="text" id="engname" class="textbox_name" value="<?=intranet_htmlspecialchars(stripslashes($engname))?>"/>
									</td>
								</tr>
								<tr>
									<td><span class="sub_row_content">(<?=$Lang['AccountMgmt']['Chi']?>)</span>
									<input name="chiname" type="text" id="chiname" class="textbox_name" value="<?=intranet_htmlspecialchars(stripslashes($chiname))?>"/>
									</td>
								</tr>
								<tr>
									<td class="formfieldtitle"><?=$i_UserNickName?></td>
									<td><input name="nickname" type="text" id="nickname" class="textbox_name" value="<?=$nickname?>"/></td>
								</tr>
								<tr>
									<td class="formfieldtitle">
										<?=$profile_dob?>
									<?php if($sys_custom['AccountMgmt']['StudentDOBRequired']){ ?>
										<span class="tabletextrequire">*</span>
									<?php } ?>
									</td>
									<td>
									<input name="dob" type="text" id="dob" value="<?=$dob?>" maxlength="10"/>
									(<?=$Lang['AccountMgmt']['YYYYMMDD']?>)</td>
								</tr>
								<tr>
								<td class="formfieldtitle"><?=$i_UserGender?><span class="tabletextrequire">*</span></td>
									<td>
										<input type="radio" name="gender" id="genderM" value="M"  <? if($gender=="M" || $gender=="") echo "checked"; ?>/>
										<label for="genderM"><?=$i_gender_male?></label>
										<input type="radio" name="gender" id="genderF" value="F" <? if($gender=="F") echo "checked"; ?>/>
										<label for="genderF"><?=$i_gender_female?></label>
									</td>
								</tr>
								<? if($special_feature['ava_hkid']) { ?>
								<tr>
									<td class="formfieldtitle"><?=$i_HKID?></td>
									<td>
										<input name="hkid" type="text" id="hkid" class="textboxnum" value="<?=$hkid?>"/>
										<? if(sizeof($errorAry)>0 && in_array($hkid, $errorAry)) echo "<font color='#FF0000'>".$i_HKID.$Lang['AccountMgmt']['UsedByOtherUser']."</font>"; ?>
									</td>
								</tr>
								<? } ?>
								<tr>
								<td class="formfieldtitle"><?=$i_UserAddress?></td>
									<td>
										<?=$linterface->GET_TEXTAREA("address", $address);?>
										<br />
										<?=$countrySelection?>
									</td>
								</tr>
								<tr>
									<td class="formfieldtitle" rowspan="<?=$sys_custom['eAdmin']['AcctMgmt']['UseFaxForBaptism']?'2':'3'?>"><?=$Lang['AccountMgmt']['Tel']?></td>
									<td><span class="sub_row_content">(<?=$Lang['AccountMgmt']['Home']?>)</span>
									<input name="homePhone" type="text" id="homePhone" class="textboxnum" value="<?=$homePhone?>"/></td>
								</tr>
								<tr>
									<td><span class="sub_row_content">(<?=$Lang['AccountMgmt']['Mobile']?>)</span>
									<input name="mobilePhone" type="text" id="mobilePhone" class="textboxnum" value="<?=$mobilePhone?>"/></td>
								</tr>
<?php
	if ($sys_custom['eAdmin']['AcctMgmt']['UseFaxForBaptism']) {
?>
								<tr>
									<td class="formfieldtitle"><?=$Lang['AccountMgmt']['Fax']?></td>
									<td colspan="2"><input name="faxPhone" type="text" id="faxPhone" class="textboxnum" value="<?=$faxPhone?>" maxlength="10"/>(<?=$Lang['AccountMgmt']['YYYYMMDD']?>)</td>
								</tr>
<?php
	}
	else {
?>
								<tr>
									<td><span class="sub_row_content">(<?=$Lang['AccountMgmt']['Fax']?>)</span>
									<input name="faxPhone" type="text" id="faxPhone" class="textboxnum" value="<?=$faxPhone?>"/></td>
								</tr>
<?php		
	}	
?>								
								
								<!-- Nationality, Birth Place, Admission Date //-->
								<tr>
									<td class="formfieldtitle"><?=$Lang['AccountMgmt']['Nationality']?></td>
									<td colspan="2"><input name="Nationality" type="text" id="Nationality" class="textboxnum" value="<?=$Nationality?>"/></td>
								</tr>
								<tr>
									<td class="formfieldtitle"><?=$Lang['AccountMgmt']['PlaceOfBirth']?></td>
									<td colspan="2"><input name="PlaceOfBirth" type="text" id="PlaceOfBirth" class="textboxnum" value="<?=$PlaceOfBirth?>"/></td>
								</tr>
								<tr>
									<td class="formfieldtitle"><?=$Lang['AccountMgmt']['AdmissionDate']?></td>
									<td colspan="2"><input name="AdmissionDate" type="text" id="AdmissionDate" class="textboxnum" value="<?=$AdmissionDate?>"/>(<?=$Lang['AccountMgmt']['YYYYMMDD']?>)</td>
								</tr>
								<tr>
									<td class="formfieldtitle"><?=$Lang['AccountMgmt']['GraduationDate']?></td>
									<td colspan="2"><input name="GraduationDate" type="text" id="GraduationDate" class="textboxnum" value="<?=$GraduationDate?>"/>(<?=$Lang['AccountMgmt']['YYYYMMDD']?>)</td>
								</tr>
								
								<?php if($plugin['medical']){ ?>
									<tr>
									<td class="formfieldtitle"><?=$Lang["medical"]["stayOverNight"]?></td>
									<td colspan="2">
										<input id="stayOverNight" type="checkbox" value="1" name="stayOverNight" <?= $stayOverNightIsChecked?> />
										<label for="stayOverNight"><?=$Lang['General']['Yes']?></label>
									</td>
									</tr>	

								<?php	}?>
								
								<!-- Additional Info //-->
								<tr>
									<td colspan="2"><em class="form_sep_title"> - <?=$Lang['AccountMgmt']['AdditionInfo']?> -</em></td>
								</tr>
								<?php if(isset($_SESSION['ncs_role'])&&$_SESSION['ncs_role']!=""){?>
								<tr>
									<td class="formfieldtitle"><?=$Lang['AccountMgmt']['staffInCharge']?></td>
									<td><?=$linterface->GET_TEXTBOX_NAME("staffInCharge", "staffInCharge", $staffInCharge, '')?></td>
								</tr>
								<tr>
									<td class="formfieldtitle"><?=$Lang['AccountMgmt']['hkDuration']?></td>
									<td><!-- <input id="hkDuration" type="text" value="<?=$hkDuration?>" name="hkDuration"/>-->
										<select name="hkDuration" id="hkDuration">
										<?php 
										for($i=intval(date('Y')); $i >= 1900; $i--){
											$selected = ($i==$hkDuration)?'selected':'';
										?>
											<option value='<?=$i?>' <?=$selected?>><?=$i?></option>
										<?php	
										}
										?>
										</select>
									</td>
								</tr>
								<tr>
									<td class="formfieldtitle"><?=$Lang['AccountMgmt']['Language']?></td>
									<td colspan="2">
										<table>
											<tr>
												<th colspan="2"><?=$Lang['AccountMgmt']['Language']?></th>
											</tr>
											<?php foreach($Lang['AccountMgmt']['LanguagePriority'] as $lang=>$word){ ?>
											<tr>
												<td>
													<?php $checked = ($LangPriority[$lang])?'checked':''; ?>
													<input type="checkbox" id="<?=$lang?>Priority" name="LangPriority[]" value="<?=$lang?>: 1" <?=$checked ?> />
												</td>
												<td>
													<label for="<?=$lang?>Priority"><?=$word?></label>
												</td>
											</tr>	
											<?php } ?>
											
											<tr>
												<td>
													<?php $checked = ($OthersPriority)?'checked':''; ?>
													<input type="checkbox" id="OthersPriority" name="OthersPriority" value="1" <?=$checked ?> />
												</td>
												<td>
													<label for="OthersPriority"><?=$Lang['General']['Others']?>:</label>
													<input id="OthersPriority_details" name="OthersPriority_details" value="<?=$OthersPriority_details ?>" />
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td class="formfieldtitle"><?=$Lang['AccountMgmt']['IsNCS']?></td>
									<td colspan="2">
										<input id="isNCS" type="checkbox" value="1" name="isNCS" <?=($isNCS)?'checked':''?> />
										<label for="isNCS"><?=$Lang['General']['Yes']?></label>
									</td>
								</tr>
								<tr>
									<td class="formfieldtitle"><?=$Lang['AccountMgmt']['NGO']?></td>
									<td><?=$linterface->GET_TEXTBOX_NAME("NGO", "NGO", $NGO, '')?></td>
								</tr>
								<?php } ?>
								<tr>
								<td class="formfieldtitle"><?=$Lang['General']['Remark']?></td>
									<td>
										<?=$linterface->GET_TEXTAREA("remark", $remark);?>
									</td>
								</tr>
								
								<?php if($plugin['StudentDataAnalysisSystem_Style'] == "tungwah"){  ?>	
								<tr>							    
								<td class="formfieldtitle"><?=$Lang['AccountMgmt']['PrimarySchool']?></td>
									<td>
										<?=$linterface->GET_SELECTION_BOX($SchoolNameAry,"id='PrimarySchoolCode' name='PrimarySchoolCode'","");?>
									</td>
								</tr>
								<?php }?>
								
								<?php if($sys_custom['StudentAccountAdditionalFields']){ ?>
								<tr>
									<td class="formfieldtitle"><?=$Lang['AccountMgmt']['NonChineseSpeaking']?></td>
									<td><?=$linterface->Get_Radio_Button("NonChineseSpeakingYes", "NonChineseSpeaking", "Y", $NonChineseSpeaking=="Y"?1:0, "", $Lang['General']['Yes'], "", 0).'&nbsp;'.$linterface->Get_Radio_Button("NonChineseSpeakingNo", "NonChineseSpeaking", "N", $NonChineseSpeaking!="Y"? 1:0, "", $Lang['General']['No'], "", 0)?></td>
								</tr>
								<tr>
									<td class="formfieldtitle"><?=$Lang['AccountMgmt']['SpecialEducationNeeds']?></td>
									<td><?=$linterface->Get_Radio_Button("SpecialEducationNeedsYes", "SpecialEducationNeeds", "Y", $SpecialEducationNeeds=="Y"?1:0, "", $Lang['General']['Yes'], "", 0).'&nbsp;'.$linterface->Get_Radio_Button("SpecialEducationNeedsNo", "SpecialEducationNeeds", "N", $SpecialEducationNeeds!="Y"?1:0, "", $Lang['General']['No'], "", 0)?></td>
								</tr>
								<tr>
									<td class="formfieldtitle"><?=$Lang['AccountMgmt']['PrimarySchool']?></td>
									<td><?=$linterface->GET_TEXTBOX_NAME("PrimarySchool", "PrimarySchool", $PrimarySchool, '')?></td>
								</tr>
								<tr>
									<td class="formfieldtitle"><?=$Lang['AccountMgmt']['UniversityInstitution']?></td>
									<td><?=$linterface->GET_TEXTBOX_NAME("University", "University", $University, '').'&nbsp;('.$Lang['AccountMgmt']['UniversityProgrammeRemark'].')'?></td>
								</tr>
								<tr>
									<td class="formfieldtitle"><?=$Lang['AccountMgmt']['Programme']?></td>
									<td><?=$linterface->GET_TEXTBOX_NAME("Programme", "Programme", $Programme, '').'&nbsp;('.$Lang['AccountMgmt']['UniversityProgrammeRemark'].')'?></td>
								</tr>
								<?php } ?>
								
								<?php if($sys_custom['BIBA_AccountMgmtCust']){ ?>
								<tr>
									<td class="formfieldtitle"><?=$Lang['AccountMgmt']['HouseholdRegister']?></td>
									<td><?=$linterface->GET_TEXTBOX_NAME("HouseholdRegister", "HouseholdRegister", $HouseholdRegister, '')?></td>
								</tr>
								<?php } ?>
														
								<!-- Internet Usage //-->
								
								<tr>
									<td colspan="2"><em class="form_sep_title">- <?=$Lang['AccountMgmt']['InternetUsage']?> -</em></td>
								</tr>
								<?if(!$plugin['imail_gamma']) {?>
						    		<? if(isset($plugin['webmail']) && $plugin['webmail'] && in_array(USERTYPE_STUDENT, $webmail_identity_allowed)) { ?>
										<tr>
											<td class="formfieldtitle"><?=$ip20TopMenu['iMail']?></td>
											<td><input type="checkbox" value="1" name="open_webmail" id="open_webmail" <?if(!isset($open_webmail) || $open_webmail==1) echo "checked";?>/>
								      			<label for="open_webmail"><?=$i_Mail_AllowSendReceiveExternalMail?></label></td>
										</tr>
									<?}?>
								<?}?>
								<? if(isset($plugin['personalfile']) && $plugin['personalfile'] && in_array(USERTYPE_STUDENT,$personalfile_identity_allowed)) { ?>
									<tr>
										<td class="formfieldtitle"><?=$ip20TopMenu['iFolder']?></td>
										<td><input type="checkbox" value="1" name="open_file" id="open_file" <?if(!isset($open_file) || $open_file==1) echo "checked";?>/>
											<label for="open_file"><?=$i_Files_OpenAccount?></label></td>
							  		</tr>
							  	<? } ?>
							  	
							  	<?php if($plugin['radius_server']){ ?>
								  <tr>
								  	<td colspan="3"><em class="form_sep_title">- <?=$Lang['AccountMgmt']['WifiUsage']?> -</em></td>
								  </tr>
								  <tr>
								  	<td class="formfieldtitle"><?=$Lang['AccountMgmt']['EnableWifiAccess']?></td>
								  	<td><input type="checkbox" name="enable_wifi_access" value="1" checked="checked" /></td>
								  </tr>
								  <?php } ?>
							  	
							  	<?=$UserExtraInfoOption?>
							</table>
							
						</div>
						<p class="spacer"></p>
	                        <div class="edit_bottom">									
		                        <?= $linterface->GET_ACTION_BTN($button_continue, "button", "goSubmit('new2.php')")?>
								<?= $linterface->GET_ACTION_BTN($button_finish, "button", "goSubmit('new_update.php')")?>
								<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "goURL('$comeFrom')")?>
	                        </div>			
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
