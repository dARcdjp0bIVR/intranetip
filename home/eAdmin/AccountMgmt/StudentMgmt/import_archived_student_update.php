<?php
// Editing by 
/*
 * 2016-11-30 Carlos: Created
 */
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libimport.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

if(isset($_SESSION['FLASH_VAR_ARCHIVED_STUDENT_IMPORT_RESULT'])){
	$xmsg = $_SESSION['FLASH_VAR_ARCHIVED_STUDENT_IMPORT_RESULT'];
	unset($_SESSION['FLASH_VAR_ARCHIVED_STUDENT_IMPORT_RESULT']);
}

intranet_auth();
intranet_opendb();

if(!($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Student"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"])) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$isKIS = $_SESSION["platform"]=="KIS";

$lf = new libfilesystem();
$laccount = new libaccountmgmt();
$linterface = new interface_html();

$cur_sessionid = md5(session_id());
$sql = "SELECT * FROM INTRANET_ARCHIVE_USER_TEMP_IMPORT WHERE ImportSession='$cur_sessionid'";
$records = $laccount->returnResultSet($sql);
$record_size = count($records);

$successAry = array();
$failAry = array();
for($i=0;$i<$record_size;$i++){
	
	$t_userlogin = $records[$i]['UserLogin'];
	$t_dob = $records[$i]['DOB'];
	$update_fields = "";
	if($special_feature['ava_strn']){
		$t_strn = $records[$i]['STRN'];
		$update_fields .= ",STRN='".$laccount->Get_Safe_Sql_Query($t_strn)."' ";
	}
	if(!$isKIS) {
		$t_websams_number = $records[$i]['WebSAMSRegNo'];
		$update_fields .= ",WebSAMSRegNo='".$laccount->Get_Safe_Sql_Query($t_websams_number)."' ";
		$t_jupas_number = $records[$i]['HKJApplNo'];
		$update_fields .= ",HKJApplNo=".(trim($t_jupas_number)==''?"NULL":"'".$laccount->Get_Safe_Sql_Query($t_jupas_number)."'")." ";
	}
	if($special_feature['ava_hkid']){
		$t_hkid = $records[$i]['HKID'];
		$update_fields .= ",HKID='".$laccount->Get_Safe_Sql_Query($t_hkid)."' ";
	}
	$sql = "UPDATE INTRANET_ARCHIVE_USER SET DateOfBirth=".($t_dob==''?"NULL":"'".$laccount->Get_Safe_Sql_Query($t_dob)."'")." $update_fields WHERE UserLogin='$t_userlogin'";
	$success = $laccount->db_db_query($sql);
	if($success){
		$successAry[] = $t_userlogin;
	}else{
		$failAry[] = $t_userlogin;
	}
	if($i % 10 == 0){
		usleep(1);
	}
}


$CurrentPageArr['StudentMgmt'] = 1;
$CurrentPage = "Mgmt_Account";

$PAGE_NAVIGATION = array();
$PAGE_NAVIGATION[] = array($Lang['AccountMgmt']['ArchivedStudent'],"archived_student.php");
$PAGE_NAVIGATION[] = array($Lang['Btn']['Import'],"");

$STEPS_OBJ[] = array($i_general_select_csv_file, 0);
$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 0);
$STEPS_OBJ[] = array($i_general_imported_result, 1);


$TAGS_OBJ[] = array($Lang['AccountMgmt']['UserList'],"index.php?clearCoo=1",0);
$TAGS_OBJ[] = array($Lang['AccountMgmt']['StudentNotInClass'],"notInClass.php?clearCoo=1",0);
$TAGS_OBJ[] = array($Lang['AccountMgmt']['ArchivedStudent'],"archived_student.php",1);

$MODULE_OBJ['title'] = $Lang['Header']['Menu']['StudentAccount'];
$MODULE_OBJ = $laccount->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$xmsg]);
?>
<form method="POST" name="form1" id="form1" action="" onsubmit="return false;">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td colspan="2"><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
</tr>
<tr>
	<td colspan="2"><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
</tr>
<tr>
	<td colspan="2">
		<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
			<tr>
				<td class="formfieldtitle" width="30%"><?=$Lang['General']['SuccessfulRecord']?></td>
				<td class="tabletext" width="70%"><?=count($successAry)?></td>
			</tr>
			<tr>
				<td class="formfieldtitle" width="30%"><?=$Lang['General']['FailureRecord']?></td>
				<td class="tabletext <?=count($failAry)?"red":""?>" width="70%"><?=count($failAry)?></td>
			</tr>
			<?php 
			if(count($failAry)>0){
				$x = '<tr>';
					$x.= '<td class="formfieldtitle" width="30%">&nbsp;</td>';
					$x.= '<td class="tabletext red" width="70%">';
						$x .= implode("<br />",$failAry);
					$x .= '</td>';
				$x.= '</tr>';
				echo $x;
			}
			?>
		</table>
	</td>
</tr>
<tr>
	<td colspan="2">        
		<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
			<tr>
                <td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
            </tr>
            <tr>
				<td align="center">
					<?=$linterface->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "window.location='import_archived_student.php'","back_btn"," class='formbutton' ")?>				
				</td>
			</tr>
        </table>                                
	</td>
</tr>
</table>
</form>
<br><br>
<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>