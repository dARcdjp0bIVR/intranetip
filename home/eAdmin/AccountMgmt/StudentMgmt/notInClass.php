<?php
# modifying by :  

############ Change Log Start ###############
#   Date:   2018-12-17 Isaac
#           added HomeTelNo, FaxNo and MobileTelNo to the keyword search
#
#   Date:   2018-11-12 Cameron
#           consolidate Amway, Oaks, CEM to use the same customized flag control ($sys_custom['project']['CourseTraining']['IsEnable'])
#
#	Date:	2018-01-10 Carlos - Fixed search keyword with special symbols issue. 
#
#	Date:	2017-12-22 Cameron
#			configure for Oaks refer to Amway
#
#	Date:	2017-10-27 Cameron
#			- don't show [Archived Student] tab for Amway
#			- hide Export function for Amway
#
#	Date:	2017-09-28 Simon
#			add export function for student without class.
#			add javascript function js_Export_student_without_class_List()
#
#	Date:	2016-08-09 Carlos 
#			$sys_custom['BIBA_AccountMgmtCust'] - display House group.
#
#	Date:	2016-06-28 Carlos - Added [Archived Student] tab.
#
#	Date:	2015-12-30 Carlos
#			$plugin['radius_server'] - added table tool buttons for enable/disable Wi-Fi access.
# 
#	Date:	2013-02-07 (YatWoon)
#			update variable, user "user_id[]" instead of "userID[]"
#
#	Date:	2012-02-15 (YatWoon)
#			update query using concat to display hyper link in English name [Case#2012-0215-1355-03073]
#
#	Date:	2012-01-19 (YatWoon)
#			Fixed: Incorrect SQL for search keyword [Case#2012-0119-0931-58089]
#
#	Date:	2012-01-10 (YatWoon)
#			display lastyear and lastclass with group_concat function
#
#	Date:	2011-02-09 Yatwoon
#			can also search with SmartCardID
#
#	Date:	2011-01-10	YatWoon
#			- IP25 UI
#			- Add "Active", "Suspend" tool options
#			- set cookies
#
############ Change Log End ###############

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

# set cookies
$arrCookies[] = array("ck_page_size", "numPerPage");
$arrCookies[] = array("ck_page_no", "pageNo");
$arrCookies[] = array("ck_page_order", "order");
$arrCookies[] = array("ck_page_field", "field");

$arrCookies[] = array("ck_parent_recordstatus", "recordstatus");
$arrCookies[] = array("ck_parent_keyword", "keyword");
if(isset($clearCoo) && $clearCoo == 1)
{
	clearCookies($arrCookies);
} 
else 
{
	updateGetCookies($arrCookies);
}

intranet_auth();
intranet_opendb();

if(!($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Student"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"])) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$laccount = new libaccountmgmt();
$ldiscipline = new libdisciplinev12();
$linterface = new interface_html();

$CurrentPageArr['StudentMgmt'] = 1;
$CurrentPage = "Mgmt_Account";

$TAGS_OBJ[] = array($Lang['AccountMgmt']['UserList'],"index.php?clearCoo=1",0);
$TAGS_OBJ[] = array($Lang['AccountMgmt']['StudentNotInClass'],"notInClass.php?clearCoo=1",1);
if (!$sys_custom['project']['CourseTraining']['IsEnable']) {
	$TAGS_OBJ[] = array($Lang['AccountMgmt']['ArchivedStudent'],"archived_student.php?clearCoo=1",0);
}
$MODULE_OBJ['title'] = $Lang['Header']['Menu']['StudentAccount'];

/*
$toolbar .= $linterface->GET_LNK_NEW("javascript:newUser()",$button_new,"","","",0)."&nbsp;&nbsp;&nbsp;&nbsp;";
$toolbar .= $linterface->GET_LNK_IMPORT("javascript:goImport()",$button_import,"","","",0)."&nbsp;&nbsp;&nbsp;&nbsp;";
$toolbar .= $linterface->GET_LNK_EMAIL("javascript:checkPost(document.form1,'email.php')",$button_email,"","","",0);
*/

// $toolbar .= $linterface->GET_LNK_EXPORT("javascript:goExport()",$button_export,"","","",0)."&nbsp;&nbsp;&nbsp;&nbsp;";

# Record Status 
$recordStatusMenu = "<select name=\"recordstatus\" id=\"recordstatus\" onChange=\"reloadForm()\">";
$recordStatusMenu .= "<option value=''".((!isset($recordstatus) || $recordstatus=="") ? " selected" : "").">$i_Discipline_Detention_All_Status</option>";
$recordStatusMenu .= "<option value='1'".(($recordstatus=='1') ? " selected" : "").">{$Lang['Status']['Active']}</option>";
$recordStatusMenu .= "<option value='0'".(($recordstatus=='0') ? " selected" : "").">{$Lang['Status']['Suspended']}</option>";
$recordStatusMenu .= "<option value='3'".(($recordstatus=='3') ? " selected" : "").">{$Lang['Status']['Left']}</option>";
$recordStatusMenu .= "</select>";

// add export student without class function
## export btn
if (!$sys_custom['project']['CourseTraining']['IsEnable']) {
	$toolbar .= $linterface->GET_LNK_EXPORT("javascript:void(0);", "", "js_Export_not_in_class_List();", "", "", 0);
}

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
if($order == "") $order = 1;
if($field == "") $field = 0;
$pageSizeChangeEnabled = true;

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;

$li = new libdbtable2007($field, $order, $pageNo);

$houseGroupNameField = Get_Lang_Selection('g.TitleChinese','g.Title');

# record status
if(isset($recordstatus) && $recordstatus==1)
	$conds .= " AND USR.RecordStatus=".STATUS_APPROVED;
else if(isset($recordstatus) && $recordstatus==3)
	$conds .= " AND USR.RecordStatus=".STATUS_GRADUATE;
else if(isset($recordstatus) && $recordstatus!=1 && $recordstatus!="")
	$conds .= " AND (USR.RecordStatus=".STATUS_SUSPENDED." OR USR.RecordStatus=".STATUS_PENDING.")";

if(isset($keyword) && $li->isMagicQuotesOn()){
	$keyword = stripslashes($keyword);
}
if($keyword != ""){
	$safe_keyword = htmlentities($li->Get_Safe_Sql_Like_Query($keyword),ENT_QUOTES);
	$conds .= " AND (UserLogin like '%$safe_keyword%' OR
				UserEmail like '%$safe_keyword%' OR
				EnglishName like '%$safe_keyword%' OR
				ChineseName like '%$safe_keyword%' OR 
				ChineseName like '%".$li->Get_Safe_Sql_Like_Query($keyword)."%' OR
				b.ClassName like '%$safe_keyword%' OR
				WebSamsRegNo like '%$safe_keyword%' OR
				CardID like '%$safe_keyword%' OR
				HomeTelNo like '%$safe_keyword%' OR
				FaxNo like '%$safe_keyword%' OR
				MobileTelNo like '%$safe_keyword%'
				)";
}
	//debug_pr($i_status_suspended);
	//debug_pr(TYPE_STUDENT);
$sql = "SELECT 
			USR.UserID
		FROM 
			INTRANET_USER USR LEFT OUTER JOIN
			YEAR_CLASS_USER ycu ON (ycu.UserID=USR.UserID) LEFT OUTER JOIN
			YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID) LEFT OUTER JOIN
			YEAR y ON (y.YearID=yc.YearID)
		WHERE
			RecordType = ".TYPE_STUDENT." AND yc.AcademicYearID=".get_Current_Academic_Year_ID();
$studentInClasses = $laccount->returnVector($sql);

if(sizeof($studentInClasses)>0) $conds .= " AND USR.UserID NOT IN (".implode(',', $studentInClasses).")";

$sql = "SELECT
			concat('<a href=\"javascript:goEdit(', USR.UserID ,');\">',USR.EnglishName,'</a>') as EnglishName,
			IFNULL(IF(ChineseName='', '---', ChineseName),'---') as ChineseName,
			UserLogin,";
if($sys_custom['BIBA_AccountMgmtCust']){
	$sql .= " IFNULL(GROUP_CONCAT(DISTINCT $houseGroupNameField SEPARATOR ', '),'---') as House, ";
}
$sql.= " IFNULL(GROUP_CONCAT(DISTINCT b.AcademicYear ORDER BY b.AcademicYear DESC SEPARATOR '__SPLIT__'), '---') as lastYear,
			IFNULL(GROUP_CONCAT(DISTINCT b.ClassName ORDER BY b.AcademicYear DESC SEPARATOR '__SPLIT__'), '---') as lastClass,          
			IFNULL(LastUsed, '---') as LastUsed,
			CASE USR.RecordStatus
				WHEN 0 THEN '". $Lang['Status']['Suspended'] ."'
				WHEN 1 THEN '". $Lang['Status']['Active'] ."'
				WHEN 3 THEN '". $Lang['Status']['Left'] ."'
				ELSE '$i_status_suspended' END as recordStatus,
			CONCAT('<input type=\'checkbox\' name=\'user_id[]\' id=\'user_id[]\' value=\'', USR.UserID ,'\'>') as checkbox
		FROM 
			INTRANET_USER USR
			left join PROFILE_CLASS_HISTORY as b on (b.UserID = USR.UserID ) ";
if($sys_custom['BIBA_AccountMgmtCust']){
	$sql .= " LEFT JOIN INTRANET_USERGROUP as ug ON ug.UserID=USR.UserID 
			  LEFT JOIN INTRANET_GROUP as g ON g.GroupID=ug.GroupID AND g.RecordType=4 AND g.AcademicYearID='".Get_Current_Academic_Year_ID()."' ";
}
$sql .=	" WHERE 
			USR.RecordType = ".TYPE_STUDENT."
			$conds
		group by USR.UserLogin";
$li->sql = $sql;
$li->field_array = array("USR.EnglishName", "ChineseName", "UserLogin");
if($sys_custom['BIBA_AccountMgmtCust']){
	$li->field_array = array_merge($li->field_array, array("House"));
}
$li->field_array = array_merge($li->field_array,array("lastYear", "lastClass", "LastUsed", "recordStatus"));
$li->no_col = sizeof($li->field_array)+2;
$li->fieldorder2 = ", USR.EnglishName";
$li->IsColOff = "UserMgmtStudentNoClass";

$pos = 0;
$li->column_list .= "<th class='num_check'>#</th>\n";
$li->column_list .= "<th width='15%' >".$li->column($pos++, $i_UserEnglishName)."</th>\n";
$li->column_list .= "<th width='15%' >".$li->column($pos++, $i_UserChineseName)."</th>\n";
$li->column_list .= "<th width='15%' >".$li->column($pos++, $i_UserLogin)."</th>\n";
if($sys_custom['BIBA_AccountMgmtCust']){
	$li->column_list .= "<th width='5%' >".$li->column($pos++, $Lang['AccountMgmt']['House'])."</th>\n";
}
$li->column_list .= "<th width='15%' >".$li->column($pos++, $Lang['AccountMgmt']['LastYear'])."</th>\n"; 
$li->column_list .= "<th width='19%' >".$li->column($pos++, $Lang['AccountMgmt']['LastClass'])."</th>\n"; 
$li->column_list .= "<th width='10%' >".$li->column($pos++, $i_frontpage_eclass_lastlogin)."</th>\n";
$li->column_list .= "<th width='10%' >".$li->column($pos++, $i_UserRecordStatus)."</th>\n";
$li->column_list .= "<th width='1'>".$li->check("user_id[]")."</th>\n";

if($msg==1)
	$msgDisplay = $i_StudentPromotion_Prompt_DataArchived;
	
$MODULE_OBJ = $laccount->GET_MODULE_OBJ_ARR();

//   debug_pr($li->built_sql());

$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$xmsg]);

?>
<script language="javascript">
function reloadForm() {
	document.form1.action = "notInClass.php";
	document.form1.submit();
}

function goEdit(id) {
	document.form1.action = "edit.php";
	document.getElementById('uid').value = id;
	document.form1.submit();
}

function newUser() {
	document.form1.action = "new.php";
	document.form1.submit();
}

function goExport() {
	document.form1.TabID.value = <?=TYPE_STUDENT?>;
	document.form1.action = "../export.php";
	document.form1.submit();
}

function goImport() {
	document.form1.TabID.value = <?=TYPE_STUDENT?>;
	document.form1.action ="../import/import.php";
	document.form1.submit();
}

function changeFormAction(obj,element,url,type) {
	if(countChecked(obj,element)==0)
		alert(globalAlertMsg2);
	else {
		if(confirm(globalAlertMsgSendPassword)) {
			document.form1.action = url;
			document.form1.submit();
		}
	}
}

function confirmArchiveAll(obj, thisURL) {
	if(confirm("<?=$Lang['AccountMgmt']['ConfirmArchiveAllStudent']?>")) {		
		obj.action = thisURL;
		obj.all.value = 1;
		obj.submit();
	}
		
}

function checkLeft(obj,element,url) {
	if(countChecked(obj,element)==0)
		alert(globalAlertMsg2);
	else {
		if(confirm("<?=$Lang['AccountMgmt']['AlertMsg_LeftConfirm']?>")) {
			document.form1.action = url;
			document.form1.submit();
		}
	}
}


function js_Export_not_in_class_List(){
	//var param = "?task=management.substitute_balance.view_print";
	var SubmitPage = "export_notInClass.php";
	//var url = SubmitPage + param;
	var url = SubmitPage;
	document.form1.action = url;
	document.form1.target = '_blank';
	document.form1.submit();
	document.form1.action = '';
	document.form1.target = '';
}

</script>
<form name="form1" method="post" action="">

<div class="content_top_tool">
	<div class="Conntent_tool">
	<?=$toolbar?>
	</div>
	<?=$linterface->Get_Search_Box_Div("keyword", $keyword)?>
<br style="clear:both" />
</div>

<div class="table_board">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr class="table-action-bar">
	<td valign="bottom"><?=$studentTypeMenu?> <?=$recordStatusMenu?></td>
	<td valign="bottom">
	<div class="common_table_tool">
		<? if($plugin['radius_server']){ ?>
        <a href="javascript:checkEditMultiple(document.form1,'user_id[]','../enable_wifi_access.php')" class="tool_approve"><?=$Lang['AccountMgmt']['EnableWifiAccess']?></a>
        <a href="javascript:checkEditMultiple(document.form1,'user_id[]','../disable_wifi_access.php')" class="tool_reject"><?=$Lang['AccountMgmt']['DisableWifiAccess']?></a>
        <? } ?>
		<? if($recordstatus!=1) {?>
		<a href="javascript:checkActivate(document.form1,'user_id[]','../approve.php')" class="tool_approve"><?=$Lang['Status']['Activate']?></a>
		<? } ?>
		<? if($recordstatus=="" || $recordstatus!=0) {?>
		<a href="javascript:checkSuspend(document.form1,'user_id[]','../suspend.php')" class="tool_reject"><?=$Lang['Status']['Suspend']?></a>
		<? } ?>
		<? if($recordstatus!=3) {?>
		<a href="javascript:checkLeft(document.form1,'user_id[]','left.php')" class="tool_other"><?=$Lang['Status']['Left']?></a>
		<? } ?>
		
		<a href="javascript:confirmArchiveAll(document.form1,'archive_confirm.php')" class="tool_other"><?=$Lang['AccountMgmt']['ArchiveAll']?></a> 
		<a href="javascript:checkArchive(document.form1,'user_id[]','archive_confirm.php', '<?=$Lang['AccountMgmt']['ConfirmArchiveSelectedStudent']?>')" class="tool_other"><?=$Lang['AccountMgmt']['ArchiveSelected']?></a> 
	</div>
	</td>
</tr>
</table>
</div>

<?= $li->display()?>

	
<input type="hidden" name="all" id="all" value="" />
<input type="hidden" name="TabID" id="TabID" value="" />
<input type="hidden" name="uid" id="uid" value="" />
<input type="hidden" name="pageNo" id="pageNo" value="<?=$li->pageNo; ?>" />
<input type="hidden" name="order" id="order" value="<?=$li->order; ?>" />
<input type="hidden" name="field" id="field" value="<?=$li->field; ?>" />
<input type="hidden" name="page_size_change" id="page_size_change" value="" />
<input type="hidden" name="numPerPage" id="numPerPage" value="<?=$li->page_size?>" />
<input type="hidden" name="comeFrom" value="/home/eAdmin/AccountMgmt/StudentMgmt/notInClass.php">
<input type="hidden" name="clearCoo" id="clearCoo" value="" />
</form> 
</body>

<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>