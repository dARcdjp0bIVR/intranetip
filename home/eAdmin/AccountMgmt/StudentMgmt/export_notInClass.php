<?php 

# modifying by :

############ Change Log Start ###############
#	Date:	2017-09-28 Simon
#			handle export function for notInClass.
#
############ Change Log End ###############


$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

$lexport = new libexporttext();
$laccount = new libaccountmgmt();

intranet_auth();
intranet_opendb();

if(!($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Student"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"])) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}



//debug_pr($_POST);

# build header array
$headerAry = array();

// build content data array
$dataAry= array();

# get the order and field from $_POST
$order = ($_POST['order'] == '') ? 1 : $_POST['order'];	// 1 => asc, 0 => desc
$field = ($_POST['field'] == '') ? 0 : $_POST['field'];


$colAry = array("USR.EnglishName", "ChineseName", "UserLogin");
if($sys_custom['BIBA_AccountMgmtCust']){
	$colAry = array_merge($colAry, array("House"));
}
$colAry = array_merge($colAry,array("lastYear", "lastClass", "LastUsed", "recordStatus"));

if($order == 1){
	$order_cond = "order by $colAry[$field]  asc, USR.EnglishName";
}else{
	$order_cond = "order by $colAry[$field] desc, USR.EnglishName";
}
//debug_pr($order_cond);
//die();
# get house group name
$houseGroupNameField = Get_Lang_Selection('g.TitleChinese','g.Title');

# record status
// if(isset($recordstatus) && $recordstatus==1)
// 	$conds .= " AND USR.RecordStatus=".STATUS_APPROVED;
// 	else if(isset($recordstatus) && $recordstatus==3)
// 		$conds .= " AND USR.RecordStatus=".STATUS_GRADUATE;
// 		else if(isset($recordstatus) && $recordstatus!=1 && $recordstatus!="")
// 			$conds .= " AND (USR.RecordStatus=".STATUS_SUSPENDED." OR USR.RecordStatus=".STATUS_PENDING.")";
			


$sql = "SELECT
			USR.UserID
		FROM
			INTRANET_USER USR LEFT OUTER JOIN
			YEAR_CLASS_USER ycu ON (ycu.UserID=USR.UserID) LEFT OUTER JOIN
			YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID) LEFT OUTER JOIN
			YEAR y ON (y.YearID=yc.YearID)
		WHERE
			RecordType = ".TYPE_STUDENT." AND yc.AcademicYearID=".get_Current_Academic_Year_ID();
$studentInClasses = $laccount->returnVector($sql);

if(sizeof($studentInClasses)>0) $conds .= " AND USR.UserID NOT IN (".implode(',', $studentInClasses).")";

$sql = "SELECT
			USR.EnglishName,
			IFNULL(IF(ChineseName='', '---', ChineseName),'---') as ChineseName,
			UserLogin,";
if($sys_custom['BIBA_AccountMgmtCust']){
	$sql .= " IFNULL(GROUP_CONCAT(DISTINCT $houseGroupNameField SEPARATOR ', '),'---') as House, ";
}
$sql.= " IFNULL(GROUP_CONCAT(DISTINCT b.AcademicYear ORDER BY b.AcademicYear DESC SEPARATOR '__SPLIT__'), '---') as lastYear,
			IFNULL(GROUP_CONCAT(DISTINCT b.ClassName ORDER BY b.AcademicYear DESC SEPARATOR '__SPLIT__'), '---') as lastClass,
			IFNULL(LastUsed, '---') as LastUsed,
			CASE USR.RecordStatus
				WHEN 0 THEN '". $Lang['Status']['Suspended'] ."'
				WHEN 1 THEN '". $Lang['Status']['Active'] ."'
				WHEN 3 THEN '". $Lang['Status']['Left'] ."'
				ELSE '$i_status_suspended' END as recordStatus
				FROM
				INTRANET_USER USR
				left join PROFILE_CLASS_HISTORY as b on (b.UserID = USR.UserID ) ";
if($sys_custom['BIBA_AccountMgmtCust']){
	$sql .= " LEFT JOIN INTRANET_USERGROUP as ug ON ug.UserID=USR.UserID
			  LEFT JOIN INTRANET_GROUP as g ON g.GroupID=ug.GroupID AND g.RecordType=4 AND g.AcademicYearID='".Get_Current_Academic_Year_ID()."' ";
}
$sql .=	" WHERE
			USR.RecordType = ".TYPE_STUDENT."
			$conds 
			group by USR.UserLogin 
			$order_cond
			";
		
$dataAry = $laccount->returnResultSet($sql);

$pos = 0;			
#Column name
$headerAry[$pos++] = $i_UserEnglishName;
$headerAry[$pos++] = $i_UserChineseName;
$headerAry[$pos++] = $i_UserLogin;
if($sys_custom['BIBA_AccountMgmtCust']){
	$headerAry[$pos++] .= $Lang['AccountMgmt']['House'];
}
$headerAry[$pos++] = $Lang['AccountMgmt']['LastYear'];
$headerAry[$pos++] = $Lang['AccountMgmt']['LastClass'];
$headerAry[$pos++] = $i_frontpage_eclass_lastlogin;
$headerAry[$pos++] = $i_UserRecordStatus;

$index = 0;

if (count($dataAry) > 0){
	foreach ($dataAry as $rData)
	{
		$i = 0;
		$rows[$index][$i++] = $rData["EnglishName"];
		$rows[$index][$i++] = $rData["ChineseName"];
		$rows[$index][$i++] = $rData["UserLogin"];
		if($sys_custom['BIBA_AccountMgmtCust']){
			$rows[$index][$i++] = $rData["House"];
		}
		$rows[$index][$i++] = preg_replace('/__SPLIT__.*/', '', $rData["lastYear"]);
		$rows[$index][$i++] = preg_replace('/__SPLIT__.*/', '', $rData["lastClass"]);
		$rows[$index][$i++] = $rData["LastUsed"];
		$rows[$index][$i++] = $rData["recordStatus"];
		$index++;
	}
}

// debug_pr($rows);
// die();

$exportContent = $lexport->GET_EXPORT_TXT($rows, $headerAry, $Delimiter="", $LineBreak="\r\n", $ColumnDefDelimiter="", $DataSize=0, $Quoted="11", $includeLineBreak=1);
$fileName = 'not_in_class.csv';
$lexport->EXPORT_FILE($fileName, $exportContent, false, false, 'UTF-8');

?>