<?php
/*
 * Using by: 
 * 
 * Change Log:
 * 
 *  Date 2018-11-15 Bill    [2018-1114-1512-04235]
 *  - added English Name and Chinese Name
 *  
 *  Date 2017-03-23 Villa
 *  - add export field
 */

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$isKIS = $_SESSION["platform"]=="KIS";

$li = new libdb();
$lexport = new libexporttext();

$exportColumn = array("UserLogin");
$fields = 'UserLogin';
$rows = array();

# Class Name
$exportColumn = array_merge($exportColumn, array("ClassName"));
$fields .= ",ClassName";

# Class Number
$exportColumn = array_merge($exportColumn, array("ClassNumber"));
$fields .= ",ClassNumber";

# Student Name
$exportColumn = array_merge($exportColumn, array("EnglishName","ChineseName"));
$fields .= ",EnglishName,ChineseName";

if($special_feature['ava_strn']) {
	$exportColumn = array_merge($exportColumn, array("STRN"));
	$fields .= ",STRN";
}

if(!$isKIS) {
	$exportColumn = array_merge($exportColumn,array("WebSAMSRegNo","HKJApplNo"));
	$fields .= ",WebSAMSRegNo,HKJApplNo";
}

$exportColumn = array_merge($exportColumn, array("DOB"));
$fields .= ",IF(DateOfBirth='0000-00-00 00:00:00' OR DateOfBirth IS NULL,'',DATE_FORMAT(DateOfBirth,'%Y-%m-%d')) as DOB";

if($special_feature['ava_hkid']) {
	$exportColumn = array_merge($exportColumn, array("HKID"));
	$fields .= ",HKID";
}

# Year of Left
$exportColumn = array_merge($exportColumn, array("YearOfLeft"));
$fields .= ",YearOfLeft";

$sql = "SELECT 
			$fields 
		FROM INTRANET_ARCHIVE_USER 
		WHERE RecordType='".TYPE_STUDENT."' 
		ORDER BY UserLogin";
$rows = $li->returnArray($sql);

$export_content = $lexport->GET_EXPORT_TXT($rows, $exportColumn);	
$filename = "eclass_archived_student.csv";
$lexport->EXPORT_FILE($filename, $export_content);

intranet_closedb();
?>