<?php
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

//intranet_auth();
intranet_opendb();

$ldb = new libdb();
$linterface = new interface_html();
$lclass = new form_class_manage();

$academicYearId = $_POST['academicYearId'];
$yearTermId = $_POST['yearTermId'];

$html['contentTbl'] = '$academicYearId = '.$academicYearId.'<br />$yearTermId = '.$yearTermId;

# Create Selection box of ClassLevel / Organization
$levelArr = $lclass->Get_Form_List(false,1,$AcademicYearID);
$levelSel = '<select name="printQRcodeClassLevel" id="LevelToPrint">';
$levelSel .= '	<option value="-1"> ----'.$button_select.'---- </option>';
foreach($levelArr as $l){
	$levelSel .= '<option value="'.$l['YearID'].'">'.$l['YearName'].'</option>';
}
$levelSel .= '</select>';


# Create Selection box of Class / Group
$sql = "SELECT yc.YearClassID, ".Get_Lang_Selection("yc.ClassTitleB5","yc.ClassTitleEN")." AS ClassTitle, y.YearID FROM YEAR_CLASS yc LEFT OUTER JOIN YEAR y ON (y.YearID=yc.YearID) WHERE yc.AcademicYearID='".Get_Current_Academic_Year_ID()."' ORDER BY y.YearID";
$classArr = $lclass->returnArray($sql);
$currYearID = $classArr[0]['YearID'];
$classScriptArr = "var classArr = {";
$classScriptArr .= " '".$currYearID."': '";
foreach($classArr as $c){
	if($currYearID != $c['YearID']){
		$classScriptArr .= "',";
		$classScriptArr .= "'".$c['YearID']."': '";	
		$currYearID = $c['YearID'];
	}
	$classScriptArr .= "<option value=\"".$c['YearClassID']."\">".$c['ClassTitle']."</option>";
}
$classScriptArr .= "'};";

### action buttons
$htmlAry['submitBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Print'], "button", "javascript:goPrint();", 'submitBtn_tb', $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");
$htmlAry['cancelBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Cancel'], "button", "js_Hide_ThickBox();", 'cancelBtn_tb', $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");
?>
<div id="thickboxContainerDiv" class="edit_pop_board">
	<form id="thickboxForm" name="thickboxForm">
		<div id="thickboxContentDiv" class="edit_pop_board_write">
			<?=$Lang['AccountMgmt']['Form']?>: <?=$levelSel?><br><br>
			<?=$Lang['AccountMgmt']['Class'] ?>: 
			<select name="printQRcodeClass" id="classToPrint">
				
			</select>
		</div>
		<div id="editBottomDiv" class="edit_bottom_v30">
			<!-- <span> Last updated : yyyy-mm-dd HH:ii:ss by xxxxxxxxxx</span> -->
			<p class="spacer"></p>
			<?=$htmlAry['submitBtn']?>
			<?=$htmlAry['cancelBtn']?>
			<p class="spacer"></p>
		</div>
	</form>
	<script>
		<?=$classScriptArr?>
		$('#LevelToPrint').change(function(){
			var selectedLevel = $('#LevelToPrint').val();
			if(selectedLevel == '-1'){
				$('#classToPrint').html('');
				$('#submitBtn_tb').attr('disabled','disabled');
			}else{
				$('#submitBtn_tb').removeAttr('disabled');
				if(typeof(classArr[selectedLevel]) != 'undefined'){
					$('#classToPrint').html(classArr[selectedLevel]);
				}else{
					$('#classToPrint').html('<option value="-1"><?=$Lang['General']['NoClass']?></option>');
					$('#submitBtn_tb').attr('disabled','disabled');
				}
			}
		});
	</script>
</div>