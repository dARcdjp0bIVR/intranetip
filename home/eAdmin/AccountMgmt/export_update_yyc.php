<?php

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbdump.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

intranet_opendb();

$FolderName = date('Ymd');
$ExportDir1 = $intranet_root."/file/tmp";
$ExportDir = $intranet_root."/file/tmp/".$FolderName;

$LibFilemanager = new libfilesystem();

$LibFilemanager->folder_new($intranet_root."/file/tmp/");
$LibFilemanager->folder_new($ExportDir);

$CSVDir = $ExportDir."/account_info"; 
$LibFilemanager->folder_new($CSVDir);

$CSVDir2 = $ExportDir."/school_info"; 
$LibFilemanager->folder_new($CSVDir2);

/*
///////////////////////////////////////////////////////////////////////////////////////////
// For teacher.csv
///////////////////////////////////////////////////////////////////////////////////////////
$sql = "	SELECT 
				UserLogin, ChineseName, EnglishName, UserPassword
 			FROM 
 				INTRANET_USER 
 			WHERE 
 				RecordType = 1 AND RecordStatus = '1' 
 			ORDER BY 
 				EnglishName
 		";
$li = new libdb();
$row = $li->returnArray($sql);
  
$TeacherContent = "";            		
//$TeacherContent .= "\"Teacher ID\",\"Teacher Chinese Name\",\"Teacher English Name\",\"Teacher Password\""."\n";
for($i=0; $i<sizeof($row); $i++)
 {	 
	 $OrigPass = $row[$i][3];
	 $DisplayPass = base64_encode($OrigPass);
	 $TeacherContent .= "\"".$row[$i][0]."\",\"".$row[$i][1]."\",\"".$row[$i][2]."\",\"".$DisplayPass."\""."\n";
}


$TeacherFile = $CSVDir."/teachers.csv";
$LibFilemanager->file_write($TeacherContent, $TeacherFile);
///////////////////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////////////////
// For student.csv
///////////////////////////////////////////////////////////////////////////////////////////
 $sql = "
 			SELECT 
 				a.UserLogin, b.ClassID, a.ClassNumber, a.ChineseName, a.EnglishName, a.UserPassword
     		FROM 
     			INTRANET_USER a,  INTRANET_CLASS b
     		WHERE 
     			a.RecordType = 2 AND a.RecordStatus = '1' 
     			AND a.ClassName = b.ClassName
			ORDER BY 
				a.ClassName, a.ClassNumber, a.EnglishName
			";     
$li = new libdb();
$row = $li->returnArray($sql);

$StudentContent = "";            		
//$StudentContent .= "\"LoginName\",\"Class\",\"Class Number\",\"Student Chinese Name\",\"Student English Name\",\"Student Password\",\"Academic Year\""."\n";
for($i=0; $i<sizeof($row); $i++)
 {	 
	 $OrigPass = $row[$i][5];
	 $DisplayPass = base64_encode($OrigPass);
	 $CurYear = getCurrentAcademicYear();
	 $StudentContent .= "\"".$row[$i][0]."\",\"".$row[$i][1]."\",\"".$row[$i][2]."\",\"".$row[$i][3]."\",\"".$row[$i][4]."\",\"".$DisplayPass."\",\"{$CurYear}\""."\n";
}

$StudentFile = $CSVDir."/students.csv";
$LibFilemanager->file_write($StudentContent, $StudentFile);
///////////////////////////////////////////////////////////////////////////////////////////
*/

///////////////////////////////////////////////////////////////////////////////////////////
// For form.csv
///////////////////////////////////////////////////////////////////////////////////////////
$FormContent = "";            		
$sql = "
 			SELECT 
 				ClassLevelID,LevelName
     		FROM 
     			INTRANET_CLASSLEVEL
     		WHERE
     			RecordStatus='1'	
			ORDER BY 
				LevelName
			";     
$li = new libdb();
$row = $li->returnArray($sql);

$FormContent = "";
//$FormContent .= "\"Form ID\",\"From Name\""."\n";

for($i=0; $i<sizeof($row); $i++)
 {	 
	 $FormContent .= "\"".$row[$i][0]."\",\"".$row[$i][1]."\""."\n";
}

$FormFile = $CSVDir2."/form.csv";
$LibFilemanager->file_write($FormContent, $FormFile);
///////////////////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////////////////
// For subject.csv
///////////////////////////////////////////////////////////////////////////////////////////
 $sql = "
 			SELECT 
 				SubjectID, SubjectName
     		FROM 
     			INTRANET_SUBJECT
     		WHERE 
     			RecordStatus = '1' 
			ORDER BY 
				SubjectName
			";     
$li = new libdb();
$row = $li->returnArray($sql);

$SubjectContent = "";            		
//$SubjectContent .= "\"Subject ID\",\"Subject Chinese Name\",\"Subject English Name\",\"Details\""."\n";
for($i=0; $i<sizeof($row); $i++)
 {	 
	 $SubjectContent .= "\"".$row[$i][0]."\",\"".$row[$i][1]."\",\"".$row[$i][1]."\",\"".$row[$i][1]."\""."\n";
}

$SubjectFile = $CSVDir2."/subject.csv";
$LibFilemanager->file_write($SubjectContent, $SubjectFile);
///////////////////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////////////////
// For class.csv
///////////////////////////////////////////////////////////////////////////////////////////
 $sql = "
 			SELECT 
 				a.ClassID, b.ClassLevelID,a.ClassName
     		FROM 
     			INTRANET_CLASS a, INTRANET_CLASSLEVEL b
     		WHERE 
     			a.ClassLevelID=b.ClassLevelID
			ORDER BY 
				a.ClassName
			";     
$li = new libdb();
$row = $li->returnArray($sql);

$ClassContent = "";            		
//$ClassContent .= "\"Class ID\",\"Form ID\",\"Class Name\""."\n";
for($i=0; $i<sizeof($row); $i++)
 {	 
	 $ClassContent .= "\"".$row[$i][0]."\",\"".$row[$i][1]."\",\"".$row[$i][2]."\""."\n";
}

$ClassFile = $CSVDir2."/classes.csv";
$LibFilemanager->file_write($ClassContent, $ClassFile);
///////////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////////////////
// For school.csv
/////////////////////////////////////////////////////////////////////////////////////////// 
$SchoolContent = "";            		
//$SchoolContent .= "\"Chinese Name\",\"English Name\""."\n";
$SchoolContent .= "\"".$BroadlearningClientName."\",\"".$BroadlearningClientName."\""."\n";

$SchoolFile = $CSVDir2."/school.csv";
$LibFilemanager->file_write($SchoolContent, $SchoolFile);
///////////////////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////////////////
// Zip file
///////////////////////////////////////////////////////////////////////////////////////////

//chdir($ExportDir);
chdir($CSVDir2);

$ZipFile = "export_".$FolderName.".zip";
exec("rm ../$ZipFile ");
exec("zip -r ../$ZipFile *");

//$ZipFile2 = $ExportDir2 = $ExportDir1."/".$ZipFile;
$ZipFile2 = $ExportDir2 = $ExportDir."/".$ZipFile;
$contentText = file_get_contents($ZipFile2);

if (file_exists($ZipFile2))
{
	$OutputFile = "school_info.zip";
	
	//output2browser("", $OutputFile, "octet-stream", $ZipFile2);
	output2browser($contentText, $OutputFile, "octet-stream");
	
}

//exec("rm -r $ExportDir1");
exec("rm -r $ExportDir");
///////////////////////////////////////////////////////////////////////////////////////////


intranet_closedb();

?>