<?php
# modifying by : 

########## Change Log [Start] #############
#
#	Date:	2014-08-21	Bill
#			Change DateOfBirth to prevent error
#
#	Date:	2014-07-17	Bill
#			Add flag $plugin['eEnrollment'] for eEnrolment
#
########## Change Log [End] #############

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$linterface 	= new interface_html();

$laccount = new libaccountmgmt();

### smartcard
$hasSmartCard = ($plugin['attendancestudent'] || $plugin['payment'] || $plugin['Lunchbox'] || $plugin['eEnrollment']);
$hasTeacherSmartCard = ($module_version['StaffAttendance'] || $plugin['payment'] );

### Title ###

$Title = $Lang['AccountMgmt']['ExportStudentAccount'];
$CurrentPageArr['StudentMgmt'] = 1; 
$CurrentPage = "Mgmt_Account";
$MODULE_OBJ = $laccount->GET_MODULE_OBJ_ARR();
$back_url = "./StudentMgmt/index.php";
$exportOption = "<input type='checkbox' name='userList' id='userList' value='1' checked><label for='userList'>".$Lang['AccountMgmt']['UserList']."</label>";
if(!isset($targetClass) || $targetClass=="0") 
	$exportOption .= "<input type='checkbox' name='notInList' id='notInList' value='1'><label for='notInList'>".$Lang['AccountMgmt']['StudentNotInClass']."</label>";



# BackBtn
$BackBtn = $linterface->GET_ACTION_BTN($button_back, "button", "goBack()","back"," class='formbutton'  onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"");
$goBackURL = $back_url;


### prepare option array
$option= array();
if($TabID==TYPE_STUDENT || $TabID==TYPE_ALUMNI) {
	$option["ClassName"] = array("ClassName",$i_ClassName);
	$option["ClassNumber"] = array("ClassNumber",$i_ClassNumber);
}
$option["UserLogin"] = array("UserLogin",$i_UserLogin);
$option["UserEmail"] = array("UserEmail",$i_UserEmail);
$option["EnglishName"] = array("EnglishName",$i_UserEnglishName);
$option["FirstName"] = array("FirstName",$i_UserFirstName);
$option["LastName"] = array("LastName",$i_UserLastName);
$option["ChineseName"] = array("ChineseName",$i_UserChineseName);
//$option["Title"] = array("IF(Title IS NULL OR TRIM(Title) = '', '', CASE Title WHEN 0 THEN 'Mr.' WHEN 1 THEN 'Miss' WHEN 2 THEN 'Mrs.' WHEN 3 THEN 'Ms.' WHEN 4 THEN 'Dr.' WHEN 5 THEN 'Prof.' ELSE '' END) AS Title ",$i_UserTitle);
$option["Gender"] = array("Gender",$i_UserGender);
$option["MobileTelNo"] = array("MobileTelNo",$i_UserMobileTelNo);
// $option["ClassNumber"] = array("ClassNumber",$i_UserClassNumber);
//$option["DateOfBirth"] = array("IF(DateOfBirth IS NULL OR date_format(DateOfBirth,'%Y-%m-%d') = '0000-00-00', '', DateOfBirth) AS DateOfBirth ",$i_UserDateOfBirth);
$option["DateOfBirth"] = array("DateOfBirth",$i_UserDateOfBirth);
$option["HomeTelNo"] = array("HomeTelNo",$i_UserHomeTelNo);
$option["OfficeTelNo"] = array("OfficeTelNo",$i_UserOfficeTelNo);
$option["FaxNo"] = array("FaxNo",$i_UserFaxNo);
$option["ICQNo"] = array("ICQNo",$i_UserICQNo);
$option["Address"] = array("Address",$i_UserAddress);
$option["Country"] = array("Country",$i_UserCountry);
$option["CardID"] = array("CardID",$i_SmartCard_CardID);
$option["WebSamsRegNo"] = array("WebSamsRegNo",$i_WebSAMS_Registration_No);
$option["HKJApplNo"] = array("HKJApplNo",$Lang['AccountMgmt']['HKJApplNo']);
$option["StaffCode"] = array("StaffCode",$Lang['AccountMgmt']['StaffCode']);

if($special_feature['ava_hkid']) 
{
	$option["HKID"] = array("HKID",$i_HKID);
} 
if($special_feature['ava_strn']) 
{
	$option["STRN"] = array("STRN",$i_STRN);
}


# Extra Info 
if($sys_custom['UserExtraInformation'] ) {
	$ExtraInfoCatInfo = $laccount->Get_User_Extra_Info_Category();
	$totalCategory = count($ExtraInfoCatInfo);
	
	for($i=0; $i<$totalCategory; $i++) {
		$catname = Get_Lang_Selection($ExtraInfoCatInfo[$i]['CategoryNameCh'], $ExtraInfoCatInfo[$i]['CategoryNameEn']);
		$option[$ExtraInfoCatInfo[$i]['CategoryCode']] = array($ExtraInfoCatInfo[$i]['CategoryCode'], $catname);	
	}
}

if($sys_custom['StudentMgmt']['BlissWisdom']) {
	$BlissFieldAry = $Lang['AccountMgmt']['BlissWisdomFieldAry'];
	for($a=0; $a<count($BlissFieldAry); $a++) {
		$option[$BlissFieldAry[$a]] = array($BlissFieldAry[$a], $BlissFieldAry[$a]);
	}	
}

# get selectbox / checkbox
$default_selected = array("ClassName","ClassNumber","UserLogin","UserEmail","EnglishName","ChineseName");
if($sys_custom['UserExpoertWithCheckbox']) 
{
	foreach($option as $id => $thisoption)
	{
		list($val,$label) = $thisoption;
		$checked = in_array($val,$default_selected)?"checked":""; 
		$selectoption .= '<input type="checkbox" name="Fields[]" value="'.$val.'" id="'.$id.'" '.$checked.'> <label for="'.$id.'">'.$label.'</label> <br>';
	}
}
else
	$selectoption = getSelectByArray(array_values($option)," name='Fields[] ' id='Fields' multiple size=10 style='width:400px;'",$default_selected ,0,1);



$PAGE_NAVIGATION[] = array($Title,"");

$TAGS_OBJ[] = array($Lang['Group']['UserList'], "", 0);    

$MODULE_OBJ = $laccount->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();

?>

<form method="POST" name="form1" id="frm1" action="export_update_bliss_wisdom.php" onsubmit="return CheckForm()">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td colspan="2" align='right'><?=$linterface->GET_SYS_MSG("",$xmsg);?></td>
</tr>
<tr>
	<td colspan="2"><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
</tr>
<tr>
	<td colspan="2">
        <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
            <tr> 
            	<td><br />
					<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">		
						<?=$ycc_cust?>
						<tr>
							<td class="formfieldtitle" align="left"  width="30%"><?=$Lang['AccountMgmt']['ExportFormat']?></td>
							<td class="tabletext">
								<input type="radio" value="0" name="ExportFormat" checked id="SpecificColumn" onclick="disableSelection()"><label for="SpecificColumn"><?=$Lang['AccountMgmt']['SpecificColumns']?> </label>
								<input type="radio" value="1" name="ExportFormat" id="DefaultFormat" onclick="disableSelection()"><label for="DefaultFormat"><?=$i_UserDefaultExport?></label>
							</td>
						</tr>
						<tr>
							<td class="formfieldtitle" align="left"  width="30%"><?=$i_export_msg2?> </td>
							<td class="tabletext">
								<?=$selectoption?>
							</td>
						</tr>
						<? if($TabID==TYPE_STUDENT || $TabID==TYPE_PARENT) {?>
						<tr>
							<td class="formfieldtitle" align="left"  width="30%"><?=$i_Discipline_List_View?> </td>
							<td class="tabletext">
								<?=$exportOption?>
							</td>
						</tr>
						<?}?>
					</table>
				</td>
	        </tr>

			<tr>
				<td colspan="2" class="dotline">
					<img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1">
				</td>
			</tr>
			<tr>
				<td colspan="2" align="center">
					<br>
					<span id="printBtn">
						<?=$linterface->GET_ACTION_BTN($button_print, "button", "click_print()","print"," class=\'formbutton\'")?>
						<img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="1" height="1" />
					</span>
					<?=$linterface->GET_ACTION_BTN($button_export, "button", "checksubmit()","export2"," class=\'formbutton\'")?>
					<img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="1" height="1" />
					<?=$linterface->GET_ACTION_BTN($button_reset, "reset", "","resetbtn"," class=\'formbutton\'")?>
					<img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="1" height="1" />
					<?=$BackBtn?>
				</td>
			</tr>	        
	    </table>
	</td>
</tr>
</table>  
<input name="TabID" value="<?=$TabID?>" type="hidden">
<input name="keyword" value="<?=$keyword?>" type="hidden">
<input name="targetClass" value="<?=$targetClass?>" type="hidden">
<input name="recordstatus" value="<?=$recordstatus?>" type="hidden">
<input name="TeachingType" value="<?=$TeachingType?>" type="hidden">
<input type=hidden name=default id=default value=''>
<input type=hidden name=filter value=1>
</form>
<br><br>
<script>
function click_print()
{
	var pass = 1;
	pass = checkExportCheckboxOption();
	
	
	if(pass == 1) {
	    if($("select>option:selected").length>0 || $("input:checkbox:checked").length>0)
	     {
	     	$("#default").val(0);
			document.form1.action='print_bliss_wisdom.php';
			document.form1.target='blank';
			document.form1.submit();
			 	
			document.form1.action='export_update_bliss_wisdom.php';
			document.form1.target='_self';
		}
		else {
			alert(globalAlertMsg18);
		}
	} else {
		alert(globalAlertMsg18);	
	}
	
}

function checkExportCheckboxOption() {
	var a = $('form #userList').is(':checked');
	var b = $('form #notInList').is(':checked');
	
	return (a==false && b==false) ? false : true; 	
}

function checksubmit(){ 
	var pass = 1;
	
	pass = checkExportCheckboxOption();
	
	
	if(pass) {
		var printdefault = $("input[name='ExportFormat']:checked").val()==1;
		if(printdefault)
		{
			$("#default").val(1);
			document.form1.submit()
		}
		else
		{
			if($("select>option:selected").length>0 || $("input:checkbox:checked").length>0)
			{
				$("#default").val(0);
		    	document.form1.submit()
			}
			else
				alert(globalAlertMsg18);
	
		}
	} else {
		alert(globalAlertMsg18);
	}
}

function goBack() {
	document.form1.action = "<?=$goBackURL?>";
	document.form1.submit();	
}

function disableSelection()
{
	var val = $("input[name='ExportFormat']:checked").val();
	if(val==1)
	{
		$("select#Fields").attr("disabled","disabled");
		$("span#printBtn").hide();
	}
	else
	{
		$("span#printBtn").show();
		$("select#Fields").attr("disabled","");
	}	
}

$(function(){
	disableSelection();	
});
</script>
<?
 $linterface->LAYOUT_STOP();
?>

