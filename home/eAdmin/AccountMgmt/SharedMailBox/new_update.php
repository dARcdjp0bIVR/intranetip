<?php
// editing by 
/******************************** Modification Log *******************************************
 * 2018-01-04 (Carlos): Enforce password checking.
 *********************************************************************************************/

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libauth.php");
include_once($PATH_WRT_ROOT."includes/libsharedmailbox.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if(!$_SESSION["SSV_USER_ACCESS"]["eAdmin-SharedMailBox"] || !$plugin['imail_gamma']) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

if(sizeof($_POST)==0){
	$_SESSION['SharedMailBoxReturnMsg'] = $Lang['SharedMailBox']['ReturnMsg']['AddMailBoxFail'];
	intranet_closedb();
	header("Location: new.php");
	exit;
}

$MailBoxName = trim($_POST["MailBoxName"])."@".$SYS_CONFIG['Mail']['UserNameSubfix'];
$MailPassword1 = trim($_POST["MailBoxPassword1"]);
$MailPassword2 = trim($_POST["MailBoxPassword2"]);
$MailBoxQuota = trim($_POST["MailBoxQuota"])==""?0:(int)$_POST["MailBoxQuota"];
$MemberList = $_POST["MemberList"];

$lsharedmailbox = new libsharedmailbox();
$lauth = new libauth();

$check_password_result = $lauth->CheckPasswordCriteria($MailPassword1,$_POST["MailBoxName"],8);
if(!in_array(1,$check_password_result)){
	$_SESSION['SharedMailBoxReturnMsg'] = $Lang['SharedMailBox']['ReturnMsg']['AddMailBoxFail'];
	intranet_closedb();
	header("Location: index.php");
	exit;
}

$Result['CreateMailBox'] = $lsharedmailbox->Create_Shared_MailBox($MailBoxName,$MailPassword1);
if($Result['CreateMailBox']){
	//$MailBoxID = $lsharedmailbox->db_insert_id();
	$MailBoxID = $Result['CreateMailBox'];
	for($i=0;$i<sizeof($MemberList);$i++){
		$uid = trim($MemberList[$i]);
		if($uid!=""){
		  	$Result['AddMember_'.$uid] = $lsharedmailbox->Add_User_To_MailBox($MailBoxID,$uid);
		}
	}
	$lsharedmailbox->Set_MailBox_Total_Quota($MailBoxName,$MailBoxQuota);
}

$url = "index.php";
if(!in_array(false,$Result)){
	//$Msg = "?Msg=".urlencode($Lang['SharedMailBox']['ReturnMsg']['AddMailBoxSuccess']);
	$_SESSION['SharedMailBoxReturnMsg'] = $Lang['SharedMailBox']['ReturnMsg']['AddMailBoxSuccess'];
}else{
	//$Msg = "?Msg=".urlencode($Lang['SharedMailBox']['ReturnMsg']['AddMailBoxFail']);
	$_SESSION['SharedMailBoxReturnMsg'] = $Lang['SharedMailBox']['ReturnMsg']['AddMailBoxFail'];
}

intranet_closedb();
header("Location: ".$url.$Msg);
?>