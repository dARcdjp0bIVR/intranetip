<?php
// editing by 
/******************************** Modification Log *********************************************
 * 2018-01-04 (Carlos): Enforce password checking.
 * 2011-12-09 (Carlos): Modified add/remove members flow - remove all then add current members
 ***********************************************************************************************/

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libauth.php");
include_once($PATH_WRT_ROOT."includes/libsharedmailbox.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if(!$_SESSION["SSV_USER_ACCESS"]["eAdmin-SharedMailBox"] || !$plugin['imail_gamma']) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

if(sizeof($_POST)==0){
	intranet_closedb();
	header("Location: edit.php?MailBoxID=".$MailBoxID);
	exit;
}

$MailBoxID = $_POST['MailBoxID'];
$MailBoxName = $_POST["MailBoxName"];
$MailPassword1 = trim($_POST["MailBoxPassword1"]);
$MailPassword2 = trim($_POST["MailBoxPassword2"]);
$MailBoxQuota = trim($_POST["MailBoxQuota"])==""?0:(int)$_POST["MailBoxQuota"];
$OldMailBoxQuota = trim($_POST["OldMailBoxQuota"])==""?0:(int)$_POST["OldMailBoxQuota"];
$NewMemberList = sizeof($_POST["MemberList"])>0?$_POST["MemberList"]:array();

$lauth = new libauth();
$lsharedmailbox = new libsharedmailbox();
$MailBox = $lsharedmailbox->Get_Shared_MailBox($MailBoxID);
//$MailBoxMembers = $lsharedmailbox->Get_MailBox_UserList($MailBoxID);
$MailBoxMembers = $lsharedmailbox->Get_MailBox_Members($MailBoxID);

$Result=array();
$OldPassword = $MailBox[0]['MailBoxPassword'];
if($MailPassword1 != '' && $MailPassword2 != '')
{
	$check_password_result = $lauth->CheckPasswordCriteria($MailPassword1,substr($MailBoxName,0,strpos($MailBoxName,'@')),8);
	if(!in_array(1,$check_password_result)){
		$_SESSION['SharedMailBoxReturnMsg'] = $Lang['SharedMailBox']['ReturnMsg']['EditMailBoxFail'];
		intranet_closedb();
		header("Location: index.php");
		exit;
	}
	
	if($OldPassword != $MailPassword1){
		$Result['UpdatePassword'] = $lsharedmailbox->Change_Shared_Mailbox_Password($MailBoxName,$MailPassword1);
	}
}

if($MailBoxQuota != $OldMailBoxQuota){
	$Result['SetTotalQuota'] = $lsharedmailbox->Set_MailBox_Total_Quota($MailBoxName,$MailBoxQuota);
}

$OldMemberList = array();
for($i=0;$i<sizeof($MailBoxMembers);$i++){
	$OldMemberList[] = $MailBoxMembers[$i]['UserID'];
}

//$MembersToAdd = array_diff($NewMemberList, $OldMemberList);
//$MembersToRemove = array_diff($OldMemberList, $NewMemberList);
$MembersToAdd = $NewMemberList;
$MembersToRemove = $OldMemberList;

if(sizeof($MembersToRemove)>0){
	foreach($MembersToRemove as $key=>$uid)
	{
		if(trim($uid)!="") $Result['RemoveMember'.$uid] = $lsharedmailbox->Remove_User_From_MailBox($MailBoxID,$uid);
	}
}

if(sizeof($MembersToAdd)>0){
	foreach($MembersToAdd as $key=>$uid)
	{
		if(trim($uid)!="") $Result['AddMember'.$uid] = $lsharedmailbox->Add_User_To_MailBox($MailBoxID,$uid);
	}
}

$Result['UpdateModifyTime'] = $lsharedmailbox->Update_MailBox_Last_Update($MailBoxName);

$url = "index.php";
if(!in_array(false,$Result)){
	//$Msg = "?Msg=".urlencode($Lang['SharedMailBox']['ReturnMsg']['EditMailBoxSuccess']);
	$_SESSION['SharedMailBoxReturnMsg'] = $Lang['SharedMailBox']['ReturnMsg']['EditMailBoxSuccess'];
}else{
	//$Msg = "?Msg=".urlencode($Lang['SharedMailBox']['ReturnMsg']['EditMailBoxFail']);
	$_SESSION['SharedMailBoxReturnMsg'] = $Lang['SharedMailBox']['ReturnMsg']['EditMailBoxFail'];
}

intranet_closedb();
header("Location: ".$url.$Msg);
?>