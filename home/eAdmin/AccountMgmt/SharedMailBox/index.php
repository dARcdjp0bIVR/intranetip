<?php 
// editing by 
/******************************** Modification Log *******************************************
 * 2018-01-04 (Carlos): Use $_SESSION['SharedMailBoxReturnMsg'] to get return message.
 *********************************************************************************************/

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libsharedmailbox.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

if(!$_SESSION["SSV_USER_ACCESS"]["eAdmin-SharedMailBox"] || !$plugin['imail_gamma']) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$linterface = new interface_html();
$lsharedmailbox = new libsharedmailbox();

$CurrentPageArr['SharedMailBox'] = 1;
$CurrentPage = "Mgmt_SharedMailBox";

$TAGS_OBJ[] = array($Lang['SharedMailBox']['SharedMailBox']);
$MODULE_OBJ['title'] = $Lang['SharedMailBox']['SharedMailBox'];

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
if($order == "") $order = 1;
if($field == "") $field = 0;
$pageSizeChangeEnabled = true;

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;

$li = new libdbtable2007($field, $order, $pageNo);

$NameField = getNameFieldByLang2("u.");

$sql = "SELECT
			CONCAT('<a href=\"edit.php?MailBoxID[]=',s.MailBoxID,'\">',s.MailBoxName,'</a>') as MailBoxName,
			COUNT(m.MailBoxMemberID) as MemberCount,
			GROUP_CONCAT(DISTINCT CONCAT('\"',$NameField,'\"') Separator ', ') as GroupMembers,
			s.LastModified,
			CONCAT('<input type=\'checkbox\' name=\'MailBoxID[]\' id=\'MailBoxID[]\' value=\'', s.MailBoxID ,'\'>') as checkbox,
			s.MailBoxID 
		FROM 
		MAIL_SHARED_MAILBOX as s 
		LEFT JOIN MAIL_SHARED_MAILBOX_MEMBER as m ON s.MailBoxID = m.MailBoxID 
		LEFT JOIN INTRANET_USER as u ON u.UserID = m.UserID ";
if($keyword!=""){
$sql .= "WHERE s.MailBoxName LIKE '%".$lsharedmailbox->Get_Safe_Sql_Like_Query($keyword)."%' ";
}
$sql .= "GROUP BY s.MailBoxID ";

$li->sql = $sql;
$li->field_array = array("MailBoxName", "MemberCount","GroupMembers","LastModified");
$li->no_col = sizeof($li->field_array)+2;
$li->IsColOff = 2;

$pos = 0;
$li->column_list .= "<td width='1'>#</td>\n";
$li->column_list .= "<td width='30%' >".$li->column($pos++, $Lang['SharedMailBox']['EmailAddress'])."</td>\n";
$li->column_list .= "<td width='10%' >".$li->column($pos++, $Lang['SharedMailBox']['NoOfSharers'])."</td>\n";
$li->column_list .= "<td width='40%' >".$li->column($pos++, $Lang['SharedMailBox']['SharedBy'])."</td>\n";
$li->column_list .= "<td width='20%' >".$li->column($pos++, $Lang['General']['LastModified'])."</td>\n";
$li->column_list .= "<td width='1'>".$li->check("MailBoxID[]")."</td>\n";


$toolbar = $linterface->GET_LNK_NEW("javascript:newSharedMailBox();",$Lang['SharedMailBox']['NewSharedMailBox'],"","","",0);

$MODULE_OBJ = $lsharedmailbox->GET_MODULE_OBJ_ARR();

if(isset($_SESSION['SharedMailBoxReturnMsg'])){
	$msg = $_SESSION['SharedMailBoxReturnMsg'];
	unset($_SESSION['SharedMailBoxReturnMsg']);
}else{
	$msg = urldecode($_REQUEST['Msg']);
}
$linterface->LAYOUT_START($msg);
?>
<script type="text/javascript" language="javascript">
function newSharedMailBox()
{
	document.form1.action="new.php";
	document.form1.submit();
}

function editSharedMailBox()
{
	var objs = $('input[name=MailBoxID\\[\\]]:checked');
	if(objs.length==1){
		document.form1.action="edit.php";
		document.form1.submit();
	}else{
		alert('<?=$Lang['SharedMailBox']['Warning']['SelectOnlyOneMailBoxToEdit']?>');
	}
}

function deleteSharedMailBox()
{
	var objs = $('input[name=MailBoxID\\[\\]]:checked');
	if(objs.length>0){
		if(confirm('<?=$Lang['SharedMailBox']['Warning']['ConfirmDeleteMailBox']?>')){
			document.form1.action="remove.php";
			document.form1.submit();
		}
	}else{
		alert('<?=$Lang['SharedMailBox']['Warning']['SelectAtLeastOneMailBox']?>');
	}
}

</script>
<form name="form1" method="post" action="">
<table width="99%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td height="40" width="40%"><?=$toolbar?></td>
		<td align="right">
			<div class="Conntent_search"><input name="keyword" id="keyword" type="text" value="<?=stripslashes($keyword)?>"/></div>
		</td>
	</tr>
	<tr>
		<td class="main_content" colspan="2">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
	        	<tr>
	              <td valign="bottom" align="right">
					<table border="0" cellspacing="0" cellpadding="0">
					  <tr>
						<td width="21"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/table_tool_01.gif" width="21" height="23" /></td>
						<td background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/table_tool_02.gif">
							<table border="0" cellspacing="0" cellpadding="2">
								<tr>
									<td nowrap class="tabletext">
										<a href="javascript:editSharedMailBox(0);" class="tabletool"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_edit.gif" width="12" height="12" border="0" align="absmiddle" /> <?=$Lang['Btn']['Edit']?></a> 
							            <a href="javascript:deleteSharedMailBox(0);" class="tabletool"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_delete.gif" width="12" height="12" border="0" align="absmiddle" /> <?=$Lang['Btn']['Delete']?></a> 
							        </td>
								</tr>
							</table>
						</td>
						<td width="6"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/table_tool_03.gif" width="6" height="23" /></td>
					  </tr>
					</table>
	              </td>
	            </tr>
	        </table>
			<?= $li->display()?>
		</td>
	</tr>
</table>	
		
<input type="hidden" name="pageNo" id="pageNo" value="<?=$li->pageNo; ?>" />
<input type="hidden" name="order" id="order" value="<?=$li->order; ?>" />
<input type="hidden" name="field" id="field" value="<?=$li->field; ?>" />
<input type="hidden" name="page_size_change" id="page_size_change" value="" />
<input type="hidden" name="numPerPage" id="numPerPage" value="<?=$li->page_size?>" />
</form>

<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>