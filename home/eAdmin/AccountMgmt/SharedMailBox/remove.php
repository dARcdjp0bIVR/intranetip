<?php
// editing by 
/******************************** Modification Log *******************************************
 * 2018-01-04 (Carlos): Use $_SESSION['SharedMailBoxReturnMsg'] to set return message.
 *********************************************************************************************/

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libsharedmailbox.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if(!$_SESSION["SSV_USER_ACCESS"]["eAdmin-SharedMailBox"] || !$plugin['imail_gamma']) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$MailBoxID = $_POST['MailBoxID'];
if(sizeof($MailBoxID)==0){
	$_SESSION['SharedMailBoxReturnMsg'] = $Lang['SharedMailBox']['ReturnMsg']['DeleteMailBoxFail'];
	intranet_closedb();
	header("Location: index.php");
	exit;
}

$lsharedmailbox = new libsharedmailbox();

$MailBox = $lsharedmailbox->Get_Shared_MailBox($MailBoxID);
$MailBoxIdTOName=array();
for($i=0;$i<sizeof($MailBox);$i++){
	$mailbox_id = $MailBox[$i]['MailBoxID'];
	$mailbox_name = $MailBox[$i]['MailBoxName'];
	$MailBoxIdTOName[$mailbox_id] = $mailbox_name;
}
$Result=array();
for($i=0;$i<sizeof($MailBoxID);$i++){
	$mailbox_id = $MailBoxID[$i];
	$mailbox_name = $MailBoxIdTOName[$mailbox_id];
	if($mailbox_id!="" && $mailbox_name!="")
		$Result['RemoveMailBox_'.$mailbox_name] = $lsharedmailbox->Remove_MailBox($mailbox_id,$mailbox_name);
}

$url = "index.php";
if(!in_array(false,$Result)){
	//$Msg = "?Msg=".urlencode($Lang['SharedMailBox']['ReturnMsg']['DeleteMailBoxSuccess']);
	$_SESSION['SharedMailBoxReturnMsg'] = $Lang['SharedMailBox']['ReturnMsg']['DeleteMailBoxSuccess'];
}else{
	//$Msg = "?Msg=".urlencode($Lang['SharedMailBox']['ReturnMsg']['DeleteMailBoxFail']);
	$_SESSION['SharedMailBoxReturnMsg'] = $Lang['SharedMailBox']['ReturnMsg']['DeleteMailBoxFail'];
}
intranet_closedb();
header("Location: ".$url.$Msg);
?>