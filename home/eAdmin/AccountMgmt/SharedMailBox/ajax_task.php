<?php
// editing by 

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libsharedmailbox.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if(!$_SESSION["SSV_USER_ACCESS"]["eAdmin-SharedMailBox"] || !$plugin['imail_gamma']) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$lsharedmailbox = new libsharedmailbox();

if($action=="check_mailboxname"){
	$MailBoxName = trim(stripslashes(urldecode($_POST["MailBoxName"])))."@".$SYS_CONFIG['Mail']['UserNameSubfix'];
	$mailbox_exist = $lsharedmailbox->Check_MailBox_Exist($MailBoxName);
	$validate_result = intranet_validateEmail($MailBoxName);
	
	if(!$validate_result){
		echo "2"; // invalid email address
	}else if($mailbox_exist){
		echo "1"; // duplicated mailbox
	}else{
		echo "0"; // OK
	}
}

intranet_closedb();
?>