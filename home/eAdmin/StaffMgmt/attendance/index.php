<?php
ini_set("memory_limit", "1024M");
set_time_limit(60*60);
// editing by 
/********************************************** Change log ********************************************
 * 2016-02-15 (Carlos): set larger memory limit for $StaffAttend3UI->Get_Module_Index() which would loop through every staff to generate duty until today. 
 * 2014-08-13 (Carlos): redirect to management page before UI output
 * 2012-05-29 (Carlos): if user has manage right, auto redirect to Management > Daily Record
 ******************************************************************************************************/
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend2.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if ((!$StaffAttend3->IS_ADMIN_USER() && !$_SESSION["SSV_PRIVILEGE"]["StaffAttendance3"]["has_access_right"]) || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$linterface = new interface_html();
$StaffAttend3UI = new libstaffattend3_ui();

$StaffAttend3UI->Get_Module_Index();

if($StaffAttend3->Check_Access_Right('MGMT-Attendance-Manage') || $StaffAttend3->IS_ADMIN_USER()){
	intranet_closedb();
	header("Location: /home/eAdmin/StaffMgmt/attendance/Management/Attendance/");
	exit;
}

$CurrentPageArr['eAdminStaffAttendance'] = 1;
$TAGS_OBJ[] = array($Lang['StaffAttendance']['StaffAttendance'], "", 0);
$MODULE_OBJ = $StaffAttend3UI->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START(urldecode($_REQUEST['Msg']));

//$StaffAttend3UI->Get_Module_Index();

$linterface->LAYOUT_STOP();
intranet_closedb();
?>