<?php
// editing by 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if (!$sys_custom['StaffAttendance']['DailyAttendanceRecord'] || (!$StaffAttend3->IS_ADMIN_USER() && 
		!$StaffAttend3->Check_Access_Right('REPORT-DAILYATTENDANCERECORD'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	echo 'die';
	intranet_closedb();
	exit();
}

$StaffAttend3UI = new libstaffattend3_ui();

$task = $_REQUEST['task'];

switch($task)
{
	case "Get_Daily_Attendance_Record_Table":
		echo $StaffAttend3UI->Get_Daily_Attendance_Record_Table($_REQUEST['TargetDate']);
	break;
	
	case "Get_Daily_Attendance_Record_Email_Form":
		echo $StaffAttend3UI->Get_Daily_Attendance_Record_Email_Form();
	break;
	
	case "Send_Email":
		$ToEmail = trim(urldecode(stripslashes($_REQUEST['ToEmail'])));
		$success = $StaffAttend3UI->Send_Daily_Attendance_Record_Mail($_REQUEST['TargetDate'],$ToEmail);
		if($success){
			echo $Lang['StaffAttendance']['ReturnMsg']['AttendanceRecordSentSuccess'];
		}else{
			echo $Lang['StaffAttendance']['ReturnMsg']['AtttendanceRecordSentUnsuccess'];
		}
	break;
}

intranet_closedb();
?>