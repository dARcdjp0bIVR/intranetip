<?php
// editing by 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend2.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if (!$sys_custom['StaffAttendance']['DailyAttendanceRecord'] || (!$StaffAttend3->IS_ADMIN_USER() && 
		!$StaffAttend3->Check_Access_Right('REPORT-DAILYATTENDANCERECORD'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$linterface = new interface_html();
$StaffAttend3UI = new libstaffattend3_ui();

$CurrentPageArr['eAdminStaffAttendance'] = 1;
$CurrentPage['ReportDailyAttendanceRecord'] = 1;

$TAGS_OBJ[] = array($Lang['StaffAttendance']['DailyLeaveAbsenceRecords'],$PATH_WRT_ROOT."home/eAdmin/StaffMgmt/attendance/Report/DailyAttendanceRecord/",1);

$MODULE_OBJ = $StaffAttend3UI->GET_MODULE_OBJ_ARR();
//$TAGS_OBJ_RIGHT[] = array(gen_online_help_btn_and_layer('staff_attendance','daily_log'));
$linterface->LAYOUT_START(urldecode($_REQUEST['Msg']));

echo $StaffAttend3UI->Get_Daily_Attendance_Record_Index();

$linterface->LAYOUT_STOP();
intranet_closedb();
?>
<script type="text/javascript" language="JavaScript">
function Get_Daily_Attendance_Record_Table()
{
	var TargetDate = $.trim($('input#TargetDate').val());
	var is_valid = Check_Valid_Date('TargetDate', 'DPWL-TargetDate');
	
	if(is_valid)
	{
		Block_Element('DivLayer');
		$('div#TableLayer').load(
			'ajax_load.php',
			{
				'task':'Get_Daily_Attendance_Record_Table',
				'TargetDate':TargetDate
			},
			function(data){
				UnBlock_Element('DivLayer');
			}
		);
	}
}

function Get_Daily_Attendance_Record_Email_Form()
{
	tb_show("<?=$Lang['StaffAttendance']['SendAttendanceRecordsToAdmin']?>","#TB_inline?height=200&width=550&inlineId=FakeLayer");
	
	$.post(
		"ajax_load.php",
		{
			'task':"Get_Daily_Attendance_Record_Email_Form"
		},
		function(ReturnData){
			$("div#TB_ajaxContent").html(ReturnData);
		}
	);
}

function SendEmail()
{
	var TargetDate = $.trim($('input#HiddenTargetDate').val());
	var ToEmail = $.trim($('#ToEmail').val());
	var re = /^([\w_\.\-])+\@(([\w_\.\-])+\.)+([\w]{2,4})+$/;
	var warningDiv = $('#ToEmailWarningDiv');
	
	if(ToEmail == '' || !re.test(ToEmail)){
		warningDiv.show();
		return false;
	}else{
		warningDiv.hide();
	}
	
	var originalBlockMsg = blockMsg;
	blockMsg = '<?=$Lang['StaffAttendance']['SendingEmailMessage']?>';
	Block_Thickbox();
	$.post(
		'ajax_load.php',
		{
			'task':'Send_Email',
			'TargetDate':TargetDate,
			'ToEmail':encodeURIComponent(ToEmail) 
		},
		function(data){
			blockMsg = originalBlockMsg;
			Get_Return_Message(data);
			Scroll_To_Top();
			window.top.tb_remove();
		}
	);
}

function Check_Valid_Date(DateObjId, WarningLayerId)
{
	var dateObj = document.getElementById(DateObjId);
	var $WarningLayer = $('#'+WarningLayerId);
	if(check_date_without_return_msg(dateObj))
	{
		var targetdate = dateObj.value;
		var currentdate = new Date();
		var curYear = '' + currentdate.getFullYear();
		var curMonth = currentdate.getMonth()+1;
		var curDay = currentdate.getDate();
		curMonth = (curMonth<10)? ('0'+curMonth):(''+curMonth);
		curDay = (curDay<10)? ('0'+curDay):(''+curDay);
		var today = curYear + '-' + curMonth + '-' + curDay;
		
		if(targetdate>today)
		{
			dateObj.focus();
			$WarningLayer.html('<?=$Lang['StaffAttendance']['UnableToViewFutureAttendanceRecords']?>');
			$WarningLayer.show();
			return false;
		}else
		{
			$WarningLayer.html('');
			$WarningLayer.hide();
			return true;
		}
	}else
	{
		dateObj.focus();
		$WarningLayer.html('<?=$Lang['General']['InvalidDateFormat']?>');
		$WarningLayer.show();
		return false;
	}
}
</script>