<?php
// editing by 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!$StaffAttend3->Check_Access_Right('REPORT-OT'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	echo 'die';
	intranet_closedb();
	exit();
}

$StaffAttend3UI = new libstaffattend3_ui();

$SelectDate = intranet_htmlspecialchars($_REQUEST['SelectDate']);
$SelectStaff = IntegerSafe($_REQUEST['SelectStaff']);
$StartDate = intranet_htmlspecialchars($_REQUEST['StartDate']);
$EndDate = intranet_htmlspecialchars($_REQUEST['EndDate']);
$SortFields = array("SortField"=>intranet_htmlspecialchars($_REQUEST['SortField']),"SortOrder"=>intranet_htmlspecialchars($_REQUEST['SortOrder']));
$Keyword = trim(urldecode(stripslashes($_REQUEST['Keyword'])));
$Task = intranet_htmlspecialchars(strtoupper($_REQUEST['Task']));

if($Task=="PRINT") include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");
echo $StaffAttend3UI->Get_Report_OT_Table($SelectDate, $SelectStaff, $StartDate, $EndDate, $Keyword, $Task, $SortFields);
if($Task=="PRINT") include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_footer.php");

intranet_closedb();
?>