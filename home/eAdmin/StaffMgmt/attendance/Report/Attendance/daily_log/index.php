<?php
// editing by 
/*
 * 2015-10-08 (Carlos): Added checkbox option [Show Absent Suspected Record].
 */
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend2.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!$StaffAttend3->Check_Access_Right('REPORT-DAILYLOG'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$linterface = new interface_html();
$StaffAttend3UI = new libstaffattend3_ui();

$CurrentPageArr['eAdminStaffAttendance'] = 1;
$CurrentPage['ReportDailyLog'] = 1;

$TAGS_OBJ[] = array($Lang['StaffAttendance']['DailyLog'],$PATH_WRT_ROOT."home/eAdmin/StaffMgmt/attendance/Report/Attendance/daily_log/",1);
$TAGS_OBJ[] = array($Lang['StaffAttendance']['InOutRecord'],$PATH_WRT_ROOT."home/eAdmin/StaffMgmt/attendance/Management/Attendance/index.php?type=2",0);

$MODULE_OBJ = $StaffAttend3UI->GET_MODULE_OBJ_ARR();
//$TAGS_OBJ_RIGHT[] = array(gen_online_help_btn_and_layer('staff_attendance','daily_log'));
$linterface->LAYOUT_START(urldecode($_REQUEST['Msg']));

echo $StaffAttend3UI->Get_Report_DailyLog_Index();

$linterface->LAYOUT_STOP();
intranet_closedb();
?>
<script type="text/JavaScript" language="JavaScript">
// dom function 
{
function Check_Go_Search_DailyLog(evt)
{
	var key = evt.which || evt.charCode || evt.keyCode;
	
	if (key == 13) // enter
	{
		Get_Report_DailyLog_List();
	}
	else
		return false;
}

function Check_Valid_Date(DateObjId, WarningLayerId)
{
	var dateObj = document.getElementById(DateObjId);
	var $WarningLayer = $('#'+WarningLayerId);
	if(check_date_without_return_msg(dateObj))
	{
		$WarningLayer.html('');
		$WarningLayer.css('visibility','hidden');
		return true;
	}else
	{
		dateObj.focus();
		$WarningLayer.html('<?=$Lang['LessonAttendance']['InvalidDateFormat']?>');
		$WarningLayer.css('visibility','visible');
		SetTimerToHideWarning(WarningLayerId, 3000);
		return false;
	}
}

function SetTimerToHideWarning(WarningLayerId, ticks)
{
	var action = "$('#"+WarningLayerId+"').css('visibility','hidden');";
	setTimeout(action,ticks);
}

function MouseOverEvent(thisElement)
{
	if($('input#SortOrder').val()=='ASC')
		$(thisElement).children('img').attr('src','<?=$PATH_WRT_ROOT."images/".$LAYOUT_SKIN."/icon_sort_a_on.gif"?>');
	else
		$(thisElement).children('img').attr('src','<?=$PATH_WRT_ROOT."images/".$LAYOUT_SKIN."/icon_sort_d_on.gif"?>');
}

function MouseOutEvent(thisElement)
{
	if($('input#SortOrder').val()=='ASC')
		$(thisElement).children('img').attr('src','<?=$PATH_WRT_ROOT."images/".$LAYOUT_SKIN."/icon_sort_a_off.gif"?>');
	else
		$(thisElement).children('img').attr('src','<?=$PATH_WRT_ROOT."images/".$LAYOUT_SKIN."/icon_sort_d_off.gif"?>');
}
}

// ajax function
{
function Get_Report_DailyLog_List()
{
	var PostVar = {
			"TargetDate": $('#TargetDate').val(),
			"SelectType": $('#SelectType').val(),
			"SelectStaff": $('#SelectStaff').val(),
			"Keyword": encodeURIComponent($('Input#Keyword').val()),
			"ShowAbsentSuspectedRecord": $('#ShowAbsentSuspectedRecord').length>0?($('#ShowAbsentSuspectedRecord').is(':checked')?1:0):0,
			"SortField": $('input#SortField').val(),
			"SortOrder": $('input#SortOrder').val()
		}
	
	if(!Check_Valid_Date('TargetDate', 'WarningLayer'))
		return;
	
	Block_Element("ReportDailyLogTableLayer");
	$.post('ajax_get_dailylog_list.php',PostVar,
					function(data){
						if (data == "die") 
							window.top.location = '/';
						else {
							$('div#ReportDailyLogTableLayer').html(data);
							Thick_Box_Init();
							UnBlock_Element("ReportDailyLogTableLayer");
						}
					});
}

function Sort(fieldname)
{
	$('input#SortField').val(fieldname);
	$order = $('input#SortOrder').val();
	if($order=='DESC')
		$('input#SortOrder').val('ASC');
	else
		$('input#SortOrder').val('DESC');
	Get_Report_DailyLog_List();
}
}

// thick box function 
{
//on page load call tb_init
function Thick_Box_Init()
{   
	tb_init('a.thickbox');//pass where to apply thickbox
}
}
</script>