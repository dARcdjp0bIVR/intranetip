<?php
// editing by 
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend2.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!$StaffAttend3->Check_Access_Right('REPORT-ATTENDANCE'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$linterface = new interface_html();
$StaffAttend3UI = new libstaffattend3_ui();

$CurrentPageArr['eAdminStaffAttendance'] = 1;
$CurrentPage['ReportAttendance'] = 1;

$TAGS_OBJ[] = array($Lang['StaffAttendance']['AttendanceReport'], "", 1);
$MODULE_OBJ = $StaffAttend3UI->GET_MODULE_OBJ_ARR();
//$TAGS_OBJ_RIGHT[] = array(gen_online_help_btn_and_layer('staff_attendance','attendance_rpt'));
$linterface->LAYOUT_START(urldecode($_REQUEST['Msg']));

//echo $StaffAttend3UI->Get_Report_Attendance_Monthly_Summary();
echo $StaffAttend3UI->Get_Report_Attendance_Index();

$linterface->LAYOUT_STOP();
intranet_closedb();
?>
<script type="text/JavaScript" language="JavaScript">
// dom function 
{
function Check_Go_Search_Monthly_Summary(evt)
{
	var key = evt.which || evt.charCode || evt.keyCode;
	
	if (key == 13) // enter
	{
		Get_Report_Attendance_Monthly_Summary_List();
	}
	else
		return false;
}

function Check_Go_Search_Monthly_Details(evt)
{
	var key = evt.which || evt.charCode || evt.keyCode;
	
	if (key == 13) // enter
	{
		Get_Report_Attendance_Monthly_Details_List();
	}
	else
		return false;
}


function Check_Go_Search_Daily_Details(evt)
{
	var key = evt.which || evt.charCode || evt.keyCode;
	
	if (key == 13) // enter
	{
		Get_Report_Attendance_Daily_Details_List();
	}
	else
		return false;
}

function Check_Go_Search_Range_Summary(evt)
{
	var key = evt.which || evt.charCode || evt.keyCode;
	
	if (key == 13) // enter
	{
		Get_Report_Attendance_Range_Summary_List();
	}
	else
		return false;
}

function Check_Go_Search_Range_Details(evt)
{
	var key = evt.which || evt.charCode || evt.keyCode;
	
	if (key == 13) // enter
	{
		Get_Report_Attendance_Range_Details_List();
	}
	else
		return false;
}
}

// ajax function
{
function Get_Report_Attendance_Monthly_Summary()
{
	var PostVar = {
		"SelectMonth":$('#SelectMonth').val(),
		"SelectYear":$('#SelectYear').val(),
		"SelectType":$('#SelectType').val(),
		"SelectWaived":$('#SelectWaived').val(),
		"HaveRecord":$('#HaveRecord').attr('checked')?1:0,
		"Keyword": encodeURIComponent($('Input#Keyword').val())
	}
	
	Block_Element("ReportAttendanceLayer");
	$.post('ajax_get_monthly_summary.php',PostVar,
					function(data){
						if (data == "die") 
							window.top.location = '/';
						else {
							$('div#ReportAttendanceLayer').html(data);
							Thick_Box_Init();
							UnBlock_Element("ReportAttendanceLayer");
						}
					});
}

function Get_Report_Attendance_Monthly_Summary_List()
{
	var PostVar = {
			"SelectMonth":$('#SelectMonth').val(),
			"SelectYear":$('#SelectYear').val(),
			"SelectType":$('#SelectType').val(),
			"SelectWaived":$('#SelectWaived').val(),
			"HaveRecord":$('#HaveRecord').attr('checked')?1:0,
			"Keyword": encodeURIComponent($('Input#Keyword').val())
		}
	
	Block_Element("ReportAttendanceTableLayer");
	$.post('ajax_get_monthly_summary_list.php',PostVar,
					function(data){
						if (data == "die") 
							window.top.location = '/';
						else {
							$('div#ReportAttendanceTableLayer').html(data);
							Thick_Box_Init();
							UnBlock_Element("ReportAttendanceTableLayer");
						}
					});
}

function Get_Report_Attendance_Range_Summary()
{
	var PostVar = {
		"SelectMonth":$('#SelectMonth').val(),
		"SelectYear":$('#SelectYear').val(),
		"SelectType":$('#SelectType').val(),
		"SelectWaived":$('#SelectWaived').val(),
		"HaveRecord":$('#HaveRecord').attr('checked')?1:0,
		"Keyword": encodeURIComponent($('Input#Keyword').val()),
		"ByDateRange":1,
		"StartDate": $('#StartDate').val(),
		"EndDate": $('#EndDate').val()
	}
	
	Block_Element("ReportAttendanceLayer");
	$.post('ajax_get_monthly_summary.php',PostVar,
					function(data){
						if (data == "die") 
							window.top.location = '/';
						else {
							$('div#ReportAttendanceLayer').html(data);
							Thick_Box_Init();
							UnBlock_Element("ReportAttendanceLayer");
						}
					});
}

function Get_Report_Attendance_Range_Summary_List()
{
	var PostVar = {
			"SelectMonth":$('#SelectMonth').val(),
			"SelectYear":$('#SelectYear').val(),
			"SelectType":$('#SelectType').val(),
			"SelectWaived":$('#SelectWaived').val(),
			"HaveRecord":$('#HaveRecord').attr('checked')?1:0,
			"Keyword": encodeURIComponent($('Input#Keyword').val()),
			"ByDateRange":1,
			"StartDate": $('#StartDate').val(),
			"EndDate": $('#EndDate').val()
		}
	
	Block_Element("ReportAttendanceTableLayer");
	$.post('ajax_get_monthly_summary_list.php',PostVar,
					function(data){
						if (data == "die") 
							window.top.location = '/';
						else {
							$('div#ReportAttendanceTableLayer').html(data);
							Thick_Box_Init();
							UnBlock_Element("ReportAttendanceTableLayer");
						}
					});
}

function Get_Report_Attendance_Range_Details(GroupID, UserID, StartDate, EndDate, StatusType)
{	
	var TargetGroupID = GroupID || $('#GroupID').val();
	var TargetStaffID = UserID || $('#SelectStaff').val();
	var TargetStartDate = StartDate || $('#StartDate').val();
	var TargetEndDate = EndDate || $('#EndDate').val();
	var TargetStatus = StatusType || $('#SelectStatus').val();
	
	var PostVar = {
		"GroupID":TargetGroupID,
		"StartDate":TargetStartDate,
		"EndDate":TargetEndDate,
		"SelectStaff":TargetStaffID,
		"SelectStatus":TargetStatus,
		"SelectWaived":$('#SelectWaived').val(),
		"Keyword": encodeURIComponent($('Input#Keyword').val()),
		"ByDateRange": 1
	};
	
	Block_Element("ReportAttendanceLayer");
	$.post('ajax_get_monthly_details.php',PostVar,
					function(data){
						if (data == "die") 
							window.top.location = '/';
						else {
							$('div#ReportAttendanceLayer').html(data);
							Thick_Box_Init();
							UnBlock_Element("ReportAttendanceLayer");
						}
					});
}

function Get_Report_Attendance_Range_Details_List(GroupID, UserID, StartDate, EndDate, StatusType)
{
	var TargetGroupID = GroupID || $('#GroupID').val();
	var TargetStaffID = UserID || $('#SelectStaff').val();
	var TargetStartDate = StartDate || $('#StartDate').val();
	var TargetEndDate = EndDate || $('#EndDate').val();
	var TargetStatus = StatusType || $('#SelectStatus').val();
	
	var PostVar = {
		"GroupID":TargetGroupID,
		"StartDate":TargetStartDate,
		"EndDate":TargetEndDate,
		"SelectStaff":TargetStaffID,
		"SelectStatus":TargetStatus,
		"SelectWaived":$('#SelectWaived').val(),
		"Keyword": encodeURIComponent($('Input#Keyword').val()),
		"ByDateRange": 1
	};
	
	Block_Element("ReportAttendanceTableLayer");
	$.post('ajax_get_monthly_details_list.php',PostVar,
					function(data){
						if (data == "die") 
							window.top.location = '/';
						else {
							$('div#ReportAttendanceTableLayer').html(data);
							Thick_Box_Init();
							UnBlock_Element("ReportAttendanceTableLayer");
						}
					});
}

function Get_Report_Attendance_Monthly_Details(GroupID, UserID, Year, Month, StatusType)
{	
	var TargetGroupID = GroupID || $('#GroupID').val();
	var TargetStaffID = UserID || $('#SelectStaff').val();
	var TargetYear = Year || $('#SelectYear').val();
	var TargetMonth = Month || $('#SelectMonth').val();
	var TargetStatus = StatusType || $('#SelectStatus').val();
	
	var PostVar = {
		"GroupID":TargetGroupID,
		"SelectMonth":TargetMonth,
		"SelectYear":TargetYear,
		"SelectStaff":TargetStaffID,
		"SelectStatus":TargetStatus,
		"SelectWaived":$('#SelectWaived').val(),
		"Keyword": encodeURIComponent($('Input#Keyword').val())
	};
	
	Block_Element("ReportAttendanceLayer");
	$.post('ajax_get_monthly_details.php',PostVar,
					function(data){
						if (data == "die") 
							window.top.location = '/';
						else {
							$('div#ReportAttendanceLayer').html(data);
							Thick_Box_Init();
							UnBlock_Element("ReportAttendanceLayer");
						}
					});
}

function Get_Report_Attendance_Monthly_Details_List(GroupID, UserID, Year, Month, StatusType)
{
	var TargetGroupID = GroupID || $('#GroupID').val();
	var TargetStaffID = UserID || $('#SelectStaff').val();
	var TargetYear = Year || $('#SelectYear').val();
	var TargetMonth = Month || $('#SelectMonth').val();
	var TargetStatus = StatusType || $('#SelectStatus').val();
	
	var PostVar = {
		"GroupID":TargetGroupID,
		"SelectMonth":TargetMonth,
		"SelectYear":TargetYear,
		"SelectStaff":TargetStaffID,
		"SelectStatus":TargetStatus,
		"SelectWaived":$('#SelectWaived').val(),
		"Keyword": encodeURIComponent($('Input#Keyword').val())
	};
	
	Block_Element("ReportAttendanceTableLayer");
	$.post('ajax_get_monthly_details_list.php',PostVar,
					function(data){
						if (data == "die") 
							window.top.location = '/';
						else {
							$('div#ReportAttendanceTableLayer').html(data);
							Thick_Box_Init();
							UnBlock_Element("ReportAttendanceTableLayer");
						}
					});
}

function Get_Report_Attendance_Daily_Details()
{
	var PostVar = {
		"SelectMonth":$('#SelectMonth').val(),
		"SelectYear":$('#SelectYear').val(),
		"SelectType":$('#SelectType').val(),
		"SelectWaived":$('#SelectWaived').val(),
		"HaveRecord":$('#HaveRecord').attr('checked')?1:0,
		"Keyword": encodeURIComponent($('Input#Keyword').val())
	}
	
	Block_Element("ReportAttendanceLayer");
	$.post('ajax_get_daily_details.php',PostVar,
					function(data){
						if (data == "die") 
							window.top.location = '/';
						else {
							$('div#ReportAttendanceLayer').html(data);
							Thick_Box_Init();
							UnBlock_Element("ReportAttendanceLayer");
						}
					});
}

function Get_Report_Attendance_Daily_Details_List()
{
	var PostVar = {
			"SelectMonth":$('#SelectMonth').val(),
			"SelectYear":$('#SelectYear').val(),
			"SelectType":$('#SelectType').val(),
			"SelectWaived":$('#SelectWaived').val(),
			"HaveRecord":$('#HaveRecord').attr('checked')?1:0,
			"Keyword": encodeURIComponent($('Input#Keyword').val())
		}
	
	Block_Element("ReportAttendanceTableLayer");
	$.post('ajax_get_daily_details_list.php',PostVar,
					function(data){
						if (data == "die") 
							window.top.location = '/';
						else {
							$('div#ReportAttendanceTableLayer').html(data);
							Thick_Box_Init();
							UnBlock_Element("ReportAttendanceTableLayer");
						}
					});
}
}

// thick box function 
{
//on page load call tb_init
function Thick_Box_Init()
{   
	tb_init('a.thickbox');//pass where to apply thickbox
}
}
</script>