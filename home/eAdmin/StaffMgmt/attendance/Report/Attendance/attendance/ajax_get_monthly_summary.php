<?php
// editing by 
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!$StaffAttend3->Check_Access_Right('REPORT-ATTENDANCE'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	echo 'die';
	intranet_closedb();
	exit();
}

$StaffAttend3UI = new libstaffattend3_ui();

$Year = $_REQUEST['SelectYear'];
$Month = $_REQUEST['SelectMonth'];
$Type = $_REQUEST['SelectType'];
$Keyword = trim(urldecode(stripslashes($_REQUEST['Keyword'])));
$WithWaived = $_REQUEST['SelectWaived'];
$HaveRecord = $_REQUEST['HaveRecord'];

$ByDateRange = $_REQUEST['ByDateRange']==1?true:false;
$StartDate = $_REQUEST['StartDate'];
$EndDate = $_REQUEST['EndDate'];

echo $StaffAttend3UI->Get_Report_Attendance_Monthly_Summary($Year, $Month, $Type, $Keyword, $WithWaived, $ByDateRange, $StartDate, $EndDate, $HaveRecord);

intranet_closedb();
?>