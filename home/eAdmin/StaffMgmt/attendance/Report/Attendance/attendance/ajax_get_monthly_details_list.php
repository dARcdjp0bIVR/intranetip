<?php
// editing by 
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!$StaffAttend3->Check_Access_Right('REPORT-ATTENDANCE'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	echo 'die';
	intranet_closedb();
	exit();
}

$StaffAttend3UI = new libstaffattend3_ui();

$GroupID = $_REQUEST['GroupID'];
$Year = $_REQUEST['SelectYear'];
$Month = $_REQUEST['SelectMonth'];
$TargetUserID = $_REQUEST['SelectStaff'];
$Status = $_REQUEST['SelectStatus'];
$Keyword = trim(urldecode(stripslashes($_REQUEST['Keyword'])));
$Task = strtoupper($_REQUEST['Task']);
$WithWaived = $_REQUEST['SelectWaived'];

$ByDateRange = $_REQUEST['ByDateRange']==1?true:false;
$StartDate = $_REQUEST['StartDate'];
$EndDate = $_REQUEST['EndDate'];

if($Task=="PRINT") include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");
echo $StaffAttend3UI->Get_Report_Attendance_Monthly_Details_List($GroupID, $TargetUserID, $Year, $Month, $Status, $Keyword, $Task, $WithWaived, $ByDateRange, $StartDate, $EndDate);
if($Task=="PRINT") include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_footer.php");

intranet_closedb();
?>