<?php
// editing by 
/*
 * 2019-07-23 (Ray): Add Duty Time
 * 2018-01-10 (Carlos): $sys_custom['StaffAttendance']['WongFutNamCollege'] - added [OT hours] and [Deduct hours] options for detailed report.
 * 2015-10-07 (Carlos): Added parameter [DisplayStatus].
 * 2014-01-09 (Carlos): Added parameter [DisplayAllCalendarDay]
 */
set_time_limit(10*60);
ini_set("memory_limit", "500M");
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!$StaffAttend3->Check_Access_Right('REPORT-CUSTOMIZE'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	echo 'die';
	intranet_closedb();
	exit();
}

$StaffAttend3UI = new libstaffattend3_ui();

$Params = array();
//$Params["ReportType"] = $_REQUEST["ReportType"];
//$Params["StaffID"] = $_REQUEST["StaffID"];
//$Params["GroupID"] = $_REQUEST["GroupID"];
$Params["SelectStaff"] = IntegerSafe($_REQUEST["SelectStaff"]);
$Params["PeriodType"] = intranet_htmlspecialchars($_REQUEST["PeriodType"]);
$Params["TargetStartDate"] = intranet_htmlspecialchars($_REQUEST["TargetStartDate"]);
$Params["TargetEndDate"] = intranet_htmlspecialchars($_REQUEST["TargetEndDate"]);
$Params["Format"] = intranet_htmlspecialchars($_REQUEST["Format"]);
$Params["StatusAll"] = intranet_htmlspecialchars($_REQUEST["StatusAll"]);
$Params["StatusPresent"] = intranet_htmlspecialchars($_REQUEST["StatusPresent"]);
$Params["StatusLate"] = intranet_htmlspecialchars($_REQUEST["StatusLate"]);
$Params["StatusAbsent"] = intranet_htmlspecialchars($_REQUEST["StatusAbsent"]);
$Params["StatusOuting"] = intranet_htmlspecialchars($_REQUEST["StatusOuting"]);
$Params["StatusEarlyLeave"] = intranet_htmlspecialchars($_REQUEST["StatusEarlyLeave"]);
$Params["StatusHoliday"] = intranet_htmlspecialchars($_REQUEST["StatusHoliday"]);
$Params["StatusAbsentSuspected"] = intranet_htmlspecialchars($_REQUEST["StatusAbsentSuspected"]);
$Params["ColAll"] = intranet_htmlspecialchars($_REQUEST["ColAll"]);
$Params["ColStaffName"] = intranet_htmlspecialchars($_REQUEST["ColStaffName"]);
$Params["ColGroupName"] = intranet_htmlspecialchars($_REQUEST["ColGroupName"]);
$Params["ColStatus"] = intranet_htmlspecialchars($_REQUEST["ColStatus"]);
$Params["ColTime"] = intranet_htmlspecialchars($_REQUEST["ColTime"]);
$Params["ColStation"] = intranet_htmlspecialchars($_REQUEST["ColStation"]);
$Params["ColSlotName"] = intranet_htmlspecialchars($_REQUEST["ColSlotName"]);
$Params["ColReason"] = intranet_htmlspecialchars($_REQUEST["ColReason"]);
$Params["ColLateMins"] = intranet_htmlspecialchars($_REQUEST["ColLateMins"]);
$Params["ColEarlyLeaveMins"] = intranet_htmlspecialchars($_REQUEST["ColEarlyLeaveMins"]);
$Params["ColWorkingdays"] = intranet_htmlspecialchars($_REQUEST["ColWorkingdays"]);
$Params["ColRemark"] = intranet_htmlspecialchars($_REQUEST["ColRemark"]);
$Params["ColDoctorCertificate"] = intranet_htmlspecialchars($_REQUEST["ColDoctorCertificate"]);
$Params["ColOTHours"] = intranet_htmlspecialchars($_REQUEST["ColOTHours"]);
$Params["ColDeductHours"] = intranet_htmlspecialchars($_REQUEST["ColDeductHours"]);
$Params["ColOffDuty"] = intranet_htmlspecialchars($_REQUEST["ColOffDuty"]);
$Params["ColDutyTime"] = intranet_htmlspecialchars($_REQUEST["ColDutyTime"]);
$Params["Task"] = intranet_htmlspecialchars(strtoupper($_REQUEST["Task"]));
if($Params["Task"]=="PRINT" || $Params["Task"]=="EXPORT"){
	$Params["SelectStaff"] = IntegerSafe($_REQUEST["HiddenSelectStaff"]);
}
$Params["SelectWaived"] = intranet_htmlspecialchars($_REQUEST["SelectWaived"]);
$Params["HaveRecord"] = intranet_htmlspecialchars($_REQUEST["HaveRecord"]);
$Params["DisplayAllCalendarDay"] = intranet_htmlspecialchars($_REQUEST["DisplayAllCalendarDay"]);
$Params["DisplayStatus"] = intranet_htmlspecialchars($_REQUEST['DisplayStatus']);

if($Params["Task"]=="PRINT") include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");
echo $StaffAttend3UI->Get_Report_Customize_Attendance_Report_Table($Params);
if($Params["Task"]=="PRINT") include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_footer.php");

intranet_closedb();
?>