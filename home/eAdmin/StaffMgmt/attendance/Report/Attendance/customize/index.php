<?php
// editing by 
/*
 * 2019-07-23 (Ray): Add Duty Time
 * 2018-12-14 (Carlos): Fix save/load template settings for ColOTHours.
 * 2018-01-10 (Carlos): $sys_custom['StaffAttendance']['WongFutNamCollege'] - added [OT hours] and [Deduct hours] options for detailed report.
 * 2015-10-07 (Carlos): added option Display Status as Icon or Symbol.
 * 2014-01-09 (Carlos): added option [DisplayAllCalendarDay]
 * 2012-08-28 (Carlos): added display option [Doctor Certificate] for detail records
 */
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend2.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!$StaffAttend3->Check_Access_Right('REPORT-CUSTOMIZE'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	header ("Location: /");
	intranet_closedb();
	exit();
}
$linterface = new interface_html();
$StaffAttend3UI = new libstaffattend3_ui();

$CurrentPageArr['eAdminStaffAttendance'] = 1;
$CurrentPage['ReportCustomizeAttendance'] = 1;

$TAGS_OBJ[] = array($Lang['StaffAttendance']['CustomizeAttendanceReport'], "", 1);
$MODULE_OBJ = $StaffAttend3UI->GET_MODULE_OBJ_ARR();
//$TAGS_OBJ_RIGHT[] = array(gen_online_help_btn_and_layer('staff_attendance','cust_rpt'));
$linterface->LAYOUT_START(urldecode($_REQUEST['Msg']));

echo $StaffAttend3UI->Get_Report_Customize_Attendance_Report_Index();

$linterface->LAYOUT_STOP();
intranet_closedb();
?>
<script type="text/JavaScript" language="JavaScript">
// dom function 
{
// Show/Hide the options layer
function ToggleOptions(ButtonLayerID, OptionLayerID)
{
	var $ButtonObj = $('#'+ButtonLayerID);
	var $OptionObj = $('#'+OptionLayerID);
	
	if($OptionObj.css('display') == 'none')
	{
		if(!$OptionObj.hasClass('report_show_option'))
		{
			$OptionObj.addClass('report_show_option');
		}
		$OptionObj.css('display','');
		$('#'+ButtonLayerID+' a').html('<img height="20" width="20" border="0" align="absmiddle" src="<?=$PATH_WRT_ROOT?>images/<?=$LAYOUT_SKIN?>/icon_hide_option.gif"/><?=$Lang['StaffAttendance']['HideReportOptions']?>');
		$ButtonObj.css('display','');
	}else
	{
		if(!$OptionObj.hasClass('report_show_option'))
		{
			$OptionObj.addClass('report_show_option');
		}
		$OptionObj.css('display','none');
		$('#'+ButtonLayerID+' a').html('<img height="20" width="20" border="0" align="absmiddle" src="<?=$PATH_WRT_ROOT?>images/<?=$LAYOUT_SKIN?>/icon_show_option.gif"/><?=$Lang['StaffAttendance']['ShowReportOptions']?>');
		$ButtonObj.css('display','');
	}
}

function SwitchLayers(ShowLayerID, HideLayerID)
{
	var $ShowLayer = $('#'+ShowLayerID);
	var $HideLayer = $('#'+HideLayerID);
	
	$ShowLayer.css('display','');
	$HideLayer.css('display','none');
}

function SwitchColumnOptions(ChoiceID)
{
	if(ChoiceID == 'FormatDetails')
	{
		// Show options: In-Out Time, Slot Name, Reason, Late Mins
		$('#ColWorkingdaysLayer').css('display','none');
		$('#ColStatusLayer').css('display','');
		$('#ColTimeLayer').css('display','');
		$('#ColStationLayer').css('display','');
		$('#ColSlotNameLayer').css('display','');
		$('#ColReasonLayer').css('display','');
		$('#ColLateMinsLayer').css('display','');
		$('#ColEarlyLeaveMinsLayer').css('display','');
		$('#ColRemarkLayer').css('display','');
		 $('#ColOTHoursLayer').css('display','');
<?php if($sys_custom['StaffAttendance']['DoctorCertificate']){ ?>
		$('#ColDoctorCertificateLayer').css('display','');
<?php } ?>
<?php if($sys_custom['StaffAttendance']['WongFutNamCollege']){ ?>
		 $('#ColDeductHoursLayer').css('display','');
<?php } ?>
		$('#ColOffDutyLayer').css('display','none');
	}else if(ChoiceID == 'FormatSummary')
	{
		// Hide options: In-Out Time, Slot Name, Reason, Late Mins
		$('#ColWorkingdaysLayer').css('display','');
		$('#ColTimeLayer').css('display','');
		$('#ColStatusLayer').css('display','none');
		$('#ColStationLayer').css('display','none');
		$('#ColSlotNameLayer').css('display','none');
		$('#ColReasonLayer').css('display','none');
		$('#ColLateMinsLayer').css('display','none');
		$('#ColEarlyLeaveMinsLayer').css('display','none');
		$('#ColRemarkLayer').css('display','none');
		$('#ColOTHoursLayer').css('display','none');
<?php if($sys_custom['StaffAttendance']['DoctorCertificate']){ ?>
		$('#ColDoctorCertificateLayer').css('display','none');
<?php } ?>
<?php if($sys_custom['StaffAttendance']['WongFutNamCollege']){ ?>
		$('#ColDeductHoursLayer').css('display','none');
<?php } ?>
		$('#ColOffDutyLayer').css('display','');
	}
}

function Uncheck(IsChecked, TargetCB)
{
	var targetCheckbox = document.getElementById(TargetCB);
	if(!IsChecked) targetCheckbox.checked = false;
}

function CheckAll(IsChecked, ElementName)
{
	var targetElements = document.getElementsByName(ElementName);
	if(targetElements != null)
	{
		for(var i=0;i<targetElements.length;i++)
		{
			targetElements[i].checked = IsChecked;
		}
	}
}

function Check_Valid_Date(DateObjId, WarningLayerId)
{
	var dateObj = document.getElementById(DateObjId);
	var $WarningLayer = $('#'+WarningLayerId);
	if(check_date_without_return_msg(dateObj))
	{
		$('#WarningLayer').html('');
		$WarningLayer.css('display','none');
		return true;
	}else
	{
		dateObj.focus();
		$('#WarningLayer').html('<?=$Lang['LessonAttendance']['InvalidDateFormat']?>');
		$WarningLayer.css('display','');
		SetTimerToHideWarning(WarningLayerId, 5000);
		return false;
	}
}

function SetTimerToHideWarning(WarningLayerId, ticks)
{
	var action = "$('#"+WarningLayerId+"').css('display','none');";
	setTimeout(action,ticks);
}

function SelectAll(obj)
{
	for (i=0; i<obj.length; i++)
	{
		obj.options[i].selected = true;
	}
}
}

// ajax function
{
function Generate_Report()
{
	var SelectStaff, Format, PeriodType;
	var TargetStartDate = $('#TargetStartDate').val();
	var TargetEndDate = $('#TargetEndDate').val();
	
//	if($('#ReportTypePersonal').attr('checked')) ReportType = $('#ReportTypePersonal').val();
//	if($('#ReportTypeGroup').attr('checked')) ReportType = $('#ReportTypeGroup').val();
	SelectStaff = Get_Selection_Value("SelectStaff[]"); 
	if(SelectStaff.length == 0){
		$('span#WarningLayer').html('<?=$Lang['StaffAttendance']['EntryLogSelectStaffWarning']?>');
		$('tr#WarningRow').css('display','');
		return;
	}else{
		$('span#WarningLayer').html('');
		$('tr#WarningRow').css('display','none');
	}
	
	if($('#FormatDetails').attr('checked')) Format = $('#FormatDetails').val();
	if($('#FormatSummary').attr('checked')) Format = $('#FormatSummary').val();
	
	if($('#PeriodTypeMonthly').attr('checked')) PeriodType = $('#PeriodTypeMonthly').val();
	if($('#PeriodTypeMonthlyByStaff').attr('checked')) PeriodType = $('#PeriodTypeMonthlyByStaff').val();
	if($('#PeriodTypeWeekly').attr('checked')) PeriodType = $('#PeriodTypeWeekly').val();
	if($('#PeriodTypeDaily').attr('checked')) PeriodType = $('#PeriodTypeDaily').val();
	
	var PostVar = {
//			"ReportType":ReportType,
//			"StaffID": $('#SelectPersonal').val(),
//			"GroupID": $('#SelectGroup').val(),
			"SelectStaff[]": SelectStaff,
			"PeriodType": PeriodType,
			"TargetStartDate": TargetStartDate,
			"TargetEndDate": TargetEndDate,
			"Format": Format,
			"StatusAll": ($('#StatusAll').attr('checked')?1:0),
			"StatusPresent": ($('#StatusPresent').attr('checked')?1:0),
			"StatusLate": ($('#StatusLate').attr('checked')?1:0),
			"StatusAbsent": ($('#StatusAbsent').attr('checked')?1:0),
			"StatusOuting": ($('#StatusOuting').attr('checked')?1:0),
			"StatusEarlyLeave": ($('#StatusEarlyLeave').attr('checked')?1:0),
			"StatusHoliday": ($('#StatusHoliday').attr('checked')?1:0),
			"StatusAbsentSuspected": ($('#StatusAbsentSuspected').attr('checked')?1:0),
			"ColAll": ($('#ColAll').attr('checked')?1:0),
			"ColStaffName": ($('#ColStaffName').attr('checked')?1:0),
			"ColGroupName": ($('#ColGroupName').attr('checked')?1:0),
			"ColWorkingdays": ($('#ColWorkingdays').attr('checked')?1:0),
			"ColStatus": ($('#ColStatus').attr('checked')?1:0),
			"ColTime": ($('#ColTime').attr('checked')?1:0),
			"ColStation": ($('#ColStation').attr('checked')?1:0),
			"ColSlotName": ($('#ColSlotName').attr('checked')?1:0),
			"ColReason": ($('#ColReason').attr('checked')?1:0),
			"ColLateMins": ($('#ColLateMins').attr('checked')?1:0),
			"ColEarlyLeaveMins": ($('#ColEarlyLeaveMins').attr('checked')?1:0),
			"ColRemark": ($('#ColRemark').attr('checked')?1:0),
			"ColOTHours": ($('#ColOTHours').attr('checked')?1:0),
<?php if($sys_custom['StaffAttendance']['DoctorCertificate']){ ?>
			"ColDoctorCertificate": ($('#ColDoctorCertificate').attr('checked')?1:0),
<?php } ?>
<?php if($sys_custom['StaffAttendance']['WongFutNamCollege']){ ?>
			"ColDeductHours": ($('#ColDeductHours').attr('checked')?1:0),
<?php } ?>
			"ColOffDuty": ($('#ColOffDuty').attr('checked')?1:0),
            "ColDutyTime": ($('#ColDutyTime').attr('checked')?1:0),
			"SelectWaived": $('#SelectWaived').val(),
			"HaveRecord": $('#HaveRecord').val(),
			"DisplayAllCalendarDay": $('#DisplayAllCalendarDay').val(),
			"DisplayStatus": $('input[name=DisplayStatus]:checked').val()
		}
	
	if(!Check_Valid_Date('TargetStartDate', 'WarningRow') || !Check_Valid_Date('TargetEndDate', 'WarningRow'))
	{
		return;
	}
	if(TargetStartDate > TargetEndDate)
	{
		$('#WarningLayer').html('<?=$Lang['StaffAttendance']['InvalidDateRange']?>');
		$('#WarningRow').css('display','');
		SetTimerToHideWarning('WarningRow', 5000);
		return;
	}
	
	Block_Element("ReportTableLayer");
	$.post('ajax_get_customize_attendance_report.php',PostVar,
					function(data){
						if (data == "die") 
							window.top.location = '/';
						else {
							$('div#ReportTableLayer').html(data);
							ToggleOptions('ToggleButtonLayer', 'ReportOptionLayer');
							Thick_Box_Init();
							UnBlock_Element("ReportTableLayer");
						}
					});
}

function Save_Settings()
{
	var PostVar = {
			"action":"save",
			"SettingTemplate": $('#SettingTemplate').val(),
			"SettingTemplateName": encodeURIComponent($('#SettingTemplate option:selected').text()),
			"SelectStaffLevel1[]": Get_Selection_Value("SelectStaffLevel1[]"),
			"SelectStaff[]": Get_Selection_Value("SelectStaff[]"),
			"PeriodTypeMonthly": ($('#PeriodTypeMonthly').attr('checked')?1:0),
			"PeriodTypeWeekly": ($('#PeriodTypeWeekly').attr('checked')?1:0),
			"PeriodTypeDaily": ($('#PeriodTypeDaily').attr('checked')?1:0),
			"TargetStartDate": $('#TargetStartDate').val(),
			"TargetEndDate": $('#TargetEndDate').val(),
			"StatusAll": ($('#StatusAll').attr('checked')?1:0),
			"StatusPresent": ($('#StatusPresent').attr('checked')?1:0),
			"StatusLate": ($('#StatusLate').attr('checked')?1:0),
			"StatusAbsent": ($('#StatusAbsent').attr('checked')?1:0),
			"StatusOuting": ($('#StatusOuting').attr('checked')?1:0),
			"StatusEarlyLeave": ($('#StatusEarlyLeave').attr('checked')?1:0),
			"StatusHoliday": ($('#StatusHoliday').attr('checked')?1:0),
			"StatusAbsentSuspected": ($('#StatusAbsentSuspected').attr('checked')?1:0),
			"SelectWaived": $('#SelectWaived').val(),
			"FormatDetails": ($('#FormatDetails').attr('checked')?1:0),
			"FormatSummary": ($('#FormatSummary').attr('checked')?1:0),
			"HaveRecord": $('#HaveRecord').val(),
			"ColAll": ($('#ColAll').attr('checked')?1:0),
			"ColStaffName": ($('#ColStaffName').attr('checked')?1:0),
			"ColGroupName": ($('#ColGroupName').attr('checked')?1:0),
			"ColWorkingdays": ($('#ColWorkingdays').attr('checked')?1:0),
			"ColStatus": ($('#ColStatus').attr('checked')?1:0),
			"ColTime": ($('#ColTime').attr('checked')?1:0),
			"ColStation": ($('#ColStation').attr('checked')?1:0),
			"ColSlotName": ($('#ColSlotName').attr('checked')?1:0),
			"ColReason": ($('#ColReason').attr('checked')?1:0),
			"ColLateMins": ($('#ColLateMins').attr('checked')?1:0),
			"ColEarlyLeaveMins": ($('#ColEarlyLeaveMins').attr('checked')?1:0),
			"ColRemark": ($('#ColRemark').attr('checked')?1:0),
			"ColOffDuty": ($('#ColOffDuty').attr('checked')?1:0),
            "ColDutyTime": ($('#ColDutyTime').attr('checked')?1:0),
			"PeriodTypeMonthlyByStaff": ($('#PeriodTypeMonthlyByStaff').attr('checked')?1:0),
			"DisplayAllCalendarDay": $('#DisplayAllCalendarDay').val(),
			"DisplayStatus": $('input[name="DisplayStatus"]:checked').val(),
			"ColOTHours": ($('#ColOTHours').attr('checked')?1:0)
		}
	$.post(
		"ajax_task.php",
		PostVar,
		function(data){
			if(data=="1"){
				Refresh_Setting_Template_Selection();
				Get_Return_Message(' <?=$Lang['StaffAttendance']['SettingsSaveSuccess']?> ');
				Scroll_To_Top();
			}else{
				Get_Return_Message(' <?=$Lang['StaffAttendance']['SettingsSaveFail']?> ');
				Scroll_To_Top();
			}
		}
	);
}

function Load_Settings()
{
	var SettingTemplateID = $('#SettingTemplate').val();
	if(SettingTemplateID==''){
		$('input#btnAdd').css('display','');
		$('input#btnSave').css('display','none');
		$('input#btnDelete').css('display','none');
		return;
	}
	$.post(
		"ajax_task.php",
		{
			"action":"load",
			"SettingTemplate":SettingTemplateID 
		},
		function(data){
			$('input#btnAdd').css('display','none');
			$('input#btnSave').css('display','');
			$('input#btnDelete').css('display','');
			if(data==""){
				Get_Return_Message(' <?=$Lang['StaffAttendance']['SettingsLoadFail']?> ');
				Scroll_To_Top();
			}else{
				var PartArr = data.split('###',3);
				var StaffLevelArr = PartArr[0].split(',');
				$('#SelectStaffLevel1\\[\\] option').each(
					function(i){
						var $t = $(this);
						$t.attr('selected',false);
						for(var j=0;j<StaffLevelArr.length;j++){
							if($t.val()==StaffLevelArr[j]) $t.attr('selected',true);
						}
					}
				);
				$('#SelectStaff\\[\\]').html(PartArr[1]);
				// trick to force IE update UI
				var tmp_parent = $('#SelectStaff\\[\\]').parent();
				tmp_parent.html(tmp_parent.html());
				var settings = PartArr[2].split(',');
				
				var format_details = $('#FormatDetails');
				var format_summary = $('#FormatSummary');
				
				$('#PeriodTypeMonthly').attr('checked',(settings[0]==1?true:false));
				$('#PeriodTypeWeekly').attr('checked',(settings[1]==1?true:false));
				$('#PeriodTypeDaily').attr('checked',(settings[2]==1?true:false));
				$('#TargetStartDate').val(settings[3]);
				$('#TargetEndDate').val(settings[4]);
				$('#StatusAll').attr('checked',(settings[5]==1?true:false));
				$('#StatusPresent').attr('checked',(settings[6]==1?true:false));
				$('#StatusLate').attr('checked',(settings[7]==1?true:false));
				$('#StatusAbsent').attr('checked',(settings[8]==1?true:false));
				$('#StatusOuting').attr('checked',(settings[9]==1?true:false));
				$('#StatusEarlyLeave').attr('checked',(settings[10]==1?true:false));
				$('#StatusHoliday').attr('checked',(settings[11]==1?true:false));
				$('#StatusAbsentSuspected').attr('checked',(settings[12]==1?true:false));
				$('#SelectWaived').val(settings[13]);
				format_details.attr('checked',(settings[14]==1?true:false));
				format_summary.attr('checked',(settings[15]==1?true:false));
				$('#HaveRecord').val(settings[16]);
				$('#ColAll').attr('checked',(settings[17]==1?true:false));
				$('#ColStaffName').attr('checked',(settings[18]==1?true:false));
				$('#ColGroupName').attr('checked',(settings[19]==1?true:false));
				$('#ColStatus').attr('checked',(settings[20]==1?true:false));
				$('#ColTime').attr('checked',(settings[21]==1?true:false));
				$('#ColStation').attr('checked',(settings[22]==1?true:false));
				$('#ColSlotName').attr('checked',(settings[23]==1?true:false));
				$('#ColReason').attr('checked',(settings[24]==1?true:false));
				$('#ColLateMins').attr('checked',(settings[25]==1?true:false));
				$('#ColEarlyLeaveMins').attr('checked',(settings[26]==1?true:false));
				$('#ColWorkingdays').attr('checked',(settings[27]==1?true:false));
				$('#ColRemark').attr('checked',(settings[28]==1?true:false));
				$('#ColOffDuty').attr('checked',(settings[29]==1?true:false));
				$('#PeriodTypeMonthlyByStaff').attr('checked',(settings[30]==1?true:false));
				$('#DisplayAllCalendarDay').val(settings[31]);
				if(settings[32]=='2'){
					$('#UseSymbolColor').attr('checked',true);
				}else{
					$('#UseIcon').attr('checked',true);
				}
				$('#ColOTHours').attr('checked', (settings[33]==1?true:false));
                $('#ColDutyTime').attr('checked',(settings[34]==1?true:false));
				if(format_details.attr('checked')) SwitchColumnOptions('FormatDetails');
				if(format_summary.attr('checked')) SwitchColumnOptions('FormatSummary');
				
				Get_Return_Message(' <?=$Lang['StaffAttendance']['SettingsLoadSuccess']?> ');
				Scroll_To_Top();
			}
		}
	);
}

function Get_Staff_Selection()
{
	var PostVar = {
			"action":"get_staff_list",
			"SelectStaffLevel1[]": Get_Selection_Value("SelectStaffLevel1[]")
		};
	$.post(
		"ajax_task.php",
		PostVar,
		function(data){
			if(data!=""){
				$('#SelectStaff\\[\\]').html(data);
				// trick to force IE update UI
				var tmp_parent = $('#SelectStaff\\[\\]').parent();
				tmp_parent.html(tmp_parent.html());
			}
		}
	);
}

function Get_Setting_Template_Form(action)
{ 
	if(action=="add")
	{
		$.post(
			'ajax_task.php',
			{
				"action":"add"
			},
			function(data) {
				if (data == "die") 
					window.top.location = '/';
				else 
					$('div#TB_ajaxContent').html(data);
			}
		);
	}else if(action=="manage")
	{
		
	}
}

function Prepare_Save_Setting()
{
	if(confirm('<?=$Lang['StaffAttendance']['ConfirmOverwriteSettingTemplate']?>')) Save_Settings();
}

function Delete_Setting_Template()
{
	if(confirm('<?=$Lang['StaffAttendance']['ConfirmDeleteSettingTemplate']?>')){
		$.post(
			'ajax_task.php',
			{
				"action":"delete",
				"SettingID":$('#SettingTemplate').val() 
			},
			function(data){
				if(data==1){
					Refresh_Setting_Template_Selection();
					Get_Return_Message(' <?=$Lang['StaffAttendance']['SettingsDeleteSuccess']?> ');
					Scroll_To_Top();
				}else{
					Get_Return_Message(' <?=$Lang['StaffAttendance']['SettingsDeleteFail']?> ');
					Scroll_To_Top();
				}
			}
		);
	}
}

function Check_Setting_Template(save)
{
	$.post(
		'ajax_task.php',
		{
			"action":"check",
			"SettingTemplateName":encodeURIComponent($('#SettingTemplateName').val()) 
		},
		function(data) {
			if (data == "1"){
				$('#TemplateNameWarningLayer').html('');
				$('#TemplateNameWarningRow').css('display','none');
				if(save==true){
					$('#SettingTemplate').append('<option value="" selected="selected">'+$('#SettingTemplateName').val()+'</option>');
					Save_Settings();
					window.top.tb_remove();
				}
			}else if(data=="0"){
				// warning
				$('#TemplateNameWarningLayer').html('<?=$Lang['StaffAttendance']['InvalidSettingTemplateName']?>');
				$('#TemplateNameWarningRow').css('display','');
			}
		}
	);
}

function Refresh_Setting_Template_Selection()
{
	var PostVar = {
			"action":"get_setting_selection",
			"SettingTemplateName": encodeURIComponent($('#SettingTemplate option:selected').text())
		};
	$.post(
		"ajax_task.php",
		PostVar,
		function(data){
			if(data!=""){
				$('#SettingTemplate').html(data);
				var SettingTemplateID = $('#SettingTemplate').val();
				if(SettingTemplateID==''){
					$('input#btnAdd').css('display','');
					$('input#btnSave').css('display','none');
					$('input#btnDelete').css('display','none');
				}else{
					$('input#btnAdd').css('display','none');
					$('input#btnSave').css('display','');
					$('input#btnDelete').css('display','');
				}
			}
		}
	);
}
}

// thick box function 
{
//on page load call tb_init
function Thick_Box_Init()
{   
	tb_init('input.thickbox');//pass where to apply thickbox
}
}

$(document).ready(function(){
	Thick_Box_Init();
});
</script>