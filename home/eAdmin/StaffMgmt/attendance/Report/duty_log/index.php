<?php
// Modifying by:
// #############################
// Date: 2019-08-15 (Ray): Created
//
// #############################
$PATH_WRT_ROOT = "../../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend2.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();


$StaffAttend3 = new libstaffattend3();
if ((!$StaffAttend3->IS_ADMIN_USER() &&
		!$StaffAttend3->Check_Access_Right('REPORT-DUTYCHANGELOG'))
	|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	intranet_closedb();
	header ("Location: /");
	exit();
}

if ($page_size_change == 1) {
	setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
	$ck_page_size = $numPerPage;
}

# preserve table view
if ($ck_duty_change_log_setting_page_number!=$pageNo && $pageNo!="")
{
	setcookie("ck_duty_change_log_setting_page_number", $pageNo, 0, "", "", 0);
	$ck_duty_change_log_setting_page_number = $pageNo;
} else if (!isset($pageNo) && $ck_duty_change_log_setting_page_number!="")
{
	$pageNo = $ck_duty_change_log_setting_page_number;
}

if ($ck_duty_change_log_setting_page_order!=$order && $order!="")
{
	setcookie("ck_duty_change_log_setting_page_order", $order, 0, "", "", 0);
	$ck_duty_change_log_setting_page_order = $order;
} else if (!isset($order) && $ck_duty_change_log_setting_page_order!="")
{
	$order = $ck_duty_change_log_setting_page_order;
}

if ($ck_duty_change_log_setting_page_field!=$field && $field!="")
{
	setcookie("ck_duty_change_log_setting_page_field", $field, 0, "", "", 0);
	$ck_duty_change_log_setting_page_field = $field;
} else if (!isset($field) && $ck_duty_change_log_setting_page_field!="")
{
	$field = $ck_duty_change_log_setting_page_field;
}

$linterface = new interface_html();
$StaffAttend3UI = new libstaffattend3_ui();


$StartDate = $StartDate ? $StartDate : date("Y-m-d");
$EndDate = $EndDate ? $EndDate : date("Y-m-d");

// TABLE INFO
if (isset($ck_page_size) && $ck_page_size != "")
	$page_size = $ck_page_size;
$pageSizeChangeEnabled = true;
if ($field == "" && $order=="") {
	$field = 0;
	$order = 0;
}
$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array(
	"a.LogDate",
	"logby",
	"logType"
);

$name_field = getNameFieldWithClassNumberByLang("b.");
$sql = "select 
		a.LogDate,
		$name_field as logby,
		case 
			when a.Section='Regular_Duty' then '".$Lang['StaffAttendance']['BasicDutySetting'] ."'
			when a.Section='Special_Duty' then '".$Lang['StaffAttendance']['SpecialDutySetting']."'
			when a.Section='Attendance_Record' then '".$Lang['StudentAttendance']['AttendanceRecords']."'
		end as logType,
		a.RecordDetail
		from 
			CARD_STAFF_ATTENDANCE3_RECORD_CHANGE_LOG as a 
			INNER join INTRANET_USER as b on (b.UserID = a.LogBy)
		where
			left(a.LogDate,10) >= '$StartDate' and left(a.LogDate,10) <= '$EndDate'";

$li->sql = $sql;
$li->no_col = 4;
$li->IsColOff = "GeneralDisplayWithNavigation";

// TABLE COLUMN
$pos = 0;
$li->column_list .= "<th>" . $li->column($pos++, $Lang['General']['LastModified']) . "</th>\n";
$li->column_list .= "<th>" . $li->column($pos++, $Lang['General']['LastModifiedBy']) . "</th>\n";
$li->column_list .= "<th>" . $li->column($pos++, $Lang['ePOS']['LogType']) . "</th>\n";
$li->column_list .= "<th>" . $Lang['eInventory']['RecordInfo'] . "</th>\n";


// ## Filter - Date Range
$date_select = $eNotice['Period_Start'] . ": ";
$date_select .= $linterface->GET_DATE_PICKER("StartDate", $StartDate);
$date_select .= $eNotice['Period_End'] . "&nbsp;";
$date_select .= $linterface->GET_DATE_PICKER("EndDate", $EndDate);
$date_select .= $linterface->GET_SMALL_BTN($button_submit, "submit");

$CurrentPageArr['eAdminStaffAttendance'] = 1;

$CurrentPage['DutyChangeLog'] = 1;
$TAGS_OBJ[] = array($Lang['StaffAttendance']['DutyChangeLog'], "", 1);


$MODULE_OBJ = $StaffAttend3UI->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START(urldecode($_REQUEST['Msg']));
?>
<SCRIPT LANGUAGE=Javascript>
    <!--//
    function check_form()
    {
        obj = document.form1;

        if(!check_date(obj.StartDate,"<?=$i_invalid_date?>"))
        {
            obj.StartDate.focus();
            return false;
        }

        if(!check_date(obj.EndDate,"<?=$i_invalid_date?>"))
        {
            obj.EndDate.focus();
            return false;
        }

        if(compareDate(obj.EndDate.value, obj.StartDate.value)<0) {
            obj.StartDate.focus();
            alert("<?=$i_con_msg_date_startend_wrong_alert?>");
            return false;
        }

        return true;
    }
    //-->
</SCRIPT>

<form name="form1" method="get" action="index.php" onSubmit="return check_form();">

	<div class="table_board">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td valign="bottom">
					<?=$date_select?>
				</td>
			</tr>
		</table>
	</div>

	<?=$li->display();?>


	<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
	<input type="hidden" name="order" value="<?php echo $li->order; ?>" />
	<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
	<input type="hidden" name="page_size_change" value="<?=$page_size_change?>" />
	<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />
</form>

<?php


$linterface->LAYOUT_STOP();
intranet_closedb();
?>
