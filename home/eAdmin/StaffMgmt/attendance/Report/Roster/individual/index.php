<?php
// editing by 
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend2.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!$StaffAttend3->Check_Access_Right('REPORT-ROSTER'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$linterface = new interface_html();
$StaffAttend3UI = new libstaffattend3_ui();

$CurrentPageArr['eAdminStaffAttendance'] = 1;
$CurrentPage['ReportRoster'] = 1;

$TAGS_OBJ[] = array($Lang['StaffAttendance']['RosterReport'], "", 0);
$MODULE_OBJ = $StaffAttend3UI->GET_MODULE_OBJ_ARR();
//$TAGS_OBJ_RIGHT[] = array(gen_online_help_btn_and_layer('staff_attendance','roster_rpt'));
$linterface->LAYOUT_START(urldecode($_REQUEST['Msg']));

echo $StaffAttend3UI->Get_Report_Roster_Individual_Index($TargetMonth, $TargetYear, $Keyword);

$linterface->LAYOUT_STOP();
intranet_closedb();
?>
<script type="text/JavaScript" language="JavaScript">
// dom function 
{
function Check_Go_Search_Roster_Individual(evt)
{
	var key = evt.which || evt.charCode || evt.keyCode;
	
	if (key == 13) // enter
	{
		Get_Report_Roster_Individual_List();
	}
	else
		return false;
}

function Check_Go_Search_Roster_Group_Calendar_Table(evt)
{
	var key = evt.which || evt.charCode || evt.keyCode;
	
	if (key == 13) // enter
	{
		Get_Report_Roster_Group_Calendar_Table();
	}
	else
		return false;
}

function ToggleLayer(ButtonLayerID, LayerID)
{
	var $ButtonObj = $('#'+ButtonLayerID+' a'); 
	var $LayerObj = $('#'+LayerID);
	if($LayerObj.css('display')=='none')
	{
		$ButtonObj.removeClass('title_off');
		$ButtonObj.addClass('title_on');
		$LayerObj.css('display','');
	}else
	{
		$ButtonObj.removeClass('title_on');
		$ButtonObj.addClass('title_off');
		$LayerObj.css('display','none');
	}
}

function ToggleAllLayer(ButtonLayerID)
{
	var $ButtonObj = $('#'+ButtonLayerID+' a'); 
	var $TitleObjs = $('#DutyBlockTitle_*_* a');
	var $BlockObjs = $('#DutyBlock_*_* a');
	if($ButtonObj.hasClass('zoom_in'))
	{
		$ButtonObj.removeClass('zoom_in');
		$ButtonObj.addClass('zoom_out');
		$ButtonObj.text('<?=$Lang['StaffAttendance']['HideDetail']?>');
		
		$("div[id*='DutyBlockTitle'] a").removeClass('title_off');
		$("div[id*='DutyBlockTitle'] a").addClass('title_on');
		$("div[id*='DutyBlock_']").css('display','');
	}else
	{
		$ButtonObj.removeClass('zoom_out');
		$ButtonObj.addClass('zoom_in');
		$ButtonObj.text('<?=$Lang['StaffAttendance']['ShowDetail']?>');
		
		$("div[id*='DutyBlockTitle'] a").removeClass('title_on');
		$("div[id*='DutyBlockTitle'] a").addClass('title_off');
		$("div[id*='DutyBlock_']").css('display','none');
	}
}
}

// ajax function
{
function Get_Report_Roster_Individual_List(TargetMonth,TargetYear)
{
	var TargetMonth = TargetMonth || '';
	var TargetYear = TargetYear || '';
	var PostVar = {
		"Keyword":encodeURIComponent($('input#Keyword').val()),
		"TargetMonth":TargetMonth,
		"TargetYear":TargetYear
		};
	
	Block_Element("IndividualIndexLayer");
	$.post('ajax_get_roster_individual_list.php',PostVar,
					function(data){
						if (data == "die") 
							window.top.location = '/';
						else {
							$('div#IndividualIndexLayer').html(data);
							Thick_Box_Init();
							UnBlock_Element("IndividualIndexLayer");
						}
					});
}

function Get_Report_Roster_Group_Calendar_Index(ID, TargetMonth, TargetYear, GroupOrIndividual)
{
	var ID = ID || '';
	var TargetMonth = TargetMonth || '';
	var TargetYear = TargetYear || '';
	var GroupOrIndividual = GroupOrIndividual || 'Individual';
	var PostVar = {
		"ID":ID,
		"GroupOrIndividual":GroupOrIndividual,
		"Keyword":encodeURIComponent($('input#Keyword').val()),
		"TargetMonth":TargetMonth,
		"TargetYear":TargetYear
		};
	
	Block_Element("RosterReportLayer");
	$.post('ajax_get_roster_individual_calendar_index.php',PostVar,
					function(data){
						if (data == "die") 
							window.top.location = '/';
						else {
							$('div#RosterReportLayer').html(data);
							Thick_Box_Init();
							UnBlock_Element("RosterReportLayer");
						}
					});
}

function Get_Report_Roster_Group_Calendar_Table(ID, TargetMonth, TargetYear, GroupOrIndividual)
{
	var ID = ID || $('#GroupList').val();
	var TargetMonth = TargetMonth || $('#TargetMonth').val();
	var TargetYear = TargetYear || $('#TargetYear').val();
	var GroupOrIndividual = GroupOrIndividual || 'Individual';
	var PostVar = {
		"ID":ID,
		"GroupOrIndividual":GroupOrIndividual,
		"Keyword":encodeURIComponent($('input#Keyword').val()),
		"TargetMonth":TargetMonth,
		"TargetYear":TargetYear
		};
	
	Block_Element("RosterTableLayer");
	$.post('ajax_get_roster_individual_calendar_table.php',PostVar,
					function(data){
						if (data == "die") 
							window.top.location = '/';
						else {
							$('div#RosterTableLayer').html(data);
							Thick_Box_Init();
							UnBlock_Element("RosterTableLayer");
						}
					});
}
}


// thick box function 
{
//on page load call tb_init
function Thick_Box_Init()
{   
	tb_init('a.thickbox');//pass where to apply thickbox
}
}
</script>