<?php
// editing by 
/******************************* Changes ***********************************
 * 2011-7-12 (Carlos): Added mode EntryLog
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!$StaffAttend3->Check_Access_Right('REPORT-ENTRYLOG'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	echo 'die';
	intranet_closedb();
	exit();
}

$StaffAttend3UI = new libstaffattend3_ui();

$StartDate = intranet_htmlspecialchars($_REQUEST['StartDate']);
$EndDate = intranet_htmlspecialchars($_REQUEST['EndDate']);
$TargetUser = IntegerSafe($_REQUEST['TargetUser']);
$Mode = intranet_htmlspecialchars($_REQUEST['Mode']);
$GroupBy = intranet_htmlspecialchars($_REQUEST['GroupBy']);
$ShowNoDutyDateOnly = ($_REQUEST['ShowNoDutyDateOnly'] == "1");
$HideWithoutData = ($_REQUEST['HideWithoutData'] == "1");
//For Print and Export Version
if($Mode=="Print" || $Mode=="Export" || $Mode=="EntryLog")
{
	$TargetUser = IntegerSafe($_REQUEST['HiddenTargetUser']); 
	$StartDate = intranet_htmlspecialchars($_REQUEST['HiddenStartDate']);
	$EndDate = intranet_htmlspecialchars($_REQUEST['HiddenEndDate']);
	$GroupBy = intranet_htmlspecialchars($_REQUEST['HiddenGroupBy']);
	$ShowNoDutyDateOnly = ($_REQUEST['HiddenShowNoDutyDateOnly'] == "1");
	$HideWithoutData = ($_REQUEST['HiddenHideWithoutData'] == "1");
}

if($Mode=="Print") include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");
echo $StaffAttend3UI->Get_Report_Entry_Log($StartDate, $EndDate, $TargetUser,$Mode,$GroupBy,$ShowNoDutyDateOnly,$HideWithoutData);
if($Mode=="Print") include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_footer.php");

intranet_closedb();
?>