<?php
// editing by 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend2.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!$StaffAttend3->Check_Access_Right('REPORT-ABSENCE'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$linterface = new interface_html();
$StaffAttend3UI = new libstaffattend3_ui();

$CurrentPageArr['eAdminStaffAttendance'] = 1;
$CurrentPage['ReportAbsence'] = 1;

$TAGS_OBJ[] = array($Lang['StaffAttendance']['AbsenceReport'], "", 1);
$MODULE_OBJ = $StaffAttend3UI->GET_MODULE_OBJ_ARR();
//$TAGS_OBJ_RIGHT[] = array(gen_online_help_btn_and_layer('staff_attendance','abs_log'));
$linterface->LAYOUT_START(urldecode($_REQUEST['Msg']));

echo $StaffAttend3UI->Get_Report_Absence_Index();

$linterface->LAYOUT_STOP();
intranet_closedb();
?>
<script type="text/JavaScript" language="JavaScript">
// dom functions
{
function SwitchLayers(ShowLayerID, HideLayerID)
{
	var $ShowLayer = $('#'+ShowLayerID);
	var $HideLayer = $('#'+HideLayerID);
	
	$ShowLayer.css('display','');
	$HideLayer.css('display','none');
}

function Check_Valid_Date(DateObjId, WarningLayerId)
{
	var dateObj = document.getElementById(DateObjId);
	var $WarningLayer = $('#'+WarningLayerId);
	if(check_date_without_return_msg(dateObj))
	{
		$WarningLayer.html('');
		$WarningLayer.css('visibility','hidden');
		return true;
	}else
	{
		dateObj.focus();
		$WarningLayer.html('<?=$Lang['LessonAttendance']['InvalidDateFormat']?>');
		$WarningLayer.css('visibility','visible');
		SetTimerToHideWarning(WarningLayerId, 3000);
		return false;
	}
}

function SetTimerToHideWarning(WarningLayerId, ticks)
{
	var action = "$('#"+WarningLayerId+"').css('visibility','hidden');";
	setTimeout(action,ticks);
}
}

// ajax function
{
function Generate_Report()
{
	var ReportType, Format;
	var TargetStartDate = $('#TargetStartDate').val();
	var TargetEndDate = $('#TargetEndDate').val();
	
	if($('#ReportTypePersonal').attr('checked')) ReportType = $('#ReportTypePersonal').val();
	if($('#ReportTypeGroup').attr('checked')) ReportType = $('#ReportTypeGroup').val();
	Format = $('#Format').val();
	
	var PostVar = {
			"ReportType":ReportType,
			"StaffID": $('#SelectPersonal').val(),
			"GroupID": $('#SelectGroup').val(),
			"TargetStartDate": TargetStartDate,
			"TargetEndDate": TargetEndDate,
			"Format": Format,
			"ColAll": ($('#ColAll').attr('checked')?1:0),
			"ColReason": ($('#ColReason').attr('checked')?1:0),
			"ColRemark": ($('#ColRemark').attr('checked')?1:0),
			"SelectWaived": $('#SelectWaived').val() 
		}
	
	if(!Check_Valid_Date('TargetStartDate', 'WarningLayer') || !Check_Valid_Date('TargetEndDate', 'WarningLayer'))
	{
		return;
	}
	if(TargetStartDate > TargetEndDate)
	{
		var $WarningLayer = $('#WarningLayer');
		$WarningLayer.html('<?=$Lang['StaffAttendance']['InvalidDateRange']?>');
		$WarningLayer.css('visibility','visible');
		SetTimerToHideWarning('WarningLayer', 3000);
		return;
	}
	
	if(Format==0)//WEB
	{
		Block_Element("ReportTableLayer");
		$.post('ajax_get_absence_report.php',PostVar,
						function(data){
							if (data == "die") 
								window.top.location = '/';
							else {
								$('div#ReportTableLayer').html(data);
								Thick_Box_Init();
								UnBlock_Element("ReportTableLayer");
							}
						});
	}else // EXCEL or CSV
	{
		document.getElementById('form1').submit();
	}
}
}

// thick box function 
{
//on page load call tb_init
function Thick_Box_Init()
{   
	tb_init('a.thickbox');//pass where to apply thickbox
}
}
</script>