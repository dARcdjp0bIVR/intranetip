<?php
//editing by 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!$StaffAttend3->Check_Access_Right('REPORT-ABSENCE'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$StaffAttend3UI = new libstaffattend3_ui();

$Params = array();
$Params["ReportType"] = $_REQUEST["ReportType"];
$Params["StaffID"] = $_REQUEST["SelectPersonal"];
$Params["GroupID"] = $_REQUEST["SelectGroup"];
$Params["TargetStartDate"] = $_REQUEST["TargetStartDate"];
$Params["TargetEndDate"] = $_REQUEST["TargetEndDate"];
$Params["Format"] = $_REQUEST["Format"];
$Params["ColAll"] = isset($_REQUEST["ColAll"])?1:0;
$Params["ColReason"] = isset($_REQUEST["ColReason"])?1:0;
$Params["ColRemark"] = isset($_REQUEST["ColRemark"])?1:0;
$Params["SelectWaived"] = $_REQUEST["SelectWaived"];

$StaffAttend3UI->Get_Report_Absence_Table($Params);

intranet_closedb();
?>