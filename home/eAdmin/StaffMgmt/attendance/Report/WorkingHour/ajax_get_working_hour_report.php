<?php
// editing by 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!$StaffAttend3->Check_Access_Right('REPORT-WORKINGHOUR'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	echo 'die';
	intranet_closedb();
	exit();
}

$StaffAttend3UI = new libstaffattend3_ui();

$Params = array();
$Params["SelectStaff"] = IntegerSafe($_REQUEST["SelectStaff"]);
$Params["CalculationType"] = intranet_htmlspecialchars($_REQUEST["CalculationType"]);
$Params["StartDate"] = intranet_htmlspecialchars($_REQUEST["StartDate"]);
$Params["EndDate"] = intranet_htmlspecialchars($_REQUEST["EndDate"]);
$Params["OverTime"] = intranet_htmlspecialchars($_REQUEST["OverTime"]);
$Params["StatusAll"] = intranet_htmlspecialchars($_REQUEST["StatusAll"]);
$Params["StatusPresent"] = intranet_htmlspecialchars($_REQUEST["StatusPresent"]);
$Params["StatusLate"] = intranet_htmlspecialchars($_REQUEST["StatusLate"]);
$Params["StatusAbsent"] = intranet_htmlspecialchars($_REQUEST["StatusAbsent"]);
$Params["StatusOuting"] = intranet_htmlspecialchars($_REQUEST["StatusOuting"]);
$Params["StatusEarlyLeave"] = intranet_htmlspecialchars($_REQUEST["StatusEarlyLeave"]);
$Params["StatusHoliday"] = intranet_htmlspecialchars($_REQUEST["StatusHoliday"]);
$Params["StatusAbsentSuspected"] = intranet_htmlspecialchars($_REQUEST["StatusAbsentSuspected"]);
$Params["Task"] = intranet_htmlspecialchars(strtoupper($_REQUEST["Task"]));
if($Params["Task"]=="PRINT" || $Params["Task"]=="EXPORT"){
	$Params["SelectStaff"] = IntegerSafe($_REQUEST["HiddenSelectStaff"]);
}
if($sys_custom['StaffAttendance']['WongFutNamCollege']){
	$Params["ReportFormat"] = intranet_htmlspecialchars($_REQUEST['ReportFormat']);
}

if($Params["Task"]=="PRINT") include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");
if($sys_custom['StaffAttendance']['WongFutNamCollege'] && $Params['ReportFormat']==2){
	echo $StaffAttend3UI->Get_Overall_Working_Hour_Report_Table($Params);
}else{
	echo $StaffAttend3UI->Get_Report_Working_Hour_Report_Table($Params);
}
if($Params["Task"]=="PRINT") include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_footer.php");

intranet_closedb();
?>