<?php
// editing by 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend2.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!$StaffAttend3->Check_Access_Right('REPORT-WORKINGHOUR'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$linterface = new interface_html();
$StaffAttend3UI = new libstaffattend3_ui();

$CurrentPageArr['eAdminStaffAttendance'] = 1;
$CurrentPage['ReportWorkingHour'] = 1;

$TAGS_OBJ[] = array($Lang['StaffAttendance']['WorkingHourReport'], "", 1);
$MODULE_OBJ = $StaffAttend3UI->GET_MODULE_OBJ_ARR();
//$TAGS_OBJ_RIGHT[] = array(gen_online_help_btn_and_layer('staff_attendance','cust_rpt'));
$linterface->LAYOUT_START(urldecode($_REQUEST['Msg']));

echo $StaffAttend3UI->Get_Report_Working_Hour_Report_Index();

$linterface->LAYOUT_STOP();
intranet_closedb();
?>
<script type="text/JavaScript" language="JavaScript">
// Show/Hide the options layer
function ToggleOptions(ButtonLayerID, OptionLayerID)
{
	var $ButtonObj = $('#'+ButtonLayerID);
	var $OptionObj = $('#'+OptionLayerID);
	
	if($OptionObj.css('display') == 'none')
	{
		if(!$OptionObj.hasClass('report_show_option'))
		{
			$OptionObj.addClass('report_show_option');
		}
		$OptionObj.css('display','');
		$('#'+ButtonLayerID+' a').html('<img height="20" width="20" border="0" align="absmiddle" src="<?=$PATH_WRT_ROOT?>images/<?=$LAYOUT_SKIN?>/icon_hide_option.gif"/><?=$Lang['StaffAttendance']['HideReportOptions']?>');
		$ButtonObj.css('display','');
	}else
	{
		if(!$OptionObj.hasClass('report_show_option'))
		{
			$OptionObj.addClass('report_show_option');
		}
		$OptionObj.css('display','none');
		$('#'+ButtonLayerID+' a').html('<img height="20" width="20" border="0" align="absmiddle" src="<?=$PATH_WRT_ROOT?>images/<?=$LAYOUT_SKIN?>/icon_show_option.gif"/><?=$Lang['StaffAttendance']['ShowReportOptions']?>');
		$ButtonObj.css('display','');
	}
}

function Uncheck(IsChecked, TargetCB)
{
	var targetCheckbox = document.getElementById(TargetCB);
	if(!IsChecked) targetCheckbox.checked = false;
}

function CheckAll(IsChecked, ElementName)
{
	var targetElements = document.getElementsByName(ElementName);
	if(targetElements != null)
	{
		for(var i=0;i<targetElements.length;i++)
		{
			targetElements[i].checked = IsChecked;
		}
	}
}

function onChangeCalcType(type, className)
{
	if(type == 2 || type == 1){
		$('input.'+className+'[type=checkbox]').attr('checked',false);
		$('.'+className).hide();
	}else{
		$('.'+className).show();
	}
}

function Check_Valid_Date(DateObjId, WarningLayerId)
{
	var dateObj = document.getElementById(DateObjId);
	var $WarningLayer = $('#'+WarningLayerId);
	if(check_date_without_return_msg(dateObj))
	{
		$('#WarningLayer').html('');
		$WarningLayer.css('display','none');
		return true;
	}else
	{
		dateObj.focus();
		$('#WarningLayer').html('<?=$Lang['LessonAttendance']['InvalidDateFormat']?>');
		$WarningLayer.css('display','');
		SetTimerToHideWarning(WarningLayerId, 5000);
		return false;
	}
}

function SetTimerToHideWarning(WarningLayerId, ticks)
{
	var action = "$('#"+WarningLayerId+"').css('display','none');";
	setTimeout(action,ticks);
}

function SelectAll(obj)
{
	for (i=0; i<obj.length; i++)
	{
		obj.options[i].selected = true;
	}
}

// ajax functions
function Get_Staff_Selection()
{
	var PostVar = {
			"Action":"get_staff_list",
			"SelectStaffLevel1[]": Get_Selection_Value("SelectStaffLevel1[]")
		};
	$.post(
		"ajax_task.php",
		PostVar,
		function(data){
			if(data!=""){
				$('#SelectStaff\\[\\]').html(data);
				// trick to force IE update UI
				var tmp_parent = $('#SelectStaff\\[\\]').parent();
				tmp_parent.html(tmp_parent.html());
			}
		}
	);
}

function Generate_Report()
{
	var SelectStaff, CalculationType;
	var StartDate = $('#StartDate').val();
	var EndDate = $('#EndDate').val();
	
	SelectStaff = Get_Selection_Value("SelectStaff[]"); 
	if(SelectStaff.length == 0){
		$('span#WarningLayer').html('<?=$Lang['StaffAttendance']['EntryLogSelectStaffWarning']?>');
		$('tr#WarningRow').css('display','');
		return;
	}else{
		$('span#WarningLayer').html('');
		$('tr#WarningRow').css('display','none');
	}
	CalculationType = $('#CalculationType').val();
	
	var PostVar = {
			"SelectStaff[]": SelectStaff,
			"CalculationType": CalculationType,
			"StartDate": StartDate,
			"EndDate": EndDate,
			"OverTime": ($('#OverTime').attr('checked')?1:0),
			"StatusAll": ($('#StatusAll').attr('checked')?1:0),
			"StatusPresent": ($('#StatusPresent').attr('checked')?1:0),
			"StatusLate": ($('#StatusLate').attr('checked')?1:0),
			"StatusAbsent": ($('#StatusAbsent').attr('checked')?1:0),
			"StatusOuting": ($('#StatusOuting').attr('checked')?1:0),
			"StatusEarlyLeave": ($('#StatusEarlyLeave').attr('checked')?1:0),
			"StatusHoliday": ($('#StatusHoliday').attr('checked')?1:0),
			"StatusAbsentSuspected": ($('#StatusAbsentSuspected').attr('checked')?1:0) 
		}
<?php if($sys_custom['StaffAttendance']['WongFutNamCollege']){ ?>
	PostVar['ReportFormat'] = $('input[name="ReportFormat"]:checked').val();
<?php } ?>
	if(!Check_Valid_Date('StartDate', 'WarningRow') || !Check_Valid_Date('EndDate', 'WarningRow'))
	{
		return;
	}
	if(StartDate > EndDate)
	{
		$('#DateWarningLayer').html('<?=$Lang['StaffAttendance']['InvalidDateRange']?>');
		$('#DateWarningLayer').css('display','');
		SetTimerToHideWarning('DateWarningLayer', 5000);
		return;
	}
	
	Block_Element("ReportTableLayer");
	$.post('ajax_get_working_hour_report.php',PostVar,
			function(data){
				if (data == "die") 
					window.top.location = '/';
				else {
					$('div#ReportTableLayer').html(data);
					ToggleOptions('ToggleButtonLayer', 'ReportOptionLayer');
					UnBlock_Element("ReportTableLayer");
				}
			});
}

</script>