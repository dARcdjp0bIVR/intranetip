<?
// editing by 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend2.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!$StaffAttend3->Check_Access_Right('SETTINGS-OT-Access'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$GeneralSetting = new libgeneralsettings();
$SettingList['IgnoreOTTime'] = min(max(0,$_REQUEST['IgnoreOTTime']),999);
$SettingList['CountAsOT'] = $_REQUEST['CountAsOT'];

$GeneralSetting->Start_Trans();
if ($GeneralSetting->Save_General_Setting('StaffAttendance',$SettingList)) {
	$GeneralSetting->Commit_Trans();
	$Msg = $Lang['StaffAttendance']['SettingApplySuccess'];
}
else {
	$GeneralSetting->RollBack_Trans();
	$Msg = $Lang['StaffAttendance']['SettingApplyFail'];
}

header("Location: index.php?Msg=".urlencode($Msg));
?>