<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

$StaffAttend3 = new libstaffattend3();
if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!$StaffAttend3->Check_Access_Right('DUTYSETUP-EMPLOYMENTPERIOD'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	echo 'die';
	intranet_closedb();
	exit();
}

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();

$PeriodStart = trim(urldecode(stripslashes($_REQUEST['PeriodStart'])));
$PeriodEnd = trim(urldecode(stripslashes($_REQUEST['PeriodEnd'])));
$StaffID = $_REQUEST['StaffID'];
$WorkingPeriodID = $_REQUEST['WorkingPeriodID'];

if ($StaffAttend3->Save_Working_Period($StaffID,$WorkingPeriodID,$PeriodStart,$PeriodEnd)) {
	echo $Lang['StaffAttendance']['SaveWorkingPeriodSuccess'];
}
else {
	echo $Lang['StaffAttendance']['SaveWorkingPeriodFail'];
}

intranet_closedb();
?>