<?php
// editing by 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend2.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if ((!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StaffAttendance"] && 
		!$StaffAttend3->Check_Access_Right('DUTYSETUP-EMPLOYMENTPERIOD'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

//$StaffAttend3UI = new libstaffattend3_ui();
$StaffAttend3 = new libstaffattend3();
$limport = new libimporttext();
$lf = new libfilesystem();

$format_array = array("UserLogin","PeriodStart","PeriodEnd");
$filepath = $userfile;
$filename = $userfile_name;

if($filepath=="none" || $filepath == "")
{
	# import failed
    header("Location: import.php?Msg=".urlencode($Lang['StaffAttendance']['DataImportFail']));
    intranet_closedb();
    exit();
}else
{
	$ext = strtoupper($lf->file_ext($filename));
    if($limport->CHECK_FILE_EXT($filename))
    {
        # read file into array
        # return 0 if fail, return csv array if success
        //$data = $lf->file_read_csv($filepath);
        $data = $limport->GET_IMPORT_TXT($filepath);
        $toprow = array_shift($data);                   # drop the title bar
    }
    for ($i=0; $i<sizeof($format_array); $i++)
    {
	     if ($toprow[$i] != $format_array[$i])
	     {
	         header("Location: import.php?Msg=".urlencode($Lang['StaffAttendance']['DataImportFail']));
	         intranet_closedb();
	         exit();
	     }
    }
    $result_array = $StaffAttend3->Import_Staff_Working_Periods($data);
    if(sizeof($result_array)>0)
    {
    	$msg = implode(", ",$result_array);
    	header("Location: import.php?Msg=".urlencode($Lang['StaffAttendance']['DataImportFail']));
    	intranet_closedb();
    	exit();
    }
}

intranet_closedb();
header("Location: index.php?Msg=".urlencode($Lang['StaffAttendance']['DataImportSuccess']));
?>