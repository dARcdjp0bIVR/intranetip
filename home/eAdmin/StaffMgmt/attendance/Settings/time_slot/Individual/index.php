<?php
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend2.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!$StaffAttend3->Check_Access_Right('DUTYSETUP-BASIC'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$ScrollTo = $_REQUEST['ScrollTo'];

$linterface = new interface_html();
$StaffAttend3UI = new libstaffattend3_ui();

$CurrentPageArr['eAdminStaffAttendance'] = 1;
$CurrentPage['DutyBasic'] = 1;
$TAGS_OBJ[] = array($Lang['StaffAttendance']['SetupStatus'], $PATH_WRT_ROOT."home/eAdmin/StaffMgmt/attendance/Settings/time_slot/status/", 1);
$TAGS_OBJ[] = array($Lang['StaffAttendance']['StaffInfo'], $PATH_WRT_ROOT."home/eAdmin/StaffMgmt/attendance/Settings/time_slot/staff_info/", 0);
$MODULE_OBJ = $StaffAttend3UI->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START(urldecode($_REQUEST['Msg']));

echo $StaffAttend3UI->Get_User_Setting_Index();

$linterface->LAYOUT_STOP();
intranet_closedb();
?>
<script>
var SlotJustCheck = 1;
var SaveSlotGoNextStep = false;
// dom function 
{
function Check_Go_Search(evt) {
	var key = evt.which || evt.charCode || evt.keyCode;
	
	if (key == 13) // enter
		Get_User_List();
	else
		return false;
}

function Check_Slot_Period() {
	var StartHour = Get_Selection_Value('StartHour','String');
	var StartMin = Get_Selection_Value('StartMin','String');
	var StartSec = Get_Selection_Value('StartSec','String');
	var EndHour = Get_Selection_Value('EndHour','String');
	var EndMin = Get_Selection_Value('EndMin','String');
	var EndSec = Get_Selection_Value('EndSec','String');
	var SlotStart = StartHour+":"+StartMin+":"+StartSec;
	var SlotEnd = EndHour+":"+EndMin+":"+EndSec;
	var ElementRow = document.getElementById('SlotTimeWarningRow');
	var ElementLayer = $('div#SlotTimeWarningLayer');
	
	if (SlotStart == SlotEnd) { // time slot must not exceed 24 hours
		ElementLayer.html('<?=$Lang['StaffAttendance']['SlotTimeIntervalLimitWarning']?>');
		ElementRow.style.display = '';
		return false;
	}
	
	ElementLayer.html('');
	ElementRow.style.display = 'none';
}
}

// ajax function 
{
function Get_User_List() {
	var PostVar = {
			Keyword: encodeURIComponent($('Input#Keyword').val())
			}

	Block_Element("UserListLayer");
	$.post('ajax_get_user_list.php',PostVar,
					function(data){
						if (data == "die") 
							window.top.location = '/';
						else {
							$('div#UserListLayer').html(data);
							$('div#SearchInputLayer').show();
							Thick_Box_Init();
							UnBlock_Element("UserListLayer");
						}
					});
}
	
function Delete_Slot(SlotID) {
	if (confirm('<?=$Lang['StaffAttendance']['DeleteSlotConfirmMessage']?>')) {
		var PostVar = {
			"SlotID":SlotID
			};
		
		Block_Element("SlotTableLayer");
		$.post('../ajax_delete_slot.php',PostVar,
			function(data) {
				if (data == "die")
					window.top.location = '/';
				else {
					Get_Return_Message(data);
					Get_User_List();
				}
			});
	}
}
	
function Save_Slot(GoNextStep) {
	SlotJustCheck = 0; // global variable setting
	SaveSlotGoNextStep = GoNextStep || false; // global variable setting
	Check_Slot_Period();
	Check_Slot_Name();
	Check_Day_Count();
	
	$(document).ajaxComplete(function(e, xhr, settings) {
		if (settings.url.indexOf('ajax_check_slot_name.php') != -1 && SlotJustCheck == 0) {
			var NameWarning = $('div#SlotNameWarningLayer').html();
			var TimeWarning = $('div#SlotTimeWarningLayer').html();
			var DayCountWarning = $('div#DutyCountWarningLayer').html();
		
			if (Trim(NameWarning) == "" && Trim(TimeWarning) == "" && Trim(DayCountWarning) == "") {
				var StartHour = Get_Selection_Value('StartHour','String');
				var StartMin = Get_Selection_Value('StartMin','String');
				var StartSec = Get_Selection_Value('StartSec','String');
				var EndHour = Get_Selection_Value('EndHour','String');
				var EndMin = Get_Selection_Value('EndMin','String');
				var EndSec = Get_Selection_Value('EndSec','String');
				var SlotStart = StartHour+":"+StartMin+":"+StartSec;
				var SlotEnd = EndHour+":"+EndMin+":"+EndSec;
				var TargetUserID = $('input#TargetUserID').val();
				var PostVar = {
					"SlotName": encodeURIComponent($('input#SlotName').val()),
					"SlotID": $('input#SlotID').val(),
					"TargetUserID": TargetUserID,
					"SlotStart": encodeURIComponent(SlotStart),
					"SlotEnd": encodeURIComponent(SlotEnd),
					"InWavie": document.getElementById('InWavie').checked,
					"OutWavie": document.getElementById('OutWavie').checked,
					"DutyCount": encodeURIComponent($('input#DutyCount').val())
				};
				
				Block_Thickbox();
				$.post('../ajax_save_slot.php',PostVar,
					function (data) {
						if (data == "die") 
							window.top.location = '/';
						else {
							Get_Return_Message(data);
							Get_User_List();
							
							if (SaveSlotGoNextStep)
								window.location = '<?=$PATH_WRT_ROOT?>home/eAdmin/StaffMgmt/attendance/Settings/time_slot/status/duty_period_index.php?ID='+TargetUserID+'&GroupOrIndividual=Individual&Msg='+encodeURIComponent(data);
							else
								window.top.tb_remove();
							Scroll_To_Top();
						}
					});
			}
			
			SlotJustCheck = 1;
		}
	});
}
	
function Check_Slot_Name() {
	SlotName = $('input#SlotName').val();
	ElementObj = $('div#SlotNameWarningLayer');
		
	if (DOM_Check_Slot_Name(SlotName,'SlotNameWarningLayer','SlotNameWarningRow')) {
		var PostVar = {
			"SlotID":$('input#SlotID').val(),
			"SlotName":encodeURIComponent(SlotName), 
			"TargetUserID":$('input#TargetUserID').val()
		}
		
		$.post('../ajax_check_slot_name.php',PostVar,
			function (data) {
				if (data == "die") 
					window.top.location = '/';
				else if (data == "1") {
					ElementObj.html('');
					document.getElementById('SlotNameWarningRow').style.display = 'none';
				}
				else {
					ElementObj.html('<?=$Lang['StaffAttendance']['SlotNameWarning']?>');
					document.getElementById('SlotNameWarningRow').style.display = '';
				}
			});
	}
}
	
function Get_Slot_Form(SlotID,StaffID) {
	var SlotID = SlotID || "";
	var PostVar = {
		"SlotID": SlotID,
		"TargetUserID":StaffID
	};
	
	$.post('../ajax_get_slot_form.php',PostVar,
		function(data) {
			if (data == "die") 
				window.top.location = '/';
			else 
				$('div#TB_ajaxContent').html(data);
		});
}
}

// thick box function 
{
//on page load call tb_init
function Thick_Box_Init(){   
	tb_init('a.thickbox');//pass where to apply thickbox
}
}


<?
if ($ScrollTo != '') {
?>
$(document).ready(function() {
	Scroll_To_Element('<?=$ScrollTo?>');
});
<?
}
?>
</script>