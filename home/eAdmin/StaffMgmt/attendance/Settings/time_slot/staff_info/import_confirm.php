<?php
// editing by 
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend2.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!$StaffAttend3->Check_Access_Right('DUTYSETUP-BASIC'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$linterface = new interface_html();
$StaffAttend3UI = new libstaffattend3_ui();



$limport = new libimporttext();
$lf = new libfilesystem();

$CardType = $_REQUEST['CardType'];
$format_array = array("UserLogin","CardID");
$filepath = $_FILES["userfile"]["tmp_name"];
$filename = $_FILES["userfile"]["name"];

if($filepath=="none" || $filepath == "")
{
	# import failed
    header("Location: import.php?Msg=".urlencode($Lang['StaffAttendance']['DataImportFail']));
    intranet_closedb();
    exit();
}else
{
	$ext = strtoupper($lf->file_ext($filename));
	if($limport->CHECK_FILE_EXT($filename))
	{
	  # read file into array
	  # return 0 if fail, return csv array if success
	  //$data = $lf->file_read_csv($filepath);
	  $data = $limport->GET_IMPORT_TXT($filepath);
	  if(sizeof($data)>0)
	  {
	  	$toprow = array_shift($data);                   # drop the title bar
	  }else
	  {
	  	header("Location: import.php?Msg=".urlencode($Lang['StaffAttendance']['DataImportFail']));
	    intranet_closedb();
	    exit();
	  }
	}
	for ($i=0; $i<sizeof($format_array); $i++)
	{
	 if ($toprow[$i] != $format_array[$i])
	 {
	     header("Location: import.php?Msg=".urlencode($Lang['StaffAttendance']['DataImportFail']));
	     intranet_closedb();
	     exit();
	 }
	}
	
	$CurrentPageArr['eAdminStaffAttendance'] = 1;
	$CurrentPage['DutyBasic'] = 1;
	$TAGS_OBJ[] = array($Lang['StaffAttendance']['SetupStatus'], $PATH_WRT_ROOT."home/eAdmin/StaffMgmt/attendance/Settings/time_slot/status/", 0);
	$TAGS_OBJ[] = array($Lang['StaffAttendance']['StaffInfo'], $PATH_WRT_ROOT."home/eAdmin/StaffMgmt/attendance/Settings/time_slot/staff_info/", 1);
	$MODULE_OBJ = $StaffAttend3UI->GET_MODULE_OBJ_ARR();
	$linterface->LAYOUT_START(urldecode($_REQUEST['Msg']));
  	echo $StaffAttend3UI->Get_User_Info_Import_Confirm_Page($data,$CardType);
}

$linterface->LAYOUT_STOP();
intranet_closedb();
?>