<?php
// editing by 
/*********************************** Changes *********************************************
 * 2012-02-03 (Carlos): Check duplicated CardID before save
 *****************************************************************************************/
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!$StaffAttend3->Check_Access_Right('DUTYSETUP-BASIC'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	echo 'die';
	intranet_closedb();
	exit();
}

$CardID = trim($_REQUEST['CardID']);
$TargetUserID = $_REQUEST['TargetUserID'];
$IsRFID = $_REQUEST['IsRFID'];

$noDuplicated = $StaffAttend3->Check_Card_ID($CardID,$TargetUserID,$IsRFID);

if ($noDuplicated && $StaffAttend3->Save_Card_ID($CardID,$TargetUserID,$IsRFID)) {
	echo $Lang['StaffAttendance']['SmartcardIDUpdateSuccess'];
}
else {
	echo $Lang['StaffAttendance']['SmartcardIDUpdateFail'];
}

intranet_closedb();
?>