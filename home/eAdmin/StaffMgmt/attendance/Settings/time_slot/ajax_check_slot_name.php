<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!$StaffAttend3->Check_Access_Right('DUTYSETUP'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	echo 'die';
	intranet_closedb();
	exit();
}

$StaffAttend3 = new libstaffattend3();

$SlotName = trim(urldecode(stripslashes($_REQUEST['SlotName'])));
$GroupID = $_REQUEST['GroupID'];
$TargetUserID = $_REQUEST['TargetUserID'];
$TemplateID = $_REQUEST['TemplateID'];
$SlotID = $_REQUEST['SlotID'];

if ($StaffAttend3->Check_Slot_Name($SlotName,$GroupID,$TargetUserID,$SlotID,$TemplateID)) {
	echo "1";
}
else {
	echo "0";
}

intranet_closedb();
?>