<?php
// Editing by 
/*
 * 2015-07-20 (Carlos): Modified js Save_Normal_Group_Duty_Period(), added DayType data. 
 * 2015-03-17 (Carlos): Modified js Drag_And_Drop_Init(), workaround IE11 fail to set min height problem with js.
 * 2015-03-17 (Carlos): If webkit browser, use onselectstart return false to workaround the drag & drop highlight text problem. Drawback is all text cannot be highlighted.
 * 2014-04-30 (Carlos): Resize div#UnAssignLayer to parent's height at Drag_And_Drop_Init() for easier accept dropping
 */
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend2.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!$StaffAttend3->Check_Access_Right('DUTYSETUP-BASIC'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$linterface = new interface_html();
$StaffAttend3UI = new libstaffattend3_ui();

$CurrentPageArr['eAdminStaffAttendance'] = 1;
$CurrentPage['DutyBasic'] = 1;
$TAGS_OBJ[] = array($Lang['StaffAttendance']['SetupStatus'], $PATH_WRT_ROOT."home/eAdmin/StaffMgmt/attendance/Settings/time_slot/status/", 1);
$TAGS_OBJ[] = array($Lang['StaffAttendance']['StaffInfo'], $PATH_WRT_ROOT."home/eAdmin/StaffMgmt/attendance/Settings/time_slot/staff_info/", 0);
$MODULE_OBJ = $StaffAttend3UI->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START(urldecode($_REQUEST['Msg']));

$ID = $_REQUEST['ID'];
$GroupOrIndividual = $_REQUEST['GroupOrIndividual'];

echo $StaffAttend3UI->Get_Basic_Setting_Duty_Period_Index($ID,$GroupOrIndividual);

$linterface->LAYOUT_STOP();
intranet_closedb();
?>
<script>
// ajax function
{	
function Delete_Duty_Period(ID,GroupSlotPeriodID,GroupOrIndividual) {
	if (confirm('<?=$Lang['StaffAttendance']['DutyPeriodDeleteConfim']?>')) {
		Block_Thickbox();
		
		var PostVar = {
			"GroupSlotPeriodID": GroupSlotPeriodID
		};
		
		$.post('<?=$PATH_WRT_ROOT?>home/eAdmin/StaffMgmt/attendance/Management/roster/normal/ajax_delete_duty_period.php',PostVar,
			function (data) {
				if (data == "die") 
					window.top.location = '/';
				else {
					Get_Normal_Group_Detail(ID,GroupOrIndividual);
					window.top.tb_remove();
					Get_Return_Message(data);
				}
			});
	}
}

function Save_Slot_Setting() {
	var SelectedSlot = Get_Check_Box_Value('TimeSlotID[]','Array');
	var ID = $('input#ID').val();
	var GroupSlotPeriodID = $('input#GroupSlotPeriodID').val();
	var DayOfWeek = Get_Check_Box_Value('DayOfWeek[]','Array');
	
	if (GroupSlotPeriodID.length > 0) {
		document.getElementById('DayOfWeekWarningRow').style.display = "none";
		$('span#DayOfWeekWarningLayer').html('');
		
		var SlotUserList = new Array();
		var EvalStr = 'var PostVar = {';
		for (var i=0; i< SelectedSlot.length; i++) {
			SlotUserList[i] = Get_Check_Box_Value(SelectedSlot[i]+'-UserID[]','Array',true);
			EvalStr += '"'+SelectedSlot[i]+'[]":SlotUserList['+i+'],';
		}
		EvalStr += '"TimeSlotID[]":SelectedSlot,';
		EvalStr += '"DayOfWeek[]":DayOfWeek,';
		EvalStr += '"ID":ID,';
		EvalStr += '"GroupSlotPeriodID":GroupSlotPeriodID};';
		eval(EvalStr);

		Block_Element('DutyPeriodLayer');
		$.post('<?=$PATH_WRT_ROOT?>home/eAdmin/StaffMgmt/attendance/Management/roster/normal/ajax_save_duty_period_user_time_slot.php',PostVar,
			function(data) {
				if (data == "die") 
					window.top.location = "/";
				else {
					Scroll_To_Top();
					Get_Return_Message(data);
					Get_Duty_Period_Detail(ID,GroupSlotPeriodID,$('input#GroupOrIndividual').val());
				}
			});
	}
	else {
		document.getElementById('DayOfWeekWarningRow').style.display = "";
		$('span#DayOfWeekWarningLayer').html('<?=$Lang['StaffAttendance']['DayOfWeekWarning']?>');
	}
}
	
function Check_Slot_Setting() {
	var SelectedSlot = Get_Check_Box_Value('TimeSlotID[]','Array');
	
	if (SelectedSlot.length > 0) {
		var SlotUserList = new Array();
		var EvalStr = 'var PostVar = {';
		for (var i=0; i< SelectedSlot.length; i++) {
			SlotUserList[i] = Get_Check_Box_Value(SelectedSlot[i]+'-UserID[]','Array',true);
			EvalStr += '"TimeSlotUser['+SelectedSlot[i]+'][]":SlotUserList['+i+'],';
		}
		EvalStr += '"TimeSlotID[]":SelectedSlot};';
		eval(EvalStr);
		
		$.post('<?=$PATH_WRT_ROOT?>home/eAdmin/StaffMgmt/attendance/Management/roster/group/ajax_check_duty_period_user_time_slot.php',PostVar,
			function(data) {
				if (data == "die") 
					window.top.location = "/";
				else {
					var Result = data.split('|=|');
					
					if (Result[0] == '1') {
						Save_Slot_Setting();
					}
					else {
						var DutyOver24Hour = Result[1].split(',');
						var TimeSlotOverlap = Result[2].split(',');
						var AlertContent = '';
						
						AlertContent += '<?=$Lang['StaffAttendance']['DutyOver24HourWarning']?>\n';
						if (Result[1] != "") {
							for (var i=0; i< DutyOver24Hour.length; i++) {
								AlertContent += document.getElementById('UserName['+DutyOver24Hour[i]+']').value+', ';
							}
						}
						else {
							AlertContent += 'N/A';
						}
						
						AlertContent += '\n\n';
						AlertContent += '<?=$Lang['StaffAttendance']['DutyOverlapWarning']?>\n';
						if (Result[2] != "") {
							for (var i=0; i< TimeSlotOverlap.length; i++) {
								AlertContent += document.getElementById('UserName['+TimeSlotOverlap[i]+']').value+', ';
							}
						}
						else {
							AlertContent += 'N/A';
						}
						
						AlertContent += '\n\n';
						AlertContent += '<?=$Lang['StaffAttendance']['DutyWarning']?>\n';
						
						alert(AlertContent);
					}
				}
			});
	}
	else {
		Save_Slot_Setting();
		/*alert('<?=$Lang['StaffAttendance']['DutyApplyDuty']?>');*/
	}
}
	
function Get_Full_Member_List(Keyword) {
	PostVar = {
		"Keyword":encodeURIComponent(Keyword),
		"GroupID":$('input#ID').val()
	};
	
	Block_Element('FullStaffListLayer');
	$('div#FullStaffListLayer').load('<?=$PATH_WRT_ROOT?>home/eAdmin/StaffMgmt/attendance/Management/roster/group/ajax_get_full_member_list.php',PostVar,
		function(data) {
			if (data == "die") 
				window.top.location = "/";
		});
	UnBlock_Element('FullStaffListLayer');
}
	
function Get_Roster_Form(ID,GroupSlotPeriodID,DayOfWeek,GroupOrIndividual,BasicOrSpecial) {
	var PostVar = {
		"ID":ID,
		"DayOfWeek":DayOfWeek,
		"GroupSlotPeriodID": GroupSlotPeriodID,
		"GroupOrIndividual": GroupOrIndividual,
		"BasicOrSpecial": BasicOrSpecial
	};
	
	Block_Element('DutyPeriodLayer');
	$('div#DutyPeriodLayer').load('<?=$PATH_WRT_ROOT?>home/eAdmin/StaffMgmt/attendance/Management/roster/normal/ajax_get_normal_roster_form.php',PostVar,
		function(data) {
			if (data == "die") 
				window.top.location = '/';
			else {
				Drag_And_Drop_Init();
				UnBlock_Element('DutyPeriodLayer');
			}
		});
}
	
function Get_Duty_Period_Detail(GroupID,GroupSlotPeriodID,GroupOrIndividual) {
	var PostVar = {
		"ID":GroupID,
		"GroupSlotPeriodID": GroupSlotPeriodID,
		"GroupOrIndividual": GroupOrIndividual,
		"BasicOrSpecial": "Basic"
	};
	
	Block_Element('DutyPeriodLayer');
	$('div#DutyPeriodLayer').load('<?=$PATH_WRT_ROOT?>home/eAdmin/StaffMgmt/attendance/Management/roster/normal/ajax_get_normal_group_duty_period_detail.php',PostVar,
		function (data) {
			if (data == "die") 
				window.top.location = '/';
			else {
				Thick_Box_Init();
				UnBlock_Element('DutyPeriodLayer');
			}
			//document.getElementById('DutyPeriodLayer').innerHTML = data;
		});
}
	
function Check_Duty_Period(GoNextStep) {
	var GoNextStep = GoNextStep || false;
	var EffectiveArray = new Array();
	
	var EffectiveStart = Trim($('Input#EffectiveStart').val());
	EffectiveArray = EffectiveStart.split('-');
	var DateStart = new Date();
	DateStart.setFullYear(EffectiveArray[0],(EffectiveArray[1]-1),EffectiveArray[2]);
	
	var EffectiveEnd = Trim($('Input#EffectiveEnd').val());
	EffectiveArray = EffectiveEnd.split('-');
	var DateEnd = new Date();
	DateEnd.setFullYear(EffectiveArray[0],(EffectiveArray[1]-1),EffectiveArray[2]);
	
	var WarningRow = document.getElementById('EffectiveDateWarningRow');
	var WarningLayer = $('div#EffectiveDateWarningLayer');
	
	if ($('#DPWL-EffectiveStart').html() == "" && $('#DPWL-EffectiveEnd').html() == "") {
		if (EffectiveStart == "" || EffectiveEnd == "") {
			WarningLayer.html('<?=$Lang['StaffAttendance']['EffectiveDateNullWarning']?>');
			WarningRow.style.display = "";
		}
		else if (DateStart > DateEnd) {
			WarningLayer.html('<?=$Lang['StaffAttendance']['EffectiveEndDateLesserThenStartDateWarning']?>');
			WarningRow.style.display = "";
		}
		else {
			var PostVar = {
					"ID": $('Input#ID').val(),
					"GroupSlotPeriodID": $('Input#GroupSlotPeriodID').val(),
					"GroupOrIndividual": $('Input#GroupOrIndividual').val(),
					"EffectiveStart": encodeURIComponent(EffectiveStart),
					"EffectiveEnd": encodeURIComponent(EffectiveEnd)
					};
					
			$.post('<?=$PATH_WRT_ROOT?>home/eAdmin/StaffMgmt/attendance/Management/roster/normal/ajax_check_normal_group_duty_period.php',PostVar,
				function(data){
					if (data == "die") 
						window.top.location = '/';
					else {
						if (data == "1") {
							WarningLayer.html('');
							WarningRow.style.display = "none";
							
							Save_Normal_Group_Duty_Period(GoNextStep);
						}
						else {
							WarningLayer.html('<?=$Lang['StaffAttendance']['EffectiveDateOverlapWarning']?>');
							WarningRow.style.display = "";
						}
					}
				});
		}
	}
}	

function Save_Normal_Group_Duty_Period(GoNextStep) {
	var GoNextStep = GoNextStep || false;
	Block_Thickbox();
	
	var ID = $('input#ID').val();
	var GroupSlotPeriodID = $('input#GroupSlotPeriodID').val();
	var GroupOrIndividual = $('input#GroupOrIndividual').val();
	var PostVar = {
		"ID": ID,
		"GroupSlotPeriodID": GroupSlotPeriodID,
		"GroupOrIndividual": GroupOrIndividual,
		"EffectiveStart": encodeURIComponent($('input#EffectiveStart').val()),
		"EffectiveEnd": encodeURIComponent($('input#EffectiveEnd').val()),
		"SkipSchoolHoliday": ((document.getElementById('SkipSchoolHoliday').checked)? '1':'0'),
		"SkipPublicHoliday": ((document.getElementById('SkipPublicHoliday').checked)? '1':'0'),
		"SkipSchoolEvent": ((document.getElementById('SkipSchoolEvent').checked)? '1':'0'),
		"SkipAcademicEvent": ((document.getElementById('SkipAcademicEvent').checked)? '1':'0'),
		"DayType": $('input[name="DayType"]:checked').val() 
	};
	
	$.post('<?=$PATH_WRT_ROOT?>home/eAdmin/StaffMgmt/attendance/Management/roster/normal/ajax_save_normal_group_duty_period.php',PostVar,
		function (data) {
			if (data == "die") 
				window.top.location = '/';
			else {
				Temp = data.split('|=|');
				
				if (GoNextStep && Temp[0] == 1) {
					if (GroupSlotPeriodID == "") {
						GroupSlotPeriodID = Temp[2];
					}
					
					Get_Duty_Period_Detail(ID,GroupSlotPeriodID,GroupOrIndividual);
				}
				else
					Get_Normal_Group_Detail(ID,GroupOrIndividual);
				
				window.top.tb_remove();
				Get_Return_Message(data);
			}
		});
}

function Get_Duty_Period_Form(ID,GroupSlotPeriodID,GroupOrIndividual) {
	GroupSlotPeriodID = GroupSlotPeriodID || "";
	
	var PostVar = {
		"ID": ID,
		"GroupSlotPeriodID": GroupSlotPeriodID,
		"GroupOrIndividual": GroupOrIndividual
	};
	
	$.post('<?=$PATH_WRT_ROOT?>home/eAdmin/StaffMgmt/attendance/Management/roster/normal/ajax_get_normal_group_duty_period_form.php',PostVar,
		function(data) {
			if (data == "die") 
				window.top.location="/";
			else {
				$('div#TB_ajaxContent').html(data);
			}
		}
		);
	
}
	
function Get_Normal_Group_Detail(ID,GroupOrIndividual) {
	var PostVar = {
		"ID": ID,
		"GroupOrIndividual": GroupOrIndividual,
		"BasicOrSpecial": "Basic"
	};
		
	Block_Element('DutyPeriodLayer');
	$('div#DutyPeriodLayer').load('<?=$PATH_WRT_ROOT?>home/eAdmin/StaffMgmt/attendance/Management/roster/normal/ajax_get_normal_group_detail.php',PostVar,
		function(data) {
			if (data == "die") 
				window.top.location="/";
			else {
				Thick_Box_Init();
				UnBlock_Element('DutyPeriodLayer');
			}
		});
}
	
function Get_Group_List(TargetMonth,TargetYear) {
	var TargetMonth = TargetMonth || '';
	var TargetYear = TargetYear || '';
	var PostVar = {
		"TargetMonth":TargetMonth,
		"TargetYear":TargetYear};
	Block_Element('DutyPeriodLayer');
	$('div#DutyPeriodLayer').load('<?=$PATH_WRT_ROOT?>home/eAdmin/StaffMgmt/attendance/Management/roster/normal/ajax_get_group_index.php',PostVar,
		function(data) {
			if (data == "die") 
				window.top.location="/";
			else {
				UnBlock_Element('DutyPeriodLayer');
			}
		});
}

function Get_Group_Detail(GroupID) {
	var PostVar = {"GroupID": GroupID};
	
	Block_Element('DutyPeriodLayer');
	$('div#DutyPeriodLayer').load('<?=$PATH_WRT_ROOT?>home/eAdmin/StaffMgmt/attendance/Management/roster/normal/ajax_get_normal_group_detail.php',PostVar,
		function(data) {
			if (data == "die") 
				window.top.location="/";
			else {
				Thick_Box_Init();
				UnBlock_Element('DutyPeriodLayer');
			}
		});
}
}

// dom function 
{
function Select_DeSelect_All(ColumnType) {	
	if (ColumnType == "UnAssign") {
		var CheckBoxObj = document.getElementsByName('UnAssign-UserID[]');
	}
	else if (ColumnType == "Full") {
		var CheckBoxObj = document.getElementsByName('Search-UserID[]');
	}
	else {
		var CheckBoxObj = document.getElementsByName(ColumnType+'-UserID[]');
	}
	
	var AnyUnChecked=false;
	for (var i=0; i< CheckBoxObj.length; i++) {
		if (!CheckBoxObj[i].checked) {
			AnyUnChecked = true;
			CheckBoxObj[i].checked = true;
			CheckBoxObj[i].parentNode.style.backgroundColor = '#A6A6A6';
		}
	}
	
	if (!AnyUnChecked) {
		for (var i=0; i< CheckBoxObj.length; i++) {
			CheckBoxObj[i].checked = false;
			CheckBoxObj[i].parentNode.style.backgroundColor = '';
		}
		
	}
}
	
function Apply_Time_Slot() {
	var TimeSlot = Get_Selection_Value('TimeSlot[]','Array');
	var TimeSlotTable = document.getElementById('TimeSlotTable');
	var TimeSlotTableCells = TimeSlotTable.tBodies[0].rows[0].cells;
	
	// get current applied slots
	var TotalSlotSelected = 0;
	for (var j=0; j< TimeSlotTableCells.length; j++) {
		if (document.getElementById('TimeSlotID['+TimeSlotTableCells[j].id+']').checked) 
			TotalSlotSelected++;
	}
	
	// minus already applyed slots
	for (var i=0; i< TimeSlot.length; i++) {
		if (document.getElementById('TimeSlotID['+TimeSlot[i]+']')) {
			TotalSlotSelected--;
		}
	}
	
	// check if the total slots going to apply is greater then system limit, block the action and prompt user
	if ((TimeSlot.length + TotalSlotSelected) > <?=$StaffAttend3->MaxSlotPerSetting?>) {
		alert('<?=$Lang['StaffAttendance']['MaxSlotPerSettingWarning1'].$StaffAttend3->MaxSlotPerSetting.$Lang['StaffAttendance']['MaxSlotPerSettingWarning2']?>');
	}
	else {
		if (TimeSlot.length > 0)
			$('#DragAndDropSessionLayer').css('display','block');
		
		var CellHeight = document.getElementById('FullStaffListLayer').offsetHeight;
		
		for (var i=0; i< TimeSlot.length; i++) {
			if (document.getElementById('TimeSlotID['+TimeSlot[i]+']')) {
				$('.TimeSlotColumn-'+TimeSlot[i]).css('display',''); 
				document.getElementById('TimeSlotID['+TimeSlot[i]+']').checked = true;
			}
			else {
				if (TimeSlotTableCells.length == 0) {
					CellIndex = 0;
				}
				else {
					var NewSlotStart = document.getElementById('SlotStart['+TimeSlot[i]+']').value;
					for (var j=0; j< TimeSlotTableCells.length; j++) {
						var CurrSlotStart = document.getElementById('SlotStart['+TimeSlotTableCells[j].id+']').value;
						
						if (NewSlotStart <= CurrSlotStart) {
							CellIndex = j;
							
							break;
						}
						else 
							CellIndex = -1;
					}
				}
				
				var cell1 = TimeSlotTable.tBodies[0].rows[0].insertCell(CellIndex);
				Content = '<input name="TimeSlotID[]" id="TimeSlotID['+TimeSlot[i]+']" type="checkbox" value="'+TimeSlot[i]+'" checked="checked" style="display:none;"/>';
				Content += '<span style="float:left;">';
				Content += document.getElementById('SlotName['+TimeSlot[i]+']').value;
				Content += '</span>';
				Content += '<span style="float:right;" class="table_row_tool row_content_tool">';
				Content += '<a onclick="$(\'.TimeSlotColumn-'+TimeSlot[i]+'\').hide(); document.getElementById(\'TimeSlotID['+TimeSlot[i]+']\').checked = false;" href="#" class="delete" title="<?=$Lang['Btn']['Remove']?>"></a>';
				Content += '</span>';
				cell1.className = 'sub_row_top TimeSlotColumn-'+TimeSlot[i];
				cell1.style.backgroundColor = '#7E7E7E';
				cell1.id = TimeSlot[i];
				cell1.innerHTML = Content;
				
				var cell2 = TimeSlotTable.tBodies[0].rows[1].insertCell(CellIndex);
				Content =	'<div class="roster_slot_tool"> ';
				Content +=	'<span style="float:left;">';
				Content +=	'<strong style="color:#000000;"><?=$Lang['StaffAttendance']['OnDuty']?> <font id="'+TimeSlot[i]+'CountLayer">(0)</font></strong>';
				Content +=	'</span>';
				Content +=	'<span style="float:right;" class="row_content_tool">';
				Content +=  '<a href="#" style="color:#000000;" onmouseover="this.style.color = \'#FF0000\';" onmouseout="this.style.color = \'#000000\';" onclick="Select_DeSelect_All(\''+TimeSlot[i]+'\'); return false;"><?=$Lang['StaffAttendance']['SelectDeSelectAll']?></a>';
				Content +=	'</span>';
				Content +=	'</div>';
				cell2.className = 'TimeSlotColumn-'+TimeSlot[i];
				cell2.innerHTML = Content;
				
				
				var cell3 = TimeSlotTable.tBodies[0].rows[2].insertCell(CellIndex);
				cell3.className = 'TimeSlotCell TimeSlotColumn-'+TimeSlot[i];
				Content = '<div id="TimeSlot-'+TimeSlot[i]+'" class="TimeSlotDragBlock TimeSlotDropBlock roster_staff_item roster_staff_item_dayoff" style="min-height:'+CellHeight+'px; height: expression(this.scrollHeight < '+(CellHeight+1)+' ? \''+CellHeight+'px\':\'auto\');"></div>';
				cell3.innerHTML = Content;
			}
		}
	}
	
	Drag_And_Drop_Init();
}

var OrgColWidth = 0;
function Toggle_Full_Member_Layer() {
	var FullMemberLayer = document.getElementById('FullMemberPanelTable');
	if (FullMemberLayer.style.display == "none") {
		FullMemberLayer.style.display = '';
		//document.getElementById('TimeSlotCell').style.width = OrgColWidth + 'px';
		document.getElementById('TimeSlotLayer').style.width = OrgColWidth + 'px';
	}
	else {
		OrgColWidth = document.getElementById('TimeSlotCell').offsetWidth;
		
		FullMemberLayer.style.display = 'none';
		var ColWidth = document.getElementById('TimeSlotCell').offsetWidth;
		document.getElementById('TimeSlotLayer').style.width = ColWidth + 'px';
	}
}

function Change_Background_Color(CheckBoxID,DivObj) {
	var Temp = CheckBoxID.split('-');
	var CheckboxObj = document.getElementById(CheckBoxID);

	if (CheckboxObj.checked) {
		//alert('-');
		CheckboxObj.checked = false;
		DivObj.style.backgroundColor = '';
	}
	else {
		//alert('+');
		CheckboxObj.checked = true;
		DivObj.style.backgroundColor = '#A6A6A6';
	}		
}

function Calculate_Selected_Count(CheckedStatus,TimeSlotID) {
}

function Calculate_Unassign_List() {
	if (document.getElementById('UnAssignLayer')) {
		var FullMemberList = Get_Check_Box_Value('Full-UserID[]','Array',true);
		var ActiveTimeSlotList = Get_Check_Box_Value('TimeSlotID[]','Array');
		var SelectedUserList = new Array();
		
		for (var i=0; i< ActiveTimeSlotList.length; i++) {
			var UserList = Get_Check_Box_Value(ActiveTimeSlotList[i]+'-UserID[]','Array',true);
			
			for (var j=0; j< UserList.length; j++) {
				if (jIN_ARRAY(FullMemberList,UserList[j]) && !jIN_ARRAY(SelectedUserList,UserList[j])) {
					SelectedUserList[UserList[j]] = UserList[j];
				}
			}
		}
		
		Content = '';
		Count = 0;
		for (var i=0; i< FullMemberList.length; i++) {
			if (!jIN_ARRAY(SelectedUserList,FullMemberList[i])) {
				var UserID = FullMemberList[i];
				var StaffName = document.getElementById('UserName['+UserID+']').value;
				//var Div = document.createElement('div');
				Content += '<div onclick="Change_Background_Color(\'UnAssign-UserID-'+UserID+'\',this);" style="cursor:pointer;">';
				Content += '<input style="display:none;" name="UnAssign-UserID[]" id="UnAssign-UserID-'+UserID+'" type="checkbox" value="'+UserID+'"/>';
				Content += StaffName;
				Content += '</div>';
				
				Count++;
			}
		}
		
		document.getElementById('UnAssignLayer').innerHTML = Content;
		document.getElementById('UnAssignCountLayer').innerHTML = '('+Count+')';
	}
}
}

// Drag and Drop function
{
function Drag_And_Drop_Init() {
	if($.browser.msie && $('#UnAssignLayer').length > 0){
		$('#UnAssignLayer').height(Math.max($('#UnAssignLayer').parent().height(), 150));
		
		var preferred_height = $('#UnAssignLayer').height();
		var setMinHeightFunc = function(obj){
			var obj = $(obj);
			var obj_parent = obj.parent();
			if(obj_parent.height() < preferred_height){
				obj_parent.height(preferred_height);
			}
			if(obj.height() < preferred_height){
				obj.height(preferred_height);
			}
		};
		$("div.TimeSlotDragBlock").each(function(){
			setMinHeightFunc(this);
		});
		$("div.UnAssignDragBlock").each(function(){
			setMinHeightFunc(this);
		});
		$("div.FullDragBlock").each(function(){
			setMinHeightFunc(this);
		});
	}	
	
<?if ($StaffAttend3->Check_Access_Right('DUTYSETUP-BASIC-Manage') || $StaffAttend3->IS_ADMIN_USER()) {?>
	var StartLeft = -10;
	var StartTop = -10;
	// draggable
	$("div.TimeSlotDragBlock").draggable({
			helper:'clone',
			revert:false,
			cursorAt:{left:StartLeft,top:StartTop},
			distance:'10',
			start: function (ev,ui) {
				ui.helper.html('');
				
				var ElementID = $(this).attr('id');
				var TimeSlotID = ElementID.substring((ElementID.indexOf('-')+1));
				var OnDutyCount = Get_Check_Box_Value(TimeSlotID+'-UserID[]','Array');
				ui.helper.html('<font style="color:#000000;">'+OnDutyCount.length+' people selected</font>');
			},
			stop: function(event, ui) {
				$('div.TimeSlotDropBlock').css('background-color','#FFFFFF');
				$('div#UnAssignLayer').css('background-color','#EEEEEE');
				$('div#FullStaffListLayer').css('background-color','#FFFFFF');
			}
		});
		
	$("div.UnAssignDragBlock").draggable({
			helper:'clone',
			revert:false,
			cursorAt:{left:StartLeft,top:StartTop},
			distance:'10',
			start: function (ev,ui) {
				ui.helper.html('');
				
				var UnAssignCount = Get_Check_Box_Value('UnAssign-UserID[]','Array');
				ui.helper.html(UnAssignCount.length+' people selected');
			},
			stop: function(event, ui) {
				$('div.TimeSlotDropBlock').css('background-color','#FFFFFF');
				$('div#UnAssignLayer').css('background-color','#EEEEEE');
				$('div#FullStaffListLayer').css('background-color','#FFFFFF');
			}
		});
	
	$("div.FullDragBlock").draggable({
			helper:'clone',
			revert:false,
			cursorAt:{left:StartLeft,top:StartTop},
			distance:'10',
			start: function (ev,ui) {
				ui.helper.html('');
				
				var FullCount = Get_Check_Box_Value('Search-UserID[]','Array');
				ui.helper.html(FullCount.length+' people selected');
			},
			stop: function(event, ui) {
				$('div.TimeSlotDropBlock').css('background-color','#FFFFFF');
				$('div#UnAssignLayer').css('background-color','#EEEEEE');
				$('div#FullStaffListLayer').css('background-color','#FFFFFF');
			}
		});
		
	// droppable
	$("div.TimeSlotDragBlock").droppable({
		accept: "div.UnAssignDragBlock, div.FullDragBlock",
		tolerance: "pointer",
		/*activeClass: 'droppable-active',
		hoverClass: 'droppable-hover',*/
		drop: function(ev, ui) {
			ElementID = $(this).attr('id');
			var TimeSlotID = Trim(ElementID.substring((ElementID.indexOf('-')+1)));
			var CurTimeSlotList = Get_Check_Box_Value(TimeSlotID+'-UserID[]','Array',true);
			if (ui.draggable.attr('id') == "UnAssignLayer") {
				var SelectedUser = Get_Check_Box_Value("UnAssign-UserID[]","Array");
			}
			else {
				var SelectedUser = Get_Check_Box_Value("Search-UserID[]","Array");
			}
			
			for (var i=0; i< SelectedUser.length; i++) {
				if (!jIN_ARRAY(CurTimeSlotList,SelectedUser[i])) {
					var UserID = SelectedUser[i];
					var StaffName = document.getElementById('UserName['+UserID+']').value;
					//var Div = document.createElement('div');
					var Content = '<div onclick="Change_Background_Color(\''+TimeSlotID+'-UserID-'+UserID+'\',this);" style="cursor:pointer;">';
					Content += '<input style="display:none;" name="'+TimeSlotID+'-UserID[]" id="'+TimeSlotID+'-UserID-'+UserID+'" type="checkbox" value="'+UserID+'"/>';
          Content += '<font class="staff_name" style="color:#000000;">'+StaffName+'</font>';
          Content += '</div>';
          var Div = $(Content);
          $('#TimeSlot-'+TimeSlotID).append(Div);
				}
			}
			
			Calculate_Unassign_List();
			
			// calculate the count for the time slot column
			var NewTimeSlotList = Get_Check_Box_Value(TimeSlotID+'-UserID[]','Array',true);
			document.getElementById(TimeSlotID+'CountLayer').innerHTML = '('+NewTimeSlotList.length+')';
			
			$('div.TimeSlotDropBlock').css('background-color','#FFFFFF');
			$('div#UnAssignLayer').css('background-color','#EEEEEE');
			$('div#FullStaffListLayer').css('background-color','#FFFFFF');
		},
		activate: function(ev, ui) {
			$('div.TimeSlotDropBlock').css('background-color','#DEB887');
			$('div#UnAssignLayer').css('background-color','#EEEEEE');
			$('div#FullStaffListLayer').css('background-color','#FFFFFF');
		},
		over: function(ev, ui) {
			$(this).css('background-color','#FFFFE6');
		},
		out:  function(ev, ui) {
			$(this).css('background-color','#DEB887');
		}
	});
	
	$("div.UnAssignDropBlock, div.FullDropBlock").droppable({
		accept: "div.TimeSlotDragBlock",
		tolerance: "pointer",
		/*activeClass: 'droppable-active',
		hoverClass: 'droppable-hover',*/
		drop: function(ev, ui) {
			ElementID = ui.draggable.attr('id');
			var TimeSlotID = Trim(ElementID.substring((ElementID.indexOf('-')+1)));
			var SlotFullUserList = Get_Check_Box_Value(TimeSlotID+'-UserID[]','Array',true);
			var SlotSelectedUserList = Get_Check_Box_Value(TimeSlotID+'-UserID[]','Array');
			
			Content = '';
			for (var i=0; i< SlotFullUserList.length; i++) {
				if (!jIN_ARRAY(SlotSelectedUserList,SlotFullUserList[i])) {
					var UserID = SlotFullUserList[i];
					var StaffName = document.getElementById('UserName['+UserID+']').value;
					
					Content += '<div onclick="Change_Background_Color(\''+TimeSlotID+'-UserID-'+UserID+'\',this);" style="cursor:pointer;">';
					Content += '<input style="display:none;" name="'+TimeSlotID+'-UserID[]" id="'+TimeSlotID+'-UserID-'+UserID+'" type="checkbox" value="'+UserID+'"/>';
          Content += '<font class="staff_name" style="color:#000000;">'+StaffName+'</font>';
          Content += '</div>';
				}
			}
			
			ui.draggable.html(Content);
			Calculate_Unassign_List();
			
			// calculate the count for the time slot column
			var NewTimeSlotList = Get_Check_Box_Value(TimeSlotID+'-UserID[]','Array',true);
			document.getElementById(TimeSlotID+'CountLayer').innerHTML = '('+NewTimeSlotList.length+')';
			
			$('div.TimeSlotDropBlock').css('background-color','#FFFFFF');
			$('div#UnAssignLayer').css('background-color','#EEEEEE');
			$('div#FullStaffListLayer').css('background-color','#FFFFFF');
		},
		activate: function(ev, ui) {
			$('div.TimeSlotDropBlock').css('background-color','#FFFFFF');
			$('div#UnAssignLayer, div#FullStaffListLayer').css('background-color','#DEB887');
		},
		over: function(ev, ui) {
			$(this).css('background-color','#FFFFE6');
		},
		out:  function(ev, ui) {
			$(this).css('background-color','#DEB887');
		}
	});
<?}?>
}
}

// thick box function 
{
//on page load call tb_init
function Thick_Box_Init(){   
	tb_init('a.thickbox');//pass where to apply thickbox
}
}

$(document).ready(function(){
	if($.browser.safari || $.browser.webkit || !!navigator.userAgent.match(/Trident\/7\./))
	{
		document.onselectstart = function () { return false; };
	}
});
</script>