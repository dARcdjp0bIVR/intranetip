<?php
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!$StaffAttend3->Check_Access_Right('DUTYSETUP-BASIC'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	echo 'die';
	intranet_closedb();
	exit();
}

$StaffAttend3 = new libstaffattend3();

$GroupID = $_REQUEST['GroupID'];
$StaffID = $_REQUEST['StaffID'];

$StaffAttend3->Start_Trans();
if ($StaffAttend3->Remove_Member($GroupID,$StaffID)) {
	$StaffAttend3->Commit_Trans();
	echo $Lang['StaffAttendance']['DeleteMemberSuccess'];
}
else {
	$StaffAttend3->RollBack_Trans();
	echo $Lang['StaffAttendance']['DeleteMemberFail'];
}

intranet_closedb();
?>