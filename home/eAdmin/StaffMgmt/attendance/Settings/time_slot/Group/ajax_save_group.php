<?php
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!$StaffAttend3->Check_Access_Right('DUTYSETUP-BASIC'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	echo 'die';
	intranet_closedb();
	exit();
}

$StaffAttend3 = new libstaffattend3();

$GroupName = trim(urldecode(stripslashes($_REQUEST['GroupName'])));

if ($StaffAttend3->Save_Group($GroupName)) {
	echo $Lang['StaffAttendance']['SaveGroupSuccess'].'|=|'.$StaffAttend3->db_insert_id();
}
else {
	echo $Lang['StaffAttendance']['SaveGroupFail'];
}

intranet_closedb();
?>