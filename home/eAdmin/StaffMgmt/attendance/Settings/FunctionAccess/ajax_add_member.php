<?php
//editing by Carlos
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

$StaffAttend3 = new libstaffattend3();

if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!$StaffAttend3->Check_Access_Right('SETTINGS-ACCESSRIGHT-Access'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	echo 'die';
	intranet_closedb();
	exit();
}

intranet_auth();
intranet_opendb();

$staffaccessright = new staff_access_right();

$GroupID = $_REQUEST['GroupID'];
$AddUserID = $_REQUEST['AddUserID'];

$staffaccessright->Start_Trans();
if ($staffaccessright->Add_Member($GroupID,$AddUserID)) {
	$staffaccessright->Commit_Trans();
	echo $Lang['StaffAttendance']['AddMemberSuccess'];
}
else {
	$staffaccessright->RollBack_Trans();
	echo $Lang['StaffAttendance']['AddMemberFail'];
}

intranet_closedb();
?>