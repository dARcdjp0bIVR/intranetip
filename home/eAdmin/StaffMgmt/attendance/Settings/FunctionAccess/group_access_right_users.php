<?php
// editing by
/*
 * 	Log
 * 	
 * 	2016-10-28 [Cameron]
 * 		- add Title Tags: Functional Rights / Monitoring Rights by calling $StaffAttend3UI->get_access_rights_tabs
 */
 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend2.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();

if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!$StaffAttend3->Check_Access_Right('SETTINGS-ACCESSRIGHT-Access'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

if(!$_REQUEST['GroupID'] || $_REQUEST['GroupID'] == '')
{
	//header ("Location: index.php");
	header("Location: group_access_right_detail.php");
	intranet_closedb();
	exit();
}

$linterface = new interface_html();
$StaffAttend3UI = new libstaffattend3_ui();

$CurrentPageArr['eAdminStaffAttendance'] = 1;
$CurrentPage['FunctionAccess'] = 1;
//$TAGS_OBJ[] = array($Lang['StaffAttendance']['FunctionAccess'], "", 0);
$TAGS_OBJ = $StaffAttend3UI->get_access_rights_tabs("FunctionalRights");
$MODULE_OBJ = $StaffAttend3UI->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START(urldecode($_REQUEST['Msg']));

echo $StaffAttend3UI->Get_Group_Access_Right_Users_Index($_REQUEST['GroupID']);

$linterface->LAYOUT_STOP();
intranet_closedb();
?>
<script type="text/JavaScript" language="JavaScript">
// dom function 
{
function Check_Go_Search(evt)
{
	var key = evt.which || evt.charCode || evt.keyCode;
	
	if (key == 13) // enter
		Get_Group_User_List();
	else
		return false;
}
}

// ajax function
{
function Get_Group_User_List()
{
	var PostVar = {
			"GroupID": $('Input#GroupID').val(),
			"Keyword": encodeURIComponent($('Input#Keyword').val())
			}

	Block_Element("UserListLayer");
	$.post('ajax_get_group_user_list.php',PostVar,
					function(data){
						if (data == "die") 
							window.top.location = '/';
						else {
							$('div#UserListLayer').html(data);
							Thick_Box_Init();
							UnBlock_Element("UserListLayer");
						}
					});
}

function Get_Add_Member_Form()
{
	var PostVar = {
		"GroupID": $('Input#GroupID').val()
	};
	
	$.post('ajax_get_add_group_member_form.php',PostVar,
		function(data) {
			if (data == "die") 
				window.top.location = '/';
			else 
				$('div#TB_ajaxContent').html(data);
		});
}

function Add_Member()
{
	var PostVar = {
		"GroupID":$('input#GroupID').val(),
		"AddUserID[]":Get_Selection_Value('AddUserID[]','Array')
	}
	
	Block_Thickbox();
	$.post('ajax_add_member.php',PostVar,
		function (data) {
			if (data == "die") 
				window.top.location = '/';
			else {
				Get_Return_Message(data);
				Get_Group_User_List();
				window.top.tb_remove();
				Scroll_To_Top();
			}
		});
}

function Delete_Member()
{
	var StaffID = Get_Check_Box_Value('StaffID[]','Array');
	
	if (StaffID.length > 0) {
		if (confirm('<?=$Lang['StaffAttendance']['AccessRightDeleteGroupMemberWarning']?>')) {
			var PostVar = {
				"GroupID":$('input#GroupID').val(),
				"StaffID[]":StaffID
			}
			
			$.post('ajax_delete_member.php',PostVar,
				function(data) {
					if (data == "die") 
						window.top.location = '/';
					else {
						Get_Return_Message(data);
						Get_Group_User_List();
						Scroll_To_Top();
					}
				});
		}
	}
	else {
		alert('<?=$Lang['StaffAttendance']['SelectAtLeastOneWarning']?>');
	}
}

}

function Check_Group_Title(GroupTitle,GroupID) {
	var GroupID = GroupID || "";
	var PostVar = {
			"GroupID": GroupID,
			"GroupTitle": encodeURIComponent(GroupTitle)
			}
	var ElementObj = $('div#GroupTitleWarningLayer');
	
	if (Trim(GroupTitle) != "") {
		$.post('ajax_check_group_title.php',PostVar,
				function(data){
					if (data == "die") 
						window.top.location = '/';
					else if (data == "1") {
						ElementObj.html('');
						ElementObj.hide();
						if (document.getElementById('GroupTitleWarningRow')) 
							document.getElementById('GroupTitleWarningRow').style.display = 'none';
					}
					else {
						ElementObj.html('<?=$Lang['StaffAttendance']['GroupTitleDuplicateWarning']?>');
						ElementObj.show('fast');
						if (document.getElementById('GroupTitleWarningRow')) 
							document.getElementById('GroupTitleWarningRow').style.display = '';
					}
				});
	}
	else if(Trim(GroupTitle) == ""){
		ElementObj.html('<?=$Lang['SysMgr']['RoleManagement']['RoleNameDuplicateWarning']?>');
		ElementObj.show('fast');
		
		if (document.getElementById('GroupTitleWarningRow')) 
			document.getElementById('GroupTitleWarningRow').style.display = '';
	}
}

function Show_Edit_Icon(LayerObj)
{
	LayerObj.style.backgroundImage = "url(<?=$PATH_WRT_ROOT?>images/<?=$LAYOUT_SKIN?>/icon_edit_b.gif)";
	LayerObj.style.backgroundPosition = "center right";
	LayerObj.style.backgroundRepeat = "no-repeat";
}

function Hide_Edit_Icon(LayerObj)
{
	LayerObj.style.backgroundImage = "";
	LayerObj.style.backgroundPosition = "";
	LayerObj.style.backgroundRepeat = "";
}

// jEditable function 
function Init_JEdit_Input(objDom)
{
	var WarningLayer = "div#GroupTitleWarningLayer";
	$(objDom).editable
	( 
      function(value, settings)
      {
    	var ElementObj = $(this);
    	if (Trim($(WarningLayer).html()) == "" && ElementObj[0].revert != value)
    	{
    		if($('Input#GroupID').val() == '')
	    	{
	    		ElementObj.html(value);
	    		$('span#GroupTitleNavLayer').html(value);
				$('Input#GroupTitle').val(value);
	    	}
	    	else
	    	{
	    		var PostVar = {
	    			//"GroupID":ElementObj.attr("id"),
	    			"GroupID":$('Input#GroupID').val(),
	    			"GroupTitle":encodeURIComponent(value)
	    		};
		    				     
		    	$.post('ajax_rename_group.php',PostVar,
		    		function (data) {
		    			Get_Return_Message(Trim(data));
		    			ElementObj.html(value);
						$('span#GroupTitleNavLayer').html(value);
						$('Input#GroupTitle').val(value);
		    		});
	    	}
		}
		else {
			ElementObj[0].reset();
		}
	  }, 
	  {
	    tooltip   : "<?=$Lang['SysMgr']['FormClassMapping']['ClickToEdit']?>",
	    event : "click",
	    onblur : "submit",
	    type : "text",     
	    style  : "display: inline",
	    height: "20px",
	    maxlength: 100,
	    onreset: function() {
	    	$(WarningLayer).html('');
	    	$(WarningLayer).hide();
	  	}
  	  }
  	);
  
  $(objDom).keyup
  (
  	 function()
  	 {
		var GroupTitle = Trim($('form input').val());
		//var GroupID = $(this).attr('id');
		//var GroupTitle = Trim($('Input#GroupTitle').val());
		var GroupID = $('Input#GroupID').val();
		Check_Group_Title(GroupTitle,GroupID);
	 }
  );
}

function Init_JEdit_Input2(objDom)
{
	$(objDom).editable
	( 
      function(value, settings)
      {
    	var ElementObj = $(this);
    	ElementObj.html(value);
		$('Input#GroupDescription').val(value);
		
		if($('Input#GroupID').val() != '' && ElementObj[0].revert != value)
		{
			var PostVar = {
	    			"GroupID":$('Input#GroupID').val(),
	    			"GroupDescription":encodeURIComponent(value)
	    		};
		    				     
		    	$.post('ajax_set_group_description.php',PostVar,
		    		function (data) {
		    			Get_Return_Message(Trim(data));
		    		});
		}
	  }, 
	  {
	    tooltip   : "<?=$Lang['SysMgr']['FormClassMapping']['ClickToEdit']?>",
	    event : "click",
	    onblur : "submit",
	    type : "text",     
	    style  : "display: inline",
	    height: "20px",
	    maxlength: 100,
	    onreset: function() {
	    	
	  	}
  	  }
  	);
}


// thick box function 
{
//on page load call tb_init
function Thick_Box_Init()
{   
	tb_init('a.thickbox');//pass where to apply thickbox
}
}

$(document).ready(
	function()
	{		
		Init_JEdit_Input('span.jEditInput');
		Init_JEdit_Input2('span.jEditInput2');
	}
);
</script>