<?php
// editing by 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();

if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!$StaffAttend3->Check_Access_Right('SETTINGS-ACCESSRIGHT-Access'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$laccessright = new staff_access_right();

$GroupTitle = trim(urldecode(stripslashes($_REQUEST['GroupTitle'])));
$GroupID = $_REQUEST['GroupID'];
$GroupDescription = trim(urldecode(stripslashes($_REQUEST['GroupDescription'])));
$Right = (is_array($_REQUEST['Right']))? $_REQUEST['Right']:array();

if(trim($GroupTitle) == "")
{
	header("Location: index.php");
	intranet_closedb();
	exit();
}

$laccessright->Start_Trans();
$GroupID = $laccessright->Insert_Group_Access_Right($GroupID, $GroupTitle, $GroupDescription,$Right);
if($GroupID != false) {
	$laccessright->Commit_Trans();
	$Msg = $Lang['StaffAttendance']['SettingApplySuccess'];
}
else {
	$laccessright->RollBack_Trans();
	$Msg = $Lang['StaffAttendance']['SettingApplyFail'];
}
//header("Location: group_access_right_detail.php?GroupID=".$GroupID."&Msg=".urlencode($Msg));
header("Location: index.php?Msg=".urlencode($Msg));
intranet_closedb();
?>