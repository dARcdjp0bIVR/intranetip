<?php
// Editing by 
/*
 * 2016-12-14 (Carlos): $sys_custom['StaffAttendance']['WongFutNamCollege'] - added [Attend One Time Slot].
 * 2016-03-15 (Carlos): Improved and added full day on leave and full day outing. 
 * 2015-08-04 (Carlos): Created.
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend2.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!$StaffAttend3->Check_Access_Right('DUTYSETUP-SPECIAL'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$linterface = new interface_html();
$StaffAttend3UI = new libstaffattend3_ui();
$limport = new libimporttext();
$lf = new libfilesystem();

function isTimeSlotsOverlapping($timeslots, $new_timeslot)
{
	$count = count($timeslots);
	for($i=0;$i<$count;$i++){
		$start_time = $timeslots[$i][0];
		$end_time = $timeslots[$i][1];
		
		if( ($start_time >= $new_timeslot[0] && $start_time <= $new_timeslot[1]) 
			|| ($end_time >= $new_timeslot[0] && $end_time <= $new_timeslot[1])
			|| ($new_timeslot[0] >= $start_time && $new_timeslot[0] <= $end_time)
			|| ($new_timeslot[1] >= $end_time && $new_timeslot[1] <= $end_time))
		{
			return true;
		}
	}
	return false;
}

$format_group_array = array("Group ID","User Logins","Date","Duty Type","Time Slot","Duty Start","Duty End","Duty Count","Waive In","Waive Out","Reason ID");
if($sys_custom['StaffAttendance']['WongFutNamCollege']){
	$format_group_array = array_merge($format_group_array, array("Attend One Time Slot"));
}
$format_individual_array = array("User Login","Date","Duty Type","Time Slot","Duty Start","Duty End","Duty Count","Waive In","Waive Out","Reason ID");
if($sys_custom['StaffAttendance']['WongFutNamCollege']){
	$format_individual_array = array_merge($format_individual_array, array("Attend One Time Slot"));
}
$duty_type_array = array("D","L","O","N","FL","FO");
$dutyTypeToDisplay = array("D"=>$Lang['StaffAttendance']['OnDuty'],"L"=>$Lang['StaffAttendance']['Holiday'],"O"=>$Lang['StaffAttendance']['Outing'],"N"=>$Lang['StaffAttendance']['NoDuty'],"FL"=>$Lang['StaffAttendance']['FullDayHoliday'],"FO"=>$Lang['StaffAttendance']['FullDayOutgoing']);
$time_slot_forbidden_chars = array('%', '&', '^', '@', '"', '\\', '\'', ':', ';', '<', '>', ',', '.', '/', '?', '[', ']', '~', '!', '#', '$', '*', '(', ')', '{', '}', '|', '=', '+', '-');
$yn_values = array("Y","N");
$yn_display_values = array("Y"=>$Lang['General']['Yes'],"N"=>$Lang['General']['No']);
$values_01 = array("0","1");
$display_values_01 = array("1"=>$Lang['General']['Yes'],"0"=>$Lang['General']['No']);
$filepath = $_FILES["userfile"]["tmp_name"];
$filename = $_FILES["userfile"]["name"];

if($filepath=="none" || $filepath == "" || !in_array($format,array("1","2")))
{
	# import failed
    header("Location: index.php?Msg=".urlencode($Lang['StaffAttendance']['DataImportFail']));
    intranet_closedb();
    exit();
}else
{
	$format_array = $format == "1" ? $format_group_array : $format_individual_array;
	
	$ext = strtoupper($lf->file_ext($filename));
    if($limport->CHECK_FILE_EXT($filename))
    {
        # read file into array
        # return 0 if fail, return csv array if success
        //$data = $lf->file_read_csv($filepath);
        $data = $limport->GET_IMPORT_TXT($filepath);
        if(sizeof($data)>0)
        {
        	$toprow = array_shift($data);                   # drop the title bar
        }else
        {
        	header("Location: index.php?Msg=".urlencode($Lang['StaffAttendance']['DataImportFail']));
		    intranet_closedb();
		    exit();
        }
        
        for ($i=0; $i<sizeof($format_array); $i++)
		{
			 if ($toprow[$i] != $format_array[$i])
			 {
			     header("Location: index.php?Msg=".urlencode($Lang['StaffAttendance']['DataImportFail']));
			     intranet_closedb();
			     exit();
			 }
		}
    }
    
    $data_count = count($data);
    
    $sql = "DROP TABLE TEMP_CARD_STAFF_IMPORT_SPECIAL_DUTY";
    $StaffAttend3->db_db_query($sql);
    
    $sql = "CREATE TABLE TEMP_CARD_STAFF_IMPORT_SPECIAL_DUTY (
				RecordID int(11) NOT NULL auto_increment PRIMARY KEY,
				GroupID int(11) DEFAULT NULL,
				UserID text NOT NULL,
				RecordDate date NOT NULL,
				DutyType varchar(2) NOT NULL,
				TimeSlot varchar(255) DEFAULT NULL,
				DutyStart time DEFAULT NULL,
				DutyEnd time DEFAULT NULL,
				DutyCount float(11) DEFAULT NULL,
				WaiveIn varchar(1) DEFAULT NULL,
				WaiveOut varchar(1) DEFAULT NULL,
				ReasonID int(11) DEFAULT NULL,
				AttendOne varchar(1) DEFAULT '0' 
			) ENGINE=InnoDB DEFAULT CHARSET=utf8";
    $StaffAttend3->db_db_query($sql);
    
    // prepare group staff data
    if($format == "1")
    {
    	$groupIdToRecord = array();
    	$groupIdToUserlogin = array();
    	$userloginToRecord = array();
    	$groups = $StaffAttend3->Get_Group_List();
		$group_count = count($groups);
		for($i=0;$i<$group_count;$i++){
			$group_id = $groups[$i]['GroupID'];
			$group_name = $groups[$i]['GroupName'];
			$groupIdToUserlogin[$group_id] = array();
			$members = $StaffAttend3->Get_Group_Member_List($group_id);
			$member_count = count($members);
			$groupIdToRecord[$group_id] = $groups[$i];
			for($j=0;$j<$member_count;$j++){
				$user_login = $members[$j]['UserLogin'];
				$groupIdToUserlogin[$group_id][$user_login] = $members[$j];
				$userloginToRecord[$user_login] = $members[$j];
				$userloginToRecord[$user_login]['GroupID'] = $group_id;
			}
		}
    }
    
    // prepare individual staff data
    if($format == "2"){
    	$userloginToRecord = array();
    	$staffs = $StaffAttend3->Get_Aval_User_List();
		$staff_count = count($staffs);
		for($i=0;$i<$staff_count;$i++){
			$user_login = $staffs[$i]['UserLogin'];
			$userloginToRecord[$user_login] = $staffs[$i];
		}
    }
    
    // prepare reason data
    $reasonIdToRecord = array();
    $reasons = $StaffAttend3->Get_Preset_Leave_Reason_List();
	$reason_count = count($reasons);
    for($i=0;$i<$reason_count;$i++){
		$reason_id = $reasons[$i]['ReasonID'];
		//$reason_type = $reasons[$i]['ReasonType'];
		$reasonIdToRecord[$reason_id] = $reasons[$i];
    }
    
    $rows = array(); // data for display
    $rowToError = array(); // error data for display
    
    $userloginToDuty = array();
    $dateUserloginToTimeSlot = array();
    $dateUserloginToNoDuty = array();
    $dateUserloginToFulldayLeave = array();
    $dateUserloginToFulldayOuting = array();
    
    for($i=0;$i<$data_count;$i++){
    	$row_num = $i+1;
    	$cols = array();
    	$col_num = -1;
    	if($format == "1"){ // group
    		$t_group_id = trim($data[$i][0]);
    		$t_userlogins = trim($data[$i][1]);
    		$t_date = trim($data[$i][2]);
    		$t_duty_type = strtoupper(trim($data[$i][3]));
    		$t_time_slot = trim($data[$i][4]);
    		$t_duty_start = trim($data[$i][5]);
    		$t_duty_end = trim($data[$i][6]);
    		$t_duty_count = trim($data[$i][7]);
    		$t_waive_in = strtoupper(trim($data[$i][8]));
    		$t_waive_out = strtoupper(trim($data[$i][9]));
    		$t_reason_id = trim($data[$i][10]);
    		if($sys_custom['StaffAttendance']['WongFutNamCollege']){
    			$t_attend_one = trim($data[$i][11]);
    		}
    		
    		$col_num+=1;
    		if($t_group_id == ""){
    			$rowToError[$row_num][] = $Lang['StaffAttendance']['ImportSpecialDutyWarning']['BlankGroupID'];
    			$cols[$col_num] = "";
    		}else if(!isset($groupIdToUserlogin[$t_group_id])){
    			$rowToError[$row_num][] = str_replace("%1", $t_group_id, $Lang['StaffAttendance']['ImportSpecialDutyWarning']['GroupIDIsNotFound']);
    			$cols[$col_num] = "";
    		}else{
    			$cols[$col_num] = $groupIdToRecord[$t_group_id]['GroupName'];
    		}
    		
    		$col_num+=1;
    		$t_userlogin_ary = array();
    		$t_userid_ary = array();
    		if($t_userlogins == ""){
    			$rowToError[$row_num][] = $Lang['StaffAttendance']['ImportSpecialDutyWarning']['BlankUserLogins'];
    			$cols[$col_num] = "";
    		}else{
    			$t_userlogin_ary = explode(",",$t_userlogins);
    			$usernames = array();
    			for($j=0;$j<sizeof($t_userlogin_ary);$j++){
    				$t_userlogin_ary[$j] = trim($t_userlogin_ary[$j]);
    				
    				if(!isset($userloginToRecord[$t_userlogin_ary[$j]])){
    					$rowToError[$row_num][] = str_replace("%1", $t_userlogin_ary[$j], $Lang['StaffAttendance']['ImportSpecialDutyWarning']['UserloginIsNotFound']);
    				}else if(!isset($groupIdToUserlogin[$t_group_id][$t_userlogin_ary[$j]])){
    					$rowToError[$row_num][] = str_replace(array("%1","%2"),array($t_userlogin_ary[$j],$t_group_id),$Lang['StaffAttendance']['ImportSpecialDutyWarning']['UserloginNotBelongToGroup']);
    				}else {
    					$usernames[] = $userloginToRecord[$t_userlogin_ary[$j]]['StaffName'];
    					$t_userid_ary[] = $userloginToRecord[$t_userlogin_ary[$j]]['UserID'];
    				}
    			}
    			$cols[$col_num] = implode(", ", $usernames);
    			$data[$i][1] = implode(",",$t_userid_ary);
    		}
    		
    	}else{
    		$t_userlogin = trim($data[$i][0]);
    		$t_date = trim($data[$i][1]);
    		$t_duty_type = strtoupper(trim($data[$i][2]));
    		$t_time_slot = trim($data[$i][3]);
    		$t_duty_start = trim($data[$i][4]);
    		$t_duty_end = trim($data[$i][5]);
    		$t_duty_count = trim($data[$i][6]);
    		$t_waive_in = strtoupper(trim($data[$i][7]));
    		$t_waive_out = strtoupper(trim($data[$i][8]));
    		$t_reason_id = trim($data[$i][9]);
    		if($sys_custom['StaffAttendance']['WongFutNamCollege']){
    			$t_attend_one = trim($data[$i][10]);
    		}
    		
    		$col_num += 1;
    		if($t_userlogin == ""){
    			$rowToError[$row_num][] = $Lang['StaffAttendance']['ImportSpecialDutyWarning']['BlankUserLogin'];
    			$cols[$col_num] = "";
    		}else if(!isset($userloginToRecord[$t_userlogin])){
    			$rowToError[$row_num][] = str_replace("%1",$t_userlogin,$Lang['StaffAttendance']['ImportSpecialDutyWarning']['IndividualUserIsNotFound']);
    			$cols[$col_num] = "";
    		}else{
    			$cols[$col_num] = $userloginToRecord[$t_userlogin]['StaffName'];
    			$data[$i][0] = $userloginToRecord[$t_userlogin]['UserID'];
    			$t_userlogin_ary = array($t_userlogin);
    		}
    	}
    	
    	$col_num += 1;
    	$cols[$col_num] = $t_date;
    	if($t_date == ""){
    		$rowToError[$row_num][] = $Lang['StaffAttendance']['ImportSpecialDutyWarning']['BlankDate'];
    	}else if(preg_match('/^\d\d\d\d-\d\d-\d\d$/', $t_date)==0){
    		$rowToError[$row_num][] = "Date is invalid.";
    	}else{
    		$t_ymd = explode("-",$t_date);
    		if(!checkdate(intval($t_ymd[1]), intval($t_ymd[2]), intval($t_ymd[0]))){
    			$rowToError[$row_num][] = $Lang['StaffAttendance']['ImportSpecialDutyWarning']['DateIsInvalid'];
    		}
    	}
    	
    	$col_num += 1;
    	if($t_duty_type == ""){
    		$rowToError[$row_num][] = $Lang['StaffAttendance']['ImportSpecialDutyWarning']['BlankDutyType'];
    		$cols[$col_num] = "";
    	}else if(!in_array($t_duty_type, $duty_type_array))
    	{
    		$rowToError[$row_num][] = $Lang['StaffAttendance']['ImportSpecialDutyWarning']['DutyTypeValueInvalid'];
    		$cols[$col_num] = "";
    	}else {
    		$cols[$col_num] = $dutyTypeToDisplay[$t_duty_type];
    	}
    	
    	$is_no_duty = $t_duty_type == "N";
    	$is_full_day_leave = $t_duty_type == "FL" || $t_duty_type == "FO";
    	
    	$col_num += 1;
    	$cols[$col_num] = $t_time_slot;
    	if($t_time_slot == ""){
    		if(!$is_no_duty && !$is_full_day_leave){
				$rowToError[$row_num][] = $Lang['StaffAttendance']['ImportSpecialDutyWarning']['BlankTimeSlot'];
    		}
		}else if(!$is_no_duty && !$is_full_day_leave){
			for($n=0;$n<strlen($t_time_slot);$n++){
				$c = $t_time_slot[$n];
				if(in_array($c, $time_slot_forbidden_chars)){
					$rowToError[$row_num][] = str_replace("%1", $t_time_slot, $Lang['StaffAttendance']['ImportSpecialDutyWarning']['TimeSlotContainsInvalidSymbols']);
					break;
				}
			}
		}
		
		$col_num += 1;
		$cols[$col_num] = $t_duty_start;
		if($t_duty_start == ""){
			if(!$is_no_duty && !$is_full_day_leave){
				$rowToError[$row_num][] = $Lang['StaffAttendance']['ImportSpecialDutyWarning']['BlankDutyStart'];
			}
		}else if(!$is_no_duty && !$is_full_day_leave && !preg_match("/(2[0-4]|[01][1-9]|10):([0-5][0-9])/", $t_duty_start)){
			$rowToError[$row_num][] = $Lang['StaffAttendance']['ImportSpecialDutyWarning']['DutyStartTimeIsInvalid'];
		}
		
		$col_num += 1;
		$cols[$col_num] = $t_duty_end;
		if($t_duty_end == ""){
			if(!$is_no_duty && !$is_full_day_leave)
				$rowToError[$row_num][] = $Lang['StaffAttendance']['ImportSpecialDutyWarning']['BlankDutyEnd'];
		}else if(!$is_no_duty && !$is_full_day_leave && !preg_match("/(2[0-4]|[01][1-9]|10):([0-5][0-9])/", $t_duty_end)){
			$rowToError[$row_num][] = $Lang['StaffAttendance']['ImportSpecialDutyWarning']['DutyEndTimeIsInvalid'];
		}
    	
    	$col_num += 1;
		$cols[$col_num] = $t_duty_count;
    	if($t_duty_count == ""){
    		if(!$is_no_duty && !$is_full_day_leave)
    			$rowToError[$row_num][] = $Lang['StaffAttendance']['ImportSpecialDutyWarning']['BlankDutyCount'];
    	}else if(!$is_no_duty && !$is_full_day_leave && !is_numeric($t_duty_count) || $t_duty_count < 0){
    		$rowToError[$row_num][] = $Lang['StaffAttendance']['ImportSpecialDutyWarning']['DutyCountMustUseNonNegativeNumber'];
    	}
    	
    	$col_num += 1;
		$cols[$col_num] = $t_waive_in;
    	if($t_waive_in == ""){
    		if(!$is_no_duty && !$is_full_day_leave)
    			$rowToError[$row_num][] = $Lang['StaffAttendance']['ImportSpecialDutyWarning']['BlankWaiveIn'];
    	}else if(!$is_no_duty && !$is_full_day_leave && !in_array($t_waive_in,$yn_values)){
    		$rowToError[$row_num][] = $Lang['StaffAttendance']['ImportSpecialDutyWarning']['WaiveInValueInvalid'];
    	}else{
    		$cols[$col_num] = $yn_display_values[$t_waive_in];
    	}
    	
    	$col_num += 1;
		$cols[$col_num] = $t_waive_out;
    	if($t_waive_out == ""){
    		if(!$is_no_duty && !$is_full_day_leave)
    			$rowToError[$row_num][] = $Lang['StaffAttendance']['ImportSpecialDutyWarning']['BlankWaiveOut'];
    	}else if(!$is_no_duty && !$is_full_day_leave && !in_array($t_waive_out,$yn_values)){
    		$rowToError[$row_num][] = $Lang['StaffAttendance']['ImportSpecialDutyWarning']['WaiveOutValueInvalid'];
    	}else{
    		$cols[$col_num] = $yn_display_values[$t_waive_out];
    	}
    	
    	$col_num += 1;
    	$cols[$col_num] = "";
    	if($t_reason_id != ""){
    		if(isset($reasonIdToRecord[$t_reason_id])){
    			$t_reason_type = $reasonIdToRecord[$t_reason_id]['ReasonType'];
    			if((($t_duty_type == "L" || $t_duty_type == "FL") && $t_reason_type != CARD_STATUS_HOLIDAY) || (($t_duty_type == "O" || $t_duty_type == "FO") && $t_reason_type != CARD_STATUS_OUTGOING)){ // holiday leave or outing
    				$rowToError[$row_num][] = $Lang['StaffAttendance']['ImportSpecialDutyWarning']['ReasonMismatchesWithDutyType'];
    			}else{
    				$cols[$col_num] = $reasonIdToRecord[$t_reason_id]['ReasonText'];
    			}
    		}
    	}
    	
    	if($sys_custom['StaffAttendance']['WongFutNamCollege']){
    		$col_num += 1;
    		if(!in_array($t_attend_one,array('0','1'))){
    			$t_attend_one = '0';
    		}
    		$cols[$col_num] = $display_values_01[$t_attend_one];
    	}
    	
    	$rows[] = $cols;
    	
    	if(!isset($rowToError[$row_num])){ // no error
    		$duty_time = array($t_date." ".$t_duty_start, $t_date." ".$t_duty_end);
    		if(!isset($dateUserloginToTimeSlot[$t_date])){
    			$dateUserloginToTimeSlot[$t_date] = array();
    		}
    		
    		if($is_no_duty){
    			// assign no duty flag by date & userlogin
    			if(!isset($dateUserloginToNoDuty[$t_date])){
    				$dateUserloginToNoDuty[$t_date] = array();
    			}
    			for($j=0;$j<count($t_userlogin_ary);$j++){
    				$dateUserloginToNoDuty[$t_date][$t_userlogin_ary[$j]] = 1;
    			}
    		}else if($is_full_day_leave)
    		{
    			if($t_duty_type == "FL")
    			{
	    			if(!isset($dateUserloginToFulldayLeave[$t_date])){
	    				$dateUserloginToFulldayLeave[$t_date] = array();
	    			}
	    			for($j=0;$j<count($t_userlogin_ary);$j++){
	    				$dateUserloginToFulldayLeave[$t_date][$t_userlogin_ary[$j]] = 1;
	    			}
    			}else if($t_duty_type == "FO"){
    				if(!isset($dateUserloginToFulldayOuting[$t_date])){
	    				$dateUserloginToFulldayOuting[$t_date] = array();
	    			}
	    			for($j=0;$j<count($t_userlogin_ary);$j++){
	    				$dateUserloginToFulldayOuting[$t_date][$t_userlogin_ary[$j]] = 1;
	    			}
    			}
    			for($j=0;$j<count($t_userlogin_ary);$j++){
	    			if(isset($dateUserloginToTimeSlot[$t_date]) && isset($dateUserloginToTimeSlot[$t_date][$t_userlogin_ary[$j]])
	    				 && count($dateUserloginToTimeSlot[$t_date][$t_userlogin_ary[$j]])>0){
	    				$rowToError[$row_num][] = str_replace("%1",$userloginToRecord[$t_userlogin_ary[$j]]['StaffName'],$Lang['StaffAttendance']['ImportSpecialDutyWarning']['StaffHasSetDutyThatDay']);
	    			}
    			}
    		}
    		else{
	    		for($j=0;$j<count($t_userlogin_ary);$j++){
	    			// check overlapping duty time
	    			if(!isset($userloginToDuty[$t_userlogin_ary[$j]])){
	    				$userloginToDuty[$t_userlogin_ary[$j]] = array();
	    			}
	    			if(isTimeSlotsOverlapping($userloginToDuty[$t_userlogin_ary[$j]], $duty_time)){
	    				$rowToError[$row_num][] = str_replace("%1",$userloginToRecord[$t_userlogin_ary[$j]]['StaffName'],$Lang['StaffAttendance']['ImportSpecialDutyWarning']['StaffHasOverlappingTimeSlots']);
	    			}else {
	    				$userloginToDuty[$t_userlogin_ary[$j]][] = $duty_time;
	    			}
	    			// check time slot name conflict in the same day
	    			if(!isset($dateUserloginToTimeSlot[$t_date][$t_userlogin_ary[$j]])){
	    				$dateUserloginToTimeSlot[$t_date][$t_userlogin_ary[$j]] = array();
	    			}
	    			if(isset($dateUserloginToTimeSlot[$t_date][$t_userlogin_ary[$j]][$t_time_slot])){
	    				$rowToError[$row_num][] = str_replace("%1",$userloginToRecord[$t_userlogin_ary[$j]]['StaffName'],$Lang['StaffAttendance']['ImportSpecialDutyWarning']['StaffHasSameTimeSlotName']);
	    			}else{
	    				$dateUserloginToTimeSlot[$t_date][$t_userlogin_ary[$j]][$t_time_slot] = $t_time_slot;
	    			}
	    			
	    			if($dateUserloginToNoDuty[$t_date][$t_userlogin_ary[$j]] == 1 && count($dateUserloginToTimeSlot[$t_date][$t_userlogin_ary[$j]])>0){
	    				$rowToError[$row_num][] = str_replace("%1",$userloginToRecord[$t_userlogin_ary[$j]]['StaffName'],$Lang['StaffAttendance']['ImportSpecialDutyWarning']['StaffShouldNotHaveDutyOnThatDay']);
	    			}
	    			if($dateUserloginToFulldayLeave[$_date][$t_userlogin_ary[$j]] == 1 && count($dateUserloginToTimeSlot[$t_date][$t_userlogin_ary[$j]])>0){
	    				$rowToError[$row_num][] = str_replace("%1",$userloginToRecord[$t_userlogin_ary[$j]]['StaffName'],$Lang['StaffAttendance']['ImportSpecialDutyWarning']['StaffHasSetFulldayLeaveThatDay']);
	    			}
	    			if($dateUserloginToFulldayOuting[$_date][$t_userlogin_ary[$j]] == 1 && count($dateUserloginToTimeSlot[$t_date][$t_userlogin_ary[$j]])>0){
	    				$rowToError[$row_num][] = str_replace("%1",$userloginToRecord[$t_userlogin_ary[$j]]['StaffName'],$Lang['StaffAttendance']['ImportSpecialDutyWarning']['StaffHasSetFulldayOutingThatDay']);
	    			}
	    		}
    		}
    	}
    }
}

$TotalRecord = $data_count;
$TotalInvalidRecord = count($rowToError);
$TotalValidRecord = $TotalRecord - $TotalInvalidRecord;

if($TotalRecord == $TotalValidRecord){	
	$insert_sql = "INSERT INTO TEMP_CARD_STAFF_IMPORT_SPECIAL_DUTY (".($format=="1"? "GroupID,":"")."UserID,RecordDate,DutyType,TimeSlot,DutyStart,DutyEnd,DutyCount,WaiveIn,WaiveOut,ReasonID".($sys_custom['StaffAttendance']['WongFutNamCollege']?",AttendOne":"").") VALUES ";
	$chunks = array_chunk($data, 200);
	$col_size = $format == 1? 11 : 10;
	if($sys_custom['StaffAttendance']['WongFutNamCollege']) $col_size += 1;
	for($i=0;$i<sizeof($chunks);$i++){
		$values = "";
		$delim = "";
		for($j=0;$j<sizeof($chunks[$i]);$j++){
			$tmp_cols = array();
			for($k=0;$k<$col_size;$k++){
				$tmp_cols[] = "'".$StaffAttend3->Get_Safe_Sql_Query($chunks[$i][$j][$k])."'";
			}
			$values .= $delim."(".implode(",",$tmp_cols).")";
			$delim = ",";
		}
		$sql = $insert_sql.$values;
		$StaffAttend3->db_db_query($sql);
	}
}


$PAGE_NAVIGATION[] = array($Lang['StaffAttendance']['SpecialRoster'],$PATH_WRT_ROOT.'home/eAdmin/StaffMgmt/attendance/Management/roster/group/normal/');
$PAGE_NAVIGATION[] = array($Lang['StaffAttendance']['ImportSpecialDuty'],'');

# step information
$STEPS_OBJ[] = array($Lang['StaffAttendance']['SelectCSVFile'], 0);
$STEPS_OBJ[] = array($Lang['StaffAttendance']['CSVConfirmation'], 1);
$STEPS_OBJ[] = array($Lang['StaffAttendance']['ImportResult'], 0);

$CurrentPageArr['eAdminStaffAttendance'] = 1;
$CurrentPage['DutySpecial'] = 1;
$TAGS_OBJ[] = array($Lang['StaffAttendance']['SpecialRoster'], $PATH_WRT_ROOT.'home/eAdmin/StaffMgmt/attendance/Management/roster/group/normal/', 1);
$TAGS_OBJ[] = array($Lang['StaffAttendance']['WholeSchoolSpecialDaySetting'], $PATH_WRT_ROOT.'home/eAdmin/StaffMgmt/attendance/Management/roster/special_duty/', 0);
$MODULE_OBJ = $StaffAttend3UI->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START(urldecode($_REQUEST['Msg']));

?>
<br />
<div>
	<div class="table_board">
	<form name="form1" method="POST" action="update.php">
	<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td><?=$StaffAttend3UI->GET_NAVIGATION($PAGE_NAVIGATION)?></td>
		</tr>
		<tr>
			<td><?=$StaffAttend3UI->GET_STEPS($STEPS_OBJ)?></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr>
	</table>
	<table width="90%" border="0" cellpadding="3" cellspacing="0" align="center">
		<tr>
			<td class="formfieldtitle" width="30%" align="left"><?=$Lang['General']['TotalRecord']?></td>
			<td class="tabletext"><?=$TotalRecord?></td>
		</tr>
		<tr>
			<td class="formfieldtitle" width="30%" align="left"><?=$Lang['General']['SuccessfulRecord']?></td>
			<td class="tabletext"><?=$TotalValidRecord?></td>
		</tr>
		<tr>
			<td class="formfieldtitle" width="30%" align="left"><?=$Lang['General']['FailureRecord']?></td>
			<td class="tabletext"><?=$TotalInvalidRecord?></td>
		</tr>
	</table>
	<table width="90%" border="0" cellpadding="3" cellspacing="0" align="center">
		<thead>
			<tr>
				<th class="tablebluetop tabletopnolink" width="1" align="left">#</th>
				<?php if($format == 1){ ?>
				<th class="tablebluetop tabletopnolink" align="left"><?=$Lang['StaffAttendance']['ImportSpecialDutyHeader']['Group']?></th>
				<?php } ?>
				<th class="tablebluetop tabletopnolink" align="left"><?=$Lang['StaffAttendance']['ImportSpecialDutyHeader']['Staff']?></th>
				<th class="tablebluetop tabletopnolink" align="left"><?=$Lang['StaffAttendance']['ImportSpecialDutyHeader']['Date']?></th>
				<th class="tablebluetop tabletopnolink" align="left"><?=$Lang['StaffAttendance']['ImportSpecialDutyHeader']['DutyType']?></th>
				<th class="tablebluetop tabletopnolink" align="left"><?=$Lang['StaffAttendance']['ImportSpecialDutyHeader']['TimeSlot']?></th>
				<th class="tablebluetop tabletopnolink" align="left"><?=$Lang['StaffAttendance']['ImportSpecialDutyHeader']['DutyStart']?></th>
				<th class="tablebluetop tabletopnolink" align="left"><?=$Lang['StaffAttendance']['ImportSpecialDutyHeader']['DutyEnd']?></th>
				<th class="tablebluetop tabletopnolink" align="left"><?=$Lang['StaffAttendance']['ImportSpecialDutyHeader']['DutyCount']?></th>
				<th class="tablebluetop tabletopnolink" align="left"><?=$Lang['StaffAttendance']['ImportSpecialDutyHeader']['WaiveIn']?></th>
				<th class="tablebluetop tabletopnolink" align="left"><?=$Lang['StaffAttendance']['ImportSpecialDutyHeader']['WaiveOut']?></th>
				<th class="tablebluetop tabletopnolink" align="left"><?=$Lang['StaffAttendance']['ImportSpecialDutyHeader']['Reason']?></th>
				<?php if($sys_custom['StaffAttendance']['WongFutNamCollege']){ ?>
				<th class="tablebluetop tabletopnolink" align="left"><?=$Lang['StaffAttendance']['ImportSpecialDutyHeader']['AttendOneTimeSlot']?></th>
				<?php } ?>
				<th class="tablebluetop tabletopnolink" align="left"><?=$Lang['StaffAttendance']['ImportSpecialDutyHeader']['Errors']?></th>
			</tr>
		</thead>
		<tbody>
	<?php
		$colspan = 13;
		if($sys_custom['StaffAttendance']['WongFutNamCollege']) $colspan += 1;
		if(count($rows)==0){
			$x = '<tr><td align="center" colspan="'.$colspan.'">'.$Lang['General']['NoRecordFound'].'</td></tr>';
		}else{
			$col_count = count($rows[0]);
			$x = '';
			for($i=0;$i<$TotalRecord;$i++)
			{
				$css_i = ($i % 2) ? "2" : "";
				$x .= '<tr>';
				$x .= '<td valign="top" class="tablebluerow'.$css_i.'" width="1">'.($i+1).'</td>';
				for($j=0;$j<$col_count;$j++){
					$x .= '<td valign="top" class="tablebluerow'.$css_i.'">'.htmlspecialchars($rows[$i][$j],ENT_QUOTES).'</td>';
				}
				$x .= '<td valign="top" class="tablebluerow'.$css_i.'">'.(isset($rowToError[$i+1])?'<font style="color:red;">'.implode('<br />',$rowToError[$i+1]).'</font>':'&nbsp;').'</td>';
				$x .= '</tr>'."\n";
			}
		}
		echo $x;
	?>
		</tbody>
	</table>
	<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tr>
			<td class="dotline"><img src="<?=$PATH_WRT_ROOT?>images/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1" /></td>
		</tr>
	</table>
	<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tr>
			<td align="center" colspan="2">
			<?php 
			if($TotalRecord == $TotalValidRecord)
			{
				echo $StaffAttend3UI->GET_ACTION_BTN($Lang['Btn']['Submit'], "submit", "", "", " class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"").'&nbsp;';
			}
			echo $StaffAttend3UI->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "javascript:window.location.href='index.php'","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"");
			?>
			</td>
		</tr>
	</table>
	<input type="hidden" name="format" value="<?=$format?>" />
	</form>
	</div>
</div>
<br />
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>