<?php
// Editing by 
// !!!!!! Please use UTF-8 encoding. !!!!!!
/*
 * 2016-12-14 (Carlos): $sys_custom['StaffAttendance']['WongFutNamCollege'] - added [Attend One Time Slot].
 * 2015-08-04 (Carlos): Created. 
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend2.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!$StaffAttend3->Check_Access_Right('DUTYSETUP-SPECIAL'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lexport = new libexporttext();

$type = $_REQUEST['type'];

if($type == 2)
{
	$filename = "special_duty_individual_import_sample.csv";
	$file_format = array("User Login","Date","Duty Type","Time Slot","Duty Start","Duty End","Duty Count","Waive In","Waive Out","Reason ID");
	if($sys_custom['StaffAttendance']['WongFutNamCollege']){
		$file_format = array_merge($file_format, array("Attend One Time Slot"));
	}
	$samples = array();
	$sample = array("teacher_t10","2014-09-01","D","Weekday_AM","08:20:00","12:45:00","0.5","N","Y","");
	if($sys_custom['StaffAttendance']['WongFutNamCollege']){
		$sample = array_merge($sample,array("1"));
	}
	$samples[] = $sample;
	$sample = array("teacher_t10","2014-09-01","D","Weekday_PM","13:45:00","17:30:00","0.5","Y","N","");
	if($sys_custom['StaffAttendance']['WongFutNamCollege']){
		$sample = array_merge($sample,array("1"));
	}
	$samples[] = $sample;
	$sample = array("teacher_t11","2014-09-02","L","Weekday","08:30:00","17:30:00","1","N","N","7");
	if($sys_custom['StaffAttendance']['WongFutNamCollege']){
		$sample = array_merge($sample,array("0"));
	}
	$samples[] = $sample;
	$sample = array("teacher_t12","2014-10-01","N","","","","","","","");
	if($sys_custom['StaffAttendance']['WongFutNamCollege']){
		$sample = array_merge($sample,array("0"));
	}
	$samples[] = $sample;
}else{
	$filename = "special_duty_group_import_sample.csv";
	$file_format = array("Group ID","User Logins","Date","Duty Type","Time Slot","Duty Start","Duty End","Duty Count","Waive In","Waive Out","Reason ID");
	if($sys_custom['StaffAttendance']['WongFutNamCollege']){
		$file_format = array_merge($file_format, array("Attend One Time Slot"));
	}
	$samples = array();
	$sample = array("1","teacher_t1,teacher_t2","2014-09-01","D","AM","08:10:00","12:45:00","0.5","N","Y","");
	if($sys_custom['StaffAttendance']['WongFutNamCollege']){
		$sample = array_merge($sample,array("1"));
	}
	$samples[] = $sample;
	$sample = array("1","teacher_t3,teacher_t4","2014-09-01","L","AM","08:10:00","12:45:00","0.5","N","Y","4");
	if($sys_custom['StaffAttendance']['WongFutNamCollege']){
		$sample = array_merge($sample,array("0"));
	}
	$samples[] = $sample;
	$sample = array("1","teacher_t5","2014-09-02","O","AM","08:10:00","12:45:00","0.5","N","Y","5");
	if($sys_custom['StaffAttendance']['WongFutNamCollege']){
		$sample = array_merge($sample,array("0"));
	}
	$samples[] = $sample;
	$sample = array("2","teacher_t6,teacher_t7,teacher_t8","2014-09-02","D","PM","13:30:00","18:00:00","0.5","N","N","");
	if($sys_custom['StaffAttendance']['WongFutNamCollege']){
		$sample = array_merge($sample,array("0"));
	}
	$samples[] = $sample;
	$sample = array("3","teacher_t9,teacher_t10","2014-10-01","N","","","","","","","");
	if($sys_custom['StaffAttendance']['WongFutNamCollege']){
		$sample = array_merge($sample,array("0"));
	}
	$samples[] = $sample;
}

$content = $lexport->GET_EXPORT_TXT($samples, $file_format, "", "\r\n", "", 0, "11");
$lexport->EXPORT_FILE($filename,$content);

intranet_closedb();
?>