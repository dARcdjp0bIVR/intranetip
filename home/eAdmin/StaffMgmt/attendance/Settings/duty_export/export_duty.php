<?php
// Editing by 
/*
 * 2016-03-15 (Carlos): Created for exporting duty to csv.
 */
set_time_limit(3600);
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend2.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!$StaffAttend3->Check_Access_Right('DUTYSETUP-SPECIAL'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lexport = new libexporttext();

$TargetID = IntegerSafe($_POST['ExportTargetID']);
$DutyType = intval($_POST['ExportDutyType']);
$StartDate = date("Y-m-d", strtotime($_POST['StartDate']));
$EndDate = date("Y-m-d", strtotime($_POST['EndDate']));

if($DutyType == 2){
	$filename = 'individual_duty.csv';
}else{
	$filename = 'group_duty.csv';
}

$rows = $StaffAttend3->Get_Export_Duty_Records($DutyType, $TargetID, $StartDate, $EndDate);
$header = array_shift($rows);

$content = $lexport->GET_EXPORT_TXT($rows, $header, "", "\r\n", "", 0, "11");
$lexport->EXPORT_FILE($filename,$content);

intranet_closedb();
?>