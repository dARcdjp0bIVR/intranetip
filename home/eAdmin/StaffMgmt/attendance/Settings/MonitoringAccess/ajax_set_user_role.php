<?php
// editing by 
/*
 * 	Log
 * 	2016-10-28 [Cameron]
 * 		- create this file
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

$StaffAttend3 = new libstaffattend3();

if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!$StaffAttend3->Check_Access_Right('SETTINGS-ACCESSRIGHT-Access'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	echo 'die';
	intranet_closedb();
	exit();
}

intranet_auth();
intranet_opendb();

$staffmonitoringright = new staff_monitoring_right();

$UserRole = $_REQUEST['UserRole'];
$GroupID = $_REQUEST['GroupID'];
$StaffID = $_REQUEST['StaffID'];

$staffmonitoringright ->Start_Trans();
if ($staffmonitoringright ->Set_UserRole($GroupID,$StaffID,$UserRole)) {
	$staffmonitoringright ->Commit_Trans();
}
else {
	$staffmonitoringright ->RollBack_Trans();
}

intranet_closedb();
?>