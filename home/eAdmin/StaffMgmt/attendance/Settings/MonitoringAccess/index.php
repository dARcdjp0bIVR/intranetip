<?php
// editing by 
/*
 * 	Log
 * 
 * 	2016-11-07 [Cameron]
 * 		- fix bug: highlight current page
 * 	
 * 	2016-10-28 [Cameron]
 * 		- create this file
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend2.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");


intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!$StaffAttend3->Check_Access_Right('SETTINGS-ACCESSRIGHT-Access'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$linterface = new interface_html();
$StaffAttend3UI = new libstaffattend3_ui();

$CurrentPageArr['eAdminStaffAttendance'] = 1;
$CurrentPage['FunctionAccess'] = 1;
$TAGS_OBJ = $StaffAttend3UI->get_access_rights_tabs("MonitoringRights");
$MODULE_OBJ = $StaffAttend3UI->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START(urldecode($_REQUEST['Msg']));

echo $StaffAttend3UI->Get_Monitoring_Access_Right_Settings_Index();

$linterface->LAYOUT_STOP();
intranet_closedb();
?>
<script type="text/JavaScript" language="JavaScript">
// dom function 
{
function Check_Go_Search(evt)
{
	var key = evt.which || evt.charCode || evt.keyCode;
	
	if (key == 13) // enter
		Get_Monitoring_Group_List();
	else
		return false;
}

}

// ajax function
{
function Get_Monitoring_Group_List()
{
	var PostVar = {
			Keyword: encodeURIComponent($('Input#Keyword').val())
			}

	Block_Element("MonitoringGroupListLayer");
	$.post('ajax_get_monitoring_group_list.php',PostVar,
					function(data){
						if (data == "die") 
							window.top.location = '/';
						else {
							$('div#MonitoringGroupListLayer').html(data);
							Thick_Box_Init();
							UnBlock_Element("MonitoringGroupListLayer");
						}
					});
}

function Delete_Group(GroupID)
{
	if (confirm('<?=$Lang['StaffAttendance']['AccessRightDeleteGroupWarning']?>')) {
		var PostVar = {
			"GroupID":GroupID
		}
		
		$.post('ajax_delete_group.php',PostVar,
			function(data) {
				if (data == "die") 
					window.top.location = '/';
				else {
					Get_Return_Message(data);
					Get_Monitoring_Group_List();
					Scroll_To_Top();
				}
			});
	}
}


}

// thick box function 
{
//on page load call tb_init
function Thick_Box_Init()
{   
	tb_init('a.thickbox');//pass where to apply thickbox
}
}
</script>