<?php
// editing by 
/*
 * 2015-10-05 (Carlos): Added symbol and color setting for status and reasons. 
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend2.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!$StaffAttend3->Check_Access_Right('SETTINGS-REASONTYPE-Access'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$linterface = new interface_html();
$StaffAttend3UI = new libstaffattend3_ui();

$CurrentPageArr['eAdminStaffAttendance'] = 1;
$CurrentPage['LeaveReason'] = 1;
$TAGS_OBJ[] = array($Lang['StaffAttendance']['ReasonTypeSettings'], "", 0);
$MODULE_OBJ = $StaffAttend3UI->GET_MODULE_OBJ_ARR();
//$TAGS_OBJ_RIGHT[] = array(gen_online_help_btn_and_layer('staff_attendance','reason'));
$linterface->LAYOUT_START(urldecode($_REQUEST['Msg']));

echo $StaffAttend3UI->Get_Leave_Reason_Index();

//$linterface->LAYOUT_STOP();
//intranet_closedb();
?>
<link href="/templates/jquery/jquery.colorPicker.css" rel="stylesheet" type="text/css">
<script type="text/JavaScript" language="JavaScript" src="/templates/jquery/jquery.colorPicker.js" ></script>
<script type="text/JavaScript" language="JavaScript">
// dom function 
function Check_Go_Search(evt)
{
	var key = evt.which || evt.charCode || evt.keyCode;
	
	if (key == 13) // enter
		Get_Leave_Reason_List();
	else
		return false;
}

// ajax function
function Get_Leave_Reason_List()
{
	var PostVar = {
			Active: Get_Selection_Value("ReasonActiveFilter","String"),
			Keyword: encodeURIComponent($('Input#Keyword').val())
			}

	Block_Element("LeaveReasonListLayer");
	$.post('ajax_get_leave_reason_list.php',PostVar,
					function(data){
						if (data == "die") 
							window.top.location = '/';
						else {
							$('div#LeaveReasonListLayer').html(data);
							Thick_Box_Init();
							UnBlock_Element("LeaveReasonListLayer");
						}
					});
}
/*
function Sort_Page(FieldName, InOrder)
{
	var PostVar = {
			"Keyword": encodeURIComponent($('Input#Keyword').val()),
			"SortField":FieldName,
			"InOrder":InOrder
			}

	Block_Element("LeaveReasonListLayer");
	$.post('ajax_get_leave_reason_list.php',PostVar,
					function(data){
						if (data == "die") 
							window.top.location = '/';
						else {
							$('div#LeaveReasonListLayer').html(data);
							Thick_Box_Init();
							UnBlock_Element("LeaveReasonListLayer");
						}
					});
}
*/
function Get_Leave_Reason_Form(TypeID, ReasonID)
{
	var ReasonID = ReasonID || "";
	var PostVar = {
		"TypeID": TypeID,
		"ReasonID": ReasonID
	};
	
	$.post('ajax_get_leave_reason_form.php',PostVar,
		function(data) {
			if (data == "die") 
				window.top.location = '/';
			else{ 
				$('div#TB_ajaxContent').html(data);
				$('#ReasonColor').colorPicker();
			}
		});
}

function Save_Preset_Leave_Reason() {
	var PostVar = {
			"ReasonType": $('Input#TypeID').val(),
			"ReasonID": $('Input#ReasonID').val(),
			"ReasonText": encodeURIComponent($('Input#ReasonText').val()),
			"ReasonDescription": encodeURIComponent($('Input#ReasonDescription').val()),
			"ReportSymbol": encodeURIComponent($('Input#ReportSymbol').val()),
			"Waived": $('Input#WaivedY').val(),
			"ReasonActive": $('Input#ReasonActiveY').val(),
			"SetAsDefault": ((document.getElementById('SetAsDefault').checked && $('Input#ReasonActiveY').val() == "1")? "1":"0"),
			"ReasonColor": $('#ReasonColor').val() 
			}
	
	Block_Thickbox();
	$.post('ajax_save_leave_reason.php',PostVar,
		function(data){
			if (data == "die") 
				window.top.location = '/';
			else {
				Get_Return_Message(data);
				Get_Leave_Reason_List();
				window.top.tb_remove();
				UnBlock_Thickbox();
			}
		});
}

function Delete_Preset_Leave_Reason(ReasonID)
{
	if (confirm('<?=$Lang['StaffAttendance']['ConfirmDeleteReason']?>')) {
		var PostVar = {
			"ReasonID":ReasonID
		}
		
		$.post('ajax_delete_leave_reason.php',PostVar,
			function(data) {
				if (data == "die") 
					window.top.location = '/';
				else {
					Get_Return_Message(data);
					Get_Leave_Reason_List();
					Scroll_To_Top();
				}
			});
	}
}

function Check_Leave_Reason()
{
	var ReasonID = $('Input#ReasonID').val() || "";
	var TypeID = $('Input#TypeID').val() || "";
	var PostVar = {
			"TypeID": TypeID,
			"ReasonID": ReasonID,
			"ReasonText": encodeURIComponent($('Input#ReasonText').val()),
			"ReportSymbol": encodeURIComponent($('Input#ReportSymbol').val())
			}
	var ElementObj = $('div#ReasonTextWarningLayer');
	
	if (Trim($('Input#ReasonText').val()) != "") {
		$.post('ajax_check_leave_reason.php',PostVar,
				function(data){
					if (data == "die") 
						window.top.location = '/';
					else if (data == "1") {
						ElementObj.html('');
						ElementObj.hide();
						if (document.getElementById('ReasonTextWarningRow')) 
							document.getElementById('ReasonTextWarningRow').style.display = 'none';
						Save_Preset_Leave_Reason();
					}
					else {
						ElementObj.html('<?=$Lang['StaffAttendance']['ReasonReportSymbolExists']?>');
						ElementObj.show('fast');
						if (document.getElementById('ReasonTextWarningRow')) 
							document.getElementById('ReasonTextWarningRow').style.display = '';
					}
				});
	}
	else {
		CheckRoleNameAjax = "";
		
		ElementObj.html('<?=$Lang['StaffAttendance']['EmptyReason']?>');
		ElementObj.show('fast');
		
		if (document.getElementById('ReasonTextWarningRow')) 
			document.getElementById('ReasonTextWarningRow').style.display = '';
	}
}

function Get_Status_Setting_Form(StatusType)
{
	$.post(
		'ajax_get_status_setting_form.php',
		{
			'StatusType':StatusType
		},
		function(data){
			if (data == "die") 
				window.top.location = '/';
			else{ 
				$('div#TB_ajaxContent').html(data);
				$('#StatusColor').colorPicker();
			}
		}
	);
}
/*
function Check_Status_Setting()
{
	var symbol = $.trim($('input#StatusSymbol').val());
	$('#SymbolWarningRow').hide();
	if(symbol != '')
	{
		Block_Thickbox();
		$.post(
			'ajax_check_status_symbol.php',
			{
				"StatusSymbol": encodeURIComponent(symbol),
				"StatusType": $('#StatusType').val() 
			},
			function(data)
			{
				if (data == "die") 
					window.top.location = '/';
				else if(data == "1"){
					Update_Status_Setting();
				}else{
					$('#SymbolWarningLayer').html('<?=$Lang['StaffAttendance']['ReasonReportSymbolExists']?>');
					$('#SymbolWarningRow').show();
					UnBlock_Thickbox();
				}
			}
		);
	}else{
		Block_Thickbox();
		Update_Status_Setting();
	}
}
*/
function Update_Status_Setting()
{
	var PostVar = {
			"StatusType": $('input#StatusType').val(),
			"StatusSymbol": encodeURIComponent($.trim($('input#StatusSymbol').val())),
			"StatusColor": $('#StatusColor').val() 
	};
	
	$.post('ajax_update_status_setting.php',PostVar,
		function(data){
			if (data == "die") 
				window.top.location = '/';
			else {
				Get_Return_Message(data);
				Get_Leave_Reason_List();
				window.top.tb_remove();
				UnBlock_Thickbox();
			}
		}
	);
}

// thick box function 
//on page load call tb_init
function Thick_Box_Init()
{   
	tb_init('a.thickbox');//pass where to apply thickbox
}

$(document).ready(function(){
	$.fn.colorPicker.defaultColors = [ '000000', '993300','333300', '000080', '333399', '333333', '800000', 'FF6600', '808000', '008000', '008080', '0000FF', '666699', '808080', 'FF0000', 'FF9900', '99CC00', '339966', '33CCCC', '3366FF', '800080', '999999', 'FF00FF', 'FFCC00', 'FFFF00', '00FF00', '00FFFF', '00CCFF', '993366', 'C0C0C0', 'FF99CC', 'FFCC99', 'FFFF99' , 'CCFFFF', '99CCFF', 'FFFFFF'];
});
</script>
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>