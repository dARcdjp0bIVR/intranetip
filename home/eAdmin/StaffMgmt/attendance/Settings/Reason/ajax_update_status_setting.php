<?php
//editing by 
/*
 * 2015-10-05 (Carlos): Created
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

$StaffAttend3 = new libstaffattend3();
if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!$StaffAttend3->Check_Access_Right('SETTINGS-REASONTYPE-Access'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	echo 'die';
	intranet_closedb();
	exit();
}

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();

$StatusType = $_REQUEST['StatusType'];
$StatusSymbol = trim(urldecode(stripslashes($_REQUEST['StatusSymbol'])));
$StatusColor = trim($_REQUEST['StatusColor']);

if ($StaffAttend3->Update_Attendance_Status_Setting($StatusType, $StatusSymbol, $StatusColor)) {
	echo $Lang['StaffAttendance']['SaveReasonSuccess'];
}
else {
	echo $Lang['StaffAttendance']['SaveReasonFail'];
}

intranet_closedb();
?>