<?
// editing by 
/*
 * 2014-02-20 (Carlos): save settings and setup the time to run cron job with crontab
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend2.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!$sys_custom['StaffAttendance']['DailyAttendanceRecord'])
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

function update_crontab($hour, $minute)
{
	$hour = sprintf("%02d",$hour);
	$minute = sprintf("%02d",$minute);
	
	$ip25_site = (checkHttpsWebProtocol()?"https":"http")."://".$_SERVER['SERVER_NAME'].($_SERVER["SERVER_PORT"]!= 80?":".$_SERVER["SERVER_PORT"]:"");
	
	$crontab_content = shell_exec("crontab -l");
	$jobs = explode("\n",$crontab_content);
	$job_cmd = "$minute $hour * * * /usr/bin/wget -q '".$ip25_site."/schedule_task/staff_attendance_mail_leave_records.php'";
	
	$crontab_content = "";
	for($i=0;$i<sizeof($jobs);$i++){
		$job = trim($jobs[$i]);
		if($job != ""){
			if(strstr($job,"/schedule_task/staff_attendance_mail_leave_records.php") === FALSE){
				$crontab_content .= $job."\n";
			}
		}
	}
	if($_REQUEST['CustomizedDisableSendEmail'] != 1){
		$crontab_content .= $job_cmd."\n";
	}
	
	$tmp_file = "/tmp/crontab.txt";
	$fh = fopen($tmp_file,"w");
	if($fh){
		fputs($fh, $crontab_content);
		fclose($fh);
	}
	
	if(file_exists($tmp_file)){
		shell_exec("crontab $tmp_file");
		unlink($tmp_file);
	}
}

$GeneralSetting = new libgeneralsettings();
$SettingList = array();

$Settings = $StaffAttend3->Get_Customized_Settings();
$SettingNames = array_keys($Settings);
for($i=0;$i<count($SettingNames);$i++){
	if($SettingNames[$i] == 'CustomizedEmailSendTime'){
		$SettingList['CustomizedEmailSendTime'] = sprintf("%02d:%02d:%02d",$_REQUEST['CustomizedEmailSendTime_hour'],$_REQUEST['CustomizedEmailSendTime_min'],$_REQUEST['CustomizedEmailSendTime_sec']);
	}else{
		$SettingList[$SettingNames[$i]] = stripslashes($_REQUEST[$SettingNames[$i]]);
	}
}

$GeneralSetting->Start_Trans();
if ($GeneralSetting->Save_General_Setting('StaffAttendance',$SettingList)) {
	$GeneralSetting->Commit_Trans();
	
	update_crontab($_REQUEST['CustomizedEmailSendTime_hour'],$_REQUEST['CustomizedEmailSendTime_min']);
	
	$Msg = $Lang['StaffAttendance']['SettingApplySuccess'];
}
else {
	$GeneralSetting->RollBack_Trans();
	$Msg = $Lang['StaffAttendance']['SettingApplyFail'];
}

header("Location: index.php?Msg=".urlencode($Msg));
?>