<?php
$PATH_WRT_ROOT = "../../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if ((!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StaffAttendance"] && 
		!$StaffAttend3->Check_Access_Right('DUTYSETUP-Roster'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	echo 'die';
	intranet_closedb();
	exit();
}

$StaffAttend3UI = new libstaffattend3_ui();

$GroupID = $_REQUEST['GroupID'];
$GroupSlotPeriodID = $_REQUEST['GroupSlotPeriodID'];

echo $StaffAttend3UI->Get_Normal_Group_Duty_Period_Detail($GroupID,$GroupSlotPeriodID);

intranet_closedb();
?>