<?php
$PATH_WRT_ROOT = "../../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if ((!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StaffAttendance"] && 
		!$StaffAttend3->Check_Access_Right('DUTYSETUP-Roster'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	echo 'die';
	intranet_closedb();
	exit();
}

$StaffAttend3 = new libstaffattend3();

$GroupID = $_REQUEST['GroupID'];
$GroupSlotPeriodID = $_REQUEST['GroupSlotPeriodID'];
$EffectiveStart = stripslashes(trim(urldecode($_REQUEST['EffectiveStart'])));
$EffectiveEnd = stripslashes(trim(urldecode($_REQUEST['EffectiveEnd'])));

if ($StaffAttend3->Check_Duty_Period($GroupID,$GroupSlotPeriodID,$EffectiveStart,$EffectiveEnd)) {
	echo "1"; // ok
}
else {
	echo "0"; // not ok
}

intranet_closedb();
?>