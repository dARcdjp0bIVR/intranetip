<?php
$PATH_WRT_ROOT = "../../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if ((!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StaffAttendance"] && 
		!$StaffAttend3->Check_Access_Right('DUTYSETUP-Roster'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	echo 'die';
	intranet_closedb();
	exit();
}

$StaffAttend3 = new libstaffattend3();

$GroupID = $_REQUEST['GroupID'];
$GroupSlotPeriodID = $_REQUEST['GroupSlotPeriodID'];
$DayOfWeek = $_REQUEST['DayOfWeek'];
$TimeSlotID = $_REQUEST['TimeSlotID'];
for ($i=0; $i< sizeof($TimeSlotID); $i++) {
	$TimeSlotUser[$TimeSlotID[$i]] = $_REQUEST[$TimeSlotID[$i]];
}

$StaffAttend3->Start_Trans();
if ($StaffAttend3->Save_Duty_Period_User_Time_Slot($GroupID,$GroupSlotPeriodID,$DayOfWeek,$TimeSlotID,$TimeSlotUser)) {
	echo $Lang['StaffAttendance']['DutyPeriodDutySaveSuccess'];
	$StaffAttend3->Commit_Trans();
}
else {
	echo $Lang['StaffAttendance']['DutyPeriodDutySaveFail'];
	$StaffAttend3->RollBack_Trans();
}

intranet_closedb();
?>