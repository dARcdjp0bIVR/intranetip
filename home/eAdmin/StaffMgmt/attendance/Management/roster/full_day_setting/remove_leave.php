<?php
// Editing by 
/*
 * 2018-02-07 Carlos: Created.
 */
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend2.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!$StaffAttend3->Check_Access_Right('MGMT-LEAVERECORD'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	intranet_closedb();
	header ("Location: /");
	exit();
}

if(isset($_SESSION['STAFF_ATTENDANCE_TEMPORARY_DATA'])){
	unset($_SESSION['STAFF_ATTENDANCE_TEMPORARY_DATA']);
}
$_SESSION['STAFF_ATTENDANCE_TEMPORARY_DATA'] = array();
if(count($_POST)>0){
	foreach($_POST as $key => $val){
		$_SESSION['STAFF_ATTENDANCE_TEMPORARY_DATA'][$key] = $val;
	}
}

$sql = "DELETE FROM CARD_STAFF_ATTENDANCE2_LEAVE_RECORD WHERE RecordID IN (".implode(",",(array)$RecordID).") ";
$delete_success = $StaffAttend3->db_db_query($sql);

$msg = $delete_success? $Lang['General']['ReturnMessage']['DeleteSuccess']:$Lang['General']['ReturnMessage']['DeleteUnsuccess'];
$_SESSION['STAFF_ATTENDANCE_LEAVE_RECORD_RESULT_MSG'] = $msg;

intranet_closedb();

header("Location:leave_list.php");
?>
