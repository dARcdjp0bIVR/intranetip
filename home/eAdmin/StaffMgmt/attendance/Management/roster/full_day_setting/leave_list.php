<?php
// Editing by  
/*
 * 2018-02-07 Carlos: Created.
 */
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend2.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!$StaffAttend3->Check_Access_Right('MGMT-LEAVERECORD'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	intranet_closedb();
	header ("Location: /");
	exit();
}

$linterface = new interface_html();
$StaffAttend3UI = new libstaffattend3_ui();

$can_manage = $StaffAttend3->IS_ADMIN_USER() || $StaffAttend3->Check_Access_Right('MGMT-LEAVERECORD-MANAGE');

# set cookies
$arrCookies[] = array("ck_staff_attendance_page_size", "numPerPage");
$arrCookies[] = array("ck_staff_attendance_page_no", "pageNo");
$arrCookies[] = array("ck_staff_attendance_page_order", "order");
$arrCookies[] = array("ck_staff_attendance_page_field", "field");

$arrCookies[] = array("ck_staff_attendance_startdate", "StartDate");
$arrCookies[] = array("ck_staff_attendance_keyword", "Keyword");
if(isset($clearCoo) && $clearCoo == 1)
{
	clearCookies($arrCookies);
} 
else 
{
	updateGetCookies($arrCookies);
}

if(isset($_SESSION['STAFF_ATTENDANCE_TEMPORARY_DATA'])){
	$temp_data = (array)$_SESSION['STAFF_ATTENDANCE_TEMPORARY_DATA'];
	if(count($temp_data)>0){
		foreach($temp_data as $key => $val){
			$$key = $val;
		}
	}
	unset($_SESSION['STAFF_ATTENDANCE_TEMPORARY_DATA']);
}

if ($page_size_change == 1)
{
    setcookie("ck_staff_attendance_page_size", $numPerPage, 0, "", "", 0);
    $ck_staff_attendance_page_size = $numPerPage;
}
if($order == "") $order = 0;
if($field == "") $field = 0;
if (isset($ck_staff_attendance_page_size) && $ck_staff_attendance_page_size != "") $page_size = $ck_staff_attendance_page_size;
$pageSizeChangeEnabled = true;

$today = date("Y-m-d");

if($StartDate == '' || !preg_match('/^\d\d\d\d-\d\d-\d\d$/',$StartDate)){
	$StartDate = date('Y-m-d');
}

if($RecordType != '' && !in_array($RecordType,array(CARD_STATUS_HOLIDAY,CARD_STATUS_OUTGOING))){
	$RecordType = ''; 
}

$record_types = array();
$record_types[] = array('',$Lang['General']['All']);
$record_types[] = array(CARD_STATUS_HOLIDAY,$Lang['StaffAttendance']['Leave']);
$record_types[] = array(CARD_STATUS_OUTGOING,$Lang['StaffAttendance']['Outgoing']);
$record_type_selection = $StaffAttend3UI->GET_SELECTION_BOX($record_types, ' id="RecordType" name="RecordType" onchange="document.form1.submit(); " ', '', $RecordType);

if($StaffAttend3->isMagicQuotesOn()){
	$Keyword = stripslashes($Keyword);
}
$Keyword = trim($Keyword);


$li = new libdbtable2007($field, $order, $pageNo);

$field_array = array("lr.RecordDate","StaffName","LeaveType","SlotName","Reason","Remark",'lr.DateInput','ModifiedByUser');

$pos = 0;
$column_list = array();
$column_list[] = "<th width=\"1\" class=\"num_check\">#</th>\n";
$column_list[] = "<th width='12%'>".$li->column_IP25($pos++, $Lang['StaffAttendance']['RecordDate'])."</th>\n";
$column_list[] = "<th width='14%'>".$li->column_IP25($pos++, $Lang['StaffAttendance']['StaffName'])."</th>\n";
$column_list[] = "<th width='5%'>".$li->column_IP25($pos++, $Lang['StaffAttendance']['SettingType'])."</th>\n";
$column_list[] = "<th width='14%'>".$li->column_IP25($pos++, $Lang['StaffAttendance']['TimeSlot'])."</th>\n";
$column_list[] = "<th width='13%'>".$li->column_IP25($pos++, $Lang['StaffAttendance']['Reason'])."</th>\n";
$column_list[] = "<th width='13%'>".$li->column_IP25($pos++, $Lang['StaffAttendance']['Remark'])."</th>\n";
$column_list[] = "<th width='14%'>".$li->column_IP25($pos++, $Lang['General']['DateInput'])."</th>\n";
$column_list[] = "<th width='14%'>".$li->column_IP25($pos++, $Lang['General']['LastModifiedBy'])."</th>\n";
$column_list[] = "<th width='1'>".$li->check("RecordID[]")."</th>\n";

$column_array = array(22,22,22,22,22,0,22,22);
$wrap_array = array_fill(0,count($field_array),0);

$name_field = $StaffAttend3->Get_Name_Field("u.");
$modify_name_field = $StaffAttend3->Get_Name_Field("u2.");

$sql = "SELECT 
			lr.RecordDate,
			$name_field as StaffName,
			IF(lr.RecordType='".CARD_STATUS_HOLIDAY."','".$Lang['StaffAttendance']['Leave']."','".$Lang['StaffAttendance']['Outgoing']."') as LeaveType,
			IF(lr.SlotID IS NULL,'Full Day',CONCAT(s.SlotName,'(',s.SlotStart,'-',s.SlotEnd,')')) as SlotName,
			IF(r.ReasonID IS NOT NULL,r.ReasonText,'') as Reason,
			lr.Remark as Remark,
			lr.DateInput,
			$modify_name_field as ModifiedByUser,
			IF(lr.RecordDate<='".$StaffAttend3->Get_Safe_Sql_Query($today)."','&nbsp;',CONCAT('<input type=\"checkbox\" name=\"RecordID[]\" value=\"',lr.RecordID,'\" />')) as Checkbox  
		FROM CARD_STAFF_ATTENDANCE2_LEAVE_RECORD as lr 
		INNER JOIN INTRANET_USER as u ON u.UserID=lr.StaffID 
		LEFT JOIN CARD_STAFF_ATTENDANCE3_SLOT as s ON s.SlotID=lr.SlotID 
		LEFT JOIN CARD_STAFF_ATTENDANCE3_REASON as r ON r.ReasonID=lr.ReasonID 
		LEFT JOIN INTRANET_USER as u2 ON u2.UserID=lr.ModifyBy 
		WHERE lr.RecordDate >= '".$StaffAttend3->Get_Safe_Sql_Query($StartDate)."' ";
if($RecordType != ''){
	$sql .= " AND lr.RecordType='".$StaffAttend3->Get_Safe_Sql_Query($RecordType)."' ";
}
if($Keyword != ''){
	$safe_keyword = $li->Get_Safe_Sql_Like_Query($Keyword);
	$sql .= " AND (u.EnglishName LIKE '%$safe_keyword%' OR u.ChineseName LIKE '%$safe_keyword%') ";
}

$li->sql = $sql;
$li->field_array = $field_array;
$li->no_col = sizeof($li->field_array)+2;
$li->title = "";
$li->column_array = $column_array;
$li->wrap_array = $wrap_array;
$li->IsColOff = "IP25_table";
$li->column_list = implode("",$column_list);

if($can_manage){
	$tool_buttons = array();
	$tool_buttons[] = array('edit','javascript:checkEdit(document.form1,\'RecordID[]\',\'edit_leave.php\');',$Lang['Btn']['Edit']);
	$tool_buttons[] = array('delete','javascript:checkRemove(document.form1,\'RecordID[]\',\'remove_leave.php\');',$Lang['Btn']['Delete']);
	$tool_buttons_html = $StaffAttend3UI->Get_DBTable_Action_Button_IP25($tool_buttons);
}

$CurrentPageArr['eAdminStaffAttendance'] = 1;
$CurrentPage['LeaveRequest'] = 1;
//$TAGS_OBJ[] = array($Lang['StaffAttendance']['FullDaySetting'], "", 1);
$TAGS_OBJ = $StaffAttend3UI->Get_Management_LeaveRecord_Navigation_Tabs('LeaveRecordByDate');
$MODULE_OBJ = $StaffAttend3UI->GET_MODULE_OBJ_ARR();
//$TAGS_OBJ_RIGHT[] = array(gen_online_help_btn_and_layer('staff_attendance','leave_record'));
if(isset($_SESSION['STAFF_ATTENDANCE_LEAVE_RECORD_RESULT_MSG'])){
	$msg = $_SESSION['STAFF_ATTENDANCE_LEAVE_RECORD_RESULT_MSG'];
	unset($_SESSION['STAFF_ATTENDANCE_LEAVE_RECORD_RESULT_MSG']);
}
$linterface->LAYOUT_START($msg);
echo $StaffAttend3UI->Include_JS_CSS();
?>
<br/>
<form name="form1" id="form1" method="post" action="">
<div class="content_top_tool" style="width:100%;">
<?php 
if ($can_manage) {
	$x .= '<div style="float:left;">';
	$x .= $StaffAttend3UI->GET_LNK_NEW('javascript:NewLeaveRecord();', $button_text="", $ParOnClick="", $ParOthers="", $ParClass="", 0);
	$x .= '</div>';
	echo $x;
} 
?>
  <div class="Conntent_search">
    <input name="Keyword" id="Keyword" type="text" value="<?php echo intranet_htmlspecialchars($Keyword)?>" onkeyup="Check_Go_Search(event);"/>
  </div>
  <br style="clear:both" />
</div>
<div class="table_board" id="Container" style="width:100%;">
<table width="100%" cellspacing="0" cellpadding="0" border="0">
	<tbody>
	<tr>
		<td valign="bottom">
		<div class="table_filter">
		  <table border="0">
		  <tr>
			  <td>
			  <?php echo $Lang['General']['StartDate'].'&nbsp;'.$StaffAttend3UI->GET_DATE_PICKER("StartDate", $StartDate,'onchange="CheckDateAndSubmit();"',"","","","","CheckDateAndSubmit();")?>
			  <span style="color:red;" id="DateWarningLayer"></span>
			  </td>
			  <td>
			  <?php echo $record_type_selection?>
			  </td>
		 </tr>
		 </table>
		</div>
		</td>
	</tr>
	<tr>
		<td><?php echo $tool_buttons_html?></td>
	</tr>
	<tr>
		<td><?php echo $li->display()?></td>
	</tr>
	</tbody>
</table>
<p class="tabletextremark" style="text-align:left;"><?php echo $Lang['StaffAttendance']['LeaveRecordCannotEditPastRecordRemark'];?></p>
<br style="clear:both" />
<input type="hidden" name="pageNo" id="pageNo" value="<?php echo $li->pageNo; ?>" />
<input type="hidden" name="order" id="order" value="<?php echo $li->order; ?>" />
<input type="hidden" name="field" id="field" value="<?php echo $li->field; ?>" />
<input type="hidden" name="page_size_change" id="page_size_change" value="" />
<input type="hidden" name="numPerPage" id="numPerPage" value="<?php echo $li->page_size?>" />
</div>
</form>
<br/>
<br/>
<script type="text/javascript" language="javascript">
function Check_Go_Search(evt)
{
	var key = evt.which || evt.charCode || evt.keyCode;
	if (key == 13){ // enter
		document.form1.submit();
	}else{
		return false;
	}
}

function CheckDateAndSubmit()
{
	document.form1.submit();
}

function NewLeaveRecord()
{
	var checkboxes = document.getElementsByName('RecordID[]');
	for(var i=0;i<checkboxes.length;i++){
		checkboxes[i].checked = false;
	}
	document.form1.action = 'edit_leave.php';
	document.form1.submit();
}
</script>
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>