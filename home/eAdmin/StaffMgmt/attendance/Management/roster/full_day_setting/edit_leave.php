<?php
// Editing by 
/*
 * 2018-02-07 Carlos: Created.
 */
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend2.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!$StaffAttend3->Check_Access_Right('MGMT-LEAVERECORD'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	intranet_closedb();
	header ("Location: /");
	exit();
}

$linterface = new interface_html();
$StaffAttend3UI = new libstaffattend3_ui();

$is_edit = isset($RecordID) && $RecordID > 0;


if($StaffAttend3->isMagicQuotesOn()){
	$Keyword = stripslashes($Keyword);
}
$Keyword = trim($Keyword);

if($is_edit){
	$records = $StaffAttend3->Get_FullDay_Setting_Records(array('RecordID'=>$RecordID));
	$staff_ids = Get_Array_by_Key($records,'StaffID');
	$RecordDate = $records[0]['RecordDate'];
}else{
	$records = array();
	$staff_ids = array();
	$RecordDate = date("Y-m-d",time()+86400);
}


$navigation_ary = array();
$navigation_ary[] = array($Lang['StaffAttendance']['FullDaySetting'],'javascript:GoBack();');
$navigation_ary[] = array($is_edit? $Lang['Btn']['Edit'] : $Lang['Btn']['New'],'');

$CurrentPageArr['eAdminStaffAttendance'] = 1;
$CurrentPage['LeaveRequest'] = 1;
//$TAGS_OBJ[] = array($Lang['StaffAttendance']['FullDaySetting'], "", 1);
$TAGS_OBJ = $StaffAttend3UI->Get_Management_LeaveRecord_Navigation_Tabs('LeaveRecordByDate');
$MODULE_OBJ = $StaffAttend3UI->GET_MODULE_OBJ_ARR();
//$TAGS_OBJ_RIGHT[] = array(gen_online_help_btn_and_layer('staff_attendance','leave_record'));
if(isset($_SESSION['STAFF_ATTENDANCE_LEAVE_RECORD_RESULT_MSG'])){
	$msg = $_SESSION['STAFF_ATTENDANCE_LEAVE_RECORD_RESULT_MSG'];
	unset($_SESSION['STAFF_ATTENDANCE_LEAVE_RECORD_RESULT_MSG']);
}
$linterface->LAYOUT_START($msg);
echo $StaffAttend3UI->Include_JS_CSS();
?>
<br/>
<?php echo $linterface->GET_NAVIGATION_IP25($navigation_ary)?>
<form name="form1" id="form1" method="post" action="edit_leave_update.php">
<?php echo $StaffAttend3UI->Get_Warning_Message_Box($Lang['General']['Instruction'],$Lang['StaffAttendance']['LeaveRecordByDateInstruction'],' style="padding:1em;font-weight:bold;" ');?>
<table width="100%" cellpadding="2" class="form_table_v30">
	<col class="field_title">
	<col class="field_c">
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="field_title">
			<?php echo $Lang['StaffAttendance']['Staff']?>
		</td>
		<td width="70%">
			<?php echo $StaffAttend3UI->Get_Staff_Selection('TargetStaffID[]','TargetStaffID[]',$ParGroup=true,$ParIndividual=true,$ParIsMultiple=true,$ParSize=20,$ParSelectedValue=$staff_ids,$ParOthers="",$ParAllStaffOption=false,$ShowOptGroupLabel=true)?>
			<?php echo $StaffAttend3UI->MultiSelectionRemark()?>
		</td>
	</tr>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="field_title">
			<?php echo $Lang['StaffAttendance']['TargetDate']?>
		</td>
		<td width="70%">
			<?php echo $StaffAttend3UI->GET_DATE_PICKER('RecordDate',$RecordDate)?>
		</td>
	</tr>
</table>
<input type="hidden" name="pageNo" id="pageNo" value="<?php echo intranet_htmlspecialchars($pageNo)?>" />
<input type="hidden" name="order" id="order" value="<?php echo intranet_htmlspecialchars($order)?>" />
<input type="hidden" name="field" id="field" value="<?php echo intranet_htmlspecialchars($field)?>" />
<input type="hidden" name="page_size_change" id="page_size_change" value="" />
<input type="hidden" name="numPerPage" id="numPerPage" value="<?php echo intranet_htmlspecialchars($numPerPage)?>" />
<input type="hidden" name="StartDate" id="StartDate" value="<?php echo intranet_htmlspecialchars($StartDate)?>" />
<input type="hidden" name="RecordType" id="RecordType" value="<?php echo intranet_htmlspecialchars($RecordType)?>" />
<input type="hidden" name="Keyword" id="Keyword" value="<?php echo str_replace('"','&quot;',$Keyword)?>" />
<div style="text-align:center;"><?php echo $linterface->GET_SMALL_BTN($Lang['Btn']['Apply'], 'button', $ParOnClick="ApplyForm();", 'applyBtn', $ParOtherAttribute="");?></div>
</form>
<br />
<div id="Container"></div>
<script type="text/javascript" language="javascript">
function GoBack()
{
	document.form1.action = 'leave_list.php';
	document.form1.submit();
}

function CheckSubmitForm(formObj)
{
	Block_Document();
	var staffIdObjs = document.getElementsByName('StaffID[]');
	var staffIds = [];
	var update_results = [];
	for(var i=0;i<staffIdObjs.length;i++){
		staffIds.push(staffIdObjs[i].value);
	}
	var update_func = function(){
		if(staffIds.length > 0){
			var staffId = staffIds.shift();
			var outgoing_cbs = document.getElementsByName(staffId+'-Outgoing[]');
			var holiday_cbs = document.getElementsByName(staffId+'-Holiday[]');
			for(var i=0;i<outgoing_cbs.length;i++){
				outgoing_cbs[i].checked = true;
			}
			for(var i=0;i<holiday_cbs.length;i++){
				holiday_cbs[i].checked = true;
			}
			var formData = $('#'+staffId+'-EditForm').serialize();
			$.post(
				'save_leave_setting.php',
				formData,
				function(returnResult){
					update_results.push(returnResult);
					setTimeout(function(){
						update_func();
					},100);
				}
			);
		}else{
			var msg = update_results.indexOf('0') == -1? '<?php echo $Lang['StaffAttendance']['FullDaySettingSaveSuccess'];?>' : '<?php echo $Lang['StaffAttendance']['FullDaySettingSaveFail'];?>';
			Get_Return_Message(msg);
			Scroll_To_Top();
			UnBlock_Document();
			setTimeout(function(){GoBack();},3000);
		}
	}

	setTimeout(function(){
		update_func();
	},100);
}

function ApplyForm()
{
	var selected_staff_ids = $('#TargetStaffID\\[\\]').val();
	var record_date = $('#RecordDate').val();
	$('#Container').html('<?php echo $StaffAttend3UI->Get_Ajax_Loading_Image();?>');
	LoadEditForm(selected_staff_ids,record_date);
}

function LoadEditForm(StaffID,RecordDate)
{
	$.post(
		'get_leave_setting_form.php',
		{
			'StaffID[]':StaffID,
			'RecordDate':RecordDate
		},
		function(returnHtml){
			$('#Container').html('');
			$('#Container').append(returnHtml);
			$('input.hasDatepick').datepick('option','minDate',new Date((new Date()).getTime() + 24 * 60 * 60 * 1000));
			for(var i=0;i<StaffID.length;i++){
				Drag_And_Drop_Init(StaffID[i]);
			}
		}
	);
}

function Switch_To_Full_Day(StaffID) {
	$('div#'+StaffID+'-TimeSlotLayer').hide();
	$('div#'+StaffID+'-FullDaySettingLayer').show();
}

function Switch_To_Time_Slot(StaffID) {
	$('div#'+StaffID+'-FullDaySettingLayer').hide();
	$('div#'+StaffID+'-TimeSlotLayer').show();
}

function Get_Reason_List(StaffID,ReasonType) 
{
	$('#'+StaffID+'-FullDaySettingLayer .ReasonSelection').html('');
	$.post(
		'ajax_get_reason_selection.php',
		{
			'ReasonType':ReasonType,
			'Id':StaffID+'-ReasonID',
			'Name':StaffID+'-ReasonID' 
		},
		function(html) {
			$('#'+StaffID+'-FullDaySettingLayer .ReasonSelection').html(html);
		}
	);
}

function Change_Background_Color(CheckBoxID,DivObj)
{
	var CheckboxObj = document.getElementById(CheckBoxID);

	if (CheckboxObj.checked) {
		//alert('-');
		CheckboxObj.checked = false;
		DivObj.style.backgroundColor = '';
	}
	else {
		//alert('+');
		CheckboxObj.checked = true;
		DivObj.style.backgroundColor = '#A6A6A6';
	}		
}

function Show_Reason_Layer(Obj)
{
	if (document.all) {
		Obj.nextSibling.style.visibility = "visible";
		Obj.nextSibling.firstChild.focus();
	}
	else {
		Obj.nextSibling.nextSibling.style.visibility = "visible";
		Obj.nextSibling.nextSibling.firstChild.nextSibling.focus();
	}
}
	
function Check_Reason_Icon(SelectObj)
{
	if (SelectObj.options[SelectObj.selectedIndex].value == "") {
		if (document.all)
			SelectObj.parentNode.previousSibling.className = "reason_alert";
		else 
			SelectObj.parentNode.previousSibling.previousSibling.className = "reason_alert";
	}
	else {
		if (document.all)
			SelectObj.parentNode.previousSibling.className = "reason";
		else 
			SelectObj.parentNode.previousSibling.previousSibling.className = "reason";
	}
	
	var ObjID = SelectObj.id.split('-');
	document.getElementById(ObjID[0]+'-'+ObjID[1]+'-'+ObjID[2]+'-User-Reason').value = SelectObj.options[SelectObj.selectedIndex].value;
	SelectObj.parentNode.style.visibility = "hidden";
}

function Get_Slot_Content_Html(StaffID,TimeSlotID,ColumnNature)
{
	var Content = '';
	var SlotName = document.getElementById(StaffID+'-SlotName['+TimeSlotID+']').value;
	
	if (ColumnNature == "Outgoing" || ColumnNature == "Holiday") {
		Content += '<table style="background: transparent; padding:0px; border:0px; width:100%; padding:0px; spacing:0px; cursor: pointer;">';
		Content += '<tr>';
		Content += '<td style="background: transparent; padding:0px; border:0px; padding:0px; spacing:0px">';
	}
	Content += '<div onclick="Change_Background_Color(\''+StaffID+'-'+TimeSlotID+'-'+ColumnNature+'\',this);" style="cursor: pointer;">';
	Content += '<input style="display:none;" name="'+StaffID+'-'+ColumnNature+'[]" id="'+StaffID+'-'+TimeSlotID+'-'+ColumnNature+'" type="checkbox" value="'+TimeSlotID+'"/>';
  Content += '<font class="staff_name" style="color:#000000;">'+SlotName+'</font>';
  Content += '</div>';
  if (ColumnNature == "Outgoing" || ColumnNature == "Holiday") {
	  Content += '</td>';
		Content += '<td style="background: transparent; padding:0px; border:0px; padding:0px; spacing:0px; width:20px;">';
		Content += '<span class="table_row_tool" style="float:right;">';
		Content += '<a href="#" onClick="Show_Reason_Layer(this); return false;" class="reason'+(($('input#'+StaffID+'-'+TimeSlotID+'-'+ColumnNature+'-User-Reason').val() > 0)? '':'_alert')+'" title="<?=$Lang['StaffAttendance']['FullDayReason']?>" style="display:inline; float:right;"></a>';
		Content += '\n'; // this linebreak must be included for firefox process
		Content += '<div id="'+StaffID+'-'+TimeSlotID+'-'+ColumnNature+'ReasonLayer" class="selectbox_layer ReasonSelector">';
		Content += '\n'; // this linebreak must be included for firefox process
	  Content += '<select name="'+StaffID+'-'+TimeSlotID+'-'+ColumnNature+'" id="'+StaffID+'-'+TimeSlotID+'-'+ColumnNature+'" onchange="Check_Reason_Icon(this);">';
	  Content += '<option value=""><?=$Lang['StaffAttendance']['SelectReason']?></option>';
	  var ReasonList = document.getElementsByName(ColumnNature+'ReasonList[]');
		for (var j=0; j< ReasonList.length; j++) {
			Content += '<option value="'+(ReasonList[j].id)+'" '+((ReasonList[j].id == $('input#'+TimeSlotID+'-'+ColumnNature+'-User-Reason').val())? "selected":"")+'>'+htmlspecialchars(ReasonList[j].value,3)+'</option>';
		}
	  Content += '</select>';
	  Content += '<br /><?=$Lang['StaffAttendance']['Remark']?><br /><textarea name="'+StaffID+'-'+TimeSlotID+'-'+ColumnNature+'-Remark" id="'+StaffID+'-'+TimeSlotID+'-'+ColumnNature+'-Remark" rows="3" wrap="virtual" style="width:140px;height:90px;"></textarea>';
	  Content += '<br /><input class="formsmallbutton" onmouseover="this.className=\'formsmallbuttonon\'" onmouseout="this.className=\'formsmallbutton\'" value="<?=$Lang['Btn']['Done']?>" type="button" onclick="$(this).closest(\'div\').css(\'visibility\',\'hidden\');">';
	  Content += '</div>';
		Content += '</span>';
		Content += '</td>';
		Content += '</tr>';
		Content += '</table>';
	}
	
	return Content;
}

function Drag_And_Drop_Init(StaffID)
{
	var StartLeft = -10;
	var StartTop = -10;
	// draggable
	$("div#"+StaffID+"-OnDuty-Cell, div#"+StaffID+"-Outgoing-Cell, div#"+StaffID+"-Holiday-Cell").draggable({
			helper:'clone',
			cursorAt:{left:StartLeft,top:StartTop},
			distance:'10',
			revert:false,
			start: function (ev,ui) {
				$('.ReasonSelector').css('visibility','hidden');
				
				var DragObj = $(this);
				var DragNature = DragObj.attr('id').split('-');
				DragNature = DragNature[1];
				ui.helper.html('');
				var DragSelectedSlots = Get_Check_Box_Value(StaffID+'-'+DragNature+'[]','Array');
				var Content = '';
				for (var i=0; i< DragSelectedSlots.length; i++) {
					Content += document.getElementById(StaffID+"-SlotName["+DragSelectedSlots[i]+"]").value+"<br>";
				}
				ui.helper.html(Content);
			},
			stop: function(event, ui) {
				$("div#"+StaffID+"-OnDuty-Cell").css('background-color','#EEEEEE');
				$("div#"+StaffID+"-Outgoing-Cell").css('background-color','#E2EDF9');
				$("div#"+StaffID+"-Holiday-Cell").css('background-color','#FFEDED');
			}
		});
		
	// droppable
	$("div#"+StaffID+"-Outgoing-Cell, div#"+StaffID+"-Holiday-Cell, div#"+StaffID+"-OnDuty-Cell").droppable({
		accept: "div#"+StaffID+"-Outgoing-Cell, div#"+StaffID+"-Holiday-Cell, div#"+StaffID+"-OnDuty-Cell",
		tolerance: "pointer",
		/*activeClass: 'droppable-active',
		hoverClass: 'droppable-hover',*/
		drop: function(ev, ui) {
			var DragObj = ui.draggable;
			var DropObj = $(this);
			var DragNature = DragObj.attr('id').split('-');
			var DragNature = DragNature[1];
			var DropNature = DropObj.attr('id').split('-');
			var DropNature = DropNature[1];
			
			var DragSelectedSlots = Get_Check_Box_Value(StaffID+'-'+DragNature+'[]','Array');
			var DragSlots = Get_Check_Box_Value(StaffID+'-'+DragNature+'[]','Array',true);
			var DropSlots = Get_Check_Box_Value(StaffID+'-'+DropNature+'[]','Array',true);
			
			for (var i=0; i< DragSelectedSlots.length; i++) {
				if (!jIN_ARRAY(DropSlots,DragSelectedSlots[i])) {
					var TimeSlotID = DragSelectedSlots[i];
					
					var Content = Get_Slot_Content_Html(StaffID,TimeSlotID,DropNature);
          var Div = $(Content);
          $('#'+StaffID+'-'+DropNature+'-Cell').append(Div);
				}
			}
			
			var Content = '';
			for (var i=0; i< DragSlots.length; i++) {
				if (!jIN_ARRAY(DragSelectedSlots,DragSlots[i])) {
					var TimeSlotID = DragSlots[i];
					
					Content += Get_Slot_Content_Html(StaffID,TimeSlotID,DragNature);
				}
			}
			
			$('#'+StaffID+'-'+DragNature+'-Cell').html(Content);
			if (is.ie) // useless code to deal with the strange UI crash case in IE
				$('#'+StaffID+'-'+DragNature+'-Cell').parent().hide('fast').show('fast');
			
			$("div#"+StaffID+"-OnDuty-Cell").css('background-color','#EEEEEE');
			$("div#"+StaffID+"-Outgoing-Cell").css('background-color','#E2EDF9');
			$("div#"+StaffID+"-Holiday-Cell").css('background-color','#FFEDED');
		},
		activate: function(ev, ui) {
			$(this).css('background-color','#DEB887');
		},
		over: function(ev, ui) {
			$(this).css('background-color','#FFFFE6');
		},
		out:  function(ev, ui) {
			$(this).css('background-color','#DEB887');
		}
	});
}

$(document).ready(function(){
	$('#form1 #RecordDate').datepick('option','minDate',new Date((new Date()).getTime() + 24 * 60 * 60 * 1000));
<?php if($is_edit){ ?>
	LoadEditForm($('#TargetStaffID\\[\\]').val(),$('#RecordDate').val());
<?php } ?>
});
</script>
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>