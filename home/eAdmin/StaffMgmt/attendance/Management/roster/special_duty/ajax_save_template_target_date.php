<?php
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!$StaffAttend3->Check_Access_Right('DUTYSETUP-SPECIAL'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	echo 'die';
	intranet_closedb();
	exit();
}

$TemplateID = $_REQUEST['TemplateID'];
$TargetDate = $_REQUEST['TargetDate'];

$StaffAttend3->Start_Trans();
if ($StaffAttend3->Save_Template_Target_Date($TemplateID,$TargetDate)) {
	echo $Lang['StaffAttendance']['DateAppliedSuccess'];
	$StaffAttend3->Commit_Trans();
}
else {
	echo $Lang['StaffAttendance']['DateAppliedFail'];
	$StaffAttend3->RollBack_Trans();
}

intranet_closedb();
?>