<?php
// Editing by 
/*
 * 2016-03-11 (Carlos): Added js exportDuty() for exporting special duty csv.
 */
$PATH_WRT_ROOT = "../../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend2.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!$StaffAttend3->Check_Access_Right('DUTYSETUP-SPECIAL'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$linterface = new interface_html();
$StaffAttend3UI = new libstaffattend3_ui();

$CurrentPageArr['eAdminStaffAttendance'] = 1;
$CurrentPage['DutySpecial'] = 1;
$TAGS_OBJ[] = array($Lang['StaffAttendance']['SpecialRoster'], "", 1);
$TAGS_OBJ[] = array($Lang['StaffAttendance']['WholeSchoolSpecialDaySetting'], $PATH_WRT_ROOT.'home/eAdmin/StaffMgmt/attendance/Management/roster/special_duty/', 0);
$MODULE_OBJ = $StaffAttend3UI->GET_MODULE_OBJ_ARR();
//$TAGS_OBJ_RIGHT[] = array(gen_online_help_btn_and_layer('staff_attendance','groupduty_tab'));
$linterface->LAYOUT_START(urldecode($_REQUEST['Msg']));

$TargetMonth = $_REQUEST['TargetMonth'];
$TargetYear = $_REQUEST['TargetYear'];

echo $StaffAttend3UI->Get_Individual_Index($TargetMonth,$TargetYear);

$linterface->LAYOUT_STOP();
intranet_closedb();
?>
<script>
// ajax function
{	
function Save_Slot_Setting() {
	var SelectedSlot = Get_Check_Box_Value('TimeSlotID[]','Array');
	var StaffID = $('input#StaffID').val();
	var GroupSlotPeriodID = $('input#GroupSlotPeriodID').val();
	var DayOfWeek = Get_Check_Box_Value('DayOfWeek[]','Array');
	
	if (GroupSlotPeriodID.length > 0) {
		document.getElementById('DayOfWeekWarningRow').style.display = "none";
		$('span#DayOfWeekWarningLayer').html('');
		
		var SlotUserList = new Array();
		var EvalStr = 'var PostVar = {';
		for (var i=0; i< SelectedSlot.length; i++) {
			SlotUserList[i] = Get_Check_Box_Value(SelectedSlot[i]+'-UserID[]','Array',true);
			EvalStr += '"'+SelectedSlot[i]+'[]":SlotUserList['+i+'],';
		}
		EvalStr += '"TimeSlotID[]":SelectedSlot,';
		EvalStr += '"DayOfWeek[]":DayOfWeek,';
		EvalStr += '"StaffID":StaffID,';
		EvalStr += '"GroupSlotPeriodID":GroupSlotPeriodID};';
		eval(EvalStr);

		Block_Element('GroupIndexLayer');
		$.post('ajax_save_duty_period_user_time_slot.php',PostVar,
			function(data) {
				if (data == "die") 
					window.top.location = "/";
				else {
					Scroll_To_Top();
					Get_Return_Message(data);
					Get_Duty_Period_Detail(StaffID,GroupSlotPeriodID);
				}
			});
	}
	else {
		document.getElementById('DayOfWeekWarningRow').style.display = "";
		$('span#DayOfWeekWarningLayer').html('<?=$Lang['StaffAttendance']['DayOfWeekWarning']?>');
	}
}
	
function Check_Slot_Setting() {
	var SelectedSlot = Get_Check_Box_Value('TimeSlotID[]','Array');
	
	if (SelectedSlot.length > 0) {
		var SlotUserList = new Array();
		var EvalStr = 'var PostVar = {';
		for (var i=0; i< SelectedSlot.length; i++) {
			SlotUserList[i] = Get_Check_Box_Value(SelectedSlot[i]+'-UserID[]','Array',true);
			EvalStr += '"'+SelectedSlot[i]+'[]":SlotUserList['+i+'],';
		}
		EvalStr += '"TimeSlotID[]":SelectedSlot};';
		eval(EvalStr);
		
		$.post('../ajax_check_duty_period_user_time_slot.php',PostVar,
			function(data) {
				if (data == "die") 
					window.top.location = "/";
				else {
					var Result = data.split('|=|');
					
					if (Result[0] == '1') {
						Save_Slot_Setting();
					}
					else {
						var DutyOver24Hour = Result[1].split(',');
						var TimeSlotOverlap = Result[2].split(',');
						var AlertContent = '';
						
						AlertContent += '<?=$Lang['StaffAttendance']['DutyOver24HourWarning']?>\n';
						if (Result[1] != "") {
							for (var i=0; i< DutyOver24Hour.length; i++) {
								AlertContent += document.getElementById('UserName['+DutyOver24Hour[i]+']').value+', ';
							}
						}
						else {
							AlertContent += 'N/A';
						}
						
						AlertContent += '\n\n';
						AlertContent += '<?=$Lang['StaffAttendance']['DutyOverlapWarning']?>\n';
						if (Result[2] != "") {
							for (var i=0; i< TimeSlotOverlap.length; i++) {
								AlertContent += document.getElementById('UserName['+TimeSlotOverlap[i]+']').value+', ';
							}
						}
						else {
							AlertContent += 'N/A';
						}
						
						AlertContent += '\n\n';
						AlertContent += '<?=$Lang['StaffAttendance']['DutyWarning']?>\n';
						
						alert(AlertContent);
					}
				}
			});
	}
	else {
		alert('<?=$Lang['StaffAttendance']['DutyApplyDuty']?>');
	}
}

function Get_Roster_Form(StaffID,GroupSlotPeriodID,DayOfWeek) {
	var PostVar = {
		"StaffID":StaffID,
		"DayOfWeek":DayOfWeek,
		"GroupSlotPeriodID": GroupSlotPeriodID
	};
	
	Block_Element('IndividualIndexLayer');
	$('div#IndividualIndexLayer').load('ajax_get_roster_form.php',PostVar,
		function(data) {
			if (data == "die") 
				window.top.location = '/';
			else {
				Drag_And_Drop_Init();
				UnBlock_Element('IndividualIndexLayer');
			}
		});
}

function Save_Duty_Period() {
	Block_Thickbox();
	
	var PostVar = {
		"StaffID": $('input#StaffID').val(),
		"GroupSlotPeriodID": $('input#GroupSlotPeriodID').val(),
		"EffectiveStart": encodeURIComponent($('input#EffectiveStart').val()),
		"EffectiveEnd": encodeURIComponent($('input#EffectiveEnd').val()),
		"SkipSchoolHoliday": ((document.getElementById('SkipSchoolHoliday').checked)? '1':'0'),
		"SkipPublicHoliday": ((document.getElementById('SkipPublicHoliday').checked)? '1':'0'),
		"SkipSchoolEvent": ((document.getElementById('SkipSchoolEvent').checked)? '1':'0')
	};
	
	$.post('ajax_save_duty_period.php',PostVar,
		function (data) {
			if (data == "die") 
				window.top.location = '/';
			else {
				Get_Normal_Detail($('input#StaffID').val());
				window.top.tb_remove();
				Get_Return_Message(data);
			}
		});
}

function Delete_Duty_Period(StaffID,GroupSlotPeriodID) {
	if (confirm('<?=$Lang['StaffAttendance']['DutyPeriodDeleteConfim']?>')) {
		Block_Thickbox();
		
		var PostVar = {
			"GroupSlotPeriodID": GroupSlotPeriodID
		};
		
		$.post('ajax_delete_duty_period.php',PostVar,
			function (data) {
				if (data == "die") 
					window.top.location = '/';
				else {
					Get_Normal_Detail(StaffID);
					window.top.tb_remove();
					Get_Return_Message(data);
				}
			});
	}
}

function Check_Duty_Period() {
	var EffectiveArray = new Array();
	
	var EffectiveStart = Trim($('Input#EffectiveStart').val());
	EffectiveArray = EffectiveStart.split('-');
	var DateStart = new Date();
	DateStart.setFullYear(EffectiveArray[0],(EffectiveArray[1]-1),EffectiveArray[2]);
	
	var EffectiveEnd = Trim($('Input#EffectiveEnd').val());
	EffectiveArray = EffectiveEnd.split('-');
	var DateEnd = new Date();
	DateEnd.setFullYear(EffectiveArray[0],(EffectiveArray[1]-1),EffectiveArray[2]);
	
	var WarningRow = document.getElementById('EffectiveDateWarningRow');
	var WarningLayer = $('div#EffectiveDateWarningLayer');
	
	if (EffectiveStart == "" || EffectiveEnd == "") {
		WarningLayer.html('<?=$Lang['StaffAttendance']['EffectiveDateNullWarning']?>');
		WarningRow.style.display = "";
	}
	else if (DateStart > DateEnd) {
		WarningLayer.html('<?=$Lang['StaffAttendance']['EffectiveEndDateLesserThenStartDateWarning']?>');
		WarningRow.style.display = "";
	}
	else {
		var PostVar = {
				"StaffID": $('Input#StaffID').val(),
				"GroupSlotPeriodID": $('Input#GroupSlotPeriodID').val(),
				"EffectiveStart": encodeURIComponent(EffectiveStart),
				"EffectiveEnd": encodeURIComponent(EffectiveEnd)
				};
				
		$.post('ajax_check_duty_period.php',PostVar,
			function(data){
				if (data == "die") 
					window.top.location = '/';
				else {
					if (data == "1") {
						WarningLayer.html('');
						WarningRow.style.display = "none";
						
						Save_Duty_Period();
					}
					else {
						WarningLayer.html('<?=$Lang['StaffAttendance']['EffectiveDateOverlapWarning']?>');
						WarningRow.style.display = "";
					}
				}
			});
	}
}	

function Get_Duty_Period_Form(StaffID,SlotPeriodID) {
	SlotPeriodID = SlotPeriodID || "";
	
	var PostVar = {
		"StaffID": StaffID,
		"SlotPeriodID": SlotPeriodID
	};
	
	$.post('ajax_get_individual_duty_period_form.php',PostVar,
		function(data) {
			if (data == "die") 
				window.top.location="/";
			else {
				$('div#TB_ajaxContent').html(data);
			}
		}
		);
}

function Get_Duty_Period_Detail(StaffID,GroupSlotPeriodID) {
	var PostVar = {
		"StaffID":StaffID,
		"GroupSlotPeriodID": GroupSlotPeriodID
	};
	
	Block_Element('IndividualIndexLayer');
	$('div#IndividualIndexLayer').load('ajax_get_individual_duty_period_detail.php',PostVar,
		function (data) {
			if (data == "die") 
				window.top.location = '/';
			else {
				Thick_Box_Init();
				UnBlock_Element('IndividualIndexLayer');
			}
			//document.getElementById('GroupIndexLayer').innerHTML = data;
		});
}

function Get_Individual_List(TargetMonth,TargetYear) {
	var TargetMonth = TargetMonth || '';
	var TargetYear = TargetYear || '';
	var PostVar = {
		"TargetMonth":TargetMonth,
		"TargetYear":TargetYear};
	Block_Element('IndividualIndexLayer');
	$('div#IndividualIndexLayer').load('ajax_get_individual_index.php',PostVar,
		function(data) {
			if (data == "die") 
				window.top.location="/";
			else {
				UnBlock_Element('IndividualIndexLayer');
			}
		});
}

function Get_Normal_Detail(UserID) {
	var PostVar = {"StaffID": UserID};
	
	Block_Element('IndividualIndexLayer');
	$('div#IndividualIndexLayer').load('ajax_get_individual_detail.php',PostVar,
		function(data) {
			if (data == "die") 
				window.top.location="/";
			else {
				Thick_Box_Init();
				UnBlock_Element('IndividualIndexLayer');
			}
		});
}

function validateDate(dateVal)
{
	var dateObj = new Date(dateVal);
	var year = '' + dateObj.getFullYear();
	var month = dateObj.getMonth()+1;
	var day = dateObj.getDate();
	month = month < 10? '0' + month : '' + month;
	day = day < 10? '0' + day : '' + day;
	
	var dateStr = year + '-' + month + '-' + day;
	if(dateVal != dateStr){
		return false;
	}
	return true;
}

function exportDuty()
{
  var selected_target_size = $('#ExportTargetID option:selected').length;
  var start_date_val = $.trim(document.getElementById('StartDate').value);
  var end_date_val = $.trim(document.getElementById('EndDate').value);
  var is_valid = true;
  
  if(selected_target_size == 0){
  	$('#ExportTargetIDError').show();
  	is_valid = false;
  }else{
  	$('#ExportTargetIDError').hide();
  }
  
  if(!validateDate(start_date_val)){
  	$('#DPWL-StartDate').html('<?=$Lang['General']['InvalidDateFormat']?>').show();
  	is_valid = false;
  	return false;
  }else{
  	$('#DPWL-StartDate').hide();
  }

  if(!validateDate(end_date_val)){
  	$('#DPWL-EndDate').html('<?=$Lang['General']['InvalidDateFormat']?>').show();
  	is_valid = false;
  	return false;
  }else{
  	$('#DPWL-EndDate').hide();
  }

  if(start_date_val > end_date_val){
	$('#DPWL-StartDate').html('<?=$Lang['General']['JS_warning']['InvalidDateRange']?>').show();
  	is_valid = false;
  	return false;
  }else{
  	$('#DPWL-StartDate').hide();
  }
	
	if(is_valid){
		$('form#ExportDutyForm').submit();
	}
}
}

// dom function 
{
function Select_DeSelect_All(ColumnType) {	
	if (ColumnType == "UnAssign") {
		var CheckBoxObj = document.getElementsByName('UnAssign-UserID[]');
	}
	else if (ColumnType == "Full") {
		var CheckBoxObj = document.getElementsByName('Search-UserID[]');
	}
	else {
		var CheckBoxObj = document.getElementsByName(ColumnType+'-UserID[]');
	}
	
	var AnyUnChecked=false;
	for (var i=0; i< CheckBoxObj.length; i++) {
		if (!CheckBoxObj[i].checked) {
			AnyUnChecked = true;
			CheckBoxObj[i].checked = true;
			CheckBoxObj[i].parentNode.style.backgroundColor = '#A6A6A6';
		}
	}
	
	if (!AnyUnChecked) {
		for (var i=0; i< CheckBoxObj.length; i++) {
			CheckBoxObj[i].checked = false;
			CheckBoxObj[i].parentNode.style.backgroundColor = '';
		}
	}
}
	
function Apply_Time_Slot() {
	var TimeSlot = Get_Selection_Value('TimeSlot[]','Array');
	var TimeSlotTable = document.getElementById('TimeSlotTable');
	var CellHeight = document.getElementById('FullStaffListLayer').offsetHeight;
	var TimeSlotTableCells = TimeSlotTable.tBodies[0].rows[0].cells;
	
	for (var i=0; i< TimeSlot.length; i++) {
		if (document.getElementById('TimeSlotID['+TimeSlot[i]+']')) {
			$('.TimeSlotColumn-'+TimeSlot[i]).css('display',''); 
			document.getElementById('TimeSlotID['+TimeSlot[i]+']').checked = true;
		}
		else {
			if (TimeSlotTableCells.length == 0) {
				CellIndex = 0;
			}
			else {
				var NewSlotStart = document.getElementById('SlotStart['+TimeSlot[i]+']').value;
				for (var j=0; j< TimeSlotTableCells.length; j++) {
					var CurrSlotStart = document.getElementById('SlotStart['+TimeSlotTableCells[j].id+']').value;
					
					if (NewSlotStart <= CurrSlotStart) {
						CellIndex = j;
						
						break;
					}
					else 
						CellIndex = -1;
				}
			}
			
			var cell1 = TimeSlotTable.tBodies[0].rows[0].insertCell(CellIndex);
			Content = '<input name="TimeSlotID[]" id="TimeSlotID['+TimeSlot[i]+']" type="checkbox" value="'+TimeSlot[i]+'" checked="checked" style="display:none;"/>';
			Content += '<span style="float:left;">';
			Content += document.getElementById('SlotName['+TimeSlot[i]+']').value;
			Content += '</span>';
			Content += '<span style="float:right;" class="table_row_tool row_content_tool">';
			Content += '<a onclick="$(\'.TimeSlotColumn-'+TimeSlot[i]+'\').hide(); document.getElementById(\'TimeSlotID['+TimeSlot[i]+']\').checked = false;" href="#" class="delete" title="<?=$Lang['Btn']['Remove']?>"></a>';
			Content += '</span>';
			cell1.className = 'sub_row_top TimeSlotColumn-'+TimeSlot[i];
			cell1.style.backgroundColor = '#7E7E7E';
			cell1.id = TimeSlot[i];
			cell1.innerHTML = Content;
			
			var cell2 = TimeSlotTable.tBodies[0].rows[1].insertCell(CellIndex);
			Content =	'<div class="roster_slot_tool"> ';
			Content +=	'<span style="float:left;">';
			Content +=	'<strong style="color:#000000;"><?=$Lang['StaffAttendance']['OnDuty']?> <font id="'+TimeSlot[i]+'CountLayer">(0)</font></strong>';
			Content +=	'</span>';
			Content +=	'<span style="float:right;" class="row_content_tool">';
			Content +=  '<a href="#" style="color:#000000;" onmouseover="this.style.color = \'#FF0000\';" onmouseout="this.style.color = \'#000000\';" onclick="Select_DeSelect_All(\''+TimeSlot[i]+'\'); return false;"><?=$Lang['StaffAttendance']['SelectDeSelectAll']?></a>';
			Content +=	'</span>';
			Content +=	'</div>';
			cell2.className = 'TimeSlotColumn-'+TimeSlot[i];
			cell2.innerHTML = Content;
			
			
			var cell3 = TimeSlotTable.tBodies[0].rows[2].insertCell(CellIndex);
			cell3.className = 'TimeSlotCell TimeSlotColumn-'+TimeSlot[i];
			Content = '<div id="TimeSlot-'+TimeSlot[i]+'" class="TimeSlotDragBlock TimeSlotDropBlock roster_staff_item roster_staff_item_dayoff" style="min-height:'+CellHeight+'px;"></div>';
			cell3.innerHTML = Content;
		}
	}
	
	Drag_And_Drop_Init();
}

var OrgColWidth = 0;
function Toggle_Full_Member_Layer() {
	var FullMemberLayer = document.getElementById('FullMemberPanelTable');
	if (FullMemberLayer.style.display == "none") {
		FullMemberLayer.style.display = '';
		//document.getElementById('TimeSlotCell').style.width = OrgColWidth + 'px';
		document.getElementById('TimeSlotLayer').style.width = OrgColWidth + 'px';
	}
	else {
		OrgColWidth = document.getElementById('TimeSlotCell').offsetWidth;
		
		FullMemberLayer.style.display = 'none';
		var ColWidth = document.getElementById('TimeSlotCell').offsetWidth;
		document.getElementById('TimeSlotLayer').style.width = ColWidth + 'px';
	}
}

function Change_Background_Color(CheckBoxID,DivObj) {
	var Temp = CheckBoxID.split('-');
	var CheckboxObj = document.getElementById(CheckBoxID);

	if (CheckboxObj.checked) {
		//alert('-');
		CheckboxObj.checked = false;
		DivObj.style.backgroundColor = '';
	}
	else {
		//alert('+');
		CheckboxObj.checked = true;
		DivObj.style.backgroundColor = '#A6A6A6';
	}		
}

function Calculate_Selected_Count(CheckedStatus,TimeSlotID) {
}
}

// Drag and Drop function
{
function Drag_And_Drop_Init() {
<?if ($StaffAttend3->Check_Access_Right('DUTYSETUP-BASIC-Manage') || $StaffAttend3->IS_ADMIN_USER()) {?>
	var StartLeft = -10;
	var StartTop = -10;
	// draggable
	$("div.TimeSlotDragBlock").draggable({
			helper:'clone',
			revert:false,
			cursorAt:{left:StartLeft,top:StartTop},
			distance:'10',
			start: function (ev,ui) {
				ui.helper.html('');
				
				var ElementID = $(this).attr('id');
				var TimeSlotID = ElementID.substring((ElementID.indexOf('-')+1));
				var OnDutyCount = Get_Check_Box_Value(TimeSlotID+'-UserID[]','Array');
				ui.helper.html('<font style="color:#000000;">'+OnDutyCount.length+' people selected</font>');
			},
			stop: function(event, ui) {
				$('div.TimeSlotDropBlock').css('background-color','#FFFFFF');
				$('div#UnAssignLayer').css('background-color','#EEEEEE');
				$('div#FullStaffListLayer').css('background-color','#FFFFFF');
			}
		});
	
	$("div.FullDragBlock").draggable({
			helper:'clone',
			revert:false,
			cursorAt:{left:StartLeft,top:StartTop},
			distance:'10',
			start: function (ev,ui) {
				ui.helper.html('');
				
				var FullCount = Get_Check_Box_Value('Search-UserID[]','Array');
				ui.helper.html(FullCount.length+' people selected');
			},
			stop: function(event, ui) {
				$('div.TimeSlotDropBlock').css('background-color','#FFFFFF');
				$('div#UnAssignLayer').css('background-color','#EEEEEE');
				$('div#FullStaffListLayer').css('background-color','#FFFFFF');
			}
		});
		
	// droppable
	$("div.TimeSlotDragBlock").droppable({
		accept: "div.UnAssignDragBlock, div.FullDragBlock",
		/*activeClass: 'droppable-active',
		hoverClass: 'droppable-hover',*/
		drop: function(ev, ui) {
			ElementID = $(this).attr('id');
			var TimeSlotID = Trim(ElementID.substring((ElementID.indexOf('-')+1)));
			var CurTimeSlotList = Get_Check_Box_Value(TimeSlotID+'-UserID[]','Array',true);
			if (ui.draggable.attr('id') == "UnAssignLayer") {
				var SelectedUser = Get_Check_Box_Value("UnAssign-UserID[]","Array");
			}
			else {
				var SelectedUser = Get_Check_Box_Value("Search-UserID[]","Array");
			}
			
			for (var i=0; i< SelectedUser.length; i++) {
				if (!jIN_ARRAY(CurTimeSlotList,SelectedUser[i])) {
					var UserID = SelectedUser[i];
					var StaffName = document.getElementById('UserName['+UserID+']').value;
					//var Div = document.createElement('div');
					var Content = '<div onclick="Change_Background_Color(\''+TimeSlotID+'-UserID-'+UserID+'\',this);" style="cursor:pointer;">';
					Content += '<input style="display:none;" name="'+TimeSlotID+'-UserID[]" id="'+TimeSlotID+'-UserID-'+UserID+'" type="checkbox" value="'+UserID+'"/>';
          Content += '<font class="staff_name" style="color:#000000;">'+StaffName+'</font>';
          Content += '</div>';
          var Div = $(Content);
          $('#TimeSlot-'+TimeSlotID).append(Div);
				}
			}
			
			// calculate the count for the time slot column
			var NewTimeSlotList = Get_Check_Box_Value(TimeSlotID+'-UserID[]','Array',true);
			document.getElementById(TimeSlotID+'CountLayer').innerHTML = '('+NewTimeSlotList.length+')';
			
			$('div.TimeSlotDropBlock').css('background-color','#FFFFFF');
			$('div#FullStaffListLayer').css('background-color','#FFFFFF');
		},
		activate: function(ev, ui) {
			$('div.TimeSlotDropBlock').css('background-color','#DEB887');
			$('div#FullStaffListLayer').css('background-color','#FFFFFF');
		},
		over: function(ev, ui) {
			$(this).css('background-color','#FFFFE6');
		},
		out:  function(ev, ui) {
			$(this).css('background-color','#DEB887');
		}
	});
	
	$("div.FullDropBlock").droppable({
		accept: "div.TimeSlotDragBlock",
		/*activeClass: 'droppable-active',
		hoverClass: 'droppable-hover',*/
		drop: function(ev, ui) {
			ElementID = ui.draggable.attr('id');
			var TimeSlotID = Trim(ElementID.substring((ElementID.indexOf('-')+1)));
			var SlotFullUserList = Get_Check_Box_Value(TimeSlotID+'-UserID[]','Array',true);
			var SlotSelectedUserList = Get_Check_Box_Value(TimeSlotID+'-UserID[]','Array');
			
			Content = '';
			for (var i=0; i< SlotFullUserList.length; i++) {
				if (!jIN_ARRAY(SlotSelectedUserList,SlotFullUserList[i])) {
					var UserID = SlotFullUserList[i];
					var StaffName = document.getElementById('UserName['+UserID+']').value;
					
					Content += '<div onclick="Change_Background_Color(\''+TimeSlotID+'-UserID-'+UserID+'\',this);" style="cursor:pointer;">';
					Content += '<input style="display:none;" name="'+TimeSlotID+'-UserID[]" id="'+TimeSlotID+'-UserID-'+UserID+'" type="checkbox" value="'+UserID+'"/>';
          Content += '<font class="staff_name" style="color:#000000;">'+StaffName+'</font>';
          Content += '</div>';
				}
			}
			
			ui.draggable.html(Content);
			
			// calculate the count for the time slot column
			var NewTimeSlotList = Get_Check_Box_Value(TimeSlotID+'-UserID[]','Array',true);
			document.getElementById(TimeSlotID+'CountLayer').innerHTML = '('+NewTimeSlotList.length+')';
			
			$('div.TimeSlotDropBlock').css('background-color','#FFFFFF');
			$('div#FullStaffListLayer').css('background-color','#FFFFFF');
		},
		activate: function(ev, ui) {
			$('div.TimeSlotDropBlock').css('background-color','#FFFFFF');
			$('div#FullStaffListLayer').css('background-color','#DEB887');
		},
		over: function(ev, ui) {
			$(this).css('background-color','#FFFFE6');
		},
		out:  function(ev, ui) {
			$(this).css('background-color','#DEB887');
		}
	});
<?}?>
}
}

// thick box function 
{
//on page load call tb_init
function Thick_Box_Init(){   
	tb_init('a.thickbox');//pass where to apply thickbox
}
}
</script>