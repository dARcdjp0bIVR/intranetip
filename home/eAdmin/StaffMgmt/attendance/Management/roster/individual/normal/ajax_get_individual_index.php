<?php
$PATH_WRT_ROOT = "../../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!$StaffAttend3->Check_Access_Right('DUTYSETUP-SPECIAL'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	echo 'die';
	intranet_closedb();
	exit();
}


$StaffAttend3UI = new libstaffattend3_ui();

$TargetMonth = $_REQUEST['TargetMonth'];
$TargetYear = $_REQUEST['TargetYear'];
$Keyword = trim(urldecode(stripslashes($_REQUEST['Keyword'])));

echo $StaffAttend3UI->Get_Normal_Individual_Index($Keyword,$TargetMonth,$TargetYear);

intranet_closedb();
?>