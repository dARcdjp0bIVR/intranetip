<?php
// editing by 
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!($StaffAttend3->Check_Access_Right('DUTYSETUP-BASIC') || $StaffAttend3->Check_Access_Right('DUTYSETUP-SPECIAL')))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	echo 'die';
	intranet_closedb();
	exit();
}

$TimeSlotID = $_REQUEST['TimeSlotID'];
$TimeSlotUser = (is_array($_REQUEST['TimeSlotUser']))? $_REQUEST['TimeSlotUser']:array();

// edit duty setting
$TimeSlotName = $_REQUEST['TimeSlotName'];
$SlotStart = (is_array($_REQUEST['SlotStart']))? $_REQUEST['SlotStart']:array();
$SlotEnd = (is_array($_REQUEST['SlotEnd']))? $_REQUEST['SlotEnd']:array();
$EditDuty = ($_REQUEST['EditDuty'] == '1')? true:false;

$Result = $StaffAttend3->Check_Duty_Period_User_Time_Slot($TimeSlotID,$TimeSlotUser,$SlotStart,$SlotEnd,$EditDuty);
if (sizeof($Result['DutyOver24Hour']) > 0 || sizeof($Result['TimeSlotOverlap']) > 0) {
	$Return = '0|=|';
	if (sizeof($Result['DutyOver24Hour']) > 0)
		$Return .= implode(',',$Result['DutyOver24Hour']);
	$Return .= '|=|';
	
	if (sizeof($Result['TimeSlotOverlap']) > 0)
		$Return .= implode(',',$Result['TimeSlotOverlap']);
}
else {
	$Return = '1';
}

echo $Return;
intranet_closedb();
?>