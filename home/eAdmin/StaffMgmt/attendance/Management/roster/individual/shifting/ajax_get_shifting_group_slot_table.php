<?php
// Editing by 
/*
 * 2015-10-19 (Carlos): Created for getting table mode time slot tables.
 */
$PATH_WRT_ROOT = "../../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!$StaffAttend3->Check_Access_Right('DUTYSETUP-SPECIAL'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	echo 'die';
	intranet_closedb();
	exit();
}

$StaffAttend3UI = new libstaffattend3_ui();

$ID = $_REQUEST['ID'];
$TargetDate = $_REQUEST['TargetDate'];
$Type = "Individual";
$SlotID = $_REQUEST['SlotID']; // array

echo $StaffAttend3UI->Get_Special_Duty_Drop_Down_Table($ID,$TargetDate,$Type, $SlotID);

intranet_closedb();
?>