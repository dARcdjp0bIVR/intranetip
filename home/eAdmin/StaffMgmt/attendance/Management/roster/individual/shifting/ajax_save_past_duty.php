<?php
/*
 * 2016-12-13 (Carlos): $sys_custom['StaffAttendance']['WongFutNamCollege'] - added post param AttendOne.
 */
$PATH_WRT_ROOT = "../../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!$StaffAttend3->Check_Access_Right('DUTYSETUP-SPECIAL'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	echo 'die';
	intranet_closedb();
	exit();
}

$StaffID = $_REQUEST['StaffID'];
$TargetDate = $_REQUEST['TargetDate'];
$OutgoingReason = $_REQUEST['OutgoingReason'];
$HolidayReason = $_REQUEST['HolidayReason'];
$TimeSlotName = (is_array($_REQUEST['TimeSlotName']))? $_REQUEST['TimeSlotName']:array();
$FullHolidayList = $_REQUEST['FullHolidayList'];
$FullHolidayReason = $_REQUEST['FullHolidayReason'];
$FullOutgoingList = $_REQUEST['FullOutgoingList'];
$FullOutgoingReason = $_REQUEST['FullOutgoingReason'];
$SlotName = $_REQUEST['SlotName'];
$SlotStart = $_REQUEST['SlotStart'];
$SlotEnd = $_REQUEST['SlotEnd'];
$SlotInWavie = $_REQUEST['SlotInWavie'];
$SlotOutWavie = $_REQUEST['SlotOutWavie'];
$SlotDutyCount = $_REQUEST['SlotDutyCount'];
$TimeSlotUser = array();
$HolidayUser = array();
$OutgoingUser = array();
for ($i=0; $i< sizeof($TimeSlotName); $i++) {
	$TimeSlotUser[$TimeSlotName[$i]] = $_REQUEST[str_replace(" ","_",$TimeSlotName[$i].'-OnDuty')];
	$HolidayUser[$TimeSlotName[$i]] = $_REQUEST[str_replace(" ","_",$TimeSlotName[$i].'-Holiday')];
	$OutgoingUser[$TimeSlotName[$i]] = $_REQUEST[str_replace(" ","_",$TimeSlotName[$i].'-Outgoing')];
}

$AttendOne = $_REQUEST['AttendOne'];
if(!in_array($AttendOne,array(0,1))) $AttendOne = 0;

$StaffAttend3->Start_Trans();
if ($StaffAttend3->Save_Past_Duty($StaffID,
								$TargetDate,
								$TimeSlotName,
								$TimeSlotUser,
								$HolidayUser,
								$OutgoingUser,
								$OutgoingReason,
								$HolidayReason,
								$FullHolidayList,
								$FullOutgoingList,
								$FullHolidayReason,
								$FullOutgoingReason,
								$SlotName,
								$SlotStart,
								$SlotEnd,
								$SlotInWavie,
								$SlotOutWavie,
								$SlotDutyCount,
								'Individual',
								$AttendOne)) {
	echo $Lang['StaffAttendance']['ShiftDutySaveSuccess'];
	$StaffAttend3->Commit_Trans();
}
else {
	echo $Lang['StaffAttendance']['ShiftDutySaveFail'];
	$StaffAttend3->RollBack_Trans();
}

intranet_closedb();
?>