<?php
// editing by 
/*
 * 2017-07-14 (Carlos): Fixed Get_Slot_Column(CellIndex,TimeSlotID,EditDuty) fail to get time slot detail issue.
 * 2016-12-13 (Carlos): $sys_custom['StaffAttendance']['WongFutNamCollege'] - modified js Save_Slot_Setting(EditDuty).
 * 2015-10-19 (Carlos): Modified js Apply_Time_Slot(), Save_Slot_Setting(), Check_Reason_Icon(), to improve performance for using new table mode for setting duty. 
 * 2015-03-17 (Carlos): Modified js Drag_And_Drop_Init(), workaround IE11 fail to set min height problem with js.
 * 2015-03-17 (Carlos): If webkit browser, use onselectstart return false to workaround the drag & drop highlight text problem. Drawback is all text cannot be highlighted.
 * 2014-12-22 (Carlos): Added js function Apply_Time_Slot_To_Confirmed_Duty()
 */
$PATH_WRT_ROOT = "../../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend2.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!$StaffAttend3->Check_Access_Right('DUTYSETUP-SPECIAL'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$linterface = new interface_html();
$StaffAttend3UI = new libstaffattend3_ui();

$CurrentPageArr['eAdminStaffAttendance'] = 1;
$CurrentPage['DutySpecial'] = 1;
$TAGS_OBJ[] = array($Lang['StaffAttendance']['SpecialRoster'], $PATH_WRT_ROOT.'home/eAdmin/StaffMgmt/attendance/Management/roster/group/normal/', 1);
$TAGS_OBJ[] = array($Lang['StaffAttendance']['WholeSchoolSpecialDaySetting'], $PATH_WRT_ROOT.'home/eAdmin/StaffMgmt/attendance/Management/roster/special_duty/', 0);
$MODULE_OBJ = $StaffAttend3UI->GET_MODULE_OBJ_ARR();
//$TAGS_OBJ_RIGHT[] = array(gen_online_help_btn_and_layer('staff_attendance','groupduty_tab'));
$linterface->LAYOUT_START(urldecode($_REQUEST['Msg']));

$StaffID = $_REQUEST['StaffID'];
$TargetMonth = $_REQUEST['TargetMonth'];
$TargetYear = $_REQUEST['TargetYear'];

echo $StaffAttend3UI->Get_Shifting_Group_Index($StaffID,$TargetMonth,$TargetYear,"Individual");

$linterface->LAYOUT_STOP();
intranet_closedb();
?>
<script>
var SlotJustCheck = 1;
// ajax function
{
function Delete_Full_Day_Setting(TargetDate,StaffID) {
	if (confirm('<?=$Lang['StaffAttendance']['DeleteFullDaySettingConfirm']?>')) {
		var PostVar = {
			"StartDate":TargetDate,
			"EndDate":TargetDate,
			"StaffID":StaffID,
			"LeaveNature":"Full"
		};
		
		$.post('../../full_day_setting/ajax_save_full_day_setting.php',PostVar,
			function (data) {
				if (data == "die") 
					window.top.location = "/";
				else {
					Get_Add_Shifting_Day_Setting_Form($('input#StaffID').val(), TargetDate);
				}
			});
	}
}
	
function Edit_Time_Slot(SlotName,EditDuty) {
	var EditDuty = EditDuty || false;
	var SlotName = SlotName || '';
	JQuerySlotName = SlotName.replace(/ /g,'\\ ');
	
	if (SlotName != "") {
		var SlotColumn = $('#'+JQuerySlotName);
		var PostVar = {
			"SlotName": encodeURIComponent(SlotColumn.children('input#SlotName\\['+JQuerySlotName+'\\]').val()),
			"SlotStart": SlotColumn.children('input#SlotStart\\['+JQuerySlotName+'\\]').val(),
			"SlotEnd": SlotColumn.children('input#SlotEnd\\['+JQuerySlotName+'\\]').val(),
			"InWavie": SlotColumn.children('input#SlotInWavie\\['+JQuerySlotName+'\\]').val(),
			"OutWavie": SlotColumn.children('input#SlotOutWavie\\['+JQuerySlotName+'\\]').val(),
			"DutyCount": SlotColumn.children('input#SlotDutyCount\\['+JQuerySlotName+'\\]').val(),
			"HiddenSlotID": encodeURIComponent(SlotName)
			};
	}
	else {
		if (EditDuty) {
			var PostVar = '';
		}
		else {
			var PostVar = {
				"TargetUserID": $('input#StaffID').val(),
				"NoNextStepBtn": '1'
			};
		}		
	}
	
	$.post('../../../../Settings/time_slot/ajax_get_slot_form.php',PostVar,
		function (data) {
			if (data == "die") 
				window.top.location = "/";
			else 
				$('div#TB_ajaxContent').html(data);
		});
}
	
function Get_Change_Confirmed_Duty_Form(StaffID, TargetDate) {
	var PostVar = {
		"StaffID":StaffID,
		"TargetDate":TargetDate
	};
	
	Block_Element('GroupIndexLayer');
	$('div#GroupIndexLayer').load('ajax_get_daily_confirmed_duty_form.php',PostVar,
		function (data) {
			if (data == "die") 
				window.top.location = "/";
			else {
				Scroll_To_Top();
				Drag_And_Drop_Init();
				Thick_Box_Init();
				UnBlock_Element('GroupIndexLayer');
			}
		});
}

function Get_Month_Calendar(Month,Year) {
	if (document.getElementById(Month+'-'+Year)) {
		$('div.MonthYearLayer').hide();
		$('div#'+Month+'-'+Year).show();
	}
	else {
		var PostVar = {
			"TargetMonth":Month,
			"TargetYear":Year
		};
		
		$.post('ajax_get_shifting_target_date_calendar.php',PostVar,
			function (data) {
				if (data == "die") 
					window.top.location = "/";
				else {
					$('div.MonthYearLayer').hide();
					
					var Div = $(data);
					$('div#day_option').append(Div);
				}
			});
	}
}

function Get_Add_Shifting_Day_Setting_Form(StaffID, TargetDate,RemoveSpecialNonSchoolDaySetting) {
	if (RemoveSpecialNonSchoolDaySetting && !confirm("<?=$Lang['StaffAttendance']['DeleteSpecialNonSchoolDayWarning']?>")) { 
		return; // if choose not to remove non school day setting, cancel the action
	}
	var PostVar = {
		"StaffID":StaffID,
		"TargetDate":TargetDate
	};
	
	Block_Element('GroupIndexLayer');
	$('div#GroupIndexLayer').load('ajax_get_shifting_group_duty_form.php',PostVar,
		function (data) {
			if (data == "die") 
				window.top.location = "/";
			else {
				Scroll_To_Top();
				Drag_And_Drop_Init();
				Thick_Box_Init();
				UnBlock_Element('GroupIndexLayer');
			}
		});
}
	
function Get_Shifting_Overview(StaffID,TargetMonth,TargetYear) {
	var TargetMonth = TargetMonth || document.getElementById('TargetMonth').options[document.getElementById('TargetMonth').selectedIndex].value;
	var TargetYear = TargetYear || document.getElementById('TargetYear').options[document.getElementById('TargetYear').selectedIndex].value;
	var PostVar = {
		"StaffID":StaffID,
		"TargetMonth":TargetMonth,
		"TargetYear":TargetYear
	};
	
	Block_Element('GroupIndexLayer');
	$('div#GroupIndexLayer').load('ajax_get_shifting_group_duty_calendar.php',PostVar,
		function (data) {
			if (data == "die") 
				window.top.location = "/";
			else {
				Scroll_To_Top();
				UnBlock_Element('GroupIndexLayer');
			}
		});
}
	
function Save_Slot_Setting(EditDuty) {
	var SetupMode = Get_Radio_Value('DutySetupMode');
	
	if (EditDuty) {
		var SelectedSlot = Get_Check_Box_Value('TimeSlotName[]','Array');
		var TargetDate = $('input#TargetDate').val();
	}
	else {
		if (SetupMode == "DragAndDrop") {
			var SelectedSlot = Get_Check_Box_Value('TimeSlotID[]','Array');
		}
		else { // basic
			var SelectedSlot = Get_Check_Box_Value('SlotInUseHidden[]','Array');
			var AvaliableStaff = Get_Check_Box_Value('Search-UserID[]','Array',true);
		}
		var TargetDate = Get_Check_Box_Value('TargetDate[]','Array');
		var RepeatStart = $('#RepeatStart').val();
		var RepeatEnd = $('#RepeatEnd').val();
		var UsingMethod = $('#TargetDateMethod').val();
	}

	var StaffID = $('input#StaffID').val();
	var ReturnMonth = $('input#ReturnMonth').val();
	var ReturnYear = $('input#ReturnYear').val();
	if (TargetDate.length > 0 || EditDuty) {
		if (!EditDuty) {
			document.getElementById('TargetDateWarningRow').style.display = "none";
			$('span#TargetDateWarningLayer').html('');
		}
		
		var SlotUserList = new Array();
		var HolidaySlotUserList = new Array();
		var OutgoingSlotUserList = new Array();
		var HaveSetting = false;
		var EvalStr = 'var PostVar = {';
		for (var i=0; i< SelectedSlot.length; i++) {
			if (SetupMode == "DragAndDrop" || EditDuty) {
				SlotUserList[i] = Get_Check_Box_Value(SelectedSlot[i]+'-OnDuty-UserID[]','Array',true);
				HolidaySlotUserList[i] = Get_Check_Box_Value(SelectedSlot[i]+'-Holiday-UserID[]','Array',true);
				OutgoingSlotUserList[i] = Get_Check_Box_Value(SelectedSlot[i]+'-Outgoing-UserID[]','Array',true);
				
				SlotUserList[i] = SlotUserList[i].concat(HolidaySlotUserList[i]).concat(OutgoingSlotUserList[i]);
				
				for (var j=0; j < OutgoingSlotUserList[i].length; j++) {
					var ReasonID = document.getElementById(SelectedSlot[i]+'-Outgoing-'+OutgoingSlotUserList[i][j]).value;
					EvalStr += '"OutgoingReason['+SelectedSlot[i]+']['+OutgoingSlotUserList[i][j]+']":"'+ReasonID+'",';
				}
				
				for (var j=0; j < HolidaySlotUserList[i].length; j++) {
					var ReasonID = document.getElementById(SelectedSlot[i]+'-Holiday-'+HolidaySlotUserList[i][j]).value;
					EvalStr += '"HolidayReason['+SelectedSlot[i]+']['+HolidaySlotUserList[i][j]+']":"'+ReasonID+'",';
				}
			}
			else {
				OnDutyArrayStr = '';
				HolidayArrayStr = '';
				OutgoingArrayStr = '';
				for (var j=0; j< AvaliableStaff.length; j++) {
					var StaffSelectedDuty = document.getElementById('DutySelect'+SelectedSlot[i]+'-'+AvaliableStaff[j]).value;
					if (StaffSelectedDuty != "NoSetting") {
						OnDutyArrayStr += AvaliableStaff[j] + ',';
					}
					
					if (StaffSelectedDuty == "Holiday") {
						HolidayArrayStr += AvaliableStaff[j] + ',';
					}
					
					if (StaffSelectedDuty == "Outgoing") {
						OutgoingArrayStr += AvaliableStaff[j] + ',';
					}
				}
				
				if (OnDutyArrayStr.substr(-1) == ',') {
					OnDutyArrayStr = OnDutyArrayStr.substr(0,(OnDutyArrayStr.length-1));
				}
				if (HolidayArrayStr.substr(-1) == ',') {
					HolidayArrayStr = HolidayArrayStr.substr(0,(HolidayArrayStr.length-1));
				}
				if (OutgoingArrayStr.substr(-1) == ',') {
					OutgoingArrayStr = OutgoingArrayStr.substr(0,(OutgoingArrayStr.length-1));
				}
				
				SlotUserList[i] = (OnDutyArrayStr != "")? OnDutyArrayStr.split(','):Array();
				HolidaySlotUserList[i] = (HolidayArrayStr != "")? HolidayArrayStr.split(','):Array();
				OutgoingSlotUserList[i] = (OutgoingArrayStr != "")? OutgoingArrayStr.split(','):Array();
				
				for (var j=0; j < OutgoingSlotUserList[i].length; j++) {
					if (document.getElementById('OutgoingReason['+SelectedSlot[i]+']['+OutgoingSlotUserList[i][j]+']')) {
						var ReasonID = document.getElementById('OutgoingReason['+SelectedSlot[i]+']['+OutgoingSlotUserList[i][j]+']').value;
						EvalStr += '"OutgoingReason['+SelectedSlot[i]+']['+OutgoingSlotUserList[i][j]+']":"'+ReasonID+'",';
					}
				}
				
				for (var j=0; j < HolidaySlotUserList[i].length; j++) {
					if (document.getElementById('HolidayReason['+SelectedSlot[i]+']['+HolidaySlotUserList[i][j]+']')) {
						var ReasonID = document.getElementById('HolidayReason['+SelectedSlot[i]+']['+HolidaySlotUserList[i][j]+']').value;
						EvalStr += '"HolidayReason['+SelectedSlot[i]+']['+HolidaySlotUserList[i][j]+']":"'+ReasonID+'",';
					}
				}
			}
			// checked if there is any setting
			if (SlotUserList[i].length > 0) {
				HaveSetting = true;
			}
			
			EvalStr += '"'+SelectedSlot[i]+'-OnDuty[]":SlotUserList['+i+'],';
				
			if (HolidaySlotUserList[i].length > 0) 
				EvalStr += '"'+SelectedSlot[i]+'-Holiday[]":HolidaySlotUserList['+i+'],';
			if (OutgoingSlotUserList[i].length > 0) 	
				EvalStr += '"'+SelectedSlot[i]+'-Outgoing[]":OutgoingSlotUserList['+i+'],';
			
			if (EditDuty) {
				EvalStr += '"SlotName['+SelectedSlot[i]+']":"'+$('Input#SlotName\\['+SelectedSlot[i].replace(/ /g,'\\ ')+'\\]').val()+'",';
				EvalStr += '"SlotStart['+SelectedSlot[i]+']":"'+$('Input#SlotStart\\['+SelectedSlot[i].replace(/ /g,'\\ ')+'\\]').val()+'",';
				EvalStr += '"SlotEnd['+SelectedSlot[i]+']":"'+$('Input#SlotEnd\\['+SelectedSlot[i].replace(/ /g,'\\ ')+'\\]').val()+'",';
				EvalStr += '"SlotInWavie['+SelectedSlot[i]+']":"'+$('Input#SlotInWavie\\['+SelectedSlot[i].replace(/ /g,'\\ ')+'\\]').val()+'",';
				EvalStr += '"SlotOutWavie['+SelectedSlot[i]+']":"'+$('Input#SlotOutWavie\\['+SelectedSlot[i].replace(/ /g,'\\ ')+'\\]').val()+'",';
				EvalStr += '"SlotDutyCount['+SelectedSlot[i]+']":"'+$('Input#SlotDutyCount\\['+SelectedSlot[i].replace(/ /g,'\\ ')+'\\]').val()+'",';
			}
		}
		if (EditDuty) {
			FullHolidayList = Get_Check_Box_Value('FullDay-Holiday-UserID[]','Array',true);
			for (var i=0; i< FullHolidayList.length; i++) {
				var ReasonID = document.getElementById('FullDay-Holiday-'+FullHolidayList[i]).value;
				EvalStr += '"FullHolidayReason['+FullHolidayList[i]+']":"'+ReasonID+'",';
			}
			FullOutgoingList = Get_Check_Box_Value('FullDay-Outgoing-UserID[]','Array',true);
			for (var i=0; i< FullOutgoingList.length; i++) {
				var ReasonID = document.getElementById('FullDay-Outgoing-'+FullOutgoingList[i]).value;
				EvalStr += '"FullOutgoingReason['+FullOutgoingList[i]+']":"'+ReasonID+'",';
			}
			
			if (FullHolidayList.length > 0)
				EvalStr += '"FullHolidayList[]":FullHolidayList,';
			if (FullOutgoingList.length > 0)
				EvalStr += '"FullOutgoingList[]":FullOutgoingList,';
			EvalStr += '"TimeSlotName[]":SelectedSlot,';
			EvalStr += '"TargetDate":TargetDate,';
		}
		else {
			var RepeatSelection = document.getElementById("RepeatSelection").value;
			var repeatDailyDay = document.getElementById("repeatDailyDay").value;
			var repeatWeeklyRange = document.getElementById("repeatWeeklyRange").value;
			var RepeatWeekDay = Get_Check_Box_Value("RepeatWeekDay[]",'Array');
			var repeatMonthlyRange = document.getElementById("repeatMonthlyRange").value;
			var monthlyRepeatBy = Get_Radio_Value("monthlyRepeatBy");
			var repeatYearlyRange = document.getElementById("repeatYearlyRange").value;
			EvalStr += '"TimeSlotID[]":SelectedSlot,';
			EvalStr += '"TargetDate[]":TargetDate,';
			EvalStr += '"UsingMethod":UsingMethod,';
			EvalStr += '"RepeatStart":RepeatStart,';
			EvalStr += '"RepeatEnd":RepeatEnd,';
			EvalStr += '"RepeatSelection":RepeatSelection,';
			EvalStr += '"repeatDailyDay":repeatDailyDay,';
			EvalStr += '"repeatWeeklyRange":repeatWeeklyRange,';
			EvalStr += '"RepeatWeekDay[]":RepeatWeekDay,';
			EvalStr += '"repeatMonthlyRange":repeatMonthlyRange,';
			EvalStr += '"monthlyRepeatBy":monthlyRepeatBy,';
			EvalStr += '"repeatYearlyRange":repeatYearlyRange,';
		}
		// if there is no duty, alert user to choose if he want to restore regular duty or set to non school day
		if (!EditDuty && !HaveSetting) {
			jConfirm("<?=$Lang['StaffAttendance']['SpecialDutySaveNoSettingWarning']?>","<?=$Lang['Btn']['Confirm']?>",
				function(Confirmed) {
					if (!Confirmed) { // save as non school day
						EvalStr += '"SetAsNonSchoolDay":1,';
					}
					EvalStr += '"StaffID":StaffID};';
					eval(EvalStr);
					<?php if($sys_custom['StaffAttendance']['WongFutNamCollege']){ ?>
					PostVar["AttendOne"] = $('input[name="AttendOne"]:checked').val();
					<?php } ?>
					Block_Element('GroupIndexLayer');
					TargetPage = (EditDuty)? 'ajax_save_past_duty.php':'ajax_save_duty.php';
					$.post(TargetPage,PostVar,
						function(data) {
							if (data == "die") 
								window.top.location = "/";
							else {
								Scroll_To_Top();
								Get_Return_Message(data);
								Get_Shifting_Overview(StaffID,ReturnMonth,ReturnYear);
							}
						});
				},
				"<?=$Lang['StaffAttendance']['RestoreRegularDuty']?>",
				"<?=$Lang['StaffAttendance']['SetNonSchoolDay']?>");
		}
		else {
			EvalStr += '"StaffID":StaffID};';
			eval(EvalStr);
			<?php if($sys_custom['StaffAttendance']['WongFutNamCollege']){ ?>
			PostVar["AttendOne"] = $('input[name="AttendOne"]:checked').val();
			<?php } ?>
			Block_Element('GroupIndexLayer');
			TargetPage = (EditDuty)? 'ajax_save_past_duty.php':'ajax_save_duty.php';
			$.post(TargetPage,PostVar,
				function(data) {
					if (data == "die") 
						window.top.location = "/";
					else {
						Scroll_To_Top();
						Get_Return_Message(data);
						Get_Shifting_Overview(StaffID,ReturnMonth,ReturnYear);
					}
				});
		}
	}
	else {
		document.getElementById('TargetDateWarningRow').style.display = "";
		$('span#TargetDateWarningLayer').html('<?=$Lang['StaffAttendance']['TargetDateSelectWarning']?>');
	}
}

function Get_Slot_Selection() {
	var PostVar = {
		"ID": $('input#StaffID').val(),
		"GroupOrIndividual":"Individual"
	};
	
	Block_Element('SlotSelectionLayer');
	$('div#SlotSelectionLayer').load('../../../../Settings/time_slot/ajax_get_slot_selection.php',PostVar,
		function (data) {
			if (data == "die") 
				window.top.location = '/';
			else {
				UnBlock_Element('SlotSelectionLayer');
			}
		});
}

function Check_Shifting_Slot_Setting(EditDuty) {
	var EditDuty = EditDuty || false;
	var SetupMode = Get_Radio_Value('DutySetupMode');
	
	if (EditDuty) {
		var SelectedSlot = Get_Check_Box_Value('TimeSlotName[]','Array');
	}
	else {
		if (SetupMode == "DragAndDrop") {
			var SelectedSlot = Get_Check_Box_Value('TimeSlotID[]','Array');
		}
		else { // basic
			var SelectedSlot = Get_Check_Box_Value('SlotInUseHidden[]','Array');
			var AvaliableStaff = Get_Check_Box_Value('Search-UserID[]','Array',true);
		}
	}
	
	if (SelectedSlot.length > 0 || EditDuty) {
		var SlotUserList = new Array();
		var HolidaySlotUserList = new Array();
		var OutgoingSlotUserList = new Array();
		var EvalStr = 'var PostVar = {';
		for (var i=0; i< SelectedSlot.length; i++) {
			if (SetupMode == "DragAndDrop" || EditDuty) {
				SlotUserList[i] = Get_Check_Box_Value(SelectedSlot[i]+'-OnDuty-UserID[]','Array',true);
				HolidaySlotUserList[i] = Get_Check_Box_Value(SelectedSlot[i]+'-Holiday-UserID[]','Array',true);
				OutgoingSlotUserList[i] = Get_Check_Box_Value(SelectedSlot[i]+'-Outgoing-UserID[]','Array',true);
	
				SlotUserList[i] = SlotUserList[i].concat(HolidaySlotUserList[i]).concat(OutgoingSlotUserList[i]);
				EvalStr += '"TimeSlotUser['+SelectedSlot[i]+'][]":SlotUserList['+i+'],';
			}
			else {
				OnDutyArrayStr = '';
				for (var j=0; j< AvaliableStaff.length; j++) {
					var StaffSelectedDuty = document.getElementById('DutySelect'+SelectedSlot[i]+'-'+AvaliableStaff[j]).value;
					if (StaffSelectedDuty != "NoSetting") {
						OnDutyArrayStr += AvaliableStaff[j] + ',';
					}
				}
				
				if (OnDutyArrayStr.substr((OnDutyArrayStr.length-1)) == ',') {
					OnDutyArrayStr = OnDutyArrayStr.substr(0,(OnDutyArrayStr.length-1));
				}
				
				SlotUserList[i] = (OnDutyArrayStr != "")? OnDutyArrayStr.split(','):Array();
				EvalStr += '"TimeSlotUser['+SelectedSlot[i]+'][]":SlotUserList['+i+'],';
			}
			
			if (EditDuty) {
				EvalStr += '"SlotStart['+SelectedSlot[i]+']":"'+$('Input#SlotStart\\['+SelectedSlot[i].replace(/ /g,'\\ ')+'\\]').val()+'",';
				EvalStr += '"SlotEnd['+SelectedSlot[i]+']":"'+$('Input#SlotEnd\\['+SelectedSlot[i].replace(/ /g,'\\ ')+'\\]').val()+'",';
			}
		}
		if (EditDuty) {
			EvalStr += '"TimeSlotName[]":SelectedSlot,';
		}
		else {
			EvalStr += '"TimeSlotID[]":SelectedSlot,';
		}
		EvalStr += '"EditDuty":'+((EditDuty)? '1':'0')+'};';
		eval(EvalStr);
		
		$.post('../ajax_check_duty_period_user_time_slot.php',PostVar,
			function(data) {
				if (data == "die") 
					window.top.location = "/";
				else {
					var Result = data.split('|=|');
					
					if (Result[0] == '1') {
						Save_Slot_Setting(EditDuty);
					}
					else {
						var DutyOver24Hour = Result[1].split(',');
						var TimeSlotOverlap = Result[2].split(',');
						var AlertContent = '';
						
						AlertContent += '<?=$Lang['StaffAttendance']['DutyOver24HourWarning']?>\n';
						if (Result[1] != "") {
							for (var i=0; i< DutyOver24Hour.length; i++) {
								if (DutyOver24Hour[i] != "")
									AlertContent += document.getElementById('UserName['+DutyOver24Hour[i]+']').value+', ';
							}
						}
						else {
							AlertContent += 'N/A';
						}
						
						AlertContent += '\n\n';
						AlertContent += '<?=$Lang['StaffAttendance']['DutyOverlapWarning']?>\n';
						if (Result[2] != "") {
							for (var i=0; i< TimeSlotOverlap.length; i++) {
								if (TimeSlotOverlap[i] != "")
									AlertContent += document.getElementById('UserName['+TimeSlotOverlap[i]+']').value+', ';
							}
						}
						else {
							AlertContent += 'N/A';
						}
						
						AlertContent += '\n\n';
						AlertContent += '<?=$Lang['StaffAttendance']['DutyWarning']?>\n';
						
						alert(AlertContent);
					}
				}
			});
	}
	else {
		Save_Slot_Setting(EditDuty);
		/*alert('<?=$Lang['StaffAttendance']['DutyApplyDuty']?>');*/
	}
}

function Get_Drop_Down_Mode_Table() {
	var PostVar = {
		"TargetDate":$('input#OriginalTargetDate').val(),
		"ID":$('input#StaffID').val() 
	};
	
	$('#DropDownLayer').html('');
	Block_Element('DropDownLayer');
	$('div#DropDownLayer').load('ajax_get_shifting_group_drop_down_setting_table.php',PostVar,
		function (data) {
			if (data == "die") 
				window.top.location = "/";
			else {
				UnBlock_Element('DropDownLayer');
			}
		});
}
}

// dom function 
{
function Calculate_Unassign_List() {
	// dummy function to make compactabke to group setting
}
	
function Remove_Slot(SlotName) {
	if (confirm('<?=$Lang['StaffAttendance']['EditDutyRemoveSlotWarning']?>')) {
		var TimeSlotTable = document.getElementById('TimeSlotTable');
		var TimeSlotTableCells = TimeSlotTable.tBodies[0].rows[0].cells;
				
		for (var j=0; j< TimeSlotTableCells.length; j++) {
			if (TimeSlotTableCells[j].id == SlotName) {
				CellIndex = j;		
				break;
			}
		}
		
		for (var i=0; i< TimeSlotTable.tBodies[0].rows.length; i++) {
			TimeSlotTable.tBodies[0].rows[i].deleteCell(CellIndex);
		}
	}
}

function Check_Slot_Period() {
	var StartHour = Get_Selection_Value('StartHour','String');
	var StartMin = Get_Selection_Value('StartMin','String');
	var StartSec = Get_Selection_Value('StartSec','String');
	var EndHour = Get_Selection_Value('EndHour','String');
	var EndMin = Get_Selection_Value('EndMin','String');
	var EndSec = Get_Selection_Value('EndSec','String');
	var SlotStart = StartHour+":"+StartMin+":"+StartSec;
	var SlotEnd = EndHour+":"+EndMin+":"+EndSec;
	var ElementRow = document.getElementById('SlotTimeWarningRow');
	var ElementLayer = $('div#SlotTimeWarningLayer');
	
	if (SlotStart == SlotEnd) { // time slot must not exceed 24 hours
		ElementLayer.html('<?=$Lang['StaffAttendance']['SlotTimeIntervalLimitWarning']?>');
		ElementRow.style.display = '';
		return false;
	}
	
	ElementLayer.html('');
	ElementRow.style.display = 'none';
}
	
function Check_Slot_Name() {
	EditDuty = $('input#EditDuty').val();
	TimeSlotNameList = Get_Check_Box_Value('TimeSlotName[]','Array');
	FormSlotID = $('input#SlotID').val();
	FormSlotName = $('input#SlotName').val();
	ElementObj = $('div#SlotNameWarningLayer');
	
	if (DOM_Check_Slot_Name(FormSlotName,'SlotNameWarningLayer','SlotNameWarningRow')) {
		if (EditDuty == "1") {
			for (var i=0; i< TimeSlotNameList.length; i++) {
				if (TimeSlotNameList[i] != FormSlotID) {
					if ($('input#SlotName\\['+TimeSlotNameList[i].replace(/ /g,'\\ ')+'\\]').val() == Trim(FormSlotName)) {
						ElementObj.html('<?=$Lang['StaffAttendance']['SlotNameWarning']?>');
						document.getElementById('SlotNameWarningRow').style.display = '';
						return;
					}
				}
			}
			
			ElementObj.html('');
			document.getElementById('SlotNameWarningRow').style.display = 'none';
		}
		else {
			var PostVar = {
				"TargetUserID":$('input#StaffID').val(),
				"SlotName":encodeURIComponent(FormSlotName)
			}
			
			$.post('../../../../Settings/time_slot/ajax_check_slot_name.php',PostVar,
				function (data) {
					if (data == "die") 
						window.top.location = '/';
					else if (data == "1") {
						ElementObj.html('');
						document.getElementById('SlotNameWarningRow').style.display = 'none';
					}
					else {
						ElementObj.html('<?=$Lang['StaffAttendance']['SlotNameWarning']?>');
						document.getElementById('SlotNameWarningRow').style.display = '';
					}
				});
		}
	}
}
	
function Save_Slot() {
	SlotJustCheck = 0;
	Check_Slot_Period();
	Check_Slot_Name();
	Check_Day_Count();
	var EditDuty = $('input#EditDuty').val();
	var GoProcess = false;
	if (EditDuty == "1") {
		Real_Save_Slot();
	}
	else {
		$(document).ajaxComplete(function(e, xhr, settings) {
			if (settings.url.indexOf('ajax_check_slot_name.php') != -1 && SlotJustCheck == 0) {
				Real_Save_Slot();
				SlotJustCheck = 1;
			}
		});
	}
}
	
function Real_Save_Slot() {
	var NameWarning = $('div#SlotNameWarningLayer').html();
	var TimeWarning = $('div#SlotTimeWarningLayer').html();
	var DayCountWarning = $('div#DutyCountWarningLayer').html();

	if (Trim(NameWarning) == "" && Trim(TimeWarning) == "" && Trim(DayCountWarning) == "") {
		FormSlotID = $('input#SlotID').val();
		FormSlotID = FormSlotID.replace(/ /g,'\\ ');
		
		if (FormSlotID != "") {
			FormSlotStartHour = Get_Selection_Value('StartHour','String');
			FormSlotStartMin = Get_Selection_Value('StartMin','String');
			FormSlotStartSec = Get_Selection_Value('StartSec','String');
			FormSlotStart = FormSlotStartHour + ':' + FormSlotStartMin + ':' + FormSlotStartSec;
			FormSlotEndHour = Get_Selection_Value('EndHour','String');
			FormSlotEndMin = Get_Selection_Value('EndMin','String');
			FormSlotEndSec = Get_Selection_Value('EndSec','String');
			FormSlotEnd = FormSlotEndHour + ':' + FormSlotEndMin + ':' + FormSlotEndSec;
			FormSlotName = Trim($('input#SlotName').val());
			
			$('input#SlotName\\['+FormSlotID+'\\]').val(FormSlotName);
			$('input#SlotStart\\['+FormSlotID+'\\]').val(FormSlotStart);
			$('input#SlotEnd\\['+FormSlotID+'\\]').val(FormSlotEnd);
			$('input#SlotInWavie\\['+FormSlotID+'\\]').val((($('input#InWavie').attr('checked'))? '1':'0'));
			$('input#SlotOutWavie\\['+FormSlotID+'\\]').val((($('input#OutWavie').attr('checked'))? '1':'0'));
			$('input#SlotDutyCount\\['+FormSlotID+'\\]').val($('input#DutyCount').val());
			
			document.getElementById('DisplaySlotNameLayer['+$('input#SlotID').val()+']').innerHTML = FormSlotName;
		}
		else {
			var EditDuty = $('input#EditDuty').val();
			
			if (EditDuty == "1") 
				Apply_Time_Slot(true);
			else {
				var StartHour = Get_Selection_Value('StartHour','String');
				var StartMin = Get_Selection_Value('StartMin','String');
				var StartSec = Get_Selection_Value('StartSec','String');
				var EndHour = Get_Selection_Value('EndHour','String');
				var EndMin = Get_Selection_Value('EndMin','String');
				var EndSec = Get_Selection_Value('EndSec','String');
				var SlotStart = StartHour+":"+StartMin+":"+StartSec;
				var SlotEnd = EndHour+":"+EndMin+":"+EndSec;
				var GroupID = $('input#GroupID').val();
				var TargetUserID = $('input#TargetUserID').val();
				var PostVar = {
					"SlotName": encodeURIComponent($('input#SlotName').val()),
					"GroupID": GroupID,
					"SlotStart": encodeURIComponent(SlotStart),
					"SlotEnd": encodeURIComponent(SlotEnd),
					"InWavie": document.getElementById('InWavie').checked,
					"OutWavie": document.getElementById('OutWavie').checked,
					"DutyCount": encodeURIComponent($('input#DutyCount').val()),
					"TargetUserID": TargetUserID
				};
				
				Block_Thickbox();
				$.post('../../../../Settings/time_slot/ajax_save_slot.php',PostVar,
					function (data) {
						if (data == "die") 
							window.top.location = '/';
						else {
							Get_Return_Message(data);

							var temp = data.split('|=|');
							var SlotID = temp[2];
							var FullUserList = document.getElementsByName('Full-UserID[]');
							var DragAndDropLayer = document.getElementById('DragAndDropSessionLayer');
							for (var i=0; i< FullUserList.length; i++) {
								var UserID = FullUserList[i].value;
								HiddenNode = document.createElement('input');
								HiddenNode.setAttribute("type", "hidden");
								HiddenNode.setAttribute("value", "");
								HiddenNode.setAttribute("name", SlotID+'-Outgoing-'+UserID);
								HiddenNode.setAttribute("id", SlotID+'-Outgoing-'+UserID);
								DragAndDropLayer.appendChild(HiddenNode);
								
								HiddenNode = document.createElement('input');
								HiddenNode.setAttribute("type", "hidden");
								HiddenNode.setAttribute("value", "");
								HiddenNode.setAttribute("name", SlotID+'-Holiday-'+UserID);
								HiddenNode.setAttribute("id", SlotID+'-Holiday-'+UserID);
								DragAndDropLayer.appendChild(HiddenNode);
							}
							
							Get_Slot_Selection();
							Get_Drop_Down_Mode_Table();
							
							window.top.tb_remove();
							Scroll_To_Top();
						}
					});
			}
		}
		
		window.top.tb_remove();
	}
}

function Get_Full_Member_List() {
	var FullOutgoingList = Get_Check_Box_Value('FullDay-Outgoing-UserID[]','Array',true);
	var FullHolidayList = Get_Check_Box_Value('FullDay-Holiday-UserID[]','Array',true);
	var UnAvalList = Get_Check_Box_Value('UnAval-UserID[]','Array',true);
	var UnAvaliableUserList = FullOutgoingList.concat(FullHolidayList).concat(UnAvalList);

	if (UnAvaliableUserList.length > 0) {
		$('div#IndividuallFullMemberLayer').css('visibility','hidden');
	}
	else {
		$('div#IndividuallFullMemberLayer').css('visibility','visible');
	}
}

function Check_Full_Outgoing_Holiday() {
	var FullMemberList = Get_Check_Box_Value('Full-UserID[]','Array',true);
	if (document.getElementsByName('TimeSlotName[]')) 
		var ActiveTimeSlotList = Get_Check_Box_Value('TimeSlotName[]','Array');
	else 
		var ActiveTimeSlotList = Get_Check_Box_Value('TimeSlotID[]','Array');
	var FullOutgoingList = Get_Check_Box_Value('FullDay-Outgoing-UserID[]','Array',true);
	var FullHolidayList = Get_Check_Box_Value('FullDay-Holiday-UserID[]','Array',true);
	
	for (var i=0; i< ActiveTimeSlotList.length; i++) {
		var UserList = Get_Check_Box_Value(ActiveTimeSlotList[i]+'-OnDuty-UserID[]','Array',true);
		var OutgoingUserList = Get_Check_Box_Value(ActiveTimeSlotList[i]+'-Outgoing-UserID[]','Array',true);
		var HolidayUserList = Get_Check_Box_Value(ActiveTimeSlotList[i]+'-Holiday-UserID[]','Array',true);
		
		OnDutyContent = '';
		OutgoingContent = '';
		HolidayContent = '';
		OnDutyCount = 0;
		OutgoingCount = 0;
		HolidayCount = 0;
		for (var j=0; j< FullMemberList.length; j++) {
			// onduty
			if (!jIN_ARRAY(FullOutgoingList,FullMemberList[j]) && !jIN_ARRAY(FullHolidayList,FullMemberList[j]) && jIN_ARRAY(UserList,FullMemberList[j])) {
				OnDutyContent += Get_Slot_Content_Html(FullMemberList[j],ActiveTimeSlotList[i],'OnDuty');
				OnDutyCount++;
			}
			
			// outgoing
			if (!jIN_ARRAY(FullOutgoingList,FullMemberList[j]) && !jIN_ARRAY(FullHolidayList,FullMemberList[j]) && jIN_ARRAY(OutgoingUserList,FullMemberList[j])) {
				OutgoingContent += Get_Slot_Content_Html(FullMemberList[j],ActiveTimeSlotList[i],'Outgoing');
				OutgoingCount++;
			}
			
			// holiday
			if (!jIN_ARRAY(FullOutgoingList,FullMemberList[j]) && !jIN_ARRAY(FullHolidayList,FullMemberList[j]) && jIN_ARRAY(HolidayUserList,FullMemberList[j])) {
				HolidayContent += Get_Slot_Content_Html(FullMemberList[j],ActiveTimeSlotList[i],'Holiday');
				HolidayCount++;
			}
		}
				
		$('div#TimeSlot-'+ActiveTimeSlotList[i]+'-OnDuty').html(OnDutyContent);
		$('div#TimeSlot-'+ActiveTimeSlotList[i]+'-Outgoing').html(OutgoingContent);
		$('div#TimeSlot-'+ActiveTimeSlotList[i]+'-Holiday').html(HolidayContent);
		
		document.getElementById(ActiveTimeSlotList[i]+'OnDutyCountLayer').innerHTML = '('+OnDutyCount+')';
		document.getElementById(ActiveTimeSlotList[i]+'OutgoingCountLayer').innerHTML = '('+OutgoingCount+')';
		document.getElementById(ActiveTimeSlotList[i]+'HolidayCountLayer').innerHTML = '('+HolidayCount+')';
	}
}

function Show_Reason_Layer(Obj) {
	if (document.all) {
		Obj.nextSibling.style.visibility = "visible";
		Obj.nextSibling.firstChild.focus();
	}
	else {
		Obj.nextSibling.nextSibling.style.visibility = "visible";
		Obj.nextSibling.nextSibling.firstChild.nextSibling.focus();
	}
}
	
function Check_Reason_Icon(SelectObj) {
	if (SelectObj.options[SelectObj.selectedIndex].value == "") {
		if (document.all)
			SelectObj.parentNode.previousSibling.className = "reason_alert";
		else 
			SelectObj.parentNode.previousSibling.previousSibling.className = "reason_alert";
	}
	else {
		if (document.all)
			SelectObj.parentNode.previousSibling.className = "reason";
		else 
			SelectObj.parentNode.previousSibling.previousSibling.className = "reason";
	}
	
	var SlotStaffReasonLayer = $('#SlotStaffReasonLayer');
	var ObjID = SelectObj.id.split('-');
	if(document.getElementById(ObjID[0]+'-'+ObjID[1]+'-'+ObjID[4]))
	{
		document.getElementById(ObjID[0]+'-'+ObjID[1]+'-'+ObjID[4]).value = SelectObj.options[SelectObj.selectedIndex].value;
	}else{
		// create the hidden reason element
		var id_name = ObjID[0]+'-'+ObjID[1]+'-'+ObjID[4];
		var val = SelectObj.options[SelectObj.selectedIndex].value;
		SlotStaffReasonLayer.append('<input type="hidden" name="'+id_name+'" id="'+id_name+'" value="'+val+'" />');
	}
	SelectObj.parentNode.style.visibility = "hidden";
}
	
function Check_All(CheckStatus,ObjClassName) {
	$('.'+ObjClassName).each(
		function () {
			this.checked = CheckStatus;
		}
	);
}
	
function Select_DeSelect_All(ColumnType,ColumnNature) {
	var ColumnNature = ColumnNature || "";
	if (ColumnType == "UnAssign") {
		var CheckBoxObj = document.getElementsByName('UnAssign-UserID[]');
	}
	else if (ColumnType == "Full") {
		var CheckBoxObj = document.getElementsByName('Search-UserID[]');
	}
	else {
		if (ColumnNature == "OnDuty") {
			var CheckBoxObj = document.getElementsByName(ColumnType+'-OnDuty-UserID[]');
		}
		else if (ColumnNature == "Outgoing") {
			var CheckBoxObj = document.getElementsByName(ColumnType+'-Outgoing-UserID[]');
		}
		else {
			var CheckBoxObj = document.getElementsByName(ColumnType+'-Holiday-UserID[]');
		}
	}
	
	var AnyUnChecked=false;
	for (var i=0; i< CheckBoxObj.length; i++) {
		if (!CheckBoxObj[i].checked) {
			AnyUnChecked = true;
			CheckBoxObj[i].checked = true;
			CheckBoxObj[i].parentNode.style.backgroundColor = '#A6A6A6';
		}
	}
	
	if (!AnyUnChecked) {
		for (var i=0; i< CheckBoxObj.length; i++) {
			CheckBoxObj[i].checked = false;
			CheckBoxObj[i].parentNode.style.backgroundColor = '';
		}
	}
}
	
function Apply_Time_Slot(EditDuty) {
	var EditDuty = EditDuty || false;
	
	var TimeSlot = Get_Selection_Value('TimeSlot[]','Array');
	var TimeSlotTable = document.getElementById('TimeSlotTable');
	var CellHeight = document.getElementById('FullStaffListLayer').offsetHeight/3;
	var TimeSlotTableCells = TimeSlotTable.tBodies[0].rows[0].cells;
	
	if (EditDuty) {
		if ((TimeSlotTableCells.length + 1) > <?=$StaffAttend3->MaxSlotPerSetting?>) {
				alert('<?=$Lang['StaffAttendance']['MaxSlotPerSettingWarning1'].$StaffAttend3->MaxSlotPerSetting.$Lang['StaffAttendance']['MaxSlotPerSettingWarning2']?>');
		}
		else {
			if (TimeSlotTableCells.length == 0) {
				CellIndex = -1;
			}
			else {
				FormSlotStartHour = Get_Selection_Value('StartHour','String');
				FormSlotStartMin = Get_Selection_Value('StartMin','String');
				FormSlotStartSec = Get_Selection_Value('StartSec','String');
				var NewSlotStart = FormSlotStartHour + ':' + FormSlotStartMin + ':' + FormSlotStartSec;
						
				for (var j=0; j< TimeSlotTableCells.length; j++) {
					var CurrSlotStart = document.getElementById('SlotStart['+TimeSlotTableCells[j].id+']').value;
					
					if (NewSlotStart <= CurrSlotStart) {
						CellIndex = j;
						
						break;
					}
					else 
						CellIndex = -1;
				}
			}
			
			Get_Slot_Column(CellIndex,$('input#SlotName').val(),EditDuty);
		}
	}
	else {
		// get current applied slots
		var TotalSlotSelected = 0;
		for (var j=0; j< TimeSlotTableCells.length; j++) {
			if (document.getElementById('TimeSlotID['+TimeSlotTableCells[j].id+']').checked) {
				TotalSlotSelected++;
			}
		}
		
		// minus already applyed slots
		for (var i=0; i< TimeSlot.length; i++) {
			if (document.getElementById('TimeSlotID['+TimeSlot[i]+']')) {
				TotalSlotSelected--;
			}
		}

		// check if the total slots going to apply is greater then system limit, block the action and prompt user
		if ((TimeSlot.length + TotalSlotSelected) > <?=$StaffAttend3->MaxSlotPerSetting?>) {
			alert('<?=$Lang['StaffAttendance']['MaxSlotPerSettingWarning1'].$StaffAttend3->MaxSlotPerSetting.$Lang['StaffAttendance']['MaxSlotPerSettingWarning2']?>');
		}
		else {	
			// drop down approach
			var slotIdToLoad = [];
			for (var i=0; i< TimeSlot.length; i++) {
				if($('.SlotCell'+TimeSlot[i]).length > 0)
				{
					$('.SlotCell'+TimeSlot[i]).css('display','');
					document.getElementById('SlotInUseHidden['+TimeSlot[i]+']').checked = true;
				}else{
					// load new time slot tables
					slotIdToLoad.push(TimeSlot[i]);
				}
			}
			if(slotIdToLoad.length > 0){
				$('#ApplyTimeSlotButton').attr('disabled', true);
				$.post(
					'ajax_get_shifting_group_slot_table.php',
					{
						'ID': $('#StaffID').val(),
						'TargetDate': $('#OriginalTargetDate').val(),
						'SlotID[]': slotIdToLoad
					},
					function(return_html){
						$('#DropDownLayer').append(return_html);
						$('#ApplyTimeSlotButton').attr('disabled', false);
					}
				);
			}
			// end of drop down approach
			
			// drag and drop approach
			for (var i=0; i< TimeSlot.length; i++) {
				if (document.getElementById('TimeSlotID['+TimeSlot[i]+']')) {
					$('.TimeSlotColumn-'+TimeSlot[i]).css('display',''); 
					document.getElementById('TimeSlotID['+TimeSlot[i]+']').checked = true;
				}
				else {
					if (TimeSlotTableCells.length == 0) {
						CellIndex = 0;
					}
					else {
						var NewSlotStart = document.getElementById('SlotStart['+TimeSlot[i]+']').value;
						
						for (var j=0; j< TimeSlotTableCells.length; j++) {
							var CurrSlotStart = document.getElementById('SlotStart['+TimeSlotTableCells[j].id+']').value;
							
							if (NewSlotStart <= CurrSlotStart) {
								CellIndex = j;
								
								break;
							}
							else 
								CellIndex = -1;
						}
					}
					
					Get_Slot_Column(CellIndex,TimeSlot[i],EditDuty);
				}
			}
			// end of drag and drop approach
		}
		
		if (TimeSlot.length > 0) {
			var SetupMode = Get_Radio_Value("DutySetupMode");
			if (SetupMode == "Basic") {
				$('#DropDownLayer').css('display','block');
			}
			else {
				$('#DragAndDropSessionLayer').css('display','block');
			}
			$('#SetupLayer').css('display','block');
			$('#SubmitButtonLayer').css('display','block');
		}
	}
	
	Thick_Box_Init();
	Drag_And_Drop_Init();
}

function Get_Slot_Column(CellIndex,TimeSlotID,EditDuty) {
	TimeSlotID = Trim(TimeSlotID);
	var FullUserList = Get_Check_Box_Value('Full-UserID[]','Array',true);
	var TimeSlotTable = document.getElementById('TimeSlotTable');
	//var CellHeight = document.getElementById('FullStaffListLayer').offsetHeight/3;
	if (EditDuty) {
		var JQuerySlotName = TimeSlotID.replace(/ /g,'\\ ');
		var FormSlotStartHour = Get_Selection_Value('StartHour','String');
		var FormSlotStartMin = Get_Selection_Value('StartMin','String');
		var FormSlotStartSec = Get_Selection_Value('StartSec','String');
		var FormSlotStart = '00:00:00';
		if(FormSlotStartHour && FormSlotStartHour != '')
		{
			FormSlotStart = FormSlotStartHour + ':' + FormSlotStartMin + ':' + FormSlotStartSec;
		}else if($('input#SlotStart\\['+JQuerySlotName+'\\]').length > 0){
			FormSlotStart = $('input#SlotStart\\['+JQuerySlotName+'\\]').val();
		}
		var FormSlotEndHour = Get_Selection_Value('EndHour','String');
		var FormSlotEndMin = Get_Selection_Value('EndMin','String');
		var FormSlotEndSec = Get_Selection_Value('EndSec','String');
		var FormSlotEnd = '00:00:00';
		if(FormSlotEndHour && FormSlotEndHour != '')
		{
			FormSlotEnd = FormSlotEndHour + ':' + FormSlotEndMin + ':' + FormSlotEndSec;
		}else if($('input#SlotEnd\\['+JQuerySlotName+'\\]').length > 0){
			FormSlotEnd = $('input#SlotEnd\\['+JQuerySlotName+'\\]').val();
		}
		var FormSlotInWavie = '0';
		var FormSlotOutWavie = '0';
		var FormSlotDutyCount = '1';
		if($('input#InWavie').length > 0){
			FormSlotInWavie = ($('input#InWavie').attr('checked'))? '1':'0';
		}else if($('input#SlotInWavie\\['+JQuerySlotName+'\\]').length > 0){
			FormSlotInWavie = $('input#SlotInWavie\\['+JQuerySlotName+'\\]').val();
		}
		if($('input#OutWavie').length > 0){
			FormSlotOutWavie = ($('input#OutWavie').attr('checked'))? '1':'0';
		}else if($('input#SlotOutWavie\\['+JQuerySlotName+'\\]').length > 0){
			FormSlotOutWavie = $('input#SlotOutWavie\\['+JQuerySlotName+'\\]').val();
		}
		if($('input#DutyCount').length > 0){
			FormSlotDutyCount = $('input#DutyCount').val();
		}else if($('input#SlotDutyCount\\['+JQuerySlotName+'\\]').length > 0){
			FormSlotDutyCount = $('input#SlotDutyCount\\['+JQuerySlotName+'\\]').val();
		}
		
		Content = '<input name="TimeSlotName[]" id="TimeSlotName['+TimeSlotID+']" type="checkbox" value="'+TimeSlotID+'" checked="checked" style="display:none;"/>';
		Content += '<input type="hidden" name="SlotName['+TimeSlotID+']" id="SlotName['+TimeSlotID+']" value="'+TimeSlotID+'">';
		Content += '<input type="hidden" name="SlotStart['+TimeSlotID+']" id="SlotStart['+TimeSlotID+']" value="'+FormSlotStart+'">';
		Content += '<input type="hidden" name="SlotEnd['+TimeSlotID+']" id="SlotEnd['+TimeSlotID+']" value="'+FormSlotEnd+'">';
		Content += '<input type="hidden" name="SlotInWavie['+TimeSlotID+']" id="SlotInWavie['+TimeSlotID+']" value="'+FormSlotInWavie+'">';
		Content += '<input type="hidden" name="SlotOutWavie['+TimeSlotID+']" id="SlotOutWavie['+TimeSlotID+']" value="'+FormSlotOutWavie+'">';
		Content += '<input type="hidden" name="SlotDutyCount['+TimeSlotID+']" id="SlotDutyCount['+TimeSlotID+']" value="'+FormSlotDutyCount+'">';
		
		for (var i=0; i< FullUserList.length; i++) {
			Content += '<input type="hidden" name="'+TimeSlotID+'-Outgoing-'+FullUserList[i]+'" id="'+TimeSlotID+'-Outgoing-'+FullUserList[i]+'" value="">';
			Content += '<input type="hidden" name="'+TimeSlotID+'-Holiday-'+FullUserList[i]+'" id="'+TimeSlotID+'-Holiday-'+FullUserList[i]+'" value="">';
		}
		
		SlotName = TimeSlotID;
	}
	else {
		Content = '<input name="TimeSlotID[]" id="TimeSlotID['+TimeSlotID+']" type="checkbox" value="'+TimeSlotID+'" checked="checked" style="display:none;"/>';
		SlotName = document.getElementById('SlotName['+TimeSlotID+']').value;
	}
	var cell1 = TimeSlotTable.tBodies[0].rows[0].insertCell(CellIndex);
	Content += '<span style="float:left;" id="DisplaySlotNameLayer['+TimeSlotID+']">';
	Content += SlotName;
	Content += '</span>';
	Content += '<span style="float:right;" class="table_row_tool row_content_tool">';
	if (EditDuty) {
		Content += '<a href="#TB_inline?height=450&width=750&inlineId=FakeLayer" onclick="Edit_Time_Slot(\''+TimeSlotID+'\');" class="thickbox edit" title="<?=$Lang['StaffAttendance']['EditTimeSlot']?>"></a>';
		Content += '<a onclick="Remove_Slot(\''+SlotName+'\'); Calculate_Unassign_List();  return false;" href="#" class="delete" title="<?=$Lang['Btn']['Remove']?>"></a>';
	}
	else {
		Content += '<a onclick="$(\'.TimeSlotColumn-'+TimeSlotID+'\').hide(); document.getElementById(\'TimeSlotID['+TimeSlotID+']\').checked = false;" href="#" class="delete" title="<?=$Lang['Btn']['Remove']?>"></a>';
	}
	Content += '</span>';
	cell1.className = 'sub_row_top TimeSlotColumn-'+TimeSlotID;
	cell1.style.backgroundColor = '#7E7E7E';
	cell1.id = TimeSlotID;
	cell1.innerHTML = Content;
	
	// on duty
	var cell2 = TimeSlotTable.tBodies[0].rows[1].insertCell(CellIndex);
	Content =	'<div class="roster_slot_tool"> ';
	Content +=	'<span style="float:left;">';
	Content +=	'<strong style="color:#000000;"><?=$Lang['StaffAttendance']['OnDuty']?> <font id="'+TimeSlotID+'OnDutyCountLayer">(0)</font></strong>';
	Content +=	'</span>';
	Content +=	'<span style="float:right;" class="row_content_tool">';
	Content +=  '<a href="#" style="color:#000000;" onmouseover="this.style.color = \'#FF0000\';" onmouseout="this.style.color = \'#000000\';" onclick="Select_DeSelect_All(\''+TimeSlotID+'\',\'OnDuty\'); return false;"><?=$Lang['StaffAttendance']['SelectDeSelectAll']?></a>';
	Content +=	'</span>';
	Content +=	'</div>';
	cell2.className = 'TimeSlotColumn-'+TimeSlotID;
	cell2.innerHTML = Content;
	
	
	var cell3 = TimeSlotTable.tBodies[0].rows[2].insertCell(CellIndex);
	cell3.className = 'TimeSlotCell TimeSlotColumn-'+TimeSlotID;
	Content = '<div id="TimeSlot-'+TimeSlotID+'-OnDuty" class="TimeSlotDragBlock TimeSlotDropBlock roster_staff_item roster_staff_item_dayoff" style="min-height:150px; height: expression(this.scrollHeight < 151 ? \'150px\':\'auto\');"></div>';
	cell3.innerHTML = Content;
	
	// outgoing
	var cell4 = TimeSlotTable.tBodies[0].rows[3].insertCell(CellIndex);
	Content =	'<div class="roster_slot_tool"> ';
	Content +=	'<span style="float:left;">';
	Content +=	'<strong style="color:#000000;"><?=$Lang['StaffAttendance']['Outgoing']?> <font id="'+TimeSlotID+'OutgoingCountLayer">(0)</font></strong>';
	Content +=	'</span>';
	Content +=	'<span style="float:right;" class="row_content_tool">';
	Content +=  '<a href="#" style="color:#000000;" onmouseover="this.style.color = \'#FF0000\';" onmouseout="this.style.color = \'#000000\';" onclick="Select_DeSelect_All(\''+TimeSlotID+'\',\'Outgoing\'); return false;"><?=$Lang['StaffAttendance']['SelectDeSelectAll']?></a>';
	Content +=	'</span>';
	Content +=	'</div>';
	cell4.className = 'roster_outing TimeSlotColumn-'+TimeSlotID;
	cell4.innerHTML = Content;
	
	var cell5 = TimeSlotTable.tBodies[0].rows[4].insertCell(CellIndex);
	cell5.className = 'roster_outing TimeSlotCell TimeSlotColumn-'+TimeSlotID;
	Content = '<div id="TimeSlot-'+TimeSlotID+'-Outgoing" class="OutgoingTimeSlotDragBlock OutgoingTimeSlotDropBlock roster_staff_item roster_staff_item_dayoff" style="min-height:150px; height: expression(this.scrollHeight < 151 ? \'150px\':\'auto\');"></div>';
	cell5.innerHTML = Content;
	
	// holiday
	var cell6 = TimeSlotTable.tBodies[0].rows[5].insertCell(CellIndex);
	Content =	'<div class="roster_slot_tool"> ';
	Content +=	'<span style="float:left;">';
	Content +=	'<strong style="color:#000000;"><?=$Lang['StaffAttendance']['Leave']?> <font id="'+TimeSlotID+'HolidayCountLayer">(0)</font></strong>';
	Content +=	'</span>';
	Content +=	'<span style="float:right;" class="row_content_tool">';
	Content +=  '<a href="#" style="color:#000000;" onmouseover="this.style.color = \'#FF0000\';" onmouseout="this.style.color = \'#000000\';" onclick="Select_DeSelect_All(\''+TimeSlotID+'\',\'Holiday\'); return false;"><?=$Lang['StaffAttendance']['SelectDeSelectAll']?></a>';
	Content +=	'</span>';
	Content +=	'</div>';
	cell6.className = 'roster_leave TimeSlotColumn-'+TimeSlotID;
	cell6.innerHTML = Content;
  
  var cell7 = TimeSlotTable.tBodies[0].rows[6].insertCell(CellIndex);
	cell7.className = 'roster_leave TimeSlotCell TimeSlotColumn-'+TimeSlotID;
	Content = '<div id="TimeSlot-'+TimeSlotID+'-Holiday" class="HolidayTimeSlotDragBlock HolidayTimeSlotDropBlock roster_staff_item roster_staff_item_dayoff" style="min-height:150px; height: expression(this.scrollHeight < 151 ? \'150px\':\'auto\');"></div>';
	cell7.innerHTML = Content;
}

var OrgColWidth = 0;
function Toggle_Full_Member_Layer() {
	var FullMemberLayer = document.getElementById('FullMemberPanelTable');
	if (FullMemberLayer.style.display == "none") {
		FullMemberLayer.style.display = '';
		//document.getElementById('TimeSlotCell').style.width = OrgColWidth + 'px';
		document.getElementById('TimeSlotLayer').style.width = OrgColWidth + 'px';
	}
	else {
		OrgColWidth = document.getElementById('TimeSlotCell').offsetWidth;
		
		FullMemberLayer.style.display = 'none';
		var ColWidth = document.getElementById('TimeSlotCell').offsetWidth;
		document.getElementById('TimeSlotLayer').style.width = ColWidth + 'px';
	}
}

function Change_Background_Color(CheckBoxID,DivObj) {
	var Temp = CheckBoxID.split('-');
	var CheckboxObj = document.getElementById(CheckBoxID);

	if (CheckboxObj.checked) {
		//alert('-');
		CheckboxObj.checked = false;
		DivObj.style.backgroundColor = '';
	}
	else {
		//alert('+');
		CheckboxObj.checked = true;
		DivObj.style.backgroundColor = '#A6A6A6';
	}		
}

function Calculate_Other_TimeSlot_List(TimeSlotID,ColumnNature,DragColumnNature) {
	var SelectedUserList = new Array();
	var OnDutyUserList = Get_Check_Box_Value(TimeSlotID+'-OnDuty-UserID[]','Array',true);
	var OutgoingUserList = Get_Check_Box_Value(TimeSlotID+'-Outgoing-UserID[]','Array',true);
	var HolidayUserList = Get_Check_Box_Value(TimeSlotID+'-Holiday-UserID[]','Array',true);
	var FullMemberList = Get_Check_Box_Value('Full-UserID[]','Array',true);
	
	
	if (ColumnNature == "OnDuty") {
		var CheckUserList = OnDutyUserList;
	}
	else if (ColumnNature == "Outgoing"){
		var CheckUserList = OutgoingUserList;
	}
	else {
		var CheckUserList = HolidayUserList;
	}
	
	if (ColumnNature == "OnDuty" || ColumnNature == "Holiday") {
		// process outgoing list
		for (var j=0; j< OutgoingUserList.length; j++) {
			if (!jIN_ARRAY(CheckUserList,OutgoingUserList[j])) {
				SelectedUserList[OutgoingUserList[j]] = OutgoingUserList[j];
			}
		}
		
		Content = '';
		Count = 0;
		for (var i=0; i< FullMemberList.length; i++) {
			if (jIN_ARRAY(SelectedUserList,FullMemberList[i])) {
				var UserID = FullMemberList[i];
				
				Content += Get_Slot_Content_Html(UserID,TimeSlotID,'Outgoing');
				Count++;
			}
		}
		
		document.getElementById('TimeSlot-'+TimeSlotID+'-Outgoing').innerHTML = Content;
		document.getElementById(TimeSlotID+'OutgoingCountLayer').innerHTML = '('+Count+')';
	}
	
	SelectedUserList = new Array();
	if (ColumnNature == "OnDuty" || ColumnNature == "Outgoing") {
		// process holiday list
		for (var j=0; j< HolidayUserList.length; j++) {
			if (!jIN_ARRAY(CheckUserList,HolidayUserList[j])) {
				SelectedUserList[HolidayUserList[j]] = HolidayUserList[j];
			}
		}
		
		Content = '';
		Count = 0;
		for (var i=0; i< FullMemberList.length; i++) {
			if (jIN_ARRAY(SelectedUserList,FullMemberList[i])) {
				var UserID = FullMemberList[i];
				
				Content += Get_Slot_Content_Html(UserID,TimeSlotID,'Holiday');
				Count++;
			}
		}
		
		document.getElementById('TimeSlot-'+TimeSlotID+'-Holiday').innerHTML = Content;
		document.getElementById(TimeSlotID+'HolidayCountLayer').innerHTML = '('+Count+')';
	}
	
	SelectedUserList = new Array();
	if (ColumnNature == "Holiday" || ColumnNature == "Outgoing") {
		// process OnDuty list
		for (var j=0; j< OnDutyUserList.length; j++) {
			if (!jIN_ARRAY(CheckUserList,OnDutyUserList[j])) {
				SelectedUserList[OnDutyUserList[j]] = OnDutyUserList[j];
			}
		}
		
		Content = '';
		Count = 0;
		for (var i=0; i< FullMemberList.length; i++) {
			if (jIN_ARRAY(SelectedUserList,FullMemberList[i])) {
				var UserID = FullMemberList[i];
				
				Content += Get_Slot_Content_Html(UserID,TimeSlotID,'OnDuty');
				Count++;
			}
		}
		
		document.getElementById('TimeSlot-'+TimeSlotID+'-OnDuty').innerHTML = Content;
		document.getElementById(TimeSlotID+'OnDutyCountLayer').innerHTML = '('+Count+')';
	}
}

function Reset_Color() {
	$('div.TimeSlotDropBlock').css('background-color','#FFFFFF');
	$("div#Full-Outgoing-Layer").css('background-color','');
	$("div#Full-Holiday-Layer").css('background-color','');
	$('div.OutgoingTimeSlotDragBlock').css('background-color','#E2EDF9');
	$('div.HolidayTimeSlotDragBlock').css('background-color','#FFEDED');
	$('div#UnAssignLayer').css('background-color','#EEEEEE');
	$('div#FullStaffListLayer').css('background-color','#FFFFFF');
}

function Switch_Reason_Selection(SlotID,StaffID,SelectedValue) {
	var OutgoingReasonSelection = document.getElementById('OutgoingReason['+SlotID+']['+StaffID+']');
	var HolidayReasonSelection = document.getElementById('HolidayReason['+SlotID+']['+StaffID+']');
	
	if (SelectedValue == 'Holiday') {
		document.getElementById('SlotCell-'+SlotID+'-'+StaffID).className = "SlotCell"+SlotID+" roster_leave";
		OutgoingReasonSelection.style.display = 'none';
		HolidayReasonSelection.style.display = 'inline';
	}
	else if (SelectedValue == 'Outgoing') {
		document.getElementById('SlotCell-'+SlotID+'-'+StaffID).className = "SlotCell"+SlotID+" roster_outing";
		OutgoingReasonSelection.style.display = 'inline';
		HolidayReasonSelection.style.display = 'none';
	}
	else {
		if (SelectedValue == 'OnDuty') {
			document.getElementById('SlotCell-'+SlotID+'-'+StaffID).className = "SlotCell"+SlotID;
		}
		else { // no setting
			document.getElementById('SlotCell-'+SlotID+'-'+StaffID).className = "SlotCell"+SlotID+" roster_off";
		}
		
		OutgoingReasonSelection.style.display = 'none';
		HolidayReasonSelection.style.display = 'none';
	}
}

function ShowPanel(thisObj, PanelID)
{
	$('.SlotInfoLayers').css('visibility','hidden').css('display','none');
	
	var $PanelObj = $('#'+PanelID);
	var $ClickOnObj = $(thisObj);
	var $pos = $ClickOnObj.position();
	var offsetY = $ClickOnObj.height();
	$PanelObj.css('position', 'absolute');
	$PanelObj.css('left',$pos.left+'px');
	$PanelObj.css('top',($pos.top+offsetY)+'px');
	$PanelObj.css('visibility','visible');
	$PanelObj.slideDown();
}
}

// Drag and Drop function
{
function Drag_And_Drop_Init() {
	if($.browser.msie && $('#UnAssignLayer').length > 0){
		$('#UnAssignLayer').height(Math.max($('#UnAssignLayer').parent().height(), 150));
		
		var preferred_height = $('#UnAssignLayer').height();
		var setMinHeightFunc = function(obj){
			var obj = $(obj);
			var obj_parent = obj.parent();
			if(obj_parent.height() < preferred_height){
				obj_parent.height(preferred_height);
			}
			if(obj.height() < preferred_height){
				obj.height(preferred_height);
			}
		};
		$("div.TimeSlotDragBlock").each(function(){
			setMinHeightFunc(this);
		});
		$("div.OutgoingTimeSlotDragBlock").each(function(){
			setMinHeightFunc(this);
		});
		$("div.HolidayTimeSlotDragBlock").each(function(){
			setMinHeightFunc(this);
		});
		$("div.FullDragBlock").each(function(){
			setMinHeightFunc(this);
		});
	}

<? if ($StaffAttend3->Check_Access_Right('DUTYSETUP-SPECIAL-Manage') || $StaffAttend3->IS_ADMIN_USER()) {?>
	var StartLeft = -10;
	var StartTop = -10;
	// draggable
	// draggable
	$("div.TimeSlotDragBlock").draggable({
			helper:'clone',
			cursorAt:{left:StartLeft,top:StartTop},
			distance:'10',
			revert:false,
			start: function (ev,ui) {
				$('div.ReasonSelector').css('visibility','hidden');
				ui.helper.html('');
				
				var ElementID = $(this).attr('id');
				ElementID = ElementID.split('-');
				var TimeSlotID = ElementID[1];
				var OnDutyCount = Get_Check_Box_Value(TimeSlotID+'-OnDuty-UserID[]','Array');
				ui.helper.html('<font style="color:#000000;">'+OnDutyCount.length+' people selected</font>');
			},
			stop: function(event, ui) {
				Reset_Color();
			}
		});
	
	$("div.OutgoingTimeSlotDragBlock").draggable({
			helper:'clone',
			cursorAt:{left:StartLeft,top:StartTop},
			distance:'10',
			revert:false,
			start: function (ev,ui) {
				$('div.ReasonSelector').css('visibility','hidden');
				ui.helper.html('');
				
				var ElementID = $(this).attr('id');
				ElementID = ElementID.split('-');
				var TimeSlotID = ElementID[1];
				var OutgoingCount = Get_Check_Box_Value(TimeSlotID+'-Outgoing-UserID[]','Array');
				ui.helper.html('<font style="color:#000000;">'+OutgoingCount.length+' people selected</font>');
			},
			stop: function(event, ui) {
				Reset_Color();
			}
		});
	
	$("div.HolidayTimeSlotDragBlock").draggable({
			helper:'clone',
			cursorAt:{left:StartLeft,top:StartTop},
			distance:'10',
			revert:false,
			start: function (ev,ui) {
				$('div.ReasonSelector').css('visibility','hidden');
				ui.helper.html('');
				
				var ElementID = $(this).attr('id');
				ElementID = ElementID.split('-');
				var TimeSlotID = ElementID[1];
				var HolidayCount = Get_Check_Box_Value(TimeSlotID+'-Holiday-UserID[]','Array');
				ui.helper.html('<font style="color:#000000;">'+HolidayCount.length+' people selected</font>');
			},
			stop: function(event, ui) {
				Reset_Color();
			}
		});
	
	$("div.FullDragBlock").draggable({
			helper:'clone',
			cursorAt:{left:StartLeft,top:StartTop},
			distance:'10',
			revert:false,
			start: function (ev,ui) {
				$('div.ReasonSelector').css('visibility','hidden');
				ui.helper.html('');
				
				var SearchCount = Get_Check_Box_Value('Search-UserID[]','Array');
				ui.helper.html(SearchCount.length+' people selected');
			},
			stop: function(event, ui) {
				Reset_Color();
			}
		});
	
	// Full Holiday	
	$("div#Full-Holiday-Layer").draggable( {
			helper:'clone',
			cursorAt:{left:StartLeft,top:StartTop},
			distance:'10',
			revert:false,
			start: function (ev,ui) {
				$('div.ReasonSelector').css('visibility','hidden');
				ui.helper.html('');
				
				var FullHolidayUser = Get_Check_Box_Value('FullDay-Holiday-UserID[]','Array');
				ui.helper.html(FullHolidayUser.length+' people selected');
			},
			stop: function(event, ui) {
				Reset_Color();
			}
	});
	
	// Full Outgoing
	$("div#Full-Outgoing-Layer").draggable( {
			helper:'clone',
			cursorAt:{left:StartLeft,top:StartTop},
			distance:'10',
			revert:false,
			start: function (ev,ui) {
				$('div.ReasonSelector').css('visibility','hidden');
				ui.helper.html('');
				
				var FullOutgoingUser = Get_Check_Box_Value('FullDay-Outgoing-UserID[]','Array');
				ui.helper.html(FullOutgoingUser.length+' people selected');
			},
			stop: function(event, ui) {
				Reset_Color();
			}
	});
		
	// droppable
	// on duty drop
	$("div.TimeSlotDragBlock").droppable({
		accept: "div.FullDragBlock, div.OutgoingTimeSlotDragBlock, div.HolidayTimeSlotDragBlock",
		tolerance: "pointer",
		/*activeClass: 'droppable-active',
		hoverClass: 'droppable-hover',*/
		drop: function(ev, ui) {
			ElementID = $(this).attr('id');
			ElementID = ElementID.split('-');
			var TimeSlotID = ElementID[1];
			var CurTimeSlotList = Get_Check_Box_Value(TimeSlotID+'-OnDuty-UserID[]','Array',true);
			if (ui.draggable.attr('id') == "TimeSlot-"+TimeSlotID+"-Outgoing") {
				var DragColumnNature = 'Outgoing';
				var SelectedUser = Get_Check_Box_Value(TimeSlotID+"-Outgoing-UserID[]","Array");
			}
			else if (ui.draggable.attr('id') == "TimeSlot-"+TimeSlotID+"-Holiday") {
				var DragColumnNature = 'Holiday';
				var SelectedUser = Get_Check_Box_Value(TimeSlotID+"-Holiday-UserID[]","Array");
			}
			else {
				var SelectedUser = Get_Check_Box_Value("Search-UserID[]","Array");
			}
			
			for (var i=0; i< SelectedUser.length; i++) {
				if (!jIN_ARRAY(CurTimeSlotList,SelectedUser[i])) {
					var UserID = SelectedUser[i];
					var StaffName = document.getElementById('UserName['+UserID+']').value;
					
					var Content = Get_Slot_Content_Html(UserID,TimeSlotID,'OnDuty');
          var Div = $(Content);
          $('#TimeSlot-'+TimeSlotID.replace(/ /g,'\\ ')+'-OnDuty').append(Div);
				}
			}
			
			// calculate the count for the time slot column
			var NewTimeSlotList = Get_Check_Box_Value(TimeSlotID+'-OnDuty-UserID[]','Array',true);
			document.getElementById(TimeSlotID+'OnDutyCountLayer').innerHTML = '('+NewTimeSlotList.length+')';
			
			// recalculate other column state
			Calculate_Other_TimeSlot_List(TimeSlotID,'OnDuty',DragColumnNature);
			
			Reset_Color();
		},
		activate: function(ev, ui) {
			$(this).css('background-color','#DEB887');
		},
		over: function(ev, ui) {
			$(this).css('background-color','#FFFFE6');
		},
		out:  function(ev, ui) {
			$(this).css('background-color','#DEB887');
		}
	});
	
	// outgoing drop
	$("div.OutgoingTimeSlotDragBlock").droppable({
		accept: "div.FullDragBlock, div.TimeSlotDragBlock, div.HolidayTimeSlotDragBlock",
		tolerance: "pointer",
		/*activeClass: 'droppable-active',
		hoverClass: 'droppable-hover',*/
		drop: function(ev, ui) {
			ElementID = $(this).attr('id');
			ElementID = ElementID.split('-');
			var TimeSlotID = ElementID[1];
			var CurTimeSlotList = Get_Check_Box_Value(TimeSlotID+'-Outgoing-UserID[]','Array',true);
			if (ui.draggable.attr('id') == "TimeSlot-"+TimeSlotID+"-OnDuty") {
				var DragColumnNature = 'OnDuty';
				var SelectedUser = Get_Check_Box_Value(TimeSlotID+"-OnDuty-UserID[]","Array");
			}
			else if (ui.draggable.attr('id') == "TimeSlot-"+TimeSlotID+"-Holiday") {
				var DragColumnNature = 'Holiday';
				var SelectedUser = Get_Check_Box_Value(TimeSlotID+"-Holiday-UserID[]","Array");
			}
			else {
				var SelectedUser = Get_Check_Box_Value("Search-UserID[]","Array");
			}
			
			for (var i=0; i< SelectedUser.length; i++) {
				if (!jIN_ARRAY(CurTimeSlotList,SelectedUser[i])) {
					var UserID = SelectedUser[i];
					
					var Content = Get_Slot_Content_Html(UserID,TimeSlotID,'Outgoing');
          var Div = $(Content);
          $('#TimeSlot-'+TimeSlotID.replace(/ /g,'\\ ')+'-Outgoing').append(Div);
				}
			}
			
			// calculate the count for the time slot column
			var NewTimeSlotList = Get_Check_Box_Value(TimeSlotID+'-Outgoing-UserID[]','Array',true);
			document.getElementById(TimeSlotID+'OutgoingCountLayer').innerHTML = '('+NewTimeSlotList.length+')';
			
			// recalculate other column state
			Calculate_Other_TimeSlot_List(TimeSlotID,'Outgoing',DragColumnNature);
			
			Reset_Color();
		},
		activate: function(ev, ui) {
			$(this).css('background-color','#DEB887');
		},
		over: function(ev, ui) {
			$(this).css('background-color','#FFFFE6');
		},
		out:  function(ev, ui) {
			$(this).css('background-color','#DEB887');
		}
	});
	
	// Holiday drop
	$("div.HolidayTimeSlotDragBlock").droppable({
		accept: "div.UnAssignDragBlock, div.FullDragBlock, div.TimeSlotDragBlock, div.OutgoingTimeSlotDragBlock",
		tolerance: "pointer",
		/*activeClass: 'droppable-active',
		hoverClass: 'droppable-hover',*/
		drop: function(ev, ui) {
			ElementID = $(this).attr('id');
			ElementID = ElementID.split('-');
			var TimeSlotID = ElementID[1];
			var CurTimeSlotList = Get_Check_Box_Value(TimeSlotID+'-Holiday-UserID[]','Array',true);
			if (ui.draggable.attr('id') == "TimeSlot-"+TimeSlotID+"-OnDuty") {
				var DragColumnNature = 'OnDuty';
				var SelectedUser = Get_Check_Box_Value(TimeSlotID+"-OnDuty-UserID[]","Array");
			}
			else if (ui.draggable.attr('id') == "TimeSlot-"+TimeSlotID+"-Outgoing") {
				var DragColumnNature = 'Outgoing';
				var SelectedUser = Get_Check_Box_Value(TimeSlotID+"-Outgoing-UserID[]","Array");
			}
			else {
				var SelectedUser = Get_Check_Box_Value("Search-UserID[]","Array");
			}
			
			for (var i=0; i< SelectedUser.length; i++) {
				if (!jIN_ARRAY(CurTimeSlotList,SelectedUser[i])) {
					var UserID = SelectedUser[i];
					
					var Content = Get_Slot_Content_Html(UserID,TimeSlotID,'Holiday');
          var Div = $(Content);
          $('#TimeSlot-'+TimeSlotID.replace(/ /g,'\\ ')+'-Holiday').append(Div);
				}
			}
			
			// calculate the count for the time slot column
			var NewTimeSlotList = Get_Check_Box_Value(TimeSlotID+'-Holiday-UserID[]','Array',true);
			document.getElementById(TimeSlotID+'HolidayCountLayer').innerHTML = '('+NewTimeSlotList.length+')';
			
			// recalculate other column state
			Calculate_Other_TimeSlot_List(TimeSlotID,'Holiday',DragColumnNature);
			
			Reset_Color();
		},
		activate: function(ev, ui) {
			$(this).css('background-color','#DEB887');
		},
		over: function(ev, ui) {
			$(this).css('background-color','#FFFFE6');
		},
		out:  function(ev, ui) {
			$(this).css('background-color','#DEB887');
		}
	});
	
	// trash drop
	$("div.FullDropBlock").droppable({
		accept: "div.TimeSlotDragBlock, div.OutgoingTimeSlotDragBlock, div.HolidayTimeSlotDragBlock, div#Full-Outgoing-Layer, div#Full-Holiday-Layer",
		tolerance: "pointer",
		/*activeClass: 'droppable-active',
		hoverClass: 'droppable-hover',*/
		drop: function(ev, ui) {
			ElementID = ui.draggable.attr('id');
			if (ElementID == 'Full-Outgoing-Layer' || ElementID == 'Full-Holiday-Layer') {
				ElementID = ElementID.split('-');
				
				var FullDaySelectedList = Get_Check_Box_Value('FullDay-'+ElementID[1]+'-UserID[]','Array');
				var FullDayList = Get_Check_Box_Value('FullDay-'+ElementID[1]+'-UserID[]','Array',true);
				
				Content = '';
				for (var i=0; i< FullDayList.length; i++) {
					if (!jIN_ARRAY(FullDaySelectedList,FullDayList[i])) {
						var UserID = FullDayList[i];
						Content += Get_Slot_Content_Html(UserID,'FullDay',ElementID[1]);
					}
				}
				ui.draggable.html(Content);
				
				Get_Full_Member_List();
			}
			else {
				ElementID = ElementID.split('-');
				var TimeSlotID = ElementID[1];
				var ColumnNature = ElementID[2];
				var SlotFullUserList = Get_Check_Box_Value(TimeSlotID+'-'+ColumnNature+'-UserID[]','Array',true);
				var SlotSelectedUserList = Get_Check_Box_Value(TimeSlotID+'-'+ColumnNature+'-UserID[]','Array');
				
				Content = '';
				for (var i=0; i< SlotFullUserList.length; i++) {
					if (!jIN_ARRAY(SlotSelectedUserList,SlotFullUserList[i])) {
						var UserID = SlotFullUserList[i];
						Content += Get_Slot_Content_Html(UserID,TimeSlotID,ColumnNature);
					}
				}
				
				ui.draggable.html(Content);
				
				// calculate the count for the time slot column
				var NewTimeSlotList = Get_Check_Box_Value(TimeSlotID+'-'+ColumnNature+'-UserID[]','Array',true);
				document.getElementById(TimeSlotID+ColumnNature+'CountLayer').innerHTML = '('+NewTimeSlotList.length+')';
			}
			Reset_Color();
		},
		activate: function(ev, ui) {
			$(this).css('background-color','#DEB887');
		},
		over: function(ev, ui) {
			$(this).css('background-color','#FFFFE6');
		},
		out:  function(ev, ui) {
			$(this).css('background-color','#DEB887');
		}
	});
	
	// Full Holiday, Outgoing Drop
	$("div#Full-Outgoing-Layer, div#Full-Holiday-Layer").droppable({
		accept: "div.FullDragBlock",
		tolerance: "pointer",
		/*activeClass: 'droppable-active',
		hoverClass: 'droppable-hover',*/
		drop: function(ev, ui) {
			var SearchSelectedUserList = Get_Check_Box_Value('Search-UserID[]','Array');
			var FullUserList = document.getElementsByName('Full-UserID[]');
			var DropLayer = $(this).attr('id');
			var DropLayer = DropLayer.split('-');
			var FullDayList = Get_Check_Box_Value('FullDay-'+DropLayer[1]+'-UserID[]','Array',true);
			
			Content = '';
			for (var i=0; i< FullUserList.length; i++) {
				if (jIN_ARRAY(SearchSelectedUserList,FullUserList[i].value) || jIN_ARRAY(FullDayList,FullUserList[i].value)) {
					var UserID = FullUserList[i].value;
					Content += Get_Slot_Content_Html(UserID,'FullDay',DropLayer[1]);
				}
			}
			$(this).html(Content);
			
			Get_Full_Member_List();
			Check_Full_Outgoing_Holiday();
			
			Reset_Color();
		},
		activate: function(ev, ui) {
			$(this).css('background-color','#DEB887');
		},
		over: function(ev, ui) {
			$(this).css('background-color','#FFFFE6');
		},
		out:  function(ev, ui) {
			$(this).css('background-color','#DEB887');
		}
	});
<?}?>
}
}

// misc 
{
function Get_Slot_Content_Html(UserID,TimeSlotID,ColumnNature) {
	var Content = '';
	var StaffName = document.getElementById('UserName['+UserID+']').value;
	
	if (ColumnNature == "Outgoing" || ColumnNature == "Holiday") {
		Content += '<table style="background: transparent; padding:0px; border:0px; width:100%; padding:0px; spacing:0px; cursor:pointer;">';
		Content += '<tr>';
		Content += '<td style="background: transparent; padding:0px; border:0px; padding:0px; spacing:0px">';
	}
	Content += '<div onclick="Change_Background_Color(\''+TimeSlotID+'-'+ColumnNature+'-UserID-'+UserID+'\',this);" style="cursor:pointer;">';
	Content += '<input style="display:none;" name="'+TimeSlotID+'-'+ColumnNature+'-UserID[]" id="'+TimeSlotID+'-'+ColumnNature+'-UserID-'+UserID+'" type="checkbox" value="'+UserID+'"/>';
  Content += '<font class="staff_name" style="color:#000000;">'+StaffName+'</font>';
  Content += '</div>';
  if (ColumnNature == "Outgoing" || ColumnNature == "Holiday") {
	  Content += '</td>';
		Content += '<td style="background: transparent; padding:0px; border:0px; padding:0px; spacing:0px; width:20px;">';
		Content += '<span class="table_row_tool" style="float:right;">';
		Content += '<a href="#" onClick="Show_Reason_Layer(this); return false;" class="reason'+(($('input#'+TimeSlotID+'-'+ColumnNature+'-'+UserID).val() > 0)? '':'_alert')+'" title="<?=$Lang['StaffAttendance']['FullDayReason']?>" style="display:inline; float:right;"></a>';
		Content += '\n'; // this linebreak must be included for firefox process
		Content += '<div id="'+TimeSlotID+'-'+UserID+'-'+ColumnNature+'ReasonLayer" class="selectbox_layer ReasonSelector">';
		Content += '\n'; // this linebreak must be included for firefox process
	  Content += '<select name="'+TimeSlotID+'-'+ColumnNature+'Reason['+UserID+']" id="'+TimeSlotID+'-'+ColumnNature+'-Select-Reason-'+UserID+'" onchange="Check_Reason_Icon(this);">';
	  Content += '<option value=""><?=$Lang['StaffAttendance']['SelectReason']?></option>';
	  var ReasonList = document.getElementsByName(ColumnNature+'ReasonList[]');
		for (var j=0; j< ReasonList.length; j++) {
			Content += '<option value="'+(ReasonList[j].id)+'" '+((ReasonList[j].id == $('input#'+TimeSlotID+'-'+ColumnNature+'-'+UserID).val())? "selected":"")+'>'+htmlspecialchars(ReasonList[j].value,3)+'</option>';
		}
	  Content += '</select>';
	  Content += '</div>';
		Content += '</span>';
		Content += '</td>';
		Content += '</tr>';
		Content += '</table>';
	}
	
	return Content;
}

function SwitchTargetDateMethod(TargetMethod) {
	if (TargetMethod == "Calendar") {
		$('#TargetDateMethod').val("Calendar");
		$("#day_option").show();
		$("#TargetDateRepeatLayer").hide();
	}
	else {
		changeRepeatMsg();
		$('#TargetDateMethod').val("Repeat");
		$("#day_option").hide();
		$("#TargetDateRepeatLayer").show();
	}
}

function showRepeatCtrl(SelectedValue) {
	$('.RepeatDetail').hide();
	if (SelectedValue == "DAILY") {
		$('#DailyRepeatLayer').show();
	}
	else if (SelectedValue == "WEEKLY") {
		$('#WeeklyRepeatLayer').show();
	}
	else if (SelectedValue == "MONTHLY") {
		$('#MonthlyRepeatLayer').show();
	}
	else if (SelectedValue == "YEARLY") {
		$('#YearlyRepeatLayer').show();
	}
}

function changeRepeatMsg() {
	var repeatSelect = document.getElementById("RepeatSelection");
	var repeatMsgSpan = document.getElementById("repeatMsgSpan");
	var repeatSelected = repeatSelect.options[repeatSelect.selectedIndex].value;
	var eventDate = $("#RepeatStart").val();
	var eventDatePieces = eventDate.split("-");
	var eventDateObj = new Date();
	eventDateObj.setFullYear(eventDatePieces[0],eventDatePieces[1]-1,eventDatePieces[2]);
	
	switch(repeatSelected)
	{
	case "DAILY":
		if (document.getElementById('repeatDailyDay').value==1)
			newMsg = "<?=$Lang['StaffAttendance']['RepeatType']['DAILY']?> ";
		else
			newMsg = "<?=$Lang['StaffAttendance']['Every']?> " + document.getElementById('repeatDailyDay').value + " <?=$Lang['StaffAttendance']['Day']?> ";
		newMsg += "<?=$Lang['StaffAttendance']['Until']?> " + $('#RepeatEnd').val();
		repeatMsgSpan.innerHTML = newMsg;
		$('#repeatMsgSpan').show();
		break;
	case "WEEKLY":
		if (document.getElementById('repeatWeeklyRange').value == "1") {
			newMsg = "<?=$Lang['StaffAttendance']['RepeatType']['WEEKLY']?> ";
			checkSame = newMsg;
		} else {
			newMsg = "<?=$Lang['StaffAttendance']['Every']?> "+document.getElementById('repeatWeeklyRange').value+" <?=$Lang['StaffAttendance']['Week']?> ";
			checkSame = newMsg;
		}
		
		<?
			$tempWeekDayArray = array("Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat");
			$tempWeekDayDisplayArray = array($Lang['StaffAttendance']['Sun'],$Lang['StaffAttendance']['Mon'],$Lang['StaffAttendance']['Tue'],$Lang['StaffAttendance']['Wed'],$Lang['StaffAttendance']['Thur'],$Lang['StaffAttendance']['Fri'],$Lang['StaffAttendance']['Sat']);
			for($i=0; $i<sizeof($tempWeekDayArray); $i++) {
		?>
		if (document.getElementById("<?=$tempWeekDayArray[$i]?>").checked) {
			if (newMsg.match("day"))
				newMsg += ", <?=$tempWeekDayDisplayArray[$i]?>";
			else
				newMsg += "<?=$tempWeekDayDisplayArray[$i]?>";
		}
		<?
			}
		?>
		var weekDayName = new Array("<?=$tempWeekDayDisplayArray[0]?>","<?=$tempWeekDayDisplayArray[1]?>",
						"<?=$tempWeekDayDisplayArray[2]?>","<?=$tempWeekDayDisplayArray[3]?>",
						"<?=$tempWeekDayDisplayArray[4]?>","<?=$tempWeekDayDisplayArray[5]?>",
						"<?=$tempWeekDayDisplayArray[6]?>");
		if (checkSame == newMsg)
			newMsg += weekDayName[eventDateObj.getDay()];
		newMsg += " <?=$Lang['StaffAttendance']['Until']?> " + $('#RepeatEnd').val();
		repeatMsgSpan.innerHTML = newMsg;
		$('#repeatMsgSpan').show();
		break;
	case "MONTHLY":
		var monthlyRepeatBy0 = document.getElementById("monthlyRepeatBy0");
		var monthlyRepeatBy1 = document.getElementById("monthlyRepeatBy1");
		
		if (document.getElementById("repeatMonthlyRange").value == "1")
			newMsg = "<?=$Lang['StaffAttendance']['EveryMonth']?> ";
		else
			newMsg = "<?=$Lang['StaffAttendance']['Every']?> "+document.getElementById("repeatMonthlyRange").value+" <?=$Lang['StaffAttendance']['RepeatMonth']?> ";
		
		if (monthlyRepeatBy0.checked)
			newMsg += "<?=$Lang['StaffAttendance']['On']?> "+eventDateObj.getDate()+" <?=($intranet_session_language == 'en')? "":$Lang['StaffAttendance']['Day']?> ";
		if (monthlyRepeatBy1.checked) {
			var nth = 0;
			for(var j=1; j<6; j++) {
				if (eventDateObj.getDate() == NthDay(j, eventDateObj.getDay(), eventDateObj.getMonth(), eventDateObj.getFullYear())) {
					nth = j;
					break;
				}
			}
			
			var daysofmonth   = new Array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
			var daysofmonthLY = new Array(31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
			
			if (isLeapYear(eventDateObj.getFullYear()))
				var days = daysofmonthLY[eventDateObj.getMonth()];
			else
				var days = daysofmonth[eventDateObj.getMonth()];
			if (days - eventDateObj.getDate() < 7)
				nth = 6;
			
			var weekDayName = new Array("<?=$tempWeekDayDisplayArray[0]?>","<?=$tempWeekDayDisplayArray[1]?>",
						"<?=$tempWeekDayDisplayArray[2]?>","<?=$tempWeekDayDisplayArray[3]?>",
						"<?=$tempWeekDayDisplayArray[4]?>","<?=$tempWeekDayDisplayArray[5]?>",
						"<?=$tempWeekDayDisplayArray[6]?>");
			var countName =  new Array("<?=$Lang['StaffAttendance']['WeekCount'][0]?>", "<?=$Lang['StaffAttendance']['WeekCount'][1]?>", 
							"<?=$Lang['StaffAttendance']['WeekCount'][2]?>", "<?=$Lang['StaffAttendance']['WeekCount'][3]?>", 
							"<?=$Lang['StaffAttendance']['WeekCount'][4]?>", "<?=$Lang['StaffAttendance']['Last']?>");
			newMsg += "<?=($intranet_session_language == "en")? $Lang['StaffAttendance']['On']." ":""?>"+ countName[nth-1] + " <?=($intranet_session_language == 'en')? "":$Lang['StaffAttendance']['WeekDay']?> " + weekDayName[eventDateObj.getDay()] + " ";
		}
		newMsg += "<?=$Lang['StaffAttendance']['Until']?> " + $('#RepeatEnd').val();
		repeatMsgSpan.innerHTML = newMsg;
		$('#repeatMsgSpan').show();
		break;
	case "YEARLY":
		if (document.getElementById("repeatYearlyRange").value == "1")
			newMsg = "<?=$Lang['StaffAttendance']['RepeatType']['YEARLY']?> ";
		else
			newMsg = "<?=$Lang['StaffAttendance']['Every']?> "+document.getElementById("repeatYearlyRange").value+" <?=$Lang['StaffAttendance']['Year']?> ";
		var monthname = new Array("<?=$Lang['StaffAttendance']['MonthName'][0]?>","<?=$Lang['StaffAttendance']['MonthName'][1]?>","<?=$Lang['StaffAttendance']['MonthName'][2]?>",
						"<?=$Lang['StaffAttendance']['MonthName'][3]?>","<?=$Lang['StaffAttendance']['MonthName'][4]?>","<?=$Lang['StaffAttendance']['MonthName'][5]?>",
						"<?=$Lang['StaffAttendance']['MonthName'][6]?>","<?=$Lang['StaffAttendance']['MonthName'][7]?>","<?=$Lang['StaffAttendance']['MonthName'][8]?>",
						"<?=$Lang['StaffAttendance']['MonthName'][9]?>","<?=$Lang['StaffAttendance']['MonthName'][10]?>","<?=$Lang['StaffAttendance']['MonthName'][11]?>");
		newMsg += monthname[eventDateObj.getMonth()]+" "+eventDateObj.getDate();
		newMsg += " <?=$Lang['StaffAttendance']['Until']?> " + $('#RepeatEnd').val();
		repeatMsgSpan.innerHTML = newMsg;
		$('#repeatMsgSpan').show();
		break;
	}
}
}

function Apply_Time_Slot_To_Confirmed_Duty()
{
	var TimeSlot = Get_Selection_Value('TimeSlot[]','Array');
	var TimeSlotTable = document.getElementById('TimeSlotTable');
	var CellHeight = document.getElementById('FullStaffListLayer').offsetHeight/3;
	var TimeSlotTableCells = TimeSlotTable.tBodies[0].rows[0].cells;
	
	if ((TimeSlotTableCells.length + 1) > <?=$StaffAttend3->MaxSlotPerSetting?>) {
			alert('<?=$Lang['StaffAttendance']['MaxSlotPerSettingWarning1'].$StaffAttend3->MaxSlotPerSetting.$Lang['StaffAttendance']['MaxSlotPerSettingWarning2']?>');
	}
	else {
		if (TimeSlotTableCells.length == 0) {
			CellIndex = -1;
		}
		else {
			FormSlotStartHour = Get_Selection_Value('StartHour','String');
			FormSlotStartMin = Get_Selection_Value('StartMin','String');
			FormSlotStartSec = Get_Selection_Value('StartSec','String');
			var NewSlotStart = FormSlotStartHour + ':' + FormSlotStartMin + ':' + FormSlotStartSec;
					
			for (var j=0; j< TimeSlotTableCells.length; j++) {
				var CurrSlotStart = document.getElementById('SlotStart['+TimeSlotTableCells[j].id+']').value;
				
				if (NewSlotStart <= CurrSlotStart) {
					CellIndex = j;
					
					break;
				}
				else 
					CellIndex = -1;
			}
		}
		for(var i=0;i<TimeSlot.length;i++)
		{
			var slot_name = TimeSlot[i];
			slot_name = slot_name.replace(/ /g,'\\ ');
			if($(TimeSlotTable).find('input#TimeSlotName\\['+slot_name+'\\]').length==0)
			{
				Get_Slot_Column(CellIndex,TimeSlot[i],true);
				CellIndex+=1;
			}
		}
	}
	
	Thick_Box_Init();
	Drag_And_Drop_Init();
}

// thick box function 
{
//on page load call tb_init
function Thick_Box_Init(){   
	tb_init('a.thickbox');//pass where to apply thickbox
}
}

$(document).ready(function(){
	if($.browser.safari || $.browser.webkit || !!navigator.userAgent.match(/Trident\/7\./))
	{
		document.onselectstart = function () { return false; };
	}
});
</script>