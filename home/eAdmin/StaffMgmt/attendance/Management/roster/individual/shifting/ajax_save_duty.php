<?php
/*
 * 2016-12-13 (Carlos): $sys_custom['StaffAttendance']['WongFutNamCollege'] - added post param AttendOne.
 */
$PATH_WRT_ROOT = "../../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!$StaffAttend3->Check_Access_Right('DUTYSETUP-SPECIAL'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	echo 'die';
	intranet_closedb();
	exit();
}

$StaffID = $_REQUEST['StaffID'];
$TargetDate = $_REQUEST['TargetDate'];
$TimeSlotID = (is_array($_REQUEST['TimeSlotID']))? $_REQUEST['TimeSlotID']:array();
$TimeSlotUser = array();
$HolidayUser = array();
$OutgoingUser = array();
for ($i=0; $i< sizeof($TimeSlotID); $i++) {
	$TimeSlotUser[$TimeSlotID[$i]] = $_REQUEST[$TimeSlotID[$i].'-OnDuty'];
	$HolidayUser[$TimeSlotID[$i]] = $_REQUEST[$TimeSlotID[$i].'-Holiday'];
	$OutgoingUser[$TimeSlotID[$i]] = $_REQUEST[$TimeSlotID[$i].'-Outgoing'];
}
$OutgoingReason = $_REQUEST['OutgoingReason'];
$HolidayReason = $_REQUEST['HolidayReason'];
$SetAsNonSchoolDay = $_REQUEST['SetAsNonSchoolDay'];

$AttendOne = $_REQUEST['AttendOne'];
if(!in_array($AttendOne,array(0,1))) $AttendOne = 0;

$UsingMethod = $_REQUEST['UsingMethod'];
$RepeatStart = $_REQUEST['RepeatStart'];
$RepeatEnd = $_REQUEST['RepeatEnd'];
$RepeatSelection = $_REQUEST['RepeatSelection'];
$repeatDailyDay = $_REQUEST['repeatDailyDay'];
$repeatWeeklyRange = $_REQUEST['repeatWeeklyRange'];
$RepeatWeekDay = (is_array($_REQUEST['RepeatWeekDay']))? $_REQUEST['RepeatWeekDay']:array();
$repeatMonthlyRange = $_REQUEST['repeatMonthlyRange'];
$monthlyRepeatBy = $_REQUEST['monthlyRepeatBy'];
$repeatYearlyRange = $_REQUEST['repeatYearlyRange'];

if ($UsingMethod == "Repeat") {
	$Rule['freq'] = $RepeatSelection;
	switch ($RepeatSelection) {
		case "DAILY":			
			$Rule['interval'] = $repeatDailyDay;
			break;
		case "WEEKLY":
			$Rule['byDay'] = $RepeatWeekDay;
			$Rule['interval'] = $repeatWeeklyRange;
			break;
		case "MONTHLY":
			$Rule['RepeatBy'] = $monthlyRepeatBy;
			$Rule['interval'] = $repeatMonthlyRange;
			break;
		case "YEARLY":
			$Rule['interval'] = $repeatYearlyRange;
			break;
		default:
			break;
	}
	
	$TargetDate = $StaffAttend3->Get_All_Repeated_Day($RepeatStart,$RepeatEnd,$Rule);
}

$StaffAttend3->Start_Trans();
if ($StaffAttend3->Save_Shifting_Duty($StaffID,$TargetDate,$TimeSlotID,$TimeSlotUser,$HolidayUser,$OutgoingUser,$OutgoingReason,$HolidayReason,"Individual",$SetAsNonSchoolDay,$AttendOne)) {
	echo $Lang['StaffAttendance']['ShiftDutySaveSuccess'];
	$StaffAttend3->Commit_Trans();
}
else {
	echo $Lang['StaffAttendance']['ShiftDutySaveFail'];
	$StaffAttend3->RollBack_Trans();
}

intranet_closedb();
?>