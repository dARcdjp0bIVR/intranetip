<?php
// editing by Kenneth Chung
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!$StaffAttend3->Check_Access_Right('MGMT-OT'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$StaffAttend3UI = new libstaffattend3_ui();

$Keyword = trim(urldecode(stripslashes($_REQUEST['Keyword'])));
$TargetDate = $_REQUEST['TargetDate'];
$StaffType = $_REQUEST['StaffType'];

if($TargetDate != "" && $TargetDate != "undefined" && $TargetDate != null)
{
	$timestamp = strtotime($TargetDate);
	$Year = date('Y',$timestamp);
	$Month = date('m',$timestamp);
	$Day = date('d',$timestamp);
}else
{
	$Year = date('Y');
	$Month = date('m');
	$Day = date('d');
}

echo $StaffAttend3UI->Get_OT_Records_Summary_List($Keyword, $StaffType, $Year, $Month, $Day);

intranet_closedb();
?>