<?php
// Editing by 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend2.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!($StaffAttend3->Check_Access_Right('MGMT-OT') || $StaffAttend3->Check_Access_Right('MGMT-Attendance')))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0) || !$sys_custom['StaffAttendance']['WongFutNamCollege']) {
	intranet_closedb();
	//header ("Location: /");
	exit();
}

$task = $_REQUEST['task'];

switch($task)
{
	case 'checkDuplication':
		
		$records = $StaffAttend3->Get_OT_Records($_POST);
		echo count($records);
		
	break;
	
	case 'checkValidity':
		
		include_once($PATH_WRT_ROOT."includes/json.php");
		
		$jsonObj = new JSON_obj();
		
		$staff_id = $_POST['StaffID'];
		$record_date = $_POST['RecordDate'];
		$ot_mins = $_POST['OTmins'];
		$check_duty_type = $_POST['CheckDutyType'];
		if($check_duty_type == 1) // check duty
		{
			$duty = $StaffAttend3->Get_Duty_By_Date($staff_id,$record_date,$CreateCardLog=false,$IgnoreFullDayHoliday=true,$FreezeDuty=false,$CacheResult=false);
			$returnAry = array();
			if(isset($duty['CardUserToDuty']) && isset($duty['CardUserToDuty'][$staff_id])){
				//echo count($duty['CardUserToDuty'][$staff_id]);
				$attendance_ary = $duty['CardUserToDuty'][$staff_id];
				$attendance_ary_size = count($attendance_ary);
				
				if($attendance_ary_size == 0){
					$returnAry[] = array('0',$Lang['StaffAttendance']['NoAttendanceRecordAndOTRecordInvalid']);
				}else{
					$last_out_time = '';
					$last_duty_end = '';
					for($i=$attendance_ary_size-1;$i>=0;$i--){
						$last_out_time = $attendance_ary[$i]['OutTime'];
						$last_duty_end = $attendance_ary[$i]['DutyEnd'];
						if($last_out_time != '') break;
					}
					//$last_out_time = $attendance_ary[$attendance_ary_size-1]['OutTime'];
					//$last_duty_end = $attendance_ary[$attendance_ary_size-1]['DutyEnd'];
					if($last_out_time == ''){
						$returnAry[] = array('0',$Lang['StaffAttendance']['AttendanceRecordNoOutTime']);
					}else if($last_duty_end != ''){
						$out_time_mins = strtotime($last_out_time) / 60; // to mins
						$duty_end_mins = strtotime($last_duty_end) / 60; // to mins
						$diff_mins = $out_time_mins - $duty_end_mins;
						if($diff_mins >= $ot_mins){
							$msg = $Lang['StaffAttendance']['OTHoursMatched'];
							$msg = str_replace('<!--LEAVE_TIME-->',$last_out_time,$msg);
							$msg = str_replace('<!--HOUR-->',sprintf("%.1f",$diff_mins/60.0),$msg);
							$returnAry[] = array('1',$msg);
						}else{
							$msg = $Lang['StaffAttendance']['OTHoursNotMatch'];
							$msg = str_replace('<!--LEAVE_TIME-->',$last_out_time,$msg);
							$msg = str_replace('<!--HOUR-->',sprintf("%.1f",$diff_mins/60.0),$msg);
							$returnAry[] = array('0',$msg);
						}
					}else{
						$returnAry[] = array('0',$Lang['StaffAttendance']['NoAttendanceRecordAndOTRecordInvalid']);
					}
				}
			}else{
				$returnAry[] = array('0',$Lang['StaffAttendance']['NoAttendanceRecordAndOTRecordInvalid']);
			}
		}else if($check_duty_type == 2){ // check tap card only
			$ts = strtotime($record_date);
			$year = date("Y", $ts);
			$month = date("m",$ts);
			$day = date("d",$ts);
			$raw_logs = $StaffAttend3->Get_Entry_Log($year,$month,array($staff_id),0,$day);
			$raw_log_size = count($raw_logs);
			if($raw_log_size < 2){
				$returnAry[] = array('0',$Lang['StaffAttendance']['OTNotEnoughTapCardTimes']);
			}else{
				$first_tap_card_time = $raw_logs[0]['RecordTime'];
				$last_tap_card_time = $raw_logs[$raw_log_size-1]['RecordTime'];
				$in_time_mins = strtotime($first_tap_card_time) / 60; // to mins
				$out_time_mins = strtotime($last_tap_card_time) / 60; // to mins
				$diff_mins = $out_time_mins - $in_time_mins;
				if($diff_mins >= $ot_mins){
					$msg = $Lang['StaffAttendance']['TapCardTimesMatchOTHours'];
					$msg = str_replace('<!--IN_TIME-->',$first_tap_card_time,$msg);
					$msg = str_replace('<!--OUT_TIME-->',$last_tap_card_time,$msg);
					$msg = str_replace('<!--HOUR-->',sprintf("%.1f",$diff_mins/60.0),$msg);
					$returnAry[] = array('1',$msg);
				}else{
					$msg = $Lang['StaffAttendance']['TapCardTimesDoesNotMatchOTHours'];
					$msg = str_replace('<!--IN_TIME-->',$first_tap_card_time,$msg);
					$msg = str_replace('<!--OUT_TIME-->',$last_tap_card_time,$msg);
					$msg = str_replace('<!--HOUR-->',sprintf("%.1f",$diff_mins/60.0),$msg);
					$returnAry[] = array('0',$msg);
				}
			}
		}else{
			$returnAry[] = array('1','');
		}
		echo $jsonObj->encode($returnAry);
	break;
}

intranet_closedb();
?>