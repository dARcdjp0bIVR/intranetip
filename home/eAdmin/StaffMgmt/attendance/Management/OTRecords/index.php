<?php
// editing by Carlos
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend2.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

if($sys_custom['StaffAttendance']['WongFutNamCollege']){
	header("Location:ot.php");
	exit;
}

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!$StaffAttend3->Check_Access_Right('MGMT-OT'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	header ("Location: /");
	intranet_closedb();
	exit();
}
$linterface = new interface_html();
$StaffAttend3UI = new libstaffattend3_ui();

$CurrentPageArr['eAdminStaffAttendance'] = 1;
$CurrentPage['OTRecords'] = 1;

$RecordType = $_REQUEST['type'];

$TAGS_OBJ[] = array($Lang['StaffAttendance']['OTRecords'], "", 0);
$MODULE_OBJ = $StaffAttend3UI->GET_MODULE_OBJ_ARR();
//$TAGS_OBJ_RIGHT[] = array(gen_online_help_btn_and_layer('staff_attendance','ot_record'));
$linterface->LAYOUT_START(urldecode($_REQUEST['Msg']));

echo $StaffAttend3UI->Get_OT_Records_Index();

$linterface->LAYOUT_STOP();
intranet_closedb();
?>
<script type="text/JavaScript" language="JavaScript">
// dom function 
{
function Check_Go_Search_Summary(evt)
{
	var key = evt.which || evt.charCode || evt.keyCode;
	
	if (key == 13) // enter
	{
		Get_OTRecord_Summary_List();
	}
	else
		return false;
}

function Check_Go_Search_OTRecord(evt, UserID)
{
	var key = evt.which || evt.charCode || evt.keyCode;
	
	if (key == 13) // enter
	{
		Get_OTRecords_OTRecord_List(UserID);
	}
	else
		return false;
}

function Check_Go_Search_RedeemRecord(evt, UserID)
{
	var key = evt.which || evt.charCode || evt.keyCode;
	
	if (key == 13) // enter
	{
		Get_OTRecords_RedeemRecord_List(UserID);
	}
	else
		return false;
}

function Check_Valid_Date(DateObjId, WarningLayerId)
{
	var dateObj = document.getElementById(DateObjId);
	var $WarningLayer = $('#'+WarningLayerId);
	if(check_date_without_return_msg(dateObj))
	{
		var targetdate = dateObj.value;
		var currentdate = new Date();
		var curYear = '' + currentdate.getFullYear();
		var curMonth = currentdate.getMonth()+1;
		var curDay = currentdate.getDate();
		curMonth = (curMonth<10)? ('0'+curMonth):(''+curMonth);
		curDay = (curDay<10)? ('0'+curDay):(''+curDay);
		var today = curYear + '-' + curMonth + '-' + curDay;
		
		if(targetdate>today)
		{
			dateObj.focus();
			$WarningLayer.html('<?=$Lang['StaffAttendance']['TakeViewFutureOTWarning']?>');
			$WarningLayer.css('visibility','visible');
			SetTimerToHideWarning(WarningLayerId, 3000);
			return false;
		}else
		{
			$WarningLayer.html('');
			$WarningLayer.css('visibility','hidden');
			return true;
		}
	}else
	{
		dateObj.focus();
		$WarningLayer.html('<?=$Lang['LessonAttendance']['InvalidDateFormat']?>');
		$WarningLayer.css('visibility','visible');
		SetTimerToHideWarning(WarningLayerId, 3000);
		return false;
	}
}

function SetTimerToHideWarning(WarningLayerId, ticks)
{
	var action = "$('#"+WarningLayerId+"').css('visibility','hidden');";
	setTimeout(action,ticks);
}

function ShowHidePeriodSelector(SelectorID, LayerID, TargetIndex)
{
	var $SelectorObj = $('#'+SelectorID);
	var $LayerObj = $('#'+LayerID);
	
	if($SelectorObj.val() == TargetIndex)
	{
		$LayerObj.css('display','inline');
	}else
	{
		$LayerObj.css('display','none');
	}
}
}

// ajax function
{
function Get_OTRecord_Summary_List()
{
	var PostVar = {
			"Keyword": encodeURIComponent($('Input#Keyword').val()),
			"TargetDate": $('Input#TargetDate').val(),
			"StaffType": $('#StaffType').val()
		}
	
	if(document.getElementById('TargetDate'))
	{
		if(!Check_Valid_Date('TargetDate','DateWarningLayer'))
			return;
	}
	Block_Element("OTRecordLayer");
	$.post('ajax_get_ot_record_summary_list.php',PostVar,
					function(data){
						if (data == "die") 
							window.top.location = '/';
						else {
							$('div#OTRecordLayer').html(data);
							Thick_Box_Init();
							UnBlock_Element("OTRecordLayer");
						}
					});
}

function Get_OTRecords_OTRecord_List(UserID)
{
	var StaffID = $('Input#UserID').val() || UserID;
	var PostVar = {
			"UserID": StaffID,
			"Keyword": encodeURIComponent($('Input#Keyword').val()),
			"SelectType": $('#SelectType').val(),
			"StartDate": $('Input#TargetStartDate').val(),
			"EndDate": $('Input#TargetEndDate').val(),
			"TargetDate": $('Input#TargetDate').val()
		}
	
	if(document.getElementById('TargetStartDate') && $('#SelectType').val() == 4)
	{
		var dateStartObj = document.getElementById('TargetStartDate');
		var dateEndObj = document.getElementById('TargetEndDate');
		
		//if(!Check_Valid_Date('TargetStartDate','DateWarningLayer')
		//  || !Check_Valid_Date('TargetEndDate','DateWarningLayer'))
		if(!check_date_without_return_msg(dateStartObj) || !check_date_without_return_msg(dateEndObj))
		{
			$('#DateWarningLayer').html('<?=$Lang['LessonAttendance']['InvalidDateFormat']?>');
			$('#DateWarningLayer').css('visibility','visible');
			SetTimerToHideWarning('DateWarningLayer', 3000);
			return;
		}
		
		if($('Input#TargetStartDate').val() > $('Input#TargetEndDate').val())
		{
			$('#DateWarningLayer').html('<?=$Lang['StaffAttendance']['InvalidDateRange']?>');
			$('#DateWarningLayer').css('visibility','visible');
			SetTimerToHideWarning('DateWarningLayer', 3000);
			return;
		}
	}
	
	Block_Element("OTRecordLayer");
	$.post('ajax_get_otrecords_ot_record_list.php',PostVar,
					function(data){
						if (data == "die") 
							window.top.location = '/';
						else {
							$('div#OTRecordLayer').html(data);
							Thick_Box_Init();
							UnBlock_Element("OTRecordLayer");
						}
					});
}

function Get_OTRecords_RedeemRecord_List(UserID)
{
	var StaffID = $('Input#UserID').val() || UserID;
	var PostVar = {
			"UserID": StaffID,
			"Keyword": encodeURIComponent($('Input#Keyword').val()),
			"SelectType": $('#SelectType').val(),
			"StartDate": $('Input#TargetStartDate').val(),
			"EndDate": $('Input#TargetEndDate').val(),
			"TargetDate": $('Input#TargetDate').val()
		}
	
	if(document.getElementById('TargetStartDate') && $('#SelectType').val() == 4)
	{
		var dateStartObj = document.getElementById('TargetStartDate');
		var dateEndObj = document.getElementById('TargetEndDate');
		
		//if(!Check_Valid_Date('TargetStartDate','DateWarningLayer')
		//  || !Check_Valid_Date('TargetEndDate','DateWarningLayer'))
		if(!check_date_without_return_msg(dateStartObj) || !check_date_without_return_msg(dateEndObj))
		{
			$('#DateWarningLayer').html('<?=$Lang['LessonAttendance']['InvalidDateFormat']?>');
			$('#DateWarningLayer').css('visibility','visible');
			SetTimerToHideWarning('DateWarningLayer', 3000);
			return;
		}
		
		if($('Input#TargetStartDate').val() > $('Input#TargetEndDate').val())
		{
			$('#DateWarningLayer').html('<?=$Lang['StaffAttendance']['InvalidDateRange']?>');
			$('#DateWarningLayer').css('visibility','visible');
			SetTimerToHideWarning('DateWarningLayer', 3000);
			return;
		}
	}
	
	Block_Element("OTRecordLayer");
	$.post('ajax_get_otrecords_redeem_record_list.php',PostVar,
					function(data){
						if (data == "die") 
							window.top.location = '/';
						else {
							$('div#OTRecordLayer').html(data);
							Thick_Box_Init();
							UnBlock_Element("OTRecordLayer");
						}
					});
}

function Get_Redeem_OT_Record_Form(UserID, Date)
{
	var Date = $('Input#TargetDate').val() || Date;
	var PostVar = {
		"UserID": UserID,
		"Date": Date
	};
	
	$.post('ajax_get_redeem_ot_record_form.php',PostVar,
		function(data) {
			if (data == "die") 
				window.top.location = '/';
			else 
				$('div#TB_ajaxContent').html(data);
		});
}

function Check_Redeem_OT_Record()
{
	var RedeemDate = $('Input#RedeemDate').val() || "";
	var PostVar = {
			"UserID": $('Input#RedeemStaffID').val(),
			"RedeemDate": RedeemDate,
			"MinsRedeemed": encodeURIComponent($('Input#MinsRedeemed').val()),
			"RedeemRemark": encodeURIComponent($('Input#RedeemRemark').val())
			};
	var err = false;
	var DateWarningLayer = $('div#RedeemDateWarningLayer');
	var MinsRedeemedWarningLayer =  $('div#MinsRedeemedWarningLayer');
	var MinsRedeemed = parseInt($('Input#MinsRedeemed').val());
	var RedeemDateWarningRow = $('#RedeemDateWarningRow');
	var MinsRedeemedWarningRow = $('#MinsRedeemedWarningRow');
	var RemarkWarningLayer = $('#RemarkWarningLayer');
	var RemarkWarningRow = $('#RemarkWarningRow');
	
	var dateObj = document.getElementById('RedeemDate');
	if(!check_date_without_return_msg(dateObj))
	{
		RedeemDateWarningRow.css('display','');
		DateWarningLayer.html('Invalid Date Format');
		DateWarningLayer.css('visibility','visible');
		DateWarningLayer.show('fast');
		err = true;
	}else
	{
		RedeemDateWarningRow.css('display','none');
		DateWarningLayer.html('');
		DateWarningLayer.css('visibility','hidden');
	}
	
	if(MinsRedeemed <= 0 || isNaN(MinsRedeemed) || document.getElementById('MinsRedeemed').value.indexOf('.')>=0)
	{
		MinsRedeemedWarningRow.css('display','');
		MinsRedeemedWarningLayer.html('<?=$Lang['StaffAttendance']['InvalidRedeemWarning']?>');
		MinsRedeemedWarningLayer.css('visibility','visible');
		MinsRedeemedWarningLayer.show('fast');
		err = true;
	}else
	{
		MinsRedeemedWarningRow.css('display','none');
		MinsRedeemedWarningLayer.html('');
		MinsRedeemedWarningLayer.css('visibility','hidden');
	}
	
	if(err) return;
	
	
	$.post('ajax_check_ot_record_for_redeem.php',PostVar,
			function(data){
				if (data == "die") 
					window.top.location = '/';
				else if (data == "1") {
					RemarkWarningLayer.html('');
					RemarkWarningLayer.hide();
					RemarkWarningRow.css('display','none');
					Save_Redeem_OT_Record();
				}
				else {
					RemarkWarningLayer.html('<?=$Lang['StaffAttendance']['NoOTRecordWarning']?>');
					RemarkWarningLayer.show('fast');
					RemarkWarningRow.css('display','');
				}
			});
	
}

function Save_Redeem_OT_Record()
{
	var UserID = $('Input#RedeemStaffID').val();
	var PostVar = {
			"UserID": UserID,
			"RedeemDate": $('Input#RedeemDate').val(),
			"MinsRedeemed": $('Input#MinsRedeemed').val(),
			"RedeemRemark": encodeURIComponent($('Input#RedeemRemark').val())
			}
	
	Block_Thickbox();
	$.post('ajax_save_redeem_record.php',PostVar,
		function(data){
			if (data == "die") 
				window.top.location = '/';
			else {
				Get_Return_Message(data);
				Get_OTRecords_RedeemRecord_List(UserID);
				window.top.tb_remove();
				UnBlock_Thickbox();
			}
		});
}
}

// thick box function 
{
//on page load call tb_init
function Thick_Box_Init()
{   
	tb_init('a.thickbox');//pass where to apply thickbox
}
}
</script>