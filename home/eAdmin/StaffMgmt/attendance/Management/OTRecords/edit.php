<?php
// Editing by 
/*
 * 2017-06-14 (Carlos): Added checkbox [No need to check duty] to allow add OT times for no duty days.
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend2.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!$StaffAttend3->Check_Access_Right('MGMT-OT'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0) || !$sys_custom['StaffAttendance']['WongFutNamCollege']) {
	intranet_closedb();
	header ("Location: /");
	exit();
}
$linterface = new interface_html();
$StaffAttend3UI = new libstaffattend3_ui();

$is_edit = isset($RecordID) && count($RecordID)>0 ? 1: 0;

if($is_edit){
	$records = $StaffAttend3->Get_OT_Records(array('RecordID'=>$RecordID));
	$StaffID = $records[0]['StaffID'];
	$StaffName = $records[0]['StaffName'];
	$RecordDate = $records[0]['RecordDate'];
	$OTmins = $records[0]['OTmins'];
	$hidden_id_fields = '';
	$hidden_id_fields .= '<input type="hidden" id="RecordID" name="RecordID" value="'.$records[0]['RecordID'].'" />';
}else{
	$OTmins = 60;
}

if($is_edit)
{
	$staff_selection = $StaffName.'<input type="hidden" name="StaffID" id="StaffID" value="'.$StaffID.'" />';
}else{
	$staff_selection = $StaffAttend3UI->Get_Staff_Selection('StaffID','StaffID',$__ParGroup=true,$__ParIndividual=true,$__ParIsMultiple=false,$__ParSize=20,$__ParSelectedValue=$StaffID,$__ParOthers="",$__ParAllStaffOption=false,$__ShowOptGroupLabel=true);
}

$CurrentPageArr['eAdminStaffAttendance'] = 1;
$CurrentPage['OTRecords'] = 1;

$pages_arr = array(
	array($Lang['StaffAttendance']['OTRecords'],'./'),
	array($is_edit? $Lang['Btn']['Edit'] : $Lang['Btn']['New'], '')
);

$TAGS_OBJ[] = array($Lang['StaffAttendance']['OTRecords'], "", 0);
$MODULE_OBJ = $StaffAttend3UI->GET_MODULE_OBJ_ARR();
//$TAGS_OBJ_RIGHT[] = array(gen_online_help_btn_and_layer('staff_attendance','ot_record'));
$linterface->LAYOUT_START(urldecode($_REQUEST['Msg']));
echo $StaffAttend3UI->Include_JS_CSS();
?>
<?=$linterface->GET_NAVIGATION_IP25($pages_arr)?>
<br />
<form name="form1" id="form1" action="edit_update.php" method="post" onsubmit="return false;">
<?=$hidden_id_fields?>
<div id="CheckResultLayer" style="display:none;">
<?=$linterface->Get_Warning_Message_Box($__title=$Lang['StaffAttendance']['CheckingResult'], $__content="", $__others=' id="CheckResultMsg" ');?>
</div>
<table width="100%" cellpadding="2" class="form_table_v30">
	<thead>
		<col class="field_title">
		<col class="field_c">
	</thead>
	<tbody>
		<tr>
			<td width="30%" valign="top" nowrap="nowrap" class="field_title">
				<?=$Lang['StaffAttendance']['Staff']?> <span class="tabletextrequire">*</span>
			</td>
			<td>
				<?=$staff_selection?>
				<?=$linterface->Get_Thickbox_Warning_Msg_Div("StaffID_Error",$Lang['StaffAttendance']['EntryLogSelectStaffWarning'], "WarnMsg")?>
			</td>
		</tr>
		<tr>
			<td width="30%" valign="top" nowrap="nowrap" class="field_title">
				<?=$Lang['General']['Date']?> <span class="tabletextrequire">*</span>
			</td>
			<td>
				<?=$linterface->GET_DATE_PICKER("RecordDate",$RecordDate)?>
				<?=$linterface->Get_Thickbox_Warning_Msg_Div("RecordDate_Error",$Lang['StaffAttendance']['TargetDateSelectWarning'], "WarnMsg")?>
			</td>
		</tr>
		<tr>
			<td width="30%" valign="top" nowrap="nowrap" class="field_title">
				<?=$Lang['StaffAttendance']['TotalOTMins']?> <span class="tabletextrequire">*</span>
			</td>
			<td>
				<?=$linterface->GET_TEXTBOX_NUMBER("OTmins", "OTmins", $OTmins, '', array('onchange'=>'restrictNonZeroPositiveNumber(this);')).'&nbsp;'.$Lang['StaffAttendance']['Mins']?>
			</td>
		</tr>
		<tr>
			<td width="30%" valign="top" nowrap="nowrap">
				&nbsp;
			</td>
			<td>
				<div style="margin-bottom:8px;">
					<?=$linterface->Get_Radio_Button("CheckDutyType1", "CheckDutyType", "1", $___isChecked=1, $___Class="", $___Display=$Lang['StaffAttendance']['CheckDuty'], $___Onclick="onCheckDutyTypeChanged(1);",$___isDisabled=0)?>
					<?=$linterface->Get_Radio_Button("CheckDutyType2", "CheckDutyType", "2", $___isChecked=0, $___Class="", $___Display=$Lang['StaffAttendance']['OnlyCheckTapCardTimes'], $___Onclick="onCheckDutyTypeChanged(2);",$___isDisabled=0)?>
					<?=$linterface->Get_Radio_Button("CheckDutyType3", "CheckDutyType", "3", $___isChecked=0, $___Class="", $___Display=$Lang['StaffAttendance']['NoNeedToCheckDutyAndTapCardTimes'], $___Onclick="onCheckDutyTypeChanged(3);",$___isDisabled=0)?>
				</div>
				<?=$linterface->GET_SMALL_BTN($Lang['StaffAttendance']['CheckRecord'], "button", "checkSubmitForm(document.form1,false);","checkBtn");?>
			</td>
		</tr>
	</tbody>
</table>
<?=$linterface->MandatoryField()?>		
<div class="edit_bottom_v30">
<?=$linterface->GET_ACTION_BTN($Lang['Btn']['Submit'],"button", "checkSubmitForm(document.form1,true);",'submitBtn').'&nbsp;'?>
<?=$linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'],"button","window.location.href='ot.php'",'cancelBtn')?>
</div>
</form>
<script type="text/javascript" language="javascript">
function restrictNonZeroPositiveNumber(obj)
{
	var val = $.trim(obj.value);
	var num = parseInt(val);
	
	if(isNaN(num) || val == ''){
		num = 60;
	}
	if(num <= 0){
		num = 60;
	}
	obj.value = num;
}

function onCheckDutyTypeChanged(check_duty_type)
{
	if(check_duty_type == '3'){
		$('#checkBtn').hide();
	}else{
		$('#checkBtn').show();
	}
}

function checkOTValidity(callback)
{
	Block_Element('form1');
	$('#CheckResultLayer').hide();
	$.post(
		'ajax_task.php',
		{
			'task':'checkValidity',
			'StaffID':$('#StaffID').val(),
			'RecordDate':$.trim($('#RecordDate').val()),
			'OTmins':$('#OTmins').val(),
			'CheckDutyType':$('input[name="CheckDutyType"]:checked').val()
		},
		function(returnData){
			var return_ary = [];
			if(JSON && JSON.parse)
			{
				return_ary = JSON.parse(returnData);
			}else{
				eval('return_ary='+returnData);
			}
			var result_code = return_ary[0][0];
			var msg = return_ary[0][1];
			var valid = true;
			if(result_code == 0){
				valid = false; 
				msg = '<span style="color:red">'+msg+'</span>';
			}else{
				msg = '<span style="color:green">'+msg+'</span>';
			}
			$('#CheckResultMsg').html(msg);
			$('#CheckResultLayer').show();
			UnBlock_Element('form1');
			if(callback && typeof(callback)=='function'){
				callback(valid);
			}
		}
	);
	
}

function checkDuplication(callback)
{
	Block_Element('form1');
	$('#CheckResultLayer').hide();
	var postData = {
			'task':'checkDuplication',
			'StaffID':$('#StaffID').val(),
			'RecordDate':$.trim($('#RecordDate').val())
		};
	if($('#RecordID').length>0){
		postData['NotRecordID'] = $('#RecordID').val();
	}
	$.post(
		'ajax_task.php',
		postData,
		function(returnData){
			var record_count = parseInt(returnData);
			var msg = '';
			var valid = true;
			if(record_count > 0){
				valid = false;
				msg = '<span style="color:red"><?=$Lang['StaffAttendance']['DuplicatedOTRecord']?></span>';
				$('#CheckResultMsg').html(msg);
			}else{
				//msg = '<span style="color:green"><?=$Lang['StaffAttendance']['OTRecordIsValid']?></span>';
			}
			//$('#CheckResultMsg').html(msg);
			$('#CheckResultLayer').show();
			UnBlock_Element('form1');
			if(callback && typeof(callback)=='function'){
				callback(valid);
			}
		}
	);
}

function checkSubmitForm(formObj,doSubmit)
{
	var is_valid = true;
	var submit_func = function(){
		Block_Element('form1');
		formObj.submit();
	};
	if($('#NoNeedToCheckDuty').is(':checked'))
	{
		checkDuplication(function(valid2){
			if(valid2 && doSubmit){
				submit_func();
			}
		});
	}else{
		checkOTValidity(function(valid1){
			if(valid1){
				checkDuplication(function(valid2){
					if(valid2 && doSubmit){
						submit_func();
					}
				});
			}
		});
	}
}
</script>
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>