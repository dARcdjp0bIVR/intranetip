<?php
// editing by Kenneth Chung
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!$StaffAttend3->Check_Access_Right('MGMT-OT'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$StaffID = $_REQUEST['UserID'];
$RedeemDate = $_REQUEST['RedeemDate'];
$MinsRedeemed = $_REQUEST['MinsRedeemed'];
$RedeemRemark = trim(urldecode(stripslashes($_REQUEST['RedeemRemark'])));

//$StaffAttend3->Start_Trans();

if($StaffAttend3->Save_Redeem_Record($StaffID, $RedeemDate, $MinsRedeemed, $RedeemRemark))
{
	//$StaffAttend3->Commit_Trans();
	echo $Lang['StaffAttendance']['RedeemRecordSuccess'];
}else
{
	//$StaffAttend3->Rollback_Trans();
	echo $Lang['StaffAttendance']['RedeemRecordFail'];
}
intranet_closedb();
?>