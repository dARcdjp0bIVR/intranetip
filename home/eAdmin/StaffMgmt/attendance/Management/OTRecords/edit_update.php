<?php
// Editing by 
/*
 * 2017-06-14 (Carlos): if $NoNeedToCheckDuty is checked, by pass duty checking. 
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend2.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!$StaffAttend3->Check_Access_Right('MGMT-OT'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0) || !$sys_custom['StaffAttendance']['WongFutNamCollege']) {
	intranet_closedb();
	//header ("Location: /");
	exit();
}

$is_edit = isset($_POST['RecordID']) && count($_POST['RecordID'])>0;

if(!$is_edit){
	
	$staff_id = $_POST['StaffID'];
	$record_date = $_POST['RecordDate'];
	
	$duty_count = 0;
	if($_POST['CheckDutyType']==1)
	{
		$duty = $StaffAttend3->Get_Duty_By_Date($staff_id,$record_date,$CreateCardLog=false,$IgnoreFullDayHoliday=true,$FreezeDuty=false,$CacheResult=false);
		if(isset($duty['CardUserToDuty']) && isset($duty['CardUserToDuty'][$staff_id])){
			$duty_count = count($duty['CardUserToDuty'][$staff_id]);
		}
		
		if($duty_count==0){
			// no duty
			$Msg = $is_edit? $Lang['General']['ReturnMessage']['UpdateUnsuccess'] : $Lang['General']['ReturnMessage']['AddUnsuccess'];
			$_SESSION['STAFF_ATTENDANCE_OT_RETURN_MSG'] = $Msg;
			
			intranet_closedb();
			header("Location: ot.php");
			exit;
		}
	}
}

$success = $StaffAttend3->Upsert_OT_Record($_POST);

if($success){
	$Msg = $is_edit? $Lang['General']['ReturnMessage']['UpdateSuccess'] : $Lang['General']['ReturnMessage']['AddSuccess'];
}else{
	$Msg = $is_edit? $Lang['General']['ReturnMessage']['UpdateUnsuccess'] : $Lang['General']['ReturnMessage']['AddUnsuccess'];
}
$_SESSION['STAFF_ATTENDANCE_OT_RETURN_MSG'] = $Msg;

intranet_closedb();
header("Location: ot.php");
exit;
?>