<?php
// editing by 
/*
 * 2019-08-13 Ray: Add update log
 * 2012-08-20 (Carlos): Added parameter DutyCount 
 */
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!$StaffAttend3->Check_Access_Right('MGMT-Attendance-Manage'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$RecordID = $_REQUEST['RecordID'];
$Remark = $_REQUEST['Remark'];
$Waived = $_REQUEST['Waived'];
$ReasonLate = $_REQUEST['ReasonLate'];
$ReasonAbsent = $_REQUEST['ReasonAbsent'];
$ReasonEarlyLeave = $_REQUEST['ReasonEarlyLeave'];
$Status = $_REQUEST['Status'];
$TargetDate = $_REQUEST['SubmitTargetDate'];
$RecordType = $_REQUEST['RecordType'];
$ProfileID = $_REQUEST['ProfileID'];
$StaffID = $_REQUEST['StaffID'];
$DutyCount =$_REQUEST['DutyCount'];

$StaffAttend3->Start_Trans();
if($StaffAttend3->isRecordChangeLogEnabled()) {
	for ($i=0; $i< sizeof($RecordID); $i++) {
		$params = array();
		$params['RecordID'] = $RecordID[$i];
		$params['TargetUserID'] = $StaffID[$i];
		$params['TargetDate'] = $TargetDate;
		$params['StatusType'] = array();
		$temp_result = $StaffAttend3->Get_Overall_Daily_Attendance_Records($params);
		if (sizeof($temp_result) > 0) {
			if ($Status[$RecordID[$i]] != "" && $temp_result[0]['Status'] != $Status[$RecordID[$i]]) {
				$StatusList = $StaffAttend3->Get_Attendance_Status_Settings();
				$RecordDetail = array();
				$RecordDetail['Name'] = $temp_result[0]['StaffName'];
				$RecordDetail['Date'] = $TargetDate;
				$RecordDetail['Time_Slot'] = $temp_result[0]['SlotName'].'('.$temp_result[0]['SlotStart'].'-'.$temp_result[0]['SlotEnd'].')';
				$RecordDetail['From_Status'] = $StatusList[$temp_result[0]['Status']]['StatusDisplay'];
				$RecordDetail['To_Status'] = $StatusList[$Status[$RecordID[$i]]]['StatusDisplay'];

				$OriginalRecordDetail = array();
				$OriginalRecordDetail['InSchoolStatus'] = $temp_result[0]['InSchoolStatus'];
				$OriginalRecordDetail['OutSchoolStatus'] = $temp_result[0]['OutSchoolStatus'];
				$OriginalRecordDetail['InTime'] = $temp_result[0]['InTime'];
				$OriginalRecordDetail['OutTime'] = $temp_result[0]['OutTime'];
				$StaffAttend3->INSERT_UPDATE_LOG('Attendance_Record', $RecordDetail, $temp_result[0]['RecordID'], $OriginalRecordDetail);
			}
		}
	}
}



if ($StaffAttend3->Save_Profile_Set($RecordID,$Status,$Waived,$ReasonLate,$ReasonAbsent,$ReasonEarlyLeave,$Remark,$ProfileID,$RecordType,$TargetDate,$StaffID,$DutyCount)) {

	$Msg = $Lang['General']['ReturnMessage']['UpdateSuccess'];
	$StaffAttend3->Commit_Trans();
}
else {
	$Msg = $Lang['General']['ReturnMessage']['UpdateUnsuccess'];
	$StaffAttend3->RollBack_Trans();
}

intranet_closedb();
$return_url = "index.php?TargetDate=".$TargetDate."&RecordType=".$RecordType."&Msg=".urlencode($Msg);
header("Location: $return_url");
?>