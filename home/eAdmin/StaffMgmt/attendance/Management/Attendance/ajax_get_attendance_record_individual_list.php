<?php
// editing by 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!$StaffAttend3->Check_Access_Right('MGMT-Attendance'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$StaffAttend3UI = new libstaffattend3_ui();

$TargetDate = $_REQUEST['TargetDate'];
$DutyType = $_REQUEST['DutyType'];
$StatusType = $_REQUEST['StatusType'];
$Keyword = trim(urldecode(stripslashes($_REQUEST['Keyword'])));
$StaffID = $_REQUEST['StaffID'];
$SlotName = trim(urldecode(stripslashes($_REQUEST['SlotName'])));

$Date = explode('-',$TargetDate);
$Year = $Date[0];
$Month = $Date[1];
$Day = $Date[2];

echo $StaffAttend3UI->Get_Attendance_Record_Individual_List($Keyword, $Year, $Month, $Day, $DutyType, $StatusType, $StaffID, $SlotName);

intranet_closedb();
?>