<?php
// editing by 
/************************************************* Changes ********************************************************
 * 2015-12-03 (Carlos): Do datetime field format conversion with the global function convertDateTimeToStandardFormat(). (Need to update this page with lib.php)
 * 2014-03-27 (Carlos): Change [Real Time Staff Status] to [Staff In-School / In-Out Status]
 * 2011-11-03 (Carlos): format SmartCardID to 10 digits
 ******************************************************************************************************************/
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend2.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");
if ($format == 2)
	include_once($PATH_WRT_ROOT."includes/libimporttext-kiosk.php");
else
	include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!$StaffAttend3->Check_Access_Right('MGMT-Attendance-Manage') && !$StaffAttend3->Check_Access_Right('REPORT-DAILYLOG'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$linterface = new interface_html();
$StaffAttend3UI = new libstaffattend3_ui();
$limport = new libimporttext();
$lf = new libfilesystem();

$format_array = array("Time","Site","Card ID");
$filepath = $_FILES["userfile"]["tmp_name"];
$filename = $_FILES["userfile"]["name"];

if($filepath=="none" || $filepath == "")
{
	# import failed
    header("Location: import.php?Msg=".urlencode($Lang['StaffAttendance']['DataImportFail']));
    intranet_closedb();
    exit();
}else
{
	$ext = strtoupper($lf->file_ext($filename));
    if($limport->CHECK_FILE_EXT($filename))
    {
        # read file into array
        # return 0 if fail, return csv array if success
        //$data = $lf->file_read_csv($filepath);
        $data = $limport->GET_IMPORT_TXT($filepath);
        if(sizeof($data)>0)
        {
        	$toprow = array_shift($data);                   # drop the title bar
        }else
        {
        	header("Location: import.php?Msg=".urlencode($Lang['StaffAttendance']['DataImportFail']));
		    intranet_closedb();
		    exit();
        }
    }
    
    // Create temp table
    $sql = "DROP TABLE TEMP_CARD_STAFF_LOG";
    $StaffAttend3->db_db_query($sql);
    $sql = "CREATE TABLE TEMP_CARD_STAFF_LOG (
             RecordDate date,
             RecordedTime datetime,
             UserID int,
             CardID varchar(255),
             SiteName varchar(255)
            )ENGINE=InnoDB DEFAULT CHARSET=utf8 ";
    $StaffAttend3->db_db_query($sql);

    # CardID -> UserID
    $sql = "SELECT CardID, UserID, RecordType FROM INTRANET_USER
                   WHERE RecordStatus = 1  
                         AND CardID != ''";
    $temp = $StaffAttend3->returnArray($sql,3);
    
    for ($i=0; $i<sizeof($temp); $i++)
    {
         list($targetCardID, $targetUserID, $targetRecordType) = $temp[$i];
         $targetCardID = FormatSmartCardID($targetCardID);
         $staff[$targetCardID] = array("UserID"=>$targetUserID,"RecordType"=>$targetRecordType);
    }
	
	$error_array = array();
    $values = "";
    $delim = "";
    // Retrieve data from file
    for ($i=0; $i<sizeof($data); $i++)
    {
         list($time,$site,$cardid) = $data[$i];
         $cardid = FormatSmartCardID($cardid);
         $time = convertDateTimeToStandardFormat($time);
         $ts = strtotime($time);             
         if($ts==-1 || preg_match('/^\d\d\d\d-\d\d-\d\d \d\d:\d\d:\d\d$/', $time)==0)
         { ## invalid time format
     		$error_array[] = array("RecordDetail"=>$data[$i],"RowNumber"=>($i+2),"ErrorMsg"=>$Lang['StaffAttendance']['SlotTimeFormatWarning']);
        	//break;
        	continue;
         }
         # Further check datetime format
         $tempYear = intval(substr($time,0,4));
         $tempMonth = intval(substr($time,5,2));
         $tempDay = intval(substr($time,8,2));
         $tempHour = intval(substr($time,11,2));
         $tempMinute = intval(substr($time,14,2));
         $tempSecond = intval(substr($time,17,2));
         //echo $tempYear." ### ".$tempMonth." ### ".$tempDay." ".$time."<br />";
         if(!checkdate($tempMonth, $tempDay, $tempYear) || $tempHour >= 24 || $tempMinute >= 60 || $tempSecond >= 60)
         {
         	$error_array[] = array("RecordDetail"=>$data[$i],"RowNumber"=>($i+2),"ErrorMsg"=>$Lang['StaffAttendance']['SlotTimeFormatWarning']);
        	//break;
        	continue;
         }
         $date = date('Y-m-d',$ts);
         $formated_time = date('Y-m-d H:i:s',$ts);
         $targetUserID = $staff[$cardid]["UserID"];
         if ($targetUserID != '')
         {
             if($staff[$cardid]["RecordType"]==1)
             {
	             $values .= "$delim('$date','$formated_time','".$StaffAttend3->Get_Safe_Sql_Query($site)."','$cardid','$targetUserID')";                 
	             $delim = ",";
             }
         }else
         {
         	$error_array[] = array("RecordDetail"=>$data[$i],"RowNumber"=>($i+2),"ErrorMsg"=>"*".$Lang['StaffAttendance']['StaffNotExists']);
         	continue;
         }
    }
    
    // Insert into temp table
    $sql = "INSERT INTO TEMP_CARD_STAFF_LOG (RecordDate,RecordedTime,SiteName,CardID, UserID) VALUES $values";
    $StaffAttend3->db_db_query($sql);
}

$name_field = getNameFieldByLang("b.");
$sql = "SELECT 
			a.RecordDate,
			DATE_FORMAT(a.RecordedTime,'%H:%i:%s') as RecordTime,
			a.SiteName,
			IF(a.CardID IS NULL, b.CardID, a.CardID) as CardID,
			$name_field as StaffName
        FROM TEMP_CARD_STAFF_LOG as a
        LEFT OUTER JOIN INTRANET_USER as b 
        	ON b.RecordType = 1
        	AND a.UserID = b.UserID 
        ORDER By b.EnglishName ";
$result = $StaffAttend3->returnArray($sql,5);

$sql = "SELECT DISTINCT RecordDate FROM TEMP_CARD_STAFF_LOG";
$temp = $StaffAttend3->returnVector($sql);
$noValidRecord = sizeof($temp);

$CurrentPageArr['eAdminStaffAttendance'] = 1;
$CurrentPage['Attendance'] = 1;
/*
$TAGS_OBJ[] = array($Lang['StaffAttendance']['AttendanceRecord'],$PATH_WRT_ROOT."home/eAdmin/StaffMgmt/attendance/Management/Attendance/",0);
$TAGS_OBJ[] = array($Lang['StaffAttendance']['ViewLateAbsentEarlyList'],$PATH_WRT_ROOT."home/eAdmin/StaffMgmt/attendance/Management/Attendance/daily_profile_list/",0);
$TAGS_OBJ[] = array($Lang['StaffAttendance']['InSchoolInOutStatus'],$PATH_WRT_ROOT."home/eAdmin/StaffMgmt/attendance/Management/Attendance/real_time_staff_status/",0);
$TAGS_OBJ[] = array($Lang['StaffAttendance']['ImportOfflineRecords'],$PATH_WRT_ROOT."home/eAdmin/StaffMgmt/attendance/Management/Attendance/offline/",1);
*/
$TAGS_OBJ = $StaffAttend3UI->Get_Management_DailyRecord_Navigation_Tabs('ImportOfflineRecords');
$MODULE_OBJ = $StaffAttend3UI->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START(urldecode($_REQUEST['Msg']));

echo $StaffAttend3UI->Get_Import_Offline_Records_Confirm_Page($result, $noValidRecord, $error_array);

$linterface->LAYOUT_STOP();
intranet_closedb();
?>