<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!$StaffAttend3->Check_Access_Right('MGMT-Attendance'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$StaffAttend3 = new libstaffattend3();

$StaffID = $_REQUEST['StaffID'];
$TargetDate = $_REQUEST['TargetDate'];
$Date = explode('-',$TargetDate);
$Year = $Date[0];
$Month = $Date[1];
$Day = $Date[2];

$SlotList = $StaffAttend3->Get_Attendance_Group_Slot_List($StaffID, $Year, $Month, $Day, "Individual");

$Slot_Collection = '<select id="SelectSlot" name="SelectSlot">';
for($i=0;$i<sizeof($SlotList);$i++)
{
	$Slot_Collection .= '<option value="'.$SlotList[$i]['SlotName'].'" '.$Selected.'>'.$SlotList[$i]['SlotName'].'</option>';
}
$Slot_Collection .= '</select>';

echo $Slot_Collection;

intranet_closedb();
?>