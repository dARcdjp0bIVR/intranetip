<?php
// editing by 
/*
 * 2019-03-22 Carlos: Changed to use div instead of iframe.
 * 2018-01-19 Carlos: Created.
 */
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!$StaffAttend3->Check_Access_Right('MGMT-Attendance'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	//echo 'die';
	intranet_closedb();
	exit();
}

$CARD_STATUS_LATEEARLYLEAVE = 6;
$CARD_STATUS_ABSENT_SUSPECTED = 7;

$PageLoadTime = time()+1;

$StaffAttend3 = new libstaffattend3();
$StaffAttend3UI = new libstaffattend3_ui();

$params = array('StaffIDToRecords'=>true,'TargetDate'=>$_POST['TargetDate'],'StatusType'=>$_POST['StatusType'],'RecordID'=>$_POST['RecordID']);
$staffIdToRecords = $StaffAttend3->Get_Overall_Daily_Attendance_Records($params);

$StaffList = array_keys($staffIdToRecords);
//debug_pr($staffIdToRecords);

$x .= '<div class="table_board">
		<table width="100%" cellspacing="0" cellpadding="0" border="0">
		<tbody>
			<tr>
				<td>
	  			<table width="100%" cellspacing="0" cellpadding="0" border="0">
					<tbody>
						<tr>
							<td valign="bottom">
							<div class="common_table_tool">
								<a href="javascript:void(0);" class="tool_set" title="'.$Lang['StaffAttendance']['ChangeStatus'].'" onclick="ShowHideBatchStatusOption(this)">'.$Lang['StaffAttendance']['ChangeStatus'].'</a>
								<a href="javascript:void(0);" class="tool_set" title="'.$Lang['StaffAttendance']['SetAbsentToPresent'].'" onclick="Set_Absent_To_Present()">'.$Lang['StaffAttendance']['SetAbsentToPresent'].'</a>
							</div>
						</td>
						</tr>
					</tbody>
				</table>
				</td>
			</tr>
			<tr>
				<td>';
		 $x .= '<table class="common_table_list">
                  <thead>
                    <tr>
                      	<th>'.$Lang['StaffAttendance']['StaffName'].'</th>
	                    <th class="sub_row_top">'.$Lang['StaffAttendance']['WorkingTimeSlotHeader'].'</th>
	                    <th class="sub_row_top">'.$Lang['StaffAttendance']['Duty'].'</th>
	                    <th class="sub_row_top">'.$Lang['StaffAttendance']['In'].'</th>
	                    <th class="sub_row_top">'.$Lang['StaffAttendance']['Out'].'</th>
						<th class="sub_row_top">'.$Lang['StaffAttendance']['InStation'].'</th>
	                    <th class="sub_row_top">'.$Lang['StaffAttendance']['OutStation'].'</th>
	                    <th class="sub_row_top">'.$Lang['StaffAttendance']['Status'].'</th>
	                    <th class="sub_row_top">'.$Lang['StaffAttendance']['Reason'].'</th>
						<th class="sub_row_top">'.$Lang['StaffAttendance']['Waived'].'</th>
						<th class="sub_row_top">'.$Lang['StaffAttendance']['Remark'].'</th>
	                    <th class="sub_row_top">'.$Lang['StaffAttendance']['LastUpdated'].'</th>
						<th class="sub_row_top"><span class="num_check">
                          <input name="checkall" id="checkall" type="checkbox" onclick="Set_Checkbox_Value(\'SelectedRecordID[]\',this.checked);" >
                      	</th>
                    </tr>
                  </thead>
                  <tbody>';
    
		$BatchStatusOptions = '<div style="visibility: hidden;" id="batch_status_options" class="selectbox_layer">
								<nobr class="select_attendance_status">
								<a href="javascript:void(0);" onclick="SetAllStatus('.CARD_STATUS_NORMAL.');"><img src="'.$PATH_WRT_ROOT.'images/'.$LAYOUT_SKIN.'/attendance/icon_present_s.png" title="'.$Lang['StaffAttendance']['Present'].'" align="absmiddle" border="0" width="24" height="20">'.$Lang['StaffAttendance']['Present'].'</a>
			    				<a href="javascript:void(0);" onclick="SetAllStatus('.CARD_STATUS_LATE.');"><img src="'.$PATH_WRT_ROOT.'images/'.$LAYOUT_SKIN.'/attendance/icon_late_s.png" title="'.$Lang['StaffAttendance']['Late'].'" align="absmiddle" border="0" width="24" height="20">'.$Lang['StaffAttendance']['Late'].'</a>
			   					<a href="javascript:void(0);" onclick="SetAllStatus('.CARD_STATUS_EARLYLEAVE.');"><img src="'.$PATH_WRT_ROOT.'images/'.$LAYOUT_SKIN.'/attendance/icon_early_leave_s.png" title="'.$Lang['StaffAttendance']['EarlyLeave'].'" align="absmiddle" border="0" width="24" height="20">'.$Lang['StaffAttendance']['EarlyLeave'].'</a>
			    				<a href="javascript:void(0);" onclick="SetAllStatus('.$CARD_STATUS_LATEEARLYLEAVE.');"><img src="'.$PATH_WRT_ROOT.'images/'.$LAYOUT_SKIN.'/attendance/icon_late_early_leave_s.png" title="'.$Lang['StaffAttendance']['Late'].' &amp; '.$Lang['StaffAttendance']['EarlyLeave'].'" align="absmiddle" border="0" width="24" height="20">'.$Lang['StaffAttendance']['Late'].' &amp; '.$Lang['StaffAttendance']['EarlyLeave'].'</a>
			     				<a href="javascript:void(0);" onclick="SetAllStatus('.CARD_STATUS_OUTGOING.');"><img src="'.$PATH_WRT_ROOT.'images/'.$LAYOUT_SKIN.'/attendance/icon_present_out_s.png" title="'.$Lang['StaffAttendance']['Outing'].'" align="absmiddle" border="0" width="24" height="20">'.$Lang['StaffAttendance']['Outing'].'</a>
			    				<a href="javascript:void(0);" onclick="SetAllStatus('.CARD_STATUS_ABSENT.');"><img src="'.$PATH_WRT_ROOT.'images/'.$LAYOUT_SKIN.'/attendance/icon_absent_s.png" title="'.$Lang['StaffAttendance']['Absent'].'" align="absmiddle" border="0" width="24" height="20">'.$Lang['StaffAttendance']['Absent'].'</a>
								<a href="javascript:void(0);" onclick="SetAllStatus('.CARD_STATUS_HOLIDAY.');"><img src="'.$PATH_WRT_ROOT.'images/'.$LAYOUT_SKIN.'/attendance/icon_present_out_s.png" title="'.$Lang['StaffAttendance']['Holiday'].'" align="absmiddle" border="0" width="24" height="20">'.$Lang['StaffAttendance']['Holiday'].'</a>
								</nobr>
							   </div>';
        $x .= $BatchStatusOptions;

      
	foreach($staffIdToRecords as $staff_id => $DataList)
	{
		for($j=0;$j<sizeof($DataList);$j++)
        {
        	if(strstr($DataList[$j]['InTime'],':'))
        	{
        		$InTime = explode(':',$DataList[$j]['InTime']);
        		$DisplayInTime = $InTime[0].':'.$InTime[1].':'.$InTime[2];
        	}else
        	{
        		$InTime = array('','','');
        		$DisplayInTime = "-";
        	}
        	
        	if(strstr($DataList[$j]['OutTime'],':'))
        	{
        		$OutTime = explode(':',$DataList[$j]['OutTime']);
        		$DisplayOutTime = $OutTime[0].':'.$OutTime[1].':'.$OutTime[2];
        	}else
        	{
        		$OutTime = array('','','');
        		$DisplayOutTime = "-";
        	}
        	
        	$record_id = $DataList[$j]['RecordID'];
        	
        	$style_hidden = '';
        	$reason0hidden = '';
        	$reason1hidden = '';
        	$reason0id = $DataList[$j]['InReasonID'];
        	$reason0text = $DataList[$j]['InReason']==''?'-':intranet_htmlspecialchars($DataList[$j]['InReason']);
        	$reason1id = $DataList[$j]['OutReasonID'];
        	$reason1text = $DataList[$j]['OutReason']==''?'-':intranet_htmlspecialchars($DataList[$j]['OutReason']);
        	switch($DataList[$j]['Status'])
        	{
        		case CARD_STATUS_NORMAL:
        		{
        			$icon_name = 'icon_present_s.png';
        			$status_text = $Lang['StaffAttendance']['Present'];
        			$waive_tooltip = $Lang['StaffAttendance']['Present'];
        			$reason0hidden = ' style="display:none;" ';
        			$reason1hidden = ' style="display:none;" ';
        			break;
        		}
        		case CARD_STATUS_ABSENT:
        		{
        			$icon_name = 'icon_absent_s.png';
        			$status_text = $Lang['StaffAttendance']['Absent'];
        			$waive_tooltip = $Lang['StaffAttendance']['Absent'];
        			$reason1hidden = ' style="display:none;" ';
        			break;
        		}
        		case CARD_STATUS_LATE:
        		{
        			$icon_name = 'icon_late_s.png';
        			$status_text = $Lang['StaffAttendance']['Late'];
        			$waive_tooltip = $Lang['StaffAttendance']['Late'];
        			$reason1hidden = ' style="display:none;" ';
        			break;
        		}
        		case CARD_STATUS_EARLYLEAVE:
        		{
        			$icon_name = 'icon_early_leave_s.png';
        			$status_text = $Lang['StaffAttendance']['EarlyLeave'];
        			$waive_tooltip = $Lang['StaffAttendance']['EarlyLeave'];
        			$reason0hidden = ' style="display:none;" ';
        			break;
        		}
        		case $CARD_STATUS_LATEEARLYLEAVE:
        		{
        			$icon_name = 'icon_late_early_leave_s.png';
        			$status_text = $Lang['StaffAttendance']['Late'].' &amp; '.$Lang['StaffAttendance']['EarlyLeave'];
        			$waive_tooltip = $Lang['StaffAttendance']['Late'];
        			
        			break;
        		}
        		case CARD_STATUS_OUTGOING:
        		{
        			$icon_name = 'icon_present_out_s.png';
        			$status_text = $Lang['StaffAttendance']['Outing'];
        			$waive_tooltip = $Lang['StaffAttendance']['Outing'];
        			$reason1hidden = ' style="display:none;" ';
        			break;
        		}
        		case CARD_STATUS_HOLIDAY:
        		{
        			$icon_name = 'icon_present_out_s.png';
        			$status_text = $Lang['StaffAttendance']['Holiday'];
        			$waive_tooltip = $Lang['StaffAttendance']['Holiday'];
        			$reason1hidden = ' style="display:none;" ';
        			break;
        		}
        		default:
        		{
        			$icon_name = 'icon_absent_suspect_s.png';
        			$status_text = $Lang['StaffAttendance']['AbsentSuspected'];
        			$waive_tooltip = $Lang['StaffAttendance']['AbsentSuspected'];
        			$reason0hidden = ' style="display:none;" ';
        			$reason1hidden = ' style="display:none;" ';
        			$style_hidden = ' style="display:none;" ';
        			break;
        		}
        	}
        	
			$StatusSelection = '<div class="selectbox_group" id="select_status_'.$record_id.'">
									<a href="javascript:void(0);" onclick="ShowHideStatusLayer('.$record_id.','.$DataList[$j]['Status'].');"><img src="'.$PATH_WRT_ROOT.'images/'.$LAYOUT_SKIN.'/attendance/'.$icon_name.'" title="'.$status_text.'" align="absmiddle" border="0" width="24" height="20">'.$status_text.'</a>
								</div>
								<input type="hidden" id="Status_'.$record_id.'" name="Status[]" value="'.$DataList[$j]['Status'].'" />
								<br style="clear: both;">';
    		
        	$reason0div = '<div class="selectbox_group" id="select_in_reason_'.$record_id.'">
							<a href="javascript:void(0);" onclick="ShowHideReasonLayer('.$record_id.',\'in\');">'.$reason0text.'</a>
						   </div>
						   <input type="hidden" id="in_reason_'.$record_id.'" name="InReason[]" value="'.$DataList[$j]['InReasonID'].'" />';
	        $reason1div = '<div class="selectbox_group" id="select_out_reason_'.$record_id.'">
							<a href="javascript:void(0);" onclick="ShowHideReasonLayer('.$record_id.',\'out\');">'.$reason1text.'</a>
							</div>
							<input type="hidden" id="out_reason_'.$record_id.'" name="OutReason[]" value="'.$DataList[$j]['OutReasonID'].'" />';
	        
			if($DataList[$j]['Status'] == CARD_STATUS_HOLIDAY)
			{
				$duty_text = $Lang['StaffAttendance']['Holiday']; 
			}else if($DataList[$j]['Status'] == CARD_STATUS_OUTGOING)
			{
				$duty_text = $Lang['StaffAttendance']['Outing'];
			}else if($DataList[$j]['Duty'] == '1')
			{
				$duty_text = $Lang['StaffAttendance']['OnDuty'];
			}else
			{
				$duty_text = $Lang['StaffAttendance']['NoDuty'];
			}
	        	
        	if($DataList[$j]['SlotName']!="")
			{
				$working_time_slot = $DataList[$j]['SlotName'].'('.$DataList[$j]['SlotStart'].'-'.$DataList[$j]['SlotEnd'].')';
			}else
			{
				$working_time_slot = '-';
			}
	        	
	        $x .= '<tr>';
			
			$display_group_name = '';
			if($DataList[$j]['GroupName'] != ''){
				$display_group_name = ' ('.$DataList[$j]['GroupName'].')';
			}
			
			if($j == 0) $x .= '<td rowspan="'.sizeof($DataList).'">'.$DataList[$j]['StaffName'].$display_group_name.'</td>';

			$jsTapCardInTime = 'if(typeof(jsTapCardTime)!=\'undefined\' && typeof(jsTapCardTime['.$DataList[$j]['UserID'].'])!=\'undefined\')Get_Available_Entry_Log_Time(jsTapCardTime['.$DataList[$j]['UserID'].'], \'InHour'.$record_id.'\', \'InMin'.$record_id.'\', \'InSec'.$record_id.'\', \'DivEntryTimes\', '.$record_id.',\'InTimeView\',\'InTimeEdit\');';
    		$jsTapCardOutTime = 'if(typeof(jsTapCardTime)!=\'undefined\' && typeof(jsTapCardTime['.$DataList[$j]['UserID'].'])!=\'undefined\')Get_Available_Entry_Log_Time(jsTapCardTime['.$DataList[$j]['UserID'].'], \'OutHour'.$record_id.'\', \'OutMin'.$record_id.'\', \'OutSec'.$record_id.'\', \'DivEntryTimes\', '.$record_id.',\'OutTimeView\',\'OutTimeEdit\');';
			$x .='<td>'.$working_time_slot.'</td>
                  <td nowrap="nowrap">'.$duty_text.'</td>
				  <td nowrap="nowrap">
					<span id="InTimeView'.$record_id.'"'.$style_hidden.'>
						<span class="row_content">'.$DisplayInTime.'</span>
                    	<div class="table_row_tool" '.(($DisplayInTime!="-" && !$sys_custom['StaffAttendance']['EnableEditTime'])?'style="display:none"':'').'><a title="'.$Lang['Btn']['Edit'].'" class="edit_dim" href="javascript:void(0);" onclick="EditElement(\'InTimeView\',\'InTimeEdit\','.$record_id.');"></a></div>
					</span>
				  	<span class="row_content" id="InTimeEdit'.$record_id.'" style="display:none;">
						<input name="InHour[]" id="InHour'.$record_id.'" value="'.$InTime[0].'" size="2" maxlength="2" type="text" onchange="'.$jsTapCardInTime.'">:
                    	<input name="InMin[]" id="InMin'.$record_id.'" value="'.$InTime[1].'" size="2" maxlength="2" type="text" onchange="'.$jsTapCardInTime.'">:
						<input name="InSec[]" id="InSec'.$record_id.'" value="'.$InTime[2].'" size="2" maxlength="2" type="text" onchange="'.$jsTapCardInTime.'">
					</span>&nbsp;
				  </td>
                  <td nowrap="nowrap">
					<span id="OutTimeView'.$record_id.'"'.$style_hidden.'>
						<span class="row_content">'.$DisplayOutTime.'</span>
                    	<div class="table_row_tool" '.(($DisplayOutTime!="-" && !$sys_custom['StaffAttendance']['EnableEditTime'])?'style="display:none"':'').'><a title="'.$Lang['Btn']['Edit'].'" class="edit_dim" href="javascript:void(0);" onclick="EditElement(\'OutTimeView\',\'OutTimeEdit\','.$record_id.');"></a></div>
					</span>
				  	<span class="row_content" id="OutTimeEdit'.$record_id.'" style="display:none;">
						<input name="OutHour[]" id="OutHour'.$record_id.'" value="'.$OutTime[0].'" size="2" maxlength="2" type="text" onchange="'.$jsTapCardOutTime.'">:
                    	<input name="OutMin[]" id="OutMin'.$record_id.'" value="'.$OutTime[1].'" size="2" maxlength="2" type="text" onchange="'.$jsTapCardOutTime.'">:
						<input name="OutSec[]" id="OutSec'.$record_id.'" value="'.$OutTime[2].'" size="2" maxlength="2" type="text" onchange="'.$jsTapCardOutTime.'">
					</span>&nbsp;
				  </td>
				  <td nowrap="nowrap">
					<span id="InStationView'.$record_id.'"'.$style_hidden.'>
						<span class="row_content">'.(($DataList[$j]['InSchoolStation']=="")?'-':intranet_htmlspecialchars($DataList[$j]['InSchoolStation'] )).'</span>
                    	<div class="table_row_tool" '.(($DisplayInTime!="-" || trim($DataList[$j]['InSchoolStation'])!="") && !$sys_custom['StaffAttendance']['EnableEditTime']?'style="display:none"':'').'><a title="'.$Lang['Btn']['Edit'].'" class="edit_dim" href="javascript:void(0);" onclick="EditElement(\'InStationView\',\'InStationEdit\','.$record_id.');"></a></div>
					</span>
				  	<span class="row_content" id="InStationEdit'.$record_id.'" style="display:none;">
						<input name="InStation[]" id="InStation'.$record_id.'" value="'.intranet_htmlspecialchars($DataList[$j]['InSchoolStation'] ).'" size="10" maxlength="100" type="text">
					</span>&nbsp;
				  </td>
				  <td nowrap="nowrap">
					<span id="OutStationView'.$record_id.'"'.$style_hidden.'>
						<span class="row_content">'.(($DataList[$j]['OutSchoolStation']=="")?'-':intranet_htmlspecialchars($DataList[$j]['OutSchoolStation'])).'</span>
                    	<div class="table_row_tool" '.(($DisplayOutTime!="-" || trim($DataList[$j]['OutSchoolStation'])!="") && !$sys_custom['StaffAttendance']['EnableEditTime']?'style="display:none"':'').'><a title="'.$Lang['Btn']['Edit'].'" class="edit_dim" href="javascript:void(0);" onclick="EditElement(\'OutStationView\',\'OutStationEdit\','.$record_id.');"></a></div>
					</span>
				  	<span class="row_content" id="OutStationEdit'.$record_id.'" style="display:none;">
						<input name="OutStation[]" id="OutStation'.$record_id.'" value="'.intranet_htmlspecialchars($DataList[$j]['OutSchoolStation']).'" size="10" maxlength="100" type="text">
					</span>&nbsp;
				  </td>
                  <td nowrap="nowrap">'.$StatusSelection.'</td>
                  <td><div id="in_reason_container_'.$record_id.'"'.$reason0hidden.'>'.$reason0div.'<br /><br /></div><div id="out_reason_container_'.$record_id.'"'.$reason1hidden.'>'.$reason1div.'</div></td>
				  <td>
					<div id="in_waived_container_'.$record_id.'" '.$reason0hidden.'><input type="checkbox" value="" '.($DataList[$j]['Waived']==1 || ($DataList[$j]['RecordStatus']!=1 && $DataList[$j]['InReasonDefaultWaived']==1)?'checked':'').' onclick="CheckWaived(\'in_waived_'.$record_id.'\',this.checked);" /><input type="hidden" id="in_waived_'.$record_id.'" name="InWaived[]" value="'.($DataList[$j]['Waived']==1 || ($DataList[$j]['RecordStatus']!=1 && $DataList[$j]['InReasonDefaultWaived']==1)?'1':'0').'" /></div>
					<div id="out_waived_container_'.$record_id.'" '.$reason1hidden.'><input type="checkbox" value="" '.($DataList[$j]['ELWaived']==1 || ($DataList[$j]['RecordStatus']!=1 && $DataList[$j]['OutReasonDefaultWaived']==1)?'checked':'').' onclick="CheckWaived(\'out_waived_'.$record_id.'\',this.checked);" /><input type="hidden" id="out_waived_'.$record_id.'" name="OutWaived[]" value="'.($DataList[$j]['ELWaived']==1 || ($DataList[$j]['RecordStatus']!=1 && $DataList[$j]['OutReasonDefaultWaived']==1)?'1':'0').'" /></div>
				  </td>
				  <td nowrap="nowrap" id="RemarkTd'.$record_id.'">
					<span id="ViewRemarkLayer'.$record_id.'"'.$style_hidden.'>
						<span class="row_content" id="DisplayRemark'.$record_id.'">'.(($DataList[$j]['Remark']=="")?'-':nl2br(intranet_htmlspecialchars($DataList[$j]['Remark']))).'</span>
						<div class="table_row_tool"><a title="'.$Lang['Btn']['Edit'].'" class="edit_dim" href="javascript:void(0);" onclick="EditRemark('.$record_id.');"/></div>
				  	</span>
					<span class="row_content" id="EditRemarkLayer'.$record_id.'" style="display:none;text-align:center;">
                        <!-- <iframe id="RemarkFrame'.$record_id.'" frameborder="0" scrolling="no" width="150px" height="100px" src="javascript:false;"></iframe> -->
						<div id="RemarkFrame'.$record_id.'" style="width:150px;height:100px;"></div>
                        <br>
                        <input class="formsmallbutton" onmouseover="this.className=\'formsmallbuttonon\'" onmouseout="this.className=\'formsmallbutton\'" value="'.$Lang['Btn']['Save'].'" type="button" onclick="SetRemark('.$record_id.');" />
                        <input class="formsmallbutton" onmouseover="this.className=\'formsmallbuttonon\'" onmouseout="this.className=\'formsmallbutton\'" value="'.$Lang['Btn']['Cancel'].'" type="button" onclick="EditRemark('.$record_id.');" />                                                                       
					</span>
					<input type="hidden" id="Remark'.$record_id.'" name="Remark[]" value="'.intranet_htmlspecialchars($DataList[$j]['Remark']).'" />
				  </td>
                  <td nowrap="nowrap">'.$DataList[$j]['LastUpdated'].(trim($DataList[$j]['LastUpdater'])!=''?'<br />by '.$DataList[$j]['LastUpdater']:'').'</td>
				  <td>
					<input name="SelectedRecordID[]" type="checkbox" value="'.$DataList[$j]['RecordID'].'" />
					<input name="RecordID[]" id="RecordID_'.$DataList[$j]['RecordID'].'" type="hidden" value="'.$DataList[$j]['RecordID'].'" />
					<input name="StaffID[]" type="hidden" value="'.$DataList[$j]['UserID'].'" />
				  </td>
               </tr>
			  ';
        	
		}
	}
 		$x .= '<input type="hidden" id="select_index" name="select_index" value="0" />
					<input type="hidden" id="PageLoadTime" name="PageLoadTime" value="'.$PageLoadTime.'" />
					</tbody>
                </table>
				</td></tr>
				</tbody>
				</table>
				<div class="edit_bottom">
                    <p class="spacer"/>
                    <input type="button" value="'.$Lang['Btn']['Submit'].'" onmouseout="this.className=\'formbutton\'" onmouseover="this.className=\'formbuttonon\'" class="formbutton" name="submit_btn" onclick="Update_Attendance();"/>
                    <input type="button" value="'.$Lang['Btn']['Cancel'].'" onmouseout="this.className=\'formbutton\'" onmouseover="this.className=\'formbuttonon\'" class="formbutton" name="cancel_btn" onclick="Get_Overall_List();"/>
                    <p class="spacer"/>
                </div>';
		
		$x .= '</div>';
		
		$x .= '<div id="DivEntryTimes"></div>';
		$x .= '<script type="text/javascript" language="javascript">'."\n";
		$x .= 'var js_select_index = -1;'."\n";
		$x .= '</script>'."\n";
		$x .= $StaffAttend3UI->Get_Entry_Log_Time_Selection_JSArray($StaffList,$TargetDate,"jsTapCardTime");

echo $x;

intranet_closedb();
?>