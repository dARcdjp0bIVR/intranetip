<?php
// editing by 
/*
 * 2019-08-13 Ray: Add update log
 * 2017-06-01 Carlos: Check out-dated record for each staff individually such that other valid records can be updated.
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!$StaffAttend3->Check_Access_Right('MGMT-Attendance'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

//$StaffAttend3UI = new libstaffattend3_ui();
$StaffAttend3 = new libstaffattend3();
$GeneralSetting = new libgeneralsettings();
		
$SettingList[] = "'IgnoreOTTime'";
$SettingList[] = "'CountAsOT'";
$Settings = $GeneralSetting->Get_General_Setting('StaffAttendance',$SettingList);
$IgnoreOTTime = $Settings['IgnoreOTTime'];// OT Time in minutes
$CountAsOT = $Settings['CountAsOT'];// 1 or 0
$HaveOT = ($IgnoreOTTime == 999)?false:true;

$GroupID = $_REQUEST['GroupID'];
$SlotName = trim(urldecode(stripslashes($_REQUEST['SlotName'])));
$StatusType = $_REQUEST['StatusType'];
$Keyword = trim(urldecode(stripslashes($_REQUEST['Keyword'])));
$TargetDate = $_REQUEST['TargetDate'];
$Date = explode('-',$TargetDate);
$Year = $Date[0];
$Month = $Date[1];
$Day = $Date[2];

$StaffID = $_REQUEST['StaffID'];
$Duty = $_REQUEST['Duty'];
$InHour = $_REQUEST['InHour'];
$InMin = $_REQUEST['InMin'];
$InSec = $_REQUEST['InSec'];
$OutHour = $_REQUEST['OutHour'];
$OutMin = $_REQUEST['OutMin'];
$OutSec = $_REQUEST['OutSec'];
$InStation = $_REQUEST['InStation'];
$OutStation = $_REQUEST['OutStation'];
$Status = $_REQUEST['Status'];
$ReasonAbsent = $_REQUEST['ReasonAbsent'];
$ReasonLate = $_REQUEST['ReasonLate'];
$ReasonEarlyLeave = $_REQUEST['ReasonEarlyLeave'];
$ReasonOuting = $_REQUEST['ReasonOuting'];
$ReasonHoliday = $_REQUEST['ReasonHoliday'];
$Waived = $_REQUEST['Waived'];
$ELWaived = $_REQUEST['ELWaived'];
$Remark = $_REQUEST['Remark']; 
$PageLoadTime = $_REQUEST['PageLoadTime'];
/*
if($StaffAttend3->isDataOutdated1($StaffID, $TargetDate, $PageLoadTime))
{
	echo $Lang['StaffAttendance']['DataOutdatedWarning'];
	intranet_closedb();
	exit();
}
*/
$CARD_STATUS_LATEEARLYLEAVE = 6;

$results = array();
$result_counter = 0;

$card_log_table_name = $StaffAttend3->createTable_Card_Staff_Attendance2_Daily_Log($Year, $Month);
$DateStr = $TargetDate; 
$Weekday = date("w", mktime(0, 0, 0, $Month, $Day, $Year));
/*
$sql = "SELECT SlotName, SlotStart, SlotEnd, DutyCount FROM CARD_STAFF_ATTENDANCE3_SLOT WHERE SlotID = '$SlotID' ";
$temp_result = $StaffAttend3->returnArray($sql);
$SlotName = $temp_result[0]['SlotName'];
$DutyStart = $temp_result[0]['SlotStart'];
$DutyEnd = $temp_result[0]['SlotEnd'];
$ProfileCountFor = $temp_result[0]['DutyCount'];
*/

$staffIdToRecords = $StaffAttend3->Get_Daily_Log_Record(implode(",",$StaffID),$Year,$Month,$Day,array(),1);

$StaffAttend3->Start_Trans();

$CARD_STATUS_LATEEARLYLEAVE = 6;
$CARD_STATUS_ABSENT_SUSPECTED = 7;



for($i=0;$i<sizeof($StaffID);$i++)
{
	// check last updated time of attendance daily log record for each staff is newer than page load time
	$is_data_outdated = 0;
	if(isset($staffIdToRecords[$StaffID[$i]])){
		foreach($staffIdToRecords[$StaffID[$i]] as $ary)
		{
			if($ary['DateModified'] != ''){
				$last_updated_ts = strtotime($ary['DateModified']);
				if($last_updated_ts >= $PageLoadTime){
					$is_data_outdated = 1;
				}
			}
		}
		if($is_data_outdated) continue;
	}

	// Find the last slot of that staff on that day
	$sql = "SELECT
				MAX(DutyEnd) as LastDutyEnd
			FROM
				$card_log_table_name 
			WHERE
				StaffID = '".$StaffID[$i]."'
				AND DayNumber = '$Day' 
			GROUP BY 
				DayNumber 
			";
	$temp_result = $StaffAttend3->returnArray($sql);
	$LastDutyEnd = $temp_result[0]['LastDutyEnd'];


	if($StaffAttend3->isRecordChangeLogEnabled()) {
		$params = array();
		$params['TargetUserID'] = $StaffID[$i];
		$params['TargetDate'] = $DateStr;
		$params['StatusType'] = array();
		$params['SlotName'] = $SlotName;
		$temp_result = $StaffAttend3->Get_Overall_Daily_Attendance_Records($params);
		if (sizeof($temp_result) > 0) {
			if ($temp_result[0]['Status'] != $Status[$i]) {
				if(trim($InHour[$i])=="" || trim($InMin[$i])=="" || trim($InSec[$i])=="")
					$InTime = "NULL";
				else
					$InTime = $InHour[$i].":".$InMin[$i].":".$InSec[$i];
				if(trim($OutHour[$i])=="" || trim($OutMin[$i])=="" || trim($OutSec[$i])=="")
					$OutTime = "NULL";
				else
					$OutTime = $OutHour[$i].":".$OutMin[$i].":".$OutSec[$i];

				$StatusList = $StaffAttend3->Get_Attendance_Status_Settings();
				$RecordDetail = array();
				$RecordDetail['Name'] = $temp_result[0]['StaffName'];
				$RecordDetail['Date'] = $Year.'-'.$Month.'-'.$Day;
				$RecordDetail['Time_Slot'] = $temp_result[0]['SlotName'].'('.$temp_result[0]['SlotStart'].'-'.$temp_result[0]['SlotEnd'].')';
				$RecordDetail['From_Status'] = $StatusList[$temp_result[0]['Status']]['StatusDisplay'];
				$RecordDetail['To_Status'] = $StatusList[$Status[$i]]['StatusDisplay'];
				$RecordDetail['To_InTime'] = $InTime;
				$RecordDetail['To_OutTime'] = $OutTime;

				$OriginalRecordDetail = array();
				$OriginalRecordDetail['InSchoolStatus'] = $temp_result[0]['InSchoolStatus'];
				$OriginalRecordDetail['OutSchoolStatus'] = $temp_result[0]['OutSchoolStatus'];
				$OriginalRecordDetail['InTime'] = $temp_result[0]['InTime'];
				$OriginalRecordDetail['OutTime'] = $temp_result[0]['OutTime'];
				$results[$result_counter] = $StaffAttend3->INSERT_UPDATE_LOG('Attendance_Record',$RecordDetail, $temp_result[0]['RecordID'], $OriginalRecordDetail);
				$result_counter++;
			}
		}
	}
	
	# Staff's status is Present
	if($Status[$i] == CARD_STATUS_NORMAL)
	{
		$sql = "SELECT 
					RecordID,
					InAttendanceRecordID,
					OutAttendanceRecordID,
					DutyCount,
					DutyStart,
					DutyEnd,
					OutWavie,
					OutSchoolStatus 
				FROM 
					$card_log_table_name 
				WHERE 
					StaffID = '".$StaffID[$i]."'
					AND DayNumber = '$Day'
					AND SlotName = '".$StaffAttend3->Get_Safe_Sql_Query($SlotName)."'
				";
					
		$temp_result = $StaffAttend3->returnArray($sql);
		if(sizeof($temp_result) > 0)# Daily Log Record Exists; Update it
		{
			$DutyStart = $temp_result[0]['DutyStart'];
			$DutyEnd = $temp_result[0]['DutyEnd'];
			$ProfileCountFor = $temp_result[0]['DutyCount'];
			$OutWavie = $temp_result[0]['OutWavie'];
			$OutSchoolStatus = $temp_result[0]['OutSchoolStatus'];
			
			$DailyLogRecordID = $temp_result[0]['RecordID'];
			$InProfileRecordID = $temp_result[0]['InAttendanceRecordID'];
			$OutProfileRecordID = $temp_result[0]['OutAttendanceRecordID'];
			if(trim($InHour[$i])=="" || trim($InMin[$i])=="" || trim($InSec[$i])=="")
				$InTime = "NULL";
			else
				$InTime = "'".$InHour[$i].":".$InMin[$i].":".$InSec[$i]."'";
			if(trim($OutHour[$i])=="" || trim($OutMin[$i])=="" || trim($OutSec[$i])=="")
				$OutTime = "NULL";
			else
				$OutTime = "'".$OutHour[$i].":".$OutMin[$i].":".$OutSec[$i]."'";
				
			if(trim($OutSchoolStatus)=='')// OutSchoolStatus is not set
			{
				if($OutWavie == 1)// this slot no need to tap-card for out, preset normal
				{
					$OutSchoolStatus = "'".CARD_STATUS_NORMAL."'";
				}else // this slot need to tap-card for out
				{
					if($OutTime != "NULL")
						$OutSchoolStatus = "'".CARD_STATUS_NORMAL."'";
					else
						$OutSchoolStatus = "NULL";
				}
			}else // OutSchoolStatus follow InSchoolStatus to be normal
			{
				$OutSchoolStatus = "'".CARD_STATUS_NORMAL."'";
			}
			
			# Delete Profiles for this staff at that date since Present status do not need any Profile records
			if($InProfileRecordID > 0)
			{
				$sql = "DELETE FROM CARD_STAFF_ATTENDANCE2_PROFILE WHERE RecordID = '$InProfileRecordID' ";
				$results[$result_counter] = $StaffAttend3->db_db_query($sql);
				$result_counter += 1;
			}
			if($OutProfileRecordID > 0)
			{
				$sql = "DELETE FROM CARD_STAFF_ATTENDANCE2_PROFILE WHERE RecordID = '$OutProfileRecordID' ";
				$results[$result_counter] = $StaffAttend3->db_db_query($sql);
				$result_counter += 1;
			}

			$sql = "UPDATE $card_log_table_name 
					SET 
						StaffID = '".$StaffID[$i]."', 
						DayNumber = '$Day',  
						Duty = '1', 
						StaffPresent = '1',
						InTime = $InTime,
						OutTime = $OutTime,
						InAttendanceRecordID = NULL,
						OutAttendanceRecordID = NULL,
						InSchoolStatus = '".CARD_STATUS_NORMAL."',
						OutSchoolStatus = $OutSchoolStatus,
						InSchoolStation = '".$StaffAttend3->Get_Safe_Sql_Query(trim(urldecode(stripslashes($InStation[$i]))))."',
						OutSchoolStation = '".$StaffAttend3->Get_Safe_Sql_Query(trim(urldecode(stripslashes($OutStation[$i]))))."',
						InWaived = NULL,
						OutWaived = NULL,
						MinLate = NULL,
						MinEarlyLeave = NULL,
						Remark = '".$StaffAttend3->Get_Safe_Sql_Query(trim(urldecode(stripslashes($Remark[$i]))))."',
						RecordStatus = '1', 
						DateConfirmed = NOW(),
						ConfirmBy = '".$_SESSION['UserID']."'
					WHERE
						RecordID = '$DailyLogRecordID'
					";
			$results[$result_counter] = $StaffAttend3->db_db_query($sql);//echo "#1".$results[$result_counter]."<br>";
			$result_counter += 1;
		}/*else # Daily Log Record Does Not Exists; Insert a New One
		{
			$InTime = $InHour[$i].":".$InMin[$i].":".$InSec[$i];
			$OutTime = $OutHour[$i].":".$OutMin[$i].":".$OutSec[$i];
			$sql = "INSERT INTO $card_log_table_name 
						(StaffID, DayNumber, SlotName, Duty, DutyStart, DutyEnd, StaffPresent, InTime, OutTime, InSchoolStatus, InSchoolStation, OutSchoolStation, Remark, RecordType, RecordStatus, DateInput, DateModified)
					VALUES
					('".$StaffID[$i]."',
						'$Day',
						'".$StaffAttend3->Get_Safe_Sql_Query($SlotName)."',
						'1',
						'$DutyStart',
						'$DutyEnd',
						'1',
						'$InTime',
						'$OutTime',
						'".CARD_STATUS_NORMAL."',
						'".$StaffAttend3->Get_Safe_Sql_Query(trim(urldecode(stripslashes($InStation[$i]))))."',
						'".$StaffAttend3->Get_Safe_Sql_Query(trim(urldecode(stripslashes($OutStation[$i]))))."',
						'".$StaffAttend3->Get_Safe_Sql_Query(trim(urldecode(stripslashes($Remark[$i]))))."',
						'".CARD_STATUS_NORMAL."',
						'1',
						NOW(),
						NOW()
					)";
			$results[$result_counter] = $StaffAttend3->db_db_query($sql);//echo "#2".$results[$result_counter]."<br>";
			$result_counter += 1;
		}*/
		if($DutyEnd == $LastDutyEnd)// Only calculate OT records if this slot is the last slot of that staff on this day
		{	
			#Calculate OT Records
			$InTime = $InHour[$i].":".$InMin[$i].":".$InSec[$i];
			$OutTime = $OutHour[$i].":".$OutMin[$i].":".$OutSec[$i];
			$OutTimeMin = date('U',strtotime($OutTime))/60;//minutes since 1970 00:00:00
			$DutyEndMin = date('U',strtotime($DutyEnd))/60;
			$OTTimeMin = floor($OutTimeMin - $DutyEndMin);
			if($HaveOT && $OTTimeMin > $IgnoreOTTime && $OTTimeMin > 0) // OT
			{
				$sql = "INSERT INTO CARD_STAFF_ATTENDANCE2_OT_RECORD
						(StaffID, RecordDate, StartTime, EndTime, OTmins, Waived, RecordStatus, DateInput, DateModified) 
						VALUES(
							'".$StaffID[$i]."',
							'$TargetDate',
							'$DutyEnd',
							'$OutTime',
							'$OTTimeMin',
							NULL,
							NULL,
							NOW(),
							NOW()
						)On Duplicate Key Update 
							StartTime = '$DutyEnd',
							EndTime = '$OutTime',
							OTmins = '$OTTimeMin',
							Waived = NULL, 
							RecordStatus = NULL,
							DateModified = NOW() ";
				$results[$result_counter] = $StaffAttend3->db_db_query($sql);//echo "##".$results[$result_counter]."<br>";
				$result_counter += 1;
			}else// Not OT
			{
				# Delete OT and Redeem Records
				$sql = "DELETE FROM CARD_STAFF_ATTENDANCE2_OT_RECORD 
						WHERE 
							StaffID = '".$StaffID[$i]."'
							AND RecordDate = '$TargetDate' ";
				$results[$result_counter] = $StaffAttend3->db_db_query($sql);
				$result_counter += 1;
				
				$sql = "DELETE FROM CARD_STAFF_ATTENDANCE2_OT_REDEEM 
						WHERE 
							StaffID = '".$StaffID[$i]."'
							AND RedeemDate = '$TargetDate' ";
				$results[$result_counter] = $StaffAttend3->db_db_query($sql);//echo "####".$results[$result_counter]."<br>";
				$result_counter += 1;
			}
		}
	}else if($Status[$i] == CARD_STATUS_ABSENT)# Staff's status is Absent
	{
		$sql = "SELECT 
					RecordID,
					InAttendanceRecordID,
					OutAttendanceRecordID,
					DutyCount,
					DutyStart,
					DutyEnd 
				FROM 
					$card_log_table_name 
				WHERE 
					StaffID = '".$StaffID[$i]."'
					AND DayNumber = '$Day'
					AND SlotName = '".$StaffAttend3->Get_Safe_Sql_Query($SlotName)."' ";
					
		$temp_result = $StaffAttend3->returnArray($sql);
		if(sizeof($temp_result) > 0)# Daily Log Record Exists; Update it
		{
			$DutyStart = $temp_result[0]['DutyStart'];
			$DutyEnd = $temp_result[0]['DutyEnd'];
			$ProfileCountFor = $temp_result[0]['DutyCount'];
			
			$DailyLogRecordID = $temp_result[0]['RecordID'];
			$InProfileRecordID = $temp_result[0]['InAttendanceRecordID'];
			$OutProfileRecordID = $temp_result[0]['OutAttendanceRecordID'];
			
			if($OutProfileRecordID > 0)
			{
				$sql = "DELETE FROM CARD_STAFF_ATTENDANCE2_PROFILE WHERE RecordID = '$OutProfileRecordID' ";
				$results[$result_counter] = $StaffAttend3->db_db_query($sql);
				$result_counter += 1;
			}
			if($InProfileRecordID > 0) # In Profile Record Exists
			{
				$sql = "UPDATE CARD_STAFF_ATTENDANCE2_PROFILE
						SET
							StaffID = '".$StaffID[$i]."',
							ReasonID = ".(($ReasonAbsent[$i]>0)?("'".$ReasonAbsent[$i]."'"):"NULL").",
							RecordDate = '$DateStr',
							Waived = '".$Waived[$i]."',
							ProfileCountFor = '$ProfileCountFor',
							RecordType = '".CARD_STATUS_ABSENT."',
							RecordStatus = '1',
							DateModified = NOW()
						WHERE
							RecordID = '$InProfileRecordID'
							AND StaffID = '".$StaffID[$i]."' 
							AND RecordDate = '$DateStr' ";
				$results[$result_counter] = $StaffAttend3->db_db_query($sql);//echo "#4".$results[$result_counter]."<br>";
				$result_counter += 1;
			}else # In Profile Record Does not Exist
			{
				$sql = "INSERT INTO CARD_STAFF_ATTENDANCE2_PROFILE
						(StaffID, ReasonID, RecordDate, Waived, ProfileCountFor, RecordType, RecordStatus, DateInput, DateModified)
						VALUES
						(
							'".$StaffID[$i]."',
							".(($ReasonAbsent[$i]>0)?("'".$ReasonAbsent[$i]."'"):"NULL").",
							'$DateStr',
							'".$Waived[$i]."',
							'$ProfileCountFor',
							'".CARD_STATUS_ABSENT."',
							'1',
							NOW(),
							NOW()
						)";
				$results[$result_counter] = $StaffAttend3->db_db_query($sql);//echo "#5".$results[$result_counter]."<br>";
				$result_counter += 1;
				
				$InProfileRecordID = $StaffAttend3->db_insert_id();
			}
			
			if(trim($InHour[$i])=="" || trim($InMin[$i])=="" || trim($InSec[$i])=="")
				$InTime = "NULL";
			else
				$InTime = "'".$InHour[$i].":".$InMin[$i].":".$InSec[$i]."'";
			if(trim($OutHour[$i])=="" || trim($OutMin[$i])=="" || trim($OutSec[$i])=="")
				$OutTime = "NULL";
			else
				$OutTime = "'".$OutHour[$i].":".$OutMin[$i].":".$OutSec[$i]."'";
			
			# Update Daily Log Record
			$sql = "UPDATE $card_log_table_name 
					SET 
						StaffID = '".$StaffID[$i]."', 
						DayNumber = '$Day',  
						Duty = '1', 
						StaffPresent = '0',
						InTime = $InTime,
						OutTime = $OutTime,
						InSchoolStatus = '".CARD_STATUS_ABSENT."',
						OutSchoolStatus = NULL,
						InSchoolStation = '".$StaffAttend3->Get_Safe_Sql_Query(trim(urldecode(stripslashes($InStation[$i]))))."',
						OutSchoolStation = '".$StaffAttend3->Get_Safe_Sql_Query(trim(urldecode(stripslashes($OutStation[$i]))))."',
						InAttendanceRecordID = '$InProfileRecordID',
						OutAttendanceRecordID = NULL,
						InWaived = '".$Waived[$i]."',
						OutWaived = NULL,
						MinLate = NULL,
						MinEarlyLeave = NULL,
						Remark = '".$StaffAttend3->Get_Safe_Sql_Query(trim(urldecode(stripslashes($Remark[$i]))))."',
						RecordStatus = '1', 
						DateConfirmed = NOW(),
						ConfirmBy = '".$_SESSION['UserID']."'
					WHERE
						RecordID = '$DailyLogRecordID'
					";
			$results[$result_counter] = $StaffAttend3->db_db_query($sql);//echo "#6".$results[$result_counter]."<br>";
			$result_counter += 1;
		}/*else # Daily Log Record Does Not Exists; Insert a New One
		{
			# Insert a New In Profile Record
			$sql = "INSERT INTO CARD_STAFF_ATTENDANCE2_PROFILE
						(StaffID, ReasonID, RecordDate, Waived, ProfileCountFor, RecordType, RecordStatus, DateInput, DateModified)
						VALUES
						(
							'".$StaffID[$i]."',
							".(($ReasonAbsent[$i]>0)?("'".$ReasonAbsent[$i]."'"):"NULL").",
							'$DateStr',
							'".$Waived[$i]."',
							'$ProfileCountFor',
							'".CARD_STATUS_ABSENT."',
							'1',
							NOW(),
							NOW()
						)";
				$results[$result_counter] = $StaffAttend3->db_db_query($sql);//echo "#7".$results[$result_counter]."<br>";
				$result_counter += 1;
				
				$InProfileRecordID = $StaffAttend3->db_insert_id();
			
			# Insert a New Daily Log Record
			$sql = "INSERT INTO $card_log_table_name 
						(StaffID, DayNumber, SlotName, Duty, DutyStart, DutyEnd, StaffPresent, InTime, InSchoolStatus, InSchoolStation, InWaived, OutTime, OutSchoolStatus, OutSchoolStation, OutWaived, InAttendanceRecordID, OutAttendanceRecordID, MinLate, MinEarlyLeave, Remark, RecordType, RecordStatus, DateInput, DateModified)
					VALUES
					('".$StaffID[$i]."',
						'$Day',
						'".$StaffAttend3->Get_Safe_Sql_Query($SlotName)."',
						'1',
						'$DutyStart',
						'$DutyEnd',
						'0',
						NULL,
						'".CARD_STATUS_ASBENT."',
						NULL,
						NULL,
						NULL,
						'".CARD_STATUS_ASBENT."',
						NULL,
						NULL,
						'$InProfileRecordID',
						NULL,
						NULL,
						NULL,
						'".$StaffAttend3->Get_Safe_Sql_Query(trim(urldecode(stripslashes($Remark[$i]))))."',
						'".CARD_STATUS_ABSENT."',
						'1',
						NOW(),
						NOW()
					)";
			$results[$result_counter] = $StaffAttend3->db_db_query($sql);//echo "#8".$results[$result_counter]."<br>";
			$result_counter += 1;
		}*/
		if($DutyEnd == $LastDutyEnd)// Only calculate OT records if this slot is the last slot of that staff on this day
		{
			# Delete OT and Redeem Records
			$sql = "DELETE FROM CARD_STAFF_ATTENDANCE2_OT_RECORD 
					WHERE 
						StaffID = '".$StaffID[$i]."'
						AND RecordDate = '$TargetDate' ";
			$results[$result_counter] = $StaffAttend3->db_db_query($sql);
			$result_counter += 1;
			
			$sql = "DELETE FROM CARD_STAFF_ATTENDANCE2_OT_REDEEM 
					WHERE 
						StaffID = '".$StaffID[$i]."'
						AND RedeemDate = '$TargetDate' ";
			$results[$result_counter] = $StaffAttend3->db_db_query($sql);
			$result_counter += 1;
		}
	}else if($Status[$i] == CARD_STATUS_LATE)# Staff's status is Late
	{
		$sql = "SELECT 
					RecordID,
					InAttendanceRecordID,
					OutAttendanceRecordID,
					DutyCount,
					DutyStart,
					DutyEnd,
					OutWavie,
					OutSchoolStatus 
				FROM 
					$card_log_table_name 
				WHERE 
					StaffID = '".$StaffID[$i]."'
					AND DayNumber = '$Day'
					AND SlotName = '".$StaffAttend3->Get_Safe_Sql_Query($SlotName)."' ";
					
		$temp_result = $StaffAttend3->returnArray($sql);
		if(sizeof($temp_result) > 0)# Daily Log Record Exists; Update it
		{
			$DutyStart = $temp_result[0]['DutyStart'];
			$DutyEnd = $temp_result[0]['DutyEnd'];
			$ProfileCountFor = $temp_result[0]['DutyCount'];
			$OutWavie = $temp_result[0]['OutWavie'];
			$OutSchoolStatus = $temp_result[0]['OutSchoolStatus'];
			
			
			$DailyLogRecordID = $temp_result[0]['RecordID'];
			$InProfileRecordID = $temp_result[0]['InAttendanceRecordID'];
			$OutProfileRecordID = $temp_result[0]['OutAttendanceRecordID'];
			
			if($OutProfileRecordID > 0)
			{
				$sql = "DELETE FROM CARD_STAFF_ATTENDANCE2_PROFILE WHERE RecordID = '$OutProfileRecordID' ";
				$results[$result_counter] = $StaffAttend3->db_db_query($sql);
				$result_counter += 1;
			}
			if($InProfileRecordID > 0) # In Profile Record Exists
			{
				$sql = "UPDATE CARD_STAFF_ATTENDANCE2_PROFILE
						SET
							StaffID = '".$StaffID[$i]."',
							ReasonID = ".(($ReasonLate[$i]>0)?("'".$ReasonLate[$i]."'"):"NULL").",
							RecordDate = '$DateStr',
							Waived = '".$Waived[$i]."',
							ProfileCountFor = '$ProfileCountFor',
							RecordType = '".CARD_STATUS_LATE."',
							RecordStatus = '1',
							DateModified = NOW()
						WHERE
							RecordID = '$InProfileRecordID'
							AND StaffID = '".$StaffID[$i]."' 
							AND RecordDate = '$DateStr' ";
				$results[$result_counter] = $StaffAttend3->db_db_query($sql);//echo "#9".$results[$result_counter]."<br>";
				$result_counter += 1;
			}else # In Profile Record Does not Exist
			{
				$sql = "INSERT INTO CARD_STAFF_ATTENDANCE2_PROFILE
						(StaffID, ReasonID, RecordDate, Waived, ProfileCountFor, RecordType, RecordStatus, DateInput, DateModified)
						VALUES
						(
							'".$StaffID[$i]."',
							".(($ReasonLate[$i]>0)?("'".$ReasonLate[$i]."'"):"NULL").",
							'$DateStr',
							'".$Waived[$i]."',
							'$ProfileCountFor',
							'".CARD_STATUS_LATE."',
							'1',
							NOW(),
							NOW()
						)";
				$results[$result_counter] = $StaffAttend3->db_db_query($sql);//echo "#10".$results[$result_counter]."<br>";
				$result_counter += 1;
				
				$InProfileRecordID = $StaffAttend3->db_insert_id();
			}
			
			//$InTime = $InHour[$i].":".$InMin[$i].":".$InSec[$i];
			//$OutTime = $OutHour[$i].":".$OutMin[$i].":".$OutSec[$i];
			//$MinLate = floor((strtotime($InTime) - strtotime($DutyStart))/60);//Late minutes
			//$MinLateStr = ($MinLate < 0)?"NULL":"'".$MinLate."'";
			if(trim($InHour[$i])=="" || trim($InMin[$i])=="" || trim($InSec[$i])==""){
				$InTime = "NULL";
				$MinLateStr = "NULL";
			}else{
				$InTime = "'".$InHour[$i].":".$InMin[$i].":".$InSec[$i]."'";
				$InTime2 = $InHour[$i].":".$InMin[$i].":".$InSec[$i];
				$MinLate = floor((strtotime($TargetDate." ".$InTime2) - strtotime($TargetDate." ".$DutyStart))/60);//Late minutes
				$MinLateStr = ($MinLate < 0)?"NULL":"'".$MinLate."'";
			}
			if(trim($OutHour[$i])=="" || trim($OutMin[$i])=="" || trim($OutSec[$i])=="")
				$OutTime = "NULL";
			else
				$OutTime = "'".$OutHour[$i].":".$OutMin[$i].":".$OutSec[$i]."'";
			
			if(trim($OutSchoolStatus)=='')// OutSchoolStatus is not set
			{
				if($OutWavie == 1)// this slot no need to tap-card for out, preset normal
				{
					$OutSchoolStatus = "'".CARD_STATUS_NORMAL."'";
				}else // this slot need to tap-card for out
				{
					if($OutTime != "NULL")
						$OutSchoolStatus = "'".CARD_STATUS_NORMAL."'";
					else
						$OutSchoolStatus = "NULL";
				}
			}else // OutSchoolStatus follow InSchoolStatus to be normal
			{
				$OutSchoolStatus = "'".CARD_STATUS_NORMAL."'";
			}
			
			# Update Daily Log Record
			$sql = "UPDATE $card_log_table_name 
					SET 
						StaffID = '".$StaffID[$i]."', 
						DayNumber = '$Day', 
						Duty = '1',  
						StaffPresent = '1',
						InTime = $InTime,
						InSchoolStatus = '".CARD_STATUS_LATE."',
						InSchoolStation = '".$StaffAttend3->Get_Safe_Sql_Query(trim(urldecode(stripslashes($InStation[$i]))))."',
						InWaived = '".$Waived[$i]."',
						OutTime = $OutTime,
						OutSchoolStatus = $OutSchoolStatus,
						OutSchoolStation = '".$StaffAttend3->Get_Safe_Sql_Query(trim(urldecode(stripslashes($OutStation[$i]))))."',
						OutWaived = NULL,
						MinLate = $MinLateStr,
						MinEarlyLeave = NULL,
						InAttendanceRecordID = '$InProfileRecordID',
						OutAttendanceRecordID = NULL,
						Remark = '".$StaffAttend3->Get_Safe_Sql_Query(trim(urldecode(stripslashes($Remark[$i]))))."',
						RecordStatus = '1', 
						DateConfirmed = NOW(),
						ConfirmBy = '".$_SESSION['UserID']."'
					WHERE
						RecordID = '$DailyLogRecordID'
					";
			$results[$result_counter] = $StaffAttend3->db_db_query($sql);//echo "#11".$results[$result_counter]."<br>";
			$result_counter += 1;
		}/*else # Daily Log Record Does Not Exists; Insert a New One
		{
			# Insert a New In Profile Record
			$sql = "INSERT INTO CARD_STAFF_ATTENDANCE2_PROFILE
						(StaffID, ReasonID, RecordDate, Waived, ProfileCountFor, RecordType, RecordStatus, DateInput, DateModified)
						VALUES
						(
							'".$StaffID[$i]."',
							".(($ReasonLate[$i]>0)?("'".$ReasonLate[$i]."'"):"NULL").",
							'$DateStr',
							'".$Waived[$i]."',
							'$ProfileCountFor',
							'".CARD_STATUS_LATE."',
							'1',
							NOW(),
							NOW()
						)";
			$results[$result_counter] = $StaffAttend3->db_db_query($sql);//echo "#12".$results[$result_counter]."<br>";
			$result_counter += 1;
			
			$InProfileRecordID = $StaffAttend3->db_insert_id();
			
			# Insert a New Daily Log Record for Late
			$InTime = $InHour[$i].":".$InMin[$i].":".$InSec[$i];
			$OutTime = $OutHour[$i].":".$OutMin[$i].":".$OutSec[$i];
			$MinLate = (strtotime($InTime) - strtotime($DutyStart))/60;//Late minutes
			
			$sql = "INSERT INTO $card_log_table_name 
						(StaffID, DayNumber, SlotName, Duty, DutyStart, DutyEnd, StaffPresent, InTime, OutTime, InSchoolStatus, OutSchoolStatus, InSchoolStation, OutSchoolStation, InWaived, OutWaived, InAttendanceRecordID, OutAttendanceRecordID, MinLate, MinEarlyLeave, Remark, RecordType, RecordStatus, DateInput, DateModified)
					VALUES
					('".$StaffID[$i]."',
						'$Day',
						'".$StaffAttend3->Get_Safe_Sql_Query($SlotName)."',
						'1',
						'$DutyStart',
						'$DutyEnd',
						'1',
						'$InTime',
						'$OutTime',
						'".CARD_STATUS_LATE."',
						NULL,
						'".$StaffAttend3->Get_Safe_Sql_Query(trim(urldecode(stripslashes($InStation[$i]))))."',
						'".$StaffAttend3->Get_Safe_Sql_Query(trim(urldecode(stripslashes($OutStation[$i]))))."',
						'".$Waived[$i]."',
						NULL,
						'$InProfileRecordID',
						NULL,
						'$MinLate',
						NULL,
						'".$StaffAttend3->Get_Safe_Sql_Query(trim(urldecode(stripslashes($Remark[$i]))))."',
						'".CARD_STATUS_LATE."',
						'1',
						NOW(),
						NOW()
					)";
			$results[$result_counter] = $StaffAttend3->db_db_query($sql);//echo "#13".$results[$result_counter]."<br>";
			$result_counter += 1;
		}*/
		
		if($DutyEnd == $LastDutyEnd)// Only calculate OT records if this slot is the last slot of that staff on this day
		{
			#Calculate OT Records
			$InTime = $InHour[$i].":".$InMin[$i].":".$InSec[$i];
			$OutTime = $OutHour[$i].":".$OutMin[$i].":".$OutSec[$i];
			$OutTimeMin = date('U',strtotime($OutTime))/60;//minutes since 1970 00:00:00
			$DutyEndMin = date('U',strtotime($DutyEnd))/60;
			$OTTimeMin = floor($OutTimeMin - $DutyEndMin);
			if($HaveOT && $OTTimeMin > $IgnoreOTTime && $OTTimeMin > 0) // OT
			{
				$sql = "INSERT INTO CARD_STAFF_ATTENDANCE2_OT_RECORD
						(StaffID, RecordDate, StartTime, EndTime, OTmins, Waived, RecordStatus, DateInput, DateModified) 
						VALUES(
							'".$StaffID[$i]."',
							'$TargetDate',
							'$DutyEnd',
							'$OutTime',
							'$OTTimeMin',
							NULL,
							NULL,
							NOW(),
							NOW()
						)On Duplicate Key Update 
							StartTime = '$DutyEnd',
							EndTime = '$OutTime',
							OTmins = '$OTTimeMin',
							Waived = NULL,
							RecordStatus = NULL,
							DateModified = NOW() ";
				$results[$result_counter] = $StaffAttend3->db_db_query($sql);
				$result_counter += 1;
			}else // Not OT
			{
				# Delete OT and Redeem Records
				$sql = "DELETE FROM CARD_STAFF_ATTENDANCE2_OT_RECORD 
						WHERE 
							StaffID = '".$StaffID[$i]."'
							AND RecordDate = '$TargetDate' ";
				$results[$result_counter] = $StaffAttend3->db_db_query($sql);
				$result_counter += 1;
				
				$sql = "DELETE FROM CARD_STAFF_ATTENDANCE2_OT_REDEEM 
						WHERE 
							StaffID = '".$StaffID[$i]."'
							AND RedeemDate = '$TargetDate' ";
				$results[$result_counter] = $StaffAttend3->db_db_query($sql);
				$result_counter += 1;
			}
		}
	}else if($Status[$i] == CARD_STATUS_EARLYLEAVE)# Staff's status is Early Leave
	{
		$sql = "SELECT 
					RecordID,
					InAttendanceRecordID,
					OutAttendanceRecordID,
					DutyCount,
					DutyStart,
					DutyEnd
				FROM 
					$card_log_table_name 
				WHERE 
					StaffID = '".$StaffID[$i]."'
					AND DayNumber = '$Day'
					AND SlotName = '".$StaffAttend3->Get_Safe_Sql_Query($SlotName)."' ";
					
		$temp_result = $StaffAttend3->returnArray($sql);
		if(sizeof($temp_result) > 0)# Daily Log Record Exists; Update it
		{
			$DutyStart = $temp_result[0]['DutyStart'];
			$DutyEnd = $temp_result[0]['DutyEnd'];
			$ProfileCountFor = $temp_result[0]['DutyCount'];
			
			$DailyLogRecordID = $temp_result[0]['RecordID'];
			$InProfileRecordID = $temp_result[0]['InAttendanceRecordID'];
			$OutProfileRecordID = $temp_result[0]['OutAttendanceRecordID'];
			
			if($InProfileRecordID > 0)
			{
				$sql = "DELETE FROM CARD_STAFF_ATTENDANCE2_PROFILE WHERE RecordID = '$InProfileRecordID' ";
				$results[$result_counter] = $StaffAttend3->db_db_query($sql);
				$result_counter += 1;
			}
			if($OutProfileRecordID > 0) # Out Profile Record Exists
			{
				$sql = "UPDATE CARD_STAFF_ATTENDANCE2_PROFILE
						SET
							StaffID = '".$StaffID[$i]."',
							ReasonID = ".(($ReasonEarlyLeave[$i]>0)?("'".$ReasonEarlyLeave[$i]."'"):"NULL").",
							RecordDate = '$DateStr',
							Waived = '".$Waived[$i]."',
							ProfileCountFor = '$ProfileCountFor',
							RecordType = '".CARD_STATUS_EARLYLEAVE."',
							RecordStatus = '1',
							DateModified = NOW()
						WHERE
							RecordID = '$OutProfileRecordID'
							AND StaffID = '".$StaffID[$i]."' 
							AND RecordDate = '$DateStr' ";
				$results[$result_counter] = $StaffAttend3->db_db_query($sql);//echo "#14".$results[$result_counter]."<br>";
				$result_counter += 1;
			}else # Out Profile Record Does not Exist
			{
				$sql = "INSERT INTO CARD_STAFF_ATTENDANCE2_PROFILE
						(StaffID, ReasonID, RecordDate, Waived, ProfileCountFor, RecordType, RecordStatus, DateInput, DateModified)
						VALUES
						(
							'".$StaffID[$i]."',
							".(($ReasonEarlyLeave[$i]>0)?("'".$ReasonEarlyLeave[$i]."'"):"NULL").",
							'$DateStr',
							'".$Waived[$i]."',
							'$ProfileCountFor',
							'".CARD_STATUS_EARLYLEAVE."',
							'1',
							NOW(),
							NOW()
						)";
				$results[$result_counter] = $StaffAttend3->db_db_query($sql);//echo "#15".$results[$result_counter]."<br>";
				$result_counter += 1;
				
				$OutProfileRecordID = $StaffAttend3->db_insert_id();
			}
			
			//$InTime = $InHour[$i].":".$InMin[$i].":".$InSec[$i];
			//$OutTime = $OutHour[$i].":".$OutMin[$i].":".$OutSec[$i];
			//$MinEarlyLeave = floor((strtotime($DutyEnd) - strtotime($OutTime))/60);//Early Leave minutes
			//$MinEarlyLeaveStr = ($MinEarlyLeave < 0)?"NULL":"'".$MinEarlyLeave."'";
			if(trim($InHour[$i])=="" || trim($InMin[$i])=="" || trim($InSec[$i])=="")
				$InTime = "NULL";
			else
				$InTime = "'".$InHour[$i].":".$InMin[$i].":".$InSec[$i]."'";
			if(trim($OutHour[$i])=="" || trim($OutMin[$i])=="" || trim($OutSec[$i])==""){
				$OutTime = "NULL";
				$MinEarlyLeaveStr = "NULL";
			}else{
				$OutTime = "'".$OutHour[$i].":".$OutMin[$i].":".$OutSec[$i]."'";
				$OutTime2 = $OutHour[$i].":".$OutMin[$i].":".$OutSec[$i];
				$MinEarlyLeave = floor((strtotime($TargetDate." ".$DutyEnd) - strtotime($TargetDate." ".$OutTime2))/60);//Early Leave minutes
				$MinEarlyLeaveStr = ($MinEarlyLeave < 0)?"NULL":"'".$MinEarlyLeave."'";
			}
			# Update Daily Log Record
			$sql = "UPDATE $card_log_table_name 
					SET 
						StaffID = '".$StaffID[$i]."', 
						DayNumber = '$Day', 
						Duty = '1', 
						StaffPresent = '1',
						InTime = $InTime,
						InSchoolStatus = NULL,
						InSchoolStation = '".$StaffAttend3->Get_Safe_Sql_Query(trim(urldecode(stripslashes($InStation[$i]))))."',
						InWaived = NULL,
						OutTime = $OutTime,
						OutSchoolStatus = '".CARD_STATUS_EARLYLEAVE."',
						OutSchoolStation = '".$StaffAttend3->Get_Safe_Sql_Query(trim(urldecode(stripslashes($OutStation[$i]))))."',
						OutWaived = '".$Waived[$i]."',
						MinLate = NULL,
						MinEarlyLeave = $MinEarlyLeaveStr,
						InAttendanceRecordID = NULL,
						OutAttendanceRecordID = '$OutProfileRecordID ',
						Remark = '".$StaffAttend3->Get_Safe_Sql_Query(trim(urldecode(stripslashes($Remark[$i]))))."',
						RecordStatus = '1', 
						DateConfirmed = NOW(),
						ConfirmBy = '".$_SESSION['UserID']."'
					WHERE
						RecordID = '$DailyLogRecordID'
					";
			$results[$result_counter] = $StaffAttend3->db_db_query($sql);//echo "#16".$results[$result_counter]."<br>";
			$result_counter += 1;
		}/*else # Daily Log Record Does Not Exists; Insert a New One
		{
			# Insert a New In Profile Record
			$sql = "INSERT INTO CARD_STAFF_ATTENDANCE2_PROFILE
						(StaffID, ReasonID, RecordDate, Waived, ProfileCountFor, RecordType, RecordStatus, DateInput, DateModified)
						VALUES
						(
							'".$StaffID[$i]."',
							".(($ReasonEarlyLeave[$i]>0)?("'".$ReasonEarlyLeave[$i]."'"):"NULL").",
							'$DateStr',
							'".$Waived[$i]."',
							'$ProfileCountFor',
							'".CARD_STATUS_EARLYLEAVE."',
							'1',
							NOW(),
							NOW()
						)";
			$results[$result_counter] = $StaffAttend3->db_db_query($sql);//echo "#17".$results[$result_counter]."<br>";
			$result_counter += 1;
			
			$OutProfileRecordID = $StaffAttend3->db_insert_id();
			
			# Insert a New Daily Log Record for Early Leave
			$InTime = $InHour[$i].":".$InMin[$i].":".$InSec[$i];
			$OutTime = $OutHour[$i].":".$OutMin[$i].":".$OutSec[$i];
			$MinEarlyLeave = (strtotime($DutyEnd) - strtotime($OutTime))/60;//Early Leave minutes
			
			$sql = "INSERT INTO $card_log_table_name 
						(StaffID, DayNumber, SlotName, Duty, DutyStart, DutyEnd, StaffPresent, InTime, OutTime, InSchoolStatus, OutSchoolStatus, InSchoolStation, OutSchoolStation, InWaived, OutWaived, InAttendanceRecordID, OutAttendanceRecordID, MinLate, MinEarlyLeave, Remark, RecordType, RecordStatus, DateInput, DateModified)
					VALUES
					('".$StaffID[$i]."',
						'$Day',
						'".$StaffAttend3->Get_Safe_Sql_Query($SlotName)."',
						'1',
						'$DutyStart',
						'$DutyEnd',
						'1',
						'$InTime',
						'$OutTime',
						NULL,
						'".CARD_STATUS_EARLYLEAVE."',
						'".$StaffAttend3->Get_Safe_Sql_Query(trim(urldecode(stripslashes($InStation[$i]))))."',
						'".$StaffAttend3->Get_Safe_Sql_Query(trim(urldecode(stripslashes($OutStation[$i]))))."',
						NULL,
						'".$Waived[$i]."',
						NULL,
						'$OutProfileRecordID',
						NULL,
						'$MinEarlyLeave',
						'".$StaffAttend3->Get_Safe_Sql_Query(trim(urldecode(stripslashes($Remark[$i]))))."',
						'".CARD_STATUS_EARLYLEAVE."',
						'1',
						NOW(),
						NOW()
					)";
			$results[$result_counter] = $StaffAttend3->db_db_query($sql);//echo "#18".$results[$result_counter]."<br>";
			$result_counter += 1;
		}*/
		if($DutyEnd == $LastDutyEnd)// Only calculate OT records if this slot is the last slot of that staff on this day
		{
			# Delete OT and Redeem Records
			$sql = "DELETE FROM CARD_STAFF_ATTENDANCE2_OT_RECORD 
					WHERE 
						StaffID = '".$StaffID[$i]."'
						AND RecordDate = '$TargetDate' ";
			$results[$result_counter] = $StaffAttend3->db_db_query($sql);
			$result_counter += 1;
			
			$sql = "DELETE FROM CARD_STAFF_ATTENDANCE2_OT_REDEEM 
					WHERE 
						StaffID = '".$StaffID[$i]."'
						AND RedeemDate = '$TargetDate' ";
			$results[$result_counter] = $StaffAttend3->db_db_query($sql);
			$result_counter += 1;
		}
	}else if($Status[$i] == CARD_STATUS_OUTGOING)# Staff's status is Outing
	{
		$sql = "SELECT 
					RecordID,
					InAttendanceRecordID,
					OutAttendanceRecordID,
					DutyCount,
					DutyStart,
					DutyEnd
				FROM 
					$card_log_table_name 
				WHERE 
					StaffID = '".$StaffID[$i]."'
					AND DayNumber = '$Day'
					AND SlotName = '".$StaffAttend3->Get_Safe_Sql_Query($SlotName)."' ";

		$temp_result = $StaffAttend3->returnArray($sql);
		if(sizeof($temp_result) > 0)# Daily Log Record Exists; Update it
		{
			$DutyStart = $temp_result[0]['DutyStart'];
			$DutyEnd = $temp_result[0]['DutyEnd'];
			$ProfileCountFor = $temp_result[0]['DutyCount'];
			
			$DailyLogRecordID = $temp_result[0]['RecordID'];
			$InProfileRecordID = $temp_result[0]['InAttendanceRecordID'];
			$OutProfileRecordID = $temp_result[0]['OutAttendanceRecordID'];
			
			if($OutProfileRecordID > 0)
			{
				$sql = "DELETE FROM CARD_STAFF_ATTENDANCE2_PROFILE WHERE RecordID = '$OutProfileRecordID' ";
				$results[$result_counter] = $StaffAttend3->db_db_query($sql);
				$result_counter += 1;
			}
			if($InProfileRecordID > 0) # In Profile Record Exists
			{
				$sql = "UPDATE CARD_STAFF_ATTENDANCE2_PROFILE
						SET
							StaffID = '".$StaffID[$i]."',
							ReasonID = ".(($ReasonOuting[$i]>0)?("'".$ReasonOuting[$i]."'"):"NULL").",
							RecordDate = '$DateStr',
							Waived = '".$Waived[$i]."',
							ProfileCountFor = '$ProfileCountFor',
							RecordType = '".CARD_STATUS_OUTGOING."',
							RecordStatus = '1',
							DateModified = NOW()
						WHERE
							RecordID = '$InProfileRecordID'
							AND StaffID = '".$StaffID[$i]."' 
							AND RecordDate = '$DateStr' ";
				$results[$result_counter] = $StaffAttend3->db_db_query($sql);//echo "#19".$results[$result_counter]."<br>";
				$result_counter += 1;
			}else # In Profile Record Does not Exist
			{
				$sql = "INSERT INTO CARD_STAFF_ATTENDANCE2_PROFILE
						(StaffID, ReasonID, RecordDate, Waived, ProfileCountFor, RecordType, RecordStatus, DateInput, DateModified)
						VALUES
						(
							'".$StaffID[$i]."',
							".(($ReasonOuting[$i]>0)?("'".$ReasonOuting[$i]."'"):"NULL").",
							'$DateStr',
							'".$Waived[$i]."',
							'$ProfileCountFor',
							'".CARD_STATUS_OUTGOING."',
							'1',
							NOW(),
							NOW()
						)";
				$results[$result_counter] = $StaffAttend3->db_db_query($sql);//echo "#20".$results[$result_counter]."<br>";
				$result_counter += 1;
				
				$InProfileRecordID = $StaffAttend3->db_insert_id();
			}
			
			if(trim($InHour[$i])=="" || trim($InMin[$i])=="" || trim($InSec[$i])=="")
				$InTime = "NULL";
			else
				$InTime = "'".$InHour[$i].":".$InMin[$i].":".$InSec[$i]."'";
			if(trim($OutHour[$i])=="" || trim($OutMin[$i])=="" || trim($OutSec[$i])=="")
				$OutTime = "NULL";
			else
				$OutTime = "'".$OutHour[$i].":".$OutMin[$i].":".$OutSec[$i]."'";
			
			# Update Daily Log Record
			$sql = "UPDATE $card_log_table_name 
					SET 
						StaffID = '".$StaffID[$i]."', 
						DayNumber = '$Day', 
						Duty = '1', 
						StaffPresent = '0',
						InTime = $InTime,
						OutTime = $OutTime,
						InSchoolStatus = '".CARD_STATUS_OUTGOING."',
						OutSchoolStatus = '".CARD_STATUS_OUTGOING."',
						InSchoolStation = '".$StaffAttend3->Get_Safe_Sql_Query(trim(urldecode(stripslashes($InStation[$i]))))."',
						OutSchoolStation = '".$StaffAttend3->Get_Safe_Sql_Query(trim(urldecode(stripslashes($OutStation[$i]))))."',
						InAttendanceRecordID = '$InProfileRecordID',
						OutAttendanceRecordID = NULL,
						InWaived = '".$Waived[$i]."',
						OutWaived = NULL,
						MinLate = NULL,
						MinEarlyLeave = NULL,
						Remark = '".$StaffAttend3->Get_Safe_Sql_Query(trim(urldecode(stripslashes($Remark[$i]))))."',
						RecordStatus = '1', 
						RecordType = '".CARD_STATUS_OUTGOING."',
						DateConfirmed = NOW(),
						ConfirmBy = '".$_SESSION['UserID']."'
					WHERE
						RecordID = '$DailyLogRecordID'
					";
			$results[$result_counter] = $StaffAttend3->db_db_query($sql);//echo "#21".$results[$result_counter]."<br>";
			$result_counter += 1;
		}/*else # Daily Log Record Does Not Exists; Insert a New One
		{
			# Insert a New In Profile Record
			$sql = "INSERT INTO CARD_STAFF_ATTENDANCE2_PROFILE
						(StaffID, ReasonID, RecordDate, Waived, ProfileCountFor, RecordType, RecordStatus, DateInput, DateModified)
						VALUES
						(
							'".$StaffID[$i]."',
							".(($ReasonOuting[$i]>0)?("'".$ReasonOuting[$i]."'"):"NULL").",
							'$DateStr',
							'".$Waived[$i]."',
							'$ProfileCountFor',
							'".CARD_STATUS_OUTGOING."',
							'1',
							NOW(),
							NOW()
						)";
			$results[$result_counter] = $StaffAttend3->db_db_query($sql);//echo "#22".$results[$result_counter]."<br>";
			$result_counter += 1;
			
			$InProfileRecordID = $StaffAttend3->db_insert_id();
			
			# Insert a New Daily Log Record
			$sql = "INSERT INTO $card_log_table_name 
						(StaffID, DayNumber, SlotName, Duty, DutyStart, DutyEnd, StaffPresent, InTime, InSchoolStatus, InSchoolStation, InWaived, OutTime, OutSchoolStatus, OutSchoolStation, OutWaived, InAttendanceRecordID, OutAttendanceRecordID, MinLate, MinEarlyLeave, Remark, RecordType, RecordStatus, DateInput, DateModified)
					VALUES
					('".$StaffID[$i]."',
						'$Day',
						'".$StaffAttend3->Get_Safe_Sql_Query($SlotName)."',
						'1',
						'$DutyStart',
						'$DutyEnd',
						'0',
						NULL,
						'".CARD_STATUS_OUTGOING."',
						NULL,
						NULL,
						NULL,
						'".CARD_STATUS_OUTGOING."',
						NULL,
						NULL,
						'$InProfileRecordID',
						NULL,
						NULL,
						NULL,
						'".$StaffAttend3->Get_Safe_Sql_Query(trim(urldecode(stripslashes($Remark[$i]))))."',
						'".CARD_STATUS_OUTGOING."',
						'1',
						NOW(),
						NOW()
					)";
			$results[$result_counter] = $StaffAttend3->db_db_query($sql);//echo "#23".$results[$result_counter]."<br>";
			$result_counter += 1;
		}*/
		if($DutyEnd == $LastDutyEnd)// Only calculate OT records if this slot is the last slot of that staff on this day
		{
			# Delete OT and Redeem Records
			$sql = "DELETE FROM CARD_STAFF_ATTENDANCE2_OT_RECORD 
					WHERE 
						StaffID = '".$StaffID[$i]."'
						AND RecordDate = '$TargetDate' ";
			$results[$result_counter] = $StaffAttend3->db_db_query($sql);
			$result_counter += 1;
			
			$sql = "DELETE FROM CARD_STAFF_ATTENDANCE2_OT_REDEEM 
					WHERE 
						StaffID = '".$StaffID[$i]."'
						AND RedeemDate = '$TargetDate' ";
			$results[$result_counter] = $StaffAttend3->db_db_query($sql);
			$result_counter += 1;
		}
	}else if($Status[$i] == $CARD_STATUS_LATEEARLYLEAVE)# Staff's status is Late And Early Leave
	{
		$sql = "SELECT 
					RecordID,
					InAttendanceRecordID,
					OutAttendanceRecordID,
					DutyCount,
					DutyStart,
					DutyEnd
				FROM 
					$card_log_table_name 
				WHERE 
					StaffID = '".$StaffID[$i]."'
					AND DayNumber = '$Day'
					AND SlotName = '".$StaffAttend3->Get_Safe_Sql_Query($SlotName)."' ";
		
		$temp_result = $StaffAttend3->returnArray($sql);
		if(sizeof($temp_result) > 0)# Daily Log Record Exists; Update it
		{
			$DutyStart = $temp_result[0]['DutyStart'];
			$DutyEnd = $temp_result[0]['DutyEnd'];
			$ProfileCountFor = $temp_result[0]['DutyCount'];
			
			$DailyLogRecordID = $temp_result[0]['RecordID'];
			$InProfileRecordID = $temp_result[0]['InAttendanceRecordID'];
			$OutProfileRecordID = $temp_result[0]['OutAttendanceRecordID'];
			if($InProfileRecordID > 0) # In Profile Record Exists
			{
				$sql = "UPDATE CARD_STAFF_ATTENDANCE2_PROFILE
						SET
							StaffID = '".$StaffID[$i]."',
							ReasonID = ".(($ReasonLate[$i]>0)?("'".$ReasonLate[$i]."'"):"NULL").",
							RecordDate = '$DateStr',
							Waived = '".$Waived[$i]."',
							ProfileCountFor = '$ProfileCountFor',
							RecordType = '".CARD_STATUS_LATE."',
							RecordStatus = '1',
							DateModified = NOW()
						WHERE
							RecordID = '$InProfileRecordID'
							AND StaffID = '".$StaffID[$i]."' 
							AND RecordDate = '$DateStr' ";
				$results[$result_counter] = $StaffAttend3->db_db_query($sql);//echo "#24".$results[$result_counter]."<br>";
				$result_counter += 1;
			}else # In Profile Record Does not Exist
			{
				$sql = "INSERT INTO CARD_STAFF_ATTENDANCE2_PROFILE
						(StaffID, ReasonID, RecordDate, Waived, ProfileCountFor, RecordType, RecordStatus, DateInput, DateModified)
						VALUES
						(
							'".$StaffID[$i]."',
							".(($ReasonLate[$i]>0)?("'".$ReasonLate[$i]."'"):"NULL").",
							'$DateStr',
							'".$Waived[$i]."',
							'$ProfileCountFor',
							'".CARD_STATUS_LATE."',
							'1',
							NOW(),
							NOW()
						)";
				$results[$result_counter] = $StaffAttend3->db_db_query($sql);//echo "#25".$results[$result_counter]."<br>";
				$result_counter += 1;
				
				$InProfileRecordID = $StaffAttend3->db_insert_id();
			}
			
			if($OutProfileRecordID > 0) # Out Profile Record Exists
			{
				$sql = "UPDATE CARD_STAFF_ATTENDANCE2_PROFILE
						SET
							StaffID = '".$StaffID[$i]."',
							ReasonID = ".(($ReasonEarlyLeave[$i]>0)?("'".$ReasonEarlyLeave[$i]."'"):"NULL").",
							RecordDate = '$DateStr',
							Waived = '".$ELWaived[$i]."',
							ProfileCountFor = '$ProfileCountFor',
							RecordType = '".CARD_STATUS_EARLYLEAVE."',
							RecordStatus = '1',
							DateModified = NOW()
						WHERE
							RecordID = '$OutProfileRecordID'
							AND StaffID = '".$StaffID[$i]."' 
							AND RecordDate = '$DateStr' ";
				$results[$result_counter] = $StaffAttend3->db_db_query($sql);//echo "#26".$results[$result_counter]."<br>";
				$result_counter += 1;
			}else # Out Profile Record Does not Exist
			{
				$sql = "INSERT INTO CARD_STAFF_ATTENDANCE2_PROFILE
						(StaffID, ReasonID, RecordDate, Waived, ProfileCountFor, RecordType, RecordStatus, DateInput, DateModified)
						VALUES
						(
							'".$StaffID[$i]."',
							".(($ReasonEarlyLeave[$i]>0)?("'".$ReasonEarlyLeave[$i]."'"):"NULL").",
							'$DateStr',
							'".$ELWaived[$i]."',
							'$ProfileCountFor',
							'".CARD_STATUS_EARLYLEAVE."',
							'1',
							NOW(),
							NOW()
						)";
				$results[$result_counter] = $StaffAttend3->db_db_query($sql);//echo "#27".$results[$result_counter]."<br>";
				$result_counter += 1;
				
				$OutProfileRecordID = $StaffAttend3->db_insert_id();
			}
			
			//$InTime = $InHour[$i].":".$InMin[$i].":".$InSec[$i];
			//$OutTime = $OutHour[$i].":".$OutMin[$i].":".$OutSec[$i];
			//$MinLate = floor((strtotime($InTime) - strtotime($DutyStart))/60);//Late minutes
			//$MinEarlyLeave = floor((strtotime($DutyEnd) - strtotime($OutTime))/60);//Early Leave minutes
			//$MinLateStr = ($MinLate < 0)?"NULL":"'".$MinLate."'";
			//$MinEarlyLeaveStr = ($MinEarlyLeave < 0)?"NULL":"'".$MinEarlyLeave."'";
			if(trim($InHour[$i])=="" || trim($InMin[$i])=="" || trim($InSec[$i])==""){
				$InTime = "NULL";
				$MinLateStr = "NULL";
			}else{
				$InTime = "'".$InHour[$i].":".$InMin[$i].":".$InSec[$i]."'";
				$InTime2 = $InHour[$i].":".$InMin[$i].":".$InSec[$i];
				$MinLate = floor((strtotime($TargetDate." ".$InTime2) - strtotime($TargetDate." ".$DutyStart))/60);//Late minutes
				$MinLateStr = ($MinLate < 0)?"NULL":"'".$MinLate."'";
			}
			if(trim($OutHour[$i])=="" || trim($OutMin[$i])=="" || trim($OutSec[$i])==""){
				$OutTime = "NULL";
				$MinEarlyLeaveStr = "NULL";
			}else{
				$OutTime = "'".$OutHour[$i].":".$OutMin[$i].":".$OutSec[$i]."'";
				$OutTime2 = $OutHour[$i].":".$OutMin[$i].":".$OutSec[$i];
				$MinEarlyLeave = floor((strtotime($TargetDate." ".$DutyEnd) - strtotime($TargetDate." ".$OutTime2))/60);//Early Leave minutes
				$MinEarlyLeaveStr = ($MinEarlyLeave < 0)?"NULL":"'".$MinEarlyLeave."'";
			}
			# Update Daily Log Record
			$sql = "UPDATE $card_log_table_name 
					SET 
						StaffID = '".$StaffID[$i]."', 
						DayNumber = '$Day', 
						Duty = '1', 
						StaffPresent = '1',
						InTime = $InTime,
						InSchoolStatus = '".CARD_STATUS_LATE."',
						InSchoolStation = '".$StaffAttend3->Get_Safe_Sql_Query(trim(urldecode(stripslashes($InStation[$i]))))."',
						InWaived = '".$Waived[$i]."',
						OutTime = $OutTime,
						OutSchoolStatus = '".CARD_STATUS_EARLYLEAVE."',
						OutSchoolStation = '".$StaffAttend3->Get_Safe_Sql_Query(trim(urldecode(stripslashes($OutStation[$i]))))."',
						OutWaived = '".$ELWaived[$i]."',
						MinLate = $MinLateStr,
						MinEarlyLeave = $MinEarlyLeaveStr,
						InAttendanceRecordID = '$InProfileRecordID',
						OutAttendanceRecordID = '$OutProfileRecordID',
						Remark = '".$StaffAttend3->Get_Safe_Sql_Query(trim(urldecode(stripslashes($Remark[$i]))))."',
						RecordStatus = '1', 
						DateConfirmed = NOW(),
						ConfirmBy = '".$_SESSION['UserID']."'
					WHERE
						RecordID = '$DailyLogRecordID'
					";
			$results[$result_counter] = $StaffAttend3->db_db_query($sql);//echo "#28".$results[$result_counter]."<br>";
			$result_counter += 1;
		}/*else # Daily Log Record Does Not Exists; Insert a New One
		{
			# Insert a New In Profile Record
			$sql = "INSERT INTO CARD_STAFF_ATTENDANCE2_PROFILE
						(StaffID, ReasonID, RecordDate, Waived, ProfileCountFor, RecordType, RecordStatus, DateInput, DateModified)
						VALUES
						(
							'".$StaffID[$i]."',
							".(($ReasonLate[$i]>0)?("'".$ReasonLate[$i]."'"):"NULL").",
							'$DateStr',
							'".$Waived[$i]."',
							'$ProfileCountFor',
							'".CARD_STATUS_LATE."',
							'1',
							NOW(),
							NOW()
						)";
			$results[$result_counter] = $StaffAttend3->db_db_query($sql);//echo "#29".$results[$result_counter]."<br>";
			$result_counter += 1;
			
			$InProfileRecordID = $StaffAttend3->db_insert_id();
			
			# Insert a New Out Profile Record
			$sql = "INSERT INTO CARD_STAFF_ATTENDANCE2_PROFILE
						(StaffID, ReasonID, RecordDate, Waived, ProfileCountFor, RecordType, RecordStatus, DateInput, DateModified)
						VALUES
						(
							'".$StaffID[$i]."',
							".(($ReasonEarlyLeave[$i]>0)?("'".$ReasonEarlyLeave[$i]."'"):"NULL").",
							'$DateStr',
							'".$ELWaived[$i]."',
							'$ProfileCountFor',
							'".CARD_STATUS_EARLYLEAVE."',
							'1',
							NOW(),
							NOW()
						)";
			$results[$result_counter] = $StaffAttend3->db_db_query($sql);//echo "#30".$results[$result_counter]."<br>";
			$result_counter += 1;
			
			$OutProfileRecordID = $StaffAttend3->db_insert_id();
			
			# Insert a New Daily Log Record for Late
			$InTime = $InHour[$i].":".$InMin[$i].":".$InSec[$i];
			$OutTime = $OutHour[$i].":".$OutMin[$i].":".$OutSec[$i];
			$MinLate = (strtotime($InTime) - strtotime($DutyStart))/60;//Late minutes
			$MinEarlyLeave = (strtotime($DutyEnd) - strtotime($OutTime))/60;//Early Leave minutes
			
			$sql = "INSERT INTO $card_log_table_name 
						(StaffID, DayNumber, SlotName, Duty, DutyStart, DutyEnd, StaffPresent, InTime, OutTime, InSchoolStatus, OutSchoolStatus, InSchoolStation, OutSchoolStation, InWaived, OutWaived, InAttendanceRecordID, OutAttendanceRecordID, MinLate, MinEarlyLeave, Remark, RecordType, RecordStatus, DateInput, DateModified)
					VALUES
					('".$StaffID[$i]."',
						'$Day',
						'".$StaffAttend3->Get_Safe_Sql_Query($SlotName)."',
						'1',
						'$DutyStart',
						'$DutyEnd',
						'1',
						'$InTime',
						'$OutTime',
						'".CARD_STATUS_LATE."',
						'".CARD_STATUS_EARLYLEAVE."',
						'".$StaffAttend3->Get_Safe_Sql_Query(trim(urldecode(stripslashes($InStation[$i]))))."',
						'".$StaffAttend3->Get_Safe_Sql_Query(trim(urldecode(stripslashes($OutStation[$i]))))."',
						'".$Waived[$i]."',
						'".$ELWaived[$i]."',
						'$InProfileRecordID',
						'$OutProfileRecordID',
						'$MinLate',
						'$MinEarlyLeave',
						'".$StaffAttend3->Get_Safe_Sql_Query(trim(urldecode(stripslashes($Remark[$i]))))."',
						'".CARD_STATUS_LATE."',
						'1',
						NOW(),
						NOW()
					)";
			$results[$result_counter] = $StaffAttend3->db_db_query($sql);//echo "#31".$results[$result_counter]."<br>";
			$result_counter += 1;
		}*/
		if($DutyEnd == $LastDutyEnd)// Only calculate OT records if this slot is the last slot of that staff on this day
		{
			# Delete OT and Redeem Records
			$sql = "DELETE FROM CARD_STAFF_ATTENDANCE2_OT_RECORD 
					WHERE 
						StaffID = '".$StaffID[$i]."'
						AND RecordDate = '$TargetDate' ";
			$results[$result_counter] = $StaffAttend3->db_db_query($sql);
			$result_counter += 1;
			
			$sql = "DELETE FROM CARD_STAFF_ATTENDANCE2_OT_REDEEM 
					WHERE 
						StaffID = '".$StaffID[$i]."'
						AND RedeemDate = '$TargetDate' ";
			$results[$result_counter] = $StaffAttend3->db_db_query($sql);
			$result_counter += 1;
		}
	}else if($Status[$i] == CARD_STATUS_HOLIDAY) # Staff's status is set to Holiday
	{
		$sql = "SELECT 
						RecordID,
						InAttendanceRecordID,
						OutAttendanceRecordID,
						DutyCount,
						DutyStart,
						DutyEnd
				FROM 
					$card_log_table_name 
				WHERE 
					StaffID = '".$StaffID[$i]."'
					AND DayNumber = '$Day'
					AND SlotName = '".$StaffAttend3->Get_Safe_Sql_Query($SlotName)."' ";
	
		$temp_result = $StaffAttend3->returnArray($sql);
		if(sizeof($temp_result) > 0)# Daily Log Record Exists; Update it
		{
			$DutyStart = $temp_result[0]['DutyStart'];
			$DutyEnd = $temp_result[0]['DutyEnd'];
			$ProfileCountFor = $temp_result[0]['DutyCount'];
			
			$DailyLogRecordID = $temp_result[0]['RecordID'];
			$InProfileRecordID = $temp_result[0]['InAttendanceRecordID'];
			$OutProfileRecordID = $temp_result[0]['OutAttendanceRecordID'];
			
			if($OutProfileRecordID > 0)
			{
				$sql = "DELETE FROM CARD_STAFF_ATTENDANCE2_PROFILE WHERE RecordID = '$OutProfileRecordID' ";
				$results[$result_counter] = $StaffAttend3->db_db_query($sql);
				$result_counter += 1;
			}
			if($InProfileRecordID > 0) # In Profile Record Exists
			{
				$sql = "UPDATE CARD_STAFF_ATTENDANCE2_PROFILE
						SET
							StaffID = '".$StaffID[$i]."',
							ReasonID = ".(($ReasonHoliday[$i]>0)?("'".$ReasonHoliday[$i]."'"):"NULL").",
							RecordDate = '$DateStr',
							Waived = '".$Waived[$i]."',
							ProfileCountFor = '$ProfileCountFor',
							RecordType = '".CARD_STATUS_HOLIDAY."',
							RecordStatus = '1',
							DateModified = NOW()
						WHERE
							RecordID = '$InProfileRecordID'
							AND StaffID = '".$StaffID[$i]."' 
							AND RecordDate = '$DateStr' ";
				$results[$result_counter] = $StaffAttend3->db_db_query($sql);//echo "#32".$results[$result_counter]."<br>";
				$result_counter += 1;
			}else # In Profile Record Does not Exist
			{
				$sql = "INSERT INTO CARD_STAFF_ATTENDANCE2_PROFILE
						(StaffID, ReasonID, RecordDate, Waived, ProfileCountFor, RecordType, RecordStatus, DateInput, DateModified)
						VALUES
						(
							'".$StaffID[$i]."',
							".(($ReasonHoliday[$i]>0)?("'".$ReasonHoliday[$i]."'"):"NULL").",
							'$DateStr',
							'".$Waived[$i]."',
							'$ProfileCountFor',
							'".CARD_STATUS_HOLIDAY."',
							'1',
							NOW(),
							NOW()
						)";
				$results[$result_counter] = $StaffAttend3->db_db_query($sql);//echo "#33".$results[$result_counter]."<br>";
				$result_counter += 1;
				
				$InProfileRecordID = $StaffAttend3->db_insert_id();
			}
			
			if(trim($InHour[$i])=="" || trim($InMin[$i])=="" || trim($InSec[$i])=="")
				$InTime = "NULL";
			else
				$InTime = "'".$InHour[$i].":".$InMin[$i].":".$InSec[$i]."'";
			if(trim($OutHour[$i])=="" || trim($OutMin[$i])=="" || trim($OutSec[$i])=="")
				$OutTime = "NULL";
			else
				$OutTime = "'".$OutHour[$i].":".$OutMin[$i].":".$OutSec[$i]."'";
			
			# Update Daily Log Record
			$sql = "UPDATE $card_log_table_name 
					SET 
						StaffID = '".$StaffID[$i]."', 
						DayNumber = '$Day',  
						Duty = '0', 
						StaffPresent = '0',
						InTime = $InTime,
						OutTime = $OutTime,
						InSchoolStatus = '".CARD_STATUS_HOLIDAY."',
						OutSchoolStatus = '".CARD_STATUS_HOLIDAY."',
						InSchoolStation = '".$StaffAttend3->Get_Safe_Sql_Query(trim(urldecode(stripslashes($InStation[$i]))))."',
						OutSchoolStation = '".$StaffAttend3->Get_Safe_Sql_Query(trim(urldecode(stripslashes($OutStation[$i]))))."',
						InAttendanceRecordID = '$InProfileRecordID',
						OutAttendanceRecordID = NULL,
						InWaived = '".$Waived[$i]."',
						OutWaived = NULL,
						MinLate = NULL,
						MinEarlyLeave = NULL,
						Remark = '".$StaffAttend3->Get_Safe_Sql_Query(trim(urldecode(stripslashes($Remark[$i]))))."',
						RecordStatus = '1', 
						RecordType = '".CARD_STATUS_HOLIDAY."',
						DateConfirmed = NOW(),
						ConfirmBy = '".$_SESSION['UserID']."'
					WHERE
						RecordID = '$DailyLogRecordID'
					";
			$results[$result_counter] = $StaffAttend3->db_db_query($sql);//echo "#34".$results[$result_counter]."<br>";
			$result_counter += 1;
		}/*else # Daily Log Record Does Not Exists; Insert a New One
		{
			# Insert a New In Profile Record
			$sql = "INSERT INTO CARD_STAFF_ATTENDANCE2_PROFILE
						(StaffID, ReasonID, RecordDate, Waived, ProfileCountFor, RecordType, RecordStatus, DateInput, DateModified)
						VALUES
						(
							'".$StaffID[$i]."',
							".(($ReasonHoliday[$i]>0)?("'".$ReasonHoliday[$i]."'"):"NULL").",
							'$DateStr',
							'".$Waived[$i]."',
							'$ProfileCountFor',
							'".CARD_STATUS_HOLIDAY."',
							'1',
							NOW(),
							NOW()
						)";
			$results[$result_counter] = $StaffAttend3->db_db_query($sql);//echo "#35".$results[$result_counter]."<br>";
			$result_counter += 1;
			
			$InProfileRecordID = $StaffAttend3->db_insert_id();
			
			# Insert a New Daily Log Record
			$sql = "INSERT INTO $card_log_table_name 
						(StaffID, DayNumber, SlotName, Duty, DutyStart, DutyEnd, StaffPresent, InTime, InSchoolStatus, InSchoolStation, InWaived, OutTime, OutSchoolStatus, OutSchoolStation, OutWaived, InAttendanceRecordID, OutAttendanceRecordID, MinLate, MinEarlyLeave, Remark, RecordType, RecordStatus, DateInput, DateModified)
					VALUES
					('".$StaffID[$i]."',
						'$Day',
						'".$StaffAttend3->Get_Safe_Sql_Query($SlotName)."',
						'0',
						'$DutyStart',
						'$DutyEnd',
						'0',
						NULL,
						'".CARD_STATUS_HOLIDAY."',
						NULL,
						NULL,
						NULL,
						'".CARD_STATUS_HOLIDAY."',
						NULL,
						'$InProfileRecordID',
						NULL,
						NULL,
						NULL,
						'".$StaffAttend3->Get_Safe_Sql_Query(trim(urldecode(stripslashes($Remark[$i]))))."',
						'".CARD_STATUS_HOLIDAY."',
						'1',
						NOW(),
						NOW()
					)";
			$results[$result_counter] = $StaffAttend3->db_db_query($sql);//echo "#36".$results[$result_counter]."<br>";
			$result_counter += 1;
		}*/
		if($DutyEnd == $LastDutyEnd)// Only calculate OT records if this slot is the last slot of that staff on this day
		{
			# Delete OT and Redeem Records
			$sql = "DELETE FROM CARD_STAFF_ATTENDANCE2_OT_RECORD 
					WHERE 
						StaffID = '".$StaffID[$i]."'
						AND RecordDate = '$TargetDate' ";
			$results[$result_counter] = $StaffAttend3->db_db_query($sql);
			$result_counter += 1;
			
			$sql = "DELETE FROM CARD_STAFF_ATTENDANCE2_OT_REDEEM 
					WHERE 
						StaffID = '".$StaffID[$i]."'
						AND RedeemDate = '$TargetDate' ";
			$results[$result_counter] = $StaffAttend3->db_db_query($sql);
			$result_counter += 1;
		}
	}
}// End for loop
//debug_r($results);
if(in_array(false, $results))
{
	$StaffAttend3->RollBack_Trans();
	echo $Lang['StaffAttendance']['TakeAttendanceFail'];
}else
{
	$StaffAttend3->Commit_Trans();
	echo $Lang['StaffAttendance']['TakeAttendanceSuccess'];
}
intranet_closedb();
?>