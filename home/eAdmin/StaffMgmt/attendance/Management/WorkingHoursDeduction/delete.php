<?php
// Editing by 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!$StaffAttend3->Check_Access_Right('MGMT-OT'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0) || !$sys_custom['StaffAttendance']['WongFutNamCollege']) {
	//header ("Location: /");
	intranet_closedb();
	exit();
}

$recordIdAry = array();
for($i=0;$i<count($_POST['RecordID']);$i++){
	$recordIdAry = array_merge($recordIdAry,explode(",",$_POST['RecordID'][$i]));
}
$recordIdAry = array_values($recordIdAry);
$success = $StaffAttend3->Delete_Working_Hours_Deduction_Record(array('RecordID'=>$recordIdAry));
$Msg = $success? $Lang['General']['ReturnMessage']['DeleteSuccess'] : $Lang['General']['ReturnMessage']['DeleteUnsuccess'];
$_SESSION['STAFF_ATTENDANCE_WORKING_HOURS_RETURN_MSG'] = $Msg;

intranet_closedb();
header("Location: ./");
exit;
?>