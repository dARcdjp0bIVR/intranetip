<?php
// Editing by 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend2.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!$StaffAttend3->Check_Access_Right('MGMT-OT'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0) || !$sys_custom['StaffAttendance']['WongFutNamCollege']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}
$linterface = new interface_html();
$StaffAttend3UI = new libstaffattend3_ui();

$pageSizeChangeEnabled = true;

$field = trim(urldecode(stripslashes($_REQUEST['field'])));
$order = trim(urldecode(stripslashes($_REQUEST['order'])));
$pageNo = trim(urldecode(stripslashes($_REQUEST['pageNo'])));
if(isset($_REQUEST['numPerPage'])){
	$numPerPage = trim(urldecode(stripslashes($_REQUEST['numPerPage'])));
	$page_size = $numPerPage;
}else if (isset($ck_staff_attendance_working_hours_deduction_page_size) && $ck_staff_attendance_working_hours_deduction_page_size != "")
{
	$page_size = $ck_staff_attendance_working_hours_deduction_page_size;
	$numPerPage = $ck_staff_attendance_working_hours_deduction_page_size;
}else{
	if(!is_numeric($numPerPage)) $numPerPage = 50;
	$page_size = $numPerPage;
}

$arrCookies = array();
$arrCookies[] = array("ck_staff_attendance_working_hours_deduction_page_size", "numPerPage");
$arrCookies[] = array("ck_staff_attendance_working_hours_deduction_page_number", "pageNo");
$arrCookies[] = array("ck_staff_attendance_working_hours_deduction_page_order", "order");
$arrCookies[] = array("ck_staff_attendance_working_hours_deduction_page_field", "field");	
if($clearCoo == 1)
	clearCookies($arrCookies);
else
	updateGetCookies($arrCookies);

if(!isset($field) || $field == '' || !in_array($field,array(0,1,2,3,4,5))) $field = 1; 
if(!isset($order) || $order == '' || !in_array($order,array(0,1))) $order = 0;
if(!isset($pageNo) || $pageNo == '') $pageNo = 1;


$li = new libdbtable2007($field,$order,$pageNo);

$Keyword = stripslashes(trim($_POST['Keyword']));
$TargetYear = isset($_POST['TargetYear']) ? $_POST['TargetYear'] : date("Y");
$TargetMonth = isset($_POST['TargetMonth']) ? $_POST['TargetMonth'] : date("m");

$MonthSelection = '<select name="TargetMonth" id="TargetMonth">';
for ($i=1; $i<= 12; $i++)
{
	$value = ($i<10)?'0'.$i:$i;
	$MonthSelection .= '<option value="'.$value.'" '.(($TargetMonth == $value)? 'selected':'').'>'.$value.'</option>';
}           			
$MonthSelection .= '</select>';
$years_arr = $StaffAttend3UI->Get_Year_Selection_Array($TargetYear);
$YearSelection = '<select name="TargetYear" id="TargetYear">';
for($i=0;$i<sizeof($years_arr);$i++)
{
	$value = $years_arr[$i];
	$YearSelection .= '<option value="'.$value.'" '.(($TargetYear == $value)? 'selected':'').'>'.$value.'</option>';
}
$YearSelection .= '</select>';
$year_month_selection = $MonthSelection.' - '.$YearSelection;


//$params = array('IsSqlQuery'=>1,'TargetYear'=>$TargetYear,'TargetMonth'=>$TargetMonth,'Keyword'=>$Keyword,'GroupByStaff'=>1);
$params = array('IsSqlQuery'=>1,'Keyword'=>$Keyword,'GroupByStaff'=>1);
$sql = $StaffAttend3->Get_Working_Hours_Deduction_Records($params);

$li->sql = $sql;
$li->IsColOff = "IP25_table";

$li->field_array = array("StaffName","RecordDate","w.DeductMin","w.Remark","w.DateModified","ModifiedBy");
$li->column_array = array(22,18,22,22,22,22);
$li->wrap_array = array(0,0,0,0,0,0);

$pos = 0;
$li->column_list .= "<th width=\"1\" class=\"num_check\">#</th>\n";
$li->column_list .= "<th>".$li->column($pos++, $Lang['StaffAttendance']['StaffName'])."</th>\n";
$li->column_list .= "<th>".$li->column($pos++, $Lang['General']['RecordDate'])."</th>\n";
$li->column_list .= "<th>".$li->column($pos++, $Lang['StaffAttendance']['DeductHours'])."</th>\n";
$li->column_list .= "<th>".$li->column($pos++, $Lang['StaffAttendance']['Remark'])."</th>\n";
$li->column_list .= "<th>".$li->column($pos++, $Lang['General']['LastUpdatedTime'])."</th>\n";
$li->column_list .= "<th>".$li->column($pos++, $Lang['General']['LastUpdatedBy'])."</th>\n";
$li->column_list .= "<th width=\"1\">".$li->check("RecordID[]")."</th>\n";
$li->no_col = $pos+2;

$tool_buttons = array();
$tool_buttons[] = array('edit',"javascript:checkEdit(document.form1,'RecordID[]','edit.php')");
$tool_buttons[] = array('delete',"javascript:checkRemove(document.form1,'RecordID[]','delete.php')");


$CurrentPageArr['eAdminStaffAttendance'] = 1;
$CurrentPage['WorkingHoursDeduction'] = 1;

$TAGS_OBJ[] = array($Lang['StaffAttendance']['WorkingHoursDeduction'], "", 0);
$MODULE_OBJ = $StaffAttend3UI->GET_MODULE_OBJ_ARR();

if(isset($_SESSION['STAFF_ATTENDANCE_WORKING_HOURS_RETURN_MSG']))
{
	$Msg = $_SESSION['STAFF_ATTENDANCE_WORKING_HOURS_RETURN_MSG'];
	unset($_SESSION['STAFF_ATTENDANCE_WORKING_HOURS_RETURN_MSG']);
}
$linterface->LAYOUT_START($Msg);
echo $StaffAttend3UI->Include_JS_CSS();
?>
<br />
<form id="form1" name="form1" method="post" action="">
	<div class="content_top_tool">
		<div class="Conntent_tool">
			<?=$linterface->GET_LNK_NEW("edit.php", $Lang['Btn']['New'], $__ParOnClick="", $__ParOthers="", $__ParClass="", $__useThickBox=0);?>
		</div>
		<div class="Conntent_search"><input type="text" id="Keyword" name="Keyword" value="<?=intranet_htmlspecialchars($Keyword)?>" onkeyup="GoSearch(event);"></div>
		<br style="clear:both;">
	</div>
	<br style="clear:both;">
	<div class="table_filter">
		<!--
		<?=$year_month_selection?>&nbsp;<input name="submitsmall" class="formsmallbutton" onmouseover="this.className='formsmallbuttonon'" onmouseout="this.className='formsmallbutton'" onclick="document.form1.submit();" value="<?=$Lang['Btn']['View']?>" type="button" />
		-->
	</div>
	<br style="clear:both">	
	<?=$linterface->Get_DBTable_Action_Button_IP25($tool_buttons)?>		
	<?=$li->display();?>
	<input type="hidden" id="pageNo" name="pageNo" value="<?=$li->pageNo?>">
	<input type="hidden" id="order" name="order" value="<?=$li->order?>">
	<input type="hidden" id="field" name="field" value="<?=$li->field?>">
	<input type="hidden" id="page_size_change" name="page_size_change" value="1">
	<input type="hidden" id="numPerPage" name="numPerPage" value="<?=$li->page_size?>">
</form>	
<br /><br />
<script type="text/JavaScript" language="JavaScript">
function GoSearch(evt)
{
	var key = evt.which || evt.charCode || evt.keyCode;
	
	if (key == 13) // enter
	{
		document.form1.submit();
	}
	else
		return false;
}
</script>
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>