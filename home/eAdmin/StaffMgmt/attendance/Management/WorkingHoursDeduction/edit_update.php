<?php
// Editing by 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!$StaffAttend3->Check_Access_Right('MGMT-OT'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0) || !$sys_custom['StaffAttendance']['WongFutNamCollege']) {
	//header ("Location: /");
	intranet_closedb();
	exit();
}

$is_edit = isset($_POST['RecordID']) && count($_POST['RecordID'])>0;

$staff_id_ary = $_POST['StaffID'];
$target_date_ary = $_POST['TargetDate'];
$deduct_min = trim($_POST['DeductMin']);
$remark = stripslashes(trim($_POST['Remark']));

$StaffAttend3->Start_Trans();
$resultAry = array();

if($is_edit){
	$resultAry[] = $StaffAttend3->Delete_Working_Hours_Deduction_Record(array('RecordID'=>$_POST['RecordID']));
}

for($i=0;$i<count($staff_id_ary);$i++)
{
	for($j=0;$j<count($target_date_ary);$j++){
		$resultAry[] = $StaffAttend3->Add_Working_Hours_Deduction_Record(array('StaffID'=>$staff_id_ary[$i],'RecordDate'=>$target_date_ary[$j],'DeductMin'=>$deduct_min,'Remark'=>$remark));
	}
}

if(!in_array(false,$resultAry)){
	$StaffAttend3->Commit_Trans();
	$Msg = $is_edit? $Lang['General']['ReturnMessage']['UpdateSuccess'] : $Lang['General']['ReturnMessage']['AddSuccess'];
}else{
	$StaffAttend3->RollBack_Trans();
	$Msg = $is_edit? $Lang['General']['ReturnMessage']['UpdateUnsuccess'] : $Lang['General']['ReturnMessage']['AddUnsuccess'];
}
$_SESSION['STAFF_ATTENDANCE_WORKING_HOURS_RETURN_MSG'] = $Msg;

intranet_closedb();
header("Location: ./");
exit;
?>