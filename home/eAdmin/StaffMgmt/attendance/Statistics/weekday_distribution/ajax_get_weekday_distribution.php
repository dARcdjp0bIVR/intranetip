<?php

/*
 * Change log
 * 
 * Date:	2017-07-14 (Icarus)
 * 			form the data in json ary and assign to $highchartDataAry, it will get by the ajax in index.php
 * 
 * */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");
include_once($PATH_WRT_ROOT."includes/json.php");

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!$StaffAttend3->Check_Access_Right('REPORT-WEEKDAYSTAT'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	echo 'die';
	intranet_closedb();
	exit();
}

$StartDate = $_REQUEST['StartDate'];
$EndDate = $_REQUEST['EndDate'];
$Status = $_REQUEST['Status'];
$GroupID = $_REQUEST['GroupID'];

$StaffAttend3UI = new libstaffattend3_ui();
$json = new JSON_obj();

//echo $StaffAttend3UI->Get_Stat_Weekday_Distribution_Chart_JS_String($GroupID,$StartDate,$EndDate,$Status);


$chart_ary = $StaffAttend3->Get_Stat_Weekday_Distribution($GroupID,$StartDate,$EndDate,$Status);

########### highchart ###########
$day_of_week = sizeof($chart_ary);						// from Mon to Sun = 7 + $chart_ary['Max'] = 8
$json_ary = array();									// store the data in array format that need to be change to json syntax
$y_max_record = $chart_ary['Max'];						// store the greatest value need to be shown on y axis
### get the x-axis ###									// store the Sun to Sat in a week in x-axis
$xaxis_data = array($Lang['StaffAttendance']['Sun'],
				    $Lang['StaffAttendance']['Mon'],
				    $Lang['StaffAttendance']['Tue'],
				    $Lang['StaffAttendance']['Wed'],
				    $Lang['StaffAttendance']['Thur'],
				    $Lang['StaffAttendance']['Fri'],
				    $Lang['StaffAttendance']['Sat']
		      );
### generate the array of json data ###
$json_ary[0]['name'] = $Lang['StaffAttendance']['Late'];
$json_ary[1]['name'] = $Lang['StaffAttendance']['Absent'];
$json_ary[2]['name'] = $Lang['StaffAttendance']['Outgoing'];
$json_ary[3]['name'] = $Lang['StaffAttendance']['EarlyLeave'];
$json_ary[4]['name'] = $Lang['StaffAttendance']['Holiday'];
for($i=1;$i<$day_of_week;$i++){
	$json_ary[0]['data'][] = $chart_ary[$i][CARD_STATUS_LATE];
	$json_ary[1]['data'][] = $chart_ary[$i][CARD_STATUS_ABSENT];
	$json_ary[2]['data'][] = $chart_ary[$i][CARD_STATUS_EARLYLEAVE];
	$json_ary[3]['data'][] = $chart_ary[$i][CARD_STATUS_HOLIDAY];
	$json_ary[4]['data'][] = $chart_ary[$i][CARD_STATUS_OUTGOING];
}
//$jsonDataAry = $json->encode($json_ary);				// generate the json syntax
########### highchart end ###########
###################################
$highchart_ary = array('xaxis_data'=>$xaxis_data, 'y_max_record'=>$y_max_record,'json_ary'=>$json_ary);
$highchartDataAry = $json->encode($highchart_ary);	

echo $highchartDataAry;
intranet_closedb();
?>