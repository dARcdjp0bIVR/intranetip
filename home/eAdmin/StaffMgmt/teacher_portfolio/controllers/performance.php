<?php
if (!defined('TEACHER_PORTFOLIO')) die('Page not found');
if (!class_exists("FileController")) include_once(dirname(__FILE__) . "/file.controller.php");
class Performance extends FileController {
	public function __construct($parent) {
		parent::__construct($parent);
		$this->parent->data["section"] = "performance";
		$this->parent->data["yearArr"] = $this->parent->YearObj->getAcademicYear();
		$this->parent->data["selectedYear"] = isset($this->parent->data["postData"]["selectedYear"]) ? $this->parent->data["postData"]["selectedYear"] : "";
		$this->parent->data["html_YearPanel"] = $this->parent->YearObj->getYearPanel($this->parent->data["yearArr"], $this->parent->data["lang"], $this->parent->data["section"], $this->parent->data["selectedYear"]);
		$this->parent->data["tpConfigs"] = $this->parent->userInfo["tpConfigs"];
		
		$this->parent->load_model('FileInfoObj', "myOBJ");
		$this->parent->load_model('FileObj');
		$this->parent->myOBJ->setModuleSection("PERFORMANCE");
		$this->parent->FileObj->setCoreKey($this->parent->coreKey);
		$this->parent->FileObj->setPathInfo($this->parent->pathInfo);
		$this->parent->FileObj->setCoreData($this->parent->data);
		$this->parent->FileObj->setTPModuleConfig($this->parent->getTMConfig());
		$this->parent->FileObj->setConfig($this->parent->getCustomConfig());
	}
	
	public function addrecord() {
		$this->parent->data["allTeachers"] = $this->parent->UserObj->getAllTeacher();
		$this->parent->data["fileLimit"] = 1;
		$this->parent->data["currDate"] = date("Ymd");
		$this->parent->FileObj->checkFolder($this->parent->data["section"]);
		$this->parent->data["currAction"] = "add";
		if ($this->parent->configs["isDev"]) {
			$this->parent->data["debugMsg"] = $this->parent->getMemoryInfo();
		}
		$this->parent->tpl->view($this->parent->data["section"] . "/edit", $this->parent->data);
	}

	public function editrecord() {
		$this->parent->data["allTeachers"] = $this->parent->UserObj->getAllTeacher();
		$this->parent->data["fileLimit"] = 1;
		$this->parent->data["currAction"] = "edit";
		$this->parent->data["currDate"] = date("Ymd");
		$this->parent->data["infoRecord"] = $this->parent->myOBJ->getRecordByID($this->parent->data["postData"]["InfoID"]);
		if (!$this->parent->data["infoRecord"]) {
			$this->parent->data["infoRecord"] = "no record";
		} else {
			$this->parent->data["infoRecordFiles"] = $this->parent->FileObj->getFilesByInfoID($this->parent->data["postData"]["InfoID"], $this->parent->data["userInfo"]["info"]["SelectedUserID"]);
		}
		if ($this->parent->configs["isDev"]) {
			$this->parent->data["debugMsg"] = $this->parent->getMemoryInfo();
		}
		$this->parent->tpl->view($this->parent->data["section"] . "/edit", $this->parent->data);
	}
}
?>