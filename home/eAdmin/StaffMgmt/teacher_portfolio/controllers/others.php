<?php
if (!defined('TEACHER_PORTFOLIO')) die('Page not found');
if (!class_exists("FileController")) include_once(dirname(__FILE__) . "/file.controller.php");
class Others extends FileController {
	public function __construct($parent) {
		parent::__construct($parent);
		$this->parent = $parent;
		$this->parent->data["section"] = "others";
		$this->parent->data["tpConfigs"] = $this->parent->userInfo["tpConfigs"];
		$this->parent->load_model('FileInfoObj', "myOBJ");
		$this->parent->load_model('FileObj');
		$this->parent->myOBJ->setModuleSection("OTHERS");
		$this->parent->FileObj->setCoreKey($this->parent->coreKey);
		$this->parent->FileObj->setPathInfo($this->parent->pathInfo);
		$this->parent->FileObj->setCoreData($this->parent->data);
		$this->parent->FileObj->setTPModuleConfig($this->parent->getTMConfig());
		$this->parent->FileObj->setConfig($this->parent->getCustomConfig());
	}
}
?>