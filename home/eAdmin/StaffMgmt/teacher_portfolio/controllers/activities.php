<?php
if (!defined('TEACHER_PORTFOLIO')) die('Page not found');
if (!class_exists("FormTemplateController")) include_once(dirname(__FILE__) . "/formtemplate.controller.php");
class Activities extends FormTemplateController {
	public function __construct($parent) {
		parent::__construct($parent);
		$this->parent->data["section"] = "activities";
		$this->parent->data["template"] = "ACTIVITIES";
		$this->parent->load_model('FormTemplateObj');
		
		/***********************************************************/
		$this->parent->data["TemplateType"] = "GRID";
		$this->parent->data["TemplateGridTable"] = "INTRANET_TEACHER_PORTFOLIO_FORM_DATA_GRID_" . $this->parent->data["template"];
		$this->parent->data["TemplateGridTableStructure"] = array(
					"FormTemplateID" => array( "field" => "FormTemplateID", "data" => "", "output" => false),
					"UserID" => array( "field" => "UserID", "groupBY" => true, "data" => "", "output" => false),
					"AcademicYear" => array( "field" => "AcademicYear", "groupBY" => true, "data" => "", "output" => true),
					"SemesterEn" => array( "field" => "SemesterEn", "action" => "EMPTY", "groupBY" => false, "data" => "", "output" => true),
					"SemesterChi" => array( "field" => "SemesterChi", "action" => "EMPTY", "groupBY" => false, "data" => "", "output" => true),
					"Hours" => array( "field" => "Hours", "groupBY" => false, "action" => "SUM", "data" => "", "output" => true),
					"EventNameEn" => array( "field" => "EventNameEn", "groupBY" => false, "action" => "EMPTY", "data" => "", "output" => true),
					"EventNameChi" => array( "field" => "EventNameChi", "groupBY" => false, "action" => "EMPTY", "data" => "", "output" => true),
					"RemarksEn" => array( "field" => "RemarksEn", "groupBY" => false, "action" => "EMPTY", "data" => "", "output" => true),
					"RemarksChi" => array( "field" => "RemarksChi", "groupBY" => false, "action" => "EMPTY", "data" => "", "output" => true),
					"DateInput" => array( "field" => "DateInput", "groupBY" => false, "data" => "__dbnow__", "output" => false),
					"InputBy" => array( "field" => "InputBy", "groupBY" => false, "data" => "", "output" => false),
					"DateModified" => array( "field" => "DateModified", "groupBY" => false, "data" => "__dbnow__", "output" => false),
					"ModifyBy" => array( "field" => "ModifyBy", "groupBY" => false, "data" => "", "output" => false)
		);
		/***********************************************************/
		
		$predefinedArr = array(
			"UserID" => array(
				"FieldLabel" => "UserID",
				"FieldNameChi" => "內聯網帳號",
				"FieldNameEn" => "Teacher Login ID",
				"isMultiLang" => "N",
				"SampleDataChi"=> "",
				"SampleData"=> "abcd1234",
				"FieldType"=> "INT",
				"FixedItem" => "Y",
				"FieldStatus" => "required",
				"showInGrid" => "N",
				"isUsed" => "Y",
				"Priority" => "1"
			),
			"TeacherName" => array(
				"FieldLabel" => "TeacherName",
				"FieldNameChi" => "教師名稱",
				"FieldNameEn" => "Teacher Name",
				"isMultiLang" => "Y",
				"SampleDataChi"=> "陳大文",
				"SampleData"=> "Chan Ta Man",
				"FieldType"=> "TEXT",
				"FixedItem" => "Y",
				"FieldStatus" => "reference",
				"showInGrid" => "N",
				"isUsed" => "Y",
				"Priority" => "2"
			),
			"AcademicYear" => array(
				"FieldLabel" => "AcademicYear",
				"FieldNameChi" => "學年",
				"FieldNameEn" => "School Year",
				"isMultiLang" => "N",
				"SampleDataChi"=> "",
				"SampleData"=> "2016-2017",
				"FieldType"=> "TEXT",
				"FixedItem" => "N",
				"FieldStatus" => "required",
				"showInGrid" => "Y",
				"isUsed" => "Y",
				"Priority" => "3"
			),
			"Semester" => array(
				"FieldLabel" => "Semester",
				"FieldNameChi" => "學期",
				"FieldNameEn" => "Semester",
				"isMultiLang" => "Y",
				"SampleDataChi"=> "學期",
				"SampleData"=> "Semester",
				"FieldType"=> "TEXT",
				"FixedItem" => "N",
				"FieldStatus" => "required",
				"showInGrid" => "Y",
				"isUsed" => "Y",
				"Priority" => "4"
			),
			"Hours" => array(
				"FieldLabel" => "Hours",
				"FieldNameChi" => "時數",
				"FieldNameEn" => "Hours",
				"isMultiLang" => "N",
				"SampleDataChi"=> "6",
				"SampleData"=> "6",
				"FieldType"=> "FLOAT",
				"FixedItem" => "N",
				"FieldStatus" => "required",
				"showInGrid" => "Y",
				"isUsed" => "Y",
				"Priority" => "5"
			),
			"EventName" => array(
				"FieldLabel" => "EventName",
				"FieldNameChi" => "活動名稱",
				"FieldNameEn" => "Event Name",
				"isMultiLang" => "Y",
				"SampleDataChi"=> "活動名稱",
				"SampleData"=> "Event Name",
				"FieldType"=> "TEXT",
				"FixedItem" => "N",
				"FieldStatus" => "required",
				"showInGrid" => "Y",
				"isUsed" => "Y",
				"Priority" => "6"
			),
			"Remarks" => array(
				"FieldLabel" => "Remarks",
				"FieldNameChi" => "備註",
				"FieldNameEn" => "Remarks",
				"isMultiLang" => "Y",
				"SampleDataChi"=> "備註",
				"SampleData"=> "Remarks",
				"FieldType"=> "TEXT",
				"FixedItem" => "N",
				"FieldStatus" => "normal",
				"showInGrid" => "Y",
				"isUsed" => "Y",
				"Priority" => "7"
			)
		);
		
		$this->parent->FormTemplateObj->setCoreData($this->parent->data);
		$this->parent->data["formTplInfo"] = $this->parent->FormTemplateObj->initTemplate($this->parent->data["template"], $predefinedArr);
		if ($this->parent->data["formTplInfo"] == NULL) {
			echo "Template Init Error";
			exit;
		}
		$this->parent->data["yearArr"] = $this->parent->FormTemplateObj->getAcademicYear();
		$this->parent->data["selectedYear"] = isset($this->parent->data["postData"]["selectedYear"]) ? $this->parent->data["postData"]["selectedYear"] : "";
		$this->parent->data["html_YearPanel"] = $this->parent->YearObj->getYearPanel($this->parent->data["yearArr"], $this->parent->data["lang"], $this->parent->data["section"], $this->parent->data["selectedYear"]);
	}
}
?>