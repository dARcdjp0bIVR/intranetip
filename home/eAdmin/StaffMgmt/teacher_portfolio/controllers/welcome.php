<?php
if (!defined('TEACHER_PORTFOLIO')) die('Page not found');
class Welcome extends TeacherPortfolio {
	public function __construct($parent) {
		parent::__construct($parent);
	}

	public function index() {
		$this->parent->data["allTeachers"] = $this->parent->UserObj->getAllTeacher();
		if ($this->parent->isAdmin() || ($this->parent->isViewer() && $this->parent->data["userInfo"]["info"]["SelectedUserID"] == 0)) {
			
		} else {
			$this->parent->data["profile"] = $this->parent->UserObj->getProfile($this->parent->data["userInfo"]);
			if (!(count($this->parent->data["profile"]) > 0)) {
				No_Access_Right_Pop_Up();
				exit;
			}
		}
		$this->parent->tpl->view("common/header", $this->parent->data);
		$this->parent->tpl->view("welcome", $this->parent->data);
		$this->parent->tpl->view("common/footer", $this->parent->data);
	}
}
?>