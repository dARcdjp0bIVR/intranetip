<?php
/*
 *  Using: 
 *  
 *  2019-02-25 Cameron
 *      - use lang file for valid/invalid/duplicate record in checkcsv()
 *
 *  2019-01-16 Cameron
 *      - don't apply stripslashes to $jsonObj->decode content in importnow() for php version < 5.4 or not set get_magic_quotes_gpc [case #U155333]
 *      
 *  2018-12-10 Cameron
 *      - modify checkcsv() to show error message if last column of the import file contain non-empty character (e.g. tab)
 */

if (!defined('TEACHER_PORTFOLIO')) die('Page not found');
class FormTemplateController extends TeacherPortfolio {
	public function __construct($parent) {
		parent::__construct($parent);
		$this->templateFolder = "template";
		$this->parent->data["settings_menu"] = array(
			"settings" => "Import",
			// "format_settings" => "Format_Settings"
		);
		$this->parent->data["childType"] = "Expand";
	}
	
	public function index() {
		if ($this->parent->configs["isDev"]) {
			$this->parent->data["debugMsg"] = $this->parent->getMemoryInfo();
		}
		$this->parent->tpl->view($this->templateFolder . "/list", $this->parent->data);
		exit;
	}
	
	public function listjson() {
		if ($this->parent->data["TemplateType"] == "GRID") {
			$jsonData = $this->parent->FormTemplateObj->getGridDataTableArray($this->parent->data["userInfo"]["info"]["SelectedUserID"], $this->parent->data["postData"], "GROUP");
		} else {
			$jsonData = $this->parent->FormTemplateObj->getDataTableArray($this->parent->data["userInfo"]["info"]["SelectedUserID"], $this->parent->data["postData"]);
		}
		$jsonObj = new JSON_obj();
		$jsonData["debugInfo"] = $this->parent->getMemoryInfo(false);
		echo $jsonObj->encode($jsonData);
	}
	
	public function childlistjson() {
		$jsonData = $this->parent->FormTemplateObj->getGridDataTableArray($this->parent->data["userInfo"]["info"]["SelectedUserID"], $this->parent->data["postData"]);
		$jsonObj = new JSON_obj();
		$jsonData["debugInfo"] = $this->parent->getMemoryInfo(false);
		echo $jsonObj->encode($jsonData);
	}
	
	public function childlist() {
		if ($this->parent->configs["isDev"]) {
			$this->parent->data["debugMsg"] = $this->parent->getMemoryInfo();
		}
		$this->parent->data["searchArr"] = "NoData";
		if (!empty($this->parent->data["getData"]["s"])) {
			$this->parent->data["searchArr"] = $this->parent->data["getData"]["s"];
		}
		// $this->parent->data["recData"] = $this->parent->FormTemplateObj->getDataTableArrayByParent($this->parent->data["searchArr"]);
		if ($this->parent->data["childType"] == "Modal") {
			$this->parent->data["loadbydt"] = "Y";
			$this->parent->tpl->view("common/header_child", $this->parent->data);
		} else {
			$this->parent->data["loadbydt"] = "N";
			if($this->parent->data['section']=='cpd'){
				$searchData = array(
					"columns" => array(
									array(
										"data" => "StartDate"
									)						
					),
					"order" => array(
									array(
										"column" => 0,
										"dir" => "asc"
									)
					),
					"s" => $this->parent->data["searchArr"]
				);
			}else{
				$searchData = array(
					"columns" => array(
									array(
										"data" => "AcademicYear"
									)						
					),
					"order" => array(
									array(
										"column" => 0,
										"dir" => "desc"
									)
					),
					"s" => $this->parent->data["searchArr"]
				);
			}
			$this->parent->data["recData"] = $this->parent->FormTemplateObj->getGridDataTableArray($this->parent->data["userInfo"]["info"]["SelectedUserID"], $searchData);
		}
		
		$this->parent->tpl->view($this->templateFolder . "/childlist", $this->parent->data);
		if ($this->parent->data["childType"] == "Modal") {
			$this->parent->tpl->view("common/footer", $this->parent->data);
		}
	}
	
	public function dfile() {
		$this->parent->data["currMethod"] = __FUNCTION__;
		$sampleData = $this->parent->FormTemplateObj->getSampleData();
		if (count($sampleData) > 0) {
			$this->parent->load_model('CSVObj');
			$this->parent->CSVObj->outputData($sampleData, $this->parent->data["section"] . "_" . $this->parent->data["getData"]["flid"]);
		} else {
			header($_SERVER['SERVER_PROTOCOL'] . ' 500 Internal Server Error', true, 500);
			exit;
		}
	}
	
	public function printall() {
		$this->parent->data["currMethod"] = __FUNCTION__;
		$this->parent->data["jsonData"] = $this->parent->FormTemplateObj->getAllData($this->parent->data["userInfo"]["info"]["SelectedUserID"]);
		if (count($this->parent->data["jsonData"]) > 0) {
			$total = count($this->parent->data["jsonData"]) - 1;
			if ($total > 0) {
				$logDetail = "Print Data: ";
				$logDetail .= "<small>Total " . $total . " Record(s)</small>";
				$this->parent->load_model('LogObj');
				$this->parent->LogObj->addRecord($this->parent->userInfo["info"]["UserID"],  str_replace("_", " ", strtoupper($this->parent->data["section"])), $logDetail);
			}
		}
		$this->parent->tpl->view($this->templateFolder . "/printall", $this->parent->data);
	}
	
	public function exportrecord() {
		$this->parent->data["currMethod"] = __FUNCTION__;
		$this->parent->load_model('CSVObj');
		$this->parent->FormTemplateObj->exportAllData($this->parent->CSVObj, $this->parent->data["userInfo"]["info"]["SelectedUserID"]);
	}
	
	public function settings() {
		if (!($this->parent->isAdmin() || $this->parent->isOwner())) {
			No_Access_Right_Pop_Up();
			exit;
		}
		$this->parent->data["fileLimit"] = "1";
		$this->parent->data["currMethod"] = __FUNCTION__;
		if ($this->parent->configs["isDev"]) {
			$this->parent->data["debugMsg"] = $this->parent->getMemoryInfo();
		}
		$this->parent->tpl->view($this->templateFolder . "/settings", $this->parent->data);
	}

	public function format_settings() {
		if (!($this->parent->isAdmin() || $this->parent->isOwner())) {
			No_Access_Right_Pop_Up();
			exit;
		}
		$this->parent->data["currMethod"] = __FUNCTION__;
		if ($this->parent->configs["isDev"]) {
			$this->parent->data["debugMsg"] = $this->parent->getMemoryInfo();
		}
		$this->parent->tpl->view($this->templateFolder . "/format_settings", $this->parent->data);
	}
	
	public function uploadfile() {
		$this->parent->load_model('FileObj');
		$this->parent->FileObj->setCoreKey($this->parent->coreKey);
		$this->parent->FileObj->setPathInfo($this->parent->pathInfo);
		$this->parent->FileObj->setCoreData($this->parent->data);
		$this->parent->FileObj->setTPModuleConfig($this->parent->getTMConfig());
		$this->parent->FileObj->setConfig($this->parent->getCustomConfig());
		$this->parent->FileObj->importCSVFileHandler($this->parent->data["section"]);
	}
	
	public function checkcsv() {
	    global $Lang;
		$this->parent->load_model('FileObj');
		$this->parent->load_model('CSVObj');
		$this->parent->load_model('UserObj');
		$this->parent->FileObj->setCoreKey($this->parent->coreKey);
		$this->parent->FileObj->setPathInfo($this->parent->pathInfo);
		$this->parent->FileObj->setCoreData($this->parent->data);
		$this->parent->FileObj->setTPModuleConfig($this->parent->getTMConfig());
		$this->parent->FileObj->setConfig($this->parent->getCustomConfig());
		$result = $this->parent->FileObj->validImportedCSV($this->parent->CSVObj, $this->parent->data["section"]);
		if (count($result["csvData"]) > 0) {
			$csvHeader = $this->parent->data["formTplInfo"]["csvHeader"];
			if($this->parent->data['userInfo']["customization"]['useEDBCPD'] && $this->parent->data["section"]=="cpd"){
				$csvHeader[6] = "CPD Hour(s)";
			}
			$imported_header = $result["csvDataHead"];
			if (count($imported_header) > 0) {
				foreach ($imported_header as $kk => $vv) {
					$imported_header[$kk] = str_replace("[ref]", "", $vv);
				}
			}
			$diff_result = array_diff($imported_header, $csvHeader);
			$diff2_result = array_diff($csvHeader, $imported_header);
			$diffArr = array_unique(array_merge($diff_result, $diff2_result));
			$strHTML = "";
			if (count($diffArr) > 0) {
				foreach ($csvHeader as $kk => $val) {
					if (!in_array($val, $diffArr)) {
						$strHTML .= "<div class='text-success'><span class='glyphicon glyphicon-ok'></span> " . $val . "</div>";
					} else {
						$strHTML .= "<div class='text-danger'><span class='glyphicon glyphicon-remove'></span> " . $val . " --> Missing Column</div>";
					}
				}
				foreach ($diffArr as $kk => $val) {
					if (!in_array($val, $csvHeader)) {
					    if (is_numeric($val)) {
					        $strHTML .= "<div class='text-danger'><span class='glyphicon glyphicon-remove'></span> " . $this->parent->data["lang"]['TeacherPortfolio']['importError']['column'] . numberToLetter($val, true) . " --> ".$this->parent->data["lang"]['TeacherPortfolio']['importError']['extraEmptyColumn']."</div>";
					    }
					    else {
    						$strHTML .= "<div class='text-danger'><span class='glyphicon glyphicon-remove'></span> " . $val . " --> Invalid Column</div>";
					    }
					}
				}
			} else {
				foreach ($csvHeader as $kk => $val) {
					$strHTML .= "<div class='text-success'><span class='glyphicon glyphicon-ok'></span> " . $val . "</div>";
				}
			}
			$fieldsArr = array();
			if (count($diffArr) > 0) {
				$result["status"] = "error";
			} else {
				$i = 0;
				foreach ($this->parent->data["formTplInfo"]["childs"] as $kk => $vv) {
					if($this->parent->data["section"]=="cpd" && $this->parent->data["userInfo"]["customization"]["ltmpsStyle"]==true && $vv["FieldLabel"]=="EventType"){
						$vv["FieldStatus"] = "normal";
					}
					if ($vv["isMultiLang"] == "Y") {
						$fieldsArr[$i] = $vv;
						$fieldsArr[$i]["Label"] = $fieldsArr[$i]["FieldLabel"] . "_chi";
						$i++;
						$fieldsArr[$i] = $vv;
						$fieldsArr[$i]["Label"] = $fieldsArr[$i]["FieldLabel"] . "_eng";
					} else {
						$fieldsArr[$i] = $vv;
						$fieldsArr[$i]["Label"] = $fieldsArr[$i]["FieldLabel"];
					}
					if ($fieldsArr[$i]["FieldLabel"] == "UserID") {
						$UserIDIndex = $i;
					}
					
					$i++;
				}
				if (count($csvHeader) > 0) {
					$result["status"] = "success";
					$csvData = $result["csvData"];
					$newCSV = array();
					$userArr = array();
					if (isset($this->parent->data["formTplInfo"]["childs"]["UserID"]) && isset($UserIDIndex)) {
						// $UserID_FieldID = $this->parent->data["formTplInfo"]["childs"]["UserID"]["FieldID"];
						if (count($csvData) > 0) {
							foreach ($csvData as $kk => $rowData) {
								$tmpData = array_values($rowData);
								$userArr[$tmpData[$UserIDIndex]] = 0;
							}
						}
						if (count($userArr) > 0) {
							/**************************************************/
							// Customization
							/**************************************************/
							if ($this->parent->data["userInfo"]["Role"]["isOwnerMode"] && $this->parent->data["section"] == "cpd" && $this->parent->data["userInfo"]["customization"]["allowImportCPDByIndividualTeacher"])
							{
								$ownerInfo = $this->parent->UserObj->getInfoByID($this->parent->data["userInfo"]["info"]["UserID"]);
								if (count($ownerInfo) > 0) {
									$userArr = array();
									$userArr[$ownerInfo[$this->parent->data["userInfo"]["info"]["UserID"]]["UserLogin"]] = $ownerInfo[$this->parent->data["userInfo"]["info"]["UserID"]]["UserID"];
								}
							} else {
								$userArr = $this->parent->UserObj->getUserIDByNames($userArr);
							}
						}
					}
					$newCSV = $this->parent->FormTemplateObj->validImportData($userArr, $csvData, $fieldsArr);
					$totalInfo = "<span class='label label-info'>Total Record: " . count($newCSV) . "</span>";
					$tableHTML = "<table id='csvData' class='table table-hover'>";
					$tableHTML .= "<thead>";
					$tableHTML .= "<tr>";
					$tableHTML .= "<th>Line No.</th>";
					foreach ($csvHeader as $key => $val) {
						if ($fieldsArr[$key]["FieldStatus"] == "required") {
							$tableHTML .= "<th>" . $val . " <i class='text-danger fa fa-asterisk fa-fw'></i></th>";
						} else if ($fieldsArr[$key]["FieldStatus"] == "reference") {
							$tableHTML .= "<th>" . $val . " <i class='text-danger fa fa-chevron-up'></i></th>";
						} else {
							$tableHTML .= "<th>" . $val . "</th>";
						}
					}
					// $tableHTML .= "<th>&nbsp;</th>";
					$tableHTML .= "</tr>";
					$tableHTML .= "</thead>";
					$tableHTML .= "<tbody>";
					$errorRor = 0;
					$duplicate = 0;
					if (count($newCSV) > 0) {
						$rowNum = 0;
						foreach ($newCSV as $kk => $rowData) {
							if (count($rowData) > 0) {
								if ($rowData["_cust_"]["valid"] == "Y") {
									if ($rowData["_cust_"]["md5_norec"] == 1) {
										$tableHTML .= "<tr class='valid text-info' id='row_" . $rowNum . "'>";
									} else {
										$tableHTML .= "<tr class='duplicate text-warning' id='row_" . $rowNum . "'>";
									}
								} else {
									$tableHTML .= "<tr class='invalid text-danger' id='row_" . $rowNum . "'>";
								}
								$line = $kk + 2;
								$tableHTML .= "<td>" . $line . "</td>";
								$hasError = false;
								$i = 0;
								foreach ($rowData as $colid => $colData) {
									$tdData = $colData;
									if($colid == "CPDMode"){
										if($colData==0){
											$tdData = $this->parent->data["lang"]["TeacherPortfolio"]["edit_cpd_mode_systematic_scheme"];
										}else if($colData==1){
											$tdData = $this->parent->data["lang"]["TeacherPortfolio"]["edit_cpd_mode_others"];
										}
									}
									if($colid == "CPDDomain"){
										$tdData = "";
										$domain = explode(",",$colData);
										foreach($domain as $m=>$d){
											$tdData .= ($d==1)?$Lang['TeacherPortfolio']['edit_cpd_domain'][$m]."<br>":"";
										}												
									}
									if($colid == "SubjectRelated"){
										$tdData = "";
										$domain = explode(",",$colData);
										foreach($domain as $m=>$d){
											$tdData .= ($d==1)?$Lang['TeacherPortfolio']['edit_cpd_subject'][$m]."<br>":"";
										}
									}
									if ($colid != "_cust_") {
										if ($rowData["_cust_"]["valid"] == "Y") {
											$tableHTML .= "<td>" . $tdData . "</td>";
										} else {
											$hasError = true;
											if (in_array($i, $rowData["_cust_"]["error"])) {
												$tooltip = "";
												if(isset($rowData["_cust_"]["errorMsg"]) && $rowData["_cust_"]["errorMsg"] != ""){
													$tooltip = ' title="'.$rowData["_cust_"]["errorMsg"][array_search($i, $rowData["_cust_"]["error"])].'"';
												}
												$tableHTML .= '<td>';
												$tableHTML .= '<span class="text-danger glyphicon glyphicon-warning-sign" '.$tooltip.'></span> '; 
												$tableHTML .= $tdData;
												$tableHTML .= '</td>';
											} else {
												$tableHTML .= "<td>" . $tdData . "</td>";
											}
										}
									}
									$i++;
								}
								/*
								if ($hasError) {
									unset($newCSV[$kk]);
									$tableHTML .= '<td data-tooltip="' . $this->parent->data["lang"]["TeacherPortfolio"]["settings_rowIgnore"] . '"><span class="text-danger glyphicon glyphicon-comment"></td>';
								} else {
									$tableHTML .= "<td><a href='#' class='rowDel'><span class='glyphicon glyphicon-remove'></span></a></td>";
								}
								*/
								$tableHTML .= "</tr>";
								if ($rowData["_cust_"]["valid"] == "Y") {
									if ($rowData["_cust_"]["md5_norec"] > 1) {
										unset($newCSV[$kk]);
									}
								} else {
									unset($newCSV[$kk]);
								}
							} else {
								unset($newCSV[$kk]);
							}
							$rowNum++;
						}
					}
					$tableHTML .= "</tbody>";
					$tableHTML .= "</table>";
					$tableHTML .= "<small>" . $this->parent->data["lang"]['TeacherPortfolio']["settings_requiredfield"] . "<br>";
					$tableHTML .= $this->parent->data["lang"]['TeacherPortfolio']["settings_referencefield"] . "</small>";
					$result["csvData"] = $newCSV;
					$totalInfo = "";
					/*
					$totalInfo = "Total Record(s): <span class='label label-default' id='tpip_totalrecord'>0</span>, ";
					$totalInfo .= "Total Record(s) for import: <span class='label label-primary' id='tpip_totalimport'>0</span>, ";
					$totalInfo .= "Total Valid Record(s): <span class='label label-info' id='tpip_totalvalid'>0</span>, ";
					$totalInfo .= "Total Duplicate Record(s): <span class='label label-warning' id='tpip_totaldup'>0</span>, ";
					$totalInfo .= "Invalid Record(s): <span class='label label-danger' id='tpip_totalerror'>0</span><br><br>";
					*/
					// $totalInfo .= "<a href='#' class='btn btn-xs csvImpHS btn-default btnShowAll'>Show all<a> ";
					// $totalInfo .= "<a href='#' class='btn btn-xs csvImpHS btn-primary btnShowImpt'>Show record(s) for import<a> ";
					$totalInfo .= "<a href='#' class='btn btn-xs csvImpHS btn-info btnShowVaild'>".$Lang['TeacherPortfolio']['import']['valid_record']." <span class='badge' id='tpip_totalvalid'>0</span><a> ";
					$totalInfo .= "<a href='#' class='btn btn-xs csvImpHS btn-danger btnShowError'>".$Lang['TeacherPortfolio']['import']['invalid_record']." <span class='badge' id='tpip_totalerror'>0</span><a> ";
					$totalInfo .= "<a href='#' class='btn btn-xs csvImpHS btn-warning btnShowDup'>".$Lang['TeacherPortfolio']['import']['duplicate_record']." <span class='badge' id='tpip_totaldup'>0</span><a>";
					
					// $totalInfo .= "<a href='#' class='btn btn-xs csvImpHS btn-danger btnRemoveDup pull-right'>Remove all duplicate record(s)<a>";
					
					$result["tableHTML"] = $totalInfo . "<br><br>" . $tableHTML;
					
					unset($csvData);
					unset($newCSV);
				} else {
					$result["status"] = "error";
				}
			}
			$result["statusmsg"] = $strHTML;
		}
		
		$jsonObj = new JSON_obj();
		echo $jsonObj->encode($result);
		exit;
	}
	
	public function importnow() {
		if (!($this->parent->isAdmin() || $this->parent->isOwner())) {
			No_Access_Right_Pop_Up();
			exit;
		}
		$jsonObj = new JSON_obj();
		$output = array(
					"jsonrpc" => "2.0",
					"errorCode" => "200",
					"Msg" => $this->parent->data["lang"]["TeacherPortfolio"]["settings_importFailed"],
					"csvData" => array()
				);
		if(isset($this->parent->data["postData"]["csvData"])) {
		    if ( !get_magic_quotes_gpc() || (phpversion_compare('5.4') != 'ELDER')) {
		        $data = $jsonObj->decode(stripslashes($this->parent->data["postData"]["csvData"]));
		    }
		    else {
			    $data = $jsonObj->decode($this->parent->data["postData"]["csvData"]);
		    }
			$this->parent->load_model('FileObj');
			$this->parent->load_model('CSVObj');
			$this->parent->load_model('UserObj');
			$this->parent->FileObj->setCoreKey($this->parent->coreKey);
			$this->parent->FileObj->setPathInfo($this->parent->pathInfo);
			$this->parent->FileObj->setCoreData($this->parent->data);
			$this->parent->FileObj->setTPModuleConfig($this->parent->getTMConfig());
			$this->parent->FileObj->setConfig($this->parent->getCustomConfig());
			$result = $this->parent->FileObj->validImportedCSV($this->parent->CSVObj, $this->parent->data["section"]);
			if (count($result["csvData"]) > 0) {
				$csvHeader = $this->parent->data["formTplInfo"]["csvHeader"];
				if($this->parent->data['userInfo']["customization"]['useEDBCPD'] && $this->parent->data["section"]=="cpd"){
					$csvHeader[6] = "CPD Hour(s)";
				}
				$imported_header = $result["csvDataHead"];
				if (count($imported_header) > 0) {
					foreach ($imported_header as $kk => $vv) {
						$imported_header[$kk] = str_replace("[ref]", "", $vv);
					}
				}
				$diff_result = array_diff($imported_header, $csvHeader);
				$diff2_result = array_diff($csvHeader, $imported_header);
				$diffArr = array_unique(array_merge($diff_result, $diff2_result));
				$csvData = $result["csvData"];
				if (!(count($diffArr) > 0)) {
					$fieldsArr = array();
					$i = 0;
					foreach ($this->parent->data["formTplInfo"]["childs"] as $kk => $vv) {
						if ($vv["isMultiLang"] == "Y") {
							$fieldsArr[$i] = $vv;
							$fieldsArr[$i]["Label"] = $fieldsArr[$i]["FieldLabel"] . "_eng";
							$i++;
							$fieldsArr[$i] = $vv;
							$fieldsArr[$i]["Label"] = $fieldsArr[$i]["FieldLabel"] . "_chi";
						} else {
							$fieldsArr[$i] = $vv;
							$fieldsArr[$i]["Label"] = $fieldsArr[$i]["FieldLabel"];
						}
						if ($fieldsArr[$i]["FieldLabel"] == "UserID") {
							$UserIDIndex = $i;
						}
						$i++;
					}
					$userArr = array();
					if (isset($this->parent->data["formTplInfo"]["childs"]["UserID"]) && isset($UserIDIndex)) {
						// $UserID_FieldID = $this->parent->data["formTplInfo"]["childs"]["UserID"]["FieldID"];
						if (count($csvData) > 0) {
							foreach ($csvData as $kk => $rowData) {
								$tmpData = array_values($rowData);
								$userArr[$tmpData[$UserIDIndex]] = 0;
							}
						}
						if (count($userArr) > 0) {
							$userArr = $this->parent->UserObj->getUserIDByNames($userArr);
						}
					}
					$newCSV = $this->parent->FormTemplateObj->validImportData($userArr, $csvData, $fieldsArr);
				}
				unset($csvData);
				if (count($newCSV) > 0) {
					$totalUpload = count($newCSV);
					$selectedData = array();
					foreach ($data as $selectedIndex => $postRowData) {
						if (isset($newCSV[$selectedIndex])) {
							$selectedData[$selectedIndex] = $newCSV[$selectedIndex];
						}
					}
					unset($postRowData);
					unset($data);
					unset($newCSV);
					$totalImported = $this->parent->FormTemplateObj->dataImport($this->parent->data["userInfo"]["info"]["UserID"], $userArr, $selectedData);
					if ($totalImported > 0) {
						$msg = "";
						$output = array(
							"jsonrpc" => "2.0",
							"Msg" => $this->parent->data["lang"]["TeacherPortfolio"]["settings_importSuccess"]
						);
					}
				}
			}
		}
		echo $jsonObj->encode($output);
		exit;
	}
	
	public function deleterecord() {
		if (!($this->parent->isAdmin() || $this->parent->isOwner())) {
			No_Access_Right_Pop_Up();
			exit;
		}
		$this->parent->data["currAction"] = "delete";
		if (isset($this->parent->data["postData"]["recid"])) {
			$recid = $this->parent->data["postData"]["recid"];
		}
		$data = array(
			"result" => "failed"
		);
		if ($recid > 0) {
			if ($this->parent->FormTemplateObj->deleteRecord($recid, $this->parent->data["userInfo"]["info"]["UserID"])) {
				$data["result"] = "success";
			}
		}
		if ($this->parent->configs["isDev"]) {
			$this->parent->data["debugMsg"] = $this->parent->getMemoryInfo();
		}
		$jsonObj = new JSON_obj();
		echo $jsonObj->encode($data);
	}
	
	public function updatesort() {
		if (!($this->parent->isAdmin() || $this->parent->isOwner())) {
			No_Access_Right_Pop_Up();
			exit;
		}
		$this->parent->data["currAction"] = "delete";
		if (isset($this->parent->data["postData"]["recid"])) {
			$recid = $this->parent->data["postData"]["recid"];
		}
		$data = array(
			"result" => "failed"
		);
		if ($recid > 0) {
			if ($this->parent->FormTemplateObj->updateSortRecords($recid)) {
				$data["result"] = "success";
			}
		}
		if ($this->parent->configs["isDev"]) {
			$this->parent->data["debugMsg"] = $this->parent->getMemoryInfo();
		}
		$jsonObj = new JSON_obj();
		echo $jsonObj->encode($data);
	}
	
	public function codelist() {
		if (!empty($this->parent->data["getData"]["s"])) {
			$this->parent->data["searchArr"] = base64_decode($this->parent->data["getData"]["s"]);
		}
		$this->parent->tpl->view("common/header_child", $this->parent->data);
		$this->parent->tpl->view($this->parent->data["section"] . "/codelist", $this->parent->data);
		$this->parent->tpl->view("common/footer", $this->parent->data);
	}
}