<?php
if (!defined('TEACHER_PORTFOLIO')) die('Page not found');
class FileController extends TeacherPortfolio {
	public function __construct($parent) {
		parent::__construct($parent);
	}

	public function index() {
		$this->parent->FileObj->checkFolder($this->parent->data["section"]);
		if ($this->parent->configs["isDev"]) {
			$this->parent->data["debugMsg"] = $this->parent->getMemoryInfo();
		}
		$this->parent->tpl->view($this->parent->data["section"] . "/list", $this->parent->data);
	}
	
	public function listjson() {
		if (count($this->parent->data["postData"]) > 0) {
			$currDate = date("Ymd");
			if ($this->parent->data["userInfo"]["Role"]["isAdminMode"] && $this->parent->data["userInfo"]["Role"]["hasAdminRight"]) {
				$jsonData = $this->parent->myOBJ->getDataTableArray(0, $this->parent->data["postData"], TRUE);
			} else {
				$jsonData = $this->parent->myOBJ->getDataTableArray($this->parent->data["userInfo"]["info"]["SelectedUserID"], $this->parent->data["postData"]);
			}
			if (count($jsonData["infonum"]) > 0) {
				$fileData = $this->parent->FileObj->getFileListByInfoIDs($jsonData["infonum"]);
				unset($jsonData["infonum"]);
				if (count($fileData) > 0) {
					foreach ($fileData as $infoId => $fileArr) {
						if (count($fileArr) > 0) {
							if (isset($jsonData["org_data"][$infoId])) {
								if (!isset($jsonData["org_data"][$infoId]["infoName"]) || $this->parent->data["section"] == "performance") {
									$jsonData["org_data"][$infoId]["infoName"] = "";
								}
								
								$jsonData["org_data"][$infoId]["infoName"] .= "<ul class='attachment'>";
								foreach ($fileArr as $fileId => $fileInfo) {
									if ($jsonData["org_data"][$infoId]["InfoPermission"] == "N" && !$this->parent->data["userInfo"]["Role"]["isAdminMode"]) {
										$jsonData["org_data"][$infoId]["infoName"] .= '<li>' . $fileInfo["OrgFileName"] . '</li>';
									} else {
										$jsonData["org_data"][$infoId]["infoName"] .= '<li><a href="#" class="btnFDAjax" rel="' . $this->parent->data["section"] . '.dfile&flid=' . $fileId . '&d=' . $currDate . '&skey=' . md5($fileId . '_' . $fileInfo["FileSize"] . '_' . $currDate) .'">' . $fileInfo["OrgFileName"] . '</a></li>';
									}
								}
								$jsonData["org_data"][$infoId]["infoName"] .= "<li>";
							}
						}
					}
				}
			}
			$jsonData["data"] = array_values($jsonData["org_data"]);
			unset($jsonData["org_data"]);
			$jsonData["debugInfo"] = $this->parent->getMemoryInfo(false);
			$jsonObj = new JSON_obj();
			echo $jsonObj->encode($jsonData);
		} else {
			echo "Page not found";
		}
	}
	
	public function addrecord() {
		if (! ($this->parent->isAdmin() || $this->parent->isOwner() )) {
			No_Access_Right_Pop_Up();
			exit;
		}
		$this->parent->data["allTeachers"] = $this->parent->UserObj->getAllTeacher();
		$this->parent->data["currDate"] = date("Ymd");
		$this->parent->FileObj->checkFolder($this->parent->data["section"]);
		$this->parent->data["currAction"] = "add";
		if ($this->parent->configs["isDev"]) {
			$this->parent->data["debugMsg"] = $this->parent->getMemoryInfo();
		}
		$this->parent->tpl->view($this->parent->data["section"] . "/edit", $this->parent->data);
	}

	public function editrecord() {
		if (! ($this->parent->isAdmin() || $this->parent->isOwner() )) {
			No_Access_Right_Pop_Up();
			exit;
		}
		$this->parent->data["allTeachers"] = $this->parent->UserObj->getAllTeacher();
		$this->parent->data["currAction"] = "edit";
		$this->parent->data["currDate"] = date("Ymd");
		$this->parent->data["infoRecord"] = $this->parent->myOBJ->getRecordByID($this->parent->data["postData"]["InfoID"]);
		if (!$this->parent->data["infoRecord"]) {
			$this->parent->data["infoRecord"] = "no record";
		} else {
			$this->parent->data["infoRecordFiles"] = $this->parent->FileObj->getFilesByInfoID($this->parent->data["postData"]["InfoID"], $this->parent->data["userInfo"]["info"]["SelectedUserID"]);
		}
		if ($this->parent->configs["isDev"]) {
			$this->parent->data["debugMsg"] = $this->parent->getMemoryInfo();
		}
		$this->parent->tpl->view($this->parent->data["section"] . "/edit", $this->parent->data);
	}
	
	public function deleterecord() {
		if (! ($this->parent->isAdmin() || $this->parent->isOwner() )) {
			No_Access_Right_Pop_Up();
			exit;
		}
		$this->parent->data["currAction"] = "delete";
		if ($this->parent->configs["isDev"]) {
			$this->parent->data["debugMsg"] = $this->parent->getMemoryInfo();
		}
		if (isset($this->parent->data["postData"]["recid"])) {
			$recid = $this->parent->data["postData"]["recid"];
		} else if (isset($this->parent->data["postData"]["InfoID"])) {
			$recid = $this->parent->data["postData"]["InfoID"];
		}
		$data = array(
			"result" => "failed"
		);
		if (isset($recid) && $recid > 0) {
			if ($this->parent->myOBJ->deleteRecord($recid, $this->parent->data["userInfo"]["info"]["UserID"])) {
				$this->parent->FileObj->removeAllFile($this->parent->data["section"], $recid, $this->parent->data["userInfo"]["info"]["UserID"]);
				$data["result"] = "success";
			}
		}
		$jsonObj = new JSON_obj();
		echo $jsonObj->encode($data);
	}
	
	public function process() {
		if (! ($this->parent->isAdmin() || $this->parent->isOwner() )) {
			No_Access_Right_Pop_Up();
			exit;
		}
		$dataMapping = $this->parent->myOBJ->getMappingInfo();
		if ($this->parent->data["section"] != "others") {
			$this->parent->data["postData"]["chiInfoType"] = $this->parent->data["postData"]["academicYear"];
			$this->parent->data["postData"]["engInfoType"] = $this->parent->data["postData"]["academicYear"];
		}
		if (isset($this->parent->data["postData"]["selectedUser"])) {
			$selectedID = explode("_", $this->parent->data["postData"]["selectedUser"]);
			$this->parent->data["postData"]["selectedUserID"] = $selectedID[0];
			unset($this->parent->data["postData"]["selectedUser"]);
		}
		if ($this->parent->data["section"] == "performance") {
			unset($this->parent->data["postData"]["academicYear"]);
			if (isset($this->parent->data["postData"]["files"][0])) {
				$file = $this->parent->FileObj->getFileByID($this->parent->data["postData"]["files"][0], $this->parent->data["userInfo"]["info"]["SelectedUserID"]);
				$this->parent->data["postData"]["chiInfoName"] = $file["OrgFileName"];
				$this->parent->data["postData"]["engInfoName"] = $file["OrgFileName"];
			}
		}
		$processData = $this->mapConvert($this->parent->data["postData"], $dataMapping);
		switch ($this->parent->data["postData"]["action"]) {
			case "insertrecord":
				$myaction = "INSERT";
				$id = null;
				break;
			case "updaterecord":
				$myaction = "UPDATE";
				$id = $this->parent->data["postData"]["recid"];
				break;
		}
		$result = $this->parent->myOBJ->dataHandler($this->parent->data["userInfo"], $processData, $myaction, $id);
		if ($result["data"]["id"] > 0) {
			if (isset($this->parent->data["postData"]["files"]) && count($this->parent->data["postData"]["files"]) > 0) {
				$this->parent->FileObj->dataHandler($this->parent->data["section"], $result["data"]["id"], $this->parent->data["postData"]["files"], $this->parent->data["userInfo"]["info"]["SelectedUserID"]);
			} else {
				$this->parent->FileObj->dataHandler($this->parent->data["section"], $result["data"]["id"], null, $this->parent->data["userInfo"]["info"]["SelectedUserID"]);
			}
		}
		$jsonObj = new JSON_obj();
		echo $jsonObj->encode($result);
	}
	
	public function uploadfile() {
		if (! ($this->parent->isAdmin() || $this->parent->isOwner() )) {
			No_Access_Right_Pop_Up();
			exit;
		}
		$this->parent->FileObj->uploadFileHandler($this->parent->data["section"]);
	}
	
	public function dfile() {
		$fileInfo = $this->parent->FileObj->validParam($this->parent->data["getData"], $this->parent->data["userInfo"]["info"]["SelectedUserID"]);
		if ($fileInfo != false) {
			$result = $this->parent->FileObj->filePrepareToUser($this->parent->data["section"], $fileInfo);
			if ($result !== false && filesize($result) > 0) {
				set_time_limit(0);
				header("Pragma: public");
				header("Expires: 0");
				header("Cache-Control: private");
				header("Content-Type: application/stream");
				header("Content-Disposition: attachment;filename=" . $fileInfo["OrgFileName"]);
				header("Content-Length: ". filesize($result));
				readfile($result);
				sleep(1);
				unlink($result);
				exit;
			}
		}
		header('Content-type: text/plain; charset=utf-8');
		echo "File not found";
		exit;
	}
	
	public function exportrecord() {
		$this->parent->data["currMethod"] = __FUNCTION__;
		$this->parent->load_model('CSVObj');
		if ($this->parent->data["userInfo"]["Role"]["isAdminMode"] && $this->parent->data["userInfo"]["Role"]["hasAdminRight"]) {
			$this->parent->myOBJ->exportAllData($this->parent->CSVObj, 0, TRUE);
		} else {
			$this->parent->myOBJ->exportAllData($this->parent->CSVObj, $this->parent->data["userInfo"]["info"]["SelectedUserID"]);
		}
	}
	
	public function printall() {
		$this->parent->data["currMethod"] = __FUNCTION__;
		$this->parent->data["jsonData"] = $this->parent->myOBJ->getAllData($this->parent->data["userInfo"]["info"]["SelectedUserID"]);
		if (count($this->parent->data["jsonData"] ) > 0) {
			$total = count($this->parent->data["jsonData"]) - 1;
			if ($total > 0) {
				$logDetail = "Print Data: ";
				$logDetail .= "<small>Total " . $total . " Record(s)</small>";
				$this->parent->load_model('LogObj');
				$this->parent->LogObj->addRecord($this->parent->userInfo["info"]["UserID"],  str_replace("_", " ", strtoupper($this->parent->data["section"])), $logDetail);
			}
		}
		$this->parent->tpl->view("template/printall", $this->parent->data);
	}
}
?>