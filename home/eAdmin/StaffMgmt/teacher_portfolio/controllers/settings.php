<?php
if (!defined('TEACHER_PORTFOLIO')) die('Page not found');
class Settings extends TeacherPortfolio {
	public function __construct($parent) {
		parent::__construct($parent);
		$this->parent->data["section"] = "settings";
		if (!($this->parent->isAdmin() && $this->parent->hasAdminRight())) {
			No_Access_Right_Pop_Up();
		}
		$this->parent->load_model("LogObj");
	}
	
	public function index() {
		if ($this->parent->configs["isDev"]) {
			$this->parent->data["debugMsg"] = $this->parent->getMemoryInfo();
		}
		$this->parent->tpl->view($this->parent->data["section"] . "/list", $this->parent->data);
	}
	
	public function listjson() {
		$jsonData = $this->parent->UserObj->getDataTableArray($this->parent->data["postData"]);
		$jsonObj = new JSON_obj();
		$jsonData["debugInfo"] = $this->parent->getMemoryInfo(false);
		echo $jsonObj->encode($jsonData);
	}
	
	public function addrecord() {
		if (!($this->parent->isAdmin() || $this->parent->isOwner())) {
			No_Access_Right_Pop_Up();
			exit;
		}
		$this->parent->data["fileLimit"] = "1";
		$this->parent->data["currMethod"] = __FUNCTION__;
		if ($this->parent->configs["isDev"]) {
			$this->parent->data["debugMsg"] = $this->parent->getMemoryInfo();
		}
		$this->parent->tpl->view($this->parent->data["section"] . "/edit", $this->parent->data);
	}
	
	public function ajaxdata() {
		if ($this->parent->isAdmin() || $this->parent->isOwner()) {
			$jsonData = $this->parent->UserObj->getSuggestionUser($this->parent->data["userInfo"]["info"]["UserID"], $this->parent->data["postData"]);
			$jsonObj = new JSON_obj();
			$jsonData["debugInfo"] = $this->parent->getMemoryInfo(false);
			echo $jsonObj->encode($jsonData);
		} else {
			No_Access_Right_Pop_Up();
		}
		exit;
	}
	
	public function process() {
		if ($this->parent->isAdmin() && $this->parent->hasAdminRight()) {
			$this->parent->UserObj->userUpdate($this->parent->data["userInfo"]["info"]["UserID"], $this->parent->data["postData"]);
			$jsonData = array(
				"result" => "Completed"
			);
			$jsonObj = new JSON_obj();
			$jsonData["debugInfo"] = $this->parent->getMemoryInfo(false);
			echo $jsonObj->encode($jsonData);
		} else {
			No_Access_Right_Pop_Up();
			exit;
		}
	}
	
	public function deleterecord() {
		if (!$this->parent->isAdmin()) {
			No_Access_Right_Pop_Up();
			exit;
		}
		$this->parent->data["currAction"] = "delete";
		if (isset($this->parent->data["postData"]["recid"])) {
			$recid = $this->parent->data["postData"]["recid"];
		}
		$data = array(
			"result" => "failed"
		);
		if ($recid > 0) {
			if ($this->parent->UserObj->deleteRecord($this->parent->data["userInfo"]["info"]["UserID"], $recid)) {
				$data["result"] = "success";
			}
		}
		if ($this->parent->configs["isDev"]) {
			$this->parent->data["debugMsg"] = $this->parent->getMemoryInfo();
		}
		$jsonObj = new JSON_obj();
		echo $jsonObj->encode($data);
	}
	
	public function logrecord() {
		if ($this->parent->configs["isDev"]) {
			$this->parent->data["debugMsg"] = $this->parent->getMemoryInfo();
		}
		$this->parent->tpl->view($this->parent->data["section"] . "/log_list", $this->parent->data);
	}
	
	public function log_listjson() {
		$jsonData = $this->parent->LogObj->getDataTableArray($this->parent->data["postData"]);
		$jsonObj = new JSON_obj();
		$jsonData["debugInfo"] = $this->parent->getMemoryInfo(false);
		echo $jsonObj->encode($jsonData);
	}
	
	public function log_exportrecord() {
		$this->parent->data["currMethod"] = __FUNCTION__;
		$this->parent->load_model('CSVObj');
		$this->parent->LogObj->exportAllData($this->parent->CSVObj);
	}
	
	public function log_printall() {
		$this->parent->data["currMethod"] = __FUNCTION__;
		$this->parent->data["jsonData"] = $this->parent->LogObj->getAllData();
		$this->parent->tpl->view("template/printall", $this->parent->data);
	}
	
	public function configs() {
		$this->parent->data["currMethod"] = __FUNCTION__;
		$this->parent->data["jsonData"] = $this->parent->LogObj->getAllData();
		$this->parent->data["tpConfigs"] = $this->parent->userInfo["tpConfigs"];
		$this->parent->tpl->view($this->parent->data["section"] . "/configs", $this->parent->data);
	}
	
	public function configsprocess() {
		if ($this->parent->isAdmin() && $this->parent->hasAdminRight()) {
			$result = $this->parent->saveConfig($this->parent->data["postData"]);
			$jsonObj = new JSON_obj();
			$result["debugInfo"] = $this->parent->getMemoryInfo(false);
			echo $jsonObj->encode($result);
		} else {
			No_Access_Right_Pop_Up();
			exit;
		}
	}
}