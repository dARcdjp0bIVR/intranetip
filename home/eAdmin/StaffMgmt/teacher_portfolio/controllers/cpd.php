<?php
/*
 * Using: 
 * 
 *  2019-02-25 Cameron
 *      - fix: apply stripslashes to data in process() for php version >= 5.4
 *       
 * 	2018-01-15 Paul
 * 		- add more field to fit EDB requirement
 * 
 * 	2017-11-21 Cameron
 * 		- change NoOfSection type from INT to FLOAT [case #A122546]
 * 
 * 	2017-11-08 Cameron
 * 		- change yearArr to use those from db
 * 		- copy addrecord() from performance 
 */
if (!defined('TEACHER_PORTFOLIO')) die('Page not found');
if (!class_exists("FormTemplateController")) include_once(dirname(__FILE__) . "/formtemplate.controller.php");
class Cpd extends FormTemplateController {
	public function __construct($parent) {
		parent::__construct($parent);
		$this->parent->data["section"] = "cpd";
		$this->parent->data["template"] = "CPD";
		$this->parent->load_model('FormTemplateObj');
		$this->mainTable = "INTRANET_TEACHER_PORTFOLIO_FORM_DATA_GRID_CPD";

		$this->userTable = $this->parent->UserObj->defaultUserTable;
		
		$this->yearTable = "ACADEMIC_YEAR";
		$this->primaryKey = "FTDataRowID";
		if($this->parent->data['userInfo']["customization"]['useEDBCPD']){
			if($this->parent->data['userInfo']["customization"]["cpdSubjectTag"] ){
				$this->dataField = array(
					"FormTemplateID", "UserID", "AcademicYear", "StartDate", "EndDate", "NoOfSection", "EventNameEn", "EventNameChi", "EventTypeEn", "EventTypeChi", "RemarksEn", "RemarksChi", "Content", "Organization", "CPDMode", "CPDDomain", "SubjectRelated");
			}else{
				$this->dataField = array(
					"FormTemplateID", "UserID", "AcademicYear", "StartDate", "EndDate", "NoOfSection", "EventNameEn", "EventNameChi", "EventTypeEn", "EventTypeChi", "RemarksEn", "RemarksChi", "Content", "Organization", "CPDMode", "CPDDomain", "BasicLawRelated");	
			}
		}else{
			$this->dataField = array(
				"FormTemplateID", "UserID", "AcademicYear", "StartDate", "EndDate", "NoOfSection", "EventNameEn", "EventNameChi", "EventTypeEn", "EventTypeChi", "RemarksEn", "RemarksChi");
		}
		
		
		/***********************************************************/
		$this->parent->data["TemplateType"] = "GRID";
		$this->parent->data["TemplateGridTable"] = "INTRANET_TEACHER_PORTFOLIO_FORM_DATA_GRID_" . $this->parent->data["template"];
		if($this->parent->data['userInfo']["customization"]['useEDBCPD']){
			if($this->parent->data['userInfo']["customization"]["cpdSubjectTag"] ){
				$this->parent->data["TemplateGridTableStructure"] = array(
					"FormTemplateID" => array( "field" => "FormTemplateID", "data" => "", "output" => false),
					"UserID" => array( "field" => "UserID", "groupBY" => true, "data" => "", "output" => false),
					"AcademicYear" => array( "field" => "AcademicYear", "groupBY" => true, "data" => "", "output" => true),
					"StartDate" => array( "field" => "StartDate", "action" => "MIN", "groupBY" => false, "data" => "", "output" => true),
					"EndDate" => array( "field" => "EndDate", "action" => "MAX", "groupBY" => false, "data" => "", "output" => true),
					"NoOfSection" => array( "field" => "NoOfSection", "groupBY" => false, "action" => "SUM", "data" => "", "output" => true),
					"EventNameEn" => array( "field" => "EventNameEn", "groupBY" => false, "action" => "EMPTY", "data" => "", "output" => true),
					"EventNameChi" => array( "field" => "EventNameChi", "groupBY" => false, "action" => "EMPTY", "data" => "", "output" => true),
					"EventTypeEn" => array( "field" => "EventTypeEn", "groupBY" => false, "action" => "EMPTY", "data" => "", "output" => true),
					"EventTypeChi" => array( "field" => "EventTypeChi", "groupBY" => false, "action" => "EMPTY", "data" => "", "output" => true),
					"RemarksEn" => array( "field" => "RemarksEn", "groupBY" => false, "action" => "EMPTY", "data" => "", "output" => true),
					"RemarksChi" => array( "field" => "RemarksChi", "groupBY" => false, "action" => "EMPTY", "data" => "", "output" => true),
					"Content" => array( "field" => "Content", "groupBY" => false, "action" => "EMPTY", "data" => "", "output" => true),
					"Organization" => array( "field" => "Organization", "groupBY" => false, "action" => "EMPTY", "data" => "", "output" => true),
					"CPDMode" => array( "field" => "CPDMode", "groupBY" => false, "action" => "EMPTY", "data" => "", "output" => true),
					"CPDDomain" => array( "field" => "CPDDomain", "groupBY" => false, "action" => "EMPTY", "data" => "", "output" => true),
					"SubjectRelated" => array( "field" => "SubjectRelated", "groupBY" => false, "action" => "EMPTY", "data" => "", "output" => true),
					"DateInput" => array( "field" => "DateInput", "groupBY" => false, "data" => "__dbnow__", "output" => false),
					"InputBy" => array( "field" => "InputBy", "groupBY" => false, "data" => "", "output" => false),
					"DateModified" => array( "field" => "DateModified", "groupBY" => false, "data" => "__dbnow__", "output" => false),
					"ModifyBy" => array( "field" => "ModifyBy", "groupBY" => false, "data" => "", "output" => false)
				);
			}else{
				$this->parent->data["TemplateGridTableStructure"] = array(
					"FormTemplateID" => array( "field" => "FormTemplateID", "data" => "", "output" => false),
					"UserID" => array( "field" => "UserID", "groupBY" => true, "data" => "", "output" => false),
					"AcademicYear" => array( "field" => "AcademicYear", "groupBY" => true, "data" => "", "output" => true),
					"StartDate" => array( "field" => "StartDate", "action" => "MIN", "groupBY" => false, "data" => "", "output" => true),
					"EndDate" => array( "field" => "EndDate", "action" => "MAX", "groupBY" => false, "data" => "", "output" => true),
					"NoOfSection" => array( "field" => "NoOfSection", "groupBY" => false, "action" => "SUM", "data" => "", "output" => true),
					"EventNameEn" => array( "field" => "EventNameEn", "groupBY" => false, "action" => "EMPTY", "data" => "", "output" => true),
					"EventNameChi" => array( "field" => "EventNameChi", "groupBY" => false, "action" => "EMPTY", "data" => "", "output" => true),
					"EventTypeEn" => array( "field" => "EventTypeEn", "groupBY" => false, "action" => "EMPTY", "data" => "", "output" => true),
					"EventTypeChi" => array( "field" => "EventTypeChi", "groupBY" => false, "action" => "EMPTY", "data" => "", "output" => true),
					"RemarksEn" => array( "field" => "RemarksEn", "groupBY" => false, "action" => "EMPTY", "data" => "", "output" => true),
					"RemarksChi" => array( "field" => "RemarksChi", "groupBY" => false, "action" => "EMPTY", "data" => "", "output" => true),
					"Content" => array( "field" => "Content", "groupBY" => false, "action" => "EMPTY", "data" => "", "output" => true),
					"Organization" => array( "field" => "Organization", "groupBY" => false, "action" => "EMPTY", "data" => "", "output" => true),
					"CPDMode" => array( "field" => "CPDMode", "groupBY" => false, "action" => "EMPTY", "data" => "", "output" => true),
					"CPDDomain" => array( "field" => "CPDDomain", "groupBY" => false, "action" => "EMPTY", "data" => "", "output" => true),
					"BasicLawRelated" => array( "field" => "BasicLawRelated", "groupBY" => false, "action" => "SUM", "data" => "", "output" => true),
					"DateInput" => array( "field" => "DateInput", "groupBY" => false, "data" => "__dbnow__", "output" => false),
					"InputBy" => array( "field" => "InputBy", "groupBY" => false, "data" => "", "output" => false),
					"DateModified" => array( "field" => "DateModified", "groupBY" => false, "data" => "__dbnow__", "output" => false),
					"ModifyBy" => array( "field" => "ModifyBy", "groupBY" => false, "data" => "", "output" => false)
				);
			}
		}else{
			$this->parent->data["TemplateGridTableStructure"] = array(
				"FormTemplateID" => array( "field" => "FormTemplateID", "data" => "", "output" => false),
				"UserID" => array( "field" => "UserID", "groupBY" => true, "data" => "", "output" => false),
				"AcademicYear" => array( "field" => "AcademicYear", "groupBY" => true, "data" => "", "output" => true),
				"StartDate" => array( "field" => "StartDate", "action" => "MIN", "groupBY" => false, "data" => "", "output" => true),
				"EndDate" => array( "field" => "EndDate", "action" => "MAX", "groupBY" => false, "data" => "", "output" => true),
				"NoOfSection" => array( "field" => "NoOfSection", "groupBY" => false, "action" => "SUM", "data" => "", "output" => true),
				"EventNameEn" => array( "field" => "EventNameEn", "groupBY" => false, "action" => "EMPTY", "data" => "", "output" => true),
				"EventNameChi" => array( "field" => "EventNameChi", "groupBY" => false, "action" => "EMPTY", "data" => "", "output" => true),
				"EventTypeEn" => array( "field" => "EventTypeEn", "groupBY" => false, "action" => "EMPTY", "data" => "", "output" => true),
				"EventTypeChi" => array( "field" => "EventTypeChi", "groupBY" => false, "action" => "EMPTY", "data" => "", "output" => true),
				"RemarksEn" => array( "field" => "RemarksEn", "groupBY" => false, "action" => "EMPTY", "data" => "", "output" => true),
				"RemarksChi" => array( "field" => "RemarksChi", "groupBY" => false, "action" => "EMPTY", "data" => "", "output" => true),
				"DateInput" => array( "field" => "DateInput", "groupBY" => false, "data" => "__dbnow__", "output" => false),
				"InputBy" => array( "field" => "InputBy", "groupBY" => false, "data" => "", "output" => false),
				"DateModified" => array( "field" => "DateModified", "groupBY" => false, "data" => "__dbnow__", "output" => false),
				"ModifyBy" => array( "field" => "ModifyBy", "groupBY" => false, "data" => "", "output" => false)
			);
		}	
		
		/***********************************************************/
				
		$predefinedArr = array(
			"UserID" => array(
				"FieldLabel" => "UserID",
				"FieldNameChi" => "內聯網帳號",
				"FieldNameEn" => "Teacher Login ID",
				"isMultiLang" => "N",
				"SampleDataChi"=> "",
				"SampleData"=> "tchr01",
				"FieldType"=> "INT",
				"FixedItem" => "Y",
				"FieldStatus" => "required",
				"showInGrid" => "N",
				"isUsed" => "Y",
				"Priority" => "1"
			),
			"TeacherName" => array(
				"FieldLabel" => "TeacherName",
				"FieldNameChi" => "教師名稱",
				"FieldNameEn" => "Teacher Name",
				"isMultiLang" => "Y",
				"SampleDataChi"=> "陳大文",
				"SampleData"=> "Chan Ta Man",
				"FieldType"=> "TEXT",
				"FixedItem" => "Y",
				"FieldStatus" => "reference",
				"showInGrid" => "N",
				"isUsed" => "Y",
				"Priority" => "2"
			),
			"AcademicYear" => array(
				"FieldLabel" => "AcademicYear",
				"FieldNameChi" => "學年",
				"FieldNameEn" => "School Year",
				"isMultiLang" => "N",
				"SampleDataChi"=> "",
				"SampleData"=> "2016-2017",
				"FieldType"=> "TEXT",
				"FixedItem" => "N",
				"FieldStatus" => "required",
				"showInGrid" => "Y",
				"isUsed" => "Y",
				"Priority" => "3"
			),
			"StartDate" => array(
				"FieldLabel" => "StartDate",
				"FieldNameChi" => "開始日期",
				"FieldNameEn" => "Start Date",
				"isMultiLang" => "N",
				"SampleDataChi"=> "2017-01-01",
				"SampleData"=> "2017-01-01",
				"FieldType"=> "DATE",
				"FixedItem" => "N",
				"FieldStatus" => "required",
				"showInGrid" => "Y",
				"isUsed" => "Y",
				"Priority" => "4"
			),
			"EndDate" => array(
				"FieldLabel" => "EndDate",
				"FieldNameChi" => "完結日期",
				"FieldNameEn" => "End Date",
				"isMultiLang" => "N",
				"SampleDataChi"=> "2017-01-31",
				"SampleData"=> "2017-01-31",
				"FieldType"=> "DATE",
				"FixedItem" => "N",
				"FieldStatus" => "required",
				"showInGrid" => "Y",
				"isUsed" => "Y",
				"Priority" => "5"
			),
			"NoOfSection" => array(
				"FieldLabel" => "NoOfSection",
				"FieldNameChi" => "節數",
				"FieldNameEn" => "No. of Section",
				"isMultiLang" => "N",
				"SampleDataChi"=> "1",
				"SampleData"=> "1",
				"FieldType"=> "FLOAT",
				"FixedItem" => "N",
				"FieldStatus" => "required",
				"showInGrid" => "Y",
				"isUsed" => "Y",
				"Priority" => "6"
			),
			"EventName" => array(
				"FieldLabel" => "EventName",
				"FieldNameChi" => "課程 /活動名稱",
				"FieldNameEn" => "Course Name/Event Name",
				"isMultiLang" => "Y",
				"SampleDataChi"=> "課程名稱",
				"SampleData"=> "Course Name",
				"FieldType"=> "TEXT",
				"FixedItem" => "N",
				"FieldStatus" => "required",
				"showInGrid" => "Y",
				"isUsed" => "Y",
				"Priority" => "7"
			),
			"EventType" => array(
				"FieldLabel" => "EventType",
				"FieldNameChi" => "性質",
				"FieldNameEn" => "Type",
				"isMultiLang" => "Y",
				"SampleDataChi"=> "研討會",
				"SampleData"=> "Seminar",
				"FieldType"=> "TEXT",
				"FixedItem" => "N",
				"FieldStatus" => "required",
				"showInGrid" => "Y",
				"isUsed" => "Y",
				"Priority" => "8"
			),
			"Remarks" => array(
				"FieldLabel" => "Remarks",
				"FieldNameChi" => "備註",
				"FieldNameEn" => "Remarks",
				"isMultiLang" => "Y",
				"SampleDataChi"=> "備註",
				"SampleData"=> "Remarks",
				"FieldType"=> "TEXT",
				"FixedItem" => "N",
				"FieldStatus" => "normal",
				"showInGrid" => "Y",
				"isUsed" => "Y",
				"Priority" => "9"
			)
		);
		if($this->parent->data['userInfo']["customization"]['useEDBCPD']){
			$predefinedArr["Content"] = array(
				"FieldLabel" => "Content",
				"FieldNameChi" => "課程/活動內容",
				"FieldNameEn" => "Content",
				"isMultiLang" => "N",
				"SampleDataChi"=> "課程/活動內容",
				"SampleData"=> "Content",
				"FieldType"=> "TEXT",
				"FixedItem" => "N",
				"FieldStatus" => "normal",
				"showInGrid" => "Y",
				"isUsed" => "Y",
				"Priority" => "10"
			);
			$predefinedArr["Organization"] = array(
				"FieldLabel" => "Organization",
				"FieldNameChi" => "主辦機構",
				"FieldNameEn" => "Organizing Body",
				"isMultiLang" => "N",
				"SampleDataChi"=> "主辦機構",
				"SampleData"=> "Organizing Body",
				"FieldType"=> "TEXT",
				"FixedItem" => "N",
				"FieldStatus" => "normal",
				"showInGrid" => "Y",
				"isUsed" => "Y",
				"Priority" => "11"
			);
			$predefinedArr["CPDMode"] = array(
				"FieldLabel" => "CPDMode",
				"FieldNameChi" => "持續專業發展模式",
				"FieldNameEn" => "CPD Mode",
				"isMultiLang" => "N",
				"SampleDataChi"=> "持續專業發展模式",
				"SampleData"=> "CPD Mode",
				"SampleDataChi"=> "0",
				"SampleData"=> "0",
				"FieldType"=> "INT",
				"FixedItem" => "N",
				"showInGrid" => "Y",
				"isUsed" => "Y",
				"Priority" => "12"
			);
			$predefinedArr["CPDDomain"] = array(
				"FieldLabel" => "CPDDomain",
				"FieldNameChi" => "範疇",
				"FieldNameEn" => "CPD Domain(s)",
				"isMultiLang" => "N",
				"SampleDataChi"=> "0,0,0,0,0,0",
				"SampleData"=> "0,0,0,0,0,0",
				"FieldType"=> "TEXT",
				"FixedItem" => "N",
				"FieldStatus" => "normal",
				"showInGrid" => "Y",
				"isUsed" => "Y",
				"Priority" => "13"
			);
			if($this->parent->data['userInfo']["customization"]["cpdSubjectTag"] ){
				$predefinedArr["SubjectRelated"] = array(
					"FieldLabel" => "SubjectRelated",
					"FieldNameChi" => "科目",
					"FieldNameEn" => "Subjects",
					"isMultiLang" => "N",
					"SampleDataChi"=> "0,0,0,0,0,0,0,0,0",
					"SampleData"=> "0,0,0,0,0,0,0,0,0",
					"FieldType"=> "TEXT",
					"FixedItem" => "N",
					"FieldStatus" => "normal",
					"showInGrid" => "Y",
					"isUsed" => "Y",
					"Priority" => "14"
				);
			}/*
			$predefinedArr["BasicLawRelated"] = array(
				"FieldLabel" => "BasicLawRelated",
				"FieldNameChi" => "與基本法相關的持續專業發展時數",
				"FieldNameEn" => "Hours Related to Basic Law",
				"isMultiLang" => "N",
				"SampleDataChi"=> "1",
				"SampleData"=> "1",
				"FieldType"=> "FLOAT",
				"FixedItem" => "N",
				"FieldStatus" => "normal",
				"showInGrid" => "N",
				"isUsed" => "Y",
				"Priority" => "14"
			);*/
		}
		if($this->parent->data['userInfo']["customization"]["ltmpsStyle"] ){
			$predefinedArr['EventType']['FieldStatus'] = "normal";
		}	
		$this->parent->FormTemplateObj->setCoreData($this->parent->data);
		$this->parent->data["formTplInfo"] = $this->parent->FormTemplateObj->initTemplate($this->parent->data["template"], $predefinedArr);
		if($this->parent->data['userInfo']["customization"]['useEDBCPD']){
			$this->parent->data["formTplInfo"]["childs"]["NoOfSection"]["FieldNameChi"] = "持續專業發展時數";
			$this->parent->data["formTplInfo"]["childs"]["NoOfSection"]["FieldNameEn"] = "CPD Hour(s)";
		}
		if ($this->parent->data["formTplInfo"] == NULL) {
			echo "Template Init Error";
			exit;
		}
		$this->parent->data["yearArr"] = $this->parent->FormTemplateObj->getAcademicYear();
//		$this->parent->data["yearArr"] = $this->parent->YearObj->getAcademicYear();
		$this->parent->data["selectedYear"] = isset($this->parent->data["postData"]["selectedYear"]) ? $this->parent->data["postData"]["selectedYear"] : "";
		$this->parent->data["html_YearPanel"] = $this->parent->YearObj->getYearPanel($this->parent->data["yearArr"], $this->parent->data["lang"], $this->parent->data["section"], $this->parent->data["selectedYear"]);
	}

	public function getRecordByID($id) {
		$result = false;
		if ($id > 0) {
			$strSQL = "SELECT * FROM " . $this->mainTable;
			$strSQL .= " WHERE " . $this->primaryKey . "='" . $this->parent->FormTemplateObj->Get_Safe_Sql_Query($id) . "' limit 1";
			$result = $this->parent->FormTemplateObj->returnDBResultSet($strSQL);
			if (count($result) == 1) {
				$result = $result[0];
			}
		}
		return $result;
	}

	public function addrecord() {
		$this->parent->data["isEJ"] = $this->parent->FormTemplateObj->isEJ;
		$this->parent->data["allTeachers"] = $this->parent->UserObj->getAllTeacher();
		//$this->parent->data["yearArr"] = $this->parent->YearObj->getAcademicYear();
		$this->parent->data["yearArr"] = $this->parent->FormTemplateObj->getAcademicYear("",true,true);
		$this->parent->data["currAction"] = "add";
		if ($this->parent->configs["isDev"]) {
			$this->parent->data["debugMsg"] = $this->parent->getMemoryInfo();
		}
		if($this->parent->data['userInfo']["customization"]['useEDBCPD']){
			$this->parent->tpl->view($this->parent->data["section"] . "/edit_edb", $this->parent->data);
		}else{	
			$this->parent->tpl->view($this->parent->data["section"] . "/edit", $this->parent->data);
		}
	}
	
	public function editrecord() {
		$getStr = $this->parent->data["getData"]["task"];
		$recordID = substr($getStr,strrpos($getStr,".") + 1);
		$this->parent->data["isEJ"] = $this->parent->FormTemplateObj->isEJ;
		$this->parent->data["allTeachers"] = $this->parent->UserObj->getAllTeacher();
		$this->parent->data["yearArr"] = $this->parent->YearObj->getAcademicYear();
		$this->parent->data["currAction"] = "edit";
		$this->parent->data["infoRecord"] = $this->getRecordByID($recordID);
		if (!$this->parent->data["infoRecord"]) {
			$this->parent->data["infoRecord"] = "no record";
		} else {
			// Do nothing
		}
		if ($this->parent->configs["isDev"]) {
			$this->parent->data["debugMsg"] = $this->parent->getMemoryInfo();
		}
		if($this->parent->data['userInfo']["customization"]['useEDBCPD']){
			$this->parent->tpl->view($this->parent->data["section"] . "/edit_edb", $this->parent->data);
		}else{	
			$this->parent->tpl->view($this->parent->data["section"] . "/edit", $this->parent->data);
		}
	}

	public function dataHandler($userInfo, $data, $action, $id) {
		$output = array(
			"result" => "fail",
			"data" => "Input Error! Please check"
		);
		$isError = false;
		$academicYearName = '';
		if ($data["dataInfo"]["AcademicYear"]["data"]) {
			$academicYearID = $data["dataInfo"]["AcademicYear"]["data"];
			$strSQL = "SELECT YearNameEN FROM " . $this->yearTable . " WHERE AcademicYearID='" . $academicYearID . "'";
			$result = $this->parent->FormTemplateObj->returnDBResultSet($strSQL);
			if (count($result) > 0) {
				$academicYearName = $result[0]["YearNameEN"];
				$data["dataInfo"]["AcademicYear"]["data"] = $academicYearName; 
			}
		}
		
		$log_name = "";
		if ($data["dataInfo"]["UserID"]["data"]) {
			$strSQL = "SELECT EnglishName FROM " . $this->userTable . " WHERE UserID='" . $data["dataInfo"]["UserID"]["data"] . "'";
			$result = $this->parent->FormTemplateObj->returnDBResultSet($strSQL);
			if (count($result) > 0) {
				$log_name = $result[0]["EnglishName"];
			}
		}
		
		$cpdParam = $this->dataField;
		$cpdParam[] = "DateModified";
		$cpdParam[] = "ModifyBy";
		
		$data["dataInfo"]["moduleSection"] = array( "field" => "moduleSection", "data" => "CPD");
		$data["dataInfo"]["DateModified"] = array( "field" => "DateInput", "data" => "__dbnow__");
		$data["dataInfo"]["ModifyBy"] = array( "field" => "InputBy", "data" => $userInfo["info"]["UserID"]);
		
		switch ($action) {
			case "INSERT":
				/********* extra field for Insert ***********/
				$cpdParam[] = "DateInput";
				$cpdParam[] = "InputBy";
				$data["dataInfo"]["InputBy"] = array( "field" => "InputBy", "data" => $userInfo["info"]["UserID"]);
				$data["dataInfo"]["DateInput"] = array( "field" => "DateInput", "data" => "__dbnow__");
				
				/********* extra field for Insert ***********/
				
				$strSQL = $this->parent->FormTemplateObj->buildInsertSQL($this->mainTable, $cpdParam, $data["dataInfo"]);
				$result = $this->parent->FormTemplateObj->DBdb_query($strSQL);
				if (!$result) {
					$isError = true;
				} else {
					$id = $this->parent->FormTemplateObj->db_insert_id();
					
					if ($id > 0) {
						$log_year = $academicYearName;
						
						if (!empty($log_name) && !empty($log_year)) {
							$logDetail = "Add Record:<div class='log_detail'>";
							$logDetail .= "<small>"  . $log_name . " / ";
							$logDetail .= $log_year . " / " . $data["dataInfo"]["StartDate"]["data"] . " / " . $data["dataInfo"]["EndDate"]["data"] . " / " . $data["dataInfo"]["NoOfSection"]["data"] . " / " . $data["dataInfo"]["EventNameEn"]["data"] . " / " . $data["dataInfo"]["EventTypeEn"]["data"];
							$logDetail .= "</small>";
							$logDetail .= "</div>";
							$this->parent->load_model('LogObj');
							$this->parent->LogObj->addRecord($userInfo["info"]["UserID"], str_replace("_", " ", strtoupper($data["dataInfo"]["moduleSection"]["data"])), $logDetail);
						}
					}
				}
				$msg = $this->parent->data["lang"]["TeacherPortfolio"]["InsertSuccess"];
				break;
				
			case "UPDATE":
				/********* extra field for Update ***********/
				$condition = $this->primaryKey . "='" . $id . "'";
				$strSQL = $this->parent->FormTemplateObj->buildUpdateSQL($this->mainTable, $cpdParam, $data["dataInfo"], $condition);
				$result = $this->parent->FormTemplateObj->DBdb_query($strSQL);
				if (!$result) {
					$isError = true;
				} else {
					$log_year = $academicYearName;
										
					if (!empty($log_name) && !empty($log_year)) {
						$logDetail = "Update Record:<div class='log_detail'>";
						$logDetail .= "<small>"  . $log_name . " / ";
						$logDetail .= $log_year . " / " . $data["dataInfo"]["StartDate"]["data"] . " / " . $data["dataInfo"]["EndDate"]["data"] . " / " . $data["dataInfo"]["NoOfSection"]["data"] . " / " . $data["dataInfo"]["EventNameEn"]["data"] . " / " . $data["dataInfo"]["EventTypeEn"]["data"];
						$logDetail .= "</small>";
						$logDetail .= "</div>";
						$this->parent->load_model('LogObj');
						$this->parent->LogObj->addRecord($userInfo["info"]["UserID"], str_replace("_", " ", strtoupper($data["dataInfo"]["moduleSection"]["data"])), $logDetail);
					}
				}
				$msg = $this->parent->data["lang"]["TeacherPortfolio"]["UpdateSuccess"];
				break;
		}
		if (!$isError) {
			$output = array(
				"result" => "success",
				"data" => array( "id" => $id, "msg" => $msg )
			);
		}
			
		return $output;
	}
	
	public function process() {
		if (! ($this->parent->isAdmin() || $this->parent->isOwner() )) {
			No_Access_Right_Pop_Up();
			exit;
		}
		
		$processData = $this->parent->data["postData"];
		$data = array();
		foreach((array)$processData as $k=>$v) {
			if ($k == 'SelectedUserID') {
				list($userID,$encoded) = explode('_',$v);
				$data["dataInfo"][$k]["data"] = $userID;
				$data["dataInfo"]["UserID"] = array( "field" => "UserID", "data" => $userID);
			}
			else {
			    if ( !get_magic_quotes_gpc() || (phpversion_compare('5.4') != 'ELDER')) {
			        $data["dataInfo"][$k]["data"] = stripslashes($v);
			    }
			    else {
				    $data["dataInfo"][$k]["data"] = $v;
			    }
			}
		}
		
		switch ($this->parent->data["postData"]["action"]) {
			case "insertrecord":
				$myaction = "INSERT";
				$id = null;
				break;
			case "updaterecord":
				$myaction = "UPDATE";
				$id = $this->parent->data["postData"]["recid"];
				break;
		}

		$result = $this->dataHandler($this->parent->data["userInfo"], $data, $myaction, $id);
		$jsonObj = new JSON_obj();
		echo $jsonObj->encode($result);
	}
	
}
?>