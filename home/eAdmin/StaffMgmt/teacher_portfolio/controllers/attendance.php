<?php
if (!defined('TEACHER_PORTFOLIO')) die('Page not found');
if (!class_exists("FormTemplateController")) include_once(dirname(__FILE__) . "/formtemplate.controller.php");
class Attendance extends FormTemplateController {
	public function __construct($parent) {
		parent::__construct($parent);
		$this->parent->data["section"] = "attendance";
		$this->parent->data["template"] = "ATTENDANCE";
		$this->parent->load_model('FormTemplateObj');
		
		/***********************************************************/
		$this->parent->data["TemplateType"] = "GRID";
		$this->parent->data["TemplateGridTable"] = "INTRANET_TEACHER_PORTFOLIO_FORM_DATA_GRID_" . $this->parent->data["template"];
		$this->parent->data["TemplateGridTableStructure"] = array(
				"FormTemplateID" => array( "field" => "FormTemplateID", "data" => "", "output" => false),
				"UserID" => array( "field" => "UserID", "groupBY" => true, "data" => "", "output" => false),
				"AcademicYear" => array( "field" => "AcademicYear", "groupBY" => true, "data" => "", "output" => true),
				"Month" => array( "field" => "Month", "action" => "EMPTY", "groupBY" => false, "data" => "", "output" => true),
				"Attend" => array( "field" => "Attend", "groupBY" => false, "action" => "SUM", "data" => "", "output" => true),
				"Late" => array( "field" => "Late", "groupBY" => false, "action" => "SUM", "data" => "", "output" => true),
				"SickLeave" => array( "field" => "SickLeave", "groupBY" => false, "action" => "SUM", "data" => "", "output" => true),
				"CasualLeave" => array( "field" => "CasualLeave", "groupBY" => false, "action" => "SUM", "data" => "", "output" => true),
				"AnnualLeave" => array( "field" => "AnnualLeave", "groupBY" => false, "action" => "SUM", "data" => "", "output" => true),
				"DateInput" => array( "field" => "DateInput", "groupBY" => false, "data" => "__dbnow__", "output" => false),
				"InputBy" => array( "field" => "InputBy", "groupBY" => false, "data" => "", "output" => false),
				"DateModified" => array( "field" => "DateModified", "groupBY" => false, "data" => "__dbnow__", "output" => false),
				"ModifyBy" => array( "field" => "ModifyBy", "groupBY" => false, "data" => "", "output" => false)
		);
		/***********************************************************/
		
		$predefinedArr = array(
			"UserID" => array(
				"FieldLabel" => "UserID",
				"FieldNameChi" => "內聯網帳號",
				"FieldNameEn" => "Teacher Login ID",
				"isMultiLang" => "N",
				"SampleDataChi"=> "",
				"SampleData"=> "tchr01",
				"FieldType"=> "INT",
				"FixedItem" => "Y",
				"FieldStatus" => "required",
				"showInGrid" => "N",
				"isUsed" => "Y",
				"Priority" => "1"
			),
			"TeacherName" => array(
				"FieldLabel" => "TeacherName",
				"FieldNameChi" => "教師名稱",
				"FieldNameEn" => "Teacher Name",
				"isMultiLang" => "Y",
				"SampleDataChi"=> "陳大文",
				"SampleData"=> "Chan Ta Man",
				"FieldType"=> "TEXT",
				"FixedItem" => "Y",
				"FieldStatus" => "reference",
				"showInGrid" => "N",
				"isUsed" => "Y",
				"Priority" => "2"
			),
			"AcademicYear" => array(
				"FieldLabel" => "AcademicYear",
				"FieldNameChi" => "學年",
				"FieldNameEn" => "School Year",
				"isMultiLang" => "N",
				"SampleDataChi"=> "",
				"SampleData"=> "2016-2017",
				"FieldType"=> "TEXT",
				"FixedItem" => "N",
				"FieldStatus" => "required",
				"showInGrid" => "Y",
				"isUsed" => "Y",
				"Priority" => "3"
			),
			"Month" => array(
				"FieldLabel" => "Month",
				"FieldNameChi" => "月份",
				"FieldNameEn" => "Month",
				"isMultiLang" => "N",
				"SampleDataChi"=> "1",
				"SampleData"=> "1",
				"FieldType"=> "INT",
				"FixedItem" => "N",
				"FieldStatus" => "required",
				"showInGrid" => "Y",
				"isUsed" => "Y",
				"Priority" => "4"
			),
			"Attend" => array(
				"FieldLabel" => "Attend",
				"FieldNameChi" => "出席",
				"FieldNameEn" => "Attend",
				"isMultiLang" => "N",
				"SampleDataChi"=> "1",
				"SampleData"=> "1",
				"FieldType"=> "FLOAT",
				"FixedItem" => "N",
				"FieldStatus" => "required",
				"showInGrid" => "Y",
				"isUsed" => "Y",
				"Priority" => "5"
			),
			"Late" => array(
				"FieldLabel" => "Late",
				"FieldNameChi" => "遲到",
				"FieldNameEn" => "Late",
				"isMultiLang" => "N",
				"SampleDataChi"=> "2",
				"SampleData"=> "2",
				"FieldType"=> "FLOAT",
				"FixedItem" => "N",
				"FieldStatus" => "required",
				"showInGrid" => "Y",
				"isUsed" => "Y",
				"Priority" => "5"
			),
			"SickLeave" => array(
				"FieldLabel" => "SickLeave",
				"FieldNameChi" => "病假 (SL)",
				"FieldNameEn" => "Sick Leave",
				"isMultiLang" => "N",
				"SampleDataChi"=> "3",
				"SampleData"=> "3",
				"FieldType"=> "FLOAT",
				"FixedItem" => "N",
				"FieldStatus" => "required",
				"showInGrid" => "Y",
				"isUsed" => "Y",
				"Priority" => "6"
			),
			"CasualLeave" => array(
				"FieldLabel" => "CasualLeave",
				"FieldNameChi" => "事假  (CL)",
				"FieldNameEn" => "Casual Leave",
				"isMultiLang" => "N",
				"SampleDataChi"=> "4",
				"SampleData"=> "4",
				"FieldType"=> "FLOAT",
				"FixedItem" => "N",
				"FieldStatus" => "required",
				"showInGrid" => "Y",
				"isUsed" => "Y",
				"Priority" => "7"
			),
			"AnnualLeave" => array(
				"FieldLabel" => "AnnualLeave",
				"FieldNameChi" => "大假 (AL)",
				"FieldNameEn" => "Annual Leave",
				"isMultiLang" => "N",
				"SampleDataChi"=> "4",
				"SampleData"=> "4",
				"FieldType"=> "FLOAT",
				"FixedItem" => "N",
				"FieldStatus" => "required",
				"showInGrid" => "Y",
				"isUsed" => "Y",
				"Priority" => "8"
			)
		);
		
		$this->parent->FormTemplateObj->setCoreData($this->parent->data);
		$this->parent->data["formTplInfo"] = $this->parent->FormTemplateObj->initTemplate($this->parent->data["template"], $predefinedArr);
		if ($this->parent->data["formTplInfo"] == NULL) {
			echo "Template Init Error";
			exit;
		}
		$this->parent->data["yearArr"] = $this->parent->FormTemplateObj->getAcademicYear();
		$this->parent->data["selectedYear"] = isset($this->parent->data["postData"]["selectedYear"]) ? $this->parent->data["postData"]["selectedYear"] : "";
		$this->parent->data["html_YearPanel"] = $this->parent->YearObj->getYearPanel($this->parent->data["yearArr"], $this->parent->data["lang"], $this->parent->data["section"], $this->parent->data["selectedYear"]);
	}
}
?>