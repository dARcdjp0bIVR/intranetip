<?php
if (defined('TEACHER_PORTFOLIO')) {
	class TBPath {
		public function __construct() {
		}
		
		public function pathInfos($currPath) {
			$current_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
			$this->urlInfo = parse_url($current_link);
			$pos = strpos($this->urlInfo["path"], ".php");
			if ($pos !== false) {
				$this->urlInfo["path"] = dirname(rtrim($this->urlInfo["path"], "/"));
			} else {
				$this->urlInfo["path"] = rtrim($this->urlInfo["path"], "/");
			}
			$pathArr = explode("/", trim($this->urlInfo["path"], "/"));
			if (count($pathArr) > 0) {
				$this->urlInfo["PATH_WRT_ROOT"] = "";
				foreach ($pathArr as $kk => $vv) {
					$this->urlInfo["PATH_WRT_ROOT"] .= "../";
				}
			} else {
				$this->urlInfo["PATH_WRT_ROOT"] = "./";
			}			
				
			$this->urlInfo["curr_path"] = $this->get_absolute_path(dirname($currPath));
			$this->urlInfo["tbFolder"] = "teacher_portfolio";
			
			$intranetdataFolderPath = $this->get_intranetdata_folder($this->urlInfo["curr_path"]);		
			if($intranetdataFolderPath){
				$uploadFilePath = $intranetdataFolderPath ."/". $this->urlInfo["tbFolder"];
			}else{
				$uploadFilePath =  $this->getFullPath($this->urlInfo["curr_path"], $this->urlInfo["PATH_WRT_ROOT"] . "file/" . $this->urlInfo["tbFolder"]);
			}		
			$this->urlInfo["uploadFilePath"] = $uploadFilePath;
			if(!file_exists($uploadFilePath)){
				mkdir($uploadFilePath, 0757);
			}
			$this->urlInfo["uploadFilePathMain"] = $this->get_absolute_path($this->urlInfo["uploadFilePath"] . "/uploads");
			if(!file_exists($this->urlInfo["uploadFilePathMain"])){
				mkdir($this->urlInfo["uploadFilePathMain"], 0757);
			}
			$this->urlInfo["uploadFilePathTemp"] = $this->get_absolute_path($this->urlInfo["uploadFilePath"] . "/tmp");
			if(!file_exists($this->urlInfo["uploadFilePathTemp"])){
				mkdir($this->urlInfo["uploadFilePathTemp"], 0757);
			}
			// $this->urlInfo["uploadFilePathViewTemp"] = $this->get_absolute_path($this->urlInfo["uploadFilePath"] . "/views");
		}
		
		public function get_absolute_path($path) {
			$path = str_replace(array('/', '\\'), DIRECTORY_SEPARATOR, $path);
			$parts = array_filter(explode(DIRECTORY_SEPARATOR, $path), 'strlen');
			$absolutes = array();
			foreach ($parts as $part) {
				if ('.' == $part) continue;
				if ('..' == $part) {
					array_pop($absolutes);
				} else {
					$absolutes[] = $part;
				}
			}
			$output_path = implode(DIRECTORY_SEPARATOR, $absolutes);
			if (substr($output_path, 0, 1) !== "/") {
				if (substr($output_path, 1, 1) != ":") {
					return "/" . $output_path;
				} else {
					return $output_path;
				}
			} else {
				return $output_path;
			}
		}
		
		public function getFullPath($currPath, $path) {
			$paths = explode("../", $path);
			$topLevel = $currPath;
			if (count($paths) > 0) {
				foreach ($paths as $kk => $vv) {
					if (empty($vv)) {
						$topLevel = dirname($topLevel);
					} else {
						$topLevel .= "/" . $vv;
					}
				}
			}
			return $this->get_absolute_path($topLevel);
		}
		
		public function get_intranetdata_folder($path){
			$pathArr = explode("/",$path);
			$currPath = "";
			foreach($pathArr as $idx=>$pathComp){
				if($idx==0)continue;
				$currPath = $currPath."/".$pathComp;
				if(is_dir($currPath."/intranetdata")){
					return $currPath."/intranetdata";
				}
			}
			return false;	
		}
		
		public function getUrlInfo() {
			return $this->urlInfo;
		}
	}
}