<div class="toolbar">
	<div class="filter btn-group">
		<?php echo $data["html_YearPanel"]; ?>
	</div>
	<ul class="actions"> 
		<li><a href="#" rel="<?php echo $data["section"]; ?>/settings" class="btnAjax btnExport"><i class="glyphicon glyphicon-cog"></i> <?php echo $data["lang"]["TeacherPortfolio"]["Settings"]; ?></a></li>
		<li><a href="#" rel="<?php echo $data["section"]; ?>/exportrecord" class="btnAjax btnExport"><i class="glyphicon glyphicon-save-file"></i> <?php echo $data["lang"]["TeacherPortfolio"]["btnExport"]; ?></a></li>
		<li><a href="#" class="btnPrint printAction"><i class="glyphicon glyphicon-print"></i> <?php echo $data["lang"]["TeacherPortfolio"]["btnPrint"]; ?></a></li>
	</ul>
</div>
<div class="content attendance">
	<table id="attendance-bdTable" class="tp-table bdTable" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th class='tbheader_col' style='width:20%' rel='academicYear'><?php echo $data["lang"]["TeacherPortfolio"]["SchoolYear"]; ?>  <span class="tp-sort"><i class="fa fa-unsorted"></i><i class="fa fa-sort-asc"></i><i class="fa fa-sort-desc"></i></span></th><!-- use "fa-sort-asc" for ascending order-->
				<th class='tbheader_col' style='width:20%' rel='attend'><?php echo $data["lang"]["TeacherPortfolio"]["Attend"]; ?>  <span class="tp-sort"><i class="fa fa-unsorted"></i><i class="fa fa-sort-asc"></i><i class="fa fa-sort-desc"></i></span></th>
				<th class='tbheader_col' style='width:20%' rel='late'><?php echo $data["lang"]["TeacherPortfolio"]["Late"]; ?>  <span class="tp-sort"><i class="fa fa-unsorted"></i><i class="fa fa-sort-asc"></i><i class="fa fa-sort-desc"></i></span></th>
				<th class='tbheader_col' style='width:20%' rel='sickLeave'><?php echo $data["lang"]["TeacherPortfolio"]["SickLeave"]; ?>  <span class="tp-sort"><i class="fa fa-unsorted"></i><i class="fa fa-sort-asc"></i><i class="fa fa-sort-desc"></i></span></th>
				<th class='tbheader_col' style='width:20%' rel='annualLeave'><?php echo $data["lang"]["TeacherPortfolio"]["AnnualLeave"]; ?>  <span class="tp-sort"><i class="fa fa-unsorted"></i><i class="fa fa-sort-asc"></i><i class="fa fa-sort-desc"></i></span></th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table><br>
	<?php echo $data["defaultFooter"]; ?>
</div>