<div class="toolbar">
	<ul class="actions">                      
	<li><a href="#" rel="<?php echo $data["section"]; ?>.exportrecord" class="btnFDAjax"><i class="glyphicon glyphicon-save-file"></i> <?php echo $data["lang"]["TeacherPortfolio"]["btnExport"]; ?></a></li>
		<li><a href="#" class="btnPrint printAction"><i class="glyphicon glyphicon-print"></i> <?php echo $data["lang"]["TeacherPortfolio"]["btnPrint"]; ?></a></li>
	</ul>
</div>
<div class="content profile">
	<div class="row">
		<label class="col-form-label"><?php echo $data["lang"]["TeacherPortfolio"]["info_ChineseName"]; ?></label>
		<div class="col-field"><span class="info"><?php echo checkEmpty($data["profile"]["ChineseName"]); ?></span></div>
	</div>
	<div class="row">
		<label class="col-form-label"><?php echo $data["lang"]["TeacherPortfolio"]["info_EnglishName"]; ?></label>
		<div class="col-field"><span class="info"><?php echo checkEmpty($data["profile"]["EnglishName"]); ?></span></div>
	</div>
	<div class="row">
		<label class="col-form-label"><?php echo $data["lang"]["TeacherPortfolio"]["info_Gender"]; ?></label>
		<div class="col-field"><span class="info short"><?php echo checkEmpty($data["lang"]["TeacherPortfolio"]["info_GenderOpt"][$data["profile"]["Gender"]]); ?></span></div>
	</div>
	<div class="row">
		<label class="col-form-label"><?php echo $data["lang"]["TeacherPortfolio"]["info_DateOfEntry"]; ?></label>
		<div class="col-field"><span class="info short"><?php echo checkEmpty($data["profile"]["DateOfEntry"]); ?></span></div>
	</div>
	<div class="row">
		<label class="col-form-label"><?php echo $data["lang"]["TeacherPortfolio"]["info_Position"]; ?></label>
		<div class="col-field"><span class="info"><?php echo checkEmpty($data["profile"]["Position"]); ?></span></div>
	</div>
	<div class="row">
		<label class="col-form-label"><?php echo $data["lang"]["TeacherPortfolio"]["info_MobileTel"]; ?></label>
		<div class="col-field"><span class="info short"><?php echo checkEmpty($data["profile"]["MobileTelNo"]); ?></span></div>
	</div>
	<div class="row">
		<label class="col-form-label"><?php echo $data["lang"]["TeacherPortfolio"]["info_HomeTel"]; ?></label>
		<div class="col-field"><span class="info short"><?php echo checkEmpty($data["profile"]["HomeTelNo"]); ?></span></div>
	</div>
	<div class="row">
		<label class="col-form-label"><?php echo $data["lang"]["TeacherPortfolio"]["info_Email"]; ?></label>
		<div class="col-field"><span class="info"><?php echo checkEmpty($data["profile"]["UserEmail"]); ?></span></div>
	</div>
	<div class="row">
		<label class="col-form-label"><?php echo $data["lang"]["TeacherPortfolio"]["info_Address"]; ?></label>
		<div class="col-field"><span class="info"><?php echo checkEmpty($data["profile"]["Address"]); ?></span></div>
	</div>
	<?php echo $data["defaultFooter"]; ?>
</div> 