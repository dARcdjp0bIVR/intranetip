<?php
/*
 *  2020-05-19 Cameron
 *      - replace samplephoto.gif with samplephoto_personal.png for default personal image
 */
global $intranet_session_language;
?>
<div class="wrapper <?php if ($data["_ROLE_"]["isAdminMode"]) { echo "special"; } else if ($data["_ROLE_"]["isViewerMode"]) { echo "special2"; } ?>">
	<!--HEADER-->
	<div class="tp-header">
<?php /* ?>
		<div class='user-lang text-center pull-right'>
			<div class="btn-group" role="group" aria-label="...">
				<a href="#" rel="en" class="btn btn-<?php if ($intranet_session_language == "en") { echo "warning"; } else { echo "default"; } ?> btn-sm">ENG</a>
				<a href="#" rel="b5" class="btn btn-<?php if ($intranet_session_language == "b5") { echo "warning"; } else { echo "default"; } ?> btn-sm">繁</a>
			</div>
		</div>
<?php */ ?>
<?php if (count($data["userInfo"]["allowMode"]) > 1) { ?>
		<div class='user-mode pull-right'>
			<div class="btn-group" role="group" aria-label="...">
<?php foreach ($data["userInfo"]["allowMode"] as $kk => $val) { ?>
				<a href="#" rel="<?php echo $val; ?>" class="btn btn-<?php if ($data["userInfo"]["currMode"] == $val) { echo "warning"; } else { echo "default"; } ?> btn-sm"><?php echo $data["lang"]["TeacherPortfolio"]["sysmode_" . $val]; ?></a>
<?php } ?>
			</div>
		</div>
<?php } ?>
<?php
	if ($data["_ROLE_"]["isViewerMode"]) {
		if ($data["_ROLE_"]["hasAdminRight"] || $data["_ROLE_"]["hasViewerRight"]) {
?>
		<div class='thrList text-center pull-right'>
			<select class="form-control input-sm">
<?php
			if (count($data["allTeachers"]) > 0) {
?>
				<option value='0'><?php echo $data["lang"]['TeacherPortfolio']["pleaseSelectTeacher"]; ?></option>
<?php
				foreach ($data["allTeachers"] as $tkk => $tvv) {
					echo "<option value='" . $tvv["UserID"] . "_" . $tvv["cs"] . "'";
					if ($data["userInfo"]["info"]["SelectedUserID"] == $tvv["UserID"]) {
						echo " selected";
					}
					echo ">" . Get_Lang_Selection($tvv["ChineseName"], $tvv["EnglishName"]) . " ( LoginID: " . $tvv["UserLogin"]. " )</option>";
				}
			} ?></select>
		</div>
<?php
		}
	} ?>
		<span class="logo"><?php echo $data["lang"]["TeacherPortfolio"]["ModuleTitle"]; ?></span>   <!--class="logo bold" for Chinese version-->
		<span class="gradient"></span>
	</div>
	<!--/HEADER-->
	<!--SIDEBAR-->
	<div class="tp-sidebar">
		<div class="user-photo"><img src='<?php
		if ($data["profile"]["PersonalPhotoLink"] != "") { echo $data["profile"]["PersonalPhotoLink"]; } else { echo "/images/myaccount_personalinfo/samplephoto_personal.png"; }
		?>'></div>
		<div class="user-name"><?php echo Get_Lang_Selection($data["profile"]["ChineseName"], $data["profile"]["EnglishName"]); ?><br /><?php echo $data["lang"]["TeacherPortfolio"]["Teacher"]; ?></div><br>
	</div>
	<!--/SIDEBAR-->
	<div class="tp-right">
		<!--MENU-->
  		<ul class="nav nav-pills nav-justified tp-menu">
			<?php
			if (count($data["userInfo"]["grantSection"]) > 0) {
				foreach ($data["userInfo"]["grantSection"] as $kk => $section) {
					if ($section != "nav") {
			?>
			<li><a data-toggle="tab" href="#<?php echo $section; ?>" rel="list"><?php echo $data["lang"]["TeacherPortfolio"]["sec_" . $section]; ?></a></li>
			<?php
					}
				}
			}?>
		</ul>
		<!--MENU-->
  		<div class="tab-content">
        	<?php
			if (count($data["userInfo"]["grantSection"]) > 0) {
				foreach ($data["userInfo"]["grantSection"] as $kk => $section) {
			?>
			<div id="<?php echo $section; ?>" class="tab-pane fade in DivIdToPrint">
				
			</div>
			<?php
				}
			}?>
			<div id="norecord" class="tab-pane fade in DivIdToPrint">
				Page not found
			</div>
  		</div>
  		<div id="selectUser">
			<div class='text-danger'><h4><i class='fa fa-warning'></i> <?php echo $data["lang"]["TeacherPortfolio"]["pleaseSelectTeacher"]; ?></h4></div>
		</div>
	</div>
</div>