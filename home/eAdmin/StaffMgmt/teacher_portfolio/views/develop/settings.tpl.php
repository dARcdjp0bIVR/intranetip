<div class="toolbar">
	<ul class="actions left">
		<li><a href="#" rel="<?php echo $data["section"]; ?>" class="btnAjax"><i class="fa fa-caret-left"></i> <?php echo $data["lang"]["TeacherPortfolio"]["back"]; ?></a></li>
	</ul>
</div>
<div class="content attendance settings">
	<div class="container-fluid">
		<div class="title">
		<span class="text1"><?php echo $data["settings_menu"][$data["selectedType"]]; ?></span> <span class="text2"><?php echo $data["lang"]["TeacherPortfolio"]["sec_" . $data["section"]]; ?> > <?php echo $data["lang"]["TeacherPortfolio"]["Settings"]; ?></span>
	</div><br>
	<div class="container-fluid <?php echo $data["currMethod"]; ?>">
		<div class="row">
			<div class="col-sm-12">
				<ul class='settingnav pull-right'>
				<?php
					if (count($data["settings_menu"]) > 0) {
						foreach ($data["settings_menu"] as $mkk => $kvv) {
				?>
					<li><a href="#" rel="<?php echo $data["section"]; ?>/settings&otype=<?php echo $mkk; ?>" class="btn btn-sm btnAjax btn-<?php echo ($data["selectedType"] == $mkk) ? 'primary': 'default'; ?>"><?php echo $data["lang"]["TeacherPortfolio"][ucfirst(strtolower($mkk))]; ?></a></li>
				<?php
						}
					}
				?>
				</ul>
			</div>
		</div><br>
		<div class="row">
			<div class="col-sm-12">
				<div class="container-fluid">
					<div class='row'>
						<div class="mainPanel col-xs-12">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4><?php echo $data["lang"]["TeacherPortfolio"]["settings_DataColumnSorting"]; ?></h4>
								</div>
								<div class="panel-body">
									<ol class='cusSort listhdr'>
							<?php
								if (isset($data["Options"][$data["selectedType"] . "Arr"])) {
									foreach ($data["Options"][$data["selectedType"] . "Arr"] as $kk => $val) {
										switch ($data["selectedType"]) {
											case "mode":
												$keyname = "CPDModeID";
												$nameChi = "CPDModeNameChi";
												$nameEn = "CPDModeNameEn";
												break;
											case "nature":
												$keyname = "CPDNatureID";
												$nameChi = "CPDNatureNameChi";
												$nameEn = "CPDNatureNameEn";
												break;
											default:
												$keyname = "CPDCategoryID";
												$nameChi = "CPDCategoryNameChi";
												$nameEn = "CPDCategoryNameEn";
												break;
										}
										echo '<li id="field_' . $val[$keyname] . '" data-nameCN="' . $val[$nameChi] . '" data-nameEN="' . $val[$nameEn] . '" title="' . Get_Lang_Selection($val[$nameChi], $val[$nameEn]) . '">';
										echo '<div class="row">';
										echo '<div class="col-xs-8"><span class="glyphicon glyphicon-resize-vertical"></span> ' . $val[$nameChi] . ', ' . $val[$nameEn] . '</div>';
										echo '<div class="col-xs-4"><a href="#" class="pull-right jqbutton jqpencil"> <i class="fa fa-pencil"></i> </a><a href="#" class="jqbutton jqtrash pull-right"> <i class="fa fa-trash"></i> </a></div>';
										echo '</div>';
										echo '</li>';
									}
								}
							?>
									</ol><br>
									<a href="#" class='btn btn-sm btn-warning jqAddBtn pull-right'><?php echo $data["lang"]["TeacherPortfolio"]["add"]; ?></a>
									<a href="#" rel="<?php echo $data["selectedType"]; ?>" class='btn btn-sm btn-primary btnFormFieldSubmit'><?php echo $data["lang"]["TeacherPortfolio"]["Submit"]; ?></a>
									<a href="#" class='btn btn-sm btn-default btnAjax' rel="<?php echo $data["section"]; ?>/<?php echo $data["currMethod"]; ?>"><?php echo $data["lang"]["TeacherPortfolio"]["Reset"]; ?></a>
									<br><small class='txt_remarks'><i><span class='glyphicon glyphicon-resize-vertical'></span> <?php echo $data["lang"]["TeacherPortfolio"]["settings_dragtosort"]; ?></i></small>
								</div>
							</div><br>
						</div>
						<div class="jqbuttonPanel col-xs-12">
							<form>
							<div class="panel panel-primary">
								<input type='hidden' name="recid" id='recid' value='0'>
								<input type='hidden' name="otype" id="otype" value='<?php echo $data["selectedType"]; ?>'>
								<div class="panel-heading">
									<h4 class='formtype type_add'><?php echo $data["lang"]["TeacherPortfolio"]["add"]; ?></h4>
									<h4 class='formtype type_edit'><?php echo $data["lang"]["TeacherPortfolio"]["edit"]; ?></h4>
								</div>
								<div class="panel-body">
									<div class='form-group'>
										<label for='nameCN'><?php echo $data["lang"]["TeacherPortfolio"]["itemName"]; ?> (<?php echo $data["lang"]["TeacherPortfolio"]["lang_chi"]; ?>)</label>
										<input type='text' name="nameCN" class='form-control' id="nameCN" value=''>
									</div>
									<div class='form-group'>
										<label for='nameEN'><?php echo $data["lang"]["TeacherPortfolio"]["itemName"]; ?> (<?php echo $data["lang"]["TeacherPortfolio"]["lang_eng"]; ?>)</label>
										<input type='text' name="nameEN" class='form-control' id="nameEN" value=''>
									</div>
								</div>
								<div class="panel-footer">
									<a href="#" class='btn btn-sm btn-primary tplSubmit'><?php echo $data["lang"]["TeacherPortfolio"]["Submit"]; ?></a>
									<a href="#" class='btn btn-sm btn-default tplReset'><?php echo $data["lang"]["TeacherPortfolio"]["Cancel"]; ?></a>
								</div>
							</div>
							</form>
						</div>
					</div>
				</div>
			</div>
			</div>
		</div>
	</div>
	<iframe id="dfileframe" style="display:none;"></iframe>
	<?php echo $data["defaultFooter"]; ?>
</div>