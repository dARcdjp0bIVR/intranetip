<div class="toolbar">
	<div class="filter btn-group">
		<?php echo $data["html_YearPanel"]; ?>
	</div>
	<ul class="actions">
	<?php if ($data["_ROLE_"]["isAdminMode"] && $data["_ROLE_"]["hasAdminRight"]) { ?>
		<li><a href="#" rel="<?php echo $data["section"]; ?>/settings" class="btnAjax"><i class="glyphicon glyphicon-cog"></i> <?php echo $data["lang"]["TeacherPortfolio"]["Settings"]; ?></a></li>
	<?php
		}
		if ($data["_ROLE_"]["isOwnerMode"] && $data["_ROLE_"]["hasOwnerRight"]) {
	?>
		<li><a href="#" rel="<?php echo $data["section"]; ?>/addrecord" class="btnAjax"><i class="fa fa-plus"></i> <?php echo $data["lang"]["TeacherPortfolio"]["btnAddNew"]; ?></a></li>
	<?php } ?>
		<li><a href="#" rel="<?php echo $data["section"]; ?>.exportrecord" class="btnFDAjax"><i class="glyphicon glyphicon-save-file"></i> <?php echo $data["lang"]["TeacherPortfolio"]["btnExport"]; ?></a></li>
		<li><a href="#" class="btnPrint printAction"><i class="glyphicon glyphicon-print"></i> <?php echo $data["lang"]["TeacherPortfolio"]["btnPrint"]; ?></a></li>
	</ul>
</div>
<div class="content develop">
	<table id="develop-bdTable" class="tp-table bdTable" cellspacing="0" width="100%">
		<thead>
			<tr>
			<?php if ($data["_ROLE_"]["isAdminMode"] && $data["_ROLE_"]["hasAdminRight"] || $data["_ROLE_"]["isViewerMode"] && $data["_ROLE_"]["hasViewerRight"]) { ?>
				<th class='tbheader_col' width="15%" rel='TeacherName'><?php echo $data["lang"]["TeacherPortfolio"]["TeacherName"]; ?> <span class="tp-sort"><i class="fa fa-unsorted"></i><i class="fa fa-sort-asc"></i><i class="fa fa-sort-desc"></i></span></th><!-- use "fa-sort-asc" for ascending order-->
			<?php
				}
			?>
				<th class='tbheader_col' width="10%" rel='academicYear'><?php echo $data["lang"]["TeacherPortfolio"]["SchoolYear"]; ?> <span class="tp-sort"><i class="fa fa-unsorted"></i><i class="fa fa-sort-asc"></i><i class="fa fa-sort-desc"></i></span></th><!-- use "fa-sort-asc" for ascending order-->
				<th class='tbheader_col' width="10%" rel='StartDate'  ><?php echo $data["lang"]["TeacherPortfolio"]["StartDate"]; ?> <span class="tp-sort"><i class="fa fa-unsorted"></i><i class="fa fa-sort-asc"></i><i class="fa fa-sort-desc"></i></span></th>
				<th class='tbheader_col' width="10%" rel='EndDate' ><?php echo $data["lang"]["TeacherPortfolio"]["EndDate"]; ?> <span class="tp-sort"><i class="fa fa-unsorted"></i><i class="fa fa-sort-asc"></i><i class="fa fa-sort-desc"></i></span></th>
				<th class='tbheader_col' width="15%" rel='isTPDD' ><?php echo $data["lang"]["TeacherPortfolio"]["TPDD"]; ?> <span class="tp-sort"><i class="fa fa-unsorted"></i><i class="fa fa-sort-asc"></i><i class="fa fa-sort-desc"></i></span></th>
				<th class='tbheader_col' rel='ActivityName' ><?php echo $data["lang"]["TeacherPortfolio"]["Event"]; ?> <span class="tp-sort"><i class="fa fa-unsorted"></i><i class="fa fa-sort-asc"></i><i class="fa fa-sort-desc"></i></span></th>
				<th class='tbheader_col' width="10%" rel='Nature' data-sort='false' ><?php echo $data["lang"]["TeacherPortfolio"]["Nature"]; ?></th>
				<th class='tbheader_col' width="10%" rel='func' data-sort='false'>&nbsp;</th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table><br>
	<?php echo $data["defaultFooter"]; ?>
</div>